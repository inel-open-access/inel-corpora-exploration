<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID06F19C98-B179-8A63-817A-F808DF2800A3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0204.wav" />
         <referenced-file url="PKZ_1964_SU0204.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0204\PKZ_1964_SU0204.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1311</ud-information>
            <ud-information attribute-name="# HIAT:w">676</ud-information>
            <ud-information attribute-name="# e">632</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">39</ud-information>
            <ud-information attribute-name="# HIAT:u">248</ud-information>
            <ud-information attribute-name="# sc">110</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="MAK">
            <abbreviation>MAK</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.228" type="appl" />
         <tli id="T1" time="1.676" type="appl" />
         <tli id="T2" time="3.806652839851264" />
         <tli id="T3" time="3.826" type="appl" />
         <tli id="T4" time="4.099" type="appl" />
         <tli id="T5" time="4.371" type="appl" />
         <tli id="T6" time="4.643" type="appl" />
         <tli id="T7" time="4.915" type="appl" />
         <tli id="T8" time="5.188" type="appl" />
         <tli id="T9" time="5.46" type="appl" />
         <tli id="T10" time="5.614" type="appl" />
         <tli id="T11" time="5.775" type="appl" />
         <tli id="T12" time="5.935" type="appl" />
         <tli id="T13" time="6.096" type="appl" />
         <tli id="T14" time="6.256" type="appl" />
         <tli id="T15" time="6.667" type="appl" />
         <tli id="T16" time="7.046" type="appl" />
         <tli id="T17" time="7.425" type="appl" />
         <tli id="T18" time="8.142" type="appl" />
         <tli id="T19" time="8.852" type="appl" />
         <tli id="T20" time="9.241" type="appl" />
         <tli id="T21" time="9.63" type="appl" />
         <tli id="T22" time="10.019" type="appl" />
         <tli id="T23" time="10.507" type="appl" />
         <tli id="T24" time="10.99" type="appl" />
         <tli id="T25" time="12.193289043936884" />
         <tli id="T26" time="12.786" type="appl" />
         <tli id="T27" time="13.334" type="appl" />
         <tli id="T28" time="13.874" type="appl" />
         <tli id="T29" time="14.413" type="appl" />
         <tli id="T30" time="14.953" type="appl" />
         <tli id="T31" time="15.684" type="appl" />
         <tli id="T742" time="15.958499999999999" type="intp" />
         <tli id="T32" time="16.416" type="appl" />
         <tli id="T33" time="18.759931858741606" />
         <tli id="T34" time="19.398" type="appl" />
         <tli id="T35" time="19.922" type="appl" />
         <tli id="T36" time="20.445" type="appl" />
         <tli id="T37" time="21.161" type="appl" />
         <tli id="T38" time="21.801" type="appl" />
         <tli id="T39" time="22.44" type="appl" />
         <tli id="T40" time="23.888" type="appl" />
         <tli id="T41" time="24.504" type="appl" />
         <tli id="T42" time="25.093" type="appl" />
         <tli id="T43" time="25.682" type="appl" />
         <tli id="T44" time="26.713236303474627" />
         <tli id="T45" time="27.459" type="appl" />
         <tli id="T46" time="29.006561306817595" />
         <tli id="T47" time="29.882" type="appl" />
         <tli id="T48" time="30.906" type="appl" />
         <tli id="T49" time="31.463" type="appl" />
         <tli id="T50" time="32.02" type="appl" />
         <tli id="T51" time="32.578" />
         <tli id="T52" time="33.588" type="appl" />
         <tli id="T53" time="34.43320826240241" />
         <tli id="T54" time="34.77987366988449" />
         <tli id="T55" time="34.999872870786575" />
         <tli id="T56" time="35.15320564717288" />
         <tli id="T57" time="35.738" type="appl" />
         <tli id="T58" time="36.83986618742222" />
         <tli id="T59" time="37.634" type="appl" />
         <tli id="T60" time="38.285" type="appl" />
         <tli id="T61" time="38.935" type="appl" />
         <tli id="T62" time="40.17318741321141" />
         <tli id="T63" time="40.829" type="appl" />
         <tli id="T64" time="41.517" type="appl" />
         <tli id="T65" time="41.896" type="appl" />
         <tli id="T66" time="42.275" type="appl" />
         <tli id="T67" time="42.654" type="appl" />
         <tli id="T68" time="43.033" type="appl" />
         <tli id="T69" time="43.412" type="appl" />
         <tli id="T70" time="43.919840470998466" />
         <tli id="T71" time="44.489" type="appl" />
         <tli id="T72" time="45.064" type="appl" />
         <tli id="T73" time="45.638" type="appl" />
         <tli id="T74" time="46.212" type="appl" />
         <tli id="T75" time="46.787" type="appl" />
         <tli id="T76" time="47.361" type="appl" />
         <tli id="T77" time="48.70648975123176" />
         <tli id="T78" time="49.275" type="appl" />
         <tli id="T79" time="49.818" type="appl" />
         <tli id="T80" time="50.36" type="appl" />
         <tli id="T81" time="50.902" type="appl" />
         <tli id="T82" time="51.445" type="appl" />
         <tli id="T83" time="51.987" type="appl" />
         <tli id="T84" time="52.53" type="appl" />
         <tli id="T85" time="53.95313736062395" />
         <tli id="T86" time="54.766" type="appl" />
         <tli id="T87" time="55.558" type="appl" />
         <tli id="T88" time="56.67979412331951" />
         <tli id="T89" time="57.619" type="appl" />
         <tli id="T90" time="60.27" type="appl" />
         <tli id="T91" time="63.0397710221253" />
         <tli id="T92" time="63.482" type="appl" />
         <tli id="T93" time="63.714" type="appl" />
         <tli id="T94" time="63.945" type="appl" />
         <tli id="T95" time="64.177" type="appl" />
         <tli id="T96" time="65.357" type="appl" />
         <tli id="T97" time="65.719" type="appl" />
         <tli id="T98" time="66.015" type="appl" />
         <tli id="T99" time="66.31" type="appl" />
         <tli id="T100" time="66.606" type="appl" />
         <tli id="T101" time="66.76642415255762" />
         <tli id="T102" time="66.939" type="intp" />
         <tli id="T103" time="66.977" type="appl" />
         <tli id="T104" time="67.224" type="appl" />
         <tli id="T105" time="67.472" type="appl" />
         <tli id="T106" time="67.72" type="appl" />
         <tli id="T107" time="68.03308621835752" />
         <tli id="T108" time="69.042" type="appl" />
         <tli id="T109" time="70.088" type="appl" />
         <tli id="T110" time="72.879" type="appl" />
         <tli id="T111" time="73.634" type="appl" />
         <tli id="T112" time="74.388" type="appl" />
         <tli id="T113" time="75.142" type="appl" />
         <tli id="T114" time="76.336" type="appl" />
         <tli id="T115" time="77.299" type="appl" />
         <tli id="T116" time="78.276" type="appl" />
         <tli id="T117" time="78.96" type="appl" />
         <tli id="T118" time="80.66637366409859" />
         <tli id="T119" time="81.402" type="appl" />
         <tli id="T120" time="83.09303151647312" />
         <tli id="T121" time="83.725" type="appl" />
         <tli id="T122" time="84.316" type="appl" />
         <tli id="T123" time="84.908" type="appl" />
         <tli id="T124" time="85.83302156407184" />
         <tli id="T125" time="86.626" type="appl" />
         <tli id="T126" time="88.99301008612001" />
         <tli id="T127" time="89.86" type="appl" />
         <tli id="T128" time="91.28633508946297" />
         <tli id="T129" time="91.981" type="appl" />
         <tli id="T130" time="92.724" type="appl" />
         <tli id="T131" time="93.48699766896482" />
         <tli id="T132" time="94.876" type="appl" />
         <tli id="T133" time="96.81298168182146" />
         <tli id="T134" time="97.76" type="appl" />
         <tli id="T135" time="98.604" type="appl" />
         <tli id="T136" time="99.447" type="appl" />
         <tli id="T137" time="100.44" type="appl" />
         <tli id="T138" time="101.88629658747263" />
         <tli id="T139" time="103.052" type="appl" />
         <tli id="T140" time="104.134" type="appl" />
         <tli id="T141" time="105.48628351132494" />
         <tli id="T142" time="106.3" type="appl" />
         <tli id="T143" time="107.251" type="appl" />
         <tli id="T144" time="108.174" type="appl" />
         <tli id="T145" time="109.098" type="appl" />
         <tli id="T146" time="109.82626774730248" />
         <tli id="T147" time="110.566" type="appl" />
         <tli id="T148" time="111.226" type="appl" />
         <tli id="T149" time="112.094" type="appl" />
         <tli id="T150" time="112.751" type="appl" />
         <tli id="T151" time="115.1595817085652" />
         <tli id="T152" time="115.386" type="appl" />
         <tli id="T153" time="115.591" type="appl" />
         <tli id="T154" time="115.796" type="appl" />
         <tli id="T155" time="116.0" type="appl" />
         <tli id="T156" time="116.728" type="appl" />
         <tli id="T157" time="117.124" type="appl" />
         <tli id="T158" time="117.41957349965028" />
         <tli id="T159" time="117.638" type="intp" />
         <tli id="T160" time="117.755" type="appl" />
         <tli id="T161" time="118.019" type="appl" />
         <tli id="T162" time="118.283" type="appl" />
         <tli id="T163" time="118.547" type="appl" />
         <tli id="T164" time="118.812" type="appl" />
         <tli id="T165" time="119.076" type="appl" />
         <tli id="T166" time="120.066" type="appl" />
         <tli id="T167" time="120.804" type="appl" />
         <tli id="T168" time="121.541" type="appl" />
         <tli id="T169" time="123.91288324748764" />
         <tli id="T170" time="124.742" type="appl" />
         <tli id="T171" time="125.351" type="appl" />
         <tli id="T172" time="125.96" type="appl" />
         <tli id="T173" time="127.2862043279863" />
         <tli id="T174" time="128.01" type="appl" />
         <tli id="T175" time="129.5395294766198" />
         <tli id="T176" time="130.227" type="appl" />
         <tli id="T177" time="130.446" type="appl" />
         <tli id="T178" time="130.665" type="appl" />
         <tli id="T179" time="130.884" type="appl" />
         <tli id="T180" time="132.01" type="appl" />
         <tli id="T181" time="132.69" type="appl" />
         <tli id="T182" time="135.35950833684774" />
         <tli id="T183" time="135.696" type="appl" />
         <tli id="T184" time="136.198" type="appl" />
         <tli id="T185" time="136.7328366818729" />
         <tli id="T186" time="137.516" type="appl" />
         <tli id="T187" time="138.334" type="appl" />
         <tli id="T188" time="139.152" type="appl" />
         <tli id="T189" time="140.3661568179831" />
         <tli id="T190" time="141.33" type="appl" />
         <tli id="T743" time="142.34605380330106" type="intp" />
         <tli id="T191" time="144.03947680880282" />
         <tli id="T192" time="144.765" type="appl" />
         <tli id="T193" time="145.414" type="appl" />
         <tli id="T194" time="146.129" type="appl" />
         <tli id="T195" time="146.745" type="appl" />
         <tli id="T196" time="148.5194605362635" />
         <tli id="T197" time="148.897" type="appl" />
         <tli id="T198" time="149.278" type="appl" />
         <tli id="T199" time="149.66" type="appl" />
         <tli id="T200" time="150.0861215123844" />
         <tli id="T201" time="150.79" type="appl" />
         <tli id="T202" time="151.85944840450423" />
         <tli id="T203" time="153.106" type="appl" />
         <tli id="T204" time="154.97277042939137" />
         <tli id="T205" time="155.575" type="appl" />
         <tli id="T206" time="156.129" type="appl" />
         <tli id="T207" time="156.682" type="appl" />
         <tli id="T208" time="157.236" type="appl" />
         <tli id="T209" time="157.789" type="appl" />
         <tli id="T210" time="158.578" type="appl" />
         <tli id="T211" time="159.89275255865624" />
         <tli id="T212" time="160.426" type="appl" />
         <tli id="T213" time="160.994" type="appl" />
         <tli id="T214" time="161.562" type="appl" />
         <tli id="T215" time="162.129" type="appl" />
         <tli id="T216" time="162.786" type="appl" />
         <tli id="T217" time="164.17273701256957" />
         <tli id="T218" time="164.51" type="appl" />
         <tli id="T219" time="164.884" type="appl" />
         <tli id="T220" time="165.259" type="appl" />
         <tli id="T221" time="166.3993955913967" />
         <tli id="T222" time="167.843" type="appl" />
         <tli id="T223" time="169.69938360492807" />
         <tli id="T224" time="172.372707228011" />
         <tli id="T225" time="173.1" type="appl" />
         <tli id="T226" time="173.797" type="appl" />
         <tli id="T227" time="174.493" type="appl" />
         <tli id="T228" time="175.085" type="appl" />
         <tli id="T229" time="175.678" type="appl" />
         <tli id="T230" time="176.27" type="appl" />
         <tli id="T231" time="176.823" type="appl" />
         <tli id="T232" time="177.377" type="appl" />
         <tli id="T233" time="178.78601726642938" />
         <tli id="T234" time="179.595" type="appl" />
         <tli id="T235" time="180.497" type="appl" />
         <tli id="T236" time="181.399" type="appl" />
         <tli id="T237" time="182.129" type="appl" />
         <tli id="T238" time="182.86" type="appl" />
         <tli id="T239" time="184.77932883039836" />
         <tli id="T240" time="185.37" type="appl" />
         <tli id="T241" time="185.917" type="appl" />
         <tli id="T242" time="186.949" type="appl" />
         <tli id="T243" time="190.0793095794032" />
         <tli id="T244" time="191.60597070081468" />
         <tli id="T245" time="194.39929388802597" />
         <tli id="T246" time="194.862" type="appl" />
         <tli id="T247" time="195.384" type="appl" />
         <tli id="T248" time="195.906" type="appl" />
         <tli id="T249" time="196.427" type="appl" />
         <tli id="T250" time="197.394" type="appl" />
         <tli id="T251" time="198.353" type="appl" />
         <tli id="T252" time="199.725" type="appl" />
         <tli id="T253" time="200.579" type="appl" />
         <tli id="T254" time="201.473" type="appl" />
         <tli id="T255" time="202.103" type="appl" />
         <tli id="T256" time="202.734" type="appl" />
         <tli id="T257" time="205.71258612835453" />
         <tli id="T258" time="206.343" type="appl" />
         <tli id="T259" time="206.845" type="appl" />
         <tli id="T260" time="207.347" type="appl" />
         <tli id="T261" time="208.438" type="appl" />
         <tli id="T262" time="214.04588919282753" />
         <tli id="T263" time="214.553" type="appl" />
         <tli id="T264" time="215.027" type="appl" />
         <tli id="T265" time="215.1658851246927" />
         <tli id="T266" time="215.57150000000001" type="intp" />
         <tli id="T267" time="215.643" type="appl" />
         <tli id="T268" time="215.98" type="appl" />
         <tli id="T741" time="216.298" type="intp" />
         <tli id="T269" time="216.616" type="appl" />
         <tli id="T270" time="217.412" type="appl" />
         <tli id="T271" time="218.207" type="appl" />
         <tli id="T272" time="219.003" type="appl" />
         <tli id="T273" time="219.934" type="appl" />
         <tli id="T274" time="220.775" type="appl" />
         <tli id="T275" time="221.534" type="appl" />
         <tli id="T276" time="222.287" type="appl" />
         <tli id="T277" time="227.2191746771464" />
         <tli id="T278" time="227.684" type="appl" />
         <tli id="T279" time="228.92583514475052" />
         <tli id="T280" time="229.564" type="appl" />
         <tli id="T281" time="230.268" type="appl" />
         <tli id="T282" time="230.971" type="appl" />
         <tli id="T283" time="231.55" type="appl" />
         <tli id="T284" time="232.129" type="appl" />
         <tli id="T285" time="232.708" type="appl" />
         <tli id="T286" time="233.60581814575855" />
         <tli id="T287" time="234.406" type="appl" />
         <tli id="T288" time="235.263" type="appl" />
         <tli id="T289" time="236.17" type="appl" />
         <tli id="T290" time="237.89913588457506" />
         <tli id="T291" time="238.591" type="appl" />
         <tli id="T292" time="242.38578625448733" />
         <tli id="T293" time="243.435" type="appl" />
         <tli id="T294" time="244.6963247369728" />
         <tli id="T295" time="245.526" type="appl" />
         <tli id="T296" time="246.2724388037575" />
         <tli id="T297" time="246.678" type="appl" />
         <tli id="T298" time="247.206" type="appl" />
         <tli id="T299" time="247.733" type="appl" />
         <tli id="T300" time="248.73909651084148" />
         <tli id="T301" time="249.326" type="appl" />
         <tli id="T302" time="249.944" type="appl" />
         <tli id="T303" time="250.562" type="appl" />
         <tli id="T304" time="251.81241868101915" />
         <tli id="T305" time="252.515" type="appl" />
         <tli id="T306" time="253.363" type="appl" />
         <tli id="T307" time="254.29" type="appl" />
         <tli id="T308" time="255.216" type="appl" />
         <tli id="T309" time="256.9903425817015" />
         <tli id="T310" time="257.464" type="appl" />
         <tli id="T311" time="257.992" type="appl" />
         <tli id="T312" time="258.52" type="appl" />
         <tli id="T313" time="259.093" type="appl" />
         <tli id="T314" time="259.647" type="appl" />
         <tli id="T315" time="260.51905372278054" />
         <tli id="T316" time="260.858" type="appl" />
         <tli id="T317" time="261.27" type="appl" />
         <tli id="T318" time="261.683" type="appl" />
         <tli id="T319" time="262.095" type="appl" />
         <tli id="T320" time="262.9190450053487" />
         <tli id="T321" time="263.607" type="appl" />
         <tli id="T322" time="264.342" type="appl" />
         <tli id="T323" time="265.076" type="appl" />
         <tli id="T324" time="265.811" type="appl" />
         <tli id="T325" time="266.57166194636767" />
         <tli id="T326" time="266.832" type="appl" />
         <tli id="T327" time="267.14569631964946" />
         <tli id="T328" time="275.07900083702776" />
         <tli id="T329" time="275.437" type="appl" />
         <tli id="T330" time="275.786" type="appl" />
         <tli id="T331" time="276.134" type="appl" />
         <tli id="T332" time="276.483" type="appl" />
         <tli id="T333" time="276.832" type="appl" />
         <tli id="T334" time="277.181" type="appl" />
         <tli id="T335" time="277.53" type="appl" />
         <tli id="T336" time="277.878" type="appl" />
         <tli id="T337" time="278.227" type="appl" />
         <tli id="T338" time="281.49231087544615" />
         <tli id="T339" time="281.98" type="appl" />
         <tli id="T340" time="282.491" type="appl" />
         <tli id="T341" time="283.003" type="appl" />
         <tli id="T342" time="283.514" type="appl" />
         <tli id="T343" time="284.55229976072064" />
         <tli id="T344" time="285.184" type="appl" />
         <tli id="T345" time="285.9" type="appl" />
         <tli id="T346" time="286.616" type="appl" />
         <tli id="T347" time="287.332" type="appl" />
         <tli id="T348" time="287.923" type="appl" />
         <tli id="T349" time="288.463" type="appl" />
         <tli id="T350" time="289.51894838714657" />
         <tli id="T351" time="289.723" type="appl" />
         <tli id="T352" time="290.06" type="appl" />
         <tli id="T353" time="290.396" type="appl" />
         <tli id="T354" time="290.733" type="appl" />
         <tli id="T355" time="292.6122704846789" />
         <tli id="T356" time="293.275" type="appl" />
         <tli id="T357" time="293.882" type="appl" />
         <tli id="T358" time="294.488" type="appl" />
         <tli id="T359" time="295.095" type="appl" />
         <tli id="T360" time="295.879" type="appl" />
         <tli id="T361" time="296.662" type="appl" />
         <tli id="T362" time="298.96558074103314" />
         <tli id="T363" time="299.657" type="appl" />
         <tli id="T364" time="300.443" type="appl" />
         <tli id="T365" time="301.229" type="appl" />
         <tli id="T366" time="301.625" type="appl" />
         <tli id="T367" time="302.022" type="appl" />
         <tli id="T368" time="305.418890634161" />
         <tli id="T369" time="306.669" type="appl" />
         <tli id="T370" time="307.179" type="appl" />
         <tli id="T371" time="307.688" type="appl" />
         <tli id="T372" time="308.197" type="appl" />
         <tli id="T373" time="308.707" type="appl" />
         <tli id="T374" time="309.216" type="appl" />
         <tli id="T375" time="309.726" type="appl" />
         <tli id="T376" time="310.235" type="appl" />
         <tli id="T377" time="310.744" type="appl" />
         <tli id="T378" time="311.254" type="appl" />
         <tli id="T379" time="311.763" type="appl" />
         <tli id="T380" time="312.272" type="appl" />
         <tli id="T381" time="312.782" type="appl" />
         <tli id="T382" time="313.291" type="appl" />
         <tli id="T383" time="313.801" type="appl" />
         <tli id="T384" time="314.31" type="appl" />
         <tli id="T385" time="314.819" type="appl" />
         <tli id="T386" time="315.329" type="appl" />
         <tli id="T387" time="315.838" type="appl" />
         <tli id="T388" time="316.347" type="appl" />
         <tli id="T389" time="316.857" type="appl" />
         <tli id="T390" time="317.366" type="appl" />
         <tli id="T391" time="317.876" type="appl" />
         <tli id="T392" time="318.385" type="appl" />
         <tli id="T393" time="318.894" type="appl" />
         <tli id="T394" time="319.404" type="appl" />
         <tli id="T395" time="319.8863380843926" />
         <tli id="T396" time="321.109" type="appl" />
         <tli id="T397" time="321.412" type="appl" />
         <tli id="T398" time="321.669" type="appl" />
         <tli id="T399" time="321.925" type="appl" />
         <tli id="T400" time="322.182" type="appl" />
         <tli id="T401" time="322.438" type="appl" />
         <tli id="T402" time="322.694" type="appl" />
         <tli id="T403" time="322.951" type="appl" />
         <tli id="T404" time="323.207" type="appl" />
         <tli id="T405" time="323.464" type="appl" />
         <tli id="T406" time="323.72" type="appl" />
         <tli id="T407" time="323.977" type="appl" />
         <tli id="T408" time="324.233" type="appl" />
         <tli id="T409" time="324.489" type="appl" />
         <tli id="T410" time="324.746" type="appl" />
         <tli id="T411" time="325.002" type="appl" />
         <tli id="T412" time="325.259" type="appl" />
         <tli id="T413" time="325.515" type="appl" />
         <tli id="T414" time="325.771" type="appl" />
         <tli id="T415" time="326.027" type="appl" />
         <tli id="T416" time="326.282" type="appl" />
         <tli id="T417" time="326.538" type="appl" />
         <tli id="T418" time="326.8988126131466" />
         <tli id="T419" time="327.718" type="appl" />
         <tli id="T420" time="328.441" type="appl" />
         <tli id="T421" time="329.165" type="appl" />
         <tli id="T422" time="329.888" type="appl" />
         <tli id="T423" time="330.612" type="appl" />
         <tli id="T424" time="331.335" type="appl" />
         <tli id="T425" time="332.059" type="appl" />
         <tli id="T426" time="334.009" type="appl" />
         <tli id="T427" time="334.738" type="appl" />
         <tli id="T428" time="335.313" type="appl" />
         <tli id="T429" time="335.889" type="appl" />
         <tli id="T430" time="336.464" type="appl" />
         <tli id="T431" time="337.039" type="appl" />
         <tli id="T432" time="337.799" type="appl" />
         <tli id="T433" time="339.37876728250137" />
         <tli id="T434" time="340.086" type="appl" />
         <tli id="T435" time="341.5587593641675" />
         <tli id="T436" time="342.438" type="appl" />
         <tli id="T437" time="343.043" type="appl" />
         <tli id="T438" time="343.648" type="appl" />
         <tli id="T439" time="344.253" type="appl" />
         <tli id="T440" time="345.89207695769346" />
         <tli id="T441" time="346.681" type="appl" />
         <tli id="T442" time="347.089" type="appl" />
         <tli id="T443" time="347.496" type="appl" />
         <tli id="T444" time="347.903" type="appl" />
         <tli id="T445" time="348.311" type="appl" />
         <tli id="T446" time="348.6320670052922" />
         <tli id="T447" time="351.8987218065656" />
         <tli id="T448" time="352.749" type="appl" />
         <tli id="T449" time="353.618" type="appl" />
         <tli id="T450" time="354.528" type="appl" />
         <tli id="T451" time="355.245376317258" />
         <tli id="T452" time="355.986" type="appl" />
         <tli id="T453" time="356.66" type="appl" />
         <tli id="T454" time="357.365" type="appl" />
         <tli id="T455" time="358.0053662922114" />
         <tli id="T456" time="358.63" type="appl" />
         <tli id="T457" time="360.60535684832706" />
         <tli id="T458" time="361.397" type="appl" />
         <tli id="T459" time="362.149" type="appl" />
         <tli id="T460" time="363.094" type="appl" />
         <tli id="T461" time="364.039" type="appl" />
         <tli id="T462" time="367.745" type="appl" />
         <tli id="T463" time="369.167" type="appl" />
         <tli id="T464" time="372.28531442349237" />
         <tli id="T465" time="373.112" type="appl" />
         <tli id="T466" time="373.865" type="appl" />
         <tli id="T467" time="374.619" type="appl" />
         <tli id="T468" time="375.372" type="appl" />
         <tli id="T469" time="376.126" type="appl" />
         <tli id="T470" time="376.907" type="appl" />
         <tli id="T471" time="377.688" type="appl" />
         <tli id="T472" time="378.87195716565185" />
         <tli id="T473" time="379.595" type="appl" />
         <tli id="T474" time="380.356" type="appl" />
         <tli id="T475" time="381.5386141462831" />
         <tli id="T476" time="382.295" type="appl" />
         <tli id="T477" time="383.055" type="appl" />
         <tli id="T478" time="384.3719371882039" />
         <tli id="T479" time="385.03" type="appl" />
         <tli id="T480" time="385.65" type="appl" />
         <tli id="T481" time="386.27" type="appl" />
         <tli id="T482" time="387.149" type="appl" />
         <tli id="T483" time="387.82" type="appl" />
         <tli id="T484" time="388.491" type="appl" />
         <tli id="T485" time="389.1" type="appl" />
         <tli id="T486" time="389.709" type="appl" />
         <tli id="T487" time="390.318" type="appl" />
         <tli id="T488" time="390.927" type="appl" />
         <tli id="T489" time="391.536" type="appl" />
         <tli id="T490" time="392.715" type="appl" />
         <tli id="T491" time="393.894" type="appl" />
         <tli id="T492" time="395.073" type="appl" />
         <tli id="T493" time="396.252" type="appl" />
         <tli id="T494" time="398.1785537054228" />
         <tli id="T495" time="399.1985500005143" />
         <tli id="T496" time="399.767" type="appl" />
         <tli id="T497" time="400.407" type="appl" />
         <tli id="T498" time="401.047" type="appl" />
         <tli id="T499" time="401.686" type="appl" />
         <tli id="T500" time="402.326" type="appl" />
         <tli id="T501" time="402.966" type="appl" />
         <tli id="T502" time="403.606" type="appl" />
         <tli id="T503" time="406.372" type="appl" />
         <tli id="T504" time="406.798" type="appl" />
         <tli id="T505" time="409.817" type="appl" />
         <tli id="T506" time="410.273" type="appl" />
         <tli id="T507" time="410.73" type="appl" />
         <tli id="T508" time="410.7685" type="intp" />
         <tli id="T509" time="410.807" type="appl" />
         <tli id="T510" time="411.034" type="appl" />
         <tli id="T511" time="411.26" type="appl" />
         <tli id="T512" time="411.487" type="appl" />
         <tli id="T513" time="411.714" type="appl" />
         <tli id="T514" time="411.941" type="appl" />
         <tli id="T515" time="412.811" type="appl" />
         <tli id="T516" time="413.032" type="appl" />
         <tli id="T517" time="413.253" type="appl" />
         <tli id="T518" time="414.127" type="appl" />
         <tli id="T519" time="414.746" type="appl" />
         <tli id="T520" time="415.365" type="appl" />
         <tli id="T521" time="415.984" type="appl" />
         <tli id="T522" time="418.112" type="appl" />
         <tli id="T523" time="418.944" type="appl" />
         <tli id="T524" time="419.776" type="appl" />
         <tli id="T525" time="420.521" type="appl" />
         <tli id="T526" time="422.629" type="appl" />
         <tli id="T527" time="423.236" type="appl" />
         <tli id="T528" time="423.844" type="appl" />
         <tli id="T529" time="424.6317909532859" />
         <tli id="T530" time="429.59843957971174" />
         <tli id="T531" time="430.29" type="appl" />
         <tli id="T532" time="431.068" type="appl" />
         <tli id="T533" time="431.845" type="appl" />
         <tli id="T534" time="432.623" type="appl" />
         <tli id="T535" time="433.4" type="appl" />
         <tli id="T536" time="434.486" type="appl" />
         <tli id="T537" time="435.208" type="appl" />
         <tli id="T538" time="435.99174969077546" />
         <tli id="T539" time="438.004" type="appl" />
         <tli id="T540" time="438.715" type="appl" />
         <tli id="T541" time="439.686" type="appl" />
         <tli id="T542" time="440.656" type="appl" />
         <tli id="T543" time="441.627" type="appl" />
         <tli id="T544" time="443.0" type="appl" />
         <tli id="T545" time="443.709" type="appl" />
         <tli id="T546" time="444.676" type="appl" />
         <tli id="T547" time="445.597" type="appl" />
         <tli id="T548" time="447.789" type="appl" />
         <tli id="T549" time="448.247" type="appl" />
         <tli id="T550" time="448.705" type="appl" />
         <tli id="T551" time="449.163" type="appl" />
         <tli id="T552" time="449.621" type="appl" />
         <tli id="T553" time="450.681" type="appl" />
         <tli id="T554" time="452.8850216630751" />
         <tli id="T555" time="454.035" type="appl" />
         <tli id="T556" time="455.023" type="appl" />
         <tli id="T557" time="456.011" type="appl" />
         <tli id="T558" time="456.999" type="appl" />
         <tli id="T559" time="457.986" type="appl" />
         <tli id="T560" time="458.974" type="appl" />
         <tli id="T561" time="459.962" type="appl" />
         <tli id="T562" time="460.95" type="appl" />
         <tli id="T563" time="461.773" type="appl" />
         <tli id="T564" time="462.596" type="appl" />
         <tli id="T565" time="463.419" type="appl" />
         <tli id="T566" time="464.242" type="appl" />
         <tli id="T567" time="465.066" type="appl" />
         <tli id="T568" time="465.889" type="appl" />
         <tli id="T569" time="466.712" type="appl" />
         <tli id="T570" time="467.535" type="appl" />
         <tli id="T571" time="470.08495918814737" />
         <tli id="T572" time="470.45" type="appl" />
         <tli id="T573" time="471.7116199463325" />
         <tli id="T574" time="472.083" type="appl" />
         <tli id="T575" time="472.569" type="appl" />
         <tli id="T576" time="473.056" type="appl" />
         <tli id="T577" time="473.542" type="appl" />
         <tli id="T578" time="474.029" type="appl" />
         <tli id="T579" time="474.516" type="appl" />
         <tli id="T580" time="475.002" type="appl" />
         <tli id="T581" time="475.489" type="appl" />
         <tli id="T582" time="475.975" type="appl" />
         <tli id="T583" time="476.462" type="appl" />
         <tli id="T584" time="476.948" type="appl" />
         <tli id="T585" time="478.7782609450056" />
         <tli id="T586" time="480.404" type="appl" />
         <tli id="T587" time="482.208" type="appl" />
         <tli id="T588" time="484.013" type="appl" />
         <tli id="T589" time="485.817" type="appl" />
         <tli id="T590" time="488.77155797992157" />
         <tli id="T591" time="489.154" type="appl" />
         <tli id="T592" time="490.14488632494675" />
         <tli id="T593" time="490.598" type="appl" />
         <tli id="T594" time="491.036" type="appl" />
         <tli id="T595" time="491.89154664726027" />
         <tli id="T596" time="492.729" type="appl" />
         <tli id="T597" time="493.516" type="appl" />
         <tli id="T598" time="494.3582043543443" />
         <tli id="T599" time="494.995" type="appl" />
         <tli id="T600" time="495.688" type="appl" />
         <tli id="T601" time="496.38" type="appl" />
         <tli id="T602" time="497.168" type="appl" />
         <tli id="T603" time="499.2848531260608" />
         <tli id="T604" time="500.062" type="appl" />
         <tli id="T605" time="502.7981736980426" />
         <tli id="T606" time="503.77" type="appl" />
         <tli id="T607" time="504.826" type="appl" />
         <tli id="T608" time="507.59148962072743" />
         <tli id="T609" time="508.141" type="appl" />
         <tli id="T610" time="508.662" type="appl" />
         <tli id="T611" time="510.6914783607114" />
         <tli id="T612" time="511.978140353866" />
         <tli id="T613" time="512.37" type="appl" />
         <tli id="T614" time="513.2848022743754" />
         <tli id="T615" time="513.93" type="appl" />
         <tli id="T616" time="515.124795591011" />
         <tli id="T617" time="515.79" type="appl" />
         <tli id="T618" time="516.514" type="appl" />
         <tli id="T619" time="517.234" type="appl" />
         <tli id="T620" time="520.2447769938232" />
         <tli id="T621" time="520.724775250337" />
         <tli id="T622" time="521.486" type="appl" />
         <tli id="T623" time="522.388" type="appl" />
         <tli id="T624" time="523.29" type="appl" />
         <tli id="T625" time="525.301" type="appl" />
         <tli id="T626" time="527.2447515679805" />
         <tli id="T627" time="527.738" type="appl" />
         <tli id="T628" time="528.367" type="appl" />
         <tli id="T629" time="529.6780760628067" />
         <tli id="T630" time="530.28" type="appl" />
         <tli id="T631" time="530.909" type="appl" />
         <tli id="T632" time="531.704" type="appl" />
         <tli id="T633" time="532.238" type="appl" />
         <tli id="T634" time="532.8913977244674" />
         <tli id="T635" time="533.678" type="appl" />
         <tli id="T636" time="536.331385229482" />
         <tli id="T637" time="537.218" type="appl" />
         <tli id="T638" time="538.9313757855975" />
         <tli id="T639" time="539.942" type="appl" />
         <tli id="T640" time="541.063" type="appl" />
         <tli id="T641" time="542.4713629273856" />
         <tli id="T642" time="543.9046910544749" />
         <tli id="T643" time="544.702" type="appl" />
         <tli id="T644" time="545.9846834993674" />
         <tli id="T645" time="546.844" type="appl" />
         <tli id="T646" time="548.7980066139335" />
         <tli id="T647" time="549.612" type="appl" />
         <tli id="T648" time="553.0779910678469" />
         <tli id="T649" time="554.026" type="appl" />
         <tli id="T650" time="554.729" type="appl" />
         <tli id="T651" time="556.1513132380245" />
         <tli id="T652" time="556.57" type="appl" />
         <tli id="T653" time="557.128" type="appl" />
         <tli id="T654" time="558.5046380234317" />
         <tli id="T655" time="559.255" type="appl" />
         <tli id="T656" time="560.06" type="appl" />
         <tli id="T657" time="560.864" type="appl" />
         <tli id="T658" time="563.4246201526965" />
         <tli id="T659" time="564.032" type="appl" />
         <tli id="T660" time="564.8112817826247" />
         <tli id="T661" time="570.6912604249169" />
         <tli id="T662" time="571.428" type="appl" />
         <tli id="T663" time="572.254" type="appl" />
         <tli id="T664" time="573.08" type="appl" />
         <tli id="T665" time="573.906" type="appl" />
         <tli id="T666" time="575.0112447335398" />
         <tli id="T667" time="575.97" type="appl" />
         <tli id="T668" time="576.941" type="appl" />
         <tli id="T669" time="577.912" type="appl" />
         <tli id="T670" time="582.4045512123403" />
         <tli id="T671" time="583.036" type="appl" />
         <tli id="T672" time="585.6112062315494" />
         <tli id="T673" time="586.617" type="appl" />
         <tli id="T674" time="587.707" type="appl" />
         <tli id="T675" time="588.798" type="appl" />
         <tli id="T676" time="589.889" type="appl" />
         <tli id="T677" time="590.979" type="appl" />
         <tli id="T678" time="591.8111837115173" />
         <tli id="T679" time="593.237845196155" />
         <tli id="T680" time="593.561" type="appl" />
         <tli id="T681" time="594.003" type="appl" />
         <tli id="T682" time="595.6178365513686" />
         <tli id="T683" time="596.149" type="appl" />
         <tli id="T684" time="596.838" type="appl" />
         <tli id="T685" time="597.528" type="appl" />
         <tli id="T686" time="598.0111611914853" />
         <tli id="T687" time="598.653" type="appl" />
         <tli id="T688" time="599.8244879383145" />
         <tli id="T689" time="600.381" type="appl" />
         <tli id="T690" time="601.011" type="appl" />
         <tli id="T691" time="601.642" type="appl" />
         <tli id="T692" time="602.667" type="appl" />
         <tli id="T693" time="603.693" type="appl" />
         <tli id="T694" time="605.8311327871867" />
         <tli id="T695" time="606.349" type="appl" />
         <tli id="T696" time="606.91" type="appl" />
         <tli id="T697" time="608.1111245056265" />
         <tli id="T698" time="608.788" type="appl" />
         <tli id="T699" time="612.0511101945093" />
         <tli id="T700" time="612.898" type="appl" />
         <tli id="T701" time="614.0311030026281" />
         <tli id="T702" time="614.864" type="appl" />
         <tli id="T703" time="616.65776012855" />
         <tli id="T704" time="616.905" type="appl" />
         <tli id="T705" time="617.3577575859657" />
         <tli id="T706" time="617.731089563254" />
         <tli id="T707" time="617.9110889094467" />
         <tli id="T708" time="618.3577539537025" />
         <tli id="T709" time="619.005" type="appl" />
         <tli id="T710" time="619.7870195955478" />
         <tli id="T711" time="620.552" type="appl" />
         <tli id="T712" time="621.277" type="appl" />
         <tli id="T713" time="622.002" type="appl" />
         <tli id="T714" time="623.6577347027072" />
         <tli id="T715" time="626.317725040887" />
         <tli id="T716" time="627.136" type="appl" />
         <tli id="T717" time="628.5310503348111" />
         <tli id="T718" time="629.314" type="appl" />
         <tli id="T719" time="630.295" type="appl" />
         <tli id="T720" time="631.107" type="appl" />
         <tli id="T721" time="631.908" type="appl" />
         <tli id="T722" time="634.4843620440706" />
         <tli id="T723" time="635.015" type="appl" />
         <tli id="T724" time="635.562" type="appl" />
         <tli id="T725" time="636.744" type="appl" />
         <tli id="T726" time="637.589" type="appl" />
         <tli id="T727" time="638.435" type="appl" />
         <tli id="T728" time="639.28" type="appl" />
         <tli id="T729" time="640.126" type="appl" />
         <tli id="T730" time="640.972" type="appl" />
         <tli id="T731" time="641.817" type="appl" />
         <tli id="T732" time="642.663" type="appl" />
         <tli id="T733" time="643.508" type="appl" />
         <tli id="T734" time="645.2443229609182" />
         <tli id="T735" time="645.382" type="appl" />
         <tli id="T736" time="645.829" type="appl" />
         <tli id="T737" time="646.276" type="appl" />
         <tli id="T738" time="646.723" type="appl" />
         <tli id="T739" time="647.7643138076148" />
         <tli id="T740" time="648.124" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T2" start="T0">
            <tli id="T0.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T158" start="T155">
            <tli id="T155.tx-KA.1" />
            <tli id="T155.tx-KA.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T2" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Высоко</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <nts id="Seg_7" n="HIAT:ip">(</nts>
                  <ats e="T2" id="Seg_8" n="HIAT:non-pho" s="T0.tx-KA.1">…</ats>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip">)</nts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T158" id="Seg_13" n="sc" s="T155">
               <ts e="T158" id="Seg_15" n="HIAT:u" s="T155">
                  <ts e="T155.tx-KA.1" id="Seg_17" n="HIAT:w" s="T155">Печь</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155.tx-KA.2" id="Seg_20" n="HIAT:w" s="T155.tx-KA.1">топить</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_23" n="HIAT:w" s="T155.tx-KA.2">вроде</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T2" id="Seg_26" n="sc" s="T0">
               <ts e="T2" id="Seg_28" n="e" s="T0">Высоко ((…)). </ts>
            </ts>
            <ts e="T158" id="Seg_29" n="sc" s="T155">
               <ts e="T158" id="Seg_31" n="e" s="T155">Печь топить вроде. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T2" id="Seg_32" s="T0">PKZ_1964_SU0204.KA.001 (001)</ta>
            <ta e="T158" id="Seg_33" s="T155">PKZ_1964_SU0204.KA.002 (058)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T2" id="Seg_34" s="T0">Высоко ((…)). </ta>
            <ta e="T158" id="Seg_35" s="T155">Печь топить вроде. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T2" id="Seg_36" s="T0">RUS:ext</ta>
            <ta e="T158" id="Seg_37" s="T155">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T2" id="Seg_38" s="T0">Высоко (…).</ta>
            <ta e="T158" id="Seg_39" s="T155">Печь топить вроде. </ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T2" id="Seg_40" s="T0">High (…).</ta>
            <ta e="T158" id="Seg_41" s="T155">To fire the stove, wasn't it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T2" id="Seg_42" s="T0">Hoch (…).</ta>
            <ta e="T158" id="Seg_43" s="T155">Den Ofen heizen, sowas.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T9" start="T2">
            <tli id="T2.tx-PKZ.1" />
            <tli id="T2.tx-PKZ.2" />
            <tli id="T2.tx-PKZ.3" />
            <tli id="T2.tx-PKZ.4" />
            <tli id="T2.tx-PKZ.5" />
         </timeline-fork>
         <timeline-fork end="T64" start="T62">
            <tli id="T62.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T101" start="T96">
            <tli id="T96.tx-PKZ.1" />
            <tli id="T96.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T110" start="T109">
            <tli id="T109.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T126" start="T124">
            <tli id="T124.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T155" start="T151">
            <tli id="T151.tx-PKZ.1" />
            <tli id="T151.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T165" start="T158">
            <tli id="T158.tx-PKZ.1" />
            <tli id="T158.tx-PKZ.2" />
            <tli id="T158.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T171" start="T169">
            <tli id="T169.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T179" start="T175">
            <tli id="T175.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T261" start="T260">
            <tli id="T260.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T265" start="T262">
            <tli id="T262.tx-PKZ.1" />
            <tli id="T262.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T312" start="T309">
            <tli id="T309.tx-PKZ.1" />
            <tli id="T309.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T320" start="T315">
            <tli id="T315.tx-PKZ.1" />
            <tli id="T315.tx-PKZ.2" />
            <tli id="T315.tx-PKZ.3" />
            <tli id="T315.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T507" start="T504">
            <tli id="T504.tx-PKZ.1" />
            <tli id="T504.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T554" start="T552">
            <tli id="T552.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T595" start="T592">
            <tli id="T592.tx-PKZ.1" />
            <tli id="T592.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T627" start="T626">
            <tli id="T626.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T631" start="T629">
            <tli id="T629.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T679" start="T678">
            <tli id="T678.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T682" start="T679">
            <tli id="T679.tx-PKZ.1" />
            <tli id="T679.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T688" start="T686">
            <tli id="T686.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T696" start="T695">
            <tli id="T695.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T724" start="T722">
            <tli id="T722.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T725" start="T724">
            <tli id="T724.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T739" start="T734">
            <tli id="T734.tx-PKZ.1" />
            <tli id="T734.tx-PKZ.2" />
            <tli id="T734.tx-PKZ.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T9" id="Seg_44" n="sc" s="T2">
               <ts e="T9" id="Seg_46" n="HIAT:u" s="T2">
                  <nts id="Seg_47" n="HIAT:ip">(</nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ats e="T2.tx-PKZ.1" id="Seg_49" n="HIAT:non-pho" s="T2">…DMG</ats>
                  <nts id="Seg_50" n="HIAT:ip">)</nts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2.tx-PKZ.2" id="Seg_54" n="HIAT:w" s="T2.tx-PKZ.1">а</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2.tx-PKZ.3" id="Seg_57" n="HIAT:w" s="T2.tx-PKZ.2">чего</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2.tx-PKZ.4" id="Seg_60" n="HIAT:w" s="T2.tx-PKZ.3">это</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2.tx-PKZ.5" id="Seg_64" n="HIAT:w" s="T2.tx-PKZ.4">на</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_67" n="HIAT:w" s="T2.tx-PKZ.5">что</ts>
                  <nts id="Seg_68" n="HIAT:ip">?</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_70" n="sc" s="T14">
               <ts e="T15" id="Seg_72" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_74" n="HIAT:w" s="T14">Сюда</ts>
                  <nts id="Seg_75" n="HIAT:ip">?</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T19" id="Seg_77" n="sc" s="T17">
               <ts e="T19" id="Seg_79" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_81" n="HIAT:w" s="T17">Iʔ</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_84" n="HIAT:w" s="T18">kudonzaʔ</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_87" n="sc" s="T22">
               <ts e="T25" id="Seg_89" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_91" n="HIAT:w" s="T22">Măn</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_94" n="HIAT:w" s="T23">dĭm</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_97" n="HIAT:w" s="T24">ibiem</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_100" n="sc" s="T27">
               <ts e="T30" id="Seg_102" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">Dĭn</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">bar</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">kudonzlaʔbəʔjə</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_114" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">Măn</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_119" n="HIAT:w" s="T31">kalla</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T742">dʼürbiem</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_125" n="sc" s="T36">
               <ts e="T39" id="Seg_127" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">Iʔgö</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">pʼaŋdəbial</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">bar</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_139" n="HIAT:u" s="T39">
                  <nts id="Seg_140" n="HIAT:ip">(</nts>
                  <nts id="Seg_141" n="HIAT:ip">(</nts>
                  <ats e="T40" id="Seg_142" n="HIAT:non-pho" s="T39">BRK</ats>
                  <nts id="Seg_143" n="HIAT:ip">)</nts>
                  <nts id="Seg_144" n="HIAT:ip">)</nts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_148" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">Koŋ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">bar</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">nʼešpək</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">šobi</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_163" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">Jezerik</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">bar</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_171" n="sc" s="T47">
               <ts e="T51" id="Seg_173" n="HIAT:u" s="T47">
                  <nts id="Seg_174" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">Не</ts>
                  <nts id="Seg_177" n="HIAT:ip">)</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">забыла</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">как</ts>
                  <nts id="Seg_185" n="HIAT:ip">"</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">упал</ts>
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_192" n="sc" s="T52">
               <ts e="T54" id="Seg_194" n="HIAT:u" s="T52">
                  <ts e="T54" id="Seg_196" n="HIAT:w" s="T52">ige</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_199" n="sc" s="T56">
               <ts e="T58" id="Seg_201" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">Sabən</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">ige</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_210" n="HIAT:u" s="T58">
                  <nts id="Seg_211" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">Kĭš-</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">Kĭškit</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">bar</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T64" id="Seg_223" n="sc" s="T62">
               <ts e="T64" id="Seg_225" n="HIAT:u" s="T62">
                  <ts e="T62.tx-PKZ.1" id="Seg_227" n="HIAT:w" s="T62">Трись</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T62.tx-PKZ.1">говорю</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_234" n="sc" s="T70">
               <ts e="T76" id="Seg_236" n="HIAT:u" s="T70">
                  <nts id="Seg_237" n="HIAT:ip">(</nts>
                  <ts e="T71" id="Seg_239" n="HIAT:w" s="T70">Sp-</ts>
                  <nts id="Seg_240" n="HIAT:ip">)</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_243" n="HIAT:w" s="T71">Ugaːndə</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_246" n="HIAT:w" s="T72">tăn</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_249" n="HIAT:w" s="T73">sagər</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_253" n="HIAT:w" s="T74">kanaʔ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_256" n="HIAT:w" s="T75">moltʼanə</ts>
                  <nts id="Seg_257" n="HIAT:ip">!</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_260" n="HIAT:u" s="T76">
                  <nts id="Seg_261" n="HIAT:ip">(</nts>
                  <nts id="Seg_262" n="HIAT:ip">(</nts>
                  <ats e="T77" id="Seg_263" n="HIAT:non-pho" s="T76">BRK</ats>
                  <nts id="Seg_264" n="HIAT:ip">)</nts>
                  <nts id="Seg_265" n="HIAT:ip">)</nts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_269" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">Dĭn</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_274" n="HIAT:w" s="T78">dʼibige</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_276" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_278" n="HIAT:w" s="T79">ige</ts>
                  <nts id="Seg_279" n="HIAT:ip">)</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_282" n="HIAT:w" s="T80">i</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_285" n="HIAT:w" s="T81">šĭšəge</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_288" n="HIAT:w" s="T82">bü</ts>
                  <nts id="Seg_289" n="HIAT:ip">,</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_292" n="HIAT:w" s="T83">sabən</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_295" n="HIAT:w" s="T84">ige</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_299" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_301" n="HIAT:w" s="T85">Păzaʔ</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_303" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_305" n="HIAT:w" s="T86">хорошенько</ts>
                  <nts id="Seg_306" n="HIAT:ip">)</nts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_310" n="HIAT:w" s="T87">kĭškit</ts>
                  <nts id="Seg_311" n="HIAT:ip">!</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_314" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_316" n="HIAT:w" s="T88">Трись</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_320" n="HIAT:u" s="T89">
                  <ts e="T91" id="Seg_322" n="HIAT:w" s="T89">to</ts>
                  <nts id="Seg_323" n="HIAT:ip">.</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T101" id="Seg_325" n="sc" s="T96">
               <ts e="T101" id="Seg_327" n="HIAT:u" s="T96">
                  <ts e="T96.tx-PKZ.1" id="Seg_329" n="HIAT:w" s="T96">Али</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx-PKZ.2" id="Seg_332" n="HIAT:w" s="T96.tx-PKZ.1">по-новому</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_335" n="HIAT:w" s="T96.tx-PKZ.2">говорю</ts>
                  <nts id="Seg_336" n="HIAT:ip">?</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_338" n="sc" s="T107">
               <ts e="T109" id="Seg_340" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_342" n="HIAT:w" s="T107">Tüžöjəʔi</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_345" n="HIAT:w" s="T108">šedendə</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_349" n="HIAT:u" s="T109">
                  <nts id="Seg_350" n="HIAT:ip">(</nts>
                  <nts id="Seg_351" n="HIAT:ip">(</nts>
                  <ats e="T109.tx-PKZ.1" id="Seg_352" n="HIAT:non-pho" s="T109">BRK</ats>
                  <nts id="Seg_353" n="HIAT:ip">)</nts>
                  <nts id="Seg_354" n="HIAT:ip">)</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_357" n="HIAT:w" s="T109.tx-PKZ.1">Нет</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_360" n="HIAT:ip">(</nts>
                  <nts id="Seg_361" n="HIAT:ip">(</nts>
                  <ats e="T111" id="Seg_362" n="HIAT:non-pho" s="T110">…</ats>
                  <nts id="Seg_363" n="HIAT:ip">)</nts>
                  <nts id="Seg_364" n="HIAT:ip">)</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_367" n="HIAT:w" s="T111">nuzaŋ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_370" n="HIAT:w" s="T112">šobiʔi</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_374" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_376" n="HIAT:w" s="T113">Amnaʔ</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_380" n="HIAT:w" s="T114">amoraʔ</ts>
                  <nts id="Seg_381" n="HIAT:ip">!</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_384" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_386" n="HIAT:w" s="T115">Padʼi</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_389" n="HIAT:w" s="T116">amorzittə</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_392" n="HIAT:w" s="T117">axota</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_396" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_398" n="HIAT:w" s="T118">Gijen</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_401" n="HIAT:w" s="T119">ibiel</ts>
                  <nts id="Seg_402" n="HIAT:ip">?</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_405" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_407" n="HIAT:w" s="T120">Amnaʔ</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_410" n="HIAT:w" s="T121">döbər</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_414" n="HIAT:w" s="T122">kuvas</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_417" n="HIAT:w" s="T123">kuza</ts>
                  <nts id="Seg_418" n="HIAT:ip">!</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_421" n="HIAT:u" s="T124">
                  <ts e="T124.tx-PKZ.1" id="Seg_423" n="HIAT:w" s="T124">Ну</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_426" n="HIAT:w" s="T124.tx-PKZ.1">хватит</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_430" n="HIAT:u" s="T126">
                  <nts id="Seg_431" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_433" n="HIAT:w" s="T126">-tədada</ts>
                  <nts id="Seg_434" n="HIAT:ip">)</nts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_438" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_440" n="HIAT:w" s="T128">Nada</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_443" n="HIAT:w" s="T129">tura</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_445" n="HIAT:ip">(</nts>
                  <ts e="T131" id="Seg_447" n="HIAT:w" s="T130">băzəjsʼtə</ts>
                  <nts id="Seg_448" n="HIAT:ip">)</nts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_452" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_454" n="HIAT:w" s="T131">Nada</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_457" n="HIAT:w" s="T132">săbərəjzittə</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_461" n="HIAT:u" s="T133">
                  <nts id="Seg_462" n="HIAT:ip">(</nts>
                  <ts e="T134" id="Seg_464" n="HIAT:w" s="T133">Nʼiʔdə=</ts>
                  <nts id="Seg_465" n="HIAT:ip">)</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_468" n="HIAT:w" s="T134">Nʼiʔdə</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_471" n="HIAT:w" s="T135">kanzittə</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_475" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_477" n="HIAT:w" s="T136">Tüžöj</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_480" n="HIAT:w" s="T137">surdəsʼtə</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_484" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_486" n="HIAT:w" s="T138">Eššim</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_489" n="HIAT:w" s="T139">băzəjsʼtə</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_493" n="HIAT:u" s="T140">
                  <nts id="Seg_494" n="HIAT:ip">(</nts>
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <ats e="T141" id="Seg_496" n="HIAT:non-pho" s="T140">BRK</ats>
                  <nts id="Seg_497" n="HIAT:ip">)</nts>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_502" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_504" n="HIAT:w" s="T141">Ipek</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_507" n="HIAT:w" s="T142">pürzittə</ts>
                  <nts id="Seg_508" n="HIAT:ip">.</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_511" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_513" n="HIAT:w" s="T143">Ipek</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_516" n="HIAT:w" s="T144">nuldəsʼtə</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_520" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_522" n="HIAT:w" s="T145">Хватит</ts>
                  <nts id="Seg_523" n="HIAT:ip">?</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_526" n="HIAT:u" s="T146">
                  <ts e="T148" id="Seg_528" n="HIAT:w" s="T146">pürzittə</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_532" n="HIAT:u" s="T148">
                  <nts id="Seg_533" n="HIAT:ip">(</nts>
                  <ts e="T149" id="Seg_535" n="HIAT:w" s="T148">Sʼel</ts>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_539" n="HIAT:w" s="T149">pürzittə</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_542" n="HIAT:w" s="T150">nada</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_546" n="HIAT:u" s="T151">
                  <ts e="T151.tx-PKZ.1" id="Seg_548" n="HIAT:w" s="T151">Еще</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx-PKZ.2" id="Seg_551" n="HIAT:w" s="T151.tx-PKZ.1">чего-</ts>
                  <nts id="Seg_552" n="HIAT:ip">(</nts>
                  <ts e="T155" id="Seg_554" n="HIAT:w" s="T151.tx-PKZ.2">нибудь</ts>
                  <nts id="Seg_555" n="HIAT:ip">)</nts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_558" n="sc" s="T158">
               <ts e="T165" id="Seg_560" n="HIAT:u" s="T158">
                  <nts id="Seg_561" n="HIAT:ip">(</nts>
                  <ts e="T158.tx-PKZ.1" id="Seg_563" n="HIAT:w" s="T158">Ча-</ts>
                  <nts id="Seg_564" n="HIAT:ip">)</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158.tx-PKZ.2" id="Seg_567" n="HIAT:w" s="T158.tx-PKZ.1">Чай</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158.tx-PKZ.3" id="Seg_570" n="HIAT:w" s="T158.tx-PKZ.2">варить</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_573" n="HIAT:w" s="T158.tx-PKZ.3">как-то</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_577" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_579" n="HIAT:w" s="T165">Segi</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_582" n="HIAT:w" s="T166">bü</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_585" n="HIAT:w" s="T167">nada</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_588" n="HIAT:w" s="T168">mĭnzərzittə</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_592" n="HIAT:u" s="T169">
                  <nts id="Seg_593" n="HIAT:ip">(</nts>
                  <nts id="Seg_594" n="HIAT:ip">(</nts>
                  <ats e="T169.tx-PKZ.1" id="Seg_595" n="HIAT:non-pho" s="T169">DMG</ats>
                  <nts id="Seg_596" n="HIAT:ip">)</nts>
                  <nts id="Seg_597" n="HIAT:ip">)</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_600" n="HIAT:w" s="T169.tx-PKZ.1">kirgarlaʔbə</ts>
                  <nts id="Seg_601" n="HIAT:ip">,</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_604" n="HIAT:w" s="T171">i</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_607" n="HIAT:w" s="T172">kuromaʔ</ts>
                  <nts id="Seg_608" n="HIAT:ip">!</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_611" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_613" n="HIAT:w" s="T173">I</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_616" n="HIAT:w" s="T174">alomaʔ</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_620" n="HIAT:u" s="T175">
                  <ts e="T175.tx-PKZ.1" id="Seg_622" n="HIAT:w" s="T175">Еще</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_625" n="HIAT:w" s="T175.tx-PKZ.1">как-то</ts>
                  <nts id="Seg_626" n="HIAT:ip">?</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_629" n="HIAT:u" s="T179">
                  <ts e="T181" id="Seg_631" n="HIAT:w" s="T179">Господи</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_635" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_637" n="HIAT:w" s="T181">Забыла</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_641" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_643" n="HIAT:w" s="T182">Măn</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_646" n="HIAT:w" s="T183">dĭn</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_649" n="HIAT:w" s="T184">ibiem</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_653" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_655" n="HIAT:w" s="T185">Dĭn</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_658" n="HIAT:w" s="T186">bar</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_660" n="HIAT:ip">(</nts>
                  <ts e="T188" id="Seg_662" n="HIAT:w" s="T187">kudonz-</ts>
                  <nts id="Seg_663" n="HIAT:ip">)</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_666" n="HIAT:w" s="T188">kudonzlaʔbəʔjə</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_670" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_672" n="HIAT:w" s="T189">Măn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_675" n="HIAT:w" s="T190">kalla</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_678" n="HIAT:w" s="T743">dʼürbiem</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_682" n="HIAT:u" s="T191">
                  <nts id="Seg_683" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_685" n="HIAT:w" s="T191">-ndəbial</ts>
                  <nts id="Seg_686" n="HIAT:ip">)</nts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_690" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_692" n="HIAT:w" s="T193">Udal</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_695" n="HIAT:w" s="T194">padʼi</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_698" n="HIAT:w" s="T195">ĭzemnie</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_702" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_704" n="HIAT:w" s="T196">Ну</ts>
                  <nts id="Seg_705" n="HIAT:ip">,</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_708" n="HIAT:w" s="T197">все</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_711" n="HIAT:w" s="T198">наверное</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_715" n="HIAT:u" s="T199">
                  <nts id="Seg_716" n="HIAT:ip">(</nts>
                  <nts id="Seg_717" n="HIAT:ip">(</nts>
                  <ats e="T200" id="Seg_718" n="HIAT:non-pho" s="T199">BRK</ats>
                  <nts id="Seg_719" n="HIAT:ip">)</nts>
                  <nts id="Seg_720" n="HIAT:ip">)</nts>
                  <nts id="Seg_721" n="HIAT:ip">.</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_724" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_726" n="HIAT:w" s="T200">Tus</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_729" n="HIAT:w" s="T201">naga</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_733" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_735" n="HIAT:w" s="T202">Nada</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_738" n="HIAT:w" s="T203">tustʼarzittə</ts>
                  <nts id="Seg_739" n="HIAT:ip">.</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_742" n="HIAT:u" s="T204">
                  <ts e="T206" id="Seg_744" n="HIAT:w" s="T204">ĭmbizʼiʔ</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_747" n="HIAT:w" s="T206">amzittə</ts>
                  <nts id="Seg_748" n="HIAT:ip">,</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_751" n="HIAT:w" s="T207">šamnak</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_754" n="HIAT:w" s="T208">naga</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_758" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_760" n="HIAT:w" s="T209">Deʔ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_763" n="HIAT:w" s="T210">šamnak</ts>
                  <nts id="Seg_764" n="HIAT:ip">!</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_767" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_769" n="HIAT:w" s="T211">Dö</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_772" n="HIAT:w" s="T212">šamnak</ts>
                  <nts id="Seg_773" n="HIAT:ip">,</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_776" n="HIAT:w" s="T213">mĭbiem</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_779" n="HIAT:w" s="T214">tănan</ts>
                  <nts id="Seg_780" n="HIAT:ip">!</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_783" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_785" n="HIAT:w" s="T215">Büžü</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_788" n="HIAT:w" s="T216">amaʔ</ts>
                  <nts id="Seg_789" n="HIAT:ip">!</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_792" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_794" n="HIAT:w" s="T217">Все</ts>
                  <nts id="Seg_795" n="HIAT:ip">,</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_798" n="HIAT:w" s="T218">скорее</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_801" n="HIAT:w" s="T219">ешь</ts>
                  <nts id="Seg_802" n="HIAT:ip">.</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_805" n="HIAT:u" s="T220">
                  <nts id="Seg_806" n="HIAT:ip">(</nts>
                  <nts id="Seg_807" n="HIAT:ip">(</nts>
                  <ats e="T221" id="Seg_808" n="HIAT:non-pho" s="T220">BRK</ats>
                  <nts id="Seg_809" n="HIAT:ip">)</nts>
                  <nts id="Seg_810" n="HIAT:ip">)</nts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_814" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_816" n="HIAT:w" s="T221">Kanžəbəj</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_818" n="HIAT:ip">(</nts>
                  <nts id="Seg_819" n="HIAT:ip">(</nts>
                  <ats e="T223" id="Seg_820" n="HIAT:non-pho" s="T222">…</ats>
                  <nts id="Seg_821" n="HIAT:ip">)</nts>
                  <nts id="Seg_822" n="HIAT:ip">)</nts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_826" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_828" n="HIAT:w" s="T223">Obberəj</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_832" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_834" n="HIAT:w" s="T224">Ugaːndə</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_837" n="HIAT:w" s="T225">kuvas</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_840" n="HIAT:w" s="T226">koʔbsaŋ</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_844" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_846" n="HIAT:w" s="T227">Ugaːndə</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_849" n="HIAT:w" s="T228">kuvas</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_852" n="HIAT:w" s="T229">nʼizeŋ</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_856" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_858" n="HIAT:w" s="T230">Ugaːndə</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_861" n="HIAT:w" s="T231">kuvas</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_864" n="HIAT:w" s="T232">il</ts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_868" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_870" n="HIAT:w" s="T233">Ugaːndə</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_873" n="HIAT:w" s="T234">jakšə</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_876" n="HIAT:w" s="T235">kuza</ts>
                  <nts id="Seg_877" n="HIAT:ip">.</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_880" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_882" n="HIAT:w" s="T236">Ugaːndə</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_885" n="HIAT:w" s="T237">šaːmnaʔbə</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_888" n="HIAT:w" s="T238">kuza</ts>
                  <nts id="Seg_889" n="HIAT:ip">.</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_892" n="HIAT:u" s="T239">
                  <nts id="Seg_893" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_895" n="HIAT:w" s="T239">-luʔpial</ts>
                  <nts id="Seg_896" n="HIAT:ip">)</nts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_900" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_902" n="HIAT:w" s="T241">Nada</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_905" n="HIAT:w" s="T242">kunolzittə</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T265" id="Seg_908" n="sc" s="T244">
               <ts e="T245" id="Seg_910" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_912" n="HIAT:w" s="T244">Idʼiʔeʔe</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_916" n="HIAT:u" s="T245">
                  <nts id="Seg_917" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_919" n="HIAT:w" s="T245">-tə</ts>
                  <nts id="Seg_920" n="HIAT:ip">)</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_923" n="HIAT:w" s="T247">axota</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_926" n="HIAT:w" s="T248">ugaːndə</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_930" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_932" n="HIAT:w" s="T249">Nanəm</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_935" n="HIAT:w" s="T250">ĭzemnie</ts>
                  <nts id="Seg_936" n="HIAT:ip">.</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_939" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_941" n="HIAT:w" s="T251">Amoraʔ</ts>
                  <nts id="Seg_942" n="HIAT:ip">,</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_945" n="HIAT:w" s="T252">amnaʔ</ts>
                  <nts id="Seg_946" n="HIAT:ip">!</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_949" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_951" n="HIAT:w" s="T253">Iʔ</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_954" n="HIAT:w" s="T254">kürümaʔ</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_958" n="HIAT:w" s="T255">iʔ</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_961" n="HIAT:w" s="T256">alomaʔ</ts>
                  <nts id="Seg_962" n="HIAT:ip">!</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_965" n="HIAT:u" s="T257">
                  <ts e="T259" id="Seg_967" n="HIAT:w" s="T257">nada</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_970" n="HIAT:w" s="T259">izittə</ts>
                  <nts id="Seg_971" n="HIAT:ip">.</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_974" n="HIAT:u" s="T260">
                  <ts e="T260.tx-PKZ.1" id="Seg_976" n="HIAT:w" s="T260">A</ts>
                  <nts id="Seg_977" n="HIAT:ip">_</nts>
                  <ts e="T261" id="Seg_979" n="HIAT:w" s="T260.tx-PKZ.1">to</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_981" n="HIAT:ip">(</nts>
                  <nts id="Seg_982" n="HIAT:ip">(</nts>
                  <ats e="T262" id="Seg_983" n="HIAT:non-pho" s="T261">DMG</ats>
                  <nts id="Seg_984" n="HIAT:ip">)</nts>
                  <nts id="Seg_985" n="HIAT:ip">)</nts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_989" n="HIAT:u" s="T262">
                  <ts e="T262.tx-PKZ.1" id="Seg_991" n="HIAT:w" s="T262">И</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262.tx-PKZ.2" id="Seg_994" n="HIAT:w" s="T262.tx-PKZ.1">еще</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_997" n="HIAT:w" s="T262.tx-PKZ.2">чего</ts>
                  <nts id="Seg_998" n="HIAT:ip">?</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T312" id="Seg_1000" n="sc" s="T268">
               <ts e="T272" id="Seg_1002" n="HIAT:u" s="T268">
                  <nts id="Seg_1003" n="HIAT:ip">(</nts>
                  <nts id="Seg_1004" n="HIAT:ip">(</nts>
                  <ats e="T741" id="Seg_1005" n="HIAT:non-pho" s="T268">DMG</ats>
                  <nts id="Seg_1006" n="HIAT:ip">)</nts>
                  <nts id="Seg_1007" n="HIAT:ip">)</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1010" n="HIAT:w" s="T741">Dʼijenə</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1013" n="HIAT:w" s="T269">kanzittə</ts>
                  <nts id="Seg_1014" n="HIAT:ip">,</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1017" n="HIAT:w" s="T270">keʔbde</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1020" n="HIAT:w" s="T271">deʔsʼittə</ts>
                  <nts id="Seg_1021" n="HIAT:ip">.</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1024" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1026" n="HIAT:w" s="T272">Sanə</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1029" n="HIAT:w" s="T273">deʔsittə</ts>
                  <nts id="Seg_1030" n="HIAT:ip">.</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1033" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1035" n="HIAT:w" s="T274">Büžü</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1038" n="HIAT:w" s="T275">kanžəbəj</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1041" n="HIAT:w" s="T276">šidegöʔ</ts>
                  <nts id="Seg_1042" n="HIAT:ip">.</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1045" n="HIAT:u" s="T277">
                  <nts id="Seg_1046" n="HIAT:ip">(</nts>
                  <ts e="T279" id="Seg_1048" n="HIAT:w" s="T277">-ləm</ts>
                  <nts id="Seg_1049" n="HIAT:ip">)</nts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1053" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1055" n="HIAT:w" s="T279">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1058" n="HIAT:w" s="T280">kutlam</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1060" n="HIAT:ip">(</nts>
                  <ts e="T282" id="Seg_1062" n="HIAT:w" s="T281">dĭm</ts>
                  <nts id="Seg_1063" n="HIAT:ip">)</nts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T285" id="Seg_1067" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1069" n="HIAT:w" s="T282">Poʔto</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1072" n="HIAT:w" s="T283">kuʔpiom</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1075" n="HIAT:w" s="T284">bar</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1079" n="HIAT:u" s="T285">
                  <nts id="Seg_1080" n="HIAT:ip">(</nts>
                  <nts id="Seg_1081" n="HIAT:ip">(</nts>
                  <ats e="T286" id="Seg_1082" n="HIAT:non-pho" s="T285">BRK</ats>
                  <nts id="Seg_1083" n="HIAT:ip">)</nts>
                  <nts id="Seg_1084" n="HIAT:ip">)</nts>
                  <nts id="Seg_1085" n="HIAT:ip">.</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1088" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1090" n="HIAT:w" s="T286">Albuga</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1093" n="HIAT:w" s="T287">kuʔpiom</ts>
                  <nts id="Seg_1094" n="HIAT:ip">.</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1097" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1099" n="HIAT:w" s="T288">Tažəp</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1102" n="HIAT:w" s="T289">kuʔpiom</ts>
                  <nts id="Seg_1103" n="HIAT:ip">.</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1106" n="HIAT:u" s="T290">
                  <nts id="Seg_1107" n="HIAT:ip">(</nts>
                  <ts e="T291" id="Seg_1109" n="HIAT:w" s="T290">Sön</ts>
                  <nts id="Seg_1110" n="HIAT:ip">)</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1113" n="HIAT:w" s="T291">kuʔpiam</ts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1117" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1119" n="HIAT:w" s="T292">Urgaːba</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1122" n="HIAT:w" s="T293">kuʔpiam</ts>
                  <nts id="Seg_1123" n="HIAT:ip">.</nts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1126" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1128" n="HIAT:w" s="T294">Все</ts>
                  <nts id="Seg_1129" n="HIAT:ip">,</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1132" n="HIAT:w" s="T295">хватит</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1136" n="HIAT:u" s="T296">
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1138" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1140" n="HIAT:w" s="T296">-m</ts>
                  <nts id="Seg_1141" n="HIAT:ip">)</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1144" n="HIAT:w" s="T298">ĭzemnie</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1147" n="HIAT:w" s="T299">ugaːndə</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1151" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1153" n="HIAT:w" s="T300">Udam</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1156" n="HIAT:w" s="T301">ĭzemnie</ts>
                  <nts id="Seg_1157" n="HIAT:ip">,</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1160" n="HIAT:w" s="T302">ulum</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1163" n="HIAT:w" s="T303">ĭzemnie</ts>
                  <nts id="Seg_1164" n="HIAT:ip">.</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1167" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1169" n="HIAT:w" s="T304">Bögəlbə</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1172" n="HIAT:w" s="T305">ĭzemnie</ts>
                  <nts id="Seg_1173" n="HIAT:ip">.</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1176" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1178" n="HIAT:w" s="T306">Üjüzeŋdə</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1181" n="HIAT:w" s="T307">ĭzemnie</ts>
                  <nts id="Seg_1182" n="HIAT:ip">.</nts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1185" n="HIAT:u" s="T308">
                  <nts id="Seg_1186" n="HIAT:ip">(</nts>
                  <nts id="Seg_1187" n="HIAT:ip">(</nts>
                  <ats e="T309" id="Seg_1188" n="HIAT:non-pho" s="T308">BRK</ats>
                  <nts id="Seg_1189" n="HIAT:ip">)</nts>
                  <nts id="Seg_1190" n="HIAT:ip">)</nts>
                  <nts id="Seg_1191" n="HIAT:ip">.</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1194" n="HIAT:u" s="T309">
                  <ts e="T309.tx-PKZ.1" id="Seg_1196" n="HIAT:w" s="T309">Ладно</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309.tx-PKZ.2" id="Seg_1199" n="HIAT:w" s="T309.tx-PKZ.1">уже</ts>
                  <nts id="Seg_1200" n="HIAT:ip">,</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1203" n="HIAT:w" s="T309.tx-PKZ.2">хватит</ts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_1206" n="sc" s="T315">
               <ts e="T320" id="Seg_1208" n="HIAT:u" s="T315">
                  <ts e="T315.tx-PKZ.1" id="Seg_1210" n="HIAT:w" s="T315">Так</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315.tx-PKZ.2" id="Seg_1213" n="HIAT:w" s="T315.tx-PKZ.1">я</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315.tx-PKZ.3" id="Seg_1216" n="HIAT:w" s="T315.tx-PKZ.2">уже</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315.tx-PKZ.4" id="Seg_1219" n="HIAT:w" s="T315.tx-PKZ.3">позабыла</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1222" n="HIAT:w" s="T315.tx-PKZ.4">опять</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1226" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1228" n="HIAT:w" s="T320">Tüʔsittə</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1231" n="HIAT:w" s="T321">kanzittə</ts>
                  <nts id="Seg_1232" n="HIAT:ip">,</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1235" n="HIAT:w" s="T322">kĭnzəsʼtə</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1238" n="HIAT:w" s="T323">nada</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1241" n="HIAT:w" s="T324">kanzittə</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1245" n="HIAT:u" s="T325">
                  <ts e="T327" id="Seg_1247" n="HIAT:w" s="T325">Nada</ts>
                  <nts id="Seg_1248" n="HIAT:ip">…</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1251" n="HIAT:u" s="T327">
                  <nts id="Seg_1252" n="HIAT:ip">(</nts>
                  <nts id="Seg_1253" n="HIAT:ip">(</nts>
                  <ats e="T328" id="Seg_1254" n="HIAT:non-pho" s="T327">BRK</ats>
                  <nts id="Seg_1255" n="HIAT:ip">)</nts>
                  <nts id="Seg_1256" n="HIAT:ip">)</nts>
                  <nts id="Seg_1257" n="HIAT:ip">.</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1260" n="HIAT:u" s="T328">
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1263" n="HIAT:w" s="T328">Старик</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1266" n="HIAT:w" s="T330">со</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1269" n="HIAT:w" s="T331">старухой</ts>
                  <nts id="Seg_1270" n="HIAT:ip">,</nts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1273" n="HIAT:w" s="T332">у</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1276" n="HIAT:w" s="T333">них</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1279" n="HIAT:w" s="T334">были</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1282" n="HIAT:w" s="T335">мальчик</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1285" n="HIAT:w" s="T336">и</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1288" n="HIAT:w" s="T337">девочка</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1292" n="HIAT:u" s="T338">
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1294" n="HIAT:ip">(</nts>
                  <ts e="T340" id="Seg_1296" n="HIAT:w" s="T338">-ʔi</ts>
                  <nts id="Seg_1297" n="HIAT:ip">)</nts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1300" n="HIAT:w" s="T340">nüke</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1303" n="HIAT:w" s="T341">i</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1306" n="HIAT:w" s="T342">büzʼe</ts>
                  <nts id="Seg_1307" n="HIAT:ip">.</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1310" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1312" n="HIAT:w" s="T343">Dĭzeŋ</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1314" n="HIAT:ip">(</nts>
                  <ts e="T345" id="Seg_1316" n="HIAT:w" s="T344">eks-</ts>
                  <nts id="Seg_1317" n="HIAT:ip">)</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1320" n="HIAT:w" s="T345">esseŋdə</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1323" n="HIAT:w" s="T346">ibiʔi</ts>
                  <nts id="Seg_1324" n="HIAT:ip">.</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1327" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1329" n="HIAT:w" s="T347">Nʼi</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1332" n="HIAT:w" s="T348">i</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1335" n="HIAT:w" s="T349">koʔbdo</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T396" id="Seg_1338" n="sc" s="T355">
               <ts e="T359" id="Seg_1340" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1342" n="HIAT:w" s="T355">Amnobiʔi</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1345" n="HIAT:w" s="T356">nüke</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1348" n="HIAT:w" s="T357">i</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1351" n="HIAT:w" s="T358">büzʼe</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1355" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1357" n="HIAT:w" s="T359">Dĭzeŋdə</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1360" n="HIAT:w" s="T360">esseŋdə</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1363" n="HIAT:w" s="T361">ibiʔi</ts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1367" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1369" n="HIAT:w" s="T362">Nʼit</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1372" n="HIAT:w" s="T363">i</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1375" n="HIAT:w" s="T364">koʔbdo</ts>
                  <nts id="Seg_1376" n="HIAT:ip">.</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1379" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1381" n="HIAT:w" s="T365">Nʼi</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1384" n="HIAT:w" s="T366">i</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1387" n="HIAT:w" s="T367">koʔbdo</ts>
                  <nts id="Seg_1388" n="HIAT:ip">.</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1391" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1393" n="HIAT:w" s="T368">Oʔb</ts>
                  <nts id="Seg_1394" n="HIAT:ip">,</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1397" n="HIAT:w" s="T369">šide</ts>
                  <nts id="Seg_1398" n="HIAT:ip">,</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1401" n="HIAT:w" s="T370">nagur</ts>
                  <nts id="Seg_1402" n="HIAT:ip">,</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1405" n="HIAT:w" s="T371">sumna</ts>
                  <nts id="Seg_1406" n="HIAT:ip">,</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1409" n="HIAT:w" s="T372">muktuʔ</ts>
                  <nts id="Seg_1410" n="HIAT:ip">,</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1413" n="HIAT:w" s="T373">sejʔpü</ts>
                  <nts id="Seg_1414" n="HIAT:ip">,</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1417" n="HIAT:w" s="T374">šinteʔtə</ts>
                  <nts id="Seg_1418" n="HIAT:ip">,</nts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1421" n="HIAT:w" s="T375">amitun</ts>
                  <nts id="Seg_1422" n="HIAT:ip">,</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1425" n="HIAT:w" s="T376">bʼeʔ</ts>
                  <nts id="Seg_1426" n="HIAT:ip">,</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1429" n="HIAT:w" s="T377">bʼeʔ</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1432" n="HIAT:w" s="T378">onʼiʔ</ts>
                  <nts id="Seg_1433" n="HIAT:ip">,</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1436" n="HIAT:w" s="T379">bʼeʔ</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1439" n="HIAT:w" s="T380">šide</ts>
                  <nts id="Seg_1440" n="HIAT:ip">,</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1443" n="HIAT:w" s="T381">bʼeʔ</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1446" n="HIAT:w" s="T382">nagur</ts>
                  <nts id="Seg_1447" n="HIAT:ip">,</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1450" n="HIAT:w" s="T383">bʼeʔ</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1453" n="HIAT:w" s="T384">teʔtə</ts>
                  <nts id="Seg_1454" n="HIAT:ip">,</nts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1457" n="HIAT:w" s="T385">bʼeʔ</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1460" n="HIAT:w" s="T386">sumna</ts>
                  <nts id="Seg_1461" n="HIAT:ip">,</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1464" n="HIAT:w" s="T387">bʼeʔ</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1467" n="HIAT:w" s="T388">muktuʔ</ts>
                  <nts id="Seg_1468" n="HIAT:ip">,</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1471" n="HIAT:w" s="T389">bʼeʔ</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1474" n="HIAT:w" s="T390">sejʔpü</ts>
                  <nts id="Seg_1475" n="HIAT:ip">,</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1478" n="HIAT:w" s="T391">bʼeʔ</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1481" n="HIAT:w" s="T392">šinteʔtə</ts>
                  <nts id="Seg_1482" n="HIAT:ip">,</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1485" n="HIAT:w" s="T393">bʼeʔ</ts>
                  <nts id="Seg_1486" n="HIAT:ip">…</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1489" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1491" n="HIAT:w" s="T395">Забыла</ts>
                  <nts id="Seg_1492" n="HIAT:ip">.</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1494" n="sc" s="T418">
               <ts e="T425" id="Seg_1496" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1498" n="HIAT:w" s="T418">Oʔb</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1502" n="HIAT:w" s="T419">šide</ts>
                  <nts id="Seg_1503" n="HIAT:ip">,</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1506" n="HIAT:w" s="T420">nagur</ts>
                  <nts id="Seg_1507" n="HIAT:ip">,</nts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1509" n="HIAT:ip">(</nts>
                  <ts e="T422" id="Seg_1511" n="HIAT:w" s="T421">muktuʔ</ts>
                  <nts id="Seg_1512" n="HIAT:ip">)</nts>
                  <nts id="Seg_1513" n="HIAT:ip">,</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1515" n="HIAT:ip">(</nts>
                  <ts e="T423" id="Seg_1517" n="HIAT:w" s="T422">sejʔpü</ts>
                  <nts id="Seg_1518" n="HIAT:ip">)</nts>
                  <nts id="Seg_1519" n="HIAT:ip">,</nts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1522" n="HIAT:w" s="T423">šinteʔtə</ts>
                  <nts id="Seg_1523" n="HIAT:ip">,</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1526" n="HIAT:w" s="T424">amitun</ts>
                  <nts id="Seg_1527" n="HIAT:ip">.</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1530" n="HIAT:u" s="T425">
                  <nts id="Seg_1531" n="HIAT:ip">(</nts>
                  <nts id="Seg_1532" n="HIAT:ip">(</nts>
                  <ats e="T426" id="Seg_1533" n="HIAT:non-pho" s="T425">BRK</ats>
                  <nts id="Seg_1534" n="HIAT:ip">)</nts>
                  <nts id="Seg_1535" n="HIAT:ip">)</nts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1539" n="HIAT:u" s="T426">
                  <nts id="Seg_1540" n="HIAT:ip">"</nts>
                  <ts e="T427" id="Seg_1542" n="HIAT:w" s="T426">Măna</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1545" n="HIAT:w" s="T427">nada</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1548" n="HIAT:w" s="T428">monoʔkozittə</ts>
                  <nts id="Seg_1549" n="HIAT:ip">,</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1552" n="HIAT:w" s="T429">a</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1555" n="HIAT:w" s="T430">šindim</ts>
                  <nts id="Seg_1556" n="HIAT:ip">?</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1559" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1561" n="HIAT:w" s="T431">Dĭ</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1564" n="HIAT:w" s="T432">koʔbdo</ts>
                  <nts id="Seg_1565" n="HIAT:ip">.</nts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1568" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1570" n="HIAT:w" s="T433">Kanaʔ</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1573" n="HIAT:w" s="T434">monoʔkot</ts>
                  <nts id="Seg_1574" n="HIAT:ip">!</nts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1577" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1579" n="HIAT:w" s="T435">Всё</ts>
                  <nts id="Seg_1580" n="HIAT:ip">.</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1583" n="HIAT:u" s="T436">
                  <nts id="Seg_1584" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_1586" n="HIAT:w" s="T436">-təbtə</ts>
                  <nts id="Seg_1587" n="HIAT:ip">)</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1590" n="HIAT:w" s="T438">turagən</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1593" n="HIAT:w" s="T439">amnolaʔbəm</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1597" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1599" n="HIAT:w" s="T440">Oj</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1602" n="HIAT:w" s="T441">bar</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1605" n="HIAT:w" s="T442">üjüm</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1608" n="HIAT:w" s="T443">kutʼümnie</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1612" n="HIAT:w" s="T444">nada</ts>
                  <nts id="Seg_1613" n="HIAT:ip">…</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1616" n="HIAT:u" s="T446">
                  <nts id="Seg_1617" n="HIAT:ip">(</nts>
                  <nts id="Seg_1618" n="HIAT:ip">(</nts>
                  <ats e="T447" id="Seg_1619" n="HIAT:non-pho" s="T446">BRK</ats>
                  <nts id="Seg_1620" n="HIAT:ip">)</nts>
                  <nts id="Seg_1621" n="HIAT:ip">)</nts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1625" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1627" n="HIAT:w" s="T447">Ulum</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1630" n="HIAT:w" s="T448">ĭzemnie</ts>
                  <nts id="Seg_1631" n="HIAT:ip">.</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1634" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1636" n="HIAT:w" s="T449">Bögelbə</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1639" n="HIAT:w" s="T450">ĭzemnie</ts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1643" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1645" n="HIAT:w" s="T451">Udam</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1648" n="HIAT:w" s="T452">ĭzemnie</ts>
                  <nts id="Seg_1649" n="HIAT:ip">.</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1652" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1654" n="HIAT:w" s="T453">Sʼimam</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1657" n="HIAT:w" s="T454">ĭzemnie</ts>
                  <nts id="Seg_1658" n="HIAT:ip">.</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1661" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1663" n="HIAT:w" s="T455">Ku</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1666" n="HIAT:w" s="T456">ĭzemnie</ts>
                  <nts id="Seg_1667" n="HIAT:ip">.</nts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1670" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1672" n="HIAT:w" s="T457">Üjüm</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1675" n="HIAT:w" s="T458">ĭzemnie</ts>
                  <nts id="Seg_1676" n="HIAT:ip">.</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1679" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1681" n="HIAT:w" s="T459">Kötenbə</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1684" n="HIAT:w" s="T460">ĭzemnie</ts>
                  <nts id="Seg_1685" n="HIAT:ip">.</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1688" n="HIAT:u" s="T461">
                  <nts id="Seg_1689" n="HIAT:ip">(</nts>
                  <nts id="Seg_1690" n="HIAT:ip">(</nts>
                  <ats e="T462" id="Seg_1691" n="HIAT:non-pho" s="T461">BRK</ats>
                  <nts id="Seg_1692" n="HIAT:ip">)</nts>
                  <nts id="Seg_1693" n="HIAT:ip">)</nts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1697" n="HIAT:u" s="T462">
                  <nts id="Seg_1698" n="HIAT:ip">(</nts>
                  <ts e="T463" id="Seg_1700" n="HIAT:w" s="T462">Dʼok</ts>
                  <nts id="Seg_1701" n="HIAT:ip">)</nts>
                  <nts id="Seg_1702" n="HIAT:ip">.</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1705" n="HIAT:u" s="T463">
                  <nts id="Seg_1706" n="HIAT:ip">(</nts>
                  <nts id="Seg_1707" n="HIAT:ip">(</nts>
                  <ats e="T464" id="Seg_1708" n="HIAT:non-pho" s="T463">BRK</ats>
                  <nts id="Seg_1709" n="HIAT:ip">)</nts>
                  <nts id="Seg_1710" n="HIAT:ip">)</nts>
                  <nts id="Seg_1711" n="HIAT:ip">.</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1714" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1716" n="HIAT:w" s="T464">Dʼijenə</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1719" n="HIAT:w" s="T465">nada</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1722" n="HIAT:w" s="T466">kanzittə</ts>
                  <nts id="Seg_1723" n="HIAT:ip">,</nts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1726" n="HIAT:w" s="T467">albuga</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1729" n="HIAT:w" s="T468">kuʔsittə</ts>
                  <nts id="Seg_1730" n="HIAT:ip">.</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1733" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1735" n="HIAT:w" s="T469">Urgaːba</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1738" n="HIAT:w" s="T470">kuʔsittə</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1741" n="HIAT:w" s="T471">nada</ts>
                  <nts id="Seg_1742" n="HIAT:ip">.</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1745" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_1747" n="HIAT:w" s="T472">Tažəp</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1750" n="HIAT:w" s="T473">kuʔsittə</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1753" n="HIAT:w" s="T474">nada</ts>
                  <nts id="Seg_1754" n="HIAT:ip">.</nts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1757" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1759" n="HIAT:w" s="T475">Bulan</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1762" n="HIAT:w" s="T476">kuʔsittə</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1765" n="HIAT:w" s="T477">nada</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1769" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1771" n="HIAT:w" s="T478">Sön</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1774" n="HIAT:w" s="T479">kuʔsittə</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1777" n="HIAT:w" s="T480">nada</ts>
                  <nts id="Seg_1778" n="HIAT:ip">.</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1781" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1783" n="HIAT:w" s="T481">Poʔto</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1786" n="HIAT:w" s="T482">nada</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1789" n="HIAT:w" s="T483">kuʔsittə</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1793" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1795" n="HIAT:w" s="T484">Uja</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1798" n="HIAT:w" s="T485">iʔgö</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1801" n="HIAT:w" s="T486">moləj</ts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1805" n="HIAT:w" s="T487">amzittə</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1808" n="HIAT:w" s="T488">nada</ts>
                  <nts id="Seg_1809" n="HIAT:ip">.</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1812" n="HIAT:u" s="T489">
                  <nts id="Seg_1813" n="HIAT:ip">(</nts>
                  <ts e="T490" id="Seg_1815" n="HIAT:w" s="T489">Mĭnzitən-</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1818" n="HIAT:w" s="T490">mĭrd-</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1821" n="HIAT:w" s="T491">mĭrzintə-</ts>
                  <nts id="Seg_1822" n="HIAT:ip">)</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1825" n="HIAT:w" s="T492">nada</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1827" n="HIAT:ip">(</nts>
                  <nts id="Seg_1828" n="HIAT:ip">(</nts>
                  <ats e="T494" id="Seg_1829" n="HIAT:non-pho" s="T493">LAUGH</ats>
                  <nts id="Seg_1830" n="HIAT:ip">)</nts>
                  <nts id="Seg_1831" n="HIAT:ip">)</nts>
                  <nts id="Seg_1832" n="HIAT:ip">.</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T502" id="Seg_1834" n="sc" s="T495">
               <ts e="T502" id="Seg_1836" n="HIAT:u" s="T495">
                  <nts id="Seg_1837" n="HIAT:ip">(</nts>
                  <ts e="T496" id="Seg_1839" n="HIAT:w" s="T495">Uja=</ts>
                  <nts id="Seg_1840" n="HIAT:ip">)</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1843" n="HIAT:w" s="T496">Uja</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1845" n="HIAT:ip">(</nts>
                  <ts e="T498" id="Seg_1847" n="HIAT:w" s="T497">eneʔ</ts>
                  <nts id="Seg_1848" n="HIAT:ip">)</nts>
                  <nts id="Seg_1849" n="HIAT:ip">,</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1852" n="HIAT:w" s="T498">šü</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1855" n="HIAT:w" s="T499">nendleʔbə</ts>
                  <nts id="Seg_1856" n="HIAT:ip">,</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1859" n="HIAT:w" s="T500">uja</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1861" n="HIAT:ip">(</nts>
                  <ts e="T502" id="Seg_1863" n="HIAT:w" s="T501">eneʔ</ts>
                  <nts id="Seg_1864" n="HIAT:ip">)</nts>
                  <nts id="Seg_1865" n="HIAT:ip">.</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T507" id="Seg_1867" n="sc" s="T504">
               <ts e="T507" id="Seg_1869" n="HIAT:u" s="T504">
                  <ts e="T504.tx-PKZ.1" id="Seg_1871" n="HIAT:w" s="T504">Кого</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504.tx-PKZ.2" id="Seg_1874" n="HIAT:w" s="T504.tx-PKZ.1">еще</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1877" n="HIAT:w" s="T504.tx-PKZ.2">раз</ts>
                  <nts id="Seg_1878" n="HIAT:ip">?</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_1880" n="sc" s="T514">
               <ts e="T517" id="Seg_1882" n="HIAT:u" s="T514">
                  <ts e="T517" id="Seg_1884" n="HIAT:w" s="T514">А-а</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_1888" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1890" n="HIAT:w" s="T517">Šü</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1893" n="HIAT:w" s="T518">nendleʔbə</ts>
                  <nts id="Seg_1894" n="HIAT:ip">,</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1897" n="HIAT:w" s="T519">uja</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1900" n="HIAT:w" s="T520">eneʔ</ts>
                  <nts id="Seg_1901" n="HIAT:ip">.</nts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1904" n="HIAT:u" s="T521">
                  <nts id="Seg_1905" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_1907" n="HIAT:w" s="T521">Puska-</ts>
                  <nts id="Seg_1908" n="HIAT:ip">)</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1911" n="HIAT:w" s="T522">Puskaj</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1914" n="HIAT:w" s="T523">pürleʔbə</ts>
                  <nts id="Seg_1915" n="HIAT:ip">.</nts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_1918" n="HIAT:u" s="T524">
                  <nts id="Seg_1919" n="HIAT:ip">(</nts>
                  <nts id="Seg_1920" n="HIAT:ip">(</nts>
                  <ats e="T525" id="Seg_1921" n="HIAT:non-pho" s="T524">BRK</ats>
                  <nts id="Seg_1922" n="HIAT:ip">)</nts>
                  <nts id="Seg_1923" n="HIAT:ip">)</nts>
                  <nts id="Seg_1924" n="HIAT:ip">.</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_1927" n="HIAT:u" s="T525">
                  <nts id="Seg_1928" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_1930" n="HIAT:w" s="T525">Šünə=</ts>
                  <nts id="Seg_1931" n="HIAT:ip">)</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1934" n="HIAT:w" s="T526">Šünə</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1937" n="HIAT:w" s="T527">endleʔbə</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1940" n="HIAT:w" s="T528">uja</ts>
                  <nts id="Seg_1941" n="HIAT:ip">.</nts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_1944" n="HIAT:u" s="T529">
                  <nts id="Seg_1945" n="HIAT:ip">(</nts>
                  <nts id="Seg_1946" n="HIAT:ip">(</nts>
                  <ats e="T530" id="Seg_1947" n="HIAT:non-pho" s="T529">BRK</ats>
                  <nts id="Seg_1948" n="HIAT:ip">)</nts>
                  <nts id="Seg_1949" n="HIAT:ip">)</nts>
                  <nts id="Seg_1950" n="HIAT:ip">.</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_1953" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1955" n="HIAT:w" s="T530">Amnaʔ</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1958" n="HIAT:w" s="T531">döbər</ts>
                  <nts id="Seg_1959" n="HIAT:ip">,</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1962" n="HIAT:w" s="T532">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1963" n="HIAT:ip">,</nts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1966" n="HIAT:w" s="T533">gijen</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1969" n="HIAT:w" s="T534">ibiel</ts>
                  <nts id="Seg_1970" n="HIAT:ip">.</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T538" id="Seg_1973" n="HIAT:u" s="T535">
                  <nts id="Seg_1974" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_1976" n="HIAT:w" s="T535">Ĭmbi=</ts>
                  <nts id="Seg_1977" n="HIAT:ip">)</nts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1980" n="HIAT:w" s="T536">Ĭmbi</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1983" n="HIAT:w" s="T537">kubial</ts>
                  <nts id="Seg_1984" n="HIAT:ip">?</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_1987" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1989" n="HIAT:w" s="T538">Gibər</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1992" n="HIAT:w" s="T539">kandəgal</ts>
                  <nts id="Seg_1993" n="HIAT:ip">?</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1996" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_1998" n="HIAT:w" s="T540">Dʼijenə</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2001" n="HIAT:w" s="T541">kandəgam</ts>
                  <nts id="Seg_2002" n="HIAT:ip">,</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2005" n="HIAT:w" s="T542">keʔbdejleʔ</ts>
                  <nts id="Seg_2006" n="HIAT:ip">.</nts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2009" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2011" n="HIAT:w" s="T543">Dĭn</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2014" n="HIAT:w" s="T544">urgaːba</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2018" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2020" n="HIAT:w" s="T545">Ugaːndə</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2023" n="HIAT:w" s="T546">pimniem</ts>
                  <nts id="Seg_2024" n="HIAT:ip">.</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2027" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_2029" n="HIAT:w" s="T547">No</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2032" n="HIAT:w" s="T548">iʔ</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2035" n="HIAT:w" s="T549">kanaʔ</ts>
                  <nts id="Seg_2036" n="HIAT:ip">,</nts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2039" n="HIAT:w" s="T550">pimniel</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2042" n="HIAT:w" s="T551">dăk</ts>
                  <nts id="Seg_2043" n="HIAT:ip">.</nts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2046" n="HIAT:u" s="T552">
                  <ts e="T552.tx-PKZ.1" id="Seg_2048" n="HIAT:w" s="T552">Ну</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2051" n="HIAT:w" s="T552.tx-PKZ.1">хватит</ts>
                  <nts id="Seg_2052" n="HIAT:ip">.</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2055" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2057" n="HIAT:w" s="T554">Oʔb</ts>
                  <nts id="Seg_2058" n="HIAT:ip">,</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2061" n="HIAT:w" s="T555">šide</ts>
                  <nts id="Seg_2062" n="HIAT:ip">,</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2065" n="HIAT:w" s="T556">nagur</ts>
                  <nts id="Seg_2066" n="HIAT:ip">,</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2069" n="HIAT:w" s="T557">teʔtə</ts>
                  <nts id="Seg_2070" n="HIAT:ip">,</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2073" n="HIAT:w" s="T558">sumna</ts>
                  <nts id="Seg_2074" n="HIAT:ip">,</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2077" n="HIAT:w" s="T559">sejʔpü</ts>
                  <nts id="Seg_2078" n="HIAT:ip">,</nts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2080" n="HIAT:ip">(</nts>
                  <ts e="T561" id="Seg_2082" n="HIAT:w" s="T560">šin-</ts>
                  <nts id="Seg_2083" n="HIAT:ip">)</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2086" n="HIAT:w" s="T561">sejʔpü</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2090" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2092" n="HIAT:w" s="T562">Oʔb</ts>
                  <nts id="Seg_2093" n="HIAT:ip">,</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2096" n="HIAT:w" s="T563">šide</ts>
                  <nts id="Seg_2097" n="HIAT:ip">,</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2100" n="HIAT:w" s="T564">nagur</ts>
                  <nts id="Seg_2101" n="HIAT:ip">,</nts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2104" n="HIAT:w" s="T565">muktuʔ</ts>
                  <nts id="Seg_2105" n="HIAT:ip">,</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2108" n="HIAT:w" s="T566">sejʔpü</ts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2112" n="HIAT:w" s="T567">sumna</ts>
                  <nts id="Seg_2113" n="HIAT:ip">,</nts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2116" n="HIAT:w" s="T568">šinteʔtə</ts>
                  <nts id="Seg_2117" n="HIAT:ip">,</nts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2120" n="HIAT:w" s="T569">amitun</ts>
                  <nts id="Seg_2121" n="HIAT:ip">,</nts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2124" n="HIAT:w" s="T570">bʼeʔ</ts>
                  <nts id="Seg_2125" n="HIAT:ip">.</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_2127" n="sc" s="T573">
               <ts e="T585" id="Seg_2129" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2131" n="HIAT:w" s="T573">Bʼeʔ</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2134" n="HIAT:w" s="T574">onʼiʔ</ts>
                  <nts id="Seg_2135" n="HIAT:ip">,</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2138" n="HIAT:w" s="T575">bʼeʔ</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2141" n="HIAT:w" s="T576">šide</ts>
                  <nts id="Seg_2142" n="HIAT:ip">,</nts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2145" n="HIAT:w" s="T577">bʼeʔ</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2148" n="HIAT:w" s="T578">nagur</ts>
                  <nts id="Seg_2149" n="HIAT:ip">,</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2152" n="HIAT:w" s="T579">bʼeʔ</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2155" n="HIAT:w" s="T580">teʔtə</ts>
                  <nts id="Seg_2156" n="HIAT:ip">,</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2159" n="HIAT:w" s="T581">bʼeʔ</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2162" n="HIAT:w" s="T582">sumna</ts>
                  <nts id="Seg_2163" n="HIAT:ip">,</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2166" n="HIAT:w" s="T583">bʼeʔ</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2169" n="HIAT:w" s="T584">sejʔpü</ts>
                  <nts id="Seg_2170" n="HIAT:ip">.</nts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2173" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2175" n="HIAT:w" s="T585">Bʼeʔ</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2178" n="HIAT:w" s="T586">šinteʔtə</ts>
                  <nts id="Seg_2179" n="HIAT:ip">,</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2182" n="HIAT:w" s="T587">bʼeʔ</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2185" n="HIAT:w" s="T588">sejʔpü</ts>
                  <nts id="Seg_2186" n="HIAT:ip">,</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2189" n="HIAT:w" s="T589">bʼeʔ</ts>
                  <nts id="Seg_2190" n="HIAT:ip">.</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2193" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2195" n="HIAT:w" s="T590">Šide</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2197" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2199" n="HIAT:w" s="T591">bʼeʔ</ts>
                  <nts id="Seg_2200" n="HIAT:ip">)</nts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2204" n="HIAT:u" s="T592">
                  <ts e="T592.tx-PKZ.1" id="Seg_2206" n="HIAT:w" s="T592">Забыла</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592.tx-PKZ.2" id="Seg_2209" n="HIAT:w" s="T592.tx-PKZ.1">я</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2211" n="HIAT:ip">(</nts>
                  <nts id="Seg_2212" n="HIAT:ip">(</nts>
                  <ats e="T595" id="Seg_2213" n="HIAT:non-pho" s="T592.tx-PKZ.2">BRK</ats>
                  <nts id="Seg_2214" n="HIAT:ip">)</nts>
                  <nts id="Seg_2215" n="HIAT:ip">)</nts>
                  <nts id="Seg_2216" n="HIAT:ip">…</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T598" id="Seg_2219" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2221" n="HIAT:w" s="T595">Surdəsʼtə</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2224" n="HIAT:w" s="T596">nada</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2227" n="HIAT:w" s="T597">tüžöjəm</ts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2231" n="HIAT:u" s="T598">
                  <ts e="T599" id="Seg_2233" n="HIAT:w" s="T598">Šedendə</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2236" n="HIAT:w" s="T599">kajzittə</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2239" n="HIAT:w" s="T600">nada</ts>
                  <nts id="Seg_2240" n="HIAT:ip">.</nts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2243" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2245" n="HIAT:w" s="T601">Surdəsʼtə</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2248" n="HIAT:w" s="T602">nada</ts>
                  <nts id="Seg_2249" n="HIAT:ip">.</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2252" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2254" n="HIAT:w" s="T603">Süt</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2257" n="HIAT:w" s="T604">detsittə</ts>
                  <nts id="Seg_2258" n="HIAT:ip">.</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2261" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2263" n="HIAT:w" s="T605">Šojdʼonə</ts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2266" n="HIAT:w" s="T606">enzittə</ts>
                  <nts id="Seg_2267" n="HIAT:ip">,</nts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2270" n="HIAT:w" s="T607">kămnasʼtə</ts>
                  <nts id="Seg_2271" n="HIAT:ip">.</nts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2274" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2276" n="HIAT:w" s="T608">Ну</ts>
                  <nts id="Seg_2277" n="HIAT:ip">,</nts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2280" n="HIAT:w" s="T609">все</ts>
                  <nts id="Seg_2281" n="HIAT:ip">.</nts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2284" n="HIAT:u" s="T610">
                  <nts id="Seg_2285" n="HIAT:ip">(</nts>
                  <nts id="Seg_2286" n="HIAT:ip">(</nts>
                  <ats e="T611" id="Seg_2287" n="HIAT:non-pho" s="T610">BRK</ats>
                  <nts id="Seg_2288" n="HIAT:ip">)</nts>
                  <nts id="Seg_2289" n="HIAT:ip">)</nts>
                  <nts id="Seg_2290" n="HIAT:ip">.</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2293" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2295" n="HIAT:w" s="T611">Ну</ts>
                  <nts id="Seg_2296" n="HIAT:ip">?</nts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T620" id="Seg_2298" n="sc" s="T614">
               <ts e="T616" id="Seg_2300" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2302" n="HIAT:w" s="T614">Gibər</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2305" n="HIAT:w" s="T615">kandəgal</ts>
                  <nts id="Seg_2306" n="HIAT:ip">?</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_2309" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2311" n="HIAT:w" s="T616">Gibər</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2314" n="HIAT:w" s="T617">nuʔməleʔbəl</ts>
                  <nts id="Seg_2315" n="HIAT:ip">?</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2318" n="HIAT:u" s="T618">
                  <nts id="Seg_2319" n="HIAT:ip">(</nts>
                  <ts e="T619" id="Seg_2321" n="HIAT:w" s="T618">Tüžöjmə</ts>
                  <nts id="Seg_2322" n="HIAT:ip">)</nts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2325" n="HIAT:w" s="T619">naga</ts>
                  <nts id="Seg_2326" n="HIAT:ip">.</nts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_2328" n="sc" s="T621">
               <ts e="T624" id="Seg_2330" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2332" n="HIAT:w" s="T621">Măndərzittə</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2334" n="HIAT:ip">(</nts>
                  <ts e="T623" id="Seg_2336" n="HIAT:w" s="T622">šonə-</ts>
                  <nts id="Seg_2337" n="HIAT:ip">)</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2340" n="HIAT:w" s="T623">kalam</ts>
                  <nts id="Seg_2341" n="HIAT:ip">.</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2344" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2346" n="HIAT:w" s="T624">Kanžəbəj</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2349" n="HIAT:w" s="T625">mănzʼiʔ</ts>
                  <nts id="Seg_2350" n="HIAT:ip">!</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2353" n="HIAT:u" s="T626">
                  <ts e="T626.tx-PKZ.1" id="Seg_2355" n="HIAT:w" s="T626">A</ts>
                  <nts id="Seg_2356" n="HIAT:ip">_</nts>
                  <ts e="T627" id="Seg_2358" n="HIAT:w" s="T626.tx-PKZ.1">to</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2361" n="HIAT:w" s="T627">măn</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2364" n="HIAT:w" s="T628">pimniem</ts>
                  <nts id="Seg_2365" n="HIAT:ip">.</nts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2368" n="HIAT:u" s="T629">
                  <ts e="T629.tx-PKZ.1" id="Seg_2370" n="HIAT:w" s="T629">Мало</ts>
                  <nts id="Seg_2371" n="HIAT:ip">,</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2374" n="HIAT:w" s="T629.tx-PKZ.1">однако</ts>
                  <nts id="Seg_2375" n="HIAT:ip">.</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T706" id="Seg_2377" n="sc" s="T634">
               <ts e="T636" id="Seg_2379" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2381" n="HIAT:w" s="T634">Zdărowă</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2384" n="HIAT:w" s="T635">igel</ts>
                  <nts id="Seg_2385" n="HIAT:ip">!</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_2388" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2390" n="HIAT:w" s="T636">Amnaʔ</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2393" n="HIAT:w" s="T637">amorzittə</ts>
                  <nts id="Seg_2394" n="HIAT:ip">!</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2397" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2399" n="HIAT:w" s="T638">Ipek</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2402" n="HIAT:w" s="T639">lem</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2405" n="HIAT:w" s="T640">keʔbdezʼiʔ</ts>
                  <nts id="Seg_2406" n="HIAT:ip">.</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T642" id="Seg_2409" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2411" n="HIAT:w" s="T641">Kajaʔ</ts>
                  <nts id="Seg_2412" n="HIAT:ip">.</nts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2415" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2417" n="HIAT:w" s="T642">Uja</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2420" n="HIAT:w" s="T643">amaʔ</ts>
                  <nts id="Seg_2421" n="HIAT:ip">!</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2424" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2426" n="HIAT:w" s="T644">Keʔbde</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2429" n="HIAT:w" s="T645">amaʔ</ts>
                  <nts id="Seg_2430" n="HIAT:ip">!</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2433" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2435" n="HIAT:w" s="T646">Oroma</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2438" n="HIAT:w" s="T647">amaʔ</ts>
                  <nts id="Seg_2439" n="HIAT:ip">!</nts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2442" n="HIAT:u" s="T648">
                  <nts id="Seg_2443" n="HIAT:ip">(</nts>
                  <nts id="Seg_2444" n="HIAT:ip">(</nts>
                  <ats e="T649" id="Seg_2445" n="HIAT:non-pho" s="T648">BRK</ats>
                  <nts id="Seg_2446" n="HIAT:ip">)</nts>
                  <nts id="Seg_2447" n="HIAT:ip">)</nts>
                  <nts id="Seg_2448" n="HIAT:ip">.</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2451" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2453" n="HIAT:w" s="T649">Ipek</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2456" n="HIAT:w" s="T650">amaʔ</ts>
                  <nts id="Seg_2457" n="HIAT:ip">!</nts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2460" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2462" n="HIAT:w" s="T651">Segi</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2465" n="HIAT:w" s="T652">bü</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2468" n="HIAT:w" s="T653">bĭdeʔ</ts>
                  <nts id="Seg_2469" n="HIAT:ip">!</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2472" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2474" n="HIAT:w" s="T654">Sĭreʔpne</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2477" n="HIAT:w" s="T655">endə</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2480" n="HIAT:w" s="T656">segi</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2483" n="HIAT:w" s="T657">bünə</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2487" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2489" n="HIAT:w" s="T658">Süt</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2492" n="HIAT:w" s="T659">amnaʔ</ts>
                  <nts id="Seg_2493" n="HIAT:ip">!</nts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_2496" n="HIAT:u" s="T660">
                  <nts id="Seg_2497" n="HIAT:ip">(</nts>
                  <nts id="Seg_2498" n="HIAT:ip">(</nts>
                  <ats e="T661" id="Seg_2499" n="HIAT:non-pho" s="T660">BRK</ats>
                  <nts id="Seg_2500" n="HIAT:ip">)</nts>
                  <nts id="Seg_2501" n="HIAT:ip">)</nts>
                  <nts id="Seg_2502" n="HIAT:ip">.</nts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2505" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_2507" n="HIAT:w" s="T661">Kanžəbəj</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2510" n="HIAT:w" s="T662">obberəj</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2513" n="HIAT:w" s="T663">dʼijenə</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2516" n="HIAT:w" s="T664">keʔbde</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2519" n="HIAT:w" s="T665">oʔbdəsʼtə</ts>
                  <nts id="Seg_2520" n="HIAT:ip">.</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2523" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_2525" n="HIAT:w" s="T666">Kalba</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2528" n="HIAT:w" s="T667">nĭŋgəsʼtə</ts>
                  <nts id="Seg_2529" n="HIAT:ip">,</nts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2532" n="HIAT:w" s="T668">sanə</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2535" n="HIAT:w" s="T669">azittə</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2539" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2541" n="HIAT:w" s="T670">Iššo</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2544" n="HIAT:w" s="T671">ĭmbi</ts>
                  <nts id="Seg_2545" n="HIAT:ip">?</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2548" n="HIAT:u" s="T672">
                  <nts id="Seg_2549" n="HIAT:ip">(</nts>
                  <ts e="T673" id="Seg_2551" n="HIAT:w" s="T672">Beške</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2554" n="HIAT:w" s="T673">i-</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2557" n="HIAT:w" s="T674">i-</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2560" n="HIAT:w" s="T675">a-</ts>
                  <nts id="Seg_2561" n="HIAT:ip">)</nts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2564" n="HIAT:w" s="T676">Beške</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2567" n="HIAT:w" s="T677">izittə</ts>
                  <nts id="Seg_2568" n="HIAT:ip">.</nts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2571" n="HIAT:u" s="T678">
                  <nts id="Seg_2572" n="HIAT:ip">(</nts>
                  <nts id="Seg_2573" n="HIAT:ip">(</nts>
                  <ats e="T678.tx-PKZ.1" id="Seg_2574" n="HIAT:non-pho" s="T678">LAUGH</ats>
                  <nts id="Seg_2575" n="HIAT:ip">)</nts>
                  <nts id="Seg_2576" n="HIAT:ip">)</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2578" n="HIAT:ip">(</nts>
                  <nts id="Seg_2579" n="HIAT:ip">(</nts>
                  <ats e="T679" id="Seg_2580" n="HIAT:non-pho" s="T678.tx-PKZ.1">BRK</ats>
                  <nts id="Seg_2581" n="HIAT:ip">)</nts>
                  <nts id="Seg_2582" n="HIAT:ip">)</nts>
                  <nts id="Seg_2583" n="HIAT:ip">.</nts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2586" n="HIAT:u" s="T679">
                  <nts id="Seg_2587" n="HIAT:ip">(</nts>
                  <ts e="T679.tx-PKZ.1" id="Seg_2589" n="HIAT:w" s="T679">Я</ts>
                  <nts id="Seg_2590" n="HIAT:ip">)</nts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679.tx-PKZ.2" id="Seg_2593" n="HIAT:w" s="T679.tx-PKZ.1">тебя</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2596" n="HIAT:w" s="T679.tx-PKZ.2">жалею</ts>
                  <nts id="Seg_2597" n="HIAT:ip">.</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2600" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2602" n="HIAT:w" s="T682">Măn</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2605" n="HIAT:w" s="T683">tănan</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2608" n="HIAT:w" s="T684">ajirlaʔbəm</ts>
                  <nts id="Seg_2609" n="HIAT:ip">.</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2612" n="HIAT:u" s="T685">
                  <nts id="Seg_2613" n="HIAT:ip">(</nts>
                  <nts id="Seg_2614" n="HIAT:ip">(</nts>
                  <ats e="T686" id="Seg_2615" n="HIAT:non-pho" s="T685">BRK</ats>
                  <nts id="Seg_2616" n="HIAT:ip">)</nts>
                  <nts id="Seg_2617" n="HIAT:ip">)</nts>
                  <nts id="Seg_2618" n="HIAT:ip">.</nts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2621" n="HIAT:u" s="T686">
                  <ts e="T686.tx-PKZ.1" id="Seg_2623" n="HIAT:w" s="T686">Коня</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2626" n="HIAT:w" s="T686.tx-PKZ.1">запрягай</ts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2630" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2632" n="HIAT:w" s="T688">Kanaʔ</ts>
                  <nts id="Seg_2633" n="HIAT:ip">,</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2636" n="HIAT:w" s="T689">ine</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2639" n="HIAT:w" s="T690">kürereʔ</ts>
                  <nts id="Seg_2640" n="HIAT:ip">!</nts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2643" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2645" n="HIAT:w" s="T691">Kanžəbəj</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2648" n="HIAT:w" s="T692">gibər-nʼibudʼ</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2651" n="HIAT:w" s="T693">inezʼiʔ</ts>
                  <nts id="Seg_2652" n="HIAT:ip">.</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2655" n="HIAT:u" s="T694">
                  <nts id="Seg_2656" n="HIAT:ip">(</nts>
                  <ts e="T695" id="Seg_2658" n="HIAT:w" s="T694">Bü</ts>
                  <nts id="Seg_2659" n="HIAT:ip">)</nts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2661" n="HIAT:ip">(</nts>
                  <nts id="Seg_2662" n="HIAT:ip">(</nts>
                  <ats e="T695.tx-PKZ.1" id="Seg_2663" n="HIAT:non-pho" s="T695">DMG</ats>
                  <nts id="Seg_2664" n="HIAT:ip">)</nts>
                  <nts id="Seg_2665" n="HIAT:ip">)</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2667" n="HIAT:ip">(</nts>
                  <ts e="T696" id="Seg_2669" n="HIAT:w" s="T695.tx-PKZ.1">de-</ts>
                  <nts id="Seg_2670" n="HIAT:ip">)</nts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2673" n="HIAT:w" s="T696">detləbəj</ts>
                  <nts id="Seg_2674" n="HIAT:ip">.</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2677" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2679" n="HIAT:w" s="T697">Paʔi</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2682" n="HIAT:w" s="T698">detləbeʔ</ts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2686" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2688" n="HIAT:w" s="T699">Noʔ</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2691" n="HIAT:w" s="T700">detləbəj</ts>
                  <nts id="Seg_2692" n="HIAT:ip">.</nts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2695" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2697" n="HIAT:w" s="T701">Săloma</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2700" n="HIAT:w" s="T702">detləbəj</ts>
                  <nts id="Seg_2701" n="HIAT:ip">.</nts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_2704" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2706" n="HIAT:w" s="T703">Все</ts>
                  <nts id="Seg_2707" n="HIAT:ip">,</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2710" n="HIAT:w" s="T704">наверное</ts>
                  <nts id="Seg_2711" n="HIAT:ip">.</nts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2714" n="HIAT:u" s="T705">
                  <nts id="Seg_2715" n="HIAT:ip">(</nts>
                  <nts id="Seg_2716" n="HIAT:ip">(</nts>
                  <ats e="T706" id="Seg_2717" n="HIAT:non-pho" s="T705">BRK</ats>
                  <nts id="Seg_2718" n="HIAT:ip">)</nts>
                  <nts id="Seg_2719" n="HIAT:ip">)</nts>
                  <nts id="Seg_2720" n="HIAT:ip">.</nts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T739" id="Seg_2722" n="sc" s="T708">
               <ts e="T710" id="Seg_2724" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2726" n="HIAT:w" s="T708">Tuganbə</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2729" n="HIAT:w" s="T709">šobi</ts>
                  <nts id="Seg_2730" n="HIAT:ip">.</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2733" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2735" n="HIAT:w" s="T710">Nada</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2737" n="HIAT:ip">(</nts>
                  <ts e="T712" id="Seg_2739" n="HIAT:w" s="T711">m-</ts>
                  <nts id="Seg_2740" n="HIAT:ip">)</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2743" n="HIAT:w" s="T712">măndərzittə</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2746" n="HIAT:w" s="T713">kanzittə</ts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2750" n="HIAT:u" s="T714">
                  <nts id="Seg_2751" n="HIAT:ip">(</nts>
                  <nts id="Seg_2752" n="HIAT:ip">(</nts>
                  <ats e="T715" id="Seg_2753" n="HIAT:non-pho" s="T714">BRK</ats>
                  <nts id="Seg_2754" n="HIAT:ip">)</nts>
                  <nts id="Seg_2755" n="HIAT:ip">)</nts>
                  <nts id="Seg_2756" n="HIAT:ip">.</nts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2759" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2761" n="HIAT:w" s="T715">Nada</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2764" n="HIAT:w" s="T716">kăštəsʼtə</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2768" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_2770" n="HIAT:w" s="T717">Amorzittə</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2773" n="HIAT:w" s="T718">mĭzittə</ts>
                  <nts id="Seg_2774" n="HIAT:ip">.</nts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2777" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2779" n="HIAT:w" s="T719">Nada</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2782" n="HIAT:w" s="T720">kunolzittə</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2785" n="HIAT:w" s="T721">dĭzeŋdə</ts>
                  <nts id="Seg_2786" n="HIAT:ip">.</nts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2789" n="HIAT:u" s="T722">
                  <ts e="T722.tx-PKZ.1" id="Seg_2791" n="HIAT:w" s="T722">Больше</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2794" n="HIAT:w" s="T722.tx-PKZ.1">все</ts>
                  <nts id="Seg_2795" n="HIAT:ip">.</nts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2798" n="HIAT:u" s="T724">
                  <nts id="Seg_2799" n="HIAT:ip">(</nts>
                  <nts id="Seg_2800" n="HIAT:ip">(</nts>
                  <ats e="T724.tx-PKZ.1" id="Seg_2801" n="HIAT:non-pho" s="T724">BRK</ats>
                  <nts id="Seg_2802" n="HIAT:ip">)</nts>
                  <nts id="Seg_2803" n="HIAT:ip">)</nts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2806" n="HIAT:w" s="T724.tx-PKZ.1">Oʔb</ts>
                  <nts id="Seg_2807" n="HIAT:ip">,</nts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2810" n="HIAT:w" s="T725">oʔb</ts>
                  <nts id="Seg_2811" n="HIAT:ip">,</nts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2814" n="HIAT:w" s="T726">šide</ts>
                  <nts id="Seg_2815" n="HIAT:ip">,</nts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2818" n="HIAT:w" s="T727">nagur</ts>
                  <nts id="Seg_2819" n="HIAT:ip">,</nts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2822" n="HIAT:w" s="T728">teʔtə</ts>
                  <nts id="Seg_2823" n="HIAT:ip">,</nts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2826" n="HIAT:w" s="T729">sumna</ts>
                  <nts id="Seg_2827" n="HIAT:ip">,</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2830" n="HIAT:w" s="T730">sejʔpü</ts>
                  <nts id="Seg_2831" n="HIAT:ip">,</nts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2834" n="HIAT:w" s="T731">amitun</ts>
                  <nts id="Seg_2835" n="HIAT:ip">,</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2838" n="HIAT:w" s="T732">šinteʔtə</ts>
                  <nts id="Seg_2839" n="HIAT:ip">,</nts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2842" n="HIAT:w" s="T733">bʼeʔ</ts>
                  <nts id="Seg_2843" n="HIAT:ip">.</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T739" id="Seg_2846" n="HIAT:u" s="T734">
                  <ts e="T734.tx-PKZ.1" id="Seg_2848" n="HIAT:w" s="T734">Вот</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734.tx-PKZ.2" id="Seg_2851" n="HIAT:w" s="T734.tx-PKZ.1">девять-то</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734.tx-PKZ.3" id="Seg_2854" n="HIAT:w" s="T734.tx-PKZ.2">оставила</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2856" n="HIAT:ip">(</nts>
                  <nts id="Seg_2857" n="HIAT:ip">(</nts>
                  <ats e="T739" id="Seg_2858" n="HIAT:non-pho" s="T734.tx-PKZ.3">…</ats>
                  <nts id="Seg_2859" n="HIAT:ip">)</nts>
                  <nts id="Seg_2860" n="HIAT:ip">)</nts>
                  <nts id="Seg_2861" n="HIAT:ip">.</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T9" id="Seg_2863" n="sc" s="T2">
               <ts e="T9" id="Seg_2865" n="e" s="T2">((…DMG)) а чего это, на что? </ts>
            </ts>
            <ts e="T15" id="Seg_2866" n="sc" s="T14">
               <ts e="T15" id="Seg_2868" n="e" s="T14">Сюда? </ts>
            </ts>
            <ts e="T19" id="Seg_2869" n="sc" s="T17">
               <ts e="T18" id="Seg_2871" n="e" s="T17">Iʔ </ts>
               <ts e="T19" id="Seg_2873" n="e" s="T18">kudonzaʔ. </ts>
            </ts>
            <ts e="T25" id="Seg_2874" n="sc" s="T22">
               <ts e="T23" id="Seg_2876" n="e" s="T22">Măn </ts>
               <ts e="T24" id="Seg_2878" n="e" s="T23">dĭm </ts>
               <ts e="T25" id="Seg_2880" n="e" s="T24">ibiem. </ts>
            </ts>
            <ts e="T32" id="Seg_2881" n="sc" s="T27">
               <ts e="T28" id="Seg_2883" n="e" s="T27">Dĭn </ts>
               <ts e="T29" id="Seg_2885" n="e" s="T28">bar </ts>
               <ts e="T30" id="Seg_2887" n="e" s="T29">kudonzlaʔbəʔjə. </ts>
               <ts e="T31" id="Seg_2889" n="e" s="T30">Măn </ts>
               <ts e="T742" id="Seg_2891" n="e" s="T31">kalla </ts>
               <ts e="T32" id="Seg_2893" n="e" s="T742">dʼürbiem. </ts>
            </ts>
            <ts e="T46" id="Seg_2894" n="sc" s="T36">
               <ts e="T37" id="Seg_2896" n="e" s="T36">Iʔgö </ts>
               <ts e="T38" id="Seg_2898" n="e" s="T37">pʼaŋdəbial </ts>
               <ts e="T39" id="Seg_2900" n="e" s="T38">bar. </ts>
               <ts e="T40" id="Seg_2902" n="e" s="T39">((BRK)). </ts>
               <ts e="T41" id="Seg_2904" n="e" s="T40">Koŋ </ts>
               <ts e="T42" id="Seg_2906" n="e" s="T41">bar </ts>
               <ts e="T43" id="Seg_2908" n="e" s="T42">nʼešpək </ts>
               <ts e="T44" id="Seg_2910" n="e" s="T43">šobi. </ts>
               <ts e="T45" id="Seg_2912" n="e" s="T44">Jezerik </ts>
               <ts e="T46" id="Seg_2914" n="e" s="T45">bar. </ts>
            </ts>
            <ts e="T51" id="Seg_2915" n="sc" s="T47">
               <ts e="T48" id="Seg_2917" n="e" s="T47">(Не) </ts>
               <ts e="T49" id="Seg_2919" n="e" s="T48">забыла, </ts>
               <ts e="T50" id="Seg_2921" n="e" s="T49">как" </ts>
               <ts e="T51" id="Seg_2923" n="e" s="T50">упал". </ts>
            </ts>
            <ts e="T54" id="Seg_2924" n="sc" s="T52">
               <ts e="T54" id="Seg_2926" n="e" s="T52">ige. </ts>
            </ts>
            <ts e="T61" id="Seg_2927" n="sc" s="T56">
               <ts e="T57" id="Seg_2929" n="e" s="T56">Sabən </ts>
               <ts e="T58" id="Seg_2931" n="e" s="T57">ige. </ts>
               <ts e="T59" id="Seg_2933" n="e" s="T58">(Kĭš-) </ts>
               <ts e="T60" id="Seg_2935" n="e" s="T59">Kĭškit </ts>
               <ts e="T61" id="Seg_2937" n="e" s="T60">bar. </ts>
            </ts>
            <ts e="T64" id="Seg_2938" n="sc" s="T62">
               <ts e="T64" id="Seg_2940" n="e" s="T62">Трись, говорю. </ts>
            </ts>
            <ts e="T91" id="Seg_2941" n="sc" s="T70">
               <ts e="T71" id="Seg_2943" n="e" s="T70">(Sp-) </ts>
               <ts e="T72" id="Seg_2945" n="e" s="T71">Ugaːndə </ts>
               <ts e="T73" id="Seg_2947" n="e" s="T72">tăn </ts>
               <ts e="T74" id="Seg_2949" n="e" s="T73">sagər, </ts>
               <ts e="T75" id="Seg_2951" n="e" s="T74">kanaʔ </ts>
               <ts e="T76" id="Seg_2953" n="e" s="T75">moltʼanə! </ts>
               <ts e="T77" id="Seg_2955" n="e" s="T76">((BRK)). </ts>
               <ts e="T78" id="Seg_2957" n="e" s="T77">Dĭn </ts>
               <ts e="T79" id="Seg_2959" n="e" s="T78">dʼibige </ts>
               <ts e="T80" id="Seg_2961" n="e" s="T79">(ige) </ts>
               <ts e="T81" id="Seg_2963" n="e" s="T80">i </ts>
               <ts e="T82" id="Seg_2965" n="e" s="T81">šĭšəge </ts>
               <ts e="T83" id="Seg_2967" n="e" s="T82">bü, </ts>
               <ts e="T84" id="Seg_2969" n="e" s="T83">sabən </ts>
               <ts e="T85" id="Seg_2971" n="e" s="T84">ige. </ts>
               <ts e="T86" id="Seg_2973" n="e" s="T85">Păzaʔ </ts>
               <ts e="T87" id="Seg_2975" n="e" s="T86">(хорошенько), </ts>
               <ts e="T88" id="Seg_2977" n="e" s="T87">kĭškit! </ts>
               <ts e="T89" id="Seg_2979" n="e" s="T88">Трись. </ts>
               <ts e="T91" id="Seg_2981" n="e" s="T89">to. </ts>
            </ts>
            <ts e="T101" id="Seg_2982" n="sc" s="T96">
               <ts e="T101" id="Seg_2984" n="e" s="T96">Али по-новому говорю? </ts>
            </ts>
            <ts e="T155" id="Seg_2985" n="sc" s="T107">
               <ts e="T108" id="Seg_2987" n="e" s="T107">Tüžöjəʔi </ts>
               <ts e="T109" id="Seg_2989" n="e" s="T108">šedendə. </ts>
               <ts e="T110" id="Seg_2991" n="e" s="T109">((BRK)) Нет, </ts>
               <ts e="T111" id="Seg_2993" n="e" s="T110">((…)) </ts>
               <ts e="T112" id="Seg_2995" n="e" s="T111">nuzaŋ </ts>
               <ts e="T113" id="Seg_2997" n="e" s="T112">šobiʔi. </ts>
               <ts e="T114" id="Seg_2999" n="e" s="T113">Amnaʔ, </ts>
               <ts e="T115" id="Seg_3001" n="e" s="T114">amoraʔ! </ts>
               <ts e="T116" id="Seg_3003" n="e" s="T115">Padʼi </ts>
               <ts e="T117" id="Seg_3005" n="e" s="T116">amorzittə </ts>
               <ts e="T118" id="Seg_3007" n="e" s="T117">axota. </ts>
               <ts e="T119" id="Seg_3009" n="e" s="T118">Gijen </ts>
               <ts e="T120" id="Seg_3011" n="e" s="T119">ibiel? </ts>
               <ts e="T121" id="Seg_3013" n="e" s="T120">Amnaʔ </ts>
               <ts e="T122" id="Seg_3015" n="e" s="T121">döbər, </ts>
               <ts e="T123" id="Seg_3017" n="e" s="T122">kuvas </ts>
               <ts e="T124" id="Seg_3019" n="e" s="T123">kuza! </ts>
               <ts e="T126" id="Seg_3021" n="e" s="T124">Ну хватит. </ts>
               <ts e="T128" id="Seg_3023" n="e" s="T126">(-tədada). </ts>
               <ts e="T129" id="Seg_3025" n="e" s="T128">Nada </ts>
               <ts e="T130" id="Seg_3027" n="e" s="T129">tura </ts>
               <ts e="T131" id="Seg_3029" n="e" s="T130">(băzəjsʼtə). </ts>
               <ts e="T132" id="Seg_3031" n="e" s="T131">Nada </ts>
               <ts e="T133" id="Seg_3033" n="e" s="T132">săbərəjzittə. </ts>
               <ts e="T134" id="Seg_3035" n="e" s="T133">(Nʼiʔdə=) </ts>
               <ts e="T135" id="Seg_3037" n="e" s="T134">Nʼiʔdə </ts>
               <ts e="T136" id="Seg_3039" n="e" s="T135">kanzittə. </ts>
               <ts e="T137" id="Seg_3041" n="e" s="T136">Tüžöj </ts>
               <ts e="T138" id="Seg_3043" n="e" s="T137">surdəsʼtə. </ts>
               <ts e="T139" id="Seg_3045" n="e" s="T138">Eššim </ts>
               <ts e="T140" id="Seg_3047" n="e" s="T139">băzəjsʼtə. </ts>
               <ts e="T141" id="Seg_3049" n="e" s="T140">((BRK)). </ts>
               <ts e="T142" id="Seg_3051" n="e" s="T141">Ipek </ts>
               <ts e="T143" id="Seg_3053" n="e" s="T142">pürzittə. </ts>
               <ts e="T144" id="Seg_3055" n="e" s="T143">Ipek </ts>
               <ts e="T145" id="Seg_3057" n="e" s="T144">nuldəsʼtə. </ts>
               <ts e="T146" id="Seg_3059" n="e" s="T145">Хватит? </ts>
               <ts e="T148" id="Seg_3061" n="e" s="T146">pürzittə. </ts>
               <ts e="T149" id="Seg_3063" n="e" s="T148">(Sʼel) </ts>
               <ts e="T150" id="Seg_3065" n="e" s="T149">pürzittə </ts>
               <ts e="T151" id="Seg_3067" n="e" s="T150">nada. </ts>
               <ts e="T155" id="Seg_3069" n="e" s="T151">Еще чего-(нибудь). </ts>
            </ts>
            <ts e="T243" id="Seg_3070" n="sc" s="T158">
               <ts e="T165" id="Seg_3072" n="e" s="T158">(Ча-) Чай варить как-то. </ts>
               <ts e="T166" id="Seg_3074" n="e" s="T165">Segi </ts>
               <ts e="T167" id="Seg_3076" n="e" s="T166">bü </ts>
               <ts e="T168" id="Seg_3078" n="e" s="T167">nada </ts>
               <ts e="T169" id="Seg_3080" n="e" s="T168">mĭnzərzittə. </ts>
               <ts e="T171" id="Seg_3082" n="e" s="T169">((DMG)) kirgarlaʔbə, </ts>
               <ts e="T172" id="Seg_3084" n="e" s="T171">i </ts>
               <ts e="T173" id="Seg_3086" n="e" s="T172">kuromaʔ! </ts>
               <ts e="T174" id="Seg_3088" n="e" s="T173">I </ts>
               <ts e="T175" id="Seg_3090" n="e" s="T174">alomaʔ. </ts>
               <ts e="T179" id="Seg_3092" n="e" s="T175">Еще как-то? </ts>
               <ts e="T181" id="Seg_3094" n="e" s="T179">Господи. </ts>
               <ts e="T182" id="Seg_3096" n="e" s="T181">Забыла. </ts>
               <ts e="T183" id="Seg_3098" n="e" s="T182">Măn </ts>
               <ts e="T184" id="Seg_3100" n="e" s="T183">dĭn </ts>
               <ts e="T185" id="Seg_3102" n="e" s="T184">ibiem. </ts>
               <ts e="T186" id="Seg_3104" n="e" s="T185">Dĭn </ts>
               <ts e="T187" id="Seg_3106" n="e" s="T186">bar </ts>
               <ts e="T188" id="Seg_3108" n="e" s="T187">(kudonz-) </ts>
               <ts e="T189" id="Seg_3110" n="e" s="T188">kudonzlaʔbəʔjə. </ts>
               <ts e="T190" id="Seg_3112" n="e" s="T189">Măn </ts>
               <ts e="T743" id="Seg_3114" n="e" s="T190">kalla </ts>
               <ts e="T191" id="Seg_3116" n="e" s="T743">dʼürbiem. </ts>
               <ts e="T193" id="Seg_3118" n="e" s="T191">(-ndəbial). </ts>
               <ts e="T194" id="Seg_3120" n="e" s="T193">Udal </ts>
               <ts e="T195" id="Seg_3122" n="e" s="T194">padʼi </ts>
               <ts e="T196" id="Seg_3124" n="e" s="T195">ĭzemnie. </ts>
               <ts e="T197" id="Seg_3126" n="e" s="T196">Ну, </ts>
               <ts e="T198" id="Seg_3128" n="e" s="T197">все </ts>
               <ts e="T199" id="Seg_3130" n="e" s="T198">наверное. </ts>
               <ts e="T200" id="Seg_3132" n="e" s="T199">((BRK)). </ts>
               <ts e="T201" id="Seg_3134" n="e" s="T200">Tus </ts>
               <ts e="T202" id="Seg_3136" n="e" s="T201">naga. </ts>
               <ts e="T203" id="Seg_3138" n="e" s="T202">Nada </ts>
               <ts e="T204" id="Seg_3140" n="e" s="T203">tustʼarzittə. </ts>
               <ts e="T206" id="Seg_3142" n="e" s="T204">ĭmbizʼiʔ </ts>
               <ts e="T207" id="Seg_3144" n="e" s="T206">amzittə, </ts>
               <ts e="T208" id="Seg_3146" n="e" s="T207">šamnak </ts>
               <ts e="T209" id="Seg_3148" n="e" s="T208">naga. </ts>
               <ts e="T210" id="Seg_3150" n="e" s="T209">Deʔ </ts>
               <ts e="T211" id="Seg_3152" n="e" s="T210">šamnak! </ts>
               <ts e="T212" id="Seg_3154" n="e" s="T211">Dö </ts>
               <ts e="T213" id="Seg_3156" n="e" s="T212">šamnak, </ts>
               <ts e="T214" id="Seg_3158" n="e" s="T213">mĭbiem </ts>
               <ts e="T215" id="Seg_3160" n="e" s="T214">tănan! </ts>
               <ts e="T216" id="Seg_3162" n="e" s="T215">Büžü </ts>
               <ts e="T217" id="Seg_3164" n="e" s="T216">amaʔ! </ts>
               <ts e="T218" id="Seg_3166" n="e" s="T217">Все, </ts>
               <ts e="T219" id="Seg_3168" n="e" s="T218">скорее </ts>
               <ts e="T220" id="Seg_3170" n="e" s="T219">ешь. </ts>
               <ts e="T221" id="Seg_3172" n="e" s="T220">((BRK)). </ts>
               <ts e="T222" id="Seg_3174" n="e" s="T221">Kanžəbəj </ts>
               <ts e="T223" id="Seg_3176" n="e" s="T222">((…)). </ts>
               <ts e="T224" id="Seg_3178" n="e" s="T223">Obberəj. </ts>
               <ts e="T225" id="Seg_3180" n="e" s="T224">Ugaːndə </ts>
               <ts e="T226" id="Seg_3182" n="e" s="T225">kuvas </ts>
               <ts e="T227" id="Seg_3184" n="e" s="T226">koʔbsaŋ. </ts>
               <ts e="T228" id="Seg_3186" n="e" s="T227">Ugaːndə </ts>
               <ts e="T229" id="Seg_3188" n="e" s="T228">kuvas </ts>
               <ts e="T230" id="Seg_3190" n="e" s="T229">nʼizeŋ. </ts>
               <ts e="T231" id="Seg_3192" n="e" s="T230">Ugaːndə </ts>
               <ts e="T232" id="Seg_3194" n="e" s="T231">kuvas </ts>
               <ts e="T233" id="Seg_3196" n="e" s="T232">il. </ts>
               <ts e="T234" id="Seg_3198" n="e" s="T233">Ugaːndə </ts>
               <ts e="T235" id="Seg_3200" n="e" s="T234">jakšə </ts>
               <ts e="T236" id="Seg_3202" n="e" s="T235">kuza. </ts>
               <ts e="T237" id="Seg_3204" n="e" s="T236">Ugaːndə </ts>
               <ts e="T238" id="Seg_3206" n="e" s="T237">šaːmnaʔbə </ts>
               <ts e="T239" id="Seg_3208" n="e" s="T238">kuza. </ts>
               <ts e="T241" id="Seg_3210" n="e" s="T239">(-luʔpial). </ts>
               <ts e="T242" id="Seg_3212" n="e" s="T241">Nada </ts>
               <ts e="T243" id="Seg_3214" n="e" s="T242">kunolzittə. </ts>
            </ts>
            <ts e="T265" id="Seg_3215" n="sc" s="T244">
               <ts e="T245" id="Seg_3217" n="e" s="T244">Idʼiʔeʔe. </ts>
               <ts e="T247" id="Seg_3219" n="e" s="T245">(-tə) </ts>
               <ts e="T248" id="Seg_3221" n="e" s="T247">axota </ts>
               <ts e="T249" id="Seg_3223" n="e" s="T248">ugaːndə. </ts>
               <ts e="T250" id="Seg_3225" n="e" s="T249">Nanəm </ts>
               <ts e="T251" id="Seg_3227" n="e" s="T250">ĭzemnie. </ts>
               <ts e="T252" id="Seg_3229" n="e" s="T251">Amoraʔ, </ts>
               <ts e="T253" id="Seg_3231" n="e" s="T252">amnaʔ! </ts>
               <ts e="T254" id="Seg_3233" n="e" s="T253">Iʔ </ts>
               <ts e="T255" id="Seg_3235" n="e" s="T254">kürümaʔ, </ts>
               <ts e="T256" id="Seg_3237" n="e" s="T255">iʔ </ts>
               <ts e="T257" id="Seg_3239" n="e" s="T256">alomaʔ! </ts>
               <ts e="T259" id="Seg_3241" n="e" s="T257">nada </ts>
               <ts e="T260" id="Seg_3243" n="e" s="T259">izittə. </ts>
               <ts e="T261" id="Seg_3245" n="e" s="T260">A_to </ts>
               <ts e="T262" id="Seg_3247" n="e" s="T261">((DMG)). </ts>
               <ts e="T265" id="Seg_3249" n="e" s="T262">И еще чего? </ts>
            </ts>
            <ts e="T312" id="Seg_3250" n="sc" s="T268">
               <ts e="T741" id="Seg_3252" n="e" s="T268">((DMG)) </ts>
               <ts e="T269" id="Seg_3254" n="e" s="T741">Dʼijenə </ts>
               <ts e="T270" id="Seg_3256" n="e" s="T269">kanzittə, </ts>
               <ts e="T271" id="Seg_3258" n="e" s="T270">keʔbde </ts>
               <ts e="T272" id="Seg_3260" n="e" s="T271">deʔsʼittə. </ts>
               <ts e="T273" id="Seg_3262" n="e" s="T272">Sanə </ts>
               <ts e="T274" id="Seg_3264" n="e" s="T273">deʔsittə. </ts>
               <ts e="T275" id="Seg_3266" n="e" s="T274">Büžü </ts>
               <ts e="T276" id="Seg_3268" n="e" s="T275">kanžəbəj </ts>
               <ts e="T277" id="Seg_3270" n="e" s="T276">šidegöʔ. </ts>
               <ts e="T279" id="Seg_3272" n="e" s="T277">(-ləm). </ts>
               <ts e="T280" id="Seg_3274" n="e" s="T279">Ĭmbi-nʼibudʼ </ts>
               <ts e="T281" id="Seg_3276" n="e" s="T280">kutlam </ts>
               <ts e="T282" id="Seg_3278" n="e" s="T281">(dĭm). </ts>
               <ts e="T283" id="Seg_3280" n="e" s="T282">Poʔto </ts>
               <ts e="T284" id="Seg_3282" n="e" s="T283">kuʔpiom </ts>
               <ts e="T285" id="Seg_3284" n="e" s="T284">bar. </ts>
               <ts e="T286" id="Seg_3286" n="e" s="T285">((BRK)). </ts>
               <ts e="T287" id="Seg_3288" n="e" s="T286">Albuga </ts>
               <ts e="T288" id="Seg_3290" n="e" s="T287">kuʔpiom. </ts>
               <ts e="T289" id="Seg_3292" n="e" s="T288">Tažəp </ts>
               <ts e="T290" id="Seg_3294" n="e" s="T289">kuʔpiom. </ts>
               <ts e="T291" id="Seg_3296" n="e" s="T290">(Sön) </ts>
               <ts e="T292" id="Seg_3298" n="e" s="T291">kuʔpiam. </ts>
               <ts e="T293" id="Seg_3300" n="e" s="T292">Urgaːba </ts>
               <ts e="T294" id="Seg_3302" n="e" s="T293">kuʔpiam. </ts>
               <ts e="T295" id="Seg_3304" n="e" s="T294">Все, </ts>
               <ts e="T296" id="Seg_3306" n="e" s="T295">хватит. </ts>
               <ts e="T298" id="Seg_3308" n="e" s="T296"> (-m) </ts>
               <ts e="T299" id="Seg_3310" n="e" s="T298">ĭzemnie </ts>
               <ts e="T300" id="Seg_3312" n="e" s="T299">ugaːndə. </ts>
               <ts e="T301" id="Seg_3314" n="e" s="T300">Udam </ts>
               <ts e="T302" id="Seg_3316" n="e" s="T301">ĭzemnie, </ts>
               <ts e="T303" id="Seg_3318" n="e" s="T302">ulum </ts>
               <ts e="T304" id="Seg_3320" n="e" s="T303">ĭzemnie. </ts>
               <ts e="T305" id="Seg_3322" n="e" s="T304">Bögəlbə </ts>
               <ts e="T306" id="Seg_3324" n="e" s="T305">ĭzemnie. </ts>
               <ts e="T307" id="Seg_3326" n="e" s="T306">Üjüzeŋdə </ts>
               <ts e="T308" id="Seg_3328" n="e" s="T307">ĭzemnie. </ts>
               <ts e="T309" id="Seg_3330" n="e" s="T308">((BRK)). </ts>
               <ts e="T312" id="Seg_3332" n="e" s="T309">Ладно уже, хватит. </ts>
            </ts>
            <ts e="T350" id="Seg_3333" n="sc" s="T315">
               <ts e="T320" id="Seg_3335" n="e" s="T315">Так я уже позабыла опять. </ts>
               <ts e="T321" id="Seg_3337" n="e" s="T320">Tüʔsittə </ts>
               <ts e="T322" id="Seg_3339" n="e" s="T321">kanzittə, </ts>
               <ts e="T323" id="Seg_3341" n="e" s="T322">kĭnzəsʼtə </ts>
               <ts e="T324" id="Seg_3343" n="e" s="T323">nada </ts>
               <ts e="T325" id="Seg_3345" n="e" s="T324">kanzittə. </ts>
               <ts e="T327" id="Seg_3347" n="e" s="T325">Nada… </ts>
               <ts e="T328" id="Seg_3349" n="e" s="T327">((BRK)). </ts>
               <ts e="T330" id="Seg_3351" n="e" s="T328"> Старик </ts>
               <ts e="T331" id="Seg_3353" n="e" s="T330">со </ts>
               <ts e="T332" id="Seg_3355" n="e" s="T331">старухой, </ts>
               <ts e="T333" id="Seg_3357" n="e" s="T332">у </ts>
               <ts e="T334" id="Seg_3359" n="e" s="T333">них </ts>
               <ts e="T335" id="Seg_3361" n="e" s="T334">были </ts>
               <ts e="T336" id="Seg_3363" n="e" s="T335">мальчик </ts>
               <ts e="T337" id="Seg_3365" n="e" s="T336">и </ts>
               <ts e="T338" id="Seg_3367" n="e" s="T337">девочка. </ts>
               <ts e="T340" id="Seg_3369" n="e" s="T338"> (-ʔi) </ts>
               <ts e="T341" id="Seg_3371" n="e" s="T340">nüke </ts>
               <ts e="T342" id="Seg_3373" n="e" s="T341">i </ts>
               <ts e="T343" id="Seg_3375" n="e" s="T342">büzʼe. </ts>
               <ts e="T344" id="Seg_3377" n="e" s="T343">Dĭzeŋ </ts>
               <ts e="T345" id="Seg_3379" n="e" s="T344">(eks-) </ts>
               <ts e="T346" id="Seg_3381" n="e" s="T345">esseŋdə </ts>
               <ts e="T347" id="Seg_3383" n="e" s="T346">ibiʔi. </ts>
               <ts e="T348" id="Seg_3385" n="e" s="T347">Nʼi </ts>
               <ts e="T349" id="Seg_3387" n="e" s="T348">i </ts>
               <ts e="T350" id="Seg_3389" n="e" s="T349">koʔbdo. </ts>
            </ts>
            <ts e="T396" id="Seg_3390" n="sc" s="T355">
               <ts e="T356" id="Seg_3392" n="e" s="T355">Amnobiʔi </ts>
               <ts e="T357" id="Seg_3394" n="e" s="T356">nüke </ts>
               <ts e="T358" id="Seg_3396" n="e" s="T357">i </ts>
               <ts e="T359" id="Seg_3398" n="e" s="T358">büzʼe. </ts>
               <ts e="T360" id="Seg_3400" n="e" s="T359">Dĭzeŋdə </ts>
               <ts e="T361" id="Seg_3402" n="e" s="T360">esseŋdə </ts>
               <ts e="T362" id="Seg_3404" n="e" s="T361">ibiʔi. </ts>
               <ts e="T363" id="Seg_3406" n="e" s="T362">Nʼit </ts>
               <ts e="T364" id="Seg_3408" n="e" s="T363">i </ts>
               <ts e="T365" id="Seg_3410" n="e" s="T364">koʔbdo. </ts>
               <ts e="T366" id="Seg_3412" n="e" s="T365">Nʼi </ts>
               <ts e="T367" id="Seg_3414" n="e" s="T366">i </ts>
               <ts e="T368" id="Seg_3416" n="e" s="T367">koʔbdo. </ts>
               <ts e="T369" id="Seg_3418" n="e" s="T368">Oʔb, </ts>
               <ts e="T370" id="Seg_3420" n="e" s="T369">šide, </ts>
               <ts e="T371" id="Seg_3422" n="e" s="T370">nagur, </ts>
               <ts e="T372" id="Seg_3424" n="e" s="T371">sumna, </ts>
               <ts e="T373" id="Seg_3426" n="e" s="T372">muktuʔ, </ts>
               <ts e="T374" id="Seg_3428" n="e" s="T373">sejʔpü, </ts>
               <ts e="T375" id="Seg_3430" n="e" s="T374">šinteʔtə, </ts>
               <ts e="T376" id="Seg_3432" n="e" s="T375">amitun, </ts>
               <ts e="T377" id="Seg_3434" n="e" s="T376">bʼeʔ, </ts>
               <ts e="T378" id="Seg_3436" n="e" s="T377">bʼeʔ </ts>
               <ts e="T379" id="Seg_3438" n="e" s="T378">onʼiʔ, </ts>
               <ts e="T380" id="Seg_3440" n="e" s="T379">bʼeʔ </ts>
               <ts e="T381" id="Seg_3442" n="e" s="T380">šide, </ts>
               <ts e="T382" id="Seg_3444" n="e" s="T381">bʼeʔ </ts>
               <ts e="T383" id="Seg_3446" n="e" s="T382">nagur, </ts>
               <ts e="T384" id="Seg_3448" n="e" s="T383">bʼeʔ </ts>
               <ts e="T385" id="Seg_3450" n="e" s="T384">teʔtə, </ts>
               <ts e="T386" id="Seg_3452" n="e" s="T385">bʼeʔ </ts>
               <ts e="T387" id="Seg_3454" n="e" s="T386">sumna, </ts>
               <ts e="T388" id="Seg_3456" n="e" s="T387">bʼeʔ </ts>
               <ts e="T389" id="Seg_3458" n="e" s="T388">muktuʔ, </ts>
               <ts e="T390" id="Seg_3460" n="e" s="T389">bʼeʔ </ts>
               <ts e="T391" id="Seg_3462" n="e" s="T390">sejʔpü, </ts>
               <ts e="T392" id="Seg_3464" n="e" s="T391">bʼeʔ </ts>
               <ts e="T393" id="Seg_3466" n="e" s="T392">šinteʔtə, </ts>
               <ts e="T395" id="Seg_3468" n="e" s="T393">bʼeʔ… </ts>
               <ts e="T396" id="Seg_3470" n="e" s="T395">Забыла. </ts>
            </ts>
            <ts e="T494" id="Seg_3471" n="sc" s="T418">
               <ts e="T419" id="Seg_3473" n="e" s="T418">Oʔb, </ts>
               <ts e="T420" id="Seg_3475" n="e" s="T419">šide, </ts>
               <ts e="T421" id="Seg_3477" n="e" s="T420">nagur, </ts>
               <ts e="T422" id="Seg_3479" n="e" s="T421">(muktuʔ), </ts>
               <ts e="T423" id="Seg_3481" n="e" s="T422">(sejʔpü), </ts>
               <ts e="T424" id="Seg_3483" n="e" s="T423">šinteʔtə, </ts>
               <ts e="T425" id="Seg_3485" n="e" s="T424">amitun. </ts>
               <ts e="T426" id="Seg_3487" n="e" s="T425">((BRK)). </ts>
               <ts e="T427" id="Seg_3489" n="e" s="T426">"Măna </ts>
               <ts e="T428" id="Seg_3491" n="e" s="T427">nada </ts>
               <ts e="T429" id="Seg_3493" n="e" s="T428">monoʔkozittə, </ts>
               <ts e="T430" id="Seg_3495" n="e" s="T429">a </ts>
               <ts e="T431" id="Seg_3497" n="e" s="T430">šindim? </ts>
               <ts e="T432" id="Seg_3499" n="e" s="T431">Dĭ </ts>
               <ts e="T433" id="Seg_3501" n="e" s="T432">koʔbdo. </ts>
               <ts e="T434" id="Seg_3503" n="e" s="T433">Kanaʔ </ts>
               <ts e="T435" id="Seg_3505" n="e" s="T434">monoʔkot! </ts>
               <ts e="T436" id="Seg_3507" n="e" s="T435">Всё. </ts>
               <ts e="T438" id="Seg_3509" n="e" s="T436">(-təbtə) </ts>
               <ts e="T439" id="Seg_3511" n="e" s="T438">turagən </ts>
               <ts e="T440" id="Seg_3513" n="e" s="T439">amnolaʔbəm. </ts>
               <ts e="T441" id="Seg_3515" n="e" s="T440">Oj </ts>
               <ts e="T442" id="Seg_3517" n="e" s="T441">bar </ts>
               <ts e="T443" id="Seg_3519" n="e" s="T442">üjüm </ts>
               <ts e="T444" id="Seg_3521" n="e" s="T443">kutʼümnie, </ts>
               <ts e="T446" id="Seg_3523" n="e" s="T444">nada… </ts>
               <ts e="T447" id="Seg_3525" n="e" s="T446">((BRK)). </ts>
               <ts e="T448" id="Seg_3527" n="e" s="T447">Ulum </ts>
               <ts e="T449" id="Seg_3529" n="e" s="T448">ĭzemnie. </ts>
               <ts e="T450" id="Seg_3531" n="e" s="T449">Bögelbə </ts>
               <ts e="T451" id="Seg_3533" n="e" s="T450">ĭzemnie. </ts>
               <ts e="T452" id="Seg_3535" n="e" s="T451">Udam </ts>
               <ts e="T453" id="Seg_3537" n="e" s="T452">ĭzemnie. </ts>
               <ts e="T454" id="Seg_3539" n="e" s="T453">Sʼimam </ts>
               <ts e="T455" id="Seg_3541" n="e" s="T454">ĭzemnie. </ts>
               <ts e="T456" id="Seg_3543" n="e" s="T455">Ku </ts>
               <ts e="T457" id="Seg_3545" n="e" s="T456">ĭzemnie. </ts>
               <ts e="T458" id="Seg_3547" n="e" s="T457">Üjüm </ts>
               <ts e="T459" id="Seg_3549" n="e" s="T458">ĭzemnie. </ts>
               <ts e="T460" id="Seg_3551" n="e" s="T459">Kötenbə </ts>
               <ts e="T461" id="Seg_3553" n="e" s="T460">ĭzemnie. </ts>
               <ts e="T462" id="Seg_3555" n="e" s="T461">((BRK)). </ts>
               <ts e="T463" id="Seg_3557" n="e" s="T462">(Dʼok). </ts>
               <ts e="T464" id="Seg_3559" n="e" s="T463">((BRK)). </ts>
               <ts e="T465" id="Seg_3561" n="e" s="T464">Dʼijenə </ts>
               <ts e="T466" id="Seg_3563" n="e" s="T465">nada </ts>
               <ts e="T467" id="Seg_3565" n="e" s="T466">kanzittə, </ts>
               <ts e="T468" id="Seg_3567" n="e" s="T467">albuga </ts>
               <ts e="T469" id="Seg_3569" n="e" s="T468">kuʔsittə. </ts>
               <ts e="T470" id="Seg_3571" n="e" s="T469">Urgaːba </ts>
               <ts e="T471" id="Seg_3573" n="e" s="T470">kuʔsittə </ts>
               <ts e="T472" id="Seg_3575" n="e" s="T471">nada. </ts>
               <ts e="T473" id="Seg_3577" n="e" s="T472">Tažəp </ts>
               <ts e="T474" id="Seg_3579" n="e" s="T473">kuʔsittə </ts>
               <ts e="T475" id="Seg_3581" n="e" s="T474">nada. </ts>
               <ts e="T476" id="Seg_3583" n="e" s="T475">Bulan </ts>
               <ts e="T477" id="Seg_3585" n="e" s="T476">kuʔsittə </ts>
               <ts e="T478" id="Seg_3587" n="e" s="T477">nada. </ts>
               <ts e="T479" id="Seg_3589" n="e" s="T478">Sön </ts>
               <ts e="T480" id="Seg_3591" n="e" s="T479">kuʔsittə </ts>
               <ts e="T481" id="Seg_3593" n="e" s="T480">nada. </ts>
               <ts e="T482" id="Seg_3595" n="e" s="T481">Poʔto </ts>
               <ts e="T483" id="Seg_3597" n="e" s="T482">nada </ts>
               <ts e="T484" id="Seg_3599" n="e" s="T483">kuʔsittə. </ts>
               <ts e="T485" id="Seg_3601" n="e" s="T484">Uja </ts>
               <ts e="T486" id="Seg_3603" n="e" s="T485">iʔgö </ts>
               <ts e="T487" id="Seg_3605" n="e" s="T486">moləj, </ts>
               <ts e="T488" id="Seg_3607" n="e" s="T487">amzittə </ts>
               <ts e="T489" id="Seg_3609" n="e" s="T488">nada. </ts>
               <ts e="T490" id="Seg_3611" n="e" s="T489">(Mĭnzitən- </ts>
               <ts e="T491" id="Seg_3613" n="e" s="T490">mĭrd- </ts>
               <ts e="T492" id="Seg_3615" n="e" s="T491">mĭrzintə-) </ts>
               <ts e="T493" id="Seg_3617" n="e" s="T492">nada </ts>
               <ts e="T494" id="Seg_3619" n="e" s="T493">((LAUGH)). </ts>
            </ts>
            <ts e="T502" id="Seg_3620" n="sc" s="T495">
               <ts e="T496" id="Seg_3622" n="e" s="T495">(Uja=) </ts>
               <ts e="T497" id="Seg_3624" n="e" s="T496">Uja </ts>
               <ts e="T498" id="Seg_3626" n="e" s="T497">(eneʔ), </ts>
               <ts e="T499" id="Seg_3628" n="e" s="T498">šü </ts>
               <ts e="T500" id="Seg_3630" n="e" s="T499">nendleʔbə, </ts>
               <ts e="T501" id="Seg_3632" n="e" s="T500">uja </ts>
               <ts e="T502" id="Seg_3634" n="e" s="T501">(eneʔ). </ts>
            </ts>
            <ts e="T507" id="Seg_3635" n="sc" s="T504">
               <ts e="T507" id="Seg_3637" n="e" s="T504">Кого еще раз? </ts>
            </ts>
            <ts e="T571" id="Seg_3638" n="sc" s="T514">
               <ts e="T517" id="Seg_3640" n="e" s="T514">А-а. </ts>
               <ts e="T518" id="Seg_3642" n="e" s="T517">Šü </ts>
               <ts e="T519" id="Seg_3644" n="e" s="T518">nendleʔbə, </ts>
               <ts e="T520" id="Seg_3646" n="e" s="T519">uja </ts>
               <ts e="T521" id="Seg_3648" n="e" s="T520">eneʔ. </ts>
               <ts e="T522" id="Seg_3650" n="e" s="T521">(Puska-) </ts>
               <ts e="T523" id="Seg_3652" n="e" s="T522">Puskaj </ts>
               <ts e="T524" id="Seg_3654" n="e" s="T523">pürleʔbə. </ts>
               <ts e="T525" id="Seg_3656" n="e" s="T524">((BRK)). </ts>
               <ts e="T526" id="Seg_3658" n="e" s="T525">(Šünə=) </ts>
               <ts e="T527" id="Seg_3660" n="e" s="T526">Šünə </ts>
               <ts e="T528" id="Seg_3662" n="e" s="T527">endleʔbə </ts>
               <ts e="T529" id="Seg_3664" n="e" s="T528">uja. </ts>
               <ts e="T530" id="Seg_3666" n="e" s="T529">((BRK)). </ts>
               <ts e="T531" id="Seg_3668" n="e" s="T530">Amnaʔ </ts>
               <ts e="T532" id="Seg_3670" n="e" s="T531">döbər, </ts>
               <ts e="T533" id="Seg_3672" n="e" s="T532">dʼăbaktəraʔ, </ts>
               <ts e="T534" id="Seg_3674" n="e" s="T533">gijen </ts>
               <ts e="T535" id="Seg_3676" n="e" s="T534">ibiel. </ts>
               <ts e="T536" id="Seg_3678" n="e" s="T535">(Ĭmbi=) </ts>
               <ts e="T537" id="Seg_3680" n="e" s="T536">Ĭmbi </ts>
               <ts e="T538" id="Seg_3682" n="e" s="T537">kubial? </ts>
               <ts e="T539" id="Seg_3684" n="e" s="T538">Gibər </ts>
               <ts e="T540" id="Seg_3686" n="e" s="T539">kandəgal? </ts>
               <ts e="T541" id="Seg_3688" n="e" s="T540">Dʼijenə </ts>
               <ts e="T542" id="Seg_3690" n="e" s="T541">kandəgam, </ts>
               <ts e="T543" id="Seg_3692" n="e" s="T542">keʔbdejleʔ. </ts>
               <ts e="T544" id="Seg_3694" n="e" s="T543">Dĭn </ts>
               <ts e="T545" id="Seg_3696" n="e" s="T544">urgaːba. </ts>
               <ts e="T546" id="Seg_3698" n="e" s="T545">Ugaːndə </ts>
               <ts e="T547" id="Seg_3700" n="e" s="T546">pimniem. </ts>
               <ts e="T548" id="Seg_3702" n="e" s="T547">No </ts>
               <ts e="T549" id="Seg_3704" n="e" s="T548">iʔ </ts>
               <ts e="T550" id="Seg_3706" n="e" s="T549">kanaʔ, </ts>
               <ts e="T551" id="Seg_3708" n="e" s="T550">pimniel </ts>
               <ts e="T552" id="Seg_3710" n="e" s="T551">dăk. </ts>
               <ts e="T554" id="Seg_3712" n="e" s="T552">Ну хватит. </ts>
               <ts e="T555" id="Seg_3714" n="e" s="T554">Oʔb, </ts>
               <ts e="T556" id="Seg_3716" n="e" s="T555">šide, </ts>
               <ts e="T557" id="Seg_3718" n="e" s="T556">nagur, </ts>
               <ts e="T558" id="Seg_3720" n="e" s="T557">teʔtə, </ts>
               <ts e="T559" id="Seg_3722" n="e" s="T558">sumna, </ts>
               <ts e="T560" id="Seg_3724" n="e" s="T559">sejʔpü, </ts>
               <ts e="T561" id="Seg_3726" n="e" s="T560">(šin-) </ts>
               <ts e="T562" id="Seg_3728" n="e" s="T561">sejʔpü. </ts>
               <ts e="T563" id="Seg_3730" n="e" s="T562">Oʔb, </ts>
               <ts e="T564" id="Seg_3732" n="e" s="T563">šide, </ts>
               <ts e="T565" id="Seg_3734" n="e" s="T564">nagur, </ts>
               <ts e="T566" id="Seg_3736" n="e" s="T565">muktuʔ, </ts>
               <ts e="T567" id="Seg_3738" n="e" s="T566">sejʔpü, </ts>
               <ts e="T568" id="Seg_3740" n="e" s="T567">sumna, </ts>
               <ts e="T569" id="Seg_3742" n="e" s="T568">šinteʔtə, </ts>
               <ts e="T570" id="Seg_3744" n="e" s="T569">amitun, </ts>
               <ts e="T571" id="Seg_3746" n="e" s="T570">bʼeʔ. </ts>
            </ts>
            <ts e="T612" id="Seg_3747" n="sc" s="T573">
               <ts e="T574" id="Seg_3749" n="e" s="T573">Bʼeʔ </ts>
               <ts e="T575" id="Seg_3751" n="e" s="T574">onʼiʔ, </ts>
               <ts e="T576" id="Seg_3753" n="e" s="T575">bʼeʔ </ts>
               <ts e="T577" id="Seg_3755" n="e" s="T576">šide, </ts>
               <ts e="T578" id="Seg_3757" n="e" s="T577">bʼeʔ </ts>
               <ts e="T579" id="Seg_3759" n="e" s="T578">nagur, </ts>
               <ts e="T580" id="Seg_3761" n="e" s="T579">bʼeʔ </ts>
               <ts e="T581" id="Seg_3763" n="e" s="T580">teʔtə, </ts>
               <ts e="T582" id="Seg_3765" n="e" s="T581">bʼeʔ </ts>
               <ts e="T583" id="Seg_3767" n="e" s="T582">sumna, </ts>
               <ts e="T584" id="Seg_3769" n="e" s="T583">bʼeʔ </ts>
               <ts e="T585" id="Seg_3771" n="e" s="T584">sejʔpü. </ts>
               <ts e="T586" id="Seg_3773" n="e" s="T585">Bʼeʔ </ts>
               <ts e="T587" id="Seg_3775" n="e" s="T586">šinteʔtə, </ts>
               <ts e="T588" id="Seg_3777" n="e" s="T587">bʼeʔ </ts>
               <ts e="T589" id="Seg_3779" n="e" s="T588">sejʔpü, </ts>
               <ts e="T590" id="Seg_3781" n="e" s="T589">bʼeʔ. </ts>
               <ts e="T591" id="Seg_3783" n="e" s="T590">Šide </ts>
               <ts e="T592" id="Seg_3785" n="e" s="T591">(bʼeʔ). </ts>
               <ts e="T595" id="Seg_3787" n="e" s="T592">Забыла я ((BRK))… </ts>
               <ts e="T596" id="Seg_3789" n="e" s="T595">Surdəsʼtə </ts>
               <ts e="T597" id="Seg_3791" n="e" s="T596">nada </ts>
               <ts e="T598" id="Seg_3793" n="e" s="T597">tüžöjəm. </ts>
               <ts e="T599" id="Seg_3795" n="e" s="T598">Šedendə </ts>
               <ts e="T600" id="Seg_3797" n="e" s="T599">kajzittə </ts>
               <ts e="T601" id="Seg_3799" n="e" s="T600">nada. </ts>
               <ts e="T602" id="Seg_3801" n="e" s="T601">Surdəsʼtə </ts>
               <ts e="T603" id="Seg_3803" n="e" s="T602">nada. </ts>
               <ts e="T604" id="Seg_3805" n="e" s="T603">Süt </ts>
               <ts e="T605" id="Seg_3807" n="e" s="T604">detsittə. </ts>
               <ts e="T606" id="Seg_3809" n="e" s="T605">Šojdʼonə </ts>
               <ts e="T607" id="Seg_3811" n="e" s="T606">enzittə, </ts>
               <ts e="T608" id="Seg_3813" n="e" s="T607">kămnasʼtə. </ts>
               <ts e="T609" id="Seg_3815" n="e" s="T608">Ну, </ts>
               <ts e="T610" id="Seg_3817" n="e" s="T609">все. </ts>
               <ts e="T611" id="Seg_3819" n="e" s="T610">((BRK)). </ts>
               <ts e="T612" id="Seg_3821" n="e" s="T611">Ну? </ts>
            </ts>
            <ts e="T620" id="Seg_3822" n="sc" s="T614">
               <ts e="T615" id="Seg_3824" n="e" s="T614">Gibər </ts>
               <ts e="T616" id="Seg_3826" n="e" s="T615">kandəgal? </ts>
               <ts e="T617" id="Seg_3828" n="e" s="T616">Gibər </ts>
               <ts e="T618" id="Seg_3830" n="e" s="T617">nuʔməleʔbəl? </ts>
               <ts e="T619" id="Seg_3832" n="e" s="T618">(Tüžöjmə) </ts>
               <ts e="T620" id="Seg_3834" n="e" s="T619">naga. </ts>
            </ts>
            <ts e="T631" id="Seg_3835" n="sc" s="T621">
               <ts e="T622" id="Seg_3837" n="e" s="T621">Măndərzittə </ts>
               <ts e="T623" id="Seg_3839" n="e" s="T622">(šonə-) </ts>
               <ts e="T624" id="Seg_3841" n="e" s="T623">kalam. </ts>
               <ts e="T625" id="Seg_3843" n="e" s="T624">Kanžəbəj </ts>
               <ts e="T626" id="Seg_3845" n="e" s="T625">mănzʼiʔ! </ts>
               <ts e="T627" id="Seg_3847" n="e" s="T626">A_to </ts>
               <ts e="T628" id="Seg_3849" n="e" s="T627">măn </ts>
               <ts e="T629" id="Seg_3851" n="e" s="T628">pimniem. </ts>
               <ts e="T631" id="Seg_3853" n="e" s="T629">Мало, однако. </ts>
            </ts>
            <ts e="T706" id="Seg_3854" n="sc" s="T634">
               <ts e="T635" id="Seg_3856" n="e" s="T634">Zdărowă </ts>
               <ts e="T636" id="Seg_3858" n="e" s="T635">igel! </ts>
               <ts e="T637" id="Seg_3860" n="e" s="T636">Amnaʔ </ts>
               <ts e="T638" id="Seg_3862" n="e" s="T637">amorzittə! </ts>
               <ts e="T639" id="Seg_3864" n="e" s="T638">Ipek </ts>
               <ts e="T640" id="Seg_3866" n="e" s="T639">lem </ts>
               <ts e="T641" id="Seg_3868" n="e" s="T640">keʔbdezʼiʔ. </ts>
               <ts e="T642" id="Seg_3870" n="e" s="T641">Kajaʔ. </ts>
               <ts e="T643" id="Seg_3872" n="e" s="T642">Uja </ts>
               <ts e="T644" id="Seg_3874" n="e" s="T643">amaʔ! </ts>
               <ts e="T645" id="Seg_3876" n="e" s="T644">Keʔbde </ts>
               <ts e="T646" id="Seg_3878" n="e" s="T645">amaʔ! </ts>
               <ts e="T647" id="Seg_3880" n="e" s="T646">Oroma </ts>
               <ts e="T648" id="Seg_3882" n="e" s="T647">amaʔ! </ts>
               <ts e="T649" id="Seg_3884" n="e" s="T648">((BRK)). </ts>
               <ts e="T650" id="Seg_3886" n="e" s="T649">Ipek </ts>
               <ts e="T651" id="Seg_3888" n="e" s="T650">amaʔ! </ts>
               <ts e="T652" id="Seg_3890" n="e" s="T651">Segi </ts>
               <ts e="T653" id="Seg_3892" n="e" s="T652">bü </ts>
               <ts e="T654" id="Seg_3894" n="e" s="T653">bĭdeʔ! </ts>
               <ts e="T655" id="Seg_3896" n="e" s="T654">Sĭreʔpne </ts>
               <ts e="T656" id="Seg_3898" n="e" s="T655">endə </ts>
               <ts e="T657" id="Seg_3900" n="e" s="T656">segi </ts>
               <ts e="T658" id="Seg_3902" n="e" s="T657">bünə. </ts>
               <ts e="T659" id="Seg_3904" n="e" s="T658">Süt </ts>
               <ts e="T660" id="Seg_3906" n="e" s="T659">amnaʔ! </ts>
               <ts e="T661" id="Seg_3908" n="e" s="T660">((BRK)). </ts>
               <ts e="T662" id="Seg_3910" n="e" s="T661">Kanžəbəj </ts>
               <ts e="T663" id="Seg_3912" n="e" s="T662">obberəj </ts>
               <ts e="T664" id="Seg_3914" n="e" s="T663">dʼijenə </ts>
               <ts e="T665" id="Seg_3916" n="e" s="T664">keʔbde </ts>
               <ts e="T666" id="Seg_3918" n="e" s="T665">oʔbdəsʼtə. </ts>
               <ts e="T667" id="Seg_3920" n="e" s="T666">Kalba </ts>
               <ts e="T668" id="Seg_3922" n="e" s="T667">nĭŋgəsʼtə, </ts>
               <ts e="T669" id="Seg_3924" n="e" s="T668">sanə </ts>
               <ts e="T670" id="Seg_3926" n="e" s="T669">azittə. </ts>
               <ts e="T671" id="Seg_3928" n="e" s="T670">Iššo </ts>
               <ts e="T672" id="Seg_3930" n="e" s="T671">ĭmbi? </ts>
               <ts e="T673" id="Seg_3932" n="e" s="T672">(Beške </ts>
               <ts e="T674" id="Seg_3934" n="e" s="T673">i- </ts>
               <ts e="T675" id="Seg_3936" n="e" s="T674">i- </ts>
               <ts e="T676" id="Seg_3938" n="e" s="T675">a-) </ts>
               <ts e="T677" id="Seg_3940" n="e" s="T676">Beške </ts>
               <ts e="T678" id="Seg_3942" n="e" s="T677">izittə. </ts>
               <ts e="T679" id="Seg_3944" n="e" s="T678">((LAUGH)) ((BRK)). </ts>
               <ts e="T682" id="Seg_3946" n="e" s="T679">(Я) тебя жалею. </ts>
               <ts e="T683" id="Seg_3948" n="e" s="T682">Măn </ts>
               <ts e="T684" id="Seg_3950" n="e" s="T683">tănan </ts>
               <ts e="T685" id="Seg_3952" n="e" s="T684">ajirlaʔbəm. </ts>
               <ts e="T686" id="Seg_3954" n="e" s="T685">((BRK)). </ts>
               <ts e="T688" id="Seg_3956" n="e" s="T686">Коня запрягай. </ts>
               <ts e="T689" id="Seg_3958" n="e" s="T688">Kanaʔ, </ts>
               <ts e="T690" id="Seg_3960" n="e" s="T689">ine </ts>
               <ts e="T691" id="Seg_3962" n="e" s="T690">kürereʔ! </ts>
               <ts e="T692" id="Seg_3964" n="e" s="T691">Kanžəbəj </ts>
               <ts e="T693" id="Seg_3966" n="e" s="T692">gibər-nʼibudʼ </ts>
               <ts e="T694" id="Seg_3968" n="e" s="T693">inezʼiʔ. </ts>
               <ts e="T695" id="Seg_3970" n="e" s="T694">(Bü) </ts>
               <ts e="T696" id="Seg_3972" n="e" s="T695">((DMG)) (de-) </ts>
               <ts e="T697" id="Seg_3974" n="e" s="T696">detləbəj. </ts>
               <ts e="T698" id="Seg_3976" n="e" s="T697">Paʔi </ts>
               <ts e="T699" id="Seg_3978" n="e" s="T698">detləbeʔ. </ts>
               <ts e="T700" id="Seg_3980" n="e" s="T699">Noʔ </ts>
               <ts e="T701" id="Seg_3982" n="e" s="T700">detləbəj. </ts>
               <ts e="T702" id="Seg_3984" n="e" s="T701">Săloma </ts>
               <ts e="T703" id="Seg_3986" n="e" s="T702">detləbəj. </ts>
               <ts e="T704" id="Seg_3988" n="e" s="T703">Все, </ts>
               <ts e="T705" id="Seg_3990" n="e" s="T704">наверное. </ts>
               <ts e="T706" id="Seg_3992" n="e" s="T705">((BRK)). </ts>
            </ts>
            <ts e="T739" id="Seg_3993" n="sc" s="T708">
               <ts e="T709" id="Seg_3995" n="e" s="T708">Tuganbə </ts>
               <ts e="T710" id="Seg_3997" n="e" s="T709">šobi. </ts>
               <ts e="T711" id="Seg_3999" n="e" s="T710">Nada </ts>
               <ts e="T712" id="Seg_4001" n="e" s="T711">(m-) </ts>
               <ts e="T713" id="Seg_4003" n="e" s="T712">măndərzittə </ts>
               <ts e="T714" id="Seg_4005" n="e" s="T713">kanzittə. </ts>
               <ts e="T715" id="Seg_4007" n="e" s="T714">((BRK)). </ts>
               <ts e="T716" id="Seg_4009" n="e" s="T715">Nada </ts>
               <ts e="T717" id="Seg_4011" n="e" s="T716">kăštəsʼtə. </ts>
               <ts e="T718" id="Seg_4013" n="e" s="T717">Amorzittə </ts>
               <ts e="T719" id="Seg_4015" n="e" s="T718">mĭzittə. </ts>
               <ts e="T720" id="Seg_4017" n="e" s="T719">Nada </ts>
               <ts e="T721" id="Seg_4019" n="e" s="T720">kunolzittə </ts>
               <ts e="T722" id="Seg_4021" n="e" s="T721">dĭzeŋdə. </ts>
               <ts e="T724" id="Seg_4023" n="e" s="T722">Больше все. </ts>
               <ts e="T725" id="Seg_4025" n="e" s="T724">((BRK)) Oʔb, </ts>
               <ts e="T726" id="Seg_4027" n="e" s="T725">oʔb, </ts>
               <ts e="T727" id="Seg_4029" n="e" s="T726">šide, </ts>
               <ts e="T728" id="Seg_4031" n="e" s="T727">nagur, </ts>
               <ts e="T729" id="Seg_4033" n="e" s="T728">teʔtə, </ts>
               <ts e="T730" id="Seg_4035" n="e" s="T729">sumna, </ts>
               <ts e="T731" id="Seg_4037" n="e" s="T730">sejʔpü, </ts>
               <ts e="T732" id="Seg_4039" n="e" s="T731">amitun, </ts>
               <ts e="T733" id="Seg_4041" n="e" s="T732">šinteʔtə, </ts>
               <ts e="T734" id="Seg_4043" n="e" s="T733">bʼeʔ. </ts>
               <ts e="T739" id="Seg_4045" n="e" s="T734">Вот девять-то оставила ((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T9" id="Seg_4046" s="T2">PKZ_1964_SU0204.PKZ.001 (002)</ta>
            <ta e="T15" id="Seg_4047" s="T14">PKZ_1964_SU0204.PKZ.002 (004)</ta>
            <ta e="T19" id="Seg_4048" s="T17">PKZ_1964_SU0204.PKZ.003 (006)</ta>
            <ta e="T25" id="Seg_4049" s="T22">PKZ_1964_SU0204.PKZ.004 (008)</ta>
            <ta e="T30" id="Seg_4050" s="T27">PKZ_1964_SU0204.PKZ.005 (010)</ta>
            <ta e="T32" id="Seg_4051" s="T30">PKZ_1964_SU0204.PKZ.006 (011)</ta>
            <ta e="T39" id="Seg_4052" s="T36">PKZ_1964_SU0204.PKZ.007 (014)</ta>
            <ta e="T40" id="Seg_4053" s="T39">PKZ_1964_SU0204.PKZ.008 (015)</ta>
            <ta e="T44" id="Seg_4054" s="T40">PKZ_1964_SU0204.PKZ.009 (016)</ta>
            <ta e="T46" id="Seg_4055" s="T44">PKZ_1964_SU0204.PKZ.010 (017)</ta>
            <ta e="T51" id="Seg_4056" s="T47">PKZ_1964_SU0204.PKZ.011 (019)</ta>
            <ta e="T54" id="Seg_4057" s="T52">PKZ_1964_SU0204.PKZ.012 (021)</ta>
            <ta e="T58" id="Seg_4058" s="T56">PKZ_1964_SU0204.PKZ.013 (023)</ta>
            <ta e="T61" id="Seg_4059" s="T58">PKZ_1964_SU0204.PKZ.014 (024)</ta>
            <ta e="T64" id="Seg_4060" s="T62">PKZ_1964_SU0204.PKZ.015 (026)</ta>
            <ta e="T76" id="Seg_4061" s="T70">PKZ_1964_SU0204.PKZ.016 (028)</ta>
            <ta e="T77" id="Seg_4062" s="T76">PKZ_1964_SU0204.PKZ.017 (029)</ta>
            <ta e="T85" id="Seg_4063" s="T77">PKZ_1964_SU0204.PKZ.018 (030)</ta>
            <ta e="T88" id="Seg_4064" s="T85">PKZ_1964_SU0204.PKZ.019 (031)</ta>
            <ta e="T89" id="Seg_4065" s="T88">PKZ_1964_SU0204.PKZ.020 (032)</ta>
            <ta e="T91" id="Seg_4066" s="T89">PKZ_1964_SU0204.PKZ.021 (033)</ta>
            <ta e="T101" id="Seg_4067" s="T96">PKZ_1964_SU0204.PKZ.022 (036)</ta>
            <ta e="T109" id="Seg_4068" s="T107">PKZ_1964_SU0204.PKZ.023 (038)</ta>
            <ta e="T113" id="Seg_4069" s="T109">PKZ_1964_SU0204.PKZ.024 (039)</ta>
            <ta e="T115" id="Seg_4070" s="T113">PKZ_1964_SU0204.PKZ.025 (040)</ta>
            <ta e="T118" id="Seg_4071" s="T115">PKZ_1964_SU0204.PKZ.026 (041)</ta>
            <ta e="T120" id="Seg_4072" s="T118">PKZ_1964_SU0204.PKZ.027 (042)</ta>
            <ta e="T124" id="Seg_4073" s="T120">PKZ_1964_SU0204.PKZ.028 (043)</ta>
            <ta e="T126" id="Seg_4074" s="T124">PKZ_1964_SU0204.PKZ.029 (044)</ta>
            <ta e="T128" id="Seg_4075" s="T126">PKZ_1964_SU0204.PKZ.030 (045)</ta>
            <ta e="T131" id="Seg_4076" s="T128">PKZ_1964_SU0204.PKZ.031 (046)</ta>
            <ta e="T133" id="Seg_4077" s="T131">PKZ_1964_SU0204.PKZ.032 (047)</ta>
            <ta e="T136" id="Seg_4078" s="T133">PKZ_1964_SU0204.PKZ.033 (048)</ta>
            <ta e="T138" id="Seg_4079" s="T136">PKZ_1964_SU0204.PKZ.034 (049)</ta>
            <ta e="T140" id="Seg_4080" s="T138">PKZ_1964_SU0204.PKZ.035 (050)</ta>
            <ta e="T141" id="Seg_4081" s="T140">PKZ_1964_SU0204.PKZ.036 (051)</ta>
            <ta e="T143" id="Seg_4082" s="T141">PKZ_1964_SU0204.PKZ.037 (052)</ta>
            <ta e="T145" id="Seg_4083" s="T143">PKZ_1964_SU0204.PKZ.038 (053)</ta>
            <ta e="T146" id="Seg_4084" s="T145">PKZ_1964_SU0204.PKZ.039 (054)</ta>
            <ta e="T148" id="Seg_4085" s="T146">PKZ_1964_SU0204.PKZ.040 (055)</ta>
            <ta e="T151" id="Seg_4086" s="T148">PKZ_1964_SU0204.PKZ.041 (056)</ta>
            <ta e="T155" id="Seg_4087" s="T151">PKZ_1964_SU0204.PKZ.042 (057)</ta>
            <ta e="T165" id="Seg_4088" s="T158">PKZ_1964_SU0204.PKZ.043 (059)</ta>
            <ta e="T169" id="Seg_4089" s="T165">PKZ_1964_SU0204.PKZ.044 (060)</ta>
            <ta e="T173" id="Seg_4090" s="T169">PKZ_1964_SU0204.PKZ.045 (061)</ta>
            <ta e="T175" id="Seg_4091" s="T173">PKZ_1964_SU0204.PKZ.046 (062)</ta>
            <ta e="T179" id="Seg_4092" s="T175">PKZ_1964_SU0204.PKZ.047 (063)</ta>
            <ta e="T181" id="Seg_4093" s="T179">PKZ_1964_SU0204.PKZ.048 (064)</ta>
            <ta e="T182" id="Seg_4094" s="T181">PKZ_1964_SU0204.PKZ.049 (065)</ta>
            <ta e="T185" id="Seg_4095" s="T182">PKZ_1964_SU0204.PKZ.050 (066)</ta>
            <ta e="T189" id="Seg_4096" s="T185">PKZ_1964_SU0204.PKZ.051 (067)</ta>
            <ta e="T191" id="Seg_4097" s="T189">PKZ_1964_SU0204.PKZ.052 (068)</ta>
            <ta e="T193" id="Seg_4098" s="T191">PKZ_1964_SU0204.PKZ.053 (069)</ta>
            <ta e="T196" id="Seg_4099" s="T193">PKZ_1964_SU0204.PKZ.054 (070)</ta>
            <ta e="T199" id="Seg_4100" s="T196">PKZ_1964_SU0204.PKZ.055 (071)</ta>
            <ta e="T200" id="Seg_4101" s="T199">PKZ_1964_SU0204.PKZ.056 (072)</ta>
            <ta e="T202" id="Seg_4102" s="T200">PKZ_1964_SU0204.PKZ.057 (073)</ta>
            <ta e="T204" id="Seg_4103" s="T202">PKZ_1964_SU0204.PKZ.058 (074)</ta>
            <ta e="T209" id="Seg_4104" s="T204">PKZ_1964_SU0204.PKZ.059 (075)</ta>
            <ta e="T211" id="Seg_4105" s="T209">PKZ_1964_SU0204.PKZ.060 (076)</ta>
            <ta e="T215" id="Seg_4106" s="T211">PKZ_1964_SU0204.PKZ.061 (077)</ta>
            <ta e="T217" id="Seg_4107" s="T215">PKZ_1964_SU0204.PKZ.062 (078)</ta>
            <ta e="T220" id="Seg_4108" s="T217">PKZ_1964_SU0204.PKZ.063 (079)</ta>
            <ta e="T221" id="Seg_4109" s="T220">PKZ_1964_SU0204.PKZ.064 (080)</ta>
            <ta e="T223" id="Seg_4110" s="T221">PKZ_1964_SU0204.PKZ.065 (081)</ta>
            <ta e="T224" id="Seg_4111" s="T223">PKZ_1964_SU0204.PKZ.066 (082)</ta>
            <ta e="T227" id="Seg_4112" s="T224">PKZ_1964_SU0204.PKZ.067 (083)</ta>
            <ta e="T230" id="Seg_4113" s="T227">PKZ_1964_SU0204.PKZ.068 (084)</ta>
            <ta e="T233" id="Seg_4114" s="T230">PKZ_1964_SU0204.PKZ.069 (085)</ta>
            <ta e="T236" id="Seg_4115" s="T233">PKZ_1964_SU0204.PKZ.070 (086)</ta>
            <ta e="T239" id="Seg_4116" s="T236">PKZ_1964_SU0204.PKZ.071 (087)</ta>
            <ta e="T241" id="Seg_4117" s="T239">PKZ_1964_SU0204.PKZ.072 (088)</ta>
            <ta e="T243" id="Seg_4118" s="T241">PKZ_1964_SU0204.PKZ.073 (089)</ta>
            <ta e="T245" id="Seg_4119" s="T244">PKZ_1964_SU0204.PKZ.074 (091)</ta>
            <ta e="T249" id="Seg_4120" s="T245">PKZ_1964_SU0204.PKZ.075 (092)</ta>
            <ta e="T251" id="Seg_4121" s="T249">PKZ_1964_SU0204.PKZ.076 (093)</ta>
            <ta e="T253" id="Seg_4122" s="T251">PKZ_1964_SU0204.PKZ.077 (094)</ta>
            <ta e="T257" id="Seg_4123" s="T253">PKZ_1964_SU0204.PKZ.078 (095)</ta>
            <ta e="T260" id="Seg_4124" s="T257">PKZ_1964_SU0204.PKZ.079 (096)</ta>
            <ta e="T262" id="Seg_4125" s="T260">PKZ_1964_SU0204.PKZ.080 (097)</ta>
            <ta e="T265" id="Seg_4126" s="T262">PKZ_1964_SU0204.PKZ.081 (098)</ta>
            <ta e="T272" id="Seg_4127" s="T268">PKZ_1964_SU0204.PKZ.082 (100)</ta>
            <ta e="T274" id="Seg_4128" s="T272">PKZ_1964_SU0204.PKZ.083 (101)</ta>
            <ta e="T277" id="Seg_4129" s="T274">PKZ_1964_SU0204.PKZ.084 (102)</ta>
            <ta e="T279" id="Seg_4130" s="T277">PKZ_1964_SU0204.PKZ.085 (103)</ta>
            <ta e="T282" id="Seg_4131" s="T279">PKZ_1964_SU0204.PKZ.086 (104)</ta>
            <ta e="T285" id="Seg_4132" s="T282">PKZ_1964_SU0204.PKZ.087 (105)</ta>
            <ta e="T286" id="Seg_4133" s="T285">PKZ_1964_SU0204.PKZ.088 (106)</ta>
            <ta e="T288" id="Seg_4134" s="T286">PKZ_1964_SU0204.PKZ.089 (107)</ta>
            <ta e="T290" id="Seg_4135" s="T288">PKZ_1964_SU0204.PKZ.090 (108)</ta>
            <ta e="T292" id="Seg_4136" s="T290">PKZ_1964_SU0204.PKZ.091 (109)</ta>
            <ta e="T294" id="Seg_4137" s="T292">PKZ_1964_SU0204.PKZ.092 (110)</ta>
            <ta e="T296" id="Seg_4138" s="T294">PKZ_1964_SU0204.PKZ.093 (111)</ta>
            <ta e="T300" id="Seg_4139" s="T296">PKZ_1964_SU0204.PKZ.094 (112)</ta>
            <ta e="T304" id="Seg_4140" s="T300">PKZ_1964_SU0204.PKZ.095 (113)</ta>
            <ta e="T306" id="Seg_4141" s="T304">PKZ_1964_SU0204.PKZ.096 (114)</ta>
            <ta e="T308" id="Seg_4142" s="T306">PKZ_1964_SU0204.PKZ.097 (115)</ta>
            <ta e="T309" id="Seg_4143" s="T308">PKZ_1964_SU0204.PKZ.098 (116)</ta>
            <ta e="T312" id="Seg_4144" s="T309">PKZ_1964_SU0204.PKZ.099 (117)</ta>
            <ta e="T320" id="Seg_4145" s="T315">PKZ_1964_SU0204.PKZ.100 (119)</ta>
            <ta e="T325" id="Seg_4146" s="T320">PKZ_1964_SU0204.PKZ.101 (120)</ta>
            <ta e="T327" id="Seg_4147" s="T325">PKZ_1964_SU0204.PKZ.102 (121)</ta>
            <ta e="T328" id="Seg_4148" s="T327">PKZ_1964_SU0204.PKZ.103 (122)</ta>
            <ta e="T338" id="Seg_4149" s="T328">PKZ_1964_SU0204.PKZ.104 (123)</ta>
            <ta e="T343" id="Seg_4150" s="T338">PKZ_1964_SU0204.PKZ.105 (124)</ta>
            <ta e="T347" id="Seg_4151" s="T343">PKZ_1964_SU0204.PKZ.106 (125)</ta>
            <ta e="T350" id="Seg_4152" s="T347">PKZ_1964_SU0204.PKZ.107 (126)</ta>
            <ta e="T359" id="Seg_4153" s="T355">PKZ_1964_SU0204.PKZ.108 (128)</ta>
            <ta e="T362" id="Seg_4154" s="T359">PKZ_1964_SU0204.PKZ.109 (129)</ta>
            <ta e="T365" id="Seg_4155" s="T362">PKZ_1964_SU0204.PKZ.110 (130)</ta>
            <ta e="T368" id="Seg_4156" s="T365">PKZ_1964_SU0204.PKZ.111 (131)</ta>
            <ta e="T395" id="Seg_4157" s="T368">PKZ_1964_SU0204.PKZ.112 (132)</ta>
            <ta e="T396" id="Seg_4158" s="T395">PKZ_1964_SU0204.PKZ.113 (133)</ta>
            <ta e="T425" id="Seg_4159" s="T418">PKZ_1964_SU0204.PKZ.114 (136)</ta>
            <ta e="T426" id="Seg_4160" s="T425">PKZ_1964_SU0204.PKZ.115 (137)</ta>
            <ta e="T431" id="Seg_4161" s="T426">PKZ_1964_SU0204.PKZ.116 (138)</ta>
            <ta e="T433" id="Seg_4162" s="T431">PKZ_1964_SU0204.PKZ.117 (139)</ta>
            <ta e="T435" id="Seg_4163" s="T433">PKZ_1964_SU0204.PKZ.118 (140)</ta>
            <ta e="T436" id="Seg_4164" s="T435">PKZ_1964_SU0204.PKZ.119 (141)</ta>
            <ta e="T440" id="Seg_4165" s="T436">PKZ_1964_SU0204.PKZ.120 (142)</ta>
            <ta e="T446" id="Seg_4166" s="T440">PKZ_1964_SU0204.PKZ.121 (143)</ta>
            <ta e="T447" id="Seg_4167" s="T446">PKZ_1964_SU0204.PKZ.122 (144)</ta>
            <ta e="T449" id="Seg_4168" s="T447">PKZ_1964_SU0204.PKZ.123 (145)</ta>
            <ta e="T451" id="Seg_4169" s="T449">PKZ_1964_SU0204.PKZ.124 (146)</ta>
            <ta e="T453" id="Seg_4170" s="T451">PKZ_1964_SU0204.PKZ.125 (147)</ta>
            <ta e="T455" id="Seg_4171" s="T453">PKZ_1964_SU0204.PKZ.126 (148)</ta>
            <ta e="T457" id="Seg_4172" s="T455">PKZ_1964_SU0204.PKZ.127 (149)</ta>
            <ta e="T459" id="Seg_4173" s="T457">PKZ_1964_SU0204.PKZ.128 (150)</ta>
            <ta e="T461" id="Seg_4174" s="T459">PKZ_1964_SU0204.PKZ.129 (151)</ta>
            <ta e="T462" id="Seg_4175" s="T461">PKZ_1964_SU0204.PKZ.130 (152)</ta>
            <ta e="T463" id="Seg_4176" s="T462">PKZ_1964_SU0204.PKZ.131 (153)</ta>
            <ta e="T464" id="Seg_4177" s="T463">PKZ_1964_SU0204.PKZ.132 (154)</ta>
            <ta e="T469" id="Seg_4178" s="T464">PKZ_1964_SU0204.PKZ.133 (155)</ta>
            <ta e="T472" id="Seg_4179" s="T469">PKZ_1964_SU0204.PKZ.134 (156)</ta>
            <ta e="T475" id="Seg_4180" s="T472">PKZ_1964_SU0204.PKZ.135 (157)</ta>
            <ta e="T478" id="Seg_4181" s="T475">PKZ_1964_SU0204.PKZ.136 (158)</ta>
            <ta e="T481" id="Seg_4182" s="T478">PKZ_1964_SU0204.PKZ.137 (159)</ta>
            <ta e="T484" id="Seg_4183" s="T481">PKZ_1964_SU0204.PKZ.138 (160)</ta>
            <ta e="T489" id="Seg_4184" s="T484">PKZ_1964_SU0204.PKZ.139 (161)</ta>
            <ta e="T494" id="Seg_4185" s="T489">PKZ_1964_SU0204.PKZ.140 (162)</ta>
            <ta e="T502" id="Seg_4186" s="T495">PKZ_1964_SU0204.PKZ.141 (164)</ta>
            <ta e="T507" id="Seg_4187" s="T504">PKZ_1964_SU0204.PKZ.142 (166)</ta>
            <ta e="T517" id="Seg_4188" s="T514">PKZ_1964_SU0204.PKZ.143 (168)</ta>
            <ta e="T521" id="Seg_4189" s="T517">PKZ_1964_SU0204.PKZ.144 (169)</ta>
            <ta e="T524" id="Seg_4190" s="T521">PKZ_1964_SU0204.PKZ.145 (170)</ta>
            <ta e="T525" id="Seg_4191" s="T524">PKZ_1964_SU0204.PKZ.146 (171)</ta>
            <ta e="T529" id="Seg_4192" s="T525">PKZ_1964_SU0204.PKZ.147 (172)</ta>
            <ta e="T530" id="Seg_4193" s="T529">PKZ_1964_SU0204.PKZ.148 (173)</ta>
            <ta e="T535" id="Seg_4194" s="T530">PKZ_1964_SU0204.PKZ.149 (174)</ta>
            <ta e="T538" id="Seg_4195" s="T535">PKZ_1964_SU0204.PKZ.150 (175)</ta>
            <ta e="T540" id="Seg_4196" s="T538">PKZ_1964_SU0204.PKZ.151 (176)</ta>
            <ta e="T543" id="Seg_4197" s="T540">PKZ_1964_SU0204.PKZ.152 (177)</ta>
            <ta e="T545" id="Seg_4198" s="T543">PKZ_1964_SU0204.PKZ.153 (178)</ta>
            <ta e="T547" id="Seg_4199" s="T545">PKZ_1964_SU0204.PKZ.154 (179)</ta>
            <ta e="T552" id="Seg_4200" s="T547">PKZ_1964_SU0204.PKZ.155 (180)</ta>
            <ta e="T554" id="Seg_4201" s="T552">PKZ_1964_SU0204.PKZ.156 (181)</ta>
            <ta e="T562" id="Seg_4202" s="T554">PKZ_1964_SU0204.PKZ.157 (182)</ta>
            <ta e="T571" id="Seg_4203" s="T562">PKZ_1964_SU0204.PKZ.158 (183)</ta>
            <ta e="T585" id="Seg_4204" s="T573">PKZ_1964_SU0204.PKZ.159 (185)</ta>
            <ta e="T590" id="Seg_4205" s="T585">PKZ_1964_SU0204.PKZ.160 (186)</ta>
            <ta e="T592" id="Seg_4206" s="T590">PKZ_1964_SU0204.PKZ.161 (187)</ta>
            <ta e="T595" id="Seg_4207" s="T592">PKZ_1964_SU0204.PKZ.162 (188)</ta>
            <ta e="T598" id="Seg_4208" s="T595">PKZ_1964_SU0204.PKZ.163 (189)</ta>
            <ta e="T601" id="Seg_4209" s="T598">PKZ_1964_SU0204.PKZ.164 (190)</ta>
            <ta e="T603" id="Seg_4210" s="T601">PKZ_1964_SU0204.PKZ.165 (191)</ta>
            <ta e="T605" id="Seg_4211" s="T603">PKZ_1964_SU0204.PKZ.166 (192)</ta>
            <ta e="T608" id="Seg_4212" s="T605">PKZ_1964_SU0204.PKZ.167 (193)</ta>
            <ta e="T610" id="Seg_4213" s="T608">PKZ_1964_SU0204.PKZ.168 (194)</ta>
            <ta e="T611" id="Seg_4214" s="T610">PKZ_1964_SU0204.PKZ.169 (195)</ta>
            <ta e="T612" id="Seg_4215" s="T611">PKZ_1964_SU0204.PKZ.170 (196)</ta>
            <ta e="T616" id="Seg_4216" s="T614">PKZ_1964_SU0204.PKZ.171 (198)</ta>
            <ta e="T618" id="Seg_4217" s="T616">PKZ_1964_SU0204.PKZ.172 (199)</ta>
            <ta e="T620" id="Seg_4218" s="T618">PKZ_1964_SU0204.PKZ.173 (200)</ta>
            <ta e="T624" id="Seg_4219" s="T621">PKZ_1964_SU0204.PKZ.174 (202)</ta>
            <ta e="T626" id="Seg_4220" s="T624">PKZ_1964_SU0204.PKZ.175 (203)</ta>
            <ta e="T629" id="Seg_4221" s="T626">PKZ_1964_SU0204.PKZ.176 (204)</ta>
            <ta e="T631" id="Seg_4222" s="T629">PKZ_1964_SU0204.PKZ.177 (205)</ta>
            <ta e="T636" id="Seg_4223" s="T634">PKZ_1964_SU0204.PKZ.178 (208)</ta>
            <ta e="T638" id="Seg_4224" s="T636">PKZ_1964_SU0204.PKZ.179 (209)</ta>
            <ta e="T641" id="Seg_4225" s="T638">PKZ_1964_SU0204.PKZ.180 (210)</ta>
            <ta e="T642" id="Seg_4226" s="T641">PKZ_1964_SU0204.PKZ.181 (211)</ta>
            <ta e="T644" id="Seg_4227" s="T642">PKZ_1964_SU0204.PKZ.182 (212)</ta>
            <ta e="T646" id="Seg_4228" s="T644">PKZ_1964_SU0204.PKZ.183 (213)</ta>
            <ta e="T648" id="Seg_4229" s="T646">PKZ_1964_SU0204.PKZ.184 (214)</ta>
            <ta e="T649" id="Seg_4230" s="T648">PKZ_1964_SU0204.PKZ.185 (215)</ta>
            <ta e="T651" id="Seg_4231" s="T649">PKZ_1964_SU0204.PKZ.186 (216)</ta>
            <ta e="T654" id="Seg_4232" s="T651">PKZ_1964_SU0204.PKZ.187 (217)</ta>
            <ta e="T658" id="Seg_4233" s="T654">PKZ_1964_SU0204.PKZ.188 (218)</ta>
            <ta e="T660" id="Seg_4234" s="T658">PKZ_1964_SU0204.PKZ.189 (219)</ta>
            <ta e="T661" id="Seg_4235" s="T660">PKZ_1964_SU0204.PKZ.190 (220)</ta>
            <ta e="T666" id="Seg_4236" s="T661">PKZ_1964_SU0204.PKZ.191 (221)</ta>
            <ta e="T670" id="Seg_4237" s="T666">PKZ_1964_SU0204.PKZ.192 (222)</ta>
            <ta e="T672" id="Seg_4238" s="T670">PKZ_1964_SU0204.PKZ.193 (223)</ta>
            <ta e="T678" id="Seg_4239" s="T672">PKZ_1964_SU0204.PKZ.194 (224)</ta>
            <ta e="T679" id="Seg_4240" s="T678">PKZ_1964_SU0204.PKZ.195 (225)</ta>
            <ta e="T682" id="Seg_4241" s="T679">PKZ_1964_SU0204.PKZ.196 (226)</ta>
            <ta e="T685" id="Seg_4242" s="T682">PKZ_1964_SU0204.PKZ.197 (227)</ta>
            <ta e="T686" id="Seg_4243" s="T685">PKZ_1964_SU0204.PKZ.198 (228)</ta>
            <ta e="T688" id="Seg_4244" s="T686">PKZ_1964_SU0204.PKZ.199 (229)</ta>
            <ta e="T691" id="Seg_4245" s="T688">PKZ_1964_SU0204.PKZ.200 (230)</ta>
            <ta e="T694" id="Seg_4246" s="T691">PKZ_1964_SU0204.PKZ.201 (231)</ta>
            <ta e="T697" id="Seg_4247" s="T694">PKZ_1964_SU0204.PKZ.202 (232)</ta>
            <ta e="T699" id="Seg_4248" s="T697">PKZ_1964_SU0204.PKZ.203 (233)</ta>
            <ta e="T701" id="Seg_4249" s="T699">PKZ_1964_SU0204.PKZ.204 (234)</ta>
            <ta e="T703" id="Seg_4250" s="T701">PKZ_1964_SU0204.PKZ.205 (235)</ta>
            <ta e="T705" id="Seg_4251" s="T703">PKZ_1964_SU0204.PKZ.206 (236)</ta>
            <ta e="T706" id="Seg_4252" s="T705">PKZ_1964_SU0204.PKZ.207 (237)</ta>
            <ta e="T710" id="Seg_4253" s="T708">PKZ_1964_SU0204.PKZ.208 (239)</ta>
            <ta e="T714" id="Seg_4254" s="T710">PKZ_1964_SU0204.PKZ.209 (240)</ta>
            <ta e="T715" id="Seg_4255" s="T714">PKZ_1964_SU0204.PKZ.210 (241)</ta>
            <ta e="T717" id="Seg_4256" s="T715">PKZ_1964_SU0204.PKZ.211 (242)</ta>
            <ta e="T719" id="Seg_4257" s="T717">PKZ_1964_SU0204.PKZ.212 (243)</ta>
            <ta e="T722" id="Seg_4258" s="T719">PKZ_1964_SU0204.PKZ.213 (244)</ta>
            <ta e="T724" id="Seg_4259" s="T722">PKZ_1964_SU0204.PKZ.214 (245)</ta>
            <ta e="T734" id="Seg_4260" s="T724">PKZ_1964_SU0204.PKZ.215 (246)</ta>
            <ta e="T739" id="Seg_4261" s="T734">PKZ_1964_SU0204.PKZ.216 (247)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T9" id="Seg_4262" s="T2">((…DMG)) а чего это, на что? </ta>
            <ta e="T15" id="Seg_4263" s="T14">Сюда? </ta>
            <ta e="T19" id="Seg_4264" s="T17">Iʔ kudonzaʔ. </ta>
            <ta e="T25" id="Seg_4265" s="T22">Măn dĭm ibiem. </ta>
            <ta e="T30" id="Seg_4266" s="T27">Dĭn bar kudonzlaʔbəʔjə. </ta>
            <ta e="T32" id="Seg_4267" s="T30">Măn kalla dʼürbiem. </ta>
            <ta e="T39" id="Seg_4268" s="T36">Iʔgö pʼaŋdəbial bar. </ta>
            <ta e="T40" id="Seg_4269" s="T39">((BRK)). </ta>
            <ta e="T44" id="Seg_4270" s="T40">Koŋ bar nʼešpək šobi. </ta>
            <ta e="T46" id="Seg_4271" s="T44">Jezerik bar. </ta>
            <ta e="T51" id="Seg_4272" s="T47">(Не) забыла, как "упал". </ta>
            <ta e="T54" id="Seg_4273" s="T52">((DMG)) ige. </ta>
            <ta e="T58" id="Seg_4274" s="T56">Sabən ige. </ta>
            <ta e="T61" id="Seg_4275" s="T58">(Kĭš-) Kĭškit bar. </ta>
            <ta e="T64" id="Seg_4276" s="T62">"Трись", говорю. </ta>
            <ta e="T76" id="Seg_4277" s="T70">(Sp-) Ugaːndə tăn sagər, kanaʔ moltʼanə! </ta>
            <ta e="T77" id="Seg_4278" s="T76">((BRK)). </ta>
            <ta e="T85" id="Seg_4279" s="T77">Dĭn dʼibige (ige) i šĭšəge bü, sabən ige. </ta>
            <ta e="T88" id="Seg_4280" s="T85">Păzaʔ (хорошенько), kĭškit! </ta>
            <ta e="T89" id="Seg_4281" s="T88">Трись. </ta>
            <ta e="T91" id="Seg_4282" s="T89">((DMG)) to. </ta>
            <ta e="T101" id="Seg_4283" s="T96">Али по-новому говорю? </ta>
            <ta e="T109" id="Seg_4284" s="T107">Tüžöjəʔi šedendə. </ta>
            <ta e="T113" id="Seg_4285" s="T109">((BRK)) Нет, ((…)) nuzaŋ šobiʔi. </ta>
            <ta e="T115" id="Seg_4286" s="T113">Amnaʔ, amoraʔ! </ta>
            <ta e="T118" id="Seg_4287" s="T115">Padʼi amorzittə axota. </ta>
            <ta e="T120" id="Seg_4288" s="T118">Gijen ibiel? </ta>
            <ta e="T124" id="Seg_4289" s="T120">Amnaʔ döbər, kuvas kuza! </ta>
            <ta e="T126" id="Seg_4290" s="T124">Ну хватит. </ta>
            <ta e="T128" id="Seg_4291" s="T126">((DMG)) (-tədada). </ta>
            <ta e="T131" id="Seg_4292" s="T128">Nada tura (băzəjsʼtə). </ta>
            <ta e="T133" id="Seg_4293" s="T131">Nada săbərəjzittə. </ta>
            <ta e="T136" id="Seg_4294" s="T133">(Nʼiʔdə=) Nʼiʔdə kanzittə. </ta>
            <ta e="T138" id="Seg_4295" s="T136">Tüžöj surdəsʼtə. </ta>
            <ta e="T140" id="Seg_4296" s="T138">Eššim băzəjsʼtə. </ta>
            <ta e="T141" id="Seg_4297" s="T140">((BRK)). </ta>
            <ta e="T143" id="Seg_4298" s="T141">Ipek pürzittə. </ta>
            <ta e="T145" id="Seg_4299" s="T143">Ipek nuldəsʼtə. </ta>
            <ta e="T146" id="Seg_4300" s="T145">Хватит? </ta>
            <ta e="T148" id="Seg_4301" s="T146">((DMG)) pürzittə. </ta>
            <ta e="T151" id="Seg_4302" s="T148">(Sʼel) pürzittə nada. </ta>
            <ta e="T155" id="Seg_4303" s="T151">Еще чего-(нибудь). </ta>
            <ta e="T165" id="Seg_4304" s="T158">(Ча-) Чай варить как-то. </ta>
            <ta e="T169" id="Seg_4305" s="T165">Segi bü nada mĭnzərzittə. </ta>
            <ta e="T173" id="Seg_4306" s="T169">((DMG)) kirgarlaʔbə, i kuromaʔ! </ta>
            <ta e="T175" id="Seg_4307" s="T173">I alomaʔ. </ta>
            <ta e="T179" id="Seg_4308" s="T175">Еще как-то?</ta>
            <ta e="T181" id="Seg_4309" s="T179">Господи. </ta>
            <ta e="T182" id="Seg_4310" s="T181">Забыла. </ta>
            <ta e="T185" id="Seg_4311" s="T182">Măn dĭn ibiem. </ta>
            <ta e="T189" id="Seg_4312" s="T185">Dĭn bar (kudonz-) kudonzlaʔbəʔjə. </ta>
            <ta e="T191" id="Seg_4313" s="T189">Măn kalla dʼürbiem. </ta>
            <ta e="T193" id="Seg_4314" s="T191">((DMG)) (-ndəbial). </ta>
            <ta e="T196" id="Seg_4315" s="T193">Udal padʼi ĭzemnie. </ta>
            <ta e="T199" id="Seg_4316" s="T196">Ну, все наверное. </ta>
            <ta e="T200" id="Seg_4317" s="T199">((BRK)). </ta>
            <ta e="T202" id="Seg_4318" s="T200">Tus naga. </ta>
            <ta e="T204" id="Seg_4319" s="T202">Nada tustʼarzittə. </ta>
            <ta e="T209" id="Seg_4320" s="T204">((DMG)) ĭmbizʼiʔ amzittə, šamnak naga. </ta>
            <ta e="T211" id="Seg_4321" s="T209">Deʔ šamnak! </ta>
            <ta e="T215" id="Seg_4322" s="T211">Dö šamnak, mĭbiem tănan! </ta>
            <ta e="T217" id="Seg_4323" s="T215">Büžü amaʔ! </ta>
            <ta e="T220" id="Seg_4324" s="T217">Всё, скорее ешь. </ta>
            <ta e="T221" id="Seg_4325" s="T220">((BRK)). </ta>
            <ta e="T223" id="Seg_4326" s="T221">Kanžəbəj ((…)). </ta>
            <ta e="T224" id="Seg_4327" s="T223">Obberəj. </ta>
            <ta e="T227" id="Seg_4328" s="T224">Ugaːndə kuvas koʔbsaŋ. </ta>
            <ta e="T230" id="Seg_4329" s="T227">Ugaːndə kuvas nʼizeŋ. </ta>
            <ta e="T233" id="Seg_4330" s="T230">Ugaːndə kuvas il. </ta>
            <ta e="T236" id="Seg_4331" s="T233">Ugaːndə jakšə kuza. </ta>
            <ta e="T239" id="Seg_4332" s="T236">Ugaːndə šaːmnaʔbə kuza. ((LAUGH))</ta>
            <ta e="T241" id="Seg_4333" s="T239">((DMG)) (-luʔpial). </ta>
            <ta e="T243" id="Seg_4334" s="T241">Nada kunolzittə. </ta>
            <ta e="T245" id="Seg_4335" s="T244">Idʼiʔeʔe. </ta>
            <ta e="T249" id="Seg_4336" s="T245">((DMG)) (-tə) axota ugaːndə. </ta>
            <ta e="T251" id="Seg_4337" s="T249">Nanəm ĭzemnie. </ta>
            <ta e="T253" id="Seg_4338" s="T251">Amoraʔ, amnaʔ! </ta>
            <ta e="T257" id="Seg_4339" s="T253">Iʔ kürümaʔ, iʔ alomaʔ! </ta>
            <ta e="T260" id="Seg_4340" s="T257">((DMG)) nada izittə. </ta>
            <ta e="T262" id="Seg_4341" s="T260">A to ((DMG)). </ta>
            <ta e="T265" id="Seg_4342" s="T262">И еще чего? </ta>
            <ta e="T272" id="Seg_4343" s="T268">((DMG)) Dʼijenə kanzittə, keʔbde deʔsʼittə. </ta>
            <ta e="T274" id="Seg_4344" s="T272">Sanə deʔsittə. </ta>
            <ta e="T277" id="Seg_4345" s="T274">Büžü kanžəbəj šidegöʔ. </ta>
            <ta e="T279" id="Seg_4346" s="T277">((DMG)) (-ləm). </ta>
            <ta e="T282" id="Seg_4347" s="T279">Ĭmbi-nʼibudʼ kutlam (dĭm). </ta>
            <ta e="T285" id="Seg_4348" s="T282">Poʔto kuʔpiom bar. </ta>
            <ta e="T286" id="Seg_4349" s="T285">((BRK)). </ta>
            <ta e="T288" id="Seg_4350" s="T286">Albuga kuʔpiom. </ta>
            <ta e="T290" id="Seg_4351" s="T288">Tažəp kuʔpiom. </ta>
            <ta e="T292" id="Seg_4352" s="T290">(Sön) kuʔpiam. </ta>
            <ta e="T294" id="Seg_4353" s="T292">Urgaːba kuʔpiam. </ta>
            <ta e="T296" id="Seg_4354" s="T294">Все, хватит. </ta>
            <ta e="T300" id="Seg_4355" s="T296">((DMG)) (-m) ĭzemnie ugaːndə. </ta>
            <ta e="T304" id="Seg_4356" s="T300">Udam ĭzemnie, ulum ĭzemnie. </ta>
            <ta e="T306" id="Seg_4357" s="T304">Bögəlbə ĭzemnie. </ta>
            <ta e="T308" id="Seg_4358" s="T306">Üjüzeŋdə ĭzemnie. </ta>
            <ta e="T309" id="Seg_4359" s="T308">((BRK)). </ta>
            <ta e="T312" id="Seg_4360" s="T309">Ладно уже, хватит. </ta>
            <ta e="T320" id="Seg_4361" s="T315">Так я уже позабыла опять. </ta>
            <ta e="T325" id="Seg_4362" s="T320">Tüʔsittə kanzittə, kĭnzəsʼtə nada kanzittə. </ta>
            <ta e="T327" id="Seg_4363" s="T325">Nada … </ta>
            <ta e="T328" id="Seg_4364" s="T327">((BRK)). </ta>
            <ta e="T338" id="Seg_4365" s="T328">((DMG)) Старик со старухой, у них были мальчик и девочка. </ta>
            <ta e="T343" id="Seg_4366" s="T338">((DMG)) (-ʔi) nüke i büzʼe. </ta>
            <ta e="T347" id="Seg_4367" s="T343">Dĭzeŋ (eks-) esseŋdə ibiʔi. </ta>
            <ta e="T350" id="Seg_4368" s="T347">Nʼi i koʔbdo. </ta>
            <ta e="T359" id="Seg_4369" s="T355">Amnobiʔi nüke i büzʼe. </ta>
            <ta e="T362" id="Seg_4370" s="T359">Dĭzeŋdə esseŋdə ibiʔi. </ta>
            <ta e="T365" id="Seg_4371" s="T362">Nʼit i koʔbdo. </ta>
            <ta e="T368" id="Seg_4372" s="T365">Nʼi i koʔbdo. </ta>
            <ta e="T395" id="Seg_4373" s="T368">Oʔb, šide, nagur, sumna, muktuʔ, sejʔpü, šinteʔtə, amitun, bʼeʔ, bʼeʔ onʼiʔ, bʼeʔ šide, bʼeʔ nagur, bʼeʔ teʔtə, bʼeʔ sumna, bʼeʔ muktuʔ, bʼeʔ sejʔpü, bʼeʔ šinteʔtə, bʼeʔ … </ta>
            <ta e="T396" id="Seg_4374" s="T395">Забыла. </ta>
            <ta e="T425" id="Seg_4375" s="T418">Oʔb, šide, nagur, (muktuʔ), (sejʔpü), šinteʔtə, amitun. </ta>
            <ta e="T426" id="Seg_4376" s="T425">((BRK)). </ta>
            <ta e="T431" id="Seg_4377" s="T426">"Măna nada monoʔkozittə, a šindim?" </ta>
            <ta e="T433" id="Seg_4378" s="T431">Dĭ koʔbdo. </ta>
            <ta e="T435" id="Seg_4379" s="T433">Kanaʔ monoʔkot! </ta>
            <ta e="T436" id="Seg_4380" s="T435">Всё. </ta>
            <ta e="T440" id="Seg_4381" s="T436">((DMG)) (-təbtə) turagən amnolaʔbəm. </ta>
            <ta e="T446" id="Seg_4382" s="T440">Oj bar üjüm kutʼümnie, nada … </ta>
            <ta e="T447" id="Seg_4383" s="T446">((BRK)). </ta>
            <ta e="T449" id="Seg_4384" s="T447">Ulum ĭzemnie. </ta>
            <ta e="T451" id="Seg_4385" s="T449">Bögelbə ĭzemnie. </ta>
            <ta e="T453" id="Seg_4386" s="T451">Udam ĭzemnie. </ta>
            <ta e="T455" id="Seg_4387" s="T453">Sʼimam ĭzemnie. </ta>
            <ta e="T457" id="Seg_4388" s="T455">Ku ĭzemnie. </ta>
            <ta e="T459" id="Seg_4389" s="T457">Üjüm ĭzemnie. </ta>
            <ta e="T461" id="Seg_4390" s="T459">Kötenbə ĭzemnie. </ta>
            <ta e="T462" id="Seg_4391" s="T461">((BRK)). </ta>
            <ta e="T463" id="Seg_4392" s="T462">(Dʼok). </ta>
            <ta e="T464" id="Seg_4393" s="T463">((BRK)). </ta>
            <ta e="T469" id="Seg_4394" s="T464">Dʼijenə nada kanzittə, albuga kuʔsittə. </ta>
            <ta e="T472" id="Seg_4395" s="T469">Urgaːba kuʔsittə nada. </ta>
            <ta e="T475" id="Seg_4396" s="T472">Tažəp kuʔsittə nada. </ta>
            <ta e="T478" id="Seg_4397" s="T475">Bulan kuʔsittə nada. </ta>
            <ta e="T481" id="Seg_4398" s="T478">Sön kuʔsittə nada. </ta>
            <ta e="T484" id="Seg_4399" s="T481">Poʔto nada kuʔsittə. </ta>
            <ta e="T489" id="Seg_4400" s="T484">Uja iʔgö moləj, amzittə nada. </ta>
            <ta e="T494" id="Seg_4401" s="T489">(Mĭnzitən- mĭrd- mĭrzintə-) nada ((LAUGH)). </ta>
            <ta e="T502" id="Seg_4402" s="T495">(Uja=) Uja (eneʔ), šü nendleʔbə, uja (eneʔ). </ta>
            <ta e="T507" id="Seg_4403" s="T504">Кого еще раз? </ta>
            <ta e="T517" id="Seg_4404" s="T514">А-а.</ta>
            <ta e="T521" id="Seg_4405" s="T517">Šü nendleʔbə, uja eneʔ. </ta>
            <ta e="T524" id="Seg_4406" s="T521">(Puska-) Puskaj pürleʔbə. </ta>
            <ta e="T525" id="Seg_4407" s="T524">((BRK)). </ta>
            <ta e="T529" id="Seg_4408" s="T525">(Šünə=) Šünə endleʔbə uja. </ta>
            <ta e="T530" id="Seg_4409" s="T529">((BRK)). </ta>
            <ta e="T535" id="Seg_4410" s="T530">Amnaʔ döbər, dʼăbaktəraʔ, gijen ibiel. </ta>
            <ta e="T538" id="Seg_4411" s="T535">(Ĭmbi=) Ĭmbi kubial? </ta>
            <ta e="T540" id="Seg_4412" s="T538">Gibər kandəgal? </ta>
            <ta e="T543" id="Seg_4413" s="T540">Dʼijenə kandəgam, keʔbdejleʔ. </ta>
            <ta e="T545" id="Seg_4414" s="T543">Dĭn urgaːba. </ta>
            <ta e="T547" id="Seg_4415" s="T545">Ugaːndə pimniem. </ta>
            <ta e="T552" id="Seg_4416" s="T547">No iʔ kanaʔ, pimniel dăk. </ta>
            <ta e="T554" id="Seg_4417" s="T552">Ну хватит. </ta>
            <ta e="T562" id="Seg_4418" s="T554">Oʔb, šide, nagur, teʔtə, sumna, sejʔpü, (šin-) sejʔpü. </ta>
            <ta e="T571" id="Seg_4419" s="T562">Oʔb, šide, nagur, muktuʔ, sejʔpü, sumna, šinteʔtə, amitun, bʼeʔ. </ta>
            <ta e="T585" id="Seg_4420" s="T573">Bʼeʔ onʼiʔ, bʼeʔ šide, bʼeʔ nagur, bʼeʔ teʔtə, bʼeʔ sumna, bʼeʔ sejʔpü. </ta>
            <ta e="T590" id="Seg_4421" s="T585">Bʼeʔ šinteʔtə, bʼeʔ sejʔpü, bʼeʔ. </ta>
            <ta e="T592" id="Seg_4422" s="T590">Šide (bʼeʔ). </ta>
            <ta e="T595" id="Seg_4423" s="T592">Забыла я ((BRK))… </ta>
            <ta e="T598" id="Seg_4424" s="T595">Surdəsʼtə nada tüžöjəm. </ta>
            <ta e="T601" id="Seg_4425" s="T598">Šedendə kajzittə nada. </ta>
            <ta e="T603" id="Seg_4426" s="T601">Surdəsʼtə nada. </ta>
            <ta e="T605" id="Seg_4427" s="T603">Süt detsittə. </ta>
            <ta e="T608" id="Seg_4428" s="T605">Šojdʼonə enzittə, kămnasʼtə. </ta>
            <ta e="T610" id="Seg_4429" s="T608">Ну, все. </ta>
            <ta e="T611" id="Seg_4430" s="T610">((BRK)). </ta>
            <ta e="T612" id="Seg_4431" s="T611">Ну? </ta>
            <ta e="T616" id="Seg_4432" s="T614">Gibər kandəgal? </ta>
            <ta e="T618" id="Seg_4433" s="T616">Gibər nuʔməleʔbəl? </ta>
            <ta e="T620" id="Seg_4434" s="T618">(Tüžöjmə) naga. </ta>
            <ta e="T624" id="Seg_4435" s="T621">Măndərzittə (šonə-) kalam. </ta>
            <ta e="T626" id="Seg_4436" s="T624">Kanžəbəj mănzʼiʔ! </ta>
            <ta e="T629" id="Seg_4437" s="T626">A to măn pimniem. </ta>
            <ta e="T631" id="Seg_4438" s="T629">Мало, однако. </ta>
            <ta e="T636" id="Seg_4439" s="T634">Zdărowă igel! </ta>
            <ta e="T638" id="Seg_4440" s="T636">Amnaʔ amorzittə! </ta>
            <ta e="T641" id="Seg_4441" s="T638">Ipek lem keʔbdezʼiʔ. </ta>
            <ta e="T642" id="Seg_4442" s="T641">Kajaʔ. </ta>
            <ta e="T644" id="Seg_4443" s="T642">Uja amaʔ! </ta>
            <ta e="T646" id="Seg_4444" s="T644">Keʔbde amaʔ! </ta>
            <ta e="T648" id="Seg_4445" s="T646">Oroma amaʔ! </ta>
            <ta e="T649" id="Seg_4446" s="T648">((BRK)). </ta>
            <ta e="T651" id="Seg_4447" s="T649">Ipek amaʔ! </ta>
            <ta e="T654" id="Seg_4448" s="T651">Segi bü bĭdeʔ! </ta>
            <ta e="T658" id="Seg_4449" s="T654">Sĭreʔpne endə segi bünə. </ta>
            <ta e="T660" id="Seg_4450" s="T658">Süt amnaʔ! </ta>
            <ta e="T661" id="Seg_4451" s="T660">((BRK)). </ta>
            <ta e="T666" id="Seg_4452" s="T661">Kanžəbəj obberəj dʼijenə keʔbde oʔbdəsʼtə. </ta>
            <ta e="T670" id="Seg_4453" s="T666">Kalba nĭŋgəsʼtə, sanə azittə. </ta>
            <ta e="T672" id="Seg_4454" s="T670">Iššo ĭmbi? </ta>
            <ta e="T678" id="Seg_4455" s="T672">(Beške i- i- a-) Beške izittə. </ta>
            <ta e="T679" id="Seg_4456" s="T678">((LAUGH)) ((BRK)). </ta>
            <ta e="T682" id="Seg_4457" s="T679">(Я) тебя жалею. </ta>
            <ta e="T685" id="Seg_4458" s="T682">Măn tănan ajirlaʔbəm. </ta>
            <ta e="T686" id="Seg_4459" s="T685">((BRK)). </ta>
            <ta e="T688" id="Seg_4460" s="T686">((DMG)) Коня запрягай. </ta>
            <ta e="T691" id="Seg_4461" s="T688">Kanaʔ, ine kürereʔ! </ta>
            <ta e="T694" id="Seg_4462" s="T691">Kanžəbəj gibər-nʼibudʼ inezʼiʔ. </ta>
            <ta e="T697" id="Seg_4463" s="T694">(Bü) ((DMG)) (de-) detləbəj. </ta>
            <ta e="T699" id="Seg_4464" s="T697">Paʔi detləbeʔ. </ta>
            <ta e="T701" id="Seg_4465" s="T699">Noʔ detləbəj. </ta>
            <ta e="T703" id="Seg_4466" s="T701">Săloma detləbəj. </ta>
            <ta e="T705" id="Seg_4467" s="T703">Все, наверное. </ta>
            <ta e="T706" id="Seg_4468" s="T705">((BRK)). </ta>
            <ta e="T710" id="Seg_4469" s="T708">Tuganbə šobi. </ta>
            <ta e="T714" id="Seg_4470" s="T710">Nada (m-) măndərzittə kanzittə. </ta>
            <ta e="T715" id="Seg_4471" s="T714">((BRK)). </ta>
            <ta e="T717" id="Seg_4472" s="T715">Nada kăštəsʼtə. </ta>
            <ta e="T719" id="Seg_4473" s="T717">Amorzittə mĭzittə. </ta>
            <ta e="T722" id="Seg_4474" s="T719">Nada kunolzittə dĭzeŋdə. </ta>
            <ta e="T724" id="Seg_4475" s="T722">Больше все. </ta>
            <ta e="T734" id="Seg_4476" s="T724">((BRK)) Oʔb, oʔb, šide, nagur, teʔtə, sumna, sejʔpü, amitun, šinteʔtə, bʼeʔ. </ta>
            <ta e="T739" id="Seg_4477" s="T734">Вот девять-то оставила ((…)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T18" id="Seg_4478" s="T17">i-ʔ</ta>
            <ta e="T19" id="Seg_4479" s="T18">kudo-nza-ʔ</ta>
            <ta e="T23" id="Seg_4480" s="T22">măn</ta>
            <ta e="T24" id="Seg_4481" s="T23">dĭ-m</ta>
            <ta e="T25" id="Seg_4482" s="T24">i-bie-m</ta>
            <ta e="T28" id="Seg_4483" s="T27">dĭn</ta>
            <ta e="T29" id="Seg_4484" s="T28">bar</ta>
            <ta e="T30" id="Seg_4485" s="T29">kudo-nz-laʔbə-ʔjə</ta>
            <ta e="T31" id="Seg_4486" s="T30">măn</ta>
            <ta e="T742" id="Seg_4487" s="T31">kal-la</ta>
            <ta e="T32" id="Seg_4488" s="T742">dʼür-bie-m</ta>
            <ta e="T37" id="Seg_4489" s="T36">iʔgö</ta>
            <ta e="T38" id="Seg_4490" s="T37">pʼaŋdə-bia-l</ta>
            <ta e="T39" id="Seg_4491" s="T38">bar</ta>
            <ta e="T41" id="Seg_4492" s="T40">koŋ</ta>
            <ta e="T42" id="Seg_4493" s="T41">bar</ta>
            <ta e="T43" id="Seg_4494" s="T42">nʼešpək</ta>
            <ta e="T44" id="Seg_4495" s="T43">šo-bi</ta>
            <ta e="T45" id="Seg_4496" s="T44">jezerik</ta>
            <ta e="T46" id="Seg_4497" s="T45">bar</ta>
            <ta e="T54" id="Seg_4498" s="T52">i-ge</ta>
            <ta e="T57" id="Seg_4499" s="T56">sabən</ta>
            <ta e="T58" id="Seg_4500" s="T57">i-ge</ta>
            <ta e="T60" id="Seg_4501" s="T59">kĭški-t</ta>
            <ta e="T61" id="Seg_4502" s="T60">bar</ta>
            <ta e="T72" id="Seg_4503" s="T71">ugaːndə</ta>
            <ta e="T73" id="Seg_4504" s="T72">tăn</ta>
            <ta e="T74" id="Seg_4505" s="T73">sagər</ta>
            <ta e="T75" id="Seg_4506" s="T74">kan-a-ʔ</ta>
            <ta e="T76" id="Seg_4507" s="T75">moltʼa-nə</ta>
            <ta e="T78" id="Seg_4508" s="T77">dĭn</ta>
            <ta e="T79" id="Seg_4509" s="T78">dʼibige</ta>
            <ta e="T80" id="Seg_4510" s="T79">i-ge</ta>
            <ta e="T81" id="Seg_4511" s="T80">i</ta>
            <ta e="T82" id="Seg_4512" s="T81">šĭšəge</ta>
            <ta e="T83" id="Seg_4513" s="T82">bü</ta>
            <ta e="T84" id="Seg_4514" s="T83">sabən</ta>
            <ta e="T85" id="Seg_4515" s="T84">i-ge</ta>
            <ta e="T86" id="Seg_4516" s="T85">păz-aʔ</ta>
            <ta e="T88" id="Seg_4517" s="T87">kĭški-t</ta>
            <ta e="T91" id="Seg_4518" s="T89">to</ta>
            <ta e="T108" id="Seg_4519" s="T107">tüžöj-əʔi</ta>
            <ta e="T109" id="Seg_4520" s="T108">šeden-də</ta>
            <ta e="T112" id="Seg_4521" s="T111">nu-zaŋ</ta>
            <ta e="T113" id="Seg_4522" s="T112">šo-bi-ʔi</ta>
            <ta e="T114" id="Seg_4523" s="T113">amna-ʔ</ta>
            <ta e="T115" id="Seg_4524" s="T114">amor-a-ʔ</ta>
            <ta e="T116" id="Seg_4525" s="T115">padʼi</ta>
            <ta e="T117" id="Seg_4526" s="T116">amor-zittə</ta>
            <ta e="T118" id="Seg_4527" s="T117">axota</ta>
            <ta e="T119" id="Seg_4528" s="T118">gijen</ta>
            <ta e="T120" id="Seg_4529" s="T119">i-bie-l</ta>
            <ta e="T121" id="Seg_4530" s="T120">amna-ʔ</ta>
            <ta e="T122" id="Seg_4531" s="T121">döbər</ta>
            <ta e="T123" id="Seg_4532" s="T122">kuvas</ta>
            <ta e="T124" id="Seg_4533" s="T123">kuza</ta>
            <ta e="T129" id="Seg_4534" s="T128">nada</ta>
            <ta e="T130" id="Seg_4535" s="T129">tura</ta>
            <ta e="T131" id="Seg_4536" s="T130">băzəj-sʼtə</ta>
            <ta e="T132" id="Seg_4537" s="T131">nada</ta>
            <ta e="T133" id="Seg_4538" s="T132">săbərəj-zittə</ta>
            <ta e="T134" id="Seg_4539" s="T133">nʼiʔdə</ta>
            <ta e="T135" id="Seg_4540" s="T134">nʼiʔdə</ta>
            <ta e="T136" id="Seg_4541" s="T135">kan-zittə</ta>
            <ta e="T137" id="Seg_4542" s="T136">tüžöj</ta>
            <ta e="T138" id="Seg_4543" s="T137">surdə-sʼtə</ta>
            <ta e="T139" id="Seg_4544" s="T138">ešši-m</ta>
            <ta e="T140" id="Seg_4545" s="T139">băzəj-sʼtə</ta>
            <ta e="T142" id="Seg_4546" s="T141">ipek</ta>
            <ta e="T143" id="Seg_4547" s="T142">pür-zittə</ta>
            <ta e="T144" id="Seg_4548" s="T143">ipek</ta>
            <ta e="T145" id="Seg_4549" s="T144">nuldə-sʼtə</ta>
            <ta e="T148" id="Seg_4550" s="T146">pür-zittə</ta>
            <ta e="T149" id="Seg_4551" s="T148">sʼel</ta>
            <ta e="T150" id="Seg_4552" s="T149">pür-zittə</ta>
            <ta e="T151" id="Seg_4553" s="T150">nada</ta>
            <ta e="T166" id="Seg_4554" s="T165">segi</ta>
            <ta e="T167" id="Seg_4555" s="T166">bü</ta>
            <ta e="T168" id="Seg_4556" s="T167">nada</ta>
            <ta e="T169" id="Seg_4557" s="T168">mĭnzər-zittə</ta>
            <ta e="T171" id="Seg_4558" s="T169">kirgar-laʔbə</ta>
            <ta e="T172" id="Seg_4559" s="T171">i-ʔ</ta>
            <ta e="T173" id="Seg_4560" s="T172">kurom-a-ʔ</ta>
            <ta e="T174" id="Seg_4561" s="T173">i-ʔ</ta>
            <ta e="T175" id="Seg_4562" s="T174">alom-a-ʔ</ta>
            <ta e="T183" id="Seg_4563" s="T182">măn</ta>
            <ta e="T184" id="Seg_4564" s="T183">dĭn</ta>
            <ta e="T185" id="Seg_4565" s="T184">i-bie-m</ta>
            <ta e="T186" id="Seg_4566" s="T185">dĭn</ta>
            <ta e="T187" id="Seg_4567" s="T186">bar</ta>
            <ta e="T189" id="Seg_4568" s="T188">kudo-nz-laʔbə-ʔjə</ta>
            <ta e="T190" id="Seg_4569" s="T189">măn</ta>
            <ta e="T743" id="Seg_4570" s="T190">kal-la</ta>
            <ta e="T191" id="Seg_4571" s="T743">dʼür-bie-m</ta>
            <ta e="T194" id="Seg_4572" s="T193">uda-l</ta>
            <ta e="T195" id="Seg_4573" s="T194">padʼi</ta>
            <ta e="T196" id="Seg_4574" s="T195">ĭzem-nie</ta>
            <ta e="T201" id="Seg_4575" s="T200">tus</ta>
            <ta e="T202" id="Seg_4576" s="T201">naga</ta>
            <ta e="T203" id="Seg_4577" s="T202">nada</ta>
            <ta e="T204" id="Seg_4578" s="T203">tustʼar-zittə</ta>
            <ta e="T206" id="Seg_4579" s="T204">ĭmbi-zʼiʔ</ta>
            <ta e="T207" id="Seg_4580" s="T206">am-zittə</ta>
            <ta e="T208" id="Seg_4581" s="T207">šamnak</ta>
            <ta e="T209" id="Seg_4582" s="T208">naga</ta>
            <ta e="T210" id="Seg_4583" s="T209">de-ʔ</ta>
            <ta e="T211" id="Seg_4584" s="T210">šamnak</ta>
            <ta e="T212" id="Seg_4585" s="T211">dö</ta>
            <ta e="T213" id="Seg_4586" s="T212">šamnak</ta>
            <ta e="T214" id="Seg_4587" s="T213">mĭ-bie-m</ta>
            <ta e="T215" id="Seg_4588" s="T214">tănan</ta>
            <ta e="T216" id="Seg_4589" s="T215">büžü</ta>
            <ta e="T217" id="Seg_4590" s="T216">am-a-ʔ</ta>
            <ta e="T222" id="Seg_4591" s="T221">kan-žə-bəj</ta>
            <ta e="T224" id="Seg_4592" s="T223">obberəj</ta>
            <ta e="T225" id="Seg_4593" s="T224">ugaːndə</ta>
            <ta e="T226" id="Seg_4594" s="T225">kuvas</ta>
            <ta e="T227" id="Seg_4595" s="T226">koʔb-saŋ</ta>
            <ta e="T228" id="Seg_4596" s="T227">ugaːndə</ta>
            <ta e="T229" id="Seg_4597" s="T228">kuvas</ta>
            <ta e="T230" id="Seg_4598" s="T229">nʼi-zeŋ</ta>
            <ta e="T231" id="Seg_4599" s="T230">ugaːndə</ta>
            <ta e="T232" id="Seg_4600" s="T231">kuvas</ta>
            <ta e="T233" id="Seg_4601" s="T232">il</ta>
            <ta e="T234" id="Seg_4602" s="T233">ugaːndə</ta>
            <ta e="T235" id="Seg_4603" s="T234">jakšə</ta>
            <ta e="T236" id="Seg_4604" s="T235">kuza</ta>
            <ta e="T237" id="Seg_4605" s="T236">ugaːndə</ta>
            <ta e="T238" id="Seg_4606" s="T237">šaːm-naʔbə</ta>
            <ta e="T239" id="Seg_4607" s="T238">kuza</ta>
            <ta e="T242" id="Seg_4608" s="T241">nada</ta>
            <ta e="T243" id="Seg_4609" s="T242">kunol-zittə</ta>
            <ta e="T245" id="Seg_4610" s="T244">idʼiʔeʔe</ta>
            <ta e="T248" id="Seg_4611" s="T247">axota</ta>
            <ta e="T249" id="Seg_4612" s="T248">ugaːndə</ta>
            <ta e="T250" id="Seg_4613" s="T249">nanə-m</ta>
            <ta e="T251" id="Seg_4614" s="T250">ĭzem-nie</ta>
            <ta e="T252" id="Seg_4615" s="T251">amor-a-ʔ</ta>
            <ta e="T253" id="Seg_4616" s="T252">amna-ʔ</ta>
            <ta e="T254" id="Seg_4617" s="T253">i-ʔ</ta>
            <ta e="T255" id="Seg_4618" s="T254">kürüm-a-ʔ</ta>
            <ta e="T256" id="Seg_4619" s="T255">i-ʔ</ta>
            <ta e="T257" id="Seg_4620" s="T256">alom-a-ʔ</ta>
            <ta e="T259" id="Seg_4621" s="T257">nada</ta>
            <ta e="T260" id="Seg_4622" s="T259">i-zittə</ta>
            <ta e="T261" id="Seg_4623" s="T260">ato</ta>
            <ta e="T269" id="Seg_4624" s="T741">dʼije-nə</ta>
            <ta e="T270" id="Seg_4625" s="T269">kan-zittə</ta>
            <ta e="T271" id="Seg_4626" s="T270">keʔbde</ta>
            <ta e="T272" id="Seg_4627" s="T271">deʔ-sʼittə</ta>
            <ta e="T273" id="Seg_4628" s="T272">sanə</ta>
            <ta e="T274" id="Seg_4629" s="T273">deʔ-sittə</ta>
            <ta e="T275" id="Seg_4630" s="T274">büžü</ta>
            <ta e="T276" id="Seg_4631" s="T275">kan-žə-bəj</ta>
            <ta e="T277" id="Seg_4632" s="T276">šide-göʔ</ta>
            <ta e="T280" id="Seg_4633" s="T279">ĭmbi=nʼibudʼ</ta>
            <ta e="T281" id="Seg_4634" s="T280">kut-la-m</ta>
            <ta e="T282" id="Seg_4635" s="T281">dĭ-m</ta>
            <ta e="T283" id="Seg_4636" s="T282">poʔto</ta>
            <ta e="T284" id="Seg_4637" s="T283">kuʔ-pio-m</ta>
            <ta e="T285" id="Seg_4638" s="T284">bar</ta>
            <ta e="T287" id="Seg_4639" s="T286">albuga</ta>
            <ta e="T288" id="Seg_4640" s="T287">kuʔ-pio-m</ta>
            <ta e="T289" id="Seg_4641" s="T288">tažəp</ta>
            <ta e="T290" id="Seg_4642" s="T289">kuʔ-pio-m</ta>
            <ta e="T291" id="Seg_4643" s="T290">sön</ta>
            <ta e="T292" id="Seg_4644" s="T291">kuʔ-pia-m</ta>
            <ta e="T293" id="Seg_4645" s="T292">urgaːba</ta>
            <ta e="T294" id="Seg_4646" s="T293">kuʔ-pia-m</ta>
            <ta e="T299" id="Seg_4647" s="T298">ĭzem-nie</ta>
            <ta e="T300" id="Seg_4648" s="T299">ugaːndə</ta>
            <ta e="T301" id="Seg_4649" s="T300">uda-m</ta>
            <ta e="T302" id="Seg_4650" s="T301">ĭzem-nie</ta>
            <ta e="T303" id="Seg_4651" s="T302">ulu-m</ta>
            <ta e="T304" id="Seg_4652" s="T303">ĭzem-nie</ta>
            <ta e="T305" id="Seg_4653" s="T304">bögəl-bə</ta>
            <ta e="T306" id="Seg_4654" s="T305">ĭzem-nie</ta>
            <ta e="T307" id="Seg_4655" s="T306">üjü-zeŋ-də</ta>
            <ta e="T308" id="Seg_4656" s="T307">ĭzem-nie</ta>
            <ta e="T321" id="Seg_4657" s="T320">tüʔ-sittə</ta>
            <ta e="T322" id="Seg_4658" s="T321">kan-zittə</ta>
            <ta e="T323" id="Seg_4659" s="T322">kĭnzə-sʼtə</ta>
            <ta e="T324" id="Seg_4660" s="T323">nada</ta>
            <ta e="T325" id="Seg_4661" s="T324">kan-zittə</ta>
            <ta e="T327" id="Seg_4662" s="T325">nada</ta>
            <ta e="T341" id="Seg_4663" s="T340">nüke</ta>
            <ta e="T342" id="Seg_4664" s="T341">i</ta>
            <ta e="T343" id="Seg_4665" s="T342">büzʼe</ta>
            <ta e="T344" id="Seg_4666" s="T343">dĭ-zeŋ</ta>
            <ta e="T346" id="Seg_4667" s="T345">es-seŋ-də</ta>
            <ta e="T347" id="Seg_4668" s="T346">i-bi-ʔi</ta>
            <ta e="T348" id="Seg_4669" s="T347">nʼi</ta>
            <ta e="T349" id="Seg_4670" s="T348">i</ta>
            <ta e="T350" id="Seg_4671" s="T349">koʔbdo</ta>
            <ta e="T356" id="Seg_4672" s="T355">amno-bi-ʔi</ta>
            <ta e="T357" id="Seg_4673" s="T356">nüke</ta>
            <ta e="T358" id="Seg_4674" s="T357">i</ta>
            <ta e="T359" id="Seg_4675" s="T358">büzʼe</ta>
            <ta e="T360" id="Seg_4676" s="T359">dĭ-zeŋ-də</ta>
            <ta e="T361" id="Seg_4677" s="T360">es-seŋ-də</ta>
            <ta e="T362" id="Seg_4678" s="T361">i-bi-ʔi</ta>
            <ta e="T363" id="Seg_4679" s="T362">nʼi-t</ta>
            <ta e="T364" id="Seg_4680" s="T363">i</ta>
            <ta e="T365" id="Seg_4681" s="T364">koʔbdo</ta>
            <ta e="T366" id="Seg_4682" s="T365">nʼi</ta>
            <ta e="T367" id="Seg_4683" s="T366">i</ta>
            <ta e="T368" id="Seg_4684" s="T367">koʔbdo</ta>
            <ta e="T369" id="Seg_4685" s="T368">oʔb</ta>
            <ta e="T370" id="Seg_4686" s="T369">šide</ta>
            <ta e="T371" id="Seg_4687" s="T370">nagur</ta>
            <ta e="T372" id="Seg_4688" s="T371">sumna</ta>
            <ta e="T373" id="Seg_4689" s="T372">muktuʔ</ta>
            <ta e="T374" id="Seg_4690" s="T373">sejʔpü</ta>
            <ta e="T375" id="Seg_4691" s="T374">šinteʔtə</ta>
            <ta e="T376" id="Seg_4692" s="T375">amitun</ta>
            <ta e="T377" id="Seg_4693" s="T376">bʼeʔ</ta>
            <ta e="T378" id="Seg_4694" s="T377">bʼeʔ</ta>
            <ta e="T379" id="Seg_4695" s="T378">onʼiʔ</ta>
            <ta e="T380" id="Seg_4696" s="T379">bʼeʔ</ta>
            <ta e="T381" id="Seg_4697" s="T380">šide</ta>
            <ta e="T382" id="Seg_4698" s="T381">bʼeʔ</ta>
            <ta e="T383" id="Seg_4699" s="T382">nagur</ta>
            <ta e="T384" id="Seg_4700" s="T383">bʼeʔ</ta>
            <ta e="T385" id="Seg_4701" s="T384">teʔtə</ta>
            <ta e="T386" id="Seg_4702" s="T385">bʼeʔ</ta>
            <ta e="T387" id="Seg_4703" s="T386">sumna</ta>
            <ta e="T388" id="Seg_4704" s="T387">bʼeʔ</ta>
            <ta e="T389" id="Seg_4705" s="T388">muktuʔ</ta>
            <ta e="T390" id="Seg_4706" s="T389">bʼeʔ</ta>
            <ta e="T391" id="Seg_4707" s="T390">sejʔpü</ta>
            <ta e="T392" id="Seg_4708" s="T391">bʼeʔ</ta>
            <ta e="T393" id="Seg_4709" s="T392">šinteʔtə</ta>
            <ta e="T395" id="Seg_4710" s="T393">bʼeʔ</ta>
            <ta e="T419" id="Seg_4711" s="T418">oʔb</ta>
            <ta e="T420" id="Seg_4712" s="T419">šide</ta>
            <ta e="T421" id="Seg_4713" s="T420">nagur</ta>
            <ta e="T422" id="Seg_4714" s="T421">muktuʔ</ta>
            <ta e="T423" id="Seg_4715" s="T422">sejʔpü</ta>
            <ta e="T424" id="Seg_4716" s="T423">šinteʔtə</ta>
            <ta e="T425" id="Seg_4717" s="T424">amitun</ta>
            <ta e="T427" id="Seg_4718" s="T426">măna</ta>
            <ta e="T428" id="Seg_4719" s="T427">nada</ta>
            <ta e="T429" id="Seg_4720" s="T428">monoʔko-zittə</ta>
            <ta e="T430" id="Seg_4721" s="T429">a</ta>
            <ta e="T431" id="Seg_4722" s="T430">šindi-m</ta>
            <ta e="T432" id="Seg_4723" s="T431">dĭ</ta>
            <ta e="T433" id="Seg_4724" s="T432">koʔbdo</ta>
            <ta e="T434" id="Seg_4725" s="T433">kan-a-ʔ</ta>
            <ta e="T435" id="Seg_4726" s="T434">monoʔko-t</ta>
            <ta e="T439" id="Seg_4727" s="T438">tura-gən</ta>
            <ta e="T440" id="Seg_4728" s="T439">amno-laʔbə-m</ta>
            <ta e="T441" id="Seg_4729" s="T440">oj</ta>
            <ta e="T442" id="Seg_4730" s="T441">bar</ta>
            <ta e="T443" id="Seg_4731" s="T442">üjü-m</ta>
            <ta e="T444" id="Seg_4732" s="T443">kutʼüm-nie</ta>
            <ta e="T446" id="Seg_4733" s="T444">nada</ta>
            <ta e="T448" id="Seg_4734" s="T447">ulu-m</ta>
            <ta e="T449" id="Seg_4735" s="T448">ĭzem-nie</ta>
            <ta e="T450" id="Seg_4736" s="T449">bögel-bə</ta>
            <ta e="T451" id="Seg_4737" s="T450">ĭzem-nie</ta>
            <ta e="T452" id="Seg_4738" s="T451">uda-m</ta>
            <ta e="T453" id="Seg_4739" s="T452">ĭzem-nie</ta>
            <ta e="T454" id="Seg_4740" s="T453">sʼima-m</ta>
            <ta e="T455" id="Seg_4741" s="T454">ĭzem-nie</ta>
            <ta e="T456" id="Seg_4742" s="T455">ku</ta>
            <ta e="T457" id="Seg_4743" s="T456">ĭzem-nie</ta>
            <ta e="T458" id="Seg_4744" s="T457">üjü-m</ta>
            <ta e="T459" id="Seg_4745" s="T458">ĭzem-nie</ta>
            <ta e="T460" id="Seg_4746" s="T459">köten-bə</ta>
            <ta e="T461" id="Seg_4747" s="T460">ĭzem-nie</ta>
            <ta e="T463" id="Seg_4748" s="T462">dʼok</ta>
            <ta e="T465" id="Seg_4749" s="T464">dʼije-nə</ta>
            <ta e="T466" id="Seg_4750" s="T465">nada</ta>
            <ta e="T467" id="Seg_4751" s="T466">kan-zittə</ta>
            <ta e="T468" id="Seg_4752" s="T467">albuga</ta>
            <ta e="T469" id="Seg_4753" s="T468">kuʔ-sittə</ta>
            <ta e="T470" id="Seg_4754" s="T469">urgaːba</ta>
            <ta e="T471" id="Seg_4755" s="T470">kuʔ-sittə</ta>
            <ta e="T472" id="Seg_4756" s="T471">nada</ta>
            <ta e="T473" id="Seg_4757" s="T472">tažəp</ta>
            <ta e="T474" id="Seg_4758" s="T473">kuʔ-sittə</ta>
            <ta e="T475" id="Seg_4759" s="T474">nada</ta>
            <ta e="T476" id="Seg_4760" s="T475">bulan</ta>
            <ta e="T477" id="Seg_4761" s="T476">kuʔ-sittə</ta>
            <ta e="T478" id="Seg_4762" s="T477">nada</ta>
            <ta e="T479" id="Seg_4763" s="T478">sön</ta>
            <ta e="T480" id="Seg_4764" s="T479">kuʔ-sittə</ta>
            <ta e="T481" id="Seg_4765" s="T480">nada</ta>
            <ta e="T482" id="Seg_4766" s="T481">poʔto</ta>
            <ta e="T483" id="Seg_4767" s="T482">nada</ta>
            <ta e="T484" id="Seg_4768" s="T483">kuʔ-sittə</ta>
            <ta e="T485" id="Seg_4769" s="T484">uja</ta>
            <ta e="T486" id="Seg_4770" s="T485">iʔgö</ta>
            <ta e="T487" id="Seg_4771" s="T486">mo-lə-j</ta>
            <ta e="T488" id="Seg_4772" s="T487">am-zittə</ta>
            <ta e="T489" id="Seg_4773" s="T488">nada</ta>
            <ta e="T493" id="Seg_4774" s="T492">nada</ta>
            <ta e="T496" id="Seg_4775" s="T495">uja</ta>
            <ta e="T498" id="Seg_4776" s="T497">en-e-ʔ</ta>
            <ta e="T499" id="Seg_4777" s="T498">šü</ta>
            <ta e="T500" id="Seg_4778" s="T499">nend-leʔbə</ta>
            <ta e="T501" id="Seg_4779" s="T500">uja</ta>
            <ta e="T502" id="Seg_4780" s="T501">en-e-ʔ</ta>
            <ta e="T518" id="Seg_4781" s="T517">šü</ta>
            <ta e="T519" id="Seg_4782" s="T518">nend-leʔbə</ta>
            <ta e="T520" id="Seg_4783" s="T519">uja</ta>
            <ta e="T521" id="Seg_4784" s="T520">en-e-ʔ</ta>
            <ta e="T523" id="Seg_4785" s="T522">puskaj</ta>
            <ta e="T524" id="Seg_4786" s="T523">pür-leʔbə</ta>
            <ta e="T526" id="Seg_4787" s="T525">šü-nə</ta>
            <ta e="T527" id="Seg_4788" s="T526">šü-nə</ta>
            <ta e="T528" id="Seg_4789" s="T527">end-leʔbə</ta>
            <ta e="T529" id="Seg_4790" s="T528">uja</ta>
            <ta e="T531" id="Seg_4791" s="T530">amna-ʔ</ta>
            <ta e="T532" id="Seg_4792" s="T531">döbər</ta>
            <ta e="T533" id="Seg_4793" s="T532">dʼăbaktər-a-ʔ</ta>
            <ta e="T534" id="Seg_4794" s="T533">gijen</ta>
            <ta e="T535" id="Seg_4795" s="T534">i-bie-l</ta>
            <ta e="T536" id="Seg_4796" s="T535">ĭmbi</ta>
            <ta e="T537" id="Seg_4797" s="T536">ĭmbi</ta>
            <ta e="T538" id="Seg_4798" s="T537">ku-bia-l</ta>
            <ta e="T539" id="Seg_4799" s="T538">gibər</ta>
            <ta e="T540" id="Seg_4800" s="T539">kandə-ga-l</ta>
            <ta e="T541" id="Seg_4801" s="T540">dʼije-nə</ta>
            <ta e="T542" id="Seg_4802" s="T541">kandə-ga-m</ta>
            <ta e="T543" id="Seg_4803" s="T542">keʔbde-j-leʔ</ta>
            <ta e="T544" id="Seg_4804" s="T543">dĭn</ta>
            <ta e="T545" id="Seg_4805" s="T544">urgaːba</ta>
            <ta e="T546" id="Seg_4806" s="T545">ugaːndə</ta>
            <ta e="T547" id="Seg_4807" s="T546">pim-nie-m</ta>
            <ta e="T548" id="Seg_4808" s="T547">no</ta>
            <ta e="T549" id="Seg_4809" s="T548">i-ʔ</ta>
            <ta e="T550" id="Seg_4810" s="T549">kan-a-ʔ</ta>
            <ta e="T551" id="Seg_4811" s="T550">pim-nie-l</ta>
            <ta e="T552" id="Seg_4812" s="T551">dăk</ta>
            <ta e="T555" id="Seg_4813" s="T554">oʔb</ta>
            <ta e="T556" id="Seg_4814" s="T555">šide</ta>
            <ta e="T557" id="Seg_4815" s="T556">nagur</ta>
            <ta e="T558" id="Seg_4816" s="T557">teʔtə</ta>
            <ta e="T559" id="Seg_4817" s="T558">sumna</ta>
            <ta e="T560" id="Seg_4818" s="T559">sejʔpü</ta>
            <ta e="T562" id="Seg_4819" s="T561">sejʔpü</ta>
            <ta e="T563" id="Seg_4820" s="T562">oʔb</ta>
            <ta e="T564" id="Seg_4821" s="T563">šide</ta>
            <ta e="T565" id="Seg_4822" s="T564">nagur</ta>
            <ta e="T566" id="Seg_4823" s="T565">muktuʔ</ta>
            <ta e="T567" id="Seg_4824" s="T566">sejʔpü</ta>
            <ta e="T568" id="Seg_4825" s="T567">sumna</ta>
            <ta e="T569" id="Seg_4826" s="T568">šinteʔtə</ta>
            <ta e="T570" id="Seg_4827" s="T569">amitun</ta>
            <ta e="T571" id="Seg_4828" s="T570">bʼeʔ</ta>
            <ta e="T574" id="Seg_4829" s="T573">bʼeʔ</ta>
            <ta e="T575" id="Seg_4830" s="T574">onʼiʔ</ta>
            <ta e="T576" id="Seg_4831" s="T575">bʼeʔ</ta>
            <ta e="T577" id="Seg_4832" s="T576">šide</ta>
            <ta e="T578" id="Seg_4833" s="T577">bʼeʔ</ta>
            <ta e="T579" id="Seg_4834" s="T578">nagur</ta>
            <ta e="T580" id="Seg_4835" s="T579">bʼeʔ</ta>
            <ta e="T581" id="Seg_4836" s="T580">teʔtə</ta>
            <ta e="T582" id="Seg_4837" s="T581">bʼeʔ</ta>
            <ta e="T583" id="Seg_4838" s="T582">sumna</ta>
            <ta e="T584" id="Seg_4839" s="T583">bʼeʔ</ta>
            <ta e="T585" id="Seg_4840" s="T584">sejʔpü</ta>
            <ta e="T586" id="Seg_4841" s="T585">bʼeʔ</ta>
            <ta e="T587" id="Seg_4842" s="T586">šinteʔtə</ta>
            <ta e="T588" id="Seg_4843" s="T587">bʼeʔ</ta>
            <ta e="T589" id="Seg_4844" s="T588">sejʔpü</ta>
            <ta e="T590" id="Seg_4845" s="T589">bʼeʔ</ta>
            <ta e="T591" id="Seg_4846" s="T590">šide</ta>
            <ta e="T592" id="Seg_4847" s="T591">bʼeʔ</ta>
            <ta e="T596" id="Seg_4848" s="T595">surdə-sʼtə</ta>
            <ta e="T597" id="Seg_4849" s="T596">nada</ta>
            <ta e="T598" id="Seg_4850" s="T597">tüžöj-ə-m</ta>
            <ta e="T599" id="Seg_4851" s="T598">šeden-də</ta>
            <ta e="T600" id="Seg_4852" s="T599">kaj-zittə</ta>
            <ta e="T601" id="Seg_4853" s="T600">nada</ta>
            <ta e="T602" id="Seg_4854" s="T601">surdə-sʼtə</ta>
            <ta e="T603" id="Seg_4855" s="T602">nada</ta>
            <ta e="T604" id="Seg_4856" s="T603">süt</ta>
            <ta e="T605" id="Seg_4857" s="T604">det-sittə</ta>
            <ta e="T606" id="Seg_4858" s="T605">šojdʼo-nə</ta>
            <ta e="T607" id="Seg_4859" s="T606">en-zittə</ta>
            <ta e="T608" id="Seg_4860" s="T607">kămna-sʼtə</ta>
            <ta e="T615" id="Seg_4861" s="T614">gibər</ta>
            <ta e="T616" id="Seg_4862" s="T615">kandə-ga-l</ta>
            <ta e="T617" id="Seg_4863" s="T616">gibər</ta>
            <ta e="T618" id="Seg_4864" s="T617">nuʔmə-leʔbə-l</ta>
            <ta e="T619" id="Seg_4865" s="T618">tüžöj-mə</ta>
            <ta e="T620" id="Seg_4866" s="T619">naga</ta>
            <ta e="T622" id="Seg_4867" s="T621">măndə-r-zittə</ta>
            <ta e="T624" id="Seg_4868" s="T623">ka-la-m</ta>
            <ta e="T625" id="Seg_4869" s="T624">kan-žə-bəj</ta>
            <ta e="T626" id="Seg_4870" s="T625">măn-zʼiʔ</ta>
            <ta e="T627" id="Seg_4871" s="T626">ato</ta>
            <ta e="T628" id="Seg_4872" s="T627">măn</ta>
            <ta e="T629" id="Seg_4873" s="T628">pim-nie-m</ta>
            <ta e="T635" id="Seg_4874" s="T634">zdărowă</ta>
            <ta e="T636" id="Seg_4875" s="T635">i-ge-l</ta>
            <ta e="T637" id="Seg_4876" s="T636">amna-ʔ</ta>
            <ta e="T638" id="Seg_4877" s="T637">amor-zittə</ta>
            <ta e="T639" id="Seg_4878" s="T638">ipek</ta>
            <ta e="T640" id="Seg_4879" s="T639">lem</ta>
            <ta e="T641" id="Seg_4880" s="T640">keʔbde-zʼiʔ</ta>
            <ta e="T642" id="Seg_4881" s="T641">kajaʔ</ta>
            <ta e="T643" id="Seg_4882" s="T642">uja</ta>
            <ta e="T644" id="Seg_4883" s="T643">am-a-ʔ</ta>
            <ta e="T645" id="Seg_4884" s="T644">keʔbde</ta>
            <ta e="T646" id="Seg_4885" s="T645">am-a-ʔ</ta>
            <ta e="T647" id="Seg_4886" s="T646">oroma</ta>
            <ta e="T648" id="Seg_4887" s="T647">am-a-ʔ</ta>
            <ta e="T650" id="Seg_4888" s="T649">ipek</ta>
            <ta e="T651" id="Seg_4889" s="T650">am-a-ʔ</ta>
            <ta e="T652" id="Seg_4890" s="T651">segi</ta>
            <ta e="T653" id="Seg_4891" s="T652">bü</ta>
            <ta e="T654" id="Seg_4892" s="T653">bĭd-e-ʔ</ta>
            <ta e="T655" id="Seg_4893" s="T654">sĭreʔpne</ta>
            <ta e="T656" id="Seg_4894" s="T655">en-də</ta>
            <ta e="T657" id="Seg_4895" s="T656">segi</ta>
            <ta e="T658" id="Seg_4896" s="T657">bü-nə</ta>
            <ta e="T659" id="Seg_4897" s="T658">süt</ta>
            <ta e="T660" id="Seg_4898" s="T659">amna-ʔ</ta>
            <ta e="T662" id="Seg_4899" s="T661">kan-žə-bəj</ta>
            <ta e="T663" id="Seg_4900" s="T662">obberəj</ta>
            <ta e="T664" id="Seg_4901" s="T663">dʼije-nə</ta>
            <ta e="T665" id="Seg_4902" s="T664">keʔbde</ta>
            <ta e="T666" id="Seg_4903" s="T665">oʔbdə-sʼtə</ta>
            <ta e="T667" id="Seg_4904" s="T666">kalba</ta>
            <ta e="T668" id="Seg_4905" s="T667">nĭŋgə-sʼtə</ta>
            <ta e="T669" id="Seg_4906" s="T668">sanə</ta>
            <ta e="T670" id="Seg_4907" s="T669">a-zittə</ta>
            <ta e="T671" id="Seg_4908" s="T670">iššo</ta>
            <ta e="T672" id="Seg_4909" s="T671">ĭmbi</ta>
            <ta e="T673" id="Seg_4910" s="T672">beške</ta>
            <ta e="T677" id="Seg_4911" s="T676">beške</ta>
            <ta e="T678" id="Seg_4912" s="T677">i-zittə</ta>
            <ta e="T683" id="Seg_4913" s="T682">măn</ta>
            <ta e="T684" id="Seg_4914" s="T683">tănan</ta>
            <ta e="T685" id="Seg_4915" s="T684">ajir-laʔbə-m</ta>
            <ta e="T689" id="Seg_4916" s="T688">kan-a-ʔ</ta>
            <ta e="T690" id="Seg_4917" s="T689">ine</ta>
            <ta e="T691" id="Seg_4918" s="T690">kürer-e-ʔ</ta>
            <ta e="T692" id="Seg_4919" s="T691">kan-žə-bəj</ta>
            <ta e="T693" id="Seg_4920" s="T692">gibər=nʼibudʼ</ta>
            <ta e="T694" id="Seg_4921" s="T693">ine-zʼiʔ</ta>
            <ta e="T695" id="Seg_4922" s="T694">bü</ta>
            <ta e="T697" id="Seg_4923" s="T696">det-lə-bəj</ta>
            <ta e="T698" id="Seg_4924" s="T697">pa-ʔi</ta>
            <ta e="T699" id="Seg_4925" s="T698">det-lə-beʔ</ta>
            <ta e="T700" id="Seg_4926" s="T699">noʔ</ta>
            <ta e="T701" id="Seg_4927" s="T700">det-lə-bəj</ta>
            <ta e="T702" id="Seg_4928" s="T701">săloma</ta>
            <ta e="T703" id="Seg_4929" s="T702">det-lə-bəj</ta>
            <ta e="T709" id="Seg_4930" s="T708">tugan-bə</ta>
            <ta e="T710" id="Seg_4931" s="T709">šo-bi</ta>
            <ta e="T711" id="Seg_4932" s="T710">nada</ta>
            <ta e="T713" id="Seg_4933" s="T712">măndə-r-zittə</ta>
            <ta e="T714" id="Seg_4934" s="T713">kan-zittə</ta>
            <ta e="T716" id="Seg_4935" s="T715">nada</ta>
            <ta e="T717" id="Seg_4936" s="T716">kăštə-sʼtə</ta>
            <ta e="T718" id="Seg_4937" s="T717">amor-zittə</ta>
            <ta e="T719" id="Seg_4938" s="T718">mĭ-zittə</ta>
            <ta e="T720" id="Seg_4939" s="T719">nada</ta>
            <ta e="T721" id="Seg_4940" s="T720">kunol-zittə</ta>
            <ta e="T722" id="Seg_4941" s="T721">dĭ-zeŋ-də</ta>
            <ta e="T725" id="Seg_4942" s="T724">oʔb</ta>
            <ta e="T726" id="Seg_4943" s="T725">oʔb</ta>
            <ta e="T727" id="Seg_4944" s="T726">šide</ta>
            <ta e="T728" id="Seg_4945" s="T727">nagur</ta>
            <ta e="T729" id="Seg_4946" s="T728">teʔtə</ta>
            <ta e="T730" id="Seg_4947" s="T729">sumna</ta>
            <ta e="T731" id="Seg_4948" s="T730">sejʔpü</ta>
            <ta e="T732" id="Seg_4949" s="T731">amitun</ta>
            <ta e="T733" id="Seg_4950" s="T732">šinteʔtə</ta>
            <ta e="T734" id="Seg_4951" s="T733">bʼeʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T18" id="Seg_4952" s="T17">e-ʔ</ta>
            <ta e="T19" id="Seg_4953" s="T18">kudo-nzə-ʔ</ta>
            <ta e="T23" id="Seg_4954" s="T22">măn</ta>
            <ta e="T24" id="Seg_4955" s="T23">dĭ-m</ta>
            <ta e="T25" id="Seg_4956" s="T24">i-bi-m</ta>
            <ta e="T28" id="Seg_4957" s="T27">dĭn</ta>
            <ta e="T29" id="Seg_4958" s="T28">bar</ta>
            <ta e="T30" id="Seg_4959" s="T29">kudo-nzə-laʔbə-jəʔ</ta>
            <ta e="T31" id="Seg_4960" s="T30">măn</ta>
            <ta e="T742" id="Seg_4961" s="T31">kan-lAʔ</ta>
            <ta e="T32" id="Seg_4962" s="T742">tʼür-bi-m</ta>
            <ta e="T37" id="Seg_4963" s="T36">iʔgö</ta>
            <ta e="T38" id="Seg_4964" s="T37">pʼaŋdə-bi-l</ta>
            <ta e="T39" id="Seg_4965" s="T38">bar</ta>
            <ta e="T41" id="Seg_4966" s="T40">koŋ</ta>
            <ta e="T42" id="Seg_4967" s="T41">bar</ta>
            <ta e="T43" id="Seg_4968" s="T42">nʼešpək</ta>
            <ta e="T44" id="Seg_4969" s="T43">šo-bi</ta>
            <ta e="T45" id="Seg_4970" s="T44">jezerik</ta>
            <ta e="T46" id="Seg_4971" s="T45">bar</ta>
            <ta e="T54" id="Seg_4972" s="T52">i-gA</ta>
            <ta e="T57" id="Seg_4973" s="T56">sabən</ta>
            <ta e="T58" id="Seg_4974" s="T57">i-gA</ta>
            <ta e="T60" id="Seg_4975" s="T59">kĭškə-t</ta>
            <ta e="T61" id="Seg_4976" s="T60">bar</ta>
            <ta e="T72" id="Seg_4977" s="T71">ugaːndə</ta>
            <ta e="T73" id="Seg_4978" s="T72">tăn</ta>
            <ta e="T74" id="Seg_4979" s="T73">sagər</ta>
            <ta e="T75" id="Seg_4980" s="T74">kan-ə-ʔ</ta>
            <ta e="T76" id="Seg_4981" s="T75">multʼa-Tə</ta>
            <ta e="T78" id="Seg_4982" s="T77">dĭn</ta>
            <ta e="T79" id="Seg_4983" s="T78">dʼibige</ta>
            <ta e="T80" id="Seg_4984" s="T79">i-gA</ta>
            <ta e="T81" id="Seg_4985" s="T80">i</ta>
            <ta e="T82" id="Seg_4986" s="T81">šišəge</ta>
            <ta e="T83" id="Seg_4987" s="T82">bü</ta>
            <ta e="T84" id="Seg_4988" s="T83">sabən</ta>
            <ta e="T85" id="Seg_4989" s="T84">i-gA</ta>
            <ta e="T86" id="Seg_4990" s="T85">bazə-ʔ</ta>
            <ta e="T88" id="Seg_4991" s="T87">kĭškə-t</ta>
            <ta e="T91" id="Seg_4992" s="T89">to</ta>
            <ta e="T108" id="Seg_4993" s="T107">tüžöj-jəʔ</ta>
            <ta e="T109" id="Seg_4994" s="T108">šeden-də</ta>
            <ta e="T112" id="Seg_4995" s="T111">nu-zAŋ</ta>
            <ta e="T113" id="Seg_4996" s="T112">šo-bi-jəʔ</ta>
            <ta e="T114" id="Seg_4997" s="T113">amnə-ʔ</ta>
            <ta e="T115" id="Seg_4998" s="T114">amor-ə-ʔ</ta>
            <ta e="T116" id="Seg_4999" s="T115">padʼi</ta>
            <ta e="T117" id="Seg_5000" s="T116">amor-zittə</ta>
            <ta e="T118" id="Seg_5001" s="T117">axota</ta>
            <ta e="T119" id="Seg_5002" s="T118">gijen</ta>
            <ta e="T120" id="Seg_5003" s="T119">i-bi-l</ta>
            <ta e="T121" id="Seg_5004" s="T120">amnə-ʔ</ta>
            <ta e="T122" id="Seg_5005" s="T121">döbər</ta>
            <ta e="T123" id="Seg_5006" s="T122">kuvas</ta>
            <ta e="T124" id="Seg_5007" s="T123">kuza</ta>
            <ta e="T129" id="Seg_5008" s="T128">nadə</ta>
            <ta e="T130" id="Seg_5009" s="T129">tura</ta>
            <ta e="T131" id="Seg_5010" s="T130">băzəj-zittə</ta>
            <ta e="T132" id="Seg_5011" s="T131">nadə</ta>
            <ta e="T133" id="Seg_5012" s="T132">săbərəjʔdə-zittə</ta>
            <ta e="T134" id="Seg_5013" s="T133">nʼiʔdə</ta>
            <ta e="T135" id="Seg_5014" s="T134">nʼiʔdə</ta>
            <ta e="T136" id="Seg_5015" s="T135">kan-zittə</ta>
            <ta e="T137" id="Seg_5016" s="T136">tüžöj</ta>
            <ta e="T138" id="Seg_5017" s="T137">surdo-zittə</ta>
            <ta e="T139" id="Seg_5018" s="T138">ešši-m</ta>
            <ta e="T140" id="Seg_5019" s="T139">băzəj-zittə</ta>
            <ta e="T142" id="Seg_5020" s="T141">ipek</ta>
            <ta e="T143" id="Seg_5021" s="T142">pür-zittə</ta>
            <ta e="T144" id="Seg_5022" s="T143">ipek</ta>
            <ta e="T145" id="Seg_5023" s="T144">nuldə-zittə</ta>
            <ta e="T148" id="Seg_5024" s="T146">pür-zittə</ta>
            <ta e="T149" id="Seg_5025" s="T148">sil</ta>
            <ta e="T150" id="Seg_5026" s="T149">pür-zittə</ta>
            <ta e="T151" id="Seg_5027" s="T150">nadə</ta>
            <ta e="T166" id="Seg_5028" s="T165">segi</ta>
            <ta e="T167" id="Seg_5029" s="T166">bü</ta>
            <ta e="T168" id="Seg_5030" s="T167">nadə</ta>
            <ta e="T169" id="Seg_5031" s="T168">mĭnzər-zittə</ta>
            <ta e="T171" id="Seg_5032" s="T169">kirgaːr-laʔbə</ta>
            <ta e="T172" id="Seg_5033" s="T171">e-ʔ</ta>
            <ta e="T173" id="Seg_5034" s="T172">kurom-ə-ʔ</ta>
            <ta e="T174" id="Seg_5035" s="T173">e-ʔ</ta>
            <ta e="T175" id="Seg_5036" s="T174">alom-ə-ʔ</ta>
            <ta e="T183" id="Seg_5037" s="T182">măn</ta>
            <ta e="T184" id="Seg_5038" s="T183">dĭn</ta>
            <ta e="T185" id="Seg_5039" s="T184">i-bi-m</ta>
            <ta e="T186" id="Seg_5040" s="T185">dĭn</ta>
            <ta e="T187" id="Seg_5041" s="T186">bar</ta>
            <ta e="T189" id="Seg_5042" s="T188">kudo-nzə-laʔbə-jəʔ</ta>
            <ta e="T190" id="Seg_5043" s="T189">măn</ta>
            <ta e="T743" id="Seg_5044" s="T190">kan-lAʔ</ta>
            <ta e="T191" id="Seg_5045" s="T743">tʼür-bi-m</ta>
            <ta e="T194" id="Seg_5046" s="T193">uda-l</ta>
            <ta e="T195" id="Seg_5047" s="T194">padʼi</ta>
            <ta e="T196" id="Seg_5048" s="T195">ĭzem-liA</ta>
            <ta e="T201" id="Seg_5049" s="T200">tus</ta>
            <ta e="T202" id="Seg_5050" s="T201">naga</ta>
            <ta e="T203" id="Seg_5051" s="T202">nadə</ta>
            <ta e="T204" id="Seg_5052" s="T203">tustʼar-zittə</ta>
            <ta e="T206" id="Seg_5053" s="T204">ĭmbi-ziʔ</ta>
            <ta e="T207" id="Seg_5054" s="T206">am-zittə</ta>
            <ta e="T208" id="Seg_5055" s="T207">šamnak</ta>
            <ta e="T209" id="Seg_5056" s="T208">naga</ta>
            <ta e="T210" id="Seg_5057" s="T209">det-ʔ</ta>
            <ta e="T211" id="Seg_5058" s="T210">šamnak</ta>
            <ta e="T212" id="Seg_5059" s="T211">dö</ta>
            <ta e="T213" id="Seg_5060" s="T212">šamnak</ta>
            <ta e="T214" id="Seg_5061" s="T213">mĭ-bi-m</ta>
            <ta e="T215" id="Seg_5062" s="T214">tănan</ta>
            <ta e="T216" id="Seg_5063" s="T215">büžü</ta>
            <ta e="T217" id="Seg_5064" s="T216">am-ə-ʔ</ta>
            <ta e="T222" id="Seg_5065" s="T221">kan-žə-bəj</ta>
            <ta e="T224" id="Seg_5066" s="T223">obberəj</ta>
            <ta e="T225" id="Seg_5067" s="T224">ugaːndə</ta>
            <ta e="T226" id="Seg_5068" s="T225">kuvas</ta>
            <ta e="T227" id="Seg_5069" s="T226">koʔbdo-zAŋ</ta>
            <ta e="T228" id="Seg_5070" s="T227">ugaːndə</ta>
            <ta e="T229" id="Seg_5071" s="T228">kuvas</ta>
            <ta e="T230" id="Seg_5072" s="T229">nʼi-zAŋ</ta>
            <ta e="T231" id="Seg_5073" s="T230">ugaːndə</ta>
            <ta e="T232" id="Seg_5074" s="T231">kuvas</ta>
            <ta e="T233" id="Seg_5075" s="T232">il</ta>
            <ta e="T234" id="Seg_5076" s="T233">ugaːndə</ta>
            <ta e="T235" id="Seg_5077" s="T234">jakšə</ta>
            <ta e="T236" id="Seg_5078" s="T235">kuza</ta>
            <ta e="T237" id="Seg_5079" s="T236">ugaːndə</ta>
            <ta e="T238" id="Seg_5080" s="T237">šʼaːm-laʔbə</ta>
            <ta e="T239" id="Seg_5081" s="T238">kuza</ta>
            <ta e="T242" id="Seg_5082" s="T241">nadə</ta>
            <ta e="T243" id="Seg_5083" s="T242">kunol-zittə</ta>
            <ta e="T245" id="Seg_5084" s="T244">idʼiʔeʔe</ta>
            <ta e="T248" id="Seg_5085" s="T247">axota</ta>
            <ta e="T249" id="Seg_5086" s="T248">ugaːndə</ta>
            <ta e="T250" id="Seg_5087" s="T249">nanə-m</ta>
            <ta e="T251" id="Seg_5088" s="T250">ĭzem-liA</ta>
            <ta e="T252" id="Seg_5089" s="T251">amor-ə-ʔ</ta>
            <ta e="T253" id="Seg_5090" s="T252">amnə-ʔ</ta>
            <ta e="T254" id="Seg_5091" s="T253">e-ʔ</ta>
            <ta e="T255" id="Seg_5092" s="T254">kürüm-ə-ʔ</ta>
            <ta e="T256" id="Seg_5093" s="T255">e-ʔ</ta>
            <ta e="T257" id="Seg_5094" s="T256">alom-ə-ʔ</ta>
            <ta e="T259" id="Seg_5095" s="T257">nadə</ta>
            <ta e="T260" id="Seg_5096" s="T259">i-zittə</ta>
            <ta e="T261" id="Seg_5097" s="T260">ato</ta>
            <ta e="T269" id="Seg_5098" s="T741">dʼije-Tə</ta>
            <ta e="T270" id="Seg_5099" s="T269">kan-zittə</ta>
            <ta e="T271" id="Seg_5100" s="T270">keʔbde</ta>
            <ta e="T272" id="Seg_5101" s="T271">det-zittə</ta>
            <ta e="T273" id="Seg_5102" s="T272">sanə</ta>
            <ta e="T274" id="Seg_5103" s="T273">det-zittə</ta>
            <ta e="T275" id="Seg_5104" s="T274">büžü</ta>
            <ta e="T276" id="Seg_5105" s="T275">kan-žə-bəj</ta>
            <ta e="T277" id="Seg_5106" s="T276">šide-göʔ</ta>
            <ta e="T280" id="Seg_5107" s="T279">ĭmbi=nʼibudʼ</ta>
            <ta e="T281" id="Seg_5108" s="T280">kut-lV-m</ta>
            <ta e="T282" id="Seg_5109" s="T281">dĭ-m</ta>
            <ta e="T283" id="Seg_5110" s="T282">poʔto</ta>
            <ta e="T284" id="Seg_5111" s="T283">kut-bi-m</ta>
            <ta e="T285" id="Seg_5112" s="T284">bar</ta>
            <ta e="T287" id="Seg_5113" s="T286">albuga</ta>
            <ta e="T288" id="Seg_5114" s="T287">kut-bi-m</ta>
            <ta e="T289" id="Seg_5115" s="T288">tažəp</ta>
            <ta e="T290" id="Seg_5116" s="T289">kut-bi-m</ta>
            <ta e="T291" id="Seg_5117" s="T290">sön</ta>
            <ta e="T292" id="Seg_5118" s="T291">kut-bi-m</ta>
            <ta e="T293" id="Seg_5119" s="T292">urgaːba</ta>
            <ta e="T294" id="Seg_5120" s="T293">kut-bi-m</ta>
            <ta e="T299" id="Seg_5121" s="T298">ĭzem-liA</ta>
            <ta e="T300" id="Seg_5122" s="T299">ugaːndə</ta>
            <ta e="T301" id="Seg_5123" s="T300">uda-m</ta>
            <ta e="T302" id="Seg_5124" s="T301">ĭzem-liA</ta>
            <ta e="T303" id="Seg_5125" s="T302">ulu-m</ta>
            <ta e="T304" id="Seg_5126" s="T303">ĭzem-liA</ta>
            <ta e="T305" id="Seg_5127" s="T304">bögəl-bə</ta>
            <ta e="T306" id="Seg_5128" s="T305">ĭzem-liA</ta>
            <ta e="T307" id="Seg_5129" s="T306">üjü-zAŋ-də</ta>
            <ta e="T308" id="Seg_5130" s="T307">ĭzem-liA</ta>
            <ta e="T321" id="Seg_5131" s="T320">tüʔ-zittə</ta>
            <ta e="T322" id="Seg_5132" s="T321">kan-zittə</ta>
            <ta e="T323" id="Seg_5133" s="T322">kĭnzə-zittə</ta>
            <ta e="T324" id="Seg_5134" s="T323">nadə</ta>
            <ta e="T325" id="Seg_5135" s="T324">kan-zittə</ta>
            <ta e="T327" id="Seg_5136" s="T325">nadə</ta>
            <ta e="T341" id="Seg_5137" s="T340">nüke</ta>
            <ta e="T342" id="Seg_5138" s="T341">i</ta>
            <ta e="T343" id="Seg_5139" s="T342">büzʼe</ta>
            <ta e="T344" id="Seg_5140" s="T343">dĭ-zAŋ</ta>
            <ta e="T346" id="Seg_5141" s="T345">ešši-zAŋ-Tə</ta>
            <ta e="T347" id="Seg_5142" s="T346">i-bi-jəʔ</ta>
            <ta e="T348" id="Seg_5143" s="T347">nʼi</ta>
            <ta e="T349" id="Seg_5144" s="T348">i</ta>
            <ta e="T350" id="Seg_5145" s="T349">koʔbdo</ta>
            <ta e="T356" id="Seg_5146" s="T355">amno-bi-jəʔ</ta>
            <ta e="T357" id="Seg_5147" s="T356">nüke</ta>
            <ta e="T358" id="Seg_5148" s="T357">i</ta>
            <ta e="T359" id="Seg_5149" s="T358">büzʼe</ta>
            <ta e="T360" id="Seg_5150" s="T359">dĭ-zAŋ-Tə</ta>
            <ta e="T361" id="Seg_5151" s="T360">ešši-zAŋ-Tə</ta>
            <ta e="T362" id="Seg_5152" s="T361">i-bi-jəʔ</ta>
            <ta e="T363" id="Seg_5153" s="T362">nʼi-t</ta>
            <ta e="T364" id="Seg_5154" s="T363">i</ta>
            <ta e="T365" id="Seg_5155" s="T364">koʔbdo</ta>
            <ta e="T366" id="Seg_5156" s="T365">nʼi</ta>
            <ta e="T367" id="Seg_5157" s="T366">i</ta>
            <ta e="T368" id="Seg_5158" s="T367">koʔbdo</ta>
            <ta e="T369" id="Seg_5159" s="T368">oʔb</ta>
            <ta e="T370" id="Seg_5160" s="T369">šide</ta>
            <ta e="T371" id="Seg_5161" s="T370">nagur</ta>
            <ta e="T372" id="Seg_5162" s="T371">sumna</ta>
            <ta e="T373" id="Seg_5163" s="T372">muktuʔ</ta>
            <ta e="T374" id="Seg_5164" s="T373">sejʔpü</ta>
            <ta e="T375" id="Seg_5165" s="T374">šĭnteʔtə</ta>
            <ta e="T376" id="Seg_5166" s="T375">amitun</ta>
            <ta e="T377" id="Seg_5167" s="T376">biəʔ</ta>
            <ta e="T378" id="Seg_5168" s="T377">biəʔ</ta>
            <ta e="T379" id="Seg_5169" s="T378">onʼiʔ</ta>
            <ta e="T380" id="Seg_5170" s="T379">biəʔ</ta>
            <ta e="T381" id="Seg_5171" s="T380">šide</ta>
            <ta e="T382" id="Seg_5172" s="T381">biəʔ</ta>
            <ta e="T383" id="Seg_5173" s="T382">nagur</ta>
            <ta e="T384" id="Seg_5174" s="T383">biəʔ</ta>
            <ta e="T385" id="Seg_5175" s="T384">teʔdə</ta>
            <ta e="T386" id="Seg_5176" s="T385">biəʔ</ta>
            <ta e="T387" id="Seg_5177" s="T386">sumna</ta>
            <ta e="T388" id="Seg_5178" s="T387">biəʔ</ta>
            <ta e="T389" id="Seg_5179" s="T388">muktuʔ</ta>
            <ta e="T390" id="Seg_5180" s="T389">biəʔ</ta>
            <ta e="T391" id="Seg_5181" s="T390">sejʔpü</ta>
            <ta e="T392" id="Seg_5182" s="T391">biəʔ</ta>
            <ta e="T393" id="Seg_5183" s="T392">šĭnteʔtə</ta>
            <ta e="T395" id="Seg_5184" s="T393">biəʔ</ta>
            <ta e="T419" id="Seg_5185" s="T418">oʔb</ta>
            <ta e="T420" id="Seg_5186" s="T419">šide</ta>
            <ta e="T421" id="Seg_5187" s="T420">nagur</ta>
            <ta e="T422" id="Seg_5188" s="T421">muktuʔ</ta>
            <ta e="T423" id="Seg_5189" s="T422">sejʔpü</ta>
            <ta e="T424" id="Seg_5190" s="T423">šĭnteʔtə</ta>
            <ta e="T425" id="Seg_5191" s="T424">amitun</ta>
            <ta e="T427" id="Seg_5192" s="T426">măna</ta>
            <ta e="T428" id="Seg_5193" s="T427">nadə</ta>
            <ta e="T429" id="Seg_5194" s="T428">monoʔko-zittə</ta>
            <ta e="T430" id="Seg_5195" s="T429">a</ta>
            <ta e="T431" id="Seg_5196" s="T430">šində-m</ta>
            <ta e="T432" id="Seg_5197" s="T431">dĭ</ta>
            <ta e="T433" id="Seg_5198" s="T432">koʔbdo</ta>
            <ta e="T434" id="Seg_5199" s="T433">kan-ə-ʔ</ta>
            <ta e="T435" id="Seg_5200" s="T434">monoʔko-t</ta>
            <ta e="T439" id="Seg_5201" s="T438">tura-Kən</ta>
            <ta e="T440" id="Seg_5202" s="T439">amno-laʔbə-m</ta>
            <ta e="T441" id="Seg_5203" s="T440">oi</ta>
            <ta e="T442" id="Seg_5204" s="T441">bar</ta>
            <ta e="T443" id="Seg_5205" s="T442">üjü-m</ta>
            <ta e="T444" id="Seg_5206" s="T443">kutʼüm-liA</ta>
            <ta e="T446" id="Seg_5207" s="T444">nadə</ta>
            <ta e="T448" id="Seg_5208" s="T447">ulu-m</ta>
            <ta e="T449" id="Seg_5209" s="T448">ĭzem-liA</ta>
            <ta e="T450" id="Seg_5210" s="T449">bukle-bə</ta>
            <ta e="T451" id="Seg_5211" s="T450">ĭzem-liA</ta>
            <ta e="T452" id="Seg_5212" s="T451">uda-m</ta>
            <ta e="T453" id="Seg_5213" s="T452">ĭzem-liA</ta>
            <ta e="T454" id="Seg_5214" s="T453">sima-m</ta>
            <ta e="T455" id="Seg_5215" s="T454">ĭzem-liA</ta>
            <ta e="T456" id="Seg_5216" s="T455">ku</ta>
            <ta e="T457" id="Seg_5217" s="T456">ĭzem-liA</ta>
            <ta e="T458" id="Seg_5218" s="T457">üjü-m</ta>
            <ta e="T459" id="Seg_5219" s="T458">ĭzem-liA</ta>
            <ta e="T460" id="Seg_5220" s="T459">köten-m</ta>
            <ta e="T461" id="Seg_5221" s="T460">ĭzem-liA</ta>
            <ta e="T463" id="Seg_5222" s="T462">dʼok</ta>
            <ta e="T465" id="Seg_5223" s="T464">dʼije-Tə</ta>
            <ta e="T466" id="Seg_5224" s="T465">nadə</ta>
            <ta e="T467" id="Seg_5225" s="T466">kan-zittə</ta>
            <ta e="T468" id="Seg_5226" s="T467">albuga</ta>
            <ta e="T469" id="Seg_5227" s="T468">kut-zittə</ta>
            <ta e="T470" id="Seg_5228" s="T469">urgaːba</ta>
            <ta e="T471" id="Seg_5229" s="T470">kut-zittə</ta>
            <ta e="T472" id="Seg_5230" s="T471">nadə</ta>
            <ta e="T473" id="Seg_5231" s="T472">tažəp</ta>
            <ta e="T474" id="Seg_5232" s="T473">kut-zittə</ta>
            <ta e="T475" id="Seg_5233" s="T474">nadə</ta>
            <ta e="T476" id="Seg_5234" s="T475">bulan</ta>
            <ta e="T477" id="Seg_5235" s="T476">kut-zittə</ta>
            <ta e="T478" id="Seg_5236" s="T477">nadə</ta>
            <ta e="T479" id="Seg_5237" s="T478">sön</ta>
            <ta e="T480" id="Seg_5238" s="T479">kut-zittə</ta>
            <ta e="T481" id="Seg_5239" s="T480">nadə</ta>
            <ta e="T482" id="Seg_5240" s="T481">poʔto</ta>
            <ta e="T483" id="Seg_5241" s="T482">nadə</ta>
            <ta e="T484" id="Seg_5242" s="T483">kut-zittə</ta>
            <ta e="T485" id="Seg_5243" s="T484">uja</ta>
            <ta e="T486" id="Seg_5244" s="T485">iʔgö</ta>
            <ta e="T487" id="Seg_5245" s="T486">mo-lV-j</ta>
            <ta e="T488" id="Seg_5246" s="T487">am-zittə</ta>
            <ta e="T489" id="Seg_5247" s="T488">nadə</ta>
            <ta e="T493" id="Seg_5248" s="T492">nadə</ta>
            <ta e="T496" id="Seg_5249" s="T495">uja</ta>
            <ta e="T498" id="Seg_5250" s="T497">hen-ə-ʔ</ta>
            <ta e="T499" id="Seg_5251" s="T498">šü</ta>
            <ta e="T500" id="Seg_5252" s="T499">nend-laʔbə</ta>
            <ta e="T501" id="Seg_5253" s="T500">uja</ta>
            <ta e="T502" id="Seg_5254" s="T501">hen-ə-ʔ</ta>
            <ta e="T518" id="Seg_5255" s="T517">šü</ta>
            <ta e="T519" id="Seg_5256" s="T518">nend-laʔbə</ta>
            <ta e="T520" id="Seg_5257" s="T519">uja</ta>
            <ta e="T521" id="Seg_5258" s="T520">hen-ə-ʔ</ta>
            <ta e="T523" id="Seg_5259" s="T522">puskaj</ta>
            <ta e="T524" id="Seg_5260" s="T523">pür-laʔbə</ta>
            <ta e="T526" id="Seg_5261" s="T525">šü-Tə</ta>
            <ta e="T527" id="Seg_5262" s="T526">šü-Tə</ta>
            <ta e="T528" id="Seg_5263" s="T527">hen-laʔbə</ta>
            <ta e="T529" id="Seg_5264" s="T528">uja</ta>
            <ta e="T531" id="Seg_5265" s="T530">amnə-ʔ</ta>
            <ta e="T532" id="Seg_5266" s="T531">döbər</ta>
            <ta e="T533" id="Seg_5267" s="T532">tʼăbaktər-ə-ʔ</ta>
            <ta e="T534" id="Seg_5268" s="T533">gijen</ta>
            <ta e="T535" id="Seg_5269" s="T534">i-bi-l</ta>
            <ta e="T536" id="Seg_5270" s="T535">ĭmbi</ta>
            <ta e="T537" id="Seg_5271" s="T536">ĭmbi</ta>
            <ta e="T538" id="Seg_5272" s="T537">ku-bi-l</ta>
            <ta e="T539" id="Seg_5273" s="T538">gibər</ta>
            <ta e="T540" id="Seg_5274" s="T539">kandə-gA-l</ta>
            <ta e="T541" id="Seg_5275" s="T540">dʼije-Tə</ta>
            <ta e="T542" id="Seg_5276" s="T541">kandə-gA-m</ta>
            <ta e="T543" id="Seg_5277" s="T542">keʔbde-j-lAʔ</ta>
            <ta e="T544" id="Seg_5278" s="T543">dĭn</ta>
            <ta e="T545" id="Seg_5279" s="T544">urgaːba</ta>
            <ta e="T546" id="Seg_5280" s="T545">ugaːndə</ta>
            <ta e="T547" id="Seg_5281" s="T546">pim-liA-m</ta>
            <ta e="T548" id="Seg_5282" s="T547">no</ta>
            <ta e="T549" id="Seg_5283" s="T548">e-ʔ</ta>
            <ta e="T550" id="Seg_5284" s="T549">kan-ə-ʔ</ta>
            <ta e="T551" id="Seg_5285" s="T550">pim-liA-l</ta>
            <ta e="T552" id="Seg_5286" s="T551">tak</ta>
            <ta e="T555" id="Seg_5287" s="T554">oʔb</ta>
            <ta e="T556" id="Seg_5288" s="T555">šide</ta>
            <ta e="T557" id="Seg_5289" s="T556">nagur</ta>
            <ta e="T558" id="Seg_5290" s="T557">teʔdə</ta>
            <ta e="T559" id="Seg_5291" s="T558">sumna</ta>
            <ta e="T560" id="Seg_5292" s="T559">sejʔpü</ta>
            <ta e="T562" id="Seg_5293" s="T561">sejʔpü</ta>
            <ta e="T563" id="Seg_5294" s="T562">oʔb</ta>
            <ta e="T564" id="Seg_5295" s="T563">šide</ta>
            <ta e="T565" id="Seg_5296" s="T564">nagur</ta>
            <ta e="T566" id="Seg_5297" s="T565">muktuʔ</ta>
            <ta e="T567" id="Seg_5298" s="T566">sejʔpü</ta>
            <ta e="T568" id="Seg_5299" s="T567">sumna</ta>
            <ta e="T569" id="Seg_5300" s="T568">šĭnteʔtə</ta>
            <ta e="T570" id="Seg_5301" s="T569">amitun</ta>
            <ta e="T571" id="Seg_5302" s="T570">biəʔ</ta>
            <ta e="T574" id="Seg_5303" s="T573">biəʔ</ta>
            <ta e="T575" id="Seg_5304" s="T574">onʼiʔ</ta>
            <ta e="T576" id="Seg_5305" s="T575">biəʔ</ta>
            <ta e="T577" id="Seg_5306" s="T576">šide</ta>
            <ta e="T578" id="Seg_5307" s="T577">biəʔ</ta>
            <ta e="T579" id="Seg_5308" s="T578">nagur</ta>
            <ta e="T580" id="Seg_5309" s="T579">biəʔ</ta>
            <ta e="T581" id="Seg_5310" s="T580">teʔdə</ta>
            <ta e="T582" id="Seg_5311" s="T581">biəʔ</ta>
            <ta e="T583" id="Seg_5312" s="T582">sumna</ta>
            <ta e="T584" id="Seg_5313" s="T583">biəʔ</ta>
            <ta e="T585" id="Seg_5314" s="T584">sejʔpü</ta>
            <ta e="T586" id="Seg_5315" s="T585">biəʔ</ta>
            <ta e="T587" id="Seg_5316" s="T586">šĭnteʔtə</ta>
            <ta e="T588" id="Seg_5317" s="T587">biəʔ</ta>
            <ta e="T589" id="Seg_5318" s="T588">sejʔpü</ta>
            <ta e="T590" id="Seg_5319" s="T589">biəʔ</ta>
            <ta e="T591" id="Seg_5320" s="T590">šide</ta>
            <ta e="T592" id="Seg_5321" s="T591">biəʔ</ta>
            <ta e="T596" id="Seg_5322" s="T595">surdo-zittə</ta>
            <ta e="T597" id="Seg_5323" s="T596">nadə</ta>
            <ta e="T598" id="Seg_5324" s="T597">tüžöj-ə-m</ta>
            <ta e="T599" id="Seg_5325" s="T598">šeden-də</ta>
            <ta e="T600" id="Seg_5326" s="T599">kaj-zittə</ta>
            <ta e="T601" id="Seg_5327" s="T600">nadə</ta>
            <ta e="T602" id="Seg_5328" s="T601">surdo-zittə</ta>
            <ta e="T603" id="Seg_5329" s="T602">nadə</ta>
            <ta e="T604" id="Seg_5330" s="T603">süt</ta>
            <ta e="T605" id="Seg_5331" s="T604">det-zittə</ta>
            <ta e="T606" id="Seg_5332" s="T605">šojdʼo-Tə</ta>
            <ta e="T607" id="Seg_5333" s="T606">hen-zittə</ta>
            <ta e="T608" id="Seg_5334" s="T607">kămnə-zittə</ta>
            <ta e="T615" id="Seg_5335" s="T614">gibər</ta>
            <ta e="T616" id="Seg_5336" s="T615">kandə-gA-l</ta>
            <ta e="T617" id="Seg_5337" s="T616">gibər</ta>
            <ta e="T618" id="Seg_5338" s="T617">nuʔmə-laʔbə-l</ta>
            <ta e="T619" id="Seg_5339" s="T618">tüžöj-m</ta>
            <ta e="T620" id="Seg_5340" s="T619">naga</ta>
            <ta e="T622" id="Seg_5341" s="T621">măndo-r-zittə</ta>
            <ta e="T624" id="Seg_5342" s="T623">kan-lV-m</ta>
            <ta e="T625" id="Seg_5343" s="T624">kan-žə-bəj</ta>
            <ta e="T626" id="Seg_5344" s="T625">măn-ziʔ</ta>
            <ta e="T627" id="Seg_5345" s="T626">ato</ta>
            <ta e="T628" id="Seg_5346" s="T627">măn</ta>
            <ta e="T629" id="Seg_5347" s="T628">pim-liA-m</ta>
            <ta e="T635" id="Seg_5348" s="T634">dărowă</ta>
            <ta e="T636" id="Seg_5349" s="T635">i-gA-l</ta>
            <ta e="T637" id="Seg_5350" s="T636">amnə-ʔ</ta>
            <ta e="T638" id="Seg_5351" s="T637">amor-zittə</ta>
            <ta e="T639" id="Seg_5352" s="T638">ipek</ta>
            <ta e="T640" id="Seg_5353" s="T639">lem</ta>
            <ta e="T641" id="Seg_5354" s="T640">keʔbde-ziʔ</ta>
            <ta e="T642" id="Seg_5355" s="T641">kajaʔ</ta>
            <ta e="T643" id="Seg_5356" s="T642">uja</ta>
            <ta e="T644" id="Seg_5357" s="T643">am-ə-ʔ</ta>
            <ta e="T645" id="Seg_5358" s="T644">keʔbde</ta>
            <ta e="T646" id="Seg_5359" s="T645">am-ə-ʔ</ta>
            <ta e="T647" id="Seg_5360" s="T646">oroma</ta>
            <ta e="T648" id="Seg_5361" s="T647">am-ə-ʔ</ta>
            <ta e="T650" id="Seg_5362" s="T649">ipek</ta>
            <ta e="T651" id="Seg_5363" s="T650">am-ə-ʔ</ta>
            <ta e="T652" id="Seg_5364" s="T651">segi</ta>
            <ta e="T653" id="Seg_5365" s="T652">bü</ta>
            <ta e="T654" id="Seg_5366" s="T653">bĭs-ə-ʔ</ta>
            <ta e="T655" id="Seg_5367" s="T654">sĭreʔp</ta>
            <ta e="T656" id="Seg_5368" s="T655">hen-t</ta>
            <ta e="T657" id="Seg_5369" s="T656">segi</ta>
            <ta e="T658" id="Seg_5370" s="T657">bü-Tə</ta>
            <ta e="T659" id="Seg_5371" s="T658">süt</ta>
            <ta e="T660" id="Seg_5372" s="T659">amnə-ʔ</ta>
            <ta e="T662" id="Seg_5373" s="T661">kan-žə-bəj</ta>
            <ta e="T663" id="Seg_5374" s="T662">obberəj</ta>
            <ta e="T664" id="Seg_5375" s="T663">dʼije-Tə</ta>
            <ta e="T665" id="Seg_5376" s="T664">keʔbde</ta>
            <ta e="T666" id="Seg_5377" s="T665">oʔbdə-zittə</ta>
            <ta e="T667" id="Seg_5378" s="T666">kalba</ta>
            <ta e="T668" id="Seg_5379" s="T667">nĭŋgə-zittə</ta>
            <ta e="T669" id="Seg_5380" s="T668">sanə</ta>
            <ta e="T670" id="Seg_5381" s="T669">a-zittə</ta>
            <ta e="T671" id="Seg_5382" s="T670">ĭššo</ta>
            <ta e="T672" id="Seg_5383" s="T671">ĭmbi</ta>
            <ta e="T673" id="Seg_5384" s="T672">beške</ta>
            <ta e="T677" id="Seg_5385" s="T676">beške</ta>
            <ta e="T678" id="Seg_5386" s="T677">i-zittə</ta>
            <ta e="T683" id="Seg_5387" s="T682">măn</ta>
            <ta e="T684" id="Seg_5388" s="T683">tănan</ta>
            <ta e="T685" id="Seg_5389" s="T684">ajir-laʔbə-m</ta>
            <ta e="T689" id="Seg_5390" s="T688">kan-ə-ʔ</ta>
            <ta e="T690" id="Seg_5391" s="T689">ine</ta>
            <ta e="T691" id="Seg_5392" s="T690">körer-ə-ʔ</ta>
            <ta e="T692" id="Seg_5393" s="T691">kan-žə-bəj</ta>
            <ta e="T693" id="Seg_5394" s="T692">gibər=nʼibudʼ</ta>
            <ta e="T694" id="Seg_5395" s="T693">ine-ziʔ</ta>
            <ta e="T695" id="Seg_5396" s="T694">bü</ta>
            <ta e="T697" id="Seg_5397" s="T696">det-lV-bəj</ta>
            <ta e="T698" id="Seg_5398" s="T697">pa-jəʔ</ta>
            <ta e="T699" id="Seg_5399" s="T698">det-lV-bAʔ</ta>
            <ta e="T700" id="Seg_5400" s="T699">noʔ</ta>
            <ta e="T701" id="Seg_5401" s="T700">det-lV-bəj</ta>
            <ta e="T702" id="Seg_5402" s="T701">săloma</ta>
            <ta e="T703" id="Seg_5403" s="T702">det-lV-bəj</ta>
            <ta e="T709" id="Seg_5404" s="T708">tugan-m</ta>
            <ta e="T710" id="Seg_5405" s="T709">šo-bi</ta>
            <ta e="T711" id="Seg_5406" s="T710">nadə</ta>
            <ta e="T713" id="Seg_5407" s="T712">măndo-r-zittə</ta>
            <ta e="T714" id="Seg_5408" s="T713">kan-zittə</ta>
            <ta e="T716" id="Seg_5409" s="T715">nadə</ta>
            <ta e="T717" id="Seg_5410" s="T716">kăštə-zittə</ta>
            <ta e="T718" id="Seg_5411" s="T717">amor-zittə</ta>
            <ta e="T719" id="Seg_5412" s="T718">mĭ-zittə</ta>
            <ta e="T720" id="Seg_5413" s="T719">nadə</ta>
            <ta e="T721" id="Seg_5414" s="T720">kunol-zittə</ta>
            <ta e="T722" id="Seg_5415" s="T721">dĭ-zAŋ-Tə</ta>
            <ta e="T725" id="Seg_5416" s="T724">oʔb</ta>
            <ta e="T726" id="Seg_5417" s="T725">oʔb</ta>
            <ta e="T727" id="Seg_5418" s="T726">šide</ta>
            <ta e="T728" id="Seg_5419" s="T727">nagur</ta>
            <ta e="T729" id="Seg_5420" s="T728">teʔdə</ta>
            <ta e="T730" id="Seg_5421" s="T729">sumna</ta>
            <ta e="T731" id="Seg_5422" s="T730">sejʔpü</ta>
            <ta e="T732" id="Seg_5423" s="T731">amitun</ta>
            <ta e="T733" id="Seg_5424" s="T732">šĭnteʔtə</ta>
            <ta e="T734" id="Seg_5425" s="T733">biəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T18" id="Seg_5426" s="T17">NEG.AUX-IMP.2SG</ta>
            <ta e="T19" id="Seg_5427" s="T18">scold-DES-CNG</ta>
            <ta e="T23" id="Seg_5428" s="T22">I.NOM</ta>
            <ta e="T24" id="Seg_5429" s="T23">this-ACC</ta>
            <ta e="T25" id="Seg_5430" s="T24">be-PST-1SG</ta>
            <ta e="T28" id="Seg_5431" s="T27">there</ta>
            <ta e="T29" id="Seg_5432" s="T28">PTCL</ta>
            <ta e="T30" id="Seg_5433" s="T29">scold-DES-DUR-3PL</ta>
            <ta e="T31" id="Seg_5434" s="T30">I.NOM</ta>
            <ta e="T742" id="Seg_5435" s="T31">go-CVB</ta>
            <ta e="T32" id="Seg_5436" s="T742">disappear-PST-1SG</ta>
            <ta e="T37" id="Seg_5437" s="T36">many</ta>
            <ta e="T38" id="Seg_5438" s="T37">write-PST-2SG</ta>
            <ta e="T39" id="Seg_5439" s="T38">PTCL</ta>
            <ta e="T41" id="Seg_5440" s="T40">chief.[NOM.SG]</ta>
            <ta e="T42" id="Seg_5441" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_5442" s="T42">thick.[NOM.SG]</ta>
            <ta e="T44" id="Seg_5443" s="T43">come-PST.[3SG]</ta>
            <ta e="T45" id="Seg_5444" s="T44">drunk.[NOM.SG]</ta>
            <ta e="T46" id="Seg_5445" s="T45">PTCL</ta>
            <ta e="T54" id="Seg_5446" s="T52">be-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_5447" s="T56">soap</ta>
            <ta e="T58" id="Seg_5448" s="T57">be-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_5449" s="T59">rub-IMP.2SG.O</ta>
            <ta e="T61" id="Seg_5450" s="T60">PTCL</ta>
            <ta e="T72" id="Seg_5451" s="T71">very</ta>
            <ta e="T73" id="Seg_5452" s="T72">you.NOM</ta>
            <ta e="T74" id="Seg_5453" s="T73">black.[NOM.SG]</ta>
            <ta e="T75" id="Seg_5454" s="T74">go-EP-IMP.2SG</ta>
            <ta e="T76" id="Seg_5455" s="T75">sauna-LAT</ta>
            <ta e="T78" id="Seg_5456" s="T77">there</ta>
            <ta e="T79" id="Seg_5457" s="T78">warm.[NOM.SG]</ta>
            <ta e="T80" id="Seg_5458" s="T79">be-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_5459" s="T80">and</ta>
            <ta e="T82" id="Seg_5460" s="T81">cold.[NOM.SG]</ta>
            <ta e="T83" id="Seg_5461" s="T82">water.[NOM.SG]</ta>
            <ta e="T84" id="Seg_5462" s="T83">soap</ta>
            <ta e="T85" id="Seg_5463" s="T84">be-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_5464" s="T85">wash-IMP.2SG</ta>
            <ta e="T88" id="Seg_5465" s="T87">rub-IMP.2SG.O</ta>
            <ta e="T91" id="Seg_5466" s="T89">then</ta>
            <ta e="T108" id="Seg_5467" s="T107">cow-PL</ta>
            <ta e="T109" id="Seg_5468" s="T108">corral-NOM/GEN/ACC.3SG</ta>
            <ta e="T112" id="Seg_5469" s="T111">Kamassian-PL</ta>
            <ta e="T113" id="Seg_5470" s="T112">come-PST-3PL</ta>
            <ta e="T114" id="Seg_5471" s="T113">sit-IMP.2SG</ta>
            <ta e="T115" id="Seg_5472" s="T114">eat-EP-IMP.2SG</ta>
            <ta e="T116" id="Seg_5473" s="T115">probably</ta>
            <ta e="T117" id="Seg_5474" s="T116">eat-INF.LAT</ta>
            <ta e="T118" id="Seg_5475" s="T117">one.wants</ta>
            <ta e="T119" id="Seg_5476" s="T118">where</ta>
            <ta e="T120" id="Seg_5477" s="T119">be-PST-2SG</ta>
            <ta e="T121" id="Seg_5478" s="T120">sit-IMP.2SG</ta>
            <ta e="T122" id="Seg_5479" s="T121">here</ta>
            <ta e="T123" id="Seg_5480" s="T122">beautiful.[NOM.SG]</ta>
            <ta e="T124" id="Seg_5481" s="T123">man.[NOM.SG]</ta>
            <ta e="T129" id="Seg_5482" s="T128">one.should</ta>
            <ta e="T130" id="Seg_5483" s="T129">house.[NOM.SG]</ta>
            <ta e="T131" id="Seg_5484" s="T130">wash.oneself-INF.LAT</ta>
            <ta e="T132" id="Seg_5485" s="T131">one.should</ta>
            <ta e="T133" id="Seg_5486" s="T132">sweep-INF.LAT</ta>
            <ta e="T134" id="Seg_5487" s="T133">outwards</ta>
            <ta e="T135" id="Seg_5488" s="T134">outwards</ta>
            <ta e="T136" id="Seg_5489" s="T135">go-INF.LAT</ta>
            <ta e="T137" id="Seg_5490" s="T136">cow.[NOM.SG]</ta>
            <ta e="T138" id="Seg_5491" s="T137">milk-INF.LAT</ta>
            <ta e="T139" id="Seg_5492" s="T138">child-ACC</ta>
            <ta e="T140" id="Seg_5493" s="T139">wash.oneself-INF.LAT</ta>
            <ta e="T142" id="Seg_5494" s="T141">bread.[NOM.SG]</ta>
            <ta e="T143" id="Seg_5495" s="T142">bake-INF.LAT</ta>
            <ta e="T144" id="Seg_5496" s="T143">bread.[NOM.SG]</ta>
            <ta e="T145" id="Seg_5497" s="T144">place-INF.LAT</ta>
            <ta e="T148" id="Seg_5498" s="T146">bake-INF.LAT</ta>
            <ta e="T149" id="Seg_5499" s="T148">fat</ta>
            <ta e="T150" id="Seg_5500" s="T149">bake-INF.LAT</ta>
            <ta e="T151" id="Seg_5501" s="T150">one.should</ta>
            <ta e="T166" id="Seg_5502" s="T165">yellow.[NOM.SG]</ta>
            <ta e="T167" id="Seg_5503" s="T166">water.[NOM.SG]</ta>
            <ta e="T168" id="Seg_5504" s="T167">one.should</ta>
            <ta e="T169" id="Seg_5505" s="T168">boil-INF.LAT</ta>
            <ta e="T171" id="Seg_5506" s="T169">shout-DUR.[3SG]</ta>
            <ta e="T172" id="Seg_5507" s="T171">NEG.AUX-IMP.2SG</ta>
            <ta e="T173" id="Seg_5508" s="T172">quarrel-EP-CNG</ta>
            <ta e="T174" id="Seg_5509" s="T173">NEG.AUX-IMP.2SG</ta>
            <ta e="T175" id="Seg_5510" s="T174">make.noise-EP-CNG</ta>
            <ta e="T183" id="Seg_5511" s="T182">I.NOM</ta>
            <ta e="T184" id="Seg_5512" s="T183">there</ta>
            <ta e="T185" id="Seg_5513" s="T184">be-PST-1SG</ta>
            <ta e="T186" id="Seg_5514" s="T185">there</ta>
            <ta e="T187" id="Seg_5515" s="T186">PTCL</ta>
            <ta e="T189" id="Seg_5516" s="T188">scold-DES-DUR-3PL</ta>
            <ta e="T190" id="Seg_5517" s="T189">I.NOM</ta>
            <ta e="T743" id="Seg_5518" s="T190">go-CVB</ta>
            <ta e="T191" id="Seg_5519" s="T743">disappear-PST-1SG</ta>
            <ta e="T194" id="Seg_5520" s="T193">hand-NOM/GEN/ACC.2SG</ta>
            <ta e="T195" id="Seg_5521" s="T194">probably</ta>
            <ta e="T196" id="Seg_5522" s="T195">hurt-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_5523" s="T200">salt.[NOM.SG]</ta>
            <ta e="T202" id="Seg_5524" s="T201">NEG.EX.[3SG]</ta>
            <ta e="T203" id="Seg_5525" s="T202">one.should</ta>
            <ta e="T204" id="Seg_5526" s="T203">salt-INF.LAT</ta>
            <ta e="T206" id="Seg_5527" s="T204">what-INS</ta>
            <ta e="T207" id="Seg_5528" s="T206">eat-INF.LAT</ta>
            <ta e="T208" id="Seg_5529" s="T207">spoon.[NOM.SG]</ta>
            <ta e="T209" id="Seg_5530" s="T208">NEG.EX.[3SG]</ta>
            <ta e="T210" id="Seg_5531" s="T209">bring-IMP.2SG</ta>
            <ta e="T211" id="Seg_5532" s="T210">spoon.[NOM.SG]</ta>
            <ta e="T212" id="Seg_5533" s="T211">that.[NOM.SG]</ta>
            <ta e="T213" id="Seg_5534" s="T212">spoon.[NOM.SG]</ta>
            <ta e="T214" id="Seg_5535" s="T213">give-PST-1SG</ta>
            <ta e="T215" id="Seg_5536" s="T214">you.DAT</ta>
            <ta e="T216" id="Seg_5537" s="T215">soon</ta>
            <ta e="T217" id="Seg_5538" s="T216">eat-EP-IMP.2SG</ta>
            <ta e="T222" id="Seg_5539" s="T221">go-OPT.DU/PL-1DU</ta>
            <ta e="T224" id="Seg_5540" s="T223">together</ta>
            <ta e="T225" id="Seg_5541" s="T224">very</ta>
            <ta e="T226" id="Seg_5542" s="T225">beautiful.[NOM.SG]</ta>
            <ta e="T227" id="Seg_5543" s="T226">girl-PL</ta>
            <ta e="T228" id="Seg_5544" s="T227">very</ta>
            <ta e="T229" id="Seg_5545" s="T228">beautiful.[NOM.SG]</ta>
            <ta e="T230" id="Seg_5546" s="T229">boy-PL</ta>
            <ta e="T231" id="Seg_5547" s="T230">very</ta>
            <ta e="T232" id="Seg_5548" s="T231">beautiful.[NOM.SG]</ta>
            <ta e="T233" id="Seg_5549" s="T232">people.[NOM.SG]</ta>
            <ta e="T234" id="Seg_5550" s="T233">very</ta>
            <ta e="T235" id="Seg_5551" s="T234">good.[NOM.SG]</ta>
            <ta e="T236" id="Seg_5552" s="T235">man.[NOM.SG]</ta>
            <ta e="T237" id="Seg_5553" s="T236">very</ta>
            <ta e="T238" id="Seg_5554" s="T237">lie-DUR.[3SG]</ta>
            <ta e="T239" id="Seg_5555" s="T238">man.[NOM.SG]</ta>
            <ta e="T242" id="Seg_5556" s="T241">one.should</ta>
            <ta e="T243" id="Seg_5557" s="T242">sleep-INF.LAT</ta>
            <ta e="T245" id="Seg_5558" s="T244">a.few</ta>
            <ta e="T248" id="Seg_5559" s="T247">one.wants</ta>
            <ta e="T249" id="Seg_5560" s="T248">very</ta>
            <ta e="T250" id="Seg_5561" s="T249">belly-NOM/GEN/ACC.1SG</ta>
            <ta e="T251" id="Seg_5562" s="T250">hurt-PRS.[3SG]</ta>
            <ta e="T252" id="Seg_5563" s="T251">eat-EP-IMP.2SG</ta>
            <ta e="T253" id="Seg_5564" s="T252">sit-IMP.2SG</ta>
            <ta e="T254" id="Seg_5565" s="T253">NEG.AUX-IMP.2SG</ta>
            <ta e="T255" id="Seg_5566" s="T254">shout-EP-CNG</ta>
            <ta e="T256" id="Seg_5567" s="T255">NEG.AUX-IMP.2SG</ta>
            <ta e="T257" id="Seg_5568" s="T256">make.noise-EP-CNG</ta>
            <ta e="T259" id="Seg_5569" s="T257">one.should</ta>
            <ta e="T260" id="Seg_5570" s="T259">take-INF.LAT</ta>
            <ta e="T261" id="Seg_5571" s="T260">otherwise</ta>
            <ta e="T269" id="Seg_5572" s="T741">forest-LAT</ta>
            <ta e="T270" id="Seg_5573" s="T269">go-INF.LAT</ta>
            <ta e="T271" id="Seg_5574" s="T270">berry.[NOM.SG]</ta>
            <ta e="T272" id="Seg_5575" s="T271">bring-INF.LAT</ta>
            <ta e="T273" id="Seg_5576" s="T272">pine.nut.[NOM.SG]</ta>
            <ta e="T274" id="Seg_5577" s="T273">bring-INF.LAT</ta>
            <ta e="T275" id="Seg_5578" s="T274">soon</ta>
            <ta e="T276" id="Seg_5579" s="T275">go-OPT.DU/PL-1DU</ta>
            <ta e="T277" id="Seg_5580" s="T276">two-COLL</ta>
            <ta e="T280" id="Seg_5581" s="T279">what=INDEF</ta>
            <ta e="T281" id="Seg_5582" s="T280">kill-FUT-1SG</ta>
            <ta e="T282" id="Seg_5583" s="T281">this-ACC</ta>
            <ta e="T283" id="Seg_5584" s="T282">goat.[NOM.SG]</ta>
            <ta e="T284" id="Seg_5585" s="T283">kill-PST-1SG</ta>
            <ta e="T285" id="Seg_5586" s="T284">PTCL</ta>
            <ta e="T287" id="Seg_5587" s="T286">sable.[NOM.SG]</ta>
            <ta e="T288" id="Seg_5588" s="T287">kill-PST-1SG</ta>
            <ta e="T289" id="Seg_5589" s="T288">squirrel.[NOM.SG]</ta>
            <ta e="T290" id="Seg_5590" s="T289">kill-PST-1SG</ta>
            <ta e="T291" id="Seg_5591" s="T290">deer.[NOM.SG]</ta>
            <ta e="T292" id="Seg_5592" s="T291">kill-PST-1SG</ta>
            <ta e="T293" id="Seg_5593" s="T292">bear.[NOM.SG]</ta>
            <ta e="T294" id="Seg_5594" s="T293">kill-PST-1SG</ta>
            <ta e="T299" id="Seg_5595" s="T298">hurt-PRS.[3SG]</ta>
            <ta e="T300" id="Seg_5596" s="T299">very</ta>
            <ta e="T301" id="Seg_5597" s="T300">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T302" id="Seg_5598" s="T301">hurt-PRS.[3SG]</ta>
            <ta e="T303" id="Seg_5599" s="T302">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T304" id="Seg_5600" s="T303">hurt-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_5601" s="T304">back-NOM/GEN/ACC.1SG</ta>
            <ta e="T306" id="Seg_5602" s="T305">hurt-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_5603" s="T306">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T308" id="Seg_5604" s="T307">hurt-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_5605" s="T320">shit-INF.LAT</ta>
            <ta e="T322" id="Seg_5606" s="T321">go-INF.LAT</ta>
            <ta e="T323" id="Seg_5607" s="T322">piss-INF.LAT</ta>
            <ta e="T324" id="Seg_5608" s="T323">one.should</ta>
            <ta e="T325" id="Seg_5609" s="T324">go-INF.LAT</ta>
            <ta e="T327" id="Seg_5610" s="T325">one.should</ta>
            <ta e="T341" id="Seg_5611" s="T340">woman.[NOM.SG]</ta>
            <ta e="T342" id="Seg_5612" s="T341">and</ta>
            <ta e="T343" id="Seg_5613" s="T342">man.[NOM.SG]</ta>
            <ta e="T344" id="Seg_5614" s="T343">this-PL</ta>
            <ta e="T346" id="Seg_5615" s="T345">child-PL-LAT</ta>
            <ta e="T347" id="Seg_5616" s="T346">be-PST-3PL</ta>
            <ta e="T348" id="Seg_5617" s="T347">son.[NOM.SG]</ta>
            <ta e="T349" id="Seg_5618" s="T348">and</ta>
            <ta e="T350" id="Seg_5619" s="T349">daughter.[NOM.SG]</ta>
            <ta e="T356" id="Seg_5620" s="T355">live-PST-3PL</ta>
            <ta e="T357" id="Seg_5621" s="T356">woman.[NOM.SG]</ta>
            <ta e="T358" id="Seg_5622" s="T357">and</ta>
            <ta e="T359" id="Seg_5623" s="T358">man.[NOM.SG]</ta>
            <ta e="T360" id="Seg_5624" s="T359">this-PL-LAT</ta>
            <ta e="T361" id="Seg_5625" s="T360">child-PL-LAT</ta>
            <ta e="T362" id="Seg_5626" s="T361">be-PST-3PL</ta>
            <ta e="T363" id="Seg_5627" s="T362">son-NOM/GEN.3SG</ta>
            <ta e="T364" id="Seg_5628" s="T363">and</ta>
            <ta e="T365" id="Seg_5629" s="T364">daughter.[NOM.SG]</ta>
            <ta e="T366" id="Seg_5630" s="T365">boy.[NOM.SG]</ta>
            <ta e="T367" id="Seg_5631" s="T366">and</ta>
            <ta e="T368" id="Seg_5632" s="T367">daughter.[NOM.SG]</ta>
            <ta e="T369" id="Seg_5633" s="T368">one.[NOM.SG]</ta>
            <ta e="T370" id="Seg_5634" s="T369">two.[NOM.SG]</ta>
            <ta e="T371" id="Seg_5635" s="T370">three.[NOM.SG]</ta>
            <ta e="T372" id="Seg_5636" s="T371">five.[NOM.SG]</ta>
            <ta e="T373" id="Seg_5637" s="T372">six.[NOM.SG]</ta>
            <ta e="T374" id="Seg_5638" s="T373">seven.[NOM.SG]</ta>
            <ta e="T375" id="Seg_5639" s="T374">eight.[NOM.SG]</ta>
            <ta e="T376" id="Seg_5640" s="T375">nine.[NOM.SG]</ta>
            <ta e="T377" id="Seg_5641" s="T376">ten.[NOM.SG]</ta>
            <ta e="T378" id="Seg_5642" s="T377">ten.[NOM.SG]</ta>
            <ta e="T379" id="Seg_5643" s="T378">one.[NOM.SG]</ta>
            <ta e="T380" id="Seg_5644" s="T379">ten.[NOM.SG]</ta>
            <ta e="T381" id="Seg_5645" s="T380">two.[NOM.SG]</ta>
            <ta e="T382" id="Seg_5646" s="T381">ten.[NOM.SG]</ta>
            <ta e="T383" id="Seg_5647" s="T382">three.[NOM.SG]</ta>
            <ta e="T384" id="Seg_5648" s="T383">ten.[NOM.SG]</ta>
            <ta e="T385" id="Seg_5649" s="T384">four.[NOM.SG]</ta>
            <ta e="T386" id="Seg_5650" s="T385">ten.[NOM.SG]</ta>
            <ta e="T387" id="Seg_5651" s="T386">five.[NOM.SG]</ta>
            <ta e="T388" id="Seg_5652" s="T387">ten.[NOM.SG]</ta>
            <ta e="T389" id="Seg_5653" s="T388">six.[NOM.SG]</ta>
            <ta e="T390" id="Seg_5654" s="T389">ten.[NOM.SG]</ta>
            <ta e="T391" id="Seg_5655" s="T390">seven.[NOM.SG]</ta>
            <ta e="T392" id="Seg_5656" s="T391">ten.[NOM.SG]</ta>
            <ta e="T393" id="Seg_5657" s="T392">eight.[NOM.SG]</ta>
            <ta e="T395" id="Seg_5658" s="T393">ten.[NOM.SG]</ta>
            <ta e="T419" id="Seg_5659" s="T418">one.[NOM.SG]</ta>
            <ta e="T420" id="Seg_5660" s="T419">two.[NOM.SG]</ta>
            <ta e="T421" id="Seg_5661" s="T420">three.[NOM.SG]</ta>
            <ta e="T422" id="Seg_5662" s="T421">six.[NOM.SG]</ta>
            <ta e="T423" id="Seg_5663" s="T422">seven.[NOM.SG]</ta>
            <ta e="T424" id="Seg_5664" s="T423">eight.[NOM.SG]</ta>
            <ta e="T425" id="Seg_5665" s="T424">nine.[NOM.SG]</ta>
            <ta e="T427" id="Seg_5666" s="T426">I.LAT</ta>
            <ta e="T428" id="Seg_5667" s="T427">one.should</ta>
            <ta e="T429" id="Seg_5668" s="T428">marry-INF.LAT</ta>
            <ta e="T430" id="Seg_5669" s="T429">and</ta>
            <ta e="T431" id="Seg_5670" s="T430">who-ACC</ta>
            <ta e="T432" id="Seg_5671" s="T431">this.[NOM.SG]</ta>
            <ta e="T433" id="Seg_5672" s="T432">daughter.[NOM.SG]</ta>
            <ta e="T434" id="Seg_5673" s="T433">go-EP-IMP.2SG</ta>
            <ta e="T435" id="Seg_5674" s="T434">marry-IMP.2SG.O</ta>
            <ta e="T439" id="Seg_5675" s="T438">house-LOC</ta>
            <ta e="T440" id="Seg_5676" s="T439">live-DUR-1SG</ta>
            <ta e="T441" id="Seg_5677" s="T440">oh</ta>
            <ta e="T442" id="Seg_5678" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_5679" s="T442">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T444" id="Seg_5680" s="T443">%%-PRS.[3SG]</ta>
            <ta e="T446" id="Seg_5681" s="T444">one.should</ta>
            <ta e="T448" id="Seg_5682" s="T447">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T449" id="Seg_5683" s="T448">hurt-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_5684" s="T449">whole-NOM/GEN/ACC.1SG</ta>
            <ta e="T451" id="Seg_5685" s="T450">hurt-PRS.[3SG]</ta>
            <ta e="T452" id="Seg_5686" s="T451">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T453" id="Seg_5687" s="T452">hurt-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_5688" s="T453">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T455" id="Seg_5689" s="T454">hurt-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_5690" s="T455">ear.[NOM.SG]</ta>
            <ta e="T457" id="Seg_5691" s="T456">hurt-PRS.[3SG]</ta>
            <ta e="T458" id="Seg_5692" s="T457">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T459" id="Seg_5693" s="T458">hurt-PRS.[3SG]</ta>
            <ta e="T460" id="Seg_5694" s="T459">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T461" id="Seg_5695" s="T460">hurt-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_5696" s="T462">no</ta>
            <ta e="T465" id="Seg_5697" s="T464">forest-LAT</ta>
            <ta e="T466" id="Seg_5698" s="T465">one.should</ta>
            <ta e="T467" id="Seg_5699" s="T466">go-INF.LAT</ta>
            <ta e="T468" id="Seg_5700" s="T467">sable.[NOM.SG]</ta>
            <ta e="T469" id="Seg_5701" s="T468">kill-INF.LAT</ta>
            <ta e="T470" id="Seg_5702" s="T469">bear.[NOM.SG]</ta>
            <ta e="T471" id="Seg_5703" s="T470">kill-INF.LAT</ta>
            <ta e="T472" id="Seg_5704" s="T471">one.should</ta>
            <ta e="T473" id="Seg_5705" s="T472">squirrel.[NOM.SG]</ta>
            <ta e="T474" id="Seg_5706" s="T473">kill-INF.LAT</ta>
            <ta e="T475" id="Seg_5707" s="T474">one.should</ta>
            <ta e="T476" id="Seg_5708" s="T475">moose.[NOM.SG]</ta>
            <ta e="T477" id="Seg_5709" s="T476">kill-INF.LAT</ta>
            <ta e="T478" id="Seg_5710" s="T477">one.should</ta>
            <ta e="T479" id="Seg_5711" s="T478">deer.[NOM.SG]</ta>
            <ta e="T480" id="Seg_5712" s="T479">kill-INF.LAT</ta>
            <ta e="T481" id="Seg_5713" s="T480">one.should</ta>
            <ta e="T482" id="Seg_5714" s="T481">goat.[NOM.SG]</ta>
            <ta e="T483" id="Seg_5715" s="T482">one.should</ta>
            <ta e="T484" id="Seg_5716" s="T483">kill-INF.LAT</ta>
            <ta e="T485" id="Seg_5717" s="T484">meat.[NOM.SG]</ta>
            <ta e="T486" id="Seg_5718" s="T485">many</ta>
            <ta e="T487" id="Seg_5719" s="T486">become-FUT-3SG</ta>
            <ta e="T488" id="Seg_5720" s="T487">eat-INF.LAT</ta>
            <ta e="T489" id="Seg_5721" s="T488">one.should</ta>
            <ta e="T493" id="Seg_5722" s="T492">one.should</ta>
            <ta e="T496" id="Seg_5723" s="T495">meat.[NOM.SG]</ta>
            <ta e="T498" id="Seg_5724" s="T497">put-EP-IMP.2SG</ta>
            <ta e="T499" id="Seg_5725" s="T498">fire.[NOM.SG]</ta>
            <ta e="T500" id="Seg_5726" s="T499">burn-DUR.[3SG]</ta>
            <ta e="T501" id="Seg_5727" s="T500">meat.[NOM.SG]</ta>
            <ta e="T502" id="Seg_5728" s="T501">put-EP-IMP.2SG</ta>
            <ta e="T518" id="Seg_5729" s="T517">fire.[NOM.SG]</ta>
            <ta e="T519" id="Seg_5730" s="T518">burn-DUR.[3SG]</ta>
            <ta e="T520" id="Seg_5731" s="T519">meat.[NOM.SG]</ta>
            <ta e="T521" id="Seg_5732" s="T520">put-EP-IMP.2SG</ta>
            <ta e="T523" id="Seg_5733" s="T522">JUSS</ta>
            <ta e="T524" id="Seg_5734" s="T523">bake-DUR.[3SG]</ta>
            <ta e="T526" id="Seg_5735" s="T525">fire-LAT</ta>
            <ta e="T527" id="Seg_5736" s="T526">fire-LAT</ta>
            <ta e="T528" id="Seg_5737" s="T527">put-DUR.[3SG]</ta>
            <ta e="T529" id="Seg_5738" s="T528">meat.[NOM.SG]</ta>
            <ta e="T531" id="Seg_5739" s="T530">sit-IMP.2SG</ta>
            <ta e="T532" id="Seg_5740" s="T531">here</ta>
            <ta e="T533" id="Seg_5741" s="T532">speak-EP-IMP.2SG</ta>
            <ta e="T534" id="Seg_5742" s="T533">where</ta>
            <ta e="T535" id="Seg_5743" s="T534">be-PST-2SG</ta>
            <ta e="T536" id="Seg_5744" s="T535">what.[NOM.SG]</ta>
            <ta e="T537" id="Seg_5745" s="T536">what.[NOM.SG]</ta>
            <ta e="T538" id="Seg_5746" s="T537">see-PST-2SG</ta>
            <ta e="T539" id="Seg_5747" s="T538">where.to</ta>
            <ta e="T540" id="Seg_5748" s="T539">walk-PRS-2SG</ta>
            <ta e="T541" id="Seg_5749" s="T540">forest-LAT</ta>
            <ta e="T542" id="Seg_5750" s="T541">walk-PRS-1SG</ta>
            <ta e="T543" id="Seg_5751" s="T542">berry-VBLZ-CVB</ta>
            <ta e="T544" id="Seg_5752" s="T543">there</ta>
            <ta e="T545" id="Seg_5753" s="T544">bear.[NOM.SG]</ta>
            <ta e="T546" id="Seg_5754" s="T545">very</ta>
            <ta e="T547" id="Seg_5755" s="T546">fear-PRS-1SG</ta>
            <ta e="T548" id="Seg_5756" s="T547">well</ta>
            <ta e="T549" id="Seg_5757" s="T548">NEG.AUX-IMP.2SG</ta>
            <ta e="T550" id="Seg_5758" s="T549">go-EP-CNG</ta>
            <ta e="T551" id="Seg_5759" s="T550">fear-PRS-2SG</ta>
            <ta e="T552" id="Seg_5760" s="T551">so</ta>
            <ta e="T555" id="Seg_5761" s="T554">one.[NOM.SG]</ta>
            <ta e="T556" id="Seg_5762" s="T555">two.[NOM.SG]</ta>
            <ta e="T557" id="Seg_5763" s="T556">three.[NOM.SG]</ta>
            <ta e="T558" id="Seg_5764" s="T557">four.[NOM.SG]</ta>
            <ta e="T559" id="Seg_5765" s="T558">five.[NOM.SG]</ta>
            <ta e="T560" id="Seg_5766" s="T559">seven.[NOM.SG]</ta>
            <ta e="T562" id="Seg_5767" s="T561">seven.[NOM.SG]</ta>
            <ta e="T563" id="Seg_5768" s="T562">one.[NOM.SG]</ta>
            <ta e="T564" id="Seg_5769" s="T563">two.[NOM.SG]</ta>
            <ta e="T565" id="Seg_5770" s="T564">three.[NOM.SG]</ta>
            <ta e="T566" id="Seg_5771" s="T565">six.[NOM.SG]</ta>
            <ta e="T567" id="Seg_5772" s="T566">seven.[NOM.SG]</ta>
            <ta e="T568" id="Seg_5773" s="T567">five.[NOM.SG]</ta>
            <ta e="T569" id="Seg_5774" s="T568">eight.[NOM.SG]</ta>
            <ta e="T570" id="Seg_5775" s="T569">nine.[NOM.SG]</ta>
            <ta e="T571" id="Seg_5776" s="T570">ten.[NOM.SG]</ta>
            <ta e="T574" id="Seg_5777" s="T573">ten.[NOM.SG]</ta>
            <ta e="T575" id="Seg_5778" s="T574">one.[NOM.SG]</ta>
            <ta e="T576" id="Seg_5779" s="T575">ten.[NOM.SG]</ta>
            <ta e="T577" id="Seg_5780" s="T576">two.[NOM.SG]</ta>
            <ta e="T578" id="Seg_5781" s="T577">ten.[NOM.SG]</ta>
            <ta e="T579" id="Seg_5782" s="T578">three.[NOM.SG]</ta>
            <ta e="T580" id="Seg_5783" s="T579">ten.[NOM.SG]</ta>
            <ta e="T581" id="Seg_5784" s="T580">four.[NOM.SG]</ta>
            <ta e="T582" id="Seg_5785" s="T581">ten.[NOM.SG]</ta>
            <ta e="T583" id="Seg_5786" s="T582">five.[NOM.SG]</ta>
            <ta e="T584" id="Seg_5787" s="T583">ten.[NOM.SG]</ta>
            <ta e="T585" id="Seg_5788" s="T584">seven.[NOM.SG]</ta>
            <ta e="T586" id="Seg_5789" s="T585">ten.[NOM.SG]</ta>
            <ta e="T587" id="Seg_5790" s="T586">eight.[NOM.SG]</ta>
            <ta e="T588" id="Seg_5791" s="T587">ten.[NOM.SG]</ta>
            <ta e="T589" id="Seg_5792" s="T588">seven.[NOM.SG]</ta>
            <ta e="T590" id="Seg_5793" s="T589">ten.[NOM.SG]</ta>
            <ta e="T591" id="Seg_5794" s="T590">two.[NOM.SG]</ta>
            <ta e="T592" id="Seg_5795" s="T591">ten.[NOM.SG]</ta>
            <ta e="T596" id="Seg_5796" s="T595">milk-INF.LAT</ta>
            <ta e="T597" id="Seg_5797" s="T596">one.should</ta>
            <ta e="T598" id="Seg_5798" s="T597">cow-EP-ACC</ta>
            <ta e="T599" id="Seg_5799" s="T598">corral-NOM/GEN/ACC.3SG</ta>
            <ta e="T600" id="Seg_5800" s="T599">close-INF.LAT</ta>
            <ta e="T601" id="Seg_5801" s="T600">one.should</ta>
            <ta e="T602" id="Seg_5802" s="T601">milk-INF.LAT</ta>
            <ta e="T603" id="Seg_5803" s="T602">one.should</ta>
            <ta e="T604" id="Seg_5804" s="T603">milk.[NOM.SG]</ta>
            <ta e="T605" id="Seg_5805" s="T604">bring-INF.LAT</ta>
            <ta e="T606" id="Seg_5806" s="T605">birchbark.vessel-LAT</ta>
            <ta e="T607" id="Seg_5807" s="T606">put-INF.LAT</ta>
            <ta e="T608" id="Seg_5808" s="T607">pour-INF.LAT</ta>
            <ta e="T615" id="Seg_5809" s="T614">where.to</ta>
            <ta e="T616" id="Seg_5810" s="T615">walk-PRS-2SG</ta>
            <ta e="T617" id="Seg_5811" s="T616">where.to</ta>
            <ta e="T618" id="Seg_5812" s="T617">run-DUR-2SG</ta>
            <ta e="T619" id="Seg_5813" s="T618">cow-NOM/GEN/ACC.1SG</ta>
            <ta e="T620" id="Seg_5814" s="T619">NEG.EX.[3SG]</ta>
            <ta e="T622" id="Seg_5815" s="T621">look-FRQ-INF.LAT</ta>
            <ta e="T624" id="Seg_5816" s="T623">go-FUT-1SG</ta>
            <ta e="T625" id="Seg_5817" s="T624">go-OPT.DU/PL-1DU</ta>
            <ta e="T626" id="Seg_5818" s="T625">I.NOM-INS</ta>
            <ta e="T627" id="Seg_5819" s="T626">otherwise</ta>
            <ta e="T628" id="Seg_5820" s="T627">I.NOM</ta>
            <ta e="T629" id="Seg_5821" s="T628">fear-PRS-1SG</ta>
            <ta e="T635" id="Seg_5822" s="T634">hello</ta>
            <ta e="T636" id="Seg_5823" s="T635">be-PRS-2SG</ta>
            <ta e="T637" id="Seg_5824" s="T636">sit-IMP.2SG</ta>
            <ta e="T638" id="Seg_5825" s="T637">eat-INF.LAT</ta>
            <ta e="T639" id="Seg_5826" s="T638">bread.[NOM.SG]</ta>
            <ta e="T640" id="Seg_5827" s="T639">bird.cherry.[NOM.SG]</ta>
            <ta e="T641" id="Seg_5828" s="T640">berry-INS</ta>
            <ta e="T642" id="Seg_5829" s="T641">butter.[NOM.SG]</ta>
            <ta e="T643" id="Seg_5830" s="T642">meat.[NOM.SG]</ta>
            <ta e="T644" id="Seg_5831" s="T643">eat-EP-IMP.2SG</ta>
            <ta e="T645" id="Seg_5832" s="T644">berry.[NOM.SG]</ta>
            <ta e="T646" id="Seg_5833" s="T645">eat-EP-IMP.2SG</ta>
            <ta e="T647" id="Seg_5834" s="T646">cream.[NOM.SG]</ta>
            <ta e="T648" id="Seg_5835" s="T647">eat-EP-IMP.2SG</ta>
            <ta e="T650" id="Seg_5836" s="T649">bread.[NOM.SG]</ta>
            <ta e="T651" id="Seg_5837" s="T650">eat-EP-IMP.2SG</ta>
            <ta e="T652" id="Seg_5838" s="T651">yellow.[NOM.SG]</ta>
            <ta e="T653" id="Seg_5839" s="T652">water.[NOM.SG]</ta>
            <ta e="T654" id="Seg_5840" s="T653">drink-EP-IMP.2SG</ta>
            <ta e="T655" id="Seg_5841" s="T654">sugar.[NOM.SG]</ta>
            <ta e="T656" id="Seg_5842" s="T655">put-IMP.2SG.O</ta>
            <ta e="T657" id="Seg_5843" s="T656">yellow.[NOM.SG]</ta>
            <ta e="T658" id="Seg_5844" s="T657">water-LAT</ta>
            <ta e="T659" id="Seg_5845" s="T658">milk.[NOM.SG]</ta>
            <ta e="T660" id="Seg_5846" s="T659">sit-IMP.2SG</ta>
            <ta e="T662" id="Seg_5847" s="T661">go-OPT.DU/PL-1DU</ta>
            <ta e="T663" id="Seg_5848" s="T662">together</ta>
            <ta e="T664" id="Seg_5849" s="T663">forest-LAT</ta>
            <ta e="T665" id="Seg_5850" s="T664">berry.[NOM.SG]</ta>
            <ta e="T666" id="Seg_5851" s="T665">collect-INF.LAT</ta>
            <ta e="T667" id="Seg_5852" s="T666">bear.leek.[NOM.SG]</ta>
            <ta e="T668" id="Seg_5853" s="T667">tear-INF.LAT</ta>
            <ta e="T669" id="Seg_5854" s="T668">pine.nut.[NOM.SG]</ta>
            <ta e="T670" id="Seg_5855" s="T669">make-INF.LAT</ta>
            <ta e="T671" id="Seg_5856" s="T670">more</ta>
            <ta e="T672" id="Seg_5857" s="T671">what.[NOM.SG]</ta>
            <ta e="T673" id="Seg_5858" s="T672">mushroom.[NOM.SG]</ta>
            <ta e="T677" id="Seg_5859" s="T676">mushroom.[NOM.SG]</ta>
            <ta e="T678" id="Seg_5860" s="T677">take-INF.LAT</ta>
            <ta e="T683" id="Seg_5861" s="T682">I.NOM</ta>
            <ta e="T684" id="Seg_5862" s="T683">you.ACC</ta>
            <ta e="T685" id="Seg_5863" s="T684">pity-DUR-1SG</ta>
            <ta e="T689" id="Seg_5864" s="T688">go-EP-IMP.2SG</ta>
            <ta e="T690" id="Seg_5865" s="T689">horse.[NOM.SG]</ta>
            <ta e="T691" id="Seg_5866" s="T690">harness-EP-IMP.2SG</ta>
            <ta e="T692" id="Seg_5867" s="T691">go-OPT.DU/PL-1DU</ta>
            <ta e="T693" id="Seg_5868" s="T692">where.to=INDEF</ta>
            <ta e="T694" id="Seg_5869" s="T693">horse-INS</ta>
            <ta e="T695" id="Seg_5870" s="T694">water.[NOM.SG]</ta>
            <ta e="T697" id="Seg_5871" s="T696">bring-FUT-1DU</ta>
            <ta e="T698" id="Seg_5872" s="T697">tree-PL</ta>
            <ta e="T699" id="Seg_5873" s="T698">bring-FUT-1PL</ta>
            <ta e="T700" id="Seg_5874" s="T699">grass.[NOM.SG]</ta>
            <ta e="T701" id="Seg_5875" s="T700">bring-FUT-1DU</ta>
            <ta e="T702" id="Seg_5876" s="T701">straw.[NOM.SG]</ta>
            <ta e="T703" id="Seg_5877" s="T702">bring-FUT-1DU</ta>
            <ta e="T709" id="Seg_5878" s="T708">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T710" id="Seg_5879" s="T709">come-PST.[3SG]</ta>
            <ta e="T711" id="Seg_5880" s="T710">one.should</ta>
            <ta e="T713" id="Seg_5881" s="T712">look-FRQ-INF.LAT</ta>
            <ta e="T714" id="Seg_5882" s="T713">go-INF.LAT</ta>
            <ta e="T716" id="Seg_5883" s="T715">one.should</ta>
            <ta e="T717" id="Seg_5884" s="T716">call-INF.LAT</ta>
            <ta e="T718" id="Seg_5885" s="T717">eat-INF.LAT</ta>
            <ta e="T719" id="Seg_5886" s="T718">give-INF.LAT</ta>
            <ta e="T720" id="Seg_5887" s="T719">one.should</ta>
            <ta e="T721" id="Seg_5888" s="T720">sleep-INF.LAT</ta>
            <ta e="T722" id="Seg_5889" s="T721">this-PL-LAT</ta>
            <ta e="T725" id="Seg_5890" s="T724">one.[NOM.SG]</ta>
            <ta e="T726" id="Seg_5891" s="T725">one.[NOM.SG]</ta>
            <ta e="T727" id="Seg_5892" s="T726">two.[NOM.SG]</ta>
            <ta e="T728" id="Seg_5893" s="T727">three.[NOM.SG]</ta>
            <ta e="T729" id="Seg_5894" s="T728">four.[NOM.SG]</ta>
            <ta e="T730" id="Seg_5895" s="T729">five.[NOM.SG]</ta>
            <ta e="T731" id="Seg_5896" s="T730">seven.[NOM.SG]</ta>
            <ta e="T732" id="Seg_5897" s="T731">nine.[NOM.SG]</ta>
            <ta e="T733" id="Seg_5898" s="T732">eight.[NOM.SG]</ta>
            <ta e="T734" id="Seg_5899" s="T733">ten.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T18" id="Seg_5900" s="T17">NEG.AUX-IMP.2SG</ta>
            <ta e="T19" id="Seg_5901" s="T18">ругать-DES-CNG</ta>
            <ta e="T23" id="Seg_5902" s="T22">я.NOM</ta>
            <ta e="T24" id="Seg_5903" s="T23">этот-ACC</ta>
            <ta e="T25" id="Seg_5904" s="T24">быть-PST-1SG</ta>
            <ta e="T28" id="Seg_5905" s="T27">там</ta>
            <ta e="T29" id="Seg_5906" s="T28">PTCL</ta>
            <ta e="T30" id="Seg_5907" s="T29">ругать-DES-DUR-3PL</ta>
            <ta e="T31" id="Seg_5908" s="T30">я.NOM</ta>
            <ta e="T742" id="Seg_5909" s="T31">пойти-CVB</ta>
            <ta e="T32" id="Seg_5910" s="T742">исчезнуть-PST-1SG</ta>
            <ta e="T37" id="Seg_5911" s="T36">много</ta>
            <ta e="T38" id="Seg_5912" s="T37">писать-PST-2SG</ta>
            <ta e="T39" id="Seg_5913" s="T38">PTCL</ta>
            <ta e="T41" id="Seg_5914" s="T40">вождь.[NOM.SG]</ta>
            <ta e="T42" id="Seg_5915" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_5916" s="T42">толстый.[NOM.SG]</ta>
            <ta e="T44" id="Seg_5917" s="T43">прийти-PST.[3SG]</ta>
            <ta e="T45" id="Seg_5918" s="T44">пьяный.[NOM.SG]</ta>
            <ta e="T46" id="Seg_5919" s="T45">PTCL</ta>
            <ta e="T54" id="Seg_5920" s="T52">быть-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_5921" s="T56">мыло</ta>
            <ta e="T58" id="Seg_5922" s="T57">быть-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_5923" s="T59">тереть-IMP.2SG.O</ta>
            <ta e="T61" id="Seg_5924" s="T60">PTCL</ta>
            <ta e="T72" id="Seg_5925" s="T71">очень</ta>
            <ta e="T73" id="Seg_5926" s="T72">ты.NOM</ta>
            <ta e="T74" id="Seg_5927" s="T73">черный.[NOM.SG]</ta>
            <ta e="T75" id="Seg_5928" s="T74">пойти-EP-IMP.2SG</ta>
            <ta e="T76" id="Seg_5929" s="T75">баня-LAT</ta>
            <ta e="T78" id="Seg_5930" s="T77">там</ta>
            <ta e="T79" id="Seg_5931" s="T78">теплый.[NOM.SG]</ta>
            <ta e="T80" id="Seg_5932" s="T79">быть-PRS.[3SG]</ta>
            <ta e="T81" id="Seg_5933" s="T80">и</ta>
            <ta e="T82" id="Seg_5934" s="T81">холодный.[NOM.SG]</ta>
            <ta e="T83" id="Seg_5935" s="T82">вода.[NOM.SG]</ta>
            <ta e="T84" id="Seg_5936" s="T83">мыло</ta>
            <ta e="T85" id="Seg_5937" s="T84">быть-PRS.[3SG]</ta>
            <ta e="T86" id="Seg_5938" s="T85">мыть-IMP.2SG</ta>
            <ta e="T88" id="Seg_5939" s="T87">тереть-IMP.2SG.O</ta>
            <ta e="T91" id="Seg_5940" s="T89">то</ta>
            <ta e="T108" id="Seg_5941" s="T107">корова-PL</ta>
            <ta e="T109" id="Seg_5942" s="T108">кораль-NOM/GEN/ACC.3SG</ta>
            <ta e="T112" id="Seg_5943" s="T111">камасинец-PL</ta>
            <ta e="T113" id="Seg_5944" s="T112">прийти-PST-3PL</ta>
            <ta e="T114" id="Seg_5945" s="T113">сидеть-IMP.2SG</ta>
            <ta e="T115" id="Seg_5946" s="T114">есть-EP-IMP.2SG</ta>
            <ta e="T116" id="Seg_5947" s="T115">наверное</ta>
            <ta e="T117" id="Seg_5948" s="T116">есть-INF.LAT</ta>
            <ta e="T118" id="Seg_5949" s="T117">хочется</ta>
            <ta e="T119" id="Seg_5950" s="T118">где</ta>
            <ta e="T120" id="Seg_5951" s="T119">быть-PST-2SG</ta>
            <ta e="T121" id="Seg_5952" s="T120">сидеть-IMP.2SG</ta>
            <ta e="T122" id="Seg_5953" s="T121">здесь</ta>
            <ta e="T123" id="Seg_5954" s="T122">красивый.[NOM.SG]</ta>
            <ta e="T124" id="Seg_5955" s="T123">мужчина.[NOM.SG]</ta>
            <ta e="T129" id="Seg_5956" s="T128">надо</ta>
            <ta e="T130" id="Seg_5957" s="T129">дом.[NOM.SG]</ta>
            <ta e="T131" id="Seg_5958" s="T130">мыться-INF.LAT</ta>
            <ta e="T132" id="Seg_5959" s="T131">надо</ta>
            <ta e="T133" id="Seg_5960" s="T132">мести-INF.LAT</ta>
            <ta e="T134" id="Seg_5961" s="T133">наружу</ta>
            <ta e="T135" id="Seg_5962" s="T134">наружу</ta>
            <ta e="T136" id="Seg_5963" s="T135">пойти-INF.LAT</ta>
            <ta e="T137" id="Seg_5964" s="T136">корова.[NOM.SG]</ta>
            <ta e="T138" id="Seg_5965" s="T137">доить-INF.LAT</ta>
            <ta e="T139" id="Seg_5966" s="T138">ребенок-ACC</ta>
            <ta e="T140" id="Seg_5967" s="T139">мыться-INF.LAT</ta>
            <ta e="T142" id="Seg_5968" s="T141">хлеб.[NOM.SG]</ta>
            <ta e="T143" id="Seg_5969" s="T142">печь-INF.LAT</ta>
            <ta e="T144" id="Seg_5970" s="T143">хлеб.[NOM.SG]</ta>
            <ta e="T145" id="Seg_5971" s="T144">поставить-INF.LAT</ta>
            <ta e="T148" id="Seg_5972" s="T146">печь-INF.LAT</ta>
            <ta e="T149" id="Seg_5973" s="T148">жир</ta>
            <ta e="T150" id="Seg_5974" s="T149">печь-INF.LAT</ta>
            <ta e="T151" id="Seg_5975" s="T150">надо</ta>
            <ta e="T166" id="Seg_5976" s="T165">желтый.[NOM.SG]</ta>
            <ta e="T167" id="Seg_5977" s="T166">вода.[NOM.SG]</ta>
            <ta e="T168" id="Seg_5978" s="T167">надо</ta>
            <ta e="T169" id="Seg_5979" s="T168">кипятить-INF.LAT</ta>
            <ta e="T171" id="Seg_5980" s="T169">кричать-DUR.[3SG]</ta>
            <ta e="T172" id="Seg_5981" s="T171">NEG.AUX-IMP.2SG</ta>
            <ta e="T173" id="Seg_5982" s="T172">ссориться-EP-CNG</ta>
            <ta e="T174" id="Seg_5983" s="T173">NEG.AUX-IMP.2SG</ta>
            <ta e="T175" id="Seg_5984" s="T174">шуметь-EP-CNG</ta>
            <ta e="T183" id="Seg_5985" s="T182">я.NOM</ta>
            <ta e="T184" id="Seg_5986" s="T183">там</ta>
            <ta e="T185" id="Seg_5987" s="T184">быть-PST-1SG</ta>
            <ta e="T186" id="Seg_5988" s="T185">там</ta>
            <ta e="T187" id="Seg_5989" s="T186">PTCL</ta>
            <ta e="T189" id="Seg_5990" s="T188">ругать-DES-DUR-3PL</ta>
            <ta e="T190" id="Seg_5991" s="T189">я.NOM</ta>
            <ta e="T743" id="Seg_5992" s="T190">пойти-CVB</ta>
            <ta e="T191" id="Seg_5993" s="T743">исчезнуть-PST-1SG</ta>
            <ta e="T194" id="Seg_5994" s="T193">рука-NOM/GEN/ACC.2SG</ta>
            <ta e="T195" id="Seg_5995" s="T194">наверное</ta>
            <ta e="T196" id="Seg_5996" s="T195">болеть-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_5997" s="T200">соль.[NOM.SG]</ta>
            <ta e="T202" id="Seg_5998" s="T201">NEG.EX.[3SG]</ta>
            <ta e="T203" id="Seg_5999" s="T202">надо</ta>
            <ta e="T204" id="Seg_6000" s="T203">солить-INF.LAT</ta>
            <ta e="T206" id="Seg_6001" s="T204">что-INS</ta>
            <ta e="T207" id="Seg_6002" s="T206">съесть-INF.LAT</ta>
            <ta e="T208" id="Seg_6003" s="T207">ложка.[NOM.SG]</ta>
            <ta e="T209" id="Seg_6004" s="T208">NEG.EX.[3SG]</ta>
            <ta e="T210" id="Seg_6005" s="T209">принести-IMP.2SG</ta>
            <ta e="T211" id="Seg_6006" s="T210">ложка.[NOM.SG]</ta>
            <ta e="T212" id="Seg_6007" s="T211">тот.[NOM.SG]</ta>
            <ta e="T213" id="Seg_6008" s="T212">ложка.[NOM.SG]</ta>
            <ta e="T214" id="Seg_6009" s="T213">дать-PST-1SG</ta>
            <ta e="T215" id="Seg_6010" s="T214">ты.DAT</ta>
            <ta e="T216" id="Seg_6011" s="T215">скоро</ta>
            <ta e="T217" id="Seg_6012" s="T216">съесть-EP-IMP.2SG</ta>
            <ta e="T222" id="Seg_6013" s="T221">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T224" id="Seg_6014" s="T223">вместе</ta>
            <ta e="T225" id="Seg_6015" s="T224">очень</ta>
            <ta e="T226" id="Seg_6016" s="T225">красивый.[NOM.SG]</ta>
            <ta e="T227" id="Seg_6017" s="T226">девушка-PL</ta>
            <ta e="T228" id="Seg_6018" s="T227">очень</ta>
            <ta e="T229" id="Seg_6019" s="T228">красивый.[NOM.SG]</ta>
            <ta e="T230" id="Seg_6020" s="T229">мальчик-PL</ta>
            <ta e="T231" id="Seg_6021" s="T230">очень</ta>
            <ta e="T232" id="Seg_6022" s="T231">красивый.[NOM.SG]</ta>
            <ta e="T233" id="Seg_6023" s="T232">люди.[NOM.SG]</ta>
            <ta e="T234" id="Seg_6024" s="T233">очень</ta>
            <ta e="T235" id="Seg_6025" s="T234">хороший.[NOM.SG]</ta>
            <ta e="T236" id="Seg_6026" s="T235">мужчина.[NOM.SG]</ta>
            <ta e="T237" id="Seg_6027" s="T236">очень</ta>
            <ta e="T238" id="Seg_6028" s="T237">лгать-DUR.[3SG]</ta>
            <ta e="T239" id="Seg_6029" s="T238">мужчина.[NOM.SG]</ta>
            <ta e="T242" id="Seg_6030" s="T241">надо</ta>
            <ta e="T243" id="Seg_6031" s="T242">спать-INF.LAT</ta>
            <ta e="T245" id="Seg_6032" s="T244">немного</ta>
            <ta e="T248" id="Seg_6033" s="T247">хочется</ta>
            <ta e="T249" id="Seg_6034" s="T248">очень</ta>
            <ta e="T250" id="Seg_6035" s="T249">живот-NOM/GEN/ACC.1SG</ta>
            <ta e="T251" id="Seg_6036" s="T250">болеть-PRS.[3SG]</ta>
            <ta e="T252" id="Seg_6037" s="T251">есть-EP-IMP.2SG</ta>
            <ta e="T253" id="Seg_6038" s="T252">сидеть-IMP.2SG</ta>
            <ta e="T254" id="Seg_6039" s="T253">NEG.AUX-IMP.2SG</ta>
            <ta e="T255" id="Seg_6040" s="T254">кричать-EP-CNG</ta>
            <ta e="T256" id="Seg_6041" s="T255">NEG.AUX-IMP.2SG</ta>
            <ta e="T257" id="Seg_6042" s="T256">шуметь-EP-CNG</ta>
            <ta e="T259" id="Seg_6043" s="T257">надо</ta>
            <ta e="T260" id="Seg_6044" s="T259">взять-INF.LAT</ta>
            <ta e="T261" id="Seg_6045" s="T260">а.то</ta>
            <ta e="T269" id="Seg_6046" s="T741">лес-LAT</ta>
            <ta e="T270" id="Seg_6047" s="T269">пойти-INF.LAT</ta>
            <ta e="T271" id="Seg_6048" s="T270">ягода.[NOM.SG]</ta>
            <ta e="T272" id="Seg_6049" s="T271">принести-INF.LAT</ta>
            <ta e="T273" id="Seg_6050" s="T272">кедровый.орех.[NOM.SG]</ta>
            <ta e="T274" id="Seg_6051" s="T273">принести-INF.LAT</ta>
            <ta e="T275" id="Seg_6052" s="T274">скоро</ta>
            <ta e="T276" id="Seg_6053" s="T275">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T277" id="Seg_6054" s="T276">два-COLL</ta>
            <ta e="T280" id="Seg_6055" s="T279">что=INDEF</ta>
            <ta e="T281" id="Seg_6056" s="T280">убить-FUT-1SG</ta>
            <ta e="T282" id="Seg_6057" s="T281">этот-ACC</ta>
            <ta e="T283" id="Seg_6058" s="T282">коза.[NOM.SG]</ta>
            <ta e="T284" id="Seg_6059" s="T283">убить-PST-1SG</ta>
            <ta e="T285" id="Seg_6060" s="T284">PTCL</ta>
            <ta e="T287" id="Seg_6061" s="T286">соболь.[NOM.SG]</ta>
            <ta e="T288" id="Seg_6062" s="T287">убить-PST-1SG</ta>
            <ta e="T289" id="Seg_6063" s="T288">белка.[NOM.SG]</ta>
            <ta e="T290" id="Seg_6064" s="T289">убить-PST-1SG</ta>
            <ta e="T291" id="Seg_6065" s="T290">олень.[NOM.SG]</ta>
            <ta e="T292" id="Seg_6066" s="T291">убить-PST-1SG</ta>
            <ta e="T293" id="Seg_6067" s="T292">медведь.[NOM.SG]</ta>
            <ta e="T294" id="Seg_6068" s="T293">убить-PST-1SG</ta>
            <ta e="T299" id="Seg_6069" s="T298">болеть-PRS.[3SG]</ta>
            <ta e="T300" id="Seg_6070" s="T299">очень</ta>
            <ta e="T301" id="Seg_6071" s="T300">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T302" id="Seg_6072" s="T301">болеть-PRS.[3SG]</ta>
            <ta e="T303" id="Seg_6073" s="T302">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T304" id="Seg_6074" s="T303">болеть-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_6075" s="T304">спина-NOM/GEN/ACC.1SG</ta>
            <ta e="T306" id="Seg_6076" s="T305">болеть-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_6077" s="T306">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T308" id="Seg_6078" s="T307">болеть-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_6079" s="T320">испражняться-INF.LAT</ta>
            <ta e="T322" id="Seg_6080" s="T321">пойти-INF.LAT</ta>
            <ta e="T323" id="Seg_6081" s="T322">мочиться-INF.LAT</ta>
            <ta e="T324" id="Seg_6082" s="T323">надо</ta>
            <ta e="T325" id="Seg_6083" s="T324">пойти-INF.LAT</ta>
            <ta e="T327" id="Seg_6084" s="T325">надо</ta>
            <ta e="T341" id="Seg_6085" s="T340">женщина.[NOM.SG]</ta>
            <ta e="T342" id="Seg_6086" s="T341">и</ta>
            <ta e="T343" id="Seg_6087" s="T342">мужчина.[NOM.SG]</ta>
            <ta e="T344" id="Seg_6088" s="T343">этот-PL</ta>
            <ta e="T346" id="Seg_6089" s="T345">ребенок-PL-LAT</ta>
            <ta e="T347" id="Seg_6090" s="T346">быть-PST-3PL</ta>
            <ta e="T348" id="Seg_6091" s="T347">сын.[NOM.SG]</ta>
            <ta e="T349" id="Seg_6092" s="T348">и</ta>
            <ta e="T350" id="Seg_6093" s="T349">дочь.[NOM.SG]</ta>
            <ta e="T356" id="Seg_6094" s="T355">жить-PST-3PL</ta>
            <ta e="T357" id="Seg_6095" s="T356">женщина.[NOM.SG]</ta>
            <ta e="T358" id="Seg_6096" s="T357">и</ta>
            <ta e="T359" id="Seg_6097" s="T358">мужчина.[NOM.SG]</ta>
            <ta e="T360" id="Seg_6098" s="T359">этот-PL-LAT</ta>
            <ta e="T361" id="Seg_6099" s="T360">ребенок-PL-LAT</ta>
            <ta e="T362" id="Seg_6100" s="T361">быть-PST-3PL</ta>
            <ta e="T363" id="Seg_6101" s="T362">сын-NOM/GEN.3SG</ta>
            <ta e="T364" id="Seg_6102" s="T363">и</ta>
            <ta e="T365" id="Seg_6103" s="T364">дочь.[NOM.SG]</ta>
            <ta e="T366" id="Seg_6104" s="T365">мальчик.[NOM.SG]</ta>
            <ta e="T367" id="Seg_6105" s="T366">и</ta>
            <ta e="T368" id="Seg_6106" s="T367">дочь.[NOM.SG]</ta>
            <ta e="T369" id="Seg_6107" s="T368">один.[NOM.SG]</ta>
            <ta e="T370" id="Seg_6108" s="T369">два.[NOM.SG]</ta>
            <ta e="T371" id="Seg_6109" s="T370">три.[NOM.SG]</ta>
            <ta e="T372" id="Seg_6110" s="T371">пять.[NOM.SG]</ta>
            <ta e="T373" id="Seg_6111" s="T372">шесть.[NOM.SG]</ta>
            <ta e="T374" id="Seg_6112" s="T373">семь.[NOM.SG]</ta>
            <ta e="T375" id="Seg_6113" s="T374">восемь.[NOM.SG]</ta>
            <ta e="T376" id="Seg_6114" s="T375">девять.[NOM.SG]</ta>
            <ta e="T377" id="Seg_6115" s="T376">десять.[NOM.SG]</ta>
            <ta e="T378" id="Seg_6116" s="T377">десять.[NOM.SG]</ta>
            <ta e="T379" id="Seg_6117" s="T378">один.[NOM.SG]</ta>
            <ta e="T380" id="Seg_6118" s="T379">десять.[NOM.SG]</ta>
            <ta e="T381" id="Seg_6119" s="T380">два.[NOM.SG]</ta>
            <ta e="T382" id="Seg_6120" s="T381">десять.[NOM.SG]</ta>
            <ta e="T383" id="Seg_6121" s="T382">три.[NOM.SG]</ta>
            <ta e="T384" id="Seg_6122" s="T383">десять.[NOM.SG]</ta>
            <ta e="T385" id="Seg_6123" s="T384">четыре.[NOM.SG]</ta>
            <ta e="T386" id="Seg_6124" s="T385">десять.[NOM.SG]</ta>
            <ta e="T387" id="Seg_6125" s="T386">пять.[NOM.SG]</ta>
            <ta e="T388" id="Seg_6126" s="T387">десять.[NOM.SG]</ta>
            <ta e="T389" id="Seg_6127" s="T388">шесть.[NOM.SG]</ta>
            <ta e="T390" id="Seg_6128" s="T389">десять.[NOM.SG]</ta>
            <ta e="T391" id="Seg_6129" s="T390">семь.[NOM.SG]</ta>
            <ta e="T392" id="Seg_6130" s="T391">десять.[NOM.SG]</ta>
            <ta e="T393" id="Seg_6131" s="T392">восемь.[NOM.SG]</ta>
            <ta e="T395" id="Seg_6132" s="T393">десять.[NOM.SG]</ta>
            <ta e="T419" id="Seg_6133" s="T418">один.[NOM.SG]</ta>
            <ta e="T420" id="Seg_6134" s="T419">два.[NOM.SG]</ta>
            <ta e="T421" id="Seg_6135" s="T420">три.[NOM.SG]</ta>
            <ta e="T422" id="Seg_6136" s="T421">шесть.[NOM.SG]</ta>
            <ta e="T423" id="Seg_6137" s="T422">семь.[NOM.SG]</ta>
            <ta e="T424" id="Seg_6138" s="T423">восемь.[NOM.SG]</ta>
            <ta e="T425" id="Seg_6139" s="T424">девять.[NOM.SG]</ta>
            <ta e="T427" id="Seg_6140" s="T426">я.LAT</ta>
            <ta e="T428" id="Seg_6141" s="T427">надо</ta>
            <ta e="T429" id="Seg_6142" s="T428">жениться-INF.LAT</ta>
            <ta e="T430" id="Seg_6143" s="T429">а</ta>
            <ta e="T431" id="Seg_6144" s="T430">кто-ACC</ta>
            <ta e="T432" id="Seg_6145" s="T431">этот.[NOM.SG]</ta>
            <ta e="T433" id="Seg_6146" s="T432">дочь.[NOM.SG]</ta>
            <ta e="T434" id="Seg_6147" s="T433">пойти-EP-IMP.2SG</ta>
            <ta e="T435" id="Seg_6148" s="T434">жениться-IMP.2SG.O</ta>
            <ta e="T439" id="Seg_6149" s="T438">дом-LOC</ta>
            <ta e="T440" id="Seg_6150" s="T439">жить-DUR-1SG</ta>
            <ta e="T441" id="Seg_6151" s="T440">о</ta>
            <ta e="T442" id="Seg_6152" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_6153" s="T442">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T444" id="Seg_6154" s="T443">%%-PRS.[3SG]</ta>
            <ta e="T446" id="Seg_6155" s="T444">надо</ta>
            <ta e="T448" id="Seg_6156" s="T447">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T449" id="Seg_6157" s="T448">болеть-PRS.[3SG]</ta>
            <ta e="T450" id="Seg_6158" s="T449">целый-NOM/GEN/ACC.1SG</ta>
            <ta e="T451" id="Seg_6159" s="T450">болеть-PRS.[3SG]</ta>
            <ta e="T452" id="Seg_6160" s="T451">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T453" id="Seg_6161" s="T452">болеть-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_6162" s="T453">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T455" id="Seg_6163" s="T454">болеть-PRS.[3SG]</ta>
            <ta e="T456" id="Seg_6164" s="T455">ухо.[NOM.SG]</ta>
            <ta e="T457" id="Seg_6165" s="T456">болеть-PRS.[3SG]</ta>
            <ta e="T458" id="Seg_6166" s="T457">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T459" id="Seg_6167" s="T458">болеть-PRS.[3SG]</ta>
            <ta e="T460" id="Seg_6168" s="T459">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T461" id="Seg_6169" s="T460">болеть-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_6170" s="T462">нет</ta>
            <ta e="T465" id="Seg_6171" s="T464">лес-LAT</ta>
            <ta e="T466" id="Seg_6172" s="T465">надо</ta>
            <ta e="T467" id="Seg_6173" s="T466">пойти-INF.LAT</ta>
            <ta e="T468" id="Seg_6174" s="T467">соболь.[NOM.SG]</ta>
            <ta e="T469" id="Seg_6175" s="T468">убить-INF.LAT</ta>
            <ta e="T470" id="Seg_6176" s="T469">медведь.[NOM.SG]</ta>
            <ta e="T471" id="Seg_6177" s="T470">убить-INF.LAT</ta>
            <ta e="T472" id="Seg_6178" s="T471">надо</ta>
            <ta e="T473" id="Seg_6179" s="T472">белка.[NOM.SG]</ta>
            <ta e="T474" id="Seg_6180" s="T473">убить-INF.LAT</ta>
            <ta e="T475" id="Seg_6181" s="T474">надо</ta>
            <ta e="T476" id="Seg_6182" s="T475">лось.[NOM.SG]</ta>
            <ta e="T477" id="Seg_6183" s="T476">убить-INF.LAT</ta>
            <ta e="T478" id="Seg_6184" s="T477">надо</ta>
            <ta e="T479" id="Seg_6185" s="T478">олень.[NOM.SG]</ta>
            <ta e="T480" id="Seg_6186" s="T479">убить-INF.LAT</ta>
            <ta e="T481" id="Seg_6187" s="T480">надо</ta>
            <ta e="T482" id="Seg_6188" s="T481">коза.[NOM.SG]</ta>
            <ta e="T483" id="Seg_6189" s="T482">надо</ta>
            <ta e="T484" id="Seg_6190" s="T483">убить-INF.LAT</ta>
            <ta e="T485" id="Seg_6191" s="T484">мясо.[NOM.SG]</ta>
            <ta e="T486" id="Seg_6192" s="T485">много</ta>
            <ta e="T487" id="Seg_6193" s="T486">стать-FUT-3SG</ta>
            <ta e="T488" id="Seg_6194" s="T487">съесть-INF.LAT</ta>
            <ta e="T489" id="Seg_6195" s="T488">надо</ta>
            <ta e="T493" id="Seg_6196" s="T492">надо</ta>
            <ta e="T496" id="Seg_6197" s="T495">мясо.[NOM.SG]</ta>
            <ta e="T498" id="Seg_6198" s="T497">класть-EP-IMP.2SG</ta>
            <ta e="T499" id="Seg_6199" s="T498">огонь.[NOM.SG]</ta>
            <ta e="T500" id="Seg_6200" s="T499">гореть-DUR.[3SG]</ta>
            <ta e="T501" id="Seg_6201" s="T500">мясо.[NOM.SG]</ta>
            <ta e="T502" id="Seg_6202" s="T501">класть-EP-IMP.2SG</ta>
            <ta e="T518" id="Seg_6203" s="T517">огонь.[NOM.SG]</ta>
            <ta e="T519" id="Seg_6204" s="T518">гореть-DUR.[3SG]</ta>
            <ta e="T520" id="Seg_6205" s="T519">мясо.[NOM.SG]</ta>
            <ta e="T521" id="Seg_6206" s="T520">класть-EP-IMP.2SG</ta>
            <ta e="T523" id="Seg_6207" s="T522">JUSS</ta>
            <ta e="T524" id="Seg_6208" s="T523">печь-DUR.[3SG]</ta>
            <ta e="T526" id="Seg_6209" s="T525">огонь-LAT</ta>
            <ta e="T527" id="Seg_6210" s="T526">огонь-LAT</ta>
            <ta e="T528" id="Seg_6211" s="T527">класть-DUR.[3SG]</ta>
            <ta e="T529" id="Seg_6212" s="T528">мясо.[NOM.SG]</ta>
            <ta e="T531" id="Seg_6213" s="T530">сидеть-IMP.2SG</ta>
            <ta e="T532" id="Seg_6214" s="T531">здесь</ta>
            <ta e="T533" id="Seg_6215" s="T532">говорить-EP-IMP.2SG</ta>
            <ta e="T534" id="Seg_6216" s="T533">где</ta>
            <ta e="T535" id="Seg_6217" s="T534">быть-PST-2SG</ta>
            <ta e="T536" id="Seg_6218" s="T535">что.[NOM.SG]</ta>
            <ta e="T537" id="Seg_6219" s="T536">что.[NOM.SG]</ta>
            <ta e="T538" id="Seg_6220" s="T537">видеть-PST-2SG</ta>
            <ta e="T539" id="Seg_6221" s="T538">куда</ta>
            <ta e="T540" id="Seg_6222" s="T539">идти-PRS-2SG</ta>
            <ta e="T541" id="Seg_6223" s="T540">лес-LAT</ta>
            <ta e="T542" id="Seg_6224" s="T541">идти-PRS-1SG</ta>
            <ta e="T543" id="Seg_6225" s="T542">ягода-VBLZ-CVB</ta>
            <ta e="T544" id="Seg_6226" s="T543">там</ta>
            <ta e="T545" id="Seg_6227" s="T544">медведь.[NOM.SG]</ta>
            <ta e="T546" id="Seg_6228" s="T545">очень</ta>
            <ta e="T547" id="Seg_6229" s="T546">бояться-PRS-1SG</ta>
            <ta e="T548" id="Seg_6230" s="T547">ну</ta>
            <ta e="T549" id="Seg_6231" s="T548">NEG.AUX-IMP.2SG</ta>
            <ta e="T550" id="Seg_6232" s="T549">пойти-EP-CNG</ta>
            <ta e="T551" id="Seg_6233" s="T550">бояться-PRS-2SG</ta>
            <ta e="T552" id="Seg_6234" s="T551">так</ta>
            <ta e="T555" id="Seg_6235" s="T554">один.[NOM.SG]</ta>
            <ta e="T556" id="Seg_6236" s="T555">два.[NOM.SG]</ta>
            <ta e="T557" id="Seg_6237" s="T556">три.[NOM.SG]</ta>
            <ta e="T558" id="Seg_6238" s="T557">четыре.[NOM.SG]</ta>
            <ta e="T559" id="Seg_6239" s="T558">пять.[NOM.SG]</ta>
            <ta e="T560" id="Seg_6240" s="T559">семь.[NOM.SG]</ta>
            <ta e="T562" id="Seg_6241" s="T561">семь.[NOM.SG]</ta>
            <ta e="T563" id="Seg_6242" s="T562">один.[NOM.SG]</ta>
            <ta e="T564" id="Seg_6243" s="T563">два.[NOM.SG]</ta>
            <ta e="T565" id="Seg_6244" s="T564">три.[NOM.SG]</ta>
            <ta e="T566" id="Seg_6245" s="T565">шесть.[NOM.SG]</ta>
            <ta e="T567" id="Seg_6246" s="T566">семь.[NOM.SG]</ta>
            <ta e="T568" id="Seg_6247" s="T567">пять.[NOM.SG]</ta>
            <ta e="T569" id="Seg_6248" s="T568">восемь.[NOM.SG]</ta>
            <ta e="T570" id="Seg_6249" s="T569">девять.[NOM.SG]</ta>
            <ta e="T571" id="Seg_6250" s="T570">десять.[NOM.SG]</ta>
            <ta e="T574" id="Seg_6251" s="T573">десять.[NOM.SG]</ta>
            <ta e="T575" id="Seg_6252" s="T574">один.[NOM.SG]</ta>
            <ta e="T576" id="Seg_6253" s="T575">десять.[NOM.SG]</ta>
            <ta e="T577" id="Seg_6254" s="T576">два.[NOM.SG]</ta>
            <ta e="T578" id="Seg_6255" s="T577">десять.[NOM.SG]</ta>
            <ta e="T579" id="Seg_6256" s="T578">три.[NOM.SG]</ta>
            <ta e="T580" id="Seg_6257" s="T579">десять.[NOM.SG]</ta>
            <ta e="T581" id="Seg_6258" s="T580">четыре.[NOM.SG]</ta>
            <ta e="T582" id="Seg_6259" s="T581">десять.[NOM.SG]</ta>
            <ta e="T583" id="Seg_6260" s="T582">пять.[NOM.SG]</ta>
            <ta e="T584" id="Seg_6261" s="T583">десять.[NOM.SG]</ta>
            <ta e="T585" id="Seg_6262" s="T584">семь.[NOM.SG]</ta>
            <ta e="T586" id="Seg_6263" s="T585">десять.[NOM.SG]</ta>
            <ta e="T587" id="Seg_6264" s="T586">восемь.[NOM.SG]</ta>
            <ta e="T588" id="Seg_6265" s="T587">десять.[NOM.SG]</ta>
            <ta e="T589" id="Seg_6266" s="T588">семь.[NOM.SG]</ta>
            <ta e="T590" id="Seg_6267" s="T589">десять.[NOM.SG]</ta>
            <ta e="T591" id="Seg_6268" s="T590">два.[NOM.SG]</ta>
            <ta e="T592" id="Seg_6269" s="T591">десять.[NOM.SG]</ta>
            <ta e="T596" id="Seg_6270" s="T595">доить-INF.LAT</ta>
            <ta e="T597" id="Seg_6271" s="T596">надо</ta>
            <ta e="T598" id="Seg_6272" s="T597">корова-EP-ACC</ta>
            <ta e="T599" id="Seg_6273" s="T598">кораль-NOM/GEN/ACC.3SG</ta>
            <ta e="T600" id="Seg_6274" s="T599">закрыть-INF.LAT</ta>
            <ta e="T601" id="Seg_6275" s="T600">надо</ta>
            <ta e="T602" id="Seg_6276" s="T601">доить-INF.LAT</ta>
            <ta e="T603" id="Seg_6277" s="T602">надо</ta>
            <ta e="T604" id="Seg_6278" s="T603">молоко.[NOM.SG]</ta>
            <ta e="T605" id="Seg_6279" s="T604">принести-INF.LAT</ta>
            <ta e="T606" id="Seg_6280" s="T605">туес-LAT</ta>
            <ta e="T607" id="Seg_6281" s="T606">класть-INF.LAT</ta>
            <ta e="T608" id="Seg_6282" s="T607">лить-INF.LAT</ta>
            <ta e="T615" id="Seg_6283" s="T614">куда</ta>
            <ta e="T616" id="Seg_6284" s="T615">идти-PRS-2SG</ta>
            <ta e="T617" id="Seg_6285" s="T616">куда</ta>
            <ta e="T618" id="Seg_6286" s="T617">бежать-DUR-2SG</ta>
            <ta e="T619" id="Seg_6287" s="T618">корова-NOM/GEN/ACC.1SG</ta>
            <ta e="T620" id="Seg_6288" s="T619">NEG.EX.[3SG]</ta>
            <ta e="T622" id="Seg_6289" s="T621">смотреть-FRQ-INF.LAT</ta>
            <ta e="T624" id="Seg_6290" s="T623">пойти-FUT-1SG</ta>
            <ta e="T625" id="Seg_6291" s="T624">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T626" id="Seg_6292" s="T625">я.NOM-INS</ta>
            <ta e="T627" id="Seg_6293" s="T626">а.то</ta>
            <ta e="T628" id="Seg_6294" s="T627">я.NOM</ta>
            <ta e="T629" id="Seg_6295" s="T628">бояться-PRS-1SG</ta>
            <ta e="T635" id="Seg_6296" s="T634">здорово</ta>
            <ta e="T636" id="Seg_6297" s="T635">быть-PRS-2SG</ta>
            <ta e="T637" id="Seg_6298" s="T636">сидеть-IMP.2SG</ta>
            <ta e="T638" id="Seg_6299" s="T637">есть-INF.LAT</ta>
            <ta e="T639" id="Seg_6300" s="T638">хлеб.[NOM.SG]</ta>
            <ta e="T640" id="Seg_6301" s="T639">черемуха.[NOM.SG]</ta>
            <ta e="T641" id="Seg_6302" s="T640">ягода-INS</ta>
            <ta e="T642" id="Seg_6303" s="T641">масло.[NOM.SG]</ta>
            <ta e="T643" id="Seg_6304" s="T642">мясо.[NOM.SG]</ta>
            <ta e="T644" id="Seg_6305" s="T643">съесть-EP-IMP.2SG</ta>
            <ta e="T645" id="Seg_6306" s="T644">ягода.[NOM.SG]</ta>
            <ta e="T646" id="Seg_6307" s="T645">съесть-EP-IMP.2SG</ta>
            <ta e="T647" id="Seg_6308" s="T646">сметана.[NOM.SG]</ta>
            <ta e="T648" id="Seg_6309" s="T647">съесть-EP-IMP.2SG</ta>
            <ta e="T650" id="Seg_6310" s="T649">хлеб.[NOM.SG]</ta>
            <ta e="T651" id="Seg_6311" s="T650">съесть-EP-IMP.2SG</ta>
            <ta e="T652" id="Seg_6312" s="T651">желтый.[NOM.SG]</ta>
            <ta e="T653" id="Seg_6313" s="T652">вода.[NOM.SG]</ta>
            <ta e="T654" id="Seg_6314" s="T653">пить-EP-IMP.2SG</ta>
            <ta e="T655" id="Seg_6315" s="T654">сахар.[NOM.SG]</ta>
            <ta e="T656" id="Seg_6316" s="T655">класть-IMP.2SG.O</ta>
            <ta e="T657" id="Seg_6317" s="T656">желтый.[NOM.SG]</ta>
            <ta e="T658" id="Seg_6318" s="T657">вода-LAT</ta>
            <ta e="T659" id="Seg_6319" s="T658">молоко.[NOM.SG]</ta>
            <ta e="T660" id="Seg_6320" s="T659">сидеть-IMP.2SG</ta>
            <ta e="T662" id="Seg_6321" s="T661">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T663" id="Seg_6322" s="T662">вместе</ta>
            <ta e="T664" id="Seg_6323" s="T663">лес-LAT</ta>
            <ta e="T665" id="Seg_6324" s="T664">ягода.[NOM.SG]</ta>
            <ta e="T666" id="Seg_6325" s="T665">собирать-INF.LAT</ta>
            <ta e="T667" id="Seg_6326" s="T666">черемша.[NOM.SG]</ta>
            <ta e="T668" id="Seg_6327" s="T667">рвать-INF.LAT</ta>
            <ta e="T669" id="Seg_6328" s="T668">кедровый.орех.[NOM.SG]</ta>
            <ta e="T670" id="Seg_6329" s="T669">делать-INF.LAT</ta>
            <ta e="T671" id="Seg_6330" s="T670">еще</ta>
            <ta e="T672" id="Seg_6331" s="T671">что.[NOM.SG]</ta>
            <ta e="T673" id="Seg_6332" s="T672">гриб.[NOM.SG]</ta>
            <ta e="T677" id="Seg_6333" s="T676">гриб.[NOM.SG]</ta>
            <ta e="T678" id="Seg_6334" s="T677">взять-INF.LAT</ta>
            <ta e="T683" id="Seg_6335" s="T682">я.NOM</ta>
            <ta e="T684" id="Seg_6336" s="T683">ты.ACC</ta>
            <ta e="T685" id="Seg_6337" s="T684">жалеть-DUR-1SG</ta>
            <ta e="T689" id="Seg_6338" s="T688">пойти-EP-IMP.2SG</ta>
            <ta e="T690" id="Seg_6339" s="T689">лошадь.[NOM.SG]</ta>
            <ta e="T691" id="Seg_6340" s="T690">запрячь-EP-IMP.2SG</ta>
            <ta e="T692" id="Seg_6341" s="T691">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T693" id="Seg_6342" s="T692">куда=INDEF</ta>
            <ta e="T694" id="Seg_6343" s="T693">лошадь-INS</ta>
            <ta e="T695" id="Seg_6344" s="T694">вода.[NOM.SG]</ta>
            <ta e="T697" id="Seg_6345" s="T696">принести-FUT-1DU</ta>
            <ta e="T698" id="Seg_6346" s="T697">дерево-PL</ta>
            <ta e="T699" id="Seg_6347" s="T698">принести-FUT-1PL</ta>
            <ta e="T700" id="Seg_6348" s="T699">трава.[NOM.SG]</ta>
            <ta e="T701" id="Seg_6349" s="T700">принести-FUT-1DU</ta>
            <ta e="T702" id="Seg_6350" s="T701">солома.[NOM.SG]</ta>
            <ta e="T703" id="Seg_6351" s="T702">принести-FUT-1DU</ta>
            <ta e="T709" id="Seg_6352" s="T708">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T710" id="Seg_6353" s="T709">прийти-PST.[3SG]</ta>
            <ta e="T711" id="Seg_6354" s="T710">надо</ta>
            <ta e="T713" id="Seg_6355" s="T712">смотреть-FRQ-INF.LAT</ta>
            <ta e="T714" id="Seg_6356" s="T713">пойти-INF.LAT</ta>
            <ta e="T716" id="Seg_6357" s="T715">надо</ta>
            <ta e="T717" id="Seg_6358" s="T716">позвать-INF.LAT</ta>
            <ta e="T718" id="Seg_6359" s="T717">есть-INF.LAT</ta>
            <ta e="T719" id="Seg_6360" s="T718">дать-INF.LAT</ta>
            <ta e="T720" id="Seg_6361" s="T719">надо</ta>
            <ta e="T721" id="Seg_6362" s="T720">спать-INF.LAT</ta>
            <ta e="T722" id="Seg_6363" s="T721">этот-PL-LAT</ta>
            <ta e="T725" id="Seg_6364" s="T724">один.[NOM.SG]</ta>
            <ta e="T726" id="Seg_6365" s="T725">один.[NOM.SG]</ta>
            <ta e="T727" id="Seg_6366" s="T726">два.[NOM.SG]</ta>
            <ta e="T728" id="Seg_6367" s="T727">три.[NOM.SG]</ta>
            <ta e="T729" id="Seg_6368" s="T728">четыре.[NOM.SG]</ta>
            <ta e="T730" id="Seg_6369" s="T729">пять.[NOM.SG]</ta>
            <ta e="T731" id="Seg_6370" s="T730">семь.[NOM.SG]</ta>
            <ta e="T732" id="Seg_6371" s="T731">девять.[NOM.SG]</ta>
            <ta e="T733" id="Seg_6372" s="T732">восемь.[NOM.SG]</ta>
            <ta e="T734" id="Seg_6373" s="T733">десять.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T18" id="Seg_6374" s="T17">aux-v:mood.pn</ta>
            <ta e="T19" id="Seg_6375" s="T18">v-v&gt;v-v:n.fin</ta>
            <ta e="T23" id="Seg_6376" s="T22">pers</ta>
            <ta e="T24" id="Seg_6377" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_6378" s="T24">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_6379" s="T27">adv</ta>
            <ta e="T29" id="Seg_6380" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_6381" s="T29">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_6382" s="T30">pers</ta>
            <ta e="T742" id="Seg_6383" s="T31">v-v:n-fin</ta>
            <ta e="T32" id="Seg_6384" s="T742">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_6385" s="T36">quant</ta>
            <ta e="T38" id="Seg_6386" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_6387" s="T38">ptcl</ta>
            <ta e="T41" id="Seg_6388" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_6389" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_6390" s="T42">adj-n:case</ta>
            <ta e="T44" id="Seg_6391" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_6392" s="T44">adj-n:case</ta>
            <ta e="T46" id="Seg_6393" s="T45">ptcl</ta>
            <ta e="T54" id="Seg_6394" s="T52">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_6395" s="T56">n</ta>
            <ta e="T58" id="Seg_6396" s="T57">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_6397" s="T59">v-v:mood.pn</ta>
            <ta e="T61" id="Seg_6398" s="T60">ptcl</ta>
            <ta e="T72" id="Seg_6399" s="T71">adv</ta>
            <ta e="T73" id="Seg_6400" s="T72">pers</ta>
            <ta e="T74" id="Seg_6401" s="T73">adj-n:case</ta>
            <ta e="T75" id="Seg_6402" s="T74">v-v:ins-v:mood.pn</ta>
            <ta e="T76" id="Seg_6403" s="T75">n-n:case</ta>
            <ta e="T78" id="Seg_6404" s="T77">adv</ta>
            <ta e="T79" id="Seg_6405" s="T78">adj-n:case</ta>
            <ta e="T80" id="Seg_6406" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_6407" s="T80">conj</ta>
            <ta e="T82" id="Seg_6408" s="T81">adj-n:case</ta>
            <ta e="T83" id="Seg_6409" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_6410" s="T83">n</ta>
            <ta e="T85" id="Seg_6411" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_6412" s="T85">v-v:mood.pn</ta>
            <ta e="T88" id="Seg_6413" s="T87">v-v:mood.pn</ta>
            <ta e="T91" id="Seg_6414" s="T89">conj</ta>
            <ta e="T108" id="Seg_6415" s="T107">n-n:num</ta>
            <ta e="T109" id="Seg_6416" s="T108">n-n:case.poss</ta>
            <ta e="T112" id="Seg_6417" s="T111">n-n:num</ta>
            <ta e="T113" id="Seg_6418" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_6419" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_6420" s="T114">v-v:ins-v:mood.pn</ta>
            <ta e="T116" id="Seg_6421" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_6422" s="T116">v-v:n.fin</ta>
            <ta e="T118" id="Seg_6423" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6424" s="T118">que</ta>
            <ta e="T120" id="Seg_6425" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_6426" s="T120">v-v:mood.pn</ta>
            <ta e="T122" id="Seg_6427" s="T121">adv</ta>
            <ta e="T123" id="Seg_6428" s="T122">adj-n:case</ta>
            <ta e="T124" id="Seg_6429" s="T123">n-n:case</ta>
            <ta e="T129" id="Seg_6430" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_6431" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_6432" s="T130">v-v:n.fin</ta>
            <ta e="T132" id="Seg_6433" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_6434" s="T132">v-v:n.fin</ta>
            <ta e="T134" id="Seg_6435" s="T133">adv</ta>
            <ta e="T135" id="Seg_6436" s="T134">adv</ta>
            <ta e="T136" id="Seg_6437" s="T135">v-v:n.fin</ta>
            <ta e="T137" id="Seg_6438" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_6439" s="T137">v-v:n.fin</ta>
            <ta e="T139" id="Seg_6440" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_6441" s="T139">v-v:n.fin</ta>
            <ta e="T142" id="Seg_6442" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_6443" s="T142">v-v:n.fin</ta>
            <ta e="T144" id="Seg_6444" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_6445" s="T144">v-v:n.fin</ta>
            <ta e="T148" id="Seg_6446" s="T146">v-v:n.fin</ta>
            <ta e="T149" id="Seg_6447" s="T148">n</ta>
            <ta e="T150" id="Seg_6448" s="T149">v-v:n.fin</ta>
            <ta e="T151" id="Seg_6449" s="T150">ptcl</ta>
            <ta e="T166" id="Seg_6450" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_6451" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_6452" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6453" s="T168">v-v:n.fin</ta>
            <ta e="T171" id="Seg_6454" s="T169">v-v&gt;v-v:pn</ta>
            <ta e="T172" id="Seg_6455" s="T171">aux-v:mood.pn</ta>
            <ta e="T173" id="Seg_6456" s="T172">v-v:ins-v:n.fin</ta>
            <ta e="T174" id="Seg_6457" s="T173">aux-v:mood.pn</ta>
            <ta e="T175" id="Seg_6458" s="T174">v-v:ins-v:n.fin</ta>
            <ta e="T183" id="Seg_6459" s="T182">pers</ta>
            <ta e="T184" id="Seg_6460" s="T183">adv</ta>
            <ta e="T185" id="Seg_6461" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_6462" s="T185">adv</ta>
            <ta e="T187" id="Seg_6463" s="T186">ptcl</ta>
            <ta e="T189" id="Seg_6464" s="T188">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_6465" s="T189">pers</ta>
            <ta e="T743" id="Seg_6466" s="T190">v-v:n-fin</ta>
            <ta e="T191" id="Seg_6467" s="T743">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_6468" s="T193">n-n:case.poss</ta>
            <ta e="T195" id="Seg_6469" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_6470" s="T195">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_6471" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_6472" s="T201">v-v:pn</ta>
            <ta e="T203" id="Seg_6473" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_6474" s="T203">v-v:n.fin</ta>
            <ta e="T206" id="Seg_6475" s="T204">que-n:case</ta>
            <ta e="T207" id="Seg_6476" s="T206">v-v:n.fin</ta>
            <ta e="T208" id="Seg_6477" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_6478" s="T208">v-v:pn</ta>
            <ta e="T210" id="Seg_6479" s="T209">v-v:mood.pn</ta>
            <ta e="T211" id="Seg_6480" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_6481" s="T211">dempro-n:case</ta>
            <ta e="T213" id="Seg_6482" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_6483" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_6484" s="T214">pers</ta>
            <ta e="T216" id="Seg_6485" s="T215">adv</ta>
            <ta e="T217" id="Seg_6486" s="T216">v-v:ins-v:mood.pn</ta>
            <ta e="T222" id="Seg_6487" s="T221">v-v:mood-v:pn</ta>
            <ta e="T224" id="Seg_6488" s="T223">adv</ta>
            <ta e="T225" id="Seg_6489" s="T224">adv</ta>
            <ta e="T226" id="Seg_6490" s="T225">adj-n:case</ta>
            <ta e="T227" id="Seg_6491" s="T226">n-n:num</ta>
            <ta e="T228" id="Seg_6492" s="T227">adv</ta>
            <ta e="T229" id="Seg_6493" s="T228">adj-n:case</ta>
            <ta e="T230" id="Seg_6494" s="T229">n-n:num</ta>
            <ta e="T231" id="Seg_6495" s="T230">adv</ta>
            <ta e="T232" id="Seg_6496" s="T231">adj-n:case</ta>
            <ta e="T233" id="Seg_6497" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_6498" s="T233">adv</ta>
            <ta e="T235" id="Seg_6499" s="T234">adj-n:case</ta>
            <ta e="T236" id="Seg_6500" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_6501" s="T236">adv</ta>
            <ta e="T238" id="Seg_6502" s="T237">v-v&gt;v-v:pn</ta>
            <ta e="T239" id="Seg_6503" s="T238">n-n:case</ta>
            <ta e="T242" id="Seg_6504" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_6505" s="T242">v-v:n.fin</ta>
            <ta e="T245" id="Seg_6506" s="T244">adv</ta>
            <ta e="T248" id="Seg_6507" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6508" s="T248">adv</ta>
            <ta e="T250" id="Seg_6509" s="T249">n-n:case.poss</ta>
            <ta e="T251" id="Seg_6510" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_6511" s="T251">v-v:ins-v:mood.pn</ta>
            <ta e="T253" id="Seg_6512" s="T252">v-v:mood.pn</ta>
            <ta e="T254" id="Seg_6513" s="T253">aux-v:mood.pn</ta>
            <ta e="T255" id="Seg_6514" s="T254">v-v:ins-v:n.fin</ta>
            <ta e="T256" id="Seg_6515" s="T255">aux-v:mood.pn</ta>
            <ta e="T257" id="Seg_6516" s="T256">v-v:ins-v:n.fin</ta>
            <ta e="T259" id="Seg_6517" s="T257">ptcl</ta>
            <ta e="T260" id="Seg_6518" s="T259">v-v:n.fin</ta>
            <ta e="T261" id="Seg_6519" s="T260">ptcl</ta>
            <ta e="T269" id="Seg_6520" s="T741">n-n:case</ta>
            <ta e="T270" id="Seg_6521" s="T269">v-v:n.fin</ta>
            <ta e="T271" id="Seg_6522" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_6523" s="T271">v-v:n.fin</ta>
            <ta e="T273" id="Seg_6524" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_6525" s="T273">v-v:n.fin</ta>
            <ta e="T275" id="Seg_6526" s="T274">adv</ta>
            <ta e="T276" id="Seg_6527" s="T275">v-v:mood-v:pn</ta>
            <ta e="T277" id="Seg_6528" s="T276">num-num&gt;num</ta>
            <ta e="T280" id="Seg_6529" s="T279">que=ptcl</ta>
            <ta e="T281" id="Seg_6530" s="T280">v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_6531" s="T281">dempro-n:case</ta>
            <ta e="T283" id="Seg_6532" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_6533" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_6534" s="T284">ptcl</ta>
            <ta e="T287" id="Seg_6535" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_6536" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_6537" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_6538" s="T289">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_6539" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_6540" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_6541" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_6542" s="T293">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_6543" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_6544" s="T299">adv</ta>
            <ta e="T301" id="Seg_6545" s="T300">n-n:case.poss</ta>
            <ta e="T302" id="Seg_6546" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_6547" s="T302">n-n:case.poss</ta>
            <ta e="T304" id="Seg_6548" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_6549" s="T304">n-n:case.poss</ta>
            <ta e="T306" id="Seg_6550" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_6551" s="T306">n-n:num-n:case.poss</ta>
            <ta e="T308" id="Seg_6552" s="T307">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_6553" s="T320">v-v:n.fin</ta>
            <ta e="T322" id="Seg_6554" s="T321">v-v:n.fin</ta>
            <ta e="T323" id="Seg_6555" s="T322">v-v:n.fin</ta>
            <ta e="T324" id="Seg_6556" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_6557" s="T324">v-v:n.fin</ta>
            <ta e="T327" id="Seg_6558" s="T325">ptcl</ta>
            <ta e="T341" id="Seg_6559" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_6560" s="T341">conj</ta>
            <ta e="T343" id="Seg_6561" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_6562" s="T343">dempro-n:num</ta>
            <ta e="T346" id="Seg_6563" s="T345">n-n:num-n:case</ta>
            <ta e="T347" id="Seg_6564" s="T346">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_6565" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_6566" s="T348">conj</ta>
            <ta e="T350" id="Seg_6567" s="T349">n-n:case</ta>
            <ta e="T356" id="Seg_6568" s="T355">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_6569" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_6570" s="T357">conj</ta>
            <ta e="T359" id="Seg_6571" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_6572" s="T359">dempro-n:num-n:case</ta>
            <ta e="T361" id="Seg_6573" s="T360">n-n:num-n:case</ta>
            <ta e="T362" id="Seg_6574" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_6575" s="T362">n-n:case.poss</ta>
            <ta e="T364" id="Seg_6576" s="T363">conj</ta>
            <ta e="T365" id="Seg_6577" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_6578" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_6579" s="T366">conj</ta>
            <ta e="T368" id="Seg_6580" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_6581" s="T368">num-n:case</ta>
            <ta e="T370" id="Seg_6582" s="T369">num-n:case</ta>
            <ta e="T371" id="Seg_6583" s="T370">num-n:case</ta>
            <ta e="T372" id="Seg_6584" s="T371">num-n:case</ta>
            <ta e="T373" id="Seg_6585" s="T372">num-n:case</ta>
            <ta e="T374" id="Seg_6586" s="T373">num-n:case</ta>
            <ta e="T375" id="Seg_6587" s="T374">num-n:case</ta>
            <ta e="T376" id="Seg_6588" s="T375">num-n:case</ta>
            <ta e="T377" id="Seg_6589" s="T376">num-n:case</ta>
            <ta e="T378" id="Seg_6590" s="T377">num-n:case</ta>
            <ta e="T379" id="Seg_6591" s="T378">num-n:case</ta>
            <ta e="T380" id="Seg_6592" s="T379">num-n:case</ta>
            <ta e="T381" id="Seg_6593" s="T380">num-n:case</ta>
            <ta e="T382" id="Seg_6594" s="T381">num-n:case</ta>
            <ta e="T383" id="Seg_6595" s="T382">num-n:case</ta>
            <ta e="T384" id="Seg_6596" s="T383">num-n:case</ta>
            <ta e="T385" id="Seg_6597" s="T384">num-n:case</ta>
            <ta e="T386" id="Seg_6598" s="T385">num-n:case</ta>
            <ta e="T387" id="Seg_6599" s="T386">num-n:case</ta>
            <ta e="T388" id="Seg_6600" s="T387">num-n:case</ta>
            <ta e="T389" id="Seg_6601" s="T388">num-n:case</ta>
            <ta e="T390" id="Seg_6602" s="T389">num-n:case</ta>
            <ta e="T391" id="Seg_6603" s="T390">num-n:case</ta>
            <ta e="T392" id="Seg_6604" s="T391">num-n:case</ta>
            <ta e="T393" id="Seg_6605" s="T392">num-n:case</ta>
            <ta e="T395" id="Seg_6606" s="T393">num-n:case</ta>
            <ta e="T419" id="Seg_6607" s="T418">num-n:case</ta>
            <ta e="T420" id="Seg_6608" s="T419">num-n:case</ta>
            <ta e="T421" id="Seg_6609" s="T420">num-n:case</ta>
            <ta e="T422" id="Seg_6610" s="T421">num-n:case</ta>
            <ta e="T423" id="Seg_6611" s="T422">num-n:case</ta>
            <ta e="T424" id="Seg_6612" s="T423">num-n:case</ta>
            <ta e="T425" id="Seg_6613" s="T424">num-n:case</ta>
            <ta e="T427" id="Seg_6614" s="T426">pers</ta>
            <ta e="T428" id="Seg_6615" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_6616" s="T428">v-v:n.fin</ta>
            <ta e="T430" id="Seg_6617" s="T429">conj</ta>
            <ta e="T431" id="Seg_6618" s="T430">que-n:case</ta>
            <ta e="T432" id="Seg_6619" s="T431">dempro-n:case</ta>
            <ta e="T433" id="Seg_6620" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_6621" s="T433">v-v:ins-v:mood.pn</ta>
            <ta e="T435" id="Seg_6622" s="T434">v-v:mood.pn</ta>
            <ta e="T439" id="Seg_6623" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_6624" s="T439">v-v&gt;v-v:pn</ta>
            <ta e="T441" id="Seg_6625" s="T440">interj</ta>
            <ta e="T442" id="Seg_6626" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_6627" s="T442">n-n:case.poss</ta>
            <ta e="T444" id="Seg_6628" s="T443">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_6629" s="T444">ptcl</ta>
            <ta e="T448" id="Seg_6630" s="T447">n-n:case.poss</ta>
            <ta e="T449" id="Seg_6631" s="T448">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_6632" s="T449">n-n:case.poss</ta>
            <ta e="T451" id="Seg_6633" s="T450">v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_6634" s="T451">n-n:case.poss</ta>
            <ta e="T453" id="Seg_6635" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_6636" s="T453">n-n:case.poss</ta>
            <ta e="T455" id="Seg_6637" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_6638" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_6639" s="T456">v-v:tense-v:pn</ta>
            <ta e="T458" id="Seg_6640" s="T457">n-n:case.poss</ta>
            <ta e="T459" id="Seg_6641" s="T458">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_6642" s="T459">n-n:case.poss</ta>
            <ta e="T461" id="Seg_6643" s="T460">v-v:tense-v:pn</ta>
            <ta e="T463" id="Seg_6644" s="T462">ptcl</ta>
            <ta e="T465" id="Seg_6645" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_6646" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_6647" s="T466">v-v:n.fin</ta>
            <ta e="T468" id="Seg_6648" s="T467">n-n:case</ta>
            <ta e="T469" id="Seg_6649" s="T468">v-v:n.fin</ta>
            <ta e="T470" id="Seg_6650" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_6651" s="T470">v-v:n.fin</ta>
            <ta e="T472" id="Seg_6652" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_6653" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_6654" s="T473">v-v:n.fin</ta>
            <ta e="T475" id="Seg_6655" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_6656" s="T475">n-n:case</ta>
            <ta e="T477" id="Seg_6657" s="T476">v-v:n.fin</ta>
            <ta e="T478" id="Seg_6658" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_6659" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_6660" s="T479">v-v:n.fin</ta>
            <ta e="T481" id="Seg_6661" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_6662" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_6663" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_6664" s="T483">v-v:n.fin</ta>
            <ta e="T485" id="Seg_6665" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_6666" s="T485">quant</ta>
            <ta e="T487" id="Seg_6667" s="T486">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_6668" s="T487">v-v:n.fin</ta>
            <ta e="T489" id="Seg_6669" s="T488">ptcl</ta>
            <ta e="T493" id="Seg_6670" s="T492">ptcl</ta>
            <ta e="T496" id="Seg_6671" s="T495">n-n:case</ta>
            <ta e="T498" id="Seg_6672" s="T497">v-v:ins-v:mood.pn</ta>
            <ta e="T499" id="Seg_6673" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_6674" s="T499">v-v&gt;v-v:pn</ta>
            <ta e="T501" id="Seg_6675" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_6676" s="T501">v-v:ins-v:mood.pn</ta>
            <ta e="T518" id="Seg_6677" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_6678" s="T518">v-v&gt;v-v:pn</ta>
            <ta e="T520" id="Seg_6679" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_6680" s="T520">v-v:ins-v:mood.pn</ta>
            <ta e="T523" id="Seg_6681" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_6682" s="T523">v-v&gt;v-v:pn</ta>
            <ta e="T526" id="Seg_6683" s="T525">n-n:case</ta>
            <ta e="T527" id="Seg_6684" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_6685" s="T527">v-v&gt;v.[v:pn]</ta>
            <ta e="T529" id="Seg_6686" s="T528">n-n:case</ta>
            <ta e="T531" id="Seg_6687" s="T530">v-v:mood.pn</ta>
            <ta e="T532" id="Seg_6688" s="T531">adv</ta>
            <ta e="T533" id="Seg_6689" s="T532">v-v:ins-v:mood.pn</ta>
            <ta e="T534" id="Seg_6690" s="T533">que</ta>
            <ta e="T535" id="Seg_6691" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_6692" s="T535">que-n:case</ta>
            <ta e="T537" id="Seg_6693" s="T536">que-n:case</ta>
            <ta e="T538" id="Seg_6694" s="T537">v-v:tense-v:pn</ta>
            <ta e="T539" id="Seg_6695" s="T538">que</ta>
            <ta e="T540" id="Seg_6696" s="T539">v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_6697" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_6698" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_6699" s="T542">n-n&gt;v-v:n.fin</ta>
            <ta e="T544" id="Seg_6700" s="T543">adv</ta>
            <ta e="T545" id="Seg_6701" s="T544">n-n:case</ta>
            <ta e="T546" id="Seg_6702" s="T545">adv</ta>
            <ta e="T547" id="Seg_6703" s="T546">v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_6704" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_6705" s="T548">aux-v:mood.pn</ta>
            <ta e="T550" id="Seg_6706" s="T549">v-v:ins-v:n.fin</ta>
            <ta e="T551" id="Seg_6707" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_6708" s="T551">ptcl</ta>
            <ta e="T555" id="Seg_6709" s="T554">num-n:case</ta>
            <ta e="T556" id="Seg_6710" s="T555">num-n:case</ta>
            <ta e="T557" id="Seg_6711" s="T556">num-n:case</ta>
            <ta e="T558" id="Seg_6712" s="T557">num-n:case</ta>
            <ta e="T559" id="Seg_6713" s="T558">num-n:case</ta>
            <ta e="T560" id="Seg_6714" s="T559">num-n:case</ta>
            <ta e="T562" id="Seg_6715" s="T561">num-n:case</ta>
            <ta e="T563" id="Seg_6716" s="T562">num-n:case</ta>
            <ta e="T564" id="Seg_6717" s="T563">num-n:case</ta>
            <ta e="T565" id="Seg_6718" s="T564">num-n:case</ta>
            <ta e="T566" id="Seg_6719" s="T565">num-n:case</ta>
            <ta e="T567" id="Seg_6720" s="T566">num-n:case</ta>
            <ta e="T568" id="Seg_6721" s="T567">num-n:case</ta>
            <ta e="T569" id="Seg_6722" s="T568">num-n:case</ta>
            <ta e="T570" id="Seg_6723" s="T569">num-n:case</ta>
            <ta e="T571" id="Seg_6724" s="T570">num-n:case</ta>
            <ta e="T574" id="Seg_6725" s="T573">num-n:case</ta>
            <ta e="T575" id="Seg_6726" s="T574">num-n:case</ta>
            <ta e="T576" id="Seg_6727" s="T575">num-n:case</ta>
            <ta e="T577" id="Seg_6728" s="T576">num-n:case</ta>
            <ta e="T578" id="Seg_6729" s="T577">num-n:case</ta>
            <ta e="T579" id="Seg_6730" s="T578">num-n:case</ta>
            <ta e="T580" id="Seg_6731" s="T579">num-n:case</ta>
            <ta e="T581" id="Seg_6732" s="T580">num-n:case</ta>
            <ta e="T582" id="Seg_6733" s="T581">num-n:case</ta>
            <ta e="T583" id="Seg_6734" s="T582">num-n:case</ta>
            <ta e="T584" id="Seg_6735" s="T583">num-n:case</ta>
            <ta e="T585" id="Seg_6736" s="T584">num-n:case</ta>
            <ta e="T586" id="Seg_6737" s="T585">num-n:case</ta>
            <ta e="T587" id="Seg_6738" s="T586">num-n:case</ta>
            <ta e="T588" id="Seg_6739" s="T587">num-n:case</ta>
            <ta e="T589" id="Seg_6740" s="T588">num-n:case</ta>
            <ta e="T590" id="Seg_6741" s="T589">num-n:case</ta>
            <ta e="T591" id="Seg_6742" s="T590">num-n:case</ta>
            <ta e="T592" id="Seg_6743" s="T591">num-n:case</ta>
            <ta e="T596" id="Seg_6744" s="T595">v-v:n.fin</ta>
            <ta e="T597" id="Seg_6745" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_6746" s="T597">n-n:ins-n:case</ta>
            <ta e="T599" id="Seg_6747" s="T598">n-n:case.poss</ta>
            <ta e="T600" id="Seg_6748" s="T599">v-v:n.fin</ta>
            <ta e="T601" id="Seg_6749" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_6750" s="T601">v-v:n.fin</ta>
            <ta e="T603" id="Seg_6751" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_6752" s="T603">n-n:case</ta>
            <ta e="T605" id="Seg_6753" s="T604">v-v:n.fin</ta>
            <ta e="T606" id="Seg_6754" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_6755" s="T606">v-v:n.fin</ta>
            <ta e="T608" id="Seg_6756" s="T607">v-v:n.fin</ta>
            <ta e="T615" id="Seg_6757" s="T614">que</ta>
            <ta e="T616" id="Seg_6758" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_6759" s="T616">que</ta>
            <ta e="T618" id="Seg_6760" s="T617">v-v&gt;v-v:pn</ta>
            <ta e="T619" id="Seg_6761" s="T618">n-n:case.poss</ta>
            <ta e="T620" id="Seg_6762" s="T619">v-v:pn</ta>
            <ta e="T622" id="Seg_6763" s="T621">v-v&gt;v-v:n.fin</ta>
            <ta e="T624" id="Seg_6764" s="T623">v-v:tense-v:pn</ta>
            <ta e="T625" id="Seg_6765" s="T624">v-v:mood-v:pn</ta>
            <ta e="T626" id="Seg_6766" s="T625">pers-n:case</ta>
            <ta e="T627" id="Seg_6767" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_6768" s="T627">pers</ta>
            <ta e="T629" id="Seg_6769" s="T628">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_6770" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_6771" s="T635">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_6772" s="T636">v-v:mood.pn</ta>
            <ta e="T638" id="Seg_6773" s="T637">v-v:n.fin</ta>
            <ta e="T639" id="Seg_6774" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_6775" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_6776" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_6777" s="T641">n-n:case</ta>
            <ta e="T643" id="Seg_6778" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_6779" s="T643">v-v:ins-v:mood.pn</ta>
            <ta e="T645" id="Seg_6780" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_6781" s="T645">v-v:ins-v:mood.pn</ta>
            <ta e="T647" id="Seg_6782" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_6783" s="T647">v-v:ins-v:mood.pn</ta>
            <ta e="T650" id="Seg_6784" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_6785" s="T650">v-v:ins-v:mood.pn</ta>
            <ta e="T652" id="Seg_6786" s="T651">adj-n:case</ta>
            <ta e="T653" id="Seg_6787" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_6788" s="T653">v-v:ins-v:mood.pn</ta>
            <ta e="T655" id="Seg_6789" s="T654">n-n:case</ta>
            <ta e="T656" id="Seg_6790" s="T655">v-v:mood.pn</ta>
            <ta e="T657" id="Seg_6791" s="T656">adj-n:case</ta>
            <ta e="T658" id="Seg_6792" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_6793" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_6794" s="T659">v-v:mood.pn</ta>
            <ta e="T662" id="Seg_6795" s="T661">v-v:mood-v:pn</ta>
            <ta e="T663" id="Seg_6796" s="T662">adv</ta>
            <ta e="T664" id="Seg_6797" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_6798" s="T664">n-n:case</ta>
            <ta e="T666" id="Seg_6799" s="T665">v-v:n.fin</ta>
            <ta e="T667" id="Seg_6800" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_6801" s="T667">v-v:n.fin</ta>
            <ta e="T669" id="Seg_6802" s="T668">n-n:case</ta>
            <ta e="T670" id="Seg_6803" s="T669">v-v:n.fin</ta>
            <ta e="T671" id="Seg_6804" s="T670">adv</ta>
            <ta e="T672" id="Seg_6805" s="T671">que-n:case</ta>
            <ta e="T673" id="Seg_6806" s="T672">n-n:case</ta>
            <ta e="T677" id="Seg_6807" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_6808" s="T677">v-v:n.fin</ta>
            <ta e="T683" id="Seg_6809" s="T682">pers</ta>
            <ta e="T684" id="Seg_6810" s="T683">pers</ta>
            <ta e="T685" id="Seg_6811" s="T684">v-v&gt;v-v:pn</ta>
            <ta e="T689" id="Seg_6812" s="T688">v-v:ins-v:mood.pn</ta>
            <ta e="T690" id="Seg_6813" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_6814" s="T690">v-v:ins-v:mood.pn</ta>
            <ta e="T692" id="Seg_6815" s="T691">v-v:mood-v:pn</ta>
            <ta e="T693" id="Seg_6816" s="T692">que=ptcl</ta>
            <ta e="T694" id="Seg_6817" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_6818" s="T694">n-n:case</ta>
            <ta e="T697" id="Seg_6819" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_6820" s="T697">n-n:num</ta>
            <ta e="T699" id="Seg_6821" s="T698">v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_6822" s="T699">n-n:case</ta>
            <ta e="T701" id="Seg_6823" s="T700">v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_6824" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_6825" s="T702">v-v:tense-v:pn</ta>
            <ta e="T709" id="Seg_6826" s="T708">n-n:case.poss</ta>
            <ta e="T710" id="Seg_6827" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_6828" s="T710">ptcl</ta>
            <ta e="T713" id="Seg_6829" s="T712">v-v&gt;v-v:n.fin</ta>
            <ta e="T714" id="Seg_6830" s="T713">v-v:n.fin</ta>
            <ta e="T716" id="Seg_6831" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_6832" s="T716">v-v:n.fin</ta>
            <ta e="T718" id="Seg_6833" s="T717">v-v:n.fin</ta>
            <ta e="T719" id="Seg_6834" s="T718">v-v:n.fin</ta>
            <ta e="T720" id="Seg_6835" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_6836" s="T720">v-v:n.fin</ta>
            <ta e="T722" id="Seg_6837" s="T721">dempro-n:num-n:case</ta>
            <ta e="T725" id="Seg_6838" s="T724">num-n:case</ta>
            <ta e="T726" id="Seg_6839" s="T725">num-n:case</ta>
            <ta e="T727" id="Seg_6840" s="T726">num-n:case</ta>
            <ta e="T728" id="Seg_6841" s="T727">num-n:case</ta>
            <ta e="T729" id="Seg_6842" s="T728">num-n:case</ta>
            <ta e="T730" id="Seg_6843" s="T729">num-n:case</ta>
            <ta e="T731" id="Seg_6844" s="T730">num-n:case</ta>
            <ta e="T732" id="Seg_6845" s="T731">num-n:case</ta>
            <ta e="T733" id="Seg_6846" s="T732">num-n:case</ta>
            <ta e="T734" id="Seg_6847" s="T733">num-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T18" id="Seg_6848" s="T17">aux</ta>
            <ta e="T19" id="Seg_6849" s="T18">v</ta>
            <ta e="T23" id="Seg_6850" s="T22">pers</ta>
            <ta e="T24" id="Seg_6851" s="T23">dempro</ta>
            <ta e="T25" id="Seg_6852" s="T24">v</ta>
            <ta e="T28" id="Seg_6853" s="T27">adv</ta>
            <ta e="T29" id="Seg_6854" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_6855" s="T29">v</ta>
            <ta e="T31" id="Seg_6856" s="T30">pers</ta>
            <ta e="T742" id="Seg_6857" s="T31">v</ta>
            <ta e="T32" id="Seg_6858" s="T742">v</ta>
            <ta e="T37" id="Seg_6859" s="T36">quant</ta>
            <ta e="T38" id="Seg_6860" s="T37">v</ta>
            <ta e="T39" id="Seg_6861" s="T38">ptcl</ta>
            <ta e="T41" id="Seg_6862" s="T40">n</ta>
            <ta e="T42" id="Seg_6863" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_6864" s="T42">adj</ta>
            <ta e="T44" id="Seg_6865" s="T43">v</ta>
            <ta e="T45" id="Seg_6866" s="T44">adj</ta>
            <ta e="T46" id="Seg_6867" s="T45">ptcl</ta>
            <ta e="T54" id="Seg_6868" s="T52">v</ta>
            <ta e="T57" id="Seg_6869" s="T56">n</ta>
            <ta e="T58" id="Seg_6870" s="T57">v</ta>
            <ta e="T60" id="Seg_6871" s="T59">v</ta>
            <ta e="T61" id="Seg_6872" s="T60">ptcl</ta>
            <ta e="T72" id="Seg_6873" s="T71">adv</ta>
            <ta e="T73" id="Seg_6874" s="T72">pers</ta>
            <ta e="T74" id="Seg_6875" s="T73">adj</ta>
            <ta e="T75" id="Seg_6876" s="T74">v</ta>
            <ta e="T76" id="Seg_6877" s="T75">n</ta>
            <ta e="T78" id="Seg_6878" s="T77">adv</ta>
            <ta e="T79" id="Seg_6879" s="T78">adj</ta>
            <ta e="T80" id="Seg_6880" s="T79">v</ta>
            <ta e="T81" id="Seg_6881" s="T80">conj</ta>
            <ta e="T82" id="Seg_6882" s="T81">adj</ta>
            <ta e="T83" id="Seg_6883" s="T82">n</ta>
            <ta e="T84" id="Seg_6884" s="T83">n</ta>
            <ta e="T85" id="Seg_6885" s="T84">v</ta>
            <ta e="T86" id="Seg_6886" s="T85">v</ta>
            <ta e="T88" id="Seg_6887" s="T87">v</ta>
            <ta e="T91" id="Seg_6888" s="T89">conj</ta>
            <ta e="T108" id="Seg_6889" s="T107">n</ta>
            <ta e="T109" id="Seg_6890" s="T108">n</ta>
            <ta e="T112" id="Seg_6891" s="T111">n</ta>
            <ta e="T113" id="Seg_6892" s="T112">v</ta>
            <ta e="T114" id="Seg_6893" s="T113">v</ta>
            <ta e="T115" id="Seg_6894" s="T114">v</ta>
            <ta e="T116" id="Seg_6895" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_6896" s="T116">v</ta>
            <ta e="T118" id="Seg_6897" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6898" s="T118">que</ta>
            <ta e="T120" id="Seg_6899" s="T119">v</ta>
            <ta e="T121" id="Seg_6900" s="T120">v</ta>
            <ta e="T122" id="Seg_6901" s="T121">adv</ta>
            <ta e="T123" id="Seg_6902" s="T122">adj</ta>
            <ta e="T124" id="Seg_6903" s="T123">n</ta>
            <ta e="T129" id="Seg_6904" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_6905" s="T129">n</ta>
            <ta e="T131" id="Seg_6906" s="T130">v</ta>
            <ta e="T132" id="Seg_6907" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_6908" s="T132">v</ta>
            <ta e="T134" id="Seg_6909" s="T133">adv</ta>
            <ta e="T135" id="Seg_6910" s="T134">adv</ta>
            <ta e="T136" id="Seg_6911" s="T135">v</ta>
            <ta e="T137" id="Seg_6912" s="T136">n</ta>
            <ta e="T138" id="Seg_6913" s="T137">v</ta>
            <ta e="T139" id="Seg_6914" s="T138">n</ta>
            <ta e="T140" id="Seg_6915" s="T139">v</ta>
            <ta e="T142" id="Seg_6916" s="T141">n</ta>
            <ta e="T143" id="Seg_6917" s="T142">v</ta>
            <ta e="T144" id="Seg_6918" s="T143">n</ta>
            <ta e="T145" id="Seg_6919" s="T144">v</ta>
            <ta e="T148" id="Seg_6920" s="T146">v</ta>
            <ta e="T149" id="Seg_6921" s="T148">n</ta>
            <ta e="T150" id="Seg_6922" s="T149">v</ta>
            <ta e="T151" id="Seg_6923" s="T150">ptcl</ta>
            <ta e="T166" id="Seg_6924" s="T165">adj</ta>
            <ta e="T167" id="Seg_6925" s="T166">n</ta>
            <ta e="T168" id="Seg_6926" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6927" s="T168">v</ta>
            <ta e="T171" id="Seg_6928" s="T169">v</ta>
            <ta e="T172" id="Seg_6929" s="T171">aux</ta>
            <ta e="T173" id="Seg_6930" s="T172">v</ta>
            <ta e="T174" id="Seg_6931" s="T173">aux</ta>
            <ta e="T175" id="Seg_6932" s="T174">v</ta>
            <ta e="T183" id="Seg_6933" s="T182">pers</ta>
            <ta e="T184" id="Seg_6934" s="T183">adv</ta>
            <ta e="T185" id="Seg_6935" s="T184">v</ta>
            <ta e="T186" id="Seg_6936" s="T185">adv</ta>
            <ta e="T187" id="Seg_6937" s="T186">ptcl</ta>
            <ta e="T189" id="Seg_6938" s="T188">v</ta>
            <ta e="T190" id="Seg_6939" s="T189">pers</ta>
            <ta e="T743" id="Seg_6940" s="T190">v</ta>
            <ta e="T191" id="Seg_6941" s="T743">v</ta>
            <ta e="T194" id="Seg_6942" s="T193">n</ta>
            <ta e="T195" id="Seg_6943" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_6944" s="T195">v</ta>
            <ta e="T201" id="Seg_6945" s="T200">n</ta>
            <ta e="T202" id="Seg_6946" s="T201">v</ta>
            <ta e="T203" id="Seg_6947" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_6948" s="T203">v</ta>
            <ta e="T206" id="Seg_6949" s="T204">que</ta>
            <ta e="T207" id="Seg_6950" s="T206">v</ta>
            <ta e="T208" id="Seg_6951" s="T207">n</ta>
            <ta e="T209" id="Seg_6952" s="T208">v</ta>
            <ta e="T210" id="Seg_6953" s="T209">v</ta>
            <ta e="T211" id="Seg_6954" s="T210">n</ta>
            <ta e="T212" id="Seg_6955" s="T211">dempro</ta>
            <ta e="T213" id="Seg_6956" s="T212">n</ta>
            <ta e="T214" id="Seg_6957" s="T213">v</ta>
            <ta e="T215" id="Seg_6958" s="T214">pers</ta>
            <ta e="T216" id="Seg_6959" s="T215">adv</ta>
            <ta e="T217" id="Seg_6960" s="T216">v</ta>
            <ta e="T222" id="Seg_6961" s="T221">v</ta>
            <ta e="T224" id="Seg_6962" s="T223">adv</ta>
            <ta e="T225" id="Seg_6963" s="T224">adv</ta>
            <ta e="T226" id="Seg_6964" s="T225">adj</ta>
            <ta e="T227" id="Seg_6965" s="T226">n</ta>
            <ta e="T228" id="Seg_6966" s="T227">adv</ta>
            <ta e="T229" id="Seg_6967" s="T228">adj</ta>
            <ta e="T230" id="Seg_6968" s="T229">n</ta>
            <ta e="T231" id="Seg_6969" s="T230">adv</ta>
            <ta e="T232" id="Seg_6970" s="T231">adj</ta>
            <ta e="T233" id="Seg_6971" s="T232">n</ta>
            <ta e="T234" id="Seg_6972" s="T233">adv</ta>
            <ta e="T235" id="Seg_6973" s="T234">adj</ta>
            <ta e="T236" id="Seg_6974" s="T235">n</ta>
            <ta e="T237" id="Seg_6975" s="T236">adv</ta>
            <ta e="T238" id="Seg_6976" s="T237">v</ta>
            <ta e="T239" id="Seg_6977" s="T238">n</ta>
            <ta e="T242" id="Seg_6978" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_6979" s="T242">v</ta>
            <ta e="T245" id="Seg_6980" s="T244">adj</ta>
            <ta e="T248" id="Seg_6981" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6982" s="T248">adv</ta>
            <ta e="T250" id="Seg_6983" s="T249">n</ta>
            <ta e="T251" id="Seg_6984" s="T250">v</ta>
            <ta e="T252" id="Seg_6985" s="T251">v</ta>
            <ta e="T253" id="Seg_6986" s="T252">v</ta>
            <ta e="T254" id="Seg_6987" s="T253">aux</ta>
            <ta e="T255" id="Seg_6988" s="T254">v</ta>
            <ta e="T256" id="Seg_6989" s="T255">aux</ta>
            <ta e="T257" id="Seg_6990" s="T256">v</ta>
            <ta e="T259" id="Seg_6991" s="T257">ptcl</ta>
            <ta e="T260" id="Seg_6992" s="T259">v</ta>
            <ta e="T261" id="Seg_6993" s="T260">ptcl</ta>
            <ta e="T269" id="Seg_6994" s="T741">n</ta>
            <ta e="T270" id="Seg_6995" s="T269">v</ta>
            <ta e="T271" id="Seg_6996" s="T270">n</ta>
            <ta e="T272" id="Seg_6997" s="T271">v</ta>
            <ta e="T273" id="Seg_6998" s="T272">n</ta>
            <ta e="T274" id="Seg_6999" s="T273">v</ta>
            <ta e="T275" id="Seg_7000" s="T274">adv</ta>
            <ta e="T276" id="Seg_7001" s="T275">v</ta>
            <ta e="T277" id="Seg_7002" s="T276">num</ta>
            <ta e="T280" id="Seg_7003" s="T279">que</ta>
            <ta e="T281" id="Seg_7004" s="T280">v</ta>
            <ta e="T282" id="Seg_7005" s="T281">dempro</ta>
            <ta e="T283" id="Seg_7006" s="T282">n</ta>
            <ta e="T284" id="Seg_7007" s="T283">v</ta>
            <ta e="T285" id="Seg_7008" s="T284">ptcl</ta>
            <ta e="T287" id="Seg_7009" s="T286">n</ta>
            <ta e="T288" id="Seg_7010" s="T287">v</ta>
            <ta e="T289" id="Seg_7011" s="T288">n</ta>
            <ta e="T290" id="Seg_7012" s="T289">v</ta>
            <ta e="T291" id="Seg_7013" s="T290">n</ta>
            <ta e="T292" id="Seg_7014" s="T291">v</ta>
            <ta e="T293" id="Seg_7015" s="T292">n</ta>
            <ta e="T294" id="Seg_7016" s="T293">v</ta>
            <ta e="T299" id="Seg_7017" s="T298">v</ta>
            <ta e="T300" id="Seg_7018" s="T299">adv</ta>
            <ta e="T301" id="Seg_7019" s="T300">n</ta>
            <ta e="T302" id="Seg_7020" s="T301">v</ta>
            <ta e="T303" id="Seg_7021" s="T302">n</ta>
            <ta e="T304" id="Seg_7022" s="T303">v</ta>
            <ta e="T305" id="Seg_7023" s="T304">n</ta>
            <ta e="T306" id="Seg_7024" s="T305">v</ta>
            <ta e="T307" id="Seg_7025" s="T306">n</ta>
            <ta e="T308" id="Seg_7026" s="T307">v</ta>
            <ta e="T321" id="Seg_7027" s="T320">v</ta>
            <ta e="T322" id="Seg_7028" s="T321">v</ta>
            <ta e="T323" id="Seg_7029" s="T322">v</ta>
            <ta e="T324" id="Seg_7030" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_7031" s="T324">v</ta>
            <ta e="T327" id="Seg_7032" s="T325">ptcl</ta>
            <ta e="T341" id="Seg_7033" s="T340">n</ta>
            <ta e="T342" id="Seg_7034" s="T341">conj</ta>
            <ta e="T343" id="Seg_7035" s="T342">n</ta>
            <ta e="T344" id="Seg_7036" s="T343">dempro</ta>
            <ta e="T346" id="Seg_7037" s="T345">n</ta>
            <ta e="T347" id="Seg_7038" s="T346">v</ta>
            <ta e="T348" id="Seg_7039" s="T347">n</ta>
            <ta e="T349" id="Seg_7040" s="T348">conj</ta>
            <ta e="T350" id="Seg_7041" s="T349">n</ta>
            <ta e="T356" id="Seg_7042" s="T355">v</ta>
            <ta e="T357" id="Seg_7043" s="T356">n</ta>
            <ta e="T358" id="Seg_7044" s="T357">conj</ta>
            <ta e="T359" id="Seg_7045" s="T358">n</ta>
            <ta e="T360" id="Seg_7046" s="T359">dempro</ta>
            <ta e="T361" id="Seg_7047" s="T360">n</ta>
            <ta e="T362" id="Seg_7048" s="T361">v</ta>
            <ta e="T363" id="Seg_7049" s="T362">n</ta>
            <ta e="T364" id="Seg_7050" s="T363">conj</ta>
            <ta e="T365" id="Seg_7051" s="T364">n</ta>
            <ta e="T366" id="Seg_7052" s="T365">n</ta>
            <ta e="T367" id="Seg_7053" s="T366">conj</ta>
            <ta e="T368" id="Seg_7054" s="T367">n</ta>
            <ta e="T369" id="Seg_7055" s="T368">num</ta>
            <ta e="T370" id="Seg_7056" s="T369">num</ta>
            <ta e="T371" id="Seg_7057" s="T370">num</ta>
            <ta e="T372" id="Seg_7058" s="T371">num</ta>
            <ta e="T373" id="Seg_7059" s="T372">num</ta>
            <ta e="T374" id="Seg_7060" s="T373">num</ta>
            <ta e="T375" id="Seg_7061" s="T374">num</ta>
            <ta e="T376" id="Seg_7062" s="T375">num</ta>
            <ta e="T377" id="Seg_7063" s="T376">num</ta>
            <ta e="T378" id="Seg_7064" s="T377">num</ta>
            <ta e="T379" id="Seg_7065" s="T378">num</ta>
            <ta e="T380" id="Seg_7066" s="T379">num</ta>
            <ta e="T381" id="Seg_7067" s="T380">num</ta>
            <ta e="T382" id="Seg_7068" s="T381">num</ta>
            <ta e="T383" id="Seg_7069" s="T382">num</ta>
            <ta e="T384" id="Seg_7070" s="T383">num</ta>
            <ta e="T385" id="Seg_7071" s="T384">num</ta>
            <ta e="T386" id="Seg_7072" s="T385">num</ta>
            <ta e="T387" id="Seg_7073" s="T386">num</ta>
            <ta e="T388" id="Seg_7074" s="T387">num</ta>
            <ta e="T389" id="Seg_7075" s="T388">num</ta>
            <ta e="T390" id="Seg_7076" s="T389">num</ta>
            <ta e="T391" id="Seg_7077" s="T390">num</ta>
            <ta e="T392" id="Seg_7078" s="T391">num</ta>
            <ta e="T393" id="Seg_7079" s="T392">num</ta>
            <ta e="T395" id="Seg_7080" s="T393">num</ta>
            <ta e="T419" id="Seg_7081" s="T418">num</ta>
            <ta e="T420" id="Seg_7082" s="T419">num</ta>
            <ta e="T421" id="Seg_7083" s="T420">num</ta>
            <ta e="T422" id="Seg_7084" s="T421">num</ta>
            <ta e="T423" id="Seg_7085" s="T422">num</ta>
            <ta e="T424" id="Seg_7086" s="T423">num</ta>
            <ta e="T425" id="Seg_7087" s="T424">num</ta>
            <ta e="T427" id="Seg_7088" s="T426">pers</ta>
            <ta e="T428" id="Seg_7089" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_7090" s="T428">v</ta>
            <ta e="T430" id="Seg_7091" s="T429">conj</ta>
            <ta e="T431" id="Seg_7092" s="T430">que</ta>
            <ta e="T432" id="Seg_7093" s="T431">dempro</ta>
            <ta e="T433" id="Seg_7094" s="T432">n</ta>
            <ta e="T434" id="Seg_7095" s="T433">v</ta>
            <ta e="T435" id="Seg_7096" s="T434">v</ta>
            <ta e="T439" id="Seg_7097" s="T438">n</ta>
            <ta e="T440" id="Seg_7098" s="T439">v</ta>
            <ta e="T441" id="Seg_7099" s="T440">interj</ta>
            <ta e="T442" id="Seg_7100" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_7101" s="T442">n</ta>
            <ta e="T444" id="Seg_7102" s="T443">v</ta>
            <ta e="T446" id="Seg_7103" s="T444">ptcl</ta>
            <ta e="T448" id="Seg_7104" s="T447">n</ta>
            <ta e="T449" id="Seg_7105" s="T448">v</ta>
            <ta e="T450" id="Seg_7106" s="T449">n</ta>
            <ta e="T451" id="Seg_7107" s="T450">v</ta>
            <ta e="T452" id="Seg_7108" s="T451">n</ta>
            <ta e="T453" id="Seg_7109" s="T452">v</ta>
            <ta e="T454" id="Seg_7110" s="T453">n</ta>
            <ta e="T455" id="Seg_7111" s="T454">v</ta>
            <ta e="T456" id="Seg_7112" s="T455">n</ta>
            <ta e="T457" id="Seg_7113" s="T456">v</ta>
            <ta e="T458" id="Seg_7114" s="T457">n</ta>
            <ta e="T459" id="Seg_7115" s="T458">v</ta>
            <ta e="T460" id="Seg_7116" s="T459">n</ta>
            <ta e="T461" id="Seg_7117" s="T460">v</ta>
            <ta e="T463" id="Seg_7118" s="T462">ptcl</ta>
            <ta e="T465" id="Seg_7119" s="T464">n</ta>
            <ta e="T466" id="Seg_7120" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_7121" s="T466">v</ta>
            <ta e="T468" id="Seg_7122" s="T467">n</ta>
            <ta e="T469" id="Seg_7123" s="T468">v</ta>
            <ta e="T470" id="Seg_7124" s="T469">n</ta>
            <ta e="T471" id="Seg_7125" s="T470">v</ta>
            <ta e="T472" id="Seg_7126" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_7127" s="T472">n</ta>
            <ta e="T474" id="Seg_7128" s="T473">v</ta>
            <ta e="T475" id="Seg_7129" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_7130" s="T475">n</ta>
            <ta e="T477" id="Seg_7131" s="T476">v</ta>
            <ta e="T478" id="Seg_7132" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_7133" s="T478">n</ta>
            <ta e="T480" id="Seg_7134" s="T479">v</ta>
            <ta e="T481" id="Seg_7135" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_7136" s="T481">n</ta>
            <ta e="T483" id="Seg_7137" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_7138" s="T483">v</ta>
            <ta e="T485" id="Seg_7139" s="T484">n</ta>
            <ta e="T486" id="Seg_7140" s="T485">quant</ta>
            <ta e="T487" id="Seg_7141" s="T486">v</ta>
            <ta e="T488" id="Seg_7142" s="T487">v</ta>
            <ta e="T489" id="Seg_7143" s="T488">ptcl</ta>
            <ta e="T493" id="Seg_7144" s="T492">ptcl</ta>
            <ta e="T496" id="Seg_7145" s="T495">n</ta>
            <ta e="T498" id="Seg_7146" s="T497">v</ta>
            <ta e="T499" id="Seg_7147" s="T498">n</ta>
            <ta e="T500" id="Seg_7148" s="T499">v</ta>
            <ta e="T501" id="Seg_7149" s="T500">n</ta>
            <ta e="T502" id="Seg_7150" s="T501">v</ta>
            <ta e="T518" id="Seg_7151" s="T517">n</ta>
            <ta e="T519" id="Seg_7152" s="T518">v</ta>
            <ta e="T520" id="Seg_7153" s="T519">n</ta>
            <ta e="T521" id="Seg_7154" s="T520">v</ta>
            <ta e="T523" id="Seg_7155" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_7156" s="T523">v</ta>
            <ta e="T526" id="Seg_7157" s="T525">n</ta>
            <ta e="T527" id="Seg_7158" s="T526">n</ta>
            <ta e="T528" id="Seg_7159" s="T527">v</ta>
            <ta e="T529" id="Seg_7160" s="T528">n</ta>
            <ta e="T531" id="Seg_7161" s="T530">v</ta>
            <ta e="T532" id="Seg_7162" s="T531">adv</ta>
            <ta e="T533" id="Seg_7163" s="T532">v</ta>
            <ta e="T534" id="Seg_7164" s="T533">que</ta>
            <ta e="T535" id="Seg_7165" s="T534">v</ta>
            <ta e="T536" id="Seg_7166" s="T535">que</ta>
            <ta e="T537" id="Seg_7167" s="T536">que</ta>
            <ta e="T538" id="Seg_7168" s="T537">v</ta>
            <ta e="T539" id="Seg_7169" s="T538">que</ta>
            <ta e="T540" id="Seg_7170" s="T539">v</ta>
            <ta e="T541" id="Seg_7171" s="T540">n</ta>
            <ta e="T542" id="Seg_7172" s="T541">v</ta>
            <ta e="T543" id="Seg_7173" s="T542">v</ta>
            <ta e="T544" id="Seg_7174" s="T543">adv</ta>
            <ta e="T545" id="Seg_7175" s="T544">n</ta>
            <ta e="T546" id="Seg_7176" s="T545">adv</ta>
            <ta e="T547" id="Seg_7177" s="T546">v</ta>
            <ta e="T548" id="Seg_7178" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_7179" s="T548">aux</ta>
            <ta e="T550" id="Seg_7180" s="T549">v</ta>
            <ta e="T551" id="Seg_7181" s="T550">v</ta>
            <ta e="T552" id="Seg_7182" s="T551">ptcl</ta>
            <ta e="T555" id="Seg_7183" s="T554">num</ta>
            <ta e="T556" id="Seg_7184" s="T555">num</ta>
            <ta e="T557" id="Seg_7185" s="T556">num</ta>
            <ta e="T558" id="Seg_7186" s="T557">num</ta>
            <ta e="T559" id="Seg_7187" s="T558">num</ta>
            <ta e="T560" id="Seg_7188" s="T559">num</ta>
            <ta e="T562" id="Seg_7189" s="T561">num</ta>
            <ta e="T563" id="Seg_7190" s="T562">num</ta>
            <ta e="T564" id="Seg_7191" s="T563">num</ta>
            <ta e="T565" id="Seg_7192" s="T564">num</ta>
            <ta e="T566" id="Seg_7193" s="T565">num</ta>
            <ta e="T567" id="Seg_7194" s="T566">num</ta>
            <ta e="T568" id="Seg_7195" s="T567">num</ta>
            <ta e="T569" id="Seg_7196" s="T568">num</ta>
            <ta e="T570" id="Seg_7197" s="T569">num</ta>
            <ta e="T571" id="Seg_7198" s="T570">num</ta>
            <ta e="T574" id="Seg_7199" s="T573">num</ta>
            <ta e="T575" id="Seg_7200" s="T574">num</ta>
            <ta e="T576" id="Seg_7201" s="T575">num</ta>
            <ta e="T577" id="Seg_7202" s="T576">num</ta>
            <ta e="T578" id="Seg_7203" s="T577">num</ta>
            <ta e="T579" id="Seg_7204" s="T578">num</ta>
            <ta e="T580" id="Seg_7205" s="T579">num</ta>
            <ta e="T581" id="Seg_7206" s="T580">num</ta>
            <ta e="T582" id="Seg_7207" s="T581">num</ta>
            <ta e="T583" id="Seg_7208" s="T582">num</ta>
            <ta e="T584" id="Seg_7209" s="T583">num</ta>
            <ta e="T585" id="Seg_7210" s="T584">num</ta>
            <ta e="T586" id="Seg_7211" s="T585">num</ta>
            <ta e="T587" id="Seg_7212" s="T586">num</ta>
            <ta e="T588" id="Seg_7213" s="T587">num</ta>
            <ta e="T589" id="Seg_7214" s="T588">num</ta>
            <ta e="T590" id="Seg_7215" s="T589">num</ta>
            <ta e="T591" id="Seg_7216" s="T590">num</ta>
            <ta e="T592" id="Seg_7217" s="T591">num</ta>
            <ta e="T596" id="Seg_7218" s="T595">v</ta>
            <ta e="T597" id="Seg_7219" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_7220" s="T597">n</ta>
            <ta e="T599" id="Seg_7221" s="T598">n</ta>
            <ta e="T600" id="Seg_7222" s="T599">v</ta>
            <ta e="T601" id="Seg_7223" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_7224" s="T601">v</ta>
            <ta e="T603" id="Seg_7225" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_7226" s="T603">n</ta>
            <ta e="T605" id="Seg_7227" s="T604">v</ta>
            <ta e="T606" id="Seg_7228" s="T605">n</ta>
            <ta e="T607" id="Seg_7229" s="T606">v</ta>
            <ta e="T608" id="Seg_7230" s="T607">v</ta>
            <ta e="T615" id="Seg_7231" s="T614">que</ta>
            <ta e="T616" id="Seg_7232" s="T615">v</ta>
            <ta e="T617" id="Seg_7233" s="T616">que</ta>
            <ta e="T618" id="Seg_7234" s="T617">v</ta>
            <ta e="T619" id="Seg_7235" s="T618">n</ta>
            <ta e="T620" id="Seg_7236" s="T619">v</ta>
            <ta e="T622" id="Seg_7237" s="T621">v</ta>
            <ta e="T624" id="Seg_7238" s="T623">v</ta>
            <ta e="T625" id="Seg_7239" s="T624">v</ta>
            <ta e="T626" id="Seg_7240" s="T625">pers</ta>
            <ta e="T627" id="Seg_7241" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_7242" s="T627">pers</ta>
            <ta e="T629" id="Seg_7243" s="T628">v</ta>
            <ta e="T635" id="Seg_7244" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_7245" s="T635">v</ta>
            <ta e="T637" id="Seg_7246" s="T636">v</ta>
            <ta e="T638" id="Seg_7247" s="T637">v</ta>
            <ta e="T639" id="Seg_7248" s="T638">n</ta>
            <ta e="T640" id="Seg_7249" s="T639">n</ta>
            <ta e="T641" id="Seg_7250" s="T640">n</ta>
            <ta e="T642" id="Seg_7251" s="T641">n</ta>
            <ta e="T643" id="Seg_7252" s="T642">n</ta>
            <ta e="T644" id="Seg_7253" s="T643">v</ta>
            <ta e="T645" id="Seg_7254" s="T644">n</ta>
            <ta e="T646" id="Seg_7255" s="T645">v</ta>
            <ta e="T647" id="Seg_7256" s="T646">n</ta>
            <ta e="T648" id="Seg_7257" s="T647">v</ta>
            <ta e="T650" id="Seg_7258" s="T649">n</ta>
            <ta e="T651" id="Seg_7259" s="T650">v</ta>
            <ta e="T652" id="Seg_7260" s="T651">adj</ta>
            <ta e="T653" id="Seg_7261" s="T652">n</ta>
            <ta e="T654" id="Seg_7262" s="T653">v</ta>
            <ta e="T655" id="Seg_7263" s="T654">n</ta>
            <ta e="T656" id="Seg_7264" s="T655">v</ta>
            <ta e="T657" id="Seg_7265" s="T656">adj</ta>
            <ta e="T658" id="Seg_7266" s="T657">n</ta>
            <ta e="T659" id="Seg_7267" s="T658">n</ta>
            <ta e="T660" id="Seg_7268" s="T659">v</ta>
            <ta e="T662" id="Seg_7269" s="T661">v</ta>
            <ta e="T663" id="Seg_7270" s="T662">adv</ta>
            <ta e="T664" id="Seg_7271" s="T663">n</ta>
            <ta e="T665" id="Seg_7272" s="T664">n</ta>
            <ta e="T666" id="Seg_7273" s="T665">v</ta>
            <ta e="T667" id="Seg_7274" s="T666">n</ta>
            <ta e="T668" id="Seg_7275" s="T667">v</ta>
            <ta e="T669" id="Seg_7276" s="T668">n</ta>
            <ta e="T670" id="Seg_7277" s="T669">v</ta>
            <ta e="T671" id="Seg_7278" s="T670">adv</ta>
            <ta e="T672" id="Seg_7279" s="T671">que</ta>
            <ta e="T673" id="Seg_7280" s="T672">n</ta>
            <ta e="T677" id="Seg_7281" s="T676">n</ta>
            <ta e="T678" id="Seg_7282" s="T677">v</ta>
            <ta e="T683" id="Seg_7283" s="T682">pers</ta>
            <ta e="T684" id="Seg_7284" s="T683">pers</ta>
            <ta e="T685" id="Seg_7285" s="T684">v</ta>
            <ta e="T689" id="Seg_7286" s="T688">v</ta>
            <ta e="T690" id="Seg_7287" s="T689">n</ta>
            <ta e="T691" id="Seg_7288" s="T690">v</ta>
            <ta e="T692" id="Seg_7289" s="T691">v</ta>
            <ta e="T693" id="Seg_7290" s="T692">que</ta>
            <ta e="T694" id="Seg_7291" s="T693">n</ta>
            <ta e="T695" id="Seg_7292" s="T694">n</ta>
            <ta e="T697" id="Seg_7293" s="T696">v</ta>
            <ta e="T698" id="Seg_7294" s="T697">n</ta>
            <ta e="T699" id="Seg_7295" s="T698">v</ta>
            <ta e="T700" id="Seg_7296" s="T699">n</ta>
            <ta e="T701" id="Seg_7297" s="T700">v</ta>
            <ta e="T702" id="Seg_7298" s="T701">n</ta>
            <ta e="T703" id="Seg_7299" s="T702">v</ta>
            <ta e="T709" id="Seg_7300" s="T708">n</ta>
            <ta e="T710" id="Seg_7301" s="T709">v</ta>
            <ta e="T711" id="Seg_7302" s="T710">ptcl</ta>
            <ta e="T713" id="Seg_7303" s="T712">v</ta>
            <ta e="T714" id="Seg_7304" s="T713">v</ta>
            <ta e="T716" id="Seg_7305" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_7306" s="T716">v</ta>
            <ta e="T718" id="Seg_7307" s="T717">v</ta>
            <ta e="T719" id="Seg_7308" s="T718">v</ta>
            <ta e="T720" id="Seg_7309" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_7310" s="T720">v</ta>
            <ta e="T722" id="Seg_7311" s="T721">dempro</ta>
            <ta e="T725" id="Seg_7312" s="T724">num</ta>
            <ta e="T726" id="Seg_7313" s="T725">num</ta>
            <ta e="T727" id="Seg_7314" s="T726">num</ta>
            <ta e="T728" id="Seg_7315" s="T727">num</ta>
            <ta e="T729" id="Seg_7316" s="T728">num</ta>
            <ta e="T730" id="Seg_7317" s="T729">num</ta>
            <ta e="T731" id="Seg_7318" s="T730">num</ta>
            <ta e="T732" id="Seg_7319" s="T731">num</ta>
            <ta e="T733" id="Seg_7320" s="T732">num</ta>
            <ta e="T734" id="Seg_7321" s="T733">num</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T18" id="Seg_7322" s="T17">0.2.h:A</ta>
            <ta e="T23" id="Seg_7323" s="T22">pro.h:Th</ta>
            <ta e="T24" id="Seg_7324" s="T23">pro:L</ta>
            <ta e="T28" id="Seg_7325" s="T27">adv:L</ta>
            <ta e="T30" id="Seg_7326" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_7327" s="T30">pro.h:A</ta>
            <ta e="T38" id="Seg_7328" s="T37">0.2.h:A</ta>
            <ta e="T41" id="Seg_7329" s="T40">np.h:A</ta>
            <ta e="T57" id="Seg_7330" s="T56">np:Th</ta>
            <ta e="T60" id="Seg_7331" s="T59">0.2.h:A</ta>
            <ta e="T73" id="Seg_7332" s="T72">pro.h:Th</ta>
            <ta e="T75" id="Seg_7333" s="T74">0.2.h:A</ta>
            <ta e="T76" id="Seg_7334" s="T75">np:G</ta>
            <ta e="T78" id="Seg_7335" s="T77">adv:L</ta>
            <ta e="T83" id="Seg_7336" s="T82">np:Th</ta>
            <ta e="T84" id="Seg_7337" s="T83">np:Th</ta>
            <ta e="T86" id="Seg_7338" s="T85">0.2.h:A</ta>
            <ta e="T88" id="Seg_7339" s="T87">0.2.h:A</ta>
            <ta e="T112" id="Seg_7340" s="T111">np.h:A</ta>
            <ta e="T114" id="Seg_7341" s="T113">0.2.h:Th</ta>
            <ta e="T115" id="Seg_7342" s="T114">0.2.h:A</ta>
            <ta e="T120" id="Seg_7343" s="T119">0.2.h:Th</ta>
            <ta e="T121" id="Seg_7344" s="T120">0.2.h:Th</ta>
            <ta e="T122" id="Seg_7345" s="T121">adv:L</ta>
            <ta e="T130" id="Seg_7346" s="T129">np:P</ta>
            <ta e="T134" id="Seg_7347" s="T133">adv:G</ta>
            <ta e="T135" id="Seg_7348" s="T134">adv:G</ta>
            <ta e="T137" id="Seg_7349" s="T136">np:Th</ta>
            <ta e="T139" id="Seg_7350" s="T138">np.h:P</ta>
            <ta e="T142" id="Seg_7351" s="T141">np:P</ta>
            <ta e="T144" id="Seg_7352" s="T143">np:P</ta>
            <ta e="T149" id="Seg_7353" s="T148">np:P</ta>
            <ta e="T167" id="Seg_7354" s="T166">np:P</ta>
            <ta e="T172" id="Seg_7355" s="T171">0.2.h:A</ta>
            <ta e="T174" id="Seg_7356" s="T173">0.2.h:A</ta>
            <ta e="T183" id="Seg_7357" s="T182">pro.h:Th</ta>
            <ta e="T184" id="Seg_7358" s="T183">adv:L</ta>
            <ta e="T186" id="Seg_7359" s="T185">adv:L</ta>
            <ta e="T189" id="Seg_7360" s="T188">0.3.h:A</ta>
            <ta e="T190" id="Seg_7361" s="T189">pro.h:A</ta>
            <ta e="T194" id="Seg_7362" s="T193">np:E</ta>
            <ta e="T201" id="Seg_7363" s="T200">np:Th</ta>
            <ta e="T206" id="Seg_7364" s="T204">pro:Ins</ta>
            <ta e="T208" id="Seg_7365" s="T207">np:Th</ta>
            <ta e="T210" id="Seg_7366" s="T209">0.2.h:A</ta>
            <ta e="T211" id="Seg_7367" s="T210">np:Th</ta>
            <ta e="T213" id="Seg_7368" s="T212">np:Th</ta>
            <ta e="T214" id="Seg_7369" s="T213">0.1.h:A</ta>
            <ta e="T215" id="Seg_7370" s="T214">pro.h:R</ta>
            <ta e="T217" id="Seg_7371" s="T216">0.2.h:A</ta>
            <ta e="T250" id="Seg_7372" s="T249">np:E 0.1.h:Poss</ta>
            <ta e="T252" id="Seg_7373" s="T251">0.2.h:A</ta>
            <ta e="T253" id="Seg_7374" s="T252">0.2.h:Th</ta>
            <ta e="T254" id="Seg_7375" s="T253">0.2.h:A</ta>
            <ta e="T256" id="Seg_7376" s="T255">0.2.h:A</ta>
            <ta e="T269" id="Seg_7377" s="T741">np:G</ta>
            <ta e="T271" id="Seg_7378" s="T270">np:Th</ta>
            <ta e="T273" id="Seg_7379" s="T272">np:Th</ta>
            <ta e="T275" id="Seg_7380" s="T274">adv:Time</ta>
            <ta e="T277" id="Seg_7381" s="T276">pro.h:A</ta>
            <ta e="T280" id="Seg_7382" s="T279">pro:P</ta>
            <ta e="T281" id="Seg_7383" s="T280">0.1.h:A</ta>
            <ta e="T283" id="Seg_7384" s="T282">np:P</ta>
            <ta e="T284" id="Seg_7385" s="T283">0.1.h:A</ta>
            <ta e="T287" id="Seg_7386" s="T286">np:P</ta>
            <ta e="T288" id="Seg_7387" s="T287">0.1.h:A</ta>
            <ta e="T289" id="Seg_7388" s="T288">np:P</ta>
            <ta e="T290" id="Seg_7389" s="T289">0.1.h:A</ta>
            <ta e="T291" id="Seg_7390" s="T290">np:P</ta>
            <ta e="T292" id="Seg_7391" s="T291">0.1.h:A</ta>
            <ta e="T293" id="Seg_7392" s="T292">np:P</ta>
            <ta e="T294" id="Seg_7393" s="T293">0.1.h:A</ta>
            <ta e="T299" id="Seg_7394" s="T298">0.3:E</ta>
            <ta e="T301" id="Seg_7395" s="T300">np:E 0.1.h:Poss</ta>
            <ta e="T303" id="Seg_7396" s="T302">np:E 0.1.h:Poss</ta>
            <ta e="T305" id="Seg_7397" s="T304">np:E 0.1.h:Poss</ta>
            <ta e="T307" id="Seg_7398" s="T306">np:E 0.3.h:Poss</ta>
            <ta e="T344" id="Seg_7399" s="T343">pro.h:Poss</ta>
            <ta e="T346" id="Seg_7400" s="T345">np.h:Th</ta>
            <ta e="T348" id="Seg_7401" s="T347">np.h:Th</ta>
            <ta e="T350" id="Seg_7402" s="T349">np.h:Th</ta>
            <ta e="T357" id="Seg_7403" s="T356">np.h:E</ta>
            <ta e="T359" id="Seg_7404" s="T358">np.h:E</ta>
            <ta e="T360" id="Seg_7405" s="T359">pro.h:Poss</ta>
            <ta e="T361" id="Seg_7406" s="T360">np.h:Th</ta>
            <ta e="T363" id="Seg_7407" s="T362">np.h:Th</ta>
            <ta e="T365" id="Seg_7408" s="T364">np.h:Th</ta>
            <ta e="T366" id="Seg_7409" s="T365">np.h:Th</ta>
            <ta e="T368" id="Seg_7410" s="T367">np.h:Th</ta>
            <ta e="T427" id="Seg_7411" s="T426">pro.h:Th</ta>
            <ta e="T431" id="Seg_7412" s="T430">pro.h:Th</ta>
            <ta e="T433" id="Seg_7413" s="T432">np.h:Th</ta>
            <ta e="T434" id="Seg_7414" s="T433">0.2.h:A</ta>
            <ta e="T435" id="Seg_7415" s="T434">0.2.h:A</ta>
            <ta e="T439" id="Seg_7416" s="T438">np:L</ta>
            <ta e="T440" id="Seg_7417" s="T439">0.1.h:E</ta>
            <ta e="T443" id="Seg_7418" s="T442">0.1.h:Poss</ta>
            <ta e="T448" id="Seg_7419" s="T447">np:E 0.1.h:Poss</ta>
            <ta e="T450" id="Seg_7420" s="T449">np:E 0.1.h:Poss</ta>
            <ta e="T452" id="Seg_7421" s="T451">np:E 0.1.h:Poss</ta>
            <ta e="T454" id="Seg_7422" s="T453">np:E 0.1.h:Poss</ta>
            <ta e="T456" id="Seg_7423" s="T455">np:E</ta>
            <ta e="T458" id="Seg_7424" s="T457">np:E 0.1.h:Poss</ta>
            <ta e="T460" id="Seg_7425" s="T459">np:E 0.1.h:Poss</ta>
            <ta e="T465" id="Seg_7426" s="T464">np:G</ta>
            <ta e="T468" id="Seg_7427" s="T467">np:P</ta>
            <ta e="T470" id="Seg_7428" s="T469">np:P</ta>
            <ta e="T473" id="Seg_7429" s="T472">np:P</ta>
            <ta e="T476" id="Seg_7430" s="T475">np:P</ta>
            <ta e="T479" id="Seg_7431" s="T478">np:P</ta>
            <ta e="T482" id="Seg_7432" s="T481">np:P</ta>
            <ta e="T485" id="Seg_7433" s="T484">np:P</ta>
            <ta e="T496" id="Seg_7434" s="T495">np:Th</ta>
            <ta e="T499" id="Seg_7435" s="T498">np:Th</ta>
            <ta e="T501" id="Seg_7436" s="T500">np:Th</ta>
            <ta e="T518" id="Seg_7437" s="T517">np:Th</ta>
            <ta e="T520" id="Seg_7438" s="T519">np:Th</ta>
            <ta e="T524" id="Seg_7439" s="T523">0.3:P</ta>
            <ta e="T526" id="Seg_7440" s="T525">np:G</ta>
            <ta e="T527" id="Seg_7441" s="T526">np:G</ta>
            <ta e="T529" id="Seg_7442" s="T528">np:Th</ta>
            <ta e="T531" id="Seg_7443" s="T530">0.2.h:Th</ta>
            <ta e="T532" id="Seg_7444" s="T531">adv:L</ta>
            <ta e="T533" id="Seg_7445" s="T532">0.2.h:A</ta>
            <ta e="T535" id="Seg_7446" s="T534">0.2.h:Th</ta>
            <ta e="T537" id="Seg_7447" s="T536">pro:Th</ta>
            <ta e="T538" id="Seg_7448" s="T537">0.2.h:E</ta>
            <ta e="T539" id="Seg_7449" s="T538">pro:G</ta>
            <ta e="T540" id="Seg_7450" s="T539">0.2.h:A</ta>
            <ta e="T541" id="Seg_7451" s="T540">np:G</ta>
            <ta e="T542" id="Seg_7452" s="T541">0.1.h:A</ta>
            <ta e="T544" id="Seg_7453" s="T543">adv:L</ta>
            <ta e="T545" id="Seg_7454" s="T544">np:Th</ta>
            <ta e="T547" id="Seg_7455" s="T546">0.1.h:E</ta>
            <ta e="T549" id="Seg_7456" s="T548">0.2.h:A</ta>
            <ta e="T551" id="Seg_7457" s="T550">0.2.h:E</ta>
            <ta e="T598" id="Seg_7458" s="T597">np:Th</ta>
            <ta e="T599" id="Seg_7459" s="T598">np:P</ta>
            <ta e="T604" id="Seg_7460" s="T603">np:Th</ta>
            <ta e="T606" id="Seg_7461" s="T605">np:G</ta>
            <ta e="T615" id="Seg_7462" s="T614">pro:G</ta>
            <ta e="T616" id="Seg_7463" s="T615">0.2.h:A</ta>
            <ta e="T617" id="Seg_7464" s="T616">pro:G</ta>
            <ta e="T618" id="Seg_7465" s="T617">0.2.h:A</ta>
            <ta e="T619" id="Seg_7466" s="T618">np:Th</ta>
            <ta e="T624" id="Seg_7467" s="T623">0.1.h:A</ta>
            <ta e="T625" id="Seg_7468" s="T624">0.1.h:A</ta>
            <ta e="T626" id="Seg_7469" s="T625">pro.h:Com</ta>
            <ta e="T628" id="Seg_7470" s="T627">pro.h:E</ta>
            <ta e="T637" id="Seg_7471" s="T636">0.2.h:Th</ta>
            <ta e="T639" id="Seg_7472" s="T638">np:P</ta>
            <ta e="T641" id="Seg_7473" s="T640">np:Com</ta>
            <ta e="T643" id="Seg_7474" s="T642">np:P</ta>
            <ta e="T644" id="Seg_7475" s="T643">0.2.h:A</ta>
            <ta e="T645" id="Seg_7476" s="T644">np:P</ta>
            <ta e="T646" id="Seg_7477" s="T645">0.2.h:A</ta>
            <ta e="T647" id="Seg_7478" s="T646">np:P</ta>
            <ta e="T648" id="Seg_7479" s="T647">0.2.h:A</ta>
            <ta e="T650" id="Seg_7480" s="T649">np:P</ta>
            <ta e="T651" id="Seg_7481" s="T650">0.2.h:A</ta>
            <ta e="T653" id="Seg_7482" s="T652">np:P</ta>
            <ta e="T654" id="Seg_7483" s="T653">0.2.h:A</ta>
            <ta e="T655" id="Seg_7484" s="T654">np:Th</ta>
            <ta e="T656" id="Seg_7485" s="T655">0.2.h:A</ta>
            <ta e="T658" id="Seg_7486" s="T657">np:G</ta>
            <ta e="T659" id="Seg_7487" s="T658">np:P</ta>
            <ta e="T660" id="Seg_7488" s="T659">0.2.h:A</ta>
            <ta e="T662" id="Seg_7489" s="T661">0.1.h:A</ta>
            <ta e="T664" id="Seg_7490" s="T663">np:G</ta>
            <ta e="T665" id="Seg_7491" s="T664">np:Th</ta>
            <ta e="T667" id="Seg_7492" s="T666">np:P</ta>
            <ta e="T669" id="Seg_7493" s="T668">np:Th</ta>
            <ta e="T673" id="Seg_7494" s="T672">np:Th</ta>
            <ta e="T677" id="Seg_7495" s="T676">np:Th</ta>
            <ta e="T683" id="Seg_7496" s="T682">pro.h:E</ta>
            <ta e="T684" id="Seg_7497" s="T683">pro.h:B</ta>
            <ta e="T689" id="Seg_7498" s="T688">0.2.h:A</ta>
            <ta e="T690" id="Seg_7499" s="T689">np:Th</ta>
            <ta e="T691" id="Seg_7500" s="T690">0.2.h:A</ta>
            <ta e="T692" id="Seg_7501" s="T691">0.1.h:A</ta>
            <ta e="T693" id="Seg_7502" s="T692">pro:G</ta>
            <ta e="T694" id="Seg_7503" s="T693">np:Com</ta>
            <ta e="T695" id="Seg_7504" s="T694">np:Th</ta>
            <ta e="T697" id="Seg_7505" s="T696">0.1.h:A</ta>
            <ta e="T698" id="Seg_7506" s="T697">np:Th</ta>
            <ta e="T699" id="Seg_7507" s="T698">0.1.h:A</ta>
            <ta e="T700" id="Seg_7508" s="T699">np:Th</ta>
            <ta e="T701" id="Seg_7509" s="T700">0.1.h:A</ta>
            <ta e="T702" id="Seg_7510" s="T701">np:Th</ta>
            <ta e="T703" id="Seg_7511" s="T702">0.1.h:A</ta>
            <ta e="T709" id="Seg_7512" s="T708">np.h:A 0.1.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T18" id="Seg_7513" s="T17">v:pred 0.2.h:S</ta>
            <ta e="T23" id="Seg_7514" s="T22">pro.h:S</ta>
            <ta e="T25" id="Seg_7515" s="T24">v:pred</ta>
            <ta e="T30" id="Seg_7516" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_7517" s="T30">pro.h:S</ta>
            <ta e="T742" id="Seg_7518" s="T31">conv:pred</ta>
            <ta e="T32" id="Seg_7519" s="T742">v:pred</ta>
            <ta e="T38" id="Seg_7520" s="T37">v:pred 0.2.h:S</ta>
            <ta e="T41" id="Seg_7521" s="T40">np.h:S</ta>
            <ta e="T44" id="Seg_7522" s="T43">v:pred</ta>
            <ta e="T54" id="Seg_7523" s="T52">v:pred</ta>
            <ta e="T57" id="Seg_7524" s="T56">np:S</ta>
            <ta e="T58" id="Seg_7525" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_7526" s="T59">v:pred 0.2.h:S</ta>
            <ta e="T73" id="Seg_7527" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_7528" s="T73">adj:pred</ta>
            <ta e="T75" id="Seg_7529" s="T74">v:pred 0.2.h:S</ta>
            <ta e="T80" id="Seg_7530" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_7531" s="T82">np:S</ta>
            <ta e="T84" id="Seg_7532" s="T83">np:S</ta>
            <ta e="T85" id="Seg_7533" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_7534" s="T85">v:pred 0.2.h:S</ta>
            <ta e="T88" id="Seg_7535" s="T87">v:pred 0.2.h:S</ta>
            <ta e="T112" id="Seg_7536" s="T111">np.h:S</ta>
            <ta e="T113" id="Seg_7537" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_7538" s="T113">v:pred 0.2.h:S</ta>
            <ta e="T115" id="Seg_7539" s="T114">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_7540" s="T116">v:pred</ta>
            <ta e="T118" id="Seg_7541" s="T117">ptcl:pred</ta>
            <ta e="T120" id="Seg_7542" s="T119">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_7543" s="T120">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_7544" s="T128">ptcl:pred</ta>
            <ta e="T130" id="Seg_7545" s="T129">np:O</ta>
            <ta e="T131" id="Seg_7546" s="T130">v:pred</ta>
            <ta e="T132" id="Seg_7547" s="T131">ptcl:pred</ta>
            <ta e="T133" id="Seg_7548" s="T132">v:pred</ta>
            <ta e="T136" id="Seg_7549" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_7550" s="T136">np:O</ta>
            <ta e="T138" id="Seg_7551" s="T137">v:pred</ta>
            <ta e="T139" id="Seg_7552" s="T138">np.h:O</ta>
            <ta e="T140" id="Seg_7553" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_7554" s="T141">np:O</ta>
            <ta e="T143" id="Seg_7555" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_7556" s="T143">np:O</ta>
            <ta e="T145" id="Seg_7557" s="T144">v:pred</ta>
            <ta e="T148" id="Seg_7558" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_7559" s="T148">np:O</ta>
            <ta e="T150" id="Seg_7560" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_7561" s="T150">ptcl:pred</ta>
            <ta e="T167" id="Seg_7562" s="T166">np:O</ta>
            <ta e="T168" id="Seg_7563" s="T167">ptcl:pred</ta>
            <ta e="T169" id="Seg_7564" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_7565" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_7566" s="T171">v:pred 0.2.h:S</ta>
            <ta e="T174" id="Seg_7567" s="T173">v:pred 0.2.h:S</ta>
            <ta e="T183" id="Seg_7568" s="T182">pro.h:S</ta>
            <ta e="T185" id="Seg_7569" s="T184">v:pred</ta>
            <ta e="T189" id="Seg_7570" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_7571" s="T189">pro.h:S</ta>
            <ta e="T743" id="Seg_7572" s="T190">conv:pred</ta>
            <ta e="T191" id="Seg_7573" s="T743">v:pred</ta>
            <ta e="T194" id="Seg_7574" s="T193">np:S</ta>
            <ta e="T196" id="Seg_7575" s="T195">v:pred</ta>
            <ta e="T201" id="Seg_7576" s="T200">np:S</ta>
            <ta e="T202" id="Seg_7577" s="T201">v:pred</ta>
            <ta e="T203" id="Seg_7578" s="T202">ptcl:pred</ta>
            <ta e="T204" id="Seg_7579" s="T203">v:pred</ta>
            <ta e="T207" id="Seg_7580" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_7581" s="T207">np:S</ta>
            <ta e="T209" id="Seg_7582" s="T208">v:pred</ta>
            <ta e="T210" id="Seg_7583" s="T209">v:pred 0.2.h:S</ta>
            <ta e="T211" id="Seg_7584" s="T210">np:O</ta>
            <ta e="T213" id="Seg_7585" s="T212">np:S</ta>
            <ta e="T214" id="Seg_7586" s="T213">v:pred 0.1.h:S</ta>
            <ta e="T215" id="Seg_7587" s="T214">pro.h:O</ta>
            <ta e="T217" id="Seg_7588" s="T216">v:pred 0.2.h:S</ta>
            <ta e="T222" id="Seg_7589" s="T221">v:pred</ta>
            <ta e="T242" id="Seg_7590" s="T241">ptcl:pred</ta>
            <ta e="T243" id="Seg_7591" s="T242">v:pred</ta>
            <ta e="T248" id="Seg_7592" s="T247">ptcl:pred</ta>
            <ta e="T250" id="Seg_7593" s="T249">np:S</ta>
            <ta e="T251" id="Seg_7594" s="T250">v:pred</ta>
            <ta e="T252" id="Seg_7595" s="T251">v:pred 0.2.h:S</ta>
            <ta e="T253" id="Seg_7596" s="T252">v:pred 0.2.h:S</ta>
            <ta e="T254" id="Seg_7597" s="T253">v:pred 0.2.h:S</ta>
            <ta e="T256" id="Seg_7598" s="T255">v:pred 0.2.h:S</ta>
            <ta e="T259" id="Seg_7599" s="T257">ptcl:pred</ta>
            <ta e="T260" id="Seg_7600" s="T259">v:pred</ta>
            <ta e="T270" id="Seg_7601" s="T269">v:pred</ta>
            <ta e="T271" id="Seg_7602" s="T270">np:O</ta>
            <ta e="T272" id="Seg_7603" s="T271">v:pred</ta>
            <ta e="T273" id="Seg_7604" s="T272">np:O</ta>
            <ta e="T274" id="Seg_7605" s="T273">v:pred</ta>
            <ta e="T276" id="Seg_7606" s="T275">v:pred</ta>
            <ta e="T277" id="Seg_7607" s="T276">pro.h:S</ta>
            <ta e="T280" id="Seg_7608" s="T279">pro:O</ta>
            <ta e="T281" id="Seg_7609" s="T280">v:pred</ta>
            <ta e="T283" id="Seg_7610" s="T282">np:O</ta>
            <ta e="T284" id="Seg_7611" s="T283">v:pred 0.1.h:S</ta>
            <ta e="T287" id="Seg_7612" s="T286">np:O</ta>
            <ta e="T288" id="Seg_7613" s="T287">v:pred 0.1.h:S</ta>
            <ta e="T289" id="Seg_7614" s="T288">np:O</ta>
            <ta e="T290" id="Seg_7615" s="T289">v:pred 0.1.h:S</ta>
            <ta e="T291" id="Seg_7616" s="T290">np:O</ta>
            <ta e="T292" id="Seg_7617" s="T291">v:pred 0.1.h:S</ta>
            <ta e="T293" id="Seg_7618" s="T292">np:O</ta>
            <ta e="T294" id="Seg_7619" s="T293">v:pred 0.1.h:S</ta>
            <ta e="T299" id="Seg_7620" s="T298">v:pred 0.3:S</ta>
            <ta e="T301" id="Seg_7621" s="T300">np:S</ta>
            <ta e="T302" id="Seg_7622" s="T301">v:pred</ta>
            <ta e="T303" id="Seg_7623" s="T302">np:S</ta>
            <ta e="T304" id="Seg_7624" s="T303">v:pred</ta>
            <ta e="T305" id="Seg_7625" s="T304">np:S</ta>
            <ta e="T306" id="Seg_7626" s="T305">v:pred</ta>
            <ta e="T307" id="Seg_7627" s="T306">np:S</ta>
            <ta e="T308" id="Seg_7628" s="T307">v:pred</ta>
            <ta e="T321" id="Seg_7629" s="T320">v:pred</ta>
            <ta e="T322" id="Seg_7630" s="T321">v:pred</ta>
            <ta e="T323" id="Seg_7631" s="T322">v:pred</ta>
            <ta e="T324" id="Seg_7632" s="T323">ptcl:pred</ta>
            <ta e="T325" id="Seg_7633" s="T324">v:pred</ta>
            <ta e="T327" id="Seg_7634" s="T325">ptcl:pred</ta>
            <ta e="T346" id="Seg_7635" s="T345">np.h:S</ta>
            <ta e="T347" id="Seg_7636" s="T346">v:pred</ta>
            <ta e="T356" id="Seg_7637" s="T355">v:pred</ta>
            <ta e="T357" id="Seg_7638" s="T356">np.h:S</ta>
            <ta e="T359" id="Seg_7639" s="T358">np.h:S</ta>
            <ta e="T361" id="Seg_7640" s="T360">np.h:S</ta>
            <ta e="T362" id="Seg_7641" s="T361">v:pred </ta>
            <ta e="T427" id="Seg_7642" s="T426">pro.h:S</ta>
            <ta e="T428" id="Seg_7643" s="T427">ptcl:pred</ta>
            <ta e="T429" id="Seg_7644" s="T428">v:pred</ta>
            <ta e="T431" id="Seg_7645" s="T430">pro.h:O</ta>
            <ta e="T434" id="Seg_7646" s="T433">v:pred 0.2.h:S</ta>
            <ta e="T435" id="Seg_7647" s="T434">v:pred 0.2.h:S</ta>
            <ta e="T440" id="Seg_7648" s="T439">v:pred 0.1.h:S</ta>
            <ta e="T444" id="Seg_7649" s="T443">v:pred</ta>
            <ta e="T446" id="Seg_7650" s="T444">ptcl:pred</ta>
            <ta e="T448" id="Seg_7651" s="T447">np:S</ta>
            <ta e="T449" id="Seg_7652" s="T448">v:pred</ta>
            <ta e="T450" id="Seg_7653" s="T449">np:S</ta>
            <ta e="T451" id="Seg_7654" s="T450">v:pred</ta>
            <ta e="T452" id="Seg_7655" s="T451">np:S</ta>
            <ta e="T453" id="Seg_7656" s="T452">v:pred</ta>
            <ta e="T454" id="Seg_7657" s="T453">np:S</ta>
            <ta e="T455" id="Seg_7658" s="T454">v:pred</ta>
            <ta e="T456" id="Seg_7659" s="T455">np:S</ta>
            <ta e="T457" id="Seg_7660" s="T456">v:pred</ta>
            <ta e="T458" id="Seg_7661" s="T457">np:S</ta>
            <ta e="T459" id="Seg_7662" s="T458">v:pred</ta>
            <ta e="T460" id="Seg_7663" s="T459">np:S</ta>
            <ta e="T461" id="Seg_7664" s="T460">v:pred</ta>
            <ta e="T463" id="Seg_7665" s="T462">ptcl.neg</ta>
            <ta e="T466" id="Seg_7666" s="T465">ptcl:pred</ta>
            <ta e="T467" id="Seg_7667" s="T466">v:pred</ta>
            <ta e="T468" id="Seg_7668" s="T467">np:O</ta>
            <ta e="T469" id="Seg_7669" s="T468">s:purp</ta>
            <ta e="T470" id="Seg_7670" s="T469">np:O</ta>
            <ta e="T471" id="Seg_7671" s="T470">v:pred</ta>
            <ta e="T472" id="Seg_7672" s="T471">ptcl:pred</ta>
            <ta e="T473" id="Seg_7673" s="T472">np:O</ta>
            <ta e="T474" id="Seg_7674" s="T473">v:pred</ta>
            <ta e="T475" id="Seg_7675" s="T474">ptcl:pred</ta>
            <ta e="T476" id="Seg_7676" s="T475">np:O</ta>
            <ta e="T477" id="Seg_7677" s="T476">v:pred</ta>
            <ta e="T478" id="Seg_7678" s="T477">ptcl:pred</ta>
            <ta e="T479" id="Seg_7679" s="T478">np:O</ta>
            <ta e="T480" id="Seg_7680" s="T479">v:pred</ta>
            <ta e="T481" id="Seg_7681" s="T480">ptcl:pred</ta>
            <ta e="T482" id="Seg_7682" s="T481">np:O</ta>
            <ta e="T483" id="Seg_7683" s="T482">ptcl:pred</ta>
            <ta e="T484" id="Seg_7684" s="T483">v:pred</ta>
            <ta e="T485" id="Seg_7685" s="T484">np:S</ta>
            <ta e="T486" id="Seg_7686" s="T485">adj:pred</ta>
            <ta e="T487" id="Seg_7687" s="T486">cop</ta>
            <ta e="T488" id="Seg_7688" s="T487">v:pred</ta>
            <ta e="T489" id="Seg_7689" s="T488">ptcl:pred</ta>
            <ta e="T493" id="Seg_7690" s="T492">ptcl:pred</ta>
            <ta e="T496" id="Seg_7691" s="T495">np:O</ta>
            <ta e="T498" id="Seg_7692" s="T497">v:pred 0.2.h:S</ta>
            <ta e="T499" id="Seg_7693" s="T498">np:S</ta>
            <ta e="T500" id="Seg_7694" s="T499">v:pred</ta>
            <ta e="T501" id="Seg_7695" s="T500">np:O</ta>
            <ta e="T502" id="Seg_7696" s="T501">v:pred 0.2.h:S</ta>
            <ta e="T518" id="Seg_7697" s="T517">np:S</ta>
            <ta e="T519" id="Seg_7698" s="T518">v:pred</ta>
            <ta e="T520" id="Seg_7699" s="T519">np:O</ta>
            <ta e="T521" id="Seg_7700" s="T520">v:pred 0.2.h:S</ta>
            <ta e="T524" id="Seg_7701" s="T523">v:pred</ta>
            <ta e="T528" id="Seg_7702" s="T527">v:pred</ta>
            <ta e="T529" id="Seg_7703" s="T528">np:O</ta>
            <ta e="T531" id="Seg_7704" s="T530">v:pred 0.2.h:S</ta>
            <ta e="T533" id="Seg_7705" s="T532">v:pred 0.2.h:S</ta>
            <ta e="T535" id="Seg_7706" s="T534">v:pred</ta>
            <ta e="T538" id="Seg_7707" s="T537">v:pred</ta>
            <ta e="T540" id="Seg_7708" s="T539">v:pred</ta>
            <ta e="T542" id="Seg_7709" s="T541">v:pred</ta>
            <ta e="T543" id="Seg_7710" s="T542">conv:pred</ta>
            <ta e="T547" id="Seg_7711" s="T546">v:pred 0.1.h:S</ta>
            <ta e="T549" id="Seg_7712" s="T548">v:pred 0.2.h:S</ta>
            <ta e="T551" id="Seg_7713" s="T550">v:pred 0.2.h:S</ta>
            <ta e="T596" id="Seg_7714" s="T595">v:pred</ta>
            <ta e="T597" id="Seg_7715" s="T596">ptcl:pred</ta>
            <ta e="T598" id="Seg_7716" s="T597">np:O</ta>
            <ta e="T599" id="Seg_7717" s="T598">np:O</ta>
            <ta e="T600" id="Seg_7718" s="T599">v:pred</ta>
            <ta e="T601" id="Seg_7719" s="T600">ptcl:pred</ta>
            <ta e="T602" id="Seg_7720" s="T601">v:pred</ta>
            <ta e="T603" id="Seg_7721" s="T602">ptcl:pred</ta>
            <ta e="T604" id="Seg_7722" s="T603">np:O</ta>
            <ta e="T605" id="Seg_7723" s="T604">v:pred</ta>
            <ta e="T607" id="Seg_7724" s="T606">v:pred</ta>
            <ta e="T608" id="Seg_7725" s="T607">v:pred</ta>
            <ta e="T616" id="Seg_7726" s="T615">v:pred</ta>
            <ta e="T618" id="Seg_7727" s="T617">v:pred</ta>
            <ta e="T619" id="Seg_7728" s="T618">np:S</ta>
            <ta e="T620" id="Seg_7729" s="T619">v:pred</ta>
            <ta e="T622" id="Seg_7730" s="T621">v:pred</ta>
            <ta e="T624" id="Seg_7731" s="T623">v:pred</ta>
            <ta e="T625" id="Seg_7732" s="T624">v:pred</ta>
            <ta e="T626" id="Seg_7733" s="T625">pro.h:S</ta>
            <ta e="T628" id="Seg_7734" s="T627">pro.h:S</ta>
            <ta e="T629" id="Seg_7735" s="T628">v:pred</ta>
            <ta e="T636" id="Seg_7736" s="T635">v:pred</ta>
            <ta e="T637" id="Seg_7737" s="T636">v:pred 0.2.h:S</ta>
            <ta e="T638" id="Seg_7738" s="T637">s:purp</ta>
            <ta e="T639" id="Seg_7739" s="T638">np:O</ta>
            <ta e="T643" id="Seg_7740" s="T642">np:O</ta>
            <ta e="T644" id="Seg_7741" s="T643">v:pred 0.2.h:S</ta>
            <ta e="T645" id="Seg_7742" s="T644">np:O</ta>
            <ta e="T646" id="Seg_7743" s="T645">v:pred 0.2.h:S</ta>
            <ta e="T647" id="Seg_7744" s="T646">np:O</ta>
            <ta e="T648" id="Seg_7745" s="T647">v:pred 0.2.h:S</ta>
            <ta e="T650" id="Seg_7746" s="T649">np:O</ta>
            <ta e="T651" id="Seg_7747" s="T650">v:pred 0.2.h:S</ta>
            <ta e="T653" id="Seg_7748" s="T652">np:O</ta>
            <ta e="T654" id="Seg_7749" s="T653">v:pred 0.2.h:S</ta>
            <ta e="T655" id="Seg_7750" s="T654">np:O</ta>
            <ta e="T656" id="Seg_7751" s="T655">v:pred 0.2.h:S</ta>
            <ta e="T659" id="Seg_7752" s="T658">np:O</ta>
            <ta e="T660" id="Seg_7753" s="T659">v:pred 0.2.h:S</ta>
            <ta e="T662" id="Seg_7754" s="T661">v:pred 0.1.h:S</ta>
            <ta e="T665" id="Seg_7755" s="T664">np:O</ta>
            <ta e="T666" id="Seg_7756" s="T665">s:purp</ta>
            <ta e="T667" id="Seg_7757" s="T666">np:O</ta>
            <ta e="T668" id="Seg_7758" s="T667">v:pred</ta>
            <ta e="T669" id="Seg_7759" s="T668">np:O</ta>
            <ta e="T670" id="Seg_7760" s="T669">v:pred</ta>
            <ta e="T673" id="Seg_7761" s="T672">np:O</ta>
            <ta e="T677" id="Seg_7762" s="T676">np:O</ta>
            <ta e="T678" id="Seg_7763" s="T677">v:pred</ta>
            <ta e="T683" id="Seg_7764" s="T682">pro.h:S</ta>
            <ta e="T684" id="Seg_7765" s="T683">pro.h:O</ta>
            <ta e="T685" id="Seg_7766" s="T684">v:pred</ta>
            <ta e="T689" id="Seg_7767" s="T688">v:pred 0.2.h:S</ta>
            <ta e="T690" id="Seg_7768" s="T689">np:O</ta>
            <ta e="T691" id="Seg_7769" s="T690">v:pred 0.2.h:S</ta>
            <ta e="T692" id="Seg_7770" s="T691">v:pred 0.1.h:S</ta>
            <ta e="T695" id="Seg_7771" s="T694">np:O</ta>
            <ta e="T697" id="Seg_7772" s="T696">v:pred 0.1.h:S</ta>
            <ta e="T698" id="Seg_7773" s="T697">np:O</ta>
            <ta e="T699" id="Seg_7774" s="T698">v:pred 0.1.h:S</ta>
            <ta e="T700" id="Seg_7775" s="T699">np:O</ta>
            <ta e="T701" id="Seg_7776" s="T700">v:pred 0.1.h:S</ta>
            <ta e="T702" id="Seg_7777" s="T701">np:O</ta>
            <ta e="T703" id="Seg_7778" s="T702">v:pred 0.1.h:S</ta>
            <ta e="T709" id="Seg_7779" s="T708">np.h:S</ta>
            <ta e="T710" id="Seg_7780" s="T709">v:pred</ta>
            <ta e="T711" id="Seg_7781" s="T710">ptcl:pred</ta>
            <ta e="T713" id="Seg_7782" s="T712">v:pred</ta>
            <ta e="T714" id="Seg_7783" s="T713">v:pred</ta>
            <ta e="T716" id="Seg_7784" s="T715">ptcl:pred</ta>
            <ta e="T717" id="Seg_7785" s="T716">v:pred</ta>
            <ta e="T718" id="Seg_7786" s="T717">s:purp</ta>
            <ta e="T719" id="Seg_7787" s="T718">s:purp</ta>
            <ta e="T720" id="Seg_7788" s="T719">ptcl:pred</ta>
            <ta e="T721" id="Seg_7789" s="T720">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T29" id="Seg_7790" s="T28">TURK:disc</ta>
            <ta e="T39" id="Seg_7791" s="T38">TURK:disc</ta>
            <ta e="T41" id="Seg_7792" s="T40">TURK:cult</ta>
            <ta e="T42" id="Seg_7793" s="T41">TURK:disc</ta>
            <ta e="T46" id="Seg_7794" s="T45">TURK:disc</ta>
            <ta e="T61" id="Seg_7795" s="T60">TURK:disc</ta>
            <ta e="T81" id="Seg_7796" s="T80">RUS:gram</ta>
            <ta e="T91" id="Seg_7797" s="T89">RUS:gram</ta>
            <ta e="T108" id="Seg_7798" s="T107">TURK:cult</ta>
            <ta e="T116" id="Seg_7799" s="T115">RUS:mod</ta>
            <ta e="T118" id="Seg_7800" s="T117">RUS:mod</ta>
            <ta e="T129" id="Seg_7801" s="T128">RUS:mod</ta>
            <ta e="T130" id="Seg_7802" s="T129">TAT:cult</ta>
            <ta e="T132" id="Seg_7803" s="T131">RUS:mod</ta>
            <ta e="T137" id="Seg_7804" s="T136">TURK:cult</ta>
            <ta e="T151" id="Seg_7805" s="T150">RUS:mod</ta>
            <ta e="T168" id="Seg_7806" s="T167">RUS:mod</ta>
            <ta e="T174" id="Seg_7807" s="T173">RUS:gram</ta>
            <ta e="T187" id="Seg_7808" s="T186">TURK:disc</ta>
            <ta e="T195" id="Seg_7809" s="T194">RUS:mod</ta>
            <ta e="T201" id="Seg_7810" s="T200">TURK:cult</ta>
            <ta e="T203" id="Seg_7811" s="T202">RUS:mod</ta>
            <ta e="T235" id="Seg_7812" s="T234">TURK:core</ta>
            <ta e="T242" id="Seg_7813" s="T241">RUS:mod</ta>
            <ta e="T248" id="Seg_7814" s="T247">RUS:mod</ta>
            <ta e="T259" id="Seg_7815" s="T257">RUS:mod</ta>
            <ta e="T261" id="Seg_7816" s="T260">RUS:gram</ta>
            <ta e="T280" id="Seg_7817" s="T279">RUS:gram(INDEF)</ta>
            <ta e="T285" id="Seg_7818" s="T284">TURK:disc</ta>
            <ta e="T324" id="Seg_7819" s="T323">RUS:mod</ta>
            <ta e="T327" id="Seg_7820" s="T325">RUS:mod</ta>
            <ta e="T342" id="Seg_7821" s="T341">RUS:gram</ta>
            <ta e="T349" id="Seg_7822" s="T348">RUS:gram</ta>
            <ta e="T358" id="Seg_7823" s="T357">RUS:gram</ta>
            <ta e="T364" id="Seg_7824" s="T363">RUS:gram</ta>
            <ta e="T367" id="Seg_7825" s="T366">RUS:gram</ta>
            <ta e="T428" id="Seg_7826" s="T427">RUS:mod</ta>
            <ta e="T430" id="Seg_7827" s="T429">RUS:gram</ta>
            <ta e="T439" id="Seg_7828" s="T438">TAT:cult</ta>
            <ta e="T442" id="Seg_7829" s="T441">TURK:disc</ta>
            <ta e="T446" id="Seg_7830" s="T444">RUS:mod</ta>
            <ta e="T463" id="Seg_7831" s="T462">TURK:disc</ta>
            <ta e="T466" id="Seg_7832" s="T465">RUS:mod</ta>
            <ta e="T472" id="Seg_7833" s="T471">RUS:mod</ta>
            <ta e="T475" id="Seg_7834" s="T474">RUS:mod</ta>
            <ta e="T478" id="Seg_7835" s="T477">RUS:mod</ta>
            <ta e="T481" id="Seg_7836" s="T480">RUS:mod</ta>
            <ta e="T483" id="Seg_7837" s="T482">RUS:mod</ta>
            <ta e="T489" id="Seg_7838" s="T488">RUS:mod</ta>
            <ta e="T493" id="Seg_7839" s="T492">RUS:mod</ta>
            <ta e="T523" id="Seg_7840" s="T522">RUS:mod</ta>
            <ta e="T533" id="Seg_7841" s="T532">%TURK:core</ta>
            <ta e="T548" id="Seg_7842" s="T547">RUS:disc</ta>
            <ta e="T552" id="Seg_7843" s="T551">RUS:gram</ta>
            <ta e="T597" id="Seg_7844" s="T596">RUS:mod</ta>
            <ta e="T598" id="Seg_7845" s="T597">TURK:cult</ta>
            <ta e="T601" id="Seg_7846" s="T600">RUS:mod</ta>
            <ta e="T603" id="Seg_7847" s="T602">RUS:mod</ta>
            <ta e="T604" id="Seg_7848" s="T603">TURK:cult</ta>
            <ta e="T619" id="Seg_7849" s="T618">TURK:cult</ta>
            <ta e="T627" id="Seg_7850" s="T626">RUS:gram</ta>
            <ta e="T635" id="Seg_7851" s="T634">RUS:cult</ta>
            <ta e="T647" id="Seg_7852" s="T646">TURK:cult</ta>
            <ta e="T659" id="Seg_7853" s="T658">TURK:cult</ta>
            <ta e="T671" id="Seg_7854" s="T670">RUS:mod</ta>
            <ta e="T693" id="Seg_7855" s="T692">RUS:gram(INDEF)</ta>
            <ta e="T709" id="Seg_7856" s="T708">TURK:core</ta>
            <ta e="T711" id="Seg_7857" s="T710">RUS:mod</ta>
            <ta e="T716" id="Seg_7858" s="T715">RUS:mod</ta>
            <ta e="T720" id="Seg_7859" s="T719">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T9" id="Seg_7860" s="T2">RUS:ext</ta>
            <ta e="T15" id="Seg_7861" s="T14">RUS:ext</ta>
            <ta e="T51" id="Seg_7862" s="T47">RUS:ext</ta>
            <ta e="T64" id="Seg_7863" s="T62">RUS:ext</ta>
            <ta e="T87" id="Seg_7864" s="T86">RUS:int</ta>
            <ta e="T89" id="Seg_7865" s="T88">RUS:ext</ta>
            <ta e="T101" id="Seg_7866" s="T96">RUS:ext</ta>
            <ta e="T111" id="Seg_7867" s="T109">RUS:int</ta>
            <ta e="T126" id="Seg_7868" s="T124">RUS:ext</ta>
            <ta e="T146" id="Seg_7869" s="T145">RUS:ext</ta>
            <ta e="T155" id="Seg_7870" s="T151">RUS:ext</ta>
            <ta e="T165" id="Seg_7871" s="T158">RUS:ext</ta>
            <ta e="T182" id="Seg_7872" s="T175">RUS:ext</ta>
            <ta e="T199" id="Seg_7873" s="T196">RUS:ext</ta>
            <ta e="T220" id="Seg_7874" s="T217">RUS:ext</ta>
            <ta e="T241" id="Seg_7875" s="T239">RUS:ext</ta>
            <ta e="T265" id="Seg_7876" s="T262">RUS:ext</ta>
            <ta e="T296" id="Seg_7877" s="T294">RUS:ext</ta>
            <ta e="T312" id="Seg_7878" s="T309">RUS:ext</ta>
            <ta e="T320" id="Seg_7879" s="T315">RUS:ext</ta>
            <ta e="T338" id="Seg_7880" s="T328">RUS:ext</ta>
            <ta e="T396" id="Seg_7881" s="T395">RUS:ext</ta>
            <ta e="T436" id="Seg_7882" s="T435">RUS:ext</ta>
            <ta e="T507" id="Seg_7883" s="T504">RUS:ext</ta>
            <ta e="T554" id="Seg_7884" s="T552">RUS:ext</ta>
            <ta e="T595" id="Seg_7885" s="T592">RUS:ext</ta>
            <ta e="T612" id="Seg_7886" s="T608">RUS:ext</ta>
            <ta e="T631" id="Seg_7887" s="T629">RUS:ext</ta>
            <ta e="T682" id="Seg_7888" s="T679">RUS:ext</ta>
            <ta e="T688" id="Seg_7889" s="T686">RUS:ext</ta>
            <ta e="T705" id="Seg_7890" s="T703">RUS:ext</ta>
            <ta e="T724" id="Seg_7891" s="T722">RUS:ext</ta>
            <ta e="T739" id="Seg_7892" s="T734">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T9" id="Seg_7893" s="T2">(…) а чего это, на что? </ta>
            <ta e="T15" id="Seg_7894" s="T14">Сюда? </ta>
            <ta e="T19" id="Seg_7895" s="T17">Не ругайся.</ta>
            <ta e="T25" id="Seg_7896" s="T22">Я там была. [?]</ta>
            <ta e="T30" id="Seg_7897" s="T27">Там ругаются.</ta>
            <ta e="T32" id="Seg_7898" s="T30">Я ушла.</ta>
            <ta e="T39" id="Seg_7899" s="T36">Ты много написал.</ta>
            <ta e="T44" id="Seg_7900" s="T40">Толстый начальник пришел.</ta>
            <ta e="T46" id="Seg_7901" s="T44">Пьяный.</ta>
            <ta e="T51" id="Seg_7902" s="T47">(Не?) забыла, как [сказать] "упал". </ta>
            <ta e="T58" id="Seg_7903" s="T56">Мыло есть.</ta>
            <ta e="T61" id="Seg_7904" s="T58">Потри.</ta>
            <ta e="T64" id="Seg_7905" s="T62">"Трись," говорю. </ta>
            <ta e="T76" id="Seg_7906" s="T70">Очень ты грязный, иди в баню.</ta>
            <ta e="T85" id="Seg_7907" s="T77">Там горячая и холодная вода есть, мыло есть.</ta>
            <ta e="T88" id="Seg_7908" s="T85">Мойся хорошо, трись.</ta>
            <ta e="T89" id="Seg_7909" s="T88">Трись. </ta>
            <ta e="T91" id="Seg_7910" s="T89">[?]</ta>
            <ta e="T101" id="Seg_7911" s="T96">Али по-новому говорю? </ta>
            <ta e="T109" id="Seg_7912" s="T107">Коров в загон.</ta>
            <ta e="T113" id="Seg_7913" s="T109">Нет, (?) камасинцы пришли.</ta>
            <ta e="T115" id="Seg_7914" s="T113">Сядь, поешь.</ta>
            <ta e="T118" id="Seg_7915" s="T115">Наверное, есть хочешь.</ta>
            <ta e="T120" id="Seg_7916" s="T118">Где ты был?</ta>
            <ta e="T124" id="Seg_7917" s="T120">Сиди здесь, хороший человек!</ta>
            <ta e="T126" id="Seg_7918" s="T124">Ну хватит. </ta>
            <ta e="T131" id="Seg_7919" s="T128">Надо дом помыть.</ta>
            <ta e="T133" id="Seg_7920" s="T131">Надо подмести.</ta>
            <ta e="T136" id="Seg_7921" s="T133">На улицу выйти.</ta>
            <ta e="T138" id="Seg_7922" s="T136">Корову подоить.</ta>
            <ta e="T140" id="Seg_7923" s="T138">Ребенка помыть.</ta>
            <ta e="T143" id="Seg_7924" s="T141">Хлеб испечь.</ta>
            <ta e="T145" id="Seg_7925" s="T143">Поставить хлеб [= тесто].</ta>
            <ta e="T146" id="Seg_7926" s="T145">Хватит? </ta>
            <ta e="T148" id="Seg_7927" s="T146">…Испечь.</ta>
            <ta e="T151" id="Seg_7928" s="T148">Надо жир топить. [?]</ta>
            <ta e="T155" id="Seg_7929" s="T151">Еще чего-(нибудь). </ta>
            <ta e="T165" id="Seg_7930" s="T158">(Ча-) Чай варить как-то. </ta>
            <ta e="T169" id="Seg_7931" s="T165">Надо чай кипятить.</ta>
            <ta e="T173" id="Seg_7932" s="T169">(Не кричи?), не сердись!</ta>
            <ta e="T175" id="Seg_7933" s="T173">Не шуми! [?]</ta>
            <ta e="T179" id="Seg_7934" s="T175">Еще как-то?</ta>
            <ta e="T181" id="Seg_7935" s="T179">Господи. </ta>
            <ta e="T182" id="Seg_7936" s="T181">Забыла. </ta>
            <ta e="T185" id="Seg_7937" s="T182">Я там была.</ta>
            <ta e="T189" id="Seg_7938" s="T185">Там ссорятся.</ta>
            <ta e="T191" id="Seg_7939" s="T189">Я ушла.</ta>
            <ta e="T196" id="Seg_7940" s="T193">У тебя рука, наверное, болит.</ta>
            <ta e="T199" id="Seg_7941" s="T196">Ну, все наверное. </ta>
            <ta e="T202" id="Seg_7942" s="T200">Соли нет.</ta>
            <ta e="T204" id="Seg_7943" s="T202">Надо посолить.</ta>
            <ta e="T209" id="Seg_7944" s="T204">…чем есть, ложки нет.</ta>
            <ta e="T211" id="Seg_7945" s="T209">Принеси ложку.</ta>
            <ta e="T215" id="Seg_7946" s="T211">Вот ложка, я тебе дала.</ta>
            <ta e="T217" id="Seg_7947" s="T215">Ешь быстрее.</ta>
            <ta e="T220" id="Seg_7948" s="T217">Всё, скорее ешь. </ta>
            <ta e="T223" id="Seg_7949" s="T221">Пойдем (?).</ta>
            <ta e="T224" id="Seg_7950" s="T223">Вместе.</ta>
            <ta e="T227" id="Seg_7951" s="T224">Очень красивые девушки.</ta>
            <ta e="T230" id="Seg_7952" s="T227">Очень красивые парни.</ta>
            <ta e="T233" id="Seg_7953" s="T230">Очень красивые люди.</ta>
            <ta e="T236" id="Seg_7954" s="T233">Очень хороший человек.</ta>
            <ta e="T239" id="Seg_7955" s="T236">Очень лживый человек.</ta>
            <ta e="T243" id="Seg_7956" s="T241">Надо поспать.</ta>
            <ta e="T245" id="Seg_7957" s="T244">Немного.</ta>
            <ta e="T249" id="Seg_7958" s="T245">(…) очень хочется.</ta>
            <ta e="T251" id="Seg_7959" s="T249">Живот у меня болит.</ta>
            <ta e="T253" id="Seg_7960" s="T251">Поешь сядь.</ta>
            <ta e="T257" id="Seg_7961" s="T253">Не кричи, не шуми.</ta>
            <ta e="T260" id="Seg_7962" s="T257">(…) надо быть/взять.</ta>
            <ta e="T262" id="Seg_7963" s="T260">А то…</ta>
            <ta e="T265" id="Seg_7964" s="T262">И еще чего? </ta>
            <ta e="T272" id="Seg_7965" s="T268">В лес пойти, ягод принести.</ta>
            <ta e="T274" id="Seg_7966" s="T272">Орехов принести.</ta>
            <ta e="T277" id="Seg_7967" s="T274">Мы скоро пойдем вдвоем.</ta>
            <ta e="T282" id="Seg_7968" s="T279">Что-нибудь там убью.</ta>
            <ta e="T285" id="Seg_7969" s="T282">Я убил козу.</ta>
            <ta e="T288" id="Seg_7970" s="T286">Я убил соболя.</ta>
            <ta e="T290" id="Seg_7971" s="T288">Я убил белку.</ta>
            <ta e="T292" id="Seg_7972" s="T290">Я убил (оленя?).</ta>
            <ta e="T294" id="Seg_7973" s="T292">Я медведя убил.</ta>
            <ta e="T296" id="Seg_7974" s="T294">Все, хватит. </ta>
            <ta e="T300" id="Seg_7975" s="T296">У меня (?) очень болит.</ta>
            <ta e="T304" id="Seg_7976" s="T300">У меня рука болит, голова болит.</ta>
            <ta e="T306" id="Seg_7977" s="T304">У меня спина болит.</ta>
            <ta e="T308" id="Seg_7978" s="T306">У него ноги болят.</ta>
            <ta e="T312" id="Seg_7979" s="T309">Ладно уже, хватит. </ta>
            <ta e="T320" id="Seg_7980" s="T315">Так я уже позабыла опять. </ta>
            <ta e="T325" id="Seg_7981" s="T320">По большой нужде идти, по малой нужде надо идти.</ta>
            <ta e="T327" id="Seg_7982" s="T325">Надо…</ta>
            <ta e="T338" id="Seg_7983" s="T328">…Старик со старухой, у них были мальчик и девочка. </ta>
            <ta e="T343" id="Seg_7984" s="T338">[Жили-были] мужчина и женщина.</ta>
            <ta e="T347" id="Seg_7985" s="T343">У них дети были.</ta>
            <ta e="T350" id="Seg_7986" s="T347">Мальчик и девочка.</ta>
            <ta e="T359" id="Seg_7987" s="T355">Жили женщина и мужчина.</ta>
            <ta e="T362" id="Seg_7988" s="T359">У них были дети.</ta>
            <ta e="T365" id="Seg_7989" s="T362">Сын и дочь.</ta>
            <ta e="T368" id="Seg_7990" s="T365">Сын и дочь.</ta>
            <ta e="T395" id="Seg_7991" s="T368">Один, два, три, пять, шесть, семь, восемь, девять, десять, одиннадцать, двенадцать, тринадцать, четырнадцать, пятнадцать, шестнадцать, семнадцать, восемнадцать…</ta>
            <ta e="T396" id="Seg_7992" s="T395">Забыла.</ta>
            <ta e="T425" id="Seg_7993" s="T418">Один, два, три, шесть, семь, восемь, девять.</ta>
            <ta e="T431" id="Seg_7994" s="T426">"Мне надо свататься, а к кому?"</ta>
            <ta e="T433" id="Seg_7995" s="T431">Это девушка.</ta>
            <ta e="T435" id="Seg_7996" s="T433">Иди посватай ее!</ta>
            <ta e="T436" id="Seg_7997" s="T435">Всё. </ta>
            <ta e="T440" id="Seg_7998" s="T436">Я живу в доме.</ta>
            <ta e="T446" id="Seg_7999" s="T440">Ой, мои ноги (?), надо…</ta>
            <ta e="T449" id="Seg_8000" s="T447">У меня голова болит.</ta>
            <ta e="T451" id="Seg_8001" s="T449">У меня спина болит.</ta>
            <ta e="T453" id="Seg_8002" s="T451">У меня рука болит.</ta>
            <ta e="T455" id="Seg_8003" s="T453">У меня глаз болит.</ta>
            <ta e="T457" id="Seg_8004" s="T455">Ухо болит.</ta>
            <ta e="T459" id="Seg_8005" s="T457">У меня нога болит.</ta>
            <ta e="T461" id="Seg_8006" s="T459">У меня зад болит.</ta>
            <ta e="T463" id="Seg_8007" s="T462">Нет.</ta>
            <ta e="T469" id="Seg_8008" s="T464">Надо в лес идти, соболя убить.</ta>
            <ta e="T472" id="Seg_8009" s="T469">Медведя надо убить.</ta>
            <ta e="T475" id="Seg_8010" s="T472">Белку надо убить.</ta>
            <ta e="T478" id="Seg_8011" s="T475">Лося надо убить.</ta>
            <ta e="T481" id="Seg_8012" s="T478">Надо убить оленя.</ta>
            <ta e="T484" id="Seg_8013" s="T481">Надо убить козу.</ta>
            <ta e="T489" id="Seg_8014" s="T484">Мяса будет много, есть надо.</ta>
            <ta e="T494" id="Seg_8015" s="T489">Варить(?) надо.</ta>
            <ta e="T502" id="Seg_8016" s="T495">(…) мясо, огонь горит, (?) мясо.</ta>
            <ta e="T507" id="Seg_8017" s="T504">Кого еще раз? </ta>
            <ta e="T517" id="Seg_8018" s="T514">А-а. </ta>
            <ta e="T521" id="Seg_8019" s="T517">Огонь горит, поставь мясо.</ta>
            <ta e="T524" id="Seg_8020" s="T521">Пусть жарится.</ta>
            <ta e="T529" id="Seg_8021" s="T525">Он(а) ставит мясо на огонь.</ta>
            <ta e="T535" id="Seg_8022" s="T530">Сядь сюда, расскажи, где ты был.</ta>
            <ta e="T538" id="Seg_8023" s="T535">Что ты видел?</ta>
            <ta e="T540" id="Seg_8024" s="T538">Куда ты ходил?</ta>
            <ta e="T543" id="Seg_8025" s="T540">Я ходил в лес, за ягодами.</ta>
            <ta e="T545" id="Seg_8026" s="T543">Там медведь.</ta>
            <ta e="T547" id="Seg_8027" s="T545">Я очень боюсь.</ta>
            <ta e="T552" id="Seg_8028" s="T547">Ну не ходи, если боишься.</ta>
            <ta e="T554" id="Seg_8029" s="T552">Ну хватит. </ta>
            <ta e="T562" id="Seg_8030" s="T554">Один, два, три, четыре, пять, семь, семь.</ta>
            <ta e="T571" id="Seg_8031" s="T562">Один, два, три, шесть, пять, восемь, девять, десять.</ta>
            <ta e="T585" id="Seg_8032" s="T573">Одиннадцать, двенадцать, тринадцать, четырнадцать, пятнадцать, семнадцать.</ta>
            <ta e="T590" id="Seg_8033" s="T585">Восемнадцать, семнадцать, десять.</ta>
            <ta e="T592" id="Seg_8034" s="T590">Двадцать</ta>
            <ta e="T595" id="Seg_8035" s="T592">Забыла я…</ta>
            <ta e="T598" id="Seg_8036" s="T595">Надо корову подоить.</ta>
            <ta e="T601" id="Seg_8037" s="T598">Загон закрыть надо.</ta>
            <ta e="T603" id="Seg_8038" s="T601">Подоить надо.</ta>
            <ta e="T605" id="Seg_8039" s="T603">Молоко принести.</ta>
            <ta e="T608" id="Seg_8040" s="T605">В (сосуд?) поместить, перелить.</ta>
            <ta e="T610" id="Seg_8041" s="T608">Ну, все. </ta>
            <ta e="T612" id="Seg_8042" s="T611">Ну? </ta>
            <ta e="T616" id="Seg_8043" s="T614">Куда ты идешь?</ta>
            <ta e="T618" id="Seg_8044" s="T616">Куда ты бежишь?</ta>
            <ta e="T620" id="Seg_8045" s="T618">Коровы (моей?) нет</ta>
            <ta e="T624" id="Seg_8046" s="T621">Пойду [ее] искать.</ta>
            <ta e="T626" id="Seg_8047" s="T624">Пойдем со мной!</ta>
            <ta e="T629" id="Seg_8048" s="T626">А то я боюсь.</ta>
            <ta e="T631" id="Seg_8049" s="T629">Мало, однако. </ta>
            <ta e="T636" id="Seg_8050" s="T634">Здорово!</ta>
            <ta e="T638" id="Seg_8051" s="T636">Садись есть!</ta>
            <ta e="T641" id="Seg_8052" s="T638">Хлеб с черемухой.</ta>
            <ta e="T642" id="Seg_8053" s="T641">Масло.</ta>
            <ta e="T644" id="Seg_8054" s="T642">Ешь мясо.</ta>
            <ta e="T646" id="Seg_8055" s="T644">Ешь ягоды!</ta>
            <ta e="T648" id="Seg_8056" s="T646">Сметану ешь!</ta>
            <ta e="T651" id="Seg_8057" s="T649">Хлеб ешь!</ta>
            <ta e="T654" id="Seg_8058" s="T651">Чай пей.</ta>
            <ta e="T658" id="Seg_8059" s="T654">Сахар положи в чай.</ta>
            <ta e="T660" id="Seg_8060" s="T658">Молоко ешь [=пей].</ta>
            <ta e="T666" id="Seg_8061" s="T661">Пойдем вместе в лес ягоды собирать.</ta>
            <ta e="T670" id="Seg_8062" s="T666">Черемшу рвать, орехи делать [=собирать].</ta>
            <ta e="T672" id="Seg_8063" s="T670">Что еще?</ta>
            <ta e="T678" id="Seg_8064" s="T672">Грибы собирать.</ta>
            <ta e="T682" id="Seg_8065" s="T679">Я тебя жалею.</ta>
            <ta e="T685" id="Seg_8066" s="T682">Я тебя жалею.</ta>
            <ta e="T688" id="Seg_8067" s="T686">Коня запрягай. </ta>
            <ta e="T691" id="Seg_8068" s="T688">Иди коня запрягай.</ta>
            <ta e="T694" id="Seg_8069" s="T691">Поедем куда-нибудь на лошади.</ta>
            <ta e="T697" id="Seg_8070" s="T694">Воду привезем.</ta>
            <ta e="T699" id="Seg_8071" s="T697">Дрова привезем.</ta>
            <ta e="T701" id="Seg_8072" s="T699">Сено привезем.</ta>
            <ta e="T703" id="Seg_8073" s="T701">Солому привезем.</ta>
            <ta e="T705" id="Seg_8074" s="T703">Все, наверное. </ta>
            <ta e="T710" id="Seg_8075" s="T708">Мой родственник приехал.</ta>
            <ta e="T714" id="Seg_8076" s="T710">Надо пойти посмотреть.</ta>
            <ta e="T717" id="Seg_8077" s="T715">Надо позвать.</ta>
            <ta e="T719" id="Seg_8078" s="T717">Накормить.</ta>
            <ta e="T722" id="Seg_8079" s="T719">Им надо спать.</ta>
            <ta e="T724" id="Seg_8080" s="T722">Все, больше [ничего].</ta>
            <ta e="T734" id="Seg_8081" s="T724">Один, один, два, три, четыре, пять, семь, девять, восемь, десять.</ta>
            <ta e="T739" id="Seg_8082" s="T734">Вот девять-то (пропустила?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T9" id="Seg_8083" s="T2">(…) and what is it, for what?</ta>
            <ta e="T15" id="Seg_8084" s="T14">Here?</ta>
            <ta e="T19" id="Seg_8085" s="T17">Don't curse.</ta>
            <ta e="T25" id="Seg_8086" s="T22">I was there. [?]</ta>
            <ta e="T30" id="Seg_8087" s="T27">They are quarrelling there.</ta>
            <ta e="T32" id="Seg_8088" s="T30">I went away.</ta>
            <ta e="T39" id="Seg_8089" s="T36">You wrote a lot.</ta>
            <ta e="T44" id="Seg_8090" s="T40">The fat chief came.</ta>
            <ta e="T46" id="Seg_8091" s="T44">Drunk.</ta>
            <ta e="T51" id="Seg_8092" s="T47">I have (not?) forgotten how [to say] "He fell".</ta>
            <ta e="T58" id="Seg_8093" s="T56">[There] is soap.</ta>
            <ta e="T61" id="Seg_8094" s="T58">Rub!</ta>
            <ta e="T64" id="Seg_8095" s="T62">I say, "rub yourself."</ta>
            <ta e="T76" id="Seg_8096" s="T70">You are very dirty, go to the sauna!</ta>
            <ta e="T85" id="Seg_8097" s="T77">There is warm and cold water, [there] is soap.</ta>
            <ta e="T88" id="Seg_8098" s="T85">Wash well, rub!</ta>
            <ta e="T89" id="Seg_8099" s="T88">Rub yourself.</ta>
            <ta e="T91" id="Seg_8100" s="T89">[?]</ta>
            <ta e="T101" id="Seg_8101" s="T96">Or should I say it anew?</ta>
            <ta e="T109" id="Seg_8102" s="T107">Cows to the corral.</ta>
            <ta e="T113" id="Seg_8103" s="T109">No, (?) the Kamas people came.</ta>
            <ta e="T115" id="Seg_8104" s="T113">Sit, eat!</ta>
            <ta e="T118" id="Seg_8105" s="T115">You must be hungry.</ta>
            <ta e="T120" id="Seg_8106" s="T118">Where were you?</ta>
            <ta e="T124" id="Seg_8107" s="T120">Sit here, good man!</ta>
            <ta e="T126" id="Seg_8108" s="T124">Well, enough!</ta>
            <ta e="T131" id="Seg_8109" s="T128">[I] have to wash the house.</ta>
            <ta e="T133" id="Seg_8110" s="T131">[I] have to sweep.</ta>
            <ta e="T136" id="Seg_8111" s="T133">To go outside.</ta>
            <ta e="T138" id="Seg_8112" s="T136">To milk the cow.</ta>
            <ta e="T140" id="Seg_8113" s="T138">To wash the child.</ta>
            <ta e="T143" id="Seg_8114" s="T141">To bake bread.</ta>
            <ta e="T145" id="Seg_8115" s="T143">To make dough.</ta>
            <ta e="T146" id="Seg_8116" s="T145">Is it enough?</ta>
            <ta e="T148" id="Seg_8117" s="T146">…to bake.</ta>
            <ta e="T151" id="Seg_8118" s="T148">One has to dissolve grease. [?]</ta>
            <ta e="T155" id="Seg_8119" s="T151">Something else.</ta>
            <ta e="T165" id="Seg_8120" s="T158">To make tea somehow.</ta>
            <ta e="T169" id="Seg_8121" s="T165">It is necessary to boil tea.</ta>
            <ta e="T173" id="Seg_8122" s="T169">(Don’t shout?), don’t get angry!</ta>
            <ta e="T175" id="Seg_8123" s="T173">Don’t make noise. [?]</ta>
            <ta e="T179" id="Seg_8124" s="T175">Something else?</ta>
            <ta e="T181" id="Seg_8125" s="T179">Oh dear.</ta>
            <ta e="T182" id="Seg_8126" s="T181">I have forgotten.</ta>
            <ta e="T185" id="Seg_8127" s="T182">I was there.</ta>
            <ta e="T189" id="Seg_8128" s="T185">People are quarrelling there.</ta>
            <ta e="T191" id="Seg_8129" s="T189">I went away.</ta>
            <ta e="T196" id="Seg_8130" s="T193">Your hand must hurt.</ta>
            <ta e="T199" id="Seg_8131" s="T196">Well, that's probably all.</ta>
            <ta e="T202" id="Seg_8132" s="T200">There is no salt.</ta>
            <ta e="T204" id="Seg_8133" s="T202">It is necessary to salt.</ta>
            <ta e="T209" id="Seg_8134" s="T204">…what to eat with, there is no spoon.</ta>
            <ta e="T211" id="Seg_8135" s="T209">Bring a spoon!</ta>
            <ta e="T215" id="Seg_8136" s="T211">Here is a spoon, I gave you.</ta>
            <ta e="T217" id="Seg_8137" s="T215">Eat fast.</ta>
            <ta e="T220" id="Seg_8138" s="T217">That's all, now eat quickly.</ta>
            <ta e="T223" id="Seg_8139" s="T221">Let’s go (?).</ta>
            <ta e="T224" id="Seg_8140" s="T223">Together.</ta>
            <ta e="T227" id="Seg_8141" s="T224">Very beautiful girls.</ta>
            <ta e="T230" id="Seg_8142" s="T227">Very beautiful boys.</ta>
            <ta e="T233" id="Seg_8143" s="T230">Very beautiful people.</ta>
            <ta e="T236" id="Seg_8144" s="T233">Very good man.</ta>
            <ta e="T239" id="Seg_8145" s="T236">Very deceitful man.</ta>
            <ta e="T243" id="Seg_8146" s="T241">[One] has to sleep.</ta>
            <ta e="T245" id="Seg_8147" s="T244">A little.</ta>
            <ta e="T249" id="Seg_8148" s="T245">[I] want to (…) very much.</ta>
            <ta e="T251" id="Seg_8149" s="T249">My stomach hurts.</ta>
            <ta e="T253" id="Seg_8150" s="T251">Sit down to eat!</ta>
            <ta e="T257" id="Seg_8151" s="T253">Don’t shout, don't make noise.</ta>
            <ta e="T260" id="Seg_8152" s="T257">One must be/take (…).</ta>
            <ta e="T262" id="Seg_8153" s="T260">Otherwise…</ta>
            <ta e="T265" id="Seg_8154" s="T262">And what else?</ta>
            <ta e="T272" id="Seg_8155" s="T268">To go to the taiga, to bring berries.</ta>
            <ta e="T274" id="Seg_8156" s="T272">To bring pine nuts.</ta>
            <ta e="T277" id="Seg_8157" s="T274">Let’s go soon, the two of us.</ta>
            <ta e="T282" id="Seg_8158" s="T279">I will kill something there.</ta>
            <ta e="T285" id="Seg_8159" s="T282">I killed a goat.</ta>
            <ta e="T288" id="Seg_8160" s="T286">I killed a sable.</ta>
            <ta e="T290" id="Seg_8161" s="T288">I killed a squirrel.</ta>
            <ta e="T292" id="Seg_8162" s="T290">I killed a (deer?).</ta>
            <ta e="T294" id="Seg_8163" s="T292">I killed a bear.</ta>
            <ta e="T296" id="Seg_8164" s="T294">That's all, enough.</ta>
            <ta e="T300" id="Seg_8165" s="T296">My (?) hurts very much.</ta>
            <ta e="T304" id="Seg_8166" s="T300">My hand hurts, my head hurts.</ta>
            <ta e="T306" id="Seg_8167" s="T304">My back hurts.</ta>
            <ta e="T308" id="Seg_8168" s="T306">His feet hurt.</ta>
            <ta e="T312" id="Seg_8169" s="T309">Okay, that's enough by now.</ta>
            <ta e="T320" id="Seg_8170" s="T315">But I have already forgotten again.</ta>
            <ta e="T325" id="Seg_8171" s="T320">[I] must go to poop, go to piss.</ta>
            <ta e="T327" id="Seg_8172" s="T325">Should…</ta>
            <ta e="T338" id="Seg_8173" s="T328">…An old man and an old woman, they had a boy and a girl.</ta>
            <ta e="T343" id="Seg_8174" s="T338">[There lived] a woman and a man.</ta>
            <ta e="T347" id="Seg_8175" s="T343">They had children.</ta>
            <ta e="T350" id="Seg_8176" s="T347">A boy and a girl.</ta>
            <ta e="T359" id="Seg_8177" s="T355">There lived a woman and a man.</ta>
            <ta e="T362" id="Seg_8178" s="T359">They had children.</ta>
            <ta e="T365" id="Seg_8179" s="T362">A son and a girl.</ta>
            <ta e="T368" id="Seg_8180" s="T365">A son and a girl.</ta>
            <ta e="T395" id="Seg_8181" s="T368">One, two, three, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen…</ta>
            <ta e="T396" id="Seg_8182" s="T395">I forgot.</ta>
            <ta e="T425" id="Seg_8183" s="T418">One, two, three, six, seven, eight, nine.</ta>
            <ta e="T431" id="Seg_8184" s="T426">"I need to marry, but whom?"</ta>
            <ta e="T433" id="Seg_8185" s="T431">This girl.</ta>
            <ta e="T435" id="Seg_8186" s="T433">Go to propose!</ta>
            <ta e="T436" id="Seg_8187" s="T435">That's all.</ta>
            <ta e="T440" id="Seg_8188" s="T436">I am living in [a] house.</ta>
            <ta e="T446" id="Seg_8189" s="T440">Oh, my feet (?), [I] have to…</ta>
            <ta e="T449" id="Seg_8190" s="T447">My head hurts.</ta>
            <ta e="T451" id="Seg_8191" s="T449">My back hurts.</ta>
            <ta e="T453" id="Seg_8192" s="T451">My hand hurts.</ta>
            <ta e="T455" id="Seg_8193" s="T453">My eye hurts.</ta>
            <ta e="T457" id="Seg_8194" s="T455">Ear hurts.</ta>
            <ta e="T459" id="Seg_8195" s="T457">My foot hurts.</ta>
            <ta e="T461" id="Seg_8196" s="T459">My bottom hurts.</ta>
            <ta e="T463" id="Seg_8197" s="T462">No.</ta>
            <ta e="T469" id="Seg_8198" s="T464">[I] must go to the taiga, to kill a sable.</ta>
            <ta e="T472" id="Seg_8199" s="T469">[I] must kill a bear.</ta>
            <ta e="T475" id="Seg_8200" s="T472">[I] must kill a squirrel.</ta>
            <ta e="T478" id="Seg_8201" s="T475">[I] must kill moose.</ta>
            <ta e="T481" id="Seg_8202" s="T478">[I] must kill a deer.</ta>
            <ta e="T484" id="Seg_8203" s="T481">[I] must kill a goat.</ta>
            <ta e="T489" id="Seg_8204" s="T484">There will be a lot of meat, [I] must eat.</ta>
            <ta e="T494" id="Seg_8205" s="T489">[I] must (boil?).</ta>
            <ta e="T502" id="Seg_8206" s="T495">(…) meat, fire is burning, (?) meat.</ta>
            <ta e="T507" id="Seg_8207" s="T504">Whom once again?</ta>
            <ta e="T517" id="Seg_8208" s="T514">Ah well.</ta>
            <ta e="T521" id="Seg_8209" s="T517">The fire is burning, put the meat [on]!</ta>
            <ta e="T524" id="Seg_8210" s="T521">Let it cook.</ta>
            <ta e="T529" id="Seg_8211" s="T525">S/he is putting meat on the fire.</ta>
            <ta e="T535" id="Seg_8212" s="T530">Sit here, tell, where have you been.</ta>
            <ta e="T538" id="Seg_8213" s="T535">What have you seen?</ta>
            <ta e="T540" id="Seg_8214" s="T538">Where are you going?</ta>
            <ta e="T543" id="Seg_8215" s="T540">I'm going to the taiga, to pick berries.</ta>
            <ta e="T545" id="Seg_8216" s="T543">There is a bear.</ta>
            <ta e="T547" id="Seg_8217" s="T545">I am very afraid.</ta>
            <ta e="T552" id="Seg_8218" s="T547">Well, don’t go if you are afraid!</ta>
            <ta e="T554" id="Seg_8219" s="T552">Well, enough.</ta>
            <ta e="T562" id="Seg_8220" s="T554">One, two, three, four, five, seven, seven.</ta>
            <ta e="T571" id="Seg_8221" s="T562">One, two, three, six, seven, five, eight, nine, ten.</ta>
            <ta e="T585" id="Seg_8222" s="T573">Eleven, twelve, thirteen, fourteen, fifteen, seventeen.</ta>
            <ta e="T590" id="Seg_8223" s="T585">Eightteen, seventeen, ten.</ta>
            <ta e="T592" id="Seg_8224" s="T590">Twenty.</ta>
            <ta e="T595" id="Seg_8225" s="T592">I forgot…</ta>
            <ta e="T598" id="Seg_8226" s="T595">[I] must milk the cow.</ta>
            <ta e="T601" id="Seg_8227" s="T598">It is necessary to close its corral.</ta>
            <ta e="T603" id="Seg_8228" s="T601">[I] must milk [it].</ta>
            <ta e="T605" id="Seg_8229" s="T603">To bring milk.</ta>
            <ta e="T608" id="Seg_8230" s="T605">To put it in the vat, to pour.</ta>
            <ta e="T610" id="Seg_8231" s="T608">Well, thst's all.</ta>
            <ta e="T612" id="Seg_8232" s="T611">Well?</ta>
            <ta e="T616" id="Seg_8233" s="T614">Where are you going?</ta>
            <ta e="T618" id="Seg_8234" s="T616">Where are you running?</ta>
            <ta e="T620" id="Seg_8235" s="T618">My (?) cow is not (here?).</ta>
            <ta e="T624" id="Seg_8236" s="T621">I will go to look for [it]?</ta>
            <ta e="T626" id="Seg_8237" s="T624">Let’s go with me!</ta>
            <ta e="T629" id="Seg_8238" s="T626">Because I'm afraid.</ta>
            <ta e="T631" id="Seg_8239" s="T629">Too little, however.</ta>
            <ta e="T636" id="Seg_8240" s="T634">Hello!</ta>
            <ta e="T638" id="Seg_8241" s="T636">Sit down to eat!</ta>
            <ta e="T641" id="Seg_8242" s="T638">Bread with bird cherries.</ta>
            <ta e="T642" id="Seg_8243" s="T641">Butter.</ta>
            <ta e="T644" id="Seg_8244" s="T642">Eat meat!</ta>
            <ta e="T646" id="Seg_8245" s="T644">Eat berries!</ta>
            <ta e="T648" id="Seg_8246" s="T646">Eat cream!</ta>
            <ta e="T651" id="Seg_8247" s="T649">Eat bread!</ta>
            <ta e="T654" id="Seg_8248" s="T651">Drink tea!</ta>
            <ta e="T658" id="Seg_8249" s="T654">Put sugar into the tea!</ta>
            <ta e="T660" id="Seg_8250" s="T658">Eat [=drink] milk!</ta>
            <ta e="T666" id="Seg_8251" s="T661">Let’s go to the taiga together to gather berries.</ta>
            <ta e="T670" id="Seg_8252" s="T666">To pick bear leek, to make [=gather] pine nuts.</ta>
            <ta e="T672" id="Seg_8253" s="T670">What else?</ta>
            <ta e="T678" id="Seg_8254" s="T672">To take [=gather] mushrooms.</ta>
            <ta e="T682" id="Seg_8255" s="T679">I feel sorry for you.</ta>
            <ta e="T685" id="Seg_8256" s="T682">I feel sorry for you.</ta>
            <ta e="T688" id="Seg_8257" s="T686">Harness the horse!</ta>
            <ta e="T691" id="Seg_8258" s="T688">Go, harness the horse!</ta>
            <ta e="T694" id="Seg_8259" s="T691">Let’s go somewhere with the horse.</ta>
            <ta e="T697" id="Seg_8260" s="T694">We will bring water.</ta>
            <ta e="T699" id="Seg_8261" s="T697">We will bring wood.</ta>
            <ta e="T701" id="Seg_8262" s="T699">We will bring hay.</ta>
            <ta e="T703" id="Seg_8263" s="T701">We will bring straw.</ta>
            <ta e="T705" id="Seg_8264" s="T703">That's probably all.</ta>
            <ta e="T710" id="Seg_8265" s="T708">My relative has come [to the village].</ta>
            <ta e="T714" id="Seg_8266" s="T710">[I] must go and see [him]?</ta>
            <ta e="T717" id="Seg_8267" s="T715">[I] must invite [him].</ta>
            <ta e="T719" id="Seg_8268" s="T717">To give [something] to eat.</ta>
            <ta e="T722" id="Seg_8269" s="T719">They must sleep.</ta>
            <ta e="T724" id="Seg_8270" s="T722">Nothing more.</ta>
            <ta e="T734" id="Seg_8271" s="T724">One, one, two, three, four, five, seven, nine, eight, ten.</ta>
            <ta e="T739" id="Seg_8272" s="T734">I (missed?) nine.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T9" id="Seg_8273" s="T2">(…) und was ist das, wozu das?</ta>
            <ta e="T15" id="Seg_8274" s="T14">Hierher?</ta>
            <ta e="T19" id="Seg_8275" s="T17">Schimpf nicht.</ta>
            <ta e="T25" id="Seg_8276" s="T22">Ich war dort. [?]</ta>
            <ta e="T30" id="Seg_8277" s="T27">Dort streiten sie sich.</ta>
            <ta e="T32" id="Seg_8278" s="T30">Ich ging weg.</ta>
            <ta e="T39" id="Seg_8279" s="T36">Du hast viel geschrieben.</ta>
            <ta e="T44" id="Seg_8280" s="T40">Der fette Dorfälteste kam.</ta>
            <ta e="T46" id="Seg_8281" s="T44">Betrunken.</ta>
            <ta e="T51" id="Seg_8282" s="T47">Ich habe (nicht?) vergessen, wie [mann sagt] "Er ist gefallen".</ta>
            <ta e="T54" id="Seg_8283" s="T52">…war.</ta>
            <ta e="T58" id="Seg_8284" s="T56">[Dort] ist Seife.</ta>
            <ta e="T61" id="Seg_8285" s="T58">Rubbel!</ta>
            <ta e="T64" id="Seg_8286" s="T62">"Rubbel", sage ich.</ta>
            <ta e="T76" id="Seg_8287" s="T70">Du bist sehr schmutzig, geh ins Badehaus!</ta>
            <ta e="T85" id="Seg_8288" s="T77">Dort gibt es warmes und kaltes Wasser, [dort] gibt es Seife.</ta>
            <ta e="T88" id="Seg_8289" s="T85">Wasch dich gut, rubbel [dich ab]!</ta>
            <ta e="T89" id="Seg_8290" s="T88">Rubbel [dich ab].</ta>
            <ta e="T91" id="Seg_8291" s="T89">[?]</ta>
            <ta e="T101" id="Seg_8292" s="T96">Oder sage ich das noch mal?</ta>
            <ta e="T109" id="Seg_8293" s="T107">Die Kühe in den Pferch.</ta>
            <ta e="T113" id="Seg_8294" s="T109">Nein, (?) die Kamassen kamen.</ta>
            <ta e="T115" id="Seg_8295" s="T113">Sitz, iss!</ta>
            <ta e="T118" id="Seg_8296" s="T115">Du hast wohl Hunger.</ta>
            <ta e="T120" id="Seg_8297" s="T118">Wo warst du?</ta>
            <ta e="T124" id="Seg_8298" s="T120">Sitz hier, guter Mann!</ta>
            <ta e="T126" id="Seg_8299" s="T124">Nun, genug!</ta>
            <ta e="T131" id="Seg_8300" s="T128">[Ich] muss das Haus putzen.</ta>
            <ta e="T133" id="Seg_8301" s="T131">[Ich] muss fegen.</ta>
            <ta e="T136" id="Seg_8302" s="T133">Hinausgehen.</ta>
            <ta e="T138" id="Seg_8303" s="T136">Die Kuh melken.</ta>
            <ta e="T140" id="Seg_8304" s="T138">Das Kind waschen.</ta>
            <ta e="T143" id="Seg_8305" s="T141">Brot backen.</ta>
            <ta e="T145" id="Seg_8306" s="T143">Teig machen.</ta>
            <ta e="T146" id="Seg_8307" s="T145">Reicht das?</ta>
            <ta e="T148" id="Seg_8308" s="T146">… backen.</ta>
            <ta e="T151" id="Seg_8309" s="T148">Man muss Fett erhitzen. [?]</ta>
            <ta e="T155" id="Seg_8310" s="T151">Noch etwas.</ta>
            <ta e="T165" id="Seg_8311" s="T158">Irgendwie Tee kochen.</ta>
            <ta e="T169" id="Seg_8312" s="T165">Man muss Tee kochen.</ta>
            <ta e="T173" id="Seg_8313" s="T169">(Schrei nicht?), werde nicht böse!</ta>
            <ta e="T175" id="Seg_8314" s="T173">Mach keine Lärm. [?]</ta>
            <ta e="T179" id="Seg_8315" s="T175">Noch irgendwas?</ta>
            <ta e="T181" id="Seg_8316" s="T179">Mein Gott.</ta>
            <ta e="T182" id="Seg_8317" s="T181">Ich habe vergessen.</ta>
            <ta e="T185" id="Seg_8318" s="T182">Ich war dort.</ta>
            <ta e="T189" id="Seg_8319" s="T185">Leute streiten sich dort.</ta>
            <ta e="T191" id="Seg_8320" s="T189">Ich ging weg.</ta>
            <ta e="T196" id="Seg_8321" s="T193">Deine Hand tut wohl weh.</ta>
            <ta e="T199" id="Seg_8322" s="T196">Nun, das ist wohl alles.</ta>
            <ta e="T202" id="Seg_8323" s="T200">Es gibt kein Salz.</ta>
            <ta e="T204" id="Seg_8324" s="T202">Man muss [es] salzen.</ta>
            <ta e="T209" id="Seg_8325" s="T204">…womit zu essen, es gibt keinen Löffel.</ta>
            <ta e="T211" id="Seg_8326" s="T209">Bring einen Löffel!</ta>
            <ta e="T215" id="Seg_8327" s="T211">Hier ist ein Löffel, ich habe ihn dir gegeben.</ta>
            <ta e="T217" id="Seg_8328" s="T215">Iss schnell.</ta>
            <ta e="T220" id="Seg_8329" s="T217">Alles, iss lieber.</ta>
            <ta e="T223" id="Seg_8330" s="T221">Lass uns gehen (?).</ta>
            <ta e="T224" id="Seg_8331" s="T223">Zusammen.</ta>
            <ta e="T227" id="Seg_8332" s="T224">Sehr schöne Mädchen.</ta>
            <ta e="T230" id="Seg_8333" s="T227">Sehr schöne Jungs.</ta>
            <ta e="T233" id="Seg_8334" s="T230">Sehr schöne Leute.</ta>
            <ta e="T236" id="Seg_8335" s="T233">Ein sehr guter Mann.</ta>
            <ta e="T239" id="Seg_8336" s="T236">Ein verlogener Mann.</ta>
            <ta e="T243" id="Seg_8337" s="T241">[Man] muss schlafen.</ta>
            <ta e="T245" id="Seg_8338" s="T244">Ein Bisschen.</ta>
            <ta e="T249" id="Seg_8339" s="T245">[Ich] möchte sehr gerne (…).</ta>
            <ta e="T251" id="Seg_8340" s="T249">Mein Bauch tut weh.</ta>
            <ta e="T253" id="Seg_8341" s="T251">Iss, setz dich.</ta>
            <ta e="T257" id="Seg_8342" s="T253">Schrei nicth, mach keinen Lärm.</ta>
            <ta e="T260" id="Seg_8343" s="T257">Einer muss (…) sein/nehmen.</ta>
            <ta e="T262" id="Seg_8344" s="T260">Sonst…</ta>
            <ta e="T265" id="Seg_8345" s="T262">Und noch was?</ta>
            <ta e="T272" id="Seg_8346" s="T268">In die Taiga gehen, Beeren bringen.</ta>
            <ta e="T274" id="Seg_8347" s="T272">Kiefernnüsse bringen.</ta>
            <ta e="T277" id="Seg_8348" s="T274">Lass uns bald gehen, wir beide.</ta>
            <ta e="T282" id="Seg_8349" s="T279">Ich werde dort etwas töten.</ta>
            <ta e="T285" id="Seg_8350" s="T282">Ich tötete eine Ziege.</ta>
            <ta e="T288" id="Seg_8351" s="T286">Ich tötete einen Zobel.</ta>
            <ta e="T290" id="Seg_8352" s="T288">Ich tötete ein Eichhörnchen.</ta>
            <ta e="T292" id="Seg_8353" s="T290">Ich tötet einen (Hirsch?).</ta>
            <ta e="T294" id="Seg_8354" s="T292">Ich tötete einen Bären.</ta>
            <ta e="T296" id="Seg_8355" s="T294">Das war's, genug!</ta>
            <ta e="T300" id="Seg_8356" s="T296">Mein (?) tut sehr doll weh.</ta>
            <ta e="T304" id="Seg_8357" s="T300">Meine Hand tut weh, mein Kopf tut weh.</ta>
            <ta e="T306" id="Seg_8358" s="T304">Mein Rücken tut weh.</ta>
            <ta e="T308" id="Seg_8359" s="T306">Seine Füße tun weh.</ta>
            <ta e="T312" id="Seg_8360" s="T309">Egal, es reicht schon.</ta>
            <ta e="T320" id="Seg_8361" s="T315">So habe ich schon wieder vergessen.</ta>
            <ta e="T325" id="Seg_8362" s="T320">[Ich] muss kacken gehen, pinkeln gehen.</ta>
            <ta e="T327" id="Seg_8363" s="T325">Soll…</ta>
            <ta e="T338" id="Seg_8364" s="T328">…Ein alter Mann und eine alte Frau, sie hatten einen Jungen und ein Mädchen.</ta>
            <ta e="T343" id="Seg_8365" s="T338">[Es lebten] eine Frau und ein Mann.</ta>
            <ta e="T347" id="Seg_8366" s="T343">Sie hatten Kinder.</ta>
            <ta e="T350" id="Seg_8367" s="T347">Einen Sohn und eine Tochter.</ta>
            <ta e="T359" id="Seg_8368" s="T355">Es lebten eine Frau und ein Mann.</ta>
            <ta e="T362" id="Seg_8369" s="T359">Sie hatten Kinder.</ta>
            <ta e="T365" id="Seg_8370" s="T362">Einen Sohn und eine Tochter.</ta>
            <ta e="T368" id="Seg_8371" s="T365">Einen Sohn und eine Tochter.</ta>
            <ta e="T395" id="Seg_8372" s="T368">Eins, zwei, drei, fünf, sechs, sieben, acht, neun, zehn, elf, zwölf, dreizehn, vierzehn, fünfzehn, sechzehn, siebzehn, achtzehn…</ta>
            <ta e="T396" id="Seg_8373" s="T395">Ich habe vergessen.</ta>
            <ta e="T425" id="Seg_8374" s="T418">Eins, zwei, drei, sechs, sieben, acht, neun.</ta>
            <ta e="T431" id="Seg_8375" s="T426">"Ich muss heiraten, aber wen?"</ta>
            <ta e="T433" id="Seg_8376" s="T431">Dieses Mädchen.</ta>
            <ta e="T435" id="Seg_8377" s="T433">Gehe, um um sie zu werben!</ta>
            <ta e="T436" id="Seg_8378" s="T435">Das war's.</ta>
            <ta e="T440" id="Seg_8379" s="T436">Ich lebe in [einem] Haus.</ta>
            <ta e="T446" id="Seg_8380" s="T440">Oh, meine Füße (?), [ich] muss…</ta>
            <ta e="T449" id="Seg_8381" s="T447">Mein Kopf tut weh.</ta>
            <ta e="T451" id="Seg_8382" s="T449">Mein Rücken tut weh.</ta>
            <ta e="T453" id="Seg_8383" s="T451">Meine Hand tut weh.</ta>
            <ta e="T455" id="Seg_8384" s="T453">Meine Augen tun weh.</ta>
            <ta e="T457" id="Seg_8385" s="T455">Ohren tun weh.</ta>
            <ta e="T459" id="Seg_8386" s="T457">Meine Füße tun weh.</ta>
            <ta e="T461" id="Seg_8387" s="T459">Mein Hintern tut weh.</ta>
            <ta e="T463" id="Seg_8388" s="T462">Nein.</ta>
            <ta e="T469" id="Seg_8389" s="T464">[Ich] muss in die Taiga gehen, um einen Zobel zu töten.</ta>
            <ta e="T472" id="Seg_8390" s="T469">[Ich] muss einen Bären töten.</ta>
            <ta e="T475" id="Seg_8391" s="T472">[Ich] muss ein Eichhörnchen töten.</ta>
            <ta e="T478" id="Seg_8392" s="T475">[Ich] muss einen Elch töten.</ta>
            <ta e="T481" id="Seg_8393" s="T478">[Ich] muss einen Hirsch töten.</ta>
            <ta e="T484" id="Seg_8394" s="T481">[Ich] muss eine Ziege töten.</ta>
            <ta e="T489" id="Seg_8395" s="T484">Es wird viel Fleisch geben, [ich] muss es essen.</ta>
            <ta e="T494" id="Seg_8396" s="T489">[Ich] muss (kochen?).</ta>
            <ta e="T502" id="Seg_8397" s="T495">(…) Fleisch, es brennt Feuer, (?) Fleisch.</ta>
            <ta e="T507" id="Seg_8398" s="T504">Wer noch mal?</ta>
            <ta e="T517" id="Seg_8399" s="T514">Ach so.</ta>
            <ta e="T521" id="Seg_8400" s="T517">Das Feuer brennt, tu das Fleisch [darauf]!</ta>
            <ta e="T524" id="Seg_8401" s="T521">Lass es kochen.</ta>
            <ta e="T529" id="Seg_8402" s="T525">Er/sie tut Fleisch auf das Feuer.</ta>
            <ta e="T535" id="Seg_8403" s="T530">Sitz hier, erzähl, wo du warst.</ta>
            <ta e="T538" id="Seg_8404" s="T535">Was hast du gesehen?</ta>
            <ta e="T540" id="Seg_8405" s="T538">Wo gehst du hin?</ta>
            <ta e="T543" id="Seg_8406" s="T540">Ich gehe in die Taiga, um Beeren zu pflücken.</ta>
            <ta e="T545" id="Seg_8407" s="T543">Dort ist ein Bär.</ta>
            <ta e="T547" id="Seg_8408" s="T545">Ich habe sehr viel Angst.</ta>
            <ta e="T552" id="Seg_8409" s="T547">Nun, geh nicht, wenn du Angst hast!</ta>
            <ta e="T554" id="Seg_8410" s="T552">Nun, genug.</ta>
            <ta e="T562" id="Seg_8411" s="T554">Eins, zwei, drei, vier, fünf, sieben, sieben.</ta>
            <ta e="T571" id="Seg_8412" s="T562">Eins, zwei, drei, sechs, sieben, fünf, acht, neun, zehn.</ta>
            <ta e="T585" id="Seg_8413" s="T573">Elf, zwölf, dreizehn, vierzehn, fünfzehn, siebzehn.</ta>
            <ta e="T590" id="Seg_8414" s="T585">Achtzehn, siebzehn, zehn.</ta>
            <ta e="T592" id="Seg_8415" s="T590">Zwanzig.</ta>
            <ta e="T595" id="Seg_8416" s="T592">Ich habe vergessen…</ta>
            <ta e="T598" id="Seg_8417" s="T595">[Ich] muss die Kuh melken.</ta>
            <ta e="T601" id="Seg_8418" s="T598">Man muss den Pferch schließen.</ta>
            <ta e="T603" id="Seg_8419" s="T601">[Ich] muss [sie] melken.</ta>
            <ta e="T605" id="Seg_8420" s="T603">Milch bringen.</ta>
            <ta e="T608" id="Seg_8421" s="T605">Sie in das Birkenrindengefäß gießen.</ta>
            <ta e="T610" id="Seg_8422" s="T608">Nun, alles.</ta>
            <ta e="T612" id="Seg_8423" s="T611">Nun?</ta>
            <ta e="T616" id="Seg_8424" s="T614">Wohin gehst du?</ta>
            <ta e="T618" id="Seg_8425" s="T616">Wohin läufst du?</ta>
            <ta e="T620" id="Seg_8426" s="T618">Meine (?) Kuh ist nicht (hier?).</ta>
            <ta e="T624" id="Seg_8427" s="T621">Ich gehe, um [sie] zu suchen.</ta>
            <ta e="T626" id="Seg_8428" s="T624">Lass uns zusammen gehen!</ta>
            <ta e="T629" id="Seg_8429" s="T626">Weil ich Angst habe.</ta>
            <ta e="T631" id="Seg_8430" s="T629">Ein Bisschen dennoch.</ta>
            <ta e="T636" id="Seg_8431" s="T634">Hallo!</ta>
            <ta e="T638" id="Seg_8432" s="T636">Setz dich, um zu essen!</ta>
            <ta e="T641" id="Seg_8433" s="T638">Brot mit Traubenkirschen.</ta>
            <ta e="T642" id="Seg_8434" s="T641">Butter.</ta>
            <ta e="T644" id="Seg_8435" s="T642">Iss Fleisch!</ta>
            <ta e="T646" id="Seg_8436" s="T644">Iss Beeren!</ta>
            <ta e="T648" id="Seg_8437" s="T646">Iss Sauerrahm!</ta>
            <ta e="T651" id="Seg_8438" s="T649">Iss Brot!</ta>
            <ta e="T654" id="Seg_8439" s="T651">Trink Tee!</ta>
            <ta e="T658" id="Seg_8440" s="T654">Tu Zucker in den Tee!</ta>
            <ta e="T660" id="Seg_8441" s="T658">Iss [=trink] Milch!</ta>
            <ta e="T666" id="Seg_8442" s="T661">Lass uns zusammen in die Taiga gehen, um Beeren zu sammeln.</ta>
            <ta e="T670" id="Seg_8443" s="T666">Um Bärlauch zu sammeln, um Kiefernnüsse zu machen [= zu sammeln].</ta>
            <ta e="T672" id="Seg_8444" s="T670">Was noch?</ta>
            <ta e="T678" id="Seg_8445" s="T672">Um Pilze zu nehmen.</ta>
            <ta e="T682" id="Seg_8446" s="T679">Ich bemitleide dich.</ta>
            <ta e="T685" id="Seg_8447" s="T682">Ich bemitleide dich.</ta>
            <ta e="T688" id="Seg_8448" s="T686">Spann das Pferd an.</ta>
            <ta e="T691" id="Seg_8449" s="T688">Geh, spann das Pferd an!</ta>
            <ta e="T694" id="Seg_8450" s="T691">Lass uns mit dem Pferd irgendwohin gehen.</ta>
            <ta e="T697" id="Seg_8451" s="T694">Wir werden Wasser bringen.</ta>
            <ta e="T699" id="Seg_8452" s="T697">Wir werden Holz bringen.</ta>
            <ta e="T701" id="Seg_8453" s="T699">Wir werden Heu bringen.</ta>
            <ta e="T703" id="Seg_8454" s="T701">Wir werden Stroh bringen.</ta>
            <ta e="T705" id="Seg_8455" s="T703">Wohl alles.</ta>
            <ta e="T710" id="Seg_8456" s="T708">Mein Verwandter kam [ins Dorf].</ta>
            <ta e="T714" id="Seg_8457" s="T710">[Ich] muss gehen und [ihn] sehen.</ta>
            <ta e="T717" id="Seg_8458" s="T715">[Ich] muss [ihn] einladen.</ta>
            <ta e="T719" id="Seg_8459" s="T717">Ihm [etwas] zu essen geben.</ta>
            <ta e="T722" id="Seg_8460" s="T719">Sie müssen schlafen.</ta>
            <ta e="T724" id="Seg_8461" s="T722">Nichts mehr.</ta>
            <ta e="T734" id="Seg_8462" s="T724">Eins, eins, zwei, drei, vier, fünf, sieben, neun, acht, zehn.</ta>
            <ta e="T739" id="Seg_8463" s="T734">Ich habe neun (ausgelassen?).</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T19" id="Seg_8464" s="T17">[GVY:] or pu-?</ta>
            <ta e="T25" id="Seg_8465" s="T22">[GVY:] If this sentence is a translation of the preceding one, it should have dĭn instead of dĭm; as a correct sentence, it would mean 'I took it'.</ta>
            <ta e="T30" id="Seg_8466" s="T27">[GVY:] kudonzaʔbəjə</ta>
            <ta e="T51" id="Seg_8467" s="T47">[AAV] whispering</ta>
            <ta e="T124" id="Seg_8468" s="T120">[GVY:] Or 'handsome man'.</ta>
            <ta e="T131" id="Seg_8469" s="T128">[GVY:] băzəjdəsʼtə?</ta>
            <ta e="T175" id="Seg_8470" s="T173">[AAV] jalomaʔ</ta>
            <ta e="T223" id="Seg_8471" s="T221">[GVY:] kolajla 'to catch fish', keʔbdejle 'to gather berries'</ta>
            <ta e="T249" id="Seg_8472" s="T245">[GVY:] amzittə?</ta>
            <ta e="T257" id="Seg_8473" s="T253">Jalom- ’make noise’? Janurik.</ta>
            <ta e="T282" id="Seg_8474" s="T279">[KlT:] dĭm = dĭn.</ta>
            <ta e="T290" id="Seg_8475" s="T288">Doublecheck the word form; [AAV] -pp- geminate.</ta>
            <ta e="T292" id="Seg_8476" s="T290">[GVY:] Söm?</ta>
            <ta e="T362" id="Seg_8477" s="T359">[GVY:] ešseŋdə.</ta>
            <ta e="T431" id="Seg_8478" s="T426">[AAV] -kk- geminate.</ta>
            <ta e="T435" id="Seg_8479" s="T433">[AAV] -kk- geminate.</ta>
            <ta e="T436" id="Seg_8480" s="T435">[AAV] whispering</ta>
            <ta e="T494" id="Seg_8481" s="T489">[KlT:] She probably intends to say ’mĭnzərzittə’.</ta>
            <ta e="T592" id="Seg_8482" s="T590">[GVY:] biʔ</ta>
            <ta e="T624" id="Seg_8483" s="T621">Künnap/Plotnikova materials ”tüžöj măndərzittə šonə kallam” - "Коров искать пойду.".</ta>
            <ta e="T641" id="Seg_8484" s="T638">[GVY:] len</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-MAK"
                      id="tx-MAK"
                      speaker="MAK"
                      type="t">
         <timeline-fork end="T17" start="T15">
            <tli id="T15.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T22" start="T19">
            <tli id="T19.tx-MAK.1" />
            <tli id="T19.tx-MAK.2" />
         </timeline-fork>
         <timeline-fork end="T27" start="T25">
            <tli id="T25.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T36" start="T33">
            <tli id="T33.tx-MAK.1" />
            <tli id="T33.tx-MAK.2" />
         </timeline-fork>
         <timeline-fork end="T56" start="T53">
            <tli id="T53.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T70" start="T64">
            <tli id="T64.tx-MAK.1" />
            <tli id="T64.tx-MAK.2" />
            <tli id="T64.tx-MAK.3" />
            <tli id="T64.tx-MAK.4" />
            <tli id="T64.tx-MAK.5" />
         </timeline-fork>
         <timeline-fork end="T95" start="T91">
            <tli id="T91.tx-MAK.1" />
            <tli id="T91.tx-MAK.2" />
            <tli id="T91.tx-MAK.3" />
         </timeline-fork>
         <timeline-fork end="T107" start="T101">
            <tli id="T101.tx-MAK.1" />
            <tli id="T101.tx-MAK.2" />
            <tli id="T101.tx-MAK.3" />
            <tli id="T101.tx-MAK.4" />
            <tli id="T101.tx-MAK.5" />
         </timeline-fork>
         <timeline-fork end="T268" start="T265">
            <tli id="T265.tx-MAK.1" />
            <tli id="T265.tx-MAK.2" />
         </timeline-fork>
         <timeline-fork end="T418" start="T413">
            <tli id="T413.tx-MAK.1" />
            <tli id="T413.tx-MAK.2" />
            <tli id="T413.tx-MAK.3" />
            <tli id="T413.tx-MAK.4" />
         </timeline-fork>
         <timeline-fork end="T495" start="T494">
            <tli id="T494.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T504" start="T502">
            <tli id="T502.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T514" start="T507">
            <tli id="T507.tx-MAK.1" />
            <tli id="T507.tx-MAK.2" />
            <tli id="T507.tx-MAK.3" />
            <tli id="T507.tx-MAK.4" />
            <tli id="T507.tx-MAK.5" />
            <tli id="T507.tx-MAK.6" />
         </timeline-fork>
         <timeline-fork end="T573" start="T571">
            <tli id="T571.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T614" start="T612">
            <tli id="T612.tx-MAK.1" />
         </timeline-fork>
         <timeline-fork end="T708" start="T706">
            <tli id="T706.tx-MAK.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-MAK">
            <ts e="T14" id="Seg_8485" n="sc" s="T9">
               <ts e="T14" id="Seg_8487" n="HIAT:u" s="T9">
                  <ts e="T14" id="Seg_8489" n="HIAT:w" s="T9">Говори-говори-говори</ts>
                  <nts id="Seg_8490" n="HIAT:ip">!</nts>
                  <nts id="Seg_8491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_8492" n="sc" s="T15">
               <ts e="T17" id="Seg_8494" n="HIAT:u" s="T15">
                  <ts e="T15.tx-MAK.1" id="Seg_8496" n="HIAT:w" s="T15">Ну</ts>
                  <nts id="Seg_8497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_8499" n="HIAT:w" s="T15.tx-MAK.1">да</ts>
                  <nts id="Seg_8500" n="HIAT:ip">!</nts>
                  <nts id="Seg_8501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_8502" n="sc" s="T19">
               <ts e="T22" id="Seg_8504" n="HIAT:u" s="T19">
                  <ts e="T19.tx-MAK.1" id="Seg_8506" n="HIAT:w" s="T19">Я</ts>
                  <nts id="Seg_8507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19.tx-MAK.2" id="Seg_8509" n="HIAT:w" s="T19.tx-MAK.1">там</ts>
                  <nts id="Seg_8510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_8512" n="HIAT:w" s="T19.tx-MAK.2">была</ts>
                  <nts id="Seg_8513" n="HIAT:ip">!</nts>
                  <nts id="Seg_8514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_8515" n="sc" s="T25">
               <ts e="T27" id="Seg_8517" n="HIAT:u" s="T25">
                  <ts e="T25.tx-MAK.1" id="Seg_8519" n="HIAT:w" s="T25">Там</ts>
                  <nts id="Seg_8520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_8522" n="HIAT:w" s="T25.tx-MAK.1">ругаются</ts>
                  <nts id="Seg_8523" n="HIAT:ip">.</nts>
                  <nts id="Seg_8524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_8525" n="sc" s="T32">
               <ts e="T33" id="Seg_8527" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_8529" n="HIAT:w" s="T32">Ага</ts>
                  <nts id="Seg_8530" n="HIAT:ip">!</nts>
                  <nts id="Seg_8531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_8533" n="HIAT:u" s="T33">
                  <ts e="T33.tx-MAK.1" id="Seg_8535" n="HIAT:w" s="T33">Много</ts>
                  <nts id="Seg_8536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33.tx-MAK.2" id="Seg_8538" n="HIAT:w" s="T33.tx-MAK.1">поди</ts>
                  <nts id="Seg_8539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_8541" n="HIAT:w" s="T33.tx-MAK.2">написал</ts>
                  <nts id="Seg_8542" n="HIAT:ip">!</nts>
                  <nts id="Seg_8543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_8544" n="sc" s="T46">
               <ts e="T47" id="Seg_8546" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_8548" n="HIAT:w" s="T46">Упал</ts>
                  <nts id="Seg_8549" n="HIAT:ip">.</nts>
                  <nts id="Seg_8550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_8551" n="sc" s="T51">
               <ts e="T52" id="Seg_8553" n="HIAT:u" s="T51">
                  <nts id="Seg_8554" n="HIAT:ip">(</nts>
                  <nts id="Seg_8555" n="HIAT:ip">(</nts>
                  <ats e="T52" id="Seg_8556" n="HIAT:non-pho" s="T51">…</ats>
                  <nts id="Seg_8557" n="HIAT:ip">)</nts>
                  <nts id="Seg_8558" n="HIAT:ip">)</nts>
                  <nts id="Seg_8559" n="HIAT:ip">.</nts>
                  <nts id="Seg_8560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_8561" n="sc" s="T53">
               <ts e="T56" id="Seg_8563" n="HIAT:u" s="T53">
                  <ts e="T53.tx-MAK.1" id="Seg_8565" n="HIAT:w" s="T53">О</ts>
                  <nts id="Seg_8566" n="HIAT:ip">,</nts>
                  <nts id="Seg_8567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_8569" n="HIAT:w" s="T53.tx-MAK.1">давай</ts>
                  <nts id="Seg_8570" n="HIAT:ip">.</nts>
                  <nts id="Seg_8571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_8572" n="sc" s="T58">
               <ts e="T59" id="Seg_8574" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_8576" n="HIAT:w" s="T58">Сначала</ts>
                  <nts id="Seg_8577" n="HIAT:ip">.</nts>
                  <nts id="Seg_8578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_8579" n="sc" s="T61">
               <ts e="T62" id="Seg_8581" n="HIAT:u" s="T61">
                  <nts id="Seg_8582" n="HIAT:ip">(</nts>
                  <nts id="Seg_8583" n="HIAT:ip">(</nts>
                  <ats e="T62" id="Seg_8584" n="HIAT:non-pho" s="T61">LAUGH</ats>
                  <nts id="Seg_8585" n="HIAT:ip">)</nts>
                  <nts id="Seg_8586" n="HIAT:ip">)</nts>
                  <nts id="Seg_8587" n="HIAT:ip">.</nts>
                  <nts id="Seg_8588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T70" id="Seg_8589" n="sc" s="T64">
               <ts e="T70" id="Seg_8591" n="HIAT:u" s="T64">
                  <ts e="T64.tx-MAK.1" id="Seg_8593" n="HIAT:w" s="T64">Сначала</ts>
                  <nts id="Seg_8594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-MAK.2" id="Seg_8596" n="HIAT:w" s="T64.tx-MAK.1">говорите</ts>
                  <nts id="Seg_8597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-MAK.3" id="Seg_8599" n="HIAT:w" s="T64.tx-MAK.2">все</ts>
                  <nts id="Seg_8600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-MAK.4" id="Seg_8602" n="HIAT:w" s="T64.tx-MAK.3">это</ts>
                  <nts id="Seg_8603" n="HIAT:ip">,</nts>
                  <nts id="Seg_8604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64.tx-MAK.5" id="Seg_8606" n="HIAT:w" s="T64.tx-MAK.4">с</ts>
                  <nts id="Seg_8607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_8609" n="HIAT:w" s="T64.tx-MAK.5">самого</ts>
                  <nts id="Seg_8610" n="HIAT:ip">.</nts>
                  <nts id="Seg_8611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T96" id="Seg_8612" n="sc" s="T91">
               <ts e="T95" id="Seg_8614" n="HIAT:u" s="T91">
                  <ts e="T91.tx-MAK.1" id="Seg_8616" n="HIAT:w" s="T91">Еще</ts>
                  <nts id="Seg_8617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91.tx-MAK.2" id="Seg_8619" n="HIAT:w" s="T91.tx-MAK.1">скажи</ts>
                  <nts id="Seg_8620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91.tx-MAK.3" id="Seg_8622" n="HIAT:w" s="T91.tx-MAK.2">это</ts>
                  <nts id="Seg_8623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_8625" n="HIAT:w" s="T91.tx-MAK.3">все</ts>
                  <nts id="Seg_8626" n="HIAT:ip">.</nts>
                  <nts id="Seg_8627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_8629" n="HIAT:u" s="T95">
                  <nts id="Seg_8630" n="HIAT:ip">(</nts>
                  <nts id="Seg_8631" n="HIAT:ip">(</nts>
                  <ats e="T96" id="Seg_8632" n="HIAT:non-pho" s="T95">…</ats>
                  <nts id="Seg_8633" n="HIAT:ip">)</nts>
                  <nts id="Seg_8634" n="HIAT:ip">)</nts>
                  <nts id="Seg_8635" n="HIAT:ip">.</nts>
                  <nts id="Seg_8636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_8637" n="sc" s="T101">
               <ts e="T107" id="Seg_8639" n="HIAT:u" s="T101">
                  <ts e="T101.tx-MAK.1" id="Seg_8641" n="HIAT:w" s="T101">Ну</ts>
                  <nts id="Seg_8642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101.tx-MAK.2" id="Seg_8644" n="HIAT:w" s="T101.tx-MAK.1">да</ts>
                  <nts id="Seg_8645" n="HIAT:ip">,</nts>
                  <nts id="Seg_8646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101.tx-MAK.3" id="Seg_8648" n="HIAT:w" s="T101.tx-MAK.2">снова</ts>
                  <nts id="Seg_8649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101.tx-MAK.4" id="Seg_8651" n="HIAT:w" s="T101.tx-MAK.3">говорите</ts>
                  <nts id="Seg_8652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101.tx-MAK.5" id="Seg_8654" n="HIAT:w" s="T101.tx-MAK.4">все</ts>
                  <nts id="Seg_8655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_8657" n="HIAT:w" s="T101.tx-MAK.5">это</ts>
                  <nts id="Seg_8658" n="HIAT:ip">.</nts>
                  <nts id="Seg_8659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T244" id="Seg_8660" n="sc" s="T243">
               <ts e="T244" id="Seg_8662" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_8664" n="HIAT:w" s="T243">Маленечко</ts>
                  <nts id="Seg_8665" n="HIAT:ip">.</nts>
                  <nts id="Seg_8666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T268" id="Seg_8667" n="sc" s="T265">
               <ts e="T268" id="Seg_8669" n="HIAT:u" s="T265">
                  <ts e="T265.tx-MAK.1" id="Seg_8671" n="HIAT:w" s="T265">А</ts>
                  <nts id="Seg_8672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265.tx-MAK.2" id="Seg_8674" n="HIAT:w" s="T265.tx-MAK.1">теперь</ts>
                  <nts id="Seg_8675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_8677" n="HIAT:w" s="T265.tx-MAK.2">сначала</ts>
                  <nts id="Seg_8678" n="HIAT:ip">.</nts>
                  <nts id="Seg_8679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T315" id="Seg_8680" n="sc" s="T312">
               <ts e="T315" id="Seg_8682" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_8684" n="HIAT:w" s="T312">Ну</ts>
                  <nts id="Seg_8685" n="HIAT:ip">,</nts>
                  <nts id="Seg_8686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_8688" n="HIAT:w" s="T313">скажи</ts>
                  <nts id="Seg_8689" n="HIAT:ip">,</nts>
                  <nts id="Seg_8690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_8692" n="HIAT:w" s="T314">бабушка</ts>
                  <nts id="Seg_8693" n="HIAT:ip">.</nts>
                  <nts id="Seg_8694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T355" id="Seg_8695" n="sc" s="T350">
               <ts e="T355" id="Seg_8697" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_8699" n="HIAT:w" s="T350">Повторите</ts>
                  <nts id="Seg_8700" n="HIAT:ip">,</nts>
                  <nts id="Seg_8701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_8703" n="HIAT:w" s="T351">бабушка</ts>
                  <nts id="Seg_8704" n="HIAT:ip">,</nts>
                  <nts id="Seg_8705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_8707" n="HIAT:w" s="T352">еще</ts>
                  <nts id="Seg_8708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_8710" n="HIAT:w" s="T353">снова</ts>
                  <nts id="Seg_8711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_8713" n="HIAT:w" s="T354">все</ts>
                  <nts id="Seg_8714" n="HIAT:ip">.</nts>
                  <nts id="Seg_8715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T418" id="Seg_8716" n="sc" s="T396">
               <ts e="T413" id="Seg_8718" n="HIAT:u" s="T396">
                  <ts e="T398" id="Seg_8720" n="HIAT:w" s="T396">В</ts>
                  <nts id="Seg_8721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_8723" n="HIAT:w" s="T398">прошлом</ts>
                  <nts id="Seg_8724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_8726" n="HIAT:w" s="T399">году</ts>
                  <nts id="Seg_8727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_8729" n="HIAT:w" s="T400">с</ts>
                  <nts id="Seg_8730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_8732" n="HIAT:w" s="T401">трудом</ts>
                  <nts id="Seg_8733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_8735" n="HIAT:w" s="T402">до</ts>
                  <nts id="Seg_8736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_8738" n="HIAT:w" s="T403">десяти</ts>
                  <nts id="Seg_8739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_8741" n="HIAT:w" s="T404">досчитали</ts>
                  <nts id="Seg_8742" n="HIAT:ip">,</nts>
                  <nts id="Seg_8743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_8745" n="HIAT:w" s="T405">а</ts>
                  <nts id="Seg_8746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_8748" n="HIAT:w" s="T406">в</ts>
                  <nts id="Seg_8749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_8751" n="HIAT:w" s="T407">этом</ts>
                  <nts id="Seg_8752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_8754" n="HIAT:w" s="T408">году</ts>
                  <nts id="Seg_8755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_8757" n="HIAT:w" s="T409">до</ts>
                  <nts id="Seg_8758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_8760" n="HIAT:w" s="T410">двадцати</ts>
                  <nts id="Seg_8761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_8763" n="HIAT:w" s="T411">запросто</ts>
                  <nts id="Seg_8764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8765" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_8767" n="HIAT:w" s="T412">считаете</ts>
                  <nts id="Seg_8768" n="HIAT:ip">)</nts>
                  <nts id="Seg_8769" n="HIAT:ip">.</nts>
                  <nts id="Seg_8770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_8772" n="HIAT:u" s="T413">
                  <ts e="T413.tx-MAK.1" id="Seg_8774" n="HIAT:w" s="T413">На</ts>
                  <nts id="Seg_8775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413.tx-MAK.2" id="Seg_8777" n="HIAT:w" s="T413.tx-MAK.1">будущий</ts>
                  <nts id="Seg_8778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413.tx-MAK.3" id="Seg_8780" n="HIAT:w" s="T413.tx-MAK.2">год</ts>
                  <nts id="Seg_8781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413.tx-MAK.4" id="Seg_8783" n="HIAT:w" s="T413.tx-MAK.3">до</ts>
                  <nts id="Seg_8784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_8786" n="HIAT:w" s="T413.tx-MAK.4">ста</ts>
                  <nts id="Seg_8787" n="HIAT:ip">.</nts>
                  <nts id="Seg_8788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T495" id="Seg_8789" n="sc" s="T494">
               <ts e="T495" id="Seg_8791" n="HIAT:u" s="T494">
                  <nts id="Seg_8792" n="HIAT:ip">(</nts>
                  <ts e="T494.tx-MAK.1" id="Seg_8794" n="HIAT:w" s="T494">Клади</ts>
                  <nts id="Seg_8795" n="HIAT:ip">)</nts>
                  <nts id="Seg_8796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_8798" n="HIAT:w" s="T494.tx-MAK.1">мясо</ts>
                  <nts id="Seg_8799" n="HIAT:ip">.</nts>
                  <nts id="Seg_8800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T504" id="Seg_8801" n="sc" s="T502">
               <ts e="T504" id="Seg_8803" n="HIAT:u" s="T502">
                  <ts e="T502.tx-MAK.1" id="Seg_8805" n="HIAT:w" s="T502">Еще</ts>
                  <nts id="Seg_8806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_8808" n="HIAT:w" s="T502.tx-MAK.1">раз</ts>
                  <nts id="Seg_8809" n="HIAT:ip">.</nts>
                  <nts id="Seg_8810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T514" id="Seg_8811" n="sc" s="T507">
               <ts e="T514" id="Seg_8813" n="HIAT:u" s="T507">
                  <ts e="T507.tx-MAK.1" id="Seg_8815" n="HIAT:w" s="T507">А</ts>
                  <nts id="Seg_8816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507.tx-MAK.2" id="Seg_8818" n="HIAT:w" s="T507.tx-MAK.1">вот</ts>
                  <nts id="Seg_8819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507.tx-MAK.3" id="Seg_8821" n="HIAT:w" s="T507.tx-MAK.2">скажите</ts>
                  <nts id="Seg_8822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507.tx-MAK.4" id="Seg_8824" n="HIAT:w" s="T507.tx-MAK.3">еще</ts>
                  <nts id="Seg_8825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507.tx-MAK.5" id="Seg_8827" n="HIAT:w" s="T507.tx-MAK.4">раз</ts>
                  <nts id="Seg_8828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507.tx-MAK.6" id="Seg_8830" n="HIAT:w" s="T507.tx-MAK.5">это</ts>
                  <nts id="Seg_8831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_8833" n="HIAT:w" s="T507.tx-MAK.6">же</ts>
                  <nts id="Seg_8834" n="HIAT:ip">.</nts>
                  <nts id="Seg_8835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T573" id="Seg_8836" n="sc" s="T571">
               <ts e="T573" id="Seg_8838" n="HIAT:u" s="T571">
                  <ts e="T571.tx-MAK.1" id="Seg_8840" n="HIAT:w" s="T571">Еще</ts>
                  <nts id="Seg_8841" n="HIAT:ip">,</nts>
                  <nts id="Seg_8842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_8844" n="HIAT:w" s="T571.tx-MAK.1">еще</ts>
                  <nts id="Seg_8845" n="HIAT:ip">.</nts>
                  <nts id="Seg_8846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T614" id="Seg_8847" n="sc" s="T612">
               <ts e="T614" id="Seg_8849" n="HIAT:u" s="T612">
                  <ts e="T612.tx-MAK.1" id="Seg_8851" n="HIAT:w" s="T612">Куда</ts>
                  <nts id="Seg_8852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_8854" n="HIAT:w" s="T612.tx-MAK.1">бежишь</ts>
                  <nts id="Seg_8855" n="HIAT:ip">?</nts>
                  <nts id="Seg_8856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T621" id="Seg_8857" n="sc" s="T620">
               <ts e="T621" id="Seg_8859" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_8861" n="HIAT:w" s="T620">Искать</ts>
                  <nts id="Seg_8862" n="HIAT:ip">.</nts>
                  <nts id="Seg_8863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_8864" n="sc" s="T631">
               <ts e="T632" id="Seg_8866" n="HIAT:u" s="T631">
                  <nts id="Seg_8867" n="HIAT:ip">(</nts>
                  <nts id="Seg_8868" n="HIAT:ip">(</nts>
                  <ats e="T632" id="Seg_8869" n="HIAT:non-pho" s="T631">BRK</ats>
                  <nts id="Seg_8870" n="HIAT:ip">)</nts>
                  <nts id="Seg_8871" n="HIAT:ip">)</nts>
                  <nts id="Seg_8872" n="HIAT:ip">.</nts>
                  <nts id="Seg_8873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_8875" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_8877" n="HIAT:w" s="T632">Zdărowă</ts>
                  <nts id="Seg_8878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_8880" n="HIAT:w" s="T633">igel</ts>
                  <nts id="Seg_8881" n="HIAT:ip">!</nts>
                  <nts id="Seg_8882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T708" id="Seg_8883" n="sc" s="T706">
               <ts e="T708" id="Seg_8885" n="HIAT:u" s="T706">
                  <ts e="T706.tx-MAK.1" id="Seg_8887" n="HIAT:w" s="T706">Еще</ts>
                  <nts id="Seg_8888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_8890" n="HIAT:w" s="T706.tx-MAK.1">раз</ts>
                  <nts id="Seg_8891" n="HIAT:ip">.</nts>
                  <nts id="Seg_8892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-MAK">
            <ts e="T14" id="Seg_8893" n="sc" s="T9">
               <ts e="T14" id="Seg_8895" n="e" s="T9">Говори-говори-говори! </ts>
            </ts>
            <ts e="T17" id="Seg_8896" n="sc" s="T15">
               <ts e="T17" id="Seg_8898" n="e" s="T15">Ну да! </ts>
            </ts>
            <ts e="T22" id="Seg_8899" n="sc" s="T19">
               <ts e="T22" id="Seg_8901" n="e" s="T19">Я там была! </ts>
            </ts>
            <ts e="T27" id="Seg_8902" n="sc" s="T25">
               <ts e="T27" id="Seg_8904" n="e" s="T25">Там ругаются. </ts>
            </ts>
            <ts e="T36" id="Seg_8905" n="sc" s="T32">
               <ts e="T33" id="Seg_8907" n="e" s="T32">Ага! </ts>
               <ts e="T36" id="Seg_8909" n="e" s="T33">Много поди написал! </ts>
            </ts>
            <ts e="T47" id="Seg_8910" n="sc" s="T46">
               <ts e="T47" id="Seg_8912" n="e" s="T46">Упал. </ts>
            </ts>
            <ts e="T52" id="Seg_8913" n="sc" s="T51">
               <ts e="T52" id="Seg_8915" n="e" s="T51">((…)). </ts>
            </ts>
            <ts e="T56" id="Seg_8916" n="sc" s="T53">
               <ts e="T56" id="Seg_8918" n="e" s="T53">О, давай. </ts>
            </ts>
            <ts e="T59" id="Seg_8919" n="sc" s="T58">
               <ts e="T59" id="Seg_8921" n="e" s="T58">Сначала. </ts>
            </ts>
            <ts e="T62" id="Seg_8922" n="sc" s="T61">
               <ts e="T62" id="Seg_8924" n="e" s="T61">((LAUGH)). </ts>
            </ts>
            <ts e="T70" id="Seg_8925" n="sc" s="T64">
               <ts e="T70" id="Seg_8927" n="e" s="T64">Сначала говорите все это, с самого. </ts>
            </ts>
            <ts e="T96" id="Seg_8928" n="sc" s="T91">
               <ts e="T95" id="Seg_8930" n="e" s="T91">Еще скажи это все. </ts>
               <ts e="T96" id="Seg_8932" n="e" s="T95">((…)). </ts>
            </ts>
            <ts e="T107" id="Seg_8933" n="sc" s="T101">
               <ts e="T107" id="Seg_8935" n="e" s="T101">Ну да, снова говорите все это. </ts>
            </ts>
            <ts e="T244" id="Seg_8936" n="sc" s="T243">
               <ts e="T244" id="Seg_8938" n="e" s="T243">Маленечко. </ts>
            </ts>
            <ts e="T268" id="Seg_8939" n="sc" s="T265">
               <ts e="T268" id="Seg_8941" n="e" s="T265">А теперь сначала. </ts>
            </ts>
            <ts e="T315" id="Seg_8942" n="sc" s="T312">
               <ts e="T313" id="Seg_8944" n="e" s="T312">Ну, </ts>
               <ts e="T314" id="Seg_8946" n="e" s="T313">скажи, </ts>
               <ts e="T315" id="Seg_8948" n="e" s="T314">бабушка. </ts>
            </ts>
            <ts e="T355" id="Seg_8949" n="sc" s="T350">
               <ts e="T351" id="Seg_8951" n="e" s="T350">Повторите, </ts>
               <ts e="T352" id="Seg_8953" n="e" s="T351">бабушка, </ts>
               <ts e="T353" id="Seg_8955" n="e" s="T352">еще </ts>
               <ts e="T354" id="Seg_8957" n="e" s="T353">снова </ts>
               <ts e="T355" id="Seg_8959" n="e" s="T354">все. </ts>
            </ts>
            <ts e="T418" id="Seg_8960" n="sc" s="T396">
               <ts e="T398" id="Seg_8962" n="e" s="T396">В </ts>
               <ts e="T399" id="Seg_8964" n="e" s="T398">прошлом </ts>
               <ts e="T400" id="Seg_8966" n="e" s="T399">году </ts>
               <ts e="T401" id="Seg_8968" n="e" s="T400">с </ts>
               <ts e="T402" id="Seg_8970" n="e" s="T401">трудом </ts>
               <ts e="T403" id="Seg_8972" n="e" s="T402">до </ts>
               <ts e="T404" id="Seg_8974" n="e" s="T403">десяти </ts>
               <ts e="T405" id="Seg_8976" n="e" s="T404">досчитали, </ts>
               <ts e="T406" id="Seg_8978" n="e" s="T405">а </ts>
               <ts e="T407" id="Seg_8980" n="e" s="T406">в </ts>
               <ts e="T408" id="Seg_8982" n="e" s="T407">этом </ts>
               <ts e="T409" id="Seg_8984" n="e" s="T408">году </ts>
               <ts e="T410" id="Seg_8986" n="e" s="T409">до </ts>
               <ts e="T411" id="Seg_8988" n="e" s="T410">двадцати </ts>
               <ts e="T412" id="Seg_8990" n="e" s="T411">запросто </ts>
               <ts e="T413" id="Seg_8992" n="e" s="T412">(считаете). </ts>
               <ts e="T418" id="Seg_8994" n="e" s="T413">На будущий год до ста. </ts>
            </ts>
            <ts e="T495" id="Seg_8995" n="sc" s="T494">
               <ts e="T495" id="Seg_8997" n="e" s="T494">(Клади) мясо. </ts>
            </ts>
            <ts e="T504" id="Seg_8998" n="sc" s="T502">
               <ts e="T504" id="Seg_9000" n="e" s="T502">Еще раз. </ts>
            </ts>
            <ts e="T514" id="Seg_9001" n="sc" s="T507">
               <ts e="T514" id="Seg_9003" n="e" s="T507">А вот скажите еще раз это же. </ts>
            </ts>
            <ts e="T573" id="Seg_9004" n="sc" s="T571">
               <ts e="T573" id="Seg_9006" n="e" s="T571">Еще, еще. </ts>
            </ts>
            <ts e="T614" id="Seg_9007" n="sc" s="T612">
               <ts e="T614" id="Seg_9009" n="e" s="T612">Куда бежишь? </ts>
            </ts>
            <ts e="T621" id="Seg_9010" n="sc" s="T620">
               <ts e="T621" id="Seg_9012" n="e" s="T620">Искать. </ts>
            </ts>
            <ts e="T634" id="Seg_9013" n="sc" s="T631">
               <ts e="T632" id="Seg_9015" n="e" s="T631">((BRK)). </ts>
               <ts e="T633" id="Seg_9017" n="e" s="T632">Zdărowă </ts>
               <ts e="T634" id="Seg_9019" n="e" s="T633">igel! </ts>
            </ts>
            <ts e="T708" id="Seg_9020" n="sc" s="T706">
               <ts e="T708" id="Seg_9022" n="e" s="T706">Еще раз. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-MAK">
            <ta e="T14" id="Seg_9023" s="T9">PKZ_1964_SU0204.MAK.001 (003)</ta>
            <ta e="T17" id="Seg_9024" s="T15">PKZ_1964_SU0204.MAK.002 (005)</ta>
            <ta e="T22" id="Seg_9025" s="T19">PKZ_1964_SU0204.MAK.003 (007)</ta>
            <ta e="T27" id="Seg_9026" s="T25">PKZ_1964_SU0204.MAK.004 (009)</ta>
            <ta e="T33" id="Seg_9027" s="T32">PKZ_1964_SU0204.MAK.005 (012)</ta>
            <ta e="T36" id="Seg_9028" s="T33">PKZ_1964_SU0204.MAK.006 (013)</ta>
            <ta e="T47" id="Seg_9029" s="T46">PKZ_1964_SU0204.MAK.007 (018)</ta>
            <ta e="T52" id="Seg_9030" s="T51">PKZ_1964_SU0204.MAK.008 (020)</ta>
            <ta e="T56" id="Seg_9031" s="T53">PKZ_1964_SU0204.MAK.009 (022)</ta>
            <ta e="T59" id="Seg_9032" s="T58">PKZ_1964_SU0204.MAK.010 (025)</ta>
            <ta e="T62" id="Seg_9033" s="T61">PKZ_1964_SU0204.MAK.011 (025)</ta>
            <ta e="T70" id="Seg_9034" s="T64">PKZ_1964_SU0204.MAK.012 (027)</ta>
            <ta e="T95" id="Seg_9035" s="T91">PKZ_1964_SU0204.MAK.013 (034)</ta>
            <ta e="T96" id="Seg_9036" s="T95">PKZ_1964_SU0204.MAK.014 (035)</ta>
            <ta e="T107" id="Seg_9037" s="T101">PKZ_1964_SU0204.MAK.015 (037)</ta>
            <ta e="T244" id="Seg_9038" s="T243">PKZ_1964_SU0204.MAK.016 (090)</ta>
            <ta e="T268" id="Seg_9039" s="T265">PKZ_1964_SU0204.MAK.017 (099)</ta>
            <ta e="T315" id="Seg_9040" s="T312">PKZ_1964_SU0204.MAK.018 (118)</ta>
            <ta e="T355" id="Seg_9041" s="T350">PKZ_1964_SU0204.MAK.019 (127)</ta>
            <ta e="T413" id="Seg_9042" s="T396">PKZ_1964_SU0204.MAK.020 (134)</ta>
            <ta e="T418" id="Seg_9043" s="T413">PKZ_1964_SU0204.MAK.021 (135)</ta>
            <ta e="T495" id="Seg_9044" s="T494">PKZ_1964_SU0204.MAK.022 (163)</ta>
            <ta e="T504" id="Seg_9045" s="T502">PKZ_1964_SU0204.MAK.023 (165)</ta>
            <ta e="T514" id="Seg_9046" s="T507">PKZ_1964_SU0204.MAK.024 (167)</ta>
            <ta e="T573" id="Seg_9047" s="T571">PKZ_1964_SU0204.MAK.025 (184)</ta>
            <ta e="T614" id="Seg_9048" s="T612">PKZ_1964_SU0204.MAK.026 (197)</ta>
            <ta e="T621" id="Seg_9049" s="T620">PKZ_1964_SU0204.MAK.027 (201)</ta>
            <ta e="T632" id="Seg_9050" s="T631">PKZ_1964_SU0204.MAK.028 (206)</ta>
            <ta e="T634" id="Seg_9051" s="T632">PKZ_1964_SU0204.MAK.029 (207)</ta>
            <ta e="T708" id="Seg_9052" s="T706">PKZ_1964_SU0204.MAK.030 (238)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-MAK">
            <ta e="T14" id="Seg_9053" s="T9">Говори-говори-говори! </ta>
            <ta e="T17" id="Seg_9054" s="T15">Ну да! </ta>
            <ta e="T22" id="Seg_9055" s="T19">Я там была! </ta>
            <ta e="T27" id="Seg_9056" s="T25">Там ругаются. </ta>
            <ta e="T33" id="Seg_9057" s="T32">Ага! </ta>
            <ta e="T36" id="Seg_9058" s="T33">Много поди написал! </ta>
            <ta e="T47" id="Seg_9059" s="T46">Упал. </ta>
            <ta e="T52" id="Seg_9060" s="T51">((…)). </ta>
            <ta e="T56" id="Seg_9061" s="T53">О, давай. </ta>
            <ta e="T59" id="Seg_9062" s="T58">Сначала. </ta>
            <ta e="T62" id="Seg_9063" s="T61">((LAUGH)). </ta>
            <ta e="T70" id="Seg_9064" s="T64">Сначала говорите все это, с самого. </ta>
            <ta e="T95" id="Seg_9065" s="T91">Еще скажи это все. </ta>
            <ta e="T96" id="Seg_9066" s="T95">((…)). </ta>
            <ta e="T107" id="Seg_9067" s="T101">Ну да, снова говорите все это. </ta>
            <ta e="T244" id="Seg_9068" s="T243">Маленечко. </ta>
            <ta e="T268" id="Seg_9069" s="T265">А теперь сначала. </ta>
            <ta e="T315" id="Seg_9070" s="T312">Ну, скажи, бабушка. </ta>
            <ta e="T355" id="Seg_9071" s="T350">Повторите, бабушка, еще снова все. </ta>
            <ta e="T413" id="Seg_9072" s="T396">((DMG)) В прошлом году с трудом до десяти досчитали, а в этом году до двадцати запросто (считаете). </ta>
            <ta e="T418" id="Seg_9073" s="T413">На будущий год до ста. </ta>
            <ta e="T495" id="Seg_9074" s="T494">(Клади) мясо. </ta>
            <ta e="T504" id="Seg_9075" s="T502">Еще раз. </ta>
            <ta e="T514" id="Seg_9076" s="T507">А вот скажите еще раз это же. </ta>
            <ta e="T573" id="Seg_9077" s="T571">Еще, еще. </ta>
            <ta e="T614" id="Seg_9078" s="T612">Куда бежишь? </ta>
            <ta e="T621" id="Seg_9079" s="T620">Искать. </ta>
            <ta e="T632" id="Seg_9080" s="T631">((BRK)). </ta>
            <ta e="T634" id="Seg_9081" s="T632">Zdărowă igel! </ta>
            <ta e="T708" id="Seg_9082" s="T706">Еще раз. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-MAK">
            <ta e="T14" id="Seg_9083" s="T9">RUS:ext</ta>
            <ta e="T17" id="Seg_9084" s="T15">RUS:ext</ta>
            <ta e="T22" id="Seg_9085" s="T19">RUS:ext</ta>
            <ta e="T27" id="Seg_9086" s="T25">RUS:ext</ta>
            <ta e="T36" id="Seg_9087" s="T32">RUS:ext</ta>
            <ta e="T47" id="Seg_9088" s="T46">RUS:ext</ta>
            <ta e="T56" id="Seg_9089" s="T53">RUS:ext</ta>
            <ta e="T59" id="Seg_9090" s="T58">RUS:ext</ta>
            <ta e="T70" id="Seg_9091" s="T64">RUS:ext</ta>
            <ta e="T95" id="Seg_9092" s="T91">RUS:ext</ta>
            <ta e="T244" id="Seg_9093" s="T243">RUS:ext</ta>
            <ta e="T268" id="Seg_9094" s="T265">RUS:ext</ta>
            <ta e="T315" id="Seg_9095" s="T312">RUS:ext</ta>
            <ta e="T355" id="Seg_9096" s="T350">RUS:ext</ta>
            <ta e="T418" id="Seg_9097" s="T396">RUS:ext</ta>
            <ta e="T495" id="Seg_9098" s="T494">RUS:ext</ta>
            <ta e="T504" id="Seg_9099" s="T502">RUS:ext</ta>
            <ta e="T514" id="Seg_9100" s="T507">RUS:ext</ta>
            <ta e="T573" id="Seg_9101" s="T571">RUS:ext</ta>
            <ta e="T614" id="Seg_9102" s="T612">RUS:ext</ta>
            <ta e="T621" id="Seg_9103" s="T620">RUS:ext</ta>
            <ta e="T708" id="Seg_9104" s="T706">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-MAK">
            <ta e="T14" id="Seg_9105" s="T9">Говори-говори-говори! </ta>
            <ta e="T17" id="Seg_9106" s="T15">Ну да! </ta>
            <ta e="T22" id="Seg_9107" s="T19">Я там была! </ta>
            <ta e="T27" id="Seg_9108" s="T25">Там ругаются. </ta>
            <ta e="T33" id="Seg_9109" s="T32">Ага! </ta>
            <ta e="T36" id="Seg_9110" s="T33">Много поди написал! </ta>
            <ta e="T47" id="Seg_9111" s="T46">Упал. </ta>
            <ta e="T56" id="Seg_9112" s="T53">О, давай. </ta>
            <ta e="T59" id="Seg_9113" s="T58">Сначала. </ta>
            <ta e="T70" id="Seg_9114" s="T64">Сначала говорите все это, с самого. </ta>
            <ta e="T95" id="Seg_9115" s="T91">Еще скажи это все. </ta>
            <ta e="T244" id="Seg_9116" s="T243">Маленечко. </ta>
            <ta e="T268" id="Seg_9117" s="T265">А теперь сначала. </ta>
            <ta e="T315" id="Seg_9118" s="T312">Ну, скажи, бабушка. </ta>
            <ta e="T355" id="Seg_9119" s="T350">Повторите, бабушка, еще снова все. </ta>
            <ta e="T413" id="Seg_9120" s="T396">В прошлом году с трудом до десяти досчитали, а в этом году до двадцати запросто (считаете). </ta>
            <ta e="T418" id="Seg_9121" s="T413">На будущий год до ста. </ta>
            <ta e="T495" id="Seg_9122" s="T494">(Клади) мясо. </ta>
            <ta e="T504" id="Seg_9123" s="T502">Еще раз. </ta>
            <ta e="T514" id="Seg_9124" s="T507">А вот скажите еще раз это же. </ta>
            <ta e="T573" id="Seg_9125" s="T571">Еще, еще. </ta>
            <ta e="T614" id="Seg_9126" s="T612">Куда бежишь? </ta>
            <ta e="T621" id="Seg_9127" s="T620">Искать. </ta>
            <ta e="T708" id="Seg_9128" s="T706">Еще раз. </ta>
         </annotation>
         <annotation name="fe" tierref="fe-MAK">
            <ta e="T14" id="Seg_9129" s="T9">Speak, speak, speak!</ta>
            <ta e="T17" id="Seg_9130" s="T15">Well, yes!</ta>
            <ta e="T22" id="Seg_9131" s="T19">I was there.</ta>
            <ta e="T27" id="Seg_9132" s="T25">They are quarrelling there.</ta>
            <ta e="T33" id="Seg_9133" s="T32">Aha!</ta>
            <ta e="T36" id="Seg_9134" s="T33">You probably wrote a lot.</ta>
            <ta e="T47" id="Seg_9135" s="T46">He fell. </ta>
            <ta e="T56" id="Seg_9136" s="T53">Oh, go on.</ta>
            <ta e="T59" id="Seg_9137" s="T58">From the beginning.</ta>
            <ta e="T70" id="Seg_9138" s="T64">Say all this from the beginning, from the very beginning.</ta>
            <ta e="T95" id="Seg_9139" s="T91">Say this once again.</ta>
            <ta e="T244" id="Seg_9140" s="T243">A little bit.</ta>
            <ta e="T268" id="Seg_9141" s="T265">And now from the beginning.</ta>
            <ta e="T315" id="Seg_9142" s="T312">Please say it, grandmother.</ta>
            <ta e="T355" id="Seg_9143" s="T350">Grandmother, please repeat all again.</ta>
            <ta e="T413" id="Seg_9144" s="T396">Last year you hardly counted up to ten, this year you did it so easily up to twenty.</ta>
            <ta e="T418" id="Seg_9145" s="T413">Next year, up to one hundred.</ta>
            <ta e="T495" id="Seg_9146" s="T494">(Put) the meat (in).</ta>
            <ta e="T504" id="Seg_9147" s="T502">Once again.</ta>
            <ta e="T514" id="Seg_9148" s="T507">Well, say the same once again.</ta>
            <ta e="T573" id="Seg_9149" s="T571">Go on, go on.</ta>
            <ta e="T614" id="Seg_9150" s="T612">Where are you running?</ta>
            <ta e="T621" id="Seg_9151" s="T620">To look for [it].</ta>
            <ta e="T708" id="Seg_9152" s="T706">Once again.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-MAK">
            <ta e="T14" id="Seg_9153" s="T9">Sag, sag, sag!</ta>
            <ta e="T17" id="Seg_9154" s="T15">Nun, ja!</ta>
            <ta e="T22" id="Seg_9155" s="T19">Ich war dort!</ta>
            <ta e="T27" id="Seg_9156" s="T25">Dort streiten sie sich.</ta>
            <ta e="T33" id="Seg_9157" s="T32">Aha!</ta>
            <ta e="T36" id="Seg_9158" s="T33">Du hast vielleicht viel geschrieben.</ta>
            <ta e="T47" id="Seg_9159" s="T46">Er ist gefallen.</ta>
            <ta e="T56" id="Seg_9160" s="T53">Na los.</ta>
            <ta e="T59" id="Seg_9161" s="T58">Von Anfang an.</ta>
            <ta e="T70" id="Seg_9162" s="T64">Sagen Sie das alles ganz von Anfang an.</ta>
            <ta e="T95" id="Seg_9163" s="T91">Sag das alles noch mal.</ta>
            <ta e="T107" id="Seg_9164" s="T101">Nun ja, sagen Sie das alles noch mal.</ta>
            <ta e="T244" id="Seg_9165" s="T243">Ein kleines Bisschen.</ta>
            <ta e="T268" id="Seg_9166" s="T265">Und jetzt von Anfang an.</ta>
            <ta e="T315" id="Seg_9167" s="T312">Nun, sage, Mütterchen.</ta>
            <ta e="T355" id="Seg_9168" s="T350">Wiederholen Sie bitte, Mütterchen, nochmal von Neuem.</ta>
            <ta e="T413" id="Seg_9169" s="T396">Im letzten Jahr haben Sie mit Mühe bis zwanzig gezählt, und in diesem Jahr ganz leicht bis zwanzig.</ta>
            <ta e="T418" id="Seg_9170" s="T413">Im nächsten Jahr bis hundert.</ta>
            <ta e="T495" id="Seg_9171" s="T494">(Leg) Fleisch (hin).</ta>
            <ta e="T504" id="Seg_9172" s="T502">Noch mal.</ta>
            <ta e="T514" id="Seg_9173" s="T507">Nun, sagen Sie das nochmal.</ta>
            <ta e="T573" id="Seg_9174" s="T571">Weiter, weiter.</ta>
            <ta e="T614" id="Seg_9175" s="T612">Wohin läufst du?</ta>
            <ta e="T621" id="Seg_9176" s="T620">Suchen.</ta>
            <ta e="T634" id="Seg_9177" s="T632">Hallo!</ta>
            <ta e="T708" id="Seg_9178" s="T706">Noch mal.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-MAK" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T742" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T743" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T741" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-MAK"
                          name="ref"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-MAK"
                          name="ts"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-MAK"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-MAK"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-MAK"
                          name="CS"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-MAK"
                          name="fr"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-MAK"
                          name="fe"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-MAK"
                          name="fg"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-MAK"
                          name="nt"
                          segmented-tier-id="tx-MAK"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
