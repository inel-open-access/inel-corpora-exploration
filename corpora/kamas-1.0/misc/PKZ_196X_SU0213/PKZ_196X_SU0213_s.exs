<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID391E97C0-6C97-E2B1-B687-52E85A721BC1">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_SU0213</transcription-name>
         <referenced-file url="PKZ_196X_SU0213.wav" />
         <referenced-file url="PKZ_196X_SU0213.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0213\PKZ_196X_SU0213.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1283</ud-information>
            <ud-information attribute-name="# HIAT:w">815</ud-information>
            <ud-information attribute-name="# e">816</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">192</ud-information>
            <ud-information attribute-name="# sc">312</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.216" type="appl" />
         <tli id="T2" time="0.9232499999999999" type="appl" />
         <tli id="T3" time="1.6304999999999998" type="appl" />
         <tli id="T4" time="2.3377499999999998" type="appl" />
         <tli id="T5" time="3.0999923153529116" />
         <tli id="T6" time="3.3066584697097725" />
         <tli id="T7" time="4.1155" type="appl" />
         <tli id="T8" time="4.945" type="appl" />
         <tli id="T9" time="5.353320062856748" />
         <tli id="T10" time="6.731999999999999" type="appl" />
         <tli id="T11" time="7.759980763593096" />
         <tli id="T12" time="9.276" type="appl" />
         <tli id="T13" time="9.98" type="appl" />
         <tli id="T14" time="10.684" type="appl" />
         <tli id="T15" time="11.388" type="appl" />
         <tli id="T16" time="13.053300975184948" />
         <tli id="T17" time="13.546571428571427" type="appl" />
         <tli id="T18" time="14.102142857142857" type="appl" />
         <tli id="T19" time="14.657714285714285" type="appl" />
         <tli id="T20" time="15.213285714285714" type="appl" />
         <tli id="T21" time="15.768857142857142" type="appl" />
         <tli id="T22" time="16.32442857142857" type="appl" />
         <tli id="T23" time="16.906667074022753" />
         <tli id="T24" time="17.27642857142857" type="appl" />
         <tli id="T25" time="17.672857142857143" type="appl" />
         <tli id="T26" time="18.069285714285716" type="appl" />
         <tli id="T27" time="18.465714285714284" type="appl" />
         <tli id="T28" time="18.862142857142857" type="appl" />
         <tli id="T29" time="19.25857142857143" type="appl" />
         <tli id="T30" time="19.655" type="appl" />
         <tli id="T31" time="22.674" type="appl" />
         <tli id="T32" time="23.2902" type="appl" />
         <tli id="T33" time="23.906399999999998" type="appl" />
         <tli id="T34" time="24.5226" type="appl" />
         <tli id="T35" time="25.1388" type="appl" />
         <tli id="T36" time="25.755" type="appl" />
         <tli id="T37" time="26.327" type="appl" />
         <tli id="T38" time="27.071333333333335" type="appl" />
         <tli id="T39" time="27.815666666666665" type="appl" />
         <tli id="T40" time="28.56" type="appl" />
         <tli id="T41" time="28.663" type="appl" />
         <tli id="T42" time="29.1706" type="appl" />
         <tli id="T43" time="29.6782" type="appl" />
         <tli id="T44" time="30.1858" type="appl" />
         <tli id="T45" time="30.6934" type="appl" />
         <tli id="T46" time="31.201" type="appl" />
         <tli id="T47" time="31.77025" type="appl" />
         <tli id="T48" time="32.3395" type="appl" />
         <tli id="T49" time="32.90875" type="appl" />
         <tli id="T50" time="33.478" type="appl" />
         <tli id="T51" time="33.687" type="appl" />
         <tli id="T52" time="34.34166666666667" type="appl" />
         <tli id="T53" time="34.99633333333333" type="appl" />
         <tli id="T54" time="35.651" type="appl" />
         <tli id="T55" time="36.385" type="appl" />
         <tli id="T56" time="37.957499999999996" type="appl" />
         <tli id="T57" time="39.53" type="appl" />
         <tli id="T58" time="41.1025" type="appl" />
         <tli id="T59" time="42.675" type="appl" />
         <tli id="T60" time="43.11322645889738" />
         <tli id="T61" time="43.798" type="appl" />
         <tli id="T62" time="44.426" type="appl" />
         <tli id="T63" time="45.054" type="appl" />
         <tli id="T64" time="45.682" type="appl" />
         <tli id="T65" time="46.31" type="appl" />
         <tli id="T66" time="46.938" type="appl" />
         <tli id="T67" time="47.566" type="appl" />
         <tli id="T68" time="48.194" type="appl" />
         <tli id="T69" time="48.822" type="appl" />
         <tli id="T70" time="50.12466666666667" type="appl" />
         <tli id="T71" time="51.42733333333333" type="appl" />
         <tli id="T72" time="52.73" type="appl" />
         <tli id="T73" time="54.485" type="appl" />
         <tli id="T74" time="55.494" type="appl" />
         <tli id="T75" time="56.503" type="appl" />
         <tli id="T76" time="56.513" type="appl" />
         <tli id="T77" time="57.2855" type="appl" />
         <tli id="T78" time="58.058" type="appl" />
         <tli id="T79" time="60.592" type="appl" />
         <tli id="T80" time="61.638666666666666" type="appl" />
         <tli id="T81" time="62.68533333333333" type="appl" />
         <tli id="T82" time="63.732" type="appl" />
         <tli id="T83" time="64.084" type="appl" />
         <tli id="T84" time="64.7565" type="appl" />
         <tli id="T85" time="65.429" type="appl" />
         <tli id="T86" time="66.1015" type="appl" />
         <tli id="T87" time="66.774" type="appl" />
         <tli id="T88" time="67.4465" type="appl" />
         <tli id="T89" time="68.119" type="appl" />
         <tli id="T90" time="68.7915" type="appl" />
         <tli id="T91" time="69.464" type="appl" />
         <tli id="T92" time="69.846" type="appl" />
         <tli id="T93" time="70.6632" type="appl" />
         <tli id="T94" time="71.4804" type="appl" />
         <tli id="T95" time="72.2976" type="appl" />
         <tli id="T96" time="73.1148" type="appl" />
         <tli id="T97" time="73.99981656003725" />
         <tli id="T98" time="74.133" type="appl" />
         <tli id="T99" time="75.37725" type="appl" />
         <tli id="T100" time="76.6215" type="appl" />
         <tli id="T101" time="77.86574999999999" type="appl" />
         <tli id="T102" time="79.11" type="appl" />
         <tli id="T103" time="81.272" type="appl" />
         <tli id="T104" time="82.09733333333334" type="appl" />
         <tli id="T105" time="82.92266666666667" type="appl" />
         <tli id="T106" time="83.748" type="appl" />
         <tli id="T107" time="84.012" type="appl" />
         <tli id="T108" time="84.9096" type="appl" />
         <tli id="T109" time="85.8072" type="appl" />
         <tli id="T110" time="86.7048" type="appl" />
         <tli id="T111" time="87.6024" type="appl" />
         <tli id="T112" time="88.5" type="appl" />
         <tli id="T113" time="88.706" type="appl" />
         <tli id="T114" time="89.48333333333333" type="appl" />
         <tli id="T115" time="90.26066666666667" type="appl" />
         <tli id="T116" time="91.038" type="appl" />
         <tli id="T117" time="92.389" type="appl" />
         <tli id="T118" time="93.137" type="appl" />
         <tli id="T119" time="93.88499999999999" type="appl" />
         <tli id="T120" time="94.633" type="appl" />
         <tli id="T121" time="94.82" type="appl" />
         <tli id="T122" time="95.80449999999999" type="appl" />
         <tli id="T123" time="96.789" type="appl" />
         <tli id="T124" time="97.094" type="appl" />
         <tli id="T125" time="97.761" type="appl" />
         <tli id="T126" time="98.428" type="appl" />
         <tli id="T127" time="99.245" type="appl" />
         <tli id="T128" time="100.215" type="appl" />
         <tli id="T129" time="101.185" type="appl" />
         <tli id="T130" time="102.155" type="appl" />
         <tli id="T131" time="103.125" type="appl" />
         <tli id="T132" time="106.8" type="appl" />
         <tli id="T133" time="107.8536" type="appl" />
         <tli id="T134" time="108.9072" type="appl" />
         <tli id="T135" time="109.96079999999999" type="appl" />
         <tli id="T964" time="110.44707692307692" type="intp" />
         <tli id="T136" time="111.0144" type="appl" />
         <tli id="T137" time="112.068" type="appl" />
         <tli id="T138" time="112.13" type="appl" />
         <tli id="T139" time="113.047" type="appl" />
         <tli id="T140" time="113.964" type="appl" />
         <tli id="T141" time="114.13971705625205" />
         <tli id="T142" time="115.31975" type="appl" />
         <tli id="T143" time="116.4315" type="appl" />
         <tli id="T144" time="117.54325" type="appl" />
         <tli id="T145" time="118.655" type="appl" />
         <tli id="T146" time="120.965" type="appl" />
         <tli id="T147" time="121.75525" type="appl" />
         <tli id="T148" time="122.5455" type="appl" />
         <tli id="T149" time="123.33575" type="appl" />
         <tli id="T150" time="124.126" type="appl" />
         <tli id="T151" time="124.337" type="appl" />
         <tli id="T152" time="125.0788" type="appl" />
         <tli id="T153" time="125.8206" type="appl" />
         <tli id="T154" time="126.5624" type="appl" />
         <tli id="T155" time="127.3042" type="appl" />
         <tli id="T156" time="128.046" type="appl" />
         <tli id="T157" time="131.454" type="appl" />
         <tli id="T158" time="132.321125" type="appl" />
         <tli id="T159" time="133.18825" type="appl" />
         <tli id="T160" time="134.055375" type="appl" />
         <tli id="T965" time="134.45558653846155" type="intp" />
         <tli id="T161" time="134.9225" type="appl" />
         <tli id="T162" time="135.789625" type="appl" />
         <tli id="T163" time="136.65675" type="appl" />
         <tli id="T164" time="137.523875" type="appl" />
         <tli id="T165" time="138.391" type="appl" />
         <tli id="T166" time="138.589" type="appl" />
         <tli id="T167" time="139.44366666666667" type="appl" />
         <tli id="T168" time="140.29833333333332" type="appl" />
         <tli id="T169" time="141.153" type="appl" />
         <tli id="T170" time="142.407" type="appl" />
         <tli id="T171" time="143.41775" type="appl" />
         <tli id="T172" time="144.42849999999999" type="appl" />
         <tli id="T173" time="145.43925" type="appl" />
         <tli id="T174" time="146.45" type="appl" />
         <tli id="T175" time="147.923" type="appl" />
         <tli id="T176" time="148.76983333333334" type="appl" />
         <tli id="T177" time="149.61666666666667" type="appl" />
         <tli id="T178" time="150.4635" type="appl" />
         <tli id="T179" time="151.31033333333332" type="appl" />
         <tli id="T180" time="152.15716666666665" type="appl" />
         <tli id="T181" time="153.004" type="appl" />
         <tli id="T182" time="153.397" type="appl" />
         <tli id="T183" time="155.958" type="appl" />
         <tli id="T184" time="157.79" type="appl" />
         <tli id="T185" time="159.10683333333333" type="appl" />
         <tli id="T186" time="160.42366666666666" type="appl" />
         <tli id="T187" time="161.7405" type="appl" />
         <tli id="T188" time="163.05733333333333" type="appl" />
         <tli id="T189" time="164.37416666666667" type="appl" />
         <tli id="T190" time="165.691" type="appl" />
         <tli id="T191" time="166.27190000000002" type="appl" />
         <tli id="T192" time="166.8528" type="appl" />
         <tli id="T193" time="167.4337" type="appl" />
         <tli id="T194" time="168.0146" type="appl" />
         <tli id="T195" time="168.59550000000002" type="appl" />
         <tli id="T196" time="169.1764" type="appl" />
         <tli id="T197" time="169.7573" type="appl" />
         <tli id="T198" time="170.3382" type="appl" />
         <tli id="T199" time="170.91910000000001" type="appl" />
         <tli id="T200" time="171.5" type="appl" />
         <tli id="T201" time="173.962" type="appl" />
         <tli id="T202" time="174.89785714285713" type="appl" />
         <tli id="T203" time="175.83371428571428" type="appl" />
         <tli id="T204" time="176.76957142857142" type="appl" />
         <tli id="T205" time="177.70542857142857" type="appl" />
         <tli id="T206" time="178.64128571428571" type="appl" />
         <tli id="T207" time="179.57714285714286" type="appl" />
         <tli id="T208" time="180.513" type="appl" />
         <tli id="T209" time="184.151" type="appl" />
         <tli id="T210" time="185.1055" type="appl" />
         <tli id="T211" time="186.06" type="appl" />
         <tli id="T212" time="187.0145" type="appl" />
         <tli id="T213" time="187.969" type="appl" />
         <tli id="T214" time="188.246" type="appl" />
         <tli id="T215" time="189.19983333333334" type="appl" />
         <tli id="T216" time="190.15366666666668" type="appl" />
         <tli id="T217" time="191.10750000000002" type="appl" />
         <tli id="T218" time="192.06133333333332" type="appl" />
         <tli id="T219" time="193.01516666666666" type="appl" />
         <tli id="T220" time="193.969" type="appl" />
         <tli id="T221" time="194.575" type="appl" />
         <tli id="T222" time="195.3335" type="appl" />
         <tli id="T223" time="196.09199999999998" type="appl" />
         <tli id="T224" time="196.8505" type="appl" />
         <tli id="T225" time="197.609" type="appl" />
         <tli id="T226" time="197.774" type="appl" />
         <tli id="T227" time="198.5456" type="appl" />
         <tli id="T228" time="199.3172" type="appl" />
         <tli id="T229" time="200.0888" type="appl" />
         <tli id="T230" time="200.8604" type="appl" />
         <tli id="T231" time="201.632" type="appl" />
         <tli id="T232" time="201.637" type="appl" />
         <tli id="T233" time="202.17825" type="appl" />
         <tli id="T234" time="202.71949999999998" type="appl" />
         <tli id="T235" time="203.26075" type="appl" />
         <tli id="T236" time="203.802" type="appl" />
         <tli id="T237" time="203.805" type="appl" />
         <tli id="T238" time="204.47666666666666" type="appl" />
         <tli id="T239" time="205.14833333333334" type="appl" />
         <tli id="T240" time="205.82" type="appl" />
         <tli id="T241" time="207.499" type="appl" />
         <tli id="T242" time="208.48" type="appl" />
         <tli id="T243" time="209.32742857142856" type="appl" />
         <tli id="T244" time="210.17485714285715" type="appl" />
         <tli id="T245" time="211.02228571428572" type="appl" />
         <tli id="T246" time="211.86971428571428" type="appl" />
         <tli id="T247" time="212.71714285714285" type="appl" />
         <tli id="T248" time="213.56457142857144" type="appl" />
         <tli id="T249" time="214.412" type="appl" />
         <tli id="T250" time="217.007" type="appl" />
         <tli id="T251" time="219.09050000000002" type="appl" />
         <tli id="T252" time="221.174" type="appl" />
         <tli id="T253" time="223.2575" type="appl" />
         <tli id="T254" time="225.341" type="appl" />
         <tli id="T255" time="225.997" type="appl" />
         <tli id="T256" time="226.85716666666667" type="appl" />
         <tli id="T257" time="227.71733333333333" type="appl" />
         <tli id="T258" time="228.5775" type="appl" />
         <tli id="T259" time="229.43766666666667" type="appl" />
         <tli id="T260" time="230.29783333333333" type="appl" />
         <tli id="T261" time="231.158" type="appl" />
         <tli id="T262" time="231.66219999999998" type="appl" />
         <tli id="T263" time="232.16639999999998" type="appl" />
         <tli id="T264" time="232.6706" type="appl" />
         <tli id="T265" time="233.1748" type="appl" />
         <tli id="T266" time="233.679" type="appl" />
         <tli id="T267" time="234.7" type="appl" />
         <tli id="T268" time="235.34366666666665" type="appl" />
         <tli id="T269" time="235.98733333333334" type="appl" />
         <tli id="T270" time="236.631" type="appl" />
         <tli id="T271" time="237.27466666666666" type="appl" />
         <tli id="T272" time="237.91833333333335" type="appl" />
         <tli id="T273" time="238.562" type="appl" />
         <tli id="T274" time="238.764" type="appl" />
         <tli id="T275" time="239.5737142857143" type="appl" />
         <tli id="T276" time="240.38342857142857" type="appl" />
         <tli id="T277" time="241.19314285714285" type="appl" />
         <tli id="T278" time="242.00285714285715" type="appl" />
         <tli id="T279" time="242.81257142857143" type="appl" />
         <tli id="T280" time="243.6222857142857" type="appl" />
         <tli id="T281" time="244.432" type="appl" />
         <tli id="T282" time="245.023" type="appl" />
         <tli id="T283" time="245.99099999999999" type="appl" />
         <tli id="T284" time="246.959" type="appl" />
         <tli id="T285" time="247.006" type="appl" />
         <tli id="T286" time="247.64533333333333" type="appl" />
         <tli id="T287" time="248.28466666666668" type="appl" />
         <tli id="T288" time="248.924" type="appl" />
         <tli id="T289" time="249.56333333333333" type="appl" />
         <tli id="T290" time="250.2026666666667" type="appl" />
         <tli id="T291" time="250.842" type="appl" />
         <tli id="T292" time="251.154" type="appl" />
         <tli id="T293" time="251.8294" type="appl" />
         <tli id="T294" time="252.5048" type="appl" />
         <tli id="T295" time="253.1802" type="appl" />
         <tli id="T296" time="253.8556" type="appl" />
         <tli id="T297" time="254.531" type="appl" />
         <tli id="T298" time="255.943" type="appl" />
         <tli id="T299" time="256.961" type="appl" />
         <tli id="T300" time="257.979" type="appl" />
         <tli id="T301" time="258.997" type="appl" />
         <tli id="T302" time="259.9883398835188" />
         <tli id="T303" time="260.78499999999997" type="appl" />
         <tli id="T304" time="261.555" type="appl" />
         <tli id="T305" time="262.325" type="appl" />
         <tli id="T306" time="263.09499999999997" type="appl" />
         <tli id="T307" time="263.86499999999995" type="appl" />
         <tli id="T308" time="264.635" type="appl" />
         <tli id="T309" time="265.405" type="appl" />
         <tli id="T310" time="265.466" type="appl" />
         <tli id="T311" time="265.93833333333333" type="appl" />
         <tli id="T312" time="266.41066666666666" type="appl" />
         <tli id="T313" time="266.88300000000004" type="appl" />
         <tli id="T314" time="267.35533333333336" type="appl" />
         <tli id="T315" time="267.8276666666667" type="appl" />
         <tli id="T316" time="268.3" type="appl" />
         <tli id="T317" time="268.9515" type="appl" />
         <tli id="T318" time="269.603" type="appl" />
         <tli id="T319" time="270.2545" type="appl" />
         <tli id="T320" time="270.906" type="appl" />
         <tli id="T321" time="271.5575" type="appl" />
         <tli id="T322" time="272.209" type="appl" />
         <tli id="T323" time="272.463" type="appl" />
         <tli id="T324" time="273.1996666666667" type="appl" />
         <tli id="T325" time="273.9363333333333" type="appl" />
         <tli id="T326" time="274.673" type="appl" />
         <tli id="T327" time="275.4096666666667" type="appl" />
         <tli id="T328" time="276.1463333333333" type="appl" />
         <tli id="T329" time="276.883" type="appl" />
         <tli id="T330" time="278.279" type="appl" />
         <tli id="T331" time="278.7704444444444" type="appl" />
         <tli id="T332" time="279.2618888888889" type="appl" />
         <tli id="T333" time="279.75333333333333" type="appl" />
         <tli id="T334" time="280.24477777777776" type="appl" />
         <tli id="T335" time="280.73622222222224" type="appl" />
         <tli id="T336" time="281.22766666666666" type="appl" />
         <tli id="T337" time="281.7191111111111" type="appl" />
         <tli id="T338" time="282.2105555555556" type="appl" />
         <tli id="T339" time="282.702" type="appl" />
         <tli id="T340" time="283.7" type="appl" />
         <tli id="T341" time="285.0143333333333" type="appl" />
         <tli id="T342" time="286.32866666666666" type="appl" />
         <tli id="T343" time="287.643" type="appl" />
         <tli id="T344" time="289.0543333333333" type="appl" />
         <tli id="T345" time="290.46566666666666" type="appl" />
         <tli id="T346" time="291.877" type="appl" />
         <tli id="T347" time="292.864" type="appl" />
         <tli id="T348" time="293.7805714285714" type="appl" />
         <tli id="T349" time="294.6971428571428" type="appl" />
         <tli id="T350" time="295.61371428571425" type="appl" />
         <tli id="T351" time="296.5302857142857" type="appl" />
         <tli id="T352" time="297.44685714285714" type="appl" />
         <tli id="T353" time="298.3634285714285" type="appl" />
         <tli id="T354" time="299.28" type="appl" />
         <tli id="T355" time="300.952" type="appl" />
         <tli id="T356" time="301.71625" type="appl" />
         <tli id="T357" time="302.4805" type="appl" />
         <tli id="T358" time="303.24475" type="appl" />
         <tli id="T359" time="304.009" type="appl" />
         <tli id="T360" time="304.589" type="appl" />
         <tli id="T361" time="305.51599999999996" type="appl" />
         <tli id="T362" time="306.443" type="appl" />
         <tli id="T363" time="307.149" type="appl" />
         <tli id="T364" time="307.718" type="appl" />
         <tli id="T365" time="308.287" type="appl" />
         <tli id="T366" time="308.856" type="appl" />
         <tli id="T367" time="309.6936666666667" type="appl" />
         <tli id="T368" time="310.53133333333335" type="appl" />
         <tli id="T369" time="311.369" type="appl" />
         <tli id="T370" time="312.20666666666665" type="appl" />
         <tli id="T371" time="313.0443333333333" type="appl" />
         <tli id="T372" time="313.882" type="appl" />
         <tli id="T373" time="314.423" type="appl" />
         <tli id="T374" time="315.2235" type="appl" />
         <tli id="T375" time="316.024" type="appl" />
         <tli id="T376" time="316.8245" type="appl" />
         <tli id="T377" time="317.625" type="appl" />
         <tli id="T378" time="319.164" type="appl" />
         <tli id="T379" time="319.95" type="appl" />
         <tli id="T380" time="320.736" type="appl" />
         <tli id="T381" time="322.757" type="appl" />
         <tli id="T382" time="323.99649999999997" type="appl" />
         <tli id="T383" time="325.236" type="appl" />
         <tli id="T384" time="325.387" type="appl" />
         <tli id="T385" time="326.106" type="appl" />
         <tli id="T386" time="326.825" type="appl" />
         <tli id="T387" time="327.54400000000004" type="appl" />
         <tli id="T388" time="328.26300000000003" type="appl" />
         <tli id="T389" time="328.982" type="appl" />
         <tli id="T390" time="329.488" type="appl" />
         <tli id="T391" time="329.994" type="appl" />
         <tli id="T392" time="330.5" type="appl" />
         <tli id="T393" time="331.202" type="appl" />
         <tli id="T394" time="332.272" type="appl" />
         <tli id="T395" time="332.732" type="appl" />
         <tli id="T396" time="334.13320000000004" type="appl" />
         <tli id="T397" time="335.5344" type="appl" />
         <tli id="T398" time="336.9356" type="appl" />
         <tli id="T399" time="338.3368" type="appl" />
         <tli id="T400" time="339.738" type="appl" />
         <tli id="T401" time="340.21915662237666" />
         <tli id="T402" time="341.16740000000004" type="appl" />
         <tli id="T403" time="342.0498" type="appl" />
         <tli id="T404" time="342.9322" type="appl" />
         <tli id="T405" time="343.8146" type="appl" />
         <tli id="T406" time="344.697" type="appl" />
         <tli id="T407" time="345.197" type="appl" />
         <tli id="T408" time="346.63275" type="appl" />
         <tli id="T409" time="348.0685" type="appl" />
         <tli id="T410" time="349.50425" type="appl" />
         <tli id="T411" time="350.94" type="appl" />
         <tli id="T412" time="352.329" type="appl" />
         <tli id="T413" time="353.4936666666667" type="appl" />
         <tli id="T414" time="354.6583333333333" type="appl" />
         <tli id="T415" time="355.823" type="appl" />
         <tli id="T416" time="355.891" type="appl" />
         <tli id="T417" time="356.67100000000005" type="appl" />
         <tli id="T418" time="357.451" type="appl" />
         <tli id="T419" time="358.0725" type="appl" />
         <tli id="T420" time="358.694" type="appl" />
         <tli id="T421" time="359.31550000000004" type="appl" />
         <tli id="T422" time="359.937" type="appl" />
         <tli id="T423" time="360.5585" type="appl" />
         <tli id="T424" time="361.18" type="appl" />
         <tli id="T425" time="361.75166666666667" type="appl" />
         <tli id="T426" time="362.3233333333333" type="appl" />
         <tli id="T427" time="362.87501191855387" />
         <tli id="T428" time="363.901" type="appl" />
         <tli id="T429" time="364.338" type="appl" />
         <tli id="T430" time="365.0105" type="appl" />
         <tli id="T431" time="365.683" type="appl" />
         <tli id="T432" time="367.88" type="appl" />
         <tli id="T433" time="368.89300000000003" type="appl" />
         <tli id="T434" time="369.906" type="appl" />
         <tli id="T435" time="370.919" type="appl" />
         <tli id="T436" time="371.96532792601295" />
         <tli id="T437" time="372.307" type="appl" />
         <tli id="T438" time="372.682" type="appl" />
         <tli id="T439" time="373.399" type="appl" />
         <tli id="T440" time="374.11600000000004" type="appl" />
         <tli id="T441" time="374.833" type="appl" />
         <tli id="T442" time="375.55" type="appl" />
         <tli id="T443" time="376.267" type="appl" />
         <tli id="T444" time="376.98400000000004" type="appl" />
         <tli id="T445" time="377.701" type="appl" />
         <tli id="T446" time="377.753" type="appl" />
         <tli id="T447" time="379.076" type="appl" />
         <tli id="T448" time="379.67733333333337" type="appl" />
         <tli id="T449" time="380.2786666666667" type="appl" />
         <tli id="T450" time="380.88" type="appl" />
         <tli id="T451" time="381.48133333333334" type="appl" />
         <tli id="T452" time="382.0826666666667" type="appl" />
         <tli id="T453" time="382.684" type="appl" />
         <tli id="T454" time="382.823" type="appl" />
         <tli id="T455" time="383.48275" type="appl" />
         <tli id="T456" time="384.1425" type="appl" />
         <tli id="T457" time="384.80224999999996" type="appl" />
         <tli id="T458" time="385.462" type="appl" />
         <tli id="T459" time="385.907" type="appl" />
         <tli id="T460" time="386.50663636363635" type="appl" />
         <tli id="T461" time="387.1062727272727" type="appl" />
         <tli id="T462" time="387.7059090909091" type="appl" />
         <tli id="T463" time="388.30554545454544" type="appl" />
         <tli id="T464" time="388.9051818181818" type="appl" />
         <tli id="T966" time="389.18193706293704" type="intp" />
         <tli id="T465" time="389.50481818181817" type="appl" />
         <tli id="T466" time="390.10445454545453" type="appl" />
         <tli id="T467" time="390.7040909090909" type="appl" />
         <tli id="T468" time="391.30372727272726" type="appl" />
         <tli id="T469" time="391.9033636363636" type="appl" />
         <tli id="T470" time="392.503" type="appl" />
         <tli id="T471" time="394.879" type="appl" />
         <tli id="T472" time="395.8006666666667" type="appl" />
         <tli id="T473" time="396.7223333333333" type="appl" />
         <tli id="T474" time="397.644" type="appl" />
         <tli id="T475" time="398.154" type="appl" />
         <tli id="T476" time="398.82349999999997" type="appl" />
         <tli id="T967" time="399.11042857142854" type="intp" />
         <tli id="T477" time="399.493" type="appl" />
         <tli id="T478" time="399.796" type="appl" />
         <tli id="T479" time="400.5863333333333" type="appl" />
         <tli id="T480" time="401.37666666666667" type="appl" />
         <tli id="T481" time="402.16700000000003" type="appl" />
         <tli id="T482" time="402.95733333333334" type="appl" />
         <tli id="T483" time="403.74766666666665" type="appl" />
         <tli id="T484" time="404.538" type="appl" />
         <tli id="T485" time="405.349" type="appl" />
         <tli id="T486" time="406.147" type="appl" />
         <tli id="T487" time="406.945" type="appl" />
         <tli id="T488" time="409.021" type="appl" />
         <tli id="T489" time="409.744" type="appl" />
         <tli id="T490" time="410.467" type="appl" />
         <tli id="T491" time="411.19" type="appl" />
         <tli id="T492" time="411.358" type="appl" />
         <tli id="T493" time="411.85525" type="appl" />
         <tli id="T494" time="412.35249999999996" type="appl" />
         <tli id="T495" time="412.84975" type="appl" />
         <tli id="T496" time="413.347" type="appl" />
         <tli id="T497" time="413.748" type="appl" />
         <tli id="T498" time="414.2975" type="appl" />
         <tli id="T499" time="414.847" type="appl" />
         <tli id="T500" time="415.3965" type="appl" />
         <tli id="T501" time="415.946" type="appl" />
         <tli id="T502" time="416.515" type="appl" />
         <tli id="T503" time="418.1375" type="appl" />
         <tli id="T504" time="419.76" type="appl" />
         <tli id="T505" time="421.082" type="appl" />
         <tli id="T506" time="421.8546" type="appl" />
         <tli id="T507" time="422.6272" type="appl" />
         <tli id="T508" time="423.39979999999997" type="appl" />
         <tli id="T509" time="424.1724" type="appl" />
         <tli id="T510" time="424.945" type="appl" />
         <tli id="T511" time="425.2" type="appl" />
         <tli id="T512" time="425.90925" type="appl" />
         <tli id="T513" time="426.6185" type="appl" />
         <tli id="T514" time="427.32775" type="appl" />
         <tli id="T515" time="428.037" type="appl" />
         <tli id="T516" time="428.12" type="appl" />
         <tli id="T517" time="428.9785" type="appl" />
         <tli id="T518" time="429.837" type="appl" />
         <tli id="T519" time="430.254" type="appl" />
         <tli id="T520" time="431.0014" type="appl" />
         <tli id="T521" time="431.7488" type="appl" />
         <tli id="T522" time="432.4962" type="appl" />
         <tli id="T523" time="433.2436" type="appl" />
         <tli id="T524" time="433.991" type="appl" />
         <tli id="T525" time="434.58" type="appl" />
         <tli id="T526" time="435.705" type="appl" />
         <tli id="T968" time="436.1871428571428" type="intp" />
         <tli id="T527" time="436.83" type="appl" />
         <tli id="T528" time="437.555" type="appl" />
         <tli id="T529" time="438.29150000000004" type="appl" />
         <tli id="T530" time="439.028" type="appl" />
         <tli id="T531" time="439.7985" type="appl" />
         <tli id="T532" time="440.569" type="appl" />
         <tli id="T533" time="440.578" type="appl" />
         <tli id="T534" time="441.30466666666666" type="appl" />
         <tli id="T535" time="442.0313333333333" type="appl" />
         <tli id="T536" time="442.758" type="appl" />
         <tli id="T537" time="443.041" type="appl" />
         <tli id="T538" time="443.8085" type="appl" />
         <tli id="T539" time="444.576" type="appl" />
         <tli id="T540" time="445.3435" type="appl" />
         <tli id="T541" time="446.111" type="appl" />
         <tli id="T542" time="446.87850000000003" type="appl" />
         <tli id="T543" time="447.646" type="appl" />
         <tli id="T544" time="448.591" type="appl" />
         <tli id="T545" time="449.2896" type="appl" />
         <tli id="T546" time="449.9882" type="appl" />
         <tli id="T969" time="450.2876" type="intp" />
         <tli id="T547" time="450.6868" type="appl" />
         <tli id="T548" time="451.3854" type="appl" />
         <tli id="T549" time="452.084" type="appl" />
         <tli id="T550" time="454.904" type="appl" />
         <tli id="T551" time="455.6616" type="appl" />
         <tli id="T552" time="456.4192" type="appl" />
         <tli id="T553" time="457.1768" type="appl" />
         <tli id="T554" time="457.9344" type="appl" />
         <tli id="T555" time="458.692" type="appl" />
         <tli id="T556" time="459.513" type="appl" />
         <tli id="T557" time="460.5603333333333" type="appl" />
         <tli id="T558" time="461.60766666666666" type="appl" />
         <tli id="T559" time="462.655" type="appl" />
         <tli id="T560" time="463.4455" type="appl" />
         <tli id="T561" time="464.236" type="appl" />
         <tli id="T562" time="465.008" type="appl" />
         <tli id="T563" time="465.83825" type="appl" />
         <tli id="T564" time="466.6685" type="appl" />
         <tli id="T565" time="467.49875" type="appl" />
         <tli id="T566" time="468.329" type="appl" />
         <tli id="T567" time="468.91375" type="appl" />
         <tli id="T568" time="469.49850000000004" type="appl" />
         <tli id="T569" time="470.08325" type="appl" />
         <tli id="T570" time="470.668" type="appl" />
         <tli id="T571" time="471.088" type="appl" />
         <tli id="T572" time="471.825" type="appl" />
         <tli id="T573" time="472.562" type="appl" />
         <tli id="T574" time="473.299" type="appl" />
         <tli id="T575" time="475.969" type="appl" />
         <tli id="T576" time="476.959" type="appl" />
         <tli id="T970" time="477.3832857142857" type="intp" />
         <tli id="T577" time="477.949" type="appl" />
         <tli id="T578" time="478.288" type="appl" />
         <tli id="T579" time="478.95866666666666" type="appl" />
         <tli id="T580" time="479.62933333333336" type="appl" />
         <tli id="T581" time="480.3" type="appl" />
         <tli id="T582" time="480.76" type="appl" />
         <tli id="T583" time="481.3645" type="appl" />
         <tli id="T584" time="481.969" type="appl" />
         <tli id="T585" time="482.57349999999997" type="appl" />
         <tli id="T586" time="483.2388020874648" />
         <tli id="T587" time="483.467" type="appl" />
         <tli id="T588" time="484.154" type="appl" />
         <tli id="T589" time="484.841" type="appl" />
         <tli id="T590" time="485.52799999999996" type="appl" />
         <tli id="T591" time="486.215" type="appl" />
         <tli id="T592" time="486.601" type="appl" />
         <tli id="T593" time="487.1144" type="appl" />
         <tli id="T594" time="487.6278" type="appl" />
         <tli id="T595" time="488.1412" type="appl" />
         <tli id="T596" time="488.6546" type="appl" />
         <tli id="T597" time="489.168" type="appl" />
         <tli id="T598" time="490.841" type="appl" />
         <tli id="T599" time="491.2838" type="appl" />
         <tli id="T600" time="491.7266" type="appl" />
         <tli id="T601" time="492.1694" type="appl" />
         <tli id="T602" time="492.61220000000003" type="appl" />
         <tli id="T603" time="493.055" type="appl" />
         <tli id="T604" time="493.9041666666667" type="appl" />
         <tli id="T605" time="494.75333333333333" type="appl" />
         <tli id="T606" time="495.60249999999996" type="appl" />
         <tli id="T607" time="496.45166666666665" type="appl" />
         <tli id="T608" time="497.30083333333334" type="appl" />
         <tli id="T609" time="498.15" type="appl" />
         <tli id="T610" time="498.155" type="appl" />
         <tli id="T611" time="498.82399999999996" type="appl" />
         <tli id="T612" time="499.493" type="appl" />
         <tli id="T613" time="500.162" type="appl" />
         <tli id="T614" time="500.831" type="appl" />
         <tli id="T615" time="501.53875672325785" />
         <tli id="T616" time="502.918" type="appl" />
         <tli id="T617" time="503.85566666666665" type="appl" />
         <tli id="T618" time="504.79333333333335" type="appl" />
         <tli id="T971" time="505.1951904761905" type="intp" />
         <tli id="T619" time="505.731" type="appl" />
         <tli id="T620" time="506.3233333333333" type="appl" />
         <tli id="T621" time="506.91566666666665" type="appl" />
         <tli id="T622" time="507.508" type="appl" />
         <tli id="T623" time="507.997" type="appl" />
         <tli id="T624" time="508.81166666666667" type="appl" />
         <tli id="T625" time="509.6263333333333" type="appl" />
         <tli id="T626" time="510.441" type="appl" />
         <tli id="T627" time="510.571" type="appl" />
         <tli id="T628" time="511.36975" type="appl" />
         <tli id="T629" time="512.1685" type="appl" />
         <tli id="T630" time="512.9672499999999" type="appl" />
         <tli id="T631" time="513.766" type="appl" />
         <tli id="T632" time="514.021" type="appl" />
         <tli id="T633" time="515.014" type="appl" />
         <tli id="T634" time="516.0070000000001" type="appl" />
         <tli id="T635" time="517.0" type="appl" />
         <tli id="T636" time="517.993" type="appl" />
         <tli id="T637" time="518.237" type="appl" />
         <tli id="T638" time="519.66675" type="appl" />
         <tli id="T639" time="521.0965" type="appl" />
         <tli id="T640" time="522.52625" type="appl" />
         <tli id="T641" time="523.956" type="appl" />
         <tli id="T642" time="524.729" type="appl" />
         <tli id="T643" time="525.609" type="appl" />
         <tli id="T644" time="526.489" type="appl" />
         <tli id="T645" time="527.369" type="appl" />
         <tli id="T646" time="527.643" type="appl" />
         <tli id="T647" time="528.3775714285714" type="appl" />
         <tli id="T648" time="529.1121428571429" type="appl" />
         <tli id="T649" time="529.8467142857143" type="appl" />
         <tli id="T650" time="530.5812857142857" type="appl" />
         <tli id="T651" time="531.3158571428571" type="appl" />
         <tli id="T652" time="532.0504285714286" type="appl" />
         <tli id="T653" time="532.785" type="appl" />
         <tli id="T654" time="533.883" type="appl" />
         <tli id="T655" time="534.7565" type="appl" />
         <tli id="T656" time="535.63" type="appl" />
         <tli id="T657" time="537.613" type="appl" />
         <tli id="T658" time="538.606" type="appl" />
         <tli id="T659" time="539.599" type="appl" />
         <tli id="T660" time="540.5920000000001" type="appl" />
         <tli id="T661" time="541.585" type="appl" />
         <tli id="T662" time="541.745" type="appl" />
         <tli id="T663" time="542.3306666666666" type="appl" />
         <tli id="T664" time="542.9163333333333" type="appl" />
         <tli id="T665" time="543.502" type="appl" />
         <tli id="T666" time="543.579" type="appl" />
         <tli id="T667" time="544.2345" type="appl" />
         <tli id="T668" time="544.89" type="appl" />
         <tli id="T669" time="545.5455" type="appl" />
         <tli id="T670" time="546.201" type="appl" />
         <tli id="T671" time="546.402" type="appl" />
         <tli id="T672" time="546.9580000000001" type="appl" />
         <tli id="T673" time="547.514" type="appl" />
         <tli id="T674" time="547.843" type="appl" />
         <tli id="T675" time="548.5139999999999" type="appl" />
         <tli id="T676" time="549.185" type="appl" />
         <tli id="T677" time="549.8553999999999" type="appl" />
         <tli id="T678" time="550.5258" type="appl" />
         <tli id="T679" time="551.1962" type="appl" />
         <tli id="T680" time="551.8666000000001" type="appl" />
         <tli id="T681" time="552.537" type="appl" />
         <tli id="T682" time="553.82" type="appl" />
         <tli id="T683" time="554.6238000000001" type="appl" />
         <tli id="T684" time="555.4276000000001" type="appl" />
         <tli id="T685" time="556.2314" type="appl" />
         <tli id="T686" time="557.0352" type="appl" />
         <tli id="T687" time="557.839" type="appl" />
         <tli id="T688" time="558.037" type="appl" />
         <tli id="T689" time="558.7633333333333" type="appl" />
         <tli id="T690" time="559.4896666666667" type="appl" />
         <tli id="T691" time="560.216" type="appl" />
         <tli id="T692" time="560.9257692307692" type="appl" />
         <tli id="T693" time="561.6355384615384" type="appl" />
         <tli id="T694" time="562.3453076923076" type="appl" />
         <tli id="T695" time="563.055076923077" type="appl" />
         <tli id="T696" time="563.7648461538462" type="appl" />
         <tli id="T697" time="564.4746153846154" type="appl" />
         <tli id="T698" time="565.1843846153846" type="appl" />
         <tli id="T699" time="565.8941538461538" type="appl" />
         <tli id="T700" time="566.603923076923" type="appl" />
         <tli id="T701" time="567.3136923076923" type="appl" />
         <tli id="T702" time="568.0234615384616" type="appl" />
         <tli id="T703" time="568.7332307692308" type="appl" />
         <tli id="T972" time="569.0374175824176" type="intp" />
         <tli id="T704" time="569.443" type="appl" />
         <tli id="T705" time="573.85" type="appl" />
         <tli id="T706" time="574.7015" type="appl" />
         <tli id="T707" time="575.553" type="appl" />
         <tli id="T708" time="576.4045" type="appl" />
         <tli id="T709" time="577.256" type="appl" />
         <tli id="T710" time="577.471" type="appl" />
         <tli id="T711" time="578.0982" type="appl" />
         <tli id="T712" time="578.7254" type="appl" />
         <tli id="T713" time="579.3525999999999" type="appl" />
         <tli id="T714" time="579.9798" type="appl" />
         <tli id="T715" time="580.607" type="appl" />
         <tli id="T716" time="581.468" type="appl" />
         <tli id="T717" time="582.3467499999999" type="appl" />
         <tli id="T718" time="583.2255" type="appl" />
         <tli id="T719" time="584.10425" type="appl" />
         <tli id="T720" time="584.983" type="appl" />
         <tli id="T721" time="588.391" type="appl" />
         <tli id="T722" time="589.2345" type="appl" />
         <tli id="T723" time="590.078" type="appl" />
         <tli id="T724" time="590.328" type="appl" />
         <tli id="T725" time="590.9005714285714" type="appl" />
         <tli id="T726" time="591.4731428571429" type="appl" />
         <tli id="T727" time="592.0457142857143" type="appl" />
         <tli id="T728" time="592.6182857142857" type="appl" />
         <tli id="T729" time="593.1908571428571" type="appl" />
         <tli id="T730" time="593.7634285714286" type="appl" />
         <tli id="T731" time="594.336" type="appl" />
         <tli id="T732" time="596.3" type="appl" />
         <tli id="T733" time="597.0093999999999" type="appl" />
         <tli id="T734" time="597.7188" type="appl" />
         <tli id="T735" time="598.4282" type="appl" />
         <tli id="T736" time="599.1376" type="appl" />
         <tli id="T737" time="599.847" type="appl" />
         <tli id="T738" time="600.8833999999999" type="appl" />
         <tli id="T739" time="601.9198" type="appl" />
         <tli id="T740" time="602.9562" type="appl" />
         <tli id="T741" time="603.9926" type="appl" />
         <tli id="T742" time="605.029" type="appl" />
         <tli id="T743" time="606.0654" type="appl" />
         <tli id="T744" time="607.1018" type="appl" />
         <tli id="T745" time="608.1382" type="appl" />
         <tli id="T746" time="609.1746" type="appl" />
         <tli id="T973" time="609.56325" type="intp" />
         <tli id="T747" time="610.211" type="appl" />
         <tli id="T748" time="612.056" type="appl" />
         <tli id="T749" time="612.8704" type="appl" />
         <tli id="T750" time="613.6848" type="appl" />
         <tli id="T751" time="614.4992000000001" type="appl" />
         <tli id="T752" time="615.3136000000001" type="appl" />
         <tli id="T753" time="616.128" type="appl" />
         <tli id="T754" time="616.265" type="appl" />
         <tli id="T755" time="616.6592499999999" type="appl" />
         <tli id="T756" time="617.0535" type="appl" />
         <tli id="T757" time="617.44775" type="appl" />
         <tli id="T758" time="617.842" type="appl" />
         <tli id="T759" time="617.887" type="appl" />
         <tli id="T760" time="618.433" type="appl" />
         <tli id="T761" time="618.9789999999999" type="appl" />
         <tli id="T762" time="619.525" type="appl" />
         <tli id="T763" time="620.0709999999999" type="appl" />
         <tli id="T764" time="620.617" type="appl" />
         <tli id="T765" time="621.6105" type="appl" />
         <tli id="T766" time="622.604" type="appl" />
         <tli id="T767" time="623.055" type="appl" />
         <tli id="T768" time="624.346" type="appl" />
         <tli id="T769" time="625.6775" type="appl" />
         <tli id="T770" time="627.009" type="appl" />
         <tli id="T771" time="627.341" type="appl" />
         <tli id="T772" time="627.859375" type="appl" />
         <tli id="T773" time="628.37775" type="appl" />
         <tli id="T774" time="628.896125" type="appl" />
         <tli id="T775" time="629.4145000000001" type="appl" />
         <tli id="T776" time="629.9328750000001" type="appl" />
         <tli id="T777" time="630.4512500000001" type="appl" />
         <tli id="T778" time="630.9696250000001" type="appl" />
         <tli id="T779" time="631.488" type="appl" />
         <tli id="T780" time="632.041" type="appl" />
         <tli id="T781" time="633.243" type="appl" />
         <tli id="T782" time="634.445" type="appl" />
         <tli id="T783" time="634.911" type="appl" />
         <tli id="T784" time="635.934" type="appl" />
         <tli id="T785" time="637.486" type="appl" />
         <tli id="T786" time="638.35" type="appl" />
         <tli id="T787" time="639.214" type="appl" />
         <tli id="T788" time="640.589" type="appl" />
         <tli id="T789" time="641.3022500000001" type="appl" />
         <tli id="T790" time="642.0155" type="appl" />
         <tli id="T791" time="642.72875" type="appl" />
         <tli id="T792" time="643.442" type="appl" />
         <tli id="T793" time="644.15525" type="appl" />
         <tli id="T794" time="644.8685" type="appl" />
         <tli id="T795" time="645.5817499999999" type="appl" />
         <tli id="T796" time="646.295" type="appl" />
         <tli id="T797" time="648.939" type="appl" />
         <tli id="T798" time="649.6126666666667" type="appl" />
         <tli id="T799" time="650.2863333333333" type="appl" />
         <tli id="T800" time="650.96" type="appl" />
         <tli id="T801" time="651.779" type="appl" />
         <tli id="T802" time="652.67" type="appl" />
         <tli id="T803" time="652.68" type="appl" />
         <tli id="T804" time="653.4033333333333" type="appl" />
         <tli id="T805" time="654.1266666666667" type="appl" />
         <tli id="T806" time="654.85" type="appl" />
         <tli id="T807" time="655.698" type="appl" />
         <tli id="T808" time="657.21" type="appl" />
         <tli id="T809" time="660.394" type="appl" />
         <tli id="T810" time="661.366" type="appl" />
         <tli id="T811" time="662.338" type="appl" />
         <tli id="T812" time="663.31" type="appl" />
         <tli id="T813" time="666.685" type="appl" />
         <tli id="T814" time="667.168" type="appl" />
         <tli id="T815" time="667.651" type="appl" />
         <tli id="T816" time="668.134" type="appl" />
         <tli id="T817" time="668.617" type="appl" />
         <tli id="T818" time="669.1" type="appl" />
         <tli id="T819" time="669.6485" type="appl" />
         <tli id="T820" time="670.197" type="appl" />
         <tli id="T821" time="670.741" type="appl" />
         <tli id="T822" time="671.285" type="appl" />
         <tli id="T823" time="671.829" type="appl" />
         <tli id="T824" time="672.373" type="appl" />
         <tli id="T825" time="672.917" type="appl" />
         <tli id="T826" time="673.461" type="appl" />
         <tli id="T827" time="674.005" type="appl" />
         <tli id="T828" time="675.507" type="appl" />
         <tli id="T829" time="676.20625" type="appl" />
         <tli id="T830" time="676.9055" type="appl" />
         <tli id="T831" time="677.60475" type="appl" />
         <tli id="T832" time="678.304" type="appl" />
         <tli id="T833" time="679.501" type="appl" />
         <tli id="T834" time="680.033" type="appl" />
         <tli id="T835" time="680.5649999999999" type="appl" />
         <tli id="T836" time="681.097" type="appl" />
         <tli id="T837" time="681.629" type="appl" />
         <tli id="T838" time="682.161" type="appl" />
         <tli id="T839" time="682.693" type="appl" />
         <tli id="T840" time="683.345" type="appl" />
         <tli id="T841" time="684.26875" type="appl" />
         <tli id="T842" time="685.1925" type="appl" />
         <tli id="T843" time="686.11625" type="appl" />
         <tli id="T844" time="687.04" type="appl" />
         <tli id="T845" time="687.215" type="appl" />
         <tli id="T846" time="688.3405" type="appl" />
         <tli id="T847" time="689.466" type="appl" />
         <tli id="T848" time="690.915" type="appl" />
         <tli id="T849" time="691.668" type="appl" />
         <tli id="T850" time="692.738" type="appl" />
         <tli id="T851" time="693.269" type="appl" />
         <tli id="T852" time="693.8" type="appl" />
         <tli id="T853" time="694.331" type="appl" />
         <tli id="T854" time="694.862" type="appl" />
         <tli id="T855" time="695.4533749999999" type="appl" />
         <tli id="T856" time="696.04475" type="appl" />
         <tli id="T857" time="696.636125" type="appl" />
         <tli id="T858" time="697.2275" type="appl" />
         <tli id="T859" time="697.8188749999999" type="appl" />
         <tli id="T860" time="698.4102499999999" type="appl" />
         <tli id="T861" time="699.001625" type="appl" />
         <tli id="T862" time="699.593" type="appl" />
         <tli id="T863" time="699.609" type="appl" />
         <tli id="T864" time="700.412" type="appl" />
         <tli id="T865" time="701.215" type="appl" />
         <tli id="T866" time="701.682" type="appl" />
         <tli id="T867" time="702.149" type="appl" />
         <tli id="T868" time="702.616" type="appl" />
         <tli id="T869" time="703.083" type="appl" />
         <tli id="T870" time="703.113" type="appl" />
         <tli id="T871" time="703.9960000000001" type="appl" />
         <tli id="T872" time="704.879" type="appl" />
         <tli id="T873" time="705.762" type="appl" />
         <tli id="T874" time="706.645" type="appl" />
         <tli id="T875" time="708.255" type="appl" />
         <tli id="T876" time="708.9067142857143" type="appl" />
         <tli id="T877" time="709.5584285714285" type="appl" />
         <tli id="T878" time="710.2101428571428" type="appl" />
         <tli id="T879" time="710.8618571428572" type="appl" />
         <tli id="T880" time="711.5135714285715" type="appl" />
         <tli id="T881" time="712.1652857142857" type="appl" />
         <tli id="T882" time="712.817" type="appl" />
         <tli id="T883" time="713.173" type="appl" />
         <tli id="T884" time="713.962" type="appl" />
         <tli id="T885" time="714.751" type="appl" />
         <tli id="T886" time="715.54" type="appl" />
         <tli id="T887" time="716.241" type="appl" />
         <tli id="T888" time="716.9797142857143" type="appl" />
         <tli id="T889" time="717.7184285714286" type="appl" />
         <tli id="T890" time="718.4571428571429" type="appl" />
         <tli id="T891" time="719.1958571428571" type="appl" />
         <tli id="T892" time="719.9345714285714" type="appl" />
         <tli id="T893" time="720.6732857142857" type="appl" />
         <tli id="T894" time="721.412" type="appl" />
         <tli id="T895" time="721.748" type="appl" />
         <tli id="T896" time="722.62525" type="appl" />
         <tli id="T897" time="723.5025" type="appl" />
         <tli id="T898" time="724.3797500000001" type="appl" />
         <tli id="T899" time="725.2570000000001" type="appl" />
         <tli id="T900" time="726.13425" type="appl" />
         <tli id="T901" time="727.0115" type="appl" />
         <tli id="T902" time="727.88875" type="appl" />
         <tli id="T903" time="728.766" type="appl" />
         <tli id="T904" time="728.921" type="appl" />
         <tli id="T905" time="729.7581111111111" type="appl" />
         <tli id="T906" time="730.5952222222222" type="appl" />
         <tli id="T907" time="731.4323333333334" type="appl" />
         <tli id="T908" time="732.2694444444445" type="appl" />
         <tli id="T909" time="733.1065555555556" type="appl" />
         <tli id="T910" time="733.9436666666667" type="appl" />
         <tli id="T911" time="734.7807777777779" type="appl" />
         <tli id="T912" time="735.617888888889" type="appl" />
         <tli id="T913" time="736.455" type="appl" />
         <tli id="T914" time="736.9848333333334" type="appl" />
         <tli id="T915" time="737.5146666666667" type="appl" />
         <tli id="T916" time="738.0445" type="appl" />
         <tli id="T917" time="738.5743333333334" type="appl" />
         <tli id="T918" time="739.1041666666667" type="appl" />
         <tli id="T919" time="739.634" type="appl" />
         <tli id="T920" time="740.7914969670973" />
         <tli id="T921" time="741.4513999999999" type="appl" />
         <tli id="T922" time="741.9918" type="appl" />
         <tli id="T923" time="742.5322" type="appl" />
         <tli id="T924" time="743.0726000000001" type="appl" />
         <tli id="T925" time="743.613" type="appl" />
         <tli id="T926" time="743.619" type="appl" />
         <tli id="T927" time="744.5426666666667" type="appl" />
         <tli id="T928" time="745.4663333333333" type="appl" />
         <tli id="T929" time="746.39" type="appl" />
         <tli id="T930" time="746.8465" type="appl" />
         <tli id="T931" time="747.303" type="appl" />
         <tli id="T932" time="747.7595" type="appl" />
         <tli id="T933" time="748.216" type="appl" />
         <tli id="T934" time="748.6725" type="appl" />
         <tli id="T935" time="749.129" type="appl" />
         <tli id="T936" time="749.5855" type="appl" />
         <tli id="T937" time="750.042" type="appl" />
         <tli id="T938" time="750.523" type="appl" />
         <tli id="T939" time="751.0653333333333" type="appl" />
         <tli id="T940" time="751.6076666666667" type="appl" />
         <tli id="T941" time="752.1500000000001" type="appl" />
         <tli id="T942" time="752.6923333333334" type="appl" />
         <tli id="T943" time="753.2346666666667" type="appl" />
         <tli id="T944" time="753.777" type="appl" />
         <tli id="T945" time="756.548" type="appl" />
         <tli id="T946" time="757.3083333333334" type="appl" />
         <tli id="T947" time="758.0686666666667" type="appl" />
         <tli id="T948" time="758.829" type="appl" />
         <tli id="T949" time="759.5893333333333" type="appl" />
         <tli id="T950" time="760.3496666666667" type="appl" />
         <tli id="T951" time="761.11" type="appl" />
         <tli id="T952" time="761.478" type="appl" />
         <tli id="T953" time="762.59125" type="appl" />
         <tli id="T954" time="763.7045" type="appl" />
         <tli id="T955" time="764.81775" type="appl" />
         <tli id="T956" time="765.931" type="appl" />
         <tli id="T957" time="766.9540000000001" type="appl" />
         <tli id="T958" time="767.977" type="appl" />
         <tli id="T959" time="769.0" type="appl" />
         <tli id="T960" time="769.9" type="appl" />
         <tli id="T961" time="770.807" type="appl" />
         <tli id="T962" time="771.7139999999999" type="appl" />
         <tli id="T963" time="772.621" type="appl" />
         <tli id="T0" time="773.184" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T396" start="T395">
            <tli id="T395.tx.1" />
         </timeline-fork>
         <timeline-fork end="T852" start="T850">
            <tli id="T850.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Dĭ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">šiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">aparat</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">taʔla</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T8" id="Seg_18" n="sc" s="T6">
               <ts e="T8" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Tararluʔpi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">bar</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T11" id="Seg_28" n="sc" s="T9">
               <ts e="T11" id="Seg_30" n="HIAT:u" s="T9">
                  <nts id="Seg_31" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">Начал</ts>
                  <nts id="Seg_34" n="HIAT:ip">)</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">šaːmzittə</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_40" n="sc" s="T12">
               <ts e="T15" id="Seg_42" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">Ugaːndə</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">ɨrɨ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">molaːmbi</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_53" n="sc" s="T16">
               <ts e="T23" id="Seg_55" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">Tăn</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">ugaːndə</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_62" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">kuguštu</ts>
                  <nts id="Seg_65" n="HIAT:ip">)</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">kuza</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">tănan</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">măllial</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">măllial</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_83" n="HIAT:u" s="T23">
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">A</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">tăn=</ts>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">A</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">tăn</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">ĭmbidə</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">ej</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">tĭmniel</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_108" n="sc" s="T31">
               <ts e="T36" id="Seg_110" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">Măn</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">tănan</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">mălliam:</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">üdʼüge</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">ibiem</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T40" id="Seg_127" n="sc" s="T37">
               <ts e="T40" id="Seg_129" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_131" n="HIAT:w" s="T37">Girgitdə</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">il</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">šobiʔi</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_140" n="sc" s="T41">
               <ts e="T46" id="Seg_142" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_144" n="HIAT:w" s="T41">То</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_147" n="HIAT:w" s="T42">ли</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_150" n="HIAT:w" s="T43">экспедиция</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_154" n="HIAT:w" s="T44">ej</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_157" n="HIAT:w" s="T45">tĭmniem</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_161" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">Üdʼüge</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_166" n="HIAT:w" s="T47">ibiem</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">ej</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">surarbiam</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_176" n="sc" s="T51">
               <ts e="T54" id="Seg_178" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">Măna</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">ej</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">nörbəbiʔi</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_189" n="sc" s="T55">
               <ts e="T59" id="Seg_191" n="HIAT:u" s="T55">
                  <nts id="Seg_192" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_194" n="HIAT:w" s="T55">Vlaskən</ts>
                  <nts id="Seg_195" n="HIAT:ip">)</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_197" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_199" n="HIAT:w" s="T56">Иван=</ts>
                  <nts id="Seg_200" n="HIAT:ip">)</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_203" n="HIAT:w" s="T57">Иван</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">Андреевич</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_209" n="sc" s="T60">
               <ts e="T69" id="Seg_211" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">Dĭgəttə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_216" n="HIAT:w" s="T61">dĭzen</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_219" n="HIAT:w" s="T62">ibi</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_222" n="HIAT:w" s="T63">бинокль</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_225" n="HIAT:w" s="T64">i</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_228" n="HIAT:w" s="T65">ĭmbi</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_232" n="HIAT:w" s="T66">tože</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_235" n="HIAT:w" s="T67">ej</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_238" n="HIAT:w" s="T68">tĭmniem</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_242" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_244" n="HIAT:w" s="T69">Măna</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">pʼerbiʔi</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">nʼimi</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_253" n="sc" s="T73">
               <ts e="T75" id="Seg_255" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">Müʔluʔpiʔi</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">stʼenanə</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_263" n="sc" s="T76">
               <ts e="T78" id="Seg_265" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_267" n="HIAT:w" s="T76">Kuŋgəŋ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_270" n="HIAT:w" s="T77">dăre</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T82" id="Seg_273" n="sc" s="T79">
               <ts e="T82" id="Seg_275" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_277" n="HIAT:w" s="T79">Sažendə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_280" n="HIAT:w" s="T80">šide</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_283" n="HIAT:w" s="T81">bʼeʔ</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_286" n="sc" s="T83">
               <ts e="T91" id="Seg_288" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_290" n="HIAT:w" s="T83">Dĭgəttə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">măn</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_296" n="HIAT:w" s="T85">măndərbiam</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_300" n="HIAT:w" s="T86">m-</ts>
                  <nts id="Seg_301" n="HIAT:ip">)</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_304" n="HIAT:w" s="T87">ugaːndə</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_306" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_308" n="HIAT:w" s="T88">nʼim-</ts>
                  <nts id="Seg_309" n="HIAT:ip">)</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_312" n="HIAT:w" s="T89">nʼimi</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_315" n="HIAT:w" s="T90">urgo</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_318" n="sc" s="T92">
               <ts e="T97" id="Seg_320" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_322" n="HIAT:w" s="T92">A</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_325" n="HIAT:w" s="T93">dĭgəttə</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_328" n="HIAT:w" s="T94">parluʔpiʔi</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_332" n="HIAT:w" s="T95">drugoj</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_335" n="HIAT:w" s="T96">kănʼessiʔ</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_338" n="sc" s="T98">
               <ts e="T102" id="Seg_340" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_342" n="HIAT:w" s="T98">Măndəbiom:</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_345" n="HIAT:w" s="T99">ugaːndə</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_348" n="HIAT:w" s="T100">kuŋgəŋ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_351" n="HIAT:w" s="T101">molaːmbi</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T106" id="Seg_354" n="sc" s="T103">
               <ts e="T106" id="Seg_356" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_358" n="HIAT:w" s="T103">Dĭgəttə</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_361" n="HIAT:w" s="T104">sazən</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_364" n="HIAT:w" s="T105">embiʔi</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T112" id="Seg_367" n="sc" s="T107">
               <ts e="T112" id="Seg_369" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_371" n="HIAT:w" s="T107">I</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_374" n="HIAT:w" s="T108">nuldəbiʔi</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_377" n="HIAT:w" s="T109">dĭ</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_380" n="HIAT:w" s="T110">binokl</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_383" n="HIAT:w" s="T111">kujanə</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T116" id="Seg_386" n="sc" s="T113">
               <ts e="T116" id="Seg_388" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_390" n="HIAT:w" s="T113">I</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_393" n="HIAT:w" s="T114">sazən</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_396" n="HIAT:w" s="T115">neniluʔpi</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_399" n="sc" s="T117">
               <ts e="T120" id="Seg_401" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_403" n="HIAT:w" s="T117">Šiʔ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_406" n="HIAT:w" s="T118">kamən</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_409" n="HIAT:w" s="T119">kalləlaʔ</ts>
                  <nts id="Seg_410" n="HIAT:ip">?</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T123" id="Seg_412" n="sc" s="T121">
               <ts e="T123" id="Seg_414" n="HIAT:u" s="T121">
                  <nts id="Seg_415" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_417" n="HIAT:w" s="T121">Păredʼan</ts>
                  <nts id="Seg_418" n="HIAT:ip">)</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_421" n="HIAT:w" s="T122">kalləbaʔ</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_424" n="sc" s="T124">
               <ts e="T126" id="Seg_426" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_428" n="HIAT:w" s="T124">No</ts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_432" n="HIAT:w" s="T125">kaŋgaʔ</ts>
                  <nts id="Seg_433" n="HIAT:ip">!</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_435" n="sc" s="T127">
               <ts e="T131" id="Seg_437" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_439" n="HIAT:w" s="T127">Privet</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_442" n="HIAT:w" s="T128">nörbəgeʔ</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_444" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_446" n="HIAT:w" s="T129">Mat-</ts>
                  <nts id="Seg_447" n="HIAT:ip">)</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_450" n="HIAT:w" s="T130">Matvejendə</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_453" n="sc" s="T132">
               <ts e="T137" id="Seg_455" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_457" n="HIAT:w" s="T132">I</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_460" n="HIAT:w" s="T133">Jelʼa</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_463" n="HIAT:w" s="T134">teinen</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_466" n="HIAT:w" s="T135">kalla</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_469" n="HIAT:w" s="T964">dʼürbi</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_472" n="HIAT:w" s="T136">Malinovkanə</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_475" n="sc" s="T138">
               <ts e="T140" id="Seg_477" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_479" n="HIAT:w" s="T138">Sazən</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_482" n="HIAT:w" s="T139">kundubi</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_485" n="sc" s="T141">
               <ts e="T145" id="Seg_487" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_489" n="HIAT:w" s="T141">Prʼedsʼedatelʼdə</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_492" n="HIAT:w" s="T142">nada</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_495" n="HIAT:w" s="T143">pečatʼʔi</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_498" n="HIAT:w" s="T144">enzittə</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_501" n="sc" s="T146">
               <ts e="T150" id="Seg_503" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_505" n="HIAT:w" s="T146">Dĭ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_508" n="HIAT:w" s="T147">kambi</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_511" n="HIAT:w" s="T148">dĭbər</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_514" n="HIAT:w" s="T149">mašinaʔizʼiʔ</ts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_517" n="sc" s="T151">
               <ts e="T156" id="Seg_519" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_521" n="HIAT:w" s="T151">A</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_523" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_525" n="HIAT:w" s="T152">dĭʔnə-</ts>
                  <nts id="Seg_526" n="HIAT:ip">)</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_529" n="HIAT:w" s="T153">dĭʔə</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_532" n="HIAT:w" s="T154">parləj</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_535" n="HIAT:w" s="T155">üdʼiŋ</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_538" n="sc" s="T157">
               <ts e="T165" id="Seg_540" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_542" n="HIAT:w" s="T157">Măn</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_545" n="HIAT:w" s="T158">sʼestranə</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_548" n="HIAT:w" s="T159">nʼit</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_551" n="HIAT:w" s="T160">kalla</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_554" n="HIAT:w" s="T965">dʼürbi</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_556" n="HIAT:ip">(</nts>
                  <ts e="T162" id="Seg_558" n="HIAT:w" s="T161">nodujla</ts>
                  <nts id="Seg_559" n="HIAT:ip">)</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_562" n="HIAT:w" s="T162">tüžöjʔi</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_564" n="HIAT:ip">(</nts>
                  <ts e="T164" id="Seg_566" n="HIAT:w" s="T163">sʼinədə</ts>
                  <nts id="Seg_567" n="HIAT:ip">)</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_570" n="HIAT:w" s="T164">deʔsʼittə</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_573" n="sc" s="T166">
               <ts e="T169" id="Seg_575" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_577" n="HIAT:w" s="T166">Bostə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_580" n="HIAT:w" s="T167">nʼit</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_583" n="HIAT:w" s="T168">ibi</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_586" n="sc" s="T170">
               <ts e="T174" id="Seg_588" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_590" n="HIAT:w" s="T170">Büžü</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_592" n="HIAT:ip">(</nts>
                  <ts e="T172" id="Seg_594" n="HIAT:w" s="T171">parluʔi-</ts>
                  <nts id="Seg_595" n="HIAT:ip">)</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_598" n="HIAT:w" s="T172">parluʔjə</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_601" n="HIAT:w" s="T173">maːʔndə</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_604" n="sc" s="T175">
               <ts e="T181" id="Seg_606" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_608" n="HIAT:w" s="T175">Onʼiʔ</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_611" n="HIAT:w" s="T176">kuza</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_614" n="HIAT:w" s="T177">tenölüʔpi</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_617" n="HIAT:w" s="T178">maʔ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_619" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_621" n="HIAT:w" s="T179">asi-</ts>
                  <nts id="Seg_622" n="HIAT:ip">)</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_625" n="HIAT:w" s="T180">azittə</ts>
                  <nts id="Seg_626" n="HIAT:ip">.</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_628" n="sc" s="T182">
               <ts e="T183" id="Seg_630" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_632" n="HIAT:w" s="T182">Zolătăʔizʼiʔ</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T200" id="Seg_635" n="sc" s="T184">
               <ts e="T190" id="Seg_637" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_639" n="HIAT:w" s="T184">A</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_642" n="HIAT:w" s="T185">baška</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_645" n="HIAT:w" s="T186">kuza</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_648" n="HIAT:w" s="T187">maʔ</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_651" n="HIAT:w" s="T188">azittə</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_654" n="HIAT:w" s="T189">sʼerʼebrogəʔ</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_658" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_660" n="HIAT:w" s="T190">A</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_663" n="HIAT:w" s="T191">nagur</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_666" n="HIAT:w" s="T192">kuza</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_669" n="HIAT:w" s="T193">bar</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_671" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_673" n="HIAT:w" s="T194">žəzə</ts>
                  <nts id="Seg_674" n="HIAT:ip">)</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_677" n="HIAT:w" s="T195">măndə:</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_680" n="HIAT:w" s="T196">măn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_683" n="HIAT:w" s="T197">paʔizʼi</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_686" n="HIAT:w" s="T198">jaʔləm</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_689" n="HIAT:w" s="T199">tura</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_692" n="sc" s="T201">
               <ts e="T208" id="Seg_694" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_696" n="HIAT:w" s="T201">A</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_699" n="HIAT:w" s="T202">teʔtə</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_702" n="HIAT:w" s="T203">kuza:</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_705" n="HIAT:w" s="T204">măn</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_708" n="HIAT:w" s="T205">alam</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_711" n="HIAT:w" s="T206">noʔə</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_714" n="HIAT:w" s="T207">sălomaʔizʼi</ts>
                  <nts id="Seg_715" n="HIAT:ip">.</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_717" n="sc" s="T209">
               <ts e="T213" id="Seg_719" n="HIAT:u" s="T209">
                  <nts id="Seg_720" n="HIAT:ip">(</nts>
                  <ts e="T210" id="Seg_722" n="HIAT:w" s="T209">Su</ts>
                  <nts id="Seg_723" n="HIAT:ip">)</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_726" n="HIAT:w" s="T210">šoləj</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_729" n="HIAT:w" s="T211">bar</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_732" n="HIAT:w" s="T212">togonordə</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T220" id="Seg_735" n="sc" s="T214">
               <ts e="T220" id="Seg_737" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_739" n="HIAT:w" s="T214">Nendləj</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_741" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_743" n="HIAT:w" s="T215">sĭjdən-</ts>
                  <nts id="Seg_744" n="HIAT:ip">)</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_747" n="HIAT:w" s="T216">šindin</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_750" n="HIAT:w" s="T217">togonordə</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_753" n="HIAT:w" s="T218">ej</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_755" n="HIAT:ip">(</nts>
                  <ts e="T220" id="Seg_757" n="HIAT:w" s="T219">nendlən</ts>
                  <nts id="Seg_758" n="HIAT:ip">)</nts>
                  <nts id="Seg_759" n="HIAT:ip">.</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_761" n="sc" s="T221">
               <ts e="T225" id="Seg_763" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_765" n="HIAT:w" s="T221">Dĭ</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_768" n="HIAT:w" s="T222">dĭʔnə</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_771" n="HIAT:w" s="T223">jakšə</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_774" n="HIAT:w" s="T224">moləj</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_777" n="sc" s="T226">
               <ts e="T231" id="Seg_779" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_781" n="HIAT:w" s="T226">A</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_784" n="HIAT:w" s="T227">šindin</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_787" n="HIAT:w" s="T228">togonordə</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_789" n="HIAT:ip">(</nts>
                  <ts e="T230" id="Seg_791" n="HIAT:w" s="T229">nen-</ts>
                  <nts id="Seg_792" n="HIAT:ip">)</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_795" n="HIAT:w" s="T230">nendluʔləj</ts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T236" id="Seg_798" n="sc" s="T232">
               <ts e="T236" id="Seg_800" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_802" n="HIAT:w" s="T232">Dĭʔnə</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_805" n="HIAT:w" s="T233">ej</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_808" n="HIAT:w" s="T234">jakšə</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_811" n="HIAT:w" s="T235">moləj</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_814" n="sc" s="T237">
               <ts e="T240" id="Seg_816" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_818" n="HIAT:w" s="T237">Bostə</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_821" n="HIAT:w" s="T238">ej</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_824" n="HIAT:w" s="T239">nendlil</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_828" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_830" n="HIAT:w" s="T240">Nuʔməluʔləj</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T249" id="Seg_833" n="sc" s="T242">
               <ts e="T249" id="Seg_835" n="HIAT:u" s="T242">
                  <nts id="Seg_836" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_838" n="HIAT:w" s="T242">A</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_841" n="HIAT:w" s="T243">ĭmbi=</ts>
                  <nts id="Seg_842" n="HIAT:ip">)</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_844" n="HIAT:ip">(</nts>
                  <ts e="T245" id="Seg_846" n="HIAT:w" s="T244">aʔ-</ts>
                  <nts id="Seg_847" n="HIAT:ip">)</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_850" n="HIAT:w" s="T245">A</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_853" n="HIAT:w" s="T246">ĭmbi</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_856" n="HIAT:w" s="T247">togonərli</ts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_860" n="HIAT:w" s="T248">nendluʔpi</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T254" id="Seg_863" n="sc" s="T250">
               <ts e="T254" id="Seg_865" n="HIAT:u" s="T250">
                  <nts id="Seg_866" n="HIAT:ip">(</nts>
                  <ts e="T251" id="Seg_868" n="HIAT:w" s="T250">Amnobiʔi</ts>
                  <nts id="Seg_869" n="HIAT:ip">)</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_872" n="HIAT:w" s="T251">Nagur</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_875" n="HIAT:w" s="T252">šoškaʔi</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_878" n="HIAT:w" s="T253">amnobiʔi</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_881" n="sc" s="T255">
               <ts e="T261" id="Seg_883" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_885" n="HIAT:w" s="T255">Dĭgəttə</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_888" n="HIAT:w" s="T256">šide</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_891" n="HIAT:w" s="T257">šoška</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_894" n="HIAT:w" s="T258">ugaːndə</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_897" n="HIAT:w" s="T259">ɨrɨ</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_900" n="HIAT:w" s="T260">ibiʔi</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_904" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_906" n="HIAT:w" s="T261">A</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_909" n="HIAT:w" s="T262">onʼiʔ</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_912" n="HIAT:w" s="T263">šoška</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_915" n="HIAT:w" s="T264">üge</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_918" n="HIAT:w" s="T265">togonərbi</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T273" id="Seg_921" n="sc" s="T267">
               <ts e="T273" id="Seg_923" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_925" n="HIAT:w" s="T267">Dĭzen</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_928" n="HIAT:w" s="T268">bar</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_931" n="HIAT:w" s="T269">jaʔtarlaʔ</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_934" n="HIAT:w" s="T270">mĭmbiʔi</ts>
                  <nts id="Seg_935" n="HIAT:ip">,</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_938" n="HIAT:w" s="T271">ara</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_941" n="HIAT:w" s="T272">bĭʔpiʔi</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_944" n="sc" s="T274">
               <ts e="T281" id="Seg_946" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_948" n="HIAT:w" s="T274">A</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_951" n="HIAT:w" s="T275">dö</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_955" n="HIAT:w" s="T276">onʼiʔ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_958" n="HIAT:w" s="T277">šoška</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_961" n="HIAT:w" s="T278">bar</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_964" n="HIAT:w" s="T279">tĭlbi</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_967" n="HIAT:w" s="T280">dʼü</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T284" id="Seg_970" n="sc" s="T282">
               <ts e="T284" id="Seg_972" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_974" n="HIAT:w" s="T282">Piʔi</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_977" n="HIAT:w" s="T283">oʔbdobiʔi</ts>
                  <nts id="Seg_978" n="HIAT:ip">.</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T291" id="Seg_980" n="sc" s="T285">
               <ts e="T291" id="Seg_982" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_984" n="HIAT:w" s="T285">I</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_987" n="HIAT:w" s="T286">maʔ</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_990" n="HIAT:w" s="T287">boskəndə</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_993" n="HIAT:w" s="T288">abi</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_995" n="HIAT:ip">(</nts>
                  <ts e="T290" id="Seg_997" n="HIAT:w" s="T289">pi-</ts>
                  <nts id="Seg_998" n="HIAT:ip">)</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1001" n="HIAT:w" s="T290">pizeŋdə</ts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T297" id="Seg_1004" n="sc" s="T292">
               <ts e="T297" id="Seg_1006" n="HIAT:u" s="T292">
                  <nts id="Seg_1007" n="HIAT:ip">(</nts>
                  <ts e="T293" id="Seg_1009" n="HIAT:w" s="T292">Dĭ-</ts>
                  <nts id="Seg_1010" n="HIAT:ip">)</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1013" n="HIAT:w" s="T293">Dĭzeŋ</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1016" n="HIAT:w" s="T294">šobiʔi</ts>
                  <nts id="Seg_1017" n="HIAT:ip">,</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1020" n="HIAT:w" s="T295">onʼiʔ</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1023" n="HIAT:w" s="T296">embi</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_1026" n="sc" s="T298">
               <ts e="T302" id="Seg_1028" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1030" n="HIAT:w" s="T298">Muʔ</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1033" n="HIAT:w" s="T299">oʔbdəbi</ts>
                  <nts id="Seg_1034" n="HIAT:ip">,</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1037" n="HIAT:w" s="T300">da</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1040" n="HIAT:w" s="T301">embi</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1044" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1046" n="HIAT:w" s="T302">A</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1049" n="HIAT:w" s="T303">onʼiʔ</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1052" n="HIAT:w" s="T304">sălomaʔizʼiʔ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1054" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_1056" n="HIAT:w" s="T305">a-</ts>
                  <nts id="Seg_1057" n="HIAT:ip">)</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1060" n="HIAT:w" s="T306">embi</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1063" n="HIAT:w" s="T307">boskəndə</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1066" n="HIAT:w" s="T308">maʔ</ts>
                  <nts id="Seg_1067" n="HIAT:ip">.</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T322" id="Seg_1069" n="sc" s="T310">
               <ts e="T316" id="Seg_1071" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_1073" n="HIAT:w" s="T310">A</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1076" n="HIAT:w" s="T311">dĭ</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1079" n="HIAT:w" s="T312">üge</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1082" n="HIAT:w" s="T313">pim</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1085" n="HIAT:w" s="T314">piʔzʼi</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1088" n="HIAT:w" s="T315">endleʔbə</ts>
                  <nts id="Seg_1089" n="HIAT:ip">.</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1092" n="HIAT:u" s="T316">
                  <nts id="Seg_1093" n="HIAT:ip">(</nts>
                  <ts e="T317" id="Seg_1095" n="HIAT:w" s="T316">Dĭ=</ts>
                  <nts id="Seg_1096" n="HIAT:ip">)</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1099" n="HIAT:w" s="T317">Dĭzeŋ</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1102" n="HIAT:w" s="T318">šobiʔi:</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1104" n="HIAT:ip">"</nts>
                  <ts e="T320" id="Seg_1106" n="HIAT:w" s="T319">Ĭmbi</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1109" n="HIAT:w" s="T320">tăn</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1112" n="HIAT:w" s="T321">togonərlial</ts>
                  <nts id="Seg_1113" n="HIAT:ip">?</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T329" id="Seg_1115" n="sc" s="T323">
               <ts e="T329" id="Seg_1117" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1119" n="HIAT:w" s="T323">Miʔ</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1122" n="HIAT:w" s="T324">büžün</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1124" n="HIAT:ip">(</nts>
                  <ts e="T326" id="Seg_1126" n="HIAT:w" s="T325">a-</ts>
                  <nts id="Seg_1127" n="HIAT:ip">)</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1130" n="HIAT:w" s="T326">abibaʔ</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1132" n="HIAT:ip">(</nts>
                  <ts e="T328" id="Seg_1134" n="HIAT:w" s="T327">maʔbi-</ts>
                  <nts id="Seg_1135" n="HIAT:ip">)</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1138" n="HIAT:w" s="T328">maʔdə</ts>
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <nts id="Seg_1140" n="HIAT:ip">.</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T339" id="Seg_1142" n="sc" s="T330">
               <ts e="T339" id="Seg_1144" n="HIAT:u" s="T330">
                  <nts id="Seg_1145" n="HIAT:ip">"</nts>
                  <ts e="T331" id="Seg_1147" n="HIAT:w" s="T330">A</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1149" n="HIAT:ip">(</nts>
                  <ts e="T332" id="Seg_1151" n="HIAT:w" s="T331">der</ts>
                  <nts id="Seg_1152" n="HIAT:ip">)</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1155" n="HIAT:w" s="T332">šindin</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1158" n="HIAT:w" s="T333">jakšə</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1161" n="HIAT:w" s="T334">maʔdə</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1164" n="HIAT:w" s="T335">moləj</ts>
                  <nts id="Seg_1165" n="HIAT:ip">,</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1168" n="HIAT:w" s="T336">măn</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1171" n="HIAT:w" s="T337">ilʼi</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1174" n="HIAT:w" s="T338">šiʔ</ts>
                  <nts id="Seg_1175" n="HIAT:ip">?</nts>
                  <nts id="Seg_1176" n="HIAT:ip">"</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_1178" n="sc" s="T340">
               <ts e="T343" id="Seg_1180" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1182" n="HIAT:w" s="T340">Dĭgəttə</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1185" n="HIAT:w" s="T341">šĭšəge</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1188" n="HIAT:w" s="T342">molaːmbi</ts>
                  <nts id="Seg_1189" n="HIAT:ip">.</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1192" n="HIAT:u" s="T343">
                  <nts id="Seg_1193" n="HIAT:ip">(</nts>
                  <ts e="T344" id="Seg_1195" n="HIAT:w" s="T343">Sĭre=</ts>
                  <nts id="Seg_1196" n="HIAT:ip">)</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1199" n="HIAT:w" s="T344">Sĭre</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1202" n="HIAT:w" s="T345">šonəga</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T354" id="Seg_1205" n="sc" s="T347">
               <ts e="T354" id="Seg_1207" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1209" n="HIAT:w" s="T347">Dĭgəttə</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1212" n="HIAT:w" s="T348">dĭzeŋ</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1214" n="HIAT:ip">(</nts>
                  <ts e="T350" id="Seg_1216" n="HIAT:w" s="T349">pă-</ts>
                  <nts id="Seg_1217" n="HIAT:ip">)</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1220" n="HIAT:w" s="T350">păʔpiʔi</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1223" n="HIAT:w" s="T351">turanə</ts>
                  <nts id="Seg_1224" n="HIAT:ip">,</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1227" n="HIAT:w" s="T352">girgit</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1230" n="HIAT:w" s="T353">săloma</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T359" id="Seg_1233" n="sc" s="T355">
               <ts e="T359" id="Seg_1235" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1237" n="HIAT:w" s="T355">Kajbiʔi</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1240" n="HIAT:w" s="T356">aj</ts>
                  <nts id="Seg_1241" n="HIAT:ip">,</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1244" n="HIAT:w" s="T357">šobi</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1247" n="HIAT:w" s="T358">urgaːba</ts>
                  <nts id="Seg_1248" n="HIAT:ip">.</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T362" id="Seg_1250" n="sc" s="T360">
               <ts e="T362" id="Seg_1252" n="HIAT:u" s="T360">
                  <nts id="Seg_1253" n="HIAT:ip">"</nts>
                  <ts e="T361" id="Seg_1255" n="HIAT:w" s="T360">Öʔləʔgeʔ</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1258" n="HIAT:w" s="T361">măna</ts>
                  <nts id="Seg_1259" n="HIAT:ip">!</nts>
                  <nts id="Seg_1260" n="HIAT:ip">"</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T372" id="Seg_1262" n="sc" s="T363">
               <ts e="T366" id="Seg_1264" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1266" n="HIAT:w" s="T363">Dĭzeŋ</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1269" n="HIAT:w" s="T364">ej</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1272" n="HIAT:w" s="T365">öʔliʔi</ts>
                  <nts id="Seg_1273" n="HIAT:ip">.</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1276" n="HIAT:u" s="T366">
                  <nts id="Seg_1277" n="HIAT:ip">"</nts>
                  <ts e="T367" id="Seg_1279" n="HIAT:w" s="T366">Tüjö</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1282" n="HIAT:w" s="T367">bar</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1285" n="HIAT:w" s="T368">băldəlam</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1288" n="HIAT:w" s="T369">šiʔ</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1290" n="HIAT:ip">(</nts>
                  <ts e="T371" id="Seg_1292" n="HIAT:w" s="T370">turagəʔ=</ts>
                  <nts id="Seg_1293" n="HIAT:ip">)</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1296" n="HIAT:w" s="T371">turanə</ts>
                  <nts id="Seg_1297" n="HIAT:ip">!</nts>
                  <nts id="Seg_1298" n="HIAT:ip">"</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T377" id="Seg_1300" n="sc" s="T373">
               <ts e="T377" id="Seg_1302" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1304" n="HIAT:w" s="T373">Dĭgəttə</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1307" n="HIAT:w" s="T374">băldəbi</ts>
                  <nts id="Seg_1308" n="HIAT:ip">,</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1311" n="HIAT:w" s="T375">dĭzeŋ</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1314" n="HIAT:w" s="T376">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_1315" n="HIAT:ip">.</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T380" id="Seg_1317" n="sc" s="T378">
               <ts e="T380" id="Seg_1319" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1321" n="HIAT:w" s="T378">Girgit</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1324" n="HIAT:w" s="T379">tura</ts>
                  <nts id="Seg_1325" n="HIAT:ip">.</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T383" id="Seg_1327" n="sc" s="T381">
               <ts e="T383" id="Seg_1329" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1331" n="HIAT:w" s="T381">Muzaŋdə</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1334" n="HIAT:w" s="T382">abiʔi</ts>
                  <nts id="Seg_1335" n="HIAT:ip">.</nts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T392" id="Seg_1337" n="sc" s="T384">
               <ts e="T389" id="Seg_1339" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1341" n="HIAT:w" s="T384">I</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1344" n="HIAT:w" s="T385">dĭbər</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1347" n="HIAT:w" s="T386">aj</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1349" n="HIAT:ip">(</nts>
                  <ts e="T388" id="Seg_1351" n="HIAT:w" s="T387">karl-</ts>
                  <nts id="Seg_1352" n="HIAT:ip">)</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1355" n="HIAT:w" s="T388">kajluʔpiʔi</ts>
                  <nts id="Seg_1356" n="HIAT:ip">.</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1359" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1361" n="HIAT:w" s="T389">Dĭ</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1364" n="HIAT:w" s="T390">dĭbər</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1367" n="HIAT:w" s="T391">šobi</ts>
                  <nts id="Seg_1368" n="HIAT:ip">.</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T394" id="Seg_1370" n="sc" s="T393">
               <ts e="T394" id="Seg_1372" n="HIAT:u" s="T393">
                  <nts id="Seg_1373" n="HIAT:ip">"</nts>
                  <ts e="T394" id="Seg_1375" n="HIAT:w" s="T393">Kargaʔ</ts>
                  <nts id="Seg_1376" n="HIAT:ip">!</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T400" id="Seg_1378" n="sc" s="T395">
               <ts e="T400" id="Seg_1380" n="HIAT:u" s="T395">
                  <ts e="T395.tx.1" id="Seg_1382" n="HIAT:w" s="T395">A</ts>
                  <nts id="Seg_1383" n="HIAT:ip">_</nts>
                  <ts e="T396" id="Seg_1385" n="HIAT:w" s="T395.tx.1">to</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1388" n="HIAT:w" s="T396">tüjö</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1391" n="HIAT:w" s="T397">bar</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1394" n="HIAT:w" s="T398">saʔməluʔləj</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1397" n="HIAT:w" s="T399">tura</ts>
                  <nts id="Seg_1398" n="HIAT:ip">"</nts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T406" id="Seg_1401" n="sc" s="T401">
               <ts e="T406" id="Seg_1403" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1405" n="HIAT:w" s="T401">Dĭgəttə</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1408" n="HIAT:w" s="T402">bar</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1411" n="HIAT:w" s="T403">baruʔpi</ts>
                  <nts id="Seg_1412" n="HIAT:ip">,</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1415" n="HIAT:w" s="T404">dĭzeŋ</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1418" n="HIAT:w" s="T405">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_1419" n="HIAT:ip">.</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T411" id="Seg_1421" n="sc" s="T407">
               <ts e="T411" id="Seg_1423" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1425" n="HIAT:w" s="T407">I</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1428" n="HIAT:w" s="T408">šobiʔi</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1431" n="HIAT:w" s="T409">döbər</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1434" n="HIAT:w" s="T410">onʼiʔdə</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T415" id="Seg_1437" n="sc" s="T412">
               <ts e="T415" id="Seg_1439" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1441" n="HIAT:w" s="T412">Nagurdə</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1444" n="HIAT:w" s="T413">šoškandə</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1447" n="HIAT:w" s="T414">šobiʔi</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T428" id="Seg_1450" n="sc" s="T416">
               <ts e="T418" id="Seg_1452" n="HIAT:u" s="T416">
                  <nts id="Seg_1453" n="HIAT:ip">"</nts>
                  <ts e="T417" id="Seg_1455" n="HIAT:w" s="T416">Öʔleʔ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1458" n="HIAT:w" s="T417">miʔnʼibeʔ</ts>
                  <nts id="Seg_1459" n="HIAT:ip">!</nts>
                  <nts id="Seg_1460" n="HIAT:ip">"</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1463" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1465" n="HIAT:w" s="T418">Dĭ</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1468" n="HIAT:w" s="T419">öʔlübi</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1471" n="HIAT:w" s="T420">i</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1474" n="HIAT:w" s="T421">ajbə</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1476" n="HIAT:ip">(</nts>
                  <ts e="T423" id="Seg_1478" n="HIAT:w" s="T422">karb-</ts>
                  <nts id="Seg_1479" n="HIAT:ip">)</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1482" n="HIAT:w" s="T423">kajbi</ts>
                  <nts id="Seg_1483" n="HIAT:ip">.</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1486" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1488" n="HIAT:w" s="T424">Dĭ</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1491" n="HIAT:w" s="T425">šobi</ts>
                  <nts id="Seg_1492" n="HIAT:ip">,</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1495" n="HIAT:w" s="T426">urgaːba</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1499" n="HIAT:u" s="T427">
                  <nts id="Seg_1500" n="HIAT:ip">"</nts>
                  <ts e="T428" id="Seg_1502" n="HIAT:w" s="T427">Öʔleʔ</ts>
                  <nts id="Seg_1503" n="HIAT:ip">!</nts>
                  <nts id="Seg_1504" n="HIAT:ip">"</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T431" id="Seg_1506" n="sc" s="T429">
               <ts e="T431" id="Seg_1508" n="HIAT:u" s="T429">
                  <nts id="Seg_1509" n="HIAT:ip">"</nts>
                  <ts e="T430" id="Seg_1511" n="HIAT:w" s="T429">Ej</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1514" n="HIAT:w" s="T430">öʔləm</ts>
                  <nts id="Seg_1515" n="HIAT:ip">!</nts>
                  <nts id="Seg_1516" n="HIAT:ip">"</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T445" id="Seg_1518" n="sc" s="T432">
               <ts e="T436" id="Seg_1520" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1522" n="HIAT:w" s="T432">Dĭgəttə</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1525" n="HIAT:w" s="T433">dĭ</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1528" n="HIAT:w" s="T434">băldəsʼtə</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1531" n="HIAT:w" s="T435">xatʼel</ts>
                  <nts id="Seg_1532" n="HIAT:ip">.</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1535" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1537" n="HIAT:w" s="T436">Ej</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1540" n="HIAT:w" s="T437">mobi</ts>
                  <nts id="Seg_1541" n="HIAT:ip">.</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1544" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1546" n="HIAT:w" s="T438">Dĭgəttə</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1548" n="HIAT:ip">(</nts>
                  <ts e="T440" id="Seg_1550" n="HIAT:w" s="T439">sʼa-</ts>
                  <nts id="Seg_1551" n="HIAT:ip">)</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1554" n="HIAT:w" s="T440">sʼabi</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1557" n="HIAT:w" s="T441">dĭbər</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1559" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1561" n="HIAT:w" s="T442">nu-</ts>
                  <nts id="Seg_1562" n="HIAT:ip">)</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1565" n="HIAT:w" s="T443">nʼuʔdə</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1568" n="HIAT:w" s="T444">trubandə</ts>
                  <nts id="Seg_1569" n="HIAT:ip">.</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T453" id="Seg_1571" n="sc" s="T446">
               <ts e="T447" id="Seg_1573" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1575" n="HIAT:w" s="T446">Paʔlaːndəga</ts>
                  <nts id="Seg_1576" n="HIAT:ip">.</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1579" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1581" n="HIAT:w" s="T447">A</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1584" n="HIAT:w" s="T448">dĭ</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1587" n="HIAT:w" s="T449">aspaʔ</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1590" n="HIAT:w" s="T450">nuldəbi</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1593" n="HIAT:w" s="T451">tʼibige</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1596" n="HIAT:w" s="T452">büzʼiʔ</ts>
                  <nts id="Seg_1597" n="HIAT:ip">.</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T458" id="Seg_1599" n="sc" s="T454">
               <ts e="T458" id="Seg_1601" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1603" n="HIAT:w" s="T454">Dĭ</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1606" n="HIAT:w" s="T455">bar</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1609" n="HIAT:w" s="T456">saʔməluʔpi</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1612" n="HIAT:w" s="T457">aspaʔdə</ts>
                  <nts id="Seg_1613" n="HIAT:ip">.</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T470" id="Seg_1615" n="sc" s="T459">
               <ts e="T470" id="Seg_1617" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1619" n="HIAT:w" s="T459">I</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1621" n="HIAT:ip">(</nts>
                  <ts e="T461" id="Seg_1623" n="HIAT:w" s="T460">bĭ-</ts>
                  <nts id="Seg_1624" n="HIAT:ip">)</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1627" n="HIAT:w" s="T461">dĭgəttə</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1630" n="HIAT:w" s="T462">paruʔpi</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1633" n="HIAT:w" s="T463">i</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_1636" n="HIAT:w" s="T464">kalla</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1639" n="HIAT:w" s="T966">dʼürbi</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1642" n="HIAT:w" s="T465">i</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1645" n="HIAT:w" s="T466">daška</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1648" n="HIAT:w" s="T467">kaməndə</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1651" n="HIAT:w" s="T468">ej</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1654" n="HIAT:w" s="T469">šobi</ts>
                  <nts id="Seg_1655" n="HIAT:ip">.</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T474" id="Seg_1657" n="sc" s="T471">
               <ts e="T474" id="Seg_1659" n="HIAT:u" s="T471">
                  <nts id="Seg_1660" n="HIAT:ip">(</nts>
                  <ts e="T472" id="Seg_1662" n="HIAT:w" s="T471">Nendəb-</ts>
                  <nts id="Seg_1663" n="HIAT:ip">)</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1666" n="HIAT:w" s="T472">Nendəbiʔi</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1669" n="HIAT:w" s="T473">dĭm</ts>
                  <nts id="Seg_1670" n="HIAT:ip">.</nts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T477" id="Seg_1672" n="sc" s="T475">
               <ts e="T477" id="Seg_1674" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1676" n="HIAT:w" s="T475">I</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_1679" n="HIAT:w" s="T476">kalla</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1682" n="HIAT:w" s="T967">dʼürbi</ts>
                  <nts id="Seg_1683" n="HIAT:ip">.</nts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T484" id="Seg_1685" n="sc" s="T478">
               <ts e="T484" id="Seg_1687" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1689" n="HIAT:w" s="T478">Kumen</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1691" n="HIAT:ip">(</nts>
                  <ts e="T480" id="Seg_1693" n="HIAT:w" s="T479">mi-</ts>
                  <nts id="Seg_1694" n="HIAT:ip">)</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1697" n="HIAT:w" s="T480">miʔ</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1700" n="HIAT:w" s="T481">plʼonkaʔi</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1702" n="HIAT:ip">(</nts>
                  <ts e="T483" id="Seg_1704" n="HIAT:w" s="T482">dʼăbaktərbiam</ts>
                  <nts id="Seg_1705" n="HIAT:ip">)</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1708" n="HIAT:w" s="T483">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_1709" n="HIAT:ip">?</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_1711" n="sc" s="T485">
               <ts e="T487" id="Seg_1713" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1715" n="HIAT:w" s="T485">Bʼeʔ</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1718" n="HIAT:w" s="T486">nagur</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T491" id="Seg_1721" n="sc" s="T488">
               <ts e="T491" id="Seg_1723" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1725" n="HIAT:w" s="T488">Šiʔ</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1728" n="HIAT:w" s="T489">ipekleʔ</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1731" n="HIAT:w" s="T490">ige</ts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T496" id="Seg_1734" n="sc" s="T492">
               <ts e="T496" id="Seg_1736" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1738" n="HIAT:w" s="T492">Ige</ts>
                  <nts id="Seg_1739" n="HIAT:ip">,</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1742" n="HIAT:w" s="T493">a</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1745" n="HIAT:w" s="T494">gijen</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1748" n="HIAT:w" s="T495">ibileʔ</ts>
                  <nts id="Seg_1749" n="HIAT:ip">?</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T501" id="Seg_1751" n="sc" s="T497">
               <ts e="T501" id="Seg_1753" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1755" n="HIAT:w" s="T497">Da</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1758" n="HIAT:w" s="T498">onʼiʔ</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1761" n="HIAT:w" s="T499">ne</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1764" n="HIAT:w" s="T500">deppi</ts>
                  <nts id="Seg_1765" n="HIAT:ip">.</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T504" id="Seg_1767" n="sc" s="T502">
               <ts e="T504" id="Seg_1769" n="HIAT:u" s="T502">
                  <nts id="Seg_1770" n="HIAT:ip">(</nts>
                  <ts e="T503" id="Seg_1772" n="HIAT:w" s="T502">Vasʼ-</ts>
                  <nts id="Seg_1773" n="HIAT:ip">)</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1776" n="HIAT:w" s="T503">Vaznʼesenkaʔizʼi</ts>
                  <nts id="Seg_1777" n="HIAT:ip">.</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T510" id="Seg_1779" n="sc" s="T505">
               <ts e="T510" id="Seg_1781" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1783" n="HIAT:w" s="T505">Onʼiʔ</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1786" n="HIAT:w" s="T506">poʔton</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1789" n="HIAT:w" s="T507">sumna</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1792" n="HIAT:w" s="T508">esseŋdə</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1795" n="HIAT:w" s="T509">ibiʔi</ts>
                  <nts id="Seg_1796" n="HIAT:ip">.</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T515" id="Seg_1798" n="sc" s="T511">
               <ts e="T515" id="Seg_1800" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_1802" n="HIAT:w" s="T511">Dĭ</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1805" n="HIAT:w" s="T512">dĭzem</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1808" n="HIAT:w" s="T513">maʔnən</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1811" n="HIAT:w" s="T514">maːbi</ts>
                  <nts id="Seg_1812" n="HIAT:ip">.</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T518" id="Seg_1814" n="sc" s="T516">
               <ts e="T518" id="Seg_1816" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1818" n="HIAT:w" s="T516">Ajm</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1821" n="HIAT:w" s="T517">kajbi</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T524" id="Seg_1824" n="sc" s="T519">
               <ts e="T524" id="Seg_1826" n="HIAT:u" s="T519">
                  <nts id="Seg_1827" n="HIAT:ip">"</nts>
                  <ts e="T520" id="Seg_1829" n="HIAT:w" s="T519">Šindimdə</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1831" n="HIAT:ip">(</nts>
                  <ts e="T521" id="Seg_1833" n="HIAT:w" s="T520">iʔ</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1836" n="HIAT:w" s="T521">öʔləʔ-</ts>
                  <nts id="Seg_1837" n="HIAT:ip">)</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1840" n="HIAT:w" s="T522">iʔ</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1842" n="HIAT:ip">(</nts>
                  <ts e="T524" id="Seg_1844" n="HIAT:w" s="T523">öʔləʔleʔ</ts>
                  <nts id="Seg_1845" n="HIAT:ip">)</nts>
                  <nts id="Seg_1846" n="HIAT:ip">!</nts>
                  <nts id="Seg_1847" n="HIAT:ip">"</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T527" id="Seg_1849" n="sc" s="T525">
               <ts e="T527" id="Seg_1851" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_1853" n="HIAT:w" s="T525">Dĭgəttə</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_1856" n="HIAT:w" s="T526">kalla</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1859" n="HIAT:w" s="T968">dʼürbi</ts>
                  <nts id="Seg_1860" n="HIAT:ip">.</nts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T532" id="Seg_1862" n="sc" s="T528">
               <ts e="T530" id="Seg_1864" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_1866" n="HIAT:w" s="T528">Urgaːba</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1869" n="HIAT:w" s="T529">šobi</ts>
                  <nts id="Seg_1870" n="HIAT:ip">.</nts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_1873" n="HIAT:u" s="T530">
                  <nts id="Seg_1874" n="HIAT:ip">"</nts>
                  <ts e="T531" id="Seg_1876" n="HIAT:w" s="T530">Kargat</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1879" n="HIAT:w" s="T531">aj</ts>
                  <nts id="Seg_1880" n="HIAT:ip">!</nts>
                  <nts id="Seg_1881" n="HIAT:ip">"</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T536" id="Seg_1883" n="sc" s="T533">
               <ts e="T536" id="Seg_1885" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_1887" n="HIAT:w" s="T533">Dĭzeŋ</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1890" n="HIAT:w" s="T534">ej</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1893" n="HIAT:w" s="T535">karlaʔi</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T543" id="Seg_1896" n="sc" s="T537">
               <ts e="T543" id="Seg_1898" n="HIAT:u" s="T537">
                  <nts id="Seg_1899" n="HIAT:ip">"</nts>
                  <ts e="T538" id="Seg_1901" n="HIAT:w" s="T537">Miʔnʼibeʔ</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1904" n="HIAT:w" s="T538">iam</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1907" n="HIAT:w" s="T539">mămbi</ts>
                  <nts id="Seg_1908" n="HIAT:ip">,</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1911" n="HIAT:w" s="T540">šindimdə</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1914" n="HIAT:w" s="T541">ej</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1917" n="HIAT:w" s="T542">öʔləʔsittə</ts>
                  <nts id="Seg_1918" n="HIAT:ip">"</nts>
                  <nts id="Seg_1919" n="HIAT:ip">.</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T549" id="Seg_1921" n="sc" s="T544">
               <ts e="T549" id="Seg_1923" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1925" n="HIAT:w" s="T544">Dĭgəttə</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1928" n="HIAT:w" s="T545">urgaːba</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_1931" n="HIAT:w" s="T546">kalla</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1934" n="HIAT:w" s="T969">dʼürbi</ts>
                  <nts id="Seg_1935" n="HIAT:ip">,</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1938" n="HIAT:w" s="T547">iat</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1941" n="HIAT:w" s="T548">šobi</ts>
                  <nts id="Seg_1942" n="HIAT:ip">.</nts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T555" id="Seg_1944" n="sc" s="T550">
               <ts e="T555" id="Seg_1946" n="HIAT:u" s="T550">
                  <nts id="Seg_1947" n="HIAT:ip">"</nts>
                  <ts e="T551" id="Seg_1949" n="HIAT:w" s="T550">Kargaʔ</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1952" n="HIAT:w" s="T551">aj</ts>
                  <nts id="Seg_1953" n="HIAT:ip">,</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1956" n="HIAT:w" s="T552">măn</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1959" n="HIAT:w" s="T553">deʔpiem</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1962" n="HIAT:w" s="T554">süt</ts>
                  <nts id="Seg_1963" n="HIAT:ip">!</nts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T561" id="Seg_1965" n="sc" s="T556">
               <ts e="T559" id="Seg_1967" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1969" n="HIAT:w" s="T556">Deʔpiem</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1972" n="HIAT:w" s="T557">noʔ</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1975" n="HIAT:w" s="T558">šiʔnʼileʔ</ts>
                  <nts id="Seg_1976" n="HIAT:ip">!</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_1979" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_1981" n="HIAT:w" s="T559">Tvorog</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1984" n="HIAT:w" s="T560">deʔpiem</ts>
                  <nts id="Seg_1985" n="HIAT:ip">"</nts>
                  <nts id="Seg_1986" n="HIAT:ip">.</nts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_1988" n="sc" s="T562">
               <ts e="T566" id="Seg_1990" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_1992" n="HIAT:w" s="T562">Dĭgəttə</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1995" n="HIAT:w" s="T563">dĭzeŋ</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1998" n="HIAT:w" s="T564">karbiʔi</ts>
                  <nts id="Seg_1999" n="HIAT:ip">,</nts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2002" n="HIAT:w" s="T565">ambiʔi</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2006" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2008" n="HIAT:w" s="T566">Dĭgəttə</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2011" n="HIAT:w" s="T567">iat</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2014" n="HIAT:w" s="T568">bazoʔ</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2017" n="HIAT:w" s="T569">kambi</ts>
                  <nts id="Seg_2018" n="HIAT:ip">.</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T574" id="Seg_2020" n="sc" s="T571">
               <ts e="T574" id="Seg_2022" n="HIAT:u" s="T571">
                  <nts id="Seg_2023" n="HIAT:ip">"</nts>
                  <ts e="T572" id="Seg_2025" n="HIAT:w" s="T571">Iʔ</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2028" n="HIAT:w" s="T572">öʔləʔgeʔ</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2031" n="HIAT:w" s="T573">šindimdə</ts>
                  <nts id="Seg_2032" n="HIAT:ip">!</nts>
                  <nts id="Seg_2033" n="HIAT:ip">"</nts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_2035" n="sc" s="T575">
               <ts e="T577" id="Seg_2037" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2039" n="HIAT:w" s="T575">Bazoʔ</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_2042" n="HIAT:w" s="T576">kalla</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2045" n="HIAT:w" s="T970">dʼürbi</ts>
                  <nts id="Seg_2046" n="HIAT:ip">.</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T581" id="Seg_2048" n="sc" s="T578">
               <ts e="T581" id="Seg_2050" n="HIAT:u" s="T578">
                  <nts id="Seg_2051" n="HIAT:ip">"</nts>
                  <ts e="T579" id="Seg_2053" n="HIAT:w" s="T578">Šindimdə</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2056" n="HIAT:w" s="T579">iʔ</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2059" n="HIAT:w" s="T580">öʔləʔ</ts>
                  <nts id="Seg_2060" n="HIAT:ip">!</nts>
                  <nts id="Seg_2061" n="HIAT:ip">"</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_2063" n="sc" s="T582">
               <ts e="T586" id="Seg_2065" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2067" n="HIAT:w" s="T582">Dĭgəttə</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2070" n="HIAT:w" s="T583">bazoʔ</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2073" n="HIAT:w" s="T584">šobi</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2076" n="HIAT:w" s="T585">urgaːba</ts>
                  <nts id="Seg_2077" n="HIAT:ip">.</nts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T591" id="Seg_2079" n="sc" s="T587">
               <ts e="T591" id="Seg_2081" n="HIAT:u" s="T587">
                  <nts id="Seg_2082" n="HIAT:ip">"</nts>
                  <ts e="T588" id="Seg_2084" n="HIAT:w" s="T587">Esseŋ</ts>
                  <nts id="Seg_2085" n="HIAT:ip">,</nts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2088" n="HIAT:w" s="T588">esseŋ</ts>
                  <nts id="Seg_2089" n="HIAT:ip">,</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2092" n="HIAT:w" s="T589">öʔleʔ</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2095" n="HIAT:w" s="T590">măna</ts>
                  <nts id="Seg_2096" n="HIAT:ip">!</nts>
                  <nts id="Seg_2097" n="HIAT:ip">"</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T597" id="Seg_2099" n="sc" s="T592">
               <ts e="T597" id="Seg_2101" n="HIAT:u" s="T592">
                  <nts id="Seg_2102" n="HIAT:ip">"</nts>
                  <ts e="T593" id="Seg_2104" n="HIAT:w" s="T592">Dʼok</ts>
                  <nts id="Seg_2105" n="HIAT:ip">,</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2107" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_2109" n="HIAT:w" s="T593">mɨ-</ts>
                  <nts id="Seg_2110" n="HIAT:ip">)</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2113" n="HIAT:w" s="T594">miʔ</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2116" n="HIAT:w" s="T595">ej</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2118" n="HIAT:ip">(</nts>
                  <ts e="T597" id="Seg_2120" n="HIAT:w" s="T596">öʔleʔ</ts>
                  <nts id="Seg_2121" n="HIAT:ip">)</nts>
                  <nts id="Seg_2122" n="HIAT:ip">"</nts>
                  <nts id="Seg_2123" n="HIAT:ip">.</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T609" id="Seg_2125" n="sc" s="T598">
               <ts e="T603" id="Seg_2127" n="HIAT:u" s="T598">
                  <nts id="Seg_2128" n="HIAT:ip">"</nts>
                  <ts e="T599" id="Seg_2130" n="HIAT:w" s="T598">A</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2133" n="HIAT:w" s="T599">măn</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2136" n="HIAT:w" s="T600">šiʔ</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2139" n="HIAT:w" s="T601">ia</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2142" n="HIAT:w" s="T602">igem</ts>
                  <nts id="Seg_2143" n="HIAT:ip">!</nts>
                  <nts id="Seg_2144" n="HIAT:ip">"</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2147" n="HIAT:u" s="T603">
                  <nts id="Seg_2148" n="HIAT:ip">"</nts>
                  <ts e="T604" id="Seg_2150" n="HIAT:w" s="T603">Dʼok</ts>
                  <nts id="Seg_2151" n="HIAT:ip">,</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2154" n="HIAT:w" s="T604">măn</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2156" n="HIAT:ip">(</nts>
                  <ts e="T606" id="Seg_2158" n="HIAT:w" s="T605">ia-</ts>
                  <nts id="Seg_2159" n="HIAT:ip">)</nts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2162" n="HIAT:w" s="T606">ianə</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2165" n="HIAT:w" s="T607">golosdə</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2168" n="HIAT:w" s="T608">todam</ts>
                  <nts id="Seg_2169" n="HIAT:ip">!</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T615" id="Seg_2171" n="sc" s="T610">
               <ts e="T615" id="Seg_2173" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2175" n="HIAT:w" s="T610">A</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2178" n="HIAT:w" s="T611">tăn</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2181" n="HIAT:w" s="T612">golosdə</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2184" n="HIAT:w" s="T613">ugaːndə</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2187" n="HIAT:w" s="T614">nʼešpək</ts>
                  <nts id="Seg_2188" n="HIAT:ip">"</nts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T622" id="Seg_2191" n="sc" s="T616">
               <ts e="T619" id="Seg_2193" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2195" n="HIAT:w" s="T616">Dĭgəttə</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2198" n="HIAT:w" s="T617">urgaːba</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_2201" n="HIAT:w" s="T618">kalla</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2204" n="HIAT:w" s="T971">dʼürbi</ts>
                  <nts id="Seg_2205" n="HIAT:ip">.</nts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2208" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2210" n="HIAT:w" s="T619">Dĭgəttə</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2213" n="HIAT:w" s="T620">iat</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2216" n="HIAT:w" s="T621">šobi</ts>
                  <nts id="Seg_2217" n="HIAT:ip">.</nts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T626" id="Seg_2219" n="sc" s="T623">
               <ts e="T626" id="Seg_2221" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2223" n="HIAT:w" s="T623">Dĭzeŋ</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2226" n="HIAT:w" s="T624">karbiʔi</ts>
                  <nts id="Seg_2227" n="HIAT:ip">,</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2230" n="HIAT:w" s="T625">bădəbi</ts>
                  <nts id="Seg_2231" n="HIAT:ip">.</nts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_2233" n="sc" s="T627">
               <ts e="T631" id="Seg_2235" n="HIAT:u" s="T627">
                  <nts id="Seg_2236" n="HIAT:ip">"</nts>
                  <ts e="T628" id="Seg_2238" n="HIAT:w" s="T627">Tüj</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2241" n="HIAT:w" s="T628">šindinədə</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2244" n="HIAT:w" s="T629">ej</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2247" n="HIAT:w" s="T630">öʔləʔ</ts>
                  <nts id="Seg_2248" n="HIAT:ip">!</nts>
                  <nts id="Seg_2249" n="HIAT:ip">"</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T636" id="Seg_2251" n="sc" s="T632">
               <ts e="T636" id="Seg_2253" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2255" n="HIAT:w" s="T632">A</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2258" n="HIAT:w" s="T633">urgaːba</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2261" n="HIAT:w" s="T634">kambi</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2264" n="HIAT:w" s="T635">kuznʼitsanə</ts>
                  <nts id="Seg_2265" n="HIAT:ip">.</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T641" id="Seg_2267" n="sc" s="T637">
               <ts e="T641" id="Seg_2269" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2271" n="HIAT:w" s="T637">Šĭkebə</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2273" n="HIAT:ip">(</nts>
                  <ts e="T639" id="Seg_2275" n="HIAT:w" s="T638">pi-</ts>
                  <nts id="Seg_2276" n="HIAT:ip">)</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2279" n="HIAT:w" s="T639">abi</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2282" n="HIAT:w" s="T640">todam</ts>
                  <nts id="Seg_2283" n="HIAT:ip">.</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T645" id="Seg_2285" n="sc" s="T642">
               <ts e="T645" id="Seg_2287" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2289" n="HIAT:w" s="T642">Dĭgəttə</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2292" n="HIAT:w" s="T643">šobi</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2295" n="HIAT:w" s="T644">bazoʔ</ts>
                  <nts id="Seg_2296" n="HIAT:ip">.</nts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T653" id="Seg_2298" n="sc" s="T646">
               <ts e="T653" id="Seg_2300" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2302" n="HIAT:w" s="T646">Dĭ</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2305" n="HIAT:w" s="T647">mălia:</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2307" n="HIAT:ip">"</nts>
                  <ts e="T649" id="Seg_2309" n="HIAT:w" s="T648">Măn</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2312" n="HIAT:w" s="T649">šiʔ</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2315" n="HIAT:w" s="T650">ial</ts>
                  <nts id="Seg_2316" n="HIAT:ip">,</nts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2319" n="HIAT:w" s="T651">deʔpiem</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2322" n="HIAT:w" s="T652">süt</ts>
                  <nts id="Seg_2323" n="HIAT:ip">.</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T656" id="Seg_2325" n="sc" s="T654">
               <ts e="T656" id="Seg_2327" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2329" n="HIAT:w" s="T654">Ipek</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2332" n="HIAT:w" s="T655">deʔpiem</ts>
                  <nts id="Seg_2333" n="HIAT:ip">.</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T661" id="Seg_2335" n="sc" s="T657">
               <ts e="T661" id="Seg_2337" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2339" n="HIAT:w" s="T657">Deʔpiem</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2342" n="HIAT:w" s="T658">šiʔnʼileʔ</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2345" n="HIAT:w" s="T659">творог</ts>
                  <nts id="Seg_2346" n="HIAT:ip">,</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2349" n="HIAT:w" s="T660">noʔ</ts>
                  <nts id="Seg_2350" n="HIAT:ip">"</nts>
                  <nts id="Seg_2351" n="HIAT:ip">.</nts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T665" id="Seg_2353" n="sc" s="T662">
               <ts e="T665" id="Seg_2355" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2357" n="HIAT:w" s="T662">Dĭzeŋ</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2360" n="HIAT:w" s="T663">bar</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2363" n="HIAT:w" s="T664">karluʔpi</ts>
                  <nts id="Seg_2364" n="HIAT:ip">.</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T670" id="Seg_2366" n="sc" s="T666">
               <ts e="T670" id="Seg_2368" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_2370" n="HIAT:w" s="T666">Dĭ</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2373" n="HIAT:w" s="T667">dĭzem</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2376" n="HIAT:w" s="T668">bar</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2379" n="HIAT:w" s="T669">amnuʔpi</ts>
                  <nts id="Seg_2380" n="HIAT:ip">.</nts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T673" id="Seg_2382" n="sc" s="T671">
               <ts e="T673" id="Seg_2384" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2386" n="HIAT:w" s="T671">Iat</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2389" n="HIAT:w" s="T672">šobi</ts>
                  <nts id="Seg_2390" n="HIAT:ip">.</nts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T681" id="Seg_2392" n="sc" s="T674">
               <ts e="T676" id="Seg_2394" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2396" n="HIAT:w" s="T674">Esseŋdə</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2399" n="HIAT:w" s="T675">naga</ts>
                  <nts id="Seg_2400" n="HIAT:ip">.</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2403" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2405" n="HIAT:w" s="T676">Onʼiʔ</ts>
                  <nts id="Seg_2406" n="HIAT:ip">,</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2409" n="HIAT:w" s="T677">самый</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2411" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2413" n="HIAT:w" s="T678">üdʼü-</ts>
                  <nts id="Seg_2414" n="HIAT:ip">)</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2417" n="HIAT:w" s="T679">üdʼüge</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2420" n="HIAT:w" s="T680">šaʔlaːmbi</ts>
                  <nts id="Seg_2421" n="HIAT:ip">.</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T687" id="Seg_2423" n="sc" s="T682">
               <ts e="T687" id="Seg_2425" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2427" n="HIAT:w" s="T682">Dĭgəttə</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2430" n="HIAT:w" s="T683">măndə:</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2432" n="HIAT:ip">"</nts>
                  <ts e="T685" id="Seg_2434" n="HIAT:w" s="T684">Urgaːba</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2437" n="HIAT:w" s="T685">bar</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2440" n="HIAT:w" s="T686">amnuʔpi</ts>
                  <nts id="Seg_2441" n="HIAT:ip">.</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T704" id="Seg_2443" n="sc" s="T688">
               <ts e="T691" id="Seg_2445" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2447" n="HIAT:w" s="T688">Măn</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2450" n="HIAT:w" s="T689">unnʼa</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2453" n="HIAT:w" s="T690">maːluʔpiam</ts>
                  <nts id="Seg_2454" n="HIAT:ip">.</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2457" n="HIAT:u" s="T691">
                  <nts id="Seg_2458" n="HIAT:ip">(</nts>
                  <ts e="T692" id="Seg_2460" n="HIAT:w" s="T691">Pʼešdə</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2463" n="HIAT:w" s="T692">ša-</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2466" n="HIAT:w" s="T693">sabi-</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2469" n="HIAT:w" s="T694">š-</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2472" n="HIAT:w" s="T695">sʼa-</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2475" n="HIAT:w" s="T696">š-</ts>
                  <nts id="Seg_2476" n="HIAT:ip">)</nts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2479" n="HIAT:w" s="T697">Pʼešdə</ts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2482" n="HIAT:w" s="T698">păʔlumbiam</ts>
                  <nts id="Seg_2483" n="HIAT:ip">,</nts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2486" n="HIAT:w" s="T699">dĭ</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2489" n="HIAT:w" s="T700">măna</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2492" n="HIAT:w" s="T701">ej</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2495" n="HIAT:w" s="T702">măndərbi</ts>
                  <nts id="Seg_2496" n="HIAT:ip">,</nts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_2499" n="HIAT:w" s="T703">kalla</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2502" n="HIAT:w" s="T972">dʼürbi</ts>
                  <nts id="Seg_2503" n="HIAT:ip">.</nts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T709" id="Seg_2505" n="sc" s="T705">
               <ts e="T709" id="Seg_2507" n="HIAT:u" s="T705">
                  <ts e="T706" id="Seg_2509" n="HIAT:w" s="T705">Dĭgəttə</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2512" n="HIAT:w" s="T706">urgaːba</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2515" n="HIAT:w" s="T707">šobi</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2518" n="HIAT:w" s="T708">poʔtonə</ts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T715" id="Seg_2521" n="sc" s="T710">
               <ts e="T715" id="Seg_2523" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2525" n="HIAT:w" s="T710">Poʔto</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2528" n="HIAT:w" s="T711">măndə:</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2530" n="HIAT:ip">"</nts>
                  <nts id="Seg_2531" n="HIAT:ip">(</nts>
                  <ts e="T713" id="Seg_2533" n="HIAT:w" s="T712">Kan-</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2536" n="HIAT:w" s="T713">kal-</ts>
                  <nts id="Seg_2537" n="HIAT:ip">)</nts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2540" n="HIAT:w" s="T714">Kanžəbəj</ts>
                  <nts id="Seg_2541" n="HIAT:ip">.</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T720" id="Seg_2543" n="sc" s="T716">
               <ts e="T720" id="Seg_2545" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2547" n="HIAT:w" s="T716">Suʔmibəj</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2550" n="HIAT:w" s="T717">dö</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2553" n="HIAT:w" s="T718">jamagə</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2555" n="HIAT:ip">(</nts>
                  <ts e="T720" id="Seg_2557" n="HIAT:w" s="T719">penzəj</ts>
                  <nts id="Seg_2558" n="HIAT:ip">)</nts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_2561" n="sc" s="T721">
               <ts e="T723" id="Seg_2563" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2565" n="HIAT:w" s="T721">Poʔto</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2568" n="HIAT:w" s="T722">suʔməbi</ts>
                  <nts id="Seg_2569" n="HIAT:ip">.</nts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T731" id="Seg_2571" n="sc" s="T724">
               <ts e="T731" id="Seg_2573" n="HIAT:u" s="T724">
                  <nts id="Seg_2574" n="HIAT:ip">(</nts>
                  <ts e="T725" id="Seg_2576" n="HIAT:w" s="T724">A-</ts>
                  <nts id="Seg_2577" n="HIAT:ip">)</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2580" n="HIAT:w" s="T725">A</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2583" n="HIAT:w" s="T726">dĭ</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2586" n="HIAT:w" s="T727">ej</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2589" n="HIAT:w" s="T728">mobi</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2592" n="HIAT:w" s="T729">suʔmistə</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2595" n="HIAT:w" s="T730">dĭbər</ts>
                  <nts id="Seg_2596" n="HIAT:ip">.</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T753" id="Seg_2598" n="sc" s="T732">
               <ts e="T737" id="Seg_2600" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2602" n="HIAT:w" s="T732">Kuzurluʔpi</ts>
                  <nts id="Seg_2603" n="HIAT:ip">,</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2606" n="HIAT:w" s="T733">ĭmbinəndə</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2609" n="HIAT:w" s="T734">bar</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2612" n="HIAT:w" s="T735">nanə</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2615" n="HIAT:w" s="T736">напорол</ts>
                  <nts id="Seg_2616" n="HIAT:ip">.</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_2619" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2621" n="HIAT:w" s="T737">Dĭgəttə</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2624" n="HIAT:w" s="T738">dĭn</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2626" n="HIAT:ip">(</nts>
                  <ts e="T740" id="Seg_2628" n="HIAT:w" s="T739">kozl-</ts>
                  <nts id="Seg_2629" n="HIAT:ip">)</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2632" n="HIAT:w" s="T740">kăzlʼataʔji</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2634" n="HIAT:ip">(</nts>
                  <ts e="T742" id="Seg_2636" n="HIAT:w" s="T741">s-</ts>
                  <nts id="Seg_2637" n="HIAT:ip">)</nts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2640" n="HIAT:w" s="T742">sumiʔluʔpiʔi</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2643" n="HIAT:w" s="T743">i</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2646" n="HIAT:w" s="T744">bazoʔ</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2649" n="HIAT:w" s="T745">iandə</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_2652" n="HIAT:w" s="T746">kalla</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2655" n="HIAT:w" s="T973">dʼürbiʔi</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2659" n="HIAT:u" s="T747">
                  <nts id="Seg_2660" n="HIAT:ip">(</nts>
                  <nts id="Seg_2661" n="HIAT:ip">(</nts>
                  <ats e="T748" id="Seg_2662" n="HIAT:non-pho" s="T747">BRK</ats>
                  <nts id="Seg_2663" n="HIAT:ip">)</nts>
                  <nts id="Seg_2664" n="HIAT:ip">)</nts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2666" n="HIAT:ip">"</nts>
                  <ts e="T749" id="Seg_2668" n="HIAT:w" s="T748">Kanžəbəj</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2671" n="HIAT:w" s="T749">gibər-nʼibudʼ</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2674" n="HIAT:w" s="T750">tănzi</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2676" n="HIAT:ip">(</nts>
                  <ts e="T752" id="Seg_2678" n="HIAT:w" s="T751">jaʔt-</ts>
                  <nts id="Seg_2679" n="HIAT:ip">)</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2682" n="HIAT:w" s="T752">jaʔtarla</ts>
                  <nts id="Seg_2683" n="HIAT:ip">"</nts>
                  <nts id="Seg_2684" n="HIAT:ip">.</nts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T758" id="Seg_2686" n="sc" s="T754">
               <ts e="T758" id="Seg_2688" n="HIAT:u" s="T754">
                  <nts id="Seg_2689" n="HIAT:ip">"</nts>
                  <ts e="T755" id="Seg_2691" n="HIAT:w" s="T754">A</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2694" n="HIAT:w" s="T755">ĭmbi</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2697" n="HIAT:w" s="T756">dĭn</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2700" n="HIAT:w" s="T757">moləj</ts>
                  <nts id="Seg_2701" n="HIAT:ip">?</nts>
                  <nts id="Seg_2702" n="HIAT:ip">"</nts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T766" id="Seg_2704" n="sc" s="T759">
               <ts e="T764" id="Seg_2706" n="HIAT:u" s="T759">
                  <nts id="Seg_2707" n="HIAT:ip">"</nts>
                  <ts e="T760" id="Seg_2709" n="HIAT:w" s="T759">Da</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2712" n="HIAT:w" s="T760">ĭmbi</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2715" n="HIAT:w" s="T761">moləj</ts>
                  <nts id="Seg_2716" n="HIAT:ip">,</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2719" n="HIAT:w" s="T762">ara</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2722" n="HIAT:w" s="T763">bĭtləbeʔ</ts>
                  <nts id="Seg_2723" n="HIAT:ip">.</nts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T766" id="Seg_2726" n="HIAT:u" s="T764">
                  <ts e="T765" id="Seg_2728" n="HIAT:w" s="T764">Nüjnə</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2731" n="HIAT:w" s="T765">nüjləbeʔ</ts>
                  <nts id="Seg_2732" n="HIAT:ip">.</nts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T770" id="Seg_2734" n="sc" s="T767">
               <ts e="T768" id="Seg_2736" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_2738" n="HIAT:w" s="T767">Suʔmiləbeʔ</ts>
                  <nts id="Seg_2739" n="HIAT:ip">.</nts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_2742" n="HIAT:u" s="T768">
                  <ts e="T769" id="Seg_2744" n="HIAT:w" s="T768">Garmonnʼiazi</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2747" n="HIAT:w" s="T769">sʼarzbibeʔ</ts>
                  <nts id="Seg_2748" n="HIAT:ip">.</nts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T779" id="Seg_2750" n="sc" s="T771">
               <ts e="T779" id="Seg_2752" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_2754" n="HIAT:w" s="T771">Dĭn</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2757" n="HIAT:w" s="T772">bar</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2760" n="HIAT:w" s="T773">ĭmbi</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2763" n="HIAT:w" s="T774">iʔgö</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2765" n="HIAT:ip">—</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2768" n="HIAT:w" s="T775">uja</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2771" n="HIAT:w" s="T776">iʔgö</ts>
                  <nts id="Seg_2772" n="HIAT:ip">,</nts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2774" n="HIAT:ip">(</nts>
                  <ts e="T778" id="Seg_2776" n="HIAT:w" s="T777">seŋ</ts>
                  <nts id="Seg_2777" n="HIAT:ip">)</nts>
                  <nts id="Seg_2778" n="HIAT:ip">,</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2781" n="HIAT:w" s="T778">kajaʔ</ts>
                  <nts id="Seg_2782" n="HIAT:ip">.</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T782" id="Seg_2784" n="sc" s="T780">
               <ts e="T782" id="Seg_2786" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_2788" n="HIAT:w" s="T780">Kola</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2791" n="HIAT:w" s="T781">mĭnzerona</ts>
                  <nts id="Seg_2792" n="HIAT:ip">.</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T784" id="Seg_2794" n="sc" s="T783">
               <ts e="T784" id="Seg_2796" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_2798" n="HIAT:w" s="T783">Ipek</ts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T787" id="Seg_2801" n="sc" s="T785">
               <ts e="T787" id="Seg_2803" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_2805" n="HIAT:w" s="T785">Sĭreʔpne</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2808" n="HIAT:w" s="T786">nuga</ts>
                  <nts id="Seg_2809" n="HIAT:ip">.</nts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T796" id="Seg_2811" n="sc" s="T788">
               <ts e="T796" id="Seg_2813" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_2815" n="HIAT:w" s="T788">Lem</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2818" n="HIAT:w" s="T789">keʔbdeʔi</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2821" n="HIAT:w" s="T790">ige</ts>
                  <nts id="Seg_2822" n="HIAT:ip">,</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2825" n="HIAT:w" s="T791">kobo</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2828" n="HIAT:w" s="T792">keʔbdeʔi</ts>
                  <nts id="Seg_2829" n="HIAT:ip">,</nts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2832" n="HIAT:w" s="T793">sagər</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2835" n="HIAT:w" s="T794">keʔbdeʔi</ts>
                  <nts id="Seg_2836" n="HIAT:ip">,</nts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2839" n="HIAT:w" s="T795">kanžəbəj</ts>
                  <nts id="Seg_2840" n="HIAT:ip">.</nts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T800" id="Seg_2842" n="sc" s="T797">
               <ts e="T800" id="Seg_2844" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_2846" n="HIAT:w" s="T797">Tăn</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2849" n="HIAT:w" s="T798">turaʔi</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2851" n="HIAT:ip">(</nts>
                  <ts e="T800" id="Seg_2853" n="HIAT:w" s="T799">kürbil</ts>
                  <nts id="Seg_2854" n="HIAT:ip">)</nts>
                  <nts id="Seg_2855" n="HIAT:ip">?</nts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T802" id="Seg_2857" n="sc" s="T801">
               <ts e="T802" id="Seg_2859" n="HIAT:u" s="T801">
                  <nts id="Seg_2860" n="HIAT:ip">(</nts>
                  <ts e="T802" id="Seg_2862" n="HIAT:w" s="T801">Kürbiel</ts>
                  <nts id="Seg_2863" n="HIAT:ip">)</nts>
                  <nts id="Seg_2864" n="HIAT:ip">.</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T806" id="Seg_2866" n="sc" s="T803">
               <ts e="T806" id="Seg_2868" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_2870" n="HIAT:w" s="T803">Kumən</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2873" n="HIAT:w" s="T804">tura</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2876" n="HIAT:w" s="T805">kürbiel</ts>
                  <nts id="Seg_2877" n="HIAT:ip">?</nts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T808" id="Seg_2879" n="sc" s="T807">
               <ts e="T808" id="Seg_2881" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_2883" n="HIAT:w" s="T807">Sejʔpü</ts>
                  <nts id="Seg_2884" n="HIAT:ip">.</nts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T812" id="Seg_2886" n="sc" s="T809">
               <ts e="T812" id="Seg_2888" n="HIAT:u" s="T809">
                  <nts id="Seg_2889" n="HIAT:ip">(</nts>
                  <ts e="T810" id="Seg_2891" n="HIAT:w" s="T809">Tăn-nu</ts>
                  <nts id="Seg_2892" n="HIAT:ip">)</nts>
                  <nts id="Seg_2893" n="HIAT:ip">,</nts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2896" n="HIAT:w" s="T810">turazaŋdə</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2899" n="HIAT:w" s="T811">kürbiel</ts>
                  <nts id="Seg_2900" n="HIAT:ip">?</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T827" id="Seg_2902" n="sc" s="T813">
               <ts e="T818" id="Seg_2904" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_2906" n="HIAT:w" s="T813">Tăn</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2908" n="HIAT:ip">(</nts>
                  <ts e="T815" id="Seg_2910" n="HIAT:w" s="T814">d-</ts>
                  <nts id="Seg_2911" n="HIAT:ip">)</nts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2914" n="HIAT:w" s="T815">dʼijenə</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2917" n="HIAT:w" s="T816">ej</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2920" n="HIAT:w" s="T817">mumbiel</ts>
                  <nts id="Seg_2921" n="HIAT:ip">?</nts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_2924" n="HIAT:u" s="T818">
                  <ts e="T819" id="Seg_2926" n="HIAT:w" s="T818">Ej</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2929" n="HIAT:w" s="T819">mĭmbiem</ts>
                  <nts id="Seg_2930" n="HIAT:ip">.</nts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_2933" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2935" n="HIAT:w" s="T820">Măn</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2938" n="HIAT:w" s="T821">pimniem</ts>
                  <nts id="Seg_2939" n="HIAT:ip">,</nts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2942" n="HIAT:w" s="T822">dĭn</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2945" n="HIAT:w" s="T823">bar</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2948" n="HIAT:w" s="T824">urgaːba</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2951" n="HIAT:w" s="T825">amno</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2953" n="HIAT:ip">(</nts>
                  <nts id="Seg_2954" n="HIAT:ip">(</nts>
                  <ats e="T827" id="Seg_2955" n="HIAT:non-pho" s="T826">…</ats>
                  <nts id="Seg_2956" n="HIAT:ip">)</nts>
                  <nts id="Seg_2957" n="HIAT:ip">)</nts>
                  <nts id="Seg_2958" n="HIAT:ip">.</nts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T832" id="Seg_2960" n="sc" s="T828">
               <ts e="T832" id="Seg_2962" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_2964" n="HIAT:w" s="T828">Ĭmbi</ts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2967" n="HIAT:w" s="T829">pimniel</ts>
                  <nts id="Seg_2968" n="HIAT:ip">,</nts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2971" n="HIAT:w" s="T830">kanaʔ</ts>
                  <nts id="Seg_2972" n="HIAT:ip">,</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2974" n="HIAT:ip">(</nts>
                  <ts e="T832" id="Seg_2976" n="HIAT:w" s="T831">ты</ts>
                  <nts id="Seg_2977" n="HIAT:ip">)</nts>
                  <nts id="Seg_2978" n="HIAT:ip">!</nts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T839" id="Seg_2980" n="sc" s="T833">
               <ts e="T839" id="Seg_2982" n="HIAT:u" s="T833">
                  <ts e="T834" id="Seg_2984" n="HIAT:w" s="T833">Ĭmbidə</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2986" n="HIAT:ip">(</nts>
                  <ts e="T835" id="Seg_2988" n="HIAT:w" s="T834">ej</ts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2991" n="HIAT:w" s="T835">aləj=</ts>
                  <nts id="Seg_2992" n="HIAT:ip">)</nts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2995" n="HIAT:w" s="T836">ej</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2998" n="HIAT:w" s="T837">aləj</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3001" n="HIAT:w" s="T838">tănan</ts>
                  <nts id="Seg_3002" n="HIAT:ip">.</nts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T844" id="Seg_3004" n="sc" s="T840">
               <ts e="T844" id="Seg_3006" n="HIAT:u" s="T840">
                  <nts id="Seg_3007" n="HIAT:ip">(</nts>
                  <ts e="T841" id="Seg_3009" n="HIAT:w" s="T840">Sʼanəluʔlʼil</ts>
                  <nts id="Seg_3010" n="HIAT:ip">)</nts>
                  <nts id="Seg_3011" n="HIAT:ip">,</nts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3014" n="HIAT:w" s="T841">dĭgəttə</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3017" n="HIAT:w" s="T842">maʔnəl</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3019" n="HIAT:ip">(</nts>
                  <ts e="T844" id="Seg_3021" n="HIAT:w" s="T843">šoləl</ts>
                  <nts id="Seg_3022" n="HIAT:ip">)</nts>
                  <nts id="Seg_3023" n="HIAT:ip">.</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T847" id="Seg_3025" n="sc" s="T845">
               <ts e="T847" id="Seg_3027" n="HIAT:u" s="T845">
                  <nts id="Seg_3028" n="HIAT:ip">(</nts>
                  <ts e="T846" id="Seg_3030" n="HIAT:w" s="T845">Leizittə</ts>
                  <nts id="Seg_3031" n="HIAT:ip">)</nts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3034" n="HIAT:w" s="T846">možna</ts>
                  <nts id="Seg_3035" n="HIAT:ip">.</nts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T849" id="Seg_3037" n="sc" s="T848">
               <ts e="" id="Seg_3039" n="HIAT:u" s="T848">
                  <nts id="Seg_3040" n="HIAT:ip">(</nts>
                  <nts id="Seg_3041" n="HIAT:ip">…</nts>
               </ts>
               <ts e="T849" id="Seg_3043" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3045" n="HIAT:w" s="T848">говорю</ts>
                  <nts id="Seg_3046" n="HIAT:ip">)</nts>
                  <nts id="Seg_3047" n="HIAT:ip">.</nts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T862" id="Seg_3049" n="sc" s="T850">
               <ts e="T854" id="Seg_3051" n="HIAT:u" s="T850">
                  <nts id="Seg_3052" n="HIAT:ip">"</nts>
                  <ts e="T850.tx.1" id="Seg_3054" n="HIAT:w" s="T850">Vsʼo</ts>
                  <nts id="Seg_3055" n="HIAT:ip">_</nts>
                  <ts e="T852" id="Seg_3057" n="HIAT:w" s="T850.tx.1">ravno</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3060" n="HIAT:w" s="T852">măn</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3063" n="HIAT:w" s="T853">pimniem</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3067" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_3069" n="HIAT:w" s="T854">Măn</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3072" n="HIAT:w" s="T855">ĭmbidə</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3075" n="HIAT:w" s="T856">naga</ts>
                  <nts id="Seg_3076" n="HIAT:ip">,</nts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3078" n="HIAT:ip">(</nts>
                  <ts e="T858" id="Seg_3080" n="HIAT:w" s="T857">multuk=</ts>
                  <nts id="Seg_3081" n="HIAT:ip">)</nts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3084" n="HIAT:w" s="T858">multuk</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3087" n="HIAT:w" s="T859">naga</ts>
                  <nts id="Seg_3088" n="HIAT:ip">,</nts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3091" n="HIAT:w" s="T860">tagajbə</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3094" n="HIAT:w" s="T861">naga</ts>
                  <nts id="Seg_3095" n="HIAT:ip">.</nts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T869" id="Seg_3097" n="sc" s="T863">
               <ts e="T865" id="Seg_3099" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_3101" n="HIAT:w" s="T863">Tolʼkă</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3104" n="HIAT:w" s="T864">pi</ts>
                  <nts id="Seg_3105" n="HIAT:ip">"</nts>
                  <nts id="Seg_3106" n="HIAT:ip">.</nts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_3109" n="HIAT:u" s="T865">
                  <nts id="Seg_3110" n="HIAT:ip">"</nts>
                  <ts e="T866" id="Seg_3112" n="HIAT:w" s="T865">A</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3115" n="HIAT:w" s="T866">dĭ</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3118" n="HIAT:w" s="T867">ej</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3120" n="HIAT:ip">(</nts>
                  <ts e="T869" id="Seg_3122" n="HIAT:w" s="T868">ĭzəmnəj</ts>
                  <nts id="Seg_3123" n="HIAT:ip">)</nts>
                  <nts id="Seg_3124" n="HIAT:ip">.</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T874" id="Seg_3126" n="sc" s="T870">
               <ts e="T874" id="Seg_3128" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3130" n="HIAT:w" s="T870">Dĭ</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3133" n="HIAT:w" s="T871">kuzagəʔ</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3136" n="HIAT:w" s="T872">pimnie</ts>
                  <nts id="Seg_3137" n="HIAT:ip">,</nts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3140" n="HIAT:w" s="T873">nuʔməluʔləj</ts>
                  <nts id="Seg_3141" n="HIAT:ip">"</nts>
                  <nts id="Seg_3142" n="HIAT:ip">.</nts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T882" id="Seg_3144" n="sc" s="T875">
               <ts e="T882" id="Seg_3146" n="HIAT:u" s="T875">
                  <ts e="T876" id="Seg_3148" n="HIAT:w" s="T875">Miʔ</ts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3151" n="HIAT:w" s="T876">kambibaʔ</ts>
                  <nts id="Seg_3152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3153" n="HIAT:ip">(</nts>
                  <ts e="T878" id="Seg_3155" n="HIAT:w" s="T877">ten-</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3158" n="HIAT:w" s="T878">te-</ts>
                  <nts id="Seg_3159" n="HIAT:ip">)</nts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3162" n="HIAT:w" s="T879">dĭ</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3164" n="HIAT:ip">(</nts>
                  <ts e="T881" id="Seg_3166" n="HIAT:w" s="T880">dʼijegə</ts>
                  <nts id="Seg_3167" n="HIAT:ip">)</nts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3170" n="HIAT:w" s="T881">dʼijenə</ts>
                  <nts id="Seg_3171" n="HIAT:ip">.</nts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T886" id="Seg_3173" n="sc" s="T883">
               <ts e="T886" id="Seg_3175" n="HIAT:u" s="T883">
                  <ts e="T884" id="Seg_3177" n="HIAT:w" s="T883">Dĭgəttə</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3180" n="HIAT:w" s="T884">sĭre</ts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3183" n="HIAT:w" s="T885">kanluʔpi</ts>
                  <nts id="Seg_3184" n="HIAT:ip">.</nts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T894" id="Seg_3186" n="sc" s="T887">
               <ts e="T894" id="Seg_3188" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_3190" n="HIAT:w" s="T887">Dĭzeŋ</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3193" n="HIAT:w" s="T888">nagurgö</ts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3196" n="HIAT:w" s="T889">kambiʔi</ts>
                  <nts id="Seg_3197" n="HIAT:ip">,</nts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3200" n="HIAT:w" s="T890">a</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3203" n="HIAT:w" s="T891">măn</ts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3206" n="HIAT:w" s="T892">parluʔpiam</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3209" n="HIAT:w" s="T893">piʔtə</ts>
                  <nts id="Seg_3210" n="HIAT:ip">.</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T903" id="Seg_3212" n="sc" s="T895">
               <ts e="T903" id="Seg_3214" n="HIAT:u" s="T895">
                  <ts e="T896" id="Seg_3216" n="HIAT:w" s="T895">Dĭgəttə</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3219" n="HIAT:w" s="T896">nünniem:</ts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3222" n="HIAT:w" s="T897">šindidə</ts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3225" n="HIAT:w" s="T898">bar</ts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3227" n="HIAT:ip">(</nts>
                  <ts e="T900" id="Seg_3229" n="HIAT:w" s="T899">muzaŋ=</ts>
                  <nts id="Seg_3230" n="HIAT:ip">)</nts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3233" n="HIAT:w" s="T900">muzaŋdə</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3236" n="HIAT:w" s="T901">bar</ts>
                  <nts id="Seg_3237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3239" n="HIAT:w" s="T902">bădlaʔpə</ts>
                  <nts id="Seg_3240" n="HIAT:ip">.</nts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T919" id="Seg_3242" n="sc" s="T904">
               <ts e="T913" id="Seg_3244" n="HIAT:u" s="T904">
                  <nts id="Seg_3245" n="HIAT:ip">(</nts>
                  <ts e="T905" id="Seg_3247" n="HIAT:w" s="T904">N-</ts>
                  <nts id="Seg_3248" n="HIAT:ip">)</nts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3251" n="HIAT:w" s="T905">Tenniem:</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3254" n="HIAT:w" s="T906">urgaːba</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3256" n="HIAT:ip">(</nts>
                  <ts e="T908" id="Seg_3258" n="HIAT:w" s="T907">š-</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3261" n="HIAT:w" s="T908">kan-</ts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3264" n="HIAT:w" s="T909">š-</ts>
                  <nts id="Seg_3265" n="HIAT:ip">)</nts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3268" n="HIAT:w" s="T910">bar</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3270" n="HIAT:ip">(</nts>
                  <nts id="Seg_3271" n="HIAT:ip">(</nts>
                  <ats e="T912" id="Seg_3272" n="HIAT:non-pho" s="T911">…</ats>
                  <nts id="Seg_3273" n="HIAT:ip">)</nts>
                  <nts id="Seg_3274" n="HIAT:ip">)</nts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3276" n="HIAT:ip">(</nts>
                  <ts e="T913" id="Seg_3278" n="HIAT:w" s="T912">măldlaʔpə</ts>
                  <nts id="Seg_3279" n="HIAT:ip">)</nts>
                  <nts id="Seg_3280" n="HIAT:ip">.</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T919" id="Seg_3283" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_3285" n="HIAT:w" s="T913">A</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3288" n="HIAT:w" s="T914">kuzittə</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3291" n="HIAT:w" s="T915">ej</ts>
                  <nts id="Seg_3292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3294" n="HIAT:w" s="T916">kubiam</ts>
                  <nts id="Seg_3295" n="HIAT:ip">,</nts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3298" n="HIAT:w" s="T917">maʔnʼi</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3301" n="HIAT:w" s="T918">šobiam</ts>
                  <nts id="Seg_3302" n="HIAT:ip">.</nts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T925" id="Seg_3304" n="sc" s="T920">
               <ts e="T925" id="Seg_3306" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3308" n="HIAT:w" s="T920">Măn</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3311" n="HIAT:w" s="T921">šonəgam</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3314" n="HIAT:w" s="T922">i</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3317" n="HIAT:w" s="T923">nereluʔpiem</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3320" n="HIAT:w" s="T924">bar</ts>
                  <nts id="Seg_3321" n="HIAT:ip">.</nts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T937" id="Seg_3323" n="sc" s="T926">
               <ts e="T929" id="Seg_3325" n="HIAT:u" s="T926">
                  <ts e="T927" id="Seg_3327" n="HIAT:w" s="T926">Paʔinə</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3330" n="HIAT:w" s="T927">nada</ts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3333" n="HIAT:w" s="T928">sʼazittə</ts>
                  <nts id="Seg_3334" n="HIAT:ip">.</nts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T937" id="Seg_3337" n="HIAT:u" s="T929">
                  <ts e="T930" id="Seg_3339" n="HIAT:w" s="T929">A</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3342" n="HIAT:w" s="T930">măn</ts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3345" n="HIAT:w" s="T931">dĭʔnə</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3348" n="HIAT:w" s="T932">mămbiam:</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3350" n="HIAT:ip">"</nts>
                  <ts e="T934" id="Seg_3352" n="HIAT:w" s="T933">Dĭ</ts>
                  <nts id="Seg_3353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3355" n="HIAT:w" s="T934">tože</ts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3358" n="HIAT:w" s="T935">paʔinə</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3361" n="HIAT:w" s="T936">sʼalia</ts>
                  <nts id="Seg_3362" n="HIAT:ip">"</nts>
                  <nts id="Seg_3363" n="HIAT:ip">.</nts>
                  <nts id="Seg_3364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T944" id="Seg_3365" n="sc" s="T938">
               <ts e="T944" id="Seg_3367" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3369" n="HIAT:w" s="T938">I</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3372" n="HIAT:w" s="T939">pa</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3375" n="HIAT:w" s="T940">baldlia</ts>
                  <nts id="Seg_3376" n="HIAT:ip">,</nts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3379" n="HIAT:w" s="T941">i</ts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3382" n="HIAT:w" s="T942">dʼünə</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3384" n="HIAT:ip">(</nts>
                  <ts e="T944" id="Seg_3386" n="HIAT:w" s="T943">barʔdlia</ts>
                  <nts id="Seg_3387" n="HIAT:ip">)</nts>
                  <nts id="Seg_3388" n="HIAT:ip">.</nts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T951" id="Seg_3390" n="sc" s="T945">
               <ts e="T951" id="Seg_3392" n="HIAT:u" s="T945">
                  <ts e="T946" id="Seg_3394" n="HIAT:w" s="T945">Kadəʔ</ts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3397" n="HIAT:w" s="T946">dĭn</ts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3400" n="HIAT:w" s="T947">dʼăbaktərzittə</ts>
                  <nts id="Seg_3401" n="HIAT:ip">,</nts>
                  <nts id="Seg_3402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3404" n="HIAT:w" s="T948">esseŋ</ts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3407" n="HIAT:w" s="T949">bar</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3410" n="HIAT:w" s="T950">kirgarlaʔbə</ts>
                  <nts id="Seg_3411" n="HIAT:ip">.</nts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T959" id="Seg_3413" n="sc" s="T952">
               <ts e="T956" id="Seg_3415" n="HIAT:u" s="T952">
                  <ts e="T953" id="Seg_3417" n="HIAT:w" s="T952">Šoškaʔi</ts>
                  <nts id="Seg_3418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3420" n="HIAT:w" s="T953">tože</ts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3423" n="HIAT:w" s="T954">bar</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3426" n="HIAT:w" s="T955">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_3427" n="HIAT:ip">.</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T959" id="Seg_3430" n="HIAT:u" s="T956">
                  <nts id="Seg_3431" n="HIAT:ip">(</nts>
                  <ts e="T957" id="Seg_3433" n="HIAT:w" s="T956">Puzo</ts>
                  <nts id="Seg_3434" n="HIAT:ip">)</nts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3437" n="HIAT:w" s="T957">tože</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3440" n="HIAT:w" s="T958">mürerlaʔbə</ts>
                  <nts id="Seg_3441" n="HIAT:ip">.</nts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T963" id="Seg_3443" n="sc" s="T960">
               <ts e="T963" id="Seg_3445" n="HIAT:u" s="T960">
                  <ts e="T961" id="Seg_3447" n="HIAT:w" s="T960">Kădedə</ts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3450" n="HIAT:w" s="T961">nʼelʼzʼa</ts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3453" n="HIAT:w" s="T962">dʼăbaktərzittə</ts>
                  <nts id="Seg_3454" n="HIAT:ip">.</nts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T5" id="Seg_3456" n="sc" s="T1">
               <ts e="T2" id="Seg_3458" n="e" s="T1">Dĭ </ts>
               <ts e="T3" id="Seg_3460" n="e" s="T2">šiʔ </ts>
               <ts e="T4" id="Seg_3462" n="e" s="T3">aparat </ts>
               <ts e="T5" id="Seg_3464" n="e" s="T4">(taʔla). </ts>
            </ts>
            <ts e="T8" id="Seg_3465" n="sc" s="T6">
               <ts e="T7" id="Seg_3467" n="e" s="T6">Tararluʔpi </ts>
               <ts e="T8" id="Seg_3469" n="e" s="T7">bar. </ts>
            </ts>
            <ts e="T11" id="Seg_3470" n="sc" s="T9">
               <ts e="T10" id="Seg_3472" n="e" s="T9">(Начал) </ts>
               <ts e="T11" id="Seg_3474" n="e" s="T10">šaːmzittə. </ts>
            </ts>
            <ts e="T15" id="Seg_3475" n="sc" s="T12">
               <ts e="T13" id="Seg_3477" n="e" s="T12">Ugaːndə </ts>
               <ts e="T14" id="Seg_3479" n="e" s="T13">ɨrɨ </ts>
               <ts e="T15" id="Seg_3481" n="e" s="T14">molaːmbi. </ts>
            </ts>
            <ts e="T30" id="Seg_3482" n="sc" s="T16">
               <ts e="T17" id="Seg_3484" n="e" s="T16">Tăn </ts>
               <ts e="T18" id="Seg_3486" n="e" s="T17">ugaːndə </ts>
               <ts e="T19" id="Seg_3488" n="e" s="T18">(kuguštu) </ts>
               <ts e="T20" id="Seg_3490" n="e" s="T19">kuza, </ts>
               <ts e="T21" id="Seg_3492" n="e" s="T20">tănan </ts>
               <ts e="T22" id="Seg_3494" n="e" s="T21">măllial, </ts>
               <ts e="T23" id="Seg_3496" n="e" s="T22">măllial. </ts>
               <ts e="T24" id="Seg_3498" n="e" s="T23">(A </ts>
               <ts e="T25" id="Seg_3500" n="e" s="T24">tăn=) </ts>
               <ts e="T26" id="Seg_3502" n="e" s="T25">A </ts>
               <ts e="T27" id="Seg_3504" n="e" s="T26">tăn </ts>
               <ts e="T28" id="Seg_3506" n="e" s="T27">ĭmbidə </ts>
               <ts e="T29" id="Seg_3508" n="e" s="T28">ej </ts>
               <ts e="T30" id="Seg_3510" n="e" s="T29">tĭmniel. </ts>
            </ts>
            <ts e="T36" id="Seg_3511" n="sc" s="T31">
               <ts e="T32" id="Seg_3513" n="e" s="T31">Măn </ts>
               <ts e="T33" id="Seg_3515" n="e" s="T32">tănan </ts>
               <ts e="T34" id="Seg_3517" n="e" s="T33">mălliam: </ts>
               <ts e="T35" id="Seg_3519" n="e" s="T34">üdʼüge </ts>
               <ts e="T36" id="Seg_3521" n="e" s="T35">ibiem. </ts>
            </ts>
            <ts e="T40" id="Seg_3522" n="sc" s="T37">
               <ts e="T38" id="Seg_3524" n="e" s="T37">Girgitdə </ts>
               <ts e="T39" id="Seg_3526" n="e" s="T38">il </ts>
               <ts e="T40" id="Seg_3528" n="e" s="T39">šobiʔi. </ts>
            </ts>
            <ts e="T50" id="Seg_3529" n="sc" s="T41">
               <ts e="T42" id="Seg_3531" n="e" s="T41">То </ts>
               <ts e="T43" id="Seg_3533" n="e" s="T42">ли </ts>
               <ts e="T44" id="Seg_3535" n="e" s="T43">экспедиция, </ts>
               <ts e="T45" id="Seg_3537" n="e" s="T44">ej </ts>
               <ts e="T46" id="Seg_3539" n="e" s="T45">tĭmniem. </ts>
               <ts e="T47" id="Seg_3541" n="e" s="T46">Üdʼüge </ts>
               <ts e="T48" id="Seg_3543" n="e" s="T47">ibiem, </ts>
               <ts e="T49" id="Seg_3545" n="e" s="T48">ej </ts>
               <ts e="T50" id="Seg_3547" n="e" s="T49">surarbiam. </ts>
            </ts>
            <ts e="T54" id="Seg_3548" n="sc" s="T51">
               <ts e="T52" id="Seg_3550" n="e" s="T51">Măna </ts>
               <ts e="T53" id="Seg_3552" n="e" s="T52">ej </ts>
               <ts e="T54" id="Seg_3554" n="e" s="T53">nörbəbiʔi. </ts>
            </ts>
            <ts e="T59" id="Seg_3555" n="sc" s="T55">
               <ts e="T56" id="Seg_3557" n="e" s="T55">(Vlaskən) </ts>
               <ts e="T57" id="Seg_3559" n="e" s="T56">(Иван=) </ts>
               <ts e="T58" id="Seg_3561" n="e" s="T57">Иван </ts>
               <ts e="T59" id="Seg_3563" n="e" s="T58">Андреевич. </ts>
            </ts>
            <ts e="T72" id="Seg_3564" n="sc" s="T60">
               <ts e="T61" id="Seg_3566" n="e" s="T60">Dĭgəttə </ts>
               <ts e="T62" id="Seg_3568" n="e" s="T61">dĭzen </ts>
               <ts e="T63" id="Seg_3570" n="e" s="T62">ibi </ts>
               <ts e="T64" id="Seg_3572" n="e" s="T63">бинокль </ts>
               <ts e="T65" id="Seg_3574" n="e" s="T64">i </ts>
               <ts e="T66" id="Seg_3576" n="e" s="T65">ĭmbi, </ts>
               <ts e="T67" id="Seg_3578" n="e" s="T66">tože </ts>
               <ts e="T68" id="Seg_3580" n="e" s="T67">ej </ts>
               <ts e="T69" id="Seg_3582" n="e" s="T68">tĭmniem. </ts>
               <ts e="T70" id="Seg_3584" n="e" s="T69">Măna </ts>
               <ts e="T71" id="Seg_3586" n="e" s="T70">pʼerbiʔi </ts>
               <ts e="T72" id="Seg_3588" n="e" s="T71">nʼimi. </ts>
            </ts>
            <ts e="T75" id="Seg_3589" n="sc" s="T73">
               <ts e="T74" id="Seg_3591" n="e" s="T73">Müʔluʔpiʔi </ts>
               <ts e="T75" id="Seg_3593" n="e" s="T74">stʼenanə. </ts>
            </ts>
            <ts e="T78" id="Seg_3594" n="sc" s="T76">
               <ts e="T77" id="Seg_3596" n="e" s="T76">Kuŋgəŋ </ts>
               <ts e="T78" id="Seg_3598" n="e" s="T77">dăre. </ts>
            </ts>
            <ts e="T82" id="Seg_3599" n="sc" s="T79">
               <ts e="T80" id="Seg_3601" n="e" s="T79">Sažendə </ts>
               <ts e="T81" id="Seg_3603" n="e" s="T80">šide </ts>
               <ts e="T82" id="Seg_3605" n="e" s="T81">bʼeʔ. </ts>
            </ts>
            <ts e="T91" id="Seg_3606" n="sc" s="T83">
               <ts e="T84" id="Seg_3608" n="e" s="T83">Dĭgəttə </ts>
               <ts e="T85" id="Seg_3610" n="e" s="T84">măn </ts>
               <ts e="T86" id="Seg_3612" n="e" s="T85">măndərbiam </ts>
               <ts e="T87" id="Seg_3614" n="e" s="T86">(m-) </ts>
               <ts e="T88" id="Seg_3616" n="e" s="T87">ugaːndə </ts>
               <ts e="T89" id="Seg_3618" n="e" s="T88">(nʼim-) </ts>
               <ts e="T90" id="Seg_3620" n="e" s="T89">nʼimi </ts>
               <ts e="T91" id="Seg_3622" n="e" s="T90">urgo. </ts>
            </ts>
            <ts e="T97" id="Seg_3623" n="sc" s="T92">
               <ts e="T93" id="Seg_3625" n="e" s="T92">A </ts>
               <ts e="T94" id="Seg_3627" n="e" s="T93">dĭgəttə </ts>
               <ts e="T95" id="Seg_3629" n="e" s="T94">parluʔpiʔi, </ts>
               <ts e="T96" id="Seg_3631" n="e" s="T95">drugoj </ts>
               <ts e="T97" id="Seg_3633" n="e" s="T96">kănʼessiʔ. </ts>
            </ts>
            <ts e="T102" id="Seg_3634" n="sc" s="T98">
               <ts e="T99" id="Seg_3636" n="e" s="T98">Măndəbiom: </ts>
               <ts e="T100" id="Seg_3638" n="e" s="T99">ugaːndə </ts>
               <ts e="T101" id="Seg_3640" n="e" s="T100">kuŋgəŋ </ts>
               <ts e="T102" id="Seg_3642" n="e" s="T101">molaːmbi. </ts>
            </ts>
            <ts e="T106" id="Seg_3643" n="sc" s="T103">
               <ts e="T104" id="Seg_3645" n="e" s="T103">Dĭgəttə </ts>
               <ts e="T105" id="Seg_3647" n="e" s="T104">sazən </ts>
               <ts e="T106" id="Seg_3649" n="e" s="T105">embiʔi. </ts>
            </ts>
            <ts e="T112" id="Seg_3650" n="sc" s="T107">
               <ts e="T108" id="Seg_3652" n="e" s="T107">I </ts>
               <ts e="T109" id="Seg_3654" n="e" s="T108">nuldəbiʔi </ts>
               <ts e="T110" id="Seg_3656" n="e" s="T109">dĭ </ts>
               <ts e="T111" id="Seg_3658" n="e" s="T110">binokl </ts>
               <ts e="T112" id="Seg_3660" n="e" s="T111">kujanə. </ts>
            </ts>
            <ts e="T116" id="Seg_3661" n="sc" s="T113">
               <ts e="T114" id="Seg_3663" n="e" s="T113">I </ts>
               <ts e="T115" id="Seg_3665" n="e" s="T114">sazən </ts>
               <ts e="T116" id="Seg_3667" n="e" s="T115">neniluʔpi. </ts>
            </ts>
            <ts e="T120" id="Seg_3668" n="sc" s="T117">
               <ts e="T118" id="Seg_3670" n="e" s="T117">Šiʔ </ts>
               <ts e="T119" id="Seg_3672" n="e" s="T118">kamən </ts>
               <ts e="T120" id="Seg_3674" n="e" s="T119">kalləlaʔ? </ts>
            </ts>
            <ts e="T123" id="Seg_3675" n="sc" s="T121">
               <ts e="T122" id="Seg_3677" n="e" s="T121">(Păredʼan) </ts>
               <ts e="T123" id="Seg_3679" n="e" s="T122">kalləbaʔ. </ts>
            </ts>
            <ts e="T126" id="Seg_3680" n="sc" s="T124">
               <ts e="T125" id="Seg_3682" n="e" s="T124">No, </ts>
               <ts e="T126" id="Seg_3684" n="e" s="T125">kaŋgaʔ! </ts>
            </ts>
            <ts e="T131" id="Seg_3685" n="sc" s="T127">
               <ts e="T128" id="Seg_3687" n="e" s="T127">Privet </ts>
               <ts e="T129" id="Seg_3689" n="e" s="T128">nörbəgeʔ </ts>
               <ts e="T130" id="Seg_3691" n="e" s="T129">(Mat-) </ts>
               <ts e="T131" id="Seg_3693" n="e" s="T130">Matvejendə. </ts>
            </ts>
            <ts e="T137" id="Seg_3694" n="sc" s="T132">
               <ts e="T133" id="Seg_3696" n="e" s="T132">I </ts>
               <ts e="T134" id="Seg_3698" n="e" s="T133">Jelʼa </ts>
               <ts e="T135" id="Seg_3700" n="e" s="T134">teinen </ts>
               <ts e="T964" id="Seg_3702" n="e" s="T135">kalla </ts>
               <ts e="T136" id="Seg_3704" n="e" s="T964">dʼürbi </ts>
               <ts e="T137" id="Seg_3706" n="e" s="T136">Malinovkanə. </ts>
            </ts>
            <ts e="T140" id="Seg_3707" n="sc" s="T138">
               <ts e="T139" id="Seg_3709" n="e" s="T138">Sazən </ts>
               <ts e="T140" id="Seg_3711" n="e" s="T139">kundubi. </ts>
            </ts>
            <ts e="T145" id="Seg_3712" n="sc" s="T141">
               <ts e="T142" id="Seg_3714" n="e" s="T141">Prʼedsʼedatelʼdə </ts>
               <ts e="T143" id="Seg_3716" n="e" s="T142">nada </ts>
               <ts e="T144" id="Seg_3718" n="e" s="T143">pečatʼʔi </ts>
               <ts e="T145" id="Seg_3720" n="e" s="T144">enzittə. </ts>
            </ts>
            <ts e="T150" id="Seg_3721" n="sc" s="T146">
               <ts e="T147" id="Seg_3723" n="e" s="T146">Dĭ </ts>
               <ts e="T148" id="Seg_3725" n="e" s="T147">kambi </ts>
               <ts e="T149" id="Seg_3727" n="e" s="T148">dĭbər </ts>
               <ts e="T150" id="Seg_3729" n="e" s="T149">mašinaʔizʼiʔ. </ts>
            </ts>
            <ts e="T156" id="Seg_3730" n="sc" s="T151">
               <ts e="T152" id="Seg_3732" n="e" s="T151">A </ts>
               <ts e="T153" id="Seg_3734" n="e" s="T152">(dĭʔnə-) </ts>
               <ts e="T154" id="Seg_3736" n="e" s="T153">dĭʔə </ts>
               <ts e="T155" id="Seg_3738" n="e" s="T154">parləj </ts>
               <ts e="T156" id="Seg_3740" n="e" s="T155">üdʼiŋ. </ts>
            </ts>
            <ts e="T165" id="Seg_3741" n="sc" s="T157">
               <ts e="T158" id="Seg_3743" n="e" s="T157">Măn </ts>
               <ts e="T159" id="Seg_3745" n="e" s="T158">sʼestranə </ts>
               <ts e="T160" id="Seg_3747" n="e" s="T159">nʼit </ts>
               <ts e="T965" id="Seg_3749" n="e" s="T160">kalla </ts>
               <ts e="T161" id="Seg_3751" n="e" s="T965">dʼürbi </ts>
               <ts e="T162" id="Seg_3753" n="e" s="T161">(nodujla) </ts>
               <ts e="T163" id="Seg_3755" n="e" s="T162">tüžöjʔi </ts>
               <ts e="T164" id="Seg_3757" n="e" s="T163">(sʼinədə) </ts>
               <ts e="T165" id="Seg_3759" n="e" s="T164">deʔsʼittə. </ts>
            </ts>
            <ts e="T169" id="Seg_3760" n="sc" s="T166">
               <ts e="T167" id="Seg_3762" n="e" s="T166">Bostə </ts>
               <ts e="T168" id="Seg_3764" n="e" s="T167">nʼit </ts>
               <ts e="T169" id="Seg_3766" n="e" s="T168">ibi. </ts>
            </ts>
            <ts e="T174" id="Seg_3767" n="sc" s="T170">
               <ts e="T171" id="Seg_3769" n="e" s="T170">Büžü </ts>
               <ts e="T172" id="Seg_3771" n="e" s="T171">(parluʔi-) </ts>
               <ts e="T173" id="Seg_3773" n="e" s="T172">parluʔjə </ts>
               <ts e="T174" id="Seg_3775" n="e" s="T173">maːʔndə. </ts>
            </ts>
            <ts e="T181" id="Seg_3776" n="sc" s="T175">
               <ts e="T176" id="Seg_3778" n="e" s="T175">Onʼiʔ </ts>
               <ts e="T177" id="Seg_3780" n="e" s="T176">kuza </ts>
               <ts e="T178" id="Seg_3782" n="e" s="T177">tenölüʔpi </ts>
               <ts e="T179" id="Seg_3784" n="e" s="T178">maʔ </ts>
               <ts e="T180" id="Seg_3786" n="e" s="T179">(asi-) </ts>
               <ts e="T181" id="Seg_3788" n="e" s="T180">azittə. </ts>
            </ts>
            <ts e="T183" id="Seg_3789" n="sc" s="T182">
               <ts e="T183" id="Seg_3791" n="e" s="T182">Zolătăʔizʼiʔ. </ts>
            </ts>
            <ts e="T200" id="Seg_3792" n="sc" s="T184">
               <ts e="T185" id="Seg_3794" n="e" s="T184">A </ts>
               <ts e="T186" id="Seg_3796" n="e" s="T185">baška </ts>
               <ts e="T187" id="Seg_3798" n="e" s="T186">kuza </ts>
               <ts e="T188" id="Seg_3800" n="e" s="T187">maʔ </ts>
               <ts e="T189" id="Seg_3802" n="e" s="T188">azittə </ts>
               <ts e="T190" id="Seg_3804" n="e" s="T189">sʼerʼebrogəʔ. </ts>
               <ts e="T191" id="Seg_3806" n="e" s="T190">A </ts>
               <ts e="T192" id="Seg_3808" n="e" s="T191">nagur </ts>
               <ts e="T193" id="Seg_3810" n="e" s="T192">kuza </ts>
               <ts e="T194" id="Seg_3812" n="e" s="T193">bar </ts>
               <ts e="T195" id="Seg_3814" n="e" s="T194">(žəzə) </ts>
               <ts e="T196" id="Seg_3816" n="e" s="T195">măndə: </ts>
               <ts e="T197" id="Seg_3818" n="e" s="T196">măn </ts>
               <ts e="T198" id="Seg_3820" n="e" s="T197">paʔizʼi </ts>
               <ts e="T199" id="Seg_3822" n="e" s="T198">jaʔləm </ts>
               <ts e="T200" id="Seg_3824" n="e" s="T199">tura. </ts>
            </ts>
            <ts e="T208" id="Seg_3825" n="sc" s="T201">
               <ts e="T202" id="Seg_3827" n="e" s="T201">A </ts>
               <ts e="T203" id="Seg_3829" n="e" s="T202">teʔtə </ts>
               <ts e="T204" id="Seg_3831" n="e" s="T203">kuza: </ts>
               <ts e="T205" id="Seg_3833" n="e" s="T204">măn </ts>
               <ts e="T206" id="Seg_3835" n="e" s="T205">alam </ts>
               <ts e="T207" id="Seg_3837" n="e" s="T206">noʔə </ts>
               <ts e="T208" id="Seg_3839" n="e" s="T207">sălomaʔizʼi. </ts>
            </ts>
            <ts e="T213" id="Seg_3840" n="sc" s="T209">
               <ts e="T210" id="Seg_3842" n="e" s="T209">(Su) </ts>
               <ts e="T211" id="Seg_3844" n="e" s="T210">šoləj </ts>
               <ts e="T212" id="Seg_3846" n="e" s="T211">bar </ts>
               <ts e="T213" id="Seg_3848" n="e" s="T212">togonordə. </ts>
            </ts>
            <ts e="T220" id="Seg_3849" n="sc" s="T214">
               <ts e="T215" id="Seg_3851" n="e" s="T214">Nendləj </ts>
               <ts e="T216" id="Seg_3853" n="e" s="T215">(sĭjdən-) </ts>
               <ts e="T217" id="Seg_3855" n="e" s="T216">šindin </ts>
               <ts e="T218" id="Seg_3857" n="e" s="T217">togonordə </ts>
               <ts e="T219" id="Seg_3859" n="e" s="T218">ej </ts>
               <ts e="T220" id="Seg_3861" n="e" s="T219">(nendlən). </ts>
            </ts>
            <ts e="T225" id="Seg_3862" n="sc" s="T221">
               <ts e="T222" id="Seg_3864" n="e" s="T221">Dĭ </ts>
               <ts e="T223" id="Seg_3866" n="e" s="T222">dĭʔnə </ts>
               <ts e="T224" id="Seg_3868" n="e" s="T223">jakšə </ts>
               <ts e="T225" id="Seg_3870" n="e" s="T224">moləj. </ts>
            </ts>
            <ts e="T231" id="Seg_3871" n="sc" s="T226">
               <ts e="T227" id="Seg_3873" n="e" s="T226">A </ts>
               <ts e="T228" id="Seg_3875" n="e" s="T227">šindin </ts>
               <ts e="T229" id="Seg_3877" n="e" s="T228">togonordə </ts>
               <ts e="T230" id="Seg_3879" n="e" s="T229">(nen-) </ts>
               <ts e="T231" id="Seg_3881" n="e" s="T230">nendluʔləj. </ts>
            </ts>
            <ts e="T236" id="Seg_3882" n="sc" s="T232">
               <ts e="T233" id="Seg_3884" n="e" s="T232">Dĭʔnə </ts>
               <ts e="T234" id="Seg_3886" n="e" s="T233">ej </ts>
               <ts e="T235" id="Seg_3888" n="e" s="T234">jakšə </ts>
               <ts e="T236" id="Seg_3890" n="e" s="T235">moləj. </ts>
            </ts>
            <ts e="T241" id="Seg_3891" n="sc" s="T237">
               <ts e="T238" id="Seg_3893" n="e" s="T237">Bostə </ts>
               <ts e="T239" id="Seg_3895" n="e" s="T238">ej </ts>
               <ts e="T240" id="Seg_3897" n="e" s="T239">nendlil. </ts>
               <ts e="T241" id="Seg_3899" n="e" s="T240">Nuʔməluʔləj. </ts>
            </ts>
            <ts e="T249" id="Seg_3900" n="sc" s="T242">
               <ts e="T243" id="Seg_3902" n="e" s="T242">(A </ts>
               <ts e="T244" id="Seg_3904" n="e" s="T243">ĭmbi=) </ts>
               <ts e="T245" id="Seg_3906" n="e" s="T244">(aʔ-) </ts>
               <ts e="T246" id="Seg_3908" n="e" s="T245">A </ts>
               <ts e="T247" id="Seg_3910" n="e" s="T246">ĭmbi </ts>
               <ts e="T248" id="Seg_3912" n="e" s="T247">togonərli, </ts>
               <ts e="T249" id="Seg_3914" n="e" s="T248">nendluʔpi. </ts>
            </ts>
            <ts e="T254" id="Seg_3915" n="sc" s="T250">
               <ts e="T251" id="Seg_3917" n="e" s="T250">(Amnobiʔi) </ts>
               <ts e="T252" id="Seg_3919" n="e" s="T251">Nagur </ts>
               <ts e="T253" id="Seg_3921" n="e" s="T252">šoškaʔi </ts>
               <ts e="T254" id="Seg_3923" n="e" s="T253">amnobiʔi. </ts>
            </ts>
            <ts e="T266" id="Seg_3924" n="sc" s="T255">
               <ts e="T256" id="Seg_3926" n="e" s="T255">Dĭgəttə </ts>
               <ts e="T257" id="Seg_3928" n="e" s="T256">šide </ts>
               <ts e="T258" id="Seg_3930" n="e" s="T257">šoška </ts>
               <ts e="T259" id="Seg_3932" n="e" s="T258">ugaːndə </ts>
               <ts e="T260" id="Seg_3934" n="e" s="T259">ɨrɨ </ts>
               <ts e="T261" id="Seg_3936" n="e" s="T260">ibiʔi. </ts>
               <ts e="T262" id="Seg_3938" n="e" s="T261">A </ts>
               <ts e="T263" id="Seg_3940" n="e" s="T262">onʼiʔ </ts>
               <ts e="T264" id="Seg_3942" n="e" s="T263">šoška </ts>
               <ts e="T265" id="Seg_3944" n="e" s="T264">üge </ts>
               <ts e="T266" id="Seg_3946" n="e" s="T265">togonərbi. </ts>
            </ts>
            <ts e="T273" id="Seg_3947" n="sc" s="T267">
               <ts e="T268" id="Seg_3949" n="e" s="T267">Dĭzen </ts>
               <ts e="T269" id="Seg_3951" n="e" s="T268">bar </ts>
               <ts e="T270" id="Seg_3953" n="e" s="T269">jaʔtarlaʔ </ts>
               <ts e="T271" id="Seg_3955" n="e" s="T270">mĭmbiʔi, </ts>
               <ts e="T272" id="Seg_3957" n="e" s="T271">ara </ts>
               <ts e="T273" id="Seg_3959" n="e" s="T272">bĭʔpiʔi. </ts>
            </ts>
            <ts e="T281" id="Seg_3960" n="sc" s="T274">
               <ts e="T275" id="Seg_3962" n="e" s="T274">A </ts>
               <ts e="T276" id="Seg_3964" n="e" s="T275">dö, </ts>
               <ts e="T277" id="Seg_3966" n="e" s="T276">onʼiʔ </ts>
               <ts e="T278" id="Seg_3968" n="e" s="T277">šoška </ts>
               <ts e="T279" id="Seg_3970" n="e" s="T278">bar </ts>
               <ts e="T280" id="Seg_3972" n="e" s="T279">tĭlbi </ts>
               <ts e="T281" id="Seg_3974" n="e" s="T280">dʼü. </ts>
            </ts>
            <ts e="T284" id="Seg_3975" n="sc" s="T282">
               <ts e="T283" id="Seg_3977" n="e" s="T282">Piʔi </ts>
               <ts e="T284" id="Seg_3979" n="e" s="T283">oʔbdobiʔi. </ts>
            </ts>
            <ts e="T291" id="Seg_3980" n="sc" s="T285">
               <ts e="T286" id="Seg_3982" n="e" s="T285">I </ts>
               <ts e="T287" id="Seg_3984" n="e" s="T286">maʔ </ts>
               <ts e="T288" id="Seg_3986" n="e" s="T287">boskəndə </ts>
               <ts e="T289" id="Seg_3988" n="e" s="T288">abi </ts>
               <ts e="T290" id="Seg_3990" n="e" s="T289">(pi-) </ts>
               <ts e="T291" id="Seg_3992" n="e" s="T290">pizeŋdə. </ts>
            </ts>
            <ts e="T297" id="Seg_3993" n="sc" s="T292">
               <ts e="T293" id="Seg_3995" n="e" s="T292">(Dĭ-) </ts>
               <ts e="T294" id="Seg_3997" n="e" s="T293">Dĭzeŋ </ts>
               <ts e="T295" id="Seg_3999" n="e" s="T294">šobiʔi, </ts>
               <ts e="T296" id="Seg_4001" n="e" s="T295">onʼiʔ </ts>
               <ts e="T297" id="Seg_4003" n="e" s="T296">embi. </ts>
            </ts>
            <ts e="T309" id="Seg_4004" n="sc" s="T298">
               <ts e="T299" id="Seg_4006" n="e" s="T298">Muʔ </ts>
               <ts e="T300" id="Seg_4008" n="e" s="T299">oʔbdəbi, </ts>
               <ts e="T301" id="Seg_4010" n="e" s="T300">da </ts>
               <ts e="T302" id="Seg_4012" n="e" s="T301">embi. </ts>
               <ts e="T303" id="Seg_4014" n="e" s="T302">A </ts>
               <ts e="T304" id="Seg_4016" n="e" s="T303">onʼiʔ </ts>
               <ts e="T305" id="Seg_4018" n="e" s="T304">sălomaʔizʼiʔ </ts>
               <ts e="T306" id="Seg_4020" n="e" s="T305">(a-) </ts>
               <ts e="T307" id="Seg_4022" n="e" s="T306">embi </ts>
               <ts e="T308" id="Seg_4024" n="e" s="T307">boskəndə </ts>
               <ts e="T309" id="Seg_4026" n="e" s="T308">maʔ. </ts>
            </ts>
            <ts e="T322" id="Seg_4027" n="sc" s="T310">
               <ts e="T311" id="Seg_4029" n="e" s="T310">A </ts>
               <ts e="T312" id="Seg_4031" n="e" s="T311">dĭ </ts>
               <ts e="T313" id="Seg_4033" n="e" s="T312">üge </ts>
               <ts e="T314" id="Seg_4035" n="e" s="T313">pim </ts>
               <ts e="T315" id="Seg_4037" n="e" s="T314">piʔzʼi </ts>
               <ts e="T316" id="Seg_4039" n="e" s="T315">endleʔbə. </ts>
               <ts e="T317" id="Seg_4041" n="e" s="T316">(Dĭ=) </ts>
               <ts e="T318" id="Seg_4043" n="e" s="T317">Dĭzeŋ </ts>
               <ts e="T319" id="Seg_4045" n="e" s="T318">šobiʔi: </ts>
               <ts e="T320" id="Seg_4047" n="e" s="T319">"Ĭmbi </ts>
               <ts e="T321" id="Seg_4049" n="e" s="T320">tăn </ts>
               <ts e="T322" id="Seg_4051" n="e" s="T321">togonərlial? </ts>
            </ts>
            <ts e="T329" id="Seg_4052" n="sc" s="T323">
               <ts e="T324" id="Seg_4054" n="e" s="T323">Miʔ </ts>
               <ts e="T325" id="Seg_4056" n="e" s="T324">büžün </ts>
               <ts e="T326" id="Seg_4058" n="e" s="T325">(a-) </ts>
               <ts e="T327" id="Seg_4060" n="e" s="T326">abibaʔ </ts>
               <ts e="T328" id="Seg_4062" n="e" s="T327">(maʔbi-) </ts>
               <ts e="T329" id="Seg_4064" n="e" s="T328">maʔdə". </ts>
            </ts>
            <ts e="T339" id="Seg_4065" n="sc" s="T330">
               <ts e="T331" id="Seg_4067" n="e" s="T330">"A </ts>
               <ts e="T332" id="Seg_4069" n="e" s="T331">(der) </ts>
               <ts e="T333" id="Seg_4071" n="e" s="T332">šindin </ts>
               <ts e="T334" id="Seg_4073" n="e" s="T333">jakšə </ts>
               <ts e="T335" id="Seg_4075" n="e" s="T334">maʔdə </ts>
               <ts e="T336" id="Seg_4077" n="e" s="T335">moləj, </ts>
               <ts e="T337" id="Seg_4079" n="e" s="T336">măn </ts>
               <ts e="T338" id="Seg_4081" n="e" s="T337">ilʼi </ts>
               <ts e="T339" id="Seg_4083" n="e" s="T338">šiʔ?" </ts>
            </ts>
            <ts e="T346" id="Seg_4084" n="sc" s="T340">
               <ts e="T341" id="Seg_4086" n="e" s="T340">Dĭgəttə </ts>
               <ts e="T342" id="Seg_4088" n="e" s="T341">šĭšəge </ts>
               <ts e="T343" id="Seg_4090" n="e" s="T342">molaːmbi. </ts>
               <ts e="T344" id="Seg_4092" n="e" s="T343">(Sĭre=) </ts>
               <ts e="T345" id="Seg_4094" n="e" s="T344">Sĭre </ts>
               <ts e="T346" id="Seg_4096" n="e" s="T345">šonəga. </ts>
            </ts>
            <ts e="T354" id="Seg_4097" n="sc" s="T347">
               <ts e="T348" id="Seg_4099" n="e" s="T347">Dĭgəttə </ts>
               <ts e="T349" id="Seg_4101" n="e" s="T348">dĭzeŋ </ts>
               <ts e="T350" id="Seg_4103" n="e" s="T349">(pă-) </ts>
               <ts e="T351" id="Seg_4105" n="e" s="T350">păʔpiʔi </ts>
               <ts e="T352" id="Seg_4107" n="e" s="T351">turanə, </ts>
               <ts e="T353" id="Seg_4109" n="e" s="T352">girgit </ts>
               <ts e="T354" id="Seg_4111" n="e" s="T353">săloma. </ts>
            </ts>
            <ts e="T359" id="Seg_4112" n="sc" s="T355">
               <ts e="T356" id="Seg_4114" n="e" s="T355">Kajbiʔi </ts>
               <ts e="T357" id="Seg_4116" n="e" s="T356">aj, </ts>
               <ts e="T358" id="Seg_4118" n="e" s="T357">šobi </ts>
               <ts e="T359" id="Seg_4120" n="e" s="T358">urgaːba. </ts>
            </ts>
            <ts e="T362" id="Seg_4121" n="sc" s="T360">
               <ts e="T361" id="Seg_4123" n="e" s="T360">"Öʔləʔgeʔ </ts>
               <ts e="T362" id="Seg_4125" n="e" s="T361">măna!" </ts>
            </ts>
            <ts e="T372" id="Seg_4126" n="sc" s="T363">
               <ts e="T364" id="Seg_4128" n="e" s="T363">Dĭzeŋ </ts>
               <ts e="T365" id="Seg_4130" n="e" s="T364">ej </ts>
               <ts e="T366" id="Seg_4132" n="e" s="T365">öʔliʔi. </ts>
               <ts e="T367" id="Seg_4134" n="e" s="T366">"Tüjö </ts>
               <ts e="T368" id="Seg_4136" n="e" s="T367">bar </ts>
               <ts e="T369" id="Seg_4138" n="e" s="T368">băldəlam </ts>
               <ts e="T370" id="Seg_4140" n="e" s="T369">šiʔ </ts>
               <ts e="T371" id="Seg_4142" n="e" s="T370">(turagəʔ=) </ts>
               <ts e="T372" id="Seg_4144" n="e" s="T371">turanə!" </ts>
            </ts>
            <ts e="T377" id="Seg_4145" n="sc" s="T373">
               <ts e="T374" id="Seg_4147" n="e" s="T373">Dĭgəttə </ts>
               <ts e="T375" id="Seg_4149" n="e" s="T374">băldəbi, </ts>
               <ts e="T376" id="Seg_4151" n="e" s="T375">dĭzeŋ </ts>
               <ts e="T377" id="Seg_4153" n="e" s="T376">nuʔməluʔpiʔi. </ts>
            </ts>
            <ts e="T380" id="Seg_4154" n="sc" s="T378">
               <ts e="T379" id="Seg_4156" n="e" s="T378">Girgit </ts>
               <ts e="T380" id="Seg_4158" n="e" s="T379">tura. </ts>
            </ts>
            <ts e="T383" id="Seg_4159" n="sc" s="T381">
               <ts e="T382" id="Seg_4161" n="e" s="T381">Muzaŋdə </ts>
               <ts e="T383" id="Seg_4163" n="e" s="T382">abiʔi. </ts>
            </ts>
            <ts e="T392" id="Seg_4164" n="sc" s="T384">
               <ts e="T385" id="Seg_4166" n="e" s="T384">I </ts>
               <ts e="T386" id="Seg_4168" n="e" s="T385">dĭbər </ts>
               <ts e="T387" id="Seg_4170" n="e" s="T386">aj </ts>
               <ts e="T388" id="Seg_4172" n="e" s="T387">(karl-) </ts>
               <ts e="T389" id="Seg_4174" n="e" s="T388">kajluʔpiʔi. </ts>
               <ts e="T390" id="Seg_4176" n="e" s="T389">Dĭ </ts>
               <ts e="T391" id="Seg_4178" n="e" s="T390">dĭbər </ts>
               <ts e="T392" id="Seg_4180" n="e" s="T391">šobi. </ts>
            </ts>
            <ts e="T394" id="Seg_4181" n="sc" s="T393">
               <ts e="T394" id="Seg_4183" n="e" s="T393">"Kargaʔ! </ts>
            </ts>
            <ts e="T400" id="Seg_4184" n="sc" s="T395">
               <ts e="T396" id="Seg_4186" n="e" s="T395">A_to </ts>
               <ts e="T397" id="Seg_4188" n="e" s="T396">tüjö </ts>
               <ts e="T398" id="Seg_4190" n="e" s="T397">bar </ts>
               <ts e="T399" id="Seg_4192" n="e" s="T398">saʔməluʔləj </ts>
               <ts e="T400" id="Seg_4194" n="e" s="T399">tura". </ts>
            </ts>
            <ts e="T406" id="Seg_4195" n="sc" s="T401">
               <ts e="T402" id="Seg_4197" n="e" s="T401">Dĭgəttə </ts>
               <ts e="T403" id="Seg_4199" n="e" s="T402">bar </ts>
               <ts e="T404" id="Seg_4201" n="e" s="T403">baruʔpi, </ts>
               <ts e="T405" id="Seg_4203" n="e" s="T404">dĭzeŋ </ts>
               <ts e="T406" id="Seg_4205" n="e" s="T405">nuʔməluʔpiʔi. </ts>
            </ts>
            <ts e="T411" id="Seg_4206" n="sc" s="T407">
               <ts e="T408" id="Seg_4208" n="e" s="T407">I </ts>
               <ts e="T409" id="Seg_4210" n="e" s="T408">šobiʔi </ts>
               <ts e="T410" id="Seg_4212" n="e" s="T409">döbər </ts>
               <ts e="T411" id="Seg_4214" n="e" s="T410">onʼiʔdə. </ts>
            </ts>
            <ts e="T415" id="Seg_4215" n="sc" s="T412">
               <ts e="T413" id="Seg_4217" n="e" s="T412">Nagurdə </ts>
               <ts e="T414" id="Seg_4219" n="e" s="T413">šoškandə </ts>
               <ts e="T415" id="Seg_4221" n="e" s="T414">šobiʔi. </ts>
            </ts>
            <ts e="T428" id="Seg_4222" n="sc" s="T416">
               <ts e="T417" id="Seg_4224" n="e" s="T416">"Öʔleʔ </ts>
               <ts e="T418" id="Seg_4226" n="e" s="T417">miʔnʼibeʔ!" </ts>
               <ts e="T419" id="Seg_4228" n="e" s="T418">Dĭ </ts>
               <ts e="T420" id="Seg_4230" n="e" s="T419">öʔlübi </ts>
               <ts e="T421" id="Seg_4232" n="e" s="T420">i </ts>
               <ts e="T422" id="Seg_4234" n="e" s="T421">ajbə </ts>
               <ts e="T423" id="Seg_4236" n="e" s="T422">(karb-) </ts>
               <ts e="T424" id="Seg_4238" n="e" s="T423">kajbi. </ts>
               <ts e="T425" id="Seg_4240" n="e" s="T424">Dĭ </ts>
               <ts e="T426" id="Seg_4242" n="e" s="T425">šobi, </ts>
               <ts e="T427" id="Seg_4244" n="e" s="T426">urgaːba. </ts>
               <ts e="T428" id="Seg_4246" n="e" s="T427">"Öʔleʔ!" </ts>
            </ts>
            <ts e="T431" id="Seg_4247" n="sc" s="T429">
               <ts e="T430" id="Seg_4249" n="e" s="T429">"Ej </ts>
               <ts e="T431" id="Seg_4251" n="e" s="T430">öʔləm!" </ts>
            </ts>
            <ts e="T445" id="Seg_4252" n="sc" s="T432">
               <ts e="T433" id="Seg_4254" n="e" s="T432">Dĭgəttə </ts>
               <ts e="T434" id="Seg_4256" n="e" s="T433">dĭ </ts>
               <ts e="T435" id="Seg_4258" n="e" s="T434">băldəsʼtə </ts>
               <ts e="T436" id="Seg_4260" n="e" s="T435">xatʼel. </ts>
               <ts e="T437" id="Seg_4262" n="e" s="T436">Ej </ts>
               <ts e="T438" id="Seg_4264" n="e" s="T437">mobi. </ts>
               <ts e="T439" id="Seg_4266" n="e" s="T438">Dĭgəttə </ts>
               <ts e="T440" id="Seg_4268" n="e" s="T439">(sʼa-) </ts>
               <ts e="T441" id="Seg_4270" n="e" s="T440">sʼabi </ts>
               <ts e="T442" id="Seg_4272" n="e" s="T441">dĭbər </ts>
               <ts e="T443" id="Seg_4274" n="e" s="T442">(nu-) </ts>
               <ts e="T444" id="Seg_4276" n="e" s="T443">nʼuʔdə </ts>
               <ts e="T445" id="Seg_4278" n="e" s="T444">trubandə. </ts>
            </ts>
            <ts e="T453" id="Seg_4279" n="sc" s="T446">
               <ts e="T447" id="Seg_4281" n="e" s="T446">Paʔlaːndəga. </ts>
               <ts e="T448" id="Seg_4283" n="e" s="T447">A </ts>
               <ts e="T449" id="Seg_4285" n="e" s="T448">dĭ </ts>
               <ts e="T450" id="Seg_4287" n="e" s="T449">aspaʔ </ts>
               <ts e="T451" id="Seg_4289" n="e" s="T450">nuldəbi </ts>
               <ts e="T452" id="Seg_4291" n="e" s="T451">tʼibige </ts>
               <ts e="T453" id="Seg_4293" n="e" s="T452">büzʼiʔ. </ts>
            </ts>
            <ts e="T458" id="Seg_4294" n="sc" s="T454">
               <ts e="T455" id="Seg_4296" n="e" s="T454">Dĭ </ts>
               <ts e="T456" id="Seg_4298" n="e" s="T455">bar </ts>
               <ts e="T457" id="Seg_4300" n="e" s="T456">saʔməluʔpi </ts>
               <ts e="T458" id="Seg_4302" n="e" s="T457">aspaʔdə. </ts>
            </ts>
            <ts e="T470" id="Seg_4303" n="sc" s="T459">
               <ts e="T460" id="Seg_4305" n="e" s="T459">I </ts>
               <ts e="T461" id="Seg_4307" n="e" s="T460">(bĭ-) </ts>
               <ts e="T462" id="Seg_4309" n="e" s="T461">dĭgəttə </ts>
               <ts e="T463" id="Seg_4311" n="e" s="T462">paruʔpi </ts>
               <ts e="T464" id="Seg_4313" n="e" s="T463">i </ts>
               <ts e="T966" id="Seg_4315" n="e" s="T464">kalla </ts>
               <ts e="T465" id="Seg_4317" n="e" s="T966">dʼürbi </ts>
               <ts e="T466" id="Seg_4319" n="e" s="T465">i </ts>
               <ts e="T467" id="Seg_4321" n="e" s="T466">daška </ts>
               <ts e="T468" id="Seg_4323" n="e" s="T467">kaməndə </ts>
               <ts e="T469" id="Seg_4325" n="e" s="T468">ej </ts>
               <ts e="T470" id="Seg_4327" n="e" s="T469">šobi. </ts>
            </ts>
            <ts e="T474" id="Seg_4328" n="sc" s="T471">
               <ts e="T472" id="Seg_4330" n="e" s="T471">(Nendəb-) </ts>
               <ts e="T473" id="Seg_4332" n="e" s="T472">Nendəbiʔi </ts>
               <ts e="T474" id="Seg_4334" n="e" s="T473">dĭm. </ts>
            </ts>
            <ts e="T477" id="Seg_4335" n="sc" s="T475">
               <ts e="T476" id="Seg_4337" n="e" s="T475">I </ts>
               <ts e="T967" id="Seg_4339" n="e" s="T476">kalla </ts>
               <ts e="T477" id="Seg_4341" n="e" s="T967">dʼürbi. </ts>
            </ts>
            <ts e="T484" id="Seg_4342" n="sc" s="T478">
               <ts e="T479" id="Seg_4344" n="e" s="T478">Kumen </ts>
               <ts e="T480" id="Seg_4346" n="e" s="T479">(mi-) </ts>
               <ts e="T481" id="Seg_4348" n="e" s="T480">miʔ </ts>
               <ts e="T482" id="Seg_4350" n="e" s="T481">plʼonkaʔi </ts>
               <ts e="T483" id="Seg_4352" n="e" s="T482">(dʼăbaktərbiam) </ts>
               <ts e="T484" id="Seg_4354" n="e" s="T483">dʼăbaktərbibaʔ? </ts>
            </ts>
            <ts e="T487" id="Seg_4355" n="sc" s="T485">
               <ts e="T486" id="Seg_4357" n="e" s="T485">Bʼeʔ </ts>
               <ts e="T487" id="Seg_4359" n="e" s="T486">nagur. </ts>
            </ts>
            <ts e="T491" id="Seg_4360" n="sc" s="T488">
               <ts e="T489" id="Seg_4362" n="e" s="T488">Šiʔ </ts>
               <ts e="T490" id="Seg_4364" n="e" s="T489">ipekleʔ </ts>
               <ts e="T491" id="Seg_4366" n="e" s="T490">ige. </ts>
            </ts>
            <ts e="T496" id="Seg_4367" n="sc" s="T492">
               <ts e="T493" id="Seg_4369" n="e" s="T492">Ige, </ts>
               <ts e="T494" id="Seg_4371" n="e" s="T493">a </ts>
               <ts e="T495" id="Seg_4373" n="e" s="T494">gijen </ts>
               <ts e="T496" id="Seg_4375" n="e" s="T495">ibileʔ? </ts>
            </ts>
            <ts e="T501" id="Seg_4376" n="sc" s="T497">
               <ts e="T498" id="Seg_4378" n="e" s="T497">Da </ts>
               <ts e="T499" id="Seg_4380" n="e" s="T498">onʼiʔ </ts>
               <ts e="T500" id="Seg_4382" n="e" s="T499">ne </ts>
               <ts e="T501" id="Seg_4384" n="e" s="T500">deppi. </ts>
            </ts>
            <ts e="T504" id="Seg_4385" n="sc" s="T502">
               <ts e="T503" id="Seg_4387" n="e" s="T502">(Vasʼ-) </ts>
               <ts e="T504" id="Seg_4389" n="e" s="T503">Vaznʼesenkaʔizʼi. </ts>
            </ts>
            <ts e="T510" id="Seg_4390" n="sc" s="T505">
               <ts e="T506" id="Seg_4392" n="e" s="T505">Onʼiʔ </ts>
               <ts e="T507" id="Seg_4394" n="e" s="T506">poʔton </ts>
               <ts e="T508" id="Seg_4396" n="e" s="T507">sumna </ts>
               <ts e="T509" id="Seg_4398" n="e" s="T508">esseŋdə </ts>
               <ts e="T510" id="Seg_4400" n="e" s="T509">ibiʔi. </ts>
            </ts>
            <ts e="T515" id="Seg_4401" n="sc" s="T511">
               <ts e="T512" id="Seg_4403" n="e" s="T511">Dĭ </ts>
               <ts e="T513" id="Seg_4405" n="e" s="T512">dĭzem </ts>
               <ts e="T514" id="Seg_4407" n="e" s="T513">maʔnən </ts>
               <ts e="T515" id="Seg_4409" n="e" s="T514">maːbi. </ts>
            </ts>
            <ts e="T518" id="Seg_4410" n="sc" s="T516">
               <ts e="T517" id="Seg_4412" n="e" s="T516">Ajm </ts>
               <ts e="T518" id="Seg_4414" n="e" s="T517">kajbi. </ts>
            </ts>
            <ts e="T524" id="Seg_4415" n="sc" s="T519">
               <ts e="T520" id="Seg_4417" n="e" s="T519">"Šindimdə </ts>
               <ts e="T521" id="Seg_4419" n="e" s="T520">(iʔ </ts>
               <ts e="T522" id="Seg_4421" n="e" s="T521">öʔləʔ-) </ts>
               <ts e="T523" id="Seg_4423" n="e" s="T522">iʔ </ts>
               <ts e="T524" id="Seg_4425" n="e" s="T523">(öʔləʔleʔ)!" </ts>
            </ts>
            <ts e="T527" id="Seg_4426" n="sc" s="T525">
               <ts e="T526" id="Seg_4428" n="e" s="T525">Dĭgəttə </ts>
               <ts e="T968" id="Seg_4430" n="e" s="T526">kalla </ts>
               <ts e="T527" id="Seg_4432" n="e" s="T968">dʼürbi. </ts>
            </ts>
            <ts e="T532" id="Seg_4433" n="sc" s="T528">
               <ts e="T529" id="Seg_4435" n="e" s="T528">Urgaːba </ts>
               <ts e="T530" id="Seg_4437" n="e" s="T529">šobi. </ts>
               <ts e="T531" id="Seg_4439" n="e" s="T530">"Kargat </ts>
               <ts e="T532" id="Seg_4441" n="e" s="T531">aj!" </ts>
            </ts>
            <ts e="T536" id="Seg_4442" n="sc" s="T533">
               <ts e="T534" id="Seg_4444" n="e" s="T533">Dĭzeŋ </ts>
               <ts e="T535" id="Seg_4446" n="e" s="T534">ej </ts>
               <ts e="T536" id="Seg_4448" n="e" s="T535">karlaʔi. </ts>
            </ts>
            <ts e="T543" id="Seg_4449" n="sc" s="T537">
               <ts e="T538" id="Seg_4451" n="e" s="T537">"Miʔnʼibeʔ </ts>
               <ts e="T539" id="Seg_4453" n="e" s="T538">iam </ts>
               <ts e="T540" id="Seg_4455" n="e" s="T539">mămbi, </ts>
               <ts e="T541" id="Seg_4457" n="e" s="T540">šindimdə </ts>
               <ts e="T542" id="Seg_4459" n="e" s="T541">ej </ts>
               <ts e="T543" id="Seg_4461" n="e" s="T542">öʔləʔsittə". </ts>
            </ts>
            <ts e="T549" id="Seg_4462" n="sc" s="T544">
               <ts e="T545" id="Seg_4464" n="e" s="T544">Dĭgəttə </ts>
               <ts e="T546" id="Seg_4466" n="e" s="T545">urgaːba </ts>
               <ts e="T969" id="Seg_4468" n="e" s="T546">kalla </ts>
               <ts e="T547" id="Seg_4470" n="e" s="T969">dʼürbi, </ts>
               <ts e="T548" id="Seg_4472" n="e" s="T547">iat </ts>
               <ts e="T549" id="Seg_4474" n="e" s="T548">šobi. </ts>
            </ts>
            <ts e="T555" id="Seg_4475" n="sc" s="T550">
               <ts e="T551" id="Seg_4477" n="e" s="T550">"Kargaʔ </ts>
               <ts e="T552" id="Seg_4479" n="e" s="T551">aj, </ts>
               <ts e="T553" id="Seg_4481" n="e" s="T552">măn </ts>
               <ts e="T554" id="Seg_4483" n="e" s="T553">deʔpiem </ts>
               <ts e="T555" id="Seg_4485" n="e" s="T554">süt! </ts>
            </ts>
            <ts e="T561" id="Seg_4486" n="sc" s="T556">
               <ts e="T557" id="Seg_4488" n="e" s="T556">Deʔpiem </ts>
               <ts e="T558" id="Seg_4490" n="e" s="T557">noʔ </ts>
               <ts e="T559" id="Seg_4492" n="e" s="T558">šiʔnʼileʔ! </ts>
               <ts e="T560" id="Seg_4494" n="e" s="T559">Tvorog </ts>
               <ts e="T561" id="Seg_4496" n="e" s="T560">deʔpiem". </ts>
            </ts>
            <ts e="T570" id="Seg_4497" n="sc" s="T562">
               <ts e="T563" id="Seg_4499" n="e" s="T562">Dĭgəttə </ts>
               <ts e="T564" id="Seg_4501" n="e" s="T563">dĭzeŋ </ts>
               <ts e="T565" id="Seg_4503" n="e" s="T564">karbiʔi, </ts>
               <ts e="T566" id="Seg_4505" n="e" s="T565">ambiʔi. </ts>
               <ts e="T567" id="Seg_4507" n="e" s="T566">Dĭgəttə </ts>
               <ts e="T568" id="Seg_4509" n="e" s="T567">iat </ts>
               <ts e="T569" id="Seg_4511" n="e" s="T568">bazoʔ </ts>
               <ts e="T570" id="Seg_4513" n="e" s="T569">kambi. </ts>
            </ts>
            <ts e="T574" id="Seg_4514" n="sc" s="T571">
               <ts e="T572" id="Seg_4516" n="e" s="T571">"Iʔ </ts>
               <ts e="T573" id="Seg_4518" n="e" s="T572">öʔləʔgeʔ </ts>
               <ts e="T574" id="Seg_4520" n="e" s="T573">šindimdə!" </ts>
            </ts>
            <ts e="T577" id="Seg_4521" n="sc" s="T575">
               <ts e="T576" id="Seg_4523" n="e" s="T575">Bazoʔ </ts>
               <ts e="T970" id="Seg_4525" n="e" s="T576">kalla </ts>
               <ts e="T577" id="Seg_4527" n="e" s="T970">dʼürbi. </ts>
            </ts>
            <ts e="T581" id="Seg_4528" n="sc" s="T578">
               <ts e="T579" id="Seg_4530" n="e" s="T578">"Šindimdə </ts>
               <ts e="T580" id="Seg_4532" n="e" s="T579">iʔ </ts>
               <ts e="T581" id="Seg_4534" n="e" s="T580">öʔləʔ!" </ts>
            </ts>
            <ts e="T586" id="Seg_4535" n="sc" s="T582">
               <ts e="T583" id="Seg_4537" n="e" s="T582">Dĭgəttə </ts>
               <ts e="T584" id="Seg_4539" n="e" s="T583">bazoʔ </ts>
               <ts e="T585" id="Seg_4541" n="e" s="T584">šobi </ts>
               <ts e="T586" id="Seg_4543" n="e" s="T585">urgaːba. </ts>
            </ts>
            <ts e="T591" id="Seg_4544" n="sc" s="T587">
               <ts e="T588" id="Seg_4546" n="e" s="T587">"Esseŋ, </ts>
               <ts e="T589" id="Seg_4548" n="e" s="T588">esseŋ, </ts>
               <ts e="T590" id="Seg_4550" n="e" s="T589">öʔleʔ </ts>
               <ts e="T591" id="Seg_4552" n="e" s="T590">măna!" </ts>
            </ts>
            <ts e="T597" id="Seg_4553" n="sc" s="T592">
               <ts e="T593" id="Seg_4555" n="e" s="T592">"Dʼok, </ts>
               <ts e="T594" id="Seg_4557" n="e" s="T593">(mɨ-) </ts>
               <ts e="T595" id="Seg_4559" n="e" s="T594">miʔ </ts>
               <ts e="T596" id="Seg_4561" n="e" s="T595">ej </ts>
               <ts e="T597" id="Seg_4563" n="e" s="T596">(öʔleʔ)". </ts>
            </ts>
            <ts e="T609" id="Seg_4564" n="sc" s="T598">
               <ts e="T599" id="Seg_4566" n="e" s="T598">"A </ts>
               <ts e="T600" id="Seg_4568" n="e" s="T599">măn </ts>
               <ts e="T601" id="Seg_4570" n="e" s="T600">šiʔ </ts>
               <ts e="T602" id="Seg_4572" n="e" s="T601">ia </ts>
               <ts e="T603" id="Seg_4574" n="e" s="T602">igem!" </ts>
               <ts e="T604" id="Seg_4576" n="e" s="T603">"Dʼok, </ts>
               <ts e="T605" id="Seg_4578" n="e" s="T604">măn </ts>
               <ts e="T606" id="Seg_4580" n="e" s="T605">(ia-) </ts>
               <ts e="T607" id="Seg_4582" n="e" s="T606">ianə </ts>
               <ts e="T608" id="Seg_4584" n="e" s="T607">golosdə </ts>
               <ts e="T609" id="Seg_4586" n="e" s="T608">todam! </ts>
            </ts>
            <ts e="T615" id="Seg_4587" n="sc" s="T610">
               <ts e="T611" id="Seg_4589" n="e" s="T610">A </ts>
               <ts e="T612" id="Seg_4591" n="e" s="T611">tăn </ts>
               <ts e="T613" id="Seg_4593" n="e" s="T612">golosdə </ts>
               <ts e="T614" id="Seg_4595" n="e" s="T613">ugaːndə </ts>
               <ts e="T615" id="Seg_4597" n="e" s="T614">nʼešpək". </ts>
            </ts>
            <ts e="T622" id="Seg_4598" n="sc" s="T616">
               <ts e="T617" id="Seg_4600" n="e" s="T616">Dĭgəttə </ts>
               <ts e="T618" id="Seg_4602" n="e" s="T617">urgaːba </ts>
               <ts e="T971" id="Seg_4604" n="e" s="T618">kalla </ts>
               <ts e="T619" id="Seg_4606" n="e" s="T971">dʼürbi. </ts>
               <ts e="T620" id="Seg_4608" n="e" s="T619">Dĭgəttə </ts>
               <ts e="T621" id="Seg_4610" n="e" s="T620">iat </ts>
               <ts e="T622" id="Seg_4612" n="e" s="T621">šobi. </ts>
            </ts>
            <ts e="T626" id="Seg_4613" n="sc" s="T623">
               <ts e="T624" id="Seg_4615" n="e" s="T623">Dĭzeŋ </ts>
               <ts e="T625" id="Seg_4617" n="e" s="T624">karbiʔi, </ts>
               <ts e="T626" id="Seg_4619" n="e" s="T625">bădəbi. </ts>
            </ts>
            <ts e="T631" id="Seg_4620" n="sc" s="T627">
               <ts e="T628" id="Seg_4622" n="e" s="T627">"Tüj </ts>
               <ts e="T629" id="Seg_4624" n="e" s="T628">šindinədə </ts>
               <ts e="T630" id="Seg_4626" n="e" s="T629">ej </ts>
               <ts e="T631" id="Seg_4628" n="e" s="T630">öʔləʔ!" </ts>
            </ts>
            <ts e="T636" id="Seg_4629" n="sc" s="T632">
               <ts e="T633" id="Seg_4631" n="e" s="T632">A </ts>
               <ts e="T634" id="Seg_4633" n="e" s="T633">urgaːba </ts>
               <ts e="T635" id="Seg_4635" n="e" s="T634">kambi </ts>
               <ts e="T636" id="Seg_4637" n="e" s="T635">kuznʼitsanə. </ts>
            </ts>
            <ts e="T641" id="Seg_4638" n="sc" s="T637">
               <ts e="T638" id="Seg_4640" n="e" s="T637">Šĭkebə </ts>
               <ts e="T639" id="Seg_4642" n="e" s="T638">(pi-) </ts>
               <ts e="T640" id="Seg_4644" n="e" s="T639">abi </ts>
               <ts e="T641" id="Seg_4646" n="e" s="T640">todam. </ts>
            </ts>
            <ts e="T645" id="Seg_4647" n="sc" s="T642">
               <ts e="T643" id="Seg_4649" n="e" s="T642">Dĭgəttə </ts>
               <ts e="T644" id="Seg_4651" n="e" s="T643">šobi </ts>
               <ts e="T645" id="Seg_4653" n="e" s="T644">bazoʔ. </ts>
            </ts>
            <ts e="T653" id="Seg_4654" n="sc" s="T646">
               <ts e="T647" id="Seg_4656" n="e" s="T646">Dĭ </ts>
               <ts e="T648" id="Seg_4658" n="e" s="T647">mălia: </ts>
               <ts e="T649" id="Seg_4660" n="e" s="T648">"Măn </ts>
               <ts e="T650" id="Seg_4662" n="e" s="T649">šiʔ </ts>
               <ts e="T651" id="Seg_4664" n="e" s="T650">ial, </ts>
               <ts e="T652" id="Seg_4666" n="e" s="T651">deʔpiem </ts>
               <ts e="T653" id="Seg_4668" n="e" s="T652">süt. </ts>
            </ts>
            <ts e="T656" id="Seg_4669" n="sc" s="T654">
               <ts e="T655" id="Seg_4671" n="e" s="T654">Ipek </ts>
               <ts e="T656" id="Seg_4673" n="e" s="T655">deʔpiem. </ts>
            </ts>
            <ts e="T661" id="Seg_4674" n="sc" s="T657">
               <ts e="T658" id="Seg_4676" n="e" s="T657">Deʔpiem </ts>
               <ts e="T659" id="Seg_4678" n="e" s="T658">šiʔnʼileʔ </ts>
               <ts e="T660" id="Seg_4680" n="e" s="T659">творог, </ts>
               <ts e="T661" id="Seg_4682" n="e" s="T660">noʔ". </ts>
            </ts>
            <ts e="T665" id="Seg_4683" n="sc" s="T662">
               <ts e="T663" id="Seg_4685" n="e" s="T662">Dĭzeŋ </ts>
               <ts e="T664" id="Seg_4687" n="e" s="T663">bar </ts>
               <ts e="T665" id="Seg_4689" n="e" s="T664">karluʔpi. </ts>
            </ts>
            <ts e="T670" id="Seg_4690" n="sc" s="T666">
               <ts e="T667" id="Seg_4692" n="e" s="T666">Dĭ </ts>
               <ts e="T668" id="Seg_4694" n="e" s="T667">dĭzem </ts>
               <ts e="T669" id="Seg_4696" n="e" s="T668">bar </ts>
               <ts e="T670" id="Seg_4698" n="e" s="T669">amnuʔpi. </ts>
            </ts>
            <ts e="T673" id="Seg_4699" n="sc" s="T671">
               <ts e="T672" id="Seg_4701" n="e" s="T671">Iat </ts>
               <ts e="T673" id="Seg_4703" n="e" s="T672">šobi. </ts>
            </ts>
            <ts e="T681" id="Seg_4704" n="sc" s="T674">
               <ts e="T675" id="Seg_4706" n="e" s="T674">Esseŋdə </ts>
               <ts e="T676" id="Seg_4708" n="e" s="T675">naga. </ts>
               <ts e="T677" id="Seg_4710" n="e" s="T676">Onʼiʔ, </ts>
               <ts e="T678" id="Seg_4712" n="e" s="T677">самый </ts>
               <ts e="T679" id="Seg_4714" n="e" s="T678">(üdʼü-) </ts>
               <ts e="T680" id="Seg_4716" n="e" s="T679">üdʼüge </ts>
               <ts e="T681" id="Seg_4718" n="e" s="T680">šaʔlaːmbi. </ts>
            </ts>
            <ts e="T687" id="Seg_4719" n="sc" s="T682">
               <ts e="T683" id="Seg_4721" n="e" s="T682">Dĭgəttə </ts>
               <ts e="T684" id="Seg_4723" n="e" s="T683">măndə: </ts>
               <ts e="T685" id="Seg_4725" n="e" s="T684">"Urgaːba </ts>
               <ts e="T686" id="Seg_4727" n="e" s="T685">bar </ts>
               <ts e="T687" id="Seg_4729" n="e" s="T686">amnuʔpi. </ts>
            </ts>
            <ts e="T704" id="Seg_4730" n="sc" s="T688">
               <ts e="T689" id="Seg_4732" n="e" s="T688">Măn </ts>
               <ts e="T690" id="Seg_4734" n="e" s="T689">unnʼa </ts>
               <ts e="T691" id="Seg_4736" n="e" s="T690">maːluʔpiam. </ts>
               <ts e="T692" id="Seg_4738" n="e" s="T691">(Pʼešdə </ts>
               <ts e="T693" id="Seg_4740" n="e" s="T692">ša- </ts>
               <ts e="T694" id="Seg_4742" n="e" s="T693">sabi- </ts>
               <ts e="T695" id="Seg_4744" n="e" s="T694">š- </ts>
               <ts e="T696" id="Seg_4746" n="e" s="T695">sʼa- </ts>
               <ts e="T697" id="Seg_4748" n="e" s="T696">š-) </ts>
               <ts e="T698" id="Seg_4750" n="e" s="T697">Pʼešdə </ts>
               <ts e="T699" id="Seg_4752" n="e" s="T698">păʔlumbiam, </ts>
               <ts e="T700" id="Seg_4754" n="e" s="T699">dĭ </ts>
               <ts e="T701" id="Seg_4756" n="e" s="T700">măna </ts>
               <ts e="T702" id="Seg_4758" n="e" s="T701">ej </ts>
               <ts e="T703" id="Seg_4760" n="e" s="T702">măndərbi, </ts>
               <ts e="T972" id="Seg_4762" n="e" s="T703">kalla </ts>
               <ts e="T704" id="Seg_4764" n="e" s="T972">dʼürbi. </ts>
            </ts>
            <ts e="T709" id="Seg_4765" n="sc" s="T705">
               <ts e="T706" id="Seg_4767" n="e" s="T705">Dĭgəttə </ts>
               <ts e="T707" id="Seg_4769" n="e" s="T706">urgaːba </ts>
               <ts e="T708" id="Seg_4771" n="e" s="T707">šobi </ts>
               <ts e="T709" id="Seg_4773" n="e" s="T708">poʔtonə. </ts>
            </ts>
            <ts e="T715" id="Seg_4774" n="sc" s="T710">
               <ts e="T711" id="Seg_4776" n="e" s="T710">Poʔto </ts>
               <ts e="T712" id="Seg_4778" n="e" s="T711">măndə: </ts>
               <ts e="T713" id="Seg_4780" n="e" s="T712">"(Kan- </ts>
               <ts e="T714" id="Seg_4782" n="e" s="T713">kal-) </ts>
               <ts e="T715" id="Seg_4784" n="e" s="T714">Kanžəbəj. </ts>
            </ts>
            <ts e="T720" id="Seg_4785" n="sc" s="T716">
               <ts e="T717" id="Seg_4787" n="e" s="T716">Suʔmibəj </ts>
               <ts e="T718" id="Seg_4789" n="e" s="T717">dö </ts>
               <ts e="T719" id="Seg_4791" n="e" s="T718">jamagə </ts>
               <ts e="T720" id="Seg_4793" n="e" s="T719">(penzəj). </ts>
            </ts>
            <ts e="T723" id="Seg_4794" n="sc" s="T721">
               <ts e="T722" id="Seg_4796" n="e" s="T721">Poʔto </ts>
               <ts e="T723" id="Seg_4798" n="e" s="T722">suʔməbi. </ts>
            </ts>
            <ts e="T731" id="Seg_4799" n="sc" s="T724">
               <ts e="T725" id="Seg_4801" n="e" s="T724">(A-) </ts>
               <ts e="T726" id="Seg_4803" n="e" s="T725">A </ts>
               <ts e="T727" id="Seg_4805" n="e" s="T726">dĭ </ts>
               <ts e="T728" id="Seg_4807" n="e" s="T727">ej </ts>
               <ts e="T729" id="Seg_4809" n="e" s="T728">mobi </ts>
               <ts e="T730" id="Seg_4811" n="e" s="T729">suʔmistə </ts>
               <ts e="T731" id="Seg_4813" n="e" s="T730">dĭbər. </ts>
            </ts>
            <ts e="T753" id="Seg_4814" n="sc" s="T732">
               <ts e="T733" id="Seg_4816" n="e" s="T732">Kuzurluʔpi, </ts>
               <ts e="T734" id="Seg_4818" n="e" s="T733">ĭmbinəndə </ts>
               <ts e="T735" id="Seg_4820" n="e" s="T734">bar </ts>
               <ts e="T736" id="Seg_4822" n="e" s="T735">nanə </ts>
               <ts e="T737" id="Seg_4824" n="e" s="T736">напорол. </ts>
               <ts e="T738" id="Seg_4826" n="e" s="T737">Dĭgəttə </ts>
               <ts e="T739" id="Seg_4828" n="e" s="T738">dĭn </ts>
               <ts e="T740" id="Seg_4830" n="e" s="T739">(kozl-) </ts>
               <ts e="T741" id="Seg_4832" n="e" s="T740">kăzlʼataʔji </ts>
               <ts e="T742" id="Seg_4834" n="e" s="T741">(s-) </ts>
               <ts e="T743" id="Seg_4836" n="e" s="T742">sumiʔluʔpiʔi </ts>
               <ts e="T744" id="Seg_4838" n="e" s="T743">i </ts>
               <ts e="T745" id="Seg_4840" n="e" s="T744">bazoʔ </ts>
               <ts e="T746" id="Seg_4842" n="e" s="T745">iandə </ts>
               <ts e="T973" id="Seg_4844" n="e" s="T746">kalla </ts>
               <ts e="T747" id="Seg_4846" n="e" s="T973">dʼürbiʔi. </ts>
               <ts e="T748" id="Seg_4848" n="e" s="T747">((BRK)) </ts>
               <ts e="T749" id="Seg_4850" n="e" s="T748">"Kanžəbəj </ts>
               <ts e="T750" id="Seg_4852" n="e" s="T749">gibər-nʼibudʼ </ts>
               <ts e="T751" id="Seg_4854" n="e" s="T750">tănzi </ts>
               <ts e="T752" id="Seg_4856" n="e" s="T751">(jaʔt-) </ts>
               <ts e="T753" id="Seg_4858" n="e" s="T752">jaʔtarla". </ts>
            </ts>
            <ts e="T758" id="Seg_4859" n="sc" s="T754">
               <ts e="T755" id="Seg_4861" n="e" s="T754">"A </ts>
               <ts e="T756" id="Seg_4863" n="e" s="T755">ĭmbi </ts>
               <ts e="T757" id="Seg_4865" n="e" s="T756">dĭn </ts>
               <ts e="T758" id="Seg_4867" n="e" s="T757">moləj?" </ts>
            </ts>
            <ts e="T766" id="Seg_4868" n="sc" s="T759">
               <ts e="T760" id="Seg_4870" n="e" s="T759">"Da </ts>
               <ts e="T761" id="Seg_4872" n="e" s="T760">ĭmbi </ts>
               <ts e="T762" id="Seg_4874" n="e" s="T761">moləj, </ts>
               <ts e="T763" id="Seg_4876" n="e" s="T762">ara </ts>
               <ts e="T764" id="Seg_4878" n="e" s="T763">bĭtləbeʔ. </ts>
               <ts e="T765" id="Seg_4880" n="e" s="T764">Nüjnə </ts>
               <ts e="T766" id="Seg_4882" n="e" s="T765">nüjləbeʔ. </ts>
            </ts>
            <ts e="T770" id="Seg_4883" n="sc" s="T767">
               <ts e="T768" id="Seg_4885" n="e" s="T767">Suʔmiləbeʔ. </ts>
               <ts e="T769" id="Seg_4887" n="e" s="T768">Garmonnʼiazi </ts>
               <ts e="T770" id="Seg_4889" n="e" s="T769">sʼarzbibeʔ. </ts>
            </ts>
            <ts e="T779" id="Seg_4890" n="sc" s="T771">
               <ts e="T772" id="Seg_4892" n="e" s="T771">Dĭn </ts>
               <ts e="T773" id="Seg_4894" n="e" s="T772">bar </ts>
               <ts e="T774" id="Seg_4896" n="e" s="T773">ĭmbi </ts>
               <ts e="T775" id="Seg_4898" n="e" s="T774">iʔgö — </ts>
               <ts e="T776" id="Seg_4900" n="e" s="T775">uja </ts>
               <ts e="T777" id="Seg_4902" n="e" s="T776">iʔgö, </ts>
               <ts e="T778" id="Seg_4904" n="e" s="T777">(seŋ), </ts>
               <ts e="T779" id="Seg_4906" n="e" s="T778">kajaʔ. </ts>
            </ts>
            <ts e="T782" id="Seg_4907" n="sc" s="T780">
               <ts e="T781" id="Seg_4909" n="e" s="T780">Kola </ts>
               <ts e="T782" id="Seg_4911" n="e" s="T781">mĭnzerona. </ts>
            </ts>
            <ts e="T784" id="Seg_4912" n="sc" s="T783">
               <ts e="T784" id="Seg_4914" n="e" s="T783">Ipek. </ts>
            </ts>
            <ts e="T787" id="Seg_4915" n="sc" s="T785">
               <ts e="T786" id="Seg_4917" n="e" s="T785">Sĭreʔpne </ts>
               <ts e="T787" id="Seg_4919" n="e" s="T786">nuga. </ts>
            </ts>
            <ts e="T796" id="Seg_4920" n="sc" s="T788">
               <ts e="T789" id="Seg_4922" n="e" s="T788">Lem </ts>
               <ts e="T790" id="Seg_4924" n="e" s="T789">keʔbdeʔi </ts>
               <ts e="T791" id="Seg_4926" n="e" s="T790">ige, </ts>
               <ts e="T792" id="Seg_4928" n="e" s="T791">kobo </ts>
               <ts e="T793" id="Seg_4930" n="e" s="T792">keʔbdeʔi, </ts>
               <ts e="T794" id="Seg_4932" n="e" s="T793">sagər </ts>
               <ts e="T795" id="Seg_4934" n="e" s="T794">keʔbdeʔi, </ts>
               <ts e="T796" id="Seg_4936" n="e" s="T795">kanžəbəj. </ts>
            </ts>
            <ts e="T800" id="Seg_4937" n="sc" s="T797">
               <ts e="T798" id="Seg_4939" n="e" s="T797">Tăn </ts>
               <ts e="T799" id="Seg_4941" n="e" s="T798">turaʔi </ts>
               <ts e="T800" id="Seg_4943" n="e" s="T799">(kürbil)? </ts>
            </ts>
            <ts e="T802" id="Seg_4944" n="sc" s="T801">
               <ts e="T802" id="Seg_4946" n="e" s="T801">(Kürbiel). </ts>
            </ts>
            <ts e="T806" id="Seg_4947" n="sc" s="T803">
               <ts e="T804" id="Seg_4949" n="e" s="T803">Kumən </ts>
               <ts e="T805" id="Seg_4951" n="e" s="T804">tura </ts>
               <ts e="T806" id="Seg_4953" n="e" s="T805">kürbiel? </ts>
            </ts>
            <ts e="T808" id="Seg_4954" n="sc" s="T807">
               <ts e="T808" id="Seg_4956" n="e" s="T807">Sejʔpü. </ts>
            </ts>
            <ts e="T812" id="Seg_4957" n="sc" s="T809">
               <ts e="T810" id="Seg_4959" n="e" s="T809">(Tăn-nu), </ts>
               <ts e="T811" id="Seg_4961" n="e" s="T810">turazaŋdə </ts>
               <ts e="T812" id="Seg_4963" n="e" s="T811">kürbiel? </ts>
            </ts>
            <ts e="T827" id="Seg_4964" n="sc" s="T813">
               <ts e="T814" id="Seg_4966" n="e" s="T813">Tăn </ts>
               <ts e="T815" id="Seg_4968" n="e" s="T814">(d-) </ts>
               <ts e="T816" id="Seg_4970" n="e" s="T815">dʼijenə </ts>
               <ts e="T817" id="Seg_4972" n="e" s="T816">ej </ts>
               <ts e="T818" id="Seg_4974" n="e" s="T817">mumbiel? </ts>
               <ts e="T819" id="Seg_4976" n="e" s="T818">Ej </ts>
               <ts e="T820" id="Seg_4978" n="e" s="T819">mĭmbiem. </ts>
               <ts e="T821" id="Seg_4980" n="e" s="T820">Măn </ts>
               <ts e="T822" id="Seg_4982" n="e" s="T821">pimniem, </ts>
               <ts e="T823" id="Seg_4984" n="e" s="T822">dĭn </ts>
               <ts e="T824" id="Seg_4986" n="e" s="T823">bar </ts>
               <ts e="T825" id="Seg_4988" n="e" s="T824">urgaːba </ts>
               <ts e="T826" id="Seg_4990" n="e" s="T825">amno </ts>
               <ts e="T827" id="Seg_4992" n="e" s="T826">((…)). </ts>
            </ts>
            <ts e="T832" id="Seg_4993" n="sc" s="T828">
               <ts e="T829" id="Seg_4995" n="e" s="T828">Ĭmbi </ts>
               <ts e="T830" id="Seg_4997" n="e" s="T829">pimniel, </ts>
               <ts e="T831" id="Seg_4999" n="e" s="T830">kanaʔ, </ts>
               <ts e="T832" id="Seg_5001" n="e" s="T831">(ты)! </ts>
            </ts>
            <ts e="T839" id="Seg_5002" n="sc" s="T833">
               <ts e="T834" id="Seg_5004" n="e" s="T833">Ĭmbidə </ts>
               <ts e="T835" id="Seg_5006" n="e" s="T834">(ej </ts>
               <ts e="T836" id="Seg_5008" n="e" s="T835">aləj=) </ts>
               <ts e="T837" id="Seg_5010" n="e" s="T836">ej </ts>
               <ts e="T838" id="Seg_5012" n="e" s="T837">aləj </ts>
               <ts e="T839" id="Seg_5014" n="e" s="T838">tănan. </ts>
            </ts>
            <ts e="T844" id="Seg_5015" n="sc" s="T840">
               <ts e="T841" id="Seg_5017" n="e" s="T840">(Sʼanəluʔlʼil), </ts>
               <ts e="T842" id="Seg_5019" n="e" s="T841">dĭgəttə </ts>
               <ts e="T843" id="Seg_5021" n="e" s="T842">maʔnəl </ts>
               <ts e="T844" id="Seg_5023" n="e" s="T843">(šoləl). </ts>
            </ts>
            <ts e="T847" id="Seg_5024" n="sc" s="T845">
               <ts e="T846" id="Seg_5026" n="e" s="T845">(Leizittə) </ts>
               <ts e="T847" id="Seg_5028" n="e" s="T846">možna. </ts>
            </ts>
            <ts e="T849" id="Seg_5029" n="sc" s="T848">
               <ts e="T849" id="Seg_5031" n="e" s="T848">(…говорю). </ts>
            </ts>
            <ts e="T862" id="Seg_5032" n="sc" s="T850">
               <ts e="T852" id="Seg_5034" n="e" s="T850">"Vsʼo_ravno </ts>
               <ts e="T853" id="Seg_5036" n="e" s="T852">măn </ts>
               <ts e="T854" id="Seg_5038" n="e" s="T853">pimniem. </ts>
               <ts e="T855" id="Seg_5040" n="e" s="T854">Măn </ts>
               <ts e="T856" id="Seg_5042" n="e" s="T855">ĭmbidə </ts>
               <ts e="T857" id="Seg_5044" n="e" s="T856">naga, </ts>
               <ts e="T858" id="Seg_5046" n="e" s="T857">(multuk=) </ts>
               <ts e="T859" id="Seg_5048" n="e" s="T858">multuk </ts>
               <ts e="T860" id="Seg_5050" n="e" s="T859">naga, </ts>
               <ts e="T861" id="Seg_5052" n="e" s="T860">tagajbə </ts>
               <ts e="T862" id="Seg_5054" n="e" s="T861">naga. </ts>
            </ts>
            <ts e="T869" id="Seg_5055" n="sc" s="T863">
               <ts e="T864" id="Seg_5057" n="e" s="T863">Tolʼkă </ts>
               <ts e="T865" id="Seg_5059" n="e" s="T864">pi". </ts>
               <ts e="T866" id="Seg_5061" n="e" s="T865">"A </ts>
               <ts e="T867" id="Seg_5063" n="e" s="T866">dĭ </ts>
               <ts e="T868" id="Seg_5065" n="e" s="T867">ej </ts>
               <ts e="T869" id="Seg_5067" n="e" s="T868">(ĭzəmnəj). </ts>
            </ts>
            <ts e="T874" id="Seg_5068" n="sc" s="T870">
               <ts e="T871" id="Seg_5070" n="e" s="T870">Dĭ </ts>
               <ts e="T872" id="Seg_5072" n="e" s="T871">kuzagəʔ </ts>
               <ts e="T873" id="Seg_5074" n="e" s="T872">pimnie, </ts>
               <ts e="T874" id="Seg_5076" n="e" s="T873">nuʔməluʔləj". </ts>
            </ts>
            <ts e="T882" id="Seg_5077" n="sc" s="T875">
               <ts e="T876" id="Seg_5079" n="e" s="T875">Miʔ </ts>
               <ts e="T877" id="Seg_5081" n="e" s="T876">kambibaʔ </ts>
               <ts e="T878" id="Seg_5083" n="e" s="T877">(ten- </ts>
               <ts e="T879" id="Seg_5085" n="e" s="T878">te-) </ts>
               <ts e="T880" id="Seg_5087" n="e" s="T879">dĭ </ts>
               <ts e="T881" id="Seg_5089" n="e" s="T880">(dʼijegə) </ts>
               <ts e="T882" id="Seg_5091" n="e" s="T881">dʼijenə. </ts>
            </ts>
            <ts e="T886" id="Seg_5092" n="sc" s="T883">
               <ts e="T884" id="Seg_5094" n="e" s="T883">Dĭgəttə </ts>
               <ts e="T885" id="Seg_5096" n="e" s="T884">sĭre </ts>
               <ts e="T886" id="Seg_5098" n="e" s="T885">kanluʔpi. </ts>
            </ts>
            <ts e="T894" id="Seg_5099" n="sc" s="T887">
               <ts e="T888" id="Seg_5101" n="e" s="T887">Dĭzeŋ </ts>
               <ts e="T889" id="Seg_5103" n="e" s="T888">nagurgö </ts>
               <ts e="T890" id="Seg_5105" n="e" s="T889">kambiʔi, </ts>
               <ts e="T891" id="Seg_5107" n="e" s="T890">a </ts>
               <ts e="T892" id="Seg_5109" n="e" s="T891">măn </ts>
               <ts e="T893" id="Seg_5111" n="e" s="T892">parluʔpiam </ts>
               <ts e="T894" id="Seg_5113" n="e" s="T893">piʔtə. </ts>
            </ts>
            <ts e="T903" id="Seg_5114" n="sc" s="T895">
               <ts e="T896" id="Seg_5116" n="e" s="T895">Dĭgəttə </ts>
               <ts e="T897" id="Seg_5118" n="e" s="T896">nünniem: </ts>
               <ts e="T898" id="Seg_5120" n="e" s="T897">šindidə </ts>
               <ts e="T899" id="Seg_5122" n="e" s="T898">bar </ts>
               <ts e="T900" id="Seg_5124" n="e" s="T899">(muzaŋ=) </ts>
               <ts e="T901" id="Seg_5126" n="e" s="T900">muzaŋdə </ts>
               <ts e="T902" id="Seg_5128" n="e" s="T901">bar </ts>
               <ts e="T903" id="Seg_5130" n="e" s="T902">bădlaʔpə. </ts>
            </ts>
            <ts e="T919" id="Seg_5131" n="sc" s="T904">
               <ts e="T905" id="Seg_5133" n="e" s="T904">(N-) </ts>
               <ts e="T906" id="Seg_5135" n="e" s="T905">Tenniem: </ts>
               <ts e="T907" id="Seg_5137" n="e" s="T906">urgaːba </ts>
               <ts e="T908" id="Seg_5139" n="e" s="T907">(š- </ts>
               <ts e="T909" id="Seg_5141" n="e" s="T908">kan- </ts>
               <ts e="T910" id="Seg_5143" n="e" s="T909">š-) </ts>
               <ts e="T911" id="Seg_5145" n="e" s="T910">bar </ts>
               <ts e="T912" id="Seg_5147" n="e" s="T911">((…)) </ts>
               <ts e="T913" id="Seg_5149" n="e" s="T912">(măldlaʔpə). </ts>
               <ts e="T914" id="Seg_5151" n="e" s="T913">A </ts>
               <ts e="T915" id="Seg_5153" n="e" s="T914">kuzittə </ts>
               <ts e="T916" id="Seg_5155" n="e" s="T915">ej </ts>
               <ts e="T917" id="Seg_5157" n="e" s="T916">kubiam, </ts>
               <ts e="T918" id="Seg_5159" n="e" s="T917">maʔnʼi </ts>
               <ts e="T919" id="Seg_5161" n="e" s="T918">šobiam. </ts>
            </ts>
            <ts e="T925" id="Seg_5162" n="sc" s="T920">
               <ts e="T921" id="Seg_5164" n="e" s="T920">Măn </ts>
               <ts e="T922" id="Seg_5166" n="e" s="T921">šonəgam </ts>
               <ts e="T923" id="Seg_5168" n="e" s="T922">i </ts>
               <ts e="T924" id="Seg_5170" n="e" s="T923">nereluʔpiem </ts>
               <ts e="T925" id="Seg_5172" n="e" s="T924">bar. </ts>
            </ts>
            <ts e="T937" id="Seg_5173" n="sc" s="T926">
               <ts e="T927" id="Seg_5175" n="e" s="T926">Paʔinə </ts>
               <ts e="T928" id="Seg_5177" n="e" s="T927">nada </ts>
               <ts e="T929" id="Seg_5179" n="e" s="T928">sʼazittə. </ts>
               <ts e="T930" id="Seg_5181" n="e" s="T929">A </ts>
               <ts e="T931" id="Seg_5183" n="e" s="T930">măn </ts>
               <ts e="T932" id="Seg_5185" n="e" s="T931">dĭʔnə </ts>
               <ts e="T933" id="Seg_5187" n="e" s="T932">mămbiam: </ts>
               <ts e="T934" id="Seg_5189" n="e" s="T933">"Dĭ </ts>
               <ts e="T935" id="Seg_5191" n="e" s="T934">tože </ts>
               <ts e="T936" id="Seg_5193" n="e" s="T935">paʔinə </ts>
               <ts e="T937" id="Seg_5195" n="e" s="T936">sʼalia". </ts>
            </ts>
            <ts e="T944" id="Seg_5196" n="sc" s="T938">
               <ts e="T939" id="Seg_5198" n="e" s="T938">I </ts>
               <ts e="T940" id="Seg_5200" n="e" s="T939">pa </ts>
               <ts e="T941" id="Seg_5202" n="e" s="T940">baldlia, </ts>
               <ts e="T942" id="Seg_5204" n="e" s="T941">i </ts>
               <ts e="T943" id="Seg_5206" n="e" s="T942">dʼünə </ts>
               <ts e="T944" id="Seg_5208" n="e" s="T943">(barʔdlia). </ts>
            </ts>
            <ts e="T951" id="Seg_5209" n="sc" s="T945">
               <ts e="T946" id="Seg_5211" n="e" s="T945">Kadəʔ </ts>
               <ts e="T947" id="Seg_5213" n="e" s="T946">dĭn </ts>
               <ts e="T948" id="Seg_5215" n="e" s="T947">dʼăbaktərzittə, </ts>
               <ts e="T949" id="Seg_5217" n="e" s="T948">esseŋ </ts>
               <ts e="T950" id="Seg_5219" n="e" s="T949">bar </ts>
               <ts e="T951" id="Seg_5221" n="e" s="T950">kirgarlaʔbə. </ts>
            </ts>
            <ts e="T959" id="Seg_5222" n="sc" s="T952">
               <ts e="T953" id="Seg_5224" n="e" s="T952">Šoškaʔi </ts>
               <ts e="T954" id="Seg_5226" n="e" s="T953">tože </ts>
               <ts e="T955" id="Seg_5228" n="e" s="T954">bar </ts>
               <ts e="T956" id="Seg_5230" n="e" s="T955">kirgarlaʔbəʔjə. </ts>
               <ts e="T957" id="Seg_5232" n="e" s="T956">(Puzo) </ts>
               <ts e="T958" id="Seg_5234" n="e" s="T957">tože </ts>
               <ts e="T959" id="Seg_5236" n="e" s="T958">mürerlaʔbə. </ts>
            </ts>
            <ts e="T963" id="Seg_5237" n="sc" s="T960">
               <ts e="T961" id="Seg_5239" n="e" s="T960">Kădedə </ts>
               <ts e="T962" id="Seg_5241" n="e" s="T961">nʼelʼzʼa </ts>
               <ts e="T963" id="Seg_5243" n="e" s="T962">dʼăbaktərzittə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_5244" s="T1">PKZ_196X_SU0213.001 (001)</ta>
            <ta e="T8" id="Seg_5245" s="T6">PKZ_196X_SU0213.002 (002)</ta>
            <ta e="T11" id="Seg_5246" s="T9">PKZ_196X_SU0213.003 (003)</ta>
            <ta e="T15" id="Seg_5247" s="T12">PKZ_196X_SU0213.004 (004)</ta>
            <ta e="T23" id="Seg_5248" s="T16">PKZ_196X_SU0213.005 (005)</ta>
            <ta e="T30" id="Seg_5249" s="T23">PKZ_196X_SU0213.006 (006)</ta>
            <ta e="T36" id="Seg_5250" s="T31">PKZ_196X_SU0213.007 (007)</ta>
            <ta e="T40" id="Seg_5251" s="T37">PKZ_196X_SU0213.008 (008)</ta>
            <ta e="T46" id="Seg_5252" s="T41">PKZ_196X_SU0213.009 (009)</ta>
            <ta e="T50" id="Seg_5253" s="T46">PKZ_196X_SU0213.010 (010)</ta>
            <ta e="T54" id="Seg_5254" s="T51">PKZ_196X_SU0213.011 (011)</ta>
            <ta e="T59" id="Seg_5255" s="T55">PKZ_196X_SU0213.012 (012)</ta>
            <ta e="T69" id="Seg_5256" s="T60">PKZ_196X_SU0213.013 (013)</ta>
            <ta e="T72" id="Seg_5257" s="T69">PKZ_196X_SU0213.014 (014)</ta>
            <ta e="T75" id="Seg_5258" s="T73">PKZ_196X_SU0213.015 (015)</ta>
            <ta e="T78" id="Seg_5259" s="T76">PKZ_196X_SU0213.016 (016)</ta>
            <ta e="T82" id="Seg_5260" s="T79">PKZ_196X_SU0213.017 (017)</ta>
            <ta e="T91" id="Seg_5261" s="T83">PKZ_196X_SU0213.018 (018)</ta>
            <ta e="T97" id="Seg_5262" s="T92">PKZ_196X_SU0213.019 (019)</ta>
            <ta e="T102" id="Seg_5263" s="T98">PKZ_196X_SU0213.020 (020)</ta>
            <ta e="T106" id="Seg_5264" s="T103">PKZ_196X_SU0213.021 (021)</ta>
            <ta e="T112" id="Seg_5265" s="T107">PKZ_196X_SU0213.022 (022)</ta>
            <ta e="T116" id="Seg_5266" s="T113">PKZ_196X_SU0213.023 (023)</ta>
            <ta e="T120" id="Seg_5267" s="T117">PKZ_196X_SU0213.024 (024)</ta>
            <ta e="T123" id="Seg_5268" s="T121">PKZ_196X_SU0213.025 (025)</ta>
            <ta e="T126" id="Seg_5269" s="T124">PKZ_196X_SU0213.026 (026)</ta>
            <ta e="T131" id="Seg_5270" s="T127">PKZ_196X_SU0213.027 (027)</ta>
            <ta e="T137" id="Seg_5271" s="T132">PKZ_196X_SU0213.028 (028)</ta>
            <ta e="T140" id="Seg_5272" s="T138">PKZ_196X_SU0213.029 (029)</ta>
            <ta e="T145" id="Seg_5273" s="T141">PKZ_196X_SU0213.030 (030)</ta>
            <ta e="T150" id="Seg_5274" s="T146">PKZ_196X_SU0213.031 (031)</ta>
            <ta e="T156" id="Seg_5275" s="T151">PKZ_196X_SU0213.032 (032)</ta>
            <ta e="T165" id="Seg_5276" s="T157">PKZ_196X_SU0213.033 (033)</ta>
            <ta e="T169" id="Seg_5277" s="T166">PKZ_196X_SU0213.034 (034)</ta>
            <ta e="T174" id="Seg_5278" s="T170">PKZ_196X_SU0213.035 (035)</ta>
            <ta e="T181" id="Seg_5279" s="T175">PKZ_196X_SU0213.036 (036)</ta>
            <ta e="T183" id="Seg_5280" s="T182">PKZ_196X_SU0213.037 (037)</ta>
            <ta e="T190" id="Seg_5281" s="T184">PKZ_196X_SU0213.038 (038)</ta>
            <ta e="T200" id="Seg_5282" s="T190">PKZ_196X_SU0213.039 (039)</ta>
            <ta e="T208" id="Seg_5283" s="T201">PKZ_196X_SU0213.040 (040)</ta>
            <ta e="T213" id="Seg_5284" s="T209">PKZ_196X_SU0213.041 (041)</ta>
            <ta e="T220" id="Seg_5285" s="T214">PKZ_196X_SU0213.042 (042)</ta>
            <ta e="T225" id="Seg_5286" s="T221">PKZ_196X_SU0213.043 (043)</ta>
            <ta e="T231" id="Seg_5287" s="T226">PKZ_196X_SU0213.044 (044)</ta>
            <ta e="T236" id="Seg_5288" s="T232">PKZ_196X_SU0213.045 (045)</ta>
            <ta e="T240" id="Seg_5289" s="T237">PKZ_196X_SU0213.046 (046)</ta>
            <ta e="T241" id="Seg_5290" s="T240">PKZ_196X_SU0213.047 (047)</ta>
            <ta e="T249" id="Seg_5291" s="T242">PKZ_196X_SU0213.048 (048)</ta>
            <ta e="T254" id="Seg_5292" s="T250">PKZ_196X_SU0213.049 (049)</ta>
            <ta e="T261" id="Seg_5293" s="T255">PKZ_196X_SU0213.050 (050)</ta>
            <ta e="T266" id="Seg_5294" s="T261">PKZ_196X_SU0213.051 (051)</ta>
            <ta e="T273" id="Seg_5295" s="T267">PKZ_196X_SU0213.052 (052)</ta>
            <ta e="T281" id="Seg_5296" s="T274">PKZ_196X_SU0213.053 (053)</ta>
            <ta e="T284" id="Seg_5297" s="T282">PKZ_196X_SU0213.054 (054)</ta>
            <ta e="T291" id="Seg_5298" s="T285">PKZ_196X_SU0213.055 (055)</ta>
            <ta e="T297" id="Seg_5299" s="T292">PKZ_196X_SU0213.056 (056)</ta>
            <ta e="T302" id="Seg_5300" s="T298">PKZ_196X_SU0213.057 (057)</ta>
            <ta e="T309" id="Seg_5301" s="T302">PKZ_196X_SU0213.058 (058)</ta>
            <ta e="T316" id="Seg_5302" s="T310">PKZ_196X_SU0213.059 (059)</ta>
            <ta e="T322" id="Seg_5303" s="T316">PKZ_196X_SU0213.060 (060)</ta>
            <ta e="T329" id="Seg_5304" s="T323">PKZ_196X_SU0213.061 (061)</ta>
            <ta e="T339" id="Seg_5305" s="T330">PKZ_196X_SU0213.062 (062)</ta>
            <ta e="T343" id="Seg_5306" s="T340">PKZ_196X_SU0213.063 (063)</ta>
            <ta e="T346" id="Seg_5307" s="T343">PKZ_196X_SU0213.064 (064)</ta>
            <ta e="T354" id="Seg_5308" s="T347">PKZ_196X_SU0213.065 (065)</ta>
            <ta e="T359" id="Seg_5309" s="T355">PKZ_196X_SU0213.066 (066)</ta>
            <ta e="T362" id="Seg_5310" s="T360">PKZ_196X_SU0213.067 (067)</ta>
            <ta e="T366" id="Seg_5311" s="T363">PKZ_196X_SU0213.068 (068)</ta>
            <ta e="T372" id="Seg_5312" s="T366">PKZ_196X_SU0213.069 (069)</ta>
            <ta e="T377" id="Seg_5313" s="T373">PKZ_196X_SU0213.070 (070)</ta>
            <ta e="T380" id="Seg_5314" s="T378">PKZ_196X_SU0213.071 (071)</ta>
            <ta e="T383" id="Seg_5315" s="T381">PKZ_196X_SU0213.072 (072)</ta>
            <ta e="T389" id="Seg_5316" s="T384">PKZ_196X_SU0213.073 (073)</ta>
            <ta e="T392" id="Seg_5317" s="T389">PKZ_196X_SU0213.074 (074)</ta>
            <ta e="T394" id="Seg_5318" s="T393">PKZ_196X_SU0213.075 (075)</ta>
            <ta e="T400" id="Seg_5319" s="T395">PKZ_196X_SU0213.076 (076)</ta>
            <ta e="T406" id="Seg_5320" s="T401">PKZ_196X_SU0213.077 (077)</ta>
            <ta e="T411" id="Seg_5321" s="T407">PKZ_196X_SU0213.078 (078)</ta>
            <ta e="T415" id="Seg_5322" s="T412">PKZ_196X_SU0213.079 (079)</ta>
            <ta e="T418" id="Seg_5323" s="T416">PKZ_196X_SU0213.080 (080)</ta>
            <ta e="T424" id="Seg_5324" s="T418">PKZ_196X_SU0213.081 (081)</ta>
            <ta e="T427" id="Seg_5325" s="T424">PKZ_196X_SU0213.082 (082)</ta>
            <ta e="T428" id="Seg_5326" s="T427">PKZ_196X_SU0213.083 (083)</ta>
            <ta e="T431" id="Seg_5327" s="T429">PKZ_196X_SU0213.084 (084)</ta>
            <ta e="T436" id="Seg_5328" s="T432">PKZ_196X_SU0213.085 (085)</ta>
            <ta e="T438" id="Seg_5329" s="T436">PKZ_196X_SU0213.086 (086)</ta>
            <ta e="T445" id="Seg_5330" s="T438">PKZ_196X_SU0213.087 (087)</ta>
            <ta e="T447" id="Seg_5331" s="T446">PKZ_196X_SU0213.088 (088)</ta>
            <ta e="T453" id="Seg_5332" s="T447">PKZ_196X_SU0213.089 (089)</ta>
            <ta e="T458" id="Seg_5333" s="T454">PKZ_196X_SU0213.090 (090)</ta>
            <ta e="T470" id="Seg_5334" s="T459">PKZ_196X_SU0213.091 (091)</ta>
            <ta e="T474" id="Seg_5335" s="T471">PKZ_196X_SU0213.092 (092)</ta>
            <ta e="T477" id="Seg_5336" s="T475">PKZ_196X_SU0213.093 (093)</ta>
            <ta e="T484" id="Seg_5337" s="T478">PKZ_196X_SU0213.094 (094)</ta>
            <ta e="T487" id="Seg_5338" s="T485">PKZ_196X_SU0213.095 (095)</ta>
            <ta e="T491" id="Seg_5339" s="T488">PKZ_196X_SU0213.096 (096)</ta>
            <ta e="T496" id="Seg_5340" s="T492">PKZ_196X_SU0213.097 (097)</ta>
            <ta e="T501" id="Seg_5341" s="T497">PKZ_196X_SU0213.098 (098)</ta>
            <ta e="T504" id="Seg_5342" s="T502">PKZ_196X_SU0213.099 (099)</ta>
            <ta e="T510" id="Seg_5343" s="T505">PKZ_196X_SU0213.100 (100)</ta>
            <ta e="T515" id="Seg_5344" s="T511">PKZ_196X_SU0213.101 (101)</ta>
            <ta e="T518" id="Seg_5345" s="T516">PKZ_196X_SU0213.102 (102)</ta>
            <ta e="T524" id="Seg_5346" s="T519">PKZ_196X_SU0213.103 (103)</ta>
            <ta e="T527" id="Seg_5347" s="T525">PKZ_196X_SU0213.104 (104)</ta>
            <ta e="T530" id="Seg_5348" s="T528">PKZ_196X_SU0213.105 (105)</ta>
            <ta e="T532" id="Seg_5349" s="T530">PKZ_196X_SU0213.106 (106)</ta>
            <ta e="T536" id="Seg_5350" s="T533">PKZ_196X_SU0213.107 (107)</ta>
            <ta e="T543" id="Seg_5351" s="T537">PKZ_196X_SU0213.108 (108)</ta>
            <ta e="T549" id="Seg_5352" s="T544">PKZ_196X_SU0213.109 (109)</ta>
            <ta e="T555" id="Seg_5353" s="T550">PKZ_196X_SU0213.110 (110)</ta>
            <ta e="T559" id="Seg_5354" s="T556">PKZ_196X_SU0213.111 (111)</ta>
            <ta e="T561" id="Seg_5355" s="T559">PKZ_196X_SU0213.112 (112)</ta>
            <ta e="T566" id="Seg_5356" s="T562">PKZ_196X_SU0213.113 (113)</ta>
            <ta e="T570" id="Seg_5357" s="T566">PKZ_196X_SU0213.114 (114)</ta>
            <ta e="T574" id="Seg_5358" s="T571">PKZ_196X_SU0213.115 (115)</ta>
            <ta e="T577" id="Seg_5359" s="T575">PKZ_196X_SU0213.116 (116)</ta>
            <ta e="T581" id="Seg_5360" s="T578">PKZ_196X_SU0213.117 (117)</ta>
            <ta e="T586" id="Seg_5361" s="T582">PKZ_196X_SU0213.118 (118)</ta>
            <ta e="T591" id="Seg_5362" s="T587">PKZ_196X_SU0213.119 (119)</ta>
            <ta e="T597" id="Seg_5363" s="T592">PKZ_196X_SU0213.120 (120)</ta>
            <ta e="T603" id="Seg_5364" s="T598">PKZ_196X_SU0213.121 (121)</ta>
            <ta e="T609" id="Seg_5365" s="T603">PKZ_196X_SU0213.122 (122)</ta>
            <ta e="T615" id="Seg_5366" s="T610">PKZ_196X_SU0213.123 (123)</ta>
            <ta e="T619" id="Seg_5367" s="T616">PKZ_196X_SU0213.124 (124)</ta>
            <ta e="T622" id="Seg_5368" s="T619">PKZ_196X_SU0213.125 (125)</ta>
            <ta e="T626" id="Seg_5369" s="T623">PKZ_196X_SU0213.126 (126)</ta>
            <ta e="T631" id="Seg_5370" s="T627">PKZ_196X_SU0213.127 (127)</ta>
            <ta e="T636" id="Seg_5371" s="T632">PKZ_196X_SU0213.128 (128)</ta>
            <ta e="T641" id="Seg_5372" s="T637">PKZ_196X_SU0213.129 (129)</ta>
            <ta e="T645" id="Seg_5373" s="T642">PKZ_196X_SU0213.130 (130)</ta>
            <ta e="T653" id="Seg_5374" s="T646">PKZ_196X_SU0213.131 (131)</ta>
            <ta e="T656" id="Seg_5375" s="T654">PKZ_196X_SU0213.132 (132)</ta>
            <ta e="T661" id="Seg_5376" s="T657">PKZ_196X_SU0213.133 (133)</ta>
            <ta e="T665" id="Seg_5377" s="T662">PKZ_196X_SU0213.134 (134)</ta>
            <ta e="T670" id="Seg_5378" s="T666">PKZ_196X_SU0213.135 (135)</ta>
            <ta e="T673" id="Seg_5379" s="T671">PKZ_196X_SU0213.136 (136)</ta>
            <ta e="T676" id="Seg_5380" s="T674">PKZ_196X_SU0213.137 (137)</ta>
            <ta e="T681" id="Seg_5381" s="T676">PKZ_196X_SU0213.138 (138)</ta>
            <ta e="T687" id="Seg_5382" s="T682">PKZ_196X_SU0213.139 (139)</ta>
            <ta e="T691" id="Seg_5383" s="T688">PKZ_196X_SU0213.140 (140)</ta>
            <ta e="T704" id="Seg_5384" s="T691">PKZ_196X_SU0213.141 (141)</ta>
            <ta e="T709" id="Seg_5385" s="T705">PKZ_196X_SU0213.142 (142)</ta>
            <ta e="T715" id="Seg_5386" s="T710">PKZ_196X_SU0213.143 (143)</ta>
            <ta e="T720" id="Seg_5387" s="T716">PKZ_196X_SU0213.144 (144)</ta>
            <ta e="T723" id="Seg_5388" s="T721">PKZ_196X_SU0213.145 (145)</ta>
            <ta e="T731" id="Seg_5389" s="T724">PKZ_196X_SU0213.146 (146)</ta>
            <ta e="T737" id="Seg_5390" s="T732">PKZ_196X_SU0213.147 (147)</ta>
            <ta e="T747" id="Seg_5391" s="T737">PKZ_196X_SU0213.148 (148)</ta>
            <ta e="T753" id="Seg_5392" s="T747">PKZ_196X_SU0213.149 (149)</ta>
            <ta e="T758" id="Seg_5393" s="T754">PKZ_196X_SU0213.150 (150)</ta>
            <ta e="T764" id="Seg_5394" s="T759">PKZ_196X_SU0213.151 (151)</ta>
            <ta e="T766" id="Seg_5395" s="T764">PKZ_196X_SU0213.152 (152)</ta>
            <ta e="T768" id="Seg_5396" s="T767">PKZ_196X_SU0213.153 (153)</ta>
            <ta e="T770" id="Seg_5397" s="T768">PKZ_196X_SU0213.154 (154)</ta>
            <ta e="T779" id="Seg_5398" s="T771">PKZ_196X_SU0213.155 (155)</ta>
            <ta e="T782" id="Seg_5399" s="T780">PKZ_196X_SU0213.156 (156)</ta>
            <ta e="T784" id="Seg_5400" s="T783">PKZ_196X_SU0213.157 (157)</ta>
            <ta e="T787" id="Seg_5401" s="T785">PKZ_196X_SU0213.158 (158)</ta>
            <ta e="T796" id="Seg_5402" s="T788">PKZ_196X_SU0213.159 (159)</ta>
            <ta e="T800" id="Seg_5403" s="T797">PKZ_196X_SU0213.160 (160)</ta>
            <ta e="T802" id="Seg_5404" s="T801">PKZ_196X_SU0213.161 (161)</ta>
            <ta e="T806" id="Seg_5405" s="T803">PKZ_196X_SU0213.162 (162)</ta>
            <ta e="T808" id="Seg_5406" s="T807">PKZ_196X_SU0213.163 (163)</ta>
            <ta e="T812" id="Seg_5407" s="T809">PKZ_196X_SU0213.164 (164)</ta>
            <ta e="T818" id="Seg_5408" s="T813">PKZ_196X_SU0213.165 (165)</ta>
            <ta e="T820" id="Seg_5409" s="T818">PKZ_196X_SU0213.166 (166)</ta>
            <ta e="T827" id="Seg_5410" s="T820">PKZ_196X_SU0213.167 (167)</ta>
            <ta e="T832" id="Seg_5411" s="T828">PKZ_196X_SU0213.168 (168)</ta>
            <ta e="T839" id="Seg_5412" s="T833">PKZ_196X_SU0213.169 (169)</ta>
            <ta e="T844" id="Seg_5413" s="T840">PKZ_196X_SU0213.170 (170)</ta>
            <ta e="T847" id="Seg_5414" s="T845">PKZ_196X_SU0213.171 (171)</ta>
            <ta e="T849" id="Seg_5415" s="T848">PKZ_196X_SU0213.172 (172)</ta>
            <ta e="T854" id="Seg_5416" s="T850">PKZ_196X_SU0213.173 (173)</ta>
            <ta e="T862" id="Seg_5417" s="T854">PKZ_196X_SU0213.174 (174)</ta>
            <ta e="T865" id="Seg_5418" s="T863">PKZ_196X_SU0213.175 (175)</ta>
            <ta e="T869" id="Seg_5419" s="T865">PKZ_196X_SU0213.176 (176)</ta>
            <ta e="T874" id="Seg_5420" s="T870">PKZ_196X_SU0213.177 (177)</ta>
            <ta e="T882" id="Seg_5421" s="T875">PKZ_196X_SU0213.178 (178)</ta>
            <ta e="T886" id="Seg_5422" s="T883">PKZ_196X_SU0213.179 (179)</ta>
            <ta e="T894" id="Seg_5423" s="T887">PKZ_196X_SU0213.180 (180)</ta>
            <ta e="T903" id="Seg_5424" s="T895">PKZ_196X_SU0213.181 (181)</ta>
            <ta e="T913" id="Seg_5425" s="T904">PKZ_196X_SU0213.182 (182)</ta>
            <ta e="T919" id="Seg_5426" s="T913">PKZ_196X_SU0213.183 (183)</ta>
            <ta e="T925" id="Seg_5427" s="T920">PKZ_196X_SU0213.184 (184)</ta>
            <ta e="T929" id="Seg_5428" s="T926">PKZ_196X_SU0213.185 (185)</ta>
            <ta e="T937" id="Seg_5429" s="T929">PKZ_196X_SU0213.186 (186)</ta>
            <ta e="T944" id="Seg_5430" s="T938">PKZ_196X_SU0213.187 (187)</ta>
            <ta e="T951" id="Seg_5431" s="T945">PKZ_196X_SU0213.188 (188)</ta>
            <ta e="T956" id="Seg_5432" s="T952">PKZ_196X_SU0213.189 (189)</ta>
            <ta e="T959" id="Seg_5433" s="T956">PKZ_196X_SU0213.190 (190)</ta>
            <ta e="T963" id="Seg_5434" s="T960">PKZ_196X_SU0213.191 (191)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_5435" s="T1">Dĭ šiʔ aparat (taʔla). </ta>
            <ta e="T8" id="Seg_5436" s="T6">Tararluʔpi bar. </ta>
            <ta e="T11" id="Seg_5437" s="T9">(Начал) šaːmzittə. </ta>
            <ta e="T15" id="Seg_5438" s="T12">Ugaːndə ɨrɨ molaːmbi. </ta>
            <ta e="T23" id="Seg_5439" s="T16">Tăn ugaːndə (kuguštu) kuza, tănan măllial, măllial. </ta>
            <ta e="T30" id="Seg_5440" s="T23">(A tăn=) A tăn ĭmbidə ej tĭmniel. </ta>
            <ta e="T36" id="Seg_5441" s="T31">Măn tănan mălliam: üdʼüge ibiem. </ta>
            <ta e="T40" id="Seg_5442" s="T37">Girgitdə il šobiʔi. </ta>
            <ta e="T46" id="Seg_5443" s="T41">То ли экспедиция, ej tĭmniem. </ta>
            <ta e="T50" id="Seg_5444" s="T46">Üdʼüge ibiem, ej surarbiam. </ta>
            <ta e="T54" id="Seg_5445" s="T51">Măna ej nörbəbiʔi. </ta>
            <ta e="T59" id="Seg_5446" s="T55">(Vlaskən) (Иван=) Иван Андреевич. </ta>
            <ta e="T69" id="Seg_5447" s="T60">Dĭgəttə dĭzen ibi бинокль i ĭmbi, tože ej tĭmniem. </ta>
            <ta e="T72" id="Seg_5448" s="T69">Măna pʼerbiʔi nʼimi. </ta>
            <ta e="T75" id="Seg_5449" s="T73">Müʔluʔpiʔi stʼenanə. </ta>
            <ta e="T78" id="Seg_5450" s="T76">Kuŋgəŋ dăre. </ta>
            <ta e="T82" id="Seg_5451" s="T79">Sažendə šide bʼeʔ. </ta>
            <ta e="T91" id="Seg_5452" s="T83">Dĭgəttə măn măndərbiam (m-) ugaːndə (nʼim-) nʼimi urgo. </ta>
            <ta e="T97" id="Seg_5453" s="T92">A dĭgəttə parluʔpiʔi, drugoj kănʼessiʔ. </ta>
            <ta e="T102" id="Seg_5454" s="T98">Măndəbiom: ugaːndə kuŋgəŋ molaːmbi. </ta>
            <ta e="T106" id="Seg_5455" s="T103">Dĭgəttə sazən embiʔi. </ta>
            <ta e="T112" id="Seg_5456" s="T107">I nuldəbiʔi dĭ binokl kujanə. </ta>
            <ta e="T116" id="Seg_5457" s="T113">I sazən neniluʔpi. </ta>
            <ta e="T120" id="Seg_5458" s="T117">Šiʔ kamən kalləlaʔ? </ta>
            <ta e="T123" id="Seg_5459" s="T121">(Păredʼan) kalləbaʔ. </ta>
            <ta e="T126" id="Seg_5460" s="T124">No, kaŋgaʔ! </ta>
            <ta e="T131" id="Seg_5461" s="T127">Privet nörbəgeʔ (Mat-) Matvejendə. </ta>
            <ta e="T137" id="Seg_5462" s="T132">I Jelʼa teinen kalla dʼürbi Malinovkanə. </ta>
            <ta e="T140" id="Seg_5463" s="T138">Sazən kundubi. </ta>
            <ta e="T145" id="Seg_5464" s="T141">Prʼedsʼedatelʼdə nada pečatʼʔi enzittə. </ta>
            <ta e="T150" id="Seg_5465" s="T146">Dĭ kambi dĭbər mašinaʔizʼiʔ. </ta>
            <ta e="T156" id="Seg_5466" s="T151">A (dĭʔnə-) dĭʔə parləj üdʼiŋ. </ta>
            <ta e="T165" id="Seg_5467" s="T157">Măn sʼestranə nʼit kalla dʼürbi (nodujla) tüžöjʔi (sʼinədə) deʔsʼittə. </ta>
            <ta e="T169" id="Seg_5468" s="T166">Bostə nʼit ibi. </ta>
            <ta e="T174" id="Seg_5469" s="T170">Büžü (parluʔi-) parluʔjə maːʔndə. </ta>
            <ta e="T181" id="Seg_5470" s="T175">Onʼiʔ kuza tenölüʔpi maʔ (asi-) azittə. </ta>
            <ta e="T183" id="Seg_5471" s="T182">Zolătăʔizʼiʔ. </ta>
            <ta e="T190" id="Seg_5472" s="T184">A baška kuza maʔ azittə sʼerʼebrogəʔ. </ta>
            <ta e="T200" id="Seg_5473" s="T190">A nagur kuza bar (žəzə) măndə: măn paʔizʼi jaʔləm tura. </ta>
            <ta e="T208" id="Seg_5474" s="T201">A teʔtə kuza: măn alam noʔə sălomaʔizʼi. </ta>
            <ta e="T213" id="Seg_5475" s="T209">(Su) šoləj bar togonordə. </ta>
            <ta e="T220" id="Seg_5476" s="T214">Nendləj (sĭjdən-) šindin togonordə ej (nendlən). </ta>
            <ta e="T225" id="Seg_5477" s="T221">Dĭ dĭʔnə jakšə moləj. </ta>
            <ta e="T231" id="Seg_5478" s="T226">A šindin togonordə (nen-) nendluʔləj. </ta>
            <ta e="T236" id="Seg_5479" s="T232">Dĭʔnə ej jakšə moləj. </ta>
            <ta e="T240" id="Seg_5480" s="T237">Bostə ej nendlil. </ta>
            <ta e="T241" id="Seg_5481" s="T240">Nuʔməluʔləj. </ta>
            <ta e="T249" id="Seg_5482" s="T242">(A ĭmbi=) (aʔ-) A ĭmbi togonərli, nendluʔpi. </ta>
            <ta e="T254" id="Seg_5483" s="T250">(Amnobiʔi) Nagur šoškaʔi amnobiʔi. </ta>
            <ta e="T261" id="Seg_5484" s="T255">Dĭgəttə šide šoška ugaːndə ɨrɨ ibiʔi. </ta>
            <ta e="T266" id="Seg_5485" s="T261">A onʼiʔ šoška üge togonərbi. </ta>
            <ta e="T273" id="Seg_5486" s="T267">Dĭzen bar jaʔtarlaʔ mĭmbiʔi, ara bĭʔpiʔi. </ta>
            <ta e="T281" id="Seg_5487" s="T274">A dö, onʼiʔ šoška bar tĭlbi dʼü. </ta>
            <ta e="T284" id="Seg_5488" s="T282">Piʔi oʔbdobiʔi. </ta>
            <ta e="T291" id="Seg_5489" s="T285">I maʔ boskəndə abi (pi-) pizeŋdə. </ta>
            <ta e="T297" id="Seg_5490" s="T292">(Dĭ-) Dĭzeŋ šobiʔi, onʼiʔ embi. </ta>
            <ta e="T302" id="Seg_5491" s="T298">Muʔ oʔbdəbi, da embi. </ta>
            <ta e="T309" id="Seg_5492" s="T302">A onʼiʔ sălomaʔizʼiʔ (a-) embi boskəndə maʔ. </ta>
            <ta e="T316" id="Seg_5493" s="T310">A dĭ üge pim piʔzʼi endleʔbə. </ta>
            <ta e="T322" id="Seg_5494" s="T316">(Dĭ=) Dĭzeŋ šobiʔi: "Ĭmbi tăn togonərlial? </ta>
            <ta e="T329" id="Seg_5495" s="T323">Miʔ büžün (a-) abibaʔ (maʔbi-) maʔdə". </ta>
            <ta e="T339" id="Seg_5496" s="T330">"A (der) šindin jakšə maʔdə moləj, măn ilʼi šiʔ?" </ta>
            <ta e="T343" id="Seg_5497" s="T340">Dĭgəttə šĭšəge molaːmbi. </ta>
            <ta e="T346" id="Seg_5498" s="T343">(Sĭre=) Sĭre šonəga. </ta>
            <ta e="T354" id="Seg_5499" s="T347">Dĭgəttə dĭzeŋ (pă-) păʔpiʔi turanə, girgit săloma. </ta>
            <ta e="T359" id="Seg_5500" s="T355">Kajbiʔi aj, šobi urgaːba. </ta>
            <ta e="T362" id="Seg_5501" s="T360">"Öʔləʔgeʔ măna!" </ta>
            <ta e="T366" id="Seg_5502" s="T363">Dĭzeŋ ej öʔliʔi. </ta>
            <ta e="T372" id="Seg_5503" s="T366">"Tüjö bar băldəlam šiʔ (turagəʔ=) turanə!" </ta>
            <ta e="T377" id="Seg_5504" s="T373">Dĭgəttə băldəbi, dĭzeŋ nuʔməluʔpiʔi. </ta>
            <ta e="T380" id="Seg_5505" s="T378">Girgit tura. </ta>
            <ta e="T383" id="Seg_5506" s="T381">Muzaŋdə abiʔi. </ta>
            <ta e="T389" id="Seg_5507" s="T384">I dĭbər aj (karl-) kajluʔpiʔi. </ta>
            <ta e="T392" id="Seg_5508" s="T389">Dĭ dĭbər šobi. </ta>
            <ta e="T394" id="Seg_5509" s="T393">"Kargaʔ! </ta>
            <ta e="T400" id="Seg_5510" s="T395">A to tüjö bar saʔməluʔləj tura". </ta>
            <ta e="T406" id="Seg_5511" s="T401">Dĭgəttə bar baruʔpi, dĭzeŋ nuʔməluʔpiʔi. </ta>
            <ta e="T411" id="Seg_5512" s="T407">I šobiʔi döbər onʼiʔdə. </ta>
            <ta e="T415" id="Seg_5513" s="T412">Nagurdə šoškandə šobiʔi. </ta>
            <ta e="T418" id="Seg_5514" s="T416">"Öʔleʔ miʔnʼibeʔ!" </ta>
            <ta e="T424" id="Seg_5515" s="T418">Dĭ öʔlübi i ajbə (karb-) kajbi. </ta>
            <ta e="T427" id="Seg_5516" s="T424">Dĭ šobi, urgaːba. </ta>
            <ta e="T428" id="Seg_5517" s="T427">"Öʔleʔ!" </ta>
            <ta e="T431" id="Seg_5518" s="T429">"Ej öʔləm!" </ta>
            <ta e="T436" id="Seg_5519" s="T432">Dĭgəttə dĭ băldəsʼtə xatʼel. </ta>
            <ta e="T438" id="Seg_5520" s="T436">Ej mobi. </ta>
            <ta e="T445" id="Seg_5521" s="T438">Dĭgəttə (sʼa-) sʼabi dĭbər (nu-) nʼuʔdə trubandə. </ta>
            <ta e="T447" id="Seg_5522" s="T446">Paʔlaːndəga. </ta>
            <ta e="T453" id="Seg_5523" s="T447">A dĭ aspaʔ nuldəbi tʼibige büzʼiʔ. </ta>
            <ta e="T458" id="Seg_5524" s="T454">Dĭ bar saʔməluʔpi aspaʔdə. </ta>
            <ta e="T470" id="Seg_5525" s="T459">I (bĭ-) dĭgəttə paruʔpi i kalla dʼürbi i daška kaməndə ej šobi. </ta>
            <ta e="T474" id="Seg_5526" s="T471">(Nendəb-) Nendəbiʔi dĭm. </ta>
            <ta e="T477" id="Seg_5527" s="T475">I kalla dʼürbi. </ta>
            <ta e="T484" id="Seg_5528" s="T478">((DMG)) Kumen (mi-) miʔ plʼonkaʔi (dʼăbaktərbiam) dʼăbaktərbibaʔ? </ta>
            <ta e="T487" id="Seg_5529" s="T485">Bʼeʔ nagur. </ta>
            <ta e="T491" id="Seg_5530" s="T488">Šiʔ ipekleʔ ige. </ta>
            <ta e="T496" id="Seg_5531" s="T492">Ige, a gijen ibileʔ? </ta>
            <ta e="T501" id="Seg_5532" s="T497">Da onʼiʔ ne deppi. </ta>
            <ta e="T504" id="Seg_5533" s="T502">(Vasʼ-) Vaznʼesenkaʔizʼi. </ta>
            <ta e="T510" id="Seg_5534" s="T505">Onʼiʔ poʔton sumna esseŋdə ibiʔi. </ta>
            <ta e="T515" id="Seg_5535" s="T511">Dĭ dĭzem maʔnən maːbi. </ta>
            <ta e="T518" id="Seg_5536" s="T516">Ajm kajbi. </ta>
            <ta e="T524" id="Seg_5537" s="T519">"Šindimdə (iʔ öʔləʔ-) iʔ (öʔləʔleʔ)!" </ta>
            <ta e="T527" id="Seg_5538" s="T525">Dĭgəttə kalla dʼürbi. </ta>
            <ta e="T530" id="Seg_5539" s="T528">Urgaːba šobi. </ta>
            <ta e="T532" id="Seg_5540" s="T530">"Kargat aj!" </ta>
            <ta e="T536" id="Seg_5541" s="T533">Dĭzeŋ ej karlaʔi. </ta>
            <ta e="T543" id="Seg_5542" s="T537">"Miʔnʼibeʔ iam mămbi, šindimdə ej öʔləʔsittə". </ta>
            <ta e="T549" id="Seg_5543" s="T544">Dĭgəttə urgaːba kalla dʼürbi, iat šobi. </ta>
            <ta e="T555" id="Seg_5544" s="T550">"Kargaʔ aj, măn deʔpiem süt! </ta>
            <ta e="T559" id="Seg_5545" s="T556">Deʔpiem noʔ šiʔnʼileʔ! </ta>
            <ta e="T561" id="Seg_5546" s="T559">Tvorog deʔpiem". </ta>
            <ta e="T566" id="Seg_5547" s="T562">Dĭgəttə dĭzeŋ karbiʔi, ambiʔi. </ta>
            <ta e="T570" id="Seg_5548" s="T566">Dĭgəttə iat bazoʔ kambi. </ta>
            <ta e="T574" id="Seg_5549" s="T571">"Iʔ öʔləʔgeʔ šindimdə!" </ta>
            <ta e="T577" id="Seg_5550" s="T575">Bazoʔ kalla dʼürbi. </ta>
            <ta e="T581" id="Seg_5551" s="T578">"Šindimdə iʔ öʔləʔ!" </ta>
            <ta e="T586" id="Seg_5552" s="T582">Dĭgəttə bazoʔ šobi urgaːba. </ta>
            <ta e="T591" id="Seg_5553" s="T587">"Esseŋ, esseŋ, öʔleʔ măna!" </ta>
            <ta e="T597" id="Seg_5554" s="T592">"Dʼok, (mɨ-) miʔ ej (öʔleʔ)". </ta>
            <ta e="T603" id="Seg_5555" s="T598">"A măn šiʔ ia igem!" </ta>
            <ta e="T609" id="Seg_5556" s="T603">"Dʼok, măn (ia-) ianə golosdə todam! </ta>
            <ta e="T615" id="Seg_5557" s="T610">A tăn golosdə ugaːndə nʼešpək". </ta>
            <ta e="T619" id="Seg_5558" s="T616">Dĭgəttə urgaːba kalla dʼürbi. </ta>
            <ta e="T622" id="Seg_5559" s="T619">Dĭgəttə iat šobi. </ta>
            <ta e="T626" id="Seg_5560" s="T623">Dĭzeŋ karbiʔi, bădəbi. </ta>
            <ta e="T631" id="Seg_5561" s="T627">"Tüj šindinədə ej öʔləʔ!" </ta>
            <ta e="T636" id="Seg_5562" s="T632">A urgaːba kambi kuznʼitsanə. </ta>
            <ta e="T641" id="Seg_5563" s="T637">Šĭkebə (pi-) abi todam. </ta>
            <ta e="T645" id="Seg_5564" s="T642">Dĭgəttə šobi bazoʔ. </ta>
            <ta e="T653" id="Seg_5565" s="T646">Dĭ mălia: "Măn šiʔ ial, deʔpiem süt. </ta>
            <ta e="T656" id="Seg_5566" s="T654">Ipek deʔpiem. </ta>
            <ta e="T661" id="Seg_5567" s="T657">Deʔpiem šiʔnʼileʔ творог, noʔ". </ta>
            <ta e="T665" id="Seg_5568" s="T662">Dĭzeŋ bar karluʔpi. </ta>
            <ta e="T670" id="Seg_5569" s="T666">Dĭ dĭzem bar amnuʔpi. </ta>
            <ta e="T673" id="Seg_5570" s="T671">Iat šobi. </ta>
            <ta e="T676" id="Seg_5571" s="T674">Esseŋdə naga. </ta>
            <ta e="T681" id="Seg_5572" s="T676">Onʼiʔ, самый (üdʼü-) üdʼüge šaʔlaːmbi. </ta>
            <ta e="T687" id="Seg_5573" s="T682">Dĭgəttə măndə: "Urgaːba bar amnuʔpi. </ta>
            <ta e="T691" id="Seg_5574" s="T688">Măn unnʼa maːluʔpiam. </ta>
            <ta e="T704" id="Seg_5575" s="T691">(Pʼešdə ša- sabi- š- sʼa- š-) Pʼešdə păʔlumbiam, dĭ măna ej măndərbi, kalla dʼürbi. </ta>
            <ta e="T709" id="Seg_5576" s="T705">Dĭgəttə urgaːba šobi poʔtonə. </ta>
            <ta e="T715" id="Seg_5577" s="T710">Poʔto măndə:"( "(Kan- kal-) Kanžəbəj. </ta>
            <ta e="T720" id="Seg_5578" s="T716">Suʔmibəj dö jamagə (penzəj). </ta>
            <ta e="T723" id="Seg_5579" s="T721">Poʔto suʔməbi. </ta>
            <ta e="T731" id="Seg_5580" s="T724">(A-) A dĭ ej mobi suʔmistə dĭbər. </ta>
            <ta e="T737" id="Seg_5581" s="T732">Kuzurluʔpi, ĭmbinəndə bar nanə напорол. </ta>
            <ta e="T747" id="Seg_5582" s="T737">Dĭgəttə dĭn (kozl-) kăzlʼataʔji (s-) sumiʔluʔpiʔi i bazoʔ iandə kalla dʼürbiʔi. </ta>
            <ta e="T753" id="Seg_5583" s="T747">((BRK)) "Kanžəbəj gibər-nʼibudʼ tănzi (jaʔt-) jaʔtarla". </ta>
            <ta e="T758" id="Seg_5584" s="T754">"A ĭmbi dĭn moləj?" </ta>
            <ta e="T764" id="Seg_5585" s="T759">"Da ĭmbi moləj, ara bĭtləbeʔ. </ta>
            <ta e="T766" id="Seg_5586" s="T764">Nüjnə nüjləbeʔ. </ta>
            <ta e="T768" id="Seg_5587" s="T767">Suʔmiləbeʔ. </ta>
            <ta e="T770" id="Seg_5588" s="T768">Garmonnʼiazi sʼarzbibeʔ. </ta>
            <ta e="T779" id="Seg_5589" s="T771">Dĭn bar ĭmbi iʔgö — uja iʔgö, (seŋ), kajaʔ. </ta>
            <ta e="T782" id="Seg_5590" s="T780">Kola mĭnzerona. </ta>
            <ta e="T784" id="Seg_5591" s="T783">Ipek. </ta>
            <ta e="T787" id="Seg_5592" s="T785">Sĭreʔpne nuga. </ta>
            <ta e="T796" id="Seg_5593" s="T788">Lem keʔbdeʔi ige, kobo keʔbdeʔi, sagər keʔbdeʔi, kanžəbəj. </ta>
            <ta e="T800" id="Seg_5594" s="T797">Tăn turaʔi (kürbil)? </ta>
            <ta e="T802" id="Seg_5595" s="T801">(Kürbiel). </ta>
            <ta e="T806" id="Seg_5596" s="T803">Kumən tura kürbiel? </ta>
            <ta e="T808" id="Seg_5597" s="T807">Sejʔpü. </ta>
            <ta e="T812" id="Seg_5598" s="T809">(Tăn-nu), turazaŋdə kürbiel? </ta>
            <ta e="T818" id="Seg_5599" s="T813">Tăn (d-) dʼijenə ej mumbiel? </ta>
            <ta e="T820" id="Seg_5600" s="T818">Ej mĭmbiem. </ta>
            <ta e="T827" id="Seg_5601" s="T820">Măn pimniem, dĭn bar urgaːba amno ((…)). </ta>
            <ta e="T832" id="Seg_5602" s="T828">Ĭmbi pimniel, kanaʔ, (ты)! </ta>
            <ta e="T839" id="Seg_5603" s="T833">Ĭmbidə (ej aləj=) ej aləj tănan. </ta>
            <ta e="T844" id="Seg_5604" s="T840">(Sʼanəluʔlʼil), dĭgəttə maʔnəl (šoləl). </ta>
            <ta e="T847" id="Seg_5605" s="T845">(Leizittə) možna. </ta>
            <ta e="T849" id="Seg_5606" s="T848">(…говорю). </ta>
            <ta e="T854" id="Seg_5607" s="T850">"Vsʼo ravno măn pimniem. </ta>
            <ta e="T862" id="Seg_5608" s="T854">Măn ĭmbidə naga, (multuk=) multuk naga, tagajbə naga. </ta>
            <ta e="T865" id="Seg_5609" s="T863">Tolʼkă pi". </ta>
            <ta e="T869" id="Seg_5610" s="T865">"A dĭ ej (ĭzəmnəj). </ta>
            <ta e="T874" id="Seg_5611" s="T870">Dĭ kuzagəʔ pimnie, nuʔməluʔləj". </ta>
            <ta e="T882" id="Seg_5612" s="T875">Miʔ kambibaʔ (ten- te-) dĭ (dʼijegə) dʼijenə. </ta>
            <ta e="T886" id="Seg_5613" s="T883">Dĭgəttə sĭre kanluʔpi. </ta>
            <ta e="T894" id="Seg_5614" s="T887">Dĭzeŋ nagurgö kambiʔi, a măn parluʔpiam piʔtə. </ta>
            <ta e="T903" id="Seg_5615" s="T895">Dĭgəttə nünniem: šindidə bar (muzaŋ=) muzaŋdə bar bădlaʔpə. </ta>
            <ta e="T913" id="Seg_5616" s="T904">(N-) Tenniem: urgaːba (š- kan- š-) bar ((…)) (măldlaʔpə). </ta>
            <ta e="T919" id="Seg_5617" s="T913">A kuzittə ej kubiam, maʔnʼi šobiam. </ta>
            <ta e="T925" id="Seg_5618" s="T920">Măn šonəgam i nereluʔpiem bar. </ta>
            <ta e="T929" id="Seg_5619" s="T926">Paʔinə nada sʼazittə. </ta>
            <ta e="T937" id="Seg_5620" s="T929">A măn dĭʔnə mămbiam: "Dĭ tože paʔinə sʼalia". </ta>
            <ta e="T944" id="Seg_5621" s="T938">I pa baldlia, i dʼünə (barʔdlia). </ta>
            <ta e="T951" id="Seg_5622" s="T945">Kadəʔ dĭn dʼăbaktərzittə, esseŋ bar kirgarlaʔbə. </ta>
            <ta e="T956" id="Seg_5623" s="T952">Šoškaʔi tože bar kirgarlaʔbəʔjə. </ta>
            <ta e="T959" id="Seg_5624" s="T956">(Puzo) tože mürerlaʔbə. </ta>
            <ta e="T963" id="Seg_5625" s="T960">Kădedə nʼelʼzʼa dʼăbaktərzittə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_5626" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_5627" s="T2">šiʔ</ta>
            <ta e="T4" id="Seg_5628" s="T3">aparat</ta>
            <ta e="T5" id="Seg_5629" s="T4">taʔla</ta>
            <ta e="T7" id="Seg_5630" s="T6">tarar-luʔ-pi</ta>
            <ta e="T8" id="Seg_5631" s="T7">bar</ta>
            <ta e="T11" id="Seg_5632" s="T10">šaːm-zittə</ta>
            <ta e="T13" id="Seg_5633" s="T12">ugaːndə</ta>
            <ta e="T14" id="Seg_5634" s="T13">ɨrɨ</ta>
            <ta e="T15" id="Seg_5635" s="T14">mo-laːm-bi</ta>
            <ta e="T17" id="Seg_5636" s="T16">tăn</ta>
            <ta e="T18" id="Seg_5637" s="T17">ugaːndə</ta>
            <ta e="T19" id="Seg_5638" s="T18">kuguštu</ta>
            <ta e="T20" id="Seg_5639" s="T19">kuza</ta>
            <ta e="T21" id="Seg_5640" s="T20">tănan</ta>
            <ta e="T22" id="Seg_5641" s="T21">măl-lia-l</ta>
            <ta e="T23" id="Seg_5642" s="T22">măl-lia-l</ta>
            <ta e="T24" id="Seg_5643" s="T23">a</ta>
            <ta e="T25" id="Seg_5644" s="T24">tăn</ta>
            <ta e="T26" id="Seg_5645" s="T25">a</ta>
            <ta e="T27" id="Seg_5646" s="T26">tăn</ta>
            <ta e="T28" id="Seg_5647" s="T27">ĭmbi=də</ta>
            <ta e="T29" id="Seg_5648" s="T28">ej</ta>
            <ta e="T30" id="Seg_5649" s="T29">tĭm-nie-l</ta>
            <ta e="T32" id="Seg_5650" s="T31">măn</ta>
            <ta e="T33" id="Seg_5651" s="T32">tănan</ta>
            <ta e="T34" id="Seg_5652" s="T33">măl-lia-m</ta>
            <ta e="T35" id="Seg_5653" s="T34">üdʼüge</ta>
            <ta e="T36" id="Seg_5654" s="T35">i-bie-m</ta>
            <ta e="T38" id="Seg_5655" s="T37">girgit=də</ta>
            <ta e="T39" id="Seg_5656" s="T38">il</ta>
            <ta e="T40" id="Seg_5657" s="T39">šo-bi-ʔi</ta>
            <ta e="T45" id="Seg_5658" s="T44">ej</ta>
            <ta e="T46" id="Seg_5659" s="T45">tĭm-nie-m</ta>
            <ta e="T47" id="Seg_5660" s="T46">üdʼüge</ta>
            <ta e="T48" id="Seg_5661" s="T47">i-bie-m</ta>
            <ta e="T49" id="Seg_5662" s="T48">ej</ta>
            <ta e="T50" id="Seg_5663" s="T49">surar-bia-m</ta>
            <ta e="T52" id="Seg_5664" s="T51">măna</ta>
            <ta e="T53" id="Seg_5665" s="T52">ej</ta>
            <ta e="T54" id="Seg_5666" s="T53">nörbə-bi-ʔi</ta>
            <ta e="T56" id="Seg_5667" s="T55">Vlas-kən</ta>
            <ta e="T61" id="Seg_5668" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_5669" s="T61">dĭ-zen</ta>
            <ta e="T63" id="Seg_5670" s="T62">i-bi</ta>
            <ta e="T65" id="Seg_5671" s="T64">i</ta>
            <ta e="T66" id="Seg_5672" s="T65">ĭmbi</ta>
            <ta e="T67" id="Seg_5673" s="T66">tože</ta>
            <ta e="T68" id="Seg_5674" s="T67">ej</ta>
            <ta e="T69" id="Seg_5675" s="T68">tĭm-nie-m</ta>
            <ta e="T70" id="Seg_5676" s="T69">măna</ta>
            <ta e="T71" id="Seg_5677" s="T70">pʼer-bi-ʔi</ta>
            <ta e="T72" id="Seg_5678" s="T71">nʼimi</ta>
            <ta e="T74" id="Seg_5679" s="T73">müʔ-luʔ-pi-ʔi</ta>
            <ta e="T75" id="Seg_5680" s="T74">stʼena-nə</ta>
            <ta e="T77" id="Seg_5681" s="T76">kuŋgə-ŋ</ta>
            <ta e="T78" id="Seg_5682" s="T77">dăre</ta>
            <ta e="T80" id="Seg_5683" s="T79">sažen-də</ta>
            <ta e="T81" id="Seg_5684" s="T80">šide</ta>
            <ta e="T82" id="Seg_5685" s="T81">bʼeʔ</ta>
            <ta e="T84" id="Seg_5686" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_5687" s="T84">măn</ta>
            <ta e="T86" id="Seg_5688" s="T85">măndə-r-bia-m</ta>
            <ta e="T88" id="Seg_5689" s="T87">ugaːndə</ta>
            <ta e="T90" id="Seg_5690" s="T89">nʼimi</ta>
            <ta e="T91" id="Seg_5691" s="T90">urgo</ta>
            <ta e="T93" id="Seg_5692" s="T92">a</ta>
            <ta e="T94" id="Seg_5693" s="T93">dĭgəttə</ta>
            <ta e="T95" id="Seg_5694" s="T94">par-luʔ-pi-ʔi</ta>
            <ta e="T96" id="Seg_5695" s="T95">drugoj</ta>
            <ta e="T97" id="Seg_5696" s="T96">kănʼes-siʔ</ta>
            <ta e="T99" id="Seg_5697" s="T98">măndə-bio-m</ta>
            <ta e="T100" id="Seg_5698" s="T99">ugaːndə</ta>
            <ta e="T101" id="Seg_5699" s="T100">kuŋgə-ŋ</ta>
            <ta e="T102" id="Seg_5700" s="T101">mo-laːm-bi</ta>
            <ta e="T104" id="Seg_5701" s="T103">dĭgəttə</ta>
            <ta e="T105" id="Seg_5702" s="T104">sazən</ta>
            <ta e="T106" id="Seg_5703" s="T105">em-bi-ʔi</ta>
            <ta e="T108" id="Seg_5704" s="T107">i</ta>
            <ta e="T109" id="Seg_5705" s="T108">nuldə-bi-ʔi</ta>
            <ta e="T110" id="Seg_5706" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_5707" s="T110">binoklʼ</ta>
            <ta e="T112" id="Seg_5708" s="T111">kuja-nə</ta>
            <ta e="T114" id="Seg_5709" s="T113">i</ta>
            <ta e="T115" id="Seg_5710" s="T114">sazən</ta>
            <ta e="T116" id="Seg_5711" s="T115">neni-luʔ-pi</ta>
            <ta e="T118" id="Seg_5712" s="T117">šiʔ</ta>
            <ta e="T119" id="Seg_5713" s="T118">kamən</ta>
            <ta e="T120" id="Seg_5714" s="T119">kal-lə-laʔ</ta>
            <ta e="T122" id="Seg_5715" s="T121">păredʼa-n</ta>
            <ta e="T123" id="Seg_5716" s="T122">kal-lə-baʔ</ta>
            <ta e="T125" id="Seg_5717" s="T124">no</ta>
            <ta e="T126" id="Seg_5718" s="T125">kaŋ-gaʔ</ta>
            <ta e="T128" id="Seg_5719" s="T127">privet</ta>
            <ta e="T129" id="Seg_5720" s="T128">nörbə-geʔ</ta>
            <ta e="T131" id="Seg_5721" s="T130">Matveje-ndə</ta>
            <ta e="T133" id="Seg_5722" s="T132">i</ta>
            <ta e="T134" id="Seg_5723" s="T133">Jelʼa</ta>
            <ta e="T135" id="Seg_5724" s="T134">teinen</ta>
            <ta e="T964" id="Seg_5725" s="T135">kal-la</ta>
            <ta e="T136" id="Seg_5726" s="T964">dʼür-bi</ta>
            <ta e="T137" id="Seg_5727" s="T136">Malinovka-nə</ta>
            <ta e="T139" id="Seg_5728" s="T138">sazən</ta>
            <ta e="T140" id="Seg_5729" s="T139">kun-du-bi</ta>
            <ta e="T142" id="Seg_5730" s="T141">prʼedsʼedatelʼ-də</ta>
            <ta e="T143" id="Seg_5731" s="T142">nada</ta>
            <ta e="T144" id="Seg_5732" s="T143">pečatʼ-ʔi</ta>
            <ta e="T145" id="Seg_5733" s="T144">en-zittə</ta>
            <ta e="T147" id="Seg_5734" s="T146">dĭ</ta>
            <ta e="T148" id="Seg_5735" s="T147">kam-bi</ta>
            <ta e="T149" id="Seg_5736" s="T148">dĭbər</ta>
            <ta e="T150" id="Seg_5737" s="T149">mašina-ʔi-zʼiʔ</ta>
            <ta e="T152" id="Seg_5738" s="T151">a</ta>
            <ta e="T154" id="Seg_5739" s="T153">dĭʔə</ta>
            <ta e="T155" id="Seg_5740" s="T154">par-lə-j</ta>
            <ta e="T156" id="Seg_5741" s="T155">üdʼi-ŋ</ta>
            <ta e="T158" id="Seg_5742" s="T157">măn</ta>
            <ta e="T159" id="Seg_5743" s="T158">sʼestra-nə</ta>
            <ta e="T160" id="Seg_5744" s="T159">nʼi-t</ta>
            <ta e="T965" id="Seg_5745" s="T160">kal-la</ta>
            <ta e="T161" id="Seg_5746" s="T965">dʼür-bi</ta>
            <ta e="T162" id="Seg_5747" s="T161">nod-u-j-la</ta>
            <ta e="T163" id="Seg_5748" s="T162">tüžöj-ʔi</ta>
            <ta e="T164" id="Seg_5749" s="T163">sʼinədə</ta>
            <ta e="T165" id="Seg_5750" s="T164">deʔ-sʼittə</ta>
            <ta e="T167" id="Seg_5751" s="T166">bos-tə</ta>
            <ta e="T168" id="Seg_5752" s="T167">nʼi-t</ta>
            <ta e="T169" id="Seg_5753" s="T168">i-bi</ta>
            <ta e="T171" id="Seg_5754" s="T170">büžü</ta>
            <ta e="T173" id="Seg_5755" s="T172">par-lu-ʔjə</ta>
            <ta e="T174" id="Seg_5756" s="T173">maːʔ-ndə</ta>
            <ta e="T176" id="Seg_5757" s="T175">onʼiʔ</ta>
            <ta e="T177" id="Seg_5758" s="T176">kuza</ta>
            <ta e="T178" id="Seg_5759" s="T177">tenö-lüʔ-pi</ta>
            <ta e="T179" id="Seg_5760" s="T178">maʔ</ta>
            <ta e="T181" id="Seg_5761" s="T180">a-zittə</ta>
            <ta e="T183" id="Seg_5762" s="T182">zolătă-ʔi-zʼiʔ</ta>
            <ta e="T185" id="Seg_5763" s="T184">a</ta>
            <ta e="T186" id="Seg_5764" s="T185">baška</ta>
            <ta e="T187" id="Seg_5765" s="T186">kuza</ta>
            <ta e="T188" id="Seg_5766" s="T187">maʔ</ta>
            <ta e="T189" id="Seg_5767" s="T188">a-zittə</ta>
            <ta e="T190" id="Seg_5768" s="T189">sʼerʼebro-gəʔ</ta>
            <ta e="T191" id="Seg_5769" s="T190">a</ta>
            <ta e="T192" id="Seg_5770" s="T191">nagur</ta>
            <ta e="T193" id="Seg_5771" s="T192">kuza</ta>
            <ta e="T194" id="Seg_5772" s="T193">bar</ta>
            <ta e="T195" id="Seg_5773" s="T194">žəzə</ta>
            <ta e="T196" id="Seg_5774" s="T195">măn-də</ta>
            <ta e="T197" id="Seg_5775" s="T196">măn</ta>
            <ta e="T198" id="Seg_5776" s="T197">pa-ʔi-zʼi</ta>
            <ta e="T199" id="Seg_5777" s="T198">jaʔ-lə-m</ta>
            <ta e="T200" id="Seg_5778" s="T199">tura</ta>
            <ta e="T202" id="Seg_5779" s="T201">a</ta>
            <ta e="T203" id="Seg_5780" s="T202">teʔtə</ta>
            <ta e="T204" id="Seg_5781" s="T203">kuza</ta>
            <ta e="T205" id="Seg_5782" s="T204">măn</ta>
            <ta e="T206" id="Seg_5783" s="T205">a-la-m</ta>
            <ta e="T207" id="Seg_5784" s="T206">no-ʔə</ta>
            <ta e="T208" id="Seg_5785" s="T207">săloma-ʔi-zʼi</ta>
            <ta e="T211" id="Seg_5786" s="T210">šo-lə-j</ta>
            <ta e="T212" id="Seg_5787" s="T211">bar</ta>
            <ta e="T213" id="Seg_5788" s="T212">togonor-də</ta>
            <ta e="T215" id="Seg_5789" s="T214">nend-lə-j</ta>
            <ta e="T217" id="Seg_5790" s="T216">šindi-n</ta>
            <ta e="T218" id="Seg_5791" s="T217">togonor-də</ta>
            <ta e="T219" id="Seg_5792" s="T218">ej</ta>
            <ta e="T220" id="Seg_5793" s="T219">nend-lə-n</ta>
            <ta e="T222" id="Seg_5794" s="T221">dĭ</ta>
            <ta e="T223" id="Seg_5795" s="T222">dĭʔ-nə</ta>
            <ta e="T224" id="Seg_5796" s="T223">jakšə</ta>
            <ta e="T225" id="Seg_5797" s="T224">mo-lə-j</ta>
            <ta e="T227" id="Seg_5798" s="T226">a</ta>
            <ta e="T228" id="Seg_5799" s="T227">šindi-n</ta>
            <ta e="T229" id="Seg_5800" s="T228">togonor-də</ta>
            <ta e="T231" id="Seg_5801" s="T230">nend-luʔ-lə-j</ta>
            <ta e="T233" id="Seg_5802" s="T232">dĭʔ-nə</ta>
            <ta e="T234" id="Seg_5803" s="T233">ej</ta>
            <ta e="T235" id="Seg_5804" s="T234">jakšə</ta>
            <ta e="T236" id="Seg_5805" s="T235">mo-lə-j</ta>
            <ta e="T238" id="Seg_5806" s="T237">bos-tə</ta>
            <ta e="T239" id="Seg_5807" s="T238">ej</ta>
            <ta e="T240" id="Seg_5808" s="T239">nend-li-l</ta>
            <ta e="T241" id="Seg_5809" s="T240">nuʔmə-luʔ-lə-j</ta>
            <ta e="T243" id="Seg_5810" s="T242">a</ta>
            <ta e="T244" id="Seg_5811" s="T243">ĭmbi</ta>
            <ta e="T246" id="Seg_5812" s="T245">a</ta>
            <ta e="T247" id="Seg_5813" s="T246">ĭmbi</ta>
            <ta e="T248" id="Seg_5814" s="T247">togonər-li</ta>
            <ta e="T249" id="Seg_5815" s="T248">nend-luʔ-pi</ta>
            <ta e="T251" id="Seg_5816" s="T250">amno-bi-ʔi</ta>
            <ta e="T252" id="Seg_5817" s="T251">nagur</ta>
            <ta e="T253" id="Seg_5818" s="T252">šoška-ʔi</ta>
            <ta e="T254" id="Seg_5819" s="T253">amno-bi-ʔi</ta>
            <ta e="T256" id="Seg_5820" s="T255">dĭgəttə</ta>
            <ta e="T257" id="Seg_5821" s="T256">šide</ta>
            <ta e="T258" id="Seg_5822" s="T257">šoška</ta>
            <ta e="T259" id="Seg_5823" s="T258">ugaːndə</ta>
            <ta e="T260" id="Seg_5824" s="T259">ɨrɨ</ta>
            <ta e="T261" id="Seg_5825" s="T260">i-bi-ʔi</ta>
            <ta e="T262" id="Seg_5826" s="T261">a</ta>
            <ta e="T263" id="Seg_5827" s="T262">onʼiʔ</ta>
            <ta e="T264" id="Seg_5828" s="T263">šoška</ta>
            <ta e="T265" id="Seg_5829" s="T264">üge</ta>
            <ta e="T266" id="Seg_5830" s="T265">togonər-bi</ta>
            <ta e="T268" id="Seg_5831" s="T267">dĭ-zen</ta>
            <ta e="T269" id="Seg_5832" s="T268">bar</ta>
            <ta e="T270" id="Seg_5833" s="T269">jaʔtar-laʔ</ta>
            <ta e="T271" id="Seg_5834" s="T270">mĭm-bi-ʔi</ta>
            <ta e="T272" id="Seg_5835" s="T271">ara</ta>
            <ta e="T273" id="Seg_5836" s="T272">bĭʔ-pi-ʔi</ta>
            <ta e="T275" id="Seg_5837" s="T274">a</ta>
            <ta e="T276" id="Seg_5838" s="T275">dö</ta>
            <ta e="T277" id="Seg_5839" s="T276">onʼiʔ</ta>
            <ta e="T278" id="Seg_5840" s="T277">šoška</ta>
            <ta e="T279" id="Seg_5841" s="T278">bar</ta>
            <ta e="T280" id="Seg_5842" s="T279">tĭl-bi</ta>
            <ta e="T281" id="Seg_5843" s="T280">dʼü</ta>
            <ta e="T283" id="Seg_5844" s="T282">pi-ʔi</ta>
            <ta e="T284" id="Seg_5845" s="T283">oʔbdo-bi-ʔi</ta>
            <ta e="T286" id="Seg_5846" s="T285">i</ta>
            <ta e="T287" id="Seg_5847" s="T286">maʔ</ta>
            <ta e="T288" id="Seg_5848" s="T287">bos-kəndə</ta>
            <ta e="T289" id="Seg_5849" s="T288">a-bi</ta>
            <ta e="T291" id="Seg_5850" s="T290">pi-zeŋ-də</ta>
            <ta e="T294" id="Seg_5851" s="T293">dĭ-zeŋ</ta>
            <ta e="T295" id="Seg_5852" s="T294">šo-bi-ʔi</ta>
            <ta e="T296" id="Seg_5853" s="T295">onʼiʔ</ta>
            <ta e="T297" id="Seg_5854" s="T296">em-bi</ta>
            <ta e="T299" id="Seg_5855" s="T298">mu-ʔ</ta>
            <ta e="T300" id="Seg_5856" s="T299">oʔbdə-bi</ta>
            <ta e="T301" id="Seg_5857" s="T300">da</ta>
            <ta e="T302" id="Seg_5858" s="T301">em-bi</ta>
            <ta e="T303" id="Seg_5859" s="T302">a</ta>
            <ta e="T304" id="Seg_5860" s="T303">onʼiʔ</ta>
            <ta e="T305" id="Seg_5861" s="T304">săloma-ʔi-zʼiʔ</ta>
            <ta e="T306" id="Seg_5862" s="T305">a-</ta>
            <ta e="T307" id="Seg_5863" s="T306">em-bi</ta>
            <ta e="T308" id="Seg_5864" s="T307">bos-kəndə</ta>
            <ta e="T309" id="Seg_5865" s="T308">maʔ</ta>
            <ta e="T311" id="Seg_5866" s="T310">a</ta>
            <ta e="T312" id="Seg_5867" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_5868" s="T312">üge</ta>
            <ta e="T314" id="Seg_5869" s="T313">pi-m</ta>
            <ta e="T315" id="Seg_5870" s="T314">pi-ʔ-zʼi</ta>
            <ta e="T316" id="Seg_5871" s="T315">end-leʔbə</ta>
            <ta e="T317" id="Seg_5872" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_5873" s="T317">dĭ-zeŋ</ta>
            <ta e="T319" id="Seg_5874" s="T318">šo-bi-ʔi</ta>
            <ta e="T320" id="Seg_5875" s="T319">ĭmbi</ta>
            <ta e="T321" id="Seg_5876" s="T320">tăn</ta>
            <ta e="T322" id="Seg_5877" s="T321">togonər-lia-l</ta>
            <ta e="T324" id="Seg_5878" s="T323">miʔ</ta>
            <ta e="T325" id="Seg_5879" s="T324">büžü-n</ta>
            <ta e="T326" id="Seg_5880" s="T325">a-</ta>
            <ta e="T327" id="Seg_5881" s="T326">a-bi-baʔ</ta>
            <ta e="T329" id="Seg_5882" s="T328">maʔ-də</ta>
            <ta e="T331" id="Seg_5883" s="T330">a</ta>
            <ta e="T333" id="Seg_5884" s="T332">šindi-n</ta>
            <ta e="T334" id="Seg_5885" s="T333">jakšə</ta>
            <ta e="T335" id="Seg_5886" s="T334">maʔ-də</ta>
            <ta e="T336" id="Seg_5887" s="T335">mo-lə-j</ta>
            <ta e="T337" id="Seg_5888" s="T336">măn</ta>
            <ta e="T338" id="Seg_5889" s="T337">ilʼi</ta>
            <ta e="T339" id="Seg_5890" s="T338">šiʔ</ta>
            <ta e="T341" id="Seg_5891" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_5892" s="T341">šĭšəge</ta>
            <ta e="T343" id="Seg_5893" s="T342">mo-laːm-bi</ta>
            <ta e="T344" id="Seg_5894" s="T343">sĭre</ta>
            <ta e="T345" id="Seg_5895" s="T344">sĭre</ta>
            <ta e="T346" id="Seg_5896" s="T345">šonə-ga</ta>
            <ta e="T348" id="Seg_5897" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_5898" s="T348">dĭ-zeŋ</ta>
            <ta e="T351" id="Seg_5899" s="T350">păʔ-pi-ʔi</ta>
            <ta e="T352" id="Seg_5900" s="T351">tura-nə</ta>
            <ta e="T353" id="Seg_5901" s="T352">girgit</ta>
            <ta e="T354" id="Seg_5902" s="T353">săloma</ta>
            <ta e="T356" id="Seg_5903" s="T355">kaj-bi-ʔi</ta>
            <ta e="T357" id="Seg_5904" s="T356">aj</ta>
            <ta e="T358" id="Seg_5905" s="T357">šo-bi</ta>
            <ta e="T359" id="Seg_5906" s="T358">urgaːba</ta>
            <ta e="T361" id="Seg_5907" s="T360">öʔ-ləʔ-geʔ</ta>
            <ta e="T362" id="Seg_5908" s="T361">măna</ta>
            <ta e="T364" id="Seg_5909" s="T363">dĭ-zeŋ</ta>
            <ta e="T365" id="Seg_5910" s="T364">ej</ta>
            <ta e="T366" id="Seg_5911" s="T365">öʔ-li-ʔi</ta>
            <ta e="T367" id="Seg_5912" s="T366">tüjö</ta>
            <ta e="T368" id="Seg_5913" s="T367">bar</ta>
            <ta e="T369" id="Seg_5914" s="T368">băldə-la-m</ta>
            <ta e="T370" id="Seg_5915" s="T369">šiʔ</ta>
            <ta e="T371" id="Seg_5916" s="T370">tura-gəʔ</ta>
            <ta e="T372" id="Seg_5917" s="T371">tura-nə</ta>
            <ta e="T374" id="Seg_5918" s="T373">dĭgəttə</ta>
            <ta e="T375" id="Seg_5919" s="T374">băldə-bi</ta>
            <ta e="T376" id="Seg_5920" s="T375">dĭ-zeŋ</ta>
            <ta e="T377" id="Seg_5921" s="T376">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T379" id="Seg_5922" s="T378">girgit</ta>
            <ta e="T380" id="Seg_5923" s="T379">tura</ta>
            <ta e="T382" id="Seg_5924" s="T381">mu-zaŋ-də</ta>
            <ta e="T383" id="Seg_5925" s="T382">a-bi-ʔi</ta>
            <ta e="T385" id="Seg_5926" s="T384">i</ta>
            <ta e="T386" id="Seg_5927" s="T385">dĭbər</ta>
            <ta e="T387" id="Seg_5928" s="T386">aj</ta>
            <ta e="T389" id="Seg_5929" s="T388">kaj-luʔ-pi-ʔi</ta>
            <ta e="T390" id="Seg_5930" s="T389">dĭ</ta>
            <ta e="T391" id="Seg_5931" s="T390">dĭbər</ta>
            <ta e="T392" id="Seg_5932" s="T391">šo-bi</ta>
            <ta e="T394" id="Seg_5933" s="T393">kar-gaʔ</ta>
            <ta e="T396" id="Seg_5934" s="T395">ato</ta>
            <ta e="T397" id="Seg_5935" s="T396">tüjö</ta>
            <ta e="T398" id="Seg_5936" s="T397">bar</ta>
            <ta e="T399" id="Seg_5937" s="T398">saʔmə-luʔ-lə-j</ta>
            <ta e="T400" id="Seg_5938" s="T399">tura</ta>
            <ta e="T402" id="Seg_5939" s="T401">dĭgəttə</ta>
            <ta e="T403" id="Seg_5940" s="T402">bar</ta>
            <ta e="T404" id="Seg_5941" s="T403">baruʔ-pi</ta>
            <ta e="T405" id="Seg_5942" s="T404">dĭ-zeŋ</ta>
            <ta e="T406" id="Seg_5943" s="T405">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T408" id="Seg_5944" s="T407">i</ta>
            <ta e="T409" id="Seg_5945" s="T408">šo-bi-ʔi</ta>
            <ta e="T410" id="Seg_5946" s="T409">döbər</ta>
            <ta e="T411" id="Seg_5947" s="T410">onʼiʔ-də</ta>
            <ta e="T413" id="Seg_5948" s="T412">nagur-də</ta>
            <ta e="T414" id="Seg_5949" s="T413">šoška-ndə</ta>
            <ta e="T415" id="Seg_5950" s="T414">šo-bi-ʔi</ta>
            <ta e="T417" id="Seg_5951" s="T416">öʔ-le-ʔ</ta>
            <ta e="T418" id="Seg_5952" s="T417">miʔnʼibeʔ</ta>
            <ta e="T419" id="Seg_5953" s="T418">dĭ</ta>
            <ta e="T420" id="Seg_5954" s="T419">öʔ-lü-bi</ta>
            <ta e="T421" id="Seg_5955" s="T420">i</ta>
            <ta e="T422" id="Seg_5956" s="T421">aj-bə</ta>
            <ta e="T424" id="Seg_5957" s="T423">kaj-bi</ta>
            <ta e="T425" id="Seg_5958" s="T424">dĭ</ta>
            <ta e="T426" id="Seg_5959" s="T425">šo-bi</ta>
            <ta e="T427" id="Seg_5960" s="T426">urgaːba</ta>
            <ta e="T428" id="Seg_5961" s="T427">öʔ-le-ʔ</ta>
            <ta e="T430" id="Seg_5962" s="T429">ej</ta>
            <ta e="T431" id="Seg_5963" s="T430">öʔ-lə-m</ta>
            <ta e="T433" id="Seg_5964" s="T432">dĭgəttə</ta>
            <ta e="T434" id="Seg_5965" s="T433">dĭ</ta>
            <ta e="T435" id="Seg_5966" s="T434">băldə-sʼtə</ta>
            <ta e="T436" id="Seg_5967" s="T435">xatʼel</ta>
            <ta e="T437" id="Seg_5968" s="T436">ej</ta>
            <ta e="T438" id="Seg_5969" s="T437">mo-bi</ta>
            <ta e="T439" id="Seg_5970" s="T438">dĭgəttə</ta>
            <ta e="T441" id="Seg_5971" s="T440">sʼa-bi</ta>
            <ta e="T442" id="Seg_5972" s="T441">dĭbər</ta>
            <ta e="T444" id="Seg_5973" s="T443">nʼuʔdə</ta>
            <ta e="T445" id="Seg_5974" s="T444">truba-ndə</ta>
            <ta e="T447" id="Seg_5975" s="T446">paʔ-laːndə-ga</ta>
            <ta e="T448" id="Seg_5976" s="T447">a</ta>
            <ta e="T449" id="Seg_5977" s="T448">dĭ</ta>
            <ta e="T450" id="Seg_5978" s="T449">aspaʔ</ta>
            <ta e="T451" id="Seg_5979" s="T450">nuldə-bi</ta>
            <ta e="T452" id="Seg_5980" s="T451">tʼibige</ta>
            <ta e="T453" id="Seg_5981" s="T452">bü-zʼiʔ</ta>
            <ta e="T455" id="Seg_5982" s="T454">dĭ</ta>
            <ta e="T456" id="Seg_5983" s="T455">bar</ta>
            <ta e="T457" id="Seg_5984" s="T456">saʔmə-luʔ-pi</ta>
            <ta e="T458" id="Seg_5985" s="T457">aspaʔ-də</ta>
            <ta e="T460" id="Seg_5986" s="T459">i</ta>
            <ta e="T462" id="Seg_5987" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_5988" s="T462">par-uʔ-pi</ta>
            <ta e="T464" id="Seg_5989" s="T463">i</ta>
            <ta e="T966" id="Seg_5990" s="T464">kal-la</ta>
            <ta e="T465" id="Seg_5991" s="T966">dʼür-bi</ta>
            <ta e="T466" id="Seg_5992" s="T465">i</ta>
            <ta e="T467" id="Seg_5993" s="T466">daška</ta>
            <ta e="T468" id="Seg_5994" s="T467">kamən=də</ta>
            <ta e="T469" id="Seg_5995" s="T468">ej</ta>
            <ta e="T470" id="Seg_5996" s="T469">šo-bi</ta>
            <ta e="T473" id="Seg_5997" s="T472">nendə-bi-ʔi</ta>
            <ta e="T474" id="Seg_5998" s="T473">dĭ-m</ta>
            <ta e="T476" id="Seg_5999" s="T475">i</ta>
            <ta e="T967" id="Seg_6000" s="T476">kal-la</ta>
            <ta e="T477" id="Seg_6001" s="T967">dʼür-bi</ta>
            <ta e="T479" id="Seg_6002" s="T478">kumen</ta>
            <ta e="T481" id="Seg_6003" s="T480">miʔ</ta>
            <ta e="T482" id="Seg_6004" s="T481">plʼonka-ʔi</ta>
            <ta e="T483" id="Seg_6005" s="T482">dʼăbaktər-bia-m</ta>
            <ta e="T484" id="Seg_6006" s="T483">dʼăbaktər-bi-baʔ</ta>
            <ta e="T486" id="Seg_6007" s="T485">bʼeʔ</ta>
            <ta e="T487" id="Seg_6008" s="T486">nagur</ta>
            <ta e="T489" id="Seg_6009" s="T488">šiʔ</ta>
            <ta e="T490" id="Seg_6010" s="T489">ipek-leʔ</ta>
            <ta e="T491" id="Seg_6011" s="T490">i-ge</ta>
            <ta e="T493" id="Seg_6012" s="T492">i-ge</ta>
            <ta e="T494" id="Seg_6013" s="T493">a</ta>
            <ta e="T495" id="Seg_6014" s="T494">gijen</ta>
            <ta e="T496" id="Seg_6015" s="T495">i-bi-leʔ</ta>
            <ta e="T498" id="Seg_6016" s="T497">da</ta>
            <ta e="T499" id="Seg_6017" s="T498">onʼiʔ</ta>
            <ta e="T500" id="Seg_6018" s="T499">ne</ta>
            <ta e="T501" id="Seg_6019" s="T500">dep-pi</ta>
            <ta e="T504" id="Seg_6020" s="T503">Vaznʼesenka-ʔi-zʼi</ta>
            <ta e="T506" id="Seg_6021" s="T505">onʼiʔ</ta>
            <ta e="T507" id="Seg_6022" s="T506">poʔto-n</ta>
            <ta e="T508" id="Seg_6023" s="T507">sumna</ta>
            <ta e="T509" id="Seg_6024" s="T508">es-seŋ-də</ta>
            <ta e="T510" id="Seg_6025" s="T509">i-bi-ʔi</ta>
            <ta e="T512" id="Seg_6026" s="T511">dĭ</ta>
            <ta e="T513" id="Seg_6027" s="T512">dĭ-zem</ta>
            <ta e="T514" id="Seg_6028" s="T513">maʔ-nə-n</ta>
            <ta e="T515" id="Seg_6029" s="T514">ma-bi</ta>
            <ta e="T517" id="Seg_6030" s="T516">aj-m</ta>
            <ta e="T518" id="Seg_6031" s="T517">kaj-bi</ta>
            <ta e="T520" id="Seg_6032" s="T519">šindi-m=də</ta>
            <ta e="T521" id="Seg_6033" s="T520">i-ʔ</ta>
            <ta e="T523" id="Seg_6034" s="T522">i-ʔ</ta>
            <ta e="T524" id="Seg_6035" s="T523">öʔ-ləʔ-leʔ</ta>
            <ta e="T526" id="Seg_6036" s="T525">dĭgəttə</ta>
            <ta e="T968" id="Seg_6037" s="T526">kal-la</ta>
            <ta e="T527" id="Seg_6038" s="T968">dʼür-bi</ta>
            <ta e="T529" id="Seg_6039" s="T528">urgaːba</ta>
            <ta e="T530" id="Seg_6040" s="T529">šo-bi</ta>
            <ta e="T531" id="Seg_6041" s="T530">kar-gat</ta>
            <ta e="T532" id="Seg_6042" s="T531">aj</ta>
            <ta e="T534" id="Seg_6043" s="T533">dĭ-zeŋ</ta>
            <ta e="T535" id="Seg_6044" s="T534">ej</ta>
            <ta e="T536" id="Seg_6045" s="T535">kar-la-ʔi</ta>
            <ta e="T538" id="Seg_6046" s="T537">miʔnʼibeʔ</ta>
            <ta e="T539" id="Seg_6047" s="T538">ia-m</ta>
            <ta e="T540" id="Seg_6048" s="T539">măm-bi</ta>
            <ta e="T541" id="Seg_6049" s="T540">šindi-m=də</ta>
            <ta e="T542" id="Seg_6050" s="T541">ej</ta>
            <ta e="T543" id="Seg_6051" s="T542">öʔ-ləʔ-sittə</ta>
            <ta e="T545" id="Seg_6052" s="T544">dĭgəttə</ta>
            <ta e="T546" id="Seg_6053" s="T545">urgaːba</ta>
            <ta e="T969" id="Seg_6054" s="T546">kal-la</ta>
            <ta e="T547" id="Seg_6055" s="T969">dʼür-bi</ta>
            <ta e="T548" id="Seg_6056" s="T547">ia-t</ta>
            <ta e="T549" id="Seg_6057" s="T548">šo-bi</ta>
            <ta e="T551" id="Seg_6058" s="T550">kar-gaʔ</ta>
            <ta e="T552" id="Seg_6059" s="T551">aj</ta>
            <ta e="T553" id="Seg_6060" s="T552">măn</ta>
            <ta e="T554" id="Seg_6061" s="T553">deʔ-pie-m</ta>
            <ta e="T555" id="Seg_6062" s="T554">süt</ta>
            <ta e="T557" id="Seg_6063" s="T556">deʔ-pie-m</ta>
            <ta e="T558" id="Seg_6064" s="T557">noʔ</ta>
            <ta e="T559" id="Seg_6065" s="T558">šiʔnʼileʔ</ta>
            <ta e="T560" id="Seg_6066" s="T559">tvorog</ta>
            <ta e="T561" id="Seg_6067" s="T560">deʔ-pie-m</ta>
            <ta e="T563" id="Seg_6068" s="T562">dĭgəttə</ta>
            <ta e="T564" id="Seg_6069" s="T563">dĭ-zeŋ</ta>
            <ta e="T565" id="Seg_6070" s="T564">kar-bi-ʔi</ta>
            <ta e="T566" id="Seg_6071" s="T565">am-bi-ʔi</ta>
            <ta e="T567" id="Seg_6072" s="T566">dĭgəttə</ta>
            <ta e="T568" id="Seg_6073" s="T567">ia-t</ta>
            <ta e="T569" id="Seg_6074" s="T568">bazoʔ</ta>
            <ta e="T570" id="Seg_6075" s="T569">kam-bi</ta>
            <ta e="T572" id="Seg_6076" s="T571">i-ʔ</ta>
            <ta e="T573" id="Seg_6077" s="T572">öʔ-ləʔ-geʔ</ta>
            <ta e="T574" id="Seg_6078" s="T573">šindi-m=də</ta>
            <ta e="T576" id="Seg_6079" s="T575">bazoʔ</ta>
            <ta e="T970" id="Seg_6080" s="T576">kal-la</ta>
            <ta e="T577" id="Seg_6081" s="T970">dʼür-bi</ta>
            <ta e="T579" id="Seg_6082" s="T578">šindi-m=də</ta>
            <ta e="T580" id="Seg_6083" s="T579">i-ʔ</ta>
            <ta e="T581" id="Seg_6084" s="T580">öʔ-ləʔ</ta>
            <ta e="T583" id="Seg_6085" s="T582">dĭgəttə</ta>
            <ta e="T584" id="Seg_6086" s="T583">bazoʔ</ta>
            <ta e="T585" id="Seg_6087" s="T584">šo-bi</ta>
            <ta e="T586" id="Seg_6088" s="T585">urgaːba</ta>
            <ta e="T588" id="Seg_6089" s="T587">es-seŋ</ta>
            <ta e="T589" id="Seg_6090" s="T588">es-seŋ</ta>
            <ta e="T590" id="Seg_6091" s="T589">öʔ-leʔ</ta>
            <ta e="T591" id="Seg_6092" s="T590">măna</ta>
            <ta e="T593" id="Seg_6093" s="T592">dʼok</ta>
            <ta e="T595" id="Seg_6094" s="T594">miʔ</ta>
            <ta e="T596" id="Seg_6095" s="T595">ej</ta>
            <ta e="T597" id="Seg_6096" s="T596">öʔ-leʔ</ta>
            <ta e="T599" id="Seg_6097" s="T598">a</ta>
            <ta e="T600" id="Seg_6098" s="T599">măn</ta>
            <ta e="T601" id="Seg_6099" s="T600">šiʔ</ta>
            <ta e="T602" id="Seg_6100" s="T601">ia</ta>
            <ta e="T603" id="Seg_6101" s="T602">i-ge-m</ta>
            <ta e="T604" id="Seg_6102" s="T603">dʼok</ta>
            <ta e="T605" id="Seg_6103" s="T604">măn</ta>
            <ta e="T607" id="Seg_6104" s="T606">ia-nə</ta>
            <ta e="T608" id="Seg_6105" s="T607">golos-də</ta>
            <ta e="T609" id="Seg_6106" s="T608">todam</ta>
            <ta e="T611" id="Seg_6107" s="T610">a</ta>
            <ta e="T612" id="Seg_6108" s="T611">tăn</ta>
            <ta e="T613" id="Seg_6109" s="T612">golos-də</ta>
            <ta e="T614" id="Seg_6110" s="T613">ugaːndə</ta>
            <ta e="T615" id="Seg_6111" s="T614">nʼešpək</ta>
            <ta e="T617" id="Seg_6112" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_6113" s="T617">urgaːba</ta>
            <ta e="T971" id="Seg_6114" s="T618">kal-la</ta>
            <ta e="T619" id="Seg_6115" s="T971">dʼür-bi</ta>
            <ta e="T620" id="Seg_6116" s="T619">dĭgəttə</ta>
            <ta e="T621" id="Seg_6117" s="T620">ia-t</ta>
            <ta e="T622" id="Seg_6118" s="T621">šo-bi</ta>
            <ta e="T624" id="Seg_6119" s="T623">dĭ-zeŋ</ta>
            <ta e="T625" id="Seg_6120" s="T624">kar-bi-ʔi</ta>
            <ta e="T626" id="Seg_6121" s="T625">bădə-bi</ta>
            <ta e="T628" id="Seg_6122" s="T627">tüj</ta>
            <ta e="T629" id="Seg_6123" s="T628">šindi-nə=də</ta>
            <ta e="T630" id="Seg_6124" s="T629">ej</ta>
            <ta e="T631" id="Seg_6125" s="T630">öʔ-ləʔ</ta>
            <ta e="T633" id="Seg_6126" s="T632">a</ta>
            <ta e="T634" id="Seg_6127" s="T633">urgaːba</ta>
            <ta e="T635" id="Seg_6128" s="T634">kam-bi</ta>
            <ta e="T636" id="Seg_6129" s="T635">kuznʼitsa-nə</ta>
            <ta e="T638" id="Seg_6130" s="T637">šĭke-bə</ta>
            <ta e="T640" id="Seg_6131" s="T639">a-bi</ta>
            <ta e="T641" id="Seg_6132" s="T640">todam</ta>
            <ta e="T643" id="Seg_6133" s="T642">dĭgəttə</ta>
            <ta e="T644" id="Seg_6134" s="T643">šo-bi</ta>
            <ta e="T645" id="Seg_6135" s="T644">bazoʔ</ta>
            <ta e="T647" id="Seg_6136" s="T646">dĭ</ta>
            <ta e="T648" id="Seg_6137" s="T647">măn-lia</ta>
            <ta e="T649" id="Seg_6138" s="T648">măn</ta>
            <ta e="T650" id="Seg_6139" s="T649">šiʔ</ta>
            <ta e="T651" id="Seg_6140" s="T650">ia-l</ta>
            <ta e="T652" id="Seg_6141" s="T651">deʔ-pie-m</ta>
            <ta e="T653" id="Seg_6142" s="T652">süt</ta>
            <ta e="T655" id="Seg_6143" s="T654">ipek</ta>
            <ta e="T656" id="Seg_6144" s="T655">deʔ-pie-m</ta>
            <ta e="T658" id="Seg_6145" s="T657">deʔ-pie-m</ta>
            <ta e="T659" id="Seg_6146" s="T658">šiʔnʼileʔ</ta>
            <ta e="T661" id="Seg_6147" s="T660">noʔ</ta>
            <ta e="T663" id="Seg_6148" s="T662">dĭ-zeŋ</ta>
            <ta e="T664" id="Seg_6149" s="T663">bar</ta>
            <ta e="T665" id="Seg_6150" s="T664">kar-luʔ-pi</ta>
            <ta e="T667" id="Seg_6151" s="T666">dĭ</ta>
            <ta e="T668" id="Seg_6152" s="T667">dĭ-zem</ta>
            <ta e="T669" id="Seg_6153" s="T668">bar</ta>
            <ta e="T670" id="Seg_6154" s="T669">am-nuʔ-pi</ta>
            <ta e="T672" id="Seg_6155" s="T671">ia-t</ta>
            <ta e="T673" id="Seg_6156" s="T672">šo-bi</ta>
            <ta e="T675" id="Seg_6157" s="T674">es-seŋ-də</ta>
            <ta e="T676" id="Seg_6158" s="T675">naga</ta>
            <ta e="T677" id="Seg_6159" s="T676">onʼiʔ</ta>
            <ta e="T680" id="Seg_6160" s="T679">üdʼüge</ta>
            <ta e="T681" id="Seg_6161" s="T680">šaʔ-laːm-bi</ta>
            <ta e="T683" id="Seg_6162" s="T682">dĭgəttə</ta>
            <ta e="T684" id="Seg_6163" s="T683">măn-də</ta>
            <ta e="T685" id="Seg_6164" s="T684">urgaːba</ta>
            <ta e="T686" id="Seg_6165" s="T685">bar</ta>
            <ta e="T687" id="Seg_6166" s="T686">am-nuʔ-pi</ta>
            <ta e="T689" id="Seg_6167" s="T688">măn</ta>
            <ta e="T690" id="Seg_6168" s="T689">unnʼa</ta>
            <ta e="T691" id="Seg_6169" s="T690">maː-luʔ-pia-m</ta>
            <ta e="T692" id="Seg_6170" s="T691">pʼeš-də</ta>
            <ta e="T698" id="Seg_6171" s="T697">pʼeš-də</ta>
            <ta e="T699" id="Seg_6172" s="T698">păʔ-lum-bia-m</ta>
            <ta e="T700" id="Seg_6173" s="T699">dĭ</ta>
            <ta e="T701" id="Seg_6174" s="T700">măna</ta>
            <ta e="T702" id="Seg_6175" s="T701">ej</ta>
            <ta e="T703" id="Seg_6176" s="T702">măndə-r-bi</ta>
            <ta e="T972" id="Seg_6177" s="T703">kal-la</ta>
            <ta e="T704" id="Seg_6178" s="T972">dʼür-bi</ta>
            <ta e="T706" id="Seg_6179" s="T705">dĭgəttə</ta>
            <ta e="T707" id="Seg_6180" s="T706">urgaːba</ta>
            <ta e="T708" id="Seg_6181" s="T707">šo-bi</ta>
            <ta e="T709" id="Seg_6182" s="T708">poʔto-nə</ta>
            <ta e="T711" id="Seg_6183" s="T710">poʔto</ta>
            <ta e="T712" id="Seg_6184" s="T711">măn-də</ta>
            <ta e="T715" id="Seg_6185" s="T714">kan-žə-bəj</ta>
            <ta e="T717" id="Seg_6186" s="T716">suʔmi-bəj</ta>
            <ta e="T718" id="Seg_6187" s="T717">dö</ta>
            <ta e="T719" id="Seg_6188" s="T718">jama-gə</ta>
            <ta e="T720" id="Seg_6189" s="T719">penzəj</ta>
            <ta e="T722" id="Seg_6190" s="T721">poʔto</ta>
            <ta e="T723" id="Seg_6191" s="T722">suʔmə-bi</ta>
            <ta e="T725" id="Seg_6192" s="T724">a-</ta>
            <ta e="T726" id="Seg_6193" s="T725">a</ta>
            <ta e="T727" id="Seg_6194" s="T726">dĭ</ta>
            <ta e="T728" id="Seg_6195" s="T727">ej</ta>
            <ta e="T729" id="Seg_6196" s="T728">mo-bi</ta>
            <ta e="T730" id="Seg_6197" s="T729">suʔmi-stə</ta>
            <ta e="T731" id="Seg_6198" s="T730">dĭbər</ta>
            <ta e="T733" id="Seg_6199" s="T732">kuzur-luʔ-pi</ta>
            <ta e="T734" id="Seg_6200" s="T733">ĭmbi-nən-də</ta>
            <ta e="T735" id="Seg_6201" s="T734">bar</ta>
            <ta e="T736" id="Seg_6202" s="T735">nanə</ta>
            <ta e="T738" id="Seg_6203" s="T737">dĭgəttə</ta>
            <ta e="T739" id="Seg_6204" s="T738">dĭn</ta>
            <ta e="T741" id="Seg_6205" s="T740">kăzlʼata-ʔji</ta>
            <ta e="T743" id="Seg_6206" s="T742">sumi-ʔluʔpiʔi</ta>
            <ta e="T744" id="Seg_6207" s="T743">i</ta>
            <ta e="T745" id="Seg_6208" s="T744">bazoʔ</ta>
            <ta e="T746" id="Seg_6209" s="T745">ia-ndə</ta>
            <ta e="T973" id="Seg_6210" s="T746">kal-la</ta>
            <ta e="T747" id="Seg_6211" s="T973">dʼür-bi-ʔi</ta>
            <ta e="T749" id="Seg_6212" s="T748">kan-žə-bəj</ta>
            <ta e="T750" id="Seg_6213" s="T749">gibər=nʼibudʼ</ta>
            <ta e="T751" id="Seg_6214" s="T750">tăn-zi</ta>
            <ta e="T753" id="Seg_6215" s="T752">jaʔtar-la</ta>
            <ta e="T755" id="Seg_6216" s="T754">a</ta>
            <ta e="T756" id="Seg_6217" s="T755">ĭmbi</ta>
            <ta e="T757" id="Seg_6218" s="T756">dĭn</ta>
            <ta e="T758" id="Seg_6219" s="T757">mo-lə-j</ta>
            <ta e="T760" id="Seg_6220" s="T759">da</ta>
            <ta e="T761" id="Seg_6221" s="T760">ĭmbi</ta>
            <ta e="T762" id="Seg_6222" s="T761">mo-lə-j</ta>
            <ta e="T763" id="Seg_6223" s="T762">ara</ta>
            <ta e="T764" id="Seg_6224" s="T763">bĭt-lə-beʔ</ta>
            <ta e="T765" id="Seg_6225" s="T764">nüjnə</ta>
            <ta e="T766" id="Seg_6226" s="T765">nüj-lə-beʔ</ta>
            <ta e="T768" id="Seg_6227" s="T767">suʔmi-lə-beʔ</ta>
            <ta e="T769" id="Seg_6228" s="T768">garmonnʼia-zi</ta>
            <ta e="T770" id="Seg_6229" s="T769">sʼar-z-bi-beʔ</ta>
            <ta e="T772" id="Seg_6230" s="T771">dĭn</ta>
            <ta e="T773" id="Seg_6231" s="T772">bar</ta>
            <ta e="T774" id="Seg_6232" s="T773">ĭmbi</ta>
            <ta e="T775" id="Seg_6233" s="T774">iʔgö</ta>
            <ta e="T776" id="Seg_6234" s="T775">uja</ta>
            <ta e="T777" id="Seg_6235" s="T776">iʔgö</ta>
            <ta e="T778" id="Seg_6236" s="T777">seŋ</ta>
            <ta e="T779" id="Seg_6237" s="T778">kajaʔ</ta>
            <ta e="T781" id="Seg_6238" s="T780">kola</ta>
            <ta e="T782" id="Seg_6239" s="T781">mĭnzer-o-na</ta>
            <ta e="T784" id="Seg_6240" s="T783">ipek</ta>
            <ta e="T786" id="Seg_6241" s="T785">sĭreʔpne</ta>
            <ta e="T787" id="Seg_6242" s="T786">nu-ga</ta>
            <ta e="T789" id="Seg_6243" s="T788">lem</ta>
            <ta e="T790" id="Seg_6244" s="T789">keʔbde-ʔi</ta>
            <ta e="T791" id="Seg_6245" s="T790">i-ge</ta>
            <ta e="T792" id="Seg_6246" s="T791">kobo</ta>
            <ta e="T793" id="Seg_6247" s="T792">keʔbde-ʔi</ta>
            <ta e="T794" id="Seg_6248" s="T793">sagər</ta>
            <ta e="T795" id="Seg_6249" s="T794">keʔbde-ʔi</ta>
            <ta e="T796" id="Seg_6250" s="T795">kan-žə-bəj</ta>
            <ta e="T798" id="Seg_6251" s="T797">tăn</ta>
            <ta e="T799" id="Seg_6252" s="T798">tura-ʔi</ta>
            <ta e="T800" id="Seg_6253" s="T799">kür-bi-l</ta>
            <ta e="T802" id="Seg_6254" s="T801">kür-bie-l</ta>
            <ta e="T804" id="Seg_6255" s="T803">kumən</ta>
            <ta e="T805" id="Seg_6256" s="T804">tura</ta>
            <ta e="T806" id="Seg_6257" s="T805">kür-bie-l</ta>
            <ta e="T808" id="Seg_6258" s="T807">sejʔpü</ta>
            <ta e="T810" id="Seg_6259" s="T809">tănnu</ta>
            <ta e="T811" id="Seg_6260" s="T810">tura-zaŋ-də</ta>
            <ta e="T812" id="Seg_6261" s="T811">kür-bie-l</ta>
            <ta e="T814" id="Seg_6262" s="T813">tăn</ta>
            <ta e="T816" id="Seg_6263" s="T815">dʼije-nə</ta>
            <ta e="T817" id="Seg_6264" s="T816">ej</ta>
            <ta e="T818" id="Seg_6265" s="T817">mum-bie-l</ta>
            <ta e="T819" id="Seg_6266" s="T818">ej</ta>
            <ta e="T820" id="Seg_6267" s="T819">mĭm-bie-m</ta>
            <ta e="T821" id="Seg_6268" s="T820">măn</ta>
            <ta e="T822" id="Seg_6269" s="T821">pim-nie-m</ta>
            <ta e="T823" id="Seg_6270" s="T822">dĭn</ta>
            <ta e="T824" id="Seg_6271" s="T823">bar</ta>
            <ta e="T825" id="Seg_6272" s="T824">urgaːba</ta>
            <ta e="T826" id="Seg_6273" s="T825">amno</ta>
            <ta e="T829" id="Seg_6274" s="T828">ĭmbi</ta>
            <ta e="T830" id="Seg_6275" s="T829">pim-nie-l</ta>
            <ta e="T831" id="Seg_6276" s="T830">kan-a-ʔ</ta>
            <ta e="T834" id="Seg_6277" s="T833">ĭmbi=də</ta>
            <ta e="T835" id="Seg_6278" s="T834">ej</ta>
            <ta e="T836" id="Seg_6279" s="T835">a-lə-j</ta>
            <ta e="T837" id="Seg_6280" s="T836">ej</ta>
            <ta e="T838" id="Seg_6281" s="T837">a-lə-j</ta>
            <ta e="T839" id="Seg_6282" s="T838">tănan</ta>
            <ta e="T841" id="Seg_6283" s="T840">sʼan-ə-luʔ-lʼi-l</ta>
            <ta e="T842" id="Seg_6284" s="T841">dĭgəttə</ta>
            <ta e="T843" id="Seg_6285" s="T842">maʔ-nə-l</ta>
            <ta e="T844" id="Seg_6286" s="T843">šo-lə-l</ta>
            <ta e="T846" id="Seg_6287" s="T845">le-i-zittə</ta>
            <ta e="T847" id="Seg_6288" s="T846">možna</ta>
            <ta e="T852" id="Seg_6289" s="T850">vsʼoravno</ta>
            <ta e="T853" id="Seg_6290" s="T852">măn</ta>
            <ta e="T854" id="Seg_6291" s="T853">pim-nie-m</ta>
            <ta e="T855" id="Seg_6292" s="T854">măn</ta>
            <ta e="T856" id="Seg_6293" s="T855">ĭmbi=də</ta>
            <ta e="T857" id="Seg_6294" s="T856">naga</ta>
            <ta e="T858" id="Seg_6295" s="T857">multuk</ta>
            <ta e="T859" id="Seg_6296" s="T858">multuk</ta>
            <ta e="T860" id="Seg_6297" s="T859">naga</ta>
            <ta e="T861" id="Seg_6298" s="T860">tagaj-bə</ta>
            <ta e="T862" id="Seg_6299" s="T861">naga</ta>
            <ta e="T864" id="Seg_6300" s="T863">tolʼkă</ta>
            <ta e="T865" id="Seg_6301" s="T864">pi</ta>
            <ta e="T866" id="Seg_6302" s="T865">a</ta>
            <ta e="T867" id="Seg_6303" s="T866">dĭ</ta>
            <ta e="T868" id="Seg_6304" s="T867">ej</ta>
            <ta e="T869" id="Seg_6305" s="T868">ĭzəm-nə-j</ta>
            <ta e="T871" id="Seg_6306" s="T870">dĭ</ta>
            <ta e="T872" id="Seg_6307" s="T871">kuza-gəʔ</ta>
            <ta e="T873" id="Seg_6308" s="T872">pim-nie</ta>
            <ta e="T874" id="Seg_6309" s="T873">nuʔmə-luʔ-lə-j</ta>
            <ta e="T876" id="Seg_6310" s="T875">miʔ</ta>
            <ta e="T877" id="Seg_6311" s="T876">kam-bi-baʔ</ta>
            <ta e="T880" id="Seg_6312" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_6313" s="T880">dʼije-gə</ta>
            <ta e="T882" id="Seg_6314" s="T881">dʼije-nə</ta>
            <ta e="T884" id="Seg_6315" s="T883">dĭgəttə</ta>
            <ta e="T885" id="Seg_6316" s="T884">sĭre</ta>
            <ta e="T886" id="Seg_6317" s="T885">kan-luʔ-pi</ta>
            <ta e="T888" id="Seg_6318" s="T887">dĭ-zeŋ</ta>
            <ta e="T889" id="Seg_6319" s="T888">nagur-göʔ</ta>
            <ta e="T890" id="Seg_6320" s="T889">kam-bi-ʔi</ta>
            <ta e="T891" id="Seg_6321" s="T890">a</ta>
            <ta e="T892" id="Seg_6322" s="T891">măn</ta>
            <ta e="T893" id="Seg_6323" s="T892">par-luʔ-pia-m</ta>
            <ta e="T894" id="Seg_6324" s="T893">piʔtə</ta>
            <ta e="T896" id="Seg_6325" s="T895">dĭgəttə</ta>
            <ta e="T897" id="Seg_6326" s="T896">nün-nie-m</ta>
            <ta e="T898" id="Seg_6327" s="T897">šindi=də</ta>
            <ta e="T899" id="Seg_6328" s="T898">bar</ta>
            <ta e="T900" id="Seg_6329" s="T899">mu-zaŋ</ta>
            <ta e="T901" id="Seg_6330" s="T900">mu-zaŋ-də</ta>
            <ta e="T902" id="Seg_6331" s="T901">bar</ta>
            <ta e="T903" id="Seg_6332" s="T902">băd-laʔpə</ta>
            <ta e="T906" id="Seg_6333" s="T905">tennie-m</ta>
            <ta e="T907" id="Seg_6334" s="T906">urgaːba</ta>
            <ta e="T909" id="Seg_6335" s="T908">kan-</ta>
            <ta e="T911" id="Seg_6336" s="T910">bar</ta>
            <ta e="T913" id="Seg_6337" s="T912">măldlaʔpə</ta>
            <ta e="T914" id="Seg_6338" s="T913">a</ta>
            <ta e="T915" id="Seg_6339" s="T914">ku-zittə</ta>
            <ta e="T916" id="Seg_6340" s="T915">ej</ta>
            <ta e="T917" id="Seg_6341" s="T916">ku-bia-m</ta>
            <ta e="T918" id="Seg_6342" s="T917">maʔ-nʼi</ta>
            <ta e="T919" id="Seg_6343" s="T918">šo-bia-m</ta>
            <ta e="T921" id="Seg_6344" s="T920">măn</ta>
            <ta e="T922" id="Seg_6345" s="T921">šonə-ga-m</ta>
            <ta e="T923" id="Seg_6346" s="T922">i</ta>
            <ta e="T924" id="Seg_6347" s="T923">nere-luʔ-pie-m</ta>
            <ta e="T925" id="Seg_6348" s="T924">bar</ta>
            <ta e="T927" id="Seg_6349" s="T926">pa-ʔi-nə</ta>
            <ta e="T928" id="Seg_6350" s="T927">nada</ta>
            <ta e="T929" id="Seg_6351" s="T928">sʼa-zittə</ta>
            <ta e="T930" id="Seg_6352" s="T929">a</ta>
            <ta e="T931" id="Seg_6353" s="T930">măn</ta>
            <ta e="T932" id="Seg_6354" s="T931">dĭʔ-nə</ta>
            <ta e="T933" id="Seg_6355" s="T932">măm-bia-m</ta>
            <ta e="T934" id="Seg_6356" s="T933">dĭ</ta>
            <ta e="T935" id="Seg_6357" s="T934">tože</ta>
            <ta e="T936" id="Seg_6358" s="T935">pa-ʔi-nə</ta>
            <ta e="T937" id="Seg_6359" s="T936">sʼa-lia</ta>
            <ta e="T939" id="Seg_6360" s="T938">i</ta>
            <ta e="T940" id="Seg_6361" s="T939">pa</ta>
            <ta e="T941" id="Seg_6362" s="T940">bald-lia</ta>
            <ta e="T942" id="Seg_6363" s="T941">i</ta>
            <ta e="T943" id="Seg_6364" s="T942">dʼü-nə</ta>
            <ta e="T944" id="Seg_6365" s="T943">barʔd-lia</ta>
            <ta e="T946" id="Seg_6366" s="T945">kadəʔ</ta>
            <ta e="T947" id="Seg_6367" s="T946">dĭn</ta>
            <ta e="T948" id="Seg_6368" s="T947">dʼăbaktər-zittə</ta>
            <ta e="T949" id="Seg_6369" s="T948">es-seŋ</ta>
            <ta e="T950" id="Seg_6370" s="T949">bar</ta>
            <ta e="T951" id="Seg_6371" s="T950">kirgar-laʔbə</ta>
            <ta e="T953" id="Seg_6372" s="T952">šoška-ʔi</ta>
            <ta e="T954" id="Seg_6373" s="T953">tože</ta>
            <ta e="T955" id="Seg_6374" s="T954">bar</ta>
            <ta e="T956" id="Seg_6375" s="T955">kirgar-laʔbə-ʔjə</ta>
            <ta e="T957" id="Seg_6376" s="T956">puzo</ta>
            <ta e="T958" id="Seg_6377" s="T957">tože</ta>
            <ta e="T959" id="Seg_6378" s="T958">müre-r-laʔbə</ta>
            <ta e="T961" id="Seg_6379" s="T960">kăde=də</ta>
            <ta e="T962" id="Seg_6380" s="T961">nʼelʼzʼa</ta>
            <ta e="T963" id="Seg_6381" s="T962">dʼăbaktər-zittə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_6382" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_6383" s="T2">šiʔ</ta>
            <ta e="T4" id="Seg_6384" s="T3">aparat</ta>
            <ta e="T5" id="Seg_6385" s="T4">taʔla</ta>
            <ta e="T7" id="Seg_6386" s="T6">tarar-luʔbdə-bi</ta>
            <ta e="T8" id="Seg_6387" s="T7">bar</ta>
            <ta e="T11" id="Seg_6388" s="T10">šʼaːm-zittə</ta>
            <ta e="T13" id="Seg_6389" s="T12">ugaːndə</ta>
            <ta e="T14" id="Seg_6390" s="T13">ɨrɨː</ta>
            <ta e="T15" id="Seg_6391" s="T14">mo-laːm-bi</ta>
            <ta e="T17" id="Seg_6392" s="T16">tăn</ta>
            <ta e="T18" id="Seg_6393" s="T17">ugaːndə</ta>
            <ta e="T19" id="Seg_6394" s="T18">kuguštu</ta>
            <ta e="T20" id="Seg_6395" s="T19">kuza</ta>
            <ta e="T21" id="Seg_6396" s="T20">tănan</ta>
            <ta e="T22" id="Seg_6397" s="T21">măn-liA-l</ta>
            <ta e="T23" id="Seg_6398" s="T22">măn-liA-l</ta>
            <ta e="T24" id="Seg_6399" s="T23">a</ta>
            <ta e="T25" id="Seg_6400" s="T24">tăn</ta>
            <ta e="T26" id="Seg_6401" s="T25">a</ta>
            <ta e="T27" id="Seg_6402" s="T26">tăn</ta>
            <ta e="T28" id="Seg_6403" s="T27">ĭmbi=də</ta>
            <ta e="T29" id="Seg_6404" s="T28">ej</ta>
            <ta e="T30" id="Seg_6405" s="T29">tĭm-liA-l</ta>
            <ta e="T32" id="Seg_6406" s="T31">măn</ta>
            <ta e="T33" id="Seg_6407" s="T32">tănan</ta>
            <ta e="T34" id="Seg_6408" s="T33">măn-liA-m</ta>
            <ta e="T35" id="Seg_6409" s="T34">üdʼüge</ta>
            <ta e="T36" id="Seg_6410" s="T35">i-bi-m</ta>
            <ta e="T38" id="Seg_6411" s="T37">girgit=də</ta>
            <ta e="T39" id="Seg_6412" s="T38">il</ta>
            <ta e="T40" id="Seg_6413" s="T39">šo-bi-jəʔ</ta>
            <ta e="T45" id="Seg_6414" s="T44">ej</ta>
            <ta e="T46" id="Seg_6415" s="T45">tĭm-liA-m</ta>
            <ta e="T47" id="Seg_6416" s="T46">üdʼüge</ta>
            <ta e="T48" id="Seg_6417" s="T47">i-bi-m</ta>
            <ta e="T49" id="Seg_6418" s="T48">ej</ta>
            <ta e="T50" id="Seg_6419" s="T49">surar-bi-m</ta>
            <ta e="T52" id="Seg_6420" s="T51">măna</ta>
            <ta e="T53" id="Seg_6421" s="T52">ej</ta>
            <ta e="T54" id="Seg_6422" s="T53">nörbə-bi-jəʔ</ta>
            <ta e="T56" id="Seg_6423" s="T55">Vlas-Kən</ta>
            <ta e="T61" id="Seg_6424" s="T60">dĭgəttə</ta>
            <ta e="T62" id="Seg_6425" s="T61">dĭ-zen</ta>
            <ta e="T63" id="Seg_6426" s="T62">i-bi</ta>
            <ta e="T65" id="Seg_6427" s="T64">i</ta>
            <ta e="T66" id="Seg_6428" s="T65">ĭmbi</ta>
            <ta e="T67" id="Seg_6429" s="T66">tože</ta>
            <ta e="T68" id="Seg_6430" s="T67">ej</ta>
            <ta e="T69" id="Seg_6431" s="T68">tĭm-liA-m</ta>
            <ta e="T70" id="Seg_6432" s="T69">măna</ta>
            <ta e="T71" id="Seg_6433" s="T70">pʼer-bi-jəʔ</ta>
            <ta e="T72" id="Seg_6434" s="T71">nʼimi</ta>
            <ta e="T74" id="Seg_6435" s="T73">müʔbdə-luʔbdə-bi-jəʔ</ta>
            <ta e="T75" id="Seg_6436" s="T74">stʼena-Tə</ta>
            <ta e="T77" id="Seg_6437" s="T76">kuŋgə-ŋ</ta>
            <ta e="T78" id="Seg_6438" s="T77">dărəʔ</ta>
            <ta e="T80" id="Seg_6439" s="T79">sažen-də</ta>
            <ta e="T81" id="Seg_6440" s="T80">šide</ta>
            <ta e="T82" id="Seg_6441" s="T81">biəʔ</ta>
            <ta e="T84" id="Seg_6442" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_6443" s="T84">măn</ta>
            <ta e="T86" id="Seg_6444" s="T85">măndo-r-bi-m</ta>
            <ta e="T88" id="Seg_6445" s="T87">ugaːndə</ta>
            <ta e="T90" id="Seg_6446" s="T89">nʼimi</ta>
            <ta e="T91" id="Seg_6447" s="T90">urgo</ta>
            <ta e="T93" id="Seg_6448" s="T92">a</ta>
            <ta e="T94" id="Seg_6449" s="T93">dĭgəttə</ta>
            <ta e="T95" id="Seg_6450" s="T94">par-luʔbdə-bi-jəʔ</ta>
            <ta e="T96" id="Seg_6451" s="T95">drugoj</ta>
            <ta e="T97" id="Seg_6452" s="T96">kănʼes-ziʔ</ta>
            <ta e="T99" id="Seg_6453" s="T98">măndo-bi-m</ta>
            <ta e="T100" id="Seg_6454" s="T99">ugaːndə</ta>
            <ta e="T101" id="Seg_6455" s="T100">kuŋgə-ŋ</ta>
            <ta e="T102" id="Seg_6456" s="T101">mo-laːm-bi</ta>
            <ta e="T104" id="Seg_6457" s="T103">dĭgəttə</ta>
            <ta e="T105" id="Seg_6458" s="T104">sazən</ta>
            <ta e="T106" id="Seg_6459" s="T105">hen-bi-jəʔ</ta>
            <ta e="T108" id="Seg_6460" s="T107">i</ta>
            <ta e="T109" id="Seg_6461" s="T108">nuldə-bi-jəʔ</ta>
            <ta e="T110" id="Seg_6462" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_6463" s="T110">binoklʼ</ta>
            <ta e="T112" id="Seg_6464" s="T111">kuja-Tə</ta>
            <ta e="T114" id="Seg_6465" s="T113">i</ta>
            <ta e="T115" id="Seg_6466" s="T114">sazən</ta>
            <ta e="T116" id="Seg_6467" s="T115">neni-luʔbdə-bi</ta>
            <ta e="T118" id="Seg_6468" s="T117">šiʔ</ta>
            <ta e="T119" id="Seg_6469" s="T118">kamən</ta>
            <ta e="T120" id="Seg_6470" s="T119">kan-lV-lAʔ</ta>
            <ta e="T122" id="Seg_6471" s="T121">păredʼa-Tə</ta>
            <ta e="T123" id="Seg_6472" s="T122">kan-lV-bAʔ</ta>
            <ta e="T125" id="Seg_6473" s="T124">no</ta>
            <ta e="T126" id="Seg_6474" s="T125">kan-KAʔ</ta>
            <ta e="T128" id="Seg_6475" s="T127">privet</ta>
            <ta e="T129" id="Seg_6476" s="T128">nörbə-KAʔ</ta>
            <ta e="T131" id="Seg_6477" s="T130">Matvejev-gəndə</ta>
            <ta e="T133" id="Seg_6478" s="T132">i</ta>
            <ta e="T134" id="Seg_6479" s="T133">Jelʼa</ta>
            <ta e="T135" id="Seg_6480" s="T134">teinen</ta>
            <ta e="T964" id="Seg_6481" s="T135">kan-lAʔ</ta>
            <ta e="T136" id="Seg_6482" s="T964">tʼür-bi</ta>
            <ta e="T137" id="Seg_6483" s="T136">Malinovka-Tə</ta>
            <ta e="T139" id="Seg_6484" s="T138">sazən</ta>
            <ta e="T140" id="Seg_6485" s="T139">kun-du-bi</ta>
            <ta e="T142" id="Seg_6486" s="T141">predsʼedatʼelʼ-Tə</ta>
            <ta e="T143" id="Seg_6487" s="T142">nadə</ta>
            <ta e="T144" id="Seg_6488" s="T143">pečatʼ-jəʔ</ta>
            <ta e="T145" id="Seg_6489" s="T144">hen-zittə</ta>
            <ta e="T147" id="Seg_6490" s="T146">dĭ</ta>
            <ta e="T148" id="Seg_6491" s="T147">kan-bi</ta>
            <ta e="T149" id="Seg_6492" s="T148">dĭbər</ta>
            <ta e="T150" id="Seg_6493" s="T149">mašina-jəʔ-ziʔ</ta>
            <ta e="T152" id="Seg_6494" s="T151">a</ta>
            <ta e="T154" id="Seg_6495" s="T153">dĭʔə</ta>
            <ta e="T155" id="Seg_6496" s="T154">par-lV-j</ta>
            <ta e="T156" id="Seg_6497" s="T155">üdʼi-ŋ</ta>
            <ta e="T158" id="Seg_6498" s="T157">măn</ta>
            <ta e="T159" id="Seg_6499" s="T158">sʼestra-Tə</ta>
            <ta e="T160" id="Seg_6500" s="T159">nʼi-t</ta>
            <ta e="T965" id="Seg_6501" s="T160">kan-lAʔ</ta>
            <ta e="T161" id="Seg_6502" s="T965">tʼür-bi</ta>
            <ta e="T162" id="Seg_6503" s="T161">noʔ-ə-j-lAʔ</ta>
            <ta e="T163" id="Seg_6504" s="T162">tüžöj-jəʔ</ta>
            <ta e="T165" id="Seg_6505" s="T164">det-zittə</ta>
            <ta e="T167" id="Seg_6506" s="T166">bos-də</ta>
            <ta e="T168" id="Seg_6507" s="T167">nʼi-t</ta>
            <ta e="T169" id="Seg_6508" s="T168">i-bi</ta>
            <ta e="T171" id="Seg_6509" s="T170">büžü</ta>
            <ta e="T173" id="Seg_6510" s="T172">par-lV-jəʔ</ta>
            <ta e="T174" id="Seg_6511" s="T173">maʔ-gəndə</ta>
            <ta e="T176" id="Seg_6512" s="T175">onʼiʔ</ta>
            <ta e="T177" id="Seg_6513" s="T176">kuza</ta>
            <ta e="T178" id="Seg_6514" s="T177">tenö-luʔbdə-bi</ta>
            <ta e="T179" id="Seg_6515" s="T178">maʔ</ta>
            <ta e="T181" id="Seg_6516" s="T180">a-zittə</ta>
            <ta e="T183" id="Seg_6517" s="T182">zolota-jəʔ-ziʔ</ta>
            <ta e="T185" id="Seg_6518" s="T184">a</ta>
            <ta e="T186" id="Seg_6519" s="T185">baška</ta>
            <ta e="T187" id="Seg_6520" s="T186">kuza</ta>
            <ta e="T188" id="Seg_6521" s="T187">maʔ</ta>
            <ta e="T189" id="Seg_6522" s="T188">a-zittə</ta>
            <ta e="T190" id="Seg_6523" s="T189">sʼerʼebro-gəʔ</ta>
            <ta e="T191" id="Seg_6524" s="T190">a</ta>
            <ta e="T192" id="Seg_6525" s="T191">nagur</ta>
            <ta e="T193" id="Seg_6526" s="T192">kuza</ta>
            <ta e="T194" id="Seg_6527" s="T193">bar</ta>
            <ta e="T195" id="Seg_6528" s="T194">žəzə</ta>
            <ta e="T196" id="Seg_6529" s="T195">măn-ntə</ta>
            <ta e="T197" id="Seg_6530" s="T196">măn</ta>
            <ta e="T198" id="Seg_6531" s="T197">pa-jəʔ-ziʔ</ta>
            <ta e="T199" id="Seg_6532" s="T198">hʼaʔ-lV-m</ta>
            <ta e="T200" id="Seg_6533" s="T199">tura</ta>
            <ta e="T202" id="Seg_6534" s="T201">a</ta>
            <ta e="T203" id="Seg_6535" s="T202">teʔdə</ta>
            <ta e="T204" id="Seg_6536" s="T203">kuza</ta>
            <ta e="T205" id="Seg_6537" s="T204">măn</ta>
            <ta e="T206" id="Seg_6538" s="T205">a-lV-m</ta>
            <ta e="T207" id="Seg_6539" s="T206">noʔ-gəʔ</ta>
            <ta e="T208" id="Seg_6540" s="T207">săloma-jəʔ-ziʔ</ta>
            <ta e="T211" id="Seg_6541" s="T210">šo-lV-j</ta>
            <ta e="T212" id="Seg_6542" s="T211">bar</ta>
            <ta e="T213" id="Seg_6543" s="T212">togonər-də</ta>
            <ta e="T215" id="Seg_6544" s="T214">nend-lV-j</ta>
            <ta e="T217" id="Seg_6545" s="T216">šində-n</ta>
            <ta e="T218" id="Seg_6546" s="T217">togonər-də</ta>
            <ta e="T219" id="Seg_6547" s="T218">ej</ta>
            <ta e="T220" id="Seg_6548" s="T219">nend-lV-m</ta>
            <ta e="T222" id="Seg_6549" s="T221">dĭ</ta>
            <ta e="T223" id="Seg_6550" s="T222">dĭ-Tə</ta>
            <ta e="T224" id="Seg_6551" s="T223">jakšə</ta>
            <ta e="T225" id="Seg_6552" s="T224">mo-lV-j</ta>
            <ta e="T227" id="Seg_6553" s="T226">a</ta>
            <ta e="T228" id="Seg_6554" s="T227">šində-n</ta>
            <ta e="T229" id="Seg_6555" s="T228">togonər-də</ta>
            <ta e="T231" id="Seg_6556" s="T230">nend-luʔbdə-lV-j</ta>
            <ta e="T233" id="Seg_6557" s="T232">dĭ-Tə</ta>
            <ta e="T234" id="Seg_6558" s="T233">ej</ta>
            <ta e="T235" id="Seg_6559" s="T234">jakšə</ta>
            <ta e="T236" id="Seg_6560" s="T235">mo-lV-j</ta>
            <ta e="T238" id="Seg_6561" s="T237">bos-də</ta>
            <ta e="T239" id="Seg_6562" s="T238">ej</ta>
            <ta e="T240" id="Seg_6563" s="T239">nend-lV-l</ta>
            <ta e="T241" id="Seg_6564" s="T240">nuʔmə-luʔbdə-lV-j</ta>
            <ta e="T243" id="Seg_6565" s="T242">a</ta>
            <ta e="T244" id="Seg_6566" s="T243">ĭmbi</ta>
            <ta e="T246" id="Seg_6567" s="T245">a</ta>
            <ta e="T247" id="Seg_6568" s="T246">ĭmbi</ta>
            <ta e="T248" id="Seg_6569" s="T247">togonər-lV</ta>
            <ta e="T249" id="Seg_6570" s="T248">nend-luʔbdə-bi</ta>
            <ta e="T251" id="Seg_6571" s="T250">amno-bi-jəʔ</ta>
            <ta e="T252" id="Seg_6572" s="T251">nagur</ta>
            <ta e="T253" id="Seg_6573" s="T252">šoška-jəʔ</ta>
            <ta e="T254" id="Seg_6574" s="T253">amno-bi-jəʔ</ta>
            <ta e="T256" id="Seg_6575" s="T255">dĭgəttə</ta>
            <ta e="T257" id="Seg_6576" s="T256">šide</ta>
            <ta e="T258" id="Seg_6577" s="T257">šoška</ta>
            <ta e="T259" id="Seg_6578" s="T258">ugaːndə</ta>
            <ta e="T260" id="Seg_6579" s="T259">ɨrɨː</ta>
            <ta e="T261" id="Seg_6580" s="T260">i-bi-jəʔ</ta>
            <ta e="T262" id="Seg_6581" s="T261">a</ta>
            <ta e="T263" id="Seg_6582" s="T262">onʼiʔ</ta>
            <ta e="T264" id="Seg_6583" s="T263">šoška</ta>
            <ta e="T265" id="Seg_6584" s="T264">üge</ta>
            <ta e="T266" id="Seg_6585" s="T265">togonər-bi</ta>
            <ta e="T268" id="Seg_6586" s="T267">dĭ-zAŋ</ta>
            <ta e="T269" id="Seg_6587" s="T268">bar</ta>
            <ta e="T270" id="Seg_6588" s="T269">jaʔtar-lAʔ</ta>
            <ta e="T271" id="Seg_6589" s="T270">mĭn-bi-jəʔ</ta>
            <ta e="T272" id="Seg_6590" s="T271">ara</ta>
            <ta e="T273" id="Seg_6591" s="T272">bĭs-bi-jəʔ</ta>
            <ta e="T275" id="Seg_6592" s="T274">a</ta>
            <ta e="T276" id="Seg_6593" s="T275">dö</ta>
            <ta e="T277" id="Seg_6594" s="T276">onʼiʔ</ta>
            <ta e="T278" id="Seg_6595" s="T277">šoška</ta>
            <ta e="T279" id="Seg_6596" s="T278">bar</ta>
            <ta e="T280" id="Seg_6597" s="T279">tĭl-bi</ta>
            <ta e="T281" id="Seg_6598" s="T280">tʼo</ta>
            <ta e="T283" id="Seg_6599" s="T282">pi-jəʔ</ta>
            <ta e="T284" id="Seg_6600" s="T283">oʔbdo-bi-jəʔ</ta>
            <ta e="T286" id="Seg_6601" s="T285">i</ta>
            <ta e="T287" id="Seg_6602" s="T286">maʔ</ta>
            <ta e="T288" id="Seg_6603" s="T287">bos-gəndə</ta>
            <ta e="T289" id="Seg_6604" s="T288">a-bi</ta>
            <ta e="T291" id="Seg_6605" s="T290">pi-zAŋ-Tə</ta>
            <ta e="T294" id="Seg_6606" s="T293">dĭ-zAŋ</ta>
            <ta e="T295" id="Seg_6607" s="T294">šo-bi-jəʔ</ta>
            <ta e="T296" id="Seg_6608" s="T295">onʼiʔ</ta>
            <ta e="T297" id="Seg_6609" s="T296">hen-bi</ta>
            <ta e="T299" id="Seg_6610" s="T298">mu-jəʔ</ta>
            <ta e="T300" id="Seg_6611" s="T299">oʔbdə-bi</ta>
            <ta e="T301" id="Seg_6612" s="T300">da</ta>
            <ta e="T302" id="Seg_6613" s="T301">hen-bi</ta>
            <ta e="T303" id="Seg_6614" s="T302">a</ta>
            <ta e="T304" id="Seg_6615" s="T303">onʼiʔ</ta>
            <ta e="T305" id="Seg_6616" s="T304">săloma-jəʔ-ziʔ</ta>
            <ta e="T307" id="Seg_6617" s="T306">hen-bi</ta>
            <ta e="T308" id="Seg_6618" s="T307">bos-gəndə</ta>
            <ta e="T309" id="Seg_6619" s="T308">maʔ</ta>
            <ta e="T311" id="Seg_6620" s="T310">a</ta>
            <ta e="T312" id="Seg_6621" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_6622" s="T312">üge</ta>
            <ta e="T314" id="Seg_6623" s="T313">pi-m</ta>
            <ta e="T315" id="Seg_6624" s="T314">pi-jəʔ-ziʔ</ta>
            <ta e="T316" id="Seg_6625" s="T315">hen-laʔbə</ta>
            <ta e="T317" id="Seg_6626" s="T316">dĭ</ta>
            <ta e="T318" id="Seg_6627" s="T317">dĭ-zAŋ</ta>
            <ta e="T319" id="Seg_6628" s="T318">šo-bi-jəʔ</ta>
            <ta e="T320" id="Seg_6629" s="T319">ĭmbi</ta>
            <ta e="T321" id="Seg_6630" s="T320">tăn</ta>
            <ta e="T322" id="Seg_6631" s="T321">togonər-liA-l</ta>
            <ta e="T324" id="Seg_6632" s="T323">miʔ</ta>
            <ta e="T325" id="Seg_6633" s="T324">büžü-n</ta>
            <ta e="T327" id="Seg_6634" s="T326">a-bi-bAʔ</ta>
            <ta e="T329" id="Seg_6635" s="T328">maʔ-də</ta>
            <ta e="T331" id="Seg_6636" s="T330">a</ta>
            <ta e="T333" id="Seg_6637" s="T332">šində-n</ta>
            <ta e="T334" id="Seg_6638" s="T333">jakšə</ta>
            <ta e="T335" id="Seg_6639" s="T334">maʔ-də</ta>
            <ta e="T336" id="Seg_6640" s="T335">mo-lV-j</ta>
            <ta e="T337" id="Seg_6641" s="T336">măn</ta>
            <ta e="T338" id="Seg_6642" s="T337">ilʼi</ta>
            <ta e="T339" id="Seg_6643" s="T338">šiʔ</ta>
            <ta e="T341" id="Seg_6644" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_6645" s="T341">šišəge</ta>
            <ta e="T343" id="Seg_6646" s="T342">mo-laːm-bi</ta>
            <ta e="T344" id="Seg_6647" s="T343">sĭri</ta>
            <ta e="T345" id="Seg_6648" s="T344">sĭri</ta>
            <ta e="T346" id="Seg_6649" s="T345">šonə-gA</ta>
            <ta e="T348" id="Seg_6650" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_6651" s="T348">dĭ-zAŋ</ta>
            <ta e="T351" id="Seg_6652" s="T350">păda-bi-jəʔ</ta>
            <ta e="T352" id="Seg_6653" s="T351">tura-Tə</ta>
            <ta e="T353" id="Seg_6654" s="T352">girgit</ta>
            <ta e="T354" id="Seg_6655" s="T353">săloma</ta>
            <ta e="T356" id="Seg_6656" s="T355">kaj-bi-jəʔ</ta>
            <ta e="T357" id="Seg_6657" s="T356">ajə</ta>
            <ta e="T358" id="Seg_6658" s="T357">šo-bi</ta>
            <ta e="T359" id="Seg_6659" s="T358">urgaːba</ta>
            <ta e="T361" id="Seg_6660" s="T360">öʔ-ləʔ-KAʔ</ta>
            <ta e="T362" id="Seg_6661" s="T361">măna</ta>
            <ta e="T364" id="Seg_6662" s="T363">dĭ-zAŋ</ta>
            <ta e="T365" id="Seg_6663" s="T364">ej</ta>
            <ta e="T366" id="Seg_6664" s="T365">öʔ-lV-ʔ</ta>
            <ta e="T367" id="Seg_6665" s="T366">tüjö</ta>
            <ta e="T368" id="Seg_6666" s="T367">bar</ta>
            <ta e="T369" id="Seg_6667" s="T368">băldə-lV-m</ta>
            <ta e="T370" id="Seg_6668" s="T369">šiʔ</ta>
            <ta e="T371" id="Seg_6669" s="T370">tura-gəʔ</ta>
            <ta e="T372" id="Seg_6670" s="T371">tura-Tə</ta>
            <ta e="T374" id="Seg_6671" s="T373">dĭgəttə</ta>
            <ta e="T375" id="Seg_6672" s="T374">băldə-bi</ta>
            <ta e="T376" id="Seg_6673" s="T375">dĭ-zAŋ</ta>
            <ta e="T377" id="Seg_6674" s="T376">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T379" id="Seg_6675" s="T378">girgit</ta>
            <ta e="T380" id="Seg_6676" s="T379">tura</ta>
            <ta e="T382" id="Seg_6677" s="T381">mu-zAŋ-də</ta>
            <ta e="T383" id="Seg_6678" s="T382">a-bi-jəʔ</ta>
            <ta e="T385" id="Seg_6679" s="T384">i</ta>
            <ta e="T386" id="Seg_6680" s="T385">dĭbər</ta>
            <ta e="T387" id="Seg_6681" s="T386">ajə</ta>
            <ta e="T389" id="Seg_6682" s="T388">kaj-luʔbdə-bi-jəʔ</ta>
            <ta e="T390" id="Seg_6683" s="T389">dĭ</ta>
            <ta e="T391" id="Seg_6684" s="T390">dĭbər</ta>
            <ta e="T392" id="Seg_6685" s="T391">šo-bi</ta>
            <ta e="T394" id="Seg_6686" s="T393">kar-KAʔ</ta>
            <ta e="T396" id="Seg_6687" s="T395">ato</ta>
            <ta e="T397" id="Seg_6688" s="T396">tüjö</ta>
            <ta e="T398" id="Seg_6689" s="T397">bar</ta>
            <ta e="T399" id="Seg_6690" s="T398">saʔmə-luʔbdə-lV-j</ta>
            <ta e="T400" id="Seg_6691" s="T399">tura</ta>
            <ta e="T402" id="Seg_6692" s="T401">dĭgəttə</ta>
            <ta e="T403" id="Seg_6693" s="T402">bar</ta>
            <ta e="T404" id="Seg_6694" s="T403">barəʔ-bi</ta>
            <ta e="T405" id="Seg_6695" s="T404">dĭ-zAŋ</ta>
            <ta e="T406" id="Seg_6696" s="T405">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T408" id="Seg_6697" s="T407">i</ta>
            <ta e="T409" id="Seg_6698" s="T408">šo-bi-jəʔ</ta>
            <ta e="T410" id="Seg_6699" s="T409">döbər</ta>
            <ta e="T411" id="Seg_6700" s="T410">onʼiʔ-Tə</ta>
            <ta e="T413" id="Seg_6701" s="T412">nagur-Tə</ta>
            <ta e="T414" id="Seg_6702" s="T413">šoška-gəndə</ta>
            <ta e="T415" id="Seg_6703" s="T414">šo-bi-jəʔ</ta>
            <ta e="T417" id="Seg_6704" s="T416">öʔ-lei-ʔ</ta>
            <ta e="T418" id="Seg_6705" s="T417">miʔnʼibeʔ</ta>
            <ta e="T419" id="Seg_6706" s="T418">dĭ</ta>
            <ta e="T420" id="Seg_6707" s="T419">öʔ-lei-bi</ta>
            <ta e="T421" id="Seg_6708" s="T420">i</ta>
            <ta e="T422" id="Seg_6709" s="T421">ajə-bə</ta>
            <ta e="T424" id="Seg_6710" s="T423">kaj-bi</ta>
            <ta e="T425" id="Seg_6711" s="T424">dĭ</ta>
            <ta e="T426" id="Seg_6712" s="T425">šo-bi</ta>
            <ta e="T427" id="Seg_6713" s="T426">urgaːba</ta>
            <ta e="T428" id="Seg_6714" s="T427">öʔ-lei-ʔ</ta>
            <ta e="T430" id="Seg_6715" s="T429">ej</ta>
            <ta e="T431" id="Seg_6716" s="T430">öʔ-lV-m</ta>
            <ta e="T433" id="Seg_6717" s="T432">dĭgəttə</ta>
            <ta e="T434" id="Seg_6718" s="T433">dĭ</ta>
            <ta e="T435" id="Seg_6719" s="T434">băldə-zittə</ta>
            <ta e="T436" id="Seg_6720" s="T435">xatʼel</ta>
            <ta e="T437" id="Seg_6721" s="T436">ej</ta>
            <ta e="T438" id="Seg_6722" s="T437">mo-bi</ta>
            <ta e="T439" id="Seg_6723" s="T438">dĭgəttə</ta>
            <ta e="T441" id="Seg_6724" s="T440">sʼa-bi</ta>
            <ta e="T442" id="Seg_6725" s="T441">dĭbər</ta>
            <ta e="T444" id="Seg_6726" s="T443">nʼuʔdə</ta>
            <ta e="T445" id="Seg_6727" s="T444">truba-gəndə</ta>
            <ta e="T447" id="Seg_6728" s="T446">paʔ-laːndə-gA</ta>
            <ta e="T448" id="Seg_6729" s="T447">a</ta>
            <ta e="T449" id="Seg_6730" s="T448">dĭ</ta>
            <ta e="T450" id="Seg_6731" s="T449">aspaʔ</ta>
            <ta e="T451" id="Seg_6732" s="T450">nuldə-bi</ta>
            <ta e="T452" id="Seg_6733" s="T451">dʼibige</ta>
            <ta e="T453" id="Seg_6734" s="T452">bü-ziʔ</ta>
            <ta e="T455" id="Seg_6735" s="T454">dĭ</ta>
            <ta e="T456" id="Seg_6736" s="T455">bar</ta>
            <ta e="T457" id="Seg_6737" s="T456">saʔmə-luʔbdə-bi</ta>
            <ta e="T458" id="Seg_6738" s="T457">aspaʔ-də</ta>
            <ta e="T460" id="Seg_6739" s="T459">i</ta>
            <ta e="T462" id="Seg_6740" s="T461">dĭgəttə</ta>
            <ta e="T463" id="Seg_6741" s="T462">par-luʔbdə-bi</ta>
            <ta e="T464" id="Seg_6742" s="T463">i</ta>
            <ta e="T966" id="Seg_6743" s="T464">kan-lAʔ</ta>
            <ta e="T465" id="Seg_6744" s="T966">tʼür-bi</ta>
            <ta e="T466" id="Seg_6745" s="T465">i</ta>
            <ta e="T467" id="Seg_6746" s="T466">daška</ta>
            <ta e="T468" id="Seg_6747" s="T467">kamən=də</ta>
            <ta e="T469" id="Seg_6748" s="T468">ej</ta>
            <ta e="T470" id="Seg_6749" s="T469">šo-bi</ta>
            <ta e="T473" id="Seg_6750" s="T472">nendə-bi-jəʔ</ta>
            <ta e="T474" id="Seg_6751" s="T473">dĭ-m</ta>
            <ta e="T476" id="Seg_6752" s="T475">i</ta>
            <ta e="T967" id="Seg_6753" s="T476">kan-lAʔ</ta>
            <ta e="T477" id="Seg_6754" s="T967">tʼür-bi</ta>
            <ta e="T479" id="Seg_6755" s="T478">kumən</ta>
            <ta e="T481" id="Seg_6756" s="T480">miʔ</ta>
            <ta e="T482" id="Seg_6757" s="T481">plʼonka-jəʔ</ta>
            <ta e="T483" id="Seg_6758" s="T482">tʼăbaktər-bi-m</ta>
            <ta e="T484" id="Seg_6759" s="T483">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T486" id="Seg_6760" s="T485">biəʔ</ta>
            <ta e="T487" id="Seg_6761" s="T486">nagur</ta>
            <ta e="T489" id="Seg_6762" s="T488">šiʔ</ta>
            <ta e="T490" id="Seg_6763" s="T489">ipek-lAʔ</ta>
            <ta e="T491" id="Seg_6764" s="T490">i-gA</ta>
            <ta e="T493" id="Seg_6765" s="T492">i-gA</ta>
            <ta e="T494" id="Seg_6766" s="T493">a</ta>
            <ta e="T495" id="Seg_6767" s="T494">gijen</ta>
            <ta e="T496" id="Seg_6768" s="T495">i-bi-lAʔ</ta>
            <ta e="T498" id="Seg_6769" s="T497">da</ta>
            <ta e="T499" id="Seg_6770" s="T498">onʼiʔ</ta>
            <ta e="T500" id="Seg_6771" s="T499">ne</ta>
            <ta e="T501" id="Seg_6772" s="T500">det-bi</ta>
            <ta e="T504" id="Seg_6773" s="T503">Văznesenka-jəʔ-ziʔ</ta>
            <ta e="T506" id="Seg_6774" s="T505">onʼiʔ</ta>
            <ta e="T507" id="Seg_6775" s="T506">poʔto-n</ta>
            <ta e="T508" id="Seg_6776" s="T507">sumna</ta>
            <ta e="T509" id="Seg_6777" s="T508">ešši-zAŋ-Tə</ta>
            <ta e="T510" id="Seg_6778" s="T509">i-bi-jəʔ</ta>
            <ta e="T512" id="Seg_6779" s="T511">dĭ</ta>
            <ta e="T513" id="Seg_6780" s="T512">dĭ-zem</ta>
            <ta e="T514" id="Seg_6781" s="T513">maʔ-Tə-n</ta>
            <ta e="T515" id="Seg_6782" s="T514">ma-bi</ta>
            <ta e="T517" id="Seg_6783" s="T516">ajə-m</ta>
            <ta e="T518" id="Seg_6784" s="T517">kaj-bi</ta>
            <ta e="T520" id="Seg_6785" s="T519">šində-m=də</ta>
            <ta e="T521" id="Seg_6786" s="T520">e-ʔ</ta>
            <ta e="T523" id="Seg_6787" s="T522">e-ʔ</ta>
            <ta e="T524" id="Seg_6788" s="T523">öʔ-ləʔ-lAʔ</ta>
            <ta e="T526" id="Seg_6789" s="T525">dĭgəttə</ta>
            <ta e="T968" id="Seg_6790" s="T526">kan-lAʔ</ta>
            <ta e="T527" id="Seg_6791" s="T968">tʼür-bi</ta>
            <ta e="T529" id="Seg_6792" s="T528">urgaːba</ta>
            <ta e="T530" id="Seg_6793" s="T529">šo-bi</ta>
            <ta e="T531" id="Seg_6794" s="T530">kar-Kut</ta>
            <ta e="T532" id="Seg_6795" s="T531">ajə</ta>
            <ta e="T534" id="Seg_6796" s="T533">dĭ-zAŋ</ta>
            <ta e="T535" id="Seg_6797" s="T534">ej</ta>
            <ta e="T536" id="Seg_6798" s="T535">kar-labaʔ-jəʔ</ta>
            <ta e="T538" id="Seg_6799" s="T537">miʔnʼibeʔ</ta>
            <ta e="T539" id="Seg_6800" s="T538">ija-m</ta>
            <ta e="T540" id="Seg_6801" s="T539">măn-bi</ta>
            <ta e="T541" id="Seg_6802" s="T540">šində-m=də</ta>
            <ta e="T542" id="Seg_6803" s="T541">ej</ta>
            <ta e="T543" id="Seg_6804" s="T542">öʔ-ləʔ-zittə</ta>
            <ta e="T545" id="Seg_6805" s="T544">dĭgəttə</ta>
            <ta e="T546" id="Seg_6806" s="T545">urgaːba</ta>
            <ta e="T969" id="Seg_6807" s="T546">kan-lAʔ</ta>
            <ta e="T547" id="Seg_6808" s="T969">tʼür-bi</ta>
            <ta e="T548" id="Seg_6809" s="T547">ija-t</ta>
            <ta e="T549" id="Seg_6810" s="T548">šo-bi</ta>
            <ta e="T551" id="Seg_6811" s="T550">kar-KAʔ</ta>
            <ta e="T552" id="Seg_6812" s="T551">ajə</ta>
            <ta e="T553" id="Seg_6813" s="T552">măn</ta>
            <ta e="T554" id="Seg_6814" s="T553">det-bi-m</ta>
            <ta e="T555" id="Seg_6815" s="T554">süt</ta>
            <ta e="T557" id="Seg_6816" s="T556">det-bi-m</ta>
            <ta e="T558" id="Seg_6817" s="T557">noʔ</ta>
            <ta e="T559" id="Seg_6818" s="T558">šiʔnʼileʔ</ta>
            <ta e="T560" id="Seg_6819" s="T559">tvorog</ta>
            <ta e="T561" id="Seg_6820" s="T560">det-bi-m</ta>
            <ta e="T563" id="Seg_6821" s="T562">dĭgəttə</ta>
            <ta e="T564" id="Seg_6822" s="T563">dĭ-zAŋ</ta>
            <ta e="T565" id="Seg_6823" s="T564">kar-bi-jəʔ</ta>
            <ta e="T566" id="Seg_6824" s="T565">am-bi-jəʔ</ta>
            <ta e="T567" id="Seg_6825" s="T566">dĭgəttə</ta>
            <ta e="T568" id="Seg_6826" s="T567">ija-t</ta>
            <ta e="T569" id="Seg_6827" s="T568">bazoʔ</ta>
            <ta e="T570" id="Seg_6828" s="T569">kan-bi</ta>
            <ta e="T572" id="Seg_6829" s="T571">e-ʔ</ta>
            <ta e="T573" id="Seg_6830" s="T572">öʔ-ləʔ-KAʔ</ta>
            <ta e="T574" id="Seg_6831" s="T573">šində-m=də</ta>
            <ta e="T576" id="Seg_6832" s="T575">bazoʔ</ta>
            <ta e="T970" id="Seg_6833" s="T576">kan-lAʔ</ta>
            <ta e="T577" id="Seg_6834" s="T970">tʼür-bi</ta>
            <ta e="T579" id="Seg_6835" s="T578">šində-m=də</ta>
            <ta e="T580" id="Seg_6836" s="T579">e-ʔ</ta>
            <ta e="T581" id="Seg_6837" s="T580">öʔ-ləʔ</ta>
            <ta e="T583" id="Seg_6838" s="T582">dĭgəttə</ta>
            <ta e="T584" id="Seg_6839" s="T583">bazoʔ</ta>
            <ta e="T585" id="Seg_6840" s="T584">šo-bi</ta>
            <ta e="T586" id="Seg_6841" s="T585">urgaːba</ta>
            <ta e="T588" id="Seg_6842" s="T587">ešši-zAŋ</ta>
            <ta e="T589" id="Seg_6843" s="T588">ešši-zAŋ</ta>
            <ta e="T590" id="Seg_6844" s="T589">öʔ-lAʔ</ta>
            <ta e="T591" id="Seg_6845" s="T590">măna</ta>
            <ta e="T593" id="Seg_6846" s="T592">dʼok</ta>
            <ta e="T595" id="Seg_6847" s="T594">miʔ</ta>
            <ta e="T596" id="Seg_6848" s="T595">ej</ta>
            <ta e="T597" id="Seg_6849" s="T596">öʔ-lAʔ</ta>
            <ta e="T599" id="Seg_6850" s="T598">a</ta>
            <ta e="T600" id="Seg_6851" s="T599">măn</ta>
            <ta e="T601" id="Seg_6852" s="T600">šiʔ</ta>
            <ta e="T602" id="Seg_6853" s="T601">ija</ta>
            <ta e="T603" id="Seg_6854" s="T602">i-gA-m</ta>
            <ta e="T604" id="Seg_6855" s="T603">dʼok</ta>
            <ta e="T605" id="Seg_6856" s="T604">măn</ta>
            <ta e="T607" id="Seg_6857" s="T606">ija-nə</ta>
            <ta e="T608" id="Seg_6858" s="T607">golos-də</ta>
            <ta e="T609" id="Seg_6859" s="T608">todam</ta>
            <ta e="T611" id="Seg_6860" s="T610">a</ta>
            <ta e="T612" id="Seg_6861" s="T611">tăn</ta>
            <ta e="T613" id="Seg_6862" s="T612">golos-də</ta>
            <ta e="T614" id="Seg_6863" s="T613">ugaːndə</ta>
            <ta e="T615" id="Seg_6864" s="T614">nʼešpək</ta>
            <ta e="T617" id="Seg_6865" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_6866" s="T617">urgaːba</ta>
            <ta e="T971" id="Seg_6867" s="T618">kan-lAʔ</ta>
            <ta e="T619" id="Seg_6868" s="T971">tʼür-bi</ta>
            <ta e="T620" id="Seg_6869" s="T619">dĭgəttə</ta>
            <ta e="T621" id="Seg_6870" s="T620">ija-t</ta>
            <ta e="T622" id="Seg_6871" s="T621">šo-bi</ta>
            <ta e="T624" id="Seg_6872" s="T623">dĭ-zAŋ</ta>
            <ta e="T625" id="Seg_6873" s="T624">kar-bi-jəʔ</ta>
            <ta e="T626" id="Seg_6874" s="T625">bădə-bi</ta>
            <ta e="T628" id="Seg_6875" s="T627">tüj</ta>
            <ta e="T629" id="Seg_6876" s="T628">šində-Tə=də</ta>
            <ta e="T630" id="Seg_6877" s="T629">ej</ta>
            <ta e="T631" id="Seg_6878" s="T630">öʔ-ləʔ</ta>
            <ta e="T633" id="Seg_6879" s="T632">a</ta>
            <ta e="T634" id="Seg_6880" s="T633">urgaːba</ta>
            <ta e="T635" id="Seg_6881" s="T634">kan-bi</ta>
            <ta e="T636" id="Seg_6882" s="T635">kuznʼitsa-Tə</ta>
            <ta e="T638" id="Seg_6883" s="T637">šĭkə-bə</ta>
            <ta e="T640" id="Seg_6884" s="T639">a-bi</ta>
            <ta e="T641" id="Seg_6885" s="T640">todam</ta>
            <ta e="T643" id="Seg_6886" s="T642">dĭgəttə</ta>
            <ta e="T644" id="Seg_6887" s="T643">šo-bi</ta>
            <ta e="T645" id="Seg_6888" s="T644">bazoʔ</ta>
            <ta e="T647" id="Seg_6889" s="T646">dĭ</ta>
            <ta e="T648" id="Seg_6890" s="T647">măn-liA</ta>
            <ta e="T649" id="Seg_6891" s="T648">măn</ta>
            <ta e="T650" id="Seg_6892" s="T649">šiʔ</ta>
            <ta e="T651" id="Seg_6893" s="T650">ija-l</ta>
            <ta e="T652" id="Seg_6894" s="T651">det-bi-m</ta>
            <ta e="T653" id="Seg_6895" s="T652">süt</ta>
            <ta e="T655" id="Seg_6896" s="T654">ipek</ta>
            <ta e="T656" id="Seg_6897" s="T655">det-bi-m</ta>
            <ta e="T658" id="Seg_6898" s="T657">det-bi-m</ta>
            <ta e="T659" id="Seg_6899" s="T658">šiʔnʼileʔ</ta>
            <ta e="T661" id="Seg_6900" s="T660">noʔ</ta>
            <ta e="T663" id="Seg_6901" s="T662">dĭ-zAŋ</ta>
            <ta e="T664" id="Seg_6902" s="T663">bar</ta>
            <ta e="T665" id="Seg_6903" s="T664">kar-luʔbdə-bi</ta>
            <ta e="T667" id="Seg_6904" s="T666">dĭ</ta>
            <ta e="T668" id="Seg_6905" s="T667">dĭ-zem</ta>
            <ta e="T669" id="Seg_6906" s="T668">bar</ta>
            <ta e="T670" id="Seg_6907" s="T669">am-luʔbdə-bi</ta>
            <ta e="T672" id="Seg_6908" s="T671">ija-t</ta>
            <ta e="T673" id="Seg_6909" s="T672">šo-bi</ta>
            <ta e="T675" id="Seg_6910" s="T674">ešši-zAŋ-də</ta>
            <ta e="T676" id="Seg_6911" s="T675">naga</ta>
            <ta e="T677" id="Seg_6912" s="T676">onʼiʔ</ta>
            <ta e="T680" id="Seg_6913" s="T679">üdʼüge</ta>
            <ta e="T681" id="Seg_6914" s="T680">šaʔ-laːm-bi</ta>
            <ta e="T683" id="Seg_6915" s="T682">dĭgəttə</ta>
            <ta e="T684" id="Seg_6916" s="T683">măn-ntə</ta>
            <ta e="T685" id="Seg_6917" s="T684">urgaːba</ta>
            <ta e="T686" id="Seg_6918" s="T685">bar</ta>
            <ta e="T687" id="Seg_6919" s="T686">am-luʔbdə-bi</ta>
            <ta e="T689" id="Seg_6920" s="T688">măn</ta>
            <ta e="T690" id="Seg_6921" s="T689">unʼə</ta>
            <ta e="T691" id="Seg_6922" s="T690">ma-luʔbdə-bi-m</ta>
            <ta e="T692" id="Seg_6923" s="T691">pʼeːš-Tə</ta>
            <ta e="T698" id="Seg_6924" s="T697">pʼeːš-Tə</ta>
            <ta e="T699" id="Seg_6925" s="T698">paʔ-luʔbdə-bi-m</ta>
            <ta e="T700" id="Seg_6926" s="T699">dĭ</ta>
            <ta e="T701" id="Seg_6927" s="T700">măna</ta>
            <ta e="T702" id="Seg_6928" s="T701">ej</ta>
            <ta e="T703" id="Seg_6929" s="T702">măndo-r-bi</ta>
            <ta e="T972" id="Seg_6930" s="T703">kan-lAʔ</ta>
            <ta e="T704" id="Seg_6931" s="T972">tʼür-bi</ta>
            <ta e="T706" id="Seg_6932" s="T705">dĭgəttə</ta>
            <ta e="T707" id="Seg_6933" s="T706">urgaːba</ta>
            <ta e="T708" id="Seg_6934" s="T707">šo-bi</ta>
            <ta e="T709" id="Seg_6935" s="T708">poʔto-Tə</ta>
            <ta e="T711" id="Seg_6936" s="T710">poʔto</ta>
            <ta e="T712" id="Seg_6937" s="T711">măn-ntə</ta>
            <ta e="T715" id="Seg_6938" s="T714">kan-žə-bəj</ta>
            <ta e="T717" id="Seg_6939" s="T716">süʔmə-bəj</ta>
            <ta e="T718" id="Seg_6940" s="T717">dö</ta>
            <ta e="T719" id="Seg_6941" s="T718">jama-gəʔ</ta>
            <ta e="T722" id="Seg_6942" s="T721">poʔto</ta>
            <ta e="T723" id="Seg_6943" s="T722">süʔmə-bi</ta>
            <ta e="T726" id="Seg_6944" s="T725">a</ta>
            <ta e="T727" id="Seg_6945" s="T726">dĭ</ta>
            <ta e="T728" id="Seg_6946" s="T727">ej</ta>
            <ta e="T729" id="Seg_6947" s="T728">mo-bi</ta>
            <ta e="T730" id="Seg_6948" s="T729">süʔmə-šte</ta>
            <ta e="T731" id="Seg_6949" s="T730">dĭbər</ta>
            <ta e="T733" id="Seg_6950" s="T732">kuzur-luʔbdə-bi</ta>
            <ta e="T734" id="Seg_6951" s="T733">ĭmbi-nən-də</ta>
            <ta e="T735" id="Seg_6952" s="T734">bar</ta>
            <ta e="T736" id="Seg_6953" s="T735">nanə</ta>
            <ta e="T738" id="Seg_6954" s="T737">dĭgəttə</ta>
            <ta e="T739" id="Seg_6955" s="T738">dĭn</ta>
            <ta e="T741" id="Seg_6956" s="T740">kăzlʼata-jəʔ</ta>
            <ta e="T743" id="Seg_6957" s="T742">süʔmə-luʔbdə</ta>
            <ta e="T744" id="Seg_6958" s="T743">i</ta>
            <ta e="T745" id="Seg_6959" s="T744">bazoʔ</ta>
            <ta e="T746" id="Seg_6960" s="T745">ija-gəndə</ta>
            <ta e="T973" id="Seg_6961" s="T746">kan-lAʔ</ta>
            <ta e="T747" id="Seg_6962" s="T973">tʼür-bi-jəʔ</ta>
            <ta e="T749" id="Seg_6963" s="T748">kan-žə-bəj</ta>
            <ta e="T750" id="Seg_6964" s="T749">gibər=nʼibudʼ</ta>
            <ta e="T751" id="Seg_6965" s="T750">tăn-ziʔ</ta>
            <ta e="T753" id="Seg_6966" s="T752">jaʔtar-lAʔ</ta>
            <ta e="T755" id="Seg_6967" s="T754">a</ta>
            <ta e="T756" id="Seg_6968" s="T755">ĭmbi</ta>
            <ta e="T757" id="Seg_6969" s="T756">dĭn</ta>
            <ta e="T758" id="Seg_6970" s="T757">mo-lV-j</ta>
            <ta e="T760" id="Seg_6971" s="T759">da</ta>
            <ta e="T761" id="Seg_6972" s="T760">ĭmbi</ta>
            <ta e="T762" id="Seg_6973" s="T761">mo-lV-j</ta>
            <ta e="T763" id="Seg_6974" s="T762">ara</ta>
            <ta e="T764" id="Seg_6975" s="T763">bĭs-lV-bAʔ</ta>
            <ta e="T765" id="Seg_6976" s="T764">nüjnə</ta>
            <ta e="T766" id="Seg_6977" s="T765">nüj-lV-bAʔ</ta>
            <ta e="T768" id="Seg_6978" s="T767">süʔmə-lV-bAʔ</ta>
            <ta e="T769" id="Seg_6979" s="T768">garmonʼ-ziʔ</ta>
            <ta e="T770" id="Seg_6980" s="T769">sʼar-z-bi-bAʔ</ta>
            <ta e="T772" id="Seg_6981" s="T771">dĭn</ta>
            <ta e="T773" id="Seg_6982" s="T772">bar</ta>
            <ta e="T774" id="Seg_6983" s="T773">ĭmbi</ta>
            <ta e="T775" id="Seg_6984" s="T774">iʔgö</ta>
            <ta e="T776" id="Seg_6985" s="T775">uja</ta>
            <ta e="T777" id="Seg_6986" s="T776">iʔgö</ta>
            <ta e="T778" id="Seg_6987" s="T777">seŋ</ta>
            <ta e="T779" id="Seg_6988" s="T778">kajaʔ</ta>
            <ta e="T781" id="Seg_6989" s="T780">kola</ta>
            <ta e="T782" id="Seg_6990" s="T781">mĭnzər-o-NTA</ta>
            <ta e="T784" id="Seg_6991" s="T783">ipek</ta>
            <ta e="T786" id="Seg_6992" s="T785">sĭreʔp</ta>
            <ta e="T787" id="Seg_6993" s="T786">nu-gA</ta>
            <ta e="T789" id="Seg_6994" s="T788">lem</ta>
            <ta e="T790" id="Seg_6995" s="T789">keʔbde-jəʔ</ta>
            <ta e="T791" id="Seg_6996" s="T790">i-gA</ta>
            <ta e="T792" id="Seg_6997" s="T791">kobo</ta>
            <ta e="T793" id="Seg_6998" s="T792">keʔbde-jəʔ</ta>
            <ta e="T794" id="Seg_6999" s="T793">sagər</ta>
            <ta e="T795" id="Seg_7000" s="T794">keʔbde-jəʔ</ta>
            <ta e="T796" id="Seg_7001" s="T795">kan-žə-bəj</ta>
            <ta e="T798" id="Seg_7002" s="T797">tăn</ta>
            <ta e="T799" id="Seg_7003" s="T798">tura-jəʔ</ta>
            <ta e="T800" id="Seg_7004" s="T799">kür-bi-l</ta>
            <ta e="T802" id="Seg_7005" s="T801">kür-bi-l</ta>
            <ta e="T804" id="Seg_7006" s="T803">kumən</ta>
            <ta e="T805" id="Seg_7007" s="T804">tura</ta>
            <ta e="T806" id="Seg_7008" s="T805">kür-bi-l</ta>
            <ta e="T808" id="Seg_7009" s="T807">sejʔpü</ta>
            <ta e="T810" id="Seg_7010" s="T809">tăn</ta>
            <ta e="T811" id="Seg_7011" s="T810">tura-zAŋ-Tə</ta>
            <ta e="T812" id="Seg_7012" s="T811">kür-bi-l</ta>
            <ta e="T814" id="Seg_7013" s="T813">tăn</ta>
            <ta e="T816" id="Seg_7014" s="T815">dʼije-Tə</ta>
            <ta e="T817" id="Seg_7015" s="T816">ej</ta>
            <ta e="T818" id="Seg_7016" s="T817">mĭn-bi-l</ta>
            <ta e="T819" id="Seg_7017" s="T818">ej</ta>
            <ta e="T820" id="Seg_7018" s="T819">mĭn-bi-m</ta>
            <ta e="T821" id="Seg_7019" s="T820">măn</ta>
            <ta e="T822" id="Seg_7020" s="T821">pim-liA-m</ta>
            <ta e="T823" id="Seg_7021" s="T822">dĭn</ta>
            <ta e="T824" id="Seg_7022" s="T823">bar</ta>
            <ta e="T825" id="Seg_7023" s="T824">urgaːba</ta>
            <ta e="T826" id="Seg_7024" s="T825">amno</ta>
            <ta e="T829" id="Seg_7025" s="T828">ĭmbi</ta>
            <ta e="T830" id="Seg_7026" s="T829">pim-liA-l</ta>
            <ta e="T831" id="Seg_7027" s="T830">kan-ə-ʔ</ta>
            <ta e="T834" id="Seg_7028" s="T833">ĭmbi=də</ta>
            <ta e="T835" id="Seg_7029" s="T834">ej</ta>
            <ta e="T836" id="Seg_7030" s="T835">a-lV-j</ta>
            <ta e="T837" id="Seg_7031" s="T836">ej</ta>
            <ta e="T838" id="Seg_7032" s="T837">a-lV-j</ta>
            <ta e="T839" id="Seg_7033" s="T838">tănan</ta>
            <ta e="T841" id="Seg_7034" s="T840">šaʔ-ə-luʔbdə-lV-l</ta>
            <ta e="T842" id="Seg_7035" s="T841">dĭgəttə</ta>
            <ta e="T843" id="Seg_7036" s="T842">maʔ-Tə-l</ta>
            <ta e="T844" id="Seg_7037" s="T843">šo-lV-l</ta>
            <ta e="T846" id="Seg_7038" s="T845">le-j-zittə</ta>
            <ta e="T847" id="Seg_7039" s="T846">možna</ta>
            <ta e="T852" id="Seg_7040" s="T850">vsʼoravno</ta>
            <ta e="T853" id="Seg_7041" s="T852">măn</ta>
            <ta e="T854" id="Seg_7042" s="T853">pim-liA-m</ta>
            <ta e="T855" id="Seg_7043" s="T854">măn</ta>
            <ta e="T856" id="Seg_7044" s="T855">ĭmbi=də</ta>
            <ta e="T857" id="Seg_7045" s="T856">naga</ta>
            <ta e="T858" id="Seg_7046" s="T857">multuk</ta>
            <ta e="T859" id="Seg_7047" s="T858">multuk</ta>
            <ta e="T860" id="Seg_7048" s="T859">naga</ta>
            <ta e="T861" id="Seg_7049" s="T860">tagaj-m</ta>
            <ta e="T862" id="Seg_7050" s="T861">naga</ta>
            <ta e="T864" id="Seg_7051" s="T863">tolʼko</ta>
            <ta e="T865" id="Seg_7052" s="T864">pi</ta>
            <ta e="T866" id="Seg_7053" s="T865">a</ta>
            <ta e="T867" id="Seg_7054" s="T866">dĭ</ta>
            <ta e="T868" id="Seg_7055" s="T867">ej</ta>
            <ta e="T869" id="Seg_7056" s="T868">ĭzem-ntə-j</ta>
            <ta e="T871" id="Seg_7057" s="T870">dĭ</ta>
            <ta e="T872" id="Seg_7058" s="T871">kuza-gəʔ</ta>
            <ta e="T873" id="Seg_7059" s="T872">pim-liA</ta>
            <ta e="T874" id="Seg_7060" s="T873">nuʔmə-luʔbdə-lV-j</ta>
            <ta e="T876" id="Seg_7061" s="T875">miʔ</ta>
            <ta e="T877" id="Seg_7062" s="T876">kan-bi-bAʔ</ta>
            <ta e="T880" id="Seg_7063" s="T879">dĭ</ta>
            <ta e="T881" id="Seg_7064" s="T880">dʼije-gəʔ</ta>
            <ta e="T882" id="Seg_7065" s="T881">dʼije-Tə</ta>
            <ta e="T884" id="Seg_7066" s="T883">dĭgəttə</ta>
            <ta e="T885" id="Seg_7067" s="T884">sĭri</ta>
            <ta e="T886" id="Seg_7068" s="T885">kan-luʔbdə-bi</ta>
            <ta e="T888" id="Seg_7069" s="T887">dĭ-zAŋ</ta>
            <ta e="T889" id="Seg_7070" s="T888">nagur-göʔ</ta>
            <ta e="T890" id="Seg_7071" s="T889">kan-bi-jəʔ</ta>
            <ta e="T891" id="Seg_7072" s="T890">a</ta>
            <ta e="T892" id="Seg_7073" s="T891">măn</ta>
            <ta e="T893" id="Seg_7074" s="T892">par-luʔbdə-bi-m</ta>
            <ta e="T894" id="Seg_7075" s="T893">piʔtə</ta>
            <ta e="T896" id="Seg_7076" s="T895">dĭgəttə</ta>
            <ta e="T897" id="Seg_7077" s="T896">nünə-liA-m</ta>
            <ta e="T898" id="Seg_7078" s="T897">šində=də</ta>
            <ta e="T899" id="Seg_7079" s="T898">bar</ta>
            <ta e="T900" id="Seg_7080" s="T899">mu-zAŋ</ta>
            <ta e="T901" id="Seg_7081" s="T900">mu-zAŋ-də</ta>
            <ta e="T902" id="Seg_7082" s="T901">bar</ta>
            <ta e="T903" id="Seg_7083" s="T902">băt-laʔbə</ta>
            <ta e="T906" id="Seg_7084" s="T905">tene-m</ta>
            <ta e="T907" id="Seg_7085" s="T906">urgaːba</ta>
            <ta e="T909" id="Seg_7086" s="T908">kan-</ta>
            <ta e="T911" id="Seg_7087" s="T910">bar</ta>
            <ta e="T913" id="Seg_7088" s="T912">măldlaʔpə</ta>
            <ta e="T914" id="Seg_7089" s="T913">a</ta>
            <ta e="T915" id="Seg_7090" s="T914">ku-zittə</ta>
            <ta e="T916" id="Seg_7091" s="T915">ej</ta>
            <ta e="T917" id="Seg_7092" s="T916">ku-bi-m</ta>
            <ta e="T918" id="Seg_7093" s="T917">maʔ-gənʼi</ta>
            <ta e="T919" id="Seg_7094" s="T918">šo-bi-m</ta>
            <ta e="T921" id="Seg_7095" s="T920">măn</ta>
            <ta e="T922" id="Seg_7096" s="T921">šonə-gA-m</ta>
            <ta e="T923" id="Seg_7097" s="T922">i</ta>
            <ta e="T924" id="Seg_7098" s="T923">nereʔ-luʔbdə-bi-m</ta>
            <ta e="T925" id="Seg_7099" s="T924">bar</ta>
            <ta e="T927" id="Seg_7100" s="T926">pa-jəʔ-Tə</ta>
            <ta e="T928" id="Seg_7101" s="T927">nadə</ta>
            <ta e="T929" id="Seg_7102" s="T928">sʼa-zittə</ta>
            <ta e="T930" id="Seg_7103" s="T929">a</ta>
            <ta e="T931" id="Seg_7104" s="T930">măn</ta>
            <ta e="T932" id="Seg_7105" s="T931">dĭ-Tə</ta>
            <ta e="T933" id="Seg_7106" s="T932">măn-bi-m</ta>
            <ta e="T934" id="Seg_7107" s="T933">dĭ</ta>
            <ta e="T935" id="Seg_7108" s="T934">tože</ta>
            <ta e="T936" id="Seg_7109" s="T935">pa-jəʔ-Tə</ta>
            <ta e="T937" id="Seg_7110" s="T936">sʼa-liA</ta>
            <ta e="T939" id="Seg_7111" s="T938">i</ta>
            <ta e="T940" id="Seg_7112" s="T939">pa</ta>
            <ta e="T941" id="Seg_7113" s="T940">băldə-liA</ta>
            <ta e="T942" id="Seg_7114" s="T941">i</ta>
            <ta e="T943" id="Seg_7115" s="T942">tʼo-Tə</ta>
            <ta e="T944" id="Seg_7116" s="T943">baʔdə-liA</ta>
            <ta e="T946" id="Seg_7117" s="T945">kădaʔ</ta>
            <ta e="T947" id="Seg_7118" s="T946">dĭn</ta>
            <ta e="T948" id="Seg_7119" s="T947">tʼăbaktər-zittə</ta>
            <ta e="T949" id="Seg_7120" s="T948">ešši-zAŋ</ta>
            <ta e="T950" id="Seg_7121" s="T949">bar</ta>
            <ta e="T951" id="Seg_7122" s="T950">kirgaːr-laʔbə</ta>
            <ta e="T953" id="Seg_7123" s="T952">šoška-jəʔ</ta>
            <ta e="T954" id="Seg_7124" s="T953">tože</ta>
            <ta e="T955" id="Seg_7125" s="T954">bar</ta>
            <ta e="T956" id="Seg_7126" s="T955">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T958" id="Seg_7127" s="T957">tože</ta>
            <ta e="T959" id="Seg_7128" s="T958">müre-r-laʔbə</ta>
            <ta e="T961" id="Seg_7129" s="T960">kădaʔ=də</ta>
            <ta e="T962" id="Seg_7130" s="T961">nʼelʼzʼa</ta>
            <ta e="T963" id="Seg_7131" s="T962">tʼăbaktər-zittə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_7132" s="T1">this.[NOM.SG]</ta>
            <ta e="T3" id="Seg_7133" s="T2">you.PL.NOM</ta>
            <ta e="T4" id="Seg_7134" s="T3">camera.[NOM.SG]</ta>
            <ta e="T5" id="Seg_7135" s="T4">%%</ta>
            <ta e="T7" id="Seg_7136" s="T6">get.tired-MOM-PST.[3SG]</ta>
            <ta e="T8" id="Seg_7137" s="T7">PTCL</ta>
            <ta e="T11" id="Seg_7138" s="T10">lie-INF.LAT</ta>
            <ta e="T13" id="Seg_7139" s="T12">very</ta>
            <ta e="T14" id="Seg_7140" s="T13">lazy.[NOM.SG]</ta>
            <ta e="T15" id="Seg_7141" s="T14">become-RES-PST.[3SG]</ta>
            <ta e="T17" id="Seg_7142" s="T16">you.NOM</ta>
            <ta e="T18" id="Seg_7143" s="T17">very</ta>
            <ta e="T19" id="Seg_7144" s="T18">%%</ta>
            <ta e="T20" id="Seg_7145" s="T19">man.[NOM.SG]</ta>
            <ta e="T21" id="Seg_7146" s="T20">you.DAT</ta>
            <ta e="T22" id="Seg_7147" s="T21">say-PRS-2SG</ta>
            <ta e="T23" id="Seg_7148" s="T22">say-PRS-2SG</ta>
            <ta e="T24" id="Seg_7149" s="T23">and</ta>
            <ta e="T25" id="Seg_7150" s="T24">you.NOM</ta>
            <ta e="T26" id="Seg_7151" s="T25">and</ta>
            <ta e="T27" id="Seg_7152" s="T26">you.NOM</ta>
            <ta e="T28" id="Seg_7153" s="T27">what.[NOM.SG]=INDEF</ta>
            <ta e="T29" id="Seg_7154" s="T28">NEG</ta>
            <ta e="T30" id="Seg_7155" s="T29">know-PRS-2SG</ta>
            <ta e="T32" id="Seg_7156" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_7157" s="T32">you.DAT</ta>
            <ta e="T34" id="Seg_7158" s="T33">say-PRS-1SG</ta>
            <ta e="T35" id="Seg_7159" s="T34">small.[NOM.SG]</ta>
            <ta e="T36" id="Seg_7160" s="T35">be-PST-1SG</ta>
            <ta e="T38" id="Seg_7161" s="T37">what.kind=INDEF</ta>
            <ta e="T39" id="Seg_7162" s="T38">people.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7163" s="T39">come-PST-3PL</ta>
            <ta e="T45" id="Seg_7164" s="T44">NEG</ta>
            <ta e="T46" id="Seg_7165" s="T45">know-PRS-1SG</ta>
            <ta e="T47" id="Seg_7166" s="T46">small.[NOM.SG]</ta>
            <ta e="T48" id="Seg_7167" s="T47">be-PST-1SG</ta>
            <ta e="T49" id="Seg_7168" s="T48">NEG</ta>
            <ta e="T50" id="Seg_7169" s="T49">ask-PST-1SG</ta>
            <ta e="T52" id="Seg_7170" s="T51">I.LAT</ta>
            <ta e="T53" id="Seg_7171" s="T52">NEG</ta>
            <ta e="T54" id="Seg_7172" s="T53">tell-PST-3PL</ta>
            <ta e="T56" id="Seg_7173" s="T55">Vlas-LOC</ta>
            <ta e="T61" id="Seg_7174" s="T60">then</ta>
            <ta e="T62" id="Seg_7175" s="T61">this-GEN.PL</ta>
            <ta e="T63" id="Seg_7176" s="T62">be-PST.[3SG]</ta>
            <ta e="T65" id="Seg_7177" s="T64">and</ta>
            <ta e="T66" id="Seg_7178" s="T65">what.[NOM.SG]</ta>
            <ta e="T67" id="Seg_7179" s="T66">also</ta>
            <ta e="T68" id="Seg_7180" s="T67">NEG</ta>
            <ta e="T69" id="Seg_7181" s="T68">know-PRS-1SG</ta>
            <ta e="T70" id="Seg_7182" s="T69">I.LAT</ta>
            <ta e="T71" id="Seg_7183" s="T70">show-PST-3PL</ta>
            <ta e="T72" id="Seg_7184" s="T71">needle.[NOM.SG]</ta>
            <ta e="T74" id="Seg_7185" s="T73">push-MOM-PST-3PL</ta>
            <ta e="T75" id="Seg_7186" s="T74">wall-LAT</ta>
            <ta e="T77" id="Seg_7187" s="T76">far-LAT.ADV</ta>
            <ta e="T78" id="Seg_7188" s="T77">so</ta>
            <ta e="T80" id="Seg_7189" s="T79">sazhen-NOM/GEN/ACC.3SG</ta>
            <ta e="T81" id="Seg_7190" s="T80">two.[NOM.SG]</ta>
            <ta e="T82" id="Seg_7191" s="T81">ten.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7192" s="T83">then</ta>
            <ta e="T85" id="Seg_7193" s="T84">I.NOM</ta>
            <ta e="T86" id="Seg_7194" s="T85">look-FRQ-PST-1SG</ta>
            <ta e="T88" id="Seg_7195" s="T87">very</ta>
            <ta e="T90" id="Seg_7196" s="T89">needle.[NOM.SG]</ta>
            <ta e="T91" id="Seg_7197" s="T90">big.[NOM.SG]</ta>
            <ta e="T93" id="Seg_7198" s="T92">and</ta>
            <ta e="T94" id="Seg_7199" s="T93">then</ta>
            <ta e="T95" id="Seg_7200" s="T94">return-MOM-PST-3PL</ta>
            <ta e="T96" id="Seg_7201" s="T95">other</ta>
            <ta e="T97" id="Seg_7202" s="T96">end-INS</ta>
            <ta e="T99" id="Seg_7203" s="T98">look-PST-1SG</ta>
            <ta e="T100" id="Seg_7204" s="T99">very</ta>
            <ta e="T101" id="Seg_7205" s="T100">far-LAT.ADV</ta>
            <ta e="T102" id="Seg_7206" s="T101">become-RES-PST.[3SG]</ta>
            <ta e="T104" id="Seg_7207" s="T103">then</ta>
            <ta e="T105" id="Seg_7208" s="T104">paper.[NOM.SG]</ta>
            <ta e="T106" id="Seg_7209" s="T105">put-PST-3PL</ta>
            <ta e="T108" id="Seg_7210" s="T107">and</ta>
            <ta e="T109" id="Seg_7211" s="T108">set.up-PST-3PL</ta>
            <ta e="T110" id="Seg_7212" s="T109">this.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7213" s="T110">binocular</ta>
            <ta e="T112" id="Seg_7214" s="T111">sun-LAT</ta>
            <ta e="T114" id="Seg_7215" s="T113">and</ta>
            <ta e="T115" id="Seg_7216" s="T114">paper.[NOM.SG]</ta>
            <ta e="T116" id="Seg_7217" s="T115">%burn-MOM-PST.[3SG]</ta>
            <ta e="T118" id="Seg_7218" s="T117">you.PL.NOM</ta>
            <ta e="T119" id="Seg_7219" s="T118">when</ta>
            <ta e="T120" id="Seg_7220" s="T119">go-FUT-2PL</ta>
            <ta e="T122" id="Seg_7221" s="T121">%%-LAT</ta>
            <ta e="T123" id="Seg_7222" s="T122">go-FUT-1PL</ta>
            <ta e="T125" id="Seg_7223" s="T124">well</ta>
            <ta e="T126" id="Seg_7224" s="T125">go-IMP.2PL</ta>
            <ta e="T128" id="Seg_7225" s="T127">regards.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7226" s="T128">tell-IMP.2PL</ta>
            <ta e="T131" id="Seg_7227" s="T130">Matveev-LAT/LOC.3SG</ta>
            <ta e="T133" id="Seg_7228" s="T132">and</ta>
            <ta e="T134" id="Seg_7229" s="T133">Elya.[NOM.SG]</ta>
            <ta e="T135" id="Seg_7230" s="T134">today</ta>
            <ta e="T964" id="Seg_7231" s="T135">go-CVB</ta>
            <ta e="T136" id="Seg_7232" s="T964">disappear-PST.[3SG]</ta>
            <ta e="T137" id="Seg_7233" s="T136">Malinovka-LAT</ta>
            <ta e="T139" id="Seg_7234" s="T138">paper.[NOM.SG]</ta>
            <ta e="T140" id="Seg_7235" s="T139">bring-%%-PST.[3SG]</ta>
            <ta e="T142" id="Seg_7236" s="T141">chairman-LAT</ta>
            <ta e="T143" id="Seg_7237" s="T142">one.should</ta>
            <ta e="T144" id="Seg_7238" s="T143">stamp-PL</ta>
            <ta e="T145" id="Seg_7239" s="T144">put-INF.LAT</ta>
            <ta e="T147" id="Seg_7240" s="T146">this.[NOM.SG]</ta>
            <ta e="T148" id="Seg_7241" s="T147">go-PST.[3SG]</ta>
            <ta e="T149" id="Seg_7242" s="T148">there</ta>
            <ta e="T150" id="Seg_7243" s="T149">machine-PL-INS</ta>
            <ta e="T152" id="Seg_7244" s="T151">and</ta>
            <ta e="T154" id="Seg_7245" s="T153">from.there</ta>
            <ta e="T155" id="Seg_7246" s="T154">return-FUT-3SG</ta>
            <ta e="T156" id="Seg_7247" s="T155">on.foot-LAT.ADV</ta>
            <ta e="T158" id="Seg_7248" s="T157">I.NOM</ta>
            <ta e="T159" id="Seg_7249" s="T158">sister-LAT</ta>
            <ta e="T160" id="Seg_7250" s="T159">son-NOM/GEN.3SG</ta>
            <ta e="T965" id="Seg_7251" s="T160">go-CVB</ta>
            <ta e="T161" id="Seg_7252" s="T965">disappear-PST.[3SG]</ta>
            <ta e="T162" id="Seg_7253" s="T161">grass-EP-VBLZ-CVB</ta>
            <ta e="T163" id="Seg_7254" s="T162">cow-PL</ta>
            <ta e="T165" id="Seg_7255" s="T164">bring-INF.LAT</ta>
            <ta e="T167" id="Seg_7256" s="T166">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T168" id="Seg_7257" s="T167">son-NOM/GEN.3SG</ta>
            <ta e="T169" id="Seg_7258" s="T168">take-PST.[3SG]</ta>
            <ta e="T171" id="Seg_7259" s="T170">soon</ta>
            <ta e="T173" id="Seg_7260" s="T172">return-FUT-3PL</ta>
            <ta e="T174" id="Seg_7261" s="T173">house-LAT/LOC.3SG</ta>
            <ta e="T176" id="Seg_7262" s="T175">one.[NOM.SG]</ta>
            <ta e="T177" id="Seg_7263" s="T176">man.[NOM.SG]</ta>
            <ta e="T178" id="Seg_7264" s="T177">think-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_7265" s="T178">house.[NOM.SG]</ta>
            <ta e="T181" id="Seg_7266" s="T180">make-INF.LAT</ta>
            <ta e="T183" id="Seg_7267" s="T182">gold-3PL-INS</ta>
            <ta e="T185" id="Seg_7268" s="T184">and</ta>
            <ta e="T186" id="Seg_7269" s="T185">another.[NOM.SG]</ta>
            <ta e="T187" id="Seg_7270" s="T186">man.[NOM.SG]</ta>
            <ta e="T188" id="Seg_7271" s="T187">house.[NOM.SG]</ta>
            <ta e="T189" id="Seg_7272" s="T188">make-INF.LAT</ta>
            <ta e="T190" id="Seg_7273" s="T189">silver-ABL</ta>
            <ta e="T191" id="Seg_7274" s="T190">and</ta>
            <ta e="T192" id="Seg_7275" s="T191">three.[NOM.SG]</ta>
            <ta e="T193" id="Seg_7276" s="T192">man.[NOM.SG]</ta>
            <ta e="T194" id="Seg_7277" s="T193">PTCL</ta>
            <ta e="T195" id="Seg_7278" s="T194">%%</ta>
            <ta e="T196" id="Seg_7279" s="T195">say-IPFVZ.[3SG]</ta>
            <ta e="T197" id="Seg_7280" s="T196">I.NOM</ta>
            <ta e="T198" id="Seg_7281" s="T197">tree-3PL-INS</ta>
            <ta e="T199" id="Seg_7282" s="T198">cut-FUT-1SG</ta>
            <ta e="T200" id="Seg_7283" s="T199">house.[NOM.SG]</ta>
            <ta e="T202" id="Seg_7284" s="T201">and</ta>
            <ta e="T203" id="Seg_7285" s="T202">four.[NOM.SG]</ta>
            <ta e="T204" id="Seg_7286" s="T203">man.[NOM.SG]</ta>
            <ta e="T205" id="Seg_7287" s="T204">I.NOM</ta>
            <ta e="T206" id="Seg_7288" s="T205">make-FUT-1SG</ta>
            <ta e="T207" id="Seg_7289" s="T206">grass-ABL</ta>
            <ta e="T208" id="Seg_7290" s="T207">straw-PL-INS</ta>
            <ta e="T211" id="Seg_7291" s="T210">come-FUT-3SG</ta>
            <ta e="T212" id="Seg_7292" s="T211">PTCL</ta>
            <ta e="T213" id="Seg_7293" s="T212">work-PTCP.PRS</ta>
            <ta e="T215" id="Seg_7294" s="T214">burn-FUT-3SG</ta>
            <ta e="T217" id="Seg_7295" s="T216">who-GEN</ta>
            <ta e="T218" id="Seg_7296" s="T217">work-PTCP.PRS</ta>
            <ta e="T219" id="Seg_7297" s="T218">NEG</ta>
            <ta e="T220" id="Seg_7298" s="T219">burn-FUT-1SG</ta>
            <ta e="T222" id="Seg_7299" s="T221">this.[NOM.SG]</ta>
            <ta e="T223" id="Seg_7300" s="T222">this-LAT</ta>
            <ta e="T224" id="Seg_7301" s="T223">good.[NOM.SG]</ta>
            <ta e="T225" id="Seg_7302" s="T224">become-FUT-3SG</ta>
            <ta e="T227" id="Seg_7303" s="T226">and</ta>
            <ta e="T228" id="Seg_7304" s="T227">who-GEN</ta>
            <ta e="T229" id="Seg_7305" s="T228">work-PTCP.PRS</ta>
            <ta e="T231" id="Seg_7306" s="T230">burn-MOM-FUT-3SG</ta>
            <ta e="T233" id="Seg_7307" s="T232">this-LAT</ta>
            <ta e="T234" id="Seg_7308" s="T233">NEG</ta>
            <ta e="T235" id="Seg_7309" s="T234">good.[NOM.SG]</ta>
            <ta e="T236" id="Seg_7310" s="T235">become-FUT-3SG</ta>
            <ta e="T238" id="Seg_7311" s="T237">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T239" id="Seg_7312" s="T238">NEG</ta>
            <ta e="T240" id="Seg_7313" s="T239">burn-FUT-2SG</ta>
            <ta e="T241" id="Seg_7314" s="T240">run-MOM-FUT-3SG</ta>
            <ta e="T243" id="Seg_7315" s="T242">and</ta>
            <ta e="T244" id="Seg_7316" s="T243">what.[NOM.SG]</ta>
            <ta e="T246" id="Seg_7317" s="T245">and</ta>
            <ta e="T247" id="Seg_7318" s="T246">what.[NOM.SG]</ta>
            <ta e="T248" id="Seg_7319" s="T247">work-FUT.[3SG]</ta>
            <ta e="T249" id="Seg_7320" s="T248">burn-MOM-PST.[3SG]</ta>
            <ta e="T251" id="Seg_7321" s="T250">live-PST-3PL</ta>
            <ta e="T252" id="Seg_7322" s="T251">three.[NOM.SG]</ta>
            <ta e="T253" id="Seg_7323" s="T252">pig-PL</ta>
            <ta e="T254" id="Seg_7324" s="T253">live-PST-3PL</ta>
            <ta e="T256" id="Seg_7325" s="T255">then</ta>
            <ta e="T257" id="Seg_7326" s="T256">two.[NOM.SG]</ta>
            <ta e="T258" id="Seg_7327" s="T257">pig.[NOM.SG]</ta>
            <ta e="T259" id="Seg_7328" s="T258">very</ta>
            <ta e="T260" id="Seg_7329" s="T259">lazy.[NOM.SG]</ta>
            <ta e="T261" id="Seg_7330" s="T260">be-PST-3PL</ta>
            <ta e="T262" id="Seg_7331" s="T261">and</ta>
            <ta e="T263" id="Seg_7332" s="T262">one.[NOM.SG]</ta>
            <ta e="T264" id="Seg_7333" s="T263">pig.[NOM.SG]</ta>
            <ta e="T265" id="Seg_7334" s="T264">always</ta>
            <ta e="T266" id="Seg_7335" s="T265">work-PST.[3SG]</ta>
            <ta e="T268" id="Seg_7336" s="T267">this-PL</ta>
            <ta e="T269" id="Seg_7337" s="T268">PTCL</ta>
            <ta e="T270" id="Seg_7338" s="T269">visit-CVB</ta>
            <ta e="T271" id="Seg_7339" s="T270">go-PST-3PL</ta>
            <ta e="T272" id="Seg_7340" s="T271">vodka.[NOM.SG]</ta>
            <ta e="T273" id="Seg_7341" s="T272">drink-PST-3PL</ta>
            <ta e="T275" id="Seg_7342" s="T274">and</ta>
            <ta e="T276" id="Seg_7343" s="T275">that.[NOM.SG]</ta>
            <ta e="T277" id="Seg_7344" s="T276">one.[NOM.SG]</ta>
            <ta e="T278" id="Seg_7345" s="T277">pig.[NOM.SG]</ta>
            <ta e="T279" id="Seg_7346" s="T278">PTCL</ta>
            <ta e="T280" id="Seg_7347" s="T279">dig-PST.[3SG]</ta>
            <ta e="T281" id="Seg_7348" s="T280">earth.[NOM.SG]</ta>
            <ta e="T283" id="Seg_7349" s="T282">stone-PL</ta>
            <ta e="T284" id="Seg_7350" s="T283">gather-PST-3PL</ta>
            <ta e="T286" id="Seg_7351" s="T285">and</ta>
            <ta e="T287" id="Seg_7352" s="T286">house.[NOM.SG]</ta>
            <ta e="T288" id="Seg_7353" s="T287">self-LAT/LOC.3SG</ta>
            <ta e="T289" id="Seg_7354" s="T288">make-PST.[3SG]</ta>
            <ta e="T291" id="Seg_7355" s="T290">stone-PL-LAT</ta>
            <ta e="T294" id="Seg_7356" s="T293">this-PL</ta>
            <ta e="T295" id="Seg_7357" s="T294">come-PST-3PL</ta>
            <ta e="T296" id="Seg_7358" s="T295">one.[NOM.SG]</ta>
            <ta e="T297" id="Seg_7359" s="T296">put-PST.[3SG]</ta>
            <ta e="T299" id="Seg_7360" s="T298">branch-PL</ta>
            <ta e="T300" id="Seg_7361" s="T299">collect-PST.[3SG]</ta>
            <ta e="T301" id="Seg_7362" s="T300">and</ta>
            <ta e="T302" id="Seg_7363" s="T301">put-PST.[3SG]</ta>
            <ta e="T303" id="Seg_7364" s="T302">and</ta>
            <ta e="T304" id="Seg_7365" s="T303">one.[NOM.SG]</ta>
            <ta e="T305" id="Seg_7366" s="T304">straw-PL-INS</ta>
            <ta e="T307" id="Seg_7367" s="T306">put-PST.[3SG]</ta>
            <ta e="T308" id="Seg_7368" s="T307">self-LAT/LOC.3SG</ta>
            <ta e="T309" id="Seg_7369" s="T308">house.[NOM.SG]</ta>
            <ta e="T311" id="Seg_7370" s="T310">and</ta>
            <ta e="T312" id="Seg_7371" s="T311">this.[NOM.SG]</ta>
            <ta e="T313" id="Seg_7372" s="T312">always</ta>
            <ta e="T314" id="Seg_7373" s="T313">stone-ACC</ta>
            <ta e="T315" id="Seg_7374" s="T314">stone-PL-INS</ta>
            <ta e="T316" id="Seg_7375" s="T315">put-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_7376" s="T316">this</ta>
            <ta e="T318" id="Seg_7377" s="T317">this-PL</ta>
            <ta e="T319" id="Seg_7378" s="T318">come-PST-3PL</ta>
            <ta e="T320" id="Seg_7379" s="T319">what.[NOM.SG]</ta>
            <ta e="T321" id="Seg_7380" s="T320">you.NOM</ta>
            <ta e="T322" id="Seg_7381" s="T321">work-PRS-2SG</ta>
            <ta e="T324" id="Seg_7382" s="T323">we.NOM</ta>
            <ta e="T325" id="Seg_7383" s="T324">soon-LOC.ADV</ta>
            <ta e="T327" id="Seg_7384" s="T326">make-PST-1PL</ta>
            <ta e="T329" id="Seg_7385" s="T328">house-NOM/GEN/ACC.3SG</ta>
            <ta e="T331" id="Seg_7386" s="T330">and</ta>
            <ta e="T333" id="Seg_7387" s="T332">who-GEN</ta>
            <ta e="T334" id="Seg_7388" s="T333">good.[NOM.SG]</ta>
            <ta e="T335" id="Seg_7389" s="T334">house-NOM/GEN/ACC.3SG</ta>
            <ta e="T336" id="Seg_7390" s="T335">become-FUT-3SG</ta>
            <ta e="T337" id="Seg_7391" s="T336">I.GEN</ta>
            <ta e="T338" id="Seg_7392" s="T337">or</ta>
            <ta e="T339" id="Seg_7393" s="T338">you.PL.NOM</ta>
            <ta e="T341" id="Seg_7394" s="T340">then</ta>
            <ta e="T342" id="Seg_7395" s="T341">cold.[NOM.SG]</ta>
            <ta e="T343" id="Seg_7396" s="T342">become-RES-PST.[3SG]</ta>
            <ta e="T344" id="Seg_7397" s="T343">snow.[NOM.SG]</ta>
            <ta e="T345" id="Seg_7398" s="T344">snow.[NOM.SG]</ta>
            <ta e="T346" id="Seg_7399" s="T345">come-PRS.[3SG]</ta>
            <ta e="T348" id="Seg_7400" s="T347">then</ta>
            <ta e="T349" id="Seg_7401" s="T348">this-PL</ta>
            <ta e="T351" id="Seg_7402" s="T350">creep.into-PST-3PL</ta>
            <ta e="T352" id="Seg_7403" s="T351">house-LAT</ta>
            <ta e="T353" id="Seg_7404" s="T352">what.kind</ta>
            <ta e="T354" id="Seg_7405" s="T353">straw.[NOM.SG]</ta>
            <ta e="T356" id="Seg_7406" s="T355">close-PST-3PL</ta>
            <ta e="T357" id="Seg_7407" s="T356">door.[NOM.SG]</ta>
            <ta e="T358" id="Seg_7408" s="T357">come-PST.[3SG]</ta>
            <ta e="T359" id="Seg_7409" s="T358">bear.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7410" s="T360">let-%%-IMP.2PL</ta>
            <ta e="T362" id="Seg_7411" s="T361">I.LAT</ta>
            <ta e="T364" id="Seg_7412" s="T363">this-PL</ta>
            <ta e="T365" id="Seg_7413" s="T364">NEG</ta>
            <ta e="T366" id="Seg_7414" s="T365">let-FUT-CNG</ta>
            <ta e="T367" id="Seg_7415" s="T366">soon</ta>
            <ta e="T368" id="Seg_7416" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_7417" s="T368">break-FUT-1SG</ta>
            <ta e="T370" id="Seg_7418" s="T369">you.PL.NOM</ta>
            <ta e="T371" id="Seg_7419" s="T370">house-ABL</ta>
            <ta e="T372" id="Seg_7420" s="T371">house-LAT</ta>
            <ta e="T374" id="Seg_7421" s="T373">then</ta>
            <ta e="T375" id="Seg_7422" s="T374">break-PST.[3SG]</ta>
            <ta e="T376" id="Seg_7423" s="T375">this-PL</ta>
            <ta e="T377" id="Seg_7424" s="T376">run-MOM-PST-3PL</ta>
            <ta e="T379" id="Seg_7425" s="T378">what.kind</ta>
            <ta e="T380" id="Seg_7426" s="T379">house.[NOM.SG]</ta>
            <ta e="T382" id="Seg_7427" s="T381">branch-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T383" id="Seg_7428" s="T382">make-PST-3PL</ta>
            <ta e="T385" id="Seg_7429" s="T384">and</ta>
            <ta e="T386" id="Seg_7430" s="T385">there</ta>
            <ta e="T387" id="Seg_7431" s="T386">door.[NOM.SG]</ta>
            <ta e="T389" id="Seg_7432" s="T388">close-MOM-PST-3PL</ta>
            <ta e="T390" id="Seg_7433" s="T389">this.[NOM.SG]</ta>
            <ta e="T391" id="Seg_7434" s="T390">there</ta>
            <ta e="T392" id="Seg_7435" s="T391">come-PST.[3SG]</ta>
            <ta e="T394" id="Seg_7436" s="T393">open-IMP.2PL</ta>
            <ta e="T396" id="Seg_7437" s="T395">otherwise</ta>
            <ta e="T397" id="Seg_7438" s="T396">soon</ta>
            <ta e="T398" id="Seg_7439" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_7440" s="T398">fall-MOM-FUT-3SG</ta>
            <ta e="T400" id="Seg_7441" s="T399">house.[NOM.SG]</ta>
            <ta e="T402" id="Seg_7442" s="T401">then</ta>
            <ta e="T403" id="Seg_7443" s="T402">PTCL</ta>
            <ta e="T404" id="Seg_7444" s="T403">throw.away-PST.[3SG]</ta>
            <ta e="T405" id="Seg_7445" s="T404">this-PL</ta>
            <ta e="T406" id="Seg_7446" s="T405">run-MOM-PST-3PL</ta>
            <ta e="T408" id="Seg_7447" s="T407">and</ta>
            <ta e="T409" id="Seg_7448" s="T408">come-PST-3PL</ta>
            <ta e="T410" id="Seg_7449" s="T409">here</ta>
            <ta e="T411" id="Seg_7450" s="T410">one-LAT</ta>
            <ta e="T413" id="Seg_7451" s="T412">three-LAT</ta>
            <ta e="T414" id="Seg_7452" s="T413">pig-LAT/LOC.3SG</ta>
            <ta e="T415" id="Seg_7453" s="T414">come-PST-3PL</ta>
            <ta e="T417" id="Seg_7454" s="T416">let-HAB-IMP.2SG</ta>
            <ta e="T418" id="Seg_7455" s="T417">we.LAT</ta>
            <ta e="T419" id="Seg_7456" s="T418">this.[NOM.SG]</ta>
            <ta e="T420" id="Seg_7457" s="T419">let-SEM-PST.[3SG]</ta>
            <ta e="T421" id="Seg_7458" s="T420">and</ta>
            <ta e="T422" id="Seg_7459" s="T421">door-ACC.3SG</ta>
            <ta e="T424" id="Seg_7460" s="T423">close-PST.[3SG]</ta>
            <ta e="T425" id="Seg_7461" s="T424">this.[NOM.SG]</ta>
            <ta e="T426" id="Seg_7462" s="T425">come-PST.[3SG]</ta>
            <ta e="T427" id="Seg_7463" s="T426">bear.[NOM.SG]</ta>
            <ta e="T428" id="Seg_7464" s="T427">let-HAB-IMP.2SG</ta>
            <ta e="T430" id="Seg_7465" s="T429">NEG</ta>
            <ta e="T431" id="Seg_7466" s="T430">let-FUT-1SG</ta>
            <ta e="T433" id="Seg_7467" s="T432">then</ta>
            <ta e="T434" id="Seg_7468" s="T433">this.[NOM.SG]</ta>
            <ta e="T435" id="Seg_7469" s="T434">break-INF.LAT</ta>
            <ta e="T436" id="Seg_7470" s="T435">want.PST.M.SG</ta>
            <ta e="T437" id="Seg_7471" s="T436">NEG</ta>
            <ta e="T438" id="Seg_7472" s="T437">can-PST.[3SG]</ta>
            <ta e="T439" id="Seg_7473" s="T438">then</ta>
            <ta e="T441" id="Seg_7474" s="T440">climb-PST.[3SG]</ta>
            <ta e="T442" id="Seg_7475" s="T441">there</ta>
            <ta e="T444" id="Seg_7476" s="T443">up</ta>
            <ta e="T445" id="Seg_7477" s="T444">pipe-LAT/LOC.3SG</ta>
            <ta e="T447" id="Seg_7478" s="T446">climb-DUR-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_7479" s="T447">and</ta>
            <ta e="T449" id="Seg_7480" s="T448">this.[NOM.SG]</ta>
            <ta e="T450" id="Seg_7481" s="T449">cauldron.[NOM.SG]</ta>
            <ta e="T451" id="Seg_7482" s="T450">place-PST.[3SG]</ta>
            <ta e="T452" id="Seg_7483" s="T451">warm.[NOM.SG]</ta>
            <ta e="T453" id="Seg_7484" s="T452">water-INS</ta>
            <ta e="T455" id="Seg_7485" s="T454">this.[NOM.SG]</ta>
            <ta e="T456" id="Seg_7486" s="T455">PTCL</ta>
            <ta e="T457" id="Seg_7487" s="T456">fall-MOM-PST.[3SG]</ta>
            <ta e="T458" id="Seg_7488" s="T457">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T460" id="Seg_7489" s="T459">and</ta>
            <ta e="T462" id="Seg_7490" s="T461">then</ta>
            <ta e="T463" id="Seg_7491" s="T462">return-MOM-PST.[3SG]</ta>
            <ta e="T464" id="Seg_7492" s="T463">and</ta>
            <ta e="T966" id="Seg_7493" s="T464">go-CVB</ta>
            <ta e="T465" id="Seg_7494" s="T966">disappear-PST.[3SG]</ta>
            <ta e="T466" id="Seg_7495" s="T465">and</ta>
            <ta e="T467" id="Seg_7496" s="T466">else</ta>
            <ta e="T468" id="Seg_7497" s="T467">when=INDEF</ta>
            <ta e="T469" id="Seg_7498" s="T468">NEG</ta>
            <ta e="T470" id="Seg_7499" s="T469">come-PST.[3SG]</ta>
            <ta e="T473" id="Seg_7500" s="T472">burn-PST-3PL</ta>
            <ta e="T474" id="Seg_7501" s="T473">this-ACC</ta>
            <ta e="T476" id="Seg_7502" s="T475">and</ta>
            <ta e="T967" id="Seg_7503" s="T476">go-CVB</ta>
            <ta e="T477" id="Seg_7504" s="T967">disappear-PST.[3SG]</ta>
            <ta e="T479" id="Seg_7505" s="T478">how.much</ta>
            <ta e="T481" id="Seg_7506" s="T480">we.NOM</ta>
            <ta e="T482" id="Seg_7507" s="T481">tape-PL</ta>
            <ta e="T483" id="Seg_7508" s="T482">speak-PST-1SG</ta>
            <ta e="T484" id="Seg_7509" s="T483">speak-PST-1PL</ta>
            <ta e="T486" id="Seg_7510" s="T485">ten.[NOM.SG]</ta>
            <ta e="T487" id="Seg_7511" s="T486">three.[NOM.SG]</ta>
            <ta e="T489" id="Seg_7512" s="T488">you.PL.GEN</ta>
            <ta e="T490" id="Seg_7513" s="T489">bread-NOM/GEN/ACC.2PL</ta>
            <ta e="T491" id="Seg_7514" s="T490">be-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_7515" s="T492">be-PRS.[3SG]</ta>
            <ta e="T494" id="Seg_7516" s="T493">and</ta>
            <ta e="T495" id="Seg_7517" s="T494">where</ta>
            <ta e="T496" id="Seg_7518" s="T495">take-PST-2PL</ta>
            <ta e="T498" id="Seg_7519" s="T497">PTCL</ta>
            <ta e="T499" id="Seg_7520" s="T498">one.[NOM.SG]</ta>
            <ta e="T500" id="Seg_7521" s="T499">woman.[NOM.SG]</ta>
            <ta e="T501" id="Seg_7522" s="T500">bring-PST.[3SG]</ta>
            <ta e="T504" id="Seg_7523" s="T503">Voznesenskoe-PL-INS</ta>
            <ta e="T506" id="Seg_7524" s="T505">one.[NOM.SG]</ta>
            <ta e="T507" id="Seg_7525" s="T506">goat-GEN</ta>
            <ta e="T508" id="Seg_7526" s="T507">five.[NOM.SG]</ta>
            <ta e="T509" id="Seg_7527" s="T508">child-PL-LAT</ta>
            <ta e="T510" id="Seg_7528" s="T509">be-PST-3PL</ta>
            <ta e="T512" id="Seg_7529" s="T511">this.[NOM.SG]</ta>
            <ta e="T513" id="Seg_7530" s="T512">this-ACC.PL</ta>
            <ta e="T514" id="Seg_7531" s="T513">house-LAT-%%</ta>
            <ta e="T515" id="Seg_7532" s="T514">leave-PST.[3SG]</ta>
            <ta e="T517" id="Seg_7533" s="T516">door-ACC</ta>
            <ta e="T518" id="Seg_7534" s="T517">close-PST.[3SG]</ta>
            <ta e="T520" id="Seg_7535" s="T519">who-ACC=INDEF</ta>
            <ta e="T521" id="Seg_7536" s="T520">NEG.AUX-IMP.2SG</ta>
            <ta e="T523" id="Seg_7537" s="T522">NEG.AUX-IMP.2SG</ta>
            <ta e="T524" id="Seg_7538" s="T523">let-%%-CNG</ta>
            <ta e="T526" id="Seg_7539" s="T525">then</ta>
            <ta e="T968" id="Seg_7540" s="T526">go-CVB</ta>
            <ta e="T527" id="Seg_7541" s="T968">disappear-PST.[3SG]</ta>
            <ta e="T529" id="Seg_7542" s="T528">bear.[NOM.SG]</ta>
            <ta e="T530" id="Seg_7543" s="T529">come-PST.[3SG]</ta>
            <ta e="T531" id="Seg_7544" s="T530">open-IMP.2PL.O</ta>
            <ta e="T532" id="Seg_7545" s="T531">door.[NOM.SG]</ta>
            <ta e="T534" id="Seg_7546" s="T533">this-PL</ta>
            <ta e="T535" id="Seg_7547" s="T534">NEG</ta>
            <ta e="T536" id="Seg_7548" s="T535">open-RES-3PL</ta>
            <ta e="T538" id="Seg_7549" s="T537">we.LAT</ta>
            <ta e="T539" id="Seg_7550" s="T538">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T540" id="Seg_7551" s="T539">say-PST.[3SG]</ta>
            <ta e="T541" id="Seg_7552" s="T540">who-ACC=INDEF</ta>
            <ta e="T542" id="Seg_7553" s="T541">NEG</ta>
            <ta e="T543" id="Seg_7554" s="T542">let-%%-INF.LAT</ta>
            <ta e="T545" id="Seg_7555" s="T544">then</ta>
            <ta e="T546" id="Seg_7556" s="T545">bear.[NOM.SG]</ta>
            <ta e="T969" id="Seg_7557" s="T546">go-CVB</ta>
            <ta e="T547" id="Seg_7558" s="T969">disappear-PST.[3SG]</ta>
            <ta e="T548" id="Seg_7559" s="T547">mother-NOM/GEN.3SG</ta>
            <ta e="T549" id="Seg_7560" s="T548">come-PST.[3SG]</ta>
            <ta e="T551" id="Seg_7561" s="T550">open-IMP.2PL</ta>
            <ta e="T552" id="Seg_7562" s="T551">door.[NOM.SG]</ta>
            <ta e="T553" id="Seg_7563" s="T552">I.NOM</ta>
            <ta e="T554" id="Seg_7564" s="T553">bring-PST-1SG</ta>
            <ta e="T555" id="Seg_7565" s="T554">milk.[NOM.SG]</ta>
            <ta e="T557" id="Seg_7566" s="T556">bring-PST-1SG</ta>
            <ta e="T558" id="Seg_7567" s="T557">grass.[NOM.SG]</ta>
            <ta e="T559" id="Seg_7568" s="T558">you.PL.ACC</ta>
            <ta e="T560" id="Seg_7569" s="T559">curd.cheese</ta>
            <ta e="T561" id="Seg_7570" s="T560">bring-PST-1SG</ta>
            <ta e="T563" id="Seg_7571" s="T562">then</ta>
            <ta e="T564" id="Seg_7572" s="T563">this-PL</ta>
            <ta e="T565" id="Seg_7573" s="T564">open-PST-3PL</ta>
            <ta e="T566" id="Seg_7574" s="T565">eat-PST-3PL</ta>
            <ta e="T567" id="Seg_7575" s="T566">then</ta>
            <ta e="T568" id="Seg_7576" s="T567">mother-NOM/GEN.3SG</ta>
            <ta e="T569" id="Seg_7577" s="T568">again</ta>
            <ta e="T570" id="Seg_7578" s="T569">go-PST.[3SG]</ta>
            <ta e="T572" id="Seg_7579" s="T571">NEG.AUX-IMP.2SG</ta>
            <ta e="T573" id="Seg_7580" s="T572">let-%%-CNG</ta>
            <ta e="T574" id="Seg_7581" s="T573">who-ACC=INDEF</ta>
            <ta e="T576" id="Seg_7582" s="T575">again</ta>
            <ta e="T970" id="Seg_7583" s="T576">go-CVB</ta>
            <ta e="T577" id="Seg_7584" s="T970">disappear-PST.[3SG]</ta>
            <ta e="T579" id="Seg_7585" s="T578">who-ACC=INDEF</ta>
            <ta e="T580" id="Seg_7586" s="T579">NEG.AUX-IMP.2SG</ta>
            <ta e="T581" id="Seg_7587" s="T580">let-%%</ta>
            <ta e="T583" id="Seg_7588" s="T582">then</ta>
            <ta e="T584" id="Seg_7589" s="T583">again</ta>
            <ta e="T585" id="Seg_7590" s="T584">come-PST.[3SG]</ta>
            <ta e="T586" id="Seg_7591" s="T585">bear.[NOM.SG]</ta>
            <ta e="T588" id="Seg_7592" s="T587">child-PL</ta>
            <ta e="T589" id="Seg_7593" s="T588">child-PL</ta>
            <ta e="T590" id="Seg_7594" s="T589">let-2PL</ta>
            <ta e="T591" id="Seg_7595" s="T590">I.LAT</ta>
            <ta e="T593" id="Seg_7596" s="T592">no</ta>
            <ta e="T595" id="Seg_7597" s="T594">we.NOM</ta>
            <ta e="T596" id="Seg_7598" s="T595">NEG</ta>
            <ta e="T597" id="Seg_7599" s="T596">let-CVB</ta>
            <ta e="T599" id="Seg_7600" s="T598">and</ta>
            <ta e="T600" id="Seg_7601" s="T599">I.GEN</ta>
            <ta e="T601" id="Seg_7602" s="T600">you.PL.NOM</ta>
            <ta e="T602" id="Seg_7603" s="T601">mother</ta>
            <ta e="T603" id="Seg_7604" s="T602">be-PRS-1SG</ta>
            <ta e="T604" id="Seg_7605" s="T603">no</ta>
            <ta e="T605" id="Seg_7606" s="T604">I.GEN</ta>
            <ta e="T607" id="Seg_7607" s="T606">mother-GEN.1SG</ta>
            <ta e="T608" id="Seg_7608" s="T607">voice-NOM/GEN/ACC.3SG</ta>
            <ta e="T609" id="Seg_7609" s="T608">thin.[NOM.SG]</ta>
            <ta e="T611" id="Seg_7610" s="T610">and</ta>
            <ta e="T612" id="Seg_7611" s="T611">you.GEN</ta>
            <ta e="T613" id="Seg_7612" s="T612">voice-NOM/GEN/ACC.3SG</ta>
            <ta e="T614" id="Seg_7613" s="T613">very</ta>
            <ta e="T615" id="Seg_7614" s="T614">thick.[NOM.SG]</ta>
            <ta e="T617" id="Seg_7615" s="T616">then</ta>
            <ta e="T618" id="Seg_7616" s="T617">bear.[NOM.SG]</ta>
            <ta e="T971" id="Seg_7617" s="T618">go-CVB</ta>
            <ta e="T619" id="Seg_7618" s="T971">disappear-PST.[3SG]</ta>
            <ta e="T620" id="Seg_7619" s="T619">then</ta>
            <ta e="T621" id="Seg_7620" s="T620">mother-NOM/GEN.3SG</ta>
            <ta e="T622" id="Seg_7621" s="T621">come-PST.[3SG]</ta>
            <ta e="T624" id="Seg_7622" s="T623">this-PL</ta>
            <ta e="T625" id="Seg_7623" s="T624">open-PST-3PL</ta>
            <ta e="T626" id="Seg_7624" s="T625">feed-PST.[3SG]</ta>
            <ta e="T628" id="Seg_7625" s="T627">now</ta>
            <ta e="T629" id="Seg_7626" s="T628">who-LAT=INDEF</ta>
            <ta e="T630" id="Seg_7627" s="T629">NEG</ta>
            <ta e="T631" id="Seg_7628" s="T630">let-%%</ta>
            <ta e="T633" id="Seg_7629" s="T632">and</ta>
            <ta e="T634" id="Seg_7630" s="T633">bear.[NOM.SG]</ta>
            <ta e="T635" id="Seg_7631" s="T634">go-PST.[3SG]</ta>
            <ta e="T636" id="Seg_7632" s="T635">smithy-LAT</ta>
            <ta e="T638" id="Seg_7633" s="T637">language-ACC.3SG</ta>
            <ta e="T640" id="Seg_7634" s="T639">make-PST.[3SG]</ta>
            <ta e="T641" id="Seg_7635" s="T640">thin.[NOM.SG]</ta>
            <ta e="T643" id="Seg_7636" s="T642">then</ta>
            <ta e="T644" id="Seg_7637" s="T643">come-PST.[3SG]</ta>
            <ta e="T645" id="Seg_7638" s="T644">again</ta>
            <ta e="T647" id="Seg_7639" s="T646">this.[NOM.SG]</ta>
            <ta e="T648" id="Seg_7640" s="T647">say-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_7641" s="T648">I.NOM</ta>
            <ta e="T650" id="Seg_7642" s="T649">you.PL.NOM</ta>
            <ta e="T651" id="Seg_7643" s="T650">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T652" id="Seg_7644" s="T651">bring-PST-1SG</ta>
            <ta e="T653" id="Seg_7645" s="T652">milk.[NOM.SG]</ta>
            <ta e="T655" id="Seg_7646" s="T654">bread.[NOM.SG]</ta>
            <ta e="T656" id="Seg_7647" s="T655">bring-PST-1SG</ta>
            <ta e="T658" id="Seg_7648" s="T657">bring-PST-1SG</ta>
            <ta e="T659" id="Seg_7649" s="T658">you.PL.ACC</ta>
            <ta e="T661" id="Seg_7650" s="T660">grass.[NOM.SG]</ta>
            <ta e="T663" id="Seg_7651" s="T662">this-PL</ta>
            <ta e="T664" id="Seg_7652" s="T663">PTCL</ta>
            <ta e="T665" id="Seg_7653" s="T664">open-MOM-PST.[3SG]</ta>
            <ta e="T667" id="Seg_7654" s="T666">this.[NOM.SG]</ta>
            <ta e="T668" id="Seg_7655" s="T667">this-ACC.PL</ta>
            <ta e="T669" id="Seg_7656" s="T668">PTCL</ta>
            <ta e="T670" id="Seg_7657" s="T669">eat-MOM-PST.[3SG]</ta>
            <ta e="T672" id="Seg_7658" s="T671">mother-NOM/GEN.3SG</ta>
            <ta e="T673" id="Seg_7659" s="T672">come-PST.[3SG]</ta>
            <ta e="T675" id="Seg_7660" s="T674">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T676" id="Seg_7661" s="T675">NEG.EX.[3SG]</ta>
            <ta e="T677" id="Seg_7662" s="T676">one.[NOM.SG]</ta>
            <ta e="T680" id="Seg_7663" s="T679">small.[NOM.SG]</ta>
            <ta e="T681" id="Seg_7664" s="T680">hide-RES-PST.[3SG]</ta>
            <ta e="T683" id="Seg_7665" s="T682">then</ta>
            <ta e="T684" id="Seg_7666" s="T683">say-IPFVZ.[3SG]</ta>
            <ta e="T685" id="Seg_7667" s="T684">bear.[NOM.SG]</ta>
            <ta e="T686" id="Seg_7668" s="T685">all</ta>
            <ta e="T687" id="Seg_7669" s="T686">eat-MOM-PST.[3SG]</ta>
            <ta e="T689" id="Seg_7670" s="T688">I.NOM</ta>
            <ta e="T690" id="Seg_7671" s="T689">alone</ta>
            <ta e="T691" id="Seg_7672" s="T690">remain-MOM-PST-1SG</ta>
            <ta e="T692" id="Seg_7673" s="T691">stove-LAT</ta>
            <ta e="T698" id="Seg_7674" s="T697">stove-LAT</ta>
            <ta e="T699" id="Seg_7675" s="T698">climb-MOM-PST-1SG</ta>
            <ta e="T700" id="Seg_7676" s="T699">this.[NOM.SG]</ta>
            <ta e="T701" id="Seg_7677" s="T700">I.LAT</ta>
            <ta e="T702" id="Seg_7678" s="T701">NEG</ta>
            <ta e="T703" id="Seg_7679" s="T702">look-FRQ-PST.[3SG]</ta>
            <ta e="T972" id="Seg_7680" s="T703">go-CVB</ta>
            <ta e="T704" id="Seg_7681" s="T972">disappear-PST.[3SG]</ta>
            <ta e="T706" id="Seg_7682" s="T705">then</ta>
            <ta e="T707" id="Seg_7683" s="T706">bear.[NOM.SG]</ta>
            <ta e="T708" id="Seg_7684" s="T707">come-PST.[3SG]</ta>
            <ta e="T709" id="Seg_7685" s="T708">goat-LAT</ta>
            <ta e="T711" id="Seg_7686" s="T710">goat.[NOM.SG]</ta>
            <ta e="T712" id="Seg_7687" s="T711">say-IPFVZ.[3SG]</ta>
            <ta e="T715" id="Seg_7688" s="T714">go-OPT.DU/PL-1DU</ta>
            <ta e="T717" id="Seg_7689" s="T716">jump-1DU</ta>
            <ta e="T718" id="Seg_7690" s="T717">that.[NOM.SG]</ta>
            <ta e="T719" id="Seg_7691" s="T718">pit-ABL</ta>
            <ta e="T722" id="Seg_7692" s="T721">goat.[NOM.SG]</ta>
            <ta e="T723" id="Seg_7693" s="T722">jump-PST.[3SG]</ta>
            <ta e="T726" id="Seg_7694" s="T725">and</ta>
            <ta e="T727" id="Seg_7695" s="T726">this.[NOM.SG]</ta>
            <ta e="T728" id="Seg_7696" s="T727">NEG</ta>
            <ta e="T729" id="Seg_7697" s="T728">can-PST.[3SG]</ta>
            <ta e="T730" id="Seg_7698" s="T729">jump-OPT.SG.[3SG]</ta>
            <ta e="T731" id="Seg_7699" s="T730">there</ta>
            <ta e="T733" id="Seg_7700" s="T732">rumble-MOM-PST.[3SG]</ta>
            <ta e="T734" id="Seg_7701" s="T733">what-%%-NOM/GEN/ACC.3SG</ta>
            <ta e="T735" id="Seg_7702" s="T734">PTCL</ta>
            <ta e="T736" id="Seg_7703" s="T735">belly.[NOM.SG]</ta>
            <ta e="T738" id="Seg_7704" s="T737">then</ta>
            <ta e="T739" id="Seg_7705" s="T738">there</ta>
            <ta e="T741" id="Seg_7706" s="T740">goatling-PL</ta>
            <ta e="T743" id="Seg_7707" s="T742">jump-MOM.[3SG]</ta>
            <ta e="T744" id="Seg_7708" s="T743">and</ta>
            <ta e="T745" id="Seg_7709" s="T744">again</ta>
            <ta e="T746" id="Seg_7710" s="T745">mother-LAT/LOC.3SG</ta>
            <ta e="T973" id="Seg_7711" s="T746">go-CVB</ta>
            <ta e="T747" id="Seg_7712" s="T973">disappear-PST-3PL</ta>
            <ta e="T749" id="Seg_7713" s="T748">go-OPT.DU/PL-1DU</ta>
            <ta e="T750" id="Seg_7714" s="T749">where.to=INDEF</ta>
            <ta e="T751" id="Seg_7715" s="T750">you.NOM-COM</ta>
            <ta e="T753" id="Seg_7716" s="T752">visit-CVB</ta>
            <ta e="T755" id="Seg_7717" s="T754">and</ta>
            <ta e="T756" id="Seg_7718" s="T755">what.[NOM.SG]</ta>
            <ta e="T757" id="Seg_7719" s="T756">there</ta>
            <ta e="T758" id="Seg_7720" s="T757">become-FUT-3SG</ta>
            <ta e="T760" id="Seg_7721" s="T759">PTCL</ta>
            <ta e="T761" id="Seg_7722" s="T760">what.[NOM.SG]</ta>
            <ta e="T762" id="Seg_7723" s="T761">become-FUT-3SG</ta>
            <ta e="T763" id="Seg_7724" s="T762">vodka.[NOM.SG]</ta>
            <ta e="T764" id="Seg_7725" s="T763">drink-FUT-1PL</ta>
            <ta e="T765" id="Seg_7726" s="T764">song.[NOM.SG]</ta>
            <ta e="T766" id="Seg_7727" s="T765">sing-FUT-1PL</ta>
            <ta e="T768" id="Seg_7728" s="T767">jump-FUT-1PL</ta>
            <ta e="T769" id="Seg_7729" s="T768">accordion-INS</ta>
            <ta e="T770" id="Seg_7730" s="T769">play-%%-PST-1PL</ta>
            <ta e="T772" id="Seg_7731" s="T771">there</ta>
            <ta e="T773" id="Seg_7732" s="T772">PTCL</ta>
            <ta e="T774" id="Seg_7733" s="T773">what.[NOM.SG]</ta>
            <ta e="T775" id="Seg_7734" s="T774">many</ta>
            <ta e="T776" id="Seg_7735" s="T775">meat.[NOM.SG]</ta>
            <ta e="T777" id="Seg_7736" s="T776">many</ta>
            <ta e="T778" id="Seg_7737" s="T777">%%.[NOM.SG]</ta>
            <ta e="T779" id="Seg_7738" s="T778">butter.[NOM.SG]</ta>
            <ta e="T781" id="Seg_7739" s="T780">fish.[NOM.SG]</ta>
            <ta e="T782" id="Seg_7740" s="T781">boil-DETR-PTCP.[3SG]</ta>
            <ta e="T784" id="Seg_7741" s="T783">bread.[NOM.SG]</ta>
            <ta e="T786" id="Seg_7742" s="T785">sugar.[NOM.SG]</ta>
            <ta e="T787" id="Seg_7743" s="T786">stand-PRS.[3SG]</ta>
            <ta e="T789" id="Seg_7744" s="T788">bird.cherry</ta>
            <ta e="T790" id="Seg_7745" s="T789">berry-PL</ta>
            <ta e="T791" id="Seg_7746" s="T790">be-PRS.[3SG]</ta>
            <ta e="T792" id="Seg_7747" s="T791">%%</ta>
            <ta e="T793" id="Seg_7748" s="T792">berry-PL</ta>
            <ta e="T794" id="Seg_7749" s="T793">black.[NOM.SG]</ta>
            <ta e="T795" id="Seg_7750" s="T794">berry-PL</ta>
            <ta e="T796" id="Seg_7751" s="T795">go-OPT.DU/PL-1DU</ta>
            <ta e="T798" id="Seg_7752" s="T797">you.NOM</ta>
            <ta e="T799" id="Seg_7753" s="T798">house-PL</ta>
            <ta e="T800" id="Seg_7754" s="T799">peel.bark-PST-2SG</ta>
            <ta e="T802" id="Seg_7755" s="T801">peel.bark-PST-2SG</ta>
            <ta e="T804" id="Seg_7756" s="T803">how.much</ta>
            <ta e="T805" id="Seg_7757" s="T804">house.[NOM.SG]</ta>
            <ta e="T806" id="Seg_7758" s="T805">peel.bark-PST-2SG</ta>
            <ta e="T808" id="Seg_7759" s="T807">seven.[NOM.SG]</ta>
            <ta e="T810" id="Seg_7760" s="T809">you.NOM</ta>
            <ta e="T811" id="Seg_7761" s="T810">house-PL-LAT</ta>
            <ta e="T812" id="Seg_7762" s="T811">peel.bark-PST-2SG</ta>
            <ta e="T814" id="Seg_7763" s="T813">you.NOM</ta>
            <ta e="T816" id="Seg_7764" s="T815">forest-LAT</ta>
            <ta e="T817" id="Seg_7765" s="T816">NEG</ta>
            <ta e="T818" id="Seg_7766" s="T817">go-PST-2SG</ta>
            <ta e="T819" id="Seg_7767" s="T818">NEG</ta>
            <ta e="T820" id="Seg_7768" s="T819">go-PST-1SG</ta>
            <ta e="T821" id="Seg_7769" s="T820">I.NOM</ta>
            <ta e="T822" id="Seg_7770" s="T821">fear-PRS-1SG</ta>
            <ta e="T823" id="Seg_7771" s="T822">there</ta>
            <ta e="T824" id="Seg_7772" s="T823">PTCL</ta>
            <ta e="T825" id="Seg_7773" s="T824">bear.[NOM.SG]</ta>
            <ta e="T826" id="Seg_7774" s="T825">live.[3SG]</ta>
            <ta e="T829" id="Seg_7775" s="T828">what.[NOM.SG]</ta>
            <ta e="T830" id="Seg_7776" s="T829">fear-PRS-2SG</ta>
            <ta e="T831" id="Seg_7777" s="T830">go-EP-IMP.2SG</ta>
            <ta e="T834" id="Seg_7778" s="T833">what.[NOM.SG]=INDEF</ta>
            <ta e="T835" id="Seg_7779" s="T834">NEG</ta>
            <ta e="T836" id="Seg_7780" s="T835">make-FUT-3SG</ta>
            <ta e="T837" id="Seg_7781" s="T836">NEG</ta>
            <ta e="T838" id="Seg_7782" s="T837">make-FUT-3SG</ta>
            <ta e="T839" id="Seg_7783" s="T838">you.DAT</ta>
            <ta e="T841" id="Seg_7784" s="T840">hide-EP-MOM-FUT-2SG</ta>
            <ta e="T842" id="Seg_7785" s="T841">then</ta>
            <ta e="T843" id="Seg_7786" s="T842">house-LAT-2SG</ta>
            <ta e="T844" id="Seg_7787" s="T843">come-FUT-2SG</ta>
            <ta e="T846" id="Seg_7788" s="T845">bone-VBLZ-INF.LAT</ta>
            <ta e="T847" id="Seg_7789" s="T846">one.can</ta>
            <ta e="T852" id="Seg_7790" s="T850">anyway</ta>
            <ta e="T853" id="Seg_7791" s="T852">I.NOM</ta>
            <ta e="T854" id="Seg_7792" s="T853">fear-PRS-1SG</ta>
            <ta e="T855" id="Seg_7793" s="T854">I.NOM</ta>
            <ta e="T856" id="Seg_7794" s="T855">what.[NOM.SG]=INDEF</ta>
            <ta e="T857" id="Seg_7795" s="T856">NEG.EX.[3SG]</ta>
            <ta e="T858" id="Seg_7796" s="T857">gun.[NOM.SG]</ta>
            <ta e="T859" id="Seg_7797" s="T858">gun.[NOM.SG]</ta>
            <ta e="T860" id="Seg_7798" s="T859">NEG.EX.[3SG]</ta>
            <ta e="T861" id="Seg_7799" s="T860">knife-NOM/GEN/ACC.1SG</ta>
            <ta e="T862" id="Seg_7800" s="T861">NEG.EX.[3SG]</ta>
            <ta e="T864" id="Seg_7801" s="T863">only</ta>
            <ta e="T865" id="Seg_7802" s="T864">stone.[NOM.SG]</ta>
            <ta e="T866" id="Seg_7803" s="T865">and</ta>
            <ta e="T867" id="Seg_7804" s="T866">this.[NOM.SG]</ta>
            <ta e="T868" id="Seg_7805" s="T867">NEG</ta>
            <ta e="T869" id="Seg_7806" s="T868">hurt-IPFVZ-3SG</ta>
            <ta e="T871" id="Seg_7807" s="T870">this.[NOM.SG]</ta>
            <ta e="T872" id="Seg_7808" s="T871">human-ABL</ta>
            <ta e="T873" id="Seg_7809" s="T872">fear-PRS.[3SG]</ta>
            <ta e="T874" id="Seg_7810" s="T873">run-MOM-FUT-3SG</ta>
            <ta e="T876" id="Seg_7811" s="T875">we.NOM</ta>
            <ta e="T877" id="Seg_7812" s="T876">go-PST-1PL</ta>
            <ta e="T880" id="Seg_7813" s="T879">this.[NOM.SG]</ta>
            <ta e="T881" id="Seg_7814" s="T880">forest-ABL</ta>
            <ta e="T882" id="Seg_7815" s="T881">forest-LAT</ta>
            <ta e="T884" id="Seg_7816" s="T883">then</ta>
            <ta e="T885" id="Seg_7817" s="T884">snow.[NOM.SG]</ta>
            <ta e="T886" id="Seg_7818" s="T885">go-MOM-PST.[3SG]</ta>
            <ta e="T888" id="Seg_7819" s="T887">this-PL</ta>
            <ta e="T889" id="Seg_7820" s="T888">three-COLL</ta>
            <ta e="T890" id="Seg_7821" s="T889">go-PST-3PL</ta>
            <ta e="T891" id="Seg_7822" s="T890">and</ta>
            <ta e="T892" id="Seg_7823" s="T891">I.NOM</ta>
            <ta e="T893" id="Seg_7824" s="T892">return-MOM-PST-1SG</ta>
            <ta e="T894" id="Seg_7825" s="T893">back</ta>
            <ta e="T896" id="Seg_7826" s="T895">then</ta>
            <ta e="T897" id="Seg_7827" s="T896">hear-PRS-1SG</ta>
            <ta e="T898" id="Seg_7828" s="T897">who.[NOM.SG]=INDEF</ta>
            <ta e="T899" id="Seg_7829" s="T898">PTCL</ta>
            <ta e="T900" id="Seg_7830" s="T899">branch-PL</ta>
            <ta e="T901" id="Seg_7831" s="T900">branch-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T902" id="Seg_7832" s="T901">PTCL</ta>
            <ta e="T903" id="Seg_7833" s="T902">cut-DUR.[3SG]</ta>
            <ta e="T906" id="Seg_7834" s="T905">think-1SG</ta>
            <ta e="T907" id="Seg_7835" s="T906">bear.[NOM.SG]</ta>
            <ta e="T909" id="Seg_7836" s="T908">go-</ta>
            <ta e="T911" id="Seg_7837" s="T910">PTCL</ta>
            <ta e="T913" id="Seg_7838" s="T912">%%</ta>
            <ta e="T914" id="Seg_7839" s="T913">and</ta>
            <ta e="T915" id="Seg_7840" s="T914">see-INF.LAT</ta>
            <ta e="T916" id="Seg_7841" s="T915">NEG</ta>
            <ta e="T917" id="Seg_7842" s="T916">see-PST-1SG</ta>
            <ta e="T918" id="Seg_7843" s="T917">house-LAT/LOC.1SG</ta>
            <ta e="T919" id="Seg_7844" s="T918">come-PST-1SG</ta>
            <ta e="T921" id="Seg_7845" s="T920">I.NOM</ta>
            <ta e="T922" id="Seg_7846" s="T921">come-PRS-1SG</ta>
            <ta e="T923" id="Seg_7847" s="T922">and</ta>
            <ta e="T924" id="Seg_7848" s="T923">frighten-MOM-PST-1SG</ta>
            <ta e="T925" id="Seg_7849" s="T924">PTCL</ta>
            <ta e="T927" id="Seg_7850" s="T926">tree-3PL-LAT</ta>
            <ta e="T928" id="Seg_7851" s="T927">one.should</ta>
            <ta e="T929" id="Seg_7852" s="T928">climb-INF.LAT</ta>
            <ta e="T930" id="Seg_7853" s="T929">and</ta>
            <ta e="T931" id="Seg_7854" s="T930">I.NOM</ta>
            <ta e="T932" id="Seg_7855" s="T931">this-LAT</ta>
            <ta e="T933" id="Seg_7856" s="T932">say-PST-1SG</ta>
            <ta e="T934" id="Seg_7857" s="T933">this</ta>
            <ta e="T935" id="Seg_7858" s="T934">also</ta>
            <ta e="T936" id="Seg_7859" s="T935">tree-PL-LAT</ta>
            <ta e="T937" id="Seg_7860" s="T936">climb-PRS.[3SG]</ta>
            <ta e="T939" id="Seg_7861" s="T938">and</ta>
            <ta e="T940" id="Seg_7862" s="T939">tree.[NOM.SG]</ta>
            <ta e="T941" id="Seg_7863" s="T940">break-PRS.[3SG]</ta>
            <ta e="T942" id="Seg_7864" s="T941">and</ta>
            <ta e="T943" id="Seg_7865" s="T942">earth-LAT</ta>
            <ta e="T944" id="Seg_7866" s="T943">carry-PRS.[3SG]</ta>
            <ta e="T946" id="Seg_7867" s="T945">how</ta>
            <ta e="T947" id="Seg_7868" s="T946">there</ta>
            <ta e="T948" id="Seg_7869" s="T947">speak-INF.LAT</ta>
            <ta e="T949" id="Seg_7870" s="T948">child-PL</ta>
            <ta e="T950" id="Seg_7871" s="T949">PTCL</ta>
            <ta e="T951" id="Seg_7872" s="T950">shout-DUR.[3SG]</ta>
            <ta e="T953" id="Seg_7873" s="T952">pig-PL</ta>
            <ta e="T954" id="Seg_7874" s="T953">also</ta>
            <ta e="T955" id="Seg_7875" s="T954">PTCL</ta>
            <ta e="T956" id="Seg_7876" s="T955">shout-DUR-3PL</ta>
            <ta e="T958" id="Seg_7877" s="T957">also</ta>
            <ta e="T959" id="Seg_7878" s="T958">bellow-FRQ-DUR.[3SG]</ta>
            <ta e="T961" id="Seg_7879" s="T960">how=INDEF</ta>
            <ta e="T962" id="Seg_7880" s="T961">one.cannot</ta>
            <ta e="T963" id="Seg_7881" s="T962">speak-INF.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_7882" s="T1">этот.[NOM.SG]</ta>
            <ta e="T3" id="Seg_7883" s="T2">вы.NOM</ta>
            <ta e="T4" id="Seg_7884" s="T3">фотоаппарат.[NOM.SG]</ta>
            <ta e="T5" id="Seg_7885" s="T4">%%</ta>
            <ta e="T7" id="Seg_7886" s="T6">устать-MOM-PST.[3SG]</ta>
            <ta e="T8" id="Seg_7887" s="T7">PTCL</ta>
            <ta e="T11" id="Seg_7888" s="T10">лгать-INF.LAT</ta>
            <ta e="T13" id="Seg_7889" s="T12">очень</ta>
            <ta e="T14" id="Seg_7890" s="T13">ленивый.[NOM.SG]</ta>
            <ta e="T15" id="Seg_7891" s="T14">стать-RES-PST.[3SG]</ta>
            <ta e="T17" id="Seg_7892" s="T16">ты.NOM</ta>
            <ta e="T18" id="Seg_7893" s="T17">очень</ta>
            <ta e="T19" id="Seg_7894" s="T18">%%</ta>
            <ta e="T20" id="Seg_7895" s="T19">мужчина.[NOM.SG]</ta>
            <ta e="T21" id="Seg_7896" s="T20">ты.DAT</ta>
            <ta e="T22" id="Seg_7897" s="T21">сказать-PRS-2SG</ta>
            <ta e="T23" id="Seg_7898" s="T22">сказать-PRS-2SG</ta>
            <ta e="T24" id="Seg_7899" s="T23">а</ta>
            <ta e="T25" id="Seg_7900" s="T24">ты.NOM</ta>
            <ta e="T26" id="Seg_7901" s="T25">а</ta>
            <ta e="T27" id="Seg_7902" s="T26">ты.NOM</ta>
            <ta e="T28" id="Seg_7903" s="T27">что.[NOM.SG]=INDEF</ta>
            <ta e="T29" id="Seg_7904" s="T28">NEG</ta>
            <ta e="T30" id="Seg_7905" s="T29">знать-PRS-2SG</ta>
            <ta e="T32" id="Seg_7906" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_7907" s="T32">ты.DAT</ta>
            <ta e="T34" id="Seg_7908" s="T33">сказать-PRS-1SG</ta>
            <ta e="T35" id="Seg_7909" s="T34">маленький.[NOM.SG]</ta>
            <ta e="T36" id="Seg_7910" s="T35">быть-PST-1SG</ta>
            <ta e="T38" id="Seg_7911" s="T37">какой=INDEF</ta>
            <ta e="T39" id="Seg_7912" s="T38">люди.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7913" s="T39">прийти-PST-3PL</ta>
            <ta e="T45" id="Seg_7914" s="T44">NEG</ta>
            <ta e="T46" id="Seg_7915" s="T45">знать-PRS-1SG</ta>
            <ta e="T47" id="Seg_7916" s="T46">маленький.[NOM.SG]</ta>
            <ta e="T48" id="Seg_7917" s="T47">быть-PST-1SG</ta>
            <ta e="T49" id="Seg_7918" s="T48">NEG</ta>
            <ta e="T50" id="Seg_7919" s="T49">спросить-PST-1SG</ta>
            <ta e="T52" id="Seg_7920" s="T51">я.LAT</ta>
            <ta e="T53" id="Seg_7921" s="T52">NEG</ta>
            <ta e="T54" id="Seg_7922" s="T53">сказать-PST-3PL</ta>
            <ta e="T56" id="Seg_7923" s="T55">Влас-LOC</ta>
            <ta e="T61" id="Seg_7924" s="T60">тогда</ta>
            <ta e="T62" id="Seg_7925" s="T61">этот-GEN.PL</ta>
            <ta e="T63" id="Seg_7926" s="T62">быть-PST.[3SG]</ta>
            <ta e="T65" id="Seg_7927" s="T64">и</ta>
            <ta e="T66" id="Seg_7928" s="T65">что.[NOM.SG]</ta>
            <ta e="T67" id="Seg_7929" s="T66">тоже</ta>
            <ta e="T68" id="Seg_7930" s="T67">NEG</ta>
            <ta e="T69" id="Seg_7931" s="T68">знать-PRS-1SG</ta>
            <ta e="T70" id="Seg_7932" s="T69">я.LAT</ta>
            <ta e="T71" id="Seg_7933" s="T70">показать-PST-3PL</ta>
            <ta e="T72" id="Seg_7934" s="T71">игла.[NOM.SG]</ta>
            <ta e="T74" id="Seg_7935" s="T73">вставить-MOM-PST-3PL</ta>
            <ta e="T75" id="Seg_7936" s="T74">стена-LAT</ta>
            <ta e="T77" id="Seg_7937" s="T76">далеко-LAT.ADV</ta>
            <ta e="T78" id="Seg_7938" s="T77">так</ta>
            <ta e="T80" id="Seg_7939" s="T79">сажень-NOM/GEN/ACC.3SG</ta>
            <ta e="T81" id="Seg_7940" s="T80">два.[NOM.SG]</ta>
            <ta e="T82" id="Seg_7941" s="T81">десять.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7942" s="T83">тогда</ta>
            <ta e="T85" id="Seg_7943" s="T84">я.NOM</ta>
            <ta e="T86" id="Seg_7944" s="T85">смотреть-FRQ-PST-1SG</ta>
            <ta e="T88" id="Seg_7945" s="T87">очень</ta>
            <ta e="T90" id="Seg_7946" s="T89">игла.[NOM.SG]</ta>
            <ta e="T91" id="Seg_7947" s="T90">большой.[NOM.SG]</ta>
            <ta e="T93" id="Seg_7948" s="T92">а</ta>
            <ta e="T94" id="Seg_7949" s="T93">тогда</ta>
            <ta e="T95" id="Seg_7950" s="T94">вернуться-MOM-PST-3PL</ta>
            <ta e="T96" id="Seg_7951" s="T95">другой</ta>
            <ta e="T97" id="Seg_7952" s="T96">конец-INS</ta>
            <ta e="T99" id="Seg_7953" s="T98">смотреть-PST-1SG</ta>
            <ta e="T100" id="Seg_7954" s="T99">очень</ta>
            <ta e="T101" id="Seg_7955" s="T100">далеко-LAT.ADV</ta>
            <ta e="T102" id="Seg_7956" s="T101">стать-RES-PST.[3SG]</ta>
            <ta e="T104" id="Seg_7957" s="T103">тогда</ta>
            <ta e="T105" id="Seg_7958" s="T104">бумага.[NOM.SG]</ta>
            <ta e="T106" id="Seg_7959" s="T105">класть-PST-3PL</ta>
            <ta e="T108" id="Seg_7960" s="T107">и</ta>
            <ta e="T109" id="Seg_7961" s="T108">установить-PST-3PL</ta>
            <ta e="T110" id="Seg_7962" s="T109">этот.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7963" s="T110">бинокль</ta>
            <ta e="T112" id="Seg_7964" s="T111">солнце-LAT</ta>
            <ta e="T114" id="Seg_7965" s="T113">и</ta>
            <ta e="T115" id="Seg_7966" s="T114">бумага.[NOM.SG]</ta>
            <ta e="T116" id="Seg_7967" s="T115">%гореть-MOM-PST.[3SG]</ta>
            <ta e="T118" id="Seg_7968" s="T117">вы.NOM</ta>
            <ta e="T119" id="Seg_7969" s="T118">когда</ta>
            <ta e="T120" id="Seg_7970" s="T119">пойти-FUT-2PL</ta>
            <ta e="T122" id="Seg_7971" s="T121">%%-LAT</ta>
            <ta e="T123" id="Seg_7972" s="T122">пойти-FUT-1PL</ta>
            <ta e="T125" id="Seg_7973" s="T124">ну</ta>
            <ta e="T126" id="Seg_7974" s="T125">пойти-IMP.2PL</ta>
            <ta e="T128" id="Seg_7975" s="T127">привет.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7976" s="T128">сказать-IMP.2PL</ta>
            <ta e="T131" id="Seg_7977" s="T130">Матвеев-LAT/LOC.3SG</ta>
            <ta e="T133" id="Seg_7978" s="T132">и</ta>
            <ta e="T134" id="Seg_7979" s="T133">Эля.[NOM.SG]</ta>
            <ta e="T135" id="Seg_7980" s="T134">сегодня</ta>
            <ta e="T964" id="Seg_7981" s="T135">пойти-CVB</ta>
            <ta e="T136" id="Seg_7982" s="T964">исчезнуть-PST.[3SG]</ta>
            <ta e="T137" id="Seg_7983" s="T136">Малиновка-LAT</ta>
            <ta e="T139" id="Seg_7984" s="T138">бумага.[NOM.SG]</ta>
            <ta e="T140" id="Seg_7985" s="T139">нести-%%-PST.[3SG]</ta>
            <ta e="T142" id="Seg_7986" s="T141">председатель-LAT</ta>
            <ta e="T143" id="Seg_7987" s="T142">надо</ta>
            <ta e="T144" id="Seg_7988" s="T143">печать-PL</ta>
            <ta e="T145" id="Seg_7989" s="T144">класть-INF.LAT</ta>
            <ta e="T147" id="Seg_7990" s="T146">этот.[NOM.SG]</ta>
            <ta e="T148" id="Seg_7991" s="T147">пойти-PST.[3SG]</ta>
            <ta e="T149" id="Seg_7992" s="T148">там</ta>
            <ta e="T150" id="Seg_7993" s="T149">машина-PL-INS</ta>
            <ta e="T152" id="Seg_7994" s="T151">а</ta>
            <ta e="T154" id="Seg_7995" s="T153">оттуда</ta>
            <ta e="T155" id="Seg_7996" s="T154">вернуться-FUT-3SG</ta>
            <ta e="T156" id="Seg_7997" s="T155">пешком-LAT.ADV</ta>
            <ta e="T158" id="Seg_7998" s="T157">я.NOM</ta>
            <ta e="T159" id="Seg_7999" s="T158">сестра-LAT</ta>
            <ta e="T160" id="Seg_8000" s="T159">сын-NOM/GEN.3SG</ta>
            <ta e="T965" id="Seg_8001" s="T160">пойти-CVB</ta>
            <ta e="T161" id="Seg_8002" s="T965">исчезнуть-PST.[3SG]</ta>
            <ta e="T162" id="Seg_8003" s="T161">трава-EP-VBLZ-CVB</ta>
            <ta e="T163" id="Seg_8004" s="T162">корова-PL</ta>
            <ta e="T165" id="Seg_8005" s="T164">принести-INF.LAT</ta>
            <ta e="T167" id="Seg_8006" s="T166">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T168" id="Seg_8007" s="T167">сын-NOM/GEN.3SG</ta>
            <ta e="T169" id="Seg_8008" s="T168">взять-PST.[3SG]</ta>
            <ta e="T171" id="Seg_8009" s="T170">скоро</ta>
            <ta e="T173" id="Seg_8010" s="T172">вернуться-FUT-3PL</ta>
            <ta e="T174" id="Seg_8011" s="T173">дом-LAT/LOC.3SG</ta>
            <ta e="T176" id="Seg_8012" s="T175">один.[NOM.SG]</ta>
            <ta e="T177" id="Seg_8013" s="T176">мужчина.[NOM.SG]</ta>
            <ta e="T178" id="Seg_8014" s="T177">думать-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_8015" s="T178">дом.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8016" s="T180">делать-INF.LAT</ta>
            <ta e="T183" id="Seg_8017" s="T182">золото-3PL-INS</ta>
            <ta e="T185" id="Seg_8018" s="T184">а</ta>
            <ta e="T186" id="Seg_8019" s="T185">другой.[NOM.SG]</ta>
            <ta e="T187" id="Seg_8020" s="T186">мужчина.[NOM.SG]</ta>
            <ta e="T188" id="Seg_8021" s="T187">дом.[NOM.SG]</ta>
            <ta e="T189" id="Seg_8022" s="T188">делать-INF.LAT</ta>
            <ta e="T190" id="Seg_8023" s="T189">серебро-ABL</ta>
            <ta e="T191" id="Seg_8024" s="T190">а</ta>
            <ta e="T192" id="Seg_8025" s="T191">три.[NOM.SG]</ta>
            <ta e="T193" id="Seg_8026" s="T192">мужчина.[NOM.SG]</ta>
            <ta e="T194" id="Seg_8027" s="T193">PTCL</ta>
            <ta e="T195" id="Seg_8028" s="T194">%%</ta>
            <ta e="T196" id="Seg_8029" s="T195">сказать-IPFVZ.[3SG]</ta>
            <ta e="T197" id="Seg_8030" s="T196">я.NOM</ta>
            <ta e="T198" id="Seg_8031" s="T197">дерево-3PL-INS</ta>
            <ta e="T199" id="Seg_8032" s="T198">резать-FUT-1SG</ta>
            <ta e="T200" id="Seg_8033" s="T199">дом.[NOM.SG]</ta>
            <ta e="T202" id="Seg_8034" s="T201">а</ta>
            <ta e="T203" id="Seg_8035" s="T202">четыре.[NOM.SG]</ta>
            <ta e="T204" id="Seg_8036" s="T203">мужчина.[NOM.SG]</ta>
            <ta e="T205" id="Seg_8037" s="T204">я.NOM</ta>
            <ta e="T206" id="Seg_8038" s="T205">делать-FUT-1SG</ta>
            <ta e="T207" id="Seg_8039" s="T206">трава-ABL</ta>
            <ta e="T208" id="Seg_8040" s="T207">солома-PL-INS</ta>
            <ta e="T211" id="Seg_8041" s="T210">прийти-FUT-3SG</ta>
            <ta e="T212" id="Seg_8042" s="T211">PTCL</ta>
            <ta e="T213" id="Seg_8043" s="T212">работать-PTCP.PRS</ta>
            <ta e="T215" id="Seg_8044" s="T214">гореть-FUT-3SG</ta>
            <ta e="T217" id="Seg_8045" s="T216">кто-GEN</ta>
            <ta e="T218" id="Seg_8046" s="T217">работать-PTCP.PRS</ta>
            <ta e="T219" id="Seg_8047" s="T218">NEG</ta>
            <ta e="T220" id="Seg_8048" s="T219">гореть-FUT-1SG</ta>
            <ta e="T222" id="Seg_8049" s="T221">этот.[NOM.SG]</ta>
            <ta e="T223" id="Seg_8050" s="T222">этот-LAT</ta>
            <ta e="T224" id="Seg_8051" s="T223">хороший.[NOM.SG]</ta>
            <ta e="T225" id="Seg_8052" s="T224">стать-FUT-3SG</ta>
            <ta e="T227" id="Seg_8053" s="T226">а</ta>
            <ta e="T228" id="Seg_8054" s="T227">кто-GEN</ta>
            <ta e="T229" id="Seg_8055" s="T228">работать-PTCP.PRS</ta>
            <ta e="T231" id="Seg_8056" s="T230">гореть-MOM-FUT-3SG</ta>
            <ta e="T233" id="Seg_8057" s="T232">этот-LAT</ta>
            <ta e="T234" id="Seg_8058" s="T233">NEG</ta>
            <ta e="T235" id="Seg_8059" s="T234">хороший.[NOM.SG]</ta>
            <ta e="T236" id="Seg_8060" s="T235">стать-FUT-3SG</ta>
            <ta e="T238" id="Seg_8061" s="T237">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T239" id="Seg_8062" s="T238">NEG</ta>
            <ta e="T240" id="Seg_8063" s="T239">гореть-FUT-2SG</ta>
            <ta e="T241" id="Seg_8064" s="T240">бежать-MOM-FUT-3SG</ta>
            <ta e="T243" id="Seg_8065" s="T242">а</ta>
            <ta e="T244" id="Seg_8066" s="T243">что.[NOM.SG]</ta>
            <ta e="T246" id="Seg_8067" s="T245">а</ta>
            <ta e="T247" id="Seg_8068" s="T246">что.[NOM.SG]</ta>
            <ta e="T248" id="Seg_8069" s="T247">работать-FUT.[3SG]</ta>
            <ta e="T249" id="Seg_8070" s="T248">гореть-MOM-PST.[3SG]</ta>
            <ta e="T251" id="Seg_8071" s="T250">жить-PST-3PL</ta>
            <ta e="T252" id="Seg_8072" s="T251">три.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8073" s="T252">свинья-PL</ta>
            <ta e="T254" id="Seg_8074" s="T253">жить-PST-3PL</ta>
            <ta e="T256" id="Seg_8075" s="T255">тогда</ta>
            <ta e="T257" id="Seg_8076" s="T256">два.[NOM.SG]</ta>
            <ta e="T258" id="Seg_8077" s="T257">свинья.[NOM.SG]</ta>
            <ta e="T259" id="Seg_8078" s="T258">очень</ta>
            <ta e="T260" id="Seg_8079" s="T259">ленивый.[NOM.SG]</ta>
            <ta e="T261" id="Seg_8080" s="T260">быть-PST-3PL</ta>
            <ta e="T262" id="Seg_8081" s="T261">а</ta>
            <ta e="T263" id="Seg_8082" s="T262">один.[NOM.SG]</ta>
            <ta e="T264" id="Seg_8083" s="T263">свинья.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8084" s="T264">всегда</ta>
            <ta e="T266" id="Seg_8085" s="T265">работать-PST.[3SG]</ta>
            <ta e="T268" id="Seg_8086" s="T267">этот-PL</ta>
            <ta e="T269" id="Seg_8087" s="T268">PTCL</ta>
            <ta e="T270" id="Seg_8088" s="T269">посещать-CVB</ta>
            <ta e="T271" id="Seg_8089" s="T270">идти-PST-3PL</ta>
            <ta e="T272" id="Seg_8090" s="T271">водка.[NOM.SG]</ta>
            <ta e="T273" id="Seg_8091" s="T272">пить-PST-3PL</ta>
            <ta e="T275" id="Seg_8092" s="T274">а</ta>
            <ta e="T276" id="Seg_8093" s="T275">тот.[NOM.SG]</ta>
            <ta e="T277" id="Seg_8094" s="T276">один.[NOM.SG]</ta>
            <ta e="T278" id="Seg_8095" s="T277">свинья.[NOM.SG]</ta>
            <ta e="T279" id="Seg_8096" s="T278">PTCL</ta>
            <ta e="T280" id="Seg_8097" s="T279">копать-PST.[3SG]</ta>
            <ta e="T281" id="Seg_8098" s="T280">земля.[NOM.SG]</ta>
            <ta e="T283" id="Seg_8099" s="T282">камень-PL</ta>
            <ta e="T284" id="Seg_8100" s="T283">собрать-PST-3PL</ta>
            <ta e="T286" id="Seg_8101" s="T285">и</ta>
            <ta e="T287" id="Seg_8102" s="T286">дом.[NOM.SG]</ta>
            <ta e="T288" id="Seg_8103" s="T287">сам-LAT/LOC.3SG</ta>
            <ta e="T289" id="Seg_8104" s="T288">делать-PST.[3SG]</ta>
            <ta e="T291" id="Seg_8105" s="T290">камень-PL-LAT</ta>
            <ta e="T294" id="Seg_8106" s="T293">этот-PL</ta>
            <ta e="T295" id="Seg_8107" s="T294">прийти-PST-3PL</ta>
            <ta e="T296" id="Seg_8108" s="T295">один.[NOM.SG]</ta>
            <ta e="T297" id="Seg_8109" s="T296">класть-PST.[3SG]</ta>
            <ta e="T299" id="Seg_8110" s="T298">ветка-PL</ta>
            <ta e="T300" id="Seg_8111" s="T299">собирать-PST.[3SG]</ta>
            <ta e="T301" id="Seg_8112" s="T300">и</ta>
            <ta e="T302" id="Seg_8113" s="T301">класть-PST.[3SG]</ta>
            <ta e="T303" id="Seg_8114" s="T302">а</ta>
            <ta e="T304" id="Seg_8115" s="T303">один.[NOM.SG]</ta>
            <ta e="T305" id="Seg_8116" s="T304">солома-PL-INS</ta>
            <ta e="T307" id="Seg_8117" s="T306">класть-PST.[3SG]</ta>
            <ta e="T308" id="Seg_8118" s="T307">сам-LAT/LOC.3SG</ta>
            <ta e="T309" id="Seg_8119" s="T308">дом.[NOM.SG]</ta>
            <ta e="T311" id="Seg_8120" s="T310">а</ta>
            <ta e="T312" id="Seg_8121" s="T311">этот.[NOM.SG]</ta>
            <ta e="T313" id="Seg_8122" s="T312">всегда</ta>
            <ta e="T314" id="Seg_8123" s="T313">камень-ACC</ta>
            <ta e="T315" id="Seg_8124" s="T314">камень-PL-INS</ta>
            <ta e="T316" id="Seg_8125" s="T315">класть-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_8126" s="T316">этот</ta>
            <ta e="T318" id="Seg_8127" s="T317">этот-PL</ta>
            <ta e="T319" id="Seg_8128" s="T318">прийти-PST-3PL</ta>
            <ta e="T320" id="Seg_8129" s="T319">что.[NOM.SG]</ta>
            <ta e="T321" id="Seg_8130" s="T320">ты.NOM</ta>
            <ta e="T322" id="Seg_8131" s="T321">работать-PRS-2SG</ta>
            <ta e="T324" id="Seg_8132" s="T323">мы.NOM</ta>
            <ta e="T325" id="Seg_8133" s="T324">скоро-LOC.ADV</ta>
            <ta e="T327" id="Seg_8134" s="T326">делать-PST-1PL</ta>
            <ta e="T329" id="Seg_8135" s="T328">дом-NOM/GEN/ACC.3SG</ta>
            <ta e="T331" id="Seg_8136" s="T330">а</ta>
            <ta e="T333" id="Seg_8137" s="T332">кто-GEN</ta>
            <ta e="T334" id="Seg_8138" s="T333">хороший.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8139" s="T334">дом-NOM/GEN/ACC.3SG</ta>
            <ta e="T336" id="Seg_8140" s="T335">стать-FUT-3SG</ta>
            <ta e="T337" id="Seg_8141" s="T336">я.GEN</ta>
            <ta e="T338" id="Seg_8142" s="T337">или</ta>
            <ta e="T339" id="Seg_8143" s="T338">вы.NOM</ta>
            <ta e="T341" id="Seg_8144" s="T340">тогда</ta>
            <ta e="T342" id="Seg_8145" s="T341">холодный.[NOM.SG]</ta>
            <ta e="T343" id="Seg_8146" s="T342">стать-RES-PST.[3SG]</ta>
            <ta e="T344" id="Seg_8147" s="T343">снег.[NOM.SG]</ta>
            <ta e="T345" id="Seg_8148" s="T344">снег.[NOM.SG]</ta>
            <ta e="T346" id="Seg_8149" s="T345">прийти-PRS.[3SG]</ta>
            <ta e="T348" id="Seg_8150" s="T347">тогда</ta>
            <ta e="T349" id="Seg_8151" s="T348">этот-PL</ta>
            <ta e="T351" id="Seg_8152" s="T350">ползти-PST-3PL</ta>
            <ta e="T352" id="Seg_8153" s="T351">дом-LAT</ta>
            <ta e="T353" id="Seg_8154" s="T352">какой</ta>
            <ta e="T354" id="Seg_8155" s="T353">солома.[NOM.SG]</ta>
            <ta e="T356" id="Seg_8156" s="T355">закрыть-PST-3PL</ta>
            <ta e="T357" id="Seg_8157" s="T356">дверь.[NOM.SG]</ta>
            <ta e="T358" id="Seg_8158" s="T357">прийти-PST.[3SG]</ta>
            <ta e="T359" id="Seg_8159" s="T358">медведь.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8160" s="T360">пускать-%%-IMP.2PL</ta>
            <ta e="T362" id="Seg_8161" s="T361">я.LAT</ta>
            <ta e="T364" id="Seg_8162" s="T363">этот-PL</ta>
            <ta e="T365" id="Seg_8163" s="T364">NEG</ta>
            <ta e="T366" id="Seg_8164" s="T365">пускать-FUT-CNG</ta>
            <ta e="T367" id="Seg_8165" s="T366">скоро</ta>
            <ta e="T368" id="Seg_8166" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_8167" s="T368">сломать-FUT-1SG</ta>
            <ta e="T370" id="Seg_8168" s="T369">вы.NOM</ta>
            <ta e="T371" id="Seg_8169" s="T370">дом-ABL</ta>
            <ta e="T372" id="Seg_8170" s="T371">дом-LAT</ta>
            <ta e="T374" id="Seg_8171" s="T373">тогда</ta>
            <ta e="T375" id="Seg_8172" s="T374">сломать-PST.[3SG]</ta>
            <ta e="T376" id="Seg_8173" s="T375">этот-PL</ta>
            <ta e="T377" id="Seg_8174" s="T376">бежать-MOM-PST-3PL</ta>
            <ta e="T379" id="Seg_8175" s="T378">какой</ta>
            <ta e="T380" id="Seg_8176" s="T379">дом.[NOM.SG]</ta>
            <ta e="T382" id="Seg_8177" s="T381">ветка-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T383" id="Seg_8178" s="T382">делать-PST-3PL</ta>
            <ta e="T385" id="Seg_8179" s="T384">и</ta>
            <ta e="T386" id="Seg_8180" s="T385">там</ta>
            <ta e="T387" id="Seg_8181" s="T386">дверь.[NOM.SG]</ta>
            <ta e="T389" id="Seg_8182" s="T388">закрыть-MOM-PST-3PL</ta>
            <ta e="T390" id="Seg_8183" s="T389">этот.[NOM.SG]</ta>
            <ta e="T391" id="Seg_8184" s="T390">там</ta>
            <ta e="T392" id="Seg_8185" s="T391">прийти-PST.[3SG]</ta>
            <ta e="T394" id="Seg_8186" s="T393">открыть-IMP.2PL</ta>
            <ta e="T396" id="Seg_8187" s="T395">а.то</ta>
            <ta e="T397" id="Seg_8188" s="T396">скоро</ta>
            <ta e="T398" id="Seg_8189" s="T397">PTCL</ta>
            <ta e="T399" id="Seg_8190" s="T398">упасть-MOM-FUT-3SG</ta>
            <ta e="T400" id="Seg_8191" s="T399">дом.[NOM.SG]</ta>
            <ta e="T402" id="Seg_8192" s="T401">тогда</ta>
            <ta e="T403" id="Seg_8193" s="T402">PTCL</ta>
            <ta e="T404" id="Seg_8194" s="T403">выбросить-PST.[3SG]</ta>
            <ta e="T405" id="Seg_8195" s="T404">этот-PL</ta>
            <ta e="T406" id="Seg_8196" s="T405">бежать-MOM-PST-3PL</ta>
            <ta e="T408" id="Seg_8197" s="T407">и</ta>
            <ta e="T409" id="Seg_8198" s="T408">прийти-PST-3PL</ta>
            <ta e="T410" id="Seg_8199" s="T409">здесь</ta>
            <ta e="T411" id="Seg_8200" s="T410">один-LAT</ta>
            <ta e="T413" id="Seg_8201" s="T412">три-LAT</ta>
            <ta e="T414" id="Seg_8202" s="T413">свинья-LAT/LOC.3SG</ta>
            <ta e="T415" id="Seg_8203" s="T414">прийти-PST-3PL</ta>
            <ta e="T417" id="Seg_8204" s="T416">пускать-HAB-IMP.2SG</ta>
            <ta e="T418" id="Seg_8205" s="T417">мы.LAT</ta>
            <ta e="T419" id="Seg_8206" s="T418">этот.[NOM.SG]</ta>
            <ta e="T420" id="Seg_8207" s="T419">пускать-SEM-PST.[3SG]</ta>
            <ta e="T421" id="Seg_8208" s="T420">и</ta>
            <ta e="T422" id="Seg_8209" s="T421">дверь-ACC.3SG</ta>
            <ta e="T424" id="Seg_8210" s="T423">закрыть-PST.[3SG]</ta>
            <ta e="T425" id="Seg_8211" s="T424">этот.[NOM.SG]</ta>
            <ta e="T426" id="Seg_8212" s="T425">прийти-PST.[3SG]</ta>
            <ta e="T427" id="Seg_8213" s="T426">медведь.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8214" s="T427">пускать-HAB-IMP.2SG</ta>
            <ta e="T430" id="Seg_8215" s="T429">NEG</ta>
            <ta e="T431" id="Seg_8216" s="T430">пускать-FUT-1SG</ta>
            <ta e="T433" id="Seg_8217" s="T432">тогда</ta>
            <ta e="T434" id="Seg_8218" s="T433">этот.[NOM.SG]</ta>
            <ta e="T435" id="Seg_8219" s="T434">сломать-INF.LAT</ta>
            <ta e="T436" id="Seg_8220" s="T435">хотеть.PST.M.SG</ta>
            <ta e="T437" id="Seg_8221" s="T436">NEG</ta>
            <ta e="T438" id="Seg_8222" s="T437">мочь-PST.[3SG]</ta>
            <ta e="T439" id="Seg_8223" s="T438">тогда</ta>
            <ta e="T441" id="Seg_8224" s="T440">влезать-PST.[3SG]</ta>
            <ta e="T442" id="Seg_8225" s="T441">там</ta>
            <ta e="T444" id="Seg_8226" s="T443">вверх</ta>
            <ta e="T445" id="Seg_8227" s="T444">труба-LAT/LOC.3SG</ta>
            <ta e="T447" id="Seg_8228" s="T446">лезть-DUR-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_8229" s="T447">а</ta>
            <ta e="T449" id="Seg_8230" s="T448">этот.[NOM.SG]</ta>
            <ta e="T450" id="Seg_8231" s="T449">котел.[NOM.SG]</ta>
            <ta e="T451" id="Seg_8232" s="T450">поставить-PST.[3SG]</ta>
            <ta e="T452" id="Seg_8233" s="T451">теплый.[NOM.SG]</ta>
            <ta e="T453" id="Seg_8234" s="T452">вода-INS</ta>
            <ta e="T455" id="Seg_8235" s="T454">этот.[NOM.SG]</ta>
            <ta e="T456" id="Seg_8236" s="T455">PTCL</ta>
            <ta e="T457" id="Seg_8237" s="T456">упасть-MOM-PST.[3SG]</ta>
            <ta e="T458" id="Seg_8238" s="T457">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T460" id="Seg_8239" s="T459">и</ta>
            <ta e="T462" id="Seg_8240" s="T461">тогда</ta>
            <ta e="T463" id="Seg_8241" s="T462">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T464" id="Seg_8242" s="T463">и</ta>
            <ta e="T966" id="Seg_8243" s="T464">пойти-CVB</ta>
            <ta e="T465" id="Seg_8244" s="T966">исчезнуть-PST.[3SG]</ta>
            <ta e="T466" id="Seg_8245" s="T465">и</ta>
            <ta e="T467" id="Seg_8246" s="T466">еще</ta>
            <ta e="T468" id="Seg_8247" s="T467">когда=INDEF</ta>
            <ta e="T469" id="Seg_8248" s="T468">NEG</ta>
            <ta e="T470" id="Seg_8249" s="T469">прийти-PST.[3SG]</ta>
            <ta e="T473" id="Seg_8250" s="T472">жечь-PST-3PL</ta>
            <ta e="T474" id="Seg_8251" s="T473">этот-ACC</ta>
            <ta e="T476" id="Seg_8252" s="T475">и</ta>
            <ta e="T967" id="Seg_8253" s="T476">пойти-CVB</ta>
            <ta e="T477" id="Seg_8254" s="T967">исчезнуть-PST.[3SG]</ta>
            <ta e="T479" id="Seg_8255" s="T478">сколько</ta>
            <ta e="T481" id="Seg_8256" s="T480">мы.NOM</ta>
            <ta e="T482" id="Seg_8257" s="T481">пленка-PL</ta>
            <ta e="T483" id="Seg_8258" s="T482">говорить-PST-1SG</ta>
            <ta e="T484" id="Seg_8259" s="T483">говорить-PST-1PL</ta>
            <ta e="T486" id="Seg_8260" s="T485">десять.[NOM.SG]</ta>
            <ta e="T487" id="Seg_8261" s="T486">три.[NOM.SG]</ta>
            <ta e="T489" id="Seg_8262" s="T488">вы.GEN</ta>
            <ta e="T490" id="Seg_8263" s="T489">хлеб-NOM/GEN/ACC.2PL</ta>
            <ta e="T491" id="Seg_8264" s="T490">быть-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_8265" s="T492">быть-PRS.[3SG]</ta>
            <ta e="T494" id="Seg_8266" s="T493">а</ta>
            <ta e="T495" id="Seg_8267" s="T494">где</ta>
            <ta e="T496" id="Seg_8268" s="T495">взять-PST-2PL</ta>
            <ta e="T498" id="Seg_8269" s="T497">PTCL</ta>
            <ta e="T499" id="Seg_8270" s="T498">один.[NOM.SG]</ta>
            <ta e="T500" id="Seg_8271" s="T499">женщина.[NOM.SG]</ta>
            <ta e="T501" id="Seg_8272" s="T500">принести-PST.[3SG]</ta>
            <ta e="T504" id="Seg_8273" s="T503">Вознесенское-PL-INS</ta>
            <ta e="T506" id="Seg_8274" s="T505">один.[NOM.SG]</ta>
            <ta e="T507" id="Seg_8275" s="T506">коза-GEN</ta>
            <ta e="T508" id="Seg_8276" s="T507">пять.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8277" s="T508">ребенок-PL-LAT</ta>
            <ta e="T510" id="Seg_8278" s="T509">быть-PST-3PL</ta>
            <ta e="T512" id="Seg_8279" s="T511">этот.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8280" s="T512">этот-ACC.PL</ta>
            <ta e="T514" id="Seg_8281" s="T513">дом-LAT-%%</ta>
            <ta e="T515" id="Seg_8282" s="T514">оставить-PST.[3SG]</ta>
            <ta e="T517" id="Seg_8283" s="T516">дверь-ACC</ta>
            <ta e="T518" id="Seg_8284" s="T517">закрыть-PST.[3SG]</ta>
            <ta e="T520" id="Seg_8285" s="T519">кто-ACC=INDEF</ta>
            <ta e="T521" id="Seg_8286" s="T520">NEG.AUX-IMP.2SG</ta>
            <ta e="T523" id="Seg_8287" s="T522">NEG.AUX-IMP.2SG</ta>
            <ta e="T524" id="Seg_8288" s="T523">пускать-%%-CNG</ta>
            <ta e="T526" id="Seg_8289" s="T525">тогда</ta>
            <ta e="T968" id="Seg_8290" s="T526">пойти-CVB</ta>
            <ta e="T527" id="Seg_8291" s="T968">исчезнуть-PST.[3SG]</ta>
            <ta e="T529" id="Seg_8292" s="T528">медведь.[NOM.SG]</ta>
            <ta e="T530" id="Seg_8293" s="T529">прийти-PST.[3SG]</ta>
            <ta e="T531" id="Seg_8294" s="T530">открыть-IMP.2PL.O</ta>
            <ta e="T532" id="Seg_8295" s="T531">дверь.[NOM.SG]</ta>
            <ta e="T534" id="Seg_8296" s="T533">этот-PL</ta>
            <ta e="T535" id="Seg_8297" s="T534">NEG</ta>
            <ta e="T536" id="Seg_8298" s="T535">открыть-RES-3PL</ta>
            <ta e="T538" id="Seg_8299" s="T537">мы.LAT</ta>
            <ta e="T539" id="Seg_8300" s="T538">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T540" id="Seg_8301" s="T539">сказать-PST.[3SG]</ta>
            <ta e="T541" id="Seg_8302" s="T540">кто-ACC=INDEF</ta>
            <ta e="T542" id="Seg_8303" s="T541">NEG</ta>
            <ta e="T543" id="Seg_8304" s="T542">пускать-%%-INF.LAT</ta>
            <ta e="T545" id="Seg_8305" s="T544">тогда</ta>
            <ta e="T546" id="Seg_8306" s="T545">медведь.[NOM.SG]</ta>
            <ta e="T969" id="Seg_8307" s="T546">пойти-CVB</ta>
            <ta e="T547" id="Seg_8308" s="T969">исчезнуть-PST.[3SG]</ta>
            <ta e="T548" id="Seg_8309" s="T547">мать-NOM/GEN.3SG</ta>
            <ta e="T549" id="Seg_8310" s="T548">прийти-PST.[3SG]</ta>
            <ta e="T551" id="Seg_8311" s="T550">открыть-IMP.2PL</ta>
            <ta e="T552" id="Seg_8312" s="T551">дверь.[NOM.SG]</ta>
            <ta e="T553" id="Seg_8313" s="T552">я.NOM</ta>
            <ta e="T554" id="Seg_8314" s="T553">принести-PST-1SG</ta>
            <ta e="T555" id="Seg_8315" s="T554">молоко.[NOM.SG]</ta>
            <ta e="T557" id="Seg_8316" s="T556">принести-PST-1SG</ta>
            <ta e="T558" id="Seg_8317" s="T557">трава.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8318" s="T558">вы.ACC</ta>
            <ta e="T560" id="Seg_8319" s="T559">творог</ta>
            <ta e="T561" id="Seg_8320" s="T560">принести-PST-1SG</ta>
            <ta e="T563" id="Seg_8321" s="T562">тогда</ta>
            <ta e="T564" id="Seg_8322" s="T563">этот-PL</ta>
            <ta e="T565" id="Seg_8323" s="T564">открыть-PST-3PL</ta>
            <ta e="T566" id="Seg_8324" s="T565">съесть-PST-3PL</ta>
            <ta e="T567" id="Seg_8325" s="T566">тогда</ta>
            <ta e="T568" id="Seg_8326" s="T567">мать-NOM/GEN.3SG</ta>
            <ta e="T569" id="Seg_8327" s="T568">опять</ta>
            <ta e="T570" id="Seg_8328" s="T569">пойти-PST.[3SG]</ta>
            <ta e="T572" id="Seg_8329" s="T571">NEG.AUX-IMP.2SG</ta>
            <ta e="T573" id="Seg_8330" s="T572">пускать-%%-CNG</ta>
            <ta e="T574" id="Seg_8331" s="T573">кто-ACC=INDEF</ta>
            <ta e="T576" id="Seg_8332" s="T575">опять</ta>
            <ta e="T970" id="Seg_8333" s="T576">пойти-CVB</ta>
            <ta e="T577" id="Seg_8334" s="T970">исчезнуть-PST.[3SG]</ta>
            <ta e="T579" id="Seg_8335" s="T578">кто-ACC=INDEF</ta>
            <ta e="T580" id="Seg_8336" s="T579">NEG.AUX-IMP.2SG</ta>
            <ta e="T581" id="Seg_8337" s="T580">пускать-%%</ta>
            <ta e="T583" id="Seg_8338" s="T582">тогда</ta>
            <ta e="T584" id="Seg_8339" s="T583">опять</ta>
            <ta e="T585" id="Seg_8340" s="T584">прийти-PST.[3SG]</ta>
            <ta e="T586" id="Seg_8341" s="T585">медведь.[NOM.SG]</ta>
            <ta e="T588" id="Seg_8342" s="T587">ребенок-PL</ta>
            <ta e="T589" id="Seg_8343" s="T588">ребенок-PL</ta>
            <ta e="T590" id="Seg_8344" s="T589">пускать-2PL</ta>
            <ta e="T591" id="Seg_8345" s="T590">я.LAT</ta>
            <ta e="T593" id="Seg_8346" s="T592">нет</ta>
            <ta e="T595" id="Seg_8347" s="T594">мы.NOM</ta>
            <ta e="T596" id="Seg_8348" s="T595">NEG</ta>
            <ta e="T597" id="Seg_8349" s="T596">пускать-CVB</ta>
            <ta e="T599" id="Seg_8350" s="T598">а</ta>
            <ta e="T600" id="Seg_8351" s="T599">я.GEN</ta>
            <ta e="T601" id="Seg_8352" s="T600">вы.NOM</ta>
            <ta e="T602" id="Seg_8353" s="T601">мать</ta>
            <ta e="T603" id="Seg_8354" s="T602">быть-PRS-1SG</ta>
            <ta e="T604" id="Seg_8355" s="T603">нет</ta>
            <ta e="T605" id="Seg_8356" s="T604">я.GEN</ta>
            <ta e="T607" id="Seg_8357" s="T606">мать-GEN.1SG</ta>
            <ta e="T608" id="Seg_8358" s="T607">голос-NOM/GEN/ACC.3SG</ta>
            <ta e="T609" id="Seg_8359" s="T608">худой.[NOM.SG]</ta>
            <ta e="T611" id="Seg_8360" s="T610">а</ta>
            <ta e="T612" id="Seg_8361" s="T611">ты.GEN</ta>
            <ta e="T613" id="Seg_8362" s="T612">голос-NOM/GEN/ACC.3SG</ta>
            <ta e="T614" id="Seg_8363" s="T613">очень</ta>
            <ta e="T615" id="Seg_8364" s="T614">толстый.[NOM.SG]</ta>
            <ta e="T617" id="Seg_8365" s="T616">тогда</ta>
            <ta e="T618" id="Seg_8366" s="T617">медведь.[NOM.SG]</ta>
            <ta e="T971" id="Seg_8367" s="T618">пойти-CVB</ta>
            <ta e="T619" id="Seg_8368" s="T971">исчезнуть-PST.[3SG]</ta>
            <ta e="T620" id="Seg_8369" s="T619">тогда</ta>
            <ta e="T621" id="Seg_8370" s="T620">мать-NOM/GEN.3SG</ta>
            <ta e="T622" id="Seg_8371" s="T621">прийти-PST.[3SG]</ta>
            <ta e="T624" id="Seg_8372" s="T623">этот-PL</ta>
            <ta e="T625" id="Seg_8373" s="T624">открыть-PST-3PL</ta>
            <ta e="T626" id="Seg_8374" s="T625">кормить-PST.[3SG]</ta>
            <ta e="T628" id="Seg_8375" s="T627">сейчас</ta>
            <ta e="T629" id="Seg_8376" s="T628">кто-LAT=INDEF</ta>
            <ta e="T630" id="Seg_8377" s="T629">NEG</ta>
            <ta e="T631" id="Seg_8378" s="T630">пускать-%%</ta>
            <ta e="T633" id="Seg_8379" s="T632">а</ta>
            <ta e="T634" id="Seg_8380" s="T633">медведь.[NOM.SG]</ta>
            <ta e="T635" id="Seg_8381" s="T634">пойти-PST.[3SG]</ta>
            <ta e="T636" id="Seg_8382" s="T635">кузница-LAT</ta>
            <ta e="T638" id="Seg_8383" s="T637">язык-ACC.3SG</ta>
            <ta e="T640" id="Seg_8384" s="T639">делать-PST.[3SG]</ta>
            <ta e="T641" id="Seg_8385" s="T640">худой.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8386" s="T642">тогда</ta>
            <ta e="T644" id="Seg_8387" s="T643">прийти-PST.[3SG]</ta>
            <ta e="T645" id="Seg_8388" s="T644">опять</ta>
            <ta e="T647" id="Seg_8389" s="T646">этот.[NOM.SG]</ta>
            <ta e="T648" id="Seg_8390" s="T647">сказать-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8391" s="T648">я.NOM</ta>
            <ta e="T650" id="Seg_8392" s="T649">вы.NOM</ta>
            <ta e="T651" id="Seg_8393" s="T650">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T652" id="Seg_8394" s="T651">принести-PST-1SG</ta>
            <ta e="T653" id="Seg_8395" s="T652">молоко.[NOM.SG]</ta>
            <ta e="T655" id="Seg_8396" s="T654">хлеб.[NOM.SG]</ta>
            <ta e="T656" id="Seg_8397" s="T655">принести-PST-1SG</ta>
            <ta e="T658" id="Seg_8398" s="T657">принести-PST-1SG</ta>
            <ta e="T659" id="Seg_8399" s="T658">вы.ACC</ta>
            <ta e="T661" id="Seg_8400" s="T660">трава.[NOM.SG]</ta>
            <ta e="T663" id="Seg_8401" s="T662">этот-PL</ta>
            <ta e="T664" id="Seg_8402" s="T663">PTCL</ta>
            <ta e="T665" id="Seg_8403" s="T664">открыть-MOM-PST.[3SG]</ta>
            <ta e="T667" id="Seg_8404" s="T666">этот.[NOM.SG]</ta>
            <ta e="T668" id="Seg_8405" s="T667">этот-ACC.PL</ta>
            <ta e="T669" id="Seg_8406" s="T668">PTCL</ta>
            <ta e="T670" id="Seg_8407" s="T669">съесть-MOM-PST.[3SG]</ta>
            <ta e="T672" id="Seg_8408" s="T671">мать-NOM/GEN.3SG</ta>
            <ta e="T673" id="Seg_8409" s="T672">прийти-PST.[3SG]</ta>
            <ta e="T675" id="Seg_8410" s="T674">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T676" id="Seg_8411" s="T675">NEG.EX.[3SG]</ta>
            <ta e="T677" id="Seg_8412" s="T676">один.[NOM.SG]</ta>
            <ta e="T680" id="Seg_8413" s="T679">маленький.[NOM.SG]</ta>
            <ta e="T681" id="Seg_8414" s="T680">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T683" id="Seg_8415" s="T682">тогда</ta>
            <ta e="T684" id="Seg_8416" s="T683">сказать-IPFVZ.[3SG]</ta>
            <ta e="T685" id="Seg_8417" s="T684">медведь.[NOM.SG]</ta>
            <ta e="T686" id="Seg_8418" s="T685">весь</ta>
            <ta e="T687" id="Seg_8419" s="T686">съесть-MOM-PST.[3SG]</ta>
            <ta e="T689" id="Seg_8420" s="T688">я.NOM</ta>
            <ta e="T690" id="Seg_8421" s="T689">один</ta>
            <ta e="T691" id="Seg_8422" s="T690">остаться-MOM-PST-1SG</ta>
            <ta e="T692" id="Seg_8423" s="T691">печь-LAT</ta>
            <ta e="T698" id="Seg_8424" s="T697">печь-LAT</ta>
            <ta e="T699" id="Seg_8425" s="T698">лезть-MOM-PST-1SG</ta>
            <ta e="T700" id="Seg_8426" s="T699">этот.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8427" s="T700">я.LAT</ta>
            <ta e="T702" id="Seg_8428" s="T701">NEG</ta>
            <ta e="T703" id="Seg_8429" s="T702">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T972" id="Seg_8430" s="T703">пойти-CVB</ta>
            <ta e="T704" id="Seg_8431" s="T972">исчезнуть-PST.[3SG]</ta>
            <ta e="T706" id="Seg_8432" s="T705">тогда</ta>
            <ta e="T707" id="Seg_8433" s="T706">медведь.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8434" s="T707">прийти-PST.[3SG]</ta>
            <ta e="T709" id="Seg_8435" s="T708">коза-LAT</ta>
            <ta e="T711" id="Seg_8436" s="T710">коза.[NOM.SG]</ta>
            <ta e="T712" id="Seg_8437" s="T711">сказать-IPFVZ.[3SG]</ta>
            <ta e="T715" id="Seg_8438" s="T714">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T717" id="Seg_8439" s="T716">прыгнуть-1DU</ta>
            <ta e="T718" id="Seg_8440" s="T717">тот.[NOM.SG]</ta>
            <ta e="T719" id="Seg_8441" s="T718">яма-ABL</ta>
            <ta e="T722" id="Seg_8442" s="T721">коза.[NOM.SG]</ta>
            <ta e="T723" id="Seg_8443" s="T722">прыгнуть-PST.[3SG]</ta>
            <ta e="T726" id="Seg_8444" s="T725">а</ta>
            <ta e="T727" id="Seg_8445" s="T726">этот.[NOM.SG]</ta>
            <ta e="T728" id="Seg_8446" s="T727">NEG</ta>
            <ta e="T729" id="Seg_8447" s="T728">мочь-PST.[3SG]</ta>
            <ta e="T730" id="Seg_8448" s="T729">прыгнуть-OPT.SG.[3SG]</ta>
            <ta e="T731" id="Seg_8449" s="T730">там</ta>
            <ta e="T733" id="Seg_8450" s="T732">греметь-MOM-PST.[3SG]</ta>
            <ta e="T734" id="Seg_8451" s="T733">что-%%-NOM/GEN/ACC.3SG</ta>
            <ta e="T735" id="Seg_8452" s="T734">PTCL</ta>
            <ta e="T736" id="Seg_8453" s="T735">живот.[NOM.SG]</ta>
            <ta e="T738" id="Seg_8454" s="T737">тогда</ta>
            <ta e="T739" id="Seg_8455" s="T738">там</ta>
            <ta e="T741" id="Seg_8456" s="T740">козлята-PL</ta>
            <ta e="T743" id="Seg_8457" s="T742">прыгнуть-MOM.[3SG]</ta>
            <ta e="T744" id="Seg_8458" s="T743">и</ta>
            <ta e="T745" id="Seg_8459" s="T744">опять</ta>
            <ta e="T746" id="Seg_8460" s="T745">мать-LAT/LOC.3SG</ta>
            <ta e="T973" id="Seg_8461" s="T746">пойти-CVB</ta>
            <ta e="T747" id="Seg_8462" s="T973">исчезнуть-PST-3PL</ta>
            <ta e="T749" id="Seg_8463" s="T748">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T750" id="Seg_8464" s="T749">куда=INDEF</ta>
            <ta e="T751" id="Seg_8465" s="T750">ты.NOM-COM</ta>
            <ta e="T753" id="Seg_8466" s="T752">посещать-CVB</ta>
            <ta e="T755" id="Seg_8467" s="T754">а</ta>
            <ta e="T756" id="Seg_8468" s="T755">что.[NOM.SG]</ta>
            <ta e="T757" id="Seg_8469" s="T756">там</ta>
            <ta e="T758" id="Seg_8470" s="T757">стать-FUT-3SG</ta>
            <ta e="T760" id="Seg_8471" s="T759">PTCL</ta>
            <ta e="T761" id="Seg_8472" s="T760">что.[NOM.SG]</ta>
            <ta e="T762" id="Seg_8473" s="T761">стать-FUT-3SG</ta>
            <ta e="T763" id="Seg_8474" s="T762">водка.[NOM.SG]</ta>
            <ta e="T764" id="Seg_8475" s="T763">пить-FUT-1PL</ta>
            <ta e="T765" id="Seg_8476" s="T764">песня.[NOM.SG]</ta>
            <ta e="T766" id="Seg_8477" s="T765">петь-FUT-1PL</ta>
            <ta e="T768" id="Seg_8478" s="T767">прыгнуть-FUT-1PL</ta>
            <ta e="T769" id="Seg_8479" s="T768">гармонь-INS</ta>
            <ta e="T770" id="Seg_8480" s="T769">играть-%%-PST-1PL</ta>
            <ta e="T772" id="Seg_8481" s="T771">там</ta>
            <ta e="T773" id="Seg_8482" s="T772">PTCL</ta>
            <ta e="T774" id="Seg_8483" s="T773">что.[NOM.SG]</ta>
            <ta e="T775" id="Seg_8484" s="T774">много</ta>
            <ta e="T776" id="Seg_8485" s="T775">мясо.[NOM.SG]</ta>
            <ta e="T777" id="Seg_8486" s="T776">много</ta>
            <ta e="T778" id="Seg_8487" s="T777">%%.[NOM.SG]</ta>
            <ta e="T779" id="Seg_8488" s="T778">масло.[NOM.SG]</ta>
            <ta e="T781" id="Seg_8489" s="T780">рыба.[NOM.SG]</ta>
            <ta e="T782" id="Seg_8490" s="T781">кипятить-DETR-PTCP.[3SG]</ta>
            <ta e="T784" id="Seg_8491" s="T783">хлеб.[NOM.SG]</ta>
            <ta e="T786" id="Seg_8492" s="T785">сахар.[NOM.SG]</ta>
            <ta e="T787" id="Seg_8493" s="T786">стоять-PRS.[3SG]</ta>
            <ta e="T789" id="Seg_8494" s="T788">черемуха</ta>
            <ta e="T790" id="Seg_8495" s="T789">ягода-PL</ta>
            <ta e="T791" id="Seg_8496" s="T790">быть-PRS.[3SG]</ta>
            <ta e="T792" id="Seg_8497" s="T791">%%</ta>
            <ta e="T793" id="Seg_8498" s="T792">ягода-PL</ta>
            <ta e="T794" id="Seg_8499" s="T793">черный.[NOM.SG]</ta>
            <ta e="T795" id="Seg_8500" s="T794">ягода-PL</ta>
            <ta e="T796" id="Seg_8501" s="T795">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T798" id="Seg_8502" s="T797">ты.NOM</ta>
            <ta e="T799" id="Seg_8503" s="T798">дом-PL</ta>
            <ta e="T800" id="Seg_8504" s="T799">снимать.кору-PST-2SG</ta>
            <ta e="T802" id="Seg_8505" s="T801">снимать.кору-PST-2SG</ta>
            <ta e="T804" id="Seg_8506" s="T803">сколько</ta>
            <ta e="T805" id="Seg_8507" s="T804">дом.[NOM.SG]</ta>
            <ta e="T806" id="Seg_8508" s="T805">снимать.кору-PST-2SG</ta>
            <ta e="T808" id="Seg_8509" s="T807">семь.[NOM.SG]</ta>
            <ta e="T810" id="Seg_8510" s="T809">ты.NOM</ta>
            <ta e="T811" id="Seg_8511" s="T810">дом-PL-LAT</ta>
            <ta e="T812" id="Seg_8512" s="T811">снимать.кору-PST-2SG</ta>
            <ta e="T814" id="Seg_8513" s="T813">ты.NOM</ta>
            <ta e="T816" id="Seg_8514" s="T815">лес-LAT</ta>
            <ta e="T817" id="Seg_8515" s="T816">NEG</ta>
            <ta e="T818" id="Seg_8516" s="T817">идти-PST-2SG</ta>
            <ta e="T819" id="Seg_8517" s="T818">NEG</ta>
            <ta e="T820" id="Seg_8518" s="T819">идти-PST-1SG</ta>
            <ta e="T821" id="Seg_8519" s="T820">я.NOM</ta>
            <ta e="T822" id="Seg_8520" s="T821">бояться-PRS-1SG</ta>
            <ta e="T823" id="Seg_8521" s="T822">там</ta>
            <ta e="T824" id="Seg_8522" s="T823">PTCL</ta>
            <ta e="T825" id="Seg_8523" s="T824">медведь.[NOM.SG]</ta>
            <ta e="T826" id="Seg_8524" s="T825">жить.[3SG]</ta>
            <ta e="T829" id="Seg_8525" s="T828">что.[NOM.SG]</ta>
            <ta e="T830" id="Seg_8526" s="T829">бояться-PRS-2SG</ta>
            <ta e="T831" id="Seg_8527" s="T830">пойти-EP-IMP.2SG</ta>
            <ta e="T834" id="Seg_8528" s="T833">что.[NOM.SG]=INDEF</ta>
            <ta e="T835" id="Seg_8529" s="T834">NEG</ta>
            <ta e="T836" id="Seg_8530" s="T835">делать-FUT-3SG</ta>
            <ta e="T837" id="Seg_8531" s="T836">NEG</ta>
            <ta e="T838" id="Seg_8532" s="T837">делать-FUT-3SG</ta>
            <ta e="T839" id="Seg_8533" s="T838">ты.DAT</ta>
            <ta e="T841" id="Seg_8534" s="T840">спрятаться-EP-MOM-FUT-2SG</ta>
            <ta e="T842" id="Seg_8535" s="T841">тогда</ta>
            <ta e="T843" id="Seg_8536" s="T842">дом-LAT-2SG</ta>
            <ta e="T844" id="Seg_8537" s="T843">прийти-FUT-2SG</ta>
            <ta e="T846" id="Seg_8538" s="T845">кость-VBLZ-INF.LAT</ta>
            <ta e="T847" id="Seg_8539" s="T846">можно</ta>
            <ta e="T852" id="Seg_8540" s="T850">всё.равно</ta>
            <ta e="T853" id="Seg_8541" s="T852">я.NOM</ta>
            <ta e="T854" id="Seg_8542" s="T853">бояться-PRS-1SG</ta>
            <ta e="T855" id="Seg_8543" s="T854">я.NOM</ta>
            <ta e="T856" id="Seg_8544" s="T855">что.[NOM.SG]=INDEF</ta>
            <ta e="T857" id="Seg_8545" s="T856">NEG.EX.[3SG]</ta>
            <ta e="T858" id="Seg_8546" s="T857">ружье.[NOM.SG]</ta>
            <ta e="T859" id="Seg_8547" s="T858">ружье.[NOM.SG]</ta>
            <ta e="T860" id="Seg_8548" s="T859">NEG.EX.[3SG]</ta>
            <ta e="T861" id="Seg_8549" s="T860">нож-NOM/GEN/ACC.1SG</ta>
            <ta e="T862" id="Seg_8550" s="T861">NEG.EX.[3SG]</ta>
            <ta e="T864" id="Seg_8551" s="T863">только</ta>
            <ta e="T865" id="Seg_8552" s="T864">камень.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8553" s="T865">а</ta>
            <ta e="T867" id="Seg_8554" s="T866">этот.[NOM.SG]</ta>
            <ta e="T868" id="Seg_8555" s="T867">NEG</ta>
            <ta e="T869" id="Seg_8556" s="T868">болеть-IPFVZ-3SG</ta>
            <ta e="T871" id="Seg_8557" s="T870">этот.[NOM.SG]</ta>
            <ta e="T872" id="Seg_8558" s="T871">человек-ABL</ta>
            <ta e="T873" id="Seg_8559" s="T872">бояться-PRS.[3SG]</ta>
            <ta e="T874" id="Seg_8560" s="T873">бежать-MOM-FUT-3SG</ta>
            <ta e="T876" id="Seg_8561" s="T875">мы.NOM</ta>
            <ta e="T877" id="Seg_8562" s="T876">пойти-PST-1PL</ta>
            <ta e="T880" id="Seg_8563" s="T879">этот.[NOM.SG]</ta>
            <ta e="T881" id="Seg_8564" s="T880">лес-ABL</ta>
            <ta e="T882" id="Seg_8565" s="T881">лес-LAT</ta>
            <ta e="T884" id="Seg_8566" s="T883">тогда</ta>
            <ta e="T885" id="Seg_8567" s="T884">снег.[NOM.SG]</ta>
            <ta e="T886" id="Seg_8568" s="T885">пойти-MOM-PST.[3SG]</ta>
            <ta e="T888" id="Seg_8569" s="T887">этот-PL</ta>
            <ta e="T889" id="Seg_8570" s="T888">три-COLL</ta>
            <ta e="T890" id="Seg_8571" s="T889">пойти-PST-3PL</ta>
            <ta e="T891" id="Seg_8572" s="T890">а</ta>
            <ta e="T892" id="Seg_8573" s="T891">я.NOM</ta>
            <ta e="T893" id="Seg_8574" s="T892">вернуться-MOM-PST-1SG</ta>
            <ta e="T894" id="Seg_8575" s="T893">назад</ta>
            <ta e="T896" id="Seg_8576" s="T895">тогда</ta>
            <ta e="T897" id="Seg_8577" s="T896">слышать-PRS-1SG</ta>
            <ta e="T898" id="Seg_8578" s="T897">кто.[NOM.SG]=INDEF</ta>
            <ta e="T899" id="Seg_8579" s="T898">PTCL</ta>
            <ta e="T900" id="Seg_8580" s="T899">ветка-PL</ta>
            <ta e="T901" id="Seg_8581" s="T900">ветка-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T902" id="Seg_8582" s="T901">PTCL</ta>
            <ta e="T903" id="Seg_8583" s="T902">резать-DUR.[3SG]</ta>
            <ta e="T906" id="Seg_8584" s="T905">думать-1SG</ta>
            <ta e="T907" id="Seg_8585" s="T906">медведь.[NOM.SG]</ta>
            <ta e="T909" id="Seg_8586" s="T908">пойти-</ta>
            <ta e="T911" id="Seg_8587" s="T910">PTCL</ta>
            <ta e="T913" id="Seg_8588" s="T912">%%</ta>
            <ta e="T914" id="Seg_8589" s="T913">а</ta>
            <ta e="T915" id="Seg_8590" s="T914">видеть-INF.LAT</ta>
            <ta e="T916" id="Seg_8591" s="T915">NEG</ta>
            <ta e="T917" id="Seg_8592" s="T916">видеть-PST-1SG</ta>
            <ta e="T918" id="Seg_8593" s="T917">дом-LAT/LOC.1SG</ta>
            <ta e="T919" id="Seg_8594" s="T918">прийти-PST-1SG</ta>
            <ta e="T921" id="Seg_8595" s="T920">я.NOM</ta>
            <ta e="T922" id="Seg_8596" s="T921">прийти-PRS-1SG</ta>
            <ta e="T923" id="Seg_8597" s="T922">и</ta>
            <ta e="T924" id="Seg_8598" s="T923">пугать-MOM-PST-1SG</ta>
            <ta e="T925" id="Seg_8599" s="T924">PTCL</ta>
            <ta e="T927" id="Seg_8600" s="T926">дерево-3PL-LAT</ta>
            <ta e="T928" id="Seg_8601" s="T927">надо</ta>
            <ta e="T929" id="Seg_8602" s="T928">влезать-INF.LAT</ta>
            <ta e="T930" id="Seg_8603" s="T929">а</ta>
            <ta e="T931" id="Seg_8604" s="T930">я.NOM</ta>
            <ta e="T932" id="Seg_8605" s="T931">этот-LAT</ta>
            <ta e="T933" id="Seg_8606" s="T932">сказать-PST-1SG</ta>
            <ta e="T934" id="Seg_8607" s="T933">этот</ta>
            <ta e="T935" id="Seg_8608" s="T934">тоже</ta>
            <ta e="T936" id="Seg_8609" s="T935">дерево-PL-LAT</ta>
            <ta e="T937" id="Seg_8610" s="T936">влезать-PRS.[3SG]</ta>
            <ta e="T939" id="Seg_8611" s="T938">и</ta>
            <ta e="T940" id="Seg_8612" s="T939">дерево.[NOM.SG]</ta>
            <ta e="T941" id="Seg_8613" s="T940">сломать-PRS.[3SG]</ta>
            <ta e="T942" id="Seg_8614" s="T941">и</ta>
            <ta e="T943" id="Seg_8615" s="T942">земля-LAT</ta>
            <ta e="T944" id="Seg_8616" s="T943">нести-PRS.[3SG]</ta>
            <ta e="T946" id="Seg_8617" s="T945">как</ta>
            <ta e="T947" id="Seg_8618" s="T946">там</ta>
            <ta e="T948" id="Seg_8619" s="T947">говорить-INF.LAT</ta>
            <ta e="T949" id="Seg_8620" s="T948">ребенок-PL</ta>
            <ta e="T950" id="Seg_8621" s="T949">PTCL</ta>
            <ta e="T951" id="Seg_8622" s="T950">кричать-DUR.[3SG]</ta>
            <ta e="T953" id="Seg_8623" s="T952">свинья-PL</ta>
            <ta e="T954" id="Seg_8624" s="T953">тоже</ta>
            <ta e="T955" id="Seg_8625" s="T954">PTCL</ta>
            <ta e="T956" id="Seg_8626" s="T955">кричать-DUR-3PL</ta>
            <ta e="T958" id="Seg_8627" s="T957">тоже</ta>
            <ta e="T959" id="Seg_8628" s="T958">мычать-FRQ-DUR.[3SG]</ta>
            <ta e="T961" id="Seg_8629" s="T960">как=INDEF</ta>
            <ta e="T962" id="Seg_8630" s="T961">нельзя</ta>
            <ta e="T963" id="Seg_8631" s="T962">говорить-INF.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_8632" s="T1">dempro.[n:case]</ta>
            <ta e="T3" id="Seg_8633" s="T2">pers</ta>
            <ta e="T4" id="Seg_8634" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_8635" s="T4">%%</ta>
            <ta e="T7" id="Seg_8636" s="T6">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T8" id="Seg_8637" s="T7">ptcl</ta>
            <ta e="T11" id="Seg_8638" s="T10">v-v:n.fin</ta>
            <ta e="T13" id="Seg_8639" s="T12">adv</ta>
            <ta e="T14" id="Seg_8640" s="T13">adj.[n:case]</ta>
            <ta e="T15" id="Seg_8641" s="T14">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T17" id="Seg_8642" s="T16">pers</ta>
            <ta e="T18" id="Seg_8643" s="T17">adv</ta>
            <ta e="T19" id="Seg_8644" s="T18">%%</ta>
            <ta e="T20" id="Seg_8645" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_8646" s="T20">pers</ta>
            <ta e="T22" id="Seg_8647" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_8648" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_8649" s="T23">conj</ta>
            <ta e="T25" id="Seg_8650" s="T24">pers</ta>
            <ta e="T26" id="Seg_8651" s="T25">conj</ta>
            <ta e="T27" id="Seg_8652" s="T26">pers</ta>
            <ta e="T28" id="Seg_8653" s="T27">que.[n:case]=ptcl</ta>
            <ta e="T29" id="Seg_8654" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_8655" s="T29">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_8656" s="T31">pers</ta>
            <ta e="T33" id="Seg_8657" s="T32">pers</ta>
            <ta e="T34" id="Seg_8658" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_8659" s="T34">adj.[n:case]</ta>
            <ta e="T36" id="Seg_8660" s="T35">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_8661" s="T37">que=ptcl</ta>
            <ta e="T39" id="Seg_8662" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_8663" s="T39">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_8664" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_8665" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_8666" s="T46">adj.[n:case]</ta>
            <ta e="T48" id="Seg_8667" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_8668" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_8669" s="T49">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_8670" s="T51">pers</ta>
            <ta e="T53" id="Seg_8671" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_8672" s="T53">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_8673" s="T55">propr-n:case</ta>
            <ta e="T61" id="Seg_8674" s="T60">adv</ta>
            <ta e="T62" id="Seg_8675" s="T61">dempro-n:case</ta>
            <ta e="T63" id="Seg_8676" s="T62">v-v:tense.[v:pn]</ta>
            <ta e="T65" id="Seg_8677" s="T64">conj</ta>
            <ta e="T66" id="Seg_8678" s="T65">que.[n:case]</ta>
            <ta e="T67" id="Seg_8679" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_8680" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_8681" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_8682" s="T69">pers</ta>
            <ta e="T71" id="Seg_8683" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_8684" s="T71">n.[n:case]</ta>
            <ta e="T74" id="Seg_8685" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_8686" s="T74">n-n:case</ta>
            <ta e="T77" id="Seg_8687" s="T76">adv-adv&gt;adv</ta>
            <ta e="T78" id="Seg_8688" s="T77">ptcl</ta>
            <ta e="T80" id="Seg_8689" s="T79">n-n:case.poss</ta>
            <ta e="T81" id="Seg_8690" s="T80">num.[n:case]</ta>
            <ta e="T82" id="Seg_8691" s="T81">num.[n:case]</ta>
            <ta e="T84" id="Seg_8692" s="T83">adv</ta>
            <ta e="T85" id="Seg_8693" s="T84">pers</ta>
            <ta e="T86" id="Seg_8694" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_8695" s="T87">adv</ta>
            <ta e="T90" id="Seg_8696" s="T89">n.[n:case]</ta>
            <ta e="T91" id="Seg_8697" s="T90">adj.[n:case]</ta>
            <ta e="T93" id="Seg_8698" s="T92">conj</ta>
            <ta e="T94" id="Seg_8699" s="T93">adv</ta>
            <ta e="T95" id="Seg_8700" s="T94">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_8701" s="T95">adj</ta>
            <ta e="T97" id="Seg_8702" s="T96">n-n:case</ta>
            <ta e="T99" id="Seg_8703" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_8704" s="T99">adv</ta>
            <ta e="T101" id="Seg_8705" s="T100">adv-adv&gt;adv</ta>
            <ta e="T102" id="Seg_8706" s="T101">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T104" id="Seg_8707" s="T103">adv</ta>
            <ta e="T105" id="Seg_8708" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_8709" s="T105">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_8710" s="T107">conj</ta>
            <ta e="T109" id="Seg_8711" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_8712" s="T109">dempro.[n:case]</ta>
            <ta e="T111" id="Seg_8713" s="T110">n</ta>
            <ta e="T112" id="Seg_8714" s="T111">n-n:case</ta>
            <ta e="T114" id="Seg_8715" s="T113">conj</ta>
            <ta e="T115" id="Seg_8716" s="T114">n.[n:case]</ta>
            <ta e="T116" id="Seg_8717" s="T115">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T118" id="Seg_8718" s="T117">pers</ta>
            <ta e="T119" id="Seg_8719" s="T118">que</ta>
            <ta e="T120" id="Seg_8720" s="T119">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_8721" s="T121">%%-n:case</ta>
            <ta e="T123" id="Seg_8722" s="T122">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_8723" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_8724" s="T125">v-v:mood.pn</ta>
            <ta e="T128" id="Seg_8725" s="T127">n.[n:case]</ta>
            <ta e="T129" id="Seg_8726" s="T128">v-v:mood.pn</ta>
            <ta e="T131" id="Seg_8727" s="T130">propr-n:case.poss</ta>
            <ta e="T133" id="Seg_8728" s="T132">conj</ta>
            <ta e="T134" id="Seg_8729" s="T133">propr.[n:case]</ta>
            <ta e="T135" id="Seg_8730" s="T134">adv</ta>
            <ta e="T964" id="Seg_8731" s="T135">v-v:n-fin</ta>
            <ta e="T136" id="Seg_8732" s="T964">v-v:tense.[v:pn]</ta>
            <ta e="T137" id="Seg_8733" s="T136">propr-n:case</ta>
            <ta e="T139" id="Seg_8734" s="T138">n.[n:case]</ta>
            <ta e="T140" id="Seg_8735" s="T139">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T142" id="Seg_8736" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_8737" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_8738" s="T143">n-n:num</ta>
            <ta e="T145" id="Seg_8739" s="T144">v-v:n.fin</ta>
            <ta e="T147" id="Seg_8740" s="T146">dempro.[n:case]</ta>
            <ta e="T148" id="Seg_8741" s="T147">v-v:tense.[v:pn]</ta>
            <ta e="T149" id="Seg_8742" s="T148">adv</ta>
            <ta e="T150" id="Seg_8743" s="T149">n-n:num-n:case</ta>
            <ta e="T152" id="Seg_8744" s="T151">conj</ta>
            <ta e="T154" id="Seg_8745" s="T153">adv</ta>
            <ta e="T155" id="Seg_8746" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_8747" s="T155">adv-adv&gt;adv</ta>
            <ta e="T158" id="Seg_8748" s="T157">pers</ta>
            <ta e="T159" id="Seg_8749" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_8750" s="T159">n-n:case.poss</ta>
            <ta e="T965" id="Seg_8751" s="T160">v-v:n-fin</ta>
            <ta e="T161" id="Seg_8752" s="T965">v-v:tense.[v:pn]</ta>
            <ta e="T162" id="Seg_8753" s="T161">n-n:ins-n&gt;v-v:n.fin</ta>
            <ta e="T163" id="Seg_8754" s="T162">n-n:num</ta>
            <ta e="T165" id="Seg_8755" s="T164">v-v:n.fin</ta>
            <ta e="T167" id="Seg_8756" s="T166">refl-n:case.poss</ta>
            <ta e="T168" id="Seg_8757" s="T167">n-n:case.poss</ta>
            <ta e="T169" id="Seg_8758" s="T168">v-v:tense.[v:pn]</ta>
            <ta e="T171" id="Seg_8759" s="T170">adv</ta>
            <ta e="T173" id="Seg_8760" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_8761" s="T173">n-n:case.poss</ta>
            <ta e="T176" id="Seg_8762" s="T175">num.[n:case]</ta>
            <ta e="T177" id="Seg_8763" s="T176">n.[n:case]</ta>
            <ta e="T178" id="Seg_8764" s="T177">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T179" id="Seg_8765" s="T178">n.[n:case]</ta>
            <ta e="T181" id="Seg_8766" s="T180">v-v:n.fin</ta>
            <ta e="T183" id="Seg_8767" s="T182">n-n:case.poss-n:case</ta>
            <ta e="T185" id="Seg_8768" s="T184">conj</ta>
            <ta e="T186" id="Seg_8769" s="T185">adj.[n:case]</ta>
            <ta e="T187" id="Seg_8770" s="T186">n.[n:case]</ta>
            <ta e="T188" id="Seg_8771" s="T187">n.[n:case]</ta>
            <ta e="T189" id="Seg_8772" s="T188">v-v:n.fin</ta>
            <ta e="T190" id="Seg_8773" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_8774" s="T190">conj</ta>
            <ta e="T192" id="Seg_8775" s="T191">num.[n:case]</ta>
            <ta e="T193" id="Seg_8776" s="T192">n.[n:case]</ta>
            <ta e="T194" id="Seg_8777" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_8778" s="T194">%%</ta>
            <ta e="T196" id="Seg_8779" s="T195">v-v&gt;v.[v:pn]</ta>
            <ta e="T197" id="Seg_8780" s="T196">pers</ta>
            <ta e="T198" id="Seg_8781" s="T197">n-n:case.poss-n:case</ta>
            <ta e="T199" id="Seg_8782" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_8783" s="T199">n.[n:case]</ta>
            <ta e="T202" id="Seg_8784" s="T201">conj</ta>
            <ta e="T203" id="Seg_8785" s="T202">num.[n:case]</ta>
            <ta e="T204" id="Seg_8786" s="T203">n.[n:case]</ta>
            <ta e="T205" id="Seg_8787" s="T204">pers</ta>
            <ta e="T206" id="Seg_8788" s="T205">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_8789" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_8790" s="T207">n-n:num-n:case</ta>
            <ta e="T211" id="Seg_8791" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_8792" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_8793" s="T212">v-v:n.fin</ta>
            <ta e="T215" id="Seg_8794" s="T214">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_8795" s="T216">que-n:case</ta>
            <ta e="T218" id="Seg_8796" s="T217">v-v:n.fin</ta>
            <ta e="T219" id="Seg_8797" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_8798" s="T219">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_8799" s="T221">dempro.[n:case]</ta>
            <ta e="T223" id="Seg_8800" s="T222">dempro-n:case</ta>
            <ta e="T224" id="Seg_8801" s="T223">adj.[n:case]</ta>
            <ta e="T225" id="Seg_8802" s="T224">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_8803" s="T226">conj</ta>
            <ta e="T228" id="Seg_8804" s="T227">que-n:case</ta>
            <ta e="T229" id="Seg_8805" s="T228">v-v:n.fin</ta>
            <ta e="T231" id="Seg_8806" s="T230">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_8807" s="T232">dempro-n:case</ta>
            <ta e="T234" id="Seg_8808" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_8809" s="T234">adj.[n:case]</ta>
            <ta e="T236" id="Seg_8810" s="T235">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_8811" s="T237">refl-n:case.poss</ta>
            <ta e="T239" id="Seg_8812" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_8813" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_8814" s="T240">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_8815" s="T242">conj</ta>
            <ta e="T244" id="Seg_8816" s="T243">que.[n:case]</ta>
            <ta e="T246" id="Seg_8817" s="T245">conj</ta>
            <ta e="T247" id="Seg_8818" s="T246">que.[n:case]</ta>
            <ta e="T248" id="Seg_8819" s="T247">v-v:tense.[v:pn]</ta>
            <ta e="T249" id="Seg_8820" s="T248">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T251" id="Seg_8821" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_8822" s="T251">num.[n:case]</ta>
            <ta e="T253" id="Seg_8823" s="T252">n-n:num</ta>
            <ta e="T254" id="Seg_8824" s="T253">v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_8825" s="T255">adv</ta>
            <ta e="T257" id="Seg_8826" s="T256">num.[n:case]</ta>
            <ta e="T258" id="Seg_8827" s="T257">n.[n:case]</ta>
            <ta e="T259" id="Seg_8828" s="T258">adv</ta>
            <ta e="T260" id="Seg_8829" s="T259">adj.[n:case]</ta>
            <ta e="T261" id="Seg_8830" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_8831" s="T261">conj</ta>
            <ta e="T263" id="Seg_8832" s="T262">num.[n:case]</ta>
            <ta e="T264" id="Seg_8833" s="T263">n.[n:case]</ta>
            <ta e="T265" id="Seg_8834" s="T264">adv</ta>
            <ta e="T266" id="Seg_8835" s="T265">v-v:tense.[v:pn]</ta>
            <ta e="T268" id="Seg_8836" s="T267">dempro-n:num</ta>
            <ta e="T269" id="Seg_8837" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_8838" s="T269">v-v:n.fin</ta>
            <ta e="T271" id="Seg_8839" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_8840" s="T271">n.[n:case]</ta>
            <ta e="T273" id="Seg_8841" s="T272">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_8842" s="T274">conj</ta>
            <ta e="T276" id="Seg_8843" s="T275">dempro.[n:case]</ta>
            <ta e="T277" id="Seg_8844" s="T276">num.[n:case]</ta>
            <ta e="T278" id="Seg_8845" s="T277">n.[n:case]</ta>
            <ta e="T279" id="Seg_8846" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_8847" s="T279">v-v:tense.[v:pn]</ta>
            <ta e="T281" id="Seg_8848" s="T280">n.[n:case]</ta>
            <ta e="T283" id="Seg_8849" s="T282">n-n:num</ta>
            <ta e="T284" id="Seg_8850" s="T283">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_8851" s="T285">conj</ta>
            <ta e="T287" id="Seg_8852" s="T286">n.[n:case]</ta>
            <ta e="T288" id="Seg_8853" s="T287">refl-n:case.poss</ta>
            <ta e="T289" id="Seg_8854" s="T288">v-v:tense.[v:pn]</ta>
            <ta e="T291" id="Seg_8855" s="T290">n-n:num-n:case</ta>
            <ta e="T294" id="Seg_8856" s="T293">dempro-n:num</ta>
            <ta e="T295" id="Seg_8857" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_8858" s="T295">num.[n:case]</ta>
            <ta e="T297" id="Seg_8859" s="T296">v-v:tense.[v:pn]</ta>
            <ta e="T299" id="Seg_8860" s="T298">n-n:num</ta>
            <ta e="T300" id="Seg_8861" s="T299">v-v:tense.[v:pn]</ta>
            <ta e="T301" id="Seg_8862" s="T300">conj</ta>
            <ta e="T302" id="Seg_8863" s="T301">v-v:tense.[v:pn]</ta>
            <ta e="T303" id="Seg_8864" s="T302">conj</ta>
            <ta e="T304" id="Seg_8865" s="T303">num.[n:case]</ta>
            <ta e="T305" id="Seg_8866" s="T304">n-n:num-n:case</ta>
            <ta e="T307" id="Seg_8867" s="T306">v-v:tense.[v:pn]</ta>
            <ta e="T308" id="Seg_8868" s="T307">refl-n:case.poss</ta>
            <ta e="T309" id="Seg_8869" s="T308">n.[n:case]</ta>
            <ta e="T311" id="Seg_8870" s="T310">conj</ta>
            <ta e="T312" id="Seg_8871" s="T311">dempro.[n:case]</ta>
            <ta e="T313" id="Seg_8872" s="T312">adv</ta>
            <ta e="T314" id="Seg_8873" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_8874" s="T314">n-n:num-n:case</ta>
            <ta e="T316" id="Seg_8875" s="T315">v-v&gt;v.[v:pn]</ta>
            <ta e="T317" id="Seg_8876" s="T316">dempro</ta>
            <ta e="T318" id="Seg_8877" s="T317">dempro-n:num</ta>
            <ta e="T319" id="Seg_8878" s="T318">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_8879" s="T319">que.[n:case]</ta>
            <ta e="T321" id="Seg_8880" s="T320">pers</ta>
            <ta e="T322" id="Seg_8881" s="T321">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_8882" s="T323">pers</ta>
            <ta e="T325" id="Seg_8883" s="T324">adv-n:case</ta>
            <ta e="T327" id="Seg_8884" s="T326">v-v:tense-v:pn</ta>
            <ta e="T329" id="Seg_8885" s="T328">n-n:case.poss</ta>
            <ta e="T331" id="Seg_8886" s="T330">conj</ta>
            <ta e="T333" id="Seg_8887" s="T332">que-n:case</ta>
            <ta e="T334" id="Seg_8888" s="T333">adj.[n:case]</ta>
            <ta e="T335" id="Seg_8889" s="T334">n-n:case.poss</ta>
            <ta e="T336" id="Seg_8890" s="T335">v-v:tense-v:pn</ta>
            <ta e="T337" id="Seg_8891" s="T336">pers</ta>
            <ta e="T338" id="Seg_8892" s="T337">conj</ta>
            <ta e="T339" id="Seg_8893" s="T338">pers</ta>
            <ta e="T341" id="Seg_8894" s="T340">adv</ta>
            <ta e="T342" id="Seg_8895" s="T341">adj.[n:case]</ta>
            <ta e="T343" id="Seg_8896" s="T342">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T344" id="Seg_8897" s="T343">n.[n:case]</ta>
            <ta e="T345" id="Seg_8898" s="T344">n.[n:case]</ta>
            <ta e="T346" id="Seg_8899" s="T345">v-v:tense.[v:pn]</ta>
            <ta e="T348" id="Seg_8900" s="T347">adv</ta>
            <ta e="T349" id="Seg_8901" s="T348">dempro-n:num</ta>
            <ta e="T351" id="Seg_8902" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_8903" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_8904" s="T352">que</ta>
            <ta e="T354" id="Seg_8905" s="T353">n.[n:case]</ta>
            <ta e="T356" id="Seg_8906" s="T355">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_8907" s="T356">n.[n:case]</ta>
            <ta e="T358" id="Seg_8908" s="T357">v-v:tense.[v:pn]</ta>
            <ta e="T359" id="Seg_8909" s="T358">n.[n:case]</ta>
            <ta e="T361" id="Seg_8910" s="T360">v-v&gt;v-v:mood.pn</ta>
            <ta e="T362" id="Seg_8911" s="T361">pers</ta>
            <ta e="T364" id="Seg_8912" s="T363">dempro-n:num</ta>
            <ta e="T365" id="Seg_8913" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_8914" s="T365">v-v:tense-v:n.fin</ta>
            <ta e="T367" id="Seg_8915" s="T366">adv</ta>
            <ta e="T368" id="Seg_8916" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8917" s="T368">v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_8918" s="T369">pers</ta>
            <ta e="T371" id="Seg_8919" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_8920" s="T371">n-n:case</ta>
            <ta e="T374" id="Seg_8921" s="T373">adv</ta>
            <ta e="T375" id="Seg_8922" s="T374">v-v:tense.[v:pn]</ta>
            <ta e="T376" id="Seg_8923" s="T375">dempro-n:num</ta>
            <ta e="T377" id="Seg_8924" s="T376">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_8925" s="T378">que</ta>
            <ta e="T380" id="Seg_8926" s="T379">n.[n:case]</ta>
            <ta e="T382" id="Seg_8927" s="T381">n-n:num-n:case.poss</ta>
            <ta e="T383" id="Seg_8928" s="T382">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_8929" s="T384">conj</ta>
            <ta e="T386" id="Seg_8930" s="T385">adv</ta>
            <ta e="T387" id="Seg_8931" s="T386">n.[n:case]</ta>
            <ta e="T389" id="Seg_8932" s="T388">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_8933" s="T389">dempro.[n:case]</ta>
            <ta e="T391" id="Seg_8934" s="T390">adv</ta>
            <ta e="T392" id="Seg_8935" s="T391">v-v:tense.[v:pn]</ta>
            <ta e="T394" id="Seg_8936" s="T393">v-v:mood.pn</ta>
            <ta e="T396" id="Seg_8937" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_8938" s="T396">adv</ta>
            <ta e="T398" id="Seg_8939" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_8940" s="T398">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_8941" s="T399">n.[n:case]</ta>
            <ta e="T402" id="Seg_8942" s="T401">adv</ta>
            <ta e="T403" id="Seg_8943" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_8944" s="T403">v-v:tense.[v:pn]</ta>
            <ta e="T405" id="Seg_8945" s="T404">dempro-n:num</ta>
            <ta e="T406" id="Seg_8946" s="T405">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_8947" s="T407">conj</ta>
            <ta e="T409" id="Seg_8948" s="T408">v-v:tense-v:pn</ta>
            <ta e="T410" id="Seg_8949" s="T409">adv</ta>
            <ta e="T411" id="Seg_8950" s="T410">num-n:case</ta>
            <ta e="T413" id="Seg_8951" s="T412">num-n:case</ta>
            <ta e="T414" id="Seg_8952" s="T413">n-n:case.poss</ta>
            <ta e="T415" id="Seg_8953" s="T414">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_8954" s="T416">v-v&gt;v-v:mood.pn</ta>
            <ta e="T418" id="Seg_8955" s="T417">pers</ta>
            <ta e="T419" id="Seg_8956" s="T418">dempro.[n:case]</ta>
            <ta e="T420" id="Seg_8957" s="T419">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T421" id="Seg_8958" s="T420">conj</ta>
            <ta e="T422" id="Seg_8959" s="T421">n-n:case.poss</ta>
            <ta e="T424" id="Seg_8960" s="T423">v-v:tense.[v:pn]</ta>
            <ta e="T425" id="Seg_8961" s="T424">dempro.[n:case]</ta>
            <ta e="T426" id="Seg_8962" s="T425">v-v:tense.[v:pn]</ta>
            <ta e="T427" id="Seg_8963" s="T426">n.[n:case]</ta>
            <ta e="T428" id="Seg_8964" s="T427">v-v&gt;v-v:mood.pn</ta>
            <ta e="T430" id="Seg_8965" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_8966" s="T430">v-v:tense-v:pn</ta>
            <ta e="T433" id="Seg_8967" s="T432">adv</ta>
            <ta e="T434" id="Seg_8968" s="T433">dempro.[n:case]</ta>
            <ta e="T435" id="Seg_8969" s="T434">v-v:n.fin</ta>
            <ta e="T436" id="Seg_8970" s="T435">v</ta>
            <ta e="T437" id="Seg_8971" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_8972" s="T437">v-v:tense.[v:pn]</ta>
            <ta e="T439" id="Seg_8973" s="T438">adv</ta>
            <ta e="T441" id="Seg_8974" s="T440">v-v:tense.[v:pn]</ta>
            <ta e="T442" id="Seg_8975" s="T441">adv</ta>
            <ta e="T444" id="Seg_8976" s="T443">adv</ta>
            <ta e="T445" id="Seg_8977" s="T444">n-n:case.poss</ta>
            <ta e="T447" id="Seg_8978" s="T446">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T448" id="Seg_8979" s="T447">conj</ta>
            <ta e="T449" id="Seg_8980" s="T448">dempro.[n:case]</ta>
            <ta e="T450" id="Seg_8981" s="T449">n.[n:case]</ta>
            <ta e="T451" id="Seg_8982" s="T450">v-v:tense.[v:pn]</ta>
            <ta e="T452" id="Seg_8983" s="T451">adj.[n:case]</ta>
            <ta e="T453" id="Seg_8984" s="T452">n-n:case</ta>
            <ta e="T455" id="Seg_8985" s="T454">dempro.[n:case]</ta>
            <ta e="T456" id="Seg_8986" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_8987" s="T456">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T458" id="Seg_8988" s="T457">n-n:case.poss</ta>
            <ta e="T460" id="Seg_8989" s="T459">conj</ta>
            <ta e="T462" id="Seg_8990" s="T461">adv</ta>
            <ta e="T463" id="Seg_8991" s="T462">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T464" id="Seg_8992" s="T463">conj</ta>
            <ta e="T966" id="Seg_8993" s="T464">v-v:n-fin</ta>
            <ta e="T465" id="Seg_8994" s="T966">v-v:tense.[v:pn]</ta>
            <ta e="T466" id="Seg_8995" s="T465">conj</ta>
            <ta e="T467" id="Seg_8996" s="T466">adv</ta>
            <ta e="T468" id="Seg_8997" s="T467">que=ptcl</ta>
            <ta e="T469" id="Seg_8998" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_8999" s="T469">v-v:tense.[v:pn]</ta>
            <ta e="T473" id="Seg_9000" s="T472">v-v:tense-v:pn</ta>
            <ta e="T474" id="Seg_9001" s="T473">dempro-n:case</ta>
            <ta e="T476" id="Seg_9002" s="T475">conj</ta>
            <ta e="T967" id="Seg_9003" s="T476">v-v:n-fin</ta>
            <ta e="T477" id="Seg_9004" s="T967">v-v:tense.[v:pn]</ta>
            <ta e="T479" id="Seg_9005" s="T478">adv</ta>
            <ta e="T481" id="Seg_9006" s="T480">pers</ta>
            <ta e="T482" id="Seg_9007" s="T481">n-n:num</ta>
            <ta e="T483" id="Seg_9008" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_9009" s="T483">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_9010" s="T485">num.[n:case]</ta>
            <ta e="T487" id="Seg_9011" s="T486">num.[n:case]</ta>
            <ta e="T489" id="Seg_9012" s="T488">pers</ta>
            <ta e="T490" id="Seg_9013" s="T489">n-n:case.poss</ta>
            <ta e="T491" id="Seg_9014" s="T490">v-v:tense.[v:pn]</ta>
            <ta e="T493" id="Seg_9015" s="T492">v-v:tense.[v:pn]</ta>
            <ta e="T494" id="Seg_9016" s="T493">conj</ta>
            <ta e="T495" id="Seg_9017" s="T494">que</ta>
            <ta e="T496" id="Seg_9018" s="T495">v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_9019" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_9020" s="T498">num.[n:case]</ta>
            <ta e="T500" id="Seg_9021" s="T499">n.[n:case]</ta>
            <ta e="T501" id="Seg_9022" s="T500">v-v:tense.[v:pn]</ta>
            <ta e="T504" id="Seg_9023" s="T503">propr-n:num-n:case</ta>
            <ta e="T506" id="Seg_9024" s="T505">num.[n:case]</ta>
            <ta e="T507" id="Seg_9025" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_9026" s="T507">num.[n:case]</ta>
            <ta e="T509" id="Seg_9027" s="T508">n-n:num-n:case</ta>
            <ta e="T510" id="Seg_9028" s="T509">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_9029" s="T511">dempro.[n:case]</ta>
            <ta e="T513" id="Seg_9030" s="T512">dempro-n:case</ta>
            <ta e="T514" id="Seg_9031" s="T513">n-n:case-%%</ta>
            <ta e="T515" id="Seg_9032" s="T514">v-v:tense.[v:pn]</ta>
            <ta e="T517" id="Seg_9033" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_9034" s="T517">v-v:tense.[v:pn]</ta>
            <ta e="T520" id="Seg_9035" s="T519">que-n:case=ptcl</ta>
            <ta e="T521" id="Seg_9036" s="T520">aux-v:mood.pn</ta>
            <ta e="T523" id="Seg_9037" s="T522">aux-v:mood.pn</ta>
            <ta e="T524" id="Seg_9038" s="T523">v-v&gt;v-v:pn</ta>
            <ta e="T526" id="Seg_9039" s="T525">adv</ta>
            <ta e="T968" id="Seg_9040" s="T526">v-v:n-fin</ta>
            <ta e="T527" id="Seg_9041" s="T968">v-v:tense.[v:pn]</ta>
            <ta e="T529" id="Seg_9042" s="T528">n.[n:case]</ta>
            <ta e="T530" id="Seg_9043" s="T529">v-v:tense.[v:pn]</ta>
            <ta e="T531" id="Seg_9044" s="T530">v-v:mood.pn</ta>
            <ta e="T532" id="Seg_9045" s="T531">n.[n:case]</ta>
            <ta e="T534" id="Seg_9046" s="T533">dempro-n:num</ta>
            <ta e="T535" id="Seg_9047" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_9048" s="T535">v-v&gt;v-v:pn</ta>
            <ta e="T538" id="Seg_9049" s="T537">pers</ta>
            <ta e="T539" id="Seg_9050" s="T538">n-n:case.poss</ta>
            <ta e="T540" id="Seg_9051" s="T539">v-v:tense.[v:pn]</ta>
            <ta e="T541" id="Seg_9052" s="T540">que-n:case=ptcl</ta>
            <ta e="T542" id="Seg_9053" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_9054" s="T542">v-v&gt;v-v:n.fin</ta>
            <ta e="T545" id="Seg_9055" s="T544">adv</ta>
            <ta e="T546" id="Seg_9056" s="T545">n.[n:case]</ta>
            <ta e="T969" id="Seg_9057" s="T546">v-v:n-fin</ta>
            <ta e="T547" id="Seg_9058" s="T969">v-v:tense.[v:pn]</ta>
            <ta e="T548" id="Seg_9059" s="T547">n-n:case.poss</ta>
            <ta e="T549" id="Seg_9060" s="T548">v-v:tense.[v:pn]</ta>
            <ta e="T551" id="Seg_9061" s="T550">v-v:mood.pn</ta>
            <ta e="T552" id="Seg_9062" s="T551">n.[n:case]</ta>
            <ta e="T553" id="Seg_9063" s="T552">pers</ta>
            <ta e="T554" id="Seg_9064" s="T553">v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_9065" s="T554">n.[n:case]</ta>
            <ta e="T557" id="Seg_9066" s="T556">v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_9067" s="T557">n.[n:case]</ta>
            <ta e="T559" id="Seg_9068" s="T558">pers</ta>
            <ta e="T560" id="Seg_9069" s="T559">n</ta>
            <ta e="T561" id="Seg_9070" s="T560">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_9071" s="T562">adv</ta>
            <ta e="T564" id="Seg_9072" s="T563">dempro-n:num</ta>
            <ta e="T565" id="Seg_9073" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_9074" s="T565">v-v:tense-v:pn</ta>
            <ta e="T567" id="Seg_9075" s="T566">adv</ta>
            <ta e="T568" id="Seg_9076" s="T567">n-n:case.poss</ta>
            <ta e="T569" id="Seg_9077" s="T568">adv</ta>
            <ta e="T570" id="Seg_9078" s="T569">v-v:tense.[v:pn]</ta>
            <ta e="T572" id="Seg_9079" s="T571">aux-v:mood.pn</ta>
            <ta e="T573" id="Seg_9080" s="T572">v-v&gt;v-v:mood.pn</ta>
            <ta e="T574" id="Seg_9081" s="T573">que-n:case=ptcl</ta>
            <ta e="T576" id="Seg_9082" s="T575">adv</ta>
            <ta e="T970" id="Seg_9083" s="T576">v-v:n-fin</ta>
            <ta e="T577" id="Seg_9084" s="T970">v-v:tense.[v:pn]</ta>
            <ta e="T579" id="Seg_9085" s="T578">que-n:case=ptcl</ta>
            <ta e="T580" id="Seg_9086" s="T579">aux-v:mood.pn</ta>
            <ta e="T581" id="Seg_9087" s="T580">v-v&gt;v</ta>
            <ta e="T583" id="Seg_9088" s="T582">adv</ta>
            <ta e="T584" id="Seg_9089" s="T583">adv</ta>
            <ta e="T585" id="Seg_9090" s="T584">v-v:tense.[v:pn]</ta>
            <ta e="T586" id="Seg_9091" s="T585">n.[n:case]</ta>
            <ta e="T588" id="Seg_9092" s="T587">n-n:num</ta>
            <ta e="T589" id="Seg_9093" s="T588">n-n:num</ta>
            <ta e="T590" id="Seg_9094" s="T589">v-v:pn</ta>
            <ta e="T591" id="Seg_9095" s="T590">pers</ta>
            <ta e="T593" id="Seg_9096" s="T592">ptcl</ta>
            <ta e="T595" id="Seg_9097" s="T594">pers</ta>
            <ta e="T596" id="Seg_9098" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_9099" s="T596">v-v:n.fin</ta>
            <ta e="T599" id="Seg_9100" s="T598">conj</ta>
            <ta e="T600" id="Seg_9101" s="T599">pers</ta>
            <ta e="T601" id="Seg_9102" s="T600">pers</ta>
            <ta e="T602" id="Seg_9103" s="T601">n</ta>
            <ta e="T603" id="Seg_9104" s="T602">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_9105" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_9106" s="T604">pers</ta>
            <ta e="T607" id="Seg_9107" s="T606">n-n:case.poss</ta>
            <ta e="T608" id="Seg_9108" s="T607">n-n:case.poss</ta>
            <ta e="T609" id="Seg_9109" s="T608">adj.[n:case]</ta>
            <ta e="T611" id="Seg_9110" s="T610">conj</ta>
            <ta e="T612" id="Seg_9111" s="T611">pers</ta>
            <ta e="T613" id="Seg_9112" s="T612">n-n:case.poss</ta>
            <ta e="T614" id="Seg_9113" s="T613">adv</ta>
            <ta e="T615" id="Seg_9114" s="T614">adj.[n:case]</ta>
            <ta e="T617" id="Seg_9115" s="T616">adv</ta>
            <ta e="T618" id="Seg_9116" s="T617">n.[n:case]</ta>
            <ta e="T971" id="Seg_9117" s="T618">v-v:n-fin</ta>
            <ta e="T619" id="Seg_9118" s="T971">v-v:tense.[v:pn]</ta>
            <ta e="T620" id="Seg_9119" s="T619">adv</ta>
            <ta e="T621" id="Seg_9120" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_9121" s="T621">v-v:tense.[v:pn]</ta>
            <ta e="T624" id="Seg_9122" s="T623">dempro-n:num</ta>
            <ta e="T625" id="Seg_9123" s="T624">v-v:tense-v:pn</ta>
            <ta e="T626" id="Seg_9124" s="T625">v-v:tense.[v:pn]</ta>
            <ta e="T628" id="Seg_9125" s="T627">adv</ta>
            <ta e="T629" id="Seg_9126" s="T628">que-n:case=ptcl</ta>
            <ta e="T630" id="Seg_9127" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_9128" s="T630">v-v&gt;v</ta>
            <ta e="T633" id="Seg_9129" s="T632">conj</ta>
            <ta e="T634" id="Seg_9130" s="T633">n.[n:case]</ta>
            <ta e="T635" id="Seg_9131" s="T634">v-v:tense.[v:pn]</ta>
            <ta e="T636" id="Seg_9132" s="T635">n-n:case</ta>
            <ta e="T638" id="Seg_9133" s="T637">n-n:case.poss</ta>
            <ta e="T640" id="Seg_9134" s="T639">v-v:tense.[v:pn]</ta>
            <ta e="T641" id="Seg_9135" s="T640">adj.[n:case]</ta>
            <ta e="T643" id="Seg_9136" s="T642">adv</ta>
            <ta e="T644" id="Seg_9137" s="T643">v-v:tense.[v:pn]</ta>
            <ta e="T645" id="Seg_9138" s="T644">adv</ta>
            <ta e="T647" id="Seg_9139" s="T646">dempro.[n:case]</ta>
            <ta e="T648" id="Seg_9140" s="T647">v-v:tense.[v:pn]</ta>
            <ta e="T649" id="Seg_9141" s="T648">pers</ta>
            <ta e="T650" id="Seg_9142" s="T649">pers</ta>
            <ta e="T651" id="Seg_9143" s="T650">n-n:case.poss</ta>
            <ta e="T652" id="Seg_9144" s="T651">v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_9145" s="T652">n.[n:case]</ta>
            <ta e="T655" id="Seg_9146" s="T654">n.[n:case]</ta>
            <ta e="T656" id="Seg_9147" s="T655">v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_9148" s="T657">v-v:tense-v:pn</ta>
            <ta e="T659" id="Seg_9149" s="T658">pers</ta>
            <ta e="T661" id="Seg_9150" s="T660">n.[n:case]</ta>
            <ta e="T663" id="Seg_9151" s="T662">dempro-n:num</ta>
            <ta e="T664" id="Seg_9152" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_9153" s="T664">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T667" id="Seg_9154" s="T666">dempro.[n:case]</ta>
            <ta e="T668" id="Seg_9155" s="T667">dempro-n:case</ta>
            <ta e="T669" id="Seg_9156" s="T668">ptcl</ta>
            <ta e="T670" id="Seg_9157" s="T669">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T672" id="Seg_9158" s="T671">n-n:case.poss</ta>
            <ta e="T673" id="Seg_9159" s="T672">v-v:tense.[v:pn]</ta>
            <ta e="T675" id="Seg_9160" s="T674">n-n:num-n:case.poss</ta>
            <ta e="T676" id="Seg_9161" s="T675">v.[v:pn]</ta>
            <ta e="T677" id="Seg_9162" s="T676">num.[n:case]</ta>
            <ta e="T680" id="Seg_9163" s="T679">adj.[n:case]</ta>
            <ta e="T681" id="Seg_9164" s="T680">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T683" id="Seg_9165" s="T682">adv</ta>
            <ta e="T684" id="Seg_9166" s="T683">v-v&gt;v.[v:pn]</ta>
            <ta e="T685" id="Seg_9167" s="T684">n.[n:case]</ta>
            <ta e="T686" id="Seg_9168" s="T685">quant</ta>
            <ta e="T687" id="Seg_9169" s="T686">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T689" id="Seg_9170" s="T688">pers</ta>
            <ta e="T690" id="Seg_9171" s="T689">adv</ta>
            <ta e="T691" id="Seg_9172" s="T690">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_9173" s="T691">n-n:case</ta>
            <ta e="T698" id="Seg_9174" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_9175" s="T698">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_9176" s="T699">dempro.[n:case]</ta>
            <ta e="T701" id="Seg_9177" s="T700">pers</ta>
            <ta e="T702" id="Seg_9178" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9179" s="T702">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T972" id="Seg_9180" s="T703">v-v:n-fin</ta>
            <ta e="T704" id="Seg_9181" s="T972">v-v:tense.[v:pn]</ta>
            <ta e="T706" id="Seg_9182" s="T705">adv</ta>
            <ta e="T707" id="Seg_9183" s="T706">n.[n:case]</ta>
            <ta e="T708" id="Seg_9184" s="T707">v-v:tense.[v:pn]</ta>
            <ta e="T709" id="Seg_9185" s="T708">n-n:case</ta>
            <ta e="T711" id="Seg_9186" s="T710">n.[n:case]</ta>
            <ta e="T712" id="Seg_9187" s="T711">v-v&gt;v.[v:pn]</ta>
            <ta e="T715" id="Seg_9188" s="T714">v-v:mood-v:pn</ta>
            <ta e="T717" id="Seg_9189" s="T716">v-v:pn</ta>
            <ta e="T718" id="Seg_9190" s="T717">dempro.[n:case]</ta>
            <ta e="T719" id="Seg_9191" s="T718">n-n:case</ta>
            <ta e="T722" id="Seg_9192" s="T721">n.[n:case]</ta>
            <ta e="T723" id="Seg_9193" s="T722">v-v:tense.[v:pn]</ta>
            <ta e="T726" id="Seg_9194" s="T725">conj</ta>
            <ta e="T727" id="Seg_9195" s="T726">dempro.[n:case]</ta>
            <ta e="T728" id="Seg_9196" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9197" s="T728">v-v:tense.[v:pn]</ta>
            <ta e="T730" id="Seg_9198" s="T729">v-v:mood.[v:pn]</ta>
            <ta e="T731" id="Seg_9199" s="T730">adv</ta>
            <ta e="T733" id="Seg_9200" s="T732">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T734" id="Seg_9201" s="T733">que-n:case-n:case.poss</ta>
            <ta e="T735" id="Seg_9202" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_9203" s="T735">n.[n:case]</ta>
            <ta e="T738" id="Seg_9204" s="T737">adv</ta>
            <ta e="T739" id="Seg_9205" s="T738">adv</ta>
            <ta e="T741" id="Seg_9206" s="T740">n-n:num</ta>
            <ta e="T743" id="Seg_9207" s="T742">v-v&gt;v.[v:pn]</ta>
            <ta e="T744" id="Seg_9208" s="T743">conj</ta>
            <ta e="T745" id="Seg_9209" s="T744">adv</ta>
            <ta e="T746" id="Seg_9210" s="T745">n-n:case.poss</ta>
            <ta e="T973" id="Seg_9211" s="T746">v-v:n-fin</ta>
            <ta e="T747" id="Seg_9212" s="T973">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_9213" s="T748">v-v:mood-v:pn</ta>
            <ta e="T750" id="Seg_9214" s="T749">que=ptcl</ta>
            <ta e="T751" id="Seg_9215" s="T750">pers-n:case</ta>
            <ta e="T753" id="Seg_9216" s="T752">v-v:n.fin</ta>
            <ta e="T755" id="Seg_9217" s="T754">conj</ta>
            <ta e="T756" id="Seg_9218" s="T755">que.[n:case]</ta>
            <ta e="T757" id="Seg_9219" s="T756">adv</ta>
            <ta e="T758" id="Seg_9220" s="T757">v-v:tense-v:pn</ta>
            <ta e="T760" id="Seg_9221" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_9222" s="T760">que.[n:case]</ta>
            <ta e="T762" id="Seg_9223" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_9224" s="T762">n.[n:case]</ta>
            <ta e="T764" id="Seg_9225" s="T763">v-v:tense-v:pn</ta>
            <ta e="T765" id="Seg_9226" s="T764">n.[n:case]</ta>
            <ta e="T766" id="Seg_9227" s="T765">v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_9228" s="T767">v-v:tense-v:pn</ta>
            <ta e="T769" id="Seg_9229" s="T768">n-n:case</ta>
            <ta e="T770" id="Seg_9230" s="T769">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_9231" s="T771">adv</ta>
            <ta e="T773" id="Seg_9232" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_9233" s="T773">que.[n:case]</ta>
            <ta e="T775" id="Seg_9234" s="T774">quant</ta>
            <ta e="T776" id="Seg_9235" s="T775">n.[n:case]</ta>
            <ta e="T777" id="Seg_9236" s="T776">quant</ta>
            <ta e="T778" id="Seg_9237" s="T777">n.[n:case]</ta>
            <ta e="T779" id="Seg_9238" s="T778">n.[n:case]</ta>
            <ta e="T781" id="Seg_9239" s="T780">n.[n:case]</ta>
            <ta e="T782" id="Seg_9240" s="T781">v-v&gt;v-v:n.fin.[v:pn]</ta>
            <ta e="T784" id="Seg_9241" s="T783">n.[n:case]</ta>
            <ta e="T786" id="Seg_9242" s="T785">n.[n:case]</ta>
            <ta e="T787" id="Seg_9243" s="T786">v-v:tense.[v:pn]</ta>
            <ta e="T789" id="Seg_9244" s="T788">n</ta>
            <ta e="T790" id="Seg_9245" s="T789">n-n:num</ta>
            <ta e="T791" id="Seg_9246" s="T790">v-v:tense.[v:pn]</ta>
            <ta e="T792" id="Seg_9247" s="T791">adj</ta>
            <ta e="T793" id="Seg_9248" s="T792">n-n:num</ta>
            <ta e="T794" id="Seg_9249" s="T793">adj.[n:case]</ta>
            <ta e="T795" id="Seg_9250" s="T794">n-n:num</ta>
            <ta e="T796" id="Seg_9251" s="T795">v-v:mood-v:pn</ta>
            <ta e="T798" id="Seg_9252" s="T797">pers</ta>
            <ta e="T799" id="Seg_9253" s="T798">n-n:num</ta>
            <ta e="T800" id="Seg_9254" s="T799">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_9255" s="T801">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_9256" s="T803">adv</ta>
            <ta e="T805" id="Seg_9257" s="T804">n.[n:case]</ta>
            <ta e="T806" id="Seg_9258" s="T805">v-v:tense-v:pn</ta>
            <ta e="T808" id="Seg_9259" s="T807">num.[n:case]</ta>
            <ta e="T810" id="Seg_9260" s="T809">pers</ta>
            <ta e="T811" id="Seg_9261" s="T810">n-n:num-n:case</ta>
            <ta e="T812" id="Seg_9262" s="T811">v-v:tense-v:pn</ta>
            <ta e="T814" id="Seg_9263" s="T813">pers</ta>
            <ta e="T816" id="Seg_9264" s="T815">n-n:case</ta>
            <ta e="T817" id="Seg_9265" s="T816">ptcl</ta>
            <ta e="T818" id="Seg_9266" s="T817">v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_9267" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_9268" s="T819">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_9269" s="T820">pers</ta>
            <ta e="T822" id="Seg_9270" s="T821">v-v:tense-v:pn</ta>
            <ta e="T823" id="Seg_9271" s="T822">adv</ta>
            <ta e="T824" id="Seg_9272" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_9273" s="T824">n.[n:case]</ta>
            <ta e="T826" id="Seg_9274" s="T825">v.[v:pn]</ta>
            <ta e="T829" id="Seg_9275" s="T828">que.[n:case]</ta>
            <ta e="T830" id="Seg_9276" s="T829">v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_9277" s="T830">v-v:ins-v:mood.pn</ta>
            <ta e="T834" id="Seg_9278" s="T833">que.[n:case]=ptcl</ta>
            <ta e="T835" id="Seg_9279" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_9280" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_9281" s="T836">ptcl</ta>
            <ta e="T838" id="Seg_9282" s="T837">v-v:tense-v:pn</ta>
            <ta e="T839" id="Seg_9283" s="T838">pers</ta>
            <ta e="T841" id="Seg_9284" s="T840">v-v:ins-v&gt;v-v:tense-v:pn</ta>
            <ta e="T842" id="Seg_9285" s="T841">adv</ta>
            <ta e="T843" id="Seg_9286" s="T842">n-n:case-n:case.poss</ta>
            <ta e="T844" id="Seg_9287" s="T843">v-v:tense-v:pn</ta>
            <ta e="T846" id="Seg_9288" s="T845">n-n&gt;v-v:n.fin</ta>
            <ta e="T847" id="Seg_9289" s="T846">ptcl</ta>
            <ta e="T852" id="Seg_9290" s="T850">adv</ta>
            <ta e="T853" id="Seg_9291" s="T852">pers</ta>
            <ta e="T854" id="Seg_9292" s="T853">v-v:tense-v:pn</ta>
            <ta e="T855" id="Seg_9293" s="T854">pers</ta>
            <ta e="T856" id="Seg_9294" s="T855">que.[n:case]=ptcl</ta>
            <ta e="T857" id="Seg_9295" s="T856">v.[v:pn]</ta>
            <ta e="T858" id="Seg_9296" s="T857">n.[n:case]</ta>
            <ta e="T859" id="Seg_9297" s="T858">n.[n:case]</ta>
            <ta e="T860" id="Seg_9298" s="T859">v.[v:pn]</ta>
            <ta e="T861" id="Seg_9299" s="T860">n-n:case.poss</ta>
            <ta e="T862" id="Seg_9300" s="T861">v.[v:pn]</ta>
            <ta e="T864" id="Seg_9301" s="T863">adv</ta>
            <ta e="T865" id="Seg_9302" s="T864">n.[n:case]</ta>
            <ta e="T866" id="Seg_9303" s="T865">conj</ta>
            <ta e="T867" id="Seg_9304" s="T866">dempro.[n:case]</ta>
            <ta e="T868" id="Seg_9305" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_9306" s="T868">v-v&gt;v-v:pn</ta>
            <ta e="T871" id="Seg_9307" s="T870">dempro.[n:case]</ta>
            <ta e="T872" id="Seg_9308" s="T871">n-n:case</ta>
            <ta e="T873" id="Seg_9309" s="T872">v-v:tense.[v:pn]</ta>
            <ta e="T874" id="Seg_9310" s="T873">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T876" id="Seg_9311" s="T875">pers</ta>
            <ta e="T877" id="Seg_9312" s="T876">v-v:tense-v:pn</ta>
            <ta e="T880" id="Seg_9313" s="T879">dempro.[n:case]</ta>
            <ta e="T881" id="Seg_9314" s="T880">n-n:case</ta>
            <ta e="T882" id="Seg_9315" s="T881">n-n:case</ta>
            <ta e="T884" id="Seg_9316" s="T883">adv</ta>
            <ta e="T885" id="Seg_9317" s="T884">n.[n:case]</ta>
            <ta e="T886" id="Seg_9318" s="T885">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T888" id="Seg_9319" s="T887">dempro-n:num</ta>
            <ta e="T889" id="Seg_9320" s="T888">num-num&gt;num</ta>
            <ta e="T890" id="Seg_9321" s="T889">v-v:tense-v:pn</ta>
            <ta e="T891" id="Seg_9322" s="T890">conj</ta>
            <ta e="T892" id="Seg_9323" s="T891">pers</ta>
            <ta e="T893" id="Seg_9324" s="T892">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T894" id="Seg_9325" s="T893">adv</ta>
            <ta e="T896" id="Seg_9326" s="T895">adv</ta>
            <ta e="T897" id="Seg_9327" s="T896">v-v:tense-v:pn</ta>
            <ta e="T898" id="Seg_9328" s="T897">que.[n:case]=ptcl</ta>
            <ta e="T899" id="Seg_9329" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_9330" s="T899">n-n:num</ta>
            <ta e="T901" id="Seg_9331" s="T900">n-n:num-n:case.poss</ta>
            <ta e="T902" id="Seg_9332" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_9333" s="T902">v-v&gt;v.[v:pn]</ta>
            <ta e="T906" id="Seg_9334" s="T905">v-v:pn</ta>
            <ta e="T907" id="Seg_9335" s="T906">n.[n:case]</ta>
            <ta e="T909" id="Seg_9336" s="T908">v</ta>
            <ta e="T911" id="Seg_9337" s="T910">ptcl</ta>
            <ta e="T913" id="Seg_9338" s="T912">v</ta>
            <ta e="T914" id="Seg_9339" s="T913">conj</ta>
            <ta e="T915" id="Seg_9340" s="T914">v-v:n.fin</ta>
            <ta e="T916" id="Seg_9341" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_9342" s="T916">v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_9343" s="T917">n-n:case.poss</ta>
            <ta e="T919" id="Seg_9344" s="T918">v-v:tense-v:pn</ta>
            <ta e="T921" id="Seg_9345" s="T920">pers</ta>
            <ta e="T922" id="Seg_9346" s="T921">v-v:tense-v:pn</ta>
            <ta e="T923" id="Seg_9347" s="T922">conj</ta>
            <ta e="T924" id="Seg_9348" s="T923">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T925" id="Seg_9349" s="T924">ptcl</ta>
            <ta e="T927" id="Seg_9350" s="T926">n-n:case.poss-n:case</ta>
            <ta e="T928" id="Seg_9351" s="T927">ptcl</ta>
            <ta e="T929" id="Seg_9352" s="T928">v-v:n.fin</ta>
            <ta e="T930" id="Seg_9353" s="T929">conj</ta>
            <ta e="T931" id="Seg_9354" s="T930">pers</ta>
            <ta e="T932" id="Seg_9355" s="T931">dempro-n:case</ta>
            <ta e="T933" id="Seg_9356" s="T932">v-v:tense-v:pn</ta>
            <ta e="T934" id="Seg_9357" s="T933">dempro</ta>
            <ta e="T935" id="Seg_9358" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_9359" s="T935">n-n:num-n:case</ta>
            <ta e="T937" id="Seg_9360" s="T936">v-v:tense.[v:pn]</ta>
            <ta e="T939" id="Seg_9361" s="T938">conj</ta>
            <ta e="T940" id="Seg_9362" s="T939">n.[n:case]</ta>
            <ta e="T941" id="Seg_9363" s="T940">v-v:tense.[v:pn]</ta>
            <ta e="T942" id="Seg_9364" s="T941">conj</ta>
            <ta e="T943" id="Seg_9365" s="T942">n-n:case</ta>
            <ta e="T944" id="Seg_9366" s="T943">v-v:tense.[v:pn]</ta>
            <ta e="T946" id="Seg_9367" s="T945">que</ta>
            <ta e="T947" id="Seg_9368" s="T946">adv</ta>
            <ta e="T948" id="Seg_9369" s="T947">v-v:n.fin</ta>
            <ta e="T949" id="Seg_9370" s="T948">n-n:num</ta>
            <ta e="T950" id="Seg_9371" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_9372" s="T950">v-v&gt;v.[v:pn]</ta>
            <ta e="T953" id="Seg_9373" s="T952">n-n:num</ta>
            <ta e="T954" id="Seg_9374" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_9375" s="T954">ptcl</ta>
            <ta e="T956" id="Seg_9376" s="T955">v-v&gt;v-v:pn</ta>
            <ta e="T958" id="Seg_9377" s="T957">ptcl</ta>
            <ta e="T959" id="Seg_9378" s="T958">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T961" id="Seg_9379" s="T960">que=ptcl</ta>
            <ta e="T962" id="Seg_9380" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_9381" s="T962">v-v:n.fin</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_9382" s="T1">dempro</ta>
            <ta e="T3" id="Seg_9383" s="T2">pers</ta>
            <ta e="T4" id="Seg_9384" s="T3">n</ta>
            <ta e="T7" id="Seg_9385" s="T6">v</ta>
            <ta e="T8" id="Seg_9386" s="T7">ptcl</ta>
            <ta e="T11" id="Seg_9387" s="T10">v</ta>
            <ta e="T13" id="Seg_9388" s="T12">adv</ta>
            <ta e="T14" id="Seg_9389" s="T13">adj</ta>
            <ta e="T15" id="Seg_9390" s="T14">v</ta>
            <ta e="T17" id="Seg_9391" s="T16">pers</ta>
            <ta e="T18" id="Seg_9392" s="T17">adv</ta>
            <ta e="T20" id="Seg_9393" s="T19">n</ta>
            <ta e="T21" id="Seg_9394" s="T20">pers</ta>
            <ta e="T22" id="Seg_9395" s="T21">v</ta>
            <ta e="T23" id="Seg_9396" s="T22">v</ta>
            <ta e="T24" id="Seg_9397" s="T23">conj</ta>
            <ta e="T25" id="Seg_9398" s="T24">pers</ta>
            <ta e="T26" id="Seg_9399" s="T25">conj</ta>
            <ta e="T27" id="Seg_9400" s="T26">pers</ta>
            <ta e="T28" id="Seg_9401" s="T27">que</ta>
            <ta e="T29" id="Seg_9402" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_9403" s="T29">v</ta>
            <ta e="T32" id="Seg_9404" s="T31">pers</ta>
            <ta e="T33" id="Seg_9405" s="T32">pers</ta>
            <ta e="T34" id="Seg_9406" s="T33">v</ta>
            <ta e="T35" id="Seg_9407" s="T34">adj</ta>
            <ta e="T36" id="Seg_9408" s="T35">v</ta>
            <ta e="T38" id="Seg_9409" s="T37">que</ta>
            <ta e="T39" id="Seg_9410" s="T38">n</ta>
            <ta e="T40" id="Seg_9411" s="T39">v</ta>
            <ta e="T45" id="Seg_9412" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_9413" s="T45">v</ta>
            <ta e="T47" id="Seg_9414" s="T46">adj</ta>
            <ta e="T48" id="Seg_9415" s="T47">v</ta>
            <ta e="T49" id="Seg_9416" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_9417" s="T49">v</ta>
            <ta e="T52" id="Seg_9418" s="T51">pers</ta>
            <ta e="T53" id="Seg_9419" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_9420" s="T53">v</ta>
            <ta e="T56" id="Seg_9421" s="T55">propr</ta>
            <ta e="T61" id="Seg_9422" s="T60">adv</ta>
            <ta e="T62" id="Seg_9423" s="T61">dempro</ta>
            <ta e="T63" id="Seg_9424" s="T62">v</ta>
            <ta e="T65" id="Seg_9425" s="T64">conj</ta>
            <ta e="T66" id="Seg_9426" s="T65">que</ta>
            <ta e="T67" id="Seg_9427" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_9428" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9429" s="T68">v</ta>
            <ta e="T70" id="Seg_9430" s="T69">pers</ta>
            <ta e="T71" id="Seg_9431" s="T70">v</ta>
            <ta e="T72" id="Seg_9432" s="T71">n</ta>
            <ta e="T74" id="Seg_9433" s="T73">v</ta>
            <ta e="T75" id="Seg_9434" s="T74">n</ta>
            <ta e="T77" id="Seg_9435" s="T76">adv</ta>
            <ta e="T78" id="Seg_9436" s="T77">ptcl</ta>
            <ta e="T80" id="Seg_9437" s="T79">n</ta>
            <ta e="T81" id="Seg_9438" s="T80">num</ta>
            <ta e="T82" id="Seg_9439" s="T81">num</ta>
            <ta e="T84" id="Seg_9440" s="T83">adv</ta>
            <ta e="T85" id="Seg_9441" s="T84">pers</ta>
            <ta e="T86" id="Seg_9442" s="T85">v</ta>
            <ta e="T88" id="Seg_9443" s="T87">adv</ta>
            <ta e="T90" id="Seg_9444" s="T89">n</ta>
            <ta e="T91" id="Seg_9445" s="T90">adj</ta>
            <ta e="T93" id="Seg_9446" s="T92">conj</ta>
            <ta e="T94" id="Seg_9447" s="T93">adv</ta>
            <ta e="T95" id="Seg_9448" s="T94">v</ta>
            <ta e="T96" id="Seg_9449" s="T95">adj</ta>
            <ta e="T97" id="Seg_9450" s="T96">n</ta>
            <ta e="T99" id="Seg_9451" s="T98">v</ta>
            <ta e="T100" id="Seg_9452" s="T99">adv</ta>
            <ta e="T101" id="Seg_9453" s="T100">adv</ta>
            <ta e="T102" id="Seg_9454" s="T101">v</ta>
            <ta e="T104" id="Seg_9455" s="T103">adv</ta>
            <ta e="T105" id="Seg_9456" s="T104">n</ta>
            <ta e="T106" id="Seg_9457" s="T105">v</ta>
            <ta e="T108" id="Seg_9458" s="T107">conj</ta>
            <ta e="T109" id="Seg_9459" s="T108">v</ta>
            <ta e="T110" id="Seg_9460" s="T109">dempro</ta>
            <ta e="T111" id="Seg_9461" s="T110">n</ta>
            <ta e="T112" id="Seg_9462" s="T111">n</ta>
            <ta e="T114" id="Seg_9463" s="T113">conj</ta>
            <ta e="T115" id="Seg_9464" s="T114">n</ta>
            <ta e="T116" id="Seg_9465" s="T115">v</ta>
            <ta e="T118" id="Seg_9466" s="T117">pers</ta>
            <ta e="T119" id="Seg_9467" s="T118">conj</ta>
            <ta e="T120" id="Seg_9468" s="T119">v</ta>
            <ta e="T123" id="Seg_9469" s="T122">v</ta>
            <ta e="T125" id="Seg_9470" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_9471" s="T125">v</ta>
            <ta e="T128" id="Seg_9472" s="T127">n</ta>
            <ta e="T129" id="Seg_9473" s="T128">v</ta>
            <ta e="T131" id="Seg_9474" s="T130">propr</ta>
            <ta e="T133" id="Seg_9475" s="T132">conj</ta>
            <ta e="T134" id="Seg_9476" s="T133">propr</ta>
            <ta e="T135" id="Seg_9477" s="T134">adv</ta>
            <ta e="T964" id="Seg_9478" s="T135">v</ta>
            <ta e="T136" id="Seg_9479" s="T964">v</ta>
            <ta e="T137" id="Seg_9480" s="T136">propr</ta>
            <ta e="T139" id="Seg_9481" s="T138">n</ta>
            <ta e="T140" id="Seg_9482" s="T139">v</ta>
            <ta e="T142" id="Seg_9483" s="T141">n</ta>
            <ta e="T143" id="Seg_9484" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_9485" s="T143">n</ta>
            <ta e="T145" id="Seg_9486" s="T144">v</ta>
            <ta e="T147" id="Seg_9487" s="T146">dempro</ta>
            <ta e="T148" id="Seg_9488" s="T147">v</ta>
            <ta e="T149" id="Seg_9489" s="T148">adv</ta>
            <ta e="T150" id="Seg_9490" s="T149">n</ta>
            <ta e="T152" id="Seg_9491" s="T151">conj</ta>
            <ta e="T154" id="Seg_9492" s="T153">adv</ta>
            <ta e="T155" id="Seg_9493" s="T154">v</ta>
            <ta e="T156" id="Seg_9494" s="T155">adv</ta>
            <ta e="T158" id="Seg_9495" s="T157">pers</ta>
            <ta e="T159" id="Seg_9496" s="T158">n</ta>
            <ta e="T160" id="Seg_9497" s="T159">n</ta>
            <ta e="T965" id="Seg_9498" s="T160">v</ta>
            <ta e="T161" id="Seg_9499" s="T965">v</ta>
            <ta e="T162" id="Seg_9500" s="T161">adv</ta>
            <ta e="T163" id="Seg_9501" s="T162">n</ta>
            <ta e="T165" id="Seg_9502" s="T164">v</ta>
            <ta e="T167" id="Seg_9503" s="T166">refl</ta>
            <ta e="T168" id="Seg_9504" s="T167">n</ta>
            <ta e="T169" id="Seg_9505" s="T168">v</ta>
            <ta e="T171" id="Seg_9506" s="T170">adv</ta>
            <ta e="T173" id="Seg_9507" s="T172">v</ta>
            <ta e="T174" id="Seg_9508" s="T173">n</ta>
            <ta e="T176" id="Seg_9509" s="T175">num</ta>
            <ta e="T177" id="Seg_9510" s="T176">n</ta>
            <ta e="T178" id="Seg_9511" s="T177">v</ta>
            <ta e="T179" id="Seg_9512" s="T178">n</ta>
            <ta e="T181" id="Seg_9513" s="T180">v</ta>
            <ta e="T183" id="Seg_9514" s="T182">n</ta>
            <ta e="T185" id="Seg_9515" s="T184">conj</ta>
            <ta e="T186" id="Seg_9516" s="T185">adj</ta>
            <ta e="T187" id="Seg_9517" s="T186">n</ta>
            <ta e="T188" id="Seg_9518" s="T187">n</ta>
            <ta e="T189" id="Seg_9519" s="T188">v</ta>
            <ta e="T190" id="Seg_9520" s="T189">n</ta>
            <ta e="T191" id="Seg_9521" s="T190">conj</ta>
            <ta e="T192" id="Seg_9522" s="T191">num</ta>
            <ta e="T193" id="Seg_9523" s="T192">n</ta>
            <ta e="T194" id="Seg_9524" s="T193">ptcl</ta>
            <ta e="T196" id="Seg_9525" s="T195">v</ta>
            <ta e="T197" id="Seg_9526" s="T196">pers</ta>
            <ta e="T198" id="Seg_9527" s="T197">n</ta>
            <ta e="T199" id="Seg_9528" s="T198">v</ta>
            <ta e="T200" id="Seg_9529" s="T199">n</ta>
            <ta e="T202" id="Seg_9530" s="T201">conj</ta>
            <ta e="T203" id="Seg_9531" s="T202">num</ta>
            <ta e="T204" id="Seg_9532" s="T203">n</ta>
            <ta e="T205" id="Seg_9533" s="T204">pers</ta>
            <ta e="T206" id="Seg_9534" s="T205">v</ta>
            <ta e="T207" id="Seg_9535" s="T206">n</ta>
            <ta e="T208" id="Seg_9536" s="T207">n</ta>
            <ta e="T211" id="Seg_9537" s="T210">v</ta>
            <ta e="T212" id="Seg_9538" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_9539" s="T212">adj</ta>
            <ta e="T215" id="Seg_9540" s="T214">v</ta>
            <ta e="T217" id="Seg_9541" s="T216">que</ta>
            <ta e="T218" id="Seg_9542" s="T217">adj</ta>
            <ta e="T219" id="Seg_9543" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_9544" s="T219">v</ta>
            <ta e="T222" id="Seg_9545" s="T221">dempro</ta>
            <ta e="T223" id="Seg_9546" s="T222">dempro</ta>
            <ta e="T224" id="Seg_9547" s="T223">adj</ta>
            <ta e="T225" id="Seg_9548" s="T224">v</ta>
            <ta e="T227" id="Seg_9549" s="T226">conj</ta>
            <ta e="T228" id="Seg_9550" s="T227">que</ta>
            <ta e="T229" id="Seg_9551" s="T228">adj</ta>
            <ta e="T231" id="Seg_9552" s="T230">v</ta>
            <ta e="T233" id="Seg_9553" s="T232">dempro</ta>
            <ta e="T234" id="Seg_9554" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_9555" s="T234">adj</ta>
            <ta e="T236" id="Seg_9556" s="T235">v</ta>
            <ta e="T238" id="Seg_9557" s="T237">refl</ta>
            <ta e="T239" id="Seg_9558" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_9559" s="T239">v</ta>
            <ta e="T241" id="Seg_9560" s="T240">v</ta>
            <ta e="T243" id="Seg_9561" s="T242">conj</ta>
            <ta e="T244" id="Seg_9562" s="T243">que</ta>
            <ta e="T246" id="Seg_9563" s="T245">conj</ta>
            <ta e="T247" id="Seg_9564" s="T246">que</ta>
            <ta e="T248" id="Seg_9565" s="T247">v</ta>
            <ta e="T249" id="Seg_9566" s="T248">v</ta>
            <ta e="T251" id="Seg_9567" s="T250">v</ta>
            <ta e="T252" id="Seg_9568" s="T251">num</ta>
            <ta e="T253" id="Seg_9569" s="T252">n</ta>
            <ta e="T254" id="Seg_9570" s="T253">v</ta>
            <ta e="T256" id="Seg_9571" s="T255">adv</ta>
            <ta e="T257" id="Seg_9572" s="T256">num</ta>
            <ta e="T258" id="Seg_9573" s="T257">n</ta>
            <ta e="T259" id="Seg_9574" s="T258">adv</ta>
            <ta e="T260" id="Seg_9575" s="T259">adj</ta>
            <ta e="T261" id="Seg_9576" s="T260">v</ta>
            <ta e="T262" id="Seg_9577" s="T261">conj</ta>
            <ta e="T263" id="Seg_9578" s="T262">num</ta>
            <ta e="T264" id="Seg_9579" s="T263">n</ta>
            <ta e="T265" id="Seg_9580" s="T264">adv</ta>
            <ta e="T266" id="Seg_9581" s="T265">v</ta>
            <ta e="T268" id="Seg_9582" s="T267">dempro</ta>
            <ta e="T269" id="Seg_9583" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_9584" s="T269">v</ta>
            <ta e="T271" id="Seg_9585" s="T270">v</ta>
            <ta e="T272" id="Seg_9586" s="T271">n</ta>
            <ta e="T273" id="Seg_9587" s="T272">v</ta>
            <ta e="T275" id="Seg_9588" s="T274">conj</ta>
            <ta e="T276" id="Seg_9589" s="T275">dempro</ta>
            <ta e="T277" id="Seg_9590" s="T276">num</ta>
            <ta e="T278" id="Seg_9591" s="T277">n</ta>
            <ta e="T279" id="Seg_9592" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_9593" s="T279">v</ta>
            <ta e="T281" id="Seg_9594" s="T280">n</ta>
            <ta e="T283" id="Seg_9595" s="T282">n</ta>
            <ta e="T284" id="Seg_9596" s="T283">v</ta>
            <ta e="T286" id="Seg_9597" s="T285">conj</ta>
            <ta e="T287" id="Seg_9598" s="T286">n</ta>
            <ta e="T288" id="Seg_9599" s="T287">refl</ta>
            <ta e="T289" id="Seg_9600" s="T288">v</ta>
            <ta e="T291" id="Seg_9601" s="T290">n</ta>
            <ta e="T294" id="Seg_9602" s="T293">dempro</ta>
            <ta e="T295" id="Seg_9603" s="T294">v</ta>
            <ta e="T296" id="Seg_9604" s="T295">num</ta>
            <ta e="T297" id="Seg_9605" s="T296">v</ta>
            <ta e="T299" id="Seg_9606" s="T298">que</ta>
            <ta e="T300" id="Seg_9607" s="T299">v</ta>
            <ta e="T301" id="Seg_9608" s="T300">conj</ta>
            <ta e="T302" id="Seg_9609" s="T301">v</ta>
            <ta e="T303" id="Seg_9610" s="T302">conj</ta>
            <ta e="T304" id="Seg_9611" s="T303">num</ta>
            <ta e="T305" id="Seg_9612" s="T304">n</ta>
            <ta e="T307" id="Seg_9613" s="T306">v</ta>
            <ta e="T308" id="Seg_9614" s="T307">refl</ta>
            <ta e="T309" id="Seg_9615" s="T308">n</ta>
            <ta e="T311" id="Seg_9616" s="T310">conj</ta>
            <ta e="T312" id="Seg_9617" s="T311">dempro</ta>
            <ta e="T313" id="Seg_9618" s="T312">adv</ta>
            <ta e="T314" id="Seg_9619" s="T313">n</ta>
            <ta e="T315" id="Seg_9620" s="T314">n</ta>
            <ta e="T316" id="Seg_9621" s="T315">v</ta>
            <ta e="T317" id="Seg_9622" s="T316">dempro</ta>
            <ta e="T318" id="Seg_9623" s="T317">dempro</ta>
            <ta e="T319" id="Seg_9624" s="T318">v</ta>
            <ta e="T320" id="Seg_9625" s="T319">que</ta>
            <ta e="T321" id="Seg_9626" s="T320">pers</ta>
            <ta e="T322" id="Seg_9627" s="T321">v</ta>
            <ta e="T324" id="Seg_9628" s="T323">pers</ta>
            <ta e="T325" id="Seg_9629" s="T324">adv</ta>
            <ta e="T327" id="Seg_9630" s="T326">v</ta>
            <ta e="T329" id="Seg_9631" s="T328">n</ta>
            <ta e="T331" id="Seg_9632" s="T330">conj</ta>
            <ta e="T333" id="Seg_9633" s="T332">que</ta>
            <ta e="T334" id="Seg_9634" s="T333">adj</ta>
            <ta e="T335" id="Seg_9635" s="T334">n</ta>
            <ta e="T336" id="Seg_9636" s="T335">v</ta>
            <ta e="T337" id="Seg_9637" s="T336">pers</ta>
            <ta e="T338" id="Seg_9638" s="T337">conj</ta>
            <ta e="T339" id="Seg_9639" s="T338">pers</ta>
            <ta e="T341" id="Seg_9640" s="T340">adv</ta>
            <ta e="T342" id="Seg_9641" s="T341">adj</ta>
            <ta e="T343" id="Seg_9642" s="T342">v</ta>
            <ta e="T344" id="Seg_9643" s="T343">n</ta>
            <ta e="T345" id="Seg_9644" s="T344">n</ta>
            <ta e="T346" id="Seg_9645" s="T345">v</ta>
            <ta e="T348" id="Seg_9646" s="T347">adv</ta>
            <ta e="T349" id="Seg_9647" s="T348">dempro</ta>
            <ta e="T351" id="Seg_9648" s="T350">v</ta>
            <ta e="T352" id="Seg_9649" s="T351">n</ta>
            <ta e="T353" id="Seg_9650" s="T352">que</ta>
            <ta e="T354" id="Seg_9651" s="T353">n</ta>
            <ta e="T356" id="Seg_9652" s="T355">v</ta>
            <ta e="T357" id="Seg_9653" s="T356">n</ta>
            <ta e="T358" id="Seg_9654" s="T357">v</ta>
            <ta e="T359" id="Seg_9655" s="T358">n</ta>
            <ta e="T361" id="Seg_9656" s="T360">v</ta>
            <ta e="T362" id="Seg_9657" s="T361">pers</ta>
            <ta e="T364" id="Seg_9658" s="T363">dempro</ta>
            <ta e="T365" id="Seg_9659" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_9660" s="T365">v</ta>
            <ta e="T367" id="Seg_9661" s="T366">adv</ta>
            <ta e="T368" id="Seg_9662" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_9663" s="T368">v</ta>
            <ta e="T370" id="Seg_9664" s="T369">pers</ta>
            <ta e="T371" id="Seg_9665" s="T370">n</ta>
            <ta e="T372" id="Seg_9666" s="T371">n</ta>
            <ta e="T374" id="Seg_9667" s="T373">adv</ta>
            <ta e="T375" id="Seg_9668" s="T374">v</ta>
            <ta e="T376" id="Seg_9669" s="T375">dempro</ta>
            <ta e="T377" id="Seg_9670" s="T376">v</ta>
            <ta e="T379" id="Seg_9671" s="T378">que</ta>
            <ta e="T380" id="Seg_9672" s="T379">n</ta>
            <ta e="T382" id="Seg_9673" s="T381">n</ta>
            <ta e="T383" id="Seg_9674" s="T382">v</ta>
            <ta e="T385" id="Seg_9675" s="T384">conj</ta>
            <ta e="T386" id="Seg_9676" s="T385">adv</ta>
            <ta e="T387" id="Seg_9677" s="T386">n</ta>
            <ta e="T389" id="Seg_9678" s="T388">v</ta>
            <ta e="T390" id="Seg_9679" s="T389">dempro</ta>
            <ta e="T391" id="Seg_9680" s="T390">adv</ta>
            <ta e="T392" id="Seg_9681" s="T391">v</ta>
            <ta e="T394" id="Seg_9682" s="T393">v</ta>
            <ta e="T396" id="Seg_9683" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_9684" s="T396">adv</ta>
            <ta e="T398" id="Seg_9685" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_9686" s="T398">v</ta>
            <ta e="T400" id="Seg_9687" s="T399">n</ta>
            <ta e="T402" id="Seg_9688" s="T401">adv</ta>
            <ta e="T403" id="Seg_9689" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_9690" s="T403">v</ta>
            <ta e="T405" id="Seg_9691" s="T404">dempro</ta>
            <ta e="T406" id="Seg_9692" s="T405">v</ta>
            <ta e="T408" id="Seg_9693" s="T407">conj</ta>
            <ta e="T409" id="Seg_9694" s="T408">v</ta>
            <ta e="T410" id="Seg_9695" s="T409">adv</ta>
            <ta e="T411" id="Seg_9696" s="T410">num</ta>
            <ta e="T413" id="Seg_9697" s="T412">num</ta>
            <ta e="T414" id="Seg_9698" s="T413">n</ta>
            <ta e="T415" id="Seg_9699" s="T414">v</ta>
            <ta e="T417" id="Seg_9700" s="T416">v</ta>
            <ta e="T418" id="Seg_9701" s="T417">pers</ta>
            <ta e="T419" id="Seg_9702" s="T418">dempro</ta>
            <ta e="T420" id="Seg_9703" s="T419">v</ta>
            <ta e="T421" id="Seg_9704" s="T420">conj</ta>
            <ta e="T422" id="Seg_9705" s="T421">n</ta>
            <ta e="T424" id="Seg_9706" s="T423">v</ta>
            <ta e="T425" id="Seg_9707" s="T424">dempro</ta>
            <ta e="T426" id="Seg_9708" s="T425">v</ta>
            <ta e="T427" id="Seg_9709" s="T426">n</ta>
            <ta e="T428" id="Seg_9710" s="T427">v</ta>
            <ta e="T430" id="Seg_9711" s="T429">ptcl</ta>
            <ta e="T433" id="Seg_9712" s="T432">adv</ta>
            <ta e="T434" id="Seg_9713" s="T433">dempro</ta>
            <ta e="T435" id="Seg_9714" s="T434">v</ta>
            <ta e="T436" id="Seg_9715" s="T435">v</ta>
            <ta e="T437" id="Seg_9716" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_9717" s="T437">v</ta>
            <ta e="T439" id="Seg_9718" s="T438">adv</ta>
            <ta e="T441" id="Seg_9719" s="T440">v</ta>
            <ta e="T442" id="Seg_9720" s="T441">adv</ta>
            <ta e="T444" id="Seg_9721" s="T443">adv</ta>
            <ta e="T445" id="Seg_9722" s="T444">n</ta>
            <ta e="T447" id="Seg_9723" s="T446">v</ta>
            <ta e="T448" id="Seg_9724" s="T447">conj</ta>
            <ta e="T449" id="Seg_9725" s="T448">dempro</ta>
            <ta e="T450" id="Seg_9726" s="T449">n</ta>
            <ta e="T451" id="Seg_9727" s="T450">n</ta>
            <ta e="T452" id="Seg_9728" s="T451">adj</ta>
            <ta e="T453" id="Seg_9729" s="T452">n</ta>
            <ta e="T455" id="Seg_9730" s="T454">dempro</ta>
            <ta e="T456" id="Seg_9731" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_9732" s="T456">v</ta>
            <ta e="T458" id="Seg_9733" s="T457">n</ta>
            <ta e="T460" id="Seg_9734" s="T459">conj</ta>
            <ta e="T462" id="Seg_9735" s="T461">adv</ta>
            <ta e="T463" id="Seg_9736" s="T462">v</ta>
            <ta e="T464" id="Seg_9737" s="T463">conj</ta>
            <ta e="T966" id="Seg_9738" s="T464">v</ta>
            <ta e="T465" id="Seg_9739" s="T966">v</ta>
            <ta e="T466" id="Seg_9740" s="T465">conj</ta>
            <ta e="T467" id="Seg_9741" s="T466">adv</ta>
            <ta e="T468" id="Seg_9742" s="T467">que</ta>
            <ta e="T469" id="Seg_9743" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_9744" s="T469">v</ta>
            <ta e="T473" id="Seg_9745" s="T472">v</ta>
            <ta e="T474" id="Seg_9746" s="T473">dempro</ta>
            <ta e="T476" id="Seg_9747" s="T475">conj</ta>
            <ta e="T967" id="Seg_9748" s="T476">v</ta>
            <ta e="T477" id="Seg_9749" s="T967">v</ta>
            <ta e="T479" id="Seg_9750" s="T478">adv</ta>
            <ta e="T481" id="Seg_9751" s="T480">pers</ta>
            <ta e="T482" id="Seg_9752" s="T481">n</ta>
            <ta e="T483" id="Seg_9753" s="T482">v</ta>
            <ta e="T484" id="Seg_9754" s="T483">v</ta>
            <ta e="T486" id="Seg_9755" s="T485">num</ta>
            <ta e="T487" id="Seg_9756" s="T486">num</ta>
            <ta e="T489" id="Seg_9757" s="T488">pers</ta>
            <ta e="T490" id="Seg_9758" s="T489">n</ta>
            <ta e="T491" id="Seg_9759" s="T490">v</ta>
            <ta e="T493" id="Seg_9760" s="T492">v</ta>
            <ta e="T494" id="Seg_9761" s="T493">conj</ta>
            <ta e="T495" id="Seg_9762" s="T494">que</ta>
            <ta e="T496" id="Seg_9763" s="T495">v</ta>
            <ta e="T498" id="Seg_9764" s="T497">ptcl</ta>
            <ta e="T499" id="Seg_9765" s="T498">num</ta>
            <ta e="T500" id="Seg_9766" s="T499">n</ta>
            <ta e="T501" id="Seg_9767" s="T500">v</ta>
            <ta e="T504" id="Seg_9768" s="T503">propr</ta>
            <ta e="T506" id="Seg_9769" s="T505">num</ta>
            <ta e="T507" id="Seg_9770" s="T506">n</ta>
            <ta e="T508" id="Seg_9771" s="T507">num</ta>
            <ta e="T509" id="Seg_9772" s="T508">n</ta>
            <ta e="T510" id="Seg_9773" s="T509">v</ta>
            <ta e="T512" id="Seg_9774" s="T511">dempro</ta>
            <ta e="T513" id="Seg_9775" s="T512">dempro</ta>
            <ta e="T514" id="Seg_9776" s="T513">n</ta>
            <ta e="T515" id="Seg_9777" s="T514">v</ta>
            <ta e="T517" id="Seg_9778" s="T516">n</ta>
            <ta e="T518" id="Seg_9779" s="T517">v</ta>
            <ta e="T520" id="Seg_9780" s="T519">que</ta>
            <ta e="T521" id="Seg_9781" s="T520">aux</ta>
            <ta e="T523" id="Seg_9782" s="T522">aux</ta>
            <ta e="T524" id="Seg_9783" s="T523">v</ta>
            <ta e="T526" id="Seg_9784" s="T525">adv</ta>
            <ta e="T968" id="Seg_9785" s="T526">v</ta>
            <ta e="T527" id="Seg_9786" s="T968">v</ta>
            <ta e="T529" id="Seg_9787" s="T528">n</ta>
            <ta e="T530" id="Seg_9788" s="T529">v</ta>
            <ta e="T531" id="Seg_9789" s="T530">v</ta>
            <ta e="T532" id="Seg_9790" s="T531">n</ta>
            <ta e="T534" id="Seg_9791" s="T533">dempro</ta>
            <ta e="T535" id="Seg_9792" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_9793" s="T535">v</ta>
            <ta e="T538" id="Seg_9794" s="T537">pers</ta>
            <ta e="T539" id="Seg_9795" s="T538">n</ta>
            <ta e="T540" id="Seg_9796" s="T539">v</ta>
            <ta e="T541" id="Seg_9797" s="T540">que</ta>
            <ta e="T542" id="Seg_9798" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_9799" s="T542">adv</ta>
            <ta e="T545" id="Seg_9800" s="T544">adv</ta>
            <ta e="T546" id="Seg_9801" s="T545">n</ta>
            <ta e="T969" id="Seg_9802" s="T546">v</ta>
            <ta e="T547" id="Seg_9803" s="T969">v</ta>
            <ta e="T548" id="Seg_9804" s="T547">n</ta>
            <ta e="T549" id="Seg_9805" s="T548">v</ta>
            <ta e="T551" id="Seg_9806" s="T550">v</ta>
            <ta e="T552" id="Seg_9807" s="T551">n</ta>
            <ta e="T553" id="Seg_9808" s="T552">pers</ta>
            <ta e="T554" id="Seg_9809" s="T553">v</ta>
            <ta e="T555" id="Seg_9810" s="T554">n</ta>
            <ta e="T557" id="Seg_9811" s="T556">v</ta>
            <ta e="T558" id="Seg_9812" s="T557">n</ta>
            <ta e="T559" id="Seg_9813" s="T558">pers</ta>
            <ta e="T560" id="Seg_9814" s="T559">n</ta>
            <ta e="T561" id="Seg_9815" s="T560">v</ta>
            <ta e="T563" id="Seg_9816" s="T562">adv</ta>
            <ta e="T564" id="Seg_9817" s="T563">dempro</ta>
            <ta e="T565" id="Seg_9818" s="T564">v</ta>
            <ta e="T566" id="Seg_9819" s="T565">v</ta>
            <ta e="T567" id="Seg_9820" s="T566">adv</ta>
            <ta e="T568" id="Seg_9821" s="T567">n</ta>
            <ta e="T569" id="Seg_9822" s="T568">adv</ta>
            <ta e="T570" id="Seg_9823" s="T569">v</ta>
            <ta e="T572" id="Seg_9824" s="T571">aux</ta>
            <ta e="T573" id="Seg_9825" s="T572">v</ta>
            <ta e="T574" id="Seg_9826" s="T573">que</ta>
            <ta e="T576" id="Seg_9827" s="T575">adv</ta>
            <ta e="T970" id="Seg_9828" s="T576">v</ta>
            <ta e="T577" id="Seg_9829" s="T970">v</ta>
            <ta e="T579" id="Seg_9830" s="T578">que</ta>
            <ta e="T580" id="Seg_9831" s="T579">aux</ta>
            <ta e="T581" id="Seg_9832" s="T580">v</ta>
            <ta e="T583" id="Seg_9833" s="T582">adv</ta>
            <ta e="T584" id="Seg_9834" s="T583">adv</ta>
            <ta e="T585" id="Seg_9835" s="T584">v</ta>
            <ta e="T586" id="Seg_9836" s="T585">n</ta>
            <ta e="T588" id="Seg_9837" s="T587">n</ta>
            <ta e="T589" id="Seg_9838" s="T588">n</ta>
            <ta e="T590" id="Seg_9839" s="T589">v</ta>
            <ta e="T591" id="Seg_9840" s="T590">pers</ta>
            <ta e="T593" id="Seg_9841" s="T592">ptcl</ta>
            <ta e="T595" id="Seg_9842" s="T594">pers</ta>
            <ta e="T596" id="Seg_9843" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_9844" s="T596">v</ta>
            <ta e="T599" id="Seg_9845" s="T598">conj</ta>
            <ta e="T600" id="Seg_9846" s="T599">pers</ta>
            <ta e="T601" id="Seg_9847" s="T600">pers</ta>
            <ta e="T602" id="Seg_9848" s="T601">n</ta>
            <ta e="T603" id="Seg_9849" s="T602">v</ta>
            <ta e="T604" id="Seg_9850" s="T603">ptcl</ta>
            <ta e="T605" id="Seg_9851" s="T604">pers</ta>
            <ta e="T607" id="Seg_9852" s="T606">n</ta>
            <ta e="T608" id="Seg_9853" s="T607">n</ta>
            <ta e="T609" id="Seg_9854" s="T608">adj</ta>
            <ta e="T611" id="Seg_9855" s="T610">conj</ta>
            <ta e="T612" id="Seg_9856" s="T611">pers</ta>
            <ta e="T613" id="Seg_9857" s="T612">n</ta>
            <ta e="T614" id="Seg_9858" s="T613">adv</ta>
            <ta e="T615" id="Seg_9859" s="T614">adj</ta>
            <ta e="T617" id="Seg_9860" s="T616">adv</ta>
            <ta e="T618" id="Seg_9861" s="T617">n</ta>
            <ta e="T971" id="Seg_9862" s="T618">v</ta>
            <ta e="T619" id="Seg_9863" s="T971">v</ta>
            <ta e="T620" id="Seg_9864" s="T619">adv</ta>
            <ta e="T621" id="Seg_9865" s="T620">n</ta>
            <ta e="T622" id="Seg_9866" s="T621">v</ta>
            <ta e="T624" id="Seg_9867" s="T623">dempro</ta>
            <ta e="T625" id="Seg_9868" s="T624">v</ta>
            <ta e="T626" id="Seg_9869" s="T625">v</ta>
            <ta e="T628" id="Seg_9870" s="T627">adv</ta>
            <ta e="T629" id="Seg_9871" s="T628">que</ta>
            <ta e="T630" id="Seg_9872" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_9873" s="T630">v</ta>
            <ta e="T633" id="Seg_9874" s="T632">conj</ta>
            <ta e="T634" id="Seg_9875" s="T633">n</ta>
            <ta e="T635" id="Seg_9876" s="T634">v</ta>
            <ta e="T636" id="Seg_9877" s="T635">n</ta>
            <ta e="T638" id="Seg_9878" s="T637">n</ta>
            <ta e="T640" id="Seg_9879" s="T639">v</ta>
            <ta e="T641" id="Seg_9880" s="T640">adj</ta>
            <ta e="T643" id="Seg_9881" s="T642">adv</ta>
            <ta e="T644" id="Seg_9882" s="T643">v</ta>
            <ta e="T645" id="Seg_9883" s="T644">adv</ta>
            <ta e="T647" id="Seg_9884" s="T646">dempro</ta>
            <ta e="T648" id="Seg_9885" s="T647">v</ta>
            <ta e="T649" id="Seg_9886" s="T648">pers</ta>
            <ta e="T650" id="Seg_9887" s="T649">pers</ta>
            <ta e="T651" id="Seg_9888" s="T650">n</ta>
            <ta e="T652" id="Seg_9889" s="T651">v</ta>
            <ta e="T653" id="Seg_9890" s="T652">n</ta>
            <ta e="T655" id="Seg_9891" s="T654">n</ta>
            <ta e="T656" id="Seg_9892" s="T655">v</ta>
            <ta e="T658" id="Seg_9893" s="T657">v</ta>
            <ta e="T659" id="Seg_9894" s="T658">pers</ta>
            <ta e="T661" id="Seg_9895" s="T660">n</ta>
            <ta e="T663" id="Seg_9896" s="T662">dempro</ta>
            <ta e="T664" id="Seg_9897" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_9898" s="T664">v</ta>
            <ta e="T667" id="Seg_9899" s="T666">dempro</ta>
            <ta e="T668" id="Seg_9900" s="T667">dempro</ta>
            <ta e="T669" id="Seg_9901" s="T668">ptcl</ta>
            <ta e="T670" id="Seg_9902" s="T669">v</ta>
            <ta e="T672" id="Seg_9903" s="T671">n</ta>
            <ta e="T673" id="Seg_9904" s="T672">v</ta>
            <ta e="T675" id="Seg_9905" s="T674">n</ta>
            <ta e="T676" id="Seg_9906" s="T675">v</ta>
            <ta e="T677" id="Seg_9907" s="T676">num</ta>
            <ta e="T680" id="Seg_9908" s="T679">adj</ta>
            <ta e="T681" id="Seg_9909" s="T680">v</ta>
            <ta e="T683" id="Seg_9910" s="T682">adv</ta>
            <ta e="T684" id="Seg_9911" s="T683">v</ta>
            <ta e="T685" id="Seg_9912" s="T684">n</ta>
            <ta e="T686" id="Seg_9913" s="T685">quant</ta>
            <ta e="T687" id="Seg_9914" s="T686">v</ta>
            <ta e="T689" id="Seg_9915" s="T688">pers</ta>
            <ta e="T690" id="Seg_9916" s="T689">adv</ta>
            <ta e="T691" id="Seg_9917" s="T690">v</ta>
            <ta e="T692" id="Seg_9918" s="T691">n</ta>
            <ta e="T698" id="Seg_9919" s="T697">n</ta>
            <ta e="T699" id="Seg_9920" s="T698">v</ta>
            <ta e="T700" id="Seg_9921" s="T699">dempro</ta>
            <ta e="T701" id="Seg_9922" s="T700">pers</ta>
            <ta e="T702" id="Seg_9923" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9924" s="T702">v</ta>
            <ta e="T972" id="Seg_9925" s="T703">v</ta>
            <ta e="T704" id="Seg_9926" s="T972">v</ta>
            <ta e="T706" id="Seg_9927" s="T705">adv</ta>
            <ta e="T707" id="Seg_9928" s="T706">n</ta>
            <ta e="T708" id="Seg_9929" s="T707">v</ta>
            <ta e="T709" id="Seg_9930" s="T708">n</ta>
            <ta e="T711" id="Seg_9931" s="T710">n</ta>
            <ta e="T712" id="Seg_9932" s="T711">v</ta>
            <ta e="T715" id="Seg_9933" s="T714">v</ta>
            <ta e="T717" id="Seg_9934" s="T716">v</ta>
            <ta e="T718" id="Seg_9935" s="T717">dempro</ta>
            <ta e="T719" id="Seg_9936" s="T718">n</ta>
            <ta e="T722" id="Seg_9937" s="T721">n</ta>
            <ta e="T723" id="Seg_9938" s="T722">v</ta>
            <ta e="T726" id="Seg_9939" s="T725">conj</ta>
            <ta e="T727" id="Seg_9940" s="T726">dempro</ta>
            <ta e="T728" id="Seg_9941" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9942" s="T728">v</ta>
            <ta e="T730" id="Seg_9943" s="T729">v</ta>
            <ta e="T731" id="Seg_9944" s="T730">adv</ta>
            <ta e="T733" id="Seg_9945" s="T732">v</ta>
            <ta e="T734" id="Seg_9946" s="T733">que</ta>
            <ta e="T735" id="Seg_9947" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_9948" s="T735">n</ta>
            <ta e="T738" id="Seg_9949" s="T737">adv</ta>
            <ta e="T739" id="Seg_9950" s="T738">adv</ta>
            <ta e="T741" id="Seg_9951" s="T740">n</ta>
            <ta e="T743" id="Seg_9952" s="T742">v</ta>
            <ta e="T744" id="Seg_9953" s="T743">conj</ta>
            <ta e="T745" id="Seg_9954" s="T744">adv</ta>
            <ta e="T746" id="Seg_9955" s="T745">n</ta>
            <ta e="T973" id="Seg_9956" s="T746">v</ta>
            <ta e="T747" id="Seg_9957" s="T973">v</ta>
            <ta e="T749" id="Seg_9958" s="T748">v</ta>
            <ta e="T750" id="Seg_9959" s="T749">que</ta>
            <ta e="T751" id="Seg_9960" s="T750">pers</ta>
            <ta e="T753" id="Seg_9961" s="T752">v</ta>
            <ta e="T755" id="Seg_9962" s="T754">conj</ta>
            <ta e="T756" id="Seg_9963" s="T755">que</ta>
            <ta e="T757" id="Seg_9964" s="T756">adv</ta>
            <ta e="T758" id="Seg_9965" s="T757">v</ta>
            <ta e="T760" id="Seg_9966" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_9967" s="T760">que</ta>
            <ta e="T762" id="Seg_9968" s="T761">v</ta>
            <ta e="T763" id="Seg_9969" s="T762">n</ta>
            <ta e="T764" id="Seg_9970" s="T763">v</ta>
            <ta e="T765" id="Seg_9971" s="T764">n</ta>
            <ta e="T766" id="Seg_9972" s="T765">v</ta>
            <ta e="T768" id="Seg_9973" s="T767">v</ta>
            <ta e="T769" id="Seg_9974" s="T768">n</ta>
            <ta e="T770" id="Seg_9975" s="T769">v</ta>
            <ta e="T772" id="Seg_9976" s="T771">adv</ta>
            <ta e="T773" id="Seg_9977" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_9978" s="T773">que</ta>
            <ta e="T775" id="Seg_9979" s="T774">quant</ta>
            <ta e="T776" id="Seg_9980" s="T775">n</ta>
            <ta e="T777" id="Seg_9981" s="T776">quant</ta>
            <ta e="T778" id="Seg_9982" s="T777">n</ta>
            <ta e="T779" id="Seg_9983" s="T778">n</ta>
            <ta e="T781" id="Seg_9984" s="T780">n</ta>
            <ta e="T782" id="Seg_9985" s="T781">adj</ta>
            <ta e="T784" id="Seg_9986" s="T783">n</ta>
            <ta e="T786" id="Seg_9987" s="T785">n</ta>
            <ta e="T787" id="Seg_9988" s="T786">v</ta>
            <ta e="T789" id="Seg_9989" s="T788">n</ta>
            <ta e="T790" id="Seg_9990" s="T789">n</ta>
            <ta e="T791" id="Seg_9991" s="T790">v</ta>
            <ta e="T792" id="Seg_9992" s="T791">adj</ta>
            <ta e="T793" id="Seg_9993" s="T792">n</ta>
            <ta e="T794" id="Seg_9994" s="T793">adj</ta>
            <ta e="T795" id="Seg_9995" s="T794">n</ta>
            <ta e="T796" id="Seg_9996" s="T795">v</ta>
            <ta e="T798" id="Seg_9997" s="T797">pers</ta>
            <ta e="T799" id="Seg_9998" s="T798">n</ta>
            <ta e="T800" id="Seg_9999" s="T799">v</ta>
            <ta e="T802" id="Seg_10000" s="T801">v</ta>
            <ta e="T804" id="Seg_10001" s="T803">adv</ta>
            <ta e="T805" id="Seg_10002" s="T804">n</ta>
            <ta e="T806" id="Seg_10003" s="T805">v</ta>
            <ta e="T808" id="Seg_10004" s="T807">num</ta>
            <ta e="T810" id="Seg_10005" s="T809">pers</ta>
            <ta e="T811" id="Seg_10006" s="T810">n</ta>
            <ta e="T812" id="Seg_10007" s="T811">v</ta>
            <ta e="T814" id="Seg_10008" s="T813">pers</ta>
            <ta e="T816" id="Seg_10009" s="T815">n</ta>
            <ta e="T817" id="Seg_10010" s="T816">ptcl</ta>
            <ta e="T818" id="Seg_10011" s="T817">v</ta>
            <ta e="T819" id="Seg_10012" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_10013" s="T819">v</ta>
            <ta e="T821" id="Seg_10014" s="T820">pers</ta>
            <ta e="T822" id="Seg_10015" s="T821">v</ta>
            <ta e="T823" id="Seg_10016" s="T822">adv</ta>
            <ta e="T824" id="Seg_10017" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_10018" s="T824">n</ta>
            <ta e="T826" id="Seg_10019" s="T825">v</ta>
            <ta e="T829" id="Seg_10020" s="T828">que</ta>
            <ta e="T830" id="Seg_10021" s="T829">v</ta>
            <ta e="T831" id="Seg_10022" s="T830">v</ta>
            <ta e="T834" id="Seg_10023" s="T833">que</ta>
            <ta e="T835" id="Seg_10024" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_10025" s="T835">v</ta>
            <ta e="T837" id="Seg_10026" s="T836">ptcl</ta>
            <ta e="T838" id="Seg_10027" s="T837">v</ta>
            <ta e="T839" id="Seg_10028" s="T838">pers</ta>
            <ta e="T841" id="Seg_10029" s="T840">v</ta>
            <ta e="T842" id="Seg_10030" s="T841">adv</ta>
            <ta e="T843" id="Seg_10031" s="T842">n</ta>
            <ta e="T844" id="Seg_10032" s="T843">v</ta>
            <ta e="T846" id="Seg_10033" s="T845">v</ta>
            <ta e="T847" id="Seg_10034" s="T846">ptcl</ta>
            <ta e="T852" id="Seg_10035" s="T850">adv</ta>
            <ta e="T853" id="Seg_10036" s="T852">pers</ta>
            <ta e="T854" id="Seg_10037" s="T853">v</ta>
            <ta e="T855" id="Seg_10038" s="T854">pers</ta>
            <ta e="T856" id="Seg_10039" s="T855">que</ta>
            <ta e="T857" id="Seg_10040" s="T856">v</ta>
            <ta e="T858" id="Seg_10041" s="T857">n</ta>
            <ta e="T859" id="Seg_10042" s="T858">n</ta>
            <ta e="T860" id="Seg_10043" s="T859">v</ta>
            <ta e="T861" id="Seg_10044" s="T860">n</ta>
            <ta e="T862" id="Seg_10045" s="T861">v</ta>
            <ta e="T864" id="Seg_10046" s="T863">adv</ta>
            <ta e="T865" id="Seg_10047" s="T864">n</ta>
            <ta e="T866" id="Seg_10048" s="T865">conj</ta>
            <ta e="T867" id="Seg_10049" s="T866">dempro</ta>
            <ta e="T868" id="Seg_10050" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_10051" s="T868">v</ta>
            <ta e="T871" id="Seg_10052" s="T870">dempro</ta>
            <ta e="T872" id="Seg_10053" s="T871">n</ta>
            <ta e="T873" id="Seg_10054" s="T872">v</ta>
            <ta e="T874" id="Seg_10055" s="T873">v</ta>
            <ta e="T876" id="Seg_10056" s="T875">pers</ta>
            <ta e="T877" id="Seg_10057" s="T876">v</ta>
            <ta e="T880" id="Seg_10058" s="T879">dempro</ta>
            <ta e="T881" id="Seg_10059" s="T880">n</ta>
            <ta e="T882" id="Seg_10060" s="T881">n</ta>
            <ta e="T884" id="Seg_10061" s="T883">adv</ta>
            <ta e="T885" id="Seg_10062" s="T884">n</ta>
            <ta e="T886" id="Seg_10063" s="T885">v</ta>
            <ta e="T888" id="Seg_10064" s="T887">dempro</ta>
            <ta e="T889" id="Seg_10065" s="T888">num</ta>
            <ta e="T890" id="Seg_10066" s="T889">v</ta>
            <ta e="T891" id="Seg_10067" s="T890">conj</ta>
            <ta e="T892" id="Seg_10068" s="T891">pers</ta>
            <ta e="T893" id="Seg_10069" s="T892">v</ta>
            <ta e="T894" id="Seg_10070" s="T893">adv</ta>
            <ta e="T896" id="Seg_10071" s="T895">adv</ta>
            <ta e="T897" id="Seg_10072" s="T896">v</ta>
            <ta e="T898" id="Seg_10073" s="T897">que</ta>
            <ta e="T899" id="Seg_10074" s="T898">ptcl</ta>
            <ta e="T900" id="Seg_10075" s="T899">n</ta>
            <ta e="T901" id="Seg_10076" s="T900">n</ta>
            <ta e="T902" id="Seg_10077" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_10078" s="T902">v</ta>
            <ta e="T906" id="Seg_10079" s="T905">v</ta>
            <ta e="T907" id="Seg_10080" s="T906">n</ta>
            <ta e="T909" id="Seg_10081" s="T908">v</ta>
            <ta e="T911" id="Seg_10082" s="T910">ptcl</ta>
            <ta e="T913" id="Seg_10083" s="T912">v</ta>
            <ta e="T914" id="Seg_10084" s="T913">conj</ta>
            <ta e="T915" id="Seg_10085" s="T914">v</ta>
            <ta e="T916" id="Seg_10086" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_10087" s="T916">v</ta>
            <ta e="T918" id="Seg_10088" s="T917">n</ta>
            <ta e="T919" id="Seg_10089" s="T918">v</ta>
            <ta e="T921" id="Seg_10090" s="T920">pers</ta>
            <ta e="T922" id="Seg_10091" s="T921">v</ta>
            <ta e="T923" id="Seg_10092" s="T922">conj</ta>
            <ta e="T924" id="Seg_10093" s="T923">v</ta>
            <ta e="T925" id="Seg_10094" s="T924">ptcl</ta>
            <ta e="T927" id="Seg_10095" s="T926">n</ta>
            <ta e="T928" id="Seg_10096" s="T927">ptcl</ta>
            <ta e="T929" id="Seg_10097" s="T928">v</ta>
            <ta e="T930" id="Seg_10098" s="T929">conj</ta>
            <ta e="T931" id="Seg_10099" s="T930">pers</ta>
            <ta e="T932" id="Seg_10100" s="T931">dempro</ta>
            <ta e="T933" id="Seg_10101" s="T932">v</ta>
            <ta e="T934" id="Seg_10102" s="T933">dempro</ta>
            <ta e="T935" id="Seg_10103" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_10104" s="T935">n</ta>
            <ta e="T937" id="Seg_10105" s="T936">v</ta>
            <ta e="T939" id="Seg_10106" s="T938">conj</ta>
            <ta e="T940" id="Seg_10107" s="T939">n</ta>
            <ta e="T941" id="Seg_10108" s="T940">v</ta>
            <ta e="T942" id="Seg_10109" s="T941">conj</ta>
            <ta e="T943" id="Seg_10110" s="T942">n</ta>
            <ta e="T944" id="Seg_10111" s="T943">v</ta>
            <ta e="T946" id="Seg_10112" s="T945">que</ta>
            <ta e="T947" id="Seg_10113" s="T946">adv</ta>
            <ta e="T948" id="Seg_10114" s="T947">v</ta>
            <ta e="T949" id="Seg_10115" s="T948">n</ta>
            <ta e="T950" id="Seg_10116" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_10117" s="T950">v</ta>
            <ta e="T953" id="Seg_10118" s="T952">n</ta>
            <ta e="T954" id="Seg_10119" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_10120" s="T954">ptcl</ta>
            <ta e="T956" id="Seg_10121" s="T955">v</ta>
            <ta e="T958" id="Seg_10122" s="T957">ptcl</ta>
            <ta e="T959" id="Seg_10123" s="T958">v</ta>
            <ta e="T961" id="Seg_10124" s="T960">que</ta>
            <ta e="T962" id="Seg_10125" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_10126" s="T962">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_10127" s="T1">pro:Th</ta>
            <ta e="T3" id="Seg_10128" s="T2">pro.h:Poss</ta>
            <ta e="T4" id="Seg_10129" s="T3">np:Th</ta>
            <ta e="T7" id="Seg_10130" s="T6">0.3.h:E</ta>
            <ta e="T15" id="Seg_10131" s="T14">0.3.h:P</ta>
            <ta e="T17" id="Seg_10132" s="T16">pro.h:Th</ta>
            <ta e="T20" id="Seg_10133" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_10134" s="T20">pro.h:R</ta>
            <ta e="T22" id="Seg_10135" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_10136" s="T22">0.1.h:A</ta>
            <ta e="T27" id="Seg_10137" s="T26">pro.h:E</ta>
            <ta e="T28" id="Seg_10138" s="T27">pro:Th</ta>
            <ta e="T32" id="Seg_10139" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_10140" s="T32">pro.h:R</ta>
            <ta e="T36" id="Seg_10141" s="T35">0.1.h:Th</ta>
            <ta e="T39" id="Seg_10142" s="T38">np.h:A</ta>
            <ta e="T46" id="Seg_10143" s="T45">0.1.h:E</ta>
            <ta e="T48" id="Seg_10144" s="T47">0.1.h:Th</ta>
            <ta e="T50" id="Seg_10145" s="T49">0.1.h:A</ta>
            <ta e="T52" id="Seg_10146" s="T51">pro.h:R</ta>
            <ta e="T54" id="Seg_10147" s="T53">0.3.h:A</ta>
            <ta e="T56" id="Seg_10148" s="T55">np:L</ta>
            <ta e="T61" id="Seg_10149" s="T60">adv:Time</ta>
            <ta e="T62" id="Seg_10150" s="T61">pro.h:Poss</ta>
            <ta e="T66" id="Seg_10151" s="T65">pro:Th</ta>
            <ta e="T69" id="Seg_10152" s="T68">0.1.h:E</ta>
            <ta e="T70" id="Seg_10153" s="T69">pro.h:R</ta>
            <ta e="T71" id="Seg_10154" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_10155" s="T71">np:Th</ta>
            <ta e="T74" id="Seg_10156" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_10157" s="T74">np:G</ta>
            <ta e="T84" id="Seg_10158" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_10159" s="T84">pro.h:A</ta>
            <ta e="T90" id="Seg_10160" s="T89">np:Th</ta>
            <ta e="T94" id="Seg_10161" s="T93">adv:Time</ta>
            <ta e="T95" id="Seg_10162" s="T94">0.3.h:A</ta>
            <ta e="T97" id="Seg_10163" s="T96">np:Th</ta>
            <ta e="T99" id="Seg_10164" s="T98">0.1.h:A</ta>
            <ta e="T102" id="Seg_10165" s="T101">0.3:Th</ta>
            <ta e="T104" id="Seg_10166" s="T103">adv:Time</ta>
            <ta e="T105" id="Seg_10167" s="T104">np:Th</ta>
            <ta e="T106" id="Seg_10168" s="T105">0.3.h:A</ta>
            <ta e="T109" id="Seg_10169" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_10170" s="T110">np:Th</ta>
            <ta e="T112" id="Seg_10171" s="T111">np:G</ta>
            <ta e="T115" id="Seg_10172" s="T114">np:P</ta>
            <ta e="T118" id="Seg_10173" s="T117">pro.h:A</ta>
            <ta e="T123" id="Seg_10174" s="T122">0.1.h:A</ta>
            <ta e="T126" id="Seg_10175" s="T125">0.2.h:A</ta>
            <ta e="T128" id="Seg_10176" s="T127">np:Th</ta>
            <ta e="T129" id="Seg_10177" s="T128">0.2.h:A</ta>
            <ta e="T131" id="Seg_10178" s="T130">np.h:R</ta>
            <ta e="T134" id="Seg_10179" s="T133">np.h:A</ta>
            <ta e="T135" id="Seg_10180" s="T134">adv:Time</ta>
            <ta e="T137" id="Seg_10181" s="T136">np:G</ta>
            <ta e="T139" id="Seg_10182" s="T138">np:Th</ta>
            <ta e="T140" id="Seg_10183" s="T139">0.3.h:A</ta>
            <ta e="T142" id="Seg_10184" s="T141">np.h:A</ta>
            <ta e="T144" id="Seg_10185" s="T143">np:Th</ta>
            <ta e="T147" id="Seg_10186" s="T146">pro.h:A</ta>
            <ta e="T149" id="Seg_10187" s="T148">adv:L</ta>
            <ta e="T150" id="Seg_10188" s="T149">np:Ins</ta>
            <ta e="T155" id="Seg_10189" s="T154">0.3.h:A</ta>
            <ta e="T158" id="Seg_10190" s="T157">pro.h:Poss</ta>
            <ta e="T159" id="Seg_10191" s="T158">np.h:Poss</ta>
            <ta e="T160" id="Seg_10192" s="T159">np.h:A</ta>
            <ta e="T163" id="Seg_10193" s="T162">np:R</ta>
            <ta e="T167" id="Seg_10194" s="T166">pro.h:Poss</ta>
            <ta e="T168" id="Seg_10195" s="T167">np.h:Th</ta>
            <ta e="T169" id="Seg_10196" s="T168">0.3.h:A</ta>
            <ta e="T173" id="Seg_10197" s="T172">0.3.h:A</ta>
            <ta e="T174" id="Seg_10198" s="T173">np:G</ta>
            <ta e="T177" id="Seg_10199" s="T176">np.h:E</ta>
            <ta e="T179" id="Seg_10200" s="T178">np:P</ta>
            <ta e="T183" id="Seg_10201" s="T182">np:Ins</ta>
            <ta e="T187" id="Seg_10202" s="T186">np.h:A</ta>
            <ta e="T188" id="Seg_10203" s="T187">np:P</ta>
            <ta e="T190" id="Seg_10204" s="T189">np:Ins</ta>
            <ta e="T193" id="Seg_10205" s="T192">np.h:A</ta>
            <ta e="T197" id="Seg_10206" s="T196">pro.h:A</ta>
            <ta e="T198" id="Seg_10207" s="T197">np:Ins</ta>
            <ta e="T200" id="Seg_10208" s="T199">np:P</ta>
            <ta e="T204" id="Seg_10209" s="T203">np.h:A</ta>
            <ta e="T205" id="Seg_10210" s="T204">pro.h:A</ta>
            <ta e="T207" id="Seg_10211" s="T206">np:Ins</ta>
            <ta e="T208" id="Seg_10212" s="T207">np:Ins</ta>
            <ta e="T211" id="Seg_10213" s="T210">0.3.h:A</ta>
            <ta e="T215" id="Seg_10214" s="T214">0.3:P</ta>
            <ta e="T222" id="Seg_10215" s="T221">pro:P</ta>
            <ta e="T223" id="Seg_10216" s="T222">pro.h:B</ta>
            <ta e="T231" id="Seg_10217" s="T230">0.3:P</ta>
            <ta e="T233" id="Seg_10218" s="T232">pro.h:B</ta>
            <ta e="T236" id="Seg_10219" s="T235">0.3:P</ta>
            <ta e="T238" id="Seg_10220" s="T237">pro.h:P</ta>
            <ta e="T241" id="Seg_10221" s="T240">0.3.h:A</ta>
            <ta e="T247" id="Seg_10222" s="T246">pro:P</ta>
            <ta e="T248" id="Seg_10223" s="T247">0.3.h:A</ta>
            <ta e="T249" id="Seg_10224" s="T248">0.3:P</ta>
            <ta e="T253" id="Seg_10225" s="T252">np.h:E</ta>
            <ta e="T256" id="Seg_10226" s="T255">adv:Time</ta>
            <ta e="T258" id="Seg_10227" s="T257">np.h:Th</ta>
            <ta e="T264" id="Seg_10228" s="T263">np.h:A</ta>
            <ta e="T268" id="Seg_10229" s="T267">pro.h:A</ta>
            <ta e="T272" id="Seg_10230" s="T271">np:P</ta>
            <ta e="T273" id="Seg_10231" s="T272">0.3.h:A</ta>
            <ta e="T278" id="Seg_10232" s="T277">np.h:A</ta>
            <ta e="T281" id="Seg_10233" s="T280">np:P</ta>
            <ta e="T283" id="Seg_10234" s="T282">np:Th</ta>
            <ta e="T284" id="Seg_10235" s="T283">0.3.h:A</ta>
            <ta e="T287" id="Seg_10236" s="T286">np:P</ta>
            <ta e="T288" id="Seg_10237" s="T287">pro.h:B</ta>
            <ta e="T289" id="Seg_10238" s="T288">0.3.h:A</ta>
            <ta e="T291" id="Seg_10239" s="T290">np:Ins</ta>
            <ta e="T294" id="Seg_10240" s="T293">pro.h:A</ta>
            <ta e="T296" id="Seg_10241" s="T295">np.h:A</ta>
            <ta e="T299" id="Seg_10242" s="T298">np:Th</ta>
            <ta e="T300" id="Seg_10243" s="T299">0.3.h:A</ta>
            <ta e="T302" id="Seg_10244" s="T301">0.3.h:A</ta>
            <ta e="T304" id="Seg_10245" s="T303">np.h:A</ta>
            <ta e="T305" id="Seg_10246" s="T304">np:Ins</ta>
            <ta e="T308" id="Seg_10247" s="T307">pro.h:B</ta>
            <ta e="T309" id="Seg_10248" s="T308">np:P</ta>
            <ta e="T312" id="Seg_10249" s="T311">pro.h:A</ta>
            <ta e="T314" id="Seg_10250" s="T313">np:Th</ta>
            <ta e="T318" id="Seg_10251" s="T317">pro.h:A</ta>
            <ta e="T321" id="Seg_10252" s="T320">pro.h:A</ta>
            <ta e="T324" id="Seg_10253" s="T323">pro.h:A</ta>
            <ta e="T329" id="Seg_10254" s="T328">np:P</ta>
            <ta e="T333" id="Seg_10255" s="T332">pro.h:Poss</ta>
            <ta e="T335" id="Seg_10256" s="T334">np:P</ta>
            <ta e="T337" id="Seg_10257" s="T336">pro.h:Poss</ta>
            <ta e="T339" id="Seg_10258" s="T338">pro.h:Poss</ta>
            <ta e="T341" id="Seg_10259" s="T340">adv:Time</ta>
            <ta e="T343" id="Seg_10260" s="T342">0.3:P</ta>
            <ta e="T345" id="Seg_10261" s="T344">np:Th</ta>
            <ta e="T348" id="Seg_10262" s="T347">adv:Time</ta>
            <ta e="T349" id="Seg_10263" s="T348">pro.h:A</ta>
            <ta e="T352" id="Seg_10264" s="T351">np:G</ta>
            <ta e="T354" id="Seg_10265" s="T353">np:Ins</ta>
            <ta e="T356" id="Seg_10266" s="T355">0.3.h:A</ta>
            <ta e="T357" id="Seg_10267" s="T356">np:P</ta>
            <ta e="T359" id="Seg_10268" s="T358">np:A</ta>
            <ta e="T361" id="Seg_10269" s="T360">0.2.h:A</ta>
            <ta e="T362" id="Seg_10270" s="T361">pro.h:Th</ta>
            <ta e="T364" id="Seg_10271" s="T363">pro.h:A</ta>
            <ta e="T369" id="Seg_10272" s="T368">0.1.h:A</ta>
            <ta e="T370" id="Seg_10273" s="T369">pro.h:Poss</ta>
            <ta e="T372" id="Seg_10274" s="T371">np:P</ta>
            <ta e="T374" id="Seg_10275" s="T373">adv:Time</ta>
            <ta e="T375" id="Seg_10276" s="T374">0.3.h:A</ta>
            <ta e="T376" id="Seg_10277" s="T375">0.3.h:A</ta>
            <ta e="T382" id="Seg_10278" s="T381">np:Ins</ta>
            <ta e="T383" id="Seg_10279" s="T382">0.3.h:A</ta>
            <ta e="T386" id="Seg_10280" s="T385">adv:L</ta>
            <ta e="T387" id="Seg_10281" s="T386">np:P</ta>
            <ta e="T389" id="Seg_10282" s="T388">0.3.h:A</ta>
            <ta e="T390" id="Seg_10283" s="T389">pro.h:A</ta>
            <ta e="T391" id="Seg_10284" s="T390">adv:L</ta>
            <ta e="T394" id="Seg_10285" s="T393">0.2.h:A</ta>
            <ta e="T400" id="Seg_10286" s="T399">np:P</ta>
            <ta e="T402" id="Seg_10287" s="T401">adv:Time</ta>
            <ta e="T404" id="Seg_10288" s="T403">0.3.h:A</ta>
            <ta e="T405" id="Seg_10289" s="T404">pro.h:A</ta>
            <ta e="T409" id="Seg_10290" s="T408">0.3.h:A</ta>
            <ta e="T410" id="Seg_10291" s="T409">adv:L</ta>
            <ta e="T411" id="Seg_10292" s="T410">np:G</ta>
            <ta e="T414" id="Seg_10293" s="T413">np:G</ta>
            <ta e="T415" id="Seg_10294" s="T414">0.3.h:A</ta>
            <ta e="T417" id="Seg_10295" s="T416">0.2.h:A</ta>
            <ta e="T418" id="Seg_10296" s="T417">pro.h:Th</ta>
            <ta e="T419" id="Seg_10297" s="T418">pro.h:Th</ta>
            <ta e="T420" id="Seg_10298" s="T419">0.3.h:A</ta>
            <ta e="T422" id="Seg_10299" s="T421">np:P</ta>
            <ta e="T424" id="Seg_10300" s="T423">0.3.h:A</ta>
            <ta e="T425" id="Seg_10301" s="T424">pro.h:A</ta>
            <ta e="T428" id="Seg_10302" s="T427">0.2.h:A</ta>
            <ta e="T431" id="Seg_10303" s="T430">0.1.h:A</ta>
            <ta e="T433" id="Seg_10304" s="T432">adv:Time</ta>
            <ta e="T434" id="Seg_10305" s="T433">pro.h:A</ta>
            <ta e="T438" id="Seg_10306" s="T437">0.3.h:A</ta>
            <ta e="T439" id="Seg_10307" s="T438">adv:Time</ta>
            <ta e="T441" id="Seg_10308" s="T440">0.3.h:A</ta>
            <ta e="T442" id="Seg_10309" s="T441">adv:L</ta>
            <ta e="T445" id="Seg_10310" s="T444">np:G</ta>
            <ta e="T447" id="Seg_10311" s="T446">0.3.h:A</ta>
            <ta e="T450" id="Seg_10312" s="T449">np:Th</ta>
            <ta e="T451" id="Seg_10313" s="T450">0.3.h:A</ta>
            <ta e="T455" id="Seg_10314" s="T454">pro.h:E</ta>
            <ta e="T458" id="Seg_10315" s="T457">np:G</ta>
            <ta e="T462" id="Seg_10316" s="T461">adv:Time</ta>
            <ta e="T463" id="Seg_10317" s="T462">0.3.h:A</ta>
            <ta e="T465" id="Seg_10318" s="T966">0.3.h:A</ta>
            <ta e="T470" id="Seg_10319" s="T469">0.3.h:A</ta>
            <ta e="T473" id="Seg_10320" s="T472">0.3.h:A</ta>
            <ta e="T474" id="Seg_10321" s="T473">pro.h:P</ta>
            <ta e="T477" id="Seg_10322" s="T967">0.3.h:A</ta>
            <ta e="T482" id="Seg_10323" s="T481">np:Th</ta>
            <ta e="T484" id="Seg_10324" s="T483">0.1.h:A</ta>
            <ta e="T489" id="Seg_10325" s="T488">pro.h:Poss</ta>
            <ta e="T490" id="Seg_10326" s="T489">np:Th</ta>
            <ta e="T493" id="Seg_10327" s="T492">0.3:Th</ta>
            <ta e="T495" id="Seg_10328" s="T494">pro:L</ta>
            <ta e="T496" id="Seg_10329" s="T495">0.2.h:A</ta>
            <ta e="T500" id="Seg_10330" s="T499">np.h:A</ta>
            <ta e="T504" id="Seg_10331" s="T503">np:So</ta>
            <ta e="T507" id="Seg_10332" s="T506">np:Poss</ta>
            <ta e="T509" id="Seg_10333" s="T508">np.h:Th</ta>
            <ta e="T512" id="Seg_10334" s="T511">pro.h:A</ta>
            <ta e="T513" id="Seg_10335" s="T512">pro.h:Th</ta>
            <ta e="T514" id="Seg_10336" s="T513">np:L</ta>
            <ta e="T517" id="Seg_10337" s="T516">np:P</ta>
            <ta e="T518" id="Seg_10338" s="T517">0.3.h:A</ta>
            <ta e="T520" id="Seg_10339" s="T519">pro.h:Th</ta>
            <ta e="T523" id="Seg_10340" s="T522">0.2.h:A</ta>
            <ta e="T526" id="Seg_10341" s="T525">adv:Time</ta>
            <ta e="T527" id="Seg_10342" s="T968">0.3.h:A</ta>
            <ta e="T529" id="Seg_10343" s="T528">np.h:A</ta>
            <ta e="T531" id="Seg_10344" s="T530">0.2.h:A</ta>
            <ta e="T532" id="Seg_10345" s="T531">np:P</ta>
            <ta e="T534" id="Seg_10346" s="T533">pro.h:A</ta>
            <ta e="T538" id="Seg_10347" s="T537">pro.h:Poss</ta>
            <ta e="T539" id="Seg_10348" s="T538">np.h:A</ta>
            <ta e="T541" id="Seg_10349" s="T540">pro.h:Th</ta>
            <ta e="T545" id="Seg_10350" s="T544">adv:Time</ta>
            <ta e="T546" id="Seg_10351" s="T545">np.h:A</ta>
            <ta e="T548" id="Seg_10352" s="T547">np.h:A 0.3.h:Poss</ta>
            <ta e="T551" id="Seg_10353" s="T550">0.2.h:A</ta>
            <ta e="T552" id="Seg_10354" s="T551">np:P</ta>
            <ta e="T553" id="Seg_10355" s="T552">pro.h:A</ta>
            <ta e="T555" id="Seg_10356" s="T554">np:Th</ta>
            <ta e="T557" id="Seg_10357" s="T556">0.1.h:A</ta>
            <ta e="T558" id="Seg_10358" s="T557">np:Th</ta>
            <ta e="T559" id="Seg_10359" s="T558">pro.h:R</ta>
            <ta e="T560" id="Seg_10360" s="T559">np:Th</ta>
            <ta e="T561" id="Seg_10361" s="T560">0.1.h:A</ta>
            <ta e="T563" id="Seg_10362" s="T562">adv:Time</ta>
            <ta e="T564" id="Seg_10363" s="T563">pro.h:A</ta>
            <ta e="T566" id="Seg_10364" s="T565">0.3.h:A</ta>
            <ta e="T567" id="Seg_10365" s="T566">adv:Time</ta>
            <ta e="T568" id="Seg_10366" s="T567">np.h:A 0.3.h:Poss</ta>
            <ta e="T572" id="Seg_10367" s="T571">0.2.h:A</ta>
            <ta e="T574" id="Seg_10368" s="T573">pro.h:Th</ta>
            <ta e="T577" id="Seg_10369" s="T970">0.3.h:A</ta>
            <ta e="T579" id="Seg_10370" s="T578">pro.h:Th</ta>
            <ta e="T580" id="Seg_10371" s="T579">0.2.h:A</ta>
            <ta e="T583" id="Seg_10372" s="T582">adv:Time</ta>
            <ta e="T586" id="Seg_10373" s="T585">np.h:A</ta>
            <ta e="T590" id="Seg_10374" s="T589">0.2.h:A</ta>
            <ta e="T591" id="Seg_10375" s="T590">pro.h:Th</ta>
            <ta e="T595" id="Seg_10376" s="T594">pro.h:A</ta>
            <ta e="T601" id="Seg_10377" s="T600">pro.h:Poss</ta>
            <ta e="T602" id="Seg_10378" s="T601">np.h:Th</ta>
            <ta e="T603" id="Seg_10379" s="T602">0.1.h:Th</ta>
            <ta e="T605" id="Seg_10380" s="T604">pro.h:Poss</ta>
            <ta e="T607" id="Seg_10381" s="T606">np.h:Poss</ta>
            <ta e="T608" id="Seg_10382" s="T607">np:Th</ta>
            <ta e="T612" id="Seg_10383" s="T611">pro.h:Poss</ta>
            <ta e="T613" id="Seg_10384" s="T612">np:Th</ta>
            <ta e="T617" id="Seg_10385" s="T616">adv:Time</ta>
            <ta e="T618" id="Seg_10386" s="T617">np.h:A</ta>
            <ta e="T620" id="Seg_10387" s="T619">adv:Time</ta>
            <ta e="T621" id="Seg_10388" s="T620">np.h:A 0.3.h:Poss</ta>
            <ta e="T624" id="Seg_10389" s="T623">pro.h:A</ta>
            <ta e="T626" id="Seg_10390" s="T625">0.3.h:A</ta>
            <ta e="T628" id="Seg_10391" s="T627">adv:Time</ta>
            <ta e="T629" id="Seg_10392" s="T628">pro.h:Th</ta>
            <ta e="T634" id="Seg_10393" s="T633">np.h:A</ta>
            <ta e="T636" id="Seg_10394" s="T635">np:G</ta>
            <ta e="T638" id="Seg_10395" s="T637">np:P</ta>
            <ta e="T640" id="Seg_10396" s="T639">0.3.h:A</ta>
            <ta e="T643" id="Seg_10397" s="T642">adv:Time</ta>
            <ta e="T644" id="Seg_10398" s="T643">0.3.h:A</ta>
            <ta e="T647" id="Seg_10399" s="T646">pro.h:A</ta>
            <ta e="T649" id="Seg_10400" s="T648">pro.h:Th</ta>
            <ta e="T650" id="Seg_10401" s="T649">pro.h:Poss</ta>
            <ta e="T651" id="Seg_10402" s="T650">np.h:Th</ta>
            <ta e="T652" id="Seg_10403" s="T651">0.1.h:A</ta>
            <ta e="T653" id="Seg_10404" s="T652">np:Th</ta>
            <ta e="T655" id="Seg_10405" s="T654">np:Th</ta>
            <ta e="T656" id="Seg_10406" s="T655">0.1.h:A</ta>
            <ta e="T658" id="Seg_10407" s="T657">0.1.h:A</ta>
            <ta e="T659" id="Seg_10408" s="T658">pro.h:R</ta>
            <ta e="T661" id="Seg_10409" s="T660">np:Th</ta>
            <ta e="T663" id="Seg_10410" s="T662">pro.h:A</ta>
            <ta e="T667" id="Seg_10411" s="T666">pro.h:A</ta>
            <ta e="T668" id="Seg_10412" s="T667">pro.h:P</ta>
            <ta e="T672" id="Seg_10413" s="T671">np.h:A 0.3.h:Poss</ta>
            <ta e="T675" id="Seg_10414" s="T674">np.h:Th</ta>
            <ta e="T677" id="Seg_10415" s="T676">np.h:A</ta>
            <ta e="T683" id="Seg_10416" s="T682">adv:Time</ta>
            <ta e="T684" id="Seg_10417" s="T683">0.3.h:A</ta>
            <ta e="T685" id="Seg_10418" s="T684">np.h:A</ta>
            <ta e="T686" id="Seg_10419" s="T685">np.h:P</ta>
            <ta e="T689" id="Seg_10420" s="T688">pro.h:Th</ta>
            <ta e="T698" id="Seg_10421" s="T697">np:G</ta>
            <ta e="T699" id="Seg_10422" s="T698">0.1.h:A</ta>
            <ta e="T700" id="Seg_10423" s="T699">pro.h:E</ta>
            <ta e="T701" id="Seg_10424" s="T700">pro.h:Th</ta>
            <ta e="T704" id="Seg_10425" s="T972">0.3.h:A</ta>
            <ta e="T706" id="Seg_10426" s="T705">adv:Time</ta>
            <ta e="T707" id="Seg_10427" s="T706">np.h:A</ta>
            <ta e="T709" id="Seg_10428" s="T708">np:G</ta>
            <ta e="T711" id="Seg_10429" s="T710">np.h:A</ta>
            <ta e="T715" id="Seg_10430" s="T714">0.1.h:A</ta>
            <ta e="T717" id="Seg_10431" s="T716">0.1.h:A</ta>
            <ta e="T719" id="Seg_10432" s="T718">np:Pth</ta>
            <ta e="T722" id="Seg_10433" s="T721">np.h:A</ta>
            <ta e="T727" id="Seg_10434" s="T726">pro.h:A</ta>
            <ta e="T731" id="Seg_10435" s="T730">adv:L</ta>
            <ta e="T733" id="Seg_10436" s="T732">0.3.h:A</ta>
            <ta e="T738" id="Seg_10437" s="T737">adv:Time</ta>
            <ta e="T739" id="Seg_10438" s="T738">adv:L</ta>
            <ta e="T741" id="Seg_10439" s="T740">np.h:A</ta>
            <ta e="T746" id="Seg_10440" s="T745">np:G</ta>
            <ta e="T747" id="Seg_10441" s="T973">0.3.h:A</ta>
            <ta e="T749" id="Seg_10442" s="T748">0.1.h:A</ta>
            <ta e="T750" id="Seg_10443" s="T749">pro:G</ta>
            <ta e="T751" id="Seg_10444" s="T750">pro.h:Com</ta>
            <ta e="T756" id="Seg_10445" s="T755">pro:Th</ta>
            <ta e="T757" id="Seg_10446" s="T756">adv:L</ta>
            <ta e="T761" id="Seg_10447" s="T760">pro:Th</ta>
            <ta e="T763" id="Seg_10448" s="T762">np:P</ta>
            <ta e="T764" id="Seg_10449" s="T763">0.1.h:A</ta>
            <ta e="T765" id="Seg_10450" s="T764">np:Th</ta>
            <ta e="T766" id="Seg_10451" s="T765">0.1.h:A</ta>
            <ta e="T768" id="Seg_10452" s="T767">0.1.h:A</ta>
            <ta e="T769" id="Seg_10453" s="T768">np:Ins</ta>
            <ta e="T770" id="Seg_10454" s="T769">0.1.h:A</ta>
            <ta e="T772" id="Seg_10455" s="T771">adv:L</ta>
            <ta e="T774" id="Seg_10456" s="T773">pro:Th</ta>
            <ta e="T776" id="Seg_10457" s="T775">np:Th</ta>
            <ta e="T781" id="Seg_10458" s="T780">np:P</ta>
            <ta e="T782" id="Seg_10459" s="T781">0.3.h:A</ta>
            <ta e="T786" id="Seg_10460" s="T785">np:Th</ta>
            <ta e="T790" id="Seg_10461" s="T789">np:Th</ta>
            <ta e="T796" id="Seg_10462" s="T795">0.1.h:A</ta>
            <ta e="T798" id="Seg_10463" s="T797">pro.h:A</ta>
            <ta e="T799" id="Seg_10464" s="T798">np:P</ta>
            <ta e="T802" id="Seg_10465" s="T801">0.2.h:A</ta>
            <ta e="T805" id="Seg_10466" s="T804">np:P</ta>
            <ta e="T806" id="Seg_10467" s="T805">0.2.h:A</ta>
            <ta e="T810" id="Seg_10468" s="T809">pro.h:A</ta>
            <ta e="T814" id="Seg_10469" s="T813">pro.h:A</ta>
            <ta e="T816" id="Seg_10470" s="T815">np:G</ta>
            <ta e="T820" id="Seg_10471" s="T819">0.1.h:A</ta>
            <ta e="T821" id="Seg_10472" s="T820">pro.h:E</ta>
            <ta e="T823" id="Seg_10473" s="T822">adv:L</ta>
            <ta e="T825" id="Seg_10474" s="T824">np.h:E</ta>
            <ta e="T830" id="Seg_10475" s="T829">0.2.h:E</ta>
            <ta e="T831" id="Seg_10476" s="T830">0.2.h:A</ta>
            <ta e="T834" id="Seg_10477" s="T833">pro:Th</ta>
            <ta e="T838" id="Seg_10478" s="T837">0.3.h:A</ta>
            <ta e="T839" id="Seg_10479" s="T838">pro.h:B</ta>
            <ta e="T841" id="Seg_10480" s="T840">0.2.h:A</ta>
            <ta e="T842" id="Seg_10481" s="T841">adv:Time</ta>
            <ta e="T843" id="Seg_10482" s="T842">np:G</ta>
            <ta e="T844" id="Seg_10483" s="T843">0.2.h:A</ta>
            <ta e="T853" id="Seg_10484" s="T852">pro.h:E</ta>
            <ta e="T855" id="Seg_10485" s="T854">pro.h:Poss</ta>
            <ta e="T856" id="Seg_10486" s="T855">pro:Th</ta>
            <ta e="T859" id="Seg_10487" s="T858">np:Th</ta>
            <ta e="T861" id="Seg_10488" s="T860">np:Th</ta>
            <ta e="T867" id="Seg_10489" s="T866">pro.h:A</ta>
            <ta e="T871" id="Seg_10490" s="T870">pro.h:E</ta>
            <ta e="T872" id="Seg_10491" s="T871">np.h:Th</ta>
            <ta e="T874" id="Seg_10492" s="T873">0.3.h:A</ta>
            <ta e="T876" id="Seg_10493" s="T875">pro.h:A</ta>
            <ta e="T882" id="Seg_10494" s="T881">np:G</ta>
            <ta e="T884" id="Seg_10495" s="T883">adv:Time</ta>
            <ta e="T885" id="Seg_10496" s="T884">np:Th</ta>
            <ta e="T888" id="Seg_10497" s="T887">pro.h:A</ta>
            <ta e="T892" id="Seg_10498" s="T891">pro.h:A</ta>
            <ta e="T896" id="Seg_10499" s="T895">adv:Time</ta>
            <ta e="T897" id="Seg_10500" s="T896">0.1.h:E</ta>
            <ta e="T898" id="Seg_10501" s="T897">pro.h:A</ta>
            <ta e="T901" id="Seg_10502" s="T900">np:P</ta>
            <ta e="T906" id="Seg_10503" s="T905">0.1.h:E</ta>
            <ta e="T917" id="Seg_10504" s="T916">0.1.h:E</ta>
            <ta e="T918" id="Seg_10505" s="T917">np:G</ta>
            <ta e="T919" id="Seg_10506" s="T918">0.1.h:A</ta>
            <ta e="T921" id="Seg_10507" s="T920">pro.h:A</ta>
            <ta e="T924" id="Seg_10508" s="T923">0.1.h:E</ta>
            <ta e="T927" id="Seg_10509" s="T926">np:G</ta>
            <ta e="T931" id="Seg_10510" s="T930">pro.h:A</ta>
            <ta e="T932" id="Seg_10511" s="T931">pro.h:R</ta>
            <ta e="T934" id="Seg_10512" s="T933">pro.h:A</ta>
            <ta e="T936" id="Seg_10513" s="T935">np:G</ta>
            <ta e="T940" id="Seg_10514" s="T939">np:P</ta>
            <ta e="T941" id="Seg_10515" s="T940">0.3.h:A</ta>
            <ta e="T943" id="Seg_10516" s="T942">np:G</ta>
            <ta e="T944" id="Seg_10517" s="T943">0.3.h:A</ta>
            <ta e="T947" id="Seg_10518" s="T946">adv:L</ta>
            <ta e="T949" id="Seg_10519" s="T948">np.h:A</ta>
            <ta e="T953" id="Seg_10520" s="T952">np:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_10521" s="T1">pro:S</ta>
            <ta e="T4" id="Seg_10522" s="T3">n:pred</ta>
            <ta e="T7" id="Seg_10523" s="T6">v:pred 0.3.h:S</ta>
            <ta e="T14" id="Seg_10524" s="T13">adj:pred</ta>
            <ta e="T15" id="Seg_10525" s="T14">cop 0.3.h:S</ta>
            <ta e="T17" id="Seg_10526" s="T16">pro.h:S</ta>
            <ta e="T20" id="Seg_10527" s="T19">n:pred</ta>
            <ta e="T22" id="Seg_10528" s="T21">v:pred 0.1.h:S</ta>
            <ta e="T23" id="Seg_10529" s="T22">v:pred 0.1.h:S</ta>
            <ta e="T27" id="Seg_10530" s="T26">pro.h:S</ta>
            <ta e="T28" id="Seg_10531" s="T27">pro:O</ta>
            <ta e="T29" id="Seg_10532" s="T28">ptcl.neg</ta>
            <ta e="T30" id="Seg_10533" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_10534" s="T31">pro.h:S</ta>
            <ta e="T34" id="Seg_10535" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_10536" s="T34">adj:pred</ta>
            <ta e="T36" id="Seg_10537" s="T35">cop 0.1.h:S</ta>
            <ta e="T39" id="Seg_10538" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_10539" s="T39">v:pred</ta>
            <ta e="T45" id="Seg_10540" s="T44">ptcl.neg</ta>
            <ta e="T46" id="Seg_10541" s="T45">v:pred 0.1.h:S</ta>
            <ta e="T47" id="Seg_10542" s="T46">adj:pred</ta>
            <ta e="T48" id="Seg_10543" s="T47">v:pred 0.1.h:S</ta>
            <ta e="T49" id="Seg_10544" s="T48">ptcl.neg</ta>
            <ta e="T50" id="Seg_10545" s="T49">v:pred 0.1.h:S</ta>
            <ta e="T53" id="Seg_10546" s="T52">ptcl.neg</ta>
            <ta e="T54" id="Seg_10547" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_10548" s="T62">v:pred</ta>
            <ta e="T66" id="Seg_10549" s="T65">pro:O</ta>
            <ta e="T68" id="Seg_10550" s="T67">ptcl.neg</ta>
            <ta e="T69" id="Seg_10551" s="T68">v:pred 0.1.h:S</ta>
            <ta e="T71" id="Seg_10552" s="T70">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_10553" s="T71">np:O</ta>
            <ta e="T74" id="Seg_10554" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T85" id="Seg_10555" s="T84">pro.h:S</ta>
            <ta e="T86" id="Seg_10556" s="T85">v:pred</ta>
            <ta e="T90" id="Seg_10557" s="T89">np:S</ta>
            <ta e="T91" id="Seg_10558" s="T90">adj:pred</ta>
            <ta e="T95" id="Seg_10559" s="T94">v:pred 0.3.h:S</ta>
            <ta e="T99" id="Seg_10560" s="T98">v:pred 0.1.h:S</ta>
            <ta e="T101" id="Seg_10561" s="T100">adv:pred</ta>
            <ta e="T102" id="Seg_10562" s="T101">cop 0.3:S</ta>
            <ta e="T105" id="Seg_10563" s="T104">np:O</ta>
            <ta e="T106" id="Seg_10564" s="T105">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_10565" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_10566" s="T110">np:O</ta>
            <ta e="T115" id="Seg_10567" s="T114">np:S</ta>
            <ta e="T116" id="Seg_10568" s="T115">v:pred</ta>
            <ta e="T118" id="Seg_10569" s="T117">pro.h:S</ta>
            <ta e="T120" id="Seg_10570" s="T119">v:pred</ta>
            <ta e="T123" id="Seg_10571" s="T122">v:pred 0.1.h:S</ta>
            <ta e="T126" id="Seg_10572" s="T125">v:pred 0.2.h:S</ta>
            <ta e="T128" id="Seg_10573" s="T127">np:O</ta>
            <ta e="T129" id="Seg_10574" s="T128">v:pred 0.2.h:S</ta>
            <ta e="T134" id="Seg_10575" s="T133">np.h:S</ta>
            <ta e="T964" id="Seg_10576" s="T135">conv:pred</ta>
            <ta e="T136" id="Seg_10577" s="T964">v:pred</ta>
            <ta e="T139" id="Seg_10578" s="T138">np:O</ta>
            <ta e="T140" id="Seg_10579" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T143" id="Seg_10580" s="T142">ptcl:pred</ta>
            <ta e="T144" id="Seg_10581" s="T143">np:O</ta>
            <ta e="T147" id="Seg_10582" s="T146">pro.h:S</ta>
            <ta e="T148" id="Seg_10583" s="T147">v:pred</ta>
            <ta e="T155" id="Seg_10584" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_10585" s="T159">np.h:S</ta>
            <ta e="T965" id="Seg_10586" s="T160">conv:pred</ta>
            <ta e="T161" id="Seg_10587" s="T965">v:pred</ta>
            <ta e="T162" id="Seg_10588" s="T161">conv:pred</ta>
            <ta e="T165" id="Seg_10589" s="T164">s:purp</ta>
            <ta e="T168" id="Seg_10590" s="T167">np.h:O</ta>
            <ta e="T169" id="Seg_10591" s="T168">v:pred 0.3.h:S</ta>
            <ta e="T173" id="Seg_10592" s="T172">v:pred 0.3.h:S</ta>
            <ta e="T177" id="Seg_10593" s="T176">np.h:S</ta>
            <ta e="T178" id="Seg_10594" s="T177">v:pred</ta>
            <ta e="T187" id="Seg_10595" s="T186">np.h:S</ta>
            <ta e="T188" id="Seg_10596" s="T187">np:O</ta>
            <ta e="T189" id="Seg_10597" s="T188">v:pred</ta>
            <ta e="T193" id="Seg_10598" s="T192">np.h:S</ta>
            <ta e="T196" id="Seg_10599" s="T195">v:pred</ta>
            <ta e="T197" id="Seg_10600" s="T196">pro.h:S</ta>
            <ta e="T199" id="Seg_10601" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_10602" s="T199">np:O</ta>
            <ta e="T204" id="Seg_10603" s="T203">np.h:S</ta>
            <ta e="T205" id="Seg_10604" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_10605" s="T205">v:pred</ta>
            <ta e="T211" id="Seg_10606" s="T210">v:pred 0.3.h:S</ta>
            <ta e="T213" id="Seg_10607" s="T212">s:purp</ta>
            <ta e="T215" id="Seg_10608" s="T214">v:pred 0.3:S</ta>
            <ta e="T219" id="Seg_10609" s="T218">ptcl.neg</ta>
            <ta e="T220" id="Seg_10610" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_10611" s="T221">pro:S</ta>
            <ta e="T224" id="Seg_10612" s="T223">adj:pred</ta>
            <ta e="T225" id="Seg_10613" s="T224">cop</ta>
            <ta e="T231" id="Seg_10614" s="T230">v:pred 0.3:S</ta>
            <ta e="T234" id="Seg_10615" s="T233">ptcl.neg</ta>
            <ta e="T235" id="Seg_10616" s="T234">adj:pred</ta>
            <ta e="T236" id="Seg_10617" s="T235">cop 0.3:S</ta>
            <ta e="T238" id="Seg_10618" s="T237">pro.h:S</ta>
            <ta e="T239" id="Seg_10619" s="T238">ptcl.neg</ta>
            <ta e="T240" id="Seg_10620" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_10621" s="T240">v:pred 0.3.h:S</ta>
            <ta e="T247" id="Seg_10622" s="T246">pro:O</ta>
            <ta e="T248" id="Seg_10623" s="T247">v:pred 0.3.h:S</ta>
            <ta e="T249" id="Seg_10624" s="T248">v:pred 0.3:S</ta>
            <ta e="T253" id="Seg_10625" s="T252">np.h:S</ta>
            <ta e="T254" id="Seg_10626" s="T253">v:pred</ta>
            <ta e="T258" id="Seg_10627" s="T257">np.h:S</ta>
            <ta e="T260" id="Seg_10628" s="T259">adj:pred</ta>
            <ta e="T261" id="Seg_10629" s="T260">cop</ta>
            <ta e="T264" id="Seg_10630" s="T263">np.h:S</ta>
            <ta e="T266" id="Seg_10631" s="T265">v:pred</ta>
            <ta e="T268" id="Seg_10632" s="T267">pro.h:S</ta>
            <ta e="T270" id="Seg_10633" s="T269">conv:pred</ta>
            <ta e="T271" id="Seg_10634" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_10635" s="T271">np:O</ta>
            <ta e="T273" id="Seg_10636" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T278" id="Seg_10637" s="T277">np.h:S</ta>
            <ta e="T280" id="Seg_10638" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_10639" s="T280">np:O</ta>
            <ta e="T283" id="Seg_10640" s="T282">np:O</ta>
            <ta e="T284" id="Seg_10641" s="T283">v:pred 0.3.h:S</ta>
            <ta e="T287" id="Seg_10642" s="T286">np:O</ta>
            <ta e="T289" id="Seg_10643" s="T288">v:pred 0.3.h:S</ta>
            <ta e="T294" id="Seg_10644" s="T293">pro.h:S</ta>
            <ta e="T295" id="Seg_10645" s="T294">v:pred</ta>
            <ta e="T296" id="Seg_10646" s="T295">np.h:S</ta>
            <ta e="T297" id="Seg_10647" s="T296">v:pred</ta>
            <ta e="T299" id="Seg_10648" s="T298">np:O</ta>
            <ta e="T300" id="Seg_10649" s="T299">v:pred 0.3.h:S</ta>
            <ta e="T302" id="Seg_10650" s="T301">v:pred 0.3.h:S</ta>
            <ta e="T304" id="Seg_10651" s="T303">np.h:S</ta>
            <ta e="T307" id="Seg_10652" s="T306">v:pred</ta>
            <ta e="T309" id="Seg_10653" s="T308">np:O</ta>
            <ta e="T312" id="Seg_10654" s="T311">pro.h:S</ta>
            <ta e="T314" id="Seg_10655" s="T313">np:O</ta>
            <ta e="T316" id="Seg_10656" s="T315">v:pred</ta>
            <ta e="T318" id="Seg_10657" s="T317">pro.h:S</ta>
            <ta e="T319" id="Seg_10658" s="T318">v:pred</ta>
            <ta e="T321" id="Seg_10659" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_10660" s="T321">v:pred</ta>
            <ta e="T324" id="Seg_10661" s="T323">pro.h:S</ta>
            <ta e="T327" id="Seg_10662" s="T326">v:pred</ta>
            <ta e="T329" id="Seg_10663" s="T328">np:O</ta>
            <ta e="T334" id="Seg_10664" s="T333">adj:pred</ta>
            <ta e="T335" id="Seg_10665" s="T334">np:S</ta>
            <ta e="T336" id="Seg_10666" s="T335">cop</ta>
            <ta e="T342" id="Seg_10667" s="T341">adj:pred</ta>
            <ta e="T343" id="Seg_10668" s="T342">cop 0.3:S</ta>
            <ta e="T345" id="Seg_10669" s="T344">np:S</ta>
            <ta e="T346" id="Seg_10670" s="T345">v:pred</ta>
            <ta e="T349" id="Seg_10671" s="T348">pro.h:S</ta>
            <ta e="T351" id="Seg_10672" s="T350">v:pred</ta>
            <ta e="T356" id="Seg_10673" s="T355">v:pred 0.3.h:S</ta>
            <ta e="T357" id="Seg_10674" s="T356">np:O</ta>
            <ta e="T358" id="Seg_10675" s="T357">v:pred</ta>
            <ta e="T359" id="Seg_10676" s="T358">np:S</ta>
            <ta e="T361" id="Seg_10677" s="T360">v:pred 0.2.h:S</ta>
            <ta e="T364" id="Seg_10678" s="T363">pro.h:S</ta>
            <ta e="T365" id="Seg_10679" s="T364">ptcl.neg</ta>
            <ta e="T366" id="Seg_10680" s="T365">v:pred</ta>
            <ta e="T369" id="Seg_10681" s="T368">v:pred 0.1.h:S</ta>
            <ta e="T372" id="Seg_10682" s="T371">np:O</ta>
            <ta e="T375" id="Seg_10683" s="T374">v:pred 0.3.h:S</ta>
            <ta e="T376" id="Seg_10684" s="T375">pro.h:S</ta>
            <ta e="T377" id="Seg_10685" s="T376">v:pred</ta>
            <ta e="T383" id="Seg_10686" s="T382">v:pred 0.3.h:S</ta>
            <ta e="T387" id="Seg_10687" s="T386">np:O</ta>
            <ta e="T389" id="Seg_10688" s="T388">v:pred 0.3.h:S</ta>
            <ta e="T390" id="Seg_10689" s="T389">pro.h:S</ta>
            <ta e="T392" id="Seg_10690" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_10691" s="T393">v:pred 0.2.h:S</ta>
            <ta e="T399" id="Seg_10692" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_10693" s="T399">np:S</ta>
            <ta e="T404" id="Seg_10694" s="T403">v:pred 0.3.h:S</ta>
            <ta e="T405" id="Seg_10695" s="T404">pro.h:S</ta>
            <ta e="T406" id="Seg_10696" s="T405">v:pred</ta>
            <ta e="T409" id="Seg_10697" s="T408">v:pred 0.3.h:S</ta>
            <ta e="T415" id="Seg_10698" s="T414">v:pred 0.3.h:S</ta>
            <ta e="T417" id="Seg_10699" s="T416">v:pred 0.2.h:S</ta>
            <ta e="T419" id="Seg_10700" s="T418">pro.h:O</ta>
            <ta e="T420" id="Seg_10701" s="T419">v:pred 0.3.h:S</ta>
            <ta e="T422" id="Seg_10702" s="T421">np:O</ta>
            <ta e="T424" id="Seg_10703" s="T423">v:pred 0.3.h:S</ta>
            <ta e="T425" id="Seg_10704" s="T424">pro.h:S</ta>
            <ta e="T426" id="Seg_10705" s="T425">v:pred</ta>
            <ta e="T428" id="Seg_10706" s="T427">v:pred 0.2.h:S</ta>
            <ta e="T430" id="Seg_10707" s="T429">ptcl.neg</ta>
            <ta e="T431" id="Seg_10708" s="T430">v:pred 0.1.h:S</ta>
            <ta e="T436" id="Seg_10709" s="T435">ptcl:pred</ta>
            <ta e="T437" id="Seg_10710" s="T436">ptcl.neg</ta>
            <ta e="T438" id="Seg_10711" s="T437">v:pred 0.3.h:S</ta>
            <ta e="T441" id="Seg_10712" s="T440">v:pred 0.3.h:S</ta>
            <ta e="T447" id="Seg_10713" s="T446">v:pred 0.3.h:S</ta>
            <ta e="T450" id="Seg_10714" s="T449">np:O</ta>
            <ta e="T451" id="Seg_10715" s="T450">v:pred 0.3.h:S</ta>
            <ta e="T455" id="Seg_10716" s="T454">pro.h:S</ta>
            <ta e="T457" id="Seg_10717" s="T456">v:pred</ta>
            <ta e="T463" id="Seg_10718" s="T462">v:pred 0.3.h:S</ta>
            <ta e="T966" id="Seg_10719" s="T464">conv:pred</ta>
            <ta e="T465" id="Seg_10720" s="T966">v:pred 0.3.h:S</ta>
            <ta e="T469" id="Seg_10721" s="T468">ptcl.neg</ta>
            <ta e="T470" id="Seg_10722" s="T469">v:pred 0.3.h:S</ta>
            <ta e="T473" id="Seg_10723" s="T472">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_10724" s="T473">pro.h:O</ta>
            <ta e="T967" id="Seg_10725" s="T476">conv:pred</ta>
            <ta e="T477" id="Seg_10726" s="T967">v:pred 0.3.h:S</ta>
            <ta e="T484" id="Seg_10727" s="T483">v:pred 0.1.h:S</ta>
            <ta e="T490" id="Seg_10728" s="T489">np:S</ta>
            <ta e="T491" id="Seg_10729" s="T490">v:pred</ta>
            <ta e="T493" id="Seg_10730" s="T492">v:pred 0.3:S</ta>
            <ta e="T496" id="Seg_10731" s="T495">v:pred 0.2.h:S</ta>
            <ta e="T500" id="Seg_10732" s="T499">np.h:S</ta>
            <ta e="T501" id="Seg_10733" s="T500">v:pred</ta>
            <ta e="T509" id="Seg_10734" s="T508">np.h:S</ta>
            <ta e="T510" id="Seg_10735" s="T509">v:pred</ta>
            <ta e="T512" id="Seg_10736" s="T511">pro.h:S</ta>
            <ta e="T513" id="Seg_10737" s="T512">pro.h:O</ta>
            <ta e="T515" id="Seg_10738" s="T514">v:pred</ta>
            <ta e="T517" id="Seg_10739" s="T516">np:O</ta>
            <ta e="T518" id="Seg_10740" s="T517">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_10741" s="T519">pro.h:O</ta>
            <ta e="T523" id="Seg_10742" s="T522">v:pred 0.2.h:S</ta>
            <ta e="T968" id="Seg_10743" s="T526">conv:pred</ta>
            <ta e="T527" id="Seg_10744" s="T968">v:pred 0.3.h:S</ta>
            <ta e="T529" id="Seg_10745" s="T528">np.h:S</ta>
            <ta e="T530" id="Seg_10746" s="T529">v:pred</ta>
            <ta e="T531" id="Seg_10747" s="T530">v:pred 0.2.h:S</ta>
            <ta e="T532" id="Seg_10748" s="T531">np:O</ta>
            <ta e="T534" id="Seg_10749" s="T533">pro.h:S</ta>
            <ta e="T535" id="Seg_10750" s="T534">ptcl.neg</ta>
            <ta e="T536" id="Seg_10751" s="T535">v:pred</ta>
            <ta e="T539" id="Seg_10752" s="T538">np.h:S</ta>
            <ta e="T540" id="Seg_10753" s="T539">v:pred</ta>
            <ta e="T541" id="Seg_10754" s="T540">pro.h:O</ta>
            <ta e="T542" id="Seg_10755" s="T541">ptcl.neg</ta>
            <ta e="T543" id="Seg_10756" s="T542">v:pred</ta>
            <ta e="T546" id="Seg_10757" s="T545">np.h:S</ta>
            <ta e="T969" id="Seg_10758" s="T546">conv:pred</ta>
            <ta e="T547" id="Seg_10759" s="T969">v:pred</ta>
            <ta e="T548" id="Seg_10760" s="T547">np.h:S</ta>
            <ta e="T549" id="Seg_10761" s="T548">v:pred</ta>
            <ta e="T551" id="Seg_10762" s="T550">v:pred 0.2.h:S</ta>
            <ta e="T552" id="Seg_10763" s="T551">np:O</ta>
            <ta e="T553" id="Seg_10764" s="T552">pro.h:S</ta>
            <ta e="T554" id="Seg_10765" s="T553">v:pred</ta>
            <ta e="T555" id="Seg_10766" s="T554">np:O</ta>
            <ta e="T557" id="Seg_10767" s="T556">v:pred 0.1.h:S</ta>
            <ta e="T558" id="Seg_10768" s="T557">np:O</ta>
            <ta e="T560" id="Seg_10769" s="T559">np:O</ta>
            <ta e="T561" id="Seg_10770" s="T560">v:pred 0.1.h:S</ta>
            <ta e="T564" id="Seg_10771" s="T563">pro.h:S</ta>
            <ta e="T565" id="Seg_10772" s="T564">v:pred</ta>
            <ta e="T566" id="Seg_10773" s="T565">v:pred 0.3.h:S</ta>
            <ta e="T568" id="Seg_10774" s="T567">np.h:S</ta>
            <ta e="T570" id="Seg_10775" s="T569">v:pred</ta>
            <ta e="T572" id="Seg_10776" s="T571">v:pred 0.2.h:S</ta>
            <ta e="T574" id="Seg_10777" s="T573">pro.h:O</ta>
            <ta e="T970" id="Seg_10778" s="T576">conv:pred</ta>
            <ta e="T577" id="Seg_10779" s="T970">v:pred 0.2.h:S</ta>
            <ta e="T579" id="Seg_10780" s="T578">pro.h:O</ta>
            <ta e="T580" id="Seg_10781" s="T579">v:pred 0.2.h:S</ta>
            <ta e="T585" id="Seg_10782" s="T584">v:pred</ta>
            <ta e="T586" id="Seg_10783" s="T585">np.h:S</ta>
            <ta e="T590" id="Seg_10784" s="T589">v:pred 0.2.h:S</ta>
            <ta e="T595" id="Seg_10785" s="T594">pro.h:S</ta>
            <ta e="T596" id="Seg_10786" s="T595">ptcl.neg</ta>
            <ta e="T597" id="Seg_10787" s="T596">conv:pred</ta>
            <ta e="T602" id="Seg_10788" s="T601">n:pred</ta>
            <ta e="T603" id="Seg_10789" s="T602">cop 0.1.h:S</ta>
            <ta e="T608" id="Seg_10790" s="T607">np:S</ta>
            <ta e="T609" id="Seg_10791" s="T608">adj:pred</ta>
            <ta e="T613" id="Seg_10792" s="T612">np:S</ta>
            <ta e="T615" id="Seg_10793" s="T614">adj:pred</ta>
            <ta e="T618" id="Seg_10794" s="T617">np.h:S</ta>
            <ta e="T971" id="Seg_10795" s="T618">conv:pred</ta>
            <ta e="T619" id="Seg_10796" s="T971">v:pred</ta>
            <ta e="T621" id="Seg_10797" s="T620">np.h:S</ta>
            <ta e="T622" id="Seg_10798" s="T621">v:pred</ta>
            <ta e="T624" id="Seg_10799" s="T623">pro.h:S</ta>
            <ta e="T625" id="Seg_10800" s="T624">v:pred</ta>
            <ta e="T626" id="Seg_10801" s="T625">v:pred 0.3.h:S</ta>
            <ta e="T629" id="Seg_10802" s="T628">pro.h:O</ta>
            <ta e="T630" id="Seg_10803" s="T629">ptcl.neg</ta>
            <ta e="T631" id="Seg_10804" s="T630">v:pred</ta>
            <ta e="T634" id="Seg_10805" s="T633">np.h:S</ta>
            <ta e="T635" id="Seg_10806" s="T634">v:pred</ta>
            <ta e="T638" id="Seg_10807" s="T637">np:O</ta>
            <ta e="T640" id="Seg_10808" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T644" id="Seg_10809" s="T643">v:pred 0.3.h:S</ta>
            <ta e="T647" id="Seg_10810" s="T646">pro.h:S</ta>
            <ta e="T648" id="Seg_10811" s="T647">v:pred</ta>
            <ta e="T649" id="Seg_10812" s="T648">pro.h:S</ta>
            <ta e="T651" id="Seg_10813" s="T650">n:pred</ta>
            <ta e="T652" id="Seg_10814" s="T651">v:pred 0.1.h:S</ta>
            <ta e="T653" id="Seg_10815" s="T652">np:O</ta>
            <ta e="T655" id="Seg_10816" s="T654">np:O</ta>
            <ta e="T656" id="Seg_10817" s="T655">v:pred 0.1.h:S</ta>
            <ta e="T658" id="Seg_10818" s="T657">v:pred 0.1.h:S</ta>
            <ta e="T661" id="Seg_10819" s="T660">np:O</ta>
            <ta e="T663" id="Seg_10820" s="T662">pro.h:S</ta>
            <ta e="T665" id="Seg_10821" s="T664">v:pred</ta>
            <ta e="T667" id="Seg_10822" s="T666">pro.h:S</ta>
            <ta e="T668" id="Seg_10823" s="T667">pro.h:O</ta>
            <ta e="T670" id="Seg_10824" s="T669">v:pred</ta>
            <ta e="T672" id="Seg_10825" s="T671">np.h:S</ta>
            <ta e="T673" id="Seg_10826" s="T672">v:pred</ta>
            <ta e="T675" id="Seg_10827" s="T674">np.h:S</ta>
            <ta e="T676" id="Seg_10828" s="T675">v:pred</ta>
            <ta e="T677" id="Seg_10829" s="T676">np.h:S</ta>
            <ta e="T681" id="Seg_10830" s="T680">v:pred</ta>
            <ta e="T684" id="Seg_10831" s="T683">v:pred 0.3.h:S</ta>
            <ta e="T685" id="Seg_10832" s="T684">np.h:S</ta>
            <ta e="T686" id="Seg_10833" s="T685">np.h:O</ta>
            <ta e="T687" id="Seg_10834" s="T686">v:pred</ta>
            <ta e="T689" id="Seg_10835" s="T688">pro.h:S</ta>
            <ta e="T691" id="Seg_10836" s="T690">v:pred</ta>
            <ta e="T699" id="Seg_10837" s="T698">v:pred 0.1.h:S</ta>
            <ta e="T700" id="Seg_10838" s="T699">pro.h:S</ta>
            <ta e="T702" id="Seg_10839" s="T701">ptcl.neg</ta>
            <ta e="T703" id="Seg_10840" s="T702">v:pred</ta>
            <ta e="T972" id="Seg_10841" s="T703">conv:pred</ta>
            <ta e="T704" id="Seg_10842" s="T972">v:pred 0.3.h:S</ta>
            <ta e="T707" id="Seg_10843" s="T706">np.h:S</ta>
            <ta e="T708" id="Seg_10844" s="T707">v:pred</ta>
            <ta e="T711" id="Seg_10845" s="T710">np.h:S</ta>
            <ta e="T712" id="Seg_10846" s="T711">v:pred</ta>
            <ta e="T715" id="Seg_10847" s="T714">v:pred 0.1.h:S</ta>
            <ta e="T717" id="Seg_10848" s="T716">v:pred 0.1.h:S</ta>
            <ta e="T722" id="Seg_10849" s="T721">np.h:S</ta>
            <ta e="T723" id="Seg_10850" s="T722">v:pred</ta>
            <ta e="T727" id="Seg_10851" s="T726">pro.h:S</ta>
            <ta e="T728" id="Seg_10852" s="T727">ptcl.neg</ta>
            <ta e="T729" id="Seg_10853" s="T728">v:pred</ta>
            <ta e="T733" id="Seg_10854" s="T732">v:pred 0.3.h:S</ta>
            <ta e="T741" id="Seg_10855" s="T740">np.h:S</ta>
            <ta e="T743" id="Seg_10856" s="T742">v:pred</ta>
            <ta e="T973" id="Seg_10857" s="T746">conv:pred</ta>
            <ta e="T747" id="Seg_10858" s="T973">v:pred 0.3.h:S</ta>
            <ta e="T749" id="Seg_10859" s="T748">v:pred 0.1.h:S</ta>
            <ta e="T753" id="Seg_10860" s="T752">conv:pred</ta>
            <ta e="T756" id="Seg_10861" s="T755">pro:S</ta>
            <ta e="T757" id="Seg_10862" s="T756">adj:pred</ta>
            <ta e="T758" id="Seg_10863" s="T757">cop</ta>
            <ta e="T761" id="Seg_10864" s="T760">pro:S</ta>
            <ta e="T762" id="Seg_10865" s="T761">v:pred</ta>
            <ta e="T763" id="Seg_10866" s="T762">np:O</ta>
            <ta e="T764" id="Seg_10867" s="T763">v:pred 0.1.h:S</ta>
            <ta e="T765" id="Seg_10868" s="T764">np:O</ta>
            <ta e="T766" id="Seg_10869" s="T765">v:pred 0.1.h:S</ta>
            <ta e="T768" id="Seg_10870" s="T767">v:pred 0.1.h:S</ta>
            <ta e="T770" id="Seg_10871" s="T769">v:pred 0.1.h:S</ta>
            <ta e="T774" id="Seg_10872" s="T773">pro:S</ta>
            <ta e="T775" id="Seg_10873" s="T774">adj:pred</ta>
            <ta e="T776" id="Seg_10874" s="T775">np:S</ta>
            <ta e="T777" id="Seg_10875" s="T776">adj:pred</ta>
            <ta e="T781" id="Seg_10876" s="T780">np:O</ta>
            <ta e="T782" id="Seg_10877" s="T781">v:pred 0.3.h:S</ta>
            <ta e="T786" id="Seg_10878" s="T785">np:S</ta>
            <ta e="T787" id="Seg_10879" s="T786">v:pred</ta>
            <ta e="T790" id="Seg_10880" s="T789">np:S</ta>
            <ta e="T791" id="Seg_10881" s="T790">v:pred</ta>
            <ta e="T796" id="Seg_10882" s="T795">v:pred 0.1.h:S</ta>
            <ta e="T798" id="Seg_10883" s="T797">pro.h:S</ta>
            <ta e="T799" id="Seg_10884" s="T798">np:O</ta>
            <ta e="T800" id="Seg_10885" s="T799">v:pred</ta>
            <ta e="T802" id="Seg_10886" s="T801">v:pred 0.2.h:S</ta>
            <ta e="T805" id="Seg_10887" s="T804">np:O</ta>
            <ta e="T806" id="Seg_10888" s="T805">v:pred 0.2.h:S</ta>
            <ta e="T810" id="Seg_10889" s="T809">pro.h:S</ta>
            <ta e="T812" id="Seg_10890" s="T811">v:pred</ta>
            <ta e="T814" id="Seg_10891" s="T813">pro.h:S</ta>
            <ta e="T817" id="Seg_10892" s="T816">ptcl.neg</ta>
            <ta e="T818" id="Seg_10893" s="T817">v:pred</ta>
            <ta e="T819" id="Seg_10894" s="T818">ptcl.neg</ta>
            <ta e="T820" id="Seg_10895" s="T819">v:pred 0.1.h:S</ta>
            <ta e="T821" id="Seg_10896" s="T820">pro.h:S</ta>
            <ta e="T822" id="Seg_10897" s="T821">v:pred</ta>
            <ta e="T825" id="Seg_10898" s="T824">np.h:S</ta>
            <ta e="T826" id="Seg_10899" s="T825">v:pred</ta>
            <ta e="T830" id="Seg_10900" s="T829">v:pred 0.2.h:S</ta>
            <ta e="T831" id="Seg_10901" s="T830">v:pred 0.2.h:S</ta>
            <ta e="T834" id="Seg_10902" s="T833">pro:O</ta>
            <ta e="T835" id="Seg_10903" s="T834">ptcl.neg</ta>
            <ta e="T837" id="Seg_10904" s="T836">ptcl.neg</ta>
            <ta e="T838" id="Seg_10905" s="T837">v:pred 0.3.h:S</ta>
            <ta e="T841" id="Seg_10906" s="T840">v:pred 0.2.h:S</ta>
            <ta e="T844" id="Seg_10907" s="T843">v:pred 0.2.h:S</ta>
            <ta e="T847" id="Seg_10908" s="T846">ptcl:pred</ta>
            <ta e="T853" id="Seg_10909" s="T852">pro.h:S</ta>
            <ta e="T854" id="Seg_10910" s="T853">v:pred</ta>
            <ta e="T856" id="Seg_10911" s="T855">pro:S</ta>
            <ta e="T857" id="Seg_10912" s="T856">v:pred</ta>
            <ta e="T859" id="Seg_10913" s="T858">np:S</ta>
            <ta e="T860" id="Seg_10914" s="T859">v:pred</ta>
            <ta e="T861" id="Seg_10915" s="T860">np:S</ta>
            <ta e="T862" id="Seg_10916" s="T861">v:pred</ta>
            <ta e="T867" id="Seg_10917" s="T866">pro.h:S</ta>
            <ta e="T868" id="Seg_10918" s="T867">ptcl.neg</ta>
            <ta e="T869" id="Seg_10919" s="T868">v:pred</ta>
            <ta e="T871" id="Seg_10920" s="T870">pro.h:S</ta>
            <ta e="T873" id="Seg_10921" s="T872">v:pred</ta>
            <ta e="T874" id="Seg_10922" s="T873">v:pred 0.3.h:S</ta>
            <ta e="T876" id="Seg_10923" s="T875">pro.h:S</ta>
            <ta e="T877" id="Seg_10924" s="T876">v:pred</ta>
            <ta e="T885" id="Seg_10925" s="T884">np:S</ta>
            <ta e="T886" id="Seg_10926" s="T885">v:pred</ta>
            <ta e="T888" id="Seg_10927" s="T887">pro.h:S</ta>
            <ta e="T890" id="Seg_10928" s="T889">v:pred</ta>
            <ta e="T892" id="Seg_10929" s="T891">pro.h:S</ta>
            <ta e="T893" id="Seg_10930" s="T892">v:pred</ta>
            <ta e="T897" id="Seg_10931" s="T896">v:pred 0.1.h:S</ta>
            <ta e="T898" id="Seg_10932" s="T897">pro.h:S</ta>
            <ta e="T901" id="Seg_10933" s="T900">np:O</ta>
            <ta e="T906" id="Seg_10934" s="T905">v:pred 0.1.h:S</ta>
            <ta e="T907" id="Seg_10935" s="T906">np.h:S</ta>
            <ta e="T916" id="Seg_10936" s="T915">ptcl.neg</ta>
            <ta e="T917" id="Seg_10937" s="T916">v:pred 0.1.h:S</ta>
            <ta e="T919" id="Seg_10938" s="T918">v:pred 0.1.h:S</ta>
            <ta e="T921" id="Seg_10939" s="T920">pro.h:S</ta>
            <ta e="T922" id="Seg_10940" s="T921">v:pred</ta>
            <ta e="T924" id="Seg_10941" s="T923">v:pred 0.1.h:S</ta>
            <ta e="T928" id="Seg_10942" s="T927">ptcl:pred</ta>
            <ta e="T931" id="Seg_10943" s="T930">pro.h:S</ta>
            <ta e="T933" id="Seg_10944" s="T932">v:pred</ta>
            <ta e="T934" id="Seg_10945" s="T933">pro.h:S</ta>
            <ta e="T937" id="Seg_10946" s="T936">v:pred</ta>
            <ta e="T940" id="Seg_10947" s="T939">np:O</ta>
            <ta e="T941" id="Seg_10948" s="T940">v:pred 0.3.h:S</ta>
            <ta e="T944" id="Seg_10949" s="T943">v:pred 0.3.h:S</ta>
            <ta e="T949" id="Seg_10950" s="T948">np.h:S</ta>
            <ta e="T951" id="Seg_10951" s="T950">v:pred</ta>
            <ta e="T953" id="Seg_10952" s="T952">np:S</ta>
            <ta e="T956" id="Seg_10953" s="T955">v:pred</ta>
            <ta e="T959" id="Seg_10954" s="T958">v:pred</ta>
            <ta e="T962" id="Seg_10955" s="T961">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_10956" s="T3">RUS:cult</ta>
            <ta e="T8" id="Seg_10957" s="T7">TURK:disc</ta>
            <ta e="T24" id="Seg_10958" s="T23">RUS:gram</ta>
            <ta e="T26" id="Seg_10959" s="T25">RUS:gram</ta>
            <ta e="T28" id="Seg_10960" s="T27">TURK:gram(INDEF)</ta>
            <ta e="T38" id="Seg_10961" s="T37">TURK:gram(INDEF)</ta>
            <ta e="T65" id="Seg_10962" s="T64">RUS:gram</ta>
            <ta e="T67" id="Seg_10963" s="T66">RUS:mod</ta>
            <ta e="T75" id="Seg_10964" s="T74">RUS:cult</ta>
            <ta e="T80" id="Seg_10965" s="T79">RUS:cult</ta>
            <ta e="T93" id="Seg_10966" s="T92">RUS:gram</ta>
            <ta e="T96" id="Seg_10967" s="T95">RUS:core</ta>
            <ta e="T97" id="Seg_10968" s="T96">RUS:core</ta>
            <ta e="T108" id="Seg_10969" s="T107">RUS:gram</ta>
            <ta e="T111" id="Seg_10970" s="T110">RUS:cult</ta>
            <ta e="T114" id="Seg_10971" s="T113">RUS:gram</ta>
            <ta e="T125" id="Seg_10972" s="T124">RUS:disc</ta>
            <ta e="T128" id="Seg_10973" s="T127">RUS:cult</ta>
            <ta e="T131" id="Seg_10974" s="T130">RUS:cult</ta>
            <ta e="T133" id="Seg_10975" s="T132">RUS:gram</ta>
            <ta e="T134" id="Seg_10976" s="T133">RUS:cult</ta>
            <ta e="T137" id="Seg_10977" s="T136">RUS:cult</ta>
            <ta e="T142" id="Seg_10978" s="T141">RUS:cult</ta>
            <ta e="T143" id="Seg_10979" s="T142">RUS:mod</ta>
            <ta e="T144" id="Seg_10980" s="T143">RUS:cult</ta>
            <ta e="T150" id="Seg_10981" s="T149">RUS:cult</ta>
            <ta e="T152" id="Seg_10982" s="T151">RUS:gram</ta>
            <ta e="T159" id="Seg_10983" s="T158">RUS:core</ta>
            <ta e="T163" id="Seg_10984" s="T162">TURK:cult</ta>
            <ta e="T183" id="Seg_10985" s="T182">RUS:cult</ta>
            <ta e="T185" id="Seg_10986" s="T184">RUS:gram</ta>
            <ta e="T186" id="Seg_10987" s="T185">TURK:core</ta>
            <ta e="T190" id="Seg_10988" s="T189">RUS:cult</ta>
            <ta e="T191" id="Seg_10989" s="T190">RUS:gram</ta>
            <ta e="T194" id="Seg_10990" s="T193">TURK:disc</ta>
            <ta e="T200" id="Seg_10991" s="T199">TAT:cult</ta>
            <ta e="T202" id="Seg_10992" s="T201">RUS:gram</ta>
            <ta e="T208" id="Seg_10993" s="T207">RUS:cult</ta>
            <ta e="T212" id="Seg_10994" s="T211">TURK:disc</ta>
            <ta e="T224" id="Seg_10995" s="T223">TURK:core</ta>
            <ta e="T227" id="Seg_10996" s="T226">RUS:gram</ta>
            <ta e="T235" id="Seg_10997" s="T234">TURK:core</ta>
            <ta e="T243" id="Seg_10998" s="T242">RUS:gram</ta>
            <ta e="T246" id="Seg_10999" s="T245">RUS:gram</ta>
            <ta e="T262" id="Seg_11000" s="T261">RUS:gram</ta>
            <ta e="T265" id="Seg_11001" s="T264">TURK:core</ta>
            <ta e="T269" id="Seg_11002" s="T268">TURK:disc</ta>
            <ta e="T272" id="Seg_11003" s="T271">TURK:cult</ta>
            <ta e="T275" id="Seg_11004" s="T274">RUS:gram</ta>
            <ta e="T279" id="Seg_11005" s="T278">TURK:disc</ta>
            <ta e="T286" id="Seg_11006" s="T285">RUS:gram</ta>
            <ta e="T301" id="Seg_11007" s="T300">RUS:gram</ta>
            <ta e="T303" id="Seg_11008" s="T302">RUS:gram</ta>
            <ta e="T305" id="Seg_11009" s="T304">RUS:cult</ta>
            <ta e="T311" id="Seg_11010" s="T310">RUS:gram</ta>
            <ta e="T313" id="Seg_11011" s="T312">TURK:core</ta>
            <ta e="T331" id="Seg_11012" s="T330">RUS:gram</ta>
            <ta e="T334" id="Seg_11013" s="T333">TURK:core</ta>
            <ta e="T338" id="Seg_11014" s="T337">RUS:gram</ta>
            <ta e="T352" id="Seg_11015" s="T351">TAT:cult</ta>
            <ta e="T354" id="Seg_11016" s="T353">RUS:cult</ta>
            <ta e="T368" id="Seg_11017" s="T367">TURK:disc</ta>
            <ta e="T371" id="Seg_11018" s="T370">TAT:cult</ta>
            <ta e="T372" id="Seg_11019" s="T371">TAT:cult</ta>
            <ta e="T380" id="Seg_11020" s="T379">TAT:cult</ta>
            <ta e="T385" id="Seg_11021" s="T384">RUS:gram</ta>
            <ta e="T396" id="Seg_11022" s="T395">RUS:gram</ta>
            <ta e="T398" id="Seg_11023" s="T397">TURK:disc</ta>
            <ta e="T400" id="Seg_11024" s="T399">TAT:cult</ta>
            <ta e="T403" id="Seg_11025" s="T402">TURK:disc</ta>
            <ta e="T408" id="Seg_11026" s="T407">RUS:gram</ta>
            <ta e="T421" id="Seg_11027" s="T420">RUS:gram</ta>
            <ta e="T436" id="Seg_11028" s="T435">RUS:mod</ta>
            <ta e="T445" id="Seg_11029" s="T444">RUS:cult</ta>
            <ta e="T448" id="Seg_11030" s="T447">RUS:gram</ta>
            <ta e="T456" id="Seg_11031" s="T455">TURK:disc</ta>
            <ta e="T460" id="Seg_11032" s="T459">RUS:gram</ta>
            <ta e="T464" id="Seg_11033" s="T463">RUS:gram</ta>
            <ta e="T466" id="Seg_11034" s="T465">RUS:gram</ta>
            <ta e="T468" id="Seg_11035" s="T467">TURK:gram(INDEF)</ta>
            <ta e="T476" id="Seg_11036" s="T475">RUS:gram</ta>
            <ta e="T482" id="Seg_11037" s="T481">RUS:cult</ta>
            <ta e="T483" id="Seg_11038" s="T482">%TURK:core</ta>
            <ta e="T484" id="Seg_11039" s="T483">%TURK:core</ta>
            <ta e="T490" id="Seg_11040" s="T489">TURK:cult</ta>
            <ta e="T494" id="Seg_11041" s="T493">RUS:gram</ta>
            <ta e="T498" id="Seg_11042" s="T497">RUS:disc</ta>
            <ta e="T520" id="Seg_11043" s="T519">TURK:gram(INDEF)</ta>
            <ta e="T541" id="Seg_11044" s="T540">TURK:gram(INDEF)</ta>
            <ta e="T555" id="Seg_11045" s="T554">TURK:cult</ta>
            <ta e="T560" id="Seg_11046" s="T559">RUS:cult</ta>
            <ta e="T569" id="Seg_11047" s="T568">TURK:core</ta>
            <ta e="T574" id="Seg_11048" s="T573">TURK:gram(INDEF)</ta>
            <ta e="T576" id="Seg_11049" s="T575">TURK:core</ta>
            <ta e="T579" id="Seg_11050" s="T578">TURK:gram(INDEF)</ta>
            <ta e="T584" id="Seg_11051" s="T583">TURK:core</ta>
            <ta e="T593" id="Seg_11052" s="T592">TURK:disc</ta>
            <ta e="T599" id="Seg_11053" s="T598">RUS:gram</ta>
            <ta e="T604" id="Seg_11054" s="T603">TURK:disc</ta>
            <ta e="T608" id="Seg_11055" s="T607">RUS:core</ta>
            <ta e="T611" id="Seg_11056" s="T610">RUS:gram</ta>
            <ta e="T613" id="Seg_11057" s="T612">RUS:core</ta>
            <ta e="T629" id="Seg_11058" s="T628">TURK:gram(INDEF)</ta>
            <ta e="T633" id="Seg_11059" s="T632">RUS:gram</ta>
            <ta e="T636" id="Seg_11060" s="T635">RUS:cult</ta>
            <ta e="T645" id="Seg_11061" s="T644">TURK:core</ta>
            <ta e="T653" id="Seg_11062" s="T652">TURK:cult</ta>
            <ta e="T655" id="Seg_11063" s="T654">TURK:cult</ta>
            <ta e="T664" id="Seg_11064" s="T663">TURK:disc</ta>
            <ta e="T669" id="Seg_11065" s="T668">TURK:disc</ta>
            <ta e="T686" id="Seg_11066" s="T685">TURK:disc</ta>
            <ta e="T692" id="Seg_11067" s="T691">RUS:cult</ta>
            <ta e="T698" id="Seg_11068" s="T697">RUS:cult</ta>
            <ta e="T719" id="Seg_11069" s="T718">RUS:core</ta>
            <ta e="T726" id="Seg_11070" s="T725">RUS:gram</ta>
            <ta e="T735" id="Seg_11071" s="T734">TURK:disc</ta>
            <ta e="T741" id="Seg_11072" s="T740">RUS:cult</ta>
            <ta e="T744" id="Seg_11073" s="T743">RUS:gram</ta>
            <ta e="T745" id="Seg_11074" s="T744">TURK:core</ta>
            <ta e="T750" id="Seg_11075" s="T749">RUS:gram(INDEF)</ta>
            <ta e="T755" id="Seg_11076" s="T754">RUS:gram</ta>
            <ta e="T760" id="Seg_11077" s="T759">RUS:disc</ta>
            <ta e="T763" id="Seg_11078" s="T762">TURK:cult</ta>
            <ta e="T769" id="Seg_11079" s="T768">RUS:cult</ta>
            <ta e="T773" id="Seg_11080" s="T772">TURK:disc</ta>
            <ta e="T784" id="Seg_11081" s="T783">TURK:cult</ta>
            <ta e="T799" id="Seg_11082" s="T798">TAT:cult</ta>
            <ta e="T805" id="Seg_11083" s="T804">TAT:cult</ta>
            <ta e="T811" id="Seg_11084" s="T810">TAT:cult</ta>
            <ta e="T824" id="Seg_11085" s="T823">TURK:disc</ta>
            <ta e="T834" id="Seg_11086" s="T833">TURK:gram(INDEF)</ta>
            <ta e="T847" id="Seg_11087" s="T846">RUS:mod</ta>
            <ta e="T856" id="Seg_11088" s="T855">TURK:gram(INDEF)</ta>
            <ta e="T858" id="Seg_11089" s="T857">TURK:cult</ta>
            <ta e="T859" id="Seg_11090" s="T858">TURK:cult</ta>
            <ta e="T864" id="Seg_11091" s="T863">RUS:mod</ta>
            <ta e="T866" id="Seg_11092" s="T865">RUS:gram</ta>
            <ta e="T891" id="Seg_11093" s="T890">RUS:gram</ta>
            <ta e="T898" id="Seg_11094" s="T897">TURK:gram(INDEF)</ta>
            <ta e="T899" id="Seg_11095" s="T898">TURK:disc</ta>
            <ta e="T902" id="Seg_11096" s="T901">TURK:disc</ta>
            <ta e="T911" id="Seg_11097" s="T910">TURK:disc</ta>
            <ta e="T914" id="Seg_11098" s="T913">RUS:gram</ta>
            <ta e="T923" id="Seg_11099" s="T922">RUS:gram</ta>
            <ta e="T925" id="Seg_11100" s="T924">TURK:disc</ta>
            <ta e="T928" id="Seg_11101" s="T927">RUS:mod</ta>
            <ta e="T930" id="Seg_11102" s="T929">RUS:gram</ta>
            <ta e="T935" id="Seg_11103" s="T934">RUS:mod</ta>
            <ta e="T939" id="Seg_11104" s="T938">RUS:gram</ta>
            <ta e="T942" id="Seg_11105" s="T941">RUS:gram</ta>
            <ta e="T944" id="Seg_11106" s="T943">tragen</ta>
            <ta e="T948" id="Seg_11107" s="T947">%TURK:core</ta>
            <ta e="T950" id="Seg_11108" s="T949">TURK:disc</ta>
            <ta e="T954" id="Seg_11109" s="T953">RUS:mod</ta>
            <ta e="T955" id="Seg_11110" s="T954">TURK:disc</ta>
            <ta e="T958" id="Seg_11111" s="T957">RUS:mod</ta>
            <ta e="T961" id="Seg_11112" s="T960">TURK:gram(INDEF)</ta>
            <ta e="T962" id="Seg_11113" s="T961">RUS:mod</ta>
            <ta e="T963" id="Seg_11114" s="T962">%TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T10" id="Seg_11115" s="T9">RUS:int</ta>
            <ta e="T44" id="Seg_11116" s="T41">RUS:int</ta>
            <ta e="T59" id="Seg_11117" s="T56">RUS:int</ta>
            <ta e="T64" id="Seg_11118" s="T63">RUS:int</ta>
            <ta e="T660" id="Seg_11119" s="T659">RUS:int</ta>
            <ta e="T678" id="Seg_11120" s="T677">RUS:int</ta>
            <ta e="T737" id="Seg_11121" s="T736">RUS:int</ta>
            <ta e="T832" id="Seg_11122" s="T831">RUS:int</ta>
            <ta e="T849" id="Seg_11123" s="T848">RUS:int</ta>
            <ta e="T852" id="Seg_11124" s="T850">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_11125" s="T1">Этот твой аппарат…</ta>
            <ta e="T8" id="Seg_11126" s="T6">Он устал.</ta>
            <ta e="T11" id="Seg_11127" s="T9">Начал врать [=неправильно работать].</ta>
            <ta e="T15" id="Seg_11128" s="T12">Очень ленивый стал.</ta>
            <ta e="T23" id="Seg_11129" s="T16">Ты очень (сильный?) человек, я тебе говорю, говорю.</ta>
            <ta e="T30" id="Seg_11130" s="T23">А ты ничего не знаешь.</ta>
            <ta e="T36" id="Seg_11131" s="T31">Я тебе говорю: я была маленькая.</ta>
            <ta e="T40" id="Seg_11132" s="T37">Какие-то люди пришли.</ta>
            <ta e="T46" id="Seg_11133" s="T41">То ли экспедиция, не знаю.</ta>
            <ta e="T50" id="Seg_11134" s="T46">Я маленькая была, не спрашивала.</ta>
            <ta e="T54" id="Seg_11135" s="T51">Мне не говорили.</ta>
            <ta e="T59" id="Seg_11136" s="T55">У Власа, Иван Андреевич. [?]</ta>
            <ta e="T69" id="Seg_11137" s="T60">У них был бинокль и ещё это, не знаю что.</ta>
            <ta e="T72" id="Seg_11138" s="T69">Они мне показали иголку.</ta>
            <ta e="T75" id="Seg_11139" s="T73">В стену воткнули.</ta>
            <ta e="T78" id="Seg_11140" s="T76">Так далеко.</ta>
            <ta e="T82" id="Seg_11141" s="T79">Саженей двадцать.</ta>
            <ta e="T91" id="Seg_11142" s="T83">Потом я посмотрела [в бинокль]: (иголка?) очень большая.</ta>
            <ta e="T97" id="Seg_11143" s="T92">А потом перевернули другим концом.</ta>
            <ta e="T102" id="Seg_11144" s="T98">Я посмотрела: очень далеко стало.</ta>
            <ta e="T106" id="Seg_11145" s="T103">Потом положили бумагу.</ta>
            <ta e="T112" id="Seg_11146" s="T107">И наставили этот бинокль на солнце.</ta>
            <ta e="T116" id="Seg_11147" s="T113">И бумага загорелась.</ta>
            <ta e="T120" id="Seg_11148" s="T117">Вы когда пойдёте?</ta>
            <ta e="T123" id="Seg_11149" s="T121">Мы пойдём (?).</ta>
            <ta e="T126" id="Seg_11150" s="T124">Ну, идите!</ta>
            <ta e="T131" id="Seg_11151" s="T127">Привет передайте Матвееву.</ta>
            <ta e="T137" id="Seg_11152" s="T132">Эля сегодня пошла в Малиновку.</ta>
            <ta e="T140" id="Seg_11153" s="T138">Бумагу понесла.</ta>
            <ta e="T145" id="Seg_11154" s="T141">У председателя надо печати поставить.</ta>
            <ta e="T150" id="Seg_11155" s="T146">Она на машине поехала.</ta>
            <ta e="T156" id="Seg_11156" s="T151"> А оттуда пешком вернётся.</ta>
            <ta e="T165" id="Seg_11157" s="T157">У моей сестры сын пошёл (?) коровам (?) принести.</ta>
            <ta e="T169" id="Seg_11158" s="T166">Своего сына взял.</ta>
            <ta e="T174" id="Seg_11159" s="T170">Скоро они домой вернулись.</ta>
            <ta e="T181" id="Seg_11160" s="T175">Один человек хотел построить дом.</ta>
            <ta e="T183" id="Seg_11161" s="T182">Из золота.</ta>
            <ta e="T190" id="Seg_11162" s="T184">А другой человек [хотел] сделать дом из серебра.</ta>
            <ta e="T200" id="Seg_11163" s="T190">А третий человек говорит: из дерева срублю дом.</ta>
            <ta e="T208" id="Seg_11164" s="T201">А четвёртый человек: "Я сделаю из травы, из соломы."</ta>
            <ta e="T213" id="Seg_11165" s="T209">Он принялся за работу. [?]</ta>
            <ta e="T220" id="Seg_11166" s="T214">Дом того, который работал, не сгорит. [?]</ta>
            <ta e="T225" id="Seg_11167" s="T221">Ему хорошо будет.</ta>
            <ta e="T231" id="Seg_11168" s="T226">А [дом] того, который работает, сгорит. [?]</ta>
            <ta e="T236" id="Seg_11169" s="T232">Ему плохо будет.</ta>
            <ta e="T240" id="Seg_11170" s="T237">Сам он не сгорит.</ta>
            <ta e="T241" id="Seg_11171" s="T240">Он убежит.</ta>
            <ta e="T249" id="Seg_11172" s="T242">Но то, что он сделал, сгорит.</ta>
            <ta e="T254" id="Seg_11173" s="T250">Жили три свиньи.</ta>
            <ta e="T261" id="Seg_11174" s="T255">Две свиньи были очень ленивые.</ta>
            <ta e="T266" id="Seg_11175" s="T261">А одна свинья всегда работала.</ta>
            <ta e="T273" id="Seg_11176" s="T267">Они всё ходили в гости, пили водку.</ta>
            <ta e="T281" id="Seg_11177" s="T274">А та одна свинья всё пахала землю.</ta>
            <ta e="T284" id="Seg_11178" s="T282">Они собирали камни.</ta>
            <ta e="T291" id="Seg_11179" s="T285">И построил(и) себе дом из камней.</ta>
            <ta e="T297" id="Seg_11180" s="T292">Они пришли, один сделал [дом?].</ta>
            <ta e="T302" id="Seg_11181" s="T298">Он собрал веток и сложил.</ta>
            <ta e="T309" id="Seg_11182" s="T302"> А один из соломы сложил себе дом.</ta>
            <ta e="T316" id="Seg_11183" s="T310">А этот всё камень за камнем кладёт.</ta>
            <ta e="T322" id="Seg_11184" s="T316">Они пришли: "Ты что работаешь?</ta>
            <ta e="T329" id="Seg_11185" s="T323">Мы быстро построили дома".</ta>
            <ta e="T339" id="Seg_11186" s="T330">"А чей дом будет лучше, мой или ваш?"</ta>
            <ta e="T343" id="Seg_11187" s="T340">Потом стало холодно.</ta>
            <ta e="T346" id="Seg_11188" s="T343">Зима наступает.</ta>
            <ta e="T354" id="Seg_11189" s="T347">Тогда они забрались в дом, который из соломы.</ta>
            <ta e="T359" id="Seg_11190" s="T355">Закрыли дверь, пришёл медведь.</ta>
            <ta e="T362" id="Seg_11191" s="T360">"Впустите меня!"</ta>
            <ta e="T366" id="Seg_11192" s="T363">Они не пускают.</ta>
            <ta e="T372" id="Seg_11193" s="T366">"Сейчас я ваш дом сломаю!"</ta>
            <ta e="T377" id="Seg_11194" s="T373">Он сломал его, они убежали.</ta>
            <ta e="T380" id="Seg_11195" s="T378">Какой дом.</ta>
            <ta e="T383" id="Seg_11196" s="T381">[Который] из веток сделали.</ta>
            <ta e="T389" id="Seg_11197" s="T384">И там дверь закрыли.</ta>
            <ta e="T392" id="Seg_11198" s="T389">Он туда пришёл.</ta>
            <ta e="T394" id="Seg_11199" s="T393">"Открывайте!</ta>
            <ta e="T400" id="Seg_11200" s="T395">А не то дом сейчас упадёт [=развалится]".</ta>
            <ta e="T406" id="Seg_11201" s="T401">Потом он его сломал, они убежали.</ta>
            <ta e="T411" id="Seg_11202" s="T407">И пришли сюда к другому.</ta>
            <ta e="T415" id="Seg_11203" s="T412">Они пришли к третьей свинье.</ta>
            <ta e="T418" id="Seg_11204" s="T416">"Впусти нас!"</ta>
            <ta e="T424" id="Seg_11205" s="T418">Он впустил их и закрыл дверь.</ta>
            <ta e="T427" id="Seg_11206" s="T424">Он пришёл, медведь.</ta>
            <ta e="T428" id="Seg_11207" s="T427">"Впустите меня!"</ta>
            <ta e="T431" id="Seg_11208" s="T429">"Не пущу!"</ta>
            <ta e="T436" id="Seg_11209" s="T432">Тогда он хотел сломать [дом].</ta>
            <ta e="T438" id="Seg_11210" s="T436">Не смог.</ta>
            <ta e="T445" id="Seg_11211" s="T438">Тогда он залез на трубу.</ta>
            <ta e="T447" id="Seg_11212" s="T446">Лезет.</ta>
            <ta e="T453" id="Seg_11213" s="T447">А [там] стоял котёл с горячей водой.</ta>
            <ta e="T458" id="Seg_11214" s="T454">Он упал в котёл.</ta>
            <ta e="T470" id="Seg_11215" s="T459">И потом (повернулся?) и ушёл и больше никогда не приходил.</ta>
            <ta e="T474" id="Seg_11216" s="T471">Они его обожгли.</ta>
            <ta e="T477" id="Seg_11217" s="T475">И он ушёл.</ta>
            <ta e="T484" id="Seg_11218" s="T478">Сколько мы плёнок рассказали?</ta>
            <ta e="T487" id="Seg_11219" s="T485">Тринадцать.</ta>
            <ta e="T491" id="Seg_11220" s="T488">Вот ваш хлеб.</ta>
            <ta e="T496" id="Seg_11221" s="T492">Вот он, а где вы его взяли?</ta>
            <ta e="T501" id="Seg_11222" s="T497">Да одна женщина дала.</ta>
            <ta e="T504" id="Seg_11223" s="T502">Из Вознесенского. [?]</ta>
            <ta e="T510" id="Seg_11224" s="T505">У одной козы было пять детей.</ta>
            <ta e="T515" id="Seg_11225" s="T511">Она оставила их дома.</ta>
            <ta e="T518" id="Seg_11226" s="T516">Закрыла дверь.</ta>
            <ta e="T524" id="Seg_11227" s="T519">"Никого не впускайте".</ta>
            <ta e="T527" id="Seg_11228" s="T525">Потом ушла.</ta>
            <ta e="T530" id="Seg_11229" s="T528">Пришёл медведь.</ta>
            <ta e="T532" id="Seg_11230" s="T530">"Откройте дверь!"</ta>
            <ta e="T536" id="Seg_11231" s="T533">Они не открыли.</ta>
            <ta e="T543" id="Seg_11232" s="T537">"Нам мама сказала никого не пускать."</ta>
            <ta e="T549" id="Seg_11233" s="T544">Тогда медведь ушёл, мать пришла.</ta>
            <ta e="T555" id="Seg_11234" s="T550">"Откройте дверь, я молока принесла.</ta>
            <ta e="T559" id="Seg_11235" s="T556">Травы вам принесла.</ta>
            <ta e="T561" id="Seg_11236" s="T559">Творога принесла".</ta>
            <ta e="T566" id="Seg_11237" s="T562">Потом они открыли дверь, поели.</ta>
            <ta e="T570" id="Seg_11238" s="T566">Потом их мать опять ушла.</ta>
            <ta e="T574" id="Seg_11239" s="T571">"Не пускайте никого!"</ta>
            <ta e="T577" id="Seg_11240" s="T575">Снова ушла.</ta>
            <ta e="T581" id="Seg_11241" s="T578">"Никого не пускайте!"</ta>
            <ta e="T586" id="Seg_11242" s="T582">Тогда опять медведь пришёл.</ta>
            <ta e="T591" id="Seg_11243" s="T587">"Дети, дети, пустите меня!"</ta>
            <ta e="T597" id="Seg_11244" s="T592">"Нет, мы не пустим".</ta>
            <ta e="T603" id="Seg_11245" s="T598">"А я ваша мать!"</ta>
            <ta e="T609" id="Seg_11246" s="T603">"Нет, у моей [=нашей] матери голос тонкий!</ta>
            <ta e="T615" id="Seg_11247" s="T610">А твой голос толстый [=грубый]".</ta>
            <ta e="T619" id="Seg_11248" s="T616">Тогда медведь ушёл.</ta>
            <ta e="T622" id="Seg_11249" s="T619">Потом мать пришла.</ta>
            <ta e="T626" id="Seg_11250" s="T623">Они открыли, она их накормила. </ta>
            <ta e="T631" id="Seg_11251" s="T627">"Теперь никого не пускайте!"</ta>
            <ta e="T636" id="Seg_11252" s="T632">А медведь пошёл на кузницу.</ta>
            <ta e="T641" id="Seg_11253" s="T637">Сделал себе тонкий язык [=голос].</ta>
            <ta e="T645" id="Seg_11254" s="T642">Потом пришёл опять.</ta>
            <ta e="T653" id="Seg_11255" s="T646">Он сказал: "Я ваша мать, я принесла молока.</ta>
            <ta e="T656" id="Seg_11256" s="T654">Хлеба принесла.</ta>
            <ta e="T661" id="Seg_11257" s="T657">Творога принесла, травы".</ta>
            <ta e="T665" id="Seg_11258" s="T662">Они открыли.</ta>
            <ta e="T670" id="Seg_11259" s="T666">Он их всех съел.</ta>
            <ta e="T673" id="Seg_11260" s="T671">Мать пришла.</ta>
            <ta e="T676" id="Seg_11261" s="T674">Её детей нет.</ta>
            <ta e="T681" id="Seg_11262" s="T676">Один, самый маленький, спрятался.</ta>
            <ta e="T687" id="Seg_11263" s="T682">Потом сказал: "Медведь всех съел.</ta>
            <ta e="T691" id="Seg_11264" s="T688">Я один остался.</ta>
            <ta e="T704" id="Seg_11265" s="T691">Я залез в печку, он меня не увидел и ушёл".</ta>
            <ta e="T709" id="Seg_11266" s="T705">Потом медведь пришёл к козе.</ta>
            <ta e="T715" id="Seg_11267" s="T710">Коза сказала: "Пойдём.</ta>
            <ta e="T720" id="Seg_11268" s="T716">Давай через эту яму прыгать.</ta>
            <ta e="T723" id="Seg_11269" s="T721">Коза прыгнула.</ta>
            <ta e="T731" id="Seg_11270" s="T724">А он не смог туда прыгнуть.</ta>
            <ta e="T737" id="Seg_11271" s="T732">Он порвал [шкуру?], [упал] на что-то и пропорол живот.</ta>
            <ta e="T747" id="Seg_11272" s="T737">Потом козлята выскочили и вернулись к матери.</ta>
            <ta e="T753" id="Seg_11273" s="T747">"Пойдём куда-нибудь с тобой в гости".</ta>
            <ta e="T758" id="Seg_11274" s="T754">"А что там будет?"</ta>
            <ta e="T764" id="Seg_11275" s="T759">"Да что будет, водку будем пить.</ta>
            <ta e="T766" id="Seg_11276" s="T764">Песни будем петь.</ta>
            <ta e="T768" id="Seg_11277" s="T767">Прыгать будем [=танцевать].</ta>
            <ta e="T770" id="Seg_11278" s="T768">Мы играли на гармони.</ta>
            <ta e="T779" id="Seg_11279" s="T771">Там всего [будет] много — мяса много, (?), масла.</ta>
            <ta e="T782" id="Seg_11280" s="T780">Варёной рыбы.</ta>
            <ta e="T784" id="Seg_11281" s="T783"> Хлеб.</ta>
            <ta e="T787" id="Seg_11282" s="T785">Сахар стоит [на столе].</ta>
            <ta e="T796" id="Seg_11283" s="T788">Черёмуха, сушёная ягода, чёрная ягода, пойдём!"</ta>
            <ta e="T800" id="Seg_11284" s="T797">Ты (?) дома?</ta>
            <ta e="T802" id="Seg_11285" s="T801">[Да,] (?).</ta>
            <ta e="T806" id="Seg_11286" s="T803">Сколько домов ты (?)?</ta>
            <ta e="T808" id="Seg_11287" s="T807">Семь.</ta>
            <ta e="T812" id="Seg_11288" s="T809">[?]</ta>
            <ta e="T818" id="Seg_11289" s="T813">Ты в лес не ходил?</ta>
            <ta e="T820" id="Seg_11290" s="T818">Не ходил.</ta>
            <ta e="T827" id="Seg_11291" s="T820">Я испугался, там ведь медведь живёт (?).</ta>
            <ta e="T832" id="Seg_11292" s="T828">Чего ты боишься, иди (ты)!</ta>
            <ta e="T839" id="Seg_11293" s="T833">Он тебе ничего не сделает.</ta>
            <ta e="T844" id="Seg_11294" s="T840">Ты (спрячешься?), а потом придёшь домой.</ta>
            <ta e="T847" id="Seg_11295" s="T845">Ты можешь (?).</ta>
            <ta e="T849" id="Seg_11296" s="T848">(…) говорю.</ta>
            <ta e="T854" id="Seg_11297" s="T850">"Я всё равно боюсь.</ta>
            <ta e="T862" id="Seg_11298" s="T854">У меня ничего нет, ни ружья, ни ножа.</ta>
            <ta e="T865" id="Seg_11299" s="T863">Только камень."</ta>
            <ta e="T869" id="Seg_11300" s="T865">"А (?) не будет.</ta>
            <ta e="T874" id="Seg_11301" s="T870">Он человека боится, убежит".</ta>
            <ta e="T882" id="Seg_11302" s="T875">Мы пошли в этот лес.</ta>
            <ta e="T886" id="Seg_11303" s="T883">Потом пошёл снег.</ta>
            <ta e="T894" id="Seg_11304" s="T887">Они втроём пошли, а я назад вернулась.</ta>
            <ta e="T903" id="Seg_11305" s="T895">Потом слышу: кто-то ветки ломает.</ta>
            <ta e="T913" id="Seg_11306" s="T904">Я думаю: медведь (?).</ta>
            <ta e="T919" id="Seg_11307" s="T913">Но я его не видела, домой пришла.</ta>
            <ta e="T925" id="Seg_11308" s="T920">Я шла и боялась.</ta>
            <ta e="T929" id="Seg_11309" s="T926">Надо залезть на дерево.</ta>
            <ta e="T937" id="Seg_11310" s="T929">А я ему говорю: "Он тоже на деревья залезает".</ta>
            <ta e="T944" id="Seg_11311" s="T938">Он сломает дерево и на землю повалит.</ta>
            <ta e="T951" id="Seg_11312" s="T945">Как тут разговаривать, дети кричат.</ta>
            <ta e="T956" id="Seg_11313" s="T952">Свиньи тоже хрюкают.</ta>
            <ta e="T959" id="Seg_11314" s="T956">(Коровы?) тоже мычат.</ta>
            <ta e="T963" id="Seg_11315" s="T960">Невозможно разговаривать.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_11316" s="T1">Your camera…</ta>
            <ta e="T8" id="Seg_11317" s="T6">It got tired.</ta>
            <ta e="T11" id="Seg_11318" s="T9">It began to lie [= work incorrectly].</ta>
            <ta e="T15" id="Seg_11319" s="T12">It became very lazy.</ta>
            <ta e="T23" id="Seg_11320" s="T16">You are a very (strong?) man, I tell you again and again.</ta>
            <ta e="T30" id="Seg_11321" s="T23">But you don't know anything.</ta>
            <ta e="T36" id="Seg_11322" s="T31">I said you: I was little.</ta>
            <ta e="T40" id="Seg_11323" s="T37">Some people came.</ta>
            <ta e="T46" id="Seg_11324" s="T41">Maybe an expedition, I don't know.</ta>
            <ta e="T50" id="Seg_11325" s="T46">I was little, I didn't ask.</ta>
            <ta e="T54" id="Seg_11326" s="T51">They didn't tell me.</ta>
            <ta e="T59" id="Seg_11327" s="T55">At Vlas', Ivan Andreevich. [?]</ta>
            <ta e="T69" id="Seg_11328" s="T60">They had binoculars and something, I don't know, too.</ta>
            <ta e="T72" id="Seg_11329" s="T69">They showed me a needle.</ta>
            <ta e="T75" id="Seg_11330" s="T73">They stuck it into the wall.</ta>
            <ta e="T78" id="Seg_11331" s="T76">So far.</ta>
            <ta e="T82" id="Seg_11332" s="T79">About twenty sazhen away.</ta>
            <ta e="T91" id="Seg_11333" s="T83">Then I looked [through the binoculars]: the (needle?) is very big.</ta>
            <ta e="T97" id="Seg_11334" s="T92">Then they turned it other end up.</ta>
            <ta e="T102" id="Seg_11335" s="T98">I saw: it became very far.</ta>
            <ta e="T106" id="Seg_11336" s="T103">Then they put [a sheet of] paper.</ta>
            <ta e="T112" id="Seg_11337" s="T107">And turned the binoculars towards the sun.</ta>
            <ta e="T116" id="Seg_11338" s="T113">And the paper took fire.</ta>
            <ta e="T120" id="Seg_11339" s="T117">When will you go?</ta>
            <ta e="T123" id="Seg_11340" s="T121">We'll go to (?).</ta>
            <ta e="T126" id="Seg_11341" s="T124">Well, go!</ta>
            <ta e="T131" id="Seg_11342" s="T127">Give my greetings to Matveev.</ta>
            <ta e="T137" id="Seg_11343" s="T132">Elya went yesterday to Malinovka.</ta>
            <ta e="T140" id="Seg_11344" s="T138">She took a paper.</ta>
            <ta e="T145" id="Seg_11345" s="T141">She had to stamp [the document] by the head [of the kolkhoz?].</ta>
            <ta e="T150" id="Seg_11346" s="T146">She went there with cars.</ta>
            <ta e="T156" id="Seg_11347" s="T151">And from there she'll come back by foot.</ta>
            <ta e="T165" id="Seg_11348" s="T157">My sister's son went (?) to give the cow (?).</ta>
            <ta e="T169" id="Seg_11349" s="T166">He took his own son.</ta>
            <ta e="T174" id="Seg_11350" s="T170">Soon they came back home.</ta>
            <ta e="T181" id="Seg_11351" s="T175">One person thought [= wanted] to make a tent.</ta>
            <ta e="T183" id="Seg_11352" s="T182">With gold.</ta>
            <ta e="T190" id="Seg_11353" s="T184">And another man [wanted] to make a house from silver.</ta>
            <ta e="T200" id="Seg_11354" s="T190">And the third man (?) says: I'll make a house from wood.</ta>
            <ta e="T208" id="Seg_11355" s="T201">And the fourth man: "I'll make [it] from the grass, from straw."</ta>
            <ta e="T213" id="Seg_11356" s="T209">He'll came to work. [?]</ta>
            <ta e="T220" id="Seg_11357" s="T214">The house of that who was working will not burn (?).</ta>
            <ta e="T225" id="Seg_11358" s="T221">He will be well.</ta>
            <ta e="T231" id="Seg_11359" s="T226">And [the house] of who is working will burn. [?]</ta>
            <ta e="T236" id="Seg_11360" s="T232">He won't be well.</ta>
            <ta e="T240" id="Seg_11361" s="T237">Himself will not burn.</ta>
            <ta e="T241" id="Seg_11362" s="T240">He'll escape.</ta>
            <ta e="T249" id="Seg_11363" s="T242">But what he has made, will burn.</ta>
            <ta e="T254" id="Seg_11364" s="T250">There lived three pigs.</ta>
            <ta e="T261" id="Seg_11365" s="T255">Two pigs were very lazy.</ta>
            <ta e="T266" id="Seg_11366" s="T261">And one pig was always working.</ta>
            <ta e="T273" id="Seg_11367" s="T267">They were going to visits, drank vodka.</ta>
            <ta e="T281" id="Seg_11368" s="T274">And that one pig ploughed earth.</ta>
            <ta e="T284" id="Seg_11369" s="T282">They gathered stones.</ta>
            <ta e="T291" id="Seg_11370" s="T285">And they built a house from stone for themselves.</ta>
            <ta e="T297" id="Seg_11371" s="T292">They came, one made [a house?]</ta>
            <ta e="T302" id="Seg_11372" s="T298">He collected twigs and made [it].</ta>
            <ta e="T309" id="Seg_11373" s="T302">And one made a house from straw for himself .</ta>
            <ta e="T316" id="Seg_11374" s="T310">And that one is always putting one stone after another.</ta>
            <ta e="T322" id="Seg_11375" s="T316">They came: "Why are you working?</ta>
            <ta e="T329" id="Seg_11376" s="T323">We built [our] house[s] fast."</ta>
            <ta e="T339" id="Seg_11377" s="T330">"But whose house will be better, mine or yours?"</ta>
            <ta e="T343" id="Seg_11378" s="T340">Then it became cold.</ta>
            <ta e="T346" id="Seg_11379" s="T343">Winter is coming.</ta>
            <ta e="T354" id="Seg_11380" s="T347">Then they came in the house that [was made from] straw.</ta>
            <ta e="T359" id="Seg_11381" s="T355">They closed the door; a bear came.</ta>
            <ta e="T362" id="Seg_11382" s="T360">"Let me in!"</ta>
            <ta e="T366" id="Seg_11383" s="T363">They don't let [him] in.</ta>
            <ta e="T372" id="Seg_11384" s="T366">"I'll (destroy?) your house now!"</ta>
            <ta e="T377" id="Seg_11385" s="T373">It destroyed [it], they ran away.</ta>
            <ta e="T380" id="Seg_11386" s="T378">Which house.</ta>
            <ta e="T383" id="Seg_11387" s="T381">[The house] they made from twigs.</ta>
            <ta e="T389" id="Seg_11388" s="T384">And there they closed the door.</ta>
            <ta e="T392" id="Seg_11389" s="T389">He came there.</ta>
            <ta e="T394" id="Seg_11390" s="T393">"Open up!</ta>
            <ta e="T400" id="Seg_11391" s="T395">Or the house will fall [= be destroyed] now."</ta>
            <ta e="T406" id="Seg_11392" s="T401">Then it (destroyed?) [it], they ran away.</ta>
            <ta e="T411" id="Seg_11393" s="T407">And they came to the other.</ta>
            <ta e="T415" id="Seg_11394" s="T412">They came to the third pig.</ta>
            <ta e="T418" id="Seg_11395" s="T416">"Let us in!"</ta>
            <ta e="T424" id="Seg_11396" s="T418">It let them in and closed the door.</ta>
            <ta e="T427" id="Seg_11397" s="T424">It came, the bear.</ta>
            <ta e="T428" id="Seg_11398" s="T427">"Let me in!"</ta>
            <ta e="T431" id="Seg_11399" s="T429">"I won't let [you] in."</ta>
            <ta e="T436" id="Seg_11400" s="T432">Then it wanted to destroy [the house].</ta>
            <ta e="T438" id="Seg_11401" s="T436">It could not.</ta>
            <ta e="T445" id="Seg_11402" s="T438">Then it climbed to the chimney.</ta>
            <ta e="T447" id="Seg_11403" s="T446">It is climbing.</ta>
            <ta e="T453" id="Seg_11404" s="T447">And [there] was a cauldron with hot water.</ta>
            <ta e="T458" id="Seg_11405" s="T454">It fell into the cauldron.</ta>
            <ta e="T470" id="Seg_11406" s="T459">And then it (returned?) and went away and didn't came again any more.</ta>
            <ta e="T474" id="Seg_11407" s="T471">They scalded it.</ta>
            <ta e="T477" id="Seg_11408" s="T475">And it went away.</ta>
            <ta e="T484" id="Seg_11409" s="T478">How much tapes have we recorded?</ta>
            <ta e="T487" id="Seg_11410" s="T485">Thirteen.</ta>
            <ta e="T491" id="Seg_11411" s="T488">There is your bread.</ta>
            <ta e="T496" id="Seg_11412" s="T492">There it is, and where have you taken it?</ta>
            <ta e="T501" id="Seg_11413" s="T497">One woman gave [it].</ta>
            <ta e="T504" id="Seg_11414" s="T502">From Voznesenskoye. [?]</ta>
            <ta e="T510" id="Seg_11415" s="T505">One goat had five children.</ta>
            <ta e="T515" id="Seg_11416" s="T511">She left them at home.</ta>
            <ta e="T518" id="Seg_11417" s="T516">Closed the door.</ta>
            <ta e="T524" id="Seg_11418" s="T519">"Don't let anyone in."</ta>
            <ta e="T527" id="Seg_11419" s="T525">Then she left.</ta>
            <ta e="T530" id="Seg_11420" s="T528">A bear came.</ta>
            <ta e="T532" id="Seg_11421" s="T530">"Open the door!"</ta>
            <ta e="T536" id="Seg_11422" s="T533">They didn't open.</ta>
            <ta e="T543" id="Seg_11423" s="T537">"Our mother said us not to let anyone in."</ta>
            <ta e="T549" id="Seg_11424" s="T544">Then the bear left, their mother came.</ta>
            <ta e="T555" id="Seg_11425" s="T550">"Open the door, I brought milk.</ta>
            <ta e="T559" id="Seg_11426" s="T556">I brought you grass.</ta>
            <ta e="T561" id="Seg_11427" s="T559">I brought curd cheese."</ta>
            <ta e="T566" id="Seg_11428" s="T562">Then they opened [the door], ate.</ta>
            <ta e="T570" id="Seg_11429" s="T566">Then their mother went away again.</ta>
            <ta e="T574" id="Seg_11430" s="T571">"Don't let anyone in!"</ta>
            <ta e="T577" id="Seg_11431" s="T575">She left again.</ta>
            <ta e="T581" id="Seg_11432" s="T578">"Don't let anyone in!"</ta>
            <ta e="T586" id="Seg_11433" s="T582">Then the bear came again.</ta>
            <ta e="T591" id="Seg_11434" s="T587">"Children, children, let me in!"</ta>
            <ta e="T597" id="Seg_11435" s="T592">"No, we won't let [you] in."</ta>
            <ta e="T603" id="Seg_11436" s="T598">"I am your mother!"</ta>
            <ta e="T609" id="Seg_11437" s="T603">"No, my [=our] mother's voice is thin!</ta>
            <ta e="T615" id="Seg_11438" s="T610">And your voice is very coarse."</ta>
            <ta e="T619" id="Seg_11439" s="T616">Then the bear went away.</ta>
            <ta e="T622" id="Seg_11440" s="T619">Then mother came.</ta>
            <ta e="T626" id="Seg_11441" s="T623">They opened [the door], [she] gave [them] food.</ta>
            <ta e="T631" id="Seg_11442" s="T627">"Now don't let anyone in!"</ta>
            <ta e="T636" id="Seg_11443" s="T632">And the bear went to the forgery.</ta>
            <ta e="T641" id="Seg_11444" s="T637">It made [himself] a thin tongue [= voice].</ta>
            <ta e="T645" id="Seg_11445" s="T642">Then it came again.</ta>
            <ta e="T653" id="Seg_11446" s="T646">He said: "I am your mother, I brought milk.</ta>
            <ta e="T656" id="Seg_11447" s="T654">I brought bread.</ta>
            <ta e="T661" id="Seg_11448" s="T657">I brought you cheese, grass."</ta>
            <ta e="T665" id="Seg_11449" s="T662">They opened [the door].</ta>
            <ta e="T670" id="Seg_11450" s="T666">He ate them all.</ta>
            <ta e="T673" id="Seg_11451" s="T671">Their mother came.</ta>
            <ta e="T676" id="Seg_11452" s="T674">Her children are not [here].</ta>
            <ta e="T681" id="Seg_11453" s="T676">One, the smallest, has hidden himself.</ta>
            <ta e="T687" id="Seg_11454" s="T682">He says: "The bear has eaten everybody.</ta>
            <ta e="T691" id="Seg_11455" s="T688">I remained alone.</ta>
            <ta e="T704" id="Seg_11456" s="T691">I climbed in the oven, it didn't see me and went away."</ta>
            <ta e="T709" id="Seg_11457" s="T705">Then the bear came to the goat.</ta>
            <ta e="T715" id="Seg_11458" s="T710">The goat says: "Let's go.</ta>
            <ta e="T720" id="Seg_11459" s="T716">Let's jump above this hole.</ta>
            <ta e="T723" id="Seg_11460" s="T721">The goat jumped.</ta>
            <ta e="T731" id="Seg_11461" s="T724">And he couldn't jump there.</ta>
            <ta e="T737" id="Seg_11462" s="T732">He grazed [his skin] (?), [fell] on something and tore his belly.</ta>
            <ta e="T747" id="Seg_11463" s="T737">Then the goatlings jumped [from there] and returned to their mother.</ta>
            <ta e="T753" id="Seg_11464" s="T747">"Let's go with you [= together] to visit someone."</ta>
            <ta e="T758" id="Seg_11465" s="T754">"What will be there?"</ta>
            <ta e="T764" id="Seg_11466" s="T759">"What will be, we'll drink vodka.</ta>
            <ta e="T766" id="Seg_11467" s="T764">We'll sing songs.</ta>
            <ta e="T768" id="Seg_11468" s="T767">We'll jump [=dance].</ta>
            <ta e="T770" id="Seg_11469" s="T768">We played concertina.</ta>
            <ta e="T779" id="Seg_11470" s="T771">There will be much of everything — lots of meat, (?), butter.</ta>
            <ta e="T782" id="Seg_11471" s="T780">Boiled fish.</ta>
            <ta e="T784" id="Seg_11472" s="T783">Bread.</ta>
            <ta e="T787" id="Seg_11473" s="T785">Sugar stands [= is on the table].</ta>
            <ta e="T796" id="Seg_11474" s="T788">Cherry berries, dried berries, black berries, let's go!"</ta>
            <ta e="T800" id="Seg_11475" s="T797">Did you (?) the houses?</ta>
            <ta e="T802" id="Seg_11476" s="T801">I did (?).</ta>
            <ta e="T806" id="Seg_11477" s="T803">How many houses did you (?)?</ta>
            <ta e="T808" id="Seg_11478" s="T807">Seven.</ta>
            <ta e="T812" id="Seg_11479" s="T809">[?]</ta>
            <ta e="T818" id="Seg_11480" s="T813">Didn't you go to the forest?</ta>
            <ta e="T820" id="Seg_11481" s="T818">I didn't go.</ta>
            <ta e="T827" id="Seg_11482" s="T820">I was afraid, because there lives the bear (?).</ta>
            <ta e="T832" id="Seg_11483" s="T828">Why are you afraid, go, you!</ta>
            <ta e="T839" id="Seg_11484" s="T833">It won't do anything to you.</ta>
            <ta e="T844" id="Seg_11485" s="T840">You'll (hide?) and then come back home.</ta>
            <ta e="T847" id="Seg_11486" s="T845">You can (?).</ta>
            <ta e="T849" id="Seg_11487" s="T848">(…) I say.</ta>
            <ta e="T854" id="Seg_11488" s="T850">"I am still afraid.</ta>
            <ta e="T862" id="Seg_11489" s="T854">I have nothing, no gun, no knife.</ta>
            <ta e="T865" id="Seg_11490" s="T863">Only a stone."</ta>
            <ta e="T869" id="Seg_11491" s="T865">"It won't (?).</ta>
            <ta e="T874" id="Seg_11492" s="T870">It has afraid of humans, he'll run away."</ta>
            <ta e="T882" id="Seg_11493" s="T875">We went to this forest.</ta>
            <ta e="T886" id="Seg_11494" s="T883">Then it began to snow.</ta>
            <ta e="T894" id="Seg_11495" s="T887">They three went, and I returned back.</ta>
            <ta e="T903" id="Seg_11496" s="T895">Then I heard: someone is breaking twigs.</ta>
            <ta e="T913" id="Seg_11497" s="T904">I think: a bear (?).</ta>
            <ta e="T919" id="Seg_11498" s="T913">But I didn't see [it], I came home.</ta>
            <ta e="T925" id="Seg_11499" s="T920">I'm going and was scared.</ta>
            <ta e="T929" id="Seg_11500" s="T926">I should climb on a tree.</ta>
            <ta e="T937" id="Seg_11501" s="T929">I said to him: "It'll climb on the tree, too".</ta>
            <ta e="T944" id="Seg_11502" s="T938">It will break the tree and bring us (down?) to earth.</ta>
            <ta e="T951" id="Seg_11503" s="T945">How [is it possible] to speak here, children are shouting.</ta>
            <ta e="T956" id="Seg_11504" s="T952">Pigs are grunting, too.</ta>
            <ta e="T959" id="Seg_11505" s="T956">(Cows?) are mooing, too.</ta>
            <ta e="T963" id="Seg_11506" s="T960">It is impossible to speak.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_11507" s="T1">Euer Kamera…</ta>
            <ta e="T8" id="Seg_11508" s="T6">Es ist müde geworden.</ta>
            <ta e="T11" id="Seg_11509" s="T9">Es hat angefangen zu lügen [= schlecht zu arbeiten].</ta>
            <ta e="T15" id="Seg_11510" s="T12">Es ist sehr faul geworden.</ta>
            <ta e="T23" id="Seg_11511" s="T16">Du bist ein sehr (starker?) Mann, ich erzähle dir wieder und wieder.</ta>
            <ta e="T30" id="Seg_11512" s="T23">Aber du weißt gar nichts.</ta>
            <ta e="T36" id="Seg_11513" s="T31">Ich sagte dir: Ich war klein.</ta>
            <ta e="T40" id="Seg_11514" s="T37">Es kamen einige Leute.</ta>
            <ta e="T46" id="Seg_11515" s="T41">Vielleicht eine Expedition, ich weiß es nicht.</ta>
            <ta e="T50" id="Seg_11516" s="T46">Ich war klein, ich habe nicht gefragt.</ta>
            <ta e="T54" id="Seg_11517" s="T51">Sie haben es mir nicht gesagt.</ta>
            <ta e="T59" id="Seg_11518" s="T55">Bei Wlas, Ivan Andrejewitsch. [?]</ta>
            <ta e="T69" id="Seg_11519" s="T60">Sie hatten Ferngläser und noch etwas, ich weiß es nicht.</ta>
            <ta e="T72" id="Seg_11520" s="T69">Sie zeigten mir eine Nadel.</ta>
            <ta e="T75" id="Seg_11521" s="T73">Sie steckten sie in die Wand.</ta>
            <ta e="T78" id="Seg_11522" s="T76">So weit.</ta>
            <ta e="T82" id="Seg_11523" s="T79">Ungefähr zwanzig Saschen entfernt.</ta>
            <ta e="T91" id="Seg_11524" s="T83">Dann schaute ich [durch das Fernglas]: die (Nadel?) ist sehr groß.</ta>
            <ta e="T97" id="Seg_11525" s="T92">Dann drehten sie das andere Ende nach oben.</ta>
            <ta e="T102" id="Seg_11526" s="T98">Ich sah: es war sehr weit weg.</ta>
            <ta e="T106" id="Seg_11527" s="T103">Dann legten sie [ein Blatt] Papier [hin].</ta>
            <ta e="T112" id="Seg_11528" s="T107">Und drehten das Fernglas zur Sonne.</ta>
            <ta e="T116" id="Seg_11529" s="T113">Und das Papier fing Feuer.</ta>
            <ta e="T120" id="Seg_11530" s="T117">Wann geht ihr?</ta>
            <ta e="T123" id="Seg_11531" s="T121">Wir gehen nach (?).</ta>
            <ta e="T126" id="Seg_11532" s="T124">Gut, geht!</ta>
            <ta e="T131" id="Seg_11533" s="T127">Grüßt Matveev.</ta>
            <ta e="T137" id="Seg_11534" s="T132">Elja ging gestern nach Malinovka.</ta>
            <ta e="T140" id="Seg_11535" s="T138">Sie nahm ein Papier [mit].</ta>
            <ta e="T145" id="Seg_11536" s="T141">Sie musste es vom Vorsitzenden [der Kolchose?] stempeln [lassen].</ta>
            <ta e="T150" id="Seg_11537" s="T146">Sie fuhr mit dem Auto dorthin. </ta>
            <ta e="T156" id="Seg_11538" s="T151">Und zurück von dort kommt sie zu Fuß.</ta>
            <ta e="T165" id="Seg_11539" s="T157">Der Sohn meiner Schwester ging (?) um der Kuh (?) zu geben.</ta>
            <ta e="T169" id="Seg_11540" s="T166">Er nahm seinen eigenen Sohn [mit].</ta>
            <ta e="T174" id="Seg_11541" s="T170">Bald kamen sie zurück nach Hause.</ta>
            <ta e="T181" id="Seg_11542" s="T175">Ein Mensch dachte [= wollte] ein Zelt machen.</ta>
            <ta e="T183" id="Seg_11543" s="T182">Mit Gold.</ta>
            <ta e="T190" id="Seg_11544" s="T184">Und ein anderer Mann [wollte] ein Haus aus Silber machen.</ta>
            <ta e="T200" id="Seg_11545" s="T190">Und der dritte Mann (?) sagt: "Ich mache ein Haus aus Holz."</ta>
            <ta e="T208" id="Seg_11546" s="T201">Und der vierte Mann: "Ich mache [es] aus Gras, aus Stroh."</ta>
            <ta e="T213" id="Seg_11547" s="T209">Er wird zum Arbeiten kommen. [?]</ta>
            <ta e="T220" id="Seg_11548" s="T214">Das Haus von dem, der gearbeitet hat, wird nicht brennen (?).</ta>
            <ta e="T225" id="Seg_11549" s="T221">Er wird in Ordnung sein.</ta>
            <ta e="T231" id="Seg_11550" s="T226">Und [das Haus] von dem, der arbeitet, wird brennen. [?]</ta>
            <ta e="T236" id="Seg_11551" s="T232">Er wird nicht in Ordnung sein.</ta>
            <ta e="T240" id="Seg_11552" s="T237">Er selber wird nicht brennen.</ta>
            <ta e="T241" id="Seg_11553" s="T240">Er wird entkommen.</ta>
            <ta e="T249" id="Seg_11554" s="T242">Aber was er gemacht hat, wird brennen.</ta>
            <ta e="T254" id="Seg_11555" s="T250">Es lebten drei Schweine.</ta>
            <ta e="T261" id="Seg_11556" s="T255">Zwei Schweine waren sehr faul.</ta>
            <ta e="T266" id="Seg_11557" s="T261">Und ein Schwein arbeitete immer.</ta>
            <ta e="T273" id="Seg_11558" s="T267">Sie gingen auf Besuch, tranken Wodka.</ta>
            <ta e="T281" id="Seg_11559" s="T274">Und das eine Schwein pflügte die Erde.</ta>
            <ta e="T284" id="Seg_11560" s="T282">Sie sammelte Steine.</ta>
            <ta e="T291" id="Seg_11561" s="T285">Und sie bauten sich ein Haus aus Stein.</ta>
            <ta e="T297" id="Seg_11562" s="T292">Sie kamen, eins machte [ein Haus?].</ta>
            <ta e="T302" id="Seg_11563" s="T298">Es sammelte Zweige und machte [es].</ta>
            <ta e="T309" id="Seg_11564" s="T302">Und eins machte sich ein Haus aus Stroh.</ta>
            <ta e="T316" id="Seg_11565" s="T310">Und es legt einen Stein neben den anderen.</ta>
            <ta e="T322" id="Seg_11566" s="T316">Sie kamen: "Warum arbeitest du?</ta>
            <ta e="T329" id="Seg_11567" s="T323">Wir haben [unsere] Häuser schnell gebaut."</ta>
            <ta e="T339" id="Seg_11568" s="T330">"Aber wessen Häuser werden besser sein, meine oder eure?"</ta>
            <ta e="T343" id="Seg_11569" s="T340">Dann wurde es kalt.</ta>
            <ta e="T346" id="Seg_11570" s="T343">Der Winter kommt.</ta>
            <ta e="T354" id="Seg_11571" s="T347">Dann kamen sie in das Haus, das aus Stroh [gemacht war].</ta>
            <ta e="T359" id="Seg_11572" s="T355">Sie schlossen die Tür, ein Bär kam.</ta>
            <ta e="T362" id="Seg_11573" s="T360">"Lasst mich rein!"</ta>
            <ta e="T366" id="Seg_11574" s="T363">Sie ließen [ihn] nicht rein.</ta>
            <ta e="T372" id="Seg_11575" s="T366">"ich werde euer Haus jetzt (zerstören?)!"</ta>
            <ta e="T377" id="Seg_11576" s="T373">Er zerstörte [es], sie rannten weg.</ta>
            <ta e="T380" id="Seg_11577" s="T378">Welches Haus.</ta>
            <ta e="T383" id="Seg_11578" s="T381">[Das Haus] dass aus Zweigen gemacht ist.</ta>
            <ta e="T389" id="Seg_11579" s="T384">Und dort schlossen sie die Tür.</ta>
            <ta e="T392" id="Seg_11580" s="T389">Er kam dorthin.</ta>
            <ta e="T394" id="Seg_11581" s="T393">"Öffnet!</ta>
            <ta e="T400" id="Seg_11582" s="T395">Oder das Haus wird jetzt fallen [= zerstört werden]."</ta>
            <ta e="T406" id="Seg_11583" s="T401">Dann zerstörte er [es], sie rannten davon.</ta>
            <ta e="T411" id="Seg_11584" s="T407">Und sie kamen zum anderen.</ta>
            <ta e="T415" id="Seg_11585" s="T412">Sie kamen zum dritten Schwein.</ta>
            <ta e="T418" id="Seg_11586" s="T416">"Lass uns rein!"</ta>
            <ta e="T424" id="Seg_11587" s="T418">Es ließ sie rein und schloss die Tür.</ta>
            <ta e="T427" id="Seg_11588" s="T424">Er kam, der Bär.</ta>
            <ta e="T428" id="Seg_11589" s="T427">"Lass mich rein!"</ta>
            <ta e="T431" id="Seg_11590" s="T429">"Ich lasse [dich] nicht rein."</ta>
            <ta e="T436" id="Seg_11591" s="T432">Dann wollte er [das Haus] zerstören.</ta>
            <ta e="T438" id="Seg_11592" s="T436">Er konnte nicht.</ta>
            <ta e="T445" id="Seg_11593" s="T438">Dann kletterte er auf den Schornstein.</ta>
            <ta e="T447" id="Seg_11594" s="T446">Er klettert.</ta>
            <ta e="T453" id="Seg_11595" s="T447">Und [dort] war ein Kessel mit heißem Wasser.</ta>
            <ta e="T458" id="Seg_11596" s="T454">Er fiel in den Kessel.</ta>
            <ta e="T470" id="Seg_11597" s="T459">Und dann (drehte er um?), rannte davon und kam nie wieder.</ta>
            <ta e="T474" id="Seg_11598" s="T471">Sie verbrühten ihn.</ta>
            <ta e="T477" id="Seg_11599" s="T475">Und er ging weg.</ta>
            <ta e="T484" id="Seg_11600" s="T478">Wie viele Bänder haben wir aufgenommen?</ta>
            <ta e="T487" id="Seg_11601" s="T485">Dreizehn.</ta>
            <ta e="T491" id="Seg_11602" s="T488">Da ist dein Brot.</ta>
            <ta e="T496" id="Seg_11603" s="T492">Da ist es, und wo hast du es hingetan?</ta>
            <ta e="T501" id="Seg_11604" s="T497">Eine Frau gab [es].</ta>
            <ta e="T504" id="Seg_11605" s="T502">Aus Voznesenskoje. [?]</ta>
            <ta e="T510" id="Seg_11606" s="T505">Eine Ziege hatte fünf Kinder.</ta>
            <ta e="T515" id="Seg_11607" s="T511">Sie ließ sie zuhause.</ta>
            <ta e="T518" id="Seg_11608" s="T516">Sie schloss die Tür.</ta>
            <ta e="T524" id="Seg_11609" s="T519">"Lasst niemanden rein."</ta>
            <ta e="T527" id="Seg_11610" s="T525">Dann ging sie weg.</ta>
            <ta e="T530" id="Seg_11611" s="T528">Ein Bär kam.</ta>
            <ta e="T532" id="Seg_11612" s="T530">"Öffnet die Tür!"</ta>
            <ta e="T536" id="Seg_11613" s="T533">Sie öffneten nicht.</ta>
            <ta e="T543" id="Seg_11614" s="T537">"Unsere Mutter sagte uns, dass wir niemanden reinlassen [sollen]."</ta>
            <ta e="T549" id="Seg_11615" s="T544">Dann ging der Bär weg, ihre Mutter kam.</ta>
            <ta e="T555" id="Seg_11616" s="T550">"Öffnet die Tür, ich habe Milch geholt.</ta>
            <ta e="T559" id="Seg_11617" s="T556">Ich habe euch Gras geholt.</ta>
            <ta e="T561" id="Seg_11618" s="T559">Ich habe Quark gebracht."</ta>
            <ta e="T566" id="Seg_11619" s="T562">Da öffneten sie [die Tür], fraßen.</ta>
            <ta e="T570" id="Seg_11620" s="T566">Dann ging ihre Mutter wieder weg.</ta>
            <ta e="T574" id="Seg_11621" s="T571">"Lasst niemanden rein!"</ta>
            <ta e="T577" id="Seg_11622" s="T575">Sie ging wieder weg.</ta>
            <ta e="T581" id="Seg_11623" s="T578">"Lasst niemanden rein!"</ta>
            <ta e="T586" id="Seg_11624" s="T582">Dann kam der Bär wieder.</ta>
            <ta e="T591" id="Seg_11625" s="T587">"Kinder, Kinder, lasst mich rein!"</ta>
            <ta e="T597" id="Seg_11626" s="T592">"Nein, wir lassen [dich] nicht rein."</ta>
            <ta e="T603" id="Seg_11627" s="T598">"Ich bin eure Mutter!"</ta>
            <ta e="T609" id="Seg_11628" s="T603">"Nein, die Stimme meiner [= unserer] Mutter ist dünn!</ta>
            <ta e="T615" id="Seg_11629" s="T610">Und deine Stimme ist sehr rau."</ta>
            <ta e="T619" id="Seg_11630" s="T616">Dann ging der Bär weg.</ta>
            <ta e="T622" id="Seg_11631" s="T619">Dann kam die Mutter.</ta>
            <ta e="T626" id="Seg_11632" s="T623">Sie öffneten [die Tür], [sie] gab [ihnen] Essen.</ta>
            <ta e="T631" id="Seg_11633" s="T627">"Jetzt lasst niemanden rein!"</ta>
            <ta e="T636" id="Seg_11634" s="T632">Und der Bär ging zur Fälscherei.</ta>
            <ta e="T641" id="Seg_11635" s="T637">Er machte [sich selbst] eine dünne Zunge [= Stimme].</ta>
            <ta e="T645" id="Seg_11636" s="T642">Dann kam er wieder.</ta>
            <ta e="T653" id="Seg_11637" s="T646">Er sagte: "Ich bin eure Mutter, ich habe Milch geholt.</ta>
            <ta e="T656" id="Seg_11638" s="T654">Ich habe Brot geholt.</ta>
            <ta e="T661" id="Seg_11639" s="T657">Ich habe euch Quark geholt, Gras."</ta>
            <ta e="T665" id="Seg_11640" s="T662">Sie öffneten [die Tür].</ta>
            <ta e="T670" id="Seg_11641" s="T666">Er fraß sie alle.</ta>
            <ta e="T673" id="Seg_11642" s="T671">Ihre Mutter kam.</ta>
            <ta e="T676" id="Seg_11643" s="T674">Ihre Kinder sind nicht da.</ta>
            <ta e="T681" id="Seg_11644" s="T676">Eins, das kleinste, hatte sich versteckt.</ta>
            <ta e="T687" id="Seg_11645" s="T682">Es sagt: "Der Bär hat alle gefressen.</ta>
            <ta e="T691" id="Seg_11646" s="T688">Ich bin alleine zurückgeblieben.</ta>
            <ta e="T704" id="Seg_11647" s="T691">Ich bin in den Ofen geklettert, er hat mich nicht gesehen und ist weggegangen."</ta>
            <ta e="T709" id="Seg_11648" s="T705">Dann kam der Bär zur Ziege.</ta>
            <ta e="T715" id="Seg_11649" s="T710">Die Ziege sagt: "Gehen wir.</ta>
            <ta e="T720" id="Seg_11650" s="T716">Lass uns über dieses Loch springen.</ta>
            <ta e="T723" id="Seg_11651" s="T721">Die Ziege sprang.</ta>
            <ta e="T731" id="Seg_11652" s="T724">Und er konnte nicht springen.</ta>
            <ta e="T737" id="Seg_11653" s="T732">Er streifte [seine Haut] ab (?), [fiel] auf etwas und riss sich seinen Bauch auf.</ta>
            <ta e="T747" id="Seg_11654" s="T737">Dann sprangen die Zicklein [von dort heraus] und kehrten zu ihrer Mutter zurück.</ta>
            <ta e="T753" id="Seg_11655" s="T747">"Lass uns mit euch [= zusammen] jemanden besuchen gehen."</ta>
            <ta e="T758" id="Seg_11656" s="T754">"Was wird dort sein?"</ta>
            <ta e="T764" id="Seg_11657" s="T759">"Was wird sein, wir werden Wodka trinken.</ta>
            <ta e="T766" id="Seg_11658" s="T764">Wir werden Lieder singen.</ta>
            <ta e="T768" id="Seg_11659" s="T767">Wir werden springen [= tanzen].</ta>
            <ta e="T770" id="Seg_11660" s="T768">Wir spielten Harmonika.</ta>
            <ta e="T779" id="Seg_11661" s="T771">Dort wird viel von allem Möglichen sein — viel Fleisch, (?), Butter.</ta>
            <ta e="T782" id="Seg_11662" s="T780">Gekochter Fisch.</ta>
            <ta e="T784" id="Seg_11663" s="T783">Brot.</ta>
            <ta e="T787" id="Seg_11664" s="T785">Zucker steht [= ist auf dem Tisch].</ta>
            <ta e="T796" id="Seg_11665" s="T788">Kirschen, getrocknete Beeren, schwarze Beeren, lass uns gehen!"</ta>
            <ta e="T800" id="Seg_11666" s="T797">Hast du (?) die Häuser?</ta>
            <ta e="T802" id="Seg_11667" s="T801">Ich habe (?).</ta>
            <ta e="T806" id="Seg_11668" s="T803">Wie viele Häuser hast du (?)?</ta>
            <ta e="T808" id="Seg_11669" s="T807">Sieben.</ta>
            <ta e="T812" id="Seg_11670" s="T809">[?]</ta>
            <ta e="T818" id="Seg_11671" s="T813">Bist du nicht in den Wald gegangen?</ta>
            <ta e="T820" id="Seg_11672" s="T818">Ich bin nicht gegangen.</ta>
            <ta e="T827" id="Seg_11673" s="T820">Ich hatte Angst, weil der Bär dort lebt (?).</ta>
            <ta e="T832" id="Seg_11674" s="T828">Warum hast du Angst, geh!</ta>
            <ta e="T839" id="Seg_11675" s="T833">Er wird dir nichts tun.</ta>
            <ta e="T844" id="Seg_11676" s="T840">Du (versteckst dich?) und kommst dann nach Hause.</ta>
            <ta e="T847" id="Seg_11677" s="T845">Du kannst (?).</ta>
            <ta e="T849" id="Seg_11678" s="T848">(…) sage ich.</ta>
            <ta e="T854" id="Seg_11679" s="T850">"Ich habe immer noch Angst.</ta>
            <ta e="T862" id="Seg_11680" s="T854">Ich habe nichts, kein Gewehr, kein Messer.</ta>
            <ta e="T865" id="Seg_11681" s="T863">Nur einen Stein."</ta>
            <ta e="T869" id="Seg_11682" s="T865">"Er wird nicht (?).</ta>
            <ta e="T874" id="Seg_11683" s="T870">Er hat Angst vor Menschen, er rennt weg."</ta>
            <ta e="T882" id="Seg_11684" s="T875">Wir gingen in diesen Wald.</ta>
            <ta e="T886" id="Seg_11685" s="T883">Dann fing es an zu schneien.</ta>
            <ta e="T894" id="Seg_11686" s="T887">Die drei gingen und ich kam zurück.</ta>
            <ta e="T903" id="Seg_11687" s="T895">Dann hörte ich: Jemand zerbricht Zweige.</ta>
            <ta e="T913" id="Seg_11688" s="T904">Ich denke: Ein Bär (?).</ta>
            <ta e="T919" id="Seg_11689" s="T913">Aber ich habe [es] nicht gesehen, ich kam nach Hause.</ta>
            <ta e="T925" id="Seg_11690" s="T920">Ich gehe und hatte Angst.</ta>
            <ta e="T929" id="Seg_11691" s="T926">Ich sollte auf einen Baum klettern.</ta>
            <ta e="T937" id="Seg_11692" s="T929">Ich sagte zu ihm: "Er [= der Bär] wird auch auf den Baum klettern.</ta>
            <ta e="T944" id="Seg_11693" s="T938">Er wird den Baum abbrechen und uns (hinunter?) auf die Erde bringen.</ta>
            <ta e="T951" id="Seg_11694" s="T945">Wie [ist es möglich] hier zu sprechen, Kinder schreien.</ta>
            <ta e="T956" id="Seg_11695" s="T952">Und Schweine grunzen.</ta>
            <ta e="T959" id="Seg_11696" s="T956">Und (Kühe?) muhen.</ta>
            <ta e="T963" id="Seg_11697" s="T960">Es ist unmöglich zu sprechen.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T23" id="Seg_11698" s="T16">[GVY:] măllial = mălliam? </ta>
            <ta e="T59" id="Seg_11699" s="T55">[GVY:] Unclear.</ta>
            <ta e="T82" id="Seg_11700" s="T79">[WNB] one sazhen is 213,36 cm</ta>
            <ta e="T220" id="Seg_11701" s="T214">[GVY:] Unclear.</ta>
            <ta e="T231" id="Seg_11702" s="T226">[GVY:] Unclear.</ta>
            <ta e="T254" id="Seg_11703" s="T250">[AAV] "Three little pigs"</ta>
            <ta e="T316" id="Seg_11704" s="T310">[GVY:] pim piʔzʼiʔ - Russ. ветку за веткой?</ta>
            <ta e="T504" id="Seg_11705" s="T502">[WNB] not clear</ta>
            <ta e="T510" id="Seg_11706" s="T505">[AAV] "Seven goatlings" (here five)</ta>
            <ta e="T561" id="Seg_11707" s="T559">[GVY:] Rus. творог 'cottage cheese or curd cheese'</ta>
            <ta e="T631" id="Seg_11708" s="T627">[GVY:] DAT from the Russian "никому [не открывайте]"?</ta>
            <ta e="T709" id="Seg_11709" s="T705">[GVY:] urga- šo- aba</ta>
            <ta e="T737" id="Seg_11710" s="T732">[AAV] Rus. "загремел" in the sense of "fell down"?</ta>
            <ta e="T770" id="Seg_11711" s="T768">[GVY:] sʼarzbibeʔ = sʼarzləbeʔ [play-FUT-1PL]?</ta>
            <ta e="T782" id="Seg_11712" s="T780">[GVY:] PART.PRES from the stative stem 'be boiled'?</ta>
            <ta e="T796" id="Seg_11713" s="T788">[GVY:] kobo: cf. koʔ- 'dry (v.)' (D 32a), kobi-noʔ 'trockenes Gras' (D 31a).</ta>
            <ta e="T802" id="Seg_11714" s="T801">[GVY:] Kürbiel = kürbiem?</ta>
            <ta e="T847" id="Seg_11715" s="T845">[GVY:] le izittə 'take bones?'</ta>
            <ta e="T869" id="Seg_11716" s="T865">[GVY:] It won't hurt you?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T964" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T965" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T966" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T967" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T968" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T969" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T970" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T971" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T972" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T973" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
