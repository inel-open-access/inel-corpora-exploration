<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD0028994-D012-8AB6-EDE9-A0C089AB47D5">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0211.wav" />
         <referenced-file url="PKZ_1964_SU0211.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0211\PKZ_1964_SU0211.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1619</ud-information>
            <ud-information attribute-name="# HIAT:w">905</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">51</ud-information>
            <ud-information attribute-name="# e">949</ud-information>
            <ud-information attribute-name="# HIAT:u">221</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.12" type="appl" />
         <tli id="T1" time="0.824" type="appl" />
         <tli id="T2" time="1.527" type="appl" />
         <tli id="T3" time="2.231" type="appl" />
         <tli id="T4" time="7.479963430509504" />
         <tli id="T5" time="8.5" type="appl" />
         <tli id="T6" time="9.163" type="appl" />
         <tli id="T7" time="9.827" type="appl" />
         <tli id="T8" time="10.477666873716027" />
         <tli id="T9" time="11.12" type="appl" />
         <tli id="T10" time="11.743" type="appl" />
         <tli id="T11" time="13.006603077472407" />
         <tli id="T12" time="13.863" type="appl" />
         <tli id="T13" time="14.546" type="appl" />
         <tli id="T14" time="15.229" type="appl" />
         <tli id="T15" time="15.852" type="appl" />
         <tli id="T16" time="16.476" type="appl" />
         <tli id="T17" time="17.099" type="appl" />
         <tli id="T18" time="17.552" type="appl" />
         <tli id="T19" time="19.593237542127834" />
         <tli id="T20" time="20.402" type="appl" />
         <tli id="T21" time="21.101" type="appl" />
         <tli id="T22" time="21.8" type="appl" />
         <tli id="T23" time="22.498" type="appl" />
         <tli id="T24" time="23.197" type="appl" />
         <tli id="T25" time="24.126548712133598" />
         <tli id="T26" time="24.788" type="appl" />
         <tli id="T27" time="26.27987151788633" />
         <tli id="T28" time="27.014" type="appl" />
         <tli id="T29" time="27.599" type="appl" />
         <tli id="T30" time="28.184" type="appl" />
         <tli id="T31" time="28.768" type="appl" />
         <tli id="T32" time="29.353" type="appl" />
         <tli id="T33" time="29.938" type="appl" />
         <tli id="T34" time="30.523" type="appl" />
         <tli id="T35" time="31.202" type="appl" />
         <tli id="T36" time="31.867" type="appl" />
         <tli id="T37" time="32.533" type="appl" />
         <tli id="T38" time="33.199" type="appl" />
         <tli id="T39" time="33.864" type="appl" />
         <tli id="T40" time="34.53" type="appl" />
         <tli id="T41" time="35.33" type="appl" />
         <tli id="T42" time="36.116" type="appl" />
         <tli id="T43" time="36.903" type="appl" />
         <tli id="T44" time="37.689" type="appl" />
         <tli id="T45" time="40.213136731580505" />
         <tli id="T46" time="41.322" type="appl" />
         <tli id="T47" time="42.28" type="appl" />
         <tli id="T48" time="43.238" type="appl" />
         <tli id="T49" time="44.195" type="appl" />
         <tli id="T50" time="45.153" type="appl" />
         <tli id="T51" time="46.111" type="appl" />
         <tli id="T52" time="50.233087744107934" />
         <tli id="T53" time="50.852" type="appl" />
         <tli id="T54" time="51.495" type="appl" />
         <tli id="T964" time="51.735749999999996" type="intp" />
         <tli id="T55" time="52.137" type="appl" />
         <tli id="T56" time="52.78" type="appl" />
         <tli id="T57" time="53.422" type="appl" />
         <tli id="T58" time="54.064" type="appl" />
         <tli id="T59" time="54.707" type="appl" />
         <tli id="T60" time="55.57972827108532" />
         <tli id="T61" time="56.208" type="appl" />
         <tli id="T62" time="56.925" type="appl" />
         <tli id="T63" time="57.641" type="appl" />
         <tli id="T64" time="58.358" type="appl" />
         <tli id="T65" time="59.074" type="appl" />
         <tli id="T66" time="59.79" type="appl" />
         <tli id="T67" time="60.507" type="appl" />
         <tli id="T68" time="61.223" type="appl" />
         <tli id="T69" time="62.133" type="appl" />
         <tli id="T70" time="62.927" type="appl" />
         <tli id="T71" time="63.72" type="appl" />
         <tli id="T72" time="64.513" type="appl" />
         <tli id="T73" time="65.307" type="appl" />
         <tli id="T74" time="66.1" type="appl" />
         <tli id="T75" time="66.893" type="appl" />
         <tli id="T76" time="67.687" type="appl" />
         <tli id="T77" time="68.99966266111709" />
         <tli id="T78" time="69.793" type="appl" />
         <tli id="T79" time="70.396" type="appl" />
         <tli id="T80" time="71.95964818976789" />
         <tli id="T81" time="75.33963166505161" />
         <tli id="T82" time="76.326" type="appl" />
         <tli id="T83" time="77.16" type="appl" />
         <tli id="T84" time="78.18628441445229" />
         <tli id="T85" time="79.449" type="appl" />
         <tli id="T86" time="80.617" type="appl" />
         <tli id="T87" time="81.785" type="appl" />
         <tli id="T88" time="82.953" type="appl" />
         <tli id="T89" time="84.121" type="appl" />
         <tli id="T90" time="85.288" type="appl" />
         <tli id="T91" time="86.456" type="appl" />
         <tli id="T92" time="87.624" type="appl" />
         <tli id="T93" time="88.792" type="appl" />
         <tli id="T94" time="89.96" type="appl" />
         <tli id="T95" time="91.73955148595479" />
         <tli id="T96" time="98.383" type="appl" />
         <tli id="T97" time="99.328" type="appl" />
         <tli id="T98" time="100.099" type="appl" />
         <tli id="T99" time="100.87" type="appl" />
         <tli id="T100" time="101.64" type="appl" />
         <tli id="T101" time="102.41" type="appl" />
         <tli id="T102" time="104.29282344642662" />
         <tli id="T103" time="105.13281933969239" />
         <tli id="T104" time="105.92" type="appl" />
         <tli id="T105" time="106.73" type="appl" />
         <tli id="T106" time="107.54" type="appl" />
         <tli id="T107" time="108.349" type="appl" />
         <tli id="T108" time="109.159" type="appl" />
         <tli id="T109" time="110.81279157034668" />
         <tli id="T110" time="113.33944588421753" />
         <tli id="T111" time="114.759" type="appl" />
         <tli id="T112" time="116.104" type="appl" />
         <tli id="T113" time="117.45" type="appl" />
         <tli id="T114" time="118.68167237027514" />
         <tli id="T115" time="119.491" type="appl" />
         <tli id="T116" time="120.184" type="appl" />
         <tli id="T117" time="120.876" type="appl" />
         <tli id="T118" time="121.568" type="appl" />
         <tli id="T119" time="122.26" type="appl" />
         <tli id="T120" time="122.952" type="appl" />
         <tli id="T121" time="123.645" type="appl" />
         <tli id="T122" time="124.337" type="appl" />
         <tli id="T123" time="125.186" type="appl" />
         <tli id="T124" time="126.022" type="appl" />
         <tli id="T125" time="126.858" type="appl" />
         <tli id="T126" time="127.693" type="appl" />
         <tli id="T127" time="128.528" type="appl" />
         <tli id="T128" time="129.45936707403212" />
         <tli id="T129" time="129.915" type="appl" />
         <tli id="T130" time="130.452" type="appl" />
         <tli id="T131" time="130.99" type="appl" />
         <tli id="T132" time="131.527" type="appl" />
         <tli id="T133" time="132.55" type="appl" />
         <tli id="T134" time="133.574" type="appl" />
         <tli id="T135" time="134.597" type="appl" />
         <tli id="T136" time="135.621" type="appl" />
         <tli id="T137" time="136.644" type="appl" />
         <tli id="T138" time="137.668" type="appl" />
         <tli id="T139" time="138.691" type="appl" />
         <tli id="T140" time="139.715" type="appl" />
         <tli id="T141" time="140.738" type="appl" />
         <tli id="T142" time="141.762" type="appl" />
         <tli id="T143" time="144.66595939577203" />
         <tli id="T144" time="145.584" type="appl" />
         <tli id="T145" time="146.292" type="appl" />
         <tli id="T146" time="147.0" type="appl" />
         <tli id="T147" time="147.74" type="appl" />
         <tli id="T148" time="148.48" type="appl" />
         <tli id="T149" time="149.22" type="appl" />
         <tli id="T150" time="149.959" type="appl" />
         <tli id="T151" time="150.699" type="appl" />
         <tli id="T152" time="151.439" type="appl" />
         <tli id="T153" time="152.179" type="appl" />
         <tli id="T154" time="153.118" type="appl" />
         <tli id="T155" time="153.933" type="appl" />
         <tli id="T156" time="154.748" type="appl" />
         <tli id="T157" time="155.564" type="appl" />
         <tli id="T158" time="156.379" type="appl" />
         <tli id="T159" time="157.194" type="appl" />
         <tli id="T160" time="160.25921649377713" />
         <tli id="T161" time="161.36" type="appl" />
         <tli id="T162" time="162.15" type="appl" />
         <tli id="T163" time="162.656" type="appl" />
         <tli id="T164" time="163.163" type="appl" />
         <tli id="T165" time="163.669" type="appl" />
         <tli id="T166" time="164.175" type="appl" />
         <tli id="T167" time="164.682" type="appl" />
         <tli id="T168" time="168.06584499389" />
         <tli id="T169" time="168.51250947681703" />
         <tli id="T170" time="169.124" type="appl" />
         <tli id="T171" time="169.808" type="appl" />
         <tli id="T172" time="170.491" type="appl" />
         <tli id="T173" time="171.175" type="appl" />
         <tli id="T174" time="172.072" type="appl" />
         <tli id="T175" time="172.97" type="appl" />
         <tli id="T176" time="173.867" type="appl" />
         <tli id="T177" time="174.764" type="appl" />
         <tli id="T178" time="175.662" type="appl" />
         <tli id="T179" time="179.1124576537423" />
         <tli id="T180" time="179.866" type="appl" />
         <tli id="T181" time="180.513" type="appl" />
         <tli id="T182" time="181.161" type="appl" />
         <tli id="T183" time="181.808" type="appl" />
         <tli id="T184" time="182.456" type="appl" />
         <tli id="T185" time="183.103" type="appl" />
         <tli id="T186" time="185.56575943692695" />
         <tli id="T187" time="186.362" type="appl" />
         <tli id="T188" time="187.055" type="appl" />
         <tli id="T189" time="188.98574271665188" />
         <tli id="T190" time="189.804" type="appl" />
         <tli id="T191" time="190.463" type="appl" />
         <tli id="T192" time="191.122" type="appl" />
         <tli id="T193" time="191.781" type="appl" />
         <tli id="T194" time="192.44" type="appl" />
         <tli id="T195" time="193.099" type="appl" />
         <tli id="T196" time="195.42571123168946" />
         <tli id="T197" time="196.082" type="appl" />
         <tli id="T198" time="196.741" type="appl" />
         <tli id="T199" time="197.401" type="appl" />
         <tli id="T200" time="198.061" type="appl" />
         <tli id="T201" time="198.72" type="appl" />
         <tli id="T202" time="199.69902367282725" />
         <tli id="T203" time="200.613" type="appl" />
         <tli id="T204" time="201.447" type="appl" />
         <tli id="T205" time="202.28" type="appl" />
         <tli id="T206" time="203.114" type="appl" />
         <tli id="T207" time="206.51899033005648" />
         <tli id="T208" time="207.462" type="appl" />
         <tli id="T209" time="208.183" type="appl" />
         <tli id="T210" time="208.904" type="appl" />
         <tli id="T211" time="209.625" type="appl" />
         <tli id="T212" time="210.346" type="appl" />
         <tli id="T213" time="211.068" type="appl" />
         <tli id="T214" time="211.789" type="appl" />
         <tli id="T215" time="212.51" type="appl" />
         <tli id="T216" time="213.231" type="appl" />
         <tli id="T217" time="213.952" type="appl" />
         <tli id="T218" time="214.636" type="appl" />
         <tli id="T219" time="215.282" type="appl" />
         <tli id="T220" time="216.9189394847756" />
         <tli id="T221" time="217.558" type="appl" />
         <tli id="T222" time="218.12" type="appl" />
         <tli id="T223" time="218.682" type="appl" />
         <tli id="T224" time="219.243" type="appl" />
         <tli id="T225" time="219.805" type="appl" />
         <tli id="T226" time="220.366" type="appl" />
         <tli id="T227" time="220.928" type="appl" />
         <tli id="T228" time="223.0989092709452" />
         <tli id="T229" time="223.55890702201935" />
         <tli id="T230" time="224.288" type="appl" />
         <tli id="T231" time="225.034" type="appl" />
         <tli id="T232" time="225.779" type="appl" />
         <tli id="T233" time="226.524" type="appl" />
         <tli id="T234" time="227.27" type="appl" />
         <tli id="T235" time="228.1988843372017" />
         <tli id="T236" time="229.41887837265915" />
         <tli id="T237" time="230.087" type="appl" />
         <tli id="T238" time="230.89" type="appl" />
         <tli id="T239" time="231.693" type="appl" />
         <tli id="T240" time="232.496" type="appl" />
         <tli id="T241" time="233.299" type="appl" />
         <tli id="T242" time="234.102" type="appl" />
         <tli id="T243" time="234.905" type="appl" />
         <tli id="T244" time="235.798" type="appl" />
         <tli id="T245" time="236.566" type="appl" />
         <tli id="T246" time="237.335" type="appl" />
         <tli id="T247" time="238.104" type="appl" />
         <tli id="T248" time="238.873" type="appl" />
         <tli id="T249" time="239.641" type="appl" />
         <tli id="T250" time="240.63882351842335" />
         <tli id="T251" time="242.2121491597783" />
         <tli id="T252" time="243.128" type="appl" />
         <tli id="T253" time="243.793" type="appl" />
         <tli id="T254" time="244.459" type="appl" />
         <tli id="T255" time="245.124" type="appl" />
         <tli id="T256" time="245.79" type="appl" />
         <tli id="T257" time="246.374" type="appl" />
         <tli id="T258" time="246.959" type="appl" />
         <tli id="T259" time="247.543" type="appl" />
         <tli id="T260" time="248.127" type="appl" />
         <tli id="T261" time="249.505" type="appl" />
         <tli id="T262" time="250.062" type="appl" />
         <tli id="T263" time="250.75210740798033" />
         <tli id="T264" time="251.925435004923" />
         <tli id="T265" time="252.99" type="appl" />
         <tli id="T266" time="254.024" type="appl" />
         <tli id="T267" time="255.059" type="appl" />
         <tli id="T268" time="256.34541339567863" />
         <tli id="T269" time="256.902" type="appl" />
         <tli id="T270" time="257.711" type="appl" />
         <tli id="T271" time="258.52" type="appl" />
         <tli id="T272" time="263.11871361439313" />
         <tli id="T273" time="264.06" type="appl" />
         <tli id="T274" time="264.969" type="appl" />
         <tli id="T275" time="265.878" type="appl" />
         <tli id="T276" time="266.786" type="appl" />
         <tli id="T277" time="267.695" type="appl" />
         <tli id="T278" time="268.604" type="appl" />
         <tli id="T279" time="269.19" type="appl" />
         <tli id="T280" time="269.775" type="appl" />
         <tli id="T281" time="270.361" type="appl" />
         <tli id="T282" time="270.947" type="appl" />
         <tli id="T283" time="271.506" type="appl" />
         <tli id="T284" time="272.32533526994894" />
         <tli id="T285" time="273.24" type="appl" />
         <tli id="T286" time="273.87" type="appl" />
         <tli id="T287" time="274.5" type="appl" />
         <tli id="T288" time="275.13" type="appl" />
         <tli id="T289" time="275.76" type="appl" />
         <tli id="T290" time="276.39" type="appl" />
         <tli id="T291" time="277.06666625628054" />
         <tli id="T292" time="277.736" type="appl" />
         <tli id="T293" time="278.445" type="appl" />
         <tli id="T294" time="279.155" type="appl" />
         <tli id="T295" time="279.864" type="appl" />
         <tli id="T296" time="280.574" type="appl" />
         <tli id="T297" time="281.283" type="appl" />
         <tli id="T298" time="281.993" type="appl" />
         <tli id="T299" time="282.932" type="appl" />
         <tli id="T300" time="284.085" type="appl" />
         <tli id="T301" time="285.102" type="appl" />
         <tli id="T302" time="286.118" type="appl" />
         <tli id="T303" time="287.135" type="appl" />
         <tli id="T304" time="289.3252521574705" />
         <tli id="T305" time="290.129" type="appl" />
         <tli id="T306" time="290.804" type="appl" />
         <tli id="T307" time="291.478" type="appl" />
         <tli id="T308" time="292.152" type="appl" />
         <tli id="T309" time="292.826" type="appl" />
         <tli id="T310" time="293.5" type="appl" />
         <tli id="T311" time="294.175" type="appl" />
         <tli id="T312" time="294.90522487702174" />
         <tli id="T313" time="295.776" type="appl" />
         <tli id="T314" time="296.573" type="appl" />
         <tli id="T315" time="297.37" type="appl" />
         <tli id="T316" time="298.191" type="appl" />
         <tli id="T317" time="299.011" type="appl" />
         <tli id="T318" time="299.832" type="appl" />
         <tli id="T319" time="300.652" type="appl" />
         <tli id="T320" time="301.473" type="appl" />
         <tli id="T321" time="302.293" type="appl" />
         <tli id="T322" time="303.99184711925386" />
         <tli id="T323" time="304.905" type="appl" />
         <tli id="T324" time="305.792" type="appl" />
         <tli id="T325" time="306.678" type="appl" />
         <tli id="T326" time="307.564" type="appl" />
         <tli id="T327" time="308.45" type="appl" />
         <tli id="T328" time="309.337" type="appl" />
         <tli id="T329" time="312.04514108008766" />
         <tli id="T330" time="313.163" type="appl" />
         <tli id="T331" time="314.221" type="appl" />
         <tli id="T332" time="315.57179050498917" />
         <tli id="T333" time="316.453" type="appl" />
         <tli id="T334" time="317.474" type="appl" />
         <tli id="T335" time="318.496" type="appl" />
         <tli id="T336" time="319.517" type="appl" />
         <tli id="T337" time="321.66509404820283" />
         <tli id="T338" time="323.00508749698395" />
         <tli id="T339" time="323.533" type="appl" />
         <tli id="T340" time="324.108" type="appl" />
         <tli id="T341" time="324.682" type="appl" />
         <tli id="T342" time="325.256" type="appl" />
         <tli id="T343" time="325.83" type="appl" />
         <tli id="T344" time="326.405" type="appl" />
         <tli id="T345" time="327.03840111147434" />
         <tli id="T346" time="328.02506295435796" />
         <tli id="T347" time="328.604" type="appl" />
         <tli id="T348" time="329.352" type="appl" />
         <tli id="T349" time="330.20505229640486" />
         <tli id="T350" time="330.697" type="appl" />
         <tli id="T351" time="331.137" type="appl" />
         <tli id="T352" time="331.577" type="appl" />
         <tli id="T353" time="332.016" type="appl" />
         <tli id="T354" time="332.456" type="appl" />
         <tli id="T355" time="332.896" type="appl" />
         <tli id="T356" time="333.336" type="appl" />
         <tli id="T357" time="333.9783671820273" />
         <tli id="T358" time="334.435" type="appl" />
         <tli id="T359" time="334.934" type="appl" />
         <tli id="T360" time="335.7316919433677" />
         <tli id="T361" time="336.715" type="appl" />
         <tli id="T362" time="337.297" type="appl" />
         <tli id="T363" time="338.9250096645924" />
         <tli id="T364" time="339.912" type="appl" />
         <tli id="T365" time="340.701" type="appl" />
         <tli id="T366" time="341.49" type="appl" />
         <tli id="T367" time="342.278" type="appl" />
         <tli id="T368" time="343.066" type="appl" />
         <tli id="T369" time="343.855" type="appl" />
         <tli id="T370" time="344.306" type="appl" />
         <tli id="T371" time="344.757" type="appl" />
         <tli id="T372" time="345.208" type="appl" />
         <tli id="T373" time="345.90497553958653" />
         <tli id="T374" time="348.40496331716326" />
         <tli id="T375" time="349.266" type="appl" />
         <tli id="T376" time="350.089" type="appl" />
         <tli id="T377" time="350.912" type="appl" />
         <tli id="T378" time="351.736" type="appl" />
         <tli id="T379" time="352.559" type="appl" />
         <tli id="T380" time="353.7916036485819" />
         <tli id="T381" time="354.354" type="appl" />
         <tli id="T382" time="355.272" type="appl" />
         <tli id="T383" time="356.19" type="appl" />
         <tli id="T384" time="357.108" type="appl" />
         <tli id="T385" time="358.026" type="appl" />
         <tli id="T386" time="359.0649112007503" />
         <tli id="T387" time="360.33823830879606" />
         <tli id="T388" time="361.053" type="appl" />
         <tli id="T389" time="361.623" type="appl" />
         <tli id="T390" time="362.194" type="appl" />
         <tli id="T391" time="362.764" type="appl" />
         <tli id="T392" time="364.1315530966391" />
         <tli id="T393" time="364.79" type="appl" />
         <tli id="T394" time="365.332" type="appl" />
         <tli id="T395" time="365.874" type="appl" />
         <tli id="T396" time="366.416" type="appl" />
         <tli id="T397" time="367.50486993784926" />
         <tli id="T398" time="368.224" type="appl" />
         <tli id="T399" time="368.727" type="appl" />
         <tli id="T400" time="369.23" type="appl" />
         <tli id="T401" time="369.981" type="appl" />
         <tli id="T402" time="370.554" type="appl" />
         <tli id="T403" time="371.128" type="appl" />
         <tli id="T404" time="371.701" type="appl" />
         <tli id="T405" time="372.3915127137525" />
         <tli id="T406" time="373.119" type="appl" />
         <tli id="T407" time="373.614" type="appl" />
         <tli id="T408" time="374.11" type="appl" />
         <tli id="T409" time="375.37816477869745" />
         <tli id="T410" time="375.95816194309526" />
         <tli id="T411" time="376.587" type="appl" />
         <tli id="T412" time="377.306" type="appl" />
         <tli id="T413" time="378.024" type="appl" />
         <tli id="T414" time="378.742" type="appl" />
         <tli id="T415" time="379.461" type="appl" />
         <tli id="T416" time="380.7248053056749" />
         <tli id="T417" time="381.971" type="appl" />
         <tli id="T418" time="382.756" type="appl" />
         <tli id="T419" time="383.54" type="appl" />
         <tli id="T420" time="384.325" type="appl" />
         <tli id="T421" time="384.925" type="appl" />
         <tli id="T422" time="385.524" type="appl" />
         <tli id="T423" time="386.124" type="appl" />
         <tli id="T424" time="386.724" type="appl" />
         <tli id="T425" time="387.323" type="appl" />
         <tli id="T426" time="387.923" type="appl" />
         <tli id="T427" time="388.522" type="appl" />
         <tli id="T428" time="392.0114167921745" />
         <tli id="T429" time="392.97807873283756" />
         <tli id="T430" time="393.408" type="appl" />
         <tli id="T431" time="393.984" type="appl" />
         <tli id="T432" time="394.56" type="appl" />
         <tli id="T433" time="395.136" type="appl" />
         <tli id="T434" time="395.712" type="appl" />
         <tli id="T435" time="396.87139303178367" />
         <tli id="T436" time="397.582" type="appl" />
         <tli id="T437" time="398.066" type="appl" />
         <tli id="T438" time="398.55" type="appl" />
         <tli id="T439" time="399.035" type="appl" />
         <tli id="T440" time="399.519" type="appl" />
         <tli id="T441" time="400.003" type="appl" />
         <tli id="T442" time="400.488" type="appl" />
         <tli id="T443" time="400.972" type="appl" />
         <tli id="T444" time="401.456" type="appl" />
         <tli id="T445" time="401.996" type="appl" />
         <tli id="T446" time="402.38" type="appl" />
         <tli id="T447" time="402.765" type="appl" />
         <tli id="T448" time="403.15" type="appl" />
         <tli id="T449" time="403.758" type="appl" />
         <tli id="T450" time="404.336" type="appl" />
         <tli id="T451" time="404.914" type="appl" />
         <tli id="T452" time="405.492" type="appl" />
         <tli id="T453" time="406.07" type="appl" />
         <tli id="T454" time="406.648" type="appl" />
         <tli id="T455" time="407.335" type="appl" />
         <tli id="T456" time="408.021" type="appl" />
         <tli id="T457" time="408.708" type="appl" />
         <tli id="T458" time="409.395" type="appl" />
         <tli id="T459" time="410.081" type="appl" />
         <tli id="T460" time="410.8179915136249" />
         <tli id="T461" time="411.773" type="appl" />
         <tli id="T462" time="412.488" type="appl" />
         <tli id="T463" time="413.063" type="appl" />
         <tli id="T464" time="413.638" type="appl" />
         <tli id="T465" time="414.213" type="appl" />
         <tli id="T466" time="414.788" type="appl" />
         <tli id="T467" time="415.83130033692544" />
         <tli id="T468" time="417.67129134122183" />
         <tli id="T469" time="419.029" type="appl" />
         <tli id="T470" time="420.511277456549" />
         <tli id="T471" time="421.101" type="appl" />
         <tli id="T472" time="421.935" type="appl" />
         <tli id="T473" time="424.3712585851274" />
         <tli id="T474" time="425.501" type="appl" />
         <tli id="T475" time="426.529" type="appl" />
         <tli id="T476" time="427.70457562189637" />
         <tli id="T477" time="428.435" type="appl" />
         <tli id="T478" time="428.993" type="appl" />
         <tli id="T479" time="429.551" type="appl" />
         <tli id="T480" time="430.108" type="appl" />
         <tli id="T481" time="430.666" type="appl" />
         <tli id="T482" time="431.224" type="appl" />
         <tli id="T483" time="431.782" type="appl" />
         <tli id="T484" time="433.35121468218296" />
         <tli id="T485" time="434.337" type="appl" />
         <tli id="T486" time="435.124" type="appl" />
         <tli id="T487" time="435.91" type="appl" />
         <tli id="T488" time="436.697" type="appl" />
         <tli id="T489" time="437.484" type="appl" />
         <tli id="T490" time="438.27" type="appl" />
         <tli id="T491" time="439.057" type="appl" />
         <tli id="T492" time="441.1511765482223" />
         <tli id="T493" time="441.977" type="appl" />
         <tli id="T494" time="442.453" type="appl" />
         <tli id="T495" time="442.93" type="appl" />
         <tli id="T496" time="443.406" type="appl" />
         <tli id="T497" time="443.882" type="appl" />
         <tli id="T498" time="444.358" type="appl" />
         <tli id="T499" time="444.835" type="appl" />
         <tli id="T500" time="445.311" type="appl" />
         <tli id="T501" time="450.8844622955876" />
         <tli id="T502" time="451.283" type="appl" />
         <tli id="T503" time="451.791" type="appl" />
         <tli id="T504" time="452.3" type="appl" />
         <tli id="T505" time="452.809" type="appl" />
         <tli id="T506" time="453.317" type="appl" />
         <tli id="T507" time="454.23777923457715" />
         <tli id="T508" time="454.972" type="appl" />
         <tli id="T509" time="455.728" type="appl" />
         <tli id="T510" time="456.483" type="appl" />
         <tli id="T511" time="457.238" type="appl" />
         <tli id="T512" time="457.994" type="appl" />
         <tli id="T513" time="458.749" type="appl" />
         <tli id="T514" time="459.505" type="appl" />
         <tli id="T515" time="460.26" type="appl" />
         <tli id="T516" time="461.015" type="appl" />
         <tli id="T517" time="461.771" type="appl" />
         <tli id="T518" time="463.95773171379534" />
         <tli id="T519" time="465.138" type="appl" />
         <tli id="T520" time="466.249" type="appl" />
         <tli id="T521" time="467.359" type="appl" />
         <tli id="T522" time="468.5910423949042" />
         <tli id="T523" time="469.514" type="appl" />
         <tli id="T524" time="470.384" type="appl" />
         <tli id="T525" time="471.254" type="appl" />
         <tli id="T526" time="472.124" type="appl" />
         <tli id="T527" time="472.994" type="appl" />
         <tli id="T528" time="473.864" type="appl" />
         <tli id="T529" time="475.5776749039719" />
         <tli id="T530" time="476.664" type="appl" />
         <tli id="T531" time="477.552" type="appl" />
         <tli id="T532" time="478.439" type="appl" />
         <tli id="T533" time="479.4776558369916" />
         <tli id="T534" time="480.987" type="appl" />
         <tli id="T535" time="481.211" type="appl" />
         <tli id="T536" time="481.6909783494062" />
         <tli id="T537" time="482.4243080974953" />
         <tli id="T538" time="482.952" type="appl" />
         <tli id="T539" time="483.566" type="appl" />
         <tli id="T540" time="484.179" type="appl" />
         <tli id="T541" time="485.2376276765283" />
         <tli id="T542" time="486.047" type="appl" />
         <tli id="T543" time="486.973" type="appl" />
         <tli id="T544" time="488.67094422440033" />
         <tli id="T545" time="489.775" type="appl" />
         <tli id="T546" time="490.801" type="appl" />
         <tli id="T547" time="491.826" type="appl" />
         <tli id="T548" time="492.852" type="appl" />
         <tli id="T549" time="493.877" type="appl" />
         <tli id="T550" time="494.902" type="appl" />
         <tli id="T551" time="495.928" type="appl" />
         <tli id="T552" time="497.2642355451906" />
         <tli id="T553" time="498.09089817030923" />
         <tli id="T554" time="498.79" type="appl" />
         <tli id="T555" time="499.8708894679439" />
         <tli id="T556" time="500.69755209306265" />
         <tli id="T557" time="501.274" type="appl" />
         <tli id="T558" time="501.912" type="appl" />
         <tli id="T559" time="502.55" type="appl" />
         <tli id="T560" time="503.189" type="appl" />
         <tli id="T561" time="503.828" type="appl" />
         <tli id="T562" time="504.466" type="appl" />
         <tli id="T563" time="505.104" type="appl" />
         <tli id="T564" time="505.89752667042217" />
         <tli id="T565" time="506.736" type="appl" />
         <tli id="T566" time="507.79085074730693" />
         <tli id="T567" time="508.593" type="appl" />
         <tli id="T568" time="509.215" type="appl" />
         <tli id="T569" time="509.837" type="appl" />
         <tli id="T570" time="510.78416944632545" />
         <tli id="T571" time="512.0774964565918" />
         <tli id="T572" time="512.814" type="appl" />
         <tli id="T573" time="513.503" type="appl" />
         <tli id="T574" time="514.191" type="appl" />
         <tli id="T575" time="514.88" type="appl" />
         <tli id="T576" time="515.568" type="appl" />
         <tli id="T577" time="516.5841410903033" />
         <tli id="T578" time="517.638" type="appl" />
         <tli id="T579" time="518.184" type="appl" />
         <tli id="T580" time="518.731" type="appl" />
         <tli id="T581" time="519.277" type="appl" />
         <tli id="T582" time="521.3907842573242" />
         <tli id="T583" time="522.139" type="appl" />
         <tli id="T584" time="522.694" type="appl" />
         <tli id="T585" time="523.249" type="appl" />
         <tli id="T586" time="523.804" type="appl" />
         <tli id="T587" time="524.359" type="appl" />
         <tli id="T588" time="524.913" type="appl" />
         <tli id="T589" time="525.468" type="appl" />
         <tli id="T590" time="526.023" type="appl" />
         <tli id="T591" time="526.578" type="appl" />
         <tli id="T592" time="527.133" type="appl" />
         <tli id="T593" time="528.2774172552889" />
         <tli id="T594" time="531.4974015128075" />
         <tli id="T595" time="532.536" type="appl" />
         <tli id="T596" time="533.356" type="appl" />
         <tli id="T597" time="534.175" type="appl" />
         <tli id="T598" time="534.994" type="appl" />
         <tli id="T599" time="535.813" type="appl" />
         <tli id="T600" time="536.633" type="appl" />
         <tli id="T601" time="538.0573694411688" />
         <tli id="T602" time="538.936" type="appl" />
         <tli id="T603" time="539.719" type="appl" />
         <tli id="T604" time="540.502" type="appl" />
         <tli id="T605" time="541.2373538942464" />
         <tli id="T606" time="542.786" type="appl" />
         <tli id="T607" time="543.431" type="appl" />
         <tli id="T608" time="544.077" type="appl" />
         <tli id="T609" time="544.723" type="appl" />
         <tli id="T610" time="545.369" type="appl" />
         <tli id="T611" time="546.014" type="appl" />
         <tli id="T612" time="546.66" type="appl" />
         <tli id="T613" time="547.559" type="appl" />
         <tli id="T614" time="548.316" type="appl" />
         <tli id="T615" time="549.073" type="appl" />
         <tli id="T616" time="549.83" type="appl" />
         <tli id="T617" time="550.7506407171851" />
         <tli id="T618" time="551.634" type="appl" />
         <tli id="T619" time="552.364" type="appl" />
         <tli id="T620" time="553.095" type="appl" />
         <tli id="T621" time="553.825" type="appl" />
         <tli id="T622" time="554.8306207701903" />
         <tli id="T623" time="557.7906062988411" />
         <tli id="T624" time="558.858" type="appl" />
         <tli id="T625" time="559.75" type="appl" />
         <tli id="T626" time="560.643" type="appl" />
         <tli id="T627" time="561.536" type="appl" />
         <tli id="T628" time="562.429" type="appl" />
         <tli id="T629" time="563.322" type="appl" />
         <tli id="T630" time="564.214" type="appl" />
         <tli id="T631" time="566.3772309855578" />
         <tli id="T632" time="568.232" type="appl" />
         <tli id="T633" time="569.103" type="appl" />
         <tli id="T634" time="569.973" type="appl" />
         <tli id="T635" time="570.844" type="appl" />
         <tli id="T636" time="571.715" type="appl" />
         <tli id="T637" time="572.52" type="appl" />
         <tli id="T638" time="573.149" type="appl" />
         <tli id="T639" time="573.777" type="appl" />
         <tli id="T640" time="574.405" type="appl" />
         <tli id="T641" time="575.034" type="appl" />
         <tli id="T642" time="575.662" type="appl" />
         <tli id="T643" time="576.291" type="appl" />
         <tli id="T644" time="576.919" type="appl" />
         <tli id="T645" time="577.547" type="appl" />
         <tli id="T646" time="578.176" type="appl" />
         <tli id="T647" time="579.1771684067505" />
         <tli id="T648" time="583.0771493397704" />
         <tli id="T649" time="586.2504671587743" />
         <tli id="T650" time="587.759" type="appl" />
         <tli id="T651" time="589.268" type="appl" />
         <tli id="T652" time="590.8771112058096" />
         <tli id="T653" time="591.865" type="appl" />
         <tli id="T654" time="592.622" type="appl" />
         <tli id="T655" time="593.38" type="appl" />
         <tli id="T656" time="594.137" type="appl" />
         <tli id="T657" time="594.895" type="appl" />
         <tli id="T658" time="595.652" type="appl" />
         <tli id="T659" time="596.5370835342433" />
         <tli id="T660" time="597.275" type="appl" />
         <tli id="T661" time="597.958" type="appl" />
         <tli id="T662" time="598.64" type="appl" />
         <tli id="T663" time="599.322" type="appl" />
         <tli id="T664" time="600.6503967576161" />
         <tli id="T665" time="601.664" type="appl" />
         <tli id="T666" time="602.46" type="appl" />
         <tli id="T667" time="603.114" type="appl" />
         <tli id="T668" time="603.767" type="appl" />
         <tli id="T669" time="604.421" type="appl" />
         <tli id="T670" time="605.5370395335194" />
         <tli id="T671" time="606.842" type="appl" />
         <tli id="T672" time="608.2503596014493" />
         <tli id="T673" time="609.2903545169211" />
         <tli id="T674" time="610.264" type="appl" />
         <tli id="T675" time="611.106" type="appl" />
         <tli id="T676" time="611.8970084396746" />
         <tli id="T677" time="612.468" type="appl" />
         <tli id="T678" time="613.179" type="appl" />
         <tli id="T679" time="613.89" type="appl" />
         <tli id="T680" time="614.601" type="appl" />
         <tli id="T681" time="615.312" type="appl" />
         <tli id="T682" time="616.023" type="appl" />
         <tli id="T683" time="616.805" type="appl" />
         <tli id="T684" time="617.487" type="appl" />
         <tli id="T685" time="618.168" type="appl" />
         <tli id="T686" time="619.044" type="appl" />
         <tli id="T687" time="619.789" type="appl" />
         <tli id="T688" time="620.534" type="appl" />
         <tli id="T689" time="621.28" type="appl" />
         <tli id="T690" time="622.025" type="appl" />
         <tli id="T691" time="622.77" type="appl" />
         <tli id="T692" time="623.348" type="appl" />
         <tli id="T693" time="623.926" type="appl" />
         <tli id="T694" time="624.784" type="appl" />
         <tli id="T695" time="625.52" type="appl" />
         <tli id="T696" time="626.256" type="appl" />
         <tli id="T697" time="626.991" type="appl" />
         <tli id="T698" time="627.726" type="appl" />
         <tli id="T699" time="628.462" type="appl" />
         <tli id="T700" time="629.198" type="appl" />
         <tli id="T701" time="630.1702524352419" />
         <tli id="T702" time="630.892" type="appl" />
         <tli id="T703" time="631.425" type="appl" />
         <tli id="T704" time="631.958" type="appl" />
         <tli id="T705" time="633.1169046957456" />
         <tli id="T706" time="633.955" type="appl" />
         <tli id="T707" time="634.659" type="appl" />
         <tli id="T708" time="635.362" type="appl" />
         <tli id="T709" time="636.065" type="appl" />
         <tli id="T710" time="636.768" type="appl" />
         <tli id="T711" time="637.472" type="appl" />
         <tli id="T712" time="638.4168787842082" />
         <tli id="T713" time="639.044" type="appl" />
         <tli id="T714" time="639.763" type="appl" />
         <tli id="T715" time="640.7168675395787" />
         <tli id="T716" time="641.237" type="appl" />
         <tli id="T717" time="641.891" type="appl" />
         <tli id="T718" time="642.545" type="appl" />
         <tli id="T719" time="643.198" type="appl" />
         <tli id="T720" time="643.852" type="appl" />
         <tli id="T721" time="644.506" type="appl" />
         <tli id="T722" time="645.16" type="appl" />
         <tli id="T723" time="645.712" type="appl" />
         <tli id="T724" time="646.254" type="appl" />
         <tli id="T725" time="646.796" type="appl" />
         <tli id="T726" time="647.338" type="appl" />
         <tli id="T727" time="647.879" type="appl" />
         <tli id="T728" time="648.421" type="appl" />
         <tli id="T729" time="648.963" type="appl" />
         <tli id="T730" time="649.370158567031" />
         <tli id="T731" time="650.5701527002677" />
         <tli id="T732" time="651.239" type="appl" />
         <tli id="T733" time="651.898" type="appl" />
         <tli id="T734" time="652.557" type="appl" />
         <tli id="T735" time="653.393" type="appl" />
         <tli id="T736" time="654.108" type="appl" />
         <tli id="T737" time="654.823" type="appl" />
         <tli id="T738" time="655.538" type="appl" />
         <tli id="T739" time="656.253" type="appl" />
         <tli id="T740" time="656.968" type="appl" />
         <tli id="T741" time="657.683" type="appl" />
         <tli id="T742" time="658.398" type="appl" />
         <tli id="T743" time="659.113" type="appl" />
         <tli id="T744" time="659.828" type="appl" />
         <tli id="T745" time="660.543" type="appl" />
         <tli id="T746" time="661.8300976504732" />
         <tli id="T747" time="662.9167590044598" />
         <tli id="T748" time="663.72" type="appl" />
         <tli id="T749" time="664.427" type="appl" />
         <tli id="T750" time="665.9234109716255" />
         <tli id="T751" time="667.262" type="appl" />
         <tli id="T752" time="668.486" type="appl" />
         <tli id="T753" time="670.0100576587042" />
         <tli id="T754" time="671.132" type="appl" />
         <tli id="T755" time="671.91" type="appl" />
         <tli id="T756" time="674.3433698065038" />
         <tli id="T757" time="675.517" type="appl" />
         <tli id="T758" time="676.322" type="appl" />
         <tli id="T759" time="677.4766878210665" />
         <tli id="T760" time="678.657" type="appl" />
         <tli id="T761" time="679.399" type="appl" />
         <tli id="T762" time="680.141" type="appl" />
         <tli id="T763" time="680.883" type="appl" />
         <tli id="T764" time="681.625" type="appl" />
         <tli id="T765" time="682.386" type="appl" />
         <tli id="T766" time="683.004" type="appl" />
         <tli id="T767" time="683.623" type="appl" />
         <tli id="T768" time="684.242" type="appl" />
         <tli id="T769" time="684.86" type="appl" />
         <tli id="T770" time="685.6966476337388" />
         <tli id="T771" time="687.9499699505946" />
         <tli id="T772" time="688.881" type="appl" />
         <tli id="T773" time="689.498" type="appl" />
         <tli id="T774" time="690.116" type="appl" />
         <tli id="T775" time="690.733" type="appl" />
         <tli id="T776" time="691.351" type="appl" />
         <tli id="T777" time="691.968" type="appl" />
         <tli id="T778" time="692.676613508733" />
         <tli id="T779" time="693.559" type="appl" />
         <tli id="T780" time="694.623270658206" />
         <tli id="T781" time="695.524" type="appl" />
         <tli id="T782" time="696.6299275143409" />
         <tli id="T783" time="697.784" type="appl" />
         <tli id="T784" time="699.0432490489617" />
         <tli id="T785" time="702.7232310575546" />
         <tli id="T786" time="703.26" type="appl" />
         <tli id="T787" time="703.887" type="appl" />
         <tli id="T788" time="706.2232139461619" />
         <tli id="T789" time="707.366" type="appl" />
         <tli id="T790" time="708.6432021148562" />
         <tli id="T791" time="709.05" type="appl" />
         <tli id="T792" time="709.551" type="appl" />
         <tli id="T793" time="710.051" type="appl" />
         <tli id="T794" time="710.551" type="appl" />
         <tli id="T795" time="711.052" type="appl" />
         <tli id="T796" time="711.6698539842424" />
         <tli id="T797" time="712.7431820700821" />
         <tli id="T798" time="713.295" type="appl" />
         <tli id="T799" time="713.859" type="appl" />
         <tli id="T800" time="714.423" type="appl" />
         <tli id="T801" time="714.986" type="appl" />
         <tli id="T802" time="715.55" type="appl" />
         <tli id="T803" time="716.114" type="appl" />
         <tli id="T804" time="716.678" type="appl" />
         <tli id="T805" time="717.242" type="appl" />
         <tli id="T806" time="718.278" type="appl" />
         <tli id="T807" time="719.17" type="appl" />
         <tli id="T808" time="720.062" type="appl" />
         <tli id="T809" time="720.997" type="appl" />
         <tli id="T810" time="721.802" type="appl" />
         <tli id="T811" time="722.652" type="appl" />
         <tli id="T812" time="723.326" type="appl" />
         <tli id="T813" time="723.999" type="appl" />
         <tli id="T814" time="724.673" type="appl" />
         <tli id="T815" time="725.6764521727455" />
         <tli id="T816" time="726.598" type="appl" />
         <tli id="T817" time="727.396" type="appl" />
         <tli id="T818" time="728.193" type="appl" />
         <tli id="T819" time="728.99" type="appl" />
         <tli id="T820" time="729.788" type="appl" />
         <tli id="T821" time="730.8497602138109" />
         <tli id="T822" time="731.772" type="appl" />
         <tli id="T823" time="732.618" type="appl" />
         <tli id="T824" time="733.346" type="appl" />
         <tli id="T825" time="734.074" type="appl" />
         <tli id="T826" time="734.802" type="appl" />
         <tli id="T827" time="735.53" type="appl" />
         <tli id="T828" time="736.258" type="appl" />
         <tli id="T829" time="737.3363951673633" />
         <tli id="T830" time="738.263057303585" />
         <tli id="T831" time="738.977" type="appl" />
         <tli id="T832" time="739.563" type="appl" />
         <tli id="T833" time="740.148" type="appl" />
         <tli id="T834" time="740.734" type="appl" />
         <tli id="T835" time="741.4563750248097" />
         <tli id="T836" time="741.847" type="appl" />
         <tli id="T837" time="742.252" type="appl" />
         <tli id="T838" time="742.657" type="appl" />
         <tli id="T839" time="743.062" type="appl" />
         <tli id="T840" time="743.903029729798" />
         <tli id="T841" time="744.3163610423575" />
         <tli id="T842" time="744.974" type="appl" />
         <tli id="T843" time="745.65" type="appl" />
         <tli id="T844" time="746.325" type="appl" />
         <tli id="T845" time="747.001" type="appl" />
         <tli id="T846" time="747.677" type="appl" />
         <tli id="T847" time="748.353" type="appl" />
         <tli id="T848" time="749.029" type="appl" />
         <tli id="T849" time="749.705" type="appl" />
         <tli id="T850" time="750.38" type="appl" />
         <tli id="T851" time="751.056" type="appl" />
         <tli id="T852" time="751.732" type="appl" />
         <tli id="T853" time="752.84" type="appl" />
         <tli id="T854" time="753.861" type="appl" />
         <tli id="T855" time="754.883" type="appl" />
         <tli id="T856" time="756.04" type="appl" />
         <tli id="T857" time="757.0429654887413" />
         <tli id="T858" time="757.5496296783301" />
         <tli id="T859" time="758.183" type="appl" />
         <tli id="T860" time="758.899" type="appl" />
         <tli id="T861" time="759.615" type="appl" />
         <tli id="T862" time="760.331" type="appl" />
         <tli id="T863" time="761.047" type="appl" />
         <tli id="T864" time="761.5296102202323" />
         <tli id="T865" time="762.171" type="appl" />
         <tli id="T866" time="762.742" type="appl" />
         <tli id="T867" time="763.312" type="appl" />
         <tli id="T868" time="763.883" type="appl" />
         <tli id="T869" time="764.454" type="appl" />
         <tli id="T870" time="765.025" type="appl" />
         <tli id="T871" time="765.596" type="appl" />
         <tli id="T872" time="766.167" type="appl" />
         <tli id="T873" time="766.738" type="appl" />
         <tli id="T874" time="767.308" type="appl" />
         <tli id="T875" time="767.879" type="appl" />
         <tli id="T876" time="769.7695699351251" />
         <tli id="T877" time="770.762" type="appl" />
         <tli id="T878" time="771.698" type="appl" />
         <tli id="T879" time="772.634" type="appl" />
         <tli id="T880" time="773.34" type="appl" />
         <tli id="T881" time="773.925" type="appl" />
         <tli id="T882" time="774.509" type="appl" />
         <tli id="T883" time="775.094" type="appl" />
         <tli id="T884" time="776.176205279795" />
         <tli id="T885" time="777.987" type="appl" />
         <tli id="T886" time="778.99" type="appl" />
         <tli id="T887" time="779.904" type="appl" />
         <tli id="T888" time="780.566" type="appl" />
         <tli id="T889" time="781.227" type="appl" />
         <tli id="T890" time="781.889" type="appl" />
         <tli id="T891" time="782.55" type="appl" />
         <tli id="T892" time="783.56" type="appl" />
         <tli id="T893" time="786.0628236108516" />
         <tli id="T894" time="787.035" type="appl" />
         <tli id="T895" time="787.756" type="appl" />
         <tli id="T896" time="788.478" type="appl" />
         <tli id="T897" time="789.199" type="appl" />
         <tli id="T898" time="789.92" type="appl" />
         <tli id="T899" time="790.641" type="appl" />
         <tli id="T900" time="791.362" type="appl" />
         <tli id="T901" time="792.084" type="appl" />
         <tli id="T902" time="792.805" type="appl" />
         <tli id="T903" time="794.5361155183184" />
         <tli id="T904" time="795.9161087715407" />
         <tli id="T905" time="796.928" type="appl" />
         <tli id="T906" time="797.741" type="appl" />
         <tli id="T907" time="798.554" type="appl" />
         <tli id="T908" time="799.366" type="appl" />
         <tli id="T909" time="800.178" type="appl" />
         <tli id="T910" time="801.2160828600032" />
         <tli id="T911" time="802.4894099680489" />
         <tli id="T912" time="804.176068388654" />
         <tli id="T913" time="805.3" type="appl" />
         <tli id="T914" time="806.233" type="appl" />
         <tli id="T915" time="807.166" type="appl" />
         <tli id="T916" time="808.099" type="appl" />
         <tli id="T917" time="809.032" type="appl" />
         <tli id="T918" time="809.965" type="appl" />
         <tli id="T919" time="810.898" type="appl" />
         <tli id="T920" time="812.4493612739147" />
         <tli id="T921" time="813.063" type="appl" />
         <tli id="T922" time="813.727" type="appl" />
         <tli id="T923" time="814.4693513981966" />
         <tli id="T924" time="814.854" type="appl" />
         <tli id="T925" time="815.256" type="appl" />
         <tli id="T926" time="815.657" type="appl" />
         <tli id="T927" time="816.059" type="appl" />
         <tli id="T928" time="816.46" type="appl" />
         <tli id="T929" time="816.862" type="appl" />
         <tli id="T930" time="817.7760018986714" />
         <tli id="T931" time="820.6493211843663" />
         <tli id="T932" time="822.6093116019863" />
         <tli id="T933" time="823.307" type="appl" />
         <tli id="T934" time="823.948" type="appl" />
         <tli id="T935" time="824.588" type="appl" />
         <tli id="T936" time="825.229" type="appl" />
         <tli id="T937" time="825.87" type="appl" />
         <tli id="T938" time="826.736" type="appl" />
         <tli id="T939" time="827.529" type="appl" />
         <tli id="T940" time="828.322" type="appl" />
         <tli id="T941" time="829.114" type="appl" />
         <tli id="T942" time="830.027" type="appl" />
         <tli id="T943" time="830.723" type="appl" />
         <tli id="T944" time="831.8492664279098" />
         <tli id="T945" time="832.866" type="appl" />
         <tli id="T946" time="833.762" type="appl" />
         <tli id="T947" time="834.658" type="appl" />
         <tli id="T948" time="835.554" type="appl" />
         <tli id="T949" time="837.5692384630053" />
         <tli id="T950" time="838.573" type="appl" />
         <tli id="T951" time="839.451" type="appl" />
         <tli id="T952" time="840.7958893545976" />
         <tli id="T953" time="842.076" type="appl" />
         <tli id="T954" time="843.106" type="appl" />
         <tli id="T955" time="844.136" type="appl" />
         <tli id="T956" time="845.165" type="appl" />
         <tli id="T957" time="846.195" type="appl" />
         <tli id="T958" time="847.224" type="appl" />
         <tli id="T959" time="848.254" type="appl" />
         <tli id="T960" time="850.1558435938448" />
         <tli id="T961" time="854.113" type="appl" />
         <tli id="T962" time="856.467" type="appl" />
         <tli id="T963" time="856.515" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klaudia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">kaheksas</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_17" n="sc" s="T0">
               <ts e="T4" id="Seg_19" n="e" s="T0">Klaudia Plotnikova, kaheksas lint. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_20" s="T0">PKZ_1964_SU0211.KA.001 (001)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_21" s="T0">Klaudia Plotnikova, kaheksas lint. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_22" s="T0">EST:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_23" s="T0">Клавдия Плотникова, восьмая пленка. </ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_24" s="T0">Klavdiya Plotnikova, eighth tape.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_25" s="T0">Klavdija Plotnikova, achtes band.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T400" start="T397">
            <tli id="T397.tx-PKZ.1" />
            <tli id="T397.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T426" start="T425">
            <tli id="T425.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T536" start="T533">
            <tli id="T533.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T875" start="T874">
            <tli id="T874.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T962" id="Seg_26" n="sc" s="T4">
               <ts e="T8" id="Seg_28" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_30" n="HIAT:w" s="T4">Măn</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_33" n="HIAT:w" s="T5">teinen</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_36" n="HIAT:w" s="T6">sumuranə</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_39" n="HIAT:w" s="T7">mĭmbiem</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_43" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_45" n="HIAT:w" s="T8">Dĭn</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_48" n="HIAT:w" s="T9">nüke</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_51" n="HIAT:w" s="T10">amnolaʔbə</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_55" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_57" n="HIAT:w" s="T11">Amaʔ</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_61" n="HIAT:w" s="T12">amoraʔ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_64" n="HIAT:w" s="T13">kăpusta</ts>
                  <nts id="Seg_65" n="HIAT:ip">!</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_68" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_70" n="HIAT:w" s="T14">Oj</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_74" n="HIAT:w" s="T15">ugandə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_77" n="HIAT:w" s="T16">namzəga</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_81" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_83" n="HIAT:w" s="T17">Em</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_86" n="HIAT:w" s="T18">amaʔ</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_90" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_92" n="HIAT:w" s="T19">Dĭgəttə</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_95" n="HIAT:w" s="T20">koʔbdo</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_98" n="HIAT:w" s="T21">kambi</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_101" n="HIAT:w" s="T22">tüʔsittə</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_105" n="HIAT:w" s="T23">men</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_109" n="HIAT:w" s="T24">dĭnziʔ</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_114" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_116" n="HIAT:w" s="T25">Măn</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_118" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_120" n="HIAT:w" s="T26">m-</ts>
                  <nts id="Seg_121" n="HIAT:ip">)</nts>
                  <nts id="Seg_122" n="HIAT:ip">…</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_125" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_127" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_130" n="HIAT:w" s="T28">dĭ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_133" n="HIAT:w" s="T29">šobi</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_137" n="HIAT:w" s="T30">a</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_140" n="HIAT:w" s="T31">măn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_143" n="HIAT:w" s="T32">dĭʔnə</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_146" n="HIAT:w" s="T33">dʼăbaktərliam:</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_149" n="HIAT:w" s="T34">Tăn</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_152" n="HIAT:w" s="T35">mĭmbiel</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_155" n="HIAT:w" s="T36">tüʔsittə</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_159" n="HIAT:w" s="T37">i</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_162" n="HIAT:w" s="T38">men</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_165" n="HIAT:w" s="T39">tănziʔ</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_169" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_171" n="HIAT:w" s="T40">Dĭ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_174" n="HIAT:w" s="T41">bar</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_177" n="HIAT:w" s="T42">kakənarluʔpi</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_181" n="HIAT:w" s="T43">ugaːndə</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_184" n="HIAT:w" s="T44">tăŋ</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_188" n="HIAT:u" s="T45">
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_191" n="HIAT:w" s="T45">Kĭškəʔi</ts>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_195" n="HIAT:w" s="T46">bar</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_197" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_199" n="HIAT:w" s="T47">üzə-</ts>
                  <nts id="Seg_200" n="HIAT:ip">)</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_203" n="HIAT:w" s="T48">üjüʔi</ts>
                  <nts id="Seg_204" n="HIAT:ip">,</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_207" n="HIAT:w" s="T49">ĭzemneʔbəʔjə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_210" n="HIAT:w" s="T50">ugaːndə</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_214" n="HIAT:u" s="T51">
                  <nts id="Seg_215" n="HIAT:ip">(</nts>
                  <nts id="Seg_216" n="HIAT:ip">(</nts>
                  <ats e="T52" id="Seg_217" n="HIAT:non-pho" s="T51">BRK</ats>
                  <nts id="Seg_218" n="HIAT:ip">)</nts>
                  <nts id="Seg_219" n="HIAT:ip">)</nts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_223" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_225" n="HIAT:w" s="T52">Iʔgö</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_228" n="HIAT:w" s="T53">pʼe</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_231" n="HIAT:w" s="T54">kalla</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_234" n="HIAT:w" s="T964">dʼürbiʔi</ts>
                  <nts id="Seg_235" n="HIAT:ip">,</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_238" n="HIAT:w" s="T55">il</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_241" n="HIAT:w" s="T56">bar</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_244" n="HIAT:w" s="T57">ĭmbidə</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_247" n="HIAT:w" s="T58">ej</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_250" n="HIAT:w" s="T59">tĭmneʔi</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_254" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_256" n="HIAT:w" s="T60">Dĭzen</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_259" n="HIAT:w" s="T61">bar</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_261" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_263" n="HIAT:w" s="T62">i-</ts>
                  <nts id="Seg_264" n="HIAT:ip">)</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_267" n="HIAT:w" s="T63">iʔgö</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_270" n="HIAT:w" s="T64">inezeŋdə</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_273" n="HIAT:w" s="T65">i</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_276" n="HIAT:w" s="T66">tüžöjəʔi</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_279" n="HIAT:w" s="T67">iʔgö</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_283" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_285" n="HIAT:w" s="T68">I</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_288" n="HIAT:w" s="T69">ular</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_291" n="HIAT:w" s="T70">iʔgö</ts>
                  <nts id="Seg_292" n="HIAT:ip">,</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_295" n="HIAT:w" s="T71">kurizəʔi</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_298" n="HIAT:w" s="T72">iʔgö</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_302" n="HIAT:w" s="T73">uja</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_304" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_306" n="HIAT:w" s="T74">amnaʔbəʔjə</ts>
                  <nts id="Seg_307" n="HIAT:ip">)</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_310" n="HIAT:w" s="T75">iʔgö</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_313" n="HIAT:w" s="T76">bar</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_317" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_319" n="HIAT:w" s="T77">Süt</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">bar</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">iʔgö</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_329" n="HIAT:u" s="T80">
                  <nts id="Seg_330" n="HIAT:ip">(</nts>
                  <nts id="Seg_331" n="HIAT:ip">(</nts>
                  <ats e="T81" id="Seg_332" n="HIAT:non-pho" s="T80">BRK</ats>
                  <nts id="Seg_333" n="HIAT:ip">)</nts>
                  <nts id="Seg_334" n="HIAT:ip">)</nts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_338" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_340" n="HIAT:w" s="T81">Nuzaŋ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_343" n="HIAT:w" s="T82">bar</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_346" n="HIAT:w" s="T83">amnolaʔpiʔi</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_350" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_352" n="HIAT:w" s="T84">Kudajdə</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_355" n="HIAT:w" s="T85">abiʔi</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_357" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_359" n="HIAT:w" s="T86">pa-</ts>
                  <nts id="Seg_360" n="HIAT:ip">)</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_363" n="HIAT:w" s="T87">paziʔ</ts>
                  <nts id="Seg_364" n="HIAT:ip">,</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_367" n="HIAT:w" s="T88">dĭgəttə</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_369" n="HIAT:ip">(</nts>
                  <ts e="T90" id="Seg_371" n="HIAT:w" s="T89">pi-</ts>
                  <nts id="Seg_372" n="HIAT:ip">)</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_375" n="HIAT:w" s="T90">pigəʔ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_378" n="HIAT:w" s="T91">abiʔi</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_382" n="HIAT:w" s="T92">dĭgəttə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_385" n="HIAT:w" s="T93">sazənzəbi</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_388" n="HIAT:w" s="T94">abiʔi</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_392" n="HIAT:u" s="T95">
                  <nts id="Seg_393" n="HIAT:ip">(</nts>
                  <nts id="Seg_394" n="HIAT:ip">(</nts>
                  <ats e="T96" id="Seg_395" n="HIAT:non-pho" s="T95">BRK</ats>
                  <nts id="Seg_396" n="HIAT:ip">)</nts>
                  <nts id="Seg_397" n="HIAT:ip">)</nts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_401" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_403" n="HIAT:w" s="T96">Bazaj</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_406" n="HIAT:w" s="T97">kudaj</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_409" n="HIAT:w" s="T98">abiʔi</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_412" n="HIAT:w" s="T99">i</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_415" n="HIAT:w" s="T100">nuldəbiʔi</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_418" n="HIAT:w" s="T101">bar</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_422" n="HIAT:u" s="T102">
                  <nts id="Seg_423" n="HIAT:ip">(</nts>
                  <nts id="Seg_424" n="HIAT:ip">(</nts>
                  <ats e="T103" id="Seg_425" n="HIAT:non-pho" s="T102">BRK</ats>
                  <nts id="Seg_426" n="HIAT:ip">)</nts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_431" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_433" n="HIAT:w" s="T103">Dĭzeŋdə</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_436" n="HIAT:w" s="T104">anʼi</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_439" n="HIAT:w" s="T105">bar</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_442" n="HIAT:w" s="T106">svečkaʔi</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_445" n="HIAT:w" s="T107">nuldəbiʔi</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_449" n="HIAT:w" s="T108">nendəbiʔi</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_453" n="HIAT:u" s="T109">
                  <nts id="Seg_454" n="HIAT:ip">(</nts>
                  <nts id="Seg_455" n="HIAT:ip">(</nts>
                  <ats e="T110" id="Seg_456" n="HIAT:non-pho" s="T109">BRK</ats>
                  <nts id="Seg_457" n="HIAT:ip">)</nts>
                  <nts id="Seg_458" n="HIAT:ip">)</nts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_462" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_464" n="HIAT:w" s="T110">Nuzaŋ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_467" n="HIAT:w" s="T111">bar</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_470" n="HIAT:w" s="T112">mĭmbiʔi</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">dʼijegən</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_477" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_479" n="HIAT:w" s="T114">I</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_482" n="HIAT:w" s="T115">dĭgəttə</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_485" n="HIAT:w" s="T116">šišəge</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_488" n="HIAT:w" s="T117">ibi</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_491" n="HIAT:w" s="T118">dak</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_494" n="HIAT:w" s="T119">šoləʔjə</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_497" n="HIAT:w" s="T120">döbər</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_500" n="HIAT:w" s="T121">bar</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_504" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_506" n="HIAT:w" s="T122">Dĭn</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_509" n="HIAT:w" s="T123">stara</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_512" n="HIAT:w" s="T124">stoibe</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_515" n="HIAT:w" s="T125">amnobiʔi</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_518" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_520" n="HIAT:w" s="T126">dĭgəttə</ts>
                  <nts id="Seg_521" n="HIAT:ip">)</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_524" n="HIAT:w" s="T127">kubiʔi:</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_527" n="HIAT:w" s="T128">Dön</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_530" n="HIAT:w" s="T129">bü</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_533" n="HIAT:w" s="T130">ej</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_536" n="HIAT:w" s="T131">kănzəlia</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_540" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_542" n="HIAT:w" s="T132">Dĭgəttə</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_545" n="HIAT:w" s="T133">dön</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_547" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_549" n="HIAT:w" s="T134">nub</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_552" n="HIAT:w" s="T135">-</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_555" n="HIAT:w" s="T136">nulə-</ts>
                  <nts id="Seg_556" n="HIAT:ip">)</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_558" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_560" n="HIAT:w" s="T137">nuldlaʔbəjəʔ</ts>
                  <nts id="Seg_561" n="HIAT:ip">)</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_564" n="HIAT:w" s="T138">bar</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_567" n="HIAT:w" s="T139">maʔsiʔ</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_570" n="HIAT:w" s="T140">i</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_573" n="HIAT:w" s="T141">dön</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_576" n="HIAT:w" s="T142">amnolaʔpiʔi</ts>
                  <nts id="Seg_577" n="HIAT:ip">.</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_580" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_582" n="HIAT:w" s="T143">Dĭzeŋ</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_585" n="HIAT:w" s="T144">bar</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_588" n="HIAT:w" s="T145">mĭmbiʔi</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_592" n="HIAT:u" s="T146">
                  <nts id="Seg_593" n="HIAT:ip">(</nts>
                  <ts e="T147" id="Seg_595" n="HIAT:w" s="T146">Aktʼ</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_598" n="HIAT:w" s="T147">-</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_601" n="HIAT:w" s="T148">aktʼ-</ts>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_605" n="HIAT:w" s="T149">Aktʼit</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_608" n="HIAT:w" s="T150">bar</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_610" n="HIAT:ip">(</nts>
                  <ts e="T152" id="Seg_612" n="HIAT:w" s="T151">tădam</ts>
                  <nts id="Seg_613" n="HIAT:ip">)</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_616" n="HIAT:w" s="T152">ibi</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_620" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_622" n="HIAT:w" s="T153">Onʼiʔ</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_625" n="HIAT:w" s="T154">kandəga</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_629" n="HIAT:w" s="T155">dăre</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_632" n="HIAT:w" s="T156">bar</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_636" n="HIAT:w" s="T157">kandəgaʔi</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_639" n="HIAT:w" s="T158">kak</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_642" n="HIAT:w" s="T159">nabəʔi</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_646" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_648" n="HIAT:w" s="T160">Dʼijenə</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_651" n="HIAT:w" s="T161">kambiʔi</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_655" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_657" n="HIAT:w" s="T162">Onʼiʔ</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_660" n="HIAT:w" s="T163">ej</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_663" n="HIAT:w" s="T164">kallia</ts>
                  <nts id="Seg_664" n="HIAT:ip">,</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_667" n="HIAT:w" s="T165">a</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_670" n="HIAT:w" s="T166">iʔgö</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_673" n="HIAT:w" s="T167">kaləʔi</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_677" n="HIAT:u" s="T168">
                  <nts id="Seg_678" n="HIAT:ip">(</nts>
                  <nts id="Seg_679" n="HIAT:ip">(</nts>
                  <ats e="T169" id="Seg_680" n="HIAT:non-pho" s="T168">BRK</ats>
                  <nts id="Seg_681" n="HIAT:ip">)</nts>
                  <nts id="Seg_682" n="HIAT:ip">)</nts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_686" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_688" n="HIAT:w" s="T169">Dĭzeŋ</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_691" n="HIAT:w" s="T170">bar</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_694" n="HIAT:w" s="T171">dʼijegəʔ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_697" n="HIAT:w" s="T172">šonəgaʔi</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_701" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_703" n="HIAT:w" s="T173">Onʼiʔ</ts>
                  <nts id="Seg_704" n="HIAT:ip">,</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_707" n="HIAT:w" s="T174">šide</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_711" n="HIAT:w" s="T175">teʔtə</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_715" n="HIAT:w" s="T176">nagur</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_719" n="HIAT:w" s="T177">sumna</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_723" n="HIAT:w" s="T178">muktuʔ</ts>
                  <nts id="Seg_724" n="HIAT:ip">.</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_727" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_729" n="HIAT:w" s="T179">Onʼiʔ</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_732" n="HIAT:w" s="T180">onʼiʔziʔ</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_734" n="HIAT:ip">(</nts>
                  <ts e="T182" id="Seg_736" n="HIAT:w" s="T181">k</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_739" n="HIAT:w" s="T182">-</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_742" n="HIAT:w" s="T183">šolə</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_745" n="HIAT:w" s="T184">-</ts>
                  <nts id="Seg_746" n="HIAT:ip">)</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_749" n="HIAT:w" s="T185">šonaʔbəjəʔ</ts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_753" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_755" n="HIAT:w" s="T186">Bü</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_758" n="HIAT:w" s="T187">bar</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_761" n="HIAT:w" s="T188">kănzəlaʔpi</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_765" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_767" n="HIAT:w" s="T189">Üjüziʔ</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_769" n="HIAT:ip">(</nts>
                  <ts e="T191" id="Seg_771" n="HIAT:w" s="T190">nuldlial=</ts>
                  <nts id="Seg_772" n="HIAT:ip">)</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_775" n="HIAT:w" s="T191">nulal</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_778" n="HIAT:w" s="T192">dak</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_782" n="HIAT:w" s="T193">dibər</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_785" n="HIAT:w" s="T194">ej</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_788" n="HIAT:w" s="T195">saʔməlial</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_792" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_794" n="HIAT:w" s="T196">Măn</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_797" n="HIAT:w" s="T197">üdʼüge</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_800" n="HIAT:w" s="T198">ibim</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_804" n="HIAT:w" s="T199">măn</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_807" n="HIAT:w" s="T200">iam</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_810" n="HIAT:w" s="T201">togonorbi</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_814" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_816" n="HIAT:w" s="T202">Kubaʔi</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_818" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_820" n="HIAT:w" s="T203">ai-</ts>
                  <nts id="Seg_821" n="HIAT:ip">)</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_824" n="HIAT:w" s="T204">abi</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_828" n="HIAT:w" s="T205">pargaʔi</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_831" n="HIAT:w" s="T206">šobi</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_835" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_837" n="HIAT:w" s="T207">Jamaʔi</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_840" n="HIAT:w" s="T208">šöʔpi</ts>
                  <nts id="Seg_841" n="HIAT:ip">,</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_843" n="HIAT:ip">(</nts>
                  <ts e="T210" id="Seg_845" n="HIAT:w" s="T209">üžü</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_848" n="HIAT:w" s="T210">-</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_851" n="HIAT:w" s="T211">üzər-</ts>
                  <nts id="Seg_852" n="HIAT:ip">)</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_855" n="HIAT:w" s="T212">üžü</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_857" n="HIAT:ip">(</nts>
                  <ts e="T214" id="Seg_859" n="HIAT:w" s="T213">s-</ts>
                  <nts id="Seg_860" n="HIAT:ip">)</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_863" n="HIAT:w" s="T214">šöʔpi</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_866" n="HIAT:w" s="T215">i</ts>
                  <nts id="Seg_867" n="HIAT:ip">…</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_870" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_872" n="HIAT:w" s="T217">Abam</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_875" n="HIAT:w" s="T218">bar</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_878" n="HIAT:w" s="T219">šerbi</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_882" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_884" n="HIAT:w" s="T220">Tüj</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_887" n="HIAT:w" s="T221">măn</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_889" n="HIAT:ip">(</nts>
                  <ts e="T223" id="Seg_891" n="HIAT:w" s="T222">šalbə</ts>
                  <nts id="Seg_892" n="HIAT:ip">)</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_895" n="HIAT:w" s="T223">naga</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_899" n="HIAT:w" s="T224">ej</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_902" n="HIAT:w" s="T225">tĭmnem</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_905" n="HIAT:w" s="T226">dĭ</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_908" n="HIAT:w" s="T227">giraːmbi</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_912" n="HIAT:u" s="T228">
                  <nts id="Seg_913" n="HIAT:ip">(</nts>
                  <nts id="Seg_914" n="HIAT:ip">(</nts>
                  <ats e="T229" id="Seg_915" n="HIAT:non-pho" s="T228">BRK</ats>
                  <nts id="Seg_916" n="HIAT:ip">)</nts>
                  <nts id="Seg_917" n="HIAT:ip">)</nts>
                  <nts id="Seg_918" n="HIAT:ip">.</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_921" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_923" n="HIAT:w" s="T229">Ugaːndə</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ts e="T231" id="Seg_927" n="HIAT:w" s="T230">pim-</ts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_931" n="HIAT:w" s="T231">pimniem</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_935" n="HIAT:w" s="T232">bar</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_938" n="HIAT:w" s="T233">măna</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_941" n="HIAT:w" s="T234">sădărlaʔbə</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_945" n="HIAT:u" s="T235">
                  <nts id="Seg_946" n="HIAT:ip">(</nts>
                  <nts id="Seg_947" n="HIAT:ip">(</nts>
                  <ats e="T236" id="Seg_948" n="HIAT:non-pho" s="T235">BRK</ats>
                  <nts id="Seg_949" n="HIAT:ip">)</nts>
                  <nts id="Seg_950" n="HIAT:ip">)</nts>
                  <nts id="Seg_951" n="HIAT:ip">.</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_954" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_956" n="HIAT:w" s="T236">Teinen</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_958" n="HIAT:ip">(</nts>
                  <ts e="T238" id="Seg_960" n="HIAT:w" s="T237">di-</ts>
                  <nts id="Seg_961" n="HIAT:ip">)</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_964" n="HIAT:w" s="T238">dʼijegən</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_967" n="HIAT:w" s="T239">šaːbilaʔ</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_971" n="HIAT:w" s="T240">ugaːndə</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_974" n="HIAT:w" s="T241">šišəge</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_977" n="HIAT:w" s="T242">ibi</ts>
                  <nts id="Seg_978" n="HIAT:ip">.</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_981" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_983" n="HIAT:w" s="T243">Da</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_986" n="HIAT:w" s="T244">miʔ</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_989" n="HIAT:w" s="T245">ugaːndə</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_992" n="HIAT:w" s="T246">kănnaːmbibaʔ</ts>
                  <nts id="Seg_993" n="HIAT:ip">,</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_996" n="HIAT:w" s="T247">bar</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_999" n="HIAT:w" s="T248">tăŋ</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1002" n="HIAT:w" s="T249">kănnaːmbibaʔ</ts>
                  <nts id="Seg_1003" n="HIAT:ip">.</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1006" n="HIAT:u" s="T250">
                  <nts id="Seg_1007" n="HIAT:ip">(</nts>
                  <nts id="Seg_1008" n="HIAT:ip">(</nts>
                  <ats e="T251" id="Seg_1009" n="HIAT:non-pho" s="T250">BRK</ats>
                  <nts id="Seg_1010" n="HIAT:ip">)</nts>
                  <nts id="Seg_1011" n="HIAT:ip">)</nts>
                  <nts id="Seg_1012" n="HIAT:ip">.</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1015" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1017" n="HIAT:w" s="T251">Ugaːndə</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1020" n="HIAT:w" s="T252">šišəge</ts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1024" n="HIAT:w" s="T253">bü</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1027" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1030" n="HIAT:w" s="T255">kănnaːmbi</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1034" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1036" n="HIAT:w" s="T256">Măn</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1038" n="HIAT:ip">(</nts>
                  <ts e="T258" id="Seg_1040" n="HIAT:w" s="T257">üjüzibə</ts>
                  <nts id="Seg_1041" n="HIAT:ip">)</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1043" n="HIAT:ip">(</nts>
                  <ts e="T259" id="Seg_1045" n="HIAT:w" s="T258">nul-</ts>
                  <nts id="Seg_1046" n="HIAT:ip">)</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1049" n="HIAT:w" s="T259">nubiam</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1053" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1055" n="HIAT:w" s="T260">Dĭ</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1058" n="HIAT:w" s="T261">ej</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1061" n="HIAT:w" s="T262">băldəbi</ts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1065" n="HIAT:u" s="T263">
                  <nts id="Seg_1066" n="HIAT:ip">(</nts>
                  <nts id="Seg_1067" n="HIAT:ip">(</nts>
                  <ats e="T264" id="Seg_1068" n="HIAT:non-pho" s="T263">BRK</ats>
                  <nts id="Seg_1069" n="HIAT:ip">)</nts>
                  <nts id="Seg_1070" n="HIAT:ip">)</nts>
                  <nts id="Seg_1071" n="HIAT:ip">.</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1074" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1076" n="HIAT:w" s="T264">Kamən</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1079" n="HIAT:w" s="T265">kunolzittə</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1082" n="HIAT:w" s="T266">iʔbələl</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1086" n="HIAT:w" s="T267">suraraʔ:</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1089" n="HIAT:w" s="T268">Öʔləl</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1092" n="HIAT:w" s="T269">măna</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1095" n="HIAT:w" s="T270">kunolzittə</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1098" n="HIAT:w" s="T271">dön</ts>
                  <nts id="Seg_1099" n="HIAT:ip">?</nts>
                  <nts id="Seg_1100" n="HIAT:ip">"</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1103" n="HIAT:u" s="T272">
                  <ts e="T274" id="Seg_1105" n="HIAT:w" s="T272">Baštap</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1108" n="HIAT:w" s="T274">dĭn</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1111" n="HIAT:w" s="T275">nubiʔi</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1114" n="HIAT:w" s="T276">Ilʼbinʼən</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1117" n="HIAT:w" s="T277">toːndə</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1121" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1123" n="HIAT:w" s="T278">Dĭgəttə</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1126" n="HIAT:w" s="T279">dö</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1129" n="HIAT:w" s="T280">bü</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1132" n="HIAT:w" s="T281">kubiʔi</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1136" n="HIAT:u" s="T282">
                  <nts id="Seg_1137" n="HIAT:ip">(</nts>
                  <ts e="T283" id="Seg_1139" n="HIAT:w" s="T282">Ej</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1142" n="HIAT:w" s="T283">m-</ts>
                  <nts id="Seg_1143" n="HIAT:ip">)</nts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1147" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1149" n="HIAT:w" s="T284">Ej</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1152" n="HIAT:w" s="T285">kănzəlia</ts>
                  <nts id="Seg_1153" n="HIAT:ip">,</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1155" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_1157" n="HIAT:w" s="T286">d-</ts>
                  <nts id="Seg_1158" n="HIAT:ip">)</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1161" n="HIAT:w" s="T287">dĭzeŋ</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1164" n="HIAT:w" s="T288">dön</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1167" n="HIAT:w" s="T289">maʔi</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1170" n="HIAT:w" s="T290">nuldəbiʔi</ts>
                  <nts id="Seg_1171" n="HIAT:ip">.</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1174" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1176" n="HIAT:w" s="T291">I</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1179" n="HIAT:w" s="T292">döbər</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1182" n="HIAT:w" s="T293">amnosʼtə</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1185" n="HIAT:w" s="T294">šobiʔi</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1188" n="HIAT:w" s="T295">kamen</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1191" n="HIAT:w" s="T296">šišəge</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1194" n="HIAT:w" s="T297">molaːmbi</ts>
                  <nts id="Seg_1195" n="HIAT:ip">.</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1198" n="HIAT:u" s="T298">
                  <nts id="Seg_1199" n="HIAT:ip">(</nts>
                  <nts id="Seg_1200" n="HIAT:ip">(</nts>
                  <ats e="T299" id="Seg_1201" n="HIAT:non-pho" s="T298">BRK</ats>
                  <nts id="Seg_1202" n="HIAT:ip">)</nts>
                  <nts id="Seg_1203" n="HIAT:ip">)</nts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1207" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1209" n="HIAT:w" s="T299">Dĭzeŋ</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1212" n="HIAT:w" s="T300">bar</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1215" n="HIAT:w" s="T301">šobiʔi</ts>
                  <nts id="Seg_1216" n="HIAT:ip">,</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1219" n="HIAT:w" s="T302">Ilʼbinʼdə</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1222" n="HIAT:w" s="T303">amnolaʔbəʔi</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1226" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1228" n="HIAT:w" s="T304">Dĭgəttə</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1231" n="HIAT:w" s="T305">döbər</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1234" n="HIAT:w" s="T306">šobiʔi</ts>
                  <nts id="Seg_1235" n="HIAT:ip">,</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1238" n="HIAT:w" s="T307">bü</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1241" n="HIAT:w" s="T308">kubiʔi</ts>
                  <nts id="Seg_1242" n="HIAT:ip">,</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1245" n="HIAT:w" s="T309">dĭ</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1248" n="HIAT:w" s="T310">ej</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1251" n="HIAT:w" s="T311">kănnia</ts>
                  <nts id="Seg_1252" n="HIAT:ip">.</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1255" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1257" n="HIAT:w" s="T312">Dön</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1260" n="HIAT:w" s="T313">maʔzandə</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1263" n="HIAT:w" s="T314">nuldəbiʔi</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1267" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1269" n="HIAT:w" s="T315">Döbər</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1271" n="HIAT:ip">(</nts>
                  <ts e="T317" id="Seg_1273" n="HIAT:w" s="T316">šo-</ts>
                  <nts id="Seg_1274" n="HIAT:ip">)</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1277" n="HIAT:w" s="T317">šonəgaʔi</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1280" n="HIAT:w" s="T318">amnozittə</ts>
                  <nts id="Seg_1281" n="HIAT:ip">,</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1284" n="HIAT:w" s="T319">kamen</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1287" n="HIAT:w" s="T320">šišəge</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1290" n="HIAT:w" s="T321">molaːlləj</ts>
                  <nts id="Seg_1291" n="HIAT:ip">.</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1294" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1296" n="HIAT:w" s="T322">Dĭzeŋ</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1299" n="HIAT:w" s="T323">ej</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1302" n="HIAT:w" s="T324">molaːmbi</ts>
                  <nts id="Seg_1303" n="HIAT:ip">,</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1306" n="HIAT:w" s="T325">dĭgəttə</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1309" n="HIAT:w" s="T326">kandəgaʔi</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1312" n="HIAT:w" s="T327">dʼijenə</ts>
                  <nts id="Seg_1313" n="HIAT:ip">,</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1316" n="HIAT:w" s="T328">vezʼdʼe</ts>
                  <nts id="Seg_1317" n="HIAT:ip">.</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1320" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1322" n="HIAT:w" s="T329">Bar</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1325" n="HIAT:w" s="T330">dʼügən</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1328" n="HIAT:w" s="T331">mĭmbiʔi</ts>
                  <nts id="Seg_1329" n="HIAT:ip">.</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1332" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1334" n="HIAT:w" s="T332">Dʼelamdə</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1337" n="HIAT:w" s="T333">kambiʔi</ts>
                  <nts id="Seg_1338" n="HIAT:ip">,</nts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1341" n="HIAT:w" s="T334">dĭn</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1344" n="HIAT:w" s="T335">bü</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1347" n="HIAT:w" s="T336">iʔgö</ts>
                  <nts id="Seg_1348" n="HIAT:ip">.</nts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1351" n="HIAT:u" s="T337">
                  <nts id="Seg_1352" n="HIAT:ip">(</nts>
                  <nts id="Seg_1353" n="HIAT:ip">(</nts>
                  <ats e="T338" id="Seg_1354" n="HIAT:non-pho" s="T337">BRK</ats>
                  <nts id="Seg_1355" n="HIAT:ip">)</nts>
                  <nts id="Seg_1356" n="HIAT:ip">)</nts>
                  <nts id="Seg_1357" n="HIAT:ip">.</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1360" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1362" n="HIAT:w" s="T338">Onʼiʔ</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1365" n="HIAT:w" s="T339">kuza</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1368" n="HIAT:w" s="T340">dön</ts>
                  <nts id="Seg_1369" n="HIAT:ip">,</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1372" n="HIAT:w" s="T341">a</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1375" n="HIAT:w" s="T342">onʼiʔ</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1378" n="HIAT:w" s="T343">kuza</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1381" n="HIAT:w" s="T344">dĭn</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1385" n="HIAT:u" s="T345">
                  <nts id="Seg_1386" n="HIAT:ip">(</nts>
                  <nts id="Seg_1387" n="HIAT:ip">(</nts>
                  <ats e="T346" id="Seg_1388" n="HIAT:non-pho" s="T345">BRK</ats>
                  <nts id="Seg_1389" n="HIAT:ip">)</nts>
                  <nts id="Seg_1390" n="HIAT:ip">)</nts>
                  <nts id="Seg_1391" n="HIAT:ip">.</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1394" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1396" n="HIAT:w" s="T346">Kanaʔ</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1399" n="HIAT:w" s="T347">dibər</ts>
                  <nts id="Seg_1400" n="HIAT:ip">,</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1403" n="HIAT:w" s="T348">döbər</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1407" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_1409" n="HIAT:w" s="T349">Dibər</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1412" n="HIAT:w" s="T350">em</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1415" n="HIAT:w" s="T351">kanaʔ</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1418" n="HIAT:w" s="T352">i</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1421" n="HIAT:w" s="T353">döbər</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1424" n="HIAT:w" s="T354">em</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1427" n="HIAT:w" s="T355">kanaʔ</ts>
                  <nts id="Seg_1428" n="HIAT:ip">.</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1431" n="HIAT:u" s="T356">
                  <nts id="Seg_1432" n="HIAT:ip">(</nts>
                  <nts id="Seg_1433" n="HIAT:ip">(</nts>
                  <ats e="T357" id="Seg_1434" n="HIAT:non-pho" s="T356">BRK</ats>
                  <nts id="Seg_1435" n="HIAT:ip">)</nts>
                  <nts id="Seg_1436" n="HIAT:ip">)</nts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1440" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1442" n="HIAT:w" s="T357">Kăde</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1445" n="HIAT:w" s="T358">dăre</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1448" n="HIAT:w" s="T359">moləj</ts>
                  <nts id="Seg_1449" n="HIAT:ip">?</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1452" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1454" n="HIAT:w" s="T360">Ej</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1457" n="HIAT:w" s="T361">kalial</ts>
                  <nts id="Seg_1458" n="HIAT:ip">,</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1461" n="HIAT:w" s="T362">nʼim</ts>
                  <nts id="Seg_1462" n="HIAT:ip">.</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1465" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1467" n="HIAT:w" s="T363">Dĭ</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1470" n="HIAT:w" s="T364">kuza</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1473" n="HIAT:w" s="T365">ugandə</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1476" n="HIAT:w" s="T366">jakšə</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1478" n="HIAT:ip">(</nts>
                  <ts e="T368" id="Seg_1480" n="HIAT:w" s="T367">amo-</ts>
                  <nts id="Seg_1481" n="HIAT:ip">)</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1484" n="HIAT:w" s="T368">amnolaʔbə</ts>
                  <nts id="Seg_1485" n="HIAT:ip">.</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1488" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1490" n="HIAT:w" s="T369">Dĭn</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1493" n="HIAT:w" s="T370">bar</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1496" n="HIAT:w" s="T371">ĭmbi</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1499" n="HIAT:w" s="T372">ige</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1503" n="HIAT:u" s="T373">
                  <nts id="Seg_1504" n="HIAT:ip">(</nts>
                  <nts id="Seg_1505" n="HIAT:ip">(</nts>
                  <ats e="T374" id="Seg_1506" n="HIAT:non-pho" s="T373">BRK</ats>
                  <nts id="Seg_1507" n="HIAT:ip">)</nts>
                  <nts id="Seg_1508" n="HIAT:ip">)</nts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1512" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1514" n="HIAT:w" s="T374">Dʼirəgəʔi</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1517" n="HIAT:w" s="T375">bar</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1520" n="HIAT:w" s="T376">šobiʔi</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1523" n="HIAT:w" s="T377">măn</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1526" n="HIAT:w" s="T378">šĭkəziʔ</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1529" n="HIAT:w" s="T379">dʼăbaktərzittə</ts>
                  <nts id="Seg_1530" n="HIAT:ip">.</nts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1533" n="HIAT:u" s="T380">
                  <nts id="Seg_1534" n="HIAT:ip">(</nts>
                  <nts id="Seg_1535" n="HIAT:ip">(</nts>
                  <ats e="T381" id="Seg_1536" n="HIAT:non-pho" s="T380">BRK</ats>
                  <nts id="Seg_1537" n="HIAT:ip">)</nts>
                  <nts id="Seg_1538" n="HIAT:ip">)</nts>
                  <nts id="Seg_1539" n="HIAT:ip">.</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1542" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1544" n="HIAT:w" s="T381">Dĭzeŋ</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1547" n="HIAT:w" s="T382">măn</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1549" n="HIAT:ip">(</nts>
                  <ts e="T384" id="Seg_1551" n="HIAT:w" s="T383">š-</ts>
                  <nts id="Seg_1552" n="HIAT:ip">)</nts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1555" n="HIAT:w" s="T384">šĭkəm</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1558" n="HIAT:w" s="T385">tüšəleʔbəʔjə</ts>
                  <nts id="Seg_1559" n="HIAT:ip">.</nts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1562" n="HIAT:u" s="T386">
                  <nts id="Seg_1563" n="HIAT:ip">(</nts>
                  <nts id="Seg_1564" n="HIAT:ip">(</nts>
                  <ats e="T387" id="Seg_1565" n="HIAT:non-pho" s="T386">BRK</ats>
                  <nts id="Seg_1566" n="HIAT:ip">)</nts>
                  <nts id="Seg_1567" n="HIAT:ip">)</nts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1571" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1573" n="HIAT:w" s="T387">Suraraʔ</ts>
                  <nts id="Seg_1574" n="HIAT:ip">,</nts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1577" n="HIAT:w" s="T388">a</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1580" n="HIAT:w" s="T389">măn</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1583" n="HIAT:w" s="T390">nörbəlem</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1586" n="HIAT:w" s="T391">tănan</ts>
                  <nts id="Seg_1587" n="HIAT:ip">.</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1590" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1592" n="HIAT:w" s="T392">Tăn</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1595" n="HIAT:w" s="T393">ĭmbi</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1597" n="HIAT:ip">(</nts>
                  <ts e="T395" id="Seg_1599" n="HIAT:w" s="T394">dĭgə</ts>
                  <nts id="Seg_1600" n="HIAT:ip">)</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1603" n="HIAT:w" s="T395">ibiel</ts>
                  <nts id="Seg_1604" n="HIAT:ip">.</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1607" n="HIAT:u" s="T396">
                  <nts id="Seg_1608" n="HIAT:ip">(</nts>
                  <nts id="Seg_1609" n="HIAT:ip">(</nts>
                  <ats e="T397" id="Seg_1610" n="HIAT:non-pho" s="T396">BRK</ats>
                  <nts id="Seg_1611" n="HIAT:ip">)</nts>
                  <nts id="Seg_1612" n="HIAT:ip">)</nts>
                  <nts id="Seg_1613" n="HIAT:ip">.</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1616" n="HIAT:u" s="T397">
                  <ts e="T397.tx-PKZ.1" id="Seg_1618" n="HIAT:w" s="T397">На</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397.tx-PKZ.2" id="Seg_1621" n="HIAT:w" s="T397.tx-PKZ.1">кого</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1624" n="HIAT:w" s="T397.tx-PKZ.2">говорить</ts>
                  <nts id="Seg_1625" n="HIAT:ip">?</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1628" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1630" n="HIAT:w" s="T400">Tăn</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1633" n="HIAT:w" s="T401">ugaːndə</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1636" n="HIAT:w" s="T402">numo</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1638" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1640" n="HIAT:w" s="T403">šĭ-</ts>
                  <nts id="Seg_1641" n="HIAT:ip">)</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1644" n="HIAT:w" s="T404">šĭkəl</ts>
                  <nts id="Seg_1645" n="HIAT:ip">.</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1648" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1650" n="HIAT:w" s="T405">A</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1653" n="HIAT:w" s="T406">măn</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1656" n="HIAT:w" s="T407">üdʼüge</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1659" n="HIAT:w" s="T408">šĭkəm</ts>
                  <nts id="Seg_1660" n="HIAT:ip">.</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1663" n="HIAT:u" s="T409">
                  <nts id="Seg_1664" n="HIAT:ip">(</nts>
                  <nts id="Seg_1665" n="HIAT:ip">(</nts>
                  <ats e="T410" id="Seg_1666" n="HIAT:non-pho" s="T409">BRK</ats>
                  <nts id="Seg_1667" n="HIAT:ip">)</nts>
                  <nts id="Seg_1668" n="HIAT:ip">)</nts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1672" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1674" n="HIAT:w" s="T410">Miʔ</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1677" n="HIAT:w" s="T411">bar</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1679" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1681" n="HIAT:w" s="T412">šo-</ts>
                  <nts id="Seg_1682" n="HIAT:ip">)</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1685" n="HIAT:w" s="T413">šobibaʔ</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1687" n="HIAT:ip">(</nts>
                  <ts e="T415" id="Seg_1689" n="HIAT:w" s="T414">oktʼ-</ts>
                  <nts id="Seg_1690" n="HIAT:ip">)</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1693" n="HIAT:w" s="T415">aktʼinə</ts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1697" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1699" n="HIAT:w" s="T416">Kuza</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1702" n="HIAT:w" s="T417">bar</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1705" n="HIAT:w" s="T418">šonəga</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1708" n="HIAT:w" s="T419">miʔnʼibeʔ</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1712" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1714" n="HIAT:w" s="T420">Suraraʔ</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1717" n="HIAT:w" s="T421">aʔtʼi</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1721" n="HIAT:w" s="T422">gibər</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1724" n="HIAT:w" s="T423">dĭ</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1727" n="HIAT:w" s="T424">kandəga</ts>
                  <nts id="Seg_1728" n="HIAT:ip">,</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425.tx-PKZ.1" id="Seg_1731" n="HIAT:w" s="T425">a</ts>
                  <nts id="Seg_1732" n="HIAT:ip">_</nts>
                  <ts e="T426" id="Seg_1734" n="HIAT:w" s="T425.tx-PKZ.1">to</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1737" n="HIAT:w" s="T426">miʔ</ts>
                  <nts id="Seg_1738" n="HIAT:ip">…</nts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1741" n="HIAT:u" s="T428">
                  <nts id="Seg_1742" n="HIAT:ip">(</nts>
                  <nts id="Seg_1743" n="HIAT:ip">(</nts>
                  <ats e="T429" id="Seg_1744" n="HIAT:non-pho" s="T428">BRK</ats>
                  <nts id="Seg_1745" n="HIAT:ip">)</nts>
                  <nts id="Seg_1746" n="HIAT:ip">)</nts>
                  <nts id="Seg_1747" n="HIAT:ip">.</nts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1750" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1752" n="HIAT:w" s="T429">Miʔ</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1755" n="HIAT:w" s="T430">bar</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1758" n="HIAT:w" s="T431">ej</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1761" n="HIAT:w" s="T432">dĭbər</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1764" n="HIAT:w" s="T433">možet</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1767" n="HIAT:w" s="T434">kambibaʔ</ts>
                  <nts id="Seg_1768" n="HIAT:ip">.</nts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1771" n="HIAT:u" s="T435">
                  <nts id="Seg_1772" n="HIAT:ip">(</nts>
                  <nts id="Seg_1773" n="HIAT:ip">(</nts>
                  <ats e="T436" id="Seg_1774" n="HIAT:non-pho" s="T435">BRK</ats>
                  <nts id="Seg_1775" n="HIAT:ip">)</nts>
                  <nts id="Seg_1776" n="HIAT:ip">)</nts>
                  <nts id="Seg_1777" n="HIAT:ip">.</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1780" n="HIAT:u" s="T436">
                  <nts id="Seg_1781" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_1783" n="HIAT:w" s="T436">Tăn</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1786" n="HIAT:w" s="T437">bar</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1789" n="HIAT:w" s="T438">ej</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1792" n="HIAT:w" s="T439">dʼăbaktərial</ts>
                  <nts id="Seg_1793" n="HIAT:ip">,</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1796" n="HIAT:w" s="T440">a</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1799" n="HIAT:w" s="T441">măn</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1801" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1803" n="HIAT:w" s="T442">dʼăbaktəriam</ts>
                  <nts id="Seg_1804" n="HIAT:ip">)</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1807" n="HIAT:w" s="T443">tănziʔ</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1811" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1813" n="HIAT:w" s="T444">A</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1816" n="HIAT:w" s="T445">măn</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1819" n="HIAT:w" s="T446">ej</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1822" n="HIAT:w" s="T447">tĭmnem</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1826" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1828" n="HIAT:w" s="T448">Măn</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1831" n="HIAT:w" s="T449">tĭmnem</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1834" n="HIAT:w" s="T450">a</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1837" n="HIAT:w" s="T451">dʼăbaktərzittə</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1840" n="HIAT:w" s="T452">ej</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1843" n="HIAT:w" s="T453">moliam</ts>
                  <nts id="Seg_1844" n="HIAT:ip">.</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1847" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1849" n="HIAT:w" s="T454">Nada</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1852" n="HIAT:w" s="T455">tăn</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1855" n="HIAT:w" s="T456">šĭkəl</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1858" n="HIAT:w" s="T457">sajnʼeʔsittə</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1861" n="HIAT:w" s="T458">i</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1864" n="HIAT:w" s="T459">baruʔsittə</ts>
                  <nts id="Seg_1865" n="HIAT:ip">"</nts>
                  <nts id="Seg_1866" n="HIAT:ip">.</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1869" n="HIAT:u" s="T460">
                  <nts id="Seg_1870" n="HIAT:ip">(</nts>
                  <nts id="Seg_1871" n="HIAT:ip">(</nts>
                  <ats e="T461" id="Seg_1872" n="HIAT:non-pho" s="T460">BRK</ats>
                  <nts id="Seg_1873" n="HIAT:ip">)</nts>
                  <nts id="Seg_1874" n="HIAT:ip">)</nts>
                  <nts id="Seg_1875" n="HIAT:ip">.</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1878" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1880" n="HIAT:w" s="T461">Dĭzeŋ</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1883" n="HIAT:w" s="T462">tüšəlbiʔi</ts>
                  <nts id="Seg_1884" n="HIAT:ip">,</nts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1887" n="HIAT:w" s="T463">tüj</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1890" n="HIAT:w" s="T464">tože</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1893" n="HIAT:w" s="T465">nuzaŋ</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1896" n="HIAT:w" s="T466">moləʔjə</ts>
                  <nts id="Seg_1897" n="HIAT:ip">.</nts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1900" n="HIAT:u" s="T467">
                  <nts id="Seg_1901" n="HIAT:ip">(</nts>
                  <nts id="Seg_1902" n="HIAT:ip">(</nts>
                  <ats e="T468" id="Seg_1903" n="HIAT:non-pho" s="T467">BRK</ats>
                  <nts id="Seg_1904" n="HIAT:ip">)</nts>
                  <nts id="Seg_1905" n="HIAT:ip">)</nts>
                  <nts id="Seg_1906" n="HIAT:ip">.</nts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1909" n="HIAT:u" s="T468">
                  <nts id="Seg_1910" n="HIAT:ip">(</nts>
                  <ts e="T469" id="Seg_1912" n="HIAT:w" s="T468">Ugaːn-</ts>
                  <nts id="Seg_1913" n="HIAT:ip">)</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1916" n="HIAT:w" s="T469">Ugaːndə</ts>
                  <nts id="Seg_1917" n="HIAT:ip">.</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1920" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1922" n="HIAT:w" s="T470">Urgo</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1925" n="HIAT:w" s="T471">beržə</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1928" n="HIAT:w" s="T472">bar</ts>
                  <nts id="Seg_1929" n="HIAT:ip">.</nts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1932" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1934" n="HIAT:w" s="T473">Tăŋ</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1937" n="HIAT:w" s="T474">bar</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1940" n="HIAT:w" s="T475">kandəga</ts>
                  <nts id="Seg_1941" n="HIAT:ip">.</nts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1944" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1946" n="HIAT:w" s="T476">Sĭre</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1949" n="HIAT:w" s="T477">bar</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1952" n="HIAT:w" s="T478">kăndlaʔbə</ts>
                  <nts id="Seg_1953" n="HIAT:ip">,</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1956" n="HIAT:w" s="T479">i</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1959" n="HIAT:w" s="T480">dʼü</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1962" n="HIAT:w" s="T481">kăndlaʔbə</ts>
                  <nts id="Seg_1963" n="HIAT:ip">,</nts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1966" n="HIAT:w" s="T482">bar</ts>
                  <nts id="Seg_1967" n="HIAT:ip">…</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1970" n="HIAT:u" s="T484">
                  <ts e="T486" id="Seg_1972" n="HIAT:w" s="T484">Sĭre</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1975" n="HIAT:w" s="T486">i</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1978" n="HIAT:w" s="T487">dʼü</ts>
                  <nts id="Seg_1979" n="HIAT:ip">,</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1981" n="HIAT:ip">(</nts>
                  <ts e="T489" id="Seg_1983" n="HIAT:w" s="T488">bar</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1986" n="HIAT:w" s="T489">m-</ts>
                  <nts id="Seg_1987" n="HIAT:ip">)</nts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1990" n="HIAT:w" s="T490">bar</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1992" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_1994" n="HIAT:w" s="T491">bĭlgarluʔpi</ts>
                  <nts id="Seg_1995" n="HIAT:ip">)</nts>
                  <nts id="Seg_1996" n="HIAT:ip">.</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1999" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_2001" n="HIAT:w" s="T492">Tăn</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2004" n="HIAT:w" s="T493">kudaj</ts>
                  <nts id="Seg_2005" n="HIAT:ip">,</nts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2008" n="HIAT:w" s="T494">kudaj</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2012" n="HIAT:w" s="T495">iʔ</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2015" n="HIAT:w" s="T496">maʔtə</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2018" n="HIAT:w" s="T497">măna</ts>
                  <nts id="Seg_2019" n="HIAT:ip">,</nts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2022" n="HIAT:w" s="T498">iʔ</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2025" n="HIAT:w" s="T499">maʔtə</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2028" n="HIAT:w" s="T500">măna</ts>
                  <nts id="Seg_2029" n="HIAT:ip">.</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2032" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_2034" n="HIAT:w" s="T501">Iʔ</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2037" n="HIAT:w" s="T502">maʔtə</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2040" n="HIAT:w" s="T503">măna</ts>
                  <nts id="Seg_2041" n="HIAT:ip">,</nts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2044" n="HIAT:w" s="T504">iʔ</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2047" n="HIAT:w" s="T505">barəʔtə</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2050" n="HIAT:w" s="T506">măna</ts>
                  <nts id="Seg_2051" n="HIAT:ip">.</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_2054" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2056" n="HIAT:w" s="T507">Tăn</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2059" n="HIAT:w" s="T508">it</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2062" n="HIAT:w" s="T509">măna</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2065" n="HIAT:w" s="T510">sĭjbə</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2068" n="HIAT:w" s="T511">sagəšsəbi</ts>
                  <nts id="Seg_2069" n="HIAT:ip">,</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2072" n="HIAT:w" s="T512">tăn</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2075" n="HIAT:w" s="T513">deʔ</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2078" n="HIAT:w" s="T514">măna</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2081" n="HIAT:w" s="T515">sĭjbə</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2084" n="HIAT:w" s="T516">sagəš</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2087" n="HIAT:w" s="T517">iʔgö</ts>
                  <nts id="Seg_2088" n="HIAT:ip">.</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_2091" n="HIAT:u" s="T518">
                  <ts e="T519" id="Seg_2093" n="HIAT:w" s="T518">Tüšəldə</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2096" n="HIAT:w" s="T519">măna</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2099" n="HIAT:w" s="T520">maktanərzittə</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2102" n="HIAT:w" s="T521">tănzi</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2106" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_2108" n="HIAT:w" s="T522">Tüšəldə</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2111" n="HIAT:w" s="T523">măna</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2115" n="HIAT:w" s="T524">tăn</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2117" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_2119" n="HIAT:w" s="T525">aʔtʼit-</ts>
                  <nts id="Seg_2120" n="HIAT:ip">)</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2123" n="HIAT:w" s="T526">aktʼitəm</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2126" n="HIAT:w" s="T527">mĭnzittə</ts>
                  <nts id="Seg_2127" n="HIAT:ip">.</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_2130" n="HIAT:u" s="T528">
                  <nts id="Seg_2131" n="HIAT:ip">(</nts>
                  <nts id="Seg_2132" n="HIAT:ip">(</nts>
                  <ats e="T529" id="Seg_2133" n="HIAT:non-pho" s="T528">BRK</ats>
                  <nts id="Seg_2134" n="HIAT:ip">)</nts>
                  <nts id="Seg_2135" n="HIAT:ip">)</nts>
                  <nts id="Seg_2136" n="HIAT:ip">.</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2139" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_2141" n="HIAT:w" s="T529">Tăn</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2143" n="HIAT:ip">(</nts>
                  <ts e="T531" id="Seg_2145" n="HIAT:w" s="T530">aksin-</ts>
                  <nts id="Seg_2146" n="HIAT:ip">)</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2148" n="HIAT:ip">(</nts>
                  <ts e="T532" id="Seg_2150" n="HIAT:w" s="T531">aktʼiziʔi</ts>
                  <nts id="Seg_2151" n="HIAT:ip">)</nts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2154" n="HIAT:w" s="T532">kanzittə</ts>
                  <nts id="Seg_2155" n="HIAT:ip">.</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_2158" n="HIAT:u" s="T533">
                  <nts id="Seg_2159" n="HIAT:ip">(</nts>
                  <nts id="Seg_2160" n="HIAT:ip">(</nts>
                  <ats e="T533.tx-PKZ.1" id="Seg_2161" n="HIAT:non-pho" s="T533">BRK</ats>
                  <nts id="Seg_2162" n="HIAT:ip">)</nts>
                  <nts id="Seg_2163" n="HIAT:ip">)</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2166" n="HIAT:w" s="T533.tx-PKZ.1">Погоди</ts>
                  <nts id="Seg_2167" n="HIAT:ip">…</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_2170" n="HIAT:u" s="T536">
                  <nts id="Seg_2171" n="HIAT:ip">(</nts>
                  <nts id="Seg_2172" n="HIAT:ip">(</nts>
                  <ats e="T537" id="Seg_2173" n="HIAT:non-pho" s="T536">BRK</ats>
                  <nts id="Seg_2174" n="HIAT:ip">)</nts>
                  <nts id="Seg_2175" n="HIAT:ip">)</nts>
                  <nts id="Seg_2176" n="HIAT:ip">.</nts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2179" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_2181" n="HIAT:w" s="T537">Vanʼa</ts>
                  <nts id="Seg_2182" n="HIAT:ip">,</nts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2185" n="HIAT:w" s="T538">kanaʔ</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2188" n="HIAT:w" s="T539">kazak</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2191" n="HIAT:w" s="T540">ianəl</ts>
                  <nts id="Seg_2192" n="HIAT:ip">.</nts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2195" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_2197" n="HIAT:w" s="T541">Kăʔbde</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2200" n="HIAT:w" s="T542">pʼeʔ</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2202" n="HIAT:ip">(</nts>
                  <ts e="T544" id="Seg_2204" n="HIAT:w" s="T543">dĭʔən</ts>
                  <nts id="Seg_2205" n="HIAT:ip">)</nts>
                  <nts id="Seg_2206" n="HIAT:ip">.</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T552" id="Seg_2209" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2211" n="HIAT:w" s="T544">Ularən</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2214" n="HIAT:w" s="T545">kăʔbde</ts>
                  <nts id="Seg_2215" n="HIAT:ip">,</nts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2218" n="HIAT:w" s="T546">ularəʔi</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2221" n="HIAT:w" s="T547">tordə</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2223" n="HIAT:ip">(</nts>
                  <ts e="T549" id="Seg_2225" n="HIAT:w" s="T548">bod-</ts>
                  <nts id="Seg_2226" n="HIAT:ip">)</nts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2229" n="HIAT:w" s="T549">băʔsittə</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2232" n="HIAT:w" s="T550">miʔnʼibeʔ</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2235" n="HIAT:w" s="T551">kereʔ</ts>
                  <nts id="Seg_2236" n="HIAT:ip">.</nts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2239" n="HIAT:u" s="T552">
                  <nts id="Seg_2240" n="HIAT:ip">(</nts>
                  <nts id="Seg_2241" n="HIAT:ip">(</nts>
                  <ats e="T553" id="Seg_2242" n="HIAT:non-pho" s="T552">BRK</ats>
                  <nts id="Seg_2243" n="HIAT:ip">)</nts>
                  <nts id="Seg_2244" n="HIAT:ip">)</nts>
                  <nts id="Seg_2245" n="HIAT:ip">.</nts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2248" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2250" n="HIAT:w" s="T553">Jakše</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2253" n="HIAT:w" s="T554">pʼet</ts>
                  <nts id="Seg_2254" n="HIAT:ip">.</nts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_2257" n="HIAT:u" s="T555">
                  <nts id="Seg_2258" n="HIAT:ip">(</nts>
                  <nts id="Seg_2259" n="HIAT:ip">(</nts>
                  <ats e="T556" id="Seg_2260" n="HIAT:non-pho" s="T555">BRK</ats>
                  <nts id="Seg_2261" n="HIAT:ip">)</nts>
                  <nts id="Seg_2262" n="HIAT:ip">)</nts>
                  <nts id="Seg_2263" n="HIAT:ip">.</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2266" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2268" n="HIAT:w" s="T556">Măn</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2271" n="HIAT:w" s="T557">taldʼen</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2274" n="HIAT:w" s="T558">pʼeštə</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2276" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_2278" n="HIAT:w" s="T559">še-</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2281" n="HIAT:w" s="T560">səbia-</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2284" n="HIAT:w" s="T561">š-</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2287" n="HIAT:w" s="T562">šabiam-</ts>
                  <nts id="Seg_2288" n="HIAT:ip">)</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2291" n="HIAT:w" s="T563">sʼabiam</ts>
                  <nts id="Seg_2292" n="HIAT:ip">.</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2295" n="HIAT:u" s="T564">
                  <ts e="T565" id="Seg_2297" n="HIAT:w" s="T564">Ej</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2300" n="HIAT:w" s="T565">amzittə</ts>
                  <nts id="Seg_2301" n="HIAT:ip">.</nts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2304" n="HIAT:u" s="T566">
                  <nts id="Seg_2305" n="HIAT:ip">(</nts>
                  <ts e="T567" id="Seg_2307" n="HIAT:w" s="T566">Va-</ts>
                  <nts id="Seg_2308" n="HIAT:ip">)</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2311" n="HIAT:w" s="T567">Vanʼka</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2314" n="HIAT:w" s="T568">abatsi</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2317" n="HIAT:w" s="T569">šobiʔi</ts>
                  <nts id="Seg_2318" n="HIAT:ip">.</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2321" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2323" n="HIAT:w" s="T570">Măndə:</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2326" n="HIAT:w" s="T571">Urgaja</ts>
                  <nts id="Seg_2327" n="HIAT:ip">,</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2329" n="HIAT:ip">(</nts>
                  <ts e="T573" id="Seg_2331" n="HIAT:w" s="T572">kutʼida</ts>
                  <nts id="Seg_2332" n="HIAT:ip">)</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2335" n="HIAT:w" s="T573">girgit</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2338" n="HIAT:w" s="T574">kola</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2341" n="HIAT:w" s="T575">bar</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2344" n="HIAT:w" s="T576">urgo</ts>
                  <nts id="Seg_2345" n="HIAT:ip">.</nts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2348" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2350" n="HIAT:w" s="T577">A</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2353" n="HIAT:w" s="T578">măn</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2356" n="HIAT:w" s="T579">mămbiam:</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2359" n="HIAT:w" s="T580">Kumən</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2362" n="HIAT:w" s="T581">dʼaʔpilaʔ</ts>
                  <nts id="Seg_2363" n="HIAT:ip">?</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T593" id="Seg_2366" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2368" n="HIAT:w" s="T582">Di</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2371" n="HIAT:w" s="T583">măndə:</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2374" n="HIAT:w" s="T584">Oʔb</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2377" n="HIAT:w" s="T585">šide</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2380" n="HIAT:w" s="T586">nagur</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2383" n="HIAT:w" s="T587">teʔtə</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2386" n="HIAT:w" s="T588">sumna</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2389" n="HIAT:w" s="T589">muktuʔ</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2391" n="HIAT:ip">(</nts>
                  <ts e="T591" id="Seg_2393" n="HIAT:w" s="T590">šejʔp-</ts>
                  <nts id="Seg_2394" n="HIAT:ip">)</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2397" n="HIAT:w" s="T591">sejʔpü</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2400" n="HIAT:w" s="T592">dʼaʔpibaʔ</ts>
                  <nts id="Seg_2401" n="HIAT:ip">"</nts>
                  <nts id="Seg_2402" n="HIAT:ip">.</nts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2405" n="HIAT:u" s="T593">
                  <nts id="Seg_2406" n="HIAT:ip">(</nts>
                  <nts id="Seg_2407" n="HIAT:ip">(</nts>
                  <ats e="T594" id="Seg_2408" n="HIAT:non-pho" s="T593">BRK</ats>
                  <nts id="Seg_2409" n="HIAT:ip">)</nts>
                  <nts id="Seg_2410" n="HIAT:ip">)</nts>
                  <nts id="Seg_2411" n="HIAT:ip">.</nts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2414" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2416" n="HIAT:w" s="T594">Fašistəʔi</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2419" n="HIAT:w" s="T595">miʔ</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2422" n="HIAT:w" s="T596">il</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2425" n="HIAT:w" s="T597">üge</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2427" n="HIAT:ip">(</nts>
                  <ts e="T599" id="Seg_2429" n="HIAT:w" s="T598">b-</ts>
                  <nts id="Seg_2430" n="HIAT:ip">)</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2433" n="HIAT:w" s="T599">bar</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2436" n="HIAT:w" s="T600">dʼabərolaʔpiʔi</ts>
                  <nts id="Seg_2437" n="HIAT:ip">.</nts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2440" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2442" n="HIAT:w" s="T601">Šidegöʔ</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2445" n="HIAT:w" s="T602">maʔnən</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2448" n="HIAT:w" s="T603">amnolaʔbi</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2451" n="HIAT:w" s="T604">bar</ts>
                  <nts id="Seg_2452" n="HIAT:ip">.</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2455" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2457" n="HIAT:w" s="T605">Griška</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2460" n="HIAT:w" s="T606">mĭmbi</ts>
                  <nts id="Seg_2461" n="HIAT:ip">,</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2464" n="HIAT:w" s="T607">kola</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2467" n="HIAT:w" s="T608">dʼaʔpi</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2469" n="HIAT:ip">(</nts>
                  <ts e="T610" id="Seg_2471" n="HIAT:w" s="T609">da</ts>
                  <nts id="Seg_2472" n="HIAT:ip">)</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2475" n="HIAT:w" s="T610">maʔndə</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2478" n="HIAT:w" s="T611">deʔpi</ts>
                  <nts id="Seg_2479" n="HIAT:ip">.</nts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2482" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2484" n="HIAT:w" s="T612">Nabəʔi</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2487" n="HIAT:w" s="T613">oʔb</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2490" n="HIAT:w" s="T614">šide</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2493" n="HIAT:w" s="T615">nagur</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2496" n="HIAT:w" s="T616">detləj</ts>
                  <nts id="Seg_2497" n="HIAT:ip">.</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2500" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2502" n="HIAT:w" s="T617">A</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2505" n="HIAT:w" s="T618">măn</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2507" n="HIAT:ip">(</nts>
                  <ts e="T620" id="Seg_2509" n="HIAT:w" s="T619">ul-</ts>
                  <nts id="Seg_2510" n="HIAT:ip">)</nts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2513" n="HIAT:w" s="T620">ular</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2515" n="HIAT:ip">(</nts>
                  <ts e="T622" id="Seg_2517" n="HIAT:w" s="T621">surdəbiam</ts>
                  <nts id="Seg_2518" n="HIAT:ip">)</nts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2522" n="HIAT:u" s="T622">
                  <nts id="Seg_2523" n="HIAT:ip">(</nts>
                  <nts id="Seg_2524" n="HIAT:ip">(</nts>
                  <ats e="T623" id="Seg_2525" n="HIAT:non-pho" s="T622">BRK</ats>
                  <nts id="Seg_2526" n="HIAT:ip">)</nts>
                  <nts id="Seg_2527" n="HIAT:ip">)</nts>
                  <nts id="Seg_2528" n="HIAT:ip">.</nts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2531" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2533" n="HIAT:w" s="T623">Măn</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2536" n="HIAT:w" s="T624">ugandə</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2538" n="HIAT:ip">(</nts>
                  <ts e="T626" id="Seg_2540" n="HIAT:w" s="T625">kălxozəndə</ts>
                  <nts id="Seg_2541" n="HIAT:ip">)</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2544" n="HIAT:w" s="T626">tăŋ</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2547" n="HIAT:w" s="T627">togonorbiam</ts>
                  <nts id="Seg_2548" n="HIAT:ip">,</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2551" n="HIAT:w" s="T628">iʔgö</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2554" n="HIAT:w" s="T629">aš</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2557" n="HIAT:w" s="T630">püdəbiem</ts>
                  <nts id="Seg_2558" n="HIAT:ip">.</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2561" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2563" n="HIAT:w" s="T631">Bʼeʔ</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2566" n="HIAT:w" s="T632">šide</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2569" n="HIAT:w" s="T633">püdəbiem</ts>
                  <nts id="Seg_2570" n="HIAT:ip">,</nts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2573" n="HIAT:w" s="T634">iššo</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2576" n="HIAT:w" s="T635">šide</ts>
                  <nts id="Seg_2577" n="HIAT:ip">.</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2580" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2582" n="HIAT:w" s="T636">Dĭgəttə</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2584" n="HIAT:ip">(</nts>
                  <ts e="T638" id="Seg_2586" n="HIAT:w" s="T637">onʼiʔ=</ts>
                  <nts id="Seg_2587" n="HIAT:ip">)</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2590" n="HIAT:w" s="T638">onʼiʔ</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2593" n="HIAT:w" s="T639">dʼala</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2595" n="HIAT:ip">(</nts>
                  <ts e="T641" id="Seg_2597" n="HIAT:w" s="T640">n-</ts>
                  <nts id="Seg_2598" n="HIAT:ip">)</nts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2601" n="HIAT:w" s="T641">nagur</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2604" n="HIAT:w" s="T642">bʼeʔ</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2607" n="HIAT:w" s="T643">püdəbiem</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2610" n="HIAT:w" s="T644">i</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2613" n="HIAT:w" s="T645">teʔtə</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2616" n="HIAT:w" s="T646">iššo</ts>
                  <nts id="Seg_2617" n="HIAT:ip">.</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2620" n="HIAT:u" s="T647">
                  <nts id="Seg_2621" n="HIAT:ip">(</nts>
                  <nts id="Seg_2622" n="HIAT:ip">(</nts>
                  <ats e="T648" id="Seg_2623" n="HIAT:non-pho" s="T647">BRK</ats>
                  <nts id="Seg_2624" n="HIAT:ip">)</nts>
                  <nts id="Seg_2625" n="HIAT:ip">)</nts>
                  <nts id="Seg_2626" n="HIAT:ip">.</nts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2629" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2631" n="HIAT:w" s="T648">Sotkaʔi</ts>
                  <nts id="Seg_2632" n="HIAT:ip">.</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2635" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2637" n="HIAT:w" s="T649">Măn</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2640" n="HIAT:w" s="T650">tugandə</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2643" n="HIAT:w" s="T651">surarlaʔbə:</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2646" n="HIAT:w" s="T652">Nada</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2649" n="HIAT:w" s="T653">măna</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2652" n="HIAT:w" s="T654">nʼilgösʼtə</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2655" n="HIAT:w" s="T655">kanzittə</ts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2658" n="HIAT:w" s="T656">šiʔnʼileʔ</ts>
                  <nts id="Seg_2659" n="HIAT:ip">,</nts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2662" n="HIAT:w" s="T657">kăde</ts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2665" n="HIAT:w" s="T658">dʼăbaktərlialaʔ</ts>
                  <nts id="Seg_2666" n="HIAT:ip">.</nts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2669" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2671" n="HIAT:w" s="T659">Šoʔ</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2674" n="HIAT:w" s="T660">kărəldʼan</ts>
                  <nts id="Seg_2675" n="HIAT:ip">,</nts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2678" n="HIAT:w" s="T661">dĭzeŋ</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2681" n="HIAT:w" s="T662">šoləʔjə</ts>
                  <nts id="Seg_2682" n="HIAT:ip">.</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2685" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2687" n="HIAT:w" s="T663">Nʼilgöt</ts>
                  <nts id="Seg_2688" n="HIAT:ip">"</nts>
                  <nts id="Seg_2689" n="HIAT:ip">.</nts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T665" id="Seg_2692" n="HIAT:u" s="T664">
                  <nts id="Seg_2693" n="HIAT:ip">(</nts>
                  <nts id="Seg_2694" n="HIAT:ip">(</nts>
                  <ats e="T665" id="Seg_2695" n="HIAT:non-pho" s="T664">BRK</ats>
                  <nts id="Seg_2696" n="HIAT:ip">)</nts>
                  <nts id="Seg_2697" n="HIAT:ip">)</nts>
                  <nts id="Seg_2698" n="HIAT:ip">.</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2701" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_2703" n="HIAT:w" s="T665">Măn</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2706" n="HIAT:w" s="T666">bar</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2709" n="HIAT:w" s="T667">üdʼüge</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2712" n="HIAT:w" s="T668">kurizəʔi</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2715" n="HIAT:w" s="T669">uʔbdlaʔbəʔjə</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2719" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2721" n="HIAT:w" s="T670">Bar</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2724" n="HIAT:w" s="T671">sʼujolaʔbəʔjə</ts>
                  <nts id="Seg_2725" n="HIAT:ip">.</nts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2728" n="HIAT:u" s="T672">
                  <nts id="Seg_2729" n="HIAT:ip">(</nts>
                  <nts id="Seg_2730" n="HIAT:ip">(</nts>
                  <ats e="T673" id="Seg_2731" n="HIAT:non-pho" s="T672">BRK</ats>
                  <nts id="Seg_2732" n="HIAT:ip">)</nts>
                  <nts id="Seg_2733" n="HIAT:ip">)</nts>
                  <nts id="Seg_2734" n="HIAT:ip">.</nts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2737" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2739" n="HIAT:w" s="T673">Nʼeʔtə</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2742" n="HIAT:w" s="T674">udal</ts>
                  <nts id="Seg_2743" n="HIAT:ip">.</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2746" n="HIAT:u" s="T675">
                  <nts id="Seg_2747" n="HIAT:ip">(</nts>
                  <nts id="Seg_2748" n="HIAT:ip">(</nts>
                  <ats e="T676" id="Seg_2749" n="HIAT:non-pho" s="T675">BRK</ats>
                  <nts id="Seg_2750" n="HIAT:ip">)</nts>
                  <nts id="Seg_2751" n="HIAT:ip">)</nts>
                  <nts id="Seg_2752" n="HIAT:ip">.</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2755" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2757" n="HIAT:w" s="T676">Taldʼen</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2760" n="HIAT:w" s="T677">nezeŋ</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2763" n="HIAT:w" s="T678">măna</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2766" n="HIAT:w" s="T679">šobiʔi</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2769" n="HIAT:w" s="T680">süt</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2772" n="HIAT:w" s="T681">kürzittə</ts>
                  <nts id="Seg_2773" n="HIAT:ip">.</nts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2776" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2778" n="HIAT:w" s="T682">Iʔgö</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2781" n="HIAT:w" s="T683">ibiʔi</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2784" n="HIAT:w" s="T684">dĭn</ts>
                  <nts id="Seg_2785" n="HIAT:ip">.</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2788" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2790" n="HIAT:w" s="T685">A</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2792" n="HIAT:ip">(</nts>
                  <ts e="T687" id="Seg_2794" n="HIAT:w" s="T686">pretsed-</ts>
                  <nts id="Seg_2795" n="HIAT:ip">)</nts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2798" n="HIAT:w" s="T687">predsedatelʼən</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2801" n="HIAT:w" s="T688">net</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2804" n="HIAT:w" s="T689">amnobi</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2807" n="HIAT:w" s="T690">dön</ts>
                  <nts id="Seg_2808" n="HIAT:ip">.</nts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2811" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2813" n="HIAT:w" s="T691">Nʼit</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2816" n="HIAT:w" s="T692">šobi</ts>
                  <nts id="Seg_2817" n="HIAT:ip">.</nts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2820" n="HIAT:u" s="T693">
                  <nts id="Seg_2821" n="HIAT:ip">(</nts>
                  <ts e="T694" id="Seg_2823" n="HIAT:w" s="T693">Măn-</ts>
                  <nts id="Seg_2824" n="HIAT:ip">)</nts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2827" n="HIAT:w" s="T694">Dĭʔnə</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2830" n="HIAT:w" s="T695">măndə:</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2833" n="HIAT:w" s="T696">kanžəbəj</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2836" n="HIAT:w" s="T697">maʔnʼibeʔ</ts>
                  <nts id="Seg_2837" n="HIAT:ip">,</nts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2840" n="HIAT:w" s="T698">miʔ</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2843" n="HIAT:w" s="T699">Krasnojarskəʔi</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2846" n="HIAT:w" s="T700">kalləbaʔ</ts>
                  <nts id="Seg_2847" n="HIAT:ip">.</nts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2850" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2852" n="HIAT:w" s="T701">A</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2855" n="HIAT:w" s="T702">măn</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2858" n="HIAT:w" s="T703">mămbiam</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2861" n="HIAT:w" s="T704">dĭʔnə:</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2864" n="HIAT:w" s="T705">Igeʔ</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2867" n="HIAT:w" s="T706">ianəl</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2870" n="HIAT:w" s="T707">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2873" n="HIAT:w" s="T708">kuvas</ts>
                  <nts id="Seg_2874" n="HIAT:ip">,</nts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2877" n="HIAT:w" s="T709">plat</ts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2879" n="HIAT:ip">(</nts>
                  <ts e="T711" id="Seg_2881" n="HIAT:w" s="T710">ib-</ts>
                  <nts id="Seg_2882" n="HIAT:ip">)</nts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2885" n="HIAT:w" s="T711">igeʔ</ts>
                  <nts id="Seg_2886" n="HIAT:ip">.</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2889" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2891" n="HIAT:w" s="T712">Oldʼa</ts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2894" n="HIAT:w" s="T713">it</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2897" n="HIAT:w" s="T714">kuvas</ts>
                  <nts id="Seg_2898" n="HIAT:ip">.</nts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2901" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2903" n="HIAT:w" s="T715">Dĭzeŋ</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2906" n="HIAT:w" s="T716">bar</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2909" n="HIAT:w" s="T717">kaknarlaʔbəʔjə</ts>
                  <nts id="Seg_2910" n="HIAT:ip">,</nts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2913" n="HIAT:w" s="T718">što</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2916" n="HIAT:w" s="T719">măn</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2919" n="HIAT:w" s="T720">dăre</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2922" n="HIAT:w" s="T721">dʼăbaktərlaʔbəm</ts>
                  <nts id="Seg_2923" n="HIAT:ip">.</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2926" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_2928" n="HIAT:w" s="T722">Dĭzeŋ</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2931" n="HIAT:w" s="T723">ej</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2934" n="HIAT:w" s="T724">tĭmneʔi</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2937" n="HIAT:w" s="T725">dʼăbaktərzittə</ts>
                  <nts id="Seg_2938" n="HIAT:ip">,</nts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2941" n="HIAT:w" s="T726">они</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2944" n="HIAT:w" s="T727">не</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2947" n="HIAT:w" s="T728">знают</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2950" n="HIAT:w" s="T729">говорить</ts>
                  <nts id="Seg_2951" n="HIAT:ip">.</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2954" n="HIAT:u" s="T730">
                  <nts id="Seg_2955" n="HIAT:ip">(</nts>
                  <nts id="Seg_2956" n="HIAT:ip">(</nts>
                  <ats e="T731" id="Seg_2957" n="HIAT:non-pho" s="T730">BRK</ats>
                  <nts id="Seg_2958" n="HIAT:ip">)</nts>
                  <nts id="Seg_2959" n="HIAT:ip">)</nts>
                  <nts id="Seg_2960" n="HIAT:ip">.</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2963" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2965" n="HIAT:w" s="T731">Teinen</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2968" n="HIAT:w" s="T732">nüke</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2971" n="HIAT:w" s="T733">šobi</ts>
                  <nts id="Seg_2972" n="HIAT:ip">.</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2975" n="HIAT:u" s="T734">
                  <ts e="T735" id="Seg_2977" n="HIAT:w" s="T734">Nada</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2980" n="HIAT:w" s="T735">măna</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2983" n="HIAT:w" s="T736">toltanoʔ</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2986" n="HIAT:w" s="T737">amzittə</ts>
                  <nts id="Seg_2987" n="HIAT:ip">,</nts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2990" n="HIAT:w" s="T738">kallam</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2993" n="HIAT:w" s="T739">tüjö</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2996" n="HIAT:w" s="T740">bostə</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2999" n="HIAT:w" s="T741">nʼinə</ts>
                  <nts id="Seg_3000" n="HIAT:ip">,</nts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3003" n="HIAT:w" s="T742">puskaj</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3006" n="HIAT:w" s="T743">amnolləj</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3009" n="HIAT:w" s="T744">măna</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_3012" n="HIAT:w" s="T745">toltanoʔ</ts>
                  <nts id="Seg_3013" n="HIAT:ip">"</nts>
                  <nts id="Seg_3014" n="HIAT:ip">.</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_3017" n="HIAT:u" s="T746">
                  <nts id="Seg_3018" n="HIAT:ip">(</nts>
                  <nts id="Seg_3019" n="HIAT:ip">(</nts>
                  <ats e="T747" id="Seg_3020" n="HIAT:non-pho" s="T746">BRK</ats>
                  <nts id="Seg_3021" n="HIAT:ip">)</nts>
                  <nts id="Seg_3022" n="HIAT:ip">)</nts>
                  <nts id="Seg_3023" n="HIAT:ip">.</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_3026" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_3028" n="HIAT:w" s="T747">Măn</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3031" n="HIAT:w" s="T748">ibiem</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3033" n="HIAT:ip">(</nts>
                  <ts e="T750" id="Seg_3035" n="HIAT:w" s="T749">Zaozʼorkagən</ts>
                  <nts id="Seg_3036" n="HIAT:ip">)</nts>
                  <nts id="Seg_3037" n="HIAT:ip">.</nts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_3040" n="HIAT:u" s="T750">
                  <nts id="Seg_3041" n="HIAT:ip">(</nts>
                  <ts e="T751" id="Seg_3043" n="HIAT:w" s="T750">Xatʼel=</ts>
                  <nts id="Seg_3044" n="HIAT:ip">)</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3047" n="HIAT:w" s="T751">Kambiam</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3050" n="HIAT:w" s="T752">aptʼekanə</ts>
                  <nts id="Seg_3051" n="HIAT:ip">.</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T756" id="Seg_3054" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_3056" n="HIAT:w" s="T753">Simandə</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3059" n="HIAT:w" s="T754">xatʼel</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3062" n="HIAT:w" s="T755">izittə</ts>
                  <nts id="Seg_3063" n="HIAT:ip">.</nts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T759" id="Seg_3066" n="HIAT:u" s="T756">
                  <ts e="T757" id="Seg_3068" n="HIAT:w" s="T756">Štobɨ</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3071" n="HIAT:w" s="T757">măndərzittə</ts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3074" n="HIAT:w" s="T758">kuŋgeŋ</ts>
                  <nts id="Seg_3075" n="HIAT:ip">.</nts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T764" id="Seg_3078" n="HIAT:u" s="T759">
                  <ts e="T760" id="Seg_3080" n="HIAT:w" s="T759">Dĭzeŋ</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3083" n="HIAT:w" s="T760">ej</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3086" n="HIAT:w" s="T761">mĭbiʔi</ts>
                  <nts id="Seg_3087" n="HIAT:ip">,</nts>
                  <nts id="Seg_3088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3090" n="HIAT:w" s="T762">măndlaʔbəʔjə:</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3093" n="HIAT:w" s="T763">Kanaʔ</ts>
                  <nts id="Seg_3094" n="HIAT:ip">.</nts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_3097" n="HIAT:u" s="T764">
                  <ts e="T765" id="Seg_3099" n="HIAT:w" s="T764">Sʼimal</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3102" n="HIAT:w" s="T765">măndərdə</ts>
                  <nts id="Seg_3103" n="HIAT:ip">,</nts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3106" n="HIAT:w" s="T766">dĭgəttə</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3109" n="HIAT:w" s="T767">šolal</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3112" n="HIAT:w" s="T768">i</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3115" n="HIAT:w" s="T769">ilil</ts>
                  <nts id="Seg_3116" n="HIAT:ip">"</nts>
                  <nts id="Seg_3117" n="HIAT:ip">.</nts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3120" n="HIAT:u" s="T770">
                  <nts id="Seg_3121" n="HIAT:ip">(</nts>
                  <nts id="Seg_3122" n="HIAT:ip">(</nts>
                  <ats e="T771" id="Seg_3123" n="HIAT:non-pho" s="T770">BRK</ats>
                  <nts id="Seg_3124" n="HIAT:ip">)</nts>
                  <nts id="Seg_3125" n="HIAT:ip">)</nts>
                  <nts id="Seg_3126" n="HIAT:ip">.</nts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_3129" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3131" n="HIAT:w" s="T771">Dĭbər</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3134" n="HIAT:w" s="T772">kambiam</ts>
                  <nts id="Seg_3135" n="HIAT:ip">,</nts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3138" n="HIAT:w" s="T773">dĭzeŋ</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3141" n="HIAT:w" s="T774">bar</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3144" n="HIAT:w" s="T775">dʼabərolaʔbə</ts>
                  <nts id="Seg_3145" n="HIAT:ip">,</nts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3148" n="HIAT:w" s="T776">Ĭmbi</ts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3151" n="HIAT:w" s="T777">dʼabərolaʔbəlaʔ</ts>
                  <nts id="Seg_3152" n="HIAT:ip">?</nts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_3155" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_3157" n="HIAT:w" s="T778">Iʔ</ts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3160" n="HIAT:w" s="T779">kirgarlaʔ</ts>
                  <nts id="Seg_3161" n="HIAT:ip">.</nts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_3164" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_3166" n="HIAT:w" s="T780">Jakšəŋ</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3169" n="HIAT:w" s="T781">agaʔ</ts>
                  <nts id="Seg_3170" n="HIAT:ip">.</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_3173" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_3175" n="HIAT:w" s="T782">Tura</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3178" n="HIAT:w" s="T783">sĭbrejeʔ</ts>
                  <nts id="Seg_3179" n="HIAT:ip">"</nts>
                  <nts id="Seg_3180" n="HIAT:ip">.</nts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3183" n="HIAT:u" s="T784">
                  <nts id="Seg_3184" n="HIAT:ip">(</nts>
                  <nts id="Seg_3185" n="HIAT:ip">(</nts>
                  <ats e="T785" id="Seg_3186" n="HIAT:non-pho" s="T784">BRK</ats>
                  <nts id="Seg_3187" n="HIAT:ip">)</nts>
                  <nts id="Seg_3188" n="HIAT:ip">)</nts>
                  <nts id="Seg_3189" n="HIAT:ip">.</nts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3192" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_3194" n="HIAT:w" s="T785">Onʼiʔtə</ts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3197" n="HIAT:w" s="T786">mămbiam:</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3200" n="HIAT:w" s="T787">Kanaʔ</ts>
                  <nts id="Seg_3201" n="HIAT:ip">.</nts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3204" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3206" n="HIAT:w" s="T788">Lʼondə</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3209" n="HIAT:w" s="T789">toʔnardə</ts>
                  <nts id="Seg_3210" n="HIAT:ip">.</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3213" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3215" n="HIAT:w" s="T790">A</ts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3218" n="HIAT:w" s="T791">dĭ</ts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3221" n="HIAT:w" s="T792">pušaj</ts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3224" n="HIAT:w" s="T793">bü</ts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3227" n="HIAT:w" s="T794">tažorləj</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3230" n="HIAT:w" s="T795">moltʼanə</ts>
                  <nts id="Seg_3231" n="HIAT:ip">.</nts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_3234" n="HIAT:u" s="T796">
                  <nts id="Seg_3235" n="HIAT:ip">(</nts>
                  <nts id="Seg_3236" n="HIAT:ip">(</nts>
                  <ats e="T797" id="Seg_3237" n="HIAT:non-pho" s="T796">BRK</ats>
                  <nts id="Seg_3238" n="HIAT:ip">)</nts>
                  <nts id="Seg_3239" n="HIAT:ip">)</nts>
                  <nts id="Seg_3240" n="HIAT:ip">.</nts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_3243" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_3245" n="HIAT:w" s="T797">Măn</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3248" n="HIAT:w" s="T798">teinen</ts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3251" n="HIAT:w" s="T799">erte</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3254" n="HIAT:w" s="T800">uʔbdəbiam</ts>
                  <nts id="Seg_3255" n="HIAT:ip">,</nts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3258" n="HIAT:w" s="T801">iššo</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3260" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_3262" n="HIAT:w" s="T802">š-</ts>
                  <nts id="Seg_3263" n="HIAT:ip">)</nts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3266" n="HIAT:w" s="T803">sumna</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3269" n="HIAT:w" s="T804">nagobi</ts>
                  <nts id="Seg_3270" n="HIAT:ip">.</nts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3273" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_3275" n="HIAT:w" s="T805">Dĭgəttə</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3278" n="HIAT:w" s="T806">pʼešbə</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3281" n="HIAT:w" s="T807">nendəbiem</ts>
                  <nts id="Seg_3282" n="HIAT:ip">.</nts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_3285" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_3287" n="HIAT:w" s="T808">Po</ts>
                  <nts id="Seg_3288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3290" n="HIAT:w" s="T809">nuldəbiam</ts>
                  <nts id="Seg_3291" n="HIAT:ip">.</nts>
                  <nts id="Seg_3292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T815" id="Seg_3294" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_3296" n="HIAT:w" s="T810">Dĭgəttə</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3299" n="HIAT:w" s="T811">bazoʔ</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3302" n="HIAT:w" s="T812">iʔbəbiem</ts>
                  <nts id="Seg_3303" n="HIAT:ip">,</nts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3306" n="HIAT:w" s="T813">iššo</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3309" n="HIAT:w" s="T814">kunolbiam</ts>
                  <nts id="Seg_3310" n="HIAT:ip">.</nts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3313" n="HIAT:u" s="T815">
                  <ts e="T816" id="Seg_3315" n="HIAT:w" s="T815">Iššo</ts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3318" n="HIAT:w" s="T816">bieʔ</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3321" n="HIAT:w" s="T817">mobi</ts>
                  <nts id="Seg_3322" n="HIAT:ip">,</nts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3325" n="HIAT:w" s="T818">dĭgəttə</ts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3327" n="HIAT:ip">(</nts>
                  <ts e="T820" id="Seg_3329" n="HIAT:w" s="T819">kolb-</ts>
                  <nts id="Seg_3330" n="HIAT:ip">)</nts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3333" n="HIAT:w" s="T820">uʔbdəbiam</ts>
                  <nts id="Seg_3334" n="HIAT:ip">.</nts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3337" n="HIAT:u" s="T821">
                  <nts id="Seg_3338" n="HIAT:ip">(</nts>
                  <nts id="Seg_3339" n="HIAT:ip">(</nts>
                  <ats e="T822" id="Seg_3340" n="HIAT:non-pho" s="T821">BRK</ats>
                  <nts id="Seg_3341" n="HIAT:ip">)</nts>
                  <nts id="Seg_3342" n="HIAT:ip">)</nts>
                  <nts id="Seg_3343" n="HIAT:ip">.</nts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T829" id="Seg_3346" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3348" n="HIAT:w" s="T822">Sedem</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3351" n="HIAT:w" s="T823">teinen</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3354" n="HIAT:w" s="T824">togonorbiam</ts>
                  <nts id="Seg_3355" n="HIAT:ip">,</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3358" n="HIAT:w" s="T825">a</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3361" n="HIAT:w" s="T826">taldʼen</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3364" n="HIAT:w" s="T827">ej</ts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3367" n="HIAT:w" s="T828">sedem</ts>
                  <nts id="Seg_3368" n="HIAT:ip">.</nts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3371" n="HIAT:u" s="T829">
                  <nts id="Seg_3372" n="HIAT:ip">(</nts>
                  <nts id="Seg_3373" n="HIAT:ip">(</nts>
                  <ats e="T830" id="Seg_3374" n="HIAT:non-pho" s="T829">BRK</ats>
                  <nts id="Seg_3375" n="HIAT:ip">)</nts>
                  <nts id="Seg_3376" n="HIAT:ip">)</nts>
                  <nts id="Seg_3377" n="HIAT:ip">.</nts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3380" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3382" n="HIAT:w" s="T830">Dĭ</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3385" n="HIAT:w" s="T831">pi</ts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3388" n="HIAT:w" s="T832">ugandə</ts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3391" n="HIAT:w" s="T833">urgo</ts>
                  <nts id="Seg_3392" n="HIAT:ip">,</nts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3395" n="HIAT:w" s="T834">sedem</ts>
                  <nts id="Seg_3396" n="HIAT:ip">.</nts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3399" n="HIAT:u" s="T835">
                  <ts e="T836" id="Seg_3401" n="HIAT:w" s="T835">A</ts>
                  <nts id="Seg_3402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3404" n="HIAT:w" s="T836">dö</ts>
                  <nts id="Seg_3405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3407" n="HIAT:w" s="T837">pa</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3410" n="HIAT:w" s="T838">ej</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3413" n="HIAT:w" s="T839">sedem</ts>
                  <nts id="Seg_3414" n="HIAT:ip">.</nts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T841" id="Seg_3417" n="HIAT:u" s="T840">
                  <nts id="Seg_3418" n="HIAT:ip">(</nts>
                  <nts id="Seg_3419" n="HIAT:ip">(</nts>
                  <ats e="T841" id="Seg_3420" n="HIAT:non-pho" s="T840">BRK</ats>
                  <nts id="Seg_3421" n="HIAT:ip">)</nts>
                  <nts id="Seg_3422" n="HIAT:ip">)</nts>
                  <nts id="Seg_3423" n="HIAT:ip">.</nts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3426" n="HIAT:u" s="T841">
                  <nts id="Seg_3427" n="HIAT:ip">(</nts>
                  <ts e="T842" id="Seg_3429" n="HIAT:w" s="T841">Dĭ</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3432" n="HIAT:w" s="T842">es-</ts>
                  <nts id="Seg_3433" n="HIAT:ip">)</nts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3436" n="HIAT:w" s="T843">Dĭ</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3439" n="HIAT:w" s="T844">ešši</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3442" n="HIAT:w" s="T845">bar</ts>
                  <nts id="Seg_3443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3445" n="HIAT:w" s="T846">piʔmezeŋdə</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3448" n="HIAT:w" s="T847">bar</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3451" n="HIAT:w" s="T848">sajnožuʔpi</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3454" n="HIAT:w" s="T849">bar</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3458" n="HIAT:w" s="T850">ši</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3461" n="HIAT:w" s="T851">molaːmbi</ts>
                  <nts id="Seg_3462" n="HIAT:ip">.</nts>
                  <nts id="Seg_3463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T855" id="Seg_3465" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3467" n="HIAT:w" s="T852">Nada</ts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3470" n="HIAT:w" s="T853">dĭm</ts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3473" n="HIAT:w" s="T854">šöʔsittə</ts>
                  <nts id="Seg_3474" n="HIAT:ip">.</nts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T857" id="Seg_3477" n="HIAT:u" s="T855">
                  <ts e="T856" id="Seg_3479" n="HIAT:w" s="T855">Заплатка</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3482" n="HIAT:w" s="T856">enzittə</ts>
                  <nts id="Seg_3483" n="HIAT:ip">.</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3486" n="HIAT:u" s="T857">
                  <nts id="Seg_3487" n="HIAT:ip">(</nts>
                  <nts id="Seg_3488" n="HIAT:ip">(</nts>
                  <ats e="T858" id="Seg_3489" n="HIAT:non-pho" s="T857">BRK</ats>
                  <nts id="Seg_3490" n="HIAT:ip">)</nts>
                  <nts id="Seg_3491" n="HIAT:ip">)</nts>
                  <nts id="Seg_3492" n="HIAT:ip">.</nts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_3495" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3497" n="HIAT:w" s="T858">Edəʔ</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3500" n="HIAT:w" s="T859">măna</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3503" n="HIAT:w" s="T860">sĭreʔpne</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3506" n="HIAT:w" s="T861">sumna</ts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3509" n="HIAT:w" s="T862">kilogram</ts>
                  <nts id="Seg_3510" n="HIAT:ip">.</nts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T864" id="Seg_3513" n="HIAT:u" s="T863">
                  <nts id="Seg_3514" n="HIAT:ip">(</nts>
                  <nts id="Seg_3515" n="HIAT:ip">(</nts>
                  <ats e="T864" id="Seg_3516" n="HIAT:non-pho" s="T863">BRK</ats>
                  <nts id="Seg_3517" n="HIAT:ip">)</nts>
                  <nts id="Seg_3518" n="HIAT:ip">)</nts>
                  <nts id="Seg_3519" n="HIAT:ip">.</nts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3522" n="HIAT:u" s="T864">
                  <ts e="T865" id="Seg_3524" n="HIAT:w" s="T864">Dĭ</ts>
                  <nts id="Seg_3525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3527" n="HIAT:w" s="T865">kuza</ts>
                  <nts id="Seg_3528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3530" n="HIAT:w" s="T866">ĭzembi</ts>
                  <nts id="Seg_3531" n="HIAT:ip">,</nts>
                  <nts id="Seg_3532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3534" n="HIAT:w" s="T867">a</ts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3537" n="HIAT:w" s="T868">tüj</ts>
                  <nts id="Seg_3538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3540" n="HIAT:w" s="T869">ej</ts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3543" n="HIAT:w" s="T870">ĭzemnie</ts>
                  <nts id="Seg_3544" n="HIAT:ip">,</nts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3547" n="HIAT:w" s="T871">uʔbdəbi</ts>
                  <nts id="Seg_3548" n="HIAT:ip">,</nts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3551" n="HIAT:w" s="T872">üjüzi</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3554" n="HIAT:w" s="T873">mĭŋge</ts>
                  <nts id="Seg_3555" n="HIAT:ip">,</nts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874.tx-PKZ.1" id="Seg_3558" n="HIAT:w" s="T874">a</ts>
                  <nts id="Seg_3559" n="HIAT:ip">_</nts>
                  <ts e="T875" id="Seg_3561" n="HIAT:w" s="T874.tx-PKZ.1">to</ts>
                  <nts id="Seg_3562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3564" n="HIAT:w" s="T875">iʔbobi</ts>
                  <nts id="Seg_3565" n="HIAT:ip">.</nts>
                  <nts id="Seg_3566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3568" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3570" n="HIAT:w" s="T876">Ej</ts>
                  <nts id="Seg_3571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3573" n="HIAT:w" s="T877">jakšə</ts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3576" n="HIAT:w" s="T878">nʼilgölaʔsittə</ts>
                  <nts id="Seg_3577" n="HIAT:ip">.</nts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3580" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_3582" n="HIAT:w" s="T879">Šobiʔi</ts>
                  <nts id="Seg_3583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3585" n="HIAT:w" s="T880">da</ts>
                  <nts id="Seg_3586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3588" n="HIAT:w" s="T881">mămbiʔi</ts>
                  <nts id="Seg_3589" n="HIAT:ip">,</nts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3592" n="HIAT:w" s="T882">ej</ts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3595" n="HIAT:w" s="T883">kuvas</ts>
                  <nts id="Seg_3596" n="HIAT:ip">.</nts>
                  <nts id="Seg_3597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T885" id="Seg_3599" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3601" n="HIAT:w" s="T884">Dʼăbaktərla</ts>
                  <nts id="Seg_3602" n="HIAT:ip">.</nts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T886" id="Seg_3605" n="HIAT:u" s="T885">
                  <nts id="Seg_3606" n="HIAT:ip">(</nts>
                  <nts id="Seg_3607" n="HIAT:ip">(</nts>
                  <ats e="T886" id="Seg_3608" n="HIAT:non-pho" s="T885">BRK</ats>
                  <nts id="Seg_3609" n="HIAT:ip">)</nts>
                  <nts id="Seg_3610" n="HIAT:ip">)</nts>
                  <nts id="Seg_3611" n="HIAT:ip">.</nts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3614" n="HIAT:u" s="T886">
                  <nts id="Seg_3615" n="HIAT:ip">(</nts>
                  <ts e="T887" id="Seg_3617" n="HIAT:w" s="T886">Dĭ=</ts>
                  <nts id="Seg_3618" n="HIAT:ip">)</nts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3621" n="HIAT:w" s="T887">dĭ</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3624" n="HIAT:w" s="T888">bar</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3627" n="HIAT:w" s="T889">kuzam</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3630" n="HIAT:w" s="T890">kuʔpi</ts>
                  <nts id="Seg_3631" n="HIAT:ip">.</nts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T893" id="Seg_3634" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3636" n="HIAT:w" s="T891">Dĭm</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3639" n="HIAT:w" s="T892">amnolbiʔi</ts>
                  <nts id="Seg_3640" n="HIAT:ip">.</nts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T903" id="Seg_3643" n="HIAT:u" s="T893">
                  <nts id="Seg_3644" n="HIAT:ip">(</nts>
                  <ts e="T894" id="Seg_3646" n="HIAT:w" s="T893">Na-</ts>
                  <nts id="Seg_3647" n="HIAT:ip">)</nts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3650" n="HIAT:w" s="T894">Nagur</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3652" n="HIAT:ip">(</nts>
                  <ts e="T896" id="Seg_3654" n="HIAT:w" s="T895">kö</ts>
                  <nts id="Seg_3655" n="HIAT:ip">)</nts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3658" n="HIAT:w" s="T896">amnolaʔpi</ts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3661" n="HIAT:w" s="T897">i</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3664" n="HIAT:w" s="T898">tuj</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3667" n="HIAT:w" s="T899">maːʔndə</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3670" n="HIAT:w" s="T900">šobi</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3673" n="HIAT:w" s="T901">i</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3676" n="HIAT:w" s="T902">mĭlleʔbə</ts>
                  <nts id="Seg_3677" n="HIAT:ip">.</nts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3680" n="HIAT:u" s="T903">
                  <nts id="Seg_3681" n="HIAT:ip">(</nts>
                  <nts id="Seg_3682" n="HIAT:ip">(</nts>
                  <ats e="T904" id="Seg_3683" n="HIAT:non-pho" s="T903">BRK</ats>
                  <nts id="Seg_3684" n="HIAT:ip">)</nts>
                  <nts id="Seg_3685" n="HIAT:ip">)</nts>
                  <nts id="Seg_3686" n="HIAT:ip">.</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T910" id="Seg_3689" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3691" n="HIAT:w" s="T904">Bar</ts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3694" n="HIAT:w" s="T905">takšeʔi</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3696" n="HIAT:ip">(</nts>
                  <ts e="T907" id="Seg_3698" n="HIAT:w" s="T906">bădlu-</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3701" n="HIAT:w" s="T907">b-</ts>
                  <nts id="Seg_3702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3704" n="HIAT:w" s="T908">bădlu-</ts>
                  <nts id="Seg_3705" n="HIAT:ip">)</nts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3708" n="HIAT:w" s="T909">băldluʔpiʔi</ts>
                  <nts id="Seg_3709" n="HIAT:ip">.</nts>
                  <nts id="Seg_3710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3712" n="HIAT:u" s="T910">
                  <nts id="Seg_3713" n="HIAT:ip">(</nts>
                  <nts id="Seg_3714" n="HIAT:ip">(</nts>
                  <ats e="T911" id="Seg_3715" n="HIAT:non-pho" s="T910">BRK</ats>
                  <nts id="Seg_3716" n="HIAT:ip">)</nts>
                  <nts id="Seg_3717" n="HIAT:ip">)</nts>
                  <nts id="Seg_3718" n="HIAT:ip">.</nts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3721" n="HIAT:u" s="T911">
                  <nts id="Seg_3722" n="HIAT:ip">(</nts>
                  <nts id="Seg_3723" n="HIAT:ip">(</nts>
                  <ats e="T912" id="Seg_3724" n="HIAT:non-pho" s="T911">BRK</ats>
                  <nts id="Seg_3725" n="HIAT:ip">)</nts>
                  <nts id="Seg_3726" n="HIAT:ip">)</nts>
                  <nts id="Seg_3727" n="HIAT:ip">.</nts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T920" id="Seg_3730" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3732" n="HIAT:w" s="T912">Măn</ts>
                  <nts id="Seg_3733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3734" n="HIAT:ip">(</nts>
                  <ts e="T914" id="Seg_3736" n="HIAT:w" s="T913">ul-</ts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3739" n="HIAT:w" s="T914">ul-</ts>
                  <nts id="Seg_3740" n="HIAT:ip">)</nts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3743" n="HIAT:w" s="T915">ulum</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3746" n="HIAT:w" s="T916">bar</ts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3749" n="HIAT:w" s="T917">piziʔ</ts>
                  <nts id="Seg_3750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3752" n="HIAT:w" s="T918">toʔnarluʔpiʔi</ts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3755" n="HIAT:w" s="T919">bar</ts>
                  <nts id="Seg_3756" n="HIAT:ip">.</nts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3759" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3761" n="HIAT:w" s="T920">Bar</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3764" n="HIAT:w" s="T921">kem</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3767" n="HIAT:w" s="T922">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_3768" n="HIAT:ip">.</nts>
                  <nts id="Seg_3769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T930" id="Seg_3771" n="HIAT:u" s="T923">
                  <nts id="Seg_3772" n="HIAT:ip">(</nts>
                  <ts e="T925" id="Seg_3774" n="HIAT:w" s="T923">Măna</ts>
                  <nts id="Seg_3775" n="HIAT:ip">)</nts>
                  <nts id="Seg_3776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3778" n="HIAT:w" s="T925">baltuziʔ</ts>
                  <nts id="Seg_3779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3781" n="HIAT:w" s="T926">bar</ts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3784" n="HIAT:w" s="T927">ulum</ts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3786" n="HIAT:ip">(</nts>
                  <ts e="T930" id="Seg_3788" n="HIAT:w" s="T928">bă-</ts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3790" n="HIAT:ip">)</nts>
                  <nts id="Seg_3791" n="HIAT:ip">…</nts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_3794" n="HIAT:u" s="T930">
                  <nts id="Seg_3795" n="HIAT:ip">(</nts>
                  <nts id="Seg_3796" n="HIAT:ip">(</nts>
                  <ats e="T931" id="Seg_3797" n="HIAT:non-pho" s="T930">BRK</ats>
                  <nts id="Seg_3798" n="HIAT:ip">)</nts>
                  <nts id="Seg_3799" n="HIAT:ip">)</nts>
                  <nts id="Seg_3800" n="HIAT:ip">.</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T932" id="Seg_3803" n="HIAT:u" s="T931">
                  <ts e="T932" id="Seg_3805" n="HIAT:w" s="T931">Băldəbiʔi</ts>
                  <nts id="Seg_3806" n="HIAT:ip">.</nts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T937" id="Seg_3809" n="HIAT:u" s="T932">
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3812" n="HIAT:w" s="T932">Dʼijenə</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3815" n="HIAT:w" s="T934">kalal</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3818" n="HIAT:w" s="T935">keʔbde</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3821" n="HIAT:w" s="T936">oʔbdəsʼtə</ts>
                  <nts id="Seg_3822" n="HIAT:ip">.</nts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_3825" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_3827" n="HIAT:w" s="T937">Kanžəbaʔ</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3830" n="HIAT:w" s="T938">maʔnʼibeʔ</ts>
                  <nts id="Seg_3831" n="HIAT:ip">,</nts>
                  <nts id="Seg_3832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3834" n="HIAT:w" s="T939">keʔbde</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3837" n="HIAT:w" s="T940">naga</ts>
                  <nts id="Seg_3838" n="HIAT:ip">.</nts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_3841" n="HIAT:u" s="T941">
                  <ts e="T942" id="Seg_3843" n="HIAT:w" s="T941">Tolʼko</ts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3845" n="HIAT:ip">(</nts>
                  <ts e="T943" id="Seg_3847" n="HIAT:w" s="T942">ka-</ts>
                  <nts id="Seg_3848" n="HIAT:ip">)</nts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3851" n="HIAT:w" s="T943">kanzittə</ts>
                  <nts id="Seg_3852" n="HIAT:ip">.</nts>
                  <nts id="Seg_3853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T949" id="Seg_3855" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3857" n="HIAT:w" s="T944">Gijendə</ts>
                  <nts id="Seg_3858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3860" n="HIAT:w" s="T945">šoləʔjə</ts>
                  <nts id="Seg_3861" n="HIAT:ip">,</nts>
                  <nts id="Seg_3862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3864" n="HIAT:w" s="T946">ugandə</ts>
                  <nts id="Seg_3865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3867" n="HIAT:w" s="T947">iʔgö</ts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3870" n="HIAT:w" s="T948">keʔbde</ts>
                  <nts id="Seg_3871" n="HIAT:ip">.</nts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T952" id="Seg_3874" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_3876" n="HIAT:w" s="T949">Dĭgəttə</ts>
                  <nts id="Seg_3877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3879" n="HIAT:w" s="T950">bazoʔ</ts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3882" n="HIAT:w" s="T951">oʔbdəlial</ts>
                  <nts id="Seg_3883" n="HIAT:ip">.</nts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T960" id="Seg_3886" n="HIAT:u" s="T952">
                  <ts e="T953" id="Seg_3888" n="HIAT:w" s="T952">Dĭgəttə</ts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3890" n="HIAT:ip">(</nts>
                  <ts e="T954" id="Seg_3892" n="HIAT:w" s="T953">moliam</ts>
                  <nts id="Seg_3893" n="HIAT:ip">)</nts>
                  <nts id="Seg_3894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3896" n="HIAT:w" s="T954">kamen</ts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3899" n="HIAT:w" s="T955">maʔnʼi</ts>
                  <nts id="Seg_3900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3902" n="HIAT:w" s="T956">kanzittə</ts>
                  <nts id="Seg_3903" n="HIAT:ip">,</nts>
                  <nts id="Seg_3904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3906" n="HIAT:w" s="T957">üge</ts>
                  <nts id="Seg_3907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3909" n="HIAT:w" s="T958">keʔbde</ts>
                  <nts id="Seg_3910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3912" n="HIAT:w" s="T959">iʔgö</ts>
                  <nts id="Seg_3913" n="HIAT:ip">.</nts>
                  <nts id="Seg_3914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T961" id="Seg_3916" n="HIAT:u" s="T960">
                  <nts id="Seg_3917" n="HIAT:ip">(</nts>
                  <nts id="Seg_3918" n="HIAT:ip">(</nts>
                  <ats e="T961" id="Seg_3919" n="HIAT:non-pho" s="T960">…</ats>
                  <nts id="Seg_3920" n="HIAT:ip">)</nts>
                  <nts id="Seg_3921" n="HIAT:ip">)</nts>
                  <nts id="Seg_3922" n="HIAT:ip">.</nts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T962" id="Seg_3925" n="HIAT:u" s="T961">
                  <nts id="Seg_3926" n="HIAT:ip">(</nts>
                  <nts id="Seg_3927" n="HIAT:ip">(</nts>
                  <ats e="T962" id="Seg_3928" n="HIAT:non-pho" s="T961">…</ats>
                  <nts id="Seg_3929" n="HIAT:ip">)</nts>
                  <nts id="Seg_3930" n="HIAT:ip">)</nts>
                  <nts id="Seg_3931" n="HIAT:ip">.</nts>
                  <nts id="Seg_3932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T962" id="Seg_3933" n="sc" s="T4">
               <ts e="T5" id="Seg_3935" n="e" s="T4">Măn </ts>
               <ts e="T6" id="Seg_3937" n="e" s="T5">teinen </ts>
               <ts e="T7" id="Seg_3939" n="e" s="T6">sumuranə </ts>
               <ts e="T8" id="Seg_3941" n="e" s="T7">mĭmbiem. </ts>
               <ts e="T9" id="Seg_3943" n="e" s="T8">Dĭn </ts>
               <ts e="T10" id="Seg_3945" n="e" s="T9">nüke </ts>
               <ts e="T11" id="Seg_3947" n="e" s="T10">amnolaʔbə. </ts>
               <ts e="T12" id="Seg_3949" n="e" s="T11">Amaʔ, </ts>
               <ts e="T13" id="Seg_3951" n="e" s="T12">amoraʔ </ts>
               <ts e="T14" id="Seg_3953" n="e" s="T13">kăpusta! </ts>
               <ts e="T15" id="Seg_3955" n="e" s="T14">Oj, </ts>
               <ts e="T16" id="Seg_3957" n="e" s="T15">ugandə </ts>
               <ts e="T17" id="Seg_3959" n="e" s="T16">namzəga. </ts>
               <ts e="T18" id="Seg_3961" n="e" s="T17">Em </ts>
               <ts e="T19" id="Seg_3963" n="e" s="T18">amaʔ. </ts>
               <ts e="T20" id="Seg_3965" n="e" s="T19">Dĭgəttə </ts>
               <ts e="T21" id="Seg_3967" n="e" s="T20">koʔbdo </ts>
               <ts e="T22" id="Seg_3969" n="e" s="T21">kambi </ts>
               <ts e="T23" id="Seg_3971" n="e" s="T22">tüʔsittə, </ts>
               <ts e="T24" id="Seg_3973" n="e" s="T23">men </ts>
               <ts e="T25" id="Seg_3975" n="e" s="T24">(dĭnziʔ). </ts>
               <ts e="T26" id="Seg_3977" n="e" s="T25">Măn </ts>
               <ts e="T27" id="Seg_3979" n="e" s="T26">(m-)… </ts>
               <ts e="T28" id="Seg_3981" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_3983" n="e" s="T28">dĭ </ts>
               <ts e="T30" id="Seg_3985" n="e" s="T29">šobi, </ts>
               <ts e="T31" id="Seg_3987" n="e" s="T30">a </ts>
               <ts e="T32" id="Seg_3989" n="e" s="T31">măn </ts>
               <ts e="T33" id="Seg_3991" n="e" s="T32">dĭʔnə </ts>
               <ts e="T34" id="Seg_3993" n="e" s="T33">dʼăbaktərliam: </ts>
               <ts e="T35" id="Seg_3995" n="e" s="T34">Tăn </ts>
               <ts e="T36" id="Seg_3997" n="e" s="T35">mĭmbiel </ts>
               <ts e="T37" id="Seg_3999" n="e" s="T36">tüʔsittə, </ts>
               <ts e="T38" id="Seg_4001" n="e" s="T37">i </ts>
               <ts e="T39" id="Seg_4003" n="e" s="T38">men </ts>
               <ts e="T40" id="Seg_4005" n="e" s="T39">tănziʔ. </ts>
               <ts e="T41" id="Seg_4007" n="e" s="T40">Dĭ </ts>
               <ts e="T42" id="Seg_4009" n="e" s="T41">bar </ts>
               <ts e="T43" id="Seg_4011" n="e" s="T42">kakənarluʔpi, </ts>
               <ts e="T44" id="Seg_4013" n="e" s="T43">ugaːndə </ts>
               <ts e="T45" id="Seg_4015" n="e" s="T44">tăŋ. </ts>
               <ts e="T46" id="Seg_4017" n="e" s="T45">(Kĭškəʔi) </ts>
               <ts e="T47" id="Seg_4019" n="e" s="T46">bar </ts>
               <ts e="T48" id="Seg_4021" n="e" s="T47">(üzə-) </ts>
               <ts e="T49" id="Seg_4023" n="e" s="T48">üjüʔi, </ts>
               <ts e="T50" id="Seg_4025" n="e" s="T49">ĭzemneʔbəʔjə </ts>
               <ts e="T51" id="Seg_4027" n="e" s="T50">ugaːndə. </ts>
               <ts e="T52" id="Seg_4029" n="e" s="T51">((BRK)). </ts>
               <ts e="T53" id="Seg_4031" n="e" s="T52">Iʔgö </ts>
               <ts e="T54" id="Seg_4033" n="e" s="T53">pʼe </ts>
               <ts e="T964" id="Seg_4035" n="e" s="T54">kalla </ts>
               <ts e="T55" id="Seg_4037" n="e" s="T964">dʼürbiʔi, </ts>
               <ts e="T56" id="Seg_4039" n="e" s="T55">il </ts>
               <ts e="T57" id="Seg_4041" n="e" s="T56">bar </ts>
               <ts e="T58" id="Seg_4043" n="e" s="T57">ĭmbidə </ts>
               <ts e="T59" id="Seg_4045" n="e" s="T58">ej </ts>
               <ts e="T60" id="Seg_4047" n="e" s="T59">tĭmneʔi. </ts>
               <ts e="T61" id="Seg_4049" n="e" s="T60">Dĭzen </ts>
               <ts e="T62" id="Seg_4051" n="e" s="T61">bar </ts>
               <ts e="T63" id="Seg_4053" n="e" s="T62">(i-) </ts>
               <ts e="T64" id="Seg_4055" n="e" s="T63">iʔgö </ts>
               <ts e="T65" id="Seg_4057" n="e" s="T64">inezeŋdə </ts>
               <ts e="T66" id="Seg_4059" n="e" s="T65">i </ts>
               <ts e="T67" id="Seg_4061" n="e" s="T66">tüžöjəʔi </ts>
               <ts e="T68" id="Seg_4063" n="e" s="T67">iʔgö. </ts>
               <ts e="T69" id="Seg_4065" n="e" s="T68">I </ts>
               <ts e="T70" id="Seg_4067" n="e" s="T69">ular </ts>
               <ts e="T71" id="Seg_4069" n="e" s="T70">iʔgö, </ts>
               <ts e="T72" id="Seg_4071" n="e" s="T71">kurizəʔi </ts>
               <ts e="T73" id="Seg_4073" n="e" s="T72">iʔgö, </ts>
               <ts e="T74" id="Seg_4075" n="e" s="T73">uja </ts>
               <ts e="T75" id="Seg_4077" n="e" s="T74">(amnaʔbəʔjə) </ts>
               <ts e="T76" id="Seg_4079" n="e" s="T75">iʔgö </ts>
               <ts e="T77" id="Seg_4081" n="e" s="T76">bar. </ts>
               <ts e="T78" id="Seg_4083" n="e" s="T77">Süt </ts>
               <ts e="T79" id="Seg_4085" n="e" s="T78">bar </ts>
               <ts e="T80" id="Seg_4087" n="e" s="T79">iʔgö. </ts>
               <ts e="T81" id="Seg_4089" n="e" s="T80">((BRK)). </ts>
               <ts e="T82" id="Seg_4091" n="e" s="T81">Nuzaŋ </ts>
               <ts e="T83" id="Seg_4093" n="e" s="T82">bar </ts>
               <ts e="T84" id="Seg_4095" n="e" s="T83">amnolaʔpiʔi. </ts>
               <ts e="T85" id="Seg_4097" n="e" s="T84">Kudajdə </ts>
               <ts e="T86" id="Seg_4099" n="e" s="T85">abiʔi </ts>
               <ts e="T87" id="Seg_4101" n="e" s="T86">(pa-) </ts>
               <ts e="T88" id="Seg_4103" n="e" s="T87">paziʔ, </ts>
               <ts e="T89" id="Seg_4105" n="e" s="T88">dĭgəttə </ts>
               <ts e="T90" id="Seg_4107" n="e" s="T89">(pi-) </ts>
               <ts e="T91" id="Seg_4109" n="e" s="T90">pigəʔ </ts>
               <ts e="T92" id="Seg_4111" n="e" s="T91">abiʔi, </ts>
               <ts e="T93" id="Seg_4113" n="e" s="T92">dĭgəttə </ts>
               <ts e="T94" id="Seg_4115" n="e" s="T93">sazənzəbi </ts>
               <ts e="T95" id="Seg_4117" n="e" s="T94">abiʔi. </ts>
               <ts e="T96" id="Seg_4119" n="e" s="T95">((BRK)). </ts>
               <ts e="T97" id="Seg_4121" n="e" s="T96">Bazaj </ts>
               <ts e="T98" id="Seg_4123" n="e" s="T97">kudaj </ts>
               <ts e="T99" id="Seg_4125" n="e" s="T98">abiʔi </ts>
               <ts e="T100" id="Seg_4127" n="e" s="T99">i </ts>
               <ts e="T101" id="Seg_4129" n="e" s="T100">nuldəbiʔi </ts>
               <ts e="T102" id="Seg_4131" n="e" s="T101">bar. </ts>
               <ts e="T103" id="Seg_4133" n="e" s="T102">((BRK)). </ts>
               <ts e="T104" id="Seg_4135" n="e" s="T103">Dĭzeŋdə </ts>
               <ts e="T105" id="Seg_4137" n="e" s="T104">anʼi </ts>
               <ts e="T106" id="Seg_4139" n="e" s="T105">bar </ts>
               <ts e="T107" id="Seg_4141" n="e" s="T106">svečkaʔi </ts>
               <ts e="T108" id="Seg_4143" n="e" s="T107">nuldəbiʔi, </ts>
               <ts e="T109" id="Seg_4145" n="e" s="T108">nendəbiʔi. </ts>
               <ts e="T110" id="Seg_4147" n="e" s="T109">((BRK)). </ts>
               <ts e="T111" id="Seg_4149" n="e" s="T110">Nuzaŋ </ts>
               <ts e="T112" id="Seg_4151" n="e" s="T111">bar </ts>
               <ts e="T113" id="Seg_4153" n="e" s="T112">mĭmbiʔi </ts>
               <ts e="T114" id="Seg_4155" n="e" s="T113">dʼijegən. </ts>
               <ts e="T115" id="Seg_4157" n="e" s="T114">I </ts>
               <ts e="T116" id="Seg_4159" n="e" s="T115">dĭgəttə </ts>
               <ts e="T117" id="Seg_4161" n="e" s="T116">šišəge </ts>
               <ts e="T118" id="Seg_4163" n="e" s="T117">ibi </ts>
               <ts e="T119" id="Seg_4165" n="e" s="T118">dak </ts>
               <ts e="T120" id="Seg_4167" n="e" s="T119">šoləʔjə </ts>
               <ts e="T121" id="Seg_4169" n="e" s="T120">döbər </ts>
               <ts e="T122" id="Seg_4171" n="e" s="T121">bar. </ts>
               <ts e="T123" id="Seg_4173" n="e" s="T122">Dĭn </ts>
               <ts e="T124" id="Seg_4175" n="e" s="T123">stara </ts>
               <ts e="T125" id="Seg_4177" n="e" s="T124">stoibe </ts>
               <ts e="T126" id="Seg_4179" n="e" s="T125">amnobiʔi, </ts>
               <ts e="T127" id="Seg_4181" n="e" s="T126">(dĭgəttə) </ts>
               <ts e="T128" id="Seg_4183" n="e" s="T127">kubiʔi: </ts>
               <ts e="T129" id="Seg_4185" n="e" s="T128">Dön </ts>
               <ts e="T130" id="Seg_4187" n="e" s="T129">bü </ts>
               <ts e="T131" id="Seg_4189" n="e" s="T130">ej </ts>
               <ts e="T132" id="Seg_4191" n="e" s="T131">kănzəlia. </ts>
               <ts e="T133" id="Seg_4193" n="e" s="T132">Dĭgəttə </ts>
               <ts e="T134" id="Seg_4195" n="e" s="T133">dön </ts>
               <ts e="T135" id="Seg_4197" n="e" s="T134">(nub </ts>
               <ts e="T136" id="Seg_4199" n="e" s="T135">- </ts>
               <ts e="T137" id="Seg_4201" n="e" s="T136">nulə-) </ts>
               <ts e="T138" id="Seg_4203" n="e" s="T137">(nuldlaʔbəjəʔ) </ts>
               <ts e="T139" id="Seg_4205" n="e" s="T138">bar </ts>
               <ts e="T140" id="Seg_4207" n="e" s="T139">maʔsiʔ </ts>
               <ts e="T141" id="Seg_4209" n="e" s="T140">i </ts>
               <ts e="T142" id="Seg_4211" n="e" s="T141">dön </ts>
               <ts e="T143" id="Seg_4213" n="e" s="T142">amnolaʔpiʔi. </ts>
               <ts e="T144" id="Seg_4215" n="e" s="T143">Dĭzeŋ </ts>
               <ts e="T145" id="Seg_4217" n="e" s="T144">bar </ts>
               <ts e="T146" id="Seg_4219" n="e" s="T145">mĭmbiʔi. </ts>
               <ts e="T147" id="Seg_4221" n="e" s="T146">(Aktʼ </ts>
               <ts e="T148" id="Seg_4223" n="e" s="T147">- </ts>
               <ts e="T149" id="Seg_4225" n="e" s="T148">aktʼ-) </ts>
               <ts e="T150" id="Seg_4227" n="e" s="T149">Aktʼit </ts>
               <ts e="T151" id="Seg_4229" n="e" s="T150">bar </ts>
               <ts e="T152" id="Seg_4231" n="e" s="T151">(tădam) </ts>
               <ts e="T153" id="Seg_4233" n="e" s="T152">ibi. </ts>
               <ts e="T154" id="Seg_4235" n="e" s="T153">Onʼiʔ </ts>
               <ts e="T155" id="Seg_4237" n="e" s="T154">kandəga, </ts>
               <ts e="T156" id="Seg_4239" n="e" s="T155">dăre </ts>
               <ts e="T157" id="Seg_4241" n="e" s="T156">bar, </ts>
               <ts e="T158" id="Seg_4243" n="e" s="T157">kandəgaʔi </ts>
               <ts e="T159" id="Seg_4245" n="e" s="T158">kak </ts>
               <ts e="T160" id="Seg_4247" n="e" s="T159">nabəʔi. </ts>
               <ts e="T161" id="Seg_4249" n="e" s="T160">Dʼijenə </ts>
               <ts e="T162" id="Seg_4251" n="e" s="T161">kambiʔi. </ts>
               <ts e="T163" id="Seg_4253" n="e" s="T162">Onʼiʔ </ts>
               <ts e="T164" id="Seg_4255" n="e" s="T163">ej </ts>
               <ts e="T165" id="Seg_4257" n="e" s="T164">kallia, </ts>
               <ts e="T166" id="Seg_4259" n="e" s="T165">a </ts>
               <ts e="T167" id="Seg_4261" n="e" s="T166">iʔgö </ts>
               <ts e="T168" id="Seg_4263" n="e" s="T167">kaləʔi. </ts>
               <ts e="T169" id="Seg_4265" n="e" s="T168">((BRK)). </ts>
               <ts e="T170" id="Seg_4267" n="e" s="T169">Dĭzeŋ </ts>
               <ts e="T171" id="Seg_4269" n="e" s="T170">bar </ts>
               <ts e="T172" id="Seg_4271" n="e" s="T171">dʼijegəʔ </ts>
               <ts e="T173" id="Seg_4273" n="e" s="T172">šonəgaʔi. </ts>
               <ts e="T174" id="Seg_4275" n="e" s="T173">Onʼiʔ, </ts>
               <ts e="T175" id="Seg_4277" n="e" s="T174">šide, </ts>
               <ts e="T176" id="Seg_4279" n="e" s="T175">teʔtə, </ts>
               <ts e="T177" id="Seg_4281" n="e" s="T176">nagur, </ts>
               <ts e="T178" id="Seg_4283" n="e" s="T177">sumna, </ts>
               <ts e="T179" id="Seg_4285" n="e" s="T178">muktuʔ. </ts>
               <ts e="T180" id="Seg_4287" n="e" s="T179">Onʼiʔ </ts>
               <ts e="T181" id="Seg_4289" n="e" s="T180">onʼiʔziʔ </ts>
               <ts e="T182" id="Seg_4291" n="e" s="T181">(k </ts>
               <ts e="T183" id="Seg_4293" n="e" s="T182">- </ts>
               <ts e="T184" id="Seg_4295" n="e" s="T183">šolə </ts>
               <ts e="T185" id="Seg_4297" n="e" s="T184">-) </ts>
               <ts e="T186" id="Seg_4299" n="e" s="T185">šonaʔbəjəʔ. </ts>
               <ts e="T187" id="Seg_4301" n="e" s="T186">Bü </ts>
               <ts e="T188" id="Seg_4303" n="e" s="T187">bar </ts>
               <ts e="T189" id="Seg_4305" n="e" s="T188">kănzəlaʔpi. </ts>
               <ts e="T190" id="Seg_4307" n="e" s="T189">Üjüziʔ </ts>
               <ts e="T191" id="Seg_4309" n="e" s="T190">(nuldlial=) </ts>
               <ts e="T192" id="Seg_4311" n="e" s="T191">nulal </ts>
               <ts e="T193" id="Seg_4313" n="e" s="T192">dak, </ts>
               <ts e="T194" id="Seg_4315" n="e" s="T193">dibər </ts>
               <ts e="T195" id="Seg_4317" n="e" s="T194">ej </ts>
               <ts e="T196" id="Seg_4319" n="e" s="T195">saʔməlial. </ts>
               <ts e="T197" id="Seg_4321" n="e" s="T196">Măn </ts>
               <ts e="T198" id="Seg_4323" n="e" s="T197">üdʼüge </ts>
               <ts e="T199" id="Seg_4325" n="e" s="T198">ibim, </ts>
               <ts e="T200" id="Seg_4327" n="e" s="T199">măn </ts>
               <ts e="T201" id="Seg_4329" n="e" s="T200">iam </ts>
               <ts e="T202" id="Seg_4331" n="e" s="T201">togonorbi. </ts>
               <ts e="T203" id="Seg_4333" n="e" s="T202">Kubaʔi </ts>
               <ts e="T204" id="Seg_4335" n="e" s="T203">(ai-) </ts>
               <ts e="T205" id="Seg_4337" n="e" s="T204">abi, </ts>
               <ts e="T206" id="Seg_4339" n="e" s="T205">pargaʔi </ts>
               <ts e="T207" id="Seg_4341" n="e" s="T206">šobi. </ts>
               <ts e="T208" id="Seg_4343" n="e" s="T207">Jamaʔi </ts>
               <ts e="T209" id="Seg_4345" n="e" s="T208">šöʔpi, </ts>
               <ts e="T210" id="Seg_4347" n="e" s="T209">(üžü </ts>
               <ts e="T211" id="Seg_4349" n="e" s="T210">- </ts>
               <ts e="T212" id="Seg_4351" n="e" s="T211">üzər-) </ts>
               <ts e="T213" id="Seg_4353" n="e" s="T212">üžü </ts>
               <ts e="T214" id="Seg_4355" n="e" s="T213">(s-) </ts>
               <ts e="T215" id="Seg_4357" n="e" s="T214">šöʔpi </ts>
               <ts e="T217" id="Seg_4359" n="e" s="T215">i… </ts>
               <ts e="T218" id="Seg_4361" n="e" s="T217">Abam </ts>
               <ts e="T219" id="Seg_4363" n="e" s="T218">bar </ts>
               <ts e="T220" id="Seg_4365" n="e" s="T219">šerbi. </ts>
               <ts e="T221" id="Seg_4367" n="e" s="T220">Tüj </ts>
               <ts e="T222" id="Seg_4369" n="e" s="T221">măn </ts>
               <ts e="T223" id="Seg_4371" n="e" s="T222">(šalbə) </ts>
               <ts e="T224" id="Seg_4373" n="e" s="T223">naga, </ts>
               <ts e="T225" id="Seg_4375" n="e" s="T224">ej </ts>
               <ts e="T226" id="Seg_4377" n="e" s="T225">tĭmnem </ts>
               <ts e="T227" id="Seg_4379" n="e" s="T226">dĭ </ts>
               <ts e="T228" id="Seg_4381" n="e" s="T227">giraːmbi. </ts>
               <ts e="T229" id="Seg_4383" n="e" s="T228">((BRK)). </ts>
               <ts e="T230" id="Seg_4385" n="e" s="T229">Ugaːndə </ts>
               <ts e="T231" id="Seg_4387" n="e" s="T230">(pim-) </ts>
               <ts e="T232" id="Seg_4389" n="e" s="T231">pimniem, </ts>
               <ts e="T233" id="Seg_4391" n="e" s="T232">bar </ts>
               <ts e="T234" id="Seg_4393" n="e" s="T233">măna </ts>
               <ts e="T235" id="Seg_4395" n="e" s="T234">sădărlaʔbə. </ts>
               <ts e="T236" id="Seg_4397" n="e" s="T235">((BRK)). </ts>
               <ts e="T237" id="Seg_4399" n="e" s="T236">Teinen </ts>
               <ts e="T238" id="Seg_4401" n="e" s="T237">(di-) </ts>
               <ts e="T239" id="Seg_4403" n="e" s="T238">dʼijegən </ts>
               <ts e="T240" id="Seg_4405" n="e" s="T239">šaːbilaʔ, </ts>
               <ts e="T241" id="Seg_4407" n="e" s="T240">ugaːndə </ts>
               <ts e="T242" id="Seg_4409" n="e" s="T241">šišəge </ts>
               <ts e="T243" id="Seg_4411" n="e" s="T242">ibi. </ts>
               <ts e="T244" id="Seg_4413" n="e" s="T243">Da </ts>
               <ts e="T245" id="Seg_4415" n="e" s="T244">miʔ </ts>
               <ts e="T246" id="Seg_4417" n="e" s="T245">ugaːndə </ts>
               <ts e="T247" id="Seg_4419" n="e" s="T246">kănnaːmbibaʔ, </ts>
               <ts e="T248" id="Seg_4421" n="e" s="T247">bar </ts>
               <ts e="T249" id="Seg_4423" n="e" s="T248">tăŋ </ts>
               <ts e="T250" id="Seg_4425" n="e" s="T249">kănnaːmbibaʔ. </ts>
               <ts e="T251" id="Seg_4427" n="e" s="T250">((BRK)). </ts>
               <ts e="T252" id="Seg_4429" n="e" s="T251">Ugaːndə </ts>
               <ts e="T253" id="Seg_4431" n="e" s="T252">šišəge, </ts>
               <ts e="T254" id="Seg_4433" n="e" s="T253">bü </ts>
               <ts e="T255" id="Seg_4435" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_4437" n="e" s="T255">kănnaːmbi. </ts>
               <ts e="T257" id="Seg_4439" n="e" s="T256">Măn </ts>
               <ts e="T258" id="Seg_4441" n="e" s="T257">(üjüzibə) </ts>
               <ts e="T259" id="Seg_4443" n="e" s="T258">(nul-) </ts>
               <ts e="T260" id="Seg_4445" n="e" s="T259">nubiam. </ts>
               <ts e="T261" id="Seg_4447" n="e" s="T260">Dĭ </ts>
               <ts e="T262" id="Seg_4449" n="e" s="T261">ej </ts>
               <ts e="T263" id="Seg_4451" n="e" s="T262">băldəbi. </ts>
               <ts e="T264" id="Seg_4453" n="e" s="T263">((BRK)). </ts>
               <ts e="T265" id="Seg_4455" n="e" s="T264">Kamən </ts>
               <ts e="T266" id="Seg_4457" n="e" s="T265">kunolzittə </ts>
               <ts e="T267" id="Seg_4459" n="e" s="T266">iʔbələl, </ts>
               <ts e="T268" id="Seg_4461" n="e" s="T267">suraraʔ: </ts>
               <ts e="T269" id="Seg_4463" n="e" s="T268">Öʔləl </ts>
               <ts e="T270" id="Seg_4465" n="e" s="T269">măna </ts>
               <ts e="T271" id="Seg_4467" n="e" s="T270">kunolzittə </ts>
               <ts e="T272" id="Seg_4469" n="e" s="T271">dön?" </ts>
               <ts e="T274" id="Seg_4471" n="e" s="T272">Baštap </ts>
               <ts e="T275" id="Seg_4473" n="e" s="T274">dĭn </ts>
               <ts e="T276" id="Seg_4475" n="e" s="T275">nubiʔi </ts>
               <ts e="T277" id="Seg_4477" n="e" s="T276">Ilʼbinʼən </ts>
               <ts e="T278" id="Seg_4479" n="e" s="T277">toːndə. </ts>
               <ts e="T279" id="Seg_4481" n="e" s="T278">Dĭgəttə </ts>
               <ts e="T280" id="Seg_4483" n="e" s="T279">dö </ts>
               <ts e="T281" id="Seg_4485" n="e" s="T280">bü </ts>
               <ts e="T282" id="Seg_4487" n="e" s="T281">kubiʔi. </ts>
               <ts e="T283" id="Seg_4489" n="e" s="T282">(Ej </ts>
               <ts e="T284" id="Seg_4491" n="e" s="T283">m-). </ts>
               <ts e="T285" id="Seg_4493" n="e" s="T284">Ej </ts>
               <ts e="T286" id="Seg_4495" n="e" s="T285">kănzəlia, </ts>
               <ts e="T287" id="Seg_4497" n="e" s="T286">(d-) </ts>
               <ts e="T288" id="Seg_4499" n="e" s="T287">dĭzeŋ </ts>
               <ts e="T289" id="Seg_4501" n="e" s="T288">dön </ts>
               <ts e="T290" id="Seg_4503" n="e" s="T289">maʔi </ts>
               <ts e="T291" id="Seg_4505" n="e" s="T290">nuldəbiʔi. </ts>
               <ts e="T292" id="Seg_4507" n="e" s="T291">I </ts>
               <ts e="T293" id="Seg_4509" n="e" s="T292">döbər </ts>
               <ts e="T294" id="Seg_4511" n="e" s="T293">amnosʼtə </ts>
               <ts e="T295" id="Seg_4513" n="e" s="T294">šobiʔi </ts>
               <ts e="T296" id="Seg_4515" n="e" s="T295">kamen </ts>
               <ts e="T297" id="Seg_4517" n="e" s="T296">šišəge </ts>
               <ts e="T298" id="Seg_4519" n="e" s="T297">molaːmbi. </ts>
               <ts e="T299" id="Seg_4521" n="e" s="T298">((BRK)). </ts>
               <ts e="T300" id="Seg_4523" n="e" s="T299">Dĭzeŋ </ts>
               <ts e="T301" id="Seg_4525" n="e" s="T300">bar </ts>
               <ts e="T302" id="Seg_4527" n="e" s="T301">šobiʔi, </ts>
               <ts e="T303" id="Seg_4529" n="e" s="T302">Ilʼbinʼdə </ts>
               <ts e="T304" id="Seg_4531" n="e" s="T303">amnolaʔbəʔi. </ts>
               <ts e="T305" id="Seg_4533" n="e" s="T304">Dĭgəttə </ts>
               <ts e="T306" id="Seg_4535" n="e" s="T305">döbər </ts>
               <ts e="T307" id="Seg_4537" n="e" s="T306">šobiʔi, </ts>
               <ts e="T308" id="Seg_4539" n="e" s="T307">bü </ts>
               <ts e="T309" id="Seg_4541" n="e" s="T308">kubiʔi, </ts>
               <ts e="T310" id="Seg_4543" n="e" s="T309">dĭ </ts>
               <ts e="T311" id="Seg_4545" n="e" s="T310">ej </ts>
               <ts e="T312" id="Seg_4547" n="e" s="T311">kănnia. </ts>
               <ts e="T313" id="Seg_4549" n="e" s="T312">Dön </ts>
               <ts e="T314" id="Seg_4551" n="e" s="T313">maʔzandə </ts>
               <ts e="T315" id="Seg_4553" n="e" s="T314">nuldəbiʔi. </ts>
               <ts e="T316" id="Seg_4555" n="e" s="T315">Döbər </ts>
               <ts e="T317" id="Seg_4557" n="e" s="T316">(šo-) </ts>
               <ts e="T318" id="Seg_4559" n="e" s="T317">šonəgaʔi </ts>
               <ts e="T319" id="Seg_4561" n="e" s="T318">amnozittə, </ts>
               <ts e="T320" id="Seg_4563" n="e" s="T319">kamen </ts>
               <ts e="T321" id="Seg_4565" n="e" s="T320">šišəge </ts>
               <ts e="T322" id="Seg_4567" n="e" s="T321">molaːlləj. </ts>
               <ts e="T323" id="Seg_4569" n="e" s="T322">Dĭzeŋ </ts>
               <ts e="T324" id="Seg_4571" n="e" s="T323">ej </ts>
               <ts e="T325" id="Seg_4573" n="e" s="T324">molaːmbi, </ts>
               <ts e="T326" id="Seg_4575" n="e" s="T325">dĭgəttə </ts>
               <ts e="T327" id="Seg_4577" n="e" s="T326">kandəgaʔi </ts>
               <ts e="T328" id="Seg_4579" n="e" s="T327">dʼijenə, </ts>
               <ts e="T329" id="Seg_4581" n="e" s="T328">vezʼdʼe. </ts>
               <ts e="T330" id="Seg_4583" n="e" s="T329">Bar </ts>
               <ts e="T331" id="Seg_4585" n="e" s="T330">dʼügən </ts>
               <ts e="T332" id="Seg_4587" n="e" s="T331">mĭmbiʔi. </ts>
               <ts e="T333" id="Seg_4589" n="e" s="T332">Dʼelamdə </ts>
               <ts e="T334" id="Seg_4591" n="e" s="T333">kambiʔi, </ts>
               <ts e="T335" id="Seg_4593" n="e" s="T334">dĭn </ts>
               <ts e="T336" id="Seg_4595" n="e" s="T335">bü </ts>
               <ts e="T337" id="Seg_4597" n="e" s="T336">iʔgö. </ts>
               <ts e="T338" id="Seg_4599" n="e" s="T337">((BRK)). </ts>
               <ts e="T339" id="Seg_4601" n="e" s="T338">Onʼiʔ </ts>
               <ts e="T340" id="Seg_4603" n="e" s="T339">kuza </ts>
               <ts e="T341" id="Seg_4605" n="e" s="T340">dön, </ts>
               <ts e="T342" id="Seg_4607" n="e" s="T341">a </ts>
               <ts e="T343" id="Seg_4609" n="e" s="T342">onʼiʔ </ts>
               <ts e="T344" id="Seg_4611" n="e" s="T343">kuza </ts>
               <ts e="T345" id="Seg_4613" n="e" s="T344">dĭn. </ts>
               <ts e="T346" id="Seg_4615" n="e" s="T345">((BRK)). </ts>
               <ts e="T347" id="Seg_4617" n="e" s="T346">Kanaʔ </ts>
               <ts e="T348" id="Seg_4619" n="e" s="T347">dibər, </ts>
               <ts e="T349" id="Seg_4621" n="e" s="T348">döbər. </ts>
               <ts e="T350" id="Seg_4623" n="e" s="T349">Dibər </ts>
               <ts e="T351" id="Seg_4625" n="e" s="T350">em </ts>
               <ts e="T352" id="Seg_4627" n="e" s="T351">kanaʔ </ts>
               <ts e="T353" id="Seg_4629" n="e" s="T352">i </ts>
               <ts e="T354" id="Seg_4631" n="e" s="T353">döbər </ts>
               <ts e="T355" id="Seg_4633" n="e" s="T354">em </ts>
               <ts e="T356" id="Seg_4635" n="e" s="T355">kanaʔ. </ts>
               <ts e="T357" id="Seg_4637" n="e" s="T356">((BRK)). </ts>
               <ts e="T358" id="Seg_4639" n="e" s="T357">Kăde </ts>
               <ts e="T359" id="Seg_4641" n="e" s="T358">dăre </ts>
               <ts e="T360" id="Seg_4643" n="e" s="T359">moləj? </ts>
               <ts e="T361" id="Seg_4645" n="e" s="T360">Ej </ts>
               <ts e="T362" id="Seg_4647" n="e" s="T361">kalial, </ts>
               <ts e="T363" id="Seg_4649" n="e" s="T362">nʼim. </ts>
               <ts e="T364" id="Seg_4651" n="e" s="T363">Dĭ </ts>
               <ts e="T365" id="Seg_4653" n="e" s="T364">kuza </ts>
               <ts e="T366" id="Seg_4655" n="e" s="T365">ugandə </ts>
               <ts e="T367" id="Seg_4657" n="e" s="T366">jakšə </ts>
               <ts e="T368" id="Seg_4659" n="e" s="T367">(amo-) </ts>
               <ts e="T369" id="Seg_4661" n="e" s="T368">amnolaʔbə. </ts>
               <ts e="T370" id="Seg_4663" n="e" s="T369">Dĭn </ts>
               <ts e="T371" id="Seg_4665" n="e" s="T370">bar </ts>
               <ts e="T372" id="Seg_4667" n="e" s="T371">ĭmbi </ts>
               <ts e="T373" id="Seg_4669" n="e" s="T372">ige. </ts>
               <ts e="T374" id="Seg_4671" n="e" s="T373">((BRK)). </ts>
               <ts e="T375" id="Seg_4673" n="e" s="T374">Dʼirəgəʔi </ts>
               <ts e="T376" id="Seg_4675" n="e" s="T375">bar </ts>
               <ts e="T377" id="Seg_4677" n="e" s="T376">šobiʔi </ts>
               <ts e="T378" id="Seg_4679" n="e" s="T377">măn </ts>
               <ts e="T379" id="Seg_4681" n="e" s="T378">šĭkəziʔ </ts>
               <ts e="T380" id="Seg_4683" n="e" s="T379">dʼăbaktərzittə. </ts>
               <ts e="T381" id="Seg_4685" n="e" s="T380">((BRK)). </ts>
               <ts e="T382" id="Seg_4687" n="e" s="T381">Dĭzeŋ </ts>
               <ts e="T383" id="Seg_4689" n="e" s="T382">măn </ts>
               <ts e="T384" id="Seg_4691" n="e" s="T383">(š-) </ts>
               <ts e="T385" id="Seg_4693" n="e" s="T384">šĭkəm </ts>
               <ts e="T386" id="Seg_4695" n="e" s="T385">tüšəleʔbəʔjə. </ts>
               <ts e="T387" id="Seg_4697" n="e" s="T386">((BRK)). </ts>
               <ts e="T388" id="Seg_4699" n="e" s="T387">Suraraʔ, </ts>
               <ts e="T389" id="Seg_4701" n="e" s="T388">a </ts>
               <ts e="T390" id="Seg_4703" n="e" s="T389">măn </ts>
               <ts e="T391" id="Seg_4705" n="e" s="T390">nörbəlem </ts>
               <ts e="T392" id="Seg_4707" n="e" s="T391">tănan. </ts>
               <ts e="T393" id="Seg_4709" n="e" s="T392">Tăn </ts>
               <ts e="T394" id="Seg_4711" n="e" s="T393">ĭmbi </ts>
               <ts e="T395" id="Seg_4713" n="e" s="T394">(dĭgə) </ts>
               <ts e="T396" id="Seg_4715" n="e" s="T395">ibiel. </ts>
               <ts e="T397" id="Seg_4717" n="e" s="T396">((BRK)). </ts>
               <ts e="T400" id="Seg_4719" n="e" s="T397">На кого говорить? </ts>
               <ts e="T401" id="Seg_4721" n="e" s="T400">Tăn </ts>
               <ts e="T402" id="Seg_4723" n="e" s="T401">ugaːndə </ts>
               <ts e="T403" id="Seg_4725" n="e" s="T402">numo </ts>
               <ts e="T404" id="Seg_4727" n="e" s="T403">(šĭ-) </ts>
               <ts e="T405" id="Seg_4729" n="e" s="T404">šĭkəl. </ts>
               <ts e="T406" id="Seg_4731" n="e" s="T405">A </ts>
               <ts e="T407" id="Seg_4733" n="e" s="T406">măn </ts>
               <ts e="T408" id="Seg_4735" n="e" s="T407">üdʼüge </ts>
               <ts e="T409" id="Seg_4737" n="e" s="T408">šĭkəm. </ts>
               <ts e="T410" id="Seg_4739" n="e" s="T409">((BRK)). </ts>
               <ts e="T411" id="Seg_4741" n="e" s="T410">Miʔ </ts>
               <ts e="T412" id="Seg_4743" n="e" s="T411">bar </ts>
               <ts e="T413" id="Seg_4745" n="e" s="T412">(šo-) </ts>
               <ts e="T414" id="Seg_4747" n="e" s="T413">šobibaʔ </ts>
               <ts e="T415" id="Seg_4749" n="e" s="T414">(oktʼ-) </ts>
               <ts e="T416" id="Seg_4751" n="e" s="T415">aktʼinə. </ts>
               <ts e="T417" id="Seg_4753" n="e" s="T416">Kuza </ts>
               <ts e="T418" id="Seg_4755" n="e" s="T417">bar </ts>
               <ts e="T419" id="Seg_4757" n="e" s="T418">šonəga </ts>
               <ts e="T420" id="Seg_4759" n="e" s="T419">miʔnʼibeʔ. </ts>
               <ts e="T421" id="Seg_4761" n="e" s="T420">Suraraʔ </ts>
               <ts e="T422" id="Seg_4763" n="e" s="T421">aʔtʼi, </ts>
               <ts e="T423" id="Seg_4765" n="e" s="T422">gibər </ts>
               <ts e="T424" id="Seg_4767" n="e" s="T423">dĭ </ts>
               <ts e="T425" id="Seg_4769" n="e" s="T424">kandəga, </ts>
               <ts e="T426" id="Seg_4771" n="e" s="T425">a_to </ts>
               <ts e="T428" id="Seg_4773" n="e" s="T426">miʔ… </ts>
               <ts e="T429" id="Seg_4775" n="e" s="T428">((BRK)). </ts>
               <ts e="T430" id="Seg_4777" n="e" s="T429">Miʔ </ts>
               <ts e="T431" id="Seg_4779" n="e" s="T430">bar </ts>
               <ts e="T432" id="Seg_4781" n="e" s="T431">ej </ts>
               <ts e="T433" id="Seg_4783" n="e" s="T432">dĭbər </ts>
               <ts e="T434" id="Seg_4785" n="e" s="T433">možet </ts>
               <ts e="T435" id="Seg_4787" n="e" s="T434">kambibaʔ. </ts>
               <ts e="T436" id="Seg_4789" n="e" s="T435">((BRK)). </ts>
               <ts e="T437" id="Seg_4791" n="e" s="T436">"Tăn </ts>
               <ts e="T438" id="Seg_4793" n="e" s="T437">bar </ts>
               <ts e="T439" id="Seg_4795" n="e" s="T438">ej </ts>
               <ts e="T440" id="Seg_4797" n="e" s="T439">dʼăbaktərial, </ts>
               <ts e="T441" id="Seg_4799" n="e" s="T440">a </ts>
               <ts e="T442" id="Seg_4801" n="e" s="T441">măn </ts>
               <ts e="T443" id="Seg_4803" n="e" s="T442">(dʼăbaktəriam) </ts>
               <ts e="T444" id="Seg_4805" n="e" s="T443">tănziʔ. </ts>
               <ts e="T445" id="Seg_4807" n="e" s="T444">A </ts>
               <ts e="T446" id="Seg_4809" n="e" s="T445">măn </ts>
               <ts e="T447" id="Seg_4811" n="e" s="T446">ej </ts>
               <ts e="T448" id="Seg_4813" n="e" s="T447">tĭmnem. </ts>
               <ts e="T449" id="Seg_4815" n="e" s="T448">Măn </ts>
               <ts e="T450" id="Seg_4817" n="e" s="T449">tĭmnem </ts>
               <ts e="T451" id="Seg_4819" n="e" s="T450">a </ts>
               <ts e="T452" id="Seg_4821" n="e" s="T451">dʼăbaktərzittə </ts>
               <ts e="T453" id="Seg_4823" n="e" s="T452">ej </ts>
               <ts e="T454" id="Seg_4825" n="e" s="T453">moliam. </ts>
               <ts e="T455" id="Seg_4827" n="e" s="T454">Nada </ts>
               <ts e="T456" id="Seg_4829" n="e" s="T455">tăn </ts>
               <ts e="T457" id="Seg_4831" n="e" s="T456">šĭkəl </ts>
               <ts e="T458" id="Seg_4833" n="e" s="T457">sajnʼeʔsittə </ts>
               <ts e="T459" id="Seg_4835" n="e" s="T458">i </ts>
               <ts e="T460" id="Seg_4837" n="e" s="T459">baruʔsittə". </ts>
               <ts e="T461" id="Seg_4839" n="e" s="T460">((BRK)). </ts>
               <ts e="T462" id="Seg_4841" n="e" s="T461">Dĭzeŋ </ts>
               <ts e="T463" id="Seg_4843" n="e" s="T462">tüšəlbiʔi, </ts>
               <ts e="T464" id="Seg_4845" n="e" s="T463">tüj </ts>
               <ts e="T465" id="Seg_4847" n="e" s="T464">tože </ts>
               <ts e="T466" id="Seg_4849" n="e" s="T465">nuzaŋ </ts>
               <ts e="T467" id="Seg_4851" n="e" s="T466">moləʔjə. </ts>
               <ts e="T468" id="Seg_4853" n="e" s="T467">((BRK)). </ts>
               <ts e="T469" id="Seg_4855" n="e" s="T468">(Ugaːn-) </ts>
               <ts e="T470" id="Seg_4857" n="e" s="T469">Ugaːndə. </ts>
               <ts e="T471" id="Seg_4859" n="e" s="T470">Urgo </ts>
               <ts e="T472" id="Seg_4861" n="e" s="T471">beržə </ts>
               <ts e="T473" id="Seg_4863" n="e" s="T472">bar. </ts>
               <ts e="T474" id="Seg_4865" n="e" s="T473">Tăŋ </ts>
               <ts e="T475" id="Seg_4867" n="e" s="T474">bar </ts>
               <ts e="T476" id="Seg_4869" n="e" s="T475">kandəga. </ts>
               <ts e="T477" id="Seg_4871" n="e" s="T476">Sĭre </ts>
               <ts e="T478" id="Seg_4873" n="e" s="T477">bar </ts>
               <ts e="T479" id="Seg_4875" n="e" s="T478">kăndlaʔbə, </ts>
               <ts e="T480" id="Seg_4877" n="e" s="T479">i </ts>
               <ts e="T481" id="Seg_4879" n="e" s="T480">dʼü </ts>
               <ts e="T482" id="Seg_4881" n="e" s="T481">kăndlaʔbə, </ts>
               <ts e="T484" id="Seg_4883" n="e" s="T482">bar… </ts>
               <ts e="T486" id="Seg_4885" n="e" s="T484">Sĭre </ts>
               <ts e="T487" id="Seg_4887" n="e" s="T486">i </ts>
               <ts e="T488" id="Seg_4889" n="e" s="T487">dʼü, </ts>
               <ts e="T489" id="Seg_4891" n="e" s="T488">(bar </ts>
               <ts e="T490" id="Seg_4893" n="e" s="T489">m-) </ts>
               <ts e="T491" id="Seg_4895" n="e" s="T490">bar </ts>
               <ts e="T492" id="Seg_4897" n="e" s="T491">(bĭlgarluʔpi). </ts>
               <ts e="T493" id="Seg_4899" n="e" s="T492">Tăn </ts>
               <ts e="T494" id="Seg_4901" n="e" s="T493">kudaj, </ts>
               <ts e="T495" id="Seg_4903" n="e" s="T494">kudaj, </ts>
               <ts e="T496" id="Seg_4905" n="e" s="T495">iʔ </ts>
               <ts e="T497" id="Seg_4907" n="e" s="T496">maʔtə </ts>
               <ts e="T498" id="Seg_4909" n="e" s="T497">măna, </ts>
               <ts e="T499" id="Seg_4911" n="e" s="T498">iʔ </ts>
               <ts e="T500" id="Seg_4913" n="e" s="T499">maʔtə </ts>
               <ts e="T501" id="Seg_4915" n="e" s="T500">măna. </ts>
               <ts e="T502" id="Seg_4917" n="e" s="T501">Iʔ </ts>
               <ts e="T503" id="Seg_4919" n="e" s="T502">maʔtə </ts>
               <ts e="T504" id="Seg_4921" n="e" s="T503">măna, </ts>
               <ts e="T505" id="Seg_4923" n="e" s="T504">iʔ </ts>
               <ts e="T506" id="Seg_4925" n="e" s="T505">barəʔtə </ts>
               <ts e="T507" id="Seg_4927" n="e" s="T506">măna. </ts>
               <ts e="T508" id="Seg_4929" n="e" s="T507">Tăn </ts>
               <ts e="T509" id="Seg_4931" n="e" s="T508">it </ts>
               <ts e="T510" id="Seg_4933" n="e" s="T509">măna </ts>
               <ts e="T511" id="Seg_4935" n="e" s="T510">sĭjbə </ts>
               <ts e="T512" id="Seg_4937" n="e" s="T511">sagəšsəbi, </ts>
               <ts e="T513" id="Seg_4939" n="e" s="T512">tăn </ts>
               <ts e="T514" id="Seg_4941" n="e" s="T513">deʔ </ts>
               <ts e="T515" id="Seg_4943" n="e" s="T514">măna </ts>
               <ts e="T516" id="Seg_4945" n="e" s="T515">sĭjbə </ts>
               <ts e="T517" id="Seg_4947" n="e" s="T516">sagəš </ts>
               <ts e="T518" id="Seg_4949" n="e" s="T517">iʔgö. </ts>
               <ts e="T519" id="Seg_4951" n="e" s="T518">Tüšəldə </ts>
               <ts e="T520" id="Seg_4953" n="e" s="T519">măna </ts>
               <ts e="T521" id="Seg_4955" n="e" s="T520">maktanərzittə </ts>
               <ts e="T522" id="Seg_4957" n="e" s="T521">tănzi. </ts>
               <ts e="T523" id="Seg_4959" n="e" s="T522">Tüšəldə </ts>
               <ts e="T524" id="Seg_4961" n="e" s="T523">măna, </ts>
               <ts e="T525" id="Seg_4963" n="e" s="T524">tăn </ts>
               <ts e="T526" id="Seg_4965" n="e" s="T525">(aʔtʼit-) </ts>
               <ts e="T527" id="Seg_4967" n="e" s="T526">aktʼitəm </ts>
               <ts e="T528" id="Seg_4969" n="e" s="T527">mĭnzittə. </ts>
               <ts e="T529" id="Seg_4971" n="e" s="T528">((BRK)). </ts>
               <ts e="T530" id="Seg_4973" n="e" s="T529">Tăn </ts>
               <ts e="T531" id="Seg_4975" n="e" s="T530">(aksin-) </ts>
               <ts e="T532" id="Seg_4977" n="e" s="T531">(aktʼiziʔi) </ts>
               <ts e="T533" id="Seg_4979" n="e" s="T532">kanzittə. </ts>
               <ts e="T536" id="Seg_4981" n="e" s="T533">((BRK)) Погоди… </ts>
               <ts e="T537" id="Seg_4983" n="e" s="T536">((BRK)). </ts>
               <ts e="T538" id="Seg_4985" n="e" s="T537">Vanʼa, </ts>
               <ts e="T539" id="Seg_4987" n="e" s="T538">kanaʔ </ts>
               <ts e="T540" id="Seg_4989" n="e" s="T539">kazak </ts>
               <ts e="T541" id="Seg_4991" n="e" s="T540">ianəl. </ts>
               <ts e="T542" id="Seg_4993" n="e" s="T541">Kăʔbde </ts>
               <ts e="T543" id="Seg_4995" n="e" s="T542">pʼeʔ </ts>
               <ts e="T544" id="Seg_4997" n="e" s="T543">(dĭʔən). </ts>
               <ts e="T545" id="Seg_4999" n="e" s="T544">Ularən </ts>
               <ts e="T546" id="Seg_5001" n="e" s="T545">kăʔbde, </ts>
               <ts e="T547" id="Seg_5003" n="e" s="T546">ularəʔi </ts>
               <ts e="T548" id="Seg_5005" n="e" s="T547">tordə </ts>
               <ts e="T549" id="Seg_5007" n="e" s="T548">(bod-) </ts>
               <ts e="T550" id="Seg_5009" n="e" s="T549">băʔsittə </ts>
               <ts e="T551" id="Seg_5011" n="e" s="T550">miʔnʼibeʔ </ts>
               <ts e="T552" id="Seg_5013" n="e" s="T551">kereʔ. </ts>
               <ts e="T553" id="Seg_5015" n="e" s="T552">((BRK)). </ts>
               <ts e="T554" id="Seg_5017" n="e" s="T553">Jakše </ts>
               <ts e="T555" id="Seg_5019" n="e" s="T554">pʼet. </ts>
               <ts e="T556" id="Seg_5021" n="e" s="T555">((BRK)). </ts>
               <ts e="T557" id="Seg_5023" n="e" s="T556">Măn </ts>
               <ts e="T558" id="Seg_5025" n="e" s="T557">taldʼen </ts>
               <ts e="T559" id="Seg_5027" n="e" s="T558">pʼeštə </ts>
               <ts e="T560" id="Seg_5029" n="e" s="T559">(še- </ts>
               <ts e="T561" id="Seg_5031" n="e" s="T560">səbia- </ts>
               <ts e="T562" id="Seg_5033" n="e" s="T561">š- </ts>
               <ts e="T563" id="Seg_5035" n="e" s="T562">šabiam-) </ts>
               <ts e="T564" id="Seg_5037" n="e" s="T563">sʼabiam. </ts>
               <ts e="T565" id="Seg_5039" n="e" s="T564">Ej </ts>
               <ts e="T566" id="Seg_5041" n="e" s="T565">amzittə. </ts>
               <ts e="T567" id="Seg_5043" n="e" s="T566">(Va-) </ts>
               <ts e="T568" id="Seg_5045" n="e" s="T567">Vanʼka </ts>
               <ts e="T569" id="Seg_5047" n="e" s="T568">abatsi </ts>
               <ts e="T570" id="Seg_5049" n="e" s="T569">šobiʔi. </ts>
               <ts e="T571" id="Seg_5051" n="e" s="T570">Măndə: </ts>
               <ts e="T572" id="Seg_5053" n="e" s="T571">Urgaja, </ts>
               <ts e="T573" id="Seg_5055" n="e" s="T572">(kutʼida) </ts>
               <ts e="T574" id="Seg_5057" n="e" s="T573">girgit </ts>
               <ts e="T575" id="Seg_5059" n="e" s="T574">kola </ts>
               <ts e="T576" id="Seg_5061" n="e" s="T575">bar </ts>
               <ts e="T577" id="Seg_5063" n="e" s="T576">urgo. </ts>
               <ts e="T578" id="Seg_5065" n="e" s="T577">A </ts>
               <ts e="T579" id="Seg_5067" n="e" s="T578">măn </ts>
               <ts e="T580" id="Seg_5069" n="e" s="T579">mămbiam: </ts>
               <ts e="T581" id="Seg_5071" n="e" s="T580">Kumən </ts>
               <ts e="T582" id="Seg_5073" n="e" s="T581">dʼaʔpilaʔ? </ts>
               <ts e="T583" id="Seg_5075" n="e" s="T582">Di </ts>
               <ts e="T584" id="Seg_5077" n="e" s="T583">măndə: </ts>
               <ts e="T585" id="Seg_5079" n="e" s="T584">Oʔb </ts>
               <ts e="T586" id="Seg_5081" n="e" s="T585">šide </ts>
               <ts e="T587" id="Seg_5083" n="e" s="T586">nagur </ts>
               <ts e="T588" id="Seg_5085" n="e" s="T587">teʔtə </ts>
               <ts e="T589" id="Seg_5087" n="e" s="T588">sumna </ts>
               <ts e="T590" id="Seg_5089" n="e" s="T589">muktuʔ </ts>
               <ts e="T591" id="Seg_5091" n="e" s="T590">(šejʔp-) </ts>
               <ts e="T592" id="Seg_5093" n="e" s="T591">sejʔpü </ts>
               <ts e="T593" id="Seg_5095" n="e" s="T592">dʼaʔpibaʔ". </ts>
               <ts e="T594" id="Seg_5097" n="e" s="T593">((BRK)). </ts>
               <ts e="T595" id="Seg_5099" n="e" s="T594">Fašistəʔi </ts>
               <ts e="T596" id="Seg_5101" n="e" s="T595">miʔ </ts>
               <ts e="T597" id="Seg_5103" n="e" s="T596">il </ts>
               <ts e="T598" id="Seg_5105" n="e" s="T597">üge </ts>
               <ts e="T599" id="Seg_5107" n="e" s="T598">(b-) </ts>
               <ts e="T600" id="Seg_5109" n="e" s="T599">bar </ts>
               <ts e="T601" id="Seg_5111" n="e" s="T600">dʼabərolaʔpiʔi. </ts>
               <ts e="T602" id="Seg_5113" n="e" s="T601">Šidegöʔ </ts>
               <ts e="T603" id="Seg_5115" n="e" s="T602">maʔnən </ts>
               <ts e="T604" id="Seg_5117" n="e" s="T603">amnolaʔbi </ts>
               <ts e="T605" id="Seg_5119" n="e" s="T604">bar. </ts>
               <ts e="T606" id="Seg_5121" n="e" s="T605">Griška </ts>
               <ts e="T607" id="Seg_5123" n="e" s="T606">mĭmbi, </ts>
               <ts e="T608" id="Seg_5125" n="e" s="T607">kola </ts>
               <ts e="T609" id="Seg_5127" n="e" s="T608">dʼaʔpi </ts>
               <ts e="T610" id="Seg_5129" n="e" s="T609">(da) </ts>
               <ts e="T611" id="Seg_5131" n="e" s="T610">maʔndə </ts>
               <ts e="T612" id="Seg_5133" n="e" s="T611">deʔpi. </ts>
               <ts e="T613" id="Seg_5135" n="e" s="T612">Nabəʔi </ts>
               <ts e="T614" id="Seg_5137" n="e" s="T613">oʔb </ts>
               <ts e="T615" id="Seg_5139" n="e" s="T614">šide </ts>
               <ts e="T616" id="Seg_5141" n="e" s="T615">nagur </ts>
               <ts e="T617" id="Seg_5143" n="e" s="T616">detləj. </ts>
               <ts e="T618" id="Seg_5145" n="e" s="T617">A </ts>
               <ts e="T619" id="Seg_5147" n="e" s="T618">măn </ts>
               <ts e="T620" id="Seg_5149" n="e" s="T619">(ul-) </ts>
               <ts e="T621" id="Seg_5151" n="e" s="T620">ular </ts>
               <ts e="T622" id="Seg_5153" n="e" s="T621">(surdəbiam). </ts>
               <ts e="T623" id="Seg_5155" n="e" s="T622">((BRK)). </ts>
               <ts e="T624" id="Seg_5157" n="e" s="T623">Măn </ts>
               <ts e="T625" id="Seg_5159" n="e" s="T624">ugandə </ts>
               <ts e="T626" id="Seg_5161" n="e" s="T625">(kălxozəndə) </ts>
               <ts e="T627" id="Seg_5163" n="e" s="T626">tăŋ </ts>
               <ts e="T628" id="Seg_5165" n="e" s="T627">togonorbiam, </ts>
               <ts e="T629" id="Seg_5167" n="e" s="T628">iʔgö </ts>
               <ts e="T630" id="Seg_5169" n="e" s="T629">aš </ts>
               <ts e="T631" id="Seg_5171" n="e" s="T630">püdəbiem. </ts>
               <ts e="T632" id="Seg_5173" n="e" s="T631">Bʼeʔ </ts>
               <ts e="T633" id="Seg_5175" n="e" s="T632">šide </ts>
               <ts e="T634" id="Seg_5177" n="e" s="T633">püdəbiem, </ts>
               <ts e="T635" id="Seg_5179" n="e" s="T634">iššo </ts>
               <ts e="T636" id="Seg_5181" n="e" s="T635">šide. </ts>
               <ts e="T637" id="Seg_5183" n="e" s="T636">Dĭgəttə </ts>
               <ts e="T638" id="Seg_5185" n="e" s="T637">(onʼiʔ=) </ts>
               <ts e="T639" id="Seg_5187" n="e" s="T638">onʼiʔ </ts>
               <ts e="T640" id="Seg_5189" n="e" s="T639">dʼala </ts>
               <ts e="T641" id="Seg_5191" n="e" s="T640">(n-) </ts>
               <ts e="T642" id="Seg_5193" n="e" s="T641">nagur </ts>
               <ts e="T643" id="Seg_5195" n="e" s="T642">bʼeʔ </ts>
               <ts e="T644" id="Seg_5197" n="e" s="T643">püdəbiem </ts>
               <ts e="T645" id="Seg_5199" n="e" s="T644">i </ts>
               <ts e="T646" id="Seg_5201" n="e" s="T645">teʔtə </ts>
               <ts e="T647" id="Seg_5203" n="e" s="T646">iššo. </ts>
               <ts e="T648" id="Seg_5205" n="e" s="T647">((BRK)). </ts>
               <ts e="T649" id="Seg_5207" n="e" s="T648">Sotkaʔi. </ts>
               <ts e="T650" id="Seg_5209" n="e" s="T649">Măn </ts>
               <ts e="T651" id="Seg_5211" n="e" s="T650">tugandə </ts>
               <ts e="T652" id="Seg_5213" n="e" s="T651">surarlaʔbə: </ts>
               <ts e="T653" id="Seg_5215" n="e" s="T652">Nada </ts>
               <ts e="T654" id="Seg_5217" n="e" s="T653">măna </ts>
               <ts e="T655" id="Seg_5219" n="e" s="T654">nʼilgösʼtə </ts>
               <ts e="T656" id="Seg_5221" n="e" s="T655">kanzittə </ts>
               <ts e="T657" id="Seg_5223" n="e" s="T656">šiʔnʼileʔ, </ts>
               <ts e="T658" id="Seg_5225" n="e" s="T657">kăde </ts>
               <ts e="T659" id="Seg_5227" n="e" s="T658">dʼăbaktərlialaʔ. </ts>
               <ts e="T660" id="Seg_5229" n="e" s="T659">Šoʔ </ts>
               <ts e="T661" id="Seg_5231" n="e" s="T660">kărəldʼan, </ts>
               <ts e="T662" id="Seg_5233" n="e" s="T661">dĭzeŋ </ts>
               <ts e="T663" id="Seg_5235" n="e" s="T662">šoləʔjə. </ts>
               <ts e="T664" id="Seg_5237" n="e" s="T663">Nʼilgöt". </ts>
               <ts e="T665" id="Seg_5239" n="e" s="T664">((BRK)). </ts>
               <ts e="T666" id="Seg_5241" n="e" s="T665">Măn </ts>
               <ts e="T667" id="Seg_5243" n="e" s="T666">bar </ts>
               <ts e="T668" id="Seg_5245" n="e" s="T667">üdʼüge </ts>
               <ts e="T669" id="Seg_5247" n="e" s="T668">kurizəʔi </ts>
               <ts e="T670" id="Seg_5249" n="e" s="T669">uʔbdlaʔbəʔjə. </ts>
               <ts e="T671" id="Seg_5251" n="e" s="T670">Bar </ts>
               <ts e="T672" id="Seg_5253" n="e" s="T671">sʼujolaʔbəʔjə. </ts>
               <ts e="T673" id="Seg_5255" n="e" s="T672">((BRK)). </ts>
               <ts e="T674" id="Seg_5257" n="e" s="T673">Nʼeʔtə </ts>
               <ts e="T675" id="Seg_5259" n="e" s="T674">udal. </ts>
               <ts e="T676" id="Seg_5261" n="e" s="T675">((BRK)). </ts>
               <ts e="T677" id="Seg_5263" n="e" s="T676">Taldʼen </ts>
               <ts e="T678" id="Seg_5265" n="e" s="T677">nezeŋ </ts>
               <ts e="T679" id="Seg_5267" n="e" s="T678">măna </ts>
               <ts e="T680" id="Seg_5269" n="e" s="T679">šobiʔi </ts>
               <ts e="T681" id="Seg_5271" n="e" s="T680">süt </ts>
               <ts e="T682" id="Seg_5273" n="e" s="T681">kürzittə. </ts>
               <ts e="T683" id="Seg_5275" n="e" s="T682">Iʔgö </ts>
               <ts e="T684" id="Seg_5277" n="e" s="T683">ibiʔi </ts>
               <ts e="T685" id="Seg_5279" n="e" s="T684">dĭn. </ts>
               <ts e="T686" id="Seg_5281" n="e" s="T685">A </ts>
               <ts e="T687" id="Seg_5283" n="e" s="T686">(pretsed-) </ts>
               <ts e="T688" id="Seg_5285" n="e" s="T687">predsedatelʼən </ts>
               <ts e="T689" id="Seg_5287" n="e" s="T688">net </ts>
               <ts e="T690" id="Seg_5289" n="e" s="T689">amnobi </ts>
               <ts e="T691" id="Seg_5291" n="e" s="T690">dön. </ts>
               <ts e="T692" id="Seg_5293" n="e" s="T691">Nʼit </ts>
               <ts e="T693" id="Seg_5295" n="e" s="T692">šobi. </ts>
               <ts e="T694" id="Seg_5297" n="e" s="T693">(Măn-) </ts>
               <ts e="T695" id="Seg_5299" n="e" s="T694">Dĭʔnə </ts>
               <ts e="T696" id="Seg_5301" n="e" s="T695">măndə: </ts>
               <ts e="T697" id="Seg_5303" n="e" s="T696">kanžəbəj </ts>
               <ts e="T698" id="Seg_5305" n="e" s="T697">maʔnʼibeʔ, </ts>
               <ts e="T699" id="Seg_5307" n="e" s="T698">miʔ </ts>
               <ts e="T700" id="Seg_5309" n="e" s="T699">Krasnojarskəʔi </ts>
               <ts e="T701" id="Seg_5311" n="e" s="T700">kalləbaʔ. </ts>
               <ts e="T702" id="Seg_5313" n="e" s="T701">A </ts>
               <ts e="T703" id="Seg_5315" n="e" s="T702">măn </ts>
               <ts e="T704" id="Seg_5317" n="e" s="T703">mămbiam </ts>
               <ts e="T705" id="Seg_5319" n="e" s="T704">dĭʔnə: </ts>
               <ts e="T706" id="Seg_5321" n="e" s="T705">Igeʔ </ts>
               <ts e="T707" id="Seg_5323" n="e" s="T706">ianəl </ts>
               <ts e="T708" id="Seg_5325" n="e" s="T707">ĭmbi-nʼibudʼ </ts>
               <ts e="T709" id="Seg_5327" n="e" s="T708">kuvas, </ts>
               <ts e="T710" id="Seg_5329" n="e" s="T709">plat </ts>
               <ts e="T711" id="Seg_5331" n="e" s="T710">(ib-) </ts>
               <ts e="T712" id="Seg_5333" n="e" s="T711">igeʔ. </ts>
               <ts e="T713" id="Seg_5335" n="e" s="T712">Oldʼa </ts>
               <ts e="T714" id="Seg_5337" n="e" s="T713">it </ts>
               <ts e="T715" id="Seg_5339" n="e" s="T714">kuvas. </ts>
               <ts e="T716" id="Seg_5341" n="e" s="T715">Dĭzeŋ </ts>
               <ts e="T717" id="Seg_5343" n="e" s="T716">bar </ts>
               <ts e="T718" id="Seg_5345" n="e" s="T717">kaknarlaʔbəʔjə, </ts>
               <ts e="T719" id="Seg_5347" n="e" s="T718">što </ts>
               <ts e="T720" id="Seg_5349" n="e" s="T719">măn </ts>
               <ts e="T721" id="Seg_5351" n="e" s="T720">dăre </ts>
               <ts e="T722" id="Seg_5353" n="e" s="T721">dʼăbaktərlaʔbəm. </ts>
               <ts e="T723" id="Seg_5355" n="e" s="T722">Dĭzeŋ </ts>
               <ts e="T724" id="Seg_5357" n="e" s="T723">ej </ts>
               <ts e="T725" id="Seg_5359" n="e" s="T724">tĭmneʔi </ts>
               <ts e="T726" id="Seg_5361" n="e" s="T725">dʼăbaktərzittə, </ts>
               <ts e="T727" id="Seg_5363" n="e" s="T726">они </ts>
               <ts e="T728" id="Seg_5365" n="e" s="T727">не </ts>
               <ts e="T729" id="Seg_5367" n="e" s="T728">знают </ts>
               <ts e="T730" id="Seg_5369" n="e" s="T729">говорить. </ts>
               <ts e="T731" id="Seg_5371" n="e" s="T730">((BRK)). </ts>
               <ts e="T732" id="Seg_5373" n="e" s="T731">Teinen </ts>
               <ts e="T733" id="Seg_5375" n="e" s="T732">nüke </ts>
               <ts e="T734" id="Seg_5377" n="e" s="T733">šobi. </ts>
               <ts e="T735" id="Seg_5379" n="e" s="T734">Nada </ts>
               <ts e="T736" id="Seg_5381" n="e" s="T735">măna </ts>
               <ts e="T737" id="Seg_5383" n="e" s="T736">toltanoʔ </ts>
               <ts e="T738" id="Seg_5385" n="e" s="T737">amzittə, </ts>
               <ts e="T739" id="Seg_5387" n="e" s="T738">kallam </ts>
               <ts e="T740" id="Seg_5389" n="e" s="T739">tüjö </ts>
               <ts e="T741" id="Seg_5391" n="e" s="T740">bostə </ts>
               <ts e="T742" id="Seg_5393" n="e" s="T741">nʼinə, </ts>
               <ts e="T743" id="Seg_5395" n="e" s="T742">puskaj </ts>
               <ts e="T744" id="Seg_5397" n="e" s="T743">amnolləj </ts>
               <ts e="T745" id="Seg_5399" n="e" s="T744">măna </ts>
               <ts e="T746" id="Seg_5401" n="e" s="T745">toltanoʔ". </ts>
               <ts e="T747" id="Seg_5403" n="e" s="T746">((BRK)). </ts>
               <ts e="T748" id="Seg_5405" n="e" s="T747">Măn </ts>
               <ts e="T749" id="Seg_5407" n="e" s="T748">ibiem </ts>
               <ts e="T750" id="Seg_5409" n="e" s="T749">(Zaozʼorkagən). </ts>
               <ts e="T751" id="Seg_5411" n="e" s="T750">(Xatʼel=) </ts>
               <ts e="T752" id="Seg_5413" n="e" s="T751">Kambiam </ts>
               <ts e="T753" id="Seg_5415" n="e" s="T752">aptʼekanə. </ts>
               <ts e="T754" id="Seg_5417" n="e" s="T753">Simandə </ts>
               <ts e="T755" id="Seg_5419" n="e" s="T754">xatʼel </ts>
               <ts e="T756" id="Seg_5421" n="e" s="T755">izittə. </ts>
               <ts e="T757" id="Seg_5423" n="e" s="T756">Štobɨ </ts>
               <ts e="T758" id="Seg_5425" n="e" s="T757">măndərzittə </ts>
               <ts e="T759" id="Seg_5427" n="e" s="T758">kuŋgeŋ. </ts>
               <ts e="T760" id="Seg_5429" n="e" s="T759">Dĭzeŋ </ts>
               <ts e="T761" id="Seg_5431" n="e" s="T760">ej </ts>
               <ts e="T762" id="Seg_5433" n="e" s="T761">mĭbiʔi, </ts>
               <ts e="T763" id="Seg_5435" n="e" s="T762">măndlaʔbəʔjə: </ts>
               <ts e="T764" id="Seg_5437" n="e" s="T763">Kanaʔ. </ts>
               <ts e="T765" id="Seg_5439" n="e" s="T764">Sʼimal </ts>
               <ts e="T766" id="Seg_5441" n="e" s="T765">măndərdə, </ts>
               <ts e="T767" id="Seg_5443" n="e" s="T766">dĭgəttə </ts>
               <ts e="T768" id="Seg_5445" n="e" s="T767">šolal </ts>
               <ts e="T769" id="Seg_5447" n="e" s="T768">i </ts>
               <ts e="T770" id="Seg_5449" n="e" s="T769">ilil". </ts>
               <ts e="T771" id="Seg_5451" n="e" s="T770">((BRK)). </ts>
               <ts e="T772" id="Seg_5453" n="e" s="T771">Dĭbər </ts>
               <ts e="T773" id="Seg_5455" n="e" s="T772">kambiam, </ts>
               <ts e="T774" id="Seg_5457" n="e" s="T773">dĭzeŋ </ts>
               <ts e="T775" id="Seg_5459" n="e" s="T774">bar </ts>
               <ts e="T776" id="Seg_5461" n="e" s="T775">dʼabərolaʔbə, </ts>
               <ts e="T777" id="Seg_5463" n="e" s="T776">Ĭmbi </ts>
               <ts e="T778" id="Seg_5465" n="e" s="T777">dʼabərolaʔbəlaʔ? </ts>
               <ts e="T779" id="Seg_5467" n="e" s="T778">Iʔ </ts>
               <ts e="T780" id="Seg_5469" n="e" s="T779">kirgarlaʔ. </ts>
               <ts e="T781" id="Seg_5471" n="e" s="T780">Jakšəŋ </ts>
               <ts e="T782" id="Seg_5473" n="e" s="T781">agaʔ. </ts>
               <ts e="T783" id="Seg_5475" n="e" s="T782">Tura </ts>
               <ts e="T784" id="Seg_5477" n="e" s="T783">sĭbrejeʔ". </ts>
               <ts e="T785" id="Seg_5479" n="e" s="T784">((BRK)). </ts>
               <ts e="T786" id="Seg_5481" n="e" s="T785">Onʼiʔtə </ts>
               <ts e="T787" id="Seg_5483" n="e" s="T786">mămbiam: </ts>
               <ts e="T788" id="Seg_5485" n="e" s="T787">Kanaʔ. </ts>
               <ts e="T789" id="Seg_5487" n="e" s="T788">Lʼondə </ts>
               <ts e="T790" id="Seg_5489" n="e" s="T789">toʔnardə. </ts>
               <ts e="T791" id="Seg_5491" n="e" s="T790">A </ts>
               <ts e="T792" id="Seg_5493" n="e" s="T791">dĭ </ts>
               <ts e="T793" id="Seg_5495" n="e" s="T792">pušaj </ts>
               <ts e="T794" id="Seg_5497" n="e" s="T793">bü </ts>
               <ts e="T795" id="Seg_5499" n="e" s="T794">tažorləj </ts>
               <ts e="T796" id="Seg_5501" n="e" s="T795">moltʼanə. </ts>
               <ts e="T797" id="Seg_5503" n="e" s="T796">((BRK)). </ts>
               <ts e="T798" id="Seg_5505" n="e" s="T797">Măn </ts>
               <ts e="T799" id="Seg_5507" n="e" s="T798">teinen </ts>
               <ts e="T800" id="Seg_5509" n="e" s="T799">erte </ts>
               <ts e="T801" id="Seg_5511" n="e" s="T800">uʔbdəbiam, </ts>
               <ts e="T802" id="Seg_5513" n="e" s="T801">iššo </ts>
               <ts e="T803" id="Seg_5515" n="e" s="T802">(š-) </ts>
               <ts e="T804" id="Seg_5517" n="e" s="T803">sumna </ts>
               <ts e="T805" id="Seg_5519" n="e" s="T804">nagobi. </ts>
               <ts e="T806" id="Seg_5521" n="e" s="T805">Dĭgəttə </ts>
               <ts e="T807" id="Seg_5523" n="e" s="T806">pʼešbə </ts>
               <ts e="T808" id="Seg_5525" n="e" s="T807">nendəbiem. </ts>
               <ts e="T809" id="Seg_5527" n="e" s="T808">Po </ts>
               <ts e="T810" id="Seg_5529" n="e" s="T809">nuldəbiam. </ts>
               <ts e="T811" id="Seg_5531" n="e" s="T810">Dĭgəttə </ts>
               <ts e="T812" id="Seg_5533" n="e" s="T811">bazoʔ </ts>
               <ts e="T813" id="Seg_5535" n="e" s="T812">iʔbəbiem, </ts>
               <ts e="T814" id="Seg_5537" n="e" s="T813">iššo </ts>
               <ts e="T815" id="Seg_5539" n="e" s="T814">kunolbiam. </ts>
               <ts e="T816" id="Seg_5541" n="e" s="T815">Iššo </ts>
               <ts e="T817" id="Seg_5543" n="e" s="T816">bieʔ </ts>
               <ts e="T818" id="Seg_5545" n="e" s="T817">mobi, </ts>
               <ts e="T819" id="Seg_5547" n="e" s="T818">dĭgəttə </ts>
               <ts e="T820" id="Seg_5549" n="e" s="T819">(kolb-) </ts>
               <ts e="T821" id="Seg_5551" n="e" s="T820">uʔbdəbiam. </ts>
               <ts e="T822" id="Seg_5553" n="e" s="T821">((BRK)). </ts>
               <ts e="T823" id="Seg_5555" n="e" s="T822">Sedem </ts>
               <ts e="T824" id="Seg_5557" n="e" s="T823">teinen </ts>
               <ts e="T825" id="Seg_5559" n="e" s="T824">togonorbiam, </ts>
               <ts e="T826" id="Seg_5561" n="e" s="T825">a </ts>
               <ts e="T827" id="Seg_5563" n="e" s="T826">taldʼen </ts>
               <ts e="T828" id="Seg_5565" n="e" s="T827">ej </ts>
               <ts e="T829" id="Seg_5567" n="e" s="T828">sedem. </ts>
               <ts e="T830" id="Seg_5569" n="e" s="T829">((BRK)). </ts>
               <ts e="T831" id="Seg_5571" n="e" s="T830">Dĭ </ts>
               <ts e="T832" id="Seg_5573" n="e" s="T831">pi </ts>
               <ts e="T833" id="Seg_5575" n="e" s="T832">ugandə </ts>
               <ts e="T834" id="Seg_5577" n="e" s="T833">urgo, </ts>
               <ts e="T835" id="Seg_5579" n="e" s="T834">sedem. </ts>
               <ts e="T836" id="Seg_5581" n="e" s="T835">A </ts>
               <ts e="T837" id="Seg_5583" n="e" s="T836">dö </ts>
               <ts e="T838" id="Seg_5585" n="e" s="T837">pa </ts>
               <ts e="T839" id="Seg_5587" n="e" s="T838">ej </ts>
               <ts e="T840" id="Seg_5589" n="e" s="T839">sedem. </ts>
               <ts e="T841" id="Seg_5591" n="e" s="T840">((BRK)). </ts>
               <ts e="T842" id="Seg_5593" n="e" s="T841">(Dĭ </ts>
               <ts e="T843" id="Seg_5595" n="e" s="T842">es-) </ts>
               <ts e="T844" id="Seg_5597" n="e" s="T843">Dĭ </ts>
               <ts e="T845" id="Seg_5599" n="e" s="T844">ešši </ts>
               <ts e="T846" id="Seg_5601" n="e" s="T845">bar </ts>
               <ts e="T847" id="Seg_5603" n="e" s="T846">piʔmezeŋdə </ts>
               <ts e="T848" id="Seg_5605" n="e" s="T847">bar </ts>
               <ts e="T849" id="Seg_5607" n="e" s="T848">sajnožuʔpi </ts>
               <ts e="T850" id="Seg_5609" n="e" s="T849">bar, </ts>
               <ts e="T851" id="Seg_5611" n="e" s="T850">ši </ts>
               <ts e="T852" id="Seg_5613" n="e" s="T851">molaːmbi. </ts>
               <ts e="T853" id="Seg_5615" n="e" s="T852">Nada </ts>
               <ts e="T854" id="Seg_5617" n="e" s="T853">dĭm </ts>
               <ts e="T855" id="Seg_5619" n="e" s="T854">šöʔsittə. </ts>
               <ts e="T856" id="Seg_5621" n="e" s="T855">Заплатка </ts>
               <ts e="T857" id="Seg_5623" n="e" s="T856">enzittə. </ts>
               <ts e="T858" id="Seg_5625" n="e" s="T857">((BRK)). </ts>
               <ts e="T859" id="Seg_5627" n="e" s="T858">Edəʔ </ts>
               <ts e="T860" id="Seg_5629" n="e" s="T859">măna </ts>
               <ts e="T861" id="Seg_5631" n="e" s="T860">sĭreʔpne </ts>
               <ts e="T862" id="Seg_5633" n="e" s="T861">sumna </ts>
               <ts e="T863" id="Seg_5635" n="e" s="T862">kilogram. </ts>
               <ts e="T864" id="Seg_5637" n="e" s="T863">((BRK)). </ts>
               <ts e="T865" id="Seg_5639" n="e" s="T864">Dĭ </ts>
               <ts e="T866" id="Seg_5641" n="e" s="T865">kuza </ts>
               <ts e="T867" id="Seg_5643" n="e" s="T866">ĭzembi, </ts>
               <ts e="T868" id="Seg_5645" n="e" s="T867">a </ts>
               <ts e="T869" id="Seg_5647" n="e" s="T868">tüj </ts>
               <ts e="T870" id="Seg_5649" n="e" s="T869">ej </ts>
               <ts e="T871" id="Seg_5651" n="e" s="T870">ĭzemnie, </ts>
               <ts e="T872" id="Seg_5653" n="e" s="T871">uʔbdəbi, </ts>
               <ts e="T873" id="Seg_5655" n="e" s="T872">üjüzi </ts>
               <ts e="T874" id="Seg_5657" n="e" s="T873">mĭŋge, </ts>
               <ts e="T875" id="Seg_5659" n="e" s="T874">a_to </ts>
               <ts e="T876" id="Seg_5661" n="e" s="T875">iʔbobi. </ts>
               <ts e="T877" id="Seg_5663" n="e" s="T876">Ej </ts>
               <ts e="T878" id="Seg_5665" n="e" s="T877">jakšə </ts>
               <ts e="T879" id="Seg_5667" n="e" s="T878">nʼilgölaʔsittə. </ts>
               <ts e="T880" id="Seg_5669" n="e" s="T879">Šobiʔi </ts>
               <ts e="T881" id="Seg_5671" n="e" s="T880">da </ts>
               <ts e="T882" id="Seg_5673" n="e" s="T881">mămbiʔi, </ts>
               <ts e="T883" id="Seg_5675" n="e" s="T882">ej </ts>
               <ts e="T884" id="Seg_5677" n="e" s="T883">kuvas. </ts>
               <ts e="T885" id="Seg_5679" n="e" s="T884">Dʼăbaktərla. </ts>
               <ts e="T886" id="Seg_5681" n="e" s="T885">((BRK)). </ts>
               <ts e="T887" id="Seg_5683" n="e" s="T886">(Dĭ=) </ts>
               <ts e="T888" id="Seg_5685" n="e" s="T887">dĭ </ts>
               <ts e="T889" id="Seg_5687" n="e" s="T888">bar </ts>
               <ts e="T890" id="Seg_5689" n="e" s="T889">kuzam </ts>
               <ts e="T891" id="Seg_5691" n="e" s="T890">kuʔpi. </ts>
               <ts e="T892" id="Seg_5693" n="e" s="T891">Dĭm </ts>
               <ts e="T893" id="Seg_5695" n="e" s="T892">amnolbiʔi. </ts>
               <ts e="T894" id="Seg_5697" n="e" s="T893">(Na-) </ts>
               <ts e="T895" id="Seg_5699" n="e" s="T894">Nagur </ts>
               <ts e="T896" id="Seg_5701" n="e" s="T895">(kö) </ts>
               <ts e="T897" id="Seg_5703" n="e" s="T896">amnolaʔpi </ts>
               <ts e="T898" id="Seg_5705" n="e" s="T897">i </ts>
               <ts e="T899" id="Seg_5707" n="e" s="T898">tuj </ts>
               <ts e="T900" id="Seg_5709" n="e" s="T899">maːʔndə </ts>
               <ts e="T901" id="Seg_5711" n="e" s="T900">šobi </ts>
               <ts e="T902" id="Seg_5713" n="e" s="T901">i </ts>
               <ts e="T903" id="Seg_5715" n="e" s="T902">mĭlleʔbə. </ts>
               <ts e="T904" id="Seg_5717" n="e" s="T903">((BRK)). </ts>
               <ts e="T905" id="Seg_5719" n="e" s="T904">Bar </ts>
               <ts e="T906" id="Seg_5721" n="e" s="T905">takšeʔi </ts>
               <ts e="T907" id="Seg_5723" n="e" s="T906">(bădlu- </ts>
               <ts e="T908" id="Seg_5725" n="e" s="T907">b- </ts>
               <ts e="T909" id="Seg_5727" n="e" s="T908">bădlu-) </ts>
               <ts e="T910" id="Seg_5729" n="e" s="T909">băldluʔpiʔi. </ts>
               <ts e="T911" id="Seg_5731" n="e" s="T910">((BRK)). </ts>
               <ts e="T912" id="Seg_5733" n="e" s="T911">((BRK)). </ts>
               <ts e="T913" id="Seg_5735" n="e" s="T912">Măn </ts>
               <ts e="T914" id="Seg_5737" n="e" s="T913">(ul- </ts>
               <ts e="T915" id="Seg_5739" n="e" s="T914">ul-) </ts>
               <ts e="T916" id="Seg_5741" n="e" s="T915">ulum </ts>
               <ts e="T917" id="Seg_5743" n="e" s="T916">bar </ts>
               <ts e="T918" id="Seg_5745" n="e" s="T917">piziʔ </ts>
               <ts e="T919" id="Seg_5747" n="e" s="T918">toʔnarluʔpiʔi </ts>
               <ts e="T920" id="Seg_5749" n="e" s="T919">bar. </ts>
               <ts e="T921" id="Seg_5751" n="e" s="T920">Bar </ts>
               <ts e="T922" id="Seg_5753" n="e" s="T921">kem </ts>
               <ts e="T923" id="Seg_5755" n="e" s="T922">mʼaŋŋaʔbə. </ts>
               <ts e="T925" id="Seg_5757" n="e" s="T923">(Măna) </ts>
               <ts e="T926" id="Seg_5759" n="e" s="T925">baltuziʔ </ts>
               <ts e="T927" id="Seg_5761" n="e" s="T926">bar </ts>
               <ts e="T928" id="Seg_5763" n="e" s="T927">ulum </ts>
               <ts e="T929" id="Seg_5765" n="e" s="T928">(bă- </ts>
               <ts e="T930" id="Seg_5767" n="e" s="T929">)… </ts>
               <ts e="T931" id="Seg_5769" n="e" s="T930">((BRK)). </ts>
               <ts e="T932" id="Seg_5771" n="e" s="T931">Băldəbiʔi. </ts>
               <ts e="T934" id="Seg_5773" n="e" s="T932"> Dʼijenə </ts>
               <ts e="T935" id="Seg_5775" n="e" s="T934">kalal </ts>
               <ts e="T936" id="Seg_5777" n="e" s="T935">keʔbde </ts>
               <ts e="T937" id="Seg_5779" n="e" s="T936">oʔbdəsʼtə. </ts>
               <ts e="T938" id="Seg_5781" n="e" s="T937">Kanžəbaʔ </ts>
               <ts e="T939" id="Seg_5783" n="e" s="T938">maʔnʼibeʔ, </ts>
               <ts e="T940" id="Seg_5785" n="e" s="T939">keʔbde </ts>
               <ts e="T941" id="Seg_5787" n="e" s="T940">naga. </ts>
               <ts e="T942" id="Seg_5789" n="e" s="T941">Tolʼko </ts>
               <ts e="T943" id="Seg_5791" n="e" s="T942">(ka-) </ts>
               <ts e="T944" id="Seg_5793" n="e" s="T943">kanzittə. </ts>
               <ts e="T945" id="Seg_5795" n="e" s="T944">Gijendə </ts>
               <ts e="T946" id="Seg_5797" n="e" s="T945">šoləʔjə, </ts>
               <ts e="T947" id="Seg_5799" n="e" s="T946">ugandə </ts>
               <ts e="T948" id="Seg_5801" n="e" s="T947">iʔgö </ts>
               <ts e="T949" id="Seg_5803" n="e" s="T948">keʔbde. </ts>
               <ts e="T950" id="Seg_5805" n="e" s="T949">Dĭgəttə </ts>
               <ts e="T951" id="Seg_5807" n="e" s="T950">bazoʔ </ts>
               <ts e="T952" id="Seg_5809" n="e" s="T951">oʔbdəlial. </ts>
               <ts e="T953" id="Seg_5811" n="e" s="T952">Dĭgəttə </ts>
               <ts e="T954" id="Seg_5813" n="e" s="T953">(moliam) </ts>
               <ts e="T955" id="Seg_5815" n="e" s="T954">kamen </ts>
               <ts e="T956" id="Seg_5817" n="e" s="T955">maʔnʼi </ts>
               <ts e="T957" id="Seg_5819" n="e" s="T956">kanzittə, </ts>
               <ts e="T958" id="Seg_5821" n="e" s="T957">üge </ts>
               <ts e="T959" id="Seg_5823" n="e" s="T958">keʔbde </ts>
               <ts e="T960" id="Seg_5825" n="e" s="T959">iʔgö. </ts>
               <ts e="T961" id="Seg_5827" n="e" s="T960">((…)). </ts>
               <ts e="T962" id="Seg_5829" n="e" s="T961">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T8" id="Seg_5830" s="T4">PKZ_1964_SU0211.PKZ.001 (002)</ta>
            <ta e="T11" id="Seg_5831" s="T8">PKZ_1964_SU0211.PKZ.002 (003)</ta>
            <ta e="T14" id="Seg_5832" s="T11">PKZ_1964_SU0211.PKZ.003 (004)</ta>
            <ta e="T17" id="Seg_5833" s="T14">PKZ_1964_SU0211.PKZ.004 (005)</ta>
            <ta e="T19" id="Seg_5834" s="T17">PKZ_1964_SU0211.PKZ.005 (006)</ta>
            <ta e="T25" id="Seg_5835" s="T19">PKZ_1964_SU0211.PKZ.006 (007)</ta>
            <ta e="T27" id="Seg_5836" s="T25">PKZ_1964_SU0211.PKZ.007 (008)</ta>
            <ta e="T40" id="Seg_5837" s="T27">PKZ_1964_SU0211.PKZ.008 (009)</ta>
            <ta e="T45" id="Seg_5838" s="T40">PKZ_1964_SU0211.PKZ.009 (011)</ta>
            <ta e="T51" id="Seg_5839" s="T45">PKZ_1964_SU0211.PKZ.010 (012)</ta>
            <ta e="T52" id="Seg_5840" s="T51">PKZ_1964_SU0211.PKZ.011 (013)</ta>
            <ta e="T60" id="Seg_5841" s="T52">PKZ_1964_SU0211.PKZ.012 (014)</ta>
            <ta e="T68" id="Seg_5842" s="T60">PKZ_1964_SU0211.PKZ.013 (015)</ta>
            <ta e="T77" id="Seg_5843" s="T68">PKZ_1964_SU0211.PKZ.014 (016)</ta>
            <ta e="T80" id="Seg_5844" s="T77">PKZ_1964_SU0211.PKZ.015 (017)</ta>
            <ta e="T81" id="Seg_5845" s="T80">PKZ_1964_SU0211.PKZ.016 (018)</ta>
            <ta e="T84" id="Seg_5846" s="T81">PKZ_1964_SU0211.PKZ.017 (019)</ta>
            <ta e="T95" id="Seg_5847" s="T84">PKZ_1964_SU0211.PKZ.018 (020)</ta>
            <ta e="T96" id="Seg_5848" s="T95">PKZ_1964_SU0211.PKZ.019 (021)</ta>
            <ta e="T102" id="Seg_5849" s="T96">PKZ_1964_SU0211.PKZ.020 (022)</ta>
            <ta e="T103" id="Seg_5850" s="T102">PKZ_1964_SU0211.PKZ.021 (023)</ta>
            <ta e="T109" id="Seg_5851" s="T103">PKZ_1964_SU0211.PKZ.022 (024)</ta>
            <ta e="T110" id="Seg_5852" s="T109">PKZ_1964_SU0211.PKZ.023 (025)</ta>
            <ta e="T114" id="Seg_5853" s="T110">PKZ_1964_SU0211.PKZ.024 (026)</ta>
            <ta e="T122" id="Seg_5854" s="T114">PKZ_1964_SU0211.PKZ.025 (027)</ta>
            <ta e="T132" id="Seg_5855" s="T122">PKZ_1964_SU0211.PKZ.026 (028) </ta>
            <ta e="T143" id="Seg_5856" s="T132">PKZ_1964_SU0211.PKZ.027 (030)</ta>
            <ta e="T146" id="Seg_5857" s="T143">PKZ_1964_SU0211.PKZ.028 (031)</ta>
            <ta e="T153" id="Seg_5858" s="T146">PKZ_1964_SU0211.PKZ.029 (032)</ta>
            <ta e="T160" id="Seg_5859" s="T153">PKZ_1964_SU0211.PKZ.030 (033)</ta>
            <ta e="T162" id="Seg_5860" s="T160">PKZ_1964_SU0211.PKZ.031 (034)</ta>
            <ta e="T168" id="Seg_5861" s="T162">PKZ_1964_SU0211.PKZ.032 (035)</ta>
            <ta e="T169" id="Seg_5862" s="T168">PKZ_1964_SU0211.PKZ.033 (036)</ta>
            <ta e="T173" id="Seg_5863" s="T169">PKZ_1964_SU0211.PKZ.034 (037)</ta>
            <ta e="T179" id="Seg_5864" s="T173">PKZ_1964_SU0211.PKZ.035 (038)</ta>
            <ta e="T186" id="Seg_5865" s="T179">PKZ_1964_SU0211.PKZ.036 (039)</ta>
            <ta e="T189" id="Seg_5866" s="T186">PKZ_1964_SU0211.PKZ.037 (040)</ta>
            <ta e="T196" id="Seg_5867" s="T189">PKZ_1964_SU0211.PKZ.038 (041)</ta>
            <ta e="T202" id="Seg_5868" s="T196">PKZ_1964_SU0211.PKZ.039 (042)</ta>
            <ta e="T207" id="Seg_5869" s="T202">PKZ_1964_SU0211.PKZ.040 (043)</ta>
            <ta e="T217" id="Seg_5870" s="T207">PKZ_1964_SU0211.PKZ.041 (044)</ta>
            <ta e="T220" id="Seg_5871" s="T217">PKZ_1964_SU0211.PKZ.042 (045)</ta>
            <ta e="T228" id="Seg_5872" s="T220">PKZ_1964_SU0211.PKZ.043 (046)</ta>
            <ta e="T229" id="Seg_5873" s="T228">PKZ_1964_SU0211.PKZ.044 (047)</ta>
            <ta e="T235" id="Seg_5874" s="T229">PKZ_1964_SU0211.PKZ.045 (048)</ta>
            <ta e="T236" id="Seg_5875" s="T235">PKZ_1964_SU0211.PKZ.046 (049)</ta>
            <ta e="T243" id="Seg_5876" s="T236">PKZ_1964_SU0211.PKZ.047 (050)</ta>
            <ta e="T250" id="Seg_5877" s="T243">PKZ_1964_SU0211.PKZ.048 (051)</ta>
            <ta e="T251" id="Seg_5878" s="T250">PKZ_1964_SU0211.PKZ.049 (052)</ta>
            <ta e="T256" id="Seg_5879" s="T251">PKZ_1964_SU0211.PKZ.050 (053)</ta>
            <ta e="T260" id="Seg_5880" s="T256">PKZ_1964_SU0211.PKZ.051 (054)</ta>
            <ta e="T263" id="Seg_5881" s="T260">PKZ_1964_SU0211.PKZ.052 (055)</ta>
            <ta e="T264" id="Seg_5882" s="T263">PKZ_1964_SU0211.PKZ.053 (056)</ta>
            <ta e="T272" id="Seg_5883" s="T264">PKZ_1964_SU0211.PKZ.054 (057)</ta>
            <ta e="T278" id="Seg_5884" s="T272">PKZ_1964_SU0211.PKZ.055 (059)</ta>
            <ta e="T282" id="Seg_5885" s="T278">PKZ_1964_SU0211.PKZ.056 (060)</ta>
            <ta e="T284" id="Seg_5886" s="T282">PKZ_1964_SU0211.PKZ.057 (061)</ta>
            <ta e="T291" id="Seg_5887" s="T284">PKZ_1964_SU0211.PKZ.058 (062)</ta>
            <ta e="T298" id="Seg_5888" s="T291">PKZ_1964_SU0211.PKZ.059 (063)</ta>
            <ta e="T299" id="Seg_5889" s="T298">PKZ_1964_SU0211.PKZ.060 (064)</ta>
            <ta e="T304" id="Seg_5890" s="T299">PKZ_1964_SU0211.PKZ.061 (065)</ta>
            <ta e="T312" id="Seg_5891" s="T304">PKZ_1964_SU0211.PKZ.062 (066)</ta>
            <ta e="T315" id="Seg_5892" s="T312">PKZ_1964_SU0211.PKZ.063 (067)</ta>
            <ta e="T322" id="Seg_5893" s="T315">PKZ_1964_SU0211.PKZ.064 (068)</ta>
            <ta e="T329" id="Seg_5894" s="T322">PKZ_1964_SU0211.PKZ.065 (069)</ta>
            <ta e="T332" id="Seg_5895" s="T329">PKZ_1964_SU0211.PKZ.066 (070)</ta>
            <ta e="T337" id="Seg_5896" s="T332">PKZ_1964_SU0211.PKZ.067 (071)</ta>
            <ta e="T338" id="Seg_5897" s="T337">PKZ_1964_SU0211.PKZ.068 (072)</ta>
            <ta e="T345" id="Seg_5898" s="T338">PKZ_1964_SU0211.PKZ.069 (073)</ta>
            <ta e="T346" id="Seg_5899" s="T345">PKZ_1964_SU0211.PKZ.070 (074)</ta>
            <ta e="T349" id="Seg_5900" s="T346">PKZ_1964_SU0211.PKZ.071 (075)</ta>
            <ta e="T356" id="Seg_5901" s="T349">PKZ_1964_SU0211.PKZ.072 (076)</ta>
            <ta e="T357" id="Seg_5902" s="T356">PKZ_1964_SU0211.PKZ.073 (077)</ta>
            <ta e="T360" id="Seg_5903" s="T357">PKZ_1964_SU0211.PKZ.074 (078)</ta>
            <ta e="T363" id="Seg_5904" s="T360">PKZ_1964_SU0211.PKZ.075 (079)</ta>
            <ta e="T369" id="Seg_5905" s="T363">PKZ_1964_SU0211.PKZ.076 (080)</ta>
            <ta e="T373" id="Seg_5906" s="T369">PKZ_1964_SU0211.PKZ.077 (081)</ta>
            <ta e="T374" id="Seg_5907" s="T373">PKZ_1964_SU0211.PKZ.078 (082)</ta>
            <ta e="T380" id="Seg_5908" s="T374">PKZ_1964_SU0211.PKZ.079 (083)</ta>
            <ta e="T381" id="Seg_5909" s="T380">PKZ_1964_SU0211.PKZ.080 (084)</ta>
            <ta e="T386" id="Seg_5910" s="T381">PKZ_1964_SU0211.PKZ.081 (085)</ta>
            <ta e="T387" id="Seg_5911" s="T386">PKZ_1964_SU0211.PKZ.082 (086)</ta>
            <ta e="T392" id="Seg_5912" s="T387">PKZ_1964_SU0211.PKZ.083 (087)</ta>
            <ta e="T396" id="Seg_5913" s="T392">PKZ_1964_SU0211.PKZ.084 (088)</ta>
            <ta e="T397" id="Seg_5914" s="T396">PKZ_1964_SU0211.PKZ.085 (089)</ta>
            <ta e="T400" id="Seg_5915" s="T397">PKZ_1964_SU0211.PKZ.086 (090)</ta>
            <ta e="T405" id="Seg_5916" s="T400">PKZ_1964_SU0211.PKZ.087 (091)</ta>
            <ta e="T409" id="Seg_5917" s="T405">PKZ_1964_SU0211.PKZ.088 (092)</ta>
            <ta e="T410" id="Seg_5918" s="T409">PKZ_1964_SU0211.PKZ.089 (093)</ta>
            <ta e="T416" id="Seg_5919" s="T410">PKZ_1964_SU0211.PKZ.090 (094)</ta>
            <ta e="T420" id="Seg_5920" s="T416">PKZ_1964_SU0211.PKZ.091 (095)</ta>
            <ta e="T428" id="Seg_5921" s="T420">PKZ_1964_SU0211.PKZ.092 (096)</ta>
            <ta e="T429" id="Seg_5922" s="T428">PKZ_1964_SU0211.PKZ.093 (097)</ta>
            <ta e="T435" id="Seg_5923" s="T429">PKZ_1964_SU0211.PKZ.094 (098)</ta>
            <ta e="T436" id="Seg_5924" s="T435">PKZ_1964_SU0211.PKZ.095 (099)</ta>
            <ta e="T444" id="Seg_5925" s="T436">PKZ_1964_SU0211.PKZ.096 (100)</ta>
            <ta e="T448" id="Seg_5926" s="T444">PKZ_1964_SU0211.PKZ.097 (101)</ta>
            <ta e="T454" id="Seg_5927" s="T448">PKZ_1964_SU0211.PKZ.098 (102)</ta>
            <ta e="T460" id="Seg_5928" s="T454">PKZ_1964_SU0211.PKZ.099 (103)</ta>
            <ta e="T461" id="Seg_5929" s="T460">PKZ_1964_SU0211.PKZ.100 (104)</ta>
            <ta e="T467" id="Seg_5930" s="T461">PKZ_1964_SU0211.PKZ.101 (105)</ta>
            <ta e="T468" id="Seg_5931" s="T467">PKZ_1964_SU0211.PKZ.102 (106)</ta>
            <ta e="T470" id="Seg_5932" s="T468">PKZ_1964_SU0211.PKZ.103 (107)</ta>
            <ta e="T473" id="Seg_5933" s="T470">PKZ_1964_SU0211.PKZ.104 (108)</ta>
            <ta e="T476" id="Seg_5934" s="T473">PKZ_1964_SU0211.PKZ.105 (109)</ta>
            <ta e="T484" id="Seg_5935" s="T476">PKZ_1964_SU0211.PKZ.106 (110)</ta>
            <ta e="T492" id="Seg_5936" s="T484">PKZ_1964_SU0211.PKZ.107 (111)</ta>
            <ta e="T501" id="Seg_5937" s="T492">PKZ_1964_SU0211.PKZ.108 (112)</ta>
            <ta e="T507" id="Seg_5938" s="T501">PKZ_1964_SU0211.PKZ.109 (113)</ta>
            <ta e="T518" id="Seg_5939" s="T507">PKZ_1964_SU0211.PKZ.110 (114)</ta>
            <ta e="T522" id="Seg_5940" s="T518">PKZ_1964_SU0211.PKZ.111 (115)</ta>
            <ta e="T528" id="Seg_5941" s="T522">PKZ_1964_SU0211.PKZ.112 (116)</ta>
            <ta e="T529" id="Seg_5942" s="T528">PKZ_1964_SU0211.PKZ.113 (117)</ta>
            <ta e="T533" id="Seg_5943" s="T529">PKZ_1964_SU0211.PKZ.114 (118)</ta>
            <ta e="T536" id="Seg_5944" s="T533">PKZ_1964_SU0211.PKZ.115 (119)</ta>
            <ta e="T537" id="Seg_5945" s="T536">PKZ_1964_SU0211.PKZ.116 (120)</ta>
            <ta e="T541" id="Seg_5946" s="T537">PKZ_1964_SU0211.PKZ.117 (121)</ta>
            <ta e="T544" id="Seg_5947" s="T541">PKZ_1964_SU0211.PKZ.118 (122)</ta>
            <ta e="T552" id="Seg_5948" s="T544">PKZ_1964_SU0211.PKZ.119 (123)</ta>
            <ta e="T553" id="Seg_5949" s="T552">PKZ_1964_SU0211.PKZ.120 (124)</ta>
            <ta e="T555" id="Seg_5950" s="T553">PKZ_1964_SU0211.PKZ.121 (125)</ta>
            <ta e="T556" id="Seg_5951" s="T555">PKZ_1964_SU0211.PKZ.122 (126)</ta>
            <ta e="T564" id="Seg_5952" s="T556">PKZ_1964_SU0211.PKZ.123 (127)</ta>
            <ta e="T566" id="Seg_5953" s="T564">PKZ_1964_SU0211.PKZ.124 (128)</ta>
            <ta e="T570" id="Seg_5954" s="T566">PKZ_1964_SU0211.PKZ.125 (129)</ta>
            <ta e="T577" id="Seg_5955" s="T570">PKZ_1964_SU0211.PKZ.126 (130) </ta>
            <ta e="T582" id="Seg_5956" s="T577">PKZ_1964_SU0211.PKZ.127 (132)</ta>
            <ta e="T593" id="Seg_5957" s="T582">PKZ_1964_SU0211.PKZ.128 (133)</ta>
            <ta e="T594" id="Seg_5958" s="T593">PKZ_1964_SU0211.PKZ.129 (134)</ta>
            <ta e="T601" id="Seg_5959" s="T594">PKZ_1964_SU0211.PKZ.130 (135)</ta>
            <ta e="T605" id="Seg_5960" s="T601">PKZ_1964_SU0211.PKZ.131 (136)</ta>
            <ta e="T612" id="Seg_5961" s="T605">PKZ_1964_SU0211.PKZ.132 (137)</ta>
            <ta e="T617" id="Seg_5962" s="T612">PKZ_1964_SU0211.PKZ.133 (138)</ta>
            <ta e="T622" id="Seg_5963" s="T617">PKZ_1964_SU0211.PKZ.134 (139)</ta>
            <ta e="T623" id="Seg_5964" s="T622">PKZ_1964_SU0211.PKZ.135 (140)</ta>
            <ta e="T631" id="Seg_5965" s="T623">PKZ_1964_SU0211.PKZ.136 (141)</ta>
            <ta e="T636" id="Seg_5966" s="T631">PKZ_1964_SU0211.PKZ.137 (142)</ta>
            <ta e="T647" id="Seg_5967" s="T636">PKZ_1964_SU0211.PKZ.138 (143)</ta>
            <ta e="T648" id="Seg_5968" s="T647">PKZ_1964_SU0211.PKZ.139 (144)</ta>
            <ta e="T649" id="Seg_5969" s="T648">PKZ_1964_SU0211.PKZ.140 (145)</ta>
            <ta e="T659" id="Seg_5970" s="T649">PKZ_1964_SU0211.PKZ.141 (146) </ta>
            <ta e="T663" id="Seg_5971" s="T659">PKZ_1964_SU0211.PKZ.142 (148)</ta>
            <ta e="T664" id="Seg_5972" s="T663">PKZ_1964_SU0211.PKZ.143 (149)</ta>
            <ta e="T665" id="Seg_5973" s="T664">PKZ_1964_SU0211.PKZ.144 (150)</ta>
            <ta e="T670" id="Seg_5974" s="T665">PKZ_1964_SU0211.PKZ.145 (151)</ta>
            <ta e="T672" id="Seg_5975" s="T670">PKZ_1964_SU0211.PKZ.146 (152)</ta>
            <ta e="T673" id="Seg_5976" s="T672">PKZ_1964_SU0211.PKZ.147 (153)</ta>
            <ta e="T675" id="Seg_5977" s="T673">PKZ_1964_SU0211.PKZ.148 (154)</ta>
            <ta e="T676" id="Seg_5978" s="T675">PKZ_1964_SU0211.PKZ.149 (155)</ta>
            <ta e="T682" id="Seg_5979" s="T676">PKZ_1964_SU0211.PKZ.150 (156)</ta>
            <ta e="T685" id="Seg_5980" s="T682">PKZ_1964_SU0211.PKZ.151 (157)</ta>
            <ta e="T691" id="Seg_5981" s="T685">PKZ_1964_SU0211.PKZ.152 (158)</ta>
            <ta e="T693" id="Seg_5982" s="T691">PKZ_1964_SU0211.PKZ.153 (159)</ta>
            <ta e="T701" id="Seg_5983" s="T693">PKZ_1964_SU0211.PKZ.154 (160)</ta>
            <ta e="T712" id="Seg_5984" s="T701">PKZ_1964_SU0211.PKZ.155 (161)</ta>
            <ta e="T715" id="Seg_5985" s="T712">PKZ_1964_SU0211.PKZ.156 (163)</ta>
            <ta e="T722" id="Seg_5986" s="T715">PKZ_1964_SU0211.PKZ.157 (164)</ta>
            <ta e="T730" id="Seg_5987" s="T722">PKZ_1964_SU0211.PKZ.158 (165)</ta>
            <ta e="T731" id="Seg_5988" s="T730">PKZ_1964_SU0211.PKZ.159 (166)</ta>
            <ta e="T734" id="Seg_5989" s="T731">PKZ_1964_SU0211.PKZ.160 (167)</ta>
            <ta e="T746" id="Seg_5990" s="T734">PKZ_1964_SU0211.PKZ.161 (168)</ta>
            <ta e="T747" id="Seg_5991" s="T746">PKZ_1964_SU0211.PKZ.162 (169)</ta>
            <ta e="T750" id="Seg_5992" s="T747">PKZ_1964_SU0211.PKZ.163 (170)</ta>
            <ta e="T753" id="Seg_5993" s="T750">PKZ_1964_SU0211.PKZ.164 (171)</ta>
            <ta e="T756" id="Seg_5994" s="T753">PKZ_1964_SU0211.PKZ.165 (172)</ta>
            <ta e="T759" id="Seg_5995" s="T756">PKZ_1964_SU0211.PKZ.166 (173)</ta>
            <ta e="T764" id="Seg_5996" s="T759">PKZ_1964_SU0211.PKZ.167 (174)</ta>
            <ta e="T770" id="Seg_5997" s="T764">PKZ_1964_SU0211.PKZ.168 (175)</ta>
            <ta e="T771" id="Seg_5998" s="T770">PKZ_1964_SU0211.PKZ.169 (176)</ta>
            <ta e="T778" id="Seg_5999" s="T771">PKZ_1964_SU0211.PKZ.170 (177)</ta>
            <ta e="T780" id="Seg_6000" s="T778">PKZ_1964_SU0211.PKZ.171 (178)</ta>
            <ta e="T782" id="Seg_6001" s="T780">PKZ_1964_SU0211.PKZ.172 (179)</ta>
            <ta e="T784" id="Seg_6002" s="T782">PKZ_1964_SU0211.PKZ.173 (180)</ta>
            <ta e="T785" id="Seg_6003" s="T784">PKZ_1964_SU0211.PKZ.174 (181)</ta>
            <ta e="T788" id="Seg_6004" s="T785">PKZ_1964_SU0211.PKZ.175 (182)</ta>
            <ta e="T790" id="Seg_6005" s="T788">PKZ_1964_SU0211.PKZ.176 (183)</ta>
            <ta e="T796" id="Seg_6006" s="T790">PKZ_1964_SU0211.PKZ.177 (184)</ta>
            <ta e="T797" id="Seg_6007" s="T796">PKZ_1964_SU0211.PKZ.178 (185)</ta>
            <ta e="T805" id="Seg_6008" s="T797">PKZ_1964_SU0211.PKZ.179 (186)</ta>
            <ta e="T808" id="Seg_6009" s="T805">PKZ_1964_SU0211.PKZ.180 (187)</ta>
            <ta e="T810" id="Seg_6010" s="T808">PKZ_1964_SU0211.PKZ.181 (188)</ta>
            <ta e="T815" id="Seg_6011" s="T810">PKZ_1964_SU0211.PKZ.182 (189)</ta>
            <ta e="T821" id="Seg_6012" s="T815">PKZ_1964_SU0211.PKZ.183 (190)</ta>
            <ta e="T822" id="Seg_6013" s="T821">PKZ_1964_SU0211.PKZ.184 (191)</ta>
            <ta e="T829" id="Seg_6014" s="T822">PKZ_1964_SU0211.PKZ.185 (192)</ta>
            <ta e="T830" id="Seg_6015" s="T829">PKZ_1964_SU0211.PKZ.186 (193)</ta>
            <ta e="T835" id="Seg_6016" s="T830">PKZ_1964_SU0211.PKZ.187 (194)</ta>
            <ta e="T840" id="Seg_6017" s="T835">PKZ_1964_SU0211.PKZ.188 (195)</ta>
            <ta e="T841" id="Seg_6018" s="T840">PKZ_1964_SU0211.PKZ.189 (196)</ta>
            <ta e="T852" id="Seg_6019" s="T841">PKZ_1964_SU0211.PKZ.190 (197)</ta>
            <ta e="T855" id="Seg_6020" s="T852">PKZ_1964_SU0211.PKZ.191 (198)</ta>
            <ta e="T857" id="Seg_6021" s="T855">PKZ_1964_SU0211.PKZ.192 (199)</ta>
            <ta e="T858" id="Seg_6022" s="T857">PKZ_1964_SU0211.PKZ.193 (200)</ta>
            <ta e="T863" id="Seg_6023" s="T858">PKZ_1964_SU0211.PKZ.194 (201)</ta>
            <ta e="T864" id="Seg_6024" s="T863">PKZ_1964_SU0211.PKZ.195 (202)</ta>
            <ta e="T876" id="Seg_6025" s="T864">PKZ_1964_SU0211.PKZ.196 (203)</ta>
            <ta e="T879" id="Seg_6026" s="T876">PKZ_1964_SU0211.PKZ.197 (204)</ta>
            <ta e="T884" id="Seg_6027" s="T879">PKZ_1964_SU0211.PKZ.198 (205)</ta>
            <ta e="T885" id="Seg_6028" s="T884">PKZ_1964_SU0211.PKZ.199 (206)</ta>
            <ta e="T886" id="Seg_6029" s="T885">PKZ_1964_SU0211.PKZ.200 (207)</ta>
            <ta e="T891" id="Seg_6030" s="T886">PKZ_1964_SU0211.PKZ.201 (208)</ta>
            <ta e="T893" id="Seg_6031" s="T891">PKZ_1964_SU0211.PKZ.202 (209)</ta>
            <ta e="T903" id="Seg_6032" s="T893">PKZ_1964_SU0211.PKZ.203 (210)</ta>
            <ta e="T904" id="Seg_6033" s="T903">PKZ_1964_SU0211.PKZ.204 (211)</ta>
            <ta e="T910" id="Seg_6034" s="T904">PKZ_1964_SU0211.PKZ.205 (212)</ta>
            <ta e="T911" id="Seg_6035" s="T910">PKZ_1964_SU0211.PKZ.206 (213)</ta>
            <ta e="T912" id="Seg_6036" s="T911">PKZ_1964_SU0211.PKZ.207 (214)</ta>
            <ta e="T920" id="Seg_6037" s="T912">PKZ_1964_SU0211.PKZ.208 (215)</ta>
            <ta e="T923" id="Seg_6038" s="T920">PKZ_1964_SU0211.PKZ.209 (216)</ta>
            <ta e="T930" id="Seg_6039" s="T923">PKZ_1964_SU0211.PKZ.210 (217)</ta>
            <ta e="T931" id="Seg_6040" s="T930">PKZ_1964_SU0211.PKZ.211 (218)</ta>
            <ta e="T932" id="Seg_6041" s="T931">PKZ_1964_SU0211.PKZ.212 (219)</ta>
            <ta e="T937" id="Seg_6042" s="T932">PKZ_1964_SU0211.PKZ.213 (220)</ta>
            <ta e="T941" id="Seg_6043" s="T937">PKZ_1964_SU0211.PKZ.214 (221)</ta>
            <ta e="T944" id="Seg_6044" s="T941">PKZ_1964_SU0211.PKZ.215 (222)</ta>
            <ta e="T949" id="Seg_6045" s="T944">PKZ_1964_SU0211.PKZ.216 (223)</ta>
            <ta e="T952" id="Seg_6046" s="T949">PKZ_1964_SU0211.PKZ.217 (224)</ta>
            <ta e="T960" id="Seg_6047" s="T952">PKZ_1964_SU0211.PKZ.218 (225)</ta>
            <ta e="T961" id="Seg_6048" s="T960">PKZ_1964_SU0211.PKZ.219 (226)</ta>
            <ta e="T962" id="Seg_6049" s="T961">PKZ_1964_SU0211.PKZ.220 (227)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T8" id="Seg_6050" s="T4">Măn teinen sumuranə mĭmbiem. </ta>
            <ta e="T11" id="Seg_6051" s="T8">Dĭn nüke amnolaʔbə. </ta>
            <ta e="T14" id="Seg_6052" s="T11">"Amaʔ, amoraʔ kăpusta!" </ta>
            <ta e="T17" id="Seg_6053" s="T14">"Oj, ugandə namzəga. </ta>
            <ta e="T19" id="Seg_6054" s="T17">Em amaʔ". </ta>
            <ta e="T25" id="Seg_6055" s="T19">Dĭgəttə koʔbdo kambi tüʔsittə, men (dĭnziʔ). </ta>
            <ta e="T27" id="Seg_6056" s="T25">Măn (m-) (…)</ta>
            <ta e="T40" id="Seg_6057" s="T27">Dĭgəttə dĭ šobi, a măn dĭʔnə dʼăbaktərliam: "Tăn mĭmbiel tüʔsittə, i men tănziʔ". </ta>
            <ta e="T45" id="Seg_6058" s="T40">Dĭ bar kakənarluʔpi, ugaːndə tăŋ. </ta>
            <ta e="T51" id="Seg_6059" s="T45">(Kĭškəʔi) bar (üzə-) üjüʔi, ĭzemneʔbəʔjə ugaːndə. </ta>
            <ta e="T52" id="Seg_6060" s="T51">((BRK)). </ta>
            <ta e="T60" id="Seg_6061" s="T52">Iʔgö pʼe kalla dʼürbiʔi, il bar ĭmbidə ej tĭmneʔi. </ta>
            <ta e="T68" id="Seg_6062" s="T60">Dĭzen bar (i-) iʔgö inezeŋdə i tüžöjəʔi iʔgö. </ta>
            <ta e="T77" id="Seg_6063" s="T68">I ular iʔgö, kurizəʔi iʔgö, uja (amnaʔbəʔjə) iʔgö bar. </ta>
            <ta e="T80" id="Seg_6064" s="T77">Süt bar iʔgö. </ta>
            <ta e="T81" id="Seg_6065" s="T80">((BRK)). </ta>
            <ta e="T84" id="Seg_6066" s="T81">Nuzaŋ bar amnolaʔpiʔi. </ta>
            <ta e="T95" id="Seg_6067" s="T84">Kudajdə abiʔi (pa-) paziʔ, dĭgəttə (pi-) pigəʔ abiʔi, dĭgəttə sazənzəbi abiʔi. </ta>
            <ta e="T96" id="Seg_6068" s="T95">((BRK)). </ta>
            <ta e="T102" id="Seg_6069" s="T96">Bazaj kudaj abiʔi i nuldəbiʔi bar. </ta>
            <ta e="T103" id="Seg_6070" s="T102">((BRK)). </ta>
            <ta e="T109" id="Seg_6071" s="T103">Dĭzeŋdə anʼi bar svečkaʔi nuldəbiʔi, nendəbiʔi. </ta>
            <ta e="T110" id="Seg_6072" s="T109">((BRK)). </ta>
            <ta e="T114" id="Seg_6073" s="T110">Nuzaŋ bar mĭmbiʔi dʼijegən. </ta>
            <ta e="T122" id="Seg_6074" s="T114">I dĭgəttə šišəge ibi dak šoləʔjə döbər bar. </ta>
            <ta e="T132" id="Seg_6075" s="T122">Dĭn stara stoibe amnobiʔi, (dĭgəttə) kubiʔi: Dön bü ej kănzəlia. </ta>
            <ta e="T143" id="Seg_6076" s="T132">Dĭgəttə dön (nub - nulə-) (nuldlaʔbəjəʔ) bar maʔsiʔ i dön amnolaʔpiʔi. </ta>
            <ta e="T146" id="Seg_6077" s="T143">Dĭzeŋ bar mĭmbiʔi. </ta>
            <ta e="T153" id="Seg_6078" s="T146">(Aktʼ - aktʼ-) Aktʼit bar (tădam) ibi. </ta>
            <ta e="T160" id="Seg_6079" s="T153">Onʼiʔ kandəga, dăre bar, kandəgaʔi kak nabəʔi. </ta>
            <ta e="T162" id="Seg_6080" s="T160">Dʼijenə kambiʔi. </ta>
            <ta e="T168" id="Seg_6081" s="T162">Onʼiʔ ej kallia, a iʔgö kaləʔi. </ta>
            <ta e="T169" id="Seg_6082" s="T168">((BRK)). </ta>
            <ta e="T173" id="Seg_6083" s="T169">((DMG)) Dĭzeŋ bar dʼijegəʔ šonəgaʔi. </ta>
            <ta e="T179" id="Seg_6084" s="T173">Onʼiʔ, šide, teʔtə, nagur, sumna, muktuʔ. </ta>
            <ta e="T186" id="Seg_6085" s="T179">Onʼiʔ onʼiʔziʔ (k - šolə -) šonaʔbəjəʔ. </ta>
            <ta e="T189" id="Seg_6086" s="T186">Bü bar kănzəlaʔpi. </ta>
            <ta e="T196" id="Seg_6087" s="T189">Üjüziʔ (nuldlial=) nulal dak, dibər ej saʔməlial. </ta>
            <ta e="T202" id="Seg_6088" s="T196">Măn üdʼüge ibim, măn iam togonorbi. </ta>
            <ta e="T207" id="Seg_6089" s="T202">Kubaʔi (ai-) abi, pargaʔi šobi. </ta>
            <ta e="T217" id="Seg_6090" s="T207">((DMG)) Jamaʔi šöʔpi, (üžü - üzər-) üžü (s-) šöʔpi i … </ta>
            <ta e="T220" id="Seg_6091" s="T217">Abam bar šerbi. </ta>
            <ta e="T228" id="Seg_6092" s="T220">Tüj măn (šalbə) naga, ej tĭmnem dĭ giraːmbi. </ta>
            <ta e="T229" id="Seg_6093" s="T228">((BRK)). </ta>
            <ta e="T235" id="Seg_6094" s="T229">Ugaːndə (pim-) pimniem, bar măna sădărlaʔbə. </ta>
            <ta e="T236" id="Seg_6095" s="T235">((BRK)). </ta>
            <ta e="T243" id="Seg_6096" s="T236">Teinen (di-) dʼijegən šaːbilaʔ, ugaːndə šišəge ibi. </ta>
            <ta e="T250" id="Seg_6097" s="T243">Da miʔ ugaːndə kănnaːmbibaʔ, bar tăŋ kănnaːmbibaʔ. </ta>
            <ta e="T251" id="Seg_6098" s="T250">((BRK)). </ta>
            <ta e="T256" id="Seg_6099" s="T251">Ugaːndə šišəge, bü bar kănnaːmbi. </ta>
            <ta e="T260" id="Seg_6100" s="T256">Măn (üjüzibə) (nul-) nubiam. </ta>
            <ta e="T263" id="Seg_6101" s="T260">Dĭ ej băldəbi. </ta>
            <ta e="T264" id="Seg_6102" s="T263">((BRK)). </ta>
            <ta e="T272" id="Seg_6103" s="T264">Kamən kunolzittə iʔbələl, suraraʔ: ((DMG)) "Öʔləl măna kunolzittə dön?" </ta>
            <ta e="T278" id="Seg_6104" s="T272">((DMG)) Baštap dĭn nubiʔi Ilʼbinʼən toːndə. </ta>
            <ta e="T282" id="Seg_6105" s="T278">Dĭgəttə dö bü kubiʔi. </ta>
            <ta e="T284" id="Seg_6106" s="T282">(Ej m-). </ta>
            <ta e="T291" id="Seg_6107" s="T284">Ej kănzəlia, (d-) dĭzeŋ dön maʔi nuldəbiʔi. </ta>
            <ta e="T298" id="Seg_6108" s="T291">I döbər amnosʼtə šobiʔi kamen šišəge molaːmbi. </ta>
            <ta e="T299" id="Seg_6109" s="T298">((BRK)). </ta>
            <ta e="T304" id="Seg_6110" s="T299">Dĭzeŋ bar šobiʔi, Ilʼbinʼdə amnolaʔbəʔi. </ta>
            <ta e="T312" id="Seg_6111" s="T304">Dĭgəttə döbər šobiʔi, bü kubiʔi, dĭ ej kănnia. </ta>
            <ta e="T315" id="Seg_6112" s="T312">Dön maʔzandə nuldəbiʔi. </ta>
            <ta e="T322" id="Seg_6113" s="T315">Döbər (šo-) šonəgaʔi amnozittə, kamen šišəge molaːlləj. </ta>
            <ta e="T329" id="Seg_6114" s="T322">Dĭzeŋ ej molaːmbi, dĭgəttə kandəgaʔi dʼijenə, vezʼdʼe. </ta>
            <ta e="T332" id="Seg_6115" s="T329">Bar dʼügən mĭmbiʔi. </ta>
            <ta e="T337" id="Seg_6116" s="T332">Dʼelamdə kambiʔi, dĭn bü iʔgö. </ta>
            <ta e="T338" id="Seg_6117" s="T337">((BRK)). </ta>
            <ta e="T345" id="Seg_6118" s="T338">Onʼiʔ kuza dön, a onʼiʔ kuza dĭn. </ta>
            <ta e="T346" id="Seg_6119" s="T345">((BRK)). </ta>
            <ta e="T349" id="Seg_6120" s="T346">Kanaʔ dibər, döbər. </ta>
            <ta e="T356" id="Seg_6121" s="T349">Dibər em kanaʔ i döbər em kanaʔ. </ta>
            <ta e="T357" id="Seg_6122" s="T356">((BRK)). </ta>
            <ta e="T360" id="Seg_6123" s="T357">Kăde dăre moləj? </ta>
            <ta e="T363" id="Seg_6124" s="T360">Ej kalial, nʼim. </ta>
            <ta e="T369" id="Seg_6125" s="T363">Dĭ kuza ugandə jakšə (amo-) amnolaʔbə. </ta>
            <ta e="T373" id="Seg_6126" s="T369">Dĭn bar ĭmbi ige. </ta>
            <ta e="T374" id="Seg_6127" s="T373">((BRK)). </ta>
            <ta e="T380" id="Seg_6128" s="T374">Dʼirəgəʔi bar šobiʔi măn šĭkəziʔ dʼăbaktərzittə. </ta>
            <ta e="T381" id="Seg_6129" s="T380">((BRK)). </ta>
            <ta e="T386" id="Seg_6130" s="T381">((DMG)) Dĭzeŋ măn (š-) šĭkəm tüšəleʔbəʔjə. </ta>
            <ta e="T387" id="Seg_6131" s="T386">((BRK)). </ta>
            <ta e="T392" id="Seg_6132" s="T387">Suraraʔ, a măn nörbəlem tănan. </ta>
            <ta e="T396" id="Seg_6133" s="T392">Tăn ĭmbi (dĭgə) ibiel. </ta>
            <ta e="T397" id="Seg_6134" s="T396">((BRK)). </ta>
            <ta e="T400" id="Seg_6135" s="T397">На кого говорить? </ta>
            <ta e="T405" id="Seg_6136" s="T400">Tăn ugaːndə numo (šĭ-) šĭkəl. </ta>
            <ta e="T409" id="Seg_6137" s="T405">A măn üdʼüge šĭkəm. </ta>
            <ta e="T410" id="Seg_6138" s="T409">((BRK)). </ta>
            <ta e="T416" id="Seg_6139" s="T410">Miʔ bar (šo-) šobibaʔ (oktʼ-) aktʼinə. </ta>
            <ta e="T420" id="Seg_6140" s="T416">Kuza bar šonəga miʔnʼibeʔ. </ta>
            <ta e="T428" id="Seg_6141" s="T420">Suraraʔ aʔtʼi, gibər dĭ kandəga, a to miʔ … </ta>
            <ta e="T429" id="Seg_6142" s="T428">((BRK)). </ta>
            <ta e="T435" id="Seg_6143" s="T429">Miʔ bar ej dĭbər možet kambibaʔ. </ta>
            <ta e="T436" id="Seg_6144" s="T435">((BRK)). </ta>
            <ta e="T444" id="Seg_6145" s="T436">"Tăn bar ej dʼăbaktərial, a măn (dʼăbaktəriam) tănziʔ". </ta>
            <ta e="T448" id="Seg_6146" s="T444">"A măn ej tĭmnem. </ta>
            <ta e="T454" id="Seg_6147" s="T448">Măn tĭmnem a dʼăbaktərzittə ej moliam". </ta>
            <ta e="T460" id="Seg_6148" s="T454">"Nada tăn šĭkəl sajnʼeʔsittə i baruʔsittə". </ta>
            <ta e="T461" id="Seg_6149" s="T460">((BRK)). </ta>
            <ta e="T467" id="Seg_6150" s="T461">Dĭzeŋ tüšəlbiʔi, tüj tože nuzaŋ moləʔjə. </ta>
            <ta e="T468" id="Seg_6151" s="T467">((BRK)). </ta>
            <ta e="T470" id="Seg_6152" s="T468">(Ugaːn-) Ugaːndə. </ta>
            <ta e="T473" id="Seg_6153" s="T470">Urgo beržə bar. </ta>
            <ta e="T476" id="Seg_6154" s="T473">Tăŋ bar kandəga. </ta>
            <ta e="T484" id="Seg_6155" s="T476">Sĭre bar kăndlaʔbə, i dʼü kăndlaʔbə, bar … </ta>
            <ta e="T492" id="Seg_6156" s="T484">((DMG)) Sĭre i dʼü, (bar m-) bar (bĭlgarluʔpi). </ta>
            <ta e="T501" id="Seg_6157" s="T492">Tăn kudaj, kudaj, iʔ maʔtə măna, iʔ maʔtə măna. </ta>
            <ta e="T507" id="Seg_6158" s="T501">Iʔ maʔtə măna, iʔ barəʔtə măna. </ta>
            <ta e="T518" id="Seg_6159" s="T507">Tăn it măna sĭjbə sagəšsəbi, tăn deʔ măna sĭjbə sagəš iʔgö. </ta>
            <ta e="T522" id="Seg_6160" s="T518">Tüšəldə măna maktanərzittə tănzi. </ta>
            <ta e="T528" id="Seg_6161" s="T522">Tüšəldə măna, tăn (aʔtʼit-) aktʼitəm mĭnzittə. </ta>
            <ta e="T529" id="Seg_6162" s="T528">((BRK)). </ta>
            <ta e="T533" id="Seg_6163" s="T529">Tăn (aksin-) (aktʼiziʔi) kanzittə. </ta>
            <ta e="T536" id="Seg_6164" s="T533">((BRK)) Погоди … </ta>
            <ta e="T537" id="Seg_6165" s="T536">((BRK)). </ta>
            <ta e="T541" id="Seg_6166" s="T537">Vanʼa, kanaʔ kazak ianəl. </ta>
            <ta e="T544" id="Seg_6167" s="T541">Kăʔbde pʼeʔ (dĭʔən). </ta>
            <ta e="T552" id="Seg_6168" s="T544">Ularən kăʔbde, ularəʔi tordə (bod-) băʔsittə miʔnʼibeʔ kereʔ. </ta>
            <ta e="T553" id="Seg_6169" s="T552">((BRK)). </ta>
            <ta e="T555" id="Seg_6170" s="T553">Jakše pʼet. </ta>
            <ta e="T556" id="Seg_6171" s="T555">((BRK)). </ta>
            <ta e="T564" id="Seg_6172" s="T556">Măn taldʼen pʼeštə (še- səbia- š- šabiam-) sʼabiam. </ta>
            <ta e="T566" id="Seg_6173" s="T564">Ej amzittə. </ta>
            <ta e="T570" id="Seg_6174" s="T566">(Va-) Vanʼka abatsi šobiʔi. </ta>
            <ta e="T577" id="Seg_6175" s="T570">Măndə: "Urgaja, (kutʼida) girgit kola bar urgo". </ta>
            <ta e="T582" id="Seg_6176" s="T577">A măn mămbiam:" Kumən dʼaʔpilaʔ?" </ta>
            <ta e="T593" id="Seg_6177" s="T582">Di măndə:" Oʔb šide nagur teʔtə sumna muktuʔ (šejʔp-) sejʔpü dʼaʔpibaʔ". </ta>
            <ta e="T594" id="Seg_6178" s="T593">((BRK)). </ta>
            <ta e="T601" id="Seg_6179" s="T594">Fašistəʔi miʔ il üge (b-) bar dʼabərolaʔpiʔi. </ta>
            <ta e="T605" id="Seg_6180" s="T601">Šidegöʔ maʔnən amnolaʔbi bar. </ta>
            <ta e="T612" id="Seg_6181" s="T605">Griška mĭmbi, kola dʼaʔpi (da) maʔndə deʔpi. </ta>
            <ta e="T617" id="Seg_6182" s="T612">Nabəʔi oʔb šide nagur detləj. </ta>
            <ta e="T622" id="Seg_6183" s="T617">A măn (ul-) ular (surdəbiam). </ta>
            <ta e="T623" id="Seg_6184" s="T622">((BRK)). </ta>
            <ta e="T631" id="Seg_6185" s="T623">Măn ugandə (kălxozəndə) tăŋ togonorbiam, iʔgö aš püdəbiem. </ta>
            <ta e="T636" id="Seg_6186" s="T631">Bʼeʔ šide püdəbiem, iššo šide. </ta>
            <ta e="T647" id="Seg_6187" s="T636">Dĭgəttə (onʼiʔ=) onʼiʔ dʼala (n-) nagur bʼeʔ püdəbiem i teʔtə iššo. </ta>
            <ta e="T648" id="Seg_6188" s="T647">((BRK)). </ta>
            <ta e="T649" id="Seg_6189" s="T648">Sotkaʔi. </ta>
            <ta e="T659" id="Seg_6190" s="T649">Măn tugandə surarlaʔbə: "Nada măna nʼilgösʼtə kanzittə šiʔnʼileʔ, kăde dʼăbaktərlialaʔ". </ta>
            <ta e="T663" id="Seg_6191" s="T659">"Šoʔ kărəldʼan, dĭzeŋ šoləʔjə. </ta>
            <ta e="T664" id="Seg_6192" s="T663">((DMG)) Nʼilgöt". </ta>
            <ta e="T665" id="Seg_6193" s="T664">((BRK)). </ta>
            <ta e="T670" id="Seg_6194" s="T665">Măn bar üdʼüge kurizəʔi uʔbdlaʔbəʔjə. </ta>
            <ta e="T672" id="Seg_6195" s="T670">Bar sʼujolaʔbəʔjə. </ta>
            <ta e="T673" id="Seg_6196" s="T672">((BRK)). </ta>
            <ta e="T675" id="Seg_6197" s="T673">Nʼeʔtə udal. </ta>
            <ta e="T676" id="Seg_6198" s="T675">((BRK)). </ta>
            <ta e="T682" id="Seg_6199" s="T676">Taldʼen nezeŋ măna šobiʔi süt kürzittə. </ta>
            <ta e="T685" id="Seg_6200" s="T682">Iʔgö ibiʔi dĭn. </ta>
            <ta e="T691" id="Seg_6201" s="T685">A (pretsed-) predsedatelʼən net amnobi dön. </ta>
            <ta e="T693" id="Seg_6202" s="T691">Nʼit šobi. </ta>
            <ta e="T701" id="Seg_6203" s="T693">(Măn-) Dĭʔnə măndə: kanžəbəj maʔnʼibeʔ, miʔ Krasnojarskəʔi kalləbaʔ. </ta>
            <ta e="T712" id="Seg_6204" s="T701">A măn mămbiam dĭʔnə: "Igeʔ ianəl ĭmbi-nʼibudʼ kuvas, plat (ib-) igeʔ. </ta>
            <ta e="T715" id="Seg_6205" s="T712">Oldʼa it kuvas". </ta>
            <ta e="T722" id="Seg_6206" s="T715">Dĭzeŋ bar kaknarlaʔbəʔjə, što măn dăre dʼăbaktərlaʔbəm. </ta>
            <ta e="T730" id="Seg_6207" s="T722">Dĭzeŋ ej tĭmneʔi dʼăbaktərzittə, они не знают говорить. </ta>
            <ta e="T731" id="Seg_6208" s="T730">((BRK)). </ta>
            <ta e="T734" id="Seg_6209" s="T731">Teinen nüke šobi. </ta>
            <ta e="T746" id="Seg_6210" s="T734">"Nada măna toltanoʔ amzittə, kallam tüjö bostə nʼinə, puskaj amnolləj măna toltanoʔ". </ta>
            <ta e="T747" id="Seg_6211" s="T746">((BRK)). </ta>
            <ta e="T750" id="Seg_6212" s="T747">Măn ibiem (Zaozʼorkagən). </ta>
            <ta e="T753" id="Seg_6213" s="T750">(Xatʼel=) Kambiam aptʼekanə. </ta>
            <ta e="T756" id="Seg_6214" s="T753">Simandə xatʼel izittə. </ta>
            <ta e="T759" id="Seg_6215" s="T756">Štobɨ măndərzittə kuŋgeŋ. </ta>
            <ta e="T764" id="Seg_6216" s="T759">Dĭzeŋ ej mĭbiʔi, măndlaʔbəʔjə:" Kanaʔ. </ta>
            <ta e="T770" id="Seg_6217" s="T764">Sʼimal măndərdə, dĭgəttə šolal i ilil". </ta>
            <ta e="T771" id="Seg_6218" s="T770">((BRK)). </ta>
            <ta e="T778" id="Seg_6219" s="T771">Dĭbər kambiam, dĭzeŋ bar dʼabərolaʔbə," Ĭmbi dʼabərolaʔbəlaʔ? </ta>
            <ta e="T780" id="Seg_6220" s="T778">Iʔ kirgarlaʔ. </ta>
            <ta e="T782" id="Seg_6221" s="T780">Jakšəŋ agaʔ. </ta>
            <ta e="T784" id="Seg_6222" s="T782">Tura sĭbrejeʔ". </ta>
            <ta e="T785" id="Seg_6223" s="T784">((BRK)). </ta>
            <ta e="T788" id="Seg_6224" s="T785">Onʼiʔtə mămbiam:" Kanaʔ. </ta>
            <ta e="T790" id="Seg_6225" s="T788">Lʼondə toʔnardə. </ta>
            <ta e="T796" id="Seg_6226" s="T790">A dĭ pušaj bü tažorləj moltʼanə. </ta>
            <ta e="T797" id="Seg_6227" s="T796">((BRK)). </ta>
            <ta e="T805" id="Seg_6228" s="T797">Măn teinen erte uʔbdəbiam, iššo (š-) sumna nagobi. </ta>
            <ta e="T808" id="Seg_6229" s="T805">Dĭgəttə pʼešbə nendəbiem. </ta>
            <ta e="T810" id="Seg_6230" s="T808">Po nuldəbiam. </ta>
            <ta e="T815" id="Seg_6231" s="T810">Dĭgəttə bazoʔ iʔbəbiem, iššo kunolbiam. </ta>
            <ta e="T821" id="Seg_6232" s="T815">Iššo bieʔ mobi, dĭgəttə (kolb-) uʔbdəbiam. </ta>
            <ta e="T822" id="Seg_6233" s="T821">((BRK)). </ta>
            <ta e="T829" id="Seg_6234" s="T822">Sedem teinen togonorbiam, a taldʼen ej sedem. </ta>
            <ta e="T830" id="Seg_6235" s="T829">((BRK)). </ta>
            <ta e="T835" id="Seg_6236" s="T830">Dĭ pi ugandə urgo, sedem. </ta>
            <ta e="T840" id="Seg_6237" s="T835">A dö pa ej sedem. </ta>
            <ta e="T841" id="Seg_6238" s="T840">((BRK)). </ta>
            <ta e="T852" id="Seg_6239" s="T841">((DMG)) (Dĭ es-) Dĭ ešši bar piʔmezeŋdə bar sajnožuʔpi bar, ši molaːmbi. </ta>
            <ta e="T855" id="Seg_6240" s="T852">Nada dĭm šöʔsittə. </ta>
            <ta e="T857" id="Seg_6241" s="T855">Заплатка enzittə. </ta>
            <ta e="T858" id="Seg_6242" s="T857">((BRK)). </ta>
            <ta e="T863" id="Seg_6243" s="T858">Edəʔ măna sĭreʔpne sumna kilogram. </ta>
            <ta e="T864" id="Seg_6244" s="T863">((BRK)). </ta>
            <ta e="T876" id="Seg_6245" s="T864">Dĭ kuza ĭzembi, a tüj ej ĭzemnie, uʔbdəbi, üjüzi mĭŋge, a to iʔbobi. </ta>
            <ta e="T879" id="Seg_6246" s="T876">Ej jakšə nʼilgölaʔsittə. </ta>
            <ta e="T884" id="Seg_6247" s="T879">Šobiʔi da mămbiʔi, ej kuvas. </ta>
            <ta e="T885" id="Seg_6248" s="T884">Dʼăbaktərla. </ta>
            <ta e="T886" id="Seg_6249" s="T885">((BRK)). </ta>
            <ta e="T891" id="Seg_6250" s="T886">(Dĭ=) dĭ bar kuzam kuʔpi. </ta>
            <ta e="T893" id="Seg_6251" s="T891">Dĭm amnolbiʔi. </ta>
            <ta e="T903" id="Seg_6252" s="T893">(Na-) Nagur (kö) amnolaʔpi i tuj maːʔndə šobi i mĭlleʔbə. </ta>
            <ta e="T904" id="Seg_6253" s="T903">((BRK)). </ta>
            <ta e="T910" id="Seg_6254" s="T904">Bar takšeʔi (bădlu- b- bădlu-) băldluʔpiʔi. </ta>
            <ta e="T911" id="Seg_6255" s="T910">((BRK)). </ta>
            <ta e="T912" id="Seg_6256" s="T911">((BRK)). </ta>
            <ta e="T920" id="Seg_6257" s="T912">Măn (ul- ul-) ulum bar piziʔ toʔnarluʔpiʔi bar. </ta>
            <ta e="T923" id="Seg_6258" s="T920">Bar kem mʼaŋŋaʔbə. </ta>
            <ta e="T930" id="Seg_6259" s="T923">((DMG)) (Măna) baltuziʔ bar ulum (bă- )… </ta>
            <ta e="T931" id="Seg_6260" s="T930">((BRK)). </ta>
            <ta e="T932" id="Seg_6261" s="T931">Băldəbiʔi. </ta>
            <ta e="T937" id="Seg_6262" s="T932">((DMG)) Dʼijenə kalal keʔbde oʔbdəsʼtə. </ta>
            <ta e="T941" id="Seg_6263" s="T937">Kanžəbaʔ maʔnʼibeʔ, keʔbde naga. </ta>
            <ta e="T944" id="Seg_6264" s="T941">Tolʼko (ka-) kanzittə. </ta>
            <ta e="T949" id="Seg_6265" s="T944">Gijendə šoləʔjə, ugandə iʔgö keʔbde. </ta>
            <ta e="T952" id="Seg_6266" s="T949">Dĭgəttə bazoʔ oʔbdəlial. </ta>
            <ta e="T960" id="Seg_6267" s="T952">Dĭgəttə (moliam) kamen maʔnʼi kanzittə, üge keʔbde iʔgö. </ta>
            <ta e="T961" id="Seg_6268" s="T960">((…)).</ta>
            <ta e="T962" id="Seg_6269" s="T961">((…)).</ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_6270" s="T4">măn</ta>
            <ta e="T6" id="Seg_6271" s="T5">teinen</ta>
            <ta e="T7" id="Seg_6272" s="T6">sumura-nə</ta>
            <ta e="T8" id="Seg_6273" s="T7">mĭm-bie-m</ta>
            <ta e="T9" id="Seg_6274" s="T8">dĭn</ta>
            <ta e="T10" id="Seg_6275" s="T9">nüke</ta>
            <ta e="T11" id="Seg_6276" s="T10">amno-laʔbə</ta>
            <ta e="T12" id="Seg_6277" s="T11">am-a-ʔ</ta>
            <ta e="T13" id="Seg_6278" s="T12">amor-a-ʔ</ta>
            <ta e="T14" id="Seg_6279" s="T13">kăpusta</ta>
            <ta e="T15" id="Seg_6280" s="T14">oj</ta>
            <ta e="T16" id="Seg_6281" s="T15">ugandə</ta>
            <ta e="T17" id="Seg_6282" s="T16">namzəga</ta>
            <ta e="T18" id="Seg_6283" s="T17">e-m</ta>
            <ta e="T19" id="Seg_6284" s="T18">am-a-ʔ</ta>
            <ta e="T20" id="Seg_6285" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_6286" s="T20">koʔbdo</ta>
            <ta e="T22" id="Seg_6287" s="T21">kam-bi</ta>
            <ta e="T23" id="Seg_6288" s="T22">tüʔ-sittə</ta>
            <ta e="T24" id="Seg_6289" s="T23">men</ta>
            <ta e="T25" id="Seg_6290" s="T24">dĭn-ziʔ</ta>
            <ta e="T26" id="Seg_6291" s="T25">măn</ta>
            <ta e="T28" id="Seg_6292" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_6293" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_6294" s="T29">šo-bi</ta>
            <ta e="T31" id="Seg_6295" s="T30">a</ta>
            <ta e="T32" id="Seg_6296" s="T31">măn</ta>
            <ta e="T33" id="Seg_6297" s="T32">dĭʔ-nə</ta>
            <ta e="T34" id="Seg_6298" s="T33">dʼăbaktər-lia-m</ta>
            <ta e="T35" id="Seg_6299" s="T34">tăn</ta>
            <ta e="T36" id="Seg_6300" s="T35">mĭm-bie-l</ta>
            <ta e="T37" id="Seg_6301" s="T36">tüʔ-sittə</ta>
            <ta e="T38" id="Seg_6302" s="T37">i</ta>
            <ta e="T39" id="Seg_6303" s="T38">men</ta>
            <ta e="T40" id="Seg_6304" s="T39">tăn-ziʔ</ta>
            <ta e="T41" id="Seg_6305" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_6306" s="T41">bar</ta>
            <ta e="T43" id="Seg_6307" s="T42">kakənar-luʔ-pi</ta>
            <ta e="T44" id="Seg_6308" s="T43">ugaːndə</ta>
            <ta e="T45" id="Seg_6309" s="T44">tăŋ</ta>
            <ta e="T46" id="Seg_6310" s="T45">kĭškə-ʔi</ta>
            <ta e="T47" id="Seg_6311" s="T46">bar</ta>
            <ta e="T49" id="Seg_6312" s="T48">üjü-ʔi</ta>
            <ta e="T50" id="Seg_6313" s="T49">ĭzem-neʔbə-ʔjə</ta>
            <ta e="T51" id="Seg_6314" s="T50">ugaːndə</ta>
            <ta e="T53" id="Seg_6315" s="T52">iʔgö</ta>
            <ta e="T54" id="Seg_6316" s="T53">pʼe</ta>
            <ta e="T964" id="Seg_6317" s="T54">kal-la</ta>
            <ta e="T55" id="Seg_6318" s="T964">dʼür-bi-ʔi</ta>
            <ta e="T56" id="Seg_6319" s="T55">il</ta>
            <ta e="T57" id="Seg_6320" s="T56">bar</ta>
            <ta e="T58" id="Seg_6321" s="T57">ĭmbi=də</ta>
            <ta e="T59" id="Seg_6322" s="T58">ej</ta>
            <ta e="T60" id="Seg_6323" s="T59">tĭmne-ʔi</ta>
            <ta e="T61" id="Seg_6324" s="T60">dĭ-zen</ta>
            <ta e="T62" id="Seg_6325" s="T61">bar</ta>
            <ta e="T64" id="Seg_6326" s="T63">iʔgö</ta>
            <ta e="T65" id="Seg_6327" s="T64">ine-zeŋ-də</ta>
            <ta e="T66" id="Seg_6328" s="T65">i</ta>
            <ta e="T67" id="Seg_6329" s="T66">tüžöj-əʔi</ta>
            <ta e="T68" id="Seg_6330" s="T67">iʔgö</ta>
            <ta e="T69" id="Seg_6331" s="T68">i</ta>
            <ta e="T70" id="Seg_6332" s="T69">ular</ta>
            <ta e="T71" id="Seg_6333" s="T70">iʔgö</ta>
            <ta e="T72" id="Seg_6334" s="T71">kurizə-ʔi</ta>
            <ta e="T73" id="Seg_6335" s="T72">iʔgö</ta>
            <ta e="T74" id="Seg_6336" s="T73">uja</ta>
            <ta e="T75" id="Seg_6337" s="T74">am-naʔbə-ʔjə</ta>
            <ta e="T76" id="Seg_6338" s="T75">iʔgö</ta>
            <ta e="T77" id="Seg_6339" s="T76">bar</ta>
            <ta e="T78" id="Seg_6340" s="T77">süt</ta>
            <ta e="T79" id="Seg_6341" s="T78">bar</ta>
            <ta e="T80" id="Seg_6342" s="T79">iʔgö</ta>
            <ta e="T82" id="Seg_6343" s="T81">nu-zaŋ</ta>
            <ta e="T83" id="Seg_6344" s="T82">bar</ta>
            <ta e="T84" id="Seg_6345" s="T83">amno-laʔpi-ʔi</ta>
            <ta e="T85" id="Seg_6346" s="T84">kudaj-də</ta>
            <ta e="T86" id="Seg_6347" s="T85">a-bi-ʔi</ta>
            <ta e="T88" id="Seg_6348" s="T87">pa-ziʔ</ta>
            <ta e="T89" id="Seg_6349" s="T88">dĭgəttə</ta>
            <ta e="T91" id="Seg_6350" s="T90">pi-gəʔ</ta>
            <ta e="T92" id="Seg_6351" s="T91">a-bi-ʔi</ta>
            <ta e="T93" id="Seg_6352" s="T92">dĭgəttə</ta>
            <ta e="T94" id="Seg_6353" s="T93">sazən-zəbi</ta>
            <ta e="T95" id="Seg_6354" s="T94">a-bi-ʔi</ta>
            <ta e="T97" id="Seg_6355" s="T96">baza-j</ta>
            <ta e="T98" id="Seg_6356" s="T97">kudaj</ta>
            <ta e="T99" id="Seg_6357" s="T98">a-bi-ʔi</ta>
            <ta e="T100" id="Seg_6358" s="T99">i</ta>
            <ta e="T101" id="Seg_6359" s="T100">nuldə-bi-ʔi</ta>
            <ta e="T102" id="Seg_6360" s="T101">bar</ta>
            <ta e="T104" id="Seg_6361" s="T103">dĭ-zeŋ-də</ta>
            <ta e="T105" id="Seg_6362" s="T104">anʼi</ta>
            <ta e="T106" id="Seg_6363" s="T105">bar</ta>
            <ta e="T107" id="Seg_6364" s="T106">svečka-ʔi</ta>
            <ta e="T108" id="Seg_6365" s="T107">nuldə-bi-ʔi</ta>
            <ta e="T109" id="Seg_6366" s="T108">nendə-bi-ʔi</ta>
            <ta e="T111" id="Seg_6367" s="T110">nu-zaŋ</ta>
            <ta e="T112" id="Seg_6368" s="T111">bar</ta>
            <ta e="T113" id="Seg_6369" s="T112">mĭm-bi-ʔi</ta>
            <ta e="T114" id="Seg_6370" s="T113">dʼije-gən</ta>
            <ta e="T115" id="Seg_6371" s="T114">i</ta>
            <ta e="T116" id="Seg_6372" s="T115">dĭgəttə</ta>
            <ta e="T117" id="Seg_6373" s="T116">šišəge</ta>
            <ta e="T118" id="Seg_6374" s="T117">i-bi</ta>
            <ta e="T119" id="Seg_6375" s="T118">dak</ta>
            <ta e="T120" id="Seg_6376" s="T119">šo-lə-ʔjə</ta>
            <ta e="T121" id="Seg_6377" s="T120">döbər</ta>
            <ta e="T122" id="Seg_6378" s="T121">bar</ta>
            <ta e="T123" id="Seg_6379" s="T122">dĭn</ta>
            <ta e="T124" id="Seg_6380" s="T123">stara</ta>
            <ta e="T125" id="Seg_6381" s="T124">stoibe</ta>
            <ta e="T126" id="Seg_6382" s="T125">amno-bi-ʔi</ta>
            <ta e="T127" id="Seg_6383" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_6384" s="T127">ku-bi-ʔi</ta>
            <ta e="T129" id="Seg_6385" s="T128">dön</ta>
            <ta e="T130" id="Seg_6386" s="T129">bü</ta>
            <ta e="T131" id="Seg_6387" s="T130">ej</ta>
            <ta e="T132" id="Seg_6388" s="T131">kănzə-lia</ta>
            <ta e="T133" id="Seg_6389" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_6390" s="T133">dön</ta>
            <ta e="T138" id="Seg_6391" s="T137">nuld-laʔbə-jəʔ</ta>
            <ta e="T139" id="Seg_6392" s="T138">bar</ta>
            <ta e="T140" id="Seg_6393" s="T139">maʔ-siʔ</ta>
            <ta e="T141" id="Seg_6394" s="T140">i</ta>
            <ta e="T142" id="Seg_6395" s="T141">dön</ta>
            <ta e="T143" id="Seg_6396" s="T142">amno-laʔpi-ʔi</ta>
            <ta e="T144" id="Seg_6397" s="T143">dĭ-zeŋ</ta>
            <ta e="T145" id="Seg_6398" s="T144">bar</ta>
            <ta e="T146" id="Seg_6399" s="T145">mĭm-bi-ʔi</ta>
            <ta e="T150" id="Seg_6400" s="T149">aktʼi-t</ta>
            <ta e="T151" id="Seg_6401" s="T150">bar</ta>
            <ta e="T152" id="Seg_6402" s="T151">tădam</ta>
            <ta e="T153" id="Seg_6403" s="T152">i-bi</ta>
            <ta e="T154" id="Seg_6404" s="T153">onʼiʔ</ta>
            <ta e="T155" id="Seg_6405" s="T154">kandə-ga</ta>
            <ta e="T156" id="Seg_6406" s="T155">dăre</ta>
            <ta e="T157" id="Seg_6407" s="T156">bar</ta>
            <ta e="T158" id="Seg_6408" s="T157">kan-də-ga-ʔi</ta>
            <ta e="T159" id="Seg_6409" s="T158">kak</ta>
            <ta e="T160" id="Seg_6410" s="T159">nabə-ʔi</ta>
            <ta e="T161" id="Seg_6411" s="T160">dʼije-nə</ta>
            <ta e="T162" id="Seg_6412" s="T161">kam-bi-ʔi</ta>
            <ta e="T163" id="Seg_6413" s="T162">onʼiʔ</ta>
            <ta e="T164" id="Seg_6414" s="T163">ej</ta>
            <ta e="T165" id="Seg_6415" s="T164">kal-lia</ta>
            <ta e="T166" id="Seg_6416" s="T165">a</ta>
            <ta e="T167" id="Seg_6417" s="T166">iʔgö</ta>
            <ta e="T168" id="Seg_6418" s="T167">ka-lə-ʔi</ta>
            <ta e="T170" id="Seg_6419" s="T169">dĭ-zeŋ</ta>
            <ta e="T171" id="Seg_6420" s="T170">bar</ta>
            <ta e="T172" id="Seg_6421" s="T171">dʼije-gəʔ</ta>
            <ta e="T173" id="Seg_6422" s="T172">šonə-ga-ʔi</ta>
            <ta e="T174" id="Seg_6423" s="T173">onʼiʔ</ta>
            <ta e="T175" id="Seg_6424" s="T174">šide</ta>
            <ta e="T176" id="Seg_6425" s="T175">teʔtə</ta>
            <ta e="T177" id="Seg_6426" s="T176">nagur</ta>
            <ta e="T178" id="Seg_6427" s="T177">sumna</ta>
            <ta e="T179" id="Seg_6428" s="T178">muktuʔ</ta>
            <ta e="T180" id="Seg_6429" s="T179">onʼiʔ</ta>
            <ta e="T181" id="Seg_6430" s="T180">onʼiʔ-ziʔ</ta>
            <ta e="T186" id="Seg_6431" s="T185">šo-naʔbə-jəʔ</ta>
            <ta e="T187" id="Seg_6432" s="T186">bü</ta>
            <ta e="T188" id="Seg_6433" s="T187">bar</ta>
            <ta e="T189" id="Seg_6434" s="T188">kănzə-laʔpi</ta>
            <ta e="T190" id="Seg_6435" s="T189">üjü-ziʔ</ta>
            <ta e="T191" id="Seg_6436" s="T190">nuld-lia-l</ta>
            <ta e="T192" id="Seg_6437" s="T191">nu-la-l</ta>
            <ta e="T193" id="Seg_6438" s="T192">dak</ta>
            <ta e="T194" id="Seg_6439" s="T193">dibər</ta>
            <ta e="T195" id="Seg_6440" s="T194">ej</ta>
            <ta e="T196" id="Seg_6441" s="T195">saʔmə-lia-l</ta>
            <ta e="T197" id="Seg_6442" s="T196">măn</ta>
            <ta e="T198" id="Seg_6443" s="T197">üdʼüge</ta>
            <ta e="T199" id="Seg_6444" s="T198">i-bi-m</ta>
            <ta e="T200" id="Seg_6445" s="T199">măn</ta>
            <ta e="T201" id="Seg_6446" s="T200">ia-m</ta>
            <ta e="T202" id="Seg_6447" s="T201">togonor-bi</ta>
            <ta e="T203" id="Seg_6448" s="T202">kuba-ʔi</ta>
            <ta e="T205" id="Seg_6449" s="T204">a-bi</ta>
            <ta e="T206" id="Seg_6450" s="T205">parga-ʔi</ta>
            <ta e="T207" id="Seg_6451" s="T206">šo-bi</ta>
            <ta e="T208" id="Seg_6452" s="T207">jama-ʔi</ta>
            <ta e="T209" id="Seg_6453" s="T208">šöʔ-pi</ta>
            <ta e="T213" id="Seg_6454" s="T212">üžü</ta>
            <ta e="T215" id="Seg_6455" s="T214">šöʔ-pi</ta>
            <ta e="T217" id="Seg_6456" s="T215">i</ta>
            <ta e="T218" id="Seg_6457" s="T217">aba-m</ta>
            <ta e="T219" id="Seg_6458" s="T218">bar</ta>
            <ta e="T220" id="Seg_6459" s="T219">šer-bi</ta>
            <ta e="T221" id="Seg_6460" s="T220">tüj</ta>
            <ta e="T222" id="Seg_6461" s="T221">măn</ta>
            <ta e="T223" id="Seg_6462" s="T222">šal-bə</ta>
            <ta e="T224" id="Seg_6463" s="T223">naga</ta>
            <ta e="T225" id="Seg_6464" s="T224">ej</ta>
            <ta e="T226" id="Seg_6465" s="T225">tĭmne-m</ta>
            <ta e="T227" id="Seg_6466" s="T226">dĭ</ta>
            <ta e="T228" id="Seg_6467" s="T227">giraːm-bi</ta>
            <ta e="T230" id="Seg_6468" s="T229">ugaːndə</ta>
            <ta e="T232" id="Seg_6469" s="T231">pim-nie-m</ta>
            <ta e="T233" id="Seg_6470" s="T232">bar</ta>
            <ta e="T234" id="Seg_6471" s="T233">măna</ta>
            <ta e="T235" id="Seg_6472" s="T234">sădăr-laʔbə</ta>
            <ta e="T237" id="Seg_6473" s="T236">teinen</ta>
            <ta e="T239" id="Seg_6474" s="T238">dʼije-gən</ta>
            <ta e="T240" id="Seg_6475" s="T239">šaː-bi-laʔ</ta>
            <ta e="T241" id="Seg_6476" s="T240">ugaːndə</ta>
            <ta e="T242" id="Seg_6477" s="T241">šišəge</ta>
            <ta e="T243" id="Seg_6478" s="T242">i-bi</ta>
            <ta e="T244" id="Seg_6479" s="T243">da</ta>
            <ta e="T245" id="Seg_6480" s="T244">miʔ</ta>
            <ta e="T246" id="Seg_6481" s="T245">ugaːndə</ta>
            <ta e="T247" id="Seg_6482" s="T246">kăn-naːm-bi-baʔ</ta>
            <ta e="T248" id="Seg_6483" s="T247">bar</ta>
            <ta e="T249" id="Seg_6484" s="T248">tăŋ</ta>
            <ta e="T250" id="Seg_6485" s="T249">kăn-naːm-bi-baʔ</ta>
            <ta e="T252" id="Seg_6486" s="T251">ugaːndə</ta>
            <ta e="T253" id="Seg_6487" s="T252">šišəge</ta>
            <ta e="T254" id="Seg_6488" s="T253">bü</ta>
            <ta e="T255" id="Seg_6489" s="T254">bar</ta>
            <ta e="T256" id="Seg_6490" s="T255">kăn-naːm-bi</ta>
            <ta e="T257" id="Seg_6491" s="T256">măn</ta>
            <ta e="T258" id="Seg_6492" s="T257">üjü-zi-bə</ta>
            <ta e="T260" id="Seg_6493" s="T259">nu-bia-m</ta>
            <ta e="T261" id="Seg_6494" s="T260">dĭ</ta>
            <ta e="T262" id="Seg_6495" s="T261">ej</ta>
            <ta e="T263" id="Seg_6496" s="T262">băldə-bi</ta>
            <ta e="T265" id="Seg_6497" s="T264">kamən</ta>
            <ta e="T266" id="Seg_6498" s="T265">kunol-zittə</ta>
            <ta e="T267" id="Seg_6499" s="T266">iʔbə-lə-l</ta>
            <ta e="T268" id="Seg_6500" s="T267">surar-ʔ</ta>
            <ta e="T269" id="Seg_6501" s="T268">öʔ-lə-l</ta>
            <ta e="T270" id="Seg_6502" s="T269">măna</ta>
            <ta e="T271" id="Seg_6503" s="T270">kunol-zittə</ta>
            <ta e="T272" id="Seg_6504" s="T271">dön</ta>
            <ta e="T274" id="Seg_6505" s="T272">baštap</ta>
            <ta e="T275" id="Seg_6506" s="T274">dĭn</ta>
            <ta e="T276" id="Seg_6507" s="T275">nu-bi-ʔi</ta>
            <ta e="T277" id="Seg_6508" s="T276">Ilʼbinʼ-ə-n</ta>
            <ta e="T278" id="Seg_6509" s="T277">toː-ndə</ta>
            <ta e="T279" id="Seg_6510" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_6511" s="T279">dö</ta>
            <ta e="T281" id="Seg_6512" s="T280">bü</ta>
            <ta e="T282" id="Seg_6513" s="T281">ku-bi-ʔi</ta>
            <ta e="T283" id="Seg_6514" s="T282">ej</ta>
            <ta e="T285" id="Seg_6515" s="T284">ej</ta>
            <ta e="T286" id="Seg_6516" s="T285">kănzə-lia</ta>
            <ta e="T288" id="Seg_6517" s="T287">dĭ-zeŋ</ta>
            <ta e="T289" id="Seg_6518" s="T288">dön</ta>
            <ta e="T290" id="Seg_6519" s="T289">ma-ʔi</ta>
            <ta e="T291" id="Seg_6520" s="T290">nuldə-bi-ʔi</ta>
            <ta e="T292" id="Seg_6521" s="T291">i</ta>
            <ta e="T293" id="Seg_6522" s="T292">döbər</ta>
            <ta e="T294" id="Seg_6523" s="T293">amno-sʼtə</ta>
            <ta e="T295" id="Seg_6524" s="T294">šo-bi-ʔi</ta>
            <ta e="T296" id="Seg_6525" s="T295">kamen</ta>
            <ta e="T297" id="Seg_6526" s="T296">šišəge</ta>
            <ta e="T298" id="Seg_6527" s="T297">mo-laːm-bi</ta>
            <ta e="T300" id="Seg_6528" s="T299">dĭ-zeŋ</ta>
            <ta e="T301" id="Seg_6529" s="T300">bar</ta>
            <ta e="T302" id="Seg_6530" s="T301">šo-bi-ʔi</ta>
            <ta e="T303" id="Seg_6531" s="T302">Ilʼbinʼ-də</ta>
            <ta e="T304" id="Seg_6532" s="T303">amno-laʔbə-ʔi</ta>
            <ta e="T305" id="Seg_6533" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_6534" s="T305">döbər</ta>
            <ta e="T307" id="Seg_6535" s="T306">šo-bi-ʔi</ta>
            <ta e="T308" id="Seg_6536" s="T307">bü</ta>
            <ta e="T309" id="Seg_6537" s="T308">ku-bi-ʔi</ta>
            <ta e="T310" id="Seg_6538" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_6539" s="T310">ej</ta>
            <ta e="T312" id="Seg_6540" s="T311">kăn-nia</ta>
            <ta e="T313" id="Seg_6541" s="T312">dön</ta>
            <ta e="T314" id="Seg_6542" s="T313">maʔ-zan-də</ta>
            <ta e="T315" id="Seg_6543" s="T314">nuldə-bi-ʔi</ta>
            <ta e="T316" id="Seg_6544" s="T315">döbər</ta>
            <ta e="T318" id="Seg_6545" s="T317">šonə-ga-ʔi</ta>
            <ta e="T319" id="Seg_6546" s="T318">amno-zittə</ta>
            <ta e="T320" id="Seg_6547" s="T319">kamen</ta>
            <ta e="T321" id="Seg_6548" s="T320">šišəge</ta>
            <ta e="T322" id="Seg_6549" s="T321">mo-laːl-lə-j</ta>
            <ta e="T323" id="Seg_6550" s="T322">dĭ-zeŋ</ta>
            <ta e="T324" id="Seg_6551" s="T323">ej</ta>
            <ta e="T325" id="Seg_6552" s="T324">mo-laːm-bi</ta>
            <ta e="T326" id="Seg_6553" s="T325">dĭgəttə</ta>
            <ta e="T327" id="Seg_6554" s="T326">kan-də-ga-ʔi</ta>
            <ta e="T328" id="Seg_6555" s="T327">dʼije-nə</ta>
            <ta e="T329" id="Seg_6556" s="T328">vezʼdʼe</ta>
            <ta e="T330" id="Seg_6557" s="T329">bar</ta>
            <ta e="T331" id="Seg_6558" s="T330">dʼü-gən</ta>
            <ta e="T332" id="Seg_6559" s="T331">mĭm-bi-ʔi</ta>
            <ta e="T333" id="Seg_6560" s="T332">Dʼelam-də</ta>
            <ta e="T334" id="Seg_6561" s="T333">kam-bi-ʔi</ta>
            <ta e="T335" id="Seg_6562" s="T334">dĭn</ta>
            <ta e="T336" id="Seg_6563" s="T335">bü</ta>
            <ta e="T337" id="Seg_6564" s="T336">iʔgö</ta>
            <ta e="T339" id="Seg_6565" s="T338">onʼiʔ</ta>
            <ta e="T340" id="Seg_6566" s="T339">kuza</ta>
            <ta e="T341" id="Seg_6567" s="T340">dön</ta>
            <ta e="T342" id="Seg_6568" s="T341">a</ta>
            <ta e="T343" id="Seg_6569" s="T342">onʼiʔ</ta>
            <ta e="T344" id="Seg_6570" s="T343">kuza</ta>
            <ta e="T345" id="Seg_6571" s="T344">dĭn</ta>
            <ta e="T347" id="Seg_6572" s="T346">kan-a-ʔ</ta>
            <ta e="T348" id="Seg_6573" s="T347">dibər</ta>
            <ta e="T349" id="Seg_6574" s="T348">döbər</ta>
            <ta e="T350" id="Seg_6575" s="T349">dibər</ta>
            <ta e="T351" id="Seg_6576" s="T350">e-m</ta>
            <ta e="T352" id="Seg_6577" s="T351">kan-a-ʔ</ta>
            <ta e="T353" id="Seg_6578" s="T352">i</ta>
            <ta e="T354" id="Seg_6579" s="T353">döbər</ta>
            <ta e="T355" id="Seg_6580" s="T354">e-m</ta>
            <ta e="T356" id="Seg_6581" s="T355">kan-a-ʔ</ta>
            <ta e="T358" id="Seg_6582" s="T357">kăde</ta>
            <ta e="T359" id="Seg_6583" s="T358">dăre</ta>
            <ta e="T360" id="Seg_6584" s="T359">mo-lə-j</ta>
            <ta e="T361" id="Seg_6585" s="T360">ej</ta>
            <ta e="T362" id="Seg_6586" s="T361">ka-lia-l</ta>
            <ta e="T363" id="Seg_6587" s="T362">nʼi-m</ta>
            <ta e="T364" id="Seg_6588" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_6589" s="T364">kuza</ta>
            <ta e="T366" id="Seg_6590" s="T365">ugandə</ta>
            <ta e="T367" id="Seg_6591" s="T366">jakšə</ta>
            <ta e="T369" id="Seg_6592" s="T368">amno-laʔbə</ta>
            <ta e="T370" id="Seg_6593" s="T369">dĭn</ta>
            <ta e="T371" id="Seg_6594" s="T370">bar</ta>
            <ta e="T372" id="Seg_6595" s="T371">ĭmbi</ta>
            <ta e="T373" id="Seg_6596" s="T372">i-ge</ta>
            <ta e="T375" id="Seg_6597" s="T374">dʼirəg-əʔi</ta>
            <ta e="T376" id="Seg_6598" s="T375">bar</ta>
            <ta e="T377" id="Seg_6599" s="T376">šo-bi-ʔi</ta>
            <ta e="T378" id="Seg_6600" s="T377">măn</ta>
            <ta e="T379" id="Seg_6601" s="T378">šĭkə-ziʔ</ta>
            <ta e="T380" id="Seg_6602" s="T379">dʼăbaktər-zittə</ta>
            <ta e="T382" id="Seg_6603" s="T381">dĭ-zeŋ</ta>
            <ta e="T383" id="Seg_6604" s="T382">măn</ta>
            <ta e="T385" id="Seg_6605" s="T384">šĭkə-m</ta>
            <ta e="T386" id="Seg_6606" s="T385">tüšə-leʔbə-ʔjə</ta>
            <ta e="T388" id="Seg_6607" s="T387">surar-ʔ</ta>
            <ta e="T389" id="Seg_6608" s="T388">a</ta>
            <ta e="T390" id="Seg_6609" s="T389">măn</ta>
            <ta e="T391" id="Seg_6610" s="T390">nörbə-le-m</ta>
            <ta e="T392" id="Seg_6611" s="T391">tănan</ta>
            <ta e="T393" id="Seg_6612" s="T392">tăn</ta>
            <ta e="T394" id="Seg_6613" s="T393">ĭmbi</ta>
            <ta e="T395" id="Seg_6614" s="T394">dĭ-gə</ta>
            <ta e="T396" id="Seg_6615" s="T395">i-bie-l</ta>
            <ta e="T401" id="Seg_6616" s="T400">tăn</ta>
            <ta e="T402" id="Seg_6617" s="T401">ugaːndə</ta>
            <ta e="T403" id="Seg_6618" s="T402">numo</ta>
            <ta e="T405" id="Seg_6619" s="T404">šĭkə-l</ta>
            <ta e="T406" id="Seg_6620" s="T405">a</ta>
            <ta e="T407" id="Seg_6621" s="T406">măn</ta>
            <ta e="T408" id="Seg_6622" s="T407">üdʼüge</ta>
            <ta e="T409" id="Seg_6623" s="T408">šĭkə-m</ta>
            <ta e="T411" id="Seg_6624" s="T410">miʔ</ta>
            <ta e="T412" id="Seg_6625" s="T411">bar</ta>
            <ta e="T414" id="Seg_6626" s="T413">šo-bi-baʔ</ta>
            <ta e="T416" id="Seg_6627" s="T415">aktʼi-nə</ta>
            <ta e="T417" id="Seg_6628" s="T416">kuza</ta>
            <ta e="T418" id="Seg_6629" s="T417">bar</ta>
            <ta e="T419" id="Seg_6630" s="T418">šonə-ga</ta>
            <ta e="T420" id="Seg_6631" s="T419">miʔnʼibeʔ</ta>
            <ta e="T421" id="Seg_6632" s="T420">surar-ʔ</ta>
            <ta e="T422" id="Seg_6633" s="T421">aʔtʼi</ta>
            <ta e="T423" id="Seg_6634" s="T422">gibər</ta>
            <ta e="T424" id="Seg_6635" s="T423">dĭ</ta>
            <ta e="T425" id="Seg_6636" s="T424">kandə-ga</ta>
            <ta e="T426" id="Seg_6637" s="T425">ato</ta>
            <ta e="T428" id="Seg_6638" s="T426">miʔ</ta>
            <ta e="T430" id="Seg_6639" s="T429">miʔ</ta>
            <ta e="T431" id="Seg_6640" s="T430">bar</ta>
            <ta e="T432" id="Seg_6641" s="T431">ej</ta>
            <ta e="T433" id="Seg_6642" s="T432">dĭbər</ta>
            <ta e="T434" id="Seg_6643" s="T433">možet</ta>
            <ta e="T435" id="Seg_6644" s="T434">kam-bi-baʔ</ta>
            <ta e="T437" id="Seg_6645" s="T436">tăn</ta>
            <ta e="T438" id="Seg_6646" s="T437">bar</ta>
            <ta e="T439" id="Seg_6647" s="T438">ej</ta>
            <ta e="T440" id="Seg_6648" s="T439">dʼăbaktər-ia-l</ta>
            <ta e="T441" id="Seg_6649" s="T440">a</ta>
            <ta e="T442" id="Seg_6650" s="T441">măn</ta>
            <ta e="T443" id="Seg_6651" s="T442">dʼăbaktər-ia-m</ta>
            <ta e="T444" id="Seg_6652" s="T443">tăn-ziʔ</ta>
            <ta e="T445" id="Seg_6653" s="T444">a</ta>
            <ta e="T446" id="Seg_6654" s="T445">măn</ta>
            <ta e="T447" id="Seg_6655" s="T446">ej</ta>
            <ta e="T448" id="Seg_6656" s="T447">tĭmne-m</ta>
            <ta e="T449" id="Seg_6657" s="T448">măn</ta>
            <ta e="T450" id="Seg_6658" s="T449">tĭmne-m</ta>
            <ta e="T451" id="Seg_6659" s="T450">a</ta>
            <ta e="T452" id="Seg_6660" s="T451">dʼăbaktər-zittə</ta>
            <ta e="T453" id="Seg_6661" s="T452">ej</ta>
            <ta e="T454" id="Seg_6662" s="T453">mo-lia-m</ta>
            <ta e="T455" id="Seg_6663" s="T454">nada</ta>
            <ta e="T456" id="Seg_6664" s="T455">tăn</ta>
            <ta e="T457" id="Seg_6665" s="T456">šĭkə-l</ta>
            <ta e="T458" id="Seg_6666" s="T457">saj-nʼeʔ-sittə</ta>
            <ta e="T459" id="Seg_6667" s="T458">i</ta>
            <ta e="T460" id="Seg_6668" s="T459">baruʔ-sittə</ta>
            <ta e="T462" id="Seg_6669" s="T461">dĭ-zeŋ</ta>
            <ta e="T463" id="Seg_6670" s="T462">tüšəl-bi-ʔi</ta>
            <ta e="T464" id="Seg_6671" s="T463">tüj</ta>
            <ta e="T465" id="Seg_6672" s="T464">tože</ta>
            <ta e="T466" id="Seg_6673" s="T465">nu-zaŋ</ta>
            <ta e="T467" id="Seg_6674" s="T466">mo-lə-ʔjə</ta>
            <ta e="T470" id="Seg_6675" s="T469">ugaːndə</ta>
            <ta e="T471" id="Seg_6676" s="T470">urgo</ta>
            <ta e="T472" id="Seg_6677" s="T471">beržə</ta>
            <ta e="T473" id="Seg_6678" s="T472">bar</ta>
            <ta e="T474" id="Seg_6679" s="T473">tăŋ</ta>
            <ta e="T475" id="Seg_6680" s="T474">bar</ta>
            <ta e="T476" id="Seg_6681" s="T475">kandə-ga</ta>
            <ta e="T477" id="Seg_6682" s="T476">sĭre</ta>
            <ta e="T478" id="Seg_6683" s="T477">bar</ta>
            <ta e="T479" id="Seg_6684" s="T478">kănd-laʔbə</ta>
            <ta e="T480" id="Seg_6685" s="T479">i</ta>
            <ta e="T481" id="Seg_6686" s="T480">dʼü</ta>
            <ta e="T482" id="Seg_6687" s="T481">kănd-laʔbə</ta>
            <ta e="T484" id="Seg_6688" s="T482">bar</ta>
            <ta e="T486" id="Seg_6689" s="T484">sĭre</ta>
            <ta e="T487" id="Seg_6690" s="T486">i</ta>
            <ta e="T488" id="Seg_6691" s="T487">dʼü</ta>
            <ta e="T489" id="Seg_6692" s="T488">bar</ta>
            <ta e="T491" id="Seg_6693" s="T490">bar</ta>
            <ta e="T492" id="Seg_6694" s="T491">bĭlgar-luʔ-pi</ta>
            <ta e="T493" id="Seg_6695" s="T492">tăn</ta>
            <ta e="T494" id="Seg_6696" s="T493">kudaj</ta>
            <ta e="T495" id="Seg_6697" s="T494">kudaj</ta>
            <ta e="T496" id="Seg_6698" s="T495">i-ʔ</ta>
            <ta e="T497" id="Seg_6699" s="T496">maʔ-tə</ta>
            <ta e="T498" id="Seg_6700" s="T497">măna</ta>
            <ta e="T499" id="Seg_6701" s="T498">i-ʔ</ta>
            <ta e="T500" id="Seg_6702" s="T499">maʔ-tə</ta>
            <ta e="T501" id="Seg_6703" s="T500">măna</ta>
            <ta e="T502" id="Seg_6704" s="T501">i-ʔ</ta>
            <ta e="T503" id="Seg_6705" s="T502">maʔ-tə</ta>
            <ta e="T504" id="Seg_6706" s="T503">măna</ta>
            <ta e="T505" id="Seg_6707" s="T504">i-ʔ</ta>
            <ta e="T506" id="Seg_6708" s="T505">barəʔ-tə</ta>
            <ta e="T507" id="Seg_6709" s="T506">măna</ta>
            <ta e="T508" id="Seg_6710" s="T507">tăn</ta>
            <ta e="T509" id="Seg_6711" s="T508">i-t</ta>
            <ta e="T510" id="Seg_6712" s="T509">măna</ta>
            <ta e="T511" id="Seg_6713" s="T510">sĭj-bə</ta>
            <ta e="T512" id="Seg_6714" s="T511">sagəš-səbi</ta>
            <ta e="T513" id="Seg_6715" s="T512">tăn</ta>
            <ta e="T514" id="Seg_6716" s="T513">de-ʔ</ta>
            <ta e="T515" id="Seg_6717" s="T514">măna</ta>
            <ta e="T516" id="Seg_6718" s="T515">sĭj-bə</ta>
            <ta e="T517" id="Seg_6719" s="T516">sagəš</ta>
            <ta e="T518" id="Seg_6720" s="T517">iʔgö</ta>
            <ta e="T519" id="Seg_6721" s="T518">tüšə-l-də</ta>
            <ta e="T520" id="Seg_6722" s="T519">măna</ta>
            <ta e="T521" id="Seg_6723" s="T520">maktanər-zittə</ta>
            <ta e="T522" id="Seg_6724" s="T521">tăn-zi</ta>
            <ta e="T523" id="Seg_6725" s="T522">tüšə-l-də</ta>
            <ta e="T524" id="Seg_6726" s="T523">măna</ta>
            <ta e="T525" id="Seg_6727" s="T524">tăn</ta>
            <ta e="T527" id="Seg_6728" s="T526">aktʼitə-m</ta>
            <ta e="T528" id="Seg_6729" s="T527">mĭn-zittə</ta>
            <ta e="T530" id="Seg_6730" s="T529">tăn</ta>
            <ta e="T532" id="Seg_6731" s="T531">aktʼi-zi-ʔi</ta>
            <ta e="T533" id="Seg_6732" s="T532">kan-zittə</ta>
            <ta e="T538" id="Seg_6733" s="T537">Vanʼa</ta>
            <ta e="T539" id="Seg_6734" s="T538">kan-a-ʔ</ta>
            <ta e="T540" id="Seg_6735" s="T539">kazak</ta>
            <ta e="T541" id="Seg_6736" s="T540">ia-nə-l</ta>
            <ta e="T542" id="Seg_6737" s="T541">kăʔbde</ta>
            <ta e="T543" id="Seg_6738" s="T542">pʼe-ʔ</ta>
            <ta e="T544" id="Seg_6739" s="T543">dĭʔən</ta>
            <ta e="T545" id="Seg_6740" s="T544">ular-ə-n</ta>
            <ta e="T546" id="Seg_6741" s="T545">kăʔbde</ta>
            <ta e="T547" id="Seg_6742" s="T546">ular-əʔi</ta>
            <ta e="T548" id="Seg_6743" s="T547">tor-də</ta>
            <ta e="T550" id="Seg_6744" s="T549">băʔ-sittə</ta>
            <ta e="T551" id="Seg_6745" s="T550">miʔnʼibeʔ</ta>
            <ta e="T552" id="Seg_6746" s="T551">kereʔ</ta>
            <ta e="T554" id="Seg_6747" s="T553">jakše</ta>
            <ta e="T555" id="Seg_6748" s="T554">pʼe-t</ta>
            <ta e="T557" id="Seg_6749" s="T556">măn</ta>
            <ta e="T558" id="Seg_6750" s="T557">taldʼen</ta>
            <ta e="T559" id="Seg_6751" s="T558">pʼeš-tə</ta>
            <ta e="T564" id="Seg_6752" s="T563">sʼa-bia-m</ta>
            <ta e="T565" id="Seg_6753" s="T564">ej</ta>
            <ta e="T566" id="Seg_6754" s="T565">am-zittə</ta>
            <ta e="T568" id="Seg_6755" s="T567">Vanʼka</ta>
            <ta e="T569" id="Seg_6756" s="T568">aba-t-si</ta>
            <ta e="T570" id="Seg_6757" s="T569">šo-bi-ʔi</ta>
            <ta e="T571" id="Seg_6758" s="T570">măn-də</ta>
            <ta e="T572" id="Seg_6759" s="T571">urgaja</ta>
            <ta e="T573" id="Seg_6760" s="T572">kutʼida</ta>
            <ta e="T574" id="Seg_6761" s="T573">girgit</ta>
            <ta e="T575" id="Seg_6762" s="T574">kola</ta>
            <ta e="T576" id="Seg_6763" s="T575">bar</ta>
            <ta e="T577" id="Seg_6764" s="T576">urgo</ta>
            <ta e="T578" id="Seg_6765" s="T577">a</ta>
            <ta e="T579" id="Seg_6766" s="T578">măn</ta>
            <ta e="T580" id="Seg_6767" s="T579">măm-bia-m</ta>
            <ta e="T581" id="Seg_6768" s="T580">kumən</ta>
            <ta e="T582" id="Seg_6769" s="T581">dʼaʔ-pi-laʔ</ta>
            <ta e="T583" id="Seg_6770" s="T582">di</ta>
            <ta e="T584" id="Seg_6771" s="T583">măn-də</ta>
            <ta e="T585" id="Seg_6772" s="T584">oʔb</ta>
            <ta e="T586" id="Seg_6773" s="T585">šide</ta>
            <ta e="T587" id="Seg_6774" s="T586">nagur</ta>
            <ta e="T588" id="Seg_6775" s="T587">teʔtə</ta>
            <ta e="T589" id="Seg_6776" s="T588">sumna</ta>
            <ta e="T590" id="Seg_6777" s="T589">muktuʔ</ta>
            <ta e="T592" id="Seg_6778" s="T591">sejʔpü</ta>
            <ta e="T593" id="Seg_6779" s="T592">dʼaʔ-pi-baʔ</ta>
            <ta e="T595" id="Seg_6780" s="T594">fašist-əʔi</ta>
            <ta e="T596" id="Seg_6781" s="T595">miʔ</ta>
            <ta e="T597" id="Seg_6782" s="T596">il</ta>
            <ta e="T598" id="Seg_6783" s="T597">üge</ta>
            <ta e="T600" id="Seg_6784" s="T599">bar</ta>
            <ta e="T601" id="Seg_6785" s="T600">dʼabəro-laʔpi-ʔi</ta>
            <ta e="T602" id="Seg_6786" s="T601">šide-göʔ</ta>
            <ta e="T603" id="Seg_6787" s="T602">maʔ-nə-n</ta>
            <ta e="T604" id="Seg_6788" s="T603">amno-laʔ-bi</ta>
            <ta e="T605" id="Seg_6789" s="T604">bar</ta>
            <ta e="T606" id="Seg_6790" s="T605">Griška</ta>
            <ta e="T607" id="Seg_6791" s="T606">mĭm-bi</ta>
            <ta e="T608" id="Seg_6792" s="T607">kola</ta>
            <ta e="T609" id="Seg_6793" s="T608">dʼaʔ-pi</ta>
            <ta e="T610" id="Seg_6794" s="T609">da</ta>
            <ta e="T611" id="Seg_6795" s="T610">maʔ-ndə</ta>
            <ta e="T612" id="Seg_6796" s="T611">deʔ-pi</ta>
            <ta e="T613" id="Seg_6797" s="T612">nabə-ʔi</ta>
            <ta e="T614" id="Seg_6798" s="T613">oʔb</ta>
            <ta e="T615" id="Seg_6799" s="T614">šide</ta>
            <ta e="T616" id="Seg_6800" s="T615">nagur</ta>
            <ta e="T617" id="Seg_6801" s="T616">det-lə-j</ta>
            <ta e="T618" id="Seg_6802" s="T617">a</ta>
            <ta e="T619" id="Seg_6803" s="T618">măn</ta>
            <ta e="T621" id="Seg_6804" s="T620">ular</ta>
            <ta e="T622" id="Seg_6805" s="T621">surdə-bia-m</ta>
            <ta e="T624" id="Seg_6806" s="T623">măn</ta>
            <ta e="T625" id="Seg_6807" s="T624">ugandə</ta>
            <ta e="T626" id="Seg_6808" s="T625">kălxoz-əndə</ta>
            <ta e="T627" id="Seg_6809" s="T626">tăŋ</ta>
            <ta e="T628" id="Seg_6810" s="T627">togonor-bia-m</ta>
            <ta e="T629" id="Seg_6811" s="T628">iʔgö</ta>
            <ta e="T630" id="Seg_6812" s="T629">aš</ta>
            <ta e="T631" id="Seg_6813" s="T630">püdə-bie-m</ta>
            <ta e="T632" id="Seg_6814" s="T631">bʼeʔ</ta>
            <ta e="T633" id="Seg_6815" s="T632">šide</ta>
            <ta e="T634" id="Seg_6816" s="T633">püdə-bie-m</ta>
            <ta e="T635" id="Seg_6817" s="T634">iššo</ta>
            <ta e="T636" id="Seg_6818" s="T635">šide</ta>
            <ta e="T637" id="Seg_6819" s="T636">dĭgəttə</ta>
            <ta e="T638" id="Seg_6820" s="T637">onʼiʔ</ta>
            <ta e="T639" id="Seg_6821" s="T638">onʼiʔ</ta>
            <ta e="T640" id="Seg_6822" s="T639">dʼala</ta>
            <ta e="T642" id="Seg_6823" s="T641">nagur</ta>
            <ta e="T643" id="Seg_6824" s="T642">bʼeʔ</ta>
            <ta e="T644" id="Seg_6825" s="T643">püdə-bie-m</ta>
            <ta e="T645" id="Seg_6826" s="T644">i</ta>
            <ta e="T646" id="Seg_6827" s="T645">teʔtə</ta>
            <ta e="T647" id="Seg_6828" s="T646">iššo</ta>
            <ta e="T649" id="Seg_6829" s="T648">sotka-ʔi</ta>
            <ta e="T650" id="Seg_6830" s="T649">măn</ta>
            <ta e="T651" id="Seg_6831" s="T650">tugan-də</ta>
            <ta e="T652" id="Seg_6832" s="T651">surar-laʔbə</ta>
            <ta e="T653" id="Seg_6833" s="T652">nada</ta>
            <ta e="T654" id="Seg_6834" s="T653">măna</ta>
            <ta e="T655" id="Seg_6835" s="T654">nʼilgö-sʼtə</ta>
            <ta e="T656" id="Seg_6836" s="T655">kan-zittə</ta>
            <ta e="T657" id="Seg_6837" s="T656">šiʔnʼileʔ</ta>
            <ta e="T658" id="Seg_6838" s="T657">kăde</ta>
            <ta e="T659" id="Seg_6839" s="T658">dʼăbaktər-lia-laʔ</ta>
            <ta e="T660" id="Seg_6840" s="T659">šo-ʔ</ta>
            <ta e="T661" id="Seg_6841" s="T660">kărəldʼan</ta>
            <ta e="T662" id="Seg_6842" s="T661">dĭ-zeŋ</ta>
            <ta e="T663" id="Seg_6843" s="T662">šo-lə-ʔjə</ta>
            <ta e="T664" id="Seg_6844" s="T663">nʼilgö-t</ta>
            <ta e="T666" id="Seg_6845" s="T665">măn</ta>
            <ta e="T667" id="Seg_6846" s="T666">bar</ta>
            <ta e="T668" id="Seg_6847" s="T667">üdʼüge</ta>
            <ta e="T669" id="Seg_6848" s="T668">kurizə-ʔi</ta>
            <ta e="T670" id="Seg_6849" s="T669">uʔbd-laʔbə-ʔjə</ta>
            <ta e="T671" id="Seg_6850" s="T670">bar</ta>
            <ta e="T672" id="Seg_6851" s="T671">sʼujo-laʔbə-ʔjə</ta>
            <ta e="T674" id="Seg_6852" s="T673">nʼeʔ-tə</ta>
            <ta e="T675" id="Seg_6853" s="T674">uda-l</ta>
            <ta e="T677" id="Seg_6854" s="T676">taldʼen</ta>
            <ta e="T678" id="Seg_6855" s="T677">ne-zeŋ</ta>
            <ta e="T679" id="Seg_6856" s="T678">măna</ta>
            <ta e="T680" id="Seg_6857" s="T679">šo-bi-ʔi</ta>
            <ta e="T681" id="Seg_6858" s="T680">süt</ta>
            <ta e="T682" id="Seg_6859" s="T681">kür-zittə</ta>
            <ta e="T683" id="Seg_6860" s="T682">iʔgö</ta>
            <ta e="T684" id="Seg_6861" s="T683">i-bi-ʔi</ta>
            <ta e="T685" id="Seg_6862" s="T684">dĭn</ta>
            <ta e="T686" id="Seg_6863" s="T685">a</ta>
            <ta e="T688" id="Seg_6864" s="T687">predsedatelʼ-ə-n</ta>
            <ta e="T689" id="Seg_6865" s="T688">ne-t</ta>
            <ta e="T690" id="Seg_6866" s="T689">amno-bi</ta>
            <ta e="T691" id="Seg_6867" s="T690">dön</ta>
            <ta e="T692" id="Seg_6868" s="T691">nʼi-t</ta>
            <ta e="T693" id="Seg_6869" s="T692">šo-bi</ta>
            <ta e="T695" id="Seg_6870" s="T694">dĭʔ-nə</ta>
            <ta e="T696" id="Seg_6871" s="T695">măn-də</ta>
            <ta e="T697" id="Seg_6872" s="T696">kan-žə-bəj</ta>
            <ta e="T698" id="Seg_6873" s="T697">maʔ-nʼibeʔ</ta>
            <ta e="T699" id="Seg_6874" s="T698">miʔ</ta>
            <ta e="T700" id="Seg_6875" s="T699">Krasnojarskə-ʔi</ta>
            <ta e="T701" id="Seg_6876" s="T700">kal-lə-baʔ</ta>
            <ta e="T702" id="Seg_6877" s="T701">a</ta>
            <ta e="T703" id="Seg_6878" s="T702">măn</ta>
            <ta e="T704" id="Seg_6879" s="T703">măm-bia-m</ta>
            <ta e="T705" id="Seg_6880" s="T704">dĭʔ-nə</ta>
            <ta e="T706" id="Seg_6881" s="T705">i-geʔ</ta>
            <ta e="T707" id="Seg_6882" s="T706">ia-nə-l</ta>
            <ta e="T708" id="Seg_6883" s="T707">ĭmbi=nʼibudʼ</ta>
            <ta e="T709" id="Seg_6884" s="T708">kuvas</ta>
            <ta e="T710" id="Seg_6885" s="T709">plat</ta>
            <ta e="T712" id="Seg_6886" s="T711">i-geʔ</ta>
            <ta e="T713" id="Seg_6887" s="T712">oldʼa</ta>
            <ta e="T714" id="Seg_6888" s="T713">i-t</ta>
            <ta e="T715" id="Seg_6889" s="T714">kuvas</ta>
            <ta e="T716" id="Seg_6890" s="T715">dĭ-zeŋ</ta>
            <ta e="T717" id="Seg_6891" s="T716">bar</ta>
            <ta e="T718" id="Seg_6892" s="T717">kaknar-laʔbə-ʔjə</ta>
            <ta e="T719" id="Seg_6893" s="T718">što</ta>
            <ta e="T720" id="Seg_6894" s="T719">măn</ta>
            <ta e="T721" id="Seg_6895" s="T720">dăre</ta>
            <ta e="T722" id="Seg_6896" s="T721">dʼăbaktər-laʔbə-m</ta>
            <ta e="T723" id="Seg_6897" s="T722">dĭ-zeŋ</ta>
            <ta e="T724" id="Seg_6898" s="T723">ej</ta>
            <ta e="T725" id="Seg_6899" s="T724">tĭmne-ʔi</ta>
            <ta e="T726" id="Seg_6900" s="T725">dʼăbaktər-zittə</ta>
            <ta e="T732" id="Seg_6901" s="T731">teinen</ta>
            <ta e="T733" id="Seg_6902" s="T732">nüke</ta>
            <ta e="T734" id="Seg_6903" s="T733">šo-bi</ta>
            <ta e="T735" id="Seg_6904" s="T734">nada</ta>
            <ta e="T736" id="Seg_6905" s="T735">măna</ta>
            <ta e="T737" id="Seg_6906" s="T736">toltanoʔ</ta>
            <ta e="T738" id="Seg_6907" s="T737">am-zittə</ta>
            <ta e="T739" id="Seg_6908" s="T738">kal-la-m</ta>
            <ta e="T740" id="Seg_6909" s="T739">tüjö</ta>
            <ta e="T741" id="Seg_6910" s="T740">bos-tə</ta>
            <ta e="T742" id="Seg_6911" s="T741">nʼi-nə</ta>
            <ta e="T743" id="Seg_6912" s="T742">puskaj</ta>
            <ta e="T744" id="Seg_6913" s="T743">amnol-lə-j</ta>
            <ta e="T745" id="Seg_6914" s="T744">măna</ta>
            <ta e="T746" id="Seg_6915" s="T745">toltanoʔ</ta>
            <ta e="T748" id="Seg_6916" s="T747">măn</ta>
            <ta e="T749" id="Seg_6917" s="T748">i-bie-m</ta>
            <ta e="T750" id="Seg_6918" s="T749">Zaozʼorka-gən</ta>
            <ta e="T751" id="Seg_6919" s="T750">xatʼel</ta>
            <ta e="T752" id="Seg_6920" s="T751">kam-bia-m</ta>
            <ta e="T753" id="Seg_6921" s="T752">aptʼeka-nə</ta>
            <ta e="T754" id="Seg_6922" s="T753">sima-ndə</ta>
            <ta e="T755" id="Seg_6923" s="T754">xatʼel</ta>
            <ta e="T756" id="Seg_6924" s="T755">i-zittə</ta>
            <ta e="T757" id="Seg_6925" s="T756">štobɨ</ta>
            <ta e="T758" id="Seg_6926" s="T757">măndə-r-zittə</ta>
            <ta e="T759" id="Seg_6927" s="T758">kuŋge-ŋ</ta>
            <ta e="T760" id="Seg_6928" s="T759">dĭ-zeŋ</ta>
            <ta e="T761" id="Seg_6929" s="T760">ej</ta>
            <ta e="T762" id="Seg_6930" s="T761">mĭ-bi-ʔi</ta>
            <ta e="T763" id="Seg_6931" s="T762">mănd-laʔbə-ʔjə</ta>
            <ta e="T764" id="Seg_6932" s="T763">kan-a-ʔ</ta>
            <ta e="T765" id="Seg_6933" s="T764">sʼima-l</ta>
            <ta e="T766" id="Seg_6934" s="T765">măndə-r-də</ta>
            <ta e="T767" id="Seg_6935" s="T766">dĭgəttə</ta>
            <ta e="T768" id="Seg_6936" s="T767">šo-la-l</ta>
            <ta e="T769" id="Seg_6937" s="T768">i</ta>
            <ta e="T770" id="Seg_6938" s="T769">i-li-l</ta>
            <ta e="T772" id="Seg_6939" s="T771">dĭbər</ta>
            <ta e="T773" id="Seg_6940" s="T772">kam-bia-m</ta>
            <ta e="T774" id="Seg_6941" s="T773">dĭ-zeŋ</ta>
            <ta e="T775" id="Seg_6942" s="T774">bar</ta>
            <ta e="T776" id="Seg_6943" s="T775">dʼabəro-laʔbə</ta>
            <ta e="T777" id="Seg_6944" s="T776">ĭmbi</ta>
            <ta e="T778" id="Seg_6945" s="T777">dʼabəro-laʔbə-laʔ</ta>
            <ta e="T779" id="Seg_6946" s="T778">i-ʔ</ta>
            <ta e="T780" id="Seg_6947" s="T779">kirgar-laʔ</ta>
            <ta e="T781" id="Seg_6948" s="T780">jakšə-ŋ</ta>
            <ta e="T782" id="Seg_6949" s="T781">a-gaʔ</ta>
            <ta e="T783" id="Seg_6950" s="T782">tura</ta>
            <ta e="T784" id="Seg_6951" s="T783">sĭbrej-e-ʔ</ta>
            <ta e="T786" id="Seg_6952" s="T785">onʼiʔ-tə</ta>
            <ta e="T787" id="Seg_6953" s="T786">măm-bia-m</ta>
            <ta e="T788" id="Seg_6954" s="T787">kan-a-ʔ</ta>
            <ta e="T789" id="Seg_6955" s="T788">lʼon-də</ta>
            <ta e="T790" id="Seg_6956" s="T789">toʔ-nar-də</ta>
            <ta e="T791" id="Seg_6957" s="T790">a</ta>
            <ta e="T792" id="Seg_6958" s="T791">dĭ</ta>
            <ta e="T793" id="Seg_6959" s="T792">pušaj</ta>
            <ta e="T794" id="Seg_6960" s="T793">bü</ta>
            <ta e="T795" id="Seg_6961" s="T794">tažor-lə-j</ta>
            <ta e="T796" id="Seg_6962" s="T795">moltʼa-nə</ta>
            <ta e="T798" id="Seg_6963" s="T797">măn</ta>
            <ta e="T799" id="Seg_6964" s="T798">teinen</ta>
            <ta e="T800" id="Seg_6965" s="T799">erte</ta>
            <ta e="T801" id="Seg_6966" s="T800">uʔbdə-bia-m</ta>
            <ta e="T802" id="Seg_6967" s="T801">iššo</ta>
            <ta e="T804" id="Seg_6968" s="T803">sumna</ta>
            <ta e="T805" id="Seg_6969" s="T804">nago-bi</ta>
            <ta e="T806" id="Seg_6970" s="T805">dĭgəttə</ta>
            <ta e="T807" id="Seg_6971" s="T806">pʼeš-bə</ta>
            <ta e="T808" id="Seg_6972" s="T807">nendə-bie-m</ta>
            <ta e="T809" id="Seg_6973" s="T808">po</ta>
            <ta e="T810" id="Seg_6974" s="T809">nuldə-bia-m</ta>
            <ta e="T811" id="Seg_6975" s="T810">dĭgəttə</ta>
            <ta e="T812" id="Seg_6976" s="T811">bazoʔ</ta>
            <ta e="T813" id="Seg_6977" s="T812">iʔbə-bie-m</ta>
            <ta e="T814" id="Seg_6978" s="T813">iššo</ta>
            <ta e="T815" id="Seg_6979" s="T814">kunol-bia-m</ta>
            <ta e="T816" id="Seg_6980" s="T815">iššo</ta>
            <ta e="T817" id="Seg_6981" s="T816">bieʔ</ta>
            <ta e="T818" id="Seg_6982" s="T817">mo-bi</ta>
            <ta e="T819" id="Seg_6983" s="T818">dĭgəttə</ta>
            <ta e="T821" id="Seg_6984" s="T820">uʔbdə-bia-m</ta>
            <ta e="T823" id="Seg_6985" s="T822">sedem</ta>
            <ta e="T824" id="Seg_6986" s="T823">teinen</ta>
            <ta e="T825" id="Seg_6987" s="T824">togonor-bia-m</ta>
            <ta e="T826" id="Seg_6988" s="T825">a</ta>
            <ta e="T827" id="Seg_6989" s="T826">taldʼen</ta>
            <ta e="T828" id="Seg_6990" s="T827">ej</ta>
            <ta e="T829" id="Seg_6991" s="T828">sedem</ta>
            <ta e="T831" id="Seg_6992" s="T830">dĭ</ta>
            <ta e="T832" id="Seg_6993" s="T831">pi</ta>
            <ta e="T833" id="Seg_6994" s="T832">ugandə</ta>
            <ta e="T834" id="Seg_6995" s="T833">urgo</ta>
            <ta e="T835" id="Seg_6996" s="T834">sedem</ta>
            <ta e="T836" id="Seg_6997" s="T835">a</ta>
            <ta e="T837" id="Seg_6998" s="T836">dö</ta>
            <ta e="T838" id="Seg_6999" s="T837">pa</ta>
            <ta e="T839" id="Seg_7000" s="T838">ej</ta>
            <ta e="T840" id="Seg_7001" s="T839">sedem</ta>
            <ta e="T842" id="Seg_7002" s="T841">dĭ</ta>
            <ta e="T844" id="Seg_7003" s="T843">dĭ</ta>
            <ta e="T845" id="Seg_7004" s="T844">ešši</ta>
            <ta e="T846" id="Seg_7005" s="T845">bar</ta>
            <ta e="T847" id="Seg_7006" s="T846">piʔme-zeŋ-də</ta>
            <ta e="T848" id="Seg_7007" s="T847">bar</ta>
            <ta e="T849" id="Seg_7008" s="T848">saj-nožuʔ-pi</ta>
            <ta e="T850" id="Seg_7009" s="T849">bar</ta>
            <ta e="T851" id="Seg_7010" s="T850">ši</ta>
            <ta e="T852" id="Seg_7011" s="T851">mo-laːm-bi</ta>
            <ta e="T853" id="Seg_7012" s="T852">nada</ta>
            <ta e="T854" id="Seg_7013" s="T853">dĭ-m</ta>
            <ta e="T855" id="Seg_7014" s="T854">šöʔ-sittə</ta>
            <ta e="T857" id="Seg_7015" s="T856">en-zittə</ta>
            <ta e="T859" id="Seg_7016" s="T858">edə-ʔ</ta>
            <ta e="T860" id="Seg_7017" s="T859">măna</ta>
            <ta e="T861" id="Seg_7018" s="T860">sĭreʔpne</ta>
            <ta e="T862" id="Seg_7019" s="T861">sumna</ta>
            <ta e="T863" id="Seg_7020" s="T862">kilogram</ta>
            <ta e="T865" id="Seg_7021" s="T864">dĭ</ta>
            <ta e="T866" id="Seg_7022" s="T865">kuza</ta>
            <ta e="T867" id="Seg_7023" s="T866">ĭzem-bi</ta>
            <ta e="T868" id="Seg_7024" s="T867">a</ta>
            <ta e="T869" id="Seg_7025" s="T868">tüj</ta>
            <ta e="T870" id="Seg_7026" s="T869">ej</ta>
            <ta e="T871" id="Seg_7027" s="T870">ĭzem-nie</ta>
            <ta e="T872" id="Seg_7028" s="T871">uʔbdə-bi</ta>
            <ta e="T873" id="Seg_7029" s="T872">üjü-zi</ta>
            <ta e="T874" id="Seg_7030" s="T873">mĭŋ-ge</ta>
            <ta e="T875" id="Seg_7031" s="T874">ato</ta>
            <ta e="T876" id="Seg_7032" s="T875">iʔbo-bi</ta>
            <ta e="T877" id="Seg_7033" s="T876">ej</ta>
            <ta e="T878" id="Seg_7034" s="T877">jakšə</ta>
            <ta e="T879" id="Seg_7035" s="T878">nʼilgö-laʔ-sittə</ta>
            <ta e="T880" id="Seg_7036" s="T879">šo-bi-ʔi</ta>
            <ta e="T881" id="Seg_7037" s="T880">da</ta>
            <ta e="T882" id="Seg_7038" s="T881">măm-bi-ʔi</ta>
            <ta e="T883" id="Seg_7039" s="T882">ej</ta>
            <ta e="T884" id="Seg_7040" s="T883">kuvas</ta>
            <ta e="T885" id="Seg_7041" s="T884">dʼăbaktər-la</ta>
            <ta e="T887" id="Seg_7042" s="T886">dĭ</ta>
            <ta e="T888" id="Seg_7043" s="T887">dĭ</ta>
            <ta e="T889" id="Seg_7044" s="T888">bar</ta>
            <ta e="T890" id="Seg_7045" s="T889">kuza-m</ta>
            <ta e="T891" id="Seg_7046" s="T890">kuʔ-pi</ta>
            <ta e="T892" id="Seg_7047" s="T891">dĭ-m</ta>
            <ta e="T893" id="Seg_7048" s="T892">amnol-bi-ʔi</ta>
            <ta e="T895" id="Seg_7049" s="T894">nagur</ta>
            <ta e="T896" id="Seg_7050" s="T895">kö</ta>
            <ta e="T897" id="Seg_7051" s="T896">amno-laʔ-pi</ta>
            <ta e="T898" id="Seg_7052" s="T897">i</ta>
            <ta e="T899" id="Seg_7053" s="T898">tuj</ta>
            <ta e="T900" id="Seg_7054" s="T899">maːʔ-ndə</ta>
            <ta e="T901" id="Seg_7055" s="T900">šo-bi</ta>
            <ta e="T902" id="Seg_7056" s="T901">i</ta>
            <ta e="T903" id="Seg_7057" s="T902">mĭl-leʔbə</ta>
            <ta e="T905" id="Seg_7058" s="T904">bar</ta>
            <ta e="T906" id="Seg_7059" s="T905">takše-ʔi</ta>
            <ta e="T910" id="Seg_7060" s="T909">băld-luʔ-pi-ʔi</ta>
            <ta e="T913" id="Seg_7061" s="T912">măn</ta>
            <ta e="T916" id="Seg_7062" s="T915">ulu-m</ta>
            <ta e="T917" id="Seg_7063" s="T916">bar</ta>
            <ta e="T918" id="Seg_7064" s="T917">pi-ziʔ</ta>
            <ta e="T919" id="Seg_7065" s="T918">toʔ-nar-luʔ-pi-ʔi</ta>
            <ta e="T920" id="Seg_7066" s="T919">bar</ta>
            <ta e="T921" id="Seg_7067" s="T920">bar</ta>
            <ta e="T922" id="Seg_7068" s="T921">kem</ta>
            <ta e="T923" id="Seg_7069" s="T922">mʼaŋ-ŋaʔbə</ta>
            <ta e="T925" id="Seg_7070" s="T923">măna</ta>
            <ta e="T926" id="Seg_7071" s="T925">baltu-ziʔ</ta>
            <ta e="T927" id="Seg_7072" s="T926">bar</ta>
            <ta e="T928" id="Seg_7073" s="T927">ulu-m</ta>
            <ta e="T932" id="Seg_7074" s="T931">băldə-bi-ʔi</ta>
            <ta e="T934" id="Seg_7075" s="T932">dʼije-nə</ta>
            <ta e="T935" id="Seg_7076" s="T934">ka-la-l</ta>
            <ta e="T936" id="Seg_7077" s="T935">keʔbde</ta>
            <ta e="T937" id="Seg_7078" s="T936">oʔbdə-sʼtə</ta>
            <ta e="T938" id="Seg_7079" s="T937">kan-žə-baʔ</ta>
            <ta e="T939" id="Seg_7080" s="T938">maʔ-nʼibeʔ</ta>
            <ta e="T940" id="Seg_7081" s="T939">keʔbde</ta>
            <ta e="T941" id="Seg_7082" s="T940">naga</ta>
            <ta e="T942" id="Seg_7083" s="T941">tolʼko</ta>
            <ta e="T944" id="Seg_7084" s="T943">kan-zittə</ta>
            <ta e="T945" id="Seg_7085" s="T944">gijen=də</ta>
            <ta e="T946" id="Seg_7086" s="T945">šo-lə-ʔjə</ta>
            <ta e="T947" id="Seg_7087" s="T946">ugandə</ta>
            <ta e="T948" id="Seg_7088" s="T947">iʔgö</ta>
            <ta e="T949" id="Seg_7089" s="T948">keʔbde</ta>
            <ta e="T950" id="Seg_7090" s="T949">dĭgəttə</ta>
            <ta e="T951" id="Seg_7091" s="T950">bazoʔ</ta>
            <ta e="T952" id="Seg_7092" s="T951">oʔbdə-lia-l</ta>
            <ta e="T953" id="Seg_7093" s="T952">dĭgəttə</ta>
            <ta e="T954" id="Seg_7094" s="T953">mo-lia-m</ta>
            <ta e="T955" id="Seg_7095" s="T954">kamen</ta>
            <ta e="T956" id="Seg_7096" s="T955">maʔ-nʼi</ta>
            <ta e="T957" id="Seg_7097" s="T956">kan-zittə</ta>
            <ta e="T958" id="Seg_7098" s="T957">üge</ta>
            <ta e="T959" id="Seg_7099" s="T958">keʔbde</ta>
            <ta e="T960" id="Seg_7100" s="T959">iʔgö</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_7101" s="T4">măn</ta>
            <ta e="T6" id="Seg_7102" s="T5">teinen</ta>
            <ta e="T7" id="Seg_7103" s="T6">šumura-Tə</ta>
            <ta e="T8" id="Seg_7104" s="T7">mĭn-bi-m</ta>
            <ta e="T9" id="Seg_7105" s="T8">dĭn</ta>
            <ta e="T10" id="Seg_7106" s="T9">nüke</ta>
            <ta e="T11" id="Seg_7107" s="T10">amno-laʔbə</ta>
            <ta e="T12" id="Seg_7108" s="T11">am-ə-ʔ</ta>
            <ta e="T13" id="Seg_7109" s="T12">amor-ə-ʔ</ta>
            <ta e="T14" id="Seg_7110" s="T13">kăpusta</ta>
            <ta e="T15" id="Seg_7111" s="T14">oi</ta>
            <ta e="T16" id="Seg_7112" s="T15">ugaːndə</ta>
            <ta e="T17" id="Seg_7113" s="T16">namzəga</ta>
            <ta e="T18" id="Seg_7114" s="T17">e-m</ta>
            <ta e="T19" id="Seg_7115" s="T18">am-ə-ʔ</ta>
            <ta e="T20" id="Seg_7116" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_7117" s="T20">koʔbdo</ta>
            <ta e="T22" id="Seg_7118" s="T21">kan-bi</ta>
            <ta e="T23" id="Seg_7119" s="T22">tüʔ-zittə</ta>
            <ta e="T24" id="Seg_7120" s="T23">men</ta>
            <ta e="T25" id="Seg_7121" s="T24">dĭn-ziʔ</ta>
            <ta e="T26" id="Seg_7122" s="T25">măn</ta>
            <ta e="T28" id="Seg_7123" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_7124" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_7125" s="T29">šo-bi</ta>
            <ta e="T31" id="Seg_7126" s="T30">a</ta>
            <ta e="T32" id="Seg_7127" s="T31">măn</ta>
            <ta e="T33" id="Seg_7128" s="T32">dĭ-Tə</ta>
            <ta e="T34" id="Seg_7129" s="T33">tʼăbaktər-liA-m</ta>
            <ta e="T35" id="Seg_7130" s="T34">tăn</ta>
            <ta e="T36" id="Seg_7131" s="T35">mĭn-bi-l</ta>
            <ta e="T37" id="Seg_7132" s="T36">tüʔ-zittə</ta>
            <ta e="T38" id="Seg_7133" s="T37">i</ta>
            <ta e="T39" id="Seg_7134" s="T38">men</ta>
            <ta e="T40" id="Seg_7135" s="T39">tăn-ziʔ</ta>
            <ta e="T41" id="Seg_7136" s="T40">dĭ</ta>
            <ta e="T42" id="Seg_7137" s="T41">bar</ta>
            <ta e="T43" id="Seg_7138" s="T42">kakənar-luʔbdə-bi</ta>
            <ta e="T44" id="Seg_7139" s="T43">ugaːndə</ta>
            <ta e="T45" id="Seg_7140" s="T44">tăŋ</ta>
            <ta e="T46" id="Seg_7141" s="T45">kĭškə-jəʔ</ta>
            <ta e="T47" id="Seg_7142" s="T46">bar</ta>
            <ta e="T49" id="Seg_7143" s="T48">üjü-jəʔ</ta>
            <ta e="T50" id="Seg_7144" s="T49">ĭzem-laʔbə-jəʔ</ta>
            <ta e="T51" id="Seg_7145" s="T50">ugaːndə</ta>
            <ta e="T53" id="Seg_7146" s="T52">iʔgö</ta>
            <ta e="T54" id="Seg_7147" s="T53">pʼe</ta>
            <ta e="T964" id="Seg_7148" s="T54">kan-lAʔ</ta>
            <ta e="T55" id="Seg_7149" s="T964">tʼür-bi-jəʔ</ta>
            <ta e="T56" id="Seg_7150" s="T55">il</ta>
            <ta e="T57" id="Seg_7151" s="T56">bar</ta>
            <ta e="T58" id="Seg_7152" s="T57">ĭmbi=də</ta>
            <ta e="T59" id="Seg_7153" s="T58">ej</ta>
            <ta e="T60" id="Seg_7154" s="T59">tĭmne-jəʔ</ta>
            <ta e="T61" id="Seg_7155" s="T60">dĭ-zen</ta>
            <ta e="T62" id="Seg_7156" s="T61">bar</ta>
            <ta e="T64" id="Seg_7157" s="T63">iʔgö</ta>
            <ta e="T65" id="Seg_7158" s="T64">ine-zAŋ-də</ta>
            <ta e="T66" id="Seg_7159" s="T65">i</ta>
            <ta e="T67" id="Seg_7160" s="T66">tüžöj-jəʔ</ta>
            <ta e="T68" id="Seg_7161" s="T67">iʔgö</ta>
            <ta e="T69" id="Seg_7162" s="T68">i</ta>
            <ta e="T70" id="Seg_7163" s="T69">ular</ta>
            <ta e="T71" id="Seg_7164" s="T70">iʔgö</ta>
            <ta e="T72" id="Seg_7165" s="T71">kuriza-jəʔ</ta>
            <ta e="T73" id="Seg_7166" s="T72">iʔgö</ta>
            <ta e="T74" id="Seg_7167" s="T73">uja</ta>
            <ta e="T75" id="Seg_7168" s="T74">am-laʔbə-jəʔ</ta>
            <ta e="T76" id="Seg_7169" s="T75">iʔgö</ta>
            <ta e="T77" id="Seg_7170" s="T76">bar</ta>
            <ta e="T78" id="Seg_7171" s="T77">süt</ta>
            <ta e="T79" id="Seg_7172" s="T78">bar</ta>
            <ta e="T80" id="Seg_7173" s="T79">iʔgö</ta>
            <ta e="T82" id="Seg_7174" s="T81">nu-zAŋ</ta>
            <ta e="T83" id="Seg_7175" s="T82">bar</ta>
            <ta e="T84" id="Seg_7176" s="T83">amno-laʔpi-jəʔ</ta>
            <ta e="T85" id="Seg_7177" s="T84">kudaj-Tə</ta>
            <ta e="T86" id="Seg_7178" s="T85">a-bi-jəʔ</ta>
            <ta e="T88" id="Seg_7179" s="T87">pa-ziʔ</ta>
            <ta e="T89" id="Seg_7180" s="T88">dĭgəttə</ta>
            <ta e="T91" id="Seg_7181" s="T90">pi-gəʔ</ta>
            <ta e="T92" id="Seg_7182" s="T91">a-bi-jəʔ</ta>
            <ta e="T93" id="Seg_7183" s="T92">dĭgəttə</ta>
            <ta e="T94" id="Seg_7184" s="T93">sazən-zəbi</ta>
            <ta e="T95" id="Seg_7185" s="T94">a-bi-jəʔ</ta>
            <ta e="T97" id="Seg_7186" s="T96">baza-j</ta>
            <ta e="T98" id="Seg_7187" s="T97">kudaj</ta>
            <ta e="T99" id="Seg_7188" s="T98">a-bi-jəʔ</ta>
            <ta e="T100" id="Seg_7189" s="T99">i</ta>
            <ta e="T101" id="Seg_7190" s="T100">nuldə-bi-jəʔ</ta>
            <ta e="T102" id="Seg_7191" s="T101">bar</ta>
            <ta e="T104" id="Seg_7192" s="T103">dĭ-zAŋ-Tə</ta>
            <ta e="T105" id="Seg_7193" s="T104">anʼi</ta>
            <ta e="T106" id="Seg_7194" s="T105">bar</ta>
            <ta e="T107" id="Seg_7195" s="T106">sv’ečka-jəʔ</ta>
            <ta e="T108" id="Seg_7196" s="T107">nuldə-bi-jəʔ</ta>
            <ta e="T109" id="Seg_7197" s="T108">nendə-bi-jəʔ</ta>
            <ta e="T111" id="Seg_7198" s="T110">nu-zAŋ</ta>
            <ta e="T112" id="Seg_7199" s="T111">bar</ta>
            <ta e="T113" id="Seg_7200" s="T112">mĭn-bi-jəʔ</ta>
            <ta e="T114" id="Seg_7201" s="T113">dʼije-Kən</ta>
            <ta e="T115" id="Seg_7202" s="T114">i</ta>
            <ta e="T116" id="Seg_7203" s="T115">dĭgəttə</ta>
            <ta e="T117" id="Seg_7204" s="T116">šišəge</ta>
            <ta e="T118" id="Seg_7205" s="T117">i-bi</ta>
            <ta e="T119" id="Seg_7206" s="T118">tak</ta>
            <ta e="T120" id="Seg_7207" s="T119">šo-lV-jəʔ</ta>
            <ta e="T121" id="Seg_7208" s="T120">döbər</ta>
            <ta e="T122" id="Seg_7209" s="T121">bar</ta>
            <ta e="T123" id="Seg_7210" s="T122">dĭn</ta>
            <ta e="T124" id="Seg_7211" s="T123">stara</ta>
            <ta e="T125" id="Seg_7212" s="T124">stoibe</ta>
            <ta e="T126" id="Seg_7213" s="T125">amno-bi-jəʔ</ta>
            <ta e="T127" id="Seg_7214" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_7215" s="T127">ku-bi-jəʔ</ta>
            <ta e="T129" id="Seg_7216" s="T128">dön</ta>
            <ta e="T130" id="Seg_7217" s="T129">bü</ta>
            <ta e="T131" id="Seg_7218" s="T130">ej</ta>
            <ta e="T132" id="Seg_7219" s="T131">kănzə-liA</ta>
            <ta e="T133" id="Seg_7220" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_7221" s="T133">dön</ta>
            <ta e="T138" id="Seg_7222" s="T137">nuldə-laʔbə-jəʔ</ta>
            <ta e="T139" id="Seg_7223" s="T138">bar</ta>
            <ta e="T140" id="Seg_7224" s="T139">maʔ-ziʔ</ta>
            <ta e="T141" id="Seg_7225" s="T140">i</ta>
            <ta e="T142" id="Seg_7226" s="T141">dön</ta>
            <ta e="T143" id="Seg_7227" s="T142">amno-laʔpi-jəʔ</ta>
            <ta e="T144" id="Seg_7228" s="T143">dĭ-zAŋ</ta>
            <ta e="T145" id="Seg_7229" s="T144">bar</ta>
            <ta e="T146" id="Seg_7230" s="T145">mĭn-bi-jəʔ</ta>
            <ta e="T150" id="Seg_7231" s="T149">aʔtʼi-t</ta>
            <ta e="T151" id="Seg_7232" s="T150">bar</ta>
            <ta e="T152" id="Seg_7233" s="T151">tădam</ta>
            <ta e="T153" id="Seg_7234" s="T152">i-bi</ta>
            <ta e="T154" id="Seg_7235" s="T153">onʼiʔ</ta>
            <ta e="T155" id="Seg_7236" s="T154">kandə-gA</ta>
            <ta e="T156" id="Seg_7237" s="T155">dărəʔ</ta>
            <ta e="T157" id="Seg_7238" s="T156">bar</ta>
            <ta e="T158" id="Seg_7239" s="T157">kan-ntə-gA-jəʔ</ta>
            <ta e="T159" id="Seg_7240" s="T158">kak</ta>
            <ta e="T160" id="Seg_7241" s="T159">nabə-jəʔ</ta>
            <ta e="T161" id="Seg_7242" s="T160">dʼije-Tə</ta>
            <ta e="T162" id="Seg_7243" s="T161">kan-bi-jəʔ</ta>
            <ta e="T163" id="Seg_7244" s="T162">onʼiʔ</ta>
            <ta e="T164" id="Seg_7245" s="T163">ej</ta>
            <ta e="T165" id="Seg_7246" s="T164">kan-liA</ta>
            <ta e="T166" id="Seg_7247" s="T165">a</ta>
            <ta e="T167" id="Seg_7248" s="T166">iʔgö</ta>
            <ta e="T168" id="Seg_7249" s="T167">kan-lV-jəʔ</ta>
            <ta e="T170" id="Seg_7250" s="T169">dĭ-zAŋ</ta>
            <ta e="T171" id="Seg_7251" s="T170">bar</ta>
            <ta e="T172" id="Seg_7252" s="T171">dʼije-gəʔ</ta>
            <ta e="T173" id="Seg_7253" s="T172">šonə-gA-jəʔ</ta>
            <ta e="T174" id="Seg_7254" s="T173">onʼiʔ</ta>
            <ta e="T175" id="Seg_7255" s="T174">šide</ta>
            <ta e="T176" id="Seg_7256" s="T175">teʔdə</ta>
            <ta e="T177" id="Seg_7257" s="T176">nagur</ta>
            <ta e="T178" id="Seg_7258" s="T177">sumna</ta>
            <ta e="T179" id="Seg_7259" s="T178">muktuʔ</ta>
            <ta e="T180" id="Seg_7260" s="T179">onʼiʔ</ta>
            <ta e="T181" id="Seg_7261" s="T180">onʼiʔ-ziʔ</ta>
            <ta e="T186" id="Seg_7262" s="T185">šo-laʔbə-jəʔ</ta>
            <ta e="T187" id="Seg_7263" s="T186">bü</ta>
            <ta e="T188" id="Seg_7264" s="T187">bar</ta>
            <ta e="T189" id="Seg_7265" s="T188">kănzə-laʔpi</ta>
            <ta e="T190" id="Seg_7266" s="T189">üjü-ziʔ</ta>
            <ta e="T191" id="Seg_7267" s="T190">nuldə-liA-l</ta>
            <ta e="T192" id="Seg_7268" s="T191">nu-lV-l</ta>
            <ta e="T193" id="Seg_7269" s="T192">tak</ta>
            <ta e="T194" id="Seg_7270" s="T193">dĭbər</ta>
            <ta e="T195" id="Seg_7271" s="T194">ej</ta>
            <ta e="T196" id="Seg_7272" s="T195">saʔmə-liA-l</ta>
            <ta e="T197" id="Seg_7273" s="T196">măn</ta>
            <ta e="T198" id="Seg_7274" s="T197">üdʼüge</ta>
            <ta e="T199" id="Seg_7275" s="T198">i-bi-m</ta>
            <ta e="T200" id="Seg_7276" s="T199">măn</ta>
            <ta e="T201" id="Seg_7277" s="T200">ija-m</ta>
            <ta e="T202" id="Seg_7278" s="T201">togonər-bi</ta>
            <ta e="T203" id="Seg_7279" s="T202">kuba-jəʔ</ta>
            <ta e="T205" id="Seg_7280" s="T204">a-bi</ta>
            <ta e="T206" id="Seg_7281" s="T205">parga-jəʔ</ta>
            <ta e="T207" id="Seg_7282" s="T206">šo-bi</ta>
            <ta e="T208" id="Seg_7283" s="T207">jama-jəʔ</ta>
            <ta e="T209" id="Seg_7284" s="T208">šöʔ-bi</ta>
            <ta e="T213" id="Seg_7285" s="T212">üžü</ta>
            <ta e="T215" id="Seg_7286" s="T214">šöʔ-bi</ta>
            <ta e="T217" id="Seg_7287" s="T215">i</ta>
            <ta e="T218" id="Seg_7288" s="T217">aba-m</ta>
            <ta e="T219" id="Seg_7289" s="T218">bar</ta>
            <ta e="T220" id="Seg_7290" s="T219">šer-bi</ta>
            <ta e="T221" id="Seg_7291" s="T220">tüj</ta>
            <ta e="T222" id="Seg_7292" s="T221">măn</ta>
            <ta e="T223" id="Seg_7293" s="T222">šal-m</ta>
            <ta e="T224" id="Seg_7294" s="T223">naga</ta>
            <ta e="T225" id="Seg_7295" s="T224">ej</ta>
            <ta e="T226" id="Seg_7296" s="T225">tĭmne-m</ta>
            <ta e="T227" id="Seg_7297" s="T226">dĭ</ta>
            <ta e="T228" id="Seg_7298" s="T227">giraːm-bi</ta>
            <ta e="T230" id="Seg_7299" s="T229">ugaːndə</ta>
            <ta e="T232" id="Seg_7300" s="T231">pim-liA-m</ta>
            <ta e="T233" id="Seg_7301" s="T232">bar</ta>
            <ta e="T234" id="Seg_7302" s="T233">măna</ta>
            <ta e="T235" id="Seg_7303" s="T234">sădăr-laʔbə</ta>
            <ta e="T237" id="Seg_7304" s="T236">teinen</ta>
            <ta e="T239" id="Seg_7305" s="T238">dʼije-Kən</ta>
            <ta e="T240" id="Seg_7306" s="T239">šaː-bi-lAʔ</ta>
            <ta e="T241" id="Seg_7307" s="T240">ugaːndə</ta>
            <ta e="T242" id="Seg_7308" s="T241">šišəge</ta>
            <ta e="T243" id="Seg_7309" s="T242">i-bi</ta>
            <ta e="T244" id="Seg_7310" s="T243">da</ta>
            <ta e="T245" id="Seg_7311" s="T244">miʔ</ta>
            <ta e="T246" id="Seg_7312" s="T245">ugaːndə</ta>
            <ta e="T247" id="Seg_7313" s="T246">kănzə-laːm-bi-bAʔ</ta>
            <ta e="T248" id="Seg_7314" s="T247">bar</ta>
            <ta e="T249" id="Seg_7315" s="T248">tăŋ</ta>
            <ta e="T250" id="Seg_7316" s="T249">kănzə-laːm-bi-bAʔ</ta>
            <ta e="T252" id="Seg_7317" s="T251">ugaːndə</ta>
            <ta e="T253" id="Seg_7318" s="T252">šišəge</ta>
            <ta e="T254" id="Seg_7319" s="T253">bü</ta>
            <ta e="T255" id="Seg_7320" s="T254">bar</ta>
            <ta e="T256" id="Seg_7321" s="T255">kănzə-laːm-bi</ta>
            <ta e="T257" id="Seg_7322" s="T256">măn</ta>
            <ta e="T258" id="Seg_7323" s="T257">üjü-ziʔ-m</ta>
            <ta e="T260" id="Seg_7324" s="T259">nu-bi-m</ta>
            <ta e="T261" id="Seg_7325" s="T260">dĭ</ta>
            <ta e="T262" id="Seg_7326" s="T261">ej</ta>
            <ta e="T263" id="Seg_7327" s="T262">băldə-bi</ta>
            <ta e="T265" id="Seg_7328" s="T264">kamən</ta>
            <ta e="T266" id="Seg_7329" s="T265">kunol-zittə</ta>
            <ta e="T267" id="Seg_7330" s="T266">iʔbö-lV-l</ta>
            <ta e="T268" id="Seg_7331" s="T267">surar-ʔ</ta>
            <ta e="T269" id="Seg_7332" s="T268">öʔ-lV-l</ta>
            <ta e="T270" id="Seg_7333" s="T269">măna</ta>
            <ta e="T271" id="Seg_7334" s="T270">kunol-zittə</ta>
            <ta e="T272" id="Seg_7335" s="T271">dön</ta>
            <ta e="T274" id="Seg_7336" s="T272">baštap</ta>
            <ta e="T275" id="Seg_7337" s="T274">dĭn</ta>
            <ta e="T276" id="Seg_7338" s="T275">nu-bi-jəʔ</ta>
            <ta e="T277" id="Seg_7339" s="T276">Ilʼbinʼ-ə-n</ta>
            <ta e="T278" id="Seg_7340" s="T277">toʔ-gəndə</ta>
            <ta e="T279" id="Seg_7341" s="T278">dĭgəttə</ta>
            <ta e="T280" id="Seg_7342" s="T279">dö</ta>
            <ta e="T281" id="Seg_7343" s="T280">bü</ta>
            <ta e="T282" id="Seg_7344" s="T281">ku-bi-jəʔ</ta>
            <ta e="T283" id="Seg_7345" s="T282">ej</ta>
            <ta e="T285" id="Seg_7346" s="T284">ej</ta>
            <ta e="T286" id="Seg_7347" s="T285">kănzə-liA</ta>
            <ta e="T288" id="Seg_7348" s="T287">dĭ-zAŋ</ta>
            <ta e="T289" id="Seg_7349" s="T288">dön</ta>
            <ta e="T290" id="Seg_7350" s="T289">maʔ-jəʔ</ta>
            <ta e="T291" id="Seg_7351" s="T290">nuldə-bi-jəʔ</ta>
            <ta e="T292" id="Seg_7352" s="T291">i</ta>
            <ta e="T293" id="Seg_7353" s="T292">döbər</ta>
            <ta e="T294" id="Seg_7354" s="T293">amno-zittə</ta>
            <ta e="T295" id="Seg_7355" s="T294">šo-bi-jəʔ</ta>
            <ta e="T296" id="Seg_7356" s="T295">kamən</ta>
            <ta e="T297" id="Seg_7357" s="T296">šišəge</ta>
            <ta e="T298" id="Seg_7358" s="T297">mo-laːm-bi</ta>
            <ta e="T300" id="Seg_7359" s="T299">dĭ-zAŋ</ta>
            <ta e="T301" id="Seg_7360" s="T300">bar</ta>
            <ta e="T302" id="Seg_7361" s="T301">šo-bi-jəʔ</ta>
            <ta e="T303" id="Seg_7362" s="T302">Ilʼbinʼ-Tə</ta>
            <ta e="T304" id="Seg_7363" s="T303">amno-laʔbə-jəʔ</ta>
            <ta e="T305" id="Seg_7364" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_7365" s="T305">döbər</ta>
            <ta e="T307" id="Seg_7366" s="T306">šo-bi-jəʔ</ta>
            <ta e="T308" id="Seg_7367" s="T307">bü</ta>
            <ta e="T309" id="Seg_7368" s="T308">ku-bi-jəʔ</ta>
            <ta e="T310" id="Seg_7369" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_7370" s="T310">ej</ta>
            <ta e="T312" id="Seg_7371" s="T311">kănzə-liA</ta>
            <ta e="T313" id="Seg_7372" s="T312">dön</ta>
            <ta e="T314" id="Seg_7373" s="T313">maʔ-zAŋ-də</ta>
            <ta e="T315" id="Seg_7374" s="T314">nuldə-bi-jəʔ</ta>
            <ta e="T316" id="Seg_7375" s="T315">döbər</ta>
            <ta e="T318" id="Seg_7376" s="T317">šonə-gA-jəʔ</ta>
            <ta e="T319" id="Seg_7377" s="T318">amno-zittə</ta>
            <ta e="T320" id="Seg_7378" s="T319">kamən</ta>
            <ta e="T321" id="Seg_7379" s="T320">šišəge</ta>
            <ta e="T322" id="Seg_7380" s="T321">mo-laːm-lV-j</ta>
            <ta e="T323" id="Seg_7381" s="T322">dĭ-zAŋ</ta>
            <ta e="T324" id="Seg_7382" s="T323">ej</ta>
            <ta e="T325" id="Seg_7383" s="T324">mo-laːm-bi</ta>
            <ta e="T326" id="Seg_7384" s="T325">dĭgəttə</ta>
            <ta e="T327" id="Seg_7385" s="T326">kan-ntə-gA-jəʔ</ta>
            <ta e="T328" id="Seg_7386" s="T327">dʼije-Tə</ta>
            <ta e="T329" id="Seg_7387" s="T328">vezʼdʼe</ta>
            <ta e="T330" id="Seg_7388" s="T329">bar</ta>
            <ta e="T331" id="Seg_7389" s="T330">tʼo-Kən</ta>
            <ta e="T332" id="Seg_7390" s="T331">mĭn-bi-jəʔ</ta>
            <ta e="T333" id="Seg_7391" s="T332">Dʼelam-Tə</ta>
            <ta e="T334" id="Seg_7392" s="T333">kan-bi-jəʔ</ta>
            <ta e="T335" id="Seg_7393" s="T334">dĭn</ta>
            <ta e="T336" id="Seg_7394" s="T335">bü</ta>
            <ta e="T337" id="Seg_7395" s="T336">iʔgö</ta>
            <ta e="T339" id="Seg_7396" s="T338">onʼiʔ</ta>
            <ta e="T340" id="Seg_7397" s="T339">kuza</ta>
            <ta e="T341" id="Seg_7398" s="T340">dön</ta>
            <ta e="T342" id="Seg_7399" s="T341">a</ta>
            <ta e="T343" id="Seg_7400" s="T342">onʼiʔ</ta>
            <ta e="T344" id="Seg_7401" s="T343">kuza</ta>
            <ta e="T345" id="Seg_7402" s="T344">dĭn</ta>
            <ta e="T347" id="Seg_7403" s="T346">kan-ə-ʔ</ta>
            <ta e="T348" id="Seg_7404" s="T347">dĭbər</ta>
            <ta e="T349" id="Seg_7405" s="T348">döbər</ta>
            <ta e="T350" id="Seg_7406" s="T349">dĭbər</ta>
            <ta e="T351" id="Seg_7407" s="T350">e-m</ta>
            <ta e="T352" id="Seg_7408" s="T351">kan-ə-ʔ</ta>
            <ta e="T353" id="Seg_7409" s="T352">i</ta>
            <ta e="T354" id="Seg_7410" s="T353">döbər</ta>
            <ta e="T355" id="Seg_7411" s="T354">e-m</ta>
            <ta e="T356" id="Seg_7412" s="T355">kan-ə-ʔ</ta>
            <ta e="T358" id="Seg_7413" s="T357">kădaʔ</ta>
            <ta e="T359" id="Seg_7414" s="T358">dărəʔ</ta>
            <ta e="T360" id="Seg_7415" s="T359">mo-lV-j</ta>
            <ta e="T361" id="Seg_7416" s="T360">ej</ta>
            <ta e="T362" id="Seg_7417" s="T361">kan-liA-l</ta>
            <ta e="T363" id="Seg_7418" s="T362">nʼi-m</ta>
            <ta e="T364" id="Seg_7419" s="T363">dĭ</ta>
            <ta e="T365" id="Seg_7420" s="T364">kuza</ta>
            <ta e="T366" id="Seg_7421" s="T365">ugaːndə</ta>
            <ta e="T367" id="Seg_7422" s="T366">jakšə</ta>
            <ta e="T369" id="Seg_7423" s="T368">amno-laʔbə</ta>
            <ta e="T370" id="Seg_7424" s="T369">dĭn</ta>
            <ta e="T371" id="Seg_7425" s="T370">bar</ta>
            <ta e="T372" id="Seg_7426" s="T371">ĭmbi</ta>
            <ta e="T373" id="Seg_7427" s="T372">i-gA</ta>
            <ta e="T375" id="Seg_7428" s="T374">dʼirək-jəʔ</ta>
            <ta e="T376" id="Seg_7429" s="T375">bar</ta>
            <ta e="T377" id="Seg_7430" s="T376">šo-bi-jəʔ</ta>
            <ta e="T378" id="Seg_7431" s="T377">măn</ta>
            <ta e="T379" id="Seg_7432" s="T378">šĭkə-ziʔ</ta>
            <ta e="T380" id="Seg_7433" s="T379">tʼăbaktər-zittə</ta>
            <ta e="T382" id="Seg_7434" s="T381">dĭ-zAŋ</ta>
            <ta e="T383" id="Seg_7435" s="T382">măn</ta>
            <ta e="T385" id="Seg_7436" s="T384">šĭkə-m</ta>
            <ta e="T386" id="Seg_7437" s="T385">tüšə-laʔbə-jəʔ</ta>
            <ta e="T388" id="Seg_7438" s="T387">surar-ʔ</ta>
            <ta e="T389" id="Seg_7439" s="T388">a</ta>
            <ta e="T390" id="Seg_7440" s="T389">măn</ta>
            <ta e="T391" id="Seg_7441" s="T390">nörbə-lV-m</ta>
            <ta e="T392" id="Seg_7442" s="T391">tănan</ta>
            <ta e="T393" id="Seg_7443" s="T392">tăn</ta>
            <ta e="T394" id="Seg_7444" s="T393">ĭmbi</ta>
            <ta e="T395" id="Seg_7445" s="T394">dĭ-gəʔ</ta>
            <ta e="T396" id="Seg_7446" s="T395">i-bi-l</ta>
            <ta e="T401" id="Seg_7447" s="T400">tăn</ta>
            <ta e="T402" id="Seg_7448" s="T401">ugaːndə</ta>
            <ta e="T403" id="Seg_7449" s="T402">numo</ta>
            <ta e="T405" id="Seg_7450" s="T404">šĭkə-l</ta>
            <ta e="T406" id="Seg_7451" s="T405">a</ta>
            <ta e="T407" id="Seg_7452" s="T406">măn</ta>
            <ta e="T408" id="Seg_7453" s="T407">üdʼüge</ta>
            <ta e="T409" id="Seg_7454" s="T408">šĭkə-m</ta>
            <ta e="T411" id="Seg_7455" s="T410">miʔ</ta>
            <ta e="T412" id="Seg_7456" s="T411">bar</ta>
            <ta e="T414" id="Seg_7457" s="T413">šo-bi-bAʔ</ta>
            <ta e="T416" id="Seg_7458" s="T415">aʔtʼi-Tə</ta>
            <ta e="T417" id="Seg_7459" s="T416">kuza</ta>
            <ta e="T418" id="Seg_7460" s="T417">bar</ta>
            <ta e="T419" id="Seg_7461" s="T418">šonə-gA</ta>
            <ta e="T420" id="Seg_7462" s="T419">miʔnʼibeʔ</ta>
            <ta e="T421" id="Seg_7463" s="T420">surar-ʔ</ta>
            <ta e="T422" id="Seg_7464" s="T421">aʔtʼi</ta>
            <ta e="T423" id="Seg_7465" s="T422">gibər</ta>
            <ta e="T424" id="Seg_7466" s="T423">dĭ</ta>
            <ta e="T425" id="Seg_7467" s="T424">kandə-gA</ta>
            <ta e="T426" id="Seg_7468" s="T425">ato</ta>
            <ta e="T428" id="Seg_7469" s="T426">miʔ</ta>
            <ta e="T430" id="Seg_7470" s="T429">miʔ</ta>
            <ta e="T431" id="Seg_7471" s="T430">bar</ta>
            <ta e="T432" id="Seg_7472" s="T431">ej</ta>
            <ta e="T433" id="Seg_7473" s="T432">dĭbər</ta>
            <ta e="T434" id="Seg_7474" s="T433">možet</ta>
            <ta e="T435" id="Seg_7475" s="T434">kan-bi-bAʔ</ta>
            <ta e="T437" id="Seg_7476" s="T436">tăn</ta>
            <ta e="T438" id="Seg_7477" s="T437">bar</ta>
            <ta e="T439" id="Seg_7478" s="T438">ej</ta>
            <ta e="T440" id="Seg_7479" s="T439">tʼăbaktər-liA-l</ta>
            <ta e="T441" id="Seg_7480" s="T440">a</ta>
            <ta e="T442" id="Seg_7481" s="T441">măn</ta>
            <ta e="T443" id="Seg_7482" s="T442">tʼăbaktər-liA-m</ta>
            <ta e="T444" id="Seg_7483" s="T443">tăn-ziʔ</ta>
            <ta e="T445" id="Seg_7484" s="T444">a</ta>
            <ta e="T446" id="Seg_7485" s="T445">măn</ta>
            <ta e="T447" id="Seg_7486" s="T446">ej</ta>
            <ta e="T448" id="Seg_7487" s="T447">tĭmne-m</ta>
            <ta e="T449" id="Seg_7488" s="T448">măn</ta>
            <ta e="T450" id="Seg_7489" s="T449">tĭmne-m</ta>
            <ta e="T451" id="Seg_7490" s="T450">a</ta>
            <ta e="T452" id="Seg_7491" s="T451">tʼăbaktər-zittə</ta>
            <ta e="T453" id="Seg_7492" s="T452">ej</ta>
            <ta e="T454" id="Seg_7493" s="T453">mo-liA-m</ta>
            <ta e="T455" id="Seg_7494" s="T454">nadə</ta>
            <ta e="T456" id="Seg_7495" s="T455">tăn</ta>
            <ta e="T457" id="Seg_7496" s="T456">šĭkə-l</ta>
            <ta e="T458" id="Seg_7497" s="T457">săj-nʼeʔbdə-zittə</ta>
            <ta e="T459" id="Seg_7498" s="T458">i</ta>
            <ta e="T460" id="Seg_7499" s="T459">barəʔ-zittə</ta>
            <ta e="T462" id="Seg_7500" s="T461">dĭ-zAŋ</ta>
            <ta e="T463" id="Seg_7501" s="T462">tüšəl-bi-jəʔ</ta>
            <ta e="T464" id="Seg_7502" s="T463">tüj</ta>
            <ta e="T465" id="Seg_7503" s="T464">tože</ta>
            <ta e="T466" id="Seg_7504" s="T465">nu-zAŋ</ta>
            <ta e="T467" id="Seg_7505" s="T466">mo-lV-jəʔ</ta>
            <ta e="T470" id="Seg_7506" s="T469">ugaːndə</ta>
            <ta e="T471" id="Seg_7507" s="T470">urgo</ta>
            <ta e="T472" id="Seg_7508" s="T471">beržə</ta>
            <ta e="T473" id="Seg_7509" s="T472">bar</ta>
            <ta e="T474" id="Seg_7510" s="T473">tăŋ</ta>
            <ta e="T475" id="Seg_7511" s="T474">bar</ta>
            <ta e="T476" id="Seg_7512" s="T475">kandə-gA</ta>
            <ta e="T477" id="Seg_7513" s="T476">sĭri</ta>
            <ta e="T478" id="Seg_7514" s="T477">bar</ta>
            <ta e="T479" id="Seg_7515" s="T478">kăn-laʔbə</ta>
            <ta e="T480" id="Seg_7516" s="T479">i</ta>
            <ta e="T481" id="Seg_7517" s="T480">tʼo</ta>
            <ta e="T482" id="Seg_7518" s="T481">kăn-laʔbə</ta>
            <ta e="T484" id="Seg_7519" s="T482">bar</ta>
            <ta e="T486" id="Seg_7520" s="T484">sĭri</ta>
            <ta e="T487" id="Seg_7521" s="T486">i</ta>
            <ta e="T488" id="Seg_7522" s="T487">tʼo</ta>
            <ta e="T489" id="Seg_7523" s="T488">bar</ta>
            <ta e="T491" id="Seg_7524" s="T490">bar</ta>
            <ta e="T492" id="Seg_7525" s="T491">bĭlgar-luʔbdə-bi</ta>
            <ta e="T493" id="Seg_7526" s="T492">tăn</ta>
            <ta e="T494" id="Seg_7527" s="T493">kudaj</ta>
            <ta e="T495" id="Seg_7528" s="T494">kudaj</ta>
            <ta e="T496" id="Seg_7529" s="T495">e-ʔ</ta>
            <ta e="T497" id="Seg_7530" s="T496">ma-t</ta>
            <ta e="T498" id="Seg_7531" s="T497">măna</ta>
            <ta e="T499" id="Seg_7532" s="T498">e-ʔ</ta>
            <ta e="T500" id="Seg_7533" s="T499">ma-t</ta>
            <ta e="T501" id="Seg_7534" s="T500">măna</ta>
            <ta e="T502" id="Seg_7535" s="T501">e-ʔ</ta>
            <ta e="T503" id="Seg_7536" s="T502">ma-t</ta>
            <ta e="T504" id="Seg_7537" s="T503">măna</ta>
            <ta e="T505" id="Seg_7538" s="T504">e-ʔ</ta>
            <ta e="T506" id="Seg_7539" s="T505">barəʔ-t</ta>
            <ta e="T507" id="Seg_7540" s="T506">măna</ta>
            <ta e="T508" id="Seg_7541" s="T507">tăn</ta>
            <ta e="T509" id="Seg_7542" s="T508">i-t</ta>
            <ta e="T510" id="Seg_7543" s="T509">măna</ta>
            <ta e="T511" id="Seg_7544" s="T510">sĭj-m</ta>
            <ta e="T512" id="Seg_7545" s="T511">sagəš-zəbi</ta>
            <ta e="T513" id="Seg_7546" s="T512">tăn</ta>
            <ta e="T514" id="Seg_7547" s="T513">det-ʔ</ta>
            <ta e="T515" id="Seg_7548" s="T514">măna</ta>
            <ta e="T516" id="Seg_7549" s="T515">sĭj-m</ta>
            <ta e="T517" id="Seg_7550" s="T516">sagəš</ta>
            <ta e="T518" id="Seg_7551" s="T517">iʔgö</ta>
            <ta e="T519" id="Seg_7552" s="T518">tüšə-lə-t</ta>
            <ta e="T520" id="Seg_7553" s="T519">măna</ta>
            <ta e="T521" id="Seg_7554" s="T520">maktanər-zittə</ta>
            <ta e="T522" id="Seg_7555" s="T521">tăn-ziʔ</ta>
            <ta e="T523" id="Seg_7556" s="T522">tüšə-lə-t</ta>
            <ta e="T524" id="Seg_7557" s="T523">măna</ta>
            <ta e="T525" id="Seg_7558" s="T524">tăn</ta>
            <ta e="T527" id="Seg_7559" s="T526">aʔtʼi-m</ta>
            <ta e="T528" id="Seg_7560" s="T527">mĭn-zittə</ta>
            <ta e="T530" id="Seg_7561" s="T529">tăn</ta>
            <ta e="T532" id="Seg_7562" s="T531">aʔtʼi-ziʔ-jəʔ</ta>
            <ta e="T533" id="Seg_7563" s="T532">kan-zittə</ta>
            <ta e="T538" id="Seg_7564" s="T537">Vanʼa</ta>
            <ta e="T539" id="Seg_7565" s="T538">kan-ə-ʔ</ta>
            <ta e="T540" id="Seg_7566" s="T539">kazak</ta>
            <ta e="T541" id="Seg_7567" s="T540">ija-Tə-l</ta>
            <ta e="T542" id="Seg_7568" s="T541">kaptə</ta>
            <ta e="T543" id="Seg_7569" s="T542">pi-ʔ</ta>
            <ta e="T544" id="Seg_7570" s="T543">dĭʔən</ta>
            <ta e="T545" id="Seg_7571" s="T544">ular-ə-n</ta>
            <ta e="T546" id="Seg_7572" s="T545">kaptə</ta>
            <ta e="T547" id="Seg_7573" s="T546">ular-jəʔ</ta>
            <ta e="T548" id="Seg_7574" s="T547">tor-də</ta>
            <ta e="T550" id="Seg_7575" s="T549">băt-zittə</ta>
            <ta e="T551" id="Seg_7576" s="T550">miʔnʼibeʔ</ta>
            <ta e="T552" id="Seg_7577" s="T551">kereʔ</ta>
            <ta e="T554" id="Seg_7578" s="T553">jakšə</ta>
            <ta e="T555" id="Seg_7579" s="T554">pi-t</ta>
            <ta e="T557" id="Seg_7580" s="T556">măn</ta>
            <ta e="T558" id="Seg_7581" s="T557">taldʼen</ta>
            <ta e="T559" id="Seg_7582" s="T558">pʼeːš-Tə</ta>
            <ta e="T564" id="Seg_7583" s="T563">sʼa-bi-m</ta>
            <ta e="T565" id="Seg_7584" s="T564">ej</ta>
            <ta e="T566" id="Seg_7585" s="T565">am-zittə</ta>
            <ta e="T568" id="Seg_7586" s="T567">Vanʼka</ta>
            <ta e="T569" id="Seg_7587" s="T568">aba-t-ziʔ</ta>
            <ta e="T570" id="Seg_7588" s="T569">šo-bi-jəʔ</ta>
            <ta e="T571" id="Seg_7589" s="T570">măn-ntə</ta>
            <ta e="T572" id="Seg_7590" s="T571">urgaja</ta>
            <ta e="T573" id="Seg_7591" s="T572">kutʼida</ta>
            <ta e="T574" id="Seg_7592" s="T573">girgit</ta>
            <ta e="T575" id="Seg_7593" s="T574">kola</ta>
            <ta e="T576" id="Seg_7594" s="T575">bar</ta>
            <ta e="T577" id="Seg_7595" s="T576">urgo</ta>
            <ta e="T578" id="Seg_7596" s="T577">a</ta>
            <ta e="T579" id="Seg_7597" s="T578">măn</ta>
            <ta e="T580" id="Seg_7598" s="T579">măn-bi-m</ta>
            <ta e="T581" id="Seg_7599" s="T580">kumən</ta>
            <ta e="T582" id="Seg_7600" s="T581">dʼabə-bi-lAʔ</ta>
            <ta e="T583" id="Seg_7601" s="T582">dĭ</ta>
            <ta e="T584" id="Seg_7602" s="T583">măn-ntə</ta>
            <ta e="T585" id="Seg_7603" s="T584">oʔb</ta>
            <ta e="T586" id="Seg_7604" s="T585">šide</ta>
            <ta e="T587" id="Seg_7605" s="T586">nagur</ta>
            <ta e="T588" id="Seg_7606" s="T587">teʔdə</ta>
            <ta e="T589" id="Seg_7607" s="T588">sumna</ta>
            <ta e="T590" id="Seg_7608" s="T589">muktuʔ</ta>
            <ta e="T592" id="Seg_7609" s="T591">sejʔpü</ta>
            <ta e="T593" id="Seg_7610" s="T592">dʼabə-bi-bAʔ</ta>
            <ta e="T595" id="Seg_7611" s="T594">fašist-jəʔ</ta>
            <ta e="T596" id="Seg_7612" s="T595">miʔ</ta>
            <ta e="T597" id="Seg_7613" s="T596">il</ta>
            <ta e="T598" id="Seg_7614" s="T597">üge</ta>
            <ta e="T600" id="Seg_7615" s="T599">bar</ta>
            <ta e="T601" id="Seg_7616" s="T600">tʼabəro-laʔpi-jəʔ</ta>
            <ta e="T602" id="Seg_7617" s="T601">šide-göʔ</ta>
            <ta e="T603" id="Seg_7618" s="T602">maʔ-Tə-n</ta>
            <ta e="T604" id="Seg_7619" s="T603">amno-laʔbə-bi</ta>
            <ta e="T605" id="Seg_7620" s="T604">bar</ta>
            <ta e="T606" id="Seg_7621" s="T605">Griška</ta>
            <ta e="T607" id="Seg_7622" s="T606">mĭn-bi</ta>
            <ta e="T608" id="Seg_7623" s="T607">kola</ta>
            <ta e="T609" id="Seg_7624" s="T608">dʼabə-bi</ta>
            <ta e="T610" id="Seg_7625" s="T609">da</ta>
            <ta e="T611" id="Seg_7626" s="T610">maʔ-gəndə</ta>
            <ta e="T612" id="Seg_7627" s="T611">det-bi</ta>
            <ta e="T613" id="Seg_7628" s="T612">nabə-jəʔ</ta>
            <ta e="T614" id="Seg_7629" s="T613">oʔb</ta>
            <ta e="T615" id="Seg_7630" s="T614">šide</ta>
            <ta e="T616" id="Seg_7631" s="T615">nagur</ta>
            <ta e="T617" id="Seg_7632" s="T616">det-lV-j</ta>
            <ta e="T618" id="Seg_7633" s="T617">a</ta>
            <ta e="T619" id="Seg_7634" s="T618">măn</ta>
            <ta e="T621" id="Seg_7635" s="T620">ular</ta>
            <ta e="T622" id="Seg_7636" s="T621">surdo-bi-m</ta>
            <ta e="T624" id="Seg_7637" s="T623">măn</ta>
            <ta e="T625" id="Seg_7638" s="T624">ugaːndə</ta>
            <ta e="T626" id="Seg_7639" s="T625">kălxoz-gəndə</ta>
            <ta e="T627" id="Seg_7640" s="T626">tăŋ</ta>
            <ta e="T628" id="Seg_7641" s="T627">togonər-bi-m</ta>
            <ta e="T629" id="Seg_7642" s="T628">iʔgö</ta>
            <ta e="T630" id="Seg_7643" s="T629">aš</ta>
            <ta e="T631" id="Seg_7644" s="T630">püdə-bi-m</ta>
            <ta e="T632" id="Seg_7645" s="T631">biəʔ</ta>
            <ta e="T633" id="Seg_7646" s="T632">šide</ta>
            <ta e="T634" id="Seg_7647" s="T633">püdə-bi-m</ta>
            <ta e="T635" id="Seg_7648" s="T634">ĭššo</ta>
            <ta e="T636" id="Seg_7649" s="T635">šide</ta>
            <ta e="T637" id="Seg_7650" s="T636">dĭgəttə</ta>
            <ta e="T638" id="Seg_7651" s="T637">onʼiʔ</ta>
            <ta e="T639" id="Seg_7652" s="T638">onʼiʔ</ta>
            <ta e="T640" id="Seg_7653" s="T639">tʼala</ta>
            <ta e="T642" id="Seg_7654" s="T641">nagur</ta>
            <ta e="T643" id="Seg_7655" s="T642">biəʔ</ta>
            <ta e="T644" id="Seg_7656" s="T643">püdə-bi-m</ta>
            <ta e="T645" id="Seg_7657" s="T644">i</ta>
            <ta e="T646" id="Seg_7658" s="T645">teʔdə</ta>
            <ta e="T647" id="Seg_7659" s="T646">ĭššo</ta>
            <ta e="T649" id="Seg_7660" s="T648">sotka-jəʔ</ta>
            <ta e="T650" id="Seg_7661" s="T649">măn</ta>
            <ta e="T651" id="Seg_7662" s="T650">tugan-də</ta>
            <ta e="T652" id="Seg_7663" s="T651">surar-laʔbə</ta>
            <ta e="T653" id="Seg_7664" s="T652">nadə</ta>
            <ta e="T654" id="Seg_7665" s="T653">măna</ta>
            <ta e="T655" id="Seg_7666" s="T654">nʼilgö-zittə</ta>
            <ta e="T656" id="Seg_7667" s="T655">kan-zittə</ta>
            <ta e="T657" id="Seg_7668" s="T656">šiʔnʼileʔ</ta>
            <ta e="T658" id="Seg_7669" s="T657">kădaʔ</ta>
            <ta e="T659" id="Seg_7670" s="T658">tʼăbaktər-liA-lAʔ</ta>
            <ta e="T660" id="Seg_7671" s="T659">šo-ʔ</ta>
            <ta e="T661" id="Seg_7672" s="T660">karəldʼaːn</ta>
            <ta e="T662" id="Seg_7673" s="T661">dĭ-zAŋ</ta>
            <ta e="T663" id="Seg_7674" s="T662">šo-lV-jəʔ</ta>
            <ta e="T664" id="Seg_7675" s="T663">nʼilgö-t</ta>
            <ta e="T666" id="Seg_7676" s="T665">măn</ta>
            <ta e="T667" id="Seg_7677" s="T666">bar</ta>
            <ta e="T668" id="Seg_7678" s="T667">üdʼüge</ta>
            <ta e="T669" id="Seg_7679" s="T668">kuriza-jəʔ</ta>
            <ta e="T670" id="Seg_7680" s="T669">uʔbdə-laʔbə-jəʔ</ta>
            <ta e="T671" id="Seg_7681" s="T670">bar</ta>
            <ta e="T672" id="Seg_7682" s="T671">sʼujo-laʔbə-jəʔ</ta>
            <ta e="T674" id="Seg_7683" s="T673">nʼeʔbdə-t</ta>
            <ta e="T675" id="Seg_7684" s="T674">uda-l</ta>
            <ta e="T677" id="Seg_7685" s="T676">taldʼen</ta>
            <ta e="T678" id="Seg_7686" s="T677">ne-zAŋ</ta>
            <ta e="T679" id="Seg_7687" s="T678">măna</ta>
            <ta e="T680" id="Seg_7688" s="T679">šo-bi-jəʔ</ta>
            <ta e="T681" id="Seg_7689" s="T680">süt</ta>
            <ta e="T682" id="Seg_7690" s="T681">kür-zittə</ta>
            <ta e="T683" id="Seg_7691" s="T682">iʔgö</ta>
            <ta e="T684" id="Seg_7692" s="T683">i-bi-jəʔ</ta>
            <ta e="T685" id="Seg_7693" s="T684">dĭn</ta>
            <ta e="T686" id="Seg_7694" s="T685">a</ta>
            <ta e="T688" id="Seg_7695" s="T687">predsʼedatʼelʼ-ə-n</ta>
            <ta e="T689" id="Seg_7696" s="T688">ne-t</ta>
            <ta e="T690" id="Seg_7697" s="T689">amno-bi</ta>
            <ta e="T691" id="Seg_7698" s="T690">dön</ta>
            <ta e="T692" id="Seg_7699" s="T691">nʼi-t</ta>
            <ta e="T693" id="Seg_7700" s="T692">šo-bi</ta>
            <ta e="T695" id="Seg_7701" s="T694">dĭ-Tə</ta>
            <ta e="T696" id="Seg_7702" s="T695">măn-ntə</ta>
            <ta e="T697" id="Seg_7703" s="T696">kan-žə-bəj</ta>
            <ta e="T698" id="Seg_7704" s="T697">maʔ-gənʼibAʔ</ta>
            <ta e="T699" id="Seg_7705" s="T698">miʔ</ta>
            <ta e="T700" id="Seg_7706" s="T699">Krasnojarskə-jəʔ</ta>
            <ta e="T701" id="Seg_7707" s="T700">kan-lV-bAʔ</ta>
            <ta e="T702" id="Seg_7708" s="T701">a</ta>
            <ta e="T703" id="Seg_7709" s="T702">măn</ta>
            <ta e="T704" id="Seg_7710" s="T703">măn-bi-m</ta>
            <ta e="T705" id="Seg_7711" s="T704">dĭ-Tə</ta>
            <ta e="T706" id="Seg_7712" s="T705">i-KAʔ</ta>
            <ta e="T707" id="Seg_7713" s="T706">ija-Tə-l</ta>
            <ta e="T708" id="Seg_7714" s="T707">ĭmbi=nʼibudʼ</ta>
            <ta e="T709" id="Seg_7715" s="T708">kuvas</ta>
            <ta e="T710" id="Seg_7716" s="T709">plat</ta>
            <ta e="T712" id="Seg_7717" s="T711">i-KAʔ</ta>
            <ta e="T713" id="Seg_7718" s="T712">oldʼa</ta>
            <ta e="T714" id="Seg_7719" s="T713">i-t</ta>
            <ta e="T715" id="Seg_7720" s="T714">kuvas</ta>
            <ta e="T716" id="Seg_7721" s="T715">dĭ-zAŋ</ta>
            <ta e="T717" id="Seg_7722" s="T716">bar</ta>
            <ta e="T718" id="Seg_7723" s="T717">kakənar-laʔbə-jəʔ</ta>
            <ta e="T719" id="Seg_7724" s="T718">što</ta>
            <ta e="T720" id="Seg_7725" s="T719">măn</ta>
            <ta e="T721" id="Seg_7726" s="T720">dărəʔ</ta>
            <ta e="T722" id="Seg_7727" s="T721">tʼăbaktər-laʔbə-m</ta>
            <ta e="T723" id="Seg_7728" s="T722">dĭ-zAŋ</ta>
            <ta e="T724" id="Seg_7729" s="T723">ej</ta>
            <ta e="T725" id="Seg_7730" s="T724">tĭmne-jəʔ</ta>
            <ta e="T726" id="Seg_7731" s="T725">tʼăbaktər-zittə</ta>
            <ta e="T732" id="Seg_7732" s="T731">teinen</ta>
            <ta e="T733" id="Seg_7733" s="T732">nüke</ta>
            <ta e="T734" id="Seg_7734" s="T733">šo-bi</ta>
            <ta e="T735" id="Seg_7735" s="T734">nadə</ta>
            <ta e="T736" id="Seg_7736" s="T735">măna</ta>
            <ta e="T737" id="Seg_7737" s="T736">toltano</ta>
            <ta e="T738" id="Seg_7738" s="T737">am-zittə</ta>
            <ta e="T739" id="Seg_7739" s="T738">kan-lV-m</ta>
            <ta e="T740" id="Seg_7740" s="T739">tüjö</ta>
            <ta e="T741" id="Seg_7741" s="T740">bos-də</ta>
            <ta e="T742" id="Seg_7742" s="T741">nʼi-Tə</ta>
            <ta e="T743" id="Seg_7743" s="T742">puskaj</ta>
            <ta e="T744" id="Seg_7744" s="T743">amnol-lV-j</ta>
            <ta e="T745" id="Seg_7745" s="T744">măna</ta>
            <ta e="T746" id="Seg_7746" s="T745">toltano</ta>
            <ta e="T748" id="Seg_7747" s="T747">măn</ta>
            <ta e="T749" id="Seg_7748" s="T748">i-bi-m</ta>
            <ta e="T750" id="Seg_7749" s="T749">Zaozʼorka-Kən</ta>
            <ta e="T751" id="Seg_7750" s="T750">xatʼel</ta>
            <ta e="T752" id="Seg_7751" s="T751">kan-bi-m</ta>
            <ta e="T753" id="Seg_7752" s="T752">aptʼeka-Tə</ta>
            <ta e="T754" id="Seg_7753" s="T753">sima-gəndə</ta>
            <ta e="T755" id="Seg_7754" s="T754">xatʼel</ta>
            <ta e="T756" id="Seg_7755" s="T755">i-zittə</ta>
            <ta e="T757" id="Seg_7756" s="T756">štobɨ</ta>
            <ta e="T758" id="Seg_7757" s="T757">măndo-r-zittə</ta>
            <ta e="T759" id="Seg_7758" s="T758">kuŋgə-ŋ</ta>
            <ta e="T760" id="Seg_7759" s="T759">dĭ-zAŋ</ta>
            <ta e="T761" id="Seg_7760" s="T760">ej</ta>
            <ta e="T762" id="Seg_7761" s="T761">mĭ-bi-jəʔ</ta>
            <ta e="T763" id="Seg_7762" s="T762">măndo-laʔbə-jəʔ</ta>
            <ta e="T764" id="Seg_7763" s="T763">kan-ə-ʔ</ta>
            <ta e="T765" id="Seg_7764" s="T764">sima-l</ta>
            <ta e="T766" id="Seg_7765" s="T765">măndo-r-də</ta>
            <ta e="T767" id="Seg_7766" s="T766">dĭgəttə</ta>
            <ta e="T768" id="Seg_7767" s="T767">šo-lV-l</ta>
            <ta e="T769" id="Seg_7768" s="T768">i</ta>
            <ta e="T770" id="Seg_7769" s="T769">i-lV-l</ta>
            <ta e="T772" id="Seg_7770" s="T771">dĭbər</ta>
            <ta e="T773" id="Seg_7771" s="T772">kan-bi-m</ta>
            <ta e="T774" id="Seg_7772" s="T773">dĭ-zAŋ</ta>
            <ta e="T775" id="Seg_7773" s="T774">bar</ta>
            <ta e="T776" id="Seg_7774" s="T775">tʼabəro-laʔbə</ta>
            <ta e="T777" id="Seg_7775" s="T776">ĭmbi</ta>
            <ta e="T778" id="Seg_7776" s="T777">tʼabəro-laʔbə-lAʔ</ta>
            <ta e="T779" id="Seg_7777" s="T778">e-ʔ</ta>
            <ta e="T780" id="Seg_7778" s="T779">kirgaːr-lAʔ</ta>
            <ta e="T781" id="Seg_7779" s="T780">jakšə-ŋ</ta>
            <ta e="T782" id="Seg_7780" s="T781">a-KAʔ</ta>
            <ta e="T783" id="Seg_7781" s="T782">tura</ta>
            <ta e="T784" id="Seg_7782" s="T783">sĭbrej-ə-ʔ</ta>
            <ta e="T786" id="Seg_7783" s="T785">onʼiʔ-Tə</ta>
            <ta e="T787" id="Seg_7784" s="T786">măn-bi-m</ta>
            <ta e="T788" id="Seg_7785" s="T787">kan-ə-ʔ</ta>
            <ta e="T789" id="Seg_7786" s="T788">lʼon-də</ta>
            <ta e="T790" id="Seg_7787" s="T789">toʔbdə-nar-t</ta>
            <ta e="T791" id="Seg_7788" s="T790">a</ta>
            <ta e="T792" id="Seg_7789" s="T791">dĭ</ta>
            <ta e="T793" id="Seg_7790" s="T792">pušaj</ta>
            <ta e="T794" id="Seg_7791" s="T793">bü</ta>
            <ta e="T795" id="Seg_7792" s="T794">tažor-lV-j</ta>
            <ta e="T796" id="Seg_7793" s="T795">multʼa-Tə</ta>
            <ta e="T798" id="Seg_7794" s="T797">măn</ta>
            <ta e="T799" id="Seg_7795" s="T798">teinen</ta>
            <ta e="T800" id="Seg_7796" s="T799">ertə</ta>
            <ta e="T801" id="Seg_7797" s="T800">uʔbdə-bi-m</ta>
            <ta e="T802" id="Seg_7798" s="T801">ĭššo</ta>
            <ta e="T804" id="Seg_7799" s="T803">sumna</ta>
            <ta e="T805" id="Seg_7800" s="T804">naga-bi</ta>
            <ta e="T806" id="Seg_7801" s="T805">dĭgəttə</ta>
            <ta e="T807" id="Seg_7802" s="T806">pʼeːš-bə</ta>
            <ta e="T808" id="Seg_7803" s="T807">nendə-bi-m</ta>
            <ta e="T809" id="Seg_7804" s="T808">po</ta>
            <ta e="T810" id="Seg_7805" s="T809">nuldə-bi-m</ta>
            <ta e="T811" id="Seg_7806" s="T810">dĭgəttə</ta>
            <ta e="T812" id="Seg_7807" s="T811">bazoʔ</ta>
            <ta e="T813" id="Seg_7808" s="T812">iʔbö-bi-m</ta>
            <ta e="T814" id="Seg_7809" s="T813">ĭššo</ta>
            <ta e="T815" id="Seg_7810" s="T814">kunol-bi-m</ta>
            <ta e="T816" id="Seg_7811" s="T815">ĭššo</ta>
            <ta e="T817" id="Seg_7812" s="T816">biəʔ</ta>
            <ta e="T818" id="Seg_7813" s="T817">mo-bi</ta>
            <ta e="T819" id="Seg_7814" s="T818">dĭgəttə</ta>
            <ta e="T821" id="Seg_7815" s="T820">uʔbdə-bi-m</ta>
            <ta e="T823" id="Seg_7816" s="T822">sedem</ta>
            <ta e="T824" id="Seg_7817" s="T823">teinen</ta>
            <ta e="T825" id="Seg_7818" s="T824">togonər-bi-m</ta>
            <ta e="T826" id="Seg_7819" s="T825">a</ta>
            <ta e="T827" id="Seg_7820" s="T826">taldʼen</ta>
            <ta e="T828" id="Seg_7821" s="T827">ej</ta>
            <ta e="T829" id="Seg_7822" s="T828">sedem</ta>
            <ta e="T831" id="Seg_7823" s="T830">dĭ</ta>
            <ta e="T832" id="Seg_7824" s="T831">pi</ta>
            <ta e="T833" id="Seg_7825" s="T832">ugaːndə</ta>
            <ta e="T834" id="Seg_7826" s="T833">urgo</ta>
            <ta e="T835" id="Seg_7827" s="T834">sedem</ta>
            <ta e="T836" id="Seg_7828" s="T835">a</ta>
            <ta e="T837" id="Seg_7829" s="T836">dö</ta>
            <ta e="T838" id="Seg_7830" s="T837">pa</ta>
            <ta e="T839" id="Seg_7831" s="T838">ej</ta>
            <ta e="T840" id="Seg_7832" s="T839">sedem</ta>
            <ta e="T842" id="Seg_7833" s="T841">dĭ</ta>
            <ta e="T844" id="Seg_7834" s="T843">dĭ</ta>
            <ta e="T845" id="Seg_7835" s="T844">ešši</ta>
            <ta e="T846" id="Seg_7836" s="T845">bar</ta>
            <ta e="T847" id="Seg_7837" s="T846">piʔme-zAŋ-də</ta>
            <ta e="T848" id="Seg_7838" s="T847">bar</ta>
            <ta e="T849" id="Seg_7839" s="T848">saj-nüzə-bi</ta>
            <ta e="T850" id="Seg_7840" s="T849">bar</ta>
            <ta e="T851" id="Seg_7841" s="T850">ši</ta>
            <ta e="T852" id="Seg_7842" s="T851">mo-laːm-bi</ta>
            <ta e="T853" id="Seg_7843" s="T852">nadə</ta>
            <ta e="T854" id="Seg_7844" s="T853">dĭ-m</ta>
            <ta e="T855" id="Seg_7845" s="T854">šöʔ-zittə</ta>
            <ta e="T857" id="Seg_7846" s="T856">hen-zittə</ta>
            <ta e="T859" id="Seg_7847" s="T858">edə-ʔ</ta>
            <ta e="T860" id="Seg_7848" s="T859">măna</ta>
            <ta e="T861" id="Seg_7849" s="T860">sĭreʔp</ta>
            <ta e="T862" id="Seg_7850" s="T861">sumna</ta>
            <ta e="T863" id="Seg_7851" s="T862">kilogram</ta>
            <ta e="T865" id="Seg_7852" s="T864">dĭ</ta>
            <ta e="T866" id="Seg_7853" s="T865">kuza</ta>
            <ta e="T867" id="Seg_7854" s="T866">ĭzem-bi</ta>
            <ta e="T868" id="Seg_7855" s="T867">a</ta>
            <ta e="T869" id="Seg_7856" s="T868">tüj</ta>
            <ta e="T870" id="Seg_7857" s="T869">ej</ta>
            <ta e="T871" id="Seg_7858" s="T870">ĭzem-liA</ta>
            <ta e="T872" id="Seg_7859" s="T871">uʔbdə-bi</ta>
            <ta e="T873" id="Seg_7860" s="T872">üjü-ziʔ</ta>
            <ta e="T874" id="Seg_7861" s="T873">mĭn-gA</ta>
            <ta e="T875" id="Seg_7862" s="T874">ato</ta>
            <ta e="T876" id="Seg_7863" s="T875">iʔbö-bi</ta>
            <ta e="T877" id="Seg_7864" s="T876">ej</ta>
            <ta e="T878" id="Seg_7865" s="T877">jakšə</ta>
            <ta e="T879" id="Seg_7866" s="T878">nʼilgö-laʔbə-zittə</ta>
            <ta e="T880" id="Seg_7867" s="T879">šo-bi-jəʔ</ta>
            <ta e="T881" id="Seg_7868" s="T880">da</ta>
            <ta e="T882" id="Seg_7869" s="T881">măn-bi-jəʔ</ta>
            <ta e="T883" id="Seg_7870" s="T882">ej</ta>
            <ta e="T884" id="Seg_7871" s="T883">kuvas</ta>
            <ta e="T885" id="Seg_7872" s="T884">tʼăbaktər-lAʔ</ta>
            <ta e="T887" id="Seg_7873" s="T886">dĭ</ta>
            <ta e="T888" id="Seg_7874" s="T887">dĭ</ta>
            <ta e="T889" id="Seg_7875" s="T888">bar</ta>
            <ta e="T890" id="Seg_7876" s="T889">kuza-m</ta>
            <ta e="T891" id="Seg_7877" s="T890">kut-bi</ta>
            <ta e="T892" id="Seg_7878" s="T891">dĭ-m</ta>
            <ta e="T893" id="Seg_7879" s="T892">amnol-bi-jəʔ</ta>
            <ta e="T895" id="Seg_7880" s="T894">nagur</ta>
            <ta e="T896" id="Seg_7881" s="T895">kö</ta>
            <ta e="T897" id="Seg_7882" s="T896">amno-laʔbə-bi</ta>
            <ta e="T898" id="Seg_7883" s="T897">i</ta>
            <ta e="T899" id="Seg_7884" s="T898">tüj</ta>
            <ta e="T900" id="Seg_7885" s="T899">maʔ-gəndə</ta>
            <ta e="T901" id="Seg_7886" s="T900">šo-bi</ta>
            <ta e="T902" id="Seg_7887" s="T901">i</ta>
            <ta e="T903" id="Seg_7888" s="T902">mĭn-laʔbə</ta>
            <ta e="T905" id="Seg_7889" s="T904">bar</ta>
            <ta e="T906" id="Seg_7890" s="T905">takše-jəʔ</ta>
            <ta e="T910" id="Seg_7891" s="T909">băldə-luʔbdə-bi-jəʔ</ta>
            <ta e="T913" id="Seg_7892" s="T912">măn</ta>
            <ta e="T916" id="Seg_7893" s="T915">ulu-m</ta>
            <ta e="T917" id="Seg_7894" s="T916">bar</ta>
            <ta e="T918" id="Seg_7895" s="T917">pi-ziʔ</ta>
            <ta e="T919" id="Seg_7896" s="T918">toʔbdə-nar-luʔbdə-bi-jəʔ</ta>
            <ta e="T920" id="Seg_7897" s="T919">bar</ta>
            <ta e="T921" id="Seg_7898" s="T920">bar</ta>
            <ta e="T922" id="Seg_7899" s="T921">kem</ta>
            <ta e="T923" id="Seg_7900" s="T922">mʼaŋ-laʔbə</ta>
            <ta e="T925" id="Seg_7901" s="T923">măna</ta>
            <ta e="T926" id="Seg_7902" s="T925">baltu-ziʔ</ta>
            <ta e="T927" id="Seg_7903" s="T926">bar</ta>
            <ta e="T928" id="Seg_7904" s="T927">ulu-m</ta>
            <ta e="T932" id="Seg_7905" s="T931">băldə-bi-jəʔ</ta>
            <ta e="T934" id="Seg_7906" s="T932">dʼije-Tə</ta>
            <ta e="T935" id="Seg_7907" s="T934">kan-lV-l</ta>
            <ta e="T936" id="Seg_7908" s="T935">keʔbde</ta>
            <ta e="T937" id="Seg_7909" s="T936">oʔbdə-zittə</ta>
            <ta e="T938" id="Seg_7910" s="T937">kan-žə-bAʔ</ta>
            <ta e="T939" id="Seg_7911" s="T938">maʔ-gənʼibAʔ</ta>
            <ta e="T940" id="Seg_7912" s="T939">keʔbde</ta>
            <ta e="T941" id="Seg_7913" s="T940">naga</ta>
            <ta e="T942" id="Seg_7914" s="T941">tolʼko</ta>
            <ta e="T944" id="Seg_7915" s="T943">kan-zittə</ta>
            <ta e="T945" id="Seg_7916" s="T944">gijen=də</ta>
            <ta e="T946" id="Seg_7917" s="T945">šo-lV-jəʔ</ta>
            <ta e="T947" id="Seg_7918" s="T946">ugaːndə</ta>
            <ta e="T948" id="Seg_7919" s="T947">iʔgö</ta>
            <ta e="T949" id="Seg_7920" s="T948">keʔbde</ta>
            <ta e="T950" id="Seg_7921" s="T949">dĭgəttə</ta>
            <ta e="T951" id="Seg_7922" s="T950">bazoʔ</ta>
            <ta e="T952" id="Seg_7923" s="T951">oʔbdə-liA-l</ta>
            <ta e="T953" id="Seg_7924" s="T952">dĭgəttə</ta>
            <ta e="T954" id="Seg_7925" s="T953">mo-liA-m</ta>
            <ta e="T955" id="Seg_7926" s="T954">kamən</ta>
            <ta e="T956" id="Seg_7927" s="T955">maʔ-gənʼi</ta>
            <ta e="T957" id="Seg_7928" s="T956">kan-zittə</ta>
            <ta e="T958" id="Seg_7929" s="T957">üge</ta>
            <ta e="T959" id="Seg_7930" s="T958">keʔbde</ta>
            <ta e="T960" id="Seg_7931" s="T959">iʔgö</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_7932" s="T4">I.NOM</ta>
            <ta e="T6" id="Seg_7933" s="T5">today</ta>
            <ta e="T7" id="Seg_7934" s="T6">%that.place-LAT</ta>
            <ta e="T8" id="Seg_7935" s="T7">go-PST-1SG</ta>
            <ta e="T9" id="Seg_7936" s="T8">there</ta>
            <ta e="T10" id="Seg_7937" s="T9">woman.[NOM.SG]</ta>
            <ta e="T11" id="Seg_7938" s="T10">live-DUR.[3SG]</ta>
            <ta e="T12" id="Seg_7939" s="T11">eat-EP-IMP.2SG</ta>
            <ta e="T13" id="Seg_7940" s="T12">eat-EP-IMP.2SG</ta>
            <ta e="T14" id="Seg_7941" s="T13">cabbage.[NOM.SG]</ta>
            <ta e="T15" id="Seg_7942" s="T14">oh</ta>
            <ta e="T16" id="Seg_7943" s="T15">very</ta>
            <ta e="T17" id="Seg_7944" s="T16">sour.[NOM.SG]</ta>
            <ta e="T18" id="Seg_7945" s="T17">NEG.AUX-1SG</ta>
            <ta e="T19" id="Seg_7946" s="T18">eat-EP-CNG</ta>
            <ta e="T20" id="Seg_7947" s="T19">then</ta>
            <ta e="T21" id="Seg_7948" s="T20">girl.[NOM.SG]</ta>
            <ta e="T22" id="Seg_7949" s="T21">go-PST.[3SG]</ta>
            <ta e="T23" id="Seg_7950" s="T22">shit-INF.LAT</ta>
            <ta e="T24" id="Seg_7951" s="T23">dog.[NOM.SG]</ta>
            <ta e="T25" id="Seg_7952" s="T24">there-INS</ta>
            <ta e="T26" id="Seg_7953" s="T25">I.NOM</ta>
            <ta e="T28" id="Seg_7954" s="T27">then</ta>
            <ta e="T29" id="Seg_7955" s="T28">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_7956" s="T29">come-PST.[3SG]</ta>
            <ta e="T31" id="Seg_7957" s="T30">and</ta>
            <ta e="T32" id="Seg_7958" s="T31">I.NOM</ta>
            <ta e="T33" id="Seg_7959" s="T32">this-LAT</ta>
            <ta e="T34" id="Seg_7960" s="T33">speak-PRS-1SG</ta>
            <ta e="T35" id="Seg_7961" s="T34">you.NOM</ta>
            <ta e="T36" id="Seg_7962" s="T35">go-PST-2SG</ta>
            <ta e="T37" id="Seg_7963" s="T36">shit-INF.LAT</ta>
            <ta e="T38" id="Seg_7964" s="T37">and</ta>
            <ta e="T39" id="Seg_7965" s="T38">dog.[NOM.SG]</ta>
            <ta e="T40" id="Seg_7966" s="T39">you.NOM-INS</ta>
            <ta e="T41" id="Seg_7967" s="T40">this.[NOM.SG]</ta>
            <ta e="T42" id="Seg_7968" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_7969" s="T42">laugh-MOM-PST.[3SG]</ta>
            <ta e="T44" id="Seg_7970" s="T43">very</ta>
            <ta e="T45" id="Seg_7971" s="T44">strongly</ta>
            <ta e="T46" id="Seg_7972" s="T45">rub-3PL</ta>
            <ta e="T47" id="Seg_7973" s="T46">PTCL</ta>
            <ta e="T49" id="Seg_7974" s="T48">foot-NOM/GEN/ACC.3PL</ta>
            <ta e="T50" id="Seg_7975" s="T49">hurt-DUR-3PL</ta>
            <ta e="T51" id="Seg_7976" s="T50">very</ta>
            <ta e="T53" id="Seg_7977" s="T52">many</ta>
            <ta e="T54" id="Seg_7978" s="T53">year.[NOM.SG]</ta>
            <ta e="T964" id="Seg_7979" s="T54">go-CVB</ta>
            <ta e="T55" id="Seg_7980" s="T964">disappear-PST-3PL</ta>
            <ta e="T56" id="Seg_7981" s="T55">people.[NOM.SG]</ta>
            <ta e="T57" id="Seg_7982" s="T56">PTCL</ta>
            <ta e="T58" id="Seg_7983" s="T57">what.[NOM.SG]=INDEF</ta>
            <ta e="T59" id="Seg_7984" s="T58">NEG</ta>
            <ta e="T60" id="Seg_7985" s="T59">know-3PL</ta>
            <ta e="T61" id="Seg_7986" s="T60">this-GEN.PL</ta>
            <ta e="T62" id="Seg_7987" s="T61">PTCL</ta>
            <ta e="T64" id="Seg_7988" s="T63">many</ta>
            <ta e="T65" id="Seg_7989" s="T64">horse-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_7990" s="T65">and</ta>
            <ta e="T67" id="Seg_7991" s="T66">cow-PL</ta>
            <ta e="T68" id="Seg_7992" s="T67">many</ta>
            <ta e="T69" id="Seg_7993" s="T68">and</ta>
            <ta e="T70" id="Seg_7994" s="T69">sheep.[NOM.SG]</ta>
            <ta e="T71" id="Seg_7995" s="T70">many</ta>
            <ta e="T72" id="Seg_7996" s="T71">hen-PL</ta>
            <ta e="T73" id="Seg_7997" s="T72">many</ta>
            <ta e="T74" id="Seg_7998" s="T73">meat.[NOM.SG]</ta>
            <ta e="T75" id="Seg_7999" s="T74">eat-DUR-3PL</ta>
            <ta e="T76" id="Seg_8000" s="T75">many</ta>
            <ta e="T77" id="Seg_8001" s="T76">PTCL</ta>
            <ta e="T78" id="Seg_8002" s="T77">milk.[NOM.SG]</ta>
            <ta e="T79" id="Seg_8003" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_8004" s="T79">many</ta>
            <ta e="T82" id="Seg_8005" s="T81">Kamassian-PL</ta>
            <ta e="T83" id="Seg_8006" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_8007" s="T83">live-DUR.PST-3PL</ta>
            <ta e="T85" id="Seg_8008" s="T84">God-LAT</ta>
            <ta e="T86" id="Seg_8009" s="T85">make-PST-3PL</ta>
            <ta e="T88" id="Seg_8010" s="T87">tree-INS</ta>
            <ta e="T89" id="Seg_8011" s="T88">then</ta>
            <ta e="T91" id="Seg_8012" s="T90">stone-ABL</ta>
            <ta e="T92" id="Seg_8013" s="T91">make-PST-3PL</ta>
            <ta e="T93" id="Seg_8014" s="T92">then</ta>
            <ta e="T94" id="Seg_8015" s="T93">paper-ADJZ</ta>
            <ta e="T95" id="Seg_8016" s="T94">make-PST-3PL</ta>
            <ta e="T97" id="Seg_8017" s="T96">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T98" id="Seg_8018" s="T97">God.[NOM.SG]</ta>
            <ta e="T99" id="Seg_8019" s="T98">make-PST-3PL</ta>
            <ta e="T100" id="Seg_8020" s="T99">and</ta>
            <ta e="T101" id="Seg_8021" s="T100">set.up-PST-3PL</ta>
            <ta e="T102" id="Seg_8022" s="T101">PTCL</ta>
            <ta e="T104" id="Seg_8023" s="T103">this-PL-LAT</ta>
            <ta e="T105" id="Seg_8024" s="T104">more</ta>
            <ta e="T106" id="Seg_8025" s="T105">PTCL</ta>
            <ta e="T107" id="Seg_8026" s="T106">candle-NOM/GEN/ACC.3PL</ta>
            <ta e="T108" id="Seg_8027" s="T107">set.up-PST-3PL</ta>
            <ta e="T109" id="Seg_8028" s="T108">light-PST-3PL</ta>
            <ta e="T111" id="Seg_8029" s="T110">Kamassian-PL</ta>
            <ta e="T112" id="Seg_8030" s="T111">PTCL</ta>
            <ta e="T113" id="Seg_8031" s="T112">go-PST-3PL</ta>
            <ta e="T114" id="Seg_8032" s="T113">forest-LOC</ta>
            <ta e="T115" id="Seg_8033" s="T114">and</ta>
            <ta e="T116" id="Seg_8034" s="T115">then</ta>
            <ta e="T117" id="Seg_8035" s="T116">cold.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8036" s="T117">be-PST.[3SG]</ta>
            <ta e="T119" id="Seg_8037" s="T118">so</ta>
            <ta e="T120" id="Seg_8038" s="T119">come-FUT-3PL</ta>
            <ta e="T121" id="Seg_8039" s="T120">here</ta>
            <ta e="T122" id="Seg_8040" s="T121">PTCL</ta>
            <ta e="T123" id="Seg_8041" s="T122">there</ta>
            <ta e="T124" id="Seg_8042" s="T123">old</ta>
            <ta e="T125" id="Seg_8043" s="T124">settlement</ta>
            <ta e="T126" id="Seg_8044" s="T125">live-PST-3PL</ta>
            <ta e="T127" id="Seg_8045" s="T126">then</ta>
            <ta e="T128" id="Seg_8046" s="T127">see-PST-3PL</ta>
            <ta e="T129" id="Seg_8047" s="T128">here</ta>
            <ta e="T130" id="Seg_8048" s="T129">water.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8049" s="T130">NEG</ta>
            <ta e="T132" id="Seg_8050" s="T131">freeze-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_8051" s="T132">then</ta>
            <ta e="T134" id="Seg_8052" s="T133">here</ta>
            <ta e="T138" id="Seg_8053" s="T137">place-DUR-3PL</ta>
            <ta e="T139" id="Seg_8054" s="T138">PTCL</ta>
            <ta e="T140" id="Seg_8055" s="T139">tent-INS</ta>
            <ta e="T141" id="Seg_8056" s="T140">and</ta>
            <ta e="T142" id="Seg_8057" s="T141">here</ta>
            <ta e="T143" id="Seg_8058" s="T142">live-DUR.PST-3PL</ta>
            <ta e="T144" id="Seg_8059" s="T143">this-PL</ta>
            <ta e="T145" id="Seg_8060" s="T144">PTCL</ta>
            <ta e="T146" id="Seg_8061" s="T145">go-PST-3PL</ta>
            <ta e="T150" id="Seg_8062" s="T149">road-NOM/GEN.3SG</ta>
            <ta e="T151" id="Seg_8063" s="T150">PTCL</ta>
            <ta e="T152" id="Seg_8064" s="T151">narrow</ta>
            <ta e="T153" id="Seg_8065" s="T152">be-PST.[3SG]</ta>
            <ta e="T154" id="Seg_8066" s="T153">one.[NOM.SG]</ta>
            <ta e="T155" id="Seg_8067" s="T154">walk-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_8068" s="T155">so</ta>
            <ta e="T157" id="Seg_8069" s="T156">PTCL</ta>
            <ta e="T158" id="Seg_8070" s="T157">go-IPFVZ-PRS-3PL</ta>
            <ta e="T159" id="Seg_8071" s="T158">like</ta>
            <ta e="T160" id="Seg_8072" s="T159">duck-PL</ta>
            <ta e="T161" id="Seg_8073" s="T160">forest-LAT</ta>
            <ta e="T162" id="Seg_8074" s="T161">go-PST-3PL</ta>
            <ta e="T163" id="Seg_8075" s="T162">one.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8076" s="T163">NEG</ta>
            <ta e="T165" id="Seg_8077" s="T164">go-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_8078" s="T165">and</ta>
            <ta e="T167" id="Seg_8079" s="T166">many</ta>
            <ta e="T168" id="Seg_8080" s="T167">go-FUT-3PL</ta>
            <ta e="T170" id="Seg_8081" s="T169">this-PL</ta>
            <ta e="T171" id="Seg_8082" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_8083" s="T171">forest-ABL</ta>
            <ta e="T173" id="Seg_8084" s="T172">come-PRS-3PL</ta>
            <ta e="T174" id="Seg_8085" s="T173">one.[NOM.SG]</ta>
            <ta e="T175" id="Seg_8086" s="T174">two.[NOM.SG]</ta>
            <ta e="T176" id="Seg_8087" s="T175">four.[NOM.SG]</ta>
            <ta e="T177" id="Seg_8088" s="T176">three.[NOM.SG]</ta>
            <ta e="T178" id="Seg_8089" s="T177">five.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8090" s="T178">six.[NOM.SG]</ta>
            <ta e="T180" id="Seg_8091" s="T179">one.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8092" s="T180">one-INS</ta>
            <ta e="T186" id="Seg_8093" s="T185">come-DUR-3PL</ta>
            <ta e="T187" id="Seg_8094" s="T186">water.[NOM.SG]</ta>
            <ta e="T188" id="Seg_8095" s="T187">PTCL</ta>
            <ta e="T189" id="Seg_8096" s="T188">freeze-DUR.PST.[3SG]</ta>
            <ta e="T190" id="Seg_8097" s="T189">foot-INS</ta>
            <ta e="T191" id="Seg_8098" s="T190">place-PRS-2SG</ta>
            <ta e="T192" id="Seg_8099" s="T191">stand-FUT-2SG</ta>
            <ta e="T193" id="Seg_8100" s="T192">so</ta>
            <ta e="T194" id="Seg_8101" s="T193">there</ta>
            <ta e="T195" id="Seg_8102" s="T194">NEG</ta>
            <ta e="T196" id="Seg_8103" s="T195">fall-PRS-2SG</ta>
            <ta e="T197" id="Seg_8104" s="T196">I.NOM</ta>
            <ta e="T198" id="Seg_8105" s="T197">small.[NOM.SG]</ta>
            <ta e="T199" id="Seg_8106" s="T198">be-PST-1SG</ta>
            <ta e="T200" id="Seg_8107" s="T199">I.NOM</ta>
            <ta e="T201" id="Seg_8108" s="T200">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T202" id="Seg_8109" s="T201">work-PST.[3SG]</ta>
            <ta e="T203" id="Seg_8110" s="T202">skin-PL</ta>
            <ta e="T205" id="Seg_8111" s="T204">make-PST.[3SG]</ta>
            <ta e="T206" id="Seg_8112" s="T205">fur.coat-NOM/GEN/ACC.3PL</ta>
            <ta e="T207" id="Seg_8113" s="T206">come-PST.[3SG]</ta>
            <ta e="T208" id="Seg_8114" s="T207">boot-PL</ta>
            <ta e="T209" id="Seg_8115" s="T208">sew-PST.[3SG]</ta>
            <ta e="T213" id="Seg_8116" s="T212">cap.[NOM.SG]</ta>
            <ta e="T215" id="Seg_8117" s="T214">sew-PST.[3SG]</ta>
            <ta e="T217" id="Seg_8118" s="T215">and</ta>
            <ta e="T218" id="Seg_8119" s="T217">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T219" id="Seg_8120" s="T218">PTCL</ta>
            <ta e="T220" id="Seg_8121" s="T219">dress-PST.[3SG]</ta>
            <ta e="T221" id="Seg_8122" s="T220">now</ta>
            <ta e="T222" id="Seg_8123" s="T221">I.NOM</ta>
            <ta e="T223" id="Seg_8124" s="T222">strength-NOM/GEN/ACC.1SG</ta>
            <ta e="T224" id="Seg_8125" s="T223">NEG.EX.[3SG]</ta>
            <ta e="T225" id="Seg_8126" s="T224">NEG</ta>
            <ta e="T226" id="Seg_8127" s="T225">know-1SG</ta>
            <ta e="T227" id="Seg_8128" s="T226">this.[NOM.SG]</ta>
            <ta e="T228" id="Seg_8129" s="T227">go.where-PST.[3SG]</ta>
            <ta e="T230" id="Seg_8130" s="T229">very</ta>
            <ta e="T232" id="Seg_8131" s="T231">fear-PRS-1SG</ta>
            <ta e="T233" id="Seg_8132" s="T232">PTCL</ta>
            <ta e="T234" id="Seg_8133" s="T233">I.ACC</ta>
            <ta e="T235" id="Seg_8134" s="T234">shake-DUR.[3SG]</ta>
            <ta e="T237" id="Seg_8135" s="T236">today</ta>
            <ta e="T239" id="Seg_8136" s="T238">forest-LOC</ta>
            <ta e="T240" id="Seg_8137" s="T239">spend.night-PST-2PL</ta>
            <ta e="T241" id="Seg_8138" s="T240">very</ta>
            <ta e="T242" id="Seg_8139" s="T241">cold.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8140" s="T242">be-PST.[3SG]</ta>
            <ta e="T244" id="Seg_8141" s="T243">and</ta>
            <ta e="T245" id="Seg_8142" s="T244">we.NOM</ta>
            <ta e="T246" id="Seg_8143" s="T245">very</ta>
            <ta e="T247" id="Seg_8144" s="T246">freeze-RES-PST-1PL</ta>
            <ta e="T248" id="Seg_8145" s="T247">PTCL</ta>
            <ta e="T249" id="Seg_8146" s="T248">strongly</ta>
            <ta e="T250" id="Seg_8147" s="T249">freeze-RES-PST-1PL</ta>
            <ta e="T252" id="Seg_8148" s="T251">very</ta>
            <ta e="T253" id="Seg_8149" s="T252">cold.[NOM.SG]</ta>
            <ta e="T254" id="Seg_8150" s="T253">water.[NOM.SG]</ta>
            <ta e="T255" id="Seg_8151" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8152" s="T255">freeze-RES-PST.[3SG]</ta>
            <ta e="T257" id="Seg_8153" s="T256">I.NOM</ta>
            <ta e="T258" id="Seg_8154" s="T257">foot-INS-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_8155" s="T259">stand-PST-1SG</ta>
            <ta e="T261" id="Seg_8156" s="T260">this.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8157" s="T261">NEG</ta>
            <ta e="T263" id="Seg_8158" s="T262">break-PST.[3SG]</ta>
            <ta e="T265" id="Seg_8159" s="T264">when</ta>
            <ta e="T266" id="Seg_8160" s="T265">sleep-INF.LAT</ta>
            <ta e="T267" id="Seg_8161" s="T266">lie-FUT-2SG</ta>
            <ta e="T268" id="Seg_8162" s="T267">ask-IMP.2SG</ta>
            <ta e="T269" id="Seg_8163" s="T268">let-FUT-2SG</ta>
            <ta e="T270" id="Seg_8164" s="T269">I.ACC</ta>
            <ta e="T271" id="Seg_8165" s="T270">sleep-INF.LAT</ta>
            <ta e="T272" id="Seg_8166" s="T271">here</ta>
            <ta e="T274" id="Seg_8167" s="T272">first</ta>
            <ta e="T275" id="Seg_8168" s="T274">there</ta>
            <ta e="T276" id="Seg_8169" s="T275">stand-PST-3PL</ta>
            <ta e="T277" id="Seg_8170" s="T276">Ilbin-EP-GEN</ta>
            <ta e="T278" id="Seg_8171" s="T277">edge-LAT/LOC.3SG</ta>
            <ta e="T279" id="Seg_8172" s="T278">then</ta>
            <ta e="T280" id="Seg_8173" s="T279">that.[NOM.SG]</ta>
            <ta e="T281" id="Seg_8174" s="T280">water.[NOM.SG]</ta>
            <ta e="T282" id="Seg_8175" s="T281">find-PST-3PL</ta>
            <ta e="T283" id="Seg_8176" s="T282">NEG</ta>
            <ta e="T285" id="Seg_8177" s="T284">NEG</ta>
            <ta e="T286" id="Seg_8178" s="T285">freeze-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_8179" s="T287">this-PL</ta>
            <ta e="T289" id="Seg_8180" s="T288">here</ta>
            <ta e="T290" id="Seg_8181" s="T289">tent-PL</ta>
            <ta e="T291" id="Seg_8182" s="T290">set.up-PST-3PL</ta>
            <ta e="T292" id="Seg_8183" s="T291">and</ta>
            <ta e="T293" id="Seg_8184" s="T292">here</ta>
            <ta e="T294" id="Seg_8185" s="T293">live-INF.LAT</ta>
            <ta e="T295" id="Seg_8186" s="T294">come-PST-3PL</ta>
            <ta e="T296" id="Seg_8187" s="T295">when</ta>
            <ta e="T297" id="Seg_8188" s="T296">cold.[NOM.SG]</ta>
            <ta e="T298" id="Seg_8189" s="T297">become-RES-PST.[3SG]</ta>
            <ta e="T300" id="Seg_8190" s="T299">this-PL</ta>
            <ta e="T301" id="Seg_8191" s="T300">PTCL</ta>
            <ta e="T302" id="Seg_8192" s="T301">come-PST-3PL</ta>
            <ta e="T303" id="Seg_8193" s="T302">Ilbin-LAT</ta>
            <ta e="T304" id="Seg_8194" s="T303">live-DUR-3PL</ta>
            <ta e="T305" id="Seg_8195" s="T304">then</ta>
            <ta e="T306" id="Seg_8196" s="T305">here</ta>
            <ta e="T307" id="Seg_8197" s="T306">come-PST-3PL</ta>
            <ta e="T308" id="Seg_8198" s="T307">water.[NOM.SG]</ta>
            <ta e="T309" id="Seg_8199" s="T308">find-PST-3PL</ta>
            <ta e="T310" id="Seg_8200" s="T309">this.[NOM.SG]</ta>
            <ta e="T311" id="Seg_8201" s="T310">NEG</ta>
            <ta e="T312" id="Seg_8202" s="T311">freeze-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_8203" s="T312">here</ta>
            <ta e="T314" id="Seg_8204" s="T313">tent-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T315" id="Seg_8205" s="T314">set.up-PST-3PL</ta>
            <ta e="T316" id="Seg_8206" s="T315">here</ta>
            <ta e="T318" id="Seg_8207" s="T317">come-PRS-3PL</ta>
            <ta e="T319" id="Seg_8208" s="T318">live-INF.LAT</ta>
            <ta e="T320" id="Seg_8209" s="T319">when</ta>
            <ta e="T321" id="Seg_8210" s="T320">cold.[NOM.SG]</ta>
            <ta e="T322" id="Seg_8211" s="T321">become-RES-FUT-3SG</ta>
            <ta e="T323" id="Seg_8212" s="T322">this-PL</ta>
            <ta e="T324" id="Seg_8213" s="T323">NEG</ta>
            <ta e="T325" id="Seg_8214" s="T324">become-RES-PST.[3SG]</ta>
            <ta e="T326" id="Seg_8215" s="T325">then</ta>
            <ta e="T327" id="Seg_8216" s="T326">go-IPFVZ-PRS-3PL</ta>
            <ta e="T328" id="Seg_8217" s="T327">forest-LAT</ta>
            <ta e="T329" id="Seg_8218" s="T328">everywhere</ta>
            <ta e="T330" id="Seg_8219" s="T329">PTCL</ta>
            <ta e="T331" id="Seg_8220" s="T330">earth-LOC</ta>
            <ta e="T332" id="Seg_8221" s="T331">go-PST-3PL</ta>
            <ta e="T333" id="Seg_8222" s="T332">Sayan.mountains-LAT</ta>
            <ta e="T334" id="Seg_8223" s="T333">go-PST-3PL</ta>
            <ta e="T335" id="Seg_8224" s="T334">there</ta>
            <ta e="T336" id="Seg_8225" s="T335">water.[NOM.SG]</ta>
            <ta e="T337" id="Seg_8226" s="T336">many</ta>
            <ta e="T339" id="Seg_8227" s="T338">one.[NOM.SG]</ta>
            <ta e="T340" id="Seg_8228" s="T339">man.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8229" s="T340">here</ta>
            <ta e="T342" id="Seg_8230" s="T341">and</ta>
            <ta e="T343" id="Seg_8231" s="T342">one.[NOM.SG]</ta>
            <ta e="T344" id="Seg_8232" s="T343">man.[NOM.SG]</ta>
            <ta e="T345" id="Seg_8233" s="T344">there</ta>
            <ta e="T347" id="Seg_8234" s="T346">go-EP-IMP.2SG</ta>
            <ta e="T348" id="Seg_8235" s="T347">there</ta>
            <ta e="T349" id="Seg_8236" s="T348">here</ta>
            <ta e="T350" id="Seg_8237" s="T349">there</ta>
            <ta e="T351" id="Seg_8238" s="T350">NEG.AUX-1SG</ta>
            <ta e="T352" id="Seg_8239" s="T351">go-EP-CNG</ta>
            <ta e="T353" id="Seg_8240" s="T352">and</ta>
            <ta e="T354" id="Seg_8241" s="T353">here</ta>
            <ta e="T355" id="Seg_8242" s="T354">NEG.AUX-1SG</ta>
            <ta e="T356" id="Seg_8243" s="T355">go-EP-CNG</ta>
            <ta e="T358" id="Seg_8244" s="T357">how</ta>
            <ta e="T359" id="Seg_8245" s="T358">so</ta>
            <ta e="T360" id="Seg_8246" s="T359">become-FUT-3SG</ta>
            <ta e="T361" id="Seg_8247" s="T360">NEG</ta>
            <ta e="T362" id="Seg_8248" s="T361">go-PRS-2SG</ta>
            <ta e="T363" id="Seg_8249" s="T362">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_8250" s="T363">this.[NOM.SG]</ta>
            <ta e="T365" id="Seg_8251" s="T364">man.[NOM.SG]</ta>
            <ta e="T366" id="Seg_8252" s="T365">very</ta>
            <ta e="T367" id="Seg_8253" s="T366">good.[NOM.SG]</ta>
            <ta e="T369" id="Seg_8254" s="T368">live-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_8255" s="T369">there</ta>
            <ta e="T371" id="Seg_8256" s="T370">PTCL</ta>
            <ta e="T372" id="Seg_8257" s="T371">what.[NOM.SG]</ta>
            <ta e="T373" id="Seg_8258" s="T372">be-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_8259" s="T374">settler-PL</ta>
            <ta e="T376" id="Seg_8260" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_8261" s="T376">come-PST-3PL</ta>
            <ta e="T378" id="Seg_8262" s="T377">I.NOM</ta>
            <ta e="T379" id="Seg_8263" s="T378">language-INS</ta>
            <ta e="T380" id="Seg_8264" s="T379">speak-INF.LAT</ta>
            <ta e="T382" id="Seg_8265" s="T381">this-PL</ta>
            <ta e="T383" id="Seg_8266" s="T382">I.NOM</ta>
            <ta e="T385" id="Seg_8267" s="T384">language-ACC</ta>
            <ta e="T386" id="Seg_8268" s="T385">learn-DUR-3PL</ta>
            <ta e="T388" id="Seg_8269" s="T387">ask-IMP.2SG</ta>
            <ta e="T389" id="Seg_8270" s="T388">and</ta>
            <ta e="T390" id="Seg_8271" s="T389">I.NOM</ta>
            <ta e="T391" id="Seg_8272" s="T390">tell-FUT-1SG</ta>
            <ta e="T392" id="Seg_8273" s="T391">you.DAT</ta>
            <ta e="T393" id="Seg_8274" s="T392">you.NOM</ta>
            <ta e="T394" id="Seg_8275" s="T393">what.[NOM.SG]</ta>
            <ta e="T395" id="Seg_8276" s="T394">this-ABL</ta>
            <ta e="T396" id="Seg_8277" s="T395">take-PST-2SG</ta>
            <ta e="T401" id="Seg_8278" s="T400">you.NOM</ta>
            <ta e="T402" id="Seg_8279" s="T401">very</ta>
            <ta e="T403" id="Seg_8280" s="T402">long.[NOM.SG]</ta>
            <ta e="T405" id="Seg_8281" s="T404">language-NOM/GEN/ACC.2SG</ta>
            <ta e="T406" id="Seg_8282" s="T405">and</ta>
            <ta e="T407" id="Seg_8283" s="T406">I.NOM</ta>
            <ta e="T408" id="Seg_8284" s="T407">small.[NOM.SG]</ta>
            <ta e="T409" id="Seg_8285" s="T408">language-ACC</ta>
            <ta e="T411" id="Seg_8286" s="T410">we.NOM</ta>
            <ta e="T412" id="Seg_8287" s="T411">PTCL</ta>
            <ta e="T414" id="Seg_8288" s="T413">come-PST-1PL</ta>
            <ta e="T416" id="Seg_8289" s="T415">road-LAT</ta>
            <ta e="T417" id="Seg_8290" s="T416">man.[NOM.SG]</ta>
            <ta e="T418" id="Seg_8291" s="T417">PTCL</ta>
            <ta e="T419" id="Seg_8292" s="T418">come-PRS.[3SG]</ta>
            <ta e="T420" id="Seg_8293" s="T419">we.LAT</ta>
            <ta e="T421" id="Seg_8294" s="T420">ask-IMP.2SG</ta>
            <ta e="T422" id="Seg_8295" s="T421">road.[NOM.SG]</ta>
            <ta e="T423" id="Seg_8296" s="T422">where.to</ta>
            <ta e="T424" id="Seg_8297" s="T423">this.[NOM.SG]</ta>
            <ta e="T425" id="Seg_8298" s="T424">walk-PRS.[3SG]</ta>
            <ta e="T426" id="Seg_8299" s="T425">otherwise</ta>
            <ta e="T428" id="Seg_8300" s="T426">we.NOM</ta>
            <ta e="T430" id="Seg_8301" s="T429">we.NOM</ta>
            <ta e="T431" id="Seg_8302" s="T430">PTCL</ta>
            <ta e="T432" id="Seg_8303" s="T431">NEG</ta>
            <ta e="T433" id="Seg_8304" s="T432">there</ta>
            <ta e="T434" id="Seg_8305" s="T433">maybe</ta>
            <ta e="T435" id="Seg_8306" s="T434">go-PST-1PL</ta>
            <ta e="T437" id="Seg_8307" s="T436">you.NOM</ta>
            <ta e="T438" id="Seg_8308" s="T437">PTCL</ta>
            <ta e="T439" id="Seg_8309" s="T438">NEG</ta>
            <ta e="T440" id="Seg_8310" s="T439">speak-PRS-2SG</ta>
            <ta e="T441" id="Seg_8311" s="T440">and</ta>
            <ta e="T442" id="Seg_8312" s="T441">I.NOM</ta>
            <ta e="T443" id="Seg_8313" s="T442">speak-PRS-1SG</ta>
            <ta e="T444" id="Seg_8314" s="T443">you.NOM-INS</ta>
            <ta e="T445" id="Seg_8315" s="T444">and</ta>
            <ta e="T446" id="Seg_8316" s="T445">I.NOM</ta>
            <ta e="T447" id="Seg_8317" s="T446">NEG</ta>
            <ta e="T448" id="Seg_8318" s="T447">know-1SG</ta>
            <ta e="T449" id="Seg_8319" s="T448">I.NOM</ta>
            <ta e="T450" id="Seg_8320" s="T449">know-1SG</ta>
            <ta e="T451" id="Seg_8321" s="T450">and</ta>
            <ta e="T452" id="Seg_8322" s="T451">speak-INF.LAT</ta>
            <ta e="T453" id="Seg_8323" s="T452">NEG</ta>
            <ta e="T454" id="Seg_8324" s="T453">can-PRS-1SG</ta>
            <ta e="T455" id="Seg_8325" s="T454">one.should</ta>
            <ta e="T456" id="Seg_8326" s="T455">you.NOM</ta>
            <ta e="T457" id="Seg_8327" s="T456">language-NOM/GEN/ACC.2SG</ta>
            <ta e="T458" id="Seg_8328" s="T457">off-pull-INF.LAT</ta>
            <ta e="T459" id="Seg_8329" s="T458">and</ta>
            <ta e="T460" id="Seg_8330" s="T459">throw.away-INF.LAT</ta>
            <ta e="T462" id="Seg_8331" s="T461">this-PL</ta>
            <ta e="T463" id="Seg_8332" s="T462">study-PST-3PL</ta>
            <ta e="T464" id="Seg_8333" s="T463">now</ta>
            <ta e="T465" id="Seg_8334" s="T464">also</ta>
            <ta e="T466" id="Seg_8335" s="T465">Kamassian-PL</ta>
            <ta e="T467" id="Seg_8336" s="T466">become-FUT-3PL</ta>
            <ta e="T470" id="Seg_8337" s="T469">very</ta>
            <ta e="T471" id="Seg_8338" s="T470">big.[NOM.SG]</ta>
            <ta e="T472" id="Seg_8339" s="T471">wind.[NOM.SG]</ta>
            <ta e="T473" id="Seg_8340" s="T472">PTCL</ta>
            <ta e="T474" id="Seg_8341" s="T473">strongly</ta>
            <ta e="T475" id="Seg_8342" s="T474">PTCL</ta>
            <ta e="T476" id="Seg_8343" s="T475">walk-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_8344" s="T476">snow.[NOM.SG]</ta>
            <ta e="T478" id="Seg_8345" s="T477">PTCL</ta>
            <ta e="T479" id="Seg_8346" s="T478">freeze-DUR.[3SG]</ta>
            <ta e="T480" id="Seg_8347" s="T479">and</ta>
            <ta e="T481" id="Seg_8348" s="T480">earth.[NOM.SG]</ta>
            <ta e="T482" id="Seg_8349" s="T481">freeze-DUR.[3SG]</ta>
            <ta e="T484" id="Seg_8350" s="T482">PTCL</ta>
            <ta e="T486" id="Seg_8351" s="T484">snow.[NOM.SG]</ta>
            <ta e="T487" id="Seg_8352" s="T486">and</ta>
            <ta e="T488" id="Seg_8353" s="T487">earth.[NOM.SG]</ta>
            <ta e="T489" id="Seg_8354" s="T488">PTCL</ta>
            <ta e="T491" id="Seg_8355" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_8356" s="T491">make.butter-MOM-PST.[3SG]</ta>
            <ta e="T493" id="Seg_8357" s="T492">you.NOM</ta>
            <ta e="T494" id="Seg_8358" s="T493">God.[NOM.SG]</ta>
            <ta e="T495" id="Seg_8359" s="T494">God.[NOM.SG]</ta>
            <ta e="T496" id="Seg_8360" s="T495">NEG.AUX-IMP.2SG</ta>
            <ta e="T497" id="Seg_8361" s="T496">leave-IMP.2SG.O</ta>
            <ta e="T498" id="Seg_8362" s="T497">I.ACC</ta>
            <ta e="T499" id="Seg_8363" s="T498">NEG.AUX-IMP.2SG</ta>
            <ta e="T500" id="Seg_8364" s="T499">leave-IMP.2SG.O</ta>
            <ta e="T501" id="Seg_8365" s="T500">I.ACC</ta>
            <ta e="T502" id="Seg_8366" s="T501">NEG.AUX-IMP.2SG</ta>
            <ta e="T503" id="Seg_8367" s="T502">leave-IMP.2SG.O</ta>
            <ta e="T504" id="Seg_8368" s="T503">I.ACC</ta>
            <ta e="T505" id="Seg_8369" s="T504">NEG.AUX-IMP.2SG</ta>
            <ta e="T506" id="Seg_8370" s="T505">throw.away-IMP.2SG.O</ta>
            <ta e="T507" id="Seg_8371" s="T506">I.ACC</ta>
            <ta e="T508" id="Seg_8372" s="T507">you.NOM</ta>
            <ta e="T509" id="Seg_8373" s="T508">take-IMP.2SG.O</ta>
            <ta e="T510" id="Seg_8374" s="T509">I.LAT</ta>
            <ta e="T511" id="Seg_8375" s="T510">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T512" id="Seg_8376" s="T511">mind-ADJZ.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8377" s="T512">you.NOM</ta>
            <ta e="T514" id="Seg_8378" s="T513">bring-IMP.2SG</ta>
            <ta e="T515" id="Seg_8379" s="T514">I.LAT</ta>
            <ta e="T516" id="Seg_8380" s="T515">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T517" id="Seg_8381" s="T516">mind.[NOM.SG]</ta>
            <ta e="T518" id="Seg_8382" s="T517">many</ta>
            <ta e="T519" id="Seg_8383" s="T518">learn-TR-IMP.2SG.O</ta>
            <ta e="T520" id="Seg_8384" s="T519">I.ACC</ta>
            <ta e="T521" id="Seg_8385" s="T520">praise-INF.LAT</ta>
            <ta e="T522" id="Seg_8386" s="T521">you.NOM-INS</ta>
            <ta e="T523" id="Seg_8387" s="T522">learn-TR-IMP.2SG.O</ta>
            <ta e="T524" id="Seg_8388" s="T523">I.ACC</ta>
            <ta e="T525" id="Seg_8389" s="T524">you.NOM</ta>
            <ta e="T527" id="Seg_8390" s="T526">road-ACC</ta>
            <ta e="T528" id="Seg_8391" s="T527">go-INF.LAT</ta>
            <ta e="T530" id="Seg_8392" s="T529">you.NOM</ta>
            <ta e="T532" id="Seg_8393" s="T531">road-INS-PL</ta>
            <ta e="T533" id="Seg_8394" s="T532">go-INF.LAT</ta>
            <ta e="T538" id="Seg_8395" s="T537">Vanya.[NOM.SG]</ta>
            <ta e="T539" id="Seg_8396" s="T538">go-EP-IMP.2SG</ta>
            <ta e="T540" id="Seg_8397" s="T539">Russian</ta>
            <ta e="T541" id="Seg_8398" s="T540">mother-LAT-2SG</ta>
            <ta e="T542" id="Seg_8399" s="T541">scissors.[NOM.SG]</ta>
            <ta e="T543" id="Seg_8400" s="T542">ask-IMP.2SG</ta>
            <ta e="T544" id="Seg_8401" s="T543">this.ABL</ta>
            <ta e="T545" id="Seg_8402" s="T544">sheep-EP-GEN</ta>
            <ta e="T546" id="Seg_8403" s="T545">scissors.[NOM.SG]</ta>
            <ta e="T547" id="Seg_8404" s="T546">sheep-PL</ta>
            <ta e="T548" id="Seg_8405" s="T547">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T550" id="Seg_8406" s="T549">cut-INF.LAT</ta>
            <ta e="T551" id="Seg_8407" s="T550">we.LAT</ta>
            <ta e="T552" id="Seg_8408" s="T551">one.needs</ta>
            <ta e="T554" id="Seg_8409" s="T553">good</ta>
            <ta e="T555" id="Seg_8410" s="T554">look.for-IMP.2SG.O</ta>
            <ta e="T557" id="Seg_8411" s="T556">I.NOM</ta>
            <ta e="T558" id="Seg_8412" s="T557">yesterday</ta>
            <ta e="T559" id="Seg_8413" s="T558">stove-LAT</ta>
            <ta e="T564" id="Seg_8414" s="T563">climb-PST-1SG</ta>
            <ta e="T565" id="Seg_8415" s="T564">NEG</ta>
            <ta e="T566" id="Seg_8416" s="T565">eat-INF.LAT</ta>
            <ta e="T568" id="Seg_8417" s="T567">Vanka.[NOM.SG]</ta>
            <ta e="T569" id="Seg_8418" s="T568">father-3SG-INS</ta>
            <ta e="T570" id="Seg_8419" s="T569">come-PST-3PL</ta>
            <ta e="T571" id="Seg_8420" s="T570">say-IPFVZ.[3SG]</ta>
            <ta e="T572" id="Seg_8421" s="T571">grandmother.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8422" s="T572">%%</ta>
            <ta e="T574" id="Seg_8423" s="T573">what.kind</ta>
            <ta e="T575" id="Seg_8424" s="T574">fish.[NOM.SG]</ta>
            <ta e="T576" id="Seg_8425" s="T575">PTCL</ta>
            <ta e="T577" id="Seg_8426" s="T576">big.[NOM.SG]</ta>
            <ta e="T578" id="Seg_8427" s="T577">and</ta>
            <ta e="T579" id="Seg_8428" s="T578">I.NOM</ta>
            <ta e="T580" id="Seg_8429" s="T579">say-PST-1SG</ta>
            <ta e="T581" id="Seg_8430" s="T580">how.much</ta>
            <ta e="T582" id="Seg_8431" s="T581">capture-PST-2PL</ta>
            <ta e="T583" id="Seg_8432" s="T582">this.[NOM.SG]</ta>
            <ta e="T584" id="Seg_8433" s="T583">say-IPFVZ.[3SG]</ta>
            <ta e="T585" id="Seg_8434" s="T584">one.[NOM.SG]</ta>
            <ta e="T586" id="Seg_8435" s="T585">two.[NOM.SG]</ta>
            <ta e="T587" id="Seg_8436" s="T586">three.[NOM.SG]</ta>
            <ta e="T588" id="Seg_8437" s="T587">four.[NOM.SG]</ta>
            <ta e="T589" id="Seg_8438" s="T588">five.[NOM.SG]</ta>
            <ta e="T590" id="Seg_8439" s="T589">six.[NOM.SG]</ta>
            <ta e="T592" id="Seg_8440" s="T591">seven.[NOM.SG]</ta>
            <ta e="T593" id="Seg_8441" s="T592">capture-PST-1PL</ta>
            <ta e="T595" id="Seg_8442" s="T594">fascist-PL</ta>
            <ta e="T596" id="Seg_8443" s="T595">we.NOM</ta>
            <ta e="T597" id="Seg_8444" s="T596">people.[NOM.SG]</ta>
            <ta e="T598" id="Seg_8445" s="T597">always</ta>
            <ta e="T600" id="Seg_8446" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_8447" s="T600">fight-DUR.PST-3PL</ta>
            <ta e="T602" id="Seg_8448" s="T601">two-COLL</ta>
            <ta e="T603" id="Seg_8449" s="T602">tent-LAT-%%</ta>
            <ta e="T604" id="Seg_8450" s="T603">sit-DUR-PST.[3SG]</ta>
            <ta e="T605" id="Seg_8451" s="T604">PTCL</ta>
            <ta e="T606" id="Seg_8452" s="T605">Grishka.[NOM.SG]</ta>
            <ta e="T607" id="Seg_8453" s="T606">go-PST.[3SG]</ta>
            <ta e="T608" id="Seg_8454" s="T607">fish.[NOM.SG]</ta>
            <ta e="T609" id="Seg_8455" s="T608">capture-PST.[3SG]</ta>
            <ta e="T610" id="Seg_8456" s="T609">and</ta>
            <ta e="T611" id="Seg_8457" s="T610">tent-LAT/LOC.3SG</ta>
            <ta e="T612" id="Seg_8458" s="T611">bring-PST.[3SG]</ta>
            <ta e="T613" id="Seg_8459" s="T612">duck-PL</ta>
            <ta e="T614" id="Seg_8460" s="T613">one.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8461" s="T614">two.[NOM.SG]</ta>
            <ta e="T616" id="Seg_8462" s="T615">three.[NOM.SG]</ta>
            <ta e="T617" id="Seg_8463" s="T616">bring-FUT-3SG</ta>
            <ta e="T618" id="Seg_8464" s="T617">and</ta>
            <ta e="T619" id="Seg_8465" s="T618">I.NOM</ta>
            <ta e="T621" id="Seg_8466" s="T620">sheep.[NOM.SG]</ta>
            <ta e="T622" id="Seg_8467" s="T621">milk-PST-1SG</ta>
            <ta e="T624" id="Seg_8468" s="T623">I.NOM</ta>
            <ta e="T625" id="Seg_8469" s="T624">very</ta>
            <ta e="T626" id="Seg_8470" s="T625">kolkhoz-LAT/LOC.3SG</ta>
            <ta e="T627" id="Seg_8471" s="T626">strongly</ta>
            <ta e="T628" id="Seg_8472" s="T627">work-PST-1SG</ta>
            <ta e="T629" id="Seg_8473" s="T628">many</ta>
            <ta e="T630" id="Seg_8474" s="T629">rye.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8475" s="T630">mow-PST-1SG</ta>
            <ta e="T632" id="Seg_8476" s="T631">ten.[NOM.SG]</ta>
            <ta e="T633" id="Seg_8477" s="T632">two.[NOM.SG]</ta>
            <ta e="T634" id="Seg_8478" s="T633">mow-PST-1SG</ta>
            <ta e="T635" id="Seg_8479" s="T634">more</ta>
            <ta e="T636" id="Seg_8480" s="T635">two.[NOM.SG]</ta>
            <ta e="T637" id="Seg_8481" s="T636">then</ta>
            <ta e="T638" id="Seg_8482" s="T637">one.[NOM.SG]</ta>
            <ta e="T639" id="Seg_8483" s="T638">one.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8484" s="T639">day.[NOM.SG]</ta>
            <ta e="T642" id="Seg_8485" s="T641">three.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8486" s="T642">ten.[NOM.SG]</ta>
            <ta e="T644" id="Seg_8487" s="T643">mow-PST-1SG</ta>
            <ta e="T645" id="Seg_8488" s="T644">and</ta>
            <ta e="T646" id="Seg_8489" s="T645">four.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8490" s="T646">more</ta>
            <ta e="T649" id="Seg_8491" s="T648">sotka-PL</ta>
            <ta e="T650" id="Seg_8492" s="T649">I.NOM</ta>
            <ta e="T651" id="Seg_8493" s="T650">relative-NOM/GEN/ACC.3SG</ta>
            <ta e="T652" id="Seg_8494" s="T651">ask-DUR.[3SG]</ta>
            <ta e="T653" id="Seg_8495" s="T652">one.should</ta>
            <ta e="T654" id="Seg_8496" s="T653">I.LAT</ta>
            <ta e="T655" id="Seg_8497" s="T654">listen-INF.LAT</ta>
            <ta e="T656" id="Seg_8498" s="T655">go-INF.LAT</ta>
            <ta e="T657" id="Seg_8499" s="T656">you.PL.ACC</ta>
            <ta e="T658" id="Seg_8500" s="T657">how</ta>
            <ta e="T659" id="Seg_8501" s="T658">speak-PRS-2PL</ta>
            <ta e="T660" id="Seg_8502" s="T659">come-IMP.2SG</ta>
            <ta e="T661" id="Seg_8503" s="T660">tomorrow</ta>
            <ta e="T662" id="Seg_8504" s="T661">this-PL</ta>
            <ta e="T663" id="Seg_8505" s="T662">come-FUT-3PL</ta>
            <ta e="T664" id="Seg_8506" s="T663">listen-IMP.2SG.O</ta>
            <ta e="T666" id="Seg_8507" s="T665">I.NOM</ta>
            <ta e="T667" id="Seg_8508" s="T666">PTCL</ta>
            <ta e="T668" id="Seg_8509" s="T667">small.[NOM.SG]</ta>
            <ta e="T669" id="Seg_8510" s="T668">hen-PL</ta>
            <ta e="T670" id="Seg_8511" s="T669">get.up-DUR-3PL</ta>
            <ta e="T671" id="Seg_8512" s="T670">PTCL</ta>
            <ta e="T672" id="Seg_8513" s="T671">pee-DUR-3PL</ta>
            <ta e="T674" id="Seg_8514" s="T673">pull-IMP.2SG.O</ta>
            <ta e="T675" id="Seg_8515" s="T674">hand-NOM/GEN/ACC.2SG</ta>
            <ta e="T677" id="Seg_8516" s="T676">yesterday</ta>
            <ta e="T678" id="Seg_8517" s="T677">woman-PL</ta>
            <ta e="T679" id="Seg_8518" s="T678">I.LAT</ta>
            <ta e="T680" id="Seg_8519" s="T679">come-PST-3PL</ta>
            <ta e="T681" id="Seg_8520" s="T680">milk.[NOM.SG]</ta>
            <ta e="T682" id="Seg_8521" s="T681">peel.bark-INF.LAT</ta>
            <ta e="T683" id="Seg_8522" s="T682">many</ta>
            <ta e="T684" id="Seg_8523" s="T683">be-PST-3PL</ta>
            <ta e="T685" id="Seg_8524" s="T684">there</ta>
            <ta e="T686" id="Seg_8525" s="T685">and</ta>
            <ta e="T688" id="Seg_8526" s="T687">chairman-EP-GEN</ta>
            <ta e="T689" id="Seg_8527" s="T688">woman-NOM/GEN.3SG</ta>
            <ta e="T690" id="Seg_8528" s="T689">sit-PST.[3SG]</ta>
            <ta e="T691" id="Seg_8529" s="T690">here</ta>
            <ta e="T692" id="Seg_8530" s="T691">son-NOM/GEN.3SG</ta>
            <ta e="T693" id="Seg_8531" s="T692">come-PST.[3SG]</ta>
            <ta e="T695" id="Seg_8532" s="T694">this-LAT</ta>
            <ta e="T696" id="Seg_8533" s="T695">say-IPFVZ.[3SG]</ta>
            <ta e="T697" id="Seg_8534" s="T696">go-OPT.DU/PL-1DU</ta>
            <ta e="T698" id="Seg_8535" s="T697">tent-LAT/LOC.1PL</ta>
            <ta e="T699" id="Seg_8536" s="T698">we.NOM</ta>
            <ta e="T700" id="Seg_8537" s="T699">Krasnoyarsk-NOM/GEN/ACC.3PL</ta>
            <ta e="T701" id="Seg_8538" s="T700">go-FUT-1PL</ta>
            <ta e="T702" id="Seg_8539" s="T701">and</ta>
            <ta e="T703" id="Seg_8540" s="T702">I.NOM</ta>
            <ta e="T704" id="Seg_8541" s="T703">say-PST-1SG</ta>
            <ta e="T705" id="Seg_8542" s="T704">this-LAT</ta>
            <ta e="T706" id="Seg_8543" s="T705">take-IMP.2PL</ta>
            <ta e="T707" id="Seg_8544" s="T706">mother-LAT-2SG</ta>
            <ta e="T708" id="Seg_8545" s="T707">what=INDEF</ta>
            <ta e="T709" id="Seg_8546" s="T708">beautiful.[NOM.SG]</ta>
            <ta e="T710" id="Seg_8547" s="T709">scarf.[NOM.SG]</ta>
            <ta e="T712" id="Seg_8548" s="T711">take-IMP.2PL</ta>
            <ta e="T713" id="Seg_8549" s="T712">clothing.[NOM.SG]</ta>
            <ta e="T714" id="Seg_8550" s="T713">take-IMP.2SG.O</ta>
            <ta e="T715" id="Seg_8551" s="T714">beautiful.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8552" s="T715">this-PL</ta>
            <ta e="T717" id="Seg_8553" s="T716">PTCL</ta>
            <ta e="T718" id="Seg_8554" s="T717">laugh-DUR-3PL</ta>
            <ta e="T719" id="Seg_8555" s="T718">that</ta>
            <ta e="T720" id="Seg_8556" s="T719">I.NOM</ta>
            <ta e="T721" id="Seg_8557" s="T720">so</ta>
            <ta e="T722" id="Seg_8558" s="T721">speak-DUR-1SG</ta>
            <ta e="T723" id="Seg_8559" s="T722">this-PL</ta>
            <ta e="T724" id="Seg_8560" s="T723">NEG</ta>
            <ta e="T725" id="Seg_8561" s="T724">know-3PL</ta>
            <ta e="T726" id="Seg_8562" s="T725">speak-INF.LAT</ta>
            <ta e="T732" id="Seg_8563" s="T731">today</ta>
            <ta e="T733" id="Seg_8564" s="T732">woman.[NOM.SG]</ta>
            <ta e="T734" id="Seg_8565" s="T733">come-PST.[3SG]</ta>
            <ta e="T735" id="Seg_8566" s="T734">one.should</ta>
            <ta e="T736" id="Seg_8567" s="T735">I.LAT</ta>
            <ta e="T737" id="Seg_8568" s="T736">potato.[NOM.SG]</ta>
            <ta e="T738" id="Seg_8569" s="T737">eat-INF.LAT</ta>
            <ta e="T739" id="Seg_8570" s="T738">go-FUT-1SG</ta>
            <ta e="T740" id="Seg_8571" s="T739">soon</ta>
            <ta e="T741" id="Seg_8572" s="T740">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T742" id="Seg_8573" s="T741">son-LAT</ta>
            <ta e="T743" id="Seg_8574" s="T742">JUSS</ta>
            <ta e="T744" id="Seg_8575" s="T743">seat-FUT-3SG</ta>
            <ta e="T745" id="Seg_8576" s="T744">I.LAT</ta>
            <ta e="T746" id="Seg_8577" s="T745">potato.[NOM.SG]</ta>
            <ta e="T748" id="Seg_8578" s="T747">I.NOM</ta>
            <ta e="T749" id="Seg_8579" s="T748">be-PST-1SG</ta>
            <ta e="T750" id="Seg_8580" s="T749">Zaozerka-LOC</ta>
            <ta e="T751" id="Seg_8581" s="T750">want.PST.M.SG</ta>
            <ta e="T752" id="Seg_8582" s="T751">go-PST-1SG</ta>
            <ta e="T753" id="Seg_8583" s="T752">pharmacy-LAT</ta>
            <ta e="T754" id="Seg_8584" s="T753">eye-LAT/LOC.3SG</ta>
            <ta e="T755" id="Seg_8585" s="T754">want.PST.M.SG</ta>
            <ta e="T756" id="Seg_8586" s="T755">take-INF.LAT</ta>
            <ta e="T757" id="Seg_8587" s="T756">so.that</ta>
            <ta e="T758" id="Seg_8588" s="T757">look-FRQ-INF.LAT</ta>
            <ta e="T759" id="Seg_8589" s="T758">far-LAT.ADV</ta>
            <ta e="T760" id="Seg_8590" s="T759">this-PL</ta>
            <ta e="T761" id="Seg_8591" s="T760">NEG</ta>
            <ta e="T762" id="Seg_8592" s="T761">give-PST-3PL</ta>
            <ta e="T763" id="Seg_8593" s="T762">look-DUR-3PL</ta>
            <ta e="T764" id="Seg_8594" s="T763">go-EP-IMP.2SG</ta>
            <ta e="T765" id="Seg_8595" s="T764">eye-NOM/GEN/ACC.2SG</ta>
            <ta e="T766" id="Seg_8596" s="T765">look-FRQ-TR</ta>
            <ta e="T767" id="Seg_8597" s="T766">then</ta>
            <ta e="T768" id="Seg_8598" s="T767">come-FUT-2SG</ta>
            <ta e="T769" id="Seg_8599" s="T768">and</ta>
            <ta e="T770" id="Seg_8600" s="T769">take-FUT-2SG</ta>
            <ta e="T772" id="Seg_8601" s="T771">there</ta>
            <ta e="T773" id="Seg_8602" s="T772">go-PST-1SG</ta>
            <ta e="T774" id="Seg_8603" s="T773">this-PL</ta>
            <ta e="T775" id="Seg_8604" s="T774">PTCL</ta>
            <ta e="T776" id="Seg_8605" s="T775">fight-DUR.[3SG]</ta>
            <ta e="T777" id="Seg_8606" s="T776">what.[NOM.SG]</ta>
            <ta e="T778" id="Seg_8607" s="T777">fight-DUR-2PL</ta>
            <ta e="T779" id="Seg_8608" s="T778">NEG.AUX-IMP.2SG</ta>
            <ta e="T780" id="Seg_8609" s="T779">shout-2PL</ta>
            <ta e="T781" id="Seg_8610" s="T780">good-LAT.ADV</ta>
            <ta e="T782" id="Seg_8611" s="T781">make-IMP.2PL</ta>
            <ta e="T783" id="Seg_8612" s="T782">house.[NOM.SG]</ta>
            <ta e="T784" id="Seg_8613" s="T783">clean-EP-IMP.2SG</ta>
            <ta e="T786" id="Seg_8614" s="T785">one-LAT</ta>
            <ta e="T787" id="Seg_8615" s="T786">say-PST-1SG</ta>
            <ta e="T788" id="Seg_8616" s="T787">go-EP-IMP.2SG</ta>
            <ta e="T789" id="Seg_8617" s="T788">flax-NOM/GEN/ACC.3SG</ta>
            <ta e="T790" id="Seg_8618" s="T789">hit-MULT-IMP.2SG.O</ta>
            <ta e="T791" id="Seg_8619" s="T790">and</ta>
            <ta e="T792" id="Seg_8620" s="T791">this.[NOM.SG]</ta>
            <ta e="T793" id="Seg_8621" s="T792">JUSS</ta>
            <ta e="T794" id="Seg_8622" s="T793">water.[NOM.SG]</ta>
            <ta e="T795" id="Seg_8623" s="T794">carry-FUT-3SG</ta>
            <ta e="T796" id="Seg_8624" s="T795">sauna-LAT</ta>
            <ta e="T798" id="Seg_8625" s="T797">I.NOM</ta>
            <ta e="T799" id="Seg_8626" s="T798">today</ta>
            <ta e="T800" id="Seg_8627" s="T799">early</ta>
            <ta e="T801" id="Seg_8628" s="T800">get.up-PST-1SG</ta>
            <ta e="T802" id="Seg_8629" s="T801">more</ta>
            <ta e="T804" id="Seg_8630" s="T803">five.[NOM.SG]</ta>
            <ta e="T805" id="Seg_8631" s="T804">NEG.EX-PST.[3SG]</ta>
            <ta e="T806" id="Seg_8632" s="T805">then</ta>
            <ta e="T807" id="Seg_8633" s="T806">stove-ACC.3SG</ta>
            <ta e="T808" id="Seg_8634" s="T807">light-PST-1SG</ta>
            <ta e="T809" id="Seg_8635" s="T808">%%</ta>
            <ta e="T810" id="Seg_8636" s="T809">place-PST-1SG</ta>
            <ta e="T811" id="Seg_8637" s="T810">then</ta>
            <ta e="T812" id="Seg_8638" s="T811">again</ta>
            <ta e="T813" id="Seg_8639" s="T812">lie-PST-1SG</ta>
            <ta e="T814" id="Seg_8640" s="T813">more</ta>
            <ta e="T815" id="Seg_8641" s="T814">sleep-PST-1SG</ta>
            <ta e="T816" id="Seg_8642" s="T815">more</ta>
            <ta e="T817" id="Seg_8643" s="T816">ten.[NOM.SG]</ta>
            <ta e="T818" id="Seg_8644" s="T817">can-PST.[3SG]</ta>
            <ta e="T819" id="Seg_8645" s="T818">then</ta>
            <ta e="T821" id="Seg_8646" s="T820">get.up-PST-1SG</ta>
            <ta e="T823" id="Seg_8647" s="T822">hard</ta>
            <ta e="T824" id="Seg_8648" s="T823">today</ta>
            <ta e="T825" id="Seg_8649" s="T824">work-PST-1SG</ta>
            <ta e="T826" id="Seg_8650" s="T825">and</ta>
            <ta e="T827" id="Seg_8651" s="T826">yesterday</ta>
            <ta e="T828" id="Seg_8652" s="T827">NEG</ta>
            <ta e="T829" id="Seg_8653" s="T828">hard</ta>
            <ta e="T831" id="Seg_8654" s="T830">this.[NOM.SG]</ta>
            <ta e="T832" id="Seg_8655" s="T831">stone.[NOM.SG]</ta>
            <ta e="T833" id="Seg_8656" s="T832">very</ta>
            <ta e="T834" id="Seg_8657" s="T833">big.[NOM.SG]</ta>
            <ta e="T835" id="Seg_8658" s="T834">hard</ta>
            <ta e="T836" id="Seg_8659" s="T835">and</ta>
            <ta e="T837" id="Seg_8660" s="T836">that.[NOM.SG]</ta>
            <ta e="T838" id="Seg_8661" s="T837">tree.[NOM.SG]</ta>
            <ta e="T839" id="Seg_8662" s="T838">NEG</ta>
            <ta e="T840" id="Seg_8663" s="T839">hard</ta>
            <ta e="T842" id="Seg_8664" s="T841">this</ta>
            <ta e="T844" id="Seg_8665" s="T843">this</ta>
            <ta e="T845" id="Seg_8666" s="T844">child.[NOM.SG]</ta>
            <ta e="T846" id="Seg_8667" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_8668" s="T846">pants-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T848" id="Seg_8669" s="T847">PTCL</ta>
            <ta e="T849" id="Seg_8670" s="T848">off-tear-PST</ta>
            <ta e="T850" id="Seg_8671" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_8672" s="T850">hole.[NOM.SG]</ta>
            <ta e="T852" id="Seg_8673" s="T851">become-RES-PST.[3SG]</ta>
            <ta e="T853" id="Seg_8674" s="T852">one.should</ta>
            <ta e="T854" id="Seg_8675" s="T853">this-ACC</ta>
            <ta e="T855" id="Seg_8676" s="T854">sew-INF.LAT</ta>
            <ta e="T857" id="Seg_8677" s="T856">put-INF.LAT</ta>
            <ta e="T859" id="Seg_8678" s="T858">hang.up-IMP.2SG</ta>
            <ta e="T860" id="Seg_8679" s="T859">I.LAT</ta>
            <ta e="T861" id="Seg_8680" s="T860">sugar.[NOM.SG]</ta>
            <ta e="T862" id="Seg_8681" s="T861">five.[NOM.SG]</ta>
            <ta e="T863" id="Seg_8682" s="T862">kilogramm.[NOM.SG]</ta>
            <ta e="T865" id="Seg_8683" s="T864">this.[NOM.SG]</ta>
            <ta e="T866" id="Seg_8684" s="T865">man.[NOM.SG]</ta>
            <ta e="T867" id="Seg_8685" s="T866">hurt-PST.[3SG]</ta>
            <ta e="T868" id="Seg_8686" s="T867">and</ta>
            <ta e="T869" id="Seg_8687" s="T868">now</ta>
            <ta e="T870" id="Seg_8688" s="T869">NEG</ta>
            <ta e="T871" id="Seg_8689" s="T870">hurt-PRS.[3SG]</ta>
            <ta e="T872" id="Seg_8690" s="T871">get.up-PST.[3SG]</ta>
            <ta e="T873" id="Seg_8691" s="T872">foot-INS</ta>
            <ta e="T874" id="Seg_8692" s="T873">go-PRS.[3SG]</ta>
            <ta e="T875" id="Seg_8693" s="T874">otherwise</ta>
            <ta e="T876" id="Seg_8694" s="T875">lie-PST.[3SG]</ta>
            <ta e="T877" id="Seg_8695" s="T876">NEG</ta>
            <ta e="T878" id="Seg_8696" s="T877">good.[NOM.SG]</ta>
            <ta e="T879" id="Seg_8697" s="T878">listen-DUR-INF.LAT</ta>
            <ta e="T880" id="Seg_8698" s="T879">come-PST-3PL</ta>
            <ta e="T881" id="Seg_8699" s="T880">and</ta>
            <ta e="T882" id="Seg_8700" s="T881">say-PST-3PL</ta>
            <ta e="T883" id="Seg_8701" s="T882">NEG</ta>
            <ta e="T884" id="Seg_8702" s="T883">beautiful.[NOM.SG]</ta>
            <ta e="T885" id="Seg_8703" s="T884">speak-CVB</ta>
            <ta e="T887" id="Seg_8704" s="T886">this</ta>
            <ta e="T888" id="Seg_8705" s="T887">this.[NOM.SG]</ta>
            <ta e="T889" id="Seg_8706" s="T888">PTCL</ta>
            <ta e="T890" id="Seg_8707" s="T889">man-ACC</ta>
            <ta e="T891" id="Seg_8708" s="T890">kill-PST.[3SG]</ta>
            <ta e="T892" id="Seg_8709" s="T891">this-ACC</ta>
            <ta e="T893" id="Seg_8710" s="T892">seat-PST-3PL</ta>
            <ta e="T895" id="Seg_8711" s="T894">three.[NOM.SG]</ta>
            <ta e="T896" id="Seg_8712" s="T895">winter.[NOM.SG]</ta>
            <ta e="T897" id="Seg_8713" s="T896">live-DUR-PST.[3SG]</ta>
            <ta e="T898" id="Seg_8714" s="T897">and</ta>
            <ta e="T899" id="Seg_8715" s="T898">now</ta>
            <ta e="T900" id="Seg_8716" s="T899">tent-LAT/LOC.3SG</ta>
            <ta e="T901" id="Seg_8717" s="T900">come-PST.[3SG]</ta>
            <ta e="T902" id="Seg_8718" s="T901">and</ta>
            <ta e="T903" id="Seg_8719" s="T902">go-DUR.[3SG]</ta>
            <ta e="T905" id="Seg_8720" s="T904">PTCL</ta>
            <ta e="T906" id="Seg_8721" s="T905">cup-NOM/GEN/ACC.3PL</ta>
            <ta e="T910" id="Seg_8722" s="T909">break-MOM-PST-3PL</ta>
            <ta e="T913" id="Seg_8723" s="T912">I.NOM</ta>
            <ta e="T916" id="Seg_8724" s="T915">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T917" id="Seg_8725" s="T916">PTCL</ta>
            <ta e="T918" id="Seg_8726" s="T917">stone-INS</ta>
            <ta e="T919" id="Seg_8727" s="T918">hit-MULT-MOM-PST-3PL</ta>
            <ta e="T920" id="Seg_8728" s="T919">PTCL</ta>
            <ta e="T921" id="Seg_8729" s="T920">PTCL</ta>
            <ta e="T922" id="Seg_8730" s="T921">blood.[NOM.SG]</ta>
            <ta e="T923" id="Seg_8731" s="T922">flow-DUR.[3SG]</ta>
            <ta e="T925" id="Seg_8732" s="T923">I.LAT</ta>
            <ta e="T926" id="Seg_8733" s="T925">axe-INS</ta>
            <ta e="T927" id="Seg_8734" s="T926">PTCL</ta>
            <ta e="T928" id="Seg_8735" s="T927">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T932" id="Seg_8736" s="T931">break-PST-3PL</ta>
            <ta e="T934" id="Seg_8737" s="T932">forest-LAT</ta>
            <ta e="T935" id="Seg_8738" s="T934">go-FUT-2SG</ta>
            <ta e="T936" id="Seg_8739" s="T935">berry.[NOM.SG]</ta>
            <ta e="T937" id="Seg_8740" s="T936">collect-INF.LAT</ta>
            <ta e="T938" id="Seg_8741" s="T937">go-OPT.DU/PL-1PL</ta>
            <ta e="T939" id="Seg_8742" s="T938">tent-LAT/LOC.1PL</ta>
            <ta e="T940" id="Seg_8743" s="T939">berry.[NOM.SG]</ta>
            <ta e="T941" id="Seg_8744" s="T940">NEG.EX.[3SG]</ta>
            <ta e="T942" id="Seg_8745" s="T941">only</ta>
            <ta e="T944" id="Seg_8746" s="T943">go-INF.LAT</ta>
            <ta e="T945" id="Seg_8747" s="T944">where=INDEF</ta>
            <ta e="T946" id="Seg_8748" s="T945">come-FUT-3PL</ta>
            <ta e="T947" id="Seg_8749" s="T946">very</ta>
            <ta e="T948" id="Seg_8750" s="T947">many</ta>
            <ta e="T949" id="Seg_8751" s="T948">berry.[NOM.SG]</ta>
            <ta e="T950" id="Seg_8752" s="T949">then</ta>
            <ta e="T951" id="Seg_8753" s="T950">again</ta>
            <ta e="T952" id="Seg_8754" s="T951">collect-PRS-2SG</ta>
            <ta e="T953" id="Seg_8755" s="T952">then</ta>
            <ta e="T954" id="Seg_8756" s="T953">can-PRS-1SG</ta>
            <ta e="T955" id="Seg_8757" s="T954">when</ta>
            <ta e="T956" id="Seg_8758" s="T955">tent-LAT/LOC.1SG</ta>
            <ta e="T957" id="Seg_8759" s="T956">go-INF.LAT</ta>
            <ta e="T958" id="Seg_8760" s="T957">always</ta>
            <ta e="T959" id="Seg_8761" s="T958">berry.[NOM.SG]</ta>
            <ta e="T960" id="Seg_8762" s="T959">many</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_8763" s="T4">я.NOM</ta>
            <ta e="T6" id="Seg_8764" s="T5">сегодня</ta>
            <ta e="T7" id="Seg_8765" s="T6">%то.место-LAT</ta>
            <ta e="T8" id="Seg_8766" s="T7">идти-PST-1SG</ta>
            <ta e="T9" id="Seg_8767" s="T8">там</ta>
            <ta e="T10" id="Seg_8768" s="T9">женщина.[NOM.SG]</ta>
            <ta e="T11" id="Seg_8769" s="T10">жить-DUR.[3SG]</ta>
            <ta e="T12" id="Seg_8770" s="T11">съесть-EP-IMP.2SG</ta>
            <ta e="T13" id="Seg_8771" s="T12">есть-EP-IMP.2SG</ta>
            <ta e="T14" id="Seg_8772" s="T13">капуста.[NOM.SG]</ta>
            <ta e="T15" id="Seg_8773" s="T14">о</ta>
            <ta e="T16" id="Seg_8774" s="T15">очень</ta>
            <ta e="T17" id="Seg_8775" s="T16">кислый.[NOM.SG]</ta>
            <ta e="T18" id="Seg_8776" s="T17">NEG.AUX-1SG</ta>
            <ta e="T19" id="Seg_8777" s="T18">съесть-EP-CNG</ta>
            <ta e="T20" id="Seg_8778" s="T19">тогда</ta>
            <ta e="T21" id="Seg_8779" s="T20">девушка.[NOM.SG]</ta>
            <ta e="T22" id="Seg_8780" s="T21">пойти-PST.[3SG]</ta>
            <ta e="T23" id="Seg_8781" s="T22">испражняться-INF.LAT</ta>
            <ta e="T24" id="Seg_8782" s="T23">собака.[NOM.SG]</ta>
            <ta e="T25" id="Seg_8783" s="T24">там-INS</ta>
            <ta e="T26" id="Seg_8784" s="T25">я.NOM</ta>
            <ta e="T28" id="Seg_8785" s="T27">тогда</ta>
            <ta e="T29" id="Seg_8786" s="T28">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_8787" s="T29">прийти-PST.[3SG]</ta>
            <ta e="T31" id="Seg_8788" s="T30">а</ta>
            <ta e="T32" id="Seg_8789" s="T31">я.NOM</ta>
            <ta e="T33" id="Seg_8790" s="T32">этот-LAT</ta>
            <ta e="T34" id="Seg_8791" s="T33">говорить-PRS-1SG</ta>
            <ta e="T35" id="Seg_8792" s="T34">ты.NOM</ta>
            <ta e="T36" id="Seg_8793" s="T35">идти-PST-2SG</ta>
            <ta e="T37" id="Seg_8794" s="T36">испражняться-INF.LAT</ta>
            <ta e="T38" id="Seg_8795" s="T37">и</ta>
            <ta e="T39" id="Seg_8796" s="T38">собака.[NOM.SG]</ta>
            <ta e="T40" id="Seg_8797" s="T39">ты.NOM-INS</ta>
            <ta e="T41" id="Seg_8798" s="T40">этот.[NOM.SG]</ta>
            <ta e="T42" id="Seg_8799" s="T41">PTCL</ta>
            <ta e="T43" id="Seg_8800" s="T42">смеяться-MOM-PST.[3SG]</ta>
            <ta e="T44" id="Seg_8801" s="T43">очень</ta>
            <ta e="T45" id="Seg_8802" s="T44">сильно</ta>
            <ta e="T46" id="Seg_8803" s="T45">тереть-3PL</ta>
            <ta e="T47" id="Seg_8804" s="T46">PTCL</ta>
            <ta e="T49" id="Seg_8805" s="T48">нога-NOM/GEN/ACC.3PL</ta>
            <ta e="T50" id="Seg_8806" s="T49">болеть-DUR-3PL</ta>
            <ta e="T51" id="Seg_8807" s="T50">очень</ta>
            <ta e="T53" id="Seg_8808" s="T52">много</ta>
            <ta e="T54" id="Seg_8809" s="T53">год.[NOM.SG]</ta>
            <ta e="T964" id="Seg_8810" s="T54">пойти-CVB</ta>
            <ta e="T55" id="Seg_8811" s="T964">исчезнуть-PST-3PL</ta>
            <ta e="T56" id="Seg_8812" s="T55">люди.[NOM.SG]</ta>
            <ta e="T57" id="Seg_8813" s="T56">PTCL</ta>
            <ta e="T58" id="Seg_8814" s="T57">что.[NOM.SG]=INDEF</ta>
            <ta e="T59" id="Seg_8815" s="T58">NEG</ta>
            <ta e="T60" id="Seg_8816" s="T59">знать-3PL</ta>
            <ta e="T61" id="Seg_8817" s="T60">этот-GEN.PL</ta>
            <ta e="T62" id="Seg_8818" s="T61">PTCL</ta>
            <ta e="T64" id="Seg_8819" s="T63">много</ta>
            <ta e="T65" id="Seg_8820" s="T64">лошадь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_8821" s="T65">и</ta>
            <ta e="T67" id="Seg_8822" s="T66">корова-PL</ta>
            <ta e="T68" id="Seg_8823" s="T67">много</ta>
            <ta e="T69" id="Seg_8824" s="T68">и</ta>
            <ta e="T70" id="Seg_8825" s="T69">овца.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8826" s="T70">много</ta>
            <ta e="T72" id="Seg_8827" s="T71">курица-PL</ta>
            <ta e="T73" id="Seg_8828" s="T72">много</ta>
            <ta e="T74" id="Seg_8829" s="T73">мясо.[NOM.SG]</ta>
            <ta e="T75" id="Seg_8830" s="T74">съесть-DUR-3PL</ta>
            <ta e="T76" id="Seg_8831" s="T75">много</ta>
            <ta e="T77" id="Seg_8832" s="T76">PTCL</ta>
            <ta e="T78" id="Seg_8833" s="T77">молоко.[NOM.SG]</ta>
            <ta e="T79" id="Seg_8834" s="T78">PTCL</ta>
            <ta e="T80" id="Seg_8835" s="T79">много</ta>
            <ta e="T82" id="Seg_8836" s="T81">камасинец-PL</ta>
            <ta e="T83" id="Seg_8837" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_8838" s="T83">жить-DUR.PST-3PL</ta>
            <ta e="T85" id="Seg_8839" s="T84">Бог-LAT</ta>
            <ta e="T86" id="Seg_8840" s="T85">делать-PST-3PL</ta>
            <ta e="T88" id="Seg_8841" s="T87">дерево-INS</ta>
            <ta e="T89" id="Seg_8842" s="T88">тогда</ta>
            <ta e="T91" id="Seg_8843" s="T90">камень-ABL</ta>
            <ta e="T92" id="Seg_8844" s="T91">делать-PST-3PL</ta>
            <ta e="T93" id="Seg_8845" s="T92">тогда</ta>
            <ta e="T94" id="Seg_8846" s="T93">бумага-ADJZ</ta>
            <ta e="T95" id="Seg_8847" s="T94">делать-PST-3PL</ta>
            <ta e="T97" id="Seg_8848" s="T96">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T98" id="Seg_8849" s="T97">бог.[NOM.SG]</ta>
            <ta e="T99" id="Seg_8850" s="T98">делать-PST-3PL</ta>
            <ta e="T100" id="Seg_8851" s="T99">и</ta>
            <ta e="T101" id="Seg_8852" s="T100">установить-PST-3PL</ta>
            <ta e="T102" id="Seg_8853" s="T101">PTCL</ta>
            <ta e="T104" id="Seg_8854" s="T103">этот-PL-LAT</ta>
            <ta e="T105" id="Seg_8855" s="T104">еще</ta>
            <ta e="T106" id="Seg_8856" s="T105">PTCL</ta>
            <ta e="T107" id="Seg_8857" s="T106">свеча-NOM/GEN/ACC.3PL</ta>
            <ta e="T108" id="Seg_8858" s="T107">установить-PST-3PL</ta>
            <ta e="T109" id="Seg_8859" s="T108">светить-PST-3PL</ta>
            <ta e="T111" id="Seg_8860" s="T110">камасинец-PL</ta>
            <ta e="T112" id="Seg_8861" s="T111">PTCL</ta>
            <ta e="T113" id="Seg_8862" s="T112">идти-PST-3PL</ta>
            <ta e="T114" id="Seg_8863" s="T113">лес-LOC</ta>
            <ta e="T115" id="Seg_8864" s="T114">и</ta>
            <ta e="T116" id="Seg_8865" s="T115">тогда</ta>
            <ta e="T117" id="Seg_8866" s="T116">холодный.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8867" s="T117">быть-PST.[3SG]</ta>
            <ta e="T119" id="Seg_8868" s="T118">так</ta>
            <ta e="T120" id="Seg_8869" s="T119">прийти-FUT-3PL</ta>
            <ta e="T121" id="Seg_8870" s="T120">здесь</ta>
            <ta e="T122" id="Seg_8871" s="T121">PTCL</ta>
            <ta e="T123" id="Seg_8872" s="T122">там</ta>
            <ta e="T124" id="Seg_8873" s="T123">старый</ta>
            <ta e="T125" id="Seg_8874" s="T124">стойбище</ta>
            <ta e="T126" id="Seg_8875" s="T125">жить-PST-3PL</ta>
            <ta e="T127" id="Seg_8876" s="T126">тогда</ta>
            <ta e="T128" id="Seg_8877" s="T127">видеть-PST-3PL</ta>
            <ta e="T129" id="Seg_8878" s="T128">здесь</ta>
            <ta e="T130" id="Seg_8879" s="T129">вода.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8880" s="T130">NEG</ta>
            <ta e="T132" id="Seg_8881" s="T131">замерзнуть-PRS.[3SG]</ta>
            <ta e="T133" id="Seg_8882" s="T132">тогда</ta>
            <ta e="T134" id="Seg_8883" s="T133">здесь</ta>
            <ta e="T138" id="Seg_8884" s="T137">поставить-DUR-3PL</ta>
            <ta e="T139" id="Seg_8885" s="T138">PTCL</ta>
            <ta e="T140" id="Seg_8886" s="T139">чум-INS</ta>
            <ta e="T141" id="Seg_8887" s="T140">и</ta>
            <ta e="T142" id="Seg_8888" s="T141">здесь</ta>
            <ta e="T143" id="Seg_8889" s="T142">жить-DUR.PST-3PL</ta>
            <ta e="T144" id="Seg_8890" s="T143">этот-PL</ta>
            <ta e="T145" id="Seg_8891" s="T144">PTCL</ta>
            <ta e="T146" id="Seg_8892" s="T145">идти-PST-3PL</ta>
            <ta e="T150" id="Seg_8893" s="T149">дорога-NOM/GEN.3SG</ta>
            <ta e="T151" id="Seg_8894" s="T150">PTCL</ta>
            <ta e="T152" id="Seg_8895" s="T151">узкий</ta>
            <ta e="T153" id="Seg_8896" s="T152">быть-PST.[3SG]</ta>
            <ta e="T154" id="Seg_8897" s="T153">один.[NOM.SG]</ta>
            <ta e="T155" id="Seg_8898" s="T154">идти-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_8899" s="T155">так</ta>
            <ta e="T157" id="Seg_8900" s="T156">PTCL</ta>
            <ta e="T158" id="Seg_8901" s="T157">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T159" id="Seg_8902" s="T158">как</ta>
            <ta e="T160" id="Seg_8903" s="T159">утка-PL</ta>
            <ta e="T161" id="Seg_8904" s="T160">лес-LAT</ta>
            <ta e="T162" id="Seg_8905" s="T161">пойти-PST-3PL</ta>
            <ta e="T163" id="Seg_8906" s="T162">один.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8907" s="T163">NEG</ta>
            <ta e="T165" id="Seg_8908" s="T164">пойти-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_8909" s="T165">а</ta>
            <ta e="T167" id="Seg_8910" s="T166">много</ta>
            <ta e="T168" id="Seg_8911" s="T167">пойти-FUT-3PL</ta>
            <ta e="T170" id="Seg_8912" s="T169">этот-PL</ta>
            <ta e="T171" id="Seg_8913" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_8914" s="T171">лес-ABL</ta>
            <ta e="T173" id="Seg_8915" s="T172">прийти-PRS-3PL</ta>
            <ta e="T174" id="Seg_8916" s="T173">один.[NOM.SG]</ta>
            <ta e="T175" id="Seg_8917" s="T174">два.[NOM.SG]</ta>
            <ta e="T176" id="Seg_8918" s="T175">четыре.[NOM.SG]</ta>
            <ta e="T177" id="Seg_8919" s="T176">три.[NOM.SG]</ta>
            <ta e="T178" id="Seg_8920" s="T177">пять.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8921" s="T178">шесть.[NOM.SG]</ta>
            <ta e="T180" id="Seg_8922" s="T179">один.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8923" s="T180">один-INS</ta>
            <ta e="T186" id="Seg_8924" s="T185">прийти-DUR-3PL</ta>
            <ta e="T187" id="Seg_8925" s="T186">вода.[NOM.SG]</ta>
            <ta e="T188" id="Seg_8926" s="T187">PTCL</ta>
            <ta e="T189" id="Seg_8927" s="T188">замерзнуть-DUR.PST.[3SG]</ta>
            <ta e="T190" id="Seg_8928" s="T189">нога-INS</ta>
            <ta e="T191" id="Seg_8929" s="T190">поставить-PRS-2SG</ta>
            <ta e="T192" id="Seg_8930" s="T191">стоять-FUT-2SG</ta>
            <ta e="T193" id="Seg_8931" s="T192">так</ta>
            <ta e="T194" id="Seg_8932" s="T193">там</ta>
            <ta e="T195" id="Seg_8933" s="T194">NEG</ta>
            <ta e="T196" id="Seg_8934" s="T195">упасть-PRS-2SG</ta>
            <ta e="T197" id="Seg_8935" s="T196">я.NOM</ta>
            <ta e="T198" id="Seg_8936" s="T197">маленький.[NOM.SG]</ta>
            <ta e="T199" id="Seg_8937" s="T198">быть-PST-1SG</ta>
            <ta e="T200" id="Seg_8938" s="T199">я.NOM</ta>
            <ta e="T201" id="Seg_8939" s="T200">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T202" id="Seg_8940" s="T201">работать-PST.[3SG]</ta>
            <ta e="T203" id="Seg_8941" s="T202">кожа-PL</ta>
            <ta e="T205" id="Seg_8942" s="T204">делать-PST.[3SG]</ta>
            <ta e="T206" id="Seg_8943" s="T205">парка-NOM/GEN/ACC.3PL</ta>
            <ta e="T207" id="Seg_8944" s="T206">прийти-PST.[3SG]</ta>
            <ta e="T208" id="Seg_8945" s="T207">сапог-PL</ta>
            <ta e="T209" id="Seg_8946" s="T208">шить-PST.[3SG]</ta>
            <ta e="T213" id="Seg_8947" s="T212">шапка.[NOM.SG]</ta>
            <ta e="T215" id="Seg_8948" s="T214">шить-PST.[3SG]</ta>
            <ta e="T217" id="Seg_8949" s="T215">и</ta>
            <ta e="T218" id="Seg_8950" s="T217">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T219" id="Seg_8951" s="T218">PTCL</ta>
            <ta e="T220" id="Seg_8952" s="T219">надеть-PST.[3SG]</ta>
            <ta e="T221" id="Seg_8953" s="T220">сейчас</ta>
            <ta e="T222" id="Seg_8954" s="T221">я.NOM</ta>
            <ta e="T223" id="Seg_8955" s="T222">сила-NOM/GEN/ACC.1SG</ta>
            <ta e="T224" id="Seg_8956" s="T223">NEG.EX.[3SG]</ta>
            <ta e="T225" id="Seg_8957" s="T224">NEG</ta>
            <ta e="T226" id="Seg_8958" s="T225">знать-1SG</ta>
            <ta e="T227" id="Seg_8959" s="T226">этот.[NOM.SG]</ta>
            <ta e="T228" id="Seg_8960" s="T227">куда.идти-PST.[3SG]</ta>
            <ta e="T230" id="Seg_8961" s="T229">очень</ta>
            <ta e="T232" id="Seg_8962" s="T231">бояться-PRS-1SG</ta>
            <ta e="T233" id="Seg_8963" s="T232">PTCL</ta>
            <ta e="T234" id="Seg_8964" s="T233">я.ACC</ta>
            <ta e="T235" id="Seg_8965" s="T234">трясти-DUR.[3SG]</ta>
            <ta e="T237" id="Seg_8966" s="T236">сегодня</ta>
            <ta e="T239" id="Seg_8967" s="T238">лес-LOC</ta>
            <ta e="T240" id="Seg_8968" s="T239">ночевать-PST-2PL</ta>
            <ta e="T241" id="Seg_8969" s="T240">очень</ta>
            <ta e="T242" id="Seg_8970" s="T241">холодный.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8971" s="T242">быть-PST.[3SG]</ta>
            <ta e="T244" id="Seg_8972" s="T243">и</ta>
            <ta e="T245" id="Seg_8973" s="T244">мы.NOM</ta>
            <ta e="T246" id="Seg_8974" s="T245">очень</ta>
            <ta e="T247" id="Seg_8975" s="T246">замерзнуть-RES-PST-1PL</ta>
            <ta e="T248" id="Seg_8976" s="T247">PTCL</ta>
            <ta e="T249" id="Seg_8977" s="T248">сильно</ta>
            <ta e="T250" id="Seg_8978" s="T249">замерзнуть-RES-PST-1PL</ta>
            <ta e="T252" id="Seg_8979" s="T251">очень</ta>
            <ta e="T253" id="Seg_8980" s="T252">холодный.[NOM.SG]</ta>
            <ta e="T254" id="Seg_8981" s="T253">вода.[NOM.SG]</ta>
            <ta e="T255" id="Seg_8982" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8983" s="T255">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T257" id="Seg_8984" s="T256">я.NOM</ta>
            <ta e="T258" id="Seg_8985" s="T257">нога-INS-NOM/GEN/ACC.1SG</ta>
            <ta e="T260" id="Seg_8986" s="T259">стоять-PST-1SG</ta>
            <ta e="T261" id="Seg_8987" s="T260">этот.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8988" s="T261">NEG</ta>
            <ta e="T263" id="Seg_8989" s="T262">сломать-PST.[3SG]</ta>
            <ta e="T265" id="Seg_8990" s="T264">когда</ta>
            <ta e="T266" id="Seg_8991" s="T265">спать-INF.LAT</ta>
            <ta e="T267" id="Seg_8992" s="T266">лежать-FUT-2SG</ta>
            <ta e="T268" id="Seg_8993" s="T267">спросить-IMP.2SG</ta>
            <ta e="T269" id="Seg_8994" s="T268">пускать-FUT-2SG</ta>
            <ta e="T270" id="Seg_8995" s="T269">я.ACC</ta>
            <ta e="T271" id="Seg_8996" s="T270">спать-INF.LAT</ta>
            <ta e="T272" id="Seg_8997" s="T271">здесь</ta>
            <ta e="T274" id="Seg_8998" s="T272">первый</ta>
            <ta e="T275" id="Seg_8999" s="T274">там</ta>
            <ta e="T276" id="Seg_9000" s="T275">стоять-PST-3PL</ta>
            <ta e="T277" id="Seg_9001" s="T276">Ильбинь-EP-GEN</ta>
            <ta e="T278" id="Seg_9002" s="T277">край-LAT/LOC.3SG</ta>
            <ta e="T279" id="Seg_9003" s="T278">тогда</ta>
            <ta e="T280" id="Seg_9004" s="T279">тот.[NOM.SG]</ta>
            <ta e="T281" id="Seg_9005" s="T280">вода.[NOM.SG]</ta>
            <ta e="T282" id="Seg_9006" s="T281">видеть-PST-3PL</ta>
            <ta e="T283" id="Seg_9007" s="T282">NEG</ta>
            <ta e="T285" id="Seg_9008" s="T284">NEG</ta>
            <ta e="T286" id="Seg_9009" s="T285">замерзнуть-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_9010" s="T287">этот-PL</ta>
            <ta e="T289" id="Seg_9011" s="T288">здесь</ta>
            <ta e="T290" id="Seg_9012" s="T289">чум-PL</ta>
            <ta e="T291" id="Seg_9013" s="T290">установить-PST-3PL</ta>
            <ta e="T292" id="Seg_9014" s="T291">и</ta>
            <ta e="T293" id="Seg_9015" s="T292">здесь</ta>
            <ta e="T294" id="Seg_9016" s="T293">жить-INF.LAT</ta>
            <ta e="T295" id="Seg_9017" s="T294">прийти-PST-3PL</ta>
            <ta e="T296" id="Seg_9018" s="T295">когда</ta>
            <ta e="T297" id="Seg_9019" s="T296">холодный.[NOM.SG]</ta>
            <ta e="T298" id="Seg_9020" s="T297">стать-RES-PST.[3SG]</ta>
            <ta e="T300" id="Seg_9021" s="T299">этот-PL</ta>
            <ta e="T301" id="Seg_9022" s="T300">PTCL</ta>
            <ta e="T302" id="Seg_9023" s="T301">прийти-PST-3PL</ta>
            <ta e="T303" id="Seg_9024" s="T302">Ильбинь-LAT</ta>
            <ta e="T304" id="Seg_9025" s="T303">жить-DUR-3PL</ta>
            <ta e="T305" id="Seg_9026" s="T304">тогда</ta>
            <ta e="T306" id="Seg_9027" s="T305">здесь</ta>
            <ta e="T307" id="Seg_9028" s="T306">прийти-PST-3PL</ta>
            <ta e="T308" id="Seg_9029" s="T307">вода.[NOM.SG]</ta>
            <ta e="T309" id="Seg_9030" s="T308">видеть-PST-3PL</ta>
            <ta e="T310" id="Seg_9031" s="T309">этот.[NOM.SG]</ta>
            <ta e="T311" id="Seg_9032" s="T310">NEG</ta>
            <ta e="T312" id="Seg_9033" s="T311">замерзнуть-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_9034" s="T312">здесь</ta>
            <ta e="T314" id="Seg_9035" s="T313">чум-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T315" id="Seg_9036" s="T314">установить-PST-3PL</ta>
            <ta e="T316" id="Seg_9037" s="T315">здесь</ta>
            <ta e="T318" id="Seg_9038" s="T317">прийти-PRS-3PL</ta>
            <ta e="T319" id="Seg_9039" s="T318">жить-INF.LAT</ta>
            <ta e="T320" id="Seg_9040" s="T319">когда</ta>
            <ta e="T321" id="Seg_9041" s="T320">холодный.[NOM.SG]</ta>
            <ta e="T322" id="Seg_9042" s="T321">стать-RES-FUT-3SG</ta>
            <ta e="T323" id="Seg_9043" s="T322">этот-PL</ta>
            <ta e="T324" id="Seg_9044" s="T323">NEG</ta>
            <ta e="T325" id="Seg_9045" s="T324">стать-RES-PST.[3SG]</ta>
            <ta e="T326" id="Seg_9046" s="T325">тогда</ta>
            <ta e="T327" id="Seg_9047" s="T326">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T328" id="Seg_9048" s="T327">лес-LAT</ta>
            <ta e="T329" id="Seg_9049" s="T328">везде</ta>
            <ta e="T330" id="Seg_9050" s="T329">PTCL</ta>
            <ta e="T331" id="Seg_9051" s="T330">земля-LOC</ta>
            <ta e="T332" id="Seg_9052" s="T331">идти-PST-3PL</ta>
            <ta e="T333" id="Seg_9053" s="T332">Саяны-LAT</ta>
            <ta e="T334" id="Seg_9054" s="T333">пойти-PST-3PL</ta>
            <ta e="T335" id="Seg_9055" s="T334">там</ta>
            <ta e="T336" id="Seg_9056" s="T335">вода.[NOM.SG]</ta>
            <ta e="T337" id="Seg_9057" s="T336">много</ta>
            <ta e="T339" id="Seg_9058" s="T338">один.[NOM.SG]</ta>
            <ta e="T340" id="Seg_9059" s="T339">мужчина.[NOM.SG]</ta>
            <ta e="T341" id="Seg_9060" s="T340">здесь</ta>
            <ta e="T342" id="Seg_9061" s="T341">а</ta>
            <ta e="T343" id="Seg_9062" s="T342">один.[NOM.SG]</ta>
            <ta e="T344" id="Seg_9063" s="T343">мужчина.[NOM.SG]</ta>
            <ta e="T345" id="Seg_9064" s="T344">там</ta>
            <ta e="T347" id="Seg_9065" s="T346">пойти-EP-IMP.2SG</ta>
            <ta e="T348" id="Seg_9066" s="T347">там</ta>
            <ta e="T349" id="Seg_9067" s="T348">здесь</ta>
            <ta e="T350" id="Seg_9068" s="T349">там</ta>
            <ta e="T351" id="Seg_9069" s="T350">NEG.AUX-1SG</ta>
            <ta e="T352" id="Seg_9070" s="T351">пойти-EP-CNG</ta>
            <ta e="T353" id="Seg_9071" s="T352">и</ta>
            <ta e="T354" id="Seg_9072" s="T353">здесь</ta>
            <ta e="T355" id="Seg_9073" s="T354">NEG.AUX-1SG</ta>
            <ta e="T356" id="Seg_9074" s="T355">пойти-EP-CNG</ta>
            <ta e="T358" id="Seg_9075" s="T357">как</ta>
            <ta e="T359" id="Seg_9076" s="T358">так</ta>
            <ta e="T360" id="Seg_9077" s="T359">стать-FUT-3SG</ta>
            <ta e="T361" id="Seg_9078" s="T360">NEG</ta>
            <ta e="T362" id="Seg_9079" s="T361">пойти-PRS-2SG</ta>
            <ta e="T363" id="Seg_9080" s="T362">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_9081" s="T363">этот.[NOM.SG]</ta>
            <ta e="T365" id="Seg_9082" s="T364">мужчина.[NOM.SG]</ta>
            <ta e="T366" id="Seg_9083" s="T365">очень</ta>
            <ta e="T367" id="Seg_9084" s="T366">хороший.[NOM.SG]</ta>
            <ta e="T369" id="Seg_9085" s="T368">жить-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_9086" s="T369">там</ta>
            <ta e="T371" id="Seg_9087" s="T370">PTCL</ta>
            <ta e="T372" id="Seg_9088" s="T371">что.[NOM.SG]</ta>
            <ta e="T373" id="Seg_9089" s="T372">быть-PRS.[3SG]</ta>
            <ta e="T375" id="Seg_9090" s="T374">переселенец-PL</ta>
            <ta e="T376" id="Seg_9091" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_9092" s="T376">прийти-PST-3PL</ta>
            <ta e="T378" id="Seg_9093" s="T377">я.NOM</ta>
            <ta e="T379" id="Seg_9094" s="T378">язык-INS</ta>
            <ta e="T380" id="Seg_9095" s="T379">говорить-INF.LAT</ta>
            <ta e="T382" id="Seg_9096" s="T381">этот-PL</ta>
            <ta e="T383" id="Seg_9097" s="T382">я.NOM</ta>
            <ta e="T385" id="Seg_9098" s="T384">язык-ACC</ta>
            <ta e="T386" id="Seg_9099" s="T385">научиться-DUR-3PL</ta>
            <ta e="T388" id="Seg_9100" s="T387">спросить-IMP.2SG</ta>
            <ta e="T389" id="Seg_9101" s="T388">а</ta>
            <ta e="T390" id="Seg_9102" s="T389">я.NOM</ta>
            <ta e="T391" id="Seg_9103" s="T390">сказать-FUT-1SG</ta>
            <ta e="T392" id="Seg_9104" s="T391">ты.DAT</ta>
            <ta e="T393" id="Seg_9105" s="T392">ты.NOM</ta>
            <ta e="T394" id="Seg_9106" s="T393">что.[NOM.SG]</ta>
            <ta e="T395" id="Seg_9107" s="T394">этот-ABL</ta>
            <ta e="T396" id="Seg_9108" s="T395">взять-PST-2SG</ta>
            <ta e="T401" id="Seg_9109" s="T400">ты.NOM</ta>
            <ta e="T402" id="Seg_9110" s="T401">очень</ta>
            <ta e="T403" id="Seg_9111" s="T402">длинный.[NOM.SG]</ta>
            <ta e="T405" id="Seg_9112" s="T404">язык-NOM/GEN/ACC.2SG</ta>
            <ta e="T406" id="Seg_9113" s="T405">а</ta>
            <ta e="T407" id="Seg_9114" s="T406">я.NOM</ta>
            <ta e="T408" id="Seg_9115" s="T407">маленький.[NOM.SG]</ta>
            <ta e="T409" id="Seg_9116" s="T408">язык-ACC</ta>
            <ta e="T411" id="Seg_9117" s="T410">мы.NOM</ta>
            <ta e="T412" id="Seg_9118" s="T411">PTCL</ta>
            <ta e="T414" id="Seg_9119" s="T413">прийти-PST-1PL</ta>
            <ta e="T416" id="Seg_9120" s="T415">дорога-LAT</ta>
            <ta e="T417" id="Seg_9121" s="T416">мужчина.[NOM.SG]</ta>
            <ta e="T418" id="Seg_9122" s="T417">PTCL</ta>
            <ta e="T419" id="Seg_9123" s="T418">прийти-PRS.[3SG]</ta>
            <ta e="T420" id="Seg_9124" s="T419">мы.LAT</ta>
            <ta e="T421" id="Seg_9125" s="T420">спросить-IMP.2SG</ta>
            <ta e="T422" id="Seg_9126" s="T421">дорога.[NOM.SG]</ta>
            <ta e="T423" id="Seg_9127" s="T422">куда</ta>
            <ta e="T424" id="Seg_9128" s="T423">этот.[NOM.SG]</ta>
            <ta e="T425" id="Seg_9129" s="T424">идти-PRS.[3SG]</ta>
            <ta e="T426" id="Seg_9130" s="T425">а.то</ta>
            <ta e="T428" id="Seg_9131" s="T426">мы.NOM</ta>
            <ta e="T430" id="Seg_9132" s="T429">мы.NOM</ta>
            <ta e="T431" id="Seg_9133" s="T430">PTCL</ta>
            <ta e="T432" id="Seg_9134" s="T431">NEG</ta>
            <ta e="T433" id="Seg_9135" s="T432">там</ta>
            <ta e="T434" id="Seg_9136" s="T433">может.быть</ta>
            <ta e="T435" id="Seg_9137" s="T434">пойти-PST-1PL</ta>
            <ta e="T437" id="Seg_9138" s="T436">ты.NOM</ta>
            <ta e="T438" id="Seg_9139" s="T437">PTCL</ta>
            <ta e="T439" id="Seg_9140" s="T438">NEG</ta>
            <ta e="T440" id="Seg_9141" s="T439">говорить-PRS-2SG</ta>
            <ta e="T441" id="Seg_9142" s="T440">а</ta>
            <ta e="T442" id="Seg_9143" s="T441">я.NOM</ta>
            <ta e="T443" id="Seg_9144" s="T442">говорить-PRS-1SG</ta>
            <ta e="T444" id="Seg_9145" s="T443">ты.NOM-INS</ta>
            <ta e="T445" id="Seg_9146" s="T444">а</ta>
            <ta e="T446" id="Seg_9147" s="T445">я.NOM</ta>
            <ta e="T447" id="Seg_9148" s="T446">NEG</ta>
            <ta e="T448" id="Seg_9149" s="T447">знать-1SG</ta>
            <ta e="T449" id="Seg_9150" s="T448">я.NOM</ta>
            <ta e="T450" id="Seg_9151" s="T449">знать-1SG</ta>
            <ta e="T451" id="Seg_9152" s="T450">а</ta>
            <ta e="T452" id="Seg_9153" s="T451">говорить-INF.LAT</ta>
            <ta e="T453" id="Seg_9154" s="T452">NEG</ta>
            <ta e="T454" id="Seg_9155" s="T453">мочь-PRS-1SG</ta>
            <ta e="T455" id="Seg_9156" s="T454">надо</ta>
            <ta e="T456" id="Seg_9157" s="T455">ты.NOM</ta>
            <ta e="T457" id="Seg_9158" s="T456">язык-NOM/GEN/ACC.2SG</ta>
            <ta e="T458" id="Seg_9159" s="T457">от-тянуть-INF.LAT</ta>
            <ta e="T459" id="Seg_9160" s="T458">и</ta>
            <ta e="T460" id="Seg_9161" s="T459">выбросить-INF.LAT</ta>
            <ta e="T462" id="Seg_9162" s="T461">этот-PL</ta>
            <ta e="T463" id="Seg_9163" s="T462">учиться-PST-3PL</ta>
            <ta e="T464" id="Seg_9164" s="T463">сейчас</ta>
            <ta e="T465" id="Seg_9165" s="T464">тоже</ta>
            <ta e="T466" id="Seg_9166" s="T465">камасинец-PL</ta>
            <ta e="T467" id="Seg_9167" s="T466">стать-FUT-3PL</ta>
            <ta e="T470" id="Seg_9168" s="T469">очень</ta>
            <ta e="T471" id="Seg_9169" s="T470">большой.[NOM.SG]</ta>
            <ta e="T472" id="Seg_9170" s="T471">ветер.[NOM.SG]</ta>
            <ta e="T473" id="Seg_9171" s="T472">PTCL</ta>
            <ta e="T474" id="Seg_9172" s="T473">сильно</ta>
            <ta e="T475" id="Seg_9173" s="T474">PTCL</ta>
            <ta e="T476" id="Seg_9174" s="T475">идти-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_9175" s="T476">снег.[NOM.SG]</ta>
            <ta e="T478" id="Seg_9176" s="T477">PTCL</ta>
            <ta e="T479" id="Seg_9177" s="T478">мерзнуть-DUR.[3SG]</ta>
            <ta e="T480" id="Seg_9178" s="T479">и</ta>
            <ta e="T481" id="Seg_9179" s="T480">земля.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9180" s="T481">мерзнуть-DUR.[3SG]</ta>
            <ta e="T484" id="Seg_9181" s="T482">PTCL</ta>
            <ta e="T486" id="Seg_9182" s="T484">снег.[NOM.SG]</ta>
            <ta e="T487" id="Seg_9183" s="T486">и</ta>
            <ta e="T488" id="Seg_9184" s="T487">земля.[NOM.SG]</ta>
            <ta e="T489" id="Seg_9185" s="T488">PTCL</ta>
            <ta e="T491" id="Seg_9186" s="T490">PTCL</ta>
            <ta e="T492" id="Seg_9187" s="T491">сбивать.масло-MOM-PST.[3SG]</ta>
            <ta e="T493" id="Seg_9188" s="T492">ты.NOM</ta>
            <ta e="T494" id="Seg_9189" s="T493">бог.[NOM.SG]</ta>
            <ta e="T495" id="Seg_9190" s="T494">бог.[NOM.SG]</ta>
            <ta e="T496" id="Seg_9191" s="T495">NEG.AUX-IMP.2SG</ta>
            <ta e="T497" id="Seg_9192" s="T496">оставить-IMP.2SG.O</ta>
            <ta e="T498" id="Seg_9193" s="T497">я.ACC</ta>
            <ta e="T499" id="Seg_9194" s="T498">NEG.AUX-IMP.2SG</ta>
            <ta e="T500" id="Seg_9195" s="T499">оставить-IMP.2SG.O</ta>
            <ta e="T501" id="Seg_9196" s="T500">я.ACC</ta>
            <ta e="T502" id="Seg_9197" s="T501">NEG.AUX-IMP.2SG</ta>
            <ta e="T503" id="Seg_9198" s="T502">оставить-IMP.2SG.O</ta>
            <ta e="T504" id="Seg_9199" s="T503">я.ACC</ta>
            <ta e="T505" id="Seg_9200" s="T504">NEG.AUX-IMP.2SG</ta>
            <ta e="T506" id="Seg_9201" s="T505">выбросить-IMP.2SG.O</ta>
            <ta e="T507" id="Seg_9202" s="T506">я.ACC</ta>
            <ta e="T508" id="Seg_9203" s="T507">ты.NOM</ta>
            <ta e="T509" id="Seg_9204" s="T508">взять-IMP.2SG.O</ta>
            <ta e="T510" id="Seg_9205" s="T509">я.LAT</ta>
            <ta e="T511" id="Seg_9206" s="T510">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T512" id="Seg_9207" s="T511">ум-ADJZ.[NOM.SG]</ta>
            <ta e="T513" id="Seg_9208" s="T512">ты.NOM</ta>
            <ta e="T514" id="Seg_9209" s="T513">принести-IMP.2SG</ta>
            <ta e="T515" id="Seg_9210" s="T514">я.LAT</ta>
            <ta e="T516" id="Seg_9211" s="T515">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T517" id="Seg_9212" s="T516">ум.[NOM.SG]</ta>
            <ta e="T518" id="Seg_9213" s="T517">много</ta>
            <ta e="T519" id="Seg_9214" s="T518">научиться-TR-IMP.2SG.O</ta>
            <ta e="T520" id="Seg_9215" s="T519">я.ACC</ta>
            <ta e="T521" id="Seg_9216" s="T520">восхвалять-INF.LAT</ta>
            <ta e="T522" id="Seg_9217" s="T521">ты.NOM-INS</ta>
            <ta e="T523" id="Seg_9218" s="T522">научиться-TR-IMP.2SG.O</ta>
            <ta e="T524" id="Seg_9219" s="T523">я.ACC</ta>
            <ta e="T525" id="Seg_9220" s="T524">ты.NOM</ta>
            <ta e="T527" id="Seg_9221" s="T526">дорога-ACC</ta>
            <ta e="T528" id="Seg_9222" s="T527">идти-INF.LAT</ta>
            <ta e="T530" id="Seg_9223" s="T529">ты.NOM</ta>
            <ta e="T532" id="Seg_9224" s="T531">дорога-INS-PL</ta>
            <ta e="T533" id="Seg_9225" s="T532">пойти-INF.LAT</ta>
            <ta e="T538" id="Seg_9226" s="T537">Ваня.[NOM.SG]</ta>
            <ta e="T539" id="Seg_9227" s="T538">пойти-EP-IMP.2SG</ta>
            <ta e="T540" id="Seg_9228" s="T539">русский</ta>
            <ta e="T541" id="Seg_9229" s="T540">мать-LAT-2SG</ta>
            <ta e="T542" id="Seg_9230" s="T541">ножницы.[NOM.SG]</ta>
            <ta e="T543" id="Seg_9231" s="T542">спросить-IMP.2SG</ta>
            <ta e="T544" id="Seg_9232" s="T543">тот.ABL</ta>
            <ta e="T545" id="Seg_9233" s="T544">овца-EP-GEN</ta>
            <ta e="T546" id="Seg_9234" s="T545">ножницы.[NOM.SG]</ta>
            <ta e="T547" id="Seg_9235" s="T546">овца-PL</ta>
            <ta e="T548" id="Seg_9236" s="T547">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T550" id="Seg_9237" s="T549">резать-INF.LAT</ta>
            <ta e="T551" id="Seg_9238" s="T550">мы.LAT</ta>
            <ta e="T552" id="Seg_9239" s="T551">нужно</ta>
            <ta e="T554" id="Seg_9240" s="T553">хороший</ta>
            <ta e="T555" id="Seg_9241" s="T554">искать-IMP.2SG.O</ta>
            <ta e="T557" id="Seg_9242" s="T556">я.NOM</ta>
            <ta e="T558" id="Seg_9243" s="T557">вчера</ta>
            <ta e="T559" id="Seg_9244" s="T558">печь-LAT</ta>
            <ta e="T564" id="Seg_9245" s="T563">влезать-PST-1SG</ta>
            <ta e="T565" id="Seg_9246" s="T564">NEG</ta>
            <ta e="T566" id="Seg_9247" s="T565">съесть-INF.LAT</ta>
            <ta e="T568" id="Seg_9248" s="T567">Ванька.[NOM.SG]</ta>
            <ta e="T569" id="Seg_9249" s="T568">отец-3SG-INS</ta>
            <ta e="T570" id="Seg_9250" s="T569">прийти-PST-3PL</ta>
            <ta e="T571" id="Seg_9251" s="T570">сказать-IPFVZ.[3SG]</ta>
            <ta e="T572" id="Seg_9252" s="T571">бабушка.[NOM.SG]</ta>
            <ta e="T573" id="Seg_9253" s="T572">%%</ta>
            <ta e="T574" id="Seg_9254" s="T573">какой</ta>
            <ta e="T575" id="Seg_9255" s="T574">рыба.[NOM.SG]</ta>
            <ta e="T576" id="Seg_9256" s="T575">PTCL</ta>
            <ta e="T577" id="Seg_9257" s="T576">большой.[NOM.SG]</ta>
            <ta e="T578" id="Seg_9258" s="T577">а</ta>
            <ta e="T579" id="Seg_9259" s="T578">я.NOM</ta>
            <ta e="T580" id="Seg_9260" s="T579">сказать-PST-1SG</ta>
            <ta e="T581" id="Seg_9261" s="T580">сколько</ta>
            <ta e="T582" id="Seg_9262" s="T581">ловить-PST-2PL</ta>
            <ta e="T583" id="Seg_9263" s="T582">этот.[NOM.SG]</ta>
            <ta e="T584" id="Seg_9264" s="T583">сказать-IPFVZ.[3SG]</ta>
            <ta e="T585" id="Seg_9265" s="T584">один.[NOM.SG]</ta>
            <ta e="T586" id="Seg_9266" s="T585">два.[NOM.SG]</ta>
            <ta e="T587" id="Seg_9267" s="T586">три.[NOM.SG]</ta>
            <ta e="T588" id="Seg_9268" s="T587">четыре.[NOM.SG]</ta>
            <ta e="T589" id="Seg_9269" s="T588">пять.[NOM.SG]</ta>
            <ta e="T590" id="Seg_9270" s="T589">шесть.[NOM.SG]</ta>
            <ta e="T592" id="Seg_9271" s="T591">семь.[NOM.SG]</ta>
            <ta e="T593" id="Seg_9272" s="T592">ловить-PST-1PL</ta>
            <ta e="T595" id="Seg_9273" s="T594">фашист-PL</ta>
            <ta e="T596" id="Seg_9274" s="T595">мы.NOM</ta>
            <ta e="T597" id="Seg_9275" s="T596">люди.[NOM.SG]</ta>
            <ta e="T598" id="Seg_9276" s="T597">всегда</ta>
            <ta e="T600" id="Seg_9277" s="T599">PTCL</ta>
            <ta e="T601" id="Seg_9278" s="T600">бороться-DUR.PST-3PL</ta>
            <ta e="T602" id="Seg_9279" s="T601">два-COLL</ta>
            <ta e="T603" id="Seg_9280" s="T602">чум-LAT-%%</ta>
            <ta e="T604" id="Seg_9281" s="T603">сидеть-DUR-PST.[3SG]</ta>
            <ta e="T605" id="Seg_9282" s="T604">PTCL</ta>
            <ta e="T606" id="Seg_9283" s="T605">Гришка.[NOM.SG]</ta>
            <ta e="T607" id="Seg_9284" s="T606">идти-PST.[3SG]</ta>
            <ta e="T608" id="Seg_9285" s="T607">рыба.[NOM.SG]</ta>
            <ta e="T609" id="Seg_9286" s="T608">ловить-PST.[3SG]</ta>
            <ta e="T610" id="Seg_9287" s="T609">и</ta>
            <ta e="T611" id="Seg_9288" s="T610">чум-LAT/LOC.3SG</ta>
            <ta e="T612" id="Seg_9289" s="T611">принести-PST.[3SG]</ta>
            <ta e="T613" id="Seg_9290" s="T612">утка-PL</ta>
            <ta e="T614" id="Seg_9291" s="T613">один.[NOM.SG]</ta>
            <ta e="T615" id="Seg_9292" s="T614">два.[NOM.SG]</ta>
            <ta e="T616" id="Seg_9293" s="T615">три.[NOM.SG]</ta>
            <ta e="T617" id="Seg_9294" s="T616">принести-FUT-3SG</ta>
            <ta e="T618" id="Seg_9295" s="T617">а</ta>
            <ta e="T619" id="Seg_9296" s="T618">я.NOM</ta>
            <ta e="T621" id="Seg_9297" s="T620">овца.[NOM.SG]</ta>
            <ta e="T622" id="Seg_9298" s="T621">доить-PST-1SG</ta>
            <ta e="T624" id="Seg_9299" s="T623">я.NOM</ta>
            <ta e="T625" id="Seg_9300" s="T624">очень</ta>
            <ta e="T626" id="Seg_9301" s="T625">колхоз-LAT/LOC.3SG</ta>
            <ta e="T627" id="Seg_9302" s="T626">сильно</ta>
            <ta e="T628" id="Seg_9303" s="T627">работать-PST-1SG</ta>
            <ta e="T629" id="Seg_9304" s="T628">много</ta>
            <ta e="T630" id="Seg_9305" s="T629">рис.[NOM.SG]</ta>
            <ta e="T631" id="Seg_9306" s="T630">жать-PST-1SG</ta>
            <ta e="T632" id="Seg_9307" s="T631">десять.[NOM.SG]</ta>
            <ta e="T633" id="Seg_9308" s="T632">два.[NOM.SG]</ta>
            <ta e="T634" id="Seg_9309" s="T633">жать-PST-1SG</ta>
            <ta e="T635" id="Seg_9310" s="T634">еще</ta>
            <ta e="T636" id="Seg_9311" s="T635">два.[NOM.SG]</ta>
            <ta e="T637" id="Seg_9312" s="T636">тогда</ta>
            <ta e="T638" id="Seg_9313" s="T637">один.[NOM.SG]</ta>
            <ta e="T639" id="Seg_9314" s="T638">один.[NOM.SG]</ta>
            <ta e="T640" id="Seg_9315" s="T639">день.[NOM.SG]</ta>
            <ta e="T642" id="Seg_9316" s="T641">три.[NOM.SG]</ta>
            <ta e="T643" id="Seg_9317" s="T642">десять.[NOM.SG]</ta>
            <ta e="T644" id="Seg_9318" s="T643">жать-PST-1SG</ta>
            <ta e="T645" id="Seg_9319" s="T644">и</ta>
            <ta e="T646" id="Seg_9320" s="T645">четыре.[NOM.SG]</ta>
            <ta e="T647" id="Seg_9321" s="T646">еще</ta>
            <ta e="T649" id="Seg_9322" s="T648">сотка-PL</ta>
            <ta e="T650" id="Seg_9323" s="T649">я.NOM</ta>
            <ta e="T651" id="Seg_9324" s="T650">родственник-NOM/GEN/ACC.3SG</ta>
            <ta e="T652" id="Seg_9325" s="T651">спросить-DUR.[3SG]</ta>
            <ta e="T653" id="Seg_9326" s="T652">надо</ta>
            <ta e="T654" id="Seg_9327" s="T653">я.LAT</ta>
            <ta e="T655" id="Seg_9328" s="T654">слушать-INF.LAT</ta>
            <ta e="T656" id="Seg_9329" s="T655">пойти-INF.LAT</ta>
            <ta e="T657" id="Seg_9330" s="T656">вы.ACC</ta>
            <ta e="T658" id="Seg_9331" s="T657">как</ta>
            <ta e="T659" id="Seg_9332" s="T658">говорить-PRS-2PL</ta>
            <ta e="T660" id="Seg_9333" s="T659">прийти-IMP.2SG</ta>
            <ta e="T661" id="Seg_9334" s="T660">завтра</ta>
            <ta e="T662" id="Seg_9335" s="T661">этот-PL</ta>
            <ta e="T663" id="Seg_9336" s="T662">прийти-FUT-3PL</ta>
            <ta e="T664" id="Seg_9337" s="T663">слушать-IMP.2SG.O</ta>
            <ta e="T666" id="Seg_9338" s="T665">я.NOM</ta>
            <ta e="T667" id="Seg_9339" s="T666">PTCL</ta>
            <ta e="T668" id="Seg_9340" s="T667">маленький.[NOM.SG]</ta>
            <ta e="T669" id="Seg_9341" s="T668">курица-PL</ta>
            <ta e="T670" id="Seg_9342" s="T669">встать-DUR-3PL</ta>
            <ta e="T671" id="Seg_9343" s="T670">PTCL</ta>
            <ta e="T672" id="Seg_9344" s="T671">пищать-DUR-3PL</ta>
            <ta e="T674" id="Seg_9345" s="T673">тянуть-IMP.2SG.O</ta>
            <ta e="T675" id="Seg_9346" s="T674">рука-NOM/GEN/ACC.2SG</ta>
            <ta e="T677" id="Seg_9347" s="T676">вчера</ta>
            <ta e="T678" id="Seg_9348" s="T677">женщина-PL</ta>
            <ta e="T679" id="Seg_9349" s="T678">я.LAT</ta>
            <ta e="T680" id="Seg_9350" s="T679">прийти-PST-3PL</ta>
            <ta e="T681" id="Seg_9351" s="T680">молоко.[NOM.SG]</ta>
            <ta e="T682" id="Seg_9352" s="T681">снимать.кору-INF.LAT</ta>
            <ta e="T683" id="Seg_9353" s="T682">много</ta>
            <ta e="T684" id="Seg_9354" s="T683">быть-PST-3PL</ta>
            <ta e="T685" id="Seg_9355" s="T684">там</ta>
            <ta e="T686" id="Seg_9356" s="T685">а</ta>
            <ta e="T688" id="Seg_9357" s="T687">председатель-EP-GEN</ta>
            <ta e="T689" id="Seg_9358" s="T688">женщина-NOM/GEN.3SG</ta>
            <ta e="T690" id="Seg_9359" s="T689">сидеть-PST.[3SG]</ta>
            <ta e="T691" id="Seg_9360" s="T690">здесь</ta>
            <ta e="T692" id="Seg_9361" s="T691">сын-NOM/GEN.3SG</ta>
            <ta e="T693" id="Seg_9362" s="T692">прийти-PST.[3SG]</ta>
            <ta e="T695" id="Seg_9363" s="T694">этот-LAT</ta>
            <ta e="T696" id="Seg_9364" s="T695">сказать-IPFVZ.[3SG]</ta>
            <ta e="T697" id="Seg_9365" s="T696">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T698" id="Seg_9366" s="T697">чум-LAT/LOC.1PL</ta>
            <ta e="T699" id="Seg_9367" s="T698">мы.NOM</ta>
            <ta e="T700" id="Seg_9368" s="T699">Красноярск-NOM/GEN/ACC.3PL</ta>
            <ta e="T701" id="Seg_9369" s="T700">пойти-FUT-1PL</ta>
            <ta e="T702" id="Seg_9370" s="T701">а</ta>
            <ta e="T703" id="Seg_9371" s="T702">я.NOM</ta>
            <ta e="T704" id="Seg_9372" s="T703">сказать-PST-1SG</ta>
            <ta e="T705" id="Seg_9373" s="T704">этот-LAT</ta>
            <ta e="T706" id="Seg_9374" s="T705">взять-IMP.2PL</ta>
            <ta e="T707" id="Seg_9375" s="T706">мать-LAT-2SG</ta>
            <ta e="T708" id="Seg_9376" s="T707">что=INDEF</ta>
            <ta e="T709" id="Seg_9377" s="T708">красивый.[NOM.SG]</ta>
            <ta e="T710" id="Seg_9378" s="T709">платок.[NOM.SG]</ta>
            <ta e="T712" id="Seg_9379" s="T711">взять-IMP.2PL</ta>
            <ta e="T713" id="Seg_9380" s="T712">одежда.[NOM.SG]</ta>
            <ta e="T714" id="Seg_9381" s="T713">взять-IMP.2SG.O</ta>
            <ta e="T715" id="Seg_9382" s="T714">красивый.[NOM.SG]</ta>
            <ta e="T716" id="Seg_9383" s="T715">этот-PL</ta>
            <ta e="T717" id="Seg_9384" s="T716">PTCL</ta>
            <ta e="T718" id="Seg_9385" s="T717">смеяться-DUR-3PL</ta>
            <ta e="T719" id="Seg_9386" s="T718">что</ta>
            <ta e="T720" id="Seg_9387" s="T719">я.NOM</ta>
            <ta e="T721" id="Seg_9388" s="T720">так</ta>
            <ta e="T722" id="Seg_9389" s="T721">говорить-DUR-1SG</ta>
            <ta e="T723" id="Seg_9390" s="T722">этот-PL</ta>
            <ta e="T724" id="Seg_9391" s="T723">NEG</ta>
            <ta e="T725" id="Seg_9392" s="T724">знать-3PL</ta>
            <ta e="T726" id="Seg_9393" s="T725">говорить-INF.LAT</ta>
            <ta e="T732" id="Seg_9394" s="T731">сегодня</ta>
            <ta e="T733" id="Seg_9395" s="T732">женщина.[NOM.SG]</ta>
            <ta e="T734" id="Seg_9396" s="T733">прийти-PST.[3SG]</ta>
            <ta e="T735" id="Seg_9397" s="T734">надо</ta>
            <ta e="T736" id="Seg_9398" s="T735">я.LAT</ta>
            <ta e="T737" id="Seg_9399" s="T736">картофель.[NOM.SG]</ta>
            <ta e="T738" id="Seg_9400" s="T737">съесть-INF.LAT</ta>
            <ta e="T739" id="Seg_9401" s="T738">пойти-FUT-1SG</ta>
            <ta e="T740" id="Seg_9402" s="T739">скоро</ta>
            <ta e="T741" id="Seg_9403" s="T740">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T742" id="Seg_9404" s="T741">сын-LAT</ta>
            <ta e="T743" id="Seg_9405" s="T742">JUSS</ta>
            <ta e="T744" id="Seg_9406" s="T743">сажать-FUT-3SG</ta>
            <ta e="T745" id="Seg_9407" s="T744">я.LAT</ta>
            <ta e="T746" id="Seg_9408" s="T745">картофель.[NOM.SG]</ta>
            <ta e="T748" id="Seg_9409" s="T747">я.NOM</ta>
            <ta e="T749" id="Seg_9410" s="T748">быть-PST-1SG</ta>
            <ta e="T750" id="Seg_9411" s="T749">Заозерка-LOC</ta>
            <ta e="T751" id="Seg_9412" s="T750">хотеть.PST.M.SG</ta>
            <ta e="T752" id="Seg_9413" s="T751">пойти-PST-1SG</ta>
            <ta e="T753" id="Seg_9414" s="T752">аптека-LAT</ta>
            <ta e="T754" id="Seg_9415" s="T753">глаз-LAT/LOC.3SG</ta>
            <ta e="T755" id="Seg_9416" s="T754">хотеть.PST.M.SG</ta>
            <ta e="T756" id="Seg_9417" s="T755">взять-INF.LAT</ta>
            <ta e="T757" id="Seg_9418" s="T756">чтобы</ta>
            <ta e="T758" id="Seg_9419" s="T757">смотреть-FRQ-INF.LAT</ta>
            <ta e="T759" id="Seg_9420" s="T758">далеко-LAT.ADV</ta>
            <ta e="T760" id="Seg_9421" s="T759">этот-PL</ta>
            <ta e="T761" id="Seg_9422" s="T760">NEG</ta>
            <ta e="T762" id="Seg_9423" s="T761">дать-PST-3PL</ta>
            <ta e="T763" id="Seg_9424" s="T762">смотреть-DUR-3PL</ta>
            <ta e="T764" id="Seg_9425" s="T763">пойти-EP-IMP.2SG</ta>
            <ta e="T765" id="Seg_9426" s="T764">глаз-NOM/GEN/ACC.2SG</ta>
            <ta e="T766" id="Seg_9427" s="T765">смотреть-FRQ-TR</ta>
            <ta e="T767" id="Seg_9428" s="T766">тогда</ta>
            <ta e="T768" id="Seg_9429" s="T767">прийти-FUT-2SG</ta>
            <ta e="T769" id="Seg_9430" s="T768">и</ta>
            <ta e="T770" id="Seg_9431" s="T769">взять-FUT-2SG</ta>
            <ta e="T772" id="Seg_9432" s="T771">там</ta>
            <ta e="T773" id="Seg_9433" s="T772">пойти-PST-1SG</ta>
            <ta e="T774" id="Seg_9434" s="T773">этот-PL</ta>
            <ta e="T775" id="Seg_9435" s="T774">PTCL</ta>
            <ta e="T776" id="Seg_9436" s="T775">бороться-DUR.[3SG]</ta>
            <ta e="T777" id="Seg_9437" s="T776">что.[NOM.SG]</ta>
            <ta e="T778" id="Seg_9438" s="T777">бороться-DUR-2PL</ta>
            <ta e="T779" id="Seg_9439" s="T778">NEG.AUX-IMP.2SG</ta>
            <ta e="T780" id="Seg_9440" s="T779">кричать-2PL</ta>
            <ta e="T781" id="Seg_9441" s="T780">хороший-LAT.ADV</ta>
            <ta e="T782" id="Seg_9442" s="T781">делать-IMP.2PL</ta>
            <ta e="T783" id="Seg_9443" s="T782">дом.[NOM.SG]</ta>
            <ta e="T784" id="Seg_9444" s="T783">чистить-EP-IMP.2SG</ta>
            <ta e="T786" id="Seg_9445" s="T785">один-LAT</ta>
            <ta e="T787" id="Seg_9446" s="T786">сказать-PST-1SG</ta>
            <ta e="T788" id="Seg_9447" s="T787">пойти-EP-IMP.2SG</ta>
            <ta e="T789" id="Seg_9448" s="T788">лен-NOM/GEN/ACC.3SG</ta>
            <ta e="T790" id="Seg_9449" s="T789">ударить-MULT-IMP.2SG.O</ta>
            <ta e="T791" id="Seg_9450" s="T790">а</ta>
            <ta e="T792" id="Seg_9451" s="T791">этот.[NOM.SG]</ta>
            <ta e="T793" id="Seg_9452" s="T792">JUSS</ta>
            <ta e="T794" id="Seg_9453" s="T793">вода.[NOM.SG]</ta>
            <ta e="T795" id="Seg_9454" s="T794">носить-FUT-3SG</ta>
            <ta e="T796" id="Seg_9455" s="T795">баня-LAT</ta>
            <ta e="T798" id="Seg_9456" s="T797">я.NOM</ta>
            <ta e="T799" id="Seg_9457" s="T798">сегодня</ta>
            <ta e="T800" id="Seg_9458" s="T799">рано</ta>
            <ta e="T801" id="Seg_9459" s="T800">встать-PST-1SG</ta>
            <ta e="T802" id="Seg_9460" s="T801">еще</ta>
            <ta e="T804" id="Seg_9461" s="T803">пять.[NOM.SG]</ta>
            <ta e="T805" id="Seg_9462" s="T804">NEG.EX-PST.[3SG]</ta>
            <ta e="T806" id="Seg_9463" s="T805">тогда</ta>
            <ta e="T807" id="Seg_9464" s="T806">печь-ACC.3SG</ta>
            <ta e="T808" id="Seg_9465" s="T807">светить-PST-1SG</ta>
            <ta e="T809" id="Seg_9466" s="T808">%%</ta>
            <ta e="T810" id="Seg_9467" s="T809">поставить-PST-1SG</ta>
            <ta e="T811" id="Seg_9468" s="T810">тогда</ta>
            <ta e="T812" id="Seg_9469" s="T811">опять</ta>
            <ta e="T813" id="Seg_9470" s="T812">лежать-PST-1SG</ta>
            <ta e="T814" id="Seg_9471" s="T813">еще</ta>
            <ta e="T815" id="Seg_9472" s="T814">спать-PST-1SG</ta>
            <ta e="T816" id="Seg_9473" s="T815">еще</ta>
            <ta e="T817" id="Seg_9474" s="T816">десять.[NOM.SG]</ta>
            <ta e="T818" id="Seg_9475" s="T817">мочь-PST.[3SG]</ta>
            <ta e="T819" id="Seg_9476" s="T818">тогда</ta>
            <ta e="T821" id="Seg_9477" s="T820">встать-PST-1SG</ta>
            <ta e="T823" id="Seg_9478" s="T822">трудный</ta>
            <ta e="T824" id="Seg_9479" s="T823">сегодня</ta>
            <ta e="T825" id="Seg_9480" s="T824">работать-PST-1SG</ta>
            <ta e="T826" id="Seg_9481" s="T825">а</ta>
            <ta e="T827" id="Seg_9482" s="T826">вчера</ta>
            <ta e="T828" id="Seg_9483" s="T827">NEG</ta>
            <ta e="T829" id="Seg_9484" s="T828">трудный</ta>
            <ta e="T831" id="Seg_9485" s="T830">этот.[NOM.SG]</ta>
            <ta e="T832" id="Seg_9486" s="T831">камень.[NOM.SG]</ta>
            <ta e="T833" id="Seg_9487" s="T832">очень</ta>
            <ta e="T834" id="Seg_9488" s="T833">большой.[NOM.SG]</ta>
            <ta e="T835" id="Seg_9489" s="T834">трудный</ta>
            <ta e="T836" id="Seg_9490" s="T835">а</ta>
            <ta e="T837" id="Seg_9491" s="T836">тот.[NOM.SG]</ta>
            <ta e="T838" id="Seg_9492" s="T837">дерево.[NOM.SG]</ta>
            <ta e="T839" id="Seg_9493" s="T838">NEG</ta>
            <ta e="T840" id="Seg_9494" s="T839">трудный</ta>
            <ta e="T842" id="Seg_9495" s="T841">этот</ta>
            <ta e="T844" id="Seg_9496" s="T843">этот</ta>
            <ta e="T845" id="Seg_9497" s="T844">ребенок.[NOM.SG]</ta>
            <ta e="T846" id="Seg_9498" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_9499" s="T846">штаны-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T848" id="Seg_9500" s="T847">PTCL</ta>
            <ta e="T849" id="Seg_9501" s="T848">от-рвать-PST</ta>
            <ta e="T850" id="Seg_9502" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_9503" s="T850">отверстие.[NOM.SG]</ta>
            <ta e="T852" id="Seg_9504" s="T851">стать-RES-PST.[3SG]</ta>
            <ta e="T853" id="Seg_9505" s="T852">надо</ta>
            <ta e="T854" id="Seg_9506" s="T853">этот-ACC</ta>
            <ta e="T855" id="Seg_9507" s="T854">шить-INF.LAT</ta>
            <ta e="T857" id="Seg_9508" s="T856">класть-INF.LAT</ta>
            <ta e="T859" id="Seg_9509" s="T858">вешать-IMP.2SG</ta>
            <ta e="T860" id="Seg_9510" s="T859">я.LAT</ta>
            <ta e="T861" id="Seg_9511" s="T860">сахар.[NOM.SG]</ta>
            <ta e="T862" id="Seg_9512" s="T861">пять.[NOM.SG]</ta>
            <ta e="T863" id="Seg_9513" s="T862">килограмм.[NOM.SG]</ta>
            <ta e="T865" id="Seg_9514" s="T864">этот.[NOM.SG]</ta>
            <ta e="T866" id="Seg_9515" s="T865">мужчина.[NOM.SG]</ta>
            <ta e="T867" id="Seg_9516" s="T866">болеть-PST.[3SG]</ta>
            <ta e="T868" id="Seg_9517" s="T867">а</ta>
            <ta e="T869" id="Seg_9518" s="T868">сейчас</ta>
            <ta e="T870" id="Seg_9519" s="T869">NEG</ta>
            <ta e="T871" id="Seg_9520" s="T870">болеть-PRS.[3SG]</ta>
            <ta e="T872" id="Seg_9521" s="T871">встать-PST.[3SG]</ta>
            <ta e="T873" id="Seg_9522" s="T872">нога-INS</ta>
            <ta e="T874" id="Seg_9523" s="T873">идти-PRS.[3SG]</ta>
            <ta e="T875" id="Seg_9524" s="T874">а.то</ta>
            <ta e="T876" id="Seg_9525" s="T875">лежать-PST.[3SG]</ta>
            <ta e="T877" id="Seg_9526" s="T876">NEG</ta>
            <ta e="T878" id="Seg_9527" s="T877">хороший.[NOM.SG]</ta>
            <ta e="T879" id="Seg_9528" s="T878">слушать-DUR-INF.LAT</ta>
            <ta e="T880" id="Seg_9529" s="T879">прийти-PST-3PL</ta>
            <ta e="T881" id="Seg_9530" s="T880">и</ta>
            <ta e="T882" id="Seg_9531" s="T881">сказать-PST-3PL</ta>
            <ta e="T883" id="Seg_9532" s="T882">NEG</ta>
            <ta e="T884" id="Seg_9533" s="T883">красивый.[NOM.SG]</ta>
            <ta e="T885" id="Seg_9534" s="T884">говорить-CVB</ta>
            <ta e="T887" id="Seg_9535" s="T886">этот</ta>
            <ta e="T888" id="Seg_9536" s="T887">этот.[NOM.SG]</ta>
            <ta e="T889" id="Seg_9537" s="T888">PTCL</ta>
            <ta e="T890" id="Seg_9538" s="T889">мужчина-ACC</ta>
            <ta e="T891" id="Seg_9539" s="T890">убить-PST.[3SG]</ta>
            <ta e="T892" id="Seg_9540" s="T891">этот-ACC</ta>
            <ta e="T893" id="Seg_9541" s="T892">сажать-PST-3PL</ta>
            <ta e="T895" id="Seg_9542" s="T894">три.[NOM.SG]</ta>
            <ta e="T896" id="Seg_9543" s="T895">зима.[NOM.SG]</ta>
            <ta e="T897" id="Seg_9544" s="T896">жить-DUR-PST.[3SG]</ta>
            <ta e="T898" id="Seg_9545" s="T897">и</ta>
            <ta e="T899" id="Seg_9546" s="T898">сейчас</ta>
            <ta e="T900" id="Seg_9547" s="T899">чум-LAT/LOC.3SG</ta>
            <ta e="T901" id="Seg_9548" s="T900">прийти-PST.[3SG]</ta>
            <ta e="T902" id="Seg_9549" s="T901">и</ta>
            <ta e="T903" id="Seg_9550" s="T902">идти-DUR.[3SG]</ta>
            <ta e="T905" id="Seg_9551" s="T904">PTCL</ta>
            <ta e="T906" id="Seg_9552" s="T905">чашка-NOM/GEN/ACC.3PL</ta>
            <ta e="T910" id="Seg_9553" s="T909">сломать-MOM-PST-3PL</ta>
            <ta e="T913" id="Seg_9554" s="T912">я.NOM</ta>
            <ta e="T916" id="Seg_9555" s="T915">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T917" id="Seg_9556" s="T916">PTCL</ta>
            <ta e="T918" id="Seg_9557" s="T917">камень-INS</ta>
            <ta e="T919" id="Seg_9558" s="T918">ударить-MULT-MOM-PST-3PL</ta>
            <ta e="T920" id="Seg_9559" s="T919">PTCL</ta>
            <ta e="T921" id="Seg_9560" s="T920">PTCL</ta>
            <ta e="T922" id="Seg_9561" s="T921">кровь.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9562" s="T922">течь-DUR.[3SG]</ta>
            <ta e="T925" id="Seg_9563" s="T923">я.LAT</ta>
            <ta e="T926" id="Seg_9564" s="T925">топор-INS</ta>
            <ta e="T927" id="Seg_9565" s="T926">PTCL</ta>
            <ta e="T928" id="Seg_9566" s="T927">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T932" id="Seg_9567" s="T931">сломать-PST-3PL</ta>
            <ta e="T934" id="Seg_9568" s="T932">лес-LAT</ta>
            <ta e="T935" id="Seg_9569" s="T934">пойти-FUT-2SG</ta>
            <ta e="T936" id="Seg_9570" s="T935">ягода.[NOM.SG]</ta>
            <ta e="T937" id="Seg_9571" s="T936">собирать-INF.LAT</ta>
            <ta e="T938" id="Seg_9572" s="T937">пойти-OPT.DU/PL-1PL</ta>
            <ta e="T939" id="Seg_9573" s="T938">чум-LAT/LOC.1PL</ta>
            <ta e="T940" id="Seg_9574" s="T939">ягода.[NOM.SG]</ta>
            <ta e="T941" id="Seg_9575" s="T940">NEG.EX.[3SG]</ta>
            <ta e="T942" id="Seg_9576" s="T941">только</ta>
            <ta e="T944" id="Seg_9577" s="T943">пойти-INF.LAT</ta>
            <ta e="T945" id="Seg_9578" s="T944">где=INDEF</ta>
            <ta e="T946" id="Seg_9579" s="T945">прийти-FUT-3PL</ta>
            <ta e="T947" id="Seg_9580" s="T946">очень</ta>
            <ta e="T948" id="Seg_9581" s="T947">много</ta>
            <ta e="T949" id="Seg_9582" s="T948">ягода.[NOM.SG]</ta>
            <ta e="T950" id="Seg_9583" s="T949">тогда</ta>
            <ta e="T951" id="Seg_9584" s="T950">опять</ta>
            <ta e="T952" id="Seg_9585" s="T951">собирать-PRS-2SG</ta>
            <ta e="T953" id="Seg_9586" s="T952">тогда</ta>
            <ta e="T954" id="Seg_9587" s="T953">мочь-PRS-1SG</ta>
            <ta e="T955" id="Seg_9588" s="T954">когда</ta>
            <ta e="T956" id="Seg_9589" s="T955">чум-LAT/LOC.1SG</ta>
            <ta e="T957" id="Seg_9590" s="T956">пойти-INF.LAT</ta>
            <ta e="T958" id="Seg_9591" s="T957">всегда</ta>
            <ta e="T959" id="Seg_9592" s="T958">ягода.[NOM.SG]</ta>
            <ta e="T960" id="Seg_9593" s="T959">много</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_9594" s="T4">pers</ta>
            <ta e="T6" id="Seg_9595" s="T5">adv</ta>
            <ta e="T7" id="Seg_9596" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_9597" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_9598" s="T8">adv</ta>
            <ta e="T10" id="Seg_9599" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_9600" s="T10">v-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_9601" s="T11">v-v:ins-v:mood.pn</ta>
            <ta e="T13" id="Seg_9602" s="T12">v-v:ins-v:mood.pn</ta>
            <ta e="T14" id="Seg_9603" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_9604" s="T14">interj</ta>
            <ta e="T16" id="Seg_9605" s="T15">adv</ta>
            <ta e="T17" id="Seg_9606" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_9607" s="T17">aux-v:pn</ta>
            <ta e="T19" id="Seg_9608" s="T18">v-v:ins-v:n.fin</ta>
            <ta e="T20" id="Seg_9609" s="T19">adv</ta>
            <ta e="T21" id="Seg_9610" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_9611" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_9612" s="T22">v-v:n.fin</ta>
            <ta e="T24" id="Seg_9613" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_9614" s="T24">adv-n:case</ta>
            <ta e="T26" id="Seg_9615" s="T25">pers</ta>
            <ta e="T28" id="Seg_9616" s="T27">adv</ta>
            <ta e="T29" id="Seg_9617" s="T28">dempro-n:case</ta>
            <ta e="T30" id="Seg_9618" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_9619" s="T30">conj</ta>
            <ta e="T32" id="Seg_9620" s="T31">pers</ta>
            <ta e="T33" id="Seg_9621" s="T32">dempro-n:case</ta>
            <ta e="T34" id="Seg_9622" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_9623" s="T34">pers</ta>
            <ta e="T36" id="Seg_9624" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_9625" s="T36">v-v:n.fin</ta>
            <ta e="T38" id="Seg_9626" s="T37">conj</ta>
            <ta e="T39" id="Seg_9627" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_9628" s="T39">pers-n:case</ta>
            <ta e="T41" id="Seg_9629" s="T40">dempro-n:case</ta>
            <ta e="T42" id="Seg_9630" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_9631" s="T42">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_9632" s="T43">adv</ta>
            <ta e="T45" id="Seg_9633" s="T44">adv</ta>
            <ta e="T46" id="Seg_9634" s="T45">v-v:pn</ta>
            <ta e="T47" id="Seg_9635" s="T46">ptcl</ta>
            <ta e="T49" id="Seg_9636" s="T48">n-n:case.poss</ta>
            <ta e="T50" id="Seg_9637" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_9638" s="T50">adv</ta>
            <ta e="T53" id="Seg_9639" s="T52">quant</ta>
            <ta e="T54" id="Seg_9640" s="T53">n-n:case</ta>
            <ta e="T964" id="Seg_9641" s="T54">v-v:n-fin</ta>
            <ta e="T55" id="Seg_9642" s="T964">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_9643" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_9644" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9645" s="T57">que-n:case=ptcl</ta>
            <ta e="T59" id="Seg_9646" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_9647" s="T59">v-v:pn</ta>
            <ta e="T61" id="Seg_9648" s="T60">dempro-n:case</ta>
            <ta e="T62" id="Seg_9649" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_9650" s="T63">quant</ta>
            <ta e="T65" id="Seg_9651" s="T64">n-n:num-n:case.poss</ta>
            <ta e="T66" id="Seg_9652" s="T65">conj</ta>
            <ta e="T67" id="Seg_9653" s="T66">n-n:num</ta>
            <ta e="T68" id="Seg_9654" s="T67">quant</ta>
            <ta e="T69" id="Seg_9655" s="T68">conj</ta>
            <ta e="T70" id="Seg_9656" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_9657" s="T70">quant</ta>
            <ta e="T72" id="Seg_9658" s="T71">n-n:num</ta>
            <ta e="T73" id="Seg_9659" s="T72">quant</ta>
            <ta e="T74" id="Seg_9660" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_9661" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_9662" s="T75">quant</ta>
            <ta e="T77" id="Seg_9663" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_9664" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_9665" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_9666" s="T79">quant</ta>
            <ta e="T82" id="Seg_9667" s="T81">n-n:num</ta>
            <ta e="T83" id="Seg_9668" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_9669" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_9670" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_9671" s="T85">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_9672" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_9673" s="T88">adv</ta>
            <ta e="T91" id="Seg_9674" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_9675" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_9676" s="T92">adv</ta>
            <ta e="T94" id="Seg_9677" s="T93">n-n&gt;adj</ta>
            <ta e="T95" id="Seg_9678" s="T94">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_9679" s="T96">n-n&gt;adj-n:case</ta>
            <ta e="T98" id="Seg_9680" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_9681" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_9682" s="T99">conj</ta>
            <ta e="T101" id="Seg_9683" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_9684" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_9685" s="T103">dempro-n:num-n:case</ta>
            <ta e="T105" id="Seg_9686" s="T104">adv</ta>
            <ta e="T106" id="Seg_9687" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_9688" s="T106">n-n:case.poss</ta>
            <ta e="T108" id="Seg_9689" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_9690" s="T108">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_9691" s="T110">n-n:num</ta>
            <ta e="T112" id="Seg_9692" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_9693" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_9694" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_9695" s="T114">conj</ta>
            <ta e="T116" id="Seg_9696" s="T115">adv</ta>
            <ta e="T117" id="Seg_9697" s="T116">adj-n:case</ta>
            <ta e="T118" id="Seg_9698" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_9699" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_9700" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_9701" s="T120">adv</ta>
            <ta e="T122" id="Seg_9702" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_9703" s="T122">adv</ta>
            <ta e="T124" id="Seg_9704" s="T123">adj</ta>
            <ta e="T125" id="Seg_9705" s="T124">n</ta>
            <ta e="T126" id="Seg_9706" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_9707" s="T126">adv</ta>
            <ta e="T128" id="Seg_9708" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_9709" s="T128">adv</ta>
            <ta e="T130" id="Seg_9710" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_9711" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_9712" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_9713" s="T132">adv</ta>
            <ta e="T134" id="Seg_9714" s="T133">adv</ta>
            <ta e="T138" id="Seg_9715" s="T137">v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_9716" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_9717" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_9718" s="T140">conj</ta>
            <ta e="T142" id="Seg_9719" s="T141">adv</ta>
            <ta e="T143" id="Seg_9720" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_9721" s="T143">dempro-n:num</ta>
            <ta e="T145" id="Seg_9722" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_9723" s="T145">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_9724" s="T149">n-n:case.poss</ta>
            <ta e="T151" id="Seg_9725" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9726" s="T151">adj</ta>
            <ta e="T153" id="Seg_9727" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_9728" s="T153">num-n:case</ta>
            <ta e="T155" id="Seg_9729" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_9730" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_9731" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_9732" s="T157">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_9733" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_9734" s="T159">n-n:num</ta>
            <ta e="T161" id="Seg_9735" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_9736" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_9737" s="T162">num-n:case</ta>
            <ta e="T164" id="Seg_9738" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_9739" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_9740" s="T165">conj</ta>
            <ta e="T167" id="Seg_9741" s="T166">quant</ta>
            <ta e="T168" id="Seg_9742" s="T167">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_9743" s="T169">dempro-n:num</ta>
            <ta e="T171" id="Seg_9744" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_9745" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_9746" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_9747" s="T173">num-n:case</ta>
            <ta e="T175" id="Seg_9748" s="T174">num-n:case</ta>
            <ta e="T176" id="Seg_9749" s="T175">num-n:case</ta>
            <ta e="T177" id="Seg_9750" s="T176">num-n:case</ta>
            <ta e="T178" id="Seg_9751" s="T177">num-n:case</ta>
            <ta e="T179" id="Seg_9752" s="T178">num-n:case</ta>
            <ta e="T180" id="Seg_9753" s="T179">num-n:case</ta>
            <ta e="T181" id="Seg_9754" s="T180">num-n:case</ta>
            <ta e="T186" id="Seg_9755" s="T185">v-v&gt;v-v:pn</ta>
            <ta e="T187" id="Seg_9756" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_9757" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_9758" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_9759" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_9760" s="T190">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_9761" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_9762" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9763" s="T193">adv</ta>
            <ta e="T195" id="Seg_9764" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_9765" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_9766" s="T196">pers</ta>
            <ta e="T198" id="Seg_9767" s="T197">adj-n:case</ta>
            <ta e="T199" id="Seg_9768" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_9769" s="T199">pers</ta>
            <ta e="T201" id="Seg_9770" s="T200">n-n:case.poss</ta>
            <ta e="T202" id="Seg_9771" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_9772" s="T202">n-n:num</ta>
            <ta e="T205" id="Seg_9773" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_9774" s="T205">n-n:case.poss</ta>
            <ta e="T207" id="Seg_9775" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_9776" s="T207">n-n:num</ta>
            <ta e="T209" id="Seg_9777" s="T208">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_9778" s="T212">n-n:case</ta>
            <ta e="T215" id="Seg_9779" s="T214">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_9780" s="T215">conj</ta>
            <ta e="T218" id="Seg_9781" s="T217">n-n:case.poss</ta>
            <ta e="T219" id="Seg_9782" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_9783" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_9784" s="T220">adv</ta>
            <ta e="T222" id="Seg_9785" s="T221">pers</ta>
            <ta e="T223" id="Seg_9786" s="T222">n-n:case.poss</ta>
            <ta e="T224" id="Seg_9787" s="T223">v-v:pn</ta>
            <ta e="T225" id="Seg_9788" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_9789" s="T225">v-v:pn</ta>
            <ta e="T227" id="Seg_9790" s="T226">dempro-n:case</ta>
            <ta e="T228" id="Seg_9791" s="T227">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_9792" s="T229">adv</ta>
            <ta e="T232" id="Seg_9793" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_9794" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_9795" s="T233">pers</ta>
            <ta e="T235" id="Seg_9796" s="T234">v-v&gt;v-v:pn</ta>
            <ta e="T237" id="Seg_9797" s="T236">adv</ta>
            <ta e="T239" id="Seg_9798" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_9799" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_9800" s="T240">adv</ta>
            <ta e="T242" id="Seg_9801" s="T241">adj-n:case</ta>
            <ta e="T243" id="Seg_9802" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_9803" s="T243">conj</ta>
            <ta e="T245" id="Seg_9804" s="T244">pers</ta>
            <ta e="T246" id="Seg_9805" s="T245">adv</ta>
            <ta e="T247" id="Seg_9806" s="T246">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_9807" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_9808" s="T248">adv</ta>
            <ta e="T250" id="Seg_9809" s="T249">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_9810" s="T251">adv</ta>
            <ta e="T253" id="Seg_9811" s="T252">adj-n:case</ta>
            <ta e="T254" id="Seg_9812" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_9813" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9814" s="T255">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_9815" s="T256">pers</ta>
            <ta e="T258" id="Seg_9816" s="T257">n-n:case-n:case.poss</ta>
            <ta e="T260" id="Seg_9817" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_9818" s="T260">dempro-n:case</ta>
            <ta e="T262" id="Seg_9819" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_9820" s="T262">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_9821" s="T264">que</ta>
            <ta e="T266" id="Seg_9822" s="T265">v-v:n.fin</ta>
            <ta e="T267" id="Seg_9823" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_9824" s="T267">v-v:mood.pn</ta>
            <ta e="T269" id="Seg_9825" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_9826" s="T269">pers</ta>
            <ta e="T271" id="Seg_9827" s="T270">v-v:n.fin</ta>
            <ta e="T272" id="Seg_9828" s="T271">adv</ta>
            <ta e="T274" id="Seg_9829" s="T272">adv</ta>
            <ta e="T275" id="Seg_9830" s="T274">adv</ta>
            <ta e="T276" id="Seg_9831" s="T275">v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_9832" s="T276">propr-n:ins-n:case</ta>
            <ta e="T278" id="Seg_9833" s="T277">n-n:case.poss</ta>
            <ta e="T279" id="Seg_9834" s="T278">adv</ta>
            <ta e="T280" id="Seg_9835" s="T279">dempro-n:case</ta>
            <ta e="T281" id="Seg_9836" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_9837" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_9838" s="T282">ptcl</ta>
            <ta e="T285" id="Seg_9839" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_9840" s="T285">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_9841" s="T287">dempro-n:num</ta>
            <ta e="T289" id="Seg_9842" s="T288">adv</ta>
            <ta e="T290" id="Seg_9843" s="T289">n-n:num</ta>
            <ta e="T291" id="Seg_9844" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_9845" s="T291">conj</ta>
            <ta e="T293" id="Seg_9846" s="T292">adv</ta>
            <ta e="T294" id="Seg_9847" s="T293">v-v:n.fin</ta>
            <ta e="T295" id="Seg_9848" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_9849" s="T295">que</ta>
            <ta e="T297" id="Seg_9850" s="T296">adj-n:case</ta>
            <ta e="T298" id="Seg_9851" s="T297">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_9852" s="T299">dempro-n:num</ta>
            <ta e="T301" id="Seg_9853" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_9854" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_9855" s="T302">propr-n:case</ta>
            <ta e="T304" id="Seg_9856" s="T303">v-v&gt;v-v:pn</ta>
            <ta e="T305" id="Seg_9857" s="T304">adv</ta>
            <ta e="T306" id="Seg_9858" s="T305">adv</ta>
            <ta e="T307" id="Seg_9859" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_9860" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_9861" s="T308">v-v:tense-v:pn</ta>
            <ta e="T310" id="Seg_9862" s="T309">dempro-n:case</ta>
            <ta e="T311" id="Seg_9863" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_9864" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_9865" s="T312">adv</ta>
            <ta e="T314" id="Seg_9866" s="T313">n-n:num-n:case.poss</ta>
            <ta e="T315" id="Seg_9867" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_9868" s="T315">adv</ta>
            <ta e="T318" id="Seg_9869" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_9870" s="T318">v-v:n.fin</ta>
            <ta e="T320" id="Seg_9871" s="T319">que</ta>
            <ta e="T321" id="Seg_9872" s="T320">adj-n:case</ta>
            <ta e="T322" id="Seg_9873" s="T321">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_9874" s="T322">dempro-n:num</ta>
            <ta e="T324" id="Seg_9875" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_9876" s="T324">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_9877" s="T325">adv</ta>
            <ta e="T327" id="Seg_9878" s="T326">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_9879" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_9880" s="T328">adv</ta>
            <ta e="T330" id="Seg_9881" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_9882" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_9883" s="T331">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_9884" s="T332">propr-n:case</ta>
            <ta e="T334" id="Seg_9885" s="T333">v-v:tense-v:pn</ta>
            <ta e="T335" id="Seg_9886" s="T334">adv</ta>
            <ta e="T336" id="Seg_9887" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_9888" s="T336">quant</ta>
            <ta e="T339" id="Seg_9889" s="T338">num-n:case</ta>
            <ta e="T340" id="Seg_9890" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_9891" s="T340">adv</ta>
            <ta e="T342" id="Seg_9892" s="T341">conj</ta>
            <ta e="T343" id="Seg_9893" s="T342">num-n:case</ta>
            <ta e="T344" id="Seg_9894" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_9895" s="T344">adv</ta>
            <ta e="T347" id="Seg_9896" s="T346">v-v:ins-v:mood.pn</ta>
            <ta e="T348" id="Seg_9897" s="T347">adv</ta>
            <ta e="T349" id="Seg_9898" s="T348">adv</ta>
            <ta e="T350" id="Seg_9899" s="T349">adv</ta>
            <ta e="T351" id="Seg_9900" s="T350">aux-v:pn</ta>
            <ta e="T352" id="Seg_9901" s="T351">v-v:ins-v:mood.pn</ta>
            <ta e="T353" id="Seg_9902" s="T352">conj</ta>
            <ta e="T354" id="Seg_9903" s="T353">adv</ta>
            <ta e="T355" id="Seg_9904" s="T354">aux-v:pn</ta>
            <ta e="T356" id="Seg_9905" s="T355">v-v:ins-v:mood.pn</ta>
            <ta e="T358" id="Seg_9906" s="T357">que</ta>
            <ta e="T359" id="Seg_9907" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_9908" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_9909" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_9910" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_9911" s="T362">n-n:case.poss</ta>
            <ta e="T364" id="Seg_9912" s="T363">dempro-n:case</ta>
            <ta e="T365" id="Seg_9913" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_9914" s="T365">adv</ta>
            <ta e="T367" id="Seg_9915" s="T366">adj-n:case</ta>
            <ta e="T369" id="Seg_9916" s="T368">v-v&gt;v-v:pn</ta>
            <ta e="T370" id="Seg_9917" s="T369">adv</ta>
            <ta e="T371" id="Seg_9918" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_9919" s="T371">que-n:case</ta>
            <ta e="T373" id="Seg_9920" s="T372">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_9921" s="T374">n-n:num</ta>
            <ta e="T376" id="Seg_9922" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_9923" s="T376">v-v:tense-v:pn</ta>
            <ta e="T378" id="Seg_9924" s="T377">pers</ta>
            <ta e="T379" id="Seg_9925" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_9926" s="T379">v-v:n.fin</ta>
            <ta e="T382" id="Seg_9927" s="T381">dempro-n:num</ta>
            <ta e="T383" id="Seg_9928" s="T382">pers</ta>
            <ta e="T385" id="Seg_9929" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_9930" s="T385">v-v&gt;v-v:pn</ta>
            <ta e="T388" id="Seg_9931" s="T387">v-v:mood.pn</ta>
            <ta e="T389" id="Seg_9932" s="T388">conj</ta>
            <ta e="T390" id="Seg_9933" s="T389">pers</ta>
            <ta e="T391" id="Seg_9934" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_9935" s="T391">pers</ta>
            <ta e="T393" id="Seg_9936" s="T392">pers</ta>
            <ta e="T394" id="Seg_9937" s="T393">que-n:case</ta>
            <ta e="T395" id="Seg_9938" s="T394">dempro-n:case</ta>
            <ta e="T396" id="Seg_9939" s="T395">v-v:tense-v:pn</ta>
            <ta e="T401" id="Seg_9940" s="T400">pers</ta>
            <ta e="T402" id="Seg_9941" s="T401">adv</ta>
            <ta e="T403" id="Seg_9942" s="T402">adj-n:case</ta>
            <ta e="T405" id="Seg_9943" s="T404">n-n:case.poss</ta>
            <ta e="T406" id="Seg_9944" s="T405">conj</ta>
            <ta e="T407" id="Seg_9945" s="T406">pers</ta>
            <ta e="T408" id="Seg_9946" s="T407">adj-n:case</ta>
            <ta e="T409" id="Seg_9947" s="T408">n-n:case</ta>
            <ta e="T411" id="Seg_9948" s="T410">pers</ta>
            <ta e="T412" id="Seg_9949" s="T411">ptcl</ta>
            <ta e="T414" id="Seg_9950" s="T413">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_9951" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_9952" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_9953" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_9954" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_9955" s="T419">pers</ta>
            <ta e="T421" id="Seg_9956" s="T420">v-v:mood.pn</ta>
            <ta e="T422" id="Seg_9957" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_9958" s="T422">que</ta>
            <ta e="T424" id="Seg_9959" s="T423">dempro-n:case</ta>
            <ta e="T425" id="Seg_9960" s="T424">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_9961" s="T425">ptcl</ta>
            <ta e="T428" id="Seg_9962" s="T426">pers</ta>
            <ta e="T430" id="Seg_9963" s="T429">pers</ta>
            <ta e="T431" id="Seg_9964" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_9965" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_9966" s="T432">adv</ta>
            <ta e="T434" id="Seg_9967" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_9968" s="T434">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_9969" s="T436">pers</ta>
            <ta e="T438" id="Seg_9970" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_9971" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_9972" s="T439">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_9973" s="T440">conj</ta>
            <ta e="T442" id="Seg_9974" s="T441">pers</ta>
            <ta e="T443" id="Seg_9975" s="T442">v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_9976" s="T443">pers-n:case</ta>
            <ta e="T445" id="Seg_9977" s="T444">conj</ta>
            <ta e="T446" id="Seg_9978" s="T445">pers</ta>
            <ta e="T447" id="Seg_9979" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_9980" s="T447">v-v:pn</ta>
            <ta e="T449" id="Seg_9981" s="T448">pers</ta>
            <ta e="T450" id="Seg_9982" s="T449">v-v:pn</ta>
            <ta e="T451" id="Seg_9983" s="T450">conj</ta>
            <ta e="T452" id="Seg_9984" s="T451">v-v:n.fin</ta>
            <ta e="T453" id="Seg_9985" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_9986" s="T453">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_9987" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_9988" s="T455">pers</ta>
            <ta e="T457" id="Seg_9989" s="T456">n-n:case.poss</ta>
            <ta e="T458" id="Seg_9990" s="T457">v&gt;v-v-v:n.fin</ta>
            <ta e="T459" id="Seg_9991" s="T458">conj</ta>
            <ta e="T460" id="Seg_9992" s="T459">v-v:n.fin</ta>
            <ta e="T462" id="Seg_9993" s="T461">dempro-n:num</ta>
            <ta e="T463" id="Seg_9994" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_9995" s="T463">adv</ta>
            <ta e="T465" id="Seg_9996" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_9997" s="T465">n-n:num</ta>
            <ta e="T467" id="Seg_9998" s="T466">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_9999" s="T469">adv</ta>
            <ta e="T471" id="Seg_10000" s="T470">adj-n:case</ta>
            <ta e="T472" id="Seg_10001" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_10002" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_10003" s="T473">adv</ta>
            <ta e="T475" id="Seg_10004" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_10005" s="T475">v-v:tense-v:pn</ta>
            <ta e="T477" id="Seg_10006" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_10007" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_10008" s="T478">v-v&gt;v-v:pn</ta>
            <ta e="T480" id="Seg_10009" s="T479">conj</ta>
            <ta e="T481" id="Seg_10010" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_10011" s="T481">v-v&gt;v-v:pn</ta>
            <ta e="T484" id="Seg_10012" s="T482">ptcl</ta>
            <ta e="T486" id="Seg_10013" s="T484">n-n:case</ta>
            <ta e="T487" id="Seg_10014" s="T486">conj</ta>
            <ta e="T488" id="Seg_10015" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_10016" s="T488">ptcl</ta>
            <ta e="T491" id="Seg_10017" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_10018" s="T491">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_10019" s="T492">pers</ta>
            <ta e="T494" id="Seg_10020" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_10021" s="T494">n-n:case</ta>
            <ta e="T496" id="Seg_10022" s="T495">aux-v:mood.pn</ta>
            <ta e="T497" id="Seg_10023" s="T496">v-v:mood.pn</ta>
            <ta e="T498" id="Seg_10024" s="T497">pers</ta>
            <ta e="T499" id="Seg_10025" s="T498">aux-v:mood.pn</ta>
            <ta e="T500" id="Seg_10026" s="T499">v-v:mood.pn</ta>
            <ta e="T501" id="Seg_10027" s="T500">pers</ta>
            <ta e="T502" id="Seg_10028" s="T501">aux-v:mood.pn</ta>
            <ta e="T503" id="Seg_10029" s="T502">v-v:mood.pn</ta>
            <ta e="T504" id="Seg_10030" s="T503">pers</ta>
            <ta e="T505" id="Seg_10031" s="T504">aux-v:mood.pn</ta>
            <ta e="T506" id="Seg_10032" s="T505">v-v:mood.pn</ta>
            <ta e="T507" id="Seg_10033" s="T506">pers</ta>
            <ta e="T508" id="Seg_10034" s="T507">pers</ta>
            <ta e="T509" id="Seg_10035" s="T508">v-v:mood.pn</ta>
            <ta e="T510" id="Seg_10036" s="T509">pers</ta>
            <ta e="T511" id="Seg_10037" s="T510">n-n:case.poss</ta>
            <ta e="T512" id="Seg_10038" s="T511">n-n&gt;adj-n:case</ta>
            <ta e="T513" id="Seg_10039" s="T512">pers</ta>
            <ta e="T514" id="Seg_10040" s="T513">v-v:mood.pn</ta>
            <ta e="T515" id="Seg_10041" s="T514">pers</ta>
            <ta e="T516" id="Seg_10042" s="T515">n-n:case.poss</ta>
            <ta e="T517" id="Seg_10043" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_10044" s="T517">quant</ta>
            <ta e="T519" id="Seg_10045" s="T518">v-v&gt;v-v:mood.pn</ta>
            <ta e="T520" id="Seg_10046" s="T519">pers</ta>
            <ta e="T521" id="Seg_10047" s="T520">v-v:n.fin</ta>
            <ta e="T522" id="Seg_10048" s="T521">pers-n:case</ta>
            <ta e="T523" id="Seg_10049" s="T522">v-v&gt;v-v:mood.pn</ta>
            <ta e="T524" id="Seg_10050" s="T523">pers</ta>
            <ta e="T525" id="Seg_10051" s="T524">pers</ta>
            <ta e="T527" id="Seg_10052" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_10053" s="T527">v-v:n.fin</ta>
            <ta e="T530" id="Seg_10054" s="T529">pers</ta>
            <ta e="T532" id="Seg_10055" s="T531">n-n:case-n:num</ta>
            <ta e="T533" id="Seg_10056" s="T532">v-v:n.fin</ta>
            <ta e="T538" id="Seg_10057" s="T537">propr.[n:case]</ta>
            <ta e="T539" id="Seg_10058" s="T538">v-v:ins-v:mood.pn</ta>
            <ta e="T540" id="Seg_10059" s="T539">n</ta>
            <ta e="T541" id="Seg_10060" s="T540">n-n:case-n:case.poss</ta>
            <ta e="T542" id="Seg_10061" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_10062" s="T542">v-v:mood.pn</ta>
            <ta e="T544" id="Seg_10063" s="T543">dempro</ta>
            <ta e="T545" id="Seg_10064" s="T544">n-n:ins-n:case</ta>
            <ta e="T546" id="Seg_10065" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_10066" s="T546">n-n:num</ta>
            <ta e="T548" id="Seg_10067" s="T547">n-n:case.poss</ta>
            <ta e="T550" id="Seg_10068" s="T549">v-v:n.fin</ta>
            <ta e="T551" id="Seg_10069" s="T550">pers</ta>
            <ta e="T552" id="Seg_10070" s="T551">adv</ta>
            <ta e="T554" id="Seg_10071" s="T553">adj</ta>
            <ta e="T555" id="Seg_10072" s="T554">v-v:mood.pn</ta>
            <ta e="T557" id="Seg_10073" s="T556">pers</ta>
            <ta e="T558" id="Seg_10074" s="T557">adv</ta>
            <ta e="T559" id="Seg_10075" s="T558">n-n:case</ta>
            <ta e="T564" id="Seg_10076" s="T563">v-v:tense-v:pn</ta>
            <ta e="T565" id="Seg_10077" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_10078" s="T565">v-v:n.fin</ta>
            <ta e="T568" id="Seg_10079" s="T567">propr-n:case</ta>
            <ta e="T569" id="Seg_10080" s="T568">n-n:case.poss-n:case</ta>
            <ta e="T570" id="Seg_10081" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_10082" s="T570">v-v&gt;v-v:pn</ta>
            <ta e="T572" id="Seg_10083" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_10084" s="T572">%%</ta>
            <ta e="T574" id="Seg_10085" s="T573">que</ta>
            <ta e="T575" id="Seg_10086" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_10087" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_10088" s="T576">adj-n:case</ta>
            <ta e="T578" id="Seg_10089" s="T577">conj</ta>
            <ta e="T579" id="Seg_10090" s="T578">pers</ta>
            <ta e="T580" id="Seg_10091" s="T579">v-v:tense-v:pn</ta>
            <ta e="T581" id="Seg_10092" s="T580">adv</ta>
            <ta e="T582" id="Seg_10093" s="T581">v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_10094" s="T582">dempro-n:case</ta>
            <ta e="T584" id="Seg_10095" s="T583">v-v&gt;v-v:pn</ta>
            <ta e="T585" id="Seg_10096" s="T584">num-n:case</ta>
            <ta e="T586" id="Seg_10097" s="T585">num-n:case</ta>
            <ta e="T587" id="Seg_10098" s="T586">num-n:case</ta>
            <ta e="T588" id="Seg_10099" s="T587">num-n:case</ta>
            <ta e="T589" id="Seg_10100" s="T588">num-n:case</ta>
            <ta e="T590" id="Seg_10101" s="T589">num-n:case</ta>
            <ta e="T592" id="Seg_10102" s="T591">num-n:case</ta>
            <ta e="T593" id="Seg_10103" s="T592">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_10104" s="T594">n-n:num</ta>
            <ta e="T596" id="Seg_10105" s="T595">pers</ta>
            <ta e="T597" id="Seg_10106" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_10107" s="T597">adv</ta>
            <ta e="T600" id="Seg_10108" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_10109" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_10110" s="T601">num-num&gt;num</ta>
            <ta e="T603" id="Seg_10111" s="T602">n-n:case-%%</ta>
            <ta e="T604" id="Seg_10112" s="T603">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_10113" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_10114" s="T605">propr-n:case</ta>
            <ta e="T607" id="Seg_10115" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_10116" s="T607">n-n:case</ta>
            <ta e="T609" id="Seg_10117" s="T608">v-v:tense-v:pn</ta>
            <ta e="T610" id="Seg_10118" s="T609">conj</ta>
            <ta e="T611" id="Seg_10119" s="T610">n-n:case.poss</ta>
            <ta e="T612" id="Seg_10120" s="T611">v-v:tense-v:pn</ta>
            <ta e="T613" id="Seg_10121" s="T612">n-n:num</ta>
            <ta e="T614" id="Seg_10122" s="T613">num-n:case</ta>
            <ta e="T615" id="Seg_10123" s="T614">num-n:case</ta>
            <ta e="T616" id="Seg_10124" s="T615">num-n:case</ta>
            <ta e="T617" id="Seg_10125" s="T616">v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_10126" s="T617">conj</ta>
            <ta e="T619" id="Seg_10127" s="T618">pers</ta>
            <ta e="T621" id="Seg_10128" s="T620">n-n:case</ta>
            <ta e="T622" id="Seg_10129" s="T621">v-v:tense-v:pn</ta>
            <ta e="T624" id="Seg_10130" s="T623">pers</ta>
            <ta e="T625" id="Seg_10131" s="T624">adv</ta>
            <ta e="T626" id="Seg_10132" s="T625">n-n:case.poss</ta>
            <ta e="T627" id="Seg_10133" s="T626">adv</ta>
            <ta e="T628" id="Seg_10134" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_10135" s="T628">quant</ta>
            <ta e="T630" id="Seg_10136" s="T629">n-n:case</ta>
            <ta e="T631" id="Seg_10137" s="T630">v-v:tense-v:pn</ta>
            <ta e="T632" id="Seg_10138" s="T631">num-n:case</ta>
            <ta e="T633" id="Seg_10139" s="T632">num-n:case</ta>
            <ta e="T634" id="Seg_10140" s="T633">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_10141" s="T634">adv</ta>
            <ta e="T636" id="Seg_10142" s="T635">num-n:case</ta>
            <ta e="T637" id="Seg_10143" s="T636">adv</ta>
            <ta e="T638" id="Seg_10144" s="T637">num-n:case</ta>
            <ta e="T639" id="Seg_10145" s="T638">num-n:case</ta>
            <ta e="T640" id="Seg_10146" s="T639">n-n:case</ta>
            <ta e="T642" id="Seg_10147" s="T641">num-n:case</ta>
            <ta e="T643" id="Seg_10148" s="T642">num-n:case</ta>
            <ta e="T644" id="Seg_10149" s="T643">v-v:tense-v:pn</ta>
            <ta e="T645" id="Seg_10150" s="T644">conj</ta>
            <ta e="T646" id="Seg_10151" s="T645">num-n:case</ta>
            <ta e="T647" id="Seg_10152" s="T646">adv</ta>
            <ta e="T649" id="Seg_10153" s="T648">n-n:num</ta>
            <ta e="T650" id="Seg_10154" s="T649">pers</ta>
            <ta e="T651" id="Seg_10155" s="T650">n-n:case.poss</ta>
            <ta e="T652" id="Seg_10156" s="T651">v-v&gt;v-v:pn</ta>
            <ta e="T653" id="Seg_10157" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10158" s="T653">pers</ta>
            <ta e="T655" id="Seg_10159" s="T654">v-v:n.fin</ta>
            <ta e="T656" id="Seg_10160" s="T655">v-v:n.fin</ta>
            <ta e="T657" id="Seg_10161" s="T656">pers</ta>
            <ta e="T658" id="Seg_10162" s="T657">que</ta>
            <ta e="T659" id="Seg_10163" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_10164" s="T659">v-v:mood.pn</ta>
            <ta e="T661" id="Seg_10165" s="T660">adv</ta>
            <ta e="T662" id="Seg_10166" s="T661">dempro-n:num</ta>
            <ta e="T663" id="Seg_10167" s="T662">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_10168" s="T663">v-v:mood.pn</ta>
            <ta e="T666" id="Seg_10169" s="T665">pers</ta>
            <ta e="T667" id="Seg_10170" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_10171" s="T667">adj-n:case</ta>
            <ta e="T669" id="Seg_10172" s="T668">n-n:num</ta>
            <ta e="T670" id="Seg_10173" s="T669">v-v&gt;v-v:pn</ta>
            <ta e="T671" id="Seg_10174" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_10175" s="T671">v-v&gt;v-v:pn</ta>
            <ta e="T674" id="Seg_10176" s="T673">v-v:mood.pn</ta>
            <ta e="T675" id="Seg_10177" s="T674">n-n:case.poss</ta>
            <ta e="T677" id="Seg_10178" s="T676">adv</ta>
            <ta e="T678" id="Seg_10179" s="T677">n-n:num</ta>
            <ta e="T679" id="Seg_10180" s="T678">pers</ta>
            <ta e="T680" id="Seg_10181" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_10182" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_10183" s="T681">v-v:n.fin</ta>
            <ta e="T683" id="Seg_10184" s="T682">quant</ta>
            <ta e="T684" id="Seg_10185" s="T683">v-v:tense-v:pn</ta>
            <ta e="T685" id="Seg_10186" s="T684">adv</ta>
            <ta e="T686" id="Seg_10187" s="T685">conj</ta>
            <ta e="T688" id="Seg_10188" s="T687">n-n:ins-n:case</ta>
            <ta e="T689" id="Seg_10189" s="T688">n-n:case.poss</ta>
            <ta e="T690" id="Seg_10190" s="T689">v-v:tense-v:pn</ta>
            <ta e="T691" id="Seg_10191" s="T690">adv</ta>
            <ta e="T692" id="Seg_10192" s="T691">n-n:case.poss</ta>
            <ta e="T693" id="Seg_10193" s="T692">v-v:tense-v:pn</ta>
            <ta e="T695" id="Seg_10194" s="T694">dempro-n:case</ta>
            <ta e="T696" id="Seg_10195" s="T695">v-v&gt;v-v:pn</ta>
            <ta e="T697" id="Seg_10196" s="T696">v-v:mood-v:pn</ta>
            <ta e="T698" id="Seg_10197" s="T697">n-n:case.poss</ta>
            <ta e="T699" id="Seg_10198" s="T698">pers</ta>
            <ta e="T700" id="Seg_10199" s="T699">n-n:case.poss</ta>
            <ta e="T701" id="Seg_10200" s="T700">v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_10201" s="T701">conj</ta>
            <ta e="T703" id="Seg_10202" s="T702">pers</ta>
            <ta e="T704" id="Seg_10203" s="T703">v-v:tense-v:pn</ta>
            <ta e="T705" id="Seg_10204" s="T704">dempro-n:case</ta>
            <ta e="T706" id="Seg_10205" s="T705">v-v:mood.pn</ta>
            <ta e="T707" id="Seg_10206" s="T706">n-n:case-n:case.poss</ta>
            <ta e="T708" id="Seg_10207" s="T707">que=ptcl</ta>
            <ta e="T709" id="Seg_10208" s="T708">adj-n:case</ta>
            <ta e="T710" id="Seg_10209" s="T709">n-n:case</ta>
            <ta e="T712" id="Seg_10210" s="T711">v-v:mood.pn</ta>
            <ta e="T713" id="Seg_10211" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_10212" s="T713">v-v:mood.pn</ta>
            <ta e="T715" id="Seg_10213" s="T714">adj-n:case</ta>
            <ta e="T716" id="Seg_10214" s="T715">dempro-n:num</ta>
            <ta e="T717" id="Seg_10215" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_10216" s="T717">v-v&gt;v-v:pn</ta>
            <ta e="T719" id="Seg_10217" s="T718">conj</ta>
            <ta e="T720" id="Seg_10218" s="T719">pers</ta>
            <ta e="T721" id="Seg_10219" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_10220" s="T721">v-v&gt;v-v:pn</ta>
            <ta e="T723" id="Seg_10221" s="T722">dempro-n:num</ta>
            <ta e="T724" id="Seg_10222" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_10223" s="T724">v-v:pn</ta>
            <ta e="T726" id="Seg_10224" s="T725">v-v:n.fin</ta>
            <ta e="T732" id="Seg_10225" s="T731">adv</ta>
            <ta e="T733" id="Seg_10226" s="T732">n-n:case</ta>
            <ta e="T734" id="Seg_10227" s="T733">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_10228" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_10229" s="T735">pers</ta>
            <ta e="T737" id="Seg_10230" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_10231" s="T737">v-v:n.fin</ta>
            <ta e="T739" id="Seg_10232" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_10233" s="T739">adv</ta>
            <ta e="T741" id="Seg_10234" s="T740">refl-n:case.poss</ta>
            <ta e="T742" id="Seg_10235" s="T741">n-n:case</ta>
            <ta e="T743" id="Seg_10236" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_10237" s="T743">v-v:tense-v:pn</ta>
            <ta e="T745" id="Seg_10238" s="T744">pers</ta>
            <ta e="T746" id="Seg_10239" s="T745">n-n:case</ta>
            <ta e="T748" id="Seg_10240" s="T747">pers</ta>
            <ta e="T749" id="Seg_10241" s="T748">v-v:tense-v:pn</ta>
            <ta e="T750" id="Seg_10242" s="T749">propr-n:case</ta>
            <ta e="T751" id="Seg_10243" s="T750">v</ta>
            <ta e="T752" id="Seg_10244" s="T751">v-v:tense-v:pn</ta>
            <ta e="T753" id="Seg_10245" s="T752">n-n:case</ta>
            <ta e="T754" id="Seg_10246" s="T753">n-n:case.poss</ta>
            <ta e="T755" id="Seg_10247" s="T754">v</ta>
            <ta e="T756" id="Seg_10248" s="T755">v-v:n.fin</ta>
            <ta e="T757" id="Seg_10249" s="T756">conj</ta>
            <ta e="T758" id="Seg_10250" s="T757">v-v&gt;v-v:n.fin</ta>
            <ta e="T759" id="Seg_10251" s="T758">adv-adv&gt;adv</ta>
            <ta e="T760" id="Seg_10252" s="T759">dempro-n:num</ta>
            <ta e="T761" id="Seg_10253" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_10254" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_10255" s="T762">v-v&gt;v-v:pn</ta>
            <ta e="T764" id="Seg_10256" s="T763">v-v:ins-v:mood.pn</ta>
            <ta e="T765" id="Seg_10257" s="T764">n-n:case.poss</ta>
            <ta e="T766" id="Seg_10258" s="T765">v-v&gt;v-v&gt;v</ta>
            <ta e="T767" id="Seg_10259" s="T766">adv</ta>
            <ta e="T768" id="Seg_10260" s="T767">v-v:tense-v:pn</ta>
            <ta e="T769" id="Seg_10261" s="T768">conj</ta>
            <ta e="T770" id="Seg_10262" s="T769">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_10263" s="T771">adv</ta>
            <ta e="T773" id="Seg_10264" s="T772">v-v:tense-v:pn</ta>
            <ta e="T774" id="Seg_10265" s="T773">dempro-n:num</ta>
            <ta e="T775" id="Seg_10266" s="T774">ptcl</ta>
            <ta e="T776" id="Seg_10267" s="T775">v-v&gt;v-v:pn</ta>
            <ta e="T777" id="Seg_10268" s="T776">que-n:case</ta>
            <ta e="T778" id="Seg_10269" s="T777">v-v&gt;v-v:pn</ta>
            <ta e="T779" id="Seg_10270" s="T778">aux-v:mood.pn</ta>
            <ta e="T780" id="Seg_10271" s="T779">v-v:pn</ta>
            <ta e="T781" id="Seg_10272" s="T780">adj-adj&gt;adv</ta>
            <ta e="T782" id="Seg_10273" s="T781">v-v:mood.pn</ta>
            <ta e="T783" id="Seg_10274" s="T782">n-n:case</ta>
            <ta e="T784" id="Seg_10275" s="T783">v-v:ins-v:mood.pn</ta>
            <ta e="T786" id="Seg_10276" s="T785">num-n:case</ta>
            <ta e="T787" id="Seg_10277" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_10278" s="T787">v-v:ins-v:mood.pn</ta>
            <ta e="T789" id="Seg_10279" s="T788">n-n:case.poss</ta>
            <ta e="T790" id="Seg_10280" s="T789">v-v&gt;v-v:mood.pn</ta>
            <ta e="T791" id="Seg_10281" s="T790">conj</ta>
            <ta e="T792" id="Seg_10282" s="T791">dempro-n:case</ta>
            <ta e="T793" id="Seg_10283" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_10284" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_10285" s="T794">v-v:tense-v:pn</ta>
            <ta e="T796" id="Seg_10286" s="T795">n-n:case</ta>
            <ta e="T798" id="Seg_10287" s="T797">pers</ta>
            <ta e="T799" id="Seg_10288" s="T798">adv</ta>
            <ta e="T800" id="Seg_10289" s="T799">adv</ta>
            <ta e="T801" id="Seg_10290" s="T800">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_10291" s="T801">adv</ta>
            <ta e="T804" id="Seg_10292" s="T803">num-n:case</ta>
            <ta e="T805" id="Seg_10293" s="T804">v-v:tense-v:pn</ta>
            <ta e="T806" id="Seg_10294" s="T805">adv</ta>
            <ta e="T807" id="Seg_10295" s="T806">n-n:case.poss</ta>
            <ta e="T808" id="Seg_10296" s="T807">v-v:tense-v:pn</ta>
            <ta e="T809" id="Seg_10297" s="T808">n</ta>
            <ta e="T810" id="Seg_10298" s="T809">v-v:tense-v:pn</ta>
            <ta e="T811" id="Seg_10299" s="T810">adv</ta>
            <ta e="T812" id="Seg_10300" s="T811">adv</ta>
            <ta e="T813" id="Seg_10301" s="T812">v-v:tense-v:pn</ta>
            <ta e="T814" id="Seg_10302" s="T813">adv</ta>
            <ta e="T815" id="Seg_10303" s="T814">v-v:tense-v:pn</ta>
            <ta e="T816" id="Seg_10304" s="T815">adv</ta>
            <ta e="T817" id="Seg_10305" s="T816">num-n:case</ta>
            <ta e="T818" id="Seg_10306" s="T817">v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_10307" s="T818">adv</ta>
            <ta e="T821" id="Seg_10308" s="T820">v-v:tense-v:pn</ta>
            <ta e="T823" id="Seg_10309" s="T822">adj</ta>
            <ta e="T824" id="Seg_10310" s="T823">adv</ta>
            <ta e="T825" id="Seg_10311" s="T824">v-v:tense-v:pn</ta>
            <ta e="T826" id="Seg_10312" s="T825">conj</ta>
            <ta e="T827" id="Seg_10313" s="T826">adv</ta>
            <ta e="T828" id="Seg_10314" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_10315" s="T828">adj</ta>
            <ta e="T831" id="Seg_10316" s="T830">dempro-n:case</ta>
            <ta e="T832" id="Seg_10317" s="T831">n-n:case</ta>
            <ta e="T833" id="Seg_10318" s="T832">adv</ta>
            <ta e="T834" id="Seg_10319" s="T833">adj-n:case</ta>
            <ta e="T835" id="Seg_10320" s="T834">adj</ta>
            <ta e="T836" id="Seg_10321" s="T835">conj</ta>
            <ta e="T837" id="Seg_10322" s="T836">dempro-n:case</ta>
            <ta e="T838" id="Seg_10323" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_10324" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_10325" s="T839">adj</ta>
            <ta e="T842" id="Seg_10326" s="T841">dempro</ta>
            <ta e="T844" id="Seg_10327" s="T843">dempro</ta>
            <ta e="T845" id="Seg_10328" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_10329" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_10330" s="T846">n-n:num-n:case.poss</ta>
            <ta e="T848" id="Seg_10331" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_10332" s="T848">adv-v-v:tense</ta>
            <ta e="T850" id="Seg_10333" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_10334" s="T850">n-n:case</ta>
            <ta e="T852" id="Seg_10335" s="T851">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T853" id="Seg_10336" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_10337" s="T853">dempro-n:case</ta>
            <ta e="T855" id="Seg_10338" s="T854">v-v:n.fin</ta>
            <ta e="T857" id="Seg_10339" s="T856">v-v:n.fin</ta>
            <ta e="T859" id="Seg_10340" s="T858">v-v:mood.pn</ta>
            <ta e="T860" id="Seg_10341" s="T859">pers</ta>
            <ta e="T861" id="Seg_10342" s="T860">n-n:case</ta>
            <ta e="T862" id="Seg_10343" s="T861">num-n:case</ta>
            <ta e="T863" id="Seg_10344" s="T862">n-n:case</ta>
            <ta e="T865" id="Seg_10345" s="T864">dempro-n:case</ta>
            <ta e="T866" id="Seg_10346" s="T865">n-n:case</ta>
            <ta e="T867" id="Seg_10347" s="T866">v-v:tense-v:pn</ta>
            <ta e="T868" id="Seg_10348" s="T867">conj</ta>
            <ta e="T869" id="Seg_10349" s="T868">adv</ta>
            <ta e="T870" id="Seg_10350" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_10351" s="T870">v-v:tense-v:pn</ta>
            <ta e="T872" id="Seg_10352" s="T871">v-v:tense-v:pn</ta>
            <ta e="T873" id="Seg_10353" s="T872">n-n:case</ta>
            <ta e="T874" id="Seg_10354" s="T873">v-v:tense-v:pn</ta>
            <ta e="T875" id="Seg_10355" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_10356" s="T875">v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_10357" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_10358" s="T877">adj-n:case</ta>
            <ta e="T879" id="Seg_10359" s="T878">v-v&gt;v-v:n.fin</ta>
            <ta e="T880" id="Seg_10360" s="T879">v-v:tense-v:pn</ta>
            <ta e="T881" id="Seg_10361" s="T880">conj</ta>
            <ta e="T882" id="Seg_10362" s="T881">v-v:tense-v:pn</ta>
            <ta e="T883" id="Seg_10363" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_10364" s="T883">adj-n:case</ta>
            <ta e="T885" id="Seg_10365" s="T884">v-v:n.fin</ta>
            <ta e="T887" id="Seg_10366" s="T886">dempro</ta>
            <ta e="T888" id="Seg_10367" s="T887">dempro-n:case</ta>
            <ta e="T889" id="Seg_10368" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_10369" s="T889">n-n:case</ta>
            <ta e="T891" id="Seg_10370" s="T890">v-v:tense-v:pn</ta>
            <ta e="T892" id="Seg_10371" s="T891">dempro-n:case</ta>
            <ta e="T893" id="Seg_10372" s="T892">v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_10373" s="T894">num-n:case</ta>
            <ta e="T896" id="Seg_10374" s="T895">n-n:case</ta>
            <ta e="T897" id="Seg_10375" s="T896">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T898" id="Seg_10376" s="T897">conj</ta>
            <ta e="T899" id="Seg_10377" s="T898">adv</ta>
            <ta e="T900" id="Seg_10378" s="T899">n-n:case.poss</ta>
            <ta e="T901" id="Seg_10379" s="T900">v-v:tense-v:pn</ta>
            <ta e="T902" id="Seg_10380" s="T901">conj</ta>
            <ta e="T903" id="Seg_10381" s="T902">v-v&gt;v-v:pn</ta>
            <ta e="T905" id="Seg_10382" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_10383" s="T905">n-n:case.poss</ta>
            <ta e="T910" id="Seg_10384" s="T909">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T913" id="Seg_10385" s="T912">pers</ta>
            <ta e="T916" id="Seg_10386" s="T915">n-n:case.poss</ta>
            <ta e="T917" id="Seg_10387" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_10388" s="T917">n-n:case</ta>
            <ta e="T919" id="Seg_10389" s="T918">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T920" id="Seg_10390" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_10391" s="T920">ptcl</ta>
            <ta e="T922" id="Seg_10392" s="T921">n-n:case</ta>
            <ta e="T923" id="Seg_10393" s="T922">v-v&gt;v-v:pn</ta>
            <ta e="T925" id="Seg_10394" s="T923">pers</ta>
            <ta e="T926" id="Seg_10395" s="T925">n-n:case</ta>
            <ta e="T927" id="Seg_10396" s="T926">ptcl</ta>
            <ta e="T928" id="Seg_10397" s="T927">n-n:case.poss</ta>
            <ta e="T932" id="Seg_10398" s="T931">v-v:tense-v:pn</ta>
            <ta e="T934" id="Seg_10399" s="T932">n-n:case</ta>
            <ta e="T935" id="Seg_10400" s="T934">v-v:tense-v:pn</ta>
            <ta e="T936" id="Seg_10401" s="T935">n-n:case</ta>
            <ta e="T937" id="Seg_10402" s="T936">v-v:n.fin</ta>
            <ta e="T938" id="Seg_10403" s="T937">v-v:mood-v:pn</ta>
            <ta e="T939" id="Seg_10404" s="T938">n-n:case.poss</ta>
            <ta e="T940" id="Seg_10405" s="T939">n-n:case</ta>
            <ta e="T941" id="Seg_10406" s="T940">v-v:pn</ta>
            <ta e="T942" id="Seg_10407" s="T941">adv</ta>
            <ta e="T944" id="Seg_10408" s="T943">v-v:n.fin</ta>
            <ta e="T945" id="Seg_10409" s="T944">que=ptcl</ta>
            <ta e="T946" id="Seg_10410" s="T945">v-v:tense-v:pn</ta>
            <ta e="T947" id="Seg_10411" s="T946">adv</ta>
            <ta e="T948" id="Seg_10412" s="T947">quant</ta>
            <ta e="T949" id="Seg_10413" s="T948">n-n:case</ta>
            <ta e="T950" id="Seg_10414" s="T949">adv</ta>
            <ta e="T951" id="Seg_10415" s="T950">adv</ta>
            <ta e="T952" id="Seg_10416" s="T951">v-v:tense-v:pn</ta>
            <ta e="T953" id="Seg_10417" s="T952">adv</ta>
            <ta e="T954" id="Seg_10418" s="T953">v-v:tense-v:pn</ta>
            <ta e="T955" id="Seg_10419" s="T954">que</ta>
            <ta e="T956" id="Seg_10420" s="T955">n-n:case.poss</ta>
            <ta e="T957" id="Seg_10421" s="T956">v-v:n.fin</ta>
            <ta e="T958" id="Seg_10422" s="T957">adv</ta>
            <ta e="T959" id="Seg_10423" s="T958">n-n:case</ta>
            <ta e="T960" id="Seg_10424" s="T959">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_10425" s="T4">pers</ta>
            <ta e="T6" id="Seg_10426" s="T5">adv</ta>
            <ta e="T7" id="Seg_10427" s="T6">n</ta>
            <ta e="T8" id="Seg_10428" s="T7">v</ta>
            <ta e="T9" id="Seg_10429" s="T8">adv</ta>
            <ta e="T10" id="Seg_10430" s="T9">n</ta>
            <ta e="T11" id="Seg_10431" s="T10">v</ta>
            <ta e="T12" id="Seg_10432" s="T11">v</ta>
            <ta e="T13" id="Seg_10433" s="T12">v</ta>
            <ta e="T14" id="Seg_10434" s="T13">n</ta>
            <ta e="T15" id="Seg_10435" s="T14">interj</ta>
            <ta e="T16" id="Seg_10436" s="T15">adv</ta>
            <ta e="T17" id="Seg_10437" s="T16">adj</ta>
            <ta e="T18" id="Seg_10438" s="T17">aux</ta>
            <ta e="T19" id="Seg_10439" s="T18">v</ta>
            <ta e="T20" id="Seg_10440" s="T19">adv</ta>
            <ta e="T21" id="Seg_10441" s="T20">n</ta>
            <ta e="T22" id="Seg_10442" s="T21">v</ta>
            <ta e="T23" id="Seg_10443" s="T22">v</ta>
            <ta e="T24" id="Seg_10444" s="T23">n</ta>
            <ta e="T25" id="Seg_10445" s="T24">adv</ta>
            <ta e="T26" id="Seg_10446" s="T25">pers</ta>
            <ta e="T28" id="Seg_10447" s="T27">adv</ta>
            <ta e="T29" id="Seg_10448" s="T28">dempro</ta>
            <ta e="T30" id="Seg_10449" s="T29">v</ta>
            <ta e="T31" id="Seg_10450" s="T30">conj</ta>
            <ta e="T32" id="Seg_10451" s="T31">pers</ta>
            <ta e="T33" id="Seg_10452" s="T32">dempro</ta>
            <ta e="T34" id="Seg_10453" s="T33">v</ta>
            <ta e="T35" id="Seg_10454" s="T34">pers</ta>
            <ta e="T36" id="Seg_10455" s="T35">v</ta>
            <ta e="T37" id="Seg_10456" s="T36">v</ta>
            <ta e="T38" id="Seg_10457" s="T37">conj</ta>
            <ta e="T39" id="Seg_10458" s="T38">n</ta>
            <ta e="T40" id="Seg_10459" s="T39">pers</ta>
            <ta e="T41" id="Seg_10460" s="T40">dempro</ta>
            <ta e="T42" id="Seg_10461" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_10462" s="T42">v</ta>
            <ta e="T44" id="Seg_10463" s="T43">adv</ta>
            <ta e="T45" id="Seg_10464" s="T44">adv</ta>
            <ta e="T46" id="Seg_10465" s="T45">v</ta>
            <ta e="T47" id="Seg_10466" s="T46">ptcl</ta>
            <ta e="T49" id="Seg_10467" s="T48">n</ta>
            <ta e="T50" id="Seg_10468" s="T49">v</ta>
            <ta e="T51" id="Seg_10469" s="T50">adv</ta>
            <ta e="T53" id="Seg_10470" s="T52">quant</ta>
            <ta e="T54" id="Seg_10471" s="T53">n</ta>
            <ta e="T964" id="Seg_10472" s="T54">v</ta>
            <ta e="T55" id="Seg_10473" s="T964">v</ta>
            <ta e="T56" id="Seg_10474" s="T55">n</ta>
            <ta e="T57" id="Seg_10475" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_10476" s="T57">que</ta>
            <ta e="T59" id="Seg_10477" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_10478" s="T59">v</ta>
            <ta e="T61" id="Seg_10479" s="T60">dempro</ta>
            <ta e="T62" id="Seg_10480" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_10481" s="T63">quant</ta>
            <ta e="T65" id="Seg_10482" s="T64">n</ta>
            <ta e="T66" id="Seg_10483" s="T65">conj</ta>
            <ta e="T67" id="Seg_10484" s="T66">n</ta>
            <ta e="T68" id="Seg_10485" s="T67">quant</ta>
            <ta e="T69" id="Seg_10486" s="T68">conj</ta>
            <ta e="T70" id="Seg_10487" s="T69">n</ta>
            <ta e="T71" id="Seg_10488" s="T70">quant</ta>
            <ta e="T72" id="Seg_10489" s="T71">n</ta>
            <ta e="T73" id="Seg_10490" s="T72">quant</ta>
            <ta e="T74" id="Seg_10491" s="T73">n</ta>
            <ta e="T75" id="Seg_10492" s="T74">v</ta>
            <ta e="T76" id="Seg_10493" s="T75">quant</ta>
            <ta e="T77" id="Seg_10494" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_10495" s="T77">n</ta>
            <ta e="T79" id="Seg_10496" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_10497" s="T79">quant</ta>
            <ta e="T82" id="Seg_10498" s="T81">n</ta>
            <ta e="T83" id="Seg_10499" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_10500" s="T83">v</ta>
            <ta e="T85" id="Seg_10501" s="T84">n</ta>
            <ta e="T86" id="Seg_10502" s="T85">v</ta>
            <ta e="T88" id="Seg_10503" s="T87">n</ta>
            <ta e="T89" id="Seg_10504" s="T88">adv</ta>
            <ta e="T91" id="Seg_10505" s="T90">n</ta>
            <ta e="T92" id="Seg_10506" s="T91">v</ta>
            <ta e="T93" id="Seg_10507" s="T92">adv</ta>
            <ta e="T94" id="Seg_10508" s="T93">adj</ta>
            <ta e="T95" id="Seg_10509" s="T94">v</ta>
            <ta e="T97" id="Seg_10510" s="T96">n</ta>
            <ta e="T98" id="Seg_10511" s="T97">n</ta>
            <ta e="T99" id="Seg_10512" s="T98">v</ta>
            <ta e="T100" id="Seg_10513" s="T99">conj</ta>
            <ta e="T101" id="Seg_10514" s="T100">v</ta>
            <ta e="T102" id="Seg_10515" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_10516" s="T103">dempro</ta>
            <ta e="T105" id="Seg_10517" s="T104">adv</ta>
            <ta e="T106" id="Seg_10518" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_10519" s="T106">n</ta>
            <ta e="T108" id="Seg_10520" s="T107">v</ta>
            <ta e="T109" id="Seg_10521" s="T108">v</ta>
            <ta e="T111" id="Seg_10522" s="T110">n</ta>
            <ta e="T112" id="Seg_10523" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_10524" s="T112">v</ta>
            <ta e="T114" id="Seg_10525" s="T113">n</ta>
            <ta e="T115" id="Seg_10526" s="T114">conj</ta>
            <ta e="T116" id="Seg_10527" s="T115">adv</ta>
            <ta e="T117" id="Seg_10528" s="T116">adj</ta>
            <ta e="T118" id="Seg_10529" s="T117">v</ta>
            <ta e="T119" id="Seg_10530" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_10531" s="T119">v</ta>
            <ta e="T121" id="Seg_10532" s="T120">adv</ta>
            <ta e="T122" id="Seg_10533" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_10534" s="T122">adv</ta>
            <ta e="T124" id="Seg_10535" s="T123">adj</ta>
            <ta e="T125" id="Seg_10536" s="T124">n</ta>
            <ta e="T126" id="Seg_10537" s="T125">v</ta>
            <ta e="T127" id="Seg_10538" s="T126">adv</ta>
            <ta e="T128" id="Seg_10539" s="T127">v</ta>
            <ta e="T129" id="Seg_10540" s="T128">adv</ta>
            <ta e="T130" id="Seg_10541" s="T129">n</ta>
            <ta e="T131" id="Seg_10542" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_10543" s="T131">v</ta>
            <ta e="T133" id="Seg_10544" s="T132">adv</ta>
            <ta e="T134" id="Seg_10545" s="T133">adv</ta>
            <ta e="T138" id="Seg_10546" s="T137">v</ta>
            <ta e="T139" id="Seg_10547" s="T138">ptcl</ta>
            <ta e="T141" id="Seg_10548" s="T140">conj</ta>
            <ta e="T142" id="Seg_10549" s="T141">adv</ta>
            <ta e="T143" id="Seg_10550" s="T142">v</ta>
            <ta e="T144" id="Seg_10551" s="T143">dempro</ta>
            <ta e="T145" id="Seg_10552" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_10553" s="T145">v</ta>
            <ta e="T150" id="Seg_10554" s="T149">n</ta>
            <ta e="T151" id="Seg_10555" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10556" s="T151">adj</ta>
            <ta e="T153" id="Seg_10557" s="T152">v</ta>
            <ta e="T154" id="Seg_10558" s="T153">num</ta>
            <ta e="T155" id="Seg_10559" s="T154">v</ta>
            <ta e="T156" id="Seg_10560" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_10561" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_10562" s="T157">v</ta>
            <ta e="T159" id="Seg_10563" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_10564" s="T159">n</ta>
            <ta e="T161" id="Seg_10565" s="T160">n</ta>
            <ta e="T162" id="Seg_10566" s="T161">v</ta>
            <ta e="T163" id="Seg_10567" s="T162">num</ta>
            <ta e="T164" id="Seg_10568" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_10569" s="T164">v</ta>
            <ta e="T166" id="Seg_10570" s="T165">conj</ta>
            <ta e="T167" id="Seg_10571" s="T166">quant</ta>
            <ta e="T168" id="Seg_10572" s="T167">v</ta>
            <ta e="T170" id="Seg_10573" s="T169">dempro</ta>
            <ta e="T171" id="Seg_10574" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_10575" s="T171">n</ta>
            <ta e="T173" id="Seg_10576" s="T172">v</ta>
            <ta e="T174" id="Seg_10577" s="T173">num</ta>
            <ta e="T175" id="Seg_10578" s="T174">num</ta>
            <ta e="T176" id="Seg_10579" s="T175">num</ta>
            <ta e="T177" id="Seg_10580" s="T176">num</ta>
            <ta e="T178" id="Seg_10581" s="T177">num</ta>
            <ta e="T179" id="Seg_10582" s="T178">num</ta>
            <ta e="T180" id="Seg_10583" s="T179">num</ta>
            <ta e="T181" id="Seg_10584" s="T180">num</ta>
            <ta e="T186" id="Seg_10585" s="T185">v</ta>
            <ta e="T187" id="Seg_10586" s="T186">n</ta>
            <ta e="T188" id="Seg_10587" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_10588" s="T188">v</ta>
            <ta e="T190" id="Seg_10589" s="T189">n</ta>
            <ta e="T191" id="Seg_10590" s="T190">v</ta>
            <ta e="T192" id="Seg_10591" s="T191">v</ta>
            <ta e="T193" id="Seg_10592" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_10593" s="T193">adv</ta>
            <ta e="T195" id="Seg_10594" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_10595" s="T195">v</ta>
            <ta e="T197" id="Seg_10596" s="T196">pers</ta>
            <ta e="T198" id="Seg_10597" s="T197">adj</ta>
            <ta e="T199" id="Seg_10598" s="T198">v</ta>
            <ta e="T200" id="Seg_10599" s="T199">pers</ta>
            <ta e="T201" id="Seg_10600" s="T200">n</ta>
            <ta e="T202" id="Seg_10601" s="T201">v</ta>
            <ta e="T203" id="Seg_10602" s="T202">n</ta>
            <ta e="T205" id="Seg_10603" s="T204">v</ta>
            <ta e="T206" id="Seg_10604" s="T205">n</ta>
            <ta e="T207" id="Seg_10605" s="T206">v</ta>
            <ta e="T208" id="Seg_10606" s="T207">n</ta>
            <ta e="T209" id="Seg_10607" s="T208">v</ta>
            <ta e="T213" id="Seg_10608" s="T212">n</ta>
            <ta e="T215" id="Seg_10609" s="T214">v</ta>
            <ta e="T217" id="Seg_10610" s="T215">conj</ta>
            <ta e="T218" id="Seg_10611" s="T217">n</ta>
            <ta e="T219" id="Seg_10612" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_10613" s="T219">v</ta>
            <ta e="T221" id="Seg_10614" s="T220">adv</ta>
            <ta e="T222" id="Seg_10615" s="T221">pers</ta>
            <ta e="T223" id="Seg_10616" s="T222">n</ta>
            <ta e="T224" id="Seg_10617" s="T223">v</ta>
            <ta e="T225" id="Seg_10618" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_10619" s="T225">v</ta>
            <ta e="T227" id="Seg_10620" s="T226">dempro</ta>
            <ta e="T228" id="Seg_10621" s="T227">v</ta>
            <ta e="T230" id="Seg_10622" s="T229">adv</ta>
            <ta e="T232" id="Seg_10623" s="T231">v</ta>
            <ta e="T233" id="Seg_10624" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_10625" s="T233">pers</ta>
            <ta e="T235" id="Seg_10626" s="T234">v</ta>
            <ta e="T237" id="Seg_10627" s="T236">adv</ta>
            <ta e="T239" id="Seg_10628" s="T238">n</ta>
            <ta e="T240" id="Seg_10629" s="T239">v</ta>
            <ta e="T241" id="Seg_10630" s="T240">adv</ta>
            <ta e="T242" id="Seg_10631" s="T241">adj</ta>
            <ta e="T243" id="Seg_10632" s="T242">v</ta>
            <ta e="T244" id="Seg_10633" s="T243">conj</ta>
            <ta e="T245" id="Seg_10634" s="T244">pers</ta>
            <ta e="T246" id="Seg_10635" s="T245">adv</ta>
            <ta e="T247" id="Seg_10636" s="T246">v</ta>
            <ta e="T248" id="Seg_10637" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_10638" s="T248">adv</ta>
            <ta e="T250" id="Seg_10639" s="T249">v</ta>
            <ta e="T252" id="Seg_10640" s="T251">adv</ta>
            <ta e="T253" id="Seg_10641" s="T252">adj</ta>
            <ta e="T254" id="Seg_10642" s="T253">n</ta>
            <ta e="T255" id="Seg_10643" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_10644" s="T255">v</ta>
            <ta e="T257" id="Seg_10645" s="T256">pers</ta>
            <ta e="T258" id="Seg_10646" s="T257">n</ta>
            <ta e="T260" id="Seg_10647" s="T259">v</ta>
            <ta e="T261" id="Seg_10648" s="T260">dempro</ta>
            <ta e="T262" id="Seg_10649" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_10650" s="T262">v</ta>
            <ta e="T265" id="Seg_10651" s="T264">conj</ta>
            <ta e="T266" id="Seg_10652" s="T265">v</ta>
            <ta e="T267" id="Seg_10653" s="T266">v</ta>
            <ta e="T268" id="Seg_10654" s="T267">v</ta>
            <ta e="T269" id="Seg_10655" s="T268">v</ta>
            <ta e="T270" id="Seg_10656" s="T269">pers</ta>
            <ta e="T271" id="Seg_10657" s="T270">v</ta>
            <ta e="T272" id="Seg_10658" s="T271">adv</ta>
            <ta e="T274" id="Seg_10659" s="T272">adv</ta>
            <ta e="T275" id="Seg_10660" s="T274">adv</ta>
            <ta e="T276" id="Seg_10661" s="T275">v</ta>
            <ta e="T277" id="Seg_10662" s="T276">propr</ta>
            <ta e="T278" id="Seg_10663" s="T277">n</ta>
            <ta e="T279" id="Seg_10664" s="T278">adv</ta>
            <ta e="T280" id="Seg_10665" s="T279">dempro</ta>
            <ta e="T281" id="Seg_10666" s="T280">n</ta>
            <ta e="T282" id="Seg_10667" s="T281">v</ta>
            <ta e="T283" id="Seg_10668" s="T282">ptcl</ta>
            <ta e="T285" id="Seg_10669" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_10670" s="T285">v</ta>
            <ta e="T288" id="Seg_10671" s="T287">dempro</ta>
            <ta e="T289" id="Seg_10672" s="T288">adv</ta>
            <ta e="T290" id="Seg_10673" s="T289">n</ta>
            <ta e="T291" id="Seg_10674" s="T290">v</ta>
            <ta e="T292" id="Seg_10675" s="T291">conj</ta>
            <ta e="T293" id="Seg_10676" s="T292">adv</ta>
            <ta e="T294" id="Seg_10677" s="T293">v</ta>
            <ta e="T295" id="Seg_10678" s="T294">v</ta>
            <ta e="T296" id="Seg_10679" s="T295">que</ta>
            <ta e="T297" id="Seg_10680" s="T296">adj</ta>
            <ta e="T298" id="Seg_10681" s="T297">v</ta>
            <ta e="T300" id="Seg_10682" s="T299">dempro</ta>
            <ta e="T301" id="Seg_10683" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_10684" s="T301">v</ta>
            <ta e="T303" id="Seg_10685" s="T302">propr</ta>
            <ta e="T304" id="Seg_10686" s="T303">v</ta>
            <ta e="T305" id="Seg_10687" s="T304">adv</ta>
            <ta e="T306" id="Seg_10688" s="T305">adv</ta>
            <ta e="T307" id="Seg_10689" s="T306">v</ta>
            <ta e="T308" id="Seg_10690" s="T307">n</ta>
            <ta e="T309" id="Seg_10691" s="T308">v</ta>
            <ta e="T310" id="Seg_10692" s="T309">dempro</ta>
            <ta e="T311" id="Seg_10693" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_10694" s="T311">v</ta>
            <ta e="T313" id="Seg_10695" s="T312">adv</ta>
            <ta e="T314" id="Seg_10696" s="T313">n</ta>
            <ta e="T315" id="Seg_10697" s="T314">v</ta>
            <ta e="T316" id="Seg_10698" s="T315">adv</ta>
            <ta e="T318" id="Seg_10699" s="T317">v</ta>
            <ta e="T319" id="Seg_10700" s="T318">v</ta>
            <ta e="T320" id="Seg_10701" s="T319">que</ta>
            <ta e="T321" id="Seg_10702" s="T320">adj</ta>
            <ta e="T322" id="Seg_10703" s="T321">v</ta>
            <ta e="T323" id="Seg_10704" s="T322">dempro</ta>
            <ta e="T324" id="Seg_10705" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_10706" s="T324">v</ta>
            <ta e="T326" id="Seg_10707" s="T325">adv</ta>
            <ta e="T327" id="Seg_10708" s="T326">v</ta>
            <ta e="T328" id="Seg_10709" s="T327">n</ta>
            <ta e="T329" id="Seg_10710" s="T328">adv</ta>
            <ta e="T330" id="Seg_10711" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_10712" s="T330">n</ta>
            <ta e="T332" id="Seg_10713" s="T331">v</ta>
            <ta e="T333" id="Seg_10714" s="T332">propr</ta>
            <ta e="T334" id="Seg_10715" s="T333">v</ta>
            <ta e="T335" id="Seg_10716" s="T334">adv</ta>
            <ta e="T336" id="Seg_10717" s="T335">n</ta>
            <ta e="T337" id="Seg_10718" s="T336">quant</ta>
            <ta e="T339" id="Seg_10719" s="T338">num</ta>
            <ta e="T340" id="Seg_10720" s="T339">n</ta>
            <ta e="T341" id="Seg_10721" s="T340">adv</ta>
            <ta e="T342" id="Seg_10722" s="T341">conj</ta>
            <ta e="T343" id="Seg_10723" s="T342">num</ta>
            <ta e="T344" id="Seg_10724" s="T343">n</ta>
            <ta e="T345" id="Seg_10725" s="T344">adv</ta>
            <ta e="T347" id="Seg_10726" s="T346">v</ta>
            <ta e="T348" id="Seg_10727" s="T347">adv</ta>
            <ta e="T349" id="Seg_10728" s="T348">adv</ta>
            <ta e="T350" id="Seg_10729" s="T349">adv</ta>
            <ta e="T351" id="Seg_10730" s="T350">aux</ta>
            <ta e="T352" id="Seg_10731" s="T351">v</ta>
            <ta e="T353" id="Seg_10732" s="T352">conj</ta>
            <ta e="T354" id="Seg_10733" s="T353">adv</ta>
            <ta e="T355" id="Seg_10734" s="T354">aux</ta>
            <ta e="T356" id="Seg_10735" s="T355">v</ta>
            <ta e="T358" id="Seg_10736" s="T357">que</ta>
            <ta e="T359" id="Seg_10737" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_10738" s="T359">v</ta>
            <ta e="T361" id="Seg_10739" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_10740" s="T361">v</ta>
            <ta e="T363" id="Seg_10741" s="T362">n</ta>
            <ta e="T364" id="Seg_10742" s="T363">dempro</ta>
            <ta e="T365" id="Seg_10743" s="T364">n</ta>
            <ta e="T366" id="Seg_10744" s="T365">adv</ta>
            <ta e="T367" id="Seg_10745" s="T366">adj</ta>
            <ta e="T369" id="Seg_10746" s="T368">v</ta>
            <ta e="T370" id="Seg_10747" s="T369">adv</ta>
            <ta e="T371" id="Seg_10748" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_10749" s="T371">que</ta>
            <ta e="T373" id="Seg_10750" s="T372">v</ta>
            <ta e="T375" id="Seg_10751" s="T374">n</ta>
            <ta e="T376" id="Seg_10752" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_10753" s="T376">v</ta>
            <ta e="T378" id="Seg_10754" s="T377">pers</ta>
            <ta e="T379" id="Seg_10755" s="T378">n</ta>
            <ta e="T380" id="Seg_10756" s="T379">v</ta>
            <ta e="T382" id="Seg_10757" s="T381">dempro</ta>
            <ta e="T383" id="Seg_10758" s="T382">pers</ta>
            <ta e="T385" id="Seg_10759" s="T384">n</ta>
            <ta e="T386" id="Seg_10760" s="T385">v</ta>
            <ta e="T388" id="Seg_10761" s="T387">v</ta>
            <ta e="T389" id="Seg_10762" s="T388">conj</ta>
            <ta e="T390" id="Seg_10763" s="T389">pers</ta>
            <ta e="T391" id="Seg_10764" s="T390">v</ta>
            <ta e="T392" id="Seg_10765" s="T391">pers</ta>
            <ta e="T393" id="Seg_10766" s="T392">pers</ta>
            <ta e="T394" id="Seg_10767" s="T393">que</ta>
            <ta e="T395" id="Seg_10768" s="T394">dempro</ta>
            <ta e="T396" id="Seg_10769" s="T395">v</ta>
            <ta e="T401" id="Seg_10770" s="T400">pers</ta>
            <ta e="T402" id="Seg_10771" s="T401">adv</ta>
            <ta e="T403" id="Seg_10772" s="T402">adj</ta>
            <ta e="T405" id="Seg_10773" s="T404">n</ta>
            <ta e="T406" id="Seg_10774" s="T405">conj</ta>
            <ta e="T407" id="Seg_10775" s="T406">pers</ta>
            <ta e="T408" id="Seg_10776" s="T407">adj</ta>
            <ta e="T409" id="Seg_10777" s="T408">n</ta>
            <ta e="T411" id="Seg_10778" s="T410">pers</ta>
            <ta e="T412" id="Seg_10779" s="T411">ptcl</ta>
            <ta e="T414" id="Seg_10780" s="T413">v</ta>
            <ta e="T416" id="Seg_10781" s="T415">n</ta>
            <ta e="T417" id="Seg_10782" s="T416">n</ta>
            <ta e="T418" id="Seg_10783" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_10784" s="T418">v</ta>
            <ta e="T420" id="Seg_10785" s="T419">pers</ta>
            <ta e="T421" id="Seg_10786" s="T420">v</ta>
            <ta e="T422" id="Seg_10787" s="T421">n</ta>
            <ta e="T423" id="Seg_10788" s="T422">que</ta>
            <ta e="T424" id="Seg_10789" s="T423">dempro</ta>
            <ta e="T425" id="Seg_10790" s="T424">v</ta>
            <ta e="T426" id="Seg_10791" s="T425">ptcl</ta>
            <ta e="T428" id="Seg_10792" s="T426">pers</ta>
            <ta e="T430" id="Seg_10793" s="T429">pers</ta>
            <ta e="T431" id="Seg_10794" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_10795" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_10796" s="T432">adv</ta>
            <ta e="T434" id="Seg_10797" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_10798" s="T434">v</ta>
            <ta e="T437" id="Seg_10799" s="T436">pers</ta>
            <ta e="T438" id="Seg_10800" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10801" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_10802" s="T439">v</ta>
            <ta e="T441" id="Seg_10803" s="T440">conj</ta>
            <ta e="T442" id="Seg_10804" s="T441">pers</ta>
            <ta e="T443" id="Seg_10805" s="T442">v</ta>
            <ta e="T444" id="Seg_10806" s="T443">pers</ta>
            <ta e="T445" id="Seg_10807" s="T444">conj</ta>
            <ta e="T446" id="Seg_10808" s="T445">pers</ta>
            <ta e="T447" id="Seg_10809" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_10810" s="T447">v</ta>
            <ta e="T449" id="Seg_10811" s="T448">pers</ta>
            <ta e="T450" id="Seg_10812" s="T449">v</ta>
            <ta e="T451" id="Seg_10813" s="T450">conj</ta>
            <ta e="T452" id="Seg_10814" s="T451">v</ta>
            <ta e="T453" id="Seg_10815" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_10816" s="T453">v</ta>
            <ta e="T455" id="Seg_10817" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_10818" s="T455">pers</ta>
            <ta e="T457" id="Seg_10819" s="T456">n</ta>
            <ta e="T458" id="Seg_10820" s="T457">v</ta>
            <ta e="T459" id="Seg_10821" s="T458">conj</ta>
            <ta e="T462" id="Seg_10822" s="T461">dempro</ta>
            <ta e="T463" id="Seg_10823" s="T462">v</ta>
            <ta e="T464" id="Seg_10824" s="T463">adv</ta>
            <ta e="T465" id="Seg_10825" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_10826" s="T465">n</ta>
            <ta e="T467" id="Seg_10827" s="T466">v</ta>
            <ta e="T470" id="Seg_10828" s="T469">adv</ta>
            <ta e="T471" id="Seg_10829" s="T470">adj</ta>
            <ta e="T472" id="Seg_10830" s="T471">n</ta>
            <ta e="T473" id="Seg_10831" s="T472">ptcl</ta>
            <ta e="T474" id="Seg_10832" s="T473">adv</ta>
            <ta e="T475" id="Seg_10833" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_10834" s="T475">v</ta>
            <ta e="T477" id="Seg_10835" s="T476">n</ta>
            <ta e="T478" id="Seg_10836" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_10837" s="T478">v</ta>
            <ta e="T480" id="Seg_10838" s="T479">conj</ta>
            <ta e="T481" id="Seg_10839" s="T480">n</ta>
            <ta e="T482" id="Seg_10840" s="T481">v</ta>
            <ta e="T484" id="Seg_10841" s="T482">ptcl</ta>
            <ta e="T486" id="Seg_10842" s="T484">n</ta>
            <ta e="T487" id="Seg_10843" s="T486">conj</ta>
            <ta e="T488" id="Seg_10844" s="T487">n</ta>
            <ta e="T489" id="Seg_10845" s="T488">ptcl</ta>
            <ta e="T491" id="Seg_10846" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_10847" s="T491">v</ta>
            <ta e="T493" id="Seg_10848" s="T492">pers</ta>
            <ta e="T494" id="Seg_10849" s="T493">n</ta>
            <ta e="T495" id="Seg_10850" s="T494">n</ta>
            <ta e="T496" id="Seg_10851" s="T495">aux</ta>
            <ta e="T497" id="Seg_10852" s="T496">v</ta>
            <ta e="T498" id="Seg_10853" s="T497">pers</ta>
            <ta e="T499" id="Seg_10854" s="T498">aux</ta>
            <ta e="T500" id="Seg_10855" s="T499">v</ta>
            <ta e="T501" id="Seg_10856" s="T500">pers</ta>
            <ta e="T502" id="Seg_10857" s="T501">aux</ta>
            <ta e="T503" id="Seg_10858" s="T502">v</ta>
            <ta e="T504" id="Seg_10859" s="T503">pers</ta>
            <ta e="T505" id="Seg_10860" s="T504">aux</ta>
            <ta e="T506" id="Seg_10861" s="T505">v</ta>
            <ta e="T507" id="Seg_10862" s="T506">pers</ta>
            <ta e="T508" id="Seg_10863" s="T507">pers</ta>
            <ta e="T509" id="Seg_10864" s="T508">v</ta>
            <ta e="T510" id="Seg_10865" s="T509">pers</ta>
            <ta e="T511" id="Seg_10866" s="T510">n</ta>
            <ta e="T512" id="Seg_10867" s="T511">adj</ta>
            <ta e="T513" id="Seg_10868" s="T512">pers</ta>
            <ta e="T514" id="Seg_10869" s="T513">v</ta>
            <ta e="T515" id="Seg_10870" s="T514">pers</ta>
            <ta e="T516" id="Seg_10871" s="T515">n</ta>
            <ta e="T517" id="Seg_10872" s="T516">n</ta>
            <ta e="T518" id="Seg_10873" s="T517">quant</ta>
            <ta e="T519" id="Seg_10874" s="T518">v</ta>
            <ta e="T520" id="Seg_10875" s="T519">pers</ta>
            <ta e="T521" id="Seg_10876" s="T520">v</ta>
            <ta e="T522" id="Seg_10877" s="T521">pers</ta>
            <ta e="T523" id="Seg_10878" s="T522">v</ta>
            <ta e="T524" id="Seg_10879" s="T523">pers</ta>
            <ta e="T525" id="Seg_10880" s="T524">pers</ta>
            <ta e="T527" id="Seg_10881" s="T526">n</ta>
            <ta e="T528" id="Seg_10882" s="T527">v</ta>
            <ta e="T530" id="Seg_10883" s="T529">pers</ta>
            <ta e="T532" id="Seg_10884" s="T531">n</ta>
            <ta e="T533" id="Seg_10885" s="T532">v</ta>
            <ta e="T538" id="Seg_10886" s="T537">propr</ta>
            <ta e="T539" id="Seg_10887" s="T538">v</ta>
            <ta e="T540" id="Seg_10888" s="T539">n</ta>
            <ta e="T541" id="Seg_10889" s="T540">n</ta>
            <ta e="T542" id="Seg_10890" s="T541">n</ta>
            <ta e="T543" id="Seg_10891" s="T542">v</ta>
            <ta e="T544" id="Seg_10892" s="T543">dempro</ta>
            <ta e="T545" id="Seg_10893" s="T544">n</ta>
            <ta e="T546" id="Seg_10894" s="T545">n</ta>
            <ta e="T547" id="Seg_10895" s="T546">n</ta>
            <ta e="T548" id="Seg_10896" s="T547">n</ta>
            <ta e="T550" id="Seg_10897" s="T549">v</ta>
            <ta e="T551" id="Seg_10898" s="T550">pers</ta>
            <ta e="T552" id="Seg_10899" s="T551">adv</ta>
            <ta e="T554" id="Seg_10900" s="T553">adj</ta>
            <ta e="T555" id="Seg_10901" s="T554">v</ta>
            <ta e="T557" id="Seg_10902" s="T556">pers</ta>
            <ta e="T558" id="Seg_10903" s="T557">adv</ta>
            <ta e="T559" id="Seg_10904" s="T558">n</ta>
            <ta e="T564" id="Seg_10905" s="T563">v</ta>
            <ta e="T565" id="Seg_10906" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_10907" s="T565">v</ta>
            <ta e="T568" id="Seg_10908" s="T567">propr</ta>
            <ta e="T569" id="Seg_10909" s="T568">n</ta>
            <ta e="T570" id="Seg_10910" s="T569">v</ta>
            <ta e="T571" id="Seg_10911" s="T570">v</ta>
            <ta e="T572" id="Seg_10912" s="T571">n</ta>
            <ta e="T574" id="Seg_10913" s="T573">que</ta>
            <ta e="T575" id="Seg_10914" s="T574">n</ta>
            <ta e="T576" id="Seg_10915" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_10916" s="T576">adj</ta>
            <ta e="T578" id="Seg_10917" s="T577">conj</ta>
            <ta e="T579" id="Seg_10918" s="T578">pers</ta>
            <ta e="T580" id="Seg_10919" s="T579">v</ta>
            <ta e="T581" id="Seg_10920" s="T580">adv</ta>
            <ta e="T582" id="Seg_10921" s="T581">v</ta>
            <ta e="T583" id="Seg_10922" s="T582">dempro</ta>
            <ta e="T584" id="Seg_10923" s="T583">v</ta>
            <ta e="T585" id="Seg_10924" s="T584">num</ta>
            <ta e="T586" id="Seg_10925" s="T585">num</ta>
            <ta e="T587" id="Seg_10926" s="T586">num</ta>
            <ta e="T588" id="Seg_10927" s="T587">num</ta>
            <ta e="T589" id="Seg_10928" s="T588">num</ta>
            <ta e="T590" id="Seg_10929" s="T589">num</ta>
            <ta e="T592" id="Seg_10930" s="T591">num</ta>
            <ta e="T593" id="Seg_10931" s="T592">v</ta>
            <ta e="T595" id="Seg_10932" s="T594">n</ta>
            <ta e="T596" id="Seg_10933" s="T595">pers</ta>
            <ta e="T597" id="Seg_10934" s="T596">n</ta>
            <ta e="T598" id="Seg_10935" s="T597">adv</ta>
            <ta e="T600" id="Seg_10936" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_10937" s="T600">v</ta>
            <ta e="T602" id="Seg_10938" s="T601">num</ta>
            <ta e="T603" id="Seg_10939" s="T602">n</ta>
            <ta e="T604" id="Seg_10940" s="T603">v</ta>
            <ta e="T605" id="Seg_10941" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_10942" s="T605">propr</ta>
            <ta e="T607" id="Seg_10943" s="T606">v</ta>
            <ta e="T608" id="Seg_10944" s="T607">n</ta>
            <ta e="T609" id="Seg_10945" s="T608">v</ta>
            <ta e="T610" id="Seg_10946" s="T609">conj</ta>
            <ta e="T611" id="Seg_10947" s="T610">n</ta>
            <ta e="T612" id="Seg_10948" s="T611">v</ta>
            <ta e="T613" id="Seg_10949" s="T612">n</ta>
            <ta e="T614" id="Seg_10950" s="T613">num</ta>
            <ta e="T615" id="Seg_10951" s="T614">num</ta>
            <ta e="T616" id="Seg_10952" s="T615">num</ta>
            <ta e="T617" id="Seg_10953" s="T616">v</ta>
            <ta e="T618" id="Seg_10954" s="T617">conj</ta>
            <ta e="T619" id="Seg_10955" s="T618">pers</ta>
            <ta e="T621" id="Seg_10956" s="T620">n</ta>
            <ta e="T622" id="Seg_10957" s="T621">v</ta>
            <ta e="T624" id="Seg_10958" s="T623">pers</ta>
            <ta e="T625" id="Seg_10959" s="T624">adv</ta>
            <ta e="T626" id="Seg_10960" s="T625">n</ta>
            <ta e="T627" id="Seg_10961" s="T626">adv</ta>
            <ta e="T628" id="Seg_10962" s="T627">v</ta>
            <ta e="T629" id="Seg_10963" s="T628">quant</ta>
            <ta e="T630" id="Seg_10964" s="T629">n</ta>
            <ta e="T631" id="Seg_10965" s="T630">v</ta>
            <ta e="T632" id="Seg_10966" s="T631">num</ta>
            <ta e="T633" id="Seg_10967" s="T632">num</ta>
            <ta e="T634" id="Seg_10968" s="T633">v</ta>
            <ta e="T635" id="Seg_10969" s="T634">adv</ta>
            <ta e="T636" id="Seg_10970" s="T635">num</ta>
            <ta e="T637" id="Seg_10971" s="T636">adv</ta>
            <ta e="T638" id="Seg_10972" s="T637">num</ta>
            <ta e="T639" id="Seg_10973" s="T638">num</ta>
            <ta e="T640" id="Seg_10974" s="T639">n</ta>
            <ta e="T642" id="Seg_10975" s="T641">num</ta>
            <ta e="T643" id="Seg_10976" s="T642">num</ta>
            <ta e="T644" id="Seg_10977" s="T643">v</ta>
            <ta e="T645" id="Seg_10978" s="T644">conj</ta>
            <ta e="T646" id="Seg_10979" s="T645">num</ta>
            <ta e="T647" id="Seg_10980" s="T646">adv</ta>
            <ta e="T649" id="Seg_10981" s="T648">n</ta>
            <ta e="T650" id="Seg_10982" s="T649">pers</ta>
            <ta e="T651" id="Seg_10983" s="T650">n</ta>
            <ta e="T652" id="Seg_10984" s="T651">v</ta>
            <ta e="T653" id="Seg_10985" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10986" s="T653">pers</ta>
            <ta e="T655" id="Seg_10987" s="T654">v</ta>
            <ta e="T656" id="Seg_10988" s="T655">v</ta>
            <ta e="T657" id="Seg_10989" s="T656">pers</ta>
            <ta e="T658" id="Seg_10990" s="T657">que</ta>
            <ta e="T659" id="Seg_10991" s="T658">v</ta>
            <ta e="T660" id="Seg_10992" s="T659">v</ta>
            <ta e="T661" id="Seg_10993" s="T660">adv</ta>
            <ta e="T662" id="Seg_10994" s="T661">dempro</ta>
            <ta e="T663" id="Seg_10995" s="T662">v</ta>
            <ta e="T664" id="Seg_10996" s="T663">v</ta>
            <ta e="T666" id="Seg_10997" s="T665">pers</ta>
            <ta e="T667" id="Seg_10998" s="T666">ptcl</ta>
            <ta e="T668" id="Seg_10999" s="T667">adj</ta>
            <ta e="T669" id="Seg_11000" s="T668">n</ta>
            <ta e="T670" id="Seg_11001" s="T669">v</ta>
            <ta e="T671" id="Seg_11002" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_11003" s="T671">v</ta>
            <ta e="T674" id="Seg_11004" s="T673">v</ta>
            <ta e="T675" id="Seg_11005" s="T674">n</ta>
            <ta e="T677" id="Seg_11006" s="T676">adv</ta>
            <ta e="T678" id="Seg_11007" s="T677">n</ta>
            <ta e="T679" id="Seg_11008" s="T678">pers</ta>
            <ta e="T680" id="Seg_11009" s="T679">v</ta>
            <ta e="T681" id="Seg_11010" s="T680">n</ta>
            <ta e="T682" id="Seg_11011" s="T681">v</ta>
            <ta e="T683" id="Seg_11012" s="T682">quant</ta>
            <ta e="T684" id="Seg_11013" s="T683">v</ta>
            <ta e="T685" id="Seg_11014" s="T684">adv</ta>
            <ta e="T686" id="Seg_11015" s="T685">conj</ta>
            <ta e="T688" id="Seg_11016" s="T687">n</ta>
            <ta e="T689" id="Seg_11017" s="T688">n</ta>
            <ta e="T690" id="Seg_11018" s="T689">v</ta>
            <ta e="T691" id="Seg_11019" s="T690">adv</ta>
            <ta e="T692" id="Seg_11020" s="T691">n</ta>
            <ta e="T693" id="Seg_11021" s="T692">v</ta>
            <ta e="T695" id="Seg_11022" s="T694">dempro</ta>
            <ta e="T696" id="Seg_11023" s="T695">v</ta>
            <ta e="T697" id="Seg_11024" s="T696">v</ta>
            <ta e="T698" id="Seg_11025" s="T697">n</ta>
            <ta e="T699" id="Seg_11026" s="T698">pers</ta>
            <ta e="T700" id="Seg_11027" s="T699">n</ta>
            <ta e="T701" id="Seg_11028" s="T700">v</ta>
            <ta e="T702" id="Seg_11029" s="T701">conj</ta>
            <ta e="T703" id="Seg_11030" s="T702">pers</ta>
            <ta e="T704" id="Seg_11031" s="T703">v</ta>
            <ta e="T705" id="Seg_11032" s="T704">dempro</ta>
            <ta e="T706" id="Seg_11033" s="T705">v</ta>
            <ta e="T707" id="Seg_11034" s="T706">n</ta>
            <ta e="T708" id="Seg_11035" s="T707">que</ta>
            <ta e="T709" id="Seg_11036" s="T708">adj</ta>
            <ta e="T710" id="Seg_11037" s="T709">n</ta>
            <ta e="T712" id="Seg_11038" s="T711">v</ta>
            <ta e="T713" id="Seg_11039" s="T712">n</ta>
            <ta e="T714" id="Seg_11040" s="T713">v</ta>
            <ta e="T715" id="Seg_11041" s="T714">adj</ta>
            <ta e="T716" id="Seg_11042" s="T715">dempro</ta>
            <ta e="T717" id="Seg_11043" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_11044" s="T717">v</ta>
            <ta e="T719" id="Seg_11045" s="T718">conj</ta>
            <ta e="T720" id="Seg_11046" s="T719">pers</ta>
            <ta e="T721" id="Seg_11047" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_11048" s="T721">v</ta>
            <ta e="T723" id="Seg_11049" s="T722">dempro</ta>
            <ta e="T724" id="Seg_11050" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_11051" s="T724">v</ta>
            <ta e="T726" id="Seg_11052" s="T725">v</ta>
            <ta e="T732" id="Seg_11053" s="T731">adv</ta>
            <ta e="T733" id="Seg_11054" s="T732">n</ta>
            <ta e="T734" id="Seg_11055" s="T733">v</ta>
            <ta e="T735" id="Seg_11056" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_11057" s="T735">pers</ta>
            <ta e="T737" id="Seg_11058" s="T736">n</ta>
            <ta e="T738" id="Seg_11059" s="T737">v</ta>
            <ta e="T739" id="Seg_11060" s="T738">v</ta>
            <ta e="T740" id="Seg_11061" s="T739">adv</ta>
            <ta e="T741" id="Seg_11062" s="T740">refl</ta>
            <ta e="T742" id="Seg_11063" s="T741">n</ta>
            <ta e="T743" id="Seg_11064" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_11065" s="T743">v</ta>
            <ta e="T745" id="Seg_11066" s="T744">pers</ta>
            <ta e="T746" id="Seg_11067" s="T745">n</ta>
            <ta e="T748" id="Seg_11068" s="T747">pers</ta>
            <ta e="T749" id="Seg_11069" s="T748">v</ta>
            <ta e="T750" id="Seg_11070" s="T749">propr</ta>
            <ta e="T751" id="Seg_11071" s="T750">v</ta>
            <ta e="T752" id="Seg_11072" s="T751">v</ta>
            <ta e="T753" id="Seg_11073" s="T752">n</ta>
            <ta e="T754" id="Seg_11074" s="T753">n</ta>
            <ta e="T755" id="Seg_11075" s="T754">v</ta>
            <ta e="T756" id="Seg_11076" s="T755">v</ta>
            <ta e="T757" id="Seg_11077" s="T756">conj</ta>
            <ta e="T758" id="Seg_11078" s="T757">v</ta>
            <ta e="T759" id="Seg_11079" s="T758">adv</ta>
            <ta e="T760" id="Seg_11080" s="T759">dempro</ta>
            <ta e="T761" id="Seg_11081" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_11082" s="T761">v</ta>
            <ta e="T763" id="Seg_11083" s="T762">v</ta>
            <ta e="T764" id="Seg_11084" s="T763">v</ta>
            <ta e="T765" id="Seg_11085" s="T764">n</ta>
            <ta e="T767" id="Seg_11086" s="T766">adv</ta>
            <ta e="T768" id="Seg_11087" s="T767">v</ta>
            <ta e="T769" id="Seg_11088" s="T768">conj</ta>
            <ta e="T770" id="Seg_11089" s="T769">v</ta>
            <ta e="T772" id="Seg_11090" s="T771">adv</ta>
            <ta e="T773" id="Seg_11091" s="T772">v</ta>
            <ta e="T774" id="Seg_11092" s="T773">dempro</ta>
            <ta e="T775" id="Seg_11093" s="T774">ptcl</ta>
            <ta e="T776" id="Seg_11094" s="T775">v</ta>
            <ta e="T777" id="Seg_11095" s="T776">que</ta>
            <ta e="T778" id="Seg_11096" s="T777">v</ta>
            <ta e="T779" id="Seg_11097" s="T778">aux</ta>
            <ta e="T780" id="Seg_11098" s="T779">v</ta>
            <ta e="T781" id="Seg_11099" s="T780">adv</ta>
            <ta e="T782" id="Seg_11100" s="T781">v</ta>
            <ta e="T783" id="Seg_11101" s="T782">n</ta>
            <ta e="T784" id="Seg_11102" s="T783">v</ta>
            <ta e="T786" id="Seg_11103" s="T785">num</ta>
            <ta e="T787" id="Seg_11104" s="T786">v</ta>
            <ta e="T788" id="Seg_11105" s="T787">v</ta>
            <ta e="T789" id="Seg_11106" s="T788">n</ta>
            <ta e="T790" id="Seg_11107" s="T789">v</ta>
            <ta e="T791" id="Seg_11108" s="T790">conj</ta>
            <ta e="T792" id="Seg_11109" s="T791">dempro</ta>
            <ta e="T793" id="Seg_11110" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_11111" s="T793">n</ta>
            <ta e="T795" id="Seg_11112" s="T794">v</ta>
            <ta e="T796" id="Seg_11113" s="T795">n</ta>
            <ta e="T798" id="Seg_11114" s="T797">pers</ta>
            <ta e="T799" id="Seg_11115" s="T798">adv</ta>
            <ta e="T800" id="Seg_11116" s="T799">adv</ta>
            <ta e="T801" id="Seg_11117" s="T800">v</ta>
            <ta e="T802" id="Seg_11118" s="T801">adv</ta>
            <ta e="T804" id="Seg_11119" s="T803">num</ta>
            <ta e="T805" id="Seg_11120" s="T804">v</ta>
            <ta e="T806" id="Seg_11121" s="T805">adv</ta>
            <ta e="T807" id="Seg_11122" s="T806">n</ta>
            <ta e="T808" id="Seg_11123" s="T807">v</ta>
            <ta e="T809" id="Seg_11124" s="T808">n</ta>
            <ta e="T810" id="Seg_11125" s="T809">v</ta>
            <ta e="T811" id="Seg_11126" s="T810">adv</ta>
            <ta e="T812" id="Seg_11127" s="T811">adv</ta>
            <ta e="T813" id="Seg_11128" s="T812">v</ta>
            <ta e="T814" id="Seg_11129" s="T813">adv</ta>
            <ta e="T815" id="Seg_11130" s="T814">v</ta>
            <ta e="T816" id="Seg_11131" s="T815">adv</ta>
            <ta e="T817" id="Seg_11132" s="T816">num</ta>
            <ta e="T818" id="Seg_11133" s="T817">v</ta>
            <ta e="T819" id="Seg_11134" s="T818">adv</ta>
            <ta e="T821" id="Seg_11135" s="T820">v</ta>
            <ta e="T823" id="Seg_11136" s="T822">adj</ta>
            <ta e="T824" id="Seg_11137" s="T823">adv</ta>
            <ta e="T825" id="Seg_11138" s="T824">v</ta>
            <ta e="T826" id="Seg_11139" s="T825">conj</ta>
            <ta e="T827" id="Seg_11140" s="T826">adv</ta>
            <ta e="T828" id="Seg_11141" s="T827">ptcl</ta>
            <ta e="T829" id="Seg_11142" s="T828">adj</ta>
            <ta e="T831" id="Seg_11143" s="T830">dempro</ta>
            <ta e="T832" id="Seg_11144" s="T831">n</ta>
            <ta e="T833" id="Seg_11145" s="T832">adv</ta>
            <ta e="T834" id="Seg_11146" s="T833">adj</ta>
            <ta e="T835" id="Seg_11147" s="T834">adj</ta>
            <ta e="T836" id="Seg_11148" s="T835">conj</ta>
            <ta e="T837" id="Seg_11149" s="T836">dempro</ta>
            <ta e="T838" id="Seg_11150" s="T837">n</ta>
            <ta e="T839" id="Seg_11151" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_11152" s="T839">adj</ta>
            <ta e="T842" id="Seg_11153" s="T841">dempro</ta>
            <ta e="T844" id="Seg_11154" s="T843">dempro</ta>
            <ta e="T845" id="Seg_11155" s="T844">n</ta>
            <ta e="T846" id="Seg_11156" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_11157" s="T846">n</ta>
            <ta e="T848" id="Seg_11158" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_11159" s="T848">v</ta>
            <ta e="T850" id="Seg_11160" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_11161" s="T850">n</ta>
            <ta e="T852" id="Seg_11162" s="T851">v</ta>
            <ta e="T853" id="Seg_11163" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_11164" s="T853">dempro</ta>
            <ta e="T855" id="Seg_11165" s="T854">v</ta>
            <ta e="T857" id="Seg_11166" s="T856">v</ta>
            <ta e="T859" id="Seg_11167" s="T858">v</ta>
            <ta e="T860" id="Seg_11168" s="T859">pers</ta>
            <ta e="T861" id="Seg_11169" s="T860">n</ta>
            <ta e="T862" id="Seg_11170" s="T861">num</ta>
            <ta e="T863" id="Seg_11171" s="T862">n</ta>
            <ta e="T865" id="Seg_11172" s="T864">dempro</ta>
            <ta e="T866" id="Seg_11173" s="T865">n</ta>
            <ta e="T867" id="Seg_11174" s="T866">v</ta>
            <ta e="T868" id="Seg_11175" s="T867">conj</ta>
            <ta e="T869" id="Seg_11176" s="T868">adv</ta>
            <ta e="T870" id="Seg_11177" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_11178" s="T870">v</ta>
            <ta e="T872" id="Seg_11179" s="T871">v</ta>
            <ta e="T873" id="Seg_11180" s="T872">n</ta>
            <ta e="T874" id="Seg_11181" s="T873">v</ta>
            <ta e="T875" id="Seg_11182" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_11183" s="T875">v</ta>
            <ta e="T877" id="Seg_11184" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_11185" s="T877">adj</ta>
            <ta e="T879" id="Seg_11186" s="T878">v</ta>
            <ta e="T880" id="Seg_11187" s="T879">v</ta>
            <ta e="T881" id="Seg_11188" s="T880">conj</ta>
            <ta e="T882" id="Seg_11189" s="T881">v</ta>
            <ta e="T883" id="Seg_11190" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_11191" s="T883">adj</ta>
            <ta e="T885" id="Seg_11192" s="T884">v</ta>
            <ta e="T887" id="Seg_11193" s="T886">dempro</ta>
            <ta e="T888" id="Seg_11194" s="T887">dempro</ta>
            <ta e="T889" id="Seg_11195" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_11196" s="T889">n</ta>
            <ta e="T891" id="Seg_11197" s="T890">v</ta>
            <ta e="T892" id="Seg_11198" s="T891">dempro</ta>
            <ta e="T893" id="Seg_11199" s="T892">v</ta>
            <ta e="T895" id="Seg_11200" s="T894">num</ta>
            <ta e="T896" id="Seg_11201" s="T895">n</ta>
            <ta e="T897" id="Seg_11202" s="T896">v</ta>
            <ta e="T898" id="Seg_11203" s="T897">conj</ta>
            <ta e="T899" id="Seg_11204" s="T898">adv</ta>
            <ta e="T900" id="Seg_11205" s="T899">n</ta>
            <ta e="T901" id="Seg_11206" s="T900">v</ta>
            <ta e="T902" id="Seg_11207" s="T901">conj</ta>
            <ta e="T903" id="Seg_11208" s="T902">v</ta>
            <ta e="T905" id="Seg_11209" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_11210" s="T905">n</ta>
            <ta e="T910" id="Seg_11211" s="T909">v</ta>
            <ta e="T913" id="Seg_11212" s="T912">pers</ta>
            <ta e="T916" id="Seg_11213" s="T915">n</ta>
            <ta e="T917" id="Seg_11214" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_11215" s="T917">n</ta>
            <ta e="T919" id="Seg_11216" s="T918">v</ta>
            <ta e="T920" id="Seg_11217" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_11218" s="T920">ptcl</ta>
            <ta e="T922" id="Seg_11219" s="T921">n</ta>
            <ta e="T923" id="Seg_11220" s="T922">v</ta>
            <ta e="T925" id="Seg_11221" s="T923">pers</ta>
            <ta e="T926" id="Seg_11222" s="T925">n</ta>
            <ta e="T927" id="Seg_11223" s="T926">ptcl</ta>
            <ta e="T928" id="Seg_11224" s="T927">n</ta>
            <ta e="T932" id="Seg_11225" s="T931">v</ta>
            <ta e="T934" id="Seg_11226" s="T932">n</ta>
            <ta e="T935" id="Seg_11227" s="T934">v</ta>
            <ta e="T936" id="Seg_11228" s="T935">n</ta>
            <ta e="T937" id="Seg_11229" s="T936">v</ta>
            <ta e="T938" id="Seg_11230" s="T937">v</ta>
            <ta e="T939" id="Seg_11231" s="T938">n</ta>
            <ta e="T940" id="Seg_11232" s="T939">n</ta>
            <ta e="T941" id="Seg_11233" s="T940">v</ta>
            <ta e="T942" id="Seg_11234" s="T941">adv</ta>
            <ta e="T944" id="Seg_11235" s="T943">v</ta>
            <ta e="T945" id="Seg_11236" s="T944">que</ta>
            <ta e="T946" id="Seg_11237" s="T945">v</ta>
            <ta e="T947" id="Seg_11238" s="T946">adv</ta>
            <ta e="T948" id="Seg_11239" s="T947">quant</ta>
            <ta e="T949" id="Seg_11240" s="T948">n</ta>
            <ta e="T950" id="Seg_11241" s="T949">adv</ta>
            <ta e="T951" id="Seg_11242" s="T950">adv</ta>
            <ta e="T952" id="Seg_11243" s="T951">v</ta>
            <ta e="T953" id="Seg_11244" s="T952">adv</ta>
            <ta e="T954" id="Seg_11245" s="T953">v</ta>
            <ta e="T955" id="Seg_11246" s="T954">que</ta>
            <ta e="T956" id="Seg_11247" s="T955">n</ta>
            <ta e="T957" id="Seg_11248" s="T956">v</ta>
            <ta e="T958" id="Seg_11249" s="T957">adv</ta>
            <ta e="T959" id="Seg_11250" s="T958">n</ta>
            <ta e="T960" id="Seg_11251" s="T959">quant</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T5" id="Seg_11252" s="T4">pro.h:A</ta>
            <ta e="T6" id="Seg_11253" s="T5">adv:Time</ta>
            <ta e="T7" id="Seg_11254" s="T6">np:G</ta>
            <ta e="T9" id="Seg_11255" s="T8">adv:L</ta>
            <ta e="T10" id="Seg_11256" s="T9">np.h:E</ta>
            <ta e="T12" id="Seg_11257" s="T11">0.2.h:A</ta>
            <ta e="T13" id="Seg_11258" s="T12">0.2.h:A</ta>
            <ta e="T14" id="Seg_11259" s="T13">np:P</ta>
            <ta e="T18" id="Seg_11260" s="T17">0.1.h:A</ta>
            <ta e="T20" id="Seg_11261" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_11262" s="T20">np.h:A</ta>
            <ta e="T24" id="Seg_11263" s="T23">np:A</ta>
            <ta e="T28" id="Seg_11264" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_11265" s="T28">pro.h:A</ta>
            <ta e="T32" id="Seg_11266" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_11267" s="T32">pro.h:R</ta>
            <ta e="T35" id="Seg_11268" s="T34">pro.h:A</ta>
            <ta e="T39" id="Seg_11269" s="T38">np:A</ta>
            <ta e="T40" id="Seg_11270" s="T39">pro.h:Com</ta>
            <ta e="T41" id="Seg_11271" s="T40">pro.h:A</ta>
            <ta e="T49" id="Seg_11272" s="T48">np:Th</ta>
            <ta e="T50" id="Seg_11273" s="T49">0.3:E</ta>
            <ta e="T54" id="Seg_11274" s="T53">np:Th</ta>
            <ta e="T56" id="Seg_11275" s="T55">np.h:E</ta>
            <ta e="T58" id="Seg_11276" s="T57">pro:Th</ta>
            <ta e="T61" id="Seg_11277" s="T60">pro.h:Poss</ta>
            <ta e="T65" id="Seg_11278" s="T64">np:Th</ta>
            <ta e="T67" id="Seg_11279" s="T66">np:Th</ta>
            <ta e="T70" id="Seg_11280" s="T69">np:Th</ta>
            <ta e="T72" id="Seg_11281" s="T71">np:Th</ta>
            <ta e="T74" id="Seg_11282" s="T73">np:P</ta>
            <ta e="T75" id="Seg_11283" s="T74">0.3.h:A</ta>
            <ta e="T78" id="Seg_11284" s="T77">np:Th</ta>
            <ta e="T82" id="Seg_11285" s="T81">np.h:E</ta>
            <ta e="T85" id="Seg_11286" s="T84">np:P</ta>
            <ta e="T86" id="Seg_11287" s="T85">0.3.h:A</ta>
            <ta e="T88" id="Seg_11288" s="T87">np:Ins</ta>
            <ta e="T89" id="Seg_11289" s="T88">adv:Time</ta>
            <ta e="T91" id="Seg_11290" s="T90">np:Ins</ta>
            <ta e="T92" id="Seg_11291" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_11292" s="T92">adv:Time</ta>
            <ta e="T94" id="Seg_11293" s="T93">np:Ins</ta>
            <ta e="T95" id="Seg_11294" s="T94">0.3.h:A</ta>
            <ta e="T97" id="Seg_11295" s="T96">np:Ins</ta>
            <ta e="T98" id="Seg_11296" s="T97">np:P</ta>
            <ta e="T99" id="Seg_11297" s="T98">0.3.h:A</ta>
            <ta e="T101" id="Seg_11298" s="T100">0.3.h:A</ta>
            <ta e="T104" id="Seg_11299" s="T103">pro.h:B</ta>
            <ta e="T107" id="Seg_11300" s="T106">np:Th</ta>
            <ta e="T108" id="Seg_11301" s="T107">0.3.h:A</ta>
            <ta e="T109" id="Seg_11302" s="T108">0.3.h:A</ta>
            <ta e="T111" id="Seg_11303" s="T110">np.h:A</ta>
            <ta e="T114" id="Seg_11304" s="T113">np:G</ta>
            <ta e="T116" id="Seg_11305" s="T115">adv:Time</ta>
            <ta e="T118" id="Seg_11306" s="T117">0.3:Th</ta>
            <ta e="T120" id="Seg_11307" s="T119">0.3.h:A</ta>
            <ta e="T121" id="Seg_11308" s="T120">adv:L</ta>
            <ta e="T123" id="Seg_11309" s="T122">adv:L</ta>
            <ta e="T125" id="Seg_11310" s="T124">np:L</ta>
            <ta e="T126" id="Seg_11311" s="T125">0.3.h:A</ta>
            <ta e="T127" id="Seg_11312" s="T126">adv:Time</ta>
            <ta e="T128" id="Seg_11313" s="T127">0.3.h:E</ta>
            <ta e="T129" id="Seg_11314" s="T128">adv:L</ta>
            <ta e="T130" id="Seg_11315" s="T129">np:P</ta>
            <ta e="T133" id="Seg_11316" s="T132">adv:Time</ta>
            <ta e="T134" id="Seg_11317" s="T133">adv:L</ta>
            <ta e="T138" id="Seg_11318" s="T137">0.3.h:A</ta>
            <ta e="T140" id="Seg_11319" s="T139">np:P</ta>
            <ta e="T142" id="Seg_11320" s="T141">adv:L</ta>
            <ta e="T143" id="Seg_11321" s="T142">0.3.h:E</ta>
            <ta e="T144" id="Seg_11322" s="T143">pro.h:A</ta>
            <ta e="T150" id="Seg_11323" s="T149">np:Th</ta>
            <ta e="T154" id="Seg_11324" s="T153">np.h:A</ta>
            <ta e="T158" id="Seg_11325" s="T157">0.3.h:A</ta>
            <ta e="T161" id="Seg_11326" s="T160">np:G</ta>
            <ta e="T162" id="Seg_11327" s="T161">0.3.h:A</ta>
            <ta e="T163" id="Seg_11328" s="T162">np.h:A</ta>
            <ta e="T167" id="Seg_11329" s="T166">np.h:A</ta>
            <ta e="T170" id="Seg_11330" s="T169">pro.h:A</ta>
            <ta e="T172" id="Seg_11331" s="T171">np:So</ta>
            <ta e="T180" id="Seg_11332" s="T179">np.h:A</ta>
            <ta e="T181" id="Seg_11333" s="T180">np.h:Com</ta>
            <ta e="T187" id="Seg_11334" s="T186">np:P</ta>
            <ta e="T190" id="Seg_11335" s="T189">np:Ins</ta>
            <ta e="T192" id="Seg_11336" s="T191">0.2.h:A</ta>
            <ta e="T194" id="Seg_11337" s="T193">adv:L</ta>
            <ta e="T196" id="Seg_11338" s="T195">0.2.h:E</ta>
            <ta e="T197" id="Seg_11339" s="T196">pro.h:Th</ta>
            <ta e="T200" id="Seg_11340" s="T199">pro.h:Poss</ta>
            <ta e="T201" id="Seg_11341" s="T200">np.h:A</ta>
            <ta e="T203" id="Seg_11342" s="T202">np:P</ta>
            <ta e="T205" id="Seg_11343" s="T204">0.3.h:A</ta>
            <ta e="T206" id="Seg_11344" s="T205">np:P</ta>
            <ta e="T207" id="Seg_11345" s="T206">0.3.h:A</ta>
            <ta e="T208" id="Seg_11346" s="T207">np:P</ta>
            <ta e="T209" id="Seg_11347" s="T208">0.3.h:A</ta>
            <ta e="T213" id="Seg_11348" s="T212">np:P</ta>
            <ta e="T215" id="Seg_11349" s="T214">0.3.h:A</ta>
            <ta e="T218" id="Seg_11350" s="T217">np.h:A</ta>
            <ta e="T221" id="Seg_11351" s="T220">adv:Time</ta>
            <ta e="T222" id="Seg_11352" s="T221">pro.h:Poss</ta>
            <ta e="T223" id="Seg_11353" s="T222">np:Th</ta>
            <ta e="T226" id="Seg_11354" s="T225">0.1.h:E</ta>
            <ta e="T227" id="Seg_11355" s="T226">pro:Th</ta>
            <ta e="T232" id="Seg_11356" s="T231">0.1.h:E</ta>
            <ta e="T234" id="Seg_11357" s="T233">pro.h:E</ta>
            <ta e="T237" id="Seg_11358" s="T236">adv:Time</ta>
            <ta e="T239" id="Seg_11359" s="T238">np:L</ta>
            <ta e="T240" id="Seg_11360" s="T239">0.2.h:A</ta>
            <ta e="T243" id="Seg_11361" s="T242">0.3:Th</ta>
            <ta e="T245" id="Seg_11362" s="T244">pro.h:P</ta>
            <ta e="T250" id="Seg_11363" s="T249">0.1.h:P</ta>
            <ta e="T254" id="Seg_11364" s="T253">np:P</ta>
            <ta e="T257" id="Seg_11365" s="T256">pro.h:A</ta>
            <ta e="T258" id="Seg_11366" s="T257">np:Ins</ta>
            <ta e="T261" id="Seg_11367" s="T260">pro:P</ta>
            <ta e="T267" id="Seg_11368" s="T266">0.2.h:A</ta>
            <ta e="T268" id="Seg_11369" s="T267">0.2.h:A</ta>
            <ta e="T269" id="Seg_11370" s="T268">0.2.h:A</ta>
            <ta e="T270" id="Seg_11371" s="T269">pro.h:Th</ta>
            <ta e="T272" id="Seg_11372" s="T271">adv:L</ta>
            <ta e="T275" id="Seg_11373" s="T274">adv:L</ta>
            <ta e="T276" id="Seg_11374" s="T275">0.3.h:A</ta>
            <ta e="T277" id="Seg_11375" s="T276">np:Poss</ta>
            <ta e="T278" id="Seg_11376" s="T277">np:L</ta>
            <ta e="T279" id="Seg_11377" s="T278">adv:Time</ta>
            <ta e="T281" id="Seg_11378" s="T280">np:Th</ta>
            <ta e="T282" id="Seg_11379" s="T281">0.3.h:E</ta>
            <ta e="T286" id="Seg_11380" s="T285">0.3:Th</ta>
            <ta e="T288" id="Seg_11381" s="T287">pro.h:A</ta>
            <ta e="T289" id="Seg_11382" s="T288">adv:L</ta>
            <ta e="T290" id="Seg_11383" s="T289">np:P</ta>
            <ta e="T293" id="Seg_11384" s="T292">adv:L</ta>
            <ta e="T295" id="Seg_11385" s="T294">0.3.h:A</ta>
            <ta e="T298" id="Seg_11386" s="T297">0.3:P</ta>
            <ta e="T300" id="Seg_11387" s="T299">pro.h:A</ta>
            <ta e="T303" id="Seg_11388" s="T302">np:L</ta>
            <ta e="T304" id="Seg_11389" s="T303">0.3.h:E</ta>
            <ta e="T305" id="Seg_11390" s="T304">adv:Time</ta>
            <ta e="T306" id="Seg_11391" s="T305">adv:L</ta>
            <ta e="T307" id="Seg_11392" s="T306">0.3.h:A</ta>
            <ta e="T308" id="Seg_11393" s="T307">np:Th</ta>
            <ta e="T309" id="Seg_11394" s="T308">0.3.h:E</ta>
            <ta e="T310" id="Seg_11395" s="T309">pro:P</ta>
            <ta e="T313" id="Seg_11396" s="T312">adv:L</ta>
            <ta e="T314" id="Seg_11397" s="T313">np:P</ta>
            <ta e="T315" id="Seg_11398" s="T314">0.3.h:A</ta>
            <ta e="T316" id="Seg_11399" s="T315">adv:L</ta>
            <ta e="T318" id="Seg_11400" s="T317">0.3.h:A</ta>
            <ta e="T322" id="Seg_11401" s="T321">0.3:P</ta>
            <ta e="T323" id="Seg_11402" s="T322">pro.h:A</ta>
            <ta e="T326" id="Seg_11403" s="T325">adv:Time</ta>
            <ta e="T327" id="Seg_11404" s="T326">0.3.h:A</ta>
            <ta e="T328" id="Seg_11405" s="T327">np:G</ta>
            <ta e="T331" id="Seg_11406" s="T330">np:L</ta>
            <ta e="T332" id="Seg_11407" s="T331">0.3.h:A</ta>
            <ta e="T333" id="Seg_11408" s="T332">np:G</ta>
            <ta e="T334" id="Seg_11409" s="T333">0.3.h:A</ta>
            <ta e="T335" id="Seg_11410" s="T334">adv:L</ta>
            <ta e="T336" id="Seg_11411" s="T335">np:Th</ta>
            <ta e="T340" id="Seg_11412" s="T339">np:Th</ta>
            <ta e="T341" id="Seg_11413" s="T340">adv:L</ta>
            <ta e="T344" id="Seg_11414" s="T343">np:Th</ta>
            <ta e="T345" id="Seg_11415" s="T344">adv:L</ta>
            <ta e="T347" id="Seg_11416" s="T346">0.2.h:A</ta>
            <ta e="T348" id="Seg_11417" s="T347">adv:L</ta>
            <ta e="T349" id="Seg_11418" s="T348">adv:L</ta>
            <ta e="T350" id="Seg_11419" s="T349">adv:L</ta>
            <ta e="T351" id="Seg_11420" s="T350">0.1.h:A</ta>
            <ta e="T354" id="Seg_11421" s="T353">adv:L</ta>
            <ta e="T355" id="Seg_11422" s="T354">0.1.h:A</ta>
            <ta e="T360" id="Seg_11423" s="T359">0.3:Th</ta>
            <ta e="T362" id="Seg_11424" s="T361">0.2.h:A</ta>
            <ta e="T363" id="Seg_11425" s="T362">0.1.h:Poss</ta>
            <ta e="T365" id="Seg_11426" s="T364">np.h:E</ta>
            <ta e="T370" id="Seg_11427" s="T369">adv:L</ta>
            <ta e="T372" id="Seg_11428" s="T371">pro:Th</ta>
            <ta e="T375" id="Seg_11429" s="T374">np.h:A</ta>
            <ta e="T378" id="Seg_11430" s="T377">pro.h:Poss</ta>
            <ta e="T379" id="Seg_11431" s="T378">np:Ins</ta>
            <ta e="T382" id="Seg_11432" s="T381">pro.h:A</ta>
            <ta e="T383" id="Seg_11433" s="T382">pro.h:Poss</ta>
            <ta e="T385" id="Seg_11434" s="T384">np:Th</ta>
            <ta e="T388" id="Seg_11435" s="T387">0.2.h:A</ta>
            <ta e="T390" id="Seg_11436" s="T389">pro.h:A</ta>
            <ta e="T392" id="Seg_11437" s="T391">pro.h:R</ta>
            <ta e="T393" id="Seg_11438" s="T392">pro.h:A</ta>
            <ta e="T394" id="Seg_11439" s="T393">pro:Th</ta>
            <ta e="T395" id="Seg_11440" s="T394">pro:So</ta>
            <ta e="T405" id="Seg_11441" s="T404">np:Th 0.2.h:Poss</ta>
            <ta e="T407" id="Seg_11442" s="T406">pro:Th</ta>
            <ta e="T409" id="Seg_11443" s="T408">np:Th</ta>
            <ta e="T411" id="Seg_11444" s="T410">pro.h:A</ta>
            <ta e="T416" id="Seg_11445" s="T415">np:G</ta>
            <ta e="T417" id="Seg_11446" s="T416">np.h:A</ta>
            <ta e="T420" id="Seg_11447" s="T419">pro:G</ta>
            <ta e="T421" id="Seg_11448" s="T420">0.2.h:A</ta>
            <ta e="T422" id="Seg_11449" s="T421">np:Th</ta>
            <ta e="T424" id="Seg_11450" s="T423">pro.h:A</ta>
            <ta e="T430" id="Seg_11451" s="T429">pro.h:A</ta>
            <ta e="T433" id="Seg_11452" s="T432">adv:L</ta>
            <ta e="T437" id="Seg_11453" s="T436">pro.h:A</ta>
            <ta e="T442" id="Seg_11454" s="T441">pro.h:A</ta>
            <ta e="T444" id="Seg_11455" s="T443">pro.h:Com</ta>
            <ta e="T446" id="Seg_11456" s="T445">pro.h:E</ta>
            <ta e="T449" id="Seg_11457" s="T448">pro.h:E</ta>
            <ta e="T454" id="Seg_11458" s="T453">0.1.h:A</ta>
            <ta e="T456" id="Seg_11459" s="T455">pro.h:Poss</ta>
            <ta e="T457" id="Seg_11460" s="T456">np:P</ta>
            <ta e="T462" id="Seg_11461" s="T461">pro.h:A</ta>
            <ta e="T464" id="Seg_11462" s="T463">adv:L</ta>
            <ta e="T466" id="Seg_11463" s="T465">np.h:Th</ta>
            <ta e="T467" id="Seg_11464" s="T466">0.3.h:P</ta>
            <ta e="T476" id="Seg_11465" s="T475">0.3:A</ta>
            <ta e="T477" id="Seg_11466" s="T476">np:P</ta>
            <ta e="T481" id="Seg_11467" s="T480">np:P</ta>
            <ta e="T486" id="Seg_11468" s="T484">np:Th</ta>
            <ta e="T488" id="Seg_11469" s="T487">np:Th</ta>
            <ta e="T496" id="Seg_11470" s="T495">0.2.h:A</ta>
            <ta e="T498" id="Seg_11471" s="T497">pro.h:Th</ta>
            <ta e="T499" id="Seg_11472" s="T498">0.2.h:A</ta>
            <ta e="T501" id="Seg_11473" s="T500">pro.h:Th</ta>
            <ta e="T502" id="Seg_11474" s="T501">0.2.h:A</ta>
            <ta e="T504" id="Seg_11475" s="T503">pro.h:Th</ta>
            <ta e="T505" id="Seg_11476" s="T504">0.2.h:A</ta>
            <ta e="T507" id="Seg_11477" s="T506">pro.h:Th</ta>
            <ta e="T508" id="Seg_11478" s="T507">pro.h:A</ta>
            <ta e="T510" id="Seg_11479" s="T509">pro:So</ta>
            <ta e="T511" id="Seg_11480" s="T510">np:Th</ta>
            <ta e="T513" id="Seg_11481" s="T512">pro.h:A</ta>
            <ta e="T515" id="Seg_11482" s="T514">pro.h:R</ta>
            <ta e="T516" id="Seg_11483" s="T515">np:Th</ta>
            <ta e="T519" id="Seg_11484" s="T518">0.2.h:A</ta>
            <ta e="T520" id="Seg_11485" s="T519">pro.h:Th</ta>
            <ta e="T522" id="Seg_11486" s="T521">pro.h:B</ta>
            <ta e="T523" id="Seg_11487" s="T522">0.2.h:A</ta>
            <ta e="T524" id="Seg_11488" s="T523">pro.h:Th</ta>
            <ta e="T525" id="Seg_11489" s="T524">pro.h:A</ta>
            <ta e="T527" id="Seg_11490" s="T526">np:Th</ta>
            <ta e="T530" id="Seg_11491" s="T529">pro.h:A</ta>
            <ta e="T532" id="Seg_11492" s="T531">np:Th</ta>
            <ta e="T539" id="Seg_11493" s="T538">0.2.h:A</ta>
            <ta e="T541" id="Seg_11494" s="T540">np:G</ta>
            <ta e="T542" id="Seg_11495" s="T541">np:Th</ta>
            <ta e="T543" id="Seg_11496" s="T542">0.2.h:A</ta>
            <ta e="T545" id="Seg_11497" s="T544">np:Poss</ta>
            <ta e="T547" id="Seg_11498" s="T546">np:Poss</ta>
            <ta e="T548" id="Seg_11499" s="T547">np:P</ta>
            <ta e="T551" id="Seg_11500" s="T550">pro.h:B</ta>
            <ta e="T555" id="Seg_11501" s="T554">0.2.h:A</ta>
            <ta e="T557" id="Seg_11502" s="T556">pro.h:A</ta>
            <ta e="T558" id="Seg_11503" s="T557">adv:Time</ta>
            <ta e="T559" id="Seg_11504" s="T558">np:G</ta>
            <ta e="T568" id="Seg_11505" s="T567">np.h:A</ta>
            <ta e="T569" id="Seg_11506" s="T568">np:G</ta>
            <ta e="T571" id="Seg_11507" s="T570">0.3.h:A</ta>
            <ta e="T575" id="Seg_11508" s="T574">np:Th</ta>
            <ta e="T579" id="Seg_11509" s="T578">pro.h:A</ta>
            <ta e="T582" id="Seg_11510" s="T581">0.2.h:A</ta>
            <ta e="T583" id="Seg_11511" s="T582">pro.h:A</ta>
            <ta e="T592" id="Seg_11512" s="T591">np:Th</ta>
            <ta e="T593" id="Seg_11513" s="T592">0.1.h:A</ta>
            <ta e="T595" id="Seg_11514" s="T594">np.h:A</ta>
            <ta e="T596" id="Seg_11515" s="T595">pro.h:Poss</ta>
            <ta e="T597" id="Seg_11516" s="T596">np.h:Th</ta>
            <ta e="T602" id="Seg_11517" s="T601">np.h:Th</ta>
            <ta e="T603" id="Seg_11518" s="T602">np:L</ta>
            <ta e="T606" id="Seg_11519" s="T605">np.h:A</ta>
            <ta e="T608" id="Seg_11520" s="T607">np:P</ta>
            <ta e="T609" id="Seg_11521" s="T608">0.3.h:A</ta>
            <ta e="T611" id="Seg_11522" s="T610">np:G</ta>
            <ta e="T612" id="Seg_11523" s="T611">0.3.h:A</ta>
            <ta e="T613" id="Seg_11524" s="T612">np:Th</ta>
            <ta e="T617" id="Seg_11525" s="T616">0.3.h:A</ta>
            <ta e="T619" id="Seg_11526" s="T618">pro.h:A</ta>
            <ta e="T621" id="Seg_11527" s="T620">np:Th</ta>
            <ta e="T624" id="Seg_11528" s="T623">pro.h:A</ta>
            <ta e="T630" id="Seg_11529" s="T629">np:P</ta>
            <ta e="T631" id="Seg_11530" s="T630">0.1.h:A</ta>
            <ta e="T633" id="Seg_11531" s="T632">np:P</ta>
            <ta e="T634" id="Seg_11532" s="T633">0.1.h:A</ta>
            <ta e="T637" id="Seg_11533" s="T636">adv:Time</ta>
            <ta e="T640" id="Seg_11534" s="T639">n:Time</ta>
            <ta e="T643" id="Seg_11535" s="T642">np:P</ta>
            <ta e="T644" id="Seg_11536" s="T643">0.1.h:A</ta>
            <ta e="T650" id="Seg_11537" s="T649">pro.h:Poss</ta>
            <ta e="T651" id="Seg_11538" s="T650">np.h:A</ta>
            <ta e="T654" id="Seg_11539" s="T653">pro.h:E</ta>
            <ta e="T657" id="Seg_11540" s="T656">pro.h:Th</ta>
            <ta e="T659" id="Seg_11541" s="T658">0.2.h:A</ta>
            <ta e="T660" id="Seg_11542" s="T659">0.2.h:A</ta>
            <ta e="T661" id="Seg_11543" s="T660">adv:Time</ta>
            <ta e="T662" id="Seg_11544" s="T661">pro.h:A</ta>
            <ta e="T664" id="Seg_11545" s="T663">0.2.h:E</ta>
            <ta e="T666" id="Seg_11546" s="T665">pro.h:Poss</ta>
            <ta e="T669" id="Seg_11547" s="T668">np:A</ta>
            <ta e="T672" id="Seg_11548" s="T671">0.3:A</ta>
            <ta e="T674" id="Seg_11549" s="T673">0.2.h:A</ta>
            <ta e="T675" id="Seg_11550" s="T674">np:Th</ta>
            <ta e="T677" id="Seg_11551" s="T676">adv:Time</ta>
            <ta e="T678" id="Seg_11552" s="T677">np.h:A</ta>
            <ta e="T679" id="Seg_11553" s="T678">pro:G</ta>
            <ta e="T681" id="Seg_11554" s="T680">np:P</ta>
            <ta e="T684" id="Seg_11555" s="T683">0.3.h:Th</ta>
            <ta e="T685" id="Seg_11556" s="T684">adv:L</ta>
            <ta e="T688" id="Seg_11557" s="T687">np.h:Poss</ta>
            <ta e="T689" id="Seg_11558" s="T688">np.h:E</ta>
            <ta e="T691" id="Seg_11559" s="T690">adv:L</ta>
            <ta e="T692" id="Seg_11560" s="T691">np.h:A 0.3.h:Poss</ta>
            <ta e="T695" id="Seg_11561" s="T694">pro.h:R</ta>
            <ta e="T696" id="Seg_11562" s="T695">0.3.h:A</ta>
            <ta e="T697" id="Seg_11563" s="T696">0.1.h:A</ta>
            <ta e="T698" id="Seg_11564" s="T697">np:G</ta>
            <ta e="T699" id="Seg_11565" s="T698">pro.h:A</ta>
            <ta e="T700" id="Seg_11566" s="T699">np:G</ta>
            <ta e="T703" id="Seg_11567" s="T702">pro.h:A</ta>
            <ta e="T705" id="Seg_11568" s="T704">pro.h:R</ta>
            <ta e="T706" id="Seg_11569" s="T705">0.2.h:A</ta>
            <ta e="T707" id="Seg_11570" s="T706">np.h:B</ta>
            <ta e="T708" id="Seg_11571" s="T707">pro:Th</ta>
            <ta e="T710" id="Seg_11572" s="T709">np:Th</ta>
            <ta e="T712" id="Seg_11573" s="T711">0.2.h:A</ta>
            <ta e="T713" id="Seg_11574" s="T712">np:Th</ta>
            <ta e="T714" id="Seg_11575" s="T713">0.2.h:A</ta>
            <ta e="T716" id="Seg_11576" s="T715">pro.h:A</ta>
            <ta e="T720" id="Seg_11577" s="T719">pro.h:A</ta>
            <ta e="T723" id="Seg_11578" s="T722">pro.h:A</ta>
            <ta e="T732" id="Seg_11579" s="T731">adv:Time</ta>
            <ta e="T733" id="Seg_11580" s="T732">np.h:A</ta>
            <ta e="T736" id="Seg_11581" s="T735">pro.h:A</ta>
            <ta e="T737" id="Seg_11582" s="T736">np:Th</ta>
            <ta e="T739" id="Seg_11583" s="T738">0.1.h:A</ta>
            <ta e="T742" id="Seg_11584" s="T741">np:G</ta>
            <ta e="T744" id="Seg_11585" s="T743">0.3.h:A</ta>
            <ta e="T745" id="Seg_11586" s="T744">pro.h:B</ta>
            <ta e="T746" id="Seg_11587" s="T745">np:Th</ta>
            <ta e="T748" id="Seg_11588" s="T747">pro.h:Th</ta>
            <ta e="T750" id="Seg_11589" s="T749">np:L</ta>
            <ta e="T752" id="Seg_11590" s="T751">0.1.h:A</ta>
            <ta e="T753" id="Seg_11591" s="T752">np:G</ta>
            <ta e="T754" id="Seg_11592" s="T753">np:Th</ta>
            <ta e="T760" id="Seg_11593" s="T759">pro.h:A</ta>
            <ta e="T763" id="Seg_11594" s="T762">0.3.h:A</ta>
            <ta e="T764" id="Seg_11595" s="T763">0.2.h:A</ta>
            <ta e="T765" id="Seg_11596" s="T764">np:Th</ta>
            <ta e="T767" id="Seg_11597" s="T766">adv:Time</ta>
            <ta e="T768" id="Seg_11598" s="T767">0.2.h:A</ta>
            <ta e="T770" id="Seg_11599" s="T769">0.2.h:A</ta>
            <ta e="T772" id="Seg_11600" s="T771">adv:L</ta>
            <ta e="T773" id="Seg_11601" s="T772">0.1.h:A</ta>
            <ta e="T774" id="Seg_11602" s="T773">pro.h:A</ta>
            <ta e="T778" id="Seg_11603" s="T777">0.2.h:A</ta>
            <ta e="T779" id="Seg_11604" s="T778">0.2.h:A</ta>
            <ta e="T782" id="Seg_11605" s="T781">0.2.h:A</ta>
            <ta e="T783" id="Seg_11606" s="T782">np:P</ta>
            <ta e="T784" id="Seg_11607" s="T783">0.2.h:A</ta>
            <ta e="T786" id="Seg_11608" s="T785">pro.h:R</ta>
            <ta e="T787" id="Seg_11609" s="T786">0.1.h:A</ta>
            <ta e="T788" id="Seg_11610" s="T787">0.2.h:A</ta>
            <ta e="T789" id="Seg_11611" s="T788">np:Th</ta>
            <ta e="T790" id="Seg_11612" s="T789">0.2.h:A</ta>
            <ta e="T794" id="Seg_11613" s="T793">np:Th</ta>
            <ta e="T795" id="Seg_11614" s="T794">0.3.h:A</ta>
            <ta e="T796" id="Seg_11615" s="T795">np:G</ta>
            <ta e="T798" id="Seg_11616" s="T797">pro.h:A</ta>
            <ta e="T799" id="Seg_11617" s="T798">adv:Time</ta>
            <ta e="T804" id="Seg_11618" s="T803">np:Th</ta>
            <ta e="T806" id="Seg_11619" s="T805">adv:Time</ta>
            <ta e="T807" id="Seg_11620" s="T806">np:P</ta>
            <ta e="T808" id="Seg_11621" s="T807">0.1.h:A</ta>
            <ta e="T810" id="Seg_11622" s="T809">0.1.h:A</ta>
            <ta e="T811" id="Seg_11623" s="T810">adv:Time</ta>
            <ta e="T813" id="Seg_11624" s="T812">0.1.h:A</ta>
            <ta e="T815" id="Seg_11625" s="T814">0.1.h:E</ta>
            <ta e="T817" id="Seg_11626" s="T816">np:Th</ta>
            <ta e="T819" id="Seg_11627" s="T818">adv:Time</ta>
            <ta e="T821" id="Seg_11628" s="T820">0.1.h:A</ta>
            <ta e="T824" id="Seg_11629" s="T823">adv:Time</ta>
            <ta e="T825" id="Seg_11630" s="T824">0.1.h:A</ta>
            <ta e="T827" id="Seg_11631" s="T826">adv:Time</ta>
            <ta e="T832" id="Seg_11632" s="T831">np:Th</ta>
            <ta e="T838" id="Seg_11633" s="T837">np:Th</ta>
            <ta e="T845" id="Seg_11634" s="T844">np.h:E</ta>
            <ta e="T847" id="Seg_11635" s="T846">np:P</ta>
            <ta e="T851" id="Seg_11636" s="T850">np:P</ta>
            <ta e="T854" id="Seg_11637" s="T853">pro:P</ta>
            <ta e="T859" id="Seg_11638" s="T858">0.2.h:A</ta>
            <ta e="T860" id="Seg_11639" s="T859">pro.h:R</ta>
            <ta e="T861" id="Seg_11640" s="T860">np:Th</ta>
            <ta e="T866" id="Seg_11641" s="T865">np.h:E</ta>
            <ta e="T869" id="Seg_11642" s="T868">adv:Time</ta>
            <ta e="T871" id="Seg_11643" s="T870">0.3.h:E</ta>
            <ta e="T872" id="Seg_11644" s="T871">0.3.h:A</ta>
            <ta e="T873" id="Seg_11645" s="T872">np:Ins</ta>
            <ta e="T874" id="Seg_11646" s="T873">0.3.h:A</ta>
            <ta e="T876" id="Seg_11647" s="T875">0.3.h:Th</ta>
            <ta e="T880" id="Seg_11648" s="T879">0.3.h:A</ta>
            <ta e="T882" id="Seg_11649" s="T881">0.3.h:A</ta>
            <ta e="T888" id="Seg_11650" s="T887">pro.h:A</ta>
            <ta e="T890" id="Seg_11651" s="T889">np.h:P</ta>
            <ta e="T892" id="Seg_11652" s="T891">pro.h:Th</ta>
            <ta e="T893" id="Seg_11653" s="T892">0.3.h:A</ta>
            <ta e="T896" id="Seg_11654" s="T895">n:Time</ta>
            <ta e="T897" id="Seg_11655" s="T896">0.3.h:E</ta>
            <ta e="T899" id="Seg_11656" s="T898">adv:Time</ta>
            <ta e="T900" id="Seg_11657" s="T899">np:G</ta>
            <ta e="T901" id="Seg_11658" s="T900">0.3.h:A</ta>
            <ta e="T903" id="Seg_11659" s="T902">0.3.h:A</ta>
            <ta e="T906" id="Seg_11660" s="T905">np:P</ta>
            <ta e="T910" id="Seg_11661" s="T909">0.3.h:A</ta>
            <ta e="T913" id="Seg_11662" s="T912">pro.h:Poss</ta>
            <ta e="T916" id="Seg_11663" s="T915">np:E</ta>
            <ta e="T918" id="Seg_11664" s="T917">np:Ins</ta>
            <ta e="T919" id="Seg_11665" s="T918">0.3.h:A</ta>
            <ta e="T922" id="Seg_11666" s="T921">np:Th</ta>
            <ta e="T926" id="Seg_11667" s="T925">np:Ins</ta>
            <ta e="T928" id="Seg_11668" s="T927">np:L 0.1.h:Poss</ta>
            <ta e="T932" id="Seg_11669" s="T931">0.3:P</ta>
            <ta e="T934" id="Seg_11670" s="T932">np:G</ta>
            <ta e="T935" id="Seg_11671" s="T934">0.2.h:A</ta>
            <ta e="T936" id="Seg_11672" s="T935">np:Th</ta>
            <ta e="T938" id="Seg_11673" s="T937">0.1.h:A</ta>
            <ta e="T939" id="Seg_11674" s="T938">np:G</ta>
            <ta e="T940" id="Seg_11675" s="T939">np:Th</ta>
            <ta e="T946" id="Seg_11676" s="T945">0.3.h:A</ta>
            <ta e="T949" id="Seg_11677" s="T948">np:Th</ta>
            <ta e="T950" id="Seg_11678" s="T949">adv:Time</ta>
            <ta e="T952" id="Seg_11679" s="T951">0.2.h:A</ta>
            <ta e="T953" id="Seg_11680" s="T952">adv:Time</ta>
            <ta e="T956" id="Seg_11681" s="T955">np:G</ta>
            <ta e="T959" id="Seg_11682" s="T958">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T5" id="Seg_11683" s="T4">pro.h:S</ta>
            <ta e="T8" id="Seg_11684" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_11685" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_11686" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_11687" s="T11">v:pred 0.2.h:S</ta>
            <ta e="T13" id="Seg_11688" s="T12">v:pred 0.2.h:S</ta>
            <ta e="T14" id="Seg_11689" s="T13">np:O</ta>
            <ta e="T17" id="Seg_11690" s="T16">adj:pred</ta>
            <ta e="T18" id="Seg_11691" s="T17">v:pred 0.1.h:S</ta>
            <ta e="T21" id="Seg_11692" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_11693" s="T21">v:pred</ta>
            <ta e="T23" id="Seg_11694" s="T22">s:purp</ta>
            <ta e="T24" id="Seg_11695" s="T23">np:S</ta>
            <ta e="T29" id="Seg_11696" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_11697" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_11698" s="T31">pro.h:S</ta>
            <ta e="T34" id="Seg_11699" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_11700" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_11701" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_11702" s="T36">s:purp</ta>
            <ta e="T39" id="Seg_11703" s="T38">np:S</ta>
            <ta e="T41" id="Seg_11704" s="T40">pro.h:S</ta>
            <ta e="T43" id="Seg_11705" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_11706" s="T45">v:pred</ta>
            <ta e="T49" id="Seg_11707" s="T48">np:O</ta>
            <ta e="T50" id="Seg_11708" s="T49">v:pred 0.3:S</ta>
            <ta e="T54" id="Seg_11709" s="T53">np:S</ta>
            <ta e="T964" id="Seg_11710" s="T54">conv:pred</ta>
            <ta e="T55" id="Seg_11711" s="T964">v:pred</ta>
            <ta e="T56" id="Seg_11712" s="T55">np.h:S</ta>
            <ta e="T58" id="Seg_11713" s="T57">pro:O</ta>
            <ta e="T59" id="Seg_11714" s="T58">ptcl.neg</ta>
            <ta e="T60" id="Seg_11715" s="T59">v:pred</ta>
            <ta e="T64" id="Seg_11716" s="T63">adj:pred</ta>
            <ta e="T65" id="Seg_11717" s="T64">np:S</ta>
            <ta e="T67" id="Seg_11718" s="T66">np:S</ta>
            <ta e="T68" id="Seg_11719" s="T67">adj:pred</ta>
            <ta e="T70" id="Seg_11720" s="T69">np:S</ta>
            <ta e="T71" id="Seg_11721" s="T70">adj:pred</ta>
            <ta e="T72" id="Seg_11722" s="T71">np:S</ta>
            <ta e="T73" id="Seg_11723" s="T72">adj:pred</ta>
            <ta e="T74" id="Seg_11724" s="T73">np:O</ta>
            <ta e="T75" id="Seg_11725" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T78" id="Seg_11726" s="T77">np:S</ta>
            <ta e="T80" id="Seg_11727" s="T79">adj:pred</ta>
            <ta e="T82" id="Seg_11728" s="T81">np.h:S</ta>
            <ta e="T84" id="Seg_11729" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_11730" s="T84">np:O</ta>
            <ta e="T86" id="Seg_11731" s="T85">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_11732" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T95" id="Seg_11733" s="T94">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_11734" s="T97">np:O</ta>
            <ta e="T99" id="Seg_11735" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T101" id="Seg_11736" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T107" id="Seg_11737" s="T106">np:O</ta>
            <ta e="T108" id="Seg_11738" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_11739" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_11740" s="T110">np.h:S</ta>
            <ta e="T113" id="Seg_11741" s="T112">v:pred</ta>
            <ta e="T117" id="Seg_11742" s="T116">adj:pred</ta>
            <ta e="T118" id="Seg_11743" s="T117">cop 0.3:S</ta>
            <ta e="T120" id="Seg_11744" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_11745" s="T125">v:pred 0.3.h:S</ta>
            <ta e="T128" id="Seg_11746" s="T127">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_11747" s="T129">np:S</ta>
            <ta e="T131" id="Seg_11748" s="T130">ptcl.neg</ta>
            <ta e="T132" id="Seg_11749" s="T131">v:pred</ta>
            <ta e="T138" id="Seg_11750" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_11751" s="T139">np:O</ta>
            <ta e="T143" id="Seg_11752" s="T142">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_11753" s="T143">pro.h:S</ta>
            <ta e="T146" id="Seg_11754" s="T145">v:pred</ta>
            <ta e="T150" id="Seg_11755" s="T149">np:S</ta>
            <ta e="T152" id="Seg_11756" s="T151">adj:pred</ta>
            <ta e="T153" id="Seg_11757" s="T152">cop</ta>
            <ta e="T154" id="Seg_11758" s="T153">np.h:S</ta>
            <ta e="T155" id="Seg_11759" s="T154">v:pred</ta>
            <ta e="T158" id="Seg_11760" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T162" id="Seg_11761" s="T161">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_11762" s="T162">np.h:S</ta>
            <ta e="T164" id="Seg_11763" s="T163">ptcl.neg</ta>
            <ta e="T165" id="Seg_11764" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_11765" s="T166">np.h:S</ta>
            <ta e="T168" id="Seg_11766" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_11767" s="T169">pro.h:S</ta>
            <ta e="T173" id="Seg_11768" s="T172">v:pred</ta>
            <ta e="T180" id="Seg_11769" s="T179">np.h:S</ta>
            <ta e="T186" id="Seg_11770" s="T185">v:pred</ta>
            <ta e="T187" id="Seg_11771" s="T186">np:S</ta>
            <ta e="T189" id="Seg_11772" s="T188">v:pred</ta>
            <ta e="T192" id="Seg_11773" s="T191">v:pred 0.2.h:S</ta>
            <ta e="T195" id="Seg_11774" s="T194">ptcl.neg</ta>
            <ta e="T196" id="Seg_11775" s="T195">v:pred 0.2.h:S</ta>
            <ta e="T197" id="Seg_11776" s="T196">pro.h:S</ta>
            <ta e="T198" id="Seg_11777" s="T197">adj:pred</ta>
            <ta e="T199" id="Seg_11778" s="T198">v:pred</ta>
            <ta e="T201" id="Seg_11779" s="T200">np.h:S</ta>
            <ta e="T202" id="Seg_11780" s="T201">v:pred</ta>
            <ta e="T203" id="Seg_11781" s="T202">np:O</ta>
            <ta e="T205" id="Seg_11782" s="T204">v:pred 0.3.h:S</ta>
            <ta e="T206" id="Seg_11783" s="T205">np:O</ta>
            <ta e="T207" id="Seg_11784" s="T206">v:pred 0.3.h:S</ta>
            <ta e="T208" id="Seg_11785" s="T207">np:O</ta>
            <ta e="T209" id="Seg_11786" s="T208">v:pred 0.3.h:S</ta>
            <ta e="T213" id="Seg_11787" s="T212">np:O</ta>
            <ta e="T215" id="Seg_11788" s="T214">v:pred 0.3.h:S</ta>
            <ta e="T218" id="Seg_11789" s="T217">np.h:S</ta>
            <ta e="T220" id="Seg_11790" s="T219">v:pred</ta>
            <ta e="T223" id="Seg_11791" s="T222">np:S</ta>
            <ta e="T224" id="Seg_11792" s="T223">v:pred</ta>
            <ta e="T225" id="Seg_11793" s="T224">ptcl.neg</ta>
            <ta e="T226" id="Seg_11794" s="T225">v:pred 0.1.h:S</ta>
            <ta e="T227" id="Seg_11795" s="T226">pro:S</ta>
            <ta e="T228" id="Seg_11796" s="T227">v:pred</ta>
            <ta e="T232" id="Seg_11797" s="T231">v:pred 0.1.h:S</ta>
            <ta e="T234" id="Seg_11798" s="T233">pro.h:S</ta>
            <ta e="T235" id="Seg_11799" s="T234">v:pred</ta>
            <ta e="T240" id="Seg_11800" s="T239">v:pred 0.2.h:S</ta>
            <ta e="T242" id="Seg_11801" s="T241">adj:pred</ta>
            <ta e="T243" id="Seg_11802" s="T242">cop 0.3:S</ta>
            <ta e="T245" id="Seg_11803" s="T244">pro.h:S</ta>
            <ta e="T247" id="Seg_11804" s="T246">v:pred</ta>
            <ta e="T250" id="Seg_11805" s="T249">v:pred 0.1.h:S</ta>
            <ta e="T253" id="Seg_11806" s="T252">adj:pred</ta>
            <ta e="T254" id="Seg_11807" s="T253">np:S</ta>
            <ta e="T256" id="Seg_11808" s="T255">v:pred</ta>
            <ta e="T257" id="Seg_11809" s="T256">pro.h:S</ta>
            <ta e="T260" id="Seg_11810" s="T259">v:pred</ta>
            <ta e="T261" id="Seg_11811" s="T260">pro:S</ta>
            <ta e="T262" id="Seg_11812" s="T261">ptcl.neg</ta>
            <ta e="T263" id="Seg_11813" s="T262">v:pred</ta>
            <ta e="T265" id="Seg_11814" s="T264">s:temp</ta>
            <ta e="T267" id="Seg_11815" s="T266">v:pred 0.2.h:S</ta>
            <ta e="T268" id="Seg_11816" s="T267">v:pred 0.2.h:S</ta>
            <ta e="T269" id="Seg_11817" s="T268">v:pred 0.2.h:S</ta>
            <ta e="T270" id="Seg_11818" s="T269">pro.h:O</ta>
            <ta e="T276" id="Seg_11819" s="T275">v:pred 0.3.h:S</ta>
            <ta e="T281" id="Seg_11820" s="T280">np:O</ta>
            <ta e="T282" id="Seg_11821" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_11822" s="T284">ptcl.neg</ta>
            <ta e="T286" id="Seg_11823" s="T285">v:pred 0.3:S</ta>
            <ta e="T288" id="Seg_11824" s="T287">pro.h:S</ta>
            <ta e="T290" id="Seg_11825" s="T289">np:O</ta>
            <ta e="T291" id="Seg_11826" s="T290">v:pred</ta>
            <ta e="T295" id="Seg_11827" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T297" id="Seg_11828" s="T296">adj:pred</ta>
            <ta e="T298" id="Seg_11829" s="T297">cop 0.3:S</ta>
            <ta e="T300" id="Seg_11830" s="T299">pro.h:S</ta>
            <ta e="T302" id="Seg_11831" s="T301">v:pred</ta>
            <ta e="T304" id="Seg_11832" s="T303">v:pred 0.3.h:S</ta>
            <ta e="T307" id="Seg_11833" s="T306">v:pred 0.3.h:S</ta>
            <ta e="T308" id="Seg_11834" s="T307">np:O</ta>
            <ta e="T309" id="Seg_11835" s="T308">v:pred 0.3.h:S</ta>
            <ta e="T310" id="Seg_11836" s="T309">pro:S</ta>
            <ta e="T311" id="Seg_11837" s="T310">ptcl.neg</ta>
            <ta e="T312" id="Seg_11838" s="T311">v:pred</ta>
            <ta e="T314" id="Seg_11839" s="T313">np:O</ta>
            <ta e="T315" id="Seg_11840" s="T314">v:pred 0.3.h:S</ta>
            <ta e="T318" id="Seg_11841" s="T317">v:pred 0.3.h:S</ta>
            <ta e="T319" id="Seg_11842" s="T318">s:purp</ta>
            <ta e="T321" id="Seg_11843" s="T320">adj:pred</ta>
            <ta e="T322" id="Seg_11844" s="T321">cop 0.3:S</ta>
            <ta e="T323" id="Seg_11845" s="T322">pro.h:S</ta>
            <ta e="T324" id="Seg_11846" s="T323">ptcl.neg</ta>
            <ta e="T325" id="Seg_11847" s="T324">v:pred</ta>
            <ta e="T327" id="Seg_11848" s="T326">v:pred 0.3.h:S</ta>
            <ta e="T332" id="Seg_11849" s="T331">v:pred 0.3.h:S</ta>
            <ta e="T334" id="Seg_11850" s="T333">v:pred 0.3.h:S</ta>
            <ta e="T336" id="Seg_11851" s="T335">np:S</ta>
            <ta e="T337" id="Seg_11852" s="T336">adj:pred</ta>
            <ta e="T340" id="Seg_11853" s="T339">np:S</ta>
            <ta e="T341" id="Seg_11854" s="T340">adj:pred</ta>
            <ta e="T344" id="Seg_11855" s="T343">np:S</ta>
            <ta e="T345" id="Seg_11856" s="T344">adj:pred</ta>
            <ta e="T347" id="Seg_11857" s="T346">v:pred 0.2.h:S</ta>
            <ta e="T351" id="Seg_11858" s="T350">v:pred 0.1.h:S</ta>
            <ta e="T355" id="Seg_11859" s="T354">v:pred 0.1.h:S</ta>
            <ta e="T360" id="Seg_11860" s="T359">v:pred 0.3:S</ta>
            <ta e="T361" id="Seg_11861" s="T360">ptcl.neg</ta>
            <ta e="T362" id="Seg_11862" s="T361">v:pred 0.2.h:S</ta>
            <ta e="T365" id="Seg_11863" s="T364">np.h:S</ta>
            <ta e="T369" id="Seg_11864" s="T368">v:pred</ta>
            <ta e="T372" id="Seg_11865" s="T371">pro:S</ta>
            <ta e="T373" id="Seg_11866" s="T372">v:pred</ta>
            <ta e="T375" id="Seg_11867" s="T374">np.h:S</ta>
            <ta e="T377" id="Seg_11868" s="T376">v:pred</ta>
            <ta e="T382" id="Seg_11869" s="T381">pro.h:S</ta>
            <ta e="T385" id="Seg_11870" s="T384">np:O</ta>
            <ta e="T386" id="Seg_11871" s="T385">v:pred</ta>
            <ta e="T388" id="Seg_11872" s="T387">v:pred 0.2.h:S</ta>
            <ta e="T390" id="Seg_11873" s="T389">pro.h:S</ta>
            <ta e="T391" id="Seg_11874" s="T390">v:pred</ta>
            <ta e="T393" id="Seg_11875" s="T392">pro.h:S</ta>
            <ta e="T394" id="Seg_11876" s="T393">pro:O</ta>
            <ta e="T396" id="Seg_11877" s="T395">v:pred</ta>
            <ta e="T403" id="Seg_11878" s="T402">adj:pred</ta>
            <ta e="T405" id="Seg_11879" s="T404">np:S</ta>
            <ta e="T407" id="Seg_11880" s="T406">pro:S</ta>
            <ta e="T409" id="Seg_11881" s="T408">np:O</ta>
            <ta e="T411" id="Seg_11882" s="T410">pro.h:S</ta>
            <ta e="T414" id="Seg_11883" s="T413">v:pred</ta>
            <ta e="T417" id="Seg_11884" s="T416">np.h:S</ta>
            <ta e="T419" id="Seg_11885" s="T418">v:pred</ta>
            <ta e="T421" id="Seg_11886" s="T420">v:pred 0.2.h:S</ta>
            <ta e="T424" id="Seg_11887" s="T423">pro.h:S</ta>
            <ta e="T425" id="Seg_11888" s="T424">v:pred</ta>
            <ta e="T430" id="Seg_11889" s="T429">pro.h:S</ta>
            <ta e="T432" id="Seg_11890" s="T431">ptcl.neg</ta>
            <ta e="T435" id="Seg_11891" s="T434">v:pred</ta>
            <ta e="T437" id="Seg_11892" s="T436">pro.h:S</ta>
            <ta e="T439" id="Seg_11893" s="T438">ptcl.neg</ta>
            <ta e="T440" id="Seg_11894" s="T439">v:pred</ta>
            <ta e="T442" id="Seg_11895" s="T441">pro.h:S</ta>
            <ta e="T443" id="Seg_11896" s="T442">v:pred</ta>
            <ta e="T446" id="Seg_11897" s="T445">pro.h:S</ta>
            <ta e="T447" id="Seg_11898" s="T446">ptcl.neg</ta>
            <ta e="T448" id="Seg_11899" s="T447">v:pred</ta>
            <ta e="T449" id="Seg_11900" s="T448">pro.h:S</ta>
            <ta e="T450" id="Seg_11901" s="T449">v:pred</ta>
            <ta e="T453" id="Seg_11902" s="T452">ptcl.neg</ta>
            <ta e="T454" id="Seg_11903" s="T453">v:pred 0.1.h:S</ta>
            <ta e="T455" id="Seg_11904" s="T454">ptcl:pred</ta>
            <ta e="T457" id="Seg_11905" s="T456">np:O</ta>
            <ta e="T462" id="Seg_11906" s="T461">pro.h:S</ta>
            <ta e="T463" id="Seg_11907" s="T462">v:pred</ta>
            <ta e="T466" id="Seg_11908" s="T465">np.h:O</ta>
            <ta e="T467" id="Seg_11909" s="T466">v:pred 0.3.h:S</ta>
            <ta e="T476" id="Seg_11910" s="T475">v:pred 0.3:S</ta>
            <ta e="T477" id="Seg_11911" s="T476">np:S</ta>
            <ta e="T479" id="Seg_11912" s="T478">v:pred</ta>
            <ta e="T481" id="Seg_11913" s="T480">np:S</ta>
            <ta e="T482" id="Seg_11914" s="T481">v:pred</ta>
            <ta e="T486" id="Seg_11915" s="T484">np:S</ta>
            <ta e="T488" id="Seg_11916" s="T487">np:S</ta>
            <ta e="T492" id="Seg_11917" s="T491">v:pred</ta>
            <ta e="T496" id="Seg_11918" s="T495">v:pred 0.2.h:S</ta>
            <ta e="T498" id="Seg_11919" s="T497">pro.h:O</ta>
            <ta e="T499" id="Seg_11920" s="T498">v:pred 0.2.h:S</ta>
            <ta e="T501" id="Seg_11921" s="T500">pro.h:O</ta>
            <ta e="T502" id="Seg_11922" s="T501">v:pred 0.2.h:S</ta>
            <ta e="T504" id="Seg_11923" s="T503">pro.h:O</ta>
            <ta e="T505" id="Seg_11924" s="T504">v:pred 0.2.h:S</ta>
            <ta e="T507" id="Seg_11925" s="T506">pro.h:O</ta>
            <ta e="T508" id="Seg_11926" s="T507">pro.h:S</ta>
            <ta e="T509" id="Seg_11927" s="T508">v:pred</ta>
            <ta e="T511" id="Seg_11928" s="T510">np:O</ta>
            <ta e="T513" id="Seg_11929" s="T512">pro.h:S</ta>
            <ta e="T514" id="Seg_11930" s="T513">v:pred</ta>
            <ta e="T516" id="Seg_11931" s="T515">np:O</ta>
            <ta e="T519" id="Seg_11932" s="T518">v:pred 0.2.h:S</ta>
            <ta e="T520" id="Seg_11933" s="T519">pro.h:O</ta>
            <ta e="T522" id="Seg_11934" s="T521">pro.h:O</ta>
            <ta e="T523" id="Seg_11935" s="T522">v:pred 0.2.h:S</ta>
            <ta e="T524" id="Seg_11936" s="T523">pro.h:O</ta>
            <ta e="T525" id="Seg_11937" s="T524">pro.h:S</ta>
            <ta e="T527" id="Seg_11938" s="T526">np:O</ta>
            <ta e="T530" id="Seg_11939" s="T529">pro.h:S</ta>
            <ta e="T532" id="Seg_11940" s="T531">np:O</ta>
            <ta e="T539" id="Seg_11941" s="T538">v:pred 0.2.h:S</ta>
            <ta e="T542" id="Seg_11942" s="T541">np:O</ta>
            <ta e="T543" id="Seg_11943" s="T542">v:pred 0.2.h:S</ta>
            <ta e="T548" id="Seg_11944" s="T547">np:O</ta>
            <ta e="T552" id="Seg_11945" s="T551">adj:pred</ta>
            <ta e="T555" id="Seg_11946" s="T554">v:pred 0.2.h:S</ta>
            <ta e="T557" id="Seg_11947" s="T556">pro.h:S</ta>
            <ta e="T564" id="Seg_11948" s="T563">v:pred</ta>
            <ta e="T565" id="Seg_11949" s="T564">ptcl.neg</ta>
            <ta e="T566" id="Seg_11950" s="T565">v:pred</ta>
            <ta e="T568" id="Seg_11951" s="T567">np.h:S</ta>
            <ta e="T570" id="Seg_11952" s="T569">v:pred</ta>
            <ta e="T571" id="Seg_11953" s="T570">v:pred 0.3.h:S</ta>
            <ta e="T574" id="Seg_11954" s="T573">pro:S</ta>
            <ta e="T575" id="Seg_11955" s="T574">np:O</ta>
            <ta e="T579" id="Seg_11956" s="T578">pro.h:S</ta>
            <ta e="T580" id="Seg_11957" s="T579">v:pred</ta>
            <ta e="T582" id="Seg_11958" s="T581">v:pred 0.2.h:S</ta>
            <ta e="T583" id="Seg_11959" s="T582">pro.h:S</ta>
            <ta e="T584" id="Seg_11960" s="T583">v:pred</ta>
            <ta e="T592" id="Seg_11961" s="T591">np:O</ta>
            <ta e="T593" id="Seg_11962" s="T592">v:pred 0.1.h:S</ta>
            <ta e="T595" id="Seg_11963" s="T594">np.h:S</ta>
            <ta e="T597" id="Seg_11964" s="T596">np.h:O</ta>
            <ta e="T601" id="Seg_11965" s="T600">v:pred</ta>
            <ta e="T602" id="Seg_11966" s="T601">np.h:S</ta>
            <ta e="T604" id="Seg_11967" s="T603">v:pred</ta>
            <ta e="T606" id="Seg_11968" s="T605">np.h:S</ta>
            <ta e="T607" id="Seg_11969" s="T606">v:pred</ta>
            <ta e="T608" id="Seg_11970" s="T607">np:O</ta>
            <ta e="T609" id="Seg_11971" s="T608">v:pred 0.3.h:S</ta>
            <ta e="T612" id="Seg_11972" s="T611">v:pred 0.3.h:S</ta>
            <ta e="T613" id="Seg_11973" s="T612">np:O</ta>
            <ta e="T617" id="Seg_11974" s="T616">v:pred 0.3.h:S</ta>
            <ta e="T619" id="Seg_11975" s="T618">pro.h:S</ta>
            <ta e="T621" id="Seg_11976" s="T620">np:O</ta>
            <ta e="T622" id="Seg_11977" s="T621">v:pred</ta>
            <ta e="T624" id="Seg_11978" s="T623">pro.h:S</ta>
            <ta e="T628" id="Seg_11979" s="T627">v:pred</ta>
            <ta e="T630" id="Seg_11980" s="T629">np:O</ta>
            <ta e="T631" id="Seg_11981" s="T630">v:pred 0.1.h:S</ta>
            <ta e="T633" id="Seg_11982" s="T632">np:O</ta>
            <ta e="T634" id="Seg_11983" s="T633">v:pred 0.1.h:S</ta>
            <ta e="T643" id="Seg_11984" s="T642">np:O</ta>
            <ta e="T644" id="Seg_11985" s="T643">v:pred 0.1.h:S</ta>
            <ta e="T651" id="Seg_11986" s="T650">np.h:S</ta>
            <ta e="T652" id="Seg_11987" s="T651">v:pred</ta>
            <ta e="T653" id="Seg_11988" s="T652">ptcl:pred</ta>
            <ta e="T655" id="Seg_11989" s="T654">s:purp</ta>
            <ta e="T657" id="Seg_11990" s="T656">pro.h:O</ta>
            <ta e="T659" id="Seg_11991" s="T658">v:pred 0.2.h:S</ta>
            <ta e="T660" id="Seg_11992" s="T659">v:pred 0.2.h:S</ta>
            <ta e="T662" id="Seg_11993" s="T661">pro.h:S</ta>
            <ta e="T663" id="Seg_11994" s="T662">v:pred</ta>
            <ta e="T664" id="Seg_11995" s="T663">v:pred 0.2.h:S</ta>
            <ta e="T669" id="Seg_11996" s="T668">np:S</ta>
            <ta e="T670" id="Seg_11997" s="T669">v:pred</ta>
            <ta e="T672" id="Seg_11998" s="T671">v:pred 0.3:S</ta>
            <ta e="T674" id="Seg_11999" s="T673">v:pred 0.2.h:S</ta>
            <ta e="T675" id="Seg_12000" s="T674">np:O</ta>
            <ta e="T678" id="Seg_12001" s="T677">np.h:S</ta>
            <ta e="T680" id="Seg_12002" s="T679">v:pred</ta>
            <ta e="T681" id="Seg_12003" s="T680">np:O</ta>
            <ta e="T682" id="Seg_12004" s="T681">s:purp</ta>
            <ta e="T683" id="Seg_12005" s="T682">adj:pred</ta>
            <ta e="T684" id="Seg_12006" s="T683">v:pred 0.3.h:S</ta>
            <ta e="T689" id="Seg_12007" s="T688">np.h:S</ta>
            <ta e="T690" id="Seg_12008" s="T689">v:pred</ta>
            <ta e="T692" id="Seg_12009" s="T691">np.h:S</ta>
            <ta e="T693" id="Seg_12010" s="T692">v:pred</ta>
            <ta e="T696" id="Seg_12011" s="T695">v:pred 0.3.h:S</ta>
            <ta e="T697" id="Seg_12012" s="T696">v:pred 0.1.h:S</ta>
            <ta e="T699" id="Seg_12013" s="T698">pro.h:S</ta>
            <ta e="T701" id="Seg_12014" s="T700">v:pred</ta>
            <ta e="T703" id="Seg_12015" s="T702">pro.h:S</ta>
            <ta e="T704" id="Seg_12016" s="T703">v:pred</ta>
            <ta e="T706" id="Seg_12017" s="T705">v:pred 0.2.h:S</ta>
            <ta e="T708" id="Seg_12018" s="T707">pro:O</ta>
            <ta e="T710" id="Seg_12019" s="T709">np:O</ta>
            <ta e="T712" id="Seg_12020" s="T711">v:pred 0.2.h:S</ta>
            <ta e="T713" id="Seg_12021" s="T712">np:O</ta>
            <ta e="T714" id="Seg_12022" s="T713">v:pred 0.2.h:S</ta>
            <ta e="T716" id="Seg_12023" s="T715">pro.h:S</ta>
            <ta e="T718" id="Seg_12024" s="T717">v:pred</ta>
            <ta e="T720" id="Seg_12025" s="T719">pro.h:S</ta>
            <ta e="T722" id="Seg_12026" s="T721">v:pred</ta>
            <ta e="T723" id="Seg_12027" s="T722">pro.h:S</ta>
            <ta e="T724" id="Seg_12028" s="T723">ptcl.neg</ta>
            <ta e="T725" id="Seg_12029" s="T724">v:pred</ta>
            <ta e="T733" id="Seg_12030" s="T732">np.h:S</ta>
            <ta e="T734" id="Seg_12031" s="T733">v:pred</ta>
            <ta e="T735" id="Seg_12032" s="T734">ptcl:pred</ta>
            <ta e="T737" id="Seg_12033" s="T736">np:O</ta>
            <ta e="T739" id="Seg_12034" s="T738">v:pred 0.1.h:S</ta>
            <ta e="T743" id="Seg_12035" s="T742">ptcl:pred</ta>
            <ta e="T744" id="Seg_12036" s="T743">v:pred 0.3.h:S</ta>
            <ta e="T746" id="Seg_12037" s="T745">np:O</ta>
            <ta e="T748" id="Seg_12038" s="T747">pro.h:S</ta>
            <ta e="T749" id="Seg_12039" s="T748">v:pred</ta>
            <ta e="T752" id="Seg_12040" s="T751">v:pred 0.1.h:S</ta>
            <ta e="T754" id="Seg_12041" s="T753">np:O</ta>
            <ta e="T755" id="Seg_12042" s="T754">ptcl:pred</ta>
            <ta e="T758" id="Seg_12043" s="T757">s:purp</ta>
            <ta e="T760" id="Seg_12044" s="T759">pro.h:S</ta>
            <ta e="T761" id="Seg_12045" s="T760">ptcl.neg</ta>
            <ta e="T762" id="Seg_12046" s="T761">v:pred</ta>
            <ta e="T763" id="Seg_12047" s="T762">v:pred 0.3.h:S</ta>
            <ta e="T764" id="Seg_12048" s="T763">v:pred 0.2.h:S</ta>
            <ta e="T765" id="Seg_12049" s="T764">np:O</ta>
            <ta e="T766" id="Seg_12050" s="T765">v:pred</ta>
            <ta e="T768" id="Seg_12051" s="T767">v:pred 0.2.h:S</ta>
            <ta e="T770" id="Seg_12052" s="T769">v:pred 0.2.h:S</ta>
            <ta e="T773" id="Seg_12053" s="T772">v:pred 0.1.h:S</ta>
            <ta e="T774" id="Seg_12054" s="T773">pro.h:S</ta>
            <ta e="T776" id="Seg_12055" s="T775">v:pred</ta>
            <ta e="T778" id="Seg_12056" s="T777">v:pred 0.2.h:S</ta>
            <ta e="T779" id="Seg_12057" s="T778">v:pred 0.2.h:S</ta>
            <ta e="T782" id="Seg_12058" s="T781">v:pred 0.2.h:S</ta>
            <ta e="T783" id="Seg_12059" s="T782">np:O</ta>
            <ta e="T784" id="Seg_12060" s="T783">v:pred 0.2.h:S</ta>
            <ta e="T787" id="Seg_12061" s="T786">v:pred 0.1.h:S</ta>
            <ta e="T788" id="Seg_12062" s="T787">v:pred 0.2.h:S</ta>
            <ta e="T789" id="Seg_12063" s="T788">np:O</ta>
            <ta e="T790" id="Seg_12064" s="T789">v:pred 0.2.h:S</ta>
            <ta e="T793" id="Seg_12065" s="T792">ptcl:pred</ta>
            <ta e="T794" id="Seg_12066" s="T793">np:O</ta>
            <ta e="T795" id="Seg_12067" s="T794">v:pred 0.3.h:S</ta>
            <ta e="T798" id="Seg_12068" s="T797">pro.h:S</ta>
            <ta e="T801" id="Seg_12069" s="T800">v:pred</ta>
            <ta e="T804" id="Seg_12070" s="T803">np:S</ta>
            <ta e="T805" id="Seg_12071" s="T804">v:pred</ta>
            <ta e="T807" id="Seg_12072" s="T806">np:O</ta>
            <ta e="T808" id="Seg_12073" s="T807">v:pred 0.1.h:S</ta>
            <ta e="T810" id="Seg_12074" s="T809">v:pred 0.1.h:S</ta>
            <ta e="T813" id="Seg_12075" s="T812">v:pred 0.1.h:S</ta>
            <ta e="T815" id="Seg_12076" s="T814">v:pred 0.1.h:S</ta>
            <ta e="T817" id="Seg_12077" s="T816">np:S</ta>
            <ta e="T818" id="Seg_12078" s="T817">v:pred</ta>
            <ta e="T821" id="Seg_12079" s="T820">v:pred 0.1.h:S</ta>
            <ta e="T825" id="Seg_12080" s="T824">v:pred 0.1.h:S</ta>
            <ta e="T828" id="Seg_12081" s="T827">ptcl.neg</ta>
            <ta e="T832" id="Seg_12082" s="T831">np:S</ta>
            <ta e="T834" id="Seg_12083" s="T833">adj:pred</ta>
            <ta e="T835" id="Seg_12084" s="T834">adj:pred</ta>
            <ta e="T838" id="Seg_12085" s="T837">np:S</ta>
            <ta e="T839" id="Seg_12086" s="T838">ptcl.neg</ta>
            <ta e="T840" id="Seg_12087" s="T839">adj:pred</ta>
            <ta e="T845" id="Seg_12088" s="T844">np.h:S</ta>
            <ta e="T847" id="Seg_12089" s="T846">np:O</ta>
            <ta e="T849" id="Seg_12090" s="T848">v:pred</ta>
            <ta e="T851" id="Seg_12091" s="T850">np:S</ta>
            <ta e="T852" id="Seg_12092" s="T851">v:pred</ta>
            <ta e="T853" id="Seg_12093" s="T852">ptcl:pred</ta>
            <ta e="T854" id="Seg_12094" s="T853">pro:O</ta>
            <ta e="T859" id="Seg_12095" s="T858">v:pred 0.2.h:S</ta>
            <ta e="T861" id="Seg_12096" s="T860">np:O</ta>
            <ta e="T866" id="Seg_12097" s="T865">np.h:S</ta>
            <ta e="T867" id="Seg_12098" s="T866">v:pred</ta>
            <ta e="T870" id="Seg_12099" s="T869">ptcl.neg</ta>
            <ta e="T871" id="Seg_12100" s="T870">v:pred 0.3.h:S</ta>
            <ta e="T872" id="Seg_12101" s="T871">v:pred 0.3.h:S</ta>
            <ta e="T874" id="Seg_12102" s="T873">v:pred 0.3.h:S</ta>
            <ta e="T876" id="Seg_12103" s="T875">v:pred 0.3.h:S</ta>
            <ta e="T877" id="Seg_12104" s="T876">ptcl.neg</ta>
            <ta e="T878" id="Seg_12105" s="T877">adj:pred</ta>
            <ta e="T880" id="Seg_12106" s="T879">v:pred 0.3.h:S</ta>
            <ta e="T882" id="Seg_12107" s="T881">v:pred 0.3.h:S</ta>
            <ta e="T883" id="Seg_12108" s="T882">ptcl.neg</ta>
            <ta e="T884" id="Seg_12109" s="T883">adj:pred</ta>
            <ta e="T885" id="Seg_12110" s="T884">conv:pred</ta>
            <ta e="T888" id="Seg_12111" s="T887">pro.h:S</ta>
            <ta e="T890" id="Seg_12112" s="T889">np.h:O</ta>
            <ta e="T891" id="Seg_12113" s="T890">v:pred</ta>
            <ta e="T892" id="Seg_12114" s="T891">pro.h:O</ta>
            <ta e="T893" id="Seg_12115" s="T892">v:pred 0.3.h:S</ta>
            <ta e="T897" id="Seg_12116" s="T896">v:pred 0.3.h:S</ta>
            <ta e="T901" id="Seg_12117" s="T900">v:pred 0.3.h:S</ta>
            <ta e="T903" id="Seg_12118" s="T902">v:pred 0.3.h:S</ta>
            <ta e="T906" id="Seg_12119" s="T905">np:O</ta>
            <ta e="T910" id="Seg_12120" s="T909">v:pred 0.3.h:S</ta>
            <ta e="T916" id="Seg_12121" s="T915">np:O</ta>
            <ta e="T919" id="Seg_12122" s="T918">v:pred 0.3.h:S</ta>
            <ta e="T922" id="Seg_12123" s="T921">np:S</ta>
            <ta e="T923" id="Seg_12124" s="T922">v:pred</ta>
            <ta e="T932" id="Seg_12125" s="T931">v:pred 0.3:S</ta>
            <ta e="T935" id="Seg_12126" s="T934">v:pred 0.2.h:S</ta>
            <ta e="T936" id="Seg_12127" s="T935">np:O</ta>
            <ta e="T937" id="Seg_12128" s="T936">s:purp</ta>
            <ta e="T938" id="Seg_12129" s="T937">v:pred 0.1.h:S</ta>
            <ta e="T940" id="Seg_12130" s="T939">np:S</ta>
            <ta e="T941" id="Seg_12131" s="T940">v:pred</ta>
            <ta e="T946" id="Seg_12132" s="T945">v:pred 0.3.h:S</ta>
            <ta e="T948" id="Seg_12133" s="T947">adj:pred</ta>
            <ta e="T949" id="Seg_12134" s="T948">np:S</ta>
            <ta e="T952" id="Seg_12135" s="T951">v:pred 0.2.h:S</ta>
            <ta e="T957" id="Seg_12136" s="T956">v:pred</ta>
            <ta e="T959" id="Seg_12137" s="T958">np:S</ta>
            <ta e="T960" id="Seg_12138" s="T959">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T14" id="Seg_12139" s="T13">RUS:cult</ta>
            <ta e="T31" id="Seg_12140" s="T30">RUS:gram</ta>
            <ta e="T34" id="Seg_12141" s="T33">%TURK:core</ta>
            <ta e="T38" id="Seg_12142" s="T37">RUS:gram</ta>
            <ta e="T42" id="Seg_12143" s="T41">TURK:disc</ta>
            <ta e="T47" id="Seg_12144" s="T46">TURK:disc</ta>
            <ta e="T57" id="Seg_12145" s="T56">TURK:disc</ta>
            <ta e="T58" id="Seg_12146" s="T57">TURK:gram(INDEF)</ta>
            <ta e="T62" id="Seg_12147" s="T61">TURK:disc</ta>
            <ta e="T66" id="Seg_12148" s="T65">RUS:gram</ta>
            <ta e="T67" id="Seg_12149" s="T66">TURK:cult</ta>
            <ta e="T69" id="Seg_12150" s="T68">RUS:gram</ta>
            <ta e="T72" id="Seg_12151" s="T71">RUS:cult</ta>
            <ta e="T77" id="Seg_12152" s="T76">TURK:disc</ta>
            <ta e="T78" id="Seg_12153" s="T77">TURK:cult</ta>
            <ta e="T79" id="Seg_12154" s="T78">TURK:disc</ta>
            <ta e="T83" id="Seg_12155" s="T82">TURK:disc</ta>
            <ta e="T85" id="Seg_12156" s="T84">TURK:cult</ta>
            <ta e="T100" id="Seg_12157" s="T99">RUS:gram</ta>
            <ta e="T102" id="Seg_12158" s="T101">TURK:disc</ta>
            <ta e="T106" id="Seg_12159" s="T105">TURK:disc</ta>
            <ta e="T107" id="Seg_12160" s="T106">RUS:cult</ta>
            <ta e="T112" id="Seg_12161" s="T111">TURK:disc</ta>
            <ta e="T115" id="Seg_12162" s="T114">RUS:gram</ta>
            <ta e="T119" id="Seg_12163" s="T118">RUS:gram</ta>
            <ta e="T122" id="Seg_12164" s="T121">TURK:disc</ta>
            <ta e="T124" id="Seg_12165" s="T123">RUS:core</ta>
            <ta e="T125" id="Seg_12166" s="T124">RUS:cult</ta>
            <ta e="T139" id="Seg_12167" s="T138">TURK:disc</ta>
            <ta e="T141" id="Seg_12168" s="T140">RUS:gram</ta>
            <ta e="T145" id="Seg_12169" s="T144">TURK:disc</ta>
            <ta e="T151" id="Seg_12170" s="T150">TURK:disc</ta>
            <ta e="T157" id="Seg_12171" s="T156">TURK:disc</ta>
            <ta e="T159" id="Seg_12172" s="T158">RUS:gram</ta>
            <ta e="T166" id="Seg_12173" s="T165">RUS:gram</ta>
            <ta e="T171" id="Seg_12174" s="T170">TURK:disc</ta>
            <ta e="T188" id="Seg_12175" s="T187">TURK:disc</ta>
            <ta e="T193" id="Seg_12176" s="T192">RUS:gram</ta>
            <ta e="T217" id="Seg_12177" s="T215">RUS:gram</ta>
            <ta e="T219" id="Seg_12178" s="T218">TURK:disc</ta>
            <ta e="T233" id="Seg_12179" s="T232">TURK:disc</ta>
            <ta e="T244" id="Seg_12180" s="T243">RUS:gram</ta>
            <ta e="T248" id="Seg_12181" s="T247">TURK:disc</ta>
            <ta e="T255" id="Seg_12182" s="T254">TURK:disc</ta>
            <ta e="T292" id="Seg_12183" s="T291">RUS:gram</ta>
            <ta e="T301" id="Seg_12184" s="T300">TURK:disc</ta>
            <ta e="T329" id="Seg_12185" s="T328">RUS:core</ta>
            <ta e="T330" id="Seg_12186" s="T329">TURK:disc</ta>
            <ta e="T342" id="Seg_12187" s="T341">RUS:gram</ta>
            <ta e="T353" id="Seg_12188" s="T352">RUS:gram</ta>
            <ta e="T367" id="Seg_12189" s="T366">TURK:core</ta>
            <ta e="T371" id="Seg_12190" s="T370">TURK:disc</ta>
            <ta e="T376" id="Seg_12191" s="T375">TURK:disc</ta>
            <ta e="T380" id="Seg_12192" s="T379">%TURK:core</ta>
            <ta e="T389" id="Seg_12193" s="T388">RUS:gram</ta>
            <ta e="T406" id="Seg_12194" s="T405">RUS:gram</ta>
            <ta e="T412" id="Seg_12195" s="T411">TURK:disc</ta>
            <ta e="T418" id="Seg_12196" s="T417">TURK:disc</ta>
            <ta e="T426" id="Seg_12197" s="T425">RUS:gram</ta>
            <ta e="T431" id="Seg_12198" s="T430">TURK:disc</ta>
            <ta e="T434" id="Seg_12199" s="T433">RUS:mod</ta>
            <ta e="T438" id="Seg_12200" s="T437">TURK:disc</ta>
            <ta e="T440" id="Seg_12201" s="T439">%TURK:core</ta>
            <ta e="T441" id="Seg_12202" s="T440">RUS:gram</ta>
            <ta e="T443" id="Seg_12203" s="T442">%TURK:core</ta>
            <ta e="T445" id="Seg_12204" s="T444">RUS:gram</ta>
            <ta e="T451" id="Seg_12205" s="T450">RUS:gram</ta>
            <ta e="T452" id="Seg_12206" s="T451">%TURK:core</ta>
            <ta e="T455" id="Seg_12207" s="T454">RUS:mod</ta>
            <ta e="T459" id="Seg_12208" s="T458">RUS:gram</ta>
            <ta e="T465" id="Seg_12209" s="T464">RUS:mod</ta>
            <ta e="T473" id="Seg_12210" s="T472">TURK:disc</ta>
            <ta e="T475" id="Seg_12211" s="T474">TURK:disc</ta>
            <ta e="T478" id="Seg_12212" s="T477">TURK:disc</ta>
            <ta e="T480" id="Seg_12213" s="T479">RUS:gram</ta>
            <ta e="T484" id="Seg_12214" s="T482">TURK:disc</ta>
            <ta e="T487" id="Seg_12215" s="T486">RUS:gram</ta>
            <ta e="T489" id="Seg_12216" s="T488">TURK:disc</ta>
            <ta e="T491" id="Seg_12217" s="T490">TURK:disc</ta>
            <ta e="T540" id="Seg_12218" s="T539">RUS:cult</ta>
            <ta e="T554" id="Seg_12219" s="T553">TURK:core</ta>
            <ta e="T559" id="Seg_12220" s="T558">RUS:cult</ta>
            <ta e="T576" id="Seg_12221" s="T575">TURK:disc</ta>
            <ta e="T578" id="Seg_12222" s="T577">RUS:gram</ta>
            <ta e="T595" id="Seg_12223" s="T594">RUS:cult</ta>
            <ta e="T600" id="Seg_12224" s="T599">TURK:disc</ta>
            <ta e="T605" id="Seg_12225" s="T604">TURK:disc</ta>
            <ta e="T610" id="Seg_12226" s="T609">RUS:gram</ta>
            <ta e="T618" id="Seg_12227" s="T617">RUS:gram</ta>
            <ta e="T626" id="Seg_12228" s="T625">RUS:cult</ta>
            <ta e="T630" id="Seg_12229" s="T629">TURK:cult</ta>
            <ta e="T635" id="Seg_12230" s="T634">RUS:mod</ta>
            <ta e="T645" id="Seg_12231" s="T644">RUS:gram</ta>
            <ta e="T647" id="Seg_12232" s="T646">RUS:mod</ta>
            <ta e="T651" id="Seg_12233" s="T650">TURK:core</ta>
            <ta e="T653" id="Seg_12234" s="T652">RUS:mod</ta>
            <ta e="T659" id="Seg_12235" s="T658">%TURK:core</ta>
            <ta e="T667" id="Seg_12236" s="T666">TURK:disc</ta>
            <ta e="T669" id="Seg_12237" s="T668">RUS:cult</ta>
            <ta e="T671" id="Seg_12238" s="T670">TURK:disc</ta>
            <ta e="T681" id="Seg_12239" s="T680">TURK:cult</ta>
            <ta e="T686" id="Seg_12240" s="T685">RUS:gram</ta>
            <ta e="T688" id="Seg_12241" s="T687">RUS:cult</ta>
            <ta e="T702" id="Seg_12242" s="T701">RUS:gram</ta>
            <ta e="T708" id="Seg_12243" s="T707">RUS:gram(INDEF)</ta>
            <ta e="T710" id="Seg_12244" s="T709">RUS:cult</ta>
            <ta e="T717" id="Seg_12245" s="T716">TURK:disc</ta>
            <ta e="T719" id="Seg_12246" s="T718">RUS:gram</ta>
            <ta e="T722" id="Seg_12247" s="T721">%TURK:core</ta>
            <ta e="T726" id="Seg_12248" s="T725">%TURK:core</ta>
            <ta e="T735" id="Seg_12249" s="T734">RUS:mod</ta>
            <ta e="T743" id="Seg_12250" s="T742">RUS:mod</ta>
            <ta e="T751" id="Seg_12251" s="T750">RUS:mod</ta>
            <ta e="T753" id="Seg_12252" s="T752">RUS:cult</ta>
            <ta e="T755" id="Seg_12253" s="T754">RUS:mod</ta>
            <ta e="T757" id="Seg_12254" s="T756">RUS:gram</ta>
            <ta e="T769" id="Seg_12255" s="T768">RUS:gram</ta>
            <ta e="T775" id="Seg_12256" s="T774">TURK:disc</ta>
            <ta e="T781" id="Seg_12257" s="T780">TURK:core</ta>
            <ta e="T783" id="Seg_12258" s="T782">TAT:cult</ta>
            <ta e="T789" id="Seg_12259" s="T788">RUS:cult</ta>
            <ta e="T791" id="Seg_12260" s="T790">RUS:gram</ta>
            <ta e="T793" id="Seg_12261" s="T792">RUS:gram</ta>
            <ta e="T802" id="Seg_12262" s="T801">RUS:mod</ta>
            <ta e="T807" id="Seg_12263" s="T806">RUS:cult</ta>
            <ta e="T814" id="Seg_12264" s="T813">RUS:mod</ta>
            <ta e="T816" id="Seg_12265" s="T815">RUS:mod</ta>
            <ta e="T826" id="Seg_12266" s="T825">RUS:gram</ta>
            <ta e="T836" id="Seg_12267" s="T835">RUS:gram</ta>
            <ta e="T846" id="Seg_12268" s="T845">TURK:disc</ta>
            <ta e="T848" id="Seg_12269" s="T847">TURK:disc</ta>
            <ta e="T850" id="Seg_12270" s="T849">TURK:disc</ta>
            <ta e="T853" id="Seg_12271" s="T852">RUS:mod</ta>
            <ta e="T863" id="Seg_12272" s="T862">RUS:cult</ta>
            <ta e="T868" id="Seg_12273" s="T867">RUS:gram</ta>
            <ta e="T875" id="Seg_12274" s="T874">RUS:gram</ta>
            <ta e="T878" id="Seg_12275" s="T877">TURK:core</ta>
            <ta e="T881" id="Seg_12276" s="T880">RUS:gram</ta>
            <ta e="T885" id="Seg_12277" s="T884">%TURK:core</ta>
            <ta e="T889" id="Seg_12278" s="T888">TURK:disc</ta>
            <ta e="T898" id="Seg_12279" s="T897">RUS:gram</ta>
            <ta e="T902" id="Seg_12280" s="T901">RUS:gram</ta>
            <ta e="T905" id="Seg_12281" s="T904">TURK:disc</ta>
            <ta e="T917" id="Seg_12282" s="T916">TURK:disc</ta>
            <ta e="T920" id="Seg_12283" s="T919">TURK:disc</ta>
            <ta e="T921" id="Seg_12284" s="T920">TURK:disc</ta>
            <ta e="T927" id="Seg_12285" s="T926">TURK:disc</ta>
            <ta e="T942" id="Seg_12286" s="T941">RUS:mod</ta>
            <ta e="T945" id="Seg_12287" s="T944">TURK:gram(INDEF)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T400" id="Seg_12288" s="T397">RUS:ext</ta>
            <ta e="T536" id="Seg_12289" s="T533">RUS:ext</ta>
            <ta e="T730" id="Seg_12290" s="T726">RUS:int</ta>
            <ta e="T856" id="Seg_12291" s="T855">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T8" id="Seg_12292" s="T4">Я сегодня ходила туда.</ta>
            <ta e="T11" id="Seg_12293" s="T8">Там женщина живет.</ta>
            <ta e="T14" id="Seg_12294" s="T11">"Ешь, ешь капусту!"</ta>
            <ta e="T17" id="Seg_12295" s="T14">"Ой, она очень кислая.</ta>
            <ta e="T19" id="Seg_12296" s="T17">Не буду есть".</ta>
            <ta e="T25" id="Seg_12297" s="T19">Девушка пошла в туалет, а собака с ней.</ta>
            <ta e="T27" id="Seg_12298" s="T25">Я…</ta>
            <ta e="T34" id="Seg_12299" s="T27">Она пришла, а я ей говорю:</ta>
            <ta e="T40" id="Seg_12300" s="T34">"Ты пошла в туалет, а собака с тобой".</ta>
            <ta e="T45" id="Seg_12301" s="T40">Она так расхохоталась.</ta>
            <ta e="T51" id="Seg_12302" s="T45">Натирай ноги, болят сильно.</ta>
            <ta e="T60" id="Seg_12303" s="T52">Много лет прошло, люди ничего не знают.</ta>
            <ta e="T68" id="Seg_12304" s="T60">У них много лошадей и коров много.</ta>
            <ta e="T77" id="Seg_12305" s="T68">И овец много, кур много, мяса много едят.</ta>
            <ta e="T80" id="Seg_12306" s="T77">Молока много.</ta>
            <ta e="T84" id="Seg_12307" s="T81">Камасинцы жили.</ta>
            <ta e="T95" id="Seg_12308" s="T84">Они делали идолов из дерева, делали из камня [= глины], делали бумажных [= рисовали на бумаге].</ta>
            <ta e="T102" id="Seg_12309" s="T96">Делали идолов из железа и ставили их.</ta>
            <ta e="T109" id="Seg_12310" s="T103">Им ставили свечки, зажигали.</ta>
            <ta e="T114" id="Seg_12311" s="T110">Камасинцы ходили в лес.</ta>
            <ta e="T122" id="Seg_12312" s="T114">Когда было холодно, они приходили сюда.</ta>
            <ta e="T128" id="Seg_12313" s="T122">Они жили здесь в старом стойбище, потом увидели:</ta>
            <ta e="T132" id="Seg_12314" s="T128">Здесь вода не замерзает.</ta>
            <ta e="T143" id="Seg_12315" s="T132">Тогда они поставили здесь юрты и жили здесь.</ta>
            <ta e="T146" id="Seg_12316" s="T143">Они ходили.</ta>
            <ta e="T153" id="Seg_12317" s="T146">Дорога была узкая.</ta>
            <ta e="T160" id="Seg_12318" s="T153">Один идет, вот так, они идут, как утки.</ta>
            <ta e="T162" id="Seg_12319" s="T160">Они ходили в лес.</ta>
            <ta e="T168" id="Seg_12320" s="T162">Один не пойдет, а много пойдут.</ta>
            <ta e="T173" id="Seg_12321" s="T169">Они из леса приходили.</ta>
            <ta e="T179" id="Seg_12322" s="T173">Один, два, три, четыре, пять, шесть.</ta>
            <ta e="T186" id="Seg_12323" s="T179">Один за другим едут.</ta>
            <ta e="T189" id="Seg_12324" s="T186">Вода замерзла.</ta>
            <ta e="T196" id="Seg_12325" s="T189">Ногами встанешь, так туда не провалишься.</ta>
            <ta e="T202" id="Seg_12326" s="T196">Я был маленькая, моя мать работала.</ta>
            <ta e="T207" id="Seg_12327" s="T202">Шкуры делала, шубы шила.</ta>
            <ta e="T217" id="Seg_12328" s="T207">Сапоги сшила, шапку сшила, и…</ta>
            <ta e="T220" id="Seg_12329" s="T217">И отец надел [их].</ta>
            <ta e="T228" id="Seg_12330" s="T220">Сейчас у меня силы нет, не знаю, куда она ушла.</ta>
            <ta e="T235" id="Seg_12331" s="T229">Я очень испугалась, меня трясет.</ta>
            <ta e="T243" id="Seg_12332" s="T236">Сегодня вы [= мы] в тайге ночевали, было очень холодно.</ta>
            <ta e="T250" id="Seg_12333" s="T243">И мы очень замерзли, совсем замерзли.</ta>
            <ta e="T256" id="Seg_12334" s="T251">Очень холодно, вода замерзла.</ta>
            <ta e="T260" id="Seg_12335" s="T256">Я ногами стояла.</ta>
            <ta e="T263" id="Seg_12336" s="T260">[Лёд?] не сломался.</ta>
            <ta e="T268" id="Seg_12337" s="T264">Когда пойдешь спать, спроси:</ta>
            <ta e="T272" id="Seg_12338" s="T268">"Ты разрешаешь мне здесь спать?"</ta>
            <ta e="T278" id="Seg_12339" s="T272">Сперва они там стояли, на берегу Ильбина.</ta>
            <ta e="T282" id="Seg_12340" s="T278">Потом нашли эту реку.</ta>
            <ta e="T291" id="Seg_12341" s="T284">Она не замерзает, они поставили здесь юрты.</ta>
            <ta e="T298" id="Seg_12342" s="T291">И сюда приходили жить, когда становилось холодно.</ta>
            <ta e="T304" id="Seg_12343" s="T299">Они пришли, жили на Ильбине.</ta>
            <ta e="T312" id="Seg_12344" s="T304">Они пришли сюда, нашли реку, которая не замерзает.</ta>
            <ta e="T315" id="Seg_12345" s="T312">Они поставили здесь юрты.</ta>
            <ta e="T322" id="Seg_12346" s="T315">Сюда приезжают жить, когда станет холодно.</ta>
            <ta e="T329" id="Seg_12347" s="T322">Они не оставались [здесь], они ездили по тайге, везде.</ta>
            <ta e="T332" id="Seg_12348" s="T329">По всей земле ездили.</ta>
            <ta e="T337" id="Seg_12349" s="T332">На Белогорье поехали, там воды много.</ta>
            <ta e="T345" id="Seg_12350" s="T338">Один человек тут, а один там.</ta>
            <ta e="T349" id="Seg_12351" s="T346">Иди сюда, туда!</ta>
            <ta e="T356" id="Seg_12352" s="T349">Я туда не пойду и сюда не пойду.</ta>
            <ta e="T360" id="Seg_12353" s="T357">Как это будет?</ta>
            <ta e="T363" id="Seg_12354" s="T360">Не пойдешь, сын.</ta>
            <ta e="T369" id="Seg_12355" s="T363">Этот человек очень хорошо живет.</ta>
            <ta e="T373" id="Seg_12356" s="T369">У него все есть.</ta>
            <ta e="T380" id="Seg_12357" s="T374">Пришлые люди приехали на моем языке говорить.</ta>
            <ta e="T386" id="Seg_12358" s="T381">Они мой язык учат.</ta>
            <ta e="T392" id="Seg_12359" s="T387">Спроси, а я скажу тебе.</ta>
            <ta e="T396" id="Seg_12360" s="T392">Что ты оттуда взял?</ta>
            <ta e="T400" id="Seg_12361" s="T397">На кого говорить? </ta>
            <ta e="T405" id="Seg_12362" s="T400">У тебя очень длинный язык.</ta>
            <ta e="T409" id="Seg_12363" s="T405">А у меня маленький язык.</ta>
            <ta e="T416" id="Seg_12364" s="T410">Мы вышли на дорогу.</ta>
            <ta e="T420" id="Seg_12365" s="T416">Человек идет к нам.</ta>
            <ta e="T428" id="Seg_12366" s="T420">Спроси дорогу, куда он идет, а то мы…</ta>
            <ta e="T435" id="Seg_12367" s="T429">Мы, может, не туда пошли.</ta>
            <ta e="T444" id="Seg_12368" s="T436">"Ты не разговариваешь, а я разговариваю с тобой".</ta>
            <ta e="T448" id="Seg_12369" s="T444">"А я не знаю.</ta>
            <ta e="T454" id="Seg_12370" s="T448">Я знаю, но говорить не могу".</ta>
            <ta e="T460" id="Seg_12371" s="T454">"Надо твой язык оторвать и выбросить".</ta>
            <ta e="T467" id="Seg_12372" s="T461">Они научились, теперь тоже будут камасинцы.</ta>
            <ta e="T470" id="Seg_12373" s="T468">Очень…</ta>
            <ta e="T473" id="Seg_12374" s="T470">Сильный ветер.</ta>
            <ta e="T476" id="Seg_12375" s="T473">Сильно дует.</ta>
            <ta e="T484" id="Seg_12376" s="T476">Снег замерзает, и земля замерзает.</ta>
            <ta e="T492" id="Seg_12377" s="T484">Снег и земля смешались.</ta>
            <ta e="T501" id="Seg_12378" s="T492">Ты, бог, бог, не оставляй меня.</ta>
            <ta e="T507" id="Seg_12379" s="T501">Не оставляй меня, не бросай меня.</ta>
            <ta e="T518" id="Seg_12380" s="T507">Ты возьми у меня сердце глупое, ты дай мне сердце мудрое.</ta>
            <ta e="T522" id="Seg_12381" s="T518">Научи меня восхвалять тебя.</ta>
            <ta e="T528" id="Seg_12382" s="T522">Научи меня по дороге твоей идти.</ta>
            <ta e="T533" id="Seg_12383" s="T529">По (дорогам?) твоим идти.</ta>
            <ta e="T536" id="Seg_12384" s="T533">Погоди… </ta>
            <ta e="T541" id="Seg_12385" s="T537">Ваня, к крестной иди.</ta>
            <ta e="T544" id="Seg_12386" s="T541">Попроси у нее ножницы.</ta>
            <ta e="T552" id="Seg_12387" s="T544">Овечьи ножницы, шерсть им стричь нам надо.</ta>
            <ta e="T555" id="Seg_12388" s="T553">Хорошо проси.</ta>
            <ta e="T564" id="Seg_12389" s="T556">Я вчера на печку лазила.</ta>
            <ta e="T566" id="Seg_12390" s="T564">Не есть (?).</ta>
            <ta e="T570" id="Seg_12391" s="T566">Ванька с отцом приехали.</ta>
            <ta e="T571" id="Seg_12392" s="T570">Говорит:</ta>
            <ta e="T577" id="Seg_12393" s="T571">"Бабушка, посмотри, какая большая рыба".</ta>
            <ta e="T582" id="Seg_12394" s="T577">Я спросила: "Сколько наловили?"</ta>
            <ta e="T593" id="Seg_12395" s="T582">Он говорит: "Семь поймали".</ta>
            <ta e="T601" id="Seg_12396" s="T594">Фашисты все с нашими людьми воевали.</ta>
            <ta e="T605" id="Seg_12397" s="T601">А мы двое дома сидели.</ta>
            <ta e="T612" id="Seg_12398" s="T605">Гришка пошел, рыбу поймал и домой принес.</ta>
            <ta e="T617" id="Seg_12399" s="T612">Уток трех принесет.</ta>
            <ta e="T622" id="Seg_12400" s="T617">А я овечек доила.</ta>
            <ta e="T631" id="Seg_12401" s="T623">Я много в колхозе работала, много ржи сжинала.</ta>
            <ta e="T636" id="Seg_12402" s="T631">Двадцать сжинала, и еще две.</ta>
            <ta e="T647" id="Seg_12403" s="T636">Потом в один день я тридцать сжала и ещё четыре.</ta>
            <ta e="T649" id="Seg_12404" s="T648">Сотки.</ta>
            <ta e="T652" id="Seg_12405" s="T649">Мой родственник спрашивает:</ta>
            <ta e="T659" id="Seg_12406" s="T652">"Надо мне пойти послушать вас, как вы разговариваете".</ta>
            <ta e="T663" id="Seg_12407" s="T659">"Приходи завтра, они придут.</ta>
            <ta e="T664" id="Seg_12408" s="T663">Послушай".</ta>
            <ta e="T670" id="Seg_12409" s="T665">У меня цыплята вылезают.</ta>
            <ta e="T672" id="Seg_12410" s="T670">Пищат.</ta>
            <ta e="T675" id="Seg_12411" s="T673">Протяни руку.</ta>
            <ta e="T682" id="Seg_12412" s="T676">Вчера женщины ко мне пришли молоко пропускать.</ta>
            <ta e="T685" id="Seg_12413" s="T682">Их было много.</ta>
            <ta e="T691" id="Seg_12414" s="T685">А жена председателя сидела здесь.</ta>
            <ta e="T693" id="Seg_12415" s="T691">Сын пришел.</ta>
            <ta e="T701" id="Seg_12416" s="T693">Говорит ей: "Пойдем домой, мы в Красноярск поедем.</ta>
            <ta e="T705" id="Seg_12417" s="T701">А я сказала ему:</ta>
            <ta e="T712" id="Seg_12418" s="T705">"Купите матери что-нибудь красивое, платок купите.</ta>
            <ta e="T715" id="Seg_12419" s="T712">Одежду купи красивую".</ta>
            <ta e="T722" id="Seg_12420" s="T715">Они смеются, что я так говорю.</ta>
            <ta e="T730" id="Seg_12421" s="T722">Они не умеют говорить [по-камасински].</ta>
            <ta e="T734" id="Seg_12422" s="T731">Вчера женщина приходила.</ta>
            <ta e="T746" id="Seg_12423" s="T734">"Мне надо картошку (сажать?), пойду к своему сыну, пусть он мне посадит картошку".</ta>
            <ta e="T750" id="Seg_12424" s="T747">Я была в Заозерке.</ta>
            <ta e="T753" id="Seg_12425" s="T750">Ходила в аптеку.</ta>
            <ta e="T756" id="Seg_12426" s="T753">Хотела купить очки.</ta>
            <ta e="T759" id="Seg_12427" s="T756">Чтобы смотреть вдаль.</ta>
            <ta e="T764" id="Seg_12428" s="T759">Они не дали, говорят: "Иди.</ta>
            <ta e="T770" id="Seg_12429" s="T764">Пусть глаза посмотрят [врачи], потом придешь и купишь".</ta>
            <ta e="T778" id="Seg_12430" s="T771">Я туда пришла, они дерутся, "Что вы деретесь?</ta>
            <ta e="T780" id="Seg_12431" s="T778">Не кричите.</ta>
            <ta e="T782" id="Seg_12432" s="T780">Хорошо себя ведите.</ta>
            <ta e="T784" id="Seg_12433" s="T782">Подмети в доме".</ta>
            <ta e="T788" id="Seg_12434" s="T785">Я сказала одному: "Иди!</ta>
            <ta e="T790" id="Seg_12435" s="T788">Лен молоти.</ta>
            <ta e="T796" id="Seg_12436" s="T790">А он пусть воду натаскает в баню".</ta>
            <ta e="T805" id="Seg_12437" s="T797">Я сегодня рано встала, еще пяти не было.</ta>
            <ta e="T808" id="Seg_12438" s="T805">Печь затопила.</ta>
            <ta e="T810" id="Seg_12439" s="T808">Воду(?) поставила.</ta>
            <ta e="T815" id="Seg_12440" s="T810">Потом опять легла и еще поспала.</ta>
            <ta e="T821" id="Seg_12441" s="T815">Десять часов стало, тогда встала.</ta>
            <ta e="T829" id="Seg_12442" s="T822">Сегодня я тяжело работала, а вчера не тяжело.</ta>
            <ta e="T835" id="Seg_12443" s="T830">Этот камень очень большой, тяжелый.</ta>
            <ta e="T840" id="Seg_12444" s="T835">А эта палка не тяжелая.</ta>
            <ta e="T852" id="Seg_12445" s="T841">Этот парень штаны порвал, дырка появилась.</ta>
            <ta e="T855" id="Seg_12446" s="T852">Надо их зашить.</ta>
            <ta e="T857" id="Seg_12447" s="T855">Заплатку поставить.</ta>
            <ta e="T863" id="Seg_12448" s="T858">Взвесь мне сахару пять килограммов.</ta>
            <ta e="T876" id="Seg_12449" s="T864">Этот человек болел, а теперь не болеет, встал, ногами ходит, а то лежал.</ta>
            <ta e="T879" id="Seg_12450" s="T876">Нехорошо (подслушивать?).</ta>
            <ta e="T884" id="Seg_12451" s="T879">Пришли и сказали, некрасиво.</ta>
            <ta e="T885" id="Seg_12452" s="T884">Говоря(?).</ta>
            <ta e="T891" id="Seg_12453" s="T886">Он человека убил.</ta>
            <ta e="T893" id="Seg_12454" s="T891">Его посадили.</ta>
            <ta e="T903" id="Seg_12455" s="T893">Три года сидел, теперь домой пришел, ходит.</ta>
            <ta e="T910" id="Seg_12456" s="T904">Все чашки разбили.</ta>
            <ta e="T920" id="Seg_12457" s="T912">Меня камнем по голове ударили.</ta>
            <ta e="T923" id="Seg_12458" s="T920">Кровь течет.</ta>
            <ta e="T930" id="Seg_12459" s="T923">Мне топором голову…</ta>
            <ta e="T932" id="Seg_12460" s="T931">…разбили.</ta>
            <ta e="T937" id="Seg_12461" s="T932">Ты пойдешь в тайгу ягоды собирать.</ta>
            <ta e="T941" id="Seg_12462" s="T937">Пойдем домой, ягод нет.</ta>
            <ta e="T944" id="Seg_12463" s="T941">Только идти (?)</ta>
            <ta e="T949" id="Seg_12464" s="T944">Куда они ни пойдут, ягод очень много.</ta>
            <ta e="T952" id="Seg_12465" s="T949">Потом опять собираешь.</ta>
            <ta e="T960" id="Seg_12466" s="T952">Когда (стану?) домой идти, очень много ягод.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T8" id="Seg_12467" s="T4">I went there today.</ta>
            <ta e="T11" id="Seg_12468" s="T8">A woman lives there.</ta>
            <ta e="T14" id="Seg_12469" s="T11">"Eat, eat cabbage!"</ta>
            <ta e="T17" id="Seg_12470" s="T14">"Oh, it's very sour!</ta>
            <ta e="T19" id="Seg_12471" s="T17">I won't eat it."</ta>
            <ta e="T25" id="Seg_12472" s="T19">Then the daughter left to shit and a dog (went) with her.</ta>
            <ta e="T27" id="Seg_12473" s="T25">I…</ta>
            <ta e="T34" id="Seg_12474" s="T27">She came and I say her:</ta>
            <ta e="T40" id="Seg_12475" s="T34">"You went to shit, a dog (went) with you."</ta>
            <ta e="T45" id="Seg_12476" s="T40">She started to laugh very hard.</ta>
            <ta e="T51" id="Seg_12477" s="T45">Rub [my?] legs, they are aching very much.</ta>
            <ta e="T60" id="Seg_12478" s="T52">Many years went by, people do not know anything.</ta>
            <ta e="T68" id="Seg_12479" s="T60">They have many horses and many cows.</ta>
            <ta e="T77" id="Seg_12480" s="T68">And many sheep and many chickens, they eat a lot of meat.</ta>
            <ta e="T80" id="Seg_12481" s="T77">[They] have a lot of milk.</ta>
            <ta e="T84" id="Seg_12482" s="T81">The Kamassians used to live.</ta>
            <ta e="T95" id="Seg_12483" s="T84">They made idols out of wood, then out of stone [= clay], then out of paper [= drew them on the paper].</ta>
            <ta e="T102" id="Seg_12484" s="T96">They made god[s] from iron and erected [them].</ta>
            <ta e="T109" id="Seg_12485" s="T103">For them they put candles, lit (them).</ta>
            <ta e="T114" id="Seg_12486" s="T110">Kamassians used to wander in the forest.</ta>
            <ta e="T122" id="Seg_12487" s="T114">When it becomes cold, they used to come here.</ta>
            <ta e="T128" id="Seg_12488" s="T122">They lived there in the old settlement, then (they) saw:</ta>
            <ta e="T132" id="Seg_12489" s="T128">The water does not freeze here.</ta>
            <ta e="T143" id="Seg_12490" s="T132">Then they built tent[s] here and lived here.</ta>
            <ta e="T146" id="Seg_12491" s="T143">They walked.</ta>
            <ta e="T153" id="Seg_12492" s="T146">The road was narrow.</ta>
            <ta e="T160" id="Seg_12493" s="T153">One is going, like this, they walked like ducks.</ta>
            <ta e="T162" id="Seg_12494" s="T160">They went to the forest.</ta>
            <ta e="T168" id="Seg_12495" s="T162">One person doesn't go, many go.</ta>
            <ta e="T173" id="Seg_12496" s="T169">They come from the taiga.</ta>
            <ta e="T179" id="Seg_12497" s="T173">One, two, four, three, five, six.</ta>
            <ta e="T186" id="Seg_12498" s="T179">They come one after another.</ta>
            <ta e="T189" id="Seg_12499" s="T186">The water froze.</ta>
            <ta e="T196" id="Seg_12500" s="T189">If you stand with your feet, you do not fall in there.</ta>
            <ta e="T202" id="Seg_12501" s="T196">I was small, my mother worked.</ta>
            <ta e="T207" id="Seg_12502" s="T202">She made skins and sewed fur coats.</ta>
            <ta e="T217" id="Seg_12503" s="T207">She sewed boots, sewed a cap and…</ta>
            <ta e="T220" id="Seg_12504" s="T217">My father wore [them].</ta>
            <ta e="T228" id="Seg_12505" s="T220">Now my strength is no more, I do not know where it went.</ta>
            <ta e="T235" id="Seg_12506" s="T229">I am very afraid, I am trembling.</ta>
            <ta e="T243" id="Seg_12507" s="T236">Today you [=we] spent a night in the taiga, it was very cold.</ta>
            <ta e="T250" id="Seg_12508" s="T243">And we froze completely, we froze very much.</ta>
            <ta e="T256" id="Seg_12509" s="T251">It is very cold, the water froze.</ta>
            <ta e="T260" id="Seg_12510" s="T256">I stood with my feet.</ta>
            <ta e="T263" id="Seg_12511" s="T260">It did not break.</ta>
            <ta e="T268" id="Seg_12512" s="T264">When you go to sleep, ask:</ta>
            <ta e="T272" id="Seg_12513" s="T268">"Will you let me sleep here?"</ta>
            <ta e="T278" id="Seg_12514" s="T272">First they stood there, on the shore of (the river) Ilbin.</ta>
            <ta e="T282" id="Seg_12515" s="T278">Then they found this river.</ta>
            <ta e="T291" id="Seg_12516" s="T284">Does not freeze, they built tents there.</ta>
            <ta e="T298" id="Seg_12517" s="T291">And they used to come to live here when it became cold.</ta>
            <ta e="T304" id="Seg_12518" s="T299">They came, they lived on Ilbin.</ta>
            <ta e="T312" id="Seg_12519" s="T304">They came here, they found a river that does not freeze.</ta>
            <ta e="T315" id="Seg_12520" s="T312">They built their tents here.</ta>
            <ta e="T322" id="Seg_12521" s="T315">They come to live here when it became cold.</ta>
            <ta e="T329" id="Seg_12522" s="T322">They did not stay [here], they went to taiga, everywhere.</ta>
            <ta e="T332" id="Seg_12523" s="T329">They wandered everywhere.</ta>
            <ta e="T337" id="Seg_12524" s="T332">They went to Belogorye, there is much water.</ta>
            <ta e="T345" id="Seg_12525" s="T338">One person here, and one person there.</ta>
            <ta e="T349" id="Seg_12526" s="T346">Go there and here!</ta>
            <ta e="T356" id="Seg_12527" s="T349">I will not go there and I will not go here.</ta>
            <ta e="T360" id="Seg_12528" s="T357">How can it be?</ta>
            <ta e="T363" id="Seg_12529" s="T360">You won't go, my son.</ta>
            <ta e="T369" id="Seg_12530" s="T363">This person lives very well.</ta>
            <ta e="T373" id="Seg_12531" s="T369">He has everything.</ta>
            <ta e="T380" id="Seg_12532" s="T374">The foreigners came to speak in my language.</ta>
            <ta e="T386" id="Seg_12533" s="T381">They are learning my language.</ta>
            <ta e="T392" id="Seg_12534" s="T387">Ask, and I will tell you.</ta>
            <ta e="T396" id="Seg_12535" s="T392">What did you take from (there?).</ta>
            <ta e="T400" id="Seg_12536" s="T397">Whom do I have to speak to?</ta>
            <ta e="T405" id="Seg_12537" s="T400">You have a very long tongue.</ta>
            <ta e="T409" id="Seg_12538" s="T405">And I have a small tongue.</ta>
            <ta e="T416" id="Seg_12539" s="T410">We came to the road.</ta>
            <ta e="T420" id="Seg_12540" s="T416">A person is coming to us.</ta>
            <ta e="T428" id="Seg_12541" s="T420">Ask the way, where does he go, because we…</ta>
            <ta e="T435" id="Seg_12542" s="T429">Perhaps we went a wrong way.</ta>
            <ta e="T444" id="Seg_12543" s="T436">"You aren't speaking, I'm speaking with you."</ta>
            <ta e="T448" id="Seg_12544" s="T444">"I don't know.</ta>
            <ta e="T454" id="Seg_12545" s="T448">I know, but I can't say (it)."</ta>
            <ta e="T460" id="Seg_12546" s="T454">"Your tongue should be torn out and thrown away."</ta>
            <ta e="T467" id="Seg_12547" s="T461">They learned it and will also become Kamassians.</ta>
            <ta e="T470" id="Seg_12548" s="T468">Very…</ta>
            <ta e="T473" id="Seg_12549" s="T470">A strong wind.</ta>
            <ta e="T476" id="Seg_12550" s="T473">It blows hard.</ta>
            <ta e="T484" id="Seg_12551" s="T476">The snow is freezing and the ground is freezing.</ta>
            <ta e="T492" id="Seg_12552" s="T484">The snow and the ground are mixed up.</ta>
            <ta e="T501" id="Seg_12553" s="T492">Oh God, oh God, don't abandon me.</ta>
            <ta e="T507" id="Seg_12554" s="T501">Don't abandon me, don't leave me.</ta>
            <ta e="T518" id="Seg_12555" s="T507">Take from me the silly heart, give me a clever heart.</ta>
            <ta e="T522" id="Seg_12556" s="T518">Teach me to praise you.</ta>
            <ta e="T528" id="Seg_12557" s="T522">Teach me to follow your way.</ta>
            <ta e="T533" id="Seg_12558" s="T529">To follow your (ways?).</ta>
            <ta e="T541" id="Seg_12559" s="T537">Vanya, go to your godmother.</ta>
            <ta e="T544" id="Seg_12560" s="T541">Ask her to give you scissors.</ta>
            <ta e="T552" id="Seg_12561" s="T544">Shears for sheep, we have to cut their fleece.</ta>
            <ta e="T555" id="Seg_12562" s="T553">Ask well.</ta>
            <ta e="T564" id="Seg_12563" s="T556">Yesterday I climbed on the stove.</ta>
            <ta e="T566" id="Seg_12564" s="T564">Not to eat (?).</ta>
            <ta e="T570" id="Seg_12565" s="T566">Van'ka came with his father.</ta>
            <ta e="T571" id="Seg_12566" s="T570">Says:</ta>
            <ta e="T577" id="Seg_12567" s="T571">"Granny, look, what a big fish."</ta>
            <ta e="T582" id="Seg_12568" s="T577">I asked: "How many did you catch?"</ta>
            <ta e="T593" id="Seg_12569" s="T582">He says: "We caught seven."</ta>
            <ta e="T601" id="Seg_12570" s="T594">The fascists fought [with] our people.</ta>
            <ta e="T605" id="Seg_12571" s="T601">And we two were sitting home.</ta>
            <ta e="T612" id="Seg_12572" s="T605">Grishka went, caught a fish and brought [it] home.</ta>
            <ta e="T617" id="Seg_12573" s="T612">He'll bring three ducks.</ta>
            <ta e="T622" id="Seg_12574" s="T617">I milked sheep.</ta>
            <ta e="T631" id="Seg_12575" s="T623">I worked hard in the kolkhoz, I cropped much rye.</ta>
            <ta e="T636" id="Seg_12576" s="T631">I cropped twenty, and two more.</ta>
            <ta e="T647" id="Seg_12577" s="T636">Then one day I cropped thirty and four more.</ta>
            <ta e="T649" id="Seg_12578" s="T648">Sotka's.</ta>
            <ta e="T652" id="Seg_12579" s="T649">A relative of mine asks:</ta>
            <ta e="T659" id="Seg_12580" s="T652">"I should go to hear you, how you're speaking."</ta>
            <ta e="T663" id="Seg_12581" s="T659">"Come tomorrow, they'll come.</ta>
            <ta e="T664" id="Seg_12582" s="T663">Listen."</ta>
            <ta e="T670" id="Seg_12583" s="T665">My chickens are coming out.</ta>
            <ta e="T672" id="Seg_12584" s="T670">They are peeping.</ta>
            <ta e="T675" id="Seg_12585" s="T673">Hold your hand.</ta>
            <ta e="T682" id="Seg_12586" s="T676">Yesterday women came to me to (separate?) the milk.</ta>
            <ta e="T685" id="Seg_12587" s="T682">They were many.</ta>
            <ta e="T691" id="Seg_12588" s="T685">Chairman's wife was sitting here.</ta>
            <ta e="T693" id="Seg_12589" s="T691">Her(?) son came.</ta>
            <ta e="T701" id="Seg_12590" s="T693">He says her: "Let's go home, we'll go to Krasnoyarsk.</ta>
            <ta e="T705" id="Seg_12591" s="T701">I said him:</ta>
            <ta e="T712" id="Seg_12592" s="T705">"Buy your mother something beautiful, buy a dress.</ta>
            <ta e="T715" id="Seg_12593" s="T712">Buy a beautiful clothing."</ta>
            <ta e="T722" id="Seg_12594" s="T715">They are laughing because I speak like this.</ta>
            <ta e="T730" id="Seg_12595" s="T722">They cannot speak [Kamassian].</ta>
            <ta e="T734" id="Seg_12596" s="T731">A woman came today.</ta>
            <ta e="T746" id="Seg_12597" s="T734">"I have to (plant?) potatoes, I'll go to my son, let him plant potatoes for me."</ta>
            <ta e="T750" id="Seg_12598" s="T747">I was in Zaozerka.</ta>
            <ta e="T753" id="Seg_12599" s="T750">I went to the pharmacy.</ta>
            <ta e="T756" id="Seg_12600" s="T753">I wanted to buy eyeglasses.</ta>
            <ta e="T759" id="Seg_12601" s="T756">To look into the distance.</ta>
            <ta e="T764" id="Seg_12602" s="T759">They didn't give [me], they say: "Go.</ta>
            <ta e="T770" id="Seg_12603" s="T764">Have your eyes checked, then you'll come and buy."</ta>
            <ta e="T778" id="Seg_12604" s="T771">I came there, they are fighting, "Why are you fighting?</ta>
            <ta e="T780" id="Seg_12605" s="T778">Don't shout.</ta>
            <ta e="T782" id="Seg_12606" s="T780">Behave well.</ta>
            <ta e="T784" id="Seg_12607" s="T782">Tidy the house."</ta>
            <ta e="T788" id="Seg_12608" s="T785">I say to one of them: "Go!</ta>
            <ta e="T790" id="Seg_12609" s="T788">Thresh the flax (?).</ta>
            <ta e="T796" id="Seg_12610" s="T790">And he, let him bring water to the sauna.</ta>
            <ta e="T805" id="Seg_12611" s="T797">Today I got up early, it wasn't five o'clock yet.</ta>
            <ta e="T808" id="Seg_12612" s="T805">I heated the stove.</ta>
            <ta e="T810" id="Seg_12613" s="T808">I put (water?) [on the stove].</ta>
            <ta e="T815" id="Seg_12614" s="T810">They I lay down again and slept some more.</ta>
            <ta e="T821" id="Seg_12615" s="T815">It was ten o'clock, then I got up.</ta>
            <ta e="T829" id="Seg_12616" s="T822">Today I worked hard, and yesterday not hard.</ta>
            <ta e="T835" id="Seg_12617" s="T830">That stone is very big, heavy.</ta>
            <ta e="T840" id="Seg_12618" s="T835">And this stick is not heavy.</ta>
            <ta e="T852" id="Seg_12619" s="T841">This child tore his pants, there is now a hole.</ta>
            <ta e="T855" id="Seg_12620" s="T852">I have to sew it up.</ta>
            <ta e="T857" id="Seg_12621" s="T855">To patch.</ta>
            <ta e="T863" id="Seg_12622" s="T858">Weigh [= sell] me five kilograms of sugar.</ta>
            <ta e="T876" id="Seg_12623" s="T864">This person was ill, now he isn't ill, he got up, he's walking, and [before] he was lying.</ta>
            <ta e="T879" id="Seg_12624" s="T876">It is not good to (eavesdrop?).</ta>
            <ta e="T884" id="Seg_12625" s="T879">[They] came and said, this is not good.</ta>
            <ta e="T885" id="Seg_12626" s="T884">Speaking(?).</ta>
            <ta e="T891" id="Seg_12627" s="T886">He killed someone.</ta>
            <ta e="T893" id="Seg_12628" s="T891">He was put into prison.</ta>
            <ta e="T903" id="Seg_12629" s="T893">He was three years in prison and now has come [home] and is walking [here].</ta>
            <ta e="T910" id="Seg_12630" s="T904">All cups have been broken.</ta>
            <ta e="T920" id="Seg_12631" s="T912">I was hit on the head with a stone.</ta>
            <ta e="T923" id="Seg_12632" s="T920">Blood is flowing.</ta>
            <ta e="T930" id="Seg_12633" s="T923">With an axe my head…</ta>
            <ta e="T932" id="Seg_12634" s="T931">…was broken.</ta>
            <ta e="T937" id="Seg_12635" s="T932">You'll go to the forest to gather berries.</ta>
            <ta e="T941" id="Seg_12636" s="T937">Let's go home, there are no berries.</ta>
            <ta e="T944" id="Seg_12637" s="T941">Only to go (?).</ta>
            <ta e="T949" id="Seg_12638" s="T944">Wherever they go, there are very many berries.</ta>
            <ta e="T952" id="Seg_12639" s="T949">Then you're gathering again.</ta>
            <ta e="T960" id="Seg_12640" s="T952">When I (decide?) to go home, there are a lot of berries.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T8" id="Seg_12641" s="T4">Ich ging heute dorthin.</ta>
            <ta e="T11" id="Seg_12642" s="T8">Dort lebt eine Frau.</ta>
            <ta e="T14" id="Seg_12643" s="T11">"Iss, iss Kohl!"</ta>
            <ta e="T17" id="Seg_12644" s="T14">"Oh, er ist sehr sauer!</ta>
            <ta e="T19" id="Seg_12645" s="T17">I werde ihn nicht essen."</ta>
            <ta e="T25" id="Seg_12646" s="T19">Dann ging die Tochter, um zu scheißen, und ein Hund mit ihr.</ta>
            <ta e="T27" id="Seg_12647" s="T25">Ich…</ta>
            <ta e="T34" id="Seg_12648" s="T27">Sie kam und ich sage ihr:</ta>
            <ta e="T40" id="Seg_12649" s="T34">"Du bist scheißen gegangen, ein Hund mit dir."</ta>
            <ta e="T45" id="Seg_12650" s="T40">Sie fing an sehr doll zu lachen.</ta>
            <ta e="T51" id="Seg_12651" s="T45">Reibe [meine?] Beine, sie schmerzen sehr.</ta>
            <ta e="T60" id="Seg_12652" s="T52">Viele Jahre sind vergangen, die Leute wissen nichts. </ta>
            <ta e="T68" id="Seg_12653" s="T60">Sie haben viele Pferde und viele Kühe.</ta>
            <ta e="T77" id="Seg_12654" s="T68">Und viele Schafe und viele Hühner, sie essen viel Fleisch.</ta>
            <ta e="T80" id="Seg_12655" s="T77">[Sie] haben viel Milch.</ta>
            <ta e="T84" id="Seg_12656" s="T81">Die Kamassen lebten.</ta>
            <ta e="T95" id="Seg_12657" s="T84">Sie machten Götzenbilder aus Holz, dann aus Stein [= Lehm], dann aus Papier [= zeichneten sie auf Papier].</ta>
            <ta e="T102" id="Seg_12658" s="T96">Sie machten Götter aus Eisen und stellten [sie] auf.</ta>
            <ta e="T109" id="Seg_12659" s="T103">Sie stellten Kerzen für sie auf, sie zünden (sie) an.</ta>
            <ta e="T114" id="Seg_12660" s="T110">Die Kamassen gingen immer durch den Wald.</ta>
            <ta e="T122" id="Seg_12661" s="T114">Wenn es kalt wurde, kamen sie immer hierher.</ta>
            <ta e="T128" id="Seg_12662" s="T122">Sie lebten dort in der alten Siedlung, dann sahen (sie):</ta>
            <ta e="T132" id="Seg_12663" s="T128">Das Wasser friert hier nicht zu.</ta>
            <ta e="T143" id="Seg_12664" s="T132">Dann bauten sie hier Zelte auf und lebten hier.</ta>
            <ta e="T146" id="Seg_12665" s="T143">Sie gingen.</ta>
            <ta e="T153" id="Seg_12666" s="T146">Der Weg war schmal.</ta>
            <ta e="T160" id="Seg_12667" s="T153">Einer geht, so, sie gingen wie Enten.</ta>
            <ta e="T162" id="Seg_12668" s="T160">Sie gingen in den Wald.</ta>
            <ta e="T168" id="Seg_12669" s="T162">Ein Mensch geht nicht, viele gehen.</ta>
            <ta e="T173" id="Seg_12670" s="T169">Sie kommen aus der Taiga.</ta>
            <ta e="T179" id="Seg_12671" s="T173">Eins, zwei, drei, vier, fünf, sechs.</ta>
            <ta e="T186" id="Seg_12672" s="T179">Sie kommen nacheinander.</ta>
            <ta e="T189" id="Seg_12673" s="T186">Das Wasser fror [zu].</ta>
            <ta e="T196" id="Seg_12674" s="T189">Wenn du mit deinen Füßen stehst, fällst du nicht dort hinein.</ta>
            <ta e="T202" id="Seg_12675" s="T196">Ich war klein, meine Mutter arbeitete.</ta>
            <ta e="T207" id="Seg_12676" s="T202">Sie machte Felle und nähte Pelzmäntel.</ta>
            <ta e="T217" id="Seg_12677" s="T207">Sie nähte Stiefel, nähte eine Mütze und…</ta>
            <ta e="T220" id="Seg_12678" s="T217">Mein Vater trug [sie].</ta>
            <ta e="T228" id="Seg_12679" s="T220">Nun habe ich keine Kraft mehr, ich weiß nicht, wo sie hin ist.</ta>
            <ta e="T235" id="Seg_12680" s="T229">Ich habe viel Angst, ich zittere.</ta>
            <ta e="T243" id="Seg_12681" s="T236">Heute verbrachtet ihr [=wir] eine Nacht in der Taiga, es war sehr kalt.</ta>
            <ta e="T250" id="Seg_12682" s="T243">Und wir froren total, wir froren sehr doll.</ta>
            <ta e="T256" id="Seg_12683" s="T251">Es war sehr kalt, das Wasser fror [zu].</ta>
            <ta e="T260" id="Seg_12684" s="T256">Ich stand mit meinen Füßen.</ta>
            <ta e="T263" id="Seg_12685" s="T260">Es brach nicht ein.</ta>
            <ta e="T268" id="Seg_12686" s="T264">Wenn du schlafen gehst, frage:</ta>
            <ta e="T272" id="Seg_12687" s="T268">"Lässt du mich hier schlafen?"</ta>
            <ta e="T278" id="Seg_12688" s="T272">Zuerst standen sie dort, am Ufer des (Flusses) Ilbin.</ta>
            <ta e="T282" id="Seg_12689" s="T278">Dann fanden sie diesen Fluss.</ta>
            <ta e="T291" id="Seg_12690" s="T284">Er friert nicht zu, sie stellten hier Zelte auf.</ta>
            <ta e="T298" id="Seg_12691" s="T291">Und sie kamen immer, um hier zu leben, wenn es kalt wurde.</ta>
            <ta e="T304" id="Seg_12692" s="T299">Sie kamen, sie lebten am Ilbin.</ta>
            <ta e="T312" id="Seg_12693" s="T304">Sie kamen hierher, sie fanden einen Fluss, der nicht zufriert.</ta>
            <ta e="T315" id="Seg_12694" s="T312">Sie bauten ihre Zelte hier auf.</ta>
            <ta e="T322" id="Seg_12695" s="T315">Sie kommen hierher zum leben, wenn es kalt wird.</ta>
            <ta e="T329" id="Seg_12696" s="T322">Sie blieben nicht [hier], sie gingen in die Taiga, überall hin.</ta>
            <ta e="T332" id="Seg_12697" s="T329">Sie gingen überall umher.</ta>
            <ta e="T337" id="Seg_12698" s="T332">Sie gingen nach Belogorje, dort gibt es viel Wasser.</ta>
            <ta e="T345" id="Seg_12699" s="T338">Ein Mensch, und ein Mensch dort.</ta>
            <ta e="T349" id="Seg_12700" s="T346">Geh hierhin und dorthin!</ta>
            <ta e="T356" id="Seg_12701" s="T349">Ich gehe nicht dorthin und ich gehe nicht hierhin.</ta>
            <ta e="T360" id="Seg_12702" s="T357">Wie wird es sein?</ta>
            <ta e="T363" id="Seg_12703" s="T360">Du gehst nicht, mein Sohn.</ta>
            <ta e="T369" id="Seg_12704" s="T363">Dieser Mensch lebt sehr gut.</ta>
            <ta e="T373" id="Seg_12705" s="T369">Er hat alles.</ta>
            <ta e="T380" id="Seg_12706" s="T374">Die Fremden kamen, um auf meiner Sprache zu sprechen.</ta>
            <ta e="T386" id="Seg_12707" s="T381">Sie lernen meine Sprache.</ta>
            <ta e="T392" id="Seg_12708" s="T387">Frage und ich sage es dir.</ta>
            <ta e="T396" id="Seg_12709" s="T392">Was hast du von dort (genommen?).</ta>
            <ta e="T400" id="Seg_12710" s="T397">Mit wem muss ich sprechen?</ta>
            <ta e="T405" id="Seg_12711" s="T400">Du hast eine sehr lange Zunge.</ta>
            <ta e="T409" id="Seg_12712" s="T405">Und ich habe eine kleine Zunge.</ta>
            <ta e="T416" id="Seg_12713" s="T410">Wir kamen zum Weg.</ta>
            <ta e="T420" id="Seg_12714" s="T416">Ein Mensch kommt zu uns.</ta>
            <ta e="T428" id="Seg_12715" s="T420">Frag nach dem Weg, wohin er geht, weil wir…</ta>
            <ta e="T435" id="Seg_12716" s="T429">Vielleicht sind wir nicht dorthin gegangen.</ta>
            <ta e="T444" id="Seg_12717" s="T436">"Du sprichst nicht, ich spreche mit dir."</ta>
            <ta e="T448" id="Seg_12718" s="T444">"Ich weiß nicht.</ta>
            <ta e="T454" id="Seg_12719" s="T448">Ich weiß, aber kann (es) nicht sagen."</ta>
            <ta e="T460" id="Seg_12720" s="T454">"Deine Zunge sollte herausgezogen und weggeworfen werden."</ta>
            <ta e="T467" id="Seg_12721" s="T461">Sie lernten es und werden auch Kamassen werden.</ta>
            <ta e="T470" id="Seg_12722" s="T468">Sehr…</ta>
            <ta e="T473" id="Seg_12723" s="T470">Ein starker Wind.</ta>
            <ta e="T476" id="Seg_12724" s="T473">Er weht kräftig.</ta>
            <ta e="T484" id="Seg_12725" s="T476">Der Schnee friert und der Boden friert.</ta>
            <ta e="T492" id="Seg_12726" s="T484">Der Schnee und der Boden vermischen sich.</ta>
            <ta e="T501" id="Seg_12727" s="T492">Oh Gott, oh Gott, verlass mich nicht.</ta>
            <ta e="T507" id="Seg_12728" s="T501">Lass mich nicht [alleine], verlass mich nicht.</ta>
            <ta e="T518" id="Seg_12729" s="T507">Nimm das dumme Herz von mir, gib mir ein kluges Herz.</ta>
            <ta e="T522" id="Seg_12730" s="T518">Lehre mich, dich zu lobpreisen.</ta>
            <ta e="T528" id="Seg_12731" s="T522">Lehre mich, deinem Weg zu folgen.</ta>
            <ta e="T533" id="Seg_12732" s="T529">Um deinen (Wegen?) zu folgen.</ta>
            <ta e="T541" id="Seg_12733" s="T537">Vanja, geh zu deiner Patin.</ta>
            <ta e="T544" id="Seg_12734" s="T541">Frag sie nach einer Schere.</ta>
            <ta e="T552" id="Seg_12735" s="T544">Eine Schere für Schafe, wir müssen ihr Fell scheren.</ta>
            <ta e="T555" id="Seg_12736" s="T553">Frage gut.</ta>
            <ta e="T564" id="Seg_12737" s="T556">Gestern bin ich auf den Ofen geklettert.</ta>
            <ta e="T566" id="Seg_12738" s="T564">Um nicht zu essen (?).</ta>
            <ta e="T570" id="Seg_12739" s="T566">Van'ka kam mit seinem Vater.</ta>
            <ta e="T571" id="Seg_12740" s="T570">Er sagt:</ta>
            <ta e="T577" id="Seg_12741" s="T571">"Oma, guck, was für ein großer Fisch."</ta>
            <ta e="T582" id="Seg_12742" s="T577">Ich fragte: "Wie viele habt ihr gefangen?"</ta>
            <ta e="T593" id="Seg_12743" s="T582">Er sagt: "Wir haben sieben gefangen."</ta>
            <ta e="T601" id="Seg_12744" s="T594">Die Faschisten kämpften mit unseren Leuten.</ta>
            <ta e="T605" id="Seg_12745" s="T601">Und wir beide saßen zuhause.</ta>
            <ta e="T612" id="Seg_12746" s="T605">Grischka ging, fing einen Fisch und brachte [ihn] nach Hause.</ta>
            <ta e="T617" id="Seg_12747" s="T612">Er wird drei Enten bringen.</ta>
            <ta e="T622" id="Seg_12748" s="T617">Ich molk ein Schaf.</ta>
            <ta e="T631" id="Seg_12749" s="T623">Ich arbeitete hart in der Kolchose, ich erntete viel Roggen.</ta>
            <ta e="T636" id="Seg_12750" s="T631">Ich erntete zwanzig und noch zwei.</ta>
            <ta e="T647" id="Seg_12751" s="T636">Dann an einem Tag erntete ich dreißig und noch vier.</ta>
            <ta e="T649" id="Seg_12752" s="T648">Sotkas.</ta>
            <ta e="T652" id="Seg_12753" s="T649">Ein Verwandter von mir fragt:</ta>
            <ta e="T659" id="Seg_12754" s="T652">"Ich sollte gehen, um zu hören, wie du sprichst."</ta>
            <ta e="T663" id="Seg_12755" s="T659">"Komm morgen, sie werden kommen.</ta>
            <ta e="T664" id="Seg_12756" s="T663">Hör zu."</ta>
            <ta e="T670" id="Seg_12757" s="T665">Meine Hühner kommen heraus.</ta>
            <ta e="T672" id="Seg_12758" s="T670">Sie gackern.</ta>
            <ta e="T675" id="Seg_12759" s="T673">Halt deine Hand.</ta>
            <ta e="T682" id="Seg_12760" s="T676">Gestern kamen Frauen zu mir, um die Milch zu (trennen?).</ta>
            <ta e="T685" id="Seg_12761" s="T682">Sie waren viele.</ta>
            <ta e="T691" id="Seg_12762" s="T685">Die Frau des Vorsitzenden saß hier.</ta>
            <ta e="T693" id="Seg_12763" s="T691">Ihr(?) Sohn kam.</ta>
            <ta e="T701" id="Seg_12764" s="T693">Er sagt ihr: "Lass uns nach Hause fahren, wir fahren nach Krasnojarsk."</ta>
            <ta e="T705" id="Seg_12765" s="T701">Ich sagte ihm:</ta>
            <ta e="T712" id="Seg_12766" s="T705">"Kauf deiner Mutter etwas schönes, kauf ein Kleid.</ta>
            <ta e="T715" id="Seg_12767" s="T712">Kauf schöne Kleidung."</ta>
            <ta e="T722" id="Seg_12768" s="T715">Sie lachen, weil ich so spreche.</ta>
            <ta e="T730" id="Seg_12769" s="T722">Sie können nicht [Kamassisch] sprechen.</ta>
            <ta e="T734" id="Seg_12770" s="T731">Eine Frau kam heute.</ta>
            <ta e="T746" id="Seg_12771" s="T734">"Ich muss Kartoffeln (pflanzen?), ich gehe zu meinem Sohn, er soll Kartoffeln für mich pflanzen."</ta>
            <ta e="T750" id="Seg_12772" s="T747">Ich war in Zaozerka.</ta>
            <ta e="T753" id="Seg_12773" s="T750">Ich ging in die Apotheke.</ta>
            <ta e="T756" id="Seg_12774" s="T753">Ich wollte eine Brille kaufen.</ta>
            <ta e="T759" id="Seg_12775" s="T756">Um in die Ferne zu schauen.</ta>
            <ta e="T764" id="Seg_12776" s="T759">Sie gaben [mir] keine, sie sagen: "Geh.</ta>
            <ta e="T770" id="Seg_12777" s="T764">Lass deine Augen untersuchen, dann kommst du und kaufst [eine]."</ta>
            <ta e="T778" id="Seg_12778" s="T771">Ich kam dorthin, sie kämpfen, "Warum kämpft ihr?</ta>
            <ta e="T780" id="Seg_12779" s="T778">Schreit nicht.</ta>
            <ta e="T782" id="Seg_12780" s="T780">Benehmt euch gut.</ta>
            <ta e="T784" id="Seg_12781" s="T782">Räumt das Haus auf."</ta>
            <ta e="T788" id="Seg_12782" s="T785">Ich sage zu einem von ihnen: "Geh!</ta>
            <ta e="T790" id="Seg_12783" s="T788">Dresch das Leinen (?).</ta>
            <ta e="T796" id="Seg_12784" s="T790">Und er, er soll Wasser ins Badehaus bringen."</ta>
            <ta e="T805" id="Seg_12785" s="T797">Heute bin ich früh aufgestanden, es war noch keine fünf Uhr.</ta>
            <ta e="T808" id="Seg_12786" s="T805">Ich heizte den Ofen.</ta>
            <ta e="T810" id="Seg_12787" s="T808">Ich stellte (Wasser?) [auf den Ofen].</ta>
            <ta e="T815" id="Seg_12788" s="T810">Dann legte ich mich wieder hin und schlief noch etwas.</ta>
            <ta e="T821" id="Seg_12789" s="T815">Es war zehn Uhr, dann stand ich auf.</ta>
            <ta e="T829" id="Seg_12790" s="T822">Heute arbeitete ich hart und gestern nicht hart.</ta>
            <ta e="T835" id="Seg_12791" s="T830">Dieser Stein ist sehr groß, schwer.</ta>
            <ta e="T840" id="Seg_12792" s="T835">Und dieser Stock ist nicht schwer.</ta>
            <ta e="T852" id="Seg_12793" s="T841">Dieses Kind hat seine Hose zerrissen, jetzt ist dort ein Loch.</ta>
            <ta e="T855" id="Seg_12794" s="T852">Ich muss es zunähen.</ta>
            <ta e="T857" id="Seg_12795" s="T855">Es flicken.</ta>
            <ta e="T863" id="Seg_12796" s="T858">Wiege [= verkaufe] mir fünf Kilo Zucker.</ta>
            <ta e="T876" id="Seg_12797" s="T864">Dieser Mensch war krank, jetzt ist er nicht krank, er ist aufgestanden, er läuft, und [vorher] lag er.</ta>
            <ta e="T879" id="Seg_12798" s="T876">Es ist nicht gut zu (lauschen?).</ta>
            <ta e="T884" id="Seg_12799" s="T879">[Sie] kamen und sagten, das ist nicht gut.</ta>
            <ta e="T885" id="Seg_12800" s="T884">Sprechend(?).</ta>
            <ta e="T891" id="Seg_12801" s="T886">Er hat jemanden getötet.</ta>
            <ta e="T893" id="Seg_12802" s="T891">Er wurde ins Gefängnis gesteckt.</ta>
            <ta e="T903" id="Seg_12803" s="T893">Er saß drei Jahre, dann kam er nach Hause, er geht.</ta>
            <ta e="T910" id="Seg_12804" s="T904">Alle Tassen sind zerbrochen.</ta>
            <ta e="T920" id="Seg_12805" s="T912">Ich wurde mit einem Stein auf den Kopf geschlagen.</ta>
            <ta e="T923" id="Seg_12806" s="T920">Es fließt Blut.</ta>
            <ta e="T930" id="Seg_12807" s="T923">Mit einer Axt mein Kopf…</ta>
            <ta e="T932" id="Seg_12808" s="T931">…wurde zerbrochen.</ta>
            <ta e="T937" id="Seg_12809" s="T932">Du wirst in den Wald gehen, um Beeren zu sammeln.</ta>
            <ta e="T941" id="Seg_12810" s="T937">Lass uns nach Hause gehen, es gibt keine Beeren.</ta>
            <ta e="T944" id="Seg_12811" s="T941">Nur um zu gehen (?).</ta>
            <ta e="T949" id="Seg_12812" s="T944">Wohin sie auch gehen, dort sind sehr viele Beeren.</ta>
            <ta e="T952" id="Seg_12813" s="T949">Dann sammelst du wieder.</ta>
            <ta e="T960" id="Seg_12814" s="T952">Wenn ich mich (entscheide?) nach Hause zu gehen, sind dort viele Beeren.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T8" id="Seg_12815" s="T4">[GVY:] This recording is transcribed as "Recording 8" in the Kamassian archive in Ekaterinburg.</ta>
            <ta e="T25" id="Seg_12816" s="T19">[GVY:] in the Ekaterinburg transcription, there is 'I' (i.e. măn), but it is heard more like men 'dog'. Dĭnziʔ = dĭziʔ?</ta>
            <ta e="T51" id="Seg_12817" s="T45">[GVY:] In the Ekaterinburg transcription, "натирай"; it makes sense, but the form kĭškəʔi is unclear (expected kĭškeʔ).</ta>
            <ta e="T95" id="Seg_12818" s="T84">[GVY:] in the Ekaterinburg transcription, "иконы".</ta>
            <ta e="T128" id="Seg_12819" s="T122">[GVY:] In the Ekaterinburg transcription, "da vot".</ta>
            <ta e="T132" id="Seg_12820" s="T128">[GVY:] kăndlia</ta>
            <ta e="T160" id="Seg_12821" s="T153">[GVY:] In the Ekaterinburg transcription, "как гуси".</ta>
            <ta e="T207" id="Seg_12822" s="T202">[GVY:] šobi = šöʔpi 'sewed'</ta>
            <ta e="T243" id="Seg_12823" s="T236">[GVY:] d’ijegət</ta>
            <ta e="T260" id="Seg_12824" s="T256">[GVY:] üjüzəbi?</ta>
            <ta e="T268" id="Seg_12825" s="T264">[GVY:] These two sentences are missing in the Ekaterinburg transcription.</ta>
            <ta e="T315" id="Seg_12826" s="T312">[GVY:] maksandə. This sentence is missing in the Ekaterinburg transcription.</ta>
            <ta e="T369" id="Seg_12827" s="T363">[GVY:] amnolaʔbəm 'I live'?</ta>
            <ta e="T396" id="Seg_12828" s="T392">[GVY:] Unclear.</ta>
            <ta e="T416" id="Seg_12829" s="T410">[GVY:] In the Ekaterinburg transcription, this sentence is translated as "Чего пришли?"</ta>
            <ta e="T492" id="Seg_12830" s="T484">[GVY:] [bĭrgaeruʔpi]. From bĭlgar- 'make butter'?</ta>
            <ta e="T518" id="Seg_12831" s="T507">[GVY:] sagəšsəbi - probably mistakenly used instead of sagəšsət 'silly'.</ta>
            <ta e="T528" id="Seg_12832" s="T522">[GVY:] The ending in aktʼitəm is unclear.</ta>
            <ta e="T544" id="Seg_12833" s="T541">[GVY:] dĭʔən = dĭʔə Ablative?</ta>
            <ta e="T636" id="Seg_12834" s="T631">[GVY:] that is, 24 sotka's (1 sotka = 100 square meters).</ta>
            <ta e="T682" id="Seg_12835" s="T676">[DCh]: Maybe it is meant to skim the cream off the milk?</ta>
            <ta e="T701" id="Seg_12836" s="T693">[GVY:] This interpretation is tentative; 'she says him' or 'I say him' are also possible.</ta>
            <ta e="T712" id="Seg_12837" s="T705">[GVY:] Plat means 'scarf' (D 53b), but it is possible that PKZ uses it as the Russian платье 'dress'. In the Ekaterinburg transcription there is "платье".</ta>
            <ta e="T746" id="Seg_12838" s="T734">[GVY:] amzittə rather means 'eat', but here it seems to be used as 'plant' (in fact, 'seat').</ta>
            <ta e="T805" id="Seg_12839" s="T797">!!!</ta>
            <ta e="T810" id="Seg_12840" s="T808">[GVY:] In the Ekaterinburg transcription, there is 'воду', but the Kamassian word is unclear.</ta>
            <ta e="T829" id="Seg_12841" s="T822">[GVY:] sedeŋ</ta>
            <ta e="T855" id="Seg_12842" s="T852">[GVY:] In the Ekaterinburg transcription there is "их", i.e. 'pants', and not "ее" (the hole).</ta>
            <ta e="T879" id="Seg_12843" s="T876">[GVY:] This whole fragment is not clear.</ta>
            <ta e="T903" id="Seg_12844" s="T893">[GVY:] ki 'month' instead of kö 'winter, year'?</ta>
            <ta e="T910" id="Seg_12845" s="T904">[GVY:] In the Ekaterinburg transcription, there is par 'two' (= Russian "пара").</ta>
            <ta e="T937" id="Seg_12846" s="T932">[GVY:] In the Ekaterinburg transcription, "поедешь".</ta>
            <ta e="T944" id="Seg_12847" s="T941">[GVY:] Unclear.</ta>
            <ta e="T949" id="Seg_12848" s="T944">[AAV] From here on, the speed is irregular.</ta>
            <ta e="T960" id="Seg_12849" s="T952">[GVY:] Unclear.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T964" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
