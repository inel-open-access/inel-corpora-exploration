<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID49BB900A-3159-B2AF-FA1C-0BEEEC3D101A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1970_F0255.wav" />
         <referenced-file url="PKZ_1970_F0255.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1970_F0255\PKZ_1970_F0255.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">672</ud-information>
            <ud-information attribute-name="# HIAT:w">423</ud-information>
            <ud-information attribute-name="# e">285</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">10</ud-information>
            <ud-information attribute-name="# HIAT:u">74</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.02" type="appl" />
         <tli id="T1" time="0.758" type="appl" />
         <tli id="T2" time="1.496" type="appl" />
         <tli id="T3" time="2.234" type="appl" />
         <tli id="T4" time="2.972" type="appl" />
         <tli id="T5" time="3.7266485370937334" />
         <tli id="T6" time="4.512" type="appl" />
         <tli id="T7" time="5.313" type="appl" />
         <tli id="T8" time="6.0" type="appl" />
         <tli id="T9" time="7.1999649732759075" />
         <tli id="T10" time="8.027" type="appl" />
         <tli id="T11" time="8.831" type="appl" />
         <tli id="T12" time="9.634" type="appl" />
         <tli id="T13" time="10.438" type="appl" />
         <tli id="T14" time="11.242" type="appl" />
         <tli id="T15" time="11.885" type="appl" />
         <tli id="T16" time="12.527" type="appl" />
         <tli id="T17" time="13.433267982547179" />
         <tli id="T18" time="14.036" type="appl" />
         <tli id="T19" time="14.719" type="appl" />
         <tli id="T20" time="15.403" type="appl" />
         <tli id="T21" time="16.086" type="appl" />
         <tli id="T22" time="16.77" type="appl" />
         <tli id="T23" time="17.453" type="appl" />
         <tli id="T24" time="18.137" type="appl" />
         <tli id="T25" time="18.82" type="appl" />
         <tli id="T26" time="20.279901341393806" />
         <tli id="T27" time="21.373" type="appl" />
         <tli id="T28" time="22.362" type="appl" />
         <tli id="T29" time="23.352" type="appl" />
         <tli id="T30" time="25.059878087540863" />
         <tli id="T31" time="26.167" type="appl" />
         <tli id="T32" time="27.116" type="appl" />
         <tli id="T33" time="28.064" type="appl" />
         <tli id="T34" time="29.013" type="appl" />
         <tli id="T35" time="29.962" type="appl" />
         <tli id="T36" time="30.911" type="appl" />
         <tli id="T37" time="31.86" type="appl" />
         <tli id="T38" time="32.809" type="appl" />
         <tli id="T39" time="33.757" type="appl" />
         <tli id="T40" time="34.706" type="appl" />
         <tli id="T441" time="35.11271428571429" type="intp" />
         <tli id="T41" time="35.655" type="appl" />
         <tli id="T42" time="36.637" type="appl" />
         <tli id="T43" time="37.555" type="appl" />
         <tli id="T44" time="38.473" type="appl" />
         <tli id="T45" time="39.392" type="appl" />
         <tli id="T46" time="40.31" type="appl" />
         <tli id="T47" time="41.228" type="appl" />
         <tli id="T48" time="42.146" type="appl" />
         <tli id="T49" time="42.91312456757131" />
         <tli id="T50" time="43.616" type="appl" />
         <tli id="T51" time="44.168" type="appl" />
         <tli id="T52" time="44.719" type="appl" />
         <tli id="T53" time="45.271" type="appl" />
         <tli id="T54" time="45.823" type="appl" />
         <tli id="T55" time="46.375" type="appl" />
         <tli id="T56" time="46.927" type="appl" />
         <tli id="T57" time="47.478" type="appl" />
         <tli id="T58" time="48.03" type="appl" />
         <tli id="T59" time="52.94640909051597" />
         <tli id="T60" time="53.86" type="appl" />
         <tli id="T61" time="54.551" type="appl" />
         <tli id="T62" time="55.242" type="appl" />
         <tli id="T63" time="55.933" type="appl" />
         <tli id="T64" time="56.625" type="appl" />
         <tli id="T65" time="57.316" type="appl" />
         <tli id="T66" time="58.007" type="appl" />
         <tli id="T67" time="58.698" type="appl" />
         <tli id="T68" time="59.524" type="appl" />
         <tli id="T69" time="60.35" type="appl" />
         <tli id="T70" time="61.176" type="appl" />
         <tli id="T71" time="62.002" type="appl" />
         <tli id="T72" time="62.793" type="appl" />
         <tli id="T73" time="63.584" type="appl" />
         <tli id="T74" time="64.375" type="appl" />
         <tli id="T75" time="65.166" type="appl" />
         <tli id="T76" time="65.957" type="appl" />
         <tli id="T77" time="66.749" type="appl" />
         <tli id="T78" time="67.54" type="appl" />
         <tli id="T79" time="68.331" type="appl" />
         <tli id="T80" time="69.122" type="appl" />
         <tli id="T81" time="69.913" type="appl" />
         <tli id="T82" time="70.704" type="appl" />
         <tli id="T83" time="71.525" type="appl" />
         <tli id="T84" time="72.225" type="appl" />
         <tli id="T85" time="72.924" type="appl" />
         <tli id="T86" time="73.624" type="appl" />
         <tli id="T87" time="74.324" type="appl" />
         <tli id="T88" time="75.024" type="appl" />
         <tli id="T89" time="75.724" type="appl" />
         <tli id="T90" time="76.424" type="appl" />
         <tli id="T91" time="77.123" type="appl" />
         <tli id="T92" time="77.823" type="appl" />
         <tli id="T93" time="78.523" type="appl" />
         <tli id="T94" time="80.0262773511148" />
         <tli id="T95" time="81.467" type="appl" />
         <tli id="T96" time="85.75958279279747" />
         <tli id="T97" time="86.533" type="appl" />
         <tli id="T98" time="87.051" type="appl" />
         <tli id="T99" time="87.569" type="appl" />
         <tli id="T100" time="88.087" type="appl" />
         <tli id="T101" time="88.605" type="appl" />
         <tli id="T102" time="89.123" type="appl" />
         <tli id="T103" time="89.641" type="appl" />
         <tli id="T104" time="90.152" type="appl" />
         <tli id="T105" time="90.663" type="appl" />
         <tli id="T106" time="91.174" type="appl" />
         <tli id="T107" time="91.685" type="appl" />
         <tli id="T108" time="92.196" type="appl" />
         <tli id="T109" time="92.707" type="appl" />
         <tli id="T110" time="93.218" type="appl" />
         <tli id="T111" time="93.729" type="appl" />
         <tli id="T112" time="94.24" type="appl" />
         <tli id="T113" time="94.751" type="appl" />
         <tli id="T114" time="95.258" type="appl" />
         <tli id="T115" time="95.766" type="appl" />
         <tli id="T116" time="96.273" type="appl" />
         <tli id="T117" time="97.43285933743275" />
         <tli id="T118" time="98.196" type="appl" />
         <tli id="T119" time="98.884" type="appl" />
         <tli id="T120" time="99.572" type="appl" />
         <tli id="T121" time="100.26" type="appl" />
         <tli id="T122" time="100.948" type="appl" />
         <tli id="T123" time="101.636" type="appl" />
         <tli id="T124" time="102.324" type="appl" />
         <tli id="T125" time="104.57949123683252" />
         <tli id="T126" time="105.128" type="appl" />
         <tli id="T127" time="106.68533385681488" />
         <tli id="T128" time="107.641" type="appl" />
         <tli id="T129" time="108.026" type="appl" />
         <tli id="T130" time="108.411" type="appl" />
         <tli id="T131" time="108.796" type="appl" />
         <tli id="T132" time="109.181" type="appl" />
         <tli id="T133" time="109.566" type="appl" />
         <tli id="T134" time="110.137" type="appl" />
         <tli id="T135" time="110.627" type="appl" />
         <tli id="T136" time="111.117" type="appl" />
         <tli id="T137" time="111.606" type="appl" />
         <tli id="T138" time="112.096" type="appl" />
         <tli id="T139" time="112.586" type="appl" />
         <tli id="T140" time="115.09277342466228" />
         <tli id="T141" time="116.172" type="appl" />
         <tli id="T142" time="116.888" type="appl" />
         <tli id="T143" time="117.603" type="appl" />
         <tli id="T144" time="118.318" type="appl" />
         <tli id="T145" time="119.033" type="appl" />
         <tli id="T146" time="119.749" type="appl" />
         <tli id="T147" time="120.464" type="appl" />
         <tli id="T148" time="121.179" type="appl" />
         <tli id="T149" time="121.894" type="appl" />
         <tli id="T150" time="122.61" type="appl" />
         <tli id="T151" time="123.325" type="appl" />
         <tli id="T152" time="124.117" type="appl" />
         <tli id="T153" time="124.659" type="appl" />
         <tli id="T154" time="125.2" type="appl" />
         <tli id="T155" time="125.742" type="appl" />
         <tli id="T156" time="126.283" type="appl" />
         <tli id="T157" time="126.825" type="appl" />
         <tli id="T158" time="127.366" type="appl" />
         <tli id="T159" time="127.908" type="appl" />
         <tli id="T160" time="128.449" type="appl" />
         <tli id="T161" time="129.318" type="appl" />
         <tli id="T162" time="130.044" type="appl" />
         <tli id="T163" time="130.77" type="appl" />
         <tli id="T164" time="131.497" type="appl" />
         <tli id="T165" time="132.223" type="appl" />
         <tli id="T166" time="132.949" type="appl" />
         <tli id="T167" time="133.675" type="appl" />
         <tli id="T168" time="134.401" type="appl" />
         <tli id="T169" time="134.899" type="appl" />
         <tli id="T170" time="135.397" type="appl" />
         <tli id="T171" time="135.895" type="appl" />
         <tli id="T172" time="136.393" type="appl" />
         <tli id="T173" time="136.891" type="appl" />
         <tli id="T174" time="137.389" type="appl" />
         <tli id="T175" time="137.887" type="appl" />
         <tli id="T176" time="138.385" type="appl" />
         <tli id="T177" time="138.883" type="appl" />
         <tli id="T178" time="139.381" type="appl" />
         <tli id="T179" time="139.879" type="appl" />
         <tli id="T180" time="140.377" type="appl" />
         <tli id="T181" time="140.875" type="appl" />
         <tli id="T182" time="141.373" type="appl" />
         <tli id="T183" time="141.871" type="appl" />
         <tli id="T184" time="142.369" type="appl" />
         <tli id="T185" time="142.867" type="appl" />
         <tli id="T186" time="143.368" type="appl" />
         <tli id="T187" time="143.869" type="appl" />
         <tli id="T188" time="144.37" type="appl" />
         <tli id="T189" time="144.871" type="appl" />
         <tli id="T190" time="145.372" type="appl" />
         <tli id="T191" time="145.873" type="appl" />
         <tli id="T192" time="146.374" type="appl" />
         <tli id="T193" time="146.875" type="appl" />
         <tli id="T194" time="147.376" type="appl" />
         <tli id="T195" time="147.878" type="appl" />
         <tli id="T196" time="148.379" type="appl" />
         <tli id="T197" time="148.88" type="appl" />
         <tli id="T198" time="149.381" type="appl" />
         <tli id="T199" time="149.882" type="appl" />
         <tli id="T200" time="150.383" type="appl" />
         <tli id="T201" time="150.884" type="appl" />
         <tli id="T202" time="151.385" type="appl" />
         <tli id="T203" time="151.886" type="appl" />
         <tli id="T204" time="152.387" type="appl" />
         <tli id="T205" time="155.67924264438793" />
         <tli id="T206" time="156.59" type="appl" />
         <tli id="T207" time="157.206" type="appl" />
         <tli id="T208" time="157.822" type="appl" />
         <tli id="T209" time="158.6125617075744" />
         <tli id="T210" time="159.429" type="appl" />
         <tli id="T211" time="160.026" type="appl" />
         <tli id="T212" time="160.623" type="appl" />
         <tli id="T213" time="161.47254779418125" />
         <tli id="T214" time="162.058" type="appl" />
         <tli id="T215" time="162.896" type="appl" />
         <tli id="T216" time="163.734" type="appl" />
         <tli id="T217" time="164.571" type="appl" />
         <tli id="T218" time="165.409" type="appl" />
         <tli id="T219" time="166.247" type="appl" />
         <tli id="T220" time="167.894" type="appl" />
         <tli id="T221" time="169.54" type="appl" />
         <tli id="T222" time="171.187" type="appl" />
         <tli id="T223" time="172.259" type="appl" />
         <tli id="T224" time="173.13" type="appl" />
         <tli id="T225" time="174.002" type="appl" />
         <tli id="T226" time="174.874" type="appl" />
         <tli id="T227" time="175.745" type="appl" />
         <tli id="T228" time="176.617" type="appl" />
         <tli id="T229" time="177.489" type="appl" />
         <tli id="T230" time="178.36" type="appl" />
         <tli id="T231" time="180.00579096613217" />
         <tli id="T232" time="180.582" type="appl" />
         <tli id="T233" time="181.284" type="appl" />
         <tli id="T234" time="181.987" type="appl" />
         <tli id="T235" time="182.69" type="appl" />
         <tli id="T236" time="183.393" type="appl" />
         <tli id="T237" time="184.095" type="appl" />
         <tli id="T238" time="184.798" type="appl" />
         <tli id="T239" time="185.769" type="appl" />
         <tli id="T240" time="186.664" type="appl" />
         <tli id="T241" time="187.559" type="appl" />
         <tli id="T242" time="188.454" type="appl" />
         <tli id="T243" time="189.349" type="appl" />
         <tli id="T244" time="190.11907509989106" />
         <tli id="T245" time="191.111" type="appl" />
         <tli id="T246" time="191.946" type="appl" />
         <tli id="T247" time="192.78" type="appl" />
         <tli id="T248" time="195.33238307128153" />
         <tli id="T249" time="196.802" type="appl" />
         <tli id="T250" time="202.06568364814143" />
         <tli id="T251" time="202.77" type="appl" />
         <tli id="T252" time="203.319" type="appl" />
         <tli id="T253" time="203.867" type="appl" />
         <tli id="T254" time="204.416" type="appl" />
         <tli id="T255" time="204.965" type="appl" />
         <tli id="T256" time="205.514" type="appl" />
         <tli id="T257" time="206.063" type="appl" />
         <tli id="T258" time="206.611" type="appl" />
         <tli id="T259" time="207.16" type="appl" />
         <tli id="T260" time="207.709" type="appl" />
         <tli id="T261" time="208.664" type="appl" />
         <tli id="T262" time="209.323" type="appl" />
         <tli id="T263" time="209.982" type="appl" />
         <tli id="T264" time="210.641" type="appl" />
         <tli id="T265" time="211.3" type="appl" />
         <tli id="T266" time="211.959" type="appl" />
         <tli id="T267" time="212.618" type="appl" />
         <tli id="T268" time="213.277" type="appl" />
         <tli id="T269" time="213.936" type="appl" />
         <tli id="T270" time="214.595" type="appl" />
         <tli id="T271" time="215.254" type="appl" />
         <tli id="T272" time="215.913" type="appl" />
         <tli id="T273" time="216.572" type="appl" />
         <tli id="T274" time="217.231" type="appl" />
         <tli id="T275" time="217.89" type="appl" />
         <tli id="T276" time="218.549" type="appl" />
         <tli id="T277" time="219.208" type="appl" />
         <tli id="T278" time="219.944" type="appl" />
         <tli id="T279" time="220.681" type="appl" />
         <tli id="T280" time="221.417" type="appl" />
         <tli id="T281" time="222.154" type="appl" />
         <tli id="T282" time="223.21891407425667" />
         <tli id="T283" time="224.023" type="appl" />
         <tli id="T284" time="224.574" type="appl" />
         <tli id="T285" time="225.124" type="appl" />
         <tli id="T286" time="225.675" type="appl" />
         <tli id="T287" time="226.225" type="appl" />
         <tli id="T288" time="227.188" type="appl" />
         <tli id="T289" time="227.966" type="appl" />
         <tli id="T290" time="228.744" type="appl" />
         <tli id="T291" time="229.69888255020498" />
         <tli id="T292" time="230.454" type="appl" />
         <tli id="T293" time="231.195" type="appl" />
         <tli id="T294" time="231.937" type="appl" />
         <tli id="T295" time="232.679" type="appl" />
         <tli id="T296" time="233.421" type="appl" />
         <tli id="T297" time="234.162" type="appl" />
         <tli id="T298" time="234.904" type="appl" />
         <tli id="T299" time="236.69218186221093" />
         <tli id="T300" time="237.394" type="appl" />
         <tli id="T301" time="238.166" type="appl" />
         <tli id="T302" time="238.938" type="appl" />
         <tli id="T303" time="239.8588331236054" />
         <tli id="T304" time="240.954" type="appl" />
         <tli id="T305" time="242.164" type="appl" />
         <tli id="T306" time="243.373" type="appl" />
         <tli id="T307" time="244.583" type="appl" />
         <tli id="T308" time="245.793" type="appl" />
         <tli id="T309" time="247.002" type="appl" />
         <tli id="T310" time="248.212" type="appl" />
         <tli id="T311" time="249.422" type="appl" />
         <tli id="T312" time="250.632" type="appl" />
         <tli id="T313" time="251.842" type="appl" />
         <tli id="T314" time="253.051" type="appl" />
         <tli id="T315" time="254.9187598593742" />
         <tli id="T316" time="256.088" type="appl" />
         <tli id="T317" time="264.0653820291284" />
         <tli id="T318" time="264.781" type="appl" />
         <tli id="T319" time="265.521" type="appl" />
         <tli id="T320" time="266.261" type="appl" />
         <tli id="T321" time="267.001" type="appl" />
         <tli id="T322" time="267.88" type="appl" />
         <tli id="T323" time="269.772020933873" />
         <tli id="T324" time="270.519" type="appl" />
         <tli id="T325" time="271.234" type="appl" />
         <tli id="T326" time="271.948" type="appl" />
         <tli id="T327" time="272.663" type="appl" />
         <tli id="T328" time="273.378" type="appl" />
         <tli id="T329" time="279.41864067121577" />
         <tli id="T330" time="280.255" type="appl" />
         <tli id="T331" time="280.928" type="appl" />
         <tli id="T332" time="281.602" type="appl" />
         <tli id="T333" time="282.276" type="appl" />
         <tli id="T334" time="282.949" type="appl" />
         <tli id="T335" time="283.623" type="appl" />
         <tli id="T336" time="284.297" type="appl" />
         <tli id="T337" time="284.97" type="appl" />
         <tli id="T338" time="285.644" type="appl" />
         <tli id="T339" time="287.58526760849634" />
         <tli id="T340" time="288.577" type="appl" />
         <tli id="T341" time="289.198" type="appl" />
         <tli id="T342" time="289.82" type="appl" />
         <tli id="T343" time="290.441" type="appl" />
         <tli id="T344" time="291.062" type="appl" />
         <tli id="T345" time="291.684" type="appl" />
         <tli id="T346" time="292.305" type="appl" />
         <tli id="T347" time="292.926" type="appl" />
         <tli id="T348" time="293.547" type="appl" />
         <tli id="T349" time="294.168" type="appl" />
         <tli id="T350" time="294.79" type="appl" />
         <tli id="T351" time="295.411" type="appl" />
         <tli id="T352" time="296.017" type="appl" />
         <tli id="T353" time="296.622" type="appl" />
         <tli id="T354" time="297.228" type="appl" />
         <tli id="T355" time="297.834" type="appl" />
         <tli id="T356" time="298.44" type="appl" />
         <tli id="T357" time="299.045" type="appl" />
         <tli id="T358" time="299.651" type="appl" />
         <tli id="T359" time="301.149" type="appl" />
         <tli id="T360" time="302.178" type="appl" />
         <tli id="T361" time="304.378519245239" />
         <tli id="T362" time="304.5522596226195" type="intp" />
         <tli id="T363" time="304.726" type="appl" />
         <tli id="T364" time="305.178" type="appl" />
         <tli id="T365" time="305.63" type="appl" />
         <tli id="T366" time="306.2451768309031" />
         <tli id="T367" time="307.165" type="appl" />
         <tli id="T368" time="307.878" type="appl" />
         <tli id="T369" time="308.59" type="appl" />
         <tli id="T370" time="309.303" type="appl" />
         <tli id="T371" time="310.016" type="appl" />
         <tli id="T372" time="310.729" type="appl" />
         <tli id="T373" time="311.441" type="appl" />
         <tli id="T374" time="312.154" type="appl" />
         <tli id="T375" time="313.47847497535156" />
         <tli id="T376" time="315.4651319772369" />
         <tli id="T377" time="316.736" type="appl" />
         <tli id="T378" time="317.751" type="appl" />
         <tli id="T379" time="318.765" type="appl" />
         <tli id="T380" time="319.78" type="appl" />
         <tli id="T381" time="320.794" type="appl" />
         <tli id="T382" time="321.809" type="appl" />
         <tli id="T383" time="322.823" type="appl" />
         <tli id="T384" time="323.838" type="appl" />
         <tli id="T385" time="324.852" type="appl" />
         <tli id="T386" time="325.867" type="appl" />
         <tli id="T387" time="326.881" type="appl" />
         <tli id="T388" time="327.81" type="appl" />
         <tli id="T389" time="328.74" type="appl" />
         <tli id="T390" time="329.669" type="appl" />
         <tli id="T391" time="331.098389257174" />
         <tli id="T392" time="332.138" type="appl" />
         <tli id="T393" time="332.967" type="appl" />
         <tli id="T394" time="333.796" type="appl" />
         <tli id="T395" time="334.624" type="appl" />
         <tli id="T396" time="335.453" type="appl" />
         <tli id="T397" time="336.282" type="appl" />
         <tli id="T398" time="337.111" type="appl" />
         <tli id="T399" time="342.1383355495304" />
         <tli id="T400" time="343.042" type="appl" />
         <tli id="T401" time="343.584" type="appl" />
         <tli id="T402" time="344.125" type="appl" />
         <tli id="T403" time="344.666" type="appl" />
         <tli id="T404" time="345.208" type="appl" />
         <tli id="T405" time="345.749" type="appl" />
         <tli id="T406" time="346.29" type="appl" />
         <tli id="T407" time="346.832" type="appl" />
         <tli id="T408" time="347.373" type="appl" />
         <tli id="T409" time="347.914" type="appl" />
         <tli id="T410" time="348.456" type="appl" />
         <tli id="T411" time="350.3382956579835" />
         <tli id="T412" time="350.59014782899175" type="intp" />
         <tli id="T413" time="350.842" type="appl" />
         <tli id="T414" time="351.436" type="appl" />
         <tli id="T415" time="352.03" type="appl" />
         <tli id="T416" time="355.4382708473873" />
         <tli id="T417" time="356.758" type="appl" />
         <tli id="T418" time="357.953" type="appl" />
         <tli id="T419" time="359.149" type="appl" />
         <tli id="T420" time="360.55157930526" />
         <tli id="T421" time="361.639" type="appl" />
         <tli id="T422" time="362.648" type="appl" />
         <tli id="T423" time="363.7382304693581" />
         <tli id="T424" time="365.734" type="appl" />
         <tli id="T425" time="367.563" type="appl" />
         <tli id="T426" time="369.393" type="appl" />
         <tli id="T427" time="370.451" type="appl" />
         <tli id="T428" time="371.192" type="appl" />
         <tli id="T429" time="372.23818911836435" />
         <tli id="T430" time="373.346" type="appl" />
         <tli id="T431" time="374.342" type="appl" />
         <tli id="T432" time="375.338" type="appl" />
         <tli id="T433" time="376.334" type="appl" />
         <tli id="T434" time="377.33" type="appl" />
         <tli id="T435" time="378.326" type="appl" />
         <tli id="T436" time="379.322" type="appl" />
         <tli id="T437" time="380.50481556916264" />
         <tli id="T438" time="381.9" type="appl" />
         <tli id="T439" time="383.60480048821205" />
         <tli id="T440" time="385.418" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T103" start="T96">
            <tli id="T96.tx.1" />
            <tli id="T96.tx.2" />
            <tli id="T96.tx.3" />
            <tli id="T96.tx.4" />
            <tli id="T96.tx.5" />
            <tli id="T96.tx.6" />
         </timeline-fork>
         <timeline-fork end="T113" start="T103">
            <tli id="T103.tx.1" />
            <tli id="T103.tx.2" />
            <tli id="T103.tx.3" />
            <tli id="T103.tx.4" />
            <tli id="T103.tx.5" />
            <tli id="T103.tx.6" />
            <tli id="T103.tx.7" />
            <tli id="T103.tx.8" />
            <tli id="T103.tx.9" />
         </timeline-fork>
         <timeline-fork end="T117" start="T113">
            <tli id="T113.tx.1" />
            <tli id="T113.tx.2" />
            <tli id="T113.tx.3" />
         </timeline-fork>
         <timeline-fork end="T127" start="T125">
            <tli id="T125.tx.1" />
         </timeline-fork>
         <timeline-fork end="T151" start="T140">
            <tli id="T140.tx.1" />
            <tli id="T140.tx.2" />
            <tli id="T140.tx.3" />
            <tli id="T140.tx.4" />
            <tli id="T140.tx.5" />
            <tli id="T140.tx.6" />
            <tli id="T140.tx.7" />
            <tli id="T140.tx.8" />
            <tli id="T140.tx.9" />
         </timeline-fork>
         <timeline-fork end="T160" start="T151">
            <tli id="T151.tx.1" />
            <tli id="T151.tx.2" />
            <tli id="T151.tx.3" />
            <tli id="T151.tx.4" />
            <tli id="T151.tx.5" />
            <tli id="T151.tx.6" />
            <tli id="T151.tx.7" />
            <tli id="T151.tx.8" />
         </timeline-fork>
         <timeline-fork end="T168" start="T160">
            <tli id="T160.tx.1" />
            <tli id="T160.tx.2" />
            <tli id="T160.tx.3" />
            <tli id="T160.tx.4" />
            <tli id="T160.tx.5" />
            <tli id="T160.tx.6" />
         </timeline-fork>
         <timeline-fork end="T185" start="T168">
            <tli id="T168.tx.1" />
            <tli id="T168.tx.2" />
            <tli id="T168.tx.3" />
            <tli id="T168.tx.4" />
            <tli id="T168.tx.5" />
            <tli id="T168.tx.6" />
            <tli id="T168.tx.7" />
            <tli id="T168.tx.8" />
            <tli id="T168.tx.9" />
            <tli id="T168.tx.10" />
            <tli id="T168.tx.11" />
            <tli id="T168.tx.12" />
            <tli id="T168.tx.13" />
            <tli id="T168.tx.14" />
            <tli id="T168.tx.15" />
            <tli id="T168.tx.16" />
         </timeline-fork>
         <timeline-fork end="T205" start="T185">
            <tli id="T185.tx.1" />
            <tli id="T185.tx.2" />
            <tli id="T185.tx.3" />
            <tli id="T185.tx.4" />
            <tli id="T185.tx.5" />
            <tli id="T185.tx.6" />
            <tli id="T185.tx.7" />
            <tli id="T185.tx.8" />
            <tli id="T185.tx.9" />
            <tli id="T185.tx.10" />
            <tli id="T185.tx.11" />
            <tli id="T185.tx.12" />
            <tli id="T185.tx.13" />
            <tli id="T185.tx.14" />
            <tli id="T185.tx.15" />
            <tli id="T185.tx.16" />
            <tli id="T185.tx.17" />
         </timeline-fork>
         <timeline-fork end="T260" start="T250">
            <tli id="T250.tx.1" />
            <tli id="T250.tx.2" />
            <tli id="T250.tx.3" />
            <tli id="T250.tx.4" />
            <tli id="T250.tx.5" />
            <tli id="T250.tx.6" />
            <tli id="T250.tx.7" />
            <tli id="T250.tx.8" />
            <tli id="T250.tx.9" />
         </timeline-fork>
         <timeline-fork end="T282" start="T277">
            <tli id="T277.tx.1" />
            <tli id="T277.tx.2" />
            <tli id="T277.tx.3" />
            <tli id="T277.tx.4" />
         </timeline-fork>
         <timeline-fork end="T287" start="T282">
            <tli id="T282.tx.1" />
            <tli id="T282.tx.2" />
            <tli id="T282.tx.3" />
            <tli id="T282.tx.4" />
         </timeline-fork>
         <timeline-fork end="T291" start="T287">
            <tli id="T287.tx.1" />
            <tli id="T287.tx.2" />
            <tli id="T287.tx.3" />
         </timeline-fork>
         <timeline-fork end="T332" start="T329">
            <tli id="T329.tx.1" />
            <tli id="T329.tx.2" />
            <tli id="T329.tx.3" />
         </timeline-fork>
         <timeline-fork end="T338" start="T332">
            <tli id="T332.tx.1" />
            <tli id="T332.tx.2" />
            <tli id="T332.tx.3" />
         </timeline-fork>
         <timeline-fork end="T351" start="T339">
            <tli id="T339.tx.1" />
            <tli id="T339.tx.2" />
            <tli id="T339.tx.3" />
            <tli id="T339.tx.4" />
            <tli id="T339.tx.5" />
            <tli id="T339.tx.6" />
            <tli id="T339.tx.7" />
            <tli id="T339.tx.8" />
            <tli id="T339.tx.9" />
            <tli id="T339.tx.10" />
         </timeline-fork>
         <timeline-fork end="T358" start="T351">
            <tli id="T351.tx.1" />
            <tli id="T351.tx.2" />
            <tli id="T351.tx.3" />
            <tli id="T351.tx.4" />
            <tli id="T351.tx.5" />
            <tli id="T351.tx.6" />
            <tli id="T351.tx.7" />
         </timeline-fork>
         <timeline-fork end="T361" start="T358">
            <tli id="T358.tx.1" />
            <tli id="T358.tx.2" />
            <tli id="T358.tx.3" />
         </timeline-fork>
         <timeline-fork end="T366" start="T361">
            <tli id="T361.tx.1" />
            <tli id="T361.tx.2" />
            <tli id="T361.tx.3" />
         </timeline-fork>
         <timeline-fork end="T375" start="T366">
            <tli id="T366.tx.1" />
            <tli id="T366.tx.2" />
            <tli id="T366.tx.3" />
            <tli id="T366.tx.4" />
            <tli id="T366.tx.5" />
            <tli id="T366.tx.6" />
            <tli id="T366.tx.7" />
            <tli id="T366.tx.8" />
         </timeline-fork>
         <timeline-fork end="T376" start="T375">
            <tli id="T375.tx.1" />
         </timeline-fork>
         <timeline-fork end="T411" start="T399">
            <tli id="T399.tx.1" />
            <tli id="T399.tx.2" />
            <tli id="T399.tx.3" />
            <tli id="T399.tx.4" />
            <tli id="T399.tx.5" />
            <tli id="T399.tx.6" />
            <tli id="T399.tx.7" />
            <tli id="T399.tx.8" />
            <tli id="T399.tx.9" />
            <tli id="T399.tx.10" />
            <tli id="T399.tx.11" />
         </timeline-fork>
         <timeline-fork end="T416" start="T411">
            <tli id="T411.tx.1" />
            <tli id="T411.tx.2" />
            <tli id="T411.tx.3" />
            <tli id="T411.tx.4" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T439" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Agafon</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Ivanovič</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šobi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">măna</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">dʼăbaktərzittə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Măn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">surarbiam:</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Kăde</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">šobial</ts>
                  <nts id="Seg_33" n="HIAT:ip">?</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Mašinazʼiʔ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">alʼi</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">nʼergölaʔ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">šobial</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">dĭn</ts>
                  <nts id="Seg_51" n="HIAT:ip">?</nts>
                  <nts id="Seg_52" n="HIAT:ip">"</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_55" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">Mămbi:</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_59" n="HIAT:ip">"</nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Nʼergölaʔ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">šobiam</ts>
                  <nts id="Seg_65" n="HIAT:ip">"</nts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Dĭgəttə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">măn</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">iʔgö</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">dĭ-</ts>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">dĭzʼiʔ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_87" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">dʼăbaktərnʼa-</ts>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_93" n="HIAT:w" s="T23">dʼăbaktərbiam</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">bostə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">šĭkətsi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_103" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">Iʔgö</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">katuškaʔi</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">bʼeʔ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">sumna</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_119" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">Dĭgəttə</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_123" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">šideŋ=</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">ši-</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">šidebiʔ=</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">šideŋ</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_137" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">ra-</ts>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_143" n="HIAT:w" s="T36">raz</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_146" n="HIAT:w" s="T37">ibi</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">măna</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">i</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_155" n="HIAT:w" s="T40">kalla</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T441">dʼürbi</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_162" n="HIAT:u" s="T41">
                  <nts id="Seg_163" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_165" n="HIAT:w" s="T41">Dĭgəttə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">măn=</ts>
                  <nts id="Seg_169" n="HIAT:ip">)</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">Dĭgəttə</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_174" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">mă-</ts>
                  <nts id="Seg_177" n="HIAT:ip">)</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">dĭ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">nörbəbi</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">dĭ</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_188" n="HIAT:ip">(</nts>
                  <ts e="T49" id="Seg_190" n="HIAT:w" s="T48">Арпату</ts>
                  <nts id="Seg_191" n="HIAT:ip">)</nts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_195" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">Dĭgəttə</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_199" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_201" n="HIAT:w" s="T50">dĭ=</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_205" n="HIAT:w" s="T51">dĭ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_208" n="HIAT:w" s="T52">măna</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">sazən</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">pʼaŋbi</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">măn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_221" n="HIAT:w" s="T56">dĭʔnə</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_224" n="HIAT:w" s="T57">tože</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">pʼaŋbiam</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_231" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_233" n="HIAT:w" s="T59">Šide</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_237" n="HIAT:w" s="T60">nagur</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_241" n="HIAT:w" s="T61">teʔtə</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_243" n="HIAT:ip">—</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_246" n="HIAT:w" s="T62">teʔtə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">kö</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_252" n="HIAT:w" s="T64">dĭ</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_255" n="HIAT:w" s="T65">măna</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">pʼaŋdəbi</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_262" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_264" n="HIAT:w" s="T67">Dĭgəttə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_266" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">kăštlə-</ts>
                  <nts id="Seg_269" n="HIAT:ip">)</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_272" n="HIAT:w" s="T69">kăštəbi</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_275" n="HIAT:w" s="T70">döbər</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_279" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_281" n="HIAT:w" s="T71">Dĭgəttə</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_284" n="HIAT:w" s="T72">măn</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_287" n="HIAT:w" s="T73">mămbiam:</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_289" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_291" n="HIAT:w" s="T74">Kamən</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_294" n="HIAT:w" s="T75">šoləj</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_297" n="HIAT:w" s="T76">Agafon</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_299" n="HIAT:ip">(</nts>
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">Iv-</ts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_306" n="HIAT:w" s="T78">dĭgəttə</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_309" n="HIAT:w" s="T79">šiʔnʼileʔ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_311" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_313" n="HIAT:w" s="T80">šo-</ts>
                  <nts id="Seg_314" n="HIAT:ip">)</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_317" n="HIAT:w" s="T81">šolam</ts>
                  <nts id="Seg_318" n="HIAT:ip">"</nts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_322" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_324" n="HIAT:w" s="T82">Di</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_327" n="HIAT:w" s="T83">bos-</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_330" n="HIAT:w" s="T84">dĭgəttə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_333" n="HIAT:w" s="T85">dĭ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_336" n="HIAT:w" s="T86">bostə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">šobi</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_343" n="HIAT:w" s="T88">măna</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_346" n="HIAT:w" s="T89">ibi</ts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_350" n="HIAT:w" s="T90">nʼergöleʔ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_353" n="HIAT:w" s="T91">šobibaʔ</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_357" n="HIAT:w" s="T92">mašinazʼiʔ</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_361" n="HIAT:u" s="T93">
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_364" n="HIAT:w" s="T93">Мы</ts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_369" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_371" n="HIAT:w" s="T94">Mašinazʼiʔ</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_374" n="HIAT:w" s="T95">šobibaʔ</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_378" n="HIAT:u" s="T96">
                  <ts e="T96.tx.1" id="Seg_380" n="HIAT:w" s="T96">Агафон</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx.2" id="Seg_383" n="HIAT:w" s="T96.tx.1">Иванович</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx.3" id="Seg_386" n="HIAT:w" s="T96.tx.2">два</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx.4" id="Seg_389" n="HIAT:w" s="T96.tx.3">раз</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx.5" id="Seg_392" n="HIAT:w" s="T96.tx.4">ко</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96.tx.6" id="Seg_395" n="HIAT:w" s="T96.tx.5">мне</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_398" n="HIAT:w" s="T96.tx.6">приезжал</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_402" n="HIAT:u" s="T103">
                  <ts e="T103.tx.1" id="Seg_404" n="HIAT:w" s="T103">Я</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.2" id="Seg_407" n="HIAT:w" s="T103.tx.1">спрашивала:</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_409" n="HIAT:ip">"</nts>
                  <ts e="T103.tx.3" id="Seg_411" n="HIAT:w" s="T103.tx.2">Как</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.4" id="Seg_414" n="HIAT:w" s="T103.tx.3">приехал</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.5" id="Seg_418" n="HIAT:w" s="T103.tx.4">на</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.6" id="Seg_421" n="HIAT:w" s="T103.tx.5">машине</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.7" id="Seg_424" n="HIAT:w" s="T103.tx.6">али</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.8" id="Seg_427" n="HIAT:w" s="T103.tx.7">на</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103.tx.9" id="Seg_430" n="HIAT:w" s="T103.tx.8">самолёте</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_433" n="HIAT:w" s="T103.tx.9">летел</ts>
                  <nts id="Seg_434" n="HIAT:ip">?</nts>
                  <nts id="Seg_435" n="HIAT:ip">"</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_438" n="HIAT:u" s="T113">
                  <ts e="T113.tx.1" id="Seg_440" n="HIAT:w" s="T113">Он</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113.tx.2" id="Seg_443" n="HIAT:w" s="T113.tx.1">сказал:</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_445" n="HIAT:ip">"</nts>
                  <ts e="T113.tx.3" id="Seg_447" n="HIAT:w" s="T113.tx.2">На</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_450" n="HIAT:w" s="T113.tx.3">самолёте</ts>
                  <nts id="Seg_451" n="HIAT:ip">"</nts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_455" n="HIAT:u" s="T117">
                  <nts id="Seg_456" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_458" n="HIAT:w" s="T117">Tогда</ts>
                  <nts id="Seg_459" n="HIAT:ip">)</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_462" n="HIAT:w" s="T118">Тогда</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_465" n="HIAT:w" s="T119">он</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_468" n="HIAT:w" s="T120">два</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_471" n="HIAT:w" s="T121">раза</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_474" n="HIAT:w" s="T122">был</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_477" n="HIAT:w" s="T123">у</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_480" n="HIAT:w" s="T124">меня</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_484" n="HIAT:u" s="T125">
                  <ts e="T125.tx.1" id="Seg_486" n="HIAT:w" s="T125">И</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_489" n="HIAT:w" s="T125.tx.1">уехал</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_493" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_495" n="HIAT:w" s="T127">Господи</ts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_499" n="HIAT:w" s="T128">опять</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_502" n="HIAT:w" s="T129">не</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_505" n="HIAT:w" s="T130">забыть</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_508" n="HIAT:w" s="T131">бы</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_510" n="HIAT:ip">(</nts>
                  <nts id="Seg_511" n="HIAT:ip">(</nts>
                  <ats e="T133" id="Seg_512" n="HIAT:non-pho" s="T132">DMG</ats>
                  <nts id="Seg_513" n="HIAT:ip">)</nts>
                  <nts id="Seg_514" n="HIAT:ip">)</nts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_518" n="HIAT:u" s="T133">
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ts e="T134" id="Seg_521" n="HIAT:w" s="T133">И</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_524" n="HIAT:w" s="T134">у</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_527" n="HIAT:w" s="T135">-</ts>
                  <nts id="Seg_528" n="HIAT:ip">)</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_531" n="HIAT:w" s="T136">И</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_534" n="HIAT:w" s="T137">уехал</ts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_538" n="HIAT:w" s="T138">и</ts>
                  <nts id="Seg_539" n="HIAT:ip">…</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_542" n="HIAT:u" s="T140">
                  <ts e="T140.tx.1" id="Seg_544" n="HIAT:w" s="T140">А</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.2" id="Seg_547" n="HIAT:w" s="T140.tx.1">Арпиту</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.3" id="Seg_550" n="HIAT:w" s="T140.tx.2">сказал</ts>
                  <nts id="Seg_551" n="HIAT:ip">,</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.4" id="Seg_554" n="HIAT:w" s="T140.tx.3">что</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.5" id="Seg_557" n="HIAT:w" s="T140.tx.4">там</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.6" id="Seg_560" n="HIAT:w" s="T140.tx.5">женщина</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.7" id="Seg_564" n="HIAT:w" s="T140.tx.6">которая</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_566" n="HIAT:ip">(</nts>
                  <ts e="T140.tx.8" id="Seg_568" n="HIAT:w" s="T140.tx.7">ра-</ts>
                  <nts id="Seg_569" n="HIAT:ip">)</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140.tx.9" id="Seg_572" n="HIAT:w" s="T140.tx.8">умеет</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_575" n="HIAT:w" s="T140.tx.9">разговаривать</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_579" n="HIAT:u" s="T151">
                  <ts e="T151.tx.1" id="Seg_581" n="HIAT:w" s="T151">Он</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.2" id="Seg_584" n="HIAT:w" s="T151.tx.1">давай</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.3" id="Seg_587" n="HIAT:w" s="T151.tx.2">мне</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.4" id="Seg_590" n="HIAT:w" s="T151.tx.3">письма</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.5" id="Seg_593" n="HIAT:w" s="T151.tx.4">писать</ts>
                  <nts id="Seg_594" n="HIAT:ip">,</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.6" id="Seg_597" n="HIAT:w" s="T151.tx.5">четыре</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.7" id="Seg_600" n="HIAT:w" s="T151.tx.6">года</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151.tx.8" id="Seg_603" n="HIAT:w" s="T151.tx.7">списывалися</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_606" n="HIAT:w" s="T151.tx.8">мы</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_610" n="HIAT:u" s="T160">
                  <ts e="T160.tx.1" id="Seg_612" n="HIAT:w" s="T160">Тогда</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_614" n="HIAT:ip">(</nts>
                  <ts e="T160.tx.2" id="Seg_616" n="HIAT:w" s="T160.tx.1">на-</ts>
                  <nts id="Seg_617" n="HIAT:ip">)</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160.tx.3" id="Seg_620" n="HIAT:w" s="T160.tx.2">звал</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160.tx.4" id="Seg_623" n="HIAT:w" s="T160.tx.3">мне</ts>
                  <nts id="Seg_624" n="HIAT:ip">,</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160.tx.5" id="Seg_627" n="HIAT:w" s="T160.tx.4">чтоб</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160.tx.6" id="Seg_630" n="HIAT:w" s="T160.tx.5">я</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_633" n="HIAT:w" s="T160.tx.6">приехала</ts>
                  <nts id="Seg_634" n="HIAT:ip">.</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_637" n="HIAT:u" s="T168">
                  <ts e="T168.tx.1" id="Seg_639" n="HIAT:w" s="T168">Но</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.2" id="Seg_642" n="HIAT:w" s="T168.tx.1">я</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.3" id="Seg_645" n="HIAT:w" s="T168.tx.2">написала</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.4" id="Seg_648" n="HIAT:w" s="T168.tx.3">ему</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.5" id="Seg_651" n="HIAT:w" s="T168.tx.4">что</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.6" id="Seg_654" n="HIAT:w" s="T168.tx.5">я</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.7" id="Seg_657" n="HIAT:w" s="T168.tx.6">не</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.8" id="Seg_660" n="HIAT:w" s="T168.tx.7">могу</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.9" id="Seg_663" n="HIAT:w" s="T168.tx.8">одна</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.10" id="Seg_666" n="HIAT:w" s="T168.tx.9">приехать</ts>
                  <nts id="Seg_667" n="HIAT:ip">,</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.11" id="Seg_670" n="HIAT:w" s="T168.tx.10">когда</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.12" id="Seg_673" n="HIAT:w" s="T168.tx.11">приедет</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.13" id="Seg_676" n="HIAT:w" s="T168.tx.12">Агафон</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.14" id="Seg_679" n="HIAT:w" s="T168.tx.13">Иванович</ts>
                  <nts id="Seg_680" n="HIAT:ip">,</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.15" id="Seg_683" n="HIAT:w" s="T168.tx.14">тогда</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168.tx.16" id="Seg_686" n="HIAT:w" s="T168.tx.15">я</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_689" n="HIAT:w" s="T168.tx.16">приеду</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_693" n="HIAT:u" s="T185">
                  <ts e="T185.tx.1" id="Seg_695" n="HIAT:w" s="T185">Он</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.2" id="Seg_698" n="HIAT:w" s="T185.tx.1">собрался</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.3" id="Seg_701" n="HIAT:w" s="T185.tx.2">и</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.4" id="Seg_704" n="HIAT:w" s="T185.tx.3">сам</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.5" id="Seg_707" n="HIAT:w" s="T185.tx.4">приехал</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.6" id="Seg_710" n="HIAT:w" s="T185.tx.5">и</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_712" n="HIAT:ip">(</nts>
                  <nts id="Seg_713" n="HIAT:ip">(</nts>
                  <ats e="T185.tx.7" id="Seg_714" n="HIAT:non-pho" s="T185.tx.6">PAUSE</ats>
                  <nts id="Seg_715" n="HIAT:ip">)</nts>
                  <nts id="Seg_716" n="HIAT:ip">)</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.8" id="Seg_719" n="HIAT:w" s="T185.tx.7">мы</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.9" id="Seg_722" n="HIAT:w" s="T185.tx.8">летели</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.10" id="Seg_725" n="HIAT:w" s="T185.tx.9">на</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.11" id="Seg_728" n="HIAT:w" s="T185.tx.10">самолёте</ts>
                  <nts id="Seg_729" n="HIAT:ip">,</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.12" id="Seg_732" n="HIAT:w" s="T185.tx.11">и</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.13" id="Seg_735" n="HIAT:w" s="T185.tx.12">на</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.14" id="Seg_738" n="HIAT:w" s="T185.tx.13">машине</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_740" n="HIAT:ip">(</nts>
                  <ts e="T185.tx.15" id="Seg_742" n="HIAT:w" s="T185.tx.14">прилете-</ts>
                  <nts id="Seg_743" n="HIAT:ip">,</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.16" id="Seg_746" n="HIAT:w" s="T185.tx.15">при-</ts>
                  <nts id="Seg_747" n="HIAT:ip">)</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185.tx.17" id="Seg_750" n="HIAT:w" s="T185.tx.16">приехали</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_753" n="HIAT:w" s="T185.tx.17">сюда</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_757" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_759" n="HIAT:w" s="T205">Чего</ts>
                  <nts id="Seg_760" n="HIAT:ip">?</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_763" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_765" n="HIAT:w" s="T206">Уже</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_768" n="HIAT:w" s="T207">говорить</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_771" n="HIAT:w" s="T208">можно</ts>
                  <nts id="Seg_772" n="HIAT:ip">?</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_775" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_777" n="HIAT:w" s="T209">Dĭgəttə</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_779" n="HIAT:ip">(</nts>
                  <ts e="T211" id="Seg_781" n="HIAT:w" s="T210">mɨ-</ts>
                  <nts id="Seg_782" n="HIAT:ip">)</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_785" n="HIAT:w" s="T211">măn</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_788" n="HIAT:w" s="T212">šobiam</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_792" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_794" n="HIAT:w" s="T213">Măn</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_796" n="HIAT:ip">(</nts>
                  <ts e="T215" id="Seg_798" n="HIAT:w" s="T214">amnolbi-</ts>
                  <nts id="Seg_799" n="HIAT:ip">)</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_802" n="HIAT:w" s="T215">nuldəbiʔi</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_805" n="HIAT:w" s="T216">dö</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_808" n="HIAT:w" s="T217">turanə</ts>
                  <nts id="Seg_809" n="HIAT:ip">,</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_812" n="HIAT:w" s="T218">Ninanə</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_816" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_818" n="HIAT:w" s="T219">Dĭgəttə</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_821" n="HIAT:w" s="T220">măn</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_824" n="HIAT:w" s="T221">mĭmbiem</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_828" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_830" n="HIAT:w" s="T222">Šobiʔi</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_833" n="HIAT:w" s="T223">măna</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_835" n="HIAT:ip">(</nts>
                  <ts e="T225" id="Seg_837" n="HIAT:w" s="T224">na-</ts>
                  <nts id="Seg_838" n="HIAT:ip">)</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_841" n="HIAT:w" s="T225">šide</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_844" n="HIAT:w" s="T226">kuza</ts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_848" n="HIAT:w" s="T227">dĭgəttə</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_851" n="HIAT:w" s="T228">mašinanə</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">(</nts>
                  <ts e="T230" id="Seg_855" n="HIAT:w" s="T229">amnomgubiʔi</ts>
                  <nts id="Seg_856" n="HIAT:ip">)</nts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_860" n="HIAT:w" s="T230">kunnaːmbiʔi</ts>
                  <nts id="Seg_861" n="HIAT:ip">.</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_864" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_866" n="HIAT:w" s="T231">Kuŋge</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_869" n="HIAT:ip">(</nts>
                  <ts e="T233" id="Seg_871" n="HIAT:w" s="T232">s-</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_874" n="HIAT:w" s="T233">go-</ts>
                  <nts id="Seg_875" n="HIAT:ip">)</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_877" n="HIAT:ip">(</nts>
                  <ts e="T235" id="Seg_879" n="HIAT:w" s="T234">за</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_882" n="HIAT:w" s="T235">город</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_885" n="HIAT:w" s="T236">dĭ</ts>
                  <nts id="Seg_886" n="HIAT:ip">)</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_889" n="HIAT:w" s="T237">gijen</ts>
                  <nts id="Seg_890" n="HIAT:ip">…</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_893" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_895" n="HIAT:w" s="T238">Šindidə</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_898" n="HIAT:w" s="T239">naga</ts>
                  <nts id="Seg_899" n="HIAT:ip">,</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_902" n="HIAT:w" s="T240">dĭn</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_905" n="HIAT:w" s="T241">bar</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_908" n="HIAT:w" s="T242">keʔbdeʔi</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_911" n="HIAT:w" s="T243">ambiam</ts>
                  <nts id="Seg_912" n="HIAT:ip">.</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_915" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_917" n="HIAT:w" s="T244">Dĭgəttə</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_920" n="HIAT:w" s="T245">dĭn</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_923" n="HIAT:w" s="T246">dʼăbaktərbiam</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ts e="T248" id="Seg_927" n="HIAT:w" s="T247">dĭ-</ts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_932" n="HIAT:u" s="T248">
                  <ts e="T250" id="Seg_934" n="HIAT:w" s="T248">Dĭgəttə</ts>
                  <nts id="Seg_935" n="HIAT:ip">…</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_938" n="HIAT:u" s="T250">
                  <ts e="T250.tx.1" id="Seg_940" n="HIAT:w" s="T250">Когда</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.2" id="Seg_943" n="HIAT:w" s="T250.tx.1">привёз</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.3" id="Seg_946" n="HIAT:w" s="T250.tx.2">меня</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.4" id="Seg_949" n="HIAT:w" s="T250.tx.3">сюда</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.5" id="Seg_952" n="HIAT:w" s="T250.tx.4">и</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.6" id="Seg_955" n="HIAT:w" s="T250.tx.5">поставил</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.7" id="Seg_958" n="HIAT:w" s="T250.tx.6">к</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.8" id="Seg_961" n="HIAT:w" s="T250.tx.7">Нине</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250.tx.9" id="Seg_964" n="HIAT:w" s="T250.tx.8">на</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_967" n="HIAT:w" s="T250.tx.9">фатеру</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_971" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_973" n="HIAT:w" s="T260">Так</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_977" n="HIAT:w" s="T261">потом</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_980" n="HIAT:w" s="T262">пришли</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_983" n="HIAT:w" s="T263">двое</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_986" n="HIAT:w" s="T264">и</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_989" n="HIAT:w" s="T265">третий</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_992" n="HIAT:w" s="T266">и</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_995" n="HIAT:w" s="T267">посадили</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_998" n="HIAT:w" s="T268">меня</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1001" n="HIAT:w" s="T269">на</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1004" n="HIAT:w" s="T270">машину</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1007" n="HIAT:w" s="T271">и</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1010" n="HIAT:w" s="T272">ездили</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1012" n="HIAT:ip">(</nts>
                  <ts e="T274" id="Seg_1014" n="HIAT:w" s="T273">за=</ts>
                  <nts id="Seg_1015" n="HIAT:ip">)</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1018" n="HIAT:w" s="T274">за</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1021" n="HIAT:w" s="T275">город</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1024" n="HIAT:w" s="T276">далёко</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1028" n="HIAT:u" s="T277">
                  <ts e="T277.tx.1" id="Seg_1030" n="HIAT:w" s="T277">И</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277.tx.2" id="Seg_1033" n="HIAT:w" s="T277.tx.1">там</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277.tx.3" id="Seg_1036" n="HIAT:w" s="T277.tx.2">я</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277.tx.4" id="Seg_1039" n="HIAT:w" s="T277.tx.3">ягоды</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1042" n="HIAT:w" s="T277.tx.4">ела</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1046" n="HIAT:u" s="T282">
                  <ts e="T282.tx.1" id="Seg_1048" n="HIAT:w" s="T282">И</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.2" id="Seg_1051" n="HIAT:w" s="T282.tx.1">разговаривала</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.3" id="Seg_1054" n="HIAT:w" s="T282.tx.2">с</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.4" id="Seg_1057" n="HIAT:w" s="T282.tx.3">ними</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1059" n="HIAT:ip">(</nts>
                  <nts id="Seg_1060" n="HIAT:ip">(</nts>
                  <ats e="T287" id="Seg_1061" n="HIAT:non-pho" s="T282.tx.4">…</ats>
                  <nts id="Seg_1062" n="HIAT:ip">)</nts>
                  <nts id="Seg_1063" n="HIAT:ip">)</nts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1067" n="HIAT:u" s="T287">
                  <ts e="T287.tx.1" id="Seg_1069" n="HIAT:w" s="T287">Больше</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1071" n="HIAT:ip">(</nts>
                  <nts id="Seg_1072" n="HIAT:ip">(</nts>
                  <ats e="T287.tx.2" id="Seg_1073" n="HIAT:non-pho" s="T287.tx.1">…</ats>
                  <nts id="Seg_1074" n="HIAT:ip">)</nts>
                  <nts id="Seg_1075" n="HIAT:ip">)</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287.tx.3" id="Seg_1078" n="HIAT:w" s="T287.tx.2">всё</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1081" n="HIAT:w" s="T287.tx.3">наверно</ts>
                  <nts id="Seg_1082" n="HIAT:ip">.</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1085" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1087" n="HIAT:w" s="T291">Dĭgəttə</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1090" n="HIAT:w" s="T292">nörbələm</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1093" n="HIAT:w" s="T293">girgit</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1095" n="HIAT:ip">(</nts>
                  <ts e="T295" id="Seg_1097" n="HIAT:w" s="T294">m-</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1100" n="HIAT:w" s="T295">m-</ts>
                  <nts id="Seg_1101" n="HIAT:ip">)</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1104" n="HIAT:w" s="T296">miʔ</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1106" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1108" n="HIAT:w" s="T297">dʼüm=</ts>
                  <nts id="Seg_1109" n="HIAT:ip">)</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1112" n="HIAT:w" s="T298">dʼüm</ts>
                  <nts id="Seg_1113" n="HIAT:ip">.</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1116" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1118" n="HIAT:w" s="T299">Dĭn</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1121" n="HIAT:w" s="T300">iʔgö</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1124" n="HIAT:w" s="T301">keʔbde</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1127" n="HIAT:w" s="T302">ige</ts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1131" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1133" n="HIAT:w" s="T303">I</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1136" n="HIAT:w" s="T304">paʔi</ts>
                  <nts id="Seg_1137" n="HIAT:ip">,</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1140" n="HIAT:w" s="T305">sĭri</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1143" n="HIAT:w" s="T306">i</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1146" n="HIAT:w" s="T307">köməʔi</ts>
                  <nts id="Seg_1147" n="HIAT:ip">,</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1150" n="HIAT:w" s="T308">ăsinaʔi</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1154" n="HIAT:w" s="T309">kedrʔi</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1157" n="HIAT:w" s="T310">ige</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1160" n="HIAT:w" s="T311">pa</ts>
                  <nts id="Seg_1161" n="HIAT:ip">,</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1164" n="HIAT:w" s="T312">gijen</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1167" n="HIAT:w" s="T313">sanə</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1170" n="HIAT:w" s="T314">özerbəlaʔbə</ts>
                  <nts id="Seg_1171" n="HIAT:ip">.</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1174" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1176" n="HIAT:w" s="T315">Малина</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1179" n="HIAT:w" s="T316">ige</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1183" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1185" n="HIAT:w" s="T317">Dĭgəttə</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1188" n="HIAT:w" s="T318">dĭn</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1191" n="HIAT:w" s="T319">sʼtʼepʔi</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1194" n="HIAT:w" s="T320">ige</ts>
                  <nts id="Seg_1195" n="HIAT:ip">.</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1198" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1200" n="HIAT:w" s="T321">Iʔgö</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1203" n="HIAT:w" s="T322">ipek</ts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1207" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1209" n="HIAT:w" s="T323">Budəj</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1212" n="HIAT:w" s="T324">ipek</ts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1215" n="HIAT:ip">(</nts>
                  <ts e="T326" id="Seg_1217" n="HIAT:w" s="T325">su-</ts>
                  <nts id="Seg_1218" n="HIAT:ip">)</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1221" n="HIAT:w" s="T326">sulu</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1224" n="HIAT:w" s="T327">i</ts>
                  <nts id="Seg_1225" n="HIAT:ip">…</nts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1228" n="HIAT:u" s="T329">
                  <ts e="T329.tx.1" id="Seg_1230" n="HIAT:w" s="T329">У</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329.tx.2" id="Seg_1233" n="HIAT:w" s="T329.tx.1">нас</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329.tx.3" id="Seg_1236" n="HIAT:w" s="T329.tx.2">там</ts>
                  <nts id="Seg_1237" n="HIAT:ip">(</nts>
                  <nts id="Seg_1238" n="HIAT:ip">(</nts>
                  <ats e="T332" id="Seg_1239" n="HIAT:non-pho" s="T329.tx.3">…</ats>
                  <nts id="Seg_1240" n="HIAT:ip">)</nts>
                  <nts id="Seg_1241" n="HIAT:ip">)</nts>
                  <nts id="Seg_1242" n="HIAT:ip">…</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1245" n="HIAT:u" s="T332">
                  <ts e="T332.tx.1" id="Seg_1247" n="HIAT:w" s="T332">у</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332.tx.2" id="Seg_1250" n="HIAT:w" s="T332.tx.1">нас</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332.tx.3" id="Seg_1253" n="HIAT:w" s="T332.tx.2">там</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1256" n="HIAT:w" s="T332.tx.3">лес</ts>
                  <nts id="Seg_1257" n="HIAT:ip">.</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1260" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1262" n="HIAT:w" s="T338">Берёзы</ts>
                  <nts id="Seg_1263" n="HIAT:ip">.</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339.tx.10" id="Seg_1266" n="HIAT:u" s="T339">
                  <ts e="T339.tx.1" id="Seg_1268" n="HIAT:w" s="T339">И</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.2" id="Seg_1271" n="HIAT:w" s="T339.tx.1">листвяги</ts>
                  <nts id="Seg_1272" n="HIAT:ip">,</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.3" id="Seg_1275" n="HIAT:w" s="T339.tx.2">как</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.4" id="Seg_1278" n="HIAT:w" s="T339.tx.3">я</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.5" id="Seg_1281" n="HIAT:w" s="T339.tx.4">их</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.6" id="Seg_1284" n="HIAT:w" s="T339.tx.5">называю</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.7" id="Seg_1287" n="HIAT:w" s="T339.tx.6">красными</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.8" id="Seg_1290" n="HIAT:w" s="T339.tx.7">по-своему</ts>
                  <nts id="Seg_1291" n="HIAT:ip">,</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.9" id="Seg_1294" n="HIAT:w" s="T339.tx.8">листвяги</ts>
                  <nts id="Seg_1295" n="HIAT:ip">,</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339.tx.10" id="Seg_1298" n="HIAT:w" s="T339.tx.9">осины</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1302" n="HIAT:u" s="T339.tx.10">
                  <nts id="Seg_1303" n="HIAT:ip">(</nts>
                  <nts id="Seg_1304" n="HIAT:ip">(</nts>
                  <ats e="T351" id="Seg_1305" n="HIAT:non-pho" s="T339.tx.10">DMG</ats>
                  <nts id="Seg_1306" n="HIAT:ip">)</nts>
                  <nts id="Seg_1307" n="HIAT:ip">)</nts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351.tx.7" id="Seg_1311" n="HIAT:u" s="T351">
                  <ts e="T351.tx.1" id="Seg_1313" n="HIAT:w" s="T351">И</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.2" id="Seg_1316" n="HIAT:w" s="T351.tx.1">кедры</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.3" id="Seg_1319" n="HIAT:w" s="T351.tx.2">на</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.4" id="Seg_1322" n="HIAT:w" s="T351.tx.3">которых</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.5" id="Seg_1325" n="HIAT:w" s="T351.tx.4">растут</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.6" id="Seg_1328" n="HIAT:w" s="T351.tx.5">орехи</ts>
                  <nts id="Seg_1329" n="HIAT:ip">,</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351.tx.7" id="Seg_1332" n="HIAT:w" s="T351.tx.6">шишки</ts>
                  <nts id="Seg_1333" n="HIAT:ip">.</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1336" n="HIAT:u" s="T351.tx.7">
                  <nts id="Seg_1337" n="HIAT:ip">(</nts>
                  <nts id="Seg_1338" n="HIAT:ip">(</nts>
                  <ats e="T358" id="Seg_1339" n="HIAT:non-pho" s="T351.tx.7">DMG</ats>
                  <nts id="Seg_1340" n="HIAT:ip">)</nts>
                  <nts id="Seg_1341" n="HIAT:ip">)</nts>
                  <nts id="Seg_1342" n="HIAT:ip">.</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358.tx.3" id="Seg_1345" n="HIAT:u" s="T358">
                  <ts e="T358.tx.1" id="Seg_1347" n="HIAT:w" s="T358">И</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358.tx.2" id="Seg_1350" n="HIAT:w" s="T358.tx.1">ягоды</ts>
                  <nts id="Seg_1351" n="HIAT:ip">,</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358.tx.3" id="Seg_1354" n="HIAT:w" s="T358.tx.2">малина</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1358" n="HIAT:u" s="T358.tx.3">
                  <nts id="Seg_1359" n="HIAT:ip">(</nts>
                  <nts id="Seg_1360" n="HIAT:ip">(</nts>
                  <ats e="T361" id="Seg_1361" n="HIAT:non-pho" s="T358.tx.3">DMG</ats>
                  <nts id="Seg_1362" n="HIAT:ip">)</nts>
                  <nts id="Seg_1363" n="HIAT:ip">)</nts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1367" n="HIAT:u" s="T361">
                  <ts e="T361.tx.1" id="Seg_1369" n="HIAT:w" s="T361">И</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1371" n="HIAT:ip">(</nts>
                  <ts e="T361.tx.2" id="Seg_1373" n="HIAT:w" s="T361.tx.1">с-</ts>
                  <nts id="Seg_1374" n="HIAT:ip">)</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361.tx.3" id="Seg_1377" n="HIAT:w" s="T361.tx.2">и</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1380" n="HIAT:w" s="T361.tx.3">степь</ts>
                  <nts id="Seg_1381" n="HIAT:ip">.</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366.tx.8" id="Seg_1384" n="HIAT:u" s="T366">
                  <ts e="T366.tx.1" id="Seg_1386" n="HIAT:w" s="T366">На</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1388" n="HIAT:ip">(</nts>
                  <ts e="T366.tx.2" id="Seg_1390" n="HIAT:w" s="T366.tx.1">пас-</ts>
                  <nts id="Seg_1391" n="HIAT:ip">)</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.3" id="Seg_1394" n="HIAT:w" s="T366.tx.2">пшеничный</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.4" id="Seg_1397" n="HIAT:w" s="T366.tx.3">хлеб</ts>
                  <nts id="Seg_1398" n="HIAT:ip">,</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.5" id="Seg_1401" n="HIAT:w" s="T366.tx.4">и</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.6" id="Seg_1404" n="HIAT:w" s="T366.tx.5">овёс</ts>
                  <nts id="Seg_1405" n="HIAT:ip">,</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.7" id="Seg_1408" n="HIAT:w" s="T366.tx.6">и</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366.tx.8" id="Seg_1411" n="HIAT:w" s="T366.tx.7">гречуха</ts>
                  <nts id="Seg_1412" n="HIAT:ip">.</nts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1415" n="HIAT:u" s="T366.tx.8">
                  <nts id="Seg_1416" n="HIAT:ip">(</nts>
                  <nts id="Seg_1417" n="HIAT:ip">(</nts>
                  <ats e="T375" id="Seg_1418" n="HIAT:non-pho" s="T366.tx.8">DMG</ats>
                  <nts id="Seg_1419" n="HIAT:ip">)</nts>
                  <nts id="Seg_1420" n="HIAT:ip">)</nts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375.tx.1" id="Seg_1424" n="HIAT:u" s="T375">
                  <ts e="T375.tx.1" id="Seg_1426" n="HIAT:w" s="T375">Так</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1430" n="HIAT:u" s="T375.tx.1">
                  <nts id="Seg_1431" n="HIAT:ip">(</nts>
                  <nts id="Seg_1432" n="HIAT:ip">(</nts>
                  <ats e="T376" id="Seg_1433" n="HIAT:non-pho" s="T375.tx.1">DMG</ats>
                  <nts id="Seg_1434" n="HIAT:ip">)</nts>
                  <nts id="Seg_1435" n="HIAT:ip">)</nts>
                  <nts id="Seg_1436" n="HIAT:ip">.</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1439" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1441" n="HIAT:w" s="T376">Kamən</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1444" n="HIAT:w" s="T377">ălʼenʼəʔi</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1447" n="HIAT:w" s="T378">ibiʔi</ts>
                  <nts id="Seg_1448" n="HIAT:ip">,</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1450" n="HIAT:ip">(</nts>
                  <ts e="T380" id="Seg_1452" n="HIAT:w" s="T379">dĭ=</ts>
                  <nts id="Seg_1453" n="HIAT:ip">)</nts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1456" n="HIAT:w" s="T380">dĭzeŋ</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1458" n="HIAT:ip">(</nts>
                  <ts e="T382" id="Seg_1460" n="HIAT:w" s="T381">əz-</ts>
                  <nts id="Seg_1461" n="HIAT:ip">)</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1464" n="HIAT:w" s="T382">ĭzembiʔi</ts>
                  <nts id="Seg_1465" n="HIAT:ip">,</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1468" n="HIAT:w" s="T383">dĭzeŋ</ts>
                  <nts id="Seg_1469" n="HIAT:ip">,</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1472" n="HIAT:w" s="T384">dĭgəttə</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1475" n="HIAT:w" s="T385">dĭzeŋ</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1478" n="HIAT:w" s="T386">öʔlübiʔi</ts>
                  <nts id="Seg_1479" n="HIAT:ip">.</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1482" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1484" n="HIAT:w" s="T387">Dĭzeŋ</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1486" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1488" n="HIAT:w" s="T388">i</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1491" n="HIAT:w" s="T389">lestə</ts>
                  <nts id="Seg_1492" n="HIAT:ip">)</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1495" n="HIAT:w" s="T390">kambiʔi</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1499" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1501" n="HIAT:w" s="T391">Iam</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1504" n="HIAT:w" s="T392">nörbəbi</ts>
                  <nts id="Seg_1505" n="HIAT:ip">,</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1508" n="HIAT:w" s="T393">urgajam</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1511" n="HIAT:w" s="T394">mămbi</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1514" n="HIAT:w" s="T395">măna</ts>
                  <nts id="Seg_1515" n="HIAT:ip">,</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1518" n="HIAT:w" s="T396">iššo</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1521" n="HIAT:w" s="T397">üdʼüge</ts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1524" n="HIAT:w" s="T398">ibiem</ts>
                  <nts id="Seg_1525" n="HIAT:ip">.</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1528" n="HIAT:u" s="T399">
                  <ts e="T399.tx.1" id="Seg_1530" n="HIAT:w" s="T399">Когда</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.2" id="Seg_1533" n="HIAT:w" s="T399.tx.1">на</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.3" id="Seg_1536" n="HIAT:w" s="T399.tx.2">оленях</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.4" id="Seg_1539" n="HIAT:w" s="T399.tx.3">ездили</ts>
                  <nts id="Seg_1540" n="HIAT:ip">,</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.5" id="Seg_1543" n="HIAT:w" s="T399.tx.4">они</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.6" id="Seg_1546" n="HIAT:w" s="T399.tx.5">стали</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.7" id="Seg_1549" n="HIAT:w" s="T399.tx.6">паршиветь</ts>
                  <nts id="Seg_1550" n="HIAT:ip">,</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.8" id="Seg_1553" n="HIAT:w" s="T399.tx.7">и</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.9" id="Seg_1556" n="HIAT:w" s="T399.tx.8">взяли</ts>
                  <nts id="Seg_1557" n="HIAT:ip">,</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.10" id="Seg_1560" n="HIAT:w" s="T399.tx.9">отпустили</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399.tx.11" id="Seg_1563" n="HIAT:w" s="T399.tx.10">в</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1566" n="HIAT:w" s="T399.tx.11">лес</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1570" n="HIAT:u" s="T411">
                  <ts e="T411.tx.1" id="Seg_1572" n="HIAT:w" s="T411">И</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411.tx.2" id="Seg_1575" n="HIAT:w" s="T411.tx.1">бабушка</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411.tx.3" id="Seg_1578" n="HIAT:w" s="T411.tx.2">говорила</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411.tx.4" id="Seg_1581" n="HIAT:w" s="T411.tx.3">и</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1584" n="HIAT:w" s="T411.tx.4">мама</ts>
                  <nts id="Seg_1585" n="HIAT:ip">.</nts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1588" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1590" n="HIAT:w" s="T416">Dĭgəttə</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1593" n="HIAT:w" s="T417">dĭzeŋ</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1596" n="HIAT:w" s="T418">mĭmbiʔi</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1599" n="HIAT:w" s="T419">dʼijenə</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1603" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1605" n="HIAT:w" s="T420">Urgo</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1608" n="HIAT:w" s="T421">măjanə</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1612" n="HIAT:w" s="T422">Belăgorjanə</ts>
                  <nts id="Seg_1613" n="HIAT:ip">.</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1616" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1618" n="HIAT:w" s="T423">Gijen</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1621" n="HIAT:w" s="T424">iʔgö</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1624" n="HIAT:w" s="T425">bü</ts>
                  <nts id="Seg_1625" n="HIAT:ip">.</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_1628" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1630" n="HIAT:w" s="T426">Dĭn</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1633" n="HIAT:w" s="T427">kola</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1636" n="HIAT:w" s="T428">dʼaʔpiʔi</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1640" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1642" n="HIAT:w" s="T429">I</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1644" n="HIAT:ip">(</nts>
                  <ts e="T431" id="Seg_1646" n="HIAT:w" s="T430">ku-</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1649" n="HIAT:w" s="T431">kubi-</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1652" n="HIAT:w" s="T432">kub-</ts>
                  <nts id="Seg_1653" n="HIAT:ip">)</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1656" n="HIAT:w" s="T433">tustʼarbiʔi</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1659" n="HIAT:w" s="T434">i</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1662" n="HIAT:w" s="T435">maʔnʼi</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1665" n="HIAT:w" s="T436">deʔpiʔi</ts>
                  <nts id="Seg_1666" n="HIAT:ip">.</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1669" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1671" n="HIAT:w" s="T437">Dĭn</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1674" n="HIAT:w" s="T438">priiskanə</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T439" id="Seg_1677" n="sc" s="T0">
               <ts e="T1" id="Seg_1679" n="e" s="T0">Agafon </ts>
               <ts e="T2" id="Seg_1681" n="e" s="T1">Ivanovič </ts>
               <ts e="T3" id="Seg_1683" n="e" s="T2">šobi </ts>
               <ts e="T4" id="Seg_1685" n="e" s="T3">măna </ts>
               <ts e="T5" id="Seg_1687" n="e" s="T4">dʼăbaktərzittə. </ts>
               <ts e="T6" id="Seg_1689" n="e" s="T5">Măn </ts>
               <ts e="T7" id="Seg_1691" n="e" s="T6">surarbiam: </ts>
               <ts e="T8" id="Seg_1693" n="e" s="T7">"Kăde </ts>
               <ts e="T9" id="Seg_1695" n="e" s="T8">šobial? </ts>
               <ts e="T10" id="Seg_1697" n="e" s="T9">Mašinazʼiʔ </ts>
               <ts e="T11" id="Seg_1699" n="e" s="T10">alʼi </ts>
               <ts e="T12" id="Seg_1701" n="e" s="T11">nʼergölaʔ </ts>
               <ts e="T13" id="Seg_1703" n="e" s="T12">šobial </ts>
               <ts e="T14" id="Seg_1705" n="e" s="T13">dĭn?" </ts>
               <ts e="T15" id="Seg_1707" n="e" s="T14">Mămbi: </ts>
               <ts e="T16" id="Seg_1709" n="e" s="T15">"Nʼergölaʔ </ts>
               <ts e="T17" id="Seg_1711" n="e" s="T16">šobiam". </ts>
               <ts e="T18" id="Seg_1713" n="e" s="T17">Dĭgəttə </ts>
               <ts e="T19" id="Seg_1715" n="e" s="T18">măn </ts>
               <ts e="T20" id="Seg_1717" n="e" s="T19">iʔgö </ts>
               <ts e="T21" id="Seg_1719" n="e" s="T20">(dĭ-) </ts>
               <ts e="T22" id="Seg_1721" n="e" s="T21">dĭzʼiʔ </ts>
               <ts e="T23" id="Seg_1723" n="e" s="T22">(dʼăbaktərnʼa-) </ts>
               <ts e="T24" id="Seg_1725" n="e" s="T23">dʼăbaktərbiam </ts>
               <ts e="T25" id="Seg_1727" n="e" s="T24">bostə </ts>
               <ts e="T26" id="Seg_1729" n="e" s="T25">šĭkətsi. </ts>
               <ts e="T27" id="Seg_1731" n="e" s="T26">Iʔgö </ts>
               <ts e="T28" id="Seg_1733" n="e" s="T27">katuškaʔi, </ts>
               <ts e="T29" id="Seg_1735" n="e" s="T28">bʼeʔ </ts>
               <ts e="T30" id="Seg_1737" n="e" s="T29">sumna. </ts>
               <ts e="T31" id="Seg_1739" n="e" s="T30">Dĭgəttə </ts>
               <ts e="T32" id="Seg_1741" n="e" s="T31">(šideŋ= </ts>
               <ts e="T33" id="Seg_1743" n="e" s="T32">ši- </ts>
               <ts e="T34" id="Seg_1745" n="e" s="T33">šidebiʔ=) </ts>
               <ts e="T35" id="Seg_1747" n="e" s="T34">šideŋ </ts>
               <ts e="T36" id="Seg_1749" n="e" s="T35">(ra-) </ts>
               <ts e="T37" id="Seg_1751" n="e" s="T36">raz </ts>
               <ts e="T38" id="Seg_1753" n="e" s="T37">ibi </ts>
               <ts e="T39" id="Seg_1755" n="e" s="T38">măna </ts>
               <ts e="T40" id="Seg_1757" n="e" s="T39">i </ts>
               <ts e="T441" id="Seg_1759" n="e" s="T40">kalla </ts>
               <ts e="T41" id="Seg_1761" n="e" s="T441">dʼürbi. </ts>
               <ts e="T42" id="Seg_1763" n="e" s="T41">(Dĭgəttə </ts>
               <ts e="T43" id="Seg_1765" n="e" s="T42">măn=) </ts>
               <ts e="T44" id="Seg_1767" n="e" s="T43">Dĭgəttə </ts>
               <ts e="T45" id="Seg_1769" n="e" s="T44">(mă-) </ts>
               <ts e="T46" id="Seg_1771" n="e" s="T45">dĭ </ts>
               <ts e="T47" id="Seg_1773" n="e" s="T46">nörbəbi </ts>
               <ts e="T48" id="Seg_1775" n="e" s="T47">dĭ </ts>
               <ts e="T49" id="Seg_1777" n="e" s="T48">(Арпату). </ts>
               <ts e="T50" id="Seg_1779" n="e" s="T49">Dĭgəttə </ts>
               <ts e="T51" id="Seg_1781" n="e" s="T50">(dĭ=) </ts>
               <ts e="T52" id="Seg_1783" n="e" s="T51">dĭ </ts>
               <ts e="T53" id="Seg_1785" n="e" s="T52">măna </ts>
               <ts e="T54" id="Seg_1787" n="e" s="T53">sazən </ts>
               <ts e="T55" id="Seg_1789" n="e" s="T54">pʼaŋbi, </ts>
               <ts e="T56" id="Seg_1791" n="e" s="T55">măn </ts>
               <ts e="T57" id="Seg_1793" n="e" s="T56">dĭʔnə </ts>
               <ts e="T58" id="Seg_1795" n="e" s="T57">tože </ts>
               <ts e="T59" id="Seg_1797" n="e" s="T58">pʼaŋbiam. </ts>
               <ts e="T60" id="Seg_1799" n="e" s="T59">Šide, </ts>
               <ts e="T61" id="Seg_1801" n="e" s="T60">nagur, </ts>
               <ts e="T62" id="Seg_1803" n="e" s="T61">teʔtə — </ts>
               <ts e="T63" id="Seg_1805" n="e" s="T62">teʔtə </ts>
               <ts e="T64" id="Seg_1807" n="e" s="T63">kö </ts>
               <ts e="T65" id="Seg_1809" n="e" s="T64">dĭ </ts>
               <ts e="T66" id="Seg_1811" n="e" s="T65">măna </ts>
               <ts e="T67" id="Seg_1813" n="e" s="T66">pʼaŋdəbi. </ts>
               <ts e="T68" id="Seg_1815" n="e" s="T67">Dĭgəttə </ts>
               <ts e="T69" id="Seg_1817" n="e" s="T68">(kăštlə-) </ts>
               <ts e="T70" id="Seg_1819" n="e" s="T69">kăštəbi </ts>
               <ts e="T71" id="Seg_1821" n="e" s="T70">döbər. </ts>
               <ts e="T72" id="Seg_1823" n="e" s="T71">Dĭgəttə </ts>
               <ts e="T73" id="Seg_1825" n="e" s="T72">măn </ts>
               <ts e="T74" id="Seg_1827" n="e" s="T73">mămbiam: </ts>
               <ts e="T75" id="Seg_1829" n="e" s="T74">"Kamən </ts>
               <ts e="T76" id="Seg_1831" n="e" s="T75">šoləj </ts>
               <ts e="T77" id="Seg_1833" n="e" s="T76">Agafon </ts>
               <ts e="T78" id="Seg_1835" n="e" s="T77">(Iv-), </ts>
               <ts e="T79" id="Seg_1837" n="e" s="T78">dĭgəttə </ts>
               <ts e="T80" id="Seg_1839" n="e" s="T79">šiʔnʼileʔ </ts>
               <ts e="T81" id="Seg_1841" n="e" s="T80">(šo-) </ts>
               <ts e="T82" id="Seg_1843" n="e" s="T81">šolam". </ts>
               <ts e="T83" id="Seg_1845" n="e" s="T82">Di </ts>
               <ts e="T84" id="Seg_1847" n="e" s="T83">bos- </ts>
               <ts e="T85" id="Seg_1849" n="e" s="T84">dĭgəttə </ts>
               <ts e="T86" id="Seg_1851" n="e" s="T85">dĭ </ts>
               <ts e="T87" id="Seg_1853" n="e" s="T86">bostə </ts>
               <ts e="T88" id="Seg_1855" n="e" s="T87">šobi, </ts>
               <ts e="T89" id="Seg_1857" n="e" s="T88">măna </ts>
               <ts e="T90" id="Seg_1859" n="e" s="T89">ibi, </ts>
               <ts e="T91" id="Seg_1861" n="e" s="T90">nʼergöleʔ </ts>
               <ts e="T92" id="Seg_1863" n="e" s="T91">šobibaʔ, </ts>
               <ts e="T93" id="Seg_1865" n="e" s="T92">mašinazʼiʔ. </ts>
               <ts e="T94" id="Seg_1867" n="e" s="T93">(Мы). </ts>
               <ts e="T95" id="Seg_1869" n="e" s="T94">Mašinazʼiʔ </ts>
               <ts e="T96" id="Seg_1871" n="e" s="T95">šobibaʔ. </ts>
               <ts e="T103" id="Seg_1873" n="e" s="T96">Агафон Иванович два раз ко мне приезжал. </ts>
               <ts e="T113" id="Seg_1875" n="e" s="T103">Я спрашивала: "Как приехал, на машине али на самолёте летел?" </ts>
               <ts e="T117" id="Seg_1877" n="e" s="T113">Он сказал: "На самолёте". </ts>
               <ts e="T118" id="Seg_1879" n="e" s="T117">(Tогда) </ts>
               <ts e="T119" id="Seg_1881" n="e" s="T118">Тогда </ts>
               <ts e="T120" id="Seg_1883" n="e" s="T119">он </ts>
               <ts e="T121" id="Seg_1885" n="e" s="T120">два </ts>
               <ts e="T122" id="Seg_1887" n="e" s="T121">раза </ts>
               <ts e="T123" id="Seg_1889" n="e" s="T122">был </ts>
               <ts e="T124" id="Seg_1891" n="e" s="T123">у </ts>
               <ts e="T125" id="Seg_1893" n="e" s="T124">меня. </ts>
               <ts e="T127" id="Seg_1895" n="e" s="T125">И уехал. </ts>
               <ts e="T128" id="Seg_1897" n="e" s="T127">Господи, </ts>
               <ts e="T129" id="Seg_1899" n="e" s="T128">опять </ts>
               <ts e="T130" id="Seg_1901" n="e" s="T129">не </ts>
               <ts e="T131" id="Seg_1903" n="e" s="T130">забыть </ts>
               <ts e="T132" id="Seg_1905" n="e" s="T131">бы </ts>
               <ts e="T133" id="Seg_1907" n="e" s="T132">((DMG)). </ts>
               <ts e="T134" id="Seg_1909" n="e" s="T133">(И </ts>
               <ts e="T135" id="Seg_1911" n="e" s="T134">у </ts>
               <ts e="T136" id="Seg_1913" n="e" s="T135">-) </ts>
               <ts e="T137" id="Seg_1915" n="e" s="T136">И </ts>
               <ts e="T138" id="Seg_1917" n="e" s="T137">уехал, </ts>
               <ts e="T140" id="Seg_1919" n="e" s="T138">и… </ts>
               <ts e="T151" id="Seg_1921" n="e" s="T140">А Арпиту сказал, что там женщина, которая (ра-) умеет разговаривать. </ts>
               <ts e="T160" id="Seg_1923" n="e" s="T151">Он давай мне письма писать, четыре года списывалися мы. </ts>
               <ts e="T168" id="Seg_1925" n="e" s="T160">Тогда (на-) звал мне, чтоб я приехала. </ts>
               <ts e="T185" id="Seg_1927" n="e" s="T168">Но я написала ему что я не могу одна приехать, когда приедет Агафон Иванович, тогда я приеду. </ts>
               <ts e="T205" id="Seg_1929" n="e" s="T185">Он собрался и сам приехал и ((PAUSE)) мы летели на самолёте, и на машине (прилете-, при-) приехали сюда. </ts>
               <ts e="T206" id="Seg_1931" n="e" s="T205">Чего? </ts>
               <ts e="T207" id="Seg_1933" n="e" s="T206">Уже </ts>
               <ts e="T208" id="Seg_1935" n="e" s="T207">говорить </ts>
               <ts e="T209" id="Seg_1937" n="e" s="T208">можно? </ts>
               <ts e="T210" id="Seg_1939" n="e" s="T209">Dĭgəttə </ts>
               <ts e="T211" id="Seg_1941" n="e" s="T210">(mɨ-) </ts>
               <ts e="T212" id="Seg_1943" n="e" s="T211">măn </ts>
               <ts e="T213" id="Seg_1945" n="e" s="T212">šobiam. </ts>
               <ts e="T214" id="Seg_1947" n="e" s="T213">Măn </ts>
               <ts e="T215" id="Seg_1949" n="e" s="T214">(amnolbi-) </ts>
               <ts e="T216" id="Seg_1951" n="e" s="T215">nuldəbiʔi </ts>
               <ts e="T217" id="Seg_1953" n="e" s="T216">dö </ts>
               <ts e="T218" id="Seg_1955" n="e" s="T217">turanə, </ts>
               <ts e="T219" id="Seg_1957" n="e" s="T218">Ninanə. </ts>
               <ts e="T220" id="Seg_1959" n="e" s="T219">Dĭgəttə </ts>
               <ts e="T221" id="Seg_1961" n="e" s="T220">măn </ts>
               <ts e="T222" id="Seg_1963" n="e" s="T221">mĭmbiem. </ts>
               <ts e="T223" id="Seg_1965" n="e" s="T222">Šobiʔi </ts>
               <ts e="T224" id="Seg_1967" n="e" s="T223">măna </ts>
               <ts e="T225" id="Seg_1969" n="e" s="T224">(na-) </ts>
               <ts e="T226" id="Seg_1971" n="e" s="T225">šide </ts>
               <ts e="T227" id="Seg_1973" n="e" s="T226">kuza, </ts>
               <ts e="T228" id="Seg_1975" n="e" s="T227">dĭgəttə </ts>
               <ts e="T229" id="Seg_1977" n="e" s="T228">mašinanə </ts>
               <ts e="T230" id="Seg_1979" n="e" s="T229">(amnomgubiʔi), </ts>
               <ts e="T231" id="Seg_1981" n="e" s="T230">kunnaːmbiʔi. </ts>
               <ts e="T232" id="Seg_1983" n="e" s="T231">Kuŋge, </ts>
               <ts e="T233" id="Seg_1985" n="e" s="T232">(s- </ts>
               <ts e="T234" id="Seg_1987" n="e" s="T233">go-) </ts>
               <ts e="T235" id="Seg_1989" n="e" s="T234">(за </ts>
               <ts e="T236" id="Seg_1991" n="e" s="T235">город </ts>
               <ts e="T237" id="Seg_1993" n="e" s="T236">dĭ) </ts>
               <ts e="T238" id="Seg_1995" n="e" s="T237">gijen… </ts>
               <ts e="T239" id="Seg_1997" n="e" s="T238">Šindidə </ts>
               <ts e="T240" id="Seg_1999" n="e" s="T239">naga, </ts>
               <ts e="T241" id="Seg_2001" n="e" s="T240">dĭn </ts>
               <ts e="T242" id="Seg_2003" n="e" s="T241">bar </ts>
               <ts e="T243" id="Seg_2005" n="e" s="T242">keʔbdeʔi </ts>
               <ts e="T244" id="Seg_2007" n="e" s="T243">ambiam. </ts>
               <ts e="T245" id="Seg_2009" n="e" s="T244">Dĭgəttə </ts>
               <ts e="T246" id="Seg_2011" n="e" s="T245">dĭn </ts>
               <ts e="T247" id="Seg_2013" n="e" s="T246">dʼăbaktərbiam </ts>
               <ts e="T248" id="Seg_2015" n="e" s="T247">(dĭ-). </ts>
               <ts e="T250" id="Seg_2017" n="e" s="T248">Dĭgəttə… </ts>
               <ts e="T260" id="Seg_2019" n="e" s="T250">Когда привёз меня сюда и поставил к Нине на фатеру. </ts>
               <ts e="T261" id="Seg_2021" n="e" s="T260">Так, </ts>
               <ts e="T262" id="Seg_2023" n="e" s="T261">потом </ts>
               <ts e="T263" id="Seg_2025" n="e" s="T262">пришли </ts>
               <ts e="T264" id="Seg_2027" n="e" s="T263">двое </ts>
               <ts e="T265" id="Seg_2029" n="e" s="T264">и </ts>
               <ts e="T266" id="Seg_2031" n="e" s="T265">третий </ts>
               <ts e="T267" id="Seg_2033" n="e" s="T266">и </ts>
               <ts e="T268" id="Seg_2035" n="e" s="T267">посадили </ts>
               <ts e="T269" id="Seg_2037" n="e" s="T268">меня </ts>
               <ts e="T270" id="Seg_2039" n="e" s="T269">на </ts>
               <ts e="T271" id="Seg_2041" n="e" s="T270">машину </ts>
               <ts e="T272" id="Seg_2043" n="e" s="T271">и </ts>
               <ts e="T273" id="Seg_2045" n="e" s="T272">ездили </ts>
               <ts e="T274" id="Seg_2047" n="e" s="T273">(за=) </ts>
               <ts e="T275" id="Seg_2049" n="e" s="T274">за </ts>
               <ts e="T276" id="Seg_2051" n="e" s="T275">город </ts>
               <ts e="T277" id="Seg_2053" n="e" s="T276">далёко. </ts>
               <ts e="T282" id="Seg_2055" n="e" s="T277">И там я ягоды ела. </ts>
               <ts e="T287" id="Seg_2057" n="e" s="T282">И разговаривала с ними ((…)). </ts>
               <ts e="T291" id="Seg_2059" n="e" s="T287">Больше ((…)) всё наверно. </ts>
               <ts e="T292" id="Seg_2061" n="e" s="T291">Dĭgəttə </ts>
               <ts e="T293" id="Seg_2063" n="e" s="T292">nörbələm </ts>
               <ts e="T294" id="Seg_2065" n="e" s="T293">girgit </ts>
               <ts e="T295" id="Seg_2067" n="e" s="T294">(m- </ts>
               <ts e="T296" id="Seg_2069" n="e" s="T295">m-) </ts>
               <ts e="T297" id="Seg_2071" n="e" s="T296">miʔ </ts>
               <ts e="T298" id="Seg_2073" n="e" s="T297">(dʼüm=) </ts>
               <ts e="T299" id="Seg_2075" n="e" s="T298">dʼüm. </ts>
               <ts e="T300" id="Seg_2077" n="e" s="T299">Dĭn </ts>
               <ts e="T301" id="Seg_2079" n="e" s="T300">iʔgö </ts>
               <ts e="T302" id="Seg_2081" n="e" s="T301">keʔbde </ts>
               <ts e="T303" id="Seg_2083" n="e" s="T302">ige. </ts>
               <ts e="T304" id="Seg_2085" n="e" s="T303">I </ts>
               <ts e="T305" id="Seg_2087" n="e" s="T304">paʔi, </ts>
               <ts e="T306" id="Seg_2089" n="e" s="T305">sĭri </ts>
               <ts e="T307" id="Seg_2091" n="e" s="T306">i </ts>
               <ts e="T308" id="Seg_2093" n="e" s="T307">köməʔi, </ts>
               <ts e="T309" id="Seg_2095" n="e" s="T308">ăsinaʔi, </ts>
               <ts e="T310" id="Seg_2097" n="e" s="T309">kedrʔi </ts>
               <ts e="T311" id="Seg_2099" n="e" s="T310">ige </ts>
               <ts e="T312" id="Seg_2101" n="e" s="T311">pa, </ts>
               <ts e="T313" id="Seg_2103" n="e" s="T312">gijen </ts>
               <ts e="T314" id="Seg_2105" n="e" s="T313">sanə </ts>
               <ts e="T315" id="Seg_2107" n="e" s="T314">özerbəlaʔbə. </ts>
               <ts e="T316" id="Seg_2109" n="e" s="T315">Малина </ts>
               <ts e="T317" id="Seg_2111" n="e" s="T316">ige. </ts>
               <ts e="T318" id="Seg_2113" n="e" s="T317">Dĭgəttə </ts>
               <ts e="T319" id="Seg_2115" n="e" s="T318">dĭn </ts>
               <ts e="T320" id="Seg_2117" n="e" s="T319">sʼtʼepʔi </ts>
               <ts e="T321" id="Seg_2119" n="e" s="T320">ige. </ts>
               <ts e="T322" id="Seg_2121" n="e" s="T321">Iʔgö </ts>
               <ts e="T323" id="Seg_2123" n="e" s="T322">ipek. </ts>
               <ts e="T324" id="Seg_2125" n="e" s="T323">Budəj </ts>
               <ts e="T325" id="Seg_2127" n="e" s="T324">ipek, </ts>
               <ts e="T326" id="Seg_2129" n="e" s="T325">(su-) </ts>
               <ts e="T327" id="Seg_2131" n="e" s="T326">sulu </ts>
               <ts e="T329" id="Seg_2133" n="e" s="T327">i… </ts>
               <ts e="T332" id="Seg_2135" n="e" s="T329">У нас там((…))… </ts>
               <ts e="T338" id="Seg_2137" n="e" s="T332">у нас там лес. </ts>
               <ts e="T339" id="Seg_2139" n="e" s="T338">Берёзы. </ts>
               <ts e="T351" id="Seg_2141" n="e" s="T339">И листвяги, как я их называю красными по-своему, листвяги, осины. ((DMG)). </ts>
               <ts e="T358" id="Seg_2143" n="e" s="T351">И кедры на которых растут орехи, шишки. ((DMG)). </ts>
               <ts e="T361" id="Seg_2145" n="e" s="T358">И ягоды, малина. ((DMG)). </ts>
               <ts e="T366" id="Seg_2147" n="e" s="T361">И (с-) и степь. </ts>
               <ts e="T375" id="Seg_2149" n="e" s="T366">На (пас-) пшеничный хлеб, и овёс, и гречуха. ((DMG)). </ts>
               <ts e="T376" id="Seg_2151" n="e" s="T375">Так. ((DMG)). </ts>
               <ts e="T377" id="Seg_2153" n="e" s="T376">Kamən </ts>
               <ts e="T378" id="Seg_2155" n="e" s="T377">ălʼenʼəʔi </ts>
               <ts e="T379" id="Seg_2157" n="e" s="T378">ibiʔi, </ts>
               <ts e="T380" id="Seg_2159" n="e" s="T379">(dĭ=) </ts>
               <ts e="T381" id="Seg_2161" n="e" s="T380">dĭzeŋ </ts>
               <ts e="T382" id="Seg_2163" n="e" s="T381">(əz-) </ts>
               <ts e="T383" id="Seg_2165" n="e" s="T382">ĭzembiʔi, </ts>
               <ts e="T384" id="Seg_2167" n="e" s="T383">dĭzeŋ, </ts>
               <ts e="T385" id="Seg_2169" n="e" s="T384">dĭgəttə </ts>
               <ts e="T386" id="Seg_2171" n="e" s="T385">dĭzeŋ </ts>
               <ts e="T387" id="Seg_2173" n="e" s="T386">öʔlübiʔi. </ts>
               <ts e="T388" id="Seg_2175" n="e" s="T387">Dĭzeŋ </ts>
               <ts e="T389" id="Seg_2177" n="e" s="T388">(i </ts>
               <ts e="T390" id="Seg_2179" n="e" s="T389">lestə) </ts>
               <ts e="T391" id="Seg_2181" n="e" s="T390">kambiʔi. </ts>
               <ts e="T392" id="Seg_2183" n="e" s="T391">Iam </ts>
               <ts e="T393" id="Seg_2185" n="e" s="T392">nörbəbi, </ts>
               <ts e="T394" id="Seg_2187" n="e" s="T393">urgajam </ts>
               <ts e="T395" id="Seg_2189" n="e" s="T394">mămbi </ts>
               <ts e="T396" id="Seg_2191" n="e" s="T395">măna, </ts>
               <ts e="T397" id="Seg_2193" n="e" s="T396">iššo </ts>
               <ts e="T398" id="Seg_2195" n="e" s="T397">üdʼüge </ts>
               <ts e="T399" id="Seg_2197" n="e" s="T398">ibiem. </ts>
               <ts e="T411" id="Seg_2199" n="e" s="T399">Когда на оленях ездили, они стали паршиветь, и взяли, отпустили в лес. </ts>
               <ts e="T416" id="Seg_2201" n="e" s="T411">И бабушка говорила и мама. </ts>
               <ts e="T417" id="Seg_2203" n="e" s="T416">Dĭgəttə </ts>
               <ts e="T418" id="Seg_2205" n="e" s="T417">dĭzeŋ </ts>
               <ts e="T419" id="Seg_2207" n="e" s="T418">mĭmbiʔi </ts>
               <ts e="T420" id="Seg_2209" n="e" s="T419">dʼijenə. </ts>
               <ts e="T421" id="Seg_2211" n="e" s="T420">Urgo </ts>
               <ts e="T422" id="Seg_2213" n="e" s="T421">măjanə, </ts>
               <ts e="T423" id="Seg_2215" n="e" s="T422">Belăgorjanə. </ts>
               <ts e="T424" id="Seg_2217" n="e" s="T423">Gijen </ts>
               <ts e="T425" id="Seg_2219" n="e" s="T424">iʔgö </ts>
               <ts e="T426" id="Seg_2221" n="e" s="T425">bü. </ts>
               <ts e="T427" id="Seg_2223" n="e" s="T426">Dĭn </ts>
               <ts e="T428" id="Seg_2225" n="e" s="T427">kola </ts>
               <ts e="T429" id="Seg_2227" n="e" s="T428">dʼaʔpiʔi. </ts>
               <ts e="T430" id="Seg_2229" n="e" s="T429">I </ts>
               <ts e="T431" id="Seg_2231" n="e" s="T430">(ku- </ts>
               <ts e="T432" id="Seg_2233" n="e" s="T431">kubi- </ts>
               <ts e="T433" id="Seg_2235" n="e" s="T432">kub-) </ts>
               <ts e="T434" id="Seg_2237" n="e" s="T433">tustʼarbiʔi </ts>
               <ts e="T435" id="Seg_2239" n="e" s="T434">i </ts>
               <ts e="T436" id="Seg_2241" n="e" s="T435">maʔnʼi </ts>
               <ts e="T437" id="Seg_2243" n="e" s="T436">deʔpiʔi. </ts>
               <ts e="T438" id="Seg_2245" n="e" s="T437">Dĭn </ts>
               <ts e="T439" id="Seg_2247" n="e" s="T438">priiskanə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_2248" s="T0">PKZ_1970_F0255.001 (001)</ta>
            <ta e="T9" id="Seg_2249" s="T5">PKZ_1970_F0255.002 (002) </ta>
            <ta e="T14" id="Seg_2250" s="T9">PKZ_1970_F0255.003 (004)</ta>
            <ta e="T17" id="Seg_2251" s="T14">PKZ_1970_F0255.004 (005)</ta>
            <ta e="T26" id="Seg_2252" s="T17">PKZ_1970_F0255.005 (006)</ta>
            <ta e="T30" id="Seg_2253" s="T26">PKZ_1970_F0255.006 (007)</ta>
            <ta e="T41" id="Seg_2254" s="T30">PKZ_1970_F0255.007 (008)</ta>
            <ta e="T49" id="Seg_2255" s="T41">PKZ_1970_F0255.008 (009)</ta>
            <ta e="T59" id="Seg_2256" s="T49">PKZ_1970_F0255.009 (010)</ta>
            <ta e="T67" id="Seg_2257" s="T59">PKZ_1970_F0255.010 (011)</ta>
            <ta e="T71" id="Seg_2258" s="T67">PKZ_1970_F0255.011 (012)</ta>
            <ta e="T82" id="Seg_2259" s="T71">PKZ_1970_F0255.012 (013)</ta>
            <ta e="T93" id="Seg_2260" s="T82">PKZ_1970_F0255.013 (014)</ta>
            <ta e="T94" id="Seg_2261" s="T93">PKZ_1970_F0255.014 (015)</ta>
            <ta e="T96" id="Seg_2262" s="T94">PKZ_1970_F0255.015 (016)</ta>
            <ta e="T103" id="Seg_2263" s="T96">PKZ_1970_F0255.016 (017)</ta>
            <ta e="T113" id="Seg_2264" s="T103">PKZ_1970_F0255.017 (018)</ta>
            <ta e="T117" id="Seg_2265" s="T113">PKZ_1970_F0255.018 (019)</ta>
            <ta e="T125" id="Seg_2266" s="T117">PKZ_1970_F0255.019 (020)</ta>
            <ta e="T127" id="Seg_2267" s="T125">PKZ_1970_F0255.020 (021)</ta>
            <ta e="T133" id="Seg_2268" s="T127">PKZ_1970_F0255.021 (022)</ta>
            <ta e="T140" id="Seg_2269" s="T133">PKZ_1970_F0255.022 (023)</ta>
            <ta e="T151" id="Seg_2270" s="T140">PKZ_1970_F0255.023 (024)</ta>
            <ta e="T160" id="Seg_2271" s="T151">PKZ_1970_F0255.024 (025)</ta>
            <ta e="T168" id="Seg_2272" s="T160">PKZ_1970_F0255.025 (026)</ta>
            <ta e="T185" id="Seg_2273" s="T168">PKZ_1970_F0255.026 (027)</ta>
            <ta e="T205" id="Seg_2274" s="T185">PKZ_1970_F0255.027 (028)</ta>
            <ta e="T206" id="Seg_2275" s="T205">PKZ_1970_F0255.028 (029)</ta>
            <ta e="T209" id="Seg_2276" s="T206">PKZ_1970_F0255.029 (029)</ta>
            <ta e="T213" id="Seg_2277" s="T209">PKZ_1970_F0255.030 (030)</ta>
            <ta e="T219" id="Seg_2278" s="T213">PKZ_1970_F0255.031 (031)</ta>
            <ta e="T222" id="Seg_2279" s="T219">PKZ_1970_F0255.032 (032)</ta>
            <ta e="T231" id="Seg_2280" s="T222">PKZ_1970_F0255.033 (033)</ta>
            <ta e="T238" id="Seg_2281" s="T231">PKZ_1970_F0255.034 (034)</ta>
            <ta e="T244" id="Seg_2282" s="T238">PKZ_1970_F0255.035 (035)</ta>
            <ta e="T248" id="Seg_2283" s="T244">PKZ_1970_F0255.036 (036)</ta>
            <ta e="T250" id="Seg_2284" s="T248">PKZ_1970_F0255.037 (037)</ta>
            <ta e="T260" id="Seg_2285" s="T250">PKZ_1970_F0255.038 (038)</ta>
            <ta e="T277" id="Seg_2286" s="T260">PKZ_1970_F0255.039 (039)</ta>
            <ta e="T282" id="Seg_2287" s="T277">PKZ_1970_F0255.040 (040)</ta>
            <ta e="T287" id="Seg_2288" s="T282">PKZ_1970_F0255.041 (041)</ta>
            <ta e="T291" id="Seg_2289" s="T287">PKZ_1970_F0255.042 (042)</ta>
            <ta e="T299" id="Seg_2290" s="T291">PKZ_1970_F0255.043 (043)</ta>
            <ta e="T303" id="Seg_2291" s="T299">PKZ_1970_F0255.044 (044)</ta>
            <ta e="T315" id="Seg_2292" s="T303">PKZ_1970_F0255.045 (045)</ta>
            <ta e="T317" id="Seg_2293" s="T315">PKZ_1970_F0255.046 (046)</ta>
            <ta e="T321" id="Seg_2294" s="T317">PKZ_1970_F0255.047 (047)</ta>
            <ta e="T323" id="Seg_2295" s="T321">PKZ_1970_F0255.048 (048)</ta>
            <ta e="T329" id="Seg_2296" s="T323">PKZ_1970_F0255.049 (049)</ta>
            <ta e="T332" id="Seg_2297" s="T329">PKZ_1970_F0255.050 (050)</ta>
            <ta e="T338" id="Seg_2298" s="T332">PKZ_1970_F0255.051 (050) </ta>
            <ta e="T339" id="Seg_2299" s="T338">PKZ_1970_F0255.052 (051)</ta>
            <ta e="T351" id="Seg_2300" s="T339">PKZ_1970_F0255.053 (052)</ta>
            <ta e="T358" id="Seg_2301" s="T351">PKZ_1970_F0255.054 (053)</ta>
            <ta e="T361" id="Seg_2302" s="T358">PKZ_1970_F0255.055 (054)</ta>
            <ta e="T366" id="Seg_2303" s="T361">PKZ_1970_F0255.056 (055)</ta>
            <ta e="T375" id="Seg_2304" s="T366">PKZ_1970_F0255.057 (056)</ta>
            <ta e="T376" id="Seg_2305" s="T375">PKZ_1970_F0255.058 (057)</ta>
            <ta e="T387" id="Seg_2306" s="T376">PKZ_1970_F0255.059 (058)</ta>
            <ta e="T391" id="Seg_2307" s="T387">PKZ_1970_F0255.060 (059)</ta>
            <ta e="T399" id="Seg_2308" s="T391">PKZ_1970_F0255.061 (060)</ta>
            <ta e="T411" id="Seg_2309" s="T399">PKZ_1970_F0255.062 (061)</ta>
            <ta e="T416" id="Seg_2310" s="T411">PKZ_1970_F0255.063 (062)</ta>
            <ta e="T420" id="Seg_2311" s="T416">PKZ_1970_F0255.064 (063)</ta>
            <ta e="T423" id="Seg_2312" s="T420">PKZ_1970_F0255.065 (064)</ta>
            <ta e="T426" id="Seg_2313" s="T423">PKZ_1970_F0255.066 (065)</ta>
            <ta e="T429" id="Seg_2314" s="T426">PKZ_1970_F0255.067 (066)</ta>
            <ta e="T437" id="Seg_2315" s="T429">PKZ_1970_F0255.068 (067)</ta>
            <ta e="T439" id="Seg_2316" s="T437">PKZ_1970_F0255.069 (068)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_2317" s="T0">Agafon Ivanovič šobi măna dʼăbaktərzittə. </ta>
            <ta e="T9" id="Seg_2318" s="T5">Măn surarbiam: "Kăde šobial? </ta>
            <ta e="T14" id="Seg_2319" s="T9">Mašinazʼiʔ alʼi nʼergölaʔ šobial dĭn?" </ta>
            <ta e="T17" id="Seg_2320" s="T14">Mămbi:" Nʼergölaʔ šobiam". </ta>
            <ta e="T26" id="Seg_2321" s="T17">Dĭgəttə măn iʔgö (dĭ-) dĭzʼiʔ (dʼăbaktərnʼa-) dʼăbaktərbiam bostə šĭkətsi. </ta>
            <ta e="T30" id="Seg_2322" s="T26">Iʔgö katuškaʔi, bʼeʔ sumna. </ta>
            <ta e="T41" id="Seg_2323" s="T30">Dĭgəttə (šideŋ= ši- šidebiʔ=) šideŋ (ra-) raz ibi măna i kalla dʼürbi. </ta>
            <ta e="T49" id="Seg_2324" s="T41">(Dĭgəttə măn=) Dĭgəttə (mă-) dĭ nörbəbi dĭ (Арпату). </ta>
            <ta e="T59" id="Seg_2325" s="T49">Dĭgəttə (dĭ=) dĭ măna sazən pʼaŋbi, măn dĭʔnə tože pʼaŋbiam. </ta>
            <ta e="T67" id="Seg_2326" s="T59">Šide, nagur, teʔtə — teʔtə kö dĭ măna pʼaŋdəbi. </ta>
            <ta e="T71" id="Seg_2327" s="T67">Dĭgəttə (kăštlə-) kăštəbi döbər. </ta>
            <ta e="T82" id="Seg_2328" s="T71">Dĭgəttə măn mămbiam: "Kamən šoləj Agafon (Iv-), dĭgəttə šiʔnʼileʔ (šo-) šolam". </ta>
            <ta e="T93" id="Seg_2329" s="T82">Di bos- dĭgəttə dĭ bostə šobi, măna ibi, nʼergöleʔ šobibaʔ, mašinazʼiʔ. </ta>
            <ta e="T94" id="Seg_2330" s="T93">(Мы).</ta>
            <ta e="T96" id="Seg_2331" s="T94">Mašinazʼiʔ šobibaʔ. </ta>
            <ta e="T103" id="Seg_2332" s="T96">Агафон Иванович два раз ко мне приезжал. </ta>
            <ta e="T113" id="Seg_2333" s="T103">Я спрашивала: "Как приехал, на машине али на самолёте летел? </ta>
            <ta e="T117" id="Seg_2334" s="T113">Он сказал: "На самолёте". </ta>
            <ta e="T125" id="Seg_2335" s="T117">(Tогда) Тогда он два раза был у меня. </ta>
            <ta e="T127" id="Seg_2336" s="T125">И уехал. </ta>
            <ta e="T133" id="Seg_2337" s="T127">Господи, опять не забыть бы ((DMG)). </ta>
            <ta e="T140" id="Seg_2338" s="T133">(И у -) И уехал, и… </ta>
            <ta e="T151" id="Seg_2339" s="T140">А Арпиту сказал, что там женщина, которая (ра -) умеет разговаривать. </ta>
            <ta e="T160" id="Seg_2340" s="T151">Он давай мне письма писать, четыре года списывалися мы. </ta>
            <ta e="T168" id="Seg_2341" s="T160">Тогда (на-) звал мне, чтоб я приехала. </ta>
            <ta e="T185" id="Seg_2342" s="T168">Но я написала ему что я не могу одна приехать, когда приедет Агафон Иванович, тогда я приеду. </ta>
            <ta e="T205" id="Seg_2343" s="T185">Он собрался и сам приехал и ((PAUSE)) мы летели на самолёте, и на машине (прилете-, при-) приехали сюда. </ta>
            <ta e="T206" id="Seg_2344" s="T205">Чего? </ta>
            <ta e="T209" id="Seg_2345" s="T206"> Уже говорить можно? </ta>
            <ta e="T213" id="Seg_2346" s="T209">Dĭgəttə (mɨ-) măn šobiam. </ta>
            <ta e="T219" id="Seg_2347" s="T213">Măn (amnolbi-) nuldəbiʔi dö turanə, Ninanə. </ta>
            <ta e="T222" id="Seg_2348" s="T219">Dĭgəttə măn mĭmbiem. </ta>
            <ta e="T231" id="Seg_2349" s="T222">Šobiʔi măna (na-) šide kuza, dĭgəttə mašinanə (amnomgubiʔi), kunnaːmbiʔi. </ta>
            <ta e="T238" id="Seg_2350" s="T231">Kuŋge, (s- go-) (за город dĭ) gijen… </ta>
            <ta e="T244" id="Seg_2351" s="T238">Šindidə naga, dĭn bar keʔbdeʔi ambiam. </ta>
            <ta e="T248" id="Seg_2352" s="T244">Dĭgəttə dĭn dʼăbaktərbiam (dĭ-). </ta>
            <ta e="T250" id="Seg_2353" s="T248">Dĭgəttə … </ta>
            <ta e="T260" id="Seg_2354" s="T250">Когда привёз меня сюда и поставил к Нине на фатеру. </ta>
            <ta e="T277" id="Seg_2355" s="T260">Так, потом пришли двое и третий и посадили меня на машину и ездили (за=) за город далёко. </ta>
            <ta e="T282" id="Seg_2356" s="T277">И там я ягоды ела. </ta>
            <ta e="T287" id="Seg_2357" s="T282">И разговаривала с ними ((…)). </ta>
            <ta e="T291" id="Seg_2358" s="T287">Больше ((…)) всё наверно. </ta>
            <ta e="T299" id="Seg_2359" s="T291">Dĭgəttə nörbələm girgit (m- m-) miʔ (dʼüm=) dʼüm. </ta>
            <ta e="T303" id="Seg_2360" s="T299">Dĭn iʔgö keʔbde ige. </ta>
            <ta e="T315" id="Seg_2361" s="T303">I paʔi, sĭri i köməʔi, ăsinaʔi, kedrʔi ige pa, gijen sanə özerbəlaʔbə. </ta>
            <ta e="T317" id="Seg_2362" s="T315">Малина ige. </ta>
            <ta e="T321" id="Seg_2363" s="T317">Dĭgəttə dĭn sʼtʼepʔi ige. </ta>
            <ta e="T323" id="Seg_2364" s="T321">Iʔgö ipek. </ta>
            <ta e="T329" id="Seg_2365" s="T323">Budəj ipek, (su-) sulu i … </ta>
            <ta e="T332" id="Seg_2366" s="T329">У нас там ((…))… </ta>
            <ta e="T338" id="Seg_2367" s="T332">у нас там лес. </ta>
            <ta e="T339" id="Seg_2368" s="T338">Берёзы. </ta>
            <ta e="T351" id="Seg_2369" s="T339">И листвяги, как я их называю красными по-своему, листвяги, осины. ((DMG)). </ta>
            <ta e="T358" id="Seg_2370" s="T351">И кедры на которых растут орехи, шишки. ((DMG)). </ta>
            <ta e="T361" id="Seg_2371" s="T358">И ягоды, малина. ((DMG)). </ta>
            <ta e="T366" id="Seg_2372" s="T361">И (с-) и степь. </ta>
            <ta e="T375" id="Seg_2373" s="T366">На (пас-) пшеничный хлеб, и овёс, и гречуха. ((DMG))</ta>
            <ta e="T376" id="Seg_2374" s="T375">Так. ((DMG)). </ta>
            <ta e="T387" id="Seg_2375" s="T376">Kamən ălʼenʼəʔi ibiʔi, (dĭ=) dĭzeŋ (əz-) ĭzembiʔi, dĭzeŋ, dĭgəttə dĭzeŋ öʔlübiʔi. ((laughter))</ta>
            <ta e="T391" id="Seg_2376" s="T387">Dĭzeŋ (i lestə) kambiʔi. ((DMG))</ta>
            <ta e="T399" id="Seg_2377" s="T391">Iam nörbəbi, urgajam mămbi măna, iššo üdʼüge ibiem. </ta>
            <ta e="T411" id="Seg_2378" s="T399">Когда на оленях ездили, они стали паршиветь, и взяли, отпустили в лес. </ta>
            <ta e="T416" id="Seg_2379" s="T411">И бабушка говорила и мама. </ta>
            <ta e="T420" id="Seg_2380" s="T416">Dĭgəttə dĭzeŋ mĭmbiʔi dʼijenə. </ta>
            <ta e="T423" id="Seg_2381" s="T420">Urgo măjanə, Belăgorjanə. </ta>
            <ta e="T426" id="Seg_2382" s="T423">Gijen iʔgö bü. </ta>
            <ta e="T429" id="Seg_2383" s="T426">Dĭn kola dʼaʔpiʔi. </ta>
            <ta e="T437" id="Seg_2384" s="T429">I (ku- kubi- kub-) tustʼarbiʔi i maʔnʼi deʔpiʔi. </ta>
            <ta e="T439" id="Seg_2385" s="T437">Dĭn priiskanə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2386" s="T0">Agafon</ta>
            <ta e="T2" id="Seg_2387" s="T1">Ivanovič</ta>
            <ta e="T3" id="Seg_2388" s="T2">šo-bi</ta>
            <ta e="T4" id="Seg_2389" s="T3">măna</ta>
            <ta e="T5" id="Seg_2390" s="T4">dʼăbaktər-zittə</ta>
            <ta e="T6" id="Seg_2391" s="T5">măn</ta>
            <ta e="T7" id="Seg_2392" s="T6">surar-bia-m</ta>
            <ta e="T8" id="Seg_2393" s="T7">kăde</ta>
            <ta e="T9" id="Seg_2394" s="T8">šo-bia-l</ta>
            <ta e="T10" id="Seg_2395" s="T9">mašina-zʼiʔ</ta>
            <ta e="T11" id="Seg_2396" s="T10">alʼi</ta>
            <ta e="T12" id="Seg_2397" s="T11">nʼergö-laʔ</ta>
            <ta e="T13" id="Seg_2398" s="T12">šo-bia-l</ta>
            <ta e="T14" id="Seg_2399" s="T13">dĭn</ta>
            <ta e="T15" id="Seg_2400" s="T14">măm-bi</ta>
            <ta e="T16" id="Seg_2401" s="T15">nʼergö-laʔ</ta>
            <ta e="T17" id="Seg_2402" s="T16">šo-bia-m</ta>
            <ta e="T18" id="Seg_2403" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_2404" s="T18">măn</ta>
            <ta e="T20" id="Seg_2405" s="T19">iʔgö</ta>
            <ta e="T22" id="Seg_2406" s="T21">dĭ-zʼiʔ</ta>
            <ta e="T24" id="Seg_2407" s="T23">dʼăbaktər-bia-m</ta>
            <ta e="T25" id="Seg_2408" s="T24">bos-tə</ta>
            <ta e="T26" id="Seg_2409" s="T25">šĭkə-t-si</ta>
            <ta e="T27" id="Seg_2410" s="T26">iʔgö</ta>
            <ta e="T28" id="Seg_2411" s="T27">katuška-ʔi</ta>
            <ta e="T29" id="Seg_2412" s="T28">bʼeʔ</ta>
            <ta e="T30" id="Seg_2413" s="T29">sumna</ta>
            <ta e="T31" id="Seg_2414" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_2415" s="T31">šide-ŋ</ta>
            <ta e="T35" id="Seg_2416" s="T34">šide-ŋ</ta>
            <ta e="T37" id="Seg_2417" s="T36">raz</ta>
            <ta e="T38" id="Seg_2418" s="T37">i-bi</ta>
            <ta e="T39" id="Seg_2419" s="T38">măna</ta>
            <ta e="T40" id="Seg_2420" s="T39">i</ta>
            <ta e="T441" id="Seg_2421" s="T40">kal-la</ta>
            <ta e="T41" id="Seg_2422" s="T441">dʼür-bi</ta>
            <ta e="T42" id="Seg_2423" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_2424" s="T42">măn</ta>
            <ta e="T44" id="Seg_2425" s="T43">dĭgəttə</ta>
            <ta e="T46" id="Seg_2426" s="T45">dĭ</ta>
            <ta e="T47" id="Seg_2427" s="T46">nörbə-bi</ta>
            <ta e="T48" id="Seg_2428" s="T47">dĭ</ta>
            <ta e="T50" id="Seg_2429" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_2430" s="T50">dĭ</ta>
            <ta e="T52" id="Seg_2431" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_2432" s="T52">măna</ta>
            <ta e="T54" id="Seg_2433" s="T53">sazən</ta>
            <ta e="T55" id="Seg_2434" s="T54">pʼaŋ-bi</ta>
            <ta e="T56" id="Seg_2435" s="T55">măn</ta>
            <ta e="T57" id="Seg_2436" s="T56">dĭʔ-nə</ta>
            <ta e="T58" id="Seg_2437" s="T57">tože</ta>
            <ta e="T59" id="Seg_2438" s="T58">pʼaŋ-bia-m</ta>
            <ta e="T60" id="Seg_2439" s="T59">šide</ta>
            <ta e="T61" id="Seg_2440" s="T60">nagur</ta>
            <ta e="T62" id="Seg_2441" s="T61">teʔtə</ta>
            <ta e="T63" id="Seg_2442" s="T62">teʔtə</ta>
            <ta e="T64" id="Seg_2443" s="T63">kö</ta>
            <ta e="T65" id="Seg_2444" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_2445" s="T65">măna</ta>
            <ta e="T67" id="Seg_2446" s="T66">pʼaŋdə-bi</ta>
            <ta e="T68" id="Seg_2447" s="T67">dĭgəttə</ta>
            <ta e="T70" id="Seg_2448" s="T69">kăštə-bi</ta>
            <ta e="T71" id="Seg_2449" s="T70">döbər</ta>
            <ta e="T72" id="Seg_2450" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_2451" s="T72">măn</ta>
            <ta e="T74" id="Seg_2452" s="T73">măm-bia-m</ta>
            <ta e="T75" id="Seg_2453" s="T74">kamən</ta>
            <ta e="T76" id="Seg_2454" s="T75">šo-lə-j</ta>
            <ta e="T77" id="Seg_2455" s="T76">Agafon</ta>
            <ta e="T79" id="Seg_2456" s="T78">dĭgəttə</ta>
            <ta e="T80" id="Seg_2457" s="T79">šiʔnʼileʔ</ta>
            <ta e="T82" id="Seg_2458" s="T81">šo-la-m</ta>
            <ta e="T83" id="Seg_2459" s="T82">di</ta>
            <ta e="T85" id="Seg_2460" s="T84">dĭgəttə</ta>
            <ta e="T86" id="Seg_2461" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_2462" s="T86">bos-tə</ta>
            <ta e="T88" id="Seg_2463" s="T87">šo-bi</ta>
            <ta e="T89" id="Seg_2464" s="T88">măna</ta>
            <ta e="T90" id="Seg_2465" s="T89">i-bi</ta>
            <ta e="T91" id="Seg_2466" s="T90">nʼergö-leʔ</ta>
            <ta e="T92" id="Seg_2467" s="T91">šo-bi-baʔ</ta>
            <ta e="T93" id="Seg_2468" s="T92">mašina-zʼiʔ</ta>
            <ta e="T95" id="Seg_2469" s="T94">mašina-zʼiʔ</ta>
            <ta e="T96" id="Seg_2470" s="T95">šo-bi-baʔ</ta>
            <ta e="T210" id="Seg_2471" s="T209">dĭgəttə</ta>
            <ta e="T212" id="Seg_2472" s="T211">măn</ta>
            <ta e="T213" id="Seg_2473" s="T212">šo-bia-m</ta>
            <ta e="T214" id="Seg_2474" s="T213">măn</ta>
            <ta e="T216" id="Seg_2475" s="T215">nuldə-bi-ʔi</ta>
            <ta e="T217" id="Seg_2476" s="T216">dö</ta>
            <ta e="T218" id="Seg_2477" s="T217">tura-nə</ta>
            <ta e="T219" id="Seg_2478" s="T218">Nina-nə</ta>
            <ta e="T220" id="Seg_2479" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_2480" s="T220">măn</ta>
            <ta e="T222" id="Seg_2481" s="T221">mĭm-bie-m</ta>
            <ta e="T223" id="Seg_2482" s="T222">šo-bi-ʔi</ta>
            <ta e="T224" id="Seg_2483" s="T223">măna</ta>
            <ta e="T226" id="Seg_2484" s="T225">šide</ta>
            <ta e="T227" id="Seg_2485" s="T226">kuza</ta>
            <ta e="T228" id="Seg_2486" s="T227">dĭgəttə</ta>
            <ta e="T229" id="Seg_2487" s="T228">mašina-nə</ta>
            <ta e="T230" id="Seg_2488" s="T229">amno-mgu-bi-ʔi</ta>
            <ta e="T231" id="Seg_2489" s="T230">kun-naːm-bi-ʔi</ta>
            <ta e="T232" id="Seg_2490" s="T231">kuŋge</ta>
            <ta e="T237" id="Seg_2491" s="T236">dĭ</ta>
            <ta e="T238" id="Seg_2492" s="T237">gijen</ta>
            <ta e="T239" id="Seg_2493" s="T238">šindi=də</ta>
            <ta e="T240" id="Seg_2494" s="T239">naga</ta>
            <ta e="T241" id="Seg_2495" s="T240">dĭn</ta>
            <ta e="T242" id="Seg_2496" s="T241">bar</ta>
            <ta e="T243" id="Seg_2497" s="T242">keʔbde-ʔi</ta>
            <ta e="T244" id="Seg_2498" s="T243">am-bia-m</ta>
            <ta e="T245" id="Seg_2499" s="T244">dĭgəttə</ta>
            <ta e="T246" id="Seg_2500" s="T245">dĭn</ta>
            <ta e="T247" id="Seg_2501" s="T246">dʼăbaktər-bia-m</ta>
            <ta e="T250" id="Seg_2502" s="T248">dĭgəttə</ta>
            <ta e="T292" id="Seg_2503" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_2504" s="T292">nörbə-lə-m</ta>
            <ta e="T294" id="Seg_2505" s="T293">girgit</ta>
            <ta e="T297" id="Seg_2506" s="T296">miʔ</ta>
            <ta e="T298" id="Seg_2507" s="T297">dʼü-m</ta>
            <ta e="T299" id="Seg_2508" s="T298">dʼü-m</ta>
            <ta e="T300" id="Seg_2509" s="T299">dĭn</ta>
            <ta e="T301" id="Seg_2510" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_2511" s="T301">keʔbde</ta>
            <ta e="T303" id="Seg_2512" s="T302">i-ge</ta>
            <ta e="T304" id="Seg_2513" s="T303">i</ta>
            <ta e="T305" id="Seg_2514" s="T304">pa-ʔi</ta>
            <ta e="T306" id="Seg_2515" s="T305">sĭri</ta>
            <ta e="T307" id="Seg_2516" s="T306">i</ta>
            <ta e="T308" id="Seg_2517" s="T307">kömə-ʔi</ta>
            <ta e="T309" id="Seg_2518" s="T308">ăsina-ʔi</ta>
            <ta e="T310" id="Seg_2519" s="T309">kedr-ʔi</ta>
            <ta e="T311" id="Seg_2520" s="T310">i-ge</ta>
            <ta e="T312" id="Seg_2521" s="T311">pa</ta>
            <ta e="T313" id="Seg_2522" s="T312">gijen</ta>
            <ta e="T314" id="Seg_2523" s="T313">sanə</ta>
            <ta e="T315" id="Seg_2524" s="T314">özer-bə-laʔbə</ta>
            <ta e="T317" id="Seg_2525" s="T316">i-ge</ta>
            <ta e="T318" id="Seg_2526" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_2527" s="T318">dĭn</ta>
            <ta e="T320" id="Seg_2528" s="T319">sʼtʼep-ʔi</ta>
            <ta e="T321" id="Seg_2529" s="T320">i-ge</ta>
            <ta e="T322" id="Seg_2530" s="T321">iʔgö</ta>
            <ta e="T323" id="Seg_2531" s="T322">ipek</ta>
            <ta e="T324" id="Seg_2532" s="T323">budəj</ta>
            <ta e="T325" id="Seg_2533" s="T324">ipek</ta>
            <ta e="T327" id="Seg_2534" s="T326">sulu</ta>
            <ta e="T329" id="Seg_2535" s="T327">i</ta>
            <ta e="T377" id="Seg_2536" s="T376">kamən</ta>
            <ta e="T378" id="Seg_2537" s="T377">ălʼenʼə-ʔi</ta>
            <ta e="T379" id="Seg_2538" s="T378">i-bi-ʔi</ta>
            <ta e="T380" id="Seg_2539" s="T379">dĭ</ta>
            <ta e="T381" id="Seg_2540" s="T380">dĭ-zeŋ</ta>
            <ta e="T383" id="Seg_2541" s="T382">ĭzem-bi-ʔi</ta>
            <ta e="T384" id="Seg_2542" s="T383">dĭ-zeŋ</ta>
            <ta e="T385" id="Seg_2543" s="T384">dĭgəttə</ta>
            <ta e="T386" id="Seg_2544" s="T385">dĭ-zeŋ</ta>
            <ta e="T387" id="Seg_2545" s="T386">öʔlu-bi-ʔi</ta>
            <ta e="T388" id="Seg_2546" s="T387">dĭ-zeŋ</ta>
            <ta e="T389" id="Seg_2547" s="T388">i</ta>
            <ta e="T390" id="Seg_2548" s="T389">les-tə</ta>
            <ta e="T391" id="Seg_2549" s="T390">kam-bi-ʔi</ta>
            <ta e="T392" id="Seg_2550" s="T391">ia-m</ta>
            <ta e="T393" id="Seg_2551" s="T392">nörbə-bi</ta>
            <ta e="T394" id="Seg_2552" s="T393">urgaja-m</ta>
            <ta e="T395" id="Seg_2553" s="T394">măm-bi</ta>
            <ta e="T396" id="Seg_2554" s="T395">măna</ta>
            <ta e="T397" id="Seg_2555" s="T396">iššo</ta>
            <ta e="T398" id="Seg_2556" s="T397">üdʼüge</ta>
            <ta e="T399" id="Seg_2557" s="T398">i-bie-m</ta>
            <ta e="T417" id="Seg_2558" s="T416">dĭgəttə</ta>
            <ta e="T418" id="Seg_2559" s="T417">dĭ-zeŋ</ta>
            <ta e="T419" id="Seg_2560" s="T418">mĭm-bi-ʔi</ta>
            <ta e="T420" id="Seg_2561" s="T419">dʼije-nə</ta>
            <ta e="T421" id="Seg_2562" s="T420">urgo</ta>
            <ta e="T422" id="Seg_2563" s="T421">măja-nə</ta>
            <ta e="T423" id="Seg_2564" s="T422">Belăgorja-nə</ta>
            <ta e="T424" id="Seg_2565" s="T423">gijen</ta>
            <ta e="T425" id="Seg_2566" s="T424">iʔgö</ta>
            <ta e="T426" id="Seg_2567" s="T425">bü</ta>
            <ta e="T427" id="Seg_2568" s="T426">dĭn</ta>
            <ta e="T428" id="Seg_2569" s="T427">kola</ta>
            <ta e="T429" id="Seg_2570" s="T428">dʼaʔ-pi-ʔi</ta>
            <ta e="T430" id="Seg_2571" s="T429">i</ta>
            <ta e="T434" id="Seg_2572" s="T433">tustʼar-bi-ʔi</ta>
            <ta e="T435" id="Seg_2573" s="T434">i</ta>
            <ta e="T436" id="Seg_2574" s="T435">maʔ-nʼi</ta>
            <ta e="T437" id="Seg_2575" s="T436">deʔ-pi-ʔi</ta>
            <ta e="T438" id="Seg_2576" s="T437">dĭn</ta>
            <ta e="T439" id="Seg_2577" s="T438">priiska-nə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2578" s="T0">Agafon</ta>
            <ta e="T2" id="Seg_2579" s="T1">Ivanovič</ta>
            <ta e="T3" id="Seg_2580" s="T2">šo-bi</ta>
            <ta e="T4" id="Seg_2581" s="T3">măna</ta>
            <ta e="T5" id="Seg_2582" s="T4">tʼăbaktər-zittə</ta>
            <ta e="T6" id="Seg_2583" s="T5">măn</ta>
            <ta e="T7" id="Seg_2584" s="T6">surar-bi-m</ta>
            <ta e="T8" id="Seg_2585" s="T7">kădaʔ</ta>
            <ta e="T9" id="Seg_2586" s="T8">šo-bi-l</ta>
            <ta e="T10" id="Seg_2587" s="T9">mašina-ziʔ</ta>
            <ta e="T11" id="Seg_2588" s="T10">aľi</ta>
            <ta e="T12" id="Seg_2589" s="T11">nʼergö-lAʔ</ta>
            <ta e="T13" id="Seg_2590" s="T12">šo-bi-l</ta>
            <ta e="T14" id="Seg_2591" s="T13">dĭn</ta>
            <ta e="T15" id="Seg_2592" s="T14">măn-bi</ta>
            <ta e="T16" id="Seg_2593" s="T15">nʼergö-lAʔ</ta>
            <ta e="T17" id="Seg_2594" s="T16">šo-bi-m</ta>
            <ta e="T18" id="Seg_2595" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_2596" s="T18">măn</ta>
            <ta e="T20" id="Seg_2597" s="T19">iʔgö</ta>
            <ta e="T22" id="Seg_2598" s="T21">dĭ-ziʔ</ta>
            <ta e="T24" id="Seg_2599" s="T23">tʼăbaktər-bi-m</ta>
            <ta e="T25" id="Seg_2600" s="T24">bos-də</ta>
            <ta e="T26" id="Seg_2601" s="T25">šĭkə-t-ziʔ</ta>
            <ta e="T27" id="Seg_2602" s="T26">iʔgö</ta>
            <ta e="T28" id="Seg_2603" s="T27">katuška-jəʔ</ta>
            <ta e="T29" id="Seg_2604" s="T28">biəʔ</ta>
            <ta e="T30" id="Seg_2605" s="T29">sumna</ta>
            <ta e="T31" id="Seg_2606" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_2607" s="T31">šide-ŋ</ta>
            <ta e="T35" id="Seg_2608" s="T34">šide-ŋ</ta>
            <ta e="T37" id="Seg_2609" s="T36">raz</ta>
            <ta e="T38" id="Seg_2610" s="T37">i-bi</ta>
            <ta e="T39" id="Seg_2611" s="T38">măna</ta>
            <ta e="T40" id="Seg_2612" s="T39">i</ta>
            <ta e="T441" id="Seg_2613" s="T40">kan-lAʔ</ta>
            <ta e="T41" id="Seg_2614" s="T441">tʼür-bi</ta>
            <ta e="T42" id="Seg_2615" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_2616" s="T42">măn</ta>
            <ta e="T44" id="Seg_2617" s="T43">dĭgəttə</ta>
            <ta e="T46" id="Seg_2618" s="T45">dĭ</ta>
            <ta e="T47" id="Seg_2619" s="T46">nörbə-bi</ta>
            <ta e="T48" id="Seg_2620" s="T47">dĭ</ta>
            <ta e="T50" id="Seg_2621" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_2622" s="T50">dĭ</ta>
            <ta e="T52" id="Seg_2623" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_2624" s="T52">măna</ta>
            <ta e="T54" id="Seg_2625" s="T53">sazən</ta>
            <ta e="T55" id="Seg_2626" s="T54">pʼaŋdə-bi</ta>
            <ta e="T56" id="Seg_2627" s="T55">măn</ta>
            <ta e="T57" id="Seg_2628" s="T56">dĭ-Tə</ta>
            <ta e="T58" id="Seg_2629" s="T57">tože</ta>
            <ta e="T59" id="Seg_2630" s="T58">pʼaŋdə-bi-m</ta>
            <ta e="T60" id="Seg_2631" s="T59">šide</ta>
            <ta e="T61" id="Seg_2632" s="T60">nagur</ta>
            <ta e="T62" id="Seg_2633" s="T61">teʔdə</ta>
            <ta e="T63" id="Seg_2634" s="T62">teʔdə</ta>
            <ta e="T64" id="Seg_2635" s="T63">kö</ta>
            <ta e="T65" id="Seg_2636" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_2637" s="T65">măna</ta>
            <ta e="T67" id="Seg_2638" s="T66">pʼaŋdə-bi</ta>
            <ta e="T68" id="Seg_2639" s="T67">dĭgəttə</ta>
            <ta e="T70" id="Seg_2640" s="T69">kăštə-bi</ta>
            <ta e="T71" id="Seg_2641" s="T70">döbər</ta>
            <ta e="T72" id="Seg_2642" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_2643" s="T72">măn</ta>
            <ta e="T74" id="Seg_2644" s="T73">măn-bi-m</ta>
            <ta e="T75" id="Seg_2645" s="T74">kamən</ta>
            <ta e="T76" id="Seg_2646" s="T75">šo-lV-j</ta>
            <ta e="T77" id="Seg_2647" s="T76">Agafon</ta>
            <ta e="T79" id="Seg_2648" s="T78">dĭgəttə</ta>
            <ta e="T80" id="Seg_2649" s="T79">šiʔnʼileʔ</ta>
            <ta e="T82" id="Seg_2650" s="T81">šo-lV-m</ta>
            <ta e="T83" id="Seg_2651" s="T82">dĭ</ta>
            <ta e="T85" id="Seg_2652" s="T84">dĭgəttə</ta>
            <ta e="T86" id="Seg_2653" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_2654" s="T86">bos-də</ta>
            <ta e="T88" id="Seg_2655" s="T87">šo-bi</ta>
            <ta e="T89" id="Seg_2656" s="T88">măna</ta>
            <ta e="T90" id="Seg_2657" s="T89">i-bi</ta>
            <ta e="T91" id="Seg_2658" s="T90">nʼergö-lAʔ</ta>
            <ta e="T92" id="Seg_2659" s="T91">šo-bi-bAʔ</ta>
            <ta e="T93" id="Seg_2660" s="T92">mašina-ziʔ</ta>
            <ta e="T95" id="Seg_2661" s="T94">mašina-ziʔ</ta>
            <ta e="T96" id="Seg_2662" s="T95">šo-bi-bAʔ</ta>
            <ta e="T210" id="Seg_2663" s="T209">dĭgəttə</ta>
            <ta e="T212" id="Seg_2664" s="T211">măn</ta>
            <ta e="T213" id="Seg_2665" s="T212">šo-bi-m</ta>
            <ta e="T214" id="Seg_2666" s="T213">măn</ta>
            <ta e="T216" id="Seg_2667" s="T215">nuldə-bi-jəʔ</ta>
            <ta e="T217" id="Seg_2668" s="T216">dö</ta>
            <ta e="T218" id="Seg_2669" s="T217">tura-Tə</ta>
            <ta e="T219" id="Seg_2670" s="T218">Nina-Tə</ta>
            <ta e="T220" id="Seg_2671" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_2672" s="T220">măn</ta>
            <ta e="T222" id="Seg_2673" s="T221">mĭn-bi-m</ta>
            <ta e="T223" id="Seg_2674" s="T222">šo-bi-jəʔ</ta>
            <ta e="T224" id="Seg_2675" s="T223">măna</ta>
            <ta e="T226" id="Seg_2676" s="T225">šide</ta>
            <ta e="T227" id="Seg_2677" s="T226">kuza</ta>
            <ta e="T228" id="Seg_2678" s="T227">dĭgəttə</ta>
            <ta e="T229" id="Seg_2679" s="T228">mašina-Tə</ta>
            <ta e="T230" id="Seg_2680" s="T229">amno-mgu-bi-jəʔ</ta>
            <ta e="T231" id="Seg_2681" s="T230">kun-laːm-bi-jəʔ</ta>
            <ta e="T232" id="Seg_2682" s="T231">kuŋgə</ta>
            <ta e="T237" id="Seg_2683" s="T236">dĭ</ta>
            <ta e="T238" id="Seg_2684" s="T237">gijen</ta>
            <ta e="T239" id="Seg_2685" s="T238">šində=də</ta>
            <ta e="T240" id="Seg_2686" s="T239">naga</ta>
            <ta e="T241" id="Seg_2687" s="T240">dĭn</ta>
            <ta e="T242" id="Seg_2688" s="T241">bar</ta>
            <ta e="T243" id="Seg_2689" s="T242">keʔbde-jəʔ</ta>
            <ta e="T244" id="Seg_2690" s="T243">am-bi-m</ta>
            <ta e="T245" id="Seg_2691" s="T244">dĭgəttə</ta>
            <ta e="T246" id="Seg_2692" s="T245">dĭn</ta>
            <ta e="T247" id="Seg_2693" s="T246">tʼăbaktər-bi-m</ta>
            <ta e="T250" id="Seg_2694" s="T248">dĭgəttə</ta>
            <ta e="T292" id="Seg_2695" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_2696" s="T292">nörbə-lV-m</ta>
            <ta e="T294" id="Seg_2697" s="T293">girgit</ta>
            <ta e="T297" id="Seg_2698" s="T296">miʔ</ta>
            <ta e="T298" id="Seg_2699" s="T297">tʼo-m</ta>
            <ta e="T299" id="Seg_2700" s="T298">tʼo-m</ta>
            <ta e="T300" id="Seg_2701" s="T299">dĭn</ta>
            <ta e="T301" id="Seg_2702" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_2703" s="T301">keʔbde</ta>
            <ta e="T303" id="Seg_2704" s="T302">i-gA</ta>
            <ta e="T304" id="Seg_2705" s="T303">i</ta>
            <ta e="T305" id="Seg_2706" s="T304">pa-jəʔ</ta>
            <ta e="T306" id="Seg_2707" s="T305">sĭri</ta>
            <ta e="T307" id="Seg_2708" s="T306">i</ta>
            <ta e="T308" id="Seg_2709" s="T307">kömə-jəʔ</ta>
            <ta e="T309" id="Seg_2710" s="T308">ăsina-jəʔ</ta>
            <ta e="T310" id="Seg_2711" s="T309">kedra-jəʔ</ta>
            <ta e="T311" id="Seg_2712" s="T310">i-gA</ta>
            <ta e="T312" id="Seg_2713" s="T311">pa</ta>
            <ta e="T313" id="Seg_2714" s="T312">gijen</ta>
            <ta e="T314" id="Seg_2715" s="T313">sanə</ta>
            <ta e="T315" id="Seg_2716" s="T314">özer-bə-laʔbə</ta>
            <ta e="T317" id="Seg_2717" s="T316">i-gA</ta>
            <ta e="T318" id="Seg_2718" s="T317">dĭgəttə</ta>
            <ta e="T319" id="Seg_2719" s="T318">dĭn</ta>
            <ta e="T320" id="Seg_2720" s="T319">sʼtʼep-jəʔ</ta>
            <ta e="T321" id="Seg_2721" s="T320">i-gA</ta>
            <ta e="T322" id="Seg_2722" s="T321">iʔgö</ta>
            <ta e="T323" id="Seg_2723" s="T322">ipek</ta>
            <ta e="T324" id="Seg_2724" s="T323">budəj</ta>
            <ta e="T325" id="Seg_2725" s="T324">ipek</ta>
            <ta e="T327" id="Seg_2726" s="T326">sulu</ta>
            <ta e="T329" id="Seg_2727" s="T327">i</ta>
            <ta e="T377" id="Seg_2728" s="T376">kamən</ta>
            <ta e="T378" id="Seg_2729" s="T377">ălʼenʼə-jəʔ</ta>
            <ta e="T379" id="Seg_2730" s="T378">i-bi-jəʔ</ta>
            <ta e="T380" id="Seg_2731" s="T379">dĭ</ta>
            <ta e="T381" id="Seg_2732" s="T380">dĭ-zAŋ</ta>
            <ta e="T383" id="Seg_2733" s="T382">ĭzem-bi-jəʔ</ta>
            <ta e="T384" id="Seg_2734" s="T383">dĭ-zAŋ</ta>
            <ta e="T385" id="Seg_2735" s="T384">dĭgəttə</ta>
            <ta e="T386" id="Seg_2736" s="T385">dĭ-zAŋ</ta>
            <ta e="T387" id="Seg_2737" s="T386">öʔlu-bi-jəʔ</ta>
            <ta e="T388" id="Seg_2738" s="T387">dĭ-zAŋ</ta>
            <ta e="T389" id="Seg_2739" s="T388">i</ta>
            <ta e="T390" id="Seg_2740" s="T389">les-Tə</ta>
            <ta e="T391" id="Seg_2741" s="T390">kan-bi-jəʔ</ta>
            <ta e="T392" id="Seg_2742" s="T391">ija-m</ta>
            <ta e="T393" id="Seg_2743" s="T392">nörbə-bi</ta>
            <ta e="T394" id="Seg_2744" s="T393">urgaja-m</ta>
            <ta e="T395" id="Seg_2745" s="T394">măn-bi</ta>
            <ta e="T396" id="Seg_2746" s="T395">măna</ta>
            <ta e="T397" id="Seg_2747" s="T396">ĭššo</ta>
            <ta e="T398" id="Seg_2748" s="T397">üdʼüge</ta>
            <ta e="T399" id="Seg_2749" s="T398">i-bi-m</ta>
            <ta e="T417" id="Seg_2750" s="T416">dĭgəttə</ta>
            <ta e="T418" id="Seg_2751" s="T417">dĭ-zAŋ</ta>
            <ta e="T419" id="Seg_2752" s="T418">mĭn-bi-jəʔ</ta>
            <ta e="T420" id="Seg_2753" s="T419">dʼije-Tə</ta>
            <ta e="T421" id="Seg_2754" s="T420">urgo</ta>
            <ta e="T422" id="Seg_2755" s="T421">măja-Tə</ta>
            <ta e="T423" id="Seg_2756" s="T422">Belăgorja-Tə</ta>
            <ta e="T424" id="Seg_2757" s="T423">gijen</ta>
            <ta e="T425" id="Seg_2758" s="T424">iʔgö</ta>
            <ta e="T426" id="Seg_2759" s="T425">bü</ta>
            <ta e="T427" id="Seg_2760" s="T426">dĭn</ta>
            <ta e="T428" id="Seg_2761" s="T427">kola</ta>
            <ta e="T429" id="Seg_2762" s="T428">dʼabə-bi-jəʔ</ta>
            <ta e="T430" id="Seg_2763" s="T429">i</ta>
            <ta e="T434" id="Seg_2764" s="T433">tustʼar-bi-jəʔ</ta>
            <ta e="T435" id="Seg_2765" s="T434">i</ta>
            <ta e="T436" id="Seg_2766" s="T435">maʔ-gənʼi</ta>
            <ta e="T437" id="Seg_2767" s="T436">det-bi-jəʔ</ta>
            <ta e="T438" id="Seg_2768" s="T437">dĭn</ta>
            <ta e="T439" id="Seg_2769" s="T438">priska-Tə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2770" s="T0">Agafon.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2771" s="T1">Ivanovich.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2772" s="T2">come-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2773" s="T3">I.LAT</ta>
            <ta e="T5" id="Seg_2774" s="T4">speak-INF.LAT</ta>
            <ta e="T6" id="Seg_2775" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_2776" s="T6">ask-PST-1SG</ta>
            <ta e="T8" id="Seg_2777" s="T7">how</ta>
            <ta e="T9" id="Seg_2778" s="T8">come-PST-2SG</ta>
            <ta e="T10" id="Seg_2779" s="T9">machine-INS</ta>
            <ta e="T11" id="Seg_2780" s="T10">or</ta>
            <ta e="T12" id="Seg_2781" s="T11">fly-CVB</ta>
            <ta e="T13" id="Seg_2782" s="T12">come-PST-2SG</ta>
            <ta e="T14" id="Seg_2783" s="T13">there</ta>
            <ta e="T15" id="Seg_2784" s="T14">say-PST.[3SG]</ta>
            <ta e="T16" id="Seg_2785" s="T15">fly-CVB</ta>
            <ta e="T17" id="Seg_2786" s="T16">come-PST-1SG</ta>
            <ta e="T18" id="Seg_2787" s="T17">then</ta>
            <ta e="T19" id="Seg_2788" s="T18">I.NOM</ta>
            <ta e="T20" id="Seg_2789" s="T19">many</ta>
            <ta e="T22" id="Seg_2790" s="T21">this-COM</ta>
            <ta e="T24" id="Seg_2791" s="T23">speak-PST-1SG</ta>
            <ta e="T25" id="Seg_2792" s="T24">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T26" id="Seg_2793" s="T25">language-3SG-INS</ta>
            <ta e="T27" id="Seg_2794" s="T26">many</ta>
            <ta e="T28" id="Seg_2795" s="T27">tape-PL</ta>
            <ta e="T29" id="Seg_2796" s="T28">ten.[NOM.SG]</ta>
            <ta e="T30" id="Seg_2797" s="T29">five.[NOM.SG]</ta>
            <ta e="T31" id="Seg_2798" s="T30">then</ta>
            <ta e="T32" id="Seg_2799" s="T31">two-LAT.ADV</ta>
            <ta e="T35" id="Seg_2800" s="T34">two-LAT.ADV</ta>
            <ta e="T37" id="Seg_2801" s="T36">time.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2802" s="T37">be-PST.[3SG]</ta>
            <ta e="T39" id="Seg_2803" s="T38">I.LAT</ta>
            <ta e="T40" id="Seg_2804" s="T39">and</ta>
            <ta e="T441" id="Seg_2805" s="T40">go-CVB</ta>
            <ta e="T41" id="Seg_2806" s="T441">disappear-PST.[3SG]</ta>
            <ta e="T42" id="Seg_2807" s="T41">then</ta>
            <ta e="T43" id="Seg_2808" s="T42">I.NOM</ta>
            <ta e="T44" id="Seg_2809" s="T43">then</ta>
            <ta e="T46" id="Seg_2810" s="T45">this.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2811" s="T46">tell-PST.[3SG]</ta>
            <ta e="T48" id="Seg_2812" s="T47">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_2813" s="T49">then</ta>
            <ta e="T51" id="Seg_2814" s="T50">this.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2815" s="T51">this.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2816" s="T52">I.LAT</ta>
            <ta e="T54" id="Seg_2817" s="T53">paper.[NOM.SG]</ta>
            <ta e="T55" id="Seg_2818" s="T54">write-PST.[3SG]</ta>
            <ta e="T56" id="Seg_2819" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_2820" s="T56">this-LAT</ta>
            <ta e="T58" id="Seg_2821" s="T57">also</ta>
            <ta e="T59" id="Seg_2822" s="T58">write-PST-1SG</ta>
            <ta e="T60" id="Seg_2823" s="T59">two.[NOM.SG]</ta>
            <ta e="T61" id="Seg_2824" s="T60">three.[NOM.SG]</ta>
            <ta e="T62" id="Seg_2825" s="T61">four.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2826" s="T62">four.[NOM.SG]</ta>
            <ta e="T64" id="Seg_2827" s="T63">winter.[NOM.SG]</ta>
            <ta e="T65" id="Seg_2828" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_2829" s="T65">I.LAT</ta>
            <ta e="T67" id="Seg_2830" s="T66">write-PST.[3SG]</ta>
            <ta e="T68" id="Seg_2831" s="T67">then</ta>
            <ta e="T70" id="Seg_2832" s="T69">call-PST.[3SG]</ta>
            <ta e="T71" id="Seg_2833" s="T70">here</ta>
            <ta e="T72" id="Seg_2834" s="T71">then</ta>
            <ta e="T73" id="Seg_2835" s="T72">I.NOM</ta>
            <ta e="T74" id="Seg_2836" s="T73">say-PST-1SG</ta>
            <ta e="T75" id="Seg_2837" s="T74">when</ta>
            <ta e="T76" id="Seg_2838" s="T75">come-FUT-3SG</ta>
            <ta e="T77" id="Seg_2839" s="T76">Agafon.[NOM.SG]</ta>
            <ta e="T79" id="Seg_2840" s="T78">then</ta>
            <ta e="T80" id="Seg_2841" s="T79">you.PL.ACC</ta>
            <ta e="T82" id="Seg_2842" s="T81">come-FUT-1SG</ta>
            <ta e="T83" id="Seg_2843" s="T82">this.[NOM.SG]</ta>
            <ta e="T85" id="Seg_2844" s="T84">then</ta>
            <ta e="T86" id="Seg_2845" s="T85">this.[NOM.SG]</ta>
            <ta e="T87" id="Seg_2846" s="T86">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T88" id="Seg_2847" s="T87">come-PST.[3SG]</ta>
            <ta e="T89" id="Seg_2848" s="T88">I.ACC</ta>
            <ta e="T90" id="Seg_2849" s="T89">take-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2850" s="T90">fly-CVB</ta>
            <ta e="T92" id="Seg_2851" s="T91">come-PST-1PL</ta>
            <ta e="T93" id="Seg_2852" s="T92">machine-INS</ta>
            <ta e="T95" id="Seg_2853" s="T94">machine-INS</ta>
            <ta e="T96" id="Seg_2854" s="T95">come-PST-1PL</ta>
            <ta e="T210" id="Seg_2855" s="T209">then</ta>
            <ta e="T212" id="Seg_2856" s="T211">I.NOM</ta>
            <ta e="T213" id="Seg_2857" s="T212">come-PST-1SG</ta>
            <ta e="T214" id="Seg_2858" s="T213">I.NOM</ta>
            <ta e="T216" id="Seg_2859" s="T215">place-PST-3PL</ta>
            <ta e="T217" id="Seg_2860" s="T216">that.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2861" s="T217">house-LAT</ta>
            <ta e="T219" id="Seg_2862" s="T218">Nina-LAT</ta>
            <ta e="T220" id="Seg_2863" s="T219">then</ta>
            <ta e="T221" id="Seg_2864" s="T220">I.NOM</ta>
            <ta e="T222" id="Seg_2865" s="T221">go-PST-1SG</ta>
            <ta e="T223" id="Seg_2866" s="T222">come-PST-3PL</ta>
            <ta e="T224" id="Seg_2867" s="T223">I.LAT</ta>
            <ta e="T226" id="Seg_2868" s="T225">two.[NOM.SG]</ta>
            <ta e="T227" id="Seg_2869" s="T226">man.[NOM.SG]</ta>
            <ta e="T228" id="Seg_2870" s="T227">then</ta>
            <ta e="T229" id="Seg_2871" s="T228">machine-LAT</ta>
            <ta e="T230" id="Seg_2872" s="T229">sit-%%-PST-3PL</ta>
            <ta e="T231" id="Seg_2873" s="T230">bring-RES-PST-3PL</ta>
            <ta e="T232" id="Seg_2874" s="T231">far</ta>
            <ta e="T237" id="Seg_2875" s="T236">this.[NOM.SG]</ta>
            <ta e="T238" id="Seg_2876" s="T237">where</ta>
            <ta e="T239" id="Seg_2877" s="T238">who.[NOM.SG]=INDEF</ta>
            <ta e="T240" id="Seg_2878" s="T239">NEG.EX.[3SG]</ta>
            <ta e="T241" id="Seg_2879" s="T240">there</ta>
            <ta e="T242" id="Seg_2880" s="T241">PTCL</ta>
            <ta e="T243" id="Seg_2881" s="T242">berry-PL</ta>
            <ta e="T244" id="Seg_2882" s="T243">eat-PST-1SG</ta>
            <ta e="T245" id="Seg_2883" s="T244">then</ta>
            <ta e="T246" id="Seg_2884" s="T245">there</ta>
            <ta e="T247" id="Seg_2885" s="T246">speak-PST-1SG</ta>
            <ta e="T250" id="Seg_2886" s="T248">then</ta>
            <ta e="T292" id="Seg_2887" s="T291">then</ta>
            <ta e="T293" id="Seg_2888" s="T292">tell-FUT-1SG</ta>
            <ta e="T294" id="Seg_2889" s="T293">what.kind</ta>
            <ta e="T297" id="Seg_2890" s="T296">we.NOM</ta>
            <ta e="T298" id="Seg_2891" s="T297">earth-ACC</ta>
            <ta e="T299" id="Seg_2892" s="T298">earth-ACC</ta>
            <ta e="T300" id="Seg_2893" s="T299">there</ta>
            <ta e="T301" id="Seg_2894" s="T300">many</ta>
            <ta e="T302" id="Seg_2895" s="T301">berry.[NOM.SG]</ta>
            <ta e="T303" id="Seg_2896" s="T302">be-PRS.[3SG]</ta>
            <ta e="T304" id="Seg_2897" s="T303">and</ta>
            <ta e="T305" id="Seg_2898" s="T304">tree-PL</ta>
            <ta e="T306" id="Seg_2899" s="T305">white.[NOM.SG]</ta>
            <ta e="T307" id="Seg_2900" s="T306">and</ta>
            <ta e="T308" id="Seg_2901" s="T307">red-PL</ta>
            <ta e="T309" id="Seg_2902" s="T308">aspen-PL</ta>
            <ta e="T310" id="Seg_2903" s="T309">cedar-PL</ta>
            <ta e="T311" id="Seg_2904" s="T310">be-PRS.[3SG]</ta>
            <ta e="T312" id="Seg_2905" s="T311">tree.[NOM.SG]</ta>
            <ta e="T313" id="Seg_2906" s="T312">where</ta>
            <ta e="T314" id="Seg_2907" s="T313">pine.nut.[NOM.SG]</ta>
            <ta e="T315" id="Seg_2908" s="T314">grow-%%-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_2909" s="T316">be-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_2910" s="T317">then</ta>
            <ta e="T319" id="Seg_2911" s="T318">there</ta>
            <ta e="T320" id="Seg_2912" s="T319">steppe-NOM/GEN/ACC.3PL</ta>
            <ta e="T321" id="Seg_2913" s="T320">be-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_2914" s="T321">many</ta>
            <ta e="T323" id="Seg_2915" s="T322">bread.[NOM.SG]</ta>
            <ta e="T324" id="Seg_2916" s="T323">wheat.[NOM.SG]</ta>
            <ta e="T325" id="Seg_2917" s="T324">bread.[NOM.SG]</ta>
            <ta e="T327" id="Seg_2918" s="T326">oat.[NOM.SG]</ta>
            <ta e="T329" id="Seg_2919" s="T327">and</ta>
            <ta e="T377" id="Seg_2920" s="T376">when</ta>
            <ta e="T378" id="Seg_2921" s="T377">reindeer-PL</ta>
            <ta e="T379" id="Seg_2922" s="T378">be-PST-3PL</ta>
            <ta e="T380" id="Seg_2923" s="T379">this.[NOM.SG]</ta>
            <ta e="T381" id="Seg_2924" s="T380">this-PL</ta>
            <ta e="T383" id="Seg_2925" s="T382">hurt-PST-3PL</ta>
            <ta e="T384" id="Seg_2926" s="T383">this-PL</ta>
            <ta e="T385" id="Seg_2927" s="T384">then</ta>
            <ta e="T386" id="Seg_2928" s="T385">this-PL</ta>
            <ta e="T387" id="Seg_2929" s="T386">send-PST-3PL</ta>
            <ta e="T388" id="Seg_2930" s="T387">this-PL</ta>
            <ta e="T389" id="Seg_2931" s="T388">and</ta>
            <ta e="T390" id="Seg_2932" s="T389">forest-LAT</ta>
            <ta e="T391" id="Seg_2933" s="T390">go-PST-3PL</ta>
            <ta e="T392" id="Seg_2934" s="T391">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T393" id="Seg_2935" s="T392">tell-PST.[3SG]</ta>
            <ta e="T394" id="Seg_2936" s="T393">grandmother-NOM/GEN/ACC.1SG</ta>
            <ta e="T395" id="Seg_2937" s="T394">say-PST.[3SG]</ta>
            <ta e="T396" id="Seg_2938" s="T395">I.LAT</ta>
            <ta e="T397" id="Seg_2939" s="T396">more</ta>
            <ta e="T398" id="Seg_2940" s="T397">small.[NOM.SG]</ta>
            <ta e="T399" id="Seg_2941" s="T398">be-PST-1SG</ta>
            <ta e="T417" id="Seg_2942" s="T416">then</ta>
            <ta e="T418" id="Seg_2943" s="T417">this-PL</ta>
            <ta e="T419" id="Seg_2944" s="T418">go-PST-3PL</ta>
            <ta e="T420" id="Seg_2945" s="T419">forest-LAT</ta>
            <ta e="T421" id="Seg_2946" s="T420">big.[NOM.SG]</ta>
            <ta e="T422" id="Seg_2947" s="T421">mountain-LAT</ta>
            <ta e="T423" id="Seg_2948" s="T422">Belogorye-LAT</ta>
            <ta e="T424" id="Seg_2949" s="T423">where</ta>
            <ta e="T425" id="Seg_2950" s="T424">many</ta>
            <ta e="T426" id="Seg_2951" s="T425">water.[NOM.SG]</ta>
            <ta e="T427" id="Seg_2952" s="T426">there</ta>
            <ta e="T428" id="Seg_2953" s="T427">fish.[NOM.SG]</ta>
            <ta e="T429" id="Seg_2954" s="T428">capture-PST-3PL</ta>
            <ta e="T430" id="Seg_2955" s="T429">and</ta>
            <ta e="T434" id="Seg_2956" s="T433">salt-PST-3PL</ta>
            <ta e="T435" id="Seg_2957" s="T434">and</ta>
            <ta e="T436" id="Seg_2958" s="T435">house-LAT/LOC.1SG</ta>
            <ta e="T437" id="Seg_2959" s="T436">bring-PST-3PL</ta>
            <ta e="T438" id="Seg_2960" s="T437">there</ta>
            <ta e="T439" id="Seg_2961" s="T438">mine-LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2962" s="T0">Агафон.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2963" s="T1">Иванович.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2964" s="T2">прийти-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2965" s="T3">я.LAT</ta>
            <ta e="T5" id="Seg_2966" s="T4">говорить-INF.LAT</ta>
            <ta e="T6" id="Seg_2967" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_2968" s="T6">спросить-PST-1SG</ta>
            <ta e="T8" id="Seg_2969" s="T7">как</ta>
            <ta e="T9" id="Seg_2970" s="T8">прийти-PST-2SG</ta>
            <ta e="T10" id="Seg_2971" s="T9">машина-INS</ta>
            <ta e="T11" id="Seg_2972" s="T10">или</ta>
            <ta e="T12" id="Seg_2973" s="T11">лететь-CVB</ta>
            <ta e="T13" id="Seg_2974" s="T12">прийти-PST-2SG</ta>
            <ta e="T14" id="Seg_2975" s="T13">там</ta>
            <ta e="T15" id="Seg_2976" s="T14">сказать-PST.[3SG]</ta>
            <ta e="T16" id="Seg_2977" s="T15">лететь-CVB</ta>
            <ta e="T17" id="Seg_2978" s="T16">прийти-PST-1SG</ta>
            <ta e="T18" id="Seg_2979" s="T17">тогда</ta>
            <ta e="T19" id="Seg_2980" s="T18">я.NOM</ta>
            <ta e="T20" id="Seg_2981" s="T19">много</ta>
            <ta e="T22" id="Seg_2982" s="T21">этот-COM</ta>
            <ta e="T24" id="Seg_2983" s="T23">говорить-PST-1SG</ta>
            <ta e="T25" id="Seg_2984" s="T24">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T26" id="Seg_2985" s="T25">язык-3SG-INS</ta>
            <ta e="T27" id="Seg_2986" s="T26">много</ta>
            <ta e="T28" id="Seg_2987" s="T27">катушка-PL</ta>
            <ta e="T29" id="Seg_2988" s="T28">десять.[NOM.SG]</ta>
            <ta e="T30" id="Seg_2989" s="T29">пять.[NOM.SG]</ta>
            <ta e="T31" id="Seg_2990" s="T30">тогда</ta>
            <ta e="T32" id="Seg_2991" s="T31">два-LAT.ADV</ta>
            <ta e="T35" id="Seg_2992" s="T34">два-LAT.ADV</ta>
            <ta e="T37" id="Seg_2993" s="T36">раз.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2994" s="T37">быть-PST.[3SG]</ta>
            <ta e="T39" id="Seg_2995" s="T38">я.LAT</ta>
            <ta e="T40" id="Seg_2996" s="T39">и</ta>
            <ta e="T441" id="Seg_2997" s="T40">пойти-CVB</ta>
            <ta e="T41" id="Seg_2998" s="T441">исчезнуть-PST.[3SG]</ta>
            <ta e="T42" id="Seg_2999" s="T41">тогда</ta>
            <ta e="T43" id="Seg_3000" s="T42">я.NOM</ta>
            <ta e="T44" id="Seg_3001" s="T43">тогда</ta>
            <ta e="T46" id="Seg_3002" s="T45">этот.[NOM.SG]</ta>
            <ta e="T47" id="Seg_3003" s="T46">сказать-PST.[3SG]</ta>
            <ta e="T48" id="Seg_3004" s="T47">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_3005" s="T49">тогда</ta>
            <ta e="T51" id="Seg_3006" s="T50">этот.[NOM.SG]</ta>
            <ta e="T52" id="Seg_3007" s="T51">этот.[NOM.SG]</ta>
            <ta e="T53" id="Seg_3008" s="T52">я.LAT</ta>
            <ta e="T54" id="Seg_3009" s="T53">бумага.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3010" s="T54">писать-PST.[3SG]</ta>
            <ta e="T56" id="Seg_3011" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_3012" s="T56">этот-LAT</ta>
            <ta e="T58" id="Seg_3013" s="T57">тоже</ta>
            <ta e="T59" id="Seg_3014" s="T58">писать-PST-1SG</ta>
            <ta e="T60" id="Seg_3015" s="T59">два.[NOM.SG]</ta>
            <ta e="T61" id="Seg_3016" s="T60">три.[NOM.SG]</ta>
            <ta e="T62" id="Seg_3017" s="T61">четыре.[NOM.SG]</ta>
            <ta e="T63" id="Seg_3018" s="T62">четыре.[NOM.SG]</ta>
            <ta e="T64" id="Seg_3019" s="T63">зима.[NOM.SG]</ta>
            <ta e="T65" id="Seg_3020" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_3021" s="T65">я.LAT</ta>
            <ta e="T67" id="Seg_3022" s="T66">писать-PST.[3SG]</ta>
            <ta e="T68" id="Seg_3023" s="T67">тогда</ta>
            <ta e="T70" id="Seg_3024" s="T69">позвать-PST.[3SG]</ta>
            <ta e="T71" id="Seg_3025" s="T70">здесь</ta>
            <ta e="T72" id="Seg_3026" s="T71">тогда</ta>
            <ta e="T73" id="Seg_3027" s="T72">я.NOM</ta>
            <ta e="T74" id="Seg_3028" s="T73">сказать-PST-1SG</ta>
            <ta e="T75" id="Seg_3029" s="T74">когда</ta>
            <ta e="T76" id="Seg_3030" s="T75">прийти-FUT-3SG</ta>
            <ta e="T77" id="Seg_3031" s="T76">Агафон.[NOM.SG]</ta>
            <ta e="T79" id="Seg_3032" s="T78">тогда</ta>
            <ta e="T80" id="Seg_3033" s="T79">вы.ACC</ta>
            <ta e="T82" id="Seg_3034" s="T81">прийти-FUT-1SG</ta>
            <ta e="T83" id="Seg_3035" s="T82">этот.[NOM.SG]</ta>
            <ta e="T85" id="Seg_3036" s="T84">тогда</ta>
            <ta e="T86" id="Seg_3037" s="T85">этот.[NOM.SG]</ta>
            <ta e="T87" id="Seg_3038" s="T86">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T88" id="Seg_3039" s="T87">прийти-PST.[3SG]</ta>
            <ta e="T89" id="Seg_3040" s="T88">я.ACC</ta>
            <ta e="T90" id="Seg_3041" s="T89">взять-PST.[3SG]</ta>
            <ta e="T91" id="Seg_3042" s="T90">лететь-CVB</ta>
            <ta e="T92" id="Seg_3043" s="T91">прийти-PST-1PL</ta>
            <ta e="T93" id="Seg_3044" s="T92">машина-INS</ta>
            <ta e="T95" id="Seg_3045" s="T94">машина-INS</ta>
            <ta e="T96" id="Seg_3046" s="T95">прийти-PST-1PL</ta>
            <ta e="T210" id="Seg_3047" s="T209">тогда</ta>
            <ta e="T212" id="Seg_3048" s="T211">я.NOM</ta>
            <ta e="T213" id="Seg_3049" s="T212">прийти-PST-1SG</ta>
            <ta e="T214" id="Seg_3050" s="T213">я.NOM</ta>
            <ta e="T216" id="Seg_3051" s="T215">поставить-PST-3PL</ta>
            <ta e="T217" id="Seg_3052" s="T216">тот.[NOM.SG]</ta>
            <ta e="T218" id="Seg_3053" s="T217">дом-LAT</ta>
            <ta e="T219" id="Seg_3054" s="T218">Нина-LAT</ta>
            <ta e="T220" id="Seg_3055" s="T219">тогда</ta>
            <ta e="T221" id="Seg_3056" s="T220">я.NOM</ta>
            <ta e="T222" id="Seg_3057" s="T221">идти-PST-1SG</ta>
            <ta e="T223" id="Seg_3058" s="T222">прийти-PST-3PL</ta>
            <ta e="T224" id="Seg_3059" s="T223">я.LAT</ta>
            <ta e="T226" id="Seg_3060" s="T225">два.[NOM.SG]</ta>
            <ta e="T227" id="Seg_3061" s="T226">мужчина.[NOM.SG]</ta>
            <ta e="T228" id="Seg_3062" s="T227">тогда</ta>
            <ta e="T229" id="Seg_3063" s="T228">машина-LAT</ta>
            <ta e="T230" id="Seg_3064" s="T229">сидеть-%%-PST-3PL</ta>
            <ta e="T231" id="Seg_3065" s="T230">нести-RES-PST-3PL</ta>
            <ta e="T232" id="Seg_3066" s="T231">далеко</ta>
            <ta e="T237" id="Seg_3067" s="T236">этот.[NOM.SG]</ta>
            <ta e="T238" id="Seg_3068" s="T237">где</ta>
            <ta e="T239" id="Seg_3069" s="T238">кто.[NOM.SG]=INDEF</ta>
            <ta e="T240" id="Seg_3070" s="T239">NEG.EX.[3SG]</ta>
            <ta e="T241" id="Seg_3071" s="T240">там</ta>
            <ta e="T242" id="Seg_3072" s="T241">PTCL</ta>
            <ta e="T243" id="Seg_3073" s="T242">ягода-PL</ta>
            <ta e="T244" id="Seg_3074" s="T243">съесть-PST-1SG</ta>
            <ta e="T245" id="Seg_3075" s="T244">тогда</ta>
            <ta e="T246" id="Seg_3076" s="T245">там</ta>
            <ta e="T247" id="Seg_3077" s="T246">говорить-PST-1SG</ta>
            <ta e="T250" id="Seg_3078" s="T248">тогда</ta>
            <ta e="T292" id="Seg_3079" s="T291">тогда</ta>
            <ta e="T293" id="Seg_3080" s="T292">сказать-FUT-1SG</ta>
            <ta e="T294" id="Seg_3081" s="T293">какой</ta>
            <ta e="T297" id="Seg_3082" s="T296">мы.NOM</ta>
            <ta e="T298" id="Seg_3083" s="T297">земля-ACC</ta>
            <ta e="T299" id="Seg_3084" s="T298">земля-ACC</ta>
            <ta e="T300" id="Seg_3085" s="T299">там</ta>
            <ta e="T301" id="Seg_3086" s="T300">много</ta>
            <ta e="T302" id="Seg_3087" s="T301">ягода.[NOM.SG]</ta>
            <ta e="T303" id="Seg_3088" s="T302">быть-PRS.[3SG]</ta>
            <ta e="T304" id="Seg_3089" s="T303">и</ta>
            <ta e="T305" id="Seg_3090" s="T304">дерево-PL</ta>
            <ta e="T306" id="Seg_3091" s="T305">белый.[NOM.SG]</ta>
            <ta e="T307" id="Seg_3092" s="T306">и</ta>
            <ta e="T308" id="Seg_3093" s="T307">красный-PL</ta>
            <ta e="T309" id="Seg_3094" s="T308">осина-PL</ta>
            <ta e="T310" id="Seg_3095" s="T309">кедр-PL</ta>
            <ta e="T311" id="Seg_3096" s="T310">быть-PRS.[3SG]</ta>
            <ta e="T312" id="Seg_3097" s="T311">дерево.[NOM.SG]</ta>
            <ta e="T313" id="Seg_3098" s="T312">где</ta>
            <ta e="T314" id="Seg_3099" s="T313">кедровый.орех.[NOM.SG]</ta>
            <ta e="T315" id="Seg_3100" s="T314">расти-%%-DUR.[3SG]</ta>
            <ta e="T317" id="Seg_3101" s="T316">быть-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_3102" s="T317">тогда</ta>
            <ta e="T319" id="Seg_3103" s="T318">там</ta>
            <ta e="T320" id="Seg_3104" s="T319">степь-NOM/GEN/ACC.3PL</ta>
            <ta e="T321" id="Seg_3105" s="T320">быть-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3106" s="T321">много</ta>
            <ta e="T323" id="Seg_3107" s="T322">хлеб.[NOM.SG]</ta>
            <ta e="T324" id="Seg_3108" s="T323">мука.[NOM.SG]</ta>
            <ta e="T325" id="Seg_3109" s="T324">хлеб.[NOM.SG]</ta>
            <ta e="T327" id="Seg_3110" s="T326">овес.[NOM.SG]</ta>
            <ta e="T329" id="Seg_3111" s="T327">и</ta>
            <ta e="T377" id="Seg_3112" s="T376">когда</ta>
            <ta e="T378" id="Seg_3113" s="T377">олень-PL</ta>
            <ta e="T379" id="Seg_3114" s="T378">быть-PST-3PL</ta>
            <ta e="T380" id="Seg_3115" s="T379">этот.[NOM.SG]</ta>
            <ta e="T381" id="Seg_3116" s="T380">этот-PL</ta>
            <ta e="T383" id="Seg_3117" s="T382">болеть-PST-3PL</ta>
            <ta e="T384" id="Seg_3118" s="T383">этот-PL</ta>
            <ta e="T385" id="Seg_3119" s="T384">тогда</ta>
            <ta e="T386" id="Seg_3120" s="T385">этот-PL</ta>
            <ta e="T387" id="Seg_3121" s="T386">посылать-PST-3PL</ta>
            <ta e="T388" id="Seg_3122" s="T387">этот-PL</ta>
            <ta e="T389" id="Seg_3123" s="T388">и</ta>
            <ta e="T390" id="Seg_3124" s="T389">лес-LAT</ta>
            <ta e="T391" id="Seg_3125" s="T390">пойти-PST-3PL</ta>
            <ta e="T392" id="Seg_3126" s="T391">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T393" id="Seg_3127" s="T392">сказать-PST.[3SG]</ta>
            <ta e="T394" id="Seg_3128" s="T393">бабушка-NOM/GEN/ACC.1SG</ta>
            <ta e="T395" id="Seg_3129" s="T394">сказать-PST.[3SG]</ta>
            <ta e="T396" id="Seg_3130" s="T395">я.LAT</ta>
            <ta e="T397" id="Seg_3131" s="T396">еще</ta>
            <ta e="T398" id="Seg_3132" s="T397">маленький.[NOM.SG]</ta>
            <ta e="T399" id="Seg_3133" s="T398">быть-PST-1SG</ta>
            <ta e="T417" id="Seg_3134" s="T416">тогда</ta>
            <ta e="T418" id="Seg_3135" s="T417">этот-PL</ta>
            <ta e="T419" id="Seg_3136" s="T418">идти-PST-3PL</ta>
            <ta e="T420" id="Seg_3137" s="T419">лес-LAT</ta>
            <ta e="T421" id="Seg_3138" s="T420">большой.[NOM.SG]</ta>
            <ta e="T422" id="Seg_3139" s="T421">гора-LAT</ta>
            <ta e="T423" id="Seg_3140" s="T422">Белогорье-LAT</ta>
            <ta e="T424" id="Seg_3141" s="T423">где</ta>
            <ta e="T425" id="Seg_3142" s="T424">много</ta>
            <ta e="T426" id="Seg_3143" s="T425">вода.[NOM.SG]</ta>
            <ta e="T427" id="Seg_3144" s="T426">там</ta>
            <ta e="T428" id="Seg_3145" s="T427">рыба.[NOM.SG]</ta>
            <ta e="T429" id="Seg_3146" s="T428">ловить-PST-3PL</ta>
            <ta e="T430" id="Seg_3147" s="T429">и</ta>
            <ta e="T434" id="Seg_3148" s="T433">солить-PST-3PL</ta>
            <ta e="T435" id="Seg_3149" s="T434">и</ta>
            <ta e="T436" id="Seg_3150" s="T435">дом-LAT/LOC.1SG</ta>
            <ta e="T437" id="Seg_3151" s="T436">принести-PST-3PL</ta>
            <ta e="T438" id="Seg_3152" s="T437">там</ta>
            <ta e="T439" id="Seg_3153" s="T438">прииск-LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3154" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_3155" s="T1">propr-n:case</ta>
            <ta e="T3" id="Seg_3156" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_3157" s="T3">pers</ta>
            <ta e="T5" id="Seg_3158" s="T4">v-v:n.fin</ta>
            <ta e="T6" id="Seg_3159" s="T5">pers</ta>
            <ta e="T7" id="Seg_3160" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_3161" s="T7">que</ta>
            <ta e="T9" id="Seg_3162" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_3163" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_3164" s="T10">conj</ta>
            <ta e="T12" id="Seg_3165" s="T11">v-v:n.fin</ta>
            <ta e="T13" id="Seg_3166" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_3167" s="T13">adv</ta>
            <ta e="T15" id="Seg_3168" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_3169" s="T15">v-v:n.fin</ta>
            <ta e="T17" id="Seg_3170" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_3171" s="T17">adv</ta>
            <ta e="T19" id="Seg_3172" s="T18">pers</ta>
            <ta e="T20" id="Seg_3173" s="T19">quant</ta>
            <ta e="T22" id="Seg_3174" s="T21">dempro-n:case</ta>
            <ta e="T24" id="Seg_3175" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_3176" s="T24">refl-n:case.poss</ta>
            <ta e="T26" id="Seg_3177" s="T25">n-n:case.poss-n:case</ta>
            <ta e="T27" id="Seg_3178" s="T26">quant</ta>
            <ta e="T28" id="Seg_3179" s="T27">n-n:num</ta>
            <ta e="T29" id="Seg_3180" s="T28">num-n:case</ta>
            <ta e="T30" id="Seg_3181" s="T29">num-n:case</ta>
            <ta e="T31" id="Seg_3182" s="T30">adv</ta>
            <ta e="T32" id="Seg_3183" s="T31">num-num&gt;adv</ta>
            <ta e="T35" id="Seg_3184" s="T34">num-num&gt;adv</ta>
            <ta e="T37" id="Seg_3185" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_3186" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_3187" s="T38">pers</ta>
            <ta e="T40" id="Seg_3188" s="T39">conj</ta>
            <ta e="T441" id="Seg_3189" s="T40">v-v:n-fin</ta>
            <ta e="T41" id="Seg_3190" s="T441">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_3191" s="T41">adv</ta>
            <ta e="T43" id="Seg_3192" s="T42">pers</ta>
            <ta e="T44" id="Seg_3193" s="T43">adv</ta>
            <ta e="T46" id="Seg_3194" s="T45">dempro-n:case</ta>
            <ta e="T47" id="Seg_3195" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_3196" s="T47">dempro-n:case</ta>
            <ta e="T50" id="Seg_3197" s="T49">adv</ta>
            <ta e="T51" id="Seg_3198" s="T50">dempro-n:case</ta>
            <ta e="T52" id="Seg_3199" s="T51">dempro-n:case</ta>
            <ta e="T53" id="Seg_3200" s="T52">pers</ta>
            <ta e="T54" id="Seg_3201" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_3202" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_3203" s="T55">pers</ta>
            <ta e="T57" id="Seg_3204" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_3205" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3206" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_3207" s="T59">num-n:case</ta>
            <ta e="T61" id="Seg_3208" s="T60">num-n:case</ta>
            <ta e="T62" id="Seg_3209" s="T61">num-n:case</ta>
            <ta e="T63" id="Seg_3210" s="T62">num-n:case</ta>
            <ta e="T64" id="Seg_3211" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_3212" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_3213" s="T65">pers</ta>
            <ta e="T67" id="Seg_3214" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_3215" s="T67">adv</ta>
            <ta e="T70" id="Seg_3216" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_3217" s="T70">adv</ta>
            <ta e="T72" id="Seg_3218" s="T71">adv</ta>
            <ta e="T73" id="Seg_3219" s="T72">pers</ta>
            <ta e="T74" id="Seg_3220" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_3221" s="T74">que</ta>
            <ta e="T76" id="Seg_3222" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_3223" s="T76">propr-n:case</ta>
            <ta e="T79" id="Seg_3224" s="T78">adv</ta>
            <ta e="T80" id="Seg_3225" s="T79">pers</ta>
            <ta e="T82" id="Seg_3226" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_3227" s="T82">dempro-n:case</ta>
            <ta e="T85" id="Seg_3228" s="T84">adv</ta>
            <ta e="T86" id="Seg_3229" s="T85">dempro-n:case</ta>
            <ta e="T87" id="Seg_3230" s="T86">refl-n:case.poss</ta>
            <ta e="T88" id="Seg_3231" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_3232" s="T88">pers</ta>
            <ta e="T90" id="Seg_3233" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_3234" s="T90">v-v:n.fin</ta>
            <ta e="T92" id="Seg_3235" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_3236" s="T92">n-n:case</ta>
            <ta e="T95" id="Seg_3237" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_3238" s="T95">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_3239" s="T209">adv</ta>
            <ta e="T212" id="Seg_3240" s="T211">pers</ta>
            <ta e="T213" id="Seg_3241" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_3242" s="T213">pers</ta>
            <ta e="T216" id="Seg_3243" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_3244" s="T216">dempro-n:case</ta>
            <ta e="T218" id="Seg_3245" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_3246" s="T218">propr-n:case</ta>
            <ta e="T220" id="Seg_3247" s="T219">adv</ta>
            <ta e="T221" id="Seg_3248" s="T220">pers</ta>
            <ta e="T222" id="Seg_3249" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_3250" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_3251" s="T223">pers</ta>
            <ta e="T226" id="Seg_3252" s="T225">num-n:case</ta>
            <ta e="T227" id="Seg_3253" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_3254" s="T227">adv</ta>
            <ta e="T229" id="Seg_3255" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_3256" s="T229">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_3257" s="T230">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_3258" s="T231">adv</ta>
            <ta e="T237" id="Seg_3259" s="T236">dempro-n:case</ta>
            <ta e="T238" id="Seg_3260" s="T237">que</ta>
            <ta e="T239" id="Seg_3261" s="T238">que-n:case=ptcl</ta>
            <ta e="T240" id="Seg_3262" s="T239">v-v:pn</ta>
            <ta e="T241" id="Seg_3263" s="T240">adv</ta>
            <ta e="T242" id="Seg_3264" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3265" s="T242">n-n:num</ta>
            <ta e="T244" id="Seg_3266" s="T243">v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_3267" s="T244">adv</ta>
            <ta e="T246" id="Seg_3268" s="T245">adv</ta>
            <ta e="T247" id="Seg_3269" s="T246">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_3270" s="T248">adv</ta>
            <ta e="T292" id="Seg_3271" s="T291">adv</ta>
            <ta e="T293" id="Seg_3272" s="T292">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_3273" s="T293">que</ta>
            <ta e="T297" id="Seg_3274" s="T296">pers</ta>
            <ta e="T298" id="Seg_3275" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_3276" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_3277" s="T299">adv</ta>
            <ta e="T301" id="Seg_3278" s="T300">quant</ta>
            <ta e="T302" id="Seg_3279" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_3280" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_3281" s="T303">conj</ta>
            <ta e="T305" id="Seg_3282" s="T304">n-n:num</ta>
            <ta e="T306" id="Seg_3283" s="T305">adj-n:case</ta>
            <ta e="T307" id="Seg_3284" s="T306">conj</ta>
            <ta e="T308" id="Seg_3285" s="T307">adj-n:num</ta>
            <ta e="T309" id="Seg_3286" s="T308">n-n:num</ta>
            <ta e="T310" id="Seg_3287" s="T309">n-n:num</ta>
            <ta e="T311" id="Seg_3288" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_3289" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_3290" s="T312">que</ta>
            <ta e="T314" id="Seg_3291" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_3292" s="T314">v-%%-v&gt;v-v:pn</ta>
            <ta e="T317" id="Seg_3293" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_3294" s="T317">adv</ta>
            <ta e="T319" id="Seg_3295" s="T318">adv</ta>
            <ta e="T320" id="Seg_3296" s="T319">n-n:case.poss</ta>
            <ta e="T321" id="Seg_3297" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_3298" s="T321">quant</ta>
            <ta e="T323" id="Seg_3299" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_3300" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_3301" s="T324">n-n:case</ta>
            <ta e="T327" id="Seg_3302" s="T326">n-n:case</ta>
            <ta e="T329" id="Seg_3303" s="T327">conj</ta>
            <ta e="T377" id="Seg_3304" s="T376">que</ta>
            <ta e="T378" id="Seg_3305" s="T377">n-n:num</ta>
            <ta e="T379" id="Seg_3306" s="T378">v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_3307" s="T379">dempro-n:case</ta>
            <ta e="T381" id="Seg_3308" s="T380">dempro-n:num</ta>
            <ta e="T383" id="Seg_3309" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_3310" s="T383">dempro-n:num</ta>
            <ta e="T385" id="Seg_3311" s="T384">adv</ta>
            <ta e="T386" id="Seg_3312" s="T385">dempro-n:num</ta>
            <ta e="T387" id="Seg_3313" s="T386">v-v:tense-v:pn</ta>
            <ta e="T388" id="Seg_3314" s="T387">dempro-n:num</ta>
            <ta e="T389" id="Seg_3315" s="T388">conj</ta>
            <ta e="T390" id="Seg_3316" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_3317" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_3318" s="T391">n-n:case.poss</ta>
            <ta e="T393" id="Seg_3319" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_3320" s="T393">n-n:case.poss</ta>
            <ta e="T395" id="Seg_3321" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_3322" s="T395">pers</ta>
            <ta e="T397" id="Seg_3323" s="T396">adv</ta>
            <ta e="T398" id="Seg_3324" s="T397">adj-n:case</ta>
            <ta e="T399" id="Seg_3325" s="T398">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_3326" s="T416">adv</ta>
            <ta e="T418" id="Seg_3327" s="T417">dempro-n:num</ta>
            <ta e="T419" id="Seg_3328" s="T418">v-v:tense-v:pn</ta>
            <ta e="T420" id="Seg_3329" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_3330" s="T420">adj-n:case</ta>
            <ta e="T422" id="Seg_3331" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_3332" s="T422">propr-n:case</ta>
            <ta e="T424" id="Seg_3333" s="T423">que</ta>
            <ta e="T425" id="Seg_3334" s="T424">quant</ta>
            <ta e="T426" id="Seg_3335" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_3336" s="T426">adv</ta>
            <ta e="T428" id="Seg_3337" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_3338" s="T428">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_3339" s="T429">conj</ta>
            <ta e="T434" id="Seg_3340" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_3341" s="T434">conj</ta>
            <ta e="T436" id="Seg_3342" s="T435">n-n:case.poss</ta>
            <ta e="T437" id="Seg_3343" s="T436">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_3344" s="T437">adv</ta>
            <ta e="T439" id="Seg_3345" s="T438">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3346" s="T0">propr</ta>
            <ta e="T2" id="Seg_3347" s="T1">propr</ta>
            <ta e="T3" id="Seg_3348" s="T2">v</ta>
            <ta e="T4" id="Seg_3349" s="T3">pers</ta>
            <ta e="T5" id="Seg_3350" s="T4">v</ta>
            <ta e="T6" id="Seg_3351" s="T5">pers</ta>
            <ta e="T7" id="Seg_3352" s="T6">v</ta>
            <ta e="T8" id="Seg_3353" s="T7">que</ta>
            <ta e="T9" id="Seg_3354" s="T8">v</ta>
            <ta e="T10" id="Seg_3355" s="T9">n</ta>
            <ta e="T11" id="Seg_3356" s="T10">conj</ta>
            <ta e="T12" id="Seg_3357" s="T11">v</ta>
            <ta e="T13" id="Seg_3358" s="T12">v</ta>
            <ta e="T14" id="Seg_3359" s="T13">adv</ta>
            <ta e="T15" id="Seg_3360" s="T14">v</ta>
            <ta e="T16" id="Seg_3361" s="T15">v</ta>
            <ta e="T17" id="Seg_3362" s="T16">v</ta>
            <ta e="T18" id="Seg_3363" s="T17">adv</ta>
            <ta e="T19" id="Seg_3364" s="T18">pers</ta>
            <ta e="T20" id="Seg_3365" s="T19">quant</ta>
            <ta e="T22" id="Seg_3366" s="T21">dempro</ta>
            <ta e="T24" id="Seg_3367" s="T23">v</ta>
            <ta e="T25" id="Seg_3368" s="T24">refl</ta>
            <ta e="T26" id="Seg_3369" s="T25">n</ta>
            <ta e="T27" id="Seg_3370" s="T26">quant</ta>
            <ta e="T28" id="Seg_3371" s="T27">n</ta>
            <ta e="T29" id="Seg_3372" s="T28">num</ta>
            <ta e="T30" id="Seg_3373" s="T29">num</ta>
            <ta e="T31" id="Seg_3374" s="T30">adv</ta>
            <ta e="T32" id="Seg_3375" s="T31">adv</ta>
            <ta e="T35" id="Seg_3376" s="T34">adv</ta>
            <ta e="T37" id="Seg_3377" s="T36">n</ta>
            <ta e="T38" id="Seg_3378" s="T37">v</ta>
            <ta e="T39" id="Seg_3379" s="T38">pers</ta>
            <ta e="T40" id="Seg_3380" s="T39">conj</ta>
            <ta e="T441" id="Seg_3381" s="T40">v</ta>
            <ta e="T41" id="Seg_3382" s="T441">v</ta>
            <ta e="T42" id="Seg_3383" s="T41">adv</ta>
            <ta e="T43" id="Seg_3384" s="T42">pers</ta>
            <ta e="T44" id="Seg_3385" s="T43">adv</ta>
            <ta e="T46" id="Seg_3386" s="T45">dempro</ta>
            <ta e="T47" id="Seg_3387" s="T46">v</ta>
            <ta e="T48" id="Seg_3388" s="T47">dempro</ta>
            <ta e="T50" id="Seg_3389" s="T49">adv</ta>
            <ta e="T51" id="Seg_3390" s="T50">dempro</ta>
            <ta e="T52" id="Seg_3391" s="T51">dempro</ta>
            <ta e="T53" id="Seg_3392" s="T52">pers</ta>
            <ta e="T54" id="Seg_3393" s="T53">n</ta>
            <ta e="T55" id="Seg_3394" s="T54">v</ta>
            <ta e="T56" id="Seg_3395" s="T55">pers</ta>
            <ta e="T57" id="Seg_3396" s="T56">dempro</ta>
            <ta e="T58" id="Seg_3397" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3398" s="T58">v</ta>
            <ta e="T60" id="Seg_3399" s="T59">num</ta>
            <ta e="T61" id="Seg_3400" s="T60">num</ta>
            <ta e="T62" id="Seg_3401" s="T61">num</ta>
            <ta e="T63" id="Seg_3402" s="T62">num</ta>
            <ta e="T64" id="Seg_3403" s="T63">n</ta>
            <ta e="T65" id="Seg_3404" s="T64">dempro</ta>
            <ta e="T66" id="Seg_3405" s="T65">pers</ta>
            <ta e="T67" id="Seg_3406" s="T66">v</ta>
            <ta e="T68" id="Seg_3407" s="T67">adv</ta>
            <ta e="T70" id="Seg_3408" s="T69">v</ta>
            <ta e="T71" id="Seg_3409" s="T70">adv</ta>
            <ta e="T72" id="Seg_3410" s="T71">adv</ta>
            <ta e="T73" id="Seg_3411" s="T72">pers</ta>
            <ta e="T74" id="Seg_3412" s="T73">v</ta>
            <ta e="T75" id="Seg_3413" s="T74">conj</ta>
            <ta e="T76" id="Seg_3414" s="T75">v</ta>
            <ta e="T77" id="Seg_3415" s="T76">propr</ta>
            <ta e="T79" id="Seg_3416" s="T78">adv</ta>
            <ta e="T80" id="Seg_3417" s="T79">pers</ta>
            <ta e="T82" id="Seg_3418" s="T81">v</ta>
            <ta e="T83" id="Seg_3419" s="T82">dempro</ta>
            <ta e="T85" id="Seg_3420" s="T84">adv</ta>
            <ta e="T86" id="Seg_3421" s="T85">dempro</ta>
            <ta e="T87" id="Seg_3422" s="T86">refl</ta>
            <ta e="T88" id="Seg_3423" s="T87">v</ta>
            <ta e="T89" id="Seg_3424" s="T88">pers</ta>
            <ta e="T90" id="Seg_3425" s="T89">v</ta>
            <ta e="T91" id="Seg_3426" s="T90">v</ta>
            <ta e="T92" id="Seg_3427" s="T91">v</ta>
            <ta e="T93" id="Seg_3428" s="T92">n</ta>
            <ta e="T95" id="Seg_3429" s="T94">n</ta>
            <ta e="T96" id="Seg_3430" s="T95">v</ta>
            <ta e="T210" id="Seg_3431" s="T209">adv</ta>
            <ta e="T212" id="Seg_3432" s="T211">pers</ta>
            <ta e="T213" id="Seg_3433" s="T212">v</ta>
            <ta e="T214" id="Seg_3434" s="T213">pers</ta>
            <ta e="T216" id="Seg_3435" s="T215">v</ta>
            <ta e="T217" id="Seg_3436" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3437" s="T217">n</ta>
            <ta e="T219" id="Seg_3438" s="T218">propr</ta>
            <ta e="T220" id="Seg_3439" s="T219">adv</ta>
            <ta e="T221" id="Seg_3440" s="T220">pers</ta>
            <ta e="T222" id="Seg_3441" s="T221">v</ta>
            <ta e="T223" id="Seg_3442" s="T222">v</ta>
            <ta e="T224" id="Seg_3443" s="T223">pers</ta>
            <ta e="T226" id="Seg_3444" s="T225">num</ta>
            <ta e="T227" id="Seg_3445" s="T226">n</ta>
            <ta e="T228" id="Seg_3446" s="T227">adv</ta>
            <ta e="T229" id="Seg_3447" s="T228">n</ta>
            <ta e="T230" id="Seg_3448" s="T229">v</ta>
            <ta e="T231" id="Seg_3449" s="T230">v</ta>
            <ta e="T232" id="Seg_3450" s="T231">adv</ta>
            <ta e="T237" id="Seg_3451" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3452" s="T237">que</ta>
            <ta e="T239" id="Seg_3453" s="T238">que</ta>
            <ta e="T240" id="Seg_3454" s="T239">v</ta>
            <ta e="T241" id="Seg_3455" s="T240">adv</ta>
            <ta e="T242" id="Seg_3456" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3457" s="T242">n</ta>
            <ta e="T244" id="Seg_3458" s="T243">v</ta>
            <ta e="T245" id="Seg_3459" s="T244">adv</ta>
            <ta e="T246" id="Seg_3460" s="T245">adv</ta>
            <ta e="T247" id="Seg_3461" s="T246">v</ta>
            <ta e="T250" id="Seg_3462" s="T248">adv</ta>
            <ta e="T292" id="Seg_3463" s="T291">adv</ta>
            <ta e="T293" id="Seg_3464" s="T292">v</ta>
            <ta e="T294" id="Seg_3465" s="T293">que</ta>
            <ta e="T297" id="Seg_3466" s="T296">pers</ta>
            <ta e="T298" id="Seg_3467" s="T297">n</ta>
            <ta e="T299" id="Seg_3468" s="T298">n</ta>
            <ta e="T300" id="Seg_3469" s="T299">adv</ta>
            <ta e="T301" id="Seg_3470" s="T300">quant</ta>
            <ta e="T302" id="Seg_3471" s="T301">n</ta>
            <ta e="T303" id="Seg_3472" s="T302">v</ta>
            <ta e="T304" id="Seg_3473" s="T303">conj</ta>
            <ta e="T305" id="Seg_3474" s="T304">n</ta>
            <ta e="T306" id="Seg_3475" s="T305">adj</ta>
            <ta e="T307" id="Seg_3476" s="T306">conj</ta>
            <ta e="T308" id="Seg_3477" s="T307">adj</ta>
            <ta e="T309" id="Seg_3478" s="T308">n</ta>
            <ta e="T310" id="Seg_3479" s="T309">n</ta>
            <ta e="T311" id="Seg_3480" s="T310">v</ta>
            <ta e="T312" id="Seg_3481" s="T311">n</ta>
            <ta e="T313" id="Seg_3482" s="T312">que</ta>
            <ta e="T314" id="Seg_3483" s="T313">n</ta>
            <ta e="T315" id="Seg_3484" s="T314">v</ta>
            <ta e="T317" id="Seg_3485" s="T316">v</ta>
            <ta e="T318" id="Seg_3486" s="T317">adv</ta>
            <ta e="T319" id="Seg_3487" s="T318">adv</ta>
            <ta e="T320" id="Seg_3488" s="T319">n</ta>
            <ta e="T321" id="Seg_3489" s="T320">v</ta>
            <ta e="T322" id="Seg_3490" s="T321">quant</ta>
            <ta e="T323" id="Seg_3491" s="T322">n</ta>
            <ta e="T324" id="Seg_3492" s="T323">n</ta>
            <ta e="T325" id="Seg_3493" s="T324">n</ta>
            <ta e="T327" id="Seg_3494" s="T326">n</ta>
            <ta e="T329" id="Seg_3495" s="T327">conj</ta>
            <ta e="T377" id="Seg_3496" s="T376">conj</ta>
            <ta e="T378" id="Seg_3497" s="T377">n</ta>
            <ta e="T379" id="Seg_3498" s="T378">v</ta>
            <ta e="T380" id="Seg_3499" s="T379">dempro</ta>
            <ta e="T381" id="Seg_3500" s="T380">dempro</ta>
            <ta e="T383" id="Seg_3501" s="T382">v</ta>
            <ta e="T384" id="Seg_3502" s="T383">dempro</ta>
            <ta e="T385" id="Seg_3503" s="T384">adv</ta>
            <ta e="T386" id="Seg_3504" s="T385">dempro</ta>
            <ta e="T387" id="Seg_3505" s="T386">v</ta>
            <ta e="T388" id="Seg_3506" s="T387">dempro</ta>
            <ta e="T389" id="Seg_3507" s="T388">conj</ta>
            <ta e="T390" id="Seg_3508" s="T389">n</ta>
            <ta e="T391" id="Seg_3509" s="T390">v</ta>
            <ta e="T392" id="Seg_3510" s="T391">n</ta>
            <ta e="T393" id="Seg_3511" s="T392">v</ta>
            <ta e="T394" id="Seg_3512" s="T393">n</ta>
            <ta e="T395" id="Seg_3513" s="T394">v</ta>
            <ta e="T396" id="Seg_3514" s="T395">pers</ta>
            <ta e="T397" id="Seg_3515" s="T396">adv</ta>
            <ta e="T398" id="Seg_3516" s="T397">adj</ta>
            <ta e="T399" id="Seg_3517" s="T398">v</ta>
            <ta e="T417" id="Seg_3518" s="T416">adv</ta>
            <ta e="T418" id="Seg_3519" s="T417">dempro</ta>
            <ta e="T419" id="Seg_3520" s="T418">v</ta>
            <ta e="T420" id="Seg_3521" s="T419">n</ta>
            <ta e="T421" id="Seg_3522" s="T420">adj</ta>
            <ta e="T422" id="Seg_3523" s="T421">n</ta>
            <ta e="T423" id="Seg_3524" s="T422">propr</ta>
            <ta e="T424" id="Seg_3525" s="T423">que</ta>
            <ta e="T425" id="Seg_3526" s="T424">quant</ta>
            <ta e="T426" id="Seg_3527" s="T425">n</ta>
            <ta e="T427" id="Seg_3528" s="T426">adv</ta>
            <ta e="T428" id="Seg_3529" s="T427">n</ta>
            <ta e="T429" id="Seg_3530" s="T428">v</ta>
            <ta e="T430" id="Seg_3531" s="T429">conj</ta>
            <ta e="T434" id="Seg_3532" s="T433">v</ta>
            <ta e="T435" id="Seg_3533" s="T434">conj</ta>
            <ta e="T436" id="Seg_3534" s="T435">n</ta>
            <ta e="T437" id="Seg_3535" s="T436">v</ta>
            <ta e="T438" id="Seg_3536" s="T437">adv</ta>
            <ta e="T439" id="Seg_3537" s="T438">n</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3538" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_3539" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_3540" s="T3">s:purp</ta>
            <ta e="T6" id="Seg_3541" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_3542" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_3543" s="T8">0.2.h:S v:pred</ta>
            <ta e="T12" id="Seg_3544" s="T11">conv:pred</ta>
            <ta e="T13" id="Seg_3545" s="T12">0.2.h:S v:pred</ta>
            <ta e="T15" id="Seg_3546" s="T14">0.3.h:S</ta>
            <ta e="T16" id="Seg_3547" s="T15">conv:pred</ta>
            <ta e="T17" id="Seg_3548" s="T16">0.1.h:S v:pred</ta>
            <ta e="T19" id="Seg_3549" s="T18">pro.h:S</ta>
            <ta e="T24" id="Seg_3550" s="T23">v:pred</ta>
            <ta e="T38" id="Seg_3551" s="T37">0.3.h:S v:pred</ta>
            <ta e="T441" id="Seg_3552" s="T40">conv:pred</ta>
            <ta e="T41" id="Seg_3553" s="T441">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_3554" s="T46">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_3555" s="T51">pro.h:S</ta>
            <ta e="T54" id="Seg_3556" s="T53">np:O</ta>
            <ta e="T55" id="Seg_3557" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_3558" s="T55">pro.h:S</ta>
            <ta e="T59" id="Seg_3559" s="T58">v:pred</ta>
            <ta e="T67" id="Seg_3560" s="T66">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_3561" s="T69">0.3.h:S v:pred</ta>
            <ta e="T73" id="Seg_3562" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_3563" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_3564" s="T74">s:temp</ta>
            <ta e="T82" id="Seg_3565" s="T81">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_3566" s="T85">pro.h:S</ta>
            <ta e="T88" id="Seg_3567" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_3568" s="T88">pro.h:O</ta>
            <ta e="T90" id="Seg_3569" s="T89">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_3570" s="T91">0.1.h:S v:pred</ta>
            <ta e="T96" id="Seg_3571" s="T95">0.1.h:S v:pred</ta>
            <ta e="T212" id="Seg_3572" s="T211">pro.h:S</ta>
            <ta e="T213" id="Seg_3573" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_3574" s="T213">pro.h:O</ta>
            <ta e="T216" id="Seg_3575" s="T215">0.3.h:S v:pred</ta>
            <ta e="T221" id="Seg_3576" s="T220">pro.h:S</ta>
            <ta e="T222" id="Seg_3577" s="T221">v:pred</ta>
            <ta e="T223" id="Seg_3578" s="T222">v:pred</ta>
            <ta e="T227" id="Seg_3579" s="T226">np.h:S</ta>
            <ta e="T230" id="Seg_3580" s="T229">0.3.h:S v:pred</ta>
            <ta e="T231" id="Seg_3581" s="T230">0.3.h:S v:pred</ta>
            <ta e="T239" id="Seg_3582" s="T238">pro.h:S</ta>
            <ta e="T240" id="Seg_3583" s="T239">v:pred</ta>
            <ta e="T243" id="Seg_3584" s="T242">np:O</ta>
            <ta e="T244" id="Seg_3585" s="T243">0.1.h:S v:pred</ta>
            <ta e="T247" id="Seg_3586" s="T246">0.1.h:S v:pred</ta>
            <ta e="T293" id="Seg_3587" s="T292">0.1.h:S v:pred</ta>
            <ta e="T302" id="Seg_3588" s="T301">np:S</ta>
            <ta e="T303" id="Seg_3589" s="T302">v:pred</ta>
            <ta e="T305" id="Seg_3590" s="T304">np:S</ta>
            <ta e="T309" id="Seg_3591" s="T308">np:S</ta>
            <ta e="T310" id="Seg_3592" s="T309">np:S</ta>
            <ta e="T311" id="Seg_3593" s="T310">v:pred</ta>
            <ta e="T315" id="Seg_3594" s="T312">s:adv</ta>
            <ta e="T316" id="Seg_3595" s="T315">np:S</ta>
            <ta e="T317" id="Seg_3596" s="T316">v:pred</ta>
            <ta e="T320" id="Seg_3597" s="T319">np:S</ta>
            <ta e="T321" id="Seg_3598" s="T320">v:pred</ta>
            <ta e="T379" id="Seg_3599" s="T376">s:temp</ta>
            <ta e="T381" id="Seg_3600" s="T380">pro.h:S</ta>
            <ta e="T383" id="Seg_3601" s="T382">pro.h:S</ta>
            <ta e="T384" id="Seg_3602" s="T383">pro.h:S</ta>
            <ta e="T386" id="Seg_3603" s="T385">pro.h:O</ta>
            <ta e="T387" id="Seg_3604" s="T386">v:pred 0.3.h:S</ta>
            <ta e="T388" id="Seg_3605" s="T387">pro:S</ta>
            <ta e="T391" id="Seg_3606" s="T390">v:pred</ta>
            <ta e="T392" id="Seg_3607" s="T391">np.h:S</ta>
            <ta e="T393" id="Seg_3608" s="T392">v:pred</ta>
            <ta e="T394" id="Seg_3609" s="T393">np.h:S</ta>
            <ta e="T395" id="Seg_3610" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_3611" s="T397">adj:pred</ta>
            <ta e="T399" id="Seg_3612" s="T398">0.1.h:S cop</ta>
            <ta e="T418" id="Seg_3613" s="T417">pro.h:S</ta>
            <ta e="T419" id="Seg_3614" s="T418">v:pred</ta>
            <ta e="T425" id="Seg_3615" s="T424">adj:pred</ta>
            <ta e="T426" id="Seg_3616" s="T425">np:S</ta>
            <ta e="T428" id="Seg_3617" s="T427">np:O</ta>
            <ta e="T429" id="Seg_3618" s="T428">0.3.h:S v:pred</ta>
            <ta e="T434" id="Seg_3619" s="T433">0.3.h:S v:pred</ta>
            <ta e="T437" id="Seg_3620" s="T436">0.3.h:S 0.3.h:S</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3621" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_3622" s="T3">pro.h:R</ta>
            <ta e="T6" id="Seg_3623" s="T5">pro.h:A</ta>
            <ta e="T9" id="Seg_3624" s="T8">0.2.h:A</ta>
            <ta e="T10" id="Seg_3625" s="T9">np:Ins</ta>
            <ta e="T13" id="Seg_3626" s="T12">0.2.h:A</ta>
            <ta e="T14" id="Seg_3627" s="T13">adv:L</ta>
            <ta e="T15" id="Seg_3628" s="T14">0.3.h:A</ta>
            <ta e="T17" id="Seg_3629" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_3630" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_3631" s="T18">pro.h:A</ta>
            <ta e="T22" id="Seg_3632" s="T21">pro.h:Com</ta>
            <ta e="T26" id="Seg_3633" s="T25">np:Ins</ta>
            <ta e="T31" id="Seg_3634" s="T30">adv:Time</ta>
            <ta e="T38" id="Seg_3635" s="T37">0.3.h:Th</ta>
            <ta e="T39" id="Seg_3636" s="T38">pro:L</ta>
            <ta e="T41" id="Seg_3637" s="T441">0.3.h:A</ta>
            <ta e="T44" id="Seg_3638" s="T43">adv:Time</ta>
            <ta e="T47" id="Seg_3639" s="T46">0.3.h:A</ta>
            <ta e="T50" id="Seg_3640" s="T49">adv:Time</ta>
            <ta e="T52" id="Seg_3641" s="T51">pro.h:A</ta>
            <ta e="T53" id="Seg_3642" s="T52">pro.h:R</ta>
            <ta e="T54" id="Seg_3643" s="T53">np:P</ta>
            <ta e="T56" id="Seg_3644" s="T55">pro.h:A</ta>
            <ta e="T57" id="Seg_3645" s="T56">pro.h:R</ta>
            <ta e="T64" id="Seg_3646" s="T63">n:Time</ta>
            <ta e="T66" id="Seg_3647" s="T65">pro.h:R</ta>
            <ta e="T67" id="Seg_3648" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_3649" s="T67">adv:Time</ta>
            <ta e="T70" id="Seg_3650" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_3651" s="T70">adv:L</ta>
            <ta e="T72" id="Seg_3652" s="T71">adv:Time</ta>
            <ta e="T73" id="Seg_3653" s="T72">pro.h:A</ta>
            <ta e="T77" id="Seg_3654" s="T76">np.h:A</ta>
            <ta e="T79" id="Seg_3655" s="T78">adv:Time</ta>
            <ta e="T80" id="Seg_3656" s="T79">pro:L</ta>
            <ta e="T82" id="Seg_3657" s="T81">0.1.h:A</ta>
            <ta e="T85" id="Seg_3658" s="T84">adv:Time</ta>
            <ta e="T86" id="Seg_3659" s="T85">pro.h:A</ta>
            <ta e="T89" id="Seg_3660" s="T88">pro.h:Th</ta>
            <ta e="T90" id="Seg_3661" s="T89">0.3.h:A</ta>
            <ta e="T92" id="Seg_3662" s="T91">0.1.h:A</ta>
            <ta e="T93" id="Seg_3663" s="T92">np:Ins</ta>
            <ta e="T95" id="Seg_3664" s="T94">np:Ins</ta>
            <ta e="T96" id="Seg_3665" s="T95">0.1.h:A</ta>
            <ta e="T210" id="Seg_3666" s="T209">adv:Time</ta>
            <ta e="T212" id="Seg_3667" s="T211">pro.h:A</ta>
            <ta e="T214" id="Seg_3668" s="T213">pro.h:Th</ta>
            <ta e="T216" id="Seg_3669" s="T215">0.3.h:A</ta>
            <ta e="T218" id="Seg_3670" s="T217">np:G</ta>
            <ta e="T219" id="Seg_3671" s="T218">np:G</ta>
            <ta e="T220" id="Seg_3672" s="T219">adv:Time</ta>
            <ta e="T221" id="Seg_3673" s="T220">pro.h:A</ta>
            <ta e="T224" id="Seg_3674" s="T223">pro:G</ta>
            <ta e="T227" id="Seg_3675" s="T226">np.h:A</ta>
            <ta e="T228" id="Seg_3676" s="T227">adv:Time</ta>
            <ta e="T229" id="Seg_3677" s="T228">np:G</ta>
            <ta e="T230" id="Seg_3678" s="T229">0.3.h:A</ta>
            <ta e="T231" id="Seg_3679" s="T230">0.3.h:A</ta>
            <ta e="T239" id="Seg_3680" s="T238">pro.h:Th</ta>
            <ta e="T241" id="Seg_3681" s="T240">adv:L</ta>
            <ta e="T243" id="Seg_3682" s="T242">np:P</ta>
            <ta e="T244" id="Seg_3683" s="T243">0.1.h:A</ta>
            <ta e="T245" id="Seg_3684" s="T244">adv:Time</ta>
            <ta e="T246" id="Seg_3685" s="T245">adv:L</ta>
            <ta e="T247" id="Seg_3686" s="T246">0.1.h:A</ta>
            <ta e="T292" id="Seg_3687" s="T291">adv:Time</ta>
            <ta e="T293" id="Seg_3688" s="T292">0.1.h:A</ta>
            <ta e="T297" id="Seg_3689" s="T296">pro.h:Poss</ta>
            <ta e="T299" id="Seg_3690" s="T298">np:Th</ta>
            <ta e="T300" id="Seg_3691" s="T299">adv:L</ta>
            <ta e="T302" id="Seg_3692" s="T301">np:Th</ta>
            <ta e="T305" id="Seg_3693" s="T304">np:Th</ta>
            <ta e="T309" id="Seg_3694" s="T308">np:Th</ta>
            <ta e="T310" id="Seg_3695" s="T309">np:Th</ta>
            <ta e="T314" id="Seg_3696" s="T313">np:P</ta>
            <ta e="T318" id="Seg_3697" s="T317">adv:Time</ta>
            <ta e="T319" id="Seg_3698" s="T318">adv:L</ta>
            <ta e="T320" id="Seg_3699" s="T319">np:Th</ta>
            <ta e="T378" id="Seg_3700" s="T377">np:Th</ta>
            <ta e="T381" id="Seg_3701" s="T380">pro.h:E</ta>
            <ta e="T385" id="Seg_3702" s="T384">adv:Time</ta>
            <ta e="T386" id="Seg_3703" s="T385">pro.h:Th</ta>
            <ta e="T387" id="Seg_3704" s="T386">0.3.h:A</ta>
            <ta e="T388" id="Seg_3705" s="T387">pro:A</ta>
            <ta e="T390" id="Seg_3706" s="T389">np:G</ta>
            <ta e="T392" id="Seg_3707" s="T391">0.1.h:Poss np.h:A</ta>
            <ta e="T394" id="Seg_3708" s="T393">np.h:A 0.1.h:Poss</ta>
            <ta e="T396" id="Seg_3709" s="T395">pro.h:R</ta>
            <ta e="T399" id="Seg_3710" s="T398">0.1.h:Th</ta>
            <ta e="T417" id="Seg_3711" s="T416">adv:Time</ta>
            <ta e="T418" id="Seg_3712" s="T417">pro.h:A</ta>
            <ta e="T420" id="Seg_3713" s="T419">np:G</ta>
            <ta e="T422" id="Seg_3714" s="T421">np:G</ta>
            <ta e="T426" id="Seg_3715" s="T425">np:Th</ta>
            <ta e="T427" id="Seg_3716" s="T426">adv:L</ta>
            <ta e="T428" id="Seg_3717" s="T427">np:P</ta>
            <ta e="T429" id="Seg_3718" s="T428">0.3.h:A</ta>
            <ta e="T434" id="Seg_3719" s="T433">0.3.h:A</ta>
            <ta e="T436" id="Seg_3720" s="T435">np:G</ta>
            <ta e="T437" id="Seg_3721" s="T436">0.3.h:A</ta>
            <ta e="T438" id="Seg_3722" s="T437">adv:L</ta>
            <ta e="T439" id="Seg_3723" s="T438">np:G</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_3724" s="T0">RUS:cult</ta>
            <ta e="T2" id="Seg_3725" s="T1">RUS:cult</ta>
            <ta e="T5" id="Seg_3726" s="T4">%TURK:core</ta>
            <ta e="T10" id="Seg_3727" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_3728" s="T10">RUS:gram</ta>
            <ta e="T24" id="Seg_3729" s="T23">%TURK:core</ta>
            <ta e="T28" id="Seg_3730" s="T27">RUS:cult</ta>
            <ta e="T37" id="Seg_3731" s="T36">RUS:core</ta>
            <ta e="T40" id="Seg_3732" s="T39">RUS:gram</ta>
            <ta e="T58" id="Seg_3733" s="T57">RUS:mod</ta>
            <ta e="T93" id="Seg_3734" s="T92">RUS:cult</ta>
            <ta e="T95" id="Seg_3735" s="T94">RUS:cult</ta>
            <ta e="T218" id="Seg_3736" s="T217">TAT:cult</ta>
            <ta e="T229" id="Seg_3737" s="T228">RUS:cult</ta>
            <ta e="T239" id="Seg_3738" s="T238">TURK:gram(INDEF)</ta>
            <ta e="T242" id="Seg_3739" s="T241">TURK:disc</ta>
            <ta e="T247" id="Seg_3740" s="T246">%TURK:core</ta>
            <ta e="T304" id="Seg_3741" s="T303">RUS:gram</ta>
            <ta e="T307" id="Seg_3742" s="T306">RUS:gram</ta>
            <ta e="T309" id="Seg_3743" s="T308">RUS:core</ta>
            <ta e="T310" id="Seg_3744" s="T309">RUS:core</ta>
            <ta e="T320" id="Seg_3745" s="T319">RUS:core</ta>
            <ta e="T329" id="Seg_3746" s="T327">RUS:gram</ta>
            <ta e="T378" id="Seg_3747" s="T377">RUS:core</ta>
            <ta e="T389" id="Seg_3748" s="T388">RUS:gram</ta>
            <ta e="T390" id="Seg_3749" s="T389">RUS:core</ta>
            <ta e="T397" id="Seg_3750" s="T396">RUS:mod</ta>
            <ta e="T423" id="Seg_3751" s="T422">RUS:cult</ta>
            <ta e="T430" id="Seg_3752" s="T429">RUS:gram</ta>
            <ta e="T435" id="Seg_3753" s="T434">RUS:gram</ta>
            <ta e="T439" id="Seg_3754" s="T438">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T378" id="Seg_3755" s="T377">parad:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T49" id="Seg_3756" s="T48">RUS:int</ta>
            <ta e="T94" id="Seg_3757" s="T93">RUS:ext</ta>
            <ta e="T103" id="Seg_3758" s="T96">RUS:ext</ta>
            <ta e="T113" id="Seg_3759" s="T103">RUS:ext</ta>
            <ta e="T117" id="Seg_3760" s="T113">RUS:ext</ta>
            <ta e="T125" id="Seg_3761" s="T117">RUS:ext</ta>
            <ta e="T127" id="Seg_3762" s="T125">RUS:ext</ta>
            <ta e="T133" id="Seg_3763" s="T127">RUS:ext</ta>
            <ta e="T140" id="Seg_3764" s="T133">RUS:ext</ta>
            <ta e="T151" id="Seg_3765" s="T140">RUS:ext</ta>
            <ta e="T160" id="Seg_3766" s="T151">RUS:ext</ta>
            <ta e="T168" id="Seg_3767" s="T160">RUS:ext</ta>
            <ta e="T185" id="Seg_3768" s="T168">RUS:ext</ta>
            <ta e="T205" id="Seg_3769" s="T185">RUS:ext</ta>
            <ta e="T209" id="Seg_3770" s="T205">RUS:ext</ta>
            <ta e="T236" id="Seg_3771" s="T234">RUS:int</ta>
            <ta e="T260" id="Seg_3772" s="T250">RUS:ext</ta>
            <ta e="T277" id="Seg_3773" s="T260">RUS:ext</ta>
            <ta e="T282" id="Seg_3774" s="T277">RUS:ext</ta>
            <ta e="T287" id="Seg_3775" s="T282">RUS:ext</ta>
            <ta e="T291" id="Seg_3776" s="T287">RUS:ext</ta>
            <ta e="T316" id="Seg_3777" s="T315">RUS:int</ta>
            <ta e="T338" id="Seg_3778" s="T329">RUS:ext</ta>
            <ta e="T339" id="Seg_3779" s="T338">RUS:ext</ta>
            <ta e="T351" id="Seg_3780" s="T339">RUS:ext</ta>
            <ta e="T358" id="Seg_3781" s="T351">RUS:ext</ta>
            <ta e="T361" id="Seg_3782" s="T358">RUS:ext</ta>
            <ta e="T366" id="Seg_3783" s="T361">RUS:ext</ta>
            <ta e="T375" id="Seg_3784" s="T366">RUS:ext</ta>
            <ta e="T376" id="Seg_3785" s="T375">RUS:ext</ta>
            <ta e="T411" id="Seg_3786" s="T399">RUS:ext</ta>
            <ta e="T416" id="Seg_3787" s="T411">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_3788" s="T0">Агафон Иванович приехал ко мне разговаривать.</ta>
            <ta e="T7" id="Seg_3789" s="T5">Я спросила:</ta>
            <ta e="T9" id="Seg_3790" s="T7">"Как ты приехал?</ta>
            <ta e="T14" id="Seg_3791" s="T9">На машине или прилетел?"</ta>
            <ta e="T17" id="Seg_3792" s="T14">Он сказал: "Я прилетел".</ta>
            <ta e="T26" id="Seg_3793" s="T17">Потом я много с ним разговаривала на своем языке.</ta>
            <ta e="T30" id="Seg_3794" s="T26">Много катушек, пятнадцать.</ta>
            <ta e="T41" id="Seg_3795" s="T30">Потом два раза он был у меня и уехал.</ta>
            <ta e="T49" id="Seg_3796" s="T41">Потом он рассказал (Арпиту?).</ta>
            <ta e="T59" id="Seg_3797" s="T49">Потом он написал мне письмо, я тоже ему написала.</ta>
            <ta e="T67" id="Seg_3798" s="T59">Два, три, четыре — четыре года он мне писал.</ta>
            <ta e="T71" id="Seg_3799" s="T67">Потом позвал [меня] сюда.</ta>
            <ta e="T82" id="Seg_3800" s="T71">Тогда я сказала: "Когда приедет Агафон Иванович, тогда я к вам приеду".</ta>
            <ta e="T93" id="Seg_3801" s="T82">Он приехал сам, забрал меня, мы полетели и на машине [ехали].</ta>
            <ta e="T94" id="Seg_3802" s="T93">Мы…</ta>
            <ta e="T96" id="Seg_3803" s="T94">Мы на машине приехали.</ta>
            <ta e="T209" id="Seg_3804" s="T205">Чего? Уже говорить можно?</ta>
            <ta e="T213" id="Seg_3805" s="T209">Потом я приехала.</ta>
            <ta e="T219" id="Seg_3806" s="T213">Меня поселили в этот дом, к Нине.</ta>
            <ta e="T222" id="Seg_3807" s="T219">Потом я (пошла?).</ta>
            <ta e="T231" id="Seg_3808" s="T222">Пришли ко мне два человека, посадили [меня] в машину и повезли.</ta>
            <ta e="T238" id="Seg_3809" s="T231">Далеко, за город, где…</ta>
            <ta e="T244" id="Seg_3810" s="T238">…никого нет, я ягод поела.</ta>
            <ta e="T248" id="Seg_3811" s="T244">Потом я там разговаривала.</ta>
            <ta e="T250" id="Seg_3812" s="T248">Потом…</ta>
            <ta e="T299" id="Seg_3813" s="T291">Потом я расскажу, какая наша земля.</ta>
            <ta e="T303" id="Seg_3814" s="T299">Там много ягод.</ta>
            <ta e="T315" id="Seg_3815" s="T303">И деревьев, белых и красные, осины, кедры есть, где орехи растут.</ta>
            <ta e="T317" id="Seg_3816" s="T315">Малина есть.</ta>
            <ta e="T321" id="Seg_3817" s="T317">Потом там степи есть.</ta>
            <ta e="T323" id="Seg_3818" s="T321">Много хлеба.</ta>
            <ta e="T329" id="Seg_3819" s="T323">Пшеница, овес и…</ta>
            <ta e="T387" id="Seg_3820" s="T376">Когда были олени, они заболели, тогда [люди] их отпустили.</ta>
            <ta e="T391" id="Seg_3821" s="T387">Они пошли в лес.</ta>
            <ta e="T399" id="Seg_3822" s="T391">Мама рассказывала, моя бабушка говорила мне, я была еще маленькая.</ta>
            <ta e="T420" id="Seg_3823" s="T416">Потом они пошли в лес.</ta>
            <ta e="T423" id="Seg_3824" s="T420">На большую гору, на Белогорье.</ta>
            <ta e="T426" id="Seg_3825" s="T423">Где много воды.</ta>
            <ta e="T429" id="Seg_3826" s="T426">Там рыбу ловили.</ta>
            <ta e="T437" id="Seg_3827" s="T429">И солили и привозили домой.</ta>
            <ta e="T439" id="Seg_3828" s="T437">Там на (прииск?).</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_3829" s="T0">Agafon Ivanovich came to speak to me.</ta>
            <ta e="T7" id="Seg_3830" s="T5">I asked:</ta>
            <ta e="T9" id="Seg_3831" s="T7">”How did you come?</ta>
            <ta e="T14" id="Seg_3832" s="T9">With a car, or did you come here flying?"</ta>
            <ta e="T17" id="Seg_3833" s="T14">He said: "I came flying."</ta>
            <ta e="T26" id="Seg_3834" s="T17">Then I spoke a lot with him in [my] own language.</ta>
            <ta e="T30" id="Seg_3835" s="T26">Many tapes, fifteen.</ta>
            <ta e="T41" id="Seg_3836" s="T30">Then two times he was at my place (?) and left.</ta>
            <ta e="T49" id="Seg_3837" s="T41">Then he told (Arpit?).</ta>
            <ta e="T59" id="Seg_3838" s="T49">Then he wrote me a letter, I also wrote to him.</ta>
            <ta e="T67" id="Seg_3839" s="T59">Two, three, four ‒ four years he wrote to me.</ta>
            <ta e="T71" id="Seg_3840" s="T67">Then he invited [me] here.</ta>
            <ta e="T82" id="Seg_3841" s="T71">Then I said: "When Agafon will come, then I will come to your place."</ta>
            <ta e="T93" id="Seg_3842" s="T82">He came himself, took me, we flew and [went] by car.</ta>
            <ta e="T94" id="Seg_3843" s="T93">We…</ta>
            <ta e="T96" id="Seg_3844" s="T94">We came by car.</ta>
            <ta e="T103" id="Seg_3845" s="T96">Agafon Ivanovich travelled to my place twice.</ta>
            <ta e="T113" id="Seg_3846" s="T103">I asked: How did you come, by car or flew by plane.</ta>
            <ta e="T117" id="Seg_3847" s="T113">He said "By plane.".</ta>
            <ta e="T125" id="Seg_3848" s="T117">Then he was at my place two times.</ta>
            <ta e="T127" id="Seg_3849" s="T125">And he left.</ta>
            <ta e="T133" id="Seg_3850" s="T127">God, (I wish) I didn't forget again…. [?]</ta>
            <ta e="T140" id="Seg_3851" s="T133">And he left and…</ta>
            <ta e="T151" id="Seg_3852" s="T140">But he told Arpad that there is a woman who can speak [Kamas].</ta>
            <ta e="T160" id="Seg_3853" s="T151">He started to write me letters, four years we wrote to each other.</ta>
            <ta e="T168" id="Seg_3854" s="T160">Then he called me to come.</ta>
            <ta e="T185" id="Seg_3855" s="T168">But I wrote to him that I cannot come alone, when Agafon Ivanovich comes, then I will come.</ta>
            <ta e="T205" id="Seg_3856" s="T185">He made up his mind and came himself and… we flew on a plane and by car (flew-) came here.</ta>
            <ta e="T209" id="Seg_3857" s="T205">What? I can already speak?</ta>
            <ta e="T213" id="Seg_3858" s="T209">Then I arrived.</ta>
            <ta e="T219" id="Seg_3859" s="T213">They (seated/placed) took me to the house, to Nina.</ta>
            <ta e="T222" id="Seg_3860" s="T219">Then I (went?).</ta>
            <ta e="T231" id="Seg_3861" s="T222">Two men came to me, then they seated [me] into a car and carried.</ta>
            <ta e="T238" id="Seg_3862" s="T231">Far… from the city, where…</ta>
            <ta e="T244" id="Seg_3863" s="T238">…nobody was, I ate berries there.</ta>
            <ta e="T248" id="Seg_3864" s="T244">Then there I spoke.</ta>
            <ta e="T250" id="Seg_3865" s="T248">Then…</ta>
            <ta e="T260" id="Seg_3866" s="T250">When he brought me here and put me at Nina’s place.</ta>
            <ta e="T277" id="Seg_3867" s="T260">So, then two people came and a third one and sat me in a car and we drove far into the countryside.</ta>
            <ta e="T282" id="Seg_3868" s="T277">And I ate berries there.</ta>
            <ta e="T287" id="Seg_3869" s="T282">And spoke with them about…</ta>
            <ta e="T291" id="Seg_3870" s="T287">More… that's it, probably.</ta>
            <ta e="T299" id="Seg_3871" s="T291">Then I will speak about what our land is like.</ta>
            <ta e="T303" id="Seg_3872" s="T299">There are many berries.</ta>
            <ta e="T315" id="Seg_3873" s="T303">And trees, white and red ones, aspen trees, there are cedars tree[s], where the pine nuts grow.</ta>
            <ta e="T317" id="Seg_3874" s="T315">There are raspberries.</ta>
            <ta e="T321" id="Seg_3875" s="T317">Then there are steppes.</ta>
            <ta e="T323" id="Seg_3876" s="T321">A lot of grain.</ta>
            <ta e="T329" id="Seg_3877" s="T323">Wheat, oats, and…</ta>
            <ta e="T338" id="Seg_3878" s="T329">We have (?) there, we have forest there.</ta>
            <ta e="T339" id="Seg_3879" s="T338">Birches.</ta>
            <ta e="T351" id="Seg_3880" s="T339">And larches or how I call them, the red, in my language, the larches, aspen trees.</ta>
            <ta e="T358" id="Seg_3881" s="T351">And the cedars on which the nuts grow, the cones.</ta>
            <ta e="T361" id="Seg_3882" s="T358">And berries, raspberries.</ta>
            <ta e="T366" id="Seg_3883" s="T361">And steppe.</ta>
            <ta e="T375" id="Seg_3884" s="T366">Wheat and oats and buckwheat.</ta>
            <ta e="T376" id="Seg_3885" s="T375">So.</ta>
            <ta e="T387" id="Seg_3886" s="T376">When [there] were reindeer, they got sick, they, then they were let [free].</ta>
            <ta e="T391" id="Seg_3887" s="T387">They went to the forest.</ta>
            <ta e="T399" id="Seg_3888" s="T391">My mother told, my grandmother said to me, I was still small.</ta>
            <ta e="T411" id="Seg_3889" s="T399">When they rode with the reindeer, they became ill with scabies, they took them and let them into the forest.</ta>
            <ta e="T416" id="Seg_3890" s="T411">Grandmother told, and mother.</ta>
            <ta e="T420" id="Seg_3891" s="T416">Then they went to the taiga.</ta>
            <ta e="T423" id="Seg_3892" s="T420">To the big mountain, to the Belogorye mountains.</ta>
            <ta e="T426" id="Seg_3893" s="T423">Where [there is] a lot of water.</ta>
            <ta e="T429" id="Seg_3894" s="T426">There they caught fish.</ta>
            <ta e="T437" id="Seg_3895" s="T429">And [killed-] salted and brought home.</ta>
            <ta e="T439" id="Seg_3896" s="T437">There to the (gold)washing site. [?]</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_3897" s="T0">Agafon Ivanowitsch kam, um mit mir zu sprechen.</ta>
            <ta e="T7" id="Seg_3898" s="T5">Ich fragte:</ta>
            <ta e="T9" id="Seg_3899" s="T7">"Wie bist du gekommen?</ta>
            <ta e="T14" id="Seg_3900" s="T9">Mit einem Auto oder bist hierher geflogen?"</ta>
            <ta e="T17" id="Seg_3901" s="T14">Es sagte: "Ich kam geflogen."</ta>
            <ta e="T26" id="Seg_3902" s="T17">Dann sprach ich viel mit ihm auf [meiner] eigenen Sprache.</ta>
            <ta e="T30" id="Seg_3903" s="T26">Viele Bänder, fünfzehn.</ta>
            <ta e="T41" id="Seg_3904" s="T30">Dann war er zweimal bei mir (?) und ging fort.</ta>
            <ta e="T49" id="Seg_3905" s="T41">Dann erzählte er (Arpit?).</ta>
            <ta e="T59" id="Seg_3906" s="T49">Dann schrieb er mir einen Brief, ich schrieb ihm auch.</ta>
            <ta e="T67" id="Seg_3907" s="T59">Zwei, drei, vier ‒ vier Jahre schrieb er mir.</ta>
            <ta e="T71" id="Seg_3908" s="T67">Dann lud er [mich] hierher ein.</ta>
            <ta e="T82" id="Seg_3909" s="T71">Dann sagte ich: "Wenn Agafon kommt, dann komme ich zu euch."</ta>
            <ta e="T93" id="Seg_3910" s="T82">Dann kam er selbst, nahm mich, wir kamen geflogen, mit dem Auto.</ta>
            <ta e="T94" id="Seg_3911" s="T93">Wir…</ta>
            <ta e="T96" id="Seg_3912" s="T94">Wir kamen mit dem Auto.</ta>
            <ta e="T103" id="Seg_3913" s="T96">Agafon Iwanowitsch reiste zweimal zu mir.</ta>
            <ta e="T113" id="Seg_3914" s="T103">Ich fragte: "Wie bist du gekommen, mit dem Auto oder mit dem Flugzeug geflogen?"</ta>
            <ta e="T117" id="Seg_3915" s="T113">Er sagte: "Mit dem Flugzeug."</ta>
            <ta e="T125" id="Seg_3916" s="T117">Dann war er zweimal bei mir.</ta>
            <ta e="T127" id="Seg_3917" s="T125">Und er fuhr weg.</ta>
            <ta e="T133" id="Seg_3918" s="T127">Mein Gott, hätte ich nicht wieder vergessen…. [?]</ta>
            <ta e="T140" id="Seg_3919" s="T133">Und er fuhr weg und…</ta>
            <ta e="T151" id="Seg_3920" s="T140">Aber er sagte Arpit, dass es dort eine Frau gibt, die [Kamassisch] sprechen kann.</ta>
            <ta e="T160" id="Seg_3921" s="T151">Er fing an, mir Briefe zu schreiben, vier Jahre schrieben wir uns.</ta>
            <ta e="T168" id="Seg_3922" s="T160">Dann lud er mich ein zu kommen.</ta>
            <ta e="T185" id="Seg_3923" s="T168">Aber ich schrieb ihm, dass ich nicht alleine kommen kann, wenn Agafon Iwanowitsch kommt, dann komme ich.</ta>
            <ta e="T205" id="Seg_3924" s="T185">Er machte sich auf den Weg und kam selbst und… wir flogen mit dem Flugzeug, und mit dem Auto (flog-) kamen wir hierher.</ta>
            <ta e="T209" id="Seg_3925" s="T205">Was? Kann ich schon sprechen?</ta>
            <ta e="T213" id="Seg_3926" s="T209">Dann kam ich an.</ta>
            <ta e="T219" id="Seg_3927" s="T213">Sie (setzten) nahmen mich zu dem Haus, zu Nina.</ta>
            <ta e="T222" id="Seg_3928" s="T219">Dann ging (ich?).</ta>
            <ta e="T231" id="Seg_3929" s="T222">Zwei Männer kamen zu mir, dann setzten sie [mich] in ein Auto, fuhren.</ta>
            <ta e="T238" id="Seg_3930" s="T231">Weit, von der Stadt, wo…</ta>
            <ta e="T244" id="Seg_3931" s="T238">…war niemand, ich aß dort Beeren.</ta>
            <ta e="T248" id="Seg_3932" s="T244">Dann sprach ich dort.</ta>
            <ta e="T250" id="Seg_3933" s="T248">Dann.</ta>
            <ta e="T260" id="Seg_3934" s="T250">Als er mich hierher brachte und mich zu Niina in die Wohnung brachte.</ta>
            <ta e="T277" id="Seg_3935" s="T260">So, dann kamen sie zu zweit und ein dritter und setzten mich in ein Auto und wir fuhren weit weg aus der Stadt.</ta>
            <ta e="T282" id="Seg_3936" s="T277">Und ich aß dort Beeren.</ta>
            <ta e="T287" id="Seg_3937" s="T282">Und sprach mit ihnen (?).</ta>
            <ta e="T291" id="Seg_3938" s="T287">Mehr… das war's wohl.</ta>
            <ta e="T299" id="Seg_3939" s="T291">Dann werde ich darüber sprechen, wie unser Land ist.</ta>
            <ta e="T303" id="Seg_3940" s="T299">Es gibt viele Beeren.</ta>
            <ta e="T315" id="Seg_3941" s="T303">Und Bäume, weiße und rote, Espenbäume, es gibt Kiefern, wo die Kiefernnüsse wachsen.</ta>
            <ta e="T317" id="Seg_3942" s="T315">Es gibt Himbeeren.</ta>
            <ta e="T321" id="Seg_3943" s="T317">Dann gibt es Steppe.</ta>
            <ta e="T323" id="Seg_3944" s="T321">Viel Getreide.</ta>
            <ta e="T329" id="Seg_3945" s="T323">Weizen, Hafer und…</ta>
            <ta e="T338" id="Seg_3946" s="T329">Wir haben (?) dort, wir haben Wald dort.</ta>
            <ta e="T339" id="Seg_3947" s="T338">Birken.</ta>
            <ta e="T351" id="Seg_3948" s="T339">Und Lärchen, wie heiße ich sie, die roten, in meiner Sprache, die Lärchen, die Ahornbäume.</ta>
            <ta e="T358" id="Seg_3949" s="T351">Und die Kiefern, woran die Nüsse wachsen, die Zapfen.</ta>
            <ta e="T361" id="Seg_3950" s="T358">Und Beeren, Himbeeren.</ta>
            <ta e="T366" id="Seg_3951" s="T361">Und Steppe.</ta>
            <ta e="T375" id="Seg_3952" s="T366">Weizen und Hafer und Buchweizen.</ta>
            <ta e="T376" id="Seg_3953" s="T375">So.</ta>
            <ta e="T387" id="Seg_3954" s="T376">Als es Rentiere gab, wurden sie krank, sie, dann wurden sie freigelassen.</ta>
            <ta e="T391" id="Seg_3955" s="T387">Sie liefen in den Wald.</ta>
            <ta e="T399" id="Seg_3956" s="T391">Meine Mutter erzählte, meine Großmutter sagte es mir, ich war noch klein.</ta>
            <ta e="T411" id="Seg_3957" s="T399">Als man mit Rentieren umherzog, bekamen sie die Krätze, man nahm sie und ließ sie im Wald frei.</ta>
            <ta e="T416" id="Seg_3958" s="T411">Sagte die Großmutter und Mutter.</ta>
            <ta e="T420" id="Seg_3959" s="T416">Dann gingen sie in die Taiga.</ta>
            <ta e="T423" id="Seg_3960" s="T420">Zum großen Berg, in die Belogorye-Berge.</ta>
            <ta e="T426" id="Seg_3961" s="T423">Wo es viel Wasser gibt.</ta>
            <ta e="T429" id="Seg_3962" s="T426">Dort fingen sie Fisch.</ta>
            <ta e="T437" id="Seg_3963" s="T429">Und [tötete-] salzten [ihn] und brachten [ihn] nach Hause.</ta>
            <ta e="T439" id="Seg_3964" s="T437">Dann zum (Gold)waschplatz. [?]</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_3965" s="T0">[GVY:] Agafon Ivanovich is a name for Ago Künnap.</ta>
            <ta e="T26" id="Seg_3966" s="T17">[KlT:] INS.3SG instead of 1SG. [GVY:] bostu pronounced boštu, as often.</ta>
            <ta e="T41" id="Seg_3967" s="T30">[GVY:] or ibi means 'he took' and then = "he recorded me" (or "took a picture of me")?</ta>
            <ta e="T219" id="Seg_3968" s="T213">[GVY:] Niinanə</ta>
            <ta e="T260" id="Seg_3969" s="T250">[KlT] фатера RUS - a variant of квартира, colloquial.</ta>
            <ta e="T277" id="Seg_3970" s="T260">[GVY:] Or "третьей (посадили)"?</ta>
            <ta e="T351" id="Seg_3971" s="T339">[KlT] Листвяг = лиственница (most likely, local variant) http://www.terminy.info/russian-language/explanatory-dictionary-efremovoy/listvyag.</ta>
            <ta e="T366" id="Seg_3972" s="T361">[AAV:] [p] not [p']</ta>
            <ta e="T375" id="Seg_3973" s="T366">[KlT] RUS гречуха = гречиха.</ta>
            <ta e="T391" id="Seg_3974" s="T387">[GVY:] Russian 'лес'? Unclear.</ta>
            <ta e="T437" id="Seg_3975" s="T429">[KlT:] Maʔnʼi - POSS.1SG.LAT in KP’s idiolect.</ta>
            <ta e="T439" id="Seg_3976" s="T437">[KlT] Check the earlier case of blizkanə! priisk - Goldwäsche?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T441" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
