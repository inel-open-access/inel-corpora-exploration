<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID33E8CA2E-D854-91DD-860C-A07DE527D3CC">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_19700819_09343_1bz</transcription-name>
         <referenced-file url="PKZ_19700819_09343-1bz.wav" />
         <referenced-file url="PKZ_19700819_09343-1bz.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700819_09343-1bz\PKZ_19700819_09343-1bz.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">2231</ud-information>
            <ud-information attribute-name="# HIAT:w">1527</ud-information>
            <ud-information attribute-name="# e">1498</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">11</ud-information>
            <ud-information attribute-name="# HIAT:u">270</ud-information>
            <ud-information attribute-name="# sc">480</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="2.8666593602322066" />
         <tli id="T2" time="2.87" type="appl" />
         <tli id="T3" time="3.659999623352383" />
         <tli id="T4" time="4.045" type="appl" />
         <tli id="T5" time="4.53" type="appl" />
         <tli id="T6" time="4.98" type="appl" />
         <tli id="T7" time="5.418" type="appl" />
         <tli id="T8" time="5.7075000000000005" type="appl" />
         <tli id="T9" time="5.856" type="appl" />
         <tli id="T10" time="6.2940000000000005" type="appl" />
         <tli id="T11" time="6.4350000000000005" type="appl" />
         <tli id="T12" time="6.732" type="appl" />
         <tli id="T13" time="7.1625" type="appl" />
         <tli id="T14" time="7.17" type="appl" />
         <tli id="T15" time="7.717142857142857" type="appl" />
         <tli id="T16" time="7.89" type="appl" />
         <tli id="T17" time="8.264285714285714" type="appl" />
         <tli id="T18" time="8.811428571428571" type="appl" />
         <tli id="T19" time="9.358571428571429" type="appl" />
         <tli id="T20" time="9.905714285714286" type="appl" />
         <tli id="T21" time="10.452857142857143" type="appl" />
         <tli id="T22" time="11.093332728331456" />
         <tli id="T23" time="11.24" type="appl" />
         <tli id="T24" time="11.678" type="appl" />
         <tli id="T25" time="12.116" type="appl" />
         <tli id="T26" time="12.554" type="appl" />
         <tli id="T27" time="12.991999999999999" type="appl" />
         <tli id="T28" time="13.43" type="appl" />
         <tli id="T29" time="16.06" type="appl" />
         <tli id="T30" time="16.509999999999998" type="appl" />
         <tli id="T31" time="16.96" type="appl" />
         <tli id="T32" time="16.98" type="appl" />
         <tli id="T33" time="17.53" type="appl" />
         <tli id="T34" time="18.193332535762586" />
         <tli id="T35" time="18.91" type="appl" />
         <tli id="T36" time="19.578625" type="appl" />
         <tli id="T37" time="20.24725" type="appl" />
         <tli id="T38" time="20.915875" type="appl" />
         <tli id="T39" time="21.5845" type="appl" />
         <tli id="T40" time="22.253125" type="appl" />
         <tli id="T41" time="22.92175" type="appl" />
         <tli id="T42" time="23.590375" type="appl" />
         <tli id="T43" time="24.259" type="appl" />
         <tli id="T44" time="24.42" type="appl" />
         <tli id="T45" time="24.902727272727276" type="appl" />
         <tli id="T46" time="25.385454545454547" type="appl" />
         <tli id="T47" time="25.86818181818182" type="appl" />
         <tli id="T48" time="26.35090909090909" type="appl" />
         <tli id="T49" time="26.833636363636366" type="appl" />
         <tli id="T50" time="27.316363636363636" type="appl" />
         <tli id="T51" time="27.79909090909091" type="appl" />
         <tli id="T52" time="28.28181818181818" type="appl" />
         <tli id="T53" time="28.764545454545456" type="appl" />
         <tli id="T54" time="29.24727272727273" type="appl" />
         <tli id="T55" time="29.73" type="appl" />
         <tli id="T56" time="29.81" type="appl" />
         <tli id="T57" time="30.358333333333334" type="appl" />
         <tli id="T58" time="30.906666666666666" type="appl" />
         <tli id="T59" time="31.455" type="appl" />
         <tli id="T60" time="32.00333333333333" type="appl" />
         <tli id="T61" time="32.55166666666667" type="appl" />
         <tli id="T62" time="33.1" type="appl" />
         <tli id="T63" time="33.11" type="appl" />
         <tli id="T64" time="33.71" type="appl" />
         <tli id="T65" time="34.31" type="appl" />
         <tli id="T66" time="34.91" type="appl" />
         <tli id="T67" time="35.51" type="appl" />
         <tli id="T68" time="36.11" type="appl" />
         <tli id="T69" time="36.2" type="appl" />
         <tli id="T70" time="36.578" type="appl" />
         <tli id="T71" time="36.956" type="appl" />
         <tli id="T72" time="37.334" type="appl" />
         <tli id="T73" time="37.712" type="appl" />
         <tli id="T74" time="38.09" type="appl" />
         <tli id="T75" time="38.15" type="appl" />
         <tli id="T76" time="39.00555555555555" type="appl" />
         <tli id="T77" time="39.86111111111111" type="appl" />
         <tli id="T78" time="40.71666666666667" type="appl" />
         <tli id="T79" time="41.57222222222222" type="appl" />
         <tli id="T80" time="42.42777777777778" type="appl" />
         <tli id="T81" time="43.28333333333333" type="appl" />
         <tli id="T82" time="44.138888888888886" type="appl" />
         <tli id="T83" time="44.99444444444445" type="appl" />
         <tli id="T84" time="45.85" type="appl" />
         <tli id="T85" time="46.53" type="appl" />
         <tli id="T86" time="47.01175" type="appl" />
         <tli id="T87" time="47.4935" type="appl" />
         <tli id="T88" time="47.97525" type="appl" />
         <tli id="T89" time="48.457" type="appl" />
         <tli id="T90" time="48.48" type="appl" />
         <tli id="T91" time="49.01" type="appl" />
         <tli id="T92" time="49.54" type="appl" />
         <tli id="T93" time="50.07" type="appl" />
         <tli id="T94" time="50.6" type="appl" />
         <tli id="T95" time="51.13" type="appl" />
         <tli id="T96" time="51.621818181818185" type="appl" />
         <tli id="T97" time="52.11363636363637" type="appl" />
         <tli id="T98" time="52.60545454545455" type="appl" />
         <tli id="T99" time="53.09727272727273" type="appl" />
         <tli id="T100" time="53.58909090909091" type="appl" />
         <tli id="T101" time="54.08090909090909" type="appl" />
         <tli id="T102" time="54.57272727272727" type="appl" />
         <tli id="T103" time="55.06454545454545" type="appl" />
         <tli id="T104" time="55.556363636363635" type="appl" />
         <tli id="T105" time="56.04818181818182" type="appl" />
         <tli id="T106" time="56.54" type="appl" />
         <tli id="T107" time="56.7" type="appl" />
         <tli id="T108" time="57.150000000000006" type="appl" />
         <tli id="T109" time="57.6" type="appl" />
         <tli id="T110" time="58.05" type="appl" />
         <tli id="T111" time="58.5" type="appl" />
         <tli id="T112" time="58.95" type="appl" />
         <tli id="T113" time="59.49999808820736" />
         <tli id="T114" time="59.56" type="appl" />
         <tli id="T115" time="60.175000000000004" type="appl" />
         <tli id="T116" time="60.790000000000006" type="appl" />
         <tli id="T117" time="61.405" type="appl" />
         <tli id="T118" time="62.02" type="appl" />
         <tli id="T119" time="62.03" type="appl" />
         <tli id="T120" time="62.60666666666667" type="appl" />
         <tli id="T121" time="63.18333333333333" type="appl" />
         <tli id="T122" time="63.80000014935583" />
         <tli id="T123" time="63.88" type="appl" />
         <tli id="T124" time="64.53857142857143" type="appl" />
         <tli id="T125" time="65.19714285714286" type="appl" />
         <tli id="T126" time="65.85571428571428" type="appl" />
         <tli id="T127" time="66.5142857142857" type="appl" />
         <tli id="T128" time="67.17285714285714" type="appl" />
         <tli id="T129" time="67.83142857142857" type="appl" />
         <tli id="T130" time="68.49" type="appl" />
         <tli id="T131" time="69.71" type="appl" />
         <tli id="T132" time="70.22333333333333" type="appl" />
         <tli id="T133" time="70.73666666666666" type="appl" />
         <tli id="T134" time="71.25" type="appl" />
         <tli id="T135" time="71.76333333333334" type="appl" />
         <tli id="T136" time="72.27666666666667" type="appl" />
         <tli id="T137" time="72.79" type="appl" />
         <tli id="T138" time="73.39" type="appl" />
         <tli id="T139" time="73.88" type="appl" />
         <tli id="T140" time="74.37" type="appl" />
         <tli id="T141" time="74.86" type="appl" />
         <tli id="T142" time="75.35" type="appl" />
         <tli id="T143" time="75.84" type="appl" />
         <tli id="T144" time="76.33" type="appl" />
         <tli id="T145" time="76.82000000000001" type="appl" />
         <tli id="T146" time="77.31" type="appl" />
         <tli id="T147" time="77.46" type="appl" />
         <tli id="T148" time="78.16" type="appl" />
         <tli id="T149" time="78.86" type="appl" />
         <tli id="T150" time="79.56" type="appl" />
         <tli id="T151" time="80.25999999999999" type="appl" />
         <tli id="T152" time="80.96" type="appl" />
         <tli id="T153" time="81.66" type="appl" />
         <tli id="T154" time="82.36" type="appl" />
         <tli id="T155" time="82.42" type="appl" />
         <tli id="T156" time="83.08483333333334" type="appl" />
         <tli id="T157" time="83.74966666666667" type="appl" />
         <tli id="T158" time="84.4145" type="appl" />
         <tli id="T159" time="85.07933333333334" type="appl" />
         <tli id="T160" time="85.74416666666667" type="appl" />
         <tli id="T161" time="86.409" type="appl" />
         <tli id="T162" time="86.48" type="appl" />
         <tli id="T163" time="86.977" type="appl" />
         <tli id="T164" time="87.474" type="appl" />
         <tli id="T165" time="87.971" type="appl" />
         <tli id="T166" time="88.468" type="appl" />
         <tli id="T167" time="88.965" type="appl" />
         <tli id="T168" time="89.462" type="appl" />
         <tli id="T169" time="89.959" type="appl" />
         <tli id="T170" time="90.456" type="appl" />
         <tli id="T171" time="90.953" type="appl" />
         <tli id="T172" time="91.45" type="appl" />
         <tli id="T173" time="91.62" type="appl" />
         <tli id="T174" time="92.17666666666668" type="appl" />
         <tli id="T175" time="92.73333333333333" type="appl" />
         <tli id="T176" time="93.28999999999999" type="appl" />
         <tli id="T177" time="93.84666666666666" type="appl" />
         <tli id="T178" time="94.40333333333334" type="appl" />
         <tli id="T179" time="95.02666535166288" />
         <tli id="T180" time="95.03" type="appl" />
         <tli id="T181" time="95.52" type="appl" />
         <tli id="T182" time="96.01" type="appl" />
         <tli id="T183" time="96.5" type="appl" />
         <tli id="T184" time="99.44" type="appl" />
         <tli id="T185" time="100.063" type="appl" />
         <tli id="T186" time="100.68599999999999" type="appl" />
         <tli id="T187" time="101.309" type="appl" />
         <tli id="T188" time="101.932" type="appl" />
         <tli id="T189" time="102.555" type="appl" />
         <tli id="T190" time="103.178" type="appl" />
         <tli id="T191" time="103.801" type="appl" />
         <tli id="T192" time="104.424" type="appl" />
         <tli id="T193" time="105.047" type="appl" />
         <tli id="T194" time="105.67" type="appl" />
         <tli id="T195" time="106.37" type="appl" />
         <tli id="T196" time="107.66833333333334" type="appl" />
         <tli id="T197" time="108.96666666666667" type="appl" />
         <tli id="T198" time="110.265" type="appl" />
         <tli id="T199" time="111.56333333333333" type="appl" />
         <tli id="T200" time="112.86166666666666" type="appl" />
         <tli id="T201" time="114.29999513476878" />
         <tli id="T202" time="114.4" type="appl" />
         <tli id="T203" time="115.0425" type="appl" />
         <tli id="T204" time="115.685" type="appl" />
         <tli id="T205" time="116.3275" type="appl" />
         <tli id="T206" time="117.0900010445391" />
         <tli id="T207" time="117.09333436937656" />
         <tli id="T208" time="117.885" type="appl" />
         <tli id="T209" time="118.71000000000001" type="appl" />
         <tli id="T210" time="119.535" type="appl" />
         <tli id="T211" time="120.42000557798106" />
         <tli id="T212" time="120.48" type="appl" />
         <tli id="T213" time="121.18833333333333" type="appl" />
         <tli id="T214" time="121.89666666666668" type="appl" />
         <tli id="T1738" time="122.22358974358976" type="intp" />
         <tli id="T215" time="122.605" type="appl" />
         <tli id="T216" time="123.31333333333333" type="appl" />
         <tli id="T217" time="124.02166666666668" type="appl" />
         <tli id="T218" time="124.76999449089156" />
         <tli id="T219" time="124.79" type="appl" />
         <tli id="T220" time="125.39750000000001" type="appl" />
         <tli id="T221" time="126.005" type="appl" />
         <tli id="T222" time="126.6125" type="appl" />
         <tli id="T223" time="127.22" type="appl" />
         <tli id="T224" time="127.67" type="appl" />
         <tli id="T225" time="128.147" type="appl" />
         <tli id="T226" time="128.624" type="appl" />
         <tli id="T227" time="129.101" type="appl" />
         <tli id="T228" time="129.578" type="appl" />
         <tli id="T229" time="130.055" type="appl" />
         <tli id="T230" time="130.532" type="appl" />
         <tli id="T231" time="131.009" type="appl" />
         <tli id="T232" time="131.486" type="appl" />
         <tli id="T233" time="131.963" type="appl" />
         <tli id="T234" time="132.44" type="appl" />
         <tli id="T235" time="133.66" type="appl" />
         <tli id="T236" time="134.49333333333334" type="appl" />
         <tli id="T237" time="135.32666666666665" type="appl" />
         <tli id="T238" time="136.16" type="appl" />
         <tli id="T239" time="136.99333333333334" type="appl" />
         <tli id="T240" time="137.82666666666665" type="appl" />
         <tli id="T241" time="138.66" type="appl" />
         <tli id="T242" time="139.49333333333334" type="appl" />
         <tli id="T243" time="140.32666666666665" type="appl" />
         <tli id="T244" time="141.16" type="appl" />
         <tli id="T245" time="141.41" type="appl" />
         <tli id="T246" time="141.98857142857142" type="appl" />
         <tli id="T247" time="142.56714285714287" type="appl" />
         <tli id="T248" time="143.1457142857143" type="appl" />
         <tli id="T249" time="143.7242857142857" type="appl" />
         <tli id="T250" time="144.30285714285714" type="appl" />
         <tli id="T251" time="144.8814285714286" type="appl" />
         <tli id="T252" time="145.46" type="appl" />
         <tli id="T253" time="146.05" type="appl" />
         <tli id="T254" time="146.745" type="appl" />
         <tli id="T255" time="147.44" type="appl" />
         <tli id="T256" time="147.9" type="appl" />
         <tli id="T257" time="148.51" type="appl" />
         <tli id="T258" time="149.12" type="appl" />
         <tli id="T259" time="149.73" type="appl" />
         <tli id="T260" time="150.34" type="appl" />
         <tli id="T261" time="150.95" type="appl" />
         <tli id="T262" time="151.56" type="appl" />
         <tli id="T263" time="152.17" type="appl" />
         <tli id="T264" time="154.99" type="appl" />
         <tli id="T265" time="159.25" type="appl" />
         <tli id="T266" time="159.29" type="appl" />
         <tli id="T267" time="159.81666666666666" type="appl" />
         <tli id="T268" time="160.34333333333333" type="appl" />
         <tli id="T269" time="160.87" type="appl" />
         <tli id="T270" time="161.17" type="appl" />
         <tli id="T271" time="161.50125" type="appl" />
         <tli id="T272" time="161.83249999999998" type="appl" />
         <tli id="T273" time="162.16375" type="appl" />
         <tli id="T274" time="162.495" type="appl" />
         <tli id="T275" time="162.82625" type="appl" />
         <tli id="T276" time="163.1575" type="appl" />
         <tli id="T277" time="163.48874999999998" type="appl" />
         <tli id="T278" time="163.82" type="appl" />
         <tli id="T279" time="164.1025" type="appl" />
         <tli id="T280" time="164.385" type="appl" />
         <tli id="T281" time="164.6675" type="appl" />
         <tli id="T282" time="165.02999604436783" />
         <tli id="T283" time="165.03332936920532" />
         <tli id="T284" time="165.585" type="appl" />
         <tli id="T285" time="166.21" type="appl" />
         <tli id="T286" time="166.37" type="appl" />
         <tli id="T287" time="167.11" type="appl" />
         <tli id="T288" time="167.85" type="appl" />
         <tli id="T289" time="168.0" type="appl" />
         <tli id="T290" time="168.4" type="appl" />
         <tli id="T291" time="168.79999999999998" type="appl" />
         <tli id="T292" time="169.19" type="appl" />
         <tli id="T293" time="169.2" type="appl" />
         <tli id="T294" time="169.59181818181818" type="appl" />
         <tli id="T295" time="169.99363636363637" type="appl" />
         <tli id="T296" time="170.21" type="appl" />
         <tli id="T297" time="170.39545454545456" type="appl" />
         <tli id="T298" time="170.79727272727274" type="appl" />
         <tli id="T299" time="170.92" type="appl" />
         <tli id="T300" time="171.01" type="appl" />
         <tli id="T301" time="171.19909090909093" type="appl" />
         <tli id="T302" time="171.60090909090908" type="appl" />
         <tli id="T303" time="171.72" type="appl" />
         <tli id="T304" time="172.00272727272727" type="appl" />
         <tli id="T305" time="172.40454545454546" type="appl" />
         <tli id="T306" time="172.80636363636364" type="appl" />
         <tli id="T307" time="173.20818181818183" type="appl" />
         <tli id="T308" time="173.77666645951385" />
         <tli id="T309" time="174.54500000000002" type="appl" />
         <tli id="T310" time="175.48" type="appl" />
         <tli id="T311" time="175.53" type="appl" />
         <tli id="T312" time="175.79818181818183" type="appl" />
         <tli id="T313" time="176.06636363636363" type="appl" />
         <tli id="T314" time="176.33454545454546" type="appl" />
         <tli id="T315" time="176.60272727272726" type="appl" />
         <tli id="T316" time="176.8709090909091" type="appl" />
         <tli id="T317" time="177.1390909090909" type="appl" />
         <tli id="T318" time="177.40727272727273" type="appl" />
         <tli id="T319" time="177.67545454545453" type="appl" />
         <tli id="T320" time="177.94363636363636" type="appl" />
         <tli id="T321" time="178.21181818181816" type="appl" />
         <tli id="T322" time="178.48" type="appl" />
         <tli id="T323" time="178.48666747567228" />
         <tli id="T324" time="178.6" type="appl" />
         <tli id="T325" time="179.04" type="appl" />
         <tli id="T326" time="179.05" type="appl" />
         <tli id="T327" time="179.48" type="appl" />
         <tli id="T328" time="179.54" type="appl" />
         <tli id="T329" time="179.92" type="appl" />
         <tli id="T330" time="180.03" type="appl" />
         <tli id="T331" time="180.34" type="appl" />
         <tli id="T332" time="180.51999999999998" type="appl" />
         <tli id="T333" time="180.768" type="appl" />
         <tli id="T334" time="181.01" type="appl" />
         <tli id="T335" time="181.196" type="appl" />
         <tli id="T336" time="181.5" type="appl" />
         <tli id="T337" time="181.624" type="appl" />
         <tli id="T338" time="181.98999999999998" type="appl" />
         <tli id="T339" time="182.052" type="appl" />
         <tli id="T340" time="182.48" type="appl" />
         <tli id="T341" time="183.22" type="appl" />
         <tli id="T342" time="183.65833333333333" type="appl" />
         <tli id="T343" time="184.09666666666666" type="appl" />
         <tli id="T344" time="184.535" type="appl" />
         <tli id="T345" time="184.97333333333333" type="appl" />
         <tli id="T346" time="185.41166666666666" type="appl" />
         <tli id="T347" time="185.85" type="appl" />
         <tli id="T348" time="185.97" type="appl" />
         <tli id="T349" time="186.82000561097084" />
         <tli id="T350" time="186.86" type="appl" />
         <tli id="T351" time="187.19" type="appl" />
         <tli id="T352" time="187.22667124114332" />
         <tli id="T353" time="187.88000290928923" />
         <tli id="T354" time="187.972" type="appl" />
         <tli id="T355" time="188.424" type="appl" />
         <tli id="T356" time="188.876" type="appl" />
         <tli id="T357" time="189.328" type="appl" />
         <tli id="T358" time="189.78" type="appl" />
         <tli id="T359" time="190.91" type="appl" />
         <tli id="T360" time="191.29" type="appl" />
         <tli id="T361" time="191.67000000000002" type="appl" />
         <tli id="T362" time="192.05" type="appl" />
         <tli id="T363" time="192.43" type="appl" />
         <tli id="T364" time="192.98" type="appl" />
         <tli id="T365" time="193.04333333333335" type="appl" />
         <tli id="T366" time="193.45499999999998" type="appl" />
         <tli id="T367" time="193.65666666666667" type="appl" />
         <tli id="T368" time="193.93" type="appl" />
         <tli id="T369" time="194.27" type="appl" />
         <tli id="T370" time="194.405" type="appl" />
         <tli id="T371" time="194.53" type="appl" />
         <tli id="T372" time="194.88" type="appl" />
         <tli id="T373" time="195.0075" type="appl" />
         <tli id="T374" time="195.35500000000002" type="appl" />
         <tli id="T375" time="195.485" type="appl" />
         <tli id="T376" time="195.83" type="appl" />
         <tli id="T377" time="195.9625" type="appl" />
         <tli id="T378" time="196.44" type="appl" />
         <tli id="T379" time="196.9175" type="appl" />
         <tli id="T380" time="197.39499999999998" type="appl" />
         <tli id="T381" time="197.788" type="appl" />
         <tli id="T382" time="197.8725" type="appl" />
         <tli id="T383" time="198.35" type="appl" />
         <tli id="T384" time="198.358" type="appl" />
         <tli id="T385" time="199.958" type="appl" />
         <tli id="T386" time="200.48514285714285" type="appl" />
         <tli id="T387" time="201.01228571428572" type="appl" />
         <tli id="T388" time="201.53942857142857" type="appl" />
         <tli id="T389" time="202.06657142857142" type="appl" />
         <tli id="T390" time="202.59371428571427" type="appl" />
         <tli id="T391" time="203.12085714285715" type="appl" />
         <tli id="T392" time="203.648" type="appl" />
         <tli id="T393" time="204.1085" type="appl" />
         <tli id="T394" time="204.56900000000002" type="appl" />
         <tli id="T395" time="205.0295" type="appl" />
         <tli id="T396" time="205.49" type="appl" />
         <tli id="T397" time="205.498" type="appl" />
         <tli id="T398" time="205.88469999999998" type="appl" />
         <tli id="T399" time="206.2714" type="appl" />
         <tli id="T400" time="206.6581" type="appl" />
         <tli id="T401" time="207.0448" type="appl" />
         <tli id="T402" time="207.4315" type="appl" />
         <tli id="T403" time="207.8182" type="appl" />
         <tli id="T404" time="208.2049" type="appl" />
         <tli id="T405" time="208.5916" type="appl" />
         <tli id="T406" time="208.97830000000002" type="appl" />
         <tli id="T407" time="209.365" type="appl" />
         <tli id="T408" time="209.688" type="appl" />
         <tli id="T409" time="210.0305" type="appl" />
         <tli id="T410" time="210.373" type="appl" />
         <tli id="T411" time="210.7155" type="appl" />
         <tli id="T412" time="211.058" type="appl" />
         <tli id="T413" time="211.4005" type="appl" />
         <tli id="T414" time="211.743" type="appl" />
         <tli id="T415" time="212.0855" type="appl" />
         <tli id="T416" time="212.428" type="appl" />
         <tli id="T417" time="212.7705" type="appl" />
         <tli id="T418" time="213.113" type="appl" />
         <tli id="T419" time="213.4555" type="appl" />
         <tli id="T420" time="213.798" type="appl" />
         <tli id="T421" time="214.118" type="appl" />
         <tli id="T422" time="214.728" type="appl" />
         <tli id="T423" time="214.81" type="appl" />
         <tli id="T424" time="215.51" type="appl" />
         <tli id="T425" time="217.48" type="appl" />
         <tli id="T426" time="218.56334918388734" />
         <tli id="T427" time="222.71" type="appl" />
         <tli id="T428" time="223.27" type="appl" />
         <tli id="T429" time="223.83" type="appl" />
         <tli id="T430" time="225.41" type="appl" />
         <tli id="T431" time="226.11" type="appl" />
         <tli id="T432" time="226.93666117563535" />
         <tli id="T433" time="227.56" type="appl" />
         <tli id="T434" time="228.69" type="appl" />
         <tli id="T435" time="229.1" type="appl" />
         <tli id="T436" time="229.785" type="appl" />
         <tli id="T437" time="230.4994125116943" />
         <tli id="T438" time="230.76001080582444" />
         <tli id="T439" time="230.85166666666666" type="appl" />
         <tli id="T440" time="231.22333333333333" type="appl" />
         <tli id="T441" time="231.595" type="appl" />
         <tli id="T442" time="231.96666666666667" type="appl" />
         <tli id="T443" time="231.98" type="appl" />
         <tli id="T444" time="232.33833333333334" type="appl" />
         <tli id="T445" time="232.3997" type="appl" />
         <tli id="T446" time="232.71" type="appl" />
         <tli id="T447" time="232.8194" type="appl" />
         <tli id="T448" time="233.23909999999998" type="appl" />
         <tli id="T449" time="233.65879999999999" type="appl" />
         <tli id="T450" time="234.0785" type="appl" />
         <tli id="T451" time="234.4982" type="appl" />
         <tli id="T452" time="234.9179" type="appl" />
         <tli id="T453" time="235.33759999999998" type="appl" />
         <tli id="T454" time="235.7573" type="appl" />
         <tli id="T455" time="236.177" type="appl" />
         <tli id="T456" time="236.19" type="appl" />
         <tli id="T457" time="236.97" type="appl" />
         <tli id="T458" time="237.13" type="appl" />
         <tli id="T459" time="237.75" type="appl" />
         <tli id="T460" time="238.37" type="appl" />
         <tli id="T461" time="238.99" type="appl" />
         <tli id="T462" time="239.05" type="appl" />
         <tli id="T463" time="239.568" type="appl" />
         <tli id="T464" time="240.086" type="appl" />
         <tli id="T465" time="240.60399999999998" type="appl" />
         <tli id="T466" time="240.67" type="appl" />
         <tli id="T467" time="241.02166666666665" type="appl" />
         <tli id="T468" time="241.12199999999999" type="appl" />
         <tli id="T469" time="241.37333333333333" type="appl" />
         <tli id="T470" time="241.64" type="appl" />
         <tli id="T471" time="241.725" type="appl" />
         <tli id="T472" time="242.07666666666665" type="appl" />
         <tli id="T473" time="242.42833333333334" type="appl" />
         <tli id="T474" time="242.78" type="appl" />
         <tli id="T475" time="243.13166666666666" type="appl" />
         <tli id="T476" time="243.48333333333335" type="appl" />
         <tli id="T477" time="243.98604480413556" />
         <tli id="T478" time="244.957" type="appl" />
         <tli id="T479" time="245.25727272727272" type="appl" />
         <tli id="T480" time="245.55754545454545" type="appl" />
         <tli id="T481" time="245.85781818181817" type="appl" />
         <tli id="T482" time="246.1580909090909" type="appl" />
         <tli id="T483" time="246.45836363636363" type="appl" />
         <tli id="T484" time="246.75863636363636" type="appl" />
         <tli id="T485" time="247.05890909090908" type="appl" />
         <tli id="T486" time="247.3591818181818" type="appl" />
         <tli id="T487" time="247.65945454545454" type="appl" />
         <tli id="T488" time="247.95972727272726" type="appl" />
         <tli id="T489" time="248.31332544159073" />
         <tli id="T490" time="248.31999209126573" />
         <tli id="T491" time="248.65833333333333" type="appl" />
         <tli id="T492" time="248.89666666666665" type="appl" />
         <tli id="T493" time="249.135" type="appl" />
         <tli id="T494" time="249.37333333333333" type="appl" />
         <tli id="T495" time="249.61166666666665" type="appl" />
         <tli id="T496" time="249.85" type="appl" />
         <tli id="T497" time="251.53" type="appl" />
         <tli id="T498" time="252.04" type="appl" />
         <tli id="T499" time="252.55" type="appl" />
         <tli id="T500" time="252.61" type="appl" />
         <tli id="T501" time="252.9" type="appl" />
         <tli id="T502" time="253.06" type="appl" />
         <tli id="T503" time="253.19" type="appl" />
         <tli id="T504" time="253.48000000000002" type="appl" />
         <tli id="T505" time="253.77" type="appl" />
         <tli id="T506" time="254.06" type="appl" />
         <tli id="T507" time="254.35" type="appl" />
         <tli id="T508" time="254.816" type="appl" />
         <tli id="T509" time="255.28199999999998" type="appl" />
         <tli id="T510" time="255.748" type="appl" />
         <tli id="T511" time="256.214" type="appl" />
         <tli id="T512" time="256.68" type="appl" />
         <tli id="T513" time="257.146" type="appl" />
         <tli id="T514" time="257.61199999999997" type="appl" />
         <tli id="T515" time="258.078" type="appl" />
         <tli id="T516" time="258.544" type="appl" />
         <tli id="T517" time="259.01" type="appl" />
         <tli id="T518" time="259.476" type="appl" />
         <tli id="T519" time="259.942" type="appl" />
         <tli id="T520" time="260.28" type="appl" />
         <tli id="T521" time="260.40799999999996" type="appl" />
         <tli id="T522" time="260.87399999999997" type="appl" />
         <tli id="T523" time="261.03" type="appl" />
         <tli id="T524" time="261.34" type="appl" />
         <tli id="T525" time="261.78" type="appl" />
         <tli id="T526" time="261.806" type="appl" />
         <tli id="T527" time="261.94" type="appl" />
         <tli id="T528" time="262.416" type="appl" />
         <tli id="T529" time="262.892" type="appl" />
         <tli id="T530" time="263.368" type="appl" />
         <tli id="T531" time="263.844" type="appl" />
         <tli id="T532" time="264.6259921978075" />
         <tli id="T533" time="265.05" type="appl" />
         <tli id="T534" time="265.40444444444444" type="appl" />
         <tli id="T535" time="265.7588888888889" type="appl" />
         <tli id="T536" time="266.11333333333334" type="appl" />
         <tli id="T537" time="266.46777777777777" type="appl" />
         <tli id="T538" time="266.82222222222225" type="appl" />
         <tli id="T539" time="267.1766666666667" type="appl" />
         <tli id="T540" time="267.5311111111111" type="appl" />
         <tli id="T541" time="267.63" type="appl" />
         <tli id="T542" time="267.8855555555556" type="appl" />
         <tli id="T543" time="267.9588888888889" type="appl" />
         <tli id="T544" time="268.24" type="appl" />
         <tli id="T545" time="268.28777777777776" type="appl" />
         <tli id="T546" time="268.6166666666667" type="appl" />
         <tli id="T547" time="268.94555555555553" type="appl" />
         <tli id="T548" time="269.27444444444444" type="appl" />
         <tli id="T549" time="269.33" type="appl" />
         <tli id="T550" time="269.58714285714285" type="appl" />
         <tli id="T551" time="269.6033333333333" type="appl" />
         <tli id="T552" time="269.8442857142857" type="appl" />
         <tli id="T553" time="269.9322222222222" type="appl" />
         <tli id="T554" time="270.1014285714286" type="appl" />
         <tli id="T555" time="270.26111111111106" type="appl" />
         <tli id="T556" time="270.3585714285714" type="appl" />
         <tli id="T557" time="270.59" type="appl" />
         <tli id="T558" time="270.61571428571426" type="appl" />
         <tli id="T559" time="270.87285714285713" type="appl" />
         <tli id="T560" time="270.98299999999995" type="appl" />
         <tli id="T561" time="271.216678524712" />
         <tli id="T562" time="271.376" type="appl" />
         <tli id="T563" time="271.769" type="appl" />
         <tli id="T564" time="271.93" type="appl" />
         <tli id="T565" time="272.162" type="appl" />
         <tli id="T566" time="272.3781666666667" type="appl" />
         <tli id="T567" time="272.55499999999995" type="appl" />
         <tli id="T568" time="272.82633333333337" type="appl" />
         <tli id="T569" time="272.948" type="appl" />
         <tli id="T570" time="273.2745" type="appl" />
         <tli id="T571" time="273.72266666666667" type="appl" />
         <tli id="T572" time="274.17083333333335" type="appl" />
         <tli id="T573" time="274.59" type="appl" />
         <tli id="T574" time="274.619" type="appl" />
         <tli id="T575" time="275.25" type="appl" />
         <tli id="T576" time="275.3" type="appl" />
         <tli id="T577" time="275.73333333333335" type="appl" />
         <tli id="T578" time="276.1666666666667" type="appl" />
         <tli id="T579" time="276.6" type="appl" />
         <tli id="T580" time="277.0333333333333" type="appl" />
         <tli id="T581" time="277.46666666666664" type="appl" />
         <tli id="T582" time="277.9" type="appl" />
         <tli id="T583" time="277.92" type="appl" />
         <tli id="T584" time="278.4166666666667" type="appl" />
         <tli id="T585" time="278.91333333333336" type="appl" />
         <tli id="T586" time="279.37" type="appl" />
         <tli id="T587" time="279.41" type="appl" />
         <tli id="T588" time="279.7866666666667" type="appl" />
         <tli id="T589" time="280.2033333333333" type="appl" />
         <tli id="T590" time="280.62" type="appl" />
         <tli id="T591" time="280.78" type="appl" />
         <tli id="T592" time="281.25" type="appl" />
         <tli id="T593" time="281.28666666666663" type="appl" />
         <tli id="T594" time="281.72227272727275" type="appl" />
         <tli id="T595" time="281.79333333333335" type="appl" />
         <tli id="T596" time="282.19454545454545" type="appl" />
         <tli id="T597" time="282.3" type="appl" />
         <tli id="T598" time="282.6668181818182" type="appl" />
         <tli id="T599" time="283.1390909090909" type="appl" />
         <tli id="T600" time="283.61136363636365" type="appl" />
         <tli id="T601" time="284.08363636363634" type="appl" />
         <tli id="T602" time="284.5559090909091" type="appl" />
         <tli id="T603" time="285.0281818181818" type="appl" />
         <tli id="T604" time="285.50045454545455" type="appl" />
         <tli id="T605" time="285.97272727272724" type="appl" />
         <tli id="T606" time="286.445" type="appl" />
         <tli id="T607" time="286.53" type="appl" />
         <tli id="T608" time="287.0333333333333" type="appl" />
         <tli id="T609" time="287.5366666666667" type="appl" />
         <tli id="T610" time="288.04" type="appl" />
         <tli id="T611" time="288.22" type="appl" />
         <tli id="T612" time="288.6157142857143" type="appl" />
         <tli id="T613" time="289.0114285714286" type="appl" />
         <tli id="T614" time="289.4071428571429" type="appl" />
         <tli id="T615" time="289.80285714285714" type="appl" />
         <tli id="T616" time="290.1985714285714" type="appl" />
         <tli id="T617" time="290.5942857142857" type="appl" />
         <tli id="T618" time="290.99" type="appl" />
         <tli id="T619" time="291.3857142857143" type="appl" />
         <tli id="T620" time="291.7814285714286" type="appl" />
         <tli id="T621" time="292.1771428571429" type="appl" />
         <tli id="T622" time="292.5728571428571" type="appl" />
         <tli id="T623" time="292.9685714285714" type="appl" />
         <tli id="T624" time="293.3642857142857" type="appl" />
         <tli id="T625" time="293.76" type="appl" />
         <tli id="T626" time="293.82" type="appl" />
         <tli id="T627" time="294.316" type="appl" />
         <tli id="T628" time="294.812" type="appl" />
         <tli id="T629" time="295.308" type="appl" />
         <tli id="T630" time="295.80400000000003" type="appl" />
         <tli id="T631" time="296.3" type="appl" />
         <tli id="T632" time="296.62" type="appl" />
         <tli id="T633" time="297.2069230769231" type="appl" />
         <tli id="T634" time="297.7938461538462" type="appl" />
         <tli id="T635" time="298.38076923076926" type="appl" />
         <tli id="T636" time="298.9676923076923" type="appl" />
         <tli id="T637" time="299.5546153846154" type="appl" />
         <tli id="T638" time="300.14153846153846" type="appl" />
         <tli id="T639" time="300.72846153846154" type="appl" />
         <tli id="T640" time="301.31538461538463" type="appl" />
         <tli id="T641" time="301.9023076923077" type="appl" />
         <tli id="T642" time="302.48923076923074" type="appl" />
         <tli id="T643" time="303.07615384615383" type="appl" />
         <tli id="T644" time="303.6630769230769" type="appl" />
         <tli id="T645" time="304.25" type="appl" />
         <tli id="T646" time="304.536" type="appl" />
         <tli id="T647" time="304.822" type="appl" />
         <tli id="T648" time="305.108" type="appl" />
         <tli id="T649" time="305.394" type="appl" />
         <tli id="T650" time="305.76000194031093" />
         <tli id="T651" time="305.972" type="appl" />
         <tli id="T652" time="306.264" type="appl" />
         <tli id="T653" time="306.556" type="appl" />
         <tli id="T654" time="306.848" type="appl" />
         <tli id="T655" time="307.14" type="appl" />
         <tli id="T656" time="307.39" type="appl" />
         <tli id="T657" time="307.7272727272727" type="appl" />
         <tli id="T658" time="308.06454545454545" type="appl" />
         <tli id="T659" time="308.40181818181816" type="appl" />
         <tli id="T660" time="308.7390909090909" type="appl" />
         <tli id="T661" time="309.0763636363636" type="appl" />
         <tli id="T662" time="309.4136363636364" type="appl" />
         <tli id="T663" time="309.7509090909091" type="appl" />
         <tli id="T664" time="310.08818181818185" type="appl" />
         <tli id="T665" time="310.42545454545456" type="appl" />
         <tli id="T666" time="310.7627272727273" type="appl" />
         <tli id="T667" time="311.2125401264183" />
         <tli id="T668" time="311.63" type="appl" />
         <tli id="T669" time="312.16" type="appl" />
         <tli id="T670" time="312.69" type="appl" />
         <tli id="T671" time="313.22" type="appl" />
         <tli id="T672" time="313.9325331938014" />
         <tli id="T673" time="314.37" type="appl" />
         <tli id="T674" time="314.77875" type="appl" />
         <tli id="T675" time="315.1875" type="appl" />
         <tli id="T676" time="315.59625" type="appl" />
         <tli id="T677" time="316.005" type="appl" />
         <tli id="T678" time="316.41375" type="appl" />
         <tli id="T679" time="316.8225" type="appl" />
         <tli id="T680" time="317.23125" type="appl" />
         <tli id="T681" time="317.64" type="appl" />
         <tli id="T682" time="317.67" type="appl" />
         <tli id="T683" time="318.04" type="appl" />
         <tli id="T684" time="318.41" type="appl" />
         <tli id="T685" time="318.78" type="appl" />
         <tli id="T686" time="319.15" type="appl" />
         <tli id="T687" time="319.52" type="appl" />
         <tli id="T688" time="320.02332496148523" />
         <tli id="T689" time="320.093" type="appl" />
         <tli id="T690" time="320.44708333333335" type="appl" />
         <tli id="T691" time="320.8011666666667" type="appl" />
         <tli id="T692" time="321.15525" type="appl" />
         <tli id="T693" time="321.50933333333336" type="appl" />
         <tli id="T694" time="321.8634166666667" type="appl" />
         <tli id="T695" time="322.2175" type="appl" />
         <tli id="T696" time="322.5715833333333" type="appl" />
         <tli id="T697" time="322.92566666666664" type="appl" />
         <tli id="T698" time="323.27975" type="appl" />
         <tli id="T699" time="323.6338333333333" type="appl" />
         <tli id="T700" time="323.98791666666665" type="appl" />
         <tli id="T701" time="324.342" type="appl" />
         <tli id="T702" time="324.4" type="appl" />
         <tli id="T703" time="324.772" type="appl" />
         <tli id="T704" time="325.144" type="appl" />
         <tli id="T705" time="325.51599999999996" type="appl" />
         <tli id="T706" time="325.888" type="appl" />
         <tli id="T707" time="326.26" type="appl" />
         <tli id="T708" time="326.632" type="appl" />
         <tli id="T709" time="327.004" type="appl" />
         <tli id="T710" time="327.376" type="appl" />
         <tli id="T711" time="327.748" type="appl" />
         <tli id="T712" time="328.12" type="appl" />
         <tli id="T713" time="328.46" type="appl" />
         <tli id="T714" time="328.8" type="appl" />
         <tli id="T715" time="329.14" type="appl" />
         <tli id="T716" time="329.48" type="appl" />
         <tli id="T717" time="329.82" type="appl" />
         <tli id="T718" time="330.05" type="appl" />
         <tli id="T719" time="330.69" type="appl" />
         <tli id="T720" time="331.33" type="appl" />
         <tli id="T721" time="331.41" type="appl" />
         <tli id="T722" time="331.85833333333335" type="appl" />
         <tli id="T723" time="332.3066666666667" type="appl" />
         <tli id="T724" time="332.755" type="appl" />
         <tli id="T725" time="333.2033333333334" type="appl" />
         <tli id="T726" time="333.6516666666667" type="appl" />
         <tli id="T727" time="334.1" type="appl" />
         <tli id="T728" time="334.11" type="appl" />
         <tli id="T729" time="334.62428571428575" type="appl" />
         <tli id="T730" time="335.1385714285714" type="appl" />
         <tli id="T731" time="335.65285714285716" type="appl" />
         <tli id="T732" time="336.16714285714284" type="appl" />
         <tli id="T733" time="336.68142857142857" type="appl" />
         <tli id="T734" time="337.19571428571425" type="appl" />
         <tli id="T735" time="337.71" type="appl" />
         <tli id="T736" time="337.76" type="appl" />
         <tli id="T737" time="338.1483333333333" type="appl" />
         <tli id="T738" time="338.53666666666663" type="appl" />
         <tli id="T739" time="338.92499999999995" type="appl" />
         <tli id="T740" time="339.31333333333333" type="appl" />
         <tli id="T741" time="339.70166666666665" type="appl" />
         <tli id="T742" time="340.129992464361" />
         <tli id="T743" time="340.93" type="appl" />
         <tli id="T744" time="341.46285714285716" type="appl" />
         <tli id="T745" time="341.9957142857143" type="appl" />
         <tli id="T746" time="342.52857142857147" type="appl" />
         <tli id="T747" time="343.06142857142856" type="appl" />
         <tli id="T748" time="343.5942857142857" type="appl" />
         <tli id="T749" time="344.12714285714287" type="appl" />
         <tli id="T750" time="344.66" type="appl" />
         <tli id="T751" time="344.78" type="appl" />
         <tli id="T752" time="345.18899999999996" type="appl" />
         <tli id="T753" time="345.59799999999996" type="appl" />
         <tli id="T754" time="346.007" type="appl" />
         <tli id="T755" time="346.416" type="appl" />
         <tli id="T756" time="346.825" type="appl" />
         <tli id="T757" time="347.234" type="appl" />
         <tli id="T758" time="347.643" type="appl" />
         <tli id="T759" time="348.052" type="appl" />
         <tli id="T760" time="348.461" type="appl" />
         <tli id="T761" time="349.0099958730061" />
         <tli id="T762" time="349.0124437834337" />
         <tli id="T763" time="349.2577777777778" type="appl" />
         <tli id="T764" time="349.6355555555555" type="appl" />
         <tli id="T765" time="350.0133333333333" type="appl" />
         <tli id="T766" time="350.3911111111111" type="appl" />
         <tli id="T767" time="350.76888888888885" type="appl" />
         <tli id="T768" time="351.14666666666665" type="appl" />
         <tli id="T769" time="351.52444444444444" type="appl" />
         <tli id="T770" time="351.9022222222222" type="appl" />
         <tli id="T771" time="352.28" type="appl" />
         <tli id="T772" time="352.3" type="appl" />
         <tli id="T773" time="352.782" type="appl" />
         <tli id="T774" time="353.264" type="appl" />
         <tli id="T775" time="353.746" type="appl" />
         <tli id="T776" time="354.228" type="appl" />
         <tli id="T777" time="354.77667388344565" />
         <tli id="T778" time="354.82" type="appl" />
         <tli id="T779" time="355.2475" type="appl" />
         <tli id="T780" time="355.675" type="appl" />
         <tli id="T781" time="356.1025" type="appl" />
         <tli id="T782" time="356.53" type="appl" />
         <tli id="T783" time="356.9575" type="appl" />
         <tli id="T784" time="357.385" type="appl" />
         <tli id="T785" time="357.8125" type="appl" />
         <tli id="T786" time="358.24" type="appl" />
         <tli id="T787" time="360.36" type="appl" />
         <tli id="T788" time="360.952" type="appl" />
         <tli id="T789" time="361.544" type="appl" />
         <tli id="T790" time="362.136" type="appl" />
         <tli id="T791" time="362.728" type="appl" />
         <tli id="T792" time="363.32" type="appl" />
         <tli id="T793" time="363.45" type="appl" />
         <tli id="T794" time="363.925" type="appl" />
         <tli id="T795" time="364.4" type="appl" />
         <tli id="T796" time="364.875" type="appl" />
         <tli id="T797" time="365.37667290823015" />
         <tli id="T798" time="366.293337238537" />
         <tli id="T799" time="367.01375" type="appl" />
         <tli id="T800" time="367.6475" type="appl" />
         <tli id="T801" time="368.28125" type="appl" />
         <tli id="T802" time="368.91499999999996" type="appl" />
         <tli id="T803" time="369.54875" type="appl" />
         <tli id="T804" time="370.1825" type="appl" />
         <tli id="T805" time="370.81624999999997" type="appl" />
         <tli id="T806" time="371.45" type="appl" />
         <tli id="T807" time="372.25" type="appl" />
         <tli id="T808" time="372.63545454545454" type="appl" />
         <tli id="T809" time="373.0209090909091" type="appl" />
         <tli id="T810" time="373.40636363636366" type="appl" />
         <tli id="T811" time="373.7918181818182" type="appl" />
         <tli id="T812" time="374.17727272727274" type="appl" />
         <tli id="T813" time="374.5627272727273" type="appl" />
         <tli id="T814" time="374.9481818181818" type="appl" />
         <tli id="T815" time="375.33363636363634" type="appl" />
         <tli id="T816" time="375.71909090909094" type="appl" />
         <tli id="T817" time="376.1045454545455" type="appl" />
         <tli id="T818" time="376.49" type="appl" />
         <tli id="T819" time="376.99" type="appl" />
         <tli id="T820" time="377.665" type="appl" />
         <tli id="T821" time="378.34000000000003" type="appl" />
         <tli id="T822" time="379.015" type="appl" />
         <tli id="T823" time="379.8033288464409" />
         <tli id="T824" time="379.94" type="appl" />
         <tli id="T825" time="380.516" type="appl" />
         <tli id="T826" time="381.092" type="appl" />
         <tli id="T827" time="381.668" type="appl" />
         <tli id="T828" time="382.24399999999997" type="appl" />
         <tli id="T829" time="382.82" type="appl" />
         <tli id="T830" time="383.396" type="appl" />
         <tli id="T831" time="383.972" type="appl" />
         <tli id="T832" time="384.548" type="appl" />
         <tli id="T833" time="385.12399999999997" type="appl" />
         <tli id="T834" time="385.880006066766" />
         <tli id="T835" time="386.14" type="appl" />
         <tli id="T836" time="386.83666666666664" type="appl" />
         <tli id="T837" time="387.53333333333336" type="appl" />
         <tli id="T838" time="388.376666370038" />
         <tli id="T839" time="388.38" type="appl" />
         <tli id="T840" time="388.69536363636365" type="appl" />
         <tli id="T841" time="389.01072727272725" type="appl" />
         <tli id="T842" time="389.3260909090909" type="appl" />
         <tli id="T843" time="389.64145454545456" type="appl" />
         <tli id="T844" time="389.95681818181816" type="appl" />
         <tli id="T845" time="390.06" type="appl" />
         <tli id="T846" time="390.2721818181818" type="appl" />
         <tli id="T847" time="390.5875454545454" type="appl" />
         <tli id="T848" time="390.66" type="appl" />
         <tli id="T849" time="390.9029090909091" type="appl" />
         <tli id="T850" time="391.21827272727273" type="appl" />
         <tli id="T851" time="391.26" type="appl" />
         <tli id="T852" time="391.53363636363633" type="appl" />
         <tli id="T853" time="391.849" type="appl" />
         <tli id="T854" time="391.86" type="appl" />
         <tli id="T855" time="391.92" type="appl" />
         <tli id="T856" time="392.31" type="appl" />
         <tli id="T857" time="392.44" type="appl" />
         <tli id="T858" time="392.815" type="appl" />
         <tli id="T859" time="393.32" type="appl" />
         <tli id="T860" time="393.46" type="appl" />
         <tli id="T861" time="393.9918181818182" type="appl" />
         <tli id="T862" time="394.52363636363634" type="appl" />
         <tli id="T863" time="395.05545454545455" type="appl" />
         <tli id="T864" time="395.5872727272727" type="appl" />
         <tli id="T865" time="396.1190909090909" type="appl" />
         <tli id="T866" time="396.65090909090907" type="appl" />
         <tli id="T867" time="397.1827272727273" type="appl" />
         <tli id="T868" time="397.71454545454543" type="appl" />
         <tli id="T869" time="398.24636363636364" type="appl" />
         <tli id="T870" time="398.7781818181818" type="appl" />
         <tli id="T871" time="399.4566641714195" />
         <tli id="T872" time="399.45999749625696" />
         <tli id="T873" time="399.82" type="appl" />
         <tli id="T874" time="400.22" type="appl" />
         <tli id="T875" time="400.62" type="appl" />
         <tli id="T876" time="401.02" type="appl" />
         <tli id="T877" time="401.42" type="appl" />
         <tli id="T878" time="401.82" type="appl" />
         <tli id="T879" time="402.02" type="appl" />
         <tli id="T880" time="402.5711111111111" type="appl" />
         <tli id="T881" time="403.1222222222222" type="appl" />
         <tli id="T882" time="403.67333333333335" type="appl" />
         <tli id="T883" time="404.22444444444443" type="appl" />
         <tli id="T884" time="404.77555555555557" type="appl" />
         <tli id="T885" time="405.32666666666665" type="appl" />
         <tli id="T886" time="405.8777777777778" type="appl" />
         <tli id="T887" time="406.2589645423034" />
         <tli id="T888" time="406.9266711738109" />
         <tli id="T889" time="407.7789606681939" />
         <tli id="T890" time="408.004375" type="appl" />
         <tli id="T891" time="408.35875" type="appl" />
         <tli id="T892" time="408.713125" type="appl" />
         <tli id="T893" time="409.0675" type="appl" />
         <tli id="T894" time="409.421875" type="appl" />
         <tli id="T895" time="409.77625" type="appl" />
         <tli id="T896" time="410.130625" type="appl" />
         <tli id="T897" time="410.485" type="appl" />
         <tli id="T898" time="410.83937499999996" type="appl" />
         <tli id="T899" time="411.19374999999997" type="appl" />
         <tli id="T900" time="411.54812499999997" type="appl" />
         <tli id="T901" time="411.9025" type="appl" />
         <tli id="T902" time="412.256875" type="appl" />
         <tli id="T903" time="412.61125" type="appl" />
         <tli id="T904" time="412.965625" type="appl" />
         <tli id="T905" time="413.91998668284253" />
         <tli id="T906" time="414.8189427249502" />
         <tli id="T907" time="415.39" type="appl" />
         <tli id="T908" time="415.48" type="appl" />
         <tli id="T909" time="415.96560646904305" />
         <tli id="T910" time="416.148" type="appl" />
         <tli id="T911" time="416.53" type="appl" />
         <tli id="T912" time="416.546" type="appl" />
         <tli id="T913" time="416.944" type="appl" />
         <tli id="T914" time="417.342" type="appl" />
         <tli id="T915" time="417.83333608364353" />
         <tli id="T916" time="418.86" type="appl" />
         <tli id="T917" time="419.375" type="appl" />
         <tli id="T918" time="419.89" type="appl" />
         <tli id="T919" time="421.07" type="appl" />
         <tli id="T920" time="421.4584615384615" type="appl" />
         <tli id="T921" time="421.8469230769231" type="appl" />
         <tli id="T922" time="422.2353846153846" type="appl" />
         <tli id="T923" time="422.62384615384616" type="appl" />
         <tli id="T924" time="423.0123076923077" type="appl" />
         <tli id="T925" time="423.40076923076924" type="appl" />
         <tli id="T926" time="423.78923076923076" type="appl" />
         <tli id="T927" time="424.1776923076923" type="appl" />
         <tli id="T928" time="424.56615384615384" type="appl" />
         <tli id="T929" time="424.76" type="appl" />
         <tli id="T930" time="424.9546153846154" type="appl" />
         <tli id="T931" time="425.3430769230769" type="appl" />
         <tli id="T932" time="425.7315384615385" type="appl" />
         <tli id="T933" time="426.3433404353285" />
         <tli id="T934" time="426.44667350529033" />
         <tli id="T935" time="426.61" type="appl" />
         <tli id="T936" time="426.90777777777777" type="appl" />
         <tli id="T937" time="427.2055555555556" type="appl" />
         <tli id="T938" time="427.50333333333333" type="appl" />
         <tli id="T939" time="427.80111111111114" type="appl" />
         <tli id="T940" time="428.0988888888889" type="appl" />
         <tli id="T941" time="428.3966666666667" type="appl" />
         <tli id="T942" time="428.69444444444446" type="appl" />
         <tli id="T943" time="428.99222222222227" type="appl" />
         <tli id="T944" time="429.33666613938493" />
         <tli id="T945" time="429.6175" type="appl" />
         <tli id="T946" time="429.94500000000005" type="appl" />
         <tli id="T947" time="430.27250000000004" type="appl" />
         <tli id="T948" time="430.73332924628875" />
         <tli id="T949" time="434.67" type="appl" />
         <tli id="T950" time="435.23" type="appl" />
         <tli id="T951" time="435.43" type="appl" />
         <tli id="T952" time="435.65000000000003" type="appl" />
         <tli id="T953" time="436.07000000000005" type="appl" />
         <tli id="T954" time="436.19" type="appl" />
         <tli id="T955" time="436.49" type="appl" />
         <tli id="T956" time="437.1033130107117" />
         <tli id="T957" time="438.44" type="appl" />
         <tli id="T958" time="439.3566926840483" />
         <tli id="T959" time="439.69" type="appl" />
         <tli id="T960" time="440.14" type="appl" />
         <tli id="T961" time="440.63" type="appl" />
         <tli id="T962" time="441.12" type="appl" />
         <tli id="T963" time="441.61" type="appl" />
         <tli id="T964" time="442.1533522226934" />
         <tli id="T965" time="442.16" type="appl" />
         <tli id="T966" time="442.62" type="appl" />
         <tli id="T967" time="443.08000000000004" type="appl" />
         <tli id="T968" time="443.6066818518344" />
         <tli id="T969" time="443.61" type="appl" />
         <tli id="T970" time="444.45125" type="appl" />
         <tli id="T971" time="445.2925" type="appl" />
         <tli id="T972" time="446.13375" type="appl" />
         <tli id="T973" time="446.975" type="appl" />
         <tli id="T974" time="447.81624999999997" type="appl" />
         <tli id="T975" time="448.65749999999997" type="appl" />
         <tli id="T976" time="449.49875" type="appl" />
         <tli id="T977" time="450.34" type="appl" />
         <tli id="T978" time="450.92" type="appl" />
         <tli id="T979" time="451.6075" type="appl" />
         <tli id="T980" time="452.295" type="appl" />
         <tli id="T981" time="452.9825" type="appl" />
         <tli id="T982" time="453.67" type="appl" />
         <tli id="T983" time="454.3575" type="appl" />
         <tli id="T984" time="455.045" type="appl" />
         <tli id="T985" time="455.7325" type="appl" />
         <tli id="T986" time="456.42" type="appl" />
         <tli id="T987" time="456.71" type="appl" />
         <tli id="T988" time="457.455" type="appl" />
         <tli id="T989" time="457.8799788059208" />
         <tli id="T990" time="457.8866454555958" />
         <tli id="T991" time="459.05" type="appl" />
         <tli id="T992" time="459.84" type="appl" />
         <tli id="T993" time="460.6700237780916" />
         <tli id="T994" time="460.93" type="appl" />
         <tli id="T995" time="461.572" type="appl" />
         <tli id="T996" time="462.214" type="appl" />
         <tli id="T997" time="462.856" type="appl" />
         <tli id="T998" time="463.498" type="appl" />
         <tli id="T999" time="464.14" type="appl" />
         <tli id="T1000" time="464.69" type="appl" />
         <tli id="T1001" time="465.49857142857144" type="appl" />
         <tli id="T1002" time="466.3071428571429" type="appl" />
         <tli id="T1003" time="467.1157142857143" type="appl" />
         <tli id="T1004" time="467.9242857142857" type="appl" />
         <tli id="T1005" time="468.73285714285714" type="appl" />
         <tli id="T1006" time="469.5414285714286" type="appl" />
         <tli id="T1007" time="470.35" type="appl" />
         <tli id="T1008" time="470.81" type="appl" />
         <tli id="T1009" time="471.795" type="appl" />
         <tli id="T1010" time="472.78" type="appl" />
         <tli id="T1011" time="473.765" type="appl" />
         <tli id="T1012" time="474.75" type="appl" />
         <tli id="T1013" time="474.83" type="appl" />
         <tli id="T1014" time="475.4657142857143" type="appl" />
         <tli id="T1015" time="476.1014285714285" type="appl" />
         <tli id="T1016" time="476.7371428571428" type="appl" />
         <tli id="T1017" time="477.37285714285713" type="appl" />
         <tli id="T1018" time="478.00857142857143" type="appl" />
         <tli id="T1019" time="478.6442857142857" type="appl" />
         <tli id="T1020" time="479.31997624378835" />
         <tli id="T1021" time="479.8242857142857" type="appl" />
         <tli id="T1022" time="480.3685714285714" type="appl" />
         <tli id="T1023" time="480.9128571428571" type="appl" />
         <tli id="T1024" time="481.45714285714286" type="appl" />
         <tli id="T1025" time="482.00142857142856" type="appl" />
         <tli id="T1026" time="482.54571428571427" type="appl" />
         <tli id="T1027" time="483.09" type="appl" />
         <tli id="T1028" time="483.21" type="appl" />
         <tli id="T1029" time="483.756" type="appl" />
         <tli id="T1030" time="484.30199999999996" type="appl" />
         <tli id="T1031" time="484.848" type="appl" />
         <tli id="T1032" time="485.394" type="appl" />
         <tli id="T1033" time="485.94" type="appl" />
         <tli id="T1034" time="486.11" type="appl" />
         <tli id="T1035" time="486.9066666666667" type="appl" />
         <tli id="T1036" time="487.7033333333333" type="appl" />
         <tli id="T1037" time="488.5" type="appl" />
         <tli id="T1038" time="490.95" type="appl" />
         <tli id="T1039" time="491.64" type="appl" />
         <tli id="T1040" time="492.23" type="appl" />
         <tli id="T1041" time="492.33" type="appl" />
         <tli id="T1042" time="492.6147368421053" type="appl" />
         <tli id="T1043" time="492.99947368421056" type="appl" />
         <tli id="T1044" time="493.3842105263158" type="appl" />
         <tli id="T1045" time="493.7689473684211" type="appl" />
         <tli id="T1046" time="494.1536842105263" type="appl" />
         <tli id="T1047" time="494.5384210526316" type="appl" />
         <tli id="T1048" time="494.92315789473685" type="appl" />
         <tli id="T1049" time="495.3078947368421" type="appl" />
         <tli id="T1050" time="495.6926315789474" type="appl" />
         <tli id="T1051" time="496.07736842105265" type="appl" />
         <tli id="T1052" time="496.4621052631579" type="appl" />
         <tli id="T1053" time="496.8468421052632" type="appl" />
         <tli id="T1054" time="497.23157894736846" type="appl" />
         <tli id="T1055" time="497.61631578947373" type="appl" />
         <tli id="T1056" time="498.00105263157894" type="appl" />
         <tli id="T1057" time="498.3857894736842" type="appl" />
         <tli id="T1058" time="498.7705263157895" type="appl" />
         <tli id="T1059" time="499.15526315789475" type="appl" />
         <tli id="T1060" time="499.55997674016334" />
         <tli id="T1061" time="499.77" type="appl" />
         <tli id="T1062" time="499.966" type="appl" />
         <tli id="T1063" time="500.392" type="appl" />
         <tli id="T1064" time="500.74" type="appl" />
         <tli id="T1065" time="500.81800000000004" type="appl" />
         <tli id="T1066" time="501.244" type="appl" />
         <tli id="T1067" time="501.67" type="appl" />
         <tli id="T1068" time="502.12" type="appl" />
         <tli id="T1069" time="502.57" type="appl" />
         <tli id="T1070" time="503.02000000000004" type="appl" />
         <tli id="T1071" time="503.47" type="appl" />
         <tli id="T1072" time="504.0733506533109" />
         <tli id="T1073" time="504.81" type="appl" />
         <tli id="T1074" time="505.4685714285714" type="appl" />
         <tli id="T1075" time="506.12714285714287" type="appl" />
         <tli id="T1076" time="506.7857142857143" type="appl" />
         <tli id="T1077" time="507.44428571428574" type="appl" />
         <tli id="T1078" time="508.10285714285715" type="appl" />
         <tli id="T1079" time="508.7614285714286" type="appl" />
         <tli id="T1080" time="509.46667024035236" />
         <tli id="T1081" time="510.0025" type="appl" />
         <tli id="T1082" time="510.58500000000004" type="appl" />
         <tli id="T1083" time="511.1675" type="appl" />
         <tli id="T1084" time="511.75" type="appl" />
         <tli id="T1085" time="511.84" type="appl" />
         <tli id="T1086" time="512.4399999999999" type="appl" />
         <tli id="T1087" time="513.04" type="appl" />
         <tli id="T1088" time="513.64" type="appl" />
         <tli id="T1089" time="514.24" type="appl" />
         <tli id="T1090" time="514.63" type="appl" />
         <tli id="T1091" time="515.3133333333333" type="appl" />
         <tli id="T1092" time="515.9966666666667" type="appl" />
         <tli id="T1093" time="516.7266517363822" />
         <tli id="T1094" time="519.91" type="appl" />
         <tli id="T1095" time="520.3183333333333" type="appl" />
         <tli id="T1096" time="520.7266666666667" type="appl" />
         <tli id="T1097" time="521.135" type="appl" />
         <tli id="T1098" time="521.5433333333333" type="appl" />
         <tli id="T1099" time="521.9516666666667" type="appl" />
         <tli id="T1100" time="522.36" type="appl" />
         <tli id="T1101" time="522.45" type="appl" />
         <tli id="T1102" time="522.735" type="appl" />
         <tli id="T1103" time="523.02" type="appl" />
         <tli id="T1104" time="523.3050000000001" type="appl" />
         <tli id="T1105" time="523.59" type="appl" />
         <tli id="T1106" time="523.875" type="appl" />
         <tli id="T1107" time="524.1600000000001" type="appl" />
         <tli id="T1108" time="524.445" type="appl" />
         <tli id="T1109" time="524.73" type="appl" />
         <tli id="T1110" time="524.98875" type="appl" />
         <tli id="T1111" time="525.2475" type="appl" />
         <tli id="T1112" time="525.50625" type="appl" />
         <tli id="T1113" time="525.765" type="appl" />
         <tli id="T1114" time="526.02375" type="appl" />
         <tli id="T1115" time="526.2825" type="appl" />
         <tli id="T1116" time="526.54125" type="appl" />
         <tli id="T1117" time="526.8" type="appl" />
         <tli id="T1118" time="527.62" type="appl" />
         <tli id="T1119" time="527.9575" type="appl" />
         <tli id="T1120" time="528.295" type="appl" />
         <tli id="T1121" time="528.6324999999999" type="appl" />
         <tli id="T1122" time="528.97" type="appl" />
         <tli id="T1123" time="529.3075" type="appl" />
         <tli id="T1124" time="529.645" type="appl" />
         <tli id="T1125" time="529.9825" type="appl" />
         <tli id="T1126" time="530.3199999999999" type="appl" />
         <tli id="T1127" time="530.6575" type="appl" />
         <tli id="T1128" time="530.995" type="appl" />
         <tli id="T1129" time="531.3325" type="appl" />
         <tli id="T1130" time="531.67" type="appl" />
         <tli id="T1131" time="532.0799999999999" type="appl" />
         <tli id="T1132" time="532.49" type="appl" />
         <tli id="T1133" time="532.73" type="appl" />
         <tli id="T1134" time="532.9" type="appl" />
         <tli id="T1135" time="533.3100000000001" type="appl" />
         <tli id="T1136" time="533.47" type="appl" />
         <tli id="T1137" time="533.72" type="appl" />
         <tli id="T1138" time="534.21" type="appl" />
         <tli id="T1139" time="535.1053028102286" />
         <tli id="T1140" time="535.29" type="appl" />
         <tli id="T1141" time="536.02" type="appl" />
         <tli id="T1142" time="536.42" type="appl" />
         <tli id="T1143" time="536.8475" type="appl" />
         <tli id="T1144" time="537.275" type="appl" />
         <tli id="T1145" time="537.31" type="appl" />
         <tli id="T1146" time="537.7025" type="appl" />
         <tli id="T1147" time="537.74" type="appl" />
         <tli id="T1148" time="538.13" type="appl" />
         <tli id="T1149" time="538.17" type="appl" />
         <tli id="T1150" time="538.48" type="appl" />
         <tli id="T1151" time="538.79" type="appl" />
         <tli id="T1152" time="539.0999999999999" type="appl" />
         <tli id="T1153" time="539.41" type="appl" />
         <tli id="T1154" time="539.72" type="appl" />
         <tli id="T1155" time="540.03" type="appl" />
         <tli id="T1156" time="540.3399999999999" type="appl" />
         <tli id="T1157" time="540.845288180368" />
         <tli id="T1158" time="541.25" type="appl" />
         <tli id="T1159" time="541.7175" type="appl" />
         <tli id="T1160" time="542.185" type="appl" />
         <tli id="T1161" time="542.6525" type="appl" />
         <tli id="T1162" time="543.12" type="appl" />
         <tli id="T1163" time="543.52" type="appl" />
         <tli id="T1164" time="544.0699999999999" type="appl" />
         <tli id="T1165" time="544.62" type="appl" />
         <tli id="T1166" time="545.17" type="appl" />
         <tli id="T1167" time="545.331943411615" />
         <tli id="T1168" time="545.5400000000001" type="appl" />
         <tli id="T1169" time="545.88" type="appl" />
         <tli id="T1170" time="546.22" type="appl" />
         <tli id="T1171" time="546.56" type="appl" />
         <tli id="T1172" time="546.9" type="appl" />
         <tli id="T1173" time="546.92" type="appl" />
         <tli id="T1174" time="547.3833333333333" type="appl" />
         <tli id="T1175" time="547.8466666666666" type="appl" />
         <tli id="T1176" time="548.31" type="appl" />
         <tli id="T1177" time="554.44" type="appl" />
         <tli id="T1178" time="555.1650000000001" type="appl" />
         <tli id="T1179" time="555.8900000000001" type="appl" />
         <tli id="T1180" time="556.615" type="appl" />
         <tli id="T1181" time="557.34" type="appl" />
         <tli id="T1182" time="557.56" type="appl" />
         <tli id="T1183" time="558.2833333333333" type="appl" />
         <tli id="T1184" time="559.0066666666667" type="appl" />
         <tli id="T1185" time="559.73" type="appl" />
         <tli id="T1186" time="560.4533333333333" type="appl" />
         <tli id="T1187" time="561.1766666666666" type="appl" />
         <tli id="T1188" time="561.9" type="appl" />
         <tli id="T1189" time="562.94" type="appl" />
         <tli id="T1190" time="563.75" type="appl" />
         <tli id="T1191" time="564.56" type="appl" />
         <tli id="T1739" time="564.9338461538462" type="intp" />
         <tli id="T1192" time="565.37" type="appl" />
         <tli id="T1193" time="566.0600155806779" />
         <tli id="T1194" time="566.64" type="appl" />
         <tli id="T1195" time="567.6441666666667" type="appl" />
         <tli id="T1196" time="568.6483333333333" type="appl" />
         <tli id="T1197" time="569.6525" type="appl" />
         <tli id="T1198" time="570.6566666666666" type="appl" />
         <tli id="T1199" time="571.6608333333334" type="appl" />
         <tli id="T1200" time="572.665" type="appl" />
         <tli id="T1201" time="573.6691666666667" type="appl" />
         <tli id="T1202" time="574.6733333333334" type="appl" />
         <tli id="T1203" time="575.6775" type="appl" />
         <tli id="T1204" time="576.6816666666667" type="appl" />
         <tli id="T1205" time="577.6858333333333" type="appl" />
         <tli id="T1206" time="578.69" type="appl" />
         <tli id="T1207" time="579.98" type="appl" />
         <tli id="T1208" time="580.9277142857143" type="appl" />
         <tli id="T1209" time="581.8754285714286" type="appl" />
         <tli id="T1210" time="582.8231428571429" type="appl" />
         <tli id="T1211" time="583.7708571428572" type="appl" />
         <tli id="T1212" time="584.7185714285714" type="appl" />
         <tli id="T1213" time="585.6662857142858" type="appl" />
         <tli id="T1214" time="586.614" type="appl" />
         <tli id="T1215" time="586.98" type="appl" />
         <tli id="T1216" time="587.785" type="appl" />
         <tli id="T1217" time="588.59" type="appl" />
         <tli id="T1218" time="589.395" type="appl" />
         <tli id="T1219" time="590.2" type="appl" />
         <tli id="T1220" time="591.005" type="appl" />
         <tli id="T1221" time="591.81" type="appl" />
         <tli id="T1222" time="592.615" type="appl" />
         <tli id="T1223" time="593.3399981338092" />
         <tli id="T1224" time="593.52" type="appl" />
         <tli id="T1225" time="594.3333333333334" type="appl" />
         <tli id="T1226" time="595.1466666666666" type="appl" />
         <tli id="T1227" time="595.9933247044427" />
         <tli id="T1228" time="596.505" type="appl" />
         <tli id="T1229" time="597.0500000000001" type="appl" />
         <tli id="T1230" time="597.595" type="appl" />
         <tli id="T1231" time="598.1400000000001" type="appl" />
         <tli id="T1232" time="598.6850000000001" type="appl" />
         <tli id="T1233" time="599.23" type="appl" />
         <tli id="T1234" time="599.7750000000001" type="appl" />
         <tli id="T1235" time="600.2066472990166" />
         <tli id="T1236" time="603.431" type="appl" />
         <tli id="T1237" time="605.133" type="appl" />
         <tli id="T1238" time="605.8710078619056" />
         <tli id="T1239" time="606.0921111111111" type="appl" />
         <tli id="T1240" time="606.4532222222222" type="appl" />
         <tli id="T1241" time="606.8143333333334" type="appl" />
         <tli id="T1242" time="607.1754444444445" type="appl" />
         <tli id="T1243" time="607.5365555555555" type="appl" />
         <tli id="T1244" time="607.8976666666666" type="appl" />
         <tli id="T1245" time="608.2587777777778" type="appl" />
         <tli id="T1246" time="608.6198888888889" type="appl" />
         <tli id="T1247" time="608.981" type="appl" />
         <tli id="T1248" time="611.391" type="appl" />
         <tli id="T1249" time="612.001" type="appl" />
         <tli id="T1250" time="612.611" type="appl" />
         <tli id="T1251" time="613.221" type="appl" />
         <tli id="T1252" time="613.831" type="appl" />
         <tli id="T1253" time="614.4409999999999" type="appl" />
         <tli id="T1254" time="615.0509999999999" type="appl" />
         <tli id="T1255" time="615.661" type="appl" />
         <tli id="T1256" time="616.271" type="appl" />
         <tli id="T1257" time="616.8809798000998" />
         <tli id="T1258" time="617.4538571428571" type="appl" />
         <tli id="T1259" time="618.0267142857142" type="appl" />
         <tli id="T1260" time="618.5995714285714" type="appl" />
         <tli id="T1261" time="619.1724285714286" type="appl" />
         <tli id="T1262" time="619.7452857142857" type="appl" />
         <tli id="T1263" time="620.3181428571428" type="appl" />
         <tli id="T1264" time="620.891" type="appl" />
         <tli id="T1265" time="621.971" type="appl" />
         <tli id="T1266" time="622.5476666666667" type="appl" />
         <tli id="T1267" time="623.1243333333333" type="appl" />
         <tli id="T1268" time="623.701" type="appl" />
         <tli id="T1269" time="624.2776666666667" type="appl" />
         <tli id="T1270" time="624.8543333333333" type="appl" />
         <tli id="T1271" time="625.431" type="appl" />
         <tli id="T1272" time="625.561" type="appl" />
         <tli id="T1273" time="626.1310000000001" type="appl" />
         <tli id="T1274" time="626.701" type="appl" />
         <tli id="T1275" time="627.271" type="appl" />
         <tli id="T1276" time="627.841" type="appl" />
         <tli id="T1277" time="628.4110000000001" type="appl" />
         <tli id="T1278" time="628.981" type="appl" />
         <tli id="T1279" time="629.391" type="appl" />
         <tli id="T1280" time="629.96475" type="appl" />
         <tli id="T1281" time="630.5385" type="appl" />
         <tli id="T1282" time="631.11225" type="appl" />
         <tli id="T1283" time="631.6859999999999" type="appl" />
         <tli id="T1284" time="632.2597499999999" type="appl" />
         <tli id="T1285" time="632.8335" type="appl" />
         <tli id="T1286" time="633.40725" type="appl" />
         <tli id="T1287" time="633.981" type="appl" />
         <tli id="T1288" time="635.861" type="appl" />
         <tli id="T1289" time="636.671" type="appl" />
         <tli id="T1290" time="636.961" type="appl" />
         <tli id="T1291" time="637.4368333333333" type="appl" />
         <tli id="T1292" time="637.9126666666667" type="appl" />
         <tli id="T1293" time="638.3885" type="appl" />
         <tli id="T1294" time="638.8643333333333" type="appl" />
         <tli id="T1295" time="639.3401666666667" type="appl" />
         <tli id="T1296" time="639.816" type="appl" />
         <tli id="T1297" time="640.2918333333333" type="appl" />
         <tli id="T1298" time="640.7676666666667" type="appl" />
         <tli id="T1299" time="641.2435" type="appl" />
         <tli id="T1300" time="641.7193333333333" type="appl" />
         <tli id="T1301" time="642.1951666666668" type="appl" />
         <tli id="T1302" time="642.8576844249772" />
         <tli id="T1303" time="643.181" type="appl" />
         <tli id="T1304" time="643.861" type="appl" />
         <tli id="T1305" time="644.5409999999999" type="appl" />
         <tli id="T1306" time="645.221" type="appl" />
         <tli id="T1307" time="645.901" type="appl" />
         <tli id="T1308" time="646.071" type="appl" />
         <tli id="T1309" time="646.48225" type="appl" />
         <tli id="T1310" time="646.8935" type="appl" />
         <tli id="T1311" time="647.30475" type="appl" />
         <tli id="T1312" time="647.716" type="appl" />
         <tli id="T1313" time="648.12725" type="appl" />
         <tli id="T1314" time="648.5385" type="appl" />
         <tli id="T1315" time="648.94975" type="appl" />
         <tli id="T1316" time="649.361" type="appl" />
         <tli id="T1317" time="649.67" type="appl" />
         <tli id="T1318" time="650.1847058823529" type="appl" />
         <tli id="T1319" time="650.6994117647058" type="appl" />
         <tli id="T1320" time="651.2141176470587" type="appl" />
         <tli id="T1321" time="651.7288235294117" type="appl" />
         <tli id="T1322" time="652.2435294117647" type="appl" />
         <tli id="T1323" time="652.7582352941176" type="appl" />
         <tli id="T1324" time="653.2729411764706" type="appl" />
         <tli id="T1325" time="653.7876470588235" type="appl" />
         <tli id="T1326" time="654.3023529411764" type="appl" />
         <tli id="T1327" time="654.8170588235294" type="appl" />
         <tli id="T1328" time="655.3317647058823" type="appl" />
         <tli id="T1329" time="655.8464705882352" type="appl" />
         <tli id="T1330" time="656.3611764705882" type="appl" />
         <tli id="T1331" time="656.8758823529412" type="appl" />
         <tli id="T1332" time="657.3905882352941" type="appl" />
         <tli id="T1333" time="657.905294117647" type="appl" />
         <tli id="T1334" time="658.42" type="appl" />
         <tli id="T1335" time="658.52" type="appl" />
         <tli id="T1336" time="658.9525" type="appl" />
         <tli id="T1337" time="659.385" type="appl" />
         <tli id="T1338" time="659.8175" type="appl" />
         <tli id="T1339" time="660.1649840627774" />
         <tli id="T1340" time="660.4983165465253" />
         <tli id="T1341" time="661.0716484185717" />
         <tli id="T1342" time="662.3399999999999" type="appl" />
         <tli id="T1343" time="662.93" type="appl" />
         <tli id="T1344" time="663.52" type="appl" />
         <tli id="T1345" time="663.6316418937558" />
         <tli id="T1346" time="664.1" type="appl" />
         <tli id="T1347" time="664.15" type="appl" />
         <tli id="T1348" time="664.845" type="appl" />
         <tli id="T1349" time="665.54" type="appl" />
         <tli id="T1350" time="666.235" type="appl" />
         <tli id="T1351" time="666.9300000000001" type="appl" />
         <tli id="T1352" time="667.625" type="appl" />
         <tli id="T1353" time="668.0316306792286" />
         <tli id="T1354" time="668.6582957486746" />
         <tli id="T1355" time="669.5449601554441" />
         <tli id="T1356" time="670.4249579125386" />
         <tli id="T1357" time="670.6725" type="appl" />
         <tli id="T1358" time="671.1550000000001" type="appl" />
         <tli id="T1359" time="671.6375" type="appl" />
         <tli id="T1360" time="672.12" type="appl" />
         <tli id="T1361" time="672.6025000000001" type="appl" />
         <tli id="T1362" time="673.085" type="appl" />
         <tli id="T1363" time="673.5675" type="appl" />
         <tli id="T1364" time="674.05" type="appl" />
         <tli id="T1365" time="674.5325" type="appl" />
         <tli id="T1366" time="675.015" type="appl" />
         <tli id="T1367" time="675.4975" type="appl" />
         <tli id="T1368" time="675.98" type="appl" />
         <tli id="T1369" time="676.4625" type="appl" />
         <tli id="T1370" time="676.9449999999999" type="appl" />
         <tli id="T1371" time="677.4275" type="appl" />
         <tli id="T1372" time="678.0716050897163" />
         <tli id="T1373" time="678.5766666666666" type="appl" />
         <tli id="T1374" time="679.2433333333333" type="appl" />
         <tli id="T1375" time="679.91" type="appl" />
         <tli id="T1376" time="681.39" type="appl" />
         <tli id="T1377" time="682.34" type="appl" />
         <tli id="T1378" time="683.02" type="appl" />
         <tli id="T1379" time="683.45" type="appl" />
         <tli id="T1380" time="683.48" type="appl" />
         <tli id="T1381" time="683.88" type="appl" />
         <tli id="T1382" time="683.8857142857144" type="appl" />
         <tli id="T1383" time="684.2914285714286" type="appl" />
         <tli id="T1384" time="684.6971428571429" type="appl" />
         <tli id="T1385" time="685.1028571428571" type="appl" />
         <tli id="T1386" time="685.5085714285715" type="appl" />
         <tli id="T1387" time="685.9142857142857" type="appl" />
         <tli id="T1388" time="686.4200213115844" />
         <tli id="T1389" time="686.82" type="appl" />
         <tli id="T1390" time="687.1600000000001" type="appl" />
         <tli id="T1391" time="687.5" type="appl" />
         <tli id="T1392" time="687.84" type="appl" />
         <tli id="T1393" time="688.18" type="appl" />
         <tli id="T1394" time="688.566682506921" />
         <tli id="T1395" time="688.838" type="appl" />
         <tli id="T1396" time="689.156" type="appl" />
         <tli id="T1397" time="689.474" type="appl" />
         <tli id="T1398" time="689.792" type="appl" />
         <tli id="T1399" time="690.11" type="appl" />
         <tli id="T1400" time="690.428" type="appl" />
         <tli id="T1401" time="690.746" type="appl" />
         <tli id="T1402" time="691.0640000000001" type="appl" />
         <tli id="T1403" time="691.3820000000001" type="appl" />
         <tli id="T1404" time="691.7" type="appl" />
         <tli id="T1405" time="691.72" type="appl" />
         <tli id="T1406" time="692.0057142857144" type="appl" />
         <tli id="T1407" time="692.2914285714286" type="appl" />
         <tli id="T1408" time="692.5771428571429" type="appl" />
         <tli id="T1409" time="692.8628571428571" type="appl" />
         <tli id="T1410" time="693.1485714285715" type="appl" />
         <tli id="T1411" time="693.4342857142857" type="appl" />
         <tli id="T1412" time="693.72" type="appl" />
         <tli id="T1413" time="694.0057142857144" type="appl" />
         <tli id="T1414" time="694.2914285714286" type="appl" />
         <tli id="T1415" time="694.5771428571429" type="appl" />
         <tli id="T1416" time="694.8628571428571" type="appl" />
         <tli id="T1417" time="695.1485714285715" type="appl" />
         <tli id="T1418" time="695.4342857142857" type="appl" />
         <tli id="T1419" time="695.72" type="appl" />
         <tli id="T1420" time="695.75" type="appl" />
         <tli id="T1421" time="696.1308333333334" type="appl" />
         <tli id="T1422" time="696.5116666666667" type="appl" />
         <tli id="T1423" time="696.8925" type="appl" />
         <tli id="T1424" time="697.2733333333333" type="appl" />
         <tli id="T1425" time="697.6541666666667" type="appl" />
         <tli id="T1426" time="698.0350000000001" type="appl" />
         <tli id="T1427" time="698.4158333333334" type="appl" />
         <tli id="T1428" time="698.7966666666667" type="appl" />
         <tli id="T1429" time="699.1775" type="appl" />
         <tli id="T1430" time="699.5583333333334" type="appl" />
         <tli id="T1431" time="699.9391666666667" type="appl" />
         <tli id="T1432" time="700.32" type="appl" />
         <tli id="T1433" time="700.72" type="appl" />
         <tli id="T1434" time="701.16" type="appl" />
         <tli id="T1435" time="701.29" type="appl" />
         <tli id="T1436" time="701.6063636363637" type="appl" />
         <tli id="T1437" time="701.9227272727272" type="appl" />
         <tli id="T1438" time="702.2390909090909" type="appl" />
         <tli id="T1439" time="702.5554545454545" type="appl" />
         <tli id="T1440" time="702.8718181818182" type="appl" />
         <tli id="T1441" time="703.1881818181818" type="appl" />
         <tli id="T1442" time="703.5045454545455" type="appl" />
         <tli id="T1443" time="703.820909090909" type="appl" />
         <tli id="T1444" time="704.1372727272727" type="appl" />
         <tli id="T1445" time="704.4536363636363" type="appl" />
         <tli id="T1446" time="704.77" type="appl" />
         <tli id="T1447" time="704.78" type="appl" />
         <tli id="T1448" time="705.1023529411765" type="appl" />
         <tli id="T1449" time="705.4247058823529" type="appl" />
         <tli id="T1450" time="705.7470588235294" type="appl" />
         <tli id="T1451" time="706.0694117647058" type="appl" />
         <tli id="T1452" time="706.3917647058823" type="appl" />
         <tli id="T1453" time="706.7141176470589" type="appl" />
         <tli id="T1454" time="707.0364705882353" type="appl" />
         <tli id="T1455" time="707.3588235294118" type="appl" />
         <tli id="T1456" time="707.6811764705882" type="appl" />
         <tli id="T1457" time="708.0035294117647" type="appl" />
         <tli id="T1458" time="708.3258823529411" type="appl" />
         <tli id="T1459" time="708.6482352941176" type="appl" />
         <tli id="T1460" time="708.9705882352941" type="appl" />
         <tli id="T1461" time="709.2929411764705" type="appl" />
         <tli id="T1462" time="709.6152941176471" type="appl" />
         <tli id="T1463" time="709.9376470588235" type="appl" />
         <tli id="T1464" time="710.26" type="appl" />
         <tli id="T1465" time="713.32" type="appl" />
         <tli id="T1466" time="714.53817881174" />
         <tli id="T1467" time="714.73" type="appl" />
         <tli id="T1468" time="715.37" type="appl" />
         <tli id="T1469" time="716.01" type="appl" />
         <tli id="T1470" time="716.65" type="appl" />
         <tli id="T1471" time="716.82" type="appl" />
         <tli id="T1472" time="717.2161538461539" type="appl" />
         <tli id="T1473" time="717.6123076923077" type="appl" />
         <tli id="T1474" time="718.0084615384616" type="appl" />
         <tli id="T1475" time="718.4046153846155" type="appl" />
         <tli id="T1476" time="718.8007692307692" type="appl" />
         <tli id="T1477" time="719.1969230769231" type="appl" />
         <tli id="T1478" time="719.593076923077" type="appl" />
         <tli id="T1479" time="719.9892307692309" type="appl" />
         <tli id="T1480" time="720.3853846153846" type="appl" />
         <tli id="T1481" time="720.59" type="appl" />
         <tli id="T1482" time="720.7815384615385" type="appl" />
         <tli id="T1483" time="721.1776923076924" type="appl" />
         <tli id="T1484" time="721.25" type="appl" />
         <tli id="T1485" time="721.5738461538461" type="appl" />
         <tli id="T1486" time="721.8114936071197" />
         <tli id="T1487" time="721.938159950944" />
         <tli id="T1488" time="722.0648262947682" />
         <tli id="T1489" time="722.3514922307913" />
         <tli id="T1490" time="722.5248251223403" />
         <tli id="T1491" time="722.8248243577135" />
         <tli id="T1492" time="723.7114887644829" />
         <tli id="T1493" time="724.2248207894547" />
         <tli id="T1494" time="724.9181523556505" />
         <tli id="T1495" time="725.0914852471994" />
         <tli id="T1496" time="725.3514845845228" />
         <tli id="T1497" time="726.1127272727273" type="appl" />
         <tli id="T1498" time="726.5663636363636" type="appl" />
         <tli id="T1499" time="727.1200217436079" />
         <tli id="T1500" time="727.1466883423077" />
         <tli id="T1501" time="727.8366865836659" />
         <tli id="T1502" time="727.9" type="appl" />
         <tli id="T1503" time="728.1614285714286" type="appl" />
         <tli id="T1504" time="728.3111111111111" type="appl" />
         <tli id="T1505" time="728.4528571428572" type="appl" />
         <tli id="T1506" time="728.7222222222222" type="appl" />
         <tli id="T1507" time="728.7442857142858" type="appl" />
         <tli id="T1508" time="729.0357142857142" type="appl" />
         <tli id="T1509" time="729.1333333333333" type="appl" />
         <tli id="T1510" time="729.3271428571428" type="appl" />
         <tli id="T1511" time="729.5444444444445" type="appl" />
         <tli id="T1512" time="729.6185714285714" type="appl" />
         <tli id="T1513" time="729.91" type="appl" />
         <tli id="T1514" time="729.9555555555555" type="appl" />
         <tli id="T1515" time="730.3666666666667" type="appl" />
         <tli id="T1516" time="730.7777777777778" type="appl" />
         <tli id="T1517" time="731.1888888888889" type="appl" />
         <tli id="T1518" time="731.6" type="appl" />
         <tli id="T1519" time="731.64" type="appl" />
         <tli id="T1520" time="732.345" type="appl" />
         <tli id="T1521" time="733.05" type="appl" />
         <tli id="T1522" time="733.21" type="appl" />
         <tli id="T1523" time="733.69875" type="appl" />
         <tli id="T1524" time="734.1875" type="appl" />
         <tli id="T1525" time="734.67625" type="appl" />
         <tli id="T1526" time="735.165" type="appl" />
         <tli id="T1527" time="735.6537500000001" type="appl" />
         <tli id="T1528" time="736.1425" type="appl" />
         <tli id="T1529" time="736.63125" type="appl" />
         <tli id="T1530" time="737.12" type="appl" />
         <tli id="T1531" time="737.19" type="appl" />
         <tli id="T1532" time="737.965" type="appl" />
         <tli id="T1533" time="738.74" type="appl" />
         <tli id="T1534" time="738.97" type="appl" />
         <tli id="T1535" time="739.4857142857143" type="appl" />
         <tli id="T1536" time="740.0014285714286" type="appl" />
         <tli id="T1537" time="740.5171428571429" type="appl" />
         <tli id="T1538" time="741.0328571428572" type="appl" />
         <tli id="T1539" time="741.258110708974" />
         <tli id="T1540" time="741.8181092816706" />
         <tli id="T1541" time="742.58" type="appl" />
         <tli id="T1542" time="742.99" type="appl" />
         <tli id="T1543" time="743.6" type="appl" />
         <tli id="T1544" time="744.21" type="appl" />
         <tli id="T1545" time="744.82" type="appl" />
         <tli id="T1546" time="745.31" type="appl" />
         <tli id="T1547" time="745.5171428571429" type="appl" />
         <tli id="T1548" time="745.885" type="appl" />
         <tli id="T1549" time="746.2142857142858" type="appl" />
         <tli id="T1550" time="748.0066872584538" />
         <tli id="T1551" time="748.1514264728812" />
         <tli id="T1552" time="748.2818948903482" />
         <tli id="T1553" time="748.4523632058649" />
         <tli id="T1554" time="749.0028571428572" type="appl" />
         <tli id="T1555" time="749.44001693857" />
         <tli id="T1556" time="749.81" type="appl" />
         <tli id="T1557" time="750.3322222222222" type="appl" />
         <tli id="T1558" time="750.8544444444444" type="appl" />
         <tli id="T1559" time="751.3766666666667" type="appl" />
         <tli id="T1560" time="751.8988888888889" type="appl" />
         <tli id="T1561" time="752.421111111111" type="appl" />
         <tli id="T1562" time="752.9433333333333" type="appl" />
         <tli id="T1563" time="753.4655555555555" type="appl" />
         <tli id="T1564" time="753.9877777777778" type="appl" />
         <tli id="T1565" time="754.6433370098752" />
         <tli id="T1566" time="754.65" type="appl" />
         <tli id="T1567" time="755.1772727272727" type="appl" />
         <tli id="T1568" time="755.7045454545455" type="appl" />
         <tli id="T1569" time="756.2318181818182" type="appl" />
         <tli id="T1570" time="756.7590909090909" type="appl" />
         <tli id="T1571" time="757.2863636363636" type="appl" />
         <tli id="T1572" time="757.8136363636364" type="appl" />
         <tli id="T1573" time="758.3409090909091" type="appl" />
         <tli id="T1574" time="758.8681818181818" type="appl" />
         <tli id="T1575" time="759.3954545454545" type="appl" />
         <tli id="T1576" time="759.9227272727273" type="appl" />
         <tli id="T1577" time="760.45" type="appl" />
         <tli id="T1578" time="760.47" type="appl" />
         <tli id="T1579" time="760.9616666666667" type="appl" />
         <tli id="T1580" time="761.4533333333334" type="appl" />
         <tli id="T1581" time="761.9449999999999" type="appl" />
         <tli id="T1582" time="762.4366666666666" type="appl" />
         <tli id="T1583" time="762.9283333333333" type="appl" />
         <tli id="T1584" time="763.42" type="appl" />
         <tli id="T1585" time="763.45" type="appl" />
         <tli id="T1586" time="763.988" type="appl" />
         <tli id="T1587" time="764.5260000000001" type="appl" />
         <tli id="T1588" time="765.064" type="appl" />
         <tli id="T1589" time="765.602" type="appl" />
         <tli id="T1590" time="766.14" type="appl" />
         <tli id="T1591" time="766.29" type="appl" />
         <tli id="T1592" time="766.8597142857143" type="appl" />
         <tli id="T1593" time="767.4294285714285" type="appl" />
         <tli id="T1594" time="767.9991428571428" type="appl" />
         <tli id="T1595" time="768.5688571428572" type="appl" />
         <tli id="T1596" time="769.1385714285715" type="appl" />
         <tli id="T1597" time="769.7082857142857" type="appl" />
         <tli id="T1598" time="770.278" type="appl" />
         <tli id="T1599" time="770.28" type="appl" />
         <tli id="T1600" time="770.8625" type="appl" />
         <tli id="T1601" time="771.4449999999999" type="appl" />
         <tli id="T1602" time="772.0275" type="appl" />
         <tli id="T1603" time="772.6566765148139" />
         <tli id="T1604" time="772.6600098396514" />
         <tli id="T1605" time="773.0200089220991" />
         <tli id="T1606" time="773.0266755717741" />
         <tli id="T1607" time="773.3625" type="appl" />
         <tli id="T1608" time="773.845" type="appl" />
         <tli id="T1609" time="774.3275" type="appl" />
         <tli id="T1610" time="774.81" type="appl" />
         <tli id="T1611" time="774.9" type="appl" />
         <tli id="T1612" time="775.3495" type="appl" />
         <tli id="T1613" time="775.799" type="appl" />
         <tli id="T1614" time="776.2484999999999" type="appl" />
         <tli id="T1615" time="776.698" type="appl" />
         <tli id="T1616" time="776.72" type="appl" />
         <tli id="T1617" time="777.28" type="appl" />
         <tli id="T1618" time="777.84" type="appl" />
         <tli id="T1619" time="778.4000000000001" type="appl" />
         <tli id="T1620" time="778.96" type="appl" />
         <tli id="T1621" time="779.52" type="appl" />
         <tli id="T1622" time="780.08" type="appl" />
         <tli id="T1623" time="780.6400000000001" type="appl" />
         <tli id="T1624" time="781.2" type="appl" />
         <tli id="T1625" time="781.28" type="appl" />
         <tli id="T1626" time="781.8149999999999" type="appl" />
         <tli id="T1627" time="782.3499999999999" type="appl" />
         <tli id="T1628" time="782.885" type="appl" />
         <tli id="T1629" time="783.42" type="appl" />
         <tli id="T1630" time="783.9549999999999" type="appl" />
         <tli id="T1631" time="784.3766466433912" />
         <tli id="T1632" time="784.7246665897043" />
         <tli id="T1633" time="784.9179994302781" />
         <tli id="T1634" time="786.75" type="appl" />
         <tli id="T1635" time="787.4542857142857" type="appl" />
         <tli id="T1636" time="788.1585714285715" type="appl" />
         <tli id="T1637" time="788.8628571428571" type="appl" />
         <tli id="T1638" time="789.5671428571428" type="appl" />
         <tli id="T1639" time="790.2714285714285" type="appl" />
         <tli id="T1640" time="790.9757142857143" type="appl" />
         <tli id="T1641" time="791.8400130377079" />
         <tli id="T1642" time="791.8433463625454" />
         <tli id="T1643" time="792.3533450626798" />
         <tli id="T1644" time="792.63" type="appl" />
         <tli id="T1645" time="793.1471428571429" type="appl" />
         <tli id="T1646" time="793.6642857142857" type="appl" />
         <tli id="T1647" time="794.1814285714286" type="appl" />
         <tli id="T1648" time="794.6985714285714" type="appl" />
         <tli id="T1649" time="795.2157142857143" type="appl" />
         <tli id="T1650" time="795.7328571428571" type="appl" />
         <tli id="T1651" time="796.25" type="appl" />
         <tli id="T1652" time="796.43" type="appl" />
         <tli id="T1653" time="797.005" type="appl" />
         <tli id="T1654" time="797.5799999999999" type="appl" />
         <tli id="T1655" time="798.155" type="appl" />
         <tli id="T1656" time="798.73" type="appl" />
         <tli id="T1657" time="799.305" type="appl" />
         <tli id="T1658" time="799.88" type="appl" />
         <tli id="T1659" time="799.93" type="appl" />
         <tli id="T1660" time="800.6" type="appl" />
         <tli id="T1661" time="801.26" type="appl" />
         <tli id="T1662" time="801.53" type="appl" />
         <tli id="T1663" time="802.1011111111111" type="appl" />
         <tli id="T1664" time="802.6722222222222" type="appl" />
         <tli id="T1665" time="803.2433333333333" type="appl" />
         <tli id="T1666" time="803.8144444444445" type="appl" />
         <tli id="T1667" time="804.3855555555555" type="appl" />
         <tli id="T1668" time="804.9566666666666" type="appl" />
         <tli id="T1669" time="805.5277777777777" type="appl" />
         <tli id="T1670" time="806.0988888888888" type="appl" />
         <tli id="T1671" time="806.67" type="appl" />
         <tli id="T1672" time="806.98" type="appl" />
         <tli id="T1673" time="807.32875" type="appl" />
         <tli id="T1674" time="807.6775" type="appl" />
         <tli id="T1675" time="808.02625" type="appl" />
         <tli id="T1676" time="808.375" type="appl" />
         <tli id="T1677" time="808.72375" type="appl" />
         <tli id="T1678" time="809.0725" type="appl" />
         <tli id="T1679" time="809.42125" type="appl" />
         <tli id="T1680" time="809.75" type="appl" />
         <tli id="T1681" time="809.8433525681342" />
         <tli id="T1682" time="810.1182352941177" type="appl" />
         <tli id="T1683" time="810.4864705882353" type="appl" />
         <tli id="T1684" time="810.854705882353" type="appl" />
         <tli id="T1685" time="811.2229411764706" type="appl" />
         <tli id="T1686" time="811.5911764705883" type="appl" />
         <tli id="T1687" time="811.9594117647059" type="appl" />
         <tli id="T1688" time="812.3276470588236" type="appl" />
         <tli id="T1689" time="812.6958823529412" type="appl" />
         <tli id="T1690" time="813.0641176470588" type="appl" />
         <tli id="T1691" time="813.4323529411764" type="appl" />
         <tli id="T1692" time="813.8005882352941" type="appl" />
         <tli id="T1693" time="814.1688235294117" type="appl" />
         <tli id="T1694" time="814.5370588235294" type="appl" />
         <tli id="T1695" time="814.905294117647" type="appl" />
         <tli id="T1696" time="815.2735294117647" type="appl" />
         <tli id="T1697" time="815.6417647058823" type="appl" />
         <tli id="T1698" time="816.01" type="appl" />
         <tli id="T1699" time="816.6933351091542" />
         <tli id="T1700" time="817.0009" type="appl" />
         <tli id="T1701" time="817.4418" type="appl" />
         <tli id="T1702" time="817.8827" type="appl" />
         <tli id="T1703" time="818.3235999999999" type="appl" />
         <tli id="T1704" time="818.7645" type="appl" />
         <tli id="T1705" time="819.2054" type="appl" />
         <tli id="T1706" time="819.6463" type="appl" />
         <tli id="T1707" time="820.0872" type="appl" />
         <tli id="T1708" time="820.5281" type="appl" />
         <tli id="T1709" time="820.969" type="appl" />
         <tli id="T1710" time="821.03" type="appl" />
         <tli id="T1711" time="821.4399999999999" type="appl" />
         <tli id="T1712" time="821.85" type="appl" />
         <tli id="T1713" time="822.26" type="appl" />
         <tli id="T1714" time="822.67" type="appl" />
         <tli id="T1715" time="823.08" type="appl" />
         <tli id="T1716" time="823.49" type="appl" />
         <tli id="T1717" time="824.0666496496585" />
         <tli id="T1718" time="824.32375" type="appl" />
         <tli id="T1719" time="824.6275" type="appl" />
         <tli id="T1720" time="824.93125" type="appl" />
         <tli id="T1721" time="825.235" type="appl" />
         <tli id="T1722" time="825.53875" type="appl" />
         <tli id="T1723" time="825.8425" type="appl" />
         <tli id="T1724" time="826.14625" type="appl" />
         <tli id="T1725" time="826.45" type="appl" />
         <tli id="T1726" time="826.855" type="appl" />
         <tli id="T1727" time="827.26" type="appl" />
         <tli id="T1728" time="827.665" type="appl" />
         <tli id="T1729" time="828.07" type="appl" />
         <tli id="T1730" time="828.475" type="appl" />
         <tli id="T1731" time="828.88" type="appl" />
         <tli id="T1732" time="829.285" type="appl" />
         <tli id="T1733" time="829.6899999999999" type="appl" />
         <tli id="T1734" time="830.095" type="appl" />
         <tli id="T1735" time="830.5" type="appl" />
         <tli id="T1736" time="830.905" type="appl" />
         <tli id="T1737" time="831.31" type="appl" />
         <tli id="T0" time="833.737" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T889" start="T887">
            <tli id="T887.tx-KA.1" />
            <tli id="T887.tx-KA.2" />
            <tli id="T887.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T1541" start="T1539">
            <tli id="T1539.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T1689" start="T1672">
            <tli id="T1672.tx-KA.1" />
            <tli id="T1672.tx-KA.2" />
            <tli id="T1672.tx-KA.3" />
            <tli id="T1672.tx-KA.4" />
            <tli id="T1672.tx-KA.5" />
            <tli id="T1672.tx-KA.6" />
            <tli id="T1672.tx-KA.7" />
            <tli id="T1672.tx-KA.8" />
            <tli id="T1672.tx-KA.9" />
            <tli id="T1672.tx-KA.10" />
            <tli id="T1672.tx-KA.11" />
            <tli id="T1672.tx-KA.12" />
            <tli id="T1672.tx-KA.13" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">На</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">каком</ts>
                  <nts id="Seg_8" n="HIAT:ip">?</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_11" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">На</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">любом</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_19" n="sc" s="T6">
               <ts e="T14" id="Seg_21" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">Или</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_26" n="HIAT:w" s="T7">сначала</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_29" n="HIAT:w" s="T9">можешь</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_32" n="HIAT:w" s="T10">по-русски</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_35" n="HIAT:w" s="T12">рассказать</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_39" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_41" n="HIAT:w" s="T14">Расскажи</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_44" n="HIAT:w" s="T15">сначала</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_47" n="HIAT:w" s="T17">по-русски</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_51" n="HIAT:w" s="T18">а</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_54" n="HIAT:w" s="T19">потом</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_57" n="HIAT:w" s="T20">уже</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_60" n="HIAT:w" s="T21">по-камасински</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T28" id="Seg_63" n="sc" s="T23">
               <ts e="T28" id="Seg_65" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_67" n="HIAT:w" s="T23">А</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_70" n="HIAT:w" s="T24">я</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_73" n="HIAT:w" s="T25">послушаю</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_77" n="HIAT:w" s="T26">мне</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_80" n="HIAT:w" s="T27">интересно</ts>
                  <nts id="Seg_81" n="HIAT:ip">!</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T34" id="Seg_83" n="sc" s="T32">
               <ts e="T34" id="Seg_85" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_87" n="HIAT:w" s="T32">Идет</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_91" n="HIAT:w" s="T33">идет</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T265" id="Seg_94" n="sc" s="T264">
               <ts e="T265" id="Seg_96" n="HIAT:u" s="T264">
                  <nts id="Seg_97" n="HIAT:ip">(</nts>
                  <nts id="Seg_98" n="HIAT:ip">(</nts>
                  <ats e="T265" id="Seg_99" n="HIAT:non-pho" s="T264">…</ats>
                  <nts id="Seg_100" n="HIAT:ip">)</nts>
                  <nts id="Seg_101" n="HIAT:ip">)</nts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T282" id="Seg_104" n="sc" s="T270">
               <ts e="T278" id="Seg_106" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_108" n="HIAT:w" s="T270">Нет</ts>
                  <nts id="Seg_109" n="HIAT:ip">,</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_112" n="HIAT:w" s="T271">насчет</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_115" n="HIAT:w" s="T272">счетовода</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_118" n="HIAT:w" s="T273">ты</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_120" n="HIAT:ip">(</nts>
                  <ts e="T275" id="Seg_122" n="HIAT:w" s="T274">с-</ts>
                  <nts id="Seg_123" n="HIAT:ip">)</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_126" n="HIAT:w" s="T275">вообще</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_129" n="HIAT:w" s="T276">не</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_132" n="HIAT:w" s="T277">рассказывала</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_136" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_138" n="HIAT:w" s="T278">Как</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_141" n="HIAT:w" s="T279">это</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_144" n="HIAT:w" s="T280">было</ts>
                  <nts id="Seg_145" n="HIAT:ip">,</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_148" n="HIAT:w" s="T281">значит</ts>
                  <nts id="Seg_149" n="HIAT:ip">?</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T285" id="Seg_151" n="sc" s="T283">
               <ts e="T285" id="Seg_153" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_155" n="HIAT:w" s="T283">Пока</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_158" n="HIAT:w" s="T284">расскажи</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T293" id="Seg_161" n="sc" s="T289">
               <ts e="T293" id="Seg_163" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_165" n="HIAT:w" s="T289">Да</ts>
                  <nts id="Seg_166" n="HIAT:ip">,</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_169" n="HIAT:w" s="T290">насчет</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_172" n="HIAT:w" s="T291">счетовода</ts>
                  <nts id="Seg_173" n="HIAT:ip">?</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_175" n="sc" s="T296">
               <ts e="T299" id="Seg_177" n="HIAT:u" s="T296">
                  <ts e="T299" id="Seg_179" n="HIAT:w" s="T296">Ага</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T303" id="Seg_182" n="sc" s="T300">
               <ts e="T303" id="Seg_184" n="HIAT:u" s="T300">
                  <ts e="T303" id="Seg_186" n="HIAT:w" s="T300">Ага</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T322" id="Seg_189" n="sc" s="T311">
               <ts e="T322" id="Seg_191" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_193" n="HIAT:w" s="T311">Ну</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_196" n="HIAT:w" s="T312">да</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_200" n="HIAT:w" s="T313">это</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_203" n="HIAT:w" s="T314">насчет</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_206" n="HIAT:w" s="T315">Паши</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_209" n="HIAT:w" s="T316">и</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_212" n="HIAT:w" s="T317">Шуры</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_216" n="HIAT:w" s="T318">а</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_219" n="HIAT:w" s="T319">вот</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_222" n="HIAT:w" s="T320">счетовод</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_225" n="HIAT:w" s="T321">тот</ts>
                  <nts id="Seg_226" n="HIAT:ip">?</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T329" id="Seg_228" n="sc" s="T324">
               <ts e="T329" id="Seg_230" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_232" n="HIAT:w" s="T324">У</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_235" n="HIAT:w" s="T325">которого</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_237" n="HIAT:ip">(</nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ats e="T329" id="Seg_239" n="HIAT:non-pho" s="T327">…</ats>
                  <nts id="Seg_240" n="HIAT:ip">)</nts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip">.</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T340" id="Seg_244" n="sc" s="T331">
               <ts e="T340" id="Seg_246" n="HIAT:u" s="T331">
                  <ts e="T333" id="Seg_248" n="HIAT:w" s="T331">Ага</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_252" n="HIAT:w" s="T333">это</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_255" n="HIAT:w" s="T335">тоже</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_258" n="HIAT:w" s="T337">есть</ts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_262" n="HIAT:w" s="T339">да</ts>
                  <nts id="Seg_263" n="HIAT:ip">?</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_265" n="sc" s="T341">
               <ts e="T347" id="Seg_267" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_269" n="HIAT:w" s="T341">А</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_273" n="HIAT:w" s="T342">а</ts>
                  <nts id="Seg_274" n="HIAT:ip">,</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_277" n="HIAT:w" s="T343">она</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_280" n="HIAT:w" s="T344">с</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ts e="T346" id="Seg_284" n="HIAT:w" s="T345">ней</ts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_289" n="HIAT:w" s="T346">ага</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_292" n="sc" s="T349">
               <ts e="T353" id="Seg_294" n="HIAT:u" s="T349">
                  <ts e="T351" id="Seg_296" n="HIAT:w" s="T349">Все</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_300" n="HIAT:w" s="T351">ясно</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T376" id="Seg_303" n="sc" s="T364">
               <ts e="T376" id="Seg_305" n="HIAT:u" s="T364">
                  <ts e="T366" id="Seg_307" n="HIAT:w" s="T364">Все</ts>
                  <nts id="Seg_308" n="HIAT:ip">,</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_311" n="HIAT:w" s="T366">слушает</ts>
                  <nts id="Seg_312" n="HIAT:ip">,</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_315" n="HIAT:w" s="T368">слушает</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_319" n="HIAT:w" s="T370">говори</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_322" n="HIAT:w" s="T372">что</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_325" n="HIAT:w" s="T374">хочешь</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T384" id="Seg_328" n="sc" s="T381">
               <ts e="T384" id="Seg_330" n="HIAT:u" s="T381">
                  <ts e="T384" id="Seg_332" n="HIAT:w" s="T381">Нет</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T396" id="Seg_335" n="sc" s="T385">
               <ts e="T392" id="Seg_337" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_339" n="HIAT:w" s="T385">Да</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_343" n="HIAT:w" s="T386">значит</ts>
                  <nts id="Seg_344" n="HIAT:ip">,</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_347" n="HIAT:w" s="T387">насчет</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_350" n="HIAT:w" s="T388">счетовода</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_353" n="HIAT:w" s="T389">по-русски</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_356" n="HIAT:w" s="T390">не</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_359" n="HIAT:w" s="T391">рассказала</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_363" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_365" n="HIAT:w" s="T392">Насчет</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_368" n="HIAT:w" s="T393">счетовода</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_371" n="HIAT:w" s="T394">можно</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_374" n="HIAT:w" s="T395">сказать</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T407" id="Seg_377" n="sc" s="T397">
               <ts e="T407" id="Seg_379" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_381" n="HIAT:w" s="T397">Вот</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_385" n="HIAT:w" s="T398">как</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_388" n="HIAT:w" s="T399">руки</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_391" n="HIAT:w" s="T400">отморозил</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_395" n="HIAT:w" s="T401">это</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_398" n="HIAT:w" s="T402">пока</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_401" n="HIAT:w" s="T403">ты</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_404" n="HIAT:w" s="T404">не</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_407" n="HIAT:w" s="T405">говорила</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_410" n="HIAT:w" s="T406">по-русски</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T420" id="Seg_413" n="sc" s="T408">
               <ts e="T420" id="Seg_415" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_417" n="HIAT:w" s="T408">А</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_420" n="HIAT:w" s="T409">рассказала</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_424" n="HIAT:w" s="T410">как</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_427" n="HIAT:w" s="T411">разошлись</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_430" n="HIAT:w" s="T412">и</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_433" n="HIAT:w" s="T413">судили</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_437" n="HIAT:w" s="T414">вот</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_440" n="HIAT:w" s="T415">это</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_443" n="HIAT:w" s="T416">ты</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_446" n="HIAT:w" s="T417">уже</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_449" n="HIAT:w" s="T418">по-русски</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_452" n="HIAT:w" s="T419">говорила</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_455" n="sc" s="T423">
               <ts e="T424" id="Seg_457" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_459" n="HIAT:w" s="T423">Ага</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T434" id="Seg_462" n="sc" s="T433">
               <ts e="T434" id="Seg_464" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_466" n="HIAT:w" s="T433">Так</ts>
                  <nts id="Seg_467" n="HIAT:ip">…</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_469" n="sc" s="T437">
               <ts e="T446" id="Seg_471" n="HIAT:u" s="T437">
                  <ts e="T439" id="Seg_473" n="HIAT:w" s="T437">По-русски</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_477" n="HIAT:w" s="T439">только</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_480" n="HIAT:w" s="T440">ты</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_483" n="HIAT:w" s="T441">вот</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_486" n="HIAT:w" s="T442">эту</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_488" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_490" n="HIAT:w" s="T444">часть</ts>
                  <nts id="Seg_491" n="HIAT:ip">)</nts>
                  <nts id="Seg_492" n="HIAT:ip">…</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T457" id="Seg_494" n="sc" s="T456">
               <ts e="T457" id="Seg_496" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_498" n="HIAT:w" s="T456">Ага</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T477" id="Seg_501" n="sc" s="T466">
               <ts e="T477" id="Seg_503" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_505" n="HIAT:w" s="T466">Ну</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_508" n="HIAT:w" s="T467">хорошо</ts>
                  <nts id="Seg_509" n="HIAT:ip">,</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_512" n="HIAT:w" s="T469">все</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_515" n="HIAT:w" s="T471">равно</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_518" n="HIAT:w" s="T472">будет</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_521" n="HIAT:w" s="T473">понятно</ts>
                  <nts id="Seg_522" n="HIAT:ip">,</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_525" n="HIAT:w" s="T474">люди</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_528" n="HIAT:w" s="T475">умные</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_530" n="HIAT:ip">—</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_533" n="HIAT:w" s="T476">поймем</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T489" id="Seg_536" n="sc" s="T478">
               <ts e="T489" id="Seg_538" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_540" n="HIAT:w" s="T478">Ну</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_544" n="HIAT:w" s="T479">что-нибудь</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_547" n="HIAT:w" s="T480">другое</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_550" n="HIAT:w" s="T481">расскажи</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_553" n="HIAT:w" s="T482">о</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_556" n="HIAT:w" s="T483">деревенской</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_559" n="HIAT:w" s="T484">жизни</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_563" n="HIAT:w" s="T485">что</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_566" n="HIAT:w" s="T486">ж</ts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_570" n="HIAT:w" s="T487">все</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_573" n="HIAT:w" s="T488">равно</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T496" id="Seg_576" n="sc" s="T490">
               <ts e="T496" id="Seg_578" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_580" n="HIAT:w" s="T490">Что</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_583" n="HIAT:w" s="T491">у</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_586" n="HIAT:w" s="T492">вас</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_589" n="HIAT:w" s="T493">там</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_592" n="HIAT:w" s="T494">еще</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_595" n="HIAT:w" s="T495">нового</ts>
                  <nts id="Seg_596" n="HIAT:ip">?</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T526" id="Seg_598" n="sc" s="T500">
               <ts e="T507" id="Seg_600" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_602" n="HIAT:w" s="T500">Кто</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_605" n="HIAT:w" s="T501">у</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_608" n="HIAT:w" s="T503">вас</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_611" n="HIAT:w" s="T504">там</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_614" n="HIAT:w" s="T505">начальствует</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_617" n="HIAT:w" s="T506">сейчас</ts>
                  <nts id="Seg_618" n="HIAT:ip">?</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_621" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_623" n="HIAT:w" s="T507">Я</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_626" n="HIAT:w" s="T508">помню</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_629" n="HIAT:w" s="T509">вот</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_632" n="HIAT:w" s="T510">председателя</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_635" n="HIAT:w" s="T511">сельсовета</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_638" n="HIAT:w" s="T512">там</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_641" n="HIAT:w" s="T513">мы</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_644" n="HIAT:w" s="T514">какие-то</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_647" n="HIAT:w" s="T515">печати</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_650" n="HIAT:w" s="T516">доставали</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_653" n="HIAT:w" s="T517">у</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_655" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_657" n="HIAT:w" s="T518">нее=</ts>
                  <nts id="Seg_658" n="HIAT:ip">)</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_661" n="HIAT:w" s="T519">его</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_664" n="HIAT:w" s="T521">и</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_667" n="HIAT:w" s="T522">все</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_670" n="HIAT:w" s="T524">такое</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T569" id="Seg_673" n="sc" s="T541">
               <ts e="T557" id="Seg_675" n="HIAT:u" s="T541">
                  <ts e="T543" id="Seg_677" n="HIAT:w" s="T541">Как</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_681" n="HIAT:w" s="T543">как</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_685" n="HIAT:w" s="T545">я</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_688" n="HIAT:w" s="T546">знаю</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_691" n="HIAT:w" s="T547">его</ts>
                  <nts id="Seg_692" n="HIAT:ip">,</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_695" n="HIAT:w" s="T548">знаю</ts>
                  <nts id="Seg_696" n="HIAT:ip">,</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_699" n="HIAT:w" s="T551">что</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_702" n="HIAT:w" s="T553">он</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_705" n="HIAT:w" s="T555">был</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T569" id="Seg_709" n="HIAT:u" s="T557">
                  <ts e="T560" id="Seg_711" n="HIAT:w" s="T557">Нет</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_715" n="HIAT:w" s="T560">нет</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_719" n="HIAT:w" s="T562">нет</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_723" n="HIAT:w" s="T563">расскажи</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_726" n="HIAT:w" s="T565">как</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_729" n="HIAT:w" s="T567">раз</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T575" id="Seg_732" n="sc" s="T573">
               <ts e="T575" id="Seg_734" n="HIAT:u" s="T573">
                  <ts e="T575" id="Seg_736" n="HIAT:w" s="T573">А-а-а</ts>
                  <nts id="Seg_737" n="HIAT:ip">…</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T590" id="Seg_739" n="sc" s="T586">
               <ts e="T590" id="Seg_741" n="HIAT:u" s="T586">
                  <ts e="T588" id="Seg_743" n="HIAT:w" s="T586">Добрый</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_746" n="HIAT:w" s="T588">был</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_749" n="HIAT:w" s="T589">человек</ts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T597" id="Seg_752" n="sc" s="T591">
               <ts e="T597" id="Seg_754" n="HIAT:u" s="T591">
                  <ts e="T593" id="Seg_756" n="HIAT:w" s="T591">Вот</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_759" n="HIAT:w" s="T593">расскажи</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_763" n="HIAT:w" s="T595">пожалуйста</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T853" id="Seg_766" n="sc" s="T839">
               <ts e="T853" id="Seg_768" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_770" n="HIAT:w" s="T839">Так</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_773" n="HIAT:w" s="T840">я</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_776" n="HIAT:w" s="T841">уж</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_779" n="HIAT:w" s="T842">не</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_782" n="HIAT:w" s="T843">помню</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_785" n="HIAT:w" s="T844">сейчас</ts>
                  <nts id="Seg_786" n="HIAT:ip">,</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_789" n="HIAT:w" s="T846">знал</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_792" n="HIAT:w" s="T847">я</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_795" n="HIAT:w" s="T849">его</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_798" n="HIAT:w" s="T850">или</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_801" n="HIAT:w" s="T852">нет</ts>
                  <nts id="Seg_802" n="HIAT:ip">.</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T857" id="Seg_804" n="sc" s="T855">
               <ts e="T857" id="Seg_806" n="HIAT:u" s="T855">
                  <ts e="T857" id="Seg_808" n="HIAT:w" s="T855">Ага</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T889" id="Seg_811" n="sc" s="T887">
               <ts e="T889" id="Seg_813" n="HIAT:u" s="T887">
                  <ts e="T887.tx-KA.1" id="Seg_815" n="HIAT:w" s="T887">Mm</ts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887.tx-KA.2" id="Seg_819" n="HIAT:w" s="T887.tx-KA.1">mm</ts>
                  <nts id="Seg_820" n="HIAT:ip">,</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887.tx-KA.3" id="Seg_823" n="HIAT:w" s="T887.tx-KA.2">mm</ts>
                  <nts id="Seg_824" n="HIAT:ip">,</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_827" n="HIAT:w" s="T887.tx-KA.3">mm</ts>
                  <nts id="Seg_828" n="HIAT:ip">.</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T907" id="Seg_830" n="sc" s="T906">
               <ts e="T907" id="Seg_832" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_834" n="HIAT:w" s="T906">Так</ts>
                  <nts id="Seg_835" n="HIAT:ip">.</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T911" id="Seg_837" n="sc" s="T908">
               <ts e="T911" id="Seg_839" n="HIAT:u" s="T908">
                  <nts id="Seg_840" n="HIAT:ip">(</nts>
                  <nts id="Seg_841" n="HIAT:ip">(</nts>
                  <ats e="T911" id="Seg_842" n="HIAT:non-pho" s="T908">…</ats>
                  <nts id="Seg_843" n="HIAT:ip">)</nts>
                  <nts id="Seg_844" n="HIAT:ip">)</nts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T934" id="Seg_847" n="sc" s="T919">
               <ts e="T934" id="Seg_849" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_851" n="HIAT:w" s="T919">Ну</ts>
                  <nts id="Seg_852" n="HIAT:ip">,</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_855" n="HIAT:w" s="T920">это</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_858" n="HIAT:w" s="T921">мы</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_861" n="HIAT:w" s="T922">еще</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_864" n="HIAT:w" s="T923">все</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_867" n="HIAT:w" s="T924">поговорим</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_871" n="HIAT:w" s="T925">а</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_874" n="HIAT:w" s="T926">может</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_877" n="HIAT:w" s="T927">ты</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_880" n="HIAT:w" s="T928">пока</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_883" n="HIAT:w" s="T930">вот</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_886" n="HIAT:w" s="T931">насчет</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_889" n="HIAT:w" s="T932">Загороднюка</ts>
                  <nts id="Seg_890" n="HIAT:ip">?</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T948" id="Seg_892" n="sc" s="T935">
               <ts e="T944" id="Seg_894" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_896" n="HIAT:w" s="T935">На</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_899" n="HIAT:w" s="T936">камасинском</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_902" n="HIAT:w" s="T937">всё</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_905" n="HIAT:w" s="T938">это</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_908" n="HIAT:w" s="T939">дело</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_912" n="HIAT:w" s="T940">как</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_915" n="HIAT:w" s="T941">с</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_918" n="HIAT:w" s="T942">ним</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_921" n="HIAT:w" s="T943">было</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_925" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_927" n="HIAT:w" s="T944">Как</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_930" n="HIAT:w" s="T945">вы</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_933" n="HIAT:w" s="T946">там</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_936" n="HIAT:w" s="T947">ехали</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T956" id="Seg_939" n="sc" s="T950">
               <ts e="T956" id="Seg_941" n="HIAT:u" s="T950">
                  <ts e="T952" id="Seg_943" n="HIAT:w" s="T950">Всё</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_946" n="HIAT:w" s="T952">слушают</ts>
                  <nts id="Seg_947" n="HIAT:ip">,</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_950" n="HIAT:w" s="T953">всё</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_953" n="HIAT:w" s="T955">записано</ts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T964" id="Seg_956" n="sc" s="T958">
               <ts e="T964" id="Seg_958" n="HIAT:u" s="T958">
                  <ts e="T960" id="Seg_960" n="HIAT:w" s="T958">Записано</ts>
                  <nts id="Seg_961" n="HIAT:ip">,</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_964" n="HIAT:w" s="T960">по-русски</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_967" n="HIAT:w" s="T961">сейчас</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_970" n="HIAT:w" s="T962">всё</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_973" n="HIAT:w" s="T963">записали</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T968" id="Seg_976" n="sc" s="T965">
               <ts e="T968" id="Seg_978" n="HIAT:u" s="T965">
                  <ts e="T966" id="Seg_980" n="HIAT:w" s="T965">Теперь</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_983" n="HIAT:w" s="T966">только</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_986" n="HIAT:w" s="T967">по-камасински</ts>
                  <nts id="Seg_987" n="HIAT:ip">.</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1072" id="Seg_989" n="sc" s="T1040">
               <ts e="T1060" id="Seg_991" n="HIAT:u" s="T1040">
                  <ts e="T1042" id="Seg_993" n="HIAT:w" s="T1040">Ты</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_996" n="HIAT:w" s="T1042">еще</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_999" n="HIAT:w" s="T1043">рассказала</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_1002" n="HIAT:w" s="T1044">насчет</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_1005" n="HIAT:w" s="T1045">того</ts>
                  <nts id="Seg_1006" n="HIAT:ip">,</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_1009" n="HIAT:w" s="T1046">как</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_1012" n="HIAT:w" s="T1047">ты</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_1015" n="HIAT:w" s="T1048">сказала</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_1018" n="HIAT:w" s="T1049">ему</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_1021" n="HIAT:w" s="T1050">что</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1023" n="HIAT:ip">"</nts>
                  <ts e="T1052" id="Seg_1025" n="HIAT:w" s="T1051">Я</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_1028" n="HIAT:w" s="T1052">тебе</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_1031" n="HIAT:w" s="T1053">как</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_1034" n="HIAT:w" s="T1054">сыну</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_1037" n="HIAT:w" s="T1055">скажу</ts>
                  <nts id="Seg_1038" n="HIAT:ip">,</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_1041" n="HIAT:w" s="T1056">что</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_1044" n="HIAT:w" s="T1057">богу</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_1047" n="HIAT:w" s="T1058">поеду</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_1050" n="HIAT:w" s="T1059">помолиться</ts>
                  <nts id="Seg_1051" n="HIAT:ip">"</nts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1067" id="Seg_1055" n="HIAT:u" s="T1060">
                  <ts e="T1062" id="Seg_1057" n="HIAT:w" s="T1060">Вот</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_1060" n="HIAT:w" s="T1062">еще</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_1063" n="HIAT:w" s="T1063">по-камасински</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_1066" n="HIAT:w" s="T1065">не</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_1069" n="HIAT:w" s="T1066">рассказала</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1072" id="Seg_1073" n="HIAT:u" s="T1067">
                  <ts e="T1068" id="Seg_1075" n="HIAT:w" s="T1067">Пожалуйста</ts>
                  <nts id="Seg_1076" n="HIAT:ip">,</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_1079" n="HIAT:w" s="T1068">пожалуйста</ts>
                  <nts id="Seg_1080" n="HIAT:ip">,</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_1083" n="HIAT:w" s="T1069">всё</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_1086" n="HIAT:w" s="T1070">слушает</ts>
                  <nts id="Seg_1087" n="HIAT:ip">,</nts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_1090" n="HIAT:w" s="T1071">пишет</ts>
                  <nts id="Seg_1091" n="HIAT:ip">.</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1117" id="Seg_1093" n="sc" s="T1101">
               <ts e="T1109" id="Seg_1095" n="HIAT:u" s="T1101">
                  <ts e="T1102" id="Seg_1097" n="HIAT:w" s="T1101">Ну</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_1100" n="HIAT:w" s="T1102">тут</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_1103" n="HIAT:w" s="T1103">мы</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_1106" n="HIAT:w" s="T1104">начали</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_1109" n="HIAT:w" s="T1105">и</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_1112" n="HIAT:w" s="T1106">о</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_1115" n="HIAT:w" s="T1107">бригадире</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_1118" n="HIAT:w" s="T1108">говорить</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1117" id="Seg_1122" n="HIAT:u" s="T1109">
                  <ts e="T1110" id="Seg_1124" n="HIAT:w" s="T1109">Нет</ts>
                  <nts id="Seg_1125" n="HIAT:ip">,</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_1128" n="HIAT:w" s="T1110">а</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_1131" n="HIAT:w" s="T1111">вот</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_1134" n="HIAT:w" s="T1112">что</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_1137" n="HIAT:w" s="T1113">мы</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_1140" n="HIAT:w" s="T1114">не</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_1143" n="HIAT:w" s="T1115">рассказали</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_1146" n="HIAT:w" s="T1116">еще</ts>
                  <nts id="Seg_1147" n="HIAT:ip">.</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1137" id="Seg_1149" n="sc" s="T1118">
               <ts e="T1130" id="Seg_1151" n="HIAT:u" s="T1118">
                  <ts e="T1119" id="Seg_1153" n="HIAT:w" s="T1118">Это</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_1156" n="HIAT:w" s="T1119">мы</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_1159" n="HIAT:w" s="T1120">только</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_1162" n="HIAT:w" s="T1121">тут</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_1165" n="HIAT:w" s="T1122">говорили</ts>
                  <nts id="Seg_1166" n="HIAT:ip">,</nts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_1169" n="HIAT:w" s="T1123">что</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1125" id="Seg_1172" n="HIAT:w" s="T1124">он</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_1175" n="HIAT:w" s="T1125">помер</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_1178" n="HIAT:w" s="T1126">два</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_1181" n="HIAT:w" s="T1127">года</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_1184" n="HIAT:w" s="T1128">тому</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_1187" n="HIAT:w" s="T1129">назад</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1137" id="Seg_1191" n="HIAT:u" s="T1130">
                  <ts e="T1131" id="Seg_1193" n="HIAT:w" s="T1130">А</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_1196" n="HIAT:w" s="T1131">тут</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_1199" n="HIAT:w" s="T1132">еще</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_1202" n="HIAT:w" s="T1134">не</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_1205" n="HIAT:w" s="T1135">рассказали</ts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1157" id="Seg_1208" n="sc" s="T1145">
               <ts e="T1149" id="Seg_1210" n="HIAT:u" s="T1145">
                  <ts e="T1147" id="Seg_1212" n="HIAT:w" s="T1145">Ага</ts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_1216" n="HIAT:w" s="T1147">ага</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1157" id="Seg_1220" n="HIAT:u" s="T1149">
                  <ts e="T1150" id="Seg_1222" n="HIAT:w" s="T1149">А</ts>
                  <nts id="Seg_1223" n="HIAT:ip">,</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_1226" n="HIAT:w" s="T1150">это</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_1229" n="HIAT:w" s="T1151">было</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1153" id="Seg_1232" n="HIAT:w" s="T1152">рассказано</ts>
                  <nts id="Seg_1233" n="HIAT:ip">,</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1154" id="Seg_1236" n="HIAT:w" s="T1153">значит</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_1240" n="HIAT:w" s="T1154">все</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_1243" n="HIAT:w" s="T1155">в</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_1246" n="HIAT:w" s="T1156">порядке</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1162" id="Seg_1249" n="sc" s="T1158">
               <ts e="T1162" id="Seg_1251" n="HIAT:u" s="T1158">
                  <ts e="T1159" id="Seg_1253" n="HIAT:w" s="T1158">Ну</ts>
                  <nts id="Seg_1254" n="HIAT:ip">,</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1160" id="Seg_1257" n="HIAT:w" s="T1159">может</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_1260" n="HIAT:w" s="T1160">насчет</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1162" id="Seg_1263" n="HIAT:w" s="T1161">бригадира</ts>
                  <nts id="Seg_1264" n="HIAT:ip">?</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1172" id="Seg_1266" n="sc" s="T1163">
               <ts e="T1166" id="Seg_1268" n="HIAT:u" s="T1163">
                  <ts e="T1164" id="Seg_1270" n="HIAT:w" s="T1163">Этого</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1165" id="Seg_1273" n="HIAT:w" s="T1164">Августа</ts>
                  <nts id="Seg_1274" n="HIAT:ip">,</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1166" id="Seg_1277" n="HIAT:w" s="T1165">немца</ts>
                  <nts id="Seg_1278" n="HIAT:ip">.</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1172" id="Seg_1281" n="HIAT:u" s="T1166">
                  <nts id="Seg_1282" n="HIAT:ip">(</nts>
                  <nts id="Seg_1283" n="HIAT:ip">(</nts>
                  <ats e="T1167" id="Seg_1284" n="HIAT:non-pho" s="T1166">NOISE</ats>
                  <nts id="Seg_1285" n="HIAT:ip">)</nts>
                  <nts id="Seg_1286" n="HIAT:ip">)</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_1289" n="HIAT:w" s="T1167">Я</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1169" id="Seg_1292" n="HIAT:w" s="T1168">его</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_1295" n="HIAT:w" s="T1169">не</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171" id="Seg_1298" n="HIAT:w" s="T1170">помню</ts>
                  <nts id="Seg_1299" n="HIAT:ip">,</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_1302" n="HIAT:w" s="T1171">кажется</ts>
                  <nts id="Seg_1303" n="HIAT:ip">.</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1176" id="Seg_1305" n="sc" s="T1173">
               <ts e="T1176" id="Seg_1307" n="HIAT:u" s="T1173">
                  <ts e="T1174" id="Seg_1309" n="HIAT:w" s="T1173">По-моему</ts>
                  <nts id="Seg_1310" n="HIAT:ip">,</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_1313" n="HIAT:w" s="T1174">не</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_1316" n="HIAT:w" s="T1175">помню</ts>
                  <nts id="Seg_1317" n="HIAT:ip">.</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1247" id="Seg_1319" n="sc" s="T1238">
               <ts e="T1247" id="Seg_1321" n="HIAT:u" s="T1238">
                  <ts e="T1239" id="Seg_1323" n="HIAT:w" s="T1238">По-русски</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_1326" n="HIAT:w" s="T1239">еще</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_1329" n="HIAT:w" s="T1240">все</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_1332" n="HIAT:w" s="T1241">это</ts>
                  <nts id="Seg_1333" n="HIAT:ip">,</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1243" id="Seg_1336" n="HIAT:w" s="T1242">ты</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_1339" n="HIAT:w" s="T1243">сейчас</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_1342" n="HIAT:w" s="T1244">много</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_1345" n="HIAT:w" s="T1245">нового</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_1348" n="HIAT:w" s="T1246">рассказала</ts>
                  <nts id="Seg_1349" n="HIAT:ip">.</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1289" id="Seg_1351" n="sc" s="T1288">
               <ts e="T1289" id="Seg_1353" n="HIAT:u" s="T1288">
                  <ts e="T1289" id="Seg_1355" n="HIAT:w" s="T1288">Так</ts>
                  <nts id="Seg_1356" n="HIAT:ip">.</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1302" id="Seg_1358" n="sc" s="T1290">
               <ts e="T1302" id="Seg_1360" n="HIAT:u" s="T1290">
                  <ts e="T1291" id="Seg_1362" n="HIAT:w" s="T1290">Вот</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1292" id="Seg_1365" n="HIAT:w" s="T1291">что</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_1368" n="HIAT:w" s="T1292">меня</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_1371" n="HIAT:w" s="T1293">интересует</ts>
                  <nts id="Seg_1372" n="HIAT:ip">,</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1295" id="Seg_1375" n="HIAT:w" s="T1294">я</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1296" id="Seg_1378" n="HIAT:w" s="T1295">помню</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1297" id="Seg_1381" n="HIAT:w" s="T1296">ведь</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1298" id="Seg_1384" n="HIAT:w" s="T1297">в</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1299" id="Seg_1387" n="HIAT:w" s="T1298">Абалакове</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_1390" n="HIAT:w" s="T1299">был</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1301" id="Seg_1393" n="HIAT:w" s="T1300">такой</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1302" id="Seg_1396" n="HIAT:w" s="T1301">магазин</ts>
                  <nts id="Seg_1397" n="HIAT:ip">.</nts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1307" id="Seg_1399" n="sc" s="T1303">
               <ts e="T1307" id="Seg_1401" n="HIAT:u" s="T1303">
                  <ts e="T1304" id="Seg_1403" n="HIAT:w" s="T1303">И</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1305" id="Seg_1406" n="HIAT:w" s="T1304">всякое</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1306" id="Seg_1409" n="HIAT:w" s="T1305">там</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_1412" n="HIAT:w" s="T1306">продавали</ts>
                  <nts id="Seg_1413" n="HIAT:ip">.</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1316" id="Seg_1415" n="sc" s="T1308">
               <ts e="T1316" id="Seg_1417" n="HIAT:u" s="T1308">
                  <ts e="T1309" id="Seg_1419" n="HIAT:w" s="T1308">Вот</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1310" id="Seg_1422" n="HIAT:w" s="T1309">что</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1311" id="Seg_1425" n="HIAT:w" s="T1310">там</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1312" id="Seg_1428" n="HIAT:w" s="T1311">сейчас</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1313" id="Seg_1431" n="HIAT:w" s="T1312">продается</ts>
                  <nts id="Seg_1432" n="HIAT:ip">,</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1314" id="Seg_1435" n="HIAT:w" s="T1313">ты</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1315" id="Seg_1438" n="HIAT:w" s="T1314">не</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1316" id="Seg_1441" n="HIAT:w" s="T1315">расскажешь</ts>
                  <nts id="Seg_1442" n="HIAT:ip">?</nts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1334" id="Seg_1444" n="sc" s="T1317">
               <ts e="T1334" id="Seg_1446" n="HIAT:u" s="T1317">
                  <ts e="T1318" id="Seg_1448" n="HIAT:w" s="T1317">Интересно</ts>
                  <nts id="Seg_1449" n="HIAT:ip">,</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_1452" n="HIAT:w" s="T1318">я</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1320" id="Seg_1455" n="HIAT:w" s="T1319">помню</ts>
                  <nts id="Seg_1456" n="HIAT:ip">,</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_1459" n="HIAT:w" s="T1320">тогда</ts>
                  <nts id="Seg_1460" n="HIAT:ip">,</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_1463" n="HIAT:w" s="T1321">когда</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_1466" n="HIAT:w" s="T1322">мы</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1324" id="Seg_1469" n="HIAT:w" s="T1323">последний</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_1472" n="HIAT:w" s="T1324">раз</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1326" id="Seg_1475" n="HIAT:w" s="T1325">там</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1327" id="Seg_1478" n="HIAT:w" s="T1326">были</ts>
                  <nts id="Seg_1479" n="HIAT:ip">,</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1328" id="Seg_1482" n="HIAT:w" s="T1327">даже</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_1485" n="HIAT:w" s="T1328">такой</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1330" id="Seg_1488" n="HIAT:w" s="T1329">спирт</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1331" id="Seg_1491" n="HIAT:w" s="T1330">продавали</ts>
                  <nts id="Seg_1492" n="HIAT:ip">,</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1332" id="Seg_1495" n="HIAT:w" s="T1331">69</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333" id="Seg_1498" n="HIAT:w" s="T1332">процентов</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1334" id="Seg_1501" n="HIAT:w" s="T1333">алкоголя</ts>
                  <nts id="Seg_1502" n="HIAT:ip">.</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1339" id="Seg_1504" n="sc" s="T1335">
               <ts e="T1339" id="Seg_1506" n="HIAT:u" s="T1335">
                  <ts e="T1336" id="Seg_1508" n="HIAT:w" s="T1335">Страшный</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_1511" n="HIAT:w" s="T1336">такой</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1338" id="Seg_1514" n="HIAT:w" s="T1337">был</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1339" id="Seg_1517" n="HIAT:w" s="T1338">напиток</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1344" id="Seg_1520" n="sc" s="T1340">
               <ts e="T1344" id="Seg_1522" n="HIAT:u" s="T1340">
                  <nts id="Seg_1523" n="HIAT:ip">(</nts>
                  <nts id="Seg_1524" n="HIAT:ip">(</nts>
                  <ats e="T1344" id="Seg_1525" n="HIAT:non-pho" s="T1340">LAUGH</ats>
                  <nts id="Seg_1526" n="HIAT:ip">)</nts>
                  <nts id="Seg_1527" n="HIAT:ip">)</nts>
                  <nts id="Seg_1528" n="HIAT:ip">.</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1346" id="Seg_1530" n="sc" s="T1345">
               <ts e="T1346" id="Seg_1532" n="HIAT:u" s="T1345">
                  <ts e="T1346" id="Seg_1534" n="HIAT:w" s="T1345">Ага</ts>
                  <nts id="Seg_1535" n="HIAT:ip">.</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1351" id="Seg_1537" n="sc" s="T1350">
               <ts e="T1351" id="Seg_1539" n="HIAT:u" s="T1350">
                  <ts e="T1351" id="Seg_1541" n="HIAT:w" s="T1350">Mhm</ts>
                  <nts id="Seg_1542" n="HIAT:ip">.</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1370" id="Seg_1544" n="sc" s="T1368">
               <ts e="T1370" id="Seg_1546" n="HIAT:u" s="T1368">
                  <ts e="T1370" id="Seg_1548" n="HIAT:w" s="T1368">Mhm</ts>
                  <nts id="Seg_1549" n="HIAT:ip">.</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1381" id="Seg_1551" n="sc" s="T1378">
               <ts e="T1381" id="Seg_1553" n="HIAT:u" s="T1378">
                  <ts e="T1379" id="Seg_1555" n="HIAT:w" s="T1378">Так</ts>
                  <nts id="Seg_1556" n="HIAT:ip">,</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1381" id="Seg_1559" n="HIAT:w" s="T1379">ну</ts>
                  <nts id="Seg_1560" n="HIAT:ip">…</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1419" id="Seg_1562" n="sc" s="T1405">
               <ts e="T1419" id="Seg_1564" n="HIAT:u" s="T1405">
                  <ts e="T1406" id="Seg_1566" n="HIAT:w" s="T1405">Нет</ts>
                  <nts id="Seg_1567" n="HIAT:ip">,</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1407" id="Seg_1570" n="HIAT:w" s="T1406">я</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1408" id="Seg_1573" n="HIAT:w" s="T1407">не</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1409" id="Seg_1576" n="HIAT:w" s="T1408">знаю</ts>
                  <nts id="Seg_1577" n="HIAT:ip">,</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_1580" n="HIAT:w" s="T1409">я</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1411" id="Seg_1583" n="HIAT:w" s="T1410">помню</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1412" id="Seg_1586" n="HIAT:w" s="T1411">когда</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1413" id="Seg_1589" n="HIAT:w" s="T1412">магазин</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1414" id="Seg_1592" n="HIAT:w" s="T1413">был</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1415" id="Seg_1595" n="HIAT:w" s="T1414">в</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1416" id="Seg_1598" n="HIAT:w" s="T1415">самом</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_1601" n="HIAT:w" s="T1416">центре</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_1604" n="HIAT:w" s="T1417">стоял</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1419" id="Seg_1607" n="HIAT:w" s="T1418">там</ts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1432" id="Seg_1610" n="sc" s="T1420">
               <ts e="T1432" id="Seg_1612" n="HIAT:u" s="T1420">
                  <ts e="T1421" id="Seg_1614" n="HIAT:w" s="T1420">На</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1422" id="Seg_1617" n="HIAT:w" s="T1421">той</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1423" id="Seg_1620" n="HIAT:w" s="T1422">же</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_1623" n="HIAT:w" s="T1423">улице</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_1626" n="HIAT:w" s="T1424">вот</ts>
                  <nts id="Seg_1627" n="HIAT:ip">,</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1426" id="Seg_1630" n="HIAT:w" s="T1425">близко</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1427" id="Seg_1633" n="HIAT:w" s="T1426">Шуры</ts>
                  <nts id="Seg_1634" n="HIAT:ip">,</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1636" n="HIAT:ip">(</nts>
                  <ts e="T1428" id="Seg_1638" n="HIAT:w" s="T1427">не</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_1641" n="HIAT:w" s="T1428">с-</ts>
                  <nts id="Seg_1642" n="HIAT:ip">)</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1430" id="Seg_1645" n="HIAT:w" s="T1429">не</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1431" id="Seg_1648" n="HIAT:w" s="T1430">очень</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1432" id="Seg_1651" n="HIAT:w" s="T1431">далеко</ts>
                  <nts id="Seg_1652" n="HIAT:ip">.</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1446" id="Seg_1654" n="sc" s="T1435">
               <ts e="T1446" id="Seg_1656" n="HIAT:u" s="T1435">
                  <ts e="T1436" id="Seg_1658" n="HIAT:w" s="T1435">До</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1437" id="Seg_1661" n="HIAT:w" s="T1436">клуба</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_1664" n="HIAT:w" s="T1437">еще</ts>
                  <nts id="Seg_1665" n="HIAT:ip">,</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_1668" n="HIAT:w" s="T1438">если</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1440" id="Seg_1671" n="HIAT:w" s="T1439">от</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_1674" n="HIAT:w" s="T1440">Шуры</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_1677" n="HIAT:w" s="T1441">пойти</ts>
                  <nts id="Seg_1678" n="HIAT:ip">,</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_1681" n="HIAT:w" s="T1442">значит</ts>
                  <nts id="Seg_1682" n="HIAT:ip">,</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1444" id="Seg_1685" n="HIAT:w" s="T1443">на</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_1688" n="HIAT:w" s="T1444">правой</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1446" id="Seg_1691" n="HIAT:w" s="T1445">стороне</ts>
                  <nts id="Seg_1692" n="HIAT:ip">.</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1464" id="Seg_1694" n="sc" s="T1447">
               <ts e="T1464" id="Seg_1696" n="HIAT:u" s="T1447">
                  <ts e="T1448" id="Seg_1698" n="HIAT:w" s="T1447">Был</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1449" id="Seg_1701" n="HIAT:w" s="T1448">дом</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1450" id="Seg_1704" n="HIAT:w" s="T1449">такой</ts>
                  <nts id="Seg_1705" n="HIAT:ip">,</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1451" id="Seg_1708" n="HIAT:w" s="T1450">еще</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1452" id="Seg_1711" n="HIAT:w" s="T1451">продавщица-то</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1453" id="Seg_1714" n="HIAT:w" s="T1452">близко</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1454" id="Seg_1717" n="HIAT:w" s="T1453">жила</ts>
                  <nts id="Seg_1718" n="HIAT:ip">,</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1455" id="Seg_1721" n="HIAT:w" s="T1454">рядом</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1456" id="Seg_1724" n="HIAT:w" s="T1455">кажется</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_1728" n="HIAT:w" s="T1456">в</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1458" id="Seg_1731" n="HIAT:w" s="T1457">соседнем</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1459" id="Seg_1734" n="HIAT:w" s="T1458">доме</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1460" id="Seg_1737" n="HIAT:w" s="T1459">что</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1461" id="Seg_1740" n="HIAT:w" s="T1460">ли</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1462" id="Seg_1743" n="HIAT:w" s="T1461">или</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1745" n="HIAT:ip">(</nts>
                  <ts e="T1463" id="Seg_1747" n="HIAT:w" s="T1462">как</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1464" id="Seg_1750" n="HIAT:w" s="T1463">там</ts>
                  <nts id="Seg_1751" n="HIAT:ip">)</nts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1486" id="Seg_1754" n="sc" s="T1481">
               <ts e="T1484" id="Seg_1756" n="HIAT:u" s="T1481">
                  <ts e="T1484" id="Seg_1758" n="HIAT:w" s="T1481">Ага</ts>
                  <nts id="Seg_1759" n="HIAT:ip">.</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1486" id="Seg_1762" n="HIAT:u" s="T1484">
                  <ts e="T1486" id="Seg_1764" n="HIAT:w" s="T1484">Ага</ts>
                  <nts id="Seg_1765" n="HIAT:ip">.</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1490" id="Seg_1767" n="sc" s="T1488">
               <ts e="T1490" id="Seg_1769" n="HIAT:u" s="T1488">
                  <ts e="T1490" id="Seg_1771" n="HIAT:w" s="T1488">Ага</ts>
                  <nts id="Seg_1772" n="HIAT:ip">.</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1493" id="Seg_1774" n="sc" s="T1492">
               <ts e="T1493" id="Seg_1776" n="HIAT:u" s="T1492">
                  <ts e="T1493" id="Seg_1778" n="HIAT:w" s="T1492">Ага</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1496" id="Seg_1781" n="sc" s="T1494">
               <ts e="T1496" id="Seg_1783" n="HIAT:u" s="T1494">
                  <ts e="T1496" id="Seg_1785" n="HIAT:w" s="T1494">Ага</ts>
                  <nts id="Seg_1786" n="HIAT:ip">.</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1513" id="Seg_1788" n="sc" s="T1500">
               <ts e="T1501" id="Seg_1790" n="HIAT:u" s="T1500">
                  <ts e="T1501" id="Seg_1792" n="HIAT:w" s="T1500">Вот-вот-вот</ts>
                  <nts id="Seg_1793" n="HIAT:ip">.</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1513" id="Seg_1796" n="HIAT:u" s="T1501">
                  <ts e="T1503" id="Seg_1798" n="HIAT:w" s="T1501">Нет</ts>
                  <nts id="Seg_1799" n="HIAT:ip">,</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_1802" n="HIAT:w" s="T1503">я</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1507" id="Seg_1805" n="HIAT:w" s="T1505">тот</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1508" id="Seg_1808" n="HIAT:w" s="T1507">магазин</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1510" id="Seg_1811" n="HIAT:w" s="T1508">вообще</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1512" id="Seg_1814" n="HIAT:w" s="T1510">не</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1513" id="Seg_1817" n="HIAT:w" s="T1512">знаю</ts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1521" id="Seg_1820" n="sc" s="T1519">
               <ts e="T1521" id="Seg_1822" n="HIAT:u" s="T1519">
                  <ts e="T1520" id="Seg_1824" n="HIAT:w" s="T1519">Нет</ts>
                  <nts id="Seg_1825" n="HIAT:ip">,</nts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1521" id="Seg_1828" n="HIAT:w" s="T1520">нет-нет-нет-нет</ts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1541" id="Seg_1831" n="sc" s="T1539">
               <ts e="T1541" id="Seg_1833" n="HIAT:u" s="T1539">
                  <ts e="T1539.tx-KA.1" id="Seg_1835" n="HIAT:w" s="T1539">Mm</ts>
                  <nts id="Seg_1836" n="HIAT:ip">,</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1541" id="Seg_1839" n="HIAT:w" s="T1539.tx-KA.1">mm</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1545" id="Seg_1842" n="sc" s="T1542">
               <ts e="T1545" id="Seg_1844" n="HIAT:u" s="T1542">
                  <nts id="Seg_1845" n="HIAT:ip">(</nts>
                  <ts e="T1543" id="Seg_1847" n="HIAT:w" s="T1542">Что</ts>
                  <nts id="Seg_1848" n="HIAT:ip">,</nts>
                  <nts id="Seg_1849" n="HIAT:ip">)</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1544" id="Seg_1852" n="HIAT:w" s="T1543">кирпичное</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1545" id="Seg_1855" n="HIAT:w" s="T1544">здание</ts>
                  <nts id="Seg_1856" n="HIAT:ip">?</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1550" id="Seg_1858" n="sc" s="T1546">
               <ts e="T1550" id="Seg_1860" n="HIAT:u" s="T1546">
                  <ts e="T1548" id="Seg_1862" n="HIAT:w" s="T1546">Или</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1550" id="Seg_1865" n="HIAT:w" s="T1548">деревянное</ts>
                  <nts id="Seg_1866" n="HIAT:ip">?</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1556" id="Seg_1868" n="sc" s="T1554">
               <ts e="T1556" id="Seg_1870" n="HIAT:u" s="T1554">
                  <ts e="T1556" id="Seg_1872" n="HIAT:w" s="T1554">Mhm</ts>
                  <nts id="Seg_1873" n="HIAT:ip">.</nts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1605" id="Seg_1875" n="sc" s="T1604">
               <ts e="T1605" id="Seg_1877" n="HIAT:u" s="T1604">
                  <ts e="T1605" id="Seg_1879" n="HIAT:w" s="T1604">Ага</ts>
                  <nts id="Seg_1880" n="HIAT:ip">.</nts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1643" id="Seg_1882" n="sc" s="T1642">
               <ts e="T1643" id="Seg_1884" n="HIAT:u" s="T1642">
                  <ts e="T1643" id="Seg_1886" n="HIAT:w" s="T1642">Ага</ts>
                  <nts id="Seg_1887" n="HIAT:ip">.</nts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1661" id="Seg_1889" n="sc" s="T1659">
               <ts e="T1660" id="Seg_1891" n="HIAT:u" s="T1659">
                  <ts e="T1660" id="Seg_1893" n="HIAT:w" s="T1659">Ага</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1661" id="Seg_1897" n="HIAT:u" s="T1660">
                  <ts e="T1661" id="Seg_1899" n="HIAT:w" s="T1660">Ага</ts>
                  <nts id="Seg_1900" n="HIAT:ip">.</nts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1689" id="Seg_1902" n="sc" s="T1672">
               <ts e="T1689" id="Seg_1904" n="HIAT:u" s="T1672">
                  <ts e="T1672.tx-KA.1" id="Seg_1906" n="HIAT:w" s="T1672">Я</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.2" id="Seg_1909" n="HIAT:w" s="T1672.tx-KA.1">так</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1911" n="HIAT:ip">"</nts>
                  <ts e="T1672.tx-KA.3" id="Seg_1913" n="HIAT:w" s="T1672.tx-KA.2">Мурачёв</ts>
                  <nts id="Seg_1914" n="HIAT:ip">"</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.4" id="Seg_1917" n="HIAT:w" s="T1672.tx-KA.3">эту</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.5" id="Seg_1920" n="HIAT:w" s="T1672.tx-KA.4">фамилию</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.6" id="Seg_1923" n="HIAT:w" s="T1672.tx-KA.5">в</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.7" id="Seg_1926" n="HIAT:w" s="T1672.tx-KA.6">общем</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.8" id="Seg_1929" n="HIAT:w" s="T1672.tx-KA.7">помню</ts>
                  <nts id="Seg_1930" n="HIAT:ip">,</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.9" id="Seg_1933" n="HIAT:w" s="T1672.tx-KA.8">а</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.10" id="Seg_1936" n="HIAT:w" s="T1672.tx-KA.9">человека</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.11" id="Seg_1939" n="HIAT:w" s="T1672.tx-KA.10">такого</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1941" n="HIAT:ip">(</nts>
                  <nts id="Seg_1942" n="HIAT:ip">(</nts>
                  <ats e="T1672.tx-KA.12"
                       id="Seg_1943"
                       n="HIAT:non-pho"
                       s="T1672.tx-KA.11">…</ats>
                  <nts id="Seg_1944" n="HIAT:ip">)</nts>
                  <nts id="Seg_1945" n="HIAT:ip">)</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672.tx-KA.13" id="Seg_1948" n="HIAT:w" s="T1672.tx-KA.12">не</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1689" id="Seg_1951" n="HIAT:w" s="T1672.tx-KA.13">знаю</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1716" id="Seg_1954" n="sc" s="T1710">
               <ts e="T1716" id="Seg_1956" n="HIAT:u" s="T1710">
                  <ts e="T1711" id="Seg_1958" n="HIAT:w" s="T1710">А</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1712" id="Seg_1961" n="HIAT:w" s="T1711">сейчас</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1713" id="Seg_1964" n="HIAT:w" s="T1712">давай</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1714" id="Seg_1967" n="HIAT:w" s="T1713">это</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1715" id="Seg_1970" n="HIAT:w" s="T1714">всё</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1716" id="Seg_1973" n="HIAT:w" s="T1715">по-камасински</ts>
                  <nts id="Seg_1974" n="HIAT:ip">.</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1737" id="Seg_1976" n="sc" s="T1717">
               <ts e="T1725" id="Seg_1978" n="HIAT:u" s="T1717">
                  <ts e="T1718" id="Seg_1980" n="HIAT:w" s="T1717">А</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1719" id="Seg_1983" n="HIAT:w" s="T1718">то</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1720" id="Seg_1986" n="HIAT:w" s="T1719">мы</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1721" id="Seg_1989" n="HIAT:w" s="T1720">заговоримся</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1722" id="Seg_1992" n="HIAT:w" s="T1721">и</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1723" id="Seg_1995" n="HIAT:w" s="T1722">забудем</ts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1724" id="Seg_1999" n="HIAT:w" s="T1723">что</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1725" id="Seg_2002" n="HIAT:w" s="T1724">говорили</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1737" id="Seg_2006" n="HIAT:u" s="T1725">
                  <ts e="T1726" id="Seg_2008" n="HIAT:w" s="T1725">Вот</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1727" id="Seg_2011" n="HIAT:w" s="T1726">сначала</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1728" id="Seg_2014" n="HIAT:w" s="T1727">насчет</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1729" id="Seg_2017" n="HIAT:w" s="T1728">спирта</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1730" id="Seg_2021" n="HIAT:w" s="T1729">что</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1731" id="Seg_2024" n="HIAT:w" s="T1730">продают</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1732" id="Seg_2027" n="HIAT:w" s="T1731">еще</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1733" id="Seg_2030" n="HIAT:w" s="T1732">такой</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1734" id="Seg_2033" n="HIAT:w" s="T1733">спирт</ts>
                  <nts id="Seg_2034" n="HIAT:ip">,</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1735" id="Seg_2037" n="HIAT:w" s="T1734">и</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1736" id="Seg_2040" n="HIAT:w" s="T1735">для</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1737" id="Seg_2043" n="HIAT:w" s="T1736">здоровья</ts>
                  <nts id="Seg_2044" n="HIAT:ip">…</nts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T5" id="Seg_2046" n="sc" s="T1">
               <ts e="T2" id="Seg_2048" n="e" s="T1">На </ts>
               <ts e="T3" id="Seg_2050" n="e" s="T2">каком? </ts>
               <ts e="T4" id="Seg_2052" n="e" s="T3">На </ts>
               <ts e="T5" id="Seg_2054" n="e" s="T4">любом. </ts>
            </ts>
            <ts e="T22" id="Seg_2055" n="sc" s="T6">
               <ts e="T7" id="Seg_2057" n="e" s="T6">Или </ts>
               <ts e="T9" id="Seg_2059" n="e" s="T7">сначала </ts>
               <ts e="T10" id="Seg_2061" n="e" s="T9">можешь </ts>
               <ts e="T12" id="Seg_2063" n="e" s="T10">по-русски </ts>
               <ts e="T14" id="Seg_2065" n="e" s="T12">рассказать. </ts>
               <ts e="T15" id="Seg_2067" n="e" s="T14">Расскажи </ts>
               <ts e="T17" id="Seg_2069" n="e" s="T15">сначала </ts>
               <ts e="T18" id="Seg_2071" n="e" s="T17">по-русски, </ts>
               <ts e="T19" id="Seg_2073" n="e" s="T18">а </ts>
               <ts e="T20" id="Seg_2075" n="e" s="T19">потом </ts>
               <ts e="T21" id="Seg_2077" n="e" s="T20">уже </ts>
               <ts e="T22" id="Seg_2079" n="e" s="T21">по-камасински. </ts>
            </ts>
            <ts e="T28" id="Seg_2080" n="sc" s="T23">
               <ts e="T24" id="Seg_2082" n="e" s="T23">А </ts>
               <ts e="T25" id="Seg_2084" n="e" s="T24">я </ts>
               <ts e="T26" id="Seg_2086" n="e" s="T25">послушаю, </ts>
               <ts e="T27" id="Seg_2088" n="e" s="T26">мне </ts>
               <ts e="T28" id="Seg_2090" n="e" s="T27">интересно! </ts>
            </ts>
            <ts e="T34" id="Seg_2091" n="sc" s="T32">
               <ts e="T33" id="Seg_2093" n="e" s="T32">Идет, </ts>
               <ts e="T34" id="Seg_2095" n="e" s="T33">идет. </ts>
            </ts>
            <ts e="T265" id="Seg_2096" n="sc" s="T264">
               <ts e="T265" id="Seg_2098" n="e" s="T264">((…)). </ts>
            </ts>
            <ts e="T282" id="Seg_2099" n="sc" s="T270">
               <ts e="T271" id="Seg_2101" n="e" s="T270">Нет, </ts>
               <ts e="T272" id="Seg_2103" n="e" s="T271">насчет </ts>
               <ts e="T273" id="Seg_2105" n="e" s="T272">счетовода </ts>
               <ts e="T274" id="Seg_2107" n="e" s="T273">ты </ts>
               <ts e="T275" id="Seg_2109" n="e" s="T274">(с-) </ts>
               <ts e="T276" id="Seg_2111" n="e" s="T275">вообще </ts>
               <ts e="T277" id="Seg_2113" n="e" s="T276">не </ts>
               <ts e="T278" id="Seg_2115" n="e" s="T277">рассказывала. </ts>
               <ts e="T279" id="Seg_2117" n="e" s="T278">Как </ts>
               <ts e="T280" id="Seg_2119" n="e" s="T279">это </ts>
               <ts e="T281" id="Seg_2121" n="e" s="T280">было, </ts>
               <ts e="T282" id="Seg_2123" n="e" s="T281">значит? </ts>
            </ts>
            <ts e="T285" id="Seg_2124" n="sc" s="T283">
               <ts e="T284" id="Seg_2126" n="e" s="T283">Пока </ts>
               <ts e="T285" id="Seg_2128" n="e" s="T284">расскажи. </ts>
            </ts>
            <ts e="T293" id="Seg_2129" n="sc" s="T289">
               <ts e="T290" id="Seg_2131" n="e" s="T289">Да, </ts>
               <ts e="T291" id="Seg_2133" n="e" s="T290">насчет </ts>
               <ts e="T293" id="Seg_2135" n="e" s="T291">счетовода? </ts>
            </ts>
            <ts e="T299" id="Seg_2136" n="sc" s="T296">
               <ts e="T299" id="Seg_2138" n="e" s="T296">Ага. </ts>
            </ts>
            <ts e="T303" id="Seg_2139" n="sc" s="T300">
               <ts e="T303" id="Seg_2141" n="e" s="T300">Ага. </ts>
            </ts>
            <ts e="T322" id="Seg_2142" n="sc" s="T311">
               <ts e="T312" id="Seg_2144" n="e" s="T311">Ну </ts>
               <ts e="T313" id="Seg_2146" n="e" s="T312">да, </ts>
               <ts e="T314" id="Seg_2148" n="e" s="T313">это </ts>
               <ts e="T315" id="Seg_2150" n="e" s="T314">насчет </ts>
               <ts e="T316" id="Seg_2152" n="e" s="T315">Паши </ts>
               <ts e="T317" id="Seg_2154" n="e" s="T316">и </ts>
               <ts e="T318" id="Seg_2156" n="e" s="T317">Шуры, </ts>
               <ts e="T319" id="Seg_2158" n="e" s="T318">а </ts>
               <ts e="T320" id="Seg_2160" n="e" s="T319">вот </ts>
               <ts e="T321" id="Seg_2162" n="e" s="T320">счетовод </ts>
               <ts e="T322" id="Seg_2164" n="e" s="T321">тот? </ts>
            </ts>
            <ts e="T329" id="Seg_2165" n="sc" s="T324">
               <ts e="T325" id="Seg_2167" n="e" s="T324">У </ts>
               <ts e="T327" id="Seg_2169" n="e" s="T325">которого </ts>
               <ts e="T329" id="Seg_2171" n="e" s="T327">((…)). </ts>
            </ts>
            <ts e="T340" id="Seg_2172" n="sc" s="T331">
               <ts e="T333" id="Seg_2174" n="e" s="T331">Ага, </ts>
               <ts e="T335" id="Seg_2176" n="e" s="T333">это </ts>
               <ts e="T337" id="Seg_2178" n="e" s="T335">тоже </ts>
               <ts e="T339" id="Seg_2180" n="e" s="T337">есть, </ts>
               <ts e="T340" id="Seg_2182" n="e" s="T339">да? </ts>
            </ts>
            <ts e="T347" id="Seg_2183" n="sc" s="T341">
               <ts e="T342" id="Seg_2185" n="e" s="T341">А, </ts>
               <ts e="T343" id="Seg_2187" n="e" s="T342">а, </ts>
               <ts e="T344" id="Seg_2189" n="e" s="T343">она </ts>
               <ts e="T345" id="Seg_2191" n="e" s="T344">с </ts>
               <ts e="T346" id="Seg_2193" n="e" s="T345">(ней), </ts>
               <ts e="T347" id="Seg_2195" n="e" s="T346">ага. </ts>
            </ts>
            <ts e="T353" id="Seg_2196" n="sc" s="T349">
               <ts e="T351" id="Seg_2198" n="e" s="T349">Все, </ts>
               <ts e="T353" id="Seg_2200" n="e" s="T351">ясно. </ts>
            </ts>
            <ts e="T376" id="Seg_2201" n="sc" s="T364">
               <ts e="T366" id="Seg_2203" n="e" s="T364">Все, </ts>
               <ts e="T368" id="Seg_2205" n="e" s="T366">слушает, </ts>
               <ts e="T370" id="Seg_2207" n="e" s="T368">слушает, </ts>
               <ts e="T372" id="Seg_2209" n="e" s="T370">говори </ts>
               <ts e="T374" id="Seg_2211" n="e" s="T372">что </ts>
               <ts e="T376" id="Seg_2213" n="e" s="T374">хочешь. </ts>
            </ts>
            <ts e="T384" id="Seg_2214" n="sc" s="T381">
               <ts e="T384" id="Seg_2216" n="e" s="T381">Нет. </ts>
            </ts>
            <ts e="T396" id="Seg_2217" n="sc" s="T385">
               <ts e="T386" id="Seg_2219" n="e" s="T385">Да, </ts>
               <ts e="T387" id="Seg_2221" n="e" s="T386">значит, </ts>
               <ts e="T388" id="Seg_2223" n="e" s="T387">насчет </ts>
               <ts e="T389" id="Seg_2225" n="e" s="T388">счетовода </ts>
               <ts e="T390" id="Seg_2227" n="e" s="T389">по-русски </ts>
               <ts e="T391" id="Seg_2229" n="e" s="T390">не </ts>
               <ts e="T392" id="Seg_2231" n="e" s="T391">рассказала. </ts>
               <ts e="T393" id="Seg_2233" n="e" s="T392">Насчет </ts>
               <ts e="T394" id="Seg_2235" n="e" s="T393">счетовода </ts>
               <ts e="T395" id="Seg_2237" n="e" s="T394">можно </ts>
               <ts e="T396" id="Seg_2239" n="e" s="T395">сказать. </ts>
            </ts>
            <ts e="T407" id="Seg_2240" n="sc" s="T397">
               <ts e="T398" id="Seg_2242" n="e" s="T397">Вот, </ts>
               <ts e="T399" id="Seg_2244" n="e" s="T398">как </ts>
               <ts e="T400" id="Seg_2246" n="e" s="T399">руки </ts>
               <ts e="T401" id="Seg_2248" n="e" s="T400">отморозил, </ts>
               <ts e="T402" id="Seg_2250" n="e" s="T401">это </ts>
               <ts e="T403" id="Seg_2252" n="e" s="T402">пока </ts>
               <ts e="T404" id="Seg_2254" n="e" s="T403">ты </ts>
               <ts e="T405" id="Seg_2256" n="e" s="T404">не </ts>
               <ts e="T406" id="Seg_2258" n="e" s="T405">говорила </ts>
               <ts e="T407" id="Seg_2260" n="e" s="T406">по-русски. </ts>
            </ts>
            <ts e="T420" id="Seg_2261" n="sc" s="T408">
               <ts e="T409" id="Seg_2263" n="e" s="T408">А </ts>
               <ts e="T410" id="Seg_2265" n="e" s="T409">рассказала, </ts>
               <ts e="T411" id="Seg_2267" n="e" s="T410">как </ts>
               <ts e="T412" id="Seg_2269" n="e" s="T411">разошлись </ts>
               <ts e="T413" id="Seg_2271" n="e" s="T412">и </ts>
               <ts e="T414" id="Seg_2273" n="e" s="T413">судили, </ts>
               <ts e="T415" id="Seg_2275" n="e" s="T414">вот </ts>
               <ts e="T416" id="Seg_2277" n="e" s="T415">это </ts>
               <ts e="T417" id="Seg_2279" n="e" s="T416">ты </ts>
               <ts e="T418" id="Seg_2281" n="e" s="T417">уже </ts>
               <ts e="T419" id="Seg_2283" n="e" s="T418">по-русски </ts>
               <ts e="T420" id="Seg_2285" n="e" s="T419">говорила. </ts>
            </ts>
            <ts e="T424" id="Seg_2286" n="sc" s="T423">
               <ts e="T424" id="Seg_2288" n="e" s="T423">Ага. </ts>
            </ts>
            <ts e="T434" id="Seg_2289" n="sc" s="T433">
               <ts e="T434" id="Seg_2291" n="e" s="T433">Так… </ts>
            </ts>
            <ts e="T446" id="Seg_2292" n="sc" s="T437">
               <ts e="T439" id="Seg_2294" n="e" s="T437">По-русски, </ts>
               <ts e="T440" id="Seg_2296" n="e" s="T439">только </ts>
               <ts e="T441" id="Seg_2298" n="e" s="T440">ты </ts>
               <ts e="T442" id="Seg_2300" n="e" s="T441">вот </ts>
               <ts e="T444" id="Seg_2302" n="e" s="T442">эту </ts>
               <ts e="T446" id="Seg_2304" n="e" s="T444">(часть)… </ts>
            </ts>
            <ts e="T457" id="Seg_2305" n="sc" s="T456">
               <ts e="T457" id="Seg_2307" n="e" s="T456">Ага. </ts>
            </ts>
            <ts e="T477" id="Seg_2308" n="sc" s="T466">
               <ts e="T467" id="Seg_2310" n="e" s="T466">Ну </ts>
               <ts e="T469" id="Seg_2312" n="e" s="T467">хорошо, </ts>
               <ts e="T471" id="Seg_2314" n="e" s="T469">все </ts>
               <ts e="T472" id="Seg_2316" n="e" s="T471">равно </ts>
               <ts e="T473" id="Seg_2318" n="e" s="T472">будет </ts>
               <ts e="T474" id="Seg_2320" n="e" s="T473">понятно, </ts>
               <ts e="T475" id="Seg_2322" n="e" s="T474">люди </ts>
               <ts e="T476" id="Seg_2324" n="e" s="T475">умные — </ts>
               <ts e="T477" id="Seg_2326" n="e" s="T476">поймем. </ts>
            </ts>
            <ts e="T489" id="Seg_2327" n="sc" s="T478">
               <ts e="T479" id="Seg_2329" n="e" s="T478">Ну, </ts>
               <ts e="T480" id="Seg_2331" n="e" s="T479">что-нибудь </ts>
               <ts e="T481" id="Seg_2333" n="e" s="T480">другое </ts>
               <ts e="T482" id="Seg_2335" n="e" s="T481">расскажи </ts>
               <ts e="T483" id="Seg_2337" n="e" s="T482">о </ts>
               <ts e="T484" id="Seg_2339" n="e" s="T483">деревенской </ts>
               <ts e="T485" id="Seg_2341" n="e" s="T484">жизни, </ts>
               <ts e="T486" id="Seg_2343" n="e" s="T485">что </ts>
               <ts e="T487" id="Seg_2345" n="e" s="T486">ж, </ts>
               <ts e="T488" id="Seg_2347" n="e" s="T487">все </ts>
               <ts e="T489" id="Seg_2349" n="e" s="T488">равно. </ts>
            </ts>
            <ts e="T496" id="Seg_2350" n="sc" s="T490">
               <ts e="T491" id="Seg_2352" n="e" s="T490">Что </ts>
               <ts e="T492" id="Seg_2354" n="e" s="T491">у </ts>
               <ts e="T493" id="Seg_2356" n="e" s="T492">вас </ts>
               <ts e="T494" id="Seg_2358" n="e" s="T493">там </ts>
               <ts e="T495" id="Seg_2360" n="e" s="T494">еще </ts>
               <ts e="T496" id="Seg_2362" n="e" s="T495">нового? </ts>
            </ts>
            <ts e="T526" id="Seg_2363" n="sc" s="T500">
               <ts e="T501" id="Seg_2365" n="e" s="T500">Кто </ts>
               <ts e="T503" id="Seg_2367" n="e" s="T501">у </ts>
               <ts e="T504" id="Seg_2369" n="e" s="T503">вас </ts>
               <ts e="T505" id="Seg_2371" n="e" s="T504">там </ts>
               <ts e="T506" id="Seg_2373" n="e" s="T505">начальствует </ts>
               <ts e="T507" id="Seg_2375" n="e" s="T506">сейчас? </ts>
               <ts e="T508" id="Seg_2377" n="e" s="T507">Я </ts>
               <ts e="T509" id="Seg_2379" n="e" s="T508">помню </ts>
               <ts e="T510" id="Seg_2381" n="e" s="T509">вот </ts>
               <ts e="T511" id="Seg_2383" n="e" s="T510">председателя </ts>
               <ts e="T512" id="Seg_2385" n="e" s="T511">сельсовета </ts>
               <ts e="T513" id="Seg_2387" n="e" s="T512">там </ts>
               <ts e="T514" id="Seg_2389" n="e" s="T513">мы </ts>
               <ts e="T515" id="Seg_2391" n="e" s="T514">какие-то </ts>
               <ts e="T516" id="Seg_2393" n="e" s="T515">печати </ts>
               <ts e="T517" id="Seg_2395" n="e" s="T516">доставали </ts>
               <ts e="T518" id="Seg_2397" n="e" s="T517">у </ts>
               <ts e="T519" id="Seg_2399" n="e" s="T518">(нее=) </ts>
               <ts e="T521" id="Seg_2401" n="e" s="T519">его </ts>
               <ts e="T522" id="Seg_2403" n="e" s="T521">и </ts>
               <ts e="T524" id="Seg_2405" n="e" s="T522">все </ts>
               <ts e="T526" id="Seg_2407" n="e" s="T524">такое. </ts>
            </ts>
            <ts e="T569" id="Seg_2408" n="sc" s="T541">
               <ts e="T543" id="Seg_2410" n="e" s="T541">Как, </ts>
               <ts e="T545" id="Seg_2412" n="e" s="T543">как, </ts>
               <ts e="T546" id="Seg_2414" n="e" s="T545">я </ts>
               <ts e="T547" id="Seg_2416" n="e" s="T546">знаю </ts>
               <ts e="T548" id="Seg_2418" n="e" s="T547">его, </ts>
               <ts e="T551" id="Seg_2420" n="e" s="T548">знаю, </ts>
               <ts e="T553" id="Seg_2422" n="e" s="T551">что </ts>
               <ts e="T555" id="Seg_2424" n="e" s="T553">он </ts>
               <ts e="T557" id="Seg_2426" n="e" s="T555">был. </ts>
               <ts e="T560" id="Seg_2428" n="e" s="T557">Нет, </ts>
               <ts e="T562" id="Seg_2430" n="e" s="T560">нет, </ts>
               <ts e="T563" id="Seg_2432" n="e" s="T562">нет, </ts>
               <ts e="T565" id="Seg_2434" n="e" s="T563">расскажи </ts>
               <ts e="T567" id="Seg_2436" n="e" s="T565">как </ts>
               <ts e="T569" id="Seg_2438" n="e" s="T567">раз. </ts>
            </ts>
            <ts e="T575" id="Seg_2439" n="sc" s="T573">
               <ts e="T575" id="Seg_2441" n="e" s="T573">А-а-а… </ts>
            </ts>
            <ts e="T590" id="Seg_2442" n="sc" s="T586">
               <ts e="T588" id="Seg_2444" n="e" s="T586">Добрый </ts>
               <ts e="T589" id="Seg_2446" n="e" s="T588">был </ts>
               <ts e="T590" id="Seg_2448" n="e" s="T589">человек. </ts>
            </ts>
            <ts e="T597" id="Seg_2449" n="sc" s="T591">
               <ts e="T593" id="Seg_2451" n="e" s="T591">Вот </ts>
               <ts e="T595" id="Seg_2453" n="e" s="T593">расскажи, </ts>
               <ts e="T597" id="Seg_2455" n="e" s="T595">пожалуйста. </ts>
            </ts>
            <ts e="T853" id="Seg_2456" n="sc" s="T839">
               <ts e="T840" id="Seg_2458" n="e" s="T839">Так </ts>
               <ts e="T841" id="Seg_2460" n="e" s="T840">я </ts>
               <ts e="T842" id="Seg_2462" n="e" s="T841">уж </ts>
               <ts e="T843" id="Seg_2464" n="e" s="T842">не </ts>
               <ts e="T844" id="Seg_2466" n="e" s="T843">помню </ts>
               <ts e="T846" id="Seg_2468" n="e" s="T844">сейчас, </ts>
               <ts e="T847" id="Seg_2470" n="e" s="T846">знал </ts>
               <ts e="T849" id="Seg_2472" n="e" s="T847">я </ts>
               <ts e="T850" id="Seg_2474" n="e" s="T849">его </ts>
               <ts e="T852" id="Seg_2476" n="e" s="T850">или </ts>
               <ts e="T853" id="Seg_2478" n="e" s="T852">нет. </ts>
            </ts>
            <ts e="T857" id="Seg_2479" n="sc" s="T855">
               <ts e="T857" id="Seg_2481" n="e" s="T855">Ага. </ts>
            </ts>
            <ts e="T889" id="Seg_2482" n="sc" s="T887">
               <ts e="T889" id="Seg_2484" n="e" s="T887">Mm, mm, mm, mm. </ts>
            </ts>
            <ts e="T907" id="Seg_2485" n="sc" s="T906">
               <ts e="T907" id="Seg_2487" n="e" s="T906">Так. </ts>
            </ts>
            <ts e="T911" id="Seg_2488" n="sc" s="T908">
               <ts e="T911" id="Seg_2490" n="e" s="T908">((…)). </ts>
            </ts>
            <ts e="T934" id="Seg_2491" n="sc" s="T919">
               <ts e="T920" id="Seg_2493" n="e" s="T919">Ну, </ts>
               <ts e="T921" id="Seg_2495" n="e" s="T920">это </ts>
               <ts e="T922" id="Seg_2497" n="e" s="T921">мы </ts>
               <ts e="T923" id="Seg_2499" n="e" s="T922">еще </ts>
               <ts e="T924" id="Seg_2501" n="e" s="T923">все </ts>
               <ts e="T925" id="Seg_2503" n="e" s="T924">поговорим, </ts>
               <ts e="T926" id="Seg_2505" n="e" s="T925">а </ts>
               <ts e="T927" id="Seg_2507" n="e" s="T926">может </ts>
               <ts e="T928" id="Seg_2509" n="e" s="T927">ты </ts>
               <ts e="T930" id="Seg_2511" n="e" s="T928">пока </ts>
               <ts e="T931" id="Seg_2513" n="e" s="T930">вот </ts>
               <ts e="T932" id="Seg_2515" n="e" s="T931">насчет </ts>
               <ts e="T934" id="Seg_2517" n="e" s="T932">Загороднюка? </ts>
            </ts>
            <ts e="T948" id="Seg_2518" n="sc" s="T935">
               <ts e="T936" id="Seg_2520" n="e" s="T935">На </ts>
               <ts e="T937" id="Seg_2522" n="e" s="T936">камасинском </ts>
               <ts e="T938" id="Seg_2524" n="e" s="T937">всё </ts>
               <ts e="T939" id="Seg_2526" n="e" s="T938">это </ts>
               <ts e="T940" id="Seg_2528" n="e" s="T939">дело, </ts>
               <ts e="T941" id="Seg_2530" n="e" s="T940">как </ts>
               <ts e="T942" id="Seg_2532" n="e" s="T941">с </ts>
               <ts e="T943" id="Seg_2534" n="e" s="T942">ним </ts>
               <ts e="T944" id="Seg_2536" n="e" s="T943">было. </ts>
               <ts e="T945" id="Seg_2538" n="e" s="T944">Как </ts>
               <ts e="T946" id="Seg_2540" n="e" s="T945">вы </ts>
               <ts e="T947" id="Seg_2542" n="e" s="T946">там </ts>
               <ts e="T948" id="Seg_2544" n="e" s="T947">ехали. </ts>
            </ts>
            <ts e="T956" id="Seg_2545" n="sc" s="T950">
               <ts e="T952" id="Seg_2547" n="e" s="T950">Всё </ts>
               <ts e="T953" id="Seg_2549" n="e" s="T952">слушают, </ts>
               <ts e="T955" id="Seg_2551" n="e" s="T953">всё </ts>
               <ts e="T956" id="Seg_2553" n="e" s="T955">записано. </ts>
            </ts>
            <ts e="T964" id="Seg_2554" n="sc" s="T958">
               <ts e="T960" id="Seg_2556" n="e" s="T958">Записано, </ts>
               <ts e="T961" id="Seg_2558" n="e" s="T960">по-русски </ts>
               <ts e="T962" id="Seg_2560" n="e" s="T961">сейчас </ts>
               <ts e="T963" id="Seg_2562" n="e" s="T962">всё </ts>
               <ts e="T964" id="Seg_2564" n="e" s="T963">записали. </ts>
            </ts>
            <ts e="T968" id="Seg_2565" n="sc" s="T965">
               <ts e="T966" id="Seg_2567" n="e" s="T965">Теперь </ts>
               <ts e="T967" id="Seg_2569" n="e" s="T966">только </ts>
               <ts e="T968" id="Seg_2571" n="e" s="T967">по-камасински. </ts>
            </ts>
            <ts e="T1072" id="Seg_2572" n="sc" s="T1040">
               <ts e="T1042" id="Seg_2574" n="e" s="T1040">Ты </ts>
               <ts e="T1043" id="Seg_2576" n="e" s="T1042">еще </ts>
               <ts e="T1044" id="Seg_2578" n="e" s="T1043">рассказала </ts>
               <ts e="T1045" id="Seg_2580" n="e" s="T1044">насчет </ts>
               <ts e="T1046" id="Seg_2582" n="e" s="T1045">того, </ts>
               <ts e="T1047" id="Seg_2584" n="e" s="T1046">как </ts>
               <ts e="T1048" id="Seg_2586" n="e" s="T1047">ты </ts>
               <ts e="T1049" id="Seg_2588" n="e" s="T1048">сказала </ts>
               <ts e="T1050" id="Seg_2590" n="e" s="T1049">ему </ts>
               <ts e="T1051" id="Seg_2592" n="e" s="T1050">что </ts>
               <ts e="T1052" id="Seg_2594" n="e" s="T1051">"Я </ts>
               <ts e="T1053" id="Seg_2596" n="e" s="T1052">тебе </ts>
               <ts e="T1054" id="Seg_2598" n="e" s="T1053">как </ts>
               <ts e="T1055" id="Seg_2600" n="e" s="T1054">сыну </ts>
               <ts e="T1056" id="Seg_2602" n="e" s="T1055">скажу, </ts>
               <ts e="T1057" id="Seg_2604" n="e" s="T1056">что </ts>
               <ts e="T1058" id="Seg_2606" n="e" s="T1057">богу </ts>
               <ts e="T1059" id="Seg_2608" n="e" s="T1058">поеду </ts>
               <ts e="T1060" id="Seg_2610" n="e" s="T1059">помолиться". </ts>
               <ts e="T1062" id="Seg_2612" n="e" s="T1060">Вот </ts>
               <ts e="T1063" id="Seg_2614" n="e" s="T1062">еще </ts>
               <ts e="T1065" id="Seg_2616" n="e" s="T1063">по-камасински </ts>
               <ts e="T1066" id="Seg_2618" n="e" s="T1065">не </ts>
               <ts e="T1067" id="Seg_2620" n="e" s="T1066">рассказала. </ts>
               <ts e="T1068" id="Seg_2622" n="e" s="T1067">Пожалуйста, </ts>
               <ts e="T1069" id="Seg_2624" n="e" s="T1068">пожалуйста, </ts>
               <ts e="T1070" id="Seg_2626" n="e" s="T1069">всё </ts>
               <ts e="T1071" id="Seg_2628" n="e" s="T1070">слушает, </ts>
               <ts e="T1072" id="Seg_2630" n="e" s="T1071">пишет. </ts>
            </ts>
            <ts e="T1117" id="Seg_2631" n="sc" s="T1101">
               <ts e="T1102" id="Seg_2633" n="e" s="T1101">Ну </ts>
               <ts e="T1103" id="Seg_2635" n="e" s="T1102">тут </ts>
               <ts e="T1104" id="Seg_2637" n="e" s="T1103">мы </ts>
               <ts e="T1105" id="Seg_2639" n="e" s="T1104">начали </ts>
               <ts e="T1106" id="Seg_2641" n="e" s="T1105">и </ts>
               <ts e="T1107" id="Seg_2643" n="e" s="T1106">о </ts>
               <ts e="T1108" id="Seg_2645" n="e" s="T1107">бригадире </ts>
               <ts e="T1109" id="Seg_2647" n="e" s="T1108">говорить. </ts>
               <ts e="T1110" id="Seg_2649" n="e" s="T1109">Нет, </ts>
               <ts e="T1111" id="Seg_2651" n="e" s="T1110">а </ts>
               <ts e="T1112" id="Seg_2653" n="e" s="T1111">вот </ts>
               <ts e="T1113" id="Seg_2655" n="e" s="T1112">что </ts>
               <ts e="T1114" id="Seg_2657" n="e" s="T1113">мы </ts>
               <ts e="T1115" id="Seg_2659" n="e" s="T1114">не </ts>
               <ts e="T1116" id="Seg_2661" n="e" s="T1115">рассказали </ts>
               <ts e="T1117" id="Seg_2663" n="e" s="T1116">еще. </ts>
            </ts>
            <ts e="T1137" id="Seg_2664" n="sc" s="T1118">
               <ts e="T1119" id="Seg_2666" n="e" s="T1118">Это </ts>
               <ts e="T1120" id="Seg_2668" n="e" s="T1119">мы </ts>
               <ts e="T1121" id="Seg_2670" n="e" s="T1120">только </ts>
               <ts e="T1122" id="Seg_2672" n="e" s="T1121">тут </ts>
               <ts e="T1123" id="Seg_2674" n="e" s="T1122">говорили, </ts>
               <ts e="T1124" id="Seg_2676" n="e" s="T1123">что </ts>
               <ts e="T1125" id="Seg_2678" n="e" s="T1124">он </ts>
               <ts e="T1126" id="Seg_2680" n="e" s="T1125">помер </ts>
               <ts e="T1127" id="Seg_2682" n="e" s="T1126">два </ts>
               <ts e="T1128" id="Seg_2684" n="e" s="T1127">года </ts>
               <ts e="T1129" id="Seg_2686" n="e" s="T1128">тому </ts>
               <ts e="T1130" id="Seg_2688" n="e" s="T1129">назад. </ts>
               <ts e="T1131" id="Seg_2690" n="e" s="T1130">А </ts>
               <ts e="T1132" id="Seg_2692" n="e" s="T1131">тут </ts>
               <ts e="T1134" id="Seg_2694" n="e" s="T1132">еще </ts>
               <ts e="T1135" id="Seg_2696" n="e" s="T1134">не </ts>
               <ts e="T1137" id="Seg_2698" n="e" s="T1135">рассказали. </ts>
            </ts>
            <ts e="T1157" id="Seg_2699" n="sc" s="T1145">
               <ts e="T1147" id="Seg_2701" n="e" s="T1145">Ага, </ts>
               <ts e="T1149" id="Seg_2703" n="e" s="T1147">ага. </ts>
               <ts e="T1150" id="Seg_2705" n="e" s="T1149">А, </ts>
               <ts e="T1151" id="Seg_2707" n="e" s="T1150">это </ts>
               <ts e="T1152" id="Seg_2709" n="e" s="T1151">было </ts>
               <ts e="T1153" id="Seg_2711" n="e" s="T1152">рассказано, </ts>
               <ts e="T1154" id="Seg_2713" n="e" s="T1153">значит, </ts>
               <ts e="T1155" id="Seg_2715" n="e" s="T1154">все </ts>
               <ts e="T1156" id="Seg_2717" n="e" s="T1155">в </ts>
               <ts e="T1157" id="Seg_2719" n="e" s="T1156">порядке. </ts>
            </ts>
            <ts e="T1162" id="Seg_2720" n="sc" s="T1158">
               <ts e="T1159" id="Seg_2722" n="e" s="T1158">Ну, </ts>
               <ts e="T1160" id="Seg_2724" n="e" s="T1159">может </ts>
               <ts e="T1161" id="Seg_2726" n="e" s="T1160">насчет </ts>
               <ts e="T1162" id="Seg_2728" n="e" s="T1161">бригадира? </ts>
            </ts>
            <ts e="T1172" id="Seg_2729" n="sc" s="T1163">
               <ts e="T1164" id="Seg_2731" n="e" s="T1163">Этого </ts>
               <ts e="T1165" id="Seg_2733" n="e" s="T1164">Августа, </ts>
               <ts e="T1166" id="Seg_2735" n="e" s="T1165">немца. </ts>
               <ts e="T1167" id="Seg_2737" n="e" s="T1166">((NOISE)) </ts>
               <ts e="T1168" id="Seg_2739" n="e" s="T1167">Я </ts>
               <ts e="T1169" id="Seg_2741" n="e" s="T1168">его </ts>
               <ts e="T1170" id="Seg_2743" n="e" s="T1169">не </ts>
               <ts e="T1171" id="Seg_2745" n="e" s="T1170">помню, </ts>
               <ts e="T1172" id="Seg_2747" n="e" s="T1171">кажется. </ts>
            </ts>
            <ts e="T1176" id="Seg_2748" n="sc" s="T1173">
               <ts e="T1174" id="Seg_2750" n="e" s="T1173">По-моему, </ts>
               <ts e="T1175" id="Seg_2752" n="e" s="T1174">не </ts>
               <ts e="T1176" id="Seg_2754" n="e" s="T1175">помню. </ts>
            </ts>
            <ts e="T1247" id="Seg_2755" n="sc" s="T1238">
               <ts e="T1239" id="Seg_2757" n="e" s="T1238">По-русски </ts>
               <ts e="T1240" id="Seg_2759" n="e" s="T1239">еще </ts>
               <ts e="T1241" id="Seg_2761" n="e" s="T1240">все </ts>
               <ts e="T1242" id="Seg_2763" n="e" s="T1241">это, </ts>
               <ts e="T1243" id="Seg_2765" n="e" s="T1242">ты </ts>
               <ts e="T1244" id="Seg_2767" n="e" s="T1243">сейчас </ts>
               <ts e="T1245" id="Seg_2769" n="e" s="T1244">много </ts>
               <ts e="T1246" id="Seg_2771" n="e" s="T1245">нового </ts>
               <ts e="T1247" id="Seg_2773" n="e" s="T1246">рассказала. </ts>
            </ts>
            <ts e="T1289" id="Seg_2774" n="sc" s="T1288">
               <ts e="T1289" id="Seg_2776" n="e" s="T1288">Так. </ts>
            </ts>
            <ts e="T1302" id="Seg_2777" n="sc" s="T1290">
               <ts e="T1291" id="Seg_2779" n="e" s="T1290">Вот </ts>
               <ts e="T1292" id="Seg_2781" n="e" s="T1291">что </ts>
               <ts e="T1293" id="Seg_2783" n="e" s="T1292">меня </ts>
               <ts e="T1294" id="Seg_2785" n="e" s="T1293">интересует, </ts>
               <ts e="T1295" id="Seg_2787" n="e" s="T1294">я </ts>
               <ts e="T1296" id="Seg_2789" n="e" s="T1295">помню </ts>
               <ts e="T1297" id="Seg_2791" n="e" s="T1296">ведь </ts>
               <ts e="T1298" id="Seg_2793" n="e" s="T1297">в </ts>
               <ts e="T1299" id="Seg_2795" n="e" s="T1298">Абалакове </ts>
               <ts e="T1300" id="Seg_2797" n="e" s="T1299">был </ts>
               <ts e="T1301" id="Seg_2799" n="e" s="T1300">такой </ts>
               <ts e="T1302" id="Seg_2801" n="e" s="T1301">магазин. </ts>
            </ts>
            <ts e="T1307" id="Seg_2802" n="sc" s="T1303">
               <ts e="T1304" id="Seg_2804" n="e" s="T1303">И </ts>
               <ts e="T1305" id="Seg_2806" n="e" s="T1304">всякое </ts>
               <ts e="T1306" id="Seg_2808" n="e" s="T1305">там </ts>
               <ts e="T1307" id="Seg_2810" n="e" s="T1306">продавали. </ts>
            </ts>
            <ts e="T1316" id="Seg_2811" n="sc" s="T1308">
               <ts e="T1309" id="Seg_2813" n="e" s="T1308">Вот </ts>
               <ts e="T1310" id="Seg_2815" n="e" s="T1309">что </ts>
               <ts e="T1311" id="Seg_2817" n="e" s="T1310">там </ts>
               <ts e="T1312" id="Seg_2819" n="e" s="T1311">сейчас </ts>
               <ts e="T1313" id="Seg_2821" n="e" s="T1312">продается, </ts>
               <ts e="T1314" id="Seg_2823" n="e" s="T1313">ты </ts>
               <ts e="T1315" id="Seg_2825" n="e" s="T1314">не </ts>
               <ts e="T1316" id="Seg_2827" n="e" s="T1315">расскажешь? </ts>
            </ts>
            <ts e="T1334" id="Seg_2828" n="sc" s="T1317">
               <ts e="T1318" id="Seg_2830" n="e" s="T1317">Интересно, </ts>
               <ts e="T1319" id="Seg_2832" n="e" s="T1318">я </ts>
               <ts e="T1320" id="Seg_2834" n="e" s="T1319">помню, </ts>
               <ts e="T1321" id="Seg_2836" n="e" s="T1320">тогда, </ts>
               <ts e="T1322" id="Seg_2838" n="e" s="T1321">когда </ts>
               <ts e="T1323" id="Seg_2840" n="e" s="T1322">мы </ts>
               <ts e="T1324" id="Seg_2842" n="e" s="T1323">последний </ts>
               <ts e="T1325" id="Seg_2844" n="e" s="T1324">раз </ts>
               <ts e="T1326" id="Seg_2846" n="e" s="T1325">там </ts>
               <ts e="T1327" id="Seg_2848" n="e" s="T1326">были, </ts>
               <ts e="T1328" id="Seg_2850" n="e" s="T1327">даже </ts>
               <ts e="T1329" id="Seg_2852" n="e" s="T1328">такой </ts>
               <ts e="T1330" id="Seg_2854" n="e" s="T1329">спирт </ts>
               <ts e="T1331" id="Seg_2856" n="e" s="T1330">продавали, </ts>
               <ts e="T1332" id="Seg_2858" n="e" s="T1331">69 </ts>
               <ts e="T1333" id="Seg_2860" n="e" s="T1332">процентов </ts>
               <ts e="T1334" id="Seg_2862" n="e" s="T1333">алкоголя. </ts>
            </ts>
            <ts e="T1339" id="Seg_2863" n="sc" s="T1335">
               <ts e="T1336" id="Seg_2865" n="e" s="T1335">Страшный </ts>
               <ts e="T1337" id="Seg_2867" n="e" s="T1336">такой </ts>
               <ts e="T1338" id="Seg_2869" n="e" s="T1337">был </ts>
               <ts e="T1339" id="Seg_2871" n="e" s="T1338">напиток. </ts>
            </ts>
            <ts e="T1344" id="Seg_2872" n="sc" s="T1340">
               <ts e="T1344" id="Seg_2874" n="e" s="T1340">((LAUGH)). </ts>
            </ts>
            <ts e="T1346" id="Seg_2875" n="sc" s="T1345">
               <ts e="T1346" id="Seg_2877" n="e" s="T1345">Ага. </ts>
            </ts>
            <ts e="T1351" id="Seg_2878" n="sc" s="T1350">
               <ts e="T1351" id="Seg_2880" n="e" s="T1350">Mhm. </ts>
            </ts>
            <ts e="T1370" id="Seg_2881" n="sc" s="T1368">
               <ts e="T1370" id="Seg_2883" n="e" s="T1368">Mhm. </ts>
            </ts>
            <ts e="T1381" id="Seg_2884" n="sc" s="T1378">
               <ts e="T1379" id="Seg_2886" n="e" s="T1378">Так, </ts>
               <ts e="T1381" id="Seg_2888" n="e" s="T1379">ну… </ts>
            </ts>
            <ts e="T1419" id="Seg_2889" n="sc" s="T1405">
               <ts e="T1406" id="Seg_2891" n="e" s="T1405">Нет, </ts>
               <ts e="T1407" id="Seg_2893" n="e" s="T1406">я </ts>
               <ts e="T1408" id="Seg_2895" n="e" s="T1407">не </ts>
               <ts e="T1409" id="Seg_2897" n="e" s="T1408">знаю, </ts>
               <ts e="T1410" id="Seg_2899" n="e" s="T1409">я </ts>
               <ts e="T1411" id="Seg_2901" n="e" s="T1410">помню </ts>
               <ts e="T1412" id="Seg_2903" n="e" s="T1411">когда </ts>
               <ts e="T1413" id="Seg_2905" n="e" s="T1412">магазин </ts>
               <ts e="T1414" id="Seg_2907" n="e" s="T1413">был </ts>
               <ts e="T1415" id="Seg_2909" n="e" s="T1414">в </ts>
               <ts e="T1416" id="Seg_2911" n="e" s="T1415">самом </ts>
               <ts e="T1417" id="Seg_2913" n="e" s="T1416">центре </ts>
               <ts e="T1418" id="Seg_2915" n="e" s="T1417">стоял </ts>
               <ts e="T1419" id="Seg_2917" n="e" s="T1418">там. </ts>
            </ts>
            <ts e="T1432" id="Seg_2918" n="sc" s="T1420">
               <ts e="T1421" id="Seg_2920" n="e" s="T1420">На </ts>
               <ts e="T1422" id="Seg_2922" n="e" s="T1421">той </ts>
               <ts e="T1423" id="Seg_2924" n="e" s="T1422">же </ts>
               <ts e="T1424" id="Seg_2926" n="e" s="T1423">улице </ts>
               <ts e="T1425" id="Seg_2928" n="e" s="T1424">вот, </ts>
               <ts e="T1426" id="Seg_2930" n="e" s="T1425">близко </ts>
               <ts e="T1427" id="Seg_2932" n="e" s="T1426">Шуры, </ts>
               <ts e="T1428" id="Seg_2934" n="e" s="T1427">(не </ts>
               <ts e="T1429" id="Seg_2936" n="e" s="T1428">с-) </ts>
               <ts e="T1430" id="Seg_2938" n="e" s="T1429">не </ts>
               <ts e="T1431" id="Seg_2940" n="e" s="T1430">очень </ts>
               <ts e="T1432" id="Seg_2942" n="e" s="T1431">далеко. </ts>
            </ts>
            <ts e="T1446" id="Seg_2943" n="sc" s="T1435">
               <ts e="T1436" id="Seg_2945" n="e" s="T1435">До </ts>
               <ts e="T1437" id="Seg_2947" n="e" s="T1436">клуба </ts>
               <ts e="T1438" id="Seg_2949" n="e" s="T1437">еще, </ts>
               <ts e="T1439" id="Seg_2951" n="e" s="T1438">если </ts>
               <ts e="T1440" id="Seg_2953" n="e" s="T1439">от </ts>
               <ts e="T1441" id="Seg_2955" n="e" s="T1440">Шуры </ts>
               <ts e="T1442" id="Seg_2957" n="e" s="T1441">пойти, </ts>
               <ts e="T1443" id="Seg_2959" n="e" s="T1442">значит, </ts>
               <ts e="T1444" id="Seg_2961" n="e" s="T1443">на </ts>
               <ts e="T1445" id="Seg_2963" n="e" s="T1444">правой </ts>
               <ts e="T1446" id="Seg_2965" n="e" s="T1445">стороне. </ts>
            </ts>
            <ts e="T1464" id="Seg_2966" n="sc" s="T1447">
               <ts e="T1448" id="Seg_2968" n="e" s="T1447">Был </ts>
               <ts e="T1449" id="Seg_2970" n="e" s="T1448">дом </ts>
               <ts e="T1450" id="Seg_2972" n="e" s="T1449">такой, </ts>
               <ts e="T1451" id="Seg_2974" n="e" s="T1450">еще </ts>
               <ts e="T1452" id="Seg_2976" n="e" s="T1451">продавщица-то </ts>
               <ts e="T1453" id="Seg_2978" n="e" s="T1452">близко </ts>
               <ts e="T1454" id="Seg_2980" n="e" s="T1453">жила, </ts>
               <ts e="T1455" id="Seg_2982" n="e" s="T1454">рядом </ts>
               <ts e="T1456" id="Seg_2984" n="e" s="T1455">кажется, </ts>
               <ts e="T1457" id="Seg_2986" n="e" s="T1456">в </ts>
               <ts e="T1458" id="Seg_2988" n="e" s="T1457">соседнем </ts>
               <ts e="T1459" id="Seg_2990" n="e" s="T1458">доме </ts>
               <ts e="T1460" id="Seg_2992" n="e" s="T1459">что </ts>
               <ts e="T1461" id="Seg_2994" n="e" s="T1460">ли </ts>
               <ts e="T1462" id="Seg_2996" n="e" s="T1461">или </ts>
               <ts e="T1463" id="Seg_2998" n="e" s="T1462">(как </ts>
               <ts e="T1464" id="Seg_3000" n="e" s="T1463">там). </ts>
            </ts>
            <ts e="T1486" id="Seg_3001" n="sc" s="T1481">
               <ts e="T1484" id="Seg_3003" n="e" s="T1481">Ага. </ts>
               <ts e="T1486" id="Seg_3005" n="e" s="T1484">Ага. </ts>
            </ts>
            <ts e="T1490" id="Seg_3006" n="sc" s="T1488">
               <ts e="T1490" id="Seg_3008" n="e" s="T1488">Ага. </ts>
            </ts>
            <ts e="T1493" id="Seg_3009" n="sc" s="T1492">
               <ts e="T1493" id="Seg_3011" n="e" s="T1492">Ага. </ts>
            </ts>
            <ts e="T1496" id="Seg_3012" n="sc" s="T1494">
               <ts e="T1496" id="Seg_3014" n="e" s="T1494">Ага. </ts>
            </ts>
            <ts e="T1513" id="Seg_3015" n="sc" s="T1500">
               <ts e="T1501" id="Seg_3017" n="e" s="T1500">Вот-вот-вот. </ts>
               <ts e="T1503" id="Seg_3019" n="e" s="T1501">Нет, </ts>
               <ts e="T1505" id="Seg_3021" n="e" s="T1503">я </ts>
               <ts e="T1507" id="Seg_3023" n="e" s="T1505">тот </ts>
               <ts e="T1508" id="Seg_3025" n="e" s="T1507">магазин </ts>
               <ts e="T1510" id="Seg_3027" n="e" s="T1508">вообще </ts>
               <ts e="T1512" id="Seg_3029" n="e" s="T1510">не </ts>
               <ts e="T1513" id="Seg_3031" n="e" s="T1512">знаю. </ts>
            </ts>
            <ts e="T1521" id="Seg_3032" n="sc" s="T1519">
               <ts e="T1520" id="Seg_3034" n="e" s="T1519">Нет, </ts>
               <ts e="T1521" id="Seg_3036" n="e" s="T1520">нет-нет-нет-нет. </ts>
            </ts>
            <ts e="T1541" id="Seg_3037" n="sc" s="T1539">
               <ts e="T1541" id="Seg_3039" n="e" s="T1539">Mm, mm. </ts>
            </ts>
            <ts e="T1545" id="Seg_3040" n="sc" s="T1542">
               <ts e="T1543" id="Seg_3042" n="e" s="T1542">(Что,) </ts>
               <ts e="T1544" id="Seg_3044" n="e" s="T1543">кирпичное </ts>
               <ts e="T1545" id="Seg_3046" n="e" s="T1544">здание? </ts>
            </ts>
            <ts e="T1550" id="Seg_3047" n="sc" s="T1546">
               <ts e="T1548" id="Seg_3049" n="e" s="T1546">Или </ts>
               <ts e="T1550" id="Seg_3051" n="e" s="T1548">деревянное? </ts>
            </ts>
            <ts e="T1556" id="Seg_3052" n="sc" s="T1554">
               <ts e="T1556" id="Seg_3054" n="e" s="T1554">Mhm. </ts>
            </ts>
            <ts e="T1605" id="Seg_3055" n="sc" s="T1604">
               <ts e="T1605" id="Seg_3057" n="e" s="T1604">Ага. </ts>
            </ts>
            <ts e="T1643" id="Seg_3058" n="sc" s="T1642">
               <ts e="T1643" id="Seg_3060" n="e" s="T1642">Ага. </ts>
            </ts>
            <ts e="T1661" id="Seg_3061" n="sc" s="T1659">
               <ts e="T1660" id="Seg_3063" n="e" s="T1659">Ага. </ts>
               <ts e="T1661" id="Seg_3065" n="e" s="T1660">Ага. </ts>
            </ts>
            <ts e="T1689" id="Seg_3066" n="sc" s="T1672">
               <ts e="T1689" id="Seg_3068" n="e" s="T1672">Я так "Мурачёв" эту фамилию в общем помню, а человека такого ((…)) не знаю. </ts>
            </ts>
            <ts e="T1716" id="Seg_3069" n="sc" s="T1710">
               <ts e="T1711" id="Seg_3071" n="e" s="T1710">А </ts>
               <ts e="T1712" id="Seg_3073" n="e" s="T1711">сейчас </ts>
               <ts e="T1713" id="Seg_3075" n="e" s="T1712">давай </ts>
               <ts e="T1714" id="Seg_3077" n="e" s="T1713">это </ts>
               <ts e="T1715" id="Seg_3079" n="e" s="T1714">всё </ts>
               <ts e="T1716" id="Seg_3081" n="e" s="T1715">по-камасински. </ts>
            </ts>
            <ts e="T1737" id="Seg_3082" n="sc" s="T1717">
               <ts e="T1718" id="Seg_3084" n="e" s="T1717">А </ts>
               <ts e="T1719" id="Seg_3086" n="e" s="T1718">то </ts>
               <ts e="T1720" id="Seg_3088" n="e" s="T1719">мы </ts>
               <ts e="T1721" id="Seg_3090" n="e" s="T1720">заговоримся </ts>
               <ts e="T1722" id="Seg_3092" n="e" s="T1721">и </ts>
               <ts e="T1723" id="Seg_3094" n="e" s="T1722">забудем, </ts>
               <ts e="T1724" id="Seg_3096" n="e" s="T1723">что </ts>
               <ts e="T1725" id="Seg_3098" n="e" s="T1724">говорили. </ts>
               <ts e="T1726" id="Seg_3100" n="e" s="T1725">Вот </ts>
               <ts e="T1727" id="Seg_3102" n="e" s="T1726">сначала </ts>
               <ts e="T1728" id="Seg_3104" n="e" s="T1727">насчет </ts>
               <ts e="T1729" id="Seg_3106" n="e" s="T1728">спирта, </ts>
               <ts e="T1730" id="Seg_3108" n="e" s="T1729">что </ts>
               <ts e="T1731" id="Seg_3110" n="e" s="T1730">продают </ts>
               <ts e="T1732" id="Seg_3112" n="e" s="T1731">еще </ts>
               <ts e="T1733" id="Seg_3114" n="e" s="T1732">такой </ts>
               <ts e="T1734" id="Seg_3116" n="e" s="T1733">спирт, </ts>
               <ts e="T1735" id="Seg_3118" n="e" s="T1734">и </ts>
               <ts e="T1736" id="Seg_3120" n="e" s="T1735">для </ts>
               <ts e="T1737" id="Seg_3122" n="e" s="T1736">здоровья… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T3" id="Seg_3123" s="T1">PKZ_19700819_09343-1bz.KA.001 (001)</ta>
            <ta e="T5" id="Seg_3124" s="T3">PKZ_19700819_09343-1bz.KA.002 (002)</ta>
            <ta e="T14" id="Seg_3125" s="T6">PKZ_19700819_09343-1bz.KA.003 (003)</ta>
            <ta e="T22" id="Seg_3126" s="T14">PKZ_19700819_09343-1bz.KA.004 (005)</ta>
            <ta e="T28" id="Seg_3127" s="T23">PKZ_19700819_09343-1bz.KA.005 (006)</ta>
            <ta e="T34" id="Seg_3128" s="T32">PKZ_19700819_09343-1bz.KA.006 (008)</ta>
            <ta e="T265" id="Seg_3129" s="T264">PKZ_19700819_09343-1bz.KA.007 (040)</ta>
            <ta e="T278" id="Seg_3130" s="T270">PKZ_19700819_09343-1bz.KA.008 (042)</ta>
            <ta e="T282" id="Seg_3131" s="T278">PKZ_19700819_09343-1bz.KA.009 (043)</ta>
            <ta e="T285" id="Seg_3132" s="T283">PKZ_19700819_09343-1bz.KA.010 (044)</ta>
            <ta e="T293" id="Seg_3133" s="T289">PKZ_19700819_09343-1bz.KA.011 (046)</ta>
            <ta e="T299" id="Seg_3134" s="T296">PKZ_19700819_09343-1bz.KA.012 (048)</ta>
            <ta e="T303" id="Seg_3135" s="T300">PKZ_19700819_09343-1bz.KA.013 (049)</ta>
            <ta e="T322" id="Seg_3136" s="T311">PKZ_19700819_09343-1bz.KA.014 (051)</ta>
            <ta e="T329" id="Seg_3137" s="T324">PKZ_19700819_09343-1bz.KA.015 (053)</ta>
            <ta e="T340" id="Seg_3138" s="T331">PKZ_19700819_09343-1bz.KA.016 (054)</ta>
            <ta e="T347" id="Seg_3139" s="T341">PKZ_19700819_09343-1bz.KA.017 (055)</ta>
            <ta e="T353" id="Seg_3140" s="T349">PKZ_19700819_09343-1bz.KA.018 (057)</ta>
            <ta e="T376" id="Seg_3141" s="T364">PKZ_19700819_09343-1bz.KA.019 (061)</ta>
            <ta e="T384" id="Seg_3142" s="T381">PKZ_19700819_09343-1bz.KA.020 (063)</ta>
            <ta e="T392" id="Seg_3143" s="T385">PKZ_19700819_09343-1bz.KA.021 (064)</ta>
            <ta e="T396" id="Seg_3144" s="T392">PKZ_19700819_09343-1bz.KA.022 (065)</ta>
            <ta e="T407" id="Seg_3145" s="T397">PKZ_19700819_09343-1bz.KA.023 (066)</ta>
            <ta e="T420" id="Seg_3146" s="T408">PKZ_19700819_09343-1bz.KA.024 (067)</ta>
            <ta e="T424" id="Seg_3147" s="T423">PKZ_19700819_09343-1bz.KA.025 (069)</ta>
            <ta e="T434" id="Seg_3148" s="T433">PKZ_19700819_09343-1bz.KA.026 (075)</ta>
            <ta e="T446" id="Seg_3149" s="T437">PKZ_19700819_09343-1bz.KA.027 (075)</ta>
            <ta e="T457" id="Seg_3150" s="T456">PKZ_19700819_09343-1bz.KA.028 (077)</ta>
            <ta e="T477" id="Seg_3151" s="T466">PKZ_19700819_09343-1bz.KA.029 (080)</ta>
            <ta e="T489" id="Seg_3152" s="T478">PKZ_19700819_09343-1bz.KA.030 (081)</ta>
            <ta e="T496" id="Seg_3153" s="T490">PKZ_19700819_09343-1bz.KA.031 (082)</ta>
            <ta e="T507" id="Seg_3154" s="T500">PKZ_19700819_09343-1bz.KA.032 (084)</ta>
            <ta e="T526" id="Seg_3155" s="T507">PKZ_19700819_09343-1bz.KA.033 (085)</ta>
            <ta e="T557" id="Seg_3156" s="T541">PKZ_19700819_09343-1bz.KA.034 (089)</ta>
            <ta e="T569" id="Seg_3157" s="T557">PKZ_19700819_09343-1bz.KA.035 (091)</ta>
            <ta e="T575" id="Seg_3158" s="T573">PKZ_19700819_09343-1bz.KA.036 (093)</ta>
            <ta e="T590" id="Seg_3159" s="T586">PKZ_19700819_09343-1bz.KA.037 (096)</ta>
            <ta e="T597" id="Seg_3160" s="T591">PKZ_19700819_09343-1bz.KA.038 (097)</ta>
            <ta e="T853" id="Seg_3161" s="T839">PKZ_19700819_09343-1bz.KA.039 (128)</ta>
            <ta e="T857" id="Seg_3162" s="T855">PKZ_19700819_09343-1bz.KA.040 (130)</ta>
            <ta e="T889" id="Seg_3163" s="T887">PKZ_19700819_09343-1bz.KA.041 (136)</ta>
            <ta e="T907" id="Seg_3164" s="T906">PKZ_19700819_09343-1bz.KA.042 (136)</ta>
            <ta e="T911" id="Seg_3165" s="T908">PKZ_19700819_09343-1bz.KA.043 (137)</ta>
            <ta e="T934" id="Seg_3166" s="T919">PKZ_19700819_09343-1bz.KA.044 (140)</ta>
            <ta e="T944" id="Seg_3167" s="T935">PKZ_19700819_09343-1bz.KA.045 (142)</ta>
            <ta e="T948" id="Seg_3168" s="T944">PKZ_19700819_09343-1bz.KA.046 (143)</ta>
            <ta e="T956" id="Seg_3169" s="T950">PKZ_19700819_09343-1bz.KA.047 (145)</ta>
            <ta e="T964" id="Seg_3170" s="T958">PKZ_19700819_09343-1bz.KA.048 (147)</ta>
            <ta e="T968" id="Seg_3171" s="T965">PKZ_19700819_09343-1bz.KA.049 (148)</ta>
            <ta e="T1060" id="Seg_3172" s="T1040">PKZ_19700819_09343-1bz.KA.050 (161)</ta>
            <ta e="T1067" id="Seg_3173" s="T1060">PKZ_19700819_09343-1bz.KA.051 (162)</ta>
            <ta e="T1072" id="Seg_3174" s="T1067">PKZ_19700819_09343-1bz.KA.052 (164)</ta>
            <ta e="T1109" id="Seg_3175" s="T1101">PKZ_19700819_09343-1bz.KA.053 (170)</ta>
            <ta e="T1117" id="Seg_3176" s="T1109">PKZ_19700819_09343-1bz.KA.054 (171)</ta>
            <ta e="T1130" id="Seg_3177" s="T1118">PKZ_19700819_09343-1bz.KA.055 (172)</ta>
            <ta e="T1137" id="Seg_3178" s="T1130">PKZ_19700819_09343-1bz.KA.056 (173)</ta>
            <ta e="T1149" id="Seg_3179" s="T1145">PKZ_19700819_09343-1bz.KA.057 (177)</ta>
            <ta e="T1157" id="Seg_3180" s="T1149">PKZ_19700819_09343-1bz.KA.058 (178)</ta>
            <ta e="T1162" id="Seg_3181" s="T1158">PKZ_19700819_09343-1bz.KA.059 (179)</ta>
            <ta e="T1166" id="Seg_3182" s="T1163">PKZ_19700819_09343-1bz.KA.060 (180)</ta>
            <ta e="T1172" id="Seg_3183" s="T1166">PKZ_19700819_09343-1bz.KA.061 (181)</ta>
            <ta e="T1176" id="Seg_3184" s="T1173">PKZ_19700819_09343-1bz.KA.062 (182)</ta>
            <ta e="T1247" id="Seg_3185" s="T1238">PKZ_19700819_09343-1bz.KA.063 (192)</ta>
            <ta e="T1289" id="Seg_3186" s="T1288">PKZ_19700819_09343-1bz.KA.064 (198)</ta>
            <ta e="T1302" id="Seg_3187" s="T1290">PKZ_19700819_09343-1bz.KA.065 (199)</ta>
            <ta e="T1307" id="Seg_3188" s="T1303">PKZ_19700819_09343-1bz.KA.066 (200)</ta>
            <ta e="T1316" id="Seg_3189" s="T1308">PKZ_19700819_09343-1bz.KA.067 (201)</ta>
            <ta e="T1334" id="Seg_3190" s="T1317">PKZ_19700819_09343-1bz.KA.068 (202)</ta>
            <ta e="T1339" id="Seg_3191" s="T1335">PKZ_19700819_09343-1bz.KA.069 (203)</ta>
            <ta e="T1344" id="Seg_3192" s="T1340">PKZ_19700819_09343-1bz.KA.070 (203)</ta>
            <ta e="T1346" id="Seg_3193" s="T1345">PKZ_19700819_09343-1bz.KA.071 (205)</ta>
            <ta e="T1351" id="Seg_3194" s="T1350">PKZ_19700819_09343-1bz.KA.072 (205)</ta>
            <ta e="T1370" id="Seg_3195" s="T1368">PKZ_19700819_09343-1bz.KA.073 (205)</ta>
            <ta e="T1381" id="Seg_3196" s="T1378">PKZ_19700819_09343-1bz.KA.074 (211)</ta>
            <ta e="T1419" id="Seg_3197" s="T1405">PKZ_19700819_09343-1bz.KA.075 (215)</ta>
            <ta e="T1432" id="Seg_3198" s="T1420">PKZ_19700819_09343-1bz.KA.076 (216)</ta>
            <ta e="T1446" id="Seg_3199" s="T1435">PKZ_19700819_09343-1bz.KA.077 (218)</ta>
            <ta e="T1464" id="Seg_3200" s="T1447">PKZ_19700819_09343-1bz.KA.078 (219)</ta>
            <ta e="T1484" id="Seg_3201" s="T1481">PKZ_19700819_09343-1bz.KA.079 (223)</ta>
            <ta e="T1486" id="Seg_3202" s="T1484">PKZ_19700819_09343-1bz.KA.080 (224)</ta>
            <ta e="T1490" id="Seg_3203" s="T1488">PKZ_19700819_09343-1bz.KA.081 (224)</ta>
            <ta e="T1493" id="Seg_3204" s="T1492">PKZ_19700819_09343-1bz.KA.082 (224)</ta>
            <ta e="T1496" id="Seg_3205" s="T1494">PKZ_19700819_09343-1bz.KA.083 (224)</ta>
            <ta e="T1501" id="Seg_3206" s="T1500">PKZ_19700819_09343-1bz.KA.084 (226)</ta>
            <ta e="T1513" id="Seg_3207" s="T1501">PKZ_19700819_09343-1bz.KA.085 (227)</ta>
            <ta e="T1521" id="Seg_3208" s="T1519">PKZ_19700819_09343-1bz.KA.086 (229)</ta>
            <ta e="T1541" id="Seg_3209" s="T1539">PKZ_19700819_09343-1bz.KA.087 (229)</ta>
            <ta e="T1545" id="Seg_3210" s="T1542">PKZ_19700819_09343-1bz.KA.088 (233)</ta>
            <ta e="T1550" id="Seg_3211" s="T1546">PKZ_19700819_09343-1bz.KA.089 (235)</ta>
            <ta e="T1556" id="Seg_3212" s="T1554">PKZ_19700819_09343-1bz.KA.090 (235)</ta>
            <ta e="T1605" id="Seg_3213" s="T1604">PKZ_19700819_09343-1bz.KA.091 (242)</ta>
            <ta e="T1643" id="Seg_3214" s="T1642">PKZ_19700819_09343-1bz.KA.092 (248)</ta>
            <ta e="T1660" id="Seg_3215" s="T1659">PKZ_19700819_09343-1bz.KA.093 (251)</ta>
            <ta e="T1661" id="Seg_3216" s="T1660">PKZ_19700819_09343-1bz.KA.094 (252)</ta>
            <ta e="T1689" id="Seg_3217" s="T1672">PKZ_19700819_09343-1bz.KA.095 (254)</ta>
            <ta e="T1716" id="Seg_3218" s="T1710">PKZ_19700819_09343-1bz.KA.096 (257)</ta>
            <ta e="T1725" id="Seg_3219" s="T1717">PKZ_19700819_09343-1bz.KA.097 (258)</ta>
            <ta e="T1737" id="Seg_3220" s="T1725">PKZ_19700819_09343-1bz.KA.098 (259)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T3" id="Seg_3221" s="T1">На каком? </ta>
            <ta e="T5" id="Seg_3222" s="T3">На любом. </ta>
            <ta e="T14" id="Seg_3223" s="T6">Или сначала можешь по-русски рассказать. </ta>
            <ta e="T22" id="Seg_3224" s="T14">Расскажи сначала по-русски, а потом уже по-камасински. </ta>
            <ta e="T28" id="Seg_3225" s="T23">А я послушаю, мне интересно! </ta>
            <ta e="T34" id="Seg_3226" s="T32">Идет, идет. </ta>
            <ta e="T265" id="Seg_3227" s="T264">((…)). </ta>
            <ta e="T278" id="Seg_3228" s="T270">Нет, насчет счетовода ты (с-) вообще не рассказывала. </ta>
            <ta e="T282" id="Seg_3229" s="T278">Как это было, значит? </ta>
            <ta e="T285" id="Seg_3230" s="T283">Пока расскажи. </ta>
            <ta e="T293" id="Seg_3231" s="T289">Да, насчет счетовода? </ta>
            <ta e="T299" id="Seg_3232" s="T296">Ага. </ta>
            <ta e="T303" id="Seg_3233" s="T300">Ага. </ta>
            <ta e="T322" id="Seg_3234" s="T311">Ну да, это насчет Паши и Шуры, а вот счетовод тот? </ta>
            <ta e="T329" id="Seg_3235" s="T324">У которого ((…)). </ta>
            <ta e="T340" id="Seg_3236" s="T331">Ага, это тоже есть, да? </ta>
            <ta e="T347" id="Seg_3237" s="T341">А, а, она с (ней), ага. </ta>
            <ta e="T353" id="Seg_3238" s="T349">Все, ясно. </ta>
            <ta e="T376" id="Seg_3239" s="T364">Все, слушает, слушает, говори что хочешь. </ta>
            <ta e="T384" id="Seg_3240" s="T381">Нет. </ta>
            <ta e="T392" id="Seg_3241" s="T385">Да, значит, насчет счетовода по-русски не рассказала. </ta>
            <ta e="T396" id="Seg_3242" s="T392">Насчет счетовода можно сказать. </ta>
            <ta e="T407" id="Seg_3243" s="T397">Вот, как руки отморозил, это пока ты не говорила по-русски. </ta>
            <ta e="T420" id="Seg_3244" s="T408">А рассказала, как разошлись и судили, вот это ты уже по-русски говорила. </ta>
            <ta e="T424" id="Seg_3245" s="T423">Ага. </ta>
            <ta e="T434" id="Seg_3246" s="T433">Так…</ta>
            <ta e="T446" id="Seg_3247" s="T437">По-русски, только ты вот эту (часть)… </ta>
            <ta e="T457" id="Seg_3248" s="T456">Ага. </ta>
            <ta e="T477" id="Seg_3249" s="T466">Ну хорошо, все равно будет понятно, люди умные — поймем. </ta>
            <ta e="T489" id="Seg_3250" s="T478">Ну, что-нибудь другое расскажи о деревенской жизни, что ж, все равно. </ta>
            <ta e="T496" id="Seg_3251" s="T490">Что у вас там еще нового? </ta>
            <ta e="T507" id="Seg_3252" s="T500">Кто у вас там начальствует сейчас? </ta>
            <ta e="T526" id="Seg_3253" s="T507">Я помню вот председателя сельсовета там мы какие-то печати доставали у (нее=) его и все такое. </ta>
            <ta e="T557" id="Seg_3254" s="T541">Как, как, я знаю его, знаю, что он был. </ta>
            <ta e="T569" id="Seg_3255" s="T557">Нет, нет, нет, расскажи как раз. </ta>
            <ta e="T575" id="Seg_3256" s="T573">А-а-а… </ta>
            <ta e="T590" id="Seg_3257" s="T586">Добрый был человек. </ta>
            <ta e="T597" id="Seg_3258" s="T591">Вот расскажи, пожалуйста. </ta>
            <ta e="T853" id="Seg_3259" s="T839">Так я уж не помню сейчас, знал я его или нет. </ta>
            <ta e="T857" id="Seg_3260" s="T855">Ага. </ta>
            <ta e="T889" id="Seg_3261" s="T887">Mm, mm, mm, mm.</ta>
            <ta e="T907" id="Seg_3262" s="T906">Так. </ta>
            <ta e="T911" id="Seg_3263" s="T908">((…)). </ta>
            <ta e="T934" id="Seg_3264" s="T919">Ну, это мы еще все поговорим, а может ты пока вот насчет Загороднюка? </ta>
            <ta e="T944" id="Seg_3265" s="T935">На камасинском всё это дело, как с ним было. </ta>
            <ta e="T948" id="Seg_3266" s="T944">Как вы там ехали. </ta>
            <ta e="T956" id="Seg_3267" s="T950">Всё слушают, всё записано. </ta>
            <ta e="T964" id="Seg_3268" s="T958">Записано, по-русски сейчас всё записали. </ta>
            <ta e="T968" id="Seg_3269" s="T965">Теперь только по-камасински. </ta>
            <ta e="T1060" id="Seg_3270" s="T1040">Ты еще рассказала насчет того, как ты сказала ему что "Я тебе как сыну скажу, что богу поеду помолиться". </ta>
            <ta e="T1067" id="Seg_3271" s="T1060">Вот еще по-камасински не рассказала. </ta>
            <ta e="T1072" id="Seg_3272" s="T1067">Пожалуйста, пожалуйста, всё слушает, пишет. </ta>
            <ta e="T1109" id="Seg_3273" s="T1101">Ну тут мы начали и о бригадире говорить. </ta>
            <ta e="T1117" id="Seg_3274" s="T1109">Нет, а вот что мы не рассказали еще. </ta>
            <ta e="T1130" id="Seg_3275" s="T1118">Это мы только тут говорили, что он помер два года тому назад. </ta>
            <ta e="T1137" id="Seg_3276" s="T1130">А тут еще не рассказали. </ta>
            <ta e="T1149" id="Seg_3277" s="T1145">Ага, ага. </ta>
            <ta e="T1157" id="Seg_3278" s="T1149">А, это было рассказано, значит, все в порядке. </ta>
            <ta e="T1162" id="Seg_3279" s="T1158">Ну, может насчет бригадира? </ta>
            <ta e="T1166" id="Seg_3280" s="T1163">Этого Августа, немца. </ta>
            <ta e="T1172" id="Seg_3281" s="T1166">((NOISE)) Я его не помню, кажется. </ta>
            <ta e="T1176" id="Seg_3282" s="T1173">По-моему, не помню. </ta>
            <ta e="T1247" id="Seg_3283" s="T1238">По-русски еще все это, ты сейчас много нового рассказала. </ta>
            <ta e="T1289" id="Seg_3284" s="T1288">Так. </ta>
            <ta e="T1302" id="Seg_3285" s="T1290">Вот что меня интересует, я помню ведь в Абалакове был такой магазин. </ta>
            <ta e="T1307" id="Seg_3286" s="T1303">И всякое там продавали. </ta>
            <ta e="T1316" id="Seg_3287" s="T1308">Вот что там сейчас продается, ты не расскажешь? </ta>
            <ta e="T1334" id="Seg_3288" s="T1317">Интересно, я помню, тогда, когда мы последний раз там были, даже такой спирт продавали, 69 процентов алкоголя. </ta>
            <ta e="T1339" id="Seg_3289" s="T1335">Страшный такой был напиток. </ta>
            <ta e="T1344" id="Seg_3290" s="T1340">((LAUGH)). </ta>
            <ta e="T1346" id="Seg_3291" s="T1345">Ага. </ta>
            <ta e="T1351" id="Seg_3292" s="T1350">Mhm. </ta>
            <ta e="T1370" id="Seg_3293" s="T1368">Mhm. </ta>
            <ta e="T1381" id="Seg_3294" s="T1378">Так, ну… </ta>
            <ta e="T1419" id="Seg_3295" s="T1405">Нет, я не знаю, я помню когда магазин был в самом центре стоял там. </ta>
            <ta e="T1432" id="Seg_3296" s="T1420">На той же улице вот, близко Шуры, (не с-) не очень далеко. </ta>
            <ta e="T1446" id="Seg_3297" s="T1435">До клуба еще, если от Шуры пойти, значит, на правой стороне. </ta>
            <ta e="T1464" id="Seg_3298" s="T1447">Был дом такой, еще продавщица-то близко жила, рядом кажется, в соседнем доме что ли или (как там). </ta>
            <ta e="T1484" id="Seg_3299" s="T1481">Ага. </ta>
            <ta e="T1486" id="Seg_3300" s="T1484">Ага. </ta>
            <ta e="T1490" id="Seg_3301" s="T1488">Ага. </ta>
            <ta e="T1493" id="Seg_3302" s="T1492">Ага. </ta>
            <ta e="T1496" id="Seg_3303" s="T1494">Ага. </ta>
            <ta e="T1501" id="Seg_3304" s="T1500">Вот-вот-вот. </ta>
            <ta e="T1513" id="Seg_3305" s="T1501">Нет, я тот магазин вообще не знаю. </ta>
            <ta e="T1521" id="Seg_3306" s="T1519">Нет, нет-нет-нет-нет. </ta>
            <ta e="T1541" id="Seg_3307" s="T1539">Mm, mm. </ta>
            <ta e="T1545" id="Seg_3308" s="T1542">(Что,) кирпичное здание? </ta>
            <ta e="T1550" id="Seg_3309" s="T1546">Или деревянное? </ta>
            <ta e="T1556" id="Seg_3310" s="T1554">Mhm. </ta>
            <ta e="T1605" id="Seg_3311" s="T1604">Ага. </ta>
            <ta e="T1643" id="Seg_3312" s="T1642">Ага. </ta>
            <ta e="T1660" id="Seg_3313" s="T1659">Ага. </ta>
            <ta e="T1661" id="Seg_3314" s="T1660">Ага. </ta>
            <ta e="T1689" id="Seg_3315" s="T1672">Я так "Мурачёв" эту фамилию в общем помню, а человека такого ((…)) не знаю. </ta>
            <ta e="T1716" id="Seg_3316" s="T1710">А сейчас давай это всё по-камасински. </ta>
            <ta e="T1725" id="Seg_3317" s="T1717">А то мы заговоримся и забудем, что говорили. </ta>
            <ta e="T1737" id="Seg_3318" s="T1725">Вот сначала насчет спирта, что продают еще такой спирт, и для здоровья… </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T3" id="Seg_3319" s="T1">RUS:ext</ta>
            <ta e="T5" id="Seg_3320" s="T3">RUS:ext</ta>
            <ta e="T14" id="Seg_3321" s="T6">RUS:ext</ta>
            <ta e="T22" id="Seg_3322" s="T14">RUS:ext</ta>
            <ta e="T28" id="Seg_3323" s="T23">RUS:ext</ta>
            <ta e="T34" id="Seg_3324" s="T32">RUS:ext</ta>
            <ta e="T265" id="Seg_3325" s="T264">FIN:ext</ta>
            <ta e="T278" id="Seg_3326" s="T270">RUS:ext</ta>
            <ta e="T282" id="Seg_3327" s="T278">RUS:ext</ta>
            <ta e="T285" id="Seg_3328" s="T283">RUS:ext</ta>
            <ta e="T293" id="Seg_3329" s="T289">RUS:ext</ta>
            <ta e="T299" id="Seg_3330" s="T296">RUS:ext</ta>
            <ta e="T303" id="Seg_3331" s="T300">RUS:ext</ta>
            <ta e="T322" id="Seg_3332" s="T311">RUS:ext</ta>
            <ta e="T329" id="Seg_3333" s="T324">RUS:ext</ta>
            <ta e="T340" id="Seg_3334" s="T331">RUS:ext</ta>
            <ta e="T347" id="Seg_3335" s="T341">RUS:ext</ta>
            <ta e="T353" id="Seg_3336" s="T349">RUS:ext</ta>
            <ta e="T376" id="Seg_3337" s="T364">RUS:ext</ta>
            <ta e="T384" id="Seg_3338" s="T381">RUS:ext</ta>
            <ta e="T392" id="Seg_3339" s="T385">RUS:ext</ta>
            <ta e="T396" id="Seg_3340" s="T392">RUS:ext</ta>
            <ta e="T407" id="Seg_3341" s="T397">RUS:ext</ta>
            <ta e="T420" id="Seg_3342" s="T408">RUS:ext</ta>
            <ta e="T424" id="Seg_3343" s="T423">RUS:ext</ta>
            <ta e="T434" id="Seg_3344" s="T433">RUS:ext</ta>
            <ta e="T446" id="Seg_3345" s="T437">RUS:ext</ta>
            <ta e="T457" id="Seg_3346" s="T456">RUS:ext</ta>
            <ta e="T477" id="Seg_3347" s="T466">RUS:ext</ta>
            <ta e="T489" id="Seg_3348" s="T478">RUS:ext</ta>
            <ta e="T496" id="Seg_3349" s="T490">RUS:ext</ta>
            <ta e="T507" id="Seg_3350" s="T500">RUS:ext</ta>
            <ta e="T526" id="Seg_3351" s="T507">RUS:ext</ta>
            <ta e="T557" id="Seg_3352" s="T541">RUS:ext</ta>
            <ta e="T569" id="Seg_3353" s="T557">RUS:ext</ta>
            <ta e="T575" id="Seg_3354" s="T573">RUS:ext</ta>
            <ta e="T590" id="Seg_3355" s="T586">RUS:ext</ta>
            <ta e="T597" id="Seg_3356" s="T591">RUS:ext</ta>
            <ta e="T853" id="Seg_3357" s="T839">RUS:ext</ta>
            <ta e="T857" id="Seg_3358" s="T855">RUS:ext</ta>
            <ta e="T907" id="Seg_3359" s="T906">RUS:ext</ta>
            <ta e="T934" id="Seg_3360" s="T919">RUS:ext</ta>
            <ta e="T944" id="Seg_3361" s="T935">RUS:ext</ta>
            <ta e="T948" id="Seg_3362" s="T944">RUS:ext</ta>
            <ta e="T956" id="Seg_3363" s="T950">RUS:ext</ta>
            <ta e="T964" id="Seg_3364" s="T958">RUS:ext</ta>
            <ta e="T968" id="Seg_3365" s="T965">RUS:ext</ta>
            <ta e="T1060" id="Seg_3366" s="T1040">RUS:ext</ta>
            <ta e="T1067" id="Seg_3367" s="T1060">RUS:ext</ta>
            <ta e="T1072" id="Seg_3368" s="T1067">RUS:ext</ta>
            <ta e="T1109" id="Seg_3369" s="T1101">RUS:ext</ta>
            <ta e="T1117" id="Seg_3370" s="T1109">RUS:ext</ta>
            <ta e="T1130" id="Seg_3371" s="T1118">RUS:ext</ta>
            <ta e="T1137" id="Seg_3372" s="T1130">RUS:ext</ta>
            <ta e="T1149" id="Seg_3373" s="T1145">RUS:ext</ta>
            <ta e="T1157" id="Seg_3374" s="T1149">RUS:ext</ta>
            <ta e="T1162" id="Seg_3375" s="T1158">RUS:ext</ta>
            <ta e="T1166" id="Seg_3376" s="T1163">RUS:ext</ta>
            <ta e="T1172" id="Seg_3377" s="T1166">RUS:ext</ta>
            <ta e="T1176" id="Seg_3378" s="T1173">RUS:ext</ta>
            <ta e="T1247" id="Seg_3379" s="T1238">RUS:ext</ta>
            <ta e="T1289" id="Seg_3380" s="T1288">RUS:ext</ta>
            <ta e="T1302" id="Seg_3381" s="T1290">RUS:ext</ta>
            <ta e="T1307" id="Seg_3382" s="T1303">RUS:ext</ta>
            <ta e="T1316" id="Seg_3383" s="T1308">RUS:ext</ta>
            <ta e="T1334" id="Seg_3384" s="T1317">RUS:ext</ta>
            <ta e="T1339" id="Seg_3385" s="T1335">RUS:ext</ta>
            <ta e="T1346" id="Seg_3386" s="T1345">RUS:ext</ta>
            <ta e="T1381" id="Seg_3387" s="T1378">RUS:ext</ta>
            <ta e="T1419" id="Seg_3388" s="T1405">RUS:ext</ta>
            <ta e="T1432" id="Seg_3389" s="T1420">RUS:ext</ta>
            <ta e="T1446" id="Seg_3390" s="T1435">RUS:ext</ta>
            <ta e="T1464" id="Seg_3391" s="T1447">RUS:ext</ta>
            <ta e="T1484" id="Seg_3392" s="T1481">RUS:ext</ta>
            <ta e="T1486" id="Seg_3393" s="T1484">RUS:ext</ta>
            <ta e="T1490" id="Seg_3394" s="T1488">RUS:ext</ta>
            <ta e="T1493" id="Seg_3395" s="T1492">RUS:ext</ta>
            <ta e="T1496" id="Seg_3396" s="T1494">RUS:ext</ta>
            <ta e="T1501" id="Seg_3397" s="T1500">RUS:ext</ta>
            <ta e="T1513" id="Seg_3398" s="T1501">RUS:ext</ta>
            <ta e="T1521" id="Seg_3399" s="T1519">RUS:ext</ta>
            <ta e="T1545" id="Seg_3400" s="T1542">RUS:ext</ta>
            <ta e="T1550" id="Seg_3401" s="T1546">RUS:ext</ta>
            <ta e="T1605" id="Seg_3402" s="T1604">RUS:ext</ta>
            <ta e="T1643" id="Seg_3403" s="T1642">RUS:ext</ta>
            <ta e="T1660" id="Seg_3404" s="T1659">RUS:ext</ta>
            <ta e="T1661" id="Seg_3405" s="T1660">RUS:ext</ta>
            <ta e="T1689" id="Seg_3406" s="T1672">RUS:ext</ta>
            <ta e="T1716" id="Seg_3407" s="T1710">RUS:ext</ta>
            <ta e="T1725" id="Seg_3408" s="T1717">RUS:ext</ta>
            <ta e="T1737" id="Seg_3409" s="T1725">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T376" id="Seg_3410" s="T364">Все, [магнитофон] слушает, слушает, говори что хочешь. </ta>
            <ta e="T889" id="Seg_3411" s="T887">Мм, мм, мм, мм.</ta>
            <ta e="T911" id="Seg_3412" s="T908">[?]</ta>
            <ta e="T1351" id="Seg_3413" s="T1350">Мм.</ta>
            <ta e="T1370" id="Seg_3414" s="T1368">Мм.</ta>
            <ta e="T1541" id="Seg_3415" s="T1539">Мм, мм.</ta>
            <ta e="T1556" id="Seg_3416" s="T1554">Мм.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T3" id="Seg_3417" s="T1">On which?</ta>
            <ta e="T5" id="Seg_3418" s="T3">On any.</ta>
            <ta e="T14" id="Seg_3419" s="T6">Or you can tell me in Russian first.</ta>
            <ta e="T22" id="Seg_3420" s="T14">Tell me first in Russian, and then in Kamas.</ta>
            <ta e="T28" id="Seg_3421" s="T23">And I'll listen, it is interesting for me!</ta>
            <ta e="T34" id="Seg_3422" s="T32">It is running.</ta>
            <ta e="T278" id="Seg_3423" s="T270">No, you didn’t talk about the accountant at all.</ta>
            <ta e="T282" id="Seg_3424" s="T278">How was it meant?</ta>
            <ta e="T285" id="Seg_3425" s="T283">Tell me so far.</ta>
            <ta e="T293" id="Seg_3426" s="T289">Yes, what about the accountant?</ta>
            <ta e="T299" id="Seg_3427" s="T296">Yes.</ta>
            <ta e="T303" id="Seg_3428" s="T300">Yes.</ta>
            <ta e="T322" id="Seg_3429" s="T311">Well, yes, this is about Pasha and Shura, but is that the accountant?</ta>
            <ta e="T329" id="Seg_3430" s="T324">Which one (…).</ta>
            <ta e="T340" id="Seg_3431" s="T331">Yeah, that too, right?</ta>
            <ta e="T347" id="Seg_3432" s="T341">Ah, ah, she is with (her), yeah.</ta>
            <ta e="T353" id="Seg_3433" s="T349">All clear.</ta>
            <ta e="T376" id="Seg_3434" s="T364">Everything, [tape recorder] listens, listens, say whatever you want.</ta>
            <ta e="T384" id="Seg_3435" s="T381">No.</ta>
            <ta e="T392" id="Seg_3436" s="T385">Yes, that means you didn’t say anything about the accountant in Russian.</ta>
            <ta e="T396" id="Seg_3437" s="T392">You can start talking about the accountant.</ta>
            <ta e="T407" id="Seg_3438" s="T397">How his hands froze, until then you spoke Russian.</ta>
            <ta e="T420" id="Seg_3439" s="T408">But when you told me how they dispersed and sued, that’s what you already said in Russian.</ta>
            <ta e="T424" id="Seg_3440" s="T423">Yes.</ta>
            <ta e="T434" id="Seg_3441" s="T433">Well…</ta>
            <ta e="T446" id="Seg_3442" s="T437">In Russian, only you this (part)…</ta>
            <ta e="T457" id="Seg_3443" s="T456">Yes.</ta>
            <ta e="T477" id="Seg_3444" s="T466">Well, it will all be clear, we are smart people - we will understand.</ta>
            <ta e="T489" id="Seg_3445" s="T478">Well, tell something else about village life.</ta>
            <ta e="T496" id="Seg_3446" s="T490">What else do you have there?</ta>
            <ta e="T507" id="Seg_3447" s="T500">Who is your boss now?</ta>
            <ta e="T526" id="Seg_3448" s="T507">I remember the chairman of the village council here, there we got some seals from him and all that.</ta>
            <ta e="T557" id="Seg_3449" s="T541">How, how, I know him, I know what he was.</ta>
            <ta e="T569" id="Seg_3450" s="T557">No, no, no, just tell me.</ta>
            <ta e="T575" id="Seg_3451" s="T573">Aha.</ta>
            <ta e="T590" id="Seg_3452" s="T586">He was a good man.</ta>
            <ta e="T597" id="Seg_3453" s="T591">Tell me, please.</ta>
            <ta e="T853" id="Seg_3454" s="T839">So I don’t remember now whether I knew him or not.</ta>
            <ta e="T857" id="Seg_3455" s="T855">Yes.</ta>
            <ta e="T889" id="Seg_3456" s="T887">Mm, mm, mm, mm.</ta>
            <ta e="T907" id="Seg_3457" s="T906">So.</ta>
            <ta e="T911" id="Seg_3458" s="T908">[?] </ta>
            <ta e="T934" id="Seg_3459" s="T919">Well, we’ll still talk about this, but maybe you can still talk about Zagorodniuk?</ta>
            <ta e="T944" id="Seg_3460" s="T935">In Kamass, the whole thing, how it was with him.</ta>
            <ta e="T948" id="Seg_3461" s="T944">How you went there.</ta>
            <ta e="T956" id="Seg_3462" s="T950">Everyone listens, everything is recorded. </ta>
            <ta e="T964" id="Seg_3463" s="T958">It’s recorded, now everything is recorded in Russian .</ta>
            <ta e="T968" id="Seg_3464" s="T965">And now only in Kamas.</ta>
            <ta e="T1060" id="Seg_3465" s="T1040">You also talked about how you told him that "I will tell you as a son that I’ll go to God to pray."</ta>
            <ta e="T1067" id="Seg_3466" s="T1060">That haven't you said in Kamass yet.</ta>
            <ta e="T1072" id="Seg_3467" s="T1067">Please, please, everybody listens, writes. </ta>
            <ta e="T1109" id="Seg_3468" s="T1101">Well, here we started talking about the brigadier.</ta>
            <ta e="T1117" id="Seg_3469" s="T1109">No, but here's what we haven't told yet.</ta>
            <ta e="T1130" id="Seg_3470" s="T1118">We only said here that he died two years ago.</ta>
            <ta e="T1137" id="Seg_3471" s="T1130">And you haven't told that yet.</ta>
            <ta e="T1149" id="Seg_3472" s="T1145">Yes, yes.</ta>
            <ta e="T1157" id="Seg_3473" s="T1149">And, it was told, then everything is in order. </ta>
            <ta e="T1162" id="Seg_3474" s="T1158">Well, maybe about the brigadier?</ta>
            <ta e="T1166" id="Seg_3475" s="T1163">This August, the German.</ta>
            <ta e="T1172" id="Seg_3476" s="T1166">I don’t remember him, it seems. </ta>
            <ta e="T1176" id="Seg_3477" s="T1173">I don’t remember.</ta>
            <ta e="T1247" id="Seg_3478" s="T1238">All in Russian again, you told many new things. </ta>
            <ta e="T1289" id="Seg_3479" s="T1288">Well.</ta>
            <ta e="T1302" id="Seg_3480" s="T1290">That's what interests me, I remember, after all, there was such a store in Abalakov.</ta>
            <ta e="T1307" id="Seg_3481" s="T1303">And they sold everything there.</ta>
            <ta e="T1316" id="Seg_3482" s="T1308">Can you tell me what is sold there now?</ta>
            <ta e="T1334" id="Seg_3483" s="T1317">Interestingly, I remember when we last visited there, they even sold such alcohol, 69-percent alcohol</ta>
            <ta e="T1339" id="Seg_3484" s="T1335">It was such a terrible drink.</ta>
            <ta e="T1346" id="Seg_3485" s="T1345">Yes.</ta>
            <ta e="T1351" id="Seg_3486" s="T1350">Mhm.</ta>
            <ta e="T1370" id="Seg_3487" s="T1368">Mhm. </ta>
            <ta e="T1381" id="Seg_3488" s="T1378">So, well …</ta>
            <ta e="T1419" id="Seg_3489" s="T1405">No, I don’t know, I remember when the store stood in the very center.</ta>
            <ta e="T1432" id="Seg_3490" s="T1420">On the same street, close to Shura, not very far.</ta>
            <ta e="T1446" id="Seg_3491" s="T1435">Still to the club, if you go from Shura on the right side.</ta>
            <ta e="T1484" id="Seg_3492" s="T1481">Yes.</ta>
            <ta e="T1486" id="Seg_3493" s="T1484">Yes.</ta>
            <ta e="T1490" id="Seg_3494" s="T1488">Yes.</ta>
            <ta e="T1493" id="Seg_3495" s="T1492">Yes.</ta>
            <ta e="T1496" id="Seg_3496" s="T1494">Yes.</ta>
            <ta e="T1501" id="Seg_3497" s="T1500">Well, well, well.</ta>
            <ta e="T1513" id="Seg_3498" s="T1501">No, I don’t know that store at all. </ta>
            <ta e="T1521" id="Seg_3499" s="T1519">No no no no no.</ta>
            <ta e="T1541" id="Seg_3500" s="T1539">Mm, mm. </ta>
            <ta e="T1545" id="Seg_3501" s="T1542">A brick building?</ta>
            <ta e="T1550" id="Seg_3502" s="T1546">Or wooden?</ta>
            <ta e="T1556" id="Seg_3503" s="T1554">Mhm. </ta>
            <ta e="T1605" id="Seg_3504" s="T1604">Yes.</ta>
            <ta e="T1643" id="Seg_3505" s="T1642">Yes.</ta>
            <ta e="T1660" id="Seg_3506" s="T1659">Yes.</ta>
            <ta e="T1661" id="Seg_3507" s="T1660">Yes.</ta>
            <ta e="T1689" id="Seg_3508" s="T1672">Ich erinnere mich an "Murachev", an diesen Namen, aber ich kenne eine solche Person nicht (…).</ta>
            <ta e="T1716" id="Seg_3509" s="T1710">And now let's do it all in Kamas.</ta>
            <ta e="T1725" id="Seg_3510" s="T1717">And then we talk and forget what we said.</ta>
            <ta e="T1737" id="Seg_3511" s="T1725">Here first, about the alcohol that they still sell such alcohol, and for health …</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T3" id="Seg_3512" s="T1">Auf welchem?</ta>
            <ta e="T5" id="Seg_3513" s="T3">Auf jedem.</ta>
            <ta e="T14" id="Seg_3514" s="T6">Oder du sagst es mir zu erst auf Russisch.</ta>
            <ta e="T22" id="Seg_3515" s="T14">Sag es mir erst auf Russisch, dann auf Kamassisch.</ta>
            <ta e="T28" id="Seg_3516" s="T23">Und ich werde zuhören, es ist interessant für mich!</ta>
            <ta e="T34" id="Seg_3517" s="T32">Es läuft.</ta>
            <ta e="T278" id="Seg_3518" s="T270">Nein, du hast überhaupt nichts über den Buchhalter erzählt.</ta>
            <ta e="T282" id="Seg_3519" s="T278">Wie war das gemeint?</ta>
            <ta e="T285" id="Seg_3520" s="T283">Sag es mir so weit.</ta>
            <ta e="T293" id="Seg_3521" s="T289">Ja, was ist mit der Buchhalterin?</ta>
            <ta e="T299" id="Seg_3522" s="T296">Ja.</ta>
            <ta e="T303" id="Seg_3523" s="T300">Ja. </ta>
            <ta e="T322" id="Seg_3524" s="T311">Nun ja, hier geht es um Pascha und Shura, aber ist das der Buchhalter?</ta>
            <ta e="T329" id="Seg_3525" s="T324">Welche (…).</ta>
            <ta e="T340" id="Seg_3526" s="T331">Ja, das auch, oder?</ta>
            <ta e="T347" id="Seg_3527" s="T341">Ah, ah, sie ist bei ihr, ja.</ta>
            <ta e="T353" id="Seg_3528" s="T349">Alles klar.</ta>
            <ta e="T376" id="Seg_3529" s="T364">Alles, [Kassettenrekorder] hört zu, hört zu, sagt, was immer du willst.</ta>
            <ta e="T384" id="Seg_3530" s="T381">Nein.</ta>
            <ta e="T392" id="Seg_3531" s="T385">Ja, das heißt, du hast auf Russisch nichts über die Buchhalterin gesagt.</ta>
            <ta e="T396" id="Seg_3532" s="T392">Du kannst anfangen über die Buchhalterin zu reden.</ta>
            <ta e="T407" id="Seg_3533" s="T397">Als seine Hände erfrierten, bis dahin sprachst du Russisch.</ta>
            <ta e="T420" id="Seg_3534" s="T408">Aber als du mir erzählt hast, wie sie sich trennten und verklagten, das hast du bereits auf Russisch gesagt.</ta>
            <ta e="T424" id="Seg_3535" s="T423">Ja.</ta>
            <ta e="T434" id="Seg_3536" s="T433">Nun…</ta>
            <ta e="T446" id="Seg_3537" s="T437">Auf Russisch nur Sie diese (Teil)…</ta>
            <ta e="T457" id="Seg_3538" s="T456">Ja.</ta>
            <ta e="T477" id="Seg_3539" s="T466">Trotzdem wird es klar sein, wir sind kluge Leute - wir werden verstehen.</ta>
            <ta e="T489" id="Seg_3540" s="T478">Erzählen Sie doch noch etwas über das Dorfleben.</ta>
            <ta e="T496" id="Seg_3541" s="T490">Was habt ihr noch da?</ta>
            <ta e="T507" id="Seg_3542" s="T500">Wer ist jetzt dein Chef?</ta>
            <ta e="T526" id="Seg_3543" s="T507">Ich erinnere mich hier an den Vorsitzenden des Dorfrats, wir haben einige Siegel von ihm und all dem.</ta>
            <ta e="T557" id="Seg_3544" s="T541">Wie, wie, ich kenne ihn, ich weiß, was er war.</ta>
            <ta e="T569" id="Seg_3545" s="T557">Nein, nein, nein, sag es mir einfach.</ta>
            <ta e="T575" id="Seg_3546" s="T573">Aha.</ta>
            <ta e="T590" id="Seg_3547" s="T586">Er war ein guter Mann.</ta>
            <ta e="T597" id="Seg_3548" s="T591">Sag es mir bitte.</ta>
            <ta e="T853" id="Seg_3549" s="T839">Also kann ich mich jetzt nicht erinnern, ob ich ihn gekannt habe oder nicht.</ta>
            <ta e="T857" id="Seg_3550" s="T855">Ja.</ta>
            <ta e="T889" id="Seg_3551" s="T887">Mm, mm, mm, mm.</ta>
            <ta e="T907" id="Seg_3552" s="T906">Also.</ta>
            <ta e="T911" id="Seg_3553" s="T908">[?]</ta>
            <ta e="T934" id="Seg_3554" s="T919">Nun, wir werden noch darüber reden, aber vielleicht kannst du auch noch über Zagorodniuk sprechen?</ta>
            <ta e="T944" id="Seg_3555" s="T935">Auf Kamassisch, die ganze Sache, wie es mit ihm war. </ta>
            <ta e="T948" id="Seg_3556" s="T944">Wie ihr dorthin gegangen seid.</ta>
            <ta e="T956" id="Seg_3557" s="T950">Jeder hört zu, alles wird aufgezeichnet.</ta>
            <ta e="T964" id="Seg_3558" s="T958">Es ist aufgenommen, auf Russisch ist jetzt alles aufgenommen.</ta>
            <ta e="T968" id="Seg_3559" s="T965">Und jetzt nur auf Kamassisch.</ta>
            <ta e="T1060" id="Seg_3560" s="T1040">Du hast auch darüber gesprochen, wie du ihm gesagt hast: "Ich werde Ihnen als Sohn sagen, dass ich zu Gott gehe, um zu beten."</ta>
            <ta e="T1067" id="Seg_3561" s="T1060">Das hast du noch nicht auf Kamassisch gesagt.</ta>
            <ta e="T1072" id="Seg_3562" s="T1067">Bitte, bitte, alle hören zu, schreibt.</ta>
            <ta e="T1109" id="Seg_3563" s="T1101">Nun, hier haben wir angefangen, über den Brigadier zu reden.</ta>
            <ta e="T1117" id="Seg_3564" s="T1109">Nein, aber hier ist, was wir noch nicht gesagt haben.</ta>
            <ta e="T1130" id="Seg_3565" s="T1118">Wir haben hier nur gesagt, dass er vor zwei Jahren gestorben ist.</ta>
            <ta e="T1137" id="Seg_3566" s="T1130">Und das hast du noch nicht erzählt.</ta>
            <ta e="T1149" id="Seg_3567" s="T1145">Ja, ja.</ta>
            <ta e="T1157" id="Seg_3568" s="T1149">Und es wurde gesagt, dann ist alles in Ordnung.</ta>
            <ta e="T1162" id="Seg_3569" s="T1158">Nun, vielleicht über den Brigadier?</ta>
            <ta e="T1166" id="Seg_3570" s="T1163">Diesen August, den Deutschen.</ta>
            <ta e="T1172" id="Seg_3571" s="T1166">Ich kann mich an ihn nicht erinnern.</ta>
            <ta e="T1176" id="Seg_3572" s="T1173">Ich erinnere mich nicht.</ta>
            <ta e="T1247" id="Seg_3573" s="T1238">Alles noch mal auf Russisch, du hast viele neue Dinge erzählt.</ta>
            <ta e="T1289" id="Seg_3574" s="T1288">Nun.</ta>
            <ta e="T1302" id="Seg_3575" s="T1290">Das interessiert mich, ich erinnere mich, dass es so einen Laden in Abalakov gab.</ta>
            <ta e="T1307" id="Seg_3576" s="T1303">Und sie haben dort alles verkauft.</ta>
            <ta e="T1316" id="Seg_3577" s="T1308">Kannst du mir sagen, was da jetzt verkauft wird?</ta>
            <ta e="T1334" id="Seg_3578" s="T1317">Interessanterweise erinnere ich mich, als wir das letzte Mal dort waren, haben sie sogar solchen Alkohol verkauft, 69-Prozentigen Alkohol.</ta>
            <ta e="T1339" id="Seg_3579" s="T1335">Es war so ein schreckliches Getränk.</ta>
            <ta e="T1346" id="Seg_3580" s="T1345">Ja.</ta>
            <ta e="T1351" id="Seg_3581" s="T1350">Mhm.</ta>
            <ta e="T1370" id="Seg_3582" s="T1368">Mhm. </ta>
            <ta e="T1381" id="Seg_3583" s="T1378">Also gut …</ta>
            <ta e="T1419" id="Seg_3584" s="T1405">Nein, ich kenne ihn nicht, ich erinnere mich, als der Laden genau im Zentrum stand.</ta>
            <ta e="T1432" id="Seg_3585" s="T1420">In der gleichen Straße, in der Nähe von Shura, nicht sehr weit.</ta>
            <ta e="T1446" id="Seg_3586" s="T1435">Noch zum Klub, wenn du von Shura gehst, auf der rechen Seite.</ta>
            <ta e="T1464" id="Seg_3587" s="T1447">Es gab so ein Haus, die Verkäuferin lebte noch in der Nähe, es scheint in der Nähe, im Nachbarhaus oder so (oder was auch immer).</ta>
            <ta e="T1484" id="Seg_3588" s="T1481">Ja.</ta>
            <ta e="T1486" id="Seg_3589" s="T1484">Ja.</ta>
            <ta e="T1490" id="Seg_3590" s="T1488">Ja.</ta>
            <ta e="T1493" id="Seg_3591" s="T1492">Ja.</ta>
            <ta e="T1496" id="Seg_3592" s="T1494">Ja.</ta>
            <ta e="T1501" id="Seg_3593" s="T1500">Nun, nun, nun.</ta>
            <ta e="T1513" id="Seg_3594" s="T1501">Nein, ich kenne diesen Laden überhaupt nicht.</ta>
            <ta e="T1521" id="Seg_3595" s="T1519">Nein, nein, nein, nein.</ta>
            <ta e="T1541" id="Seg_3596" s="T1539">Mm, mm. </ta>
            <ta e="T1545" id="Seg_3597" s="T1542">Ein Backsteingebäude?</ta>
            <ta e="T1550" id="Seg_3598" s="T1546">Oder aus Holz?</ta>
            <ta e="T1556" id="Seg_3599" s="T1554">Mhm. </ta>
            <ta e="T1605" id="Seg_3600" s="T1604">Ja.</ta>
            <ta e="T1643" id="Seg_3601" s="T1642">Ja.</ta>
            <ta e="T1660" id="Seg_3602" s="T1659">Ja.</ta>
            <ta e="T1661" id="Seg_3603" s="T1660">Ja.</ta>
            <ta e="T1689" id="Seg_3604" s="T1672">I remember "Murachev", I in general remember this name, but I don’t know such a person (…).</ta>
            <ta e="T1716" id="Seg_3605" s="T1710">Und jetzt alles auf Kamassisch. </ta>
            <ta e="T1725" id="Seg_3606" s="T1717">Und dann plaudern wir und vergessen, was wir gesagt haben.</ta>
            <ta e="T1737" id="Seg_3607" s="T1725">Hier zunächst über den Alkohol, den sie noch verkaufen, und für die Gesundheit …</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T278" id="Seg_3608" s="T270">[AAV] KA is wrong: she actually did tell that the woman with whom Pasha lived worked as "счетовод".</ta>
            <ta e="T322" id="Seg_3609" s="T311">[AAV] KA misunderstoods the "счетовод" to be a man, not a woman.</ta>
            <ta e="T1464" id="Seg_3610" s="T1447">There was such a house, the saleswoman still lived close, it seems close, in the neighboring house or something (or whatever).</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T719" start="T718">
            <tli id="T718.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T820" start="T819">
            <tli id="T819.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1344" start="T1341">
            <tli id="T1341.tx-PKZ.1" />
            <tli id="T1341.tx-PKZ.2" />
            <tli id="T1341.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T1359" start="T1358">
            <tli id="T1358.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1484" start="T1471">
            <tli id="T1471.tx-PKZ.1" />
            <tli id="T1471.tx-PKZ.2" />
            <tli id="T1471.tx-PKZ.3" />
            <tli id="T1471.tx-PKZ.4" />
            <tli id="T1471.tx-PKZ.5" />
            <tli id="T1471.tx-PKZ.6" />
            <tli id="T1471.tx-PKZ.7" />
            <tli id="T1471.tx-PKZ.8" />
            <tli id="T1471.tx-PKZ.9" />
         </timeline-fork>
         <timeline-fork end="T1486" start="T1484">
            <tli id="T1484.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1492" start="T1491">
            <tli id="T1491.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1495" start="T1493">
            <tli id="T1493.tx-PKZ.1" />
            <tli id="T1493.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T1499" start="T1495">
            <tli id="T1495.tx-PKZ.1" />
            <tli id="T1495.tx-PKZ.2" />
            <tli id="T1495.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T1544" start="T1542">
            <tli id="T1542.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T16" id="Seg_3611" n="sc" s="T6">
               <ts e="T16" id="Seg_3613" n="HIAT:u" s="T6">
                  <ts e="T8" id="Seg_3615" n="HIAT:w" s="T6">Ну</ts>
                  <nts id="Seg_3616" n="HIAT:ip">,</nts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_3619" n="HIAT:w" s="T8">можно</ts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_3622" n="HIAT:w" s="T11">по-русски</ts>
                  <nts id="Seg_3623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_3625" n="HIAT:w" s="T13">рассказать</ts>
                  <nts id="Seg_3626" n="HIAT:ip">.</nts>
                  <nts id="Seg_3627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_3628" n="sc" s="T29">
               <ts e="T31" id="Seg_3630" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_3632" n="HIAT:w" s="T29">Ну</ts>
                  <nts id="Seg_3633" n="HIAT:ip">,</nts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_3636" n="HIAT:w" s="T30">идет</ts>
                  <nts id="Seg_3637" n="HIAT:ip">?</nts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T43" id="Seg_3639" n="sc" s="T35">
               <ts e="T43" id="Seg_3641" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_3643" n="HIAT:w" s="T35">Паша</ts>
                  <nts id="Seg_3644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_3646" n="HIAT:w" s="T36">Василинич</ts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3648" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_3650" n="HIAT:w" s="T37">свою</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_3653" n="HIAT:w" s="T38">Шу-</ts>
                  <nts id="Seg_3654" n="HIAT:ip">)</nts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_3657" n="HIAT:w" s="T39">со</ts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_3660" n="HIAT:w" s="T40">своей</ts>
                  <nts id="Seg_3661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_3663" n="HIAT:w" s="T41">Шурой</ts>
                  <nts id="Seg_3664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_3666" n="HIAT:w" s="T42">разошлись</ts>
                  <nts id="Seg_3667" n="HIAT:ip">.</nts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_3669" n="sc" s="T44">
               <ts e="T55" id="Seg_3671" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_3673" n="HIAT:w" s="T44">Ей</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_3676" n="HIAT:w" s="T45">всё</ts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_3679" n="HIAT:w" s="T46">в</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_3682" n="HIAT:w" s="T47">доме</ts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_3685" n="HIAT:w" s="T48">осталось</ts>
                  <nts id="Seg_3686" n="HIAT:ip">,</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_3689" n="HIAT:w" s="T49">а</ts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_3692" n="HIAT:w" s="T50">ему</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_3695" n="HIAT:w" s="T51">только</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_3698" n="HIAT:w" s="T52">одно</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_3701" n="HIAT:w" s="T53">мотоцикло</ts>
                  <nts id="Seg_3702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_3704" n="HIAT:w" s="T54">досталось</ts>
                  <nts id="Seg_3705" n="HIAT:ip">.</nts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_3707" n="sc" s="T56">
               <ts e="T62" id="Seg_3709" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_3711" n="HIAT:w" s="T56">Он</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_3714" n="HIAT:w" s="T57">там</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_3717" n="HIAT:w" s="T58">связался</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_3720" n="HIAT:w" s="T59">с</ts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_3723" n="HIAT:w" s="T60">одной</ts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_3726" n="HIAT:w" s="T61">женщиной</ts>
                  <nts id="Seg_3727" n="HIAT:ip">.</nts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T68" id="Seg_3729" n="sc" s="T63">
               <ts e="T68" id="Seg_3731" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_3733" n="HIAT:w" s="T63">Она</ts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_3736" n="HIAT:w" s="T64">в</ts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_3739" n="HIAT:w" s="T65">конторе</ts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_3742" n="HIAT:w" s="T66">работала</ts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_3745" n="HIAT:w" s="T67">счетоводом</ts>
                  <nts id="Seg_3746" n="HIAT:ip">.</nts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T74" id="Seg_3748" n="sc" s="T69">
               <ts e="T74" id="Seg_3750" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_3752" n="HIAT:w" s="T69">У</ts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_3755" n="HIAT:w" s="T70">ей</ts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_3758" n="HIAT:w" s="T71">тоже</ts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_3761" n="HIAT:w" s="T72">мужик</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_3764" n="HIAT:w" s="T73">был</ts>
                  <nts id="Seg_3765" n="HIAT:ip">.</nts>
                  <nts id="Seg_3766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_3767" n="sc" s="T75">
               <ts e="T84" id="Seg_3769" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_3771" n="HIAT:w" s="T75">Пьяный</ts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_3774" n="HIAT:w" s="T76">напился</ts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_3777" n="HIAT:w" s="T77">и</ts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_3780" n="HIAT:w" s="T78">спал</ts>
                  <nts id="Seg_3781" n="HIAT:ip">,</nts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_3784" n="HIAT:w" s="T79">руку</ts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_3787" n="HIAT:w" s="T80">отморозил</ts>
                  <nts id="Seg_3788" n="HIAT:ip">,</nts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_3791" n="HIAT:w" s="T81">в</ts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_3794" n="HIAT:w" s="T82">больнице</ts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_3797" n="HIAT:w" s="T83">лежал</ts>
                  <nts id="Seg_3798" n="HIAT:ip">.</nts>
                  <nts id="Seg_3799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_3800" n="sc" s="T85">
               <ts e="T89" id="Seg_3802" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_3804" n="HIAT:w" s="T85">И</ts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_3807" n="HIAT:w" s="T86">уехал</ts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_3810" n="HIAT:w" s="T87">к</ts>
                  <nts id="Seg_3811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_3813" n="HIAT:w" s="T88">матери</ts>
                  <nts id="Seg_3814" n="HIAT:ip">.</nts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T106" id="Seg_3816" n="sc" s="T90">
               <ts e="T95" id="Seg_3818" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_3820" n="HIAT:w" s="T90">Она</ts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_3823" n="HIAT:w" s="T91">потом</ts>
                  <nts id="Seg_3824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_3826" n="HIAT:w" s="T92">выслала</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_3829" n="HIAT:w" s="T93">всё</ts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_3832" n="HIAT:w" s="T94">евонно</ts>
                  <nts id="Seg_3833" n="HIAT:ip">.</nts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_3836" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_3838" n="HIAT:w" s="T95">И</ts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_3841" n="HIAT:w" s="T96">с</ts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_3844" n="HIAT:w" s="T97">Пашей</ts>
                  <nts id="Seg_3845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_3847" n="HIAT:w" s="T98">сошлась</ts>
                  <nts id="Seg_3848" n="HIAT:ip">,</nts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_3851" n="HIAT:w" s="T99">а</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_3854" n="HIAT:w" s="T100">Паша</ts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_3857" n="HIAT:w" s="T101">разделился</ts>
                  <nts id="Seg_3858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3859" n="HIAT:ip">(</nts>
                  <ts e="T103" id="Seg_3861" n="HIAT:w" s="T102">со-</ts>
                  <nts id="Seg_3862" n="HIAT:ip">)</nts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_3865" n="HIAT:w" s="T103">со</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_3868" n="HIAT:w" s="T104">своёй</ts>
                  <nts id="Seg_3869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_3871" n="HIAT:w" s="T105">женой</ts>
                  <nts id="Seg_3872" n="HIAT:ip">.</nts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_3874" n="sc" s="T107">
               <ts e="T113" id="Seg_3876" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_3878" n="HIAT:w" s="T107">И</ts>
                  <nts id="Seg_3879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_3881" n="HIAT:w" s="T108">на</ts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_3884" n="HIAT:w" s="T109">суд</ts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_3887" n="HIAT:w" s="T110">она</ts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_3890" n="HIAT:w" s="T111">подала</ts>
                  <nts id="Seg_3891" n="HIAT:ip">,</nts>
                  <nts id="Seg_3892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_3894" n="HIAT:w" s="T112">он</ts>
                  <nts id="Seg_3895" n="HIAT:ip">…</nts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_3897" n="sc" s="T114">
               <ts e="T118" id="Seg_3899" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_3901" n="HIAT:w" s="T114">Высудили</ts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_3904" n="HIAT:w" s="T115">с</ts>
                  <nts id="Seg_3905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_3907" n="HIAT:w" s="T116">его</ts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_3910" n="HIAT:w" s="T117">алименты</ts>
                  <nts id="Seg_3911" n="HIAT:ip">.</nts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_3913" n="sc" s="T119">
               <ts e="T122" id="Seg_3915" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_3917" n="HIAT:w" s="T119">Всем</ts>
                  <nts id="Seg_3918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_3920" n="HIAT:w" s="T120">троим</ts>
                  <nts id="Seg_3921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_3923" n="HIAT:w" s="T121">платит</ts>
                  <nts id="Seg_3924" n="HIAT:ip">.</nts>
                  <nts id="Seg_3925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_3926" n="sc" s="T123">
               <ts e="T130" id="Seg_3928" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_3930" n="HIAT:w" s="T123">Когда</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_3933" n="HIAT:w" s="T124">она</ts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_3936" n="HIAT:w" s="T125">собралась</ts>
                  <nts id="Seg_3937" n="HIAT:ip">,</nts>
                  <nts id="Seg_3938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_3940" n="HIAT:w" s="T126">поехала</ts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_3943" n="HIAT:w" s="T127">туды</ts>
                  <nts id="Seg_3944" n="HIAT:ip">,</nts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_3947" n="HIAT:w" s="T128">в</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_3950" n="HIAT:w" s="T129">Сушенское</ts>
                  <nts id="Seg_3951" n="HIAT:ip">.</nts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_3953" n="sc" s="T131">
               <ts e="T137" id="Seg_3955" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_3957" n="HIAT:w" s="T131">Он</ts>
                  <nts id="Seg_3958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_3960" n="HIAT:w" s="T132">мальчишка</ts>
                  <nts id="Seg_3961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_3963" n="HIAT:w" s="T133">одного</ts>
                  <nts id="Seg_3964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_3966" n="HIAT:w" s="T134">не</ts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_3969" n="HIAT:w" s="T135">отдал</ts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_3972" n="HIAT:w" s="T136">ей</ts>
                  <nts id="Seg_3973" n="HIAT:ip">.</nts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T146" id="Seg_3975" n="sc" s="T138">
               <ts e="T146" id="Seg_3977" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_3979" n="HIAT:w" s="T138">Но</ts>
                  <nts id="Seg_3980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_3982" n="HIAT:w" s="T139">она</ts>
                  <nts id="Seg_3983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_3985" n="HIAT:w" s="T140">всё</ts>
                  <nts id="Seg_3986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_3988" n="HIAT:w" s="T141">хотела</ts>
                  <nts id="Seg_3989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_3991" n="HIAT:w" s="T142">отобрать</ts>
                  <nts id="Seg_3992" n="HIAT:ip">,</nts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_3995" n="HIAT:w" s="T143">он</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_3998" n="HIAT:w" s="T144">его</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_4001" n="HIAT:w" s="T145">увез</ts>
                  <nts id="Seg_4002" n="HIAT:ip">.</nts>
                  <nts id="Seg_4003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T154" id="Seg_4004" n="sc" s="T147">
               <ts e="T154" id="Seg_4006" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_4008" n="HIAT:w" s="T147">А</ts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_4011" n="HIAT:w" s="T148">теперь</ts>
                  <nts id="Seg_4012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_4014" n="HIAT:w" s="T149">нашу</ts>
                  <nts id="Seg_4015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_4017" n="HIAT:w" s="T150">деревню</ts>
                  <nts id="Seg_4018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_4020" n="HIAT:w" s="T151">с</ts>
                  <nts id="Seg_4021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_4023" n="HIAT:w" s="T152">Вознесенкой</ts>
                  <nts id="Seg_4024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_4026" n="HIAT:w" s="T153">соединили</ts>
                  <nts id="Seg_4027" n="HIAT:ip">.</nts>
                  <nts id="Seg_4028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T161" id="Seg_4029" n="sc" s="T155">
               <ts e="T161" id="Seg_4031" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_4033" n="HIAT:w" s="T155">И</ts>
                  <nts id="Seg_4034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_4036" n="HIAT:w" s="T156">там</ts>
                  <nts id="Seg_4037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_4039" n="HIAT:w" s="T157">он</ts>
                  <nts id="Seg_4040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_4042" n="HIAT:w" s="T158">живет</ts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_4045" n="HIAT:w" s="T159">с</ts>
                  <nts id="Seg_4046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_4048" n="HIAT:w" s="T160">ей</ts>
                  <nts id="Seg_4049" n="HIAT:ip">.</nts>
                  <nts id="Seg_4050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_4051" n="sc" s="T162">
               <ts e="T172" id="Seg_4053" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_4055" n="HIAT:w" s="T162">А</ts>
                  <nts id="Seg_4056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_4058" n="HIAT:w" s="T163">мать</ts>
                  <nts id="Seg_4059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_4061" n="HIAT:w" s="T164">евонна</ts>
                  <nts id="Seg_4062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_4064" n="HIAT:w" s="T165">тут</ts>
                  <nts id="Seg_4065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_4067" n="HIAT:w" s="T166">живет</ts>
                  <nts id="Seg_4068" n="HIAT:ip">,</nts>
                  <nts id="Seg_4069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_4071" n="HIAT:w" s="T167">а</ts>
                  <nts id="Seg_4072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_4074" n="HIAT:w" s="T168">изба</ts>
                  <nts id="Seg_4075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_4077" n="HIAT:w" s="T169">стоит</ts>
                  <nts id="Seg_4078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_4080" n="HIAT:w" s="T170">одна</ts>
                  <nts id="Seg_4081" n="HIAT:ip">,</nts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_4084" n="HIAT:w" s="T171">пустая</ts>
                  <nts id="Seg_4085" n="HIAT:ip">.</nts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T179" id="Seg_4087" n="sc" s="T173">
               <ts e="T179" id="Seg_4089" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_4091" n="HIAT:w" s="T173">Никто</ts>
                  <nts id="Seg_4092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_4094" n="HIAT:w" s="T174">не</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_4097" n="HIAT:w" s="T175">берет</ts>
                  <nts id="Seg_4098" n="HIAT:ip">,</nts>
                  <nts id="Seg_4099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_4101" n="HIAT:w" s="T176">люди</ts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_4104" n="HIAT:w" s="T177">все</ts>
                  <nts id="Seg_4105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_4107" n="HIAT:w" s="T178">разъезжаются</ts>
                  <nts id="Seg_4108" n="HIAT:ip">.</nts>
                  <nts id="Seg_4109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_4110" n="sc" s="T180">
               <ts e="T183" id="Seg_4112" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_4114" n="HIAT:w" s="T180">С</ts>
                  <nts id="Seg_4115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_4117" n="HIAT:w" s="T181">той</ts>
                  <nts id="Seg_4118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_4120" n="HIAT:w" s="T182">деревни</ts>
                  <nts id="Seg_4121" n="HIAT:ip">.</nts>
                  <nts id="Seg_4122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T194" id="Seg_4123" n="sc" s="T184">
               <ts e="T194" id="Seg_4125" n="HIAT:u" s="T184">
                  <nts id="Seg_4126" n="HIAT:ip">(</nts>
                  <ts e="T185" id="Seg_4128" n="HIAT:w" s="T184">Pa-</ts>
                  <nts id="Seg_4129" n="HIAT:ip">)</nts>
                  <nts id="Seg_4130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_4132" n="HIAT:w" s="T185">Paša</ts>
                  <nts id="Seg_4133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_4135" n="HIAT:w" s="T186">Vasilinič</ts>
                  <nts id="Seg_4136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_4138" n="HIAT:w" s="T187">bostə</ts>
                  <nts id="Seg_4139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_4141" n="HIAT:w" s="T188">Šuranə</ts>
                  <nts id="Seg_4142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_4144" n="HIAT:w" s="T189">baʔluʔpi</ts>
                  <nts id="Seg_4145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_4147" n="HIAT:w" s="T190">i</ts>
                  <nts id="Seg_4148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_4150" n="HIAT:w" s="T191">baška</ts>
                  <nts id="Seg_4151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_4153" n="HIAT:w" s="T192">ne</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_4156" n="HIAT:w" s="T193">ibi</ts>
                  <nts id="Seg_4157" n="HIAT:ip">.</nts>
                  <nts id="Seg_4158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_4159" n="sc" s="T195">
               <ts e="T201" id="Seg_4161" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_4163" n="HIAT:w" s="T195">Dĭgəttə</ts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_4166" n="HIAT:w" s="T196">dĭ</ts>
                  <nts id="Seg_4167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_4169" n="HIAT:w" s="T197">dĭʔnə</ts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_4172" n="HIAT:w" s="T198">подала</ts>
                  <nts id="Seg_4173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_4175" n="HIAT:w" s="T199">suttə</ts>
                  <nts id="Seg_4176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_4178" n="HIAT:w" s="T200">dĭ</ts>
                  <nts id="Seg_4179" n="HIAT:ip">.</nts>
                  <nts id="Seg_4180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T206" id="Seg_4181" n="sc" s="T202">
               <ts e="T206" id="Seg_4183" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_4185" n="HIAT:w" s="T202">Dĭzeŋ</ts>
                  <nts id="Seg_4186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4187" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_4189" n="HIAT:w" s="T203">су-</ts>
                  <nts id="Seg_4190" n="HIAT:ip">)</nts>
                  <nts id="Seg_4191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_4193" n="HIAT:w" s="T204">судили</ts>
                  <nts id="Seg_4194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_4196" n="HIAT:w" s="T205">dĭ</ts>
                  <nts id="Seg_4197" n="HIAT:ip">.</nts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T211" id="Seg_4199" n="sc" s="T207">
               <ts e="T211" id="Seg_4201" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_4203" n="HIAT:w" s="T207">Tüj</ts>
                  <nts id="Seg_4204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_4206" n="HIAT:w" s="T208">aktʼa</ts>
                  <nts id="Seg_4207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_4209" n="HIAT:w" s="T209">esseŋdə</ts>
                  <nts id="Seg_4210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_4212" n="HIAT:w" s="T210">mĭʔleʔbə</ts>
                  <nts id="Seg_4213" n="HIAT:ip">.</nts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T218" id="Seg_4215" n="sc" s="T212">
               <ts e="T218" id="Seg_4217" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_4219" n="HIAT:w" s="T212">A</ts>
                  <nts id="Seg_4220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_4222" n="HIAT:w" s="T213">dĭ</ts>
                  <nts id="Seg_4223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1738" id="Seg_4225" n="HIAT:w" s="T214">kalla</ts>
                  <nts id="Seg_4226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_4228" n="HIAT:w" s="T1738">dʼürbi</ts>
                  <nts id="Seg_4229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_4231" n="HIAT:w" s="T215">gijen</ts>
                  <nts id="Seg_4232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_4234" n="HIAT:w" s="T216">Lenin</ts>
                  <nts id="Seg_4235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_4237" n="HIAT:w" s="T217">amnolaʔpi</ts>
                  <nts id="Seg_4238" n="HIAT:ip">.</nts>
                  <nts id="Seg_4239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_4240" n="sc" s="T219">
               <ts e="T223" id="Seg_4242" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_4244" n="HIAT:w" s="T219">Dĭn</ts>
                  <nts id="Seg_4245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_4247" n="HIAT:w" s="T220">amnolaʔbə</ts>
                  <nts id="Seg_4248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4249" n="HIAT:ip">(</nts>
                  <ts e="T222" id="Seg_4251" n="HIAT:w" s="T221">dĭ</ts>
                  <nts id="Seg_4252" n="HIAT:ip">)</nts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_4255" n="HIAT:w" s="T222">tüj</ts>
                  <nts id="Seg_4256" n="HIAT:ip">.</nts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T234" id="Seg_4258" n="sc" s="T224">
               <ts e="T234" id="Seg_4260" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_4262" n="HIAT:w" s="T224">Šide</ts>
                  <nts id="Seg_4263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_4265" n="HIAT:w" s="T225">nʼi</ts>
                  <nts id="Seg_4266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_4268" n="HIAT:w" s="T226">i</ts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4270" n="HIAT:ip">(</nts>
                  <ts e="T228" id="Seg_4272" n="HIAT:w" s="T227">ko-</ts>
                  <nts id="Seg_4273" n="HIAT:ip">)</nts>
                  <nts id="Seg_4274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_4276" n="HIAT:w" s="T228">koʔbdo</ts>
                  <nts id="Seg_4277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_4279" n="HIAT:w" s="T229">dĭzi</ts>
                  <nts id="Seg_4280" n="HIAT:ip">,</nts>
                  <nts id="Seg_4281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_4283" n="HIAT:w" s="T230">onʼiʔ</ts>
                  <nts id="Seg_4284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_4286" n="HIAT:w" s="T231">nʼi</ts>
                  <nts id="Seg_4287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4288" n="HIAT:ip">(</nts>
                  <ts e="T233" id="Seg_4290" n="HIAT:w" s="T232">dĭ=</ts>
                  <nts id="Seg_4291" n="HIAT:ip">)</nts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_4294" n="HIAT:w" s="T233">dĭn</ts>
                  <nts id="Seg_4295" n="HIAT:ip">.</nts>
                  <nts id="Seg_4296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T244" id="Seg_4297" n="sc" s="T235">
               <ts e="T244" id="Seg_4299" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_4301" n="HIAT:w" s="T235">Dĭgəttə</ts>
                  <nts id="Seg_4302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4303" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_4305" n="HIAT:w" s="T236">m-</ts>
                  <nts id="Seg_4306" n="HIAT:ip">)</nts>
                  <nts id="Seg_4307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_4309" n="HIAT:w" s="T237">măn</ts>
                  <nts id="Seg_4310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_4312" n="HIAT:w" s="T238">koʔbdom</ts>
                  <nts id="Seg_4313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_4315" n="HIAT:w" s="T239">šobi</ts>
                  <nts id="Seg_4316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4317" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_4319" n="HIAT:w" s="T240">sjuda</ts>
                  <nts id="Seg_4320" n="HIAT:ip">)</nts>
                  <nts id="Seg_4321" n="HIAT:ip">,</nts>
                  <nts id="Seg_4322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_4324" n="HIAT:w" s="T241">onʼiʔ</ts>
                  <nts id="Seg_4325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_4327" n="HIAT:w" s="T242">nʼibə</ts>
                  <nts id="Seg_4328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_4330" n="HIAT:w" s="T243">deʔpi</ts>
                  <nts id="Seg_4331" n="HIAT:ip">.</nts>
                  <nts id="Seg_4332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T252" id="Seg_4333" n="sc" s="T245">
               <ts e="T252" id="Seg_4335" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_4337" n="HIAT:w" s="T245">Dĭ</ts>
                  <nts id="Seg_4338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_4340" n="HIAT:w" s="T246">măndə</ts>
                  <nts id="Seg_4341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_4343" n="HIAT:w" s="T247">Pašanə:</ts>
                  <nts id="Seg_4344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4345" n="HIAT:ip">"</nts>
                  <ts e="T249" id="Seg_4347" n="HIAT:w" s="T248">Iʔ</ts>
                  <nts id="Seg_4348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_4350" n="HIAT:w" s="T249">iʔ</ts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_4353" n="HIAT:w" s="T250">šide</ts>
                  <nts id="Seg_4354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_4356" n="HIAT:w" s="T251">nʼi</ts>
                  <nts id="Seg_4357" n="HIAT:ip">!</nts>
                  <nts id="Seg_4358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_4359" n="sc" s="T253">
               <ts e="T255" id="Seg_4361" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_4363" n="HIAT:w" s="T253">Ej</ts>
                  <nts id="Seg_4364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_4366" n="HIAT:w" s="T254">kereʔ</ts>
                  <nts id="Seg_4367" n="HIAT:ip">"</nts>
                  <nts id="Seg_4368" n="HIAT:ip">.</nts>
                  <nts id="Seg_4369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T263" id="Seg_4370" n="sc" s="T256">
               <ts e="T263" id="Seg_4372" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_4374" n="HIAT:w" s="T256">Nʼebosʼ</ts>
                  <nts id="Seg_4375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_4377" n="HIAT:w" s="T257">dĭm</ts>
                  <nts id="Seg_4378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_4380" n="HIAT:w" s="T258">ibi</ts>
                  <nts id="Seg_4381" n="HIAT:ip">,</nts>
                  <nts id="Seg_4382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_4384" n="HIAT:w" s="T259">a</ts>
                  <nts id="Seg_4385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_4387" n="HIAT:w" s="T260">esseŋdə</ts>
                  <nts id="Seg_4388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_4390" n="HIAT:w" s="T261">ej</ts>
                  <nts id="Seg_4391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_4393" n="HIAT:w" s="T262">kereʔ</ts>
                  <nts id="Seg_4394" n="HIAT:ip">.</nts>
                  <nts id="Seg_4395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T269" id="Seg_4396" n="sc" s="T266">
               <ts e="T269" id="Seg_4398" n="HIAT:u" s="T266">
                  <nts id="Seg_4399" n="HIAT:ip">(</nts>
                  <nts id="Seg_4400" n="HIAT:ip">(</nts>
                  <ats e="T267" id="Seg_4401" n="HIAT:non-pho" s="T266">BRK</ats>
                  <nts id="Seg_4402" n="HIAT:ip">)</nts>
                  <nts id="Seg_4403" n="HIAT:ip">)</nts>
                  <nts id="Seg_4404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_4406" n="HIAT:w" s="T267">Только</ts>
                  <nts id="Seg_4407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_4409" n="HIAT:w" s="T268">по-своему</ts>
                  <nts id="Seg_4410" n="HIAT:ip">.</nts>
                  <nts id="Seg_4411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T288" id="Seg_4412" n="sc" s="T286">
               <ts e="T288" id="Seg_4414" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_4416" n="HIAT:w" s="T286">Я</ts>
                  <nts id="Seg_4417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_4419" n="HIAT:w" s="T287">говорила</ts>
                  <nts id="Seg_4420" n="HIAT:ip">.</nts>
                  <nts id="Seg_4421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T310" id="Seg_4422" n="sc" s="T292">
               <ts e="T308" id="Seg_4424" n="HIAT:u" s="T292">
                  <nts id="Seg_4425" n="HIAT:ip">(</nts>
                  <ts e="T294" id="Seg_4427" n="HIAT:w" s="T292">Ну</ts>
                  <nts id="Seg_4428" n="HIAT:ip">)</nts>
                  <nts id="Seg_4429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_4431" n="HIAT:w" s="T294">всё</ts>
                  <nts id="Seg_4432" n="HIAT:ip">,</nts>
                  <nts id="Seg_4433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_4435" n="HIAT:w" s="T295">как</ts>
                  <nts id="Seg_4436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_4438" n="HIAT:w" s="T297">Паша</ts>
                  <nts id="Seg_4439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_4441" n="HIAT:w" s="T298">сошелся</ts>
                  <nts id="Seg_4442" n="HIAT:ip">,</nts>
                  <nts id="Seg_4443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_4445" n="HIAT:w" s="T301">как</ts>
                  <nts id="Seg_4446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_4448" n="HIAT:w" s="T302">детей</ts>
                  <nts id="Seg_4449" n="HIAT:ip">,</nts>
                  <nts id="Seg_4450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_4452" n="HIAT:w" s="T304">как</ts>
                  <nts id="Seg_4453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_4455" n="HIAT:w" s="T305">на</ts>
                  <nts id="Seg_4456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_4458" n="HIAT:w" s="T306">суд</ts>
                  <nts id="Seg_4459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_4461" n="HIAT:w" s="T307">подала</ts>
                  <nts id="Seg_4462" n="HIAT:ip">.</nts>
                  <nts id="Seg_4463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_4465" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_4467" n="HIAT:w" s="T308">Лименты</ts>
                  <nts id="Seg_4468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_4470" n="HIAT:w" s="T309">высудила</ts>
                  <nts id="Seg_4471" n="HIAT:ip">.</nts>
                  <nts id="Seg_4472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T340" id="Seg_4473" n="sc" s="T323">
               <ts e="T340" id="Seg_4475" n="HIAT:u" s="T323">
                  <ts e="T326" id="Seg_4477" n="HIAT:w" s="T323">Ну</ts>
                  <nts id="Seg_4478" n="HIAT:ip">,</nts>
                  <nts id="Seg_4479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_4481" n="HIAT:w" s="T326">я</ts>
                  <nts id="Seg_4482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_4484" n="HIAT:w" s="T328">говорила</ts>
                  <nts id="Seg_4485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4486" n="HIAT:ip">(</nts>
                  <ts e="T332" id="Seg_4488" n="HIAT:w" s="T330">с=</ts>
                  <nts id="Seg_4489" n="HIAT:ip">)</nts>
                  <nts id="Seg_4490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_4492" n="HIAT:w" s="T332">с</ts>
                  <nts id="Seg_4493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_4495" n="HIAT:w" s="T334">им</ts>
                  <nts id="Seg_4496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_4498" n="HIAT:w" s="T336">сошлася</ts>
                  <nts id="Seg_4499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_4501" n="HIAT:w" s="T338">она</ts>
                  <nts id="Seg_4502" n="HIAT:ip">.</nts>
                  <nts id="Seg_4503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_4504" n="sc" s="T348">
               <ts e="T350" id="Seg_4506" n="HIAT:u" s="T348">
                  <ts e="T350" id="Seg_4508" n="HIAT:w" s="T348">Говорила</ts>
                  <nts id="Seg_4509" n="HIAT:ip">.</nts>
                  <nts id="Seg_4510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_4511" n="sc" s="T352">
               <ts e="T358" id="Seg_4513" n="HIAT:u" s="T352">
                  <ts e="T354" id="Seg_4515" n="HIAT:w" s="T352">И</ts>
                  <nts id="Seg_4516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_4518" n="HIAT:w" s="T354">как</ts>
                  <nts id="Seg_4519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_4521" n="HIAT:w" s="T355">он</ts>
                  <nts id="Seg_4522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_4524" n="HIAT:w" s="T356">руки</ts>
                  <nts id="Seg_4525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_4527" n="HIAT:w" s="T357">отморозил</ts>
                  <nts id="Seg_4528" n="HIAT:ip">.</nts>
                  <nts id="Seg_4529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T369" id="Seg_4530" n="sc" s="T359">
               <ts e="T363" id="Seg_4532" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_4534" n="HIAT:w" s="T359">Ну</ts>
                  <nts id="Seg_4535" n="HIAT:ip">,</nts>
                  <nts id="Seg_4536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_4538" n="HIAT:w" s="T360">он</ts>
                  <nts id="Seg_4539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_4541" n="HIAT:w" s="T361">же</ts>
                  <nts id="Seg_4542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_4544" n="HIAT:w" s="T362">говорит</ts>
                  <nts id="Seg_4545" n="HIAT:ip">?</nts>
                  <nts id="Seg_4546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_4548" n="HIAT:u" s="T363">
                  <ts e="T365" id="Seg_4550" n="HIAT:w" s="T363">Я</ts>
                  <nts id="Seg_4551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_4553" n="HIAT:w" s="T365">уж</ts>
                  <nts id="Seg_4554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4555" n="HIAT:ip">(</nts>
                  <ts e="T369" id="Seg_4557" n="HIAT:w" s="T367">рас-</ts>
                  <nts id="Seg_4558" n="HIAT:ip">)</nts>
                  <nts id="Seg_4559" n="HIAT:ip">.</nts>
                  <nts id="Seg_4560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T383" id="Seg_4561" n="sc" s="T371">
               <ts e="T383" id="Seg_4563" n="HIAT:u" s="T371">
                  <ts e="T373" id="Seg_4565" n="HIAT:w" s="T371">Рассказывала</ts>
                  <nts id="Seg_4566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_4568" n="HIAT:w" s="T373">же</ts>
                  <nts id="Seg_4569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_4571" n="HIAT:w" s="T375">я</ts>
                  <nts id="Seg_4572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_4574" n="HIAT:w" s="T377">по-русски</ts>
                  <nts id="Seg_4575" n="HIAT:ip">,</nts>
                  <nts id="Seg_4576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_4578" n="HIAT:w" s="T378">или</ts>
                  <nts id="Seg_4579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_4581" n="HIAT:w" s="T379">нет</ts>
                  <nts id="Seg_4582" n="HIAT:ip">,</nts>
                  <nts id="Seg_4583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_4585" n="HIAT:w" s="T380">только</ts>
                  <nts id="Seg_4586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_4588" n="HIAT:w" s="T382">по-своему</ts>
                  <nts id="Seg_4589" n="HIAT:ip">?</nts>
                  <nts id="Seg_4590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T422" id="Seg_4591" n="sc" s="T421">
               <ts e="T422" id="Seg_4593" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_4595" n="HIAT:w" s="T421">Тута</ts>
                  <nts id="Seg_4596" n="HIAT:ip">?</nts>
                  <nts id="Seg_4597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T426" id="Seg_4598" n="sc" s="T425">
               <ts e="T426" id="Seg_4600" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_4602" n="HIAT:w" s="T425">Забыла</ts>
                  <nts id="Seg_4603" n="HIAT:ip">…</nts>
                  <nts id="Seg_4604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T429" id="Seg_4605" n="sc" s="T427">
               <ts e="T429" id="Seg_4607" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_4609" n="HIAT:w" s="T427">Ну</ts>
                  <nts id="Seg_4610" n="HIAT:ip">,</nts>
                  <nts id="Seg_4611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_4613" n="HIAT:w" s="T428">давай</ts>
                  <nts id="Seg_4614" n="HIAT:ip">.</nts>
                  <nts id="Seg_4615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T432" id="Seg_4616" n="sc" s="T430">
               <ts e="T432" id="Seg_4618" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_4620" n="HIAT:w" s="T430">Как</ts>
                  <nts id="Seg_4621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_4623" n="HIAT:w" s="T431">начать-то</ts>
                  <nts id="Seg_4624" n="HIAT:ip">?</nts>
                  <nts id="Seg_4625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T434" id="Seg_4626" n="sc" s="T433">
               <ts e="T434" id="Seg_4628" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_4630" n="HIAT:w" s="T433">По-своему</ts>
                  <nts id="Seg_4631" n="HIAT:ip">?</nts>
                  <nts id="Seg_4632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T438" id="Seg_4633" n="sc" s="T435">
               <ts e="T438" id="Seg_4635" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_4637" n="HIAT:w" s="T435">По-русски</ts>
                  <nts id="Seg_4638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_4640" n="HIAT:w" s="T436">рассказать</ts>
                  <nts id="Seg_4641" n="HIAT:ip">?</nts>
                  <nts id="Seg_4642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T455" id="Seg_4643" n="sc" s="T443">
               <ts e="T455" id="Seg_4645" n="HIAT:u" s="T443">
                  <ts e="T445" id="Seg_4647" n="HIAT:w" s="T443">Нет</ts>
                  <nts id="Seg_4648" n="HIAT:ip">,</nts>
                  <nts id="Seg_4649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_4651" n="HIAT:w" s="T445">я</ts>
                  <nts id="Seg_4652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_4654" n="HIAT:w" s="T447">говорила</ts>
                  <nts id="Seg_4655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_4657" n="HIAT:w" s="T448">вот</ts>
                  <nts id="Seg_4658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_4660" n="HIAT:w" s="T449">сейчас</ts>
                  <nts id="Seg_4661" n="HIAT:ip">,</nts>
                  <nts id="Seg_4662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_4664" n="HIAT:w" s="T450">как</ts>
                  <nts id="Seg_4665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_4667" n="HIAT:w" s="T451">он</ts>
                  <nts id="Seg_4668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_4670" n="HIAT:w" s="T452">сошелся</ts>
                  <nts id="Seg_4671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_4673" n="HIAT:w" s="T453">с</ts>
                  <nts id="Seg_4674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_4676" n="HIAT:w" s="T454">ей</ts>
                  <nts id="Seg_4677" n="HIAT:ip">.</nts>
                  <nts id="Seg_4678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T461" id="Seg_4679" n="sc" s="T458">
               <ts e="T461" id="Seg_4681" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_4683" n="HIAT:w" s="T458">На</ts>
                  <nts id="Seg_4684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_4686" n="HIAT:w" s="T459">свою</ts>
                  <nts id="Seg_4687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_4689" n="HIAT:w" s="T460">наречие</ts>
                  <nts id="Seg_4690" n="HIAT:ip">.</nts>
                  <nts id="Seg_4691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T470" id="Seg_4692" n="sc" s="T462">
               <ts e="T470" id="Seg_4694" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_4696" n="HIAT:w" s="T462">А</ts>
                  <nts id="Seg_4697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_4699" n="HIAT:w" s="T463">по-русски-то</ts>
                  <nts id="Seg_4700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_4702" n="HIAT:w" s="T464">я</ts>
                  <nts id="Seg_4703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_4705" n="HIAT:w" s="T465">не</ts>
                  <nts id="Seg_4706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_4708" n="HIAT:w" s="T468">рассказала</ts>
                  <nts id="Seg_4709" n="HIAT:ip">.</nts>
                  <nts id="Seg_4710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T502" id="Seg_4711" n="sc" s="T497">
               <ts e="T502" id="Seg_4713" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_4715" n="HIAT:w" s="T497">Ну</ts>
                  <nts id="Seg_4716" n="HIAT:ip">,</nts>
                  <nts id="Seg_4717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_4719" n="HIAT:w" s="T498">чего</ts>
                  <nts id="Seg_4720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_4722" n="HIAT:w" s="T499">нового</ts>
                  <nts id="Seg_4723" n="HIAT:ip">…</nts>
                  <nts id="Seg_4724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T525" id="Seg_4725" n="sc" s="T520">
               <ts e="T525" id="Seg_4727" n="HIAT:u" s="T520">
                  <ts e="T523" id="Seg_4729" n="HIAT:w" s="T520">Нет</ts>
                  <nts id="Seg_4730" n="HIAT:ip">,</nts>
                  <nts id="Seg_4731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_4733" n="HIAT:w" s="T523">это</ts>
                  <nts id="Seg_4734" n="HIAT:ip">…</nts>
                  <nts id="Seg_4735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T532" id="Seg_4736" n="sc" s="T527">
               <ts e="T532" id="Seg_4738" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_4740" n="HIAT:w" s="T527">Теперь</ts>
                  <nts id="Seg_4741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_4743" n="HIAT:w" s="T528">у</ts>
                  <nts id="Seg_4744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_4746" n="HIAT:w" s="T529">нас</ts>
                  <nts id="Seg_4747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_4749" n="HIAT:w" s="T530">Загороднюк</ts>
                  <nts id="Seg_4750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_4752" n="HIAT:w" s="T531">помер</ts>
                  <nts id="Seg_4753" n="HIAT:ip">.</nts>
                  <nts id="Seg_4754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T544" id="Seg_4755" n="sc" s="T533">
               <ts e="T544" id="Seg_4757" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_4759" n="HIAT:w" s="T533">Можно</ts>
                  <nts id="Seg_4760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_4762" n="HIAT:w" s="T534">про</ts>
                  <nts id="Seg_4763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_4765" n="HIAT:w" s="T535">его</ts>
                  <nts id="Seg_4766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_4768" n="HIAT:w" s="T536">сказать</ts>
                  <nts id="Seg_4769" n="HIAT:ip">,</nts>
                  <nts id="Seg_4770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_4772" n="HIAT:w" s="T537">ты</ts>
                  <nts id="Seg_4773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_4775" n="HIAT:w" s="T538">не</ts>
                  <nts id="Seg_4776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_4778" n="HIAT:w" s="T539">знал</ts>
                  <nts id="Seg_4779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_4781" n="HIAT:w" s="T540">про</ts>
                  <nts id="Seg_4782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_4784" n="HIAT:w" s="T542">его</ts>
                  <nts id="Seg_4785" n="HIAT:ip">?</nts>
                  <nts id="Seg_4786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T561" id="Seg_4787" n="sc" s="T549">
               <ts e="T561" id="Seg_4789" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_4791" n="HIAT:w" s="T549">Ну</ts>
                  <nts id="Seg_4792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_4794" n="HIAT:w" s="T550">а</ts>
                  <nts id="Seg_4795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_4797" n="HIAT:w" s="T552">ты</ts>
                  <nts id="Seg_4798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_4800" n="HIAT:w" s="T554">не</ts>
                  <nts id="Seg_4801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_4803" n="HIAT:w" s="T556">знал</ts>
                  <nts id="Seg_4804" n="HIAT:ip">,</nts>
                  <nts id="Seg_4805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_4807" n="HIAT:w" s="T558">что</ts>
                  <nts id="Seg_4808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_4810" n="HIAT:w" s="T559">помер</ts>
                  <nts id="Seg_4811" n="HIAT:ip">?</nts>
                  <nts id="Seg_4812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T574" id="Seg_4813" n="sc" s="T564">
               <ts e="T574" id="Seg_4815" n="HIAT:u" s="T564">
                  <nts id="Seg_4816" n="HIAT:ip">(</nts>
                  <ts e="T566" id="Seg_4818" n="HIAT:w" s="T564">Уже=</ts>
                  <nts id="Seg_4819" n="HIAT:ip">)</nts>
                  <nts id="Seg_4820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_4822" n="HIAT:w" s="T566">Уже</ts>
                  <nts id="Seg_4823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_4825" n="HIAT:w" s="T568">два</ts>
                  <nts id="Seg_4826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_4828" n="HIAT:w" s="T570">года</ts>
                  <nts id="Seg_4829" n="HIAT:ip">,</nts>
                  <nts id="Seg_4830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_4832" n="HIAT:w" s="T571">как</ts>
                  <nts id="Seg_4833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_4835" n="HIAT:w" s="T572">помер</ts>
                  <nts id="Seg_4836" n="HIAT:ip">.</nts>
                  <nts id="Seg_4837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T582" id="Seg_4838" n="sc" s="T576">
               <ts e="T582" id="Seg_4840" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_4842" n="HIAT:w" s="T576">Так</ts>
                  <nts id="Seg_4843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_4845" n="HIAT:w" s="T577">я</ts>
                  <nts id="Seg_4846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_4848" n="HIAT:w" s="T578">сильно</ts>
                  <nts id="Seg_4849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_4851" n="HIAT:w" s="T579">плакала</ts>
                  <nts id="Seg_4852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_4854" n="HIAT:w" s="T580">об</ts>
                  <nts id="Seg_4855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_4857" n="HIAT:w" s="T581">ем</ts>
                  <nts id="Seg_4858" n="HIAT:ip">.</nts>
                  <nts id="Seg_4859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T587" id="Seg_4860" n="sc" s="T583">
               <ts e="T587" id="Seg_4862" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_4864" n="HIAT:w" s="T583">Хороший</ts>
                  <nts id="Seg_4865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_4867" n="HIAT:w" s="T584">человек</ts>
                  <nts id="Seg_4868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_4870" n="HIAT:w" s="T585">был</ts>
                  <nts id="Seg_4871" n="HIAT:ip">.</nts>
                  <nts id="Seg_4872" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T606" id="Seg_4873" n="sc" s="T592">
               <ts e="T606" id="Seg_4875" n="HIAT:u" s="T592">
                  <ts e="T594" id="Seg_4877" n="HIAT:w" s="T592">Вот</ts>
                  <nts id="Seg_4878" n="HIAT:ip">,</nts>
                  <nts id="Seg_4879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_4881" n="HIAT:w" s="T594">бывало</ts>
                  <nts id="Seg_4882" n="HIAT:ip">,</nts>
                  <nts id="Seg_4883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_4885" n="HIAT:w" s="T596">приду</ts>
                  <nts id="Seg_4886" n="HIAT:ip">,</nts>
                  <nts id="Seg_4887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_4889" n="HIAT:w" s="T598">куды</ts>
                  <nts id="Seg_4890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_4892" n="HIAT:w" s="T599">мне</ts>
                  <nts id="Seg_4893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_4895" n="HIAT:w" s="T600">надо</ts>
                  <nts id="Seg_4896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_4898" n="HIAT:w" s="T601">ехать</ts>
                  <nts id="Seg_4899" n="HIAT:ip">,</nts>
                  <nts id="Seg_4900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_4902" n="HIAT:w" s="T602">в</ts>
                  <nts id="Seg_4903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_4905" n="HIAT:w" s="T603">Уяр</ts>
                  <nts id="Seg_4906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_4908" n="HIAT:w" s="T604">али</ts>
                  <nts id="Seg_4909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_4911" n="HIAT:w" s="T605">куды</ts>
                  <nts id="Seg_4912" n="HIAT:ip">.</nts>
                  <nts id="Seg_4913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T610" id="Seg_4914" n="sc" s="T607">
               <ts e="T610" id="Seg_4916" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_4918" n="HIAT:w" s="T607">В</ts>
                  <nts id="Seg_4919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_4921" n="HIAT:w" s="T608">Агинско</ts>
                  <nts id="Seg_4922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_4924" n="HIAT:w" s="T609">ли</ts>
                  <nts id="Seg_4925" n="HIAT:ip">.</nts>
                  <nts id="Seg_4926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T625" id="Seg_4927" n="sc" s="T611">
               <ts e="T625" id="Seg_4929" n="HIAT:u" s="T611">
                  <nts id="Seg_4930" n="HIAT:ip">(</nts>
                  <ts e="T612" id="Seg_4932" n="HIAT:w" s="T611">Прид-</ts>
                  <nts id="Seg_4933" n="HIAT:ip">)</nts>
                  <nts id="Seg_4934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_4936" n="HIAT:w" s="T612">Я</ts>
                  <nts id="Seg_4937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_4939" n="HIAT:w" s="T613">все</ts>
                  <nts id="Seg_4940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_4942" n="HIAT:w" s="T614">его</ts>
                  <nts id="Seg_4943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_4945" n="HIAT:w" s="T615">чтоб</ts>
                  <nts id="Seg_4946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_4948" n="HIAT:w" s="T616">одного</ts>
                  <nts id="Seg_4949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_4951" n="HIAT:w" s="T617">захватить</ts>
                  <nts id="Seg_4952" n="HIAT:ip">,</nts>
                  <nts id="Seg_4953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_4955" n="HIAT:w" s="T618">никто</ts>
                  <nts id="Seg_4956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_4958" n="HIAT:w" s="T619">не</ts>
                  <nts id="Seg_4959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_4961" n="HIAT:w" s="T620">слыхал</ts>
                  <nts id="Seg_4962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_4964" n="HIAT:w" s="T621">и</ts>
                  <nts id="Seg_4965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_4967" n="HIAT:w" s="T622">не</ts>
                  <nts id="Seg_4968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_4970" n="HIAT:w" s="T623">завидывал</ts>
                  <nts id="Seg_4971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_4973" n="HIAT:w" s="T624">бы</ts>
                  <nts id="Seg_4974" n="HIAT:ip">.</nts>
                  <nts id="Seg_4975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_4976" n="sc" s="T626">
               <ts e="T631" id="Seg_4978" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_4980" n="HIAT:w" s="T626">Он</ts>
                  <nts id="Seg_4981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_4983" n="HIAT:w" s="T627">же</ts>
                  <nts id="Seg_4984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_4986" n="HIAT:w" s="T628">против</ts>
                  <nts id="Seg_4987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_4989" n="HIAT:w" s="T629">меня</ts>
                  <nts id="Seg_4990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_4992" n="HIAT:w" s="T630">жил</ts>
                  <nts id="Seg_4993" n="HIAT:ip">.</nts>
                  <nts id="Seg_4994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T655" id="Seg_4995" n="sc" s="T632">
               <ts e="T645" id="Seg_4997" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_4999" n="HIAT:w" s="T632">Когда</ts>
                  <nts id="Seg_5000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_5002" n="HIAT:w" s="T633">идет</ts>
                  <nts id="Seg_5003" n="HIAT:ip">,</nts>
                  <nts id="Seg_5004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_5006" n="HIAT:w" s="T634">я</ts>
                  <nts id="Seg_5007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_5009" n="HIAT:w" s="T635">иду</ts>
                  <nts id="Seg_5010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_5012" n="HIAT:w" s="T636">навстречу</ts>
                  <nts id="Seg_5013" n="HIAT:ip">,</nts>
                  <nts id="Seg_5014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_5016" n="HIAT:w" s="T637">скажу:</ts>
                  <nts id="Seg_5017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5018" n="HIAT:ip">"</nts>
                  <ts e="T639" id="Seg_5020" n="HIAT:w" s="T638">Василий</ts>
                  <nts id="Seg_5021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_5023" n="HIAT:w" s="T639">Федорович</ts>
                  <nts id="Seg_5024" n="HIAT:ip">,</nts>
                  <nts id="Seg_5025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5026" n="HIAT:ip">(</nts>
                  <ts e="T641" id="Seg_5028" n="HIAT:w" s="T640">мож-</ts>
                  <nts id="Seg_5029" n="HIAT:ip">)</nts>
                  <nts id="Seg_5030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_5032" n="HIAT:w" s="T641">машины</ts>
                  <nts id="Seg_5033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_5035" n="HIAT:w" s="T642">пойдут</ts>
                  <nts id="Seg_5036" n="HIAT:ip">,</nts>
                  <nts id="Seg_5037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_5039" n="HIAT:w" s="T643">можно</ts>
                  <nts id="Seg_5040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_5042" n="HIAT:w" s="T644">уехать</ts>
                  <nts id="Seg_5043" n="HIAT:ip">?</nts>
                  <nts id="Seg_5044" n="HIAT:ip">"</nts>
                  <nts id="Seg_5045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_5047" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_5049" n="HIAT:w" s="T645">Он</ts>
                  <nts id="Seg_5050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_5052" n="HIAT:w" s="T646">скажет:</ts>
                  <nts id="Seg_5053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5054" n="HIAT:ip">"</nts>
                  <ts e="T648" id="Seg_5056" n="HIAT:w" s="T647">Завтре</ts>
                  <nts id="Seg_5057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_5059" n="HIAT:w" s="T648">со</ts>
                  <nts id="Seg_5060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_5062" n="HIAT:w" s="T649">мной</ts>
                  <nts id="Seg_5063" n="HIAT:ip">"</nts>
                  <nts id="Seg_5064" n="HIAT:ip">.</nts>
                  <nts id="Seg_5065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_5067" n="HIAT:u" s="T650">
                  <nts id="Seg_5068" n="HIAT:ip">"</nts>
                  <ts e="T651" id="Seg_5070" n="HIAT:w" s="T650">А</ts>
                  <nts id="Seg_5071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_5073" n="HIAT:w" s="T651">куды</ts>
                  <nts id="Seg_5074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_5076" n="HIAT:w" s="T652">ты</ts>
                  <nts id="Seg_5077" n="HIAT:ip">,</nts>
                  <nts id="Seg_5078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_5080" n="HIAT:w" s="T653">говорит</ts>
                  <nts id="Seg_5081" n="HIAT:ip">,</nts>
                  <nts id="Seg_5082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_5084" n="HIAT:w" s="T654">хочешь</ts>
                  <nts id="Seg_5085" n="HIAT:ip">?</nts>
                  <nts id="Seg_5086" n="HIAT:ip">"</nts>
                  <nts id="Seg_5087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T672" id="Seg_5088" n="sc" s="T656">
               <ts e="T667" id="Seg_5090" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_5092" n="HIAT:w" s="T656">Я</ts>
                  <nts id="Seg_5093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_5095" n="HIAT:w" s="T657">говорю:</ts>
                  <nts id="Seg_5096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5097" n="HIAT:ip">"</nts>
                  <ts e="T659" id="Seg_5099" n="HIAT:w" s="T658">Да</ts>
                  <nts id="Seg_5100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5101" n="HIAT:ip">(</nts>
                  <nts id="Seg_5102" n="HIAT:ip">(</nts>
                  <ats e="T660" id="Seg_5103" n="HIAT:non-pho" s="T659">…</ats>
                  <nts id="Seg_5104" n="HIAT:ip">)</nts>
                  <nts id="Seg_5105" n="HIAT:ip">)</nts>
                  <nts id="Seg_5106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_5108" n="HIAT:w" s="T660">тебе</ts>
                  <nts id="Seg_5109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_5111" n="HIAT:w" s="T661">как</ts>
                  <nts id="Seg_5112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_5114" n="HIAT:w" s="T662">сыну</ts>
                  <nts id="Seg_5115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_5117" n="HIAT:w" s="T663">расскажу</ts>
                  <nts id="Seg_5118" n="HIAT:ip">,</nts>
                  <nts id="Seg_5119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_5121" n="HIAT:w" s="T664">еду</ts>
                  <nts id="Seg_5122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_5124" n="HIAT:w" s="T665">богу</ts>
                  <nts id="Seg_5125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_5127" n="HIAT:w" s="T666">молиться</ts>
                  <nts id="Seg_5128" n="HIAT:ip">.</nts>
                  <nts id="Seg_5129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_5131" n="HIAT:u" s="T667">
                  <nts id="Seg_5132" n="HIAT:ip">"</nts>
                  <ts e="T668" id="Seg_5134" n="HIAT:w" s="T667">Завтре</ts>
                  <nts id="Seg_5135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_5137" n="HIAT:w" s="T668">обязательно</ts>
                  <nts id="Seg_5138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_5140" n="HIAT:w" s="T669">со</ts>
                  <nts id="Seg_5141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_5143" n="HIAT:w" s="T670">мной</ts>
                  <nts id="Seg_5144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_5146" n="HIAT:w" s="T671">уедешь</ts>
                  <nts id="Seg_5147" n="HIAT:ip">"</nts>
                  <nts id="Seg_5148" n="HIAT:ip">.</nts>
                  <nts id="Seg_5149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T681" id="Seg_5150" n="sc" s="T673">
               <ts e="T681" id="Seg_5152" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_5154" n="HIAT:w" s="T673">Ну</ts>
                  <nts id="Seg_5155" n="HIAT:ip">,</nts>
                  <nts id="Seg_5156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_5158" n="HIAT:w" s="T674">я</ts>
                  <nts id="Seg_5159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_5161" n="HIAT:w" s="T675">на</ts>
                  <nts id="Seg_5162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_5164" n="HIAT:w" s="T676">утро</ts>
                  <nts id="Seg_5165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_5167" n="HIAT:w" s="T677">поднимаюсь</ts>
                  <nts id="Seg_5168" n="HIAT:ip">,</nts>
                  <nts id="Seg_5169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_5171" n="HIAT:w" s="T678">всё</ts>
                  <nts id="Seg_5172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_5174" n="HIAT:w" s="T679">складаю</ts>
                  <nts id="Seg_5175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_5177" n="HIAT:w" s="T680">и</ts>
                  <nts id="Seg_5178" n="HIAT:ip">…</nts>
                  <nts id="Seg_5179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T688" id="Seg_5180" n="sc" s="T682">
               <ts e="T688" id="Seg_5182" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_5184" n="HIAT:w" s="T682">Они</ts>
                  <nts id="Seg_5185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_5187" n="HIAT:w" s="T683">же</ts>
                  <nts id="Seg_5188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5189" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_5191" n="HIAT:w" s="T684">ра-</ts>
                  <nts id="Seg_5192" n="HIAT:ip">)</nts>
                  <nts id="Seg_5193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_5195" n="HIAT:w" s="T685">знаешь</ts>
                  <nts id="Seg_5196" n="HIAT:ip">,</nts>
                  <nts id="Seg_5197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_5199" n="HIAT:w" s="T686">где</ts>
                  <nts id="Seg_5200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_5202" n="HIAT:w" s="T687">жили</ts>
                  <nts id="Seg_5203" n="HIAT:ip">?</nts>
                  <nts id="Seg_5204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T701" id="Seg_5205" n="sc" s="T689">
               <ts e="T701" id="Seg_5207" n="HIAT:u" s="T689">
                  <nts id="Seg_5208" n="HIAT:ip">(</nts>
                  <ts e="T690" id="Seg_5210" n="HIAT:w" s="T689">Сейчас</ts>
                  <nts id="Seg_5211" n="HIAT:ip">)</nts>
                  <nts id="Seg_5212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_5214" n="HIAT:w" s="T690">сюды</ts>
                  <nts id="Seg_5215" n="HIAT:ip">,</nts>
                  <nts id="Seg_5216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_5218" n="HIAT:w" s="T691">к</ts>
                  <nts id="Seg_5219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_5221" n="HIAT:w" s="T692">имя</ts>
                  <nts id="Seg_5222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_5224" n="HIAT:w" s="T693">приду</ts>
                  <nts id="Seg_5225" n="HIAT:ip">,</nts>
                  <nts id="Seg_5226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_5228" n="HIAT:w" s="T694">его</ts>
                  <nts id="Seg_5229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_5231" n="HIAT:w" s="T695">еще</ts>
                  <nts id="Seg_5232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_5234" n="HIAT:w" s="T696">нету</ts>
                  <nts id="Seg_5235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5236" n="HIAT:ip">(</nts>
                  <ts e="T698" id="Seg_5238" n="HIAT:w" s="T697">с</ts>
                  <nts id="Seg_5239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_5241" n="HIAT:w" s="T698">этой=</ts>
                  <nts id="Seg_5242" n="HIAT:ip">)</nts>
                  <nts id="Seg_5243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_5245" n="HIAT:w" s="T699">с</ts>
                  <nts id="Seg_5246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_5248" n="HIAT:w" s="T700">конторы</ts>
                  <nts id="Seg_5249" n="HIAT:ip">.</nts>
                  <nts id="Seg_5250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T717" id="Seg_5251" n="sc" s="T702">
               <ts e="T712" id="Seg_5253" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_5255" n="HIAT:w" s="T702">Я</ts>
                  <nts id="Seg_5256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_5258" n="HIAT:w" s="T703">сижу</ts>
                  <nts id="Seg_5259" n="HIAT:ip">,</nts>
                  <nts id="Seg_5260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_5262" n="HIAT:w" s="T704">он</ts>
                  <nts id="Seg_5263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_5265" n="HIAT:w" s="T705">заходит:</ts>
                  <nts id="Seg_5266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5267" n="HIAT:ip">"</nts>
                  <ts e="T707" id="Seg_5269" n="HIAT:w" s="T706">О</ts>
                  <nts id="Seg_5270" n="HIAT:ip">,</nts>
                  <nts id="Seg_5271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_5273" n="HIAT:w" s="T707">ты</ts>
                  <nts id="Seg_5274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_5276" n="HIAT:w" s="T708">тут</ts>
                  <nts id="Seg_5277" n="HIAT:ip">,</nts>
                  <nts id="Seg_5278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_5280" n="HIAT:w" s="T709">девка</ts>
                  <nts id="Seg_5281" n="HIAT:ip">,</nts>
                  <nts id="Seg_5282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_5284" n="HIAT:w" s="T710">уже</ts>
                  <nts id="Seg_5285" n="HIAT:ip">"</nts>
                  <nts id="Seg_5286" n="HIAT:ip">,</nts>
                  <nts id="Seg_5287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5288" n="HIAT:ip">—</nts>
                  <nts id="Seg_5289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_5291" n="HIAT:w" s="T711">скажет</ts>
                  <nts id="Seg_5292" n="HIAT:ip">.</nts>
                  <nts id="Seg_5293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_5295" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_5297" n="HIAT:w" s="T712">Он</ts>
                  <nts id="Seg_5298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_5300" n="HIAT:w" s="T713">всё</ts>
                  <nts id="Seg_5301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_5303" n="HIAT:w" s="T714">меня</ts>
                  <nts id="Seg_5304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_5306" n="HIAT:w" s="T715">девкой</ts>
                  <nts id="Seg_5307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_5309" n="HIAT:w" s="T716">звал</ts>
                  <nts id="Seg_5310" n="HIAT:ip">.</nts>
                  <nts id="Seg_5311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T720" id="Seg_5312" n="sc" s="T718">
               <ts e="T720" id="Seg_5314" n="HIAT:u" s="T718">
                  <nts id="Seg_5315" n="HIAT:ip">"</nts>
                  <ts e="T718.tx-PKZ.1" id="Seg_5317" n="HIAT:w" s="T718">Тут</ts>
                  <nts id="Seg_5318" n="HIAT:ip">"</nts>
                  <nts id="Seg_5319" n="HIAT:ip">,</nts>
                  <nts id="Seg_5320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_5322" n="HIAT:w" s="T718.tx-PKZ.1">-</ts>
                  <nts id="Seg_5323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_5325" n="HIAT:w" s="T719">говорю</ts>
                  <nts id="Seg_5326" n="HIAT:ip">.</nts>
                  <nts id="Seg_5327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T727" id="Seg_5328" n="sc" s="T721">
               <ts e="T727" id="Seg_5330" n="HIAT:u" s="T721">
                  <nts id="Seg_5331" n="HIAT:ip">"</nts>
                  <ts e="T722" id="Seg_5333" n="HIAT:w" s="T721">Я</ts>
                  <nts id="Seg_5334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_5336" n="HIAT:w" s="T722">хотел</ts>
                  <nts id="Seg_5337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_5339" n="HIAT:w" s="T723">послать</ts>
                  <nts id="Seg_5340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_5342" n="HIAT:w" s="T724">Линку</ts>
                  <nts id="Seg_5343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_5345" n="HIAT:w" s="T725">за</ts>
                  <nts id="Seg_5346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_5348" n="HIAT:w" s="T726">тобой</ts>
                  <nts id="Seg_5349" n="HIAT:ip">"</nts>
                  <nts id="Seg_5350" n="HIAT:ip">.</nts>
                  <nts id="Seg_5351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T735" id="Seg_5352" n="sc" s="T728">
               <ts e="T735" id="Seg_5354" n="HIAT:u" s="T728">
                  <ts e="T729" id="Seg_5356" n="HIAT:w" s="T728">Ее</ts>
                  <nts id="Seg_5357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_5359" n="HIAT:w" s="T729">звать</ts>
                  <nts id="Seg_5360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_5362" n="HIAT:w" s="T730">Лизавета</ts>
                  <nts id="Seg_5363" n="HIAT:ip">,</nts>
                  <nts id="Seg_5364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_5366" n="HIAT:w" s="T731">он</ts>
                  <nts id="Seg_5367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_5369" n="HIAT:w" s="T732">всё</ts>
                  <nts id="Seg_5370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_5372" n="HIAT:w" s="T733">Линкой</ts>
                  <nts id="Seg_5373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_5375" n="HIAT:w" s="T734">звал</ts>
                  <nts id="Seg_5376" n="HIAT:ip">.</nts>
                  <nts id="Seg_5377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T742" id="Seg_5378" n="sc" s="T736">
               <ts e="T742" id="Seg_5380" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_5382" n="HIAT:w" s="T736">А</ts>
                  <nts id="Seg_5383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_5385" n="HIAT:w" s="T737">ее</ts>
                  <nts id="Seg_5386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_5388" n="HIAT:w" s="T738">так</ts>
                  <nts id="Seg_5389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_5391" n="HIAT:w" s="T739">и</ts>
                  <nts id="Seg_5392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_5394" n="HIAT:w" s="T740">звали</ts>
                  <nts id="Seg_5395" n="HIAT:ip">,</nts>
                  <nts id="Seg_5396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_5398" n="HIAT:w" s="T741">Линкой</ts>
                  <nts id="Seg_5399" n="HIAT:ip">.</nts>
                  <nts id="Seg_5400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_5401" n="sc" s="T743">
               <ts e="T750" id="Seg_5403" n="HIAT:u" s="T743">
                  <ts e="T744" id="Seg_5405" n="HIAT:w" s="T743">Ну</ts>
                  <nts id="Seg_5406" n="HIAT:ip">,</nts>
                  <nts id="Seg_5407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_5409" n="HIAT:w" s="T744">мы</ts>
                  <nts id="Seg_5410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_5412" n="HIAT:w" s="T745">с</ts>
                  <nts id="Seg_5413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_5415" n="HIAT:w" s="T746">им</ts>
                  <nts id="Seg_5416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_5418" n="HIAT:w" s="T747">приехали</ts>
                  <nts id="Seg_5419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_5421" n="HIAT:w" s="T748">к</ts>
                  <nts id="Seg_5422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_5424" n="HIAT:w" s="T749">конторе</ts>
                  <nts id="Seg_5425" n="HIAT:ip">.</nts>
                  <nts id="Seg_5426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T761" id="Seg_5427" n="sc" s="T751">
               <ts e="T761" id="Seg_5429" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_5431" n="HIAT:w" s="T751">А</ts>
                  <nts id="Seg_5432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_5434" n="HIAT:w" s="T752">там</ts>
                  <nts id="Seg_5435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_5437" n="HIAT:w" s="T753">одна</ts>
                  <nts id="Seg_5438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_5440" n="HIAT:w" s="T754">женщина</ts>
                  <nts id="Seg_5441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_5443" n="HIAT:w" s="T755">подбежала</ts>
                  <nts id="Seg_5444" n="HIAT:ip">,</nts>
                  <nts id="Seg_5445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_5447" n="HIAT:w" s="T756">говорит:</ts>
                  <nts id="Seg_5448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5449" n="HIAT:ip">"</nts>
                  <ts e="T758" id="Seg_5451" n="HIAT:w" s="T757">Может</ts>
                  <nts id="Seg_5452" n="HIAT:ip">,</nts>
                  <nts id="Seg_5453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_5455" n="HIAT:w" s="T758">и</ts>
                  <nts id="Seg_5456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_5458" n="HIAT:w" s="T759">меня</ts>
                  <nts id="Seg_5459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5460" n="HIAT:ip">(</nts>
                  <ts e="T761" id="Seg_5462" n="HIAT:w" s="T760">возьмете</ts>
                  <nts id="Seg_5463" n="HIAT:ip">)</nts>
                  <nts id="Seg_5464" n="HIAT:ip">?</nts>
                  <nts id="Seg_5465" n="HIAT:ip">"</nts>
                  <nts id="Seg_5466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T771" id="Seg_5467" n="sc" s="T762">
               <ts e="T771" id="Seg_5469" n="HIAT:u" s="T762">
                  <nts id="Seg_5470" n="HIAT:ip">"</nts>
                  <ts e="T763" id="Seg_5472" n="HIAT:w" s="T762">Нет</ts>
                  <nts id="Seg_5473" n="HIAT:ip">,</nts>
                  <nts id="Seg_5474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_5476" n="HIAT:w" s="T763">говорит</ts>
                  <nts id="Seg_5477" n="HIAT:ip">,</nts>
                  <nts id="Seg_5478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_5480" n="HIAT:w" s="T764">никого</ts>
                  <nts id="Seg_5481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_5483" n="HIAT:w" s="T765">не</ts>
                  <nts id="Seg_5484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_5486" n="HIAT:w" s="T766">возьму</ts>
                  <nts id="Seg_5487" n="HIAT:ip">"</nts>
                  <nts id="Seg_5488" n="HIAT:ip">,</nts>
                  <nts id="Seg_5489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5490" n="HIAT:ip">—</nts>
                  <nts id="Seg_5491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_5493" n="HIAT:w" s="T767">а</ts>
                  <nts id="Seg_5494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_5496" n="HIAT:w" s="T768">я</ts>
                  <nts id="Seg_5497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_5499" n="HIAT:w" s="T769">там</ts>
                  <nts id="Seg_5500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_5502" n="HIAT:w" s="T770">сижу</ts>
                  <nts id="Seg_5503" n="HIAT:ip">.</nts>
                  <nts id="Seg_5504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T777" id="Seg_5505" n="sc" s="T772">
               <ts e="T777" id="Seg_5507" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_5509" n="HIAT:w" s="T772">Думаю</ts>
                  <nts id="Seg_5510" n="HIAT:ip">,</nts>
                  <nts id="Seg_5511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_5513" n="HIAT:w" s="T773">может</ts>
                  <nts id="Seg_5514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_5516" n="HIAT:w" s="T774">еще</ts>
                  <nts id="Seg_5517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_5519" n="HIAT:w" s="T775">кто</ts>
                  <nts id="Seg_5520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_5522" n="HIAT:w" s="T776">поедет</ts>
                  <nts id="Seg_5523" n="HIAT:ip">.</nts>
                  <nts id="Seg_5524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T786" id="Seg_5525" n="sc" s="T778">
               <ts e="T786" id="Seg_5527" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_5529" n="HIAT:w" s="T778">Взяли</ts>
                  <nts id="Seg_5530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_5532" n="HIAT:w" s="T779">трое</ts>
                  <nts id="Seg_5533" n="HIAT:ip">,</nts>
                  <nts id="Seg_5534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_5536" n="HIAT:w" s="T780">поехали</ts>
                  <nts id="Seg_5537" n="HIAT:ip">,</nts>
                  <nts id="Seg_5538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_5540" n="HIAT:w" s="T781">а</ts>
                  <nts id="Seg_5541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_5543" n="HIAT:w" s="T782">тую</ts>
                  <nts id="Seg_5544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_5546" n="HIAT:w" s="T783">женщину</ts>
                  <nts id="Seg_5547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_5549" n="HIAT:w" s="T784">не</ts>
                  <nts id="Seg_5550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_5552" n="HIAT:w" s="T785">взял</ts>
                  <nts id="Seg_5553" n="HIAT:ip">.</nts>
                  <nts id="Seg_5554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T792" id="Seg_5555" n="sc" s="T787">
               <ts e="T792" id="Seg_5557" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_5559" n="HIAT:w" s="T787">Как</ts>
                  <nts id="Seg_5560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_5562" n="HIAT:w" s="T788">я</ts>
                  <nts id="Seg_5563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_5565" n="HIAT:w" s="T789">скажу</ts>
                  <nts id="Seg_5566" n="HIAT:ip">,</nts>
                  <nts id="Seg_5567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_5569" n="HIAT:w" s="T790">обязательно</ts>
                  <nts id="Seg_5570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_5572" n="HIAT:w" s="T791">возьмет</ts>
                  <nts id="Seg_5573" n="HIAT:ip">.</nts>
                  <nts id="Seg_5574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T797" id="Seg_5575" n="sc" s="T793">
               <ts e="T797" id="Seg_5577" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_5579" n="HIAT:w" s="T793">Если</ts>
                  <nts id="Seg_5580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_5582" n="HIAT:w" s="T794">сам</ts>
                  <nts id="Seg_5583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_5585" n="HIAT:w" s="T795">не</ts>
                  <nts id="Seg_5586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_5588" n="HIAT:w" s="T796">поедет</ts>
                  <nts id="Seg_5589" n="HIAT:ip">…</nts>
                  <nts id="Seg_5590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T806" id="Seg_5591" n="sc" s="T798">
               <ts e="T806" id="Seg_5593" n="HIAT:u" s="T798">
                  <ts e="T799" id="Seg_5595" n="HIAT:w" s="T798">Спрошу</ts>
                  <nts id="Seg_5596" n="HIAT:ip">,</nts>
                  <nts id="Seg_5597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_5599" n="HIAT:w" s="T799">сам</ts>
                  <nts id="Seg_5600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_5602" n="HIAT:w" s="T800">не</ts>
                  <nts id="Seg_5603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_5605" n="HIAT:w" s="T801">поедет:</ts>
                  <nts id="Seg_5606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5607" n="HIAT:ip">"</nts>
                  <ts e="T803" id="Seg_5609" n="HIAT:w" s="T802">Ладно</ts>
                  <nts id="Seg_5610" n="HIAT:ip">,</nts>
                  <nts id="Seg_5611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_5613" n="HIAT:w" s="T803">я</ts>
                  <nts id="Seg_5614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_5616" n="HIAT:w" s="T804">скажу</ts>
                  <nts id="Seg_5617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_5619" n="HIAT:w" s="T805">тебе</ts>
                  <nts id="Seg_5620" n="HIAT:ip">"</nts>
                  <nts id="Seg_5621" n="HIAT:ip">.</nts>
                  <nts id="Seg_5622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T818" id="Seg_5623" n="sc" s="T807">
               <ts e="T818" id="Seg_5625" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_5627" n="HIAT:w" s="T807">Думаю:</ts>
                  <nts id="Seg_5628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_5630" n="HIAT:w" s="T808">ну</ts>
                  <nts id="Seg_5631" n="HIAT:ip">,</nts>
                  <nts id="Seg_5632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_5634" n="HIAT:w" s="T809">соберусь</ts>
                  <nts id="Seg_5635" n="HIAT:ip">,</nts>
                  <nts id="Seg_5636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_5638" n="HIAT:w" s="T810">пойду</ts>
                  <nts id="Seg_5639" n="HIAT:ip">,</nts>
                  <nts id="Seg_5640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_5642" n="HIAT:w" s="T811">а</ts>
                  <nts id="Seg_5643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_5645" n="HIAT:w" s="T812">то</ts>
                  <nts id="Seg_5646" n="HIAT:ip">,</nts>
                  <nts id="Seg_5647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_5649" n="HIAT:w" s="T813">говорю</ts>
                  <nts id="Seg_5650" n="HIAT:ip">,</nts>
                  <nts id="Seg_5651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_5653" n="HIAT:w" s="T814">как</ts>
                  <nts id="Seg_5654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_5656" n="HIAT:w" s="T815">он</ts>
                  <nts id="Seg_5657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_5659" n="HIAT:w" s="T816">будет</ts>
                  <nts id="Seg_5660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_5662" n="HIAT:w" s="T817">знать</ts>
                  <nts id="Seg_5663" n="HIAT:ip">.</nts>
                  <nts id="Seg_5664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T823" id="Seg_5665" n="sc" s="T819">
               <ts e="T823" id="Seg_5667" n="HIAT:u" s="T819">
                  <ts e="T819.tx-PKZ.1" id="Seg_5669" n="HIAT:w" s="T819">Забудет</ts>
                  <nts id="Seg_5670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_5672" n="HIAT:w" s="T819.tx-PKZ.1">-</ts>
                  <nts id="Seg_5673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_5675" n="HIAT:w" s="T820">народу</ts>
                  <nts id="Seg_5676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_5678" n="HIAT:w" s="T821">же</ts>
                  <nts id="Seg_5679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_5681" n="HIAT:w" s="T822">много</ts>
                  <nts id="Seg_5682" n="HIAT:ip">…</nts>
                  <nts id="Seg_5683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T834" id="Seg_5684" n="sc" s="T824">
               <ts e="T834" id="Seg_5686" n="HIAT:u" s="T824">
                  <ts e="T825" id="Seg_5688" n="HIAT:w" s="T824">Я</ts>
                  <nts id="Seg_5689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_5691" n="HIAT:w" s="T825">иду</ts>
                  <nts id="Seg_5692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5693" n="HIAT:ip">(</nts>
                  <ts e="T827" id="Seg_5695" n="HIAT:w" s="T826">встреча</ts>
                  <nts id="Seg_5696" n="HIAT:ip">)</nts>
                  <nts id="Seg_5697" n="HIAT:ip">,</nts>
                  <nts id="Seg_5698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_5700" n="HIAT:w" s="T827">а</ts>
                  <nts id="Seg_5701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_5703" n="HIAT:w" s="T828">там</ts>
                  <nts id="Seg_5704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5705" n="HIAT:ip">—</nts>
                  <nts id="Seg_5706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_5708" n="HIAT:w" s="T829">Ты</ts>
                  <nts id="Seg_5709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_5711" n="HIAT:w" s="T830">Августа-то</ts>
                  <nts id="Seg_5712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_5714" n="HIAT:w" s="T831">знаешь</ts>
                  <nts id="Seg_5715" n="HIAT:ip">,</nts>
                  <nts id="Seg_5716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5717" n="HIAT:ip">(</nts>
                  <ts e="T833" id="Seg_5719" n="HIAT:w" s="T832">немец</ts>
                  <nts id="Seg_5720" n="HIAT:ip">)</nts>
                  <nts id="Seg_5721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_5723" n="HIAT:w" s="T833">наш</ts>
                  <nts id="Seg_5724" n="HIAT:ip">.</nts>
                  <nts id="Seg_5725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T838" id="Seg_5726" n="sc" s="T835">
               <ts e="T838" id="Seg_5728" n="HIAT:u" s="T835">
                  <ts e="T836" id="Seg_5730" n="HIAT:w" s="T835">Он</ts>
                  <nts id="Seg_5731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_5733" n="HIAT:w" s="T836">бригадир</ts>
                  <nts id="Seg_5734" n="HIAT:ip">,</nts>
                  <nts id="Seg_5735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_5737" n="HIAT:w" s="T837">Август</ts>
                  <nts id="Seg_5738" n="HIAT:ip">.</nts>
                  <nts id="Seg_5739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T854" id="Seg_5740" n="sc" s="T845">
               <ts e="T854" id="Seg_5742" n="HIAT:u" s="T845">
                  <ts e="T848" id="Seg_5744" n="HIAT:w" s="T845">Он</ts>
                  <nts id="Seg_5745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_5747" n="HIAT:w" s="T848">уехал</ts>
                  <nts id="Seg_5748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_5750" n="HIAT:w" s="T851">сейчас</ts>
                  <nts id="Seg_5751" n="HIAT:ip">.</nts>
                  <nts id="Seg_5752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T859" id="Seg_5753" n="sc" s="T856">
               <ts e="T859" id="Seg_5755" n="HIAT:u" s="T856">
                  <ts e="T858" id="Seg_5757" n="HIAT:w" s="T856">В</ts>
                  <nts id="Seg_5758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_5760" n="HIAT:w" s="T858">Волгоград</ts>
                  <nts id="Seg_5761" n="HIAT:ip">.</nts>
                  <nts id="Seg_5762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T871" id="Seg_5763" n="sc" s="T860">
               <ts e="T871" id="Seg_5765" n="HIAT:u" s="T860">
                  <ts e="T861" id="Seg_5767" n="HIAT:w" s="T860">У</ts>
                  <nts id="Seg_5768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_5770" n="HIAT:w" s="T861">его</ts>
                  <nts id="Seg_5771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_5773" n="HIAT:w" s="T862">сын</ts>
                  <nts id="Seg_5774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_5776" n="HIAT:w" s="T863">женился</ts>
                  <nts id="Seg_5777" n="HIAT:ip">,</nts>
                  <nts id="Seg_5778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_5780" n="HIAT:w" s="T864">тоже</ts>
                  <nts id="Seg_5781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_5783" n="HIAT:w" s="T865">на</ts>
                  <nts id="Seg_5784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_5786" n="HIAT:w" s="T866">волгоградской</ts>
                  <nts id="Seg_5787" n="HIAT:ip">,</nts>
                  <nts id="Seg_5788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_5790" n="HIAT:w" s="T867">и</ts>
                  <nts id="Seg_5791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_5793" n="HIAT:w" s="T868">уехал</ts>
                  <nts id="Seg_5794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_5796" n="HIAT:w" s="T869">в</ts>
                  <nts id="Seg_5797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_5799" n="HIAT:w" s="T870">Волгоград</ts>
                  <nts id="Seg_5800" n="HIAT:ip">.</nts>
                  <nts id="Seg_5801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T878" id="Seg_5802" n="sc" s="T872">
               <ts e="T878" id="Seg_5804" n="HIAT:u" s="T872">
                  <ts e="T873" id="Seg_5806" n="HIAT:w" s="T872">Там</ts>
                  <nts id="Seg_5807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_5809" n="HIAT:w" s="T873">у</ts>
                  <nts id="Seg_5810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_5812" n="HIAT:w" s="T874">нас</ts>
                  <nts id="Seg_5813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_5815" n="HIAT:w" s="T875">всякие</ts>
                  <nts id="Seg_5816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_5818" n="HIAT:w" s="T876">теперь</ts>
                  <nts id="Seg_5819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_5821" n="HIAT:w" s="T877">люди</ts>
                  <nts id="Seg_5822" n="HIAT:ip">.</nts>
                  <nts id="Seg_5823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T888" id="Seg_5824" n="sc" s="T879">
               <ts e="T888" id="Seg_5826" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_5828" n="HIAT:w" s="T879">Вот</ts>
                  <nts id="Seg_5829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_5831" n="HIAT:w" s="T880">после</ts>
                  <nts id="Seg_5832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_5834" n="HIAT:w" s="T881">тебя</ts>
                  <nts id="Seg_5835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_5837" n="HIAT:w" s="T882">сюды</ts>
                  <nts id="Seg_5838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_5840" n="HIAT:w" s="T883">к</ts>
                  <nts id="Seg_5841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_5843" n="HIAT:w" s="T884">Малиновке</ts>
                  <nts id="Seg_5844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_5846" n="HIAT:w" s="T885">целая</ts>
                  <nts id="Seg_5847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_5849" n="HIAT:w" s="T886">деревня</ts>
                  <nts id="Seg_5850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_5852" n="HIAT:w" s="T887">настроилась</ts>
                  <nts id="Seg_5853" n="HIAT:ip">.</nts>
                  <nts id="Seg_5854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T905" id="Seg_5855" n="sc" s="T889">
               <ts e="T905" id="Seg_5857" n="HIAT:u" s="T889">
                  <ts e="T890" id="Seg_5859" n="HIAT:w" s="T889">И</ts>
                  <nts id="Seg_5860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5861" n="HIAT:ip">(</nts>
                  <ts e="T891" id="Seg_5863" n="HIAT:w" s="T890">сю-</ts>
                  <nts id="Seg_5864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_5866" n="HIAT:w" s="T891">сюды</ts>
                  <nts id="Seg_5867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_5869" n="HIAT:w" s="T892">к</ts>
                  <nts id="Seg_5870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_5872" n="HIAT:w" s="T893">М-</ts>
                  <nts id="Seg_5873" n="HIAT:ip">)</nts>
                  <nts id="Seg_5874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_5876" n="HIAT:w" s="T894">сюды</ts>
                  <nts id="Seg_5877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_5879" n="HIAT:w" s="T895">вот</ts>
                  <nts id="Seg_5880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_5882" n="HIAT:w" s="T896">на</ts>
                  <nts id="Seg_5883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_5885" n="HIAT:w" s="T897">Пермяково</ts>
                  <nts id="Seg_5886" n="HIAT:ip">,</nts>
                  <nts id="Seg_5887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_5889" n="HIAT:w" s="T898">сюды</ts>
                  <nts id="Seg_5890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_5892" n="HIAT:w" s="T899">к</ts>
                  <nts id="Seg_5893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_5895" n="HIAT:w" s="T900">Малиновке</ts>
                  <nts id="Seg_5896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_5898" n="HIAT:w" s="T901">чуть</ts>
                  <nts id="Seg_5899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_5901" n="HIAT:w" s="T902">не</ts>
                  <nts id="Seg_5902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_5904" n="HIAT:w" s="T903">до</ts>
                  <nts id="Seg_5905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_5907" n="HIAT:w" s="T904">кладбища</ts>
                  <nts id="Seg_5908" n="HIAT:ip">.</nts>
                  <nts id="Seg_5909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T915" id="Seg_5910" n="sc" s="T909">
               <ts e="T915" id="Seg_5912" n="HIAT:u" s="T909">
                  <nts id="Seg_5913" n="HIAT:ip">(</nts>
                  <ts e="T910" id="Seg_5915" n="HIAT:w" s="T909">И</ts>
                  <nts id="Seg_5916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_5918" n="HIAT:w" s="T910">во-</ts>
                  <nts id="Seg_5919" n="HIAT:ip">)</nts>
                  <nts id="Seg_5920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_5922" n="HIAT:w" s="T912">И</ts>
                  <nts id="Seg_5923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_5925" n="HIAT:w" s="T913">вот</ts>
                  <nts id="Seg_5926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_5928" n="HIAT:w" s="T914">это</ts>
                  <nts id="Seg_5929" n="HIAT:ip">…</nts>
                  <nts id="Seg_5930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T918" id="Seg_5931" n="sc" s="T916">
               <ts e="T918" id="Seg_5933" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_5935" n="HIAT:w" s="T916">Опять</ts>
                  <nts id="Seg_5936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5937" n="HIAT:ip">(</nts>
                  <nts id="Seg_5938" n="HIAT:ip">(</nts>
                  <ats e="T918" id="Seg_5939" n="HIAT:non-pho" s="T917">…</ats>
                  <nts id="Seg_5940" n="HIAT:ip">)</nts>
                  <nts id="Seg_5941" n="HIAT:ip">)</nts>
                  <nts id="Seg_5942" n="HIAT:ip">.</nts>
                  <nts id="Seg_5943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T933" id="Seg_5944" n="sc" s="T929">
               <ts e="T933" id="Seg_5946" n="HIAT:u" s="T929">
                  <ts e="T933" id="Seg_5948" n="HIAT:w" s="T929">Загороднюка</ts>
                  <nts id="Seg_5949" n="HIAT:ip">?</nts>
                  <nts id="Seg_5950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T954" id="Seg_5951" n="sc" s="T949">
               <ts e="T954" id="Seg_5953" n="HIAT:u" s="T949">
                  <ts e="T951" id="Seg_5955" n="HIAT:w" s="T949">Ну</ts>
                  <nts id="Seg_5956" n="HIAT:ip">,</nts>
                  <nts id="Seg_5957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_5959" n="HIAT:w" s="T951">пусти</ts>
                  <nts id="Seg_5960" n="HIAT:ip">.</nts>
                  <nts id="Seg_5961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T959" id="Seg_5962" n="sc" s="T957">
               <ts e="T959" id="Seg_5964" n="HIAT:u" s="T957">
                  <ts e="T959" id="Seg_5966" n="HIAT:w" s="T957">Записаны</ts>
                  <nts id="Seg_5967" n="HIAT:ip">?</nts>
                  <nts id="Seg_5968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T977" id="Seg_5969" n="sc" s="T969">
               <ts e="T977" id="Seg_5971" n="HIAT:u" s="T969">
                  <nts id="Seg_5972" n="HIAT:ip">(</nts>
                  <ts e="T970" id="Seg_5974" n="HIAT:w" s="T969">Каmen=</ts>
                  <nts id="Seg_5975" n="HIAT:ip">)</nts>
                  <nts id="Seg_5976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_5978" n="HIAT:w" s="T970">miʔ</ts>
                  <nts id="Seg_5979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_5981" n="HIAT:w" s="T971">külambi</ts>
                  <nts id="Seg_5982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_5984" n="HIAT:w" s="T972">Zagorodnʼuk</ts>
                  <nts id="Seg_5985" n="HIAT:ip">,</nts>
                  <nts id="Seg_5986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_5988" n="HIAT:w" s="T973">uže</ts>
                  <nts id="Seg_5989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5990" n="HIAT:ip">(</nts>
                  <ts e="T975" id="Seg_5992" n="HIAT:w" s="T974">sĭ-</ts>
                  <nts id="Seg_5993" n="HIAT:ip">)</nts>
                  <nts id="Seg_5994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_5996" n="HIAT:w" s="T975">šide</ts>
                  <nts id="Seg_5997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_5999" n="HIAT:w" s="T976">kö</ts>
                  <nts id="Seg_6000" n="HIAT:ip">.</nts>
                  <nts id="Seg_6001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T986" id="Seg_6002" n="sc" s="T978">
               <ts e="T986" id="Seg_6004" n="HIAT:u" s="T978">
                  <ts e="T979" id="Seg_6006" n="HIAT:w" s="T978">Kamen</ts>
                  <nts id="Seg_6007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_6009" n="HIAT:w" s="T979">măn</ts>
                  <nts id="Seg_6010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_6012" n="HIAT:w" s="T980">gibər</ts>
                  <nts id="Seg_6013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_6015" n="HIAT:w" s="T981">kalam</ts>
                  <nts id="Seg_6016" n="HIAT:ip">,</nts>
                  <nts id="Seg_6017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_6019" n="HIAT:w" s="T982">mănlăm</ts>
                  <nts id="Seg_6020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_6022" n="HIAT:w" s="T983">dĭʔnə:</ts>
                  <nts id="Seg_6023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6024" n="HIAT:ip">"</nts>
                  <nts id="Seg_6025" n="HIAT:ip">(</nts>
                  <ts e="T985" id="Seg_6027" n="HIAT:w" s="T984">I-</ts>
                  <nts id="Seg_6028" n="HIAT:ip">)</nts>
                  <nts id="Seg_6029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_6031" n="HIAT:w" s="T985">Kalal</ts>
                  <nts id="Seg_6032" n="HIAT:ip">?</nts>
                  <nts id="Seg_6033" n="HIAT:ip">"</nts>
                  <nts id="Seg_6034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T989" id="Seg_6035" n="sc" s="T987">
               <ts e="T989" id="Seg_6037" n="HIAT:u" s="T987">
                  <nts id="Seg_6038" n="HIAT:ip">"</nts>
                  <ts e="T988" id="Seg_6040" n="HIAT:w" s="T987">Nu</ts>
                  <nts id="Seg_6041" n="HIAT:ip">,</nts>
                  <nts id="Seg_6042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_6044" n="HIAT:w" s="T988">šoʔ</ts>
                  <nts id="Seg_6045" n="HIAT:ip">"</nts>
                  <nts id="Seg_6046" n="HIAT:ip">.</nts>
                  <nts id="Seg_6047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T993" id="Seg_6048" n="sc" s="T990">
               <ts e="T993" id="Seg_6050" n="HIAT:u" s="T990">
                  <nts id="Seg_6051" n="HIAT:ip">(</nts>
                  <ts e="T991" id="Seg_6053" n="HIAT:w" s="T990">Măn=</ts>
                  <nts id="Seg_6054" n="HIAT:ip">)</nts>
                  <nts id="Seg_6055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_6057" n="HIAT:w" s="T991">Măn</ts>
                  <nts id="Seg_6058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_6060" n="HIAT:w" s="T992">šobiam</ts>
                  <nts id="Seg_6061" n="HIAT:ip">.</nts>
                  <nts id="Seg_6062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T999" id="Seg_6063" n="sc" s="T994">
               <ts e="T999" id="Seg_6065" n="HIAT:u" s="T994">
                  <ts e="T995" id="Seg_6067" n="HIAT:w" s="T994">Dĭgəttə</ts>
                  <nts id="Seg_6068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_6070" n="HIAT:w" s="T995">dĭ</ts>
                  <nts id="Seg_6071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_6073" n="HIAT:w" s="T996">măndə:</ts>
                  <nts id="Seg_6074" n="HIAT:ip">"</nts>
                  <nts id="Seg_6075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6076" n="HIAT:ip">"</nts>
                  <ts e="T998" id="Seg_6078" n="HIAT:w" s="T997">Tăn</ts>
                  <nts id="Seg_6079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_6081" n="HIAT:w" s="T998">šobial</ts>
                  <nts id="Seg_6082" n="HIAT:ip">.</nts>
                  <nts id="Seg_6083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1007" id="Seg_6084" n="sc" s="T1000">
               <ts e="T1007" id="Seg_6086" n="HIAT:u" s="T1000">
                  <ts e="T1001" id="Seg_6088" n="HIAT:w" s="T1000">A</ts>
                  <nts id="Seg_6089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_6091" n="HIAT:w" s="T1001">măn</ts>
                  <nts id="Seg_6092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_6094" n="HIAT:w" s="T1002">öʔlusʼtə</ts>
                  <nts id="Seg_6095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_6097" n="HIAT:w" s="T1003">xatʼel</ts>
                  <nts id="Seg_6098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_6100" n="HIAT:w" s="T1004">bostə</ts>
                  <nts id="Seg_6101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_6103" n="HIAT:w" s="T1005">nen</ts>
                  <nts id="Seg_6104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_6106" n="HIAT:w" s="T1006">tănzi</ts>
                  <nts id="Seg_6107" n="HIAT:ip">"</nts>
                  <nts id="Seg_6108" n="HIAT:ip">.</nts>
                  <nts id="Seg_6109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1012" id="Seg_6110" n="sc" s="T1008">
               <ts e="T1012" id="Seg_6112" n="HIAT:u" s="T1008">
                  <ts e="T1009" id="Seg_6114" n="HIAT:w" s="T1008">Dĭgəttə</ts>
                  <nts id="Seg_6115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_6117" n="HIAT:w" s="T1009">amnolbibaʔ</ts>
                  <nts id="Seg_6118" n="HIAT:ip">,</nts>
                  <nts id="Seg_6119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_6121" n="HIAT:w" s="T1010">kambibaʔ</ts>
                  <nts id="Seg_6122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_6124" n="HIAT:w" s="T1011">kăntoranə</ts>
                  <nts id="Seg_6125" n="HIAT:ip">.</nts>
                  <nts id="Seg_6126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1027" id="Seg_6127" n="sc" s="T1013">
               <ts e="T1020" id="Seg_6129" n="HIAT:u" s="T1013">
                  <ts e="T1014" id="Seg_6131" n="HIAT:w" s="T1013">Dĭ</ts>
                  <nts id="Seg_6132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6133" n="HIAT:ip">(</nts>
                  <ts e="T1015" id="Seg_6135" n="HIAT:w" s="T1014">другой=</ts>
                  <nts id="Seg_6136" n="HIAT:ip">)</nts>
                  <nts id="Seg_6137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_6139" n="HIAT:w" s="T1015">baška</ts>
                  <nts id="Seg_6140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_6142" n="HIAT:w" s="T1016">nüke</ts>
                  <nts id="Seg_6143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_6145" n="HIAT:w" s="T1017">šobi:</ts>
                  <nts id="Seg_6146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6147" n="HIAT:ip">"</nts>
                  <ts e="T1019" id="Seg_6149" n="HIAT:w" s="T1018">Măna</ts>
                  <nts id="Seg_6150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_6152" n="HIAT:w" s="T1019">iləl</ts>
                  <nts id="Seg_6153" n="HIAT:ip">?</nts>
                  <nts id="Seg_6154" n="HIAT:ip">"</nts>
                  <nts id="Seg_6155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1027" id="Seg_6157" n="HIAT:u" s="T1020">
                  <nts id="Seg_6158" n="HIAT:ip">(</nts>
                  <ts e="T1021" id="Seg_6160" n="HIAT:w" s="T1020">Măn=</ts>
                  <nts id="Seg_6161" n="HIAT:ip">)</nts>
                  <nts id="Seg_6162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_6164" n="HIAT:w" s="T1021">Dĭ</ts>
                  <nts id="Seg_6165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_6167" n="HIAT:w" s="T1022">mămbi:</ts>
                  <nts id="Seg_6168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6169" n="HIAT:ip">"</nts>
                  <ts e="T1024" id="Seg_6171" n="HIAT:w" s="T1023">Šindimdə</ts>
                  <nts id="Seg_6172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6173" n="HIAT:ip">(</nts>
                  <ts e="T1025" id="Seg_6175" n="HIAT:w" s="T1024">em=</ts>
                  <nts id="Seg_6176" n="HIAT:ip">)</nts>
                  <nts id="Seg_6177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_6179" n="HIAT:w" s="T1025">ej</ts>
                  <nts id="Seg_6180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_6182" n="HIAT:w" s="T1026">iləm</ts>
                  <nts id="Seg_6183" n="HIAT:ip">"</nts>
                  <nts id="Seg_6184" n="HIAT:ip">.</nts>
                  <nts id="Seg_6185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1033" id="Seg_6186" n="sc" s="T1028">
               <ts e="T1033" id="Seg_6188" n="HIAT:u" s="T1028">
                  <ts e="T1029" id="Seg_6190" n="HIAT:w" s="T1028">Măn</ts>
                  <nts id="Seg_6191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_6193" n="HIAT:w" s="T1029">tenəbiem:</ts>
                  <nts id="Seg_6194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_6196" n="HIAT:w" s="T1030">išo</ts>
                  <nts id="Seg_6197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_6199" n="HIAT:w" s="T1031">šində</ts>
                  <nts id="Seg_6200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_6202" n="HIAT:w" s="T1032">kaləj</ts>
                  <nts id="Seg_6203" n="HIAT:ip">.</nts>
                  <nts id="Seg_6204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1037" id="Seg_6205" n="sc" s="T1034">
               <ts e="T1037" id="Seg_6207" n="HIAT:u" s="T1034">
                  <ts e="T1035" id="Seg_6209" n="HIAT:w" s="T1034">Dĭgəttə</ts>
                  <nts id="Seg_6210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_6212" n="HIAT:w" s="T1035">nagurgöʔ</ts>
                  <nts id="Seg_6213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_6215" n="HIAT:w" s="T1036">kambibaʔ</ts>
                  <nts id="Seg_6216" n="HIAT:ip">.</nts>
                  <nts id="Seg_6217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1041" id="Seg_6218" n="sc" s="T1038">
               <ts e="T1041" id="Seg_6220" n="HIAT:u" s="T1038">
                  <ts e="T1039" id="Seg_6222" n="HIAT:w" s="T1038">Ну</ts>
                  <nts id="Seg_6223" n="HIAT:ip">,</nts>
                  <nts id="Seg_6224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_6226" n="HIAT:w" s="T1039">всё</ts>
                  <nts id="Seg_6227" n="HIAT:ip">.</nts>
                  <nts id="Seg_6228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1064" id="Seg_6229" n="sc" s="T1061">
               <ts e="T1064" id="Seg_6231" n="HIAT:u" s="T1061">
                  <ts e="T1064" id="Seg_6233" n="HIAT:w" s="T1061">Сказала</ts>
                  <nts id="Seg_6234" n="HIAT:ip">!</nts>
                  <nts id="Seg_6235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1084" id="Seg_6236" n="sc" s="T1073">
               <ts e="T1080" id="Seg_6238" n="HIAT:u" s="T1073">
                  <ts e="T1074" id="Seg_6240" n="HIAT:w" s="T1073">Dĭgəttə</ts>
                  <nts id="Seg_6241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_6243" n="HIAT:w" s="T1074">dĭʔnə</ts>
                  <nts id="Seg_6244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_6246" n="HIAT:w" s="T1075">măn</ts>
                  <nts id="Seg_6247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_6249" n="HIAT:w" s="T1076">mămbial</ts>
                  <nts id="Seg_6250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6251" n="HIAT:ip">(</nts>
                  <ts e="T1078" id="Seg_6253" n="HIAT:w" s="T1077">măn=</ts>
                  <nts id="Seg_6254" n="HIAT:ip">)</nts>
                  <nts id="Seg_6255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6256" n="HIAT:ip">"</nts>
                  <ts e="T1079" id="Seg_6258" n="HIAT:w" s="T1078">Giber</ts>
                  <nts id="Seg_6259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_6261" n="HIAT:w" s="T1079">kandəgal</ts>
                  <nts id="Seg_6262" n="HIAT:ip">?</nts>
                  <nts id="Seg_6263" n="HIAT:ip">"</nts>
                  <nts id="Seg_6264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1084" id="Seg_6266" n="HIAT:u" s="T1080">
                  <ts e="T1081" id="Seg_6268" n="HIAT:w" s="T1080">Măn</ts>
                  <nts id="Seg_6269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_6271" n="HIAT:w" s="T1081">măndəm:</ts>
                  <nts id="Seg_6272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6273" n="HIAT:ip">"</nts>
                  <ts e="T1083" id="Seg_6275" n="HIAT:w" s="T1082">Kudajdə</ts>
                  <nts id="Seg_6276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_6278" n="HIAT:w" s="T1083">üzəsʼtə</ts>
                  <nts id="Seg_6279" n="HIAT:ip">"</nts>
                  <nts id="Seg_6280" n="HIAT:ip">.</nts>
                  <nts id="Seg_6281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1089" id="Seg_6282" n="sc" s="T1085">
               <ts e="T1089" id="Seg_6284" n="HIAT:u" s="T1085">
                  <nts id="Seg_6285" n="HIAT:ip">"</nts>
                  <ts e="T1086" id="Seg_6287" n="HIAT:w" s="T1085">Nu</ts>
                  <nts id="Seg_6288" n="HIAT:ip">,</nts>
                  <nts id="Seg_6289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6290" n="HIAT:ip">(</nts>
                  <ts e="T1087" id="Seg_6292" n="HIAT:w" s="T1086">karəlʼdʼan</ts>
                  <nts id="Seg_6293" n="HIAT:ip">)</nts>
                  <nts id="Seg_6294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_6296" n="HIAT:w" s="T1087">mănzi</ts>
                  <nts id="Seg_6297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_6299" n="HIAT:w" s="T1088">kalal</ts>
                  <nts id="Seg_6300" n="HIAT:ip">"</nts>
                  <nts id="Seg_6301" n="HIAT:ip">.</nts>
                  <nts id="Seg_6302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1093" id="Seg_6303" n="sc" s="T1090">
               <ts e="T1093" id="Seg_6305" n="HIAT:u" s="T1090">
                  <ts e="T1091" id="Seg_6307" n="HIAT:w" s="T1090">Dăre</ts>
                  <nts id="Seg_6308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_6310" n="HIAT:w" s="T1091">nörbəbi</ts>
                  <nts id="Seg_6311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_6313" n="HIAT:w" s="T1092">măna</ts>
                  <nts id="Seg_6314" n="HIAT:ip">.</nts>
                  <nts id="Seg_6315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1100" id="Seg_6316" n="sc" s="T1094">
               <ts e="T1100" id="Seg_6318" n="HIAT:u" s="T1094">
                  <ts e="T1095" id="Seg_6320" n="HIAT:w" s="T1094">А</ts>
                  <nts id="Seg_6321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_6323" n="HIAT:w" s="T1095">так</ts>
                  <nts id="Seg_6324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_6326" n="HIAT:w" s="T1096">я</ts>
                  <nts id="Seg_6327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_6329" n="HIAT:w" s="T1097">тебе</ts>
                  <nts id="Seg_6330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_6332" n="HIAT:w" s="T1098">рассказала</ts>
                  <nts id="Seg_6333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_6335" n="HIAT:w" s="T1099">всё</ts>
                  <nts id="Seg_6336" n="HIAT:ip">.</nts>
                  <nts id="Seg_6337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1138" id="Seg_6338" n="sc" s="T1133">
               <ts e="T1138" id="Seg_6340" n="HIAT:u" s="T1133">
                  <ts e="T1136" id="Seg_6342" n="HIAT:w" s="T1133">Говорила</ts>
                  <nts id="Seg_6343" n="HIAT:ip">,</nts>
                  <nts id="Seg_6344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_6346" n="HIAT:w" s="T1136">говорила</ts>
                  <nts id="Seg_6347" n="HIAT:ip">.</nts>
                  <nts id="Seg_6348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1141" id="Seg_6349" n="sc" s="T1139">
               <ts e="T1141" id="Seg_6351" n="HIAT:u" s="T1139">
                  <ts e="T1140" id="Seg_6353" n="HIAT:w" s="T1139">Šide</ts>
                  <nts id="Seg_6354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_6356" n="HIAT:w" s="T1140">kö</ts>
                  <nts id="Seg_6357" n="HIAT:ip">.</nts>
                  <nts id="Seg_6358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1148" id="Seg_6359" n="sc" s="T1142">
               <ts e="T1148" id="Seg_6361" n="HIAT:u" s="T1142">
                  <ts e="T1143" id="Seg_6363" n="HIAT:w" s="T1142">Два</ts>
                  <nts id="Seg_6364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_6366" n="HIAT:w" s="T1143">года</ts>
                  <nts id="Seg_6367" n="HIAT:ip">,</nts>
                  <nts id="Seg_6368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_6370" n="HIAT:w" s="T1144">šide</ts>
                  <nts id="Seg_6371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_6373" n="HIAT:w" s="T1146">kö</ts>
                  <nts id="Seg_6374" n="HIAT:ip">.</nts>
                  <nts id="Seg_6375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1181" id="Seg_6376" n="sc" s="T1177">
               <ts e="T1181" id="Seg_6378" n="HIAT:u" s="T1177">
                  <ts e="T1178" id="Seg_6380" n="HIAT:w" s="T1177">Miʔ</ts>
                  <nts id="Seg_6381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1179" id="Seg_6383" n="HIAT:w" s="T1178">brigadir</ts>
                  <nts id="Seg_6384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_6386" n="HIAT:w" s="T1179">ibi</ts>
                  <nts id="Seg_6387" n="HIAT:ip">,</nts>
                  <nts id="Seg_6388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_6390" n="HIAT:w" s="T1180">nʼemec</ts>
                  <nts id="Seg_6391" n="HIAT:ip">.</nts>
                  <nts id="Seg_6392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1188" id="Seg_6393" n="sc" s="T1182">
               <ts e="T1188" id="Seg_6395" n="HIAT:u" s="T1182">
                  <ts e="T1183" id="Seg_6397" n="HIAT:w" s="T1182">Dĭ</ts>
                  <nts id="Seg_6398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_6400" n="HIAT:w" s="T1183">ugandə</ts>
                  <nts id="Seg_6401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6402" n="HIAT:ip">(</nts>
                  <ts e="T1185" id="Seg_6404" n="HIAT:w" s="T1184">il-</ts>
                  <nts id="Seg_6405" n="HIAT:ip">)</nts>
                  <nts id="Seg_6406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_6408" n="HIAT:w" s="T1185">ildə</ts>
                  <nts id="Seg_6409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1187" id="Seg_6411" n="HIAT:w" s="T1186">ineʔi</ts>
                  <nts id="Seg_6412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_6414" n="HIAT:w" s="T1187">mĭbi</ts>
                  <nts id="Seg_6415" n="HIAT:ip">.</nts>
                  <nts id="Seg_6416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1193" id="Seg_6417" n="sc" s="T1189">
               <ts e="T1193" id="Seg_6419" n="HIAT:u" s="T1189">
                  <ts e="T1190" id="Seg_6421" n="HIAT:w" s="T1189">Tüj</ts>
                  <nts id="Seg_6422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_6424" n="HIAT:w" s="T1190">dĭ</ts>
                  <nts id="Seg_6425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1739" id="Seg_6427" n="HIAT:w" s="T1191">kalla</ts>
                  <nts id="Seg_6428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_6430" n="HIAT:w" s="T1739">dʼürbi</ts>
                  <nts id="Seg_6431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_6433" n="HIAT:w" s="T1192">Vălgăgrattə</ts>
                  <nts id="Seg_6434" n="HIAT:ip">.</nts>
                  <nts id="Seg_6435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1206" id="Seg_6436" n="sc" s="T1194">
               <ts e="T1206" id="Seg_6438" n="HIAT:u" s="T1194">
                  <ts e="T1195" id="Seg_6440" n="HIAT:w" s="T1194">A</ts>
                  <nts id="Seg_6441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1196" id="Seg_6443" n="HIAT:w" s="T1195">kamen</ts>
                  <nts id="Seg_6444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1197" id="Seg_6446" n="HIAT:w" s="T1196">šobi</ts>
                  <nts id="Seg_6447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_6449" n="HIAT:w" s="T1197">dĭ</ts>
                  <nts id="Seg_6450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_6452" n="HIAT:w" s="T1198">baška</ts>
                  <nts id="Seg_6453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_6455" n="HIAT:w" s="T1199">predsʼedatʼelʼ</ts>
                  <nts id="Seg_6456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_6458" n="HIAT:w" s="T1200">miʔnʼibeʔ</ts>
                  <nts id="Seg_6459" n="HIAT:ip">,</nts>
                  <nts id="Seg_6460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_6462" n="HIAT:w" s="T1201">dĭ</ts>
                  <nts id="Seg_6463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_6465" n="HIAT:w" s="T1202">ildə</ts>
                  <nts id="Seg_6466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_6468" n="HIAT:w" s="T1203">ine</ts>
                  <nts id="Seg_6469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6470" n="HIAT:ip">(</nts>
                  <ts e="T1205" id="Seg_6472" n="HIAT:w" s="T1204">mĭbiʔi=</ts>
                  <nts id="Seg_6473" n="HIAT:ip">)</nts>
                  <nts id="Seg_6474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1206" id="Seg_6476" n="HIAT:w" s="T1205">mĭbi</ts>
                  <nts id="Seg_6477" n="HIAT:ip">.</nts>
                  <nts id="Seg_6478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1214" id="Seg_6479" n="sc" s="T1207">
               <ts e="T1214" id="Seg_6481" n="HIAT:u" s="T1207">
                  <nts id="Seg_6482" n="HIAT:ip">(</nts>
                  <ts e="T1208" id="Seg_6484" n="HIAT:w" s="T1207">Šindimdə</ts>
                  <nts id="Seg_6485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1209" id="Seg_6487" n="HIAT:w" s="T1208">ej=</ts>
                  <nts id="Seg_6488" n="HIAT:ip">)</nts>
                  <nts id="Seg_6489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6490" n="HIAT:ip">(</nts>
                  <ts e="T1210" id="Seg_6492" n="HIAT:w" s="T1209">Keʔbdejleʔ</ts>
                  <nts id="Seg_6493" n="HIAT:ip">)</nts>
                  <nts id="Seg_6494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1211" id="Seg_6496" n="HIAT:w" s="T1210">kambiʔi</ts>
                  <nts id="Seg_6497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1212" id="Seg_6499" n="HIAT:w" s="T1211">inezi</ts>
                  <nts id="Seg_6500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_6502" n="HIAT:w" s="T1212">mĭbi</ts>
                  <nts id="Seg_6503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_6505" n="HIAT:w" s="T1213">tože</ts>
                  <nts id="Seg_6506" n="HIAT:ip">.</nts>
                  <nts id="Seg_6507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1223" id="Seg_6508" n="sc" s="T1215">
               <ts e="T1223" id="Seg_6510" n="HIAT:u" s="T1215">
                  <ts e="T1216" id="Seg_6512" n="HIAT:w" s="T1215">Dĭ</ts>
                  <nts id="Seg_6513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1217" id="Seg_6515" n="HIAT:w" s="T1216">predsʼedatʼelʼ</ts>
                  <nts id="Seg_6516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_6518" n="HIAT:w" s="T1217">tüj</ts>
                  <nts id="Seg_6519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1219" id="Seg_6521" n="HIAT:w" s="T1218">miʔnʼibeʔ</ts>
                  <nts id="Seg_6522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1220" id="Seg_6524" n="HIAT:w" s="T1219">Văznʼesenkanə</ts>
                  <nts id="Seg_6525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_6527" n="HIAT:w" s="T1220">kambi</ts>
                  <nts id="Seg_6528" n="HIAT:ip">,</nts>
                  <nts id="Seg_6529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_6531" n="HIAT:w" s="T1221">dĭn</ts>
                  <nts id="Seg_6532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_6534" n="HIAT:w" s="T1222">amnolaʔbə</ts>
                  <nts id="Seg_6535" n="HIAT:ip">.</nts>
                  <nts id="Seg_6536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1235" id="Seg_6537" n="sc" s="T1224">
               <ts e="T1227" id="Seg_6539" n="HIAT:u" s="T1224">
                  <ts e="T1225" id="Seg_6541" n="HIAT:w" s="T1224">Dĭʔnə</ts>
                  <nts id="Seg_6542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_6544" n="HIAT:w" s="T1225">tura</ts>
                  <nts id="Seg_6545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1227" id="Seg_6547" n="HIAT:w" s="T1226">nuldəbiʔi</ts>
                  <nts id="Seg_6548" n="HIAT:ip">.</nts>
                  <nts id="Seg_6549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1235" id="Seg_6551" n="HIAT:u" s="T1227">
                  <ts e="T1228" id="Seg_6553" n="HIAT:w" s="T1227">Turat</ts>
                  <nts id="Seg_6554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_6556" n="HIAT:w" s="T1228">tüj</ts>
                  <nts id="Seg_6557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1230" id="Seg_6559" n="HIAT:w" s="T1229">nulaʔbə</ts>
                  <nts id="Seg_6560" n="HIAT:ip">,</nts>
                  <nts id="Seg_6561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1231" id="Seg_6563" n="HIAT:w" s="T1230">šindidə</ts>
                  <nts id="Seg_6564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1232" id="Seg_6566" n="HIAT:w" s="T1231">ej</ts>
                  <nts id="Seg_6567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1233" id="Seg_6569" n="HIAT:w" s="T1232">amnolia</ts>
                  <nts id="Seg_6570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1234" id="Seg_6572" n="HIAT:w" s="T1233">dĭ</ts>
                  <nts id="Seg_6573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1235" id="Seg_6575" n="HIAT:w" s="T1234">turagən</ts>
                  <nts id="Seg_6576" n="HIAT:ip">.</nts>
                  <nts id="Seg_6577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1237" id="Seg_6578" n="sc" s="T1236">
               <ts e="T1237" id="Seg_6580" n="HIAT:u" s="T1236">
                  <ts e="T1237" id="Seg_6582" n="HIAT:w" s="T1236">Kabarləj</ts>
                  <nts id="Seg_6583" n="HIAT:ip">.</nts>
                  <nts id="Seg_6584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1264" id="Seg_6585" n="sc" s="T1248">
               <ts e="T1257" id="Seg_6587" n="HIAT:u" s="T1248">
                  <ts e="T1249" id="Seg_6589" n="HIAT:w" s="T1248">Председатель</ts>
                  <nts id="Seg_6590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1250" id="Seg_6592" n="HIAT:w" s="T1249">у</ts>
                  <nts id="Seg_6593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1251" id="Seg_6595" n="HIAT:w" s="T1250">нас</ts>
                  <nts id="Seg_6596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1252" id="Seg_6598" n="HIAT:w" s="T1251">хороший</ts>
                  <nts id="Seg_6599" n="HIAT:ip">,</nts>
                  <nts id="Seg_6600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6601" n="HIAT:ip">(</nts>
                  <ts e="T1253" id="Seg_6603" n="HIAT:w" s="T1252">л-</ts>
                  <nts id="Seg_6604" n="HIAT:ip">)</nts>
                  <nts id="Seg_6605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1254" id="Seg_6607" n="HIAT:w" s="T1253">с</ts>
                  <nts id="Seg_6608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1255" id="Seg_6610" n="HIAT:w" s="T1254">людям</ts>
                  <nts id="Seg_6611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1256" id="Seg_6613" n="HIAT:w" s="T1255">хорошо</ts>
                  <nts id="Seg_6614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1257" id="Seg_6616" n="HIAT:w" s="T1256">обходился</ts>
                  <nts id="Seg_6617" n="HIAT:ip">.</nts>
                  <nts id="Seg_6618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1264" id="Seg_6620" n="HIAT:u" s="T1257">
                  <ts e="T1258" id="Seg_6622" n="HIAT:w" s="T1257">Коней</ts>
                  <nts id="Seg_6623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_6625" n="HIAT:w" s="T1258">давал</ts>
                  <nts id="Seg_6626" n="HIAT:ip">,</nts>
                  <nts id="Seg_6627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_6629" n="HIAT:w" s="T1259">даже</ts>
                  <nts id="Seg_6630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_6632" n="HIAT:w" s="T1260">по</ts>
                  <nts id="Seg_6633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1262" id="Seg_6635" n="HIAT:w" s="T1261">ягоды</ts>
                  <nts id="Seg_6636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1263" id="Seg_6638" n="HIAT:w" s="T1262">ездили</ts>
                  <nts id="Seg_6639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_6641" n="HIAT:w" s="T1263">люди</ts>
                  <nts id="Seg_6642" n="HIAT:ip">.</nts>
                  <nts id="Seg_6643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1271" id="Seg_6644" n="sc" s="T1265">
               <ts e="T1271" id="Seg_6646" n="HIAT:u" s="T1265">
                  <ts e="T1266" id="Seg_6648" n="HIAT:w" s="T1265">А</ts>
                  <nts id="Seg_6649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1267" id="Seg_6651" n="HIAT:w" s="T1266">теперь</ts>
                  <nts id="Seg_6652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1268" id="Seg_6654" n="HIAT:w" s="T1267">нас</ts>
                  <nts id="Seg_6655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_6657" n="HIAT:w" s="T1268">соединили</ts>
                  <nts id="Seg_6658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1270" id="Seg_6660" n="HIAT:w" s="T1269">с</ts>
                  <nts id="Seg_6661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1271" id="Seg_6663" n="HIAT:w" s="T1270">Вознесенки</ts>
                  <nts id="Seg_6664" n="HIAT:ip">.</nts>
                  <nts id="Seg_6665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1278" id="Seg_6666" n="sc" s="T1272">
               <ts e="T1278" id="Seg_6668" n="HIAT:u" s="T1272">
                  <ts e="T1273" id="Seg_6670" n="HIAT:w" s="T1272">Ему</ts>
                  <nts id="Seg_6671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_6673" n="HIAT:w" s="T1273">выстроили</ts>
                  <nts id="Seg_6674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1275" id="Seg_6676" n="HIAT:w" s="T1274">тут</ts>
                  <nts id="Seg_6677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1276" id="Seg_6679" n="HIAT:w" s="T1275">дом</ts>
                  <nts id="Seg_6680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1277" id="Seg_6682" n="HIAT:w" s="T1276">в</ts>
                  <nts id="Seg_6683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1278" id="Seg_6685" n="HIAT:w" s="T1277">Абалаковой</ts>
                  <nts id="Seg_6686" n="HIAT:ip">.</nts>
                  <nts id="Seg_6687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1287" id="Seg_6688" n="sc" s="T1279">
               <ts e="T1287" id="Seg_6690" n="HIAT:u" s="T1279">
                  <nts id="Seg_6691" n="HIAT:ip">(</nts>
                  <ts e="T1280" id="Seg_6693" n="HIAT:w" s="T1279">И=</ts>
                  <nts id="Seg_6694" n="HIAT:ip">)</nts>
                  <nts id="Seg_6695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_6697" n="HIAT:w" s="T1280">А</ts>
                  <nts id="Seg_6698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1282" id="Seg_6700" n="HIAT:w" s="T1281">теперь</ts>
                  <nts id="Seg_6701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_6703" n="HIAT:w" s="T1282">в</ts>
                  <nts id="Seg_6704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_6706" n="HIAT:w" s="T1283">Вознесенку</ts>
                  <nts id="Seg_6707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1285" id="Seg_6709" n="HIAT:w" s="T1284">уехал</ts>
                  <nts id="Seg_6710" n="HIAT:ip">,</nts>
                  <nts id="Seg_6711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1286" id="Seg_6713" n="HIAT:w" s="T1285">там</ts>
                  <nts id="Seg_6714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1287" id="Seg_6716" n="HIAT:w" s="T1286">живет</ts>
                  <nts id="Seg_6717" n="HIAT:ip">.</nts>
                  <nts id="Seg_6718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1344" id="Seg_6719" n="sc" s="T1341">
               <ts e="T1344" id="Seg_6721" n="HIAT:u" s="T1341">
                  <ts e="T1341.tx-PKZ.1" id="Seg_6723" n="HIAT:w" s="T1341">И</ts>
                  <nts id="Seg_6724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1341.tx-PKZ.2" id="Seg_6726" n="HIAT:w" s="T1341.tx-PKZ.1">сейчас</ts>
                  <nts id="Seg_6727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1341.tx-PKZ.3" id="Seg_6729" n="HIAT:w" s="T1341.tx-PKZ.2">есть</ts>
                  <nts id="Seg_6730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_6732" n="HIAT:w" s="T1341.tx-PKZ.3">такое</ts>
                  <nts id="Seg_6733" n="HIAT:ip">.</nts>
                  <nts id="Seg_6734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1353" id="Seg_6735" n="sc" s="T1347">
               <ts e="T1353" id="Seg_6737" n="HIAT:u" s="T1347">
                  <ts e="T1348" id="Seg_6739" n="HIAT:w" s="T1347">Продают</ts>
                  <nts id="Seg_6740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_6742" n="HIAT:w" s="T1348">прямо</ts>
                  <nts id="Seg_6743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1350" id="Seg_6745" n="HIAT:w" s="T1349">поллитром</ts>
                  <nts id="Seg_6746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_6748" n="HIAT:w" s="T1350">такое</ts>
                  <nts id="Seg_6749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6750" n="HIAT:ip">(</nts>
                  <ts e="T1352" id="Seg_6752" n="HIAT:w" s="T1351">крепкое</ts>
                  <nts id="Seg_6753" n="HIAT:ip">)</nts>
                  <nts id="Seg_6754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1353" id="Seg_6756" n="HIAT:w" s="T1352">вино</ts>
                  <nts id="Seg_6757" n="HIAT:ip">.</nts>
                  <nts id="Seg_6758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1355" id="Seg_6759" n="sc" s="T1354">
               <ts e="T1355" id="Seg_6761" n="HIAT:u" s="T1354">
                  <ts e="T1355" id="Seg_6763" n="HIAT:w" s="T1354">Спирт</ts>
                  <nts id="Seg_6764" n="HIAT:ip">!</nts>
                  <nts id="Seg_6765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1375" id="Seg_6766" n="sc" s="T1356">
               <ts e="T1372" id="Seg_6768" n="HIAT:u" s="T1356">
                  <ts e="T1357" id="Seg_6770" n="HIAT:w" s="T1356">Ну</ts>
                  <nts id="Seg_6771" n="HIAT:ip">,</nts>
                  <nts id="Seg_6772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358" id="Seg_6774" n="HIAT:w" s="T1357">он</ts>
                  <nts id="Seg_6775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358.tx-PKZ.1" id="Seg_6777" n="HIAT:w" s="T1358">так</ts>
                  <nts id="Seg_6778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6779" n="HIAT:ip">(</nts>
                  <nts id="Seg_6780" n="HIAT:ip">(</nts>
                  <ats e="T1359" id="Seg_6781" n="HIAT:non-pho" s="T1358.tx-PKZ.1">COUGH</ats>
                  <nts id="Seg_6782" n="HIAT:ip">)</nts>
                  <nts id="Seg_6783" n="HIAT:ip">)</nts>
                  <nts id="Seg_6784" n="HIAT:ip">,</nts>
                  <nts id="Seg_6785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_6787" n="HIAT:w" s="T1359">для</ts>
                  <nts id="Seg_6788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_6790" n="HIAT:w" s="T1360">здоровья</ts>
                  <nts id="Seg_6791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1362" id="Seg_6793" n="HIAT:w" s="T1361">вот</ts>
                  <nts id="Seg_6794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1363" id="Seg_6796" n="HIAT:w" s="T1362">столь</ts>
                  <nts id="Seg_6797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_6799" n="HIAT:w" s="T1363">выпьем</ts>
                  <nts id="Seg_6800" n="HIAT:ip">,</nts>
                  <nts id="Seg_6801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_6803" n="HIAT:w" s="T1364">лучше</ts>
                  <nts id="Seg_6804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1366" id="Seg_6806" n="HIAT:w" s="T1365">как-то</ts>
                  <nts id="Seg_6807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6808" n="HIAT:ip">(</nts>
                  <ts e="T1367" id="Seg_6810" n="HIAT:w" s="T1366">как=</ts>
                  <nts id="Seg_6811" n="HIAT:ip">)</nts>
                  <nts id="Seg_6812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_6814" n="HIAT:w" s="T1367">как</ts>
                  <nts id="Seg_6815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1369" id="Seg_6817" n="HIAT:w" s="T1368">это</ts>
                  <nts id="Seg_6818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1370" id="Seg_6820" n="HIAT:w" s="T1369">вот</ts>
                  <nts id="Seg_6821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6822" n="HIAT:ip">(</nts>
                  <ts e="T1371" id="Seg_6824" n="HIAT:w" s="T1370">са-</ts>
                  <nts id="Seg_6825" n="HIAT:ip">)</nts>
                  <nts id="Seg_6826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_6828" n="HIAT:w" s="T1371">самогонка</ts>
                  <nts id="Seg_6829" n="HIAT:ip">.</nts>
                  <nts id="Seg_6830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1375" id="Seg_6832" n="HIAT:u" s="T1372">
                  <ts e="T1373" id="Seg_6834" n="HIAT:w" s="T1372">Он</ts>
                  <nts id="Seg_6835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1374" id="Seg_6837" n="HIAT:w" s="T1373">чистый</ts>
                  <nts id="Seg_6838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1375" id="Seg_6840" n="HIAT:w" s="T1374">ведь</ts>
                  <nts id="Seg_6841" n="HIAT:ip">.</nts>
                  <nts id="Seg_6842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1377" id="Seg_6843" n="sc" s="T1376">
               <ts e="T1377" id="Seg_6845" n="HIAT:u" s="T1376">
                  <ts e="T1377" id="Seg_6847" n="HIAT:w" s="T1376">Вот</ts>
                  <nts id="Seg_6848" n="HIAT:ip">.</nts>
                  <nts id="Seg_6849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1388" id="Seg_6850" n="sc" s="T1380">
               <ts e="T1388" id="Seg_6852" n="HIAT:u" s="T1380">
                  <ts e="T1382" id="Seg_6854" n="HIAT:w" s="T1380">А</ts>
                  <nts id="Seg_6855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_6857" n="HIAT:w" s="T1382">ты</ts>
                  <nts id="Seg_6858" n="HIAT:ip">,</nts>
                  <nts id="Seg_6859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1384" id="Seg_6861" n="HIAT:w" s="T1383">тот</ts>
                  <nts id="Seg_6862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_6864" n="HIAT:w" s="T1384">магазин</ts>
                  <nts id="Seg_6865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_6867" n="HIAT:w" s="T1385">у</ts>
                  <nts id="Seg_6868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1387" id="Seg_6870" n="HIAT:w" s="T1386">нас</ts>
                  <nts id="Seg_6871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_6873" n="HIAT:w" s="T1387">теперь</ts>
                  <nts id="Seg_6874" n="HIAT:ip">…</nts>
                  <nts id="Seg_6875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1404" id="Seg_6876" n="sc" s="T1389">
               <ts e="T1394" id="Seg_6878" n="HIAT:u" s="T1389">
                  <ts e="T1390" id="Seg_6880" n="HIAT:w" s="T1389">Ты</ts>
                  <nts id="Seg_6881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1391" id="Seg_6883" n="HIAT:w" s="T1390">в</ts>
                  <nts id="Seg_6884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_6886" n="HIAT:w" s="T1391">этим</ts>
                  <nts id="Seg_6887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1393" id="Seg_6889" n="HIAT:w" s="T1392">был</ts>
                  <nts id="Seg_6890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_6892" n="HIAT:w" s="T1393">магазине</ts>
                  <nts id="Seg_6893" n="HIAT:ip">?</nts>
                  <nts id="Seg_6894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1404" id="Seg_6896" n="HIAT:u" s="T1394">
                  <ts e="T1395" id="Seg_6898" n="HIAT:w" s="T1394">У</ts>
                  <nts id="Seg_6899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1396" id="Seg_6901" n="HIAT:w" s="T1395">нас</ts>
                  <nts id="Seg_6902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1397" id="Seg_6904" n="HIAT:w" s="T1396">теперь</ts>
                  <nts id="Seg_6905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1398" id="Seg_6907" n="HIAT:w" s="T1397">другой</ts>
                  <nts id="Seg_6908" n="HIAT:ip">,</nts>
                  <nts id="Seg_6909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_6911" n="HIAT:w" s="T1398">на</ts>
                  <nts id="Seg_6912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_6914" n="HIAT:w" s="T1399">том</ts>
                  <nts id="Seg_6915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_6917" n="HIAT:w" s="T1400">бугре</ts>
                  <nts id="Seg_6918" n="HIAT:ip">,</nts>
                  <nts id="Seg_6919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_6921" n="HIAT:w" s="T1401">ты</ts>
                  <nts id="Seg_6922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1403" id="Seg_6924" n="HIAT:w" s="T1402">его</ts>
                  <nts id="Seg_6925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1404" id="Seg_6927" n="HIAT:w" s="T1403">знаешь</ts>
                  <nts id="Seg_6928" n="HIAT:ip">?</nts>
                  <nts id="Seg_6929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1434" id="Seg_6930" n="sc" s="T1433">
               <ts e="T1434" id="Seg_6932" n="HIAT:u" s="T1433">
                  <ts e="T1434" id="Seg_6934" n="HIAT:w" s="T1433">Вот</ts>
                  <nts id="Seg_6935" n="HIAT:ip">…</nts>
                  <nts id="Seg_6936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1466" id="Seg_6937" n="sc" s="T1465">
               <ts e="T1466" id="Seg_6939" n="HIAT:u" s="T1465">
                  <ts e="T1466" id="Seg_6941" n="HIAT:w" s="T1465">Погоди</ts>
                  <nts id="Seg_6942" n="HIAT:ip">…</nts>
                  <nts id="Seg_6943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1470" id="Seg_6944" n="sc" s="T1467">
               <ts e="T1470" id="Seg_6946" n="HIAT:u" s="T1467">
                  <ts e="T1468" id="Seg_6948" n="HIAT:w" s="T1467">Ну</ts>
                  <nts id="Seg_6949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1469" id="Seg_6951" n="HIAT:w" s="T1468">тут</ts>
                  <nts id="Seg_6952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1470" id="Seg_6954" n="HIAT:w" s="T1469">это</ts>
                  <nts id="Seg_6955" n="HIAT:ip">…</nts>
                  <nts id="Seg_6956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1487" id="Seg_6957" n="sc" s="T1471">
               <ts e="T1487" id="Seg_6959" n="HIAT:u" s="T1471">
                  <ts e="T1471.tx-PKZ.1" id="Seg_6961" n="HIAT:w" s="T1471">Как</ts>
                  <nts id="Seg_6962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.2" id="Seg_6964" n="HIAT:w" s="T1471.tx-PKZ.1">идешь</ts>
                  <nts id="Seg_6965" n="HIAT:ip">,</nts>
                  <nts id="Seg_6966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.3" id="Seg_6968" n="HIAT:w" s="T1471.tx-PKZ.2">еще</ts>
                  <nts id="Seg_6969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.4" id="Seg_6971" n="HIAT:w" s="T1471.tx-PKZ.3">лог</ts>
                  <nts id="Seg_6972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.5" id="Seg_6974" n="HIAT:w" s="T1471.tx-PKZ.4">не</ts>
                  <nts id="Seg_6975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.6" id="Seg_6977" n="HIAT:w" s="T1471.tx-PKZ.5">переходишь</ts>
                  <nts id="Seg_6978" n="HIAT:ip">,</nts>
                  <nts id="Seg_6979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.7" id="Seg_6981" n="HIAT:w" s="T1471.tx-PKZ.6">вот</ts>
                  <nts id="Seg_6982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.8" id="Seg_6984" n="HIAT:w" s="T1471.tx-PKZ.7">здесь</ts>
                  <nts id="Seg_6985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471.tx-PKZ.9" id="Seg_6987" n="HIAT:w" s="T1471.tx-PKZ.8">магазин</ts>
                  <nts id="Seg_6988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1484" id="Seg_6990" n="HIAT:w" s="T1471.tx-PKZ.9">был</ts>
                  <nts id="Seg_6991" n="HIAT:ip">,</nts>
                  <nts id="Seg_6992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1484.tx-PKZ.1" id="Seg_6994" n="HIAT:w" s="T1484">а</ts>
                  <nts id="Seg_6995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1486" id="Seg_6997" n="HIAT:w" s="T1484.tx-PKZ.1">тут</ts>
                  <nts id="Seg_6998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1487" id="Seg_7000" n="HIAT:w" s="T1486">лог</ts>
                  <nts id="Seg_7001" n="HIAT:ip">.</nts>
                  <nts id="Seg_7002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1499" id="Seg_7003" n="sc" s="T1488">
               <ts e="T1499" id="Seg_7005" n="HIAT:u" s="T1488">
                  <ts e="T1489" id="Seg_7007" n="HIAT:w" s="T1488">А</ts>
                  <nts id="Seg_7008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_7010" n="HIAT:w" s="T1489">теперь</ts>
                  <nts id="Seg_7011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491.tx-PKZ.1" id="Seg_7013" n="HIAT:w" s="T1491">за</ts>
                  <nts id="Seg_7014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1492" id="Seg_7016" n="HIAT:w" s="T1491.tx-PKZ.1">логом</ts>
                  <nts id="Seg_7017" n="HIAT:ip">,</nts>
                  <nts id="Seg_7018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7019" n="HIAT:ip">(</nts>
                  <nts id="Seg_7020" n="HIAT:ip">(</nts>
                  <ats e="T1493" id="Seg_7021" n="HIAT:non-pho" s="T1492">PAUSE</ats>
                  <nts id="Seg_7022" n="HIAT:ip">)</nts>
                  <nts id="Seg_7023" n="HIAT:ip">)</nts>
                  <nts id="Seg_7024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1493.tx-PKZ.1" id="Seg_7026" n="HIAT:w" s="T1493">тут</ts>
                  <nts id="Seg_7027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1493.tx-PKZ.2" id="Seg_7029" n="HIAT:w" s="T1493.tx-PKZ.1">это</ts>
                  <nts id="Seg_7030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495" id="Seg_7032" n="HIAT:w" s="T1493.tx-PKZ.2">где</ts>
                  <nts id="Seg_7033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495.tx-PKZ.1" id="Seg_7035" n="HIAT:w" s="T1495">моя</ts>
                  <nts id="Seg_7036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495.tx-PKZ.2" id="Seg_7038" n="HIAT:w" s="T1495.tx-PKZ.1">родимая</ts>
                  <nts id="Seg_7039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495.tx-PKZ.3" id="Seg_7041" n="HIAT:w" s="T1495.tx-PKZ.2">изба</ts>
                  <nts id="Seg_7042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1499" id="Seg_7044" n="HIAT:w" s="T1495.tx-PKZ.3">была</ts>
                  <nts id="Seg_7045" n="HIAT:ip">.</nts>
                  <nts id="Seg_7046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1518" id="Seg_7047" n="sc" s="T1502">
               <ts e="T1518" id="Seg_7049" n="HIAT:u" s="T1502">
                  <ts e="T1504" id="Seg_7051" n="HIAT:w" s="T1502">Ну</ts>
                  <nts id="Seg_7052" n="HIAT:ip">,</nts>
                  <nts id="Seg_7053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1506" id="Seg_7055" n="HIAT:w" s="T1504">а</ts>
                  <nts id="Seg_7056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1509" id="Seg_7058" n="HIAT:w" s="T1506">как</ts>
                  <nts id="Seg_7059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1511" id="Seg_7061" n="HIAT:w" s="T1509">там</ts>
                  <nts id="Seg_7062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1514" id="Seg_7064" n="HIAT:w" s="T1511">она</ts>
                  <nts id="Seg_7065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1515" id="Seg_7067" n="HIAT:w" s="T1514">сгорела</ts>
                  <nts id="Seg_7068" n="HIAT:ip">,</nts>
                  <nts id="Seg_7069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1516" id="Seg_7071" n="HIAT:w" s="T1515">не</ts>
                  <nts id="Seg_7072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1517" id="Seg_7074" n="HIAT:w" s="T1516">при</ts>
                  <nts id="Seg_7075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1518" id="Seg_7077" n="HIAT:w" s="T1517">тебе</ts>
                  <nts id="Seg_7078" n="HIAT:ip">?</nts>
                  <nts id="Seg_7079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1530" id="Seg_7080" n="sc" s="T1522">
               <ts e="T1530" id="Seg_7082" n="HIAT:u" s="T1522">
                  <ts e="T1523" id="Seg_7084" n="HIAT:w" s="T1522">Сгорела</ts>
                  <nts id="Seg_7085" n="HIAT:ip">,</nts>
                  <nts id="Seg_7086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_7088" n="HIAT:w" s="T1523">а</ts>
                  <nts id="Seg_7089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1525" id="Seg_7091" n="HIAT:w" s="T1524">теперь</ts>
                  <nts id="Seg_7092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1526" id="Seg_7094" n="HIAT:w" s="T1525">на</ts>
                  <nts id="Seg_7095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1527" id="Seg_7097" n="HIAT:w" s="T1526">этим</ts>
                  <nts id="Seg_7098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1528" id="Seg_7100" n="HIAT:w" s="T1527">месте</ts>
                  <nts id="Seg_7101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1529" id="Seg_7103" n="HIAT:w" s="T1528">поставили</ts>
                  <nts id="Seg_7104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1530" id="Seg_7106" n="HIAT:w" s="T1529">магазин</ts>
                  <nts id="Seg_7107" n="HIAT:ip">.</nts>
                  <nts id="Seg_7108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1533" id="Seg_7109" n="sc" s="T1531">
               <ts e="T1533" id="Seg_7111" n="HIAT:u" s="T1531">
                  <ts e="T1532" id="Seg_7113" n="HIAT:w" s="T1531">Большой</ts>
                  <nts id="Seg_7114" n="HIAT:ip">,</nts>
                  <nts id="Seg_7115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1533" id="Seg_7117" n="HIAT:w" s="T1532">обширный</ts>
                  <nts id="Seg_7118" n="HIAT:ip">.</nts>
                  <nts id="Seg_7119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1540" id="Seg_7120" n="sc" s="T1534">
               <ts e="T1540" id="Seg_7122" n="HIAT:u" s="T1534">
                  <ts e="T1535" id="Seg_7124" n="HIAT:w" s="T1534">Вот</ts>
                  <nts id="Seg_7125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1536" id="Seg_7127" n="HIAT:w" s="T1535">больше</ts>
                  <nts id="Seg_7128" n="HIAT:ip">,</nts>
                  <nts id="Seg_7129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1537" id="Seg_7131" n="HIAT:w" s="T1536">пожалуй</ts>
                  <nts id="Seg_7132" n="HIAT:ip">,</nts>
                  <nts id="Seg_7133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1538" id="Seg_7135" n="HIAT:w" s="T1537">этого</ts>
                  <nts id="Seg_7136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1539" id="Seg_7138" n="HIAT:w" s="T1538">дома</ts>
                  <nts id="Seg_7139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1540" id="Seg_7141" n="HIAT:w" s="T1539">будет</ts>
                  <nts id="Seg_7142" n="HIAT:ip">.</nts>
                  <nts id="Seg_7143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1544" id="Seg_7144" n="sc" s="T1542">
               <ts e="T1544" id="Seg_7146" n="HIAT:u" s="T1542">
                  <ts e="T1542.tx-PKZ.1" id="Seg_7148" n="HIAT:w" s="T1542">Там</ts>
                  <nts id="Seg_7149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7150" n="HIAT:ip">(</nts>
                  <ts e="T1544" id="Seg_7152" n="HIAT:w" s="T1542.tx-PKZ.1">кирпи-</ts>
                  <nts id="Seg_7153" n="HIAT:ip">)</nts>
                  <nts id="Seg_7154" n="HIAT:ip">…</nts>
                  <nts id="Seg_7155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1555" id="Seg_7156" n="sc" s="T1545">
               <ts e="T1555" id="Seg_7158" n="HIAT:u" s="T1545">
                  <ts e="T1547" id="Seg_7160" n="HIAT:w" s="T1545">Нет</ts>
                  <nts id="Seg_7161" n="HIAT:ip">,</nts>
                  <nts id="Seg_7162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1549" id="Seg_7164" n="HIAT:w" s="T1547">это</ts>
                  <nts id="Seg_7165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7166" n="HIAT:ip">(</nts>
                  <ts e="T1551" id="Seg_7168" n="HIAT:w" s="T1549">деревя-</ts>
                  <nts id="Seg_7169" n="HIAT:ip">)</nts>
                  <nts id="Seg_7170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1552" id="Seg_7172" n="HIAT:w" s="T1551">деревянное</ts>
                  <nts id="Seg_7173" n="HIAT:ip">,</nts>
                  <nts id="Seg_7174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1553" id="Seg_7176" n="HIAT:w" s="T1552">но</ts>
                  <nts id="Seg_7177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1554" id="Seg_7179" n="HIAT:w" s="T1553">хорошее</ts>
                  <nts id="Seg_7180" n="HIAT:ip">,</nts>
                  <nts id="Seg_7181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1555" id="Seg_7183" n="HIAT:w" s="T1554">большое</ts>
                  <nts id="Seg_7184" n="HIAT:ip">.</nts>
                  <nts id="Seg_7185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1565" id="Seg_7186" n="sc" s="T1556">
               <ts e="T1565" id="Seg_7188" n="HIAT:u" s="T1556">
                  <ts e="T1557" id="Seg_7190" n="HIAT:w" s="T1556">Так</ts>
                  <nts id="Seg_7191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1558" id="Seg_7193" n="HIAT:w" s="T1557">тут</ts>
                  <nts id="Seg_7194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1559" id="Seg_7196" n="HIAT:w" s="T1558">зайдешь</ts>
                  <nts id="Seg_7197" n="HIAT:ip">,</nts>
                  <nts id="Seg_7198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1560" id="Seg_7200" n="HIAT:w" s="T1559">всё</ts>
                  <nts id="Seg_7201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1561" id="Seg_7203" n="HIAT:w" s="T1560">тут</ts>
                  <nts id="Seg_7204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1562" id="Seg_7206" n="HIAT:w" s="T1561">полки</ts>
                  <nts id="Seg_7207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1563" id="Seg_7209" n="HIAT:w" s="T1562">закладено</ts>
                  <nts id="Seg_7210" n="HIAT:ip">,</nts>
                  <nts id="Seg_7211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1564" id="Seg_7213" n="HIAT:w" s="T1563">тут</ts>
                  <nts id="Seg_7214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1565" id="Seg_7216" n="HIAT:w" s="T1564">и</ts>
                  <nts id="Seg_7217" n="HIAT:ip">…</nts>
                  <nts id="Seg_7218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1577" id="Seg_7219" n="sc" s="T1566">
               <ts e="T1577" id="Seg_7221" n="HIAT:u" s="T1566">
                  <ts e="T1567" id="Seg_7223" n="HIAT:w" s="T1566">И</ts>
                  <nts id="Seg_7224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1568" id="Seg_7226" n="HIAT:w" s="T1567">потом</ts>
                  <nts id="Seg_7227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1569" id="Seg_7229" n="HIAT:w" s="T1568">тут</ts>
                  <nts id="Seg_7230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1570" id="Seg_7232" n="HIAT:w" s="T1569">так</ts>
                  <nts id="Seg_7233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1571" id="Seg_7235" n="HIAT:w" s="T1570">выкладена</ts>
                  <nts id="Seg_7236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1572" id="Seg_7238" n="HIAT:w" s="T1571">така</ts>
                  <nts id="Seg_7239" n="HIAT:ip">,</nts>
                  <nts id="Seg_7240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1573" id="Seg_7242" n="HIAT:w" s="T1572">как</ts>
                  <nts id="Seg_7243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1574" id="Seg_7245" n="HIAT:w" s="T1573">вроде</ts>
                  <nts id="Seg_7246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1575" id="Seg_7248" n="HIAT:w" s="T1574">вот</ts>
                  <nts id="Seg_7249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1576" id="Seg_7251" n="HIAT:w" s="T1575">печи</ts>
                  <nts id="Seg_7252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1577" id="Seg_7254" n="HIAT:w" s="T1576">такой</ts>
                  <nts id="Seg_7255" n="HIAT:ip">.</nts>
                  <nts id="Seg_7256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1584" id="Seg_7257" n="sc" s="T1578">
               <ts e="T1584" id="Seg_7259" n="HIAT:u" s="T1578">
                  <ts e="T1579" id="Seg_7261" n="HIAT:w" s="T1578">А</ts>
                  <nts id="Seg_7262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1580" id="Seg_7264" n="HIAT:w" s="T1579">потом</ts>
                  <nts id="Seg_7265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1581" id="Seg_7267" n="HIAT:w" s="T1580">чего-то</ts>
                  <nts id="Seg_7268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1582" id="Seg_7270" n="HIAT:w" s="T1581">там</ts>
                  <nts id="Seg_7271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1583" id="Seg_7273" n="HIAT:w" s="T1582">загорело</ts>
                  <nts id="Seg_7274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1584" id="Seg_7276" n="HIAT:w" s="T1583">опеть</ts>
                  <nts id="Seg_7277" n="HIAT:ip">.</nts>
                  <nts id="Seg_7278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1590" id="Seg_7279" n="sc" s="T1585">
               <ts e="T1590" id="Seg_7281" n="HIAT:u" s="T1585">
                  <ts e="T1586" id="Seg_7283" n="HIAT:w" s="T1585">На</ts>
                  <nts id="Seg_7284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1587" id="Seg_7286" n="HIAT:w" s="T1586">этим</ts>
                  <nts id="Seg_7287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1588" id="Seg_7289" n="HIAT:w" s="T1587">месте</ts>
                  <nts id="Seg_7290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7291" n="HIAT:ip">(</nts>
                  <ts e="T1589" id="Seg_7293" n="HIAT:w" s="T1588">загорело=</ts>
                  <nts id="Seg_7294" n="HIAT:ip">)</nts>
                  <nts id="Seg_7295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1590" id="Seg_7297" n="HIAT:w" s="T1589">загорело</ts>
                  <nts id="Seg_7298" n="HIAT:ip">.</nts>
                  <nts id="Seg_7299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1598" id="Seg_7300" n="sc" s="T1591">
               <ts e="T1598" id="Seg_7302" n="HIAT:u" s="T1591">
                  <ts e="T1592" id="Seg_7304" n="HIAT:w" s="T1591">Тады</ts>
                  <nts id="Seg_7305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7306" n="HIAT:ip">(</nts>
                  <ts e="T1593" id="Seg_7308" n="HIAT:w" s="T1592">затушили</ts>
                  <nts id="Seg_7309" n="HIAT:ip">)</nts>
                  <nts id="Seg_7310" n="HIAT:ip">,</nts>
                  <nts id="Seg_7311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1594" id="Seg_7313" n="HIAT:w" s="T1593">теперь</ts>
                  <nts id="Seg_7314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1595" id="Seg_7316" n="HIAT:w" s="T1594">печка</ts>
                  <nts id="Seg_7317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1596" id="Seg_7319" n="HIAT:w" s="T1595">железна</ts>
                  <nts id="Seg_7320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1597" id="Seg_7322" n="HIAT:w" s="T1596">там</ts>
                  <nts id="Seg_7323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1598" id="Seg_7325" n="HIAT:w" s="T1597">стоит</ts>
                  <nts id="Seg_7326" n="HIAT:ip">.</nts>
                  <nts id="Seg_7327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1603" id="Seg_7328" n="sc" s="T1599">
               <ts e="T1603" id="Seg_7330" n="HIAT:u" s="T1599">
                  <ts e="T1600" id="Seg_7332" n="HIAT:w" s="T1599">Почему</ts>
                  <nts id="Seg_7333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1601" id="Seg_7335" n="HIAT:w" s="T1600">загорело</ts>
                  <nts id="Seg_7336" n="HIAT:ip">,</nts>
                  <nts id="Seg_7337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1602" id="Seg_7339" n="HIAT:w" s="T1601">не</ts>
                  <nts id="Seg_7340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1603" id="Seg_7342" n="HIAT:w" s="T1602">знаю</ts>
                  <nts id="Seg_7343" n="HIAT:ip">.</nts>
                  <nts id="Seg_7344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1610" id="Seg_7345" n="sc" s="T1606">
               <ts e="T1610" id="Seg_7347" n="HIAT:u" s="T1606">
                  <ts e="T1607" id="Seg_7349" n="HIAT:w" s="T1606">И</ts>
                  <nts id="Seg_7350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1608" id="Seg_7352" n="HIAT:w" s="T1607">потом</ts>
                  <nts id="Seg_7353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1609" id="Seg_7355" n="HIAT:w" s="T1608">там</ts>
                  <nts id="Seg_7356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1610" id="Seg_7358" n="HIAT:w" s="T1609">туды</ts>
                  <nts id="Seg_7359" n="HIAT:ip">.</nts>
                  <nts id="Seg_7360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1615" id="Seg_7361" n="sc" s="T1611">
               <ts e="T1615" id="Seg_7363" n="HIAT:u" s="T1611">
                  <ts e="T1612" id="Seg_7365" n="HIAT:w" s="T1611">И</ts>
                  <nts id="Seg_7366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1613" id="Seg_7368" n="HIAT:w" s="T1612">туды</ts>
                  <nts id="Seg_7369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1614" id="Seg_7371" n="HIAT:w" s="T1613">сзади</ts>
                  <nts id="Seg_7372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1615" id="Seg_7374" n="HIAT:w" s="T1614">там</ts>
                  <nts id="Seg_7375" n="HIAT:ip">.</nts>
                  <nts id="Seg_7376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1624" id="Seg_7377" n="sc" s="T1616">
               <ts e="T1624" id="Seg_7379" n="HIAT:u" s="T1616">
                  <ts e="T1617" id="Seg_7381" n="HIAT:w" s="T1616">Уже</ts>
                  <nts id="Seg_7382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1618" id="Seg_7384" n="HIAT:w" s="T1617">привозят</ts>
                  <nts id="Seg_7385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1619" id="Seg_7387" n="HIAT:w" s="T1618">машину</ts>
                  <nts id="Seg_7388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7389" n="HIAT:ip">(</nts>
                  <ts e="T1620" id="Seg_7391" n="HIAT:w" s="T1619">му-</ts>
                  <nts id="Seg_7392" n="HIAT:ip">)</nts>
                  <nts id="Seg_7393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1621" id="Seg_7395" n="HIAT:w" s="T1620">муки</ts>
                  <nts id="Seg_7396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1622" id="Seg_7398" n="HIAT:w" s="T1621">кули</ts>
                  <nts id="Seg_7399" n="HIAT:ip">,</nts>
                  <nts id="Seg_7400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1623" id="Seg_7402" n="HIAT:w" s="T1622">там</ts>
                  <nts id="Seg_7403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1624" id="Seg_7405" n="HIAT:w" s="T1623">ставят</ts>
                  <nts id="Seg_7406" n="HIAT:ip">.</nts>
                  <nts id="Seg_7407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1633" id="Seg_7408" n="sc" s="T1625">
               <ts e="T1633" id="Seg_7410" n="HIAT:u" s="T1625">
                  <ts e="T1626" id="Seg_7412" n="HIAT:w" s="T1625">И</ts>
                  <nts id="Seg_7413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1627" id="Seg_7415" n="HIAT:w" s="T1626">каку</ts>
                  <nts id="Seg_7416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1628" id="Seg_7418" n="HIAT:w" s="T1627">рыбу</ts>
                  <nts id="Seg_7419" n="HIAT:ip">,</nts>
                  <nts id="Seg_7420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1629" id="Seg_7422" n="HIAT:w" s="T1628">дак</ts>
                  <nts id="Seg_7423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1630" id="Seg_7425" n="HIAT:w" s="T1629">она</ts>
                  <nts id="Seg_7426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1631" id="Seg_7428" n="HIAT:w" s="T1630">оттедова</ts>
                  <nts id="Seg_7429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1632" id="Seg_7431" n="HIAT:w" s="T1631">приносит</ts>
                  <nts id="Seg_7432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1633" id="Seg_7434" n="HIAT:w" s="T1632">сюды</ts>
                  <nts id="Seg_7435" n="HIAT:ip">.</nts>
                  <nts id="Seg_7436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1641" id="Seg_7437" n="sc" s="T1634">
               <ts e="T1641" id="Seg_7439" n="HIAT:u" s="T1634">
                  <ts e="T1635" id="Seg_7441" n="HIAT:w" s="T1634">Привозят</ts>
                  <nts id="Seg_7442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1636" id="Seg_7444" n="HIAT:w" s="T1635">бочки</ts>
                  <nts id="Seg_7445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1637" id="Seg_7447" n="HIAT:w" s="T1636">пива</ts>
                  <nts id="Seg_7448" n="HIAT:ip">,</nts>
                  <nts id="Seg_7449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1638" id="Seg_7451" n="HIAT:w" s="T1637">и</ts>
                  <nts id="Seg_7452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1639" id="Seg_7454" n="HIAT:w" s="T1638">потом</ts>
                  <nts id="Seg_7455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1640" id="Seg_7457" n="HIAT:w" s="T1639">разное</ts>
                  <nts id="Seg_7458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1641" id="Seg_7460" n="HIAT:w" s="T1640">вино</ts>
                  <nts id="Seg_7461" n="HIAT:ip">.</nts>
                  <nts id="Seg_7462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1651" id="Seg_7463" n="sc" s="T1644">
               <ts e="T1651" id="Seg_7465" n="HIAT:u" s="T1644">
                  <ts e="T1645" id="Seg_7467" n="HIAT:w" s="T1644">Полный</ts>
                  <nts id="Seg_7468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1646" id="Seg_7470" n="HIAT:w" s="T1645">теперь</ts>
                  <nts id="Seg_7471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1647" id="Seg_7473" n="HIAT:w" s="T1646">магазин</ts>
                  <nts id="Seg_7474" n="HIAT:ip">,</nts>
                  <nts id="Seg_7475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1648" id="Seg_7477" n="HIAT:w" s="T1647">их</ts>
                  <nts id="Seg_7478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1649" id="Seg_7480" n="HIAT:w" s="T1648">две</ts>
                  <nts id="Seg_7481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1650" id="Seg_7483" n="HIAT:w" s="T1649">продавщицы</ts>
                  <nts id="Seg_7484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1651" id="Seg_7486" n="HIAT:w" s="T1650">были</ts>
                  <nts id="Seg_7487" n="HIAT:ip">.</nts>
                  <nts id="Seg_7488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1658" id="Seg_7489" n="sc" s="T1652">
               <ts e="T1658" id="Seg_7491" n="HIAT:u" s="T1652">
                  <ts e="T1653" id="Seg_7493" n="HIAT:w" s="T1652">А</ts>
                  <nts id="Seg_7494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1654" id="Seg_7496" n="HIAT:w" s="T1653">теперь</ts>
                  <nts id="Seg_7497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1655" id="Seg_7499" n="HIAT:w" s="T1654">одна</ts>
                  <nts id="Seg_7500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1656" id="Seg_7502" n="HIAT:w" s="T1655">моёго</ts>
                  <nts id="Seg_7503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1657" id="Seg_7505" n="HIAT:w" s="T1656">внучка</ts>
                  <nts id="Seg_7506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1658" id="Seg_7508" n="HIAT:w" s="T1657">жена</ts>
                  <nts id="Seg_7509" n="HIAT:ip">.</nts>
                  <nts id="Seg_7510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1671" id="Seg_7511" n="sc" s="T1662">
               <ts e="T1671" id="Seg_7513" n="HIAT:u" s="T1662">
                  <ts e="T1663" id="Seg_7515" n="HIAT:w" s="T1662">Вот</ts>
                  <nts id="Seg_7516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1664" id="Seg_7518" n="HIAT:w" s="T1663">ты</ts>
                  <nts id="Seg_7519" n="HIAT:ip">,</nts>
                  <nts id="Seg_7520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1665" id="Seg_7522" n="HIAT:w" s="T1664">можешь</ts>
                  <nts id="Seg_7523" n="HIAT:ip">,</nts>
                  <nts id="Seg_7524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1666" id="Seg_7526" n="HIAT:w" s="T1665">знаешь</ts>
                  <nts id="Seg_7527" n="HIAT:ip">,</nts>
                  <nts id="Seg_7528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1667" id="Seg_7530" n="HIAT:w" s="T1666">напротив</ts>
                  <nts id="Seg_7531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1668" id="Seg_7533" n="HIAT:w" s="T1667">Загороднюка</ts>
                  <nts id="Seg_7534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1669" id="Seg_7536" n="HIAT:w" s="T1668">жил</ts>
                  <nts id="Seg_7537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1670" id="Seg_7539" n="HIAT:w" s="T1669">Николай</ts>
                  <nts id="Seg_7540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1671" id="Seg_7542" n="HIAT:w" s="T1670">Мурачёв</ts>
                  <nts id="Seg_7543" n="HIAT:ip">.</nts>
                  <nts id="Seg_7544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1698" id="Seg_7545" n="sc" s="T1680">
               <ts e="T1698" id="Seg_7547" n="HIAT:u" s="T1680">
                  <ts e="T1682" id="Seg_7549" n="HIAT:w" s="T1680">Ну</ts>
                  <nts id="Seg_7550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7551" n="HIAT:ip">(</nts>
                  <ts e="T1683" id="Seg_7553" n="HIAT:w" s="T1682">он</ts>
                  <nts id="Seg_7554" n="HIAT:ip">)</nts>
                  <nts id="Seg_7555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1684" id="Seg_7557" n="HIAT:w" s="T1683">Николай</ts>
                  <nts id="Seg_7558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1685" id="Seg_7560" n="HIAT:w" s="T1684">Мурачёв</ts>
                  <nts id="Seg_7561" n="HIAT:ip">,</nts>
                  <nts id="Seg_7562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1686" id="Seg_7564" n="HIAT:w" s="T1685">он</ts>
                  <nts id="Seg_7565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1687" id="Seg_7567" n="HIAT:w" s="T1686">вот</ts>
                  <nts id="Seg_7568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1688" id="Seg_7570" n="HIAT:w" s="T1687">так</ts>
                  <nts id="Seg_7571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1689" id="Seg_7573" n="HIAT:w" s="T1688">вот</ts>
                  <nts id="Seg_7574" n="HIAT:ip">,</nts>
                  <nts id="Seg_7575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1690" id="Seg_7577" n="HIAT:w" s="T1689">где</ts>
                  <nts id="Seg_7578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1691" id="Seg_7580" n="HIAT:w" s="T1690">вы</ts>
                  <nts id="Seg_7581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1692" id="Seg_7583" n="HIAT:w" s="T1691">на</ts>
                  <nts id="Seg_7584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1693" id="Seg_7586" n="HIAT:w" s="T1692">фатере</ts>
                  <nts id="Seg_7587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1694" id="Seg_7589" n="HIAT:w" s="T1693">были</ts>
                  <nts id="Seg_7590" n="HIAT:ip">,</nts>
                  <nts id="Seg_7591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1695" id="Seg_7593" n="HIAT:w" s="T1694">а</ts>
                  <nts id="Seg_7594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1696" id="Seg_7596" n="HIAT:w" s="T1695">он</ts>
                  <nts id="Seg_7597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1697" id="Seg_7599" n="HIAT:w" s="T1696">вот</ts>
                  <nts id="Seg_7600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1698" id="Seg_7602" n="HIAT:w" s="T1697">сюды</ts>
                  <nts id="Seg_7603" n="HIAT:ip">.</nts>
                  <nts id="Seg_7604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1709" id="Seg_7605" n="sc" s="T1699">
               <ts e="T1709" id="Seg_7607" n="HIAT:u" s="T1699">
                  <ts e="T1700" id="Seg_7609" n="HIAT:w" s="T1699">На</ts>
                  <nts id="Seg_7610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1701" id="Seg_7612" n="HIAT:w" s="T1700">уголке</ts>
                  <nts id="Seg_7613" n="HIAT:ip">,</nts>
                  <nts id="Seg_7614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1702" id="Seg_7616" n="HIAT:w" s="T1701">за</ts>
                  <nts id="Seg_7617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1703" id="Seg_7619" n="HIAT:w" s="T1702">им</ts>
                  <nts id="Seg_7620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1704" id="Seg_7622" n="HIAT:w" s="T1703">еще</ts>
                  <nts id="Seg_7623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1705" id="Seg_7625" n="HIAT:w" s="T1704">один</ts>
                  <nts id="Seg_7626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1706" id="Seg_7628" n="HIAT:w" s="T1705">дом</ts>
                  <nts id="Seg_7629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1707" id="Seg_7631" n="HIAT:w" s="T1706">только</ts>
                  <nts id="Seg_7632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1708" id="Seg_7634" n="HIAT:w" s="T1707">стоит</ts>
                  <nts id="Seg_7635" n="HIAT:ip">,</nts>
                  <nts id="Seg_7636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1709" id="Seg_7638" n="HIAT:w" s="T1708">Загороднюк</ts>
                  <nts id="Seg_7639" n="HIAT:ip">.</nts>
                  <nts id="Seg_7640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T16" id="Seg_7641" n="sc" s="T6">
               <ts e="T8" id="Seg_7643" n="e" s="T6">Ну, </ts>
               <ts e="T11" id="Seg_7645" n="e" s="T8">можно </ts>
               <ts e="T13" id="Seg_7647" n="e" s="T11">по-русски </ts>
               <ts e="T16" id="Seg_7649" n="e" s="T13">рассказать. </ts>
            </ts>
            <ts e="T31" id="Seg_7650" n="sc" s="T29">
               <ts e="T30" id="Seg_7652" n="e" s="T29">Ну, </ts>
               <ts e="T31" id="Seg_7654" n="e" s="T30">идет? </ts>
            </ts>
            <ts e="T43" id="Seg_7655" n="sc" s="T35">
               <ts e="T36" id="Seg_7657" n="e" s="T35">Паша </ts>
               <ts e="T37" id="Seg_7659" n="e" s="T36">Василинич </ts>
               <ts e="T38" id="Seg_7661" n="e" s="T37">(свою </ts>
               <ts e="T39" id="Seg_7663" n="e" s="T38">Шу-) </ts>
               <ts e="T40" id="Seg_7665" n="e" s="T39">со </ts>
               <ts e="T41" id="Seg_7667" n="e" s="T40">своей </ts>
               <ts e="T42" id="Seg_7669" n="e" s="T41">Шурой </ts>
               <ts e="T43" id="Seg_7671" n="e" s="T42">разошлись. </ts>
            </ts>
            <ts e="T55" id="Seg_7672" n="sc" s="T44">
               <ts e="T45" id="Seg_7674" n="e" s="T44">Ей </ts>
               <ts e="T46" id="Seg_7676" n="e" s="T45">всё </ts>
               <ts e="T47" id="Seg_7678" n="e" s="T46">в </ts>
               <ts e="T48" id="Seg_7680" n="e" s="T47">доме </ts>
               <ts e="T49" id="Seg_7682" n="e" s="T48">осталось, </ts>
               <ts e="T50" id="Seg_7684" n="e" s="T49">а </ts>
               <ts e="T51" id="Seg_7686" n="e" s="T50">ему </ts>
               <ts e="T52" id="Seg_7688" n="e" s="T51">только </ts>
               <ts e="T53" id="Seg_7690" n="e" s="T52">одно </ts>
               <ts e="T54" id="Seg_7692" n="e" s="T53">мотоцикло </ts>
               <ts e="T55" id="Seg_7694" n="e" s="T54">досталось. </ts>
            </ts>
            <ts e="T62" id="Seg_7695" n="sc" s="T56">
               <ts e="T57" id="Seg_7697" n="e" s="T56">Он </ts>
               <ts e="T58" id="Seg_7699" n="e" s="T57">там </ts>
               <ts e="T59" id="Seg_7701" n="e" s="T58">связался </ts>
               <ts e="T60" id="Seg_7703" n="e" s="T59">с </ts>
               <ts e="T61" id="Seg_7705" n="e" s="T60">одной </ts>
               <ts e="T62" id="Seg_7707" n="e" s="T61">женщиной. </ts>
            </ts>
            <ts e="T68" id="Seg_7708" n="sc" s="T63">
               <ts e="T64" id="Seg_7710" n="e" s="T63">Она </ts>
               <ts e="T65" id="Seg_7712" n="e" s="T64">в </ts>
               <ts e="T66" id="Seg_7714" n="e" s="T65">конторе </ts>
               <ts e="T67" id="Seg_7716" n="e" s="T66">работала </ts>
               <ts e="T68" id="Seg_7718" n="e" s="T67">счетоводом. </ts>
            </ts>
            <ts e="T74" id="Seg_7719" n="sc" s="T69">
               <ts e="T70" id="Seg_7721" n="e" s="T69">У </ts>
               <ts e="T71" id="Seg_7723" n="e" s="T70">ей </ts>
               <ts e="T72" id="Seg_7725" n="e" s="T71">тоже </ts>
               <ts e="T73" id="Seg_7727" n="e" s="T72">мужик </ts>
               <ts e="T74" id="Seg_7729" n="e" s="T73">был. </ts>
            </ts>
            <ts e="T84" id="Seg_7730" n="sc" s="T75">
               <ts e="T76" id="Seg_7732" n="e" s="T75">Пьяный </ts>
               <ts e="T77" id="Seg_7734" n="e" s="T76">напился </ts>
               <ts e="T78" id="Seg_7736" n="e" s="T77">и </ts>
               <ts e="T79" id="Seg_7738" n="e" s="T78">спал, </ts>
               <ts e="T80" id="Seg_7740" n="e" s="T79">руку </ts>
               <ts e="T81" id="Seg_7742" n="e" s="T80">отморозил, </ts>
               <ts e="T82" id="Seg_7744" n="e" s="T81">в </ts>
               <ts e="T83" id="Seg_7746" n="e" s="T82">больнице </ts>
               <ts e="T84" id="Seg_7748" n="e" s="T83">лежал. </ts>
            </ts>
            <ts e="T89" id="Seg_7749" n="sc" s="T85">
               <ts e="T86" id="Seg_7751" n="e" s="T85">И </ts>
               <ts e="T87" id="Seg_7753" n="e" s="T86">уехал </ts>
               <ts e="T88" id="Seg_7755" n="e" s="T87">к </ts>
               <ts e="T89" id="Seg_7757" n="e" s="T88">матери. </ts>
            </ts>
            <ts e="T106" id="Seg_7758" n="sc" s="T90">
               <ts e="T91" id="Seg_7760" n="e" s="T90">Она </ts>
               <ts e="T92" id="Seg_7762" n="e" s="T91">потом </ts>
               <ts e="T93" id="Seg_7764" n="e" s="T92">выслала </ts>
               <ts e="T94" id="Seg_7766" n="e" s="T93">всё </ts>
               <ts e="T95" id="Seg_7768" n="e" s="T94">евонно. </ts>
               <ts e="T96" id="Seg_7770" n="e" s="T95">И </ts>
               <ts e="T97" id="Seg_7772" n="e" s="T96">с </ts>
               <ts e="T98" id="Seg_7774" n="e" s="T97">Пашей </ts>
               <ts e="T99" id="Seg_7776" n="e" s="T98">сошлась, </ts>
               <ts e="T100" id="Seg_7778" n="e" s="T99">а </ts>
               <ts e="T101" id="Seg_7780" n="e" s="T100">Паша </ts>
               <ts e="T102" id="Seg_7782" n="e" s="T101">разделился </ts>
               <ts e="T103" id="Seg_7784" n="e" s="T102">(со-) </ts>
               <ts e="T104" id="Seg_7786" n="e" s="T103">со </ts>
               <ts e="T105" id="Seg_7788" n="e" s="T104">своёй </ts>
               <ts e="T106" id="Seg_7790" n="e" s="T105">женой. </ts>
            </ts>
            <ts e="T113" id="Seg_7791" n="sc" s="T107">
               <ts e="T108" id="Seg_7793" n="e" s="T107">И </ts>
               <ts e="T109" id="Seg_7795" n="e" s="T108">на </ts>
               <ts e="T110" id="Seg_7797" n="e" s="T109">суд </ts>
               <ts e="T111" id="Seg_7799" n="e" s="T110">она </ts>
               <ts e="T112" id="Seg_7801" n="e" s="T111">подала, </ts>
               <ts e="T113" id="Seg_7803" n="e" s="T112">он… </ts>
            </ts>
            <ts e="T118" id="Seg_7804" n="sc" s="T114">
               <ts e="T115" id="Seg_7806" n="e" s="T114">Высудили </ts>
               <ts e="T116" id="Seg_7808" n="e" s="T115">с </ts>
               <ts e="T117" id="Seg_7810" n="e" s="T116">его </ts>
               <ts e="T118" id="Seg_7812" n="e" s="T117">алименты. </ts>
            </ts>
            <ts e="T122" id="Seg_7813" n="sc" s="T119">
               <ts e="T120" id="Seg_7815" n="e" s="T119">Всем </ts>
               <ts e="T121" id="Seg_7817" n="e" s="T120">троим </ts>
               <ts e="T122" id="Seg_7819" n="e" s="T121">платит. </ts>
            </ts>
            <ts e="T130" id="Seg_7820" n="sc" s="T123">
               <ts e="T124" id="Seg_7822" n="e" s="T123">Когда </ts>
               <ts e="T125" id="Seg_7824" n="e" s="T124">она </ts>
               <ts e="T126" id="Seg_7826" n="e" s="T125">собралась, </ts>
               <ts e="T127" id="Seg_7828" n="e" s="T126">поехала </ts>
               <ts e="T128" id="Seg_7830" n="e" s="T127">туды, </ts>
               <ts e="T129" id="Seg_7832" n="e" s="T128">в </ts>
               <ts e="T130" id="Seg_7834" n="e" s="T129">Сушенское. </ts>
            </ts>
            <ts e="T137" id="Seg_7835" n="sc" s="T131">
               <ts e="T132" id="Seg_7837" n="e" s="T131">Он </ts>
               <ts e="T133" id="Seg_7839" n="e" s="T132">мальчишка </ts>
               <ts e="T134" id="Seg_7841" n="e" s="T133">одного </ts>
               <ts e="T135" id="Seg_7843" n="e" s="T134">не </ts>
               <ts e="T136" id="Seg_7845" n="e" s="T135">отдал </ts>
               <ts e="T137" id="Seg_7847" n="e" s="T136">ей. </ts>
            </ts>
            <ts e="T146" id="Seg_7848" n="sc" s="T138">
               <ts e="T139" id="Seg_7850" n="e" s="T138">Но </ts>
               <ts e="T140" id="Seg_7852" n="e" s="T139">она </ts>
               <ts e="T141" id="Seg_7854" n="e" s="T140">всё </ts>
               <ts e="T142" id="Seg_7856" n="e" s="T141">хотела </ts>
               <ts e="T143" id="Seg_7858" n="e" s="T142">отобрать, </ts>
               <ts e="T144" id="Seg_7860" n="e" s="T143">он </ts>
               <ts e="T145" id="Seg_7862" n="e" s="T144">его </ts>
               <ts e="T146" id="Seg_7864" n="e" s="T145">увез. </ts>
            </ts>
            <ts e="T154" id="Seg_7865" n="sc" s="T147">
               <ts e="T148" id="Seg_7867" n="e" s="T147">А </ts>
               <ts e="T149" id="Seg_7869" n="e" s="T148">теперь </ts>
               <ts e="T150" id="Seg_7871" n="e" s="T149">нашу </ts>
               <ts e="T151" id="Seg_7873" n="e" s="T150">деревню </ts>
               <ts e="T152" id="Seg_7875" n="e" s="T151">с </ts>
               <ts e="T153" id="Seg_7877" n="e" s="T152">Вознесенкой </ts>
               <ts e="T154" id="Seg_7879" n="e" s="T153">соединили. </ts>
            </ts>
            <ts e="T161" id="Seg_7880" n="sc" s="T155">
               <ts e="T156" id="Seg_7882" n="e" s="T155">И </ts>
               <ts e="T157" id="Seg_7884" n="e" s="T156">там </ts>
               <ts e="T158" id="Seg_7886" n="e" s="T157">он </ts>
               <ts e="T159" id="Seg_7888" n="e" s="T158">живет </ts>
               <ts e="T160" id="Seg_7890" n="e" s="T159">с </ts>
               <ts e="T161" id="Seg_7892" n="e" s="T160">ей. </ts>
            </ts>
            <ts e="T172" id="Seg_7893" n="sc" s="T162">
               <ts e="T163" id="Seg_7895" n="e" s="T162">А </ts>
               <ts e="T164" id="Seg_7897" n="e" s="T163">мать </ts>
               <ts e="T165" id="Seg_7899" n="e" s="T164">евонна </ts>
               <ts e="T166" id="Seg_7901" n="e" s="T165">тут </ts>
               <ts e="T167" id="Seg_7903" n="e" s="T166">живет, </ts>
               <ts e="T168" id="Seg_7905" n="e" s="T167">а </ts>
               <ts e="T169" id="Seg_7907" n="e" s="T168">изба </ts>
               <ts e="T170" id="Seg_7909" n="e" s="T169">стоит </ts>
               <ts e="T171" id="Seg_7911" n="e" s="T170">одна, </ts>
               <ts e="T172" id="Seg_7913" n="e" s="T171">пустая. </ts>
            </ts>
            <ts e="T179" id="Seg_7914" n="sc" s="T173">
               <ts e="T174" id="Seg_7916" n="e" s="T173">Никто </ts>
               <ts e="T175" id="Seg_7918" n="e" s="T174">не </ts>
               <ts e="T176" id="Seg_7920" n="e" s="T175">берет, </ts>
               <ts e="T177" id="Seg_7922" n="e" s="T176">люди </ts>
               <ts e="T178" id="Seg_7924" n="e" s="T177">все </ts>
               <ts e="T179" id="Seg_7926" n="e" s="T178">разъезжаются. </ts>
            </ts>
            <ts e="T183" id="Seg_7927" n="sc" s="T180">
               <ts e="T181" id="Seg_7929" n="e" s="T180">С </ts>
               <ts e="T182" id="Seg_7931" n="e" s="T181">той </ts>
               <ts e="T183" id="Seg_7933" n="e" s="T182">деревни. </ts>
            </ts>
            <ts e="T194" id="Seg_7934" n="sc" s="T184">
               <ts e="T185" id="Seg_7936" n="e" s="T184">(Pa-) </ts>
               <ts e="T186" id="Seg_7938" n="e" s="T185">Paša </ts>
               <ts e="T187" id="Seg_7940" n="e" s="T186">Vasilinič </ts>
               <ts e="T188" id="Seg_7942" n="e" s="T187">bostə </ts>
               <ts e="T189" id="Seg_7944" n="e" s="T188">Šuranə </ts>
               <ts e="T190" id="Seg_7946" n="e" s="T189">baʔluʔpi </ts>
               <ts e="T191" id="Seg_7948" n="e" s="T190">i </ts>
               <ts e="T192" id="Seg_7950" n="e" s="T191">baška </ts>
               <ts e="T193" id="Seg_7952" n="e" s="T192">ne </ts>
               <ts e="T194" id="Seg_7954" n="e" s="T193">ibi. </ts>
            </ts>
            <ts e="T201" id="Seg_7955" n="sc" s="T195">
               <ts e="T196" id="Seg_7957" n="e" s="T195">Dĭgəttə </ts>
               <ts e="T197" id="Seg_7959" n="e" s="T196">dĭ </ts>
               <ts e="T198" id="Seg_7961" n="e" s="T197">dĭʔnə </ts>
               <ts e="T199" id="Seg_7963" n="e" s="T198">подала </ts>
               <ts e="T200" id="Seg_7965" n="e" s="T199">suttə </ts>
               <ts e="T201" id="Seg_7967" n="e" s="T200">dĭ. </ts>
            </ts>
            <ts e="T206" id="Seg_7968" n="sc" s="T202">
               <ts e="T203" id="Seg_7970" n="e" s="T202">Dĭzeŋ </ts>
               <ts e="T204" id="Seg_7972" n="e" s="T203">(су-) </ts>
               <ts e="T205" id="Seg_7974" n="e" s="T204">судили </ts>
               <ts e="T206" id="Seg_7976" n="e" s="T205">dĭ. </ts>
            </ts>
            <ts e="T211" id="Seg_7977" n="sc" s="T207">
               <ts e="T208" id="Seg_7979" n="e" s="T207">Tüj </ts>
               <ts e="T209" id="Seg_7981" n="e" s="T208">aktʼa </ts>
               <ts e="T210" id="Seg_7983" n="e" s="T209">esseŋdə </ts>
               <ts e="T211" id="Seg_7985" n="e" s="T210">mĭʔleʔbə. </ts>
            </ts>
            <ts e="T218" id="Seg_7986" n="sc" s="T212">
               <ts e="T213" id="Seg_7988" n="e" s="T212">A </ts>
               <ts e="T214" id="Seg_7990" n="e" s="T213">dĭ </ts>
               <ts e="T1738" id="Seg_7992" n="e" s="T214">kalla </ts>
               <ts e="T215" id="Seg_7994" n="e" s="T1738">dʼürbi </ts>
               <ts e="T216" id="Seg_7996" n="e" s="T215">gijen </ts>
               <ts e="T217" id="Seg_7998" n="e" s="T216">Lenin </ts>
               <ts e="T218" id="Seg_8000" n="e" s="T217">amnolaʔpi. </ts>
            </ts>
            <ts e="T223" id="Seg_8001" n="sc" s="T219">
               <ts e="T220" id="Seg_8003" n="e" s="T219">Dĭn </ts>
               <ts e="T221" id="Seg_8005" n="e" s="T220">amnolaʔbə </ts>
               <ts e="T222" id="Seg_8007" n="e" s="T221">(dĭ) </ts>
               <ts e="T223" id="Seg_8009" n="e" s="T222">tüj. </ts>
            </ts>
            <ts e="T234" id="Seg_8010" n="sc" s="T224">
               <ts e="T225" id="Seg_8012" n="e" s="T224">Šide </ts>
               <ts e="T226" id="Seg_8014" n="e" s="T225">nʼi </ts>
               <ts e="T227" id="Seg_8016" n="e" s="T226">i </ts>
               <ts e="T228" id="Seg_8018" n="e" s="T227">(ko-) </ts>
               <ts e="T229" id="Seg_8020" n="e" s="T228">koʔbdo </ts>
               <ts e="T230" id="Seg_8022" n="e" s="T229">dĭzi, </ts>
               <ts e="T231" id="Seg_8024" n="e" s="T230">onʼiʔ </ts>
               <ts e="T232" id="Seg_8026" n="e" s="T231">nʼi </ts>
               <ts e="T233" id="Seg_8028" n="e" s="T232">(dĭ=) </ts>
               <ts e="T234" id="Seg_8030" n="e" s="T233">dĭn. </ts>
            </ts>
            <ts e="T244" id="Seg_8031" n="sc" s="T235">
               <ts e="T236" id="Seg_8033" n="e" s="T235">Dĭgəttə </ts>
               <ts e="T237" id="Seg_8035" n="e" s="T236">(m-) </ts>
               <ts e="T238" id="Seg_8037" n="e" s="T237">măn </ts>
               <ts e="T239" id="Seg_8039" n="e" s="T238">koʔbdom </ts>
               <ts e="T240" id="Seg_8041" n="e" s="T239">šobi </ts>
               <ts e="T241" id="Seg_8043" n="e" s="T240">(sjuda), </ts>
               <ts e="T242" id="Seg_8045" n="e" s="T241">onʼiʔ </ts>
               <ts e="T243" id="Seg_8047" n="e" s="T242">nʼibə </ts>
               <ts e="T244" id="Seg_8049" n="e" s="T243">deʔpi. </ts>
            </ts>
            <ts e="T252" id="Seg_8050" n="sc" s="T245">
               <ts e="T246" id="Seg_8052" n="e" s="T245">Dĭ </ts>
               <ts e="T247" id="Seg_8054" n="e" s="T246">măndə </ts>
               <ts e="T248" id="Seg_8056" n="e" s="T247">Pašanə: </ts>
               <ts e="T249" id="Seg_8058" n="e" s="T248">"Iʔ </ts>
               <ts e="T250" id="Seg_8060" n="e" s="T249">iʔ </ts>
               <ts e="T251" id="Seg_8062" n="e" s="T250">šide </ts>
               <ts e="T252" id="Seg_8064" n="e" s="T251">nʼi! </ts>
            </ts>
            <ts e="T255" id="Seg_8065" n="sc" s="T253">
               <ts e="T254" id="Seg_8067" n="e" s="T253">Ej </ts>
               <ts e="T255" id="Seg_8069" n="e" s="T254">kereʔ". </ts>
            </ts>
            <ts e="T263" id="Seg_8070" n="sc" s="T256">
               <ts e="T257" id="Seg_8072" n="e" s="T256">Nʼebosʼ </ts>
               <ts e="T258" id="Seg_8074" n="e" s="T257">dĭm </ts>
               <ts e="T259" id="Seg_8076" n="e" s="T258">ibi, </ts>
               <ts e="T260" id="Seg_8078" n="e" s="T259">a </ts>
               <ts e="T261" id="Seg_8080" n="e" s="T260">esseŋdə </ts>
               <ts e="T262" id="Seg_8082" n="e" s="T261">ej </ts>
               <ts e="T263" id="Seg_8084" n="e" s="T262">kereʔ. </ts>
            </ts>
            <ts e="T269" id="Seg_8085" n="sc" s="T266">
               <ts e="T267" id="Seg_8087" n="e" s="T266">((BRK)) </ts>
               <ts e="T268" id="Seg_8089" n="e" s="T267">Только </ts>
               <ts e="T269" id="Seg_8091" n="e" s="T268">по-своему. </ts>
            </ts>
            <ts e="T288" id="Seg_8092" n="sc" s="T286">
               <ts e="T287" id="Seg_8094" n="e" s="T286">Я </ts>
               <ts e="T288" id="Seg_8096" n="e" s="T287">говорила. </ts>
            </ts>
            <ts e="T310" id="Seg_8097" n="sc" s="T292">
               <ts e="T294" id="Seg_8099" n="e" s="T292">(Ну) </ts>
               <ts e="T295" id="Seg_8101" n="e" s="T294">всё, </ts>
               <ts e="T297" id="Seg_8103" n="e" s="T295">как </ts>
               <ts e="T298" id="Seg_8105" n="e" s="T297">Паша </ts>
               <ts e="T301" id="Seg_8107" n="e" s="T298">сошелся, </ts>
               <ts e="T302" id="Seg_8109" n="e" s="T301">как </ts>
               <ts e="T304" id="Seg_8111" n="e" s="T302">детей, </ts>
               <ts e="T305" id="Seg_8113" n="e" s="T304">как </ts>
               <ts e="T306" id="Seg_8115" n="e" s="T305">на </ts>
               <ts e="T307" id="Seg_8117" n="e" s="T306">суд </ts>
               <ts e="T308" id="Seg_8119" n="e" s="T307">подала. </ts>
               <ts e="T309" id="Seg_8121" n="e" s="T308">Лименты </ts>
               <ts e="T310" id="Seg_8123" n="e" s="T309">высудила. </ts>
            </ts>
            <ts e="T340" id="Seg_8124" n="sc" s="T323">
               <ts e="T326" id="Seg_8126" n="e" s="T323">Ну, </ts>
               <ts e="T328" id="Seg_8128" n="e" s="T326">я </ts>
               <ts e="T330" id="Seg_8130" n="e" s="T328">говорила </ts>
               <ts e="T332" id="Seg_8132" n="e" s="T330">(с=) </ts>
               <ts e="T334" id="Seg_8134" n="e" s="T332">с </ts>
               <ts e="T336" id="Seg_8136" n="e" s="T334">им </ts>
               <ts e="T338" id="Seg_8138" n="e" s="T336">сошлася </ts>
               <ts e="T340" id="Seg_8140" n="e" s="T338">она. </ts>
            </ts>
            <ts e="T350" id="Seg_8141" n="sc" s="T348">
               <ts e="T350" id="Seg_8143" n="e" s="T348">Говорила. </ts>
            </ts>
            <ts e="T358" id="Seg_8144" n="sc" s="T352">
               <ts e="T354" id="Seg_8146" n="e" s="T352">И </ts>
               <ts e="T355" id="Seg_8148" n="e" s="T354">как </ts>
               <ts e="T356" id="Seg_8150" n="e" s="T355">он </ts>
               <ts e="T357" id="Seg_8152" n="e" s="T356">руки </ts>
               <ts e="T358" id="Seg_8154" n="e" s="T357">отморозил. </ts>
            </ts>
            <ts e="T369" id="Seg_8155" n="sc" s="T359">
               <ts e="T360" id="Seg_8157" n="e" s="T359">Ну, </ts>
               <ts e="T361" id="Seg_8159" n="e" s="T360">он </ts>
               <ts e="T362" id="Seg_8161" n="e" s="T361">же </ts>
               <ts e="T363" id="Seg_8163" n="e" s="T362">говорит? </ts>
               <ts e="T365" id="Seg_8165" n="e" s="T363">Я </ts>
               <ts e="T367" id="Seg_8167" n="e" s="T365">уж </ts>
               <ts e="T369" id="Seg_8169" n="e" s="T367">(рас-). </ts>
            </ts>
            <ts e="T383" id="Seg_8170" n="sc" s="T371">
               <ts e="T373" id="Seg_8172" n="e" s="T371">Рассказывала </ts>
               <ts e="T375" id="Seg_8174" n="e" s="T373">же </ts>
               <ts e="T377" id="Seg_8176" n="e" s="T375">я </ts>
               <ts e="T378" id="Seg_8178" n="e" s="T377">по-русски, </ts>
               <ts e="T379" id="Seg_8180" n="e" s="T378">или </ts>
               <ts e="T380" id="Seg_8182" n="e" s="T379">нет, </ts>
               <ts e="T382" id="Seg_8184" n="e" s="T380">только </ts>
               <ts e="T383" id="Seg_8186" n="e" s="T382">по-своему? </ts>
            </ts>
            <ts e="T422" id="Seg_8187" n="sc" s="T421">
               <ts e="T422" id="Seg_8189" n="e" s="T421">Тута? </ts>
            </ts>
            <ts e="T426" id="Seg_8190" n="sc" s="T425">
               <ts e="T426" id="Seg_8192" n="e" s="T425">Забыла… </ts>
            </ts>
            <ts e="T429" id="Seg_8193" n="sc" s="T427">
               <ts e="T428" id="Seg_8195" n="e" s="T427">Ну, </ts>
               <ts e="T429" id="Seg_8197" n="e" s="T428">давай. </ts>
            </ts>
            <ts e="T432" id="Seg_8198" n="sc" s="T430">
               <ts e="T431" id="Seg_8200" n="e" s="T430">Как </ts>
               <ts e="T432" id="Seg_8202" n="e" s="T431">начать-то? </ts>
            </ts>
            <ts e="T434" id="Seg_8203" n="sc" s="T433">
               <ts e="T434" id="Seg_8205" n="e" s="T433">По-своему? </ts>
            </ts>
            <ts e="T438" id="Seg_8206" n="sc" s="T435">
               <ts e="T436" id="Seg_8208" n="e" s="T435">По-русски </ts>
               <ts e="T438" id="Seg_8210" n="e" s="T436">рассказать? </ts>
            </ts>
            <ts e="T455" id="Seg_8211" n="sc" s="T443">
               <ts e="T445" id="Seg_8213" n="e" s="T443">Нет, </ts>
               <ts e="T447" id="Seg_8215" n="e" s="T445">я </ts>
               <ts e="T448" id="Seg_8217" n="e" s="T447">говорила </ts>
               <ts e="T449" id="Seg_8219" n="e" s="T448">вот </ts>
               <ts e="T450" id="Seg_8221" n="e" s="T449">сейчас, </ts>
               <ts e="T451" id="Seg_8223" n="e" s="T450">как </ts>
               <ts e="T452" id="Seg_8225" n="e" s="T451">он </ts>
               <ts e="T453" id="Seg_8227" n="e" s="T452">сошелся </ts>
               <ts e="T454" id="Seg_8229" n="e" s="T453">с </ts>
               <ts e="T455" id="Seg_8231" n="e" s="T454">ей. </ts>
            </ts>
            <ts e="T461" id="Seg_8232" n="sc" s="T458">
               <ts e="T459" id="Seg_8234" n="e" s="T458">На </ts>
               <ts e="T460" id="Seg_8236" n="e" s="T459">свою </ts>
               <ts e="T461" id="Seg_8238" n="e" s="T460">наречие. </ts>
            </ts>
            <ts e="T470" id="Seg_8239" n="sc" s="T462">
               <ts e="T463" id="Seg_8241" n="e" s="T462">А </ts>
               <ts e="T464" id="Seg_8243" n="e" s="T463">по-русски-то </ts>
               <ts e="T465" id="Seg_8245" n="e" s="T464">я </ts>
               <ts e="T468" id="Seg_8247" n="e" s="T465">не </ts>
               <ts e="T470" id="Seg_8249" n="e" s="T468">рассказала. </ts>
            </ts>
            <ts e="T502" id="Seg_8250" n="sc" s="T497">
               <ts e="T498" id="Seg_8252" n="e" s="T497">Ну, </ts>
               <ts e="T499" id="Seg_8254" n="e" s="T498">чего </ts>
               <ts e="T502" id="Seg_8256" n="e" s="T499">нового… </ts>
            </ts>
            <ts e="T525" id="Seg_8257" n="sc" s="T520">
               <ts e="T523" id="Seg_8259" n="e" s="T520">Нет, </ts>
               <ts e="T525" id="Seg_8261" n="e" s="T523">это… </ts>
            </ts>
            <ts e="T532" id="Seg_8262" n="sc" s="T527">
               <ts e="T528" id="Seg_8264" n="e" s="T527">Теперь </ts>
               <ts e="T529" id="Seg_8266" n="e" s="T528">у </ts>
               <ts e="T530" id="Seg_8268" n="e" s="T529">нас </ts>
               <ts e="T531" id="Seg_8270" n="e" s="T530">Загороднюк </ts>
               <ts e="T532" id="Seg_8272" n="e" s="T531">помер. </ts>
            </ts>
            <ts e="T544" id="Seg_8273" n="sc" s="T533">
               <ts e="T534" id="Seg_8275" n="e" s="T533">Можно </ts>
               <ts e="T535" id="Seg_8277" n="e" s="T534">про </ts>
               <ts e="T536" id="Seg_8279" n="e" s="T535">его </ts>
               <ts e="T537" id="Seg_8281" n="e" s="T536">сказать, </ts>
               <ts e="T538" id="Seg_8283" n="e" s="T537">ты </ts>
               <ts e="T539" id="Seg_8285" n="e" s="T538">не </ts>
               <ts e="T540" id="Seg_8287" n="e" s="T539">знал </ts>
               <ts e="T542" id="Seg_8289" n="e" s="T540">про </ts>
               <ts e="T544" id="Seg_8291" n="e" s="T542">его? </ts>
            </ts>
            <ts e="T561" id="Seg_8292" n="sc" s="T549">
               <ts e="T550" id="Seg_8294" n="e" s="T549">Ну </ts>
               <ts e="T552" id="Seg_8296" n="e" s="T550">а </ts>
               <ts e="T554" id="Seg_8298" n="e" s="T552">ты </ts>
               <ts e="T556" id="Seg_8300" n="e" s="T554">не </ts>
               <ts e="T558" id="Seg_8302" n="e" s="T556">знал, </ts>
               <ts e="T559" id="Seg_8304" n="e" s="T558">что </ts>
               <ts e="T561" id="Seg_8306" n="e" s="T559">помер? </ts>
            </ts>
            <ts e="T574" id="Seg_8307" n="sc" s="T564">
               <ts e="T566" id="Seg_8309" n="e" s="T564">(Уже=) </ts>
               <ts e="T568" id="Seg_8311" n="e" s="T566">Уже </ts>
               <ts e="T570" id="Seg_8313" n="e" s="T568">два </ts>
               <ts e="T571" id="Seg_8315" n="e" s="T570">года, </ts>
               <ts e="T572" id="Seg_8317" n="e" s="T571">как </ts>
               <ts e="T574" id="Seg_8319" n="e" s="T572">помер. </ts>
            </ts>
            <ts e="T582" id="Seg_8320" n="sc" s="T576">
               <ts e="T577" id="Seg_8322" n="e" s="T576">Так </ts>
               <ts e="T578" id="Seg_8324" n="e" s="T577">я </ts>
               <ts e="T579" id="Seg_8326" n="e" s="T578">сильно </ts>
               <ts e="T580" id="Seg_8328" n="e" s="T579">плакала </ts>
               <ts e="T581" id="Seg_8330" n="e" s="T580">об </ts>
               <ts e="T582" id="Seg_8332" n="e" s="T581">ем. </ts>
            </ts>
            <ts e="T587" id="Seg_8333" n="sc" s="T583">
               <ts e="T584" id="Seg_8335" n="e" s="T583">Хороший </ts>
               <ts e="T585" id="Seg_8337" n="e" s="T584">человек </ts>
               <ts e="T587" id="Seg_8339" n="e" s="T585">был. </ts>
            </ts>
            <ts e="T606" id="Seg_8340" n="sc" s="T592">
               <ts e="T594" id="Seg_8342" n="e" s="T592">Вот, </ts>
               <ts e="T596" id="Seg_8344" n="e" s="T594">бывало, </ts>
               <ts e="T598" id="Seg_8346" n="e" s="T596">приду, </ts>
               <ts e="T599" id="Seg_8348" n="e" s="T598">куды </ts>
               <ts e="T600" id="Seg_8350" n="e" s="T599">мне </ts>
               <ts e="T601" id="Seg_8352" n="e" s="T600">надо </ts>
               <ts e="T602" id="Seg_8354" n="e" s="T601">ехать, </ts>
               <ts e="T603" id="Seg_8356" n="e" s="T602">в </ts>
               <ts e="T604" id="Seg_8358" n="e" s="T603">Уяр </ts>
               <ts e="T605" id="Seg_8360" n="e" s="T604">али </ts>
               <ts e="T606" id="Seg_8362" n="e" s="T605">куды. </ts>
            </ts>
            <ts e="T610" id="Seg_8363" n="sc" s="T607">
               <ts e="T608" id="Seg_8365" n="e" s="T607">В </ts>
               <ts e="T609" id="Seg_8367" n="e" s="T608">Агинско </ts>
               <ts e="T610" id="Seg_8369" n="e" s="T609">ли. </ts>
            </ts>
            <ts e="T625" id="Seg_8370" n="sc" s="T611">
               <ts e="T612" id="Seg_8372" n="e" s="T611">(Прид-) </ts>
               <ts e="T613" id="Seg_8374" n="e" s="T612">Я </ts>
               <ts e="T614" id="Seg_8376" n="e" s="T613">все </ts>
               <ts e="T615" id="Seg_8378" n="e" s="T614">его </ts>
               <ts e="T616" id="Seg_8380" n="e" s="T615">чтоб </ts>
               <ts e="T617" id="Seg_8382" n="e" s="T616">одного </ts>
               <ts e="T618" id="Seg_8384" n="e" s="T617">захватить, </ts>
               <ts e="T619" id="Seg_8386" n="e" s="T618">никто </ts>
               <ts e="T620" id="Seg_8388" n="e" s="T619">не </ts>
               <ts e="T621" id="Seg_8390" n="e" s="T620">слыхал </ts>
               <ts e="T622" id="Seg_8392" n="e" s="T621">и </ts>
               <ts e="T623" id="Seg_8394" n="e" s="T622">не </ts>
               <ts e="T624" id="Seg_8396" n="e" s="T623">завидывал </ts>
               <ts e="T625" id="Seg_8398" n="e" s="T624">бы. </ts>
            </ts>
            <ts e="T631" id="Seg_8399" n="sc" s="T626">
               <ts e="T627" id="Seg_8401" n="e" s="T626">Он </ts>
               <ts e="T628" id="Seg_8403" n="e" s="T627">же </ts>
               <ts e="T629" id="Seg_8405" n="e" s="T628">против </ts>
               <ts e="T630" id="Seg_8407" n="e" s="T629">меня </ts>
               <ts e="T631" id="Seg_8409" n="e" s="T630">жил. </ts>
            </ts>
            <ts e="T655" id="Seg_8410" n="sc" s="T632">
               <ts e="T633" id="Seg_8412" n="e" s="T632">Когда </ts>
               <ts e="T634" id="Seg_8414" n="e" s="T633">идет, </ts>
               <ts e="T635" id="Seg_8416" n="e" s="T634">я </ts>
               <ts e="T636" id="Seg_8418" n="e" s="T635">иду </ts>
               <ts e="T637" id="Seg_8420" n="e" s="T636">навстречу, </ts>
               <ts e="T638" id="Seg_8422" n="e" s="T637">скажу: </ts>
               <ts e="T639" id="Seg_8424" n="e" s="T638">"Василий </ts>
               <ts e="T640" id="Seg_8426" n="e" s="T639">Федорович, </ts>
               <ts e="T641" id="Seg_8428" n="e" s="T640">(мож-) </ts>
               <ts e="T642" id="Seg_8430" n="e" s="T641">машины </ts>
               <ts e="T643" id="Seg_8432" n="e" s="T642">пойдут, </ts>
               <ts e="T644" id="Seg_8434" n="e" s="T643">можно </ts>
               <ts e="T645" id="Seg_8436" n="e" s="T644">уехать?" </ts>
               <ts e="T646" id="Seg_8438" n="e" s="T645">Он </ts>
               <ts e="T647" id="Seg_8440" n="e" s="T646">скажет: </ts>
               <ts e="T648" id="Seg_8442" n="e" s="T647">"Завтре </ts>
               <ts e="T649" id="Seg_8444" n="e" s="T648">со </ts>
               <ts e="T650" id="Seg_8446" n="e" s="T649">мной". </ts>
               <ts e="T651" id="Seg_8448" n="e" s="T650">"А </ts>
               <ts e="T652" id="Seg_8450" n="e" s="T651">куды </ts>
               <ts e="T653" id="Seg_8452" n="e" s="T652">ты, </ts>
               <ts e="T654" id="Seg_8454" n="e" s="T653">говорит, </ts>
               <ts e="T655" id="Seg_8456" n="e" s="T654">хочешь?" </ts>
            </ts>
            <ts e="T672" id="Seg_8457" n="sc" s="T656">
               <ts e="T657" id="Seg_8459" n="e" s="T656">Я </ts>
               <ts e="T658" id="Seg_8461" n="e" s="T657">говорю: </ts>
               <ts e="T659" id="Seg_8463" n="e" s="T658">"Да </ts>
               <ts e="T660" id="Seg_8465" n="e" s="T659">((…)) </ts>
               <ts e="T661" id="Seg_8467" n="e" s="T660">тебе </ts>
               <ts e="T662" id="Seg_8469" n="e" s="T661">как </ts>
               <ts e="T663" id="Seg_8471" n="e" s="T662">сыну </ts>
               <ts e="T664" id="Seg_8473" n="e" s="T663">расскажу, </ts>
               <ts e="T665" id="Seg_8475" n="e" s="T664">еду </ts>
               <ts e="T666" id="Seg_8477" n="e" s="T665">богу </ts>
               <ts e="T667" id="Seg_8479" n="e" s="T666">молиться. </ts>
               <ts e="T668" id="Seg_8481" n="e" s="T667">"Завтре </ts>
               <ts e="T669" id="Seg_8483" n="e" s="T668">обязательно </ts>
               <ts e="T670" id="Seg_8485" n="e" s="T669">со </ts>
               <ts e="T671" id="Seg_8487" n="e" s="T670">мной </ts>
               <ts e="T672" id="Seg_8489" n="e" s="T671">уедешь". </ts>
            </ts>
            <ts e="T681" id="Seg_8490" n="sc" s="T673">
               <ts e="T674" id="Seg_8492" n="e" s="T673">Ну, </ts>
               <ts e="T675" id="Seg_8494" n="e" s="T674">я </ts>
               <ts e="T676" id="Seg_8496" n="e" s="T675">на </ts>
               <ts e="T677" id="Seg_8498" n="e" s="T676">утро </ts>
               <ts e="T678" id="Seg_8500" n="e" s="T677">поднимаюсь, </ts>
               <ts e="T679" id="Seg_8502" n="e" s="T678">всё </ts>
               <ts e="T680" id="Seg_8504" n="e" s="T679">складаю </ts>
               <ts e="T681" id="Seg_8506" n="e" s="T680">и… </ts>
            </ts>
            <ts e="T688" id="Seg_8507" n="sc" s="T682">
               <ts e="T683" id="Seg_8509" n="e" s="T682">Они </ts>
               <ts e="T684" id="Seg_8511" n="e" s="T683">же </ts>
               <ts e="T685" id="Seg_8513" n="e" s="T684">(ра-) </ts>
               <ts e="T686" id="Seg_8515" n="e" s="T685">знаешь, </ts>
               <ts e="T687" id="Seg_8517" n="e" s="T686">где </ts>
               <ts e="T688" id="Seg_8519" n="e" s="T687">жили? </ts>
            </ts>
            <ts e="T701" id="Seg_8520" n="sc" s="T689">
               <ts e="T690" id="Seg_8522" n="e" s="T689">(Сейчас) </ts>
               <ts e="T691" id="Seg_8524" n="e" s="T690">сюды, </ts>
               <ts e="T692" id="Seg_8526" n="e" s="T691">к </ts>
               <ts e="T693" id="Seg_8528" n="e" s="T692">имя </ts>
               <ts e="T694" id="Seg_8530" n="e" s="T693">приду, </ts>
               <ts e="T695" id="Seg_8532" n="e" s="T694">его </ts>
               <ts e="T696" id="Seg_8534" n="e" s="T695">еще </ts>
               <ts e="T697" id="Seg_8536" n="e" s="T696">нету </ts>
               <ts e="T698" id="Seg_8538" n="e" s="T697">(с </ts>
               <ts e="T699" id="Seg_8540" n="e" s="T698">этой=) </ts>
               <ts e="T700" id="Seg_8542" n="e" s="T699">с </ts>
               <ts e="T701" id="Seg_8544" n="e" s="T700">конторы. </ts>
            </ts>
            <ts e="T717" id="Seg_8545" n="sc" s="T702">
               <ts e="T703" id="Seg_8547" n="e" s="T702">Я </ts>
               <ts e="T704" id="Seg_8549" n="e" s="T703">сижу, </ts>
               <ts e="T705" id="Seg_8551" n="e" s="T704">он </ts>
               <ts e="T706" id="Seg_8553" n="e" s="T705">заходит: </ts>
               <ts e="T707" id="Seg_8555" n="e" s="T706">"О, </ts>
               <ts e="T708" id="Seg_8557" n="e" s="T707">ты </ts>
               <ts e="T709" id="Seg_8559" n="e" s="T708">тут, </ts>
               <ts e="T710" id="Seg_8561" n="e" s="T709">девка, </ts>
               <ts e="T711" id="Seg_8563" n="e" s="T710">уже", — </ts>
               <ts e="T712" id="Seg_8565" n="e" s="T711">скажет. </ts>
               <ts e="T713" id="Seg_8567" n="e" s="T712">Он </ts>
               <ts e="T714" id="Seg_8569" n="e" s="T713">всё </ts>
               <ts e="T715" id="Seg_8571" n="e" s="T714">меня </ts>
               <ts e="T716" id="Seg_8573" n="e" s="T715">девкой </ts>
               <ts e="T717" id="Seg_8575" n="e" s="T716">звал. </ts>
            </ts>
            <ts e="T720" id="Seg_8576" n="sc" s="T718">
               <ts e="T719" id="Seg_8578" n="e" s="T718">"Тут", - </ts>
               <ts e="T720" id="Seg_8580" n="e" s="T719">говорю. </ts>
            </ts>
            <ts e="T727" id="Seg_8581" n="sc" s="T721">
               <ts e="T722" id="Seg_8583" n="e" s="T721">"Я </ts>
               <ts e="T723" id="Seg_8585" n="e" s="T722">хотел </ts>
               <ts e="T724" id="Seg_8587" n="e" s="T723">послать </ts>
               <ts e="T725" id="Seg_8589" n="e" s="T724">Линку </ts>
               <ts e="T726" id="Seg_8591" n="e" s="T725">за </ts>
               <ts e="T727" id="Seg_8593" n="e" s="T726">тобой". </ts>
            </ts>
            <ts e="T735" id="Seg_8594" n="sc" s="T728">
               <ts e="T729" id="Seg_8596" n="e" s="T728">Ее </ts>
               <ts e="T730" id="Seg_8598" n="e" s="T729">звать </ts>
               <ts e="T731" id="Seg_8600" n="e" s="T730">Лизавета, </ts>
               <ts e="T732" id="Seg_8602" n="e" s="T731">он </ts>
               <ts e="T733" id="Seg_8604" n="e" s="T732">всё </ts>
               <ts e="T734" id="Seg_8606" n="e" s="T733">Линкой </ts>
               <ts e="T735" id="Seg_8608" n="e" s="T734">звал. </ts>
            </ts>
            <ts e="T742" id="Seg_8609" n="sc" s="T736">
               <ts e="T737" id="Seg_8611" n="e" s="T736">А </ts>
               <ts e="T738" id="Seg_8613" n="e" s="T737">ее </ts>
               <ts e="T739" id="Seg_8615" n="e" s="T738">так </ts>
               <ts e="T740" id="Seg_8617" n="e" s="T739">и </ts>
               <ts e="T741" id="Seg_8619" n="e" s="T740">звали, </ts>
               <ts e="T742" id="Seg_8621" n="e" s="T741">Линкой. </ts>
            </ts>
            <ts e="T750" id="Seg_8622" n="sc" s="T743">
               <ts e="T744" id="Seg_8624" n="e" s="T743">Ну, </ts>
               <ts e="T745" id="Seg_8626" n="e" s="T744">мы </ts>
               <ts e="T746" id="Seg_8628" n="e" s="T745">с </ts>
               <ts e="T747" id="Seg_8630" n="e" s="T746">им </ts>
               <ts e="T748" id="Seg_8632" n="e" s="T747">приехали </ts>
               <ts e="T749" id="Seg_8634" n="e" s="T748">к </ts>
               <ts e="T750" id="Seg_8636" n="e" s="T749">конторе. </ts>
            </ts>
            <ts e="T761" id="Seg_8637" n="sc" s="T751">
               <ts e="T752" id="Seg_8639" n="e" s="T751">А </ts>
               <ts e="T753" id="Seg_8641" n="e" s="T752">там </ts>
               <ts e="T754" id="Seg_8643" n="e" s="T753">одна </ts>
               <ts e="T755" id="Seg_8645" n="e" s="T754">женщина </ts>
               <ts e="T756" id="Seg_8647" n="e" s="T755">подбежала, </ts>
               <ts e="T757" id="Seg_8649" n="e" s="T756">говорит: </ts>
               <ts e="T758" id="Seg_8651" n="e" s="T757">"Может, </ts>
               <ts e="T759" id="Seg_8653" n="e" s="T758">и </ts>
               <ts e="T760" id="Seg_8655" n="e" s="T759">меня </ts>
               <ts e="T761" id="Seg_8657" n="e" s="T760">(возьмете)?" </ts>
            </ts>
            <ts e="T771" id="Seg_8658" n="sc" s="T762">
               <ts e="T763" id="Seg_8660" n="e" s="T762">"Нет, </ts>
               <ts e="T764" id="Seg_8662" n="e" s="T763">говорит, </ts>
               <ts e="T765" id="Seg_8664" n="e" s="T764">никого </ts>
               <ts e="T766" id="Seg_8666" n="e" s="T765">не </ts>
               <ts e="T767" id="Seg_8668" n="e" s="T766">возьму", — </ts>
               <ts e="T768" id="Seg_8670" n="e" s="T767">а </ts>
               <ts e="T769" id="Seg_8672" n="e" s="T768">я </ts>
               <ts e="T770" id="Seg_8674" n="e" s="T769">там </ts>
               <ts e="T771" id="Seg_8676" n="e" s="T770">сижу. </ts>
            </ts>
            <ts e="T777" id="Seg_8677" n="sc" s="T772">
               <ts e="T773" id="Seg_8679" n="e" s="T772">Думаю, </ts>
               <ts e="T774" id="Seg_8681" n="e" s="T773">может </ts>
               <ts e="T775" id="Seg_8683" n="e" s="T774">еще </ts>
               <ts e="T776" id="Seg_8685" n="e" s="T775">кто </ts>
               <ts e="T777" id="Seg_8687" n="e" s="T776">поедет. </ts>
            </ts>
            <ts e="T786" id="Seg_8688" n="sc" s="T778">
               <ts e="T779" id="Seg_8690" n="e" s="T778">Взяли </ts>
               <ts e="T780" id="Seg_8692" n="e" s="T779">трое, </ts>
               <ts e="T781" id="Seg_8694" n="e" s="T780">поехали, </ts>
               <ts e="T782" id="Seg_8696" n="e" s="T781">а </ts>
               <ts e="T783" id="Seg_8698" n="e" s="T782">тую </ts>
               <ts e="T784" id="Seg_8700" n="e" s="T783">женщину </ts>
               <ts e="T785" id="Seg_8702" n="e" s="T784">не </ts>
               <ts e="T786" id="Seg_8704" n="e" s="T785">взял. </ts>
            </ts>
            <ts e="T792" id="Seg_8705" n="sc" s="T787">
               <ts e="T788" id="Seg_8707" n="e" s="T787">Как </ts>
               <ts e="T789" id="Seg_8709" n="e" s="T788">я </ts>
               <ts e="T790" id="Seg_8711" n="e" s="T789">скажу, </ts>
               <ts e="T791" id="Seg_8713" n="e" s="T790">обязательно </ts>
               <ts e="T792" id="Seg_8715" n="e" s="T791">возьмет. </ts>
            </ts>
            <ts e="T797" id="Seg_8716" n="sc" s="T793">
               <ts e="T794" id="Seg_8718" n="e" s="T793">Если </ts>
               <ts e="T795" id="Seg_8720" n="e" s="T794">сам </ts>
               <ts e="T796" id="Seg_8722" n="e" s="T795">не </ts>
               <ts e="T797" id="Seg_8724" n="e" s="T796">поедет… </ts>
            </ts>
            <ts e="T806" id="Seg_8725" n="sc" s="T798">
               <ts e="T799" id="Seg_8727" n="e" s="T798">Спрошу, </ts>
               <ts e="T800" id="Seg_8729" n="e" s="T799">сам </ts>
               <ts e="T801" id="Seg_8731" n="e" s="T800">не </ts>
               <ts e="T802" id="Seg_8733" n="e" s="T801">поедет: </ts>
               <ts e="T803" id="Seg_8735" n="e" s="T802">"Ладно, </ts>
               <ts e="T804" id="Seg_8737" n="e" s="T803">я </ts>
               <ts e="T805" id="Seg_8739" n="e" s="T804">скажу </ts>
               <ts e="T806" id="Seg_8741" n="e" s="T805">тебе". </ts>
            </ts>
            <ts e="T818" id="Seg_8742" n="sc" s="T807">
               <ts e="T808" id="Seg_8744" n="e" s="T807">Думаю: </ts>
               <ts e="T809" id="Seg_8746" n="e" s="T808">ну, </ts>
               <ts e="T810" id="Seg_8748" n="e" s="T809">соберусь, </ts>
               <ts e="T811" id="Seg_8750" n="e" s="T810">пойду, </ts>
               <ts e="T812" id="Seg_8752" n="e" s="T811">а </ts>
               <ts e="T813" id="Seg_8754" n="e" s="T812">то, </ts>
               <ts e="T814" id="Seg_8756" n="e" s="T813">говорю, </ts>
               <ts e="T815" id="Seg_8758" n="e" s="T814">как </ts>
               <ts e="T816" id="Seg_8760" n="e" s="T815">он </ts>
               <ts e="T817" id="Seg_8762" n="e" s="T816">будет </ts>
               <ts e="T818" id="Seg_8764" n="e" s="T817">знать. </ts>
            </ts>
            <ts e="T823" id="Seg_8765" n="sc" s="T819">
               <ts e="T820" id="Seg_8767" n="e" s="T819">Забудет - </ts>
               <ts e="T821" id="Seg_8769" n="e" s="T820">народу </ts>
               <ts e="T822" id="Seg_8771" n="e" s="T821">же </ts>
               <ts e="T823" id="Seg_8773" n="e" s="T822">много… </ts>
            </ts>
            <ts e="T834" id="Seg_8774" n="sc" s="T824">
               <ts e="T825" id="Seg_8776" n="e" s="T824">Я </ts>
               <ts e="T826" id="Seg_8778" n="e" s="T825">иду </ts>
               <ts e="T827" id="Seg_8780" n="e" s="T826">(встреча), </ts>
               <ts e="T828" id="Seg_8782" n="e" s="T827">а </ts>
               <ts e="T829" id="Seg_8784" n="e" s="T828">там — </ts>
               <ts e="T830" id="Seg_8786" n="e" s="T829">Ты </ts>
               <ts e="T831" id="Seg_8788" n="e" s="T830">Августа-то </ts>
               <ts e="T832" id="Seg_8790" n="e" s="T831">знаешь, </ts>
               <ts e="T833" id="Seg_8792" n="e" s="T832">(немец) </ts>
               <ts e="T834" id="Seg_8794" n="e" s="T833">наш. </ts>
            </ts>
            <ts e="T838" id="Seg_8795" n="sc" s="T835">
               <ts e="T836" id="Seg_8797" n="e" s="T835">Он </ts>
               <ts e="T837" id="Seg_8799" n="e" s="T836">бригадир, </ts>
               <ts e="T838" id="Seg_8801" n="e" s="T837">Август. </ts>
            </ts>
            <ts e="T854" id="Seg_8802" n="sc" s="T845">
               <ts e="T848" id="Seg_8804" n="e" s="T845">Он </ts>
               <ts e="T851" id="Seg_8806" n="e" s="T848">уехал </ts>
               <ts e="T854" id="Seg_8808" n="e" s="T851">сейчас. </ts>
            </ts>
            <ts e="T859" id="Seg_8809" n="sc" s="T856">
               <ts e="T858" id="Seg_8811" n="e" s="T856">В </ts>
               <ts e="T859" id="Seg_8813" n="e" s="T858">Волгоград. </ts>
            </ts>
            <ts e="T871" id="Seg_8814" n="sc" s="T860">
               <ts e="T861" id="Seg_8816" n="e" s="T860">У </ts>
               <ts e="T862" id="Seg_8818" n="e" s="T861">его </ts>
               <ts e="T863" id="Seg_8820" n="e" s="T862">сын </ts>
               <ts e="T864" id="Seg_8822" n="e" s="T863">женился, </ts>
               <ts e="T865" id="Seg_8824" n="e" s="T864">тоже </ts>
               <ts e="T866" id="Seg_8826" n="e" s="T865">на </ts>
               <ts e="T867" id="Seg_8828" n="e" s="T866">волгоградской, </ts>
               <ts e="T868" id="Seg_8830" n="e" s="T867">и </ts>
               <ts e="T869" id="Seg_8832" n="e" s="T868">уехал </ts>
               <ts e="T870" id="Seg_8834" n="e" s="T869">в </ts>
               <ts e="T871" id="Seg_8836" n="e" s="T870">Волгоград. </ts>
            </ts>
            <ts e="T878" id="Seg_8837" n="sc" s="T872">
               <ts e="T873" id="Seg_8839" n="e" s="T872">Там </ts>
               <ts e="T874" id="Seg_8841" n="e" s="T873">у </ts>
               <ts e="T875" id="Seg_8843" n="e" s="T874">нас </ts>
               <ts e="T876" id="Seg_8845" n="e" s="T875">всякие </ts>
               <ts e="T877" id="Seg_8847" n="e" s="T876">теперь </ts>
               <ts e="T878" id="Seg_8849" n="e" s="T877">люди. </ts>
            </ts>
            <ts e="T888" id="Seg_8850" n="sc" s="T879">
               <ts e="T880" id="Seg_8852" n="e" s="T879">Вот </ts>
               <ts e="T881" id="Seg_8854" n="e" s="T880">после </ts>
               <ts e="T882" id="Seg_8856" n="e" s="T881">тебя </ts>
               <ts e="T883" id="Seg_8858" n="e" s="T882">сюды </ts>
               <ts e="T884" id="Seg_8860" n="e" s="T883">к </ts>
               <ts e="T885" id="Seg_8862" n="e" s="T884">Малиновке </ts>
               <ts e="T886" id="Seg_8864" n="e" s="T885">целая </ts>
               <ts e="T887" id="Seg_8866" n="e" s="T886">деревня </ts>
               <ts e="T888" id="Seg_8868" n="e" s="T887">настроилась. </ts>
            </ts>
            <ts e="T905" id="Seg_8869" n="sc" s="T889">
               <ts e="T890" id="Seg_8871" n="e" s="T889">И </ts>
               <ts e="T891" id="Seg_8873" n="e" s="T890">(сю- </ts>
               <ts e="T892" id="Seg_8875" n="e" s="T891">сюды </ts>
               <ts e="T893" id="Seg_8877" n="e" s="T892">к </ts>
               <ts e="T894" id="Seg_8879" n="e" s="T893">М-) </ts>
               <ts e="T895" id="Seg_8881" n="e" s="T894">сюды </ts>
               <ts e="T896" id="Seg_8883" n="e" s="T895">вот </ts>
               <ts e="T897" id="Seg_8885" n="e" s="T896">на </ts>
               <ts e="T898" id="Seg_8887" n="e" s="T897">Пермяково, </ts>
               <ts e="T899" id="Seg_8889" n="e" s="T898">сюды </ts>
               <ts e="T900" id="Seg_8891" n="e" s="T899">к </ts>
               <ts e="T901" id="Seg_8893" n="e" s="T900">Малиновке </ts>
               <ts e="T902" id="Seg_8895" n="e" s="T901">чуть </ts>
               <ts e="T903" id="Seg_8897" n="e" s="T902">не </ts>
               <ts e="T904" id="Seg_8899" n="e" s="T903">до </ts>
               <ts e="T905" id="Seg_8901" n="e" s="T904">кладбища. </ts>
            </ts>
            <ts e="T915" id="Seg_8902" n="sc" s="T909">
               <ts e="T910" id="Seg_8904" n="e" s="T909">(И </ts>
               <ts e="T912" id="Seg_8906" n="e" s="T910">во-) </ts>
               <ts e="T913" id="Seg_8908" n="e" s="T912">И </ts>
               <ts e="T914" id="Seg_8910" n="e" s="T913">вот </ts>
               <ts e="T915" id="Seg_8912" n="e" s="T914">это… </ts>
            </ts>
            <ts e="T918" id="Seg_8913" n="sc" s="T916">
               <ts e="T917" id="Seg_8915" n="e" s="T916">Опять </ts>
               <ts e="T918" id="Seg_8917" n="e" s="T917">((…)). </ts>
            </ts>
            <ts e="T933" id="Seg_8918" n="sc" s="T929">
               <ts e="T933" id="Seg_8920" n="e" s="T929">Загороднюка? </ts>
            </ts>
            <ts e="T954" id="Seg_8921" n="sc" s="T949">
               <ts e="T951" id="Seg_8923" n="e" s="T949">Ну, </ts>
               <ts e="T954" id="Seg_8925" n="e" s="T951">пусти. </ts>
            </ts>
            <ts e="T959" id="Seg_8926" n="sc" s="T957">
               <ts e="T959" id="Seg_8928" n="e" s="T957">Записаны? </ts>
            </ts>
            <ts e="T977" id="Seg_8929" n="sc" s="T969">
               <ts e="T970" id="Seg_8931" n="e" s="T969">(Каmen=) </ts>
               <ts e="T971" id="Seg_8933" n="e" s="T970">miʔ </ts>
               <ts e="T972" id="Seg_8935" n="e" s="T971">külambi </ts>
               <ts e="T973" id="Seg_8937" n="e" s="T972">Zagorodnʼuk, </ts>
               <ts e="T974" id="Seg_8939" n="e" s="T973">uže </ts>
               <ts e="T975" id="Seg_8941" n="e" s="T974">(sĭ-) </ts>
               <ts e="T976" id="Seg_8943" n="e" s="T975">šide </ts>
               <ts e="T977" id="Seg_8945" n="e" s="T976">kö. </ts>
            </ts>
            <ts e="T986" id="Seg_8946" n="sc" s="T978">
               <ts e="T979" id="Seg_8948" n="e" s="T978">Kamen </ts>
               <ts e="T980" id="Seg_8950" n="e" s="T979">măn </ts>
               <ts e="T981" id="Seg_8952" n="e" s="T980">gibər </ts>
               <ts e="T982" id="Seg_8954" n="e" s="T981">kalam, </ts>
               <ts e="T983" id="Seg_8956" n="e" s="T982">mănlăm </ts>
               <ts e="T984" id="Seg_8958" n="e" s="T983">dĭʔnə: </ts>
               <ts e="T985" id="Seg_8960" n="e" s="T984">"(I-) </ts>
               <ts e="T986" id="Seg_8962" n="e" s="T985">Kalal?" </ts>
            </ts>
            <ts e="T989" id="Seg_8963" n="sc" s="T987">
               <ts e="T988" id="Seg_8965" n="e" s="T987">"Nu, </ts>
               <ts e="T989" id="Seg_8967" n="e" s="T988">šoʔ". </ts>
            </ts>
            <ts e="T993" id="Seg_8968" n="sc" s="T990">
               <ts e="T991" id="Seg_8970" n="e" s="T990">(Măn=) </ts>
               <ts e="T992" id="Seg_8972" n="e" s="T991">Măn </ts>
               <ts e="T993" id="Seg_8974" n="e" s="T992">šobiam. </ts>
            </ts>
            <ts e="T999" id="Seg_8975" n="sc" s="T994">
               <ts e="T995" id="Seg_8977" n="e" s="T994">Dĭgəttə </ts>
               <ts e="T996" id="Seg_8979" n="e" s="T995">dĭ </ts>
               <ts e="T997" id="Seg_8981" n="e" s="T996">măndə:" </ts>
               <ts e="T998" id="Seg_8983" n="e" s="T997">"Tăn </ts>
               <ts e="T999" id="Seg_8985" n="e" s="T998">šobial. </ts>
            </ts>
            <ts e="T1007" id="Seg_8986" n="sc" s="T1000">
               <ts e="T1001" id="Seg_8988" n="e" s="T1000">A </ts>
               <ts e="T1002" id="Seg_8990" n="e" s="T1001">măn </ts>
               <ts e="T1003" id="Seg_8992" n="e" s="T1002">öʔlusʼtə </ts>
               <ts e="T1004" id="Seg_8994" n="e" s="T1003">xatʼel </ts>
               <ts e="T1005" id="Seg_8996" n="e" s="T1004">bostə </ts>
               <ts e="T1006" id="Seg_8998" n="e" s="T1005">nen </ts>
               <ts e="T1007" id="Seg_9000" n="e" s="T1006">tănzi". </ts>
            </ts>
            <ts e="T1012" id="Seg_9001" n="sc" s="T1008">
               <ts e="T1009" id="Seg_9003" n="e" s="T1008">Dĭgəttə </ts>
               <ts e="T1010" id="Seg_9005" n="e" s="T1009">amnolbibaʔ, </ts>
               <ts e="T1011" id="Seg_9007" n="e" s="T1010">kambibaʔ </ts>
               <ts e="T1012" id="Seg_9009" n="e" s="T1011">kăntoranə. </ts>
            </ts>
            <ts e="T1027" id="Seg_9010" n="sc" s="T1013">
               <ts e="T1014" id="Seg_9012" n="e" s="T1013">Dĭ </ts>
               <ts e="T1015" id="Seg_9014" n="e" s="T1014">(другой=) </ts>
               <ts e="T1016" id="Seg_9016" n="e" s="T1015">baška </ts>
               <ts e="T1017" id="Seg_9018" n="e" s="T1016">nüke </ts>
               <ts e="T1018" id="Seg_9020" n="e" s="T1017">šobi: </ts>
               <ts e="T1019" id="Seg_9022" n="e" s="T1018">"Măna </ts>
               <ts e="T1020" id="Seg_9024" n="e" s="T1019">iləl?" </ts>
               <ts e="T1021" id="Seg_9026" n="e" s="T1020">(Măn=) </ts>
               <ts e="T1022" id="Seg_9028" n="e" s="T1021">Dĭ </ts>
               <ts e="T1023" id="Seg_9030" n="e" s="T1022">mămbi: </ts>
               <ts e="T1024" id="Seg_9032" n="e" s="T1023">"Šindimdə </ts>
               <ts e="T1025" id="Seg_9034" n="e" s="T1024">(em=) </ts>
               <ts e="T1026" id="Seg_9036" n="e" s="T1025">ej </ts>
               <ts e="T1027" id="Seg_9038" n="e" s="T1026">iləm". </ts>
            </ts>
            <ts e="T1033" id="Seg_9039" n="sc" s="T1028">
               <ts e="T1029" id="Seg_9041" n="e" s="T1028">Măn </ts>
               <ts e="T1030" id="Seg_9043" n="e" s="T1029">tenəbiem: </ts>
               <ts e="T1031" id="Seg_9045" n="e" s="T1030">išo </ts>
               <ts e="T1032" id="Seg_9047" n="e" s="T1031">šində </ts>
               <ts e="T1033" id="Seg_9049" n="e" s="T1032">kaləj. </ts>
            </ts>
            <ts e="T1037" id="Seg_9050" n="sc" s="T1034">
               <ts e="T1035" id="Seg_9052" n="e" s="T1034">Dĭgəttə </ts>
               <ts e="T1036" id="Seg_9054" n="e" s="T1035">nagurgöʔ </ts>
               <ts e="T1037" id="Seg_9056" n="e" s="T1036">kambibaʔ. </ts>
            </ts>
            <ts e="T1041" id="Seg_9057" n="sc" s="T1038">
               <ts e="T1039" id="Seg_9059" n="e" s="T1038">Ну, </ts>
               <ts e="T1041" id="Seg_9061" n="e" s="T1039">всё. </ts>
            </ts>
            <ts e="T1064" id="Seg_9062" n="sc" s="T1061">
               <ts e="T1064" id="Seg_9064" n="e" s="T1061">Сказала! </ts>
            </ts>
            <ts e="T1084" id="Seg_9065" n="sc" s="T1073">
               <ts e="T1074" id="Seg_9067" n="e" s="T1073">Dĭgəttə </ts>
               <ts e="T1075" id="Seg_9069" n="e" s="T1074">dĭʔnə </ts>
               <ts e="T1076" id="Seg_9071" n="e" s="T1075">măn </ts>
               <ts e="T1077" id="Seg_9073" n="e" s="T1076">mămbial </ts>
               <ts e="T1078" id="Seg_9075" n="e" s="T1077">(măn=) </ts>
               <ts e="T1079" id="Seg_9077" n="e" s="T1078">"Giber </ts>
               <ts e="T1080" id="Seg_9079" n="e" s="T1079">kandəgal?" </ts>
               <ts e="T1081" id="Seg_9081" n="e" s="T1080">Măn </ts>
               <ts e="T1082" id="Seg_9083" n="e" s="T1081">măndəm: </ts>
               <ts e="T1083" id="Seg_9085" n="e" s="T1082">"Kudajdə </ts>
               <ts e="T1084" id="Seg_9087" n="e" s="T1083">üzəsʼtə". </ts>
            </ts>
            <ts e="T1089" id="Seg_9088" n="sc" s="T1085">
               <ts e="T1086" id="Seg_9090" n="e" s="T1085">"Nu, </ts>
               <ts e="T1087" id="Seg_9092" n="e" s="T1086">(karəlʼdʼan) </ts>
               <ts e="T1088" id="Seg_9094" n="e" s="T1087">mănzi </ts>
               <ts e="T1089" id="Seg_9096" n="e" s="T1088">kalal". </ts>
            </ts>
            <ts e="T1093" id="Seg_9097" n="sc" s="T1090">
               <ts e="T1091" id="Seg_9099" n="e" s="T1090">Dăre </ts>
               <ts e="T1092" id="Seg_9101" n="e" s="T1091">nörbəbi </ts>
               <ts e="T1093" id="Seg_9103" n="e" s="T1092">măna. </ts>
            </ts>
            <ts e="T1100" id="Seg_9104" n="sc" s="T1094">
               <ts e="T1095" id="Seg_9106" n="e" s="T1094">А </ts>
               <ts e="T1096" id="Seg_9108" n="e" s="T1095">так </ts>
               <ts e="T1097" id="Seg_9110" n="e" s="T1096">я </ts>
               <ts e="T1098" id="Seg_9112" n="e" s="T1097">тебе </ts>
               <ts e="T1099" id="Seg_9114" n="e" s="T1098">рассказала </ts>
               <ts e="T1100" id="Seg_9116" n="e" s="T1099">всё. </ts>
            </ts>
            <ts e="T1138" id="Seg_9117" n="sc" s="T1133">
               <ts e="T1136" id="Seg_9119" n="e" s="T1133">Говорила, </ts>
               <ts e="T1138" id="Seg_9121" n="e" s="T1136">говорила. </ts>
            </ts>
            <ts e="T1141" id="Seg_9122" n="sc" s="T1139">
               <ts e="T1140" id="Seg_9124" n="e" s="T1139">Šide </ts>
               <ts e="T1141" id="Seg_9126" n="e" s="T1140">kö. </ts>
            </ts>
            <ts e="T1148" id="Seg_9127" n="sc" s="T1142">
               <ts e="T1143" id="Seg_9129" n="e" s="T1142">Два </ts>
               <ts e="T1144" id="Seg_9131" n="e" s="T1143">года, </ts>
               <ts e="T1146" id="Seg_9133" n="e" s="T1144">šide </ts>
               <ts e="T1148" id="Seg_9135" n="e" s="T1146">kö. </ts>
            </ts>
            <ts e="T1181" id="Seg_9136" n="sc" s="T1177">
               <ts e="T1178" id="Seg_9138" n="e" s="T1177">Miʔ </ts>
               <ts e="T1179" id="Seg_9140" n="e" s="T1178">brigadir </ts>
               <ts e="T1180" id="Seg_9142" n="e" s="T1179">ibi, </ts>
               <ts e="T1181" id="Seg_9144" n="e" s="T1180">nʼemec. </ts>
            </ts>
            <ts e="T1188" id="Seg_9145" n="sc" s="T1182">
               <ts e="T1183" id="Seg_9147" n="e" s="T1182">Dĭ </ts>
               <ts e="T1184" id="Seg_9149" n="e" s="T1183">ugandə </ts>
               <ts e="T1185" id="Seg_9151" n="e" s="T1184">(il-) </ts>
               <ts e="T1186" id="Seg_9153" n="e" s="T1185">ildə </ts>
               <ts e="T1187" id="Seg_9155" n="e" s="T1186">ineʔi </ts>
               <ts e="T1188" id="Seg_9157" n="e" s="T1187">mĭbi. </ts>
            </ts>
            <ts e="T1193" id="Seg_9158" n="sc" s="T1189">
               <ts e="T1190" id="Seg_9160" n="e" s="T1189">Tüj </ts>
               <ts e="T1191" id="Seg_9162" n="e" s="T1190">dĭ </ts>
               <ts e="T1739" id="Seg_9164" n="e" s="T1191">kalla </ts>
               <ts e="T1192" id="Seg_9166" n="e" s="T1739">dʼürbi </ts>
               <ts e="T1193" id="Seg_9168" n="e" s="T1192">Vălgăgrattə. </ts>
            </ts>
            <ts e="T1206" id="Seg_9169" n="sc" s="T1194">
               <ts e="T1195" id="Seg_9171" n="e" s="T1194">A </ts>
               <ts e="T1196" id="Seg_9173" n="e" s="T1195">kamen </ts>
               <ts e="T1197" id="Seg_9175" n="e" s="T1196">šobi </ts>
               <ts e="T1198" id="Seg_9177" n="e" s="T1197">dĭ </ts>
               <ts e="T1199" id="Seg_9179" n="e" s="T1198">baška </ts>
               <ts e="T1200" id="Seg_9181" n="e" s="T1199">predsʼedatʼelʼ </ts>
               <ts e="T1201" id="Seg_9183" n="e" s="T1200">miʔnʼibeʔ, </ts>
               <ts e="T1202" id="Seg_9185" n="e" s="T1201">dĭ </ts>
               <ts e="T1203" id="Seg_9187" n="e" s="T1202">ildə </ts>
               <ts e="T1204" id="Seg_9189" n="e" s="T1203">ine </ts>
               <ts e="T1205" id="Seg_9191" n="e" s="T1204">(mĭbiʔi=) </ts>
               <ts e="T1206" id="Seg_9193" n="e" s="T1205">mĭbi. </ts>
            </ts>
            <ts e="T1214" id="Seg_9194" n="sc" s="T1207">
               <ts e="T1208" id="Seg_9196" n="e" s="T1207">(Šindimdə </ts>
               <ts e="T1209" id="Seg_9198" n="e" s="T1208">ej=) </ts>
               <ts e="T1210" id="Seg_9200" n="e" s="T1209">(Keʔbdejleʔ) </ts>
               <ts e="T1211" id="Seg_9202" n="e" s="T1210">kambiʔi </ts>
               <ts e="T1212" id="Seg_9204" n="e" s="T1211">inezi </ts>
               <ts e="T1213" id="Seg_9206" n="e" s="T1212">mĭbi </ts>
               <ts e="T1214" id="Seg_9208" n="e" s="T1213">tože. </ts>
            </ts>
            <ts e="T1223" id="Seg_9209" n="sc" s="T1215">
               <ts e="T1216" id="Seg_9211" n="e" s="T1215">Dĭ </ts>
               <ts e="T1217" id="Seg_9213" n="e" s="T1216">predsʼedatʼelʼ </ts>
               <ts e="T1218" id="Seg_9215" n="e" s="T1217">tüj </ts>
               <ts e="T1219" id="Seg_9217" n="e" s="T1218">miʔnʼibeʔ </ts>
               <ts e="T1220" id="Seg_9219" n="e" s="T1219">Văznʼesenkanə </ts>
               <ts e="T1221" id="Seg_9221" n="e" s="T1220">kambi, </ts>
               <ts e="T1222" id="Seg_9223" n="e" s="T1221">dĭn </ts>
               <ts e="T1223" id="Seg_9225" n="e" s="T1222">amnolaʔbə. </ts>
            </ts>
            <ts e="T1235" id="Seg_9226" n="sc" s="T1224">
               <ts e="T1225" id="Seg_9228" n="e" s="T1224">Dĭʔnə </ts>
               <ts e="T1226" id="Seg_9230" n="e" s="T1225">tura </ts>
               <ts e="T1227" id="Seg_9232" n="e" s="T1226">nuldəbiʔi. </ts>
               <ts e="T1228" id="Seg_9234" n="e" s="T1227">Turat </ts>
               <ts e="T1229" id="Seg_9236" n="e" s="T1228">tüj </ts>
               <ts e="T1230" id="Seg_9238" n="e" s="T1229">nulaʔbə, </ts>
               <ts e="T1231" id="Seg_9240" n="e" s="T1230">šindidə </ts>
               <ts e="T1232" id="Seg_9242" n="e" s="T1231">ej </ts>
               <ts e="T1233" id="Seg_9244" n="e" s="T1232">amnolia </ts>
               <ts e="T1234" id="Seg_9246" n="e" s="T1233">dĭ </ts>
               <ts e="T1235" id="Seg_9248" n="e" s="T1234">turagən. </ts>
            </ts>
            <ts e="T1237" id="Seg_9249" n="sc" s="T1236">
               <ts e="T1237" id="Seg_9251" n="e" s="T1236">Kabarləj. </ts>
            </ts>
            <ts e="T1264" id="Seg_9252" n="sc" s="T1248">
               <ts e="T1249" id="Seg_9254" n="e" s="T1248">Председатель </ts>
               <ts e="T1250" id="Seg_9256" n="e" s="T1249">у </ts>
               <ts e="T1251" id="Seg_9258" n="e" s="T1250">нас </ts>
               <ts e="T1252" id="Seg_9260" n="e" s="T1251">хороший, </ts>
               <ts e="T1253" id="Seg_9262" n="e" s="T1252">(л-) </ts>
               <ts e="T1254" id="Seg_9264" n="e" s="T1253">с </ts>
               <ts e="T1255" id="Seg_9266" n="e" s="T1254">людям </ts>
               <ts e="T1256" id="Seg_9268" n="e" s="T1255">хорошо </ts>
               <ts e="T1257" id="Seg_9270" n="e" s="T1256">обходился. </ts>
               <ts e="T1258" id="Seg_9272" n="e" s="T1257">Коней </ts>
               <ts e="T1259" id="Seg_9274" n="e" s="T1258">давал, </ts>
               <ts e="T1260" id="Seg_9276" n="e" s="T1259">даже </ts>
               <ts e="T1261" id="Seg_9278" n="e" s="T1260">по </ts>
               <ts e="T1262" id="Seg_9280" n="e" s="T1261">ягоды </ts>
               <ts e="T1263" id="Seg_9282" n="e" s="T1262">ездили </ts>
               <ts e="T1264" id="Seg_9284" n="e" s="T1263">люди. </ts>
            </ts>
            <ts e="T1271" id="Seg_9285" n="sc" s="T1265">
               <ts e="T1266" id="Seg_9287" n="e" s="T1265">А </ts>
               <ts e="T1267" id="Seg_9289" n="e" s="T1266">теперь </ts>
               <ts e="T1268" id="Seg_9291" n="e" s="T1267">нас </ts>
               <ts e="T1269" id="Seg_9293" n="e" s="T1268">соединили </ts>
               <ts e="T1270" id="Seg_9295" n="e" s="T1269">с </ts>
               <ts e="T1271" id="Seg_9297" n="e" s="T1270">Вознесенки. </ts>
            </ts>
            <ts e="T1278" id="Seg_9298" n="sc" s="T1272">
               <ts e="T1273" id="Seg_9300" n="e" s="T1272">Ему </ts>
               <ts e="T1274" id="Seg_9302" n="e" s="T1273">выстроили </ts>
               <ts e="T1275" id="Seg_9304" n="e" s="T1274">тут </ts>
               <ts e="T1276" id="Seg_9306" n="e" s="T1275">дом </ts>
               <ts e="T1277" id="Seg_9308" n="e" s="T1276">в </ts>
               <ts e="T1278" id="Seg_9310" n="e" s="T1277">Абалаковой. </ts>
            </ts>
            <ts e="T1287" id="Seg_9311" n="sc" s="T1279">
               <ts e="T1280" id="Seg_9313" n="e" s="T1279">(И=) </ts>
               <ts e="T1281" id="Seg_9315" n="e" s="T1280">А </ts>
               <ts e="T1282" id="Seg_9317" n="e" s="T1281">теперь </ts>
               <ts e="T1283" id="Seg_9319" n="e" s="T1282">в </ts>
               <ts e="T1284" id="Seg_9321" n="e" s="T1283">Вознесенку </ts>
               <ts e="T1285" id="Seg_9323" n="e" s="T1284">уехал, </ts>
               <ts e="T1286" id="Seg_9325" n="e" s="T1285">там </ts>
               <ts e="T1287" id="Seg_9327" n="e" s="T1286">живет. </ts>
            </ts>
            <ts e="T1344" id="Seg_9328" n="sc" s="T1341">
               <ts e="T1344" id="Seg_9330" n="e" s="T1341">И сейчас есть такое. </ts>
            </ts>
            <ts e="T1353" id="Seg_9331" n="sc" s="T1347">
               <ts e="T1348" id="Seg_9333" n="e" s="T1347">Продают </ts>
               <ts e="T1349" id="Seg_9335" n="e" s="T1348">прямо </ts>
               <ts e="T1350" id="Seg_9337" n="e" s="T1349">поллитром </ts>
               <ts e="T1351" id="Seg_9339" n="e" s="T1350">такое </ts>
               <ts e="T1352" id="Seg_9341" n="e" s="T1351">(крепкое) </ts>
               <ts e="T1353" id="Seg_9343" n="e" s="T1352">вино. </ts>
            </ts>
            <ts e="T1355" id="Seg_9344" n="sc" s="T1354">
               <ts e="T1355" id="Seg_9346" n="e" s="T1354">Спирт! </ts>
            </ts>
            <ts e="T1375" id="Seg_9347" n="sc" s="T1356">
               <ts e="T1357" id="Seg_9349" n="e" s="T1356">Ну, </ts>
               <ts e="T1358" id="Seg_9351" n="e" s="T1357">он </ts>
               <ts e="T1359" id="Seg_9353" n="e" s="T1358">так ((COUGH)), </ts>
               <ts e="T1360" id="Seg_9355" n="e" s="T1359">для </ts>
               <ts e="T1361" id="Seg_9357" n="e" s="T1360">здоровья </ts>
               <ts e="T1362" id="Seg_9359" n="e" s="T1361">вот </ts>
               <ts e="T1363" id="Seg_9361" n="e" s="T1362">столь </ts>
               <ts e="T1364" id="Seg_9363" n="e" s="T1363">выпьем, </ts>
               <ts e="T1365" id="Seg_9365" n="e" s="T1364">лучше </ts>
               <ts e="T1366" id="Seg_9367" n="e" s="T1365">как-то </ts>
               <ts e="T1367" id="Seg_9369" n="e" s="T1366">(как=) </ts>
               <ts e="T1368" id="Seg_9371" n="e" s="T1367">как </ts>
               <ts e="T1369" id="Seg_9373" n="e" s="T1368">это </ts>
               <ts e="T1370" id="Seg_9375" n="e" s="T1369">вот </ts>
               <ts e="T1371" id="Seg_9377" n="e" s="T1370">(са-) </ts>
               <ts e="T1372" id="Seg_9379" n="e" s="T1371">самогонка. </ts>
               <ts e="T1373" id="Seg_9381" n="e" s="T1372">Он </ts>
               <ts e="T1374" id="Seg_9383" n="e" s="T1373">чистый </ts>
               <ts e="T1375" id="Seg_9385" n="e" s="T1374">ведь. </ts>
            </ts>
            <ts e="T1377" id="Seg_9386" n="sc" s="T1376">
               <ts e="T1377" id="Seg_9388" n="e" s="T1376">Вот. </ts>
            </ts>
            <ts e="T1388" id="Seg_9389" n="sc" s="T1380">
               <ts e="T1382" id="Seg_9391" n="e" s="T1380">А </ts>
               <ts e="T1383" id="Seg_9393" n="e" s="T1382">ты, </ts>
               <ts e="T1384" id="Seg_9395" n="e" s="T1383">тот </ts>
               <ts e="T1385" id="Seg_9397" n="e" s="T1384">магазин </ts>
               <ts e="T1386" id="Seg_9399" n="e" s="T1385">у </ts>
               <ts e="T1387" id="Seg_9401" n="e" s="T1386">нас </ts>
               <ts e="T1388" id="Seg_9403" n="e" s="T1387">теперь… </ts>
            </ts>
            <ts e="T1404" id="Seg_9404" n="sc" s="T1389">
               <ts e="T1390" id="Seg_9406" n="e" s="T1389">Ты </ts>
               <ts e="T1391" id="Seg_9408" n="e" s="T1390">в </ts>
               <ts e="T1392" id="Seg_9410" n="e" s="T1391">этим </ts>
               <ts e="T1393" id="Seg_9412" n="e" s="T1392">был </ts>
               <ts e="T1394" id="Seg_9414" n="e" s="T1393">магазине? </ts>
               <ts e="T1395" id="Seg_9416" n="e" s="T1394">У </ts>
               <ts e="T1396" id="Seg_9418" n="e" s="T1395">нас </ts>
               <ts e="T1397" id="Seg_9420" n="e" s="T1396">теперь </ts>
               <ts e="T1398" id="Seg_9422" n="e" s="T1397">другой, </ts>
               <ts e="T1399" id="Seg_9424" n="e" s="T1398">на </ts>
               <ts e="T1400" id="Seg_9426" n="e" s="T1399">том </ts>
               <ts e="T1401" id="Seg_9428" n="e" s="T1400">бугре, </ts>
               <ts e="T1402" id="Seg_9430" n="e" s="T1401">ты </ts>
               <ts e="T1403" id="Seg_9432" n="e" s="T1402">его </ts>
               <ts e="T1404" id="Seg_9434" n="e" s="T1403">знаешь? </ts>
            </ts>
            <ts e="T1434" id="Seg_9435" n="sc" s="T1433">
               <ts e="T1434" id="Seg_9437" n="e" s="T1433">Вот… </ts>
            </ts>
            <ts e="T1466" id="Seg_9438" n="sc" s="T1465">
               <ts e="T1466" id="Seg_9440" n="e" s="T1465">Погоди… </ts>
            </ts>
            <ts e="T1470" id="Seg_9441" n="sc" s="T1467">
               <ts e="T1468" id="Seg_9443" n="e" s="T1467">Ну </ts>
               <ts e="T1469" id="Seg_9445" n="e" s="T1468">тут </ts>
               <ts e="T1470" id="Seg_9447" n="e" s="T1469">это… </ts>
            </ts>
            <ts e="T1487" id="Seg_9448" n="sc" s="T1471">
               <ts e="T1484" id="Seg_9450" n="e" s="T1471">Как идешь, еще лог не переходишь, вот здесь магазин был, </ts>
               <ts e="T1486" id="Seg_9452" n="e" s="T1484">а тут </ts>
               <ts e="T1487" id="Seg_9454" n="e" s="T1486">лог. </ts>
            </ts>
            <ts e="T1499" id="Seg_9455" n="sc" s="T1488">
               <ts e="T1489" id="Seg_9457" n="e" s="T1488">А </ts>
               <ts e="T1491" id="Seg_9459" n="e" s="T1489">теперь </ts>
               <ts e="T1492" id="Seg_9461" n="e" s="T1491">за логом, </ts>
               <ts e="T1493" id="Seg_9463" n="e" s="T1492">((PAUSE)) </ts>
               <ts e="T1495" id="Seg_9465" n="e" s="T1493">тут это где </ts>
               <ts e="T1499" id="Seg_9467" n="e" s="T1495">моя родимая изба была. </ts>
            </ts>
            <ts e="T1518" id="Seg_9468" n="sc" s="T1502">
               <ts e="T1504" id="Seg_9470" n="e" s="T1502">Ну, </ts>
               <ts e="T1506" id="Seg_9472" n="e" s="T1504">а </ts>
               <ts e="T1509" id="Seg_9474" n="e" s="T1506">как </ts>
               <ts e="T1511" id="Seg_9476" n="e" s="T1509">там </ts>
               <ts e="T1514" id="Seg_9478" n="e" s="T1511">она </ts>
               <ts e="T1515" id="Seg_9480" n="e" s="T1514">сгорела, </ts>
               <ts e="T1516" id="Seg_9482" n="e" s="T1515">не </ts>
               <ts e="T1517" id="Seg_9484" n="e" s="T1516">при </ts>
               <ts e="T1518" id="Seg_9486" n="e" s="T1517">тебе? </ts>
            </ts>
            <ts e="T1530" id="Seg_9487" n="sc" s="T1522">
               <ts e="T1523" id="Seg_9489" n="e" s="T1522">Сгорела, </ts>
               <ts e="T1524" id="Seg_9491" n="e" s="T1523">а </ts>
               <ts e="T1525" id="Seg_9493" n="e" s="T1524">теперь </ts>
               <ts e="T1526" id="Seg_9495" n="e" s="T1525">на </ts>
               <ts e="T1527" id="Seg_9497" n="e" s="T1526">этим </ts>
               <ts e="T1528" id="Seg_9499" n="e" s="T1527">месте </ts>
               <ts e="T1529" id="Seg_9501" n="e" s="T1528">поставили </ts>
               <ts e="T1530" id="Seg_9503" n="e" s="T1529">магазин. </ts>
            </ts>
            <ts e="T1533" id="Seg_9504" n="sc" s="T1531">
               <ts e="T1532" id="Seg_9506" n="e" s="T1531">Большой, </ts>
               <ts e="T1533" id="Seg_9508" n="e" s="T1532">обширный. </ts>
            </ts>
            <ts e="T1540" id="Seg_9509" n="sc" s="T1534">
               <ts e="T1535" id="Seg_9511" n="e" s="T1534">Вот </ts>
               <ts e="T1536" id="Seg_9513" n="e" s="T1535">больше, </ts>
               <ts e="T1537" id="Seg_9515" n="e" s="T1536">пожалуй, </ts>
               <ts e="T1538" id="Seg_9517" n="e" s="T1537">этого </ts>
               <ts e="T1539" id="Seg_9519" n="e" s="T1538">дома </ts>
               <ts e="T1540" id="Seg_9521" n="e" s="T1539">будет. </ts>
            </ts>
            <ts e="T1544" id="Seg_9522" n="sc" s="T1542">
               <ts e="T1544" id="Seg_9524" n="e" s="T1542">Там (кирпи-)… </ts>
            </ts>
            <ts e="T1555" id="Seg_9525" n="sc" s="T1545">
               <ts e="T1547" id="Seg_9527" n="e" s="T1545">Нет, </ts>
               <ts e="T1549" id="Seg_9529" n="e" s="T1547">это </ts>
               <ts e="T1551" id="Seg_9531" n="e" s="T1549">(деревя-) </ts>
               <ts e="T1552" id="Seg_9533" n="e" s="T1551">деревянное, </ts>
               <ts e="T1553" id="Seg_9535" n="e" s="T1552">но </ts>
               <ts e="T1554" id="Seg_9537" n="e" s="T1553">хорошее, </ts>
               <ts e="T1555" id="Seg_9539" n="e" s="T1554">большое. </ts>
            </ts>
            <ts e="T1565" id="Seg_9540" n="sc" s="T1556">
               <ts e="T1557" id="Seg_9542" n="e" s="T1556">Так </ts>
               <ts e="T1558" id="Seg_9544" n="e" s="T1557">тут </ts>
               <ts e="T1559" id="Seg_9546" n="e" s="T1558">зайдешь, </ts>
               <ts e="T1560" id="Seg_9548" n="e" s="T1559">всё </ts>
               <ts e="T1561" id="Seg_9550" n="e" s="T1560">тут </ts>
               <ts e="T1562" id="Seg_9552" n="e" s="T1561">полки </ts>
               <ts e="T1563" id="Seg_9554" n="e" s="T1562">закладено, </ts>
               <ts e="T1564" id="Seg_9556" n="e" s="T1563">тут </ts>
               <ts e="T1565" id="Seg_9558" n="e" s="T1564">и… </ts>
            </ts>
            <ts e="T1577" id="Seg_9559" n="sc" s="T1566">
               <ts e="T1567" id="Seg_9561" n="e" s="T1566">И </ts>
               <ts e="T1568" id="Seg_9563" n="e" s="T1567">потом </ts>
               <ts e="T1569" id="Seg_9565" n="e" s="T1568">тут </ts>
               <ts e="T1570" id="Seg_9567" n="e" s="T1569">так </ts>
               <ts e="T1571" id="Seg_9569" n="e" s="T1570">выкладена </ts>
               <ts e="T1572" id="Seg_9571" n="e" s="T1571">така, </ts>
               <ts e="T1573" id="Seg_9573" n="e" s="T1572">как </ts>
               <ts e="T1574" id="Seg_9575" n="e" s="T1573">вроде </ts>
               <ts e="T1575" id="Seg_9577" n="e" s="T1574">вот </ts>
               <ts e="T1576" id="Seg_9579" n="e" s="T1575">печи </ts>
               <ts e="T1577" id="Seg_9581" n="e" s="T1576">такой. </ts>
            </ts>
            <ts e="T1584" id="Seg_9582" n="sc" s="T1578">
               <ts e="T1579" id="Seg_9584" n="e" s="T1578">А </ts>
               <ts e="T1580" id="Seg_9586" n="e" s="T1579">потом </ts>
               <ts e="T1581" id="Seg_9588" n="e" s="T1580">чего-то </ts>
               <ts e="T1582" id="Seg_9590" n="e" s="T1581">там </ts>
               <ts e="T1583" id="Seg_9592" n="e" s="T1582">загорело </ts>
               <ts e="T1584" id="Seg_9594" n="e" s="T1583">опеть. </ts>
            </ts>
            <ts e="T1590" id="Seg_9595" n="sc" s="T1585">
               <ts e="T1586" id="Seg_9597" n="e" s="T1585">На </ts>
               <ts e="T1587" id="Seg_9599" n="e" s="T1586">этим </ts>
               <ts e="T1588" id="Seg_9601" n="e" s="T1587">месте </ts>
               <ts e="T1589" id="Seg_9603" n="e" s="T1588">(загорело=) </ts>
               <ts e="T1590" id="Seg_9605" n="e" s="T1589">загорело. </ts>
            </ts>
            <ts e="T1598" id="Seg_9606" n="sc" s="T1591">
               <ts e="T1592" id="Seg_9608" n="e" s="T1591">Тады </ts>
               <ts e="T1593" id="Seg_9610" n="e" s="T1592">(затушили), </ts>
               <ts e="T1594" id="Seg_9612" n="e" s="T1593">теперь </ts>
               <ts e="T1595" id="Seg_9614" n="e" s="T1594">печка </ts>
               <ts e="T1596" id="Seg_9616" n="e" s="T1595">железна </ts>
               <ts e="T1597" id="Seg_9618" n="e" s="T1596">там </ts>
               <ts e="T1598" id="Seg_9620" n="e" s="T1597">стоит. </ts>
            </ts>
            <ts e="T1603" id="Seg_9621" n="sc" s="T1599">
               <ts e="T1600" id="Seg_9623" n="e" s="T1599">Почему </ts>
               <ts e="T1601" id="Seg_9625" n="e" s="T1600">загорело, </ts>
               <ts e="T1602" id="Seg_9627" n="e" s="T1601">не </ts>
               <ts e="T1603" id="Seg_9629" n="e" s="T1602">знаю. </ts>
            </ts>
            <ts e="T1610" id="Seg_9630" n="sc" s="T1606">
               <ts e="T1607" id="Seg_9632" n="e" s="T1606">И </ts>
               <ts e="T1608" id="Seg_9634" n="e" s="T1607">потом </ts>
               <ts e="T1609" id="Seg_9636" n="e" s="T1608">там </ts>
               <ts e="T1610" id="Seg_9638" n="e" s="T1609">туды. </ts>
            </ts>
            <ts e="T1615" id="Seg_9639" n="sc" s="T1611">
               <ts e="T1612" id="Seg_9641" n="e" s="T1611">И </ts>
               <ts e="T1613" id="Seg_9643" n="e" s="T1612">туды </ts>
               <ts e="T1614" id="Seg_9645" n="e" s="T1613">сзади </ts>
               <ts e="T1615" id="Seg_9647" n="e" s="T1614">там. </ts>
            </ts>
            <ts e="T1624" id="Seg_9648" n="sc" s="T1616">
               <ts e="T1617" id="Seg_9650" n="e" s="T1616">Уже </ts>
               <ts e="T1618" id="Seg_9652" n="e" s="T1617">привозят </ts>
               <ts e="T1619" id="Seg_9654" n="e" s="T1618">машину </ts>
               <ts e="T1620" id="Seg_9656" n="e" s="T1619">(му-) </ts>
               <ts e="T1621" id="Seg_9658" n="e" s="T1620">муки </ts>
               <ts e="T1622" id="Seg_9660" n="e" s="T1621">кули, </ts>
               <ts e="T1623" id="Seg_9662" n="e" s="T1622">там </ts>
               <ts e="T1624" id="Seg_9664" n="e" s="T1623">ставят. </ts>
            </ts>
            <ts e="T1633" id="Seg_9665" n="sc" s="T1625">
               <ts e="T1626" id="Seg_9667" n="e" s="T1625">И </ts>
               <ts e="T1627" id="Seg_9669" n="e" s="T1626">каку </ts>
               <ts e="T1628" id="Seg_9671" n="e" s="T1627">рыбу, </ts>
               <ts e="T1629" id="Seg_9673" n="e" s="T1628">дак </ts>
               <ts e="T1630" id="Seg_9675" n="e" s="T1629">она </ts>
               <ts e="T1631" id="Seg_9677" n="e" s="T1630">оттедова </ts>
               <ts e="T1632" id="Seg_9679" n="e" s="T1631">приносит </ts>
               <ts e="T1633" id="Seg_9681" n="e" s="T1632">сюды. </ts>
            </ts>
            <ts e="T1641" id="Seg_9682" n="sc" s="T1634">
               <ts e="T1635" id="Seg_9684" n="e" s="T1634">Привозят </ts>
               <ts e="T1636" id="Seg_9686" n="e" s="T1635">бочки </ts>
               <ts e="T1637" id="Seg_9688" n="e" s="T1636">пива, </ts>
               <ts e="T1638" id="Seg_9690" n="e" s="T1637">и </ts>
               <ts e="T1639" id="Seg_9692" n="e" s="T1638">потом </ts>
               <ts e="T1640" id="Seg_9694" n="e" s="T1639">разное </ts>
               <ts e="T1641" id="Seg_9696" n="e" s="T1640">вино. </ts>
            </ts>
            <ts e="T1651" id="Seg_9697" n="sc" s="T1644">
               <ts e="T1645" id="Seg_9699" n="e" s="T1644">Полный </ts>
               <ts e="T1646" id="Seg_9701" n="e" s="T1645">теперь </ts>
               <ts e="T1647" id="Seg_9703" n="e" s="T1646">магазин, </ts>
               <ts e="T1648" id="Seg_9705" n="e" s="T1647">их </ts>
               <ts e="T1649" id="Seg_9707" n="e" s="T1648">две </ts>
               <ts e="T1650" id="Seg_9709" n="e" s="T1649">продавщицы </ts>
               <ts e="T1651" id="Seg_9711" n="e" s="T1650">были. </ts>
            </ts>
            <ts e="T1658" id="Seg_9712" n="sc" s="T1652">
               <ts e="T1653" id="Seg_9714" n="e" s="T1652">А </ts>
               <ts e="T1654" id="Seg_9716" n="e" s="T1653">теперь </ts>
               <ts e="T1655" id="Seg_9718" n="e" s="T1654">одна </ts>
               <ts e="T1656" id="Seg_9720" n="e" s="T1655">моёго </ts>
               <ts e="T1657" id="Seg_9722" n="e" s="T1656">внучка </ts>
               <ts e="T1658" id="Seg_9724" n="e" s="T1657">жена. </ts>
            </ts>
            <ts e="T1671" id="Seg_9725" n="sc" s="T1662">
               <ts e="T1663" id="Seg_9727" n="e" s="T1662">Вот </ts>
               <ts e="T1664" id="Seg_9729" n="e" s="T1663">ты, </ts>
               <ts e="T1665" id="Seg_9731" n="e" s="T1664">можешь, </ts>
               <ts e="T1666" id="Seg_9733" n="e" s="T1665">знаешь, </ts>
               <ts e="T1667" id="Seg_9735" n="e" s="T1666">напротив </ts>
               <ts e="T1668" id="Seg_9737" n="e" s="T1667">Загороднюка </ts>
               <ts e="T1669" id="Seg_9739" n="e" s="T1668">жил </ts>
               <ts e="T1670" id="Seg_9741" n="e" s="T1669">Николай </ts>
               <ts e="T1671" id="Seg_9743" n="e" s="T1670">Мурачёв. </ts>
            </ts>
            <ts e="T1698" id="Seg_9744" n="sc" s="T1680">
               <ts e="T1682" id="Seg_9746" n="e" s="T1680">Ну </ts>
               <ts e="T1683" id="Seg_9748" n="e" s="T1682">(он) </ts>
               <ts e="T1684" id="Seg_9750" n="e" s="T1683">Николай </ts>
               <ts e="T1685" id="Seg_9752" n="e" s="T1684">Мурачёв, </ts>
               <ts e="T1686" id="Seg_9754" n="e" s="T1685">он </ts>
               <ts e="T1687" id="Seg_9756" n="e" s="T1686">вот </ts>
               <ts e="T1688" id="Seg_9758" n="e" s="T1687">так </ts>
               <ts e="T1689" id="Seg_9760" n="e" s="T1688">вот, </ts>
               <ts e="T1690" id="Seg_9762" n="e" s="T1689">где </ts>
               <ts e="T1691" id="Seg_9764" n="e" s="T1690">вы </ts>
               <ts e="T1692" id="Seg_9766" n="e" s="T1691">на </ts>
               <ts e="T1693" id="Seg_9768" n="e" s="T1692">фатере </ts>
               <ts e="T1694" id="Seg_9770" n="e" s="T1693">были, </ts>
               <ts e="T1695" id="Seg_9772" n="e" s="T1694">а </ts>
               <ts e="T1696" id="Seg_9774" n="e" s="T1695">он </ts>
               <ts e="T1697" id="Seg_9776" n="e" s="T1696">вот </ts>
               <ts e="T1698" id="Seg_9778" n="e" s="T1697">сюды. </ts>
            </ts>
            <ts e="T1709" id="Seg_9779" n="sc" s="T1699">
               <ts e="T1700" id="Seg_9781" n="e" s="T1699">На </ts>
               <ts e="T1701" id="Seg_9783" n="e" s="T1700">уголке, </ts>
               <ts e="T1702" id="Seg_9785" n="e" s="T1701">за </ts>
               <ts e="T1703" id="Seg_9787" n="e" s="T1702">им </ts>
               <ts e="T1704" id="Seg_9789" n="e" s="T1703">еще </ts>
               <ts e="T1705" id="Seg_9791" n="e" s="T1704">один </ts>
               <ts e="T1706" id="Seg_9793" n="e" s="T1705">дом </ts>
               <ts e="T1707" id="Seg_9795" n="e" s="T1706">только </ts>
               <ts e="T1708" id="Seg_9797" n="e" s="T1707">стоит, </ts>
               <ts e="T1709" id="Seg_9799" n="e" s="T1708">Загороднюк. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T16" id="Seg_9800" s="T6">PKZ_19700819_09343-1bz.PKZ.001 (004)</ta>
            <ta e="T31" id="Seg_9801" s="T29">PKZ_19700819_09343-1bz.PKZ.002 (007)</ta>
            <ta e="T43" id="Seg_9802" s="T35">PKZ_19700819_09343-1bz.PKZ.003 (009)</ta>
            <ta e="T55" id="Seg_9803" s="T44">PKZ_19700819_09343-1bz.PKZ.004 (010)</ta>
            <ta e="T62" id="Seg_9804" s="T56">PKZ_19700819_09343-1bz.PKZ.005 (011)</ta>
            <ta e="T68" id="Seg_9805" s="T63">PKZ_19700819_09343-1bz.PKZ.006 (012)</ta>
            <ta e="T74" id="Seg_9806" s="T69">PKZ_19700819_09343-1bz.PKZ.007 (013)</ta>
            <ta e="T84" id="Seg_9807" s="T75">PKZ_19700819_09343-1bz.PKZ.008 (014)</ta>
            <ta e="T89" id="Seg_9808" s="T85">PKZ_19700819_09343-1bz.PKZ.009 (015)</ta>
            <ta e="T95" id="Seg_9809" s="T90">PKZ_19700819_09343-1bz.PKZ.010 (016)</ta>
            <ta e="T106" id="Seg_9810" s="T95">PKZ_19700819_09343-1bz.PKZ.011 (017)</ta>
            <ta e="T113" id="Seg_9811" s="T107">PKZ_19700819_09343-1bz.PKZ.012 (018)</ta>
            <ta e="T118" id="Seg_9812" s="T114">PKZ_19700819_09343-1bz.PKZ.013 (019)</ta>
            <ta e="T122" id="Seg_9813" s="T119">PKZ_19700819_09343-1bz.PKZ.014 (020)</ta>
            <ta e="T130" id="Seg_9814" s="T123">PKZ_19700819_09343-1bz.PKZ.015 (021)</ta>
            <ta e="T137" id="Seg_9815" s="T131">PKZ_19700819_09343-1bz.PKZ.016 (022)</ta>
            <ta e="T146" id="Seg_9816" s="T138">PKZ_19700819_09343-1bz.PKZ.017 (023)</ta>
            <ta e="T154" id="Seg_9817" s="T147">PKZ_19700819_09343-1bz.PKZ.018 (024)</ta>
            <ta e="T161" id="Seg_9818" s="T155">PKZ_19700819_09343-1bz.PKZ.019 (025)</ta>
            <ta e="T172" id="Seg_9819" s="T162">PKZ_19700819_09343-1bz.PKZ.020 (026)</ta>
            <ta e="T179" id="Seg_9820" s="T173">PKZ_19700819_09343-1bz.PKZ.021 (027)</ta>
            <ta e="T183" id="Seg_9821" s="T180">PKZ_19700819_09343-1bz.PKZ.022 (028)</ta>
            <ta e="T194" id="Seg_9822" s="T184">PKZ_19700819_09343-1bz.PKZ.023 (029)</ta>
            <ta e="T201" id="Seg_9823" s="T195">PKZ_19700819_09343-1bz.PKZ.024 (030)</ta>
            <ta e="T206" id="Seg_9824" s="T202">PKZ_19700819_09343-1bz.PKZ.025 (031)</ta>
            <ta e="T211" id="Seg_9825" s="T207">PKZ_19700819_09343-1bz.PKZ.026 (032)</ta>
            <ta e="T218" id="Seg_9826" s="T212">PKZ_19700819_09343-1bz.PKZ.027 (033)</ta>
            <ta e="T223" id="Seg_9827" s="T219">PKZ_19700819_09343-1bz.PKZ.028 (034)</ta>
            <ta e="T234" id="Seg_9828" s="T224">PKZ_19700819_09343-1bz.PKZ.029 (035)</ta>
            <ta e="T244" id="Seg_9829" s="T235">PKZ_19700819_09343-1bz.PKZ.030 (036)</ta>
            <ta e="T252" id="Seg_9830" s="T245">PKZ_19700819_09343-1bz.PKZ.031 (037)</ta>
            <ta e="T255" id="Seg_9831" s="T253">PKZ_19700819_09343-1bz.PKZ.032 (038)</ta>
            <ta e="T263" id="Seg_9832" s="T256">PKZ_19700819_09343-1bz.PKZ.033 (039)</ta>
            <ta e="T269" id="Seg_9833" s="T266">PKZ_19700819_09343-1bz.PKZ.034 (041)</ta>
            <ta e="T288" id="Seg_9834" s="T286">PKZ_19700819_09343-1bz.PKZ.035 (045)</ta>
            <ta e="T308" id="Seg_9835" s="T292">PKZ_19700819_09343-1bz.PKZ.036 (047)</ta>
            <ta e="T310" id="Seg_9836" s="T308">PKZ_19700819_09343-1bz.PKZ.037 (050)</ta>
            <ta e="T340" id="Seg_9837" s="T323">PKZ_19700819_09343-1bz.PKZ.038 (052)</ta>
            <ta e="T350" id="Seg_9838" s="T348">PKZ_19700819_09343-1bz.PKZ.039 (056)</ta>
            <ta e="T358" id="Seg_9839" s="T352">PKZ_19700819_09343-1bz.PKZ.040 (058)</ta>
            <ta e="T363" id="Seg_9840" s="T359">PKZ_19700819_09343-1bz.PKZ.041 (059)</ta>
            <ta e="T369" id="Seg_9841" s="T363">PKZ_19700819_09343-1bz.PKZ.042 (060)</ta>
            <ta e="T383" id="Seg_9842" s="T371">PKZ_19700819_09343-1bz.PKZ.043 (062)</ta>
            <ta e="T422" id="Seg_9843" s="T421">PKZ_19700819_09343-1bz.PKZ.044 (068)</ta>
            <ta e="T426" id="Seg_9844" s="T425">PKZ_19700819_09343-1bz.PKZ.045 (070)</ta>
            <ta e="T429" id="Seg_9845" s="T427">PKZ_19700819_09343-1bz.PKZ.046 (071)</ta>
            <ta e="T432" id="Seg_9846" s="T430">PKZ_19700819_09343-1bz.PKZ.047 (072)</ta>
            <ta e="T434" id="Seg_9847" s="T433">PKZ_19700819_09343-1bz.PKZ.048 (073)</ta>
            <ta e="T438" id="Seg_9848" s="T435">PKZ_19700819_09343-1bz.PKZ.049 (074)</ta>
            <ta e="T455" id="Seg_9849" s="T443">PKZ_19700819_09343-1bz.PKZ.050 (076)</ta>
            <ta e="T461" id="Seg_9850" s="T458">PKZ_19700819_09343-1bz.PKZ.051 (078)</ta>
            <ta e="T470" id="Seg_9851" s="T462">PKZ_19700819_09343-1bz.PKZ.052 (079)</ta>
            <ta e="T502" id="Seg_9852" s="T497">PKZ_19700819_09343-1bz.PKZ.053 (083)</ta>
            <ta e="T525" id="Seg_9853" s="T520">PKZ_19700819_09343-1bz.PKZ.054 (086)</ta>
            <ta e="T532" id="Seg_9854" s="T527">PKZ_19700819_09343-1bz.PKZ.055 (087)</ta>
            <ta e="T544" id="Seg_9855" s="T533">PKZ_19700819_09343-1bz.PKZ.056 (088)</ta>
            <ta e="T561" id="Seg_9856" s="T549">PKZ_19700819_09343-1bz.PKZ.057 (090)</ta>
            <ta e="T574" id="Seg_9857" s="T564">PKZ_19700819_09343-1bz.PKZ.058 (092)</ta>
            <ta e="T582" id="Seg_9858" s="T576">PKZ_19700819_09343-1bz.PKZ.059 (094)</ta>
            <ta e="T587" id="Seg_9859" s="T583">PKZ_19700819_09343-1bz.PKZ.060 (095)</ta>
            <ta e="T606" id="Seg_9860" s="T592">PKZ_19700819_09343-1bz.PKZ.061 (098)</ta>
            <ta e="T610" id="Seg_9861" s="T607">PKZ_19700819_09343-1bz.PKZ.062 (099)</ta>
            <ta e="T625" id="Seg_9862" s="T611">PKZ_19700819_09343-1bz.PKZ.063 (100)</ta>
            <ta e="T631" id="Seg_9863" s="T626">PKZ_19700819_09343-1bz.PKZ.064 (101)</ta>
            <ta e="T645" id="Seg_9864" s="T632">PKZ_19700819_09343-1bz.PKZ.065 (102)</ta>
            <ta e="T650" id="Seg_9865" s="T645">PKZ_19700819_09343-1bz.PKZ.066 (103)</ta>
            <ta e="T655" id="Seg_9866" s="T650">PKZ_19700819_09343-1bz.PKZ.067 (104)</ta>
            <ta e="T667" id="Seg_9867" s="T656">PKZ_19700819_09343-1bz.PKZ.068 (105)</ta>
            <ta e="T672" id="Seg_9868" s="T667">PKZ_19700819_09343-1bz.PKZ.069 (106)</ta>
            <ta e="T681" id="Seg_9869" s="T673">PKZ_19700819_09343-1bz.PKZ.070 (107)</ta>
            <ta e="T688" id="Seg_9870" s="T682">PKZ_19700819_09343-1bz.PKZ.071 (108)</ta>
            <ta e="T701" id="Seg_9871" s="T689">PKZ_19700819_09343-1bz.PKZ.072 (109)</ta>
            <ta e="T712" id="Seg_9872" s="T702">PKZ_19700819_09343-1bz.PKZ.073 (110)</ta>
            <ta e="T717" id="Seg_9873" s="T712">PKZ_19700819_09343-1bz.PKZ.074 (111)</ta>
            <ta e="T720" id="Seg_9874" s="T718">PKZ_19700819_09343-1bz.PKZ.075 (112)</ta>
            <ta e="T727" id="Seg_9875" s="T721">PKZ_19700819_09343-1bz.PKZ.076 (113)</ta>
            <ta e="T735" id="Seg_9876" s="T728">PKZ_19700819_09343-1bz.PKZ.077 (114)</ta>
            <ta e="T742" id="Seg_9877" s="T736">PKZ_19700819_09343-1bz.PKZ.078 (115)</ta>
            <ta e="T750" id="Seg_9878" s="T743">PKZ_19700819_09343-1bz.PKZ.079 (116)</ta>
            <ta e="T761" id="Seg_9879" s="T751">PKZ_19700819_09343-1bz.PKZ.080 (117)</ta>
            <ta e="T771" id="Seg_9880" s="T762">PKZ_19700819_09343-1bz.PKZ.081 (118)</ta>
            <ta e="T777" id="Seg_9881" s="T772">PKZ_19700819_09343-1bz.PKZ.082 (119)</ta>
            <ta e="T786" id="Seg_9882" s="T778">PKZ_19700819_09343-1bz.PKZ.083 (120)</ta>
            <ta e="T792" id="Seg_9883" s="T787">PKZ_19700819_09343-1bz.PKZ.084 (121)</ta>
            <ta e="T797" id="Seg_9884" s="T793">PKZ_19700819_09343-1bz.PKZ.085 (122)</ta>
            <ta e="T806" id="Seg_9885" s="T798">PKZ_19700819_09343-1bz.PKZ.086 (123)</ta>
            <ta e="T818" id="Seg_9886" s="T807">PKZ_19700819_09343-1bz.PKZ.087 (124)</ta>
            <ta e="T823" id="Seg_9887" s="T819">PKZ_19700819_09343-1bz.PKZ.088 (125)</ta>
            <ta e="T834" id="Seg_9888" s="T824">PKZ_19700819_09343-1bz.PKZ.089 (126)</ta>
            <ta e="T838" id="Seg_9889" s="T835">PKZ_19700819_09343-1bz.PKZ.090 (127)</ta>
            <ta e="T854" id="Seg_9890" s="T845">PKZ_19700819_09343-1bz.PKZ.091 (129)</ta>
            <ta e="T859" id="Seg_9891" s="T856">PKZ_19700819_09343-1bz.PKZ.092 (131)</ta>
            <ta e="T871" id="Seg_9892" s="T860">PKZ_19700819_09343-1bz.PKZ.093 (132)</ta>
            <ta e="T878" id="Seg_9893" s="T872">PKZ_19700819_09343-1bz.PKZ.094 (133)</ta>
            <ta e="T888" id="Seg_9894" s="T879">PKZ_19700819_09343-1bz.PKZ.095 (134)</ta>
            <ta e="T905" id="Seg_9895" s="T889">PKZ_19700819_09343-1bz.PKZ.096 (135)</ta>
            <ta e="T915" id="Seg_9896" s="T909">PKZ_19700819_09343-1bz.PKZ.097 (138)</ta>
            <ta e="T918" id="Seg_9897" s="T916">PKZ_19700819_09343-1bz.PKZ.098 (139)</ta>
            <ta e="T933" id="Seg_9898" s="T929">PKZ_19700819_09343-1bz.PKZ.099 (141)</ta>
            <ta e="T954" id="Seg_9899" s="T949">PKZ_19700819_09343-1bz.PKZ.100 (144)</ta>
            <ta e="T959" id="Seg_9900" s="T957">PKZ_19700819_09343-1bz.PKZ.101 (146)</ta>
            <ta e="T977" id="Seg_9901" s="T969">PKZ_19700819_09343-1bz.PKZ.102 (149)</ta>
            <ta e="T986" id="Seg_9902" s="T978">PKZ_19700819_09343-1bz.PKZ.103 (150)</ta>
            <ta e="T989" id="Seg_9903" s="T987">PKZ_19700819_09343-1bz.PKZ.104 (151)</ta>
            <ta e="T993" id="Seg_9904" s="T990">PKZ_19700819_09343-1bz.PKZ.105 (152)</ta>
            <ta e="T999" id="Seg_9905" s="T994">PKZ_19700819_09343-1bz.PKZ.106 (153)</ta>
            <ta e="T1007" id="Seg_9906" s="T1000">PKZ_19700819_09343-1bz.PKZ.107 (154)</ta>
            <ta e="T1012" id="Seg_9907" s="T1008">PKZ_19700819_09343-1bz.PKZ.108 (155)</ta>
            <ta e="T1020" id="Seg_9908" s="T1013">PKZ_19700819_09343-1bz.PKZ.109 (156)</ta>
            <ta e="T1027" id="Seg_9909" s="T1020">PKZ_19700819_09343-1bz.PKZ.110 (157)</ta>
            <ta e="T1033" id="Seg_9910" s="T1028">PKZ_19700819_09343-1bz.PKZ.111 (158)</ta>
            <ta e="T1037" id="Seg_9911" s="T1034">PKZ_19700819_09343-1bz.PKZ.112 (159)</ta>
            <ta e="T1041" id="Seg_9912" s="T1038">PKZ_19700819_09343-1bz.PKZ.113 (160)</ta>
            <ta e="T1064" id="Seg_9913" s="T1061">PKZ_19700819_09343-1bz.PKZ.114 (163)</ta>
            <ta e="T1080" id="Seg_9914" s="T1073">PKZ_19700819_09343-1bz.PKZ.115 (165)</ta>
            <ta e="T1084" id="Seg_9915" s="T1080">PKZ_19700819_09343-1bz.PKZ.116 (166)</ta>
            <ta e="T1089" id="Seg_9916" s="T1085">PKZ_19700819_09343-1bz.PKZ.117 (167)</ta>
            <ta e="T1093" id="Seg_9917" s="T1090">PKZ_19700819_09343-1bz.PKZ.118 (168)</ta>
            <ta e="T1100" id="Seg_9918" s="T1094">PKZ_19700819_09343-1bz.PKZ.119 (169)</ta>
            <ta e="T1138" id="Seg_9919" s="T1133">PKZ_19700819_09343-1bz.PKZ.120 (174)</ta>
            <ta e="T1141" id="Seg_9920" s="T1139">PKZ_19700819_09343-1bz.PKZ.121 (175)</ta>
            <ta e="T1148" id="Seg_9921" s="T1142">PKZ_19700819_09343-1bz.PKZ.122 (176)</ta>
            <ta e="T1181" id="Seg_9922" s="T1177">PKZ_19700819_09343-1bz.PKZ.123 (183)</ta>
            <ta e="T1188" id="Seg_9923" s="T1182">PKZ_19700819_09343-1bz.PKZ.124 (184)</ta>
            <ta e="T1193" id="Seg_9924" s="T1189">PKZ_19700819_09343-1bz.PKZ.125 (185)</ta>
            <ta e="T1206" id="Seg_9925" s="T1194">PKZ_19700819_09343-1bz.PKZ.126 (186)</ta>
            <ta e="T1214" id="Seg_9926" s="T1207">PKZ_19700819_09343-1bz.PKZ.127 (187)</ta>
            <ta e="T1223" id="Seg_9927" s="T1215">PKZ_19700819_09343-1bz.PKZ.128 (188)</ta>
            <ta e="T1227" id="Seg_9928" s="T1224">PKZ_19700819_09343-1bz.PKZ.129 (189)</ta>
            <ta e="T1235" id="Seg_9929" s="T1227">PKZ_19700819_09343-1bz.PKZ.130 (190)</ta>
            <ta e="T1237" id="Seg_9930" s="T1236">PKZ_19700819_09343-1bz.PKZ.131 (191)</ta>
            <ta e="T1257" id="Seg_9931" s="T1248">PKZ_19700819_09343-1bz.PKZ.132 (193)</ta>
            <ta e="T1264" id="Seg_9932" s="T1257">PKZ_19700819_09343-1bz.PKZ.133 (194)</ta>
            <ta e="T1271" id="Seg_9933" s="T1265">PKZ_19700819_09343-1bz.PKZ.134 (195)</ta>
            <ta e="T1278" id="Seg_9934" s="T1272">PKZ_19700819_09343-1bz.PKZ.135 (196)</ta>
            <ta e="T1287" id="Seg_9935" s="T1279">PKZ_19700819_09343-1bz.PKZ.136 (197)</ta>
            <ta e="T1344" id="Seg_9936" s="T1341">PKZ_19700819_09343-1bz.PKZ.137 (204)</ta>
            <ta e="T1353" id="Seg_9937" s="T1347">PKZ_19700819_09343-1bz.PKZ.138 (206)</ta>
            <ta e="T1355" id="Seg_9938" s="T1354">PKZ_19700819_09343-1bz.PKZ.139 (207)</ta>
            <ta e="T1372" id="Seg_9939" s="T1356">PKZ_19700819_09343-1bz.PKZ.140 (208)</ta>
            <ta e="T1375" id="Seg_9940" s="T1372">PKZ_19700819_09343-1bz.PKZ.141 (209)</ta>
            <ta e="T1377" id="Seg_9941" s="T1376">PKZ_19700819_09343-1bz.PKZ.142 (210)</ta>
            <ta e="T1388" id="Seg_9942" s="T1380">PKZ_19700819_09343-1bz.PKZ.143 (212)</ta>
            <ta e="T1394" id="Seg_9943" s="T1389">PKZ_19700819_09343-1bz.PKZ.144 (213)</ta>
            <ta e="T1404" id="Seg_9944" s="T1394">PKZ_19700819_09343-1bz.PKZ.145 (214)</ta>
            <ta e="T1434" id="Seg_9945" s="T1433">PKZ_19700819_09343-1bz.PKZ.146 (217)</ta>
            <ta e="T1466" id="Seg_9946" s="T1465">PKZ_19700819_09343-1bz.PKZ.147 (220)</ta>
            <ta e="T1470" id="Seg_9947" s="T1467">PKZ_19700819_09343-1bz.PKZ.148 (221)</ta>
            <ta e="T1487" id="Seg_9948" s="T1471">PKZ_19700819_09343-1bz.PKZ.149 (222)</ta>
            <ta e="T1499" id="Seg_9949" s="T1488">PKZ_19700819_09343-1bz.PKZ.150 (225)</ta>
            <ta e="T1518" id="Seg_9950" s="T1502">PKZ_19700819_09343-1bz.PKZ.151 (228)</ta>
            <ta e="T1530" id="Seg_9951" s="T1522">PKZ_19700819_09343-1bz.PKZ.152 (230)</ta>
            <ta e="T1533" id="Seg_9952" s="T1531">PKZ_19700819_09343-1bz.PKZ.153 (231)</ta>
            <ta e="T1540" id="Seg_9953" s="T1534">PKZ_19700819_09343-1bz.PKZ.154 (232)</ta>
            <ta e="T1544" id="Seg_9954" s="T1542">PKZ_19700819_09343-1bz.PKZ.155 (232)</ta>
            <ta e="T1555" id="Seg_9955" s="T1545">PKZ_19700819_09343-1bz.PKZ.156 (234)</ta>
            <ta e="T1565" id="Seg_9956" s="T1556">PKZ_19700819_09343-1bz.PKZ.157 (236)</ta>
            <ta e="T1577" id="Seg_9957" s="T1566">PKZ_19700819_09343-1bz.PKZ.158 (237)</ta>
            <ta e="T1584" id="Seg_9958" s="T1578">PKZ_19700819_09343-1bz.PKZ.159 (238)</ta>
            <ta e="T1590" id="Seg_9959" s="T1585">PKZ_19700819_09343-1bz.PKZ.160 (239)</ta>
            <ta e="T1598" id="Seg_9960" s="T1591">PKZ_19700819_09343-1bz.PKZ.161 (240)</ta>
            <ta e="T1603" id="Seg_9961" s="T1599">PKZ_19700819_09343-1bz.PKZ.162 (241)</ta>
            <ta e="T1610" id="Seg_9962" s="T1606">PKZ_19700819_09343-1bz.PKZ.163 (243)</ta>
            <ta e="T1615" id="Seg_9963" s="T1611">PKZ_19700819_09343-1bz.PKZ.164 (244)</ta>
            <ta e="T1624" id="Seg_9964" s="T1616">PKZ_19700819_09343-1bz.PKZ.165 (245)</ta>
            <ta e="T1633" id="Seg_9965" s="T1625">PKZ_19700819_09343-1bz.PKZ.166 (246)</ta>
            <ta e="T1641" id="Seg_9966" s="T1634">PKZ_19700819_09343-1bz.PKZ.167 (247)</ta>
            <ta e="T1651" id="Seg_9967" s="T1644">PKZ_19700819_09343-1bz.PKZ.168 (249)</ta>
            <ta e="T1658" id="Seg_9968" s="T1652">PKZ_19700819_09343-1bz.PKZ.169 (250)</ta>
            <ta e="T1671" id="Seg_9969" s="T1662">PKZ_19700819_09343-1bz.PKZ.170 (253)</ta>
            <ta e="T1698" id="Seg_9970" s="T1680">PKZ_19700819_09343-1bz.PKZ.171 (255)</ta>
            <ta e="T1709" id="Seg_9971" s="T1699">PKZ_19700819_09343-1bz.PKZ.172 (256)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T16" id="Seg_9972" s="T6">Ну, можно по-русски рассказать. </ta>
            <ta e="T31" id="Seg_9973" s="T29">Ну, идет? </ta>
            <ta e="T43" id="Seg_9974" s="T35">Паша Василинич (свою Шу-) со своей Шурой разошлись. </ta>
            <ta e="T55" id="Seg_9975" s="T44">Ей всё в доме осталось, а ему только одно мотоцикло досталось. </ta>
            <ta e="T62" id="Seg_9976" s="T56">Он там связался с одной женщиной. </ta>
            <ta e="T68" id="Seg_9977" s="T63">Она в конторе работала счетоводом. </ta>
            <ta e="T74" id="Seg_9978" s="T69">У ей тоже мужик был. </ta>
            <ta e="T84" id="Seg_9979" s="T75">Пьяный напился и спал, руку отморозил, в больнице лежал. </ta>
            <ta e="T89" id="Seg_9980" s="T85">И уехал к матери. </ta>
            <ta e="T95" id="Seg_9981" s="T90">Она потом выслала всё евонно. </ta>
            <ta e="T106" id="Seg_9982" s="T95">И с Пашей сошлась, а Паша разделился (со-) со своёй женой. </ta>
            <ta e="T113" id="Seg_9983" s="T107">И на суд она подала, он… </ta>
            <ta e="T118" id="Seg_9984" s="T114">Высудили с его алименты. </ta>
            <ta e="T122" id="Seg_9985" s="T119">Всем троим платит. </ta>
            <ta e="T130" id="Seg_9986" s="T123">Когда она собралась, поехала туды, в Сушенское. </ta>
            <ta e="T137" id="Seg_9987" s="T131">Он мальчишка одного не отдал ей. </ta>
            <ta e="T146" id="Seg_9988" s="T138">Но она всё хотела отобрать, он его увез. </ta>
            <ta e="T154" id="Seg_9989" s="T147">А теперь нашу деревню с Вознесенкой соединили. </ta>
            <ta e="T161" id="Seg_9990" s="T155">И там он живет с ей. </ta>
            <ta e="T172" id="Seg_9991" s="T162">А мать евонна тут живет, а изба стоит одна, пустая. </ta>
            <ta e="T179" id="Seg_9992" s="T173">Никто не берет, люди все разъезжаются. </ta>
            <ta e="T183" id="Seg_9993" s="T180">С той деревни. </ta>
            <ta e="T194" id="Seg_9994" s="T184">(Pa-) Paša Vasilinič bostə Šuranə baʔluʔpi i baška ne ibi. </ta>
            <ta e="T201" id="Seg_9995" s="T195">Dĭgəttə dĭ dĭʔnə подала suttə dĭ. </ta>
            <ta e="T206" id="Seg_9996" s="T202">Dĭzeŋ (су-) судили dĭ. </ta>
            <ta e="T211" id="Seg_9997" s="T207">Tüj aktʼa esseŋdə mĭʔleʔbə. </ta>
            <ta e="T218" id="Seg_9998" s="T212">A dĭ kalla dʼürbi gijen Lenin amnolaʔpi. </ta>
            <ta e="T223" id="Seg_9999" s="T219">Dĭn amnolaʔbə (dĭ) tüj. </ta>
            <ta e="T234" id="Seg_10000" s="T224">Šide nʼi i (ko-) koʔbdo dĭzi, onʼiʔ nʼi (dĭ=) dĭn. </ta>
            <ta e="T244" id="Seg_10001" s="T235">Dĭgəttə (m-) măn koʔbdom šobi (sjuda), onʼiʔ nʼibə deʔpi. </ta>
            <ta e="T252" id="Seg_10002" s="T245">Dĭ măndə Pašanə: "Iʔ iʔ šide nʼi! </ta>
            <ta e="T255" id="Seg_10003" s="T253">Ej kereʔ". </ta>
            <ta e="T263" id="Seg_10004" s="T256">Nʼebosʼ dĭm ibi, a esseŋdə ej kereʔ. </ta>
            <ta e="T269" id="Seg_10005" s="T266">((BRK)) Только по-своему. </ta>
            <ta e="T288" id="Seg_10006" s="T286">Я говорила. </ta>
            <ta e="T308" id="Seg_10007" s="T292">(Ну) всё, как Паша сошелся, как детей, как на суд подала. </ta>
            <ta e="T310" id="Seg_10008" s="T308">Лименты высудила. </ta>
            <ta e="T340" id="Seg_10009" s="T323">Ну, я говорила (с=) с им сошлася она. </ta>
            <ta e="T350" id="Seg_10010" s="T348">Говорила. </ta>
            <ta e="T358" id="Seg_10011" s="T352">И как он руки отморозил. </ta>
            <ta e="T363" id="Seg_10012" s="T359">Ну, он же говорит? </ta>
            <ta e="T369" id="Seg_10013" s="T363">Я уж (рас-). </ta>
            <ta e="T383" id="Seg_10014" s="T371">Рассказывала же я по-русски, или нет, только по-своему? </ta>
            <ta e="T422" id="Seg_10015" s="T421">Тута? </ta>
            <ta e="T426" id="Seg_10016" s="T425">Забыла… </ta>
            <ta e="T429" id="Seg_10017" s="T427">Ну, давай. </ta>
            <ta e="T432" id="Seg_10018" s="T430">Как начать-то? </ta>
            <ta e="T434" id="Seg_10019" s="T433">По-своему? </ta>
            <ta e="T438" id="Seg_10020" s="T435">По-русски рассказать? </ta>
            <ta e="T455" id="Seg_10021" s="T443">Нет, я говорила вот сейчас, как он сошелся с ей. </ta>
            <ta e="T461" id="Seg_10022" s="T458">На свою наречие. </ta>
            <ta e="T470" id="Seg_10023" s="T462">А по-русски-то я не рассказала. </ta>
            <ta e="T502" id="Seg_10024" s="T497">Ну, чего нового… </ta>
            <ta e="T525" id="Seg_10025" s="T520">Нет, это… </ta>
            <ta e="T532" id="Seg_10026" s="T527">Теперь у нас Загороднюк помер. </ta>
            <ta e="T544" id="Seg_10027" s="T533">Можно про его сказать, ты не знал про его? </ta>
            <ta e="T561" id="Seg_10028" s="T549">Ну а ты не знал, что помер? </ta>
            <ta e="T574" id="Seg_10029" s="T564">(Уже=) Уже два года, как помер. </ta>
            <ta e="T582" id="Seg_10030" s="T576">Так я сильно плакала об ем. </ta>
            <ta e="T587" id="Seg_10031" s="T583">Хороший человек был. </ta>
            <ta e="T606" id="Seg_10032" s="T592">Вот, бывало, приду, куды мне надо ехать, в Уяр али куды. </ta>
            <ta e="T610" id="Seg_10033" s="T607">В Агинско ли. </ta>
            <ta e="T625" id="Seg_10034" s="T611">(Прид-) Я все его чтоб одного захватить, никто не слыхал и не завидывал бы. </ta>
            <ta e="T631" id="Seg_10035" s="T626">Он же против меня жил. </ta>
            <ta e="T645" id="Seg_10036" s="T632">Когда идет, я иду навстречу, скажу: "Василий Федорович, (мож-) машины пойдут, можно уехать?" </ta>
            <ta e="T650" id="Seg_10037" s="T645">Он скажет: "Завтре со мной". </ta>
            <ta e="T655" id="Seg_10038" s="T650">"А куды ты, говорит, хочешь?" </ta>
            <ta e="T667" id="Seg_10039" s="T656">Я говорю: "Да ((…)) тебе как сыну расскажу, еду богу молиться. </ta>
            <ta e="T672" id="Seg_10040" s="T667">"Завтре обязательно со мной уедешь". </ta>
            <ta e="T681" id="Seg_10041" s="T673">Ну, я на утро поднимаюсь, всё складаю и… </ta>
            <ta e="T688" id="Seg_10042" s="T682">Они же (ра-) знаешь, где жили? </ta>
            <ta e="T701" id="Seg_10043" s="T689">(Сейчас) сюды, к имя приду, его еще нету (с этой=) с конторы. </ta>
            <ta e="T712" id="Seg_10044" s="T702">Я сижу, он заходит: "О, ты тут, девка, уже", — скажет. </ta>
            <ta e="T717" id="Seg_10045" s="T712">Он всё меня девкой звал. </ta>
            <ta e="T720" id="Seg_10046" s="T718">"Тут", - говорю. </ta>
            <ta e="T727" id="Seg_10047" s="T721">"Я хотел послать Линку за тобой". </ta>
            <ta e="T735" id="Seg_10048" s="T728">Ее звать Лизавета, он всё Линкой звал. </ta>
            <ta e="T742" id="Seg_10049" s="T736">А ее так и звали, Линкой. </ta>
            <ta e="T750" id="Seg_10050" s="T743">Ну, мы с им приехали к конторе. </ta>
            <ta e="T761" id="Seg_10051" s="T751">А там одна женщина подбежала, говорит: "Может, и меня (возьмете)?" </ta>
            <ta e="T771" id="Seg_10052" s="T762">"Нет, говорит, никого не возьму", — а я там сижу. </ta>
            <ta e="T777" id="Seg_10053" s="T772">Думаю, может еще кто поедет. </ta>
            <ta e="T786" id="Seg_10054" s="T778">Взяли трое, поехали, а тую женщину не взял. </ta>
            <ta e="T792" id="Seg_10055" s="T787">Как я скажу, обязательно возьмет. </ta>
            <ta e="T797" id="Seg_10056" s="T793">Если сам не поедет… </ta>
            <ta e="T806" id="Seg_10057" s="T798">Спрошу, сам не поедет: "Ладно, я скажу тебе". </ta>
            <ta e="T818" id="Seg_10058" s="T807">Думаю: ну, соберусь, пойду, а то, говорю, как он будет знать. </ta>
            <ta e="T823" id="Seg_10059" s="T819">Забудет - народу же много… </ta>
            <ta e="T834" id="Seg_10060" s="T824">Я иду (встреча), а там — Ты Августа-то знаешь, (немец) наш. </ta>
            <ta e="T838" id="Seg_10061" s="T835">Он бригадир, Август. </ta>
            <ta e="T854" id="Seg_10062" s="T845">Он уехал сейчас. </ta>
            <ta e="T859" id="Seg_10063" s="T856">В Волгоград. </ta>
            <ta e="T871" id="Seg_10064" s="T860">У его сын женился, тоже на волгоградской, и уехал в Волгоград. </ta>
            <ta e="T878" id="Seg_10065" s="T872">Там у нас всякие теперь люди. </ta>
            <ta e="T888" id="Seg_10066" s="T879">Вот после тебя сюды к Малиновке целая деревня настроилась. </ta>
            <ta e="T905" id="Seg_10067" s="T889">И (сю- сюды к М-) сюды вот на Пермяково, сюды к Малиновке чуть не до кладбища. </ta>
            <ta e="T915" id="Seg_10068" s="T909">(И во-) И вот это… </ta>
            <ta e="T918" id="Seg_10069" s="T916">Опять ((…)). </ta>
            <ta e="T933" id="Seg_10070" s="T929">Загороднюка? </ta>
            <ta e="T954" id="Seg_10071" s="T949">Ну, пусти. </ta>
            <ta e="T959" id="Seg_10072" s="T957">Записаны? </ta>
            <ta e="T977" id="Seg_10073" s="T969">(Каmen=) miʔ külambi Zagorodnʼuk, uže (sĭ-) šide kö. </ta>
            <ta e="T986" id="Seg_10074" s="T978">Kamen măn gibər kalam, mănlăm dĭʔnə: "(I-) Kalal?" </ta>
            <ta e="T989" id="Seg_10075" s="T987">"Nu, šoʔ". </ta>
            <ta e="T993" id="Seg_10076" s="T990">(Măn=) Măn šobiam. </ta>
            <ta e="T999" id="Seg_10077" s="T994">Dĭgəttə dĭ măndə:" "Tăn šobial. </ta>
            <ta e="T1007" id="Seg_10078" s="T1000">A măn öʔlusʼtə xatʼel bostə nen tănzi". </ta>
            <ta e="T1012" id="Seg_10079" s="T1008">Dĭgəttə amnolbibaʔ, kambibaʔ kăntoranə. </ta>
            <ta e="T1020" id="Seg_10080" s="T1013">Dĭ (другой=) baška nüke šobi: "Măna iləl?" </ta>
            <ta e="T1027" id="Seg_10081" s="T1020">(Măn=) Dĭ mămbi: "Šindimdə (em=) ej iləm". </ta>
            <ta e="T1033" id="Seg_10082" s="T1028">Măn tenəbiem: išo šində kaləj. </ta>
            <ta e="T1037" id="Seg_10083" s="T1034">Dĭgəttə nagurgöʔ kambibaʔ. </ta>
            <ta e="T1041" id="Seg_10084" s="T1038">Ну, всё. </ta>
            <ta e="T1064" id="Seg_10085" s="T1061">Сказала! </ta>
            <ta e="T1080" id="Seg_10086" s="T1073">Dĭgəttə dĭʔnə măn mămbial (măn=) "Giber kandəgal?" </ta>
            <ta e="T1084" id="Seg_10087" s="T1080">Măn măndəm: "Kudajdə üzəsʼtə". </ta>
            <ta e="T1089" id="Seg_10088" s="T1085">"Nu, (karəlʼdʼan) mănzi kalal". </ta>
            <ta e="T1093" id="Seg_10089" s="T1090">Dăre nörbəbi măna. </ta>
            <ta e="T1100" id="Seg_10090" s="T1094">А так я тебе рассказала всё. </ta>
            <ta e="T1138" id="Seg_10091" s="T1133">Говорила, говорила. </ta>
            <ta e="T1141" id="Seg_10092" s="T1139">Šide kö. </ta>
            <ta e="T1148" id="Seg_10093" s="T1142">Два года, šide kö. </ta>
            <ta e="T1181" id="Seg_10094" s="T1177">Miʔ brigadir ibi, nʼemec. </ta>
            <ta e="T1188" id="Seg_10095" s="T1182">Dĭ ugandə (il-) ildə ineʔi mĭbi. </ta>
            <ta e="T1193" id="Seg_10096" s="T1189">Tüj dĭ kalla dʼürbi Vălgăgrattə. </ta>
            <ta e="T1206" id="Seg_10097" s="T1194">A kamen šobi dĭ baška predsʼedatʼelʼ miʔnʼibeʔ, dĭ ildə ine (mĭbiʔi=) mĭbi. </ta>
            <ta e="T1214" id="Seg_10098" s="T1207">(Šindimdə ej=) (Keʔbdejleʔ) kambiʔi inezi mĭbi tože. </ta>
            <ta e="T1223" id="Seg_10099" s="T1215">Dĭ predsʼedatʼelʼ tüj miʔnʼibeʔ Văznʼesenkanə kambi, dĭn amnolaʔbə. </ta>
            <ta e="T1227" id="Seg_10100" s="T1224">Dĭʔnə tura nuldəbiʔi. </ta>
            <ta e="T1235" id="Seg_10101" s="T1227">Turat tüj nulaʔbə, šindidə ej amnolia dĭ turagən. </ta>
            <ta e="T1237" id="Seg_10102" s="T1236">Kabarləj. </ta>
            <ta e="T1257" id="Seg_10103" s="T1248">Председатель у нас хороший, (л-) с людям хорошо обходился. </ta>
            <ta e="T1264" id="Seg_10104" s="T1257">Коней давал, даже по ягоды ездили люди. </ta>
            <ta e="T1271" id="Seg_10105" s="T1265">А теперь нас соединили с Вознесенки. </ta>
            <ta e="T1278" id="Seg_10106" s="T1272">Ему выстроили тут дом в Абалаковой. </ta>
            <ta e="T1287" id="Seg_10107" s="T1279">(И=) А теперь в Вознесенку уехал, там живет. </ta>
            <ta e="T1344" id="Seg_10108" s="T1341">И сейчас есть такое. </ta>
            <ta e="T1353" id="Seg_10109" s="T1347">Продают прямо поллитром такое (крепкое) вино. </ta>
            <ta e="T1355" id="Seg_10110" s="T1354">Спирт! </ta>
            <ta e="T1372" id="Seg_10111" s="T1356">Ну, он так ((COUGH)), для здоровья вот столь выпьем, лучше как-то (как=) как это вот (са-) самогонка. </ta>
            <ta e="T1375" id="Seg_10112" s="T1372">Он чистый ведь. </ta>
            <ta e="T1377" id="Seg_10113" s="T1376">Вот. </ta>
            <ta e="T1388" id="Seg_10114" s="T1380">А ты, тот магазин у нас теперь… </ta>
            <ta e="T1394" id="Seg_10115" s="T1389">Ты в этим был магазине? </ta>
            <ta e="T1404" id="Seg_10116" s="T1394">У нас теперь другой, на том бугре, ты его знаешь? </ta>
            <ta e="T1434" id="Seg_10117" s="T1433">Вот… </ta>
            <ta e="T1466" id="Seg_10118" s="T1465">Погоди… </ta>
            <ta e="T1470" id="Seg_10119" s="T1467">Ну тут это… </ta>
            <ta e="T1487" id="Seg_10120" s="T1471">Как идешь, еще лог не переходишь, вот здесь магазин был, а тут лог. </ta>
            <ta e="T1499" id="Seg_10121" s="T1488">А теперь за логом, ((PAUSE)) тут это где моя родимая изба была. </ta>
            <ta e="T1518" id="Seg_10122" s="T1502">Ну, а как там она сгорела, не при тебе? </ta>
            <ta e="T1530" id="Seg_10123" s="T1522">Сгорела, а теперь на этим месте поставили магазин. </ta>
            <ta e="T1533" id="Seg_10124" s="T1531">Большой, обширный. </ta>
            <ta e="T1540" id="Seg_10125" s="T1534">Вот больше, пожалуй, этого дома будет. </ta>
            <ta e="T1544" id="Seg_10126" s="T1542">Там (кирпи-)…</ta>
            <ta e="T1555" id="Seg_10127" s="T1545">Нет, это (деревя-) деревянное, но хорошее, большое. </ta>
            <ta e="T1565" id="Seg_10128" s="T1556">Так тут зайдешь, всё тут полки закладено, тут и… </ta>
            <ta e="T1577" id="Seg_10129" s="T1566">И потом тут так выкладена така, как вроде вот печи такой. </ta>
            <ta e="T1584" id="Seg_10130" s="T1578">А потом чего-то там загорело опеть. </ta>
            <ta e="T1590" id="Seg_10131" s="T1585">На этим месте (загорело=) загорело. </ta>
            <ta e="T1598" id="Seg_10132" s="T1591">Тады (затушили), теперь печка железна там стоит. </ta>
            <ta e="T1603" id="Seg_10133" s="T1599">Почему загорело, не знаю. </ta>
            <ta e="T1610" id="Seg_10134" s="T1606">И потом там туды. </ta>
            <ta e="T1615" id="Seg_10135" s="T1611">И туды сзади там. </ta>
            <ta e="T1624" id="Seg_10136" s="T1616">Уже привозят машину (му-) муки кули, там ставят. </ta>
            <ta e="T1633" id="Seg_10137" s="T1625">И каку рыбу, дак она оттедова приносит сюды. </ta>
            <ta e="T1641" id="Seg_10138" s="T1634">Привозят бочки пива, и потом разное вино. </ta>
            <ta e="T1651" id="Seg_10139" s="T1644">Полный теперь магазин, их две продавщицы были. </ta>
            <ta e="T1658" id="Seg_10140" s="T1652">А теперь одна моёго внучка жена. </ta>
            <ta e="T1671" id="Seg_10141" s="T1662">Вот ты, можешь, знаешь, напротив Загороднюка жил Николай Мурачёв. </ta>
            <ta e="T1698" id="Seg_10142" s="T1680">Ну (он) Николай Мурачёв, он вот так вот, где вы на фатере были, а он вот сюды. </ta>
            <ta e="T1709" id="Seg_10143" s="T1699">На уголке, за им еще один дом только стоит, Загороднюк. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T50" id="Seg_10144" s="T49">а</ta>
            <ta e="T100" id="Seg_10145" s="T99">а</ta>
            <ta e="T148" id="Seg_10146" s="T147">а</ta>
            <ta e="T163" id="Seg_10147" s="T162">а</ta>
            <ta e="T168" id="Seg_10148" s="T167">а</ta>
            <ta e="T186" id="Seg_10149" s="T185">Paša</ta>
            <ta e="T187" id="Seg_10150" s="T186">Vasilinič</ta>
            <ta e="T188" id="Seg_10151" s="T187">bos-tə</ta>
            <ta e="T189" id="Seg_10152" s="T188">Šura-nə</ta>
            <ta e="T190" id="Seg_10153" s="T189">baʔ-luʔ-pi</ta>
            <ta e="T191" id="Seg_10154" s="T190">i</ta>
            <ta e="T192" id="Seg_10155" s="T191">baška</ta>
            <ta e="T193" id="Seg_10156" s="T192">ne</ta>
            <ta e="T194" id="Seg_10157" s="T193">i-bi</ta>
            <ta e="T196" id="Seg_10158" s="T195">dĭgəttə</ta>
            <ta e="T197" id="Seg_10159" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_10160" s="T197">dĭʔ-nə</ta>
            <ta e="T200" id="Seg_10161" s="T199">sut-tə</ta>
            <ta e="T201" id="Seg_10162" s="T200">dĭ</ta>
            <ta e="T203" id="Seg_10163" s="T202">dĭ-zeŋ</ta>
            <ta e="T206" id="Seg_10164" s="T205">dĭ</ta>
            <ta e="T208" id="Seg_10165" s="T207">tüj</ta>
            <ta e="T209" id="Seg_10166" s="T208">aktʼa</ta>
            <ta e="T210" id="Seg_10167" s="T209">es-seŋ-də</ta>
            <ta e="T211" id="Seg_10168" s="T210">mĭ-ʔleʔbə</ta>
            <ta e="T213" id="Seg_10169" s="T212">a</ta>
            <ta e="T214" id="Seg_10170" s="T213">dĭ</ta>
            <ta e="T1738" id="Seg_10171" s="T214">kal-la</ta>
            <ta e="T215" id="Seg_10172" s="T1738">dʼür-bi</ta>
            <ta e="T216" id="Seg_10173" s="T215">gijen</ta>
            <ta e="T217" id="Seg_10174" s="T216">Lenin</ta>
            <ta e="T218" id="Seg_10175" s="T217">amno-laʔ-pi</ta>
            <ta e="T220" id="Seg_10176" s="T219">dĭn</ta>
            <ta e="T221" id="Seg_10177" s="T220">amno-laʔbə</ta>
            <ta e="T222" id="Seg_10178" s="T221">dĭ</ta>
            <ta e="T223" id="Seg_10179" s="T222">tüj</ta>
            <ta e="T225" id="Seg_10180" s="T224">šide</ta>
            <ta e="T226" id="Seg_10181" s="T225">nʼi</ta>
            <ta e="T227" id="Seg_10182" s="T226">i</ta>
            <ta e="T229" id="Seg_10183" s="T228">koʔbdo</ta>
            <ta e="T230" id="Seg_10184" s="T229">dĭ-zi</ta>
            <ta e="T231" id="Seg_10185" s="T230">onʼiʔ</ta>
            <ta e="T232" id="Seg_10186" s="T231">nʼi</ta>
            <ta e="T233" id="Seg_10187" s="T232">dĭ</ta>
            <ta e="T234" id="Seg_10188" s="T233">dĭn</ta>
            <ta e="T236" id="Seg_10189" s="T235">dĭgəttə</ta>
            <ta e="T238" id="Seg_10190" s="T237">măn</ta>
            <ta e="T239" id="Seg_10191" s="T238">koʔbdo-m</ta>
            <ta e="T240" id="Seg_10192" s="T239">šo-bi</ta>
            <ta e="T242" id="Seg_10193" s="T241">onʼiʔ</ta>
            <ta e="T243" id="Seg_10194" s="T242">nʼi-bə</ta>
            <ta e="T244" id="Seg_10195" s="T243">deʔ-pi</ta>
            <ta e="T246" id="Seg_10196" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_10197" s="T246">măn-də</ta>
            <ta e="T248" id="Seg_10198" s="T247">Paša-nə</ta>
            <ta e="T249" id="Seg_10199" s="T248">i-ʔ</ta>
            <ta e="T250" id="Seg_10200" s="T249">i-ʔ</ta>
            <ta e="T251" id="Seg_10201" s="T250">šide</ta>
            <ta e="T252" id="Seg_10202" s="T251">nʼi</ta>
            <ta e="T254" id="Seg_10203" s="T253">ej</ta>
            <ta e="T255" id="Seg_10204" s="T254">kereʔ</ta>
            <ta e="T257" id="Seg_10205" s="T256">nʼebosʼ</ta>
            <ta e="T258" id="Seg_10206" s="T257">dĭ-m</ta>
            <ta e="T259" id="Seg_10207" s="T258">i-bi</ta>
            <ta e="T260" id="Seg_10208" s="T259">a</ta>
            <ta e="T261" id="Seg_10209" s="T260">es-seŋ-də</ta>
            <ta e="T262" id="Seg_10210" s="T261">ej</ta>
            <ta e="T263" id="Seg_10211" s="T262">kereʔ</ta>
            <ta e="T463" id="Seg_10212" s="T462">а</ta>
            <ta e="T552" id="Seg_10213" s="T550">а</ta>
            <ta e="T737" id="Seg_10214" s="T736">а</ta>
            <ta e="T752" id="Seg_10215" s="T751">а</ta>
            <ta e="T768" id="Seg_10216" s="T767">а</ta>
            <ta e="T782" id="Seg_10217" s="T781">а</ta>
            <ta e="T812" id="Seg_10218" s="T811">а</ta>
            <ta e="T828" id="Seg_10219" s="T827">а</ta>
            <ta e="T971" id="Seg_10220" s="T970">miʔ</ta>
            <ta e="T972" id="Seg_10221" s="T971">kü-lam-bi</ta>
            <ta e="T973" id="Seg_10222" s="T972">Zagorodnʼuk</ta>
            <ta e="T974" id="Seg_10223" s="T973">uže</ta>
            <ta e="T976" id="Seg_10224" s="T975">šide</ta>
            <ta e="T977" id="Seg_10225" s="T976">kö</ta>
            <ta e="T979" id="Seg_10226" s="T978">kamen</ta>
            <ta e="T980" id="Seg_10227" s="T979">măn</ta>
            <ta e="T981" id="Seg_10228" s="T980">gibər</ta>
            <ta e="T982" id="Seg_10229" s="T981">ka-la-m</ta>
            <ta e="T983" id="Seg_10230" s="T982">măn-lă-m</ta>
            <ta e="T984" id="Seg_10231" s="T983">dĭʔ-nə</ta>
            <ta e="T986" id="Seg_10232" s="T985">ka-la-l</ta>
            <ta e="T988" id="Seg_10233" s="T987">nu</ta>
            <ta e="T989" id="Seg_10234" s="T988">šo-ʔ</ta>
            <ta e="T991" id="Seg_10235" s="T990">măn</ta>
            <ta e="T992" id="Seg_10236" s="T991">măn</ta>
            <ta e="T993" id="Seg_10237" s="T992">šo-bia-m</ta>
            <ta e="T995" id="Seg_10238" s="T994">dĭgəttə</ta>
            <ta e="T996" id="Seg_10239" s="T995">dĭ</ta>
            <ta e="T997" id="Seg_10240" s="T996">măn-də</ta>
            <ta e="T998" id="Seg_10241" s="T997">tăn</ta>
            <ta e="T999" id="Seg_10242" s="T998">šo-bia-l</ta>
            <ta e="T1001" id="Seg_10243" s="T1000">a</ta>
            <ta e="T1002" id="Seg_10244" s="T1001">măn</ta>
            <ta e="T1003" id="Seg_10245" s="T1002">öʔlu-sʼtə</ta>
            <ta e="T1004" id="Seg_10246" s="T1003">xatʼel</ta>
            <ta e="T1005" id="Seg_10247" s="T1004">bos-tə</ta>
            <ta e="T1006" id="Seg_10248" s="T1005">ne-n</ta>
            <ta e="T1007" id="Seg_10249" s="T1006">tăn-zi</ta>
            <ta e="T1009" id="Seg_10250" s="T1008">dĭgəttə</ta>
            <ta e="T1010" id="Seg_10251" s="T1009">amno-l-bi-baʔ</ta>
            <ta e="T1011" id="Seg_10252" s="T1010">kam-bi-baʔ</ta>
            <ta e="T1012" id="Seg_10253" s="T1011">kăntora-nə</ta>
            <ta e="T1014" id="Seg_10254" s="T1013">dĭ</ta>
            <ta e="T1016" id="Seg_10255" s="T1015">baška</ta>
            <ta e="T1017" id="Seg_10256" s="T1016">nüke</ta>
            <ta e="T1018" id="Seg_10257" s="T1017">šo-bi</ta>
            <ta e="T1019" id="Seg_10258" s="T1018">măna</ta>
            <ta e="T1020" id="Seg_10259" s="T1019">i-lə-l</ta>
            <ta e="T1021" id="Seg_10260" s="T1020">măn</ta>
            <ta e="T1022" id="Seg_10261" s="T1021">dĭ</ta>
            <ta e="T1023" id="Seg_10262" s="T1022">măm-bi</ta>
            <ta e="T1024" id="Seg_10263" s="T1023">šindi-m=də</ta>
            <ta e="T1025" id="Seg_10264" s="T1024">e-m</ta>
            <ta e="T1026" id="Seg_10265" s="T1025">ej</ta>
            <ta e="T1027" id="Seg_10266" s="T1026">i-lə-m</ta>
            <ta e="T1029" id="Seg_10267" s="T1028">măn</ta>
            <ta e="T1030" id="Seg_10268" s="T1029">tenə-bie-m</ta>
            <ta e="T1031" id="Seg_10269" s="T1030">išo</ta>
            <ta e="T1032" id="Seg_10270" s="T1031">šində</ta>
            <ta e="T1033" id="Seg_10271" s="T1032">ka-lə-j</ta>
            <ta e="T1035" id="Seg_10272" s="T1034">dĭgəttə</ta>
            <ta e="T1036" id="Seg_10273" s="T1035">nagur-göʔ</ta>
            <ta e="T1037" id="Seg_10274" s="T1036">kam-bi-baʔ</ta>
            <ta e="T1074" id="Seg_10275" s="T1073">dĭgəttə</ta>
            <ta e="T1075" id="Seg_10276" s="T1074">dĭʔ-nə</ta>
            <ta e="T1076" id="Seg_10277" s="T1075">măn</ta>
            <ta e="T1077" id="Seg_10278" s="T1076">măm-bia-l</ta>
            <ta e="T1078" id="Seg_10279" s="T1077">măn</ta>
            <ta e="T1079" id="Seg_10280" s="T1078">giber</ta>
            <ta e="T1080" id="Seg_10281" s="T1079">kandə-ga-l</ta>
            <ta e="T1081" id="Seg_10282" s="T1080">măn</ta>
            <ta e="T1082" id="Seg_10283" s="T1081">măn-də-m</ta>
            <ta e="T1083" id="Seg_10284" s="T1082">kudaj-də</ta>
            <ta e="T1084" id="Seg_10285" s="T1083">üzə-sʼtə</ta>
            <ta e="T1086" id="Seg_10286" s="T1085">nu</ta>
            <ta e="T1087" id="Seg_10287" s="T1086">karəlʼdʼan</ta>
            <ta e="T1088" id="Seg_10288" s="T1087">măn-zi</ta>
            <ta e="T1089" id="Seg_10289" s="T1088">ka-la-l</ta>
            <ta e="T1091" id="Seg_10290" s="T1090">dăre</ta>
            <ta e="T1092" id="Seg_10291" s="T1091">nörbə-bi</ta>
            <ta e="T1093" id="Seg_10292" s="T1092">măna</ta>
            <ta e="T1095" id="Seg_10293" s="T1094">а</ta>
            <ta e="T1140" id="Seg_10294" s="T1139">šide</ta>
            <ta e="T1141" id="Seg_10295" s="T1140">kö</ta>
            <ta e="T1146" id="Seg_10296" s="T1144">šide</ta>
            <ta e="T1148" id="Seg_10297" s="T1146">kö</ta>
            <ta e="T1178" id="Seg_10298" s="T1177">miʔ</ta>
            <ta e="T1179" id="Seg_10299" s="T1178">brigadir</ta>
            <ta e="T1180" id="Seg_10300" s="T1179">i-bi</ta>
            <ta e="T1181" id="Seg_10301" s="T1180">nʼemec</ta>
            <ta e="T1183" id="Seg_10302" s="T1182">dĭ</ta>
            <ta e="T1184" id="Seg_10303" s="T1183">ugandə</ta>
            <ta e="T1186" id="Seg_10304" s="T1185">il-də</ta>
            <ta e="T1187" id="Seg_10305" s="T1186">ine-ʔi</ta>
            <ta e="T1188" id="Seg_10306" s="T1187">mĭ-bi</ta>
            <ta e="T1190" id="Seg_10307" s="T1189">tüj</ta>
            <ta e="T1191" id="Seg_10308" s="T1190">dĭ</ta>
            <ta e="T1739" id="Seg_10309" s="T1191">kal-la</ta>
            <ta e="T1192" id="Seg_10310" s="T1739">dʼür-bi</ta>
            <ta e="T1193" id="Seg_10311" s="T1192">Vălgăgrat-tə</ta>
            <ta e="T1195" id="Seg_10312" s="T1194">a</ta>
            <ta e="T1196" id="Seg_10313" s="T1195">kamen</ta>
            <ta e="T1197" id="Seg_10314" s="T1196">šo-bi</ta>
            <ta e="T1198" id="Seg_10315" s="T1197">dĭ</ta>
            <ta e="T1199" id="Seg_10316" s="T1198">baška</ta>
            <ta e="T1200" id="Seg_10317" s="T1199">predsʼedatʼelʼ</ta>
            <ta e="T1201" id="Seg_10318" s="T1200">miʔnʼibeʔ</ta>
            <ta e="T1202" id="Seg_10319" s="T1201">dĭ</ta>
            <ta e="T1203" id="Seg_10320" s="T1202">il-də</ta>
            <ta e="T1204" id="Seg_10321" s="T1203">ine</ta>
            <ta e="T1205" id="Seg_10322" s="T1204">mĭ-bi-ʔi</ta>
            <ta e="T1206" id="Seg_10323" s="T1205">mĭ-bi</ta>
            <ta e="T1208" id="Seg_10324" s="T1207">šindi-m=də</ta>
            <ta e="T1209" id="Seg_10325" s="T1208">ej</ta>
            <ta e="T1210" id="Seg_10326" s="T1209">keʔbde-j-leʔ</ta>
            <ta e="T1211" id="Seg_10327" s="T1210">kam-bi-ʔi</ta>
            <ta e="T1212" id="Seg_10328" s="T1211">ine-zi</ta>
            <ta e="T1213" id="Seg_10329" s="T1212">mĭ-bi</ta>
            <ta e="T1214" id="Seg_10330" s="T1213">tože</ta>
            <ta e="T1216" id="Seg_10331" s="T1215">dĭ</ta>
            <ta e="T1217" id="Seg_10332" s="T1216">predsʼedatʼelʼ</ta>
            <ta e="T1218" id="Seg_10333" s="T1217">tüj</ta>
            <ta e="T1219" id="Seg_10334" s="T1218">miʔnʼibeʔ</ta>
            <ta e="T1220" id="Seg_10335" s="T1219">Văznʼesenka-nə</ta>
            <ta e="T1221" id="Seg_10336" s="T1220">kam-bi</ta>
            <ta e="T1222" id="Seg_10337" s="T1221">dĭn</ta>
            <ta e="T1223" id="Seg_10338" s="T1222">amno-laʔbə</ta>
            <ta e="T1225" id="Seg_10339" s="T1224">dĭʔ-nə</ta>
            <ta e="T1226" id="Seg_10340" s="T1225">tura</ta>
            <ta e="T1227" id="Seg_10341" s="T1226">nuldə-bi-ʔi</ta>
            <ta e="T1228" id="Seg_10342" s="T1227">tura-t</ta>
            <ta e="T1229" id="Seg_10343" s="T1228">tüj</ta>
            <ta e="T1230" id="Seg_10344" s="T1229">nu-laʔbə</ta>
            <ta e="T1231" id="Seg_10345" s="T1230">šindi=də</ta>
            <ta e="T1232" id="Seg_10346" s="T1231">ej</ta>
            <ta e="T1233" id="Seg_10347" s="T1232">amno-lia</ta>
            <ta e="T1234" id="Seg_10348" s="T1233">dĭ</ta>
            <ta e="T1235" id="Seg_10349" s="T1234">tura-gən</ta>
            <ta e="T1237" id="Seg_10350" s="T1236">kabarləj</ta>
            <ta e="T1266" id="Seg_10351" s="T1265">а</ta>
            <ta e="T1382" id="Seg_10352" s="T1380">а</ta>
            <ta e="T1506" id="Seg_10353" s="T1504">а</ta>
            <ta e="T1524" id="Seg_10354" s="T1523">а</ta>
            <ta e="T1579" id="Seg_10355" s="T1578">а</ta>
            <ta e="T1653" id="Seg_10356" s="T1652">а</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T50" id="Seg_10357" s="T49">a</ta>
            <ta e="T100" id="Seg_10358" s="T99">a</ta>
            <ta e="T148" id="Seg_10359" s="T147">a</ta>
            <ta e="T163" id="Seg_10360" s="T162">a</ta>
            <ta e="T168" id="Seg_10361" s="T167">a</ta>
            <ta e="T186" id="Seg_10362" s="T185">Paša</ta>
            <ta e="T187" id="Seg_10363" s="T186">Vasilinič</ta>
            <ta e="T188" id="Seg_10364" s="T187">bos-də</ta>
            <ta e="T189" id="Seg_10365" s="T188">Šura-Tə</ta>
            <ta e="T190" id="Seg_10366" s="T189">baʔbdə-luʔbdə-bi</ta>
            <ta e="T191" id="Seg_10367" s="T190">i</ta>
            <ta e="T192" id="Seg_10368" s="T191">baška</ta>
            <ta e="T193" id="Seg_10369" s="T192">ne</ta>
            <ta e="T194" id="Seg_10370" s="T193">i-bi</ta>
            <ta e="T196" id="Seg_10371" s="T195">dĭgəttə</ta>
            <ta e="T197" id="Seg_10372" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_10373" s="T197">dĭ-Tə</ta>
            <ta e="T200" id="Seg_10374" s="T199">sut-Tə</ta>
            <ta e="T201" id="Seg_10375" s="T200">dĭ</ta>
            <ta e="T203" id="Seg_10376" s="T202">dĭ-zAŋ</ta>
            <ta e="T206" id="Seg_10377" s="T205">dĭ</ta>
            <ta e="T208" id="Seg_10378" s="T207">tüj</ta>
            <ta e="T209" id="Seg_10379" s="T208">aktʼa</ta>
            <ta e="T210" id="Seg_10380" s="T209">ešši-zAŋ-Tə</ta>
            <ta e="T211" id="Seg_10381" s="T210">mĭ-laʔbə</ta>
            <ta e="T213" id="Seg_10382" s="T212">a</ta>
            <ta e="T214" id="Seg_10383" s="T213">dĭ</ta>
            <ta e="T1738" id="Seg_10384" s="T214">kan-lAʔ</ta>
            <ta e="T215" id="Seg_10385" s="T1738">tʼür-bi</ta>
            <ta e="T216" id="Seg_10386" s="T215">gijen</ta>
            <ta e="T217" id="Seg_10387" s="T216">Lenin</ta>
            <ta e="T218" id="Seg_10388" s="T217">amno-laʔbə-bi</ta>
            <ta e="T220" id="Seg_10389" s="T219">dĭn</ta>
            <ta e="T221" id="Seg_10390" s="T220">amno-laʔbə</ta>
            <ta e="T222" id="Seg_10391" s="T221">dĭ</ta>
            <ta e="T223" id="Seg_10392" s="T222">tüj</ta>
            <ta e="T225" id="Seg_10393" s="T224">šide</ta>
            <ta e="T226" id="Seg_10394" s="T225">nʼi</ta>
            <ta e="T227" id="Seg_10395" s="T226">i</ta>
            <ta e="T229" id="Seg_10396" s="T228">koʔbdo</ta>
            <ta e="T230" id="Seg_10397" s="T229">dĭ-ziʔ</ta>
            <ta e="T231" id="Seg_10398" s="T230">onʼiʔ</ta>
            <ta e="T232" id="Seg_10399" s="T231">nʼi</ta>
            <ta e="T233" id="Seg_10400" s="T232">dĭ</ta>
            <ta e="T234" id="Seg_10401" s="T233">dĭn</ta>
            <ta e="T236" id="Seg_10402" s="T235">dĭgəttə</ta>
            <ta e="T238" id="Seg_10403" s="T237">măn</ta>
            <ta e="T239" id="Seg_10404" s="T238">koʔbdo-m</ta>
            <ta e="T240" id="Seg_10405" s="T239">šo-bi</ta>
            <ta e="T242" id="Seg_10406" s="T241">onʼiʔ</ta>
            <ta e="T243" id="Seg_10407" s="T242">nʼi-bə</ta>
            <ta e="T244" id="Seg_10408" s="T243">det-bi</ta>
            <ta e="T246" id="Seg_10409" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_10410" s="T246">măn-ntə</ta>
            <ta e="T248" id="Seg_10411" s="T247">Paša-Tə</ta>
            <ta e="T249" id="Seg_10412" s="T248">e-ʔ</ta>
            <ta e="T250" id="Seg_10413" s="T249">e-ʔ</ta>
            <ta e="T251" id="Seg_10414" s="T250">šide</ta>
            <ta e="T252" id="Seg_10415" s="T251">nʼi</ta>
            <ta e="T254" id="Seg_10416" s="T253">ej</ta>
            <ta e="T255" id="Seg_10417" s="T254">kereʔ</ta>
            <ta e="T257" id="Seg_10418" s="T256">nʼebosʼ</ta>
            <ta e="T258" id="Seg_10419" s="T257">dĭ-m</ta>
            <ta e="T259" id="Seg_10420" s="T258">i-bi</ta>
            <ta e="T260" id="Seg_10421" s="T259">a</ta>
            <ta e="T261" id="Seg_10422" s="T260">ešši-zAŋ-Tə</ta>
            <ta e="T262" id="Seg_10423" s="T261">ej</ta>
            <ta e="T263" id="Seg_10424" s="T262">kereʔ</ta>
            <ta e="T463" id="Seg_10425" s="T462">a</ta>
            <ta e="T552" id="Seg_10426" s="T550">a</ta>
            <ta e="T737" id="Seg_10427" s="T736">a</ta>
            <ta e="T752" id="Seg_10428" s="T751">a</ta>
            <ta e="T768" id="Seg_10429" s="T767">a</ta>
            <ta e="T782" id="Seg_10430" s="T781">a</ta>
            <ta e="T812" id="Seg_10431" s="T811">a</ta>
            <ta e="T828" id="Seg_10432" s="T827">a</ta>
            <ta e="T971" id="Seg_10433" s="T970">miʔ</ta>
            <ta e="T972" id="Seg_10434" s="T971">kü-laːm-bi</ta>
            <ta e="T973" id="Seg_10435" s="T972">Zagorodnʼuk</ta>
            <ta e="T974" id="Seg_10436" s="T973">uže</ta>
            <ta e="T976" id="Seg_10437" s="T975">šide</ta>
            <ta e="T977" id="Seg_10438" s="T976">kö</ta>
            <ta e="T979" id="Seg_10439" s="T978">kamən</ta>
            <ta e="T980" id="Seg_10440" s="T979">măn</ta>
            <ta e="T981" id="Seg_10441" s="T980">gibər</ta>
            <ta e="T982" id="Seg_10442" s="T981">kan-lV-m</ta>
            <ta e="T983" id="Seg_10443" s="T982">măn-lV-m</ta>
            <ta e="T984" id="Seg_10444" s="T983">dĭ-Tə</ta>
            <ta e="T986" id="Seg_10445" s="T985">kan-lV-l</ta>
            <ta e="T988" id="Seg_10446" s="T987">nu</ta>
            <ta e="T989" id="Seg_10447" s="T988">šo-ʔ</ta>
            <ta e="T991" id="Seg_10448" s="T990">măn</ta>
            <ta e="T992" id="Seg_10449" s="T991">măn</ta>
            <ta e="T993" id="Seg_10450" s="T992">šo-bi-m</ta>
            <ta e="T995" id="Seg_10451" s="T994">dĭgəttə</ta>
            <ta e="T996" id="Seg_10452" s="T995">dĭ</ta>
            <ta e="T997" id="Seg_10453" s="T996">măn-ntə</ta>
            <ta e="T998" id="Seg_10454" s="T997">tăn</ta>
            <ta e="T999" id="Seg_10455" s="T998">šo-bi-l</ta>
            <ta e="T1001" id="Seg_10456" s="T1000">a</ta>
            <ta e="T1002" id="Seg_10457" s="T1001">măn</ta>
            <ta e="T1003" id="Seg_10458" s="T1002">öʔlu-zittə</ta>
            <ta e="T1004" id="Seg_10459" s="T1003">xatʼel</ta>
            <ta e="T1005" id="Seg_10460" s="T1004">bos-də</ta>
            <ta e="T1006" id="Seg_10461" s="T1005">ne-ŋ</ta>
            <ta e="T1007" id="Seg_10462" s="T1006">tăn-ziʔ</ta>
            <ta e="T1009" id="Seg_10463" s="T1008">dĭgəttə</ta>
            <ta e="T1010" id="Seg_10464" s="T1009">amnə-l-bi-bAʔ</ta>
            <ta e="T1011" id="Seg_10465" s="T1010">kan-bi-bAʔ</ta>
            <ta e="T1012" id="Seg_10466" s="T1011">kăntora-Tə</ta>
            <ta e="T1014" id="Seg_10467" s="T1013">dĭ</ta>
            <ta e="T1016" id="Seg_10468" s="T1015">baška</ta>
            <ta e="T1017" id="Seg_10469" s="T1016">nüke</ta>
            <ta e="T1018" id="Seg_10470" s="T1017">šo-bi</ta>
            <ta e="T1019" id="Seg_10471" s="T1018">măna</ta>
            <ta e="T1020" id="Seg_10472" s="T1019">i-lV-l</ta>
            <ta e="T1021" id="Seg_10473" s="T1020">măn</ta>
            <ta e="T1022" id="Seg_10474" s="T1021">dĭ</ta>
            <ta e="T1023" id="Seg_10475" s="T1022">măn-bi</ta>
            <ta e="T1024" id="Seg_10476" s="T1023">šində-m=də</ta>
            <ta e="T1025" id="Seg_10477" s="T1024">e-m</ta>
            <ta e="T1026" id="Seg_10478" s="T1025">ej</ta>
            <ta e="T1027" id="Seg_10479" s="T1026">i-lV-m</ta>
            <ta e="T1029" id="Seg_10480" s="T1028">măn</ta>
            <ta e="T1030" id="Seg_10481" s="T1029">tene-bi-m</ta>
            <ta e="T1031" id="Seg_10482" s="T1030">ĭššo</ta>
            <ta e="T1032" id="Seg_10483" s="T1031">šində</ta>
            <ta e="T1033" id="Seg_10484" s="T1032">kan-lV-j</ta>
            <ta e="T1035" id="Seg_10485" s="T1034">dĭgəttə</ta>
            <ta e="T1036" id="Seg_10486" s="T1035">nagur-göʔ</ta>
            <ta e="T1037" id="Seg_10487" s="T1036">kan-bi-bAʔ</ta>
            <ta e="T1074" id="Seg_10488" s="T1073">dĭgəttə</ta>
            <ta e="T1075" id="Seg_10489" s="T1074">dĭ-Tə</ta>
            <ta e="T1076" id="Seg_10490" s="T1075">măn</ta>
            <ta e="T1077" id="Seg_10491" s="T1076">măn-bi-l</ta>
            <ta e="T1078" id="Seg_10492" s="T1077">măn</ta>
            <ta e="T1079" id="Seg_10493" s="T1078">gibər</ta>
            <ta e="T1080" id="Seg_10494" s="T1079">kandə-gA-l</ta>
            <ta e="T1081" id="Seg_10495" s="T1080">măn</ta>
            <ta e="T1082" id="Seg_10496" s="T1081">măn-ntə-m</ta>
            <ta e="T1083" id="Seg_10497" s="T1082">kudaj-Tə</ta>
            <ta e="T1084" id="Seg_10498" s="T1083">üzə-zittə</ta>
            <ta e="T1086" id="Seg_10499" s="T1085">nu</ta>
            <ta e="T1087" id="Seg_10500" s="T1086">karəldʼaːn</ta>
            <ta e="T1088" id="Seg_10501" s="T1087">măn-ziʔ</ta>
            <ta e="T1089" id="Seg_10502" s="T1088">kan-lV-l</ta>
            <ta e="T1091" id="Seg_10503" s="T1090">dărəʔ</ta>
            <ta e="T1092" id="Seg_10504" s="T1091">nörbə-bi</ta>
            <ta e="T1093" id="Seg_10505" s="T1092">măna</ta>
            <ta e="T1095" id="Seg_10506" s="T1094">a</ta>
            <ta e="T1140" id="Seg_10507" s="T1139">šide</ta>
            <ta e="T1141" id="Seg_10508" s="T1140">kö</ta>
            <ta e="T1146" id="Seg_10509" s="T1144">šide</ta>
            <ta e="T1148" id="Seg_10510" s="T1146">kö</ta>
            <ta e="T1178" id="Seg_10511" s="T1177">miʔ</ta>
            <ta e="T1179" id="Seg_10512" s="T1178">brigadir</ta>
            <ta e="T1180" id="Seg_10513" s="T1179">i-bi</ta>
            <ta e="T1181" id="Seg_10514" s="T1180">nʼemec</ta>
            <ta e="T1183" id="Seg_10515" s="T1182">dĭ</ta>
            <ta e="T1184" id="Seg_10516" s="T1183">ugaːndə</ta>
            <ta e="T1186" id="Seg_10517" s="T1185">il-də</ta>
            <ta e="T1187" id="Seg_10518" s="T1186">ine-jəʔ</ta>
            <ta e="T1188" id="Seg_10519" s="T1187">mĭ-bi</ta>
            <ta e="T1190" id="Seg_10520" s="T1189">tüj</ta>
            <ta e="T1191" id="Seg_10521" s="T1190">dĭ</ta>
            <ta e="T1739" id="Seg_10522" s="T1191">kan-lAʔ</ta>
            <ta e="T1192" id="Seg_10523" s="T1739">tʼür-bi</ta>
            <ta e="T1193" id="Seg_10524" s="T1192">Vălgăgrat-Tə</ta>
            <ta e="T1195" id="Seg_10525" s="T1194">a</ta>
            <ta e="T1196" id="Seg_10526" s="T1195">kamən</ta>
            <ta e="T1197" id="Seg_10527" s="T1196">šo-bi</ta>
            <ta e="T1198" id="Seg_10528" s="T1197">dĭ</ta>
            <ta e="T1199" id="Seg_10529" s="T1198">baška</ta>
            <ta e="T1200" id="Seg_10530" s="T1199">predsʼedatʼelʼ</ta>
            <ta e="T1201" id="Seg_10531" s="T1200">miʔnʼibeʔ</ta>
            <ta e="T1202" id="Seg_10532" s="T1201">dĭ</ta>
            <ta e="T1203" id="Seg_10533" s="T1202">il-də</ta>
            <ta e="T1204" id="Seg_10534" s="T1203">ine</ta>
            <ta e="T1205" id="Seg_10535" s="T1204">mĭ-bi-jəʔ</ta>
            <ta e="T1206" id="Seg_10536" s="T1205">mĭ-bi</ta>
            <ta e="T1208" id="Seg_10537" s="T1207">šində-m=də</ta>
            <ta e="T1209" id="Seg_10538" s="T1208">ej</ta>
            <ta e="T1210" id="Seg_10539" s="T1209">keʔbde-j-lAʔ</ta>
            <ta e="T1211" id="Seg_10540" s="T1210">kan-bi-jəʔ</ta>
            <ta e="T1212" id="Seg_10541" s="T1211">ine-ziʔ</ta>
            <ta e="T1213" id="Seg_10542" s="T1212">mĭ-bi</ta>
            <ta e="T1214" id="Seg_10543" s="T1213">tože</ta>
            <ta e="T1216" id="Seg_10544" s="T1215">dĭ</ta>
            <ta e="T1217" id="Seg_10545" s="T1216">predsʼedatʼelʼ</ta>
            <ta e="T1218" id="Seg_10546" s="T1217">tüj</ta>
            <ta e="T1219" id="Seg_10547" s="T1218">miʔnʼibeʔ</ta>
            <ta e="T1220" id="Seg_10548" s="T1219">Văznesenka-Tə</ta>
            <ta e="T1221" id="Seg_10549" s="T1220">kan-bi</ta>
            <ta e="T1222" id="Seg_10550" s="T1221">dĭn</ta>
            <ta e="T1223" id="Seg_10551" s="T1222">amno-laʔbə</ta>
            <ta e="T1225" id="Seg_10552" s="T1224">dĭ-Tə</ta>
            <ta e="T1226" id="Seg_10553" s="T1225">tura</ta>
            <ta e="T1227" id="Seg_10554" s="T1226">nuldə-bi-jəʔ</ta>
            <ta e="T1228" id="Seg_10555" s="T1227">tura-t</ta>
            <ta e="T1229" id="Seg_10556" s="T1228">tüj</ta>
            <ta e="T1230" id="Seg_10557" s="T1229">nu-laʔbə</ta>
            <ta e="T1231" id="Seg_10558" s="T1230">šində=də</ta>
            <ta e="T1232" id="Seg_10559" s="T1231">ej</ta>
            <ta e="T1233" id="Seg_10560" s="T1232">amno-liA</ta>
            <ta e="T1234" id="Seg_10561" s="T1233">dĭ</ta>
            <ta e="T1235" id="Seg_10562" s="T1234">tura-Kən</ta>
            <ta e="T1237" id="Seg_10563" s="T1236">kabarləj</ta>
            <ta e="T1266" id="Seg_10564" s="T1265">a</ta>
            <ta e="T1382" id="Seg_10565" s="T1380">a</ta>
            <ta e="T1506" id="Seg_10566" s="T1504">a</ta>
            <ta e="T1524" id="Seg_10567" s="T1523">a</ta>
            <ta e="T1579" id="Seg_10568" s="T1578">a</ta>
            <ta e="T1653" id="Seg_10569" s="T1652">a</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T50" id="Seg_10570" s="T49">and</ta>
            <ta e="T100" id="Seg_10571" s="T99">and</ta>
            <ta e="T148" id="Seg_10572" s="T147">and</ta>
            <ta e="T163" id="Seg_10573" s="T162">and</ta>
            <ta e="T168" id="Seg_10574" s="T167">and</ta>
            <ta e="T186" id="Seg_10575" s="T185">Pasha.[NOM.SG]</ta>
            <ta e="T187" id="Seg_10576" s="T186">Vasilinich.[NOM.SG]</ta>
            <ta e="T188" id="Seg_10577" s="T187">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T189" id="Seg_10578" s="T188">Shura-LAT</ta>
            <ta e="T190" id="Seg_10579" s="T189">throw-MOM-PST.[3SG]</ta>
            <ta e="T191" id="Seg_10580" s="T190">and</ta>
            <ta e="T192" id="Seg_10581" s="T191">another.[NOM.SG]</ta>
            <ta e="T193" id="Seg_10582" s="T192">woman.[NOM.SG]</ta>
            <ta e="T194" id="Seg_10583" s="T193">take-PST.[3SG]</ta>
            <ta e="T196" id="Seg_10584" s="T195">then</ta>
            <ta e="T197" id="Seg_10585" s="T196">this.[NOM.SG]</ta>
            <ta e="T198" id="Seg_10586" s="T197">this-LAT</ta>
            <ta e="T200" id="Seg_10587" s="T199">judgment-LAT</ta>
            <ta e="T201" id="Seg_10588" s="T200">this.[NOM.SG]</ta>
            <ta e="T203" id="Seg_10589" s="T202">this-PL</ta>
            <ta e="T206" id="Seg_10590" s="T205">this.[NOM.SG]</ta>
            <ta e="T208" id="Seg_10591" s="T207">now</ta>
            <ta e="T209" id="Seg_10592" s="T208">money.[NOM.SG]</ta>
            <ta e="T210" id="Seg_10593" s="T209">child-PL-LAT</ta>
            <ta e="T211" id="Seg_10594" s="T210">give-DUR.[3SG]</ta>
            <ta e="T213" id="Seg_10595" s="T212">and</ta>
            <ta e="T214" id="Seg_10596" s="T213">this.[NOM.SG]</ta>
            <ta e="T1738" id="Seg_10597" s="T214">go-CVB</ta>
            <ta e="T215" id="Seg_10598" s="T1738">disappear-PST.[3SG]</ta>
            <ta e="T216" id="Seg_10599" s="T215">where</ta>
            <ta e="T217" id="Seg_10600" s="T216">Lenin</ta>
            <ta e="T218" id="Seg_10601" s="T217">live-DUR-PST.[3SG]</ta>
            <ta e="T220" id="Seg_10602" s="T219">there</ta>
            <ta e="T221" id="Seg_10603" s="T220">live-DUR.[3SG]</ta>
            <ta e="T222" id="Seg_10604" s="T221">this.[NOM.SG]</ta>
            <ta e="T223" id="Seg_10605" s="T222">now</ta>
            <ta e="T225" id="Seg_10606" s="T224">two.[NOM.SG]</ta>
            <ta e="T226" id="Seg_10607" s="T225">boy.[NOM.SG]</ta>
            <ta e="T227" id="Seg_10608" s="T226">and</ta>
            <ta e="T229" id="Seg_10609" s="T228">girl.[NOM.SG]</ta>
            <ta e="T230" id="Seg_10610" s="T229">this-COM</ta>
            <ta e="T231" id="Seg_10611" s="T230">one.[NOM.SG]</ta>
            <ta e="T232" id="Seg_10612" s="T231">boy.[NOM.SG]</ta>
            <ta e="T233" id="Seg_10613" s="T232">this.[NOM.SG]</ta>
            <ta e="T234" id="Seg_10614" s="T233">there</ta>
            <ta e="T236" id="Seg_10615" s="T235">then</ta>
            <ta e="T238" id="Seg_10616" s="T237">I.GEN</ta>
            <ta e="T239" id="Seg_10617" s="T238">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T240" id="Seg_10618" s="T239">come-PST.[3SG]</ta>
            <ta e="T242" id="Seg_10619" s="T241">one.[NOM.SG]</ta>
            <ta e="T243" id="Seg_10620" s="T242">son-ACC.3SG</ta>
            <ta e="T244" id="Seg_10621" s="T243">bring-PST.[3SG]</ta>
            <ta e="T246" id="Seg_10622" s="T245">this.[NOM.SG]</ta>
            <ta e="T247" id="Seg_10623" s="T246">say-IPFVZ.[3SG]</ta>
            <ta e="T248" id="Seg_10624" s="T247">Pasha-LAT</ta>
            <ta e="T249" id="Seg_10625" s="T248">NEG.AUX-IMP.2SG</ta>
            <ta e="T250" id="Seg_10626" s="T249">NEG.AUX-IMP.2SG</ta>
            <ta e="T251" id="Seg_10627" s="T250">two.[NOM.SG]</ta>
            <ta e="T252" id="Seg_10628" s="T251">boy.[NOM.SG]</ta>
            <ta e="T254" id="Seg_10629" s="T253">NEG</ta>
            <ta e="T255" id="Seg_10630" s="T254">one.needs</ta>
            <ta e="T257" id="Seg_10631" s="T256">must.have.been</ta>
            <ta e="T258" id="Seg_10632" s="T257">this-ACC</ta>
            <ta e="T259" id="Seg_10633" s="T258">take-PST.[3SG]</ta>
            <ta e="T260" id="Seg_10634" s="T259">and</ta>
            <ta e="T261" id="Seg_10635" s="T260">child-PL-LAT</ta>
            <ta e="T262" id="Seg_10636" s="T261">NEG</ta>
            <ta e="T263" id="Seg_10637" s="T262">one.needs</ta>
            <ta e="T463" id="Seg_10638" s="T462">and</ta>
            <ta e="T552" id="Seg_10639" s="T550">and</ta>
            <ta e="T737" id="Seg_10640" s="T736">and</ta>
            <ta e="T752" id="Seg_10641" s="T751">and</ta>
            <ta e="T768" id="Seg_10642" s="T767">and</ta>
            <ta e="T782" id="Seg_10643" s="T781">and</ta>
            <ta e="T812" id="Seg_10644" s="T811">and</ta>
            <ta e="T828" id="Seg_10645" s="T827">and</ta>
            <ta e="T971" id="Seg_10646" s="T970">we.NOM</ta>
            <ta e="T972" id="Seg_10647" s="T971">die-RES-PST.[3SG]</ta>
            <ta e="T973" id="Seg_10648" s="T972">Zagorodnyuk.[NOM.SG]</ta>
            <ta e="T974" id="Seg_10649" s="T973">already</ta>
            <ta e="T976" id="Seg_10650" s="T975">two.[NOM.SG]</ta>
            <ta e="T977" id="Seg_10651" s="T976">winter.[NOM.SG]</ta>
            <ta e="T979" id="Seg_10652" s="T978">when</ta>
            <ta e="T980" id="Seg_10653" s="T979">I.NOM</ta>
            <ta e="T981" id="Seg_10654" s="T980">where.to</ta>
            <ta e="T982" id="Seg_10655" s="T981">go-FUT-1SG</ta>
            <ta e="T983" id="Seg_10656" s="T982">say-FUT-1SG</ta>
            <ta e="T984" id="Seg_10657" s="T983">this-LAT</ta>
            <ta e="T986" id="Seg_10658" s="T985">go-FUT-2SG</ta>
            <ta e="T988" id="Seg_10659" s="T987">well</ta>
            <ta e="T989" id="Seg_10660" s="T988">come-IMP.2SG</ta>
            <ta e="T991" id="Seg_10661" s="T990">I.NOM</ta>
            <ta e="T992" id="Seg_10662" s="T991">I.NOM</ta>
            <ta e="T993" id="Seg_10663" s="T992">come-PST-1SG</ta>
            <ta e="T995" id="Seg_10664" s="T994">then</ta>
            <ta e="T996" id="Seg_10665" s="T995">this.[NOM.SG]</ta>
            <ta e="T997" id="Seg_10666" s="T996">say-IPFVZ.[3SG]</ta>
            <ta e="T998" id="Seg_10667" s="T997">you.NOM</ta>
            <ta e="T999" id="Seg_10668" s="T998">come-PST-2SG</ta>
            <ta e="T1001" id="Seg_10669" s="T1000">and</ta>
            <ta e="T1002" id="Seg_10670" s="T1001">I.NOM</ta>
            <ta e="T1003" id="Seg_10671" s="T1002">send-INF.LAT</ta>
            <ta e="T1004" id="Seg_10672" s="T1003">want.PST.M.SG</ta>
            <ta e="T1005" id="Seg_10673" s="T1004">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1006" id="Seg_10674" s="T1005">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1007" id="Seg_10675" s="T1006">you.NOM-INS</ta>
            <ta e="T1009" id="Seg_10676" s="T1008">then</ta>
            <ta e="T1010" id="Seg_10677" s="T1009">sit.down-FRQ-PST-1PL</ta>
            <ta e="T1011" id="Seg_10678" s="T1010">go-PST-1PL</ta>
            <ta e="T1012" id="Seg_10679" s="T1011">central.office-LAT</ta>
            <ta e="T1014" id="Seg_10680" s="T1013">this.[NOM.SG]</ta>
            <ta e="T1016" id="Seg_10681" s="T1015">another.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_10682" s="T1016">woman.[NOM.SG]</ta>
            <ta e="T1018" id="Seg_10683" s="T1017">come-PST.[3SG]</ta>
            <ta e="T1019" id="Seg_10684" s="T1018">I.ACC</ta>
            <ta e="T1020" id="Seg_10685" s="T1019">take-FUT-2SG</ta>
            <ta e="T1021" id="Seg_10686" s="T1020">I.NOM</ta>
            <ta e="T1022" id="Seg_10687" s="T1021">this</ta>
            <ta e="T1023" id="Seg_10688" s="T1022">say-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_10689" s="T1023">who-ACC=INDEF</ta>
            <ta e="T1025" id="Seg_10690" s="T1024">NEG.AUX-1SG</ta>
            <ta e="T1026" id="Seg_10691" s="T1025">NEG</ta>
            <ta e="T1027" id="Seg_10692" s="T1026">take-FUT-1SG</ta>
            <ta e="T1029" id="Seg_10693" s="T1028">I.NOM</ta>
            <ta e="T1030" id="Seg_10694" s="T1029">think-PST-1SG</ta>
            <ta e="T1031" id="Seg_10695" s="T1030">more</ta>
            <ta e="T1032" id="Seg_10696" s="T1031">who.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_10697" s="T1032">go-FUT-3SG</ta>
            <ta e="T1035" id="Seg_10698" s="T1034">then</ta>
            <ta e="T1036" id="Seg_10699" s="T1035">three-COLL</ta>
            <ta e="T1037" id="Seg_10700" s="T1036">go-PST-1PL</ta>
            <ta e="T1074" id="Seg_10701" s="T1073">then</ta>
            <ta e="T1075" id="Seg_10702" s="T1074">this-LAT</ta>
            <ta e="T1076" id="Seg_10703" s="T1075">I.NOM</ta>
            <ta e="T1077" id="Seg_10704" s="T1076">say-PST-2SG</ta>
            <ta e="T1078" id="Seg_10705" s="T1077">I.NOM</ta>
            <ta e="T1079" id="Seg_10706" s="T1078">where.to</ta>
            <ta e="T1080" id="Seg_10707" s="T1079">walk-PRS-2SG</ta>
            <ta e="T1081" id="Seg_10708" s="T1080">I.NOM</ta>
            <ta e="T1082" id="Seg_10709" s="T1081">say-IPFVZ-1SG</ta>
            <ta e="T1083" id="Seg_10710" s="T1082">God-LAT</ta>
            <ta e="T1084" id="Seg_10711" s="T1083">pray-INF.LAT</ta>
            <ta e="T1086" id="Seg_10712" s="T1085">well</ta>
            <ta e="T1087" id="Seg_10713" s="T1086">tomorrow</ta>
            <ta e="T1088" id="Seg_10714" s="T1087">I.NOM-COM</ta>
            <ta e="T1089" id="Seg_10715" s="T1088">go-FUT-2SG</ta>
            <ta e="T1091" id="Seg_10716" s="T1090">so</ta>
            <ta e="T1092" id="Seg_10717" s="T1091">tell-PST.[3SG]</ta>
            <ta e="T1093" id="Seg_10718" s="T1092">I.LAT</ta>
            <ta e="T1095" id="Seg_10719" s="T1094">and</ta>
            <ta e="T1140" id="Seg_10720" s="T1139">two.[NOM.SG]</ta>
            <ta e="T1141" id="Seg_10721" s="T1140">winter.[NOM.SG]</ta>
            <ta e="T1146" id="Seg_10722" s="T1144">two.[NOM.SG]</ta>
            <ta e="T1148" id="Seg_10723" s="T1146">winter.[NOM.SG]</ta>
            <ta e="T1178" id="Seg_10724" s="T1177">we.NOM</ta>
            <ta e="T1179" id="Seg_10725" s="T1178">foreman.[NOM.SG]</ta>
            <ta e="T1180" id="Seg_10726" s="T1179">be-PST.[3SG]</ta>
            <ta e="T1181" id="Seg_10727" s="T1180">German.[NOM.SG]</ta>
            <ta e="T1183" id="Seg_10728" s="T1182">this.[NOM.SG]</ta>
            <ta e="T1184" id="Seg_10729" s="T1183">very</ta>
            <ta e="T1186" id="Seg_10730" s="T1185">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T1187" id="Seg_10731" s="T1186">horse-PL</ta>
            <ta e="T1188" id="Seg_10732" s="T1187">give-PST.[3SG]</ta>
            <ta e="T1190" id="Seg_10733" s="T1189">now</ta>
            <ta e="T1191" id="Seg_10734" s="T1190">this.[NOM.SG]</ta>
            <ta e="T1739" id="Seg_10735" s="T1191">go-CVB</ta>
            <ta e="T1192" id="Seg_10736" s="T1739">disappear-PST.[3SG]</ta>
            <ta e="T1193" id="Seg_10737" s="T1192">Volgograd-LAT</ta>
            <ta e="T1195" id="Seg_10738" s="T1194">and</ta>
            <ta e="T1196" id="Seg_10739" s="T1195">when</ta>
            <ta e="T1197" id="Seg_10740" s="T1196">come-PST.[3SG]</ta>
            <ta e="T1198" id="Seg_10741" s="T1197">this.[NOM.SG]</ta>
            <ta e="T1199" id="Seg_10742" s="T1198">another.[NOM.SG]</ta>
            <ta e="T1200" id="Seg_10743" s="T1199">chairman.[NOM.SG]</ta>
            <ta e="T1201" id="Seg_10744" s="T1200">we.LAT</ta>
            <ta e="T1202" id="Seg_10745" s="T1201">this.[NOM.SG]</ta>
            <ta e="T1203" id="Seg_10746" s="T1202">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T1204" id="Seg_10747" s="T1203">horse.[NOM.SG]</ta>
            <ta e="T1205" id="Seg_10748" s="T1204">give-PST-3PL</ta>
            <ta e="T1206" id="Seg_10749" s="T1205">give-PST.[3SG]</ta>
            <ta e="T1208" id="Seg_10750" s="T1207">who-ACC=INDEF</ta>
            <ta e="T1209" id="Seg_10751" s="T1208">NEG</ta>
            <ta e="T1210" id="Seg_10752" s="T1209">berry-VBLZ-CVB</ta>
            <ta e="T1211" id="Seg_10753" s="T1210">go-PST-3PL</ta>
            <ta e="T1212" id="Seg_10754" s="T1211">horse-INS</ta>
            <ta e="T1213" id="Seg_10755" s="T1212">give-PST.[3SG]</ta>
            <ta e="T1214" id="Seg_10756" s="T1213">also</ta>
            <ta e="T1216" id="Seg_10757" s="T1215">this.[NOM.SG]</ta>
            <ta e="T1217" id="Seg_10758" s="T1216">chairman.[NOM.SG]</ta>
            <ta e="T1218" id="Seg_10759" s="T1217">now</ta>
            <ta e="T1219" id="Seg_10760" s="T1218">we.LAT</ta>
            <ta e="T1220" id="Seg_10761" s="T1219">Voznesenskoe-LAT</ta>
            <ta e="T1221" id="Seg_10762" s="T1220">go-PST.[3SG]</ta>
            <ta e="T1222" id="Seg_10763" s="T1221">there</ta>
            <ta e="T1223" id="Seg_10764" s="T1222">live-DUR.[3SG]</ta>
            <ta e="T1225" id="Seg_10765" s="T1224">this-LAT</ta>
            <ta e="T1226" id="Seg_10766" s="T1225">house.[NOM.SG]</ta>
            <ta e="T1227" id="Seg_10767" s="T1226">set.up-PST-3PL</ta>
            <ta e="T1228" id="Seg_10768" s="T1227">house-NOM/GEN.3SG</ta>
            <ta e="T1229" id="Seg_10769" s="T1228">now</ta>
            <ta e="T1230" id="Seg_10770" s="T1229">stand-DUR.[3SG]</ta>
            <ta e="T1231" id="Seg_10771" s="T1230">who.[NOM.SG]=INDEF</ta>
            <ta e="T1232" id="Seg_10772" s="T1231">NEG</ta>
            <ta e="T1233" id="Seg_10773" s="T1232">live-PRS.[3SG]</ta>
            <ta e="T1234" id="Seg_10774" s="T1233">this.[NOM.SG]</ta>
            <ta e="T1235" id="Seg_10775" s="T1234">house-LOC</ta>
            <ta e="T1237" id="Seg_10776" s="T1236">enough</ta>
            <ta e="T1266" id="Seg_10777" s="T1265">and</ta>
            <ta e="T1382" id="Seg_10778" s="T1380">and</ta>
            <ta e="T1506" id="Seg_10779" s="T1504">and</ta>
            <ta e="T1524" id="Seg_10780" s="T1523">and</ta>
            <ta e="T1579" id="Seg_10781" s="T1578">and</ta>
            <ta e="T1653" id="Seg_10782" s="T1652">and</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T50" id="Seg_10783" s="T49">а</ta>
            <ta e="T100" id="Seg_10784" s="T99">а</ta>
            <ta e="T148" id="Seg_10785" s="T147">а</ta>
            <ta e="T163" id="Seg_10786" s="T162">а</ta>
            <ta e="T168" id="Seg_10787" s="T167">а</ta>
            <ta e="T186" id="Seg_10788" s="T185">Паша.[NOM.SG]</ta>
            <ta e="T187" id="Seg_10789" s="T186">Василинич.[NOM.SG]</ta>
            <ta e="T188" id="Seg_10790" s="T187">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T189" id="Seg_10791" s="T188">Шура-LAT</ta>
            <ta e="T190" id="Seg_10792" s="T189">бросить-MOM-PST.[3SG]</ta>
            <ta e="T191" id="Seg_10793" s="T190">и</ta>
            <ta e="T192" id="Seg_10794" s="T191">другой.[NOM.SG]</ta>
            <ta e="T193" id="Seg_10795" s="T192">женщина.[NOM.SG]</ta>
            <ta e="T194" id="Seg_10796" s="T193">взять-PST.[3SG]</ta>
            <ta e="T196" id="Seg_10797" s="T195">тогда</ta>
            <ta e="T197" id="Seg_10798" s="T196">этот.[NOM.SG]</ta>
            <ta e="T198" id="Seg_10799" s="T197">этот-LAT</ta>
            <ta e="T200" id="Seg_10800" s="T199">суд-LAT</ta>
            <ta e="T201" id="Seg_10801" s="T200">этот.[NOM.SG]</ta>
            <ta e="T203" id="Seg_10802" s="T202">этот-PL</ta>
            <ta e="T206" id="Seg_10803" s="T205">этот.[NOM.SG]</ta>
            <ta e="T208" id="Seg_10804" s="T207">сейчас</ta>
            <ta e="T209" id="Seg_10805" s="T208">деньги.[NOM.SG]</ta>
            <ta e="T210" id="Seg_10806" s="T209">ребенок-PL-LAT</ta>
            <ta e="T211" id="Seg_10807" s="T210">дать-DUR.[3SG]</ta>
            <ta e="T213" id="Seg_10808" s="T212">а</ta>
            <ta e="T214" id="Seg_10809" s="T213">этот.[NOM.SG]</ta>
            <ta e="T1738" id="Seg_10810" s="T214">пойти-CVB</ta>
            <ta e="T215" id="Seg_10811" s="T1738">исчезнуть-PST.[3SG]</ta>
            <ta e="T216" id="Seg_10812" s="T215">где</ta>
            <ta e="T217" id="Seg_10813" s="T216">Ленин</ta>
            <ta e="T218" id="Seg_10814" s="T217">жить-DUR-PST.[3SG]</ta>
            <ta e="T220" id="Seg_10815" s="T219">там</ta>
            <ta e="T221" id="Seg_10816" s="T220">жить-DUR.[3SG]</ta>
            <ta e="T222" id="Seg_10817" s="T221">этот.[NOM.SG]</ta>
            <ta e="T223" id="Seg_10818" s="T222">сейчас</ta>
            <ta e="T225" id="Seg_10819" s="T224">два.[NOM.SG]</ta>
            <ta e="T226" id="Seg_10820" s="T225">мальчик.[NOM.SG]</ta>
            <ta e="T227" id="Seg_10821" s="T226">и</ta>
            <ta e="T229" id="Seg_10822" s="T228">девушка.[NOM.SG]</ta>
            <ta e="T230" id="Seg_10823" s="T229">этот-COM</ta>
            <ta e="T231" id="Seg_10824" s="T230">один.[NOM.SG]</ta>
            <ta e="T232" id="Seg_10825" s="T231">мальчик.[NOM.SG]</ta>
            <ta e="T233" id="Seg_10826" s="T232">этот.[NOM.SG]</ta>
            <ta e="T234" id="Seg_10827" s="T233">там</ta>
            <ta e="T236" id="Seg_10828" s="T235">тогда</ta>
            <ta e="T238" id="Seg_10829" s="T237">я.GEN</ta>
            <ta e="T239" id="Seg_10830" s="T238">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T240" id="Seg_10831" s="T239">прийти-PST.[3SG]</ta>
            <ta e="T242" id="Seg_10832" s="T241">один.[NOM.SG]</ta>
            <ta e="T243" id="Seg_10833" s="T242">сын-ACC.3SG</ta>
            <ta e="T244" id="Seg_10834" s="T243">принести-PST.[3SG]</ta>
            <ta e="T246" id="Seg_10835" s="T245">этот.[NOM.SG]</ta>
            <ta e="T247" id="Seg_10836" s="T246">сказать-IPFVZ.[3SG]</ta>
            <ta e="T248" id="Seg_10837" s="T247">Паша-LAT</ta>
            <ta e="T249" id="Seg_10838" s="T248">NEG.AUX-IMP.2SG</ta>
            <ta e="T250" id="Seg_10839" s="T249">NEG.AUX-IMP.2SG</ta>
            <ta e="T251" id="Seg_10840" s="T250">два.[NOM.SG]</ta>
            <ta e="T252" id="Seg_10841" s="T251">мальчик.[NOM.SG]</ta>
            <ta e="T254" id="Seg_10842" s="T253">NEG</ta>
            <ta e="T255" id="Seg_10843" s="T254">нужно</ta>
            <ta e="T257" id="Seg_10844" s="T256">небось</ta>
            <ta e="T258" id="Seg_10845" s="T257">этот-ACC</ta>
            <ta e="T259" id="Seg_10846" s="T258">взять-PST.[3SG]</ta>
            <ta e="T260" id="Seg_10847" s="T259">а</ta>
            <ta e="T261" id="Seg_10848" s="T260">ребенок-PL-LAT</ta>
            <ta e="T262" id="Seg_10849" s="T261">NEG</ta>
            <ta e="T263" id="Seg_10850" s="T262">нужно</ta>
            <ta e="T463" id="Seg_10851" s="T462">а</ta>
            <ta e="T552" id="Seg_10852" s="T550">а</ta>
            <ta e="T737" id="Seg_10853" s="T736">а</ta>
            <ta e="T752" id="Seg_10854" s="T751">а</ta>
            <ta e="T768" id="Seg_10855" s="T767">а</ta>
            <ta e="T782" id="Seg_10856" s="T781">а</ta>
            <ta e="T812" id="Seg_10857" s="T811">а</ta>
            <ta e="T828" id="Seg_10858" s="T827">а</ta>
            <ta e="T971" id="Seg_10859" s="T970">мы.NOM</ta>
            <ta e="T972" id="Seg_10860" s="T971">умереть-RES-PST.[3SG]</ta>
            <ta e="T973" id="Seg_10861" s="T972">Загороднюк.[NOM.SG]</ta>
            <ta e="T974" id="Seg_10862" s="T973">уже</ta>
            <ta e="T976" id="Seg_10863" s="T975">два.[NOM.SG]</ta>
            <ta e="T977" id="Seg_10864" s="T976">зима.[NOM.SG]</ta>
            <ta e="T979" id="Seg_10865" s="T978">когда</ta>
            <ta e="T980" id="Seg_10866" s="T979">я.NOM</ta>
            <ta e="T981" id="Seg_10867" s="T980">куда</ta>
            <ta e="T982" id="Seg_10868" s="T981">пойти-FUT-1SG</ta>
            <ta e="T983" id="Seg_10869" s="T982">сказать-FUT-1SG</ta>
            <ta e="T984" id="Seg_10870" s="T983">этот-LAT</ta>
            <ta e="T986" id="Seg_10871" s="T985">пойти-FUT-2SG</ta>
            <ta e="T988" id="Seg_10872" s="T987">ну</ta>
            <ta e="T989" id="Seg_10873" s="T988">прийти-IMP.2SG</ta>
            <ta e="T991" id="Seg_10874" s="T990">я.NOM</ta>
            <ta e="T992" id="Seg_10875" s="T991">я.NOM</ta>
            <ta e="T993" id="Seg_10876" s="T992">прийти-PST-1SG</ta>
            <ta e="T995" id="Seg_10877" s="T994">тогда</ta>
            <ta e="T996" id="Seg_10878" s="T995">этот.[NOM.SG]</ta>
            <ta e="T997" id="Seg_10879" s="T996">сказать-IPFVZ.[3SG]</ta>
            <ta e="T998" id="Seg_10880" s="T997">ты.NOM</ta>
            <ta e="T999" id="Seg_10881" s="T998">прийти-PST-2SG</ta>
            <ta e="T1001" id="Seg_10882" s="T1000">а</ta>
            <ta e="T1002" id="Seg_10883" s="T1001">я.NOM</ta>
            <ta e="T1003" id="Seg_10884" s="T1002">посылать-INF.LAT</ta>
            <ta e="T1004" id="Seg_10885" s="T1003">хотеть.PST.M.SG</ta>
            <ta e="T1005" id="Seg_10886" s="T1004">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1006" id="Seg_10887" s="T1005">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1007" id="Seg_10888" s="T1006">ты.NOM-INS</ta>
            <ta e="T1009" id="Seg_10889" s="T1008">тогда</ta>
            <ta e="T1010" id="Seg_10890" s="T1009">сесть-FRQ-PST-1PL</ta>
            <ta e="T1011" id="Seg_10891" s="T1010">пойти-PST-1PL</ta>
            <ta e="T1012" id="Seg_10892" s="T1011">контора-LAT</ta>
            <ta e="T1014" id="Seg_10893" s="T1013">этот.[NOM.SG]</ta>
            <ta e="T1016" id="Seg_10894" s="T1015">другой.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_10895" s="T1016">женщина.[NOM.SG]</ta>
            <ta e="T1018" id="Seg_10896" s="T1017">прийти-PST.[3SG]</ta>
            <ta e="T1019" id="Seg_10897" s="T1018">я.ACC</ta>
            <ta e="T1020" id="Seg_10898" s="T1019">взять-FUT-2SG</ta>
            <ta e="T1021" id="Seg_10899" s="T1020">я.NOM</ta>
            <ta e="T1022" id="Seg_10900" s="T1021">этот</ta>
            <ta e="T1023" id="Seg_10901" s="T1022">сказать-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_10902" s="T1023">кто-ACC=INDEF</ta>
            <ta e="T1025" id="Seg_10903" s="T1024">NEG.AUX-1SG</ta>
            <ta e="T1026" id="Seg_10904" s="T1025">NEG</ta>
            <ta e="T1027" id="Seg_10905" s="T1026">взять-FUT-1SG</ta>
            <ta e="T1029" id="Seg_10906" s="T1028">я.NOM</ta>
            <ta e="T1030" id="Seg_10907" s="T1029">думать-PST-1SG</ta>
            <ta e="T1031" id="Seg_10908" s="T1030">еще</ta>
            <ta e="T1032" id="Seg_10909" s="T1031">кто.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_10910" s="T1032">пойти-FUT-3SG</ta>
            <ta e="T1035" id="Seg_10911" s="T1034">тогда</ta>
            <ta e="T1036" id="Seg_10912" s="T1035">три-COLL</ta>
            <ta e="T1037" id="Seg_10913" s="T1036">пойти-PST-1PL</ta>
            <ta e="T1074" id="Seg_10914" s="T1073">тогда</ta>
            <ta e="T1075" id="Seg_10915" s="T1074">этот-LAT</ta>
            <ta e="T1076" id="Seg_10916" s="T1075">я.NOM</ta>
            <ta e="T1077" id="Seg_10917" s="T1076">сказать-PST-2SG</ta>
            <ta e="T1078" id="Seg_10918" s="T1077">я.NOM</ta>
            <ta e="T1079" id="Seg_10919" s="T1078">куда</ta>
            <ta e="T1080" id="Seg_10920" s="T1079">идти-PRS-2SG</ta>
            <ta e="T1081" id="Seg_10921" s="T1080">я.NOM</ta>
            <ta e="T1082" id="Seg_10922" s="T1081">сказать-IPFVZ-1SG</ta>
            <ta e="T1083" id="Seg_10923" s="T1082">Бог-LAT</ta>
            <ta e="T1084" id="Seg_10924" s="T1083">молиться-INF.LAT</ta>
            <ta e="T1086" id="Seg_10925" s="T1085">ну</ta>
            <ta e="T1087" id="Seg_10926" s="T1086">завтра</ta>
            <ta e="T1088" id="Seg_10927" s="T1087">я.NOM-COM</ta>
            <ta e="T1089" id="Seg_10928" s="T1088">пойти-FUT-2SG</ta>
            <ta e="T1091" id="Seg_10929" s="T1090">так</ta>
            <ta e="T1092" id="Seg_10930" s="T1091">сказать-PST.[3SG]</ta>
            <ta e="T1093" id="Seg_10931" s="T1092">я.LAT</ta>
            <ta e="T1095" id="Seg_10932" s="T1094">а</ta>
            <ta e="T1140" id="Seg_10933" s="T1139">два.[NOM.SG]</ta>
            <ta e="T1141" id="Seg_10934" s="T1140">зима.[NOM.SG]</ta>
            <ta e="T1146" id="Seg_10935" s="T1144">два.[NOM.SG]</ta>
            <ta e="T1148" id="Seg_10936" s="T1146">зима.[NOM.SG]</ta>
            <ta e="T1178" id="Seg_10937" s="T1177">мы.NOM</ta>
            <ta e="T1179" id="Seg_10938" s="T1178">бригадир.[NOM.SG]</ta>
            <ta e="T1180" id="Seg_10939" s="T1179">быть-PST.[3SG]</ta>
            <ta e="T1181" id="Seg_10940" s="T1180">немец.[NOM.SG]</ta>
            <ta e="T1183" id="Seg_10941" s="T1182">этот.[NOM.SG]</ta>
            <ta e="T1184" id="Seg_10942" s="T1183">очень</ta>
            <ta e="T1186" id="Seg_10943" s="T1185">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T1187" id="Seg_10944" s="T1186">лошадь-PL</ta>
            <ta e="T1188" id="Seg_10945" s="T1187">дать-PST.[3SG]</ta>
            <ta e="T1190" id="Seg_10946" s="T1189">сейчас</ta>
            <ta e="T1191" id="Seg_10947" s="T1190">этот.[NOM.SG]</ta>
            <ta e="T1739" id="Seg_10948" s="T1191">пойти-CVB</ta>
            <ta e="T1192" id="Seg_10949" s="T1739">исчезнуть-PST.[3SG]</ta>
            <ta e="T1193" id="Seg_10950" s="T1192">Волгоград-LAT</ta>
            <ta e="T1195" id="Seg_10951" s="T1194">а</ta>
            <ta e="T1196" id="Seg_10952" s="T1195">когда</ta>
            <ta e="T1197" id="Seg_10953" s="T1196">прийти-PST.[3SG]</ta>
            <ta e="T1198" id="Seg_10954" s="T1197">этот.[NOM.SG]</ta>
            <ta e="T1199" id="Seg_10955" s="T1198">другой.[NOM.SG]</ta>
            <ta e="T1200" id="Seg_10956" s="T1199">председатель.[NOM.SG]</ta>
            <ta e="T1201" id="Seg_10957" s="T1200">мы.LAT</ta>
            <ta e="T1202" id="Seg_10958" s="T1201">этот.[NOM.SG]</ta>
            <ta e="T1203" id="Seg_10959" s="T1202">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T1204" id="Seg_10960" s="T1203">лошадь.[NOM.SG]</ta>
            <ta e="T1205" id="Seg_10961" s="T1204">дать-PST-3PL</ta>
            <ta e="T1206" id="Seg_10962" s="T1205">дать-PST.[3SG]</ta>
            <ta e="T1208" id="Seg_10963" s="T1207">кто-ACC=INDEF</ta>
            <ta e="T1209" id="Seg_10964" s="T1208">NEG</ta>
            <ta e="T1210" id="Seg_10965" s="T1209">ягода-VBLZ-CVB</ta>
            <ta e="T1211" id="Seg_10966" s="T1210">пойти-PST-3PL</ta>
            <ta e="T1212" id="Seg_10967" s="T1211">лошадь-INS</ta>
            <ta e="T1213" id="Seg_10968" s="T1212">дать-PST.[3SG]</ta>
            <ta e="T1214" id="Seg_10969" s="T1213">тоже</ta>
            <ta e="T1216" id="Seg_10970" s="T1215">этот.[NOM.SG]</ta>
            <ta e="T1217" id="Seg_10971" s="T1216">председатель.[NOM.SG]</ta>
            <ta e="T1218" id="Seg_10972" s="T1217">сейчас</ta>
            <ta e="T1219" id="Seg_10973" s="T1218">мы.LAT</ta>
            <ta e="T1220" id="Seg_10974" s="T1219">Вознесенское-LAT</ta>
            <ta e="T1221" id="Seg_10975" s="T1220">пойти-PST.[3SG]</ta>
            <ta e="T1222" id="Seg_10976" s="T1221">там</ta>
            <ta e="T1223" id="Seg_10977" s="T1222">жить-DUR.[3SG]</ta>
            <ta e="T1225" id="Seg_10978" s="T1224">этот-LAT</ta>
            <ta e="T1226" id="Seg_10979" s="T1225">дом.[NOM.SG]</ta>
            <ta e="T1227" id="Seg_10980" s="T1226">установить-PST-3PL</ta>
            <ta e="T1228" id="Seg_10981" s="T1227">дом-NOM/GEN.3SG</ta>
            <ta e="T1229" id="Seg_10982" s="T1228">сейчас</ta>
            <ta e="T1230" id="Seg_10983" s="T1229">стоять-DUR.[3SG]</ta>
            <ta e="T1231" id="Seg_10984" s="T1230">кто.[NOM.SG]=INDEF</ta>
            <ta e="T1232" id="Seg_10985" s="T1231">NEG</ta>
            <ta e="T1233" id="Seg_10986" s="T1232">жить-PRS.[3SG]</ta>
            <ta e="T1234" id="Seg_10987" s="T1233">этот.[NOM.SG]</ta>
            <ta e="T1235" id="Seg_10988" s="T1234">дом-LOC</ta>
            <ta e="T1237" id="Seg_10989" s="T1236">хватит</ta>
            <ta e="T1266" id="Seg_10990" s="T1265">а</ta>
            <ta e="T1382" id="Seg_10991" s="T1380">а</ta>
            <ta e="T1506" id="Seg_10992" s="T1504">а</ta>
            <ta e="T1524" id="Seg_10993" s="T1523">а</ta>
            <ta e="T1579" id="Seg_10994" s="T1578">а</ta>
            <ta e="T1653" id="Seg_10995" s="T1652">а</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T50" id="Seg_10996" s="T49">conj</ta>
            <ta e="T100" id="Seg_10997" s="T99">conj</ta>
            <ta e="T148" id="Seg_10998" s="T147">conj</ta>
            <ta e="T163" id="Seg_10999" s="T162">conj</ta>
            <ta e="T168" id="Seg_11000" s="T167">conj</ta>
            <ta e="T186" id="Seg_11001" s="T185">propr.[n:case]</ta>
            <ta e="T187" id="Seg_11002" s="T186">propr.[n:case]</ta>
            <ta e="T188" id="Seg_11003" s="T187">refl-n:case.poss</ta>
            <ta e="T189" id="Seg_11004" s="T188">propr-n:case</ta>
            <ta e="T190" id="Seg_11005" s="T189">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T191" id="Seg_11006" s="T190">conj</ta>
            <ta e="T192" id="Seg_11007" s="T191">adj.[n:case]</ta>
            <ta e="T193" id="Seg_11008" s="T192">n.[n:case]</ta>
            <ta e="T194" id="Seg_11009" s="T193">v-v:tense.[v:pn]</ta>
            <ta e="T196" id="Seg_11010" s="T195">adv</ta>
            <ta e="T197" id="Seg_11011" s="T196">dempro.[n:case]</ta>
            <ta e="T198" id="Seg_11012" s="T197">dempro-n:case</ta>
            <ta e="T200" id="Seg_11013" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_11014" s="T200">dempro.[n:case]</ta>
            <ta e="T203" id="Seg_11015" s="T202">dempro-n:num</ta>
            <ta e="T206" id="Seg_11016" s="T205">dempro.[n:case]</ta>
            <ta e="T208" id="Seg_11017" s="T207">adv</ta>
            <ta e="T209" id="Seg_11018" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_11019" s="T209">n-n:num-n:case</ta>
            <ta e="T211" id="Seg_11020" s="T210">v-v&gt;v.[v:pn]</ta>
            <ta e="T213" id="Seg_11021" s="T212">conj</ta>
            <ta e="T214" id="Seg_11022" s="T213">dempro.[n:case]</ta>
            <ta e="T1738" id="Seg_11023" s="T214">v-v:n-fin</ta>
            <ta e="T215" id="Seg_11024" s="T1738">v-v:tense.[v:pn]</ta>
            <ta e="T216" id="Seg_11025" s="T215">que</ta>
            <ta e="T217" id="Seg_11026" s="T216">propr</ta>
            <ta e="T218" id="Seg_11027" s="T217">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T220" id="Seg_11028" s="T219">adv</ta>
            <ta e="T221" id="Seg_11029" s="T220">v-v&gt;v.[v:pn]</ta>
            <ta e="T222" id="Seg_11030" s="T221">dempro.[n:case]</ta>
            <ta e="T223" id="Seg_11031" s="T222">adv</ta>
            <ta e="T225" id="Seg_11032" s="T224">num.[n:case]</ta>
            <ta e="T226" id="Seg_11033" s="T225">n.[n:case]</ta>
            <ta e="T227" id="Seg_11034" s="T226">conj</ta>
            <ta e="T229" id="Seg_11035" s="T228">n.[n:case]</ta>
            <ta e="T230" id="Seg_11036" s="T229">dempro-n:case</ta>
            <ta e="T231" id="Seg_11037" s="T230">num.[n:case]</ta>
            <ta e="T232" id="Seg_11038" s="T231">n.[n:case]</ta>
            <ta e="T233" id="Seg_11039" s="T232">dempro.[n:case]</ta>
            <ta e="T234" id="Seg_11040" s="T233">adv</ta>
            <ta e="T236" id="Seg_11041" s="T235">adv</ta>
            <ta e="T238" id="Seg_11042" s="T237">pers</ta>
            <ta e="T239" id="Seg_11043" s="T238">n-n:case.poss</ta>
            <ta e="T240" id="Seg_11044" s="T239">v-v:tense.[v:pn]</ta>
            <ta e="T242" id="Seg_11045" s="T241">num.[n:case]</ta>
            <ta e="T243" id="Seg_11046" s="T242">n-n:case.poss</ta>
            <ta e="T244" id="Seg_11047" s="T243">v-v:tense.[v:pn]</ta>
            <ta e="T246" id="Seg_11048" s="T245">dempro.[n:case]</ta>
            <ta e="T247" id="Seg_11049" s="T246">v-v&gt;v.[v:pn]</ta>
            <ta e="T248" id="Seg_11050" s="T247">propr-n:case</ta>
            <ta e="T249" id="Seg_11051" s="T248">aux-v:mood.pn</ta>
            <ta e="T250" id="Seg_11052" s="T249">aux-v:mood.pn</ta>
            <ta e="T251" id="Seg_11053" s="T250">num.[n:case]</ta>
            <ta e="T252" id="Seg_11054" s="T251">n.[n:case]</ta>
            <ta e="T254" id="Seg_11055" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_11056" s="T254">adv</ta>
            <ta e="T257" id="Seg_11057" s="T256">adv</ta>
            <ta e="T258" id="Seg_11058" s="T257">dempro-n:case</ta>
            <ta e="T259" id="Seg_11059" s="T258">v-v:tense.[v:pn]</ta>
            <ta e="T260" id="Seg_11060" s="T259">conj</ta>
            <ta e="T261" id="Seg_11061" s="T260">n-n:num-n:case</ta>
            <ta e="T262" id="Seg_11062" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11063" s="T262">adv</ta>
            <ta e="T463" id="Seg_11064" s="T462">conj</ta>
            <ta e="T552" id="Seg_11065" s="T550">conj</ta>
            <ta e="T737" id="Seg_11066" s="T736">conj</ta>
            <ta e="T752" id="Seg_11067" s="T751">conj</ta>
            <ta e="T768" id="Seg_11068" s="T767">conj</ta>
            <ta e="T782" id="Seg_11069" s="T781">conj</ta>
            <ta e="T812" id="Seg_11070" s="T811">conj</ta>
            <ta e="T828" id="Seg_11071" s="T827">conj</ta>
            <ta e="T971" id="Seg_11072" s="T970">pers</ta>
            <ta e="T972" id="Seg_11073" s="T971">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T973" id="Seg_11074" s="T972">n.[n:case]</ta>
            <ta e="T974" id="Seg_11075" s="T973">adv</ta>
            <ta e="T976" id="Seg_11076" s="T975">num.[n:case]</ta>
            <ta e="T977" id="Seg_11077" s="T976">n.[n:case]</ta>
            <ta e="T979" id="Seg_11078" s="T978">que</ta>
            <ta e="T980" id="Seg_11079" s="T979">pers</ta>
            <ta e="T981" id="Seg_11080" s="T980">que</ta>
            <ta e="T982" id="Seg_11081" s="T981">v-v:tense-v:pn</ta>
            <ta e="T983" id="Seg_11082" s="T982">v-v:tense-v:pn</ta>
            <ta e="T984" id="Seg_11083" s="T983">dempro-n:case</ta>
            <ta e="T986" id="Seg_11084" s="T985">v-v:tense-v:pn</ta>
            <ta e="T988" id="Seg_11085" s="T987">ptcl</ta>
            <ta e="T989" id="Seg_11086" s="T988">v-v:mood.pn</ta>
            <ta e="T991" id="Seg_11087" s="T990">pers</ta>
            <ta e="T992" id="Seg_11088" s="T991">pers</ta>
            <ta e="T993" id="Seg_11089" s="T992">v-v:tense-v:pn</ta>
            <ta e="T995" id="Seg_11090" s="T994">adv</ta>
            <ta e="T996" id="Seg_11091" s="T995">dempro.[n:case]</ta>
            <ta e="T997" id="Seg_11092" s="T996">v-v&gt;v.[v:pn]</ta>
            <ta e="T998" id="Seg_11093" s="T997">pers</ta>
            <ta e="T999" id="Seg_11094" s="T998">v-v:tense-v:pn</ta>
            <ta e="T1001" id="Seg_11095" s="T1000">conj</ta>
            <ta e="T1002" id="Seg_11096" s="T1001">pers</ta>
            <ta e="T1003" id="Seg_11097" s="T1002">v-v:n.fin</ta>
            <ta e="T1004" id="Seg_11098" s="T1003">v</ta>
            <ta e="T1005" id="Seg_11099" s="T1004">refl-n:case.poss</ta>
            <ta e="T1006" id="Seg_11100" s="T1005">n-n:case.poss</ta>
            <ta e="T1007" id="Seg_11101" s="T1006">pers-n:case</ta>
            <ta e="T1009" id="Seg_11102" s="T1008">adv</ta>
            <ta e="T1010" id="Seg_11103" s="T1009">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1011" id="Seg_11104" s="T1010">v-v:tense-v:pn</ta>
            <ta e="T1012" id="Seg_11105" s="T1011">n-n:case</ta>
            <ta e="T1014" id="Seg_11106" s="T1013">dempro.[n:case]</ta>
            <ta e="T1016" id="Seg_11107" s="T1015">adj.[n:case]</ta>
            <ta e="T1017" id="Seg_11108" s="T1016">n.[n:case]</ta>
            <ta e="T1018" id="Seg_11109" s="T1017">v-v:tense.[v:pn]</ta>
            <ta e="T1019" id="Seg_11110" s="T1018">pers</ta>
            <ta e="T1020" id="Seg_11111" s="T1019">v-v:tense-v:pn</ta>
            <ta e="T1021" id="Seg_11112" s="T1020">pers</ta>
            <ta e="T1022" id="Seg_11113" s="T1021">dempro</ta>
            <ta e="T1023" id="Seg_11114" s="T1022">v-v:tense.[v:pn]</ta>
            <ta e="T1024" id="Seg_11115" s="T1023">que-n:case=ptcl</ta>
            <ta e="T1025" id="Seg_11116" s="T1024">aux-v:pn</ta>
            <ta e="T1026" id="Seg_11117" s="T1025">ptcl</ta>
            <ta e="T1027" id="Seg_11118" s="T1026">v-v:tense-v:pn</ta>
            <ta e="T1029" id="Seg_11119" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_11120" s="T1029">v-v:tense-v:pn</ta>
            <ta e="T1031" id="Seg_11121" s="T1030">adv</ta>
            <ta e="T1032" id="Seg_11122" s="T1031">que.[n:case]</ta>
            <ta e="T1033" id="Seg_11123" s="T1032">v-v:tense-v:pn</ta>
            <ta e="T1035" id="Seg_11124" s="T1034">adv</ta>
            <ta e="T1036" id="Seg_11125" s="T1035">num-num&gt;num</ta>
            <ta e="T1037" id="Seg_11126" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1074" id="Seg_11127" s="T1073">adv</ta>
            <ta e="T1075" id="Seg_11128" s="T1074">dempro-n:case</ta>
            <ta e="T1076" id="Seg_11129" s="T1075">pers</ta>
            <ta e="T1077" id="Seg_11130" s="T1076">v-v:tense-v:pn</ta>
            <ta e="T1078" id="Seg_11131" s="T1077">pers</ta>
            <ta e="T1079" id="Seg_11132" s="T1078">que</ta>
            <ta e="T1080" id="Seg_11133" s="T1079">v-v:tense-v:pn</ta>
            <ta e="T1081" id="Seg_11134" s="T1080">pers</ta>
            <ta e="T1082" id="Seg_11135" s="T1081">v-v&gt;v-v:pn</ta>
            <ta e="T1083" id="Seg_11136" s="T1082">n-n:case</ta>
            <ta e="T1084" id="Seg_11137" s="T1083">v-v:n.fin</ta>
            <ta e="T1086" id="Seg_11138" s="T1085">ptcl</ta>
            <ta e="T1087" id="Seg_11139" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_11140" s="T1087">pers-n:case</ta>
            <ta e="T1089" id="Seg_11141" s="T1088">v-v:tense-v:pn</ta>
            <ta e="T1091" id="Seg_11142" s="T1090">ptcl</ta>
            <ta e="T1092" id="Seg_11143" s="T1091">v-v:tense.[v:pn]</ta>
            <ta e="T1093" id="Seg_11144" s="T1092">pers</ta>
            <ta e="T1095" id="Seg_11145" s="T1094">conj</ta>
            <ta e="T1140" id="Seg_11146" s="T1139">num.[n:case]</ta>
            <ta e="T1141" id="Seg_11147" s="T1140">n.[n:case]</ta>
            <ta e="T1146" id="Seg_11148" s="T1144">num.[n:case]</ta>
            <ta e="T1148" id="Seg_11149" s="T1146">n.[n:case]</ta>
            <ta e="T1178" id="Seg_11150" s="T1177">pers</ta>
            <ta e="T1179" id="Seg_11151" s="T1178">n.[n:case]</ta>
            <ta e="T1180" id="Seg_11152" s="T1179">v-v:tense.[v:pn]</ta>
            <ta e="T1181" id="Seg_11153" s="T1180">n.[n:case]</ta>
            <ta e="T1183" id="Seg_11154" s="T1182">dempro.[n:case]</ta>
            <ta e="T1184" id="Seg_11155" s="T1183">adv</ta>
            <ta e="T1186" id="Seg_11156" s="T1185">n-n:case.poss</ta>
            <ta e="T1187" id="Seg_11157" s="T1186">n-n:num</ta>
            <ta e="T1188" id="Seg_11158" s="T1187">v-v:tense.[v:pn]</ta>
            <ta e="T1190" id="Seg_11159" s="T1189">adv</ta>
            <ta e="T1191" id="Seg_11160" s="T1190">dempro.[n:case]</ta>
            <ta e="T1739" id="Seg_11161" s="T1191">v-v:n-fin</ta>
            <ta e="T1192" id="Seg_11162" s="T1739">v-v:tense.[v:pn]</ta>
            <ta e="T1193" id="Seg_11163" s="T1192">propr-n:case</ta>
            <ta e="T1195" id="Seg_11164" s="T1194">conj</ta>
            <ta e="T1196" id="Seg_11165" s="T1195">que</ta>
            <ta e="T1197" id="Seg_11166" s="T1196">v-v:tense.[v:pn]</ta>
            <ta e="T1198" id="Seg_11167" s="T1197">dempro.[n:case]</ta>
            <ta e="T1199" id="Seg_11168" s="T1198">adj.[n:case]</ta>
            <ta e="T1200" id="Seg_11169" s="T1199">n.[n:case]</ta>
            <ta e="T1201" id="Seg_11170" s="T1200">pers</ta>
            <ta e="T1202" id="Seg_11171" s="T1201">dempro.[n:case]</ta>
            <ta e="T1203" id="Seg_11172" s="T1202">n-n:case.poss</ta>
            <ta e="T1204" id="Seg_11173" s="T1203">n.[n:case]</ta>
            <ta e="T1205" id="Seg_11174" s="T1204">v-v:tense-v:pn</ta>
            <ta e="T1206" id="Seg_11175" s="T1205">v-v:tense.[v:pn]</ta>
            <ta e="T1208" id="Seg_11176" s="T1207">que-n:case=ptcl</ta>
            <ta e="T1209" id="Seg_11177" s="T1208">ptcl</ta>
            <ta e="T1210" id="Seg_11178" s="T1209">n-n&gt;v-v:n.fin</ta>
            <ta e="T1211" id="Seg_11179" s="T1210">v-v:tense-v:pn</ta>
            <ta e="T1212" id="Seg_11180" s="T1211">n-n:case</ta>
            <ta e="T1213" id="Seg_11181" s="T1212">v-v:tense.[v:pn]</ta>
            <ta e="T1214" id="Seg_11182" s="T1213">ptcl</ta>
            <ta e="T1216" id="Seg_11183" s="T1215">dempro.[n:case]</ta>
            <ta e="T1217" id="Seg_11184" s="T1216">n.[n:case]</ta>
            <ta e="T1218" id="Seg_11185" s="T1217">adv</ta>
            <ta e="T1219" id="Seg_11186" s="T1218">pers</ta>
            <ta e="T1220" id="Seg_11187" s="T1219">propr-n:case</ta>
            <ta e="T1221" id="Seg_11188" s="T1220">v-v:tense.[v:pn]</ta>
            <ta e="T1222" id="Seg_11189" s="T1221">adv</ta>
            <ta e="T1223" id="Seg_11190" s="T1222">v-v&gt;v.[v:pn]</ta>
            <ta e="T1225" id="Seg_11191" s="T1224">dempro-n:case</ta>
            <ta e="T1226" id="Seg_11192" s="T1225">n.[n:case]</ta>
            <ta e="T1227" id="Seg_11193" s="T1226">v-v:tense-v:pn</ta>
            <ta e="T1228" id="Seg_11194" s="T1227">n-n:case.poss</ta>
            <ta e="T1229" id="Seg_11195" s="T1228">adv</ta>
            <ta e="T1230" id="Seg_11196" s="T1229">v-v&gt;v.[v:pn]</ta>
            <ta e="T1231" id="Seg_11197" s="T1230">que.[n:case]=ptcl</ta>
            <ta e="T1232" id="Seg_11198" s="T1231">ptcl</ta>
            <ta e="T1233" id="Seg_11199" s="T1232">v-v:tense.[v:pn]</ta>
            <ta e="T1234" id="Seg_11200" s="T1233">dempro.[n:case]</ta>
            <ta e="T1235" id="Seg_11201" s="T1234">n-n:case</ta>
            <ta e="T1237" id="Seg_11202" s="T1236">ptcl</ta>
            <ta e="T1266" id="Seg_11203" s="T1265">conj</ta>
            <ta e="T1382" id="Seg_11204" s="T1380">conj</ta>
            <ta e="T1506" id="Seg_11205" s="T1504">conj</ta>
            <ta e="T1524" id="Seg_11206" s="T1523">conj</ta>
            <ta e="T1579" id="Seg_11207" s="T1578">conj</ta>
            <ta e="T1653" id="Seg_11208" s="T1652">conj</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T50" id="Seg_11209" s="T49">conj</ta>
            <ta e="T100" id="Seg_11210" s="T99">conj</ta>
            <ta e="T148" id="Seg_11211" s="T147">conj</ta>
            <ta e="T163" id="Seg_11212" s="T162">conj</ta>
            <ta e="T168" id="Seg_11213" s="T167">conj</ta>
            <ta e="T186" id="Seg_11214" s="T185">propr</ta>
            <ta e="T187" id="Seg_11215" s="T186">propr</ta>
            <ta e="T188" id="Seg_11216" s="T187">refl</ta>
            <ta e="T189" id="Seg_11217" s="T188">propr</ta>
            <ta e="T190" id="Seg_11218" s="T189">v</ta>
            <ta e="T191" id="Seg_11219" s="T190">conj</ta>
            <ta e="T192" id="Seg_11220" s="T191">adj</ta>
            <ta e="T193" id="Seg_11221" s="T192">n</ta>
            <ta e="T194" id="Seg_11222" s="T193">v</ta>
            <ta e="T196" id="Seg_11223" s="T195">adv</ta>
            <ta e="T197" id="Seg_11224" s="T196">dempro</ta>
            <ta e="T198" id="Seg_11225" s="T197">dempro</ta>
            <ta e="T200" id="Seg_11226" s="T199">n</ta>
            <ta e="T201" id="Seg_11227" s="T200">dempro</ta>
            <ta e="T203" id="Seg_11228" s="T202">dempro</ta>
            <ta e="T206" id="Seg_11229" s="T205">dempro</ta>
            <ta e="T208" id="Seg_11230" s="T207">adv</ta>
            <ta e="T209" id="Seg_11231" s="T208">n</ta>
            <ta e="T210" id="Seg_11232" s="T209">n</ta>
            <ta e="T211" id="Seg_11233" s="T210">v</ta>
            <ta e="T213" id="Seg_11234" s="T212">conj</ta>
            <ta e="T214" id="Seg_11235" s="T213">dempro</ta>
            <ta e="T1738" id="Seg_11236" s="T214">v</ta>
            <ta e="T215" id="Seg_11237" s="T1738">v</ta>
            <ta e="T216" id="Seg_11238" s="T215">que</ta>
            <ta e="T217" id="Seg_11239" s="T216">propr</ta>
            <ta e="T218" id="Seg_11240" s="T217">v</ta>
            <ta e="T220" id="Seg_11241" s="T219">adv</ta>
            <ta e="T221" id="Seg_11242" s="T220">v</ta>
            <ta e="T222" id="Seg_11243" s="T221">dempro</ta>
            <ta e="T223" id="Seg_11244" s="T222">adv</ta>
            <ta e="T225" id="Seg_11245" s="T224">num</ta>
            <ta e="T226" id="Seg_11246" s="T225">n</ta>
            <ta e="T227" id="Seg_11247" s="T226">conj</ta>
            <ta e="T229" id="Seg_11248" s="T228">n</ta>
            <ta e="T230" id="Seg_11249" s="T229">dempro</ta>
            <ta e="T231" id="Seg_11250" s="T230">num</ta>
            <ta e="T232" id="Seg_11251" s="T231">n</ta>
            <ta e="T233" id="Seg_11252" s="T232">dempro</ta>
            <ta e="T234" id="Seg_11253" s="T233">adv</ta>
            <ta e="T236" id="Seg_11254" s="T235">adv</ta>
            <ta e="T238" id="Seg_11255" s="T237">pers</ta>
            <ta e="T239" id="Seg_11256" s="T238">n</ta>
            <ta e="T240" id="Seg_11257" s="T239">v</ta>
            <ta e="T242" id="Seg_11258" s="T241">num</ta>
            <ta e="T243" id="Seg_11259" s="T242">n</ta>
            <ta e="T244" id="Seg_11260" s="T243">v</ta>
            <ta e="T246" id="Seg_11261" s="T245">dempro</ta>
            <ta e="T247" id="Seg_11262" s="T246">v</ta>
            <ta e="T248" id="Seg_11263" s="T247">propr</ta>
            <ta e="T249" id="Seg_11264" s="T248">aux</ta>
            <ta e="T250" id="Seg_11265" s="T249">aux</ta>
            <ta e="T251" id="Seg_11266" s="T250">num</ta>
            <ta e="T252" id="Seg_11267" s="T251">n</ta>
            <ta e="T254" id="Seg_11268" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_11269" s="T254">adv</ta>
            <ta e="T257" id="Seg_11270" s="T256">adv</ta>
            <ta e="T258" id="Seg_11271" s="T257">dempro</ta>
            <ta e="T259" id="Seg_11272" s="T258">v</ta>
            <ta e="T260" id="Seg_11273" s="T259">conj</ta>
            <ta e="T261" id="Seg_11274" s="T260">n</ta>
            <ta e="T262" id="Seg_11275" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11276" s="T262">adv</ta>
            <ta e="T463" id="Seg_11277" s="T462">conj</ta>
            <ta e="T552" id="Seg_11278" s="T550">conj</ta>
            <ta e="T737" id="Seg_11279" s="T736">conj</ta>
            <ta e="T752" id="Seg_11280" s="T751">conj</ta>
            <ta e="T768" id="Seg_11281" s="T767">conj</ta>
            <ta e="T782" id="Seg_11282" s="T781">conj</ta>
            <ta e="T812" id="Seg_11283" s="T811">conj</ta>
            <ta e="T828" id="Seg_11284" s="T827">conj</ta>
            <ta e="T971" id="Seg_11285" s="T970">pers</ta>
            <ta e="T972" id="Seg_11286" s="T971">v</ta>
            <ta e="T973" id="Seg_11287" s="T972">n</ta>
            <ta e="T974" id="Seg_11288" s="T973">adv</ta>
            <ta e="T976" id="Seg_11289" s="T975">num</ta>
            <ta e="T977" id="Seg_11290" s="T976">n</ta>
            <ta e="T979" id="Seg_11291" s="T978">que</ta>
            <ta e="T980" id="Seg_11292" s="T979">pers</ta>
            <ta e="T981" id="Seg_11293" s="T980">que</ta>
            <ta e="T982" id="Seg_11294" s="T981">v</ta>
            <ta e="T983" id="Seg_11295" s="T982">v</ta>
            <ta e="T984" id="Seg_11296" s="T983">dempro</ta>
            <ta e="T986" id="Seg_11297" s="T985">v</ta>
            <ta e="T988" id="Seg_11298" s="T987">ptcl</ta>
            <ta e="T989" id="Seg_11299" s="T988">v</ta>
            <ta e="T991" id="Seg_11300" s="T990">pers</ta>
            <ta e="T992" id="Seg_11301" s="T991">pers</ta>
            <ta e="T993" id="Seg_11302" s="T992">v</ta>
            <ta e="T995" id="Seg_11303" s="T994">adv</ta>
            <ta e="T996" id="Seg_11304" s="T995">dempro</ta>
            <ta e="T997" id="Seg_11305" s="T996">v</ta>
            <ta e="T998" id="Seg_11306" s="T997">pers</ta>
            <ta e="T999" id="Seg_11307" s="T998">v</ta>
            <ta e="T1001" id="Seg_11308" s="T1000">conj</ta>
            <ta e="T1002" id="Seg_11309" s="T1001">pers</ta>
            <ta e="T1003" id="Seg_11310" s="T1002">v</ta>
            <ta e="T1004" id="Seg_11311" s="T1003">v</ta>
            <ta e="T1005" id="Seg_11312" s="T1004">refl</ta>
            <ta e="T1006" id="Seg_11313" s="T1005">n</ta>
            <ta e="T1007" id="Seg_11314" s="T1006">pers</ta>
            <ta e="T1009" id="Seg_11315" s="T1008">adv</ta>
            <ta e="T1010" id="Seg_11316" s="T1009">v</ta>
            <ta e="T1011" id="Seg_11317" s="T1010">v</ta>
            <ta e="T1012" id="Seg_11318" s="T1011">n</ta>
            <ta e="T1014" id="Seg_11319" s="T1013">dempro</ta>
            <ta e="T1016" id="Seg_11320" s="T1015">adj</ta>
            <ta e="T1017" id="Seg_11321" s="T1016">n</ta>
            <ta e="T1018" id="Seg_11322" s="T1017">v</ta>
            <ta e="T1019" id="Seg_11323" s="T1018">pers</ta>
            <ta e="T1020" id="Seg_11324" s="T1019">v</ta>
            <ta e="T1021" id="Seg_11325" s="T1020">pers</ta>
            <ta e="T1022" id="Seg_11326" s="T1021">dempro</ta>
            <ta e="T1023" id="Seg_11327" s="T1022">v</ta>
            <ta e="T1024" id="Seg_11328" s="T1023">que</ta>
            <ta e="T1025" id="Seg_11329" s="T1024">aux</ta>
            <ta e="T1026" id="Seg_11330" s="T1025">ptcl</ta>
            <ta e="T1027" id="Seg_11331" s="T1026">n</ta>
            <ta e="T1029" id="Seg_11332" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_11333" s="T1029">v</ta>
            <ta e="T1031" id="Seg_11334" s="T1030">adv</ta>
            <ta e="T1032" id="Seg_11335" s="T1031">que</ta>
            <ta e="T1033" id="Seg_11336" s="T1032">v</ta>
            <ta e="T1035" id="Seg_11337" s="T1034">adv</ta>
            <ta e="T1036" id="Seg_11338" s="T1035">num</ta>
            <ta e="T1037" id="Seg_11339" s="T1036">v</ta>
            <ta e="T1074" id="Seg_11340" s="T1073">adv</ta>
            <ta e="T1075" id="Seg_11341" s="T1074">dempro</ta>
            <ta e="T1076" id="Seg_11342" s="T1075">pers</ta>
            <ta e="T1077" id="Seg_11343" s="T1076">v</ta>
            <ta e="T1078" id="Seg_11344" s="T1077">pers</ta>
            <ta e="T1079" id="Seg_11345" s="T1078">que</ta>
            <ta e="T1080" id="Seg_11346" s="T1079">v</ta>
            <ta e="T1081" id="Seg_11347" s="T1080">pers</ta>
            <ta e="T1082" id="Seg_11348" s="T1081">v</ta>
            <ta e="T1083" id="Seg_11349" s="T1082">n</ta>
            <ta e="T1084" id="Seg_11350" s="T1083">v</ta>
            <ta e="T1086" id="Seg_11351" s="T1085">ptcl</ta>
            <ta e="T1087" id="Seg_11352" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_11353" s="T1087">pers</ta>
            <ta e="T1089" id="Seg_11354" s="T1088">v</ta>
            <ta e="T1091" id="Seg_11355" s="T1090">ptcl</ta>
            <ta e="T1092" id="Seg_11356" s="T1091">v</ta>
            <ta e="T1093" id="Seg_11357" s="T1092">pers</ta>
            <ta e="T1095" id="Seg_11358" s="T1094">conj</ta>
            <ta e="T1140" id="Seg_11359" s="T1139">num</ta>
            <ta e="T1141" id="Seg_11360" s="T1140">n</ta>
            <ta e="T1146" id="Seg_11361" s="T1144">num</ta>
            <ta e="T1148" id="Seg_11362" s="T1146">n</ta>
            <ta e="T1178" id="Seg_11363" s="T1177">pers</ta>
            <ta e="T1179" id="Seg_11364" s="T1178">n</ta>
            <ta e="T1180" id="Seg_11365" s="T1179">v</ta>
            <ta e="T1181" id="Seg_11366" s="T1180">n</ta>
            <ta e="T1183" id="Seg_11367" s="T1182">dempro</ta>
            <ta e="T1184" id="Seg_11368" s="T1183">adv</ta>
            <ta e="T1186" id="Seg_11369" s="T1185">n</ta>
            <ta e="T1187" id="Seg_11370" s="T1186">n</ta>
            <ta e="T1188" id="Seg_11371" s="T1187">v</ta>
            <ta e="T1190" id="Seg_11372" s="T1189">adv</ta>
            <ta e="T1191" id="Seg_11373" s="T1190">dempro</ta>
            <ta e="T1739" id="Seg_11374" s="T1191">v</ta>
            <ta e="T1192" id="Seg_11375" s="T1739">v</ta>
            <ta e="T1193" id="Seg_11376" s="T1192">propr</ta>
            <ta e="T1195" id="Seg_11377" s="T1194">conj</ta>
            <ta e="T1196" id="Seg_11378" s="T1195">que</ta>
            <ta e="T1197" id="Seg_11379" s="T1196">v</ta>
            <ta e="T1198" id="Seg_11380" s="T1197">dempro</ta>
            <ta e="T1199" id="Seg_11381" s="T1198">adj</ta>
            <ta e="T1200" id="Seg_11382" s="T1199">n</ta>
            <ta e="T1201" id="Seg_11383" s="T1200">pers</ta>
            <ta e="T1202" id="Seg_11384" s="T1201">dempro</ta>
            <ta e="T1203" id="Seg_11385" s="T1202">n</ta>
            <ta e="T1204" id="Seg_11386" s="T1203">n</ta>
            <ta e="T1205" id="Seg_11387" s="T1204">v</ta>
            <ta e="T1206" id="Seg_11388" s="T1205">v</ta>
            <ta e="T1208" id="Seg_11389" s="T1207">que</ta>
            <ta e="T1209" id="Seg_11390" s="T1208">ptcl</ta>
            <ta e="T1210" id="Seg_11391" s="T1209">v</ta>
            <ta e="T1211" id="Seg_11392" s="T1210">v</ta>
            <ta e="T1212" id="Seg_11393" s="T1211">n</ta>
            <ta e="T1213" id="Seg_11394" s="T1212">v</ta>
            <ta e="T1214" id="Seg_11395" s="T1213">ptcl</ta>
            <ta e="T1216" id="Seg_11396" s="T1215">dempro</ta>
            <ta e="T1217" id="Seg_11397" s="T1216">n</ta>
            <ta e="T1218" id="Seg_11398" s="T1217">adv</ta>
            <ta e="T1219" id="Seg_11399" s="T1218">pers</ta>
            <ta e="T1220" id="Seg_11400" s="T1219">propr</ta>
            <ta e="T1221" id="Seg_11401" s="T1220">v</ta>
            <ta e="T1222" id="Seg_11402" s="T1221">adv</ta>
            <ta e="T1223" id="Seg_11403" s="T1222">v</ta>
            <ta e="T1225" id="Seg_11404" s="T1224">dempro</ta>
            <ta e="T1226" id="Seg_11405" s="T1225">n</ta>
            <ta e="T1227" id="Seg_11406" s="T1226">v</ta>
            <ta e="T1228" id="Seg_11407" s="T1227">n</ta>
            <ta e="T1229" id="Seg_11408" s="T1228">adv</ta>
            <ta e="T1230" id="Seg_11409" s="T1229">v</ta>
            <ta e="T1231" id="Seg_11410" s="T1230">que</ta>
            <ta e="T1232" id="Seg_11411" s="T1231">ptcl</ta>
            <ta e="T1233" id="Seg_11412" s="T1232">v</ta>
            <ta e="T1234" id="Seg_11413" s="T1233">dempro</ta>
            <ta e="T1235" id="Seg_11414" s="T1234">n</ta>
            <ta e="T1237" id="Seg_11415" s="T1236">ptcl</ta>
            <ta e="T1266" id="Seg_11416" s="T1265">conj</ta>
            <ta e="T1382" id="Seg_11417" s="T1380">conj</ta>
            <ta e="T1506" id="Seg_11418" s="T1504">conj</ta>
            <ta e="T1524" id="Seg_11419" s="T1523">conj</ta>
            <ta e="T1579" id="Seg_11420" s="T1578">conj</ta>
            <ta e="T1653" id="Seg_11421" s="T1652">conj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T189" id="Seg_11422" s="T188">np:So</ta>
            <ta e="T193" id="Seg_11423" s="T192">np.h:Th</ta>
            <ta e="T196" id="Seg_11424" s="T195">adv:Time</ta>
            <ta e="T197" id="Seg_11425" s="T196">pro.h:A</ta>
            <ta e="T198" id="Seg_11426" s="T197">pro.h:B</ta>
            <ta e="T200" id="Seg_11427" s="T199">np:G</ta>
            <ta e="T208" id="Seg_11428" s="T207">adv:Time</ta>
            <ta e="T209" id="Seg_11429" s="T208">np:Th</ta>
            <ta e="T210" id="Seg_11430" s="T209">pro.h:R</ta>
            <ta e="T211" id="Seg_11431" s="T210">0.3.h:A</ta>
            <ta e="T214" id="Seg_11432" s="T213">pro.h:A</ta>
            <ta e="T216" id="Seg_11433" s="T215">pro:L</ta>
            <ta e="T217" id="Seg_11434" s="T216">np.h:E</ta>
            <ta e="T220" id="Seg_11435" s="T219">adv:L</ta>
            <ta e="T221" id="Seg_11436" s="T220">0.3.h:E</ta>
            <ta e="T223" id="Seg_11437" s="T222">adv:Time</ta>
            <ta e="T226" id="Seg_11438" s="T225">np.h:Th</ta>
            <ta e="T229" id="Seg_11439" s="T228">np.h:Th</ta>
            <ta e="T230" id="Seg_11440" s="T229">np.h:Com</ta>
            <ta e="T232" id="Seg_11441" s="T231">np.h:Th</ta>
            <ta e="T236" id="Seg_11442" s="T235">adv:Time</ta>
            <ta e="T238" id="Seg_11443" s="T237">pro.h:Poss</ta>
            <ta e="T239" id="Seg_11444" s="T238">np.h:A</ta>
            <ta e="T243" id="Seg_11445" s="T242">np.h:Th</ta>
            <ta e="T244" id="Seg_11446" s="T243">0.3.h:A</ta>
            <ta e="T246" id="Seg_11447" s="T245">pro.h:A</ta>
            <ta e="T248" id="Seg_11448" s="T247">np.h:R</ta>
            <ta e="T249" id="Seg_11449" s="T248">0.2.h:A</ta>
            <ta e="T252" id="Seg_11450" s="T251">np.h:Th</ta>
            <ta e="T258" id="Seg_11451" s="T257">pro.h:Th</ta>
            <ta e="T261" id="Seg_11452" s="T260">np.h:Th</ta>
            <ta e="T971" id="Seg_11453" s="T970">pro.h:Poss</ta>
            <ta e="T973" id="Seg_11454" s="T972">np.h:P</ta>
            <ta e="T980" id="Seg_11455" s="T979">pro.h:A</ta>
            <ta e="T981" id="Seg_11456" s="T980">pro:L</ta>
            <ta e="T983" id="Seg_11457" s="T982">0.1.h:A</ta>
            <ta e="T984" id="Seg_11458" s="T983">pro.h:R</ta>
            <ta e="T986" id="Seg_11459" s="T985">0.2.h:A</ta>
            <ta e="T989" id="Seg_11460" s="T988">0.2.h:A</ta>
            <ta e="T992" id="Seg_11461" s="T991">pro.h:A</ta>
            <ta e="T995" id="Seg_11462" s="T994">adv:Time</ta>
            <ta e="T996" id="Seg_11463" s="T995">pro.h:A</ta>
            <ta e="T998" id="Seg_11464" s="T997">pro.h:A</ta>
            <ta e="T1002" id="Seg_11465" s="T1001">pro.h:A</ta>
            <ta e="T1006" id="Seg_11466" s="T1005">np.h:Th</ta>
            <ta e="T1007" id="Seg_11467" s="T1006">pro.h:B</ta>
            <ta e="T1009" id="Seg_11468" s="T1008">adv:Time</ta>
            <ta e="T1010" id="Seg_11469" s="T1009">0.1.h:A</ta>
            <ta e="T1011" id="Seg_11470" s="T1010">0.1.h:A</ta>
            <ta e="T1012" id="Seg_11471" s="T1011">np:G</ta>
            <ta e="T1017" id="Seg_11472" s="T1016">np.h:A</ta>
            <ta e="T1019" id="Seg_11473" s="T1018">pro.h:Th</ta>
            <ta e="T1020" id="Seg_11474" s="T1019">0.2.h:A</ta>
            <ta e="T1022" id="Seg_11475" s="T1021">pro.h:A</ta>
            <ta e="T1024" id="Seg_11476" s="T1023">pro.h:Th</ta>
            <ta e="T1027" id="Seg_11477" s="T1026">0.1.h:A</ta>
            <ta e="T1029" id="Seg_11478" s="T1028">pro.h:E</ta>
            <ta e="T1032" id="Seg_11479" s="T1031">pro.h:A</ta>
            <ta e="T1035" id="Seg_11480" s="T1034">adv:Time</ta>
            <ta e="T1036" id="Seg_11481" s="T1035">np.h:A</ta>
            <ta e="T1074" id="Seg_11482" s="T1073">adv:Time</ta>
            <ta e="T1075" id="Seg_11483" s="T1074">pro.h:R</ta>
            <ta e="T1076" id="Seg_11484" s="T1075">pro.h:A</ta>
            <ta e="T1079" id="Seg_11485" s="T1078">pro:G</ta>
            <ta e="T1080" id="Seg_11486" s="T1079">0.2.h:A</ta>
            <ta e="T1081" id="Seg_11487" s="T1080">pro.h:A</ta>
            <ta e="T1083" id="Seg_11488" s="T1082">np:R</ta>
            <ta e="T1088" id="Seg_11489" s="T1087">pro.h:Com</ta>
            <ta e="T1089" id="Seg_11490" s="T1088">0.2.h:A</ta>
            <ta e="T1178" id="Seg_11491" s="T1177">pro.h:Poss</ta>
            <ta e="T1179" id="Seg_11492" s="T1178">np.h:Th</ta>
            <ta e="T1181" id="Seg_11493" s="T1180">np.h:Th</ta>
            <ta e="T1183" id="Seg_11494" s="T1182">pro.h:A</ta>
            <ta e="T1186" id="Seg_11495" s="T1185">pro.h:R</ta>
            <ta e="T1187" id="Seg_11496" s="T1186">np:Th</ta>
            <ta e="T1190" id="Seg_11497" s="T1189">adv:Time</ta>
            <ta e="T1191" id="Seg_11498" s="T1190">pro.h:A</ta>
            <ta e="T1193" id="Seg_11499" s="T1192">np:G</ta>
            <ta e="T1200" id="Seg_11500" s="T1199">np.h:A</ta>
            <ta e="T1201" id="Seg_11501" s="T1200">pro:G</ta>
            <ta e="T1202" id="Seg_11502" s="T1201">pro.h:A</ta>
            <ta e="T1203" id="Seg_11503" s="T1202">np:R</ta>
            <ta e="T1204" id="Seg_11504" s="T1203">np:Th</ta>
            <ta e="T1211" id="Seg_11505" s="T1210">0.3.h:A</ta>
            <ta e="T1212" id="Seg_11506" s="T1211">np:Ins</ta>
            <ta e="T1213" id="Seg_11507" s="T1212">0.3.h:A</ta>
            <ta e="T1217" id="Seg_11508" s="T1216">np.h:A</ta>
            <ta e="T1218" id="Seg_11509" s="T1217">adv:Time</ta>
            <ta e="T1219" id="Seg_11510" s="T1218">pro.h:Poss</ta>
            <ta e="T1220" id="Seg_11511" s="T1219">np:G</ta>
            <ta e="T1222" id="Seg_11512" s="T1221">adv:L</ta>
            <ta e="T1223" id="Seg_11513" s="T1222">0.3.h:E</ta>
            <ta e="T1225" id="Seg_11514" s="T1224">pro.h:B</ta>
            <ta e="T1226" id="Seg_11515" s="T1225">np:P</ta>
            <ta e="T1227" id="Seg_11516" s="T1226">0.3.h:A</ta>
            <ta e="T1228" id="Seg_11517" s="T1227">np:Th</ta>
            <ta e="T1229" id="Seg_11518" s="T1228">adv:Time</ta>
            <ta e="T1231" id="Seg_11519" s="T1230">pro.h:E</ta>
            <ta e="T1235" id="Seg_11520" s="T1234">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T187" id="Seg_11521" s="T186">np.h:S</ta>
            <ta e="T190" id="Seg_11522" s="T189">v:pred</ta>
            <ta e="T193" id="Seg_11523" s="T192">np.h:S</ta>
            <ta e="T194" id="Seg_11524" s="T193">v:pred</ta>
            <ta e="T197" id="Seg_11525" s="T196">pro.h:S</ta>
            <ta e="T203" id="Seg_11526" s="T202">pro.h:S</ta>
            <ta e="T209" id="Seg_11527" s="T208">np:O</ta>
            <ta e="T211" id="Seg_11528" s="T210">v:pred 0.3.h:S</ta>
            <ta e="T214" id="Seg_11529" s="T213">pro.h:S</ta>
            <ta e="T1738" id="Seg_11530" s="T214">conv:pred</ta>
            <ta e="T215" id="Seg_11531" s="T1738">v:pred</ta>
            <ta e="T217" id="Seg_11532" s="T216">np.h:S</ta>
            <ta e="T218" id="Seg_11533" s="T217">v:pred</ta>
            <ta e="T221" id="Seg_11534" s="T220">v:pred 0.3.h:S</ta>
            <ta e="T226" id="Seg_11535" s="T225">np.h:S</ta>
            <ta e="T229" id="Seg_11536" s="T228">np.h:S</ta>
            <ta e="T230" id="Seg_11537" s="T229">n:pred</ta>
            <ta e="T232" id="Seg_11538" s="T231">np.h:S</ta>
            <ta e="T234" id="Seg_11539" s="T233">adj:pred</ta>
            <ta e="T239" id="Seg_11540" s="T238">np.h:S</ta>
            <ta e="T240" id="Seg_11541" s="T239">v:pred</ta>
            <ta e="T243" id="Seg_11542" s="T242">np.h:O</ta>
            <ta e="T244" id="Seg_11543" s="T243">v:pred 0.3.h:S</ta>
            <ta e="T246" id="Seg_11544" s="T245">pro.h:S</ta>
            <ta e="T247" id="Seg_11545" s="T246">v:pred</ta>
            <ta e="T249" id="Seg_11546" s="T248">v:pred 0.2.h:S</ta>
            <ta e="T252" id="Seg_11547" s="T251">np.h:O</ta>
            <ta e="T254" id="Seg_11548" s="T253">ptcl.neg</ta>
            <ta e="T255" id="Seg_11549" s="T254">adj:pred</ta>
            <ta e="T259" id="Seg_11550" s="T258">v:pred</ta>
            <ta e="T261" id="Seg_11551" s="T260">np.h:S</ta>
            <ta e="T262" id="Seg_11552" s="T261">ptcl.neg</ta>
            <ta e="T263" id="Seg_11553" s="T262">adj:pred</ta>
            <ta e="T972" id="Seg_11554" s="T971">v:pred</ta>
            <ta e="T973" id="Seg_11555" s="T972">np.h:S</ta>
            <ta e="T980" id="Seg_11556" s="T979">pro.h:S</ta>
            <ta e="T982" id="Seg_11557" s="T981">v:pred</ta>
            <ta e="T983" id="Seg_11558" s="T982">v:pred 0.1.h:S</ta>
            <ta e="T986" id="Seg_11559" s="T985">v:pred 0.2.h:S</ta>
            <ta e="T989" id="Seg_11560" s="T988">v:pred 0.2.h:S</ta>
            <ta e="T992" id="Seg_11561" s="T991">pro.h:S</ta>
            <ta e="T993" id="Seg_11562" s="T992">v:pred</ta>
            <ta e="T996" id="Seg_11563" s="T995">pro.h:S</ta>
            <ta e="T997" id="Seg_11564" s="T996">v:pred</ta>
            <ta e="T998" id="Seg_11565" s="T997">pro.h:S</ta>
            <ta e="T999" id="Seg_11566" s="T998">v:pred</ta>
            <ta e="T1002" id="Seg_11567" s="T1001">pro.h:S</ta>
            <ta e="T1004" id="Seg_11568" s="T1003">v:pred</ta>
            <ta e="T1006" id="Seg_11569" s="T1005">np.h:O</ta>
            <ta e="T1010" id="Seg_11570" s="T1009">v:pred 0.1.h:S</ta>
            <ta e="T1011" id="Seg_11571" s="T1010">v:pred 0.1.h:S</ta>
            <ta e="T1017" id="Seg_11572" s="T1016">np.h:S</ta>
            <ta e="T1018" id="Seg_11573" s="T1017">v:pred</ta>
            <ta e="T1019" id="Seg_11574" s="T1018">pro.h:O</ta>
            <ta e="T1020" id="Seg_11575" s="T1019">v:pred 0.2.h:S</ta>
            <ta e="T1022" id="Seg_11576" s="T1021">pro.h:S</ta>
            <ta e="T1023" id="Seg_11577" s="T1022">v:pred</ta>
            <ta e="T1024" id="Seg_11578" s="T1023">pro.h:O</ta>
            <ta e="T1026" id="Seg_11579" s="T1025">ptcl.neg</ta>
            <ta e="T1027" id="Seg_11580" s="T1026">v:pred 0.1.h:S</ta>
            <ta e="T1029" id="Seg_11581" s="T1028">pro.h:S</ta>
            <ta e="T1030" id="Seg_11582" s="T1029">v:pred</ta>
            <ta e="T1032" id="Seg_11583" s="T1031">pro.h:S</ta>
            <ta e="T1033" id="Seg_11584" s="T1032">v:pred</ta>
            <ta e="T1036" id="Seg_11585" s="T1035">np.h:S</ta>
            <ta e="T1037" id="Seg_11586" s="T1036">v:pred</ta>
            <ta e="T1076" id="Seg_11587" s="T1075">pro.h:S</ta>
            <ta e="T1077" id="Seg_11588" s="T1076">v:pred</ta>
            <ta e="T1080" id="Seg_11589" s="T1079">v:pred 0.2.h:S</ta>
            <ta e="T1081" id="Seg_11590" s="T1080">pro.h:S</ta>
            <ta e="T1082" id="Seg_11591" s="T1081">v:pred</ta>
            <ta e="T1089" id="Seg_11592" s="T1088">v:pred 0.2.h:S</ta>
            <ta e="T1092" id="Seg_11593" s="T1091">v:pred 0.3.h:S</ta>
            <ta e="T1179" id="Seg_11594" s="T1178">np.h:S</ta>
            <ta e="T1180" id="Seg_11595" s="T1179">cop</ta>
            <ta e="T1181" id="Seg_11596" s="T1180">n:pred</ta>
            <ta e="T1183" id="Seg_11597" s="T1182">pro.h:S</ta>
            <ta e="T1187" id="Seg_11598" s="T1186">np:O</ta>
            <ta e="T1188" id="Seg_11599" s="T1187">v:pred</ta>
            <ta e="T1191" id="Seg_11600" s="T1190">pro.h:S</ta>
            <ta e="T1739" id="Seg_11601" s="T1191">conv:pred</ta>
            <ta e="T1192" id="Seg_11602" s="T1739">v:pred</ta>
            <ta e="T1197" id="Seg_11603" s="T1196">v:pred</ta>
            <ta e="T1200" id="Seg_11604" s="T1199">np.h:S</ta>
            <ta e="T1202" id="Seg_11605" s="T1201">pro.h:S</ta>
            <ta e="T1204" id="Seg_11606" s="T1203">np:O</ta>
            <ta e="T1206" id="Seg_11607" s="T1205">v:pred</ta>
            <ta e="T1211" id="Seg_11608" s="T1210">v:pred 0.3.h:S</ta>
            <ta e="T1213" id="Seg_11609" s="T1212">v:pred 0.3.h:S</ta>
            <ta e="T1217" id="Seg_11610" s="T1216">np.h:S</ta>
            <ta e="T1221" id="Seg_11611" s="T1220">v:pred</ta>
            <ta e="T1223" id="Seg_11612" s="T1222">v:pred 0.3.h:S</ta>
            <ta e="T1226" id="Seg_11613" s="T1225">np:O</ta>
            <ta e="T1227" id="Seg_11614" s="T1226">v:pred 0.3.h:S</ta>
            <ta e="T1228" id="Seg_11615" s="T1227">np:S</ta>
            <ta e="T1230" id="Seg_11616" s="T1229">v:pred</ta>
            <ta e="T1231" id="Seg_11617" s="T1230">pro.h:S</ta>
            <ta e="T1232" id="Seg_11618" s="T1231">ptcl.neg</ta>
            <ta e="T1233" id="Seg_11619" s="T1232">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T50" id="Seg_11620" s="T49">RUS:gram</ta>
            <ta e="T100" id="Seg_11621" s="T99">RUS:gram</ta>
            <ta e="T148" id="Seg_11622" s="T147">RUS:gram</ta>
            <ta e="T163" id="Seg_11623" s="T162">RUS:gram</ta>
            <ta e="T168" id="Seg_11624" s="T167">RUS:gram</ta>
            <ta e="T186" id="Seg_11625" s="T185">RUS:cult</ta>
            <ta e="T187" id="Seg_11626" s="T186">RUS:cult</ta>
            <ta e="T189" id="Seg_11627" s="T188">RUS:cult</ta>
            <ta e="T191" id="Seg_11628" s="T190">RUS:gram</ta>
            <ta e="T192" id="Seg_11629" s="T191">TURK:core</ta>
            <ta e="T200" id="Seg_11630" s="T199">RUS:cult</ta>
            <ta e="T209" id="Seg_11631" s="T208">TAT:cult</ta>
            <ta e="T213" id="Seg_11632" s="T212">RUS:gram</ta>
            <ta e="T217" id="Seg_11633" s="T216">RUS:cult</ta>
            <ta e="T227" id="Seg_11634" s="T226">RUS:gram</ta>
            <ta e="T248" id="Seg_11635" s="T247">RUS:cult</ta>
            <ta e="T255" id="Seg_11636" s="T254">TURK:core</ta>
            <ta e="T257" id="Seg_11637" s="T256">RUS:mod</ta>
            <ta e="T260" id="Seg_11638" s="T259">RUS:gram</ta>
            <ta e="T263" id="Seg_11639" s="T262">TURK:core</ta>
            <ta e="T463" id="Seg_11640" s="T462">RUS:gram</ta>
            <ta e="T552" id="Seg_11641" s="T550">RUS:gram</ta>
            <ta e="T737" id="Seg_11642" s="T736">RUS:gram</ta>
            <ta e="T752" id="Seg_11643" s="T751">RUS:gram</ta>
            <ta e="T768" id="Seg_11644" s="T767">RUS:gram</ta>
            <ta e="T782" id="Seg_11645" s="T781">RUS:gram</ta>
            <ta e="T812" id="Seg_11646" s="T811">RUS:gram</ta>
            <ta e="T828" id="Seg_11647" s="T827">RUS:gram</ta>
            <ta e="T974" id="Seg_11648" s="T973">RUS:core</ta>
            <ta e="T988" id="Seg_11649" s="T987">RUS:disc</ta>
            <ta e="T1001" id="Seg_11650" s="T1000">RUS:gram</ta>
            <ta e="T1004" id="Seg_11651" s="T1003">RUS:mod</ta>
            <ta e="T1012" id="Seg_11652" s="T1011">RUS:cult</ta>
            <ta e="T1016" id="Seg_11653" s="T1015">TURK:core</ta>
            <ta e="T1024" id="Seg_11654" s="T1023">TURK:gram(INDEF)</ta>
            <ta e="T1031" id="Seg_11655" s="T1030">RUS:mod</ta>
            <ta e="T1083" id="Seg_11656" s="T1082">TURK:cult</ta>
            <ta e="T1086" id="Seg_11657" s="T1085">RUS:disc</ta>
            <ta e="T1095" id="Seg_11658" s="T1094">RUS:gram</ta>
            <ta e="T1179" id="Seg_11659" s="T1178">RUS:cult</ta>
            <ta e="T1181" id="Seg_11660" s="T1180">RUS:cult</ta>
            <ta e="T1193" id="Seg_11661" s="T1192">RUS:cult</ta>
            <ta e="T1195" id="Seg_11662" s="T1194">RUS:gram</ta>
            <ta e="T1199" id="Seg_11663" s="T1198">TURK:core</ta>
            <ta e="T1200" id="Seg_11664" s="T1199">RUS:cult</ta>
            <ta e="T1208" id="Seg_11665" s="T1207">TURK:gram(INDEF)</ta>
            <ta e="T1214" id="Seg_11666" s="T1213">RUS:mod</ta>
            <ta e="T1217" id="Seg_11667" s="T1216">RUS:cult</ta>
            <ta e="T1226" id="Seg_11668" s="T1225">TAT:cult</ta>
            <ta e="T1228" id="Seg_11669" s="T1227">TAT:cult</ta>
            <ta e="T1231" id="Seg_11670" s="T1230">TURK:gram(INDEF)</ta>
            <ta e="T1235" id="Seg_11671" s="T1234">TAT:cult</ta>
            <ta e="T1266" id="Seg_11672" s="T1265">RUS:gram</ta>
            <ta e="T1382" id="Seg_11673" s="T1380">RUS:gram</ta>
            <ta e="T1506" id="Seg_11674" s="T1504">RUS:gram</ta>
            <ta e="T1524" id="Seg_11675" s="T1523">RUS:gram</ta>
            <ta e="T1579" id="Seg_11676" s="T1578">RUS:gram</ta>
            <ta e="T1653" id="Seg_11677" s="T1652">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T16" id="Seg_11678" s="T6">RUS:ext</ta>
            <ta e="T31" id="Seg_11679" s="T29">RUS:ext</ta>
            <ta e="T43" id="Seg_11680" s="T35">RUS:ext</ta>
            <ta e="T55" id="Seg_11681" s="T44">RUS:ext</ta>
            <ta e="T62" id="Seg_11682" s="T56">RUS:ext</ta>
            <ta e="T68" id="Seg_11683" s="T63">RUS:ext</ta>
            <ta e="T74" id="Seg_11684" s="T69">RUS:ext</ta>
            <ta e="T84" id="Seg_11685" s="T75">RUS:ext</ta>
            <ta e="T89" id="Seg_11686" s="T85">RUS:ext</ta>
            <ta e="T95" id="Seg_11687" s="T90">RUS:ext</ta>
            <ta e="T106" id="Seg_11688" s="T95">RUS:ext</ta>
            <ta e="T113" id="Seg_11689" s="T107">RUS:ext</ta>
            <ta e="T118" id="Seg_11690" s="T114">RUS:ext</ta>
            <ta e="T122" id="Seg_11691" s="T119">RUS:ext</ta>
            <ta e="T130" id="Seg_11692" s="T123">RUS:ext</ta>
            <ta e="T137" id="Seg_11693" s="T131">RUS:ext</ta>
            <ta e="T146" id="Seg_11694" s="T138">RUS:ext</ta>
            <ta e="T154" id="Seg_11695" s="T147">RUS:ext</ta>
            <ta e="T161" id="Seg_11696" s="T155">RUS:ext</ta>
            <ta e="T172" id="Seg_11697" s="T162">RUS:ext</ta>
            <ta e="T179" id="Seg_11698" s="T173">RUS:ext</ta>
            <ta e="T183" id="Seg_11699" s="T180">RUS:ext</ta>
            <ta e="T269" id="Seg_11700" s="T266">RUS:ext</ta>
            <ta e="T288" id="Seg_11701" s="T286">RUS:ext</ta>
            <ta e="T308" id="Seg_11702" s="T292">RUS:ext</ta>
            <ta e="T310" id="Seg_11703" s="T308">RUS:ext</ta>
            <ta e="T340" id="Seg_11704" s="T323">RUS:ext</ta>
            <ta e="T350" id="Seg_11705" s="T348">RUS:ext</ta>
            <ta e="T358" id="Seg_11706" s="T352">RUS:ext</ta>
            <ta e="T363" id="Seg_11707" s="T359">RUS:ext</ta>
            <ta e="T369" id="Seg_11708" s="T363">RUS:ext</ta>
            <ta e="T383" id="Seg_11709" s="T371">RUS:ext</ta>
            <ta e="T422" id="Seg_11710" s="T421">RUS:ext</ta>
            <ta e="T426" id="Seg_11711" s="T425">RUS:ext</ta>
            <ta e="T429" id="Seg_11712" s="T427">RUS:ext</ta>
            <ta e="T432" id="Seg_11713" s="T430">RUS:ext</ta>
            <ta e="T434" id="Seg_11714" s="T433">RUS:ext</ta>
            <ta e="T438" id="Seg_11715" s="T435">RUS:ext</ta>
            <ta e="T455" id="Seg_11716" s="T443">RUS:ext</ta>
            <ta e="T461" id="Seg_11717" s="T458">RUS:ext</ta>
            <ta e="T470" id="Seg_11718" s="T462">RUS:ext</ta>
            <ta e="T502" id="Seg_11719" s="T497">RUS:ext</ta>
            <ta e="T525" id="Seg_11720" s="T520">RUS:ext</ta>
            <ta e="T532" id="Seg_11721" s="T527">RUS:ext</ta>
            <ta e="T544" id="Seg_11722" s="T533">RUS:ext</ta>
            <ta e="T561" id="Seg_11723" s="T549">RUS:ext</ta>
            <ta e="T574" id="Seg_11724" s="T564">RUS:ext</ta>
            <ta e="T582" id="Seg_11725" s="T576">RUS:ext</ta>
            <ta e="T587" id="Seg_11726" s="T583">RUS:ext</ta>
            <ta e="T606" id="Seg_11727" s="T592">RUS:ext</ta>
            <ta e="T610" id="Seg_11728" s="T607">RUS:ext</ta>
            <ta e="T625" id="Seg_11729" s="T611">RUS:ext</ta>
            <ta e="T631" id="Seg_11730" s="T626">RUS:ext</ta>
            <ta e="T645" id="Seg_11731" s="T632">RUS:ext</ta>
            <ta e="T650" id="Seg_11732" s="T645">RUS:ext</ta>
            <ta e="T655" id="Seg_11733" s="T650">RUS:ext</ta>
            <ta e="T667" id="Seg_11734" s="T656">RUS:ext</ta>
            <ta e="T672" id="Seg_11735" s="T667">RUS:ext</ta>
            <ta e="T681" id="Seg_11736" s="T673">RUS:ext</ta>
            <ta e="T688" id="Seg_11737" s="T682">RUS:ext</ta>
            <ta e="T701" id="Seg_11738" s="T689">RUS:ext</ta>
            <ta e="T712" id="Seg_11739" s="T702">RUS:ext</ta>
            <ta e="T717" id="Seg_11740" s="T712">RUS:ext</ta>
            <ta e="T720" id="Seg_11741" s="T718">RUS:ext</ta>
            <ta e="T727" id="Seg_11742" s="T721">RUS:ext</ta>
            <ta e="T735" id="Seg_11743" s="T728">RUS:ext</ta>
            <ta e="T742" id="Seg_11744" s="T736">RUS:ext</ta>
            <ta e="T750" id="Seg_11745" s="T743">RUS:ext</ta>
            <ta e="T761" id="Seg_11746" s="T751">RUS:ext</ta>
            <ta e="T771" id="Seg_11747" s="T762">RUS:ext</ta>
            <ta e="T777" id="Seg_11748" s="T772">RUS:ext</ta>
            <ta e="T786" id="Seg_11749" s="T778">RUS:ext</ta>
            <ta e="T792" id="Seg_11750" s="T787">RUS:ext</ta>
            <ta e="T797" id="Seg_11751" s="T793">RUS:ext</ta>
            <ta e="T806" id="Seg_11752" s="T798">RUS:ext</ta>
            <ta e="T818" id="Seg_11753" s="T807">RUS:ext</ta>
            <ta e="T823" id="Seg_11754" s="T819">RUS:ext</ta>
            <ta e="T834" id="Seg_11755" s="T824">RUS:ext</ta>
            <ta e="T838" id="Seg_11756" s="T835">RUS:ext</ta>
            <ta e="T854" id="Seg_11757" s="T845">RUS:ext</ta>
            <ta e="T859" id="Seg_11758" s="T856">RUS:ext</ta>
            <ta e="T871" id="Seg_11759" s="T860">RUS:ext</ta>
            <ta e="T878" id="Seg_11760" s="T872">RUS:ext</ta>
            <ta e="T888" id="Seg_11761" s="T879">RUS:ext</ta>
            <ta e="T905" id="Seg_11762" s="T889">RUS:ext</ta>
            <ta e="T915" id="Seg_11763" s="T909">RUS:ext</ta>
            <ta e="T918" id="Seg_11764" s="T916">RUS:ext</ta>
            <ta e="T933" id="Seg_11765" s="T929">RUS:ext</ta>
            <ta e="T954" id="Seg_11766" s="T949">RUS:ext</ta>
            <ta e="T959" id="Seg_11767" s="T957">RUS:ext</ta>
            <ta e="T1041" id="Seg_11768" s="T1038">RUS:ext</ta>
            <ta e="T1064" id="Seg_11769" s="T1061">RUS:ext</ta>
            <ta e="T1100" id="Seg_11770" s="T1094">RUS:ext</ta>
            <ta e="T1138" id="Seg_11771" s="T1133">RUS:ext</ta>
            <ta e="T1144" id="Seg_11772" s="T1142">RUS:ext</ta>
            <ta e="T1257" id="Seg_11773" s="T1248">RUS:ext</ta>
            <ta e="T1264" id="Seg_11774" s="T1257">RUS:ext</ta>
            <ta e="T1271" id="Seg_11775" s="T1265">RUS:ext</ta>
            <ta e="T1278" id="Seg_11776" s="T1272">RUS:ext</ta>
            <ta e="T1287" id="Seg_11777" s="T1279">RUS:ext</ta>
            <ta e="T1344" id="Seg_11778" s="T1341">RUS:ext</ta>
            <ta e="T1353" id="Seg_11779" s="T1347">RUS:ext</ta>
            <ta e="T1355" id="Seg_11780" s="T1354">RUS:ext</ta>
            <ta e="T1372" id="Seg_11781" s="T1356">RUS:ext</ta>
            <ta e="T1375" id="Seg_11782" s="T1372">RUS:ext</ta>
            <ta e="T1377" id="Seg_11783" s="T1376">RUS:ext</ta>
            <ta e="T1388" id="Seg_11784" s="T1380">RUS:ext</ta>
            <ta e="T1394" id="Seg_11785" s="T1389">RUS:ext</ta>
            <ta e="T1404" id="Seg_11786" s="T1394">RUS:ext</ta>
            <ta e="T1434" id="Seg_11787" s="T1433">RUS:ext</ta>
            <ta e="T1466" id="Seg_11788" s="T1465">RUS:ext</ta>
            <ta e="T1470" id="Seg_11789" s="T1467">RUS:ext</ta>
            <ta e="T1487" id="Seg_11790" s="T1471">RUS:ext</ta>
            <ta e="T1499" id="Seg_11791" s="T1488">RUS:ext</ta>
            <ta e="T1518" id="Seg_11792" s="T1502">RUS:ext</ta>
            <ta e="T1530" id="Seg_11793" s="T1522">RUS:ext</ta>
            <ta e="T1533" id="Seg_11794" s="T1531">RUS:ext</ta>
            <ta e="T1540" id="Seg_11795" s="T1534">RUS:ext</ta>
            <ta e="T1544" id="Seg_11796" s="T1542">RUS:ext</ta>
            <ta e="T1555" id="Seg_11797" s="T1545">RUS:ext</ta>
            <ta e="T1565" id="Seg_11798" s="T1556">RUS:ext</ta>
            <ta e="T1577" id="Seg_11799" s="T1566">RUS:ext</ta>
            <ta e="T1584" id="Seg_11800" s="T1578">RUS:ext</ta>
            <ta e="T1590" id="Seg_11801" s="T1585">RUS:ext</ta>
            <ta e="T1598" id="Seg_11802" s="T1591">RUS:ext</ta>
            <ta e="T1603" id="Seg_11803" s="T1599">RUS:ext</ta>
            <ta e="T1610" id="Seg_11804" s="T1606">RUS:ext</ta>
            <ta e="T1615" id="Seg_11805" s="T1611">RUS:ext</ta>
            <ta e="T1624" id="Seg_11806" s="T1616">RUS:ext</ta>
            <ta e="T1633" id="Seg_11807" s="T1625">RUS:ext</ta>
            <ta e="T1641" id="Seg_11808" s="T1634">RUS:ext</ta>
            <ta e="T1651" id="Seg_11809" s="T1644">RUS:ext</ta>
            <ta e="T1658" id="Seg_11810" s="T1652">RUS:ext</ta>
            <ta e="T1671" id="Seg_11811" s="T1662">RUS:ext</ta>
            <ta e="T1698" id="Seg_11812" s="T1680">RUS:ext</ta>
            <ta e="T1709" id="Seg_11813" s="T1699">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T194" id="Seg_11814" s="T184">Паша Василинич бросил свою Шуру и взял другую женщину.</ta>
            <ta e="T201" id="Seg_11815" s="T195">Потом она на него подала в суд.</ta>
            <ta e="T206" id="Seg_11816" s="T202">Они судились.</ta>
            <ta e="T211" id="Seg_11817" s="T207">Теперь он дает деньги детям.</ta>
            <ta e="T218" id="Seg_11818" s="T212">А она уехала туда, где жил Ленин.</ta>
            <ta e="T223" id="Seg_11819" s="T219">Теперь она там живет.</ta>
            <ta e="T234" id="Seg_11820" s="T224">Два мальчика и девочка с ней, один мальчик здесь.</ta>
            <ta e="T244" id="Seg_11821" s="T235">Потом моя (дочь?) приехала сюда, одного сына привезла.</ta>
            <ta e="T252" id="Seg_11822" s="T245">(Она?) говорит Паше: "Не бери двух сыновей!</ta>
            <ta e="T255" id="Seg_11823" s="T253">Не надо!"</ta>
            <ta e="T263" id="Seg_11824" s="T256">Небось его взяла, а детей [ей] не надо.</ta>
            <ta e="T977" id="Seg_11825" s="T969">Умер наш Загороднюк, уже два года назад.</ta>
            <ta e="T986" id="Seg_11826" s="T978">Когда я куда-нибудь еду, я говорю ему: "Ты поедешь?"</ta>
            <ta e="T989" id="Seg_11827" s="T987">"Ну, приходи".</ta>
            <ta e="T993" id="Seg_11828" s="T990">Я пришла.</ta>
            <ta e="T999" id="Seg_11829" s="T994">Тогда он говорит: "Ты пришла.</ta>
            <ta e="T1007" id="Seg_11830" s="T1000">А я хотел послать свою жену за тобой".</ta>
            <ta e="T1012" id="Seg_11831" s="T1008">Тогда мы сели, поехали к конторе.</ta>
            <ta e="T1020" id="Seg_11832" s="T1013">Другая женщина подошла: "Меня возьмешь?"</ta>
            <ta e="T1027" id="Seg_11833" s="T1020">Он говорит: "Я никого не возьму".</ta>
            <ta e="T1033" id="Seg_11834" s="T1028">Я подумала: еще кто-нибудь поедет.</ta>
            <ta e="T1037" id="Seg_11835" s="T1034">Потом поехали втроем.</ta>
            <ta e="T1080" id="Seg_11836" s="T1073">Потом я говорю ему [= он говорит мне]: "Куда ты едешь?"</ta>
            <ta e="T1084" id="Seg_11837" s="T1080">Я говорю: "Богу помолиться".</ta>
            <ta e="T1089" id="Seg_11838" s="T1085">"Ну, завтра со мной поедешь".</ta>
            <ta e="T1093" id="Seg_11839" s="T1090">Так он мне сказал.</ta>
            <ta e="T1141" id="Seg_11840" s="T1139">Два года.</ta>
            <ta e="T1148" id="Seg_11841" s="T1142">Два года, два года.</ta>
            <ta e="T1181" id="Seg_11842" s="T1177">У нас был бригадир, немец.</ta>
            <ta e="T1188" id="Seg_11843" s="T1182">Он часто лошадей давал людям.</ta>
            <ta e="T1193" id="Seg_11844" s="T1189">Теперь он уехал в Волгоград.</ta>
            <ta e="T1206" id="Seg_11845" s="T1194">А когда к нам приехал новый председатель, он людям отдал лошадей.</ta>
            <ta e="T1214" id="Seg_11846" s="T1207">И тем, кто за ягодами ездил на лошади, тоже давал.</ta>
            <ta e="T1223" id="Seg_11847" s="T1215">Этот председатель у нас уехал в Вознесенское, там живет.</ta>
            <ta e="T1227" id="Seg_11848" s="T1224">Ему дом поставили.</ta>
            <ta e="T1235" id="Seg_11849" s="T1227">Его дом теперь стоит, никто не живет в этом доме.</ta>
            <ta e="T1237" id="Seg_11850" s="T1236">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T16" id="Seg_11851" s="T6">Well, I can tell you in Russian.</ta>
            <ta e="T31" id="Seg_11852" s="T29">Is ist running?</ta>
            <ta e="T43" id="Seg_11853" s="T35">Pasha Vasilinich and his Shura broke up.</ta>
            <ta e="T55" id="Seg_11854" s="T44">She left everything in the house, and he only got one motorcycle.</ta>
            <ta e="T62" id="Seg_11855" s="T56">He contacted one woman there.</ta>
            <ta e="T68" id="Seg_11856" s="T63">She worked as an accountant in an office.</ta>
            <ta e="T74" id="Seg_11857" s="T69">She also had a husband.</ta>
            <ta e="T84" id="Seg_11858" s="T75">The drunk got drunk and slept, his hand froze, he lay in the hospital.</ta>
            <ta e="T89" id="Seg_11859" s="T85">And he went to his mother.</ta>
            <ta e="T95" id="Seg_11860" s="T90">She send everything…</ta>
            <ta e="T106" id="Seg_11861" s="T95">And she got along with Pasha, and Pasha split up with his wife.</ta>
            <ta e="T113" id="Seg_11862" s="T107">And she sued, he …</ta>
            <ta e="T118" id="Seg_11863" s="T114">Judged with his alimony.</ta>
            <ta e="T122" id="Seg_11864" s="T119">All three pays.</ta>
            <ta e="T130" id="Seg_11865" s="T123">When she got ready, she went there, to Sushenskoye.</ta>
            <ta e="T137" id="Seg_11866" s="T131">He did not give the boy to her.</ta>
            <ta e="T146" id="Seg_11867" s="T138">But she wanted to take everything away, he took him away.</ta>
            <ta e="T154" id="Seg_11868" s="T147">And now our village is connected with Voznesenka.</ta>
            <ta e="T161" id="Seg_11869" s="T155">And there he lives with her.</ta>
            <ta e="T172" id="Seg_11870" s="T162">And mother Evonna lives here, and the hut stands alone, empty.</ta>
            <ta e="T179" id="Seg_11871" s="T173">Nobody takes it, people all leave.</ta>
            <ta e="T183" id="Seg_11872" s="T180">From that village.</ta>
            <ta e="T194" id="Seg_11873" s="T184">Pasha Vasilinich abandoned his Shura and took another woman.</ta>
            <ta e="T201" id="Seg_11874" s="T195">She sue him.</ta>
            <ta e="T206" id="Seg_11875" s="T202">They went to court.</ta>
            <ta e="T211" id="Seg_11876" s="T207">Now he's paying money to [his] children.</ta>
            <ta e="T218" id="Seg_11877" s="T212">And she went where Lenin had lived.</ta>
            <ta e="T223" id="Seg_11878" s="T219">Now she's living there.</ta>
            <ta e="T234" id="Seg_11879" s="T224">Two boys and a girl are with her, one boy is there.</ta>
            <ta e="T244" id="Seg_11880" s="T235">Then my (daughter?) came (here?) and brought one son.</ta>
            <ta e="T252" id="Seg_11881" s="T245">(She?) said to Pasha: "Don't take two sons!</ta>
            <ta e="T255" id="Seg_11882" s="T253">Don't."</ta>
            <ta e="T263" id="Seg_11883" s="T256">She took him, and doesn't want the children.</ta>
            <ta e="T269" id="Seg_11884" s="T266">Only in your own way.</ta>
            <ta e="T288" id="Seg_11885" s="T286">I told it.</ta>
            <ta e="T308" id="Seg_11886" s="T292">(Well) everything, how she met Pasha, how about the children, how she sued.</ta>
            <ta e="T310" id="Seg_11887" s="T308">Alimony denied.</ta>
            <ta e="T340" id="Seg_11888" s="T323">Well, I spoke with her.</ta>
            <ta e="T350" id="Seg_11889" s="T348">I told that.</ta>
            <ta e="T358" id="Seg_11890" s="T352">And how he froze his hands.</ta>
            <ta e="T363" id="Seg_11891" s="T359">Well, is he talking?</ta>
            <ta e="T369" id="Seg_11892" s="T363">I already…</ta>
            <ta e="T383" id="Seg_11893" s="T371">Did I tell you in Russian or not, only in my own way?</ta>
            <ta e="T422" id="Seg_11894" s="T421">Here?</ta>
            <ta e="T426" id="Seg_11895" s="T425">I forgot about it.</ta>
            <ta e="T429" id="Seg_11896" s="T427">Well, let's go.</ta>
            <ta e="T432" id="Seg_11897" s="T430">How will I start?</ta>
            <ta e="T434" id="Seg_11898" s="T433">In my own way?</ta>
            <ta e="T438" id="Seg_11899" s="T435">Tell it in Russian?</ta>
            <ta e="T455" id="Seg_11900" s="T443">No, I said right now how he got along with her.</ta>
            <ta e="T461" id="Seg_11901" s="T458">In my own dialect.</ta>
            <ta e="T470" id="Seg_11902" s="T462">I didn't tell it in Russian.</ta>
            <ta e="T502" id="Seg_11903" s="T497">Well, what's new …</ta>
            <ta e="T525" id="Seg_11904" s="T520">No, this is …</ta>
            <ta e="T532" id="Seg_11905" s="T527">Now Zagorodniuk has died.</ta>
            <ta e="T544" id="Seg_11906" s="T533">I can talk about him, you did not know about him?</ta>
            <ta e="T561" id="Seg_11907" s="T549">Well, didn’t you know that he was dead?</ta>
            <ta e="T574" id="Seg_11908" s="T564">It’s been already two years since he died.</ta>
            <ta e="T582" id="Seg_11909" s="T576">So I cried a lot about him.</ta>
            <ta e="T587" id="Seg_11910" s="T583">He was a good person.</ta>
            <ta e="T606" id="Seg_11911" s="T592">Here, it’s happened, I’ll come, where should I go, to Uyar or where.</ta>
            <ta e="T610" id="Seg_11912" s="T607">To Aginsky.</ta>
            <ta e="T631" id="Seg_11913" s="T626">I lived against me. </ta>
            <ta e="T645" id="Seg_11914" s="T632">When he goes, I go to meet him, I will say: "Vasily Fedorovich, the cars will go, can I leave?"</ta>
            <ta e="T650" id="Seg_11915" s="T645">He will say, "Tomorrow with me."</ta>
            <ta e="T655" id="Seg_11916" s="T650">And where, he says, do you want to go?</ta>
            <ta e="T667" id="Seg_11917" s="T656">I say: "Yes (…) I will tell you as my son, I’m going to pray to God.</ta>
            <ta e="T672" id="Seg_11918" s="T667">"Tomorrow you will definitely leave with me."</ta>
            <ta e="T681" id="Seg_11919" s="T673">Well, I get up in the morning, put everything up and …</ta>
            <ta e="T688" id="Seg_11920" s="T682">Do you know where they lived?</ta>
            <ta e="T701" id="Seg_11921" s="T689">(Now) I’ll come here to them, he isn’t there yet from this office.</ta>
            <ta e="T712" id="Seg_11922" s="T702">I am sitting, he enters: "Oh, you are here, girl, already," he will say.</ta>
            <ta e="T717" id="Seg_11923" s="T712">He kept calling me a girl.</ta>
            <ta e="T720" id="Seg_11924" s="T718">“Here,” I say.</ta>
            <ta e="T727" id="Seg_11925" s="T721">"I wanted to send Linka for you."</ta>
            <ta e="T735" id="Seg_11926" s="T728">Her name is Lizaveta, he called her Linka.</ta>
            <ta e="T742" id="Seg_11927" s="T736">And her name was Linka.</ta>
            <ta e="T750" id="Seg_11928" s="T743">Well, we came to the office with him.</ta>
            <ta e="T761" id="Seg_11929" s="T751">And there one woman ran up, said: "Maybe you will take me with you?"</ta>
            <ta e="T771" id="Seg_11930" s="T762">"No, he says, I won’t take anyone," and I’m sitting there.</ta>
            <ta e="T777" id="Seg_11931" s="T772">I think maybe someone else will go.</ta>
            <ta e="T786" id="Seg_11932" s="T778">They took three, drove away, but didn’t take that woman.</ta>
            <ta e="T792" id="Seg_11933" s="T787">As I say, I’ll definitely take it.</ta>
            <ta e="T797" id="Seg_11934" s="T793">If he doesn’t go …</ta>
            <ta e="T806" id="Seg_11935" s="T798">I’ll ask, he won’t go: “Okay, I’ll tell you.”</ta>
            <ta e="T818" id="Seg_11936" s="T807">I think: well, I’ll get ready, I’ll go, otherwise I’ll say how he will know.</ta>
            <ta e="T823" id="Seg_11937" s="T819">Forgetting - there are a lot of people …</ta>
            <ta e="T834" id="Seg_11938" s="T824">I go (meet), and there - you know Augusta, (German) our.</ta>
            <ta e="T838" id="Seg_11939" s="T835">He's the leader, August.</ta>
            <ta e="T854" id="Seg_11940" s="T845">He left now.</ta>
            <ta e="T859" id="Seg_11941" s="T856">To Volgograd.</ta>
            <ta e="T871" id="Seg_11942" s="T860">His son got married, also to Volgograd, and left for Volgograd.</ta>
            <ta e="T878" id="Seg_11943" s="T872">There we have all sorts of people now.</ta>
            <ta e="T915" id="Seg_11944" s="T909">And here it is …</ta>
            <ta e="T918" id="Seg_11945" s="T916">Again.</ta>
            <ta e="T933" id="Seg_11946" s="T929">Zagorodniuk?</ta>
            <ta e="T954" id="Seg_11947" s="T949">Well, let's start.</ta>
            <ta e="T959" id="Seg_11948" s="T957">Recorded?</ta>
            <ta e="T977" id="Seg_11949" s="T969">Our Zagorodnyuk was dead, it's already two years ago.</ta>
            <ta e="T986" id="Seg_11950" s="T978">When I go somewhere, I tell him: "Will you go?"</ta>
            <ta e="T989" id="Seg_11951" s="T987">"Well, come."</ta>
            <ta e="T993" id="Seg_11952" s="T990">I came.</ta>
            <ta e="T999" id="Seg_11953" s="T994">Then he says: "You've come.</ta>
            <ta e="T1007" id="Seg_11954" s="T1000">I was going to send my wife for you."</ta>
            <ta e="T1012" id="Seg_11955" s="T1008">Then we got in, went to the farm office.</ta>
            <ta e="T1020" id="Seg_11956" s="T1013">Another woman came: "Will you take me?"</ta>
            <ta e="T1027" id="Seg_11957" s="T1020">He says: "I won't take anyone."</ta>
            <ta e="T1033" id="Seg_11958" s="T1028">I thought that somebody else will go.</ta>
            <ta e="T1037" id="Seg_11959" s="T1034">Then three of us went.</ta>
            <ta e="T1041" id="Seg_11960" s="T1038">Well, that's all.</ta>
            <ta e="T1064" id="Seg_11961" s="T1061">I did tell!</ta>
            <ta e="T1080" id="Seg_11962" s="T1073">Then I say to him [= he says to me]: "Where do you go?"</ta>
            <ta e="T1084" id="Seg_11963" s="T1080">I say: "To pray to God."</ta>
            <ta e="T1089" id="Seg_11964" s="T1085">"Well, tomorrow you'll go with me."</ta>
            <ta e="T1093" id="Seg_11965" s="T1090">He told me so.</ta>
            <ta e="T1100" id="Seg_11966" s="T1094">And so I told you everything.</ta>
            <ta e="T1138" id="Seg_11967" s="T1133">I told, I told.</ta>
            <ta e="T1141" id="Seg_11968" s="T1139">Two years.</ta>
            <ta e="T1148" id="Seg_11969" s="T1142">Two years, two years.</ta>
            <ta e="T1181" id="Seg_11970" s="T1177">We had a brigade-leader, a German.</ta>
            <ta e="T1188" id="Seg_11971" s="T1182">He often gave people horses.</ta>
            <ta e="T1193" id="Seg_11972" s="T1189">Now he's moved to Volgograd.</ta>
            <ta e="T1206" id="Seg_11973" s="T1194">When the new chairman has come to us, he gave people horses.</ta>
            <ta e="T1214" id="Seg_11974" s="T1207">He gave [them] also to those who was going with a horse to gather berries.</ta>
            <ta e="T1223" id="Seg_11975" s="T1215">This chairman of ours went to Voznesenskoe, he lives there.</ta>
            <ta e="T1227" id="Seg_11976" s="T1224">They built him a house.</ta>
            <ta e="T1235" id="Seg_11977" s="T1227">His house is standing now, nobody lives in this house.</ta>
            <ta e="T1237" id="Seg_11978" s="T1236">That's all.</ta>
            <ta e="T1257" id="Seg_11979" s="T1248">We have a good chairman, he treated people well.</ta>
            <ta e="T1264" id="Seg_11980" s="T1257">He gave horses, even to people picking berries.</ta>
            <ta e="T1271" id="Seg_11981" s="T1265">And now we are connected to Voznesenskoe.</ta>
            <ta e="T1278" id="Seg_11982" s="T1272">He built a house here in Abalakova.</ta>
            <ta e="T1287" id="Seg_11983" s="T1279">And now he’s gone to Voznesenskoe, he lives there.</ta>
            <ta e="T1344" id="Seg_11984" s="T1341">And now there is such.</ta>
            <ta e="T1353" id="Seg_11985" s="T1347">They sell such a (strong) wine directly in half a liter.</ta>
            <ta e="T1355" id="Seg_11986" s="T1354">Alcohol.</ta>
            <ta e="T1372" id="Seg_11987" s="T1356">Well, he’s so, for health we’ll drink so much, it’s better somehow as this moonshine.</ta>
            <ta e="T1375" id="Seg_11988" s="T1372">He is pure after all.</ta>
            <ta e="T1377" id="Seg_11989" s="T1376">Well.</ta>
            <ta e="T1388" id="Seg_11990" s="T1380">And you, that store we have now …</ta>
            <ta e="T1394" id="Seg_11991" s="T1389">Have you been to this store?</ta>
            <ta e="T1404" id="Seg_11992" s="T1394">We now have another, on that hill, do you know it? </ta>
            <ta e="T1434" id="Seg_11993" s="T1433">Well…</ta>
            <ta e="T1466" id="Seg_11994" s="T1465">Wait a minute…</ta>
            <ta e="T1470" id="Seg_11995" s="T1467">Well here it is …</ta>
            <ta e="T1487" id="Seg_11996" s="T1471">As you go, you still don’t cross the ravine, here was the store, and here the ravine.</ta>
            <ta e="T1499" id="Seg_11997" s="T1488">And now in the ravine, there was my birth house. </ta>
            <ta e="T1518" id="Seg_11998" s="T1502">Well, how did it burn there, not with you?</ta>
            <ta e="T1530" id="Seg_11999" s="T1522">It burned down, and now they opened a store on this place.</ta>
            <ta e="T1533" id="Seg_12000" s="T1531">Large, extensive.</ta>
            <ta e="T1540" id="Seg_12001" s="T1534">Here it is bigger, perhaps, it will be this house.</ta>
            <ta e="T1544" id="Seg_12002" s="T1542">There (bricks) …</ta>
            <ta e="T1555" id="Seg_12003" s="T1545">No, it's wooden, but nice, big.</ta>
            <ta e="T1565" id="Seg_12004" s="T1556">So you’ll come in, all the shelves are laid here, and then …</ta>
            <ta e="T1577" id="Seg_12005" s="T1566">And then here is laid out such as it seems like this furnace.</ta>
            <ta e="T1590" id="Seg_12006" s="T1585">Burned on this spot.</ta>
            <ta e="T1598" id="Seg_12007" s="T1591">Now (is is extinguished), now the iron stove is standing there.</ta>
            <ta e="T1603" id="Seg_12008" s="T1599">Why it burned, I don't know.</ta>
            <ta e="T1610" id="Seg_12009" s="T1606">And then there.</ta>
            <ta e="T1615" id="Seg_12010" s="T1611">And back there. </ta>
            <ta e="T1624" id="Seg_12011" s="T1616">They’ve already brought a [baking] machine, they’re putting it there.</ta>
            <ta e="T1633" id="Seg_12012" s="T1625">And some kind of fish, so they bring it here.</ta>
            <ta e="T1641" id="Seg_12013" s="T1634">Barrels of beer are brought in, and then different wines.</ta>
            <ta e="T1651" id="Seg_12014" s="T1644">Now the store is ready, there are two sales woman.</ta>
            <ta e="T1658" id="Seg_12015" s="T1652">And now my granddaughter is one of the woman. </ta>
            <ta e="T1671" id="Seg_12016" s="T1662">Here you might know, in front of Zagorodniuk lived Nikolai Murachev.</ta>
            <ta e="T1698" id="Seg_12017" s="T1680">Well (he) Nikolai Murachev, this is where you were at the flat, and here he is.</ta>
            <ta e="T1709" id="Seg_12018" s="T1699">On the corner, behind that, there is only one more house, Zagorodniuk.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T16" id="Seg_12019" s="T6">Nun, ich kann es dir auf Russisch sagen.</ta>
            <ta e="T31" id="Seg_12020" s="T29">Läuft es?</ta>
            <ta e="T43" id="Seg_12021" s="T35">Pasha Vasilinich und seine Shura haben sich getrennt.</ta>
            <ta e="T55" id="Seg_12022" s="T44">Sie hat alles im Haus gelassen und er hat nur ein Motorrad bekommen.</ta>
            <ta e="T62" id="Seg_12023" s="T56">Er kontaktierte dort eine Frau.</ta>
            <ta e="T68" id="Seg_12024" s="T63">Sie arbeitete als Buchhalterin in einem Büro.</ta>
            <ta e="T74" id="Seg_12025" s="T69">Sie hatte auch einen Ehemann.</ta>
            <ta e="T84" id="Seg_12026" s="T75">Der Betrunkene wurde betrunken und schlief ein, seine Hand erstarrte, er lag im Krankenhaus.</ta>
            <ta e="T89" id="Seg_12027" s="T85">Und er ging zu seiner Mutter.</ta>
            <ta e="T95" id="Seg_12028" s="T90">Sie schickte alles…</ta>
            <ta e="T106" id="Seg_12029" s="T95">Und sie vertrug sich mit Pascha, und Pascha trennte sich von seiner Frau.</ta>
            <ta e="T113" id="Seg_12030" s="T107">Und sie hat geklagt, er …</ta>
            <ta e="T118" id="Seg_12031" s="T114">Bekommt Unterhalt von ihm.</ta>
            <ta e="T122" id="Seg_12032" s="T119">Alle drei zahlen.</ta>
            <ta e="T130" id="Seg_12033" s="T123">Als sie fertig war, ging sie dorthin, nach Sushenskoye.</ta>
            <ta e="T137" id="Seg_12034" s="T131">Er gab ihr den Jungen nicht.</ta>
            <ta e="T146" id="Seg_12035" s="T138">Aber sie wollte alles wegnehmen, er nahm ihn weg.</ta>
            <ta e="T154" id="Seg_12036" s="T147">Und jetzt ist unser Dorf mit Voznesenka verbunden.</ta>
            <ta e="T161" id="Seg_12037" s="T155">Und dort wohnt er bei ihr.</ta>
            <ta e="T172" id="Seg_12038" s="T162">Und Mutter Evonna wohnt hier und die Hütte steht allein, leer.</ta>
            <ta e="T179" id="Seg_12039" s="T173">Niemand nimmt es, alle Leute zerstreuen sich.</ta>
            <ta e="T183" id="Seg_12040" s="T180">Aus diesem Dorf.</ta>
            <ta e="T194" id="Seg_12041" s="T184">Pasha Vasilinich hat seine Shura verlassen und sich eine andere Frau genommen.</ta>
            <ta e="T201" id="Seg_12042" s="T195">Sie verklagt ihn.</ta>
            <ta e="T206" id="Seg_12043" s="T202">Sie gingen vor Gericht.</ta>
            <ta e="T211" id="Seg_12044" s="T207">Jetzt bezahlt er Geld an [seine] Kinder.</ta>
            <ta e="T218" id="Seg_12045" s="T212">Und sie ging dorthin, wo Lenin gelebt hatte. </ta>
            <ta e="T223" id="Seg_12046" s="T219">Jetzt lebt sie dort.</ta>
            <ta e="T234" id="Seg_12047" s="T224">Zwei Jungen und ein Mädchen sind bei ihr, ein Junge ist dort.</ta>
            <ta e="T244" id="Seg_12048" s="T235">Dann kam meine (Tochter?) (hierher?) und brachte einen Sohn.</ta>
            <ta e="T252" id="Seg_12049" s="T245">(Sie?) sagte zu Pasha: "Nimm nicht meine zwei Söhne!</ta>
            <ta e="T255" id="Seg_12050" s="T253">Tu es nicht!"</ta>
            <ta e="T263" id="Seg_12051" s="T256">Sie nahm ihn und wollte die Kinder nicht.</ta>
            <ta e="T269" id="Seg_12052" s="T266">Nur auf deine Weise.</ta>
            <ta e="T288" id="Seg_12053" s="T286">Ich habe es erzählt.</ta>
            <ta e="T308" id="Seg_12054" s="T292">(Nun) alles, wie sie mit Pascha zusammenkam, wie mit den Kinder, wie sie klagte.</ta>
            <ta e="T310" id="Seg_12055" s="T308">Unterhalt verweigert. </ta>
            <ta e="T340" id="Seg_12056" s="T323">Nun, ich habe mit ihr gesprochen.</ta>
            <ta e="T350" id="Seg_12057" s="T348">Ich habe es erzählt.</ta>
            <ta e="T358" id="Seg_12058" s="T352">Und wie seine Hände gefroren sind.</ta>
            <ta e="T363" id="Seg_12059" s="T359">Nun, redet er?</ta>
            <ta e="T369" id="Seg_12060" s="T363">Ich schon…</ta>
            <ta e="T383" id="Seg_12061" s="T371">Habe ich es dir auf Russisch gesagt oder nicht, nur auf meine eigene Art?</ta>
            <ta e="T422" id="Seg_12062" s="T421">Hier?</ta>
            <ta e="T426" id="Seg_12063" s="T425">Ich habe es vergessen.</ta>
            <ta e="T429" id="Seg_12064" s="T427">Nun, los.</ta>
            <ta e="T432" id="Seg_12065" s="T430">Wie fange ich an?</ta>
            <ta e="T434" id="Seg_12066" s="T433">Auf meine Weise?</ta>
            <ta e="T438" id="Seg_12067" s="T435">Auf Russisch erzählen?</ta>
            <ta e="T455" id="Seg_12068" s="T443">Nein, ich sagte gerade, wie er mit ihr klar gekommen ist.</ta>
            <ta e="T461" id="Seg_12069" s="T458">In meinem eigenen Dialekt.</ta>
            <ta e="T470" id="Seg_12070" s="T462">Aber auf Russisch habe ich es nicht gesagt.</ta>
            <ta e="T502" id="Seg_12071" s="T497">Nun, was ist neu …</ta>
            <ta e="T525" id="Seg_12072" s="T520">Nein, das ist …</ta>
            <ta e="T532" id="Seg_12073" s="T527">Jetzt ist Zagorodniuk bei uns gestorben.</ta>
            <ta e="T544" id="Seg_12074" s="T533">Ich kann über ihn reden, du weißt nichts über ihn?</ta>
            <ta e="T561" id="Seg_12075" s="T549">Wusstest du nicht, dass er tot ist?</ta>
            <ta e="T574" id="Seg_12076" s="T564">Es sind bereits zwei Jahre vergangen, seit er gestorben ist.</ta>
            <ta e="T582" id="Seg_12077" s="T576">Also habe ich viel über ihn geweint.</ta>
            <ta e="T587" id="Seg_12078" s="T583">Er war ein guter Mensch.</ta>
            <ta e="T606" id="Seg_12079" s="T592">Hier ist es passiert, ich werde kommen, wohin muss ich gehen, nach Uyar oder wohin.</ta>
            <ta e="T610" id="Seg_12080" s="T607">Nach Aginsky.</ta>
            <ta e="T631" id="Seg_12081" s="T626">Er hat gegen mich gelebt.</ta>
            <ta e="T645" id="Seg_12082" s="T632">Wenn er geht, gehe ich ihm entgegen, ich werde sagen: "Vasily Fedorovich, die Autos fahren los, kann ich gehen?"</ta>
            <ta e="T650" id="Seg_12083" s="T645">Er wird sagen: "Morgen mit mir."</ta>
            <ta e="T655" id="Seg_12084" s="T650">Und wohin, sagt er, willst du?</ta>
            <ta e="T667" id="Seg_12085" s="T656">Ich sage: "Ja (…) Ich werde dir wie meinem Sohn sagen, dass ich zu Gott beten werde. </ta>
            <ta e="T672" id="Seg_12086" s="T667">"Morgen wirst du definitiv mit mir gehen."</ta>
            <ta e="T681" id="Seg_12087" s="T673">Nun, ich stehe morgens auf, stelle alles auf und …</ta>
            <ta e="T688" id="Seg_12088" s="T682">Weißt du, wo sie gelebt haben?</ta>
            <ta e="T701" id="Seg_12089" s="T689">Jetzt komme ich zu ihnen, er ist noch nicht aus diesem Büro.</ta>
            <ta e="T712" id="Seg_12090" s="T702">Ich sitze, er tritt ein: "Oh, du bist schon da, Mädchen", wird er sagen.</ta>
            <ta e="T717" id="Seg_12091" s="T712">Er nannte mich immer wieder ein Mädchen.</ta>
            <ta e="T720" id="Seg_12092" s="T718">"Hier", sage ich.</ta>
            <ta e="T727" id="Seg_12093" s="T721">"Ich wollte Linka nach dir schicken."</ta>
            <ta e="T735" id="Seg_12094" s="T728">Sie heißt Lizaveta, er hat sie Linka genannt.</ta>
            <ta e="T742" id="Seg_12095" s="T736">Und sie hieß Linka.</ta>
            <ta e="T750" id="Seg_12096" s="T743">Wir sind mit ihm ins Büro gekommen.</ta>
            <ta e="T761" id="Seg_12097" s="T751">Und da lief eine Frau auf mich zu und sagte: "Vielleicht nimmst du mich mit?"</ta>
            <ta e="T771" id="Seg_12098" s="T762">"Nein, sagt er, ich nehme niemanden", und ich sitze da.</ta>
            <ta e="T777" id="Seg_12099" s="T772">Ich denke, vielleicht geht jemand anderes.</ta>
            <ta e="T786" id="Seg_12100" s="T778">Sie haben drei genommen, sind weggefahren, haben aber diese Frau nicht mitgenommen.</ta>
            <ta e="T792" id="Seg_12101" s="T787">Wie gesagt, ich nehme es auf jeden Fall.</ta>
            <ta e="T797" id="Seg_12102" s="T793">Wenn er nicht geht …</ta>
            <ta e="T806" id="Seg_12103" s="T798">Ich werde fragen, wenn er nicht geht: "Okay, ich werde es dir sagen."</ta>
            <ta e="T818" id="Seg_12104" s="T807">Ich denke: Nun, ich werde mich fertig machen, ich werde gehen, sonst werde ich sagen, wie er es wissen wird.</ta>
            <ta e="T823" id="Seg_12105" s="T819">Vergessen - es gibt viele Leute …</ta>
            <ta e="T834" id="Seg_12106" s="T824">Ich gehe (treffe), und dort, du kennst August, unser (Deutscher)</ta>
            <ta e="T838" id="Seg_12107" s="T835">Er ist der Vorarbeiter, August.</ta>
            <ta e="T854" id="Seg_12108" s="T845">Er ist jetzt gegangen.</ta>
            <ta e="T859" id="Seg_12109" s="T856">Nach Wolgograd.</ta>
            <ta e="T871" id="Seg_12110" s="T860">Sein Sohn heiratete ebenfalls nach Wolgograd und ging nach Wolgograd.</ta>
            <ta e="T878" id="Seg_12111" s="T872">Dort haben wir jetzt alle möglichen Leute.</ta>
            <ta e="T915" id="Seg_12112" s="T909">Und hier ist es …</ta>
            <ta e="T918" id="Seg_12113" s="T916">Wieder.</ta>
            <ta e="T933" id="Seg_12114" s="T929">Zagorodniuk?</ta>
            <ta e="T954" id="Seg_12115" s="T949">Nun, los.</ta>
            <ta e="T959" id="Seg_12116" s="T957">Aufgenommen? </ta>
            <ta e="T977" id="Seg_12117" s="T969">Unser Zagorodnyuk war tot, schon seit zwei Jahren.</ta>
            <ta e="T986" id="Seg_12118" s="T978">Als ich irgendwohin ging, sagte ich ihm: "Wirst du gehen?"</ta>
            <ta e="T989" id="Seg_12119" s="T987">"Nun, komm."</ta>
            <ta e="T993" id="Seg_12120" s="T990">Ich kam.</ta>
            <ta e="T999" id="Seg_12121" s="T994">Dann sagte er: "Du bist gekommen.</ta>
            <ta e="T1007" id="Seg_12122" s="T1000">Ich wollte meine Frau nach dir schicken."</ta>
            <ta e="T1012" id="Seg_12123" s="T1008">Dann kamen wir rein, gingen in die Geschäftsstelle.</ta>
            <ta e="T1020" id="Seg_12124" s="T1013">Eine andere Frau kam: "Wirst du mich mitnehmen?"</ta>
            <ta e="T1027" id="Seg_12125" s="T1020">Er sagt: "Ich werde niemanden mitnehmen."</ta>
            <ta e="T1033" id="Seg_12126" s="T1028">Ich dachte, dass jemand anderes gehen wird.</ta>
            <ta e="T1037" id="Seg_12127" s="T1034">Dann gingen wir drei los.</ta>
            <ta e="T1041" id="Seg_12128" s="T1038">Nun, das ist alles.</ta>
            <ta e="T1064" id="Seg_12129" s="T1061">Ich habe es erzählt!</ta>
            <ta e="T1080" id="Seg_12130" s="T1073">Dann sagte ich zu ihm [= er sagte zu mir]: "Wohin gehst du?"</ta>
            <ta e="T1084" id="Seg_12131" s="T1080">Ich sagte: "Um zu beten."</ta>
            <ta e="T1089" id="Seg_12132" s="T1085">"Nun, morgen wirst du mit mir kommen."</ta>
            <ta e="T1093" id="Seg_12133" s="T1090">Er sagte es mir so.</ta>
            <ta e="T1100" id="Seg_12134" s="T1094">Und so habe ich dir alles erzählt.</ta>
            <ta e="T1138" id="Seg_12135" s="T1133">Ich habe erzählt, ich habe erzählt.</ta>
            <ta e="T1141" id="Seg_12136" s="T1139">Zwei Jahre.</ta>
            <ta e="T1148" id="Seg_12137" s="T1142">Zwei Jahre, zwei Jahre.</ta>
            <ta e="T1181" id="Seg_12138" s="T1177">Wir hatten einen Brigadeführer, einen Deutschen.</ta>
            <ta e="T1188" id="Seg_12139" s="T1182">Er gab Leuten oft Pferde.</ta>
            <ta e="T1193" id="Seg_12140" s="T1189">Jetzt ist er nach Wolgograd gezogen.</ta>
            <ta e="T1206" id="Seg_12141" s="T1194">Als der neue Vorsitzende zu uns gekommen ist, gab er den Leuten Pferde.</ta>
            <ta e="T1214" id="Seg_12142" s="T1207">Er gab [sie] auch denen, die mit Pferden Beeren sammeln gingen.</ta>
            <ta e="T1223" id="Seg_12143" s="T1215">Unser Vorsitzender ging nach Voznesenskoe, er lebt dort.</ta>
            <ta e="T1227" id="Seg_12144" s="T1224">Sie bauten ihm ein Haus.</ta>
            <ta e="T1235" id="Seg_12145" s="T1227">Sein Haus steht jetzt, niemand lebt in diesem Haus.</ta>
            <ta e="T1237" id="Seg_12146" s="T1236">Das ist alles.</ta>
            <ta e="T1257" id="Seg_12147" s="T1248">Wir haben einen guten Vorsitzenden, er hat die Menschen gut behandelt.</ta>
            <ta e="T1264" id="Seg_12148" s="T1257">Er verteilte Pferde, sogar an die Menschen, die Beeren sammeln.</ta>
            <ta e="T1271" id="Seg_12149" s="T1265">Und jetzt sind wir mit Voznesenskoe verbunden.</ta>
            <ta e="T1278" id="Seg_12150" s="T1272">Er hat hier in Abalakova ein Haus gebaut.</ta>
            <ta e="T1287" id="Seg_12151" s="T1279">Und jetzt ist er nach Voznesenskoe gegangen, er lebt dort.</ta>
            <ta e="T1344" id="Seg_12152" s="T1341">Und nun gibt es dort solchen.</ta>
            <ta e="T1353" id="Seg_12153" s="T1347">Sie verkaufen einen solchen (starken) Wein direkt in einem halben Liter.</ta>
            <ta e="T1355" id="Seg_12154" s="T1354">Akohol.</ta>
            <ta e="T1372" id="Seg_12155" s="T1356">Nun, er ist so, für die Gesundheit trinken wir so, es ist irgendwie besser als die Schwarzgebrannten (Mondschein).</ta>
            <ta e="T1375" id="Seg_12156" s="T1372">Immerhin ist er rein.</ta>
            <ta e="T1377" id="Seg_12157" s="T1376">Nun.</ta>
            <ta e="T1388" id="Seg_12158" s="T1380">Und du, das Geschäft, das wir jetzt haben …</ta>
            <ta e="T1394" id="Seg_12159" s="T1389">Warst du schon in diesem Laden?</ta>
            <ta e="T1404" id="Seg_12160" s="T1394">Wir haben jetzt einen anderen auf diesem Hügel, kennst du ihn?</ta>
            <ta e="T1434" id="Seg_12161" s="T1433">Nun…</ta>
            <ta e="T1466" id="Seg_12162" s="T1465">Moment mal …</ta>
            <ta e="T1470" id="Seg_12163" s="T1467">Nun, hier ist es …</ta>
            <ta e="T1487" id="Seg_12164" s="T1471">Während du gehst, überquerst du immer noch nicht die Schlucht, hier war der Laden und hier die Schlucht.</ta>
            <ta e="T1499" id="Seg_12165" s="T1488">Und jetzt in der Schlucht, dort war mein Geburtshaus.</ta>
            <ta e="T1518" id="Seg_12166" s="T1502">Nun, wie hat es dort gebrannt, nicht bei dir?</ta>
            <ta e="T1530" id="Seg_12167" s="T1522">Er ist niedergebrannt, und jetzt haben sie einen Laden an diesem Ort eröffnet.</ta>
            <ta e="T1533" id="Seg_12168" s="T1531">Groß, umfangreich.</ta>
            <ta e="T1540" id="Seg_12169" s="T1534">Dort ist es größer, vielleicht wird es dieses Haus sein.</ta>
            <ta e="T1544" id="Seg_12170" s="T1542">Dort (Ziegel-) …</ta>
            <ta e="T1555" id="Seg_12171" s="T1545">Nein, es ist aus Holz, aber schön, groß.</ta>
            <ta e="T1565" id="Seg_12172" s="T1556">Du kommst also herein, alle Regale werden hier abgelegt und dann …</ta>
            <ta e="T1577" id="Seg_12173" s="T1566">Und dann ist hier so angelegt, wie es scheint wie dieser Ofen.</ta>
            <ta e="T1590" id="Seg_12174" s="T1585">An diesem Ort verbrannt.</ta>
            <ta e="T1598" id="Seg_12175" s="T1591">Dann (erlosch es), jetzt steht der Eisenofen da.</ta>
            <ta e="T1603" id="Seg_12176" s="T1599">Warum es verbrannt ist, weiß ich nicht.</ta>
            <ta e="T1610" id="Seg_12177" s="T1606">Und dann da.</ta>
            <ta e="T1615" id="Seg_12178" s="T1611">Und da hinten.</ta>
            <ta e="T1624" id="Seg_12179" s="T1616">Sie haben bereits eine [] Maschine mitgebracht und stellen sie dort ab.</ta>
            <ta e="T1633" id="Seg_12180" s="T1625">Und was für Fisch, also bringt sie ihn hierher.</ta>
            <ta e="T1641" id="Seg_12181" s="T1634">Es werden Fässer mit Bier und dann Wein gebracht.</ta>
            <ta e="T1651" id="Seg_12182" s="T1644">Der Laden ist jetzt fertig, zwei Verkäuferinnen sind da. </ta>
            <ta e="T1658" id="Seg_12183" s="T1652">Und jetzt ist meine Enkelin eine der Frauen. </ta>
            <ta e="T1671" id="Seg_12184" s="T1662">Hier könntest du wissen, vor Zagorodniuk lebte Nikolai Murachev. </ta>
            <ta e="T1698" id="Seg_12185" s="T1680">Nun, Nikolay Murachev, hier warst du in der Wohnung und hier ist er.</ta>
            <ta e="T1709" id="Seg_12186" s="T1699">An der Ecke, dahinter gibt es nur noch ein Haus, Zagorodniuk.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T206" id="Seg_12187" s="T202">[GVY:] Or 'he was tried'. In both case, "dĭ" is unclear.</ta>
            <ta e="T234" id="Seg_12188" s="T224">[GVY:] The transcription is tentative.</ta>
            <ta e="T1089" id="Seg_12189" s="T1085">[GVY:] karədʼan?</ta>
            <ta e="T1188" id="Seg_12190" s="T1182">[GVY:] ugandə as 'often'?</ta>
            <ta e="T1214" id="Seg_12191" s="T1207">[GVY:] keʔbde ile?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T1738" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1739" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1534" />
            <conversion-tli id="T1535" />
            <conversion-tli id="T1536" />
            <conversion-tli id="T1537" />
            <conversion-tli id="T1538" />
            <conversion-tli id="T1539" />
            <conversion-tli id="T1540" />
            <conversion-tli id="T1541" />
            <conversion-tli id="T1542" />
            <conversion-tli id="T1543" />
            <conversion-tli id="T1544" />
            <conversion-tli id="T1545" />
            <conversion-tli id="T1546" />
            <conversion-tli id="T1547" />
            <conversion-tli id="T1548" />
            <conversion-tli id="T1549" />
            <conversion-tli id="T1550" />
            <conversion-tli id="T1551" />
            <conversion-tli id="T1552" />
            <conversion-tli id="T1553" />
            <conversion-tli id="T1554" />
            <conversion-tli id="T1555" />
            <conversion-tli id="T1556" />
            <conversion-tli id="T1557" />
            <conversion-tli id="T1558" />
            <conversion-tli id="T1559" />
            <conversion-tli id="T1560" />
            <conversion-tli id="T1561" />
            <conversion-tli id="T1562" />
            <conversion-tli id="T1563" />
            <conversion-tli id="T1564" />
            <conversion-tli id="T1565" />
            <conversion-tli id="T1566" />
            <conversion-tli id="T1567" />
            <conversion-tli id="T1568" />
            <conversion-tli id="T1569" />
            <conversion-tli id="T1570" />
            <conversion-tli id="T1571" />
            <conversion-tli id="T1572" />
            <conversion-tli id="T1573" />
            <conversion-tli id="T1574" />
            <conversion-tli id="T1575" />
            <conversion-tli id="T1576" />
            <conversion-tli id="T1577" />
            <conversion-tli id="T1578" />
            <conversion-tli id="T1579" />
            <conversion-tli id="T1580" />
            <conversion-tli id="T1581" />
            <conversion-tli id="T1582" />
            <conversion-tli id="T1583" />
            <conversion-tli id="T1584" />
            <conversion-tli id="T1585" />
            <conversion-tli id="T1586" />
            <conversion-tli id="T1587" />
            <conversion-tli id="T1588" />
            <conversion-tli id="T1589" />
            <conversion-tli id="T1590" />
            <conversion-tli id="T1591" />
            <conversion-tli id="T1592" />
            <conversion-tli id="T1593" />
            <conversion-tli id="T1594" />
            <conversion-tli id="T1595" />
            <conversion-tli id="T1596" />
            <conversion-tli id="T1597" />
            <conversion-tli id="T1598" />
            <conversion-tli id="T1599" />
            <conversion-tli id="T1600" />
            <conversion-tli id="T1601" />
            <conversion-tli id="T1602" />
            <conversion-tli id="T1603" />
            <conversion-tli id="T1604" />
            <conversion-tli id="T1605" />
            <conversion-tli id="T1606" />
            <conversion-tli id="T1607" />
            <conversion-tli id="T1608" />
            <conversion-tli id="T1609" />
            <conversion-tli id="T1610" />
            <conversion-tli id="T1611" />
            <conversion-tli id="T1612" />
            <conversion-tli id="T1613" />
            <conversion-tli id="T1614" />
            <conversion-tli id="T1615" />
            <conversion-tli id="T1616" />
            <conversion-tli id="T1617" />
            <conversion-tli id="T1618" />
            <conversion-tli id="T1619" />
            <conversion-tli id="T1620" />
            <conversion-tli id="T1621" />
            <conversion-tli id="T1622" />
            <conversion-tli id="T1623" />
            <conversion-tli id="T1624" />
            <conversion-tli id="T1625" />
            <conversion-tli id="T1626" />
            <conversion-tli id="T1627" />
            <conversion-tli id="T1628" />
            <conversion-tli id="T1629" />
            <conversion-tli id="T1630" />
            <conversion-tli id="T1631" />
            <conversion-tli id="T1632" />
            <conversion-tli id="T1633" />
            <conversion-tli id="T1634" />
            <conversion-tli id="T1635" />
            <conversion-tli id="T1636" />
            <conversion-tli id="T1637" />
            <conversion-tli id="T1638" />
            <conversion-tli id="T1639" />
            <conversion-tli id="T1640" />
            <conversion-tli id="T1641" />
            <conversion-tli id="T1642" />
            <conversion-tli id="T1643" />
            <conversion-tli id="T1644" />
            <conversion-tli id="T1645" />
            <conversion-tli id="T1646" />
            <conversion-tli id="T1647" />
            <conversion-tli id="T1648" />
            <conversion-tli id="T1649" />
            <conversion-tli id="T1650" />
            <conversion-tli id="T1651" />
            <conversion-tli id="T1652" />
            <conversion-tli id="T1653" />
            <conversion-tli id="T1654" />
            <conversion-tli id="T1655" />
            <conversion-tli id="T1656" />
            <conversion-tli id="T1657" />
            <conversion-tli id="T1658" />
            <conversion-tli id="T1659" />
            <conversion-tli id="T1660" />
            <conversion-tli id="T1661" />
            <conversion-tli id="T1662" />
            <conversion-tli id="T1663" />
            <conversion-tli id="T1664" />
            <conversion-tli id="T1665" />
            <conversion-tli id="T1666" />
            <conversion-tli id="T1667" />
            <conversion-tli id="T1668" />
            <conversion-tli id="T1669" />
            <conversion-tli id="T1670" />
            <conversion-tli id="T1671" />
            <conversion-tli id="T1672" />
            <conversion-tli id="T1673" />
            <conversion-tli id="T1674" />
            <conversion-tli id="T1675" />
            <conversion-tli id="T1676" />
            <conversion-tli id="T1677" />
            <conversion-tli id="T1678" />
            <conversion-tli id="T1679" />
            <conversion-tli id="T1680" />
            <conversion-tli id="T1681" />
            <conversion-tli id="T1682" />
            <conversion-tli id="T1683" />
            <conversion-tli id="T1684" />
            <conversion-tli id="T1685" />
            <conversion-tli id="T1686" />
            <conversion-tli id="T1687" />
            <conversion-tli id="T1688" />
            <conversion-tli id="T1689" />
            <conversion-tli id="T1690" />
            <conversion-tli id="T1691" />
            <conversion-tli id="T1692" />
            <conversion-tli id="T1693" />
            <conversion-tli id="T1694" />
            <conversion-tli id="T1695" />
            <conversion-tli id="T1696" />
            <conversion-tli id="T1697" />
            <conversion-tli id="T1698" />
            <conversion-tli id="T1699" />
            <conversion-tli id="T1700" />
            <conversion-tli id="T1701" />
            <conversion-tli id="T1702" />
            <conversion-tli id="T1703" />
            <conversion-tli id="T1704" />
            <conversion-tli id="T1705" />
            <conversion-tli id="T1706" />
            <conversion-tli id="T1707" />
            <conversion-tli id="T1708" />
            <conversion-tli id="T1709" />
            <conversion-tli id="T1710" />
            <conversion-tli id="T1711" />
            <conversion-tli id="T1712" />
            <conversion-tli id="T1713" />
            <conversion-tli id="T1714" />
            <conversion-tli id="T1715" />
            <conversion-tli id="T1716" />
            <conversion-tli id="T1717" />
            <conversion-tli id="T1718" />
            <conversion-tli id="T1719" />
            <conversion-tli id="T1720" />
            <conversion-tli id="T1721" />
            <conversion-tli id="T1722" />
            <conversion-tli id="T1723" />
            <conversion-tli id="T1724" />
            <conversion-tli id="T1725" />
            <conversion-tli id="T1726" />
            <conversion-tli id="T1727" />
            <conversion-tli id="T1728" />
            <conversion-tli id="T1729" />
            <conversion-tli id="T1730" />
            <conversion-tli id="T1731" />
            <conversion-tli id="T1732" />
            <conversion-tli id="T1733" />
            <conversion-tli id="T1734" />
            <conversion-tli id="T1735" />
            <conversion-tli id="T1736" />
            <conversion-tli id="T1737" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
