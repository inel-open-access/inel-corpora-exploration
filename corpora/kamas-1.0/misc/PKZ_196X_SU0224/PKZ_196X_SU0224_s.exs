<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF0966203-1DCC-5A7B-4B23-093E84DAB3F3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0224.wav" />
         <referenced-file url="PKZ_196X_SU0224.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0224\PKZ_196X_SU0224.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1778</ud-information>
            <ud-information attribute-name="# HIAT:w">1093</ud-information>
            <ud-information attribute-name="# e">1119</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">30</ud-information>
            <ud-information attribute-name="# HIAT:u">230</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.786" type="appl" />
         <tli id="T2" time="1.571" type="appl" />
         <tli id="T3" time="2.357" type="appl" />
         <tli id="T4" time="3.142" type="appl" />
         <tli id="T5" time="3.928" type="appl" />
         <tli id="T6" time="4.714" type="appl" />
         <tli id="T7" time="5.499" type="appl" />
         <tli id="T8" time="6.3733083958245444" />
         <tli id="T9" time="7.21" type="appl" />
         <tli id="T10" time="8.07" type="appl" />
         <tli id="T11" time="9.819961576411666" />
         <tli id="T12" time="10.652" type="appl" />
         <tli id="T13" time="11.458" type="appl" />
         <tli id="T14" time="12.265" type="appl" />
         <tli id="T15" time="13.072" type="appl" />
         <tli id="T16" time="13.878" type="appl" />
         <tli id="T17" time="14.685" type="appl" />
         <tli id="T18" time="15.73" type="appl" />
         <tli id="T19" time="16.57" type="appl" />
         <tli id="T20" time="17.605" type="appl" />
         <tli id="T21" time="19.117" type="appl" />
         <tli id="T22" time="20.144" type="appl" />
         <tli id="T23" time="21.171" type="appl" />
         <tli id="T24" time="22.198" type="appl" />
         <tli id="T25" time="23.225" type="appl" />
         <tli id="T26" time="24.252" type="appl" />
         <tli id="T27" time="25.279" type="appl" />
         <tli id="T28" time="26.306" type="appl" />
         <tli id="T29" time="27.333" type="appl" />
         <tli id="T30" time="28.36" type="appl" />
         <tli id="T31" time="29.387" type="appl" />
         <tli id="T32" time="30.414" type="appl" />
         <tli id="T33" time="31.441" type="appl" />
         <tli id="T34" time="32.197" type="appl" />
         <tli id="T35" time="32.933" type="appl" />
         <tli id="T36" time="34.76653063203451" />
         <tli id="T37" time="36.32" type="appl" />
         <tli id="T38" time="37.78" type="appl" />
         <tli id="T39" time="39.24" type="appl" />
         <tli id="T40" time="40.7" type="appl" />
         <tli id="T41" time="42.77316597030364" />
         <tli id="T42" time="43.669" type="appl" />
         <tli id="T43" time="44.498" type="appl" />
         <tli id="T44" time="45.328" type="appl" />
         <tli id="T45" time="46.157" type="appl" />
         <tli id="T46" time="46.986" type="appl" />
         <tli id="T47" time="47.653146875893135" />
         <tli id="T48" time="48.897" type="appl" />
         <tli id="T49" time="49.904" type="appl" />
         <tli id="T50" time="50.912" type="appl" />
         <tli id="T51" time="51.919" type="appl" />
         <tli id="T52" time="52.926" type="appl" />
         <tli id="T53" time="53.933" type="appl" />
         <tli id="T54" time="54.94" type="appl" />
         <tli id="T55" time="55.948" type="appl" />
         <tli id="T56" time="56.955" type="appl" />
         <tli id="T57" time="57.962" type="appl" />
         <tli id="T58" time="59.225" type="appl" />
         <tli id="T59" time="60.449" type="appl" />
         <tli id="T60" time="61.672" type="appl" />
         <tli id="T61" time="63.89974997278059" />
         <tli id="T62" time="64.859" type="appl" />
         <tli id="T63" time="65.561" type="appl" />
         <tli id="T64" time="66.263" type="appl" />
         <tli id="T65" time="66.965" type="appl" />
         <tli id="T66" time="67.667" type="appl" />
         <tli id="T67" time="68.684" type="appl" />
         <tli id="T68" time="69.392" type="appl" />
         <tli id="T69" time="70.1" type="appl" />
         <tli id="T70" time="70.807" type="appl" />
         <tli id="T71" time="71.959" type="appl" />
         <tli id="T72" time="72.932" type="appl" />
         <tli id="T73" time="73.904" type="appl" />
         <tli id="T1117" time="74.321" type="intp" />
         <tli id="T74" time="74.877" type="appl" />
         <tli id="T75" time="75.874" type="appl" />
         <tli id="T76" time="76.722" type="appl" />
         <tli id="T77" time="77.57" type="appl" />
         <tli id="T78" time="78.47302628373505" />
         <tli id="T79" time="79.507" type="appl" />
         <tli id="T80" time="80.433" type="appl" />
         <tli id="T81" time="81.36" type="appl" />
         <tli id="T82" time="82.287" type="appl" />
         <tli id="T83" time="83.213" type="appl" />
         <tli id="T84" time="84.14" type="appl" />
         <tli id="T85" time="85.80633092328212" />
         <tli id="T86" time="86.668" type="appl" />
         <tli id="T87" time="87.356" type="appl" />
         <tli id="T88" time="88.044" type="appl" />
         <tli id="T89" time="88.732" type="appl" />
         <tli id="T90" time="89.81298191270737" />
         <tli id="T91" time="90.962" type="appl" />
         <tli id="T92" time="91.968" type="appl" />
         <tli id="T93" time="93.4796342324809" />
         <tli id="T94" time="96.55962218109069" />
         <tli id="T95" time="97.37" type="appl" />
         <tli id="T96" time="98.02" type="appl" />
         <tli id="T97" time="98.67" type="appl" />
         <tli id="T98" time="99.32" type="appl" />
         <tli id="T99" time="99.939" type="appl" />
         <tli id="T100" time="100.543" type="appl" />
         <tli id="T101" time="101.147" type="appl" />
         <tli id="T102" time="101.751" type="appl" />
         <tli id="T1118" time="102.0898581801478" type="intp" />
         <tli id="T103" time="102.54166908701154" />
         <tli id="T1119" time="102.98951412377545" type="intp" />
         <tli id="T104" time="103.512" type="appl" />
         <tli id="T105" time="104.563" type="appl" />
         <tli id="T106" time="105.615" type="appl" />
         <tli id="T107" time="106.667" type="appl" />
         <tli id="T108" time="107.718" type="appl" />
         <tli id="T109" time="108.77" type="appl" />
         <tli id="T110" time="110.1595689671598" />
         <tli id="T111" time="110.842" type="appl" />
         <tli id="T112" time="111.434" type="appl" />
         <tli id="T113" time="112.026" type="appl" />
         <tli id="T114" time="112.618" type="appl" />
         <tli id="T115" time="113.27289011867659" />
         <tli id="T116" time="114.79288417123726" />
         <tli id="T117" time="121.26619217578289" />
         <tli id="T118" time="122.409" type="appl" />
         <tli id="T119" time="123.568" type="appl" />
         <tli id="T120" time="124.728" type="appl" />
         <tli id="T121" time="125.887" type="appl" />
         <tli id="T122" time="127.046" type="appl" />
         <tli id="T123" time="128.6194967370742" />
         <tli id="T124" time="129.593" type="appl" />
         <tli id="T125" time="130.232" type="appl" />
         <tli id="T126" time="130.87" type="appl" />
         <tli id="T127" time="131.508" type="appl" />
         <tli id="T128" time="132.147" type="appl" />
         <tli id="T129" time="133.4194779556868" />
         <tli id="T130" time="134.293" type="appl" />
         <tli id="T131" time="135.495" type="appl" />
         <tli id="T132" time="136.698" type="appl" />
         <tli id="T133" time="138.0727930815085" />
         <tli id="T134" time="138.56" type="appl" />
         <tli id="T135" time="139.174" type="appl" />
         <tli id="T136" time="139.789" type="appl" />
         <tli id="T137" time="140.403" type="appl" />
         <tli id="T138" time="141.018" type="appl" />
         <tli id="T139" time="141.632" type="appl" />
         <tli id="T140" time="142.247" type="appl" />
         <tli id="T141" time="142.861" type="appl" />
         <tli id="T142" time="143.476" type="appl" />
         <tli id="T143" time="144.09" type="appl" />
         <tli id="T144" time="144.705" type="appl" />
         <tli id="T145" time="146.47" type="appl" />
         <tli id="T146" time="147.415" type="appl" />
         <tli id="T147" time="148.159" type="appl" />
         <tli id="T148" time="148.904" type="appl" />
         <tli id="T149" time="149.648" type="appl" />
         <tli id="T150" time="150.393" type="appl" />
         <tli id="T151" time="156.47" type="appl" />
         <tli id="T152" time="157.182" type="appl" />
         <tli id="T153" time="157.674" type="appl" />
         <tli id="T154" time="158.166" type="appl" />
         <tli id="T155" time="158.658" type="appl" />
         <tli id="T156" time="159.15" type="appl" />
         <tli id="T157" time="159.828" type="appl" />
         <tli id="T158" time="160.455" type="appl" />
         <tli id="T159" time="161.082" type="appl" />
         <tli id="T160" time="161.71" type="appl" />
         <tli id="T161" time="162.39" type="appl" />
         <tli id="T162" time="163.02" type="appl" />
         <tli id="T163" time="163.65" type="appl" />
         <tli id="T164" time="164.28" type="appl" />
         <tli id="T165" time="164.91" type="appl" />
         <tli id="T166" time="165.77" type="appl" />
         <tli id="T167" time="166.3" type="appl" />
         <tli id="T168" time="167.02601312655662" />
         <tli id="T169" time="167.86" type="appl" />
         <tli id="T170" time="168.6" type="appl" />
         <tli id="T171" time="169.34" type="appl" />
         <tli id="T172" time="170.99933091307483" />
         <tli id="T173" time="171.842" type="appl" />
         <tli id="T174" time="172.428" type="appl" />
         <tli id="T175" time="173.0259896498224" />
         <tli id="T176" time="175.00598190250008" />
         <tli id="T177" time="175.622" type="appl" />
         <tli id="T178" time="176.194" type="appl" />
         <tli id="T179" time="176.766" type="appl" />
         <tli id="T180" time="177.338" type="appl" />
         <tli id="T181" time="177.91" type="appl" />
         <tli id="T182" time="178.945" type="appl" />
         <tli id="T183" time="179.695" type="appl" />
         <tli id="T184" time="180.445" type="appl" />
         <tli id="T185" time="181.315" type="appl" />
         <tli id="T186" time="182.15" type="appl" />
         <tli id="T187" time="182.985" type="appl" />
         <tli id="T188" time="183.82" type="appl" />
         <tli id="T189" time="184.655" type="appl" />
         <tli id="T190" time="185.49" type="appl" />
         <tli id="T191" time="186.372" type="appl" />
         <tli id="T192" time="186.834" type="appl" />
         <tli id="T193" time="187.296" type="appl" />
         <tli id="T194" time="187.758" type="appl" />
         <tli id="T195" time="188.22" type="appl" />
         <tli id="T196" time="189.029" type="appl" />
         <tli id="T197" time="189.798" type="appl" />
         <tli id="T198" time="190.567" type="appl" />
         <tli id="T199" time="191.336" type="appl" />
         <tli id="T200" time="192.104" type="appl" />
         <tli id="T201" time="192.873" type="appl" />
         <tli id="T202" time="193.642" type="appl" />
         <tli id="T203" time="194.411" type="appl" />
         <tli id="T204" time="195.4392352845108" />
         <tli id="T205" time="196.036" type="appl" />
         <tli id="T206" time="196.591" type="appl" />
         <tli id="T207" time="197.147" type="appl" />
         <tli id="T208" time="197.703" type="appl" />
         <tli id="T209" time="198.259" type="appl" />
         <tli id="T210" time="198.814" type="appl" />
         <tli id="T211" time="199.73255181893654" />
         <tli id="T212" time="200.726" type="appl" />
         <tli id="T213" time="201.581" type="appl" />
         <tli id="T214" time="202.437" type="appl" />
         <tli id="T215" time="203.293" type="appl" />
         <tli id="T216" time="204.149" type="appl" />
         <tli id="T217" time="205.004" type="appl" />
         <tli id="T218" time="207.70585395429862" />
         <tli id="T219" time="208.545" type="appl" />
         <tli id="T220" time="209.201" type="appl" />
         <tli id="T221" time="209.856" type="appl" />
         <tli id="T222" time="210.512" type="appl" />
         <tli id="T223" time="211.167" type="appl" />
         <tli id="T224" time="211.823" type="appl" />
         <tli id="T225" time="212.478" type="appl" />
         <tli id="T226" time="213.134" type="appl" />
         <tli id="T227" time="213.789" type="appl" />
         <tli id="T228" time="214.445" type="appl" />
         <tli id="T229" time="215.1" type="appl" />
         <tli id="T230" time="216.42581983477822" />
         <tli id="T231" time="217.299" type="appl" />
         <tli id="T232" time="217.868" type="appl" />
         <tli id="T233" time="218.438" type="appl" />
         <tli id="T234" time="219.007" type="appl" />
         <tli id="T235" time="219.576" type="appl" />
         <tli id="T236" time="220.145" type="appl" />
         <tli id="T237" time="220.714" type="appl" />
         <tli id="T238" time="221.283" type="appl" />
         <tli id="T239" time="221.852" type="appl" />
         <tli id="T240" time="222.422" type="appl" />
         <tli id="T241" time="222.991" type="appl" />
         <tli id="T242" time="223.56" type="appl" />
         <tli id="T243" time="224.33" type="appl" />
         <tli id="T244" time="225.3191183703744" />
         <tli id="T245" time="226.985778515726" />
         <tli id="T246" time="227.905" type="appl" />
         <tli id="T247" time="228.745" type="appl" />
         <tli id="T248" time="229.585" type="appl" />
         <tli id="T249" time="230.67909739782516" />
         <tli id="T250" time="231.23" type="appl" />
         <tli id="T251" time="231.85" type="appl" />
         <tli id="T252" time="232.62575644759585" />
         <tli id="T253" time="233.524" type="appl" />
         <tli id="T254" time="234.182" type="appl" />
         <tli id="T255" time="234.841" type="appl" />
         <tli id="T256" time="235.499" type="appl" />
         <tli id="T257" time="236.158" type="appl" />
         <tli id="T258" time="236.816" type="appl" />
         <tli id="T259" time="237.475" type="appl" />
         <tli id="T260" time="238.422" type="appl" />
         <tli id="T261" time="239.29239702900225" />
         <tli id="T262" time="239.915" type="appl" />
         <tli id="T263" time="240.47" type="appl" />
         <tli id="T264" time="241.025" type="appl" />
         <tli id="T265" time="241.58" type="appl" />
         <tli id="T266" time="242.135" type="appl" />
         <tli id="T267" time="242.69" type="appl" />
         <tli id="T268" time="243.245" type="appl" />
         <tli id="T269" time="244.23904434040585" />
         <tli id="T270" time="245.318" type="appl" />
         <tli id="T271" time="246.255" type="appl" />
         <tli id="T272" time="247.193" type="appl" />
         <tli id="T273" time="248.13" type="appl" />
         <tli id="T274" time="249.068" type="appl" />
         <tli id="T275" time="250.31235391006706" />
         <tli id="T276" time="251.604" type="appl" />
         <tli id="T277" time="252.829" type="appl" />
         <tli id="T278" time="253.95233966751502" />
         <tli id="T279" time="254.772" type="appl" />
         <tli id="T280" time="255.565" type="appl" />
         <tli id="T281" time="256.357" type="appl" />
         <tli id="T282" time="257.3989928481021" />
         <tli id="T283" time="258.323" type="appl" />
         <tli id="T284" time="259.176" type="appl" />
         <tli id="T285" time="260.028" type="appl" />
         <tli id="T286" time="260.881" type="appl" />
         <tli id="T287" time="261.734" type="appl" />
         <tli id="T288" time="262.587" type="appl" />
         <tli id="T289" time="264.3589656150904" />
         <tli id="T290" time="265.135" type="appl" />
         <tli id="T291" time="265.837" type="appl" />
         <tli id="T292" time="266.539" type="appl" />
         <tli id="T293" time="267.241" type="appl" />
         <tli id="T294" time="268.205617230562" />
         <tli id="T295" time="268.721" type="appl" />
         <tli id="T296" time="269.259" type="appl" />
         <tli id="T297" time="269.797" type="appl" />
         <tli id="T298" time="270.335" type="appl" />
         <tli id="T299" time="271.7322700981259" />
         <tli id="T300" time="272.454" type="appl" />
         <tli id="T301" time="273.058" type="appl" />
         <tli id="T302" time="273.661" type="appl" />
         <tli id="T303" time="274.265" type="appl" />
         <tli id="T304" time="274.869" type="appl" />
         <tli id="T305" time="275.473" type="appl" />
         <tli id="T306" time="276.076" type="appl" />
         <tli id="T307" time="276.68" type="appl" />
         <tli id="T308" time="277.284" type="appl" />
         <tli id="T309" time="277.888" type="appl" />
         <tli id="T310" time="278.492" type="appl" />
         <tli id="T311" time="279.095" type="appl" />
         <tli id="T312" time="279.699" type="appl" />
         <tli id="T313" time="280.303" type="appl" />
         <tli id="T314" time="281.195" type="appl" />
         <tli id="T315" time="281.99" type="appl" />
         <tli id="T316" time="282.785" type="appl" />
         <tli id="T317" time="283.581" type="appl" />
         <tli id="T318" time="284.376" type="appl" />
         <tli id="T319" time="285.171" type="appl" />
         <tli id="T320" time="285.751" type="appl" />
         <tli id="T321" time="286.292" type="appl" />
         <tli id="T322" time="286.833" type="appl" />
         <tli id="T323" time="287.374" type="appl" />
         <tli id="T324" time="287.916" type="appl" />
         <tli id="T325" time="288.457" type="appl" />
         <tli id="T326" time="288.998" type="appl" />
         <tli id="T327" time="289.539" type="appl" />
         <tli id="T328" time="290.08" type="appl" />
         <tli id="T329" time="290.693" type="appl" />
         <tli id="T330" time="291.237" type="appl" />
         <tli id="T331" time="291.78" type="appl" />
         <tli id="T332" time="292.323" type="appl" />
         <tli id="T333" time="292.867" type="appl" />
         <tli id="T334" time="293.41" type="appl" />
         <tli id="T335" time="293.953" type="appl" />
         <tli id="T336" time="294.497" type="appl" />
         <tli id="T337" time="295.3521776780489" />
         <tli id="T338" time="296.83217188712115" />
         <tli id="T339" time="297.617" type="appl" />
         <tli id="T340" time="298.183" type="appl" />
         <tli id="T341" time="298.75" type="appl" />
         <tli id="T342" time="299.317" type="appl" />
         <tli id="T343" time="299.883" type="appl" />
         <tli id="T344" time="300.45" type="appl" />
         <tli id="T345" time="301.017" type="appl" />
         <tli id="T346" time="301.583" type="appl" />
         <tli id="T347" time="302.41215005375835" />
         <tli id="T348" time="304.445" type="appl" />
         <tli id="T349" time="305.162" type="appl" />
         <tli id="T350" time="305.79" type="appl" />
         <tli id="T351" time="306.418" type="appl" />
         <tli id="T352" time="307.045" type="appl" />
         <tli id="T353" time="308.441" type="appl" />
         <tli id="T354" time="309.513" type="appl" />
         <tli id="T355" time="310.584" type="appl" />
         <tli id="T356" time="311.656" type="appl" />
         <tli id="T357" time="312.727" type="appl" />
         <tli id="T358" time="313.799" type="appl" />
         <tli id="T359" time="314.87" type="appl" />
         <tli id="T360" time="315.797" type="appl" />
         <tli id="T361" time="316.553" type="appl" />
         <tli id="T362" time="317.309" type="appl" />
         <tli id="T363" time="318.8654190086694" />
         <tli id="T364" time="319.962" type="appl" />
         <tli id="T365" time="320.576" type="appl" />
         <tli id="T366" time="321.19" type="appl" />
         <tli id="T367" time="321.938" type="appl" />
         <tli id="T368" time="322.678" type="appl" />
         <tli id="T369" time="323.516" type="appl" />
         <tli id="T370" time="324.28" type="appl" />
         <tli id="T371" time="325.043" type="appl" />
         <tli id="T372" time="325.806" type="appl" />
         <tli id="T373" time="326.57" type="appl" />
         <tli id="T374" time="328.00538324577764" />
         <tli id="T375" time="328.734" type="appl" />
         <tli id="T376" time="329.388" type="appl" />
         <tli id="T377" time="330.042" type="appl" />
         <tli id="T378" time="330.696" type="appl" />
         <tli id="T379" time="331.35" type="appl" />
         <tli id="T380" time="332.004" type="appl" />
         <tli id="T381" time="332.658" type="appl" />
         <tli id="T382" time="334.201" type="appl" />
         <tli id="T383" time="335.693" type="appl" />
         <tli id="T384" time="337.184" type="appl" />
         <tli id="T385" time="338.199" type="appl" />
         <tli id="T386" time="338.965" type="appl" />
         <tli id="T387" time="340.7853332403337" />
         <tli id="T388" time="341.997" type="appl" />
         <tli id="T389" time="342.937" type="appl" />
         <tli id="T1120" time="343.88056408856806" type="intp" />
         <tli id="T390" time="345.1386495399921" />
         <tli id="T391" time="346.507" type="appl" />
         <tli id="T392" time="347.752" type="appl" />
         <tli id="T393" time="348.568" type="appl" />
         <tli id="T394" time="349.306" type="appl" />
         <tli id="T395" time="350.044" type="appl" />
         <tli id="T396" time="350.782" type="appl" />
         <tli id="T397" time="351.52" type="appl" />
         <tli id="T398" time="352.034" type="appl" />
         <tli id="T399" time="352.509" type="appl" />
         <tli id="T400" time="352.983" type="appl" />
         <tli id="T401" time="353.457" type="appl" />
         <tli id="T402" time="353.931" type="appl" />
         <tli id="T403" time="354.406" type="appl" />
         <tli id="T404" time="354.88" type="appl" />
         <tli id="T405" time="355.908" type="appl" />
         <tli id="T406" time="356.896" type="appl" />
         <tli id="T407" time="357.884" type="appl" />
         <tli id="T408" time="358.872" type="appl" />
         <tli id="T409" time="359.86" type="appl" />
         <tli id="T410" time="360.585" type="appl" />
         <tli id="T411" time="361.305" type="appl" />
         <tli id="T412" time="362.23" type="appl" />
         <tli id="T413" time="363.03" type="appl" />
         <tli id="T414" time="363.83" type="appl" />
         <tli id="T415" time="364.63" type="appl" />
         <tli id="T416" time="365.43" type="appl" />
         <tli id="T417" time="366.286" type="appl" />
         <tli id="T418" time="366.956" type="appl" />
         <tli id="T419" time="367.627" type="appl" />
         <tli id="T420" time="368.387" type="appl" />
         <tli id="T421" time="369.147" type="appl" />
         <tli id="T422" time="369.922" type="appl" />
         <tli id="T423" time="370.652" type="appl" />
         <tli id="T424" time="371.382" type="appl" />
         <tli id="T425" time="372.112" type="appl" />
         <tli id="T426" time="372.842" type="appl" />
         <tli id="T427" time="373.572" type="appl" />
         <tli id="T428" time="374.541" type="appl" />
         <tli id="T429" time="375.337" type="appl" />
         <tli id="T430" time="376.134" type="appl" />
         <tli id="T431" time="376.802" type="appl" />
         <tli id="T432" time="377.414" type="appl" />
         <tli id="T433" time="378.025" type="appl" />
         <tli id="T434" time="378.637" type="appl" />
         <tli id="T435" time="379.46518189365383" />
         <tli id="T436" time="380.229" type="appl" />
         <tli id="T437" time="380.958" type="appl" />
         <tli id="T438" time="381.686" type="appl" />
         <tli id="T439" time="382.414" type="appl" />
         <tli id="T440" time="383.143" type="appl" />
         <tli id="T441" time="384.15849686296394" />
         <tli id="T442" time="385.408" type="appl" />
         <tli id="T443" time="386.635" type="appl" />
         <tli id="T444" time="387.862" type="appl" />
         <tli id="T445" time="389.2584769077398" />
         <tli id="T446" time="389.976" type="appl" />
         <tli id="T447" time="390.711" type="appl" />
         <tli id="T448" time="391.447" type="appl" />
         <tli id="T449" time="392.183" type="appl" />
         <tli id="T450" time="392.919" type="appl" />
         <tli id="T451" time="393.654" type="appl" />
         <tli id="T452" time="394.39" type="appl" />
         <tli id="T453" time="395.017" type="appl" />
         <tli id="T454" time="395.624" type="appl" />
         <tli id="T455" time="396.70511443717083" />
         <tli id="T456" time="397.445" type="appl" />
         <tli id="T457" time="398.235" type="appl" />
         <tli id="T458" time="399.025" type="appl" />
         <tli id="T459" time="399.815" type="appl" />
         <tli id="T460" time="400.672" type="appl" />
         <tli id="T461" time="401.404" type="appl" />
         <tli id="T462" time="402.136" type="appl" />
         <tli id="T463" time="402.868" type="appl" />
         <tli id="T464" time="403.6" type="appl" />
         <tli id="T465" time="404.315" type="appl" />
         <tli id="T466" time="405.269" type="appl" />
         <tli id="T467" time="406.153" type="appl" />
         <tli id="T468" time="407.036" type="appl" />
         <tli id="T469" time="407.92" type="appl" />
         <tli id="T470" time="408.804" type="appl" />
         <tli id="T471" time="410.6050600494032" />
         <tli id="T472" time="411.436" type="appl" />
         <tli id="T473" time="412.331" type="appl" />
         <tli id="T474" time="413.227" type="appl" />
         <tli id="T475" time="414.122" type="appl" />
         <tli id="T476" time="415.018" type="appl" />
         <tli id="T477" time="415.789" type="appl" />
         <tli id="T478" time="416.399" type="appl" />
         <tli id="T479" time="417.009" type="appl" />
         <tli id="T480" time="417.619" type="appl" />
         <tli id="T481" time="418.262" type="appl" />
         <tli id="T482" time="418.843" type="appl" />
         <tli id="T483" time="419.425" type="appl" />
         <tli id="T484" time="420.006" type="appl" />
         <tli id="T485" time="420.588" type="appl" />
         <tli id="T486" time="421.302" type="appl" />
         <tli id="T487" time="421.885" type="appl" />
         <tli id="T488" time="422.467" type="appl" />
         <tli id="T489" time="423.049" type="appl" />
         <tli id="T490" time="423.632" type="appl" />
         <tli id="T491" time="424.214" type="appl" />
         <tli id="T492" time="424.796" type="appl" />
         <tli id="T493" time="425.379" type="appl" />
         <tli id="T494" time="425.961" type="appl" />
         <tli id="T495" time="426.872" type="appl" />
         <tli id="T496" time="427.69" type="appl" />
         <tli id="T497" time="428.508" type="appl" />
         <tli id="T498" time="429.325" type="appl" />
         <tli id="T499" time="430.308" type="appl" />
         <tli id="T500" time="431.12" type="appl" />
         <tli id="T501" time="431.932" type="appl" />
         <tli id="T502" time="432.745" type="appl" />
         <tli id="T503" time="434.125" type="appl" />
         <tli id="T504" time="435.195" type="appl" />
         <tli id="T505" time="436.265" type="appl" />
         <tli id="T506" time="437.168" type="appl" />
         <tli id="T507" time="437.78" type="appl" />
         <tli id="T508" time="438.392" type="appl" />
         <tli id="T509" time="439.005" type="appl" />
         <tli id="T510" time="439.791" type="appl" />
         <tli id="T511" time="440.517" type="appl" />
         <tli id="T512" time="441.243" type="appl" />
         <tli id="T513" time="441.969" type="appl" />
         <tli id="T514" time="443.09826624317816" />
         <tli id="T515" time="443.578" type="appl" />
         <tli id="T516" time="444.13" type="appl" />
         <tli id="T517" time="444.681" type="appl" />
         <tli id="T518" time="445.233" type="appl" />
         <tli id="T519" time="445.784" type="appl" />
         <tli id="T520" time="446.336" type="appl" />
         <tli id="T521" time="446.887" type="appl" />
         <tli id="T522" time="447.52" type="appl" />
         <tli id="T523" time="448.132" type="appl" />
         <tli id="T524" time="448.744" type="appl" />
         <tli id="T525" time="449.357" type="appl" />
         <tli id="T526" time="450.852" type="appl" />
         <tli id="T527" time="452.247" type="appl" />
         <tli id="T528" time="453.2" type="appl" />
         <tli id="T529" time="454.144" type="appl" />
         <tli id="T530" time="455.087" type="appl" />
         <tli id="T531" time="456.03" type="appl" />
         <tli id="T532" time="456.973" type="appl" />
         <tli id="T533" time="457.917" type="appl" />
         <tli id="T534" time="458.86" type="appl" />
         <tli id="T535" time="459.37" type="appl" />
         <tli id="T536" time="459.83" type="appl" />
         <tli id="T537" time="460.29" type="appl" />
         <tli id="T538" time="460.75" type="appl" />
         <tli id="T539" time="461.239" type="appl" />
         <tli id="T540" time="461.728" type="appl" />
         <tli id="T541" time="462.217" type="appl" />
         <tli id="T542" time="462.706" type="appl" />
         <tli id="T543" time="463.195" type="appl" />
         <tli id="T544" time="463.685" type="appl" />
         <tli id="T545" time="464.174" type="appl" />
         <tli id="T546" time="464.663" type="appl" />
         <tli id="T547" time="465.152" type="appl" />
         <tli id="T548" time="465.641" type="appl" />
         <tli id="T549" time="466.13" type="appl" />
         <tli id="T550" time="467.093" type="appl" />
         <tli id="T551" time="467.903" type="appl" />
         <tli id="T552" time="469.071" type="appl" />
         <tli id="T553" time="470.092" type="appl" />
         <tli id="T554" time="471.114" type="appl" />
         <tli id="T555" time="472.135" type="appl" />
         <tli id="T556" time="473.001" type="appl" />
         <tli id="T557" time="473.762" type="appl" />
         <tli id="T558" time="474.523" type="appl" />
         <tli id="T559" time="475.284" type="appl" />
         <tli id="T560" time="476.01834576668125" />
         <tli id="T561" time="477.275" type="appl" />
         <tli id="T562" time="478.505" type="appl" />
         <tli id="T563" time="480.37145373382145" />
         <tli id="T564" time="481.08" type="appl" />
         <tli id="T565" time="481.64" type="appl" />
         <tli id="T566" time="482.2" type="appl" />
         <tli id="T567" time="482.9247770765001" />
         <tli id="T568" time="483.408" type="appl" />
         <tli id="T569" time="483.866" type="appl" />
         <tli id="T570" time="484.323" type="appl" />
         <tli id="T571" time="485.06476870313156" />
         <tli id="T572" time="486.132" type="appl" />
         <tli id="T573" time="487.004" type="appl" />
         <tli id="T574" time="487.876" type="appl" />
         <tli id="T575" time="488.748" type="appl" />
         <tli id="T576" time="489.62" type="appl" />
         <tli id="T577" time="490.395" type="appl" />
         <tli id="T578" time="491.145" type="appl" />
         <tli id="T579" time="491.895" type="appl" />
         <tli id="T580" time="492.745" type="appl" />
         <tli id="T581" time="493.575" type="appl" />
         <tli id="T582" time="495.38472832314875" />
         <tli id="T583" time="496.195" type="appl" />
         <tli id="T584" time="496.91" type="appl" />
         <tli id="T585" time="497.512" type="appl" />
         <tli id="T586" time="498.054" type="appl" />
         <tli id="T587" time="498.596" type="appl" />
         <tli id="T588" time="499.138" type="appl" />
         <tli id="T589" time="500.2513759475754" />
         <tli id="T590" time="500.832" type="appl" />
         <tli id="T591" time="501.392" type="appl" />
         <tli id="T592" time="501.952" type="appl" />
         <tli id="T593" time="502.512" type="appl" />
         <tli id="T594" time="503.072" type="appl" />
         <tli id="T595" time="503.632" type="appl" />
         <tli id="T596" time="504.192" type="appl" />
         <tli id="T597" time="504.775" type="appl" />
         <tli id="T598" time="505.33" type="appl" />
         <tli id="T599" time="505.885" type="appl" />
         <tli id="T600" time="506.44" type="appl" />
         <tli id="T601" time="506.995" type="appl" />
         <tli id="T602" time="507.585" type="appl" />
         <tli id="T603" time="508.095" type="appl" />
         <tli id="T604" time="509.0113416715435" />
         <tli id="T605" time="509.768" type="appl" />
         <tli id="T606" time="510.407" type="appl" />
         <tli id="T607" time="511.045" type="appl" />
         <tli id="T608" time="511.683" type="appl" />
         <tli id="T609" time="512.322" type="appl" />
         <tli id="T610" time="512.96" type="appl" />
         <tli id="T611" time="513.732" type="appl" />
         <tli id="T612" time="514.415" type="appl" />
         <tli id="T613" time="515.098" type="appl" />
         <tli id="T614" time="515.78" type="appl" />
         <tli id="T615" time="516.462" type="appl" />
         <tli id="T616" time="517.145" type="appl" />
         <tli id="T617" time="517.828" type="appl" />
         <tli id="T618" time="518.51" type="appl" />
         <tli id="T619" time="519.192" type="appl" />
         <tli id="T620" time="519.875" type="appl" />
         <tli id="T621" time="521.317" type="appl" />
         <tli id="T622" time="522.519" type="appl" />
         <tli id="T623" time="523.541" type="appl" />
         <tli id="T624" time="524.564" type="appl" />
         <tli id="T625" time="525.586" type="appl" />
         <tli id="T626" time="526.608" type="appl" />
         <tli id="T627" time="527.63" type="appl" />
         <tli id="T628" time="529.492" type="appl" />
         <tli id="T629" time="531.006" type="appl" />
         <tli id="T630" time="533.2512468255374" />
         <tli id="T631" time="534.064" type="appl" />
         <tli id="T632" time="534.708" type="appl" />
         <tli id="T633" time="535.352" type="appl" />
         <tli id="T634" time="535.996" type="appl" />
         <tli id="T635" time="536.8645660206595" />
         <tli id="T636" time="537.705" type="appl" />
         <tli id="T637" time="538.525" type="appl" />
         <tli id="T638" time="539.335" type="appl" />
         <tli id="T639" time="540.3045525606652" />
         <tli id="T640" time="541.229" type="appl" />
         <tli id="T641" time="542.047" type="appl" />
         <tli id="T642" time="542.866" type="appl" />
         <tli id="T643" time="543.685" type="appl" />
         <tli id="T644" time="544.503" type="appl" />
         <tli id="T645" time="545.322" type="appl" />
         <tli id="T646" time="546.631" type="appl" />
         <tli id="T647" time="547.753" type="appl" />
         <tli id="T648" time="548.874" type="appl" />
         <tli id="T649" time="549.995" type="appl" />
         <tli id="T650" time="551.117" type="appl" />
         <tli id="T651" time="552.238" type="appl" />
         <tli id="T652" time="553.642" type="appl" />
         <tli id="T653" time="554.835" type="appl" />
         <tli id="T654" time="556.028" type="appl" />
         <tli id="T655" time="557.3578191679028" />
         <tli id="T656" time="558.33" type="appl" />
         <tli id="T657" time="559.12" type="appl" />
         <tli id="T658" time="559.91" type="appl" />
         <tli id="T659" time="560.7" type="appl" />
         <tli id="T660" time="561.49" type="appl" />
         <tli id="T661" time="563.2244628795405" />
         <tli id="T662" time="563.999" type="appl" />
         <tli id="T663" time="564.618" type="appl" />
         <tli id="T664" time="565.237" type="appl" />
         <tli id="T665" time="565.856" type="appl" />
         <tli id="T666" time="566.474" type="appl" />
         <tli id="T667" time="567.093" type="appl" />
         <tli id="T668" time="567.712" type="appl" />
         <tli id="T669" time="568.331" type="appl" />
         <tli id="T670" time="568.95" type="appl" />
         <tli id="T671" time="569.675" type="appl" />
         <tli id="T672" time="570.31" type="appl" />
         <tli id="T673" time="570.945" type="appl" />
         <tli id="T674" time="573.2644235951386" />
         <tli id="T675" time="574.403" type="appl" />
         <tli id="T676" time="575.307" type="appl" />
         <tli id="T677" time="576.1366519400043" />
         <tli id="T678" time="577.038" type="appl" />
         <tli id="T679" time="577.776" type="appl" />
         <tli id="T680" time="578.514" type="appl" />
         <tli id="T681" time="579.252" type="appl" />
         <tli id="T682" time="580.7243944057324" />
         <tli id="T683" time="581.853" type="appl" />
         <tli id="T684" time="582.926" type="appl" />
         <tli id="T685" time="583.999" type="appl" />
         <tli id="T686" time="585.072" type="appl" />
         <tli id="T687" time="586.145" type="appl" />
         <tli id="T688" time="587.218" type="appl" />
         <tli id="T689" time="588.291" type="appl" />
         <tli id="T690" time="589.297" type="appl" />
         <tli id="T691" time="590.094" type="appl" />
         <tli id="T692" time="590.891" type="appl" />
         <tli id="T693" time="591.687" type="appl" />
         <tli id="T694" time="592.484" type="appl" />
         <tli id="T695" time="593.281" type="appl" />
         <tli id="T696" time="594.0843421308709" />
         <tli id="T697" time="596.2110004763394" />
         <tli id="T698" time="596.939" type="appl" />
         <tli id="T699" time="597.764" type="appl" />
         <tli id="T700" time="598.589" type="appl" />
         <tli id="T701" time="599.414" type="appl" />
         <tli id="T702" time="600.309" type="appl" />
         <tli id="T703" time="601.149" type="appl" />
         <tli id="T704" time="601.989" type="appl" />
         <tli id="T705" time="604.2509690175157" />
         <tli id="T706" time="605.39" type="appl" />
         <tli id="T707" time="606.37" type="appl" />
         <tli id="T708" time="607.35" type="appl" />
         <tli id="T709" time="608.33" type="appl" />
         <tli id="T710" time="609.31" type="appl" />
         <tli id="T711" time="610.29" type="appl" />
         <tli id="T712" time="611.232" type="appl" />
         <tli id="T713" time="611.99" type="appl" />
         <tli id="T714" time="612.748" type="appl" />
         <tli id="T715" time="613.506" type="appl" />
         <tli id="T716" time="614.264" type="appl" />
         <tli id="T717" time="615.022" type="appl" />
         <tli id="T718" time="615.78" type="appl" />
         <tli id="T719" time="616.538" type="appl" />
         <tli id="T720" time="617.296" type="appl" />
         <tli id="T721" time="618.054" type="appl" />
         <tli id="T722" time="619.17" type="appl" />
         <tli id="T723" time="620.16" type="appl" />
         <tli id="T724" time="621.149" type="appl" />
         <tli id="T725" time="622.139" type="appl" />
         <tli id="T726" time="623.438" type="appl" />
         <tli id="T727" time="624.686" type="appl" />
         <tli id="T728" time="625.933" type="appl" />
         <tli id="T729" time="627.181" type="appl" />
         <tli id="T730" time="628.014" type="appl" />
         <tli id="T731" time="628.758" type="appl" />
         <tli id="T732" time="629.502" type="appl" />
         <tli id="T733" time="630.246" type="appl" />
         <tli id="T734" time="630.99" type="appl" />
         <tli id="T735" time="631.734" type="appl" />
         <tli id="T736" time="632.478" type="appl" />
         <tli id="T737" time="633.222" type="appl" />
         <tli id="T738" time="633.966" type="appl" />
         <tli id="T739" time="634.71" type="appl" />
         <tli id="T740" time="635.7908456081494" />
         <tli id="T741" time="636.744" type="appl" />
         <tli id="T742" time="637.3946414161436" />
         <tli id="T743" time="638.061" type="appl" />
         <tli id="T744" time="638.754" type="appl" />
         <tli id="T745" time="639.447" type="appl" />
         <tli id="T746" time="640.139" type="appl" />
         <tli id="T747" time="640.832" type="appl" />
         <tli id="T748" time="641.525" type="appl" />
         <tli id="T749" time="642.2380078816649" />
         <tli id="T750" time="643.302" type="appl" />
         <tli id="T751" time="644.356" type="appl" />
         <tli id="T752" time="645.41" type="appl" />
         <tli id="T753" time="646.464" type="appl" />
         <tli id="T754" time="647.518" type="appl" />
         <tli id="T755" time="648.572" type="appl" />
         <tli id="T756" time="649.626" type="appl" />
         <tli id="T757" time="650.6666728186149" />
         <tli id="T758" time="652.14" type="appl" />
         <tli id="T759" time="653.6" type="appl" />
         <tli id="T760" time="655.06" type="appl" />
         <tli id="T761" time="656.519" type="appl" />
         <tli id="T762" time="657.979" type="appl" />
         <tli id="T763" time="660.010750840399" />
         <tli id="T764" time="661.34" type="appl" />
         <tli id="T765" time="662.91" type="appl" />
         <tli id="T766" time="664.48" type="appl" />
         <tli id="T767" time="666.05" type="appl" />
         <tli id="T768" time="666.957" type="appl" />
         <tli id="T769" time="667.851" type="appl" />
         <tli id="T770" time="668.744" type="appl" />
         <tli id="T771" time="669.389" type="appl" />
         <tli id="T772" time="670.034" type="appl" />
         <tli id="T773" time="670.679" type="appl" />
         <tli id="T774" time="671.324" type="appl" />
         <tli id="T775" time="671.969" type="appl" />
         <tli id="T776" time="672.615" type="appl" />
         <tli id="T777" time="673.26" type="appl" />
         <tli id="T778" time="673.905" type="appl" />
         <tli id="T779" time="674.55" type="appl" />
         <tli id="T780" time="675.195" type="appl" />
         <tli id="T781" time="675.84" type="appl" />
         <tli id="T782" time="677.305" type="appl" />
         <tli id="T783" time="678.26" type="appl" />
         <tli id="T784" time="679.216" type="appl" />
         <tli id="T785" time="680.171" type="appl" />
         <tli id="T786" time="681.126" type="appl" />
         <tli id="T787" time="682.081" type="appl" />
         <tli id="T788" time="683.036" type="appl" />
         <tli id="T789" time="683.992" type="appl" />
         <tli id="T790" time="684.947" type="appl" />
         <tli id="T791" time="685.3906515338132" />
         <tli id="T792" time="695.161" type="appl" />
         <tli id="T793" time="696.176" type="appl" />
         <tli id="T794" time="696.981" type="appl" />
         <tli id="T795" time="697.786" type="appl" />
         <tli id="T796" time="698.591" type="appl" />
         <tli id="T797" time="699.396" type="appl" />
         <tli id="T798" time="700.201" type="appl" />
         <tli id="T799" time="701.762" type="appl" />
         <tli id="T800" time="703.105" type="appl" />
         <tli id="T801" time="704.447" type="appl" />
         <tli id="T802" time="705.789" type="appl" />
         <tli id="T803" time="707.131" type="appl" />
         <tli id="T804" time="708.474" type="appl" />
         <tli id="T805" time="710.270554183622" />
         <tli id="T806" time="711.28" type="appl" />
         <tli id="T807" time="712.39" type="appl" />
         <tli id="T808" time="713.5" type="appl" />
         <tli id="T809" time="714.61" type="appl" />
         <tli id="T810" time="716.0171983647945" />
         <tli id="T811" time="716.726" type="appl" />
         <tli id="T812" time="717.418" type="appl" />
         <tli id="T813" time="718.11" type="appl" />
         <tli id="T814" time="718.9905200641017" />
         <tli id="T815" time="719.812" type="appl" />
         <tli id="T816" time="720.52" type="appl" />
         <tli id="T817" time="721.083" type="appl" />
         <tli id="T818" time="721.639" type="appl" />
         <tli id="T819" time="722.7571719925963" />
         <tli id="T820" time="723.239" type="appl" />
         <tli id="T821" time="723.797" type="appl" />
         <tli id="T822" time="724.356" type="appl" />
         <tli id="T823" time="724.915" type="appl" />
         <tli id="T824" time="725.474" type="appl" />
         <tli id="T825" time="726.032" type="appl" />
         <tli id="T826" time="726.591" type="appl" />
         <tli id="T827" time="727.15" type="appl" />
         <tli id="T828" time="728.0" type="appl" />
         <tli id="T829" time="728.841" type="appl" />
         <tli id="T830" time="729.681" type="appl" />
         <tli id="T831" time="730.522" type="appl" />
         <tli id="T832" time="731.362" type="appl" />
         <tli id="T833" time="732.203" type="appl" />
         <tli id="T834" time="733.043" type="appl" />
         <tli id="T835" time="734.383" type="appl" />
         <tli id="T836" time="735.576" type="appl" />
         <tli id="T837" time="736.768" type="appl" />
         <tli id="T838" time="737.961" type="appl" />
         <tli id="T839" time="739.154" type="appl" />
         <tli id="T840" time="740.583768907277" />
         <tli id="T841" time="741.893" type="appl" />
         <tli id="T842" time="742.996" type="appl" />
         <tli id="T843" time="744.1" type="appl" />
         <tli id="T844" time="745.203" type="appl" />
         <tli id="T845" time="746.261" type="appl" />
         <tli id="T846" time="747.182" type="appl" />
         <tli id="T847" time="748.104" type="appl" />
         <tli id="T848" time="749.025" type="appl" />
         <tli id="T849" time="750.9703949331083" />
         <tli id="T850" time="752.193" type="appl" />
         <tli id="T851" time="753.458" type="appl" />
         <tli id="T852" time="754.376" type="appl" />
         <tli id="T853" time="755.111" type="appl" />
         <tli id="T854" time="755.847" type="appl" />
         <tli id="T855" time="756.583" type="appl" />
         <tli id="T856" time="757.319" type="appl" />
         <tli id="T857" time="758.054" type="appl" />
         <tli id="T858" time="758.79" type="appl" />
         <tli id="T859" time="759.625" type="appl" />
         <tli id="T860" time="760.2" type="appl" />
         <tli id="T861" time="760.775" type="appl" />
         <tli id="T862" time="761.35" type="appl" />
         <tli id="T863" time="761.925" type="appl" />
         <tli id="T864" time="762.5" type="appl" />
         <tli id="T865" time="763.075" type="appl" />
         <tli id="T866" time="763.65" type="appl" />
         <tli id="T867" time="764.328" type="appl" />
         <tli id="T868" time="764.925" type="appl" />
         <tli id="T869" time="765.522" type="appl" />
         <tli id="T870" time="766.4103345196456" />
         <tli id="T871" time="767.431" type="appl" />
         <tli id="T872" time="768.062" type="appl" />
         <tli id="T873" time="768.693" type="appl" />
         <tli id="T874" time="769.324" type="appl" />
         <tli id="T875" time="769.86" type="appl" />
         <tli id="T876" time="770.384" type="appl" />
         <tli id="T877" time="771.0903162077929" />
         <tli id="T878" time="772.231" type="appl" />
         <tli id="T879" time="773.301" type="appl" />
         <tli id="T880" time="774.4836362637288" />
         <tli id="T881" time="775.806" type="appl" />
         <tli id="T882" time="777.126" type="appl" />
         <tli id="T883" time="778.445" type="appl" />
         <tli id="T884" time="778.844" type="appl" />
         <tli id="T885" time="779.086" type="appl" />
         <tli id="T886" time="779.329" type="appl" />
         <tli id="T887" time="785.9769246260735" />
         <tli id="T888" time="787.191" type="appl" />
         <tli id="T889" time="788.2769156266587" />
         <tli id="T890" time="789.415" type="appl" />
         <tli id="T891" time="790.329" type="appl" />
         <tli id="T892" time="791.244" type="appl" />
         <tli id="T893" time="792.159" type="appl" />
         <tli id="T894" time="793.074" type="appl" />
         <tli id="T895" time="793.988" type="appl" />
         <tli id="T896" time="794.903" type="appl" />
         <tli id="T897" time="795.818" type="appl" />
         <tli id="T898" time="796.732" type="appl" />
         <tli id="T899" time="798.1368770465588" />
         <tli id="T900" time="799.017" type="appl" />
         <tli id="T901" time="799.654" type="appl" />
         <tli id="T902" time="800.29" type="appl" />
         <tli id="T903" time="800.927" type="appl" />
         <tli id="T904" time="801.564" type="appl" />
         <tli id="T905" time="802.177" type="appl" />
         <tli id="T906" time="802.667" type="appl" />
         <tli id="T907" time="803.157" type="appl" />
         <tli id="T908" time="803.785" type="appl" />
         <tli id="T909" time="804.413" type="appl" />
         <tli id="T910" time="805.041" type="appl" />
         <tli id="T911" time="805.669" type="appl" />
         <tli id="T912" time="806.297" type="appl" />
         <tli id="T913" time="807.637" type="appl" />
         <tli id="T914" time="808.677" type="appl" />
         <tli id="T915" time="809.718" type="appl" />
         <tli id="T916" time="810.758" type="appl" />
         <tli id="T917" time="811.798" type="appl" />
         <tli id="T918" time="812.838" type="appl" />
         <tli id="T919" time="813.878" type="appl" />
         <tli id="T920" time="814.918" type="appl" />
         <tli id="T1121" time="815.3984615384616" type="intp" />
         <tli id="T921" time="815.959" type="appl" />
         <tli id="T922" time="816.999" type="appl" />
         <tli id="T923" time="818.0656532358233" />
         <tli id="T924" time="819.963" type="appl" />
         <tli id="T925" time="828.3967586455626" />
         <tli id="T926" time="829.262" type="appl" />
         <tli id="T927" time="830.024" type="appl" />
         <tli id="T928" time="830.787" type="appl" />
         <tli id="T929" time="831.549" type="appl" />
         <tli id="T930" time="832.311" type="appl" />
         <tli id="T931" time="833.074" type="appl" />
         <tli id="T932" time="833.836" type="appl" />
         <tli id="T933" time="834.598" type="appl" />
         <tli id="T934" time="836.778" type="appl" />
         <tli id="T935" time="837.495" type="appl" />
         <tli id="T936" time="838.213" type="appl" />
         <tli id="T937" time="838.93" type="appl" />
         <tli id="T938" time="839.648" type="appl" />
         <tli id="T939" time="840.366" type="appl" />
         <tli id="T940" time="841.083" type="appl" />
         <tli id="T941" time="842.0700384780271" />
         <tli id="T942" time="842.962" type="appl" />
         <tli id="T943" time="843.863" type="appl" />
         <tli id="T944" time="844.765" type="appl" />
         <tli id="T945" time="845.667" type="appl" />
         <tli id="T946" time="846.568" type="appl" />
         <tli id="T947" time="848.2633475781537" />
         <tli id="T948" time="848.684" type="appl" />
         <tli id="T949" time="849.223" type="appl" />
         <tli id="T950" time="849.762" type="appl" />
         <tli id="T951" time="850.302" type="appl" />
         <tli id="T952" time="850.841" type="appl" />
         <tli id="T953" time="851.9166666167644" />
         <tli id="T954" time="852.867" type="appl" />
         <tli id="T955" time="853.627" type="appl" />
         <tli id="T956" time="854.387" type="appl" />
         <tli id="T957" time="855.4899859683983" />
         <tli id="T958" time="856.2" type="appl" />
         <tli id="T959" time="856.959" type="appl" />
         <tli id="T960" time="857.717" type="appl" />
         <tli id="T961" time="858.475" type="appl" />
         <tli id="T962" time="859.234" type="appl" />
         <tli id="T963" time="860.3033004681737" />
         <tli id="T964" time="860.852" type="appl" />
         <tli id="T965" time="861.382" type="appl" />
         <tli id="T966" time="862.7899574050383" />
         <tli id="T967" time="863.712" type="appl" />
         <tli id="T968" time="864.592" type="appl" />
         <tli id="T969" time="865.472" type="appl" />
         <tli id="T970" time="865.979" type="appl" />
         <tli id="T971" time="866.485" type="appl" />
         <tli id="T972" time="866.992" type="appl" />
         <tli id="T973" time="867.499" type="appl" />
         <tli id="T974" time="868.005" type="appl" />
         <tli id="T975" time="868.6966009601645" />
         <tli id="T976" time="869.532" type="appl" />
         <tli id="T977" time="870.337" type="appl" />
         <tli id="T978" time="871.142" type="appl" />
         <tli id="T979" time="871.947" type="appl" />
         <tli id="T980" time="872.752" type="appl" />
         <tli id="T981" time="873.364" type="appl" />
         <tli id="T982" time="873.976" type="appl" />
         <tli id="T983" time="874.589" type="appl" />
         <tli id="T984" time="875.201" type="appl" />
         <tli id="T985" time="875.813" type="appl" />
         <tli id="T986" time="876.425" type="appl" />
         <tli id="T987" time="877.038" type="appl" />
         <tli id="T988" time="878.9632274555303" />
         <tli id="T989" time="879.7" type="appl" />
         <tli id="T990" time="880.32" type="appl" />
         <tli id="T991" time="880.94" type="appl" />
         <tli id="T992" time="883.3632102392585" />
         <tli id="T993" time="884.241" type="appl" />
         <tli id="T994" time="885.033" type="appl" />
         <tli id="T995" time="885.824" type="appl" />
         <tli id="T996" time="886.616" type="appl" />
         <tli id="T997" time="887.407" type="appl" />
         <tli id="T998" time="888.073" type="appl" />
         <tli id="T999" time="888.648" type="appl" />
         <tli id="T1000" time="889.222" type="appl" />
         <tli id="T1001" time="889.796" type="appl" />
         <tli id="T1002" time="890.37" type="appl" />
         <tli id="T1003" time="890.945" type="appl" />
         <tli id="T1004" time="892.4965078357853" />
         <tli id="T1005" time="893.112" type="appl" />
         <tli id="T1006" time="893.794" type="appl" />
         <tli id="T1007" time="894.476" type="appl" />
         <tli id="T1008" time="895.158" type="appl" />
         <tli id="T1009" time="895.841" type="appl" />
         <tli id="T1010" time="896.523" type="appl" />
         <tli id="T1011" time="897.205" type="appl" />
         <tli id="T1012" time="897.887" type="appl" />
         <tli id="T1013" time="898.556" type="appl" />
         <tli id="T1014" time="899.209" type="appl" />
         <tli id="T1015" time="899.862" type="appl" />
         <tli id="T1016" time="900.514" type="appl" />
         <tli id="T1017" time="901.994" type="appl" />
         <tli id="T1018" time="903.374" type="appl" />
         <tli id="T1019" time="904.754" type="appl" />
         <tli id="T1020" time="905.413" type="appl" />
         <tli id="T1021" time="906.017" type="appl" />
         <tli id="T1022" time="906.62" type="appl" />
         <tli id="T1023" time="907.223" type="appl" />
         <tli id="T1024" time="907.827" type="appl" />
         <tli id="T1025" time="909.2697755386039" />
         <tli id="T1026" time="910.858" type="appl" />
         <tli id="T1027" time="912.3697634089579" />
         <tli id="T1028" time="912.969" type="appl" />
         <tli id="T1029" time="913.508" type="appl" />
         <tli id="T1030" time="914.047" type="appl" />
         <tli id="T1031" time="914.586" type="appl" />
         <tli id="T1032" time="915.125" type="appl" />
         <tli id="T1033" time="915.664" type="appl" />
         <tli id="T1034" time="916.203" type="appl" />
         <tli id="T1035" time="916.742" type="appl" />
         <tli id="T1036" time="917.281" type="appl" />
         <tli id="T1037" time="917.82" type="appl" />
         <tli id="T1038" time="919.52" type="appl" />
         <tli id="T1039" time="920.26" type="appl" />
         <tli id="T1040" time="920.81" type="appl" />
         <tli id="T1041" time="921.6563937388571" />
         <tli id="T1042" time="922.5" type="appl" />
         <tli id="T1043" time="923.34" type="appl" />
         <tli id="T1044" time="924.18" type="appl" />
         <tli id="T1045" time="925.02" type="appl" />
         <tli id="T1046" time="925.86" type="appl" />
         <tli id="T1047" time="927.135" type="appl" />
         <tli id="T1048" time="928.03" type="appl" />
         <tli id="T1049" time="928.925" type="appl" />
         <tli id="T1050" time="929.82" type="appl" />
         <tli id="T1051" time="930.715" type="appl" />
         <tli id="T1052" time="931.61" type="appl" />
         <tli id="T1053" time="932.218" type="appl" />
         <tli id="T1054" time="932.776" type="appl" />
         <tli id="T1055" time="933.334" type="appl" />
         <tli id="T1056" time="933.892" type="appl" />
         <tli id="T1057" time="934.45" type="appl" />
         <tli id="T1058" time="935.02" type="appl" />
         <tli id="T1059" time="935.59" type="appl" />
         <tli id="T1060" time="936.16" type="appl" />
         <tli id="T1061" time="936.73" type="appl" />
         <tli id="T1062" time="937.3" type="appl" />
         <tli id="T1063" time="937.87" type="appl" />
         <tli id="T1064" time="938.44" type="appl" />
         <tli id="T1065" time="938.932" type="appl" />
         <tli id="T1066" time="939.413" type="appl" />
         <tli id="T1067" time="939.895" type="appl" />
         <tli id="T1068" time="940.377" type="appl" />
         <tli id="T1069" time="940.858" type="appl" />
         <tli id="T1070" time="941.34" type="appl" />
         <tli id="T1071" time="942.456" type="appl" />
         <tli id="T1072" time="943.232" type="appl" />
         <tli id="T1073" time="944.008" type="appl" />
         <tli id="T1074" time="944.784" type="appl" />
         <tli id="T1075" time="945.56" type="appl" />
         <tli id="T1076" time="946.785" type="appl" />
         <tli id="T1077" time="947.54" type="appl" />
         <tli id="T1078" time="948.15" type="appl" />
         <tli id="T1079" time="948.76" type="appl" />
         <tli id="T1080" time="949.37" type="appl" />
         <tli id="T1081" time="949.98" type="appl" />
         <tli id="T1082" time="950.59" type="appl" />
         <tli id="T1083" time="951.2" type="appl" />
         <tli id="T1084" time="951.81" type="appl" />
         <tli id="T1085" time="952.537" type="appl" />
         <tli id="T1086" time="953.103" type="appl" />
         <tli id="T1087" time="953.67" type="appl" />
         <tli id="T1088" time="954.237" type="appl" />
         <tli id="T1089" time="954.803" type="appl" />
         <tli id="T1090" time="955.37" type="appl" />
         <tli id="T1091" time="956.133" type="appl" />
         <tli id="T1092" time="956.796" type="appl" />
         <tli id="T1093" time="957.459" type="appl" />
         <tli id="T1094" time="958.121" type="appl" />
         <tli id="T1095" time="958.784" type="appl" />
         <tli id="T1096" time="959.447" type="appl" />
         <tli id="T1097" time="960.11" type="appl" />
         <tli id="T1098" time="960.9762398879922" />
         <tli id="T1099" time="961.45" type="appl" />
         <tli id="T1100" time="961.97" type="appl" />
         <tli id="T1101" time="962.49" type="appl" />
         <tli id="T1102" time="963.01" type="appl" />
         <tli id="T1103" time="963.53" type="appl" />
         <tli id="T1104" time="964.4428929903235" />
         <tli id="T1105" time="965.755" type="appl" />
         <tli id="T1106" time="966.935" type="appl" />
         <tli id="T1122" time="967.3514705882353" type="intp" />
         <tli id="T1107" time="968.115" type="appl" />
         <tli id="T1123" time="968.8326842105263" type="intp" />
         <tli id="T1108" time="970.063" type="appl" />
         <tli id="T1109" time="971.927" type="appl" />
         <tli id="T1110" time="973.79" type="appl" />
         <tli id="T1111" time="974.762" type="appl" />
         <tli id="T1112" time="975.635" type="appl" />
         <tli id="T1113" time="976.653" type="appl" />
         <tli id="T1114" time="977.672" type="appl" />
         <tli id="T1115" time="979.1761686752318" />
         <tli id="T1116" time="979.689" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T260" start="T259">
            <tli id="T259.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T320" start="T319">
            <tli id="T319.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T658" start="T657">
            <tli id="T657.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T792" start="T791">
            <tli id="T791.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T1115" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nʼergölaʔbə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bar</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kirgarlaʔbə</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">amnoləj</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">dăk</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_19" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">dʼü=</ts>
                  <nts id="Seg_22" n="HIAT:ip">)</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">dʼü</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">tegolaʔbə</ts>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_34" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">Dĭ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">multuk</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_43" n="HIAT:u" s="T10">
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <nts id="Seg_45" n="HIAT:ip">(</nts>
                  <ats e="T11" id="Seg_46" n="HIAT:non-pho" s="T10">BRK</ats>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip">)</nts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_54" n="HIAT:w" s="T11">Girgit</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_57" n="HIAT:w" s="T12">tĭrgit</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_60" n="HIAT:w" s="T13">загадка</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_63" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_65" n="HIAT:w" s="T14">kədet-</ts>
                  <nts id="Seg_66" n="HIAT:ip">)</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_69" n="HIAT:w" s="T15">kötengəndə</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_72" n="HIAT:w" s="T16">ягодка</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_76" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_78" n="HIAT:w" s="T17">Dĭ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_81" n="HIAT:w" s="T18">multuk</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_85" n="HIAT:u" s="T19">
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <nts id="Seg_87" n="HIAT:ip">(</nts>
                  <ats e="T20" id="Seg_88" n="HIAT:non-pho" s="T19">BRK</ats>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_94" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_96" n="HIAT:w" s="T20">Kamen</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_99" n="HIAT:w" s="T21">miʔ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_102" n="HIAT:w" s="T22">iʔgö</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_105" n="HIAT:w" s="T23">ibibeʔ</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_109" n="HIAT:w" s="T24">nuzaŋ</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_113" n="HIAT:w" s="T25">măn</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_116" n="HIAT:w" s="T26">mĭmbiem</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_119" n="HIAT:w" s="T27">urgo</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_122" n="HIAT:w" s="T28">măjagən</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_126" n="HIAT:w" s="T29">Belăgorjagən</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_130" n="HIAT:w" s="T30">dĭn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_133" n="HIAT:w" s="T31">kola</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_136" n="HIAT:w" s="T32">dʼaʔpiam</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_140" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_142" n="HIAT:w" s="T33">I</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_145" n="HIAT:w" s="T34">uja</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_148" n="HIAT:w" s="T35">kuʔpiam</ts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_152" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_154" n="HIAT:w" s="T36">Dĭgəttə</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_157" n="HIAT:w" s="T37">bar</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_160" n="HIAT:w" s="T38">măjaʔi</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T40" id="Seg_164" n="HIAT:w" s="T39">sagərʔi</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">maluʔpiʔi</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_172" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_174" n="HIAT:w" s="T41">Măn</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_177" n="HIAT:w" s="T42">šalbə</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_180" n="HIAT:w" s="T43">už</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_183" n="HIAT:w" s="T44">naga</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_187" n="HIAT:w" s="T45">bar</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_190" n="HIAT:w" s="T46">külambi</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_194" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_196" n="HIAT:w" s="T47">A</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_199" n="HIAT:w" s="T48">dĭn</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">gijen</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_206" n="HIAT:w" s="T50">maʔi</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_209" n="HIAT:w" s="T51">nugabiʔi</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_212" n="HIAT:w" s="T52">bar</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_216" n="HIAT:w" s="T53">saʔməluʔpiʔi</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_219" n="HIAT:w" s="T54">i</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_222" n="HIAT:w" s="T55">bar</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_225" n="HIAT:w" s="T56">külambiʔi</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_229" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_231" n="HIAT:w" s="T57">Tolʼkă</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_234" n="HIAT:w" s="T58">šo</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_237" n="HIAT:w" s="T59">păjdluʔpiʔi</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_241" n="HIAT:u" s="T60">
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <nts id="Seg_243" n="HIAT:ip">(</nts>
                  <ats e="T61" id="Seg_244" n="HIAT:non-pho" s="T60">BRK</ats>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip">)</nts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_250" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_252" n="HIAT:w" s="T61">Tüj</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_254" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_256" n="HIAT:w" s="T62">ĭm-</ts>
                  <nts id="Seg_257" n="HIAT:ip">)</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_260" n="HIAT:w" s="T63">ĭmbidə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_263" n="HIAT:w" s="T64">ej</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_266" n="HIAT:w" s="T65">kuliam</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_270" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_272" n="HIAT:w" s="T66">Ĭmbidə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_275" n="HIAT:w" s="T67">ej</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_279" n="HIAT:w" s="T68">nʼilol-</ts>
                  <nts id="Seg_280" n="HIAT:ip">)</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_283" n="HIAT:w" s="T69">nʼilgöliam</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_287" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_289" n="HIAT:w" s="T70">Bar</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_291" n="HIAT:ip">(</nts>
                  <ts e="T72" id="Seg_293" n="HIAT:w" s="T71">s-</ts>
                  <nts id="Seg_294" n="HIAT:ip">)</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_297" n="HIAT:w" s="T72">šalbə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_300" n="HIAT:w" s="T73">kalla</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_303" n="HIAT:w" s="T1117">dʼürbi</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_307" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_309" n="HIAT:w" s="T74">Tüj</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_312" n="HIAT:w" s="T75">măn</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_315" n="HIAT:w" s="T76">unnʼam</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_318" n="HIAT:w" s="T77">amnolaʔbəm</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_322" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_324" n="HIAT:w" s="T78">Šindidə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_327" n="HIAT:w" s="T79">naga</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_331" n="HIAT:w" s="T80">bar</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T81">külambiʔi</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">măn</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">nuzaŋbə</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_344" n="HIAT:u" s="T84">
                  <nts id="Seg_345" n="HIAT:ip">(</nts>
                  <nts id="Seg_346" n="HIAT:ip">(</nts>
                  <ats e="T85" id="Seg_347" n="HIAT:non-pho" s="T84">BRK</ats>
                  <nts id="Seg_348" n="HIAT:ip">)</nts>
                  <nts id="Seg_349" n="HIAT:ip">)</nts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_353" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_355" n="HIAT:w" s="T85">Tüj</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_358" n="HIAT:w" s="T86">bar</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_361" n="HIAT:w" s="T87">aktʼizaŋbə</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_364" n="HIAT:w" s="T88">bar</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_367" n="HIAT:w" s="T89">maluʔpi</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_371" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_373" n="HIAT:w" s="T90">Bar</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_376" n="HIAT:w" s="T91">noʔsi</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_379" n="HIAT:w" s="T92">özerluʔpi</ts>
                  <nts id="Seg_380" n="HIAT:ip">.</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_383" n="HIAT:u" s="T93">
                  <nts id="Seg_384" n="HIAT:ip">(</nts>
                  <nts id="Seg_385" n="HIAT:ip">(</nts>
                  <ats e="T94" id="Seg_386" n="HIAT:non-pho" s="T93">BRK</ats>
                  <nts id="Seg_387" n="HIAT:ip">)</nts>
                  <nts id="Seg_388" n="HIAT:ip">)</nts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_392" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_394" n="HIAT:w" s="T94">Măn</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_397" n="HIAT:w" s="T95">ugandə</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_400" n="HIAT:w" s="T96">kuvas</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_403" n="HIAT:w" s="T97">mobiam</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_407" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_409" n="HIAT:w" s="T98">A</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_412" n="HIAT:w" s="T99">giber</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_415" n="HIAT:w" s="T100">dĭ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_418" n="HIAT:w" s="T101">kuvas</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1118" id="Seg_421" n="HIAT:w" s="T102">kalla</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_424" n="HIAT:w" s="T1118">dʼürbi</ts>
                  <nts id="Seg_425" n="HIAT:ip">?</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_428" n="HIAT:u" s="T103">
                  <ts e="T1119" id="Seg_430" n="HIAT:w" s="T103">Kalla</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_433" n="HIAT:w" s="T1119">dʼürbi</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_436" n="HIAT:w" s="T104">urgo</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_439" n="HIAT:w" s="T105">măjazaŋdə</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_443" n="HIAT:w" s="T106">i</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_446" n="HIAT:w" s="T107">gijendə</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_449" n="HIAT:w" s="T108">naga</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_453" n="HIAT:u" s="T109">
                  <nts id="Seg_454" n="HIAT:ip">(</nts>
                  <nts id="Seg_455" n="HIAT:ip">(</nts>
                  <ats e="T110" id="Seg_456" n="HIAT:non-pho" s="T109">BRK</ats>
                  <nts id="Seg_457" n="HIAT:ip">)</nts>
                  <nts id="Seg_458" n="HIAT:ip">)</nts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_462" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_464" n="HIAT:w" s="T110">Tüj</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_467" n="HIAT:w" s="T111">dĭ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_470" n="HIAT:w" s="T112">kamendə</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">ej</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_476" n="HIAT:w" s="T114">šoləj</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_480" n="HIAT:u" s="T115">
                  <nts id="Seg_481" n="HIAT:ip">(</nts>
                  <nts id="Seg_482" n="HIAT:ip">(</nts>
                  <ats e="T116" id="Seg_483" n="HIAT:non-pho" s="T115">BRK</ats>
                  <nts id="Seg_484" n="HIAT:ip">)</nts>
                  <nts id="Seg_485" n="HIAT:ip">)</nts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_489" n="HIAT:u" s="T116">
                  <nts id="Seg_490" n="HIAT:ip">(</nts>
                  <nts id="Seg_491" n="HIAT:ip">(</nts>
                  <ats e="T117" id="Seg_492" n="HIAT:non-pho" s="T116">BRK</ats>
                  <nts id="Seg_493" n="HIAT:ip">)</nts>
                  <nts id="Seg_494" n="HIAT:ip">)</nts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_498" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_500" n="HIAT:w" s="T117">Măn</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_503" n="HIAT:w" s="T118">uʔbdəbiam</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_505" n="HIAT:ip">(</nts>
                  <ts e="T120" id="Seg_507" n="HIAT:w" s="T119">pereulăktə</ts>
                  <nts id="Seg_508" n="HIAT:ip">)</nts>
                  <nts id="Seg_509" n="HIAT:ip">,</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_511" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_513" n="HIAT:w" s="T120">šo-</ts>
                  <nts id="Seg_514" n="HIAT:ip">)</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_517" n="HIAT:w" s="T121">šonəga</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_521" n="HIAT:w" s="T122">aga</ts>
                  <nts id="Seg_522" n="HIAT:ip">)</nts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_526" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_528" n="HIAT:w" s="T123">Măn</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_531" n="HIAT:w" s="T124">măndəm:</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_534" n="HIAT:w" s="T125">ugandə</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_537" n="HIAT:w" s="T126">urgo</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_540" n="HIAT:w" s="T127">kuza</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_543" n="HIAT:w" s="T128">šonəga</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_547" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_549" n="HIAT:w" s="T129">Aparatsi</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_552" n="HIAT:w" s="T130">turaʔi</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_555" n="HIAT:w" s="T131">kürleʔbə</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_558" n="HIAT:w" s="T132">mĭnge</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_562" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_564" n="HIAT:w" s="T133">Dĭgəttə</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_567" n="HIAT:w" s="T134">măn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_570" n="HIAT:w" s="T135">nubiam</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_574" n="HIAT:w" s="T136">turagəʔ</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_577" n="HIAT:w" s="T137">toʔndə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_580" n="HIAT:w" s="T138">dĭ</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_582" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_584" n="HIAT:w" s="T139">m-</ts>
                  <nts id="Seg_585" n="HIAT:ip">)</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_588" n="HIAT:w" s="T140">bar</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_591" n="HIAT:w" s="T141">măna</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_593" n="HIAT:ip">(</nts>
                  <ts e="T143" id="Seg_595" n="HIAT:w" s="T142">k-</ts>
                  <nts id="Seg_596" n="HIAT:ip">)</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_599" n="HIAT:w" s="T143">kürbi</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_603" n="HIAT:u" s="T144">
                  <nts id="Seg_604" n="HIAT:ip">(</nts>
                  <nts id="Seg_605" n="HIAT:ip">(</nts>
                  <ats e="T145" id="Seg_606" n="HIAT:non-pho" s="T144">BRK</ats>
                  <nts id="Seg_607" n="HIAT:ip">)</nts>
                  <nts id="Seg_608" n="HIAT:ip">)</nts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_612" n="HIAT:u" s="T145">
                  <nts id="Seg_613" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_615" n="HIAT:w" s="T145">Dĭn</ts>
                  <nts id="Seg_616" n="HIAT:ip">)</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_619" n="HIAT:w" s="T146">nükegən</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_622" n="HIAT:w" s="T147">kolat</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_625" n="HIAT:w" s="T148">ibi</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_629" n="HIAT:w" s="T149">dĭn</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_633" n="HIAT:u" s="T150">
                  <nts id="Seg_634" n="HIAT:ip">(</nts>
                  <nts id="Seg_635" n="HIAT:ip">(</nts>
                  <ats e="T151" id="Seg_636" n="HIAT:non-pho" s="T150">BRK</ats>
                  <nts id="Seg_637" n="HIAT:ip">)</nts>
                  <nts id="Seg_638" n="HIAT:ip">)</nts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_642" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_644" n="HIAT:w" s="T151">Tĭn</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_647" n="HIAT:w" s="T152">kolat</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_650" n="HIAT:w" s="T153">ibi</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_653" n="HIAT:w" s="T154">dĭn</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_655" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_657" n="HIAT:w" s="T155">meit</ts>
                  <nts id="Seg_658" n="HIAT:ip">)</nts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_662" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_664" n="HIAT:w" s="T156">Dĭgəttə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_667" n="HIAT:w" s="T157">dĭʔnə</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_670" n="HIAT:w" s="T158">šobi</ts>
                  <nts id="Seg_671" n="HIAT:ip">,</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_674" n="HIAT:w" s="T159">măndə:</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_677" n="HIAT:w" s="T160">Măn</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_680" n="HIAT:w" s="T161">tănan</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_683" n="HIAT:w" s="T162">kola</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_686" n="HIAT:w" s="T163">detlem</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_689" n="HIAT:w" s="T164">teinen</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_693" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_695" n="HIAT:w" s="T165">A</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_698" n="HIAT:w" s="T166">măn</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_701" n="HIAT:w" s="T167">šobiam:</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_704" n="HIAT:w" s="T168">Tăn</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_707" n="HIAT:w" s="T169">kolal</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_710" n="HIAT:w" s="T170">meit</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_713" n="HIAT:w" s="T171">ibi</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_717" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_719" n="HIAT:w" s="T172">Măn</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_722" n="HIAT:w" s="T173">ej</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_725" n="HIAT:w" s="T174">ibiem</ts>
                  <nts id="Seg_726" n="HIAT:ip">"</nts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_730" n="HIAT:u" s="T175">
                  <nts id="Seg_731" n="HIAT:ip">(</nts>
                  <nts id="Seg_732" n="HIAT:ip">(</nts>
                  <ats e="T176" id="Seg_733" n="HIAT:non-pho" s="T175">BRK</ats>
                  <nts id="Seg_734" n="HIAT:ip">)</nts>
                  <nts id="Seg_735" n="HIAT:ip">)</nts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_739" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_741" n="HIAT:w" s="T176">Ĭmbi</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_744" n="HIAT:w" s="T177">dĭ</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_747" n="HIAT:w" s="T178">măna</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_750" n="HIAT:w" s="T179">mĭləj</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_754" n="HIAT:w" s="T180">idʼiʔeje</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_758" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_760" n="HIAT:w" s="T181">Nu</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_763" n="HIAT:w" s="T182">idʼiʔeʔe</ts>
                  <nts id="Seg_764" n="HIAT:ip">,</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_767" n="HIAT:w" s="T183">kabarləj</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_771" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_773" n="HIAT:w" s="T184">Dĭzeŋ</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_776" n="HIAT:w" s="T185">bostə</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_778" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_780" n="HIAT:w" s="T186">aktʼa=</ts>
                  <nts id="Seg_781" n="HIAT:ip">)</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_784" n="HIAT:w" s="T187">aktʼanə</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_786" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_788" n="HIAT:w" s="T188">š-</ts>
                  <nts id="Seg_789" n="HIAT:ip">)</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_792" n="HIAT:w" s="T189">sadarbiʔi</ts>
                  <nts id="Seg_793" n="HIAT:ip">.</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_796" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_798" n="HIAT:w" s="T190">A</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_801" n="HIAT:w" s="T191">tăn</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_804" n="HIAT:w" s="T192">ej</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_807" n="HIAT:w" s="T193">mĭbiel</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_810" n="HIAT:w" s="T194">aktʼa</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_814" n="HIAT:u" s="T195">
                  <nts id="Seg_815" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_817" n="HIAT:w" s="T195">I</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_820" n="HIAT:w" s="T196">dĭ-</ts>
                  <nts id="Seg_821" n="HIAT:ip">)</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_824" n="HIAT:w" s="T197">I</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_826" n="HIAT:ip">(</nts>
                  <ts e="T199" id="Seg_828" n="HIAT:w" s="T198">dĭn=</ts>
                  <nts id="Seg_829" n="HIAT:ip">)</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_832" n="HIAT:w" s="T199">dĭ</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_835" n="HIAT:w" s="T200">kumen</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_838" n="HIAT:w" s="T201">mĭləʔi</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_841" n="HIAT:w" s="T202">kabarləj</ts>
                  <nts id="Seg_842" n="HIAT:ip">.</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_845" n="HIAT:u" s="T203">
                  <nts id="Seg_846" n="HIAT:ip">(</nts>
                  <nts id="Seg_847" n="HIAT:ip">(</nts>
                  <ats e="T204" id="Seg_848" n="HIAT:non-pho" s="T203">BRK</ats>
                  <nts id="Seg_849" n="HIAT:ip">)</nts>
                  <nts id="Seg_850" n="HIAT:ip">)</nts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_854" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_856" n="HIAT:w" s="T204">Dĭgəttə</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_859" n="HIAT:w" s="T205">măn</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_861" n="HIAT:ip">(</nts>
                  <ts e="T207" id="Seg_863" n="HIAT:w" s="T206">u</ts>
                  <nts id="Seg_864" n="HIAT:ip">)</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_867" n="HIAT:w" s="T207">kambiam</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_870" n="HIAT:w" s="T208">Anissʼanə</ts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_874" n="HIAT:w" s="T209">dĭ</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_877" n="HIAT:w" s="T210">măndə:</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_880" n="HIAT:w" s="T211">Jelʼa</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_883" n="HIAT:w" s="T212">surarbi</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_886" n="HIAT:w" s="T213">munəʔi</ts>
                  <nts id="Seg_887" n="HIAT:ip">,</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_890" n="HIAT:w" s="T214">măn</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_893" n="HIAT:w" s="T215">oʔbdəbiam</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_896" n="HIAT:w" s="T216">bʼeʔ</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_899" n="HIAT:w" s="T217">munəjʔ</ts>
                  <nts id="Seg_900" n="HIAT:ip">.</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_903" n="HIAT:u" s="T218">
                  <nts id="Seg_904" n="HIAT:ip">(</nts>
                  <ts e="T219" id="Seg_906" n="HIAT:w" s="T218">Mĭ-</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_909" n="HIAT:w" s="T219">me-</ts>
                  <nts id="Seg_910" n="HIAT:ip">)</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_913" n="HIAT:w" s="T220">Mĭləm</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_915" n="HIAT:ip">(</nts>
                  <ts e="T222" id="Seg_917" n="HIAT:w" s="T221">bʼeʔ</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_920" n="HIAT:w" s="T222">k-</ts>
                  <nts id="Seg_921" n="HIAT:ip">)</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_924" n="HIAT:w" s="T223">onʼiʔ</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_927" n="HIAT:w" s="T224">munəjʔ</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_930" n="HIAT:w" s="T225">bʼeʔ</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_933" n="HIAT:w" s="T226">kăpʼejkazi</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_936" n="HIAT:w" s="T227">mĭləm</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_939" n="HIAT:w" s="T228">dĭʔnə</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_943" n="HIAT:u" s="T229">
                  <nts id="Seg_944" n="HIAT:ip">(</nts>
                  <nts id="Seg_945" n="HIAT:ip">(</nts>
                  <ats e="T230" id="Seg_946" n="HIAT:non-pho" s="T229">BRK</ats>
                  <nts id="Seg_947" n="HIAT:ip">)</nts>
                  <nts id="Seg_948" n="HIAT:ip">)</nts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_952" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_954" n="HIAT:w" s="T230">Jelʼa</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_957" n="HIAT:w" s="T231">măndə:</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_960" n="HIAT:w" s="T232">Kanžəbəj</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_963" n="HIAT:w" s="T233">miʔnʼibeʔ</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_967" n="HIAT:w" s="T234">a</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_970" n="HIAT:w" s="T235">măn</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_973" n="HIAT:w" s="T236">măndəm:</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_976" n="HIAT:w" s="T237">Dʼok</ts>
                  <nts id="Seg_977" n="HIAT:ip">,</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_980" n="HIAT:w" s="T238">ej</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_983" n="HIAT:w" s="T239">kalam</ts>
                  <nts id="Seg_984" n="HIAT:ip">,</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_987" n="HIAT:w" s="T240">ugandə</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_990" n="HIAT:w" s="T241">kuŋgəʔ</ts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_994" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_996" n="HIAT:w" s="T242">Ĭmbi</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_999" n="HIAT:w" s="T243">kuŋgeʔ</ts>
                  <nts id="Seg_1000" n="HIAT:ip">?</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_1003" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_1005" n="HIAT:w" s="T244">Amnaʔ</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_1009" n="HIAT:u" s="T245">
                  <nts id="Seg_1010" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_1012" n="HIAT:w" s="T245">Süjö-</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1015" n="HIAT:w" s="T246">süjönə=</ts>
                  <nts id="Seg_1016" n="HIAT:ip">)</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1019" n="HIAT:w" s="T247">Bazaj</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1022" n="HIAT:w" s="T248">süjönə</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_1026" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_1028" n="HIAT:w" s="T249">I</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1031" n="HIAT:w" s="T250">büžü</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1034" n="HIAT:w" s="T251">šolal</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_1038" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1040" n="HIAT:w" s="T252">Dʼok</ts>
                  <nts id="Seg_1041" n="HIAT:ip">,</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1044" n="HIAT:w" s="T253">măn</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1047" n="HIAT:w" s="T254">pimniem</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1050" n="HIAT:w" s="T255">bazaj</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1052" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_1054" n="HIAT:w" s="T256">sʼü-</ts>
                  <nts id="Seg_1055" n="HIAT:ip">)</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1058" n="HIAT:w" s="T257">süjönə</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1060" n="HIAT:ip">(</nts>
                  <ts e="T259" id="Seg_1062" n="HIAT:w" s="T258">amnəsʼtə</ts>
                  <nts id="Seg_1063" n="HIAT:ip">)</nts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_1067" n="HIAT:u" s="T259">
                  <ts e="T259.tx-PKZ.1" id="Seg_1069" n="HIAT:w" s="T259">A</ts>
                  <nts id="Seg_1070" n="HIAT:ip">_</nts>
                  <ts e="T260" id="Seg_1072" n="HIAT:w" s="T259.tx-PKZ.1">to</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1075" n="HIAT:w" s="T260">külalləl</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1079" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_1081" n="HIAT:w" s="T261">Dʼok</ts>
                  <nts id="Seg_1082" n="HIAT:ip">,</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1085" n="HIAT:w" s="T262">ej</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1088" n="HIAT:w" s="T263">külalləl</ts>
                  <nts id="Seg_1089" n="HIAT:ip">,</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1092" n="HIAT:w" s="T264">dĭn</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1095" n="HIAT:w" s="T265">jakše</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1098" n="HIAT:w" s="T266">amnozittə</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1102" n="HIAT:w" s="T267">kak</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1105" n="HIAT:w" s="T268">turagən</ts>
                  <nts id="Seg_1106" n="HIAT:ip">.</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1109" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1111" n="HIAT:w" s="T269">Büžü</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1114" n="HIAT:w" s="T270">maʔnəl</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1117" n="HIAT:w" s="T271">šolal</ts>
                  <nts id="Seg_1118" n="HIAT:ip">,</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1121" n="HIAT:w" s="T272">miʔnʼibeʔ</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1124" n="HIAT:w" s="T273">amnolal</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1128" n="HIAT:u" s="T274">
                  <nts id="Seg_1129" n="HIAT:ip">(</nts>
                  <nts id="Seg_1130" n="HIAT:ip">(</nts>
                  <ats e="T275" id="Seg_1131" n="HIAT:non-pho" s="T274">BRK</ats>
                  <nts id="Seg_1132" n="HIAT:ip">)</nts>
                  <nts id="Seg_1133" n="HIAT:ip">)</nts>
                  <nts id="Seg_1134" n="HIAT:ip">.</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1137" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1139" n="HIAT:w" s="T275">Nagurköʔ</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1142" n="HIAT:w" s="T276">kalləbaʔ</ts>
                  <nts id="Seg_1143" n="HIAT:ip">.</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1146" n="HIAT:u" s="T277">
                  <nts id="Seg_1147" n="HIAT:ip">(</nts>
                  <nts id="Seg_1148" n="HIAT:ip">(</nts>
                  <ats e="T278" id="Seg_1149" n="HIAT:non-pho" s="T277">BRK</ats>
                  <nts id="Seg_1150" n="HIAT:ip">)</nts>
                  <nts id="Seg_1151" n="HIAT:ip">)</nts>
                  <nts id="Seg_1152" n="HIAT:ip">.</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1155" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1157" n="HIAT:w" s="T278">Nüdʼin</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1160" n="HIAT:w" s="T279">kalam</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1163" n="HIAT:w" s="T280">onʼiʔ</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1166" n="HIAT:w" s="T281">tibinə</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1170" n="HIAT:u" s="T282">
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T283" id="Seg_1173" n="HIAT:w" s="T282">T-</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1176" n="HIAT:w" s="T283">tĭbi</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1179" n="HIAT:w" s="T284">tĭ-</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1182" n="HIAT:w" s="T285">tĭbi=</ts>
                  <nts id="Seg_1183" n="HIAT:ip">)</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1186" n="HIAT:w" s="T286">Onʼiʔ</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1189" n="HIAT:w" s="T287">kuzanə</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1193" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1195" n="HIAT:w" s="T288">Măllam:</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1197" n="HIAT:ip">(</nts>
                  <ts e="T290" id="Seg_1199" n="HIAT:w" s="T289">Kun-</ts>
                  <nts id="Seg_1200" n="HIAT:ip">)</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1203" n="HIAT:w" s="T290">Kundə</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1206" n="HIAT:w" s="T291">dĭ</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1209" n="HIAT:w" s="T292">nükem</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1212" n="HIAT:w" s="T293">bălʼnʼitsanə</ts>
                  <nts id="Seg_1213" n="HIAT:ip">.</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1216" n="HIAT:u" s="T294">
                  <nts id="Seg_1217" n="HIAT:ip">(</nts>
                  <ts e="T295" id="Seg_1219" n="HIAT:w" s="T294">Dĭʔnə</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1222" n="HIAT:w" s="T295">s-</ts>
                  <nts id="Seg_1223" n="HIAT:ip">)</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1226" n="HIAT:w" s="T296">Dĭʔnə</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1229" n="HIAT:w" s="T297">sazən</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1232" n="HIAT:w" s="T298">mĭbiʔi</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1236" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1238" n="HIAT:w" s="T299">A</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1241" n="HIAT:w" s="T300">ej</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1243" n="HIAT:ip">(</nts>
                  <ts e="T302" id="Seg_1245" n="HIAT:w" s="T301">kol-</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1248" n="HIAT:w" s="T302">ko-</ts>
                  <nts id="Seg_1249" n="HIAT:ip">)</nts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1252" n="HIAT:w" s="T303">kundlal</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1255" n="HIAT:w" s="T304">dăk</ts>
                  <nts id="Seg_1256" n="HIAT:ip">,</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1259" n="HIAT:w" s="T305">kăštʼit</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1262" n="HIAT:w" s="T306">скорая</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1265" n="HIAT:w" s="T307">помощь</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1269" n="HIAT:w" s="T308">dĭ</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1272" n="HIAT:w" s="T309">šoləj</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1275" n="HIAT:w" s="T310">da</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1278" n="HIAT:w" s="T311">kunnalləj</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1281" n="HIAT:w" s="T312">dĭm</ts>
                  <nts id="Seg_1282" n="HIAT:ip">.</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1285" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1287" n="HIAT:w" s="T313">Dăre</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1290" n="HIAT:w" s="T314">nörbələm</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1293" n="HIAT:w" s="T315">diʔnə</ts>
                  <nts id="Seg_1294" n="HIAT:ip">,</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1297" n="HIAT:w" s="T316">možet</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1300" n="HIAT:w" s="T317">dĭ</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1303" n="HIAT:w" s="T318">nʼilgöləj</ts>
                  <nts id="Seg_1304" n="HIAT:ip">.</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1307" n="HIAT:u" s="T319">
                  <ts e="T319.tx-PKZ.1" id="Seg_1309" n="HIAT:w" s="T319">A</ts>
                  <nts id="Seg_1310" n="HIAT:ip">_</nts>
                  <ts e="T320" id="Seg_1312" n="HIAT:w" s="T319.tx-PKZ.1">to</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1315" n="HIAT:w" s="T320">vedʼ</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1318" n="HIAT:w" s="T321">tăn</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1321" n="HIAT:w" s="T322">kujdərgan</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1324" n="HIAT:w" s="T323">ibiel</ts>
                  <nts id="Seg_1325" n="HIAT:ip">,</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1328" n="HIAT:w" s="T324">a</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1331" n="HIAT:w" s="T325">il</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1334" n="HIAT:w" s="T326">bar</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1337" n="HIAT:w" s="T327">măllia:</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1339" n="HIAT:ip">(</nts>
                  <ts e="T329" id="Seg_1341" n="HIAT:w" s="T328">Kod-</ts>
                  <nts id="Seg_1342" n="HIAT:ip">)</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1345" n="HIAT:w" s="T329">Kujdərgan</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1348" n="HIAT:w" s="T330">ibiel</ts>
                  <nts id="Seg_1349" n="HIAT:ip">,</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1352" n="HIAT:w" s="T331">bostə</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1355" n="HIAT:w" s="T332">ĭmbidə</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1358" n="HIAT:w" s="T333">ej</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1361" n="HIAT:w" s="T334">kabažarbia</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1364" n="HIAT:w" s="T335">dĭ</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1367" n="HIAT:w" s="T336">nükenə</ts>
                  <nts id="Seg_1368" n="HIAT:ip">"</nts>
                  <nts id="Seg_1369" n="HIAT:ip">.</nts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1372" n="HIAT:u" s="T337">
                  <nts id="Seg_1373" n="HIAT:ip">(</nts>
                  <nts id="Seg_1374" n="HIAT:ip">(</nts>
                  <ats e="T338" id="Seg_1375" n="HIAT:non-pho" s="T337">BRK</ats>
                  <nts id="Seg_1376" n="HIAT:ip">)</nts>
                  <nts id="Seg_1377" n="HIAT:ip">)</nts>
                  <nts id="Seg_1378" n="HIAT:ip">.</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1381" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1383" n="HIAT:w" s="T338">A</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1386" n="HIAT:w" s="T339">abal</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1389" n="HIAT:w" s="T340">külambi</ts>
                  <nts id="Seg_1390" n="HIAT:ip">,</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1393" n="HIAT:w" s="T341">a</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1396" n="HIAT:w" s="T342">tura</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1398" n="HIAT:ip">(</nts>
                  <ts e="T344" id="Seg_1400" n="HIAT:w" s="T343">nan</ts>
                  <nts id="Seg_1401" n="HIAT:ip">)</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1404" n="HIAT:w" s="T344">vedʼ</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1407" n="HIAT:w" s="T345">tăn</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1410" n="HIAT:w" s="T346">ibiel</ts>
                  <nts id="Seg_1411" n="HIAT:ip">.</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1414" n="HIAT:u" s="T347">
                  <nts id="Seg_1415" n="HIAT:ip">(</nts>
                  <nts id="Seg_1416" n="HIAT:ip">(</nts>
                  <ats e="T348" id="Seg_1417" n="HIAT:non-pho" s="T347">BRK</ats>
                  <nts id="Seg_1418" n="HIAT:ip">)</nts>
                  <nts id="Seg_1419" n="HIAT:ip">)</nts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1423" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1425" n="HIAT:w" s="T348">Men</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1428" n="HIAT:w" s="T349">unnʼa</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1431" n="HIAT:w" s="T350">amnobi</ts>
                  <nts id="Seg_1432" n="HIAT:ip">,</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1435" n="HIAT:w" s="T351">amnobi</ts>
                  <nts id="Seg_1436" n="HIAT:ip">.</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1439" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1441" n="HIAT:w" s="T352">Скучно</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1444" n="HIAT:w" s="T353">unnʼanə</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1447" n="HIAT:w" s="T354">amnozittə</ts>
                  <nts id="Seg_1448" n="HIAT:ip">,</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1451" n="HIAT:w" s="T355">kalam</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1454" n="HIAT:w" s="T356">ele</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1457" n="HIAT:w" s="T357">boskəndə</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1460" n="HIAT:w" s="T358">kulim</ts>
                  <nts id="Seg_1461" n="HIAT:ip">.</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1464" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1466" n="HIAT:w" s="T359">Kambi</ts>
                  <nts id="Seg_1467" n="HIAT:ip">,</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1470" n="HIAT:w" s="T360">kambi</ts>
                  <nts id="Seg_1471" n="HIAT:ip">,</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1474" n="HIAT:w" s="T361">kubi</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1477" n="HIAT:w" s="T362">zajtsəm</ts>
                  <nts id="Seg_1478" n="HIAT:ip">.</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1481" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1483" n="HIAT:w" s="T363">Davaj</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1486" n="HIAT:w" s="T364">tănzi</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1489" n="HIAT:w" s="T365">amnozittə</ts>
                  <nts id="Seg_1490" n="HIAT:ip">?</nts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1493" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1495" n="HIAT:w" s="T366">Davaj</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1498" n="HIAT:w" s="T367">amnozittə</ts>
                  <nts id="Seg_1499" n="HIAT:ip">.</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1502" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1504" n="HIAT:w" s="T368">Dĭgəttə</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1507" n="HIAT:w" s="T369">nüdʼi</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1510" n="HIAT:w" s="T370">molambi</ts>
                  <nts id="Seg_1511" n="HIAT:ip">,</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1514" n="HIAT:w" s="T371">dĭzeŋ</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1517" n="HIAT:w" s="T372">iʔpiʔi</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1520" n="HIAT:w" s="T373">kunolzittə</ts>
                  <nts id="Seg_1521" n="HIAT:ip">.</nts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1524" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1526" n="HIAT:w" s="T374">Zajəs</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1529" n="HIAT:w" s="T375">kunolluʔpi</ts>
                  <nts id="Seg_1530" n="HIAT:ip">,</nts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1533" n="HIAT:w" s="T376">a</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1535" n="HIAT:ip">(</nts>
                  <ts e="T378" id="Seg_1537" n="HIAT:w" s="T377">mei-</ts>
                  <nts id="Seg_1538" n="HIAT:ip">)</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1541" n="HIAT:w" s="T378">men</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1544" n="HIAT:w" s="T379">ej</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1547" n="HIAT:w" s="T380">kunollia</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1551" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1553" n="HIAT:w" s="T381">Nüdʼin</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1556" n="HIAT:w" s="T382">bar</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1559" n="HIAT:w" s="T383">kürümleʔbə</ts>
                  <nts id="Seg_1560" n="HIAT:ip">.</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1563" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1565" n="HIAT:w" s="T384">A</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1568" n="HIAT:w" s="T385">zajəs</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1571" n="HIAT:w" s="T386">nereʔluʔpi</ts>
                  <nts id="Seg_1572" n="HIAT:ip">.</nts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1575" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1577" n="HIAT:w" s="T387">Sĭjdə</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1580" n="HIAT:w" s="T388">pʼatkəndə</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_1583" n="HIAT:w" s="T389">kalla</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1586" n="HIAT:w" s="T1120">dʼürbi</ts>
                  <nts id="Seg_1587" n="HIAT:ip">.</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1590" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1592" n="HIAT:w" s="T390">Ĭmbi</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1595" n="HIAT:w" s="T391">kürümniel</ts>
                  <nts id="Seg_1596" n="HIAT:ip">?</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1599" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1601" n="HIAT:w" s="T392">Šoləj</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1604" n="HIAT:w" s="T393">volk</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1607" n="HIAT:w" s="T394">i</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1610" n="HIAT:w" s="T395">miʔnʼibeʔ</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1613" n="HIAT:w" s="T396">amnəj</ts>
                  <nts id="Seg_1614" n="HIAT:ip">.</nts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1617" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1619" n="HIAT:w" s="T397">A</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1622" n="HIAT:w" s="T398">men</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1625" n="HIAT:w" s="T399">măndə:</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1628" n="HIAT:w" s="T400">Ej</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1631" n="HIAT:w" s="T401">jakšə</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1634" n="HIAT:w" s="T402">măn</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1637" n="HIAT:w" s="T403">elem</ts>
                  <nts id="Seg_1638" n="HIAT:ip">.</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1641" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1643" n="HIAT:w" s="T404">Kalam</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1645" n="HIAT:ip">(</nts>
                  <ts e="T406" id="Seg_1647" n="HIAT:w" s="T405">volkə</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1650" n="HIAT:w" s="T406">is-</ts>
                  <nts id="Seg_1651" n="HIAT:ip">)</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1654" n="HIAT:w" s="T407">volk</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1656" n="HIAT:ip">(</nts>
                  <ts e="T409" id="Seg_1658" n="HIAT:w" s="T408">măndərzittə</ts>
                  <nts id="Seg_1659" n="HIAT:ip">)</nts>
                  <nts id="Seg_1660" n="HIAT:ip">.</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1663" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1665" n="HIAT:w" s="T409">Dĭgəttə</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1668" n="HIAT:w" s="T410">kambi</ts>
                  <nts id="Seg_1669" n="HIAT:ip">.</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1672" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1674" n="HIAT:w" s="T411">Šonəga</ts>
                  <nts id="Seg_1675" n="HIAT:ip">,</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1678" n="HIAT:w" s="T412">šonəga</ts>
                  <nts id="Seg_1679" n="HIAT:ip">,</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1682" n="HIAT:w" s="T413">volk</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1685" n="HIAT:w" s="T414">dĭʔnə</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1688" n="HIAT:w" s="T415">šonəga</ts>
                  <nts id="Seg_1689" n="HIAT:ip">.</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1692" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1694" n="HIAT:w" s="T416">Davaj</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1697" n="HIAT:w" s="T417">tănzi</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1700" n="HIAT:w" s="T418">amnozittə</ts>
                  <nts id="Seg_1701" n="HIAT:ip">?</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1704" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1706" n="HIAT:w" s="T419">Davaj</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1709" n="HIAT:w" s="T420">amnozittə</ts>
                  <nts id="Seg_1710" n="HIAT:ip">.</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1713" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1715" n="HIAT:w" s="T421">Nüdʼi</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1718" n="HIAT:w" s="T422">šobi</ts>
                  <nts id="Seg_1719" n="HIAT:ip">,</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1721" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_1723" n="HIAT:w" s="T423">dĭ-</ts>
                  <nts id="Seg_1724" n="HIAT:ip">)</nts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1727" n="HIAT:w" s="T424">dĭzeŋ</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1730" n="HIAT:w" s="T425">iʔpiʔi</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1733" n="HIAT:w" s="T426">kunolzittə</ts>
                  <nts id="Seg_1734" n="HIAT:ip">.</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1737" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1739" n="HIAT:w" s="T427">Men</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1742" n="HIAT:w" s="T428">nüdʼin</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1745" n="HIAT:w" s="T429">kürümnuʔpi</ts>
                  <nts id="Seg_1746" n="HIAT:ip">.</nts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1749" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1751" n="HIAT:w" s="T430">A</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1754" n="HIAT:w" s="T431">volk</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1757" n="HIAT:w" s="T432">măndə:</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1760" n="HIAT:w" s="T433">Ĭmbi</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1763" n="HIAT:w" s="T434">kürümnaʔbəl</ts>
                  <nts id="Seg_1764" n="HIAT:ip">?</nts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1767" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1769" n="HIAT:w" s="T435">Tüj</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1772" n="HIAT:w" s="T436">urgaːba</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1775" n="HIAT:w" s="T437">šoləj</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1778" n="HIAT:w" s="T438">i</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1781" n="HIAT:w" s="T439">miʔnʼibeʔ</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1784" n="HIAT:w" s="T440">amnəj</ts>
                  <nts id="Seg_1785" n="HIAT:ip">.</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1788" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1790" n="HIAT:w" s="T441">Dĭgəttə</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1793" n="HIAT:w" s="T442">men</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1796" n="HIAT:w" s="T443">ertən</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1799" n="HIAT:w" s="T444">uʔbdəbi:</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1802" n="HIAT:w" s="T445">Ej</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1805" n="HIAT:w" s="T446">jakše</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1808" n="HIAT:w" s="T447">elem</ts>
                  <nts id="Seg_1809" n="HIAT:ip">,</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1812" n="HIAT:w" s="T448">kalam</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1815" n="HIAT:w" s="T449">urgaːba</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1817" n="HIAT:ip">(</nts>
                  <ts e="T451" id="Seg_1819" n="HIAT:w" s="T450">mărno-</ts>
                  <nts id="Seg_1820" n="HIAT:ip">)</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1823" n="HIAT:w" s="T451">măndərzittə</ts>
                  <nts id="Seg_1824" n="HIAT:ip">.</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1827" n="HIAT:u" s="T452">
                  <nts id="Seg_1828" n="HIAT:ip">(</nts>
                  <ts e="T453" id="Seg_1830" n="HIAT:w" s="T452">Dəg-</ts>
                  <nts id="Seg_1831" n="HIAT:ip">)</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1834" n="HIAT:w" s="T453">Dĭgəttə</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1837" n="HIAT:w" s="T454">kambi</ts>
                  <nts id="Seg_1838" n="HIAT:ip">.</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1841" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1843" n="HIAT:w" s="T455">Šonəga</ts>
                  <nts id="Seg_1844" n="HIAT:ip">,</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1847" n="HIAT:w" s="T456">šonəga</ts>
                  <nts id="Seg_1848" n="HIAT:ip">,</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1851" n="HIAT:w" s="T457">urgaːba</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1854" n="HIAT:w" s="T458">šonəga</ts>
                  <nts id="Seg_1855" n="HIAT:ip">.</nts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1858" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1860" n="HIAT:w" s="T459">Urgaːba</ts>
                  <nts id="Seg_1861" n="HIAT:ip">,</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1864" n="HIAT:w" s="T460">urgaːba</ts>
                  <nts id="Seg_1865" n="HIAT:ip">,</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1868" n="HIAT:w" s="T461">davaj</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1871" n="HIAT:w" s="T462">tănzi</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1874" n="HIAT:w" s="T463">amnozittə</ts>
                  <nts id="Seg_1875" n="HIAT:ip">.</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_1878" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1880" n="HIAT:w" s="T464">Davaj</ts>
                  <nts id="Seg_1881" n="HIAT:ip">.</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_1884" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_1886" n="HIAT:w" s="T465">Dĭgəttə</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1889" n="HIAT:w" s="T466">nüdʼi</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1892" n="HIAT:w" s="T467">molambi</ts>
                  <nts id="Seg_1893" n="HIAT:ip">,</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1895" n="HIAT:ip">(</nts>
                  <ts e="T469" id="Seg_1897" n="HIAT:w" s="T468">iʔpil-</ts>
                  <nts id="Seg_1898" n="HIAT:ip">)</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1901" n="HIAT:w" s="T469">iʔpiʔi</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1904" n="HIAT:w" s="T470">kunolzittə</ts>
                  <nts id="Seg_1905" n="HIAT:ip">.</nts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1908" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1910" n="HIAT:w" s="T471">Men</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1913" n="HIAT:w" s="T472">nüdʼin</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1916" n="HIAT:w" s="T473">bar</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1919" n="HIAT:w" s="T474">kürümnuʔpi</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1922" n="HIAT:w" s="T475">bar</ts>
                  <nts id="Seg_1923" n="HIAT:ip">.</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1926" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1928" n="HIAT:w" s="T476">A</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1931" n="HIAT:w" s="T477">urgaːba:</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1934" n="HIAT:w" s="T478">Ĭmbi</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1937" n="HIAT:w" s="T479">kürümniel</ts>
                  <nts id="Seg_1938" n="HIAT:ip">?</nts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1941" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1943" n="HIAT:w" s="T480">Kuza</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1946" n="HIAT:w" s="T481">šoləj</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1949" n="HIAT:w" s="T482">da</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1952" n="HIAT:w" s="T483">miʔnʼibeʔ</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1955" n="HIAT:w" s="T484">kutləj</ts>
                  <nts id="Seg_1956" n="HIAT:ip">.</nts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1959" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1961" n="HIAT:w" s="T485">A</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1964" n="HIAT:w" s="T486">dĭ</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1967" n="HIAT:w" s="T487">măndə:</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1970" n="HIAT:w" s="T488">Ej</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1973" n="HIAT:w" s="T489">jakše</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1976" n="HIAT:w" s="T490">elem</ts>
                  <nts id="Seg_1977" n="HIAT:ip">,</nts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1980" n="HIAT:w" s="T491">kalam</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1983" n="HIAT:w" s="T492">kuza</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1986" n="HIAT:w" s="T493">măndərzittə</ts>
                  <nts id="Seg_1987" n="HIAT:ip">.</nts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1990" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1992" n="HIAT:w" s="T494">Ertən</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1995" n="HIAT:w" s="T495">uʔbdəbi</ts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1999" n="HIAT:w" s="T496">kambi</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2001" n="HIAT:ip">(</nts>
                  <nts id="Seg_2002" n="HIAT:ip">(</nts>
                  <ats e="T498" id="Seg_2003" n="HIAT:non-pho" s="T497">…</ats>
                  <nts id="Seg_2004" n="HIAT:ip">)</nts>
                  <nts id="Seg_2005" n="HIAT:ip">)</nts>
                  <nts id="Seg_2006" n="HIAT:ip">.</nts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_2009" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_2011" n="HIAT:w" s="T498">Bar</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2014" n="HIAT:w" s="T499">paʔi</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2017" n="HIAT:w" s="T500">mĭmbi</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2021" n="HIAT:w" s="T501">mĭmbi</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_2025" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_2027" n="HIAT:w" s="T502">Dĭgəttə</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2030" n="HIAT:w" s="T503">sʼtʼeptə</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2033" n="HIAT:w" s="T504">šobi</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2037" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2039" n="HIAT:w" s="T505">Kuliat</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2042" n="HIAT:w" s="T506">bar:</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2045" n="HIAT:w" s="T507">kuza</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2048" n="HIAT:w" s="T508">šonəga</ts>
                  <nts id="Seg_2049" n="HIAT:ip">.</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2052" n="HIAT:u" s="T509">
                  <nts id="Seg_2053" n="HIAT:ip">(</nts>
                  <ts e="T510" id="Seg_2055" n="HIAT:w" s="T509">Paʔ-</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2058" n="HIAT:w" s="T510">paʔ-</ts>
                  <nts id="Seg_2059" n="HIAT:ip">)</nts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2062" n="HIAT:w" s="T511">Paʔi</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2065" n="HIAT:w" s="T512">izittə</ts>
                  <nts id="Seg_2066" n="HIAT:ip">,</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2069" n="HIAT:w" s="T513">inezi</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_2073" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2075" n="HIAT:w" s="T514">Dĭgəttə</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2078" n="HIAT:w" s="T515">dĭ</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2081" n="HIAT:w" s="T516">măndə:</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2084" n="HIAT:w" s="T517">Kuza</ts>
                  <nts id="Seg_2085" n="HIAT:ip">,</nts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2088" n="HIAT:w" s="T518">kuza</ts>
                  <nts id="Seg_2089" n="HIAT:ip">,</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2092" n="HIAT:w" s="T519">davaj</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2095" n="HIAT:w" s="T520">amnozittə</ts>
                  <nts id="Seg_2096" n="HIAT:ip">.</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2099" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_2101" n="HIAT:w" s="T521">Dĭ</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2104" n="HIAT:w" s="T522">deʔpi</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2107" n="HIAT:w" s="T523">dĭm</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2110" n="HIAT:w" s="T524">maʔndə</ts>
                  <nts id="Seg_2111" n="HIAT:ip">.</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2114" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2116" n="HIAT:w" s="T525">Iʔbəbiʔi</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2119" n="HIAT:w" s="T526">kunolzittə</ts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2123" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_2125" n="HIAT:w" s="T527">Kuza</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2128" n="HIAT:w" s="T528">kunolluʔpi</ts>
                  <nts id="Seg_2129" n="HIAT:ip">,</nts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2132" n="HIAT:w" s="T529">a</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2135" n="HIAT:w" s="T530">men</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2138" n="HIAT:w" s="T531">bar</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2141" n="HIAT:w" s="T532">davaj</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2144" n="HIAT:w" s="T533">kürümzittə</ts>
                  <nts id="Seg_2145" n="HIAT:ip">.</nts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2148" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_2150" n="HIAT:w" s="T534">A</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2153" n="HIAT:w" s="T535">kuza</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2156" n="HIAT:w" s="T536">uʔbdəbi</ts>
                  <nts id="Seg_2157" n="HIAT:ip">,</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2160" n="HIAT:w" s="T537">măndə:</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2163" n="HIAT:w" s="T538">Tăn</ts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2166" n="HIAT:w" s="T539">püjölial</ts>
                  <nts id="Seg_2167" n="HIAT:ip">,</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2170" n="HIAT:w" s="T540">amoraʔ</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2173" n="HIAT:w" s="T541">da</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2176" n="HIAT:w" s="T542">kunolaʔ</ts>
                  <nts id="Seg_2177" n="HIAT:ip">,</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2180" n="HIAT:w" s="T543">măna</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2182" n="HIAT:ip">(</nts>
                  <ts e="T545" id="Seg_2184" n="HIAT:w" s="T544">deʔ=</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2187" n="HIAT:w" s="T545">dĭʔ</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2190" n="HIAT:w" s="T546">-</ts>
                  <nts id="Seg_2191" n="HIAT:ip">)</nts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2194" n="HIAT:w" s="T547">mĭʔ</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2197" n="HIAT:w" s="T548">kunolzittə</ts>
                  <nts id="Seg_2198" n="HIAT:ip">.</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2201" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2203" n="HIAT:w" s="T549">Bar</ts>
                  <nts id="Seg_2204" n="HIAT:ip">!</nts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2207" n="HIAT:u" s="T550">
                  <nts id="Seg_2208" n="HIAT:ip">(</nts>
                  <nts id="Seg_2209" n="HIAT:ip">(</nts>
                  <ats e="T551" id="Seg_2210" n="HIAT:non-pho" s="T550">BRK</ats>
                  <nts id="Seg_2211" n="HIAT:ip">)</nts>
                  <nts id="Seg_2212" n="HIAT:ip">)</nts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2216" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2218" n="HIAT:w" s="T551">Onʼiʔ</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2221" n="HIAT:w" s="T552">kuza</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2224" n="HIAT:w" s="T553">kambi</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2227" n="HIAT:w" s="T554">pajlaʔ</ts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2231" n="HIAT:u" s="T555">
                  <nts id="Seg_2232" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_2234" n="HIAT:w" s="T555">Pa-</ts>
                  <nts id="Seg_2235" n="HIAT:ip">)</nts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2238" n="HIAT:w" s="T556">Paʔi</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2241" n="HIAT:w" s="T557">ibi</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2244" n="HIAT:w" s="T558">i</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2247" n="HIAT:w" s="T559">šonəga</ts>
                  <nts id="Seg_2248" n="HIAT:ip">.</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2251" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2253" n="HIAT:w" s="T560">Dĭʔnə</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2256" n="HIAT:w" s="T561">urgaːba</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2259" n="HIAT:w" s="T562">šonəga</ts>
                  <nts id="Seg_2260" n="HIAT:ip">.</nts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2263" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2265" n="HIAT:w" s="T563">Măn</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2268" n="HIAT:w" s="T564">tüjö</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2271" n="HIAT:w" s="T565">tăn</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2274" n="HIAT:w" s="T566">amnam</ts>
                  <nts id="Seg_2275" n="HIAT:ip">.</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2278" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2280" n="HIAT:w" s="T567">Tăn</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2283" n="HIAT:w" s="T568">iʔ</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2286" n="HIAT:w" s="T569">amaʔ</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2289" n="HIAT:w" s="T570">măna</ts>
                  <nts id="Seg_2290" n="HIAT:ip">.</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2293" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2295" n="HIAT:w" s="T571">Dĭn</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2298" n="HIAT:w" s="T572">ăxotnikʔi</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2301" n="HIAT:w" s="T573">šonəgaʔi</ts>
                  <nts id="Seg_2302" n="HIAT:ip">,</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2305" n="HIAT:w" s="T574">kutləʔjə</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2308" n="HIAT:w" s="T575">tănan</ts>
                  <nts id="Seg_2309" n="HIAT:ip">.</nts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2312" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2314" n="HIAT:w" s="T576">Dĭ</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2317" n="HIAT:w" s="T577">bar</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2320" n="HIAT:w" s="T578">nereʔluʔpi</ts>
                  <nts id="Seg_2321" n="HIAT:ip">.</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2324" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2326" n="HIAT:w" s="T579">Eneʔ</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2329" n="HIAT:w" s="T580">măna</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2332" n="HIAT:w" s="T581">paʔizi</ts>
                  <nts id="Seg_2333" n="HIAT:ip">.</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2336" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2338" n="HIAT:w" s="T582">Da</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2341" n="HIAT:w" s="T583">kanaʔ</ts>
                  <nts id="Seg_2342" n="HIAT:ip">.</nts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2345" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2347" n="HIAT:w" s="T584">Štobɨ</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2350" n="HIAT:w" s="T585">dĭzeŋ</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2353" n="HIAT:w" s="T586">ej</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2356" n="HIAT:w" s="T587">kubiʔi</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2359" n="HIAT:w" s="T588">măna</ts>
                  <nts id="Seg_2360" n="HIAT:ip">.</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2363" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2365" n="HIAT:w" s="T589">A</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2368" n="HIAT:w" s="T590">dĭ</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2371" n="HIAT:w" s="T591">mănlia:</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2374" n="HIAT:w" s="T592">Nada</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2377" n="HIAT:w" s="T593">enzittə</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2380" n="HIAT:w" s="T594">dăk</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2383" n="HIAT:w" s="T595">sarzittə</ts>
                  <nts id="Seg_2384" n="HIAT:ip">.</nts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2387" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2389" n="HIAT:w" s="T596">Dĭgəttə</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2391" n="HIAT:ip">(</nts>
                  <ts e="T598" id="Seg_2393" n="HIAT:w" s="T597">dĭ=</ts>
                  <nts id="Seg_2394" n="HIAT:ip">)</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2397" n="HIAT:w" s="T598">dĭ</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2400" n="HIAT:w" s="T599">măndə:</ts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2403" n="HIAT:w" s="T600">Saraʔ</ts>
                  <nts id="Seg_2404" n="HIAT:ip">.</nts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T604" id="Seg_2407" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2409" n="HIAT:w" s="T601">Dĭ</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2412" n="HIAT:w" s="T602">sarbi</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2415" n="HIAT:w" s="T603">dĭm</ts>
                  <nts id="Seg_2416" n="HIAT:ip">.</nts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2419" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_2421" n="HIAT:w" s="T604">A</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2424" n="HIAT:w" s="T605">tüj</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2427" n="HIAT:w" s="T606">nada</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2429" n="HIAT:ip">(</nts>
                  <ts e="T608" id="Seg_2431" n="HIAT:w" s="T607">balt-</ts>
                  <nts id="Seg_2432" n="HIAT:ip">)</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2435" n="HIAT:w" s="T608">baltu</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2438" n="HIAT:w" s="T609">jaʔsittə</ts>
                  <nts id="Seg_2439" n="HIAT:ip">.</nts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2442" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2444" n="HIAT:w" s="T610">I</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2447" n="HIAT:w" s="T611">dĭgəttə</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2450" n="HIAT:w" s="T612">dĭ</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2453" n="HIAT:w" s="T613">urgaːbam</ts>
                  <nts id="Seg_2454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2456" n="HIAT:w" s="T614">dĭ</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2459" n="HIAT:w" s="T615">baltuzi</ts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2462" n="HIAT:w" s="T616">jaʔluʔpi</ts>
                  <nts id="Seg_2463" n="HIAT:ip">,</nts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2466" n="HIAT:w" s="T617">i</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2469" n="HIAT:w" s="T618">urgaːba</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2472" n="HIAT:w" s="T619">külambi</ts>
                  <nts id="Seg_2473" n="HIAT:ip">.</nts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2476" n="HIAT:u" s="T620">
                  <nts id="Seg_2477" n="HIAT:ip">(</nts>
                  <nts id="Seg_2478" n="HIAT:ip">(</nts>
                  <ats e="T621" id="Seg_2479" n="HIAT:non-pho" s="T620">BRK</ats>
                  <nts id="Seg_2480" n="HIAT:ip">)</nts>
                  <nts id="Seg_2481" n="HIAT:ip">)</nts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_2485" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2487" n="HIAT:w" s="T621">Urgaːba</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2490" n="HIAT:w" s="T622">tibizi</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2493" n="HIAT:w" s="T623">bar</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2496" n="HIAT:w" s="T624">tugan</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2498" n="HIAT:ip">(</nts>
                  <ts e="T626" id="Seg_2500" n="HIAT:w" s="T625">molab-</ts>
                  <nts id="Seg_2501" n="HIAT:ip">)</nts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2504" n="HIAT:w" s="T626">molaʔpiʔi</ts>
                  <nts id="Seg_2505" n="HIAT:ip">.</nts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2508" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_2510" n="HIAT:w" s="T627">Davaj</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2513" n="HIAT:w" s="T628">kuʔsittə</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2516" n="HIAT:w" s="T629">репа</ts>
                  <nts id="Seg_2517" n="HIAT:ip">.</nts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T635" id="Seg_2520" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2522" n="HIAT:w" s="T630">Măna</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2525" n="HIAT:w" s="T631">kăreškiʔi</ts>
                  <nts id="Seg_2526" n="HIAT:ip">,</nts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2529" n="HIAT:w" s="T632">a</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2532" n="HIAT:w" s="T633">tănan</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2535" n="HIAT:w" s="T634">verškiʔi</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2539" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2541" n="HIAT:w" s="T635">Dĭgəttə</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2544" n="HIAT:w" s="T636">kuʔpiʔi</ts>
                  <nts id="Seg_2545" n="HIAT:ip">.</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2548" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2550" n="HIAT:w" s="T637">Dĭ</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2553" n="HIAT:w" s="T638">özerbi</ts>
                  <nts id="Seg_2554" n="HIAT:ip">.</nts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2557" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2559" n="HIAT:w" s="T639">Tibi</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2562" n="HIAT:w" s="T640">ibi</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2565" n="HIAT:w" s="T641">kăreškiʔi</ts>
                  <nts id="Seg_2566" n="HIAT:ip">,</nts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2569" n="HIAT:w" s="T642">a</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2572" n="HIAT:w" s="T643">urgaːba</ts>
                  <nts id="Seg_2573" n="HIAT:ip">—</nts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2576" n="HIAT:w" s="T644">verškiʔi</ts>
                  <nts id="Seg_2577" n="HIAT:ip">.</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2580" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2582" n="HIAT:w" s="T645">Dĭgəttə</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2585" n="HIAT:w" s="T646">urgaːba:</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2588" n="HIAT:w" s="T647">No</ts>
                  <nts id="Seg_2589" n="HIAT:ip">,</nts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2592" n="HIAT:w" s="T648">kuza</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2595" n="HIAT:w" s="T649">măna</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2598" n="HIAT:w" s="T650">mʼegluʔpi</ts>
                  <nts id="Seg_2599" n="HIAT:ip">.</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2602" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2604" n="HIAT:w" s="T651">Dĭgəttə</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2607" n="HIAT:w" s="T652">bazo</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2610" n="HIAT:w" s="T653">ejü</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2613" n="HIAT:w" s="T654">molambi</ts>
                  <nts id="Seg_2614" n="HIAT:ip">.</nts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_2617" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2619" n="HIAT:w" s="T655">Kuza</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2622" n="HIAT:w" s="T656">măndə</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2624" n="HIAT:ip">(</nts>
                  <ts e="T657.tx-PKZ.1" id="Seg_2626" n="HIAT:w" s="T657">urgaːbanə</ts>
                  <nts id="Seg_2627" n="HIAT:ip">)</nts>
                  <ts e="T658" id="Seg_2629" n="HIAT:w" s="T657.tx-PKZ.1">:</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2632" n="HIAT:w" s="T658">Davaj</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2635" n="HIAT:w" s="T659">bazo</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2638" n="HIAT:w" s="T660">kuʔsittə</ts>
                  <nts id="Seg_2639" n="HIAT:ip">.</nts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2642" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_2644" n="HIAT:w" s="T661">No</ts>
                  <nts id="Seg_2645" n="HIAT:ip">,</nts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2648" n="HIAT:w" s="T662">urgaːba:</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2650" n="HIAT:ip">(</nts>
                  <ts e="T664" id="Seg_2652" n="HIAT:w" s="T663">Măna=</ts>
                  <nts id="Seg_2653" n="HIAT:ip">)</nts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2656" n="HIAT:w" s="T664">Măna</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2659" n="HIAT:w" s="T665">deʔ</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2662" n="HIAT:w" s="T666">kăreškiʔi</ts>
                  <nts id="Seg_2663" n="HIAT:ip">,</nts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2666" n="HIAT:w" s="T667">a</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2669" n="HIAT:w" s="T668">tănan</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2672" n="HIAT:w" s="T669">verškiʔi</ts>
                  <nts id="Seg_2673" n="HIAT:ip">.</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2676" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2678" n="HIAT:w" s="T670">Dĭgəttə</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2681" n="HIAT:w" s="T671">budəj</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2684" n="HIAT:w" s="T672">ipek</ts>
                  <nts id="Seg_2685" n="HIAT:ip">…</nts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2688" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2690" n="HIAT:w" s="T674">Budəj</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2693" n="HIAT:w" s="T675">ipek</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2696" n="HIAT:w" s="T676">kuʔpiʔi</ts>
                  <nts id="Seg_2697" n="HIAT:ip">.</nts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2700" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2702" n="HIAT:w" s="T677">Dĭ</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2705" n="HIAT:w" s="T678">özerbi</ts>
                  <nts id="Seg_2706" n="HIAT:ip">,</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2709" n="HIAT:w" s="T679">jakše</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2712" n="HIAT:w" s="T680">ugandə</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2715" n="HIAT:w" s="T681">ibi</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2719" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2721" n="HIAT:w" s="T682">Dĭ</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2724" n="HIAT:w" s="T683">kuza</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2727" n="HIAT:w" s="T684">ibi</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2730" n="HIAT:w" s="T685">verškiʔi</ts>
                  <nts id="Seg_2731" n="HIAT:ip">,</nts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2734" n="HIAT:w" s="T686">a</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2737" n="HIAT:w" s="T687">urgaːbanə</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2740" n="HIAT:w" s="T688">kăreškiʔi</ts>
                  <nts id="Seg_2741" n="HIAT:ip">.</nts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2744" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_2746" n="HIAT:w" s="T689">I</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2749" n="HIAT:w" s="T690">dĭgəttə</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2751" n="HIAT:ip">(</nts>
                  <ts e="T692" id="Seg_2753" n="HIAT:w" s="T691">dĭ-</ts>
                  <nts id="Seg_2754" n="HIAT:ip">)</nts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2757" n="HIAT:w" s="T692">dĭzen</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2760" n="HIAT:w" s="T693">bar</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2763" n="HIAT:w" s="T694">družbat</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2766" n="HIAT:w" s="T695">külambi</ts>
                  <nts id="Seg_2767" n="HIAT:ip">.</nts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2770" n="HIAT:u" s="T696">
                  <nts id="Seg_2771" n="HIAT:ip">(</nts>
                  <nts id="Seg_2772" n="HIAT:ip">(</nts>
                  <ats e="T697" id="Seg_2773" n="HIAT:non-pho" s="T696">BRK</ats>
                  <nts id="Seg_2774" n="HIAT:ip">)</nts>
                  <nts id="Seg_2775" n="HIAT:ip">)</nts>
                  <nts id="Seg_2776" n="HIAT:ip">.</nts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2779" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2781" n="HIAT:w" s="T697">Amnolaʔpi</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2784" n="HIAT:w" s="T698">büzʼe</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2787" n="HIAT:w" s="T699">da</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2790" n="HIAT:w" s="T700">nüke</ts>
                  <nts id="Seg_2791" n="HIAT:ip">.</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_2794" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2796" n="HIAT:w" s="T701">Dĭzeŋgən</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2799" n="HIAT:w" s="T702">esseŋdə</ts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2802" n="HIAT:w" s="T703">ej</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2805" n="HIAT:w" s="T704">ibiʔi</ts>
                  <nts id="Seg_2806" n="HIAT:ip">.</nts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_2809" n="HIAT:u" s="T705">
                  <nts id="Seg_2810" n="HIAT:ip">(</nts>
                  <ts e="T706" id="Seg_2812" n="HIAT:w" s="T705">Onʼiʔ</ts>
                  <nts id="Seg_2813" n="HIAT:ip">)</nts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2816" n="HIAT:w" s="T706">sĭre</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2819" n="HIAT:w" s="T707">bar</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2822" n="HIAT:w" s="T708">saʔməluʔpi</ts>
                  <nts id="Seg_2823" n="HIAT:ip">,</nts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2826" n="HIAT:w" s="T709">ugandə</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2829" n="HIAT:w" s="T710">urgo</ts>
                  <nts id="Seg_2830" n="HIAT:ip">.</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2833" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_2835" n="HIAT:w" s="T711">Esseŋ</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2838" n="HIAT:w" s="T712">bar</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2841" n="HIAT:w" s="T713">girgit</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2844" n="HIAT:w" s="T714">bar</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2846" n="HIAT:ip">(</nts>
                  <ts e="T716" id="Seg_2848" n="HIAT:w" s="T715">кат</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2851" n="HIAT:w" s="T716">-</ts>
                  <nts id="Seg_2852" n="HIAT:ip">)</nts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2855" n="HIAT:w" s="T717">катаются</ts>
                  <nts id="Seg_2856" n="HIAT:ip">,</nts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2859" n="HIAT:w" s="T718">girgit</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2862" n="HIAT:w" s="T719">bar</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2865" n="HIAT:w" s="T720">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_2866" n="HIAT:ip">.</nts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2869" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2871" n="HIAT:w" s="T721">Snegurkaʔi</ts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2874" n="HIAT:w" s="T722">alaʔbəʔjə</ts>
                  <nts id="Seg_2875" n="HIAT:ip">,</nts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2878" n="HIAT:w" s="T723">bar</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2881" n="HIAT:w" s="T724">alaʔbəʔjə</ts>
                  <nts id="Seg_2882" n="HIAT:ip">.</nts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2885" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2887" n="HIAT:w" s="T725">Dĭgəttə</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2890" n="HIAT:w" s="T726">abiʔi</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2893" n="HIAT:w" s="T727">urgo</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2896" n="HIAT:w" s="T728">nüke</ts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_2900" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2902" n="HIAT:w" s="T729">A</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2905" n="HIAT:w" s="T730">büzʼe</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2908" n="HIAT:w" s="T731">üge</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2911" n="HIAT:w" s="T732">măndobi</ts>
                  <nts id="Seg_2912" n="HIAT:ip">,</nts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2915" n="HIAT:w" s="T733">măndobi:</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2918" n="HIAT:w" s="T734">Kanžəbəj</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2920" n="HIAT:ip">(</nts>
                  <ts e="T736" id="Seg_2922" n="HIAT:w" s="T735">san-</ts>
                  <nts id="Seg_2923" n="HIAT:ip">)</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2926" n="HIAT:w" s="T736">tănzi</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2929" n="HIAT:w" s="T737">nʼiʔtə</ts>
                  <nts id="Seg_2930" n="HIAT:ip">,</nts>
                  <nts id="Seg_2931" n="HIAT:ip">—</nts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2934" n="HIAT:w" s="T738">nükenə</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2937" n="HIAT:w" s="T739">măllia</ts>
                  <nts id="Seg_2938" n="HIAT:ip">.</nts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2941" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_2943" n="HIAT:w" s="T740">No</ts>
                  <nts id="Seg_2944" n="HIAT:ip">,</nts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2947" n="HIAT:w" s="T741">kanžəbəj</ts>
                  <nts id="Seg_2948" n="HIAT:ip">.</nts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_2951" n="HIAT:u" s="T742">
                  <nts id="Seg_2952" n="HIAT:ip">(</nts>
                  <ts e="T743" id="Seg_2954" n="HIAT:w" s="T742">Ka-</ts>
                  <nts id="Seg_2955" n="HIAT:ip">)</nts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2958" n="HIAT:w" s="T743">Kambiʔi</ts>
                  <nts id="Seg_2959" n="HIAT:ip">,</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2962" n="HIAT:w" s="T744">măndobiʔi</ts>
                  <nts id="Seg_2963" n="HIAT:ip">,</nts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2966" n="HIAT:w" s="T745">dĭgəttə</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2969" n="HIAT:w" s="T746">davaj</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2972" n="HIAT:w" s="T747">azittə</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2975" n="HIAT:w" s="T748">koʔbdo</ts>
                  <nts id="Seg_2976" n="HIAT:ip">.</nts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_2979" n="HIAT:u" s="T749">
                  <ts e="T751" id="Seg_2981" n="HIAT:w" s="T749">Abiʔi</ts>
                  <nts id="Seg_2982" n="HIAT:ip">,</nts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2985" n="HIAT:w" s="T751">ulu</ts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2988" n="HIAT:w" s="T752">abiʔi</ts>
                  <nts id="Seg_2989" n="HIAT:ip">,</nts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2992" n="HIAT:w" s="T753">simaʔi</ts>
                  <nts id="Seg_2993" n="HIAT:ip">,</nts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2996" n="HIAT:w" s="T754">püjet</ts>
                  <nts id="Seg_2997" n="HIAT:ip">,</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3000" n="HIAT:w" s="T755">aŋdə</ts>
                  <nts id="Seg_3001" n="HIAT:ip">,</nts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3004" n="HIAT:w" s="T756">simat</ts>
                  <nts id="Seg_3005" n="HIAT:ip">.</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T763" id="Seg_3008" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_3010" n="HIAT:w" s="T757">Dĭgəttə</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3013" n="HIAT:w" s="T758">kössi</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3016" n="HIAT:w" s="T759">bar</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3019" n="HIAT:w" s="T760">siman</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3022" n="HIAT:w" s="T761">toʔndə</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3025" n="HIAT:w" s="T762">dʼüʔpiʔi</ts>
                  <nts id="Seg_3026" n="HIAT:ip">.</nts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3029" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_3031" n="HIAT:w" s="T763">Dĭgəttə</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3034" n="HIAT:w" s="T764">dĭ</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3037" n="HIAT:w" s="T765">simatsi</ts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3040" n="HIAT:w" s="T766">mʼeŋgəlluʔpi</ts>
                  <nts id="Seg_3041" n="HIAT:ip">.</nts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_3044" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3046" n="HIAT:w" s="T767">Dĭgəttə</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3049" n="HIAT:w" s="T768">büzʼe</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3052" n="HIAT:w" s="T769">kandlaʔbə</ts>
                  <nts id="Seg_3053" n="HIAT:ip">.</nts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3056" n="HIAT:u" s="T770">
                  <nts id="Seg_3057" n="HIAT:ip">(</nts>
                  <ts e="T771" id="Seg_3059" n="HIAT:w" s="T770">Kam-</ts>
                  <nts id="Seg_3060" n="HIAT:ip">)</nts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3063" n="HIAT:w" s="T771">Kambi</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3066" n="HIAT:w" s="T772">turanə</ts>
                  <nts id="Seg_3067" n="HIAT:ip">,</nts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3070" n="HIAT:w" s="T773">a</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3072" n="HIAT:ip">(</nts>
                  <ts e="T775" id="Seg_3074" n="HIAT:w" s="T774">dĭ=</ts>
                  <nts id="Seg_3075" n="HIAT:ip">)</nts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3078" n="HIAT:w" s="T775">dĭzeŋ</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3080" n="HIAT:ip">(</nts>
                  <ts e="T777" id="Seg_3082" n="HIAT:w" s="T776">dĭʔ-</ts>
                  <nts id="Seg_3083" n="HIAT:ip">)</nts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3086" n="HIAT:w" s="T777">tože</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3089" n="HIAT:w" s="T778">dĭzi</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3092" n="HIAT:w" s="T779">kambiʔi</ts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3095" n="HIAT:w" s="T780">turanə</ts>
                  <nts id="Seg_3096" n="HIAT:ip">.</nts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_3099" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3101" n="HIAT:w" s="T781">Bar</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3104" n="HIAT:w" s="T782">ugandə</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3107" n="HIAT:w" s="T783">dĭʔnə</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3110" n="HIAT:w" s="T784">jakše</ts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3113" n="HIAT:w" s="T785">molambi</ts>
                  <nts id="Seg_3114" n="HIAT:ip">,</nts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3117" n="HIAT:w" s="T786">ešši</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3119" n="HIAT:ip">(</nts>
                  <ts e="T788" id="Seg_3121" n="HIAT:w" s="T787">ge</ts>
                  <nts id="Seg_3122" n="HIAT:ip">)</nts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3125" n="HIAT:w" s="T788">bar</ts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3128" n="HIAT:w" s="T789">ibiʔi</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3131" n="HIAT:w" s="T790">dĭʔnə</ts>
                  <nts id="Seg_3132" n="HIAT:ip">.</nts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3135" n="HIAT:u" s="T791">
                  <nts id="Seg_3136" n="HIAT:ip">(</nts>
                  <nts id="Seg_3137" n="HIAT:ip">(</nts>
                  <ats e="T791.tx-PKZ.1" id="Seg_3138" n="HIAT:non-pho" s="T791">NOISE</ats>
                  <nts id="Seg_3139" n="HIAT:ip">)</nts>
                  <nts id="Seg_3140" n="HIAT:ip">)</nts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3142" n="HIAT:ip">(</nts>
                  <nts id="Seg_3143" n="HIAT:ip">(</nts>
                  <ats e="T792" id="Seg_3144" n="HIAT:non-pho" s="T791.tx-PKZ.1">BRK</ats>
                  <nts id="Seg_3145" n="HIAT:ip">)</nts>
                  <nts id="Seg_3146" n="HIAT:ip">)</nts>
                  <nts id="Seg_3147" n="HIAT:ip">.</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T798" id="Seg_3150" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3152" n="HIAT:w" s="T792">Dĭgəttə</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3155" n="HIAT:w" s="T793">dĭʔnə</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3158" n="HIAT:w" s="T794">jamaʔi</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3161" n="HIAT:w" s="T795">ibiʔi</ts>
                  <nts id="Seg_3162" n="HIAT:ip">,</nts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3165" n="HIAT:w" s="T796">kuvazəʔi</ts>
                  <nts id="Seg_3166" n="HIAT:ip">,</nts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3169" n="HIAT:w" s="T797">köməʔi</ts>
                  <nts id="Seg_3170" n="HIAT:ip">.</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_3173" n="HIAT:u" s="T798">
                  <ts e="T799" id="Seg_3175" n="HIAT:w" s="T798">Dĭgəttə</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3178" n="HIAT:w" s="T799">eʔbdənə</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3181" n="HIAT:w" s="T800">ibiʔi</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3184" n="HIAT:w" s="T801">лента</ts>
                  <nts id="Seg_3185" n="HIAT:ip">,</nts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3188" n="HIAT:w" s="T802">ugandə</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3191" n="HIAT:w" s="T803">kuvas</ts>
                  <nts id="Seg_3192" n="HIAT:ip">,</nts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3195" n="HIAT:w" s="T804">атласный</ts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_3199" n="HIAT:u" s="T805">
                  <nts id="Seg_3200" n="HIAT:ip">(</nts>
                  <ts e="T806" id="Seg_3202" n="HIAT:w" s="T805">Gəttə</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3205" n="HIAT:w" s="T806">dĭ=</ts>
                  <nts id="Seg_3206" n="HIAT:ip">)</nts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3209" n="HIAT:w" s="T807">Dĭgəttə</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3212" n="HIAT:w" s="T808">ejü</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3215" n="HIAT:w" s="T809">molambi</ts>
                  <nts id="Seg_3216" n="HIAT:ip">.</nts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3219" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_3221" n="HIAT:w" s="T810">A</ts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3224" n="HIAT:w" s="T811">dĭ</ts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3227" n="HIAT:w" s="T812">koʔbdo</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3230" n="HIAT:w" s="T813">amnolaʔbə</ts>
                  <nts id="Seg_3231" n="HIAT:ip">.</nts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3234" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_3236" n="HIAT:w" s="T814">Bü</ts>
                  <nts id="Seg_3237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3239" n="HIAT:w" s="T815">mʼaŋnaʔbə</ts>
                  <nts id="Seg_3240" n="HIAT:ip">.</nts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3243" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3245" n="HIAT:w" s="T816">Dĭ</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3248" n="HIAT:w" s="T817">bar</ts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3251" n="HIAT:w" s="T818">pimnie</ts>
                  <nts id="Seg_3252" n="HIAT:ip">.</nts>
                  <nts id="Seg_3253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3255" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3257" n="HIAT:w" s="T819">A</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3259" n="HIAT:ip">(</nts>
                  <ts e="T821" id="Seg_3261" n="HIAT:w" s="T820">nüket</ts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3264" n="HIAT:w" s="T821">măn-</ts>
                  <nts id="Seg_3265" n="HIAT:ip">)</nts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3268" n="HIAT:w" s="T822">nüke</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3271" n="HIAT:w" s="T823">măndə:</ts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3274" n="HIAT:w" s="T824">Ĭmbi</ts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3277" n="HIAT:w" s="T825">tăn</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3280" n="HIAT:w" s="T826">dʼorlaʔbəl</ts>
                  <nts id="Seg_3281" n="HIAT:ip">?</nts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3284" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_3286" n="HIAT:w" s="T827">A</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3289" n="HIAT:w" s="T828">bostən</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3292" n="HIAT:w" s="T829">bar</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3294" n="HIAT:ip">(</nts>
                  <ts e="T831" id="Seg_3296" n="HIAT:w" s="T830">kă-</ts>
                  <nts id="Seg_3297" n="HIAT:ip">)</nts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3300" n="HIAT:w" s="T831">kăjəldə</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3303" n="HIAT:w" s="T832">mʼaŋnaʔbəʔjə</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3306" n="HIAT:w" s="T833">kadəlgən</ts>
                  <nts id="Seg_3307" n="HIAT:ip">.</nts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3310" n="HIAT:u" s="T834">
                  <nts id="Seg_3311" n="HIAT:ip">(</nts>
                  <ts e="T835" id="Seg_3313" n="HIAT:w" s="T834">Ta=</ts>
                  <nts id="Seg_3314" n="HIAT:ip">)</nts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3317" n="HIAT:w" s="T835">Dĭgəttə</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3319" n="HIAT:ip">(</nts>
                  <ts e="T837" id="Seg_3321" n="HIAT:w" s="T836">ej-</ts>
                  <nts id="Seg_3322" n="HIAT:ip">)</nts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3325" n="HIAT:w" s="T837">bazo</ts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3328" n="HIAT:w" s="T838">kuja</ts>
                  <nts id="Seg_3329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3331" n="HIAT:w" s="T839">molaʔpi</ts>
                  <nts id="Seg_3332" n="HIAT:ip">.</nts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T844" id="Seg_3335" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3337" n="HIAT:w" s="T840">Всякий</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3340" n="HIAT:w" s="T841">noʔ</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3343" n="HIAT:w" s="T842">özerleʔbəʔjə</ts>
                  <nts id="Seg_3344" n="HIAT:ip">,</nts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3347" n="HIAT:w" s="T843">svʼetogəʔjə</ts>
                  <nts id="Seg_3348" n="HIAT:ip">.</nts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3351" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_3353" n="HIAT:w" s="T844">Dĭgəttə</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3356" n="HIAT:w" s="T845">šobiʔi</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3359" n="HIAT:w" s="T846">koʔbsaŋ:</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3362" n="HIAT:w" s="T847">Kanžəbəj</ts>
                  <nts id="Seg_3363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3365" n="HIAT:w" s="T848">miʔsi</ts>
                  <nts id="Seg_3366" n="HIAT:ip">!</nts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T851" id="Seg_3369" n="HIAT:u" s="T849">
                  <ts e="T850" id="Seg_3371" n="HIAT:w" s="T849">Kăštliaʔi</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3374" n="HIAT:w" s="T850">koʔbdot</ts>
                  <nts id="Seg_3375" n="HIAT:ip">.</nts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T858" id="Seg_3378" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_3380" n="HIAT:w" s="T851">Nüke</ts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3382" n="HIAT:ip">(</nts>
                  <ts e="T853" id="Seg_3384" n="HIAT:w" s="T852">măn-</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3387" n="HIAT:w" s="T853">măn-</ts>
                  <nts id="Seg_3388" n="HIAT:ip">)</nts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3391" n="HIAT:w" s="T854">măllia:</ts>
                  <nts id="Seg_3392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3394" n="HIAT:w" s="T855">Kanaʔ</ts>
                  <nts id="Seg_3395" n="HIAT:ip">,</nts>
                  <nts id="Seg_3396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3398" n="HIAT:w" s="T856">ĭmbi</ts>
                  <nts id="Seg_3399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3401" n="HIAT:w" s="T857">amnolaʔbəl</ts>
                  <nts id="Seg_3402" n="HIAT:ip">?</nts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_3405" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_3407" n="HIAT:w" s="T858">Dʼok</ts>
                  <nts id="Seg_3408" n="HIAT:ip">,</nts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3411" n="HIAT:w" s="T859">măn</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3414" n="HIAT:w" s="T860">em</ts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3417" n="HIAT:w" s="T861">kanaʔ</ts>
                  <nts id="Seg_3418" n="HIAT:ip">,</nts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3420" n="HIAT:ip">(</nts>
                  <ts e="T863" id="Seg_3422" n="HIAT:w" s="T862">kuja=</ts>
                  <nts id="Seg_3423" n="HIAT:ip">)</nts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3426" n="HIAT:w" s="T863">ulum</ts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3429" n="HIAT:w" s="T864">bar</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3432" n="HIAT:w" s="T865">ĭzemnuʔləj</ts>
                  <nts id="Seg_3433" n="HIAT:ip">.</nts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3436" n="HIAT:u" s="T866">
                  <nts id="Seg_3437" n="HIAT:ip">(</nts>
                  <ts e="T867" id="Seg_3439" n="HIAT:w" s="T866">Kuj-</ts>
                  <nts id="Seg_3440" n="HIAT:ip">)</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3443" n="HIAT:w" s="T867">Kuja</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3446" n="HIAT:w" s="T868">nendləj</ts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3449" n="HIAT:w" s="T869">ulum</ts>
                  <nts id="Seg_3450" n="HIAT:ip">.</nts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T874" id="Seg_3453" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3455" n="HIAT:w" s="T870">No</ts>
                  <nts id="Seg_3456" n="HIAT:ip">,</nts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3459" n="HIAT:w" s="T871">plat</ts>
                  <nts id="Seg_3460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3461" n="HIAT:ip">(</nts>
                  <ts e="T873" id="Seg_3463" n="HIAT:w" s="T872">ser-</ts>
                  <nts id="Seg_3464" n="HIAT:ip">)</nts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3467" n="HIAT:w" s="T873">šerdə</ts>
                  <nts id="Seg_3468" n="HIAT:ip">.</nts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T877" id="Seg_3471" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3473" n="HIAT:w" s="T874">Dĭgəttə</ts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3476" n="HIAT:w" s="T875">dĭ</ts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3479" n="HIAT:w" s="T876">kambi</ts>
                  <nts id="Seg_3480" n="HIAT:ip">.</nts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3483" n="HIAT:u" s="T877">
                  <ts e="T878" id="Seg_3485" n="HIAT:w" s="T877">Bar</ts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3488" n="HIAT:w" s="T878">koʔbsaŋ</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3491" n="HIAT:w" s="T879">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_3492" n="HIAT:ip">.</nts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_3495" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_3497" n="HIAT:w" s="T880">Svʼetogəʔi</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3499" n="HIAT:ip">(</nts>
                  <ts e="T882" id="Seg_3501" n="HIAT:w" s="T881">nʼi-</ts>
                  <nts id="Seg_3502" n="HIAT:ip">)</nts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3505" n="HIAT:w" s="T882">nĭŋgəleʔbəʔjə</ts>
                  <nts id="Seg_3506" n="HIAT:ip">.</nts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T887" id="Seg_3509" n="HIAT:u" s="T883">
                  <ts e="T884" id="Seg_3511" n="HIAT:w" s="T883">A</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3514" n="HIAT:w" s="T884">dĭ</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3516" n="HIAT:ip">(</nts>
                  <ts e="T886" id="Seg_3518" n="HIAT:w" s="T885">amnolaʔbə</ts>
                  <nts id="Seg_3519" n="HIAT:ip">)</nts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3521" n="HIAT:ip">(</nts>
                  <nts id="Seg_3522" n="HIAT:ip">(</nts>
                  <ats e="T887" id="Seg_3523" n="HIAT:non-pho" s="T886">DMG</ats>
                  <nts id="Seg_3524" n="HIAT:ip">)</nts>
                  <nts id="Seg_3525" n="HIAT:ip">)</nts>
                  <nts id="Seg_3526" n="HIAT:ip">.</nts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T889" id="Seg_3529" n="HIAT:u" s="T887">
                  <nts id="Seg_3530" n="HIAT:ip">(</nts>
                  <ts e="T889" id="Seg_3532" n="HIAT:w" s="T887">-ambi</ts>
                  <nts id="Seg_3533" n="HIAT:ip">)</nts>
                  <nts id="Seg_3534" n="HIAT:ip">.</nts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T899" id="Seg_3537" n="HIAT:u" s="T889">
                  <ts e="T890" id="Seg_3539" n="HIAT:w" s="T889">Dĭgəttə</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3542" n="HIAT:w" s="T890">dĭzeŋ</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3545" n="HIAT:w" s="T891">embiʔi</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3548" n="HIAT:w" s="T892">šü</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3551" n="HIAT:w" s="T893">ugandə</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3554" n="HIAT:w" s="T894">urgo</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3557" n="HIAT:w" s="T895">i</ts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3560" n="HIAT:w" s="T896">davaj</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3563" n="HIAT:w" s="T897">nuʔməsittə</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3566" n="HIAT:w" s="T898">šünə</ts>
                  <nts id="Seg_3567" n="HIAT:ip">.</nts>
                  <nts id="Seg_3568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_3570" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3572" n="HIAT:w" s="T899">Mănliaʔi:</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3575" n="HIAT:w" s="T900">Ĭmbi</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3578" n="HIAT:w" s="T901">tăn</ts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3581" n="HIAT:w" s="T902">ej</ts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3584" n="HIAT:w" s="T903">suʔmiliel</ts>
                  <nts id="Seg_3585" n="HIAT:ip">?</nts>
                  <nts id="Seg_3586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T907" id="Seg_3588" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3590" n="HIAT:w" s="T904">A</ts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3593" n="HIAT:w" s="T905">dĭ</ts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3596" n="HIAT:w" s="T906">pimnie</ts>
                  <nts id="Seg_3597" n="HIAT:ip">.</nts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3600" n="HIAT:u" s="T907">
                  <ts e="T908" id="Seg_3602" n="HIAT:w" s="T907">A</ts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3605" n="HIAT:w" s="T908">dĭgəttə</ts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3608" n="HIAT:w" s="T909">kak</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3611" n="HIAT:w" s="T910">dĭm</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3614" n="HIAT:w" s="T911">pĭštʼerleʔbəʔjə</ts>
                  <nts id="Seg_3615" n="HIAT:ip">.</nts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3618" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3620" n="HIAT:w" s="T912">A</ts>
                  <nts id="Seg_3621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3623" n="HIAT:w" s="T913">dĭ</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3626" n="HIAT:w" s="T914">uʔbdəbi</ts>
                  <nts id="Seg_3627" n="HIAT:ip">,</nts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3629" n="HIAT:ip">(</nts>
                  <ts e="T916" id="Seg_3631" n="HIAT:w" s="T915">sa-</ts>
                  <nts id="Seg_3632" n="HIAT:ip">)</nts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3635" n="HIAT:w" s="T916">suʔmiluʔpi</ts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3638" n="HIAT:w" s="T917">i</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3641" n="HIAT:w" s="T918">bar</ts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3644" n="HIAT:w" s="T919">šüʔən</ts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_3647" n="HIAT:w" s="T920">kalla</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3650" n="HIAT:w" s="T1121">dʼürbi</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3653" n="HIAT:w" s="T921">nʼuʔdə</ts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3656" n="HIAT:w" s="T922">bar</ts>
                  <nts id="Seg_3657" n="HIAT:ip">.</nts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T924" id="Seg_3660" n="HIAT:u" s="T923">
                  <nts id="Seg_3661" n="HIAT:ip">(</nts>
                  <nts id="Seg_3662" n="HIAT:ip">(</nts>
                  <ats e="T924" id="Seg_3663" n="HIAT:non-pho" s="T923">BRK</ats>
                  <nts id="Seg_3664" n="HIAT:ip">)</nts>
                  <nts id="Seg_3665" n="HIAT:ip">)</nts>
                  <nts id="Seg_3666" n="HIAT:ip">.</nts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T925" id="Seg_3669" n="HIAT:u" s="T924">
                  <nts id="Seg_3670" n="HIAT:ip">(</nts>
                  <nts id="Seg_3671" n="HIAT:ip">(</nts>
                  <ats e="T925" id="Seg_3672" n="HIAT:non-pho" s="T924">BRK</ats>
                  <nts id="Seg_3673" n="HIAT:ip">)</nts>
                  <nts id="Seg_3674" n="HIAT:ip">)</nts>
                  <nts id="Seg_3675" n="HIAT:ip">.</nts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_3678" n="HIAT:u" s="T925">
                  <ts e="T926" id="Seg_3680" n="HIAT:w" s="T925">Raz</ts>
                  <nts id="Seg_3681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3683" n="HIAT:w" s="T926">buzom</ts>
                  <nts id="Seg_3684" n="HIAT:ip">,</nts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3686" n="HIAT:ip">(</nts>
                  <ts e="T928" id="Seg_3688" n="HIAT:w" s="T927">măn</ts>
                  <nts id="Seg_3689" n="HIAT:ip">)</nts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3692" n="HIAT:w" s="T928">ugandə</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3695" n="HIAT:w" s="T929">kuvas</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3697" n="HIAT:ip">(</nts>
                  <ts e="T931" id="Seg_3699" n="HIAT:w" s="T930">sĭre</ts>
                  <nts id="Seg_3700" n="HIAT:ip">)</nts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3703" n="HIAT:w" s="T931">buzo</ts>
                  <nts id="Seg_3704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3706" n="HIAT:w" s="T932">ibi</ts>
                  <nts id="Seg_3707" n="HIAT:ip">.</nts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_3710" n="HIAT:u" s="T933">
                  <nts id="Seg_3711" n="HIAT:ip">(</nts>
                  <ts e="T934" id="Seg_3713" n="HIAT:w" s="T933">Dĭgəttə</ts>
                  <nts id="Seg_3714" n="HIAT:ip">)</nts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3717" n="HIAT:w" s="T934">mĭmbi</ts>
                  <nts id="Seg_3718" n="HIAT:ip">,</nts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3721" n="HIAT:w" s="T935">mĭmbi</ts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3723" n="HIAT:ip">(</nts>
                  <ts e="T937" id="Seg_3725" n="HIAT:w" s="T936">măn</ts>
                  <nts id="Seg_3726" n="HIAT:ip">)</nts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3729" n="HIAT:w" s="T937">nüdʼin</ts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3731" n="HIAT:ip">(</nts>
                  <ts e="T939" id="Seg_3733" n="HIAT:w" s="T938">bĭtəlzittə</ts>
                  <nts id="Seg_3734" n="HIAT:ip">)</nts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3736" n="HIAT:ip">(</nts>
                  <ts e="T940" id="Seg_3738" n="HIAT:w" s="T939">sut-</ts>
                  <nts id="Seg_3739" n="HIAT:ip">)</nts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3742" n="HIAT:w" s="T940">sütsi</ts>
                  <nts id="Seg_3743" n="HIAT:ip">.</nts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T947" id="Seg_3746" n="HIAT:u" s="T941">
                  <ts e="T942" id="Seg_3748" n="HIAT:w" s="T941">Kirgariam</ts>
                  <nts id="Seg_3749" n="HIAT:ip">,</nts>
                  <nts id="Seg_3750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3752" n="HIAT:w" s="T942">kirgariam</ts>
                  <nts id="Seg_3753" n="HIAT:ip">,</nts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3756" n="HIAT:w" s="T943">măndərbiam</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3759" n="HIAT:w" s="T944">măndərbiam</ts>
                  <nts id="Seg_3760" n="HIAT:ip">,</nts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3763" n="HIAT:w" s="T945">gijendə</ts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3766" n="HIAT:w" s="T946">naga</ts>
                  <nts id="Seg_3767" n="HIAT:ip">.</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T953" id="Seg_3770" n="HIAT:u" s="T947">
                  <ts e="T948" id="Seg_3772" n="HIAT:w" s="T947">No</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3775" n="HIAT:w" s="T948">giraːmbi</ts>
                  <nts id="Seg_3776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3777" n="HIAT:ip">(</nts>
                  <ts e="T950" id="Seg_3779" n="HIAT:w" s="T949">bu-</ts>
                  <nts id="Seg_3780" n="HIAT:ip">)</nts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3783" n="HIAT:w" s="T950">buzo</ts>
                  <nts id="Seg_3784" n="HIAT:ip">,</nts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3787" n="HIAT:w" s="T951">ej</ts>
                  <nts id="Seg_3788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3790" n="HIAT:w" s="T952">tĭmniem</ts>
                  <nts id="Seg_3791" n="HIAT:ip">.</nts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T957" id="Seg_3794" n="HIAT:u" s="T953">
                  <ts e="T954" id="Seg_3796" n="HIAT:w" s="T953">Ertən</ts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3799" n="HIAT:w" s="T954">uʔbdəbiam</ts>
                  <nts id="Seg_3800" n="HIAT:ip">,</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3803" n="HIAT:w" s="T955">vezʼdʼe</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3806" n="HIAT:w" s="T956">mĭmbiem</ts>
                  <nts id="Seg_3807" n="HIAT:ip">.</nts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T963" id="Seg_3810" n="HIAT:u" s="T957">
                  <ts e="T958" id="Seg_3812" n="HIAT:w" s="T957">Dʼagagən</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3815" n="HIAT:w" s="T958">mĭmbiem</ts>
                  <nts id="Seg_3816" n="HIAT:ip">,</nts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3819" n="HIAT:w" s="T959">tenəbiem</ts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3822" n="HIAT:w" s="T960">dĭ</ts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3825" n="HIAT:w" s="T961">saʔməluʔpi</ts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3828" n="HIAT:w" s="T962">bünə</ts>
                  <nts id="Seg_3829" n="HIAT:ip">.</nts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_3832" n="HIAT:u" s="T963">
                  <ts e="T964" id="Seg_3834" n="HIAT:w" s="T963">Dĭn</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3837" n="HIAT:w" s="T964">naga</ts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3840" n="HIAT:w" s="T965">vezde</ts>
                  <nts id="Seg_3841" n="HIAT:ip">.</nts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T969" id="Seg_3844" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_3846" n="HIAT:w" s="T966">Tenəbiem:</ts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3849" n="HIAT:w" s="T967">volk</ts>
                  <nts id="Seg_3850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3852" n="HIAT:w" s="T968">amnuʔpi</ts>
                  <nts id="Seg_3853" n="HIAT:ip">.</nts>
                  <nts id="Seg_3854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T975" id="Seg_3856" n="HIAT:u" s="T969">
                  <ts e="T970" id="Seg_3858" n="HIAT:w" s="T969">Dʼok</ts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3860" n="HIAT:ip">(</nts>
                  <ts e="T971" id="Seg_3862" n="HIAT:w" s="T970">ej</ts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3865" n="HIAT:w" s="T971">volk</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3868" n="HIAT:w" s="T972">amn-</ts>
                  <nts id="Seg_3869" n="HIAT:ip">)</nts>
                  <nts id="Seg_3870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3872" n="HIAT:w" s="T973">ej</ts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3875" n="HIAT:w" s="T974">amnuʔpi</ts>
                  <nts id="Seg_3876" n="HIAT:ip">.</nts>
                  <nts id="Seg_3877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T980" id="Seg_3879" n="HIAT:u" s="T975">
                  <ts e="T976" id="Seg_3881" n="HIAT:w" s="T975">Dĭgəttə</ts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3884" n="HIAT:w" s="T976">dön</ts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3887" n="HIAT:w" s="T977">il</ts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3890" n="HIAT:w" s="T978">noʔ</ts>
                  <nts id="Seg_3891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3893" n="HIAT:w" s="T979">jaʔpiʔi</ts>
                  <nts id="Seg_3894" n="HIAT:ip">.</nts>
                  <nts id="Seg_3895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T988" id="Seg_3897" n="HIAT:u" s="T980">
                  <ts e="T981" id="Seg_3899" n="HIAT:w" s="T980">Dĭzeŋ</ts>
                  <nts id="Seg_3900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3902" n="HIAT:w" s="T981">наверное</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3905" n="HIAT:w" s="T982">ibiʔi</ts>
                  <nts id="Seg_3906" n="HIAT:ip">,</nts>
                  <nts id="Seg_3907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3909" n="HIAT:w" s="T983">üjüttə</ts>
                  <nts id="Seg_3910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3911" n="HIAT:ip">(</nts>
                  <ts e="T985" id="Seg_3913" n="HIAT:w" s="T984">s-</ts>
                  <nts id="Seg_3914" n="HIAT:ip">)</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3917" n="HIAT:w" s="T985">sarbiʔi</ts>
                  <nts id="Seg_3918" n="HIAT:ip">,</nts>
                  <nts id="Seg_3919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3921" n="HIAT:w" s="T986">kumbiʔi</ts>
                  <nts id="Seg_3922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3924" n="HIAT:w" s="T987">maʔndə</ts>
                  <nts id="Seg_3925" n="HIAT:ip">.</nts>
                  <nts id="Seg_3926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_3928" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_3930" n="HIAT:w" s="T988">Šide</ts>
                  <nts id="Seg_3931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3933" n="HIAT:w" s="T989">üjütsi</ts>
                  <nts id="Seg_3934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3936" n="HIAT:w" s="T990">volk</ts>
                  <nts id="Seg_3937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3939" n="HIAT:w" s="T991">ibi</ts>
                  <nts id="Seg_3940" n="HIAT:ip">.</nts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_3943" n="HIAT:u" s="T992">
                  <ts e="T993" id="Seg_3945" n="HIAT:w" s="T992">Măn</ts>
                  <nts id="Seg_3946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3947" n="HIAT:ip">(</nts>
                  <ts e="T994" id="Seg_3949" n="HIAT:w" s="T993">s-</ts>
                  <nts id="Seg_3950" n="HIAT:ip">)</nts>
                  <nts id="Seg_3951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3953" n="HIAT:w" s="T994">tuganbə</ts>
                  <nts id="Seg_3954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3956" n="HIAT:w" s="T995">šobi</ts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3959" n="HIAT:w" s="T996">Kanoklerdə</ts>
                  <nts id="Seg_3960" n="HIAT:ip">)</nts>
                  <nts id="Seg_3961" n="HIAT:ip">.</nts>
                  <nts id="Seg_3962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1004" id="Seg_3964" n="HIAT:u" s="T997">
                  <ts e="T998" id="Seg_3966" n="HIAT:w" s="T997">I</ts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3969" n="HIAT:w" s="T998">nörbəlie:</ts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3971" n="HIAT:ip">(</nts>
                  <ts e="T1000" id="Seg_3973" n="HIAT:w" s="T999">t-</ts>
                  <nts id="Seg_3974" n="HIAT:ip">)</nts>
                  <nts id="Seg_3975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3977" n="HIAT:w" s="T1000">šide</ts>
                  <nts id="Seg_3978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3980" n="HIAT:w" s="T1001">tüžöj</ts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3983" n="HIAT:w" s="T1002">ibi</ts>
                  <nts id="Seg_3984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3985" n="HIAT:ip">(</nts>
                  <ts e="T1004" id="Seg_3987" n="HIAT:w" s="T1003">ilgən</ts>
                  <nts id="Seg_3988" n="HIAT:ip">)</nts>
                  <nts id="Seg_3989" n="HIAT:ip">.</nts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1012" id="Seg_3992" n="HIAT:u" s="T1004">
                  <nts id="Seg_3993" n="HIAT:ip">(</nts>
                  <ts e="T1005" id="Seg_3995" n="HIAT:w" s="T1004">Ugandə</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3998" n="HIAT:w" s="T1005">il</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_4001" n="HIAT:w" s="T1006">i-</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_4004" n="HIAT:w" s="T1007">il=</ts>
                  <nts id="Seg_4005" n="HIAT:ip">)</nts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_4008" n="HIAT:w" s="T1008">Ugandə</ts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4010" n="HIAT:ip">(</nts>
                  <ts e="T1010" id="Seg_4012" n="HIAT:w" s="T1009">š-</ts>
                  <nts id="Seg_4013" n="HIAT:ip">)</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_4016" n="HIAT:w" s="T1010">sildə</ts>
                  <nts id="Seg_4017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_4019" n="HIAT:w" s="T1011">iʔgö</ts>
                  <nts id="Seg_4020" n="HIAT:ip">.</nts>
                  <nts id="Seg_4021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1016" id="Seg_4023" n="HIAT:u" s="T1012">
                  <ts e="T1013" id="Seg_4025" n="HIAT:w" s="T1012">A</ts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_4028" n="HIAT:w" s="T1013">miʔnʼibeʔ</ts>
                  <nts id="Seg_4029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_4031" n="HIAT:w" s="T1014">amnoləj</ts>
                  <nts id="Seg_4032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_4034" n="HIAT:w" s="T1015">bar</ts>
                  <nts id="Seg_4035" n="HIAT:ip">.</nts>
                  <nts id="Seg_4036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1019" id="Seg_4038" n="HIAT:u" s="T1016">
                  <ts e="T1017" id="Seg_4040" n="HIAT:w" s="T1016">Ĭmbi</ts>
                  <nts id="Seg_4041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_4043" n="HIAT:w" s="T1017">todam</ts>
                  <nts id="Seg_4044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4045" n="HIAT:ip">(</nts>
                  <ts e="T1019" id="Seg_4047" n="HIAT:w" s="T1018">molaʔjə</ts>
                  <nts id="Seg_4048" n="HIAT:ip">)</nts>
                  <nts id="Seg_4049" n="HIAT:ip">.</nts>
                  <nts id="Seg_4050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1025" id="Seg_4052" n="HIAT:u" s="T1019">
                  <ts e="T1020" id="Seg_4054" n="HIAT:w" s="T1019">Noʔ</ts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_4057" n="HIAT:w" s="T1020">ej</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_4060" n="HIAT:w" s="T1021">amnia</ts>
                  <nts id="Seg_4061" n="HIAT:ip">,</nts>
                  <nts id="Seg_4062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_4064" n="HIAT:w" s="T1022">bü</ts>
                  <nts id="Seg_4065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_4067" n="HIAT:w" s="T1023">ej</ts>
                  <nts id="Seg_4068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_4070" n="HIAT:w" s="T1024">bĭtlie</ts>
                  <nts id="Seg_4071" n="HIAT:ip">.</nts>
                  <nts id="Seg_4072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1027" id="Seg_4074" n="HIAT:u" s="T1025">
                  <ts e="T1026" id="Seg_4076" n="HIAT:w" s="T1025">Amga</ts>
                  <nts id="Seg_4077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_4079" n="HIAT:w" s="T1026">amnaʔbə</ts>
                  <nts id="Seg_4080" n="HIAT:ip">.</nts>
                  <nts id="Seg_4081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1037" id="Seg_4083" n="HIAT:u" s="T1027">
                  <ts e="T1028" id="Seg_4085" n="HIAT:w" s="T1027">Baška</ts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_4088" n="HIAT:w" s="T1028">tüžöj</ts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_4091" n="HIAT:w" s="T1029">šoləʔjə</ts>
                  <nts id="Seg_4092" n="HIAT:ip">,</nts>
                  <nts id="Seg_4093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_4095" n="HIAT:w" s="T1030">bar</ts>
                  <nts id="Seg_4096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_4098" n="HIAT:w" s="T1031">noʔ</ts>
                  <nts id="Seg_4099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_4101" n="HIAT:w" s="T1032">amnuʔləʔjə</ts>
                  <nts id="Seg_4102" n="HIAT:ip">,</nts>
                  <nts id="Seg_4103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_4105" n="HIAT:w" s="T1033">a</ts>
                  <nts id="Seg_4106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_4108" n="HIAT:w" s="T1034">dĭ</ts>
                  <nts id="Seg_4109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_4111" n="HIAT:w" s="T1035">ej</ts>
                  <nts id="Seg_4112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_4114" n="HIAT:w" s="T1036">amnia</ts>
                  <nts id="Seg_4115" n="HIAT:ip">.</nts>
                  <nts id="Seg_4116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1038" id="Seg_4118" n="HIAT:u" s="T1037">
                  <nts id="Seg_4119" n="HIAT:ip">(</nts>
                  <nts id="Seg_4120" n="HIAT:ip">(</nts>
                  <ats e="T1038" id="Seg_4121" n="HIAT:non-pho" s="T1037">BRK</ats>
                  <nts id="Seg_4122" n="HIAT:ip">)</nts>
                  <nts id="Seg_4123" n="HIAT:ip">)</nts>
                  <nts id="Seg_4124" n="HIAT:ip">.</nts>
                  <nts id="Seg_4125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1041" id="Seg_4127" n="HIAT:u" s="T1038">
                  <nts id="Seg_4128" n="HIAT:ip">"</nts>
                  <ts e="T1039" id="Seg_4130" n="HIAT:w" s="T1038">Ej</ts>
                  <nts id="Seg_4131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_4133" n="HIAT:w" s="T1039">tĭmnem</ts>
                  <nts id="Seg_4134" n="HIAT:ip">"</nts>
                  <nts id="Seg_4135" n="HIAT:ip">,</nts>
                  <nts id="Seg_4136" n="HIAT:ip">—</nts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_4139" n="HIAT:w" s="T1040">măndə</ts>
                  <nts id="Seg_4140" n="HIAT:ip">.</nts>
                  <nts id="Seg_4141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1046" id="Seg_4143" n="HIAT:u" s="T1041">
                  <ts e="T1042" id="Seg_4145" n="HIAT:w" s="T1041">Šeden</ts>
                  <nts id="Seg_4146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_4148" n="HIAT:w" s="T1042">dĭrgit</ts>
                  <nts id="Seg_4149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_4151" n="HIAT:w" s="T1043">ali</ts>
                  <nts id="Seg_4152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_4154" n="HIAT:w" s="T1044">ĭmbi</ts>
                  <nts id="Seg_4155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_4157" n="HIAT:w" s="T1045">dĭrgit</ts>
                  <nts id="Seg_4158" n="HIAT:ip">.</nts>
                  <nts id="Seg_4159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1052" id="Seg_4161" n="HIAT:u" s="T1046">
                  <ts e="T1047" id="Seg_4163" n="HIAT:w" s="T1046">Miʔ</ts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_4166" n="HIAT:w" s="T1047">ibibeʔ</ts>
                  <nts id="Seg_4167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_4169" n="HIAT:w" s="T1048">tĭn</ts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4171" n="HIAT:ip">(</nts>
                  <ts e="T1050" id="Seg_4173" n="HIAT:w" s="T1049">se-</ts>
                  <nts id="Seg_4174" n="HIAT:ip">)</nts>
                  <nts id="Seg_4175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_4177" n="HIAT:w" s="T1050">sʼestrandə</ts>
                  <nts id="Seg_4178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_4180" n="HIAT:w" s="T1051">tüžöjdə</ts>
                  <nts id="Seg_4181" n="HIAT:ip">.</nts>
                  <nts id="Seg_4182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1057" id="Seg_4184" n="HIAT:u" s="T1052">
                  <ts e="T1053" id="Seg_4186" n="HIAT:w" s="T1052">Dĭ</ts>
                  <nts id="Seg_4187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_4189" n="HIAT:w" s="T1053">vedʼ</ts>
                  <nts id="Seg_4190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_4192" n="HIAT:w" s="T1054">ĭmbidə</ts>
                  <nts id="Seg_4193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_4195" n="HIAT:w" s="T1055">ej</ts>
                  <nts id="Seg_4196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_4198" n="HIAT:w" s="T1056">tĭmnet</ts>
                  <nts id="Seg_4199" n="HIAT:ip">.</nts>
                  <nts id="Seg_4200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1064" id="Seg_4202" n="HIAT:u" s="T1057">
                  <ts e="T1058" id="Seg_4204" n="HIAT:w" s="T1057">Tože</ts>
                  <nts id="Seg_4205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_4207" n="HIAT:w" s="T1058">tüžöj</ts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_4210" n="HIAT:w" s="T1059">dăre</ts>
                  <nts id="Seg_4211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_4213" n="HIAT:w" s="T1060">molambi</ts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_4216" n="HIAT:w" s="T1061">bar</ts>
                  <nts id="Seg_4217" n="HIAT:ip">,</nts>
                  <nts id="Seg_4218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_4220" n="HIAT:w" s="T1062">todam</ts>
                  <nts id="Seg_4221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_4223" n="HIAT:w" s="T1063">mobi</ts>
                  <nts id="Seg_4224" n="HIAT:ip">.</nts>
                  <nts id="Seg_4225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1070" id="Seg_4227" n="HIAT:u" s="T1064">
                  <ts e="T1065" id="Seg_4229" n="HIAT:w" s="T1064">A</ts>
                  <nts id="Seg_4230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4231" n="HIAT:ip">(</nts>
                  <ts e="T1066" id="Seg_4233" n="HIAT:w" s="T1065">boʔ</ts>
                  <nts id="Seg_4234" n="HIAT:ip">)</nts>
                  <nts id="Seg_4235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_4237" n="HIAT:w" s="T1066">sildə</ts>
                  <nts id="Seg_4238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_4240" n="HIAT:w" s="T1067">iʔgö</ts>
                  <nts id="Seg_4241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4242" n="HIAT:ip">(</nts>
                  <ts e="T1069" id="Seg_4244" n="HIAT:w" s="T1068">m-</ts>
                  <nts id="Seg_4245" n="HIAT:ip">)</nts>
                  <nts id="Seg_4246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_4248" n="HIAT:w" s="T1069">ibi</ts>
                  <nts id="Seg_4249" n="HIAT:ip">.</nts>
                  <nts id="Seg_4250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1075" id="Seg_4252" n="HIAT:u" s="T1070">
                  <ts e="T1071" id="Seg_4254" n="HIAT:w" s="T1070">Amnobi</ts>
                  <nts id="Seg_4255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_4257" n="HIAT:w" s="T1071">miʔnʼibeʔ</ts>
                  <nts id="Seg_4258" n="HIAT:ip">,</nts>
                  <nts id="Seg_4259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4260" n="HIAT:ip">(</nts>
                  <ts e="T1073" id="Seg_4262" n="HIAT:w" s="T1072">tona-</ts>
                  <nts id="Seg_4263" n="HIAT:ip">)</nts>
                  <nts id="Seg_4264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_4266" n="HIAT:w" s="T1073">todam</ts>
                  <nts id="Seg_4267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_4269" n="HIAT:w" s="T1074">molambi</ts>
                  <nts id="Seg_4270" n="HIAT:ip">.</nts>
                  <nts id="Seg_4271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1076" id="Seg_4273" n="HIAT:u" s="T1075">
                  <nts id="Seg_4274" n="HIAT:ip">(</nts>
                  <nts id="Seg_4275" n="HIAT:ip">(</nts>
                  <ats e="T1076" id="Seg_4276" n="HIAT:non-pho" s="T1075">BRK</ats>
                  <nts id="Seg_4277" n="HIAT:ip">)</nts>
                  <nts id="Seg_4278" n="HIAT:ip">)</nts>
                  <nts id="Seg_4279" n="HIAT:ip">.</nts>
                  <nts id="Seg_4280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1084" id="Seg_4282" n="HIAT:u" s="T1076">
                  <ts e="T1077" id="Seg_4284" n="HIAT:w" s="T1076">Dĭ</ts>
                  <nts id="Seg_4285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_4287" n="HIAT:w" s="T1077">onʼiʔ</ts>
                  <nts id="Seg_4288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_4290" n="HIAT:w" s="T1078">koʔbdozi</ts>
                  <nts id="Seg_4291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_4293" n="HIAT:w" s="T1079">amnolaʔpi</ts>
                  <nts id="Seg_4294" n="HIAT:ip">,</nts>
                  <nts id="Seg_4295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_4297" n="HIAT:w" s="T1080">dĭgəttə</ts>
                  <nts id="Seg_4298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4299" n="HIAT:ip">(</nts>
                  <ts e="T1082" id="Seg_4301" n="HIAT:w" s="T1081">baš-</ts>
                  <nts id="Seg_4302" n="HIAT:ip">)</nts>
                  <nts id="Seg_4303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_4305" n="HIAT:w" s="T1082">baska</ts>
                  <nts id="Seg_4306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_4308" n="HIAT:w" s="T1083">ibi</ts>
                  <nts id="Seg_4309" n="HIAT:ip">.</nts>
                  <nts id="Seg_4310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1090" id="Seg_4312" n="HIAT:u" s="T1084">
                  <ts e="T1085" id="Seg_4314" n="HIAT:w" s="T1084">Možet</ts>
                  <nts id="Seg_4315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_4317" n="HIAT:w" s="T1085">dĭ</ts>
                  <nts id="Seg_4318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4319" n="HIAT:ip">(</nts>
                  <ts e="T1087" id="Seg_4321" n="HIAT:w" s="T1086">чё-</ts>
                  <nts id="Seg_4322" n="HIAT:ip">)</nts>
                  <nts id="Seg_4323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_4325" n="HIAT:w" s="T1087">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_4326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4327" n="HIAT:ip">(</nts>
                  <ts e="T1089" id="Seg_4329" n="HIAT:w" s="T1088">tĭ-</ts>
                  <nts id="Seg_4330" n="HIAT:ip">)</nts>
                  <nts id="Seg_4331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_4333" n="HIAT:w" s="T1089">tĭlbi</ts>
                  <nts id="Seg_4334" n="HIAT:ip">?</nts>
                  <nts id="Seg_4335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1097" id="Seg_4337" n="HIAT:u" s="T1090">
                  <ts e="T1091" id="Seg_4339" n="HIAT:w" s="T1090">I</ts>
                  <nts id="Seg_4340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_4342" n="HIAT:w" s="T1091">tüj</ts>
                  <nts id="Seg_4343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4344" n="HIAT:ip">(</nts>
                  <ts e="T1093" id="Seg_4346" n="HIAT:w" s="T1092">tüžej-</ts>
                  <nts id="Seg_4347" n="HIAT:ip">)</nts>
                  <nts id="Seg_4348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_4350" n="HIAT:w" s="T1093">tüžöjdə</ts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_4353" n="HIAT:w" s="T1094">bar</ts>
                  <nts id="Seg_4354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_4356" n="HIAT:w" s="T1095">ej</ts>
                  <nts id="Seg_4357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_4359" n="HIAT:w" s="T1096">nuliaʔi</ts>
                  <nts id="Seg_4360" n="HIAT:ip">.</nts>
                  <nts id="Seg_4361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1098" id="Seg_4363" n="HIAT:u" s="T1097">
                  <nts id="Seg_4364" n="HIAT:ip">(</nts>
                  <nts id="Seg_4365" n="HIAT:ip">(</nts>
                  <ats e="T1098" id="Seg_4366" n="HIAT:non-pho" s="T1097">BRK</ats>
                  <nts id="Seg_4367" n="HIAT:ip">)</nts>
                  <nts id="Seg_4368" n="HIAT:ip">)</nts>
                  <nts id="Seg_4369" n="HIAT:ip">.</nts>
                  <nts id="Seg_4370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1104" id="Seg_4372" n="HIAT:u" s="T1098">
                  <ts e="T1099" id="Seg_4374" n="HIAT:w" s="T1098">Šiʔ</ts>
                  <nts id="Seg_4375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4376" n="HIAT:ip">(</nts>
                  <ts e="T1100" id="Seg_4378" n="HIAT:w" s="T1099">var-</ts>
                  <nts id="Seg_4379" n="HIAT:ip">)</nts>
                  <nts id="Seg_4380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_4382" n="HIAT:w" s="T1100">bar</ts>
                  <nts id="Seg_4383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_4385" n="HIAT:w" s="T1101">karəldʼan</ts>
                  <nts id="Seg_4386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4387" n="HIAT:ip">(</nts>
                  <ts e="T1103" id="Seg_4389" n="HIAT:w" s="T1102">kala-</ts>
                  <nts id="Seg_4390" n="HIAT:ip">)</nts>
                  <nts id="Seg_4391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4392" n="HIAT:ip">(</nts>
                  <nts id="Seg_4393" n="HIAT:ip">(</nts>
                  <ats e="T1104" id="Seg_4394" n="HIAT:non-pho" s="T1103">DMG</ats>
                  <nts id="Seg_4395" n="HIAT:ip">)</nts>
                  <nts id="Seg_4396" n="HIAT:ip">)</nts>
                  <nts id="Seg_4397" n="HIAT:ip">.</nts>
                  <nts id="Seg_4398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1107" id="Seg_4400" n="HIAT:u" s="T1104">
                  <ts e="T1105" id="Seg_4402" n="HIAT:w" s="T1104">Ši</ts>
                  <nts id="Seg_4403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_4405" n="HIAT:w" s="T1105">karəldʼan</ts>
                  <nts id="Seg_4406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_4408" n="HIAT:w" s="T1106">kalla</ts>
                  <nts id="Seg_4409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_4411" n="HIAT:w" s="T1122">dʼürləbeʔ</ts>
                  <nts id="Seg_4412" n="HIAT:ip">.</nts>
                  <nts id="Seg_4413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1110" id="Seg_4415" n="HIAT:u" s="T1107">
                  <nts id="Seg_4416" n="HIAT:ip">(</nts>
                  <ts e="T1123" id="Seg_4418" n="HIAT:w" s="T1107">Kalla</ts>
                  <nts id="Seg_4419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_4421" n="HIAT:w" s="T1123">dʼürləleʔ</ts>
                  <nts id="Seg_4422" n="HIAT:ip">)</nts>
                  <nts id="Seg_4423" n="HIAT:ip">,</nts>
                  <nts id="Seg_4424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_4426" n="HIAT:w" s="T1108">davajtʼe</ts>
                  <nts id="Seg_4427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4428" n="HIAT:ip">(</nts>
                  <ts e="T1110" id="Seg_4430" n="HIAT:w" s="T1109">kamrolabaʔ</ts>
                  <nts id="Seg_4431" n="HIAT:ip">)</nts>
                  <nts id="Seg_4432" n="HIAT:ip">.</nts>
                  <nts id="Seg_4433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1112" id="Seg_4435" n="HIAT:u" s="T1110">
                  <ts e="T1111" id="Seg_4437" n="HIAT:w" s="T1110">I</ts>
                  <nts id="Seg_4438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_4440" n="HIAT:w" s="T1111">pănarlabaʔ</ts>
                  <nts id="Seg_4441" n="HIAT:ip">.</nts>
                  <nts id="Seg_4442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1115" id="Seg_4444" n="HIAT:u" s="T1112">
                  <ts e="T1113" id="Seg_4446" n="HIAT:w" s="T1112">Dĭgəttə</ts>
                  <nts id="Seg_4447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_4449" n="HIAT:w" s="T1113">kangaʔ</ts>
                  <nts id="Seg_4450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_4452" n="HIAT:w" s="T1114">kudajzi</ts>
                  <nts id="Seg_4453" n="HIAT:ip">.</nts>
                  <nts id="Seg_4454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T1115" id="Seg_4455" n="sc" s="T0">
               <ts e="T1" id="Seg_4457" n="e" s="T0">Nʼergölaʔbə </ts>
               <ts e="T2" id="Seg_4459" n="e" s="T1">bar </ts>
               <ts e="T3" id="Seg_4461" n="e" s="T2">kirgarlaʔbə, </ts>
               <ts e="T4" id="Seg_4463" n="e" s="T3">amnoləj </ts>
               <ts e="T5" id="Seg_4465" n="e" s="T4">dăk </ts>
               <ts e="T6" id="Seg_4467" n="e" s="T5">(dʼü=) </ts>
               <ts e="T7" id="Seg_4469" n="e" s="T6">dʼü </ts>
               <ts e="T8" id="Seg_4471" n="e" s="T7">(tegolaʔbə). </ts>
               <ts e="T9" id="Seg_4473" n="e" s="T8">Dĭ </ts>
               <ts e="T10" id="Seg_4475" n="e" s="T9">multuk. </ts>
               <ts e="T11" id="Seg_4477" n="e" s="T10">((BRK)). </ts>
               <ts e="T12" id="Seg_4479" n="e" s="T11">Girgit </ts>
               <ts e="T13" id="Seg_4481" n="e" s="T12">tĭrgit </ts>
               <ts e="T14" id="Seg_4483" n="e" s="T13">загадка, </ts>
               <ts e="T15" id="Seg_4485" n="e" s="T14">(kədet-) </ts>
               <ts e="T16" id="Seg_4487" n="e" s="T15">kötengəndə </ts>
               <ts e="T17" id="Seg_4489" n="e" s="T16">ягодка. </ts>
               <ts e="T18" id="Seg_4491" n="e" s="T17">Dĭ </ts>
               <ts e="T19" id="Seg_4493" n="e" s="T18">multuk. </ts>
               <ts e="T20" id="Seg_4495" n="e" s="T19">((BRK)). </ts>
               <ts e="T21" id="Seg_4497" n="e" s="T20">Kamen </ts>
               <ts e="T22" id="Seg_4499" n="e" s="T21">miʔ </ts>
               <ts e="T23" id="Seg_4501" n="e" s="T22">iʔgö </ts>
               <ts e="T24" id="Seg_4503" n="e" s="T23">ibibeʔ, </ts>
               <ts e="T25" id="Seg_4505" n="e" s="T24">nuzaŋ, </ts>
               <ts e="T26" id="Seg_4507" n="e" s="T25">măn </ts>
               <ts e="T27" id="Seg_4509" n="e" s="T26">mĭmbiem </ts>
               <ts e="T28" id="Seg_4511" n="e" s="T27">urgo </ts>
               <ts e="T29" id="Seg_4513" n="e" s="T28">măjagən, </ts>
               <ts e="T30" id="Seg_4515" n="e" s="T29">Belăgorjagən, </ts>
               <ts e="T31" id="Seg_4517" n="e" s="T30">dĭn </ts>
               <ts e="T32" id="Seg_4519" n="e" s="T31">kola </ts>
               <ts e="T33" id="Seg_4521" n="e" s="T32">dʼaʔpiam. </ts>
               <ts e="T34" id="Seg_4523" n="e" s="T33">I </ts>
               <ts e="T35" id="Seg_4525" n="e" s="T34">uja </ts>
               <ts e="T36" id="Seg_4527" n="e" s="T35">kuʔpiam. </ts>
               <ts e="T37" id="Seg_4529" n="e" s="T36">Dĭgəttə </ts>
               <ts e="T38" id="Seg_4531" n="e" s="T37">bar </ts>
               <ts e="T39" id="Seg_4533" n="e" s="T38">măjaʔi </ts>
               <ts e="T40" id="Seg_4535" n="e" s="T39">(sagərʔi) </ts>
               <ts e="T41" id="Seg_4537" n="e" s="T40">maluʔpiʔi. </ts>
               <ts e="T42" id="Seg_4539" n="e" s="T41">Măn </ts>
               <ts e="T43" id="Seg_4541" n="e" s="T42">šalbə </ts>
               <ts e="T44" id="Seg_4543" n="e" s="T43">už </ts>
               <ts e="T45" id="Seg_4545" n="e" s="T44">naga, </ts>
               <ts e="T46" id="Seg_4547" n="e" s="T45">bar </ts>
               <ts e="T47" id="Seg_4549" n="e" s="T46">külambi. </ts>
               <ts e="T48" id="Seg_4551" n="e" s="T47">A </ts>
               <ts e="T49" id="Seg_4553" n="e" s="T48">dĭn, </ts>
               <ts e="T50" id="Seg_4555" n="e" s="T49">gijen </ts>
               <ts e="T51" id="Seg_4557" n="e" s="T50">maʔi </ts>
               <ts e="T52" id="Seg_4559" n="e" s="T51">nugabiʔi </ts>
               <ts e="T53" id="Seg_4561" n="e" s="T52">bar, </ts>
               <ts e="T54" id="Seg_4563" n="e" s="T53">saʔməluʔpiʔi </ts>
               <ts e="T55" id="Seg_4565" n="e" s="T54">i </ts>
               <ts e="T56" id="Seg_4567" n="e" s="T55">bar </ts>
               <ts e="T57" id="Seg_4569" n="e" s="T56">külambiʔi. </ts>
               <ts e="T58" id="Seg_4571" n="e" s="T57">Tolʼkă </ts>
               <ts e="T59" id="Seg_4573" n="e" s="T58">šo </ts>
               <ts e="T60" id="Seg_4575" n="e" s="T59">păjdluʔpiʔi. </ts>
               <ts e="T61" id="Seg_4577" n="e" s="T60">((BRK)). </ts>
               <ts e="T62" id="Seg_4579" n="e" s="T61">Tüj </ts>
               <ts e="T63" id="Seg_4581" n="e" s="T62">(ĭm-) </ts>
               <ts e="T64" id="Seg_4583" n="e" s="T63">ĭmbidə </ts>
               <ts e="T65" id="Seg_4585" n="e" s="T64">ej </ts>
               <ts e="T66" id="Seg_4587" n="e" s="T65">kuliam. </ts>
               <ts e="T67" id="Seg_4589" n="e" s="T66">Ĭmbidə </ts>
               <ts e="T68" id="Seg_4591" n="e" s="T67">ej </ts>
               <ts e="T69" id="Seg_4593" n="e" s="T68">(nʼilol-) </ts>
               <ts e="T70" id="Seg_4595" n="e" s="T69">nʼilgöliam. </ts>
               <ts e="T71" id="Seg_4597" n="e" s="T70">Bar </ts>
               <ts e="T72" id="Seg_4599" n="e" s="T71">(s-) </ts>
               <ts e="T73" id="Seg_4601" n="e" s="T72">šalbə </ts>
               <ts e="T1117" id="Seg_4603" n="e" s="T73">kalla </ts>
               <ts e="T74" id="Seg_4605" n="e" s="T1117">dʼürbi. </ts>
               <ts e="T75" id="Seg_4607" n="e" s="T74">Tüj </ts>
               <ts e="T76" id="Seg_4609" n="e" s="T75">măn </ts>
               <ts e="T77" id="Seg_4611" n="e" s="T76">unnʼam </ts>
               <ts e="T78" id="Seg_4613" n="e" s="T77">amnolaʔbəm. </ts>
               <ts e="T79" id="Seg_4615" n="e" s="T78">Šindidə </ts>
               <ts e="T80" id="Seg_4617" n="e" s="T79">naga, </ts>
               <ts e="T81" id="Seg_4619" n="e" s="T80">bar </ts>
               <ts e="T82" id="Seg_4621" n="e" s="T81">külambiʔi </ts>
               <ts e="T83" id="Seg_4623" n="e" s="T82">măn </ts>
               <ts e="T84" id="Seg_4625" n="e" s="T83">nuzaŋbə. </ts>
               <ts e="T85" id="Seg_4627" n="e" s="T84">((BRK)). </ts>
               <ts e="T86" id="Seg_4629" n="e" s="T85">Tüj </ts>
               <ts e="T87" id="Seg_4631" n="e" s="T86">bar </ts>
               <ts e="T88" id="Seg_4633" n="e" s="T87">aktʼizaŋbə </ts>
               <ts e="T89" id="Seg_4635" n="e" s="T88">bar </ts>
               <ts e="T90" id="Seg_4637" n="e" s="T89">maluʔpi. </ts>
               <ts e="T91" id="Seg_4639" n="e" s="T90">Bar </ts>
               <ts e="T92" id="Seg_4641" n="e" s="T91">noʔsi </ts>
               <ts e="T93" id="Seg_4643" n="e" s="T92">özerluʔpi. </ts>
               <ts e="T94" id="Seg_4645" n="e" s="T93">((BRK)). </ts>
               <ts e="T95" id="Seg_4647" n="e" s="T94">Măn </ts>
               <ts e="T96" id="Seg_4649" n="e" s="T95">ugandə </ts>
               <ts e="T97" id="Seg_4651" n="e" s="T96">kuvas </ts>
               <ts e="T98" id="Seg_4653" n="e" s="T97">mobiam. </ts>
               <ts e="T99" id="Seg_4655" n="e" s="T98">A </ts>
               <ts e="T100" id="Seg_4657" n="e" s="T99">giber </ts>
               <ts e="T101" id="Seg_4659" n="e" s="T100">dĭ </ts>
               <ts e="T102" id="Seg_4661" n="e" s="T101">kuvas </ts>
               <ts e="T1118" id="Seg_4663" n="e" s="T102">kalla </ts>
               <ts e="T103" id="Seg_4665" n="e" s="T1118">dʼürbi? </ts>
               <ts e="T1119" id="Seg_4667" n="e" s="T103">Kalla </ts>
               <ts e="T104" id="Seg_4669" n="e" s="T1119">dʼürbi </ts>
               <ts e="T105" id="Seg_4671" n="e" s="T104">urgo </ts>
               <ts e="T106" id="Seg_4673" n="e" s="T105">măjazaŋdə, </ts>
               <ts e="T107" id="Seg_4675" n="e" s="T106">i </ts>
               <ts e="T108" id="Seg_4677" n="e" s="T107">gijendə </ts>
               <ts e="T109" id="Seg_4679" n="e" s="T108">naga. </ts>
               <ts e="T110" id="Seg_4681" n="e" s="T109">((BRK)). </ts>
               <ts e="T111" id="Seg_4683" n="e" s="T110">Tüj </ts>
               <ts e="T112" id="Seg_4685" n="e" s="T111">dĭ </ts>
               <ts e="T113" id="Seg_4687" n="e" s="T112">kamendə </ts>
               <ts e="T114" id="Seg_4689" n="e" s="T113">ej </ts>
               <ts e="T115" id="Seg_4691" n="e" s="T114">šoləj. </ts>
               <ts e="T116" id="Seg_4693" n="e" s="T115">((BRK)). </ts>
               <ts e="T117" id="Seg_4695" n="e" s="T116">((BRK)). </ts>
               <ts e="T118" id="Seg_4697" n="e" s="T117">Măn </ts>
               <ts e="T119" id="Seg_4699" n="e" s="T118">uʔbdəbiam </ts>
               <ts e="T120" id="Seg_4701" n="e" s="T119">(pereulăktə), </ts>
               <ts e="T121" id="Seg_4703" n="e" s="T120">(šo-) </ts>
               <ts e="T122" id="Seg_4705" n="e" s="T121">šonəga </ts>
               <ts e="T123" id="Seg_4707" n="e" s="T122">(aga). </ts>
               <ts e="T124" id="Seg_4709" n="e" s="T123">Măn </ts>
               <ts e="T125" id="Seg_4711" n="e" s="T124">măndəm: </ts>
               <ts e="T126" id="Seg_4713" n="e" s="T125">ugandə </ts>
               <ts e="T127" id="Seg_4715" n="e" s="T126">urgo </ts>
               <ts e="T128" id="Seg_4717" n="e" s="T127">kuza </ts>
               <ts e="T129" id="Seg_4719" n="e" s="T128">šonəga. </ts>
               <ts e="T130" id="Seg_4721" n="e" s="T129">Aparatsi </ts>
               <ts e="T131" id="Seg_4723" n="e" s="T130">turaʔi </ts>
               <ts e="T132" id="Seg_4725" n="e" s="T131">kürleʔbə </ts>
               <ts e="T133" id="Seg_4727" n="e" s="T132">mĭnge. </ts>
               <ts e="T134" id="Seg_4729" n="e" s="T133">Dĭgəttə </ts>
               <ts e="T135" id="Seg_4731" n="e" s="T134">măn </ts>
               <ts e="T136" id="Seg_4733" n="e" s="T135">nubiam, </ts>
               <ts e="T137" id="Seg_4735" n="e" s="T136">turagəʔ </ts>
               <ts e="T138" id="Seg_4737" n="e" s="T137">toʔndə </ts>
               <ts e="T139" id="Seg_4739" n="e" s="T138">dĭ </ts>
               <ts e="T140" id="Seg_4741" n="e" s="T139">(m-) </ts>
               <ts e="T141" id="Seg_4743" n="e" s="T140">bar </ts>
               <ts e="T142" id="Seg_4745" n="e" s="T141">măna </ts>
               <ts e="T143" id="Seg_4747" n="e" s="T142">(k-) </ts>
               <ts e="T144" id="Seg_4749" n="e" s="T143">kürbi. </ts>
               <ts e="T145" id="Seg_4751" n="e" s="T144">((BRK)). </ts>
               <ts e="T146" id="Seg_4753" n="e" s="T145">(Dĭn) </ts>
               <ts e="T147" id="Seg_4755" n="e" s="T146">nükegən </ts>
               <ts e="T148" id="Seg_4757" n="e" s="T147">kolat </ts>
               <ts e="T149" id="Seg_4759" n="e" s="T148">ibi, </ts>
               <ts e="T150" id="Seg_4761" n="e" s="T149">dĭn. </ts>
               <ts e="T151" id="Seg_4763" n="e" s="T150">((BRK)). </ts>
               <ts e="T152" id="Seg_4765" n="e" s="T151">Tĭn </ts>
               <ts e="T153" id="Seg_4767" n="e" s="T152">kolat </ts>
               <ts e="T154" id="Seg_4769" n="e" s="T153">ibi </ts>
               <ts e="T155" id="Seg_4771" n="e" s="T154">dĭn </ts>
               <ts e="T156" id="Seg_4773" n="e" s="T155">(meit). </ts>
               <ts e="T157" id="Seg_4775" n="e" s="T156">Dĭgəttə </ts>
               <ts e="T158" id="Seg_4777" n="e" s="T157">dĭʔnə </ts>
               <ts e="T159" id="Seg_4779" n="e" s="T158">šobi, </ts>
               <ts e="T160" id="Seg_4781" n="e" s="T159">măndə: </ts>
               <ts e="T161" id="Seg_4783" n="e" s="T160">Măn </ts>
               <ts e="T162" id="Seg_4785" n="e" s="T161">tănan </ts>
               <ts e="T163" id="Seg_4787" n="e" s="T162">kola </ts>
               <ts e="T164" id="Seg_4789" n="e" s="T163">detlem </ts>
               <ts e="T165" id="Seg_4791" n="e" s="T164">teinen. </ts>
               <ts e="T166" id="Seg_4793" n="e" s="T165">A </ts>
               <ts e="T167" id="Seg_4795" n="e" s="T166">măn </ts>
               <ts e="T168" id="Seg_4797" n="e" s="T167">šobiam: </ts>
               <ts e="T169" id="Seg_4799" n="e" s="T168">Tăn </ts>
               <ts e="T170" id="Seg_4801" n="e" s="T169">kolal </ts>
               <ts e="T171" id="Seg_4803" n="e" s="T170">meit </ts>
               <ts e="T172" id="Seg_4805" n="e" s="T171">ibi. </ts>
               <ts e="T173" id="Seg_4807" n="e" s="T172">Măn </ts>
               <ts e="T174" id="Seg_4809" n="e" s="T173">ej </ts>
               <ts e="T175" id="Seg_4811" n="e" s="T174">ibiem". </ts>
               <ts e="T176" id="Seg_4813" n="e" s="T175">((BRK)). </ts>
               <ts e="T177" id="Seg_4815" n="e" s="T176">Ĭmbi </ts>
               <ts e="T178" id="Seg_4817" n="e" s="T177">dĭ </ts>
               <ts e="T179" id="Seg_4819" n="e" s="T178">măna </ts>
               <ts e="T180" id="Seg_4821" n="e" s="T179">mĭləj, </ts>
               <ts e="T181" id="Seg_4823" n="e" s="T180">idʼiʔeje. </ts>
               <ts e="T182" id="Seg_4825" n="e" s="T181">Nu </ts>
               <ts e="T183" id="Seg_4827" n="e" s="T182">idʼiʔeʔe, </ts>
               <ts e="T184" id="Seg_4829" n="e" s="T183">kabarləj. </ts>
               <ts e="T185" id="Seg_4831" n="e" s="T184">Dĭzeŋ </ts>
               <ts e="T186" id="Seg_4833" n="e" s="T185">bostə </ts>
               <ts e="T187" id="Seg_4835" n="e" s="T186">(aktʼa=) </ts>
               <ts e="T188" id="Seg_4837" n="e" s="T187">aktʼanə </ts>
               <ts e="T189" id="Seg_4839" n="e" s="T188">(š-) </ts>
               <ts e="T190" id="Seg_4841" n="e" s="T189">sadarbiʔi. </ts>
               <ts e="T191" id="Seg_4843" n="e" s="T190">A </ts>
               <ts e="T192" id="Seg_4845" n="e" s="T191">tăn </ts>
               <ts e="T193" id="Seg_4847" n="e" s="T192">ej </ts>
               <ts e="T194" id="Seg_4849" n="e" s="T193">mĭbiel </ts>
               <ts e="T195" id="Seg_4851" n="e" s="T194">aktʼa. </ts>
               <ts e="T196" id="Seg_4853" n="e" s="T195">(I </ts>
               <ts e="T197" id="Seg_4855" n="e" s="T196">dĭ-) </ts>
               <ts e="T198" id="Seg_4857" n="e" s="T197">I </ts>
               <ts e="T199" id="Seg_4859" n="e" s="T198">(dĭn=) </ts>
               <ts e="T200" id="Seg_4861" n="e" s="T199">dĭ </ts>
               <ts e="T201" id="Seg_4863" n="e" s="T200">kumen </ts>
               <ts e="T202" id="Seg_4865" n="e" s="T201">mĭləʔi </ts>
               <ts e="T203" id="Seg_4867" n="e" s="T202">kabarləj. </ts>
               <ts e="T204" id="Seg_4869" n="e" s="T203">((BRK)). </ts>
               <ts e="T205" id="Seg_4871" n="e" s="T204">Dĭgəttə </ts>
               <ts e="T206" id="Seg_4873" n="e" s="T205">măn </ts>
               <ts e="T207" id="Seg_4875" n="e" s="T206">(u) </ts>
               <ts e="T208" id="Seg_4877" n="e" s="T207">kambiam </ts>
               <ts e="T209" id="Seg_4879" n="e" s="T208">Anissʼanə, </ts>
               <ts e="T210" id="Seg_4881" n="e" s="T209">dĭ </ts>
               <ts e="T211" id="Seg_4883" n="e" s="T210">măndə: </ts>
               <ts e="T212" id="Seg_4885" n="e" s="T211">Jelʼa </ts>
               <ts e="T213" id="Seg_4887" n="e" s="T212">surarbi </ts>
               <ts e="T214" id="Seg_4889" n="e" s="T213">munəʔi, </ts>
               <ts e="T215" id="Seg_4891" n="e" s="T214">măn </ts>
               <ts e="T216" id="Seg_4893" n="e" s="T215">oʔbdəbiam </ts>
               <ts e="T217" id="Seg_4895" n="e" s="T216">bʼeʔ </ts>
               <ts e="T218" id="Seg_4897" n="e" s="T217">munəjʔ. </ts>
               <ts e="T219" id="Seg_4899" n="e" s="T218">(Mĭ- </ts>
               <ts e="T220" id="Seg_4901" n="e" s="T219">me-) </ts>
               <ts e="T221" id="Seg_4903" n="e" s="T220">Mĭləm </ts>
               <ts e="T222" id="Seg_4905" n="e" s="T221">(bʼeʔ </ts>
               <ts e="T223" id="Seg_4907" n="e" s="T222">k-) </ts>
               <ts e="T224" id="Seg_4909" n="e" s="T223">onʼiʔ </ts>
               <ts e="T225" id="Seg_4911" n="e" s="T224">munəjʔ </ts>
               <ts e="T226" id="Seg_4913" n="e" s="T225">bʼeʔ </ts>
               <ts e="T227" id="Seg_4915" n="e" s="T226">kăpʼejkazi </ts>
               <ts e="T228" id="Seg_4917" n="e" s="T227">mĭləm </ts>
               <ts e="T229" id="Seg_4919" n="e" s="T228">dĭʔnə. </ts>
               <ts e="T230" id="Seg_4921" n="e" s="T229">((BRK)). </ts>
               <ts e="T231" id="Seg_4923" n="e" s="T230">Jelʼa </ts>
               <ts e="T232" id="Seg_4925" n="e" s="T231">măndə: </ts>
               <ts e="T233" id="Seg_4927" n="e" s="T232">Kanžəbəj </ts>
               <ts e="T234" id="Seg_4929" n="e" s="T233">miʔnʼibeʔ, </ts>
               <ts e="T235" id="Seg_4931" n="e" s="T234">a </ts>
               <ts e="T236" id="Seg_4933" n="e" s="T235">măn </ts>
               <ts e="T237" id="Seg_4935" n="e" s="T236">măndəm: </ts>
               <ts e="T238" id="Seg_4937" n="e" s="T237">Dʼok, </ts>
               <ts e="T239" id="Seg_4939" n="e" s="T238">ej </ts>
               <ts e="T240" id="Seg_4941" n="e" s="T239">kalam, </ts>
               <ts e="T241" id="Seg_4943" n="e" s="T240">ugandə </ts>
               <ts e="T242" id="Seg_4945" n="e" s="T241">kuŋgəʔ. </ts>
               <ts e="T243" id="Seg_4947" n="e" s="T242">Ĭmbi </ts>
               <ts e="T244" id="Seg_4949" n="e" s="T243">kuŋgeʔ? </ts>
               <ts e="T245" id="Seg_4951" n="e" s="T244">Amnaʔ. </ts>
               <ts e="T246" id="Seg_4953" n="e" s="T245">(Süjö- </ts>
               <ts e="T247" id="Seg_4955" n="e" s="T246">süjönə=) </ts>
               <ts e="T248" id="Seg_4957" n="e" s="T247">Bazaj </ts>
               <ts e="T249" id="Seg_4959" n="e" s="T248">süjönə. </ts>
               <ts e="T250" id="Seg_4961" n="e" s="T249">I </ts>
               <ts e="T251" id="Seg_4963" n="e" s="T250">büžü </ts>
               <ts e="T252" id="Seg_4965" n="e" s="T251">šolal. </ts>
               <ts e="T253" id="Seg_4967" n="e" s="T252">Dʼok, </ts>
               <ts e="T254" id="Seg_4969" n="e" s="T253">măn </ts>
               <ts e="T255" id="Seg_4971" n="e" s="T254">pimniem </ts>
               <ts e="T256" id="Seg_4973" n="e" s="T255">bazaj </ts>
               <ts e="T257" id="Seg_4975" n="e" s="T256">(sʼü-) </ts>
               <ts e="T258" id="Seg_4977" n="e" s="T257">süjönə </ts>
               <ts e="T259" id="Seg_4979" n="e" s="T258">(amnəsʼtə). </ts>
               <ts e="T260" id="Seg_4981" n="e" s="T259">A_to </ts>
               <ts e="T261" id="Seg_4983" n="e" s="T260">külalləl. </ts>
               <ts e="T262" id="Seg_4985" n="e" s="T261">Dʼok, </ts>
               <ts e="T263" id="Seg_4987" n="e" s="T262">ej </ts>
               <ts e="T264" id="Seg_4989" n="e" s="T263">külalləl, </ts>
               <ts e="T265" id="Seg_4991" n="e" s="T264">dĭn </ts>
               <ts e="T266" id="Seg_4993" n="e" s="T265">jakše </ts>
               <ts e="T267" id="Seg_4995" n="e" s="T266">amnozittə, </ts>
               <ts e="T268" id="Seg_4997" n="e" s="T267">kak </ts>
               <ts e="T269" id="Seg_4999" n="e" s="T268">turagən. </ts>
               <ts e="T270" id="Seg_5001" n="e" s="T269">Büžü </ts>
               <ts e="T271" id="Seg_5003" n="e" s="T270">maʔnəl </ts>
               <ts e="T272" id="Seg_5005" n="e" s="T271">šolal, </ts>
               <ts e="T273" id="Seg_5007" n="e" s="T272">miʔnʼibeʔ </ts>
               <ts e="T274" id="Seg_5009" n="e" s="T273">amnolal. </ts>
               <ts e="T275" id="Seg_5011" n="e" s="T274">((BRK)). </ts>
               <ts e="T276" id="Seg_5013" n="e" s="T275">Nagurköʔ </ts>
               <ts e="T277" id="Seg_5015" n="e" s="T276">kalləbaʔ. </ts>
               <ts e="T278" id="Seg_5017" n="e" s="T277">((BRK)). </ts>
               <ts e="T279" id="Seg_5019" n="e" s="T278">Nüdʼin </ts>
               <ts e="T280" id="Seg_5021" n="e" s="T279">kalam </ts>
               <ts e="T281" id="Seg_5023" n="e" s="T280">onʼiʔ </ts>
               <ts e="T282" id="Seg_5025" n="e" s="T281">tibinə. </ts>
               <ts e="T283" id="Seg_5027" n="e" s="T282">(T- </ts>
               <ts e="T284" id="Seg_5029" n="e" s="T283">tĭbi </ts>
               <ts e="T285" id="Seg_5031" n="e" s="T284">tĭ- </ts>
               <ts e="T286" id="Seg_5033" n="e" s="T285">tĭbi=) </ts>
               <ts e="T287" id="Seg_5035" n="e" s="T286">Onʼiʔ </ts>
               <ts e="T288" id="Seg_5037" n="e" s="T287">kuzanə. </ts>
               <ts e="T289" id="Seg_5039" n="e" s="T288">Măllam: </ts>
               <ts e="T290" id="Seg_5041" n="e" s="T289">(Kun-) </ts>
               <ts e="T291" id="Seg_5043" n="e" s="T290">Kundə </ts>
               <ts e="T292" id="Seg_5045" n="e" s="T291">dĭ </ts>
               <ts e="T293" id="Seg_5047" n="e" s="T292">nükem </ts>
               <ts e="T294" id="Seg_5049" n="e" s="T293">bălʼnʼitsanə. </ts>
               <ts e="T295" id="Seg_5051" n="e" s="T294">(Dĭʔnə </ts>
               <ts e="T296" id="Seg_5053" n="e" s="T295">s-) </ts>
               <ts e="T297" id="Seg_5055" n="e" s="T296">Dĭʔnə </ts>
               <ts e="T298" id="Seg_5057" n="e" s="T297">sazən </ts>
               <ts e="T299" id="Seg_5059" n="e" s="T298">mĭbiʔi. </ts>
               <ts e="T300" id="Seg_5061" n="e" s="T299">A </ts>
               <ts e="T301" id="Seg_5063" n="e" s="T300">ej </ts>
               <ts e="T302" id="Seg_5065" n="e" s="T301">(kol- </ts>
               <ts e="T303" id="Seg_5067" n="e" s="T302">ko-) </ts>
               <ts e="T304" id="Seg_5069" n="e" s="T303">kundlal </ts>
               <ts e="T305" id="Seg_5071" n="e" s="T304">dăk, </ts>
               <ts e="T306" id="Seg_5073" n="e" s="T305">kăštʼit </ts>
               <ts e="T307" id="Seg_5075" n="e" s="T306">скорая </ts>
               <ts e="T308" id="Seg_5077" n="e" s="T307">помощь, </ts>
               <ts e="T309" id="Seg_5079" n="e" s="T308">dĭ </ts>
               <ts e="T310" id="Seg_5081" n="e" s="T309">šoləj </ts>
               <ts e="T311" id="Seg_5083" n="e" s="T310">da </ts>
               <ts e="T312" id="Seg_5085" n="e" s="T311">kunnalləj </ts>
               <ts e="T313" id="Seg_5087" n="e" s="T312">dĭm. </ts>
               <ts e="T314" id="Seg_5089" n="e" s="T313">Dăre </ts>
               <ts e="T315" id="Seg_5091" n="e" s="T314">nörbələm </ts>
               <ts e="T316" id="Seg_5093" n="e" s="T315">diʔnə, </ts>
               <ts e="T317" id="Seg_5095" n="e" s="T316">možet </ts>
               <ts e="T318" id="Seg_5097" n="e" s="T317">dĭ </ts>
               <ts e="T319" id="Seg_5099" n="e" s="T318">nʼilgöləj. </ts>
               <ts e="T320" id="Seg_5101" n="e" s="T319">A_to </ts>
               <ts e="T321" id="Seg_5103" n="e" s="T320">vedʼ </ts>
               <ts e="T322" id="Seg_5105" n="e" s="T321">tăn </ts>
               <ts e="T323" id="Seg_5107" n="e" s="T322">kujdərgan </ts>
               <ts e="T324" id="Seg_5109" n="e" s="T323">ibiel, </ts>
               <ts e="T325" id="Seg_5111" n="e" s="T324">a </ts>
               <ts e="T326" id="Seg_5113" n="e" s="T325">il </ts>
               <ts e="T327" id="Seg_5115" n="e" s="T326">bar </ts>
               <ts e="T328" id="Seg_5117" n="e" s="T327">măllia: </ts>
               <ts e="T329" id="Seg_5119" n="e" s="T328">(Kod-) </ts>
               <ts e="T330" id="Seg_5121" n="e" s="T329">Kujdərgan </ts>
               <ts e="T331" id="Seg_5123" n="e" s="T330">ibiel, </ts>
               <ts e="T332" id="Seg_5125" n="e" s="T331">bostə </ts>
               <ts e="T333" id="Seg_5127" n="e" s="T332">ĭmbidə </ts>
               <ts e="T334" id="Seg_5129" n="e" s="T333">ej </ts>
               <ts e="T335" id="Seg_5131" n="e" s="T334">kabažarbia </ts>
               <ts e="T336" id="Seg_5133" n="e" s="T335">dĭ </ts>
               <ts e="T337" id="Seg_5135" n="e" s="T336">nükenə". </ts>
               <ts e="T338" id="Seg_5137" n="e" s="T337">((BRK)). </ts>
               <ts e="T339" id="Seg_5139" n="e" s="T338">A </ts>
               <ts e="T340" id="Seg_5141" n="e" s="T339">abal </ts>
               <ts e="T341" id="Seg_5143" n="e" s="T340">külambi, </ts>
               <ts e="T342" id="Seg_5145" n="e" s="T341">a </ts>
               <ts e="T343" id="Seg_5147" n="e" s="T342">tura </ts>
               <ts e="T344" id="Seg_5149" n="e" s="T343">(nan) </ts>
               <ts e="T345" id="Seg_5151" n="e" s="T344">vedʼ </ts>
               <ts e="T346" id="Seg_5153" n="e" s="T345">tăn </ts>
               <ts e="T347" id="Seg_5155" n="e" s="T346">ibiel. </ts>
               <ts e="T348" id="Seg_5157" n="e" s="T347">((BRK)). </ts>
               <ts e="T349" id="Seg_5159" n="e" s="T348">Men </ts>
               <ts e="T350" id="Seg_5161" n="e" s="T349">unnʼa </ts>
               <ts e="T351" id="Seg_5163" n="e" s="T350">amnobi, </ts>
               <ts e="T352" id="Seg_5165" n="e" s="T351">amnobi. </ts>
               <ts e="T353" id="Seg_5167" n="e" s="T352">Скучно </ts>
               <ts e="T354" id="Seg_5169" n="e" s="T353">unnʼanə </ts>
               <ts e="T355" id="Seg_5171" n="e" s="T354">amnozittə, </ts>
               <ts e="T356" id="Seg_5173" n="e" s="T355">kalam </ts>
               <ts e="T357" id="Seg_5175" n="e" s="T356">ele </ts>
               <ts e="T358" id="Seg_5177" n="e" s="T357">boskəndə </ts>
               <ts e="T359" id="Seg_5179" n="e" s="T358">kulim. </ts>
               <ts e="T360" id="Seg_5181" n="e" s="T359">Kambi, </ts>
               <ts e="T361" id="Seg_5183" n="e" s="T360">kambi, </ts>
               <ts e="T362" id="Seg_5185" n="e" s="T361">kubi </ts>
               <ts e="T363" id="Seg_5187" n="e" s="T362">zajtsəm. </ts>
               <ts e="T364" id="Seg_5189" n="e" s="T363">Davaj </ts>
               <ts e="T365" id="Seg_5191" n="e" s="T364">tănzi </ts>
               <ts e="T366" id="Seg_5193" n="e" s="T365">amnozittə? </ts>
               <ts e="T367" id="Seg_5195" n="e" s="T366">Davaj </ts>
               <ts e="T368" id="Seg_5197" n="e" s="T367">amnozittə. </ts>
               <ts e="T369" id="Seg_5199" n="e" s="T368">Dĭgəttə </ts>
               <ts e="T370" id="Seg_5201" n="e" s="T369">nüdʼi </ts>
               <ts e="T371" id="Seg_5203" n="e" s="T370">molambi, </ts>
               <ts e="T372" id="Seg_5205" n="e" s="T371">dĭzeŋ </ts>
               <ts e="T373" id="Seg_5207" n="e" s="T372">iʔpiʔi </ts>
               <ts e="T374" id="Seg_5209" n="e" s="T373">kunolzittə. </ts>
               <ts e="T375" id="Seg_5211" n="e" s="T374">Zajəs </ts>
               <ts e="T376" id="Seg_5213" n="e" s="T375">kunolluʔpi, </ts>
               <ts e="T377" id="Seg_5215" n="e" s="T376">a </ts>
               <ts e="T378" id="Seg_5217" n="e" s="T377">(mei-) </ts>
               <ts e="T379" id="Seg_5219" n="e" s="T378">men </ts>
               <ts e="T380" id="Seg_5221" n="e" s="T379">ej </ts>
               <ts e="T381" id="Seg_5223" n="e" s="T380">kunollia. </ts>
               <ts e="T382" id="Seg_5225" n="e" s="T381">Nüdʼin </ts>
               <ts e="T383" id="Seg_5227" n="e" s="T382">bar </ts>
               <ts e="T384" id="Seg_5229" n="e" s="T383">kürümleʔbə. </ts>
               <ts e="T385" id="Seg_5231" n="e" s="T384">A </ts>
               <ts e="T386" id="Seg_5233" n="e" s="T385">zajəs </ts>
               <ts e="T387" id="Seg_5235" n="e" s="T386">nereʔluʔpi. </ts>
               <ts e="T388" id="Seg_5237" n="e" s="T387">Sĭjdə </ts>
               <ts e="T389" id="Seg_5239" n="e" s="T388">pʼatkəndə </ts>
               <ts e="T1120" id="Seg_5241" n="e" s="T389">kalla </ts>
               <ts e="T390" id="Seg_5243" n="e" s="T1120">dʼürbi. </ts>
               <ts e="T391" id="Seg_5245" n="e" s="T390">Ĭmbi </ts>
               <ts e="T392" id="Seg_5247" n="e" s="T391">kürümniel? </ts>
               <ts e="T393" id="Seg_5249" n="e" s="T392">Šoləj </ts>
               <ts e="T394" id="Seg_5251" n="e" s="T393">volk </ts>
               <ts e="T395" id="Seg_5253" n="e" s="T394">i </ts>
               <ts e="T396" id="Seg_5255" n="e" s="T395">miʔnʼibeʔ </ts>
               <ts e="T397" id="Seg_5257" n="e" s="T396">amnəj. </ts>
               <ts e="T398" id="Seg_5259" n="e" s="T397">A </ts>
               <ts e="T399" id="Seg_5261" n="e" s="T398">men </ts>
               <ts e="T400" id="Seg_5263" n="e" s="T399">măndə: </ts>
               <ts e="T401" id="Seg_5265" n="e" s="T400">Ej </ts>
               <ts e="T402" id="Seg_5267" n="e" s="T401">jakšə </ts>
               <ts e="T403" id="Seg_5269" n="e" s="T402">măn </ts>
               <ts e="T404" id="Seg_5271" n="e" s="T403">elem. </ts>
               <ts e="T405" id="Seg_5273" n="e" s="T404">Kalam </ts>
               <ts e="T406" id="Seg_5275" n="e" s="T405">(volkə </ts>
               <ts e="T407" id="Seg_5277" n="e" s="T406">is-) </ts>
               <ts e="T408" id="Seg_5279" n="e" s="T407">volk </ts>
               <ts e="T409" id="Seg_5281" n="e" s="T408">(măndərzittə). </ts>
               <ts e="T410" id="Seg_5283" n="e" s="T409">Dĭgəttə </ts>
               <ts e="T411" id="Seg_5285" n="e" s="T410">kambi. </ts>
               <ts e="T412" id="Seg_5287" n="e" s="T411">Šonəga, </ts>
               <ts e="T413" id="Seg_5289" n="e" s="T412">šonəga, </ts>
               <ts e="T414" id="Seg_5291" n="e" s="T413">volk </ts>
               <ts e="T415" id="Seg_5293" n="e" s="T414">dĭʔnə </ts>
               <ts e="T416" id="Seg_5295" n="e" s="T415">šonəga. </ts>
               <ts e="T417" id="Seg_5297" n="e" s="T416">Davaj </ts>
               <ts e="T418" id="Seg_5299" n="e" s="T417">tănzi </ts>
               <ts e="T419" id="Seg_5301" n="e" s="T418">amnozittə? </ts>
               <ts e="T420" id="Seg_5303" n="e" s="T419">Davaj </ts>
               <ts e="T421" id="Seg_5305" n="e" s="T420">amnozittə. </ts>
               <ts e="T422" id="Seg_5307" n="e" s="T421">Nüdʼi </ts>
               <ts e="T423" id="Seg_5309" n="e" s="T422">šobi, </ts>
               <ts e="T424" id="Seg_5311" n="e" s="T423">(dĭ-) </ts>
               <ts e="T425" id="Seg_5313" n="e" s="T424">dĭzeŋ </ts>
               <ts e="T426" id="Seg_5315" n="e" s="T425">iʔpiʔi </ts>
               <ts e="T427" id="Seg_5317" n="e" s="T426">kunolzittə. </ts>
               <ts e="T428" id="Seg_5319" n="e" s="T427">Men </ts>
               <ts e="T429" id="Seg_5321" n="e" s="T428">nüdʼin </ts>
               <ts e="T430" id="Seg_5323" n="e" s="T429">kürümnuʔpi. </ts>
               <ts e="T431" id="Seg_5325" n="e" s="T430">A </ts>
               <ts e="T432" id="Seg_5327" n="e" s="T431">volk </ts>
               <ts e="T433" id="Seg_5329" n="e" s="T432">măndə: </ts>
               <ts e="T434" id="Seg_5331" n="e" s="T433">Ĭmbi </ts>
               <ts e="T435" id="Seg_5333" n="e" s="T434">kürümnaʔbəl? </ts>
               <ts e="T436" id="Seg_5335" n="e" s="T435">Tüj </ts>
               <ts e="T437" id="Seg_5337" n="e" s="T436">urgaːba </ts>
               <ts e="T438" id="Seg_5339" n="e" s="T437">šoləj </ts>
               <ts e="T439" id="Seg_5341" n="e" s="T438">i </ts>
               <ts e="T440" id="Seg_5343" n="e" s="T439">miʔnʼibeʔ </ts>
               <ts e="T441" id="Seg_5345" n="e" s="T440">amnəj. </ts>
               <ts e="T442" id="Seg_5347" n="e" s="T441">Dĭgəttə </ts>
               <ts e="T443" id="Seg_5349" n="e" s="T442">men </ts>
               <ts e="T444" id="Seg_5351" n="e" s="T443">ertən </ts>
               <ts e="T445" id="Seg_5353" n="e" s="T444">uʔbdəbi: </ts>
               <ts e="T446" id="Seg_5355" n="e" s="T445">Ej </ts>
               <ts e="T447" id="Seg_5357" n="e" s="T446">jakše </ts>
               <ts e="T448" id="Seg_5359" n="e" s="T447">elem, </ts>
               <ts e="T449" id="Seg_5361" n="e" s="T448">kalam </ts>
               <ts e="T450" id="Seg_5363" n="e" s="T449">urgaːba </ts>
               <ts e="T451" id="Seg_5365" n="e" s="T450">(mărno-) </ts>
               <ts e="T452" id="Seg_5367" n="e" s="T451">măndərzittə. </ts>
               <ts e="T453" id="Seg_5369" n="e" s="T452">(Dəg-) </ts>
               <ts e="T454" id="Seg_5371" n="e" s="T453">Dĭgəttə </ts>
               <ts e="T455" id="Seg_5373" n="e" s="T454">kambi. </ts>
               <ts e="T456" id="Seg_5375" n="e" s="T455">Šonəga, </ts>
               <ts e="T457" id="Seg_5377" n="e" s="T456">šonəga, </ts>
               <ts e="T458" id="Seg_5379" n="e" s="T457">urgaːba </ts>
               <ts e="T459" id="Seg_5381" n="e" s="T458">šonəga. </ts>
               <ts e="T460" id="Seg_5383" n="e" s="T459">Urgaːba, </ts>
               <ts e="T461" id="Seg_5385" n="e" s="T460">urgaːba, </ts>
               <ts e="T462" id="Seg_5387" n="e" s="T461">davaj </ts>
               <ts e="T463" id="Seg_5389" n="e" s="T462">tănzi </ts>
               <ts e="T464" id="Seg_5391" n="e" s="T463">amnozittə. </ts>
               <ts e="T465" id="Seg_5393" n="e" s="T464">Davaj. </ts>
               <ts e="T466" id="Seg_5395" n="e" s="T465">Dĭgəttə </ts>
               <ts e="T467" id="Seg_5397" n="e" s="T466">nüdʼi </ts>
               <ts e="T468" id="Seg_5399" n="e" s="T467">molambi, </ts>
               <ts e="T469" id="Seg_5401" n="e" s="T468">(iʔpil-) </ts>
               <ts e="T470" id="Seg_5403" n="e" s="T469">iʔpiʔi </ts>
               <ts e="T471" id="Seg_5405" n="e" s="T470">kunolzittə. </ts>
               <ts e="T472" id="Seg_5407" n="e" s="T471">Men </ts>
               <ts e="T473" id="Seg_5409" n="e" s="T472">nüdʼin </ts>
               <ts e="T474" id="Seg_5411" n="e" s="T473">bar </ts>
               <ts e="T475" id="Seg_5413" n="e" s="T474">kürümnuʔpi </ts>
               <ts e="T476" id="Seg_5415" n="e" s="T475">bar. </ts>
               <ts e="T477" id="Seg_5417" n="e" s="T476">A </ts>
               <ts e="T478" id="Seg_5419" n="e" s="T477">urgaːba: </ts>
               <ts e="T479" id="Seg_5421" n="e" s="T478">Ĭmbi </ts>
               <ts e="T480" id="Seg_5423" n="e" s="T479">kürümniel? </ts>
               <ts e="T481" id="Seg_5425" n="e" s="T480">Kuza </ts>
               <ts e="T482" id="Seg_5427" n="e" s="T481">šoləj </ts>
               <ts e="T483" id="Seg_5429" n="e" s="T482">da </ts>
               <ts e="T484" id="Seg_5431" n="e" s="T483">miʔnʼibeʔ </ts>
               <ts e="T485" id="Seg_5433" n="e" s="T484">kutləj. </ts>
               <ts e="T486" id="Seg_5435" n="e" s="T485">A </ts>
               <ts e="T487" id="Seg_5437" n="e" s="T486">dĭ </ts>
               <ts e="T488" id="Seg_5439" n="e" s="T487">măndə: </ts>
               <ts e="T489" id="Seg_5441" n="e" s="T488">Ej </ts>
               <ts e="T490" id="Seg_5443" n="e" s="T489">jakše </ts>
               <ts e="T491" id="Seg_5445" n="e" s="T490">elem, </ts>
               <ts e="T492" id="Seg_5447" n="e" s="T491">kalam </ts>
               <ts e="T493" id="Seg_5449" n="e" s="T492">kuza </ts>
               <ts e="T494" id="Seg_5451" n="e" s="T493">măndərzittə. </ts>
               <ts e="T495" id="Seg_5453" n="e" s="T494">Ertən </ts>
               <ts e="T496" id="Seg_5455" n="e" s="T495">uʔbdəbi, </ts>
               <ts e="T497" id="Seg_5457" n="e" s="T496">kambi </ts>
               <ts e="T498" id="Seg_5459" n="e" s="T497">((…)). </ts>
               <ts e="T499" id="Seg_5461" n="e" s="T498">Bar </ts>
               <ts e="T500" id="Seg_5463" n="e" s="T499">paʔi </ts>
               <ts e="T501" id="Seg_5465" n="e" s="T500">mĭmbi, </ts>
               <ts e="T502" id="Seg_5467" n="e" s="T501">mĭmbi. </ts>
               <ts e="T503" id="Seg_5469" n="e" s="T502">Dĭgəttə </ts>
               <ts e="T504" id="Seg_5471" n="e" s="T503">sʼtʼeptə </ts>
               <ts e="T505" id="Seg_5473" n="e" s="T504">šobi. </ts>
               <ts e="T506" id="Seg_5475" n="e" s="T505">Kuliat </ts>
               <ts e="T507" id="Seg_5477" n="e" s="T506">bar: </ts>
               <ts e="T508" id="Seg_5479" n="e" s="T507">kuza </ts>
               <ts e="T509" id="Seg_5481" n="e" s="T508">šonəga. </ts>
               <ts e="T510" id="Seg_5483" n="e" s="T509">(Paʔ- </ts>
               <ts e="T511" id="Seg_5485" n="e" s="T510">paʔ-) </ts>
               <ts e="T512" id="Seg_5487" n="e" s="T511">Paʔi </ts>
               <ts e="T513" id="Seg_5489" n="e" s="T512">izittə, </ts>
               <ts e="T514" id="Seg_5491" n="e" s="T513">inezi. </ts>
               <ts e="T515" id="Seg_5493" n="e" s="T514">Dĭgəttə </ts>
               <ts e="T516" id="Seg_5495" n="e" s="T515">dĭ </ts>
               <ts e="T517" id="Seg_5497" n="e" s="T516">măndə: </ts>
               <ts e="T518" id="Seg_5499" n="e" s="T517">Kuza, </ts>
               <ts e="T519" id="Seg_5501" n="e" s="T518">kuza, </ts>
               <ts e="T520" id="Seg_5503" n="e" s="T519">davaj </ts>
               <ts e="T521" id="Seg_5505" n="e" s="T520">amnozittə. </ts>
               <ts e="T522" id="Seg_5507" n="e" s="T521">Dĭ </ts>
               <ts e="T523" id="Seg_5509" n="e" s="T522">deʔpi </ts>
               <ts e="T524" id="Seg_5511" n="e" s="T523">dĭm </ts>
               <ts e="T525" id="Seg_5513" n="e" s="T524">maʔndə. </ts>
               <ts e="T526" id="Seg_5515" n="e" s="T525">Iʔbəbiʔi </ts>
               <ts e="T527" id="Seg_5517" n="e" s="T526">kunolzittə. </ts>
               <ts e="T528" id="Seg_5519" n="e" s="T527">Kuza </ts>
               <ts e="T529" id="Seg_5521" n="e" s="T528">kunolluʔpi, </ts>
               <ts e="T530" id="Seg_5523" n="e" s="T529">a </ts>
               <ts e="T531" id="Seg_5525" n="e" s="T530">men </ts>
               <ts e="T532" id="Seg_5527" n="e" s="T531">bar </ts>
               <ts e="T533" id="Seg_5529" n="e" s="T532">davaj </ts>
               <ts e="T534" id="Seg_5531" n="e" s="T533">kürümzittə. </ts>
               <ts e="T535" id="Seg_5533" n="e" s="T534">A </ts>
               <ts e="T536" id="Seg_5535" n="e" s="T535">kuza </ts>
               <ts e="T537" id="Seg_5537" n="e" s="T536">uʔbdəbi, </ts>
               <ts e="T538" id="Seg_5539" n="e" s="T537">măndə: </ts>
               <ts e="T539" id="Seg_5541" n="e" s="T538">Tăn </ts>
               <ts e="T540" id="Seg_5543" n="e" s="T539">püjölial, </ts>
               <ts e="T541" id="Seg_5545" n="e" s="T540">amoraʔ </ts>
               <ts e="T542" id="Seg_5547" n="e" s="T541">da </ts>
               <ts e="T543" id="Seg_5549" n="e" s="T542">kunolaʔ, </ts>
               <ts e="T544" id="Seg_5551" n="e" s="T543">măna </ts>
               <ts e="T545" id="Seg_5553" n="e" s="T544">(deʔ= </ts>
               <ts e="T546" id="Seg_5555" n="e" s="T545">dĭʔ </ts>
               <ts e="T547" id="Seg_5557" n="e" s="T546">-) </ts>
               <ts e="T548" id="Seg_5559" n="e" s="T547">mĭʔ </ts>
               <ts e="T549" id="Seg_5561" n="e" s="T548">kunolzittə. </ts>
               <ts e="T550" id="Seg_5563" n="e" s="T549">Bar! </ts>
               <ts e="T551" id="Seg_5565" n="e" s="T550">((BRK)). </ts>
               <ts e="T552" id="Seg_5567" n="e" s="T551">Onʼiʔ </ts>
               <ts e="T553" id="Seg_5569" n="e" s="T552">kuza </ts>
               <ts e="T554" id="Seg_5571" n="e" s="T553">kambi </ts>
               <ts e="T555" id="Seg_5573" n="e" s="T554">pajlaʔ. </ts>
               <ts e="T556" id="Seg_5575" n="e" s="T555">(Pa-) </ts>
               <ts e="T557" id="Seg_5577" n="e" s="T556">Paʔi </ts>
               <ts e="T558" id="Seg_5579" n="e" s="T557">ibi </ts>
               <ts e="T559" id="Seg_5581" n="e" s="T558">i </ts>
               <ts e="T560" id="Seg_5583" n="e" s="T559">šonəga. </ts>
               <ts e="T561" id="Seg_5585" n="e" s="T560">Dĭʔnə </ts>
               <ts e="T562" id="Seg_5587" n="e" s="T561">urgaːba </ts>
               <ts e="T563" id="Seg_5589" n="e" s="T562">šonəga. </ts>
               <ts e="T564" id="Seg_5591" n="e" s="T563">Măn </ts>
               <ts e="T565" id="Seg_5593" n="e" s="T564">tüjö </ts>
               <ts e="T566" id="Seg_5595" n="e" s="T565">tăn </ts>
               <ts e="T567" id="Seg_5597" n="e" s="T566">amnam. </ts>
               <ts e="T568" id="Seg_5599" n="e" s="T567">Tăn </ts>
               <ts e="T569" id="Seg_5601" n="e" s="T568">iʔ </ts>
               <ts e="T570" id="Seg_5603" n="e" s="T569">amaʔ </ts>
               <ts e="T571" id="Seg_5605" n="e" s="T570">măna. </ts>
               <ts e="T572" id="Seg_5607" n="e" s="T571">Dĭn </ts>
               <ts e="T573" id="Seg_5609" n="e" s="T572">ăxotnikʔi </ts>
               <ts e="T574" id="Seg_5611" n="e" s="T573">šonəgaʔi, </ts>
               <ts e="T575" id="Seg_5613" n="e" s="T574">kutləʔjə </ts>
               <ts e="T576" id="Seg_5615" n="e" s="T575">tănan. </ts>
               <ts e="T577" id="Seg_5617" n="e" s="T576">Dĭ </ts>
               <ts e="T578" id="Seg_5619" n="e" s="T577">bar </ts>
               <ts e="T579" id="Seg_5621" n="e" s="T578">nereʔluʔpi. </ts>
               <ts e="T580" id="Seg_5623" n="e" s="T579">Eneʔ </ts>
               <ts e="T581" id="Seg_5625" n="e" s="T580">măna </ts>
               <ts e="T582" id="Seg_5627" n="e" s="T581">paʔizi. </ts>
               <ts e="T583" id="Seg_5629" n="e" s="T582">Da </ts>
               <ts e="T584" id="Seg_5631" n="e" s="T583">kanaʔ. </ts>
               <ts e="T585" id="Seg_5633" n="e" s="T584">Štobɨ </ts>
               <ts e="T586" id="Seg_5635" n="e" s="T585">dĭzeŋ </ts>
               <ts e="T587" id="Seg_5637" n="e" s="T586">ej </ts>
               <ts e="T588" id="Seg_5639" n="e" s="T587">kubiʔi </ts>
               <ts e="T589" id="Seg_5641" n="e" s="T588">măna. </ts>
               <ts e="T590" id="Seg_5643" n="e" s="T589">A </ts>
               <ts e="T591" id="Seg_5645" n="e" s="T590">dĭ </ts>
               <ts e="T592" id="Seg_5647" n="e" s="T591">mănlia: </ts>
               <ts e="T593" id="Seg_5649" n="e" s="T592">Nada </ts>
               <ts e="T594" id="Seg_5651" n="e" s="T593">enzittə </ts>
               <ts e="T595" id="Seg_5653" n="e" s="T594">dăk </ts>
               <ts e="T596" id="Seg_5655" n="e" s="T595">sarzittə. </ts>
               <ts e="T597" id="Seg_5657" n="e" s="T596">Dĭgəttə </ts>
               <ts e="T598" id="Seg_5659" n="e" s="T597">(dĭ=) </ts>
               <ts e="T599" id="Seg_5661" n="e" s="T598">dĭ </ts>
               <ts e="T600" id="Seg_5663" n="e" s="T599">măndə: </ts>
               <ts e="T601" id="Seg_5665" n="e" s="T600">Saraʔ. </ts>
               <ts e="T602" id="Seg_5667" n="e" s="T601">Dĭ </ts>
               <ts e="T603" id="Seg_5669" n="e" s="T602">sarbi </ts>
               <ts e="T604" id="Seg_5671" n="e" s="T603">dĭm. </ts>
               <ts e="T605" id="Seg_5673" n="e" s="T604">A </ts>
               <ts e="T606" id="Seg_5675" n="e" s="T605">tüj </ts>
               <ts e="T607" id="Seg_5677" n="e" s="T606">nada </ts>
               <ts e="T608" id="Seg_5679" n="e" s="T607">(balt-) </ts>
               <ts e="T609" id="Seg_5681" n="e" s="T608">baltu </ts>
               <ts e="T610" id="Seg_5683" n="e" s="T609">jaʔsittə. </ts>
               <ts e="T611" id="Seg_5685" n="e" s="T610">I </ts>
               <ts e="T612" id="Seg_5687" n="e" s="T611">dĭgəttə </ts>
               <ts e="T613" id="Seg_5689" n="e" s="T612">dĭ </ts>
               <ts e="T614" id="Seg_5691" n="e" s="T613">urgaːbam </ts>
               <ts e="T615" id="Seg_5693" n="e" s="T614">dĭ </ts>
               <ts e="T616" id="Seg_5695" n="e" s="T615">baltuzi </ts>
               <ts e="T617" id="Seg_5697" n="e" s="T616">jaʔluʔpi, </ts>
               <ts e="T618" id="Seg_5699" n="e" s="T617">i </ts>
               <ts e="T619" id="Seg_5701" n="e" s="T618">urgaːba </ts>
               <ts e="T620" id="Seg_5703" n="e" s="T619">külambi. </ts>
               <ts e="T621" id="Seg_5705" n="e" s="T620">((BRK)). </ts>
               <ts e="T622" id="Seg_5707" n="e" s="T621">Urgaːba </ts>
               <ts e="T623" id="Seg_5709" n="e" s="T622">tibizi </ts>
               <ts e="T624" id="Seg_5711" n="e" s="T623">bar </ts>
               <ts e="T625" id="Seg_5713" n="e" s="T624">tugan </ts>
               <ts e="T626" id="Seg_5715" n="e" s="T625">(molab-) </ts>
               <ts e="T627" id="Seg_5717" n="e" s="T626">molaʔpiʔi. </ts>
               <ts e="T628" id="Seg_5719" n="e" s="T627">Davaj </ts>
               <ts e="T629" id="Seg_5721" n="e" s="T628">kuʔsittə </ts>
               <ts e="T630" id="Seg_5723" n="e" s="T629">репа. </ts>
               <ts e="T631" id="Seg_5725" n="e" s="T630">Măna </ts>
               <ts e="T632" id="Seg_5727" n="e" s="T631">kăreškiʔi, </ts>
               <ts e="T633" id="Seg_5729" n="e" s="T632">a </ts>
               <ts e="T634" id="Seg_5731" n="e" s="T633">tănan </ts>
               <ts e="T635" id="Seg_5733" n="e" s="T634">verškiʔi. </ts>
               <ts e="T636" id="Seg_5735" n="e" s="T635">Dĭgəttə </ts>
               <ts e="T637" id="Seg_5737" n="e" s="T636">kuʔpiʔi. </ts>
               <ts e="T638" id="Seg_5739" n="e" s="T637">Dĭ </ts>
               <ts e="T639" id="Seg_5741" n="e" s="T638">özerbi. </ts>
               <ts e="T640" id="Seg_5743" n="e" s="T639">Tibi </ts>
               <ts e="T641" id="Seg_5745" n="e" s="T640">ibi </ts>
               <ts e="T642" id="Seg_5747" n="e" s="T641">kăreškiʔi, </ts>
               <ts e="T643" id="Seg_5749" n="e" s="T642">a </ts>
               <ts e="T644" id="Seg_5751" n="e" s="T643">urgaːba— </ts>
               <ts e="T645" id="Seg_5753" n="e" s="T644">verškiʔi. </ts>
               <ts e="T646" id="Seg_5755" n="e" s="T645">Dĭgəttə </ts>
               <ts e="T647" id="Seg_5757" n="e" s="T646">urgaːba: </ts>
               <ts e="T648" id="Seg_5759" n="e" s="T647">No, </ts>
               <ts e="T649" id="Seg_5761" n="e" s="T648">kuza </ts>
               <ts e="T650" id="Seg_5763" n="e" s="T649">măna </ts>
               <ts e="T651" id="Seg_5765" n="e" s="T650">mʼegluʔpi. </ts>
               <ts e="T652" id="Seg_5767" n="e" s="T651">Dĭgəttə </ts>
               <ts e="T653" id="Seg_5769" n="e" s="T652">bazo </ts>
               <ts e="T654" id="Seg_5771" n="e" s="T653">ejü </ts>
               <ts e="T655" id="Seg_5773" n="e" s="T654">molambi. </ts>
               <ts e="T656" id="Seg_5775" n="e" s="T655">Kuza </ts>
               <ts e="T657" id="Seg_5777" n="e" s="T656">măndə </ts>
               <ts e="T658" id="Seg_5779" n="e" s="T657">(urgaːbanə): </ts>
               <ts e="T659" id="Seg_5781" n="e" s="T658">Davaj </ts>
               <ts e="T660" id="Seg_5783" n="e" s="T659">bazo </ts>
               <ts e="T661" id="Seg_5785" n="e" s="T660">kuʔsittə. </ts>
               <ts e="T662" id="Seg_5787" n="e" s="T661">No, </ts>
               <ts e="T663" id="Seg_5789" n="e" s="T662">urgaːba: </ts>
               <ts e="T664" id="Seg_5791" n="e" s="T663">(Măna=) </ts>
               <ts e="T665" id="Seg_5793" n="e" s="T664">Măna </ts>
               <ts e="T666" id="Seg_5795" n="e" s="T665">deʔ </ts>
               <ts e="T667" id="Seg_5797" n="e" s="T666">kăreškiʔi, </ts>
               <ts e="T668" id="Seg_5799" n="e" s="T667">a </ts>
               <ts e="T669" id="Seg_5801" n="e" s="T668">tănan </ts>
               <ts e="T670" id="Seg_5803" n="e" s="T669">verškiʔi. </ts>
               <ts e="T671" id="Seg_5805" n="e" s="T670">Dĭgəttə </ts>
               <ts e="T672" id="Seg_5807" n="e" s="T671">budəj </ts>
               <ts e="T674" id="Seg_5809" n="e" s="T672">ipek… </ts>
               <ts e="T675" id="Seg_5811" n="e" s="T674">Budəj </ts>
               <ts e="T676" id="Seg_5813" n="e" s="T675">ipek </ts>
               <ts e="T677" id="Seg_5815" n="e" s="T676">kuʔpiʔi. </ts>
               <ts e="T678" id="Seg_5817" n="e" s="T677">Dĭ </ts>
               <ts e="T679" id="Seg_5819" n="e" s="T678">özerbi, </ts>
               <ts e="T680" id="Seg_5821" n="e" s="T679">jakše </ts>
               <ts e="T681" id="Seg_5823" n="e" s="T680">ugandə </ts>
               <ts e="T682" id="Seg_5825" n="e" s="T681">ibi. </ts>
               <ts e="T683" id="Seg_5827" n="e" s="T682">Dĭ </ts>
               <ts e="T684" id="Seg_5829" n="e" s="T683">kuza </ts>
               <ts e="T685" id="Seg_5831" n="e" s="T684">ibi </ts>
               <ts e="T686" id="Seg_5833" n="e" s="T685">verškiʔi, </ts>
               <ts e="T687" id="Seg_5835" n="e" s="T686">a </ts>
               <ts e="T688" id="Seg_5837" n="e" s="T687">urgaːbanə </ts>
               <ts e="T689" id="Seg_5839" n="e" s="T688">kăreškiʔi. </ts>
               <ts e="T690" id="Seg_5841" n="e" s="T689">I </ts>
               <ts e="T691" id="Seg_5843" n="e" s="T690">dĭgəttə </ts>
               <ts e="T692" id="Seg_5845" n="e" s="T691">(dĭ-) </ts>
               <ts e="T693" id="Seg_5847" n="e" s="T692">dĭzen </ts>
               <ts e="T694" id="Seg_5849" n="e" s="T693">bar </ts>
               <ts e="T695" id="Seg_5851" n="e" s="T694">družbat </ts>
               <ts e="T696" id="Seg_5853" n="e" s="T695">külambi. </ts>
               <ts e="T697" id="Seg_5855" n="e" s="T696">((BRK)). </ts>
               <ts e="T698" id="Seg_5857" n="e" s="T697">Amnolaʔpi </ts>
               <ts e="T699" id="Seg_5859" n="e" s="T698">büzʼe </ts>
               <ts e="T700" id="Seg_5861" n="e" s="T699">da </ts>
               <ts e="T701" id="Seg_5863" n="e" s="T700">nüke. </ts>
               <ts e="T702" id="Seg_5865" n="e" s="T701">Dĭzeŋgən </ts>
               <ts e="T703" id="Seg_5867" n="e" s="T702">esseŋdə </ts>
               <ts e="T704" id="Seg_5869" n="e" s="T703">ej </ts>
               <ts e="T705" id="Seg_5871" n="e" s="T704">ibiʔi. </ts>
               <ts e="T706" id="Seg_5873" n="e" s="T705">(Onʼiʔ) </ts>
               <ts e="T707" id="Seg_5875" n="e" s="T706">sĭre </ts>
               <ts e="T708" id="Seg_5877" n="e" s="T707">bar </ts>
               <ts e="T709" id="Seg_5879" n="e" s="T708">saʔməluʔpi, </ts>
               <ts e="T710" id="Seg_5881" n="e" s="T709">ugandə </ts>
               <ts e="T711" id="Seg_5883" n="e" s="T710">urgo. </ts>
               <ts e="T712" id="Seg_5885" n="e" s="T711">Esseŋ </ts>
               <ts e="T713" id="Seg_5887" n="e" s="T712">bar </ts>
               <ts e="T714" id="Seg_5889" n="e" s="T713">girgit </ts>
               <ts e="T715" id="Seg_5891" n="e" s="T714">bar </ts>
               <ts e="T716" id="Seg_5893" n="e" s="T715">(кат </ts>
               <ts e="T717" id="Seg_5895" n="e" s="T716">-) </ts>
               <ts e="T718" id="Seg_5897" n="e" s="T717">катаются, </ts>
               <ts e="T719" id="Seg_5899" n="e" s="T718">girgit </ts>
               <ts e="T720" id="Seg_5901" n="e" s="T719">bar </ts>
               <ts e="T721" id="Seg_5903" n="e" s="T720">sʼarlaʔbəʔjə. </ts>
               <ts e="T722" id="Seg_5905" n="e" s="T721">Snegurkaʔi </ts>
               <ts e="T723" id="Seg_5907" n="e" s="T722">alaʔbəʔjə, </ts>
               <ts e="T724" id="Seg_5909" n="e" s="T723">bar </ts>
               <ts e="T725" id="Seg_5911" n="e" s="T724">alaʔbəʔjə. </ts>
               <ts e="T726" id="Seg_5913" n="e" s="T725">Dĭgəttə </ts>
               <ts e="T727" id="Seg_5915" n="e" s="T726">abiʔi </ts>
               <ts e="T728" id="Seg_5917" n="e" s="T727">urgo </ts>
               <ts e="T729" id="Seg_5919" n="e" s="T728">nüke. </ts>
               <ts e="T730" id="Seg_5921" n="e" s="T729">A </ts>
               <ts e="T731" id="Seg_5923" n="e" s="T730">büzʼe </ts>
               <ts e="T732" id="Seg_5925" n="e" s="T731">üge </ts>
               <ts e="T733" id="Seg_5927" n="e" s="T732">măndobi, </ts>
               <ts e="T734" id="Seg_5929" n="e" s="T733">măndobi: </ts>
               <ts e="T735" id="Seg_5931" n="e" s="T734">Kanžəbəj </ts>
               <ts e="T736" id="Seg_5933" n="e" s="T735">(san-) </ts>
               <ts e="T737" id="Seg_5935" n="e" s="T736">tănzi </ts>
               <ts e="T738" id="Seg_5937" n="e" s="T737">nʼiʔtə,— </ts>
               <ts e="T739" id="Seg_5939" n="e" s="T738">nükenə </ts>
               <ts e="T740" id="Seg_5941" n="e" s="T739">măllia. </ts>
               <ts e="T741" id="Seg_5943" n="e" s="T740">No, </ts>
               <ts e="T742" id="Seg_5945" n="e" s="T741">kanžəbəj. </ts>
               <ts e="T743" id="Seg_5947" n="e" s="T742">(Ka-) </ts>
               <ts e="T744" id="Seg_5949" n="e" s="T743">Kambiʔi, </ts>
               <ts e="T745" id="Seg_5951" n="e" s="T744">măndobiʔi, </ts>
               <ts e="T746" id="Seg_5953" n="e" s="T745">dĭgəttə </ts>
               <ts e="T747" id="Seg_5955" n="e" s="T746">davaj </ts>
               <ts e="T748" id="Seg_5957" n="e" s="T747">azittə </ts>
               <ts e="T749" id="Seg_5959" n="e" s="T748">koʔbdo. </ts>
               <ts e="T751" id="Seg_5961" n="e" s="T749">Abiʔi, </ts>
               <ts e="T752" id="Seg_5963" n="e" s="T751">ulu </ts>
               <ts e="T753" id="Seg_5965" n="e" s="T752">abiʔi, </ts>
               <ts e="T754" id="Seg_5967" n="e" s="T753">simaʔi, </ts>
               <ts e="T755" id="Seg_5969" n="e" s="T754">püjet, </ts>
               <ts e="T756" id="Seg_5971" n="e" s="T755">aŋdə, </ts>
               <ts e="T757" id="Seg_5973" n="e" s="T756">simat. </ts>
               <ts e="T758" id="Seg_5975" n="e" s="T757">Dĭgəttə </ts>
               <ts e="T759" id="Seg_5977" n="e" s="T758">kössi </ts>
               <ts e="T760" id="Seg_5979" n="e" s="T759">bar </ts>
               <ts e="T761" id="Seg_5981" n="e" s="T760">siman </ts>
               <ts e="T762" id="Seg_5983" n="e" s="T761">toʔndə </ts>
               <ts e="T763" id="Seg_5985" n="e" s="T762">dʼüʔpiʔi. </ts>
               <ts e="T764" id="Seg_5987" n="e" s="T763">Dĭgəttə </ts>
               <ts e="T765" id="Seg_5989" n="e" s="T764">dĭ </ts>
               <ts e="T766" id="Seg_5991" n="e" s="T765">simatsi </ts>
               <ts e="T767" id="Seg_5993" n="e" s="T766">mʼeŋgəlluʔpi. </ts>
               <ts e="T768" id="Seg_5995" n="e" s="T767">Dĭgəttə </ts>
               <ts e="T769" id="Seg_5997" n="e" s="T768">büzʼe </ts>
               <ts e="T770" id="Seg_5999" n="e" s="T769">kandlaʔbə. </ts>
               <ts e="T771" id="Seg_6001" n="e" s="T770">(Kam-) </ts>
               <ts e="T772" id="Seg_6003" n="e" s="T771">Kambi </ts>
               <ts e="T773" id="Seg_6005" n="e" s="T772">turanə, </ts>
               <ts e="T774" id="Seg_6007" n="e" s="T773">a </ts>
               <ts e="T775" id="Seg_6009" n="e" s="T774">(dĭ=) </ts>
               <ts e="T776" id="Seg_6011" n="e" s="T775">dĭzeŋ </ts>
               <ts e="T777" id="Seg_6013" n="e" s="T776">(dĭʔ-) </ts>
               <ts e="T778" id="Seg_6015" n="e" s="T777">tože </ts>
               <ts e="T779" id="Seg_6017" n="e" s="T778">dĭzi </ts>
               <ts e="T780" id="Seg_6019" n="e" s="T779">kambiʔi </ts>
               <ts e="T781" id="Seg_6021" n="e" s="T780">turanə. </ts>
               <ts e="T782" id="Seg_6023" n="e" s="T781">Bar </ts>
               <ts e="T783" id="Seg_6025" n="e" s="T782">ugandə </ts>
               <ts e="T784" id="Seg_6027" n="e" s="T783">dĭʔnə </ts>
               <ts e="T785" id="Seg_6029" n="e" s="T784">jakše </ts>
               <ts e="T786" id="Seg_6031" n="e" s="T785">molambi, </ts>
               <ts e="T787" id="Seg_6033" n="e" s="T786">ešši </ts>
               <ts e="T788" id="Seg_6035" n="e" s="T787">(ge) </ts>
               <ts e="T789" id="Seg_6037" n="e" s="T788">bar </ts>
               <ts e="T790" id="Seg_6039" n="e" s="T789">ibiʔi </ts>
               <ts e="T791" id="Seg_6041" n="e" s="T790">dĭʔnə. </ts>
               <ts e="T792" id="Seg_6043" n="e" s="T791">((NOISE)) ((BRK)). </ts>
               <ts e="T793" id="Seg_6045" n="e" s="T792">Dĭgəttə </ts>
               <ts e="T794" id="Seg_6047" n="e" s="T793">dĭʔnə </ts>
               <ts e="T795" id="Seg_6049" n="e" s="T794">jamaʔi </ts>
               <ts e="T796" id="Seg_6051" n="e" s="T795">ibiʔi, </ts>
               <ts e="T797" id="Seg_6053" n="e" s="T796">kuvazəʔi, </ts>
               <ts e="T798" id="Seg_6055" n="e" s="T797">köməʔi. </ts>
               <ts e="T799" id="Seg_6057" n="e" s="T798">Dĭgəttə </ts>
               <ts e="T800" id="Seg_6059" n="e" s="T799">eʔbdənə </ts>
               <ts e="T801" id="Seg_6061" n="e" s="T800">ibiʔi </ts>
               <ts e="T802" id="Seg_6063" n="e" s="T801">лента, </ts>
               <ts e="T803" id="Seg_6065" n="e" s="T802">ugandə </ts>
               <ts e="T804" id="Seg_6067" n="e" s="T803">kuvas, </ts>
               <ts e="T805" id="Seg_6069" n="e" s="T804">атласный. </ts>
               <ts e="T806" id="Seg_6071" n="e" s="T805">(Gəttə </ts>
               <ts e="T807" id="Seg_6073" n="e" s="T806">dĭ=) </ts>
               <ts e="T808" id="Seg_6075" n="e" s="T807">Dĭgəttə </ts>
               <ts e="T809" id="Seg_6077" n="e" s="T808">ejü </ts>
               <ts e="T810" id="Seg_6079" n="e" s="T809">molambi. </ts>
               <ts e="T811" id="Seg_6081" n="e" s="T810">A </ts>
               <ts e="T812" id="Seg_6083" n="e" s="T811">dĭ </ts>
               <ts e="T813" id="Seg_6085" n="e" s="T812">koʔbdo </ts>
               <ts e="T814" id="Seg_6087" n="e" s="T813">amnolaʔbə. </ts>
               <ts e="T815" id="Seg_6089" n="e" s="T814">Bü </ts>
               <ts e="T816" id="Seg_6091" n="e" s="T815">mʼaŋnaʔbə. </ts>
               <ts e="T817" id="Seg_6093" n="e" s="T816">Dĭ </ts>
               <ts e="T818" id="Seg_6095" n="e" s="T817">bar </ts>
               <ts e="T819" id="Seg_6097" n="e" s="T818">pimnie. </ts>
               <ts e="T820" id="Seg_6099" n="e" s="T819">A </ts>
               <ts e="T821" id="Seg_6101" n="e" s="T820">(nüket </ts>
               <ts e="T822" id="Seg_6103" n="e" s="T821">măn-) </ts>
               <ts e="T823" id="Seg_6105" n="e" s="T822">nüke </ts>
               <ts e="T824" id="Seg_6107" n="e" s="T823">măndə: </ts>
               <ts e="T825" id="Seg_6109" n="e" s="T824">Ĭmbi </ts>
               <ts e="T826" id="Seg_6111" n="e" s="T825">tăn </ts>
               <ts e="T827" id="Seg_6113" n="e" s="T826">dʼorlaʔbəl? </ts>
               <ts e="T828" id="Seg_6115" n="e" s="T827">A </ts>
               <ts e="T829" id="Seg_6117" n="e" s="T828">bostən </ts>
               <ts e="T830" id="Seg_6119" n="e" s="T829">bar </ts>
               <ts e="T831" id="Seg_6121" n="e" s="T830">(kă-) </ts>
               <ts e="T832" id="Seg_6123" n="e" s="T831">kăjəldə </ts>
               <ts e="T833" id="Seg_6125" n="e" s="T832">mʼaŋnaʔbəʔjə </ts>
               <ts e="T834" id="Seg_6127" n="e" s="T833">kadəlgən. </ts>
               <ts e="T835" id="Seg_6129" n="e" s="T834">(Ta=) </ts>
               <ts e="T836" id="Seg_6131" n="e" s="T835">Dĭgəttə </ts>
               <ts e="T837" id="Seg_6133" n="e" s="T836">(ej-) </ts>
               <ts e="T838" id="Seg_6135" n="e" s="T837">bazo </ts>
               <ts e="T839" id="Seg_6137" n="e" s="T838">kuja </ts>
               <ts e="T840" id="Seg_6139" n="e" s="T839">molaʔpi. </ts>
               <ts e="T841" id="Seg_6141" n="e" s="T840">Всякий </ts>
               <ts e="T842" id="Seg_6143" n="e" s="T841">noʔ </ts>
               <ts e="T843" id="Seg_6145" n="e" s="T842">özerleʔbəʔjə, </ts>
               <ts e="T844" id="Seg_6147" n="e" s="T843">svʼetogəʔjə. </ts>
               <ts e="T845" id="Seg_6149" n="e" s="T844">Dĭgəttə </ts>
               <ts e="T846" id="Seg_6151" n="e" s="T845">šobiʔi </ts>
               <ts e="T847" id="Seg_6153" n="e" s="T846">koʔbsaŋ: </ts>
               <ts e="T848" id="Seg_6155" n="e" s="T847">Kanžəbəj </ts>
               <ts e="T849" id="Seg_6157" n="e" s="T848">miʔsi! </ts>
               <ts e="T850" id="Seg_6159" n="e" s="T849">Kăštliaʔi </ts>
               <ts e="T851" id="Seg_6161" n="e" s="T850">koʔbdot. </ts>
               <ts e="T852" id="Seg_6163" n="e" s="T851">Nüke </ts>
               <ts e="T853" id="Seg_6165" n="e" s="T852">(măn- </ts>
               <ts e="T854" id="Seg_6167" n="e" s="T853">măn-) </ts>
               <ts e="T855" id="Seg_6169" n="e" s="T854">măllia: </ts>
               <ts e="T856" id="Seg_6171" n="e" s="T855">Kanaʔ, </ts>
               <ts e="T857" id="Seg_6173" n="e" s="T856">ĭmbi </ts>
               <ts e="T858" id="Seg_6175" n="e" s="T857">amnolaʔbəl? </ts>
               <ts e="T859" id="Seg_6177" n="e" s="T858">Dʼok, </ts>
               <ts e="T860" id="Seg_6179" n="e" s="T859">măn </ts>
               <ts e="T861" id="Seg_6181" n="e" s="T860">em </ts>
               <ts e="T862" id="Seg_6183" n="e" s="T861">kanaʔ, </ts>
               <ts e="T863" id="Seg_6185" n="e" s="T862">(kuja=) </ts>
               <ts e="T864" id="Seg_6187" n="e" s="T863">ulum </ts>
               <ts e="T865" id="Seg_6189" n="e" s="T864">bar </ts>
               <ts e="T866" id="Seg_6191" n="e" s="T865">ĭzemnuʔləj. </ts>
               <ts e="T867" id="Seg_6193" n="e" s="T866">(Kuj-) </ts>
               <ts e="T868" id="Seg_6195" n="e" s="T867">Kuja </ts>
               <ts e="T869" id="Seg_6197" n="e" s="T868">nendləj </ts>
               <ts e="T870" id="Seg_6199" n="e" s="T869">ulum. </ts>
               <ts e="T871" id="Seg_6201" n="e" s="T870">No, </ts>
               <ts e="T872" id="Seg_6203" n="e" s="T871">plat </ts>
               <ts e="T873" id="Seg_6205" n="e" s="T872">(ser-) </ts>
               <ts e="T874" id="Seg_6207" n="e" s="T873">šerdə. </ts>
               <ts e="T875" id="Seg_6209" n="e" s="T874">Dĭgəttə </ts>
               <ts e="T876" id="Seg_6211" n="e" s="T875">dĭ </ts>
               <ts e="T877" id="Seg_6213" n="e" s="T876">kambi. </ts>
               <ts e="T878" id="Seg_6215" n="e" s="T877">Bar </ts>
               <ts e="T879" id="Seg_6217" n="e" s="T878">koʔbsaŋ </ts>
               <ts e="T880" id="Seg_6219" n="e" s="T879">sʼarlaʔbəʔjə. </ts>
               <ts e="T881" id="Seg_6221" n="e" s="T880">Svʼetogəʔi </ts>
               <ts e="T882" id="Seg_6223" n="e" s="T881">(nʼi-) </ts>
               <ts e="T883" id="Seg_6225" n="e" s="T882">nĭŋgəleʔbəʔjə. </ts>
               <ts e="T884" id="Seg_6227" n="e" s="T883">A </ts>
               <ts e="T885" id="Seg_6229" n="e" s="T884">dĭ </ts>
               <ts e="T886" id="Seg_6231" n="e" s="T885">(amnolaʔbə) </ts>
               <ts e="T887" id="Seg_6233" n="e" s="T886">((DMG)). </ts>
               <ts e="T889" id="Seg_6235" n="e" s="T887">(-ambi). </ts>
               <ts e="T890" id="Seg_6237" n="e" s="T889">Dĭgəttə </ts>
               <ts e="T891" id="Seg_6239" n="e" s="T890">dĭzeŋ </ts>
               <ts e="T892" id="Seg_6241" n="e" s="T891">embiʔi </ts>
               <ts e="T893" id="Seg_6243" n="e" s="T892">šü </ts>
               <ts e="T894" id="Seg_6245" n="e" s="T893">ugandə </ts>
               <ts e="T895" id="Seg_6247" n="e" s="T894">urgo </ts>
               <ts e="T896" id="Seg_6249" n="e" s="T895">i </ts>
               <ts e="T897" id="Seg_6251" n="e" s="T896">davaj </ts>
               <ts e="T898" id="Seg_6253" n="e" s="T897">nuʔməsittə </ts>
               <ts e="T899" id="Seg_6255" n="e" s="T898">šünə. </ts>
               <ts e="T900" id="Seg_6257" n="e" s="T899">Mănliaʔi: </ts>
               <ts e="T901" id="Seg_6259" n="e" s="T900">Ĭmbi </ts>
               <ts e="T902" id="Seg_6261" n="e" s="T901">tăn </ts>
               <ts e="T903" id="Seg_6263" n="e" s="T902">ej </ts>
               <ts e="T904" id="Seg_6265" n="e" s="T903">suʔmiliel? </ts>
               <ts e="T905" id="Seg_6267" n="e" s="T904">A </ts>
               <ts e="T906" id="Seg_6269" n="e" s="T905">dĭ </ts>
               <ts e="T907" id="Seg_6271" n="e" s="T906">pimnie. </ts>
               <ts e="T908" id="Seg_6273" n="e" s="T907">A </ts>
               <ts e="T909" id="Seg_6275" n="e" s="T908">dĭgəttə </ts>
               <ts e="T910" id="Seg_6277" n="e" s="T909">kak </ts>
               <ts e="T911" id="Seg_6279" n="e" s="T910">dĭm </ts>
               <ts e="T912" id="Seg_6281" n="e" s="T911">pĭštʼerleʔbəʔjə. </ts>
               <ts e="T913" id="Seg_6283" n="e" s="T912">A </ts>
               <ts e="T914" id="Seg_6285" n="e" s="T913">dĭ </ts>
               <ts e="T915" id="Seg_6287" n="e" s="T914">uʔbdəbi, </ts>
               <ts e="T916" id="Seg_6289" n="e" s="T915">(sa-) </ts>
               <ts e="T917" id="Seg_6291" n="e" s="T916">suʔmiluʔpi </ts>
               <ts e="T918" id="Seg_6293" n="e" s="T917">i </ts>
               <ts e="T919" id="Seg_6295" n="e" s="T918">bar </ts>
               <ts e="T920" id="Seg_6297" n="e" s="T919">šüʔən </ts>
               <ts e="T1121" id="Seg_6299" n="e" s="T920">kalla </ts>
               <ts e="T921" id="Seg_6301" n="e" s="T1121">dʼürbi </ts>
               <ts e="T922" id="Seg_6303" n="e" s="T921">nʼuʔdə </ts>
               <ts e="T923" id="Seg_6305" n="e" s="T922">bar. </ts>
               <ts e="T924" id="Seg_6307" n="e" s="T923">((BRK)). </ts>
               <ts e="T925" id="Seg_6309" n="e" s="T924">((BRK)). </ts>
               <ts e="T926" id="Seg_6311" n="e" s="T925">Raz </ts>
               <ts e="T927" id="Seg_6313" n="e" s="T926">buzom, </ts>
               <ts e="T928" id="Seg_6315" n="e" s="T927">(măn) </ts>
               <ts e="T929" id="Seg_6317" n="e" s="T928">ugandə </ts>
               <ts e="T930" id="Seg_6319" n="e" s="T929">kuvas </ts>
               <ts e="T931" id="Seg_6321" n="e" s="T930">(sĭre) </ts>
               <ts e="T932" id="Seg_6323" n="e" s="T931">buzo </ts>
               <ts e="T933" id="Seg_6325" n="e" s="T932">ibi. </ts>
               <ts e="T934" id="Seg_6327" n="e" s="T933">(Dĭgəttə) </ts>
               <ts e="T935" id="Seg_6329" n="e" s="T934">mĭmbi, </ts>
               <ts e="T936" id="Seg_6331" n="e" s="T935">mĭmbi </ts>
               <ts e="T937" id="Seg_6333" n="e" s="T936">(măn) </ts>
               <ts e="T938" id="Seg_6335" n="e" s="T937">nüdʼin </ts>
               <ts e="T939" id="Seg_6337" n="e" s="T938">(bĭtəlzittə) </ts>
               <ts e="T940" id="Seg_6339" n="e" s="T939">(sut-) </ts>
               <ts e="T941" id="Seg_6341" n="e" s="T940">sütsi. </ts>
               <ts e="T942" id="Seg_6343" n="e" s="T941">Kirgariam, </ts>
               <ts e="T943" id="Seg_6345" n="e" s="T942">kirgariam, </ts>
               <ts e="T944" id="Seg_6347" n="e" s="T943">măndərbiam </ts>
               <ts e="T945" id="Seg_6349" n="e" s="T944">măndərbiam, </ts>
               <ts e="T946" id="Seg_6351" n="e" s="T945">gijendə </ts>
               <ts e="T947" id="Seg_6353" n="e" s="T946">naga. </ts>
               <ts e="T948" id="Seg_6355" n="e" s="T947">No </ts>
               <ts e="T949" id="Seg_6357" n="e" s="T948">giraːmbi </ts>
               <ts e="T950" id="Seg_6359" n="e" s="T949">(bu-) </ts>
               <ts e="T951" id="Seg_6361" n="e" s="T950">buzo, </ts>
               <ts e="T952" id="Seg_6363" n="e" s="T951">ej </ts>
               <ts e="T953" id="Seg_6365" n="e" s="T952">tĭmniem. </ts>
               <ts e="T954" id="Seg_6367" n="e" s="T953">Ertən </ts>
               <ts e="T955" id="Seg_6369" n="e" s="T954">uʔbdəbiam, </ts>
               <ts e="T956" id="Seg_6371" n="e" s="T955">vezʼdʼe </ts>
               <ts e="T957" id="Seg_6373" n="e" s="T956">mĭmbiem. </ts>
               <ts e="T958" id="Seg_6375" n="e" s="T957">Dʼagagən </ts>
               <ts e="T959" id="Seg_6377" n="e" s="T958">mĭmbiem, </ts>
               <ts e="T960" id="Seg_6379" n="e" s="T959">tenəbiem </ts>
               <ts e="T961" id="Seg_6381" n="e" s="T960">dĭ </ts>
               <ts e="T962" id="Seg_6383" n="e" s="T961">saʔməluʔpi </ts>
               <ts e="T963" id="Seg_6385" n="e" s="T962">bünə. </ts>
               <ts e="T964" id="Seg_6387" n="e" s="T963">Dĭn </ts>
               <ts e="T965" id="Seg_6389" n="e" s="T964">naga </ts>
               <ts e="T966" id="Seg_6391" n="e" s="T965">vezde. </ts>
               <ts e="T967" id="Seg_6393" n="e" s="T966">Tenəbiem: </ts>
               <ts e="T968" id="Seg_6395" n="e" s="T967">volk </ts>
               <ts e="T969" id="Seg_6397" n="e" s="T968">amnuʔpi. </ts>
               <ts e="T970" id="Seg_6399" n="e" s="T969">Dʼok </ts>
               <ts e="T971" id="Seg_6401" n="e" s="T970">(ej </ts>
               <ts e="T972" id="Seg_6403" n="e" s="T971">volk </ts>
               <ts e="T973" id="Seg_6405" n="e" s="T972">amn-) </ts>
               <ts e="T974" id="Seg_6407" n="e" s="T973">ej </ts>
               <ts e="T975" id="Seg_6409" n="e" s="T974">amnuʔpi. </ts>
               <ts e="T976" id="Seg_6411" n="e" s="T975">Dĭgəttə </ts>
               <ts e="T977" id="Seg_6413" n="e" s="T976">dön </ts>
               <ts e="T978" id="Seg_6415" n="e" s="T977">il </ts>
               <ts e="T979" id="Seg_6417" n="e" s="T978">noʔ </ts>
               <ts e="T980" id="Seg_6419" n="e" s="T979">jaʔpiʔi. </ts>
               <ts e="T981" id="Seg_6421" n="e" s="T980">Dĭzeŋ </ts>
               <ts e="T982" id="Seg_6423" n="e" s="T981">наверное </ts>
               <ts e="T983" id="Seg_6425" n="e" s="T982">ibiʔi, </ts>
               <ts e="T984" id="Seg_6427" n="e" s="T983">üjüttə </ts>
               <ts e="T985" id="Seg_6429" n="e" s="T984">(s-) </ts>
               <ts e="T986" id="Seg_6431" n="e" s="T985">sarbiʔi, </ts>
               <ts e="T987" id="Seg_6433" n="e" s="T986">kumbiʔi </ts>
               <ts e="T988" id="Seg_6435" n="e" s="T987">maʔndə. </ts>
               <ts e="T989" id="Seg_6437" n="e" s="T988">Šide </ts>
               <ts e="T990" id="Seg_6439" n="e" s="T989">üjütsi </ts>
               <ts e="T991" id="Seg_6441" n="e" s="T990">volk </ts>
               <ts e="T992" id="Seg_6443" n="e" s="T991">ibi. </ts>
               <ts e="T993" id="Seg_6445" n="e" s="T992">Măn </ts>
               <ts e="T994" id="Seg_6447" n="e" s="T993">(s-) </ts>
               <ts e="T995" id="Seg_6449" n="e" s="T994">tuganbə </ts>
               <ts e="T996" id="Seg_6451" n="e" s="T995">šobi </ts>
               <ts e="T997" id="Seg_6453" n="e" s="T996">Kanoklerdə). </ts>
               <ts e="T998" id="Seg_6455" n="e" s="T997">I </ts>
               <ts e="T999" id="Seg_6457" n="e" s="T998">nörbəlie: </ts>
               <ts e="T1000" id="Seg_6459" n="e" s="T999">(t-) </ts>
               <ts e="T1001" id="Seg_6461" n="e" s="T1000">šide </ts>
               <ts e="T1002" id="Seg_6463" n="e" s="T1001">tüžöj </ts>
               <ts e="T1003" id="Seg_6465" n="e" s="T1002">ibi </ts>
               <ts e="T1004" id="Seg_6467" n="e" s="T1003">(ilgən). </ts>
               <ts e="T1005" id="Seg_6469" n="e" s="T1004">(Ugandə </ts>
               <ts e="T1006" id="Seg_6471" n="e" s="T1005">il </ts>
               <ts e="T1007" id="Seg_6473" n="e" s="T1006">i- </ts>
               <ts e="T1008" id="Seg_6475" n="e" s="T1007">il=) </ts>
               <ts e="T1009" id="Seg_6477" n="e" s="T1008">Ugandə </ts>
               <ts e="T1010" id="Seg_6479" n="e" s="T1009">(š-) </ts>
               <ts e="T1011" id="Seg_6481" n="e" s="T1010">sildə </ts>
               <ts e="T1012" id="Seg_6483" n="e" s="T1011">iʔgö. </ts>
               <ts e="T1013" id="Seg_6485" n="e" s="T1012">A </ts>
               <ts e="T1014" id="Seg_6487" n="e" s="T1013">miʔnʼibeʔ </ts>
               <ts e="T1015" id="Seg_6489" n="e" s="T1014">amnoləj </ts>
               <ts e="T1016" id="Seg_6491" n="e" s="T1015">bar. </ts>
               <ts e="T1017" id="Seg_6493" n="e" s="T1016">Ĭmbi </ts>
               <ts e="T1018" id="Seg_6495" n="e" s="T1017">todam </ts>
               <ts e="T1019" id="Seg_6497" n="e" s="T1018">(molaʔjə). </ts>
               <ts e="T1020" id="Seg_6499" n="e" s="T1019">Noʔ </ts>
               <ts e="T1021" id="Seg_6501" n="e" s="T1020">ej </ts>
               <ts e="T1022" id="Seg_6503" n="e" s="T1021">amnia, </ts>
               <ts e="T1023" id="Seg_6505" n="e" s="T1022">bü </ts>
               <ts e="T1024" id="Seg_6507" n="e" s="T1023">ej </ts>
               <ts e="T1025" id="Seg_6509" n="e" s="T1024">bĭtlie. </ts>
               <ts e="T1026" id="Seg_6511" n="e" s="T1025">Amga </ts>
               <ts e="T1027" id="Seg_6513" n="e" s="T1026">amnaʔbə. </ts>
               <ts e="T1028" id="Seg_6515" n="e" s="T1027">Baška </ts>
               <ts e="T1029" id="Seg_6517" n="e" s="T1028">tüžöj </ts>
               <ts e="T1030" id="Seg_6519" n="e" s="T1029">šoləʔjə, </ts>
               <ts e="T1031" id="Seg_6521" n="e" s="T1030">bar </ts>
               <ts e="T1032" id="Seg_6523" n="e" s="T1031">noʔ </ts>
               <ts e="T1033" id="Seg_6525" n="e" s="T1032">amnuʔləʔjə, </ts>
               <ts e="T1034" id="Seg_6527" n="e" s="T1033">a </ts>
               <ts e="T1035" id="Seg_6529" n="e" s="T1034">dĭ </ts>
               <ts e="T1036" id="Seg_6531" n="e" s="T1035">ej </ts>
               <ts e="T1037" id="Seg_6533" n="e" s="T1036">amnia. </ts>
               <ts e="T1038" id="Seg_6535" n="e" s="T1037">((BRK)). </ts>
               <ts e="T1039" id="Seg_6537" n="e" s="T1038">"Ej </ts>
               <ts e="T1040" id="Seg_6539" n="e" s="T1039">tĭmnem",— </ts>
               <ts e="T1041" id="Seg_6541" n="e" s="T1040">măndə. </ts>
               <ts e="T1042" id="Seg_6543" n="e" s="T1041">Šeden </ts>
               <ts e="T1043" id="Seg_6545" n="e" s="T1042">dĭrgit </ts>
               <ts e="T1044" id="Seg_6547" n="e" s="T1043">ali </ts>
               <ts e="T1045" id="Seg_6549" n="e" s="T1044">ĭmbi </ts>
               <ts e="T1046" id="Seg_6551" n="e" s="T1045">dĭrgit. </ts>
               <ts e="T1047" id="Seg_6553" n="e" s="T1046">Miʔ </ts>
               <ts e="T1048" id="Seg_6555" n="e" s="T1047">ibibeʔ </ts>
               <ts e="T1049" id="Seg_6557" n="e" s="T1048">tĭn </ts>
               <ts e="T1050" id="Seg_6559" n="e" s="T1049">(se-) </ts>
               <ts e="T1051" id="Seg_6561" n="e" s="T1050">sʼestrandə </ts>
               <ts e="T1052" id="Seg_6563" n="e" s="T1051">tüžöjdə. </ts>
               <ts e="T1053" id="Seg_6565" n="e" s="T1052">Dĭ </ts>
               <ts e="T1054" id="Seg_6567" n="e" s="T1053">vedʼ </ts>
               <ts e="T1055" id="Seg_6569" n="e" s="T1054">ĭmbidə </ts>
               <ts e="T1056" id="Seg_6571" n="e" s="T1055">ej </ts>
               <ts e="T1057" id="Seg_6573" n="e" s="T1056">tĭmnet. </ts>
               <ts e="T1058" id="Seg_6575" n="e" s="T1057">Tože </ts>
               <ts e="T1059" id="Seg_6577" n="e" s="T1058">tüžöj </ts>
               <ts e="T1060" id="Seg_6579" n="e" s="T1059">dăre </ts>
               <ts e="T1061" id="Seg_6581" n="e" s="T1060">molambi </ts>
               <ts e="T1062" id="Seg_6583" n="e" s="T1061">bar, </ts>
               <ts e="T1063" id="Seg_6585" n="e" s="T1062">todam </ts>
               <ts e="T1064" id="Seg_6587" n="e" s="T1063">mobi. </ts>
               <ts e="T1065" id="Seg_6589" n="e" s="T1064">A </ts>
               <ts e="T1066" id="Seg_6591" n="e" s="T1065">(boʔ) </ts>
               <ts e="T1067" id="Seg_6593" n="e" s="T1066">sildə </ts>
               <ts e="T1068" id="Seg_6595" n="e" s="T1067">iʔgö </ts>
               <ts e="T1069" id="Seg_6597" n="e" s="T1068">(m-) </ts>
               <ts e="T1070" id="Seg_6599" n="e" s="T1069">ibi. </ts>
               <ts e="T1071" id="Seg_6601" n="e" s="T1070">Amnobi </ts>
               <ts e="T1072" id="Seg_6603" n="e" s="T1071">miʔnʼibeʔ, </ts>
               <ts e="T1073" id="Seg_6605" n="e" s="T1072">(tona-) </ts>
               <ts e="T1074" id="Seg_6607" n="e" s="T1073">todam </ts>
               <ts e="T1075" id="Seg_6609" n="e" s="T1074">molambi. </ts>
               <ts e="T1076" id="Seg_6611" n="e" s="T1075">((BRK)). </ts>
               <ts e="T1077" id="Seg_6613" n="e" s="T1076">Dĭ </ts>
               <ts e="T1078" id="Seg_6615" n="e" s="T1077">onʼiʔ </ts>
               <ts e="T1079" id="Seg_6617" n="e" s="T1078">koʔbdozi </ts>
               <ts e="T1080" id="Seg_6619" n="e" s="T1079">amnolaʔpi, </ts>
               <ts e="T1081" id="Seg_6621" n="e" s="T1080">dĭgəttə </ts>
               <ts e="T1082" id="Seg_6623" n="e" s="T1081">(baš-) </ts>
               <ts e="T1083" id="Seg_6625" n="e" s="T1082">baska </ts>
               <ts e="T1084" id="Seg_6627" n="e" s="T1083">ibi. </ts>
               <ts e="T1085" id="Seg_6629" n="e" s="T1084">Možet </ts>
               <ts e="T1086" id="Seg_6631" n="e" s="T1085">dĭ </ts>
               <ts e="T1087" id="Seg_6633" n="e" s="T1086">(чё-) </ts>
               <ts e="T1088" id="Seg_6635" n="e" s="T1087">ĭmbi-nʼibudʼ </ts>
               <ts e="T1089" id="Seg_6637" n="e" s="T1088">(tĭ-) </ts>
               <ts e="T1090" id="Seg_6639" n="e" s="T1089">tĭlbi? </ts>
               <ts e="T1091" id="Seg_6641" n="e" s="T1090">I </ts>
               <ts e="T1092" id="Seg_6643" n="e" s="T1091">tüj </ts>
               <ts e="T1093" id="Seg_6645" n="e" s="T1092">(tüžej-) </ts>
               <ts e="T1094" id="Seg_6647" n="e" s="T1093">tüžöjdə </ts>
               <ts e="T1095" id="Seg_6649" n="e" s="T1094">bar </ts>
               <ts e="T1096" id="Seg_6651" n="e" s="T1095">ej </ts>
               <ts e="T1097" id="Seg_6653" n="e" s="T1096">nuliaʔi. </ts>
               <ts e="T1098" id="Seg_6655" n="e" s="T1097">((BRK)). </ts>
               <ts e="T1099" id="Seg_6657" n="e" s="T1098">Šiʔ </ts>
               <ts e="T1100" id="Seg_6659" n="e" s="T1099">(var-) </ts>
               <ts e="T1101" id="Seg_6661" n="e" s="T1100">bar </ts>
               <ts e="T1102" id="Seg_6663" n="e" s="T1101">karəldʼan </ts>
               <ts e="T1103" id="Seg_6665" n="e" s="T1102">(kala-) </ts>
               <ts e="T1104" id="Seg_6667" n="e" s="T1103">((DMG)). </ts>
               <ts e="T1105" id="Seg_6669" n="e" s="T1104">Ši </ts>
               <ts e="T1106" id="Seg_6671" n="e" s="T1105">karəldʼan </ts>
               <ts e="T1122" id="Seg_6673" n="e" s="T1106">kalla </ts>
               <ts e="T1107" id="Seg_6675" n="e" s="T1122">dʼürləbeʔ. </ts>
               <ts e="T1123" id="Seg_6677" n="e" s="T1107">(Kalla </ts>
               <ts e="T1108" id="Seg_6679" n="e" s="T1123">dʼürləleʔ), </ts>
               <ts e="T1109" id="Seg_6681" n="e" s="T1108">davajtʼe </ts>
               <ts e="T1110" id="Seg_6683" n="e" s="T1109">(kamrolabaʔ). </ts>
               <ts e="T1111" id="Seg_6685" n="e" s="T1110">I </ts>
               <ts e="T1112" id="Seg_6687" n="e" s="T1111">pănarlabaʔ. </ts>
               <ts e="T1113" id="Seg_6689" n="e" s="T1112">Dĭgəttə </ts>
               <ts e="T1114" id="Seg_6691" n="e" s="T1113">kangaʔ </ts>
               <ts e="T1115" id="Seg_6693" n="e" s="T1114">kudajzi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T8" id="Seg_6694" s="T0">PKZ_196X_SU0224.001 (001)</ta>
            <ta e="T10" id="Seg_6695" s="T8">PKZ_196X_SU0224.002 (002)</ta>
            <ta e="T11" id="Seg_6696" s="T10">PKZ_196X_SU0224.003 (003)</ta>
            <ta e="T17" id="Seg_6697" s="T11">PKZ_196X_SU0224.004 (004)</ta>
            <ta e="T19" id="Seg_6698" s="T17">PKZ_196X_SU0224.005 (005)</ta>
            <ta e="T20" id="Seg_6699" s="T19">PKZ_196X_SU0224.006 (006)</ta>
            <ta e="T33" id="Seg_6700" s="T20">PKZ_196X_SU0224.007 (007)</ta>
            <ta e="T36" id="Seg_6701" s="T33">PKZ_196X_SU0224.008 (008)</ta>
            <ta e="T41" id="Seg_6702" s="T36">PKZ_196X_SU0224.009 (009)</ta>
            <ta e="T47" id="Seg_6703" s="T41">PKZ_196X_SU0224.010 (010)</ta>
            <ta e="T57" id="Seg_6704" s="T47">PKZ_196X_SU0224.011 (011)</ta>
            <ta e="T60" id="Seg_6705" s="T57">PKZ_196X_SU0224.012 (012)</ta>
            <ta e="T61" id="Seg_6706" s="T60">PKZ_196X_SU0224.013 (013)</ta>
            <ta e="T66" id="Seg_6707" s="T61">PKZ_196X_SU0224.014 (014)</ta>
            <ta e="T70" id="Seg_6708" s="T66">PKZ_196X_SU0224.015 (015)</ta>
            <ta e="T74" id="Seg_6709" s="T70">PKZ_196X_SU0224.016 (016)</ta>
            <ta e="T78" id="Seg_6710" s="T74">PKZ_196X_SU0224.017 (017)</ta>
            <ta e="T84" id="Seg_6711" s="T78">PKZ_196X_SU0224.018 (018)</ta>
            <ta e="T85" id="Seg_6712" s="T84">PKZ_196X_SU0224.019 (019)</ta>
            <ta e="T90" id="Seg_6713" s="T85">PKZ_196X_SU0224.020 (020)</ta>
            <ta e="T93" id="Seg_6714" s="T90">PKZ_196X_SU0224.021 (021)</ta>
            <ta e="T94" id="Seg_6715" s="T93">PKZ_196X_SU0224.022 (022)</ta>
            <ta e="T98" id="Seg_6716" s="T94">PKZ_196X_SU0224.023 (023)</ta>
            <ta e="T103" id="Seg_6717" s="T98">PKZ_196X_SU0224.024 (024)</ta>
            <ta e="T109" id="Seg_6718" s="T103">PKZ_196X_SU0224.025 (025)</ta>
            <ta e="T110" id="Seg_6719" s="T109">PKZ_196X_SU0224.026 (026)</ta>
            <ta e="T115" id="Seg_6720" s="T110">PKZ_196X_SU0224.027 (027)</ta>
            <ta e="T116" id="Seg_6721" s="T115">PKZ_196X_SU0224.028 (028)</ta>
            <ta e="T117" id="Seg_6722" s="T116">PKZ_196X_SU0224.029 (029)</ta>
            <ta e="T123" id="Seg_6723" s="T117">PKZ_196X_SU0224.030 (030)</ta>
            <ta e="T129" id="Seg_6724" s="T123">PKZ_196X_SU0224.031 (031)</ta>
            <ta e="T133" id="Seg_6725" s="T129">PKZ_196X_SU0224.032 (032)</ta>
            <ta e="T144" id="Seg_6726" s="T133">PKZ_196X_SU0224.033 (033)</ta>
            <ta e="T145" id="Seg_6727" s="T144">PKZ_196X_SU0224.034 (034)</ta>
            <ta e="T150" id="Seg_6728" s="T145">PKZ_196X_SU0224.035 (035)</ta>
            <ta e="T151" id="Seg_6729" s="T150">PKZ_196X_SU0224.036 (036)</ta>
            <ta e="T156" id="Seg_6730" s="T151">PKZ_196X_SU0224.037 (037)</ta>
            <ta e="T165" id="Seg_6731" s="T156">PKZ_196X_SU0224.038 (038) </ta>
            <ta e="T172" id="Seg_6732" s="T165">PKZ_196X_SU0224.039 (040) </ta>
            <ta e="T175" id="Seg_6733" s="T172">PKZ_196X_SU0224.040 (042)</ta>
            <ta e="T176" id="Seg_6734" s="T175">PKZ_196X_SU0224.041 (043)</ta>
            <ta e="T181" id="Seg_6735" s="T176">PKZ_196X_SU0224.042 (044)</ta>
            <ta e="T184" id="Seg_6736" s="T181">PKZ_196X_SU0224.043 (045)</ta>
            <ta e="T190" id="Seg_6737" s="T184">PKZ_196X_SU0224.044 (046)</ta>
            <ta e="T195" id="Seg_6738" s="T190">PKZ_196X_SU0224.045 (047)</ta>
            <ta e="T203" id="Seg_6739" s="T195">PKZ_196X_SU0224.046 (048)</ta>
            <ta e="T204" id="Seg_6740" s="T203">PKZ_196X_SU0224.047 (049)</ta>
            <ta e="T218" id="Seg_6741" s="T204">PKZ_196X_SU0224.048 (050) </ta>
            <ta e="T229" id="Seg_6742" s="T218">PKZ_196X_SU0224.049 (052)</ta>
            <ta e="T230" id="Seg_6743" s="T229">PKZ_196X_SU0224.050 (053)</ta>
            <ta e="T242" id="Seg_6744" s="T230">PKZ_196X_SU0224.051 (054)</ta>
            <ta e="T244" id="Seg_6745" s="T242">PKZ_196X_SU0224.052 (055)</ta>
            <ta e="T245" id="Seg_6746" s="T244">PKZ_196X_SU0224.053 (056)</ta>
            <ta e="T249" id="Seg_6747" s="T245">PKZ_196X_SU0224.054 (057)</ta>
            <ta e="T252" id="Seg_6748" s="T249">PKZ_196X_SU0224.055 (058)</ta>
            <ta e="T259" id="Seg_6749" s="T252">PKZ_196X_SU0224.056 (059)</ta>
            <ta e="T261" id="Seg_6750" s="T259">PKZ_196X_SU0224.057 (060)</ta>
            <ta e="T269" id="Seg_6751" s="T261">PKZ_196X_SU0224.058 (061)</ta>
            <ta e="T274" id="Seg_6752" s="T269">PKZ_196X_SU0224.059 (062)</ta>
            <ta e="T275" id="Seg_6753" s="T274">PKZ_196X_SU0224.060 (063)</ta>
            <ta e="T277" id="Seg_6754" s="T275">PKZ_196X_SU0224.061 (064)</ta>
            <ta e="T278" id="Seg_6755" s="T277">PKZ_196X_SU0224.062 (065)</ta>
            <ta e="T282" id="Seg_6756" s="T278">PKZ_196X_SU0224.063 (066)</ta>
            <ta e="T288" id="Seg_6757" s="T282">PKZ_196X_SU0224.064 (067)</ta>
            <ta e="T294" id="Seg_6758" s="T288">PKZ_196X_SU0224.065 (068) </ta>
            <ta e="T299" id="Seg_6759" s="T294">PKZ_196X_SU0224.066 (070)</ta>
            <ta e="T313" id="Seg_6760" s="T299">PKZ_196X_SU0224.067 (071)</ta>
            <ta e="T319" id="Seg_6761" s="T313">PKZ_196X_SU0224.068 (072)</ta>
            <ta e="T337" id="Seg_6762" s="T319">PKZ_196X_SU0224.069 (073) </ta>
            <ta e="T338" id="Seg_6763" s="T337">PKZ_196X_SU0224.070 (075)</ta>
            <ta e="T347" id="Seg_6764" s="T338">PKZ_196X_SU0224.071 (076)</ta>
            <ta e="T348" id="Seg_6765" s="T347">PKZ_196X_SU0224.072 (077)</ta>
            <ta e="T352" id="Seg_6766" s="T348">PKZ_196X_SU0224.073 (078)</ta>
            <ta e="T359" id="Seg_6767" s="T352">PKZ_196X_SU0224.074 (079)</ta>
            <ta e="T363" id="Seg_6768" s="T359">PKZ_196X_SU0224.075 (080)</ta>
            <ta e="T366" id="Seg_6769" s="T363">PKZ_196X_SU0224.076 (081)</ta>
            <ta e="T368" id="Seg_6770" s="T366">PKZ_196X_SU0224.077 (082)</ta>
            <ta e="T374" id="Seg_6771" s="T368">PKZ_196X_SU0224.078 (083)</ta>
            <ta e="T381" id="Seg_6772" s="T374">PKZ_196X_SU0224.079 (084)</ta>
            <ta e="T384" id="Seg_6773" s="T381">PKZ_196X_SU0224.080 (085)</ta>
            <ta e="T387" id="Seg_6774" s="T384">PKZ_196X_SU0224.081 (086)</ta>
            <ta e="T390" id="Seg_6775" s="T387">PKZ_196X_SU0224.082 (087)</ta>
            <ta e="T392" id="Seg_6776" s="T390">PKZ_196X_SU0224.083 (088)</ta>
            <ta e="T397" id="Seg_6777" s="T392">PKZ_196X_SU0224.084 (089)</ta>
            <ta e="T404" id="Seg_6778" s="T397">PKZ_196X_SU0224.085 (090)</ta>
            <ta e="T409" id="Seg_6779" s="T404">PKZ_196X_SU0224.086 (091)</ta>
            <ta e="T411" id="Seg_6780" s="T409">PKZ_196X_SU0224.087 (092)</ta>
            <ta e="T416" id="Seg_6781" s="T411">PKZ_196X_SU0224.088 (093)</ta>
            <ta e="T419" id="Seg_6782" s="T416">PKZ_196X_SU0224.089 (094)</ta>
            <ta e="T421" id="Seg_6783" s="T419">PKZ_196X_SU0224.090 (095)</ta>
            <ta e="T427" id="Seg_6784" s="T421">PKZ_196X_SU0224.091 (096)</ta>
            <ta e="T430" id="Seg_6785" s="T427">PKZ_196X_SU0224.092 (097)</ta>
            <ta e="T435" id="Seg_6786" s="T430">PKZ_196X_SU0224.093 (098)</ta>
            <ta e="T441" id="Seg_6787" s="T435">PKZ_196X_SU0224.094 (099)</ta>
            <ta e="T452" id="Seg_6788" s="T441">PKZ_196X_SU0224.095 (100) </ta>
            <ta e="T455" id="Seg_6789" s="T452">PKZ_196X_SU0224.096 (102)</ta>
            <ta e="T459" id="Seg_6790" s="T455">PKZ_196X_SU0224.097 (103)</ta>
            <ta e="T464" id="Seg_6791" s="T459">PKZ_196X_SU0224.098 (104)</ta>
            <ta e="T465" id="Seg_6792" s="T464">PKZ_196X_SU0224.099 (105)</ta>
            <ta e="T471" id="Seg_6793" s="T465">PKZ_196X_SU0224.100 (106)</ta>
            <ta e="T476" id="Seg_6794" s="T471">PKZ_196X_SU0224.101 (107)</ta>
            <ta e="T480" id="Seg_6795" s="T476">PKZ_196X_SU0224.102 (108)</ta>
            <ta e="T485" id="Seg_6796" s="T480">PKZ_196X_SU0224.103 (109)</ta>
            <ta e="T494" id="Seg_6797" s="T485">PKZ_196X_SU0224.104 (110)</ta>
            <ta e="T498" id="Seg_6798" s="T494">PKZ_196X_SU0224.105 (111)</ta>
            <ta e="T502" id="Seg_6799" s="T498">PKZ_196X_SU0224.106 (112)</ta>
            <ta e="T505" id="Seg_6800" s="T502">PKZ_196X_SU0224.107 (113)</ta>
            <ta e="T509" id="Seg_6801" s="T505">PKZ_196X_SU0224.108 (114)</ta>
            <ta e="T514" id="Seg_6802" s="T509">PKZ_196X_SU0224.109 (115)</ta>
            <ta e="T521" id="Seg_6803" s="T514">PKZ_196X_SU0224.110 (116)</ta>
            <ta e="T525" id="Seg_6804" s="T521">PKZ_196X_SU0224.111 (117)</ta>
            <ta e="T527" id="Seg_6805" s="T525">PKZ_196X_SU0224.112 (118)</ta>
            <ta e="T534" id="Seg_6806" s="T527">PKZ_196X_SU0224.113 (119)</ta>
            <ta e="T549" id="Seg_6807" s="T534">PKZ_196X_SU0224.114 (120) </ta>
            <ta e="T550" id="Seg_6808" s="T549">PKZ_196X_SU0224.115 (122)</ta>
            <ta e="T551" id="Seg_6809" s="T550">PKZ_196X_SU0224.116 (123)</ta>
            <ta e="T555" id="Seg_6810" s="T551">PKZ_196X_SU0224.117 (124)</ta>
            <ta e="T560" id="Seg_6811" s="T555">PKZ_196X_SU0224.118 (125)</ta>
            <ta e="T563" id="Seg_6812" s="T560">PKZ_196X_SU0224.119 (126)</ta>
            <ta e="T567" id="Seg_6813" s="T563">PKZ_196X_SU0224.120 (127)</ta>
            <ta e="T571" id="Seg_6814" s="T567">PKZ_196X_SU0224.121 (128)</ta>
            <ta e="T576" id="Seg_6815" s="T571">PKZ_196X_SU0224.122 (129)</ta>
            <ta e="T579" id="Seg_6816" s="T576">PKZ_196X_SU0224.123 (130)</ta>
            <ta e="T582" id="Seg_6817" s="T579">PKZ_196X_SU0224.124 (131)</ta>
            <ta e="T584" id="Seg_6818" s="T582">PKZ_196X_SU0224.125 (132)</ta>
            <ta e="T589" id="Seg_6819" s="T584">PKZ_196X_SU0224.126 (133)</ta>
            <ta e="T596" id="Seg_6820" s="T589">PKZ_196X_SU0224.127 (134)</ta>
            <ta e="T601" id="Seg_6821" s="T596">PKZ_196X_SU0224.128 (135)</ta>
            <ta e="T604" id="Seg_6822" s="T601">PKZ_196X_SU0224.129 (136)</ta>
            <ta e="T610" id="Seg_6823" s="T604">PKZ_196X_SU0224.130 (137)</ta>
            <ta e="T620" id="Seg_6824" s="T610">PKZ_196X_SU0224.131 (138)</ta>
            <ta e="T621" id="Seg_6825" s="T620">PKZ_196X_SU0224.132 (139)</ta>
            <ta e="T627" id="Seg_6826" s="T621">PKZ_196X_SU0224.133 (140)</ta>
            <ta e="T630" id="Seg_6827" s="T627">PKZ_196X_SU0224.134 (141)</ta>
            <ta e="T635" id="Seg_6828" s="T630">PKZ_196X_SU0224.135 (142)</ta>
            <ta e="T637" id="Seg_6829" s="T635">PKZ_196X_SU0224.136 (143)</ta>
            <ta e="T639" id="Seg_6830" s="T637">PKZ_196X_SU0224.137 (144)</ta>
            <ta e="T645" id="Seg_6831" s="T639">PKZ_196X_SU0224.138 (145)</ta>
            <ta e="T651" id="Seg_6832" s="T645">PKZ_196X_SU0224.139 (146)</ta>
            <ta e="T655" id="Seg_6833" s="T651">PKZ_196X_SU0224.140 (147)</ta>
            <ta e="T661" id="Seg_6834" s="T655">PKZ_196X_SU0224.141 (148)</ta>
            <ta e="T670" id="Seg_6835" s="T661">PKZ_196X_SU0224.142 (149)</ta>
            <ta e="T674" id="Seg_6836" s="T670">PKZ_196X_SU0224.143 (150)</ta>
            <ta e="T677" id="Seg_6837" s="T674">PKZ_196X_SU0224.144 (151)</ta>
            <ta e="T682" id="Seg_6838" s="T677">PKZ_196X_SU0224.145 (152)</ta>
            <ta e="T689" id="Seg_6839" s="T682">PKZ_196X_SU0224.146 (153)</ta>
            <ta e="T696" id="Seg_6840" s="T689">PKZ_196X_SU0224.147 (154)</ta>
            <ta e="T697" id="Seg_6841" s="T696">PKZ_196X_SU0224.148 (155)</ta>
            <ta e="T701" id="Seg_6842" s="T697">PKZ_196X_SU0224.149 (156)</ta>
            <ta e="T705" id="Seg_6843" s="T701">PKZ_196X_SU0224.150 (157)</ta>
            <ta e="T711" id="Seg_6844" s="T705">PKZ_196X_SU0224.151 (158)</ta>
            <ta e="T721" id="Seg_6845" s="T711">PKZ_196X_SU0224.152 (159)</ta>
            <ta e="T725" id="Seg_6846" s="T721">PKZ_196X_SU0224.153 (160)</ta>
            <ta e="T729" id="Seg_6847" s="T725">PKZ_196X_SU0224.154 (161)</ta>
            <ta e="T740" id="Seg_6848" s="T729">PKZ_196X_SU0224.155 (162)</ta>
            <ta e="T742" id="Seg_6849" s="T740">PKZ_196X_SU0224.156 (163)</ta>
            <ta e="T749" id="Seg_6850" s="T742">PKZ_196X_SU0224.157 (164)</ta>
            <ta e="T757" id="Seg_6851" s="T749">PKZ_196X_SU0224.158 (165)</ta>
            <ta e="T763" id="Seg_6852" s="T757">PKZ_196X_SU0224.159 (166)</ta>
            <ta e="T767" id="Seg_6853" s="T763">PKZ_196X_SU0224.160 (167)</ta>
            <ta e="T770" id="Seg_6854" s="T767">PKZ_196X_SU0224.161 (168)</ta>
            <ta e="T781" id="Seg_6855" s="T770">PKZ_196X_SU0224.162 (169)</ta>
            <ta e="T791" id="Seg_6856" s="T781">PKZ_196X_SU0224.163 (170)</ta>
            <ta e="T792" id="Seg_6857" s="T791">PKZ_196X_SU0224.164 (171)</ta>
            <ta e="T798" id="Seg_6858" s="T792">PKZ_196X_SU0224.165 (172)</ta>
            <ta e="T805" id="Seg_6859" s="T798">PKZ_196X_SU0224.166 (173)</ta>
            <ta e="T810" id="Seg_6860" s="T805">PKZ_196X_SU0224.167 (174)</ta>
            <ta e="T814" id="Seg_6861" s="T810">PKZ_196X_SU0224.168 (175)</ta>
            <ta e="T816" id="Seg_6862" s="T814">PKZ_196X_SU0224.169 (176)</ta>
            <ta e="T819" id="Seg_6863" s="T816">PKZ_196X_SU0224.170 (177)</ta>
            <ta e="T827" id="Seg_6864" s="T819">PKZ_196X_SU0224.171 (178)</ta>
            <ta e="T834" id="Seg_6865" s="T827">PKZ_196X_SU0224.172 (179)</ta>
            <ta e="T840" id="Seg_6866" s="T834">PKZ_196X_SU0224.173 (180)</ta>
            <ta e="T844" id="Seg_6867" s="T840">PKZ_196X_SU0224.174 (181)</ta>
            <ta e="T849" id="Seg_6868" s="T844">PKZ_196X_SU0224.175 (182)</ta>
            <ta e="T851" id="Seg_6869" s="T849">PKZ_196X_SU0224.176 (183)</ta>
            <ta e="T858" id="Seg_6870" s="T851">PKZ_196X_SU0224.177 (184)</ta>
            <ta e="T866" id="Seg_6871" s="T858">PKZ_196X_SU0224.178 (185)</ta>
            <ta e="T870" id="Seg_6872" s="T866">PKZ_196X_SU0224.179 (186)</ta>
            <ta e="T874" id="Seg_6873" s="T870">PKZ_196X_SU0224.180 (187)</ta>
            <ta e="T877" id="Seg_6874" s="T874">PKZ_196X_SU0224.181 (188)</ta>
            <ta e="T880" id="Seg_6875" s="T877">PKZ_196X_SU0224.182 (189)</ta>
            <ta e="T883" id="Seg_6876" s="T880">PKZ_196X_SU0224.183 (190)</ta>
            <ta e="T887" id="Seg_6877" s="T883">PKZ_196X_SU0224.184 (191)</ta>
            <ta e="T889" id="Seg_6878" s="T887">PKZ_196X_SU0224.185 (192)</ta>
            <ta e="T899" id="Seg_6879" s="T889">PKZ_196X_SU0224.186 (193)</ta>
            <ta e="T904" id="Seg_6880" s="T899">PKZ_196X_SU0224.187 (194)</ta>
            <ta e="T907" id="Seg_6881" s="T904">PKZ_196X_SU0224.188 (195)</ta>
            <ta e="T912" id="Seg_6882" s="T907">PKZ_196X_SU0224.189 (196)</ta>
            <ta e="T923" id="Seg_6883" s="T912">PKZ_196X_SU0224.190 (197)</ta>
            <ta e="T924" id="Seg_6884" s="T923">PKZ_196X_SU0224.191 (198)</ta>
            <ta e="T925" id="Seg_6885" s="T924">PKZ_196X_SU0224.192 (199)</ta>
            <ta e="T933" id="Seg_6886" s="T925">PKZ_196X_SU0224.193 (200)</ta>
            <ta e="T941" id="Seg_6887" s="T933">PKZ_196X_SU0224.194 (201)</ta>
            <ta e="T947" id="Seg_6888" s="T941">PKZ_196X_SU0224.195 (202)</ta>
            <ta e="T953" id="Seg_6889" s="T947">PKZ_196X_SU0224.196 (203)</ta>
            <ta e="T957" id="Seg_6890" s="T953">PKZ_196X_SU0224.197 (204)</ta>
            <ta e="T963" id="Seg_6891" s="T957">PKZ_196X_SU0224.198 (205)</ta>
            <ta e="T966" id="Seg_6892" s="T963">PKZ_196X_SU0224.199 (206)</ta>
            <ta e="T969" id="Seg_6893" s="T966">PKZ_196X_SU0224.200 (207)</ta>
            <ta e="T975" id="Seg_6894" s="T969">PKZ_196X_SU0224.201 (208)</ta>
            <ta e="T980" id="Seg_6895" s="T975">PKZ_196X_SU0224.202 (209)</ta>
            <ta e="T988" id="Seg_6896" s="T980">PKZ_196X_SU0224.203 (210)</ta>
            <ta e="T992" id="Seg_6897" s="T988">PKZ_196X_SU0224.204 (211)</ta>
            <ta e="T997" id="Seg_6898" s="T992">PKZ_196X_SU0224.205 (212)</ta>
            <ta e="T1004" id="Seg_6899" s="T997">PKZ_196X_SU0224.206 (213)</ta>
            <ta e="T1012" id="Seg_6900" s="T1004">PKZ_196X_SU0224.207 (214)</ta>
            <ta e="T1016" id="Seg_6901" s="T1012">PKZ_196X_SU0224.208 (215)</ta>
            <ta e="T1019" id="Seg_6902" s="T1016">PKZ_196X_SU0224.209 (216)</ta>
            <ta e="T1025" id="Seg_6903" s="T1019">PKZ_196X_SU0224.210 (217)</ta>
            <ta e="T1027" id="Seg_6904" s="T1025">PKZ_196X_SU0224.211 (218)</ta>
            <ta e="T1037" id="Seg_6905" s="T1027">PKZ_196X_SU0224.212 (219)</ta>
            <ta e="T1038" id="Seg_6906" s="T1037">PKZ_196X_SU0224.213 (220)</ta>
            <ta e="T1041" id="Seg_6907" s="T1038">PKZ_196X_SU0224.214 (221)</ta>
            <ta e="T1046" id="Seg_6908" s="T1041">PKZ_196X_SU0224.215 (222)</ta>
            <ta e="T1052" id="Seg_6909" s="T1046">PKZ_196X_SU0224.216 (223)</ta>
            <ta e="T1057" id="Seg_6910" s="T1052">PKZ_196X_SU0224.217 (224)</ta>
            <ta e="T1064" id="Seg_6911" s="T1057">PKZ_196X_SU0224.218 (225)</ta>
            <ta e="T1070" id="Seg_6912" s="T1064">PKZ_196X_SU0224.219 (226)</ta>
            <ta e="T1075" id="Seg_6913" s="T1070">PKZ_196X_SU0224.220 (227)</ta>
            <ta e="T1076" id="Seg_6914" s="T1075">PKZ_196X_SU0224.221 (228)</ta>
            <ta e="T1084" id="Seg_6915" s="T1076">PKZ_196X_SU0224.222 (229)</ta>
            <ta e="T1090" id="Seg_6916" s="T1084">PKZ_196X_SU0224.223 (230)</ta>
            <ta e="T1097" id="Seg_6917" s="T1090">PKZ_196X_SU0224.224 (231)</ta>
            <ta e="T1098" id="Seg_6918" s="T1097">PKZ_196X_SU0224.225 (232)</ta>
            <ta e="T1104" id="Seg_6919" s="T1098">PKZ_196X_SU0224.226 (233)</ta>
            <ta e="T1107" id="Seg_6920" s="T1104">PKZ_196X_SU0224.227 (234)</ta>
            <ta e="T1110" id="Seg_6921" s="T1107">PKZ_196X_SU0224.228 (235)</ta>
            <ta e="T1112" id="Seg_6922" s="T1110">PKZ_196X_SU0224.229 (236)</ta>
            <ta e="T1115" id="Seg_6923" s="T1112">PKZ_196X_SU0224.230 (237)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T8" id="Seg_6924" s="T0">Nʼergölaʔbə bar kirgarlaʔbə, amnoləj dăk (dʼü=) dʼü (tegolaʔbə). </ta>
            <ta e="T10" id="Seg_6925" s="T8">Dĭ multuk. </ta>
            <ta e="T11" id="Seg_6926" s="T10">((BRK)). </ta>
            <ta e="T17" id="Seg_6927" s="T11">Girgit tĭrgit загадка, (kədet-) kötengəndə ягодка. </ta>
            <ta e="T19" id="Seg_6928" s="T17">Dĭ multuk. </ta>
            <ta e="T20" id="Seg_6929" s="T19">((BRK)). </ta>
            <ta e="T33" id="Seg_6930" s="T20">Kamen miʔ iʔgö ibibeʔ, nuzaŋ, măn mĭmbiem urgo măjagən, Belăgorjagən, dĭn kola dʼaʔpiam. </ta>
            <ta e="T36" id="Seg_6931" s="T33">I uja kuʔpiam. </ta>
            <ta e="T41" id="Seg_6932" s="T36">Dĭgəttə bar măjaʔi (sagərʔi) maluʔpiʔi. </ta>
            <ta e="T47" id="Seg_6933" s="T41">Măn šalbə už naga, bar külambi. </ta>
            <ta e="T57" id="Seg_6934" s="T47">A dĭn, gijen maʔi nugabiʔi bar, saʔməluʔpiʔi i bar külambiʔi. </ta>
            <ta e="T60" id="Seg_6935" s="T57">Tolʼkă šo păjdluʔpiʔi. </ta>
            <ta e="T61" id="Seg_6936" s="T60">((BRK)). </ta>
            <ta e="T66" id="Seg_6937" s="T61">Tüj (ĭm-) ĭmbidə ej kuliam. </ta>
            <ta e="T70" id="Seg_6938" s="T66">Ĭmbidə ej (nʼilol-) nʼilgöliam. </ta>
            <ta e="T74" id="Seg_6939" s="T70">Bar (s-) šalbə kalla dʼürbi. </ta>
            <ta e="T78" id="Seg_6940" s="T74">Tüj măn unnʼam amnolaʔbəm. </ta>
            <ta e="T84" id="Seg_6941" s="T78">Šindidə naga, bar külambiʔi măn nuzaŋbə. </ta>
            <ta e="T85" id="Seg_6942" s="T84">((BRK)). </ta>
            <ta e="T90" id="Seg_6943" s="T85">Tüj bar aktʼizaŋbə bar maluʔpi. </ta>
            <ta e="T93" id="Seg_6944" s="T90">Bar noʔsi özerluʔpi. </ta>
            <ta e="T94" id="Seg_6945" s="T93">((BRK)). </ta>
            <ta e="T98" id="Seg_6946" s="T94">Măn ugandə kuvas mobiam. </ta>
            <ta e="T103" id="Seg_6947" s="T98">A giber dĭ kuvas kalla dʼürbi? </ta>
            <ta e="T109" id="Seg_6948" s="T103">Kalla dʼürbi urgo măjazaŋdə, i gijendə naga. </ta>
            <ta e="T110" id="Seg_6949" s="T109">((BRK)). </ta>
            <ta e="T115" id="Seg_6950" s="T110">Tüj dĭ kamendə ej šoləj. </ta>
            <ta e="T116" id="Seg_6951" s="T115">((BRK)). </ta>
            <ta e="T117" id="Seg_6952" s="T116">((BRK)). </ta>
            <ta e="T123" id="Seg_6953" s="T117">Măn uʔbdəbiam (pereulăktə), (šo-) šonəga (aga). </ta>
            <ta e="T129" id="Seg_6954" s="T123">Măn măndəm: ugandə urgo kuza šonəga. </ta>
            <ta e="T133" id="Seg_6955" s="T129">Aparatsi turaʔi kürleʔbə mĭnge. </ta>
            <ta e="T144" id="Seg_6956" s="T133">Dĭgəttə măn nubiam, turagəʔ toʔndə dĭ (m-) bar măna (k-) kürbi. </ta>
            <ta e="T145" id="Seg_6957" s="T144">((BRK)). </ta>
            <ta e="T150" id="Seg_6958" s="T145">(Dĭn) nükegən kolat ibi, dĭn. </ta>
            <ta e="T151" id="Seg_6959" s="T150">((BRK)). </ta>
            <ta e="T156" id="Seg_6960" s="T151">Tĭn kolat ibi dĭn (meit). </ta>
            <ta e="T165" id="Seg_6961" s="T156">Dĭgəttə dĭʔnə šobi, măndə: "Măn tănan kola detlem teinen". </ta>
            <ta e="T172" id="Seg_6962" s="T165">A măn šobiam: "Tăn kolal meit ibi". </ta>
            <ta e="T175" id="Seg_6963" s="T172">Măn ej ibiem". </ta>
            <ta e="T176" id="Seg_6964" s="T175">((BRK)). </ta>
            <ta e="T181" id="Seg_6965" s="T176">((DMG)) Ĭmbi dĭ măna mĭləj, idʼiʔeje. </ta>
            <ta e="T184" id="Seg_6966" s="T181">Nu idʼiʔeʔe, kabarləj. </ta>
            <ta e="T190" id="Seg_6967" s="T184">Dĭzeŋ bostə (aktʼa=) aktʼanə (š-) sadarbiʔi. </ta>
            <ta e="T195" id="Seg_6968" s="T190">A tăn ej mĭbiel aktʼa. </ta>
            <ta e="T203" id="Seg_6969" s="T195">(I dĭ-) I (dĭn=) dĭ kumen mĭləʔi kabarləj. </ta>
            <ta e="T204" id="Seg_6970" s="T203">((BRK)). </ta>
            <ta e="T218" id="Seg_6971" s="T204">Dĭgəttə măn (u) kambiam Anissʼanə, dĭ măndə: "Jelʼa surarbi munəʔi, măn oʔbdəbiam bʼeʔ munəjʔ. </ta>
            <ta e="T229" id="Seg_6972" s="T218">(Mĭ- me-) Mĭləm (bʼeʔ k-) onʼiʔ munəjʔ bʼeʔ kăpʼejkazi mĭləm dĭʔnə. </ta>
            <ta e="T230" id="Seg_6973" s="T229">((BRK)). </ta>
            <ta e="T242" id="Seg_6974" s="T230">Jelʼa măndə:" Kanžəbəj miʔnʼibeʔ", a măn măndəm:" Dʼok, ej kalam, ugandə kuŋgəʔ". </ta>
            <ta e="T244" id="Seg_6975" s="T242">"Ĭmbi kuŋgeʔ? </ta>
            <ta e="T245" id="Seg_6976" s="T244">Amnaʔ. </ta>
            <ta e="T249" id="Seg_6977" s="T245">(Süjö- süjönə=) Bazaj süjönə. </ta>
            <ta e="T252" id="Seg_6978" s="T249">I büžü šolal". </ta>
            <ta e="T259" id="Seg_6979" s="T252">"Dʼok, măn pimniem bazaj (sʼü-) süjönə (amnəsʼtə). </ta>
            <ta e="T261" id="Seg_6980" s="T259">A to külalləl". </ta>
            <ta e="T269" id="Seg_6981" s="T261">"Dʼok, ej külalləl, dĭn jakše amnozittə, kak turagən. </ta>
            <ta e="T274" id="Seg_6982" s="T269">Büžü maʔnəl šolal, miʔnʼibeʔ amnolal. </ta>
            <ta e="T275" id="Seg_6983" s="T274">((BRK)). </ta>
            <ta e="T277" id="Seg_6984" s="T275">Nagurköʔ kalləbaʔ. </ta>
            <ta e="T278" id="Seg_6985" s="T277">((BRK)). </ta>
            <ta e="T282" id="Seg_6986" s="T278">Nüdʼin kalam onʼiʔ tibinə. </ta>
            <ta e="T288" id="Seg_6987" s="T282">(T- tĭbi tĭ- tĭbi=) Onʼiʔ kuzanə. </ta>
            <ta e="T294" id="Seg_6988" s="T288">Măllam: (Kun-) Kundə dĭ nükem bălʼnʼitsanə. </ta>
            <ta e="T299" id="Seg_6989" s="T294">(Dĭʔnə s-) Dĭʔnə sazən mĭbiʔi. </ta>
            <ta e="T313" id="Seg_6990" s="T299">A ej (kol- ko-) kundlal dăk, kăštʼit скорая помощь, dĭ šoləj da kunnalləj dĭm". </ta>
            <ta e="T319" id="Seg_6991" s="T313">Dăre nörbələm diʔnə, možet dĭ nʼilgöləj. </ta>
            <ta e="T337" id="Seg_6992" s="T319">A to vedʼ tăn kujdərgan ibiel, a il bar măllia: "(Kod-) Kujdərgan ibiel, bostə ĭmbidə ej kabažarbia dĭ nükenə". </ta>
            <ta e="T338" id="Seg_6993" s="T337">((BRK)). </ta>
            <ta e="T347" id="Seg_6994" s="T338">A abal külambi, a tura (nan) vedʼ tăn ibiel. </ta>
            <ta e="T348" id="Seg_6995" s="T347">((BRK)). </ta>
            <ta e="T352" id="Seg_6996" s="T348">Men unnʼa amnobi, amnobi. </ta>
            <ta e="T359" id="Seg_6997" s="T352">"Скучно unnʼanə amnozittə, kalam ele boskəndə kulim". </ta>
            <ta e="T363" id="Seg_6998" s="T359">Kambi, kambi, kubi zajtsəm. </ta>
            <ta e="T366" id="Seg_6999" s="T363">"Davaj tănzi amnozittə?" </ta>
            <ta e="T368" id="Seg_7000" s="T366">"Davaj amnozittə". </ta>
            <ta e="T374" id="Seg_7001" s="T368">Dĭgəttə nüdʼi molambi, dĭzeŋ iʔpiʔi kunolzittə. </ta>
            <ta e="T381" id="Seg_7002" s="T374">Zajəs kunolluʔpi, a (mei-) men ej kunollia. </ta>
            <ta e="T384" id="Seg_7003" s="T381">Nüdʼin bar kürümleʔbə. </ta>
            <ta e="T387" id="Seg_7004" s="T384">A zajəs nereʔluʔpi. </ta>
            <ta e="T390" id="Seg_7005" s="T387">Sĭjdə pʼatkəndə kalla dʼürbi. </ta>
            <ta e="T392" id="Seg_7006" s="T390">"Ĭmbi kürümniel? </ta>
            <ta e="T397" id="Seg_7007" s="T392">Šoləj volk i miʔnʼibeʔ amnəj". </ta>
            <ta e="T404" id="Seg_7008" s="T397">A men măndə:" Ej jakšə măn elem. </ta>
            <ta e="T409" id="Seg_7009" s="T404">Kalam (volkə is-) volk (măndərzittə). </ta>
            <ta e="T411" id="Seg_7010" s="T409">Dĭgəttə kambi. </ta>
            <ta e="T416" id="Seg_7011" s="T411">Šonəga, šonəga, volk dĭʔnə šonəga. </ta>
            <ta e="T419" id="Seg_7012" s="T416">"Davaj tănzi amnozittə?" </ta>
            <ta e="T421" id="Seg_7013" s="T419">"Davaj amnozittə". </ta>
            <ta e="T427" id="Seg_7014" s="T421">Nüdʼi šobi, (dĭ-) dĭzeŋ iʔpiʔi kunolzittə. </ta>
            <ta e="T430" id="Seg_7015" s="T427">Men nüdʼin kürümnuʔpi. </ta>
            <ta e="T435" id="Seg_7016" s="T430">A volk măndə:" Ĭmbi kürümnaʔbəl? </ta>
            <ta e="T441" id="Seg_7017" s="T435">Tüj urgaːba šoləj i miʔnʼibeʔ amnəj". </ta>
            <ta e="T452" id="Seg_7018" s="T441">Dĭgəttə men ertən uʔbdəbi: "Ej jakše elem, kalam urgaːba (mărno-) măndərzittə". </ta>
            <ta e="T455" id="Seg_7019" s="T452">(Dəg-) Dĭgəttə kambi. </ta>
            <ta e="T459" id="Seg_7020" s="T455">Šonəga, šonəga, urgaːba šonəga. </ta>
            <ta e="T464" id="Seg_7021" s="T459">"Urgaːba, urgaːba, davaj tănzi amnozittə". </ta>
            <ta e="T465" id="Seg_7022" s="T464">"Davaj". </ta>
            <ta e="T471" id="Seg_7023" s="T465">Dĭgəttə nüdʼi molambi, (iʔpil-) iʔpiʔi kunolzittə. </ta>
            <ta e="T476" id="Seg_7024" s="T471">Men nüdʼin bar kürümnuʔpi bar. </ta>
            <ta e="T480" id="Seg_7025" s="T476">A urgaːba:" Ĭmbi kürümniel? </ta>
            <ta e="T485" id="Seg_7026" s="T480">Kuza šoləj da miʔnʼibeʔ kutləj". </ta>
            <ta e="T494" id="Seg_7027" s="T485">A dĭ măndə:" Ej jakše elem, kalam kuza măndərzittə. </ta>
            <ta e="T498" id="Seg_7028" s="T494">Ertən uʔbdəbi, kambi ((…)). </ta>
            <ta e="T502" id="Seg_7029" s="T498">Bar paʔi mĭmbi, mĭmbi. </ta>
            <ta e="T505" id="Seg_7030" s="T502">Dĭgəttə sʼtʼeptə šobi. </ta>
            <ta e="T509" id="Seg_7031" s="T505">Kuliat bar: kuza šonəga. </ta>
            <ta e="T514" id="Seg_7032" s="T509">(Paʔ- paʔ-) Paʔi izittə, inezi. </ta>
            <ta e="T521" id="Seg_7033" s="T514">Dĭgəttə dĭ măndə:" Kuza, kuza, davaj amnozittə". </ta>
            <ta e="T525" id="Seg_7034" s="T521">Dĭ deʔpi dĭm maʔndə. </ta>
            <ta e="T527" id="Seg_7035" s="T525">Iʔbəbiʔi kunolzittə. </ta>
            <ta e="T534" id="Seg_7036" s="T527">Kuza kunolluʔpi, a men bar davaj kürümzittə. </ta>
            <ta e="T549" id="Seg_7037" s="T534">A kuza uʔbdəbi, măndə: "Tăn püjölial, amoraʔ da kunolaʔ, măna (deʔ= dĭʔ -) mĭʔ kunolzittə". </ta>
            <ta e="T550" id="Seg_7038" s="T549">Bar! </ta>
            <ta e="T551" id="Seg_7039" s="T550">((BRK)). </ta>
            <ta e="T555" id="Seg_7040" s="T551">Onʼiʔ kuza kambi pajlaʔ. </ta>
            <ta e="T560" id="Seg_7041" s="T555">(Pa-) Paʔi ibi i šonəga. </ta>
            <ta e="T563" id="Seg_7042" s="T560">Dĭʔnə urgaːba šonəga. </ta>
            <ta e="T567" id="Seg_7043" s="T563">"Măn tüjö tăn amnam". </ta>
            <ta e="T571" id="Seg_7044" s="T567">"Tăn iʔ amaʔ măna. </ta>
            <ta e="T576" id="Seg_7045" s="T571">Dĭn ăxotnikʔi šonəgaʔi, kutləʔjə tănan". </ta>
            <ta e="T579" id="Seg_7046" s="T576">Dĭ bar nereʔluʔpi. </ta>
            <ta e="T582" id="Seg_7047" s="T579">Eneʔ măna paʔizi. </ta>
            <ta e="T584" id="Seg_7048" s="T582">Da kanaʔ. </ta>
            <ta e="T589" id="Seg_7049" s="T584">Štobɨ dĭzeŋ ej kubiʔi măna". </ta>
            <ta e="T596" id="Seg_7050" s="T589">A dĭ mănlia:" Nada enzittə dăk sarzittə". </ta>
            <ta e="T601" id="Seg_7051" s="T596">Dĭgəttə (dĭ=) dĭ măndə:" Saraʔ". </ta>
            <ta e="T604" id="Seg_7052" s="T601">Dĭ sarbi dĭm. </ta>
            <ta e="T610" id="Seg_7053" s="T604">"A tüj nada (balt-) baltu jaʔsittə". </ta>
            <ta e="T620" id="Seg_7054" s="T610">I dĭgəttə dĭ urgaːbam dĭ baltuzi jaʔluʔpi, i urgaːba külambi. </ta>
            <ta e="T621" id="Seg_7055" s="T620">((BRK)). </ta>
            <ta e="T627" id="Seg_7056" s="T621">Urgaːba tibizi bar tugan (molab-) molaʔpiʔi. </ta>
            <ta e="T630" id="Seg_7057" s="T627">Davaj kuʔsittə репа. </ta>
            <ta e="T635" id="Seg_7058" s="T630">"Măna kăreškiʔi, a tănan verškiʔi". </ta>
            <ta e="T637" id="Seg_7059" s="T635">Dĭgəttə kuʔpiʔi. </ta>
            <ta e="T639" id="Seg_7060" s="T637">Dĭ özerbi. </ta>
            <ta e="T645" id="Seg_7061" s="T639">Tibi ibi kăreškiʔi, a urgaːba— verškiʔi. </ta>
            <ta e="T651" id="Seg_7062" s="T645">Dĭgəttə urgaːba:" No, kuza măna mʼegluʔpi". </ta>
            <ta e="T655" id="Seg_7063" s="T651">Dĭgəttə bazo ejü molambi. </ta>
            <ta e="T661" id="Seg_7064" s="T655">Kuza măndə (urgaːbanə):" Davaj bazo kuʔsittə". </ta>
            <ta e="T670" id="Seg_7065" s="T661">No, urgaːba: "(Măna=) Măna deʔ kăreškiʔi, a tănan verškiʔi". </ta>
            <ta e="T674" id="Seg_7066" s="T670">Dĭgəttə budəj ipek … </ta>
            <ta e="T677" id="Seg_7067" s="T674">Budəj ipek kuʔpiʔi. </ta>
            <ta e="T682" id="Seg_7068" s="T677">Dĭ özerbi, jakše ugandə ibi. </ta>
            <ta e="T689" id="Seg_7069" s="T682">Dĭ kuza ibi verškiʔi, a urgaːbanə kăreškiʔi. </ta>
            <ta e="T696" id="Seg_7070" s="T689">I dĭgəttə (dĭ-) dĭzen bar družbat külambi. </ta>
            <ta e="T697" id="Seg_7071" s="T696">((BRK)). </ta>
            <ta e="T701" id="Seg_7072" s="T697">Amnolaʔpi büzʼe da nüke. </ta>
            <ta e="T705" id="Seg_7073" s="T701">Dĭzeŋgən esseŋdə ej ibiʔi. </ta>
            <ta e="T711" id="Seg_7074" s="T705">(Onʼiʔ) sĭre bar saʔməluʔpi, ugandə urgo. </ta>
            <ta e="T721" id="Seg_7075" s="T711">Esseŋ bar girgit bar (кат -) катаются, girgit bar sʼarlaʔbəʔjə. </ta>
            <ta e="T725" id="Seg_7076" s="T721">Snegurkaʔi alaʔbəʔjə, bar alaʔbəʔjə. </ta>
            <ta e="T729" id="Seg_7077" s="T725">Dĭgəttə ((NOISE)) abiʔi urgo nüke. </ta>
            <ta e="T740" id="Seg_7078" s="T729">A büzʼe üge măndobi, măndobi:" Kanžəbəj (san-) tănzi nʼiʔtə",— nükenə măllia. </ta>
            <ta e="T742" id="Seg_7079" s="T740">"No, kanžəbəj." </ta>
            <ta e="T749" id="Seg_7080" s="T742">(Ka-) Kambiʔi, măndobiʔi, dĭgəttə davaj azittə koʔbdo. </ta>
            <ta e="T757" id="Seg_7081" s="T749">Abiʔi, ulu abiʔi, simaʔi, püjet, aŋdə, simat. </ta>
            <ta e="T763" id="Seg_7082" s="T757">Dĭgəttə kössi bar siman toʔndə dʼüʔpiʔi. </ta>
            <ta e="T767" id="Seg_7083" s="T763">Dĭgəttə dĭ simatsi mʼeŋgəlluʔpi. </ta>
            <ta e="T770" id="Seg_7084" s="T767">Dĭgəttə büzʼe kandlaʔbə. </ta>
            <ta e="T781" id="Seg_7085" s="T770">(Kam-) Kambi turanə, a (dĭ=) dĭzeŋ (dĭʔ-) tože dĭzi kambiʔi turanə. </ta>
            <ta e="T791" id="Seg_7086" s="T781">Bar ugandə ((NOISE)) dĭʔnə jakše molambi, ešši (ge) bar ibiʔi dĭʔnə. </ta>
            <ta e="T792" id="Seg_7087" s="T791">((NOISE)) ((BRK)). </ta>
            <ta e="T798" id="Seg_7088" s="T792">Dĭgəttə dĭʔnə jamaʔi ibiʔi, kuvazəʔi, köməʔi. </ta>
            <ta e="T805" id="Seg_7089" s="T798">Dĭgəttə eʔbdənə ibiʔi лента, ugandə kuvas, атласный. </ta>
            <ta e="T810" id="Seg_7090" s="T805">(Gəttə dĭ=) Dĭgəttə ejü molambi. </ta>
            <ta e="T814" id="Seg_7091" s="T810">A dĭ koʔbdo amnolaʔbə. </ta>
            <ta e="T816" id="Seg_7092" s="T814">Bü mʼaŋnaʔbə. </ta>
            <ta e="T819" id="Seg_7093" s="T816">Dĭ bar pimnie. </ta>
            <ta e="T827" id="Seg_7094" s="T819">A (nüket măn-) nüke măndə:" Ĭmbi tăn dʼorlaʔbəl? </ta>
            <ta e="T834" id="Seg_7095" s="T827">A bostən bar (kă-) kăjəldə mʼaŋnaʔbəʔjə kadəlgən. </ta>
            <ta e="T840" id="Seg_7096" s="T834">(Ta=) Dĭgəttə (ej-) bazo kuja molaʔpi. </ta>
            <ta e="T844" id="Seg_7097" s="T840">Всякий noʔ özerleʔbəʔjə, svʼetogəʔjə. </ta>
            <ta e="T849" id="Seg_7098" s="T844">Dĭgəttə šobiʔi koʔbsaŋ:" Kanžəbəj miʔsi!" </ta>
            <ta e="T851" id="Seg_7099" s="T849">Kăštliaʔi koʔbdot. </ta>
            <ta e="T858" id="Seg_7100" s="T851">Nüke (măn- măn-) măllia:" Kanaʔ, ĭmbi amnolaʔbəl?" </ta>
            <ta e="T866" id="Seg_7101" s="T858">"Dʼok, măn em kanaʔ, (kuja=) ulum bar ĭzemnuʔləj. </ta>
            <ta e="T870" id="Seg_7102" s="T866">(Kuj-) Kuja nendləj ulum". </ta>
            <ta e="T874" id="Seg_7103" s="T870">"No, plat (ser-) šerdə". </ta>
            <ta e="T877" id="Seg_7104" s="T874">Dĭgəttə dĭ kambi. </ta>
            <ta e="T880" id="Seg_7105" s="T877">Bar koʔbsaŋ sʼarlaʔbəʔjə. </ta>
            <ta e="T883" id="Seg_7106" s="T880">Svʼetogəʔi (nʼi-) nĭŋgəleʔbəʔjə. </ta>
            <ta e="T887" id="Seg_7107" s="T883">A dĭ (amnolaʔbə) ((DMG)). </ta>
            <ta e="T889" id="Seg_7108" s="T887">((DMG)) (-ambi). </ta>
            <ta e="T899" id="Seg_7109" s="T889">Dĭgəttə dĭzeŋ embiʔi šü ugandə urgo i davaj nuʔməsittə šünə. </ta>
            <ta e="T904" id="Seg_7110" s="T899">Mănliaʔi:" Ĭmbi tăn ej suʔmiliel?" </ta>
            <ta e="T907" id="Seg_7111" s="T904">A dĭ pimnie. </ta>
            <ta e="T912" id="Seg_7112" s="T907">A dĭgəttə kak dĭm pĭštʼerleʔbəʔjə. </ta>
            <ta e="T923" id="Seg_7113" s="T912">A dĭ uʔbdəbi, (sa-) suʔmiluʔpi i bar šüʔən kalla dʼürbi nʼuʔdə bar. </ta>
            <ta e="T924" id="Seg_7114" s="T923">((BRK)). </ta>
            <ta e="T925" id="Seg_7115" s="T924">((BRK)). </ta>
            <ta e="T933" id="Seg_7116" s="T925">Raz buzom, (măn) ugandə kuvas (sĭre) buzo ibi. </ta>
            <ta e="T941" id="Seg_7117" s="T933">(Dĭgəttə) mĭmbi, mĭmbi (măn) nüdʼin (bĭtəlzittə) (sut-) sütsi. </ta>
            <ta e="T947" id="Seg_7118" s="T941">Kirgariam, kirgariam, măndərbiam măndərbiam, gijendə naga. </ta>
            <ta e="T953" id="Seg_7119" s="T947">No giraːmbi (bu-) buzo, ej tĭmniem. </ta>
            <ta e="T957" id="Seg_7120" s="T953">Ertən uʔbdəbiam, vezʼdʼe mĭmbiem. </ta>
            <ta e="T963" id="Seg_7121" s="T957">Dʼagagən mĭmbiem, tenəbiem dĭ saʔməluʔpi bünə. </ta>
            <ta e="T966" id="Seg_7122" s="T963">Dĭn naga везде. </ta>
            <ta e="T969" id="Seg_7123" s="T966">Tenəbiem: volk amnuʔpi. </ta>
            <ta e="T975" id="Seg_7124" s="T969">Dʼok (ej volk amn-) ej amnuʔpi. </ta>
            <ta e="T980" id="Seg_7125" s="T975">Dĭgəttə dön il noʔ jaʔpiʔi. </ta>
            <ta e="T988" id="Seg_7126" s="T980">Dĭzeŋ наверное ibiʔi, üjüttə (s-) sarbiʔi, kumbiʔi maʔndə. </ta>
            <ta e="T992" id="Seg_7127" s="T988">Šide üjütsi volk ibi. </ta>
            <ta e="T997" id="Seg_7128" s="T992">((BRK)) Măn (s-) tuganbə šobi Kanoklerdə). </ta>
            <ta e="T1004" id="Seg_7129" s="T997">I nörbəlie: (t-) šide tüžöj ibi (ilgən). </ta>
            <ta e="T1012" id="Seg_7130" s="T1004">(Ugandə il i- il=) Ugandə (š-) sildə iʔgö. </ta>
            <ta e="T1016" id="Seg_7131" s="T1012">A miʔnʼibeʔ amnoləj bar. </ta>
            <ta e="T1019" id="Seg_7132" s="T1016">Ĭmbi todam (molaʔjə). </ta>
            <ta e="T1025" id="Seg_7133" s="T1019">Noʔ ej amnia, bü ej bĭtlie. </ta>
            <ta e="T1027" id="Seg_7134" s="T1025">Amga amnaʔbə. </ta>
            <ta e="T1037" id="Seg_7135" s="T1027">Baška tüžöj šoləʔjə, bar noʔ amnuʔləʔjə, a dĭ ej amnia. </ta>
            <ta e="T1038" id="Seg_7136" s="T1037">((BRK)). </ta>
            <ta e="T1041" id="Seg_7137" s="T1038">"Ej tĭmnem",— măndə. </ta>
            <ta e="T1046" id="Seg_7138" s="T1041">Šeden dĭrgit ali ĭmbi dĭrgit. </ta>
            <ta e="T1052" id="Seg_7139" s="T1046">Miʔ ibibeʔ tĭn (se-) sʼestrandə tüžöjdə. </ta>
            <ta e="T1057" id="Seg_7140" s="T1052">Dĭ vedʼ ĭmbidə ej tĭmnet. </ta>
            <ta e="T1064" id="Seg_7141" s="T1057">Tože tüžöj dăre molambi bar, todam mobi. </ta>
            <ta e="T1070" id="Seg_7142" s="T1064">A (boʔ) sildə iʔgö (m-) ibi. </ta>
            <ta e="T1075" id="Seg_7143" s="T1070">Amnobi miʔnʼibeʔ, (tona-) todam molambi. </ta>
            <ta e="T1076" id="Seg_7144" s="T1075">((BRK)). </ta>
            <ta e="T1084" id="Seg_7145" s="T1076">Dĭ onʼiʔ koʔbdozi amnolaʔpi, dĭgəttə (baš-) baska ibi. </ta>
            <ta e="T1090" id="Seg_7146" s="T1084">Možet dĭ (чё-) ĭmbi-nʼibudʼ (tĭ-) tĭlbi? </ta>
            <ta e="T1097" id="Seg_7147" s="T1090">I tüj (tüžej-) tüžöjdə bar ej nuliaʔi. </ta>
            <ta e="T1098" id="Seg_7148" s="T1097">((BRK)). </ta>
            <ta e="T1104" id="Seg_7149" s="T1098">Šiʔ (var-) bar karəldʼan (kala-) ((DMG)). </ta>
            <ta e="T1107" id="Seg_7150" s="T1104">Ši karəldʼan kalla dʼürləbeʔ. </ta>
            <ta e="T1110" id="Seg_7151" s="T1107">(Kalla dʼürləleʔ), davajtʼe (kamrolabaʔ). </ta>
            <ta e="T1112" id="Seg_7152" s="T1110">I pănarlabaʔ. </ta>
            <ta e="T1115" id="Seg_7153" s="T1112">Dĭgəttə kangaʔ kudajzi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T1" id="Seg_7154" s="T0">nʼergö-laʔbə</ta>
            <ta e="T2" id="Seg_7155" s="T1">bar</ta>
            <ta e="T3" id="Seg_7156" s="T2">kirgar-laʔbə</ta>
            <ta e="T4" id="Seg_7157" s="T3">amno-lə-j</ta>
            <ta e="T5" id="Seg_7158" s="T4">dăk</ta>
            <ta e="T6" id="Seg_7159" s="T5">dʼü</ta>
            <ta e="T7" id="Seg_7160" s="T6">dʼü</ta>
            <ta e="T8" id="Seg_7161" s="T7">tego-laʔbə</ta>
            <ta e="T9" id="Seg_7162" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_7163" s="T9">multuk</ta>
            <ta e="T12" id="Seg_7164" s="T11">girgit</ta>
            <ta e="T13" id="Seg_7165" s="T12">tĭrgit</ta>
            <ta e="T16" id="Seg_7166" s="T15">köten-gəndə</ta>
            <ta e="T18" id="Seg_7167" s="T17">dĭ</ta>
            <ta e="T19" id="Seg_7168" s="T18">multuk</ta>
            <ta e="T21" id="Seg_7169" s="T20">kamen</ta>
            <ta e="T22" id="Seg_7170" s="T21">miʔ</ta>
            <ta e="T23" id="Seg_7171" s="T22">iʔgö</ta>
            <ta e="T24" id="Seg_7172" s="T23">i-bi-beʔ</ta>
            <ta e="T25" id="Seg_7173" s="T24">nu-zaŋ</ta>
            <ta e="T26" id="Seg_7174" s="T25">măn</ta>
            <ta e="T27" id="Seg_7175" s="T26">mĭm-bie-m</ta>
            <ta e="T28" id="Seg_7176" s="T27">urgo</ta>
            <ta e="T29" id="Seg_7177" s="T28">măja-gən</ta>
            <ta e="T30" id="Seg_7178" s="T29">Belăgorja-gən</ta>
            <ta e="T31" id="Seg_7179" s="T30">dĭn</ta>
            <ta e="T32" id="Seg_7180" s="T31">kola</ta>
            <ta e="T33" id="Seg_7181" s="T32">dʼaʔ-pia-m</ta>
            <ta e="T34" id="Seg_7182" s="T33">i</ta>
            <ta e="T35" id="Seg_7183" s="T34">uja</ta>
            <ta e="T36" id="Seg_7184" s="T35">kuʔ-pia-m</ta>
            <ta e="T37" id="Seg_7185" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_7186" s="T37">bar</ta>
            <ta e="T39" id="Seg_7187" s="T38">măja-ʔi</ta>
            <ta e="T40" id="Seg_7188" s="T39">sagər-ʔi</ta>
            <ta e="T41" id="Seg_7189" s="T40">ma-luʔ-pi-ʔi</ta>
            <ta e="T42" id="Seg_7190" s="T41">măn</ta>
            <ta e="T43" id="Seg_7191" s="T42">šal-bə</ta>
            <ta e="T44" id="Seg_7192" s="T43">už</ta>
            <ta e="T45" id="Seg_7193" s="T44">naga</ta>
            <ta e="T46" id="Seg_7194" s="T45">bar</ta>
            <ta e="T47" id="Seg_7195" s="T46">kü-lam-bi</ta>
            <ta e="T48" id="Seg_7196" s="T47">a</ta>
            <ta e="T49" id="Seg_7197" s="T48">dĭn</ta>
            <ta e="T50" id="Seg_7198" s="T49">gijen</ta>
            <ta e="T51" id="Seg_7199" s="T50">ma-ʔi</ta>
            <ta e="T52" id="Seg_7200" s="T51">nu-ga-bi-ʔi</ta>
            <ta e="T53" id="Seg_7201" s="T52">bar</ta>
            <ta e="T54" id="Seg_7202" s="T53">saʔmə-luʔ-pi-ʔi</ta>
            <ta e="T55" id="Seg_7203" s="T54">i</ta>
            <ta e="T56" id="Seg_7204" s="T55">bar</ta>
            <ta e="T57" id="Seg_7205" s="T56">kü-lam-bi-ʔi</ta>
            <ta e="T58" id="Seg_7206" s="T57">tolʼkă</ta>
            <ta e="T59" id="Seg_7207" s="T58">šo</ta>
            <ta e="T60" id="Seg_7208" s="T59">păjd-luʔ-pi-ʔi</ta>
            <ta e="T62" id="Seg_7209" s="T61">tüj</ta>
            <ta e="T64" id="Seg_7210" s="T63">ĭmbi=də</ta>
            <ta e="T65" id="Seg_7211" s="T64">ej</ta>
            <ta e="T66" id="Seg_7212" s="T65">ku-lia-m</ta>
            <ta e="T67" id="Seg_7213" s="T66">ĭmbi=də</ta>
            <ta e="T68" id="Seg_7214" s="T67">ej</ta>
            <ta e="T70" id="Seg_7215" s="T69">nʼilgö-lia-m</ta>
            <ta e="T71" id="Seg_7216" s="T70">bar</ta>
            <ta e="T73" id="Seg_7217" s="T72">šal-bə</ta>
            <ta e="T1117" id="Seg_7218" s="T73">kal-la</ta>
            <ta e="T74" id="Seg_7219" s="T1117">dʼür-bi</ta>
            <ta e="T75" id="Seg_7220" s="T74">tüj</ta>
            <ta e="T76" id="Seg_7221" s="T75">măn</ta>
            <ta e="T77" id="Seg_7222" s="T76">unnʼa-m</ta>
            <ta e="T78" id="Seg_7223" s="T77">amno-laʔbə-m</ta>
            <ta e="T79" id="Seg_7224" s="T78">šindi=də</ta>
            <ta e="T80" id="Seg_7225" s="T79">naga</ta>
            <ta e="T81" id="Seg_7226" s="T80">bar</ta>
            <ta e="T82" id="Seg_7227" s="T81">kü-lam-bi-ʔi</ta>
            <ta e="T83" id="Seg_7228" s="T82">măn</ta>
            <ta e="T84" id="Seg_7229" s="T83">nu-zaŋ-bə</ta>
            <ta e="T86" id="Seg_7230" s="T85">tüj</ta>
            <ta e="T87" id="Seg_7231" s="T86">bar</ta>
            <ta e="T88" id="Seg_7232" s="T87">aktʼi-zaŋ-bə</ta>
            <ta e="T89" id="Seg_7233" s="T88">bar</ta>
            <ta e="T90" id="Seg_7234" s="T89">ma-luʔ-pi</ta>
            <ta e="T91" id="Seg_7235" s="T90">bar</ta>
            <ta e="T92" id="Seg_7236" s="T91">noʔ-si</ta>
            <ta e="T93" id="Seg_7237" s="T92">özer-luʔ-pi</ta>
            <ta e="T95" id="Seg_7238" s="T94">măn</ta>
            <ta e="T96" id="Seg_7239" s="T95">ugandə</ta>
            <ta e="T97" id="Seg_7240" s="T96">kuvas</ta>
            <ta e="T98" id="Seg_7241" s="T97">mo-bia-m</ta>
            <ta e="T99" id="Seg_7242" s="T98">a</ta>
            <ta e="T100" id="Seg_7243" s="T99">giber</ta>
            <ta e="T101" id="Seg_7244" s="T100">dĭ</ta>
            <ta e="T102" id="Seg_7245" s="T101">kuvas</ta>
            <ta e="T1118" id="Seg_7246" s="T102">kal-la</ta>
            <ta e="T103" id="Seg_7247" s="T1118">dʼür-bi</ta>
            <ta e="T1119" id="Seg_7248" s="T103">kal-la</ta>
            <ta e="T104" id="Seg_7249" s="T1119">dʼür-bi</ta>
            <ta e="T105" id="Seg_7250" s="T104">urgo</ta>
            <ta e="T106" id="Seg_7251" s="T105">măja-zaŋ-də</ta>
            <ta e="T107" id="Seg_7252" s="T106">i</ta>
            <ta e="T108" id="Seg_7253" s="T107">gijen=də</ta>
            <ta e="T109" id="Seg_7254" s="T108">naga</ta>
            <ta e="T111" id="Seg_7255" s="T110">tüj</ta>
            <ta e="T112" id="Seg_7256" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_7257" s="T112">kamen=də</ta>
            <ta e="T114" id="Seg_7258" s="T113">ej</ta>
            <ta e="T115" id="Seg_7259" s="T114">šo-lə-j</ta>
            <ta e="T118" id="Seg_7260" s="T117">măn</ta>
            <ta e="T119" id="Seg_7261" s="T118">uʔbdə-bia-m</ta>
            <ta e="T120" id="Seg_7262" s="T119">pereulăk-tə</ta>
            <ta e="T122" id="Seg_7263" s="T121">šonə-ga</ta>
            <ta e="T123" id="Seg_7264" s="T122">aga</ta>
            <ta e="T124" id="Seg_7265" s="T123">măn</ta>
            <ta e="T125" id="Seg_7266" s="T124">măn-də-m</ta>
            <ta e="T126" id="Seg_7267" s="T125">ugandə</ta>
            <ta e="T127" id="Seg_7268" s="T126">urgo</ta>
            <ta e="T128" id="Seg_7269" s="T127">kuza</ta>
            <ta e="T129" id="Seg_7270" s="T128">šonə-ga</ta>
            <ta e="T130" id="Seg_7271" s="T129">aparat-si</ta>
            <ta e="T131" id="Seg_7272" s="T130">tura-ʔi</ta>
            <ta e="T132" id="Seg_7273" s="T131">kür-leʔbə</ta>
            <ta e="T133" id="Seg_7274" s="T132">mĭn-ge</ta>
            <ta e="T134" id="Seg_7275" s="T133">dĭgəttə</ta>
            <ta e="T135" id="Seg_7276" s="T134">măn</ta>
            <ta e="T136" id="Seg_7277" s="T135">nu-bia-m</ta>
            <ta e="T137" id="Seg_7278" s="T136">tura-gəʔ</ta>
            <ta e="T138" id="Seg_7279" s="T137">toʔ-ndə</ta>
            <ta e="T139" id="Seg_7280" s="T138">dĭ</ta>
            <ta e="T141" id="Seg_7281" s="T140">bar</ta>
            <ta e="T142" id="Seg_7282" s="T141">măna</ta>
            <ta e="T144" id="Seg_7283" s="T143">kür-bi</ta>
            <ta e="T146" id="Seg_7284" s="T145">dĭn</ta>
            <ta e="T147" id="Seg_7285" s="T146">nüke-gən</ta>
            <ta e="T148" id="Seg_7286" s="T147">kola-t</ta>
            <ta e="T149" id="Seg_7287" s="T148">i-bi</ta>
            <ta e="T150" id="Seg_7288" s="T149">dĭn</ta>
            <ta e="T152" id="Seg_7289" s="T151">tĭ-n</ta>
            <ta e="T153" id="Seg_7290" s="T152">kola-t</ta>
            <ta e="T154" id="Seg_7291" s="T153">i-bi</ta>
            <ta e="T155" id="Seg_7292" s="T154">dĭ-n</ta>
            <ta e="T156" id="Seg_7293" s="T155">mei-t</ta>
            <ta e="T157" id="Seg_7294" s="T156">dĭgəttə</ta>
            <ta e="T158" id="Seg_7295" s="T157">dĭʔ-nə</ta>
            <ta e="T159" id="Seg_7296" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_7297" s="T159">măn-də</ta>
            <ta e="T161" id="Seg_7298" s="T160">măn</ta>
            <ta e="T162" id="Seg_7299" s="T161">tănan</ta>
            <ta e="T163" id="Seg_7300" s="T162">kola</ta>
            <ta e="T164" id="Seg_7301" s="T163">det-le-m</ta>
            <ta e="T165" id="Seg_7302" s="T164">teinen</ta>
            <ta e="T166" id="Seg_7303" s="T165">a</ta>
            <ta e="T167" id="Seg_7304" s="T166">măn</ta>
            <ta e="T168" id="Seg_7305" s="T167">šo-bia-m</ta>
            <ta e="T169" id="Seg_7306" s="T168">tăn</ta>
            <ta e="T170" id="Seg_7307" s="T169">kola-l</ta>
            <ta e="T171" id="Seg_7308" s="T170">mei-t</ta>
            <ta e="T172" id="Seg_7309" s="T171">i-bi</ta>
            <ta e="T173" id="Seg_7310" s="T172">măn</ta>
            <ta e="T174" id="Seg_7311" s="T173">ej</ta>
            <ta e="T175" id="Seg_7312" s="T174">i-bie-m</ta>
            <ta e="T177" id="Seg_7313" s="T176">ĭmbi</ta>
            <ta e="T178" id="Seg_7314" s="T177">dĭ</ta>
            <ta e="T179" id="Seg_7315" s="T178">măna</ta>
            <ta e="T180" id="Seg_7316" s="T179">mĭ-lə-j</ta>
            <ta e="T181" id="Seg_7317" s="T180">idʼiʔeje</ta>
            <ta e="T182" id="Seg_7318" s="T181">nu</ta>
            <ta e="T183" id="Seg_7319" s="T182">idʼiʔeʔe</ta>
            <ta e="T184" id="Seg_7320" s="T183">kabarləj</ta>
            <ta e="T185" id="Seg_7321" s="T184">dĭ-zeŋ</ta>
            <ta e="T186" id="Seg_7322" s="T185">bos-tə</ta>
            <ta e="T187" id="Seg_7323" s="T186">aktʼa</ta>
            <ta e="T188" id="Seg_7324" s="T187">aktʼa-nə</ta>
            <ta e="T190" id="Seg_7325" s="T189">sadar-bi-ʔi</ta>
            <ta e="T191" id="Seg_7326" s="T190">a</ta>
            <ta e="T192" id="Seg_7327" s="T191">tăn</ta>
            <ta e="T193" id="Seg_7328" s="T192">ej</ta>
            <ta e="T194" id="Seg_7329" s="T193">mĭ-bie-l</ta>
            <ta e="T195" id="Seg_7330" s="T194">aktʼa</ta>
            <ta e="T196" id="Seg_7331" s="T195">i</ta>
            <ta e="T198" id="Seg_7332" s="T197">i</ta>
            <ta e="T199" id="Seg_7333" s="T198">dĭ-n</ta>
            <ta e="T200" id="Seg_7334" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_7335" s="T200">kumen</ta>
            <ta e="T202" id="Seg_7336" s="T201">mĭ-lə-ʔi</ta>
            <ta e="T203" id="Seg_7337" s="T202">kabarləj</ta>
            <ta e="T205" id="Seg_7338" s="T204">dĭgəttə</ta>
            <ta e="T206" id="Seg_7339" s="T205">măn</ta>
            <ta e="T207" id="Seg_7340" s="T206">u</ta>
            <ta e="T208" id="Seg_7341" s="T207">kam-bia-m</ta>
            <ta e="T209" id="Seg_7342" s="T208">Anissʼa-nə</ta>
            <ta e="T210" id="Seg_7343" s="T209">dĭ</ta>
            <ta e="T211" id="Seg_7344" s="T210">măn-də</ta>
            <ta e="T212" id="Seg_7345" s="T211">Jelʼa</ta>
            <ta e="T213" id="Seg_7346" s="T212">surar-bi</ta>
            <ta e="T214" id="Seg_7347" s="T213">munə-ʔi</ta>
            <ta e="T215" id="Seg_7348" s="T214">măn</ta>
            <ta e="T216" id="Seg_7349" s="T215">oʔbdə-bia-m</ta>
            <ta e="T217" id="Seg_7350" s="T216">bʼeʔ</ta>
            <ta e="T218" id="Seg_7351" s="T217">munəj-ʔ</ta>
            <ta e="T221" id="Seg_7352" s="T220">mĭ-lə-m</ta>
            <ta e="T222" id="Seg_7353" s="T221">bʼeʔ</ta>
            <ta e="T224" id="Seg_7354" s="T223">onʼiʔ</ta>
            <ta e="T225" id="Seg_7355" s="T224">munəj-ʔ</ta>
            <ta e="T226" id="Seg_7356" s="T225">bʼeʔ</ta>
            <ta e="T227" id="Seg_7357" s="T226">kăpʼejka-zi</ta>
            <ta e="T228" id="Seg_7358" s="T227">mĭ-lə-m</ta>
            <ta e="T229" id="Seg_7359" s="T228">dĭʔ-nə</ta>
            <ta e="T231" id="Seg_7360" s="T230">Jelʼa</ta>
            <ta e="T232" id="Seg_7361" s="T231">măn-də</ta>
            <ta e="T233" id="Seg_7362" s="T232">kan-žə-bəj</ta>
            <ta e="T234" id="Seg_7363" s="T233">miʔnʼibeʔ</ta>
            <ta e="T235" id="Seg_7364" s="T234">a</ta>
            <ta e="T236" id="Seg_7365" s="T235">măn</ta>
            <ta e="T237" id="Seg_7366" s="T236">măn-də-m</ta>
            <ta e="T238" id="Seg_7367" s="T237">dʼok</ta>
            <ta e="T239" id="Seg_7368" s="T238">ej</ta>
            <ta e="T240" id="Seg_7369" s="T239">ka-la-m</ta>
            <ta e="T241" id="Seg_7370" s="T240">ugandə</ta>
            <ta e="T242" id="Seg_7371" s="T241">kuŋgəʔ</ta>
            <ta e="T243" id="Seg_7372" s="T242">ĭmbi</ta>
            <ta e="T244" id="Seg_7373" s="T243">kuŋge-ʔ</ta>
            <ta e="T245" id="Seg_7374" s="T244">amna-ʔ</ta>
            <ta e="T247" id="Seg_7375" s="T246">süjö-nə</ta>
            <ta e="T248" id="Seg_7376" s="T247">baza-j</ta>
            <ta e="T249" id="Seg_7377" s="T248">süjö-nə</ta>
            <ta e="T250" id="Seg_7378" s="T249">i</ta>
            <ta e="T251" id="Seg_7379" s="T250">büžü</ta>
            <ta e="T252" id="Seg_7380" s="T251">šo-la-l</ta>
            <ta e="T253" id="Seg_7381" s="T252">dʼok</ta>
            <ta e="T254" id="Seg_7382" s="T253">măn</ta>
            <ta e="T255" id="Seg_7383" s="T254">pim-nie-m</ta>
            <ta e="T256" id="Seg_7384" s="T255">baza-j</ta>
            <ta e="T258" id="Seg_7385" s="T257">süjö-nə</ta>
            <ta e="T259" id="Seg_7386" s="T258">amnə-sʼtə</ta>
            <ta e="T260" id="Seg_7387" s="T259">ato</ta>
            <ta e="T261" id="Seg_7388" s="T260">kü-lal-lə-l</ta>
            <ta e="T262" id="Seg_7389" s="T261">dʼok</ta>
            <ta e="T263" id="Seg_7390" s="T262">ej</ta>
            <ta e="T264" id="Seg_7391" s="T263">kü-lal-lə-l</ta>
            <ta e="T265" id="Seg_7392" s="T264">dĭn</ta>
            <ta e="T266" id="Seg_7393" s="T265">jakše</ta>
            <ta e="T267" id="Seg_7394" s="T266">amno-zittə</ta>
            <ta e="T268" id="Seg_7395" s="T267">kak</ta>
            <ta e="T269" id="Seg_7396" s="T268">tura-gən</ta>
            <ta e="T270" id="Seg_7397" s="T269">büžü</ta>
            <ta e="T271" id="Seg_7398" s="T270">maʔ-nə-l</ta>
            <ta e="T272" id="Seg_7399" s="T271">šo-la-l</ta>
            <ta e="T273" id="Seg_7400" s="T272">miʔnʼibeʔ</ta>
            <ta e="T274" id="Seg_7401" s="T273">amno-la-l</ta>
            <ta e="T276" id="Seg_7402" s="T275">nagur-köʔ</ta>
            <ta e="T277" id="Seg_7403" s="T276">kal-lə-baʔ</ta>
            <ta e="T279" id="Seg_7404" s="T278">nüdʼi-n</ta>
            <ta e="T280" id="Seg_7405" s="T279">ka-la-m</ta>
            <ta e="T281" id="Seg_7406" s="T280">onʼiʔ</ta>
            <ta e="T282" id="Seg_7407" s="T281">tibi-nə</ta>
            <ta e="T287" id="Seg_7408" s="T286">onʼiʔ</ta>
            <ta e="T288" id="Seg_7409" s="T287">kuza-nə</ta>
            <ta e="T289" id="Seg_7410" s="T288">măl-la-m</ta>
            <ta e="T291" id="Seg_7411" s="T290">kun-də</ta>
            <ta e="T292" id="Seg_7412" s="T291">dĭ</ta>
            <ta e="T293" id="Seg_7413" s="T292">nüke-m</ta>
            <ta e="T294" id="Seg_7414" s="T293">bălʼnʼitsa-nə</ta>
            <ta e="T295" id="Seg_7415" s="T294">dĭʔ-nə</ta>
            <ta e="T297" id="Seg_7416" s="T296">dĭʔ-nə</ta>
            <ta e="T298" id="Seg_7417" s="T297">sazən</ta>
            <ta e="T299" id="Seg_7418" s="T298">mĭ-bi-ʔi</ta>
            <ta e="T300" id="Seg_7419" s="T299">a</ta>
            <ta e="T301" id="Seg_7420" s="T300">ej</ta>
            <ta e="T304" id="Seg_7421" s="T303">kund-la-l</ta>
            <ta e="T305" id="Seg_7422" s="T304">dăk</ta>
            <ta e="T306" id="Seg_7423" s="T305">kăštʼi-t</ta>
            <ta e="T309" id="Seg_7424" s="T308">dĭ</ta>
            <ta e="T310" id="Seg_7425" s="T309">šo-lə-j</ta>
            <ta e="T311" id="Seg_7426" s="T310">da</ta>
            <ta e="T312" id="Seg_7427" s="T311">kun-nal-lə-j</ta>
            <ta e="T313" id="Seg_7428" s="T312">dĭ-m</ta>
            <ta e="T314" id="Seg_7429" s="T313">dăre</ta>
            <ta e="T315" id="Seg_7430" s="T314">nörbə-lə-m</ta>
            <ta e="T316" id="Seg_7431" s="T315">diʔ-nə</ta>
            <ta e="T317" id="Seg_7432" s="T316">možet</ta>
            <ta e="T318" id="Seg_7433" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_7434" s="T318">nʼilgö-lə-j</ta>
            <ta e="T320" id="Seg_7435" s="T319">ato</ta>
            <ta e="T321" id="Seg_7436" s="T320">vedʼ</ta>
            <ta e="T322" id="Seg_7437" s="T321">tăn</ta>
            <ta e="T323" id="Seg_7438" s="T322">kujdərgan</ta>
            <ta e="T324" id="Seg_7439" s="T323">i-bie-l</ta>
            <ta e="T325" id="Seg_7440" s="T324">a</ta>
            <ta e="T326" id="Seg_7441" s="T325">il</ta>
            <ta e="T327" id="Seg_7442" s="T326">bar</ta>
            <ta e="T328" id="Seg_7443" s="T327">măl-lia</ta>
            <ta e="T330" id="Seg_7444" s="T329">kujdərgan</ta>
            <ta e="T331" id="Seg_7445" s="T330">i-bie-l</ta>
            <ta e="T332" id="Seg_7446" s="T331">bos-tə</ta>
            <ta e="T333" id="Seg_7447" s="T332">ĭmbi=də</ta>
            <ta e="T334" id="Seg_7448" s="T333">ej</ta>
            <ta e="T335" id="Seg_7449" s="T334">kabažar-bia</ta>
            <ta e="T336" id="Seg_7450" s="T335">dĭ</ta>
            <ta e="T337" id="Seg_7451" s="T336">nüke-nə</ta>
            <ta e="T339" id="Seg_7452" s="T338">a</ta>
            <ta e="T340" id="Seg_7453" s="T339">aba-l</ta>
            <ta e="T341" id="Seg_7454" s="T340">kü-lam-bi</ta>
            <ta e="T342" id="Seg_7455" s="T341">a</ta>
            <ta e="T343" id="Seg_7456" s="T342">tura</ta>
            <ta e="T344" id="Seg_7457" s="T343">nan</ta>
            <ta e="T345" id="Seg_7458" s="T344">vedʼ</ta>
            <ta e="T346" id="Seg_7459" s="T345">tăn</ta>
            <ta e="T347" id="Seg_7460" s="T346">i-bie-l</ta>
            <ta e="T349" id="Seg_7461" s="T348">men</ta>
            <ta e="T350" id="Seg_7462" s="T349">unnʼa</ta>
            <ta e="T351" id="Seg_7463" s="T350">amno-bi</ta>
            <ta e="T352" id="Seg_7464" s="T351">amno-bi</ta>
            <ta e="T354" id="Seg_7465" s="T353">unnʼa-nə</ta>
            <ta e="T355" id="Seg_7466" s="T354">amno-zittə</ta>
            <ta e="T356" id="Seg_7467" s="T355">ka-la-m</ta>
            <ta e="T357" id="Seg_7468" s="T356">ele</ta>
            <ta e="T358" id="Seg_7469" s="T357">bos-kəndə</ta>
            <ta e="T359" id="Seg_7470" s="T358">ku-li-m</ta>
            <ta e="T360" id="Seg_7471" s="T359">kam-bi</ta>
            <ta e="T361" id="Seg_7472" s="T360">kam-bi</ta>
            <ta e="T362" id="Seg_7473" s="T361">ku-bi</ta>
            <ta e="T363" id="Seg_7474" s="T362">zajtsə-m</ta>
            <ta e="T364" id="Seg_7475" s="T363">davaj</ta>
            <ta e="T365" id="Seg_7476" s="T364">tăn-zi</ta>
            <ta e="T366" id="Seg_7477" s="T365">amno-zittə</ta>
            <ta e="T367" id="Seg_7478" s="T366">davaj</ta>
            <ta e="T368" id="Seg_7479" s="T367">amno-zittə</ta>
            <ta e="T369" id="Seg_7480" s="T368">dĭgəttə</ta>
            <ta e="T370" id="Seg_7481" s="T369">nüdʼi</ta>
            <ta e="T371" id="Seg_7482" s="T370">mo-lam-bi</ta>
            <ta e="T372" id="Seg_7483" s="T371">dĭ-zeŋ</ta>
            <ta e="T373" id="Seg_7484" s="T372">iʔ-pi-ʔi</ta>
            <ta e="T374" id="Seg_7485" s="T373">kunol-zittə</ta>
            <ta e="T375" id="Seg_7486" s="T374">zajəs</ta>
            <ta e="T376" id="Seg_7487" s="T375">kunol-luʔ-pi</ta>
            <ta e="T377" id="Seg_7488" s="T376">a</ta>
            <ta e="T379" id="Seg_7489" s="T378">men</ta>
            <ta e="T380" id="Seg_7490" s="T379">ej</ta>
            <ta e="T381" id="Seg_7491" s="T380">kunol-lia</ta>
            <ta e="T382" id="Seg_7492" s="T381">nüdʼi-n</ta>
            <ta e="T383" id="Seg_7493" s="T382">bar</ta>
            <ta e="T384" id="Seg_7494" s="T383">kürüm-leʔbə</ta>
            <ta e="T385" id="Seg_7495" s="T384">a</ta>
            <ta e="T386" id="Seg_7496" s="T385">zajəs</ta>
            <ta e="T387" id="Seg_7497" s="T386">nereʔ-luʔ-pi</ta>
            <ta e="T388" id="Seg_7498" s="T387">sĭj-də</ta>
            <ta e="T389" id="Seg_7499" s="T388">pʼatkə-ndə</ta>
            <ta e="T1120" id="Seg_7500" s="T389">kal-la</ta>
            <ta e="T390" id="Seg_7501" s="T1120">dʼür-bi</ta>
            <ta e="T391" id="Seg_7502" s="T390">ĭmbi</ta>
            <ta e="T392" id="Seg_7503" s="T391">kürüm-nie-l</ta>
            <ta e="T393" id="Seg_7504" s="T392">šo-lə-j</ta>
            <ta e="T394" id="Seg_7505" s="T393">volk</ta>
            <ta e="T395" id="Seg_7506" s="T394">i</ta>
            <ta e="T396" id="Seg_7507" s="T395">miʔnʼibeʔ</ta>
            <ta e="T397" id="Seg_7508" s="T396">am-nə-j</ta>
            <ta e="T398" id="Seg_7509" s="T397">a</ta>
            <ta e="T399" id="Seg_7510" s="T398">men</ta>
            <ta e="T400" id="Seg_7511" s="T399">măn-də</ta>
            <ta e="T401" id="Seg_7512" s="T400">ej</ta>
            <ta e="T402" id="Seg_7513" s="T401">jakšə</ta>
            <ta e="T403" id="Seg_7514" s="T402">măn</ta>
            <ta e="T404" id="Seg_7515" s="T403">elem</ta>
            <ta e="T405" id="Seg_7516" s="T404">ka-la-m</ta>
            <ta e="T406" id="Seg_7517" s="T405">volkə</ta>
            <ta e="T408" id="Seg_7518" s="T407">volk</ta>
            <ta e="T409" id="Seg_7519" s="T408">măndə-r-zittə</ta>
            <ta e="T410" id="Seg_7520" s="T409">dĭgəttə</ta>
            <ta e="T411" id="Seg_7521" s="T410">kam-bi</ta>
            <ta e="T412" id="Seg_7522" s="T411">šonə-ga</ta>
            <ta e="T413" id="Seg_7523" s="T412">šonə-ga</ta>
            <ta e="T414" id="Seg_7524" s="T413">volk</ta>
            <ta e="T415" id="Seg_7525" s="T414">dĭʔ-nə</ta>
            <ta e="T416" id="Seg_7526" s="T415">šonə-ga</ta>
            <ta e="T417" id="Seg_7527" s="T416">davaj</ta>
            <ta e="T418" id="Seg_7528" s="T417">tăn-zi</ta>
            <ta e="T419" id="Seg_7529" s="T418">amno-zittə</ta>
            <ta e="T420" id="Seg_7530" s="T419">davaj</ta>
            <ta e="T421" id="Seg_7531" s="T420">amno-zittə</ta>
            <ta e="T422" id="Seg_7532" s="T421">nüdʼi</ta>
            <ta e="T423" id="Seg_7533" s="T422">šo-bi</ta>
            <ta e="T425" id="Seg_7534" s="T424">dĭ-zeŋ</ta>
            <ta e="T426" id="Seg_7535" s="T425">iʔ-pi-ʔi</ta>
            <ta e="T427" id="Seg_7536" s="T426">kunol-zittə</ta>
            <ta e="T428" id="Seg_7537" s="T427">men</ta>
            <ta e="T429" id="Seg_7538" s="T428">nüdʼi-n</ta>
            <ta e="T430" id="Seg_7539" s="T429">kürüm-nuʔ-pi</ta>
            <ta e="T431" id="Seg_7540" s="T430">a</ta>
            <ta e="T432" id="Seg_7541" s="T431">volk</ta>
            <ta e="T433" id="Seg_7542" s="T432">măn-də</ta>
            <ta e="T434" id="Seg_7543" s="T433">ĭmbi</ta>
            <ta e="T435" id="Seg_7544" s="T434">kürüm-naʔbə-l</ta>
            <ta e="T436" id="Seg_7545" s="T435">tüj</ta>
            <ta e="T437" id="Seg_7546" s="T436">urgaːba</ta>
            <ta e="T438" id="Seg_7547" s="T437">šo-lə-j</ta>
            <ta e="T439" id="Seg_7548" s="T438">i</ta>
            <ta e="T440" id="Seg_7549" s="T439">miʔnʼibeʔ</ta>
            <ta e="T441" id="Seg_7550" s="T440">am-nə-j</ta>
            <ta e="T442" id="Seg_7551" s="T441">dĭgəttə</ta>
            <ta e="T443" id="Seg_7552" s="T442">men</ta>
            <ta e="T444" id="Seg_7553" s="T443">ertə-n</ta>
            <ta e="T445" id="Seg_7554" s="T444">uʔbdə-bi</ta>
            <ta e="T446" id="Seg_7555" s="T445">ej</ta>
            <ta e="T447" id="Seg_7556" s="T446">jakše</ta>
            <ta e="T448" id="Seg_7557" s="T447">elem</ta>
            <ta e="T449" id="Seg_7558" s="T448">ka-la-m</ta>
            <ta e="T450" id="Seg_7559" s="T449">urgaːba</ta>
            <ta e="T452" id="Seg_7560" s="T451">măndə-r-zittə</ta>
            <ta e="T454" id="Seg_7561" s="T453">dĭgəttə</ta>
            <ta e="T455" id="Seg_7562" s="T454">kam-bi</ta>
            <ta e="T456" id="Seg_7563" s="T455">šonə-ga</ta>
            <ta e="T457" id="Seg_7564" s="T456">šonə-ga</ta>
            <ta e="T458" id="Seg_7565" s="T457">urgaːba</ta>
            <ta e="T459" id="Seg_7566" s="T458">šonə-ga</ta>
            <ta e="T460" id="Seg_7567" s="T459">urgaːba</ta>
            <ta e="T461" id="Seg_7568" s="T460">urgaːba</ta>
            <ta e="T462" id="Seg_7569" s="T461">davaj</ta>
            <ta e="T463" id="Seg_7570" s="T462">tăn-zi</ta>
            <ta e="T464" id="Seg_7571" s="T463">amno-zittə</ta>
            <ta e="T465" id="Seg_7572" s="T464">davaj</ta>
            <ta e="T466" id="Seg_7573" s="T465">dĭgəttə</ta>
            <ta e="T467" id="Seg_7574" s="T466">nüdʼi</ta>
            <ta e="T468" id="Seg_7575" s="T467">mo-lam-bi</ta>
            <ta e="T470" id="Seg_7576" s="T469">iʔ-pi-ʔi</ta>
            <ta e="T471" id="Seg_7577" s="T470">kunol-zittə</ta>
            <ta e="T472" id="Seg_7578" s="T471">men</ta>
            <ta e="T473" id="Seg_7579" s="T472">nüdʼi-n</ta>
            <ta e="T474" id="Seg_7580" s="T473">bar</ta>
            <ta e="T475" id="Seg_7581" s="T474">kürüm-nuʔ-pi</ta>
            <ta e="T476" id="Seg_7582" s="T475">bar</ta>
            <ta e="T477" id="Seg_7583" s="T476">a</ta>
            <ta e="T478" id="Seg_7584" s="T477">urgaːba</ta>
            <ta e="T479" id="Seg_7585" s="T478">ĭmbi</ta>
            <ta e="T480" id="Seg_7586" s="T479">kürüm-nie-l</ta>
            <ta e="T481" id="Seg_7587" s="T480">kuza</ta>
            <ta e="T482" id="Seg_7588" s="T481">šo-lə-j</ta>
            <ta e="T483" id="Seg_7589" s="T482">da</ta>
            <ta e="T484" id="Seg_7590" s="T483">miʔnʼibeʔ</ta>
            <ta e="T485" id="Seg_7591" s="T484">kut-lə-j</ta>
            <ta e="T486" id="Seg_7592" s="T485">a</ta>
            <ta e="T487" id="Seg_7593" s="T486">dĭ</ta>
            <ta e="T488" id="Seg_7594" s="T487">măn-də</ta>
            <ta e="T489" id="Seg_7595" s="T488">ej</ta>
            <ta e="T490" id="Seg_7596" s="T489">jakše</ta>
            <ta e="T491" id="Seg_7597" s="T490">elem</ta>
            <ta e="T492" id="Seg_7598" s="T491">ka-la-m</ta>
            <ta e="T493" id="Seg_7599" s="T492">kuza</ta>
            <ta e="T494" id="Seg_7600" s="T493">măndə-r-zittə</ta>
            <ta e="T495" id="Seg_7601" s="T494">ertə-n</ta>
            <ta e="T496" id="Seg_7602" s="T495">uʔbdə-bi</ta>
            <ta e="T497" id="Seg_7603" s="T496">kam-bi</ta>
            <ta e="T499" id="Seg_7604" s="T498">bar</ta>
            <ta e="T500" id="Seg_7605" s="T499">pa-ʔi</ta>
            <ta e="T501" id="Seg_7606" s="T500">mĭm-bi</ta>
            <ta e="T502" id="Seg_7607" s="T501">mĭm-bi</ta>
            <ta e="T503" id="Seg_7608" s="T502">dĭgəttə</ta>
            <ta e="T504" id="Seg_7609" s="T503">sʼtʼep-tə</ta>
            <ta e="T505" id="Seg_7610" s="T504">šo-bi</ta>
            <ta e="T506" id="Seg_7611" s="T505">ku-lia-t</ta>
            <ta e="T507" id="Seg_7612" s="T506">bar</ta>
            <ta e="T508" id="Seg_7613" s="T507">kuza</ta>
            <ta e="T509" id="Seg_7614" s="T508">šonə-ga</ta>
            <ta e="T512" id="Seg_7615" s="T511">pa-ʔi</ta>
            <ta e="T513" id="Seg_7616" s="T512">i-zittə</ta>
            <ta e="T514" id="Seg_7617" s="T513">ine-zi</ta>
            <ta e="T515" id="Seg_7618" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_7619" s="T515">dĭ</ta>
            <ta e="T517" id="Seg_7620" s="T516">măn-də</ta>
            <ta e="T518" id="Seg_7621" s="T517">kuza</ta>
            <ta e="T519" id="Seg_7622" s="T518">kuza</ta>
            <ta e="T520" id="Seg_7623" s="T519">davaj</ta>
            <ta e="T521" id="Seg_7624" s="T520">amno-zittə</ta>
            <ta e="T522" id="Seg_7625" s="T521">dĭ</ta>
            <ta e="T523" id="Seg_7626" s="T522">deʔ-pi</ta>
            <ta e="T524" id="Seg_7627" s="T523">dĭ-m</ta>
            <ta e="T525" id="Seg_7628" s="T524">maʔ-ndə</ta>
            <ta e="T526" id="Seg_7629" s="T525">iʔbə-bi-ʔi</ta>
            <ta e="T527" id="Seg_7630" s="T526">kunol-zittə</ta>
            <ta e="T528" id="Seg_7631" s="T527">kuza</ta>
            <ta e="T529" id="Seg_7632" s="T528">kunol-luʔ-pi</ta>
            <ta e="T530" id="Seg_7633" s="T529">a</ta>
            <ta e="T531" id="Seg_7634" s="T530">men</ta>
            <ta e="T532" id="Seg_7635" s="T531">bar</ta>
            <ta e="T533" id="Seg_7636" s="T532">davaj</ta>
            <ta e="T534" id="Seg_7637" s="T533">kürüm-zittə</ta>
            <ta e="T535" id="Seg_7638" s="T534">a</ta>
            <ta e="T536" id="Seg_7639" s="T535">kuza</ta>
            <ta e="T537" id="Seg_7640" s="T536">uʔbdə-bi</ta>
            <ta e="T538" id="Seg_7641" s="T537">măn-də</ta>
            <ta e="T539" id="Seg_7642" s="T538">tăn</ta>
            <ta e="T540" id="Seg_7643" s="T539">püjö-lia-l</ta>
            <ta e="T541" id="Seg_7644" s="T540">amor-a-ʔ</ta>
            <ta e="T542" id="Seg_7645" s="T541">da</ta>
            <ta e="T543" id="Seg_7646" s="T542">kunol-a-ʔ</ta>
            <ta e="T544" id="Seg_7647" s="T543">măna</ta>
            <ta e="T545" id="Seg_7648" s="T544">de-ʔ</ta>
            <ta e="T546" id="Seg_7649" s="T545">dĭʔ</ta>
            <ta e="T548" id="Seg_7650" s="T547">mĭ-ʔ</ta>
            <ta e="T549" id="Seg_7651" s="T548">kunol-zittə</ta>
            <ta e="T550" id="Seg_7652" s="T549">bar</ta>
            <ta e="T552" id="Seg_7653" s="T551">onʼiʔ</ta>
            <ta e="T553" id="Seg_7654" s="T552">kuza</ta>
            <ta e="T554" id="Seg_7655" s="T553">kam-bi</ta>
            <ta e="T555" id="Seg_7656" s="T554">pa-j-laʔ</ta>
            <ta e="T557" id="Seg_7657" s="T556">pa-ʔi</ta>
            <ta e="T558" id="Seg_7658" s="T557">i-bi</ta>
            <ta e="T559" id="Seg_7659" s="T558">i</ta>
            <ta e="T560" id="Seg_7660" s="T559">šonə-ga</ta>
            <ta e="T561" id="Seg_7661" s="T560">dĭʔ-nə</ta>
            <ta e="T562" id="Seg_7662" s="T561">urgaːba</ta>
            <ta e="T563" id="Seg_7663" s="T562">šonə-ga</ta>
            <ta e="T564" id="Seg_7664" s="T563">măn</ta>
            <ta e="T565" id="Seg_7665" s="T564">tüjö</ta>
            <ta e="T566" id="Seg_7666" s="T565">tăn</ta>
            <ta e="T567" id="Seg_7667" s="T566">am-na-m</ta>
            <ta e="T568" id="Seg_7668" s="T567">tăn</ta>
            <ta e="T569" id="Seg_7669" s="T568">i-ʔ</ta>
            <ta e="T570" id="Seg_7670" s="T569">am-a-ʔ</ta>
            <ta e="T571" id="Seg_7671" s="T570">măna</ta>
            <ta e="T572" id="Seg_7672" s="T571">dĭn</ta>
            <ta e="T573" id="Seg_7673" s="T572">ăxotnik-ʔi</ta>
            <ta e="T574" id="Seg_7674" s="T573">šonə-ga-ʔi</ta>
            <ta e="T575" id="Seg_7675" s="T574">kut-lə-ʔjə</ta>
            <ta e="T576" id="Seg_7676" s="T575">tănan</ta>
            <ta e="T577" id="Seg_7677" s="T576">dĭ</ta>
            <ta e="T578" id="Seg_7678" s="T577">bar</ta>
            <ta e="T579" id="Seg_7679" s="T578">nereʔ-luʔ-pi</ta>
            <ta e="T580" id="Seg_7680" s="T579">en-e-ʔ</ta>
            <ta e="T581" id="Seg_7681" s="T580">măna</ta>
            <ta e="T582" id="Seg_7682" s="T581">pa-ʔi-zi</ta>
            <ta e="T583" id="Seg_7683" s="T582">da</ta>
            <ta e="T584" id="Seg_7684" s="T583">kan-a-ʔ</ta>
            <ta e="T585" id="Seg_7685" s="T584">štobɨ</ta>
            <ta e="T586" id="Seg_7686" s="T585">dĭ-zeŋ</ta>
            <ta e="T587" id="Seg_7687" s="T586">ej</ta>
            <ta e="T588" id="Seg_7688" s="T587">ku-bi-ʔi</ta>
            <ta e="T589" id="Seg_7689" s="T588">măna</ta>
            <ta e="T590" id="Seg_7690" s="T589">a</ta>
            <ta e="T591" id="Seg_7691" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_7692" s="T591">măn-lia</ta>
            <ta e="T593" id="Seg_7693" s="T592">nada</ta>
            <ta e="T594" id="Seg_7694" s="T593">en-zittə</ta>
            <ta e="T595" id="Seg_7695" s="T594">dăk</ta>
            <ta e="T596" id="Seg_7696" s="T595">sar-zittə</ta>
            <ta e="T597" id="Seg_7697" s="T596">dĭgəttə</ta>
            <ta e="T598" id="Seg_7698" s="T597">dĭ</ta>
            <ta e="T599" id="Seg_7699" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_7700" s="T599">măn-də</ta>
            <ta e="T601" id="Seg_7701" s="T600">sar-a-ʔ</ta>
            <ta e="T602" id="Seg_7702" s="T601">dĭ</ta>
            <ta e="T603" id="Seg_7703" s="T602">sar-bi</ta>
            <ta e="T604" id="Seg_7704" s="T603">dĭ-m</ta>
            <ta e="T605" id="Seg_7705" s="T604">a</ta>
            <ta e="T606" id="Seg_7706" s="T605">tüj</ta>
            <ta e="T607" id="Seg_7707" s="T606">nada</ta>
            <ta e="T609" id="Seg_7708" s="T608">baltu</ta>
            <ta e="T610" id="Seg_7709" s="T609">jaʔ-sittə</ta>
            <ta e="T611" id="Seg_7710" s="T610">i</ta>
            <ta e="T612" id="Seg_7711" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_7712" s="T612">dĭ</ta>
            <ta e="T614" id="Seg_7713" s="T613">urgaːba-m</ta>
            <ta e="T615" id="Seg_7714" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_7715" s="T615">baltu-zi</ta>
            <ta e="T617" id="Seg_7716" s="T616">jaʔ-luʔ-pi</ta>
            <ta e="T618" id="Seg_7717" s="T617">i</ta>
            <ta e="T619" id="Seg_7718" s="T618">urgaːba</ta>
            <ta e="T620" id="Seg_7719" s="T619">kü-lam-bi</ta>
            <ta e="T622" id="Seg_7720" s="T621">urgaːba</ta>
            <ta e="T623" id="Seg_7721" s="T622">tibi-zi</ta>
            <ta e="T624" id="Seg_7722" s="T623">bar</ta>
            <ta e="T625" id="Seg_7723" s="T624">tugan</ta>
            <ta e="T627" id="Seg_7724" s="T626">mo-laʔ-pi-ʔi</ta>
            <ta e="T628" id="Seg_7725" s="T627">davaj</ta>
            <ta e="T629" id="Seg_7726" s="T628">kuʔ-sittə</ta>
            <ta e="T631" id="Seg_7727" s="T630">măna</ta>
            <ta e="T632" id="Seg_7728" s="T631">kăreški-ʔi</ta>
            <ta e="T633" id="Seg_7729" s="T632">a</ta>
            <ta e="T634" id="Seg_7730" s="T633">tănan</ta>
            <ta e="T635" id="Seg_7731" s="T634">verški-ʔi</ta>
            <ta e="T636" id="Seg_7732" s="T635">dĭgəttə</ta>
            <ta e="T637" id="Seg_7733" s="T636">kuʔ-pi-ʔi</ta>
            <ta e="T638" id="Seg_7734" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_7735" s="T638">özer-bi</ta>
            <ta e="T640" id="Seg_7736" s="T639">tibi</ta>
            <ta e="T641" id="Seg_7737" s="T640">i-bi</ta>
            <ta e="T642" id="Seg_7738" s="T641">kăreški-ʔi</ta>
            <ta e="T643" id="Seg_7739" s="T642">a</ta>
            <ta e="T644" id="Seg_7740" s="T643">urgaːba</ta>
            <ta e="T645" id="Seg_7741" s="T644">verški-ʔi</ta>
            <ta e="T646" id="Seg_7742" s="T645">dĭgəttə</ta>
            <ta e="T647" id="Seg_7743" s="T646">urgaːba</ta>
            <ta e="T648" id="Seg_7744" s="T647">no</ta>
            <ta e="T649" id="Seg_7745" s="T648">kuza</ta>
            <ta e="T650" id="Seg_7746" s="T649">măna</ta>
            <ta e="T651" id="Seg_7747" s="T650">mʼeg-luʔ-pi</ta>
            <ta e="T652" id="Seg_7748" s="T651">dĭgəttə</ta>
            <ta e="T653" id="Seg_7749" s="T652">bazo</ta>
            <ta e="T654" id="Seg_7750" s="T653">ejü</ta>
            <ta e="T655" id="Seg_7751" s="T654">mo-lam-bi</ta>
            <ta e="T656" id="Seg_7752" s="T655">kuza</ta>
            <ta e="T657" id="Seg_7753" s="T656">măn-də</ta>
            <ta e="T658" id="Seg_7754" s="T657">urgaːba-nə</ta>
            <ta e="T659" id="Seg_7755" s="T658">davaj</ta>
            <ta e="T660" id="Seg_7756" s="T659">bazo</ta>
            <ta e="T661" id="Seg_7757" s="T660">kuʔ-sittə</ta>
            <ta e="T662" id="Seg_7758" s="T661">no</ta>
            <ta e="T663" id="Seg_7759" s="T662">urgaːba</ta>
            <ta e="T664" id="Seg_7760" s="T663">măna</ta>
            <ta e="T665" id="Seg_7761" s="T664">măna</ta>
            <ta e="T666" id="Seg_7762" s="T665">de-ʔ</ta>
            <ta e="T667" id="Seg_7763" s="T666">kăreški-ʔi</ta>
            <ta e="T668" id="Seg_7764" s="T667">a</ta>
            <ta e="T669" id="Seg_7765" s="T668">tănan</ta>
            <ta e="T670" id="Seg_7766" s="T669">verški-ʔi</ta>
            <ta e="T671" id="Seg_7767" s="T670">dĭgəttə</ta>
            <ta e="T672" id="Seg_7768" s="T671">budəj</ta>
            <ta e="T674" id="Seg_7769" s="T672">ipek</ta>
            <ta e="T675" id="Seg_7770" s="T674">budəj</ta>
            <ta e="T676" id="Seg_7771" s="T675">ipek</ta>
            <ta e="T677" id="Seg_7772" s="T676">kuʔ-pi-ʔi</ta>
            <ta e="T678" id="Seg_7773" s="T677">dĭ</ta>
            <ta e="T679" id="Seg_7774" s="T678">özer-bi</ta>
            <ta e="T680" id="Seg_7775" s="T679">jakše</ta>
            <ta e="T681" id="Seg_7776" s="T680">ugandə</ta>
            <ta e="T682" id="Seg_7777" s="T681">i-bi</ta>
            <ta e="T683" id="Seg_7778" s="T682">dĭ</ta>
            <ta e="T684" id="Seg_7779" s="T683">kuza</ta>
            <ta e="T685" id="Seg_7780" s="T684">i-bi</ta>
            <ta e="T686" id="Seg_7781" s="T685">verški-ʔi</ta>
            <ta e="T687" id="Seg_7782" s="T686">a</ta>
            <ta e="T688" id="Seg_7783" s="T687">urgaːba-nə</ta>
            <ta e="T689" id="Seg_7784" s="T688">kăreški-ʔi</ta>
            <ta e="T690" id="Seg_7785" s="T689">i</ta>
            <ta e="T691" id="Seg_7786" s="T690">dĭgəttə</ta>
            <ta e="T693" id="Seg_7787" s="T692">dĭ-zen</ta>
            <ta e="T694" id="Seg_7788" s="T693">bar</ta>
            <ta e="T695" id="Seg_7789" s="T694">družba-t</ta>
            <ta e="T696" id="Seg_7790" s="T695">kü-lam-bi</ta>
            <ta e="T698" id="Seg_7791" s="T697">amno-laʔ-pi</ta>
            <ta e="T699" id="Seg_7792" s="T698">büzʼe</ta>
            <ta e="T700" id="Seg_7793" s="T699">da</ta>
            <ta e="T701" id="Seg_7794" s="T700">nüke</ta>
            <ta e="T702" id="Seg_7795" s="T701">dĭ-zeŋ-gən</ta>
            <ta e="T703" id="Seg_7796" s="T702">es-seŋ-də</ta>
            <ta e="T704" id="Seg_7797" s="T703">ej</ta>
            <ta e="T705" id="Seg_7798" s="T704">i-bi-ʔi</ta>
            <ta e="T706" id="Seg_7799" s="T705">onʼiʔ</ta>
            <ta e="T707" id="Seg_7800" s="T706">sĭre</ta>
            <ta e="T708" id="Seg_7801" s="T707">bar</ta>
            <ta e="T709" id="Seg_7802" s="T708">saʔmə-luʔ-pi</ta>
            <ta e="T710" id="Seg_7803" s="T709">ugandə</ta>
            <ta e="T711" id="Seg_7804" s="T710">urgo</ta>
            <ta e="T712" id="Seg_7805" s="T711">es-seŋ</ta>
            <ta e="T713" id="Seg_7806" s="T712">bar</ta>
            <ta e="T714" id="Seg_7807" s="T713">girgit</ta>
            <ta e="T715" id="Seg_7808" s="T714">bar</ta>
            <ta e="T719" id="Seg_7809" s="T718">girgit</ta>
            <ta e="T720" id="Seg_7810" s="T719">bar</ta>
            <ta e="T721" id="Seg_7811" s="T720">sʼar-laʔbə-ʔjə</ta>
            <ta e="T722" id="Seg_7812" s="T721">snegurka-ʔi</ta>
            <ta e="T723" id="Seg_7813" s="T722">a-laʔbə-ʔjə</ta>
            <ta e="T724" id="Seg_7814" s="T723">bar</ta>
            <ta e="T725" id="Seg_7815" s="T724">a-laʔbə-ʔjə</ta>
            <ta e="T726" id="Seg_7816" s="T725">dĭgəttə</ta>
            <ta e="T727" id="Seg_7817" s="T726">a-bi-ʔi</ta>
            <ta e="T728" id="Seg_7818" s="T727">urgo</ta>
            <ta e="T729" id="Seg_7819" s="T728">nüke</ta>
            <ta e="T730" id="Seg_7820" s="T729">a</ta>
            <ta e="T731" id="Seg_7821" s="T730">büzʼe</ta>
            <ta e="T732" id="Seg_7822" s="T731">üge</ta>
            <ta e="T733" id="Seg_7823" s="T732">măndo-bi</ta>
            <ta e="T734" id="Seg_7824" s="T733">măndo-bi</ta>
            <ta e="T735" id="Seg_7825" s="T734">kan-žə-bəj</ta>
            <ta e="T737" id="Seg_7826" s="T736">tăn-zi</ta>
            <ta e="T738" id="Seg_7827" s="T737">nʼiʔtə</ta>
            <ta e="T739" id="Seg_7828" s="T738">nüke-nə</ta>
            <ta e="T740" id="Seg_7829" s="T739">măl-lia</ta>
            <ta e="T741" id="Seg_7830" s="T740">no</ta>
            <ta e="T742" id="Seg_7831" s="T741">kan-žə-bəj</ta>
            <ta e="T744" id="Seg_7832" s="T743">kam-bi-ʔi</ta>
            <ta e="T745" id="Seg_7833" s="T744">măndo-bi-ʔi</ta>
            <ta e="T746" id="Seg_7834" s="T745">dĭgəttə</ta>
            <ta e="T747" id="Seg_7835" s="T746">davaj</ta>
            <ta e="T748" id="Seg_7836" s="T747">a-zittə</ta>
            <ta e="T749" id="Seg_7837" s="T748">koʔbdo</ta>
            <ta e="T751" id="Seg_7838" s="T749">a-bi-ʔi</ta>
            <ta e="T752" id="Seg_7839" s="T751">ulu</ta>
            <ta e="T753" id="Seg_7840" s="T752">a-bi-ʔi</ta>
            <ta e="T754" id="Seg_7841" s="T753">sima-ʔi</ta>
            <ta e="T755" id="Seg_7842" s="T754">püje-t</ta>
            <ta e="T756" id="Seg_7843" s="T755">aŋ-də</ta>
            <ta e="T757" id="Seg_7844" s="T756">sima-t</ta>
            <ta e="T758" id="Seg_7845" s="T757">dĭgəttə</ta>
            <ta e="T759" id="Seg_7846" s="T758">kös-si</ta>
            <ta e="T760" id="Seg_7847" s="T759">bar</ta>
            <ta e="T761" id="Seg_7848" s="T760">sima-n</ta>
            <ta e="T762" id="Seg_7849" s="T761">toʔ-ndə</ta>
            <ta e="T763" id="Seg_7850" s="T762">dʼüʔ-pi-ʔi</ta>
            <ta e="T764" id="Seg_7851" s="T763">dĭgəttə</ta>
            <ta e="T765" id="Seg_7852" s="T764">dĭ</ta>
            <ta e="T766" id="Seg_7853" s="T765">sima-t-si</ta>
            <ta e="T767" id="Seg_7854" s="T766">mʼeŋgəl-luʔ-pi</ta>
            <ta e="T768" id="Seg_7855" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_7856" s="T768">büzʼe</ta>
            <ta e="T770" id="Seg_7857" s="T769">kand-laʔbə</ta>
            <ta e="T772" id="Seg_7858" s="T771">kam-bi</ta>
            <ta e="T773" id="Seg_7859" s="T772">tura-nə</ta>
            <ta e="T774" id="Seg_7860" s="T773">a</ta>
            <ta e="T775" id="Seg_7861" s="T774">dĭ</ta>
            <ta e="T776" id="Seg_7862" s="T775">dĭ-zeŋ</ta>
            <ta e="T778" id="Seg_7863" s="T777">tože</ta>
            <ta e="T779" id="Seg_7864" s="T778">dĭ-zi</ta>
            <ta e="T780" id="Seg_7865" s="T779">kam-bi-ʔi</ta>
            <ta e="T781" id="Seg_7866" s="T780">tura-nə</ta>
            <ta e="T782" id="Seg_7867" s="T781">bar</ta>
            <ta e="T783" id="Seg_7868" s="T782">ugandə</ta>
            <ta e="T784" id="Seg_7869" s="T783">dĭʔ-nə</ta>
            <ta e="T785" id="Seg_7870" s="T784">jakše</ta>
            <ta e="T786" id="Seg_7871" s="T785">mo-lam-bi</ta>
            <ta e="T787" id="Seg_7872" s="T786">ešši</ta>
            <ta e="T789" id="Seg_7873" s="T788">bar</ta>
            <ta e="T790" id="Seg_7874" s="T789">i-bi-ʔi</ta>
            <ta e="T791" id="Seg_7875" s="T790">dĭʔ-nə</ta>
            <ta e="T793" id="Seg_7876" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_7877" s="T793">dĭʔ-nə</ta>
            <ta e="T795" id="Seg_7878" s="T794">jama-ʔi</ta>
            <ta e="T796" id="Seg_7879" s="T795">i-bi-ʔi</ta>
            <ta e="T797" id="Seg_7880" s="T796">kuvazə-ʔi</ta>
            <ta e="T798" id="Seg_7881" s="T797">kömə-ʔi</ta>
            <ta e="T799" id="Seg_7882" s="T798">dĭgəttə</ta>
            <ta e="T800" id="Seg_7883" s="T799">eʔbdə-nə</ta>
            <ta e="T801" id="Seg_7884" s="T800">i-bi-ʔi</ta>
            <ta e="T803" id="Seg_7885" s="T802">ugandə</ta>
            <ta e="T804" id="Seg_7886" s="T803">kuvas</ta>
            <ta e="T806" id="Seg_7887" s="T805">gəttə</ta>
            <ta e="T807" id="Seg_7888" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_7889" s="T807">dĭgəttə</ta>
            <ta e="T809" id="Seg_7890" s="T808">ejü</ta>
            <ta e="T810" id="Seg_7891" s="T809">mo-lam-bi</ta>
            <ta e="T811" id="Seg_7892" s="T810">a</ta>
            <ta e="T812" id="Seg_7893" s="T811">dĭ</ta>
            <ta e="T813" id="Seg_7894" s="T812">koʔbdo</ta>
            <ta e="T814" id="Seg_7895" s="T813">amno-laʔbə</ta>
            <ta e="T815" id="Seg_7896" s="T814">bü</ta>
            <ta e="T816" id="Seg_7897" s="T815">mʼaŋ-naʔbə</ta>
            <ta e="T817" id="Seg_7898" s="T816">dĭ</ta>
            <ta e="T818" id="Seg_7899" s="T817">bar</ta>
            <ta e="T819" id="Seg_7900" s="T818">pim-nie</ta>
            <ta e="T820" id="Seg_7901" s="T819">a</ta>
            <ta e="T821" id="Seg_7902" s="T820">nüke-t</ta>
            <ta e="T823" id="Seg_7903" s="T822">nüke</ta>
            <ta e="T824" id="Seg_7904" s="T823">măn-də</ta>
            <ta e="T825" id="Seg_7905" s="T824">ĭmbi</ta>
            <ta e="T826" id="Seg_7906" s="T825">tăn</ta>
            <ta e="T827" id="Seg_7907" s="T826">dʼor-laʔbə-l</ta>
            <ta e="T828" id="Seg_7908" s="T827">a</ta>
            <ta e="T829" id="Seg_7909" s="T828">bos-tən</ta>
            <ta e="T830" id="Seg_7910" s="T829">bar</ta>
            <ta e="T832" id="Seg_7911" s="T831">kăjəl-də</ta>
            <ta e="T833" id="Seg_7912" s="T832">mʼaŋ-naʔbə-ʔjə</ta>
            <ta e="T834" id="Seg_7913" s="T833">kadəl-gən</ta>
            <ta e="T835" id="Seg_7914" s="T834">ta</ta>
            <ta e="T836" id="Seg_7915" s="T835">dĭgəttə</ta>
            <ta e="T838" id="Seg_7916" s="T837">bazo</ta>
            <ta e="T839" id="Seg_7917" s="T838">kuja</ta>
            <ta e="T840" id="Seg_7918" s="T839">mo-laʔ-pi</ta>
            <ta e="T842" id="Seg_7919" s="T841">noʔ</ta>
            <ta e="T843" id="Seg_7920" s="T842">özer-leʔbə-ʔjə</ta>
            <ta e="T844" id="Seg_7921" s="T843">svʼetog-əʔjə</ta>
            <ta e="T845" id="Seg_7922" s="T844">dĭgəttə</ta>
            <ta e="T846" id="Seg_7923" s="T845">šo-bi-ʔi</ta>
            <ta e="T847" id="Seg_7924" s="T846">koʔb-saŋ</ta>
            <ta e="T848" id="Seg_7925" s="T847">kan-žə-bəj</ta>
            <ta e="T849" id="Seg_7926" s="T848">miʔ-si</ta>
            <ta e="T850" id="Seg_7927" s="T849">kăšt-lia-ʔi</ta>
            <ta e="T851" id="Seg_7928" s="T850">koʔbdo-t</ta>
            <ta e="T852" id="Seg_7929" s="T851">nüke</ta>
            <ta e="T855" id="Seg_7930" s="T854">măl-lia</ta>
            <ta e="T856" id="Seg_7931" s="T855">kan-a-ʔ</ta>
            <ta e="T857" id="Seg_7932" s="T856">ĭmbi</ta>
            <ta e="T858" id="Seg_7933" s="T857">amno-laʔbə-l</ta>
            <ta e="T859" id="Seg_7934" s="T858">dʼok</ta>
            <ta e="T860" id="Seg_7935" s="T859">măn</ta>
            <ta e="T861" id="Seg_7936" s="T860">e-m</ta>
            <ta e="T862" id="Seg_7937" s="T861">kan-a-ʔ</ta>
            <ta e="T863" id="Seg_7938" s="T862">kuja</ta>
            <ta e="T864" id="Seg_7939" s="T863">ulu-m</ta>
            <ta e="T865" id="Seg_7940" s="T864">bar</ta>
            <ta e="T866" id="Seg_7941" s="T865">ĭzem-nuʔ-lə-j</ta>
            <ta e="T868" id="Seg_7942" s="T867">kuja</ta>
            <ta e="T869" id="Seg_7943" s="T868">nend-lə-j</ta>
            <ta e="T870" id="Seg_7944" s="T869">ulu-m</ta>
            <ta e="T871" id="Seg_7945" s="T870">no</ta>
            <ta e="T872" id="Seg_7946" s="T871">plat</ta>
            <ta e="T874" id="Seg_7947" s="T873">šer-də</ta>
            <ta e="T875" id="Seg_7948" s="T874">dĭgəttə</ta>
            <ta e="T876" id="Seg_7949" s="T875">dĭ</ta>
            <ta e="T877" id="Seg_7950" s="T876">kam-bi</ta>
            <ta e="T878" id="Seg_7951" s="T877">bar</ta>
            <ta e="T879" id="Seg_7952" s="T878">koʔb-saŋ</ta>
            <ta e="T880" id="Seg_7953" s="T879">sʼar-laʔbə-ʔjə</ta>
            <ta e="T881" id="Seg_7954" s="T880">svʼetog-əʔi</ta>
            <ta e="T883" id="Seg_7955" s="T882">nĭŋgə-leʔbə-ʔjə</ta>
            <ta e="T884" id="Seg_7956" s="T883">a</ta>
            <ta e="T885" id="Seg_7957" s="T884">dĭ</ta>
            <ta e="T886" id="Seg_7958" s="T885">amno-laʔbə</ta>
            <ta e="T890" id="Seg_7959" s="T889">dĭgəttə</ta>
            <ta e="T891" id="Seg_7960" s="T890">dĭ-zeŋ</ta>
            <ta e="T892" id="Seg_7961" s="T891">em-bi-ʔi</ta>
            <ta e="T893" id="Seg_7962" s="T892">šü</ta>
            <ta e="T894" id="Seg_7963" s="T893">ugandə</ta>
            <ta e="T895" id="Seg_7964" s="T894">urgo</ta>
            <ta e="T896" id="Seg_7965" s="T895">i</ta>
            <ta e="T897" id="Seg_7966" s="T896">davaj</ta>
            <ta e="T898" id="Seg_7967" s="T897">nuʔmə-sittə</ta>
            <ta e="T899" id="Seg_7968" s="T898">šü-nə</ta>
            <ta e="T900" id="Seg_7969" s="T899">măn-lia-ʔi</ta>
            <ta e="T901" id="Seg_7970" s="T900">ĭmbi</ta>
            <ta e="T902" id="Seg_7971" s="T901">tăn</ta>
            <ta e="T903" id="Seg_7972" s="T902">ej</ta>
            <ta e="T904" id="Seg_7973" s="T903">suʔmi-lie-l</ta>
            <ta e="T905" id="Seg_7974" s="T904">a</ta>
            <ta e="T906" id="Seg_7975" s="T905">dĭ</ta>
            <ta e="T907" id="Seg_7976" s="T906">pim-nie</ta>
            <ta e="T908" id="Seg_7977" s="T907">a</ta>
            <ta e="T909" id="Seg_7978" s="T908">dĭgəttə</ta>
            <ta e="T910" id="Seg_7979" s="T909">kak</ta>
            <ta e="T911" id="Seg_7980" s="T910">dĭ-m</ta>
            <ta e="T912" id="Seg_7981" s="T911">pĭštʼer-leʔbə-ʔjə</ta>
            <ta e="T913" id="Seg_7982" s="T912">a</ta>
            <ta e="T914" id="Seg_7983" s="T913">dĭ</ta>
            <ta e="T915" id="Seg_7984" s="T914">uʔbdə-bi</ta>
            <ta e="T917" id="Seg_7985" s="T916">suʔmi-luʔ-pi</ta>
            <ta e="T918" id="Seg_7986" s="T917">i</ta>
            <ta e="T919" id="Seg_7987" s="T918">bar</ta>
            <ta e="T920" id="Seg_7988" s="T919">šü-ʔən</ta>
            <ta e="T1121" id="Seg_7989" s="T920">kal-la</ta>
            <ta e="T921" id="Seg_7990" s="T1121">dʼür-bi</ta>
            <ta e="T922" id="Seg_7991" s="T921">nʼuʔdə</ta>
            <ta e="T923" id="Seg_7992" s="T922">bar</ta>
            <ta e="T926" id="Seg_7993" s="T925">raz</ta>
            <ta e="T927" id="Seg_7994" s="T926">buzo-m</ta>
            <ta e="T928" id="Seg_7995" s="T927">măn</ta>
            <ta e="T929" id="Seg_7996" s="T928">ugandə</ta>
            <ta e="T930" id="Seg_7997" s="T929">kuvas</ta>
            <ta e="T931" id="Seg_7998" s="T930">sĭre</ta>
            <ta e="T932" id="Seg_7999" s="T931">buzo</ta>
            <ta e="T933" id="Seg_8000" s="T932">i-bi</ta>
            <ta e="T934" id="Seg_8001" s="T933">dĭgəttə</ta>
            <ta e="T935" id="Seg_8002" s="T934">mĭm-bi</ta>
            <ta e="T936" id="Seg_8003" s="T935">mĭm-bi</ta>
            <ta e="T937" id="Seg_8004" s="T936">măn</ta>
            <ta e="T938" id="Seg_8005" s="T937">nüdʼi-n</ta>
            <ta e="T939" id="Seg_8006" s="T938">bĭt-əl-zittə</ta>
            <ta e="T941" id="Seg_8007" s="T940">süt-si</ta>
            <ta e="T942" id="Seg_8008" s="T941">kirgar-ia-m</ta>
            <ta e="T943" id="Seg_8009" s="T942">kirgar-ia-m</ta>
            <ta e="T944" id="Seg_8010" s="T943">măndə-r-bia-m</ta>
            <ta e="T945" id="Seg_8011" s="T944">măndə-r-bia-m</ta>
            <ta e="T946" id="Seg_8012" s="T945">gijen=də</ta>
            <ta e="T947" id="Seg_8013" s="T946">naga</ta>
            <ta e="T948" id="Seg_8014" s="T947">no</ta>
            <ta e="T949" id="Seg_8015" s="T948">giraːm-bi</ta>
            <ta e="T951" id="Seg_8016" s="T950">buzo</ta>
            <ta e="T952" id="Seg_8017" s="T951">ej</ta>
            <ta e="T953" id="Seg_8018" s="T952">tĭm-nie-m</ta>
            <ta e="T954" id="Seg_8019" s="T953">ertə-n</ta>
            <ta e="T955" id="Seg_8020" s="T954">uʔbdə-bia-m</ta>
            <ta e="T956" id="Seg_8021" s="T955">vezʼdʼe</ta>
            <ta e="T957" id="Seg_8022" s="T956">mĭm-bie-m</ta>
            <ta e="T958" id="Seg_8023" s="T957">dʼaga-gən</ta>
            <ta e="T959" id="Seg_8024" s="T958">mĭm-bie-m</ta>
            <ta e="T960" id="Seg_8025" s="T959">tenə-bie-m</ta>
            <ta e="T961" id="Seg_8026" s="T960">dĭ</ta>
            <ta e="T962" id="Seg_8027" s="T961">saʔmə-luʔ-pi</ta>
            <ta e="T963" id="Seg_8028" s="T962">bü-nə</ta>
            <ta e="T964" id="Seg_8029" s="T963">dĭn</ta>
            <ta e="T965" id="Seg_8030" s="T964">naga</ta>
            <ta e="T967" id="Seg_8031" s="T966">tenə-bie-m</ta>
            <ta e="T968" id="Seg_8032" s="T967">volk</ta>
            <ta e="T969" id="Seg_8033" s="T968">am-nuʔ-pi</ta>
            <ta e="T970" id="Seg_8034" s="T969">dʼok</ta>
            <ta e="T971" id="Seg_8035" s="T970">ej</ta>
            <ta e="T972" id="Seg_8036" s="T971">volk</ta>
            <ta e="T974" id="Seg_8037" s="T973">ej</ta>
            <ta e="T975" id="Seg_8038" s="T974">am-nuʔ-pi</ta>
            <ta e="T976" id="Seg_8039" s="T975">dĭgəttə</ta>
            <ta e="T977" id="Seg_8040" s="T976">dön</ta>
            <ta e="T978" id="Seg_8041" s="T977">il</ta>
            <ta e="T979" id="Seg_8042" s="T978">noʔ</ta>
            <ta e="T980" id="Seg_8043" s="T979">jaʔ-pi-ʔi</ta>
            <ta e="T981" id="Seg_8044" s="T980">dĭ-zeŋ</ta>
            <ta e="T983" id="Seg_8045" s="T982">i-bi-ʔi</ta>
            <ta e="T984" id="Seg_8046" s="T983">üjü-ttə</ta>
            <ta e="T986" id="Seg_8047" s="T985">sar-bi-ʔi</ta>
            <ta e="T987" id="Seg_8048" s="T986">kum-bi-ʔi</ta>
            <ta e="T988" id="Seg_8049" s="T987">maʔ-ndə</ta>
            <ta e="T989" id="Seg_8050" s="T988">šide</ta>
            <ta e="T990" id="Seg_8051" s="T989">üjü-t-si</ta>
            <ta e="T991" id="Seg_8052" s="T990">volk</ta>
            <ta e="T992" id="Seg_8053" s="T991">i-bi</ta>
            <ta e="T993" id="Seg_8054" s="T992">măn</ta>
            <ta e="T995" id="Seg_8055" s="T994">tugan-bə</ta>
            <ta e="T996" id="Seg_8056" s="T995">šo-bi</ta>
            <ta e="T997" id="Seg_8057" s="T996">Kanokler-də</ta>
            <ta e="T998" id="Seg_8058" s="T997">i</ta>
            <ta e="T999" id="Seg_8059" s="T998">nörbə-lie</ta>
            <ta e="T1001" id="Seg_8060" s="T1000">šide</ta>
            <ta e="T1002" id="Seg_8061" s="T1001">tüžöj</ta>
            <ta e="T1003" id="Seg_8062" s="T1002">i-bi</ta>
            <ta e="T1004" id="Seg_8063" s="T1003">il-gən</ta>
            <ta e="T1005" id="Seg_8064" s="T1004">ugandə</ta>
            <ta e="T1006" id="Seg_8065" s="T1005">il</ta>
            <ta e="T1008" id="Seg_8066" s="T1007">il</ta>
            <ta e="T1009" id="Seg_8067" s="T1008">ugandə</ta>
            <ta e="T1011" id="Seg_8068" s="T1010">sil-də</ta>
            <ta e="T1012" id="Seg_8069" s="T1011">iʔgö</ta>
            <ta e="T1013" id="Seg_8070" s="T1012">a</ta>
            <ta e="T1014" id="Seg_8071" s="T1013">miʔnʼibeʔ</ta>
            <ta e="T1015" id="Seg_8072" s="T1014">amno-lə-j</ta>
            <ta e="T1016" id="Seg_8073" s="T1015">bar</ta>
            <ta e="T1017" id="Seg_8074" s="T1016">ĭmbi</ta>
            <ta e="T1018" id="Seg_8075" s="T1017">todam</ta>
            <ta e="T1019" id="Seg_8076" s="T1018">mo-la-ʔjə</ta>
            <ta e="T1020" id="Seg_8077" s="T1019">noʔ</ta>
            <ta e="T1021" id="Seg_8078" s="T1020">ej</ta>
            <ta e="T1022" id="Seg_8079" s="T1021">am-nia</ta>
            <ta e="T1023" id="Seg_8080" s="T1022">bü</ta>
            <ta e="T1024" id="Seg_8081" s="T1023">ej</ta>
            <ta e="T1025" id="Seg_8082" s="T1024">bĭt-lie</ta>
            <ta e="T1026" id="Seg_8083" s="T1025">amga</ta>
            <ta e="T1027" id="Seg_8084" s="T1026">am-naʔbə</ta>
            <ta e="T1028" id="Seg_8085" s="T1027">baška</ta>
            <ta e="T1029" id="Seg_8086" s="T1028">tüžöj</ta>
            <ta e="T1030" id="Seg_8087" s="T1029">šo-lə-ʔjə</ta>
            <ta e="T1031" id="Seg_8088" s="T1030">bar</ta>
            <ta e="T1032" id="Seg_8089" s="T1031">noʔ</ta>
            <ta e="T1033" id="Seg_8090" s="T1032">am-nuʔ-lə-ʔjə</ta>
            <ta e="T1034" id="Seg_8091" s="T1033">a</ta>
            <ta e="T1035" id="Seg_8092" s="T1034">dĭ</ta>
            <ta e="T1036" id="Seg_8093" s="T1035">ej</ta>
            <ta e="T1037" id="Seg_8094" s="T1036">am-nia</ta>
            <ta e="T1039" id="Seg_8095" s="T1038">ej</ta>
            <ta e="T1040" id="Seg_8096" s="T1039">tĭmne-m</ta>
            <ta e="T1041" id="Seg_8097" s="T1040">măn-də</ta>
            <ta e="T1042" id="Seg_8098" s="T1041">šeden</ta>
            <ta e="T1043" id="Seg_8099" s="T1042">dĭrgit</ta>
            <ta e="T1044" id="Seg_8100" s="T1043">ali</ta>
            <ta e="T1045" id="Seg_8101" s="T1044">ĭmbi</ta>
            <ta e="T1046" id="Seg_8102" s="T1045">dĭrgit</ta>
            <ta e="T1047" id="Seg_8103" s="T1046">miʔ</ta>
            <ta e="T1048" id="Seg_8104" s="T1047">i-bi-beʔ</ta>
            <ta e="T1049" id="Seg_8105" s="T1048">tĭ-n</ta>
            <ta e="T1051" id="Seg_8106" s="T1050">sʼestra-ndə</ta>
            <ta e="T1052" id="Seg_8107" s="T1051">tüžöj-də</ta>
            <ta e="T1053" id="Seg_8108" s="T1052">dĭ</ta>
            <ta e="T1054" id="Seg_8109" s="T1053">vedʼ</ta>
            <ta e="T1055" id="Seg_8110" s="T1054">ĭmbi=də</ta>
            <ta e="T1056" id="Seg_8111" s="T1055">ej</ta>
            <ta e="T1057" id="Seg_8112" s="T1056">tĭmne-t</ta>
            <ta e="T1058" id="Seg_8113" s="T1057">tože</ta>
            <ta e="T1059" id="Seg_8114" s="T1058">tüžöj</ta>
            <ta e="T1060" id="Seg_8115" s="T1059">dăre</ta>
            <ta e="T1061" id="Seg_8116" s="T1060">mo-lam-bi</ta>
            <ta e="T1062" id="Seg_8117" s="T1061">bar</ta>
            <ta e="T1063" id="Seg_8118" s="T1062">todam</ta>
            <ta e="T1064" id="Seg_8119" s="T1063">mo-bi</ta>
            <ta e="T1065" id="Seg_8120" s="T1064">a</ta>
            <ta e="T1066" id="Seg_8121" s="T1065">boʔ</ta>
            <ta e="T1067" id="Seg_8122" s="T1066">sil-də</ta>
            <ta e="T1068" id="Seg_8123" s="T1067">iʔgö</ta>
            <ta e="T1070" id="Seg_8124" s="T1069">i-bi</ta>
            <ta e="T1071" id="Seg_8125" s="T1070">amno-bi</ta>
            <ta e="T1072" id="Seg_8126" s="T1071">miʔnʼibeʔ</ta>
            <ta e="T1074" id="Seg_8127" s="T1073">todam</ta>
            <ta e="T1075" id="Seg_8128" s="T1074">mo-lam-bi</ta>
            <ta e="T1077" id="Seg_8129" s="T1076">dĭ</ta>
            <ta e="T1078" id="Seg_8130" s="T1077">onʼiʔ</ta>
            <ta e="T1079" id="Seg_8131" s="T1078">koʔbdo-zi</ta>
            <ta e="T1080" id="Seg_8132" s="T1079">amno-laʔ-pi</ta>
            <ta e="T1081" id="Seg_8133" s="T1080">dĭgəttə</ta>
            <ta e="T1084" id="Seg_8134" s="T1083">i-bi</ta>
            <ta e="T1085" id="Seg_8135" s="T1084">možet</ta>
            <ta e="T1086" id="Seg_8136" s="T1085">dĭ</ta>
            <ta e="T1088" id="Seg_8137" s="T1087">ĭmbi=nʼibudʼ</ta>
            <ta e="T1090" id="Seg_8138" s="T1089">tĭl-bi</ta>
            <ta e="T1091" id="Seg_8139" s="T1090">i</ta>
            <ta e="T1092" id="Seg_8140" s="T1091">tüj</ta>
            <ta e="T1094" id="Seg_8141" s="T1093">tüžöj-də</ta>
            <ta e="T1095" id="Seg_8142" s="T1094">bar</ta>
            <ta e="T1096" id="Seg_8143" s="T1095">ej</ta>
            <ta e="T1097" id="Seg_8144" s="T1096">nu-lia-ʔi</ta>
            <ta e="T1099" id="Seg_8145" s="T1098">šiʔ</ta>
            <ta e="T1101" id="Seg_8146" s="T1100">bar</ta>
            <ta e="T1102" id="Seg_8147" s="T1101">karəldʼan</ta>
            <ta e="T1105" id="Seg_8148" s="T1104">ši</ta>
            <ta e="T1106" id="Seg_8149" s="T1105">karəldʼan</ta>
            <ta e="T1122" id="Seg_8150" s="T1106">kal-la</ta>
            <ta e="T1107" id="Seg_8151" s="T1122">dʼür-lə-beʔ</ta>
            <ta e="T1123" id="Seg_8152" s="T1107">kal-la</ta>
            <ta e="T1108" id="Seg_8153" s="T1123">dʼür-lə-leʔ</ta>
            <ta e="T1109" id="Seg_8154" s="T1108">davajtʼe</ta>
            <ta e="T1110" id="Seg_8155" s="T1109">kamro-la-baʔ</ta>
            <ta e="T1111" id="Seg_8156" s="T1110">i</ta>
            <ta e="T1112" id="Seg_8157" s="T1111">pănar-la-baʔ</ta>
            <ta e="T1113" id="Seg_8158" s="T1112">dĭgəttə</ta>
            <ta e="T1114" id="Seg_8159" s="T1113">kan-gaʔ</ta>
            <ta e="T1115" id="Seg_8160" s="T1114">kudaj-zi</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T1" id="Seg_8161" s="T0">nʼergö-laʔbə</ta>
            <ta e="T2" id="Seg_8162" s="T1">bar</ta>
            <ta e="T3" id="Seg_8163" s="T2">kirgaːr-laʔbə</ta>
            <ta e="T4" id="Seg_8164" s="T3">amno-lV-j</ta>
            <ta e="T5" id="Seg_8165" s="T4">tak</ta>
            <ta e="T6" id="Seg_8166" s="T5">tʼo</ta>
            <ta e="T7" id="Seg_8167" s="T6">tʼo</ta>
            <ta e="T8" id="Seg_8168" s="T7">tego-laʔbə</ta>
            <ta e="T9" id="Seg_8169" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_8170" s="T9">multuk</ta>
            <ta e="T12" id="Seg_8171" s="T11">girgit</ta>
            <ta e="T13" id="Seg_8172" s="T12">dĭrgit</ta>
            <ta e="T16" id="Seg_8173" s="T15">köten-gəndə</ta>
            <ta e="T18" id="Seg_8174" s="T17">dĭ</ta>
            <ta e="T19" id="Seg_8175" s="T18">multuk</ta>
            <ta e="T21" id="Seg_8176" s="T20">kamən</ta>
            <ta e="T22" id="Seg_8177" s="T21">miʔ</ta>
            <ta e="T23" id="Seg_8178" s="T22">iʔgö</ta>
            <ta e="T24" id="Seg_8179" s="T23">i-bi-bAʔ</ta>
            <ta e="T25" id="Seg_8180" s="T24">nu-zAŋ</ta>
            <ta e="T26" id="Seg_8181" s="T25">măn</ta>
            <ta e="T27" id="Seg_8182" s="T26">mĭn-bi-m</ta>
            <ta e="T28" id="Seg_8183" s="T27">urgo</ta>
            <ta e="T29" id="Seg_8184" s="T28">măja-Kən</ta>
            <ta e="T30" id="Seg_8185" s="T29">Belăgorja-Kən</ta>
            <ta e="T31" id="Seg_8186" s="T30">dĭn</ta>
            <ta e="T32" id="Seg_8187" s="T31">kola</ta>
            <ta e="T33" id="Seg_8188" s="T32">dʼabə-bi-m</ta>
            <ta e="T34" id="Seg_8189" s="T33">i</ta>
            <ta e="T35" id="Seg_8190" s="T34">uja</ta>
            <ta e="T36" id="Seg_8191" s="T35">kut-bi-m</ta>
            <ta e="T37" id="Seg_8192" s="T36">dĭgəttə</ta>
            <ta e="T38" id="Seg_8193" s="T37">bar</ta>
            <ta e="T39" id="Seg_8194" s="T38">măja-jəʔ</ta>
            <ta e="T40" id="Seg_8195" s="T39">sagər-jəʔ</ta>
            <ta e="T41" id="Seg_8196" s="T40">ma-luʔbdə-bi-jəʔ</ta>
            <ta e="T42" id="Seg_8197" s="T41">măn</ta>
            <ta e="T43" id="Seg_8198" s="T42">šal-m</ta>
            <ta e="T44" id="Seg_8199" s="T43">už</ta>
            <ta e="T45" id="Seg_8200" s="T44">naga</ta>
            <ta e="T46" id="Seg_8201" s="T45">bar</ta>
            <ta e="T47" id="Seg_8202" s="T46">kü-laːm-bi</ta>
            <ta e="T48" id="Seg_8203" s="T47">a</ta>
            <ta e="T49" id="Seg_8204" s="T48">dĭn</ta>
            <ta e="T50" id="Seg_8205" s="T49">gijen</ta>
            <ta e="T51" id="Seg_8206" s="T50">maʔ-jəʔ</ta>
            <ta e="T52" id="Seg_8207" s="T51">nu-gA-bi-jəʔ</ta>
            <ta e="T53" id="Seg_8208" s="T52">bar</ta>
            <ta e="T54" id="Seg_8209" s="T53">saʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T55" id="Seg_8210" s="T54">i</ta>
            <ta e="T56" id="Seg_8211" s="T55">bar</ta>
            <ta e="T57" id="Seg_8212" s="T56">kü-laːm-bi-jəʔ</ta>
            <ta e="T58" id="Seg_8213" s="T57">tolʼko</ta>
            <ta e="T59" id="Seg_8214" s="T58">šo</ta>
            <ta e="T60" id="Seg_8215" s="T59">păjdə-luʔbdə-bi-jəʔ</ta>
            <ta e="T62" id="Seg_8216" s="T61">tüj</ta>
            <ta e="T64" id="Seg_8217" s="T63">ĭmbi=də</ta>
            <ta e="T65" id="Seg_8218" s="T64">ej</ta>
            <ta e="T66" id="Seg_8219" s="T65">ku-liA-m</ta>
            <ta e="T67" id="Seg_8220" s="T66">ĭmbi=də</ta>
            <ta e="T68" id="Seg_8221" s="T67">ej</ta>
            <ta e="T70" id="Seg_8222" s="T69">nʼilgö-liA-m</ta>
            <ta e="T71" id="Seg_8223" s="T70">bar</ta>
            <ta e="T73" id="Seg_8224" s="T72">šal-m</ta>
            <ta e="T1117" id="Seg_8225" s="T73">kan-lAʔ</ta>
            <ta e="T74" id="Seg_8226" s="T1117">tʼür-bi</ta>
            <ta e="T75" id="Seg_8227" s="T74">tüj</ta>
            <ta e="T76" id="Seg_8228" s="T75">măn</ta>
            <ta e="T77" id="Seg_8229" s="T76">unʼə-m</ta>
            <ta e="T78" id="Seg_8230" s="T77">amno-laʔbə-m</ta>
            <ta e="T79" id="Seg_8231" s="T78">šində=də</ta>
            <ta e="T80" id="Seg_8232" s="T79">naga</ta>
            <ta e="T81" id="Seg_8233" s="T80">bar</ta>
            <ta e="T82" id="Seg_8234" s="T81">kü-laːm-bi-jəʔ</ta>
            <ta e="T83" id="Seg_8235" s="T82">măn</ta>
            <ta e="T84" id="Seg_8236" s="T83">nu-zAŋ-m</ta>
            <ta e="T86" id="Seg_8237" s="T85">tüj</ta>
            <ta e="T87" id="Seg_8238" s="T86">bar</ta>
            <ta e="T88" id="Seg_8239" s="T87">aʔtʼi-zAŋ-bə</ta>
            <ta e="T89" id="Seg_8240" s="T88">bar</ta>
            <ta e="T90" id="Seg_8241" s="T89">ma-luʔbdə-bi</ta>
            <ta e="T91" id="Seg_8242" s="T90">bar</ta>
            <ta e="T92" id="Seg_8243" s="T91">noʔ-ziʔ</ta>
            <ta e="T93" id="Seg_8244" s="T92">özer-luʔbdə-bi</ta>
            <ta e="T95" id="Seg_8245" s="T94">măn</ta>
            <ta e="T96" id="Seg_8246" s="T95">ugaːndə</ta>
            <ta e="T97" id="Seg_8247" s="T96">kuvas</ta>
            <ta e="T98" id="Seg_8248" s="T97">mo-bi-m</ta>
            <ta e="T99" id="Seg_8249" s="T98">a</ta>
            <ta e="T100" id="Seg_8250" s="T99">gibər</ta>
            <ta e="T101" id="Seg_8251" s="T100">dĭ</ta>
            <ta e="T102" id="Seg_8252" s="T101">kuvas</ta>
            <ta e="T1118" id="Seg_8253" s="T102">kan-lAʔ</ta>
            <ta e="T103" id="Seg_8254" s="T1118">tʼür-bi</ta>
            <ta e="T1119" id="Seg_8255" s="T103">kan-lAʔ</ta>
            <ta e="T104" id="Seg_8256" s="T1119">tʼür-bi</ta>
            <ta e="T105" id="Seg_8257" s="T104">urgo</ta>
            <ta e="T106" id="Seg_8258" s="T105">măja-zAŋ-Tə</ta>
            <ta e="T107" id="Seg_8259" s="T106">i</ta>
            <ta e="T108" id="Seg_8260" s="T107">gijen=də</ta>
            <ta e="T109" id="Seg_8261" s="T108">naga</ta>
            <ta e="T111" id="Seg_8262" s="T110">tüj</ta>
            <ta e="T112" id="Seg_8263" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_8264" s="T112">kamən=də</ta>
            <ta e="T114" id="Seg_8265" s="T113">ej</ta>
            <ta e="T115" id="Seg_8266" s="T114">šo-lV-j</ta>
            <ta e="T118" id="Seg_8267" s="T117">măn</ta>
            <ta e="T119" id="Seg_8268" s="T118">uʔbdə-bi-m</ta>
            <ta e="T120" id="Seg_8269" s="T119">pereulăk-Tə</ta>
            <ta e="T122" id="Seg_8270" s="T121">šonə-gA</ta>
            <ta e="T123" id="Seg_8271" s="T122">aga</ta>
            <ta e="T124" id="Seg_8272" s="T123">măn</ta>
            <ta e="T125" id="Seg_8273" s="T124">măn-ntə-m</ta>
            <ta e="T126" id="Seg_8274" s="T125">ugaːndə</ta>
            <ta e="T127" id="Seg_8275" s="T126">urgo</ta>
            <ta e="T128" id="Seg_8276" s="T127">kuza</ta>
            <ta e="T129" id="Seg_8277" s="T128">šonə-gA</ta>
            <ta e="T130" id="Seg_8278" s="T129">aparat-ziʔ</ta>
            <ta e="T131" id="Seg_8279" s="T130">tura-jəʔ</ta>
            <ta e="T132" id="Seg_8280" s="T131">kür-laʔbə</ta>
            <ta e="T133" id="Seg_8281" s="T132">mĭn-gA</ta>
            <ta e="T134" id="Seg_8282" s="T133">dĭgəttə</ta>
            <ta e="T135" id="Seg_8283" s="T134">măn</ta>
            <ta e="T136" id="Seg_8284" s="T135">nu-bi-m</ta>
            <ta e="T137" id="Seg_8285" s="T136">tura-gəʔ</ta>
            <ta e="T138" id="Seg_8286" s="T137">toʔ-gəndə</ta>
            <ta e="T139" id="Seg_8287" s="T138">dĭ</ta>
            <ta e="T141" id="Seg_8288" s="T140">bar</ta>
            <ta e="T142" id="Seg_8289" s="T141">măna</ta>
            <ta e="T144" id="Seg_8290" s="T143">kür-bi</ta>
            <ta e="T146" id="Seg_8291" s="T145">dĭn</ta>
            <ta e="T147" id="Seg_8292" s="T146">nüke-Kən</ta>
            <ta e="T148" id="Seg_8293" s="T147">kola-t</ta>
            <ta e="T149" id="Seg_8294" s="T148">i-bi</ta>
            <ta e="T150" id="Seg_8295" s="T149">dĭn</ta>
            <ta e="T152" id="Seg_8296" s="T151">dĭ-n</ta>
            <ta e="T153" id="Seg_8297" s="T152">kola-t</ta>
            <ta e="T154" id="Seg_8298" s="T153">i-bi</ta>
            <ta e="T155" id="Seg_8299" s="T154">dĭ-n</ta>
            <ta e="T156" id="Seg_8300" s="T155">mej-t</ta>
            <ta e="T157" id="Seg_8301" s="T156">dĭgəttə</ta>
            <ta e="T158" id="Seg_8302" s="T157">dĭ-Tə</ta>
            <ta e="T159" id="Seg_8303" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_8304" s="T159">măn-ntə</ta>
            <ta e="T161" id="Seg_8305" s="T160">măn</ta>
            <ta e="T162" id="Seg_8306" s="T161">tănan</ta>
            <ta e="T163" id="Seg_8307" s="T162">kola</ta>
            <ta e="T164" id="Seg_8308" s="T163">det-lV-m</ta>
            <ta e="T165" id="Seg_8309" s="T164">teinen</ta>
            <ta e="T166" id="Seg_8310" s="T165">a</ta>
            <ta e="T167" id="Seg_8311" s="T166">măn</ta>
            <ta e="T168" id="Seg_8312" s="T167">šo-bi-m</ta>
            <ta e="T169" id="Seg_8313" s="T168">tăn</ta>
            <ta e="T170" id="Seg_8314" s="T169">kola-l</ta>
            <ta e="T171" id="Seg_8315" s="T170">mej-t</ta>
            <ta e="T172" id="Seg_8316" s="T171">i-bi</ta>
            <ta e="T173" id="Seg_8317" s="T172">măn</ta>
            <ta e="T174" id="Seg_8318" s="T173">ej</ta>
            <ta e="T175" id="Seg_8319" s="T174">i-bi-m</ta>
            <ta e="T177" id="Seg_8320" s="T176">ĭmbi</ta>
            <ta e="T178" id="Seg_8321" s="T177">dĭ</ta>
            <ta e="T179" id="Seg_8322" s="T178">măna</ta>
            <ta e="T180" id="Seg_8323" s="T179">mĭ-lV-j</ta>
            <ta e="T181" id="Seg_8324" s="T180">idʼiʔeʔe</ta>
            <ta e="T182" id="Seg_8325" s="T181">nu</ta>
            <ta e="T183" id="Seg_8326" s="T182">idʼiʔeʔe</ta>
            <ta e="T184" id="Seg_8327" s="T183">kabarləj</ta>
            <ta e="T185" id="Seg_8328" s="T184">dĭ-zAŋ</ta>
            <ta e="T186" id="Seg_8329" s="T185">bos-də</ta>
            <ta e="T187" id="Seg_8330" s="T186">aktʼa</ta>
            <ta e="T188" id="Seg_8331" s="T187">aktʼa-Tə</ta>
            <ta e="T190" id="Seg_8332" s="T189">sădar-bi-jəʔ</ta>
            <ta e="T191" id="Seg_8333" s="T190">a</ta>
            <ta e="T192" id="Seg_8334" s="T191">tăn</ta>
            <ta e="T193" id="Seg_8335" s="T192">ej</ta>
            <ta e="T194" id="Seg_8336" s="T193">mĭ-bi-l</ta>
            <ta e="T195" id="Seg_8337" s="T194">aktʼa</ta>
            <ta e="T196" id="Seg_8338" s="T195">i</ta>
            <ta e="T198" id="Seg_8339" s="T197">i</ta>
            <ta e="T199" id="Seg_8340" s="T198">dĭ-n</ta>
            <ta e="T200" id="Seg_8341" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_8342" s="T200">kumən</ta>
            <ta e="T202" id="Seg_8343" s="T201">mĭ-lV-jəʔ</ta>
            <ta e="T203" id="Seg_8344" s="T202">kabarləj</ta>
            <ta e="T205" id="Seg_8345" s="T204">dĭgəttə</ta>
            <ta e="T206" id="Seg_8346" s="T205">măn</ta>
            <ta e="T207" id="Seg_8347" s="T206">u</ta>
            <ta e="T208" id="Seg_8348" s="T207">kan-bi-m</ta>
            <ta e="T209" id="Seg_8349" s="T208">Anissʼa-Tə</ta>
            <ta e="T210" id="Seg_8350" s="T209">dĭ</ta>
            <ta e="T211" id="Seg_8351" s="T210">măn-ntə</ta>
            <ta e="T212" id="Seg_8352" s="T211">Jelʼa</ta>
            <ta e="T213" id="Seg_8353" s="T212">surar-bi</ta>
            <ta e="T214" id="Seg_8354" s="T213">munəj-jəʔ</ta>
            <ta e="T215" id="Seg_8355" s="T214">măn</ta>
            <ta e="T216" id="Seg_8356" s="T215">oʔbdə-bi-m</ta>
            <ta e="T217" id="Seg_8357" s="T216">biəʔ</ta>
            <ta e="T218" id="Seg_8358" s="T217">munəj-jəʔ</ta>
            <ta e="T221" id="Seg_8359" s="T220">mĭ-lV-m</ta>
            <ta e="T222" id="Seg_8360" s="T221">biəʔ</ta>
            <ta e="T224" id="Seg_8361" s="T223">onʼiʔ</ta>
            <ta e="T225" id="Seg_8362" s="T224">munəj-jəʔ</ta>
            <ta e="T226" id="Seg_8363" s="T225">biəʔ</ta>
            <ta e="T227" id="Seg_8364" s="T226">kăpʼejka-ziʔ</ta>
            <ta e="T228" id="Seg_8365" s="T227">mĭ-lV-m</ta>
            <ta e="T229" id="Seg_8366" s="T228">dĭ-Tə</ta>
            <ta e="T231" id="Seg_8367" s="T230">Jelʼa</ta>
            <ta e="T232" id="Seg_8368" s="T231">măn-ntə</ta>
            <ta e="T233" id="Seg_8369" s="T232">kan-žə-bəj</ta>
            <ta e="T234" id="Seg_8370" s="T233">miʔnʼibeʔ</ta>
            <ta e="T235" id="Seg_8371" s="T234">a</ta>
            <ta e="T236" id="Seg_8372" s="T235">măn</ta>
            <ta e="T237" id="Seg_8373" s="T236">măn-ntə-m</ta>
            <ta e="T238" id="Seg_8374" s="T237">dʼok</ta>
            <ta e="T239" id="Seg_8375" s="T238">ej</ta>
            <ta e="T240" id="Seg_8376" s="T239">kan-lV-m</ta>
            <ta e="T241" id="Seg_8377" s="T240">ugaːndə</ta>
            <ta e="T242" id="Seg_8378" s="T241">kuŋgə</ta>
            <ta e="T243" id="Seg_8379" s="T242">ĭmbi</ta>
            <ta e="T244" id="Seg_8380" s="T243">kuŋgə-ʔ</ta>
            <ta e="T245" id="Seg_8381" s="T244">amnə-ʔ</ta>
            <ta e="T247" id="Seg_8382" s="T246">süjö-Tə</ta>
            <ta e="T248" id="Seg_8383" s="T247">baza-j</ta>
            <ta e="T249" id="Seg_8384" s="T248">süjö-Tə</ta>
            <ta e="T250" id="Seg_8385" s="T249">i</ta>
            <ta e="T251" id="Seg_8386" s="T250">büžü</ta>
            <ta e="T252" id="Seg_8387" s="T251">šo-lV-l</ta>
            <ta e="T253" id="Seg_8388" s="T252">dʼok</ta>
            <ta e="T254" id="Seg_8389" s="T253">măn</ta>
            <ta e="T255" id="Seg_8390" s="T254">pim-liA-m</ta>
            <ta e="T256" id="Seg_8391" s="T255">baza-j</ta>
            <ta e="T258" id="Seg_8392" s="T257">süjö-Tə</ta>
            <ta e="T259" id="Seg_8393" s="T258">amnə-zittə</ta>
            <ta e="T260" id="Seg_8394" s="T259">ato</ta>
            <ta e="T261" id="Seg_8395" s="T260">kü-laːm-lV-l</ta>
            <ta e="T262" id="Seg_8396" s="T261">dʼok</ta>
            <ta e="T263" id="Seg_8397" s="T262">ej</ta>
            <ta e="T264" id="Seg_8398" s="T263">kü-laːm-lV-l</ta>
            <ta e="T265" id="Seg_8399" s="T264">dĭn</ta>
            <ta e="T266" id="Seg_8400" s="T265">jakšə</ta>
            <ta e="T267" id="Seg_8401" s="T266">amno-zittə</ta>
            <ta e="T268" id="Seg_8402" s="T267">kak</ta>
            <ta e="T269" id="Seg_8403" s="T268">tura-Kən</ta>
            <ta e="T270" id="Seg_8404" s="T269">büžü</ta>
            <ta e="T271" id="Seg_8405" s="T270">maʔ-Tə-l</ta>
            <ta e="T272" id="Seg_8406" s="T271">šo-lV-l</ta>
            <ta e="T273" id="Seg_8407" s="T272">miʔnʼibeʔ</ta>
            <ta e="T274" id="Seg_8408" s="T273">amno-lV-l</ta>
            <ta e="T276" id="Seg_8409" s="T275">nagur-göʔ</ta>
            <ta e="T277" id="Seg_8410" s="T276">kan-lV-bAʔ</ta>
            <ta e="T279" id="Seg_8411" s="T278">nüdʼi-n</ta>
            <ta e="T280" id="Seg_8412" s="T279">kan-lV-m</ta>
            <ta e="T281" id="Seg_8413" s="T280">onʼiʔ</ta>
            <ta e="T282" id="Seg_8414" s="T281">tibi-Tə</ta>
            <ta e="T287" id="Seg_8415" s="T286">onʼiʔ</ta>
            <ta e="T288" id="Seg_8416" s="T287">kuza-Tə</ta>
            <ta e="T289" id="Seg_8417" s="T288">măn-lV-m</ta>
            <ta e="T291" id="Seg_8418" s="T290">kun-t</ta>
            <ta e="T292" id="Seg_8419" s="T291">dĭ</ta>
            <ta e="T293" id="Seg_8420" s="T292">nüke-m</ta>
            <ta e="T294" id="Seg_8421" s="T293">bălʼnʼitsa-Tə</ta>
            <ta e="T295" id="Seg_8422" s="T294">dĭ-Tə</ta>
            <ta e="T297" id="Seg_8423" s="T296">dĭ-Tə</ta>
            <ta e="T298" id="Seg_8424" s="T297">sazən</ta>
            <ta e="T299" id="Seg_8425" s="T298">mĭ-bi-jəʔ</ta>
            <ta e="T300" id="Seg_8426" s="T299">a</ta>
            <ta e="T301" id="Seg_8427" s="T300">ej</ta>
            <ta e="T304" id="Seg_8428" s="T303">kun-lV-l</ta>
            <ta e="T305" id="Seg_8429" s="T304">tak</ta>
            <ta e="T306" id="Seg_8430" s="T305">kăštə-t</ta>
            <ta e="T309" id="Seg_8431" s="T308">dĭ</ta>
            <ta e="T310" id="Seg_8432" s="T309">šo-lV-j</ta>
            <ta e="T311" id="Seg_8433" s="T310">da</ta>
            <ta e="T312" id="Seg_8434" s="T311">kun-laːm-lV-j</ta>
            <ta e="T313" id="Seg_8435" s="T312">dĭ-m</ta>
            <ta e="T314" id="Seg_8436" s="T313">dărəʔ</ta>
            <ta e="T315" id="Seg_8437" s="T314">nörbə-lV-m</ta>
            <ta e="T316" id="Seg_8438" s="T315">dĭ-Tə</ta>
            <ta e="T317" id="Seg_8439" s="T316">možet</ta>
            <ta e="T318" id="Seg_8440" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_8441" s="T318">nʼilgö-lV-j</ta>
            <ta e="T320" id="Seg_8442" s="T319">ato</ta>
            <ta e="T321" id="Seg_8443" s="T320">vedʼ</ta>
            <ta e="T322" id="Seg_8444" s="T321">tăn</ta>
            <ta e="T323" id="Seg_8445" s="T322">kujdərgan</ta>
            <ta e="T324" id="Seg_8446" s="T323">i-bi-l</ta>
            <ta e="T325" id="Seg_8447" s="T324">a</ta>
            <ta e="T326" id="Seg_8448" s="T325">il</ta>
            <ta e="T327" id="Seg_8449" s="T326">bar</ta>
            <ta e="T328" id="Seg_8450" s="T327">măn-liA</ta>
            <ta e="T330" id="Seg_8451" s="T329">kujdərgan</ta>
            <ta e="T331" id="Seg_8452" s="T330">i-bi-l</ta>
            <ta e="T332" id="Seg_8453" s="T331">bos-də</ta>
            <ta e="T333" id="Seg_8454" s="T332">ĭmbi=də</ta>
            <ta e="T334" id="Seg_8455" s="T333">ej</ta>
            <ta e="T335" id="Seg_8456" s="T334">kabažar-bi</ta>
            <ta e="T336" id="Seg_8457" s="T335">dĭ</ta>
            <ta e="T337" id="Seg_8458" s="T336">nüke-Tə</ta>
            <ta e="T339" id="Seg_8459" s="T338">a</ta>
            <ta e="T340" id="Seg_8460" s="T339">aba-l</ta>
            <ta e="T341" id="Seg_8461" s="T340">kü-laːm-bi</ta>
            <ta e="T342" id="Seg_8462" s="T341">a</ta>
            <ta e="T343" id="Seg_8463" s="T342">tura</ta>
            <ta e="T344" id="Seg_8464" s="T343">nan</ta>
            <ta e="T345" id="Seg_8465" s="T344">vedʼ</ta>
            <ta e="T346" id="Seg_8466" s="T345">tăn</ta>
            <ta e="T347" id="Seg_8467" s="T346">i-bi-l</ta>
            <ta e="T349" id="Seg_8468" s="T348">men</ta>
            <ta e="T350" id="Seg_8469" s="T349">unʼə</ta>
            <ta e="T351" id="Seg_8470" s="T350">amno-bi</ta>
            <ta e="T352" id="Seg_8471" s="T351">amno-bi</ta>
            <ta e="T354" id="Seg_8472" s="T353">unʼə-Tə</ta>
            <ta e="T355" id="Seg_8473" s="T354">amno-zittə</ta>
            <ta e="T356" id="Seg_8474" s="T355">kan-lV-m</ta>
            <ta e="T357" id="Seg_8475" s="T356">hele</ta>
            <ta e="T358" id="Seg_8476" s="T357">bos-gəndə</ta>
            <ta e="T359" id="Seg_8477" s="T358">ku-lV-m</ta>
            <ta e="T360" id="Seg_8478" s="T359">kan-bi</ta>
            <ta e="T361" id="Seg_8479" s="T360">kan-bi</ta>
            <ta e="T362" id="Seg_8480" s="T361">ku-bi</ta>
            <ta e="T363" id="Seg_8481" s="T362">zajats-m</ta>
            <ta e="T364" id="Seg_8482" s="T363">davaj</ta>
            <ta e="T365" id="Seg_8483" s="T364">tăn-ziʔ</ta>
            <ta e="T366" id="Seg_8484" s="T365">amno-zittə</ta>
            <ta e="T367" id="Seg_8485" s="T366">davaj</ta>
            <ta e="T368" id="Seg_8486" s="T367">amno-zittə</ta>
            <ta e="T369" id="Seg_8487" s="T368">dĭgəttə</ta>
            <ta e="T370" id="Seg_8488" s="T369">nüdʼi</ta>
            <ta e="T371" id="Seg_8489" s="T370">mo-laːm-bi</ta>
            <ta e="T372" id="Seg_8490" s="T371">dĭ-zAŋ</ta>
            <ta e="T373" id="Seg_8491" s="T372">iʔbə-bi-jəʔ</ta>
            <ta e="T374" id="Seg_8492" s="T373">kunol-zittə</ta>
            <ta e="T375" id="Seg_8493" s="T374">zajats</ta>
            <ta e="T376" id="Seg_8494" s="T375">kunol-luʔbdə-bi</ta>
            <ta e="T377" id="Seg_8495" s="T376">a</ta>
            <ta e="T379" id="Seg_8496" s="T378">men</ta>
            <ta e="T380" id="Seg_8497" s="T379">ej</ta>
            <ta e="T381" id="Seg_8498" s="T380">kunol-liA</ta>
            <ta e="T382" id="Seg_8499" s="T381">nüdʼi-n</ta>
            <ta e="T383" id="Seg_8500" s="T382">bar</ta>
            <ta e="T384" id="Seg_8501" s="T383">kürüm-laʔbə</ta>
            <ta e="T385" id="Seg_8502" s="T384">a</ta>
            <ta e="T386" id="Seg_8503" s="T385">zajats</ta>
            <ta e="T387" id="Seg_8504" s="T386">nereʔ-luʔbdə-bi</ta>
            <ta e="T388" id="Seg_8505" s="T387">sĭj-də</ta>
            <ta e="T389" id="Seg_8506" s="T388">pʼatkə-gəndə</ta>
            <ta e="T1120" id="Seg_8507" s="T389">kan-lAʔ</ta>
            <ta e="T390" id="Seg_8508" s="T1120">tʼür-bi</ta>
            <ta e="T391" id="Seg_8509" s="T390">ĭmbi</ta>
            <ta e="T392" id="Seg_8510" s="T391">kürüm-liA-l</ta>
            <ta e="T393" id="Seg_8511" s="T392">šo-lV-j</ta>
            <ta e="T394" id="Seg_8512" s="T393">volk</ta>
            <ta e="T395" id="Seg_8513" s="T394">i</ta>
            <ta e="T396" id="Seg_8514" s="T395">miʔnʼibeʔ</ta>
            <ta e="T397" id="Seg_8515" s="T396">am-lV-j</ta>
            <ta e="T398" id="Seg_8516" s="T397">a</ta>
            <ta e="T399" id="Seg_8517" s="T398">men</ta>
            <ta e="T400" id="Seg_8518" s="T399">măn-ntə</ta>
            <ta e="T401" id="Seg_8519" s="T400">ej</ta>
            <ta e="T402" id="Seg_8520" s="T401">jakšə</ta>
            <ta e="T403" id="Seg_8521" s="T402">măn</ta>
            <ta e="T404" id="Seg_8522" s="T403">hele</ta>
            <ta e="T405" id="Seg_8523" s="T404">kan-lV-m</ta>
            <ta e="T406" id="Seg_8524" s="T405">volk</ta>
            <ta e="T408" id="Seg_8525" s="T407">volk</ta>
            <ta e="T409" id="Seg_8526" s="T408">măndo-r-zittə</ta>
            <ta e="T410" id="Seg_8527" s="T409">dĭgəttə</ta>
            <ta e="T411" id="Seg_8528" s="T410">kan-bi</ta>
            <ta e="T412" id="Seg_8529" s="T411">šonə-gA</ta>
            <ta e="T413" id="Seg_8530" s="T412">šonə-gA</ta>
            <ta e="T414" id="Seg_8531" s="T413">volk</ta>
            <ta e="T415" id="Seg_8532" s="T414">dĭ-Tə</ta>
            <ta e="T416" id="Seg_8533" s="T415">šonə-gA</ta>
            <ta e="T417" id="Seg_8534" s="T416">davaj</ta>
            <ta e="T418" id="Seg_8535" s="T417">tăn-ziʔ</ta>
            <ta e="T419" id="Seg_8536" s="T418">amno-zittə</ta>
            <ta e="T420" id="Seg_8537" s="T419">davaj</ta>
            <ta e="T421" id="Seg_8538" s="T420">amno-zittə</ta>
            <ta e="T422" id="Seg_8539" s="T421">nüdʼi</ta>
            <ta e="T423" id="Seg_8540" s="T422">šo-bi</ta>
            <ta e="T425" id="Seg_8541" s="T424">dĭ-zAŋ</ta>
            <ta e="T426" id="Seg_8542" s="T425">iʔbə-bi-jəʔ</ta>
            <ta e="T427" id="Seg_8543" s="T426">kunol-zittə</ta>
            <ta e="T428" id="Seg_8544" s="T427">men</ta>
            <ta e="T429" id="Seg_8545" s="T428">nüdʼi-n</ta>
            <ta e="T430" id="Seg_8546" s="T429">kürüm-luʔbdə-bi</ta>
            <ta e="T431" id="Seg_8547" s="T430">a</ta>
            <ta e="T432" id="Seg_8548" s="T431">volk</ta>
            <ta e="T433" id="Seg_8549" s="T432">măn-ntə</ta>
            <ta e="T434" id="Seg_8550" s="T433">ĭmbi</ta>
            <ta e="T435" id="Seg_8551" s="T434">kürüm-laʔbə-l</ta>
            <ta e="T436" id="Seg_8552" s="T435">tüj</ta>
            <ta e="T437" id="Seg_8553" s="T436">urgaːba</ta>
            <ta e="T438" id="Seg_8554" s="T437">šo-lV-j</ta>
            <ta e="T439" id="Seg_8555" s="T438">i</ta>
            <ta e="T440" id="Seg_8556" s="T439">miʔnʼibeʔ</ta>
            <ta e="T441" id="Seg_8557" s="T440">am-lV-j</ta>
            <ta e="T442" id="Seg_8558" s="T441">dĭgəttə</ta>
            <ta e="T443" id="Seg_8559" s="T442">men</ta>
            <ta e="T444" id="Seg_8560" s="T443">ertə-n</ta>
            <ta e="T445" id="Seg_8561" s="T444">uʔbdə-bi</ta>
            <ta e="T446" id="Seg_8562" s="T445">ej</ta>
            <ta e="T447" id="Seg_8563" s="T446">jakšə</ta>
            <ta e="T448" id="Seg_8564" s="T447">hele</ta>
            <ta e="T449" id="Seg_8565" s="T448">kan-lV-m</ta>
            <ta e="T450" id="Seg_8566" s="T449">urgaːba</ta>
            <ta e="T452" id="Seg_8567" s="T451">măndo-r-zittə</ta>
            <ta e="T454" id="Seg_8568" s="T453">dĭgəttə</ta>
            <ta e="T455" id="Seg_8569" s="T454">kan-bi</ta>
            <ta e="T456" id="Seg_8570" s="T455">šonə-gA</ta>
            <ta e="T457" id="Seg_8571" s="T456">šonə-gA</ta>
            <ta e="T458" id="Seg_8572" s="T457">urgaːba</ta>
            <ta e="T459" id="Seg_8573" s="T458">šonə-gA</ta>
            <ta e="T460" id="Seg_8574" s="T459">urgaːba</ta>
            <ta e="T461" id="Seg_8575" s="T460">urgaːba</ta>
            <ta e="T462" id="Seg_8576" s="T461">davaj</ta>
            <ta e="T463" id="Seg_8577" s="T462">tăn-ziʔ</ta>
            <ta e="T464" id="Seg_8578" s="T463">amno-zittə</ta>
            <ta e="T465" id="Seg_8579" s="T464">davaj</ta>
            <ta e="T466" id="Seg_8580" s="T465">dĭgəttə</ta>
            <ta e="T467" id="Seg_8581" s="T466">nüdʼi</ta>
            <ta e="T468" id="Seg_8582" s="T467">mo-laːm-bi</ta>
            <ta e="T470" id="Seg_8583" s="T469">iʔbə-bi-jəʔ</ta>
            <ta e="T471" id="Seg_8584" s="T470">kunol-zittə</ta>
            <ta e="T472" id="Seg_8585" s="T471">men</ta>
            <ta e="T473" id="Seg_8586" s="T472">nüdʼi-n</ta>
            <ta e="T474" id="Seg_8587" s="T473">bar</ta>
            <ta e="T475" id="Seg_8588" s="T474">kürüm-luʔbdə-bi</ta>
            <ta e="T476" id="Seg_8589" s="T475">bar</ta>
            <ta e="T477" id="Seg_8590" s="T476">a</ta>
            <ta e="T478" id="Seg_8591" s="T477">urgaːba</ta>
            <ta e="T479" id="Seg_8592" s="T478">ĭmbi</ta>
            <ta e="T480" id="Seg_8593" s="T479">kürüm-liA-l</ta>
            <ta e="T481" id="Seg_8594" s="T480">kuza</ta>
            <ta e="T482" id="Seg_8595" s="T481">šo-lV-j</ta>
            <ta e="T483" id="Seg_8596" s="T482">da</ta>
            <ta e="T484" id="Seg_8597" s="T483">miʔnʼibeʔ</ta>
            <ta e="T485" id="Seg_8598" s="T484">kut-lV-j</ta>
            <ta e="T486" id="Seg_8599" s="T485">a</ta>
            <ta e="T487" id="Seg_8600" s="T486">dĭ</ta>
            <ta e="T488" id="Seg_8601" s="T487">măn-ntə</ta>
            <ta e="T489" id="Seg_8602" s="T488">ej</ta>
            <ta e="T490" id="Seg_8603" s="T489">jakšə</ta>
            <ta e="T491" id="Seg_8604" s="T490">hele</ta>
            <ta e="T492" id="Seg_8605" s="T491">kan-lV-m</ta>
            <ta e="T493" id="Seg_8606" s="T492">kuza</ta>
            <ta e="T494" id="Seg_8607" s="T493">măndo-r-zittə</ta>
            <ta e="T495" id="Seg_8608" s="T494">ertə-n</ta>
            <ta e="T496" id="Seg_8609" s="T495">uʔbdə-bi</ta>
            <ta e="T497" id="Seg_8610" s="T496">kan-bi</ta>
            <ta e="T499" id="Seg_8611" s="T498">bar</ta>
            <ta e="T500" id="Seg_8612" s="T499">pa-jəʔ</ta>
            <ta e="T501" id="Seg_8613" s="T500">mĭn-bi</ta>
            <ta e="T502" id="Seg_8614" s="T501">mĭn-bi</ta>
            <ta e="T503" id="Seg_8615" s="T502">dĭgəttə</ta>
            <ta e="T504" id="Seg_8616" s="T503">sʼtʼep-Tə</ta>
            <ta e="T505" id="Seg_8617" s="T504">šo-bi</ta>
            <ta e="T506" id="Seg_8618" s="T505">ku-liA-t</ta>
            <ta e="T507" id="Seg_8619" s="T506">bar</ta>
            <ta e="T508" id="Seg_8620" s="T507">kuza</ta>
            <ta e="T509" id="Seg_8621" s="T508">šonə-gA</ta>
            <ta e="T512" id="Seg_8622" s="T511">pa-jəʔ</ta>
            <ta e="T513" id="Seg_8623" s="T512">i-zittə</ta>
            <ta e="T514" id="Seg_8624" s="T513">ine-ziʔ</ta>
            <ta e="T515" id="Seg_8625" s="T514">dĭgəttə</ta>
            <ta e="T516" id="Seg_8626" s="T515">dĭ</ta>
            <ta e="T517" id="Seg_8627" s="T516">măn-ntə</ta>
            <ta e="T518" id="Seg_8628" s="T517">kuza</ta>
            <ta e="T519" id="Seg_8629" s="T518">kuza</ta>
            <ta e="T520" id="Seg_8630" s="T519">davaj</ta>
            <ta e="T521" id="Seg_8631" s="T520">amno-zittə</ta>
            <ta e="T522" id="Seg_8632" s="T521">dĭ</ta>
            <ta e="T523" id="Seg_8633" s="T522">det-bi</ta>
            <ta e="T524" id="Seg_8634" s="T523">dĭ-m</ta>
            <ta e="T525" id="Seg_8635" s="T524">maʔ-gəndə</ta>
            <ta e="T526" id="Seg_8636" s="T525">iʔbə-bi-jəʔ</ta>
            <ta e="T527" id="Seg_8637" s="T526">kunol-zittə</ta>
            <ta e="T528" id="Seg_8638" s="T527">kuza</ta>
            <ta e="T529" id="Seg_8639" s="T528">kunol-luʔbdə-bi</ta>
            <ta e="T530" id="Seg_8640" s="T529">a</ta>
            <ta e="T531" id="Seg_8641" s="T530">men</ta>
            <ta e="T532" id="Seg_8642" s="T531">bar</ta>
            <ta e="T533" id="Seg_8643" s="T532">davaj</ta>
            <ta e="T534" id="Seg_8644" s="T533">kürüm-zittə</ta>
            <ta e="T535" id="Seg_8645" s="T534">a</ta>
            <ta e="T536" id="Seg_8646" s="T535">kuza</ta>
            <ta e="T537" id="Seg_8647" s="T536">uʔbdə-bi</ta>
            <ta e="T538" id="Seg_8648" s="T537">măn-ntə</ta>
            <ta e="T539" id="Seg_8649" s="T538">tăn</ta>
            <ta e="T540" id="Seg_8650" s="T539">püjö-liA-l</ta>
            <ta e="T541" id="Seg_8651" s="T540">amor-ə-ʔ</ta>
            <ta e="T542" id="Seg_8652" s="T541">da</ta>
            <ta e="T543" id="Seg_8653" s="T542">kunol-ə-ʔ</ta>
            <ta e="T544" id="Seg_8654" s="T543">măna</ta>
            <ta e="T545" id="Seg_8655" s="T544">det-ʔ</ta>
            <ta e="T548" id="Seg_8656" s="T547">mĭ-ʔ</ta>
            <ta e="T549" id="Seg_8657" s="T548">kunol-zittə</ta>
            <ta e="T550" id="Seg_8658" s="T549">bar</ta>
            <ta e="T552" id="Seg_8659" s="T551">onʼiʔ</ta>
            <ta e="T553" id="Seg_8660" s="T552">kuza</ta>
            <ta e="T554" id="Seg_8661" s="T553">kan-bi</ta>
            <ta e="T555" id="Seg_8662" s="T554">pa-j-lAʔ</ta>
            <ta e="T557" id="Seg_8663" s="T556">pa-jəʔ</ta>
            <ta e="T558" id="Seg_8664" s="T557">i-bi</ta>
            <ta e="T559" id="Seg_8665" s="T558">i</ta>
            <ta e="T560" id="Seg_8666" s="T559">šonə-gA</ta>
            <ta e="T561" id="Seg_8667" s="T560">dĭ-Tə</ta>
            <ta e="T562" id="Seg_8668" s="T561">urgaːba</ta>
            <ta e="T563" id="Seg_8669" s="T562">šonə-gA</ta>
            <ta e="T564" id="Seg_8670" s="T563">măn</ta>
            <ta e="T565" id="Seg_8671" s="T564">tüjö</ta>
            <ta e="T566" id="Seg_8672" s="T565">tăn</ta>
            <ta e="T567" id="Seg_8673" s="T566">am-lV-m</ta>
            <ta e="T568" id="Seg_8674" s="T567">tăn</ta>
            <ta e="T569" id="Seg_8675" s="T568">e-ʔ</ta>
            <ta e="T570" id="Seg_8676" s="T569">am-ə-ʔ</ta>
            <ta e="T571" id="Seg_8677" s="T570">măna</ta>
            <ta e="T572" id="Seg_8678" s="T571">dĭn</ta>
            <ta e="T573" id="Seg_8679" s="T572">ăxotnik-jəʔ</ta>
            <ta e="T574" id="Seg_8680" s="T573">šonə-gA-jəʔ</ta>
            <ta e="T575" id="Seg_8681" s="T574">kut-lV-jəʔ</ta>
            <ta e="T576" id="Seg_8682" s="T575">tănan</ta>
            <ta e="T577" id="Seg_8683" s="T576">dĭ</ta>
            <ta e="T578" id="Seg_8684" s="T577">bar</ta>
            <ta e="T579" id="Seg_8685" s="T578">nereʔ-luʔbdə-bi</ta>
            <ta e="T580" id="Seg_8686" s="T579">hen-ə-ʔ</ta>
            <ta e="T581" id="Seg_8687" s="T580">măna</ta>
            <ta e="T582" id="Seg_8688" s="T581">pa-jəʔ-ziʔ</ta>
            <ta e="T583" id="Seg_8689" s="T582">da</ta>
            <ta e="T584" id="Seg_8690" s="T583">kan-ə-ʔ</ta>
            <ta e="T585" id="Seg_8691" s="T584">štobɨ</ta>
            <ta e="T586" id="Seg_8692" s="T585">dĭ-zAŋ</ta>
            <ta e="T587" id="Seg_8693" s="T586">ej</ta>
            <ta e="T588" id="Seg_8694" s="T587">ku-bi-jəʔ</ta>
            <ta e="T589" id="Seg_8695" s="T588">măna</ta>
            <ta e="T590" id="Seg_8696" s="T589">a</ta>
            <ta e="T591" id="Seg_8697" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_8698" s="T591">măn-liA</ta>
            <ta e="T593" id="Seg_8699" s="T592">nadə</ta>
            <ta e="T594" id="Seg_8700" s="T593">hen-zittə</ta>
            <ta e="T595" id="Seg_8701" s="T594">tak</ta>
            <ta e="T596" id="Seg_8702" s="T595">sar-zittə</ta>
            <ta e="T597" id="Seg_8703" s="T596">dĭgəttə</ta>
            <ta e="T598" id="Seg_8704" s="T597">dĭ</ta>
            <ta e="T599" id="Seg_8705" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_8706" s="T599">măn-ntə</ta>
            <ta e="T601" id="Seg_8707" s="T600">sar-ə-ʔ</ta>
            <ta e="T602" id="Seg_8708" s="T601">dĭ</ta>
            <ta e="T603" id="Seg_8709" s="T602">sar-bi</ta>
            <ta e="T604" id="Seg_8710" s="T603">dĭ-m</ta>
            <ta e="T605" id="Seg_8711" s="T604">a</ta>
            <ta e="T606" id="Seg_8712" s="T605">tüj</ta>
            <ta e="T607" id="Seg_8713" s="T606">nadə</ta>
            <ta e="T609" id="Seg_8714" s="T608">baltu</ta>
            <ta e="T610" id="Seg_8715" s="T609">hʼaʔ-zittə</ta>
            <ta e="T611" id="Seg_8716" s="T610">i</ta>
            <ta e="T612" id="Seg_8717" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_8718" s="T612">dĭ</ta>
            <ta e="T614" id="Seg_8719" s="T613">urgaːba-m</ta>
            <ta e="T615" id="Seg_8720" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_8721" s="T615">baltu-ziʔ</ta>
            <ta e="T617" id="Seg_8722" s="T616">hʼaʔ-luʔbdə-bi</ta>
            <ta e="T618" id="Seg_8723" s="T617">i</ta>
            <ta e="T619" id="Seg_8724" s="T618">urgaːba</ta>
            <ta e="T620" id="Seg_8725" s="T619">kü-laːm-bi</ta>
            <ta e="T622" id="Seg_8726" s="T621">urgaːba</ta>
            <ta e="T623" id="Seg_8727" s="T622">tibi-ziʔ</ta>
            <ta e="T624" id="Seg_8728" s="T623">bar</ta>
            <ta e="T625" id="Seg_8729" s="T624">tugan</ta>
            <ta e="T627" id="Seg_8730" s="T626">mo-laʔbə-bi-jəʔ</ta>
            <ta e="T628" id="Seg_8731" s="T627">davaj</ta>
            <ta e="T629" id="Seg_8732" s="T628">kuʔ-zittə</ta>
            <ta e="T631" id="Seg_8733" s="T630">măna</ta>
            <ta e="T632" id="Seg_8734" s="T631">kareški-jəʔ</ta>
            <ta e="T633" id="Seg_8735" s="T632">a</ta>
            <ta e="T634" id="Seg_8736" s="T633">tănan</ta>
            <ta e="T635" id="Seg_8737" s="T634">verški-jəʔ</ta>
            <ta e="T636" id="Seg_8738" s="T635">dĭgəttə</ta>
            <ta e="T637" id="Seg_8739" s="T636">kuʔ-bi-jəʔ</ta>
            <ta e="T638" id="Seg_8740" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_8741" s="T638">özer-bi</ta>
            <ta e="T640" id="Seg_8742" s="T639">tibi</ta>
            <ta e="T641" id="Seg_8743" s="T640">i-bi</ta>
            <ta e="T642" id="Seg_8744" s="T641">kareški-jəʔ</ta>
            <ta e="T643" id="Seg_8745" s="T642">a</ta>
            <ta e="T644" id="Seg_8746" s="T643">urgaːba</ta>
            <ta e="T645" id="Seg_8747" s="T644">verški-jəʔ</ta>
            <ta e="T646" id="Seg_8748" s="T645">dĭgəttə</ta>
            <ta e="T647" id="Seg_8749" s="T646">urgaːba</ta>
            <ta e="T648" id="Seg_8750" s="T647">no</ta>
            <ta e="T649" id="Seg_8751" s="T648">kuza</ta>
            <ta e="T650" id="Seg_8752" s="T649">măna</ta>
            <ta e="T651" id="Seg_8753" s="T650">mʼeg-luʔbdə-bi</ta>
            <ta e="T652" id="Seg_8754" s="T651">dĭgəttə</ta>
            <ta e="T653" id="Seg_8755" s="T652">bazoʔ</ta>
            <ta e="T654" id="Seg_8756" s="T653">ejü</ta>
            <ta e="T655" id="Seg_8757" s="T654">mo-laːm-bi</ta>
            <ta e="T656" id="Seg_8758" s="T655">kuza</ta>
            <ta e="T657" id="Seg_8759" s="T656">măn-ntə</ta>
            <ta e="T658" id="Seg_8760" s="T657">urgaːba-Tə</ta>
            <ta e="T659" id="Seg_8761" s="T658">davaj</ta>
            <ta e="T660" id="Seg_8762" s="T659">bazoʔ</ta>
            <ta e="T661" id="Seg_8763" s="T660">kuʔ-zittə</ta>
            <ta e="T662" id="Seg_8764" s="T661">no</ta>
            <ta e="T663" id="Seg_8765" s="T662">urgaːba</ta>
            <ta e="T664" id="Seg_8766" s="T663">măna</ta>
            <ta e="T665" id="Seg_8767" s="T664">măna</ta>
            <ta e="T666" id="Seg_8768" s="T665">det-ʔ</ta>
            <ta e="T667" id="Seg_8769" s="T666">kareški-jəʔ</ta>
            <ta e="T668" id="Seg_8770" s="T667">a</ta>
            <ta e="T669" id="Seg_8771" s="T668">tănan</ta>
            <ta e="T670" id="Seg_8772" s="T669">verški-jəʔ</ta>
            <ta e="T671" id="Seg_8773" s="T670">dĭgəttə</ta>
            <ta e="T672" id="Seg_8774" s="T671">budəj</ta>
            <ta e="T674" id="Seg_8775" s="T672">ipek</ta>
            <ta e="T675" id="Seg_8776" s="T674">budəj</ta>
            <ta e="T676" id="Seg_8777" s="T675">ipek</ta>
            <ta e="T677" id="Seg_8778" s="T676">kuʔ-bi-jəʔ</ta>
            <ta e="T678" id="Seg_8779" s="T677">dĭ</ta>
            <ta e="T679" id="Seg_8780" s="T678">özer-bi</ta>
            <ta e="T680" id="Seg_8781" s="T679">jakšə</ta>
            <ta e="T681" id="Seg_8782" s="T680">ugaːndə</ta>
            <ta e="T682" id="Seg_8783" s="T681">i-bi</ta>
            <ta e="T683" id="Seg_8784" s="T682">dĭ</ta>
            <ta e="T684" id="Seg_8785" s="T683">kuza</ta>
            <ta e="T685" id="Seg_8786" s="T684">i-bi</ta>
            <ta e="T686" id="Seg_8787" s="T685">verški-jəʔ</ta>
            <ta e="T687" id="Seg_8788" s="T686">a</ta>
            <ta e="T688" id="Seg_8789" s="T687">urgaːba-Tə</ta>
            <ta e="T689" id="Seg_8790" s="T688">kareški-jəʔ</ta>
            <ta e="T690" id="Seg_8791" s="T689">i</ta>
            <ta e="T691" id="Seg_8792" s="T690">dĭgəttə</ta>
            <ta e="T693" id="Seg_8793" s="T692">dĭ-zen</ta>
            <ta e="T694" id="Seg_8794" s="T693">bar</ta>
            <ta e="T695" id="Seg_8795" s="T694">družba-t</ta>
            <ta e="T696" id="Seg_8796" s="T695">kü-laːm-bi</ta>
            <ta e="T698" id="Seg_8797" s="T697">amno-laʔbə-bi</ta>
            <ta e="T699" id="Seg_8798" s="T698">büzʼe</ta>
            <ta e="T700" id="Seg_8799" s="T699">da</ta>
            <ta e="T701" id="Seg_8800" s="T700">nüke</ta>
            <ta e="T702" id="Seg_8801" s="T701">dĭ-zAŋ-Kən</ta>
            <ta e="T703" id="Seg_8802" s="T702">ešši-zAŋ-Tə</ta>
            <ta e="T704" id="Seg_8803" s="T703">ej</ta>
            <ta e="T705" id="Seg_8804" s="T704">i-bi-jəʔ</ta>
            <ta e="T706" id="Seg_8805" s="T705">onʼiʔ</ta>
            <ta e="T707" id="Seg_8806" s="T706">sĭri</ta>
            <ta e="T708" id="Seg_8807" s="T707">bar</ta>
            <ta e="T709" id="Seg_8808" s="T708">saʔmə-luʔbdə-bi</ta>
            <ta e="T710" id="Seg_8809" s="T709">ugaːndə</ta>
            <ta e="T711" id="Seg_8810" s="T710">urgo</ta>
            <ta e="T712" id="Seg_8811" s="T711">ešši-zAŋ</ta>
            <ta e="T713" id="Seg_8812" s="T712">bar</ta>
            <ta e="T714" id="Seg_8813" s="T713">girgit</ta>
            <ta e="T715" id="Seg_8814" s="T714">bar</ta>
            <ta e="T719" id="Seg_8815" s="T718">girgit</ta>
            <ta e="T720" id="Seg_8816" s="T719">bar</ta>
            <ta e="T721" id="Seg_8817" s="T720">sʼar-laʔbə-jəʔ</ta>
            <ta e="T722" id="Seg_8818" s="T721">snegurka-jəʔ</ta>
            <ta e="T723" id="Seg_8819" s="T722">a-laʔbə-jəʔ</ta>
            <ta e="T724" id="Seg_8820" s="T723">bar</ta>
            <ta e="T725" id="Seg_8821" s="T724">a-laʔbə-jəʔ</ta>
            <ta e="T726" id="Seg_8822" s="T725">dĭgəttə</ta>
            <ta e="T727" id="Seg_8823" s="T726">a-bi-jəʔ</ta>
            <ta e="T728" id="Seg_8824" s="T727">urgo</ta>
            <ta e="T729" id="Seg_8825" s="T728">nüke</ta>
            <ta e="T730" id="Seg_8826" s="T729">a</ta>
            <ta e="T731" id="Seg_8827" s="T730">büzʼe</ta>
            <ta e="T732" id="Seg_8828" s="T731">üge</ta>
            <ta e="T733" id="Seg_8829" s="T732">măndo-bi</ta>
            <ta e="T734" id="Seg_8830" s="T733">măndo-bi</ta>
            <ta e="T735" id="Seg_8831" s="T734">kan-žə-bəj</ta>
            <ta e="T737" id="Seg_8832" s="T736">tăn-ziʔ</ta>
            <ta e="T738" id="Seg_8833" s="T737">nʼiʔdə</ta>
            <ta e="T739" id="Seg_8834" s="T738">nüke-Tə</ta>
            <ta e="T740" id="Seg_8835" s="T739">măn-liA</ta>
            <ta e="T741" id="Seg_8836" s="T740">no</ta>
            <ta e="T742" id="Seg_8837" s="T741">kan-žə-bəj</ta>
            <ta e="T744" id="Seg_8838" s="T743">kan-bi-jəʔ</ta>
            <ta e="T745" id="Seg_8839" s="T744">măndo-bi-jəʔ</ta>
            <ta e="T746" id="Seg_8840" s="T745">dĭgəttə</ta>
            <ta e="T747" id="Seg_8841" s="T746">davaj</ta>
            <ta e="T748" id="Seg_8842" s="T747">a-zittə</ta>
            <ta e="T749" id="Seg_8843" s="T748">koʔbdo</ta>
            <ta e="T751" id="Seg_8844" s="T749">a-bi-jəʔ</ta>
            <ta e="T752" id="Seg_8845" s="T751">ulu</ta>
            <ta e="T753" id="Seg_8846" s="T752">a-bi-jəʔ</ta>
            <ta e="T754" id="Seg_8847" s="T753">sima-jəʔ</ta>
            <ta e="T755" id="Seg_8848" s="T754">püje-t</ta>
            <ta e="T756" id="Seg_8849" s="T755">aŋ-də</ta>
            <ta e="T757" id="Seg_8850" s="T756">sima-t</ta>
            <ta e="T758" id="Seg_8851" s="T757">dĭgəttə</ta>
            <ta e="T759" id="Seg_8852" s="T758">kös-ziʔ</ta>
            <ta e="T760" id="Seg_8853" s="T759">bar</ta>
            <ta e="T761" id="Seg_8854" s="T760">sima-n</ta>
            <ta e="T762" id="Seg_8855" s="T761">toʔ-gəndə</ta>
            <ta e="T763" id="Seg_8856" s="T762">dʼüʔ-bi-jəʔ</ta>
            <ta e="T764" id="Seg_8857" s="T763">dĭgəttə</ta>
            <ta e="T765" id="Seg_8858" s="T764">dĭ</ta>
            <ta e="T766" id="Seg_8859" s="T765">sima-t-ziʔ</ta>
            <ta e="T767" id="Seg_8860" s="T766">mʼeŋgəl-luʔbdə-bi</ta>
            <ta e="T768" id="Seg_8861" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_8862" s="T768">büzʼe</ta>
            <ta e="T770" id="Seg_8863" s="T769">kandə-laʔbə</ta>
            <ta e="T772" id="Seg_8864" s="T771">kan-bi</ta>
            <ta e="T773" id="Seg_8865" s="T772">tura-Tə</ta>
            <ta e="T774" id="Seg_8866" s="T773">a</ta>
            <ta e="T775" id="Seg_8867" s="T774">dĭ</ta>
            <ta e="T776" id="Seg_8868" s="T775">dĭ-zAŋ</ta>
            <ta e="T778" id="Seg_8869" s="T777">tože</ta>
            <ta e="T779" id="Seg_8870" s="T778">dĭ-ziʔ</ta>
            <ta e="T780" id="Seg_8871" s="T779">kan-bi-jəʔ</ta>
            <ta e="T781" id="Seg_8872" s="T780">tura-Tə</ta>
            <ta e="T782" id="Seg_8873" s="T781">bar</ta>
            <ta e="T783" id="Seg_8874" s="T782">ugaːndə</ta>
            <ta e="T784" id="Seg_8875" s="T783">dĭ-Tə</ta>
            <ta e="T785" id="Seg_8876" s="T784">jakšə</ta>
            <ta e="T786" id="Seg_8877" s="T785">mo-laːm-bi</ta>
            <ta e="T787" id="Seg_8878" s="T786">ešši</ta>
            <ta e="T789" id="Seg_8879" s="T788">bar</ta>
            <ta e="T790" id="Seg_8880" s="T789">i-bi-jəʔ</ta>
            <ta e="T791" id="Seg_8881" s="T790">dĭ-Tə</ta>
            <ta e="T793" id="Seg_8882" s="T792">dĭgəttə</ta>
            <ta e="T794" id="Seg_8883" s="T793">dĭ-Tə</ta>
            <ta e="T795" id="Seg_8884" s="T794">jama-jəʔ</ta>
            <ta e="T796" id="Seg_8885" s="T795">i-bi-jəʔ</ta>
            <ta e="T797" id="Seg_8886" s="T796">kuvas-jəʔ</ta>
            <ta e="T798" id="Seg_8887" s="T797">kömə-jəʔ</ta>
            <ta e="T799" id="Seg_8888" s="T798">dĭgəttə</ta>
            <ta e="T800" id="Seg_8889" s="T799">eʔbdə-Tə</ta>
            <ta e="T801" id="Seg_8890" s="T800">i-bi-jəʔ</ta>
            <ta e="T803" id="Seg_8891" s="T802">ugaːndə</ta>
            <ta e="T804" id="Seg_8892" s="T803">kuvas</ta>
            <ta e="T806" id="Seg_8893" s="T805">dĭgəttə</ta>
            <ta e="T807" id="Seg_8894" s="T806">dĭ</ta>
            <ta e="T808" id="Seg_8895" s="T807">dĭgəttə</ta>
            <ta e="T809" id="Seg_8896" s="T808">ejü</ta>
            <ta e="T810" id="Seg_8897" s="T809">mo-laːm-bi</ta>
            <ta e="T811" id="Seg_8898" s="T810">a</ta>
            <ta e="T812" id="Seg_8899" s="T811">dĭ</ta>
            <ta e="T813" id="Seg_8900" s="T812">koʔbdo</ta>
            <ta e="T814" id="Seg_8901" s="T813">amno-laʔbə</ta>
            <ta e="T815" id="Seg_8902" s="T814">bü</ta>
            <ta e="T816" id="Seg_8903" s="T815">mʼaŋ-laʔbə</ta>
            <ta e="T817" id="Seg_8904" s="T816">dĭ</ta>
            <ta e="T818" id="Seg_8905" s="T817">bar</ta>
            <ta e="T819" id="Seg_8906" s="T818">pim-liA</ta>
            <ta e="T820" id="Seg_8907" s="T819">a</ta>
            <ta e="T821" id="Seg_8908" s="T820">nüke-t</ta>
            <ta e="T823" id="Seg_8909" s="T822">nüke</ta>
            <ta e="T824" id="Seg_8910" s="T823">măn-ntə</ta>
            <ta e="T825" id="Seg_8911" s="T824">ĭmbi</ta>
            <ta e="T826" id="Seg_8912" s="T825">tăn</ta>
            <ta e="T827" id="Seg_8913" s="T826">tʼor-laʔbə-l</ta>
            <ta e="T828" id="Seg_8914" s="T827">a</ta>
            <ta e="T829" id="Seg_8915" s="T828">bos-dən</ta>
            <ta e="T830" id="Seg_8916" s="T829">bar</ta>
            <ta e="T832" id="Seg_8917" s="T831">kăjəl-də</ta>
            <ta e="T833" id="Seg_8918" s="T832">mʼaŋ-laʔbə-jəʔ</ta>
            <ta e="T834" id="Seg_8919" s="T833">kadul-Kən</ta>
            <ta e="T835" id="Seg_8920" s="T834">ta</ta>
            <ta e="T836" id="Seg_8921" s="T835">dĭgəttə</ta>
            <ta e="T838" id="Seg_8922" s="T837">bazoʔ</ta>
            <ta e="T839" id="Seg_8923" s="T838">kuja</ta>
            <ta e="T840" id="Seg_8924" s="T839">mo-laʔbə-bi</ta>
            <ta e="T842" id="Seg_8925" s="T841">noʔ</ta>
            <ta e="T843" id="Seg_8926" s="T842">özer-laʔbə-jəʔ</ta>
            <ta e="T844" id="Seg_8927" s="T843">svetok-jəʔ</ta>
            <ta e="T845" id="Seg_8928" s="T844">dĭgəttə</ta>
            <ta e="T846" id="Seg_8929" s="T845">šo-bi-jəʔ</ta>
            <ta e="T847" id="Seg_8930" s="T846">koʔbdo-zAŋ</ta>
            <ta e="T848" id="Seg_8931" s="T847">kan-žə-bəj</ta>
            <ta e="T849" id="Seg_8932" s="T848">miʔ-ziʔ</ta>
            <ta e="T850" id="Seg_8933" s="T849">kăštə-liA-jəʔ</ta>
            <ta e="T851" id="Seg_8934" s="T850">koʔbdo-t</ta>
            <ta e="T852" id="Seg_8935" s="T851">nüke</ta>
            <ta e="T855" id="Seg_8936" s="T854">măn-liA</ta>
            <ta e="T856" id="Seg_8937" s="T855">kan-ə-ʔ</ta>
            <ta e="T857" id="Seg_8938" s="T856">ĭmbi</ta>
            <ta e="T858" id="Seg_8939" s="T857">amno-laʔbə-l</ta>
            <ta e="T859" id="Seg_8940" s="T858">dʼok</ta>
            <ta e="T860" id="Seg_8941" s="T859">măn</ta>
            <ta e="T861" id="Seg_8942" s="T860">e-m</ta>
            <ta e="T862" id="Seg_8943" s="T861">kan-ə-ʔ</ta>
            <ta e="T863" id="Seg_8944" s="T862">kuja</ta>
            <ta e="T864" id="Seg_8945" s="T863">ulu-m</ta>
            <ta e="T865" id="Seg_8946" s="T864">bar</ta>
            <ta e="T866" id="Seg_8947" s="T865">ĭzem-luʔbdə-lV-j</ta>
            <ta e="T868" id="Seg_8948" s="T867">kuja</ta>
            <ta e="T869" id="Seg_8949" s="T868">nend-lV-j</ta>
            <ta e="T870" id="Seg_8950" s="T869">ulu-m</ta>
            <ta e="T871" id="Seg_8951" s="T870">no</ta>
            <ta e="T872" id="Seg_8952" s="T871">plat</ta>
            <ta e="T874" id="Seg_8953" s="T873">šer-t</ta>
            <ta e="T875" id="Seg_8954" s="T874">dĭgəttə</ta>
            <ta e="T876" id="Seg_8955" s="T875">dĭ</ta>
            <ta e="T877" id="Seg_8956" s="T876">kan-bi</ta>
            <ta e="T878" id="Seg_8957" s="T877">bar</ta>
            <ta e="T879" id="Seg_8958" s="T878">koʔbdo-zAŋ</ta>
            <ta e="T880" id="Seg_8959" s="T879">sʼar-laʔbə-jəʔ</ta>
            <ta e="T881" id="Seg_8960" s="T880">svetok-jəʔ</ta>
            <ta e="T883" id="Seg_8961" s="T882">nĭŋgə-laʔbə-jəʔ</ta>
            <ta e="T884" id="Seg_8962" s="T883">a</ta>
            <ta e="T885" id="Seg_8963" s="T884">dĭ</ta>
            <ta e="T886" id="Seg_8964" s="T885">amno-laʔbə</ta>
            <ta e="T890" id="Seg_8965" s="T889">dĭgəttə</ta>
            <ta e="T891" id="Seg_8966" s="T890">dĭ-zAŋ</ta>
            <ta e="T892" id="Seg_8967" s="T891">hen-bi-jəʔ</ta>
            <ta e="T893" id="Seg_8968" s="T892">šü</ta>
            <ta e="T894" id="Seg_8969" s="T893">ugaːndə</ta>
            <ta e="T895" id="Seg_8970" s="T894">urgo</ta>
            <ta e="T896" id="Seg_8971" s="T895">i</ta>
            <ta e="T897" id="Seg_8972" s="T896">davaj</ta>
            <ta e="T898" id="Seg_8973" s="T897">nuʔmə-zittə</ta>
            <ta e="T899" id="Seg_8974" s="T898">šü-Tə</ta>
            <ta e="T900" id="Seg_8975" s="T899">măn-liA-jəʔ</ta>
            <ta e="T901" id="Seg_8976" s="T900">ĭmbi</ta>
            <ta e="T902" id="Seg_8977" s="T901">tăn</ta>
            <ta e="T903" id="Seg_8978" s="T902">ej</ta>
            <ta e="T904" id="Seg_8979" s="T903">süʔmə-liA-l</ta>
            <ta e="T905" id="Seg_8980" s="T904">a</ta>
            <ta e="T906" id="Seg_8981" s="T905">dĭ</ta>
            <ta e="T907" id="Seg_8982" s="T906">pim-liA</ta>
            <ta e="T908" id="Seg_8983" s="T907">a</ta>
            <ta e="T909" id="Seg_8984" s="T908">dĭgəttə</ta>
            <ta e="T910" id="Seg_8985" s="T909">kak</ta>
            <ta e="T911" id="Seg_8986" s="T910">dĭ-m</ta>
            <ta e="T912" id="Seg_8987" s="T911">pĭštʼer-laʔbə-jəʔ</ta>
            <ta e="T913" id="Seg_8988" s="T912">a</ta>
            <ta e="T914" id="Seg_8989" s="T913">dĭ</ta>
            <ta e="T915" id="Seg_8990" s="T914">uʔbdə-bi</ta>
            <ta e="T917" id="Seg_8991" s="T916">süʔmə-luʔbdə-bi</ta>
            <ta e="T918" id="Seg_8992" s="T917">i</ta>
            <ta e="T919" id="Seg_8993" s="T918">bar</ta>
            <ta e="T920" id="Seg_8994" s="T919">šü-Kən</ta>
            <ta e="T1121" id="Seg_8995" s="T920">kan-lAʔ</ta>
            <ta e="T921" id="Seg_8996" s="T1121">tʼür-bi</ta>
            <ta e="T922" id="Seg_8997" s="T921">nʼuʔdə</ta>
            <ta e="T923" id="Seg_8998" s="T922">bar</ta>
            <ta e="T926" id="Seg_8999" s="T925">raz</ta>
            <ta e="T927" id="Seg_9000" s="T926">büzəj-m</ta>
            <ta e="T928" id="Seg_9001" s="T927">măn</ta>
            <ta e="T929" id="Seg_9002" s="T928">ugaːndə</ta>
            <ta e="T930" id="Seg_9003" s="T929">kuvas</ta>
            <ta e="T931" id="Seg_9004" s="T930">sĭri</ta>
            <ta e="T932" id="Seg_9005" s="T931">büzəj</ta>
            <ta e="T933" id="Seg_9006" s="T932">i-bi</ta>
            <ta e="T934" id="Seg_9007" s="T933">dĭgəttə</ta>
            <ta e="T935" id="Seg_9008" s="T934">mĭn-bi</ta>
            <ta e="T936" id="Seg_9009" s="T935">mĭn-bi</ta>
            <ta e="T937" id="Seg_9010" s="T936">măn</ta>
            <ta e="T938" id="Seg_9011" s="T937">nüdʼi-n</ta>
            <ta e="T939" id="Seg_9012" s="T938">bĭs-lə-zittə</ta>
            <ta e="T941" id="Seg_9013" s="T940">süt-ziʔ</ta>
            <ta e="T942" id="Seg_9014" s="T941">kirgaːr-liA-m</ta>
            <ta e="T943" id="Seg_9015" s="T942">kirgaːr-liA-m</ta>
            <ta e="T944" id="Seg_9016" s="T943">măndo-r-bi-m</ta>
            <ta e="T945" id="Seg_9017" s="T944">măndo-r-bi-m</ta>
            <ta e="T946" id="Seg_9018" s="T945">gijen=də</ta>
            <ta e="T947" id="Seg_9019" s="T946">naga</ta>
            <ta e="T948" id="Seg_9020" s="T947">no</ta>
            <ta e="T949" id="Seg_9021" s="T948">giraːm-bi</ta>
            <ta e="T951" id="Seg_9022" s="T950">büzəj</ta>
            <ta e="T952" id="Seg_9023" s="T951">ej</ta>
            <ta e="T953" id="Seg_9024" s="T952">tĭm-liA-m</ta>
            <ta e="T954" id="Seg_9025" s="T953">ertə-n</ta>
            <ta e="T955" id="Seg_9026" s="T954">uʔbdə-bi-m</ta>
            <ta e="T956" id="Seg_9027" s="T955">vezʼdʼe</ta>
            <ta e="T957" id="Seg_9028" s="T956">mĭn-bi-m</ta>
            <ta e="T958" id="Seg_9029" s="T957">tʼăga-Kən</ta>
            <ta e="T959" id="Seg_9030" s="T958">mĭn-bi-m</ta>
            <ta e="T960" id="Seg_9031" s="T959">tene-bi-m</ta>
            <ta e="T961" id="Seg_9032" s="T960">dĭ</ta>
            <ta e="T962" id="Seg_9033" s="T961">saʔmə-luʔbdə-bi</ta>
            <ta e="T963" id="Seg_9034" s="T962">bü-Tə</ta>
            <ta e="T964" id="Seg_9035" s="T963">dĭn</ta>
            <ta e="T965" id="Seg_9036" s="T964">naga</ta>
            <ta e="T967" id="Seg_9037" s="T966">tene-bi-m</ta>
            <ta e="T968" id="Seg_9038" s="T967">volk</ta>
            <ta e="T969" id="Seg_9039" s="T968">am-luʔbdə-bi</ta>
            <ta e="T970" id="Seg_9040" s="T969">dʼok</ta>
            <ta e="T971" id="Seg_9041" s="T970">ej</ta>
            <ta e="T972" id="Seg_9042" s="T971">volk</ta>
            <ta e="T974" id="Seg_9043" s="T973">ej</ta>
            <ta e="T975" id="Seg_9044" s="T974">am-luʔbdə-bi</ta>
            <ta e="T976" id="Seg_9045" s="T975">dĭgəttə</ta>
            <ta e="T977" id="Seg_9046" s="T976">dön</ta>
            <ta e="T978" id="Seg_9047" s="T977">il</ta>
            <ta e="T979" id="Seg_9048" s="T978">noʔ</ta>
            <ta e="T980" id="Seg_9049" s="T979">hʼaʔ-bi-jəʔ</ta>
            <ta e="T981" id="Seg_9050" s="T980">dĭ-zAŋ</ta>
            <ta e="T983" id="Seg_9051" s="T982">i-bi-jəʔ</ta>
            <ta e="T984" id="Seg_9052" s="T983">üjü-ttə</ta>
            <ta e="T986" id="Seg_9053" s="T985">sar-bi-jəʔ</ta>
            <ta e="T987" id="Seg_9054" s="T986">kun-bi-jəʔ</ta>
            <ta e="T988" id="Seg_9055" s="T987">maʔ-gəndə</ta>
            <ta e="T989" id="Seg_9056" s="T988">šide</ta>
            <ta e="T990" id="Seg_9057" s="T989">üjü-t-ziʔ</ta>
            <ta e="T991" id="Seg_9058" s="T990">volk</ta>
            <ta e="T992" id="Seg_9059" s="T991">i-bi</ta>
            <ta e="T993" id="Seg_9060" s="T992">măn</ta>
            <ta e="T995" id="Seg_9061" s="T994">tugan-m</ta>
            <ta e="T996" id="Seg_9062" s="T995">šo-bi</ta>
            <ta e="T997" id="Seg_9063" s="T996">Kanokler-Tə</ta>
            <ta e="T998" id="Seg_9064" s="T997">i</ta>
            <ta e="T999" id="Seg_9065" s="T998">nörbə-liA</ta>
            <ta e="T1001" id="Seg_9066" s="T1000">šide</ta>
            <ta e="T1002" id="Seg_9067" s="T1001">tüžöj</ta>
            <ta e="T1003" id="Seg_9068" s="T1002">i-bi</ta>
            <ta e="T1004" id="Seg_9069" s="T1003">il-Kən</ta>
            <ta e="T1005" id="Seg_9070" s="T1004">ugaːndə</ta>
            <ta e="T1006" id="Seg_9071" s="T1005">il</ta>
            <ta e="T1008" id="Seg_9072" s="T1007">il</ta>
            <ta e="T1009" id="Seg_9073" s="T1008">ugaːndə</ta>
            <ta e="T1011" id="Seg_9074" s="T1010">sil-də</ta>
            <ta e="T1012" id="Seg_9075" s="T1011">iʔgö</ta>
            <ta e="T1013" id="Seg_9076" s="T1012">a</ta>
            <ta e="T1014" id="Seg_9077" s="T1013">miʔnʼibeʔ</ta>
            <ta e="T1015" id="Seg_9078" s="T1014">amno-lV-j</ta>
            <ta e="T1016" id="Seg_9079" s="T1015">bar</ta>
            <ta e="T1017" id="Seg_9080" s="T1016">ĭmbi</ta>
            <ta e="T1018" id="Seg_9081" s="T1017">todam</ta>
            <ta e="T1019" id="Seg_9082" s="T1018">mo-lV-jəʔ</ta>
            <ta e="T1020" id="Seg_9083" s="T1019">noʔ</ta>
            <ta e="T1021" id="Seg_9084" s="T1020">ej</ta>
            <ta e="T1022" id="Seg_9085" s="T1021">am-liA</ta>
            <ta e="T1023" id="Seg_9086" s="T1022">bü</ta>
            <ta e="T1024" id="Seg_9087" s="T1023">ej</ta>
            <ta e="T1025" id="Seg_9088" s="T1024">bĭs-liA</ta>
            <ta e="T1026" id="Seg_9089" s="T1025">amka</ta>
            <ta e="T1027" id="Seg_9090" s="T1026">am-laʔbə</ta>
            <ta e="T1028" id="Seg_9091" s="T1027">baška</ta>
            <ta e="T1029" id="Seg_9092" s="T1028">tüžöj</ta>
            <ta e="T1030" id="Seg_9093" s="T1029">šo-lV-jəʔ</ta>
            <ta e="T1031" id="Seg_9094" s="T1030">bar</ta>
            <ta e="T1032" id="Seg_9095" s="T1031">noʔ</ta>
            <ta e="T1033" id="Seg_9096" s="T1032">am-luʔbdə-lV-jəʔ</ta>
            <ta e="T1034" id="Seg_9097" s="T1033">a</ta>
            <ta e="T1035" id="Seg_9098" s="T1034">dĭ</ta>
            <ta e="T1036" id="Seg_9099" s="T1035">ej</ta>
            <ta e="T1037" id="Seg_9100" s="T1036">am-liA</ta>
            <ta e="T1039" id="Seg_9101" s="T1038">ej</ta>
            <ta e="T1040" id="Seg_9102" s="T1039">tĭmne-m</ta>
            <ta e="T1041" id="Seg_9103" s="T1040">măn-ntə</ta>
            <ta e="T1042" id="Seg_9104" s="T1041">šeden</ta>
            <ta e="T1043" id="Seg_9105" s="T1042">dĭrgit</ta>
            <ta e="T1044" id="Seg_9106" s="T1043">aľi</ta>
            <ta e="T1045" id="Seg_9107" s="T1044">ĭmbi</ta>
            <ta e="T1046" id="Seg_9108" s="T1045">dĭrgit</ta>
            <ta e="T1047" id="Seg_9109" s="T1046">miʔ</ta>
            <ta e="T1048" id="Seg_9110" s="T1047">i-bi-bAʔ</ta>
            <ta e="T1049" id="Seg_9111" s="T1048">dĭ-n</ta>
            <ta e="T1051" id="Seg_9112" s="T1050">sʼestra-gəndə</ta>
            <ta e="T1052" id="Seg_9113" s="T1051">tüžöj-də</ta>
            <ta e="T1053" id="Seg_9114" s="T1052">dĭ</ta>
            <ta e="T1054" id="Seg_9115" s="T1053">vedʼ</ta>
            <ta e="T1055" id="Seg_9116" s="T1054">ĭmbi=də</ta>
            <ta e="T1056" id="Seg_9117" s="T1055">ej</ta>
            <ta e="T1057" id="Seg_9118" s="T1056">tĭmne-t</ta>
            <ta e="T1058" id="Seg_9119" s="T1057">tože</ta>
            <ta e="T1059" id="Seg_9120" s="T1058">tüžöj</ta>
            <ta e="T1060" id="Seg_9121" s="T1059">dărəʔ</ta>
            <ta e="T1061" id="Seg_9122" s="T1060">mo-laːm-bi</ta>
            <ta e="T1062" id="Seg_9123" s="T1061">bar</ta>
            <ta e="T1063" id="Seg_9124" s="T1062">todam</ta>
            <ta e="T1064" id="Seg_9125" s="T1063">mo-bi</ta>
            <ta e="T1065" id="Seg_9126" s="T1064">a</ta>
            <ta e="T1066" id="Seg_9127" s="T1065">boʔ</ta>
            <ta e="T1067" id="Seg_9128" s="T1066">sil-də</ta>
            <ta e="T1068" id="Seg_9129" s="T1067">iʔgö</ta>
            <ta e="T1070" id="Seg_9130" s="T1069">i-bi</ta>
            <ta e="T1071" id="Seg_9131" s="T1070">amno-bi</ta>
            <ta e="T1072" id="Seg_9132" s="T1071">miʔnʼibeʔ</ta>
            <ta e="T1074" id="Seg_9133" s="T1073">todam</ta>
            <ta e="T1075" id="Seg_9134" s="T1074">mo-laːm-bi</ta>
            <ta e="T1077" id="Seg_9135" s="T1076">dĭ</ta>
            <ta e="T1078" id="Seg_9136" s="T1077">onʼiʔ</ta>
            <ta e="T1079" id="Seg_9137" s="T1078">koʔbdo-ziʔ</ta>
            <ta e="T1080" id="Seg_9138" s="T1079">amno-laʔbə-bi</ta>
            <ta e="T1081" id="Seg_9139" s="T1080">dĭgəttə</ta>
            <ta e="T1084" id="Seg_9140" s="T1083">i-bi</ta>
            <ta e="T1085" id="Seg_9141" s="T1084">možet</ta>
            <ta e="T1086" id="Seg_9142" s="T1085">dĭ</ta>
            <ta e="T1088" id="Seg_9143" s="T1087">ĭmbi=nʼibudʼ</ta>
            <ta e="T1090" id="Seg_9144" s="T1089">tĭl-bi</ta>
            <ta e="T1091" id="Seg_9145" s="T1090">i</ta>
            <ta e="T1092" id="Seg_9146" s="T1091">tüj</ta>
            <ta e="T1094" id="Seg_9147" s="T1093">tüžöj-də</ta>
            <ta e="T1095" id="Seg_9148" s="T1094">bar</ta>
            <ta e="T1096" id="Seg_9149" s="T1095">ej</ta>
            <ta e="T1097" id="Seg_9150" s="T1096">nu-liA-jəʔ</ta>
            <ta e="T1099" id="Seg_9151" s="T1098">šiʔ</ta>
            <ta e="T1101" id="Seg_9152" s="T1100">bar</ta>
            <ta e="T1102" id="Seg_9153" s="T1101">karəldʼaːn</ta>
            <ta e="T1105" id="Seg_9154" s="T1104">ši</ta>
            <ta e="T1106" id="Seg_9155" s="T1105">karəldʼaːn</ta>
            <ta e="T1122" id="Seg_9156" s="T1106">kan-lAʔ</ta>
            <ta e="T1107" id="Seg_9157" s="T1122">tʼür-lV-bAʔ</ta>
            <ta e="T1123" id="Seg_9158" s="T1107">kan-lAʔ</ta>
            <ta e="T1108" id="Seg_9159" s="T1123">tʼür-lV-lAʔ</ta>
            <ta e="T1109" id="Seg_9160" s="T1108">davajtʼe</ta>
            <ta e="T1110" id="Seg_9161" s="T1109">kamro-lV-bAʔ</ta>
            <ta e="T1111" id="Seg_9162" s="T1110">i</ta>
            <ta e="T1112" id="Seg_9163" s="T1111">panar-lV-bAʔ</ta>
            <ta e="T1113" id="Seg_9164" s="T1112">dĭgəttə</ta>
            <ta e="T1114" id="Seg_9165" s="T1113">kan-KAʔ</ta>
            <ta e="T1115" id="Seg_9166" s="T1114">kudaj-ziʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T1" id="Seg_9167" s="T0">fly-DUR.[3SG]</ta>
            <ta e="T2" id="Seg_9168" s="T1">PTCL</ta>
            <ta e="T3" id="Seg_9169" s="T2">shout-DUR.[3SG]</ta>
            <ta e="T4" id="Seg_9170" s="T3">sit-FUT-3SG</ta>
            <ta e="T5" id="Seg_9171" s="T4">so</ta>
            <ta e="T6" id="Seg_9172" s="T5">earth.[NOM.SG]</ta>
            <ta e="T7" id="Seg_9173" s="T6">earth.[NOM.SG]</ta>
            <ta e="T8" id="Seg_9174" s="T7">%%-DUR.[3SG]</ta>
            <ta e="T9" id="Seg_9175" s="T8">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_9176" s="T9">gun.[NOM.SG]</ta>
            <ta e="T12" id="Seg_9177" s="T11">what.kind</ta>
            <ta e="T13" id="Seg_9178" s="T12">such</ta>
            <ta e="T16" id="Seg_9179" s="T15">ass-LAT/LOC.3SG</ta>
            <ta e="T18" id="Seg_9180" s="T17">this.[NOM.SG]</ta>
            <ta e="T19" id="Seg_9181" s="T18">gun.[NOM.SG]</ta>
            <ta e="T21" id="Seg_9182" s="T20">when</ta>
            <ta e="T22" id="Seg_9183" s="T21">we.NOM</ta>
            <ta e="T23" id="Seg_9184" s="T22">many</ta>
            <ta e="T24" id="Seg_9185" s="T23">be-PST-1PL</ta>
            <ta e="T25" id="Seg_9186" s="T24">Kamassian-PL</ta>
            <ta e="T26" id="Seg_9187" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_9188" s="T26">go-PST-1SG</ta>
            <ta e="T28" id="Seg_9189" s="T27">big.[NOM.SG]</ta>
            <ta e="T29" id="Seg_9190" s="T28">mountain-LOC</ta>
            <ta e="T30" id="Seg_9191" s="T29">Belogorye-LOC</ta>
            <ta e="T31" id="Seg_9192" s="T30">there</ta>
            <ta e="T32" id="Seg_9193" s="T31">fish.[NOM.SG]</ta>
            <ta e="T33" id="Seg_9194" s="T32">capture-PST-1SG</ta>
            <ta e="T34" id="Seg_9195" s="T33">and</ta>
            <ta e="T35" id="Seg_9196" s="T34">meat.[NOM.SG]</ta>
            <ta e="T36" id="Seg_9197" s="T35">kill-PST-1SG</ta>
            <ta e="T37" id="Seg_9198" s="T36">then</ta>
            <ta e="T38" id="Seg_9199" s="T37">all</ta>
            <ta e="T39" id="Seg_9200" s="T38">mountain-PL</ta>
            <ta e="T40" id="Seg_9201" s="T39">black-PL</ta>
            <ta e="T41" id="Seg_9202" s="T40">remain-MOM-PST-3PL</ta>
            <ta e="T42" id="Seg_9203" s="T41">I.NOM</ta>
            <ta e="T43" id="Seg_9204" s="T42">strength-NOM/GEN/ACC.1SG</ta>
            <ta e="T44" id="Seg_9205" s="T43">PTCL</ta>
            <ta e="T45" id="Seg_9206" s="T44">NEG.EX.[3SG]</ta>
            <ta e="T46" id="Seg_9207" s="T45">PTCL</ta>
            <ta e="T47" id="Seg_9208" s="T46">die-RES-PST.[3SG]</ta>
            <ta e="T48" id="Seg_9209" s="T47">and</ta>
            <ta e="T49" id="Seg_9210" s="T48">there</ta>
            <ta e="T50" id="Seg_9211" s="T49">where</ta>
            <ta e="T51" id="Seg_9212" s="T50">tent-PL</ta>
            <ta e="T52" id="Seg_9213" s="T51">stand-PRS-PST-3PL</ta>
            <ta e="T53" id="Seg_9214" s="T52">PTCL</ta>
            <ta e="T54" id="Seg_9215" s="T53">fall-MOM-PST-3PL</ta>
            <ta e="T55" id="Seg_9216" s="T54">and</ta>
            <ta e="T56" id="Seg_9217" s="T55">all</ta>
            <ta e="T57" id="Seg_9218" s="T56">die-RES-PST-3PL</ta>
            <ta e="T58" id="Seg_9219" s="T57">only</ta>
            <ta e="T59" id="Seg_9220" s="T58">birchbark.[NOM.SG]</ta>
            <ta e="T60" id="Seg_9221" s="T59">drill-MOM-PST-3PL</ta>
            <ta e="T62" id="Seg_9222" s="T61">now</ta>
            <ta e="T64" id="Seg_9223" s="T63">what.[NOM.SG]=INDEF</ta>
            <ta e="T65" id="Seg_9224" s="T64">NEG</ta>
            <ta e="T66" id="Seg_9225" s="T65">see-PRS-1SG</ta>
            <ta e="T67" id="Seg_9226" s="T66">what.[NOM.SG]=INDEF</ta>
            <ta e="T68" id="Seg_9227" s="T67">NEG</ta>
            <ta e="T70" id="Seg_9228" s="T69">listen-PRS-1SG</ta>
            <ta e="T71" id="Seg_9229" s="T70">all</ta>
            <ta e="T73" id="Seg_9230" s="T72">strength-NOM/GEN/ACC.1SG</ta>
            <ta e="T1117" id="Seg_9231" s="T73">go-CVB</ta>
            <ta e="T74" id="Seg_9232" s="T1117">disappear-PST.[3SG]</ta>
            <ta e="T75" id="Seg_9233" s="T74">now</ta>
            <ta e="T76" id="Seg_9234" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_9235" s="T76">alone-NOM/GEN/ACC.1SG</ta>
            <ta e="T78" id="Seg_9236" s="T77">live-DUR-1SG</ta>
            <ta e="T79" id="Seg_9237" s="T78">who.[NOM.SG]=INDEF</ta>
            <ta e="T80" id="Seg_9238" s="T79">NEG.EX.[3SG]</ta>
            <ta e="T81" id="Seg_9239" s="T80">all</ta>
            <ta e="T82" id="Seg_9240" s="T81">die-RES-PST-3PL</ta>
            <ta e="T83" id="Seg_9241" s="T82">I.NOM</ta>
            <ta e="T84" id="Seg_9242" s="T83">Kamassian-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T86" id="Seg_9243" s="T85">now</ta>
            <ta e="T87" id="Seg_9244" s="T86">all</ta>
            <ta e="T88" id="Seg_9245" s="T87">road-PL-ACC.3SG</ta>
            <ta e="T89" id="Seg_9246" s="T88">PTCL</ta>
            <ta e="T90" id="Seg_9247" s="T89">remain-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_9248" s="T90">all</ta>
            <ta e="T92" id="Seg_9249" s="T91">grass-INS</ta>
            <ta e="T93" id="Seg_9250" s="T92">grow-MOM-PST.[3SG]</ta>
            <ta e="T95" id="Seg_9251" s="T94">I.NOM</ta>
            <ta e="T96" id="Seg_9252" s="T95">very</ta>
            <ta e="T97" id="Seg_9253" s="T96">beautiful.[NOM.SG]</ta>
            <ta e="T98" id="Seg_9254" s="T97">become-PST-1SG</ta>
            <ta e="T99" id="Seg_9255" s="T98">and</ta>
            <ta e="T100" id="Seg_9256" s="T99">where.to</ta>
            <ta e="T101" id="Seg_9257" s="T100">this.[NOM.SG]</ta>
            <ta e="T102" id="Seg_9258" s="T101">beautiful.[NOM.SG]</ta>
            <ta e="T1118" id="Seg_9259" s="T102">go-CVB</ta>
            <ta e="T103" id="Seg_9260" s="T1118">disappear-PST.[3SG]</ta>
            <ta e="T1119" id="Seg_9261" s="T103">go-CVB</ta>
            <ta e="T104" id="Seg_9262" s="T1119">disappear-PST.[3SG]</ta>
            <ta e="T105" id="Seg_9263" s="T104">big.[NOM.SG]</ta>
            <ta e="T106" id="Seg_9264" s="T105">mountain-PL-LAT</ta>
            <ta e="T107" id="Seg_9265" s="T106">and</ta>
            <ta e="T108" id="Seg_9266" s="T107">where=INDEF</ta>
            <ta e="T109" id="Seg_9267" s="T108">NEG.EX.[3SG]</ta>
            <ta e="T111" id="Seg_9268" s="T110">now</ta>
            <ta e="T112" id="Seg_9269" s="T111">this.[NOM.SG]</ta>
            <ta e="T113" id="Seg_9270" s="T112">when=INDEF</ta>
            <ta e="T114" id="Seg_9271" s="T113">NEG</ta>
            <ta e="T115" id="Seg_9272" s="T114">come-FUT-3SG</ta>
            <ta e="T118" id="Seg_9273" s="T117">I.NOM</ta>
            <ta e="T119" id="Seg_9274" s="T118">get.up-PST-1SG</ta>
            <ta e="T120" id="Seg_9275" s="T119">side.street-LAT</ta>
            <ta e="T122" id="Seg_9276" s="T121">come-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_9277" s="T122">grandfather.[NOM.SG]</ta>
            <ta e="T124" id="Seg_9278" s="T123">I.NOM</ta>
            <ta e="T125" id="Seg_9279" s="T124">say-IPFVZ-1SG</ta>
            <ta e="T126" id="Seg_9280" s="T125">very</ta>
            <ta e="T127" id="Seg_9281" s="T126">big.[NOM.SG]</ta>
            <ta e="T128" id="Seg_9282" s="T127">man.[NOM.SG]</ta>
            <ta e="T129" id="Seg_9283" s="T128">come-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_9284" s="T129">camera-INS</ta>
            <ta e="T131" id="Seg_9285" s="T130">house-PL</ta>
            <ta e="T132" id="Seg_9286" s="T131">take.picture-DUR.[3SG]</ta>
            <ta e="T133" id="Seg_9287" s="T132">go-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_9288" s="T133">then</ta>
            <ta e="T135" id="Seg_9289" s="T134">I.NOM</ta>
            <ta e="T136" id="Seg_9290" s="T135">stand-PST-1SG</ta>
            <ta e="T137" id="Seg_9291" s="T136">house-ABL</ta>
            <ta e="T138" id="Seg_9292" s="T137">edge-LAT/LOC.3SG</ta>
            <ta e="T139" id="Seg_9293" s="T138">this.[NOM.SG]</ta>
            <ta e="T141" id="Seg_9294" s="T140">PTCL</ta>
            <ta e="T142" id="Seg_9295" s="T141">I.ACC</ta>
            <ta e="T144" id="Seg_9296" s="T143">take.picture-PST.[3SG]</ta>
            <ta e="T146" id="Seg_9297" s="T145">there</ta>
            <ta e="T147" id="Seg_9298" s="T146">woman-LOC</ta>
            <ta e="T148" id="Seg_9299" s="T147">fish-NOM/GEN.3SG</ta>
            <ta e="T149" id="Seg_9300" s="T148">be-PST.[3SG]</ta>
            <ta e="T150" id="Seg_9301" s="T149">there</ta>
            <ta e="T152" id="Seg_9302" s="T151">this-GEN</ta>
            <ta e="T153" id="Seg_9303" s="T152">fish-NOM/GEN.3SG</ta>
            <ta e="T154" id="Seg_9304" s="T153">take-PST.[3SG]</ta>
            <ta e="T155" id="Seg_9305" s="T154">this-GEN</ta>
            <ta e="T156" id="Seg_9306" s="T155">daughter_in_law-NOM/GEN.3SG</ta>
            <ta e="T157" id="Seg_9307" s="T156">then</ta>
            <ta e="T158" id="Seg_9308" s="T157">this-LAT</ta>
            <ta e="T159" id="Seg_9309" s="T158">come-PST.[3SG]</ta>
            <ta e="T160" id="Seg_9310" s="T159">say-IPFVZ.[3SG]</ta>
            <ta e="T161" id="Seg_9311" s="T160">I.NOM</ta>
            <ta e="T162" id="Seg_9312" s="T161">you.DAT</ta>
            <ta e="T163" id="Seg_9313" s="T162">fish.[NOM.SG]</ta>
            <ta e="T164" id="Seg_9314" s="T163">bring-FUT-1SG</ta>
            <ta e="T165" id="Seg_9315" s="T164">today</ta>
            <ta e="T166" id="Seg_9316" s="T165">and</ta>
            <ta e="T167" id="Seg_9317" s="T166">I.NOM</ta>
            <ta e="T168" id="Seg_9318" s="T167">come-PST-1SG</ta>
            <ta e="T169" id="Seg_9319" s="T168">you.NOM</ta>
            <ta e="T170" id="Seg_9320" s="T169">fish-NOM/GEN/ACC.2SG</ta>
            <ta e="T171" id="Seg_9321" s="T170">daughter_in_law-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_9322" s="T171">take-PST.[3SG]</ta>
            <ta e="T173" id="Seg_9323" s="T172">I.NOM</ta>
            <ta e="T174" id="Seg_9324" s="T173">NEG</ta>
            <ta e="T175" id="Seg_9325" s="T174">take-PST-1SG</ta>
            <ta e="T177" id="Seg_9326" s="T176">what.[NOM.SG]</ta>
            <ta e="T178" id="Seg_9327" s="T177">this.[NOM.SG]</ta>
            <ta e="T179" id="Seg_9328" s="T178">I.LAT</ta>
            <ta e="T180" id="Seg_9329" s="T179">give-FUT-3SG</ta>
            <ta e="T181" id="Seg_9330" s="T180">a.few</ta>
            <ta e="T182" id="Seg_9331" s="T181">well</ta>
            <ta e="T183" id="Seg_9332" s="T182">a.few</ta>
            <ta e="T184" id="Seg_9333" s="T183">enough</ta>
            <ta e="T185" id="Seg_9334" s="T184">this-PL</ta>
            <ta e="T186" id="Seg_9335" s="T185">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_9336" s="T186">money.[NOM.SG]</ta>
            <ta e="T188" id="Seg_9337" s="T187">money-LAT</ta>
            <ta e="T190" id="Seg_9338" s="T189">sell-PST-3PL</ta>
            <ta e="T191" id="Seg_9339" s="T190">and</ta>
            <ta e="T192" id="Seg_9340" s="T191">you.NOM</ta>
            <ta e="T193" id="Seg_9341" s="T192">NEG</ta>
            <ta e="T194" id="Seg_9342" s="T193">give-PST-2SG</ta>
            <ta e="T195" id="Seg_9343" s="T194">money.[NOM.SG]</ta>
            <ta e="T196" id="Seg_9344" s="T195">and</ta>
            <ta e="T198" id="Seg_9345" s="T197">and</ta>
            <ta e="T199" id="Seg_9346" s="T198">this-GEN</ta>
            <ta e="T200" id="Seg_9347" s="T199">this.[NOM.SG]</ta>
            <ta e="T201" id="Seg_9348" s="T200">how.much</ta>
            <ta e="T202" id="Seg_9349" s="T201">give-FUT-3PL</ta>
            <ta e="T203" id="Seg_9350" s="T202">enough</ta>
            <ta e="T205" id="Seg_9351" s="T204">then</ta>
            <ta e="T206" id="Seg_9352" s="T205">I.NOM</ta>
            <ta e="T207" id="Seg_9353" s="T206">%%</ta>
            <ta e="T208" id="Seg_9354" s="T207">go-PST-1SG</ta>
            <ta e="T209" id="Seg_9355" s="T208">Anisya-LAT</ta>
            <ta e="T210" id="Seg_9356" s="T209">this.[NOM.SG]</ta>
            <ta e="T211" id="Seg_9357" s="T210">say-IPFVZ.[3SG]</ta>
            <ta e="T212" id="Seg_9358" s="T211">Elya.[NOM.SG]</ta>
            <ta e="T213" id="Seg_9359" s="T212">ask-PST.[3SG]</ta>
            <ta e="T214" id="Seg_9360" s="T213">egg-PL</ta>
            <ta e="T215" id="Seg_9361" s="T214">I.NOM</ta>
            <ta e="T216" id="Seg_9362" s="T215">collect-PST-1SG</ta>
            <ta e="T217" id="Seg_9363" s="T216">ten.[NOM.SG]</ta>
            <ta e="T218" id="Seg_9364" s="T217">egg-PL</ta>
            <ta e="T221" id="Seg_9365" s="T220">give-FUT-1SG</ta>
            <ta e="T222" id="Seg_9366" s="T221">ten.[NOM.SG]</ta>
            <ta e="T224" id="Seg_9367" s="T223">one.[NOM.SG]</ta>
            <ta e="T225" id="Seg_9368" s="T224">egg-PL</ta>
            <ta e="T226" id="Seg_9369" s="T225">ten.[NOM.SG]</ta>
            <ta e="T227" id="Seg_9370" s="T226">copeck-INS</ta>
            <ta e="T228" id="Seg_9371" s="T227">give-FUT-1SG</ta>
            <ta e="T229" id="Seg_9372" s="T228">this-LAT</ta>
            <ta e="T231" id="Seg_9373" s="T230">Elya.[NOM.SG]</ta>
            <ta e="T232" id="Seg_9374" s="T231">say-IPFVZ.[3SG]</ta>
            <ta e="T233" id="Seg_9375" s="T232">go-OPT.DU/PL-1DU</ta>
            <ta e="T234" id="Seg_9376" s="T233">we.LAT</ta>
            <ta e="T235" id="Seg_9377" s="T234">and</ta>
            <ta e="T236" id="Seg_9378" s="T235">I.NOM</ta>
            <ta e="T237" id="Seg_9379" s="T236">say-IPFVZ-1SG</ta>
            <ta e="T238" id="Seg_9380" s="T237">no</ta>
            <ta e="T239" id="Seg_9381" s="T238">NEG</ta>
            <ta e="T240" id="Seg_9382" s="T239">go-FUT-1SG</ta>
            <ta e="T241" id="Seg_9383" s="T240">very</ta>
            <ta e="T242" id="Seg_9384" s="T241">far</ta>
            <ta e="T243" id="Seg_9385" s="T242">what.[NOM.SG]</ta>
            <ta e="T244" id="Seg_9386" s="T243">far-CNG</ta>
            <ta e="T245" id="Seg_9387" s="T244">sit.down-IMP.2SG</ta>
            <ta e="T247" id="Seg_9388" s="T246">bird-LAT</ta>
            <ta e="T248" id="Seg_9389" s="T247">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T249" id="Seg_9390" s="T248">bird-LAT</ta>
            <ta e="T250" id="Seg_9391" s="T249">and</ta>
            <ta e="T251" id="Seg_9392" s="T250">soon</ta>
            <ta e="T252" id="Seg_9393" s="T251">come-FUT-2SG</ta>
            <ta e="T253" id="Seg_9394" s="T252">no</ta>
            <ta e="T254" id="Seg_9395" s="T253">I.NOM</ta>
            <ta e="T255" id="Seg_9396" s="T254">fear-PRS-1SG</ta>
            <ta e="T256" id="Seg_9397" s="T255">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T258" id="Seg_9398" s="T257">bird-LAT</ta>
            <ta e="T259" id="Seg_9399" s="T258">sit.down-INF.LAT</ta>
            <ta e="T260" id="Seg_9400" s="T259">otherwise</ta>
            <ta e="T261" id="Seg_9401" s="T260">die-RES-FUT-2SG</ta>
            <ta e="T262" id="Seg_9402" s="T261">no</ta>
            <ta e="T263" id="Seg_9403" s="T262">NEG</ta>
            <ta e="T264" id="Seg_9404" s="T263">die-RES-FUT-2SG</ta>
            <ta e="T265" id="Seg_9405" s="T264">there</ta>
            <ta e="T266" id="Seg_9406" s="T265">good</ta>
            <ta e="T267" id="Seg_9407" s="T266">sit-INF.LAT</ta>
            <ta e="T268" id="Seg_9408" s="T267">like</ta>
            <ta e="T269" id="Seg_9409" s="T268">house-LOC</ta>
            <ta e="T270" id="Seg_9410" s="T269">soon</ta>
            <ta e="T271" id="Seg_9411" s="T270">house-LAT-2SG</ta>
            <ta e="T272" id="Seg_9412" s="T271">come-FUT-2SG</ta>
            <ta e="T273" id="Seg_9413" s="T272">we.LAT</ta>
            <ta e="T274" id="Seg_9414" s="T273">live-FUT-2SG</ta>
            <ta e="T276" id="Seg_9415" s="T275">three-COLL</ta>
            <ta e="T277" id="Seg_9416" s="T276">go-FUT-1PL</ta>
            <ta e="T279" id="Seg_9417" s="T278">evening-LOC.ADV</ta>
            <ta e="T280" id="Seg_9418" s="T279">go-FUT-1SG</ta>
            <ta e="T281" id="Seg_9419" s="T280">one.[NOM.SG]</ta>
            <ta e="T282" id="Seg_9420" s="T281">man-LAT</ta>
            <ta e="T287" id="Seg_9421" s="T286">one.[NOM.SG]</ta>
            <ta e="T288" id="Seg_9422" s="T287">man-LAT</ta>
            <ta e="T289" id="Seg_9423" s="T288">say-FUT-1SG</ta>
            <ta e="T291" id="Seg_9424" s="T290">bring-IMP.2SG.O</ta>
            <ta e="T292" id="Seg_9425" s="T291">this.[NOM.SG]</ta>
            <ta e="T293" id="Seg_9426" s="T292">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T294" id="Seg_9427" s="T293">hospital-LAT</ta>
            <ta e="T295" id="Seg_9428" s="T294">this-LAT</ta>
            <ta e="T297" id="Seg_9429" s="T296">this-LAT</ta>
            <ta e="T298" id="Seg_9430" s="T297">paper.[NOM.SG]</ta>
            <ta e="T299" id="Seg_9431" s="T298">give-PST-3PL</ta>
            <ta e="T300" id="Seg_9432" s="T299">and</ta>
            <ta e="T301" id="Seg_9433" s="T300">NEG</ta>
            <ta e="T304" id="Seg_9434" s="T303">bring-FUT-2SG</ta>
            <ta e="T305" id="Seg_9435" s="T304">so</ta>
            <ta e="T306" id="Seg_9436" s="T305">call-IMP.2SG.O</ta>
            <ta e="T309" id="Seg_9437" s="T308">this.[NOM.SG]</ta>
            <ta e="T310" id="Seg_9438" s="T309">come-FUT-3SG</ta>
            <ta e="T311" id="Seg_9439" s="T310">and</ta>
            <ta e="T312" id="Seg_9440" s="T311">bring-RES-FUT-3SG</ta>
            <ta e="T313" id="Seg_9441" s="T312">this-ACC</ta>
            <ta e="T314" id="Seg_9442" s="T313">so</ta>
            <ta e="T315" id="Seg_9443" s="T314">tell-FUT-1SG</ta>
            <ta e="T316" id="Seg_9444" s="T315">this-LAT</ta>
            <ta e="T317" id="Seg_9445" s="T316">maybe</ta>
            <ta e="T318" id="Seg_9446" s="T317">this.[NOM.SG]</ta>
            <ta e="T319" id="Seg_9447" s="T318">listen-FUT-3SG</ta>
            <ta e="T320" id="Seg_9448" s="T319">otherwise</ta>
            <ta e="T321" id="Seg_9449" s="T320">you.know</ta>
            <ta e="T322" id="Seg_9450" s="T321">you.NOM</ta>
            <ta e="T323" id="Seg_9451" s="T322">%kettle.[NOM.SG]</ta>
            <ta e="T324" id="Seg_9452" s="T323">take-PST-2SG</ta>
            <ta e="T325" id="Seg_9453" s="T324">and</ta>
            <ta e="T326" id="Seg_9454" s="T325">people.[NOM.SG]</ta>
            <ta e="T327" id="Seg_9455" s="T326">PTCL</ta>
            <ta e="T328" id="Seg_9456" s="T327">say-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_9457" s="T329">%kettle.[NOM.SG]</ta>
            <ta e="T331" id="Seg_9458" s="T330">take-PST-2SG</ta>
            <ta e="T332" id="Seg_9459" s="T331">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T333" id="Seg_9460" s="T332">what.[NOM.SG]=INDEF</ta>
            <ta e="T334" id="Seg_9461" s="T333">NEG</ta>
            <ta e="T335" id="Seg_9462" s="T334">help-PST.[3SG]</ta>
            <ta e="T336" id="Seg_9463" s="T335">this.[NOM.SG]</ta>
            <ta e="T337" id="Seg_9464" s="T336">woman-LAT</ta>
            <ta e="T339" id="Seg_9465" s="T338">and</ta>
            <ta e="T340" id="Seg_9466" s="T339">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T341" id="Seg_9467" s="T340">die-RES-PST.[3SG]</ta>
            <ta e="T342" id="Seg_9468" s="T341">and</ta>
            <ta e="T343" id="Seg_9469" s="T342">house.[NOM.SG]</ta>
            <ta e="T344" id="Seg_9470" s="T343">%%</ta>
            <ta e="T345" id="Seg_9471" s="T344">you.know</ta>
            <ta e="T346" id="Seg_9472" s="T345">you.NOM</ta>
            <ta e="T347" id="Seg_9473" s="T346">be-PST-2SG</ta>
            <ta e="T349" id="Seg_9474" s="T348">dog.[NOM.SG]</ta>
            <ta e="T350" id="Seg_9475" s="T349">alone</ta>
            <ta e="T351" id="Seg_9476" s="T350">live-PST.[3SG]</ta>
            <ta e="T352" id="Seg_9477" s="T351">live-PST.[3SG]</ta>
            <ta e="T354" id="Seg_9478" s="T353">alone-LAT</ta>
            <ta e="T355" id="Seg_9479" s="T354">live-INF.LAT</ta>
            <ta e="T356" id="Seg_9480" s="T355">go-FUT-1SG</ta>
            <ta e="T357" id="Seg_9481" s="T356">companion</ta>
            <ta e="T358" id="Seg_9482" s="T357">self-LAT/LOC.3SG</ta>
            <ta e="T359" id="Seg_9483" s="T358">find-FUT-1SG</ta>
            <ta e="T360" id="Seg_9484" s="T359">go-PST.[3SG]</ta>
            <ta e="T361" id="Seg_9485" s="T360">go-PST.[3SG]</ta>
            <ta e="T362" id="Seg_9486" s="T361">see-PST.[3SG]</ta>
            <ta e="T363" id="Seg_9487" s="T362">hare-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_9488" s="T363">HORT</ta>
            <ta e="T365" id="Seg_9489" s="T364">you.NOM-COM</ta>
            <ta e="T366" id="Seg_9490" s="T365">live-INF.LAT</ta>
            <ta e="T367" id="Seg_9491" s="T366">HORT</ta>
            <ta e="T368" id="Seg_9492" s="T367">live-INF.LAT</ta>
            <ta e="T369" id="Seg_9493" s="T368">then</ta>
            <ta e="T370" id="Seg_9494" s="T369">evening.[NOM.SG]</ta>
            <ta e="T371" id="Seg_9495" s="T370">become-RES-PST.[3SG]</ta>
            <ta e="T372" id="Seg_9496" s="T371">this-PL</ta>
            <ta e="T373" id="Seg_9497" s="T372">lie.down-PST-3PL</ta>
            <ta e="T374" id="Seg_9498" s="T373">sleep-INF.LAT</ta>
            <ta e="T375" id="Seg_9499" s="T374">hare.[NOM.SG]</ta>
            <ta e="T376" id="Seg_9500" s="T375">sleep-MOM-PST.[3SG]</ta>
            <ta e="T377" id="Seg_9501" s="T376">and</ta>
            <ta e="T379" id="Seg_9502" s="T378">dog.[NOM.SG]</ta>
            <ta e="T380" id="Seg_9503" s="T379">NEG</ta>
            <ta e="T381" id="Seg_9504" s="T380">sleep-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_9505" s="T381">evening-LOC.ADV</ta>
            <ta e="T383" id="Seg_9506" s="T382">PTCL</ta>
            <ta e="T384" id="Seg_9507" s="T383">shout-DUR.[3SG]</ta>
            <ta e="T385" id="Seg_9508" s="T384">and</ta>
            <ta e="T386" id="Seg_9509" s="T385">hare.[NOM.SG]</ta>
            <ta e="T387" id="Seg_9510" s="T386">frighten-MOM-PST.[3SG]</ta>
            <ta e="T388" id="Seg_9511" s="T387">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T389" id="Seg_9512" s="T388">heel-LAT/LOC.3SG</ta>
            <ta e="T1120" id="Seg_9513" s="T389">go-CVB</ta>
            <ta e="T390" id="Seg_9514" s="T1120">disappear-PST.[3SG]</ta>
            <ta e="T391" id="Seg_9515" s="T390">what.[NOM.SG]</ta>
            <ta e="T392" id="Seg_9516" s="T391">shout-PRS-2SG</ta>
            <ta e="T393" id="Seg_9517" s="T392">come-FUT-3SG</ta>
            <ta e="T394" id="Seg_9518" s="T393">wolf.[NOM.SG]</ta>
            <ta e="T395" id="Seg_9519" s="T394">and</ta>
            <ta e="T396" id="Seg_9520" s="T395">we.ACC</ta>
            <ta e="T397" id="Seg_9521" s="T396">eat-FUT-3SG</ta>
            <ta e="T398" id="Seg_9522" s="T397">and</ta>
            <ta e="T399" id="Seg_9523" s="T398">dog.[NOM.SG]</ta>
            <ta e="T400" id="Seg_9524" s="T399">say-IPFVZ.[3SG]</ta>
            <ta e="T401" id="Seg_9525" s="T400">NEG</ta>
            <ta e="T402" id="Seg_9526" s="T401">good.[NOM.SG]</ta>
            <ta e="T403" id="Seg_9527" s="T402">I.NOM</ta>
            <ta e="T404" id="Seg_9528" s="T403">companion.[NOM.SG]</ta>
            <ta e="T405" id="Seg_9529" s="T404">go-FUT-1SG</ta>
            <ta e="T406" id="Seg_9530" s="T405">wolf.[NOM.SG]</ta>
            <ta e="T408" id="Seg_9531" s="T407">wolf.[NOM.SG]</ta>
            <ta e="T409" id="Seg_9532" s="T408">look-FRQ-INF.LAT</ta>
            <ta e="T410" id="Seg_9533" s="T409">then</ta>
            <ta e="T411" id="Seg_9534" s="T410">go-PST.[3SG]</ta>
            <ta e="T412" id="Seg_9535" s="T411">come-PRS.[3SG]</ta>
            <ta e="T413" id="Seg_9536" s="T412">come-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_9537" s="T413">wolf.[NOM.SG]</ta>
            <ta e="T415" id="Seg_9538" s="T414">this-LAT</ta>
            <ta e="T416" id="Seg_9539" s="T415">come-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_9540" s="T416">HORT</ta>
            <ta e="T418" id="Seg_9541" s="T417">you.NOM-COM</ta>
            <ta e="T419" id="Seg_9542" s="T418">live-INF.LAT</ta>
            <ta e="T420" id="Seg_9543" s="T419">HORT</ta>
            <ta e="T421" id="Seg_9544" s="T420">live-INF.LAT</ta>
            <ta e="T422" id="Seg_9545" s="T421">evening</ta>
            <ta e="T423" id="Seg_9546" s="T422">come-PST.[3SG]</ta>
            <ta e="T425" id="Seg_9547" s="T424">this-PL</ta>
            <ta e="T426" id="Seg_9548" s="T425">lie.down-PST-3PL</ta>
            <ta e="T427" id="Seg_9549" s="T426">sleep-INF.LAT</ta>
            <ta e="T428" id="Seg_9550" s="T427">dog.[NOM.SG]</ta>
            <ta e="T429" id="Seg_9551" s="T428">evening-LOC.ADV</ta>
            <ta e="T430" id="Seg_9552" s="T429">shout-MOM-PST.[3SG]</ta>
            <ta e="T431" id="Seg_9553" s="T430">and</ta>
            <ta e="T432" id="Seg_9554" s="T431">wolf.[NOM.SG]</ta>
            <ta e="T433" id="Seg_9555" s="T432">say-IPFVZ.[3SG]</ta>
            <ta e="T434" id="Seg_9556" s="T433">what.[NOM.SG]</ta>
            <ta e="T435" id="Seg_9557" s="T434">shout-DUR-2SG</ta>
            <ta e="T436" id="Seg_9558" s="T435">now</ta>
            <ta e="T437" id="Seg_9559" s="T436">bear.[NOM.SG]</ta>
            <ta e="T438" id="Seg_9560" s="T437">come-FUT-3SG</ta>
            <ta e="T439" id="Seg_9561" s="T438">and</ta>
            <ta e="T440" id="Seg_9562" s="T439">we.ACC</ta>
            <ta e="T441" id="Seg_9563" s="T440">eat-FUT-3SG</ta>
            <ta e="T442" id="Seg_9564" s="T441">then</ta>
            <ta e="T443" id="Seg_9565" s="T442">dog.[NOM.SG]</ta>
            <ta e="T444" id="Seg_9566" s="T443">morning-LOC.ADV</ta>
            <ta e="T445" id="Seg_9567" s="T444">get.up-PST.[3SG]</ta>
            <ta e="T446" id="Seg_9568" s="T445">NEG</ta>
            <ta e="T447" id="Seg_9569" s="T446">good</ta>
            <ta e="T448" id="Seg_9570" s="T447">companion.[NOM.SG]</ta>
            <ta e="T449" id="Seg_9571" s="T448">go-FUT-1SG</ta>
            <ta e="T450" id="Seg_9572" s="T449">bear.[NOM.SG]</ta>
            <ta e="T452" id="Seg_9573" s="T451">look-FRQ-INF.LAT</ta>
            <ta e="T454" id="Seg_9574" s="T453">then</ta>
            <ta e="T455" id="Seg_9575" s="T454">go-PST.[3SG]</ta>
            <ta e="T456" id="Seg_9576" s="T455">come-PRS.[3SG]</ta>
            <ta e="T457" id="Seg_9577" s="T456">come-PRS.[3SG]</ta>
            <ta e="T458" id="Seg_9578" s="T457">bear.[NOM.SG]</ta>
            <ta e="T459" id="Seg_9579" s="T458">come-PRS.[3SG]</ta>
            <ta e="T460" id="Seg_9580" s="T459">bear.[NOM.SG]</ta>
            <ta e="T461" id="Seg_9581" s="T460">bear.[NOM.SG]</ta>
            <ta e="T462" id="Seg_9582" s="T461">HORT</ta>
            <ta e="T463" id="Seg_9583" s="T462">you.NOM-COM</ta>
            <ta e="T464" id="Seg_9584" s="T463">live-INF.LAT</ta>
            <ta e="T465" id="Seg_9585" s="T464">HORT</ta>
            <ta e="T466" id="Seg_9586" s="T465">then</ta>
            <ta e="T467" id="Seg_9587" s="T466">evening</ta>
            <ta e="T468" id="Seg_9588" s="T467">become-RES-PST.[3SG]</ta>
            <ta e="T470" id="Seg_9589" s="T469">lie.down-PST-3PL</ta>
            <ta e="T471" id="Seg_9590" s="T470">sleep-INF.LAT</ta>
            <ta e="T472" id="Seg_9591" s="T471">dog.[NOM.SG]</ta>
            <ta e="T473" id="Seg_9592" s="T472">evening-LOC.ADV</ta>
            <ta e="T474" id="Seg_9593" s="T473">PTCL</ta>
            <ta e="T475" id="Seg_9594" s="T474">shout-MOM-PST.[3SG]</ta>
            <ta e="T476" id="Seg_9595" s="T475">PTCL</ta>
            <ta e="T477" id="Seg_9596" s="T476">and</ta>
            <ta e="T478" id="Seg_9597" s="T477">bear.[NOM.SG]</ta>
            <ta e="T479" id="Seg_9598" s="T478">what.[NOM.SG]</ta>
            <ta e="T480" id="Seg_9599" s="T479">shout-PRS-2SG</ta>
            <ta e="T481" id="Seg_9600" s="T480">man.[NOM.SG]</ta>
            <ta e="T482" id="Seg_9601" s="T481">come-FUT-3SG</ta>
            <ta e="T483" id="Seg_9602" s="T482">and</ta>
            <ta e="T484" id="Seg_9603" s="T483">we.ACC</ta>
            <ta e="T485" id="Seg_9604" s="T484">kill-FUT-3SG</ta>
            <ta e="T486" id="Seg_9605" s="T485">and</ta>
            <ta e="T487" id="Seg_9606" s="T486">this.[NOM.SG]</ta>
            <ta e="T488" id="Seg_9607" s="T487">say-IPFVZ.[3SG]</ta>
            <ta e="T489" id="Seg_9608" s="T488">NEG</ta>
            <ta e="T490" id="Seg_9609" s="T489">good</ta>
            <ta e="T491" id="Seg_9610" s="T490">companion.[NOM.SG]</ta>
            <ta e="T492" id="Seg_9611" s="T491">go-FUT-1SG</ta>
            <ta e="T493" id="Seg_9612" s="T492">man.[NOM.SG]</ta>
            <ta e="T494" id="Seg_9613" s="T493">look-FRQ-INF.LAT</ta>
            <ta e="T495" id="Seg_9614" s="T494">morning-LOC.ADV</ta>
            <ta e="T496" id="Seg_9615" s="T495">get.up-PST.[3SG]</ta>
            <ta e="T497" id="Seg_9616" s="T496">go-PST.[3SG]</ta>
            <ta e="T499" id="Seg_9617" s="T498">all</ta>
            <ta e="T500" id="Seg_9618" s="T499">tree-PL</ta>
            <ta e="T501" id="Seg_9619" s="T500">go-PST.[3SG]</ta>
            <ta e="T502" id="Seg_9620" s="T501">go-PST.[3SG]</ta>
            <ta e="T503" id="Seg_9621" s="T502">then</ta>
            <ta e="T504" id="Seg_9622" s="T503">steppe-LAT</ta>
            <ta e="T505" id="Seg_9623" s="T504">come-PST.[3SG]</ta>
            <ta e="T506" id="Seg_9624" s="T505">see-PRS-3SG.O</ta>
            <ta e="T507" id="Seg_9625" s="T506">PTCL</ta>
            <ta e="T508" id="Seg_9626" s="T507">man.[NOM.SG]</ta>
            <ta e="T509" id="Seg_9627" s="T508">come-PRS.[3SG]</ta>
            <ta e="T512" id="Seg_9628" s="T511">tree-PL</ta>
            <ta e="T513" id="Seg_9629" s="T512">take-INF.LAT</ta>
            <ta e="T514" id="Seg_9630" s="T513">horse-INS</ta>
            <ta e="T515" id="Seg_9631" s="T514">then</ta>
            <ta e="T516" id="Seg_9632" s="T515">this.[NOM.SG]</ta>
            <ta e="T517" id="Seg_9633" s="T516">say-IPFVZ.[3SG]</ta>
            <ta e="T518" id="Seg_9634" s="T517">man.[NOM.SG]</ta>
            <ta e="T519" id="Seg_9635" s="T518">man.[NOM.SG]</ta>
            <ta e="T520" id="Seg_9636" s="T519">HORT</ta>
            <ta e="T521" id="Seg_9637" s="T520">live-INF.LAT</ta>
            <ta e="T522" id="Seg_9638" s="T521">this.[NOM.SG]</ta>
            <ta e="T523" id="Seg_9639" s="T522">bring-PST.[3SG]</ta>
            <ta e="T524" id="Seg_9640" s="T523">this-ACC</ta>
            <ta e="T525" id="Seg_9641" s="T524">house-LAT/LOC.3SG</ta>
            <ta e="T526" id="Seg_9642" s="T525">lie.down-PST-3PL</ta>
            <ta e="T527" id="Seg_9643" s="T526">sleep-INF.LAT</ta>
            <ta e="T528" id="Seg_9644" s="T527">man.[NOM.SG]</ta>
            <ta e="T529" id="Seg_9645" s="T528">sleep-MOM-PST.[3SG]</ta>
            <ta e="T530" id="Seg_9646" s="T529">and</ta>
            <ta e="T531" id="Seg_9647" s="T530">dog.[NOM.SG]</ta>
            <ta e="T532" id="Seg_9648" s="T531">PTCL</ta>
            <ta e="T533" id="Seg_9649" s="T532">INCH</ta>
            <ta e="T534" id="Seg_9650" s="T533">shout-INF.LAT</ta>
            <ta e="T535" id="Seg_9651" s="T534">and</ta>
            <ta e="T536" id="Seg_9652" s="T535">man.[NOM.SG]</ta>
            <ta e="T537" id="Seg_9653" s="T536">get.up-PST.[3SG]</ta>
            <ta e="T538" id="Seg_9654" s="T537">say-IPFVZ.[3SG]</ta>
            <ta e="T539" id="Seg_9655" s="T538">you.NOM</ta>
            <ta e="T540" id="Seg_9656" s="T539">be.hungry-PRS-2SG</ta>
            <ta e="T541" id="Seg_9657" s="T540">eat-EP-IMP.2SG</ta>
            <ta e="T542" id="Seg_9658" s="T541">and</ta>
            <ta e="T543" id="Seg_9659" s="T542">sleep-EP-IMP.2SG</ta>
            <ta e="T544" id="Seg_9660" s="T543">I.LAT</ta>
            <ta e="T545" id="Seg_9661" s="T544">bring-IMP.2SG</ta>
            <ta e="T548" id="Seg_9662" s="T547">give-IMP.2SG</ta>
            <ta e="T549" id="Seg_9663" s="T548">sleep-INF.LAT</ta>
            <ta e="T550" id="Seg_9664" s="T549">all</ta>
            <ta e="T552" id="Seg_9665" s="T551">one.[NOM.SG]</ta>
            <ta e="T553" id="Seg_9666" s="T552">man.[NOM.SG]</ta>
            <ta e="T554" id="Seg_9667" s="T553">go-PST.[3SG]</ta>
            <ta e="T555" id="Seg_9668" s="T554">tree-VBLZ-CVB</ta>
            <ta e="T557" id="Seg_9669" s="T556">tree-PL</ta>
            <ta e="T558" id="Seg_9670" s="T557">take-PST.[3SG]</ta>
            <ta e="T559" id="Seg_9671" s="T558">and</ta>
            <ta e="T560" id="Seg_9672" s="T559">come-PRS.[3SG]</ta>
            <ta e="T561" id="Seg_9673" s="T560">this-LAT</ta>
            <ta e="T562" id="Seg_9674" s="T561">bear.[NOM.SG]</ta>
            <ta e="T563" id="Seg_9675" s="T562">come-PRS.[3SG]</ta>
            <ta e="T564" id="Seg_9676" s="T563">I.NOM</ta>
            <ta e="T565" id="Seg_9677" s="T564">soon</ta>
            <ta e="T566" id="Seg_9678" s="T565">you.NOM</ta>
            <ta e="T567" id="Seg_9679" s="T566">eat-FUT-1SG</ta>
            <ta e="T568" id="Seg_9680" s="T567">you.NOM</ta>
            <ta e="T569" id="Seg_9681" s="T568">NEG.AUX-IMP.2SG</ta>
            <ta e="T570" id="Seg_9682" s="T569">eat-EP-CNG</ta>
            <ta e="T571" id="Seg_9683" s="T570">I.ACC</ta>
            <ta e="T572" id="Seg_9684" s="T571">there</ta>
            <ta e="T573" id="Seg_9685" s="T572">hunter-PL</ta>
            <ta e="T574" id="Seg_9686" s="T573">come-PRS-3PL</ta>
            <ta e="T575" id="Seg_9687" s="T574">kill-FUT-3PL</ta>
            <ta e="T576" id="Seg_9688" s="T575">you.ACC</ta>
            <ta e="T577" id="Seg_9689" s="T576">this.[NOM.SG]</ta>
            <ta e="T578" id="Seg_9690" s="T577">PTCL</ta>
            <ta e="T579" id="Seg_9691" s="T578">frighten-MOM-PST.[3SG]</ta>
            <ta e="T580" id="Seg_9692" s="T579">put-EP-IMP.2SG</ta>
            <ta e="T581" id="Seg_9693" s="T580">I.ACC</ta>
            <ta e="T582" id="Seg_9694" s="T581">tree-PL-INS</ta>
            <ta e="T583" id="Seg_9695" s="T582">and</ta>
            <ta e="T584" id="Seg_9696" s="T583">go-EP-IMP.2SG</ta>
            <ta e="T585" id="Seg_9697" s="T584">so.that</ta>
            <ta e="T586" id="Seg_9698" s="T585">this-PL</ta>
            <ta e="T587" id="Seg_9699" s="T586">NEG</ta>
            <ta e="T588" id="Seg_9700" s="T587">see-PST-3PL</ta>
            <ta e="T589" id="Seg_9701" s="T588">I.ACC</ta>
            <ta e="T590" id="Seg_9702" s="T589">and</ta>
            <ta e="T591" id="Seg_9703" s="T590">this.[NOM.SG]</ta>
            <ta e="T592" id="Seg_9704" s="T591">say-PRS.[3SG]</ta>
            <ta e="T593" id="Seg_9705" s="T592">one.should</ta>
            <ta e="T594" id="Seg_9706" s="T593">put-INF.LAT</ta>
            <ta e="T595" id="Seg_9707" s="T594">so</ta>
            <ta e="T596" id="Seg_9708" s="T595">bind-INF.LAT</ta>
            <ta e="T597" id="Seg_9709" s="T596">then</ta>
            <ta e="T598" id="Seg_9710" s="T597">this.[NOM.SG]</ta>
            <ta e="T599" id="Seg_9711" s="T598">this.[NOM.SG]</ta>
            <ta e="T600" id="Seg_9712" s="T599">say-IPFVZ.[3SG]</ta>
            <ta e="T601" id="Seg_9713" s="T600">bind-EP-IMP.2SG</ta>
            <ta e="T602" id="Seg_9714" s="T601">this.[NOM.SG]</ta>
            <ta e="T603" id="Seg_9715" s="T602">bind-PST.[3SG]</ta>
            <ta e="T604" id="Seg_9716" s="T603">this-ACC</ta>
            <ta e="T605" id="Seg_9717" s="T604">and</ta>
            <ta e="T606" id="Seg_9718" s="T605">now</ta>
            <ta e="T607" id="Seg_9719" s="T606">one.should</ta>
            <ta e="T609" id="Seg_9720" s="T608">axe.[NOM.SG]</ta>
            <ta e="T610" id="Seg_9721" s="T609">cut-INF.LAT</ta>
            <ta e="T611" id="Seg_9722" s="T610">and</ta>
            <ta e="T612" id="Seg_9723" s="T611">then</ta>
            <ta e="T613" id="Seg_9724" s="T612">this.[NOM.SG]</ta>
            <ta e="T614" id="Seg_9725" s="T613">bear-ACC</ta>
            <ta e="T615" id="Seg_9726" s="T614">this.[NOM.SG]</ta>
            <ta e="T616" id="Seg_9727" s="T615">axe-INS</ta>
            <ta e="T617" id="Seg_9728" s="T616">cut-MOM-PST.[3SG]</ta>
            <ta e="T618" id="Seg_9729" s="T617">and</ta>
            <ta e="T619" id="Seg_9730" s="T618">bear.[NOM.SG]</ta>
            <ta e="T620" id="Seg_9731" s="T619">die-RES-PST.[3SG]</ta>
            <ta e="T622" id="Seg_9732" s="T621">bear.[NOM.SG]</ta>
            <ta e="T623" id="Seg_9733" s="T622">man-COM</ta>
            <ta e="T624" id="Seg_9734" s="T623">PTCL</ta>
            <ta e="T625" id="Seg_9735" s="T624">relative.[NOM.SG]</ta>
            <ta e="T627" id="Seg_9736" s="T626">become-DUR-PST-3PL</ta>
            <ta e="T628" id="Seg_9737" s="T627">INCH</ta>
            <ta e="T629" id="Seg_9738" s="T628">sow-INF.LAT</ta>
            <ta e="T631" id="Seg_9739" s="T630">I.LAT</ta>
            <ta e="T632" id="Seg_9740" s="T631">roots-NOM/GEN/ACC.3PL</ta>
            <ta e="T633" id="Seg_9741" s="T632">and</ta>
            <ta e="T634" id="Seg_9742" s="T633">you.DAT</ta>
            <ta e="T635" id="Seg_9743" s="T634">tops-NOM/GEN/ACC.3PL</ta>
            <ta e="T636" id="Seg_9744" s="T635">then</ta>
            <ta e="T637" id="Seg_9745" s="T636">sow-PST-3PL</ta>
            <ta e="T638" id="Seg_9746" s="T637">this.[NOM.SG]</ta>
            <ta e="T639" id="Seg_9747" s="T638">grow-PST.[3SG]</ta>
            <ta e="T640" id="Seg_9748" s="T639">man.[NOM.SG]</ta>
            <ta e="T641" id="Seg_9749" s="T640">take-PST.[3SG]</ta>
            <ta e="T642" id="Seg_9750" s="T641">roots-NOM/GEN/ACC.3PL</ta>
            <ta e="T643" id="Seg_9751" s="T642">and</ta>
            <ta e="T644" id="Seg_9752" s="T643">bear.[NOM.SG]</ta>
            <ta e="T645" id="Seg_9753" s="T644">tops-NOM/GEN/ACC.3PL</ta>
            <ta e="T646" id="Seg_9754" s="T645">then</ta>
            <ta e="T647" id="Seg_9755" s="T646">bear.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9756" s="T647">well</ta>
            <ta e="T649" id="Seg_9757" s="T648">man.[NOM.SG]</ta>
            <ta e="T650" id="Seg_9758" s="T649">I.ACC</ta>
            <ta e="T651" id="Seg_9759" s="T650">lure-MOM-PST.[3SG]</ta>
            <ta e="T652" id="Seg_9760" s="T651">then</ta>
            <ta e="T653" id="Seg_9761" s="T652">again</ta>
            <ta e="T654" id="Seg_9762" s="T653">warm.[NOM.SG]</ta>
            <ta e="T655" id="Seg_9763" s="T654">become-RES-PST.[3SG]</ta>
            <ta e="T656" id="Seg_9764" s="T655">man.[NOM.SG]</ta>
            <ta e="T657" id="Seg_9765" s="T656">say-IPFVZ.[3SG]</ta>
            <ta e="T658" id="Seg_9766" s="T657">bear-LAT</ta>
            <ta e="T659" id="Seg_9767" s="T658">HORT</ta>
            <ta e="T660" id="Seg_9768" s="T659">again</ta>
            <ta e="T661" id="Seg_9769" s="T660">sow-INF.LAT</ta>
            <ta e="T662" id="Seg_9770" s="T661">well</ta>
            <ta e="T663" id="Seg_9771" s="T662">bear.[NOM.SG]</ta>
            <ta e="T664" id="Seg_9772" s="T663">I.LAT</ta>
            <ta e="T665" id="Seg_9773" s="T664">I.LAT</ta>
            <ta e="T666" id="Seg_9774" s="T665">bring-IMP.2SG</ta>
            <ta e="T667" id="Seg_9775" s="T666">roots-NOM/GEN/ACC.3PL</ta>
            <ta e="T668" id="Seg_9776" s="T667">and</ta>
            <ta e="T669" id="Seg_9777" s="T668">you.DAT</ta>
            <ta e="T670" id="Seg_9778" s="T669">tops-NOM/GEN/ACC.3PL</ta>
            <ta e="T671" id="Seg_9779" s="T670">then</ta>
            <ta e="T672" id="Seg_9780" s="T671">wheat.[NOM.SG]</ta>
            <ta e="T674" id="Seg_9781" s="T672">bread.[NOM.SG]</ta>
            <ta e="T675" id="Seg_9782" s="T674">wheat.[NOM.SG]</ta>
            <ta e="T676" id="Seg_9783" s="T675">bread.[NOM.SG]</ta>
            <ta e="T677" id="Seg_9784" s="T676">sow-PST-3PL</ta>
            <ta e="T678" id="Seg_9785" s="T677">this.[NOM.SG]</ta>
            <ta e="T679" id="Seg_9786" s="T678">grow-PST.[3SG]</ta>
            <ta e="T680" id="Seg_9787" s="T679">good</ta>
            <ta e="T681" id="Seg_9788" s="T680">very</ta>
            <ta e="T682" id="Seg_9789" s="T681">be-PST.[3SG]</ta>
            <ta e="T683" id="Seg_9790" s="T682">this.[NOM.SG]</ta>
            <ta e="T684" id="Seg_9791" s="T683">man.[NOM.SG]</ta>
            <ta e="T685" id="Seg_9792" s="T684">take-PST.[3SG]</ta>
            <ta e="T686" id="Seg_9793" s="T685">tops-NOM/GEN/ACC.3PL</ta>
            <ta e="T687" id="Seg_9794" s="T686">and</ta>
            <ta e="T688" id="Seg_9795" s="T687">bear-LAT</ta>
            <ta e="T689" id="Seg_9796" s="T688">roots-NOM/GEN/ACC.3PL</ta>
            <ta e="T690" id="Seg_9797" s="T689">and</ta>
            <ta e="T691" id="Seg_9798" s="T690">then</ta>
            <ta e="T693" id="Seg_9799" s="T692">this-GEN.PL</ta>
            <ta e="T694" id="Seg_9800" s="T693">PTCL</ta>
            <ta e="T695" id="Seg_9801" s="T694">friendship-NOM/GEN.3SG</ta>
            <ta e="T696" id="Seg_9802" s="T695">die-RES-PST.[3SG]</ta>
            <ta e="T698" id="Seg_9803" s="T697">live-DUR-PST</ta>
            <ta e="T699" id="Seg_9804" s="T698">man.[NOM.SG]</ta>
            <ta e="T700" id="Seg_9805" s="T699">and</ta>
            <ta e="T701" id="Seg_9806" s="T700">woman.[NOM.SG]</ta>
            <ta e="T702" id="Seg_9807" s="T701">this-PL-LOC</ta>
            <ta e="T703" id="Seg_9808" s="T702">child-PL-LAT</ta>
            <ta e="T704" id="Seg_9809" s="T703">NEG</ta>
            <ta e="T705" id="Seg_9810" s="T704">be-PST-3PL</ta>
            <ta e="T706" id="Seg_9811" s="T705">one.[NOM.SG]</ta>
            <ta e="T707" id="Seg_9812" s="T706">snow.[NOM.SG]</ta>
            <ta e="T708" id="Seg_9813" s="T707">PTCL</ta>
            <ta e="T709" id="Seg_9814" s="T708">fall-MOM-PST.[3SG]</ta>
            <ta e="T710" id="Seg_9815" s="T709">very</ta>
            <ta e="T711" id="Seg_9816" s="T710">big.[NOM.SG]</ta>
            <ta e="T712" id="Seg_9817" s="T711">child-PL</ta>
            <ta e="T713" id="Seg_9818" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_9819" s="T713">what.kind.[NOM.SG]</ta>
            <ta e="T715" id="Seg_9820" s="T714">PTCL</ta>
            <ta e="T719" id="Seg_9821" s="T718">what.kind.[NOM.SG]</ta>
            <ta e="T720" id="Seg_9822" s="T719">PTCL</ta>
            <ta e="T721" id="Seg_9823" s="T720">play-DUR-3PL</ta>
            <ta e="T722" id="Seg_9824" s="T721">snowman-PL</ta>
            <ta e="T723" id="Seg_9825" s="T722">make-DUR-3PL</ta>
            <ta e="T724" id="Seg_9826" s="T723">all</ta>
            <ta e="T725" id="Seg_9827" s="T724">make-DUR-3PL</ta>
            <ta e="T726" id="Seg_9828" s="T725">then</ta>
            <ta e="T727" id="Seg_9829" s="T726">make-PST-3PL</ta>
            <ta e="T728" id="Seg_9830" s="T727">big.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9831" s="T728">woman.[NOM.SG]</ta>
            <ta e="T730" id="Seg_9832" s="T729">and</ta>
            <ta e="T731" id="Seg_9833" s="T730">man.[NOM.SG]</ta>
            <ta e="T732" id="Seg_9834" s="T731">always</ta>
            <ta e="T733" id="Seg_9835" s="T732">look-PST.[3SG]</ta>
            <ta e="T734" id="Seg_9836" s="T733">look-PST.[3SG]</ta>
            <ta e="T735" id="Seg_9837" s="T734">go-OPT.DU/PL-1DU</ta>
            <ta e="T737" id="Seg_9838" s="T736">you.NOM-COM</ta>
            <ta e="T738" id="Seg_9839" s="T737">outwards</ta>
            <ta e="T739" id="Seg_9840" s="T738">woman-LAT</ta>
            <ta e="T740" id="Seg_9841" s="T739">say-PRS.[3SG]</ta>
            <ta e="T741" id="Seg_9842" s="T740">well</ta>
            <ta e="T742" id="Seg_9843" s="T741">go-OPT.DU/PL-1DU</ta>
            <ta e="T744" id="Seg_9844" s="T743">go-PST-3PL</ta>
            <ta e="T745" id="Seg_9845" s="T744">look-PST-3PL</ta>
            <ta e="T746" id="Seg_9846" s="T745">then</ta>
            <ta e="T747" id="Seg_9847" s="T746">INCH</ta>
            <ta e="T748" id="Seg_9848" s="T747">make-INF.LAT</ta>
            <ta e="T749" id="Seg_9849" s="T748">girl.[NOM.SG]</ta>
            <ta e="T751" id="Seg_9850" s="T749">make-PST-3PL</ta>
            <ta e="T752" id="Seg_9851" s="T751">head.[NOM.SG]</ta>
            <ta e="T753" id="Seg_9852" s="T752">make-PST-3PL</ta>
            <ta e="T754" id="Seg_9853" s="T753">eye-PL</ta>
            <ta e="T755" id="Seg_9854" s="T754">nose-NOM/GEN.3SG</ta>
            <ta e="T756" id="Seg_9855" s="T755">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T757" id="Seg_9856" s="T756">eye-NOM/GEN.3SG</ta>
            <ta e="T758" id="Seg_9857" s="T757">then</ta>
            <ta e="T759" id="Seg_9858" s="T758">coal-INS</ta>
            <ta e="T760" id="Seg_9859" s="T759">PTCL</ta>
            <ta e="T761" id="Seg_9860" s="T760">eye-GEN</ta>
            <ta e="T762" id="Seg_9861" s="T761">edge-LAT/LOC.3SG</ta>
            <ta e="T763" id="Seg_9862" s="T762">smear-PST-3PL</ta>
            <ta e="T764" id="Seg_9863" s="T763">then</ta>
            <ta e="T765" id="Seg_9864" s="T764">this.[NOM.SG]</ta>
            <ta e="T766" id="Seg_9865" s="T765">eye-3SG-INS</ta>
            <ta e="T767" id="Seg_9866" s="T766">%blink-MOM-PST.[3SG]</ta>
            <ta e="T768" id="Seg_9867" s="T767">then</ta>
            <ta e="T769" id="Seg_9868" s="T768">man.[NOM.SG]</ta>
            <ta e="T770" id="Seg_9869" s="T769">walk-DUR.[3SG]</ta>
            <ta e="T772" id="Seg_9870" s="T771">go-PST.[3SG]</ta>
            <ta e="T773" id="Seg_9871" s="T772">house-LAT</ta>
            <ta e="T774" id="Seg_9872" s="T773">and</ta>
            <ta e="T775" id="Seg_9873" s="T774">this.[NOM.SG]</ta>
            <ta e="T776" id="Seg_9874" s="T775">this-PL</ta>
            <ta e="T778" id="Seg_9875" s="T777">also</ta>
            <ta e="T779" id="Seg_9876" s="T778">this-COM</ta>
            <ta e="T780" id="Seg_9877" s="T779">go-PST-3PL</ta>
            <ta e="T781" id="Seg_9878" s="T780">house-LAT</ta>
            <ta e="T782" id="Seg_9879" s="T781">PTCL</ta>
            <ta e="T783" id="Seg_9880" s="T782">very</ta>
            <ta e="T784" id="Seg_9881" s="T783">this-LAT</ta>
            <ta e="T785" id="Seg_9882" s="T784">good</ta>
            <ta e="T786" id="Seg_9883" s="T785">become-RES-PST.[3SG]</ta>
            <ta e="T787" id="Seg_9884" s="T786">child.[NOM.SG]</ta>
            <ta e="T789" id="Seg_9885" s="T788">all</ta>
            <ta e="T790" id="Seg_9886" s="T789">take-PST-3PL</ta>
            <ta e="T791" id="Seg_9887" s="T790">this-LAT</ta>
            <ta e="T793" id="Seg_9888" s="T792">then</ta>
            <ta e="T794" id="Seg_9889" s="T793">this-LAT</ta>
            <ta e="T795" id="Seg_9890" s="T794">boot-PL</ta>
            <ta e="T796" id="Seg_9891" s="T795">take-PST-3PL</ta>
            <ta e="T797" id="Seg_9892" s="T796">beautiful-PL</ta>
            <ta e="T798" id="Seg_9893" s="T797">red-PL</ta>
            <ta e="T799" id="Seg_9894" s="T798">then</ta>
            <ta e="T800" id="Seg_9895" s="T799">hair-LAT</ta>
            <ta e="T801" id="Seg_9896" s="T800">take-PST-3PL</ta>
            <ta e="T803" id="Seg_9897" s="T802">very</ta>
            <ta e="T804" id="Seg_9898" s="T803">beautiful.[NOM.SG]</ta>
            <ta e="T806" id="Seg_9899" s="T805">then</ta>
            <ta e="T807" id="Seg_9900" s="T806">this.[NOM.SG]</ta>
            <ta e="T808" id="Seg_9901" s="T807">then</ta>
            <ta e="T809" id="Seg_9902" s="T808">warm.[NOM.SG]</ta>
            <ta e="T810" id="Seg_9903" s="T809">become-RES-PST.[3SG]</ta>
            <ta e="T811" id="Seg_9904" s="T810">and</ta>
            <ta e="T812" id="Seg_9905" s="T811">this.[NOM.SG]</ta>
            <ta e="T813" id="Seg_9906" s="T812">girl.[NOM.SG]</ta>
            <ta e="T814" id="Seg_9907" s="T813">sit-DUR.[3SG]</ta>
            <ta e="T815" id="Seg_9908" s="T814">water.[NOM.SG]</ta>
            <ta e="T816" id="Seg_9909" s="T815">flow-DUR.[3SG]</ta>
            <ta e="T817" id="Seg_9910" s="T816">this.[NOM.SG]</ta>
            <ta e="T818" id="Seg_9911" s="T817">PTCL</ta>
            <ta e="T819" id="Seg_9912" s="T818">fear-PRS.[3SG]</ta>
            <ta e="T820" id="Seg_9913" s="T819">and</ta>
            <ta e="T821" id="Seg_9914" s="T820">woman-NOM/GEN.3SG</ta>
            <ta e="T823" id="Seg_9915" s="T822">woman.[NOM.SG]</ta>
            <ta e="T824" id="Seg_9916" s="T823">say-IPFVZ.[3SG]</ta>
            <ta e="T825" id="Seg_9917" s="T824">what.[NOM.SG]</ta>
            <ta e="T826" id="Seg_9918" s="T825">you.NOM</ta>
            <ta e="T827" id="Seg_9919" s="T826">cry-DUR-2SG</ta>
            <ta e="T828" id="Seg_9920" s="T827">and</ta>
            <ta e="T829" id="Seg_9921" s="T828">self-NOM/GEN/ACC.3PL</ta>
            <ta e="T830" id="Seg_9922" s="T829">PTCL</ta>
            <ta e="T832" id="Seg_9923" s="T831">tear-NOM/GEN/ACC.3SG</ta>
            <ta e="T833" id="Seg_9924" s="T832">flow-DUR-3PL</ta>
            <ta e="T834" id="Seg_9925" s="T833">face-LOC</ta>
            <ta e="T835" id="Seg_9926" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_9927" s="T835">then</ta>
            <ta e="T838" id="Seg_9928" s="T837">again</ta>
            <ta e="T839" id="Seg_9929" s="T838">sun.[NOM.SG]</ta>
            <ta e="T840" id="Seg_9930" s="T839">become-DUR-PST.[3SG]</ta>
            <ta e="T842" id="Seg_9931" s="T841">grass.[NOM.SG]</ta>
            <ta e="T843" id="Seg_9932" s="T842">grow-DUR-3PL</ta>
            <ta e="T844" id="Seg_9933" s="T843">flower-PL</ta>
            <ta e="T845" id="Seg_9934" s="T844">then</ta>
            <ta e="T846" id="Seg_9935" s="T845">come-PST-3PL</ta>
            <ta e="T847" id="Seg_9936" s="T846">daughter-PL</ta>
            <ta e="T848" id="Seg_9937" s="T847">go-OPT.DU/PL-1DU</ta>
            <ta e="T849" id="Seg_9938" s="T848">we.NOM-COM</ta>
            <ta e="T850" id="Seg_9939" s="T849">call-PRS-3PL</ta>
            <ta e="T851" id="Seg_9940" s="T850">girl-NOM/GEN.3SG</ta>
            <ta e="T852" id="Seg_9941" s="T851">woman.[NOM.SG]</ta>
            <ta e="T855" id="Seg_9942" s="T854">say-PRS.[3SG]</ta>
            <ta e="T856" id="Seg_9943" s="T855">go-EP-IMP.2SG</ta>
            <ta e="T857" id="Seg_9944" s="T856">what.[NOM.SG]</ta>
            <ta e="T858" id="Seg_9945" s="T857">sit-DUR-2SG</ta>
            <ta e="T859" id="Seg_9946" s="T858">no</ta>
            <ta e="T860" id="Seg_9947" s="T859">I.NOM</ta>
            <ta e="T861" id="Seg_9948" s="T860">NEG.AUX-1SG</ta>
            <ta e="T862" id="Seg_9949" s="T861">go-EP-CNG</ta>
            <ta e="T863" id="Seg_9950" s="T862">sun.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9951" s="T863">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T865" id="Seg_9952" s="T864">PTCL</ta>
            <ta e="T866" id="Seg_9953" s="T865">hurt-MOM-FUT-3SG</ta>
            <ta e="T868" id="Seg_9954" s="T867">sun.[NOM.SG]</ta>
            <ta e="T869" id="Seg_9955" s="T868">burn-FUT-3SG</ta>
            <ta e="T870" id="Seg_9956" s="T869">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T871" id="Seg_9957" s="T870">well</ta>
            <ta e="T872" id="Seg_9958" s="T871">scarf.[NOM.SG]</ta>
            <ta e="T874" id="Seg_9959" s="T873">dress-IMP.2SG.O</ta>
            <ta e="T875" id="Seg_9960" s="T874">then</ta>
            <ta e="T876" id="Seg_9961" s="T875">this.[NOM.SG]</ta>
            <ta e="T877" id="Seg_9962" s="T876">go-PST.[3SG]</ta>
            <ta e="T878" id="Seg_9963" s="T877">all</ta>
            <ta e="T879" id="Seg_9964" s="T878">daughter-PL</ta>
            <ta e="T880" id="Seg_9965" s="T879">play-DUR-3PL</ta>
            <ta e="T881" id="Seg_9966" s="T880">flower-PL</ta>
            <ta e="T883" id="Seg_9967" s="T882">tear-DUR-3PL</ta>
            <ta e="T884" id="Seg_9968" s="T883">and</ta>
            <ta e="T885" id="Seg_9969" s="T884">this.[NOM.SG]</ta>
            <ta e="T886" id="Seg_9970" s="T885">sit-DUR.[3SG]</ta>
            <ta e="T890" id="Seg_9971" s="T889">then</ta>
            <ta e="T891" id="Seg_9972" s="T890">this-PL</ta>
            <ta e="T892" id="Seg_9973" s="T891">put-PST-3PL</ta>
            <ta e="T893" id="Seg_9974" s="T892">fire.[NOM.SG]</ta>
            <ta e="T894" id="Seg_9975" s="T893">very</ta>
            <ta e="T895" id="Seg_9976" s="T894">big.[NOM.SG]</ta>
            <ta e="T896" id="Seg_9977" s="T895">and</ta>
            <ta e="T897" id="Seg_9978" s="T896">INCH</ta>
            <ta e="T898" id="Seg_9979" s="T897">run-INF.LAT</ta>
            <ta e="T899" id="Seg_9980" s="T898">fire-LAT</ta>
            <ta e="T900" id="Seg_9981" s="T899">say-PRS-3PL</ta>
            <ta e="T901" id="Seg_9982" s="T900">what.[NOM.SG]</ta>
            <ta e="T902" id="Seg_9983" s="T901">you.NOM</ta>
            <ta e="T903" id="Seg_9984" s="T902">NEG</ta>
            <ta e="T904" id="Seg_9985" s="T903">jump-PRS-2SG</ta>
            <ta e="T905" id="Seg_9986" s="T904">and</ta>
            <ta e="T906" id="Seg_9987" s="T905">this.[NOM.SG]</ta>
            <ta e="T907" id="Seg_9988" s="T906">fear-PRS.[3SG]</ta>
            <ta e="T908" id="Seg_9989" s="T907">and</ta>
            <ta e="T909" id="Seg_9990" s="T908">then</ta>
            <ta e="T910" id="Seg_9991" s="T909">like</ta>
            <ta e="T911" id="Seg_9992" s="T910">this-ACC</ta>
            <ta e="T912" id="Seg_9993" s="T911">make.fun-DUR-3PL</ta>
            <ta e="T913" id="Seg_9994" s="T912">and</ta>
            <ta e="T914" id="Seg_9995" s="T913">this.[NOM.SG]</ta>
            <ta e="T915" id="Seg_9996" s="T914">get.up-PST.[3SG]</ta>
            <ta e="T917" id="Seg_9997" s="T916">jump-MOM-PST.[3SG]</ta>
            <ta e="T918" id="Seg_9998" s="T917">and</ta>
            <ta e="T919" id="Seg_9999" s="T918">PTCL</ta>
            <ta e="T920" id="Seg_10000" s="T919">fire-LOC</ta>
            <ta e="T1121" id="Seg_10001" s="T920">go-CVB</ta>
            <ta e="T921" id="Seg_10002" s="T1121">disappear-PST.[3SG]</ta>
            <ta e="T922" id="Seg_10003" s="T921">up</ta>
            <ta e="T923" id="Seg_10004" s="T922">PTCL</ta>
            <ta e="T926" id="Seg_10005" s="T925">time.[NOM.SG]</ta>
            <ta e="T927" id="Seg_10006" s="T926">calf-NOM/GEN/ACC.1SG</ta>
            <ta e="T928" id="Seg_10007" s="T927">I.NOM</ta>
            <ta e="T929" id="Seg_10008" s="T928">very</ta>
            <ta e="T930" id="Seg_10009" s="T929">beautiful.[NOM.SG]</ta>
            <ta e="T931" id="Seg_10010" s="T930">white.[NOM.SG]</ta>
            <ta e="T932" id="Seg_10011" s="T931">calf.[NOM.SG]</ta>
            <ta e="T933" id="Seg_10012" s="T932">be-PST.[3SG]</ta>
            <ta e="T934" id="Seg_10013" s="T933">then</ta>
            <ta e="T935" id="Seg_10014" s="T934">go-PST.[3SG]</ta>
            <ta e="T936" id="Seg_10015" s="T935">go-PST.[3SG]</ta>
            <ta e="T937" id="Seg_10016" s="T936">I.NOM</ta>
            <ta e="T938" id="Seg_10017" s="T937">evening-LOC.ADV</ta>
            <ta e="T939" id="Seg_10018" s="T938">drink-TR-INF.LAT</ta>
            <ta e="T941" id="Seg_10019" s="T940">milk-INS</ta>
            <ta e="T942" id="Seg_10020" s="T941">shout-PRS-1SG</ta>
            <ta e="T943" id="Seg_10021" s="T942">shout-PRS-1SG</ta>
            <ta e="T944" id="Seg_10022" s="T943">look-FRQ-PST-1SG</ta>
            <ta e="T945" id="Seg_10023" s="T944">look-FRQ-PST-1SG</ta>
            <ta e="T946" id="Seg_10024" s="T945">where=INDEF</ta>
            <ta e="T947" id="Seg_10025" s="T946">NEG.EX.[3SG]</ta>
            <ta e="T948" id="Seg_10026" s="T947">well</ta>
            <ta e="T949" id="Seg_10027" s="T948">go.where-PST.[3SG]</ta>
            <ta e="T951" id="Seg_10028" s="T950">calf.[NOM.SG]</ta>
            <ta e="T952" id="Seg_10029" s="T951">NEG</ta>
            <ta e="T953" id="Seg_10030" s="T952">know-PRS-1SG</ta>
            <ta e="T954" id="Seg_10031" s="T953">morning-LOC.ADV</ta>
            <ta e="T955" id="Seg_10032" s="T954">get.up-PST-1SG</ta>
            <ta e="T956" id="Seg_10033" s="T955">everywhere</ta>
            <ta e="T957" id="Seg_10034" s="T956">go-PST-1SG</ta>
            <ta e="T958" id="Seg_10035" s="T957">river-LOC</ta>
            <ta e="T959" id="Seg_10036" s="T958">go-PST-1SG</ta>
            <ta e="T960" id="Seg_10037" s="T959">think-PST-1SG</ta>
            <ta e="T961" id="Seg_10038" s="T960">this.[NOM.SG]</ta>
            <ta e="T962" id="Seg_10039" s="T961">fall-MOM-PST.[3SG]</ta>
            <ta e="T963" id="Seg_10040" s="T962">water-LAT</ta>
            <ta e="T964" id="Seg_10041" s="T963">there</ta>
            <ta e="T965" id="Seg_10042" s="T964">NEG.EX.[3SG]</ta>
            <ta e="T967" id="Seg_10043" s="T966">think-PST-1SG</ta>
            <ta e="T968" id="Seg_10044" s="T967">wolf.[NOM.SG]</ta>
            <ta e="T969" id="Seg_10045" s="T968">eat-MOM-PST.[3SG]</ta>
            <ta e="T970" id="Seg_10046" s="T969">no</ta>
            <ta e="T971" id="Seg_10047" s="T970">NEG</ta>
            <ta e="T972" id="Seg_10048" s="T971">wolf.[NOM.SG]</ta>
            <ta e="T974" id="Seg_10049" s="T973">NEG</ta>
            <ta e="T975" id="Seg_10050" s="T974">eat-MOM-PST.[3SG]</ta>
            <ta e="T976" id="Seg_10051" s="T975">then</ta>
            <ta e="T977" id="Seg_10052" s="T976">here</ta>
            <ta e="T978" id="Seg_10053" s="T977">people.[NOM.SG]</ta>
            <ta e="T979" id="Seg_10054" s="T978">grass.[NOM.SG]</ta>
            <ta e="T980" id="Seg_10055" s="T979">cut-PST-3PL</ta>
            <ta e="T981" id="Seg_10056" s="T980">this-PL</ta>
            <ta e="T983" id="Seg_10057" s="T982">take-PST-3PL</ta>
            <ta e="T984" id="Seg_10058" s="T983">foot-ABL.3SG</ta>
            <ta e="T986" id="Seg_10059" s="T985">bind-PST-3PL</ta>
            <ta e="T987" id="Seg_10060" s="T986">bring-PST-3PL</ta>
            <ta e="T988" id="Seg_10061" s="T987">house-LAT/LOC.3SG</ta>
            <ta e="T989" id="Seg_10062" s="T988">two.[NOM.SG]</ta>
            <ta e="T990" id="Seg_10063" s="T989">foot-3SG-INS</ta>
            <ta e="T991" id="Seg_10064" s="T990">wolf.[NOM.SG]</ta>
            <ta e="T992" id="Seg_10065" s="T991">be-PST.[3SG]</ta>
            <ta e="T993" id="Seg_10066" s="T992">I.NOM</ta>
            <ta e="T995" id="Seg_10067" s="T994">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T996" id="Seg_10068" s="T995">come-PST.[3SG]</ta>
            <ta e="T997" id="Seg_10069" s="T996">Kan_Okler-LAT</ta>
            <ta e="T998" id="Seg_10070" s="T997">and</ta>
            <ta e="T999" id="Seg_10071" s="T998">tell-PRS.[3SG]</ta>
            <ta e="T1001" id="Seg_10072" s="T1000">two.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_10073" s="T1001">cow.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_10074" s="T1002">be-PST.[3SG]</ta>
            <ta e="T1004" id="Seg_10075" s="T1003">people-LOC</ta>
            <ta e="T1005" id="Seg_10076" s="T1004">very</ta>
            <ta e="T1006" id="Seg_10077" s="T1005">people.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_10078" s="T1007">people.[NOM.SG]</ta>
            <ta e="T1009" id="Seg_10079" s="T1008">very</ta>
            <ta e="T1011" id="Seg_10080" s="T1010">fat-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_10081" s="T1011">many</ta>
            <ta e="T1013" id="Seg_10082" s="T1012">and</ta>
            <ta e="T1014" id="Seg_10083" s="T1013">we.LAT</ta>
            <ta e="T1015" id="Seg_10084" s="T1014">live-FUT-3SG</ta>
            <ta e="T1016" id="Seg_10085" s="T1015">PTCL</ta>
            <ta e="T1017" id="Seg_10086" s="T1016">what.[NOM.SG]</ta>
            <ta e="T1018" id="Seg_10087" s="T1017">thin.[NOM.SG]</ta>
            <ta e="T1019" id="Seg_10088" s="T1018">become-FUT-3PL</ta>
            <ta e="T1020" id="Seg_10089" s="T1019">grass.[NOM.SG]</ta>
            <ta e="T1021" id="Seg_10090" s="T1020">NEG</ta>
            <ta e="T1022" id="Seg_10091" s="T1021">eat-PRS.[3SG]</ta>
            <ta e="T1023" id="Seg_10092" s="T1022">water.[NOM.SG]</ta>
            <ta e="T1024" id="Seg_10093" s="T1023">NEG</ta>
            <ta e="T1025" id="Seg_10094" s="T1024">drink-PRS.[3SG]</ta>
            <ta e="T1026" id="Seg_10095" s="T1025">few</ta>
            <ta e="T1027" id="Seg_10096" s="T1026">eat-DUR.[3SG]</ta>
            <ta e="T1028" id="Seg_10097" s="T1027">another.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_10098" s="T1028">cow.[NOM.SG]</ta>
            <ta e="T1030" id="Seg_10099" s="T1029">come-FUT-3PL</ta>
            <ta e="T1031" id="Seg_10100" s="T1030">PTCL</ta>
            <ta e="T1032" id="Seg_10101" s="T1031">grass.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_10102" s="T1032">eat-MOM-FUT-3PL</ta>
            <ta e="T1034" id="Seg_10103" s="T1033">and</ta>
            <ta e="T1035" id="Seg_10104" s="T1034">this.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_10105" s="T1035">NEG</ta>
            <ta e="T1037" id="Seg_10106" s="T1036">eat-PRS.[3SG]</ta>
            <ta e="T1039" id="Seg_10107" s="T1038">NEG</ta>
            <ta e="T1040" id="Seg_10108" s="T1039">know-1SG</ta>
            <ta e="T1041" id="Seg_10109" s="T1040">say-IPFVZ.[3SG]</ta>
            <ta e="T1042" id="Seg_10110" s="T1041">corral.[NOM.SG]</ta>
            <ta e="T1043" id="Seg_10111" s="T1042">such.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_10112" s="T1043">or</ta>
            <ta e="T1045" id="Seg_10113" s="T1044">what.[NOM.SG]</ta>
            <ta e="T1046" id="Seg_10114" s="T1045">such.[NOM.SG]</ta>
            <ta e="T1047" id="Seg_10115" s="T1046">we.NOM</ta>
            <ta e="T1048" id="Seg_10116" s="T1047">take-PST-1PL</ta>
            <ta e="T1049" id="Seg_10117" s="T1048">this-GEN</ta>
            <ta e="T1051" id="Seg_10118" s="T1050">sister-LAT/LOC.3SG</ta>
            <ta e="T1052" id="Seg_10119" s="T1051">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T1053" id="Seg_10120" s="T1052">this.[NOM.SG]</ta>
            <ta e="T1054" id="Seg_10121" s="T1053">you.know</ta>
            <ta e="T1055" id="Seg_10122" s="T1054">what.[NOM.SG]=INDEF</ta>
            <ta e="T1056" id="Seg_10123" s="T1055">NEG</ta>
            <ta e="T1057" id="Seg_10124" s="T1056">know-3SG.O</ta>
            <ta e="T1058" id="Seg_10125" s="T1057">also</ta>
            <ta e="T1059" id="Seg_10126" s="T1058">cow.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_10127" s="T1059">so</ta>
            <ta e="T1061" id="Seg_10128" s="T1060">become-RES-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_10129" s="T1061">PTCL</ta>
            <ta e="T1063" id="Seg_10130" s="T1062">thin.[NOM.SG]</ta>
            <ta e="T1064" id="Seg_10131" s="T1063">become-PST.[3SG]</ta>
            <ta e="T1065" id="Seg_10132" s="T1064">and</ta>
            <ta e="T1066" id="Seg_10133" s="T1065">%%</ta>
            <ta e="T1067" id="Seg_10134" s="T1066">fat-NOM/GEN/ACC.3SG</ta>
            <ta e="T1068" id="Seg_10135" s="T1067">many</ta>
            <ta e="T1070" id="Seg_10136" s="T1069">be-PST.[3SG]</ta>
            <ta e="T1071" id="Seg_10137" s="T1070">live-PST.[3SG]</ta>
            <ta e="T1072" id="Seg_10138" s="T1071">we.LAT</ta>
            <ta e="T1074" id="Seg_10139" s="T1073">thin.[NOM.SG]</ta>
            <ta e="T1075" id="Seg_10140" s="T1074">become-RES-PST.[3SG]</ta>
            <ta e="T1077" id="Seg_10141" s="T1076">this.[NOM.SG]</ta>
            <ta e="T1078" id="Seg_10142" s="T1077">one.[NOM.SG]</ta>
            <ta e="T1079" id="Seg_10143" s="T1078">daughter-COM</ta>
            <ta e="T1080" id="Seg_10144" s="T1079">live-DUR-PST</ta>
            <ta e="T1081" id="Seg_10145" s="T1080">then</ta>
            <ta e="T1084" id="Seg_10146" s="T1083">be-PST.[3SG]</ta>
            <ta e="T1085" id="Seg_10147" s="T1084">maybe</ta>
            <ta e="T1086" id="Seg_10148" s="T1085">this.[NOM.SG]</ta>
            <ta e="T1088" id="Seg_10149" s="T1087">what=INDEF</ta>
            <ta e="T1090" id="Seg_10150" s="T1089">dig-PST.[3SG]</ta>
            <ta e="T1091" id="Seg_10151" s="T1090">and</ta>
            <ta e="T1092" id="Seg_10152" s="T1091">now</ta>
            <ta e="T1094" id="Seg_10153" s="T1093">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T1095" id="Seg_10154" s="T1094">PTCL</ta>
            <ta e="T1096" id="Seg_10155" s="T1095">NEG</ta>
            <ta e="T1097" id="Seg_10156" s="T1096">stand-PRS-3PL</ta>
            <ta e="T1099" id="Seg_10157" s="T1098">you.PL.NOM</ta>
            <ta e="T1101" id="Seg_10158" s="T1100">PTCL</ta>
            <ta e="T1102" id="Seg_10159" s="T1101">tomorrow</ta>
            <ta e="T1105" id="Seg_10160" s="T1104">hole.[NOM.SG]</ta>
            <ta e="T1106" id="Seg_10161" s="T1105">tomorrow</ta>
            <ta e="T1122" id="Seg_10162" s="T1106">go-CVB</ta>
            <ta e="T1107" id="Seg_10163" s="T1122">disappear-FUT-1PL</ta>
            <ta e="T1123" id="Seg_10164" s="T1107">go-CVB</ta>
            <ta e="T1108" id="Seg_10165" s="T1123">disappear-FUT-2PL</ta>
            <ta e="T1109" id="Seg_10166" s="T1108">HORT.PL</ta>
            <ta e="T1110" id="Seg_10167" s="T1109">hug-FUT-1PL</ta>
            <ta e="T1111" id="Seg_10168" s="T1110">and</ta>
            <ta e="T1112" id="Seg_10169" s="T1111">kiss-FUT-1PL</ta>
            <ta e="T1113" id="Seg_10170" s="T1112">then</ta>
            <ta e="T1114" id="Seg_10171" s="T1113">go-IMP.2PL</ta>
            <ta e="T1115" id="Seg_10172" s="T1114">God-COM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T1" id="Seg_10173" s="T0">лететь-DUR.[3SG]</ta>
            <ta e="T2" id="Seg_10174" s="T1">PTCL</ta>
            <ta e="T3" id="Seg_10175" s="T2">кричать-DUR.[3SG]</ta>
            <ta e="T4" id="Seg_10176" s="T3">сидеть-FUT-3SG</ta>
            <ta e="T5" id="Seg_10177" s="T4">так</ta>
            <ta e="T6" id="Seg_10178" s="T5">земля.[NOM.SG]</ta>
            <ta e="T7" id="Seg_10179" s="T6">земля.[NOM.SG]</ta>
            <ta e="T8" id="Seg_10180" s="T7">%%-DUR.[3SG]</ta>
            <ta e="T9" id="Seg_10181" s="T8">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_10182" s="T9">ружье.[NOM.SG]</ta>
            <ta e="T12" id="Seg_10183" s="T11">какой</ta>
            <ta e="T13" id="Seg_10184" s="T12">такой</ta>
            <ta e="T16" id="Seg_10185" s="T15">зад-LAT/LOC.3SG</ta>
            <ta e="T18" id="Seg_10186" s="T17">этот.[NOM.SG]</ta>
            <ta e="T19" id="Seg_10187" s="T18">ружье.[NOM.SG]</ta>
            <ta e="T21" id="Seg_10188" s="T20">когда</ta>
            <ta e="T22" id="Seg_10189" s="T21">мы.NOM</ta>
            <ta e="T23" id="Seg_10190" s="T22">много</ta>
            <ta e="T24" id="Seg_10191" s="T23">быть-PST-1PL</ta>
            <ta e="T25" id="Seg_10192" s="T24">камасинец-PL</ta>
            <ta e="T26" id="Seg_10193" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_10194" s="T26">идти-PST-1SG</ta>
            <ta e="T28" id="Seg_10195" s="T27">большой.[NOM.SG]</ta>
            <ta e="T29" id="Seg_10196" s="T28">гора-LOC</ta>
            <ta e="T30" id="Seg_10197" s="T29">Белогорье-LOC</ta>
            <ta e="T31" id="Seg_10198" s="T30">там</ta>
            <ta e="T32" id="Seg_10199" s="T31">рыба.[NOM.SG]</ta>
            <ta e="T33" id="Seg_10200" s="T32">ловить-PST-1SG</ta>
            <ta e="T34" id="Seg_10201" s="T33">и</ta>
            <ta e="T35" id="Seg_10202" s="T34">мясо.[NOM.SG]</ta>
            <ta e="T36" id="Seg_10203" s="T35">убить-PST-1SG</ta>
            <ta e="T37" id="Seg_10204" s="T36">тогда</ta>
            <ta e="T38" id="Seg_10205" s="T37">весь</ta>
            <ta e="T39" id="Seg_10206" s="T38">гора-PL</ta>
            <ta e="T40" id="Seg_10207" s="T39">черный-PL</ta>
            <ta e="T41" id="Seg_10208" s="T40">остаться-MOM-PST-3PL</ta>
            <ta e="T42" id="Seg_10209" s="T41">я.NOM</ta>
            <ta e="T43" id="Seg_10210" s="T42">сила-NOM/GEN/ACC.1SG</ta>
            <ta e="T44" id="Seg_10211" s="T43">PTCL</ta>
            <ta e="T45" id="Seg_10212" s="T44">NEG.EX.[3SG]</ta>
            <ta e="T46" id="Seg_10213" s="T45">PTCL</ta>
            <ta e="T47" id="Seg_10214" s="T46">умереть-RES-PST.[3SG]</ta>
            <ta e="T48" id="Seg_10215" s="T47">а</ta>
            <ta e="T49" id="Seg_10216" s="T48">там</ta>
            <ta e="T50" id="Seg_10217" s="T49">где</ta>
            <ta e="T51" id="Seg_10218" s="T50">чум-PL</ta>
            <ta e="T52" id="Seg_10219" s="T51">стоять-PRS-PST-3PL</ta>
            <ta e="T53" id="Seg_10220" s="T52">PTCL</ta>
            <ta e="T54" id="Seg_10221" s="T53">упасть-MOM-PST-3PL</ta>
            <ta e="T55" id="Seg_10222" s="T54">и</ta>
            <ta e="T56" id="Seg_10223" s="T55">весь</ta>
            <ta e="T57" id="Seg_10224" s="T56">умереть-RES-PST-3PL</ta>
            <ta e="T58" id="Seg_10225" s="T57">только</ta>
            <ta e="T59" id="Seg_10226" s="T58">береста.[NOM.SG]</ta>
            <ta e="T60" id="Seg_10227" s="T59">сверлить-MOM-PST-3PL</ta>
            <ta e="T62" id="Seg_10228" s="T61">сейчас</ta>
            <ta e="T64" id="Seg_10229" s="T63">что.[NOM.SG]=INDEF</ta>
            <ta e="T65" id="Seg_10230" s="T64">NEG</ta>
            <ta e="T66" id="Seg_10231" s="T65">видеть-PRS-1SG</ta>
            <ta e="T67" id="Seg_10232" s="T66">что.[NOM.SG]=INDEF</ta>
            <ta e="T68" id="Seg_10233" s="T67">NEG</ta>
            <ta e="T70" id="Seg_10234" s="T69">слушать-PRS-1SG</ta>
            <ta e="T71" id="Seg_10235" s="T70">весь</ta>
            <ta e="T73" id="Seg_10236" s="T72">сила-NOM/GEN/ACC.1SG</ta>
            <ta e="T1117" id="Seg_10237" s="T73">пойти-CVB</ta>
            <ta e="T74" id="Seg_10238" s="T1117">исчезнуть-PST.[3SG]</ta>
            <ta e="T75" id="Seg_10239" s="T74">сейчас</ta>
            <ta e="T76" id="Seg_10240" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_10241" s="T76">один-NOM/GEN/ACC.1SG</ta>
            <ta e="T78" id="Seg_10242" s="T77">жить-DUR-1SG</ta>
            <ta e="T79" id="Seg_10243" s="T78">кто.[NOM.SG]=INDEF</ta>
            <ta e="T80" id="Seg_10244" s="T79">NEG.EX.[3SG]</ta>
            <ta e="T81" id="Seg_10245" s="T80">весь</ta>
            <ta e="T82" id="Seg_10246" s="T81">умереть-RES-PST-3PL</ta>
            <ta e="T83" id="Seg_10247" s="T82">я.NOM</ta>
            <ta e="T84" id="Seg_10248" s="T83">камасинец-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T86" id="Seg_10249" s="T85">сейчас</ta>
            <ta e="T87" id="Seg_10250" s="T86">весь</ta>
            <ta e="T88" id="Seg_10251" s="T87">дорога-PL-ACC.3SG</ta>
            <ta e="T89" id="Seg_10252" s="T88">PTCL</ta>
            <ta e="T90" id="Seg_10253" s="T89">остаться-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_10254" s="T90">весь</ta>
            <ta e="T92" id="Seg_10255" s="T91">трава-INS</ta>
            <ta e="T93" id="Seg_10256" s="T92">расти-MOM-PST.[3SG]</ta>
            <ta e="T95" id="Seg_10257" s="T94">я.NOM</ta>
            <ta e="T96" id="Seg_10258" s="T95">очень</ta>
            <ta e="T97" id="Seg_10259" s="T96">красивый.[NOM.SG]</ta>
            <ta e="T98" id="Seg_10260" s="T97">стать-PST-1SG</ta>
            <ta e="T99" id="Seg_10261" s="T98">а</ta>
            <ta e="T100" id="Seg_10262" s="T99">куда</ta>
            <ta e="T101" id="Seg_10263" s="T100">этот.[NOM.SG]</ta>
            <ta e="T102" id="Seg_10264" s="T101">красивый.[NOM.SG]</ta>
            <ta e="T1118" id="Seg_10265" s="T102">пойти-CVB</ta>
            <ta e="T103" id="Seg_10266" s="T1118">исчезнуть-PST.[3SG]</ta>
            <ta e="T1119" id="Seg_10267" s="T103">пойти-CVB</ta>
            <ta e="T104" id="Seg_10268" s="T1119">исчезнуть-PST.[3SG]</ta>
            <ta e="T105" id="Seg_10269" s="T104">большой.[NOM.SG]</ta>
            <ta e="T106" id="Seg_10270" s="T105">гора-PL-LAT</ta>
            <ta e="T107" id="Seg_10271" s="T106">и</ta>
            <ta e="T108" id="Seg_10272" s="T107">где=INDEF</ta>
            <ta e="T109" id="Seg_10273" s="T108">NEG.EX.[3SG]</ta>
            <ta e="T111" id="Seg_10274" s="T110">сейчас</ta>
            <ta e="T112" id="Seg_10275" s="T111">этот.[NOM.SG]</ta>
            <ta e="T113" id="Seg_10276" s="T112">когда=INDEF</ta>
            <ta e="T114" id="Seg_10277" s="T113">NEG</ta>
            <ta e="T115" id="Seg_10278" s="T114">прийти-FUT-3SG</ta>
            <ta e="T118" id="Seg_10279" s="T117">я.NOM</ta>
            <ta e="T119" id="Seg_10280" s="T118">встать-PST-1SG</ta>
            <ta e="T120" id="Seg_10281" s="T119">переулок-LAT</ta>
            <ta e="T122" id="Seg_10282" s="T121">прийти-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_10283" s="T122">дед.[NOM.SG]</ta>
            <ta e="T124" id="Seg_10284" s="T123">я.NOM</ta>
            <ta e="T125" id="Seg_10285" s="T124">сказать-IPFVZ-1SG</ta>
            <ta e="T126" id="Seg_10286" s="T125">очень</ta>
            <ta e="T127" id="Seg_10287" s="T126">большой.[NOM.SG]</ta>
            <ta e="T128" id="Seg_10288" s="T127">мужчина.[NOM.SG]</ta>
            <ta e="T129" id="Seg_10289" s="T128">прийти-PRS.[3SG]</ta>
            <ta e="T130" id="Seg_10290" s="T129">фотоаппарат-INS</ta>
            <ta e="T131" id="Seg_10291" s="T130">дом-PL</ta>
            <ta e="T132" id="Seg_10292" s="T131">фотографировать-DUR.[3SG]</ta>
            <ta e="T133" id="Seg_10293" s="T132">идти-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_10294" s="T133">тогда</ta>
            <ta e="T135" id="Seg_10295" s="T134">я.NOM</ta>
            <ta e="T136" id="Seg_10296" s="T135">стоять-PST-1SG</ta>
            <ta e="T137" id="Seg_10297" s="T136">дом-ABL</ta>
            <ta e="T138" id="Seg_10298" s="T137">край-LAT/LOC.3SG</ta>
            <ta e="T139" id="Seg_10299" s="T138">этот.[NOM.SG]</ta>
            <ta e="T141" id="Seg_10300" s="T140">PTCL</ta>
            <ta e="T142" id="Seg_10301" s="T141">я.ACC</ta>
            <ta e="T144" id="Seg_10302" s="T143">фотографировать-PST.[3SG]</ta>
            <ta e="T146" id="Seg_10303" s="T145">там</ta>
            <ta e="T147" id="Seg_10304" s="T146">женщина-LOC</ta>
            <ta e="T148" id="Seg_10305" s="T147">рыба-NOM/GEN.3SG</ta>
            <ta e="T149" id="Seg_10306" s="T148">быть-PST.[3SG]</ta>
            <ta e="T150" id="Seg_10307" s="T149">там</ta>
            <ta e="T152" id="Seg_10308" s="T151">этот-GEN</ta>
            <ta e="T153" id="Seg_10309" s="T152">рыба-NOM/GEN.3SG</ta>
            <ta e="T154" id="Seg_10310" s="T153">взять-PST.[3SG]</ta>
            <ta e="T155" id="Seg_10311" s="T154">этот-GEN</ta>
            <ta e="T156" id="Seg_10312" s="T155">невестка-NOM/GEN.3SG</ta>
            <ta e="T157" id="Seg_10313" s="T156">тогда</ta>
            <ta e="T158" id="Seg_10314" s="T157">этот-LAT</ta>
            <ta e="T159" id="Seg_10315" s="T158">прийти-PST.[3SG]</ta>
            <ta e="T160" id="Seg_10316" s="T159">сказать-IPFVZ.[3SG]</ta>
            <ta e="T161" id="Seg_10317" s="T160">я.NOM</ta>
            <ta e="T162" id="Seg_10318" s="T161">ты.DAT</ta>
            <ta e="T163" id="Seg_10319" s="T162">рыба.[NOM.SG]</ta>
            <ta e="T164" id="Seg_10320" s="T163">принести-FUT-1SG</ta>
            <ta e="T165" id="Seg_10321" s="T164">сегодня</ta>
            <ta e="T166" id="Seg_10322" s="T165">а</ta>
            <ta e="T167" id="Seg_10323" s="T166">я.NOM</ta>
            <ta e="T168" id="Seg_10324" s="T167">прийти-PST-1SG</ta>
            <ta e="T169" id="Seg_10325" s="T168">ты.NOM</ta>
            <ta e="T170" id="Seg_10326" s="T169">рыба-NOM/GEN/ACC.2SG</ta>
            <ta e="T171" id="Seg_10327" s="T170">невестка-NOM/GEN.3SG</ta>
            <ta e="T172" id="Seg_10328" s="T171">взять-PST.[3SG]</ta>
            <ta e="T173" id="Seg_10329" s="T172">я.NOM</ta>
            <ta e="T174" id="Seg_10330" s="T173">NEG</ta>
            <ta e="T175" id="Seg_10331" s="T174">взять-PST-1SG</ta>
            <ta e="T177" id="Seg_10332" s="T176">что.[NOM.SG]</ta>
            <ta e="T178" id="Seg_10333" s="T177">этот.[NOM.SG]</ta>
            <ta e="T179" id="Seg_10334" s="T178">я.LAT</ta>
            <ta e="T180" id="Seg_10335" s="T179">дать-FUT-3SG</ta>
            <ta e="T181" id="Seg_10336" s="T180">немного</ta>
            <ta e="T182" id="Seg_10337" s="T181">ну</ta>
            <ta e="T183" id="Seg_10338" s="T182">немного</ta>
            <ta e="T184" id="Seg_10339" s="T183">хватит</ta>
            <ta e="T185" id="Seg_10340" s="T184">этот-PL</ta>
            <ta e="T186" id="Seg_10341" s="T185">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_10342" s="T186">деньги.[NOM.SG]</ta>
            <ta e="T188" id="Seg_10343" s="T187">деньги-LAT</ta>
            <ta e="T190" id="Seg_10344" s="T189">продавать-PST-3PL</ta>
            <ta e="T191" id="Seg_10345" s="T190">а</ta>
            <ta e="T192" id="Seg_10346" s="T191">ты.NOM</ta>
            <ta e="T193" id="Seg_10347" s="T192">NEG</ta>
            <ta e="T194" id="Seg_10348" s="T193">дать-PST-2SG</ta>
            <ta e="T195" id="Seg_10349" s="T194">деньги.[NOM.SG]</ta>
            <ta e="T196" id="Seg_10350" s="T195">и</ta>
            <ta e="T198" id="Seg_10351" s="T197">и</ta>
            <ta e="T199" id="Seg_10352" s="T198">этот-GEN</ta>
            <ta e="T200" id="Seg_10353" s="T199">этот.[NOM.SG]</ta>
            <ta e="T201" id="Seg_10354" s="T200">сколько</ta>
            <ta e="T202" id="Seg_10355" s="T201">дать-FUT-3PL</ta>
            <ta e="T203" id="Seg_10356" s="T202">хватит</ta>
            <ta e="T205" id="Seg_10357" s="T204">тогда</ta>
            <ta e="T206" id="Seg_10358" s="T205">я.NOM</ta>
            <ta e="T207" id="Seg_10359" s="T206">%%</ta>
            <ta e="T208" id="Seg_10360" s="T207">пойти-PST-1SG</ta>
            <ta e="T209" id="Seg_10361" s="T208">Анисья-LAT</ta>
            <ta e="T210" id="Seg_10362" s="T209">этот.[NOM.SG]</ta>
            <ta e="T211" id="Seg_10363" s="T210">сказать-IPFVZ.[3SG]</ta>
            <ta e="T212" id="Seg_10364" s="T211">Эля.[NOM.SG]</ta>
            <ta e="T213" id="Seg_10365" s="T212">спросить-PST.[3SG]</ta>
            <ta e="T214" id="Seg_10366" s="T213">яйцо-PL</ta>
            <ta e="T215" id="Seg_10367" s="T214">я.NOM</ta>
            <ta e="T216" id="Seg_10368" s="T215">собирать-PST-1SG</ta>
            <ta e="T217" id="Seg_10369" s="T216">десять.[NOM.SG]</ta>
            <ta e="T218" id="Seg_10370" s="T217">яйцо-PL</ta>
            <ta e="T221" id="Seg_10371" s="T220">дать-FUT-1SG</ta>
            <ta e="T222" id="Seg_10372" s="T221">десять.[NOM.SG]</ta>
            <ta e="T224" id="Seg_10373" s="T223">один.[NOM.SG]</ta>
            <ta e="T225" id="Seg_10374" s="T224">яйцо-PL</ta>
            <ta e="T226" id="Seg_10375" s="T225">десять.[NOM.SG]</ta>
            <ta e="T227" id="Seg_10376" s="T226">копейка-INS</ta>
            <ta e="T228" id="Seg_10377" s="T227">дать-FUT-1SG</ta>
            <ta e="T229" id="Seg_10378" s="T228">этот-LAT</ta>
            <ta e="T231" id="Seg_10379" s="T230">Эля.[NOM.SG]</ta>
            <ta e="T232" id="Seg_10380" s="T231">сказать-IPFVZ.[3SG]</ta>
            <ta e="T233" id="Seg_10381" s="T232">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T234" id="Seg_10382" s="T233">мы.LAT</ta>
            <ta e="T235" id="Seg_10383" s="T234">а</ta>
            <ta e="T236" id="Seg_10384" s="T235">я.NOM</ta>
            <ta e="T237" id="Seg_10385" s="T236">сказать-IPFVZ-1SG</ta>
            <ta e="T238" id="Seg_10386" s="T237">нет</ta>
            <ta e="T239" id="Seg_10387" s="T238">NEG</ta>
            <ta e="T240" id="Seg_10388" s="T239">пойти-FUT-1SG</ta>
            <ta e="T241" id="Seg_10389" s="T240">очень</ta>
            <ta e="T242" id="Seg_10390" s="T241">далеко</ta>
            <ta e="T243" id="Seg_10391" s="T242">что.[NOM.SG]</ta>
            <ta e="T244" id="Seg_10392" s="T243">далеко-CNG</ta>
            <ta e="T245" id="Seg_10393" s="T244">сесть-IMP.2SG</ta>
            <ta e="T247" id="Seg_10394" s="T246">птица-LAT</ta>
            <ta e="T248" id="Seg_10395" s="T247">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T249" id="Seg_10396" s="T248">птица-LAT</ta>
            <ta e="T250" id="Seg_10397" s="T249">и</ta>
            <ta e="T251" id="Seg_10398" s="T250">скоро</ta>
            <ta e="T252" id="Seg_10399" s="T251">прийти-FUT-2SG</ta>
            <ta e="T253" id="Seg_10400" s="T252">нет</ta>
            <ta e="T254" id="Seg_10401" s="T253">я.NOM</ta>
            <ta e="T255" id="Seg_10402" s="T254">бояться-PRS-1SG</ta>
            <ta e="T256" id="Seg_10403" s="T255">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T258" id="Seg_10404" s="T257">птица-LAT</ta>
            <ta e="T259" id="Seg_10405" s="T258">сесть-INF.LAT</ta>
            <ta e="T260" id="Seg_10406" s="T259">а.то</ta>
            <ta e="T261" id="Seg_10407" s="T260">умереть-RES-FUT-2SG</ta>
            <ta e="T262" id="Seg_10408" s="T261">нет</ta>
            <ta e="T263" id="Seg_10409" s="T262">NEG</ta>
            <ta e="T264" id="Seg_10410" s="T263">умереть-RES-FUT-2SG</ta>
            <ta e="T265" id="Seg_10411" s="T264">там</ta>
            <ta e="T266" id="Seg_10412" s="T265">хороший</ta>
            <ta e="T267" id="Seg_10413" s="T266">сидеть-INF.LAT</ta>
            <ta e="T268" id="Seg_10414" s="T267">как</ta>
            <ta e="T269" id="Seg_10415" s="T268">дом-LOC</ta>
            <ta e="T270" id="Seg_10416" s="T269">скоро</ta>
            <ta e="T271" id="Seg_10417" s="T270">дом-LAT-2SG</ta>
            <ta e="T272" id="Seg_10418" s="T271">прийти-FUT-2SG</ta>
            <ta e="T273" id="Seg_10419" s="T272">мы.LAT</ta>
            <ta e="T274" id="Seg_10420" s="T273">жить-FUT-2SG</ta>
            <ta e="T276" id="Seg_10421" s="T275">три-COLL</ta>
            <ta e="T277" id="Seg_10422" s="T276">пойти-FUT-1PL</ta>
            <ta e="T279" id="Seg_10423" s="T278">вечер-LOC.ADV</ta>
            <ta e="T280" id="Seg_10424" s="T279">пойти-FUT-1SG</ta>
            <ta e="T281" id="Seg_10425" s="T280">один.[NOM.SG]</ta>
            <ta e="T282" id="Seg_10426" s="T281">мужчина-LAT</ta>
            <ta e="T287" id="Seg_10427" s="T286">один.[NOM.SG]</ta>
            <ta e="T288" id="Seg_10428" s="T287">мужчина-LAT</ta>
            <ta e="T289" id="Seg_10429" s="T288">сказать-FUT-1SG</ta>
            <ta e="T291" id="Seg_10430" s="T290">нести-IMP.2SG.O</ta>
            <ta e="T292" id="Seg_10431" s="T291">этот.[NOM.SG]</ta>
            <ta e="T293" id="Seg_10432" s="T292">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T294" id="Seg_10433" s="T293">больница-LAT</ta>
            <ta e="T295" id="Seg_10434" s="T294">этот-LAT</ta>
            <ta e="T297" id="Seg_10435" s="T296">этот-LAT</ta>
            <ta e="T298" id="Seg_10436" s="T297">бумага.[NOM.SG]</ta>
            <ta e="T299" id="Seg_10437" s="T298">дать-PST-3PL</ta>
            <ta e="T300" id="Seg_10438" s="T299">а</ta>
            <ta e="T301" id="Seg_10439" s="T300">NEG</ta>
            <ta e="T304" id="Seg_10440" s="T303">нести-FUT-2SG</ta>
            <ta e="T305" id="Seg_10441" s="T304">так</ta>
            <ta e="T306" id="Seg_10442" s="T305">позвать-IMP.2SG.O</ta>
            <ta e="T309" id="Seg_10443" s="T308">этот.[NOM.SG]</ta>
            <ta e="T310" id="Seg_10444" s="T309">прийти-FUT-3SG</ta>
            <ta e="T311" id="Seg_10445" s="T310">и</ta>
            <ta e="T312" id="Seg_10446" s="T311">нести-RES-FUT-3SG</ta>
            <ta e="T313" id="Seg_10447" s="T312">этот-ACC</ta>
            <ta e="T314" id="Seg_10448" s="T313">так</ta>
            <ta e="T315" id="Seg_10449" s="T314">сказать-FUT-1SG</ta>
            <ta e="T316" id="Seg_10450" s="T315">этот-LAT</ta>
            <ta e="T317" id="Seg_10451" s="T316">может.быть</ta>
            <ta e="T318" id="Seg_10452" s="T317">этот.[NOM.SG]</ta>
            <ta e="T319" id="Seg_10453" s="T318">слушать-FUT-3SG</ta>
            <ta e="T320" id="Seg_10454" s="T319">а.то</ta>
            <ta e="T321" id="Seg_10455" s="T320">ведь</ta>
            <ta e="T322" id="Seg_10456" s="T321">ты.NOM</ta>
            <ta e="T323" id="Seg_10457" s="T322">%самовар.[NOM.SG]</ta>
            <ta e="T324" id="Seg_10458" s="T323">взять-PST-2SG</ta>
            <ta e="T325" id="Seg_10459" s="T324">а</ta>
            <ta e="T326" id="Seg_10460" s="T325">люди.[NOM.SG]</ta>
            <ta e="T327" id="Seg_10461" s="T326">PTCL</ta>
            <ta e="T328" id="Seg_10462" s="T327">сказать-PRS.[3SG]</ta>
            <ta e="T330" id="Seg_10463" s="T329">%самовар.[NOM.SG]</ta>
            <ta e="T331" id="Seg_10464" s="T330">взять-PST-2SG</ta>
            <ta e="T332" id="Seg_10465" s="T331">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T333" id="Seg_10466" s="T332">что.[NOM.SG]=INDEF</ta>
            <ta e="T334" id="Seg_10467" s="T333">NEG</ta>
            <ta e="T335" id="Seg_10468" s="T334">помогать-PST.[3SG]</ta>
            <ta e="T336" id="Seg_10469" s="T335">этот.[NOM.SG]</ta>
            <ta e="T337" id="Seg_10470" s="T336">женщина-LAT</ta>
            <ta e="T339" id="Seg_10471" s="T338">а</ta>
            <ta e="T340" id="Seg_10472" s="T339">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T341" id="Seg_10473" s="T340">умереть-RES-PST.[3SG]</ta>
            <ta e="T342" id="Seg_10474" s="T341">а</ta>
            <ta e="T343" id="Seg_10475" s="T342">дом.[NOM.SG]</ta>
            <ta e="T344" id="Seg_10476" s="T343">%%</ta>
            <ta e="T345" id="Seg_10477" s="T344">ведь</ta>
            <ta e="T346" id="Seg_10478" s="T345">ты.NOM</ta>
            <ta e="T347" id="Seg_10479" s="T346">быть-PST-2SG</ta>
            <ta e="T349" id="Seg_10480" s="T348">собака.[NOM.SG]</ta>
            <ta e="T350" id="Seg_10481" s="T349">один</ta>
            <ta e="T351" id="Seg_10482" s="T350">жить-PST.[3SG]</ta>
            <ta e="T352" id="Seg_10483" s="T351">жить-PST.[3SG]</ta>
            <ta e="T354" id="Seg_10484" s="T353">один-LAT</ta>
            <ta e="T355" id="Seg_10485" s="T354">жить-INF.LAT</ta>
            <ta e="T356" id="Seg_10486" s="T355">пойти-FUT-1SG</ta>
            <ta e="T357" id="Seg_10487" s="T356">товарищ</ta>
            <ta e="T358" id="Seg_10488" s="T357">сам-LAT/LOC.3SG</ta>
            <ta e="T359" id="Seg_10489" s="T358">найти-FUT-1SG</ta>
            <ta e="T360" id="Seg_10490" s="T359">пойти-PST.[3SG]</ta>
            <ta e="T361" id="Seg_10491" s="T360">пойти-PST.[3SG]</ta>
            <ta e="T362" id="Seg_10492" s="T361">видеть-PST.[3SG]</ta>
            <ta e="T363" id="Seg_10493" s="T362">заяц-NOM/GEN/ACC.1SG</ta>
            <ta e="T364" id="Seg_10494" s="T363">HORT</ta>
            <ta e="T365" id="Seg_10495" s="T364">ты.NOM-COM</ta>
            <ta e="T366" id="Seg_10496" s="T365">жить-INF.LAT</ta>
            <ta e="T367" id="Seg_10497" s="T366">HORT</ta>
            <ta e="T368" id="Seg_10498" s="T367">жить-INF.LAT</ta>
            <ta e="T369" id="Seg_10499" s="T368">тогда</ta>
            <ta e="T370" id="Seg_10500" s="T369">вечер.[NOM.SG]</ta>
            <ta e="T371" id="Seg_10501" s="T370">стать-RES-PST.[3SG]</ta>
            <ta e="T372" id="Seg_10502" s="T371">этот-PL</ta>
            <ta e="T373" id="Seg_10503" s="T372">ложиться-PST-3PL</ta>
            <ta e="T374" id="Seg_10504" s="T373">спать-INF.LAT</ta>
            <ta e="T375" id="Seg_10505" s="T374">заяц.[NOM.SG]</ta>
            <ta e="T376" id="Seg_10506" s="T375">спать-MOM-PST.[3SG]</ta>
            <ta e="T377" id="Seg_10507" s="T376">а</ta>
            <ta e="T379" id="Seg_10508" s="T378">собака.[NOM.SG]</ta>
            <ta e="T380" id="Seg_10509" s="T379">NEG</ta>
            <ta e="T381" id="Seg_10510" s="T380">спать-PRS.[3SG]</ta>
            <ta e="T382" id="Seg_10511" s="T381">вечер-LOC.ADV</ta>
            <ta e="T383" id="Seg_10512" s="T382">PTCL</ta>
            <ta e="T384" id="Seg_10513" s="T383">кричать-DUR.[3SG]</ta>
            <ta e="T385" id="Seg_10514" s="T384">а</ta>
            <ta e="T386" id="Seg_10515" s="T385">заяц.[NOM.SG]</ta>
            <ta e="T387" id="Seg_10516" s="T386">пугать-MOM-PST.[3SG]</ta>
            <ta e="T388" id="Seg_10517" s="T387">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T389" id="Seg_10518" s="T388">пятка-LAT/LOC.3SG</ta>
            <ta e="T1120" id="Seg_10519" s="T389">пойти-CVB</ta>
            <ta e="T390" id="Seg_10520" s="T1120">исчезнуть-PST.[3SG]</ta>
            <ta e="T391" id="Seg_10521" s="T390">что.[NOM.SG]</ta>
            <ta e="T392" id="Seg_10522" s="T391">кричать-PRS-2SG</ta>
            <ta e="T393" id="Seg_10523" s="T392">прийти-FUT-3SG</ta>
            <ta e="T394" id="Seg_10524" s="T393">волк.[NOM.SG]</ta>
            <ta e="T395" id="Seg_10525" s="T394">и</ta>
            <ta e="T396" id="Seg_10526" s="T395">мы.ACC</ta>
            <ta e="T397" id="Seg_10527" s="T396">съесть-FUT-3SG</ta>
            <ta e="T398" id="Seg_10528" s="T397">а</ta>
            <ta e="T399" id="Seg_10529" s="T398">собака.[NOM.SG]</ta>
            <ta e="T400" id="Seg_10530" s="T399">сказать-IPFVZ.[3SG]</ta>
            <ta e="T401" id="Seg_10531" s="T400">NEG</ta>
            <ta e="T402" id="Seg_10532" s="T401">хороший.[NOM.SG]</ta>
            <ta e="T403" id="Seg_10533" s="T402">я.NOM</ta>
            <ta e="T404" id="Seg_10534" s="T403">товарищ.[NOM.SG]</ta>
            <ta e="T405" id="Seg_10535" s="T404">пойти-FUT-1SG</ta>
            <ta e="T406" id="Seg_10536" s="T405">волк.[NOM.SG]</ta>
            <ta e="T408" id="Seg_10537" s="T407">волк.[NOM.SG]</ta>
            <ta e="T409" id="Seg_10538" s="T408">смотреть-FRQ-INF.LAT</ta>
            <ta e="T410" id="Seg_10539" s="T409">тогда</ta>
            <ta e="T411" id="Seg_10540" s="T410">пойти-PST.[3SG]</ta>
            <ta e="T412" id="Seg_10541" s="T411">прийти-PRS.[3SG]</ta>
            <ta e="T413" id="Seg_10542" s="T412">прийти-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_10543" s="T413">волк.[NOM.SG]</ta>
            <ta e="T415" id="Seg_10544" s="T414">этот-LAT</ta>
            <ta e="T416" id="Seg_10545" s="T415">прийти-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_10546" s="T416">HORT</ta>
            <ta e="T418" id="Seg_10547" s="T417">ты.NOM-COM</ta>
            <ta e="T419" id="Seg_10548" s="T418">жить-INF.LAT</ta>
            <ta e="T420" id="Seg_10549" s="T419">HORT</ta>
            <ta e="T421" id="Seg_10550" s="T420">жить-INF.LAT</ta>
            <ta e="T422" id="Seg_10551" s="T421">вечер</ta>
            <ta e="T423" id="Seg_10552" s="T422">прийти-PST.[3SG]</ta>
            <ta e="T425" id="Seg_10553" s="T424">этот-PL</ta>
            <ta e="T426" id="Seg_10554" s="T425">ложиться-PST-3PL</ta>
            <ta e="T427" id="Seg_10555" s="T426">спать-INF.LAT</ta>
            <ta e="T428" id="Seg_10556" s="T427">собака.[NOM.SG]</ta>
            <ta e="T429" id="Seg_10557" s="T428">вечер-LOC.ADV</ta>
            <ta e="T430" id="Seg_10558" s="T429">кричать-MOM-PST.[3SG]</ta>
            <ta e="T431" id="Seg_10559" s="T430">а</ta>
            <ta e="T432" id="Seg_10560" s="T431">волк.[NOM.SG]</ta>
            <ta e="T433" id="Seg_10561" s="T432">сказать-IPFVZ.[3SG]</ta>
            <ta e="T434" id="Seg_10562" s="T433">что.[NOM.SG]</ta>
            <ta e="T435" id="Seg_10563" s="T434">кричать-DUR-2SG</ta>
            <ta e="T436" id="Seg_10564" s="T435">сейчас</ta>
            <ta e="T437" id="Seg_10565" s="T436">медведь.[NOM.SG]</ta>
            <ta e="T438" id="Seg_10566" s="T437">прийти-FUT-3SG</ta>
            <ta e="T439" id="Seg_10567" s="T438">и</ta>
            <ta e="T440" id="Seg_10568" s="T439">мы.ACC</ta>
            <ta e="T441" id="Seg_10569" s="T440">съесть-FUT-3SG</ta>
            <ta e="T442" id="Seg_10570" s="T441">тогда</ta>
            <ta e="T443" id="Seg_10571" s="T442">собака.[NOM.SG]</ta>
            <ta e="T444" id="Seg_10572" s="T443">утро-LOC.ADV</ta>
            <ta e="T445" id="Seg_10573" s="T444">встать-PST.[3SG]</ta>
            <ta e="T446" id="Seg_10574" s="T445">NEG</ta>
            <ta e="T447" id="Seg_10575" s="T446">хороший</ta>
            <ta e="T448" id="Seg_10576" s="T447">товарищ.[NOM.SG]</ta>
            <ta e="T449" id="Seg_10577" s="T448">пойти-FUT-1SG</ta>
            <ta e="T450" id="Seg_10578" s="T449">медведь.[NOM.SG]</ta>
            <ta e="T452" id="Seg_10579" s="T451">смотреть-FRQ-INF.LAT</ta>
            <ta e="T454" id="Seg_10580" s="T453">тогда</ta>
            <ta e="T455" id="Seg_10581" s="T454">пойти-PST.[3SG]</ta>
            <ta e="T456" id="Seg_10582" s="T455">прийти-PRS.[3SG]</ta>
            <ta e="T457" id="Seg_10583" s="T456">прийти-PRS.[3SG]</ta>
            <ta e="T458" id="Seg_10584" s="T457">медведь.[NOM.SG]</ta>
            <ta e="T459" id="Seg_10585" s="T458">прийти-PRS.[3SG]</ta>
            <ta e="T460" id="Seg_10586" s="T459">медведь.[NOM.SG]</ta>
            <ta e="T461" id="Seg_10587" s="T460">медведь.[NOM.SG]</ta>
            <ta e="T462" id="Seg_10588" s="T461">HORT</ta>
            <ta e="T463" id="Seg_10589" s="T462">ты.NOM-COM</ta>
            <ta e="T464" id="Seg_10590" s="T463">жить-INF.LAT</ta>
            <ta e="T465" id="Seg_10591" s="T464">HORT</ta>
            <ta e="T466" id="Seg_10592" s="T465">тогда</ta>
            <ta e="T467" id="Seg_10593" s="T466">вечер</ta>
            <ta e="T468" id="Seg_10594" s="T467">стать-RES-PST.[3SG]</ta>
            <ta e="T470" id="Seg_10595" s="T469">ложиться-PST-3PL</ta>
            <ta e="T471" id="Seg_10596" s="T470">спать-INF.LAT</ta>
            <ta e="T472" id="Seg_10597" s="T471">собака.[NOM.SG]</ta>
            <ta e="T473" id="Seg_10598" s="T472">вечер-LOC.ADV</ta>
            <ta e="T474" id="Seg_10599" s="T473">PTCL</ta>
            <ta e="T475" id="Seg_10600" s="T474">кричать-MOM-PST.[3SG]</ta>
            <ta e="T476" id="Seg_10601" s="T475">PTCL</ta>
            <ta e="T477" id="Seg_10602" s="T476">а</ta>
            <ta e="T478" id="Seg_10603" s="T477">медведь.[NOM.SG]</ta>
            <ta e="T479" id="Seg_10604" s="T478">что.[NOM.SG]</ta>
            <ta e="T480" id="Seg_10605" s="T479">кричать-PRS-2SG</ta>
            <ta e="T481" id="Seg_10606" s="T480">мужчина.[NOM.SG]</ta>
            <ta e="T482" id="Seg_10607" s="T481">прийти-FUT-3SG</ta>
            <ta e="T483" id="Seg_10608" s="T482">и</ta>
            <ta e="T484" id="Seg_10609" s="T483">мы.ACC</ta>
            <ta e="T485" id="Seg_10610" s="T484">убить-FUT-3SG</ta>
            <ta e="T486" id="Seg_10611" s="T485">а</ta>
            <ta e="T487" id="Seg_10612" s="T486">этот.[NOM.SG]</ta>
            <ta e="T488" id="Seg_10613" s="T487">сказать-IPFVZ.[3SG]</ta>
            <ta e="T489" id="Seg_10614" s="T488">NEG</ta>
            <ta e="T490" id="Seg_10615" s="T489">хороший</ta>
            <ta e="T491" id="Seg_10616" s="T490">товарищ.[NOM.SG]</ta>
            <ta e="T492" id="Seg_10617" s="T491">пойти-FUT-1SG</ta>
            <ta e="T493" id="Seg_10618" s="T492">мужчина.[NOM.SG]</ta>
            <ta e="T494" id="Seg_10619" s="T493">смотреть-FRQ-INF.LAT</ta>
            <ta e="T495" id="Seg_10620" s="T494">утро-LOC.ADV</ta>
            <ta e="T496" id="Seg_10621" s="T495">встать-PST.[3SG]</ta>
            <ta e="T497" id="Seg_10622" s="T496">пойти-PST.[3SG]</ta>
            <ta e="T499" id="Seg_10623" s="T498">весь</ta>
            <ta e="T500" id="Seg_10624" s="T499">дерево-PL</ta>
            <ta e="T501" id="Seg_10625" s="T500">идти-PST.[3SG]</ta>
            <ta e="T502" id="Seg_10626" s="T501">идти-PST.[3SG]</ta>
            <ta e="T503" id="Seg_10627" s="T502">тогда</ta>
            <ta e="T504" id="Seg_10628" s="T503">степь-LAT</ta>
            <ta e="T505" id="Seg_10629" s="T504">прийти-PST.[3SG]</ta>
            <ta e="T506" id="Seg_10630" s="T505">видеть-PRS-3SG.O</ta>
            <ta e="T507" id="Seg_10631" s="T506">PTCL</ta>
            <ta e="T508" id="Seg_10632" s="T507">мужчина.[NOM.SG]</ta>
            <ta e="T509" id="Seg_10633" s="T508">прийти-PRS.[3SG]</ta>
            <ta e="T512" id="Seg_10634" s="T511">дерево-PL</ta>
            <ta e="T513" id="Seg_10635" s="T512">взять-INF.LAT</ta>
            <ta e="T514" id="Seg_10636" s="T513">лошадь-INS</ta>
            <ta e="T515" id="Seg_10637" s="T514">тогда</ta>
            <ta e="T516" id="Seg_10638" s="T515">этот.[NOM.SG]</ta>
            <ta e="T517" id="Seg_10639" s="T516">сказать-IPFVZ.[3SG]</ta>
            <ta e="T518" id="Seg_10640" s="T517">мужчина.[NOM.SG]</ta>
            <ta e="T519" id="Seg_10641" s="T518">мужчина.[NOM.SG]</ta>
            <ta e="T520" id="Seg_10642" s="T519">HORT</ta>
            <ta e="T521" id="Seg_10643" s="T520">жить-INF.LAT</ta>
            <ta e="T522" id="Seg_10644" s="T521">этот.[NOM.SG]</ta>
            <ta e="T523" id="Seg_10645" s="T522">принести-PST.[3SG]</ta>
            <ta e="T524" id="Seg_10646" s="T523">этот-ACC</ta>
            <ta e="T525" id="Seg_10647" s="T524">дом-LAT/LOC.3SG</ta>
            <ta e="T526" id="Seg_10648" s="T525">ложиться-PST-3PL</ta>
            <ta e="T527" id="Seg_10649" s="T526">спать-INF.LAT</ta>
            <ta e="T528" id="Seg_10650" s="T527">мужчина.[NOM.SG]</ta>
            <ta e="T529" id="Seg_10651" s="T528">спать-MOM-PST.[3SG]</ta>
            <ta e="T530" id="Seg_10652" s="T529">а</ta>
            <ta e="T531" id="Seg_10653" s="T530">собака.[NOM.SG]</ta>
            <ta e="T532" id="Seg_10654" s="T531">PTCL</ta>
            <ta e="T533" id="Seg_10655" s="T532">INCH</ta>
            <ta e="T534" id="Seg_10656" s="T533">кричать-INF.LAT</ta>
            <ta e="T535" id="Seg_10657" s="T534">а</ta>
            <ta e="T536" id="Seg_10658" s="T535">мужчина.[NOM.SG]</ta>
            <ta e="T537" id="Seg_10659" s="T536">встать-PST.[3SG]</ta>
            <ta e="T538" id="Seg_10660" s="T537">сказать-IPFVZ.[3SG]</ta>
            <ta e="T539" id="Seg_10661" s="T538">ты.NOM</ta>
            <ta e="T540" id="Seg_10662" s="T539">быть.голодным-PRS-2SG</ta>
            <ta e="T541" id="Seg_10663" s="T540">есть-EP-IMP.2SG</ta>
            <ta e="T542" id="Seg_10664" s="T541">и</ta>
            <ta e="T543" id="Seg_10665" s="T542">спать-EP-IMP.2SG</ta>
            <ta e="T544" id="Seg_10666" s="T543">я.LAT</ta>
            <ta e="T545" id="Seg_10667" s="T544">принести-IMP.2SG</ta>
            <ta e="T548" id="Seg_10668" s="T547">дать-IMP.2SG</ta>
            <ta e="T549" id="Seg_10669" s="T548">спать-INF.LAT</ta>
            <ta e="T550" id="Seg_10670" s="T549">весь</ta>
            <ta e="T552" id="Seg_10671" s="T551">один.[NOM.SG]</ta>
            <ta e="T553" id="Seg_10672" s="T552">мужчина.[NOM.SG]</ta>
            <ta e="T554" id="Seg_10673" s="T553">пойти-PST.[3SG]</ta>
            <ta e="T555" id="Seg_10674" s="T554">дерево-VBLZ-CVB</ta>
            <ta e="T557" id="Seg_10675" s="T556">дерево-PL</ta>
            <ta e="T558" id="Seg_10676" s="T557">взять-PST.[3SG]</ta>
            <ta e="T559" id="Seg_10677" s="T558">и</ta>
            <ta e="T560" id="Seg_10678" s="T559">прийти-PRS.[3SG]</ta>
            <ta e="T561" id="Seg_10679" s="T560">этот-LAT</ta>
            <ta e="T562" id="Seg_10680" s="T561">медведь.[NOM.SG]</ta>
            <ta e="T563" id="Seg_10681" s="T562">прийти-PRS.[3SG]</ta>
            <ta e="T564" id="Seg_10682" s="T563">я.NOM</ta>
            <ta e="T565" id="Seg_10683" s="T564">скоро</ta>
            <ta e="T566" id="Seg_10684" s="T565">ты.NOM</ta>
            <ta e="T567" id="Seg_10685" s="T566">съесть-FUT-1SG</ta>
            <ta e="T568" id="Seg_10686" s="T567">ты.NOM</ta>
            <ta e="T569" id="Seg_10687" s="T568">NEG.AUX-IMP.2SG</ta>
            <ta e="T570" id="Seg_10688" s="T569">съесть-EP-CNG</ta>
            <ta e="T571" id="Seg_10689" s="T570">я.ACC</ta>
            <ta e="T572" id="Seg_10690" s="T571">там</ta>
            <ta e="T573" id="Seg_10691" s="T572">охотник-PL</ta>
            <ta e="T574" id="Seg_10692" s="T573">прийти-PRS-3PL</ta>
            <ta e="T575" id="Seg_10693" s="T574">убить-FUT-3PL</ta>
            <ta e="T576" id="Seg_10694" s="T575">ты.ACC</ta>
            <ta e="T577" id="Seg_10695" s="T576">этот.[NOM.SG]</ta>
            <ta e="T578" id="Seg_10696" s="T577">PTCL</ta>
            <ta e="T579" id="Seg_10697" s="T578">пугать-MOM-PST.[3SG]</ta>
            <ta e="T580" id="Seg_10698" s="T579">класть-EP-IMP.2SG</ta>
            <ta e="T581" id="Seg_10699" s="T580">я.ACC</ta>
            <ta e="T582" id="Seg_10700" s="T581">дерево-PL-INS</ta>
            <ta e="T583" id="Seg_10701" s="T582">и</ta>
            <ta e="T584" id="Seg_10702" s="T583">пойти-EP-IMP.2SG</ta>
            <ta e="T585" id="Seg_10703" s="T584">чтобы</ta>
            <ta e="T586" id="Seg_10704" s="T585">этот-PL</ta>
            <ta e="T587" id="Seg_10705" s="T586">NEG</ta>
            <ta e="T588" id="Seg_10706" s="T587">видеть-PST-3PL</ta>
            <ta e="T589" id="Seg_10707" s="T588">я.ACC</ta>
            <ta e="T590" id="Seg_10708" s="T589">а</ta>
            <ta e="T591" id="Seg_10709" s="T590">этот.[NOM.SG]</ta>
            <ta e="T592" id="Seg_10710" s="T591">сказать-PRS.[3SG]</ta>
            <ta e="T593" id="Seg_10711" s="T592">надо</ta>
            <ta e="T594" id="Seg_10712" s="T593">класть-INF.LAT</ta>
            <ta e="T595" id="Seg_10713" s="T594">так</ta>
            <ta e="T596" id="Seg_10714" s="T595">завязать-INF.LAT</ta>
            <ta e="T597" id="Seg_10715" s="T596">тогда</ta>
            <ta e="T598" id="Seg_10716" s="T597">этот.[NOM.SG]</ta>
            <ta e="T599" id="Seg_10717" s="T598">этот.[NOM.SG]</ta>
            <ta e="T600" id="Seg_10718" s="T599">сказать-IPFVZ.[3SG]</ta>
            <ta e="T601" id="Seg_10719" s="T600">завязать-EP-IMP.2SG</ta>
            <ta e="T602" id="Seg_10720" s="T601">этот.[NOM.SG]</ta>
            <ta e="T603" id="Seg_10721" s="T602">завязать-PST.[3SG]</ta>
            <ta e="T604" id="Seg_10722" s="T603">этот-ACC</ta>
            <ta e="T605" id="Seg_10723" s="T604">а</ta>
            <ta e="T606" id="Seg_10724" s="T605">сейчас</ta>
            <ta e="T607" id="Seg_10725" s="T606">надо</ta>
            <ta e="T609" id="Seg_10726" s="T608">топор.[NOM.SG]</ta>
            <ta e="T610" id="Seg_10727" s="T609">резать-INF.LAT</ta>
            <ta e="T611" id="Seg_10728" s="T610">и</ta>
            <ta e="T612" id="Seg_10729" s="T611">тогда</ta>
            <ta e="T613" id="Seg_10730" s="T612">этот.[NOM.SG]</ta>
            <ta e="T614" id="Seg_10731" s="T613">медведь-ACC</ta>
            <ta e="T615" id="Seg_10732" s="T614">этот.[NOM.SG]</ta>
            <ta e="T616" id="Seg_10733" s="T615">топор-INS</ta>
            <ta e="T617" id="Seg_10734" s="T616">резать-MOM-PST.[3SG]</ta>
            <ta e="T618" id="Seg_10735" s="T617">и</ta>
            <ta e="T619" id="Seg_10736" s="T618">медведь.[NOM.SG]</ta>
            <ta e="T620" id="Seg_10737" s="T619">умереть-RES-PST.[3SG]</ta>
            <ta e="T622" id="Seg_10738" s="T621">медведь.[NOM.SG]</ta>
            <ta e="T623" id="Seg_10739" s="T622">мужчина-COM</ta>
            <ta e="T624" id="Seg_10740" s="T623">PTCL</ta>
            <ta e="T625" id="Seg_10741" s="T624">родственник.[NOM.SG]</ta>
            <ta e="T627" id="Seg_10742" s="T626">стать-DUR-PST-3PL</ta>
            <ta e="T628" id="Seg_10743" s="T627">INCH</ta>
            <ta e="T629" id="Seg_10744" s="T628">сеять-INF.LAT</ta>
            <ta e="T631" id="Seg_10745" s="T630">я.LAT</ta>
            <ta e="T632" id="Seg_10746" s="T631">корешки-NOM/GEN/ACC.3PL</ta>
            <ta e="T633" id="Seg_10747" s="T632">а</ta>
            <ta e="T634" id="Seg_10748" s="T633">ты.DAT</ta>
            <ta e="T635" id="Seg_10749" s="T634">вершки-NOM/GEN/ACC.3PL</ta>
            <ta e="T636" id="Seg_10750" s="T635">тогда</ta>
            <ta e="T637" id="Seg_10751" s="T636">сеять-PST-3PL</ta>
            <ta e="T638" id="Seg_10752" s="T637">этот.[NOM.SG]</ta>
            <ta e="T639" id="Seg_10753" s="T638">расти-PST.[3SG]</ta>
            <ta e="T640" id="Seg_10754" s="T639">мужчина.[NOM.SG]</ta>
            <ta e="T641" id="Seg_10755" s="T640">взять-PST.[3SG]</ta>
            <ta e="T642" id="Seg_10756" s="T641">корешки-NOM/GEN/ACC.3PL</ta>
            <ta e="T643" id="Seg_10757" s="T642">а</ta>
            <ta e="T644" id="Seg_10758" s="T643">медведь.[NOM.SG]</ta>
            <ta e="T645" id="Seg_10759" s="T644">вершки-NOM/GEN/ACC.3PL</ta>
            <ta e="T646" id="Seg_10760" s="T645">тогда</ta>
            <ta e="T647" id="Seg_10761" s="T646">медведь.[NOM.SG]</ta>
            <ta e="T648" id="Seg_10762" s="T647">ну</ta>
            <ta e="T649" id="Seg_10763" s="T648">мужчина.[NOM.SG]</ta>
            <ta e="T650" id="Seg_10764" s="T649">я.ACC</ta>
            <ta e="T651" id="Seg_10765" s="T650">приманивать-MOM-PST.[3SG]</ta>
            <ta e="T652" id="Seg_10766" s="T651">тогда</ta>
            <ta e="T653" id="Seg_10767" s="T652">опять</ta>
            <ta e="T654" id="Seg_10768" s="T653">теплый.[NOM.SG]</ta>
            <ta e="T655" id="Seg_10769" s="T654">стать-RES-PST.[3SG]</ta>
            <ta e="T656" id="Seg_10770" s="T655">мужчина.[NOM.SG]</ta>
            <ta e="T657" id="Seg_10771" s="T656">сказать-IPFVZ.[3SG]</ta>
            <ta e="T658" id="Seg_10772" s="T657">медведь-LAT</ta>
            <ta e="T659" id="Seg_10773" s="T658">HORT</ta>
            <ta e="T660" id="Seg_10774" s="T659">опять</ta>
            <ta e="T661" id="Seg_10775" s="T660">сеять-INF.LAT</ta>
            <ta e="T662" id="Seg_10776" s="T661">ну</ta>
            <ta e="T663" id="Seg_10777" s="T662">медведь.[NOM.SG]</ta>
            <ta e="T664" id="Seg_10778" s="T663">я.LAT</ta>
            <ta e="T665" id="Seg_10779" s="T664">я.LAT</ta>
            <ta e="T666" id="Seg_10780" s="T665">принести-IMP.2SG</ta>
            <ta e="T667" id="Seg_10781" s="T666">корешки-NOM/GEN/ACC.3PL</ta>
            <ta e="T668" id="Seg_10782" s="T667">а</ta>
            <ta e="T669" id="Seg_10783" s="T668">ты.DAT</ta>
            <ta e="T670" id="Seg_10784" s="T669">вершки-NOM/GEN/ACC.3PL</ta>
            <ta e="T671" id="Seg_10785" s="T670">тогда</ta>
            <ta e="T672" id="Seg_10786" s="T671">пшеница.[NOM.SG]</ta>
            <ta e="T674" id="Seg_10787" s="T672">хлеб.[NOM.SG]</ta>
            <ta e="T675" id="Seg_10788" s="T674">пшеница.[NOM.SG]</ta>
            <ta e="T676" id="Seg_10789" s="T675">хлеб.[NOM.SG]</ta>
            <ta e="T677" id="Seg_10790" s="T676">сеять-PST-3PL</ta>
            <ta e="T678" id="Seg_10791" s="T677">этот.[NOM.SG]</ta>
            <ta e="T679" id="Seg_10792" s="T678">расти-PST.[3SG]</ta>
            <ta e="T680" id="Seg_10793" s="T679">хороший</ta>
            <ta e="T681" id="Seg_10794" s="T680">очень</ta>
            <ta e="T682" id="Seg_10795" s="T681">быть-PST.[3SG]</ta>
            <ta e="T683" id="Seg_10796" s="T682">этот.[NOM.SG]</ta>
            <ta e="T684" id="Seg_10797" s="T683">мужчина.[NOM.SG]</ta>
            <ta e="T685" id="Seg_10798" s="T684">взять-PST.[3SG]</ta>
            <ta e="T686" id="Seg_10799" s="T685">вершки-NOM/GEN/ACC.3PL</ta>
            <ta e="T687" id="Seg_10800" s="T686">а</ta>
            <ta e="T688" id="Seg_10801" s="T687">медведь-LAT</ta>
            <ta e="T689" id="Seg_10802" s="T688">корешки-NOM/GEN/ACC.3PL</ta>
            <ta e="T690" id="Seg_10803" s="T689">и</ta>
            <ta e="T691" id="Seg_10804" s="T690">тогда</ta>
            <ta e="T693" id="Seg_10805" s="T692">этот-GEN.PL</ta>
            <ta e="T694" id="Seg_10806" s="T693">PTCL</ta>
            <ta e="T695" id="Seg_10807" s="T694">дружба-NOM/GEN.3SG</ta>
            <ta e="T696" id="Seg_10808" s="T695">умереть-RES-PST.[3SG]</ta>
            <ta e="T698" id="Seg_10809" s="T697">жить-DUR-PST</ta>
            <ta e="T699" id="Seg_10810" s="T698">мужчина.[NOM.SG]</ta>
            <ta e="T700" id="Seg_10811" s="T699">и</ta>
            <ta e="T701" id="Seg_10812" s="T700">женщина.[NOM.SG]</ta>
            <ta e="T702" id="Seg_10813" s="T701">этот-PL-LOC</ta>
            <ta e="T703" id="Seg_10814" s="T702">ребенок-PL-LAT</ta>
            <ta e="T704" id="Seg_10815" s="T703">NEG</ta>
            <ta e="T705" id="Seg_10816" s="T704">быть-PST-3PL</ta>
            <ta e="T706" id="Seg_10817" s="T705">один.[NOM.SG]</ta>
            <ta e="T707" id="Seg_10818" s="T706">снег.[NOM.SG]</ta>
            <ta e="T708" id="Seg_10819" s="T707">PTCL</ta>
            <ta e="T709" id="Seg_10820" s="T708">упасть-MOM-PST.[3SG]</ta>
            <ta e="T710" id="Seg_10821" s="T709">очень</ta>
            <ta e="T711" id="Seg_10822" s="T710">большой.[NOM.SG]</ta>
            <ta e="T712" id="Seg_10823" s="T711">ребенок-PL</ta>
            <ta e="T713" id="Seg_10824" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_10825" s="T713">какой.[NOM.SG]</ta>
            <ta e="T715" id="Seg_10826" s="T714">PTCL</ta>
            <ta e="T719" id="Seg_10827" s="T718">какой.[NOM.SG]</ta>
            <ta e="T720" id="Seg_10828" s="T719">PTCL</ta>
            <ta e="T721" id="Seg_10829" s="T720">играть-DUR-3PL</ta>
            <ta e="T722" id="Seg_10830" s="T721">снеговик-PL</ta>
            <ta e="T723" id="Seg_10831" s="T722">делать-DUR-3PL</ta>
            <ta e="T724" id="Seg_10832" s="T723">весь</ta>
            <ta e="T725" id="Seg_10833" s="T724">делать-DUR-3PL</ta>
            <ta e="T726" id="Seg_10834" s="T725">тогда</ta>
            <ta e="T727" id="Seg_10835" s="T726">делать-PST-3PL</ta>
            <ta e="T728" id="Seg_10836" s="T727">большой.[NOM.SG]</ta>
            <ta e="T729" id="Seg_10837" s="T728">женщина.[NOM.SG]</ta>
            <ta e="T730" id="Seg_10838" s="T729">а</ta>
            <ta e="T731" id="Seg_10839" s="T730">мужчина.[NOM.SG]</ta>
            <ta e="T732" id="Seg_10840" s="T731">всегда</ta>
            <ta e="T733" id="Seg_10841" s="T732">смотреть-PST.[3SG]</ta>
            <ta e="T734" id="Seg_10842" s="T733">смотреть-PST.[3SG]</ta>
            <ta e="T735" id="Seg_10843" s="T734">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T737" id="Seg_10844" s="T736">ты.NOM-COM</ta>
            <ta e="T738" id="Seg_10845" s="T737">наружу</ta>
            <ta e="T739" id="Seg_10846" s="T738">женщина-LAT</ta>
            <ta e="T740" id="Seg_10847" s="T739">сказать-PRS.[3SG]</ta>
            <ta e="T741" id="Seg_10848" s="T740">ну</ta>
            <ta e="T742" id="Seg_10849" s="T741">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T744" id="Seg_10850" s="T743">пойти-PST-3PL</ta>
            <ta e="T745" id="Seg_10851" s="T744">смотреть-PST-3PL</ta>
            <ta e="T746" id="Seg_10852" s="T745">тогда</ta>
            <ta e="T747" id="Seg_10853" s="T746">INCH</ta>
            <ta e="T748" id="Seg_10854" s="T747">делать-INF.LAT</ta>
            <ta e="T749" id="Seg_10855" s="T748">девушка.[NOM.SG]</ta>
            <ta e="T751" id="Seg_10856" s="T749">делать-PST-3PL</ta>
            <ta e="T752" id="Seg_10857" s="T751">голова.[NOM.SG]</ta>
            <ta e="T753" id="Seg_10858" s="T752">делать-PST-3PL</ta>
            <ta e="T754" id="Seg_10859" s="T753">глаз-PL</ta>
            <ta e="T755" id="Seg_10860" s="T754">нос-NOM/GEN.3SG</ta>
            <ta e="T756" id="Seg_10861" s="T755">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T757" id="Seg_10862" s="T756">глаз-NOM/GEN.3SG</ta>
            <ta e="T758" id="Seg_10863" s="T757">тогда</ta>
            <ta e="T759" id="Seg_10864" s="T758">уголь-INS</ta>
            <ta e="T760" id="Seg_10865" s="T759">PTCL</ta>
            <ta e="T761" id="Seg_10866" s="T760">глаз-GEN</ta>
            <ta e="T762" id="Seg_10867" s="T761">край-LAT/LOC.3SG</ta>
            <ta e="T763" id="Seg_10868" s="T762">мазать-PST-3PL</ta>
            <ta e="T764" id="Seg_10869" s="T763">тогда</ta>
            <ta e="T765" id="Seg_10870" s="T764">этот.[NOM.SG]</ta>
            <ta e="T766" id="Seg_10871" s="T765">глаз-3SG-INS</ta>
            <ta e="T767" id="Seg_10872" s="T766">%моргнуть-MOM-PST.[3SG]</ta>
            <ta e="T768" id="Seg_10873" s="T767">тогда</ta>
            <ta e="T769" id="Seg_10874" s="T768">мужчина.[NOM.SG]</ta>
            <ta e="T770" id="Seg_10875" s="T769">идти-DUR.[3SG]</ta>
            <ta e="T772" id="Seg_10876" s="T771">пойти-PST.[3SG]</ta>
            <ta e="T773" id="Seg_10877" s="T772">дом-LAT</ta>
            <ta e="T774" id="Seg_10878" s="T773">а</ta>
            <ta e="T775" id="Seg_10879" s="T774">этот.[NOM.SG]</ta>
            <ta e="T776" id="Seg_10880" s="T775">этот-PL</ta>
            <ta e="T778" id="Seg_10881" s="T777">тоже</ta>
            <ta e="T779" id="Seg_10882" s="T778">этот-COM</ta>
            <ta e="T780" id="Seg_10883" s="T779">пойти-PST-3PL</ta>
            <ta e="T781" id="Seg_10884" s="T780">дом-LAT</ta>
            <ta e="T782" id="Seg_10885" s="T781">PTCL</ta>
            <ta e="T783" id="Seg_10886" s="T782">очень</ta>
            <ta e="T784" id="Seg_10887" s="T783">этот-LAT</ta>
            <ta e="T785" id="Seg_10888" s="T784">хороший</ta>
            <ta e="T786" id="Seg_10889" s="T785">стать-RES-PST.[3SG]</ta>
            <ta e="T787" id="Seg_10890" s="T786">ребенок.[NOM.SG]</ta>
            <ta e="T789" id="Seg_10891" s="T788">весь</ta>
            <ta e="T790" id="Seg_10892" s="T789">взять-PST-3PL</ta>
            <ta e="T791" id="Seg_10893" s="T790">этот-LAT</ta>
            <ta e="T793" id="Seg_10894" s="T792">тогда</ta>
            <ta e="T794" id="Seg_10895" s="T793">этот-LAT</ta>
            <ta e="T795" id="Seg_10896" s="T794">сапог-PL</ta>
            <ta e="T796" id="Seg_10897" s="T795">взять-PST-3PL</ta>
            <ta e="T797" id="Seg_10898" s="T796">красивый-PL</ta>
            <ta e="T798" id="Seg_10899" s="T797">красный-PL</ta>
            <ta e="T799" id="Seg_10900" s="T798">тогда</ta>
            <ta e="T800" id="Seg_10901" s="T799">волосы-LAT</ta>
            <ta e="T801" id="Seg_10902" s="T800">взять-PST-3PL</ta>
            <ta e="T803" id="Seg_10903" s="T802">очень</ta>
            <ta e="T804" id="Seg_10904" s="T803">красивый.[NOM.SG]</ta>
            <ta e="T806" id="Seg_10905" s="T805">тогда</ta>
            <ta e="T807" id="Seg_10906" s="T806">этот.[NOM.SG]</ta>
            <ta e="T808" id="Seg_10907" s="T807">тогда</ta>
            <ta e="T809" id="Seg_10908" s="T808">теплый.[NOM.SG]</ta>
            <ta e="T810" id="Seg_10909" s="T809">стать-RES-PST.[3SG]</ta>
            <ta e="T811" id="Seg_10910" s="T810">а</ta>
            <ta e="T812" id="Seg_10911" s="T811">этот.[NOM.SG]</ta>
            <ta e="T813" id="Seg_10912" s="T812">девушка.[NOM.SG]</ta>
            <ta e="T814" id="Seg_10913" s="T813">сидеть-DUR.[3SG]</ta>
            <ta e="T815" id="Seg_10914" s="T814">вода.[NOM.SG]</ta>
            <ta e="T816" id="Seg_10915" s="T815">течь-DUR.[3SG]</ta>
            <ta e="T817" id="Seg_10916" s="T816">этот.[NOM.SG]</ta>
            <ta e="T818" id="Seg_10917" s="T817">PTCL</ta>
            <ta e="T819" id="Seg_10918" s="T818">бояться-PRS.[3SG]</ta>
            <ta e="T820" id="Seg_10919" s="T819">а</ta>
            <ta e="T821" id="Seg_10920" s="T820">женщина-NOM/GEN.3SG</ta>
            <ta e="T823" id="Seg_10921" s="T822">женщина.[NOM.SG]</ta>
            <ta e="T824" id="Seg_10922" s="T823">сказать-IPFVZ.[3SG]</ta>
            <ta e="T825" id="Seg_10923" s="T824">что.[NOM.SG]</ta>
            <ta e="T826" id="Seg_10924" s="T825">ты.NOM</ta>
            <ta e="T827" id="Seg_10925" s="T826">плакать-DUR-2SG</ta>
            <ta e="T828" id="Seg_10926" s="T827">а</ta>
            <ta e="T829" id="Seg_10927" s="T828">сам-NOM/GEN/ACC.3PL</ta>
            <ta e="T830" id="Seg_10928" s="T829">PTCL</ta>
            <ta e="T832" id="Seg_10929" s="T831">слеза-NOM/GEN/ACC.3SG</ta>
            <ta e="T833" id="Seg_10930" s="T832">течь-DUR-3PL</ta>
            <ta e="T834" id="Seg_10931" s="T833">лицо-LOC</ta>
            <ta e="T835" id="Seg_10932" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_10933" s="T835">тогда</ta>
            <ta e="T838" id="Seg_10934" s="T837">опять</ta>
            <ta e="T839" id="Seg_10935" s="T838">солнце.[NOM.SG]</ta>
            <ta e="T840" id="Seg_10936" s="T839">стать-DUR-PST.[3SG]</ta>
            <ta e="T842" id="Seg_10937" s="T841">трава.[NOM.SG]</ta>
            <ta e="T843" id="Seg_10938" s="T842">расти-DUR-3PL</ta>
            <ta e="T844" id="Seg_10939" s="T843">цветок-PL</ta>
            <ta e="T845" id="Seg_10940" s="T844">тогда</ta>
            <ta e="T846" id="Seg_10941" s="T845">прийти-PST-3PL</ta>
            <ta e="T847" id="Seg_10942" s="T846">дочь-PL</ta>
            <ta e="T848" id="Seg_10943" s="T847">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T849" id="Seg_10944" s="T848">мы.NOM-COM</ta>
            <ta e="T850" id="Seg_10945" s="T849">позвать-PRS-3PL</ta>
            <ta e="T851" id="Seg_10946" s="T850">девушка-NOM/GEN.3SG</ta>
            <ta e="T852" id="Seg_10947" s="T851">женщина.[NOM.SG]</ta>
            <ta e="T855" id="Seg_10948" s="T854">сказать-PRS.[3SG]</ta>
            <ta e="T856" id="Seg_10949" s="T855">пойти-EP-IMP.2SG</ta>
            <ta e="T857" id="Seg_10950" s="T856">что.[NOM.SG]</ta>
            <ta e="T858" id="Seg_10951" s="T857">сидеть-DUR-2SG</ta>
            <ta e="T859" id="Seg_10952" s="T858">нет</ta>
            <ta e="T860" id="Seg_10953" s="T859">я.NOM</ta>
            <ta e="T861" id="Seg_10954" s="T860">NEG.AUX-1SG</ta>
            <ta e="T862" id="Seg_10955" s="T861">пойти-EP-CNG</ta>
            <ta e="T863" id="Seg_10956" s="T862">солнце.[NOM.SG]</ta>
            <ta e="T864" id="Seg_10957" s="T863">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T865" id="Seg_10958" s="T864">PTCL</ta>
            <ta e="T866" id="Seg_10959" s="T865">болеть-MOM-FUT-3SG</ta>
            <ta e="T868" id="Seg_10960" s="T867">солнце.[NOM.SG]</ta>
            <ta e="T869" id="Seg_10961" s="T868">гореть-FUT-3SG</ta>
            <ta e="T870" id="Seg_10962" s="T869">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T871" id="Seg_10963" s="T870">ну</ta>
            <ta e="T872" id="Seg_10964" s="T871">платок.[NOM.SG]</ta>
            <ta e="T874" id="Seg_10965" s="T873">надеть-IMP.2SG.O</ta>
            <ta e="T875" id="Seg_10966" s="T874">тогда</ta>
            <ta e="T876" id="Seg_10967" s="T875">этот.[NOM.SG]</ta>
            <ta e="T877" id="Seg_10968" s="T876">пойти-PST.[3SG]</ta>
            <ta e="T878" id="Seg_10969" s="T877">весь</ta>
            <ta e="T879" id="Seg_10970" s="T878">дочь-PL</ta>
            <ta e="T880" id="Seg_10971" s="T879">играть-DUR-3PL</ta>
            <ta e="T881" id="Seg_10972" s="T880">цветок-PL</ta>
            <ta e="T883" id="Seg_10973" s="T882">рвать-DUR-3PL</ta>
            <ta e="T884" id="Seg_10974" s="T883">а</ta>
            <ta e="T885" id="Seg_10975" s="T884">этот.[NOM.SG]</ta>
            <ta e="T886" id="Seg_10976" s="T885">сидеть-DUR.[3SG]</ta>
            <ta e="T890" id="Seg_10977" s="T889">тогда</ta>
            <ta e="T891" id="Seg_10978" s="T890">этот-PL</ta>
            <ta e="T892" id="Seg_10979" s="T891">класть-PST-3PL</ta>
            <ta e="T893" id="Seg_10980" s="T892">огонь.[NOM.SG]</ta>
            <ta e="T894" id="Seg_10981" s="T893">очень</ta>
            <ta e="T895" id="Seg_10982" s="T894">большой.[NOM.SG]</ta>
            <ta e="T896" id="Seg_10983" s="T895">и</ta>
            <ta e="T897" id="Seg_10984" s="T896">INCH</ta>
            <ta e="T898" id="Seg_10985" s="T897">бежать-INF.LAT</ta>
            <ta e="T899" id="Seg_10986" s="T898">огонь-LAT</ta>
            <ta e="T900" id="Seg_10987" s="T899">сказать-PRS-3PL</ta>
            <ta e="T901" id="Seg_10988" s="T900">что.[NOM.SG]</ta>
            <ta e="T902" id="Seg_10989" s="T901">ты.NOM</ta>
            <ta e="T903" id="Seg_10990" s="T902">NEG</ta>
            <ta e="T904" id="Seg_10991" s="T903">прыгнуть-PRS-2SG</ta>
            <ta e="T905" id="Seg_10992" s="T904">а</ta>
            <ta e="T906" id="Seg_10993" s="T905">этот.[NOM.SG]</ta>
            <ta e="T907" id="Seg_10994" s="T906">бояться-PRS.[3SG]</ta>
            <ta e="T908" id="Seg_10995" s="T907">а</ta>
            <ta e="T909" id="Seg_10996" s="T908">тогда</ta>
            <ta e="T910" id="Seg_10997" s="T909">как</ta>
            <ta e="T911" id="Seg_10998" s="T910">этот-ACC</ta>
            <ta e="T912" id="Seg_10999" s="T911">насмехаться-DUR-3PL</ta>
            <ta e="T913" id="Seg_11000" s="T912">а</ta>
            <ta e="T914" id="Seg_11001" s="T913">этот.[NOM.SG]</ta>
            <ta e="T915" id="Seg_11002" s="T914">встать-PST.[3SG]</ta>
            <ta e="T917" id="Seg_11003" s="T916">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T918" id="Seg_11004" s="T917">и</ta>
            <ta e="T919" id="Seg_11005" s="T918">PTCL</ta>
            <ta e="T920" id="Seg_11006" s="T919">огонь-LOC</ta>
            <ta e="T1121" id="Seg_11007" s="T920">пойти-CVB</ta>
            <ta e="T921" id="Seg_11008" s="T1121">исчезнуть-PST.[3SG]</ta>
            <ta e="T922" id="Seg_11009" s="T921">вверх</ta>
            <ta e="T923" id="Seg_11010" s="T922">PTCL</ta>
            <ta e="T926" id="Seg_11011" s="T925">раз.[NOM.SG]</ta>
            <ta e="T927" id="Seg_11012" s="T926">теленок-NOM/GEN/ACC.1SG</ta>
            <ta e="T928" id="Seg_11013" s="T927">я.NOM</ta>
            <ta e="T929" id="Seg_11014" s="T928">очень</ta>
            <ta e="T930" id="Seg_11015" s="T929">красивый.[NOM.SG]</ta>
            <ta e="T931" id="Seg_11016" s="T930">белый.[NOM.SG]</ta>
            <ta e="T932" id="Seg_11017" s="T931">теленок.[NOM.SG]</ta>
            <ta e="T933" id="Seg_11018" s="T932">быть-PST.[3SG]</ta>
            <ta e="T934" id="Seg_11019" s="T933">тогда</ta>
            <ta e="T935" id="Seg_11020" s="T934">идти-PST.[3SG]</ta>
            <ta e="T936" id="Seg_11021" s="T935">идти-PST.[3SG]</ta>
            <ta e="T937" id="Seg_11022" s="T936">я.NOM</ta>
            <ta e="T938" id="Seg_11023" s="T937">вечер-LOC.ADV</ta>
            <ta e="T939" id="Seg_11024" s="T938">пить-TR-INF.LAT</ta>
            <ta e="T941" id="Seg_11025" s="T940">молоко-INS</ta>
            <ta e="T942" id="Seg_11026" s="T941">кричать-PRS-1SG</ta>
            <ta e="T943" id="Seg_11027" s="T942">кричать-PRS-1SG</ta>
            <ta e="T944" id="Seg_11028" s="T943">смотреть-FRQ-PST-1SG</ta>
            <ta e="T945" id="Seg_11029" s="T944">смотреть-FRQ-PST-1SG</ta>
            <ta e="T946" id="Seg_11030" s="T945">где=INDEF</ta>
            <ta e="T947" id="Seg_11031" s="T946">NEG.EX.[3SG]</ta>
            <ta e="T948" id="Seg_11032" s="T947">ну</ta>
            <ta e="T949" id="Seg_11033" s="T948">куда.идти-PST.[3SG]</ta>
            <ta e="T951" id="Seg_11034" s="T950">теленок.[NOM.SG]</ta>
            <ta e="T952" id="Seg_11035" s="T951">NEG</ta>
            <ta e="T953" id="Seg_11036" s="T952">знать-PRS-1SG</ta>
            <ta e="T954" id="Seg_11037" s="T953">утро-LOC.ADV</ta>
            <ta e="T955" id="Seg_11038" s="T954">встать-PST-1SG</ta>
            <ta e="T956" id="Seg_11039" s="T955">везде</ta>
            <ta e="T957" id="Seg_11040" s="T956">идти-PST-1SG</ta>
            <ta e="T958" id="Seg_11041" s="T957">река-LOC</ta>
            <ta e="T959" id="Seg_11042" s="T958">идти-PST-1SG</ta>
            <ta e="T960" id="Seg_11043" s="T959">думать-PST-1SG</ta>
            <ta e="T961" id="Seg_11044" s="T960">этот.[NOM.SG]</ta>
            <ta e="T962" id="Seg_11045" s="T961">упасть-MOM-PST.[3SG]</ta>
            <ta e="T963" id="Seg_11046" s="T962">вода-LAT</ta>
            <ta e="T964" id="Seg_11047" s="T963">там</ta>
            <ta e="T965" id="Seg_11048" s="T964">NEG.EX.[3SG]</ta>
            <ta e="T967" id="Seg_11049" s="T966">думать-PST-1SG</ta>
            <ta e="T968" id="Seg_11050" s="T967">волк.[NOM.SG]</ta>
            <ta e="T969" id="Seg_11051" s="T968">съесть-MOM-PST.[3SG]</ta>
            <ta e="T970" id="Seg_11052" s="T969">нет</ta>
            <ta e="T971" id="Seg_11053" s="T970">NEG</ta>
            <ta e="T972" id="Seg_11054" s="T971">волк.[NOM.SG]</ta>
            <ta e="T974" id="Seg_11055" s="T973">NEG</ta>
            <ta e="T975" id="Seg_11056" s="T974">съесть-MOM-PST.[3SG]</ta>
            <ta e="T976" id="Seg_11057" s="T975">тогда</ta>
            <ta e="T977" id="Seg_11058" s="T976">здесь</ta>
            <ta e="T978" id="Seg_11059" s="T977">люди.[NOM.SG]</ta>
            <ta e="T979" id="Seg_11060" s="T978">трава.[NOM.SG]</ta>
            <ta e="T980" id="Seg_11061" s="T979">резать-PST-3PL</ta>
            <ta e="T981" id="Seg_11062" s="T980">этот-PL</ta>
            <ta e="T983" id="Seg_11063" s="T982">взять-PST-3PL</ta>
            <ta e="T984" id="Seg_11064" s="T983">нога-ABL.3SG</ta>
            <ta e="T986" id="Seg_11065" s="T985">завязать-PST-3PL</ta>
            <ta e="T987" id="Seg_11066" s="T986">нести-PST-3PL</ta>
            <ta e="T988" id="Seg_11067" s="T987">дом-LAT/LOC.3SG</ta>
            <ta e="T989" id="Seg_11068" s="T988">два.[NOM.SG]</ta>
            <ta e="T990" id="Seg_11069" s="T989">нога-3SG-INS</ta>
            <ta e="T991" id="Seg_11070" s="T990">волк.[NOM.SG]</ta>
            <ta e="T992" id="Seg_11071" s="T991">быть-PST.[3SG]</ta>
            <ta e="T993" id="Seg_11072" s="T992">я.NOM</ta>
            <ta e="T995" id="Seg_11073" s="T994">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T996" id="Seg_11074" s="T995">прийти-PST.[3SG]</ta>
            <ta e="T997" id="Seg_11075" s="T996">Кан_Оклер-LAT</ta>
            <ta e="T998" id="Seg_11076" s="T997">и</ta>
            <ta e="T999" id="Seg_11077" s="T998">сказать-PRS.[3SG]</ta>
            <ta e="T1001" id="Seg_11078" s="T1000">два.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_11079" s="T1001">корова.[NOM.SG]</ta>
            <ta e="T1003" id="Seg_11080" s="T1002">быть-PST.[3SG]</ta>
            <ta e="T1004" id="Seg_11081" s="T1003">люди-LOC</ta>
            <ta e="T1005" id="Seg_11082" s="T1004">очень</ta>
            <ta e="T1006" id="Seg_11083" s="T1005">люди.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_11084" s="T1007">люди.[NOM.SG]</ta>
            <ta e="T1009" id="Seg_11085" s="T1008">очень</ta>
            <ta e="T1011" id="Seg_11086" s="T1010">жир-NOM/GEN/ACC.3SG</ta>
            <ta e="T1012" id="Seg_11087" s="T1011">много</ta>
            <ta e="T1013" id="Seg_11088" s="T1012">а</ta>
            <ta e="T1014" id="Seg_11089" s="T1013">мы.LAT</ta>
            <ta e="T1015" id="Seg_11090" s="T1014">жить-FUT-3SG</ta>
            <ta e="T1016" id="Seg_11091" s="T1015">PTCL</ta>
            <ta e="T1017" id="Seg_11092" s="T1016">что.[NOM.SG]</ta>
            <ta e="T1018" id="Seg_11093" s="T1017">худой.[NOM.SG]</ta>
            <ta e="T1019" id="Seg_11094" s="T1018">стать-FUT-3PL</ta>
            <ta e="T1020" id="Seg_11095" s="T1019">трава.[NOM.SG]</ta>
            <ta e="T1021" id="Seg_11096" s="T1020">NEG</ta>
            <ta e="T1022" id="Seg_11097" s="T1021">съесть-PRS.[3SG]</ta>
            <ta e="T1023" id="Seg_11098" s="T1022">вода.[NOM.SG]</ta>
            <ta e="T1024" id="Seg_11099" s="T1023">NEG</ta>
            <ta e="T1025" id="Seg_11100" s="T1024">пить-PRS.[3SG]</ta>
            <ta e="T1026" id="Seg_11101" s="T1025">мало</ta>
            <ta e="T1027" id="Seg_11102" s="T1026">съесть-DUR.[3SG]</ta>
            <ta e="T1028" id="Seg_11103" s="T1027">другой.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_11104" s="T1028">корова.[NOM.SG]</ta>
            <ta e="T1030" id="Seg_11105" s="T1029">прийти-FUT-3PL</ta>
            <ta e="T1031" id="Seg_11106" s="T1030">PTCL</ta>
            <ta e="T1032" id="Seg_11107" s="T1031">трава.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_11108" s="T1032">съесть-MOM-FUT-3PL</ta>
            <ta e="T1034" id="Seg_11109" s="T1033">а</ta>
            <ta e="T1035" id="Seg_11110" s="T1034">этот.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_11111" s="T1035">NEG</ta>
            <ta e="T1037" id="Seg_11112" s="T1036">съесть-PRS.[3SG]</ta>
            <ta e="T1039" id="Seg_11113" s="T1038">NEG</ta>
            <ta e="T1040" id="Seg_11114" s="T1039">знать-1SG</ta>
            <ta e="T1041" id="Seg_11115" s="T1040">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1042" id="Seg_11116" s="T1041">кораль.[NOM.SG]</ta>
            <ta e="T1043" id="Seg_11117" s="T1042">такой.[NOM.SG]</ta>
            <ta e="T1044" id="Seg_11118" s="T1043">или</ta>
            <ta e="T1045" id="Seg_11119" s="T1044">что.[NOM.SG]</ta>
            <ta e="T1046" id="Seg_11120" s="T1045">такой.[NOM.SG]</ta>
            <ta e="T1047" id="Seg_11121" s="T1046">мы.NOM</ta>
            <ta e="T1048" id="Seg_11122" s="T1047">взять-PST-1PL</ta>
            <ta e="T1049" id="Seg_11123" s="T1048">этот-GEN</ta>
            <ta e="T1051" id="Seg_11124" s="T1050">сестра-LAT/LOC.3SG</ta>
            <ta e="T1052" id="Seg_11125" s="T1051">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T1053" id="Seg_11126" s="T1052">этот.[NOM.SG]</ta>
            <ta e="T1054" id="Seg_11127" s="T1053">ведь</ta>
            <ta e="T1055" id="Seg_11128" s="T1054">что.[NOM.SG]=INDEF</ta>
            <ta e="T1056" id="Seg_11129" s="T1055">NEG</ta>
            <ta e="T1057" id="Seg_11130" s="T1056">знать-3SG.O</ta>
            <ta e="T1058" id="Seg_11131" s="T1057">тоже</ta>
            <ta e="T1059" id="Seg_11132" s="T1058">корова.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_11133" s="T1059">так</ta>
            <ta e="T1061" id="Seg_11134" s="T1060">стать-RES-PST.[3SG]</ta>
            <ta e="T1062" id="Seg_11135" s="T1061">PTCL</ta>
            <ta e="T1063" id="Seg_11136" s="T1062">худой.[NOM.SG]</ta>
            <ta e="T1064" id="Seg_11137" s="T1063">стать-PST.[3SG]</ta>
            <ta e="T1065" id="Seg_11138" s="T1064">а</ta>
            <ta e="T1066" id="Seg_11139" s="T1065">%%</ta>
            <ta e="T1067" id="Seg_11140" s="T1066">жир-NOM/GEN/ACC.3SG</ta>
            <ta e="T1068" id="Seg_11141" s="T1067">много</ta>
            <ta e="T1070" id="Seg_11142" s="T1069">быть-PST.[3SG]</ta>
            <ta e="T1071" id="Seg_11143" s="T1070">жить-PST.[3SG]</ta>
            <ta e="T1072" id="Seg_11144" s="T1071">мы.LAT</ta>
            <ta e="T1074" id="Seg_11145" s="T1073">худой.[NOM.SG]</ta>
            <ta e="T1075" id="Seg_11146" s="T1074">стать-RES-PST.[3SG]</ta>
            <ta e="T1077" id="Seg_11147" s="T1076">этот.[NOM.SG]</ta>
            <ta e="T1078" id="Seg_11148" s="T1077">один.[NOM.SG]</ta>
            <ta e="T1079" id="Seg_11149" s="T1078">дочь-COM</ta>
            <ta e="T1080" id="Seg_11150" s="T1079">жить-DUR-PST</ta>
            <ta e="T1081" id="Seg_11151" s="T1080">тогда</ta>
            <ta e="T1084" id="Seg_11152" s="T1083">быть-PST.[3SG]</ta>
            <ta e="T1085" id="Seg_11153" s="T1084">может.быть</ta>
            <ta e="T1086" id="Seg_11154" s="T1085">этот.[NOM.SG]</ta>
            <ta e="T1088" id="Seg_11155" s="T1087">что=INDEF</ta>
            <ta e="T1090" id="Seg_11156" s="T1089">копать-PST.[3SG]</ta>
            <ta e="T1091" id="Seg_11157" s="T1090">и</ta>
            <ta e="T1092" id="Seg_11158" s="T1091">сейчас</ta>
            <ta e="T1094" id="Seg_11159" s="T1093">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T1095" id="Seg_11160" s="T1094">PTCL</ta>
            <ta e="T1096" id="Seg_11161" s="T1095">NEG</ta>
            <ta e="T1097" id="Seg_11162" s="T1096">стоять-PRS-3PL</ta>
            <ta e="T1099" id="Seg_11163" s="T1098">вы.NOM</ta>
            <ta e="T1101" id="Seg_11164" s="T1100">PTCL</ta>
            <ta e="T1102" id="Seg_11165" s="T1101">завтра</ta>
            <ta e="T1105" id="Seg_11166" s="T1104">отверстие.[NOM.SG]</ta>
            <ta e="T1106" id="Seg_11167" s="T1105">завтра</ta>
            <ta e="T1122" id="Seg_11168" s="T1106">пойти-CVB</ta>
            <ta e="T1107" id="Seg_11169" s="T1122">исчезнуть-FUT-1PL</ta>
            <ta e="T1123" id="Seg_11170" s="T1107">пойти-CVB</ta>
            <ta e="T1108" id="Seg_11171" s="T1123">исчезнуть-FUT-2PL</ta>
            <ta e="T1109" id="Seg_11172" s="T1108">HORT.PL</ta>
            <ta e="T1110" id="Seg_11173" s="T1109">обнимать-FUT-1PL</ta>
            <ta e="T1111" id="Seg_11174" s="T1110">и</ta>
            <ta e="T1112" id="Seg_11175" s="T1111">целовать-FUT-1PL</ta>
            <ta e="T1113" id="Seg_11176" s="T1112">тогда</ta>
            <ta e="T1114" id="Seg_11177" s="T1113">пойти-IMP.2PL</ta>
            <ta e="T1115" id="Seg_11178" s="T1114">бог-COM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T1" id="Seg_11179" s="T0">v-v&gt;v-v:pn</ta>
            <ta e="T2" id="Seg_11180" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_11181" s="T2">v-v&gt;v-v:pn</ta>
            <ta e="T4" id="Seg_11182" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_11183" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_11184" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_11185" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_11186" s="T7">v-v&gt;v-v:pn</ta>
            <ta e="T9" id="Seg_11187" s="T8">dempro-n:case</ta>
            <ta e="T10" id="Seg_11188" s="T9">n-n:case</ta>
            <ta e="T12" id="Seg_11189" s="T11">que</ta>
            <ta e="T13" id="Seg_11190" s="T12">adj</ta>
            <ta e="T16" id="Seg_11191" s="T15">n-n:case.poss</ta>
            <ta e="T18" id="Seg_11192" s="T17">dempro-n:case</ta>
            <ta e="T19" id="Seg_11193" s="T18">n-n:case</ta>
            <ta e="T21" id="Seg_11194" s="T20">que</ta>
            <ta e="T22" id="Seg_11195" s="T21">pers</ta>
            <ta e="T23" id="Seg_11196" s="T22">quant</ta>
            <ta e="T24" id="Seg_11197" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_11198" s="T24">n-n:num</ta>
            <ta e="T26" id="Seg_11199" s="T25">pers</ta>
            <ta e="T27" id="Seg_11200" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_11201" s="T27">adj-n:case</ta>
            <ta e="T29" id="Seg_11202" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_11203" s="T29">propr-n:case</ta>
            <ta e="T31" id="Seg_11204" s="T30">adv</ta>
            <ta e="T32" id="Seg_11205" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_11206" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_11207" s="T33">conj</ta>
            <ta e="T35" id="Seg_11208" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_11209" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_11210" s="T36">adv</ta>
            <ta e="T38" id="Seg_11211" s="T37">quant</ta>
            <ta e="T39" id="Seg_11212" s="T38">n-n:num</ta>
            <ta e="T40" id="Seg_11213" s="T39">adj-n:num</ta>
            <ta e="T41" id="Seg_11214" s="T40">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_11215" s="T41">pers</ta>
            <ta e="T43" id="Seg_11216" s="T42">n-n:case.poss</ta>
            <ta e="T44" id="Seg_11217" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_11218" s="T44">v-v:pn</ta>
            <ta e="T46" id="Seg_11219" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_11220" s="T46">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_11221" s="T47">conj</ta>
            <ta e="T49" id="Seg_11222" s="T48">adv</ta>
            <ta e="T50" id="Seg_11223" s="T49">que</ta>
            <ta e="T51" id="Seg_11224" s="T50">n-n:num</ta>
            <ta e="T52" id="Seg_11225" s="T51">v-v:tense-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_11226" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_11227" s="T53">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_11228" s="T54">conj</ta>
            <ta e="T56" id="Seg_11229" s="T55">quant</ta>
            <ta e="T57" id="Seg_11230" s="T56">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_11231" s="T57">adv</ta>
            <ta e="T59" id="Seg_11232" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_11233" s="T59">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_11234" s="T61">adv</ta>
            <ta e="T64" id="Seg_11235" s="T63">que-n:case=ptcl</ta>
            <ta e="T65" id="Seg_11236" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_11237" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_11238" s="T66">que-n:case=ptcl</ta>
            <ta e="T68" id="Seg_11239" s="T67">ptcl</ta>
            <ta e="T70" id="Seg_11240" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_11241" s="T70">quant</ta>
            <ta e="T73" id="Seg_11242" s="T72">n-n:case.poss</ta>
            <ta e="T1117" id="Seg_11243" s="T73">v-v:n-fin</ta>
            <ta e="T74" id="Seg_11244" s="T1117">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_11245" s="T74">adv</ta>
            <ta e="T76" id="Seg_11246" s="T75">pers</ta>
            <ta e="T77" id="Seg_11247" s="T76">adv-n:case.poss</ta>
            <ta e="T78" id="Seg_11248" s="T77">v-v&gt;v-v:pn</ta>
            <ta e="T79" id="Seg_11249" s="T78">que-n:case=ptcl</ta>
            <ta e="T80" id="Seg_11250" s="T79">v-v:pn</ta>
            <ta e="T81" id="Seg_11251" s="T80">quant</ta>
            <ta e="T82" id="Seg_11252" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_11253" s="T82">pers</ta>
            <ta e="T84" id="Seg_11254" s="T83">n-n:num-n:case.poss</ta>
            <ta e="T86" id="Seg_11255" s="T85">adv</ta>
            <ta e="T87" id="Seg_11256" s="T86">quant</ta>
            <ta e="T88" id="Seg_11257" s="T87">n-n:num-n:case.poss</ta>
            <ta e="T89" id="Seg_11258" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_11259" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_11260" s="T90">quant</ta>
            <ta e="T92" id="Seg_11261" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_11262" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_11263" s="T94">pers</ta>
            <ta e="T96" id="Seg_11264" s="T95">adv</ta>
            <ta e="T97" id="Seg_11265" s="T96">adj-n:case</ta>
            <ta e="T98" id="Seg_11266" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_11267" s="T98">conj</ta>
            <ta e="T100" id="Seg_11268" s="T99">que</ta>
            <ta e="T101" id="Seg_11269" s="T100">dempro-n:case</ta>
            <ta e="T102" id="Seg_11270" s="T101">adj-n:case</ta>
            <ta e="T1118" id="Seg_11271" s="T102">v-v:n-fin</ta>
            <ta e="T103" id="Seg_11272" s="T1118">v-v:tense-v:pn</ta>
            <ta e="T1119" id="Seg_11273" s="T103">v-v:n-fin</ta>
            <ta e="T104" id="Seg_11274" s="T1119">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_11275" s="T104">adj-n:case</ta>
            <ta e="T106" id="Seg_11276" s="T105">n-n:num-n:case</ta>
            <ta e="T107" id="Seg_11277" s="T106">conj</ta>
            <ta e="T108" id="Seg_11278" s="T107">que=ptcl</ta>
            <ta e="T109" id="Seg_11279" s="T108">v-v.pn</ta>
            <ta e="T111" id="Seg_11280" s="T110">adv</ta>
            <ta e="T112" id="Seg_11281" s="T111">dempro-n:case</ta>
            <ta e="T113" id="Seg_11282" s="T112">que=ptcl</ta>
            <ta e="T114" id="Seg_11283" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_11284" s="T114">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_11285" s="T117">pers</ta>
            <ta e="T119" id="Seg_11286" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_11287" s="T119">n-n:case</ta>
            <ta e="T122" id="Seg_11288" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_11289" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_11290" s="T123">pers</ta>
            <ta e="T125" id="Seg_11291" s="T124">v-v&gt;v-v:pn</ta>
            <ta e="T126" id="Seg_11292" s="T125">adv</ta>
            <ta e="T127" id="Seg_11293" s="T126">adj-n:case</ta>
            <ta e="T128" id="Seg_11294" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_11295" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_11296" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_11297" s="T130">n-n:num</ta>
            <ta e="T132" id="Seg_11298" s="T131">v-v&gt;v-v:pn</ta>
            <ta e="T133" id="Seg_11299" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_11300" s="T133">adv</ta>
            <ta e="T135" id="Seg_11301" s="T134">pers</ta>
            <ta e="T136" id="Seg_11302" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_11303" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_11304" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_11305" s="T138">dempro-n:case</ta>
            <ta e="T141" id="Seg_11306" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_11307" s="T141">pers</ta>
            <ta e="T144" id="Seg_11308" s="T143">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_11309" s="T145">adv</ta>
            <ta e="T147" id="Seg_11310" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_11311" s="T147">n-n:case.poss</ta>
            <ta e="T149" id="Seg_11312" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_11313" s="T149">adv</ta>
            <ta e="T152" id="Seg_11314" s="T151">dempro-n:case</ta>
            <ta e="T153" id="Seg_11315" s="T152">n-n:case.poss</ta>
            <ta e="T154" id="Seg_11316" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_11317" s="T154">dempro-n:case</ta>
            <ta e="T156" id="Seg_11318" s="T155">n-n:case.poss</ta>
            <ta e="T157" id="Seg_11319" s="T156">adv</ta>
            <ta e="T158" id="Seg_11320" s="T157">dempro-n:case</ta>
            <ta e="T159" id="Seg_11321" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_11322" s="T159">v-v&gt;v-v:pn</ta>
            <ta e="T161" id="Seg_11323" s="T160">pers</ta>
            <ta e="T162" id="Seg_11324" s="T161">pers</ta>
            <ta e="T163" id="Seg_11325" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_11326" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_11327" s="T164">adv</ta>
            <ta e="T166" id="Seg_11328" s="T165">conj</ta>
            <ta e="T167" id="Seg_11329" s="T166">pers</ta>
            <ta e="T168" id="Seg_11330" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_11331" s="T168">pers</ta>
            <ta e="T170" id="Seg_11332" s="T169">n-n:case.poss</ta>
            <ta e="T171" id="Seg_11333" s="T170">n-n:case.poss</ta>
            <ta e="T172" id="Seg_11334" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_11335" s="T172">pers</ta>
            <ta e="T174" id="Seg_11336" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_11337" s="T174">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_11338" s="T176">que-n:case</ta>
            <ta e="T178" id="Seg_11339" s="T177">dempro-n:case</ta>
            <ta e="T179" id="Seg_11340" s="T178">pers</ta>
            <ta e="T180" id="Seg_11341" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_11342" s="T180">adv</ta>
            <ta e="T182" id="Seg_11343" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_11344" s="T182">adv</ta>
            <ta e="T184" id="Seg_11345" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_11346" s="T184">dempro-n:num</ta>
            <ta e="T186" id="Seg_11347" s="T185">refl-n:case.poss</ta>
            <ta e="T187" id="Seg_11348" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_11349" s="T187">n-n:case</ta>
            <ta e="T190" id="Seg_11350" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_11351" s="T190">conj</ta>
            <ta e="T192" id="Seg_11352" s="T191">pers</ta>
            <ta e="T193" id="Seg_11353" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_11354" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_11355" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_11356" s="T195">conj</ta>
            <ta e="T198" id="Seg_11357" s="T197">conj</ta>
            <ta e="T199" id="Seg_11358" s="T198">dempro-n:case</ta>
            <ta e="T200" id="Seg_11359" s="T199">dempro-n:case</ta>
            <ta e="T201" id="Seg_11360" s="T200">adv</ta>
            <ta e="T202" id="Seg_11361" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_11362" s="T202">ptcl</ta>
            <ta e="T205" id="Seg_11363" s="T204">adv</ta>
            <ta e="T206" id="Seg_11364" s="T205">pers</ta>
            <ta e="T207" id="Seg_11365" s="T206">%%</ta>
            <ta e="T208" id="Seg_11366" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_11367" s="T208">propr-n:case</ta>
            <ta e="T210" id="Seg_11368" s="T209">dempro-n:case</ta>
            <ta e="T211" id="Seg_11369" s="T210">v-v&gt;v-v:pn</ta>
            <ta e="T212" id="Seg_11370" s="T211">propr-n:case</ta>
            <ta e="T213" id="Seg_11371" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_11372" s="T213">n-n:num</ta>
            <ta e="T215" id="Seg_11373" s="T214">pers</ta>
            <ta e="T216" id="Seg_11374" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_11375" s="T216">num-n:case</ta>
            <ta e="T218" id="Seg_11376" s="T217">n-n:num</ta>
            <ta e="T221" id="Seg_11377" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_11378" s="T221">num-n:case</ta>
            <ta e="T224" id="Seg_11379" s="T223">num-n:case</ta>
            <ta e="T225" id="Seg_11380" s="T224">n-n:num</ta>
            <ta e="T226" id="Seg_11381" s="T225">num-n:case</ta>
            <ta e="T227" id="Seg_11382" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_11383" s="T227">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_11384" s="T228">dempro-n:case</ta>
            <ta e="T231" id="Seg_11385" s="T230">propr-n:case</ta>
            <ta e="T232" id="Seg_11386" s="T231">v-v&gt;v-v:pn</ta>
            <ta e="T233" id="Seg_11387" s="T232">v-v:mood-v:pn</ta>
            <ta e="T234" id="Seg_11388" s="T233">pers</ta>
            <ta e="T235" id="Seg_11389" s="T234">conj</ta>
            <ta e="T236" id="Seg_11390" s="T235">pers</ta>
            <ta e="T237" id="Seg_11391" s="T236">v-v&gt;v-v:pn</ta>
            <ta e="T238" id="Seg_11392" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_11393" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_11394" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_11395" s="T240">adv</ta>
            <ta e="T242" id="Seg_11396" s="T241">adv</ta>
            <ta e="T243" id="Seg_11397" s="T242">que-n:case</ta>
            <ta e="T244" id="Seg_11398" s="T243">adv-v:n.fin</ta>
            <ta e="T245" id="Seg_11399" s="T244">v-v:mood.pn</ta>
            <ta e="T247" id="Seg_11400" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_11401" s="T247">n-n&gt;adj-n:case</ta>
            <ta e="T249" id="Seg_11402" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_11403" s="T249">conj</ta>
            <ta e="T251" id="Seg_11404" s="T250">adv</ta>
            <ta e="T252" id="Seg_11405" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_11406" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_11407" s="T253">pers</ta>
            <ta e="T255" id="Seg_11408" s="T254">v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_11409" s="T255">n-n&gt;adj-n:case</ta>
            <ta e="T258" id="Seg_11410" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_11411" s="T258">v-v:n.fin</ta>
            <ta e="T260" id="Seg_11412" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_11413" s="T260">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_11414" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11415" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_11416" s="T263">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_11417" s="T264">adv</ta>
            <ta e="T266" id="Seg_11418" s="T265">adj</ta>
            <ta e="T267" id="Seg_11419" s="T266">v-v:n.fin</ta>
            <ta e="T268" id="Seg_11420" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_11421" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_11422" s="T269">adv</ta>
            <ta e="T271" id="Seg_11423" s="T270">n-n:case-n:case.poss</ta>
            <ta e="T272" id="Seg_11424" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_11425" s="T272">pers</ta>
            <ta e="T274" id="Seg_11426" s="T273">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_11427" s="T275">num-num&gt;num</ta>
            <ta e="T277" id="Seg_11428" s="T276">v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_11429" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_11430" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_11431" s="T280">num-n:case</ta>
            <ta e="T282" id="Seg_11432" s="T281">n-n:case</ta>
            <ta e="T287" id="Seg_11433" s="T286">num-n:case</ta>
            <ta e="T288" id="Seg_11434" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_11435" s="T288">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_11436" s="T290">v-v:mood.pn</ta>
            <ta e="T292" id="Seg_11437" s="T291">dempro-n:case</ta>
            <ta e="T293" id="Seg_11438" s="T292">n-n:case.poss</ta>
            <ta e="T294" id="Seg_11439" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_11440" s="T294">dempro-n:case</ta>
            <ta e="T297" id="Seg_11441" s="T296">dempro-n:case</ta>
            <ta e="T298" id="Seg_11442" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_11443" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_11444" s="T299">conj</ta>
            <ta e="T301" id="Seg_11445" s="T300">ptcl</ta>
            <ta e="T304" id="Seg_11446" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_11447" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_11448" s="T305">v-v:mood.pn</ta>
            <ta e="T309" id="Seg_11449" s="T308">dempro-n:case</ta>
            <ta e="T310" id="Seg_11450" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_11451" s="T310">conj</ta>
            <ta e="T312" id="Seg_11452" s="T311">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_11453" s="T312">dempro-n:case</ta>
            <ta e="T314" id="Seg_11454" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_11455" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_11456" s="T315">dempro-n:case</ta>
            <ta e="T317" id="Seg_11457" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_11458" s="T317">dempro-n:case</ta>
            <ta e="T319" id="Seg_11459" s="T318">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_11460" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_11461" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_11462" s="T321">pers</ta>
            <ta e="T323" id="Seg_11463" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_11464" s="T323">v-v:tense-v:pn</ta>
            <ta e="T325" id="Seg_11465" s="T324">conj</ta>
            <ta e="T326" id="Seg_11466" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_11467" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_11468" s="T327">v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_11469" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_11470" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_11471" s="T331">refl-n:case.poss</ta>
            <ta e="T333" id="Seg_11472" s="T332">que-n:case=ptcl</ta>
            <ta e="T334" id="Seg_11473" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_11474" s="T334">v-v:tense-v:pn</ta>
            <ta e="T336" id="Seg_11475" s="T335">dempro-n:case</ta>
            <ta e="T337" id="Seg_11476" s="T336">n-n:case</ta>
            <ta e="T339" id="Seg_11477" s="T338">conj</ta>
            <ta e="T340" id="Seg_11478" s="T339">n-n:case.poss</ta>
            <ta e="T341" id="Seg_11479" s="T340">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T342" id="Seg_11480" s="T341">conj</ta>
            <ta e="T343" id="Seg_11481" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_11482" s="T343">%%</ta>
            <ta e="T345" id="Seg_11483" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_11484" s="T345">pers</ta>
            <ta e="T347" id="Seg_11485" s="T346">v-v:tense-v:pn</ta>
            <ta e="T349" id="Seg_11486" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_11487" s="T349">adv</ta>
            <ta e="T351" id="Seg_11488" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_11489" s="T351">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_11490" s="T353">adv-n:case</ta>
            <ta e="T355" id="Seg_11491" s="T354">v-v:n.fin</ta>
            <ta e="T356" id="Seg_11492" s="T355">v-v:tense-v:pn</ta>
            <ta e="T357" id="Seg_11493" s="T356">n</ta>
            <ta e="T358" id="Seg_11494" s="T357">refl-n:case.poss</ta>
            <ta e="T359" id="Seg_11495" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_11496" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_11497" s="T360">v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_11498" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_11499" s="T362">n-n:case.poss</ta>
            <ta e="T364" id="Seg_11500" s="T363">v</ta>
            <ta e="T365" id="Seg_11501" s="T364">pers-n:case</ta>
            <ta e="T366" id="Seg_11502" s="T365">v-v:n.fin</ta>
            <ta e="T367" id="Seg_11503" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_11504" s="T367">v-v:n.fin</ta>
            <ta e="T369" id="Seg_11505" s="T368">adv</ta>
            <ta e="T370" id="Seg_11506" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_11507" s="T370">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_11508" s="T371">dempro-n:num</ta>
            <ta e="T373" id="Seg_11509" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_11510" s="T373">v-v:n.fin</ta>
            <ta e="T375" id="Seg_11511" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_11512" s="T375">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_11513" s="T376">conj</ta>
            <ta e="T379" id="Seg_11514" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_11515" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_11516" s="T380">v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_11517" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_11518" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_11519" s="T383">v-v&gt;v-v:pn</ta>
            <ta e="T385" id="Seg_11520" s="T384">conj</ta>
            <ta e="T386" id="Seg_11521" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_11522" s="T386">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T388" id="Seg_11523" s="T387">n-n:case.poss</ta>
            <ta e="T389" id="Seg_11524" s="T388">n-n:case.poss</ta>
            <ta e="T1120" id="Seg_11525" s="T389">v-v:n-fin</ta>
            <ta e="T390" id="Seg_11526" s="T1120">v-v:tense-v:pn</ta>
            <ta e="T391" id="Seg_11527" s="T390">que-n:case</ta>
            <ta e="T392" id="Seg_11528" s="T391">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_11529" s="T392">v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_11530" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_11531" s="T394">conj</ta>
            <ta e="T396" id="Seg_11532" s="T395">pers</ta>
            <ta e="T397" id="Seg_11533" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_11534" s="T397">conj</ta>
            <ta e="T399" id="Seg_11535" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_11536" s="T399">v-v&gt;v-v:pn</ta>
            <ta e="T401" id="Seg_11537" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_11538" s="T401">adj-n:case</ta>
            <ta e="T403" id="Seg_11539" s="T402">pers</ta>
            <ta e="T404" id="Seg_11540" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_11541" s="T404">v-v:tense-v:pn</ta>
            <ta e="T406" id="Seg_11542" s="T405">n-n:case</ta>
            <ta e="T408" id="Seg_11543" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_11544" s="T408">v-v&gt;v-v:n.fin</ta>
            <ta e="T410" id="Seg_11545" s="T409">adv</ta>
            <ta e="T411" id="Seg_11546" s="T410">v-v:tense-v:pn</ta>
            <ta e="T412" id="Seg_11547" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_11548" s="T412">v-v:tense-v:pn</ta>
            <ta e="T414" id="Seg_11549" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_11550" s="T414">dempro-n:case</ta>
            <ta e="T416" id="Seg_11551" s="T415">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_11552" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_11553" s="T417">pers-n:case</ta>
            <ta e="T419" id="Seg_11554" s="T418">v-v:n.fin</ta>
            <ta e="T420" id="Seg_11555" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_11556" s="T420">v-v:n.fin</ta>
            <ta e="T422" id="Seg_11557" s="T421">n</ta>
            <ta e="T423" id="Seg_11558" s="T422">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_11559" s="T424">dempro-n:num</ta>
            <ta e="T426" id="Seg_11560" s="T425">v-v:tense-v:pn</ta>
            <ta e="T427" id="Seg_11561" s="T426">v-v:n.fin</ta>
            <ta e="T428" id="Seg_11562" s="T427">n-n:case</ta>
            <ta e="T429" id="Seg_11563" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_11564" s="T429">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_11565" s="T430">conj</ta>
            <ta e="T432" id="Seg_11566" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_11567" s="T432">v-v&gt;v-v:pn</ta>
            <ta e="T434" id="Seg_11568" s="T433">que-n:case</ta>
            <ta e="T435" id="Seg_11569" s="T434">v-v&gt;v-v:pn</ta>
            <ta e="T436" id="Seg_11570" s="T435">adv</ta>
            <ta e="T437" id="Seg_11571" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_11572" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_11573" s="T438">conj</ta>
            <ta e="T440" id="Seg_11574" s="T439">pers</ta>
            <ta e="T441" id="Seg_11575" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_11576" s="T441">adv</ta>
            <ta e="T443" id="Seg_11577" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_11578" s="T443">n-adv:case</ta>
            <ta e="T445" id="Seg_11579" s="T444">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_11580" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_11581" s="T446">adj</ta>
            <ta e="T448" id="Seg_11582" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_11583" s="T448">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_11584" s="T449">n-n:case</ta>
            <ta e="T452" id="Seg_11585" s="T451">v-v&gt;v-v:n.fin</ta>
            <ta e="T454" id="Seg_11586" s="T453">adv</ta>
            <ta e="T455" id="Seg_11587" s="T454">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_11588" s="T455">v-v:tense-v:pn</ta>
            <ta e="T457" id="Seg_11589" s="T456">v-v:tense-v:pn</ta>
            <ta e="T458" id="Seg_11590" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_11591" s="T458">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_11592" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_11593" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_11594" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_11595" s="T462">pers-n:case</ta>
            <ta e="T464" id="Seg_11596" s="T463">v-v:n.fin</ta>
            <ta e="T465" id="Seg_11597" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_11598" s="T465">adv</ta>
            <ta e="T467" id="Seg_11599" s="T466">n</ta>
            <ta e="T468" id="Seg_11600" s="T467">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_11601" s="T469">v-v:tense-v:pn</ta>
            <ta e="T471" id="Seg_11602" s="T470">v-v:n.fin</ta>
            <ta e="T472" id="Seg_11603" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_11604" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_11605" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_11606" s="T474">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_11607" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_11608" s="T476">conj</ta>
            <ta e="T478" id="Seg_11609" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_11610" s="T478">que-n:case</ta>
            <ta e="T480" id="Seg_11611" s="T479">v-v:tense-v:pn</ta>
            <ta e="T481" id="Seg_11612" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_11613" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_11614" s="T482">conj</ta>
            <ta e="T484" id="Seg_11615" s="T483">pers</ta>
            <ta e="T485" id="Seg_11616" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_11617" s="T485">conj</ta>
            <ta e="T487" id="Seg_11618" s="T486">dempro-n:case</ta>
            <ta e="T488" id="Seg_11619" s="T487">v-v&gt;v-v:pn</ta>
            <ta e="T489" id="Seg_11620" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_11621" s="T489">adj</ta>
            <ta e="T491" id="Seg_11622" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_11623" s="T491">v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_11624" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_11625" s="T493">v-v&gt;v-v:n.fin</ta>
            <ta e="T495" id="Seg_11626" s="T494">n-adv:case</ta>
            <ta e="T496" id="Seg_11627" s="T495">v-v:tense-v:pn</ta>
            <ta e="T497" id="Seg_11628" s="T496">v-v:tense-v:pn</ta>
            <ta e="T499" id="Seg_11629" s="T498">quant</ta>
            <ta e="T500" id="Seg_11630" s="T499">n-n:num</ta>
            <ta e="T501" id="Seg_11631" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_11632" s="T501">v-v:tense-v:pn</ta>
            <ta e="T503" id="Seg_11633" s="T502">adv</ta>
            <ta e="T504" id="Seg_11634" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_11635" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_11636" s="T505">v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_11637" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_11638" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_11639" s="T508">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_11640" s="T511">n-n:num</ta>
            <ta e="T513" id="Seg_11641" s="T512">v-v:n.fin</ta>
            <ta e="T514" id="Seg_11642" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_11643" s="T514">adv</ta>
            <ta e="T516" id="Seg_11644" s="T515">dempro-n:case</ta>
            <ta e="T517" id="Seg_11645" s="T516">v-v&gt;v-v:pn</ta>
            <ta e="T518" id="Seg_11646" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_11647" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_11648" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_11649" s="T520">v-v:n.fin</ta>
            <ta e="T522" id="Seg_11650" s="T521">dempro-n:case</ta>
            <ta e="T523" id="Seg_11651" s="T522">v-v:tense-v:pn</ta>
            <ta e="T524" id="Seg_11652" s="T523">dempro-n:case</ta>
            <ta e="T525" id="Seg_11653" s="T524">n-n:case.poss</ta>
            <ta e="T526" id="Seg_11654" s="T525">v-v:tense-v:pn</ta>
            <ta e="T527" id="Seg_11655" s="T526">v-v:n.fin</ta>
            <ta e="T528" id="Seg_11656" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_11657" s="T528">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_11658" s="T529">conj</ta>
            <ta e="T531" id="Seg_11659" s="T530">n-n:case</ta>
            <ta e="T532" id="Seg_11660" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_11661" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_11662" s="T533">v-v:n.fin</ta>
            <ta e="T535" id="Seg_11663" s="T534">conj</ta>
            <ta e="T536" id="Seg_11664" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_11665" s="T536">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_11666" s="T537">v-v&gt;v-v:pn</ta>
            <ta e="T539" id="Seg_11667" s="T538">pers</ta>
            <ta e="T540" id="Seg_11668" s="T539">v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_11669" s="T540">v-v:ins-v:mood.pn</ta>
            <ta e="T542" id="Seg_11670" s="T541">conj</ta>
            <ta e="T543" id="Seg_11671" s="T542">v-v:ins-v:mood.pn</ta>
            <ta e="T544" id="Seg_11672" s="T543">pers</ta>
            <ta e="T545" id="Seg_11673" s="T544">v-v:mood.pn</ta>
            <ta e="T548" id="Seg_11674" s="T547">v-v:mood.pn</ta>
            <ta e="T549" id="Seg_11675" s="T548">v-v:n.fin</ta>
            <ta e="T550" id="Seg_11676" s="T549">quant</ta>
            <ta e="T552" id="Seg_11677" s="T551">num-n:case</ta>
            <ta e="T553" id="Seg_11678" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_11679" s="T553">v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_11680" s="T554">n-n&gt;v-v:n.fin</ta>
            <ta e="T557" id="Seg_11681" s="T556">n-n:num</ta>
            <ta e="T558" id="Seg_11682" s="T557">v-v:tense-v:pn</ta>
            <ta e="T559" id="Seg_11683" s="T558">conj</ta>
            <ta e="T560" id="Seg_11684" s="T559">v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_11685" s="T560">dempro-n:case</ta>
            <ta e="T562" id="Seg_11686" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_11687" s="T562">v-v:tense-v:pn</ta>
            <ta e="T564" id="Seg_11688" s="T563">pers</ta>
            <ta e="T565" id="Seg_11689" s="T564">adv</ta>
            <ta e="T566" id="Seg_11690" s="T565">pers</ta>
            <ta e="T567" id="Seg_11691" s="T566">v-v:tense-v:pn</ta>
            <ta e="T568" id="Seg_11692" s="T567">pers</ta>
            <ta e="T569" id="Seg_11693" s="T568">aux-v:mood.pn</ta>
            <ta e="T570" id="Seg_11694" s="T569">v-v:ins-v:mood.pn</ta>
            <ta e="T571" id="Seg_11695" s="T570">pers</ta>
            <ta e="T572" id="Seg_11696" s="T571">adv</ta>
            <ta e="T573" id="Seg_11697" s="T572">n-n:num</ta>
            <ta e="T574" id="Seg_11698" s="T573">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_11699" s="T574">v-v:tense-v:pn</ta>
            <ta e="T576" id="Seg_11700" s="T575">pers</ta>
            <ta e="T577" id="Seg_11701" s="T576">dempro-n:case</ta>
            <ta e="T578" id="Seg_11702" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_11703" s="T578">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T580" id="Seg_11704" s="T579">v-v:ins-v:mood.pn</ta>
            <ta e="T581" id="Seg_11705" s="T580">pers</ta>
            <ta e="T582" id="Seg_11706" s="T581">n-n:num-n:case</ta>
            <ta e="T583" id="Seg_11707" s="T582">conj</ta>
            <ta e="T584" id="Seg_11708" s="T583">v-v:ins-v:mood.pn</ta>
            <ta e="T585" id="Seg_11709" s="T584">conj</ta>
            <ta e="T586" id="Seg_11710" s="T585">dempro-n:num</ta>
            <ta e="T587" id="Seg_11711" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_11712" s="T587">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_11713" s="T588">pers</ta>
            <ta e="T590" id="Seg_11714" s="T589">conj</ta>
            <ta e="T591" id="Seg_11715" s="T590">dempro-n:case</ta>
            <ta e="T592" id="Seg_11716" s="T591">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_11717" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_11718" s="T593">v-v:n.fin</ta>
            <ta e="T595" id="Seg_11719" s="T594">ptcl</ta>
            <ta e="T596" id="Seg_11720" s="T595">v-v:n.fin</ta>
            <ta e="T597" id="Seg_11721" s="T596">adv</ta>
            <ta e="T598" id="Seg_11722" s="T597">dempro-n:case</ta>
            <ta e="T599" id="Seg_11723" s="T598">dempro-n:case</ta>
            <ta e="T600" id="Seg_11724" s="T599">v-v&gt;v-v:pn</ta>
            <ta e="T601" id="Seg_11725" s="T600">v-v:ins-v:mood.pn</ta>
            <ta e="T602" id="Seg_11726" s="T601">dempro-n:case</ta>
            <ta e="T603" id="Seg_11727" s="T602">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_11728" s="T603">dempro-n:case</ta>
            <ta e="T605" id="Seg_11729" s="T604">conj</ta>
            <ta e="T606" id="Seg_11730" s="T605">adv</ta>
            <ta e="T607" id="Seg_11731" s="T606">ptcl</ta>
            <ta e="T609" id="Seg_11732" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_11733" s="T609">v-v:n.fin</ta>
            <ta e="T611" id="Seg_11734" s="T610">conj</ta>
            <ta e="T612" id="Seg_11735" s="T611">adv</ta>
            <ta e="T613" id="Seg_11736" s="T612">dempro-n:case</ta>
            <ta e="T614" id="Seg_11737" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_11738" s="T614">dempro-n:case</ta>
            <ta e="T616" id="Seg_11739" s="T615">n-n:case</ta>
            <ta e="T617" id="Seg_11740" s="T616">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T618" id="Seg_11741" s="T617">conj</ta>
            <ta e="T619" id="Seg_11742" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_11743" s="T619">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_11744" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_11745" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_11746" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_11747" s="T624">n-n:case</ta>
            <ta e="T627" id="Seg_11748" s="T626">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_11749" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_11750" s="T628">v-v:n.fin</ta>
            <ta e="T631" id="Seg_11751" s="T630">pers</ta>
            <ta e="T632" id="Seg_11752" s="T631">n-n:case.poss</ta>
            <ta e="T633" id="Seg_11753" s="T632">conj</ta>
            <ta e="T634" id="Seg_11754" s="T633">pers</ta>
            <ta e="T635" id="Seg_11755" s="T634">n-n:case.poss</ta>
            <ta e="T636" id="Seg_11756" s="T635">adv</ta>
            <ta e="T637" id="Seg_11757" s="T636">v-v:tense-v:pn</ta>
            <ta e="T638" id="Seg_11758" s="T637">dempro-n:case</ta>
            <ta e="T639" id="Seg_11759" s="T638">v-v:tense-v:pn</ta>
            <ta e="T640" id="Seg_11760" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_11761" s="T640">v-v:tense-v:pn</ta>
            <ta e="T642" id="Seg_11762" s="T641">n-n:case.poss</ta>
            <ta e="T643" id="Seg_11763" s="T642">conj</ta>
            <ta e="T644" id="Seg_11764" s="T643">n-n:case</ta>
            <ta e="T645" id="Seg_11765" s="T644">n-n:case.poss</ta>
            <ta e="T646" id="Seg_11766" s="T645">adv</ta>
            <ta e="T647" id="Seg_11767" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_11768" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_11769" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_11770" s="T649">pers</ta>
            <ta e="T651" id="Seg_11771" s="T650">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_11772" s="T651">adv</ta>
            <ta e="T653" id="Seg_11773" s="T652">adv</ta>
            <ta e="T654" id="Seg_11774" s="T653">adj-n:case</ta>
            <ta e="T655" id="Seg_11775" s="T654">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T656" id="Seg_11776" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_11777" s="T656">v-v&gt;v-v:pn</ta>
            <ta e="T658" id="Seg_11778" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_11779" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_11780" s="T659">adv</ta>
            <ta e="T661" id="Seg_11781" s="T660">v-v:n.fin</ta>
            <ta e="T662" id="Seg_11782" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_11783" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_11784" s="T663">pers</ta>
            <ta e="T665" id="Seg_11785" s="T664">pers</ta>
            <ta e="T666" id="Seg_11786" s="T665">v-v:mood.pn</ta>
            <ta e="T667" id="Seg_11787" s="T666">n-n:case.poss</ta>
            <ta e="T668" id="Seg_11788" s="T667">conj</ta>
            <ta e="T669" id="Seg_11789" s="T668">pers</ta>
            <ta e="T670" id="Seg_11790" s="T669">n-n:case.poss</ta>
            <ta e="T671" id="Seg_11791" s="T670">adv</ta>
            <ta e="T672" id="Seg_11792" s="T671">n-n:case</ta>
            <ta e="T674" id="Seg_11793" s="T672">n-n:case</ta>
            <ta e="T675" id="Seg_11794" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_11795" s="T675">n-n:case</ta>
            <ta e="T677" id="Seg_11796" s="T676">v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_11797" s="T677">dempro-n:case</ta>
            <ta e="T679" id="Seg_11798" s="T678">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_11799" s="T679">adj</ta>
            <ta e="T681" id="Seg_11800" s="T680">adv</ta>
            <ta e="T682" id="Seg_11801" s="T681">v-v:tense-v:pn</ta>
            <ta e="T683" id="Seg_11802" s="T682">dempro-n:case</ta>
            <ta e="T684" id="Seg_11803" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_11804" s="T684">v-v:tense-v:pn</ta>
            <ta e="T686" id="Seg_11805" s="T685">n-n:case.poss</ta>
            <ta e="T687" id="Seg_11806" s="T686">conj</ta>
            <ta e="T688" id="Seg_11807" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_11808" s="T688">n-n:case.poss</ta>
            <ta e="T690" id="Seg_11809" s="T689">conj</ta>
            <ta e="T691" id="Seg_11810" s="T690">adv</ta>
            <ta e="T693" id="Seg_11811" s="T692">dempro-n:case</ta>
            <ta e="T694" id="Seg_11812" s="T693">ptcl</ta>
            <ta e="T695" id="Seg_11813" s="T694">n-n:case.poss</ta>
            <ta e="T696" id="Seg_11814" s="T695">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_11815" s="T697">v-v&gt;v-v:tense</ta>
            <ta e="T699" id="Seg_11816" s="T698">n-n:case</ta>
            <ta e="T700" id="Seg_11817" s="T699">conj</ta>
            <ta e="T701" id="Seg_11818" s="T700">n-n:case</ta>
            <ta e="T702" id="Seg_11819" s="T701">dempro-n:num-n:case</ta>
            <ta e="T703" id="Seg_11820" s="T702">n-n:num-n:case</ta>
            <ta e="T704" id="Seg_11821" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_11822" s="T704">v-v:tense-v:pn</ta>
            <ta e="T706" id="Seg_11823" s="T705">num-n:case</ta>
            <ta e="T707" id="Seg_11824" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_11825" s="T707">ptcl</ta>
            <ta e="T709" id="Seg_11826" s="T708">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_11827" s="T709">adv</ta>
            <ta e="T711" id="Seg_11828" s="T710">adj-n:case</ta>
            <ta e="T712" id="Seg_11829" s="T711">n-n:num</ta>
            <ta e="T713" id="Seg_11830" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_11831" s="T713">que-n:case</ta>
            <ta e="T715" id="Seg_11832" s="T714">ptcl</ta>
            <ta e="T719" id="Seg_11833" s="T718">que-n:case</ta>
            <ta e="T720" id="Seg_11834" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11835" s="T720">v-v&gt;v-v:pn</ta>
            <ta e="T722" id="Seg_11836" s="T721">n-n:num</ta>
            <ta e="T723" id="Seg_11837" s="T722">v-v&gt;v-v:pn</ta>
            <ta e="T724" id="Seg_11838" s="T723">quant</ta>
            <ta e="T725" id="Seg_11839" s="T724">v-v&gt;v-v:pn</ta>
            <ta e="T726" id="Seg_11840" s="T725">adv</ta>
            <ta e="T727" id="Seg_11841" s="T726">v-v:tense-v:pn</ta>
            <ta e="T728" id="Seg_11842" s="T727">adj-n:case</ta>
            <ta e="T729" id="Seg_11843" s="T728">n-n:case</ta>
            <ta e="T730" id="Seg_11844" s="T729">conj</ta>
            <ta e="T731" id="Seg_11845" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_11846" s="T731">adv</ta>
            <ta e="T733" id="Seg_11847" s="T732">v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_11848" s="T733">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_11849" s="T734">v-v:mood-v:pn</ta>
            <ta e="T737" id="Seg_11850" s="T736">pers-n:case</ta>
            <ta e="T738" id="Seg_11851" s="T737">adv</ta>
            <ta e="T739" id="Seg_11852" s="T738">n-n:case</ta>
            <ta e="T740" id="Seg_11853" s="T739">v-v:tense-v:pn</ta>
            <ta e="T741" id="Seg_11854" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_11855" s="T741">v-v:mood-v:pn</ta>
            <ta e="T744" id="Seg_11856" s="T743">v-v:tense-v:pn</ta>
            <ta e="T745" id="Seg_11857" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_11858" s="T745">adv</ta>
            <ta e="T747" id="Seg_11859" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_11860" s="T747">v-v:n.fin</ta>
            <ta e="T749" id="Seg_11861" s="T748">n-n:case</ta>
            <ta e="T751" id="Seg_11862" s="T749">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_11863" s="T751">n-n:case</ta>
            <ta e="T753" id="Seg_11864" s="T752">v-v:tense-v:pn</ta>
            <ta e="T754" id="Seg_11865" s="T753">n-n:num</ta>
            <ta e="T755" id="Seg_11866" s="T754">n-n:case.poss</ta>
            <ta e="T756" id="Seg_11867" s="T755">n-n:case.poss</ta>
            <ta e="T757" id="Seg_11868" s="T756">n-n:case.poss</ta>
            <ta e="T758" id="Seg_11869" s="T757">adv</ta>
            <ta e="T759" id="Seg_11870" s="T758">n-n:case</ta>
            <ta e="T760" id="Seg_11871" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_11872" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_11873" s="T761">n-n:case.poss</ta>
            <ta e="T763" id="Seg_11874" s="T762">v-v:tense-v:pn</ta>
            <ta e="T764" id="Seg_11875" s="T763">adv</ta>
            <ta e="T765" id="Seg_11876" s="T764">dempro-n:case</ta>
            <ta e="T766" id="Seg_11877" s="T765">n-n:case.poss-n:case</ta>
            <ta e="T767" id="Seg_11878" s="T766">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_11879" s="T767">adv</ta>
            <ta e="T769" id="Seg_11880" s="T768">n-n:case</ta>
            <ta e="T770" id="Seg_11881" s="T769">v-v&gt;v-v:pn</ta>
            <ta e="T772" id="Seg_11882" s="T771">v-v:tense-v:pn</ta>
            <ta e="T773" id="Seg_11883" s="T772">n-n:case</ta>
            <ta e="T774" id="Seg_11884" s="T773">conj</ta>
            <ta e="T775" id="Seg_11885" s="T774">dempro-n:case</ta>
            <ta e="T776" id="Seg_11886" s="T775">dempro-n:num</ta>
            <ta e="T778" id="Seg_11887" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_11888" s="T778">dempro-n:case</ta>
            <ta e="T780" id="Seg_11889" s="T779">v-v:tense-v:pn</ta>
            <ta e="T781" id="Seg_11890" s="T780">n-n:case</ta>
            <ta e="T782" id="Seg_11891" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_11892" s="T782">adv</ta>
            <ta e="T784" id="Seg_11893" s="T783">dempro-n:case</ta>
            <ta e="T785" id="Seg_11894" s="T784">adj</ta>
            <ta e="T786" id="Seg_11895" s="T785">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T787" id="Seg_11896" s="T786">n-n:case</ta>
            <ta e="T789" id="Seg_11897" s="T788">quant</ta>
            <ta e="T790" id="Seg_11898" s="T789">v-v:tense-v:pn</ta>
            <ta e="T791" id="Seg_11899" s="T790">dempro-n:case</ta>
            <ta e="T793" id="Seg_11900" s="T792">adv</ta>
            <ta e="T794" id="Seg_11901" s="T793">dempro-n:case</ta>
            <ta e="T795" id="Seg_11902" s="T794">n-n:num</ta>
            <ta e="T796" id="Seg_11903" s="T795">v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_11904" s="T796">adj-n:num</ta>
            <ta e="T798" id="Seg_11905" s="T797">adj-n:num</ta>
            <ta e="T799" id="Seg_11906" s="T798">adv</ta>
            <ta e="T800" id="Seg_11907" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_11908" s="T800">v-v:tense-v:pn</ta>
            <ta e="T803" id="Seg_11909" s="T802">adv</ta>
            <ta e="T804" id="Seg_11910" s="T803">adj-n:case</ta>
            <ta e="T806" id="Seg_11911" s="T805">adv</ta>
            <ta e="T807" id="Seg_11912" s="T806">dempro-n:case</ta>
            <ta e="T808" id="Seg_11913" s="T807">adv</ta>
            <ta e="T809" id="Seg_11914" s="T808">adj-n:case</ta>
            <ta e="T810" id="Seg_11915" s="T809">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T811" id="Seg_11916" s="T810">conj</ta>
            <ta e="T812" id="Seg_11917" s="T811">dempro-n:case</ta>
            <ta e="T813" id="Seg_11918" s="T812">n-n:case</ta>
            <ta e="T814" id="Seg_11919" s="T813">v-v&gt;v-v:pn</ta>
            <ta e="T815" id="Seg_11920" s="T814">n-n:case</ta>
            <ta e="T816" id="Seg_11921" s="T815">v-v&gt;v-v:pn</ta>
            <ta e="T817" id="Seg_11922" s="T816">dempro-n:case</ta>
            <ta e="T818" id="Seg_11923" s="T817">ptcl</ta>
            <ta e="T819" id="Seg_11924" s="T818">v-v:tense-v:pn</ta>
            <ta e="T820" id="Seg_11925" s="T819">conj</ta>
            <ta e="T821" id="Seg_11926" s="T820">n-n:case.poss</ta>
            <ta e="T823" id="Seg_11927" s="T822">n-n:case</ta>
            <ta e="T824" id="Seg_11928" s="T823">v-v&gt;v-v:pn</ta>
            <ta e="T825" id="Seg_11929" s="T824">que-n:case</ta>
            <ta e="T826" id="Seg_11930" s="T825">pers</ta>
            <ta e="T827" id="Seg_11931" s="T826">v-v&gt;v-v:pn</ta>
            <ta e="T828" id="Seg_11932" s="T827">conj</ta>
            <ta e="T829" id="Seg_11933" s="T828">refl-n:case.poss</ta>
            <ta e="T830" id="Seg_11934" s="T829">ptcl</ta>
            <ta e="T832" id="Seg_11935" s="T831">n-n:case.poss</ta>
            <ta e="T833" id="Seg_11936" s="T832">v-v&gt;v-v:pn</ta>
            <ta e="T834" id="Seg_11937" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_11938" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_11939" s="T835">adv</ta>
            <ta e="T838" id="Seg_11940" s="T837">adv</ta>
            <ta e="T839" id="Seg_11941" s="T838">n-n:case</ta>
            <ta e="T840" id="Seg_11942" s="T839">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T842" id="Seg_11943" s="T841">n-n:case</ta>
            <ta e="T843" id="Seg_11944" s="T842">v-v&gt;v-v:pn</ta>
            <ta e="T844" id="Seg_11945" s="T843">n-n:num</ta>
            <ta e="T845" id="Seg_11946" s="T844">adv</ta>
            <ta e="T846" id="Seg_11947" s="T845">v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_11948" s="T846">n-n:num</ta>
            <ta e="T848" id="Seg_11949" s="T847">v-v:mood-v:pn</ta>
            <ta e="T849" id="Seg_11950" s="T848">pers-n:case</ta>
            <ta e="T850" id="Seg_11951" s="T849">v-v:tense-v:pn</ta>
            <ta e="T851" id="Seg_11952" s="T850">n-n:case.poss</ta>
            <ta e="T852" id="Seg_11953" s="T851">n-n:case</ta>
            <ta e="T855" id="Seg_11954" s="T854">v-v:tense-v:pn</ta>
            <ta e="T856" id="Seg_11955" s="T855">v-v:ins-v:mood.pn</ta>
            <ta e="T857" id="Seg_11956" s="T856">que-n:case</ta>
            <ta e="T858" id="Seg_11957" s="T857">v-v&gt;v-v:pn</ta>
            <ta e="T859" id="Seg_11958" s="T858">ptcl</ta>
            <ta e="T860" id="Seg_11959" s="T859">pers</ta>
            <ta e="T861" id="Seg_11960" s="T860">aux-v:pn</ta>
            <ta e="T862" id="Seg_11961" s="T861">v-v:ins-v:n.fin</ta>
            <ta e="T863" id="Seg_11962" s="T862">n-n:case</ta>
            <ta e="T864" id="Seg_11963" s="T863">n-n:case.poss</ta>
            <ta e="T865" id="Seg_11964" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_11965" s="T865">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T868" id="Seg_11966" s="T867">n-n:case</ta>
            <ta e="T869" id="Seg_11967" s="T868">v-v:tense-v:pn</ta>
            <ta e="T870" id="Seg_11968" s="T869">n-n:case.poss</ta>
            <ta e="T871" id="Seg_11969" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_11970" s="T871">n-n:case</ta>
            <ta e="T874" id="Seg_11971" s="T873">v-v:mood.pn</ta>
            <ta e="T875" id="Seg_11972" s="T874">adv</ta>
            <ta e="T876" id="Seg_11973" s="T875">dempro-n:case</ta>
            <ta e="T877" id="Seg_11974" s="T876">v-v:tense-v:pn</ta>
            <ta e="T878" id="Seg_11975" s="T877">quant</ta>
            <ta e="T879" id="Seg_11976" s="T878">n-n:num</ta>
            <ta e="T880" id="Seg_11977" s="T879">v-v&gt;v-v:pn</ta>
            <ta e="T881" id="Seg_11978" s="T880">n-n:num</ta>
            <ta e="T883" id="Seg_11979" s="T882">v-v&gt;v-v:pn</ta>
            <ta e="T884" id="Seg_11980" s="T883">conj</ta>
            <ta e="T885" id="Seg_11981" s="T884">dempro-n:case</ta>
            <ta e="T886" id="Seg_11982" s="T885">v-v&gt;v-v:pn</ta>
            <ta e="T890" id="Seg_11983" s="T889">adv</ta>
            <ta e="T891" id="Seg_11984" s="T890">dempro-n:num</ta>
            <ta e="T892" id="Seg_11985" s="T891">v-v:tense-v:pn</ta>
            <ta e="T893" id="Seg_11986" s="T892">n-n:case</ta>
            <ta e="T894" id="Seg_11987" s="T893">adv</ta>
            <ta e="T895" id="Seg_11988" s="T894">adj-n:case</ta>
            <ta e="T896" id="Seg_11989" s="T895">conj</ta>
            <ta e="T897" id="Seg_11990" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11991" s="T897">v-v:n.fin</ta>
            <ta e="T899" id="Seg_11992" s="T898">n-n:case</ta>
            <ta e="T900" id="Seg_11993" s="T899">v-v:tense-v:pn</ta>
            <ta e="T901" id="Seg_11994" s="T900">que-n:case</ta>
            <ta e="T902" id="Seg_11995" s="T901">pers</ta>
            <ta e="T903" id="Seg_11996" s="T902">ptcl</ta>
            <ta e="T904" id="Seg_11997" s="T903">v-v:tense-v:pn</ta>
            <ta e="T905" id="Seg_11998" s="T904">conj</ta>
            <ta e="T906" id="Seg_11999" s="T905">dempro-n:case</ta>
            <ta e="T907" id="Seg_12000" s="T906">v-v:tense-v:pn</ta>
            <ta e="T908" id="Seg_12001" s="T907">conj</ta>
            <ta e="T909" id="Seg_12002" s="T908">adv</ta>
            <ta e="T910" id="Seg_12003" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_12004" s="T910">dempro-n:case</ta>
            <ta e="T912" id="Seg_12005" s="T911">v-v&gt;v-v:pn</ta>
            <ta e="T913" id="Seg_12006" s="T912">conj</ta>
            <ta e="T914" id="Seg_12007" s="T913">dempro-n:case</ta>
            <ta e="T915" id="Seg_12008" s="T914">v-v:tense-v:pn</ta>
            <ta e="T917" id="Seg_12009" s="T916">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_12010" s="T917">conj</ta>
            <ta e="T919" id="Seg_12011" s="T918">ptcl</ta>
            <ta e="T920" id="Seg_12012" s="T919">n-n:case</ta>
            <ta e="T1121" id="Seg_12013" s="T920">v-v:n-fin</ta>
            <ta e="T921" id="Seg_12014" s="T1121">v-v:tense-v:pn</ta>
            <ta e="T922" id="Seg_12015" s="T921">adv</ta>
            <ta e="T923" id="Seg_12016" s="T922">ptcl</ta>
            <ta e="T926" id="Seg_12017" s="T925">n-n:case</ta>
            <ta e="T927" id="Seg_12018" s="T926">n-n:case.poss</ta>
            <ta e="T928" id="Seg_12019" s="T927">pers</ta>
            <ta e="T929" id="Seg_12020" s="T928">adv</ta>
            <ta e="T930" id="Seg_12021" s="T929">adj-n:case</ta>
            <ta e="T931" id="Seg_12022" s="T930">adj-n:case</ta>
            <ta e="T932" id="Seg_12023" s="T931">n-n:case</ta>
            <ta e="T933" id="Seg_12024" s="T932">v-v:tense-v:pn</ta>
            <ta e="T934" id="Seg_12025" s="T933">adv</ta>
            <ta e="T935" id="Seg_12026" s="T934">v-v:tense-v:pn</ta>
            <ta e="T936" id="Seg_12027" s="T935">v-v:tense-v:pn</ta>
            <ta e="T937" id="Seg_12028" s="T936">pers</ta>
            <ta e="T938" id="Seg_12029" s="T937">n-n:case</ta>
            <ta e="T939" id="Seg_12030" s="T938">v-v&gt;v-v:n.fin</ta>
            <ta e="T941" id="Seg_12031" s="T940">n-n:case</ta>
            <ta e="T942" id="Seg_12032" s="T941">v-v:tense-v:pn</ta>
            <ta e="T943" id="Seg_12033" s="T942">v-v:tense-v:pn</ta>
            <ta e="T944" id="Seg_12034" s="T943">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T945" id="Seg_12035" s="T944">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T946" id="Seg_12036" s="T945">que=ptcl</ta>
            <ta e="T947" id="Seg_12037" s="T946">v-v:pn</ta>
            <ta e="T948" id="Seg_12038" s="T947">ptcl</ta>
            <ta e="T949" id="Seg_12039" s="T948">v-v:tense-v:pn</ta>
            <ta e="T951" id="Seg_12040" s="T950">n-n:case</ta>
            <ta e="T952" id="Seg_12041" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_12042" s="T952">v-v:tense-v:pn</ta>
            <ta e="T954" id="Seg_12043" s="T953">n-adv:case</ta>
            <ta e="T955" id="Seg_12044" s="T954">v-v:tense-v:pn</ta>
            <ta e="T956" id="Seg_12045" s="T955">adv</ta>
            <ta e="T957" id="Seg_12046" s="T956">v-v:tense-v:pn</ta>
            <ta e="T958" id="Seg_12047" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_12048" s="T958">v-v:tense-v:pn</ta>
            <ta e="T960" id="Seg_12049" s="T959">v-v:tense-v:pn</ta>
            <ta e="T961" id="Seg_12050" s="T960">dempro-n:case</ta>
            <ta e="T962" id="Seg_12051" s="T961">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T963" id="Seg_12052" s="T962">n-n:case</ta>
            <ta e="T964" id="Seg_12053" s="T963">adv</ta>
            <ta e="T965" id="Seg_12054" s="T964">v-v:pn</ta>
            <ta e="T967" id="Seg_12055" s="T966">v-v:tense-v:pn</ta>
            <ta e="T968" id="Seg_12056" s="T967">n-n:case</ta>
            <ta e="T969" id="Seg_12057" s="T968">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T970" id="Seg_12058" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_12059" s="T970">ptcl</ta>
            <ta e="T972" id="Seg_12060" s="T971">n-n:case</ta>
            <ta e="T974" id="Seg_12061" s="T973">ptcl</ta>
            <ta e="T975" id="Seg_12062" s="T974">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T976" id="Seg_12063" s="T975">adv</ta>
            <ta e="T977" id="Seg_12064" s="T976">adv</ta>
            <ta e="T978" id="Seg_12065" s="T977">n-n:case</ta>
            <ta e="T979" id="Seg_12066" s="T978">n-n:case</ta>
            <ta e="T980" id="Seg_12067" s="T979">v-v:tense-v:pn</ta>
            <ta e="T981" id="Seg_12068" s="T980">dempro-n:num</ta>
            <ta e="T983" id="Seg_12069" s="T982">v-v:tense-v:pn</ta>
            <ta e="T984" id="Seg_12070" s="T983">n-n:case.poss</ta>
            <ta e="T986" id="Seg_12071" s="T985">v-v:tense-v:pn</ta>
            <ta e="T987" id="Seg_12072" s="T986">v-v:tense-v:pn</ta>
            <ta e="T988" id="Seg_12073" s="T987">n-n:case.poss</ta>
            <ta e="T989" id="Seg_12074" s="T988">num-n:case</ta>
            <ta e="T990" id="Seg_12075" s="T989">n-n:case.poss-n:case</ta>
            <ta e="T991" id="Seg_12076" s="T990">n-n:case</ta>
            <ta e="T992" id="Seg_12077" s="T991">v-v:tense-v:pn</ta>
            <ta e="T993" id="Seg_12078" s="T992">pers</ta>
            <ta e="T995" id="Seg_12079" s="T994">n-n:case.poss</ta>
            <ta e="T996" id="Seg_12080" s="T995">v-v:tense-v:pn</ta>
            <ta e="T997" id="Seg_12081" s="T996">propr-n:case</ta>
            <ta e="T998" id="Seg_12082" s="T997">conj</ta>
            <ta e="T999" id="Seg_12083" s="T998">v-v:tense-v:pn</ta>
            <ta e="T1001" id="Seg_12084" s="T1000">num-n:case</ta>
            <ta e="T1002" id="Seg_12085" s="T1001">n-n:case</ta>
            <ta e="T1003" id="Seg_12086" s="T1002">v-v:tense-v:pn</ta>
            <ta e="T1004" id="Seg_12087" s="T1003">n-n:case</ta>
            <ta e="T1005" id="Seg_12088" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_12089" s="T1005">n-n:case</ta>
            <ta e="T1008" id="Seg_12090" s="T1007">n-n:case</ta>
            <ta e="T1009" id="Seg_12091" s="T1008">adv</ta>
            <ta e="T1011" id="Seg_12092" s="T1010">n-n:case.poss</ta>
            <ta e="T1012" id="Seg_12093" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_12094" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_12095" s="T1013">pers</ta>
            <ta e="T1015" id="Seg_12096" s="T1014">v-v:tense-v:pn</ta>
            <ta e="T1016" id="Seg_12097" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_12098" s="T1016">que-n:case</ta>
            <ta e="T1018" id="Seg_12099" s="T1017">adj-n:case</ta>
            <ta e="T1019" id="Seg_12100" s="T1018">v-v:tense-v:pn</ta>
            <ta e="T1020" id="Seg_12101" s="T1019">n-n:case</ta>
            <ta e="T1021" id="Seg_12102" s="T1020">ptcl</ta>
            <ta e="T1022" id="Seg_12103" s="T1021">v-v:tense-v:pn</ta>
            <ta e="T1023" id="Seg_12104" s="T1022">n-n:case</ta>
            <ta e="T1024" id="Seg_12105" s="T1023">ptcl</ta>
            <ta e="T1025" id="Seg_12106" s="T1024">v-v:tense-v:pn</ta>
            <ta e="T1026" id="Seg_12107" s="T1025">adv</ta>
            <ta e="T1027" id="Seg_12108" s="T1026">v-v&gt;v-v:pn</ta>
            <ta e="T1028" id="Seg_12109" s="T1027">adj-n:case</ta>
            <ta e="T1029" id="Seg_12110" s="T1028">n-n:case</ta>
            <ta e="T1030" id="Seg_12111" s="T1029">v-v:tense-v:pn</ta>
            <ta e="T1031" id="Seg_12112" s="T1030">ptcl</ta>
            <ta e="T1032" id="Seg_12113" s="T1031">n-n:case</ta>
            <ta e="T1033" id="Seg_12114" s="T1032">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1034" id="Seg_12115" s="T1033">conj</ta>
            <ta e="T1035" id="Seg_12116" s="T1034">dempro-n:case</ta>
            <ta e="T1036" id="Seg_12117" s="T1035">ptcl</ta>
            <ta e="T1037" id="Seg_12118" s="T1036">v-v:tense-v:pn</ta>
            <ta e="T1039" id="Seg_12119" s="T1038">ptcl</ta>
            <ta e="T1040" id="Seg_12120" s="T1039">v-v:pn</ta>
            <ta e="T1041" id="Seg_12121" s="T1040">v-v&gt;v-v:pn</ta>
            <ta e="T1042" id="Seg_12122" s="T1041">n-n:case</ta>
            <ta e="T1043" id="Seg_12123" s="T1042">adj-n:case</ta>
            <ta e="T1044" id="Seg_12124" s="T1043">conj</ta>
            <ta e="T1045" id="Seg_12125" s="T1044">que-n:case</ta>
            <ta e="T1046" id="Seg_12126" s="T1045">adj-n:case</ta>
            <ta e="T1047" id="Seg_12127" s="T1046">pers</ta>
            <ta e="T1048" id="Seg_12128" s="T1047">v-v:tense-v:pn</ta>
            <ta e="T1049" id="Seg_12129" s="T1048">dempro-n:case</ta>
            <ta e="T1051" id="Seg_12130" s="T1050">n-n:case.poss</ta>
            <ta e="T1052" id="Seg_12131" s="T1051">n-n:case.poss</ta>
            <ta e="T1053" id="Seg_12132" s="T1052">dempro-n:case</ta>
            <ta e="T1054" id="Seg_12133" s="T1053">ptcl</ta>
            <ta e="T1055" id="Seg_12134" s="T1054">que-n:case=ptcl</ta>
            <ta e="T1056" id="Seg_12135" s="T1055">ptcl</ta>
            <ta e="T1057" id="Seg_12136" s="T1056">v-v:pn</ta>
            <ta e="T1058" id="Seg_12137" s="T1057">ptcl</ta>
            <ta e="T1059" id="Seg_12138" s="T1058">n-n:case</ta>
            <ta e="T1060" id="Seg_12139" s="T1059">ptcl</ta>
            <ta e="T1061" id="Seg_12140" s="T1060">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1062" id="Seg_12141" s="T1061">ptcl</ta>
            <ta e="T1063" id="Seg_12142" s="T1062">adj-n:case</ta>
            <ta e="T1064" id="Seg_12143" s="T1063">v-v:tense-v:pn</ta>
            <ta e="T1065" id="Seg_12144" s="T1064">conj</ta>
            <ta e="T1066" id="Seg_12145" s="T1065">%%</ta>
            <ta e="T1067" id="Seg_12146" s="T1066">n-n:case.poss</ta>
            <ta e="T1068" id="Seg_12147" s="T1067">quant</ta>
            <ta e="T1070" id="Seg_12148" s="T1069">v-v:tense-v:pn</ta>
            <ta e="T1071" id="Seg_12149" s="T1070">v-v:tense-v:pn</ta>
            <ta e="T1072" id="Seg_12150" s="T1071">pers</ta>
            <ta e="T1074" id="Seg_12151" s="T1073">adj-n:case</ta>
            <ta e="T1075" id="Seg_12152" s="T1074">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1077" id="Seg_12153" s="T1076">dempro-n:case</ta>
            <ta e="T1078" id="Seg_12154" s="T1077">num-n:case</ta>
            <ta e="T1079" id="Seg_12155" s="T1078">n-n:case</ta>
            <ta e="T1080" id="Seg_12156" s="T1079">v-v&gt;v-v:tense</ta>
            <ta e="T1081" id="Seg_12157" s="T1080">adv</ta>
            <ta e="T1084" id="Seg_12158" s="T1083">v-v:tense-v:pn</ta>
            <ta e="T1085" id="Seg_12159" s="T1084">ptcl</ta>
            <ta e="T1086" id="Seg_12160" s="T1085">dempro-n:case</ta>
            <ta e="T1088" id="Seg_12161" s="T1087">que=ptcl</ta>
            <ta e="T1090" id="Seg_12162" s="T1089">v-v:tense-v:pn</ta>
            <ta e="T1091" id="Seg_12163" s="T1090">conj</ta>
            <ta e="T1092" id="Seg_12164" s="T1091">adv</ta>
            <ta e="T1094" id="Seg_12165" s="T1093">n-n:case.poss</ta>
            <ta e="T1095" id="Seg_12166" s="T1094">ptcl</ta>
            <ta e="T1096" id="Seg_12167" s="T1095">ptcl</ta>
            <ta e="T1097" id="Seg_12168" s="T1096">v-v:tense-v:pn</ta>
            <ta e="T1099" id="Seg_12169" s="T1098">pers</ta>
            <ta e="T1101" id="Seg_12170" s="T1100">ptcl</ta>
            <ta e="T1102" id="Seg_12171" s="T1101">adv</ta>
            <ta e="T1105" id="Seg_12172" s="T1104">n-n:case</ta>
            <ta e="T1106" id="Seg_12173" s="T1105">adv</ta>
            <ta e="T1122" id="Seg_12174" s="T1106">v-v:n-fin</ta>
            <ta e="T1107" id="Seg_12175" s="T1122">v-v:tense-v:pn</ta>
            <ta e="T1123" id="Seg_12176" s="T1107">v-v:n-fin</ta>
            <ta e="T1108" id="Seg_12177" s="T1123">v-v:tense-v:pn</ta>
            <ta e="T1109" id="Seg_12178" s="T1108">ptcl</ta>
            <ta e="T1110" id="Seg_12179" s="T1109">v-v:tense-v:pn</ta>
            <ta e="T1111" id="Seg_12180" s="T1110">conj</ta>
            <ta e="T1112" id="Seg_12181" s="T1111">v-v:tense-v:pn</ta>
            <ta e="T1113" id="Seg_12182" s="T1112">adv</ta>
            <ta e="T1114" id="Seg_12183" s="T1113">v-v:mood.pn</ta>
            <ta e="T1115" id="Seg_12184" s="T1114">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T1" id="Seg_12185" s="T0">v</ta>
            <ta e="T2" id="Seg_12186" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_12187" s="T2">v</ta>
            <ta e="T5" id="Seg_12188" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_12189" s="T5">n</ta>
            <ta e="T7" id="Seg_12190" s="T6">n</ta>
            <ta e="T8" id="Seg_12191" s="T7">v</ta>
            <ta e="T9" id="Seg_12192" s="T8">dempro</ta>
            <ta e="T10" id="Seg_12193" s="T9">n</ta>
            <ta e="T12" id="Seg_12194" s="T11">que</ta>
            <ta e="T13" id="Seg_12195" s="T12">adj</ta>
            <ta e="T16" id="Seg_12196" s="T15">n</ta>
            <ta e="T18" id="Seg_12197" s="T17">dempro</ta>
            <ta e="T19" id="Seg_12198" s="T18">n</ta>
            <ta e="T21" id="Seg_12199" s="T20">que</ta>
            <ta e="T22" id="Seg_12200" s="T21">pers</ta>
            <ta e="T23" id="Seg_12201" s="T22">quant</ta>
            <ta e="T24" id="Seg_12202" s="T23">v</ta>
            <ta e="T25" id="Seg_12203" s="T24">n</ta>
            <ta e="T26" id="Seg_12204" s="T25">pers</ta>
            <ta e="T27" id="Seg_12205" s="T26">v</ta>
            <ta e="T28" id="Seg_12206" s="T27">adj</ta>
            <ta e="T29" id="Seg_12207" s="T28">n</ta>
            <ta e="T30" id="Seg_12208" s="T29">propr</ta>
            <ta e="T31" id="Seg_12209" s="T30">adv</ta>
            <ta e="T32" id="Seg_12210" s="T31">n</ta>
            <ta e="T33" id="Seg_12211" s="T32">v</ta>
            <ta e="T34" id="Seg_12212" s="T33">conj</ta>
            <ta e="T35" id="Seg_12213" s="T34">n</ta>
            <ta e="T36" id="Seg_12214" s="T35">v</ta>
            <ta e="T37" id="Seg_12215" s="T36">adv</ta>
            <ta e="T38" id="Seg_12216" s="T37">quant</ta>
            <ta e="T39" id="Seg_12217" s="T38">n</ta>
            <ta e="T40" id="Seg_12218" s="T39">adj</ta>
            <ta e="T41" id="Seg_12219" s="T40">v</ta>
            <ta e="T42" id="Seg_12220" s="T41">pers</ta>
            <ta e="T43" id="Seg_12221" s="T42">n</ta>
            <ta e="T44" id="Seg_12222" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_12223" s="T44">v</ta>
            <ta e="T46" id="Seg_12224" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_12225" s="T46">v</ta>
            <ta e="T48" id="Seg_12226" s="T47">conj</ta>
            <ta e="T49" id="Seg_12227" s="T48">adv</ta>
            <ta e="T50" id="Seg_12228" s="T49">que</ta>
            <ta e="T51" id="Seg_12229" s="T50">n</ta>
            <ta e="T52" id="Seg_12230" s="T51">v</ta>
            <ta e="T53" id="Seg_12231" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_12232" s="T53">v</ta>
            <ta e="T55" id="Seg_12233" s="T54">conj</ta>
            <ta e="T56" id="Seg_12234" s="T55">quant</ta>
            <ta e="T57" id="Seg_12235" s="T56">v</ta>
            <ta e="T58" id="Seg_12236" s="T57">adv</ta>
            <ta e="T59" id="Seg_12237" s="T58">n</ta>
            <ta e="T60" id="Seg_12238" s="T59">v</ta>
            <ta e="T62" id="Seg_12239" s="T61">adv</ta>
            <ta e="T64" id="Seg_12240" s="T63">que</ta>
            <ta e="T65" id="Seg_12241" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_12242" s="T65">v</ta>
            <ta e="T67" id="Seg_12243" s="T66">que</ta>
            <ta e="T68" id="Seg_12244" s="T67">ptcl</ta>
            <ta e="T70" id="Seg_12245" s="T69">v</ta>
            <ta e="T71" id="Seg_12246" s="T70">quant</ta>
            <ta e="T73" id="Seg_12247" s="T72">n</ta>
            <ta e="T1117" id="Seg_12248" s="T73">v</ta>
            <ta e="T74" id="Seg_12249" s="T1117">v</ta>
            <ta e="T75" id="Seg_12250" s="T74">adv</ta>
            <ta e="T76" id="Seg_12251" s="T75">pers</ta>
            <ta e="T77" id="Seg_12252" s="T76">adv</ta>
            <ta e="T78" id="Seg_12253" s="T77">v</ta>
            <ta e="T79" id="Seg_12254" s="T78">que</ta>
            <ta e="T80" id="Seg_12255" s="T79">v</ta>
            <ta e="T81" id="Seg_12256" s="T80">quant</ta>
            <ta e="T82" id="Seg_12257" s="T81">v</ta>
            <ta e="T83" id="Seg_12258" s="T82">pers</ta>
            <ta e="T84" id="Seg_12259" s="T83">n</ta>
            <ta e="T86" id="Seg_12260" s="T85">adv</ta>
            <ta e="T87" id="Seg_12261" s="T86">quant</ta>
            <ta e="T88" id="Seg_12262" s="T87">n</ta>
            <ta e="T89" id="Seg_12263" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_12264" s="T89">v</ta>
            <ta e="T91" id="Seg_12265" s="T90">quant</ta>
            <ta e="T92" id="Seg_12266" s="T91">n</ta>
            <ta e="T93" id="Seg_12267" s="T92">v</ta>
            <ta e="T95" id="Seg_12268" s="T94">pers</ta>
            <ta e="T96" id="Seg_12269" s="T95">adv</ta>
            <ta e="T97" id="Seg_12270" s="T96">adj</ta>
            <ta e="T98" id="Seg_12271" s="T97">v</ta>
            <ta e="T99" id="Seg_12272" s="T98">conj</ta>
            <ta e="T100" id="Seg_12273" s="T99">que</ta>
            <ta e="T101" id="Seg_12274" s="T100">dempro</ta>
            <ta e="T102" id="Seg_12275" s="T101">adj</ta>
            <ta e="T1118" id="Seg_12276" s="T102">v</ta>
            <ta e="T103" id="Seg_12277" s="T1118">v</ta>
            <ta e="T1119" id="Seg_12278" s="T103">v</ta>
            <ta e="T104" id="Seg_12279" s="T1119">v</ta>
            <ta e="T105" id="Seg_12280" s="T104">adj</ta>
            <ta e="T106" id="Seg_12281" s="T105">n</ta>
            <ta e="T107" id="Seg_12282" s="T106">conj</ta>
            <ta e="T108" id="Seg_12283" s="T107">que</ta>
            <ta e="T109" id="Seg_12284" s="T108">v</ta>
            <ta e="T111" id="Seg_12285" s="T110">adv</ta>
            <ta e="T112" id="Seg_12286" s="T111">dempro</ta>
            <ta e="T113" id="Seg_12287" s="T112">que</ta>
            <ta e="T114" id="Seg_12288" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_12289" s="T114">v</ta>
            <ta e="T118" id="Seg_12290" s="T117">pers</ta>
            <ta e="T119" id="Seg_12291" s="T118">v</ta>
            <ta e="T120" id="Seg_12292" s="T119">n</ta>
            <ta e="T122" id="Seg_12293" s="T121">v</ta>
            <ta e="T123" id="Seg_12294" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_12295" s="T123">pers</ta>
            <ta e="T125" id="Seg_12296" s="T124">v</ta>
            <ta e="T126" id="Seg_12297" s="T125">adv</ta>
            <ta e="T127" id="Seg_12298" s="T126">adj</ta>
            <ta e="T128" id="Seg_12299" s="T127">n</ta>
            <ta e="T129" id="Seg_12300" s="T128">v</ta>
            <ta e="T130" id="Seg_12301" s="T129">n</ta>
            <ta e="T131" id="Seg_12302" s="T130">n</ta>
            <ta e="T132" id="Seg_12303" s="T131">v</ta>
            <ta e="T133" id="Seg_12304" s="T132">v</ta>
            <ta e="T134" id="Seg_12305" s="T133">adv</ta>
            <ta e="T135" id="Seg_12306" s="T134">pers</ta>
            <ta e="T136" id="Seg_12307" s="T135">v</ta>
            <ta e="T137" id="Seg_12308" s="T136">n</ta>
            <ta e="T138" id="Seg_12309" s="T137">n</ta>
            <ta e="T139" id="Seg_12310" s="T138">dempro</ta>
            <ta e="T141" id="Seg_12311" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_12312" s="T141">pers</ta>
            <ta e="T144" id="Seg_12313" s="T143">v</ta>
            <ta e="T146" id="Seg_12314" s="T145">adv</ta>
            <ta e="T147" id="Seg_12315" s="T146">n</ta>
            <ta e="T148" id="Seg_12316" s="T147">n</ta>
            <ta e="T149" id="Seg_12317" s="T148">v</ta>
            <ta e="T150" id="Seg_12318" s="T149">adv</ta>
            <ta e="T152" id="Seg_12319" s="T151">dempro</ta>
            <ta e="T153" id="Seg_12320" s="T152">n</ta>
            <ta e="T154" id="Seg_12321" s="T153">v</ta>
            <ta e="T155" id="Seg_12322" s="T154">dempro</ta>
            <ta e="T156" id="Seg_12323" s="T155">n</ta>
            <ta e="T157" id="Seg_12324" s="T156">adv</ta>
            <ta e="T158" id="Seg_12325" s="T157">dempro</ta>
            <ta e="T159" id="Seg_12326" s="T158">v</ta>
            <ta e="T160" id="Seg_12327" s="T159">v</ta>
            <ta e="T161" id="Seg_12328" s="T160">pers</ta>
            <ta e="T162" id="Seg_12329" s="T161">pers</ta>
            <ta e="T163" id="Seg_12330" s="T162">n</ta>
            <ta e="T164" id="Seg_12331" s="T163">v</ta>
            <ta e="T165" id="Seg_12332" s="T164">adv</ta>
            <ta e="T166" id="Seg_12333" s="T165">conj</ta>
            <ta e="T167" id="Seg_12334" s="T166">pers</ta>
            <ta e="T168" id="Seg_12335" s="T167">v</ta>
            <ta e="T169" id="Seg_12336" s="T168">pers</ta>
            <ta e="T170" id="Seg_12337" s="T169">n</ta>
            <ta e="T171" id="Seg_12338" s="T170">n</ta>
            <ta e="T172" id="Seg_12339" s="T171">v</ta>
            <ta e="T173" id="Seg_12340" s="T172">pers</ta>
            <ta e="T174" id="Seg_12341" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_12342" s="T174">v</ta>
            <ta e="T177" id="Seg_12343" s="T176">que</ta>
            <ta e="T178" id="Seg_12344" s="T177">dempro</ta>
            <ta e="T179" id="Seg_12345" s="T178">pers</ta>
            <ta e="T180" id="Seg_12346" s="T179">v</ta>
            <ta e="T181" id="Seg_12347" s="T180">adv</ta>
            <ta e="T182" id="Seg_12348" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_12349" s="T182">adj</ta>
            <ta e="T184" id="Seg_12350" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_12351" s="T184">dempro</ta>
            <ta e="T186" id="Seg_12352" s="T185">refl</ta>
            <ta e="T187" id="Seg_12353" s="T186">n</ta>
            <ta e="T188" id="Seg_12354" s="T187">n</ta>
            <ta e="T190" id="Seg_12355" s="T189">v</ta>
            <ta e="T191" id="Seg_12356" s="T190">conj</ta>
            <ta e="T192" id="Seg_12357" s="T191">pers</ta>
            <ta e="T193" id="Seg_12358" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_12359" s="T193">v</ta>
            <ta e="T195" id="Seg_12360" s="T194">n</ta>
            <ta e="T196" id="Seg_12361" s="T195">conj</ta>
            <ta e="T198" id="Seg_12362" s="T197">conj</ta>
            <ta e="T199" id="Seg_12363" s="T198">dempro</ta>
            <ta e="T200" id="Seg_12364" s="T199">dempro</ta>
            <ta e="T201" id="Seg_12365" s="T200">adv</ta>
            <ta e="T202" id="Seg_12366" s="T201">v</ta>
            <ta e="T203" id="Seg_12367" s="T202">ptcl</ta>
            <ta e="T205" id="Seg_12368" s="T204">adv</ta>
            <ta e="T206" id="Seg_12369" s="T205">pers</ta>
            <ta e="T208" id="Seg_12370" s="T207">v</ta>
            <ta e="T209" id="Seg_12371" s="T208">propr</ta>
            <ta e="T210" id="Seg_12372" s="T209">dempro</ta>
            <ta e="T211" id="Seg_12373" s="T210">v</ta>
            <ta e="T212" id="Seg_12374" s="T211">propr</ta>
            <ta e="T213" id="Seg_12375" s="T212">v</ta>
            <ta e="T214" id="Seg_12376" s="T213">n</ta>
            <ta e="T215" id="Seg_12377" s="T214">pers</ta>
            <ta e="T216" id="Seg_12378" s="T215">v</ta>
            <ta e="T217" id="Seg_12379" s="T216">num</ta>
            <ta e="T218" id="Seg_12380" s="T217">n</ta>
            <ta e="T221" id="Seg_12381" s="T220">v</ta>
            <ta e="T222" id="Seg_12382" s="T221">num</ta>
            <ta e="T224" id="Seg_12383" s="T223">num</ta>
            <ta e="T225" id="Seg_12384" s="T224">n</ta>
            <ta e="T226" id="Seg_12385" s="T225">num</ta>
            <ta e="T227" id="Seg_12386" s="T226">n</ta>
            <ta e="T228" id="Seg_12387" s="T227">v</ta>
            <ta e="T229" id="Seg_12388" s="T228">dempro</ta>
            <ta e="T231" id="Seg_12389" s="T230">propr</ta>
            <ta e="T232" id="Seg_12390" s="T231">v</ta>
            <ta e="T233" id="Seg_12391" s="T232">v</ta>
            <ta e="T234" id="Seg_12392" s="T233">pers</ta>
            <ta e="T235" id="Seg_12393" s="T234">conj</ta>
            <ta e="T236" id="Seg_12394" s="T235">pers</ta>
            <ta e="T237" id="Seg_12395" s="T236">v</ta>
            <ta e="T238" id="Seg_12396" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_12397" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_12398" s="T239">v</ta>
            <ta e="T241" id="Seg_12399" s="T240">adv</ta>
            <ta e="T242" id="Seg_12400" s="T241">adv</ta>
            <ta e="T243" id="Seg_12401" s="T242">que</ta>
            <ta e="T245" id="Seg_12402" s="T244">v</ta>
            <ta e="T247" id="Seg_12403" s="T246">n</ta>
            <ta e="T248" id="Seg_12404" s="T247">n</ta>
            <ta e="T249" id="Seg_12405" s="T248">n</ta>
            <ta e="T250" id="Seg_12406" s="T249">conj</ta>
            <ta e="T251" id="Seg_12407" s="T250">adv</ta>
            <ta e="T252" id="Seg_12408" s="T251">v</ta>
            <ta e="T253" id="Seg_12409" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_12410" s="T253">pers</ta>
            <ta e="T255" id="Seg_12411" s="T254">v</ta>
            <ta e="T256" id="Seg_12412" s="T255">n</ta>
            <ta e="T258" id="Seg_12413" s="T257">n</ta>
            <ta e="T259" id="Seg_12414" s="T258">v</ta>
            <ta e="T260" id="Seg_12415" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_12416" s="T260">v</ta>
            <ta e="T262" id="Seg_12417" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_12418" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_12419" s="T263">v</ta>
            <ta e="T265" id="Seg_12420" s="T264">adv</ta>
            <ta e="T266" id="Seg_12421" s="T265">adj</ta>
            <ta e="T267" id="Seg_12422" s="T266">v</ta>
            <ta e="T268" id="Seg_12423" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_12424" s="T268">n</ta>
            <ta e="T270" id="Seg_12425" s="T269">adv</ta>
            <ta e="T271" id="Seg_12426" s="T270">n</ta>
            <ta e="T272" id="Seg_12427" s="T271">v</ta>
            <ta e="T273" id="Seg_12428" s="T272">pers</ta>
            <ta e="T274" id="Seg_12429" s="T273">v</ta>
            <ta e="T276" id="Seg_12430" s="T275">num</ta>
            <ta e="T277" id="Seg_12431" s="T276">v</ta>
            <ta e="T279" id="Seg_12432" s="T278">n</ta>
            <ta e="T280" id="Seg_12433" s="T279">v</ta>
            <ta e="T281" id="Seg_12434" s="T280">num</ta>
            <ta e="T282" id="Seg_12435" s="T281">n</ta>
            <ta e="T287" id="Seg_12436" s="T286">num</ta>
            <ta e="T288" id="Seg_12437" s="T287">n</ta>
            <ta e="T289" id="Seg_12438" s="T288">v</ta>
            <ta e="T291" id="Seg_12439" s="T290">v</ta>
            <ta e="T292" id="Seg_12440" s="T291">dempro</ta>
            <ta e="T293" id="Seg_12441" s="T292">n</ta>
            <ta e="T294" id="Seg_12442" s="T293">n</ta>
            <ta e="T295" id="Seg_12443" s="T294">dempro</ta>
            <ta e="T297" id="Seg_12444" s="T296">dempro</ta>
            <ta e="T298" id="Seg_12445" s="T297">n</ta>
            <ta e="T299" id="Seg_12446" s="T298">v</ta>
            <ta e="T300" id="Seg_12447" s="T299">conj</ta>
            <ta e="T301" id="Seg_12448" s="T300">ptcl</ta>
            <ta e="T304" id="Seg_12449" s="T303">v</ta>
            <ta e="T305" id="Seg_12450" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_12451" s="T305">v</ta>
            <ta e="T309" id="Seg_12452" s="T308">dempro</ta>
            <ta e="T310" id="Seg_12453" s="T309">v</ta>
            <ta e="T311" id="Seg_12454" s="T310">conj</ta>
            <ta e="T312" id="Seg_12455" s="T311">v</ta>
            <ta e="T313" id="Seg_12456" s="T312">dempro</ta>
            <ta e="T314" id="Seg_12457" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_12458" s="T314">v</ta>
            <ta e="T316" id="Seg_12459" s="T315">dempro</ta>
            <ta e="T317" id="Seg_12460" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_12461" s="T317">dempro</ta>
            <ta e="T319" id="Seg_12462" s="T318">v</ta>
            <ta e="T320" id="Seg_12463" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_12464" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_12465" s="T321">pers</ta>
            <ta e="T323" id="Seg_12466" s="T322">n</ta>
            <ta e="T324" id="Seg_12467" s="T323">v</ta>
            <ta e="T325" id="Seg_12468" s="T324">conj</ta>
            <ta e="T326" id="Seg_12469" s="T325">n</ta>
            <ta e="T327" id="Seg_12470" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_12471" s="T327">v</ta>
            <ta e="T330" id="Seg_12472" s="T329">n</ta>
            <ta e="T331" id="Seg_12473" s="T330">v</ta>
            <ta e="T332" id="Seg_12474" s="T331">refl</ta>
            <ta e="T333" id="Seg_12475" s="T332">que</ta>
            <ta e="T334" id="Seg_12476" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_12477" s="T334">v</ta>
            <ta e="T336" id="Seg_12478" s="T335">dempro</ta>
            <ta e="T337" id="Seg_12479" s="T336">n</ta>
            <ta e="T339" id="Seg_12480" s="T338">conj</ta>
            <ta e="T340" id="Seg_12481" s="T339">n</ta>
            <ta e="T341" id="Seg_12482" s="T340">v</ta>
            <ta e="T342" id="Seg_12483" s="T341">conj</ta>
            <ta e="T343" id="Seg_12484" s="T342">n</ta>
            <ta e="T345" id="Seg_12485" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_12486" s="T345">pers</ta>
            <ta e="T347" id="Seg_12487" s="T346">v</ta>
            <ta e="T349" id="Seg_12488" s="T348">n</ta>
            <ta e="T350" id="Seg_12489" s="T349">adv</ta>
            <ta e="T351" id="Seg_12490" s="T350">v</ta>
            <ta e="T352" id="Seg_12491" s="T351">v</ta>
            <ta e="T354" id="Seg_12492" s="T353">adv</ta>
            <ta e="T355" id="Seg_12493" s="T354">v</ta>
            <ta e="T356" id="Seg_12494" s="T355">v</ta>
            <ta e="T357" id="Seg_12495" s="T356">n</ta>
            <ta e="T358" id="Seg_12496" s="T357">refl</ta>
            <ta e="T359" id="Seg_12497" s="T358">v</ta>
            <ta e="T360" id="Seg_12498" s="T359">v</ta>
            <ta e="T361" id="Seg_12499" s="T360">v</ta>
            <ta e="T362" id="Seg_12500" s="T361">v</ta>
            <ta e="T363" id="Seg_12501" s="T362">n</ta>
            <ta e="T364" id="Seg_12502" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_12503" s="T364">pers</ta>
            <ta e="T366" id="Seg_12504" s="T365">v</ta>
            <ta e="T367" id="Seg_12505" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_12506" s="T367">v</ta>
            <ta e="T369" id="Seg_12507" s="T368">adv</ta>
            <ta e="T370" id="Seg_12508" s="T369">n</ta>
            <ta e="T371" id="Seg_12509" s="T370">v</ta>
            <ta e="T372" id="Seg_12510" s="T371">dempro</ta>
            <ta e="T373" id="Seg_12511" s="T372">v</ta>
            <ta e="T374" id="Seg_12512" s="T373">v</ta>
            <ta e="T375" id="Seg_12513" s="T374">n</ta>
            <ta e="T376" id="Seg_12514" s="T375">v</ta>
            <ta e="T377" id="Seg_12515" s="T376">conj</ta>
            <ta e="T379" id="Seg_12516" s="T378">n</ta>
            <ta e="T380" id="Seg_12517" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_12518" s="T380">v</ta>
            <ta e="T382" id="Seg_12519" s="T381">n</ta>
            <ta e="T383" id="Seg_12520" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_12521" s="T383">v</ta>
            <ta e="T385" id="Seg_12522" s="T384">conj</ta>
            <ta e="T386" id="Seg_12523" s="T385">n</ta>
            <ta e="T387" id="Seg_12524" s="T386">v</ta>
            <ta e="T388" id="Seg_12525" s="T387">n</ta>
            <ta e="T389" id="Seg_12526" s="T388">n</ta>
            <ta e="T1120" id="Seg_12527" s="T389">v</ta>
            <ta e="T390" id="Seg_12528" s="T1120">v</ta>
            <ta e="T391" id="Seg_12529" s="T390">que</ta>
            <ta e="T392" id="Seg_12530" s="T391">v</ta>
            <ta e="T393" id="Seg_12531" s="T392">v</ta>
            <ta e="T394" id="Seg_12532" s="T393">n</ta>
            <ta e="T395" id="Seg_12533" s="T394">conj</ta>
            <ta e="T396" id="Seg_12534" s="T395">pers</ta>
            <ta e="T397" id="Seg_12535" s="T396">v</ta>
            <ta e="T398" id="Seg_12536" s="T397">conj</ta>
            <ta e="T399" id="Seg_12537" s="T398">n</ta>
            <ta e="T400" id="Seg_12538" s="T399">v</ta>
            <ta e="T401" id="Seg_12539" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_12540" s="T401">adj</ta>
            <ta e="T403" id="Seg_12541" s="T402">pers</ta>
            <ta e="T404" id="Seg_12542" s="T403">n</ta>
            <ta e="T405" id="Seg_12543" s="T404">v</ta>
            <ta e="T406" id="Seg_12544" s="T405">n</ta>
            <ta e="T408" id="Seg_12545" s="T407">n</ta>
            <ta e="T409" id="Seg_12546" s="T408">v</ta>
            <ta e="T410" id="Seg_12547" s="T409">adv</ta>
            <ta e="T411" id="Seg_12548" s="T410">v</ta>
            <ta e="T412" id="Seg_12549" s="T411">v</ta>
            <ta e="T413" id="Seg_12550" s="T412">v</ta>
            <ta e="T414" id="Seg_12551" s="T413">n</ta>
            <ta e="T415" id="Seg_12552" s="T414">dempro</ta>
            <ta e="T416" id="Seg_12553" s="T415">v</ta>
            <ta e="T417" id="Seg_12554" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_12555" s="T417">pers</ta>
            <ta e="T419" id="Seg_12556" s="T418">v</ta>
            <ta e="T420" id="Seg_12557" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_12558" s="T420">v</ta>
            <ta e="T422" id="Seg_12559" s="T421">n</ta>
            <ta e="T423" id="Seg_12560" s="T422">v</ta>
            <ta e="T425" id="Seg_12561" s="T424">dempro</ta>
            <ta e="T426" id="Seg_12562" s="T425">v</ta>
            <ta e="T427" id="Seg_12563" s="T426">v</ta>
            <ta e="T428" id="Seg_12564" s="T427">n</ta>
            <ta e="T429" id="Seg_12565" s="T428">n</ta>
            <ta e="T430" id="Seg_12566" s="T429">v</ta>
            <ta e="T431" id="Seg_12567" s="T430">conj</ta>
            <ta e="T432" id="Seg_12568" s="T431">n</ta>
            <ta e="T433" id="Seg_12569" s="T432">v</ta>
            <ta e="T434" id="Seg_12570" s="T433">que</ta>
            <ta e="T435" id="Seg_12571" s="T434">v</ta>
            <ta e="T436" id="Seg_12572" s="T435">adv</ta>
            <ta e="T437" id="Seg_12573" s="T436">n</ta>
            <ta e="T438" id="Seg_12574" s="T437">v</ta>
            <ta e="T439" id="Seg_12575" s="T438">conj</ta>
            <ta e="T440" id="Seg_12576" s="T439">pers</ta>
            <ta e="T441" id="Seg_12577" s="T440">v</ta>
            <ta e="T442" id="Seg_12578" s="T441">adv</ta>
            <ta e="T443" id="Seg_12579" s="T442">n</ta>
            <ta e="T444" id="Seg_12580" s="T443">adv</ta>
            <ta e="T445" id="Seg_12581" s="T444">v</ta>
            <ta e="T446" id="Seg_12582" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_12583" s="T446">adj</ta>
            <ta e="T448" id="Seg_12584" s="T447">n</ta>
            <ta e="T449" id="Seg_12585" s="T448">v</ta>
            <ta e="T450" id="Seg_12586" s="T449">n</ta>
            <ta e="T452" id="Seg_12587" s="T451">v</ta>
            <ta e="T454" id="Seg_12588" s="T453">adv</ta>
            <ta e="T455" id="Seg_12589" s="T454">v</ta>
            <ta e="T456" id="Seg_12590" s="T455">v</ta>
            <ta e="T457" id="Seg_12591" s="T456">v</ta>
            <ta e="T458" id="Seg_12592" s="T457">n</ta>
            <ta e="T459" id="Seg_12593" s="T458">v</ta>
            <ta e="T460" id="Seg_12594" s="T459">n</ta>
            <ta e="T461" id="Seg_12595" s="T460">n</ta>
            <ta e="T462" id="Seg_12596" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_12597" s="T462">pers</ta>
            <ta e="T464" id="Seg_12598" s="T463">v</ta>
            <ta e="T465" id="Seg_12599" s="T464">ptcl</ta>
            <ta e="T466" id="Seg_12600" s="T465">adv</ta>
            <ta e="T467" id="Seg_12601" s="T466">n</ta>
            <ta e="T468" id="Seg_12602" s="T467">v</ta>
            <ta e="T470" id="Seg_12603" s="T469">v</ta>
            <ta e="T471" id="Seg_12604" s="T470">v</ta>
            <ta e="T472" id="Seg_12605" s="T471">n</ta>
            <ta e="T473" id="Seg_12606" s="T472">n</ta>
            <ta e="T474" id="Seg_12607" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_12608" s="T474">v</ta>
            <ta e="T476" id="Seg_12609" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_12610" s="T476">conj</ta>
            <ta e="T478" id="Seg_12611" s="T477">n</ta>
            <ta e="T479" id="Seg_12612" s="T478">que</ta>
            <ta e="T480" id="Seg_12613" s="T479">v</ta>
            <ta e="T481" id="Seg_12614" s="T480">n</ta>
            <ta e="T482" id="Seg_12615" s="T481">v</ta>
            <ta e="T483" id="Seg_12616" s="T482">conj</ta>
            <ta e="T484" id="Seg_12617" s="T483">pers</ta>
            <ta e="T485" id="Seg_12618" s="T484">v</ta>
            <ta e="T486" id="Seg_12619" s="T485">conj</ta>
            <ta e="T487" id="Seg_12620" s="T486">dempro</ta>
            <ta e="T488" id="Seg_12621" s="T487">v</ta>
            <ta e="T489" id="Seg_12622" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_12623" s="T489">adj</ta>
            <ta e="T491" id="Seg_12624" s="T490">n</ta>
            <ta e="T492" id="Seg_12625" s="T491">v</ta>
            <ta e="T493" id="Seg_12626" s="T492">n</ta>
            <ta e="T494" id="Seg_12627" s="T493">v</ta>
            <ta e="T495" id="Seg_12628" s="T494">adv</ta>
            <ta e="T496" id="Seg_12629" s="T495">v</ta>
            <ta e="T497" id="Seg_12630" s="T496">v</ta>
            <ta e="T499" id="Seg_12631" s="T498">quant</ta>
            <ta e="T500" id="Seg_12632" s="T499">n</ta>
            <ta e="T501" id="Seg_12633" s="T500">v</ta>
            <ta e="T502" id="Seg_12634" s="T501">v</ta>
            <ta e="T503" id="Seg_12635" s="T502">adv</ta>
            <ta e="T504" id="Seg_12636" s="T503">n</ta>
            <ta e="T505" id="Seg_12637" s="T504">v</ta>
            <ta e="T506" id="Seg_12638" s="T505">v</ta>
            <ta e="T507" id="Seg_12639" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_12640" s="T507">n</ta>
            <ta e="T509" id="Seg_12641" s="T508">v</ta>
            <ta e="T512" id="Seg_12642" s="T511">n</ta>
            <ta e="T513" id="Seg_12643" s="T512">v</ta>
            <ta e="T514" id="Seg_12644" s="T513">n</ta>
            <ta e="T515" id="Seg_12645" s="T514">adv</ta>
            <ta e="T516" id="Seg_12646" s="T515">dempro</ta>
            <ta e="T517" id="Seg_12647" s="T516">v</ta>
            <ta e="T518" id="Seg_12648" s="T517">n</ta>
            <ta e="T519" id="Seg_12649" s="T518">n</ta>
            <ta e="T520" id="Seg_12650" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_12651" s="T520">v</ta>
            <ta e="T522" id="Seg_12652" s="T521">dempro</ta>
            <ta e="T523" id="Seg_12653" s="T522">v</ta>
            <ta e="T524" id="Seg_12654" s="T523">dempro</ta>
            <ta e="T525" id="Seg_12655" s="T524">n</ta>
            <ta e="T526" id="Seg_12656" s="T525">v</ta>
            <ta e="T527" id="Seg_12657" s="T526">v</ta>
            <ta e="T528" id="Seg_12658" s="T527">n</ta>
            <ta e="T529" id="Seg_12659" s="T528">v</ta>
            <ta e="T530" id="Seg_12660" s="T529">conj</ta>
            <ta e="T531" id="Seg_12661" s="T530">n</ta>
            <ta e="T532" id="Seg_12662" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_12663" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_12664" s="T533">v</ta>
            <ta e="T535" id="Seg_12665" s="T534">conj</ta>
            <ta e="T536" id="Seg_12666" s="T535">n</ta>
            <ta e="T537" id="Seg_12667" s="T536">v</ta>
            <ta e="T538" id="Seg_12668" s="T537">v</ta>
            <ta e="T539" id="Seg_12669" s="T538">pers</ta>
            <ta e="T540" id="Seg_12670" s="T539">v</ta>
            <ta e="T541" id="Seg_12671" s="T540">v</ta>
            <ta e="T542" id="Seg_12672" s="T541">conj</ta>
            <ta e="T543" id="Seg_12673" s="T542">v</ta>
            <ta e="T544" id="Seg_12674" s="T543">pers</ta>
            <ta e="T545" id="Seg_12675" s="T544">v</ta>
            <ta e="T546" id="Seg_12676" s="T545">pers</ta>
            <ta e="T548" id="Seg_12677" s="T547">pers</ta>
            <ta e="T549" id="Seg_12678" s="T548">v</ta>
            <ta e="T550" id="Seg_12679" s="T549">quant</ta>
            <ta e="T552" id="Seg_12680" s="T551">num</ta>
            <ta e="T553" id="Seg_12681" s="T552">n</ta>
            <ta e="T554" id="Seg_12682" s="T553">v</ta>
            <ta e="T555" id="Seg_12683" s="T554">v</ta>
            <ta e="T557" id="Seg_12684" s="T556">n</ta>
            <ta e="T558" id="Seg_12685" s="T557">v</ta>
            <ta e="T559" id="Seg_12686" s="T558">conj</ta>
            <ta e="T560" id="Seg_12687" s="T559">v</ta>
            <ta e="T561" id="Seg_12688" s="T560">dempro</ta>
            <ta e="T562" id="Seg_12689" s="T561">n</ta>
            <ta e="T563" id="Seg_12690" s="T562">v</ta>
            <ta e="T564" id="Seg_12691" s="T563">pers</ta>
            <ta e="T565" id="Seg_12692" s="T564">adv</ta>
            <ta e="T566" id="Seg_12693" s="T565">pers</ta>
            <ta e="T567" id="Seg_12694" s="T566">v</ta>
            <ta e="T568" id="Seg_12695" s="T567">pers</ta>
            <ta e="T569" id="Seg_12696" s="T568">aux</ta>
            <ta e="T570" id="Seg_12697" s="T569">v</ta>
            <ta e="T571" id="Seg_12698" s="T570">pers</ta>
            <ta e="T572" id="Seg_12699" s="T571">adv</ta>
            <ta e="T573" id="Seg_12700" s="T572">n</ta>
            <ta e="T574" id="Seg_12701" s="T573">v</ta>
            <ta e="T575" id="Seg_12702" s="T574">v</ta>
            <ta e="T576" id="Seg_12703" s="T575">pers</ta>
            <ta e="T577" id="Seg_12704" s="T576">dempro</ta>
            <ta e="T578" id="Seg_12705" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_12706" s="T578">v</ta>
            <ta e="T580" id="Seg_12707" s="T579">v</ta>
            <ta e="T581" id="Seg_12708" s="T580">pers</ta>
            <ta e="T582" id="Seg_12709" s="T581">n</ta>
            <ta e="T583" id="Seg_12710" s="T582">conj</ta>
            <ta e="T584" id="Seg_12711" s="T583">v</ta>
            <ta e="T585" id="Seg_12712" s="T584">conj</ta>
            <ta e="T586" id="Seg_12713" s="T585">dempro</ta>
            <ta e="T587" id="Seg_12714" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_12715" s="T587">v</ta>
            <ta e="T589" id="Seg_12716" s="T588">pers</ta>
            <ta e="T590" id="Seg_12717" s="T589">conj</ta>
            <ta e="T591" id="Seg_12718" s="T590">dempro</ta>
            <ta e="T592" id="Seg_12719" s="T591">v</ta>
            <ta e="T593" id="Seg_12720" s="T592">ptcl</ta>
            <ta e="T594" id="Seg_12721" s="T593">v</ta>
            <ta e="T595" id="Seg_12722" s="T594">ptcl</ta>
            <ta e="T596" id="Seg_12723" s="T595">v</ta>
            <ta e="T597" id="Seg_12724" s="T596">adv</ta>
            <ta e="T598" id="Seg_12725" s="T597">dempro</ta>
            <ta e="T599" id="Seg_12726" s="T598">dempro</ta>
            <ta e="T600" id="Seg_12727" s="T599">v</ta>
            <ta e="T601" id="Seg_12728" s="T600">v</ta>
            <ta e="T602" id="Seg_12729" s="T601">dempro</ta>
            <ta e="T603" id="Seg_12730" s="T602">v</ta>
            <ta e="T604" id="Seg_12731" s="T603">dempro</ta>
            <ta e="T605" id="Seg_12732" s="T604">conj</ta>
            <ta e="T606" id="Seg_12733" s="T605">adv</ta>
            <ta e="T607" id="Seg_12734" s="T606">ptcl</ta>
            <ta e="T609" id="Seg_12735" s="T608">n</ta>
            <ta e="T610" id="Seg_12736" s="T609">v</ta>
            <ta e="T611" id="Seg_12737" s="T610">conj</ta>
            <ta e="T612" id="Seg_12738" s="T611">adv</ta>
            <ta e="T613" id="Seg_12739" s="T612">dempro</ta>
            <ta e="T614" id="Seg_12740" s="T613">n</ta>
            <ta e="T615" id="Seg_12741" s="T614">dempro</ta>
            <ta e="T616" id="Seg_12742" s="T615">n</ta>
            <ta e="T617" id="Seg_12743" s="T616">v</ta>
            <ta e="T618" id="Seg_12744" s="T617">conj</ta>
            <ta e="T619" id="Seg_12745" s="T618">n</ta>
            <ta e="T620" id="Seg_12746" s="T619">v</ta>
            <ta e="T622" id="Seg_12747" s="T621">n</ta>
            <ta e="T623" id="Seg_12748" s="T622">n</ta>
            <ta e="T624" id="Seg_12749" s="T623">ptcl</ta>
            <ta e="T625" id="Seg_12750" s="T624">n</ta>
            <ta e="T627" id="Seg_12751" s="T626">v</ta>
            <ta e="T628" id="Seg_12752" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_12753" s="T628">v</ta>
            <ta e="T631" id="Seg_12754" s="T630">pers</ta>
            <ta e="T632" id="Seg_12755" s="T631">n</ta>
            <ta e="T633" id="Seg_12756" s="T632">conj</ta>
            <ta e="T634" id="Seg_12757" s="T633">pers</ta>
            <ta e="T635" id="Seg_12758" s="T634">n</ta>
            <ta e="T636" id="Seg_12759" s="T635">adv</ta>
            <ta e="T637" id="Seg_12760" s="T636">v</ta>
            <ta e="T638" id="Seg_12761" s="T637">dempro</ta>
            <ta e="T639" id="Seg_12762" s="T638">v</ta>
            <ta e="T640" id="Seg_12763" s="T639">n</ta>
            <ta e="T641" id="Seg_12764" s="T640">v</ta>
            <ta e="T642" id="Seg_12765" s="T641">n</ta>
            <ta e="T643" id="Seg_12766" s="T642">conj</ta>
            <ta e="T644" id="Seg_12767" s="T643">n</ta>
            <ta e="T645" id="Seg_12768" s="T644">n</ta>
            <ta e="T646" id="Seg_12769" s="T645">adv</ta>
            <ta e="T647" id="Seg_12770" s="T646">n</ta>
            <ta e="T648" id="Seg_12771" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_12772" s="T648">n</ta>
            <ta e="T650" id="Seg_12773" s="T649">pers</ta>
            <ta e="T651" id="Seg_12774" s="T650">v</ta>
            <ta e="T652" id="Seg_12775" s="T651">adv</ta>
            <ta e="T653" id="Seg_12776" s="T652">adv</ta>
            <ta e="T654" id="Seg_12777" s="T653">adj</ta>
            <ta e="T655" id="Seg_12778" s="T654">v</ta>
            <ta e="T656" id="Seg_12779" s="T655">n</ta>
            <ta e="T657" id="Seg_12780" s="T656">v</ta>
            <ta e="T658" id="Seg_12781" s="T657">n</ta>
            <ta e="T659" id="Seg_12782" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_12783" s="T659">adv</ta>
            <ta e="T661" id="Seg_12784" s="T660">v</ta>
            <ta e="T662" id="Seg_12785" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_12786" s="T662">n</ta>
            <ta e="T664" id="Seg_12787" s="T663">pers</ta>
            <ta e="T665" id="Seg_12788" s="T664">pers</ta>
            <ta e="T666" id="Seg_12789" s="T665">v</ta>
            <ta e="T667" id="Seg_12790" s="T666">n</ta>
            <ta e="T668" id="Seg_12791" s="T667">conj</ta>
            <ta e="T669" id="Seg_12792" s="T668">pers</ta>
            <ta e="T670" id="Seg_12793" s="T669">n</ta>
            <ta e="T671" id="Seg_12794" s="T670">adv</ta>
            <ta e="T672" id="Seg_12795" s="T671">n</ta>
            <ta e="T674" id="Seg_12796" s="T672">n</ta>
            <ta e="T675" id="Seg_12797" s="T674">n</ta>
            <ta e="T676" id="Seg_12798" s="T675">n</ta>
            <ta e="T677" id="Seg_12799" s="T676">v</ta>
            <ta e="T678" id="Seg_12800" s="T677">dempro</ta>
            <ta e="T679" id="Seg_12801" s="T678">v</ta>
            <ta e="T680" id="Seg_12802" s="T679">adj</ta>
            <ta e="T681" id="Seg_12803" s="T680">adv</ta>
            <ta e="T682" id="Seg_12804" s="T681">v</ta>
            <ta e="T683" id="Seg_12805" s="T682">dempro</ta>
            <ta e="T684" id="Seg_12806" s="T683">n</ta>
            <ta e="T685" id="Seg_12807" s="T684">v</ta>
            <ta e="T686" id="Seg_12808" s="T685">n</ta>
            <ta e="T687" id="Seg_12809" s="T686">conj</ta>
            <ta e="T688" id="Seg_12810" s="T687">n</ta>
            <ta e="T689" id="Seg_12811" s="T688">n</ta>
            <ta e="T690" id="Seg_12812" s="T689">conj</ta>
            <ta e="T691" id="Seg_12813" s="T690">adv</ta>
            <ta e="T693" id="Seg_12814" s="T692">dempro</ta>
            <ta e="T694" id="Seg_12815" s="T693">ptcl</ta>
            <ta e="T695" id="Seg_12816" s="T694">n</ta>
            <ta e="T696" id="Seg_12817" s="T695">v</ta>
            <ta e="T698" id="Seg_12818" s="T697">v</ta>
            <ta e="T699" id="Seg_12819" s="T698">n</ta>
            <ta e="T700" id="Seg_12820" s="T699">conj</ta>
            <ta e="T701" id="Seg_12821" s="T700">n</ta>
            <ta e="T702" id="Seg_12822" s="T701">dempro</ta>
            <ta e="T703" id="Seg_12823" s="T702">n</ta>
            <ta e="T704" id="Seg_12824" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_12825" s="T704">v</ta>
            <ta e="T706" id="Seg_12826" s="T705">num</ta>
            <ta e="T707" id="Seg_12827" s="T706">n</ta>
            <ta e="T708" id="Seg_12828" s="T707">ptcl</ta>
            <ta e="T709" id="Seg_12829" s="T708">v</ta>
            <ta e="T710" id="Seg_12830" s="T709">adv</ta>
            <ta e="T711" id="Seg_12831" s="T710">adj</ta>
            <ta e="T712" id="Seg_12832" s="T711">n</ta>
            <ta e="T713" id="Seg_12833" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_12834" s="T713">que</ta>
            <ta e="T715" id="Seg_12835" s="T714">ptcl</ta>
            <ta e="T719" id="Seg_12836" s="T718">que</ta>
            <ta e="T720" id="Seg_12837" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_12838" s="T720">v</ta>
            <ta e="T722" id="Seg_12839" s="T721">n</ta>
            <ta e="T723" id="Seg_12840" s="T722">v</ta>
            <ta e="T724" id="Seg_12841" s="T723">quant</ta>
            <ta e="T725" id="Seg_12842" s="T724">v</ta>
            <ta e="T726" id="Seg_12843" s="T725">adv</ta>
            <ta e="T727" id="Seg_12844" s="T726">v</ta>
            <ta e="T728" id="Seg_12845" s="T727">adj</ta>
            <ta e="T729" id="Seg_12846" s="T728">n</ta>
            <ta e="T730" id="Seg_12847" s="T729">conj</ta>
            <ta e="T731" id="Seg_12848" s="T730">n</ta>
            <ta e="T732" id="Seg_12849" s="T731">adv</ta>
            <ta e="T733" id="Seg_12850" s="T732">v</ta>
            <ta e="T734" id="Seg_12851" s="T733">v</ta>
            <ta e="T735" id="Seg_12852" s="T734">v</ta>
            <ta e="T737" id="Seg_12853" s="T736">pers</ta>
            <ta e="T738" id="Seg_12854" s="T737">adv</ta>
            <ta e="T739" id="Seg_12855" s="T738">n</ta>
            <ta e="T740" id="Seg_12856" s="T739">v</ta>
            <ta e="T741" id="Seg_12857" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_12858" s="T741">v</ta>
            <ta e="T744" id="Seg_12859" s="T743">v</ta>
            <ta e="T745" id="Seg_12860" s="T744">v</ta>
            <ta e="T746" id="Seg_12861" s="T745">adv</ta>
            <ta e="T747" id="Seg_12862" s="T746">ptcl</ta>
            <ta e="T748" id="Seg_12863" s="T747">v</ta>
            <ta e="T749" id="Seg_12864" s="T748">n</ta>
            <ta e="T751" id="Seg_12865" s="T749">v</ta>
            <ta e="T752" id="Seg_12866" s="T751">n</ta>
            <ta e="T753" id="Seg_12867" s="T752">v</ta>
            <ta e="T754" id="Seg_12868" s="T753">n</ta>
            <ta e="T755" id="Seg_12869" s="T754">n</ta>
            <ta e="T756" id="Seg_12870" s="T755">n</ta>
            <ta e="T757" id="Seg_12871" s="T756">n</ta>
            <ta e="T758" id="Seg_12872" s="T757">adv</ta>
            <ta e="T759" id="Seg_12873" s="T758">n</ta>
            <ta e="T760" id="Seg_12874" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_12875" s="T760">n</ta>
            <ta e="T762" id="Seg_12876" s="T761">n</ta>
            <ta e="T763" id="Seg_12877" s="T762">v</ta>
            <ta e="T764" id="Seg_12878" s="T763">adv</ta>
            <ta e="T765" id="Seg_12879" s="T764">dempro</ta>
            <ta e="T766" id="Seg_12880" s="T765">n</ta>
            <ta e="T767" id="Seg_12881" s="T766">v</ta>
            <ta e="T768" id="Seg_12882" s="T767">adv</ta>
            <ta e="T769" id="Seg_12883" s="T768">n</ta>
            <ta e="T770" id="Seg_12884" s="T769">v</ta>
            <ta e="T772" id="Seg_12885" s="T771">v</ta>
            <ta e="T773" id="Seg_12886" s="T772">n</ta>
            <ta e="T774" id="Seg_12887" s="T773">conj</ta>
            <ta e="T775" id="Seg_12888" s="T774">dempro</ta>
            <ta e="T776" id="Seg_12889" s="T775">dempro</ta>
            <ta e="T778" id="Seg_12890" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_12891" s="T778">dempro</ta>
            <ta e="T780" id="Seg_12892" s="T779">v</ta>
            <ta e="T781" id="Seg_12893" s="T780">n</ta>
            <ta e="T782" id="Seg_12894" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_12895" s="T782">adv</ta>
            <ta e="T784" id="Seg_12896" s="T783">dempro</ta>
            <ta e="T785" id="Seg_12897" s="T784">adj</ta>
            <ta e="T786" id="Seg_12898" s="T785">v</ta>
            <ta e="T787" id="Seg_12899" s="T786">n</ta>
            <ta e="T789" id="Seg_12900" s="T788">quant</ta>
            <ta e="T790" id="Seg_12901" s="T789">v</ta>
            <ta e="T791" id="Seg_12902" s="T790">dempro</ta>
            <ta e="T793" id="Seg_12903" s="T792">adv</ta>
            <ta e="T794" id="Seg_12904" s="T793">dempro</ta>
            <ta e="T795" id="Seg_12905" s="T794">n</ta>
            <ta e="T796" id="Seg_12906" s="T795">v</ta>
            <ta e="T797" id="Seg_12907" s="T796">adj</ta>
            <ta e="T798" id="Seg_12908" s="T797">adj</ta>
            <ta e="T799" id="Seg_12909" s="T798">adv</ta>
            <ta e="T800" id="Seg_12910" s="T799">n</ta>
            <ta e="T801" id="Seg_12911" s="T800">v</ta>
            <ta e="T803" id="Seg_12912" s="T802">adv</ta>
            <ta e="T804" id="Seg_12913" s="T803">adj</ta>
            <ta e="T806" id="Seg_12914" s="T805">adv</ta>
            <ta e="T807" id="Seg_12915" s="T806">dempro</ta>
            <ta e="T808" id="Seg_12916" s="T807">adv</ta>
            <ta e="T809" id="Seg_12917" s="T808">adj</ta>
            <ta e="T810" id="Seg_12918" s="T809">v</ta>
            <ta e="T811" id="Seg_12919" s="T810">conj</ta>
            <ta e="T812" id="Seg_12920" s="T811">dempro</ta>
            <ta e="T813" id="Seg_12921" s="T812">n</ta>
            <ta e="T814" id="Seg_12922" s="T813">v</ta>
            <ta e="T815" id="Seg_12923" s="T814">n</ta>
            <ta e="T816" id="Seg_12924" s="T815">v</ta>
            <ta e="T817" id="Seg_12925" s="T816">dempro</ta>
            <ta e="T818" id="Seg_12926" s="T817">ptcl</ta>
            <ta e="T819" id="Seg_12927" s="T818">v</ta>
            <ta e="T820" id="Seg_12928" s="T819">conj</ta>
            <ta e="T821" id="Seg_12929" s="T820">n</ta>
            <ta e="T823" id="Seg_12930" s="T822">n</ta>
            <ta e="T824" id="Seg_12931" s="T823">v</ta>
            <ta e="T825" id="Seg_12932" s="T824">que</ta>
            <ta e="T826" id="Seg_12933" s="T825">pers</ta>
            <ta e="T827" id="Seg_12934" s="T826">v</ta>
            <ta e="T828" id="Seg_12935" s="T827">conj</ta>
            <ta e="T829" id="Seg_12936" s="T828">refl</ta>
            <ta e="T830" id="Seg_12937" s="T829">ptcl</ta>
            <ta e="T832" id="Seg_12938" s="T831">n</ta>
            <ta e="T833" id="Seg_12939" s="T832">v</ta>
            <ta e="T834" id="Seg_12940" s="T833">n</ta>
            <ta e="T835" id="Seg_12941" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_12942" s="T835">adv</ta>
            <ta e="T838" id="Seg_12943" s="T837">adv</ta>
            <ta e="T839" id="Seg_12944" s="T838">n</ta>
            <ta e="T840" id="Seg_12945" s="T839">v</ta>
            <ta e="T842" id="Seg_12946" s="T841">n</ta>
            <ta e="T843" id="Seg_12947" s="T842">v</ta>
            <ta e="T844" id="Seg_12948" s="T843">n</ta>
            <ta e="T845" id="Seg_12949" s="T844">adv</ta>
            <ta e="T846" id="Seg_12950" s="T845">v</ta>
            <ta e="T847" id="Seg_12951" s="T846">n</ta>
            <ta e="T848" id="Seg_12952" s="T847">v</ta>
            <ta e="T849" id="Seg_12953" s="T848">pers</ta>
            <ta e="T850" id="Seg_12954" s="T849">v</ta>
            <ta e="T851" id="Seg_12955" s="T850">n</ta>
            <ta e="T852" id="Seg_12956" s="T851">n</ta>
            <ta e="T855" id="Seg_12957" s="T854">v</ta>
            <ta e="T856" id="Seg_12958" s="T855">v</ta>
            <ta e="T857" id="Seg_12959" s="T856">que</ta>
            <ta e="T858" id="Seg_12960" s="T857">v</ta>
            <ta e="T859" id="Seg_12961" s="T858">ptcl</ta>
            <ta e="T860" id="Seg_12962" s="T859">pers</ta>
            <ta e="T861" id="Seg_12963" s="T860">aux</ta>
            <ta e="T862" id="Seg_12964" s="T861">v</ta>
            <ta e="T863" id="Seg_12965" s="T862">n</ta>
            <ta e="T864" id="Seg_12966" s="T863">n</ta>
            <ta e="T865" id="Seg_12967" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_12968" s="T865">v</ta>
            <ta e="T868" id="Seg_12969" s="T867">n</ta>
            <ta e="T869" id="Seg_12970" s="T868">v</ta>
            <ta e="T870" id="Seg_12971" s="T869">n</ta>
            <ta e="T871" id="Seg_12972" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_12973" s="T871">n</ta>
            <ta e="T874" id="Seg_12974" s="T873">v</ta>
            <ta e="T875" id="Seg_12975" s="T874">adv</ta>
            <ta e="T876" id="Seg_12976" s="T875">dempro</ta>
            <ta e="T877" id="Seg_12977" s="T876">v</ta>
            <ta e="T878" id="Seg_12978" s="T877">quant</ta>
            <ta e="T879" id="Seg_12979" s="T878">n</ta>
            <ta e="T880" id="Seg_12980" s="T879">v</ta>
            <ta e="T881" id="Seg_12981" s="T880">n</ta>
            <ta e="T883" id="Seg_12982" s="T882">v</ta>
            <ta e="T884" id="Seg_12983" s="T883">conj</ta>
            <ta e="T885" id="Seg_12984" s="T884">dempro</ta>
            <ta e="T886" id="Seg_12985" s="T885">v</ta>
            <ta e="T890" id="Seg_12986" s="T889">adv</ta>
            <ta e="T891" id="Seg_12987" s="T890">dempro</ta>
            <ta e="T892" id="Seg_12988" s="T891">v</ta>
            <ta e="T893" id="Seg_12989" s="T892">n</ta>
            <ta e="T894" id="Seg_12990" s="T893">adv</ta>
            <ta e="T895" id="Seg_12991" s="T894">adj</ta>
            <ta e="T896" id="Seg_12992" s="T895">conj</ta>
            <ta e="T897" id="Seg_12993" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_12994" s="T897">v</ta>
            <ta e="T899" id="Seg_12995" s="T898">n</ta>
            <ta e="T900" id="Seg_12996" s="T899">v</ta>
            <ta e="T901" id="Seg_12997" s="T900">que</ta>
            <ta e="T902" id="Seg_12998" s="T901">pers</ta>
            <ta e="T903" id="Seg_12999" s="T902">ptcl</ta>
            <ta e="T904" id="Seg_13000" s="T903">v</ta>
            <ta e="T905" id="Seg_13001" s="T904">conj</ta>
            <ta e="T906" id="Seg_13002" s="T905">dempro</ta>
            <ta e="T907" id="Seg_13003" s="T906">v</ta>
            <ta e="T908" id="Seg_13004" s="T907">conj</ta>
            <ta e="T909" id="Seg_13005" s="T908">adv</ta>
            <ta e="T910" id="Seg_13006" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_13007" s="T910">dempro</ta>
            <ta e="T912" id="Seg_13008" s="T911">v</ta>
            <ta e="T913" id="Seg_13009" s="T912">conj</ta>
            <ta e="T914" id="Seg_13010" s="T913">dempro</ta>
            <ta e="T915" id="Seg_13011" s="T914">v</ta>
            <ta e="T917" id="Seg_13012" s="T916">v</ta>
            <ta e="T918" id="Seg_13013" s="T917">conj</ta>
            <ta e="T919" id="Seg_13014" s="T918">ptcl</ta>
            <ta e="T920" id="Seg_13015" s="T919">n</ta>
            <ta e="T1121" id="Seg_13016" s="T920">v</ta>
            <ta e="T921" id="Seg_13017" s="T1121">v</ta>
            <ta e="T922" id="Seg_13018" s="T921">adv</ta>
            <ta e="T923" id="Seg_13019" s="T922">ptcl</ta>
            <ta e="T926" id="Seg_13020" s="T925">n</ta>
            <ta e="T927" id="Seg_13021" s="T926">n</ta>
            <ta e="T928" id="Seg_13022" s="T927">pers</ta>
            <ta e="T929" id="Seg_13023" s="T928">adv</ta>
            <ta e="T930" id="Seg_13024" s="T929">adj</ta>
            <ta e="T931" id="Seg_13025" s="T930">adj</ta>
            <ta e="T932" id="Seg_13026" s="T931">n</ta>
            <ta e="T933" id="Seg_13027" s="T932">v</ta>
            <ta e="T934" id="Seg_13028" s="T933">adv</ta>
            <ta e="T935" id="Seg_13029" s="T934">v</ta>
            <ta e="T936" id="Seg_13030" s="T935">v</ta>
            <ta e="T937" id="Seg_13031" s="T936">pers</ta>
            <ta e="T938" id="Seg_13032" s="T937">n</ta>
            <ta e="T939" id="Seg_13033" s="T938">v</ta>
            <ta e="T941" id="Seg_13034" s="T940">n</ta>
            <ta e="T942" id="Seg_13035" s="T941">v</ta>
            <ta e="T943" id="Seg_13036" s="T942">v</ta>
            <ta e="T944" id="Seg_13037" s="T943">v</ta>
            <ta e="T945" id="Seg_13038" s="T944">v</ta>
            <ta e="T946" id="Seg_13039" s="T945">que</ta>
            <ta e="T947" id="Seg_13040" s="T946">v</ta>
            <ta e="T948" id="Seg_13041" s="T947">ptcl</ta>
            <ta e="T949" id="Seg_13042" s="T948">v</ta>
            <ta e="T951" id="Seg_13043" s="T950">n</ta>
            <ta e="T952" id="Seg_13044" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_13045" s="T952">v</ta>
            <ta e="T954" id="Seg_13046" s="T953">adv</ta>
            <ta e="T955" id="Seg_13047" s="T954">v</ta>
            <ta e="T956" id="Seg_13048" s="T955">adv</ta>
            <ta e="T957" id="Seg_13049" s="T956">v</ta>
            <ta e="T958" id="Seg_13050" s="T957">n</ta>
            <ta e="T959" id="Seg_13051" s="T958">v</ta>
            <ta e="T960" id="Seg_13052" s="T959">v</ta>
            <ta e="T961" id="Seg_13053" s="T960">dempro</ta>
            <ta e="T962" id="Seg_13054" s="T961">v</ta>
            <ta e="T963" id="Seg_13055" s="T962">n</ta>
            <ta e="T964" id="Seg_13056" s="T963">adv</ta>
            <ta e="T965" id="Seg_13057" s="T964">v</ta>
            <ta e="T967" id="Seg_13058" s="T966">v</ta>
            <ta e="T968" id="Seg_13059" s="T967">n</ta>
            <ta e="T969" id="Seg_13060" s="T968">v</ta>
            <ta e="T970" id="Seg_13061" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_13062" s="T970">ptcl</ta>
            <ta e="T972" id="Seg_13063" s="T971">n</ta>
            <ta e="T974" id="Seg_13064" s="T973">ptcl</ta>
            <ta e="T975" id="Seg_13065" s="T974">v</ta>
            <ta e="T976" id="Seg_13066" s="T975">adv</ta>
            <ta e="T977" id="Seg_13067" s="T976">adv</ta>
            <ta e="T978" id="Seg_13068" s="T977">n</ta>
            <ta e="T979" id="Seg_13069" s="T978">n</ta>
            <ta e="T980" id="Seg_13070" s="T979">v</ta>
            <ta e="T981" id="Seg_13071" s="T980">dempro</ta>
            <ta e="T983" id="Seg_13072" s="T982">v</ta>
            <ta e="T984" id="Seg_13073" s="T983">n</ta>
            <ta e="T986" id="Seg_13074" s="T985">v</ta>
            <ta e="T987" id="Seg_13075" s="T986">v</ta>
            <ta e="T988" id="Seg_13076" s="T987">n</ta>
            <ta e="T989" id="Seg_13077" s="T988">num</ta>
            <ta e="T990" id="Seg_13078" s="T989">n</ta>
            <ta e="T991" id="Seg_13079" s="T990">n</ta>
            <ta e="T992" id="Seg_13080" s="T991">v</ta>
            <ta e="T993" id="Seg_13081" s="T992">pers</ta>
            <ta e="T995" id="Seg_13082" s="T994">n</ta>
            <ta e="T996" id="Seg_13083" s="T995">v</ta>
            <ta e="T997" id="Seg_13084" s="T996">propr</ta>
            <ta e="T998" id="Seg_13085" s="T997">conj</ta>
            <ta e="T999" id="Seg_13086" s="T998">v</ta>
            <ta e="T1001" id="Seg_13087" s="T1000">num</ta>
            <ta e="T1002" id="Seg_13088" s="T1001">n</ta>
            <ta e="T1003" id="Seg_13089" s="T1002">v</ta>
            <ta e="T1004" id="Seg_13090" s="T1003">n</ta>
            <ta e="T1005" id="Seg_13091" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_13092" s="T1005">n</ta>
            <ta e="T1008" id="Seg_13093" s="T1007">n</ta>
            <ta e="T1009" id="Seg_13094" s="T1008">adv</ta>
            <ta e="T1011" id="Seg_13095" s="T1010">n</ta>
            <ta e="T1012" id="Seg_13096" s="T1011">quant</ta>
            <ta e="T1013" id="Seg_13097" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_13098" s="T1013">pers</ta>
            <ta e="T1015" id="Seg_13099" s="T1014">v</ta>
            <ta e="T1016" id="Seg_13100" s="T1015">ptcl</ta>
            <ta e="T1017" id="Seg_13101" s="T1016">que</ta>
            <ta e="T1018" id="Seg_13102" s="T1017">adj</ta>
            <ta e="T1019" id="Seg_13103" s="T1018">v</ta>
            <ta e="T1020" id="Seg_13104" s="T1019">n</ta>
            <ta e="T1021" id="Seg_13105" s="T1020">ptcl</ta>
            <ta e="T1022" id="Seg_13106" s="T1021">v</ta>
            <ta e="T1023" id="Seg_13107" s="T1022">n</ta>
            <ta e="T1024" id="Seg_13108" s="T1023">ptcl</ta>
            <ta e="T1025" id="Seg_13109" s="T1024">v</ta>
            <ta e="T1026" id="Seg_13110" s="T1025">adv</ta>
            <ta e="T1027" id="Seg_13111" s="T1026">v</ta>
            <ta e="T1028" id="Seg_13112" s="T1027">adj</ta>
            <ta e="T1029" id="Seg_13113" s="T1028">n</ta>
            <ta e="T1030" id="Seg_13114" s="T1029">v</ta>
            <ta e="T1031" id="Seg_13115" s="T1030">ptcl</ta>
            <ta e="T1032" id="Seg_13116" s="T1031">n</ta>
            <ta e="T1033" id="Seg_13117" s="T1032">v</ta>
            <ta e="T1034" id="Seg_13118" s="T1033">conj</ta>
            <ta e="T1035" id="Seg_13119" s="T1034">dempro</ta>
            <ta e="T1036" id="Seg_13120" s="T1035">ptcl</ta>
            <ta e="T1037" id="Seg_13121" s="T1036">v</ta>
            <ta e="T1039" id="Seg_13122" s="T1038">ptcl</ta>
            <ta e="T1040" id="Seg_13123" s="T1039">v</ta>
            <ta e="T1041" id="Seg_13124" s="T1040">v</ta>
            <ta e="T1042" id="Seg_13125" s="T1041">n</ta>
            <ta e="T1043" id="Seg_13126" s="T1042">adj</ta>
            <ta e="T1044" id="Seg_13127" s="T1043">conj</ta>
            <ta e="T1045" id="Seg_13128" s="T1044">que</ta>
            <ta e="T1046" id="Seg_13129" s="T1045">adj</ta>
            <ta e="T1047" id="Seg_13130" s="T1046">pers</ta>
            <ta e="T1048" id="Seg_13131" s="T1047">v</ta>
            <ta e="T1049" id="Seg_13132" s="T1048">dempro</ta>
            <ta e="T1051" id="Seg_13133" s="T1050">n</ta>
            <ta e="T1052" id="Seg_13134" s="T1051">n</ta>
            <ta e="T1053" id="Seg_13135" s="T1052">dempro</ta>
            <ta e="T1054" id="Seg_13136" s="T1053">ptcl</ta>
            <ta e="T1055" id="Seg_13137" s="T1054">que</ta>
            <ta e="T1056" id="Seg_13138" s="T1055">ptcl</ta>
            <ta e="T1057" id="Seg_13139" s="T1056">v</ta>
            <ta e="T1058" id="Seg_13140" s="T1057">ptcl</ta>
            <ta e="T1059" id="Seg_13141" s="T1058">n</ta>
            <ta e="T1060" id="Seg_13142" s="T1059">ptcl</ta>
            <ta e="T1061" id="Seg_13143" s="T1060">v</ta>
            <ta e="T1062" id="Seg_13144" s="T1061">ptcl</ta>
            <ta e="T1063" id="Seg_13145" s="T1062">adj</ta>
            <ta e="T1064" id="Seg_13146" s="T1063">v</ta>
            <ta e="T1065" id="Seg_13147" s="T1064">conj</ta>
            <ta e="T1067" id="Seg_13148" s="T1066">n</ta>
            <ta e="T1068" id="Seg_13149" s="T1067">quant</ta>
            <ta e="T1070" id="Seg_13150" s="T1069">v</ta>
            <ta e="T1071" id="Seg_13151" s="T1070">v</ta>
            <ta e="T1072" id="Seg_13152" s="T1071">pers</ta>
            <ta e="T1074" id="Seg_13153" s="T1073">adj</ta>
            <ta e="T1075" id="Seg_13154" s="T1074">v</ta>
            <ta e="T1077" id="Seg_13155" s="T1076">dempro</ta>
            <ta e="T1078" id="Seg_13156" s="T1077">num</ta>
            <ta e="T1079" id="Seg_13157" s="T1078">n</ta>
            <ta e="T1080" id="Seg_13158" s="T1079">v</ta>
            <ta e="T1081" id="Seg_13159" s="T1080">adv</ta>
            <ta e="T1084" id="Seg_13160" s="T1083">v</ta>
            <ta e="T1085" id="Seg_13161" s="T1084">ptcl</ta>
            <ta e="T1086" id="Seg_13162" s="T1085">dempro</ta>
            <ta e="T1088" id="Seg_13163" s="T1087">que</ta>
            <ta e="T1090" id="Seg_13164" s="T1089">v</ta>
            <ta e="T1091" id="Seg_13165" s="T1090">conj</ta>
            <ta e="T1092" id="Seg_13166" s="T1091">adv</ta>
            <ta e="T1094" id="Seg_13167" s="T1093">n</ta>
            <ta e="T1095" id="Seg_13168" s="T1094">ptcl</ta>
            <ta e="T1096" id="Seg_13169" s="T1095">ptcl</ta>
            <ta e="T1097" id="Seg_13170" s="T1096">v</ta>
            <ta e="T1099" id="Seg_13171" s="T1098">pers</ta>
            <ta e="T1101" id="Seg_13172" s="T1100">ptcl</ta>
            <ta e="T1102" id="Seg_13173" s="T1101">adv</ta>
            <ta e="T1105" id="Seg_13174" s="T1104">n</ta>
            <ta e="T1106" id="Seg_13175" s="T1105">adv</ta>
            <ta e="T1122" id="Seg_13176" s="T1106">v</ta>
            <ta e="T1107" id="Seg_13177" s="T1122">v</ta>
            <ta e="T1123" id="Seg_13178" s="T1107">v</ta>
            <ta e="T1108" id="Seg_13179" s="T1123">v</ta>
            <ta e="T1109" id="Seg_13180" s="T1108">ptcl</ta>
            <ta e="T1110" id="Seg_13181" s="T1109">v</ta>
            <ta e="T1111" id="Seg_13182" s="T1110">conj</ta>
            <ta e="T1112" id="Seg_13183" s="T1111">v</ta>
            <ta e="T1113" id="Seg_13184" s="T1112">adv</ta>
            <ta e="T1114" id="Seg_13185" s="T1113">v</ta>
            <ta e="T1115" id="Seg_13186" s="T1114">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T1" id="Seg_13187" s="T0">0.3:A</ta>
            <ta e="T3" id="Seg_13188" s="T2">0.3:A</ta>
            <ta e="T4" id="Seg_13189" s="T3">0.3:Th</ta>
            <ta e="T9" id="Seg_13190" s="T8">pro:Th</ta>
            <ta e="T10" id="Seg_13191" s="T9">np:Th</ta>
            <ta e="T16" id="Seg_13192" s="T15">np:L</ta>
            <ta e="T18" id="Seg_13193" s="T17">pro:Th</ta>
            <ta e="T19" id="Seg_13194" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_13195" s="T21">pro.h:Th</ta>
            <ta e="T26" id="Seg_13196" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_13197" s="T28">np:L</ta>
            <ta e="T30" id="Seg_13198" s="T29">np:L</ta>
            <ta e="T31" id="Seg_13199" s="T30">adv:L</ta>
            <ta e="T32" id="Seg_13200" s="T31">np:P</ta>
            <ta e="T33" id="Seg_13201" s="T32">0.1.h:A</ta>
            <ta e="T35" id="Seg_13202" s="T34">np:P</ta>
            <ta e="T36" id="Seg_13203" s="T35">0.1.h:A</ta>
            <ta e="T37" id="Seg_13204" s="T36">adv:Time</ta>
            <ta e="T39" id="Seg_13205" s="T38">np:Th</ta>
            <ta e="T42" id="Seg_13206" s="T41">pro.h:Poss</ta>
            <ta e="T43" id="Seg_13207" s="T42">np:Th</ta>
            <ta e="T47" id="Seg_13208" s="T46">0.3:P</ta>
            <ta e="T49" id="Seg_13209" s="T48">adv:L</ta>
            <ta e="T50" id="Seg_13210" s="T49">adv:L</ta>
            <ta e="T51" id="Seg_13211" s="T50">np:Th</ta>
            <ta e="T54" id="Seg_13212" s="T53">0.3:P</ta>
            <ta e="T56" id="Seg_13213" s="T55">np:P</ta>
            <ta e="T59" id="Seg_13214" s="T58">np:P</ta>
            <ta e="T60" id="Seg_13215" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_13216" s="T61">adv:Time</ta>
            <ta e="T64" id="Seg_13217" s="T63">pro:Th</ta>
            <ta e="T66" id="Seg_13218" s="T65">0.1.h:E</ta>
            <ta e="T67" id="Seg_13219" s="T66">pro:Th</ta>
            <ta e="T70" id="Seg_13220" s="T69">0.1.h:A</ta>
            <ta e="T73" id="Seg_13221" s="T72">np:Th</ta>
            <ta e="T75" id="Seg_13222" s="T74">adv:Time</ta>
            <ta e="T76" id="Seg_13223" s="T75">pro.h:E</ta>
            <ta e="T79" id="Seg_13224" s="T78">pro.h:Th</ta>
            <ta e="T84" id="Seg_13225" s="T82">np.h:P</ta>
            <ta e="T86" id="Seg_13226" s="T85">adv:Time</ta>
            <ta e="T88" id="Seg_13227" s="T87">np:Th</ta>
            <ta e="T90" id="Seg_13228" s="T89">0.3.h:Th</ta>
            <ta e="T91" id="Seg_13229" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_13230" s="T91">np:Ins</ta>
            <ta e="T95" id="Seg_13231" s="T94">pro.h:P</ta>
            <ta e="T100" id="Seg_13232" s="T99">pro:G</ta>
            <ta e="T101" id="Seg_13233" s="T100">pro.h:A</ta>
            <ta e="T104" id="Seg_13234" s="T1119">0.3.h:A</ta>
            <ta e="T106" id="Seg_13235" s="T105">np:G</ta>
            <ta e="T108" id="Seg_13236" s="T107">pro:L</ta>
            <ta e="T109" id="Seg_13237" s="T108">0.3.h:Th</ta>
            <ta e="T111" id="Seg_13238" s="T110">adv:Time</ta>
            <ta e="T112" id="Seg_13239" s="T111">pro.h:A</ta>
            <ta e="T113" id="Seg_13240" s="T112">n:Time</ta>
            <ta e="T118" id="Seg_13241" s="T117">pro.h:A</ta>
            <ta e="T123" id="Seg_13242" s="T122">np.h:A</ta>
            <ta e="T124" id="Seg_13243" s="T123">pro.h:A</ta>
            <ta e="T128" id="Seg_13244" s="T127">np.h:A</ta>
            <ta e="T130" id="Seg_13245" s="T129">np:Ins</ta>
            <ta e="T131" id="Seg_13246" s="T130">np:Th</ta>
            <ta e="T132" id="Seg_13247" s="T131">0.3.h:A</ta>
            <ta e="T133" id="Seg_13248" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_13249" s="T133">adv:Time</ta>
            <ta e="T135" id="Seg_13250" s="T134">pro.h:Th</ta>
            <ta e="T137" id="Seg_13251" s="T136">np:Poss</ta>
            <ta e="T138" id="Seg_13252" s="T137">np:L</ta>
            <ta e="T139" id="Seg_13253" s="T138">pro.h:A</ta>
            <ta e="T142" id="Seg_13254" s="T141">pro.h:Th</ta>
            <ta e="T147" id="Seg_13255" s="T146">np.h:Poss</ta>
            <ta e="T148" id="Seg_13256" s="T147">np:Th</ta>
            <ta e="T150" id="Seg_13257" s="T149">adv:L</ta>
            <ta e="T152" id="Seg_13258" s="T151">pro.h:Poss</ta>
            <ta e="T153" id="Seg_13259" s="T152">np:Th</ta>
            <ta e="T155" id="Seg_13260" s="T154">pro.h:Poss</ta>
            <ta e="T156" id="Seg_13261" s="T155">np.h:A</ta>
            <ta e="T157" id="Seg_13262" s="T156">adv:Time</ta>
            <ta e="T158" id="Seg_13263" s="T157">pro:G</ta>
            <ta e="T159" id="Seg_13264" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_13265" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_13266" s="T160">pro.h:A</ta>
            <ta e="T162" id="Seg_13267" s="T161">pro.h:R</ta>
            <ta e="T163" id="Seg_13268" s="T162">np:Th</ta>
            <ta e="T165" id="Seg_13269" s="T164">adv:Time</ta>
            <ta e="T167" id="Seg_13270" s="T166">pro.h:A</ta>
            <ta e="T169" id="Seg_13271" s="T168">pro.h:Poss</ta>
            <ta e="T170" id="Seg_13272" s="T169">np:Th</ta>
            <ta e="T171" id="Seg_13273" s="T170">np.h:A</ta>
            <ta e="T173" id="Seg_13274" s="T172">pro.h:A</ta>
            <ta e="T177" id="Seg_13275" s="T176">pro:Th</ta>
            <ta e="T178" id="Seg_13276" s="T177">pro.h:A</ta>
            <ta e="T179" id="Seg_13277" s="T178">pro.h:R</ta>
            <ta e="T181" id="Seg_13278" s="T180">np:Th</ta>
            <ta e="T183" id="Seg_13279" s="T182">np:Th</ta>
            <ta e="T185" id="Seg_13280" s="T184">pro.h:A</ta>
            <ta e="T188" id="Seg_13281" s="T187">np:B</ta>
            <ta e="T192" id="Seg_13282" s="T191">pro.h:A</ta>
            <ta e="T195" id="Seg_13283" s="T194">np:Th</ta>
            <ta e="T200" id="Seg_13284" s="T199">pro.h:A</ta>
            <ta e="T201" id="Seg_13285" s="T200">np:Th</ta>
            <ta e="T205" id="Seg_13286" s="T204">adv:Time</ta>
            <ta e="T206" id="Seg_13287" s="T205">pro.h:A</ta>
            <ta e="T209" id="Seg_13288" s="T208">np:G</ta>
            <ta e="T210" id="Seg_13289" s="T209">pro.h:A</ta>
            <ta e="T212" id="Seg_13290" s="T211">np.h:A</ta>
            <ta e="T214" id="Seg_13291" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_13292" s="T214">pro.h:A</ta>
            <ta e="T218" id="Seg_13293" s="T217">np:Th</ta>
            <ta e="T221" id="Seg_13294" s="T220">0.1.h:A</ta>
            <ta e="T225" id="Seg_13295" s="T224">np:Th</ta>
            <ta e="T227" id="Seg_13296" s="T226">np:Ins</ta>
            <ta e="T228" id="Seg_13297" s="T227">0.1.h:A</ta>
            <ta e="T229" id="Seg_13298" s="T228">pro.h:R</ta>
            <ta e="T231" id="Seg_13299" s="T230">pro.h:A</ta>
            <ta e="T233" id="Seg_13300" s="T232">0.1.h:A</ta>
            <ta e="T234" id="Seg_13301" s="T233">pro:G</ta>
            <ta e="T236" id="Seg_13302" s="T235">pro.h:A</ta>
            <ta e="T240" id="Seg_13303" s="T239">0.1.h:A</ta>
            <ta e="T245" id="Seg_13304" s="T244">0.2.h:A</ta>
            <ta e="T249" id="Seg_13305" s="T248">np:L</ta>
            <ta e="T251" id="Seg_13306" s="T250">adv:Time</ta>
            <ta e="T252" id="Seg_13307" s="T251">0.2.h:A</ta>
            <ta e="T254" id="Seg_13308" s="T253">pro.h:E</ta>
            <ta e="T258" id="Seg_13309" s="T257">np:L</ta>
            <ta e="T261" id="Seg_13310" s="T260">0.2.h:P</ta>
            <ta e="T264" id="Seg_13311" s="T263">0.2.h:P</ta>
            <ta e="T265" id="Seg_13312" s="T264">adv:L</ta>
            <ta e="T269" id="Seg_13313" s="T268">np:L</ta>
            <ta e="T271" id="Seg_13314" s="T270">np:L</ta>
            <ta e="T272" id="Seg_13315" s="T271">0.2.h:A</ta>
            <ta e="T273" id="Seg_13316" s="T272">pro:L</ta>
            <ta e="T274" id="Seg_13317" s="T273">0.2.h:E</ta>
            <ta e="T277" id="Seg_13318" s="T276">0.1.h:A</ta>
            <ta e="T279" id="Seg_13319" s="T278">n:Time</ta>
            <ta e="T280" id="Seg_13320" s="T279">0.1.h:A</ta>
            <ta e="T282" id="Seg_13321" s="T281">np:G</ta>
            <ta e="T288" id="Seg_13322" s="T287">np:G</ta>
            <ta e="T289" id="Seg_13323" s="T288">0.1.h:A</ta>
            <ta e="T291" id="Seg_13324" s="T290">0.2.h:A</ta>
            <ta e="T293" id="Seg_13325" s="T292">np.h:Th</ta>
            <ta e="T294" id="Seg_13326" s="T293">np:G</ta>
            <ta e="T297" id="Seg_13327" s="T296">pro.h:R</ta>
            <ta e="T298" id="Seg_13328" s="T297">np:Th</ta>
            <ta e="T299" id="Seg_13329" s="T298">0.3.h:A</ta>
            <ta e="T304" id="Seg_13330" s="T303">0.2.h:A</ta>
            <ta e="T306" id="Seg_13331" s="T305">0.2.h:A</ta>
            <ta e="T309" id="Seg_13332" s="T308">pro.h:A</ta>
            <ta e="T312" id="Seg_13333" s="T311">0.3.h:A</ta>
            <ta e="T313" id="Seg_13334" s="T312">pro.h:Th</ta>
            <ta e="T315" id="Seg_13335" s="T314">0.1.h:A</ta>
            <ta e="T316" id="Seg_13336" s="T315">pro.h:R</ta>
            <ta e="T318" id="Seg_13337" s="T317">pro.h:E</ta>
            <ta e="T322" id="Seg_13338" s="T321">pro.h:A</ta>
            <ta e="T323" id="Seg_13339" s="T322">np:Th</ta>
            <ta e="T326" id="Seg_13340" s="T325">np.h:A</ta>
            <ta e="T330" id="Seg_13341" s="T329">np:Th</ta>
            <ta e="T331" id="Seg_13342" s="T330">0.2.h:A</ta>
            <ta e="T332" id="Seg_13343" s="T331">pro.h:A</ta>
            <ta e="T337" id="Seg_13344" s="T336">np.h:B</ta>
            <ta e="T340" id="Seg_13345" s="T339">np.h:P</ta>
            <ta e="T343" id="Seg_13346" s="T342">np:Th</ta>
            <ta e="T346" id="Seg_13347" s="T345">pro.h:Poss</ta>
            <ta e="T349" id="Seg_13348" s="T348">np.h:E</ta>
            <ta e="T356" id="Seg_13349" s="T355">0.1.h:A</ta>
            <ta e="T357" id="Seg_13350" s="T356">np.h:Th</ta>
            <ta e="T359" id="Seg_13351" s="T358">0.1.h:A</ta>
            <ta e="T360" id="Seg_13352" s="T359">0.3.h:A</ta>
            <ta e="T361" id="Seg_13353" s="T360">0.3.h:A</ta>
            <ta e="T362" id="Seg_13354" s="T361">0.3.h:E</ta>
            <ta e="T363" id="Seg_13355" s="T362">np.h:Th</ta>
            <ta e="T365" id="Seg_13356" s="T364">pro.h:Com</ta>
            <ta e="T369" id="Seg_13357" s="T368">adv:Time</ta>
            <ta e="T370" id="Seg_13358" s="T369">n:Time</ta>
            <ta e="T372" id="Seg_13359" s="T371">pro.h:A</ta>
            <ta e="T375" id="Seg_13360" s="T374">np.h:E</ta>
            <ta e="T379" id="Seg_13361" s="T378">np.h:A</ta>
            <ta e="T382" id="Seg_13362" s="T381">n:Time</ta>
            <ta e="T384" id="Seg_13363" s="T383">0.3.h:A</ta>
            <ta e="T386" id="Seg_13364" s="T385">np.h:E</ta>
            <ta e="T388" id="Seg_13365" s="T387">np:Th</ta>
            <ta e="T389" id="Seg_13366" s="T388">np:L</ta>
            <ta e="T392" id="Seg_13367" s="T391">0.2.h:A</ta>
            <ta e="T394" id="Seg_13368" s="T393">np.h:A</ta>
            <ta e="T396" id="Seg_13369" s="T395">pro.h:P</ta>
            <ta e="T397" id="Seg_13370" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_13371" s="T398">np.h:A</ta>
            <ta e="T403" id="Seg_13372" s="T402">pro.h:Poss</ta>
            <ta e="T404" id="Seg_13373" s="T403">np.h:Th</ta>
            <ta e="T405" id="Seg_13374" s="T404">0.1.h:A</ta>
            <ta e="T408" id="Seg_13375" s="T407">np.h:Th</ta>
            <ta e="T410" id="Seg_13376" s="T409">adv:Time</ta>
            <ta e="T411" id="Seg_13377" s="T410">0.3.h:A</ta>
            <ta e="T412" id="Seg_13378" s="T411">0.3.h:A</ta>
            <ta e="T413" id="Seg_13379" s="T412">0.3.h:A</ta>
            <ta e="T414" id="Seg_13380" s="T413">np.h:A</ta>
            <ta e="T415" id="Seg_13381" s="T414">pro:G</ta>
            <ta e="T418" id="Seg_13382" s="T417">pro.h:Com</ta>
            <ta e="T422" id="Seg_13383" s="T421">n:Time</ta>
            <ta e="T425" id="Seg_13384" s="T424">pro.h:A</ta>
            <ta e="T428" id="Seg_13385" s="T427">np.h:A</ta>
            <ta e="T429" id="Seg_13386" s="T428">n:Time</ta>
            <ta e="T432" id="Seg_13387" s="T431">np.h:A</ta>
            <ta e="T435" id="Seg_13388" s="T434">0.2.h:A</ta>
            <ta e="T436" id="Seg_13389" s="T435">adv:Time</ta>
            <ta e="T437" id="Seg_13390" s="T436">np.h:A</ta>
            <ta e="T440" id="Seg_13391" s="T439">pro.h:P</ta>
            <ta e="T441" id="Seg_13392" s="T440">0.3.h:A</ta>
            <ta e="T442" id="Seg_13393" s="T441">adv:Time</ta>
            <ta e="T443" id="Seg_13394" s="T442">np.h:A</ta>
            <ta e="T444" id="Seg_13395" s="T443">n:Time</ta>
            <ta e="T448" id="Seg_13396" s="T447">np.h:Th</ta>
            <ta e="T449" id="Seg_13397" s="T448">0.1.h:A</ta>
            <ta e="T450" id="Seg_13398" s="T449">np.h:Th</ta>
            <ta e="T454" id="Seg_13399" s="T453">adv:Time</ta>
            <ta e="T455" id="Seg_13400" s="T454">0.3.h:A</ta>
            <ta e="T456" id="Seg_13401" s="T455">0.3.h:A</ta>
            <ta e="T457" id="Seg_13402" s="T456">0.3.h:A</ta>
            <ta e="T458" id="Seg_13403" s="T457">np.h:A</ta>
            <ta e="T463" id="Seg_13404" s="T462">pro.h:Com</ta>
            <ta e="T466" id="Seg_13405" s="T465">adv:Time</ta>
            <ta e="T467" id="Seg_13406" s="T466">n:Time</ta>
            <ta e="T470" id="Seg_13407" s="T469">0.3.h:A</ta>
            <ta e="T472" id="Seg_13408" s="T471">np.h:A</ta>
            <ta e="T473" id="Seg_13409" s="T472">n:Time</ta>
            <ta e="T478" id="Seg_13410" s="T477">np.h:A</ta>
            <ta e="T480" id="Seg_13411" s="T479">0.2.h:A</ta>
            <ta e="T481" id="Seg_13412" s="T480">np.h:A</ta>
            <ta e="T484" id="Seg_13413" s="T483">pro.h:P</ta>
            <ta e="T485" id="Seg_13414" s="T484">0.3.h:A</ta>
            <ta e="T487" id="Seg_13415" s="T486">pro.h:A</ta>
            <ta e="T491" id="Seg_13416" s="T490">np.h:Th</ta>
            <ta e="T492" id="Seg_13417" s="T491">0.1.h:A</ta>
            <ta e="T493" id="Seg_13418" s="T492">np.h:Th</ta>
            <ta e="T495" id="Seg_13419" s="T494">n:Time</ta>
            <ta e="T496" id="Seg_13420" s="T495">0.3.h:A</ta>
            <ta e="T497" id="Seg_13421" s="T496">0.3.h:A</ta>
            <ta e="T500" id="Seg_13422" s="T499">np:L</ta>
            <ta e="T501" id="Seg_13423" s="T500">0.3.h:A</ta>
            <ta e="T502" id="Seg_13424" s="T501">0.3.h:A</ta>
            <ta e="T503" id="Seg_13425" s="T502">adv:Time</ta>
            <ta e="T504" id="Seg_13426" s="T503">np:G</ta>
            <ta e="T505" id="Seg_13427" s="T504">0.3.h:A</ta>
            <ta e="T506" id="Seg_13428" s="T505">0.3.h:E</ta>
            <ta e="T508" id="Seg_13429" s="T507">np.h:A</ta>
            <ta e="T512" id="Seg_13430" s="T511">np:Th</ta>
            <ta e="T514" id="Seg_13431" s="T513">np:Ins</ta>
            <ta e="T515" id="Seg_13432" s="T514">adv:Time</ta>
            <ta e="T516" id="Seg_13433" s="T515">pro.h:A</ta>
            <ta e="T522" id="Seg_13434" s="T521">pro.h:A</ta>
            <ta e="T524" id="Seg_13435" s="T523">pro.h:Th</ta>
            <ta e="T525" id="Seg_13436" s="T524">np:G</ta>
            <ta e="T526" id="Seg_13437" s="T525">0.3.h:A</ta>
            <ta e="T528" id="Seg_13438" s="T527">np.h:E</ta>
            <ta e="T531" id="Seg_13439" s="T530">np.h:A</ta>
            <ta e="T536" id="Seg_13440" s="T535">np.h:A</ta>
            <ta e="T538" id="Seg_13441" s="T537">0.3.h:A</ta>
            <ta e="T539" id="Seg_13442" s="T538">pro.h:Th</ta>
            <ta e="T541" id="Seg_13443" s="T540">0.3.h:A</ta>
            <ta e="T543" id="Seg_13444" s="T542">0.3.h:E</ta>
            <ta e="T544" id="Seg_13445" s="T543">pro.h:R</ta>
            <ta e="T548" id="Seg_13446" s="T547">0.2.h:A</ta>
            <ta e="T553" id="Seg_13447" s="T552">np.h:A</ta>
            <ta e="T557" id="Seg_13448" s="T556">np:Th</ta>
            <ta e="T558" id="Seg_13449" s="T557">0.3.h:A</ta>
            <ta e="T560" id="Seg_13450" s="T559">0.3.h:A</ta>
            <ta e="T561" id="Seg_13451" s="T560">pro:G</ta>
            <ta e="T562" id="Seg_13452" s="T561">np.h:A</ta>
            <ta e="T564" id="Seg_13453" s="T563">pro.h:A</ta>
            <ta e="T565" id="Seg_13454" s="T564">adv:Time</ta>
            <ta e="T566" id="Seg_13455" s="T565">pro.h:P</ta>
            <ta e="T568" id="Seg_13456" s="T567">pro.h:A</ta>
            <ta e="T569" id="Seg_13457" s="T568">0.2.h:A</ta>
            <ta e="T571" id="Seg_13458" s="T570">pro.h:P</ta>
            <ta e="T572" id="Seg_13459" s="T571">adv:L</ta>
            <ta e="T573" id="Seg_13460" s="T572">np.h:A</ta>
            <ta e="T575" id="Seg_13461" s="T574">0.3.h:A</ta>
            <ta e="T576" id="Seg_13462" s="T575">pro.h:P</ta>
            <ta e="T577" id="Seg_13463" s="T576">pro.h:E</ta>
            <ta e="T580" id="Seg_13464" s="T579">0.2.h:A</ta>
            <ta e="T581" id="Seg_13465" s="T580">pro.h:Th</ta>
            <ta e="T582" id="Seg_13466" s="T581">np:Ins</ta>
            <ta e="T584" id="Seg_13467" s="T583">0.2.h:A</ta>
            <ta e="T586" id="Seg_13468" s="T585">pro.h:E</ta>
            <ta e="T589" id="Seg_13469" s="T588">pro.h:Th</ta>
            <ta e="T591" id="Seg_13470" s="T590">pro.h:A</ta>
            <ta e="T597" id="Seg_13471" s="T596">adv:Time</ta>
            <ta e="T599" id="Seg_13472" s="T598">pro.h:A</ta>
            <ta e="T601" id="Seg_13473" s="T600">0.2.h:A</ta>
            <ta e="T602" id="Seg_13474" s="T601">pro.h:A</ta>
            <ta e="T604" id="Seg_13475" s="T603">pro.h:Th</ta>
            <ta e="T606" id="Seg_13476" s="T605">adv:Time</ta>
            <ta e="T609" id="Seg_13477" s="T608">np:Ins</ta>
            <ta e="T612" id="Seg_13478" s="T611">adv:Time</ta>
            <ta e="T613" id="Seg_13479" s="T612">pro.h:A</ta>
            <ta e="T614" id="Seg_13480" s="T613">np.h:E</ta>
            <ta e="T616" id="Seg_13481" s="T615">np:Ins</ta>
            <ta e="T619" id="Seg_13482" s="T618">np.h:P</ta>
            <ta e="T622" id="Seg_13483" s="T621">np.h:P</ta>
            <ta e="T623" id="Seg_13484" s="T622">np.h:Com</ta>
            <ta e="T625" id="Seg_13485" s="T624">np.h:Th</ta>
            <ta e="T631" id="Seg_13486" s="T630">pro.h:A</ta>
            <ta e="T632" id="Seg_13487" s="T631">np:Th</ta>
            <ta e="T634" id="Seg_13488" s="T633">pro.h:A</ta>
            <ta e="T635" id="Seg_13489" s="T634">np:Th</ta>
            <ta e="T636" id="Seg_13490" s="T635">adv:Time</ta>
            <ta e="T637" id="Seg_13491" s="T636">0.3.h:A</ta>
            <ta e="T638" id="Seg_13492" s="T637">pro.h:P</ta>
            <ta e="T640" id="Seg_13493" s="T639">np.h:A</ta>
            <ta e="T642" id="Seg_13494" s="T641">np:Th</ta>
            <ta e="T644" id="Seg_13495" s="T643">np.h:A</ta>
            <ta e="T645" id="Seg_13496" s="T644">np:Th</ta>
            <ta e="T646" id="Seg_13497" s="T645">adv:Time</ta>
            <ta e="T647" id="Seg_13498" s="T646">np.h:A</ta>
            <ta e="T649" id="Seg_13499" s="T648">np.h:A</ta>
            <ta e="T650" id="Seg_13500" s="T649">pro.h:E</ta>
            <ta e="T652" id="Seg_13501" s="T651">adv:Time</ta>
            <ta e="T656" id="Seg_13502" s="T655">np.h:A</ta>
            <ta e="T658" id="Seg_13503" s="T657">np.h:R</ta>
            <ta e="T663" id="Seg_13504" s="T662">np.h:A</ta>
            <ta e="T665" id="Seg_13505" s="T664">pro.h:R</ta>
            <ta e="T666" id="Seg_13506" s="T665">0.2.h:A</ta>
            <ta e="T667" id="Seg_13507" s="T666">np:Th</ta>
            <ta e="T669" id="Seg_13508" s="T668">pro.h:A</ta>
            <ta e="T670" id="Seg_13509" s="T669">np:Th</ta>
            <ta e="T671" id="Seg_13510" s="T670">adv:Time</ta>
            <ta e="T675" id="Seg_13511" s="T674">np:Th</ta>
            <ta e="T676" id="Seg_13512" s="T675">np:Th</ta>
            <ta e="T677" id="Seg_13513" s="T676">0.3.h:A</ta>
            <ta e="T678" id="Seg_13514" s="T677">pro:P</ta>
            <ta e="T682" id="Seg_13515" s="T681">0.3:Th</ta>
            <ta e="T684" id="Seg_13516" s="T683">np.h:A</ta>
            <ta e="T686" id="Seg_13517" s="T685">np:Th</ta>
            <ta e="T688" id="Seg_13518" s="T687">np:G</ta>
            <ta e="T689" id="Seg_13519" s="T688">np:Th</ta>
            <ta e="T691" id="Seg_13520" s="T690">adv:Time</ta>
            <ta e="T693" id="Seg_13521" s="T692">pro.h:Poss</ta>
            <ta e="T695" id="Seg_13522" s="T694">np:P</ta>
            <ta e="T699" id="Seg_13523" s="T698">np.h:E</ta>
            <ta e="T701" id="Seg_13524" s="T700">np.h:E</ta>
            <ta e="T702" id="Seg_13525" s="T701">pro.h:Poss</ta>
            <ta e="T703" id="Seg_13526" s="T702">np.h:Th</ta>
            <ta e="T707" id="Seg_13527" s="T706">np:Th</ta>
            <ta e="T712" id="Seg_13528" s="T711">np.h:A</ta>
            <ta e="T719" id="Seg_13529" s="T718">pro.h:A</ta>
            <ta e="T722" id="Seg_13530" s="T721">np:P</ta>
            <ta e="T723" id="Seg_13531" s="T722">0.3.h:A</ta>
            <ta e="T724" id="Seg_13532" s="T723">np:P</ta>
            <ta e="T725" id="Seg_13533" s="T724">0.3.h:A</ta>
            <ta e="T726" id="Seg_13534" s="T725">adv:Time</ta>
            <ta e="T727" id="Seg_13535" s="T726">0.3.h:A</ta>
            <ta e="T729" id="Seg_13536" s="T728">np.h:P</ta>
            <ta e="T731" id="Seg_13537" s="T730">np.h:A</ta>
            <ta e="T735" id="Seg_13538" s="T734">0.1.h:A</ta>
            <ta e="T737" id="Seg_13539" s="T736">pro.h:Com</ta>
            <ta e="T738" id="Seg_13540" s="T737">adv:G</ta>
            <ta e="T739" id="Seg_13541" s="T738">np.h:R</ta>
            <ta e="T740" id="Seg_13542" s="T739">0.3.h:A</ta>
            <ta e="T742" id="Seg_13543" s="T741">0.1.h:A</ta>
            <ta e="T744" id="Seg_13544" s="T743">0.3.h:A</ta>
            <ta e="T745" id="Seg_13545" s="T744">0.3.h:A</ta>
            <ta e="T746" id="Seg_13546" s="T745">adv:Time</ta>
            <ta e="T749" id="Seg_13547" s="T748">np.h:P</ta>
            <ta e="T751" id="Seg_13548" s="T749">0.3.h:A</ta>
            <ta e="T752" id="Seg_13549" s="T751">np:P</ta>
            <ta e="T753" id="Seg_13550" s="T752">0.3.h:A</ta>
            <ta e="T754" id="Seg_13551" s="T753">np:P</ta>
            <ta e="T755" id="Seg_13552" s="T754">np:P</ta>
            <ta e="T756" id="Seg_13553" s="T755">np:P</ta>
            <ta e="T757" id="Seg_13554" s="T756">np:P</ta>
            <ta e="T758" id="Seg_13555" s="T757">adv:Time</ta>
            <ta e="T759" id="Seg_13556" s="T758">np:Ins</ta>
            <ta e="T761" id="Seg_13557" s="T760">np:Poss</ta>
            <ta e="T762" id="Seg_13558" s="T761">np:L</ta>
            <ta e="T763" id="Seg_13559" s="T762">0.3.h:A</ta>
            <ta e="T764" id="Seg_13560" s="T763">adv:Time</ta>
            <ta e="T765" id="Seg_13561" s="T764">pro.h:A</ta>
            <ta e="T766" id="Seg_13562" s="T765">np:Ins</ta>
            <ta e="T768" id="Seg_13563" s="T767">adv:Time</ta>
            <ta e="T769" id="Seg_13564" s="T768">np.h:A</ta>
            <ta e="T772" id="Seg_13565" s="T771">0.3.h:A</ta>
            <ta e="T773" id="Seg_13566" s="T772">np:G</ta>
            <ta e="T776" id="Seg_13567" s="T775">pro.h:A</ta>
            <ta e="T779" id="Seg_13568" s="T778">pro.h:Com</ta>
            <ta e="T781" id="Seg_13569" s="T780">np:G</ta>
            <ta e="T784" id="Seg_13570" s="T783">pro.h:P</ta>
            <ta e="T789" id="Seg_13571" s="T788">np:Th</ta>
            <ta e="T790" id="Seg_13572" s="T789">0.3.h:A</ta>
            <ta e="T791" id="Seg_13573" s="T790">pro.h:B</ta>
            <ta e="T793" id="Seg_13574" s="T792">adv:Time</ta>
            <ta e="T794" id="Seg_13575" s="T793">pro.h:B</ta>
            <ta e="T795" id="Seg_13576" s="T794">np:Th</ta>
            <ta e="T796" id="Seg_13577" s="T795">0.3.h:A</ta>
            <ta e="T799" id="Seg_13578" s="T798">adv:Time</ta>
            <ta e="T800" id="Seg_13579" s="T799">np:B</ta>
            <ta e="T801" id="Seg_13580" s="T800">0.3.h:A</ta>
            <ta e="T808" id="Seg_13581" s="T807">adv:Time</ta>
            <ta e="T813" id="Seg_13582" s="T812">np.h:Th</ta>
            <ta e="T815" id="Seg_13583" s="T814">np:Th</ta>
            <ta e="T817" id="Seg_13584" s="T816">pro.h:E</ta>
            <ta e="T823" id="Seg_13585" s="T822">np.h:A</ta>
            <ta e="T826" id="Seg_13586" s="T825">pro.h:E</ta>
            <ta e="T832" id="Seg_13587" s="T831">np:Th</ta>
            <ta e="T834" id="Seg_13588" s="T833">np:L</ta>
            <ta e="T836" id="Seg_13589" s="T835">adv:Time</ta>
            <ta e="T839" id="Seg_13590" s="T838">np:Th</ta>
            <ta e="T842" id="Seg_13591" s="T841">np:P</ta>
            <ta e="T844" id="Seg_13592" s="T843">np:P</ta>
            <ta e="T845" id="Seg_13593" s="T844">adv:Time</ta>
            <ta e="T847" id="Seg_13594" s="T846">np.h:A</ta>
            <ta e="T848" id="Seg_13595" s="T847">0.1.h:A</ta>
            <ta e="T849" id="Seg_13596" s="T848">pro.h:Com</ta>
            <ta e="T850" id="Seg_13597" s="T849">0.3.h:A</ta>
            <ta e="T851" id="Seg_13598" s="T850">np.h:Th</ta>
            <ta e="T852" id="Seg_13599" s="T851">np.h:A</ta>
            <ta e="T856" id="Seg_13600" s="T855">0.2.h:A</ta>
            <ta e="T858" id="Seg_13601" s="T857">0.2.h:A</ta>
            <ta e="T860" id="Seg_13602" s="T859">pro.h:A</ta>
            <ta e="T864" id="Seg_13603" s="T863">np:E</ta>
            <ta e="T868" id="Seg_13604" s="T867">np:A</ta>
            <ta e="T870" id="Seg_13605" s="T869">np:E</ta>
            <ta e="T872" id="Seg_13606" s="T871">np:Th</ta>
            <ta e="T874" id="Seg_13607" s="T873">0.2.h:A</ta>
            <ta e="T875" id="Seg_13608" s="T874">adv:Time</ta>
            <ta e="T876" id="Seg_13609" s="T875">pro.h:A</ta>
            <ta e="T879" id="Seg_13610" s="T878">np.h:A</ta>
            <ta e="T881" id="Seg_13611" s="T880">np:P</ta>
            <ta e="T883" id="Seg_13612" s="T882">0.3.h:A</ta>
            <ta e="T885" id="Seg_13613" s="T884">pro.h:Th</ta>
            <ta e="T890" id="Seg_13614" s="T889">adv:Time</ta>
            <ta e="T891" id="Seg_13615" s="T890">pro.h:A</ta>
            <ta e="T893" id="Seg_13616" s="T892">np:P</ta>
            <ta e="T899" id="Seg_13617" s="T898">np:G</ta>
            <ta e="T900" id="Seg_13618" s="T899">0.3.h:A</ta>
            <ta e="T902" id="Seg_13619" s="T901">pro.h:A</ta>
            <ta e="T906" id="Seg_13620" s="T905">pro.h:E</ta>
            <ta e="T909" id="Seg_13621" s="T908">adv:Time</ta>
            <ta e="T911" id="Seg_13622" s="T910">pro.h:Th</ta>
            <ta e="T912" id="Seg_13623" s="T911">0.3.h:A</ta>
            <ta e="T914" id="Seg_13624" s="T913">pro.h:A</ta>
            <ta e="T917" id="Seg_13625" s="T916">0.3.h:A</ta>
            <ta e="T920" id="Seg_13626" s="T919">np:L</ta>
            <ta e="T921" id="Seg_13627" s="T1121">0.3.h:A</ta>
            <ta e="T928" id="Seg_13628" s="T927">pro.h:Poss</ta>
            <ta e="T932" id="Seg_13629" s="T931">np:Th</ta>
            <ta e="T934" id="Seg_13630" s="T933">adv:Time</ta>
            <ta e="T935" id="Seg_13631" s="T934">0.3.h:A</ta>
            <ta e="T936" id="Seg_13632" s="T935">0.3.h:A</ta>
            <ta e="T938" id="Seg_13633" s="T937">n:Time</ta>
            <ta e="T941" id="Seg_13634" s="T940">np:Ins</ta>
            <ta e="T942" id="Seg_13635" s="T941">0.1.h:A</ta>
            <ta e="T943" id="Seg_13636" s="T942">0.1.h:A</ta>
            <ta e="T944" id="Seg_13637" s="T943">0.1.h:A</ta>
            <ta e="T945" id="Seg_13638" s="T944">0.1.h:A</ta>
            <ta e="T946" id="Seg_13639" s="T945">pro:L</ta>
            <ta e="T947" id="Seg_13640" s="T946">0.3:Th</ta>
            <ta e="T951" id="Seg_13641" s="T950">np:A</ta>
            <ta e="T953" id="Seg_13642" s="T952">0.1.h:E</ta>
            <ta e="T954" id="Seg_13643" s="T953">n:Time</ta>
            <ta e="T955" id="Seg_13644" s="T954">0.1.h:A</ta>
            <ta e="T957" id="Seg_13645" s="T956">0.1.h:A</ta>
            <ta e="T958" id="Seg_13646" s="T957">np:G</ta>
            <ta e="T959" id="Seg_13647" s="T958">0.1.h:A</ta>
            <ta e="T960" id="Seg_13648" s="T959">0.1.h:E</ta>
            <ta e="T961" id="Seg_13649" s="T960">pro:Th</ta>
            <ta e="T963" id="Seg_13650" s="T962">np:G</ta>
            <ta e="T964" id="Seg_13651" s="T963">adv:L</ta>
            <ta e="T965" id="Seg_13652" s="T964">0.3:Th</ta>
            <ta e="T967" id="Seg_13653" s="T966">0.1.h:E</ta>
            <ta e="T968" id="Seg_13654" s="T967">np:A</ta>
            <ta e="T975" id="Seg_13655" s="T974">0.3.h:A</ta>
            <ta e="T976" id="Seg_13656" s="T975">adv:Time</ta>
            <ta e="T977" id="Seg_13657" s="T976">adv:L</ta>
            <ta e="T978" id="Seg_13658" s="T977">np.h:A</ta>
            <ta e="T979" id="Seg_13659" s="T978">np:P</ta>
            <ta e="T981" id="Seg_13660" s="T980">pro.h:A</ta>
            <ta e="T984" id="Seg_13661" s="T983">np:Th</ta>
            <ta e="T986" id="Seg_13662" s="T985">0.3.h:A</ta>
            <ta e="T987" id="Seg_13663" s="T986">0.3.h:A</ta>
            <ta e="T988" id="Seg_13664" s="T987">np:G</ta>
            <ta e="T990" id="Seg_13665" s="T989">np:Ins</ta>
            <ta e="T991" id="Seg_13666" s="T990">np:Th</ta>
            <ta e="T992" id="Seg_13667" s="T991">0.3.h:Th</ta>
            <ta e="T993" id="Seg_13668" s="T992">pro.h:Poss</ta>
            <ta e="T995" id="Seg_13669" s="T994">np.h:A</ta>
            <ta e="T997" id="Seg_13670" s="T996">np:G</ta>
            <ta e="T999" id="Seg_13671" s="T998">0.3.h:A</ta>
            <ta e="T1002" id="Seg_13672" s="T1001">np:Th</ta>
            <ta e="T1014" id="Seg_13673" s="T1013">pro:L pro.h:Poss</ta>
            <ta e="T1015" id="Seg_13674" s="T1014">0.3:Th</ta>
            <ta e="T1019" id="Seg_13675" s="T1018">0.3:P</ta>
            <ta e="T1020" id="Seg_13676" s="T1019">np:P</ta>
            <ta e="T1022" id="Seg_13677" s="T1021">0.3:A</ta>
            <ta e="T1023" id="Seg_13678" s="T1022">np:P</ta>
            <ta e="T1025" id="Seg_13679" s="T1024">0.3:A</ta>
            <ta e="T1026" id="Seg_13680" s="T1025">np:P</ta>
            <ta e="T1027" id="Seg_13681" s="T1026">0.3:A</ta>
            <ta e="T1029" id="Seg_13682" s="T1028">np:A</ta>
            <ta e="T1032" id="Seg_13683" s="T1031">np:P</ta>
            <ta e="T1033" id="Seg_13684" s="T1032">0.3:A</ta>
            <ta e="T1035" id="Seg_13685" s="T1034">pro:A</ta>
            <ta e="T1040" id="Seg_13686" s="T1039">0.1.h:E</ta>
            <ta e="T1041" id="Seg_13687" s="T1040">0.3.h:A</ta>
            <ta e="T1047" id="Seg_13688" s="T1046">pro.h:A</ta>
            <ta e="T1049" id="Seg_13689" s="T1048">pro.h:Poss</ta>
            <ta e="T1051" id="Seg_13690" s="T1050">np:So</ta>
            <ta e="T1052" id="Seg_13691" s="T1051">np:Th</ta>
            <ta e="T1053" id="Seg_13692" s="T1052">pro.h:E</ta>
            <ta e="T1055" id="Seg_13693" s="T1054">np:Th</ta>
            <ta e="T1059" id="Seg_13694" s="T1058">np:P</ta>
            <ta e="T1064" id="Seg_13695" s="T1063">0.3:P</ta>
            <ta e="T1070" id="Seg_13696" s="T1069">0.3:Th</ta>
            <ta e="T1071" id="Seg_13697" s="T1070">0.3:E</ta>
            <ta e="T1072" id="Seg_13698" s="T1071">pro:L</ta>
            <ta e="T1075" id="Seg_13699" s="T1074">0.3:P</ta>
            <ta e="T1077" id="Seg_13700" s="T1076">pro:E</ta>
            <ta e="T1079" id="Seg_13701" s="T1078">np.h:Com</ta>
            <ta e="T1081" id="Seg_13702" s="T1080">adv:Time</ta>
            <ta e="T1086" id="Seg_13703" s="T1085">pro:A</ta>
            <ta e="T1088" id="Seg_13704" s="T1087">pro:Th</ta>
            <ta e="T1092" id="Seg_13705" s="T1091">adv:Time</ta>
            <ta e="T1094" id="Seg_13706" s="T1093">np:Th</ta>
            <ta e="T1102" id="Seg_13707" s="T1101">adv:Time</ta>
            <ta e="T1106" id="Seg_13708" s="T1105">adv:Time</ta>
            <ta e="T1107" id="Seg_13709" s="T1122">0.1.h:A</ta>
            <ta e="T1108" id="Seg_13710" s="T1123">0.2.h:A</ta>
            <ta e="T1110" id="Seg_13711" s="T1109">0.1.h:A</ta>
            <ta e="T1112" id="Seg_13712" s="T1111">0.1.h:A</ta>
            <ta e="T1113" id="Seg_13713" s="T1112">adv:Time</ta>
            <ta e="T1114" id="Seg_13714" s="T1113">0.2.h:A</ta>
            <ta e="T1115" id="Seg_13715" s="T1114">np:Com</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T1" id="Seg_13716" s="T0">v:pred 0.3:S</ta>
            <ta e="T3" id="Seg_13717" s="T2">v:pred 0.3:S</ta>
            <ta e="T4" id="Seg_13718" s="T3">v:pred 0.3:S</ta>
            <ta e="T9" id="Seg_13719" s="T8">pro:S</ta>
            <ta e="T10" id="Seg_13720" s="T9">n:pred</ta>
            <ta e="T18" id="Seg_13721" s="T17">pro:S</ta>
            <ta e="T19" id="Seg_13722" s="T18">n:pred</ta>
            <ta e="T22" id="Seg_13723" s="T21">pro.h:S</ta>
            <ta e="T23" id="Seg_13724" s="T22">adj:pred</ta>
            <ta e="T24" id="Seg_13725" s="T23">cop</ta>
            <ta e="T26" id="Seg_13726" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_13727" s="T26">v:pred</ta>
            <ta e="T32" id="Seg_13728" s="T31">np:O</ta>
            <ta e="T33" id="Seg_13729" s="T32">v:pred 0.1.h:S</ta>
            <ta e="T35" id="Seg_13730" s="T34">np:O</ta>
            <ta e="T36" id="Seg_13731" s="T35">v:pred 0.1.h:S</ta>
            <ta e="T39" id="Seg_13732" s="T38">np:S</ta>
            <ta e="T41" id="Seg_13733" s="T40">v:pred</ta>
            <ta e="T43" id="Seg_13734" s="T42">np:S</ta>
            <ta e="T45" id="Seg_13735" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_13736" s="T46">v:pred 0.3:S</ta>
            <ta e="T51" id="Seg_13737" s="T50">np:S</ta>
            <ta e="T52" id="Seg_13738" s="T51">v:pred</ta>
            <ta e="T54" id="Seg_13739" s="T53">v:pred 0.3:S</ta>
            <ta e="T56" id="Seg_13740" s="T55">np:S</ta>
            <ta e="T57" id="Seg_13741" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_13742" s="T58">np:O</ta>
            <ta e="T60" id="Seg_13743" s="T59">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_13744" s="T63">pro:O</ta>
            <ta e="T65" id="Seg_13745" s="T64">ptcl.neg</ta>
            <ta e="T66" id="Seg_13746" s="T65">v:pred 0.1.h:S</ta>
            <ta e="T67" id="Seg_13747" s="T66">pro:O</ta>
            <ta e="T68" id="Seg_13748" s="T67">ptcl.neg</ta>
            <ta e="T70" id="Seg_13749" s="T69">v:pred 0.1.h:S</ta>
            <ta e="T73" id="Seg_13750" s="T72">np:S</ta>
            <ta e="T1117" id="Seg_13751" s="T73">conv:pred</ta>
            <ta e="T74" id="Seg_13752" s="T1117">v:pred</ta>
            <ta e="T76" id="Seg_13753" s="T75">pro.h:S</ta>
            <ta e="T78" id="Seg_13754" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_13755" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_13756" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_13757" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_13758" s="T82">np.h:S</ta>
            <ta e="T88" id="Seg_13759" s="T87">np:O</ta>
            <ta e="T90" id="Seg_13760" s="T89">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_13761" s="T90">np:S</ta>
            <ta e="T93" id="Seg_13762" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_13763" s="T94">pro.h:S</ta>
            <ta e="T97" id="Seg_13764" s="T96">adj:pred</ta>
            <ta e="T98" id="Seg_13765" s="T97">cop</ta>
            <ta e="T101" id="Seg_13766" s="T100">pro.h:S</ta>
            <ta e="T1118" id="Seg_13767" s="T102">conv:pred</ta>
            <ta e="T103" id="Seg_13768" s="T1118">v:pred</ta>
            <ta e="T1119" id="Seg_13769" s="T103">conv:pred</ta>
            <ta e="T104" id="Seg_13770" s="T1119">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_13771" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T112" id="Seg_13772" s="T111">pro.h:S</ta>
            <ta e="T114" id="Seg_13773" s="T113">ptcl.neg</ta>
            <ta e="T115" id="Seg_13774" s="T114">v:pred</ta>
            <ta e="T118" id="Seg_13775" s="T117">pro.h:S</ta>
            <ta e="T119" id="Seg_13776" s="T118">v:pred</ta>
            <ta e="T122" id="Seg_13777" s="T121">v:pred</ta>
            <ta e="T123" id="Seg_13778" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_13779" s="T123">pro.h:S</ta>
            <ta e="T125" id="Seg_13780" s="T124">v:pred</ta>
            <ta e="T128" id="Seg_13781" s="T127">np.h:S</ta>
            <ta e="T129" id="Seg_13782" s="T128">v:pred</ta>
            <ta e="T131" id="Seg_13783" s="T130">np:O</ta>
            <ta e="T132" id="Seg_13784" s="T131">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_13785" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_13786" s="T134">pro.h:S</ta>
            <ta e="T136" id="Seg_13787" s="T135">v:pred</ta>
            <ta e="T139" id="Seg_13788" s="T138">pro.h:S</ta>
            <ta e="T142" id="Seg_13789" s="T141">pro.h:O</ta>
            <ta e="T144" id="Seg_13790" s="T143">v:pred</ta>
            <ta e="T148" id="Seg_13791" s="T147">np:S</ta>
            <ta e="T149" id="Seg_13792" s="T148">v:pred</ta>
            <ta e="T153" id="Seg_13793" s="T152">np:O</ta>
            <ta e="T154" id="Seg_13794" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_13795" s="T155">np.h:S</ta>
            <ta e="T159" id="Seg_13796" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_13797" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_13798" s="T160">pro.h:S</ta>
            <ta e="T163" id="Seg_13799" s="T162">np:O</ta>
            <ta e="T164" id="Seg_13800" s="T163">v:pred</ta>
            <ta e="T167" id="Seg_13801" s="T166">pro.h:S</ta>
            <ta e="T168" id="Seg_13802" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_13803" s="T169">np:O</ta>
            <ta e="T171" id="Seg_13804" s="T170">np.h:S</ta>
            <ta e="T172" id="Seg_13805" s="T171">v:pred</ta>
            <ta e="T173" id="Seg_13806" s="T172">pro.h:S</ta>
            <ta e="T174" id="Seg_13807" s="T173">ptcl.neg</ta>
            <ta e="T175" id="Seg_13808" s="T174">v:pred</ta>
            <ta e="T177" id="Seg_13809" s="T176">pro:O</ta>
            <ta e="T178" id="Seg_13810" s="T177">pro.h:S</ta>
            <ta e="T180" id="Seg_13811" s="T179">v:pred</ta>
            <ta e="T181" id="Seg_13812" s="T180">np:O</ta>
            <ta e="T183" id="Seg_13813" s="T182">np:S</ta>
            <ta e="T184" id="Seg_13814" s="T183">ptcl:pred</ta>
            <ta e="T185" id="Seg_13815" s="T184">pro.h:S</ta>
            <ta e="T190" id="Seg_13816" s="T189">v:pred</ta>
            <ta e="T192" id="Seg_13817" s="T191">pro.h:S</ta>
            <ta e="T193" id="Seg_13818" s="T192">ptcl.neg</ta>
            <ta e="T194" id="Seg_13819" s="T193">v:pred</ta>
            <ta e="T195" id="Seg_13820" s="T194">np:O</ta>
            <ta e="T200" id="Seg_13821" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_13822" s="T200">np:O</ta>
            <ta e="T202" id="Seg_13823" s="T201">v:pred</ta>
            <ta e="T203" id="Seg_13824" s="T202">adj:pred</ta>
            <ta e="T206" id="Seg_13825" s="T205">pro.h:S</ta>
            <ta e="T208" id="Seg_13826" s="T207">v:pred</ta>
            <ta e="T210" id="Seg_13827" s="T209">pro.h:S</ta>
            <ta e="T211" id="Seg_13828" s="T210">v:pred</ta>
            <ta e="T212" id="Seg_13829" s="T211">np.h:S</ta>
            <ta e="T213" id="Seg_13830" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_13831" s="T213">np:O</ta>
            <ta e="T215" id="Seg_13832" s="T214">pro.h:S</ta>
            <ta e="T216" id="Seg_13833" s="T215">v:pred</ta>
            <ta e="T218" id="Seg_13834" s="T217">np:O</ta>
            <ta e="T221" id="Seg_13835" s="T220">v:pred 0.1.h:S</ta>
            <ta e="T225" id="Seg_13836" s="T224">np:O</ta>
            <ta e="T228" id="Seg_13837" s="T227">v:pred 0.1.h:S</ta>
            <ta e="T231" id="Seg_13838" s="T230">np.h:S</ta>
            <ta e="T232" id="Seg_13839" s="T231">v:pred</ta>
            <ta e="T233" id="Seg_13840" s="T232">v:pred 0.1.h:S</ta>
            <ta e="T236" id="Seg_13841" s="T235">pro.h:S</ta>
            <ta e="T237" id="Seg_13842" s="T236">v:pred</ta>
            <ta e="T238" id="Seg_13843" s="T237">ptcl.neg</ta>
            <ta e="T239" id="Seg_13844" s="T238">ptcl.neg</ta>
            <ta e="T240" id="Seg_13845" s="T239">v:pred 0.1.h:S</ta>
            <ta e="T245" id="Seg_13846" s="T244">v:pred 0.2.h:S</ta>
            <ta e="T252" id="Seg_13847" s="T251">v:pred 0.2.h:S</ta>
            <ta e="T254" id="Seg_13848" s="T253">pro.h:S</ta>
            <ta e="T255" id="Seg_13849" s="T254">v:pred</ta>
            <ta e="T261" id="Seg_13850" s="T260">v:pred 0.2.h:S</ta>
            <ta e="T262" id="Seg_13851" s="T261">ptcl.neg</ta>
            <ta e="T263" id="Seg_13852" s="T262">ptcl.neg</ta>
            <ta e="T264" id="Seg_13853" s="T263">v:pred 0.2.h:S</ta>
            <ta e="T272" id="Seg_13854" s="T271">v:pred 0.2.h:S</ta>
            <ta e="T274" id="Seg_13855" s="T273">v:pred 0.2.h:S</ta>
            <ta e="T277" id="Seg_13856" s="T276">v:pred 0.1.h:S</ta>
            <ta e="T280" id="Seg_13857" s="T279">v:pred 0.1.h:S</ta>
            <ta e="T289" id="Seg_13858" s="T288">v:pred 0.1.h:S</ta>
            <ta e="T291" id="Seg_13859" s="T290">v:pred 0.2.h:S</ta>
            <ta e="T293" id="Seg_13860" s="T292">np.h:O</ta>
            <ta e="T298" id="Seg_13861" s="T297">np:O</ta>
            <ta e="T299" id="Seg_13862" s="T298">v:pred 0.3.h:S</ta>
            <ta e="T301" id="Seg_13863" s="T300">ptcl.neg</ta>
            <ta e="T304" id="Seg_13864" s="T303">v:pred 0.2.h:S</ta>
            <ta e="T306" id="Seg_13865" s="T305">v:pred 0.2.h:S</ta>
            <ta e="T309" id="Seg_13866" s="T308">pro.h:S</ta>
            <ta e="T310" id="Seg_13867" s="T309">v:pred</ta>
            <ta e="T312" id="Seg_13868" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T313" id="Seg_13869" s="T312">pro.h:O</ta>
            <ta e="T315" id="Seg_13870" s="T314">v:pred 0.1.h:S</ta>
            <ta e="T318" id="Seg_13871" s="T317">pro.h:S</ta>
            <ta e="T319" id="Seg_13872" s="T318">v:pred</ta>
            <ta e="T322" id="Seg_13873" s="T321">pro.h:S</ta>
            <ta e="T323" id="Seg_13874" s="T322">np:O</ta>
            <ta e="T324" id="Seg_13875" s="T323">v:pred</ta>
            <ta e="T326" id="Seg_13876" s="T325">np.h:S</ta>
            <ta e="T328" id="Seg_13877" s="T327">v:pred</ta>
            <ta e="T330" id="Seg_13878" s="T329">np:O</ta>
            <ta e="T331" id="Seg_13879" s="T330">v:pred 0.2.h:S</ta>
            <ta e="T332" id="Seg_13880" s="T331">pro.h:S</ta>
            <ta e="T334" id="Seg_13881" s="T333">ptcl.neg</ta>
            <ta e="T335" id="Seg_13882" s="T334">v:pred</ta>
            <ta e="T340" id="Seg_13883" s="T339">np.h:S</ta>
            <ta e="T341" id="Seg_13884" s="T340">v:pred</ta>
            <ta e="T343" id="Seg_13885" s="T342">np:S</ta>
            <ta e="T347" id="Seg_13886" s="T346">v:pred</ta>
            <ta e="T349" id="Seg_13887" s="T348">np.h:S</ta>
            <ta e="T351" id="Seg_13888" s="T350">v:pred</ta>
            <ta e="T352" id="Seg_13889" s="T351">v:pred</ta>
            <ta e="T356" id="Seg_13890" s="T355">v:pred 0.1.h:S</ta>
            <ta e="T357" id="Seg_13891" s="T356">np.h:O</ta>
            <ta e="T359" id="Seg_13892" s="T358">v:pred 0.1.h:S</ta>
            <ta e="T360" id="Seg_13893" s="T359">v:pred 0.3.h:S</ta>
            <ta e="T361" id="Seg_13894" s="T360">v:pred 0.3.h:S</ta>
            <ta e="T362" id="Seg_13895" s="T361">v:pred 0.3.h:S</ta>
            <ta e="T363" id="Seg_13896" s="T362">np.h:O</ta>
            <ta e="T364" id="Seg_13897" s="T363">v:pred</ta>
            <ta e="T370" id="Seg_13898" s="T369">np:S</ta>
            <ta e="T371" id="Seg_13899" s="T370">v:pred</ta>
            <ta e="T372" id="Seg_13900" s="T371">pro.h:S</ta>
            <ta e="T373" id="Seg_13901" s="T372">v:pred</ta>
            <ta e="T375" id="Seg_13902" s="T374">np.h:S</ta>
            <ta e="T376" id="Seg_13903" s="T375">v:pred</ta>
            <ta e="T379" id="Seg_13904" s="T378">np.h:S</ta>
            <ta e="T380" id="Seg_13905" s="T379">ptcl.neg</ta>
            <ta e="T381" id="Seg_13906" s="T380">v:pred</ta>
            <ta e="T384" id="Seg_13907" s="T383">v:pred 0.3.h:S</ta>
            <ta e="T386" id="Seg_13908" s="T385">np.h:S</ta>
            <ta e="T387" id="Seg_13909" s="T386">v:pred</ta>
            <ta e="T388" id="Seg_13910" s="T387">np:S</ta>
            <ta e="T1120" id="Seg_13911" s="T389">conv:pred</ta>
            <ta e="T390" id="Seg_13912" s="T1120">v:pred</ta>
            <ta e="T392" id="Seg_13913" s="T391">v:pred 0.2.h:S</ta>
            <ta e="T393" id="Seg_13914" s="T392">v:pred</ta>
            <ta e="T394" id="Seg_13915" s="T393">np.h:S</ta>
            <ta e="T396" id="Seg_13916" s="T395">pro.h:O</ta>
            <ta e="T397" id="Seg_13917" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T399" id="Seg_13918" s="T398">np.h:S</ta>
            <ta e="T400" id="Seg_13919" s="T399">v:pred</ta>
            <ta e="T401" id="Seg_13920" s="T400">ptcl.neg</ta>
            <ta e="T402" id="Seg_13921" s="T401">adj:pred</ta>
            <ta e="T404" id="Seg_13922" s="T403">np.h:S</ta>
            <ta e="T405" id="Seg_13923" s="T404">v:pred 0.1.h:S</ta>
            <ta e="T408" id="Seg_13924" s="T407">np.h:O</ta>
            <ta e="T409" id="Seg_13925" s="T408">s:purp</ta>
            <ta e="T411" id="Seg_13926" s="T410">v:pred 0.3.h:S</ta>
            <ta e="T412" id="Seg_13927" s="T411">v:pred 0.3.h:S</ta>
            <ta e="T413" id="Seg_13928" s="T412">v:pred 0.3.h:S</ta>
            <ta e="T414" id="Seg_13929" s="T413">np.h:S</ta>
            <ta e="T416" id="Seg_13930" s="T415">v:pred</ta>
            <ta e="T417" id="Seg_13931" s="T416">ptcl:pred</ta>
            <ta e="T422" id="Seg_13932" s="T421">np:S</ta>
            <ta e="T423" id="Seg_13933" s="T422">v:pred</ta>
            <ta e="T425" id="Seg_13934" s="T424">pro.h:S</ta>
            <ta e="T426" id="Seg_13935" s="T425">v:pred</ta>
            <ta e="T427" id="Seg_13936" s="T426">s:purp</ta>
            <ta e="T428" id="Seg_13937" s="T427">np.h:S</ta>
            <ta e="T430" id="Seg_13938" s="T429">v:pred</ta>
            <ta e="T432" id="Seg_13939" s="T431">np.h:S</ta>
            <ta e="T433" id="Seg_13940" s="T432">v:pred</ta>
            <ta e="T435" id="Seg_13941" s="T434">v:pred 0.2.h:S</ta>
            <ta e="T437" id="Seg_13942" s="T436">np.h:S</ta>
            <ta e="T438" id="Seg_13943" s="T437">v:pred</ta>
            <ta e="T440" id="Seg_13944" s="T439">pro.h:O</ta>
            <ta e="T441" id="Seg_13945" s="T440">v:pred 0.3.h:S</ta>
            <ta e="T443" id="Seg_13946" s="T442">np.h:S</ta>
            <ta e="T445" id="Seg_13947" s="T444">v:pred</ta>
            <ta e="T446" id="Seg_13948" s="T445">ptcl.neg</ta>
            <ta e="T447" id="Seg_13949" s="T446">adj:pred</ta>
            <ta e="T448" id="Seg_13950" s="T447">np.h:S</ta>
            <ta e="T449" id="Seg_13951" s="T448">v:pred 0.1.h:S</ta>
            <ta e="T450" id="Seg_13952" s="T449">np.h:O</ta>
            <ta e="T452" id="Seg_13953" s="T451">s:purp</ta>
            <ta e="T455" id="Seg_13954" s="T454">v:pred 0.3.h:S</ta>
            <ta e="T456" id="Seg_13955" s="T455">v:pred 0.3.h:S</ta>
            <ta e="T457" id="Seg_13956" s="T456">v:pred 0.3.h:S</ta>
            <ta e="T458" id="Seg_13957" s="T457">np.h:S</ta>
            <ta e="T459" id="Seg_13958" s="T458">v:pred</ta>
            <ta e="T462" id="Seg_13959" s="T461">ptcl:pred</ta>
            <ta e="T465" id="Seg_13960" s="T464">ptcl:pred</ta>
            <ta e="T467" id="Seg_13961" s="T466">np:S</ta>
            <ta e="T468" id="Seg_13962" s="T467">v:pred</ta>
            <ta e="T470" id="Seg_13963" s="T469">v:pred 0.3.h:S</ta>
            <ta e="T472" id="Seg_13964" s="T471">np.h:S</ta>
            <ta e="T475" id="Seg_13965" s="T474">v:pred</ta>
            <ta e="T478" id="Seg_13966" s="T477">np.h:S</ta>
            <ta e="T480" id="Seg_13967" s="T479">v:pred 0.2.h:S</ta>
            <ta e="T481" id="Seg_13968" s="T480">np.h:S</ta>
            <ta e="T482" id="Seg_13969" s="T481">v:pred</ta>
            <ta e="T484" id="Seg_13970" s="T483">pro.h:O</ta>
            <ta e="T485" id="Seg_13971" s="T484">v:pred 0.3.h:S</ta>
            <ta e="T487" id="Seg_13972" s="T486">pro.h:S</ta>
            <ta e="T488" id="Seg_13973" s="T487">v:pred</ta>
            <ta e="T489" id="Seg_13974" s="T488">ptcl.neg</ta>
            <ta e="T490" id="Seg_13975" s="T489">adj:pred</ta>
            <ta e="T491" id="Seg_13976" s="T490">np.h:S</ta>
            <ta e="T492" id="Seg_13977" s="T491">v:pred 0.1.h:S</ta>
            <ta e="T493" id="Seg_13978" s="T492">np.h:O</ta>
            <ta e="T494" id="Seg_13979" s="T493">s:purp</ta>
            <ta e="T496" id="Seg_13980" s="T495">v:pred 0.3.h:S</ta>
            <ta e="T497" id="Seg_13981" s="T496">v:pred 0.3.h:S</ta>
            <ta e="T501" id="Seg_13982" s="T500">v:pred 0.3.h:S</ta>
            <ta e="T502" id="Seg_13983" s="T501">v:pred 0.3.h:S</ta>
            <ta e="T505" id="Seg_13984" s="T504">v:pred 0.3.h:S</ta>
            <ta e="T506" id="Seg_13985" s="T505">v:pred 0.3.h:S</ta>
            <ta e="T508" id="Seg_13986" s="T507">np.h:S</ta>
            <ta e="T509" id="Seg_13987" s="T508">v:pred</ta>
            <ta e="T512" id="Seg_13988" s="T511">np:O</ta>
            <ta e="T516" id="Seg_13989" s="T515">pro.h:S</ta>
            <ta e="T517" id="Seg_13990" s="T516">v:pred</ta>
            <ta e="T520" id="Seg_13991" s="T519">ptcl:pred</ta>
            <ta e="T522" id="Seg_13992" s="T521">pro.h:S</ta>
            <ta e="T523" id="Seg_13993" s="T522">v:pred</ta>
            <ta e="T524" id="Seg_13994" s="T523">pro.h:O</ta>
            <ta e="T526" id="Seg_13995" s="T525">v:pred 0.3.h:S</ta>
            <ta e="T528" id="Seg_13996" s="T527">np.h:S</ta>
            <ta e="T529" id="Seg_13997" s="T528">v:pred</ta>
            <ta e="T533" id="Seg_13998" s="T532">ptcl:pred</ta>
            <ta e="T536" id="Seg_13999" s="T535">np.h:S</ta>
            <ta e="T537" id="Seg_14000" s="T536">v:pred</ta>
            <ta e="T538" id="Seg_14001" s="T537">v:pred 0.3.h:S</ta>
            <ta e="T539" id="Seg_14002" s="T538">pro.h:S</ta>
            <ta e="T540" id="Seg_14003" s="T539">v:pred</ta>
            <ta e="T541" id="Seg_14004" s="T540">v:pred 0.3.h:S</ta>
            <ta e="T543" id="Seg_14005" s="T542">v:pred 0.3.h:S</ta>
            <ta e="T548" id="Seg_14006" s="T547">v:pred 0.2.h:S</ta>
            <ta e="T553" id="Seg_14007" s="T552">np.h:S</ta>
            <ta e="T554" id="Seg_14008" s="T553">v:pred</ta>
            <ta e="T555" id="Seg_14009" s="T554">conv:pred</ta>
            <ta e="T557" id="Seg_14010" s="T556">np:O</ta>
            <ta e="T558" id="Seg_14011" s="T557">v:pred 0.3.h:S</ta>
            <ta e="T560" id="Seg_14012" s="T559">v:pred 0.3.h:S</ta>
            <ta e="T562" id="Seg_14013" s="T561">np.h:S</ta>
            <ta e="T563" id="Seg_14014" s="T562">v:pred</ta>
            <ta e="T564" id="Seg_14015" s="T563">pro.h:S</ta>
            <ta e="T566" id="Seg_14016" s="T565">pro.h:O</ta>
            <ta e="T567" id="Seg_14017" s="T566">v:pred</ta>
            <ta e="T568" id="Seg_14018" s="T567">pro.h:S</ta>
            <ta e="T569" id="Seg_14019" s="T568">v:pred 0.2.h:S</ta>
            <ta e="T571" id="Seg_14020" s="T570">pro.h:O</ta>
            <ta e="T573" id="Seg_14021" s="T572">np.h:S</ta>
            <ta e="T574" id="Seg_14022" s="T573">v:pred</ta>
            <ta e="T575" id="Seg_14023" s="T574">v:pred 0.3.h:S</ta>
            <ta e="T576" id="Seg_14024" s="T575">pro.h:O</ta>
            <ta e="T577" id="Seg_14025" s="T576">pro.h:S</ta>
            <ta e="T579" id="Seg_14026" s="T578">v:pred</ta>
            <ta e="T580" id="Seg_14027" s="T579">v:pred 0.2.h:S</ta>
            <ta e="T581" id="Seg_14028" s="T580">pro.h:O</ta>
            <ta e="T584" id="Seg_14029" s="T583">v:pred 0.2.h:S</ta>
            <ta e="T586" id="Seg_14030" s="T585">pro.h:S</ta>
            <ta e="T587" id="Seg_14031" s="T586">ptcl.neg</ta>
            <ta e="T588" id="Seg_14032" s="T587">v:pred</ta>
            <ta e="T589" id="Seg_14033" s="T588">pro.h:O</ta>
            <ta e="T591" id="Seg_14034" s="T590">pro.h:S</ta>
            <ta e="T592" id="Seg_14035" s="T591">v:pred</ta>
            <ta e="T593" id="Seg_14036" s="T592">ptcl:pred</ta>
            <ta e="T599" id="Seg_14037" s="T598">pro.h:S</ta>
            <ta e="T600" id="Seg_14038" s="T599">v:pred</ta>
            <ta e="T601" id="Seg_14039" s="T600">v:pred 0.2.h:S</ta>
            <ta e="T602" id="Seg_14040" s="T601">pro.h:S</ta>
            <ta e="T603" id="Seg_14041" s="T602">v:pred</ta>
            <ta e="T604" id="Seg_14042" s="T603">pro.h:O</ta>
            <ta e="T607" id="Seg_14043" s="T606">ptcl:pred</ta>
            <ta e="T613" id="Seg_14044" s="T612">pro.h:S</ta>
            <ta e="T614" id="Seg_14045" s="T613">np.h:O</ta>
            <ta e="T617" id="Seg_14046" s="T616">v:pred</ta>
            <ta e="T619" id="Seg_14047" s="T618">np.h:S</ta>
            <ta e="T620" id="Seg_14048" s="T619">v:pred</ta>
            <ta e="T622" id="Seg_14049" s="T621">np.h:S</ta>
            <ta e="T625" id="Seg_14050" s="T624">n:pred</ta>
            <ta e="T627" id="Seg_14051" s="T626">cop</ta>
            <ta e="T628" id="Seg_14052" s="T627">ptcl:pred</ta>
            <ta e="T631" id="Seg_14053" s="T630">pro.h:S</ta>
            <ta e="T632" id="Seg_14054" s="T631">np:O</ta>
            <ta e="T634" id="Seg_14055" s="T633">pro.h:S</ta>
            <ta e="T635" id="Seg_14056" s="T634">np:O</ta>
            <ta e="T637" id="Seg_14057" s="T636">v:pred 0.3.h:S</ta>
            <ta e="T638" id="Seg_14058" s="T637">pro.h:S</ta>
            <ta e="T639" id="Seg_14059" s="T638">v:pred</ta>
            <ta e="T640" id="Seg_14060" s="T639">np.h:S</ta>
            <ta e="T641" id="Seg_14061" s="T640">v:pred</ta>
            <ta e="T642" id="Seg_14062" s="T641">np:O</ta>
            <ta e="T644" id="Seg_14063" s="T643">np.h:S</ta>
            <ta e="T645" id="Seg_14064" s="T644">np:O</ta>
            <ta e="T647" id="Seg_14065" s="T646">np.h:S</ta>
            <ta e="T649" id="Seg_14066" s="T648">np.h:S</ta>
            <ta e="T650" id="Seg_14067" s="T649">pro.h:O</ta>
            <ta e="T651" id="Seg_14068" s="T650">v:pred</ta>
            <ta e="T654" id="Seg_14069" s="T653">adj:pred</ta>
            <ta e="T655" id="Seg_14070" s="T654">cop 0.3.h:S</ta>
            <ta e="T656" id="Seg_14071" s="T655">np.h:S</ta>
            <ta e="T657" id="Seg_14072" s="T656">v:pred</ta>
            <ta e="T659" id="Seg_14073" s="T658">ptcl:pred</ta>
            <ta e="T663" id="Seg_14074" s="T662">np.h:S</ta>
            <ta e="T666" id="Seg_14075" s="T665">v:pred 0.2.h:S</ta>
            <ta e="T667" id="Seg_14076" s="T666">np:O</ta>
            <ta e="T669" id="Seg_14077" s="T668">pro.h:S</ta>
            <ta e="T670" id="Seg_14078" s="T669">np:O</ta>
            <ta e="T675" id="Seg_14079" s="T674">np:O</ta>
            <ta e="T676" id="Seg_14080" s="T675">np:O</ta>
            <ta e="T677" id="Seg_14081" s="T676">v:pred 0.3.h:S</ta>
            <ta e="T678" id="Seg_14082" s="T677">pro.S</ta>
            <ta e="T679" id="Seg_14083" s="T678">v:pred</ta>
            <ta e="T680" id="Seg_14084" s="T679">adj:pred</ta>
            <ta e="T682" id="Seg_14085" s="T681">cop 0.3:S</ta>
            <ta e="T684" id="Seg_14086" s="T683">np.h:S</ta>
            <ta e="T685" id="Seg_14087" s="T684">v:pred</ta>
            <ta e="T686" id="Seg_14088" s="T685">np:O</ta>
            <ta e="T689" id="Seg_14089" s="T688">np:O</ta>
            <ta e="T695" id="Seg_14090" s="T694">np:S</ta>
            <ta e="T696" id="Seg_14091" s="T695">v:pred</ta>
            <ta e="T698" id="Seg_14092" s="T697">v:pred</ta>
            <ta e="T699" id="Seg_14093" s="T698">np.h:S</ta>
            <ta e="T701" id="Seg_14094" s="T700">np.h:S</ta>
            <ta e="T703" id="Seg_14095" s="T702">np.h:S</ta>
            <ta e="T704" id="Seg_14096" s="T703">ptcl.neg</ta>
            <ta e="T705" id="Seg_14097" s="T704">v:pred</ta>
            <ta e="T707" id="Seg_14098" s="T706">np:S</ta>
            <ta e="T709" id="Seg_14099" s="T708">v:pred</ta>
            <ta e="T712" id="Seg_14100" s="T711">np.h:S</ta>
            <ta e="T719" id="Seg_14101" s="T718">pro.h:S</ta>
            <ta e="T721" id="Seg_14102" s="T720">v:pred</ta>
            <ta e="T722" id="Seg_14103" s="T721">np:O</ta>
            <ta e="T723" id="Seg_14104" s="T722">v:pred 0.3.h:S</ta>
            <ta e="T724" id="Seg_14105" s="T723">np:O</ta>
            <ta e="T725" id="Seg_14106" s="T724">v:pred 0.3.h:S</ta>
            <ta e="T727" id="Seg_14107" s="T726">v:pred 0.3.h:S</ta>
            <ta e="T729" id="Seg_14108" s="T728">np.h:O</ta>
            <ta e="T731" id="Seg_14109" s="T730">np.h:S</ta>
            <ta e="T733" id="Seg_14110" s="T732">v:pred</ta>
            <ta e="T734" id="Seg_14111" s="T733">v:pred</ta>
            <ta e="T735" id="Seg_14112" s="T734">v:pred 0.1.h:S</ta>
            <ta e="T740" id="Seg_14113" s="T739">v:pred 0.3.h:S</ta>
            <ta e="T742" id="Seg_14114" s="T741">v:pred 0.1.h:S</ta>
            <ta e="T744" id="Seg_14115" s="T743">v:pred 0.3.h:S</ta>
            <ta e="T745" id="Seg_14116" s="T744">v:pred 0.3.h:S</ta>
            <ta e="T747" id="Seg_14117" s="T746">ptcl:pred</ta>
            <ta e="T749" id="Seg_14118" s="T748">np.h:O</ta>
            <ta e="T751" id="Seg_14119" s="T749">v:pred 0.3.h:S</ta>
            <ta e="T752" id="Seg_14120" s="T751">np:O</ta>
            <ta e="T753" id="Seg_14121" s="T752">v:pred 0.3.h:S</ta>
            <ta e="T754" id="Seg_14122" s="T753">np:O</ta>
            <ta e="T755" id="Seg_14123" s="T754">np:O</ta>
            <ta e="T756" id="Seg_14124" s="T755">np:O</ta>
            <ta e="T757" id="Seg_14125" s="T756">np:O</ta>
            <ta e="T763" id="Seg_14126" s="T762">v:pred 0.3.h:S</ta>
            <ta e="T765" id="Seg_14127" s="T764">pro.h:S</ta>
            <ta e="T767" id="Seg_14128" s="T766">v:pred</ta>
            <ta e="T769" id="Seg_14129" s="T768">np.h:S</ta>
            <ta e="T770" id="Seg_14130" s="T769">v:pred</ta>
            <ta e="T772" id="Seg_14131" s="T771">v:pred 0.3.h:S</ta>
            <ta e="T776" id="Seg_14132" s="T775">pro.h:S</ta>
            <ta e="T780" id="Seg_14133" s="T779">v:pred</ta>
            <ta e="T784" id="Seg_14134" s="T783">pro.h:S</ta>
            <ta e="T785" id="Seg_14135" s="T784">adj:pred</ta>
            <ta e="T786" id="Seg_14136" s="T785">cop</ta>
            <ta e="T789" id="Seg_14137" s="T788">np:O</ta>
            <ta e="T790" id="Seg_14138" s="T789">v:pred 0.3.h:S</ta>
            <ta e="T795" id="Seg_14139" s="T794">np:O</ta>
            <ta e="T796" id="Seg_14140" s="T795">v:pred 0.3.h:S</ta>
            <ta e="T797" id="Seg_14141" s="T796">adj:pred</ta>
            <ta e="T798" id="Seg_14142" s="T797">adj:pred</ta>
            <ta e="T801" id="Seg_14143" s="T800">v:pred 0.3.h:S</ta>
            <ta e="T804" id="Seg_14144" s="T803">adj:pred</ta>
            <ta e="T809" id="Seg_14145" s="T808">adj:pred</ta>
            <ta e="T810" id="Seg_14146" s="T809">cop 0.3:S</ta>
            <ta e="T813" id="Seg_14147" s="T812">np.h:S</ta>
            <ta e="T814" id="Seg_14148" s="T813">v:pred</ta>
            <ta e="T815" id="Seg_14149" s="T814">np:S</ta>
            <ta e="T816" id="Seg_14150" s="T815">v:pred</ta>
            <ta e="T817" id="Seg_14151" s="T816">pro.h:S</ta>
            <ta e="T819" id="Seg_14152" s="T818">v:pred</ta>
            <ta e="T823" id="Seg_14153" s="T822">np.h:S</ta>
            <ta e="T824" id="Seg_14154" s="T823">v:pred</ta>
            <ta e="T826" id="Seg_14155" s="T825">pro.h:S</ta>
            <ta e="T827" id="Seg_14156" s="T826">v:pred</ta>
            <ta e="T832" id="Seg_14157" s="T831">np:S</ta>
            <ta e="T833" id="Seg_14158" s="T832">v:pred</ta>
            <ta e="T839" id="Seg_14159" s="T838">np:S</ta>
            <ta e="T840" id="Seg_14160" s="T839">v:pred</ta>
            <ta e="T842" id="Seg_14161" s="T841">np:S</ta>
            <ta e="T843" id="Seg_14162" s="T842">v:pred</ta>
            <ta e="T844" id="Seg_14163" s="T843">np:S</ta>
            <ta e="T846" id="Seg_14164" s="T845">v:pred</ta>
            <ta e="T847" id="Seg_14165" s="T846">np.h:S</ta>
            <ta e="T848" id="Seg_14166" s="T847">v:pred 0.1.h:S</ta>
            <ta e="T850" id="Seg_14167" s="T849">v:pred 0.3.h:S</ta>
            <ta e="T851" id="Seg_14168" s="T850">np.h:O</ta>
            <ta e="T852" id="Seg_14169" s="T851">np.h:S</ta>
            <ta e="T855" id="Seg_14170" s="T854">v:pred</ta>
            <ta e="T856" id="Seg_14171" s="T855">v:pred 0.2.h:S</ta>
            <ta e="T858" id="Seg_14172" s="T857">v:pred 0.2.h:S</ta>
            <ta e="T859" id="Seg_14173" s="T858">ptcl.neg</ta>
            <ta e="T860" id="Seg_14174" s="T859">pro.h:S</ta>
            <ta e="T861" id="Seg_14175" s="T860">v:pred</ta>
            <ta e="T864" id="Seg_14176" s="T863">np:S</ta>
            <ta e="T866" id="Seg_14177" s="T865">v:pred</ta>
            <ta e="T868" id="Seg_14178" s="T867">np:S</ta>
            <ta e="T869" id="Seg_14179" s="T868">v:pred</ta>
            <ta e="T870" id="Seg_14180" s="T869">np:O</ta>
            <ta e="T872" id="Seg_14181" s="T871">np:O</ta>
            <ta e="T874" id="Seg_14182" s="T873"> 0.2.h:S v:pred</ta>
            <ta e="T876" id="Seg_14183" s="T875">pro.h:S</ta>
            <ta e="T877" id="Seg_14184" s="T876">v:pred</ta>
            <ta e="T879" id="Seg_14185" s="T878">np.h:S</ta>
            <ta e="T880" id="Seg_14186" s="T879">v:pred</ta>
            <ta e="T881" id="Seg_14187" s="T880">np:O</ta>
            <ta e="T883" id="Seg_14188" s="T882">v:pred 0.3.h:S</ta>
            <ta e="T885" id="Seg_14189" s="T884">pro.h:S</ta>
            <ta e="T886" id="Seg_14190" s="T885">v:pred</ta>
            <ta e="T891" id="Seg_14191" s="T890">pro.h:S</ta>
            <ta e="T892" id="Seg_14192" s="T891">v:pred</ta>
            <ta e="T893" id="Seg_14193" s="T892">np:O</ta>
            <ta e="T897" id="Seg_14194" s="T896">ptcl:pred</ta>
            <ta e="T900" id="Seg_14195" s="T899">v:pred 0.3.h:S</ta>
            <ta e="T902" id="Seg_14196" s="T901">pro.h:S</ta>
            <ta e="T903" id="Seg_14197" s="T902">ptcl.neg</ta>
            <ta e="T904" id="Seg_14198" s="T903">v:pred</ta>
            <ta e="T906" id="Seg_14199" s="T905">pro.h:S</ta>
            <ta e="T907" id="Seg_14200" s="T906">v:pred</ta>
            <ta e="T911" id="Seg_14201" s="T910">pro.h:O</ta>
            <ta e="T912" id="Seg_14202" s="T911">v:pred 0.3.h:S</ta>
            <ta e="T914" id="Seg_14203" s="T913">pro.h:S</ta>
            <ta e="T915" id="Seg_14204" s="T914">v:pred</ta>
            <ta e="T917" id="Seg_14205" s="T916">v:pred 0.3.h:S</ta>
            <ta e="T1121" id="Seg_14206" s="T920">conv:pred</ta>
            <ta e="T921" id="Seg_14207" s="T1121">v:pred 0.3.h:S</ta>
            <ta e="T932" id="Seg_14208" s="T931">np:S</ta>
            <ta e="T933" id="Seg_14209" s="T932">v:pred</ta>
            <ta e="T935" id="Seg_14210" s="T934">v:pred 0.3.h:S</ta>
            <ta e="T936" id="Seg_14211" s="T935">v:pred 0.3.h:S</ta>
            <ta e="T942" id="Seg_14212" s="T941">v:pred 0.1.h:S</ta>
            <ta e="T943" id="Seg_14213" s="T942">v:pred 0.1.h:S</ta>
            <ta e="T944" id="Seg_14214" s="T943">v:pred 0.1.h:S</ta>
            <ta e="T945" id="Seg_14215" s="T944">v:pred 0.1.h:S</ta>
            <ta e="T947" id="Seg_14216" s="T946">v:pred 0.3:S</ta>
            <ta e="T949" id="Seg_14217" s="T948">v:pred</ta>
            <ta e="T951" id="Seg_14218" s="T950">np:S</ta>
            <ta e="T952" id="Seg_14219" s="T951">ptcl.neg</ta>
            <ta e="T953" id="Seg_14220" s="T952">v:pred 0.1.h:S</ta>
            <ta e="T955" id="Seg_14221" s="T954">v:pred 0.1.h:S</ta>
            <ta e="T957" id="Seg_14222" s="T956">v:pred 0.1.h:S</ta>
            <ta e="T959" id="Seg_14223" s="T958">v:pred 0.1.h:S</ta>
            <ta e="T960" id="Seg_14224" s="T959">v:pred 0.1.h:S</ta>
            <ta e="T961" id="Seg_14225" s="T960">pro:S</ta>
            <ta e="T962" id="Seg_14226" s="T961">v:pred</ta>
            <ta e="T965" id="Seg_14227" s="T964">v:pred 0.3:S</ta>
            <ta e="T967" id="Seg_14228" s="T966">v:pred 0.1.h:S</ta>
            <ta e="T968" id="Seg_14229" s="T967">np:S</ta>
            <ta e="T969" id="Seg_14230" s="T968">v:pred</ta>
            <ta e="T970" id="Seg_14231" s="T969">ptcl.neg</ta>
            <ta e="T974" id="Seg_14232" s="T973">ptcl.neg</ta>
            <ta e="T975" id="Seg_14233" s="T974">v:pred 0.3.h:S</ta>
            <ta e="T978" id="Seg_14234" s="T977">np.h:S</ta>
            <ta e="T979" id="Seg_14235" s="T978">np:O</ta>
            <ta e="T980" id="Seg_14236" s="T979">v:pred</ta>
            <ta e="T981" id="Seg_14237" s="T980">pro.h:S</ta>
            <ta e="T983" id="Seg_14238" s="T982">v:pred</ta>
            <ta e="T984" id="Seg_14239" s="T983">np:O</ta>
            <ta e="T986" id="Seg_14240" s="T985">v:pred 0.3.h:S</ta>
            <ta e="T987" id="Seg_14241" s="T986">v:pred 0.3.h:S</ta>
            <ta e="T991" id="Seg_14242" s="T990">n:pred</ta>
            <ta e="T992" id="Seg_14243" s="T991">cop 0.3.h:S</ta>
            <ta e="T995" id="Seg_14244" s="T994">np.h:S</ta>
            <ta e="T996" id="Seg_14245" s="T995">v:pred</ta>
            <ta e="T999" id="Seg_14246" s="T998">v:pred 0.3.h:S</ta>
            <ta e="T1002" id="Seg_14247" s="T1001">np:S</ta>
            <ta e="T1003" id="Seg_14248" s="T1002">v:pred</ta>
            <ta e="T1015" id="Seg_14249" s="T1014">v:pred 0.3:S</ta>
            <ta e="T1018" id="Seg_14250" s="T1017">adj:pred</ta>
            <ta e="T1019" id="Seg_14251" s="T1018">cop 0.3:S</ta>
            <ta e="T1020" id="Seg_14252" s="T1019">np:O</ta>
            <ta e="T1021" id="Seg_14253" s="T1020">ptcl.neg</ta>
            <ta e="T1022" id="Seg_14254" s="T1021">v:pred 0.3:S</ta>
            <ta e="T1023" id="Seg_14255" s="T1022">np:O</ta>
            <ta e="T1024" id="Seg_14256" s="T1023">ptcl.neg</ta>
            <ta e="T1025" id="Seg_14257" s="T1024">v:pred 0.3:S</ta>
            <ta e="T1026" id="Seg_14258" s="T1025">np:O</ta>
            <ta e="T1027" id="Seg_14259" s="T1026">v:pred 0.3:S</ta>
            <ta e="T1029" id="Seg_14260" s="T1028">np:S</ta>
            <ta e="T1030" id="Seg_14261" s="T1029">v:pred</ta>
            <ta e="T1032" id="Seg_14262" s="T1031">np:O</ta>
            <ta e="T1033" id="Seg_14263" s="T1032">v:pred 0.3:S</ta>
            <ta e="T1035" id="Seg_14264" s="T1034">pro:S</ta>
            <ta e="T1036" id="Seg_14265" s="T1035">ptcl.neg</ta>
            <ta e="T1037" id="Seg_14266" s="T1036">v:pred</ta>
            <ta e="T1039" id="Seg_14267" s="T1038">ptcl.neg</ta>
            <ta e="T1040" id="Seg_14268" s="T1039">v:pred 0.1.h:S</ta>
            <ta e="T1041" id="Seg_14269" s="T1040">v:pred 0.3.h:S</ta>
            <ta e="T1047" id="Seg_14270" s="T1046">pro.h:S</ta>
            <ta e="T1048" id="Seg_14271" s="T1047">v:pred</ta>
            <ta e="T1052" id="Seg_14272" s="T1051">np:O</ta>
            <ta e="T1053" id="Seg_14273" s="T1052">pro.h:S</ta>
            <ta e="T1055" id="Seg_14274" s="T1054">np:O</ta>
            <ta e="T1056" id="Seg_14275" s="T1055">ptcl.neg</ta>
            <ta e="T1057" id="Seg_14276" s="T1056">v:pred</ta>
            <ta e="T1059" id="Seg_14277" s="T1058">np:S</ta>
            <ta e="T1060" id="Seg_14278" s="T1059">ptcl:pred</ta>
            <ta e="T1061" id="Seg_14279" s="T1060">cop</ta>
            <ta e="T1063" id="Seg_14280" s="T1062">adj:pred</ta>
            <ta e="T1064" id="Seg_14281" s="T1063">cop 0.3:S</ta>
            <ta e="T1067" id="Seg_14282" s="T1066">adj:pred</ta>
            <ta e="T1070" id="Seg_14283" s="T1069">cop 0.3:S</ta>
            <ta e="T1071" id="Seg_14284" s="T1070">v:pred 0.3:S</ta>
            <ta e="T1074" id="Seg_14285" s="T1073">adj:pred</ta>
            <ta e="T1075" id="Seg_14286" s="T1074">cop 0.3:S</ta>
            <ta e="T1077" id="Seg_14287" s="T1076">pro:S</ta>
            <ta e="T1080" id="Seg_14288" s="T1079">v:pred</ta>
            <ta e="T1084" id="Seg_14289" s="T1083">v:pred</ta>
            <ta e="T1086" id="Seg_14290" s="T1085">pro:S</ta>
            <ta e="T1088" id="Seg_14291" s="T1087">pro:O</ta>
            <ta e="T1090" id="Seg_14292" s="T1089">v:pred</ta>
            <ta e="T1094" id="Seg_14293" s="T1093">np:S</ta>
            <ta e="T1096" id="Seg_14294" s="T1095">ptcl.neg</ta>
            <ta e="T1097" id="Seg_14295" s="T1096">v:pred</ta>
            <ta e="T1122" id="Seg_14296" s="T1106">conv:pred</ta>
            <ta e="T1107" id="Seg_14297" s="T1122">v:pred 0.1.h:S</ta>
            <ta e="T1123" id="Seg_14298" s="T1107">conv:pred</ta>
            <ta e="T1108" id="Seg_14299" s="T1123">v:pred 0.2.h:S</ta>
            <ta e="T1110" id="Seg_14300" s="T1109">v:pred 0.1.h:S</ta>
            <ta e="T1112" id="Seg_14301" s="T1111">v:pred 0.1.h:S</ta>
            <ta e="T1114" id="Seg_14302" s="T1113">v:pred 0.2.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T2" id="Seg_14303" s="T1">TURK:disc</ta>
            <ta e="T5" id="Seg_14304" s="T4">RUS:gram</ta>
            <ta e="T10" id="Seg_14305" s="T9">TURK:cult</ta>
            <ta e="T19" id="Seg_14306" s="T18">TURK:cult</ta>
            <ta e="T34" id="Seg_14307" s="T33">RUS:gram</ta>
            <ta e="T38" id="Seg_14308" s="T37">TURK:disc</ta>
            <ta e="T44" id="Seg_14309" s="T43">RUS:disc</ta>
            <ta e="T46" id="Seg_14310" s="T45">TURK:disc</ta>
            <ta e="T48" id="Seg_14311" s="T47">RUS:gram</ta>
            <ta e="T53" id="Seg_14312" s="T52">TURK:disc</ta>
            <ta e="T55" id="Seg_14313" s="T54">RUS:gram</ta>
            <ta e="T56" id="Seg_14314" s="T55">TURK:disc</ta>
            <ta e="T58" id="Seg_14315" s="T57">RUS:mod</ta>
            <ta e="T64" id="Seg_14316" s="T63">TURK:gram(INDEF)</ta>
            <ta e="T67" id="Seg_14317" s="T66">TURK:gram(INDEF)</ta>
            <ta e="T71" id="Seg_14318" s="T70">TURK:disc</ta>
            <ta e="T79" id="Seg_14319" s="T78">TURK:gram(INDEF)</ta>
            <ta e="T81" id="Seg_14320" s="T80">TURK:disc</ta>
            <ta e="T87" id="Seg_14321" s="T86">TURK:disc</ta>
            <ta e="T89" id="Seg_14322" s="T88">TURK:disc</ta>
            <ta e="T91" id="Seg_14323" s="T90">TURK:disc</ta>
            <ta e="T99" id="Seg_14324" s="T98">RUS:gram</ta>
            <ta e="T107" id="Seg_14325" s="T106">RUS:gram</ta>
            <ta e="T108" id="Seg_14326" s="T107">TURK:gram(INDEF)</ta>
            <ta e="T113" id="Seg_14327" s="T112">TURK:gram(INDEF)</ta>
            <ta e="T120" id="Seg_14328" s="T119">RUS:cult</ta>
            <ta e="T123" id="Seg_14329" s="T122">TURK:core</ta>
            <ta e="T130" id="Seg_14330" s="T129">RUS:cult</ta>
            <ta e="T131" id="Seg_14331" s="T130">TAT:cult</ta>
            <ta e="T132" id="Seg_14332" s="T131">RUS:calq</ta>
            <ta e="T137" id="Seg_14333" s="T136">TAT:cult</ta>
            <ta e="T141" id="Seg_14334" s="T140">TURK:disc</ta>
            <ta e="T144" id="Seg_14335" s="T143">RUS:calq</ta>
            <ta e="T166" id="Seg_14336" s="T165">RUS:gram</ta>
            <ta e="T190" id="Seg_14337" s="T189">TURK:cult</ta>
            <ta e="T191" id="Seg_14338" s="T190">RUS:gram</ta>
            <ta e="T196" id="Seg_14339" s="T195">RUS:gram</ta>
            <ta e="T198" id="Seg_14340" s="T197">RUS:gram</ta>
            <ta e="T227" id="Seg_14341" s="T226">RUS:cult</ta>
            <ta e="T235" id="Seg_14342" s="T234">RUS:gram</ta>
            <ta e="T238" id="Seg_14343" s="T237">TURK:disc</ta>
            <ta e="T250" id="Seg_14344" s="T249">RUS:gram</ta>
            <ta e="T253" id="Seg_14345" s="T252">TURK:disc</ta>
            <ta e="T260" id="Seg_14346" s="T259">RUS:gram</ta>
            <ta e="T262" id="Seg_14347" s="T261">TURK:disc</ta>
            <ta e="T266" id="Seg_14348" s="T265">TURK:core</ta>
            <ta e="T268" id="Seg_14349" s="T267">RUS:gram</ta>
            <ta e="T269" id="Seg_14350" s="T268">TAT:cult</ta>
            <ta e="T294" id="Seg_14351" s="T293">RUS:cult</ta>
            <ta e="T300" id="Seg_14352" s="T299">RUS:gram</ta>
            <ta e="T305" id="Seg_14353" s="T304">RUS:gram</ta>
            <ta e="T311" id="Seg_14354" s="T310">RUS:gram</ta>
            <ta e="T317" id="Seg_14355" s="T316">RUS:mod</ta>
            <ta e="T320" id="Seg_14356" s="T319">RUS:gram</ta>
            <ta e="T321" id="Seg_14357" s="T320">RUS:mod</ta>
            <ta e="T323" id="Seg_14358" s="T322">%TURK:cult</ta>
            <ta e="T325" id="Seg_14359" s="T324">RUS:gram</ta>
            <ta e="T327" id="Seg_14360" s="T326">TURK:disc</ta>
            <ta e="T330" id="Seg_14361" s="T329">%TURK:cult</ta>
            <ta e="T333" id="Seg_14362" s="T332">TURK:gram(INDEF)</ta>
            <ta e="T339" id="Seg_14363" s="T338">RUS:gram</ta>
            <ta e="T342" id="Seg_14364" s="T341">RUS:gram</ta>
            <ta e="T343" id="Seg_14365" s="T342">TAT:cult</ta>
            <ta e="T345" id="Seg_14366" s="T344">RUS:mod</ta>
            <ta e="T363" id="Seg_14367" s="T362">RUS:core</ta>
            <ta e="T364" id="Seg_14368" s="T363">RUS:gram</ta>
            <ta e="T367" id="Seg_14369" s="T366">RUS:gram</ta>
            <ta e="T375" id="Seg_14370" s="T374">RUS:core</ta>
            <ta e="T377" id="Seg_14371" s="T376">RUS:gram</ta>
            <ta e="T383" id="Seg_14372" s="T382">TURK:disc</ta>
            <ta e="T385" id="Seg_14373" s="T384">RUS:gram</ta>
            <ta e="T386" id="Seg_14374" s="T385">RUS:core</ta>
            <ta e="T389" id="Seg_14375" s="T388">RUS:core</ta>
            <ta e="T394" id="Seg_14376" s="T393">RUS:core</ta>
            <ta e="T395" id="Seg_14377" s="T394">RUS:gram</ta>
            <ta e="T398" id="Seg_14378" s="T397">RUS:gram</ta>
            <ta e="T402" id="Seg_14379" s="T401">TURK:core</ta>
            <ta e="T406" id="Seg_14380" s="T405">RUS:core</ta>
            <ta e="T408" id="Seg_14381" s="T407">RUS:core</ta>
            <ta e="T414" id="Seg_14382" s="T413">RUS:core</ta>
            <ta e="T417" id="Seg_14383" s="T416">RUS:gram</ta>
            <ta e="T420" id="Seg_14384" s="T419">RUS:gram</ta>
            <ta e="T431" id="Seg_14385" s="T430">RUS:gram</ta>
            <ta e="T432" id="Seg_14386" s="T431">RUS:core</ta>
            <ta e="T439" id="Seg_14387" s="T438">RUS:gram</ta>
            <ta e="T447" id="Seg_14388" s="T446">TURK:core</ta>
            <ta e="T462" id="Seg_14389" s="T461">RUS:gram</ta>
            <ta e="T465" id="Seg_14390" s="T464">RUS:gram</ta>
            <ta e="T474" id="Seg_14391" s="T473">TURK:disc</ta>
            <ta e="T476" id="Seg_14392" s="T475">TURK:disc</ta>
            <ta e="T477" id="Seg_14393" s="T476">RUS:gram</ta>
            <ta e="T483" id="Seg_14394" s="T482">RUS:gram</ta>
            <ta e="T486" id="Seg_14395" s="T485">RUS:gram</ta>
            <ta e="T490" id="Seg_14396" s="T489">TURK:core</ta>
            <ta e="T499" id="Seg_14397" s="T498">TURK:disc</ta>
            <ta e="T504" id="Seg_14398" s="T503">RUS:core</ta>
            <ta e="T507" id="Seg_14399" s="T506">TURK:disc</ta>
            <ta e="T520" id="Seg_14400" s="T519">RUS:gram</ta>
            <ta e="T530" id="Seg_14401" s="T529">RUS:gram</ta>
            <ta e="T532" id="Seg_14402" s="T531">TURK:disc</ta>
            <ta e="T533" id="Seg_14403" s="T532">RUS:gram</ta>
            <ta e="T535" id="Seg_14404" s="T534">RUS:gram</ta>
            <ta e="T542" id="Seg_14405" s="T541">RUS:gram</ta>
            <ta e="T550" id="Seg_14406" s="T549">TURK:disc</ta>
            <ta e="T559" id="Seg_14407" s="T558">RUS:gram</ta>
            <ta e="T573" id="Seg_14408" s="T572">RUS:cult</ta>
            <ta e="T578" id="Seg_14409" s="T577">TURK:disc</ta>
            <ta e="T583" id="Seg_14410" s="T582">RUS:gram</ta>
            <ta e="T585" id="Seg_14411" s="T584">RUS:gram</ta>
            <ta e="T590" id="Seg_14412" s="T589">RUS:gram</ta>
            <ta e="T593" id="Seg_14413" s="T592">RUS:mod</ta>
            <ta e="T595" id="Seg_14414" s="T594">RUS:gram</ta>
            <ta e="T605" id="Seg_14415" s="T604">RUS:gram</ta>
            <ta e="T607" id="Seg_14416" s="T606">RUS:mod</ta>
            <ta e="T611" id="Seg_14417" s="T610">RUS:gram</ta>
            <ta e="T618" id="Seg_14418" s="T617">RUS:gram</ta>
            <ta e="T624" id="Seg_14419" s="T623">TURK:disc</ta>
            <ta e="T625" id="Seg_14420" s="T624">TURK:core</ta>
            <ta e="T628" id="Seg_14421" s="T627">RUS:gram</ta>
            <ta e="T632" id="Seg_14422" s="T631">RUS:cult</ta>
            <ta e="T633" id="Seg_14423" s="T632">RUS:gram</ta>
            <ta e="T635" id="Seg_14424" s="T634">RUS:cult</ta>
            <ta e="T642" id="Seg_14425" s="T641">RUS:cult</ta>
            <ta e="T643" id="Seg_14426" s="T642">RUS:gram</ta>
            <ta e="T645" id="Seg_14427" s="T644">RUS:cult</ta>
            <ta e="T648" id="Seg_14428" s="T647">RUS:disc</ta>
            <ta e="T659" id="Seg_14429" s="T658">RUS:gram</ta>
            <ta e="T662" id="Seg_14430" s="T661">RUS:disc</ta>
            <ta e="T667" id="Seg_14431" s="T666">RUS:cult</ta>
            <ta e="T668" id="Seg_14432" s="T667">RUS:gram</ta>
            <ta e="T670" id="Seg_14433" s="T669">RUS:cult</ta>
            <ta e="T680" id="Seg_14434" s="T679">TURK:core</ta>
            <ta e="T686" id="Seg_14435" s="T685">RUS:cult</ta>
            <ta e="T687" id="Seg_14436" s="T686">RUS:gram</ta>
            <ta e="T689" id="Seg_14437" s="T688">RUS:cult</ta>
            <ta e="T690" id="Seg_14438" s="T689">RUS:gram</ta>
            <ta e="T694" id="Seg_14439" s="T693">TURK:disc</ta>
            <ta e="T695" id="Seg_14440" s="T694">RUS:core</ta>
            <ta e="T700" id="Seg_14441" s="T699">RUS:gram</ta>
            <ta e="T708" id="Seg_14442" s="T707">TURK:disc</ta>
            <ta e="T713" id="Seg_14443" s="T712">TURK:disc</ta>
            <ta e="T715" id="Seg_14444" s="T714">TURK:disc</ta>
            <ta e="T720" id="Seg_14445" s="T719">TURK:disc</ta>
            <ta e="T722" id="Seg_14446" s="T721">RUS:cult</ta>
            <ta e="T724" id="Seg_14447" s="T723">TURK:disc</ta>
            <ta e="T730" id="Seg_14448" s="T729">RUS:gram</ta>
            <ta e="T741" id="Seg_14449" s="T740">RUS:disc</ta>
            <ta e="T747" id="Seg_14450" s="T746">RUS:gram</ta>
            <ta e="T760" id="Seg_14451" s="T759">TURK:disc</ta>
            <ta e="T773" id="Seg_14452" s="T772">TAT:cult</ta>
            <ta e="T774" id="Seg_14453" s="T773">RUS:gram</ta>
            <ta e="T778" id="Seg_14454" s="T777">RUS:mod</ta>
            <ta e="T781" id="Seg_14455" s="T780">TAT:cult</ta>
            <ta e="T782" id="Seg_14456" s="T781">TURK:disc</ta>
            <ta e="T785" id="Seg_14457" s="T784">TURK:core</ta>
            <ta e="T789" id="Seg_14458" s="T788">TURK:disc</ta>
            <ta e="T811" id="Seg_14459" s="T810">RUS:gram</ta>
            <ta e="T818" id="Seg_14460" s="T817">TURK:disc</ta>
            <ta e="T820" id="Seg_14461" s="T819">RUS:gram</ta>
            <ta e="T828" id="Seg_14462" s="T827">RUS:gram</ta>
            <ta e="T830" id="Seg_14463" s="T829">TURK:disc</ta>
            <ta e="T835" id="Seg_14464" s="T834">RUS:gram</ta>
            <ta e="T843" id="Seg_14465" s="T842">RUS:core</ta>
            <ta e="T859" id="Seg_14466" s="T858">TURK:disc</ta>
            <ta e="T865" id="Seg_14467" s="T864">TURK:disc</ta>
            <ta e="T871" id="Seg_14468" s="T870">RUS:disc</ta>
            <ta e="T872" id="Seg_14469" s="T871">RUS:cult</ta>
            <ta e="T878" id="Seg_14470" s="T877">TURK:disc</ta>
            <ta e="T881" id="Seg_14471" s="T880">RUS:core</ta>
            <ta e="T884" id="Seg_14472" s="T883">RUS:gram</ta>
            <ta e="T896" id="Seg_14473" s="T895">RUS:gram</ta>
            <ta e="T897" id="Seg_14474" s="T896">RUS:gram</ta>
            <ta e="T905" id="Seg_14475" s="T904">RUS:gram</ta>
            <ta e="T908" id="Seg_14476" s="T907">RUS:gram</ta>
            <ta e="T910" id="Seg_14477" s="T909">RUS:gram</ta>
            <ta e="T913" id="Seg_14478" s="T912">RUS:gram</ta>
            <ta e="T918" id="Seg_14479" s="T917">RUS:gram</ta>
            <ta e="T919" id="Seg_14480" s="T918">TURK:disc</ta>
            <ta e="T923" id="Seg_14481" s="T922">TURK:disc</ta>
            <ta e="T926" id="Seg_14482" s="T925">RUS:core</ta>
            <ta e="T941" id="Seg_14483" s="T940">TURK:cult</ta>
            <ta e="T946" id="Seg_14484" s="T945">TURK:gram(INDEF)</ta>
            <ta e="T948" id="Seg_14485" s="T947">RUS:disc</ta>
            <ta e="T956" id="Seg_14486" s="T955">RUS:core</ta>
            <ta e="T968" id="Seg_14487" s="T967">RUS:core</ta>
            <ta e="T970" id="Seg_14488" s="T969">TURK:disc</ta>
            <ta e="T972" id="Seg_14489" s="T971">RUS:core</ta>
            <ta e="T991" id="Seg_14490" s="T990">RUS:core</ta>
            <ta e="T995" id="Seg_14491" s="T994">TURK:core</ta>
            <ta e="T998" id="Seg_14492" s="T997">RUS:gram</ta>
            <ta e="T1002" id="Seg_14493" s="T1001">TURK:cult</ta>
            <ta e="T1013" id="Seg_14494" s="T1012">RUS:gram</ta>
            <ta e="T1016" id="Seg_14495" s="T1015">TURK:disc</ta>
            <ta e="T1028" id="Seg_14496" s="T1027">TURK:core</ta>
            <ta e="T1029" id="Seg_14497" s="T1028">TURK:cult</ta>
            <ta e="T1031" id="Seg_14498" s="T1030">TURK:disc</ta>
            <ta e="T1034" id="Seg_14499" s="T1033">RUS:gram</ta>
            <ta e="T1044" id="Seg_14500" s="T1043">RUS:gram</ta>
            <ta e="T1051" id="Seg_14501" s="T1050">RUS:core</ta>
            <ta e="T1052" id="Seg_14502" s="T1051">TURK:cult</ta>
            <ta e="T1054" id="Seg_14503" s="T1053">RUS:mod</ta>
            <ta e="T1055" id="Seg_14504" s="T1054">TURK:gram(INDEF)</ta>
            <ta e="T1058" id="Seg_14505" s="T1057">RUS:mod</ta>
            <ta e="T1059" id="Seg_14506" s="T1058">TURK:cult</ta>
            <ta e="T1062" id="Seg_14507" s="T1061">TURK:disc</ta>
            <ta e="T1065" id="Seg_14508" s="T1064">RUS:gram</ta>
            <ta e="T1085" id="Seg_14509" s="T1084">RUS:mod</ta>
            <ta e="T1088" id="Seg_14510" s="T1087">RUS:gram(INDEF)</ta>
            <ta e="T1091" id="Seg_14511" s="T1090">RUS:gram</ta>
            <ta e="T1094" id="Seg_14512" s="T1093">TURK:cult</ta>
            <ta e="T1095" id="Seg_14513" s="T1094">TURK:disc</ta>
            <ta e="T1101" id="Seg_14514" s="T1100">TURK:disc</ta>
            <ta e="T1109" id="Seg_14515" s="T1108">RUS:gram</ta>
            <ta e="T1111" id="Seg_14516" s="T1110">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T14" id="Seg_14517" s="T13">RUS:int</ta>
            <ta e="T17" id="Seg_14518" s="T16">RUS:int</ta>
            <ta e="T308" id="Seg_14519" s="T306">RUS:int</ta>
            <ta e="T353" id="Seg_14520" s="T352">RUS:int</ta>
            <ta e="T630" id="Seg_14521" s="T629">RUS:int</ta>
            <ta e="T718" id="Seg_14522" s="T715">RUS:int</ta>
            <ta e="T802" id="Seg_14523" s="T801">RUS:int</ta>
            <ta e="T805" id="Seg_14524" s="T804">RUS:int</ta>
            <ta e="T841" id="Seg_14525" s="T840">RUS:int</ta>
            <ta e="T982" id="Seg_14526" s="T981">RUS:int</ta>
            <ta e="T1087" id="Seg_14527" s="T1086">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T8" id="Seg_14528" s="T0">Летит — гремит, сидит — землю (?).</ta>
            <ta e="T10" id="Seg_14529" s="T8">Это ружье.</ta>
            <ta e="T17" id="Seg_14530" s="T11">Что это за загадка, в заду ягодка.</ta>
            <ta e="T19" id="Seg_14531" s="T17">Это ружье.</ta>
            <ta e="T33" id="Seg_14532" s="T20">Когда нас, камасинцев, было много, я ходила на высокие горы, на Белогорье, там я ловила рыбу.</ta>
            <ta e="T36" id="Seg_14533" s="T33">И охотилась.</ta>
            <ta e="T41" id="Seg_14534" s="T36">Потом горы остались черными. [?]</ta>
            <ta e="T47" id="Seg_14535" s="T41">У меня силы уж нет, вся исчезла.</ta>
            <ta e="T57" id="Seg_14536" s="T47">А там, где чумы стояли, они все упали и исчезли.</ta>
            <ta e="T60" id="Seg_14537" s="T57">Только береста (свернулась?).</ta>
            <ta e="T66" id="Seg_14538" s="T61">Сейчас я ничего не вижу.</ta>
            <ta e="T70" id="Seg_14539" s="T66">Ничего не слышу.</ta>
            <ta e="T74" id="Seg_14540" s="T70">Вся моя сила ушла.</ta>
            <ta e="T78" id="Seg_14541" s="T74">Теперь я одна живу.</ta>
            <ta e="T84" id="Seg_14542" s="T78">Никого нет, все умерли мои камасинцы.</ta>
            <ta e="T90" id="Seg_14543" s="T85">Все мои дороги теперь остались [=заброшены?].</ta>
            <ta e="T93" id="Seg_14544" s="T90">Все травой поросли.</ta>
            <ta e="T98" id="Seg_14545" s="T94">Я стала [=была?] очень красивой.</ta>
            <ta e="T103" id="Seg_14546" s="T98">А куда эта красота ушла?</ta>
            <ta e="T109" id="Seg_14547" s="T103">Ушла к высоким горам, и нигде ее нет.</ta>
            <ta e="T115" id="Seg_14548" s="T110">Теперь она никогда не придет.</ta>
            <ta e="T123" id="Seg_14549" s="T117">Я стояла в переулке, идет (старик?).</ta>
            <ta e="T129" id="Seg_14550" s="T123">Я думаю: очень большой человек идет.</ta>
            <ta e="T133" id="Seg_14551" s="T129">Идет и фотоаппаратом снимает дома.</ta>
            <ta e="T144" id="Seg_14552" s="T133">Я стояла около дома, он [и] меня снял.</ta>
            <ta e="T150" id="Seg_14553" s="T145">У женщины была рыба.</ta>
            <ta e="T156" id="Seg_14554" s="T151">Ее невестка взяла ее рыбу.</ta>
            <ta e="T160" id="Seg_14555" s="T156">Потом пришла к ней и говорит:</ta>
            <ta e="T165" id="Seg_14556" s="T160">"Я сегодня принесу тебе рыбу".</ta>
            <ta e="T168" id="Seg_14557" s="T165">А я пришла.</ta>
            <ta e="T172" id="Seg_14558" s="T168">"Твою рыбу невестка забрала.</ta>
            <ta e="T175" id="Seg_14559" s="T172">Я не брала".</ta>
            <ta e="T181" id="Seg_14560" s="T176">Что она мне даст, немножко.</ta>
            <ta e="T184" id="Seg_14561" s="T181">Ну, немножко, хватит.</ta>
            <ta e="T190" id="Seg_14562" s="T184">Они сами продавали за деньги.</ta>
            <ta e="T195" id="Seg_14563" s="T190">А ты не дала денег.</ta>
            <ta e="T203" id="Seg_14564" s="T195">Сколько она даст, хватит.</ta>
            <ta e="T211" id="Seg_14565" s="T204">Потом я пришла к Анисье, она говорит:</ta>
            <ta e="T218" id="Seg_14566" s="T211">"Эля просила яиц, я собрала десять яиц.</ta>
            <ta e="T229" id="Seg_14567" s="T218">Я дам по десять копеек одно яйцо".</ta>
            <ta e="T242" id="Seg_14568" s="T230">Эля говорит: "Поедем к нам", а я говорю: "Нет, не поеду, очень далеко".</ta>
            <ta e="T244" id="Seg_14569" s="T242">"Почему далеко?</ta>
            <ta e="T245" id="Seg_14570" s="T244">Садись.</ta>
            <ta e="T249" id="Seg_14571" s="T245">В железную птицу [= в самолет].</ta>
            <ta e="T252" id="Seg_14572" s="T249">И скоро будешь там".</ta>
            <ta e="T259" id="Seg_14573" s="T252">"Нет, я боюсь садиться в железную птицу.</ta>
            <ta e="T261" id="Seg_14574" s="T259">А то умрешь".</ta>
            <ta e="T269" id="Seg_14575" s="T261">"Нет, не умрешь, там хорошо сидеть, как в доме.</ta>
            <ta e="T274" id="Seg_14576" s="T269">Скоро приедешь домой, у нас будешь жить.</ta>
            <ta e="T277" id="Seg_14577" s="T275">Втроем поедем".</ta>
            <ta e="T282" id="Seg_14578" s="T278">Вечером я пойду к одному мужчине.</ta>
            <ta e="T288" id="Seg_14579" s="T282">К одному человеку.</ta>
            <ta e="T289" id="Seg_14580" s="T288">Я скажу:</ta>
            <ta e="T294" id="Seg_14581" s="T289">"Отвези эту женщину в больницу.</ta>
            <ta e="T299" id="Seg_14582" s="T294">Ей бумагу дали.</ta>
            <ta e="T313" id="Seg_14583" s="T299">А если ты [ее] не повезешь, вызови скорую помощь, она приедет и заберет ее".</ta>
            <ta e="T319" id="Seg_14584" s="T313">Так я скажу ему, может, он послушает.</ta>
            <ta e="T328" id="Seg_14585" s="T319">А то ведь ты взял самовар, а люди говорят:</ta>
            <ta e="T337" id="Seg_14586" s="T328">"Самовар взял, а сам ничем не помогал этой женщине".</ta>
            <ta e="T347" id="Seg_14587" s="T338">"Твой отец умер, а дом (?) ведь ты взял".</ta>
            <ta e="T352" id="Seg_14588" s="T348">Собака жила-жила одна.</ta>
            <ta e="T359" id="Seg_14589" s="T352">"Скучно одной жить, пойду найду себе друга".</ta>
            <ta e="T363" id="Seg_14590" s="T359">Шла-шла, увидела зайца.</ta>
            <ta e="T366" id="Seg_14591" s="T363">"Давай вместе жить?"</ta>
            <ta e="T368" id="Seg_14592" s="T366">"Давай жить".</ta>
            <ta e="T374" id="Seg_14593" s="T368">Настала ночь, они легли спать.</ta>
            <ta e="T381" id="Seg_14594" s="T374">Заяц заснул, а собака не спит.</ta>
            <ta e="T384" id="Seg_14595" s="T381">Ночью она лает.</ta>
            <ta e="T387" id="Seg_14596" s="T384">А заяц испугался.</ta>
            <ta e="T390" id="Seg_14597" s="T387">У него сердце в пятки ушло.</ta>
            <ta e="T392" id="Seg_14598" s="T390">"Зачем ты лаешь?</ta>
            <ta e="T397" id="Seg_14599" s="T392">Волк придет и съест нас".</ta>
            <ta e="T404" id="Seg_14600" s="T397">Собака думает: "Мой друг плохой.</ta>
            <ta e="T409" id="Seg_14601" s="T404">Пойду поищу волка".</ta>
            <ta e="T411" id="Seg_14602" s="T409">Тогда он пошел.</ta>
            <ta e="T416" id="Seg_14603" s="T411">Идет-идет, [видит]: идет волк.</ta>
            <ta e="T419" id="Seg_14604" s="T416">"Давай с тобой жить?"</ta>
            <ta e="T421" id="Seg_14605" s="T419">"Давай жить".</ta>
            <ta e="T427" id="Seg_14606" s="T421">Настала ночь, они легли спать.</ta>
            <ta e="T430" id="Seg_14607" s="T427">Ночью собака залаяла.</ta>
            <ta e="T435" id="Seg_14608" s="T430">Волк говорит: Почему ты лаешь?</ta>
            <ta e="T441" id="Seg_14609" s="T435">Сейчас медведь придет и съест нас".</ta>
            <ta e="T445" id="Seg_14610" s="T441">Утром собака проснулась.</ta>
            <ta e="T452" id="Seg_14611" s="T445">"Мой друг нехорош, пойду я поищу медведя".</ta>
            <ta e="T455" id="Seg_14612" s="T452">Она пошла.</ta>
            <ta e="T459" id="Seg_14613" s="T455">Идет-идет, [видит]: медведь идет.</ta>
            <ta e="T464" id="Seg_14614" s="T459">"Медведь-медведь, давай с тобой жить?"</ta>
            <ta e="T465" id="Seg_14615" s="T464">"Давай".</ta>
            <ta e="T471" id="Seg_14616" s="T465">Настала ночь, они легли спать.</ta>
            <ta e="T476" id="Seg_14617" s="T471">Ночью собака залаяла.</ta>
            <ta e="T480" id="Seg_14618" s="T476">А медведь: "Что ты лаешь?</ta>
            <ta e="T485" id="Seg_14619" s="T480">Человек придет и убьет нас".</ta>
            <ta e="T494" id="Seg_14620" s="T485">[Собака] думает: "Нехороший друг, пойду человека искать".</ta>
            <ta e="T498" id="Seg_14621" s="T494">Утром она встала и пошла.</ta>
            <ta e="T502" id="Seg_14622" s="T498">Шла-шла через лес.</ta>
            <ta e="T505" id="Seg_14623" s="T502">Потом вышла в степь.</ta>
            <ta e="T509" id="Seg_14624" s="T505">Видит: человек идет.</ta>
            <ta e="T514" id="Seg_14625" s="T509">За дровами, с лошадью.</ta>
            <ta e="T521" id="Seg_14626" s="T514">Тогда она говорит: "Человек, давай жить [вместе]".</ta>
            <ta e="T525" id="Seg_14627" s="T521">Он взял ее домой.</ta>
            <ta e="T527" id="Seg_14628" s="T525">Они легли спать.</ta>
            <ta e="T534" id="Seg_14629" s="T527">Человек заснул, а собака залаяла.</ta>
            <ta e="T538" id="Seg_14630" s="T534">Человек встал и говорит:</ta>
            <ta e="T549" id="Seg_14631" s="T538">"Ты голодная, поешь и спи, и мне (дай?) спать".</ta>
            <ta e="T550" id="Seg_14632" s="T549">Все!</ta>
            <ta e="T555" id="Seg_14633" s="T551">Один человек пошел за дровами.</ta>
            <ta e="T560" id="Seg_14634" s="T555">Взял дрова и идет.</ta>
            <ta e="T563" id="Seg_14635" s="T560">К нему пришел медведь.</ta>
            <ta e="T567" id="Seg_14636" s="T563">"Я тебя сейчас съем".</ta>
            <ta e="T571" id="Seg_14637" s="T567">"Не ешь меня.</ta>
            <ta e="T576" id="Seg_14638" s="T571">Охотники идут сюда, они тебя убьют".</ta>
            <ta e="T579" id="Seg_14639" s="T576">Тот испугался.</ta>
            <ta e="T582" id="Seg_14640" s="T579">Укрой меня поленьями.</ta>
            <ta e="T584" id="Seg_14641" s="T582">И уходи.</ta>
            <ta e="T589" id="Seg_14642" s="T584">Чтобы они меня не увидели".</ta>
            <ta e="T596" id="Seg_14643" s="T589">А тот говорит: "Если закрывать, то надо [тебя] связать".</ta>
            <ta e="T601" id="Seg_14644" s="T596">Тогда он говорит: "Свяжи".</ta>
            <ta e="T604" id="Seg_14645" s="T601">Тот его связал.</ta>
            <ta e="T610" id="Seg_14646" s="T604">"А теперь надо порубить тебя топором".</ta>
            <ta e="T620" id="Seg_14647" s="T610">Потом он зарубил медведя топором, и медведь умер.</ta>
            <ta e="T627" id="Seg_14648" s="T621">Медведь с человеком породнились.</ta>
            <ta e="T630" id="Seg_14649" s="T627">Она стали сажать репу.</ta>
            <ta e="T635" id="Seg_14650" s="T630">"Мне корешки, а тебе вершки".</ta>
            <ta e="T637" id="Seg_14651" s="T635">Они посеяли ее.</ta>
            <ta e="T639" id="Seg_14652" s="T637">Она выросла.</ta>
            <ta e="T645" id="Seg_14653" s="T639">Человек взял корешки, а медведь — вершки.</ta>
            <ta e="T651" id="Seg_14654" s="T645">Потом медведь [думает]: "Ну, человек меня обманул".</ta>
            <ta e="T655" id="Seg_14655" s="T651">Потом опять стало тепло.</ta>
            <ta e="T661" id="Seg_14656" s="T655">Человек говорит медведю: "Давай снова сеять".</ta>
            <ta e="T670" id="Seg_14657" s="T661">Медведь [говорит]: "Ты мне дай корешки, а себе вершки".</ta>
            <ta e="T674" id="Seg_14658" s="T670">Ну, пшеницу…</ta>
            <ta e="T677" id="Seg_14659" s="T674">…они посеяли пшеницу.</ta>
            <ta e="T682" id="Seg_14660" s="T677">Она выросла, очень хорошая была.</ta>
            <ta e="T689" id="Seg_14661" s="T682">Человек взял вершки, а медведю [достались] корешки.</ta>
            <ta e="T696" id="Seg_14662" s="T689">И потом их дружба кончилась.</ta>
            <ta e="T701" id="Seg_14663" s="T697">Жили мужчина и женщина.</ta>
            <ta e="T705" id="Seg_14664" s="T701">У них не было детей.</ta>
            <ta e="T711" id="Seg_14665" s="T705">Однажды выпало много снега.</ta>
            <ta e="T721" id="Seg_14666" s="T711">Одни дети катаются на санках, другие играют.</ta>
            <ta e="T725" id="Seg_14667" s="T721">Делают снеговиков, все делают.</ta>
            <ta e="T729" id="Seg_14668" s="T725">Они сделали большую женщину.</ta>
            <ta e="T740" id="Seg_14669" s="T729">А мужчина смотрел-смотрел и говорит женщине: "Пойдем на улицу".</ta>
            <ta e="T742" id="Seg_14670" s="T740">"Ну пойдем".</ta>
            <ta e="T749" id="Seg_14671" s="T742">Они пошли, посмотрели, потом стали лепить девушку".</ta>
            <ta e="T757" id="Seg_14672" s="T749">Сделали… голову сделали, глаза, нос, рот.</ta>
            <ta e="T763" id="Seg_14673" s="T757">Потом они нарисовали углем глаза.</ta>
            <ta e="T767" id="Seg_14674" s="T763">Тогда она (моргнула?).</ta>
            <ta e="T770" id="Seg_14675" s="T767">Потом мужчина ушел.</ta>
            <ta e="T781" id="Seg_14676" s="T770">Он пошел в дом, а они тоже с ним пошли в дом.</ta>
            <ta e="T791" id="Seg_14677" s="T781">Очень ей хорошо стало, они ребенку все покупали.</ta>
            <ta e="T798" id="Seg_14678" s="T792">Купили ей сапоги, красивые, красные.</ta>
            <ta e="T805" id="Seg_14679" s="T798">Потом купили ленту, очень красивую, сатиновую.</ta>
            <ta e="T810" id="Seg_14680" s="T805">Потом стало тепло.</ta>
            <ta e="T814" id="Seg_14681" s="T810">Девушка сидит.</ta>
            <ta e="T816" id="Seg_14682" s="T814">Вода течет.</ta>
            <ta e="T819" id="Seg_14683" s="T816">Ей страшно.</ta>
            <ta e="T827" id="Seg_14684" s="T819">А женщина спрашивает: "Что ты плачешь?"</ta>
            <ta e="T834" id="Seg_14685" s="T827">А у них самих слезы текут по лицу.</ta>
            <ta e="T840" id="Seg_14686" s="T834">Потом опять вышло солнце.</ta>
            <ta e="T844" id="Seg_14687" s="T840">Всякая трава и цветы растут.</ta>
            <ta e="T849" id="Seg_14688" s="T844">Пришли девушки: "Пойдем с нами!"</ta>
            <ta e="T851" id="Seg_14689" s="T849">Они зовут девушку.</ta>
            <ta e="T858" id="Seg_14690" s="T851">Женщина говорит: "Иди, что ты сидишь?"</ta>
            <ta e="T866" id="Seg_14691" s="T858">"Нет, не пойду, солнце… у меня голова будет болеть.</ta>
            <ta e="T870" id="Seg_14692" s="T866">Солнце мне голову напечет".</ta>
            <ta e="T874" id="Seg_14693" s="T870">"Ну, надень платок".</ta>
            <ta e="T877" id="Seg_14694" s="T874">Тогда она пошла.</ta>
            <ta e="T880" id="Seg_14695" s="T877">Все девушки играют.</ta>
            <ta e="T883" id="Seg_14696" s="T880">Цветы срывают.</ta>
            <ta e="T887" id="Seg_14697" s="T883">А она сидит.</ta>
            <ta e="T899" id="Seg_14698" s="T889">Потом они сделали очень большой костер и стали (прыгать?) [через] огонь.</ta>
            <ta e="T904" id="Seg_14699" s="T899">Говорят: "Почему ты не прыгаешь?"</ta>
            <ta e="T907" id="Seg_14700" s="T904">А ей страшно.</ta>
            <ta e="T912" id="Seg_14701" s="T907">Тогда они стали смеяться над ней.</ta>
            <ta e="T923" id="Seg_14702" s="T912">А она встала, прыгнула, и взлетела вверх над огнем.</ta>
            <ta e="T933" id="Seg_14703" s="T925">Однажды мой теленок, у меня был очень красивый белый теленок.</ta>
            <ta e="T941" id="Seg_14704" s="T933">Однажды вечером я пошла напоить его молоком.</ta>
            <ta e="T947" id="Seg_14705" s="T941">Я кричала, я искала, его нигде нет.</ta>
            <ta e="T953" id="Seg_14706" s="T947">Ну, куда ушел теленок, я не знаю.</ta>
            <ta e="T957" id="Seg_14707" s="T953">Утром я встала, везде ходила.</ta>
            <ta e="T963" id="Seg_14708" s="T957">К реке ходила, думала, он упал в реку.</ta>
            <ta e="T966" id="Seg_14709" s="T963">Его нигде нет.</ta>
            <ta e="T969" id="Seg_14710" s="T966">Я думала: волк его съел.</ta>
            <ta e="T975" id="Seg_14711" s="T969">Нет, не съел.</ta>
            <ta e="T980" id="Seg_14712" s="T975">А люди там косили траву.</ta>
            <ta e="T988" id="Seg_14713" s="T980">Они, наверное, взяли его, связали ему ноги, отнесли домой.</ta>
            <ta e="T992" id="Seg_14714" s="T988">Это был волк с двумя ногами.</ta>
            <ta e="T997" id="Seg_14715" s="T992">Мой родственник приехал в Кан-Оклер.</ta>
            <ta e="T1004" id="Seg_14716" s="T997">И рассказывает: у людей есть (по?) две коровы.</ta>
            <ta e="T1012" id="Seg_14717" s="T1004">Очень жирные.</ta>
            <ta e="T1016" id="Seg_14718" s="T1012">А у нас [если] будут жить.</ta>
            <ta e="T1019" id="Seg_14719" s="T1016">Станут очень худыми.</ta>
            <ta e="T1025" id="Seg_14720" s="T1019">Траву не ест, воду не пьет.</ta>
            <ta e="T1027" id="Seg_14721" s="T1025">Ест мало.</ta>
            <ta e="T1037" id="Seg_14722" s="T1027">Другие коровы приходят и едят траву, а он не ест. </ta>
            <ta e="T1041" id="Seg_14723" s="T1038">"Не знаю", - говорит.</ta>
            <ta e="T1046" id="Seg_14724" s="T1041">Двор такой или что.</ta>
            <ta e="T1052" id="Seg_14725" s="T1046">Мы взяли корову у его сестры.</ta>
            <ta e="T1057" id="Seg_14726" s="T1052">Она ведь ничего не знает.</ta>
            <ta e="T1064" id="Seg_14727" s="T1057">[Эта] корова тоже такой стала, худой.</ta>
            <ta e="T1070" id="Seg_14728" s="T1064">А [раньше?] жира у нее было много.</ta>
            <ta e="T1075" id="Seg_14729" s="T1070">Она жила с нами и стала худой.</ta>
            <ta e="T1084" id="Seg_14730" s="T1076">Она жила с девушкой/дочерью, после этого она стала (другой?).</ta>
            <ta e="T1090" id="Seg_14731" s="T1084">Может, она что-нибудь выкопала?</ta>
            <ta e="T1097" id="Seg_14732" s="T1090">И теперь у нее коровы не (стоят?).</ta>
            <ta e="T1104" id="Seg_14733" s="T1098">Завтра вы…</ta>
            <ta e="T1107" id="Seg_14734" s="T1104">Завтра вы уезжаете.</ta>
            <ta e="T1110" id="Seg_14735" s="T1107">Вы уезжаете, давайте обнимемся.</ta>
            <ta e="T1112" id="Seg_14736" s="T1110">И поцелуемся.</ta>
            <ta e="T1115" id="Seg_14737" s="T1112">Потом езжайте с богом.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T8" id="Seg_14738" s="T0">When it flies, it shouts, when it sits down, it (?) the ground.</ta>
            <ta e="T10" id="Seg_14739" s="T8">That's a gun.</ta>
            <ta e="T17" id="Seg_14740" s="T11">What for a riddle, in the ass there is a berry.</ta>
            <ta e="T19" id="Seg_14741" s="T17">That's a gun.</ta>
            <ta e="T33" id="Seg_14742" s="T20">When we, the Kamassians, were numerous, I used to go in high mountains, in Belogorye, there I caught fish.</ta>
            <ta e="T36" id="Seg_14743" s="T33">And I hunted.</ta>
            <ta e="T41" id="Seg_14744" s="T36">Then the mountains remained black. [?]</ta>
            <ta e="T47" id="Seg_14745" s="T41">I have no more force, it died [= disappeared].</ta>
            <ta e="T57" id="Seg_14746" s="T47">And where the tents stood, they fell down and vanished.</ta>
            <ta e="T60" id="Seg_14747" s="T57">Only the birchbark [remained and] (curled up?).</ta>
            <ta e="T66" id="Seg_14748" s="T61">Now I don't see anything.</ta>
            <ta e="T70" id="Seg_14749" s="T66">I don't hear anything.</ta>
            <ta e="T74" id="Seg_14750" s="T70">My force went away.</ta>
            <ta e="T78" id="Seg_14751" s="T74">Now I am living alone.</ta>
            <ta e="T84" id="Seg_14752" s="T78">There is nobody [left], all my Kamassians are dead.</ta>
            <ta e="T90" id="Seg_14753" s="T85">Now all my roads have been left [= abandone(d)?].</ta>
            <ta e="T93" id="Seg_14754" s="T90">They are all overgrown with grass.</ta>
            <ta e="T98" id="Seg_14755" s="T94">I became [= was?] very beautiful.</ta>
            <ta e="T103" id="Seg_14756" s="T98">Where has this beauty gone?</ta>
            <ta e="T109" id="Seg_14757" s="T103">It has gone to the high mountains and is not to be found anywhere.</ta>
            <ta e="T115" id="Seg_14758" s="T110">Now it will never come.</ta>
            <ta e="T123" id="Seg_14759" s="T117">I stood in a small (street?), an old (man?) comes.</ta>
            <ta e="T129" id="Seg_14760" s="T123">I think: a very big man is coming.</ta>
            <ta e="T133" id="Seg_14761" s="T129">He's going and taking pictures of houses with a camera.</ta>
            <ta e="T144" id="Seg_14762" s="T133">I was standing (near?) a house, he took picture of me [as well].</ta>
            <ta e="T150" id="Seg_14763" s="T145">A woman had some fish.</ta>
            <ta e="T156" id="Seg_14764" s="T151">Her daughter-in-law took her fish.</ta>
            <ta e="T160" id="Seg_14765" s="T156">Then he came to her and says:</ta>
            <ta e="T165" id="Seg_14766" s="T160">"I'll bring you fish today."</ta>
            <ta e="T168" id="Seg_14767" s="T165">And I came.</ta>
            <ta e="T172" id="Seg_14768" s="T168">"Your daughter-in-law took your fish.</ta>
            <ta e="T175" id="Seg_14769" s="T172">I didn't take [it]."</ta>
            <ta e="T181" id="Seg_14770" s="T176">What will s/he give me, [only] a few.</ta>
            <ta e="T184" id="Seg_14771" s="T181">Well, a few, it's enough.</ta>
            <ta e="T190" id="Seg_14772" s="T184">They sold it for money themselves.</ta>
            <ta e="T195" id="Seg_14773" s="T190">And you didn't give money.</ta>
            <ta e="T203" id="Seg_14774" s="T195">What she'll give to you will be enough.</ta>
            <ta e="T211" id="Seg_14775" s="T204">Then I came to Anisya, she said:</ta>
            <ta e="T218" id="Seg_14776" s="T211">"Elya asked for eggs, I gathered ten eggs.</ta>
            <ta e="T229" id="Seg_14777" s="T218">I'll give her one egg for ten kopecks.</ta>
            <ta e="T242" id="Seg_14778" s="T230">Elya says: "Let's go to us", and I say: "No, I won't go, it's too far."</ta>
            <ta e="T244" id="Seg_14779" s="T242">"Why is it far?</ta>
            <ta e="T245" id="Seg_14780" s="T244">Sit [in].</ta>
            <ta e="T249" id="Seg_14781" s="T245">In an iron bird [= in a plane].</ta>
            <ta e="T252" id="Seg_14782" s="T249">And you'll arrive soon."</ta>
            <ta e="T259" id="Seg_14783" s="T252">"No, I'm afraid of sitting on an iron bird.</ta>
            <ta e="T261" id="Seg_14784" s="T259">[Because] one may die."</ta>
            <ta e="T269" id="Seg_14785" s="T261">"No, you won't die, it's good to sit here, like in a house.</ta>
            <ta e="T274" id="Seg_14786" s="T269">You'll arrive home [= in our home] soon, you'll live at our place.</ta>
            <ta e="T277" id="Seg_14787" s="T275">We'll go three together."</ta>
            <ta e="T282" id="Seg_14788" s="T278">Tonight I'll go to a man.</ta>
            <ta e="T288" id="Seg_14789" s="T282">To a man.</ta>
            <ta e="T289" id="Seg_14790" s="T288">I'll say:</ta>
            <ta e="T294" id="Seg_14791" s="T289">"Carry this woman to the hospital.</ta>
            <ta e="T299" id="Seg_14792" s="T294">She was given a paper.</ta>
            <ta e="T313" id="Seg_14793" s="T299">And if you won't take [her], call the ambulance, they'll come and carry her."</ta>
            <ta e="T319" id="Seg_14794" s="T313">I'll say him so, maybe he'll obey.</ta>
            <ta e="T328" id="Seg_14795" s="T319">Because you'va taken the samovar, and people say:</ta>
            <ta e="T337" id="Seg_14796" s="T328">"You've taken the samovar, but [you]'ve never helped this woman."</ta>
            <ta e="T347" id="Seg_14797" s="T338">"Your father died, but the house (?) was yours".</ta>
            <ta e="T352" id="Seg_14798" s="T348">A dog lived alone for a long time.</ta>
            <ta e="T359" id="Seg_14799" s="T352">"It is boring to live alone, let me go and find myself a friend."</ta>
            <ta e="T363" id="Seg_14800" s="T359">It went and went and saw a hare.</ta>
            <ta e="T366" id="Seg_14801" s="T363">"Let's live together."</ta>
            <ta e="T368" id="Seg_14802" s="T366">"Let's live [together]."</ta>
            <ta e="T374" id="Seg_14803" s="T368">Then the night came and they laid down to sleep.</ta>
            <ta e="T381" id="Seg_14804" s="T374">The hare fell asleep, and the dog doesn't sleep.</ta>
            <ta e="T384" id="Seg_14805" s="T381">In the night it's barking.</ta>
            <ta e="T387" id="Seg_14806" s="T384">And the hare was afraid.</ta>
            <ta e="T390" id="Seg_14807" s="T387">Its heart sank.</ta>
            <ta e="T392" id="Seg_14808" s="T390">"Why are you barking?</ta>
            <ta e="T397" id="Seg_14809" s="T392">A wolf comes and eats us."</ta>
            <ta e="T404" id="Seg_14810" s="T397">The dog thinks: "My friend isn't good.</ta>
            <ta e="T409" id="Seg_14811" s="T404">I'll go and look for the wolf."</ta>
            <ta e="T411" id="Seg_14812" s="T409">Then he went.</ta>
            <ta e="T416" id="Seg_14813" s="T411">It walked for some time, [and then] a wolf is coming.</ta>
            <ta e="T419" id="Seg_14814" s="T416">"Let's live together."</ta>
            <ta e="T421" id="Seg_14815" s="T419">"Let's live [together]."</ta>
            <ta e="T427" id="Seg_14816" s="T421">The evening came, they went to sleep.</ta>
            <ta e="T430" id="Seg_14817" s="T427">In the night, the dog barked.</ta>
            <ta e="T435" id="Seg_14818" s="T430">The wolf says: "Why are you barking?</ta>
            <ta e="T441" id="Seg_14819" s="T435">A bear comes and eats us."</ta>
            <ta e="T445" id="Seg_14820" s="T441">In the morning, the dog got up:</ta>
            <ta e="T452" id="Seg_14821" s="T445">"My friend isn't good, let me go and look for the bear."</ta>
            <ta e="T455" id="Seg_14822" s="T452">Then it went.</ta>
            <ta e="T459" id="Seg_14823" s="T455">It walked for some time, [the it sees:] a bear is coming.</ta>
            <ta e="T464" id="Seg_14824" s="T459">"Bear, let's live together."</ta>
            <ta e="T465" id="Seg_14825" s="T464">"Let's do it."</ta>
            <ta e="T471" id="Seg_14826" s="T465">The night came, they went to sleep.</ta>
            <ta e="T476" id="Seg_14827" s="T471">In the night, the dog barked.</ta>
            <ta e="T480" id="Seg_14828" s="T476">The bear [says]: "Why are you barking?</ta>
            <ta e="T485" id="Seg_14829" s="T480">The man'll come and kill us."</ta>
            <ta e="T494" id="Seg_14830" s="T485">[The dog] thinks: "My friend isn't good, let me go and look for the man.</ta>
            <ta e="T498" id="Seg_14831" s="T494">In the morning it got up and left.</ta>
            <ta e="T502" id="Seg_14832" s="T498">It walked through the forest.</ta>
            <ta e="T505" id="Seg_14833" s="T502">Then it reached the steppe.</ta>
            <ta e="T509" id="Seg_14834" s="T505">It saw a man's coming.</ta>
            <ta e="T514" id="Seg_14835" s="T509">To bring wood, with a horse.</ta>
            <ta e="T521" id="Seg_14836" s="T514">Then it says: "Man, let's live [together]."</ta>
            <ta e="T525" id="Seg_14837" s="T521">He took it home.</ta>
            <ta e="T527" id="Seg_14838" s="T525">They laid down to sleep.</ta>
            <ta e="T534" id="Seg_14839" s="T527">The man fell asleep, and the dog started barking.</ta>
            <ta e="T538" id="Seg_14840" s="T534">And the man stood up and says:</ta>
            <ta e="T549" id="Seg_14841" s="T538">"You are hungry, eat and sleep, and (let?) me sleep.</ta>
            <ta e="T550" id="Seg_14842" s="T549">That's all!</ta>
            <ta e="T555" id="Seg_14843" s="T551">One man went to fetch wood.</ta>
            <ta e="T560" id="Seg_14844" s="T555">He took wood and was going [home?].</ta>
            <ta e="T563" id="Seg_14845" s="T560">A bear came to him.</ta>
            <ta e="T567" id="Seg_14846" s="T563">"I'm going to eat you."</ta>
            <ta e="T571" id="Seg_14847" s="T567">"Don't eat me.</ta>
            <ta e="T576" id="Seg_14848" s="T571">Hunters are coming here, they'll kill you."</ta>
            <ta e="T579" id="Seg_14849" s="T576">It [= the bear] was afraid.</ta>
            <ta e="T582" id="Seg_14850" s="T579">Cover me with logs.</ta>
            <ta e="T584" id="Seg_14851" s="T582">And go away.</ta>
            <ta e="T589" id="Seg_14852" s="T584">So that they won't see me."</ta>
            <ta e="T596" id="Seg_14853" s="T589">And [the man] says: "If [I] cover [you], [I] must bind [you]."</ta>
            <ta e="T601" id="Seg_14854" s="T596">Then it says: "Bind [me]."</ta>
            <ta e="T604" id="Seg_14855" s="T601">He bound it.</ta>
            <ta e="T610" id="Seg_14856" s="T604">"And now [I] must hit [you with] the axe."</ta>
            <ta e="T620" id="Seg_14857" s="T610">Then he hit this bear with the axe, and the bear died.</ta>
            <ta e="T627" id="Seg_14858" s="T621">The bear and the man became related.</ta>
            <ta e="T630" id="Seg_14859" s="T627">They started planting turnip.</ta>
            <ta e="T635" id="Seg_14860" s="T630">"I'll take the roots, and you'll take the tops."</ta>
            <ta e="T637" id="Seg_14861" s="T635">They sowed [it].</ta>
            <ta e="T639" id="Seg_14862" s="T637">It grew up.</ta>
            <ta e="T645" id="Seg_14863" s="T639">The man took the roots, and the bear [took] the tops.</ta>
            <ta e="T651" id="Seg_14864" s="T645">Then the bear [thinks]: "Well, the man tricked me."</ta>
            <ta e="T655" id="Seg_14865" s="T651">Then it became warm again.</ta>
            <ta e="T661" id="Seg_14866" s="T655">The man says to the bear: "Let's sow again."</ta>
            <ta e="T670" id="Seg_14867" s="T661">The bear [says]: "You'll give me the roots, and you [will take] the tops."</ta>
            <ta e="T674" id="Seg_14868" s="T670">Then, wheat…</ta>
            <ta e="T677" id="Seg_14869" s="T674">…they sowed wheat.</ta>
            <ta e="T682" id="Seg_14870" s="T677">It grew up, it was very good.</ta>
            <ta e="T689" id="Seg_14871" s="T682">The man took the tops, and the roots [passed] to the bear.</ta>
            <ta e="T696" id="Seg_14872" s="T689">And then their friendship died.</ta>
            <ta e="T701" id="Seg_14873" s="T697">There lived a man and a woman.</ta>
            <ta e="T705" id="Seg_14874" s="T701">They had no children.</ta>
            <ta e="T711" id="Seg_14875" s="T705">Once there was very much snow.</ta>
            <ta e="T721" id="Seg_14876" s="T711">Some children sled, some play.</ta>
            <ta e="T725" id="Seg_14877" s="T721">They make snowmen, they make everything.</ta>
            <ta e="T729" id="Seg_14878" s="T725">Then they made a big woman.</ta>
            <ta e="T740" id="Seg_14879" s="T729">The man looked [at it] and [then] said to the woman: "Let's go out."</ta>
            <ta e="T742" id="Seg_14880" s="T740">"Well, let's go."</ta>
            <ta e="T749" id="Seg_14881" s="T742">They went, looked, then started making a girl.</ta>
            <ta e="T757" id="Seg_14882" s="T749">They make the head, the eyes, the nose, the mouth.</ta>
            <ta e="T763" id="Seg_14883" s="T757">Then they painted with coal (around) the eyes.</ta>
            <ta e="T767" id="Seg_14884" s="T763">Then she (blinked?).</ta>
            <ta e="T770" id="Seg_14885" s="T767">Then the man went away.</ta>
            <ta e="T781" id="Seg_14886" s="T770">He went into the house, they went into the house with her, too.</ta>
            <ta e="T791" id="Seg_14887" s="T781">She lived very well, they bought all sort of things [for their] child. [?]</ta>
            <ta e="T798" id="Seg_14888" s="T792">Then they bought her boots, [they are] beautiful, red.</ta>
            <ta e="T805" id="Seg_14889" s="T798">They they bought a hairband, very beautiful, made from satin.</ta>
            <ta e="T810" id="Seg_14890" s="T805">Then it became warm.</ta>
            <ta e="T814" id="Seg_14891" s="T810">This girl was sitting.</ta>
            <ta e="T816" id="Seg_14892" s="T814">Water is flowing.</ta>
            <ta e="T819" id="Seg_14893" s="T816">She is afraid.</ta>
            <ta e="T827" id="Seg_14894" s="T819">The woman asks: "Why are you crying?"</ta>
            <ta e="T834" id="Seg_14895" s="T827">But the tears flow on their faces.</ta>
            <ta e="T840" id="Seg_14896" s="T834">Then appeared the sun.</ta>
            <ta e="T844" id="Seg_14897" s="T840">Various grass and flowers are growing.</ta>
            <ta e="T849" id="Seg_14898" s="T844">Then girls came: "Come with us!"</ta>
            <ta e="T851" id="Seg_14899" s="T849">They call the girl.</ta>
            <ta e="T858" id="Seg_14900" s="T851">The woman says: "Go, why are you sitting?"</ta>
            <ta e="T866" id="Seg_14901" s="T858">"No, I won't go, the sun… my head will ache.</ta>
            <ta e="T870" id="Seg_14902" s="T866">The sun will burn my head."</ta>
            <ta e="T874" id="Seg_14903" s="T870">"Well, put on a scarf."</ta>
            <ta e="T877" id="Seg_14904" s="T874">Then she went.</ta>
            <ta e="T880" id="Seg_14905" s="T877">All girls are playing.</ta>
            <ta e="T883" id="Seg_14906" s="T880">They pick flowers.</ta>
            <ta e="T887" id="Seg_14907" s="T883">And she is (sitting?).</ta>
            <ta e="T899" id="Seg_14908" s="T889">Then they made a very big fire and started (jumping?) [over] the fire.</ta>
            <ta e="T904" id="Seg_14909" s="T899">They say: "Why aren't you jumping?"</ta>
            <ta e="T907" id="Seg_14910" s="T904">She is afraid.</ta>
            <ta e="T912" id="Seg_14911" s="T907">Then they started making fun of her.</ta>
            <ta e="T923" id="Seg_14912" s="T912">Then stood up, jumped and went up from the fire.</ta>
            <ta e="T933" id="Seg_14913" s="T925">Once my calf, I had a very beautiful white calf.</ta>
            <ta e="T941" id="Seg_14914" s="T933">Once in the evening I went to give him milk.</ta>
            <ta e="T947" id="Seg_14915" s="T941">I shouted, I searched, it is nowhere.</ta>
            <ta e="T953" id="Seg_14916" s="T947">Where did the calf go, I don't know.</ta>
            <ta e="T957" id="Seg_14917" s="T953">In the morning I got up, I went everywhere.</ta>
            <ta e="T963" id="Seg_14918" s="T957">I went to the river, I thought it [could] fall into the water.</ta>
            <ta e="T966" id="Seg_14919" s="T963">It is nowhere.</ta>
            <ta e="T969" id="Seg_14920" s="T966">I thought: a wolf ate it.</ta>
            <ta e="T975" id="Seg_14921" s="T969">No, it didn't eat [it].</ta>
            <ta e="T980" id="Seg_14922" s="T975">People was mowing grass here.</ta>
            <ta e="T988" id="Seg_14923" s="T980">They must have taken it, bound its legs, carried home.</ta>
            <ta e="T992" id="Seg_14924" s="T988">It was a wolf with two legs.</ta>
            <ta e="T997" id="Seg_14925" s="T992">My relative came to Kan-Okler.</ta>
            <ta e="T1004" id="Seg_14926" s="T997">And says: (someone?) had two cows.</ta>
            <ta e="T1012" id="Seg_14927" s="T1004">Very fat.</ta>
            <ta e="T1016" id="Seg_14928" s="T1012">And [when] it lives in our [village].</ta>
            <ta e="T1019" id="Seg_14929" s="T1016">They become very skinny.</ta>
            <ta e="T1025" id="Seg_14930" s="T1019">It doesn't eat grass, doesn't drink water.</ta>
            <ta e="T1027" id="Seg_14931" s="T1025">It eats little.</ta>
            <ta e="T1037" id="Seg_14932" s="T1027">Other cows come and eat all the grass, and it doesn't eat.</ta>
            <ta e="T1041" id="Seg_14933" s="T1038">"I don't know," - he says.</ta>
            <ta e="T1046" id="Seg_14934" s="T1041">[It's] because of the yard, or what.</ta>
            <ta e="T1052" id="Seg_14935" s="T1046">We took a cow from his sister.</ta>
            <ta e="T1057" id="Seg_14936" s="T1052">Because she doesn't know anything.</ta>
            <ta e="T1064" id="Seg_14937" s="T1057">[This] cow became the same, too, it became scraggy.</ta>
            <ta e="T1070" id="Seg_14938" s="T1064">And [earlier?] it was fat.</ta>
            <ta e="T1075" id="Seg_14939" s="T1070">It lived with us and became scraggy.</ta>
            <ta e="T1084" id="Seg_14940" s="T1076">It lived with a young woman/daughter, after that it (changed?).</ta>
            <ta e="T1090" id="Seg_14941" s="T1084">Maybe has it dug out something?</ta>
            <ta e="T1097" id="Seg_14942" s="T1090">And now her cows do not (stand?).</ta>
            <ta e="T1104" id="Seg_14943" s="T1098">Tomorrow you…</ta>
            <ta e="T1107" id="Seg_14944" s="T1104">Tomorrow we are leaving.</ta>
            <ta e="T1110" id="Seg_14945" s="T1107">You are leaving, let's hug each other.</ta>
            <ta e="T1112" id="Seg_14946" s="T1110">And [let's] kiss each other,</ta>
            <ta e="T1115" id="Seg_14947" s="T1112">Then go and God be with you.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T8" id="Seg_14948" s="T0">Wenn es fliegt, schreit es, wenn es sich setzt, (?) es den Boden.</ta>
            <ta e="T10" id="Seg_14949" s="T8">Das is ein Gewehr.</ta>
            <ta e="T17" id="Seg_14950" s="T11">Was für ein Rätsel, im Arsch ist eine Beere. [?]</ta>
            <ta e="T19" id="Seg_14951" s="T17">Das ist ein Gewehr.</ta>
            <ta e="T33" id="Seg_14952" s="T20">Als wir, die Kamassier, noch zahlreich waren, ging ich ins hohe Gebirge in Belogorye, dort fing ich Fische.</ta>
            <ta e="T36" id="Seg_14953" s="T33">Und ich trocknete Fleisch.</ta>
            <ta e="T41" id="Seg_14954" s="T36">Dann wurden die Berge schwarz. [?]</ta>
            <ta e="T47" id="Seg_14955" s="T41">Ich habe keine Kraft mehr, sie starb [= verschwand].</ta>
            <ta e="T57" id="Seg_14956" s="T47">Uns wo die Zelte standen, fielen sie um und verschwanden.</ta>
            <ta e="T60" id="Seg_14957" s="T57">Nur die Birkenrinde [bleib übrig und] (drehte sich ein?).</ta>
            <ta e="T66" id="Seg_14958" s="T61">Jetzt sehe ich nichts.</ta>
            <ta e="T70" id="Seg_14959" s="T66">Ich höre nichts.</ta>
            <ta e="T74" id="Seg_14960" s="T70">Meine Kraft ging weg.</ta>
            <ta e="T78" id="Seg_14961" s="T74">Jetzt lebe ich alleine.</ta>
            <ta e="T84" id="Seg_14962" s="T78">Es ist niemand [übrig], all meine Kamassier sind tot.</ta>
            <ta e="T90" id="Seg_14963" s="T85">Jetzt sind all meine Straßen verlassen worden.</ta>
            <ta e="T93" id="Seg_14964" s="T90">Sie sind vom Gras überwachsen.</ta>
            <ta e="T98" id="Seg_14965" s="T94">Ich wurde [= war?] sehr schön.</ta>
            <ta e="T103" id="Seg_14966" s="T98">Wo ist diese Schönheit hin?</ta>
            <ta e="T109" id="Seg_14967" s="T103">Sie is zu dem hohen Gebirge gegangen, und ist nirgendswo zu finden.</ta>
            <ta e="T115" id="Seg_14968" s="T110">Jetzt wird sie nie kommen.</ta>
            <ta e="T123" id="Seg_14969" s="T117">Ich stand in einer kleinen (Straße?), (?) kommt.</ta>
            <ta e="T129" id="Seg_14970" s="T123">Ich sehe: ein sehr großer Mann kommt.</ta>
            <ta e="T133" id="Seg_14971" s="T129">Er geht und macht Bilder von Häusern mit einer Kamera.</ta>
            <ta e="T144" id="Seg_14972" s="T133">Ich stand (nahe?) einem Haus, er machte von mir [auch] ein Bild.</ta>
            <ta e="T150" id="Seg_14973" s="T145">Eine Frau hatte einige Fische.</ta>
            <ta e="T156" id="Seg_14974" s="T151">Ihre (Schwiegertochter?) nahm sie ihr.</ta>
            <ta e="T160" id="Seg_14975" s="T156">Dann kam er zu ihr und sagte:</ta>
            <ta e="T165" id="Seg_14976" s="T160">“Ich bringe dir heute Fische.“</ta>
            <ta e="T168" id="Seg_14977" s="T165">Und ich kam.</ta>
            <ta e="T172" id="Seg_14978" s="T168">“Deine (Schwiegertochter?) nahm deine Fische.</ta>
            <ta e="T175" id="Seg_14979" s="T172">Ich nahm [sie] nicht.”</ta>
            <ta e="T181" id="Seg_14980" s="T176">Was wird sie/er mir geben, [nur] ein Paar.</ta>
            <ta e="T184" id="Seg_14981" s="T181">Gut, ein Paar, das reicht.</ta>
            <ta e="T190" id="Seg_14982" s="T184">Sie verkauften sie für Geld. [?]</ta>
            <ta e="T195" id="Seg_14983" s="T190">Und du hast kein Geld gegeben.</ta>
            <ta e="T203" id="Seg_14984" s="T195">Was er dir gibt, wird genug sein.</ta>
            <ta e="T211" id="Seg_14985" s="T204">Dann kam ich zu Anisya, sie sagte:</ta>
            <ta e="T218" id="Seg_14986" s="T211">“(?) bat um Eier, ich sammelte zehn Eier.</ta>
            <ta e="T229" id="Seg_14987" s="T218">Ich gebe ihr ein Ei für zehen Kopecken.</ta>
            <ta e="T242" id="Seg_14988" s="T230">Elya sagt: “Gehen wir zu uns“, und ich sage: „Nein, ich gehe nicht, es ist zu weit.“</ta>
            <ta e="T244" id="Seg_14989" s="T242">“Wieso ist es weit?“</ta>
            <ta e="T245" id="Seg_14990" s="T244">Sitze [drinnen]</ta>
            <ta e="T249" id="Seg_14991" s="T245">In einem eisernen Vogel [= in einem Flugzeug].</ta>
            <ta e="T252" id="Seg_14992" s="T249">Und du kommst bald an.“</ta>
            <ta e="T259" id="Seg_14993" s="T252">“Nein, ich habe Angst, in einem eisernen Vogel zu sitzen.</ta>
            <ta e="T261" id="Seg_14994" s="T259">[Weil] du [= ich] sterben könntest.“</ta>
            <ta e="T269" id="Seg_14995" s="T261">“Nein, ich [= du] stirbst nicht, es ist gut, hier zu sitzen, wie in einem Haus.”</ta>
            <ta e="T274" id="Seg_14996" s="T269">Du kommst bald zu Hause an [= in unserem Haus], du werdest bei uns leben.</ta>
            <ta e="T277" id="Seg_14997" s="T275">Wir gehen alle drei zusammen.</ta>
            <ta e="T282" id="Seg_14998" s="T278">Heute Nacht gehe ich zu einem Mann.</ta>
            <ta e="T288" id="Seg_14999" s="T282">Zu einem Mann.</ta>
            <ta e="T289" id="Seg_15000" s="T288">Ich werde sagen:</ta>
            <ta e="T294" id="Seg_15001" s="T289">“Trag diese Frau ins Krankenhaus.</ta>
            <ta e="T299" id="Seg_15002" s="T294">Ihr wurde ein Paier gegeben. </ta>
            <ta e="T313" id="Seg_15003" s="T299">Und wenn du [sie] nicht nimmst, rufe ich einen Krankenwagen, sie komme und tragen sie.”</ta>
            <ta e="T319" id="Seg_15004" s="T313">Ich sag’s ihm so, vielleicht gehorcht er.</ta>
            <ta e="T328" id="Seg_15005" s="T319">Weil du einen Samovar gekauft hast, und Leute sagen:</ta>
            <ta e="T337" id="Seg_15006" s="T328">“Du kauftest einen Samovar, und er hat nie diese Frau geholfen.“</ta>
            <ta e="T347" id="Seg_15007" s="T338">Dein Vater ist gestorben, und das Haus (?) war deins.</ta>
            <ta e="T352" id="Seg_15008" s="T348">Ein Hund lebte lange Zeit alleine.</ta>
            <ta e="T359" id="Seg_15009" s="T352">“Es ist langweilig, alleine zu Hause zu leben, lass mich gehen, mir einen Freund suchen.“</ta>
            <ta e="T363" id="Seg_15010" s="T359">Er lief und lief, und er fand einen Hasen.</ta>
            <ta e="T366" id="Seg_15011" s="T363">“Lass uns zusammenleben?“</ta>
            <ta e="T368" id="Seg_15012" s="T366">“Lass uns leben [zusammen]?”</ta>
            <ta e="T374" id="Seg_15013" s="T368">Dann kam die Nacht, und sie legten sich zum Schlafen hin.</ta>
            <ta e="T381" id="Seg_15014" s="T374">Der Hase schlief ein, und der Hund schläft nicht.</ta>
            <ta e="T384" id="Seg_15015" s="T381">In der Nacht bellt er.</ta>
            <ta e="T387" id="Seg_15016" s="T384">Und der Hase bekam Angst.</ta>
            <ta e="T390" id="Seg_15017" s="T387">Er bekam große Angst.</ta>
            <ta e="T392" id="Seg_15018" s="T390">“Warum bellst du?</ta>
            <ta e="T397" id="Seg_15019" s="T392">Ein Wolf kommt, uns fressen.“</ta>
            <ta e="T404" id="Seg_15020" s="T397">Der Hund sagt: „Mein Freund ist nicht gut.</ta>
            <ta e="T409" id="Seg_15021" s="T404">Ich gehe und (sehe?) den Wolf.“</ta>
            <ta e="T411" id="Seg_15022" s="T409">Dann ging er.</ta>
            <ta e="T416" id="Seg_15023" s="T411">Er läufte einiger Zeit, [und dann] ein Wolf kommt.</ta>
            <ta e="T419" id="Seg_15024" s="T416">“Lass uns zusammenleben?”</ta>
            <ta e="T421" id="Seg_15025" s="T419">“Lass uns leben [zsammen].“</ta>
            <ta e="T427" id="Seg_15026" s="T421">Der Abend kommt, sie legte sich zum Schlafen hin.</ta>
            <ta e="T430" id="Seg_15027" s="T427">In der Nacht, bellte der Hund.</ta>
            <ta e="T435" id="Seg_15028" s="T430">Der Wolf sagt: “Warum bellst du?</ta>
            <ta e="T441" id="Seg_15029" s="T435">Ein Bär kommt uns fressen.“</ta>
            <ta e="T445" id="Seg_15030" s="T441">Am Morgen, stand der Hund auf:</ta>
            <ta e="T452" id="Seg_15031" s="T445">“Mein Freund ist nicht gut, lass mich gehen, den Bär suchen.“</ta>
            <ta e="T455" id="Seg_15032" s="T452">Dann gin ger.</ta>
            <ta e="T459" id="Seg_15033" s="T455">Er läuft einiger Zeit, [dann sieht er:] ain Bär kommt.</ta>
            <ta e="T464" id="Seg_15034" s="T459">“Bär, lass uns zusammenleben.“</ta>
            <ta e="T465" id="Seg_15035" s="T464">“OK.”</ta>
            <ta e="T471" id="Seg_15036" s="T465">Die Nacht kam, sie legten sich zum Schlafen hin.</ta>
            <ta e="T476" id="Seg_15037" s="T471">In der Nacht, bellte der Hund.</ta>
            <ta e="T480" id="Seg_15038" s="T476">Der Bär [sagt]: “Warum bellst du?</ta>
            <ta e="T485" id="Seg_15039" s="T480">Der Mann wird kommen und uns töten.“</ta>
            <ta e="T494" id="Seg_15040" s="T485">[Der Hund] sagt: “Mein Freund ist nicht gut, lass mich gehen und nach dem Mann suchen.</ta>
            <ta e="T498" id="Seg_15041" s="T494">Morgens stand er auf und ging.</ta>
            <ta e="T502" id="Seg_15042" s="T498">Er lief durch den Wald.</ta>
            <ta e="T505" id="Seg_15043" s="T502">Dann erreichte er die Steppe.</ta>
            <ta e="T509" id="Seg_15044" s="T505">Er sah einen Mann kommen.</ta>
            <ta e="T514" id="Seg_15045" s="T509">Um Holz zu bringen, mit einem Pferd.</ta>
            <ta e="T521" id="Seg_15046" s="T514">Dann sagte er “Mann, lass uns leben [zusammen].“</ta>
            <ta e="T525" id="Seg_15047" s="T521">Er nahm ihn nach Hause.</ta>
            <ta e="T527" id="Seg_15048" s="T525">Sie legten sich zum Schlafen hin.</ta>
            <ta e="T534" id="Seg_15049" s="T527">Der Mann schlief ein, und der Hund fing an, zu bellen.</ta>
            <ta e="T538" id="Seg_15050" s="T534">Und der Mann steht auf und sagt:</ta>
            <ta e="T549" id="Seg_15051" s="T538">“Du hast Hunger, friss und schlaf, and (lass?) mich schlafen.</ta>
            <ta e="T550" id="Seg_15052" s="T549">Das is alles!</ta>
            <ta e="T555" id="Seg_15053" s="T551">Ein Mann ging Holz holen.</ta>
            <ta e="T560" id="Seg_15054" s="T555">Er nahm Holz und ging [nach Hause?].</ta>
            <ta e="T563" id="Seg_15055" s="T560">Ein Bär kommt zu ihm.</ta>
            <ta e="T567" id="Seg_15056" s="T563">“Ich werde dich fressen.“</ta>
            <ta e="T571" id="Seg_15057" s="T567">“Friss mich nicht.</ta>
            <ta e="T576" id="Seg_15058" s="T571">Jäger kommen her, sie werden dich töten.“</ta>
            <ta e="T579" id="Seg_15059" s="T576">Er [= der Bär] hatte Angst.</ta>
            <ta e="T582" id="Seg_15060" s="T579">Bedecke mich mit dem Holz.</ta>
            <ta e="T584" id="Seg_15061" s="T582">Und ging weg.</ta>
            <ta e="T589" id="Seg_15062" s="T584">So, dass sie mich nicht sehen werden.“</ta>
            <ta e="T596" id="Seg_15063" s="T589">Und [der Mann] sagt: “[Um dich] zu bedecken, muss [ich dich] binden.“</ta>
            <ta e="T601" id="Seg_15064" s="T596">Dann sagt er: “Binde [mich].“</ta>
            <ta e="T604" id="Seg_15065" s="T601">Er band ihn.</ta>
            <ta e="T610" id="Seg_15066" s="T604">“Und jetzt muss ich [dich mit] einer Axt hauen.“!</ta>
            <ta e="T620" id="Seg_15067" s="T610">Dann haute er diesen Bär mit der Axt, und der Bär starb.</ta>
            <ta e="T627" id="Seg_15068" s="T621">Der Bär und der Mann wurden verwandt.</ta>
            <ta e="T630" id="Seg_15069" s="T627">Sie fingen an, </ta>
            <ta e="T635" id="Seg_15070" s="T630">“Ich nehme die Wurzeln, und du nimmst das Obere.”</ta>
            <ta e="T637" id="Seg_15071" s="T635">Dann sähten sie [es].</ta>
            <ta e="T639" id="Seg_15072" s="T637">Es wuchs.</ta>
            <ta e="T645" id="Seg_15073" s="T639">Der Mann nahm die Wurzeln, und der Bär [nahm] das Obere.</ta>
            <ta e="T651" id="Seg_15074" s="T645">Dann [denkt sich] der Bär:” Also, der Mann hat mich beschummelt.”</ta>
            <ta e="T655" id="Seg_15075" s="T651">Dann wurde es wieder warm.</ta>
            <ta e="T661" id="Seg_15076" s="T655">Der Mann sagt zum Bären: “Lass uns wieder sähen.”</ta>
            <ta e="T670" id="Seg_15077" s="T661">Der Bär [sagt]: “Du gibst mir die Wurzeln, und du [nimmst] das Obere.“</ta>
            <ta e="T674" id="Seg_15078" s="T670">Dann, Weizen…</ta>
            <ta e="T677" id="Seg_15079" s="T674">…sie sähten Weizen.</ta>
            <ta e="T682" id="Seg_15080" s="T677">Er wuchs, er war sehr gut.</ta>
            <ta e="T689" id="Seg_15081" s="T682">Der Mann nahm das Obere, und die Wurzeln [gingen] an den Bär.</ta>
            <ta e="T696" id="Seg_15082" s="T689">Und dann starb ihre Freundschaft.</ta>
            <ta e="T701" id="Seg_15083" s="T697">Es lebte ein Mann und eine Frau</ta>
            <ta e="T705" id="Seg_15084" s="T701">Sie hatten keine Kinder.</ta>
            <ta e="T711" id="Seg_15085" s="T705">Es war einmal sehr viel Schnee.</ta>
            <ta e="T721" id="Seg_15086" s="T711">Einige Kinder rodeln, einige spielen.</ta>
            <ta e="T725" id="Seg_15087" s="T721">Sie Machen Schneemänner, sie Machen alles.</ta>
            <ta e="T729" id="Seg_15088" s="T725">Dann machten sie eine große Frau.</ta>
            <ta e="T740" id="Seg_15089" s="T729">Der Mann schaute [sich sie an] und sagte [dann] zu der Frau: “Gehen wir hinaus.”</ta>
            <ta e="T742" id="Seg_15090" s="T740">“Also, gehen wir.”</ta>
            <ta e="T749" id="Seg_15091" s="T742">Sie gingen, schauten, dann fingen an, ein Mädchen zu machen.</ta>
            <ta e="T757" id="Seg_15092" s="T749">Sie machten den Kopf, die Augen, die Nase, den Mund.</ta>
            <ta e="T763" id="Seg_15093" s="T757">Dann malten sie mit Kohle um die Augen.</ta>
            <ta e="T767" id="Seg_15094" s="T763">Dann (?) sie die Augen.</ta>
            <ta e="T770" id="Seg_15095" s="T767">Dann ging der Mann weg.</ta>
            <ta e="T781" id="Seg_15096" s="T770">Sie ging ins Haus, sie gingen auch mit ihr ins Haus.</ta>
            <ta e="T791" id="Seg_15097" s="T781">Sie lebte sehr gut, sie kauften allerlei Sachen [für ihr] Kind (?)</ta>
            <ta e="T798" id="Seg_15098" s="T792">Dann kauften sie ihr Stiefel, sie sind schön, rot.</ta>
            <ta e="T805" id="Seg_15099" s="T798">Sie kauften ein Haarband, sehr schön, aus Seide gemacht.</ta>
            <ta e="T810" id="Seg_15100" s="T805">Dann wurde es warm.</ta>
            <ta e="T814" id="Seg_15101" s="T810">Dieses Mädchen saß.</ta>
            <ta e="T816" id="Seg_15102" s="T814">Wasser fließt.</ta>
            <ta e="T819" id="Seg_15103" s="T816">Sie hatte solche Angst.</ta>
            <ta e="T827" id="Seg_15104" s="T819">Die Frau fragt: “Warum weinst du?”</ta>
            <ta e="T834" id="Seg_15105" s="T827">Aber die Tränen fließen in ihren Gesichtern.</ta>
            <ta e="T840" id="Seg_15106" s="T834">Dann erschien die Sonne.</ta>
            <ta e="T844" id="Seg_15107" s="T840">Verschiedenes Gras und Blumen wachsen.</ta>
            <ta e="T849" id="Seg_15108" s="T844">Dann kamen Mädchen: “Gehen wir zusammen!”</ta>
            <ta e="T851" id="Seg_15109" s="T849">Sie rufen das Mädchen.</ta>
            <ta e="T858" id="Seg_15110" s="T851">Die Frau sagt: “Geh, warum sitzt du?”</ta>
            <ta e="T866" id="Seg_15111" s="T858">“Nein, ich gehe nicht, die Sonne… ich bekomme Kopfweh. </ta>
            <ta e="T870" id="Seg_15112" s="T866">Die Sonne wird mein Kopf versengen.”</ta>
            <ta e="T874" id="Seg_15113" s="T870">“Gut, nimm einen Schal.“</ta>
            <ta e="T877" id="Seg_15114" s="T874">Dann ging sie.</ta>
            <ta e="T880" id="Seg_15115" s="T877">Alle Mädchen spielen.</ta>
            <ta e="T883" id="Seg_15116" s="T880">Sie pflücken Blumen.</ta>
            <ta e="T887" id="Seg_15117" s="T883">Und sie (sitzt?).</ta>
            <ta e="T899" id="Seg_15118" s="T889">Dann machten sie ein sehr großes Feuer, und fingen an [über] das Feuer zu springen.</ta>
            <ta e="T904" id="Seg_15119" s="T899">Sie sagen: “Warum springst du nicht?”</ta>
            <ta e="T907" id="Seg_15120" s="T904">Sie hat Angst.</ta>
            <ta e="T912" id="Seg_15121" s="T907">Dann fingen sie an, sie zu (?).</ta>
            <ta e="T923" id="Seg_15122" s="T912">Dann stand auf, sprang und flog vom Feuer hinauf.</ta>
            <ta e="T933" id="Seg_15123" s="T925">Einmal mein Kalb, ich hatte ein wunderschönes weißes Kalb.</ta>
            <ta e="T941" id="Seg_15124" s="T933">Einmal am Abend ging ich ihm Milch geben.</ta>
            <ta e="T947" id="Seg_15125" s="T941">Ich rufte, ich suchte, es ist nirgendswo.</ta>
            <ta e="T953" id="Seg_15126" s="T947">Wohin ging das Kalb, ich weiß es nicht.</ta>
            <ta e="T957" id="Seg_15127" s="T953">Am Morgen stand ich auf, ich ging überall.</ta>
            <ta e="T963" id="Seg_15128" s="T957">Ich ging zum Fluss, ich dachte, es [könnte] ins Wasser springen [= fallen].</ta>
            <ta e="T966" id="Seg_15129" s="T963">Es ist nirgendswo.</ta>
            <ta e="T969" id="Seg_15130" s="T966">ich dachte: ein Wolf fraß es.</ta>
            <ta e="T975" id="Seg_15131" s="T969">Nein, er fraß [es] nicht.</ta>
            <ta e="T980" id="Seg_15132" s="T975">Dann mähten Leute Grass hier.</ta>
            <ta e="T988" id="Seg_15133" s="T980">Sie müssen es genommen haben, seine Beine gebunden, es nach Hause getragen.</ta>
            <ta e="T992" id="Seg_15134" s="T988">Es war ein Wolf auf zwei Beine // Ein Wolf nahm [es] auf zwei Beinen [die gebunden waren].</ta>
            <ta e="T997" id="Seg_15135" s="T992">Mein Verwandte kam um zu (?).</ta>
            <ta e="T1004" id="Seg_15136" s="T997">Und sagt: (jemand?) hat zwei Kühe.</ta>
            <ta e="T1012" id="Seg_15137" s="T1004">Sehr stark.</ta>
            <ta e="T1016" id="Seg_15138" s="T1012">Und wenn es [= sie?] eineiger Zeit bei uns lebt.</ta>
            <ta e="T1019" id="Seg_15139" s="T1016">Sie warden sehr dürr.</ta>
            <ta e="T1025" id="Seg_15140" s="T1019">Es frißt kein Gras, drinkt nicht Wasser</ta>
            <ta e="T1027" id="Seg_15141" s="T1025">Es frißt wenig.</ta>
            <ta e="T1037" id="Seg_15142" s="T1027">Andere Kühe kommen und fressen das ganze Gras, und es frißt nicht.</ta>
            <ta e="T1041" id="Seg_15143" s="T1038">“Ich weiß nicht,” – sagt er.</ta>
            <ta e="T1046" id="Seg_15144" s="T1041">[es ist] wegen der Wiese, oder was.</ta>
            <ta e="T1052" id="Seg_15145" s="T1046">Wir nahmen eine Kuh von seiner Schwester.</ta>
            <ta e="T1057" id="Seg_15146" s="T1052">Weil sie nicht weiß.</ta>
            <ta e="T1064" id="Seg_15147" s="T1057">[Diese] Kuh wurde auch genauso, sie wurde klapperdürr.</ta>
            <ta e="T1070" id="Seg_15148" s="T1064">Und [früher?] war sie kräftig.</ta>
            <ta e="T1075" id="Seg_15149" s="T1070">Sie wohnete bei uns und wurde klapperdürr.</ta>
            <ta e="T1084" id="Seg_15150" s="T1076">Sie wohnte bei einer jungen Frau, danach verwandelte sie sich.</ta>
            <ta e="T1090" id="Seg_15151" s="T1084">Vielleicht hat sie ih etwas (gegeben?). </ta>
            <ta e="T1097" id="Seg_15152" s="T1090">Und jetzt stehen (?) ihre Kühe nicht.</ta>
            <ta e="T1104" id="Seg_15153" s="T1098">Morgen du…</ta>
            <ta e="T1107" id="Seg_15154" s="T1104">Morgen gehen wir.</ta>
            <ta e="T1110" id="Seg_15155" s="T1107">Du gehst, lass uns umarmen.</ta>
            <ta e="T1112" id="Seg_15156" s="T1110">Und [lass uns] einander küssen.</ta>
            <ta e="T1115" id="Seg_15157" s="T1112">Dann geht und Gott sei mit euch.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T41" id="Seg_15158" s="T36">[GVY:] sakərʔi = sagərʔi? Sagər măjaʔi - Sayan mountains.</ta>
            <ta e="T57" id="Seg_15159" s="T47">[GVY:] nugabiʔi - a contamination of the PRS and PST forms.</ta>
            <ta e="T123" id="Seg_15160" s="T117">[GVY:] aga from the Turkic?</ta>
            <ta e="T150" id="Seg_15161" s="T145">[GVY:] This episode is not fully clear.</ta>
            <ta e="T172" id="Seg_15162" s="T168">[GVY:] mejăt?</ta>
            <ta e="T299" id="Seg_15163" s="T294">[GVY:] an appointment card for hospitalisation?</ta>
            <ta e="T313" id="Seg_15164" s="T299">[GVY:] kondlal</ta>
            <ta e="T328" id="Seg_15165" s="T319">[GVY:] the samovar episode is not fully clear.</ta>
            <ta e="T337" id="Seg_15166" s="T328">[GVY:] kabažarbia = kabažarbial?</ta>
            <ta e="T347" id="Seg_15167" s="T338">[GVY:] or "was yours, wasn't it'. </ta>
            <ta e="T384" id="Seg_15168" s="T381">[GVY:] is growling.</ta>
            <ta e="T409" id="Seg_15169" s="T404">[GVY:] is- - she was going to say искать.</ta>
            <ta e="T485" id="Seg_15170" s="T480">[GVY:] [kotluj] with an "o"</ta>
            <ta e="T498" id="Seg_15171" s="T494">[GVY:] kambiviʔbi.</ta>
            <ta e="T576" id="Seg_15172" s="T571">[GVY:] Or "here come hunters [sometime]"?</ta>
            <ta e="T729" id="Seg_15173" s="T725">[AAV] From here on, there is persistent background noise (voices), seems to be a recording of audio lessons of German.</ta>
            <ta e="T740" id="Seg_15174" s="T729">[GVY:] mándobi with a long o</ta>
            <ta e="T757" id="Seg_15175" s="T749">[GVY:] the eyes were mentioned twice.</ta>
            <ta e="T899" id="Seg_15176" s="T889">[GVY] numəsittə, not -z-. Maybe PKZ meant suʔmə- 'jump', cf. the next sentence.</ta>
            <ta e="T933" id="Seg_15177" s="T925">[AAV] Plotnikova's voice is very quiet, and other voices (speaking German) become louder.</ta>
            <ta e="T988" id="Seg_15178" s="T980">[GVY:] šarbiʔi</ta>
            <ta e="T992" id="Seg_15179" s="T988">[GVY:] the person who took it was like a wolf.</ta>
            <ta e="T997" id="Seg_15180" s="T992">[AAV] Both Plotnikova's and background voices become louder again.</ta>
            <ta e="T1012" id="Seg_15181" s="T1004">[GVY:] seldə?</ta>
            <ta e="T1084" id="Seg_15182" s="T1076">[GVY:] baska = baška?</ta>
            <ta e="T1097" id="Seg_15183" s="T1090">[GVY:] Unclear.</ta>
            <ta e="T1107" id="Seg_15184" s="T1104">[GVY:] kalladʼürləbeʔ = kalladʼürləleʔ [-2PL]?</ta>
            <ta e="T1110" id="Seg_15185" s="T1107">[GVY:] davajk'e</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T103" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
