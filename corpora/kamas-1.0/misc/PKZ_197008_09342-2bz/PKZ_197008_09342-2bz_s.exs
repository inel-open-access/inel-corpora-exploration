<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC30F10B1-8D1D-D455-86AD-30E5565958C6">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_197008_09342_2bz</transcription-name>
         <referenced-file url="PKZ_197008_09342-2bz.wav" />
         <referenced-file url="PKZ_197008_09342-2bz.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_197008_09342-2bz\PKZ_197008_09342-2bz.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1994</ud-information>
            <ud-information attribute-name="# HIAT:w">1282</ud-information>
            <ud-information attribute-name="# e">1276</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">18</ud-information>
            <ud-information attribute-name="# HIAT:u">242</ud-information>
            <ud-information attribute-name="# sc">452</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="1.74" type="appl" />
         <tli id="T2" time="2.085" type="appl" />
         <tli id="T3" time="2.4299999999999997" type="appl" />
         <tli id="T4" time="2.775" type="appl" />
         <tli id="T5" time="3.12" type="appl" />
         <tli id="T6" time="3.465" type="appl" />
         <tli id="T7" time="3.8099999999999996" type="appl" />
         <tli id="T8" time="4.155" type="appl" />
         <tli id="T9" time="5.059967770103958" />
         <tli id="T10" time="10.26" type="appl" />
         <tli id="T11" time="10.52" type="appl" />
         <tli id="T12" time="10.78" type="appl" />
         <tli id="T13" time="11.206595285302706" />
         <tli id="T14" time="11.31" type="appl" />
         <tli id="T15" time="11.745000000000001" type="appl" />
         <tli id="T16" time="12.18" type="appl" />
         <tli id="T17" time="12.33" type="appl" />
         <tli id="T18" time="12.75" type="appl" />
         <tli id="T19" time="12.961428571428572" type="appl" />
         <tli id="T20" time="13.018" type="appl" />
         <tli id="T21" time="13.286" type="appl" />
         <tli id="T22" time="13.554" type="appl" />
         <tli id="T23" time="13.592857142857143" type="appl" />
         <tli id="T24" time="13.822" type="appl" />
         <tli id="T25" time="14.09" type="appl" />
         <tli id="T26" time="14.224285714285715" type="appl" />
         <tli id="T27" time="14.855714285714285" type="appl" />
         <tli id="T28" time="15.487142857142857" type="appl" />
         <tli id="T29" time="16.11857142857143" type="appl" />
         <tli id="T30" time="16.75" type="appl" />
         <tli id="T31" time="16.93" type="appl" />
         <tli id="T32" time="17.65988751384109" />
         <tli id="T33" time="17.87" type="appl" />
         <tli id="T34" time="18.26" type="appl" />
         <tli id="T35" time="18.65" type="appl" />
         <tli id="T36" time="19.04" type="appl" />
         <tli id="T37" time="19.43" type="appl" />
         <tli id="T38" time="19.82" type="appl" />
         <tli id="T39" time="20.21" type="appl" />
         <tli id="T40" time="20.599999999999998" type="appl" />
         <tli id="T41" time="20.99" type="appl" />
         <tli id="T42" time="21.37" type="appl" />
         <tli id="T43" time="23.23" type="appl" />
         <tli id="T44" time="26.1" type="appl" />
         <tli id="T45" time="26.580000000000002" type="appl" />
         <tli id="T46" time="27.060000000000002" type="appl" />
         <tli id="T47" time="27.54" type="appl" />
         <tli id="T48" time="27.55" type="appl" />
         <tli id="T49" time="28.02" type="appl" />
         <tli id="T50" time="28.2" type="appl" />
         <tli id="T51" time="28.5" type="appl" />
         <tli id="T52" time="29.22" type="appl" />
         <tli id="T53" time="29.74" type="appl" />
         <tli id="T54" time="30.259999999999998" type="appl" />
         <tli id="T55" time="30.78" type="appl" />
         <tli id="T56" time="30.95" type="appl" />
         <tli id="T57" time="31.3" type="appl" />
         <tli id="T58" time="31.75" type="appl" />
         <tli id="T59" time="31.82" type="appl" />
         <tli id="T60" time="32.1" type="appl" />
         <tli id="T61" time="32.99" type="appl" />
         <tli id="T62" time="33.1" type="appl" />
         <tli id="T63" time="33.55461538461539" type="appl" />
         <tli id="T64" time="34.00923076923077" type="appl" />
         <tli id="T65" time="34.463846153846156" type="appl" />
         <tli id="T66" time="34.918461538461536" type="appl" />
         <tli id="T67" time="35.37307692307692" type="appl" />
         <tli id="T68" time="35.82769230769231" type="appl" />
         <tli id="T69" time="36.28230769230769" type="appl" />
         <tli id="T70" time="36.73692307692308" type="appl" />
         <tli id="T71" time="37.19153846153846" type="appl" />
         <tli id="T72" time="37.646153846153844" type="appl" />
         <tli id="T73" time="38.10076923076923" type="appl" />
         <tli id="T74" time="38.55538461538461" type="appl" />
         <tli id="T75" time="39.01" type="appl" />
         <tli id="T76" time="39.44" type="appl" />
         <tli id="T77" time="39.87" type="appl" />
         <tli id="T78" time="40.3" type="appl" />
         <tli id="T79" time="40.73" type="appl" />
         <tli id="T80" time="41.16" type="appl" />
         <tli id="T81" time="41.26" type="appl" />
         <tli id="T82" time="41.86333333333333" type="appl" />
         <tli id="T83" time="42.46666666666667" type="appl" />
         <tli id="T84" time="43.07" type="appl" />
         <tli id="T85" time="44.38" type="appl" />
         <tli id="T86" time="44.983333333333334" type="appl" />
         <tli id="T87" time="45.586666666666666" type="appl" />
         <tli id="T88" time="46.19" type="appl" />
         <tli id="T89" time="46.23" type="appl" />
         <tli id="T90" time="46.634" type="appl" />
         <tli id="T91" time="47.038" type="appl" />
         <tli id="T92" time="47.442" type="appl" />
         <tli id="T93" time="47.846" type="appl" />
         <tli id="T94" time="48.76635604520481" />
         <tli id="T95" time="52.08" type="appl" />
         <tli id="T96" time="53.2" type="appl" />
         <tli id="T97" time="53.71" type="appl" />
         <tli id="T98" time="54.16666666666667" type="appl" />
         <tli id="T99" time="54.623333333333335" type="appl" />
         <tli id="T100" time="55.08" type="appl" />
         <tli id="T101" time="55.53666666666667" type="appl" />
         <tli id="T102" time="55.99333333333334" type="appl" />
         <tli id="T103" time="56.45" type="appl" />
         <tli id="T104" time="57.0" type="appl" />
         <tli id="T105" time="57.54666666666667" type="appl" />
         <tli id="T106" time="58.093333333333334" type="appl" />
         <tli id="T107" time="58.64" type="appl" />
         <tli id="T108" time="58.86629171280362" />
         <tli id="T109" time="59.03962394208254" />
         <tli id="T110" time="59.51295426049807" />
         <tli id="T111" time="60.306999999999995" type="appl" />
         <tli id="T112" time="60.67961349602928" />
         <tli id="T113" time="61.13961056603873" />
         <tli id="T114" time="61.924" type="appl" />
         <tli id="T115" time="62.463" type="appl" />
         <tli id="T116" time="63.001999999999995" type="appl" />
         <tli id="T117" time="63.541" type="appl" />
         <tli id="T118" time="64.15292470581079" />
         <tli id="T119" time="64.59" type="appl" />
         <tli id="T120" time="65.20666799390308" />
         <tli id="T121" time="65.21333461810613" />
         <tli id="T122" time="66.06333571436835" />
         <tli id="T123" time="66.1" type="appl" />
         <tli id="T124" time="66.63499999999999" type="appl" />
         <tli id="T125" time="67.17" type="appl" />
         <tli id="T126" time="67.29" type="appl" />
         <tli id="T127" time="68.4" type="appl" />
         <tli id="T128" time="70.75" type="appl" />
         <tli id="T129" time="71.94333333333333" type="appl" />
         <tli id="T130" time="73.13666666666667" type="appl" />
         <tli id="T131" time="74.33" type="appl" />
         <tli id="T132" time="75.52333333333333" type="appl" />
         <tli id="T133" time="76.71666666666667" type="appl" />
         <tli id="T134" time="77.91" type="appl" />
         <tli id="T135" time="79.10333333333332" type="appl" />
         <tli id="T136" time="80.29666666666667" type="appl" />
         <tli id="T137" time="81.49" type="appl" />
         <tli id="T138" time="81.76" type="appl" />
         <tli id="T139" time="82.56" type="appl" />
         <tli id="T140" time="83.36" type="appl" />
         <tli id="T141" time="84.16" type="appl" />
         <tli id="T142" time="84.24" type="appl" />
         <tli id="T143" time="85.09599999999999" type="appl" />
         <tli id="T144" time="85.952" type="appl" />
         <tli id="T145" time="86.80799999999999" type="appl" />
         <tli id="T146" time="87.664" type="appl" />
         <tli id="T147" time="88.52" type="appl" />
         <tli id="T148" time="88.6" type="appl" />
         <tli id="T149" time="89.4" type="appl" />
         <tli id="T150" time="90.33942457533429" />
         <tli id="T151" time="93.01" type="appl" />
         <tli id="T152" time="94.285" type="appl" />
         <tli id="T153" time="95.60605769573236" />
         <tli id="T154" time="96.02" type="appl" />
         <tli id="T155" time="96.9" type="appl" />
         <tli id="T156" time="97.78" type="appl" />
         <tli id="T157" time="101.2" type="appl" />
         <tli id="T158" time="102.08500000000001" type="appl" />
         <tli id="T159" time="103.16600954197463" />
         <tli id="T160" time="103.71" type="appl" />
         <tli id="T161" time="104.99499999999999" type="appl" />
         <tli id="T162" time="106.28" type="appl" />
         <tli id="T163" time="107.565" type="appl" />
         <tli id="T164" time="108.85" type="appl" />
         <tli id="T165" time="110.26" type="appl" />
         <tli id="T166" time="111.83928763012386" />
         <tli id="T167" time="111.91" type="appl" />
         <tli id="T168" time="112.31" type="appl" />
         <tli id="T169" time="112.71" type="appl" />
         <tli id="T170" time="113.11" type="appl" />
         <tli id="T171" time="113.50999999999999" type="appl" />
         <tli id="T172" time="113.91" type="appl" />
         <tli id="T173" time="114.14" type="appl" />
         <tli id="T174" time="114.94916666666667" type="appl" />
         <tli id="T175" time="115.75833333333333" type="appl" />
         <tli id="T176" time="116.5675" type="appl" />
         <tli id="T177" time="117.37666666666667" type="appl" />
         <tli id="T178" time="118.18583333333333" type="appl" />
         <tli id="T179" time="118.995" type="appl" />
         <tli id="T180" time="119.80416666666666" type="appl" />
         <tli id="T181" time="120.61333333333333" type="appl" />
         <tli id="T182" time="121.4225" type="appl" />
         <tli id="T183" time="122.23166666666665" type="appl" />
         <tli id="T184" time="123.04083333333332" type="appl" />
         <tli id="T185" time="123.85" type="appl" />
         <tli id="T186" time="124.06" type="appl" />
         <tli id="T187" time="124.94771428571428" type="appl" />
         <tli id="T188" time="125.83542857142858" type="appl" />
         <tli id="T189" time="126.72314285714286" type="appl" />
         <tli id="T190" time="127.61085714285714" type="appl" />
         <tli id="T191" time="128.49857142857144" type="appl" />
         <tli id="T192" time="129.38628571428572" type="appl" />
         <tli id="T193" time="130.274" type="appl" />
         <tli id="T194" time="130.62" type="appl" />
         <tli id="T195" time="131.4375" type="appl" />
         <tli id="T196" time="132.255" type="appl" />
         <tli id="T197" time="133.0725" type="appl" />
         <tli id="T198" time="133.89" type="appl" />
         <tli id="T199" time="137.89" type="appl" />
         <tli id="T200" time="138.2911111111111" type="appl" />
         <tli id="T201" time="138.6922222222222" type="appl" />
         <tli id="T202" time="139.09333333333333" type="appl" />
         <tli id="T203" time="139.49444444444444" type="appl" />
         <tli id="T204" time="139.89555555555555" type="appl" />
         <tli id="T205" time="140.29666666666665" type="appl" />
         <tli id="T206" time="140.6977777777778" type="appl" />
         <tli id="T207" time="141.01910176681028" />
         <tli id="T208" time="141.0988888888889" type="appl" />
         <tli id="T209" time="141.289" type="appl" />
         <tli id="T210" time="141.5" type="appl" />
         <tli id="T211" time="141.81799999999998" type="appl" />
         <tli id="T212" time="142.347" type="appl" />
         <tli id="T213" time="142.876" type="appl" />
         <tli id="T214" time="143.31" type="appl" />
         <tli id="T215" time="143.78" type="appl" />
         <tli id="T216" time="144.25" type="appl" />
         <tli id="T217" time="144.72" type="appl" />
         <tli id="T218" time="145.19" type="appl" />
         <tli id="T219" time="145.36" type="appl" />
         <tli id="T220" time="145.98000000000002" type="appl" />
         <tli id="T221" time="146.6" type="appl" />
         <tli id="T222" time="147.17906253041514" />
         <tli id="T223" time="147.5923932310033" />
         <tli id="T224" time="147.7825" type="intp" />
         <tli id="T225" time="148.13" type="appl" />
         <tli id="T226" time="148.24" type="appl" />
         <tli id="T227" time="148.82500000000002" type="appl" />
         <tli id="T228" time="149.11599999999999" type="appl" />
         <tli id="T229" time="149.52" type="appl" />
         <tli id="T230" time="149.992" type="appl" />
         <tli id="T231" time="150.0" type="appl" />
         <tli id="T232" time="151.06" type="appl" />
         <tli id="T233" time="151.12" type="appl" />
         <tli id="T234" time="151.82" type="appl" />
         <tli id="T235" time="152.20569717950391" />
         <tli id="T236" time="152.41750000000002" type="appl" />
         <tli id="T237" time="152.58569475907692" />
         <tli id="T238" time="152.78500000000003" type="appl" />
         <tli id="T239" time="153.07" type="appl" />
         <tli id="T240" time="153.1525" type="appl" />
         <tli id="T241" time="153.52" type="appl" />
         <tli id="T242" time="153.66" type="appl" />
         <tli id="T243" time="153.88750000000002" type="appl" />
         <tli id="T244" time="154.25" type="appl" />
         <tli id="T245" time="154.255" type="appl" />
         <tli id="T246" time="154.6225" type="appl" />
         <tli id="T247" time="154.84" type="appl" />
         <tli id="T248" time="154.99" type="appl" />
         <tli id="T249" time="155.43" type="appl" />
         <tli id="T250" time="155.74" type="appl" />
         <tli id="T251" time="156.56433333333334" type="appl" />
         <tli id="T252" time="157.38866666666667" type="appl" />
         <tli id="T253" time="158.26633565908384" />
         <tli id="T254" time="158.27" type="appl" />
         <tli id="T255" time="158.645" type="appl" />
         <tli id="T256" time="159.02" type="appl" />
         <tli id="T257" time="159.17231947167602" />
         <tli id="T258" time="159.4015" type="appl" />
         <tli id="T259" time="159.8963382974764" />
         <tli id="T260" time="159.97" type="appl" />
         <tli id="T261" time="160.16" type="appl" />
         <tli id="T262" time="160.47" type="appl" />
         <tli id="T263" time="160.845" type="appl" />
         <tli id="T264" time="161.27" type="appl" />
         <tli id="T265" time="161.53" type="appl" />
         <tli id="T266" time="161.71" type="appl" />
         <tli id="T267" time="162.20333333333335" type="appl" />
         <tli id="T268" time="162.215" type="appl" />
         <tli id="T269" time="162.69666666666666" type="appl" />
         <tli id="T270" time="162.9" type="appl" />
         <tli id="T271" time="163.09229450306088" />
         <tli id="T272" time="163.45895883422787" />
         <tli id="T273" time="163.52499999999998" type="appl" />
         <tli id="T274" time="164.1" type="appl" />
         <tli id="T275" time="164.675" type="appl" />
         <tli id="T276" time="165.23894749643833" />
         <tli id="T277" time="165.64561157282353" />
         <tli id="T278" time="166.3256072415331" />
         <tli id="T279" time="166.82" type="appl" />
         <tli id="T280" time="167.07" type="appl" />
         <tli id="T281" time="167.25025" type="appl" />
         <tli id="T282" time="167.6805" type="appl" />
         <tli id="T283" time="168.11075" type="appl" />
         <tli id="T284" time="168.47" type="appl" />
         <tli id="T285" time="168.541" type="appl" />
         <tli id="T286" time="169.07" type="appl" />
         <tli id="T287" time="169.547" type="appl" />
         <tli id="T288" time="170.024" type="appl" />
         <tli id="T289" time="170.501" type="appl" />
         <tli id="T290" time="170.978" type="appl" />
         <tli id="T291" time="171.45499999999998" type="appl" />
         <tli id="T292" time="171.932" type="appl" />
         <tli id="T293" time="172.25" type="appl" />
         <tli id="T294" time="172.409" type="appl" />
         <tli id="T295" time="172.60666666666665" type="appl" />
         <tli id="T296" time="172.886" type="appl" />
         <tli id="T297" time="172.96333333333334" type="appl" />
         <tli id="T298" time="173.32" type="appl" />
         <tli id="T299" time="173.363" type="appl" />
         <tli id="T300" time="173.43" type="appl" />
         <tli id="T301" time="173.84" type="appl" />
         <tli id="T302" time="173.88809523809525" type="appl" />
         <tli id="T303" time="174.3461904761905" type="appl" />
         <tli id="T304" time="174.80428571428573" type="appl" />
         <tli id="T305" time="175.26238095238097" type="appl" />
         <tli id="T306" time="175.7204761904762" type="appl" />
         <tli id="T307" time="176.14" type="appl" />
         <tli id="T308" time="176.17857142857144" type="appl" />
         <tli id="T309" time="176.63666666666668" type="appl" />
         <tli id="T310" time="176.675" type="appl" />
         <tli id="T311" time="177.09476190476192" type="appl" />
         <tli id="T312" time="177.21" type="appl" />
         <tli id="T313" time="177.35" type="appl" />
         <tli id="T314" time="177.55285714285716" type="appl" />
         <tli id="T315" time="177.789375" type="appl" />
         <tli id="T316" time="178.0109523809524" type="appl" />
         <tli id="T317" time="178.22875" type="appl" />
         <tli id="T318" time="178.46904761904761" type="appl" />
         <tli id="T319" time="178.668125" type="appl" />
         <tli id="T320" time="178.92714285714285" type="appl" />
         <tli id="T321" time="179.1075" type="appl" />
         <tli id="T322" time="179.3852380952381" type="appl" />
         <tli id="T323" time="179.546875" type="appl" />
         <tli id="T324" time="179.84333333333333" type="appl" />
         <tli id="T325" time="179.98624999999998" type="appl" />
         <tli id="T326" time="180.30142857142857" type="appl" />
         <tli id="T327" time="180.425625" type="appl" />
         <tli id="T328" time="180.7595238095238" type="appl" />
         <tli id="T329" time="180.865" type="appl" />
         <tli id="T330" time="181.21761904761905" type="appl" />
         <tli id="T331" time="181.304375" type="appl" />
         <tli id="T332" time="181.6757142857143" type="appl" />
         <tli id="T333" time="181.74375" type="appl" />
         <tli id="T334" time="182.13380952380953" type="appl" />
         <tli id="T335" time="182.183125" type="appl" />
         <tli id="T336" time="182.59190476190477" type="appl" />
         <tli id="T337" time="182.6225" type="appl" />
         <tli id="T338" time="183.05" type="appl" />
         <tli id="T339" time="183.061875" type="appl" />
         <tli id="T340" time="183.25" type="appl" />
         <tli id="T341" time="183.50125" type="appl" />
         <tli id="T342" time="183.94062499999998" type="appl" />
         <tli id="T343" time="184.23633333333333" type="appl" />
         <tli id="T344" time="184.38" type="appl" />
         <tli id="T345" time="185.22266666666667" type="appl" />
         <tli id="T346" time="186.209" type="appl" />
         <tli id="T347" time="187.06" type="appl" />
         <tli id="T348" time="188.26" type="appl" />
         <tli id="T349" time="189.0" type="appl" />
         <tli id="T350" time="189.5375" type="appl" />
         <tli id="T351" time="190.075" type="appl" />
         <tli id="T352" time="190.6125" type="appl" />
         <tli id="T353" time="191.15" type="appl" />
         <tli id="T354" time="191.17" type="appl" />
         <tli id="T355" time="192.01" type="appl" />
         <tli id="T356" time="194.3587620152975" />
         <tli id="T357" time="195.6" type="appl" />
         <tli id="T358" time="196.17875042272618" />
         <tli id="T359" time="197.53874176014543" />
         <tli id="T360" time="197.7405" type="appl" />
         <tli id="T361" time="197.98" type="appl" />
         <tli id="T362" time="198.381" type="appl" />
         <tli id="T363" time="199.0215" type="appl" />
         <tli id="T364" time="199.662" type="appl" />
         <tli id="T365" time="202.18" type="appl" />
         <tli id="T366" time="202.61" type="appl" />
         <tli id="T367" time="203.04" type="appl" />
         <tli id="T368" time="203.47" type="appl" />
         <tli id="T369" time="203.9" type="appl" />
         <tli id="T370" time="204.33" type="appl" />
         <tli id="T371" time="204.76" type="appl" />
         <tli id="T372" time="205.19" type="appl" />
         <tli id="T373" time="205.62" type="appl" />
         <tli id="T374" time="206.05" type="appl" />
         <tli id="T375" time="206.48" type="appl" />
         <tli id="T376" time="206.91" type="appl" />
         <tli id="T377" time="207.34" type="appl" />
         <tli id="T378" time="207.76999999999998" type="appl" />
         <tli id="T379" time="208.2" type="appl" />
         <tli id="T380" time="208.63" type="appl" />
         <tli id="T381" time="209.06" type="appl" />
         <tli id="T382" time="209.48999999999998" type="appl" />
         <tli id="T383" time="209.92" type="appl" />
         <tli id="T384" time="210.35" type="appl" />
         <tli id="T385" time="210.77999999999997" type="appl" />
         <tli id="T386" time="211.20999999999998" type="appl" />
         <tli id="T387" time="211.64" type="appl" />
         <tli id="T388" time="212.19" type="appl" />
         <tli id="T389" time="212.29" type="appl" />
         <tli id="T390" time="212.87911111111111" type="appl" />
         <tli id="T391" time="213.28" type="appl" />
         <tli id="T392" time="213.4682222222222" type="appl" />
         <tli id="T393" time="213.76" type="appl" />
         <tli id="T394" time="214.29863500657672" />
         <tli id="T395" time="214.64644444444443" type="appl" />
         <tli id="T396" time="214.94099999999997" type="intp" />
         <tli id="T397" time="215.23555555555555" type="appl" />
         <tli id="T398" time="215.82466666666667" type="appl" />
         <tli id="T399" time="216.41377777777777" type="appl" />
         <tli id="T400" time="217.0028888888889" type="appl" />
         <tli id="T401" time="217.59199999999998" type="appl" />
         <tli id="T402" time="218.1811111111111" type="appl" />
         <tli id="T403" time="218.77022222222223" type="appl" />
         <tli id="T404" time="219.35933333333332" type="appl" />
         <tli id="T405" time="219.94844444444445" type="appl" />
         <tli id="T406" time="220.53755555555557" type="appl" />
         <tli id="T407" time="221.12666666666667" type="appl" />
         <tli id="T408" time="221.7157777777778" type="appl" />
         <tli id="T409" time="222.45191640688915" />
         <tli id="T410" time="222.55191576993468" />
         <tli id="T411" time="223.09191233038055" />
         <tli id="T412" time="223.38538461538462" type="appl" />
         <tli id="T413" time="223.77076923076922" type="appl" />
         <tli id="T414" time="224.15615384615384" type="appl" />
         <tli id="T415" time="224.54153846153847" type="appl" />
         <tli id="T416" time="224.92692307692306" type="appl" />
         <tli id="T417" time="225.31230769230768" type="appl" />
         <tli id="T418" time="225.6976923076923" type="appl" />
         <tli id="T419" time="226.08307692307693" type="appl" />
         <tli id="T420" time="226.46846153846153" type="appl" />
         <tli id="T421" time="226.85384615384615" type="appl" />
         <tli id="T422" time="227.23923076923077" type="appl" />
         <tli id="T423" time="227.62461538461537" type="appl" />
         <tli id="T424" time="228.01" type="appl" />
         <tli id="T425" time="228.04" type="appl" />
         <tli id="T426" time="228.59" type="appl" />
         <tli id="T427" time="228.64" type="appl" />
         <tli id="T428" time="229.01" type="appl" />
         <tli id="T429" time="229.27666666666667" type="appl" />
         <tli id="T430" time="229.4985714285714" type="appl" />
         <tli id="T431" time="229.91333333333333" type="appl" />
         <tli id="T432" time="229.98714285714286" type="appl" />
         <tli id="T433" time="230.47571428571428" type="appl" />
         <tli id="T434" time="230.55" type="appl" />
         <tli id="T435" time="230.96428571428572" type="appl" />
         <tli id="T436" time="231.45285714285714" type="appl" />
         <tli id="T437" time="231.9414285714286" type="appl" />
         <tli id="T438" time="232.43" type="appl" />
         <tli id="T439" time="232.8425" type="appl" />
         <tli id="T440" time="233.255" type="appl" />
         <tli id="T441" time="233.6675" type="appl" />
         <tli id="T442" time="234.07999999999998" type="appl" />
         <tli id="T443" time="234.4925" type="appl" />
         <tli id="T444" time="234.905" type="appl" />
         <tli id="T445" time="235.3175" type="appl" />
         <tli id="T446" time="235.73" type="appl" />
         <tli id="T447" time="235.79" type="appl" />
         <tli id="T448" time="236.165" type="appl" />
         <tli id="T449" time="236.54" type="appl" />
         <tli id="T450" time="236.915" type="appl" />
         <tli id="T451" time="237.29" type="appl" />
         <tli id="T452" time="237.665" type="appl" />
         <tli id="T453" time="238.04" type="appl" />
         <tli id="T454" time="238.415" type="appl" />
         <tli id="T455" time="238.79" type="appl" />
         <tli id="T456" time="239.11" type="appl" />
         <tli id="T457" time="239.4645625" type="appl" />
         <tli id="T458" time="239.819125" type="appl" />
         <tli id="T459" time="240.1736875" type="appl" />
         <tli id="T460" time="240.52825" type="appl" />
         <tli id="T461" time="240.8828125" type="appl" />
         <tli id="T462" time="241.23737500000001" type="appl" />
         <tli id="T463" time="241.5919375" type="appl" />
         <tli id="T464" time="241.94650000000001" type="appl" />
         <tli id="T465" time="242.3010625" type="appl" />
         <tli id="T466" time="242.655625" type="appl" />
         <tli id="T467" time="243.0101875" type="appl" />
         <tli id="T468" time="243.36475" type="appl" />
         <tli id="T469" time="243.7193125" type="appl" />
         <tli id="T470" time="244.073875" type="appl" />
         <tli id="T471" time="244.4284375" type="appl" />
         <tli id="T472" time="244.783" type="appl" />
         <tli id="T473" time="244.98" type="appl" />
         <tli id="T474" time="245.43" type="appl" />
         <tli id="T475" time="245.88" type="appl" />
         <tli id="T476" time="246.32999999999998" type="appl" />
         <tli id="T477" time="246.78" type="appl" />
         <tli id="T478" time="247.23000000000002" type="appl" />
         <tli id="T479" time="247.68" type="appl" />
         <tli id="T480" time="248.31" type="appl" />
         <tli id="T481" time="249.35500000000002" type="appl" />
         <tli id="T482" time="250.4" type="appl" />
         <tli id="T483" time="250.74" type="appl" />
         <tli id="T484" time="251.09363636363636" type="appl" />
         <tli id="T485" time="251.44727272727275" type="appl" />
         <tli id="T486" time="251.8009090909091" type="appl" />
         <tli id="T487" time="252.15454545454546" type="appl" />
         <tli id="T488" time="252.5081818181818" type="appl" />
         <tli id="T489" time="252.8618181818182" type="appl" />
         <tli id="T490" time="253.21545454545455" type="appl" />
         <tli id="T491" time="253.5690909090909" type="appl" />
         <tli id="T492" time="253.92272727272726" type="appl" />
         <tli id="T493" time="254.27636363636364" type="appl" />
         <tli id="T494" time="254.63" type="appl" />
         <tli id="T495" time="254.7783771674084" />
         <tli id="T496" time="255.94" type="appl" />
         <tli id="T497" time="257.06" type="appl" />
         <tli id="T498" time="258.18" type="appl" />
         <tli id="T499" time="259.3" type="appl" />
         <tli id="T500" time="260.42" type="appl" />
         <tli id="T501" time="261.54" type="appl" />
         <tli id="T502" time="262.66" type="appl" />
         <tli id="T503" time="262.93" type="appl" />
         <tli id="T504" time="263.8090909090909" type="appl" />
         <tli id="T505" time="264.6881818181818" type="appl" />
         <tli id="T506" time="265.5672727272727" type="appl" />
         <tli id="T507" time="266.4463636363636" type="appl" />
         <tli id="T508" time="267.32545454545453" type="appl" />
         <tli id="T509" time="268.2045454545455" type="appl" />
         <tli id="T510" time="269.0836363636364" type="appl" />
         <tli id="T511" time="269.9627272727273" type="appl" />
         <tli id="T512" time="270.8418181818182" type="appl" />
         <tli id="T513" time="271.7209090909091" type="appl" />
         <tli id="T514" time="272.6" type="appl" />
         <tli id="T515" time="272.98400000000004" type="appl" />
         <tli id="T516" time="273.368" type="appl" />
         <tli id="T517" time="273.752" type="appl" />
         <tli id="T518" time="274.13599999999997" type="appl" />
         <tli id="T519" time="274.52" type="appl" />
         <tli id="T520" time="274.88" type="appl" />
         <tli id="T521" time="275.73" type="appl" />
         <tli id="T522" time="276.58" type="appl" />
         <tli id="T523" time="277.43" type="appl" />
         <tli id="T524" time="278.03999999999996" type="appl" />
         <tli id="T525" time="278.65" type="appl" />
         <tli id="T526" time="278.72" type="appl" />
         <tli id="T527" time="279.53333333333336" type="appl" />
         <tli id="T528" time="280.3466666666667" type="appl" />
         <tli id="T529" time="281.63820608143845" />
         <tli id="T530" time="283.23" type="appl" />
         <tli id="T531" time="283.90999999999997" type="appl" />
         <tli id="T532" time="284.59" type="appl" />
         <tli id="T533" time="284.79" type="appl" />
         <tli id="T534" time="285.38250000000005" type="appl" />
         <tli id="T535" time="285.975" type="appl" />
         <tli id="T536" time="286.5675" type="appl" />
         <tli id="T537" time="287.16" type="appl" />
         <tli id="T538" time="290.37" type="appl" />
         <tli id="T539" time="291.08000000000004" type="appl" />
         <tli id="T540" time="291.79" type="appl" />
         <tli id="T541" time="297.46" type="appl" />
         <tli id="T542" time="298.1894" type="appl" />
         <tli id="T543" time="298.9188" type="appl" />
         <tli id="T544" time="299.64820000000003" type="appl" />
         <tli id="T545" time="300.37760000000003" type="appl" />
         <tli id="T546" time="300.38" type="appl" />
         <tli id="T547" time="300.7814285714286" type="appl" />
         <tli id="T548" time="301.107" type="appl" />
         <tli id="T549" time="301.18285714285713" type="appl" />
         <tli id="T550" time="301.5842857142857" type="appl" />
         <tli id="T551" time="301.98571428571427" type="appl" />
         <tli id="T552" time="302.38714285714286" type="appl" />
         <tli id="T553" time="302.7885714285714" type="appl" />
         <tli id="T554" time="303.19" type="appl" />
         <tli id="T555" time="303.58" type="appl" />
         <tli id="T556" time="304.204" type="appl" />
         <tli id="T557" time="304.828" type="appl" />
         <tli id="T558" time="305.452" type="appl" />
         <tli id="T559" time="306.07599999999996" type="appl" />
         <tli id="T560" time="306.7" type="appl" />
         <tli id="T561" time="307.62" type="appl" />
         <tli id="T562" time="308.27250000000004" type="appl" />
         <tli id="T563" time="308.925" type="appl" />
         <tli id="T564" time="309.4646955049087" />
         <tli id="T565" time="309.76" type="appl" />
         <tli id="T566" time="310.23" type="appl" />
         <tli id="T567" time="310.26933333333335" type="appl" />
         <tli id="T568" time="310.824686842328" />
         <tli id="T569" time="311.056343421164" type="intp" />
         <tli id="T570" time="311.288" type="appl" />
         <tli id="T571" time="311.29571428571427" type="appl" />
         <tli id="T572" time="311.89142857142855" type="appl" />
         <tli id="T573" time="312.4871428571428" type="appl" />
         <tli id="T574" time="313.08285714285716" type="appl" />
         <tli id="T575" time="313.67857142857144" type="appl" />
         <tli id="T576" time="314.2742857142857" type="appl" />
         <tli id="T577" time="314.9446605998039" />
         <tli id="T578" time="315.2" type="appl" />
         <tli id="T579" time="315.8933333333333" type="appl" />
         <tli id="T580" time="316.58666666666664" type="appl" />
         <tli id="T581" time="317.35131193709975" />
         <tli id="T582" time="317.89599999999996" type="appl" />
         <tli id="T583" time="318.512" type="appl" />
         <tli id="T584" time="319.128" type="appl" />
         <tli id="T585" time="319.744" type="appl" />
         <tli id="T586" time="320.39332381074587" />
         <tli id="T587" time="320.44" type="appl" />
         <tli id="T588" time="321.35" type="appl" />
         <tli id="T589" time="322.10461499386406" />
         <tli id="T590" time="323.45" type="appl" />
         <tli id="T591" time="324.115" type="appl" />
         <tli id="T592" time="324.78" type="appl" />
         <tli id="T593" time="325.445" type="appl" />
         <tli id="T594" time="326.11" type="appl" />
         <tli id="T595" time="326.775" type="appl" />
         <tli id="T596" time="327.4799974215741" />
         <tli id="T597" time="327.48333073367564" />
         <tli id="T598" time="328.00333333333333" type="appl" />
         <tli id="T599" time="328.5566666666667" type="appl" />
         <tli id="T600" time="329.11" type="appl" />
         <tli id="T601" time="329.6633333333333" type="appl" />
         <tli id="T602" time="330.21666666666664" type="appl" />
         <tli id="T603" time="330.7900023798821" />
         <tli id="T604" time="330.81" type="appl" />
         <tli id="T605" time="331.3525" type="appl" />
         <tli id="T606" time="331.895" type="appl" />
         <tli id="T607" time="332.4375" type="appl" />
         <tli id="T608" time="332.98" type="appl" />
         <tli id="T609" time="335.55" type="appl" />
         <tli id="T610" time="336.14923076923077" type="appl" />
         <tli id="T611" time="336.7484615384615" type="appl" />
         <tli id="T612" time="337.3476923076923" type="appl" />
         <tli id="T613" time="337.9469230769231" type="appl" />
         <tli id="T614" time="338.54615384615386" type="appl" />
         <tli id="T615" time="339.1453846153846" type="appl" />
         <tli id="T616" time="339.7446153846154" type="appl" />
         <tli id="T617" time="340.34384615384613" type="appl" />
         <tli id="T618" time="340.9430769230769" type="appl" />
         <tli id="T619" time="341.5423076923077" type="appl" />
         <tli id="T620" time="342.14153846153846" type="appl" />
         <tli id="T621" time="342.7407692307692" type="appl" />
         <tli id="T622" time="343.34" type="appl" />
         <tli id="T623" time="343.71" type="appl" />
         <tli id="T624" time="344.10499999999996" type="appl" />
         <tli id="T625" time="344.5" type="appl" />
         <tli id="T626" time="344.895" type="appl" />
         <tli id="T627" time="345.28999999999996" type="appl" />
         <tli id="T628" time="345.685" type="appl" />
         <tli id="T629" time="346.08" type="appl" />
         <tli id="T630" time="346.475" type="appl" />
         <tli id="T631" time="346.9577900227805" />
         <tli id="T632" time="347.07" type="appl" />
         <tli id="T633" time="347.50363636363636" type="appl" />
         <tli id="T634" time="347.9372727272727" type="appl" />
         <tli id="T635" time="348.3709090909091" type="appl" />
         <tli id="T636" time="348.80454545454546" type="appl" />
         <tli id="T637" time="349.23818181818183" type="appl" />
         <tli id="T638" time="349.67181818181814" type="appl" />
         <tli id="T639" time="350.1054545454545" type="appl" />
         <tli id="T640" time="350.5390909090909" type="appl" />
         <tli id="T641" time="350.97272727272724" type="appl" />
         <tli id="T642" time="351.4063636363636" type="appl" />
         <tli id="T643" time="351.84" type="appl" />
         <tli id="T644" time="351.93" type="appl" />
         <tli id="T645" time="352.27727272727276" type="appl" />
         <tli id="T646" time="352.62454545454545" type="appl" />
         <tli id="T647" time="352.9718181818182" type="appl" />
         <tli id="T648" time="353.3190909090909" type="appl" />
         <tli id="T649" time="353.66636363636366" type="appl" />
         <tli id="T650" time="354.01363636363635" type="appl" />
         <tli id="T651" time="354.3609090909091" type="appl" />
         <tli id="T652" time="354.7081818181818" type="appl" />
         <tli id="T653" time="355.05545454545455" type="appl" />
         <tli id="T654" time="355.40272727272725" type="appl" />
         <tli id="T655" time="356.09666410360967" />
         <tli id="T656" time="356.16333333333336" type="appl" />
         <tli id="T657" time="356.57666666666665" type="appl" />
         <tli id="T658" time="356.99" type="appl" />
         <tli id="T659" time="357.40333333333336" type="appl" />
         <tli id="T660" time="357.81666666666666" type="appl" />
         <tli id="T661" time="358.23" type="appl" />
         <tli id="T662" time="358.6433333333334" type="appl" />
         <tli id="T663" time="359.0566666666667" type="appl" />
         <tli id="T664" time="359.7977082378269" />
         <tli id="T665" time="360.11" type="appl" />
         <tli id="T666" time="360.548" type="appl" />
         <tli id="T667" time="360.986" type="appl" />
         <tli id="T668" time="361.42400000000004" type="appl" />
         <tli id="T669" time="361.862" type="appl" />
         <tli id="T670" time="362.3" type="appl" />
         <tli id="T671" time="364.14" type="appl" />
         <tli id="T672" time="365.3233333333333" type="appl" />
         <tli id="T673" time="366.50666666666666" type="appl" />
         <tli id="T674" time="367.69" type="appl" />
         <tli id="T675" time="367.91" type="appl" />
         <tli id="T676" time="368.40500000000003" type="appl" />
         <tli id="T677" time="368.90000000000003" type="appl" />
         <tli id="T678" time="369.395" type="appl" />
         <tli id="T679" time="369.89" type="appl" />
         <tli id="T680" time="370.385" type="appl" />
         <tli id="T681" time="370.88" type="appl" />
         <tli id="T682" time="370.93" type="appl" />
         <tli id="T683" time="371.52250000000004" type="appl" />
         <tli id="T684" time="372.115" type="appl" />
         <tli id="T685" time="372.7075" type="appl" />
         <tli id="T686" time="373.3" type="appl" />
         <tli id="T687" time="373.61" type="appl" />
         <tli id="T688" time="374.3625" type="appl" />
         <tli id="T689" time="375.115" type="appl" />
         <tli id="T690" time="375.8675" type="appl" />
         <tli id="T691" time="376.62" type="appl" />
         <tli id="T692" time="376.78" type="appl" />
         <tli id="T693" time="377.22777777777776" type="appl" />
         <tli id="T694" time="377.67555555555555" type="appl" />
         <tli id="T695" time="378.12333333333333" type="appl" />
         <tli id="T696" time="378.5711111111111" type="appl" />
         <tli id="T697" time="379.01888888888885" type="appl" />
         <tli id="T698" time="379.46666666666664" type="appl" />
         <tli id="T699" time="379.9144444444444" type="appl" />
         <tli id="T700" time="380.3622222222222" type="appl" />
         <tli id="T701" time="380.9775733308707" />
         <tli id="T702" time="381.39" type="appl" />
         <tli id="T703" time="382.3785" type="appl" />
         <tli id="T704" time="383.36699999999996" type="appl" />
         <tli id="T705" time="384.3555" type="appl" />
         <tli id="T706" time="384.8175488718192" />
         <tli id="T707" time="385.5175444131379" />
         <tli id="T708" time="385.85" type="appl" />
         <tli id="T709" time="386.27" type="appl" />
         <tli id="T710" time="386.69" type="appl" />
         <tli id="T711" time="387.10999999999996" type="appl" />
         <tli id="T712" time="387.53" type="appl" />
         <tli id="T713" time="389.45" type="appl" />
         <tli id="T714" time="389.71000000000004" type="appl" />
         <tli id="T715" time="389.93" type="appl" />
         <tli id="T716" time="389.97" type="appl" />
         <tli id="T717" time="390.64" type="appl" />
         <tli id="T718" time="391.075" type="appl" />
         <tli id="T719" time="391.51" type="appl" />
         <tli id="T720" time="391.945" type="appl" />
         <tli id="T721" time="392.4708334569039" />
         <tli id="T722" time="393.48" type="appl" />
         <tli id="T723" time="394.385" type="appl" />
         <tli id="T724" time="395.29" type="appl" />
         <tli id="T725" time="396.16" type="appl" />
         <tli id="T726" time="396.7331666666667" type="appl" />
         <tli id="T727" time="397.3063333333333" type="appl" />
         <tli id="T728" time="397.62413396585043" />
         <tli id="T729" time="397.8795" type="appl" />
         <tli id="T730" time="398.2128333333333" type="appl" />
         <tli id="T731" time="398.4526666666667" type="appl" />
         <tli id="T732" time="399.0258333333333" type="appl" />
         <tli id="T733" time="399.11566666666664" type="appl" />
         <tli id="T734" time="399.599" type="appl" />
         <tli id="T735" time="400.0185" type="appl" />
         <tli id="T736" time="400.92133333333334" type="appl" />
         <tli id="T737" time="401.82416666666666" type="appl" />
         <tli id="T738" time="402.727" type="appl" />
         <tli id="T739" time="402.92" type="appl" />
         <tli id="T740" time="403.47818181818184" type="appl" />
         <tli id="T741" time="404.03636363636366" type="appl" />
         <tli id="T742" time="404.5945454545455" type="appl" />
         <tli id="T743" time="405.1527272727273" type="appl" />
         <tli id="T744" time="405.7109090909091" type="appl" />
         <tli id="T745" time="406.2690909090909" type="appl" />
         <tli id="T746" time="406.8272727272727" type="appl" />
         <tli id="T747" time="407.38545454545454" type="appl" />
         <tli id="T748" time="407.94363636363636" type="appl" />
         <tli id="T749" time="408.5018181818182" type="appl" />
         <tli id="T750" time="409.06" type="appl" />
         <tli id="T751" time="409.15" type="appl" />
         <tli id="T752" time="409.82374999999996" type="appl" />
         <tli id="T753" time="410.4975" type="appl" />
         <tli id="T754" time="411.17125" type="appl" />
         <tli id="T755" time="411.845" type="appl" />
         <tli id="T756" time="412.51875" type="appl" />
         <tli id="T757" time="413.1925" type="appl" />
         <tli id="T758" time="413.86625000000004" type="appl" />
         <tli id="T759" time="414.6640254288092" />
         <tli id="T760" time="414.8" type="appl" />
         <tli id="T761" time="415.31" type="appl" />
         <tli id="T762" time="415.82" type="appl" />
         <tli id="T763" time="416.33000000000004" type="appl" />
         <tli id="T764" time="416.84000000000003" type="appl" />
         <tli id="T765" time="417.35" type="appl" />
         <tli id="T766" time="417.9" type="appl" />
         <tli id="T767" time="418.5783333333333" type="appl" />
         <tli id="T768" time="419.25666666666666" type="appl" />
         <tli id="T769" time="419.935" type="appl" />
         <tli id="T770" time="420.61333333333334" type="appl" />
         <tli id="T771" time="421.2916666666667" type="appl" />
         <tli id="T772" time="421.97" type="appl" />
         <tli id="T773" time="422.2039774024424" />
         <tli id="T774" time="423.19" type="appl" />
         <tli id="T775" time="423.91999999999996" type="appl" />
         <tli id="T776" time="424.65" type="appl" />
         <tli id="T777" time="425.84" type="appl" />
         <tli id="T778" time="427.3639445355919" />
         <tli id="T779" time="427.38001214158123" />
         <tli id="T780" time="427.96" type="appl" />
         <tli id="T781" time="428.66" type="appl" />
         <tli id="T782" time="429.36" type="appl" />
         <tli id="T783" time="431.84" type="appl" />
         <tli id="T784" time="432.3142857142857" type="appl" />
         <tli id="T785" time="432.7885714285714" type="appl" />
         <tli id="T786" time="433.2628571428571" type="appl" />
         <tli id="T787" time="433.7371428571429" type="appl" />
         <tli id="T788" time="434.2114285714286" type="appl" />
         <tli id="T789" time="434.6857142857143" type="appl" />
         <tli id="T790" time="435.16" type="appl" />
         <tli id="T791" time="435.66" type="appl" />
         <tli id="T792" time="436.12111111111113" type="appl" />
         <tli id="T793" time="436.58222222222224" type="appl" />
         <tli id="T794" time="437.04333333333335" type="appl" />
         <tli id="T795" time="437.50444444444446" type="appl" />
         <tli id="T796" time="437.96555555555557" type="appl" />
         <tli id="T797" time="438.4266666666667" type="appl" />
         <tli id="T798" time="438.8877777777778" type="appl" />
         <tli id="T799" time="439.3488888888889" type="appl" />
         <tli id="T800" time="439.81" type="appl" />
         <tli id="T801" time="439.8766773346737" />
         <tli id="T802" time="440.69500000000005" type="appl" />
         <tli id="T803" time="441.70385319632123" />
         <tli id="T804" time="441.7066656784069" />
         <tli id="T805" time="442.03000000000003" type="appl" />
         <tli id="T806" time="442.40000000000003" type="appl" />
         <tli id="T807" time="442.77000000000004" type="appl" />
         <tli id="T808" time="443.14" type="appl" />
         <tli id="T809" time="443.51" type="appl" />
         <tli id="T810" time="443.88" type="appl" />
         <tli id="T811" time="444.25" type="appl" />
         <tli id="T812" time="444.26" type="appl" />
         <tli id="T813" time="444.615" type="appl" />
         <tli id="T814" time="444.96999999999997" type="appl" />
         <tli id="T815" time="445.325" type="appl" />
         <tli id="T816" time="445.68" type="appl" />
         <tli id="T817" time="446.03499999999997" type="appl" />
         <tli id="T818" time="446.39" type="appl" />
         <tli id="T819" time="446.74" type="appl" />
         <tli id="T820" time="447.16" type="appl" />
         <tli id="T821" time="447.58" type="appl" />
         <tli id="T822" time="447.78" type="appl" />
         <tli id="T823" time="447.98667776066793" />
         <tli id="T824" time="448.29999999999995" type="appl" />
         <tli id="T825" time="448.82" type="appl" />
         <tli id="T826" time="449.34000000000003" type="appl" />
         <tli id="T827" time="450.1704659341763" />
         <tli id="T828" time="450.663" type="appl" />
         <tli id="T829" time="451.2644" type="appl" />
         <tli id="T830" time="451.86580000000004" type="appl" />
         <tli id="T831" time="452.4672" type="appl" />
         <tli id="T832" time="453.0686" type="appl" />
         <tli id="T833" time="453.303" type="appl" />
         <tli id="T834" time="453.67" type="appl" />
         <tli id="T835" time="453.7196666666667" type="appl" />
         <tli id="T836" time="454.1363333333333" type="appl" />
         <tli id="T837" time="454.553" type="appl" />
         <tli id="T838" time="454.9696666666667" type="appl" />
         <tli id="T839" time="455.3863333333333" type="appl" />
         <tli id="T840" time="456.1237613474871" />
         <tli id="T841" time="457.363" type="appl" />
         <tli id="T842" time="458.153" type="appl" />
         <tli id="T843" time="458.943" type="appl" />
         <tli id="T844" time="459.493" type="appl" />
         <tli id="T845" time="459.6437389266898" />
         <tli id="T846" time="460.513" type="appl" />
         <tli id="T847" time="461.483" type="appl" />
         <tli id="T848" time="462.228" type="appl" />
         <tli id="T849" time="462.973" type="appl" />
         <tli id="T850" time="467.14" type="appl" />
         <tli id="T851" time="467.64914285714286" type="appl" />
         <tli id="T852" time="468.1582857142857" type="appl" />
         <tli id="T853" time="468.66742857142856" type="appl" />
         <tli id="T854" time="469.17657142857144" type="appl" />
         <tli id="T855" time="469.6857142857143" type="appl" />
         <tli id="T856" time="469.89" type="appl" />
         <tli id="T857" time="470.19485714285713" type="appl" />
         <tli id="T858" time="470.65" type="appl" />
         <tli id="T859" time="470.704" type="appl" />
         <tli id="T860" time="471.31" type="appl" />
         <tli id="T861" time="472.4483333333333" type="appl" />
         <tli id="T862" time="473.58666666666664" type="appl" />
         <tli id="T863" time="474.725" type="appl" />
         <tli id="T864" time="475.86333333333334" type="appl" />
         <tli id="T865" time="477.00166666666667" type="appl" />
         <tli id="T866" time="478.14" type="appl" />
         <tli id="T867" time="479.38" type="appl" />
         <tli id="T868" time="479.885" type="appl" />
         <tli id="T869" time="480.39" type="appl" />
         <tli id="T870" time="480.895" type="appl" />
         <tli id="T871" time="481.40000000000003" type="appl" />
         <tli id="T872" time="481.90500000000003" type="appl" />
         <tli id="T873" time="482.41" type="appl" />
         <tli id="T874" time="482.43" type="appl" />
         <tli id="T875" time="483.1575" type="appl" />
         <tli id="T876" time="483.885" type="appl" />
         <tli id="T877" time="484.61249999999995" type="appl" />
         <tli id="T878" time="485.34" type="appl" />
         <tli id="T879" time="485.49" type="appl" />
         <tli id="T880" time="486.09857142857146" type="appl" />
         <tli id="T881" time="486.70714285714286" type="appl" />
         <tli id="T882" time="487.3157142857143" type="appl" />
         <tli id="T883" time="487.9242857142857" type="appl" />
         <tli id="T884" time="488.53285714285715" type="appl" />
         <tli id="T885" time="489.14142857142855" type="appl" />
         <tli id="T886" time="489.75" type="appl" />
         <tli id="T887" time="491.8035340821332" />
         <tli id="T888" time="492.33750000000003" type="appl" />
         <tli id="T889" time="492.95500000000004" type="appl" />
         <tli id="T890" time="493.57250000000005" type="appl" />
         <tli id="T891" time="494.19000000000005" type="appl" />
         <tli id="T892" time="494.8075" type="appl" />
         <tli id="T893" time="495.425" type="appl" />
         <tli id="T894" time="496.0425" type="appl" />
         <tli id="T895" time="496.66" type="appl" />
         <tli id="T896" time="496.85" type="appl" />
         <tli id="T897" time="497.3616666666667" type="appl" />
         <tli id="T898" time="497.87333333333333" type="appl" />
         <tli id="T899" time="498.385" type="appl" />
         <tli id="T900" time="498.8966666666667" type="appl" />
         <tli id="T901" time="499.40833333333336" type="appl" />
         <tli id="T902" time="499.92" type="appl" />
         <tli id="T903" time="499.96" type="appl" />
         <tli id="T904" time="500.48" type="appl" />
         <tli id="T905" time="501.0" type="appl" />
         <tli id="T906" time="501.52" type="appl" />
         <tli id="T907" time="502.04" type="appl" />
         <tli id="T908" time="503.6" type="appl" />
         <tli id="T909" time="504.298" type="appl" />
         <tli id="T910" time="504.996" type="appl" />
         <tli id="T911" time="505.694" type="appl" />
         <tli id="T912" time="506.392" type="appl" />
         <tli id="T913" time="507.27010223317563" />
         <tli id="T914" time="509.08" type="appl" />
         <tli id="T915" time="509.6" type="appl" />
         <tli id="T916" time="511.02" type="appl" />
         <tli id="T917" time="512.0625" type="appl" />
         <tli id="T918" time="513.105" type="appl" />
         <tli id="T919" time="514.1475" type="appl" />
         <tli id="T920" time="515.19" type="appl" />
         <tli id="T921" time="515.84" type="appl" />
         <tli id="T922" time="516.645" type="appl" />
         <tli id="T923" time="517.45" type="appl" />
         <tli id="T924" time="518.255" type="appl" />
         <tli id="T925" time="519.06" type="appl" />
         <tli id="T926" time="519.27" type="appl" />
         <tli id="T927" time="520.2357142857143" type="appl" />
         <tli id="T928" time="521.2014285714286" type="appl" />
         <tli id="T929" time="522.1671428571428" type="appl" />
         <tli id="T930" time="523.1328571428571" type="appl" />
         <tli id="T931" time="524.0985714285714" type="appl" />
         <tli id="T932" time="525.0642857142857" type="appl" />
         <tli id="T933" time="526.03" type="appl" />
         <tli id="T934" time="526.12" type="appl" />
         <tli id="T935" time="526.5566666666666" type="appl" />
         <tli id="T936" time="526.9933333333333" type="appl" />
         <tli id="T937" time="527.43" type="appl" />
         <tli id="T938" time="527.59" type="appl" />
         <tli id="T939" time="528.3316666666667" type="appl" />
         <tli id="T940" time="529.0733333333334" type="appl" />
         <tli id="T941" time="529.815" type="appl" />
         <tli id="T942" time="530.5566666666667" type="appl" />
         <tli id="T943" time="531.2983333333334" type="appl" />
         <tli id="T944" time="532.04" type="appl" />
         <tli id="T945" time="532.7816666666666" type="appl" />
         <tli id="T946" time="533.5233333333333" type="appl" />
         <tli id="T947" time="534.265" type="appl" />
         <tli id="T948" time="535.0066666666667" type="appl" />
         <tli id="T949" time="535.7483333333333" type="appl" />
         <tli id="T950" time="536.49" type="appl" />
         <tli id="T951" time="537.46" type="appl" />
         <tli id="T952" time="538.28875" type="appl" />
         <tli id="T953" time="539.1175000000001" type="appl" />
         <tli id="T954" time="539.9462500000001" type="appl" />
         <tli id="T955" time="540.7750000000001" type="appl" />
         <tli id="T956" time="541.60375" type="appl" />
         <tli id="T957" time="542.4325" type="appl" />
         <tli id="T958" time="543.26125" type="appl" />
         <tli id="T959" time="544.2565333116166" />
         <tli id="T960" time="544.46" type="appl" />
         <tli id="T961" time="545.2" type="appl" />
         <tli id="T962" time="545.94" type="appl" />
         <tli id="T963" time="546.6800000000001" type="appl" />
         <tli id="T964" time="547.4200000000001" type="appl" />
         <tli id="T965" time="548.16" type="appl" />
         <tli id="T966" time="548.9" type="appl" />
         <tli id="T967" time="549.64" type="appl" />
         <tli id="T968" time="550.38" type="appl" />
         <tli id="T969" time="552.28" type="appl" />
         <tli id="T970" time="553.3090909090909" type="appl" />
         <tli id="T971" time="554.3381818181819" type="appl" />
         <tli id="T972" time="555.3672727272727" type="appl" />
         <tli id="T973" time="556.3963636363636" type="appl" />
         <tli id="T974" time="557.4254545454545" type="appl" />
         <tli id="T975" time="558.4545454545455" type="appl" />
         <tli id="T976" time="559.4836363636364" type="appl" />
         <tli id="T977" time="560.5127272727273" type="appl" />
         <tli id="T978" time="561.5418181818181" type="appl" />
         <tli id="T979" time="562.5709090909091" type="appl" />
         <tli id="T980" time="563.6" type="appl" />
         <tli id="T981" time="563.69" type="appl" />
         <tli id="T982" time="564.7900000000001" type="appl" />
         <tli id="T983" time="565.89" type="appl" />
         <tli id="T984" time="566.99" type="appl" />
         <tli id="T985" time="568.44" type="appl" />
         <tli id="T986" time="569.5500000000001" type="appl" />
         <tli id="T987" time="570.66" type="appl" />
         <tli id="T988" time="571.77" type="appl" />
         <tli id="T989" time="572.395" type="appl" />
         <tli id="T990" time="573.02" type="appl" />
         <tli id="T991" time="573.645" type="appl" />
         <tli id="T992" time="574.27" type="appl" />
         <tli id="T993" time="574.29" type="appl" />
         <tli id="T994" time="574.78" type="appl" />
         <tli id="T995" time="575.27" type="appl" />
         <tli id="T996" time="575.76" type="appl" />
         <tli id="T997" time="576.4229950912629" />
         <tli id="T998" time="576.58" type="appl" />
         <tli id="T1525" time="577.195" type="intp" />
         <tli id="T999" time="578.0150000000001" type="appl" />
         <tli id="T1000" time="579.45" type="appl" />
         <tli id="T1001" time="580.57" type="appl" />
         <tli id="T1002" time="581.5160000000001" type="appl" />
         <tli id="T1003" time="582.462" type="appl" />
         <tli id="T1004" time="583.408" type="appl" />
         <tli id="T1005" time="584.3539999999999" type="appl" />
         <tli id="T1006" time="585.3829380201428" />
         <tli id="T1007" time="587.37" type="appl" />
         <tli id="T1008" time="588.166" type="appl" />
         <tli id="T1009" time="588.962" type="appl" />
         <tli id="T1010" time="589.758" type="appl" />
         <tli id="T1011" time="590.554" type="appl" />
         <tli id="T1012" time="591.35" type="appl" />
         <tli id="T1013" time="591.54" type="appl" />
         <tli id="T1014" time="592.0925" type="appl" />
         <tli id="T1522" time="592.3475" type="intp" />
         <tli id="T1015" time="592.645" type="appl" />
         <tli id="T1016" time="593.1975" type="appl" />
         <tli id="T1017" time="593.75" type="appl" />
         <tli id="T1018" time="594.47" type="appl" />
         <tli id="T1019" time="595.905" type="appl" />
         <tli id="T1020" time="597.34" type="appl" />
         <tli id="T1021" time="598.7750000000001" type="appl" />
         <tli id="T1022" time="600.21" type="appl" />
         <tli id="T1023" time="601.645" type="appl" />
         <tli id="T1024" time="603.08" type="appl" />
         <tli id="T1025" time="604.5150000000001" type="appl" />
         <tli id="T1026" time="605.95" type="appl" />
         <tli id="T1027" time="607.385" type="appl" />
         <tli id="T1028" time="608.9694544504825" />
         <tli id="T1029" time="609.44" type="appl" />
         <tli id="T1030" time="610.2978571428572" type="appl" />
         <tli id="T1031" time="611.1557142857143" type="appl" />
         <tli id="T1032" time="612.0135714285715" type="appl" />
         <tli id="T1033" time="612.8714285714286" type="appl" />
         <tli id="T1034" time="613.7292857142858" type="appl" />
         <tli id="T1035" time="614.5871428571429" type="appl" />
         <tli id="T1036" time="615.445" type="appl" />
         <tli id="T1037" time="616.3028571428572" type="appl" />
         <tli id="T1038" time="617.1607142857143" type="appl" />
         <tli id="T1039" time="618.0185714285715" type="appl" />
         <tli id="T1040" time="618.8764285714286" type="appl" />
         <tli id="T1041" time="619.7342857142858" type="appl" />
         <tli id="T1042" time="620.5921428571429" type="appl" />
         <tli id="T1043" time="621.5360410732044" />
         <tli id="T1044" time="622.4" type="appl" />
         <tli id="T1045" time="623.208" type="appl" />
         <tli id="T1046" time="624.016" type="appl" />
         <tli id="T1047" time="624.8240000000001" type="appl" />
         <tli id="T1048" time="625.6320000000001" type="appl" />
         <tli id="T1049" time="626.6560084611357" />
         <tli id="T1050" time="627.22" type="appl" />
         <tli id="T1051" time="628.1175000000001" type="appl" />
         <tli id="T1052" time="629.015" type="appl" />
         <tli id="T1053" time="629.9124999999999" type="appl" />
         <tli id="T1054" time="630.81" type="appl" />
         <tli id="T1055" time="630.8266589791549" />
         <tli id="T1056" time="631.62" type="appl" />
         <tli id="T1057" time="632.4226383967614" />
         <tli id="T1058" time="632.86" type="appl" />
         <tli id="T1059" time="633.4983333333333" type="appl" />
         <tli id="T1060" time="634.1366666666667" type="appl" />
         <tli id="T1061" time="634.7750000000001" type="appl" />
         <tli id="T1062" time="635.4133333333334" type="appl" />
         <tli id="T1063" time="636.0516666666667" type="appl" />
         <tli id="T1064" time="637.0300048830812" />
         <tli id="T1065" time="637.88" type="appl" />
         <tli id="T1066" time="638.5275" type="appl" />
         <tli id="T1067" time="639.175" type="appl" />
         <tli id="T1068" time="639.8225" type="appl" />
         <tli id="T1069" time="640.47" type="appl" />
         <tli id="T1070" time="640.53" type="appl" />
         <tli id="T1071" time="641.135" type="appl" />
         <tli id="T1072" time="641.74" type="appl" />
         <tli id="T1073" time="642.345" type="appl" />
         <tli id="T1074" time="642.9499999999999" type="appl" />
         <tli id="T1075" time="643.555" type="appl" />
         <tli id="T1076" time="644.16" type="appl" />
         <tli id="T1077" time="644.2" type="appl" />
         <tli id="T1078" time="645.73125" type="appl" />
         <tli id="T1079" time="647.2625" type="appl" />
         <tli id="T1080" time="648.79375" type="appl" />
         <tli id="T1081" time="650.325" type="appl" />
         <tli id="T1082" time="651.85625" type="appl" />
         <tli id="T1083" time="653.3875" type="appl" />
         <tli id="T1084" time="654.91875" type="appl" />
         <tli id="T1085" time="656.45" type="appl" />
         <tli id="T1086" time="656.69" type="appl" />
         <tli id="T1087" time="657.4166666666667" type="appl" />
         <tli id="T1088" time="658.1433333333333" type="appl" />
         <tli id="T1089" time="658.87" type="appl" />
         <tli id="T1090" time="659.5966666666667" type="appl" />
         <tli id="T1091" time="660.3233333333333" type="appl" />
         <tli id="T1092" time="661.1166743776531" />
         <tli id="T1093" time="662.15" type="appl" />
         <tli id="T1094" time="662.765" type="appl" />
         <tli id="T1095" time="663.38" type="appl" />
         <tli id="T1096" time="663.995" type="appl" />
         <tli id="T1097" time="664.8033175619319" />
         <tli id="T1098" time="665.1" type="appl" />
         <tli id="T1099" time="665.9200000000001" type="appl" />
         <tli id="T1100" time="666.74" type="appl" />
         <tli id="T1101" time="667.2800205363611" />
         <tli id="T1102" time="668.005" type="appl" />
         <tli id="T1103" time="668.65" type="appl" />
         <tli id="T1104" time="669.2950000000001" type="appl" />
         <tli id="T1105" time="669.94" type="appl" />
         <tli id="T1106" time="670.585" type="appl" />
         <tli id="T1107" time="671.3366613639083" />
         <tli id="T1108" time="671.65" type="appl" />
         <tli id="T1109" time="672.082" type="appl" />
         <tli id="T1110" time="672.514" type="appl" />
         <tli id="T1111" time="672.9459999999999" type="appl" />
         <tli id="T1112" time="673.3779999999999" type="appl" />
         <tli id="T1113" time="673.8366454400466" />
         <tli id="T1114" time="674.39" type="appl" />
         <tli id="T1115" time="675.0633333333333" type="appl" />
         <tli id="T1116" time="675.7366666666667" type="appl" />
         <tli id="T1523" time="676.0252380952381" type="intp" />
         <tli id="T1117" time="676.41" type="appl" />
         <tli id="T1118" time="676.5" type="appl" />
         <tli id="T1119" time="677.175" type="appl" />
         <tli id="T1120" time="677.85" type="appl" />
         <tli id="T1121" time="678.525" type="appl" />
         <tli id="T1122" time="679.1999999999999" type="appl" />
         <tli id="T1123" time="679.875" type="appl" />
         <tli id="T1124" time="680.55" type="appl" />
         <tli id="T1125" time="680.69" type="appl" />
         <tli id="T1126" time="682.0771428571429" type="appl" />
         <tli id="T1127" time="683.4642857142858" type="appl" />
         <tli id="T1128" time="684.8514285714286" type="appl" />
         <tli id="T1129" time="686.2385714285715" type="appl" />
         <tli id="T1130" time="687.6257142857144" type="appl" />
         <tli id="T1131" time="689.0128571428572" type="appl" />
         <tli id="T1132" time="690.4000000000001" type="appl" />
         <tli id="T1133" time="691.7871428571428" type="appl" />
         <tli id="T1134" time="693.1742857142857" type="appl" />
         <tli id="T1135" time="694.5614285714286" type="appl" />
         <tli id="T1136" time="695.9485714285714" type="appl" />
         <tli id="T1137" time="697.3357142857143" type="appl" />
         <tli id="T1138" time="698.7228571428572" type="appl" />
         <tli id="T1139" time="700.11" type="appl" />
         <tli id="T1140" time="705.75" type="appl" />
         <tli id="T1141" time="706.631375" type="appl" />
         <tli id="T1142" time="707.51275" type="appl" />
         <tli id="T1143" time="708.394125" type="appl" />
         <tli id="T1144" time="709.2755" type="appl" />
         <tli id="T1145" time="710.156875" type="appl" />
         <tli id="T1146" time="711.0382500000001" type="appl" />
         <tli id="T1147" time="711.919625" type="appl" />
         <tli id="T1148" time="712.801" type="appl" />
         <tli id="T1149" time="713.94" type="appl" />
         <tli id="T1150" time="714.44" type="appl" />
         <tli id="T1151" time="715.119976232352" />
         <tli id="T1152" time="715.1366427928594" />
         <tli id="T1153" time="715.2283333333334" type="appl" />
         <tli id="T1154" time="715.4266666666666" type="appl" />
         <tli id="T1155" time="715.625" type="appl" />
         <tli id="T1156" time="715.8233333333333" type="appl" />
         <tli id="T1157" time="716.0216666666666" type="appl" />
         <tli id="T1158" time="716.22" type="appl" />
         <tli id="T1159" time="716.4183333333333" type="appl" />
         <tli id="T1160" time="716.6166666666667" type="appl" />
         <tli id="T1161" time="716.8149999999999" type="appl" />
         <tli id="T1162" time="717.0133333333333" type="appl" />
         <tli id="T1163" time="717.2116666666666" type="appl" />
         <tli id="T1164" time="718.2420917624376" />
         <tli id="T1165" time="719.25" type="appl" />
         <tli id="T1166" time="719.796875" type="appl" />
         <tli id="T1167" time="720.34375" type="appl" />
         <tli id="T1168" time="720.890625" type="appl" />
         <tli id="T1169" time="721.4375" type="appl" />
         <tli id="T1170" time="721.984375" type="appl" />
         <tli id="T1171" time="722.53125" type="appl" />
         <tli id="T1172" time="723.078125" type="appl" />
         <tli id="T1173" time="723.625" type="appl" />
         <tli id="T1174" time="723.98" type="appl" />
         <tli id="T1175" time="724.8466666666667" type="appl" />
         <tli id="T1176" time="725.7133333333334" type="appl" />
         <tli id="T1177" time="726.5799999999999" type="appl" />
         <tli id="T1178" time="727.4466666666666" type="appl" />
         <tli id="T1179" time="728.3133333333333" type="appl" />
         <tli id="T1180" time="729.18" type="appl" />
         <tli id="T1181" time="729.43" type="appl" />
         <tli id="T1182" time="729.8528571428571" type="appl" />
         <tli id="T1183" time="730.2757142857142" type="appl" />
         <tli id="T1184" time="730.6985714285714" type="appl" />
         <tli id="T1185" time="731.1214285714285" type="appl" />
         <tli id="T1186" time="731.5442857142857" type="appl" />
         <tli id="T1187" time="731.9671428571428" type="appl" />
         <tli id="T1188" time="732.39" type="appl" />
         <tli id="T1189" time="732.8128571428572" type="appl" />
         <tli id="T1190" time="733.2357142857143" type="appl" />
         <tli id="T1191" time="733.6585714285715" type="appl" />
         <tli id="T1192" time="734.0814285714285" type="appl" />
         <tli id="T1193" time="734.5042857142857" type="appl" />
         <tli id="T1194" time="734.9271428571428" type="appl" />
         <tli id="T1195" time="735.35" type="appl" />
         <tli id="T1196" time="735.51" type="appl" />
         <tli id="T1197" time="736.027" type="appl" />
         <tli id="T1198" time="736.544" type="appl" />
         <tli id="T1199" time="737.0609999999999" type="appl" />
         <tli id="T1200" time="737.578" type="appl" />
         <tli id="T1201" time="738.095" type="appl" />
         <tli id="T1202" time="738.612" type="appl" />
         <tli id="T1203" time="739.1289999999999" type="appl" />
         <tli id="T1204" time="739.646" type="appl" />
         <tli id="T1205" time="740.37" type="appl" />
         <tli id="T1206" time="740.9111111111112" type="appl" />
         <tli id="T1207" time="741.4522222222222" type="appl" />
         <tli id="T1208" time="741.9933333333333" type="appl" />
         <tli id="T1209" time="742.5344444444445" type="appl" />
         <tli id="T1210" time="743.0755555555555" type="appl" />
         <tli id="T1211" time="743.6166666666667" type="appl" />
         <tli id="T1212" time="744.1577777777778" type="appl" />
         <tli id="T1213" time="744.6988888888889" type="appl" />
         <tli id="T1214" time="745.24" type="appl" />
         <tli id="T1215" time="745.57" type="appl" />
         <tli id="T1216" time="746.0414285714286" type="appl" />
         <tli id="T1217" time="746.5128571428572" type="appl" />
         <tli id="T1218" time="746.9842857142858" type="appl" />
         <tli id="T1219" time="747.4557142857143" type="appl" />
         <tli id="T1220" time="747.9271428571428" type="appl" />
         <tli id="T1221" time="748.3985714285715" type="appl" />
         <tli id="T1222" time="748.87" type="appl" />
         <tli id="T1223" time="749.3414285714285" type="appl" />
         <tli id="T1224" time="749.8128571428572" type="appl" />
         <tli id="T1225" time="750.2842857142857" type="appl" />
         <tli id="T1226" time="750.7557142857142" type="appl" />
         <tli id="T1227" time="751.2271428571428" type="appl" />
         <tli id="T1228" time="751.6985714285714" type="appl" />
         <tli id="T1229" time="752.17" type="appl" />
         <tli id="T1230" time="752.54" type="appl" />
         <tli id="T1231" time="753.4266666666666" type="appl" />
         <tli id="T1232" time="754.3133333333334" type="appl" />
         <tli id="T1233" time="755.2" type="appl" />
         <tli id="T1234" time="755.38" type="appl" />
         <tli id="T1235" time="756.4633333333334" type="appl" />
         <tli id="T1236" time="757.5466666666666" type="appl" />
         <tli id="T1237" time="758.63" type="appl" />
         <tli id="T1238" time="758.86" type="appl" />
         <tli id="T1239" time="759.5575" type="appl" />
         <tli id="T1240" time="760.255" type="appl" />
         <tli id="T1241" time="760.9525" type="appl" />
         <tli id="T1242" time="761.65" type="appl" />
         <tli id="T1243" time="761.66" type="appl" />
         <tli id="T1244" time="762.2" type="appl" />
         <tli id="T1245" time="762.74" type="appl" />
         <tli id="T1246" time="763.28" type="appl" />
         <tli id="T1247" time="764.0484666614947" />
         <tli id="T1248" time="764.53" type="appl" />
         <tli id="T1249" time="765.055" type="appl" />
         <tli id="T1250" time="765.5799999999999" type="appl" />
         <tli id="T1251" time="766.105" type="appl" />
         <tli id="T1252" time="766.63" type="appl" />
         <tli id="T1253" time="767.155" type="appl" />
         <tli id="T1254" time="767.68" type="appl" />
         <tli id="T1255" time="769.12" type="appl" />
         <tli id="T1256" time="769.5971428571429" type="appl" />
         <tli id="T1257" time="770.0742857142857" type="appl" />
         <tli id="T1258" time="770.5514285714286" type="appl" />
         <tli id="T1259" time="771.0285714285715" type="appl" />
         <tli id="T1260" time="771.5057142857144" type="appl" />
         <tli id="T1261" time="771.9828571428571" type="appl" />
         <tli id="T1262" time="772.935076724141" />
         <tli id="T1263" time="774.47" type="appl" />
         <tli id="T1264" time="775.1316666666667" type="appl" />
         <tli id="T1265" time="775.7933333333334" type="appl" />
         <tli id="T1266" time="776.455" type="appl" />
         <tli id="T1267" time="777.1166666666667" type="appl" />
         <tli id="T1268" time="777.7783333333334" type="appl" />
         <tli id="T1269" time="778.44" type="appl" />
         <tli id="T1270" time="778.46" type="appl" />
         <tli id="T1271" time="779.0175" type="appl" />
         <tli id="T1272" time="779.575" type="appl" />
         <tli id="T1273" time="780.1325" type="appl" />
         <tli id="T1274" time="780.69" type="appl" />
         <tli id="T1275" time="781.2475" type="appl" />
         <tli id="T1276" time="781.805" type="appl" />
         <tli id="T1277" time="782.3625" type="appl" />
         <tli id="T1278" time="782.92" type="appl" />
         <tli id="T1279" time="783.42" type="appl" />
         <tli id="T1280" time="783.9019999999999" type="appl" />
         <tli id="T1281" time="784.384" type="appl" />
         <tli id="T1282" time="784.866" type="appl" />
         <tli id="T1283" time="785.3480000000001" type="appl" />
         <tli id="T1284" time="785.83" type="appl" />
         <tli id="T1285" time="786.14" type="appl" />
         <tli id="T1286" time="786.7674999999999" type="appl" />
         <tli id="T1287" time="787.395" type="appl" />
         <tli id="T1288" time="788.0225" type="appl" />
         <tli id="T1289" time="788.65" type="appl" />
         <tli id="T1290" time="789.7" type="appl" />
         <tli id="T1291" time="790.662" type="appl" />
         <tli id="T1292" time="791.624" type="appl" />
         <tli id="T1293" time="792.586" type="appl" />
         <tli id="T1294" time="793.548" type="appl" />
         <tli id="T1295" time="794.6016053840063" />
         <tli id="T1296" time="795.4" type="appl" />
         <tli id="T1297" time="795.9739999999999" type="appl" />
         <tli id="T1298" time="796.548" type="appl" />
         <tli id="T1299" time="797.122" type="appl" />
         <tli id="T1300" time="797.696" type="appl" />
         <tli id="T1301" time="798.27" type="appl" />
         <tli id="T1302" time="798.8439999999999" type="appl" />
         <tli id="T1303" time="799.418" type="appl" />
         <tli id="T1304" time="799.992" type="appl" />
         <tli id="T1305" time="800.566" type="appl" />
         <tli id="T1306" time="801.14" type="appl" />
         <tli id="T1307" time="801.2148965934176" />
         <tli id="T1308" time="801.7681666666667" type="appl" />
         <tli id="T1309" time="802.2163333333334" type="appl" />
         <tli id="T1310" time="802.6645000000001" type="appl" />
         <tli id="T1311" time="803.1126666666667" type="appl" />
         <tli id="T1312" time="803.5608333333333" type="appl" />
         <tli id="T1313" time="804.2548772300019" />
         <tli id="T1314" time="804.69" type="appl" />
         <tli id="T1315" time="805.12" type="appl" />
         <tli id="T1316" time="805.55" type="appl" />
         <tli id="T1317" time="805.98" type="appl" />
         <tli id="T1318" time="806.3900198800287" />
         <tli id="T1319" time="806.90075" type="appl" />
         <tli id="T1320" time="807.3915" type="appl" />
         <tli id="T1321" time="807.88225" type="appl" />
         <tli id="T1322" time="808.373" type="appl" />
         <tli id="T1323" time="808.86375" type="appl" />
         <tli id="T1324" time="809.3545" type="appl" />
         <tli id="T1325" time="809.84525" type="appl" />
         <tli id="T1326" time="810.4281712420128" />
         <tli id="T1327" time="810.63" type="appl" />
         <tli id="T1328" time="811.215" type="appl" />
         <tli id="T1329" time="811.8" type="appl" />
         <tli id="T1330" time="812.385" type="appl" />
         <tli id="T1331" time="812.97" type="appl" />
         <tli id="T1332" time="813.1" type="appl" />
         <tli id="T1333" time="813.64" type="appl" />
         <tli id="T1334" time="814.1800000000001" type="appl" />
         <tli id="T1335" time="814.72" type="appl" />
         <tli id="T1336" time="814.96" type="appl" />
         <tli id="T1337" time="815.496" type="appl" />
         <tli id="T1338" time="816.032" type="appl" />
         <tli id="T1339" time="816.568" type="appl" />
         <tli id="T1340" time="817.104" type="appl" />
         <tli id="T1341" time="817.64" type="appl" />
         <tli id="T1342" time="818.08" type="appl" />
         <tli id="T1343" time="818.5" type="appl" />
         <tli id="T1344" time="818.92" type="appl" />
         <tli id="T1345" time="819.34" type="appl" />
         <tli id="T1346" time="819.76" type="appl" />
         <tli id="T1347" time="820.18" type="appl" />
         <tli id="T1348" time="820.2" type="appl" />
         <tli id="T1349" time="820.6675" type="appl" />
         <tli id="T1350" time="821.135" type="appl" />
         <tli id="T1351" time="821.6025000000001" type="appl" />
         <tli id="T1352" time="822.07" type="appl" />
         <tli id="T1353" time="822.5375" type="appl" />
         <tli id="T1354" time="823.005" type="appl" />
         <tli id="T1355" time="823.4725" type="appl" />
         <tli id="T1356" time="823.94" type="appl" />
         <tli id="T1357" time="824.4075" type="appl" />
         <tli id="T1358" time="824.875" type="appl" />
         <tli id="T1359" time="825.3425" type="appl" />
         <tli id="T1360" time="825.81" type="appl" />
         <tli id="T1361" time="826.2774999999999" type="appl" />
         <tli id="T1362" time="826.745" type="appl" />
         <tli id="T1363" time="827.2125" type="appl" />
         <tli id="T1364" time="827.68" type="appl" />
         <tli id="T1365" time="827.91" type="appl" />
         <tli id="T1366" time="828.4119999999999" type="appl" />
         <tli id="T1367" time="828.914" type="appl" />
         <tli id="T1368" time="829.4159999999999" type="appl" />
         <tli id="T1369" time="829.918" type="appl" />
         <tli id="T1370" time="830.42" type="appl" />
         <tli id="T1371" time="830.97" type="appl" />
         <tli id="T1372" time="831.601625" type="appl" />
         <tli id="T1373" time="832.23325" type="appl" />
         <tli id="T1374" time="832.864875" type="appl" />
         <tli id="T1375" time="833.4965" type="appl" />
         <tli id="T1376" time="834.1281250000001" type="appl" />
         <tli id="T1377" time="834.75975" type="appl" />
         <tli id="T1378" time="835.391375" type="appl" />
         <tli id="T1379" time="836.023" type="appl" />
         <tli id="T1380" time="836.563" type="appl" />
         <tli id="T1381" time="837.51" type="appl" />
         <tli id="T1382" time="838.429" type="appl" />
         <tli id="T1383" time="838.633" type="appl" />
         <tli id="T1384" time="839.1525714285715" type="appl" />
         <tli id="T1385" time="839.6721428571428" type="appl" />
         <tli id="T1386" time="840.1917142857143" type="appl" />
         <tli id="T1387" time="840.7112857142857" type="appl" />
         <tli id="T1388" time="841.2308571428572" type="appl" />
         <tli id="T1389" time="841.7504285714285" type="appl" />
         <tli id="T1390" time="842.27" type="appl" />
         <tli id="T1391" time="843.39" type="appl" />
         <tli id="T1392" time="844.24" type="appl" />
         <tli id="T1393" time="845.09" type="appl" />
         <tli id="T1394" time="845.9399999999999" type="appl" />
         <tli id="T1395" time="846.79" type="appl" />
         <tli id="T1524" time="847.1542857142857" type="intp" />
         <tli id="T1396" time="847.64" type="appl" />
         <tli id="T1397" time="848.0" type="appl" />
         <tli id="T1398" time="848.502" type="appl" />
         <tli id="T1399" time="849.004" type="appl" />
         <tli id="T1400" time="849.506" type="appl" />
         <tli id="T1401" time="850.008" type="appl" />
         <tli id="T1402" time="850.51" type="appl" />
         <tli id="T1403" time="850.53" type="appl" />
         <tli id="T1404" time="851.2814285714286" type="appl" />
         <tli id="T1405" time="852.0328571428571" type="appl" />
         <tli id="T1406" time="852.7842857142857" type="appl" />
         <tli id="T1407" time="853.5357142857142" type="appl" />
         <tli id="T1408" time="854.2871428571428" type="appl" />
         <tli id="T1409" time="855.0385714285713" type="appl" />
         <tli id="T1410" time="855.79" type="appl" />
         <tli id="T1411" time="856.46" type="appl" />
         <tli id="T1412" time="857.1966666666667" type="appl" />
         <tli id="T1413" time="857.9333333333333" type="appl" />
         <tli id="T1414" time="858.8545294528628" />
         <tli id="T1415" time="859.28" type="appl" />
         <tli id="T1416" time="860.34" type="appl" />
         <tli id="T1417" time="861.4" type="appl" />
         <tli id="T1418" time="862.46" type="appl" />
         <tli id="T1419" time="863.52" type="appl" />
         <tli id="T1420" time="864.58" type="appl" />
         <tli id="T1421" time="865.64" type="appl" />
         <tli id="T1422" time="866.7" type="appl" />
         <tli id="T1423" time="866.75" type="appl" />
         <tli id="T1424" time="867.545" type="appl" />
         <tli id="T1425" time="868.34" type="appl" />
         <tli id="T1426" time="869.135" type="appl" />
         <tli id="T1427" time="869.9300000000001" type="appl" />
         <tli id="T1428" time="870.725" type="appl" />
         <tli id="T1429" time="871.52" type="appl" />
         <tli id="T1430" time="872.315" type="appl" />
         <tli id="T1431" time="873.11" type="appl" />
         <tli id="T1432" time="873.9050000000001" type="appl" />
         <tli id="T1433" time="874.7" type="appl" />
         <tli id="T1434" time="875.44" type="appl" />
         <tli id="T1435" time="876.0350000000001" type="appl" />
         <tli id="T1436" time="877.0344136545406" />
         <tli id="T1437" time="877.76" type="appl" />
         <tli id="T1438" time="878.506" type="appl" />
         <tli id="T1439" time="879.252" type="appl" />
         <tli id="T1440" time="879.998" type="appl" />
         <tli id="T1441" time="880.744" type="appl" />
         <tli id="T1442" time="881.49" type="appl" />
         <tli id="T1443" time="881.72" type="appl" />
         <tli id="T1444" time="882.596" type="appl" />
         <tli id="T1445" time="883.472" type="appl" />
         <tli id="T1446" time="884.3480000000001" type="appl" />
         <tli id="T1447" time="885.224" type="appl" />
         <tli id="T1448" time="886.1" type="appl" />
         <tli id="T1449" time="887.47" type="appl" />
         <tli id="T1450" time="888.4633333333334" type="appl" />
         <tli id="T1451" time="889.4566666666667" type="appl" />
         <tli id="T1452" time="890.45" type="appl" />
         <tli id="T1453" time="893.45" type="appl" />
         <tli id="T1454" time="894.42025" type="appl" />
         <tli id="T1455" time="895.3905" type="appl" />
         <tli id="T1456" time="896.36075" type="appl" />
         <tli id="T1457" time="897.331" type="appl" />
         <tli id="T1458" time="897.53" type="appl" />
         <tli id="T1459" time="898.2166666666667" type="appl" />
         <tli id="T1460" time="898.9033333333333" type="appl" />
         <tli id="T1461" time="899.59" type="appl" />
         <tli id="T1462" time="900.214266008495" />
         <tli id="T1463" time="902.2" type="appl" />
         <tli id="T1464" time="903.95" type="appl" />
         <tli id="T1465" time="905.7" type="appl" />
         <tli id="T1466" time="905.99" type="appl" />
         <tli id="T1467" time="906.7652" type="appl" />
         <tli id="T1468" time="907.5404" type="appl" />
         <tli id="T1469" time="908.3156" type="appl" />
         <tli id="T1470" time="909.0908" type="appl" />
         <tli id="T1471" time="909.9608705933331" />
         <tli id="T1472" time="912.72" type="appl" />
         <tli id="T1473" time="913.6833333333334" type="appl" />
         <tli id="T1474" time="914.6466666666666" type="appl" />
         <tli id="T1475" time="915.61" type="appl" />
         <tli id="T1476" time="916.32" type="appl" />
         <tli id="T1477" time="916.865" type="appl" />
         <tli id="T1478" time="917.4100000000001" type="appl" />
         <tli id="T1479" time="917.955" type="appl" />
         <tli id="T1480" time="918.5" type="appl" />
         <tli id="T1481" time="919.045" type="appl" />
         <tli id="T1482" time="919.5899999999999" type="appl" />
         <tli id="T1483" time="920.135" type="appl" />
         <tli id="T1484" time="920.68" type="appl" />
         <tli id="T1485" time="920.8" type="appl" />
         <tli id="T1486" time="921.4033333333333" type="appl" />
         <tli id="T1487" time="922.0066666666667" type="appl" />
         <tli id="T1488" time="922.61" type="appl" />
         <tli id="T1489" time="922.7" type="appl" />
         <tli id="T1490" time="923.34" type="appl" />
         <tli id="T1491" time="923.98" type="appl" />
         <tli id="T1492" time="924.5207778527626" />
         <tli id="T1493" time="925.52" type="appl" />
         <tli id="T1494" time="926.1733333333333" type="appl" />
         <tli id="T1495" time="926.8266666666667" type="appl" />
         <tli id="T1496" time="927.48" type="appl" />
         <tli id="T1497" time="928.1320000000001" type="appl" />
         <tli id="T1498" time="928.784" type="appl" />
         <tli id="T1499" time="929.436" type="appl" />
         <tli id="T1500" time="930.088" type="appl" />
         <tli id="T1501" time="930.74" type="appl" />
         <tli id="T1502" time="931.25" type="appl" />
         <tli id="T1503" time="931.76" type="appl" />
         <tli id="T1504" time="932.27" type="appl" />
         <tli id="T1505" time="932.78" type="appl" />
         <tli id="T1506" time="933.29" type="appl" />
         <tli id="T1507" time="933.8" type="appl" />
         <tli id="T1508" time="934.31" type="appl" />
         <tli id="T1509" time="936.21" type="appl" />
         <tli id="T1510" time="936.5880000000001" type="appl" />
         <tli id="T1511" time="936.966" type="appl" />
         <tli id="T1512" time="937.344" type="appl" />
         <tli id="T1513" time="937.722" type="appl" />
         <tli id="T1514" time="938.1" type="appl" />
         <tli id="T1515" time="938.13" type="appl" />
         <tli id="T1516" time="939.8" type="appl" />
         <tli id="T1517" time="940.3025" type="appl" />
         <tli id="T1518" time="940.62" type="appl" />
         <tli id="T1519" time="940.805" type="appl" />
         <tli id="T1520" time="941.3074999999999" type="appl" />
         <tli id="T1521" time="941.8940005258731" />
         <tli id="T0" time="945.247" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T106" start="T104">
            <tli id="T104.tx-PKZ.1" />
            <tli id="T104.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T268" start="T265">
            <tli id="T265.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T408" start="T394">
            <tli id="T394.tx-PKZ.1" />
            <tli id="T394.tx-PKZ.2" />
            <tli id="T394.tx-PKZ.3" />
            <tli id="T394.tx-PKZ.4" />
            <tli id="T394.tx-PKZ.5" />
            <tli id="T394.tx-PKZ.6" />
            <tli id="T394.tx-PKZ.7" />
            <tli id="T394.tx-PKZ.8" />
            <tli id="T394.tx-PKZ.9" />
            <tli id="T394.tx-PKZ.10" />
            <tli id="T394.tx-PKZ.11" />
            <tli id="T394.tx-PKZ.12" />
            <tli id="T394.tx-PKZ.13" />
            <tli id="T394.tx-PKZ.14" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T9" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">А</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">я</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">уж</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">и</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">забыла</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">чего</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">говорила</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_25" n="HIAT:ip">(</nts>
                  <nts id="Seg_26" n="HIAT:ip">(</nts>
                  <ats e="T9" id="Seg_27" n="HIAT:non-pho" s="T8">…</ats>
                  <nts id="Seg_28" n="HIAT:ip">)</nts>
                  <nts id="Seg_29" n="HIAT:ip">)</nts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_32" n="sc" s="T17">
               <ts e="T30" id="Seg_34" n="HIAT:u" s="T17">
                  <ts e="T19" id="Seg_36" n="HIAT:w" s="T17">Вот</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_39" n="HIAT:w" s="T19">я</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_42" n="HIAT:w" s="T23">выпустила</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_46" n="HIAT:w" s="T26">сейчас</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_49" n="HIAT:w" s="T27">переговорила</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_52" n="HIAT:w" s="T28">и</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_55" n="HIAT:w" s="T29">забыла</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T51" id="Seg_58" n="sc" s="T44">
               <ts e="T51" id="Seg_60" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_62" n="HIAT:w" s="T44">А</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_66" n="HIAT:w" s="T45">вот</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_69" n="HIAT:w" s="T46">чего</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_72" n="HIAT:w" s="T47">я</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_75" n="HIAT:w" s="T49">говорила</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_78" n="sc" s="T52">
               <ts e="T59" id="Seg_80" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_82" n="HIAT:w" s="T52">Когда</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_85" n="HIAT:w" s="T53">я</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_88" n="HIAT:w" s="T54">собралась</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_91" n="HIAT:w" s="T55">сюды</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_94" n="HIAT:w" s="T57">ехать</ts>
                  <nts id="Seg_95" n="HIAT:ip">…</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_97" n="sc" s="T60">
               <ts e="T61" id="Seg_99" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_101" n="HIAT:w" s="T60">Пусти</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T80" id="Seg_104" n="sc" s="T62">
               <ts e="T75" id="Seg_106" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_108" n="HIAT:w" s="T62">Когда</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_111" n="HIAT:w" s="T63">я</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_114" n="HIAT:w" s="T64">собралась</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_117" n="HIAT:w" s="T65">сюды</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_120" n="HIAT:w" s="T66">ехать</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_124" n="HIAT:w" s="T67">там</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_127" n="HIAT:w" s="T68">люди</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_130" n="HIAT:w" s="T69">говорили:</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_132" n="HIAT:ip">"</nts>
                  <ts e="T71" id="Seg_134" n="HIAT:w" s="T70">Не</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_137" n="HIAT:w" s="T71">езди</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_141" n="HIAT:w" s="T72">ты</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_144" n="HIAT:w" s="T73">помрешь</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_146" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_148" n="HIAT:w" s="T74">ведь</ts>
                  <nts id="Seg_149" n="HIAT:ip">)</nts>
                  <nts id="Seg_150" n="HIAT:ip">"</nts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_154" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_156" n="HIAT:w" s="T75">А</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_159" n="HIAT:w" s="T76">я</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_162" n="HIAT:w" s="T77">сказала:</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T79" id="Seg_166" n="HIAT:w" s="T78">Все</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_169" n="HIAT:w" s="T79">равно</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_172" n="sc" s="T81">
               <ts e="T84" id="Seg_174" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_176" n="HIAT:w" s="T81">И</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_179" n="HIAT:w" s="T82">там</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_182" n="HIAT:w" s="T83">похоронят</ts>
                  <nts id="Seg_183" n="HIAT:ip">"</nts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T88" id="Seg_186" n="sc" s="T85">
               <ts e="T88" id="Seg_188" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_190" n="HIAT:w" s="T85">Все</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_193" n="HIAT:w" s="T86">равно</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_196" n="HIAT:w" s="T87">земля</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T94" id="Seg_199" n="sc" s="T89">
               <ts e="T94" id="Seg_201" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_203" n="HIAT:w" s="T89">Вот</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_206" n="HIAT:w" s="T90">это</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_209" n="HIAT:w" s="T91">вот</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_212" n="HIAT:w" s="T92">я</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_215" n="HIAT:w" s="T93">говорила</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T106" id="Seg_218" n="sc" s="T104">
               <ts e="T106" id="Seg_220" n="HIAT:u" s="T104">
                  <ts e="T104.tx-PKZ.1" id="Seg_222" n="HIAT:w" s="T104">Ну</ts>
                  <nts id="Seg_223" n="HIAT:ip">,</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104.tx-PKZ.2" id="Seg_226" n="HIAT:w" s="T104.tx-PKZ.1">еще</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_229" n="HIAT:w" s="T104.tx-PKZ.2">это</ts>
                  <nts id="Seg_230" n="HIAT:ip">…</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_232" n="sc" s="T108">
               <ts e="T118" id="Seg_234" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_236" n="HIAT:w" s="T108">Про</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_239" n="HIAT:w" s="T109">это</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_242" n="HIAT:w" s="T110">говорить</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_246" n="HIAT:w" s="T111">что</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_249" n="HIAT:w" s="T112">я</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_252" n="HIAT:w" s="T113">набрала</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_255" n="HIAT:w" s="T114">гостинцев</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_258" n="HIAT:w" s="T115">еще</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_261" n="HIAT:w" s="T116">туды</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_264" n="HIAT:w" s="T117">своим</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_267" n="sc" s="T126">
               <ts e="T127" id="Seg_269" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_271" n="HIAT:w" s="T126">Пускайте</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_274" n="sc" s="T128">
               <ts e="T137" id="Seg_276" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_278" n="HIAT:w" s="T128">Kamen</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_280" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_282" n="HIAT:w" s="T129">măn</ts>
                  <nts id="Seg_283" n="HIAT:ip">)</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_286" n="HIAT:w" s="T130">amnobiam</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_289" n="HIAT:w" s="T131">Tartu</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_293" n="HIAT:w" s="T132">kambiam</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_297" n="HIAT:w" s="T133">ibiem</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_299" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_301" n="HIAT:w" s="T134">na-</ts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_305" n="HIAT:w" s="T135">nagur</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_308" n="HIAT:w" s="T136">platokəʔjə</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_311" n="sc" s="T138">
               <ts e="T141" id="Seg_313" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_315" n="HIAT:w" s="T138">Bostə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_318" n="HIAT:w" s="T139">ildə</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_321" n="HIAT:w" s="T140">mĭzittə</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_324" n="sc" s="T142">
               <ts e="T147" id="Seg_326" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_328" n="HIAT:w" s="T142">Dĭgəttə</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_331" n="HIAT:w" s="T143">döbər</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_334" n="HIAT:w" s="T144">šobiam</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_338" n="HIAT:w" s="T145">ibiem</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_341" n="HIAT:w" s="T146">piʔmeʔi</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_344" n="sc" s="T148">
               <ts e="T150" id="Seg_346" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_348" n="HIAT:w" s="T148">Kujnek</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_351" n="HIAT:w" s="T149">ibiem</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_354" n="sc" s="T151">
               <ts e="T153" id="Seg_356" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_358" n="HIAT:w" s="T151">Nʼinə</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_362" n="HIAT:w" s="T152">plʼemʼannikamnə</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_365" n="sc" s="T154">
               <ts e="T156" id="Seg_367" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_369" n="HIAT:w" s="T154">Koʔbdonə</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_372" n="HIAT:w" s="T155">ibiem</ts>
                  <nts id="Seg_373" n="HIAT:ip">…</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T159" id="Seg_375" n="sc" s="T157">
               <ts e="T159" id="Seg_377" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_379" n="HIAT:w" s="T157">Platʼtʼa</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_382" n="HIAT:w" s="T158">ibiem</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_385" n="sc" s="T160">
               <ts e="T164" id="Seg_387" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_389" n="HIAT:w" s="T160">Dĭgəttə</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_392" n="HIAT:w" s="T161">plʼemʼanniktə</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_395" n="HIAT:w" s="T162">ibiem</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_398" n="HIAT:w" s="T163">kujnek</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_401" n="sc" s="T165">
               <ts e="T166" id="Seg_403" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_405" n="HIAT:w" s="T165">Kabarləj</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T185" id="Seg_408" n="sc" s="T173">
               <ts e="T185" id="Seg_410" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_412" n="HIAT:w" s="T173">Когда</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_415" n="HIAT:w" s="T174">я</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_418" n="HIAT:w" s="T175">сюды</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_421" n="HIAT:w" s="T176">приехала</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_424" n="HIAT:w" s="T177">в</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_427" n="HIAT:w" s="T178">Тарту</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_431" n="HIAT:w" s="T179">пошла</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_434" n="HIAT:w" s="T180">в</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_437" n="HIAT:w" s="T181">магазин</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_440" n="HIAT:w" s="T182">взяла</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_443" n="HIAT:w" s="T183">три</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_446" n="HIAT:w" s="T184">платка</ts>
                  <nts id="Seg_447" n="HIAT:ip">.</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T193" id="Seg_449" n="sc" s="T186">
               <ts e="T193" id="Seg_451" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_453" n="HIAT:w" s="T186">Сюды</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_456" n="HIAT:w" s="T187">приехала</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_459" n="HIAT:w" s="T188">вот</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_463" n="HIAT:w" s="T189">взяла</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_466" n="HIAT:w" s="T190">штаны</ts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_470" n="HIAT:w" s="T191">рубашку</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_473" n="HIAT:w" s="T192">внуку</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T198" id="Seg_476" n="sc" s="T194">
               <ts e="T198" id="Seg_478" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_480" n="HIAT:w" s="T194">Потом</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_483" n="HIAT:w" s="T195">взяла</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_486" n="HIAT:w" s="T196">рубашку</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_489" n="HIAT:w" s="T197">племяннику</ts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T210" id="Seg_492" n="sc" s="T199">
               <ts e="T210" id="Seg_494" n="HIAT:u" s="T199">
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <nts id="Seg_496" n="HIAT:ip">(</nts>
                  <ats e="T200" id="Seg_497" n="HIAT:non-pho" s="T199">BRK</ats>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip">)</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_502" n="HIAT:w" s="T200">Забыла</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_505" n="HIAT:w" s="T201">еще</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_508" n="HIAT:w" s="T202">вот</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_511" n="HIAT:w" s="T203">один</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_514" n="HIAT:w" s="T204">платок</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_517" n="HIAT:w" s="T205">взяла</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_520" n="HIAT:w" s="T206">в</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_523" n="HIAT:w" s="T208">Красноярский</ts>
                  <nts id="Seg_524" n="HIAT:ip">.</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_526" n="sc" s="T219">
               <ts e="T221" id="Seg_528" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_530" n="HIAT:w" s="T219">Нет</ts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_534" n="HIAT:w" s="T220">это</ts>
                  <nts id="Seg_535" n="HIAT:ip">…</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_538" n="HIAT:u" s="T221">
                  <ts e="T224" id="Seg_540" n="HIAT:w" s="T221">Это</ts>
                  <nts id="Seg_541" n="HIAT:ip">…</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T230" id="Seg_543" n="sc" s="T226">
               <ts e="T230" id="Seg_545" n="HIAT:u" s="T226">
                  <ts e="T228" id="Seg_547" n="HIAT:w" s="T226">Штаны</ts>
                  <nts id="Seg_548" n="HIAT:ip">,</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_551" n="HIAT:w" s="T228">piʔmeʔi</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T237" id="Seg_554" n="sc" s="T233">
               <ts e="T237" id="Seg_556" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_558" n="HIAT:w" s="T233">Piʔme</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_560" n="HIAT:ip">—</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_563" n="HIAT:w" s="T234">штаны</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T249" id="Seg_566" n="sc" s="T239">
               <ts e="T249" id="Seg_568" n="HIAT:u" s="T239">
                  <ts e="T242" id="Seg_570" n="HIAT:w" s="T239">А</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_573" n="HIAT:ip">(</nts>
                  <ts e="T244" id="Seg_575" n="HIAT:w" s="T242">а</ts>
                  <nts id="Seg_576" n="HIAT:ip">)</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_579" n="HIAT:w" s="T244">эти</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_582" n="HIAT:w" s="T247">piʔme</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_585" n="sc" s="T250">
               <ts e="T253" id="Seg_587" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_589" n="HIAT:w" s="T250">Jamaʔi</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_591" n="HIAT:ip">—</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_594" n="HIAT:w" s="T251">piʔme</ts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_598" n="HIAT:w" s="T252">jaʔma</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T260" id="Seg_601" n="sc" s="T257">
               <ts e="T260" id="Seg_603" n="HIAT:u" s="T257">
                  <ts e="T260" id="Seg_605" n="HIAT:w" s="T257">Вот</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_608" n="sc" s="T261">
               <ts e="T270" id="Seg_610" n="HIAT:u" s="T261">
                  <nts id="Seg_611" n="HIAT:ip">(</nts>
                  <ts e="T263" id="Seg_613" n="HIAT:w" s="T261">Это=</ts>
                  <nts id="Seg_614" n="HIAT:ip">)</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_617" n="HIAT:w" s="T263">Это</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265.tx-PKZ.1" id="Seg_620" n="HIAT:w" s="T265">обутки</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_623" n="HIAT:w" s="T265.tx-PKZ.1">-</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_626" n="HIAT:w" s="T268">jaʔma</ts>
                  <nts id="Seg_627" n="HIAT:ip">.</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T280" id="Seg_629" n="sc" s="T271">
               <ts e="T276" id="Seg_631" n="HIAT:u" s="T271">
                  <ts e="T273" id="Seg_633" n="HIAT:w" s="T271">А</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_636" n="HIAT:w" s="T273">piʔmeʔi</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_638" n="HIAT:ip">—</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_641" n="HIAT:w" s="T274">это</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_644" n="HIAT:w" s="T275">штаны</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_648" n="HIAT:u" s="T276">
                  <nts id="Seg_649" n="HIAT:ip">(</nts>
                  <ts e="T277" id="Seg_651" n="HIAT:w" s="T276">K-</ts>
                  <nts id="Seg_652" n="HIAT:ip">)</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_655" n="HIAT:w" s="T277">Kujnek</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_657" n="HIAT:ip">—</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_660" n="HIAT:w" s="T278">рубаха</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T301" id="Seg_663" n="sc" s="T284">
               <ts e="T286" id="Seg_665" n="HIAT:u" s="T284">
                  <ts e="T286" id="Seg_667" n="HIAT:w" s="T284">Ну</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_671" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_673" n="HIAT:w" s="T286">А</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_676" n="HIAT:w" s="T287">вот</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_679" n="HIAT:w" s="T288">платье</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_682" n="HIAT:w" s="T289">не</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_685" n="HIAT:w" s="T290">знаю</ts>
                  <nts id="Seg_686" n="HIAT:ip">,</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_689" n="HIAT:w" s="T291">вот</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_691" n="HIAT:ip">(</nts>
                  <ts e="T294" id="Seg_693" n="HIAT:w" s="T292">этой=</ts>
                  <nts id="Seg_694" n="HIAT:ip">)</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_697" n="HIAT:w" s="T294">Наде</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_700" n="HIAT:w" s="T296">взяла</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_703" n="HIAT:w" s="T299">платье</ts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T312" id="Seg_706" n="sc" s="T307">
               <ts e="T312" id="Seg_708" n="HIAT:u" s="T307">
                  <ts e="T310" id="Seg_710" n="HIAT:w" s="T307">Не</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_713" n="HIAT:w" s="T310">знаю</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T344" id="Seg_716" n="sc" s="T313">
               <ts e="T344" id="Seg_718" n="HIAT:u" s="T313">
                  <ts e="T315" id="Seg_720" n="HIAT:w" s="T313">Тады</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_723" n="HIAT:w" s="T315">не</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_726" n="HIAT:w" s="T317">носили</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_730" n="HIAT:w" s="T319">они</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_733" n="HIAT:w" s="T321">все</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_736" n="HIAT:w" s="T323">в</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_739" n="HIAT:w" s="T325">штанах</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_742" n="HIAT:w" s="T327">ходили</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_745" n="HIAT:w" s="T329">так</ts>
                  <nts id="Seg_746" n="HIAT:ip">,</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_749" n="HIAT:w" s="T331">как</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_752" n="HIAT:w" s="T333">вот</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_755" n="HIAT:w" s="T335">теперь</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_758" n="HIAT:w" s="T337">все</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_761" n="HIAT:w" s="T339">ходят</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_764" n="HIAT:w" s="T341">в</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_767" n="HIAT:w" s="T342">штанах</ts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T348" id="Seg_770" n="sc" s="T347">
               <ts e="T348" id="Seg_772" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_774" n="HIAT:w" s="T347">Kujnek</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_777" n="sc" s="T349">
               <ts e="T353" id="Seg_779" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_781" n="HIAT:w" s="T349">А</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_783" n="HIAT:ip">(</nts>
                  <ts e="T351" id="Seg_785" n="HIAT:w" s="T350">с-</ts>
                  <nts id="Seg_786" n="HIAT:ip">)</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_789" n="HIAT:w" s="T351">шапка</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_791" n="HIAT:ip">—</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_794" n="HIAT:w" s="T352">üžü</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T364" id="Seg_797" n="sc" s="T359">
               <ts e="T364" id="Seg_799" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_801" n="HIAT:w" s="T359">Не</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_804" n="HIAT:w" s="T360">знаю</ts>
                  <nts id="Seg_805" n="HIAT:ip">,</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_808" n="HIAT:w" s="T362">что</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_811" n="HIAT:w" s="T363">сказать</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_814" n="sc" s="T389">
               <ts e="T410" id="Seg_816" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_818" n="HIAT:w" s="T389">Можно</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_821" n="HIAT:w" s="T390">это</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_824" n="HIAT:w" s="T392">рассказать</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_827" n="HIAT:ip">(</nts>
                  <ts e="T394.tx-PKZ.1" id="Seg_829" n="HIAT:w" s="T394">как=</ts>
                  <nts id="Seg_830" n="HIAT:ip">)</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_832" n="HIAT:ip">(</nts>
                  <nts id="Seg_833" n="HIAT:ip">(</nts>
                  <ats e="T394.tx-PKZ.2"
                       id="Seg_834"
                       n="HIAT:non-pho"
                       s="T394.tx-PKZ.1">LAUGH</ats>
                  <nts id="Seg_835" n="HIAT:ip">)</nts>
                  <nts id="Seg_836" n="HIAT:ip">)</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.3" id="Seg_839" n="HIAT:w" s="T394.tx-PKZ.2">как</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.4" id="Seg_842" n="HIAT:w" s="T394.tx-PKZ.3">Александр</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.5" id="Seg_845" n="HIAT:w" s="T394.tx-PKZ.4">Константинович</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.6" id="Seg_848" n="HIAT:w" s="T394.tx-PKZ.5">шел</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.7" id="Seg_851" n="HIAT:w" s="T394.tx-PKZ.6">ко</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.8" id="Seg_854" n="HIAT:w" s="T394.tx-PKZ.7">мне</ts>
                  <nts id="Seg_855" n="HIAT:ip">,</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.9" id="Seg_858" n="HIAT:w" s="T394.tx-PKZ.8">я</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.10" id="Seg_861" n="HIAT:w" s="T394.tx-PKZ.9">часто</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.11" id="Seg_864" n="HIAT:w" s="T394.tx-PKZ.10">смеюсь</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.12" id="Seg_867" n="HIAT:w" s="T394.tx-PKZ.11">над</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.13" id="Seg_870" n="HIAT:w" s="T394.tx-PKZ.12">этим</ts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394.tx-PKZ.14" id="Seg_874" n="HIAT:w" s="T394.tx-PKZ.13">даже</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_877" n="HIAT:w" s="T394.tx-PKZ.14">сегодня</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_880" n="HIAT:w" s="T408">смеялась</ts>
                  <nts id="Seg_881" n="HIAT:ip">?</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_883" n="sc" s="T411">
               <ts e="T424" id="Seg_885" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_887" n="HIAT:w" s="T411">Он</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_890" n="HIAT:w" s="T412">идет:</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_892" n="HIAT:ip">"</nts>
                  <ts e="T414" id="Seg_894" n="HIAT:w" s="T413">Бабушка</ts>
                  <nts id="Seg_895" n="HIAT:ip">,</nts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_898" n="HIAT:w" s="T414">я</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_901" n="HIAT:w" s="T415">иду</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_904" n="HIAT:w" s="T416">к</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_907" n="HIAT:w" s="T417">тебе</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_910" n="HIAT:w" s="T418">разговаривать</ts>
                  <nts id="Seg_911" n="HIAT:ip">"</nts>
                  <nts id="Seg_912" n="HIAT:ip">,</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_915" n="HIAT:w" s="T419">а</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_918" n="HIAT:w" s="T420">я</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_921" n="HIAT:w" s="T421">его</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_924" n="HIAT:w" s="T422">по-своему</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_927" n="HIAT:w" s="T423">ругаю</ts>
                  <nts id="Seg_928" n="HIAT:ip">.</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_930" n="sc" s="T428">
               <ts e="T438" id="Seg_932" n="HIAT:u" s="T428">
                  <ts e="T430" id="Seg_934" n="HIAT:w" s="T428">Слухай</ts>
                  <nts id="Seg_935" n="HIAT:ip">,</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_938" n="HIAT:w" s="T430">слухай</ts>
                  <nts id="Seg_939" n="HIAT:ip">,</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_942" n="HIAT:w" s="T432">сперва</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_945" n="HIAT:w" s="T433">расскажу</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_949" n="HIAT:w" s="T435">как</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_952" n="HIAT:w" s="T436">было</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_955" n="HIAT:w" s="T437">дело</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_959" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_961" n="HIAT:w" s="T438">А</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_964" n="HIAT:w" s="T439">потом</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_967" n="HIAT:w" s="T440">опять</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_970" n="HIAT:w" s="T441">расскажу</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_974" n="HIAT:w" s="T442">ты</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_977" n="HIAT:w" s="T443">может</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_980" n="HIAT:w" s="T444">лучше</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_983" n="HIAT:w" s="T445">запомнишь</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T455" id="Seg_986" n="sc" s="T447">
               <ts e="T455" id="Seg_988" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_990" n="HIAT:w" s="T447">Вот</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_993" n="HIAT:w" s="T448">он</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_996" n="HIAT:w" s="T449">идет:</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_998" n="HIAT:ip">"</nts>
                  <ts e="T451" id="Seg_1000" n="HIAT:w" s="T450">Бабушка</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1004" n="HIAT:w" s="T451">я</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1007" n="HIAT:w" s="T452">к</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1010" n="HIAT:w" s="T453">тебе</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1013" n="HIAT:w" s="T454">иду</ts>
                  <nts id="Seg_1014" n="HIAT:ip">!</nts>
                  <nts id="Seg_1015" n="HIAT:ip">"</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T472" id="Seg_1017" n="sc" s="T456">
               <ts e="T472" id="Seg_1019" n="HIAT:u" s="T456">
                  <nts id="Seg_1020" n="HIAT:ip">(</nts>
                  <ts e="T457" id="Seg_1022" n="HIAT:w" s="T456">Я</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1025" n="HIAT:w" s="T457">г-</ts>
                  <nts id="Seg_1026" n="HIAT:ip">)</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1029" n="HIAT:w" s="T458">А</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1032" n="HIAT:w" s="T459">я</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1035" n="HIAT:w" s="T460">ему</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1038" n="HIAT:w" s="T461">говорю:</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1040" n="HIAT:ip">"</nts>
                  <ts e="T463" id="Seg_1042" n="HIAT:w" s="T462">Ты</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1045" n="HIAT:w" s="T463">мне</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1048" n="HIAT:w" s="T464">надоел</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1052" n="HIAT:w" s="T465">убирайся</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1055" n="HIAT:w" s="T466">отседова</ts>
                  <nts id="Seg_1056" n="HIAT:ip">,</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1059" n="HIAT:w" s="T467">не</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1062" n="HIAT:w" s="T468">ходи</ts>
                  <nts id="Seg_1063" n="HIAT:ip">"</nts>
                  <nts id="Seg_1064" n="HIAT:ip">,</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1066" n="HIAT:ip">—</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1069" n="HIAT:w" s="T469">на</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1072" n="HIAT:w" s="T470">его</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1075" n="HIAT:w" s="T471">говорю</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T479" id="Seg_1078" n="sc" s="T473">
               <ts e="T479" id="Seg_1080" n="HIAT:u" s="T473">
                  <nts id="Seg_1081" n="HIAT:ip">"</nts>
                  <ts e="T474" id="Seg_1083" n="HIAT:w" s="T473">Боле</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1086" n="HIAT:w" s="T474">не</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1089" n="HIAT:w" s="T475">буду</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1092" n="HIAT:w" s="T476">с</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1095" n="HIAT:w" s="T477">тобой</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1098" n="HIAT:w" s="T478">разговаривать</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T482" id="Seg_1101" n="sc" s="T480">
               <ts e="T482" id="Seg_1103" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1105" n="HIAT:w" s="T480">Уходи</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1108" n="HIAT:w" s="T481">отседова</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1111" n="sc" s="T483">
               <ts e="T494" id="Seg_1113" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_1115" n="HIAT:w" s="T483">Я</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1118" n="HIAT:w" s="T484">тебе</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1121" n="HIAT:w" s="T485">ничего</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1124" n="HIAT:w" s="T486">не</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1127" n="HIAT:w" s="T487">скажу</ts>
                  <nts id="Seg_1128" n="HIAT:ip">"</nts>
                  <nts id="Seg_1129" n="HIAT:ip">,</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1131" n="HIAT:ip">—</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1134" n="HIAT:w" s="T488">вот</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1137" n="HIAT:w" s="T489">это</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1140" n="HIAT:w" s="T490">я</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1143" n="HIAT:w" s="T491">ему</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1146" n="HIAT:w" s="T492">так</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1149" n="HIAT:w" s="T493">говорила</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T502" id="Seg_1152" n="sc" s="T495">
               <ts e="T502" id="Seg_1154" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1156" n="HIAT:w" s="T495">А</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1159" n="HIAT:w" s="T496">он</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1162" n="HIAT:w" s="T497">идет</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1165" n="HIAT:w" s="T498">да</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1168" n="HIAT:w" s="T499">говорит:</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1170" n="HIAT:ip">"</nts>
                  <ts e="T501" id="Seg_1172" n="HIAT:w" s="T500">Ой</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1174" n="HIAT:ip">(</nts>
                  <nts id="Seg_1175" n="HIAT:ip">(</nts>
                  <ats e="T502" id="Seg_1176" n="HIAT:non-pho" s="T501">DMG</ats>
                  <nts id="Seg_1177" n="HIAT:ip">)</nts>
                  <nts id="Seg_1178" n="HIAT:ip">)</nts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T519" id="Seg_1181" n="sc" s="T503">
               <ts e="T514" id="Seg_1183" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1185" n="HIAT:w" s="T503">Kamen</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1188" n="HIAT:w" s="T504">dĭ</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1191" n="HIAT:w" s="T505">šobi</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1194" n="HIAT:w" s="T506">măna</ts>
                  <nts id="Seg_1195" n="HIAT:ip">,</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1198" n="HIAT:w" s="T507">Александр</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1201" n="HIAT:w" s="T508">Константинович</ts>
                  <nts id="Seg_1202" n="HIAT:ip">,</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1205" n="HIAT:w" s="T509">măndə:</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1207" n="HIAT:ip">"</nts>
                  <ts e="T511" id="Seg_1209" n="HIAT:w" s="T510">Urgaja</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1213" n="HIAT:w" s="T511">măn</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1216" n="HIAT:w" s="T512">tănan</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1219" n="HIAT:w" s="T513">šonəgam</ts>
                  <nts id="Seg_1220" n="HIAT:ip">!</nts>
                  <nts id="Seg_1221" n="HIAT:ip">"</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1224" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_1226" n="HIAT:w" s="T514">A</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1229" n="HIAT:w" s="T515">măn</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1232" n="HIAT:w" s="T516">măndəm:</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1234" n="HIAT:ip">"</nts>
                  <ts e="T518" id="Seg_1236" n="HIAT:w" s="T517">Iʔ</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1239" n="HIAT:w" s="T518">šoʔ</ts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T522" id="Seg_1242" n="sc" s="T520">
               <ts e="T522" id="Seg_1244" n="HIAT:u" s="T520">
                  <nts id="Seg_1245" n="HIAT:ip">(</nts>
                  <ts e="T521" id="Seg_1247" n="HIAT:w" s="T520">Tuʔ</ts>
                  <nts id="Seg_1248" n="HIAT:ip">)</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1251" n="HIAT:w" s="T521">măna</ts>
                  <nts id="Seg_1252" n="HIAT:ip">.</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T525" id="Seg_1254" n="sc" s="T523">
               <ts e="T525" id="Seg_1256" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_1258" n="HIAT:w" s="T523">Kanaʔ</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1261" n="HIAT:w" s="T524">döʔə</ts>
                  <nts id="Seg_1262" n="HIAT:ip">!</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T529" id="Seg_1264" n="sc" s="T526">
               <ts e="T529" id="Seg_1266" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_1268" n="HIAT:w" s="T526">Tăn</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1271" n="HIAT:w" s="T527">măna</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1273" n="HIAT:ip">(</nts>
                  <ts e="T529" id="Seg_1275" n="HIAT:w" s="T528">надое-</ts>
                  <nts id="Seg_1276" n="HIAT:ip">)</nts>
                  <nts id="Seg_1277" n="HIAT:ip">.</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T532" id="Seg_1279" n="sc" s="T530">
               <ts e="T532" id="Seg_1281" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1283" n="HIAT:w" s="T530">Tăn</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1286" n="HIAT:w" s="T531">măna</ts>
                  <nts id="Seg_1287" n="HIAT:ip">…</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T537" id="Seg_1289" n="sc" s="T533">
               <ts e="T537" id="Seg_1291" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_1293" n="HIAT:w" s="T533">Măn</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1296" n="HIAT:w" s="T534">tănzi</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1299" n="HIAT:w" s="T535">ej</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1302" n="HIAT:w" s="T536">dʼăbaktərlam</ts>
                  <nts id="Seg_1303" n="HIAT:ip">.</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T540" id="Seg_1305" n="sc" s="T538">
               <ts e="T540" id="Seg_1307" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1309" n="HIAT:w" s="T538">Kanaʔ</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1312" n="HIAT:w" s="T539">döʔə</ts>
                  <nts id="Seg_1313" n="HIAT:ip">"</nts>
                  <nts id="Seg_1314" n="HIAT:ip">.</nts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T548" id="Seg_1316" n="sc" s="T541">
               <ts e="T548" id="Seg_1318" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1320" n="HIAT:w" s="T541">По-русски</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1323" n="HIAT:w" s="T542">хорошо</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1326" n="HIAT:w" s="T543">рассказывала</ts>
                  <nts id="Seg_1327" n="HIAT:ip">,</nts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1330" n="HIAT:w" s="T544">опять</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1333" n="HIAT:w" s="T545">забыла</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T560" id="Seg_1336" n="sc" s="T555">
               <ts e="T560" id="Seg_1338" n="HIAT:u" s="T555">
                  <ts e="T556" id="Seg_1340" n="HIAT:w" s="T555">Kamen</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1343" n="HIAT:w" s="T556">dĭ</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1346" n="HIAT:w" s="T557">măna</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1348" n="HIAT:ip">(</nts>
                  <ts e="T559" id="Seg_1350" n="HIAT:w" s="T558">šobi=</ts>
                  <nts id="Seg_1351" n="HIAT:ip">)</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1354" n="HIAT:w" s="T559">šobi</ts>
                  <nts id="Seg_1355" n="HIAT:ip">…</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T566" id="Seg_1357" n="sc" s="T561">
               <ts e="T566" id="Seg_1359" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1361" n="HIAT:w" s="T561">Ой</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1365" n="HIAT:w" s="T562">опять</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1368" n="HIAT:w" s="T563">по-своему</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1371" n="HIAT:w" s="T564">начала</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_1374" n="sc" s="T568">
               <ts e="T581" id="Seg_1376" n="HIAT:u" s="T568">
                  <ts e="T571" id="Seg_1378" n="HIAT:w" s="T568">Kamen</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1381" n="HIAT:w" s="T571">dĭ</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1384" n="HIAT:w" s="T572">măna</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1387" n="HIAT:w" s="T573">šobi</ts>
                  <nts id="Seg_1388" n="HIAT:ip">,</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1391" n="HIAT:w" s="T574">măn</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1394" n="HIAT:w" s="T575">kudolbiam</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1397" n="HIAT:w" s="T576">dĭm:</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1399" n="HIAT:ip">"</nts>
                  <ts e="T579" id="Seg_1401" n="HIAT:w" s="T578">Iʔ</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1404" n="HIAT:w" s="T579">šoʔ</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1407" n="HIAT:w" s="T580">măna</ts>
                  <nts id="Seg_1408" n="HIAT:ip">!</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_1411" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_1413" n="HIAT:w" s="T581">Măn</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1416" n="HIAT:w" s="T582">tănan</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1419" n="HIAT:w" s="T583">ĭmbidə</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1422" n="HIAT:w" s="T584">ej</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1424" n="HIAT:ip">(</nts>
                  <ts e="T586" id="Seg_1426" n="HIAT:w" s="T585">nörbələm</ts>
                  <nts id="Seg_1427" n="HIAT:ip">)</nts>
                  <nts id="Seg_1428" n="HIAT:ip">.</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T589" id="Seg_1430" n="sc" s="T587">
               <ts e="T589" id="Seg_1432" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_1434" n="HIAT:w" s="T587">Kanaʔ</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1437" n="HIAT:w" s="T588">döʔə</ts>
                  <nts id="Seg_1438" n="HIAT:ip">!</nts>
                  <nts id="Seg_1439" n="HIAT:ip">"</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T596" id="Seg_1441" n="sc" s="T590">
               <ts e="T596" id="Seg_1443" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_1445" n="HIAT:w" s="T590">Dĭgəttə</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1448" n="HIAT:w" s="T591">dĭ</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1451" n="HIAT:w" s="T592">măndə:</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1453" n="HIAT:ip">"</nts>
                  <ts e="T594" id="Seg_1455" n="HIAT:w" s="T593">Ugandə</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1458" n="HIAT:w" s="T594">jakše</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1461" n="HIAT:w" s="T595">dʼăbaktərial</ts>
                  <nts id="Seg_1462" n="HIAT:ip">"</nts>
                  <nts id="Seg_1463" n="HIAT:ip">.</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T603" id="Seg_1465" n="sc" s="T597">
               <ts e="T603" id="Seg_1467" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_1469" n="HIAT:w" s="T597">Măn</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1472" n="HIAT:w" s="T598">măndəm:</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1474" n="HIAT:ip">"</nts>
                  <ts e="T600" id="Seg_1476" n="HIAT:w" s="T599">Прямо</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1479" n="HIAT:w" s="T600">jakše</ts>
                  <nts id="Seg_1480" n="HIAT:ip">,</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1483" n="HIAT:w" s="T601">kudolliam</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1486" n="HIAT:w" s="T602">tănan</ts>
                  <nts id="Seg_1487" n="HIAT:ip">.</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T608" id="Seg_1489" n="sc" s="T604">
               <ts e="T608" id="Seg_1491" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_1493" n="HIAT:w" s="T604">A</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_1496" n="HIAT:w" s="T605">tăn</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1499" n="HIAT:w" s="T606">mănlial:</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1502" n="HIAT:w" s="T607">jakše</ts>
                  <nts id="Seg_1503" n="HIAT:ip">"</nts>
                  <nts id="Seg_1504" n="HIAT:ip">.</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T622" id="Seg_1506" n="sc" s="T609">
               <ts e="T622" id="Seg_1508" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_1510" n="HIAT:w" s="T609">Когды</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1513" n="HIAT:w" s="T610">шел</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1516" n="HIAT:w" s="T611">ко</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1519" n="HIAT:w" s="T612">мне</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1521" n="HIAT:ip">(</nts>
                  <ts e="T614" id="Seg_1523" n="HIAT:w" s="T613">Констан-</ts>
                  <nts id="Seg_1524" n="HIAT:ip">)</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1527" n="HIAT:w" s="T614">Александр</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1530" n="HIAT:w" s="T615">Константинов</ts>
                  <nts id="Seg_1531" n="HIAT:ip">,</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_1534" n="HIAT:w" s="T616">говорит:</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1536" n="HIAT:ip">"</nts>
                  <ts e="T618" id="Seg_1538" n="HIAT:w" s="T617">Бабушка</ts>
                  <nts id="Seg_1539" n="HIAT:ip">,</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_1542" n="HIAT:w" s="T618">я</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_1545" n="HIAT:w" s="T619">к</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_1548" n="HIAT:w" s="T620">тебе</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_1551" n="HIAT:w" s="T621">иду</ts>
                  <nts id="Seg_1552" n="HIAT:ip">!</nts>
                  <nts id="Seg_1553" n="HIAT:ip">"</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_1555" n="sc" s="T623">
               <ts e="T631" id="Seg_1557" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_1559" n="HIAT:w" s="T623">А</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_1562" n="HIAT:w" s="T624">я</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1565" n="HIAT:w" s="T625">говорю:</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1567" n="HIAT:ip">"</nts>
                  <ts e="T627" id="Seg_1569" n="HIAT:w" s="T626">Не</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_1572" n="HIAT:w" s="T627">ходи</ts>
                  <nts id="Seg_1573" n="HIAT:ip">,</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_1576" n="HIAT:w" s="T628">зачем</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_1579" n="HIAT:w" s="T629">ты</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_1582" n="HIAT:w" s="T630">идешь</ts>
                  <nts id="Seg_1583" n="HIAT:ip">?</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_1585" n="sc" s="T632">
               <ts e="T643" id="Seg_1587" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_1589" n="HIAT:w" s="T632">Ты</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_1592" n="HIAT:w" s="T633">мне</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_1595" n="HIAT:w" s="T634">надоел</ts>
                  <nts id="Seg_1596" n="HIAT:ip">,</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1599" n="HIAT:w" s="T635">уходи</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_1602" n="HIAT:w" s="T636">отседова</ts>
                  <nts id="Seg_1603" n="HIAT:ip">,</nts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_1606" n="HIAT:w" s="T637">я</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_1609" n="HIAT:w" s="T638">с</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_1612" n="HIAT:w" s="T639">тобой</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_1615" n="HIAT:w" s="T640">не</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_1618" n="HIAT:w" s="T641">буду</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_1621" n="HIAT:w" s="T642">разговаривать</ts>
                  <nts id="Seg_1622" n="HIAT:ip">"</nts>
                  <nts id="Seg_1623" n="HIAT:ip">.</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T664" id="Seg_1625" n="sc" s="T644">
               <ts e="T655" id="Seg_1627" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_1629" n="HIAT:w" s="T644">А</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_1632" n="HIAT:w" s="T645">он</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_1635" n="HIAT:w" s="T646">говорит:</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1637" n="HIAT:ip">"</nts>
                  <ts e="T648" id="Seg_1639" n="HIAT:w" s="T647">Ой</ts>
                  <nts id="Seg_1640" n="HIAT:ip">,</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_1643" n="HIAT:w" s="T648">бабушка</ts>
                  <nts id="Seg_1644" n="HIAT:ip">,</nts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_1647" n="HIAT:w" s="T649">как</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_1650" n="HIAT:w" s="T650">ты</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_1653" n="HIAT:w" s="T651">хорошо</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1655" n="HIAT:ip">(</nts>
                  <ts e="T653" id="Seg_1657" n="HIAT:w" s="T652">р-</ts>
                  <nts id="Seg_1658" n="HIAT:ip">)</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_1661" n="HIAT:w" s="T653">мне</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_1664" n="HIAT:w" s="T654">рассказываешь</ts>
                  <nts id="Seg_1665" n="HIAT:ip">"</nts>
                  <nts id="Seg_1666" n="HIAT:ip">.</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_1669" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_1671" n="HIAT:w" s="T655">А</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_1674" n="HIAT:w" s="T656">я</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_1677" n="HIAT:w" s="T657">ему</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_1680" n="HIAT:w" s="T658">говорю:</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1682" n="HIAT:ip">"</nts>
                  <ts e="T660" id="Seg_1684" n="HIAT:w" s="T659">Прямо</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_1687" n="HIAT:w" s="T660">хорошо</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_1691" n="HIAT:w" s="T661">я</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_1694" n="HIAT:w" s="T662">тебя</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_1697" n="HIAT:w" s="T663">ругаю</ts>
                  <nts id="Seg_1698" n="HIAT:ip">"</nts>
                  <nts id="Seg_1699" n="HIAT:ip">.</nts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T670" id="Seg_1701" n="sc" s="T665">
               <ts e="T670" id="Seg_1703" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_1705" n="HIAT:w" s="T665">А</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_1708" n="HIAT:w" s="T666">он</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_1711" n="HIAT:w" s="T667">говорит:</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1713" n="HIAT:ip">"</nts>
                  <ts e="T669" id="Seg_1715" n="HIAT:w" s="T668">Ну</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_1718" n="HIAT:w" s="T669">ладно</ts>
                  <nts id="Seg_1719" n="HIAT:ip">"</nts>
                  <nts id="Seg_1720" n="HIAT:ip">.</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T674" id="Seg_1722" n="sc" s="T671">
               <ts e="T674" id="Seg_1724" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_1726" n="HIAT:w" s="T671">Ему</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1728" n="HIAT:ip">(</nts>
                  <ts e="T673" id="Seg_1730" n="HIAT:w" s="T672">глянулося</ts>
                  <nts id="Seg_1731" n="HIAT:ip">)</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_1734" n="HIAT:w" s="T673">приехать</ts>
                  <nts id="Seg_1735" n="HIAT:ip">.</nts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T681" id="Seg_1737" n="sc" s="T675">
               <ts e="T681" id="Seg_1739" n="HIAT:u" s="T675">
                  <nts id="Seg_1740" n="HIAT:ip">"</nts>
                  <ts e="T676" id="Seg_1742" n="HIAT:w" s="T675">Ну</ts>
                  <nts id="Seg_1743" n="HIAT:ip">,</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_1746" n="HIAT:w" s="T676">я</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_1749" n="HIAT:w" s="T677">хотел</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_1752" n="HIAT:w" s="T678">с</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_1755" n="HIAT:w" s="T679">сыном</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_1758" n="HIAT:w" s="T680">приехать</ts>
                  <nts id="Seg_1759" n="HIAT:ip">"</nts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T686" id="Seg_1762" n="sc" s="T682">
               <ts e="T686" id="Seg_1764" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_1766" n="HIAT:w" s="T682">Его</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_1769" n="HIAT:w" s="T683">сын</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_1772" n="HIAT:w" s="T684">Константином</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_1775" n="HIAT:w" s="T685">звать</ts>
                  <nts id="Seg_1776" n="HIAT:ip">.</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T691" id="Seg_1778" n="sc" s="T687">
               <ts e="T691" id="Seg_1780" n="HIAT:u" s="T687">
                  <nts id="Seg_1781" n="HIAT:ip">"</nts>
                  <ts e="T688" id="Seg_1783" n="HIAT:w" s="T687">И</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_1786" n="HIAT:w" s="T688">теперь</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_1789" n="HIAT:w" s="T689">напрок</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_1792" n="HIAT:w" s="T690">приеду</ts>
                  <nts id="Seg_1793" n="HIAT:ip">"</nts>
                  <nts id="Seg_1794" n="HIAT:ip">.</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T701" id="Seg_1796" n="sc" s="T692">
               <ts e="T701" id="Seg_1798" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_1800" n="HIAT:w" s="T692">Ну</ts>
                  <nts id="Seg_1801" n="HIAT:ip">,</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_1804" n="HIAT:w" s="T693">а</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_1807" n="HIAT:w" s="T694">я</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_1810" n="HIAT:w" s="T695">говорю:</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1812" n="HIAT:ip">"</nts>
                  <ts e="T697" id="Seg_1814" n="HIAT:w" s="T696">Приедет</ts>
                  <nts id="Seg_1815" n="HIAT:ip">,</nts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_1818" n="HIAT:w" s="T697">напрок</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_1821" n="HIAT:w" s="T698">может</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_1824" n="HIAT:w" s="T699">я</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_1827" n="HIAT:w" s="T700">помру</ts>
                  <nts id="Seg_1828" n="HIAT:ip">"</nts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T706" id="Seg_1831" n="sc" s="T702">
               <ts e="T706" id="Seg_1833" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_1835" n="HIAT:w" s="T702">Стара</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_1838" n="HIAT:w" s="T703">уже</ts>
                  <nts id="Seg_1839" n="HIAT:ip">,</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_1842" n="HIAT:w" s="T704">дожидаешь</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_1845" n="HIAT:w" s="T705">смерти</ts>
                  <nts id="Seg_1846" n="HIAT:ip">.</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T712" id="Seg_1848" n="sc" s="T707">
               <ts e="T712" id="Seg_1850" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_1852" n="HIAT:w" s="T707">Ну</ts>
                  <nts id="Seg_1853" n="HIAT:ip">,</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_1856" n="HIAT:w" s="T708">можно</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_1859" n="HIAT:w" s="T709">и</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_1862" n="HIAT:w" s="T710">это</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_1865" n="HIAT:w" s="T711">поговорить</ts>
                  <nts id="Seg_1866" n="HIAT:ip">.</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T716" id="Seg_1868" n="sc" s="T713">
               <ts e="T716" id="Seg_1870" n="HIAT:u" s="T713">
                  <ts e="T714" id="Seg_1872" n="HIAT:w" s="T713">Ну</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_1875" n="HIAT:w" s="T714">чего</ts>
                  <nts id="Seg_1876" n="HIAT:ip">?</nts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T724" id="Seg_1878" n="sc" s="T722">
               <ts e="T724" id="Seg_1880" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_1882" n="HIAT:w" s="T722">Ой</ts>
                  <nts id="Seg_1883" n="HIAT:ip">,</nts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_1886" n="HIAT:w" s="T723">господи</ts>
                  <nts id="Seg_1887" n="HIAT:ip">.</nts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T738" id="Seg_1889" n="sc" s="T728">
               <ts e="T738" id="Seg_1891" n="HIAT:u" s="T728">
                  <ts e="T730" id="Seg_1893" n="HIAT:w" s="T728">А-а-а</ts>
                  <nts id="Seg_1894" n="HIAT:ip">,</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1896" n="HIAT:ip">(</nts>
                  <ts e="T733" id="Seg_1898" n="HIAT:w" s="T730">Алек-</ts>
                  <nts id="Seg_1899" n="HIAT:ip">)</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_1902" n="HIAT:w" s="T733">Александр</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_1905" n="HIAT:w" s="T735">Константинович</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_1908" n="HIAT:w" s="T736">обещал</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_1911" n="HIAT:w" s="T737">приехать</ts>
                  <nts id="Seg_1912" n="HIAT:ip">.</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_1914" n="sc" s="T739">
               <ts e="T750" id="Seg_1916" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_1918" n="HIAT:w" s="T739">Написал</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_1921" n="HIAT:w" s="T740">письмо</ts>
                  <nts id="Seg_1922" n="HIAT:ip">,</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_1925" n="HIAT:w" s="T741">а</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_1928" n="HIAT:w" s="T742">потом</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1930" n="HIAT:ip">(</nts>
                  <ts e="T744" id="Seg_1932" n="HIAT:w" s="T743">я</ts>
                  <nts id="Seg_1933" n="HIAT:ip">)</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_1936" n="HIAT:w" s="T744">ждала-ждала</ts>
                  <nts id="Seg_1937" n="HIAT:ip">,</nts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_1940" n="HIAT:w" s="T745">он</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_1943" n="HIAT:w" s="T746">опять</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_1946" n="HIAT:w" s="T747">пишет:</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1948" n="HIAT:ip">"</nts>
                  <ts e="T749" id="Seg_1950" n="HIAT:w" s="T748">Не</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_1953" n="HIAT:w" s="T749">приеду</ts>
                  <nts id="Seg_1954" n="HIAT:ip">.</nts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T759" id="Seg_1956" n="sc" s="T751">
               <ts e="T759" id="Seg_1958" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_1960" n="HIAT:w" s="T751">Ездил</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_1963" n="HIAT:w" s="T752">я</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_1966" n="HIAT:w" s="T753">в</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_1969" n="HIAT:w" s="T754">Москву</ts>
                  <nts id="Seg_1970" n="HIAT:ip">,</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_1973" n="HIAT:w" s="T755">и</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_1976" n="HIAT:w" s="T756">на</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_1979" n="HIAT:w" s="T757">север</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_1982" n="HIAT:w" s="T758">ездил</ts>
                  <nts id="Seg_1983" n="HIAT:ip">.</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T765" id="Seg_1985" n="sc" s="T760">
               <ts e="T765" id="Seg_1987" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_1989" n="HIAT:w" s="T760">Так</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_1992" n="HIAT:w" s="T761">что</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_1995" n="HIAT:w" s="T762">не</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_1998" n="HIAT:w" s="T763">могу</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2001" n="HIAT:w" s="T764">приехать</ts>
                  <nts id="Seg_2002" n="HIAT:ip">"</nts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T772" id="Seg_2005" n="sc" s="T766">
               <ts e="T772" id="Seg_2007" n="HIAT:u" s="T766">
                  <ts e="T767" id="Seg_2009" n="HIAT:w" s="T766">Ну</ts>
                  <nts id="Seg_2010" n="HIAT:ip">,</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2013" n="HIAT:w" s="T767">я</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2016" n="HIAT:w" s="T768">и</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2019" n="HIAT:w" s="T769">не</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2022" n="HIAT:w" s="T770">стала</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2025" n="HIAT:w" s="T771">дожидать</ts>
                  <nts id="Seg_2026" n="HIAT:ip">.</nts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T776" id="Seg_2028" n="sc" s="T773">
               <ts e="T776" id="Seg_2030" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2032" n="HIAT:w" s="T773">А</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2035" n="HIAT:w" s="T774">потом</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2038" n="HIAT:w" s="T775">приехал</ts>
                  <nts id="Seg_2039" n="HIAT:ip">…</nts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T778" id="Seg_2041" n="sc" s="T777">
               <ts e="T778" id="Seg_2043" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_2045" n="HIAT:w" s="T777">Господи</ts>
                  <nts id="Seg_2046" n="HIAT:ip">.</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T782" id="Seg_2048" n="sc" s="T779">
               <ts e="T782" id="Seg_2050" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_2052" n="HIAT:w" s="T779">И</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2055" n="HIAT:w" s="T780">имя-то</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2058" n="HIAT:w" s="T781">забываю</ts>
                  <nts id="Seg_2059" n="HIAT:ip">.</nts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T790" id="Seg_2061" n="sc" s="T783">
               <ts e="T790" id="Seg_2063" n="HIAT:u" s="T783">
                  <ts e="T784" id="Seg_2065" n="HIAT:w" s="T783">Арпит</ts>
                  <nts id="Seg_2066" n="HIAT:ip">,</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2069" n="HIAT:w" s="T784">я</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2072" n="HIAT:w" s="T785">с</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2075" n="HIAT:w" s="T786">им</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2078" n="HIAT:w" s="T787">собралась</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2081" n="HIAT:w" s="T788">да</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2084" n="HIAT:w" s="T789">уехала</ts>
                  <nts id="Seg_2085" n="HIAT:ip">.</nts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T800" id="Seg_2087" n="sc" s="T791">
               <ts e="T800" id="Seg_2089" n="HIAT:u" s="T791">
                  <nts id="Seg_2090" n="HIAT:ip">"</nts>
                  <ts e="T792" id="Seg_2092" n="HIAT:w" s="T791">Так</ts>
                  <nts id="Seg_2093" n="HIAT:ip">,</nts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2096" n="HIAT:w" s="T792">думаю</ts>
                  <nts id="Seg_2097" n="HIAT:ip">,</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2100" n="HIAT:w" s="T793">раз</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2103" n="HIAT:w" s="T794">не</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2106" n="HIAT:w" s="T795">хочет</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2109" n="HIAT:w" s="T796">приезжать</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2112" n="HIAT:w" s="T797">дак</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2115" n="HIAT:w" s="T798">и</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2118" n="HIAT:w" s="T799">уеду</ts>
                  <nts id="Seg_2119" n="HIAT:ip">"</nts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T803" id="Seg_2122" n="sc" s="T801">
               <ts e="T803" id="Seg_2124" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_2126" n="HIAT:w" s="T801">Приехала</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2129" n="HIAT:w" s="T802">сюды</ts>
                  <nts id="Seg_2130" n="HIAT:ip">.</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T823" id="Seg_2132" n="sc" s="T819">
               <ts e="T823" id="Seg_2134" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_2136" n="HIAT:w" s="T819">Как</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2139" n="HIAT:w" s="T820">не</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2142" n="HIAT:w" s="T821">говорили</ts>
                  <nts id="Seg_2143" n="HIAT:ip">?</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T834" id="Seg_2145" n="sc" s="T828">
               <ts e="T834" id="Seg_2147" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_2149" n="HIAT:w" s="T828">Я</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2152" n="HIAT:w" s="T829">по-русски</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2155" n="HIAT:w" s="T830">записала</ts>
                  <nts id="Seg_2156" n="HIAT:ip">,</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2159" n="HIAT:w" s="T831">потом</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2162" n="HIAT:w" s="T832">по-камасински</ts>
                  <nts id="Seg_2163" n="HIAT:ip">.</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T845" id="Seg_2165" n="sc" s="T841">
               <ts e="T845" id="Seg_2167" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_2169" n="HIAT:w" s="T841">Мне</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2172" n="HIAT:w" s="T842">думается</ts>
                  <nts id="Seg_2173" n="HIAT:ip">,</nts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2176" n="HIAT:w" s="T843">рассказали</ts>
                  <nts id="Seg_2177" n="HIAT:ip">.</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T849" id="Seg_2179" n="sc" s="T847">
               <ts e="T849" id="Seg_2181" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_2183" n="HIAT:w" s="T847">Два</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2186" n="HIAT:w" s="T848">раз</ts>
                  <nts id="Seg_2187" n="HIAT:ip">.</nts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T859" id="Seg_2189" n="sc" s="T850">
               <ts e="T859" id="Seg_2191" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_2193" n="HIAT:w" s="T850">Ну</ts>
                  <nts id="Seg_2194" n="HIAT:ip">,</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2197" n="HIAT:w" s="T851">пускай</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2200" n="HIAT:w" s="T852">расскажу</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2203" n="HIAT:w" s="T853">опять</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2206" n="HIAT:w" s="T854">про</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2209" n="HIAT:w" s="T855">это</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2212" n="HIAT:w" s="T857">же</ts>
                  <nts id="Seg_2213" n="HIAT:ip">.</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T866" id="Seg_2215" n="sc" s="T860">
               <ts e="T866" id="Seg_2217" n="HIAT:u" s="T860">
                  <nts id="Seg_2218" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_2220" n="HIAT:w" s="T860">Kamen=</ts>
                  <nts id="Seg_2221" n="HIAT:ip">)</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2224" n="HIAT:w" s="T861">Kamen</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2227" n="HIAT:w" s="T862">šobi</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2230" n="HIAT:w" s="T863">Александр</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2233" n="HIAT:w" s="T864">Константинович</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2236" n="HIAT:w" s="T865">măna</ts>
                  <nts id="Seg_2237" n="HIAT:ip">…</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T873" id="Seg_2239" n="sc" s="T867">
               <ts e="T873" id="Seg_2241" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_2243" n="HIAT:w" s="T867">Da</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2246" n="HIAT:w" s="T868">dĭ</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_2249" n="HIAT:w" s="T869">pʼaŋdəbi</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2252" n="HIAT:w" s="T870">măna:</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2254" n="HIAT:ip">"</nts>
                  <ts e="T872" id="Seg_2256" n="HIAT:w" s="T871">Šolam</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2259" n="HIAT:w" s="T872">tănan</ts>
                  <nts id="Seg_2260" n="HIAT:ip">"</nts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T878" id="Seg_2263" n="sc" s="T874">
               <ts e="T878" id="Seg_2265" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_2267" n="HIAT:w" s="T874">Măn</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2269" n="HIAT:ip">(</nts>
                  <ts e="T876" id="Seg_2271" n="HIAT:w" s="T875">edəʔpiem</ts>
                  <nts id="Seg_2272" n="HIAT:ip">)</nts>
                  <nts id="Seg_2273" n="HIAT:ip">,</nts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2276" n="HIAT:w" s="T876">edəʔpiem</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2278" n="HIAT:ip">—</nts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2281" n="HIAT:w" s="T877">naga</ts>
                  <nts id="Seg_2282" n="HIAT:ip">.</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T886" id="Seg_2284" n="sc" s="T879">
               <ts e="T886" id="Seg_2286" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_2288" n="HIAT:w" s="T879">Dĭgəttə</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2291" n="HIAT:w" s="T880">pʼaŋdlaʔbə</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2294" n="HIAT:w" s="T881">măna</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2297" n="HIAT:w" s="T882">sazən:</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2299" n="HIAT:ip">"</nts>
                  <ts e="T884" id="Seg_2301" n="HIAT:w" s="T883">Măn</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_2304" n="HIAT:w" s="T884">ej</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_2307" n="HIAT:w" s="T885">šolam</ts>
                  <nts id="Seg_2308" n="HIAT:ip">.</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T895" id="Seg_2310" n="sc" s="T887">
               <ts e="T895" id="Seg_2312" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_2314" n="HIAT:w" s="T887">Dö</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2316" n="HIAT:ip">(</nts>
                  <nts id="Seg_2317" n="HIAT:ip">(</nts>
                  <ats e="T889" id="Seg_2318" n="HIAT:non-pho" s="T888">…</ats>
                  <nts id="Seg_2319" n="HIAT:ip">)</nts>
                  <nts id="Seg_2320" n="HIAT:ip">)</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2323" n="HIAT:w" s="T889">dĭ</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_2326" n="HIAT:w" s="T890">kögən</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2328" n="HIAT:ip">(</nts>
                  <ts e="T892" id="Seg_2330" n="HIAT:w" s="T891">šo-</ts>
                  <nts id="Seg_2331" n="HIAT:ip">)</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2334" n="HIAT:w" s="T892">šolam</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2336" n="HIAT:ip">(</nts>
                  <ts e="T894" id="Seg_2338" n="HIAT:w" s="T893">šiʔ-</ts>
                  <nts id="Seg_2339" n="HIAT:ip">)</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_2342" n="HIAT:w" s="T894">tănan</ts>
                  <nts id="Seg_2343" n="HIAT:ip">"</nts>
                  <nts id="Seg_2344" n="HIAT:ip">.</nts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T902" id="Seg_2346" n="sc" s="T896">
               <ts e="T902" id="Seg_2348" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_2350" n="HIAT:w" s="T896">A</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_2353" n="HIAT:w" s="T897">măn</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_2356" n="HIAT:w" s="T898">mămbiam:</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2358" n="HIAT:ip">"</nts>
                  <ts e="T900" id="Seg_2360" n="HIAT:w" s="T899">Măn</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2363" n="HIAT:w" s="T900">možet</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_2366" n="HIAT:w" s="T901">külalləm</ts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T907" id="Seg_2369" n="sc" s="T903">
               <ts e="T907" id="Seg_2371" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_2373" n="HIAT:w" s="T903">A</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_2376" n="HIAT:w" s="T904">tăn</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2379" n="HIAT:w" s="T905">dĭgəttə</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_2382" n="HIAT:w" s="T906">šolal</ts>
                  <nts id="Seg_2383" n="HIAT:ip">"</nts>
                  <nts id="Seg_2384" n="HIAT:ip">.</nts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T913" id="Seg_2386" n="sc" s="T908">
               <ts e="T913" id="Seg_2388" n="HIAT:u" s="T908">
                  <ts e="T909" id="Seg_2390" n="HIAT:w" s="T908">Dĭgəttə</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2393" n="HIAT:w" s="T909">oʔbdəbiam</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2396" n="HIAT:w" s="T910">da</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_2399" n="HIAT:w" s="T911">döbər</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_2402" n="HIAT:w" s="T912">šobiam</ts>
                  <nts id="Seg_2403" n="HIAT:ip">.</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T915" id="Seg_2405" n="sc" s="T914">
               <ts e="T915" id="Seg_2407" n="HIAT:u" s="T914">
                  <nts id="Seg_2408" n="HIAT:ip">(</nts>
                  <nts id="Seg_2409" n="HIAT:ip">(</nts>
                  <ats e="T915" id="Seg_2410" n="HIAT:non-pho" s="T914">BRK</ats>
                  <nts id="Seg_2411" n="HIAT:ip">)</nts>
                  <nts id="Seg_2412" n="HIAT:ip">)</nts>
                  <nts id="Seg_2413" n="HIAT:ip">.</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T920" id="Seg_2415" n="sc" s="T916">
               <ts e="T920" id="Seg_2417" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_2419" n="HIAT:w" s="T916">Măn</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_2422" n="HIAT:w" s="T917">koʔbdom</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2424" n="HIAT:ip">(</nts>
                  <ts e="T919" id="Seg_2426" n="HIAT:w" s="T918">tibinə</ts>
                  <nts id="Seg_2427" n="HIAT:ip">)</nts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_2430" n="HIAT:w" s="T919">kambi</ts>
                  <nts id="Seg_2431" n="HIAT:ip">.</nts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T933" id="Seg_2433" n="sc" s="T921">
               <ts e="T933" id="Seg_2435" n="HIAT:u" s="T921">
                  <ts e="T922" id="Seg_2437" n="HIAT:w" s="T921">Ibiem</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_2440" n="HIAT:w" s="T922">Krasnăjarskagən</ts>
                  <nts id="Seg_2441" n="HIAT:ip">,</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_2444" n="HIAT:w" s="T923">dĭ</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_2447" n="HIAT:w" s="T924">măndə:</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2449" n="HIAT:ip">"</nts>
                  <ts e="T927" id="Seg_2451" n="HIAT:w" s="T926">Măna</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_2454" n="HIAT:w" s="T927">onʼiʔ</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_2457" n="HIAT:w" s="T928">nʼi</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_2460" n="HIAT:w" s="T929">izittə</ts>
                  <nts id="Seg_2461" n="HIAT:ip">,</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_2464" n="HIAT:w" s="T930">abanə</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_2467" n="HIAT:w" s="T931">iʔ</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_2470" n="HIAT:w" s="T932">nörbit</ts>
                  <nts id="Seg_2471" n="HIAT:ip">"</nts>
                  <nts id="Seg_2472" n="HIAT:ip">.</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T950" id="Seg_2474" n="sc" s="T934">
               <ts e="T950" id="Seg_2476" n="HIAT:u" s="T934">
                  <ts e="T935" id="Seg_2478" n="HIAT:w" s="T934">A</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_2481" n="HIAT:w" s="T935">măn</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_2484" n="HIAT:w" s="T936">mămbiam:</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2486" n="HIAT:ip">"</nts>
                  <ts e="T939" id="Seg_2488" n="HIAT:w" s="T938">Kak</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_2491" n="HIAT:w" s="T939">ej</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_2494" n="HIAT:w" s="T940">nörbəsʼtə</ts>
                  <nts id="Seg_2495" n="HIAT:ip">,</nts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_2498" n="HIAT:w" s="T941">abat</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_2501" n="HIAT:w" s="T942">bostə</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_2504" n="HIAT:w" s="T943">ibi</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_2507" n="HIAT:w" s="T944">boskəndə</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2509" n="HIAT:ip">(</nts>
                  <ts e="T946" id="Seg_2511" n="HIAT:w" s="T945">i</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_2514" n="HIAT:w" s="T946">d-</ts>
                  <nts id="Seg_2515" n="HIAT:ip">)</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_2518" n="HIAT:w" s="T947">i</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_2521" n="HIAT:w" s="T948">šiʔ</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_2524" n="HIAT:w" s="T949">deʔpi</ts>
                  <nts id="Seg_2525" n="HIAT:ip">"</nts>
                  <nts id="Seg_2526" n="HIAT:ip">.</nts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T959" id="Seg_2528" n="sc" s="T951">
               <ts e="T959" id="Seg_2530" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_2532" n="HIAT:w" s="T951">Dĭgəttə</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_2535" n="HIAT:w" s="T952">šobiam</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_2538" n="HIAT:w" s="T953">abanə</ts>
                  <nts id="Seg_2539" n="HIAT:ip">,</nts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_2542" n="HIAT:w" s="T954">nörbəbiem</ts>
                  <nts id="Seg_2543" n="HIAT:ip">,</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_2546" n="HIAT:w" s="T955">dĭ</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_2549" n="HIAT:w" s="T956">pʼaŋdəbi</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_2552" n="HIAT:w" s="T957">dĭʔnə</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_2555" n="HIAT:w" s="T958">sazən</ts>
                  <nts id="Seg_2556" n="HIAT:ip">.</nts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T968" id="Seg_2558" n="sc" s="T960">
               <ts e="T968" id="Seg_2560" n="HIAT:u" s="T960">
                  <ts e="T961" id="Seg_2562" n="HIAT:w" s="T960">Dĭgəttə</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_2565" n="HIAT:w" s="T961">dĭ</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_2568" n="HIAT:w" s="T962">dĭʔnə</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_2571" n="HIAT:w" s="T963">sazən</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_2574" n="HIAT:w" s="T964">pʼaŋdəbi</ts>
                  <nts id="Seg_2575" n="HIAT:ip">,</nts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_2578" n="HIAT:w" s="T965">dĭ</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_2581" n="HIAT:w" s="T966">döbər</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_2584" n="HIAT:w" s="T967">šobi</ts>
                  <nts id="Seg_2585" n="HIAT:ip">.</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T980" id="Seg_2587" n="sc" s="T969">
               <ts e="T980" id="Seg_2589" n="HIAT:u" s="T969">
                  <ts e="T970" id="Seg_2591" n="HIAT:w" s="T969">Dĭgəttə</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2593" n="HIAT:ip">(</nts>
                  <ts e="T971" id="Seg_2595" n="HIAT:w" s="T970">uja=</ts>
                  <nts id="Seg_2596" n="HIAT:ip">)</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_2599" n="HIAT:w" s="T971">šoška</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_2602" n="HIAT:w" s="T972">dʼagarbibaʔ</ts>
                  <nts id="Seg_2603" n="HIAT:ip">,</nts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_2606" n="HIAT:w" s="T973">uja</ts>
                  <nts id="Seg_2607" n="HIAT:ip">,</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_2610" n="HIAT:w" s="T974">iʔgö</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_2613" n="HIAT:w" s="T975">ipek</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_2616" n="HIAT:w" s="T976">pürbibeʔ</ts>
                  <nts id="Seg_2617" n="HIAT:ip">,</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_2620" n="HIAT:w" s="T977">i</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_2623" n="HIAT:w" s="T978">il</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_2626" n="HIAT:w" s="T979">oʔbdəbibaʔ</ts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T984" id="Seg_2629" n="sc" s="T981">
               <ts e="T984" id="Seg_2631" n="HIAT:u" s="T981">
                  <ts e="T982" id="Seg_2633" n="HIAT:w" s="T981">Dĭgəttə</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_2636" n="HIAT:w" s="T982">ara</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_2639" n="HIAT:w" s="T983">ibi</ts>
                  <nts id="Seg_2640" n="HIAT:ip">.</nts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T992" id="Seg_2642" n="sc" s="T985">
               <ts e="T988" id="Seg_2644" n="HIAT:u" s="T985">
                  <ts e="T986" id="Seg_2646" n="HIAT:w" s="T985">Pʼelʼmʼenʼəʔi</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_2649" n="HIAT:w" s="T986">abibaʔ</ts>
                  <nts id="Seg_2650" n="HIAT:ip">,</nts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_2653" n="HIAT:w" s="T987">kătletaʔi</ts>
                  <nts id="Seg_2654" n="HIAT:ip">.</nts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_2657" n="HIAT:u" s="T988">
                  <ts e="T989" id="Seg_2659" n="HIAT:w" s="T988">Iʔgö</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2661" n="HIAT:ip">(</nts>
                  <ts e="T990" id="Seg_2663" n="HIAT:w" s="T989">ibi=</ts>
                  <nts id="Seg_2664" n="HIAT:ip">)</nts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_2667" n="HIAT:w" s="T990">ĭmbi</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_2670" n="HIAT:w" s="T991">ibi</ts>
                  <nts id="Seg_2671" n="HIAT:ip">.</nts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T997" id="Seg_2673" n="sc" s="T993">
               <ts e="T997" id="Seg_2675" n="HIAT:u" s="T993">
                  <ts e="T994" id="Seg_2677" n="HIAT:w" s="T993">I</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_2680" n="HIAT:w" s="T994">il</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_2683" n="HIAT:w" s="T995">iʔgö</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_2686" n="HIAT:w" s="T996">ibiʔi</ts>
                  <nts id="Seg_2687" n="HIAT:ip">.</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1000" id="Seg_2689" n="sc" s="T998">
               <ts e="T1000" id="Seg_2691" n="HIAT:u" s="T998">
                  <ts e="T1525" id="Seg_2693" n="HIAT:w" s="T998">Kazan</ts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_2696" n="HIAT:w" s="T1525">turagən</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_2699" n="HIAT:w" s="T999">šobiʔi</ts>
                  <nts id="Seg_2700" n="HIAT:ip">.</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1006" id="Seg_2702" n="sc" s="T1001">
               <ts e="T1006" id="Seg_2704" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_2706" n="HIAT:w" s="T1001">Dĭgəttə</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_2709" n="HIAT:w" s="T1002">măn</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_2712" n="HIAT:w" s="T1003">dĭʔnə</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_2715" n="HIAT:w" s="T1004">ibiem</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_2718" n="HIAT:w" s="T1005">palʼto</ts>
                  <nts id="Seg_2719" n="HIAT:ip">.</nts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1012" id="Seg_2721" n="sc" s="T1007">
               <ts e="T1012" id="Seg_2723" n="HIAT:u" s="T1007">
                  <ts e="T1008" id="Seg_2725" n="HIAT:w" s="T1007">Dĭgəttə</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_2728" n="HIAT:w" s="T1008">aktʼa</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_2731" n="HIAT:w" s="T1009">dĭʔnə</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2733" n="HIAT:ip">(</nts>
                  <ts e="T1011" id="Seg_2735" n="HIAT:w" s="T1010">ak-</ts>
                  <nts id="Seg_2736" n="HIAT:ip">)</nts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_2739" n="HIAT:w" s="T1011">mĭbiem</ts>
                  <nts id="Seg_2740" n="HIAT:ip">.</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1017" id="Seg_2742" n="sc" s="T1013">
               <ts e="T1017" id="Seg_2744" n="HIAT:u" s="T1013">
                  <ts e="T1014" id="Seg_2746" n="HIAT:w" s="T1013">Dĭ</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1522" id="Seg_2749" n="HIAT:w" s="T1014">kalla</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_2752" n="HIAT:w" s="T1522">dʼürbi</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_2755" n="HIAT:w" s="T1015">dĭ</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_2758" n="HIAT:w" s="T1016">nʼizi</ts>
                  <nts id="Seg_2759" n="HIAT:ip">.</nts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1028" id="Seg_2761" n="sc" s="T1018">
               <ts e="T1028" id="Seg_2763" n="HIAT:u" s="T1018">
                  <ts e="T1019" id="Seg_2765" n="HIAT:w" s="T1018">Dĭbər</ts>
                  <nts id="Seg_2766" n="HIAT:ip">,</nts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2768" n="HIAT:ip">(</nts>
                  <ts e="T1020" id="Seg_2770" n="HIAT:w" s="T1019">gijen</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_2773" n="HIAT:w" s="T1020">=</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_2776" n="HIAT:w" s="T1021">il=</ts>
                  <nts id="Seg_2777" n="HIAT:ip">)</nts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_2780" n="HIAT:w" s="T1022">gijen</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_2783" n="HIAT:w" s="T1023">il</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2785" n="HIAT:ip">(</nts>
                  <ts e="T1025" id="Seg_2787" n="HIAT:w" s="T1024">dʼăbaktərzi-</ts>
                  <nts id="Seg_2788" n="HIAT:ip">)</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_2791" n="HIAT:w" s="T1025">dʼăbaktəriaʔi</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_2794" n="HIAT:w" s="T1026">bostə</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_2797" n="HIAT:w" s="T1027">šĭketsi</ts>
                  <nts id="Seg_2798" n="HIAT:ip">.</nts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1049" id="Seg_2800" n="sc" s="T1029">
               <ts e="T1049" id="Seg_2802" n="HIAT:u" s="T1029">
                  <ts e="T1030" id="Seg_2804" n="HIAT:w" s="T1029">Dĭgəttə</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2806" n="HIAT:ip">(</nts>
                  <ts e="T1031" id="Seg_2808" n="HIAT:w" s="T1030">dĭ=</ts>
                  <nts id="Seg_2809" n="HIAT:ip">)</nts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_2812" n="HIAT:w" s="T1031">dĭ</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_2815" n="HIAT:w" s="T1032">nʼin</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_2818" n="HIAT:w" s="T1033">ia</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2820" n="HIAT:ip">(</nts>
                  <ts e="T1035" id="Seg_2822" n="HIAT:w" s="T1034">dĭ-</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_2825" n="HIAT:w" s="T1035">dĭm=</ts>
                  <nts id="Seg_2826" n="HIAT:ip">)</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_2829" n="HIAT:w" s="T1036">dĭm</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_2832" n="HIAT:w" s="T1037">ej</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_2835" n="HIAT:w" s="T1038">ajirbi</ts>
                  <nts id="Seg_2836" n="HIAT:ip">,</nts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_2839" n="HIAT:w" s="T1039">dĭgəttə</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_2842" n="HIAT:w" s="T1040">dĭ</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_2845" n="HIAT:w" s="T1041">döbər</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_2848" n="HIAT:w" s="T1042">pʼaŋdlaʔbə:</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2850" n="HIAT:ip">"</nts>
                  <nts id="Seg_2851" n="HIAT:ip">(</nts>
                  <ts e="T1045" id="Seg_2853" n="HIAT:w" s="T1044">Kažen-</ts>
                  <nts id="Seg_2854" n="HIAT:ip">)</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_2857" n="HIAT:w" s="T1045">Kažnej</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2859" n="HIAT:ip">(</nts>
                  <ts e="T1047" id="Seg_2861" n="HIAT:w" s="T1046">dʼan-</ts>
                  <nts id="Seg_2862" n="HIAT:ip">)</nts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_2865" n="HIAT:w" s="T1047">dʼala</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_2868" n="HIAT:w" s="T1048">dʼorlaʔbəm</ts>
                  <nts id="Seg_2869" n="HIAT:ip">.</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1057" id="Seg_2871" n="sc" s="T1050">
               <ts e="T1057" id="Seg_2873" n="HIAT:u" s="T1050">
                  <ts e="T1051" id="Seg_2875" n="HIAT:w" s="T1050">Miʔ</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_2878" n="HIAT:w" s="T1051">стали</ts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_2881" n="HIAT:w" s="T1052">dĭʔnə</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_2884" n="HIAT:w" s="T1053">pʼaŋdəsʼtə:</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2886" n="HIAT:ip">"</nts>
                  <ts e="T1056" id="Seg_2888" n="HIAT:w" s="T1055">Šoʔ</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_2891" n="HIAT:w" s="T1056">maʔnəl</ts>
                  <nts id="Seg_2892" n="HIAT:ip">"</nts>
                  <nts id="Seg_2893" n="HIAT:ip">.</nts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1064" id="Seg_2895" n="sc" s="T1058">
               <ts e="T1064" id="Seg_2897" n="HIAT:u" s="T1058">
                  <ts e="T1059" id="Seg_2899" n="HIAT:w" s="T1058">Dĭgəttə</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_2902" n="HIAT:w" s="T1059">dĭ</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_2905" n="HIAT:w" s="T1060">măndə:</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2907" n="HIAT:ip">"</nts>
                  <ts e="T1062" id="Seg_2909" n="HIAT:w" s="T1061">Măn</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_2912" n="HIAT:w" s="T1062">unnʼa</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_2915" n="HIAT:w" s="T1063">šolam</ts>
                  <nts id="Seg_2916" n="HIAT:ip">"</nts>
                  <nts id="Seg_2917" n="HIAT:ip">.</nts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1069" id="Seg_2919" n="sc" s="T1065">
               <ts e="T1069" id="Seg_2921" n="HIAT:u" s="T1065">
                  <nts id="Seg_2922" n="HIAT:ip">"</nts>
                  <ts e="T1066" id="Seg_2924" n="HIAT:w" s="T1065">A</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_2927" n="HIAT:w" s="T1066">ĭmbi</ts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_2930" n="HIAT:w" s="T1067">unnʼa</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_2933" n="HIAT:w" s="T1068">šolal</ts>
                  <nts id="Seg_2934" n="HIAT:ip">?</nts>
                  <nts id="Seg_2935" n="HIAT:ip">"</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1076" id="Seg_2937" n="sc" s="T1070">
               <ts e="T1076" id="Seg_2939" n="HIAT:u" s="T1070">
                  <ts e="T1071" id="Seg_2941" n="HIAT:w" s="T1070">A</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_2944" n="HIAT:w" s="T1071">dĭ</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_2947" n="HIAT:w" s="T1072">bazo</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_2950" n="HIAT:w" s="T1073">pʼaŋdlaʔbə:</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2952" n="HIAT:ip">"</nts>
                  <ts e="T1075" id="Seg_2954" n="HIAT:w" s="T1074">Aktʼa</ts>
                  <nts id="Seg_2955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_2957" n="HIAT:w" s="T1075">naga</ts>
                  <nts id="Seg_2958" n="HIAT:ip">"</nts>
                  <nts id="Seg_2959" n="HIAT:ip">.</nts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1085" id="Seg_2961" n="sc" s="T1077">
               <ts e="T1085" id="Seg_2963" n="HIAT:u" s="T1077">
                  <ts e="T1078" id="Seg_2965" n="HIAT:w" s="T1077">Măn</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_2968" n="HIAT:w" s="T1078">bazo</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_2971" n="HIAT:w" s="T1079">öʔlubiem</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2973" n="HIAT:ip">(</nts>
                  <ts e="T1081" id="Seg_2975" n="HIAT:w" s="T1080">šide</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_2978" n="HIAT:w" s="T1081">bʼeʔ=</ts>
                  <nts id="Seg_2979" n="HIAT:ip">)</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_2982" n="HIAT:w" s="T1082">šide</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_2985" n="HIAT:w" s="T1083">biʔ</ts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_2988" n="HIAT:w" s="T1084">aktʼa</ts>
                  <nts id="Seg_2989" n="HIAT:ip">.</nts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1092" id="Seg_2991" n="sc" s="T1086">
               <ts e="T1092" id="Seg_2993" n="HIAT:u" s="T1086">
                  <ts e="T1087" id="Seg_2995" n="HIAT:w" s="T1086">Dĭgəttə</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_2998" n="HIAT:w" s="T1087">abat</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_3001" n="HIAT:w" s="T1088">sumna</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3003" n="HIAT:ip">(</nts>
                  <ts e="T1090" id="Seg_3005" n="HIAT:w" s="T1089">šide</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_3008" n="HIAT:w" s="T1090">biʔ</ts>
                  <nts id="Seg_3009" n="HIAT:ip">)</nts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1092" id="Seg_3012" n="HIAT:w" s="T1091">öʔlubi</ts>
                  <nts id="Seg_3013" n="HIAT:ip">.</nts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1097" id="Seg_3015" n="sc" s="T1093">
               <ts e="T1097" id="Seg_3017" n="HIAT:u" s="T1093">
                  <ts e="T1094" id="Seg_3019" n="HIAT:w" s="T1093">Dĭgəttə</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_3022" n="HIAT:w" s="T1094">dö</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_3025" n="HIAT:w" s="T1095">šobi</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_3028" n="HIAT:w" s="T1096">onʼiʔ</ts>
                  <nts id="Seg_3029" n="HIAT:ip">.</nts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1100" id="Seg_3031" n="sc" s="T1098">
               <ts e="T1100" id="Seg_3033" n="HIAT:u" s="T1098">
                  <ts e="T1099" id="Seg_3035" n="HIAT:w" s="T1098">Dĭ</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_3038" n="HIAT:w" s="T1099">maːluʔpi</ts>
                  <nts id="Seg_3039" n="HIAT:ip">.</nts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1107" id="Seg_3041" n="sc" s="T1101">
               <ts e="T1107" id="Seg_3043" n="HIAT:u" s="T1101">
                  <ts e="T1102" id="Seg_3045" n="HIAT:w" s="T1101">Dĭgəttə</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3047" n="HIAT:ip">(</nts>
                  <ts e="T1103" id="Seg_3049" n="HIAT:w" s="T1102">dĭ=</ts>
                  <nts id="Seg_3050" n="HIAT:ip">)</nts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_3053" n="HIAT:w" s="T1103">dĭʔnə</ts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_3056" n="HIAT:w" s="T1104">dĭ</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_3059" n="HIAT:w" s="T1105">pʼaŋdəbi</ts>
                  <nts id="Seg_3060" n="HIAT:ip">,</nts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_3063" n="HIAT:w" s="T1106">pʼaŋdəbi</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1113" id="Seg_3066" n="sc" s="T1108">
               <ts e="T1113" id="Seg_3068" n="HIAT:u" s="T1108">
                  <ts e="T1109" id="Seg_3070" n="HIAT:w" s="T1108">Dĭ</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3072" n="HIAT:ip">(</nts>
                  <ts e="T1110" id="Seg_3074" n="HIAT:w" s="T1109">ej=</ts>
                  <nts id="Seg_3075" n="HIAT:ip">)</nts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_3078" n="HIAT:w" s="T1110">üge</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_3081" n="HIAT:w" s="T1111">ej</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_3084" n="HIAT:w" s="T1112">šolia</ts>
                  <nts id="Seg_3085" n="HIAT:ip">.</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1117" id="Seg_3087" n="sc" s="T1114">
               <ts e="T1117" id="Seg_3089" n="HIAT:u" s="T1114">
                  <ts e="T1115" id="Seg_3091" n="HIAT:w" s="T1114">Dĭgəttə</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_3094" n="HIAT:w" s="T1115">dĭ</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1523" id="Seg_3097" n="HIAT:w" s="T1116">kalla</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_3100" n="HIAT:w" s="T1523">dʼürbi</ts>
                  <nts id="Seg_3101" n="HIAT:ip">.</nts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1124" id="Seg_3103" n="sc" s="T1118">
               <ts e="T1124" id="Seg_3105" n="HIAT:u" s="T1118">
                  <nts id="Seg_3106" n="HIAT:ip">(</nts>
                  <ts e="T1119" id="Seg_3108" n="HIAT:w" s="T1118">Gil-</ts>
                  <nts id="Seg_3109" n="HIAT:ip">)</nts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_3112" n="HIAT:w" s="T1119">Gijen</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_3115" n="HIAT:w" s="T1120">Lenin</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_3118" n="HIAT:w" s="T1121">amnobi</ts>
                  <nts id="Seg_3119" n="HIAT:ip">,</nts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_3122" n="HIAT:w" s="T1122">dĭ</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_3125" n="HIAT:w" s="T1123">turanə</ts>
                  <nts id="Seg_3126" n="HIAT:ip">.</nts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1139" id="Seg_3128" n="sc" s="T1125">
               <ts e="T1139" id="Seg_3130" n="HIAT:u" s="T1125">
                  <ts e="T1126" id="Seg_3132" n="HIAT:w" s="T1125">Dĭn</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_3135" n="HIAT:w" s="T1126">stălovăjăn</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3137" n="HIAT:ip">(</nts>
                  <ts e="T1128" id="Seg_3139" n="HIAT:w" s="T1127">tob-</ts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_3142" n="HIAT:w" s="T1128">tomn-</ts>
                  <nts id="Seg_3143" n="HIAT:ip">)</nts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_3146" n="HIAT:w" s="T1129">stălovăjăn</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3148" n="HIAT:ip">(</nts>
                  <nts id="Seg_3149" n="HIAT:ip">(</nts>
                  <ats e="T1131" id="Seg_3150" n="HIAT:non-pho" s="T1130">…</ats>
                  <nts id="Seg_3151" n="HIAT:ip">)</nts>
                  <nts id="Seg_3152" n="HIAT:ip">)</nts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_3155" n="HIAT:w" s="T1131">ipek</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_3158" n="HIAT:w" s="T1132">mĭlie</ts>
                  <nts id="Seg_3159" n="HIAT:ip">,</nts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_3162" n="HIAT:w" s="T1133">ara</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_3165" n="HIAT:w" s="T1134">mĭlie</ts>
                  <nts id="Seg_3166" n="HIAT:ip">,</nts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1136" id="Seg_3169" n="HIAT:w" s="T1135">bar</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_3172" n="HIAT:w" s="T1136">ĭmbi</ts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_3175" n="HIAT:w" s="T1137">mĭlie</ts>
                  <nts id="Seg_3176" n="HIAT:ip">,</nts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_3179" n="HIAT:w" s="T1138">pʼaŋdlaʔbə</ts>
                  <nts id="Seg_3180" n="HIAT:ip">.</nts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1148" id="Seg_3182" n="sc" s="T1140">
               <ts e="T1148" id="Seg_3184" n="HIAT:u" s="T1140">
                  <nts id="Seg_3185" n="HIAT:ip">(</nts>
                  <ts e="T1141" id="Seg_3187" n="HIAT:w" s="T1140">Bʼeʔ</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1142" id="Seg_3190" n="HIAT:w" s="T1141">sumn-</ts>
                  <nts id="Seg_3191" n="HIAT:ip">)</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1143" id="Seg_3194" n="HIAT:w" s="T1142">Bʼeʔ</ts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_3197" n="HIAT:w" s="T1143">nagur</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_3200" n="HIAT:w" s="T1144">i</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_3203" n="HIAT:w" s="T1145">sumna</ts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1147" id="Seg_3206" n="HIAT:w" s="T1146">aktʼa</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_3209" n="HIAT:w" s="T1147">iliet</ts>
                  <nts id="Seg_3210" n="HIAT:ip">.</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1151" id="Seg_3212" n="sc" s="T1149">
               <ts e="T1151" id="Seg_3214" n="HIAT:u" s="T1149">
                  <ts e="T1150" id="Seg_3216" n="HIAT:w" s="T1149">По-русски</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_3219" n="HIAT:w" s="T1150">можно</ts>
                  <nts id="Seg_3220" n="HIAT:ip">.</nts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1180" id="Seg_3222" n="sc" s="T1165">
               <ts e="T1180" id="Seg_3224" n="HIAT:u" s="T1165">
                  <ts e="T1166" id="Seg_3226" n="HIAT:w" s="T1165">Моя</ts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_3229" n="HIAT:w" s="T1166">племянница</ts>
                  <nts id="Seg_3230" n="HIAT:ip">,</nts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_3233" n="HIAT:w" s="T1167">была</ts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1169" id="Seg_3236" n="HIAT:w" s="T1168">я</ts>
                  <nts id="Seg_3237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_3239" n="HIAT:w" s="T1169">в</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3241" n="HIAT:ip">(</nts>
                  <ts e="T1171" id="Seg_3243" n="HIAT:w" s="T1170">Красноярскем</ts>
                  <nts id="Seg_3244" n="HIAT:ip">)</nts>
                  <nts id="Seg_3245" n="HIAT:ip">,</nts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_3248" n="HIAT:w" s="T1171">она</ts>
                  <nts id="Seg_3249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1174" id="Seg_3251" n="HIAT:w" s="T1172">говорит:</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3253" n="HIAT:ip">"</nts>
                  <ts e="T1175" id="Seg_3255" n="HIAT:w" s="T1174">Пишет</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_3258" n="HIAT:w" s="T1175">мне</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1177" id="Seg_3261" n="HIAT:w" s="T1176">парень</ts>
                  <nts id="Seg_3262" n="HIAT:ip">,</nts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_3265" n="HIAT:w" s="T1177">чтобы</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1179" id="Seg_3268" n="HIAT:w" s="T1178">я</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_3271" n="HIAT:w" s="T1179">поехала</ts>
                  <nts id="Seg_3272" n="HIAT:ip">.</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1195" id="Seg_3274" n="sc" s="T1181">
               <ts e="T1195" id="Seg_3276" n="HIAT:u" s="T1181">
                  <ts e="T1182" id="Seg_3278" n="HIAT:w" s="T1181">Да</ts>
                  <nts id="Seg_3279" n="HIAT:ip">,</nts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_3282" n="HIAT:w" s="T1182">говорит</ts>
                  <nts id="Seg_3283" n="HIAT:ip">,</nts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_3286" n="HIAT:w" s="T1183">отцу</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_3289" n="HIAT:w" s="T1184">не</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_3292" n="HIAT:w" s="T1185">сказывай</ts>
                  <nts id="Seg_3293" n="HIAT:ip">"</nts>
                  <nts id="Seg_3294" n="HIAT:ip">,</nts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1187" id="Seg_3297" n="HIAT:w" s="T1186">а</ts>
                  <nts id="Seg_3298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_3300" n="HIAT:w" s="T1187">я</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3302" n="HIAT:ip">(</nts>
                  <ts e="T1189" id="Seg_3304" n="HIAT:w" s="T1188">ему</ts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1190" id="Seg_3307" n="HIAT:w" s="T1189">сказала=</ts>
                  <nts id="Seg_3308" n="HIAT:ip">)</nts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_3311" n="HIAT:w" s="T1190">ей</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_3314" n="HIAT:w" s="T1191">сказала:</ts>
                  <nts id="Seg_3315" n="HIAT:ip">"</nts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_3318" n="HIAT:w" s="T1192">Почему</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1194" id="Seg_3321" n="HIAT:w" s="T1193">не</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1195" id="Seg_3324" n="HIAT:w" s="T1194">сказывай</ts>
                  <nts id="Seg_3325" n="HIAT:ip">?</nts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1204" id="Seg_3327" n="sc" s="T1196">
               <ts e="T1204" id="Seg_3329" n="HIAT:u" s="T1196">
                  <ts e="T1197" id="Seg_3331" n="HIAT:w" s="T1196">Отец</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_3334" n="HIAT:w" s="T1197">сам</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_3337" n="HIAT:w" s="T1198">женился</ts>
                  <nts id="Seg_3338" n="HIAT:ip">,</nts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_3341" n="HIAT:w" s="T1199">и</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_3344" n="HIAT:w" s="T1200">вас</ts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_3347" n="HIAT:w" s="T1201">напустил</ts>
                  <nts id="Seg_3348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_3350" n="HIAT:w" s="T1202">на</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_3353" n="HIAT:w" s="T1203">свет</ts>
                  <nts id="Seg_3354" n="HIAT:ip">"</nts>
                  <nts id="Seg_3355" n="HIAT:ip">.</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1214" id="Seg_3357" n="sc" s="T1205">
               <ts e="T1214" id="Seg_3359" n="HIAT:u" s="T1205">
                  <ts e="T1206" id="Seg_3361" n="HIAT:w" s="T1205">Я</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1207" id="Seg_3364" n="HIAT:w" s="T1206">приехала</ts>
                  <nts id="Seg_3365" n="HIAT:ip">,</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1208" id="Seg_3368" n="HIAT:w" s="T1207">отцу</ts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1209" id="Seg_3371" n="HIAT:w" s="T1208">сказала</ts>
                  <nts id="Seg_3372" n="HIAT:ip">,</nts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1210" id="Seg_3375" n="HIAT:w" s="T1209">он</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1211" id="Seg_3378" n="HIAT:w" s="T1210">стал</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1212" id="Seg_3381" n="HIAT:w" s="T1211">ей</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_3384" n="HIAT:w" s="T1212">писать</ts>
                  <nts id="Seg_3385" n="HIAT:ip">,</nts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_3388" n="HIAT:w" s="T1213">разрешил</ts>
                  <nts id="Seg_3389" n="HIAT:ip">.</nts>
                  <nts id="Seg_3390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1229" id="Seg_3391" n="sc" s="T1215">
               <ts e="T1229" id="Seg_3393" n="HIAT:u" s="T1215">
                  <nts id="Seg_3394" n="HIAT:ip">(</nts>
                  <ts e="T1216" id="Seg_3396" n="HIAT:w" s="T1215">И</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1217" id="Seg_3399" n="HIAT:w" s="T1216">он</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_3402" n="HIAT:w" s="T1217">собра-</ts>
                  <nts id="Seg_3403" n="HIAT:ip">)</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1219" id="Seg_3406" n="HIAT:w" s="T1218">Она</ts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1220" id="Seg_3409" n="HIAT:w" s="T1219">ему</ts>
                  <nts id="Seg_3410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_3412" n="HIAT:w" s="T1220">тоже</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_3415" n="HIAT:w" s="T1221">написала</ts>
                  <nts id="Seg_3416" n="HIAT:ip">,</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_3419" n="HIAT:w" s="T1222">он</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_3422" n="HIAT:w" s="T1223">собрался</ts>
                  <nts id="Seg_3423" n="HIAT:ip">,</nts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_3426" n="HIAT:w" s="T1224">приехал</ts>
                  <nts id="Seg_3427" n="HIAT:ip">,</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_3430" n="HIAT:w" s="T1225">приехали</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1227" id="Seg_3433" n="HIAT:w" s="T1226">сюды</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1228" id="Seg_3436" n="HIAT:w" s="T1227">к</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_3439" n="HIAT:w" s="T1228">нам</ts>
                  <nts id="Seg_3440" n="HIAT:ip">.</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1233" id="Seg_3442" n="sc" s="T1230">
               <ts e="T1233" id="Seg_3444" n="HIAT:u" s="T1230">
                  <ts e="T1231" id="Seg_3446" n="HIAT:w" s="T1230">Мы</ts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1232" id="Seg_3449" n="HIAT:w" s="T1231">закололи</ts>
                  <nts id="Seg_3450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1233" id="Seg_3452" n="HIAT:w" s="T1232">свинью</ts>
                  <nts id="Seg_3453" n="HIAT:ip">.</nts>
                  <nts id="Seg_3454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1237" id="Seg_3455" n="sc" s="T1234">
               <ts e="T1237" id="Seg_3457" n="HIAT:u" s="T1234">
                  <ts e="T1235" id="Seg_3459" n="HIAT:w" s="T1234">И</ts>
                  <nts id="Seg_3460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1236" id="Seg_3462" n="HIAT:w" s="T1235">всего</ts>
                  <nts id="Seg_3463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1237" id="Seg_3465" n="HIAT:w" s="T1236">наготовили</ts>
                  <nts id="Seg_3466" n="HIAT:ip">.</nts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1242" id="Seg_3468" n="sc" s="T1238">
               <ts e="T1242" id="Seg_3470" n="HIAT:u" s="T1238">
                  <ts e="T1239" id="Seg_3472" n="HIAT:w" s="T1238">Пельменев</ts>
                  <nts id="Seg_3473" n="HIAT:ip">,</nts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_3476" n="HIAT:w" s="T1239">и</ts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_3479" n="HIAT:w" s="T1240">котлет</ts>
                  <nts id="Seg_3480" n="HIAT:ip">,</nts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_3483" n="HIAT:w" s="T1241">всего</ts>
                  <nts id="Seg_3484" n="HIAT:ip">.</nts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1247" id="Seg_3486" n="sc" s="T1243">
               <ts e="T1247" id="Seg_3488" n="HIAT:u" s="T1243">
                  <ts e="T1244" id="Seg_3490" n="HIAT:w" s="T1243">И</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_3493" n="HIAT:w" s="T1244">много</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_3496" n="HIAT:w" s="T1245">людей</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_3499" n="HIAT:w" s="T1246">было</ts>
                  <nts id="Seg_3500" n="HIAT:ip">.</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1254" id="Seg_3502" n="sc" s="T1248">
               <ts e="T1254" id="Seg_3504" n="HIAT:u" s="T1248">
                  <ts e="T1249" id="Seg_3506" n="HIAT:w" s="T1248">А</ts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1250" id="Seg_3509" n="HIAT:w" s="T1249">потом</ts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1251" id="Seg_3512" n="HIAT:w" s="T1250">я</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1252" id="Seg_3515" n="HIAT:w" s="T1251">купила</ts>
                  <nts id="Seg_3516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1253" id="Seg_3518" n="HIAT:w" s="T1252">ей</ts>
                  <nts id="Seg_3519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1254" id="Seg_3521" n="HIAT:w" s="T1253">пальто</ts>
                  <nts id="Seg_3522" n="HIAT:ip">.</nts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1262" id="Seg_3524" n="sc" s="T1255">
               <ts e="T1262" id="Seg_3526" n="HIAT:u" s="T1255">
                  <ts e="T1256" id="Seg_3528" n="HIAT:w" s="T1255">Тогда</ts>
                  <nts id="Seg_3529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1257" id="Seg_3531" n="HIAT:w" s="T1256">на</ts>
                  <nts id="Seg_3532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1258" id="Seg_3534" n="HIAT:w" s="T1257">дорогу</ts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_3537" n="HIAT:w" s="T1258">дала</ts>
                  <nts id="Seg_3538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_3540" n="HIAT:w" s="T1259">денег</ts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_3543" n="HIAT:w" s="T1260">сто</ts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1262" id="Seg_3546" n="HIAT:w" s="T1261">рублей</ts>
                  <nts id="Seg_3547" n="HIAT:ip">.</nts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1269" id="Seg_3549" n="sc" s="T1263">
               <ts e="T1269" id="Seg_3551" n="HIAT:u" s="T1263">
                  <ts e="T1264" id="Seg_3553" n="HIAT:w" s="T1263">И</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_3556" n="HIAT:w" s="T1264">опять</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1266" id="Seg_3559" n="HIAT:w" s="T1265">она</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3561" n="HIAT:ip">(</nts>
                  <ts e="T1267" id="Seg_3563" n="HIAT:w" s="T1266">та-</ts>
                  <nts id="Seg_3564" n="HIAT:ip">)</nts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1268" id="Seg_3567" n="HIAT:w" s="T1267">туды</ts>
                  <nts id="Seg_3568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_3570" n="HIAT:w" s="T1268">уехала</ts>
                  <nts id="Seg_3571" n="HIAT:ip">.</nts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1278" id="Seg_3573" n="sc" s="T1270">
               <ts e="T1278" id="Seg_3575" n="HIAT:u" s="T1270">
                  <ts e="T1271" id="Seg_3577" n="HIAT:w" s="T1270">Свекровка</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1272" id="Seg_3580" n="HIAT:w" s="T1271">ее</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_3583" n="HIAT:w" s="T1272">не</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_3586" n="HIAT:w" s="T1273">залюбила</ts>
                  <nts id="Seg_3587" n="HIAT:ip">,</nts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1275" id="Seg_3590" n="HIAT:w" s="T1274">она</ts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1276" id="Seg_3593" n="HIAT:w" s="T1275">пишет</ts>
                  <nts id="Seg_3594" n="HIAT:ip">,</nts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1277" id="Seg_3597" n="HIAT:w" s="T1276">что</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1278" id="Seg_3600" n="HIAT:w" s="T1277">плачу</ts>
                  <nts id="Seg_3601" n="HIAT:ip">.</nts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1289" id="Seg_3603" n="sc" s="T1279">
               <ts e="T1289" id="Seg_3605" n="HIAT:u" s="T1279">
                  <nts id="Seg_3606" n="HIAT:ip">(</nts>
                  <ts e="T1280" id="Seg_3608" n="HIAT:w" s="T1279">А=</ts>
                  <nts id="Seg_3609" n="HIAT:ip">)</nts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_3612" n="HIAT:w" s="T1280">А</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1282" id="Seg_3615" n="HIAT:w" s="T1281">мы</ts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_3618" n="HIAT:w" s="T1282">написали</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1285" id="Seg_3621" n="HIAT:w" s="T1283">ей:</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3623" n="HIAT:ip">"</nts>
                  <ts e="T1286" id="Seg_3625" n="HIAT:w" s="T1285">Чем</ts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1287" id="Seg_3628" n="HIAT:w" s="T1286">плакать</ts>
                  <nts id="Seg_3629" n="HIAT:ip">,</nts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1288" id="Seg_3632" n="HIAT:w" s="T1287">приезжай</ts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1289" id="Seg_3635" n="HIAT:w" s="T1288">сюды</ts>
                  <nts id="Seg_3636" n="HIAT:ip">"</nts>
                  <nts id="Seg_3637" n="HIAT:ip">.</nts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1295" id="Seg_3639" n="sc" s="T1290">
               <ts e="T1295" id="Seg_3641" n="HIAT:u" s="T1290">
                  <ts e="T1291" id="Seg_3643" n="HIAT:w" s="T1290">Ну</ts>
                  <nts id="Seg_3644" n="HIAT:ip">,</nts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1292" id="Seg_3647" n="HIAT:w" s="T1291">она</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_3650" n="HIAT:w" s="T1292">говорила</ts>
                  <nts id="Seg_3651" n="HIAT:ip">,</nts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_3654" n="HIAT:w" s="T1293">вдвоем</ts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1295" id="Seg_3657" n="HIAT:w" s="T1294">приедут</ts>
                  <nts id="Seg_3658" n="HIAT:ip">.</nts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1306" id="Seg_3660" n="sc" s="T1296">
               <ts e="T1306" id="Seg_3662" n="HIAT:u" s="T1296">
                  <ts e="T1297" id="Seg_3664" n="HIAT:w" s="T1296">Ну</ts>
                  <nts id="Seg_3665" n="HIAT:ip">,</nts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1298" id="Seg_3668" n="HIAT:w" s="T1297">а</ts>
                  <nts id="Seg_3669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1299" id="Seg_3671" n="HIAT:w" s="T1298">потом</ts>
                  <nts id="Seg_3672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_3674" n="HIAT:w" s="T1299">говорит:</ts>
                  <nts id="Seg_3675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3676" n="HIAT:ip">"</nts>
                  <ts e="T1301" id="Seg_3678" n="HIAT:w" s="T1300">Я</ts>
                  <nts id="Seg_3679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1302" id="Seg_3681" n="HIAT:w" s="T1301">одна</ts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1303" id="Seg_3684" n="HIAT:w" s="T1302">приеду</ts>
                  <nts id="Seg_3685" n="HIAT:ip">"</nts>
                  <nts id="Seg_3686" n="HIAT:ip">,</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3688" n="HIAT:ip">(</nts>
                  <ts e="T1304" id="Seg_3690" n="HIAT:w" s="T1303">на-</ts>
                  <nts id="Seg_3691" n="HIAT:ip">)</nts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1305" id="Seg_3694" n="HIAT:w" s="T1304">в</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1306" id="Seg_3697" n="HIAT:w" s="T1305">письме</ts>
                  <nts id="Seg_3698" n="HIAT:ip">.</nts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1313" id="Seg_3700" n="sc" s="T1307">
               <ts e="T1313" id="Seg_3702" n="HIAT:u" s="T1307">
                  <ts e="T1308" id="Seg_3704" n="HIAT:w" s="T1307">А</ts>
                  <nts id="Seg_3705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1309" id="Seg_3707" n="HIAT:w" s="T1308">мы</ts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1310" id="Seg_3710" n="HIAT:w" s="T1309">говорим:</ts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3712" n="HIAT:ip">"</nts>
                  <ts e="T1311" id="Seg_3714" n="HIAT:w" s="T1310">Почему</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1312" id="Seg_3717" n="HIAT:w" s="T1311">одна</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1313" id="Seg_3720" n="HIAT:w" s="T1312">приедешь</ts>
                  <nts id="Seg_3721" n="HIAT:ip">?</nts>
                  <nts id="Seg_3722" n="HIAT:ip">"</nts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1326" id="Seg_3724" n="sc" s="T1314">
               <ts e="T1318" id="Seg_3726" n="HIAT:u" s="T1314">
                  <nts id="Seg_3727" n="HIAT:ip">"</nts>
                  <ts e="T1315" id="Seg_3729" n="HIAT:w" s="T1314">Ну</ts>
                  <nts id="Seg_3730" n="HIAT:ip">,</nts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1316" id="Seg_3733" n="HIAT:w" s="T1315">говорит</ts>
                  <nts id="Seg_3734" n="HIAT:ip">,</nts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1317" id="Seg_3737" n="HIAT:w" s="T1316">денег</ts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1318" id="Seg_3740" n="HIAT:w" s="T1317">нету</ts>
                  <nts id="Seg_3741" n="HIAT:ip">"</nts>
                  <nts id="Seg_3742" n="HIAT:ip">.</nts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1326" id="Seg_3745" n="HIAT:u" s="T1318">
                  <ts e="T1319" id="Seg_3747" n="HIAT:w" s="T1318">Я</ts>
                  <nts id="Seg_3748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1320" id="Seg_3750" n="HIAT:w" s="T1319">опять</ts>
                  <nts id="Seg_3751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_3753" n="HIAT:w" s="T1320">сто</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_3756" n="HIAT:w" s="T1321">рублей</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_3759" n="HIAT:w" s="T1322">послала</ts>
                  <nts id="Seg_3760" n="HIAT:ip">,</nts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1324" id="Seg_3763" n="HIAT:w" s="T1323">и</ts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_3766" n="HIAT:w" s="T1324">отец</ts>
                  <nts id="Seg_3767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1326" id="Seg_3769" n="HIAT:w" s="T1325">пятьдесят</ts>
                  <nts id="Seg_3770" n="HIAT:ip">.</nts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1331" id="Seg_3772" n="sc" s="T1327">
               <ts e="T1331" id="Seg_3774" n="HIAT:u" s="T1327">
                  <ts e="T1328" id="Seg_3776" n="HIAT:w" s="T1327">Ну</ts>
                  <nts id="Seg_3777" n="HIAT:ip">,</nts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_3780" n="HIAT:w" s="T1328">она</ts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1330" id="Seg_3783" n="HIAT:w" s="T1329">приехала</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1331" id="Seg_3786" n="HIAT:w" s="T1330">одна</ts>
                  <nts id="Seg_3787" n="HIAT:ip">.</nts>
                  <nts id="Seg_3788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1335" id="Seg_3789" n="sc" s="T1332">
               <ts e="T1335" id="Seg_3791" n="HIAT:u" s="T1332">
                  <ts e="T1333" id="Seg_3793" n="HIAT:w" s="T1332">А</ts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1334" id="Seg_3796" n="HIAT:w" s="T1333">он</ts>
                  <nts id="Seg_3797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1335" id="Seg_3799" n="HIAT:w" s="T1334">остался</ts>
                  <nts id="Seg_3800" n="HIAT:ip">.</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1341" id="Seg_3802" n="sc" s="T1336">
               <ts e="T1341" id="Seg_3804" n="HIAT:u" s="T1336">
                  <ts e="T1337" id="Seg_3806" n="HIAT:w" s="T1336">Она</ts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1338" id="Seg_3809" n="HIAT:w" s="T1337">опять</ts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1339" id="Seg_3812" n="HIAT:w" s="T1338">ему</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1340" id="Seg_3815" n="HIAT:w" s="T1339">письмо</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1341" id="Seg_3818" n="HIAT:w" s="T1340">писала</ts>
                  <nts id="Seg_3819" n="HIAT:ip">.</nts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1347" id="Seg_3821" n="sc" s="T1342">
               <ts e="T1347" id="Seg_3823" n="HIAT:u" s="T1342">
                  <ts e="T1343" id="Seg_3825" n="HIAT:w" s="T1342">Не</ts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_3828" n="HIAT:w" s="T1343">знаю</ts>
                  <nts id="Seg_3829" n="HIAT:ip">,</nts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1345" id="Seg_3832" n="HIAT:w" s="T1344">приедет</ts>
                  <nts id="Seg_3833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1346" id="Seg_3835" n="HIAT:w" s="T1345">ли</ts>
                  <nts id="Seg_3836" n="HIAT:ip">,</nts>
                  <nts id="Seg_3837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1347" id="Seg_3839" n="HIAT:w" s="T1346">нет</ts>
                  <nts id="Seg_3840" n="HIAT:ip">.</nts>
                  <nts id="Seg_3841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1364" id="Seg_3842" n="sc" s="T1348">
               <ts e="T1364" id="Seg_3844" n="HIAT:u" s="T1348">
                  <ts e="T1349" id="Seg_3846" n="HIAT:w" s="T1348">А</ts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1350" id="Seg_3849" n="HIAT:w" s="T1349">потом</ts>
                  <nts id="Seg_3850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_3852" n="HIAT:w" s="T1350">она</ts>
                  <nts id="Seg_3853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1352" id="Seg_3855" n="HIAT:w" s="T1351">уехала</ts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1353" id="Seg_3858" n="HIAT:w" s="T1352">туды</ts>
                  <nts id="Seg_3859" n="HIAT:ip">,</nts>
                  <nts id="Seg_3860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_3862" n="HIAT:w" s="T1353">где</ts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_3865" n="HIAT:w" s="T1354">Ленин</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1356" id="Seg_3868" n="HIAT:w" s="T1355">был</ts>
                  <nts id="Seg_3869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1357" id="Seg_3871" n="HIAT:w" s="T1356">в</ts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1358" id="Seg_3874" n="HIAT:w" s="T1357">ссылке</ts>
                  <nts id="Seg_3875" n="HIAT:ip">,</nts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3877" n="HIAT:ip">(</nts>
                  <ts e="T1359" id="Seg_3879" n="HIAT:w" s="T1358">в</ts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_3882" n="HIAT:w" s="T1359">этой</ts>
                  <nts id="Seg_3883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_3885" n="HIAT:w" s="T1360">д-</ts>
                  <nts id="Seg_3886" n="HIAT:ip">)</nts>
                  <nts id="Seg_3887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1362" id="Seg_3889" n="HIAT:w" s="T1361">в</ts>
                  <nts id="Seg_3890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1363" id="Seg_3892" n="HIAT:w" s="T1362">этот</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_3895" n="HIAT:w" s="T1363">город</ts>
                  <nts id="Seg_3896" n="HIAT:ip">.</nts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1370" id="Seg_3898" n="sc" s="T1365">
               <ts e="T1370" id="Seg_3900" n="HIAT:u" s="T1365">
                  <ts e="T1366" id="Seg_3902" n="HIAT:w" s="T1365">И</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1367" id="Seg_3905" n="HIAT:w" s="T1366">там</ts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_3908" n="HIAT:w" s="T1367">в</ts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1369" id="Seg_3911" n="HIAT:w" s="T1368">столовой</ts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1370" id="Seg_3914" n="HIAT:w" s="T1369">работает</ts>
                  <nts id="Seg_3915" n="HIAT:ip">.</nts>
                  <nts id="Seg_3916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1379" id="Seg_3917" n="sc" s="T1371">
               <ts e="T1379" id="Seg_3919" n="HIAT:u" s="T1371">
                  <ts e="T1372" id="Seg_3921" n="HIAT:w" s="T1371">И</ts>
                  <nts id="Seg_3922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3923" n="HIAT:ip">(</nts>
                  <ts e="T1373" id="Seg_3925" n="HIAT:w" s="T1372">это=</ts>
                  <nts id="Seg_3926" n="HIAT:ip">)</nts>
                  <nts id="Seg_3927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1374" id="Seg_3929" n="HIAT:w" s="T1373">выдает</ts>
                  <nts id="Seg_3930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1375" id="Seg_3932" n="HIAT:w" s="T1374">там</ts>
                  <nts id="Seg_3933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1376" id="Seg_3935" n="HIAT:w" s="T1375">водки</ts>
                  <nts id="Seg_3936" n="HIAT:ip">,</nts>
                  <nts id="Seg_3937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1377" id="Seg_3939" n="HIAT:w" s="T1376">всего</ts>
                  <nts id="Seg_3940" n="HIAT:ip">,</nts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1378" id="Seg_3943" n="HIAT:w" s="T1377">чего</ts>
                  <nts id="Seg_3944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1379" id="Seg_3946" n="HIAT:w" s="T1378">кушать</ts>
                  <nts id="Seg_3947" n="HIAT:ip">.</nts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1381" id="Seg_3949" n="sc" s="T1380">
               <ts e="T1381" id="Seg_3951" n="HIAT:u" s="T1380">
                  <ts e="T1381" id="Seg_3953" n="HIAT:w" s="T1380">Всё</ts>
                  <nts id="Seg_3954" n="HIAT:ip">.</nts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1390" id="Seg_3956" n="sc" s="T1382">
               <ts e="T1383" id="Seg_3958" n="HIAT:u" s="T1382">
                  <nts id="Seg_3959" n="HIAT:ip">(</nts>
                  <nts id="Seg_3960" n="HIAT:ip">(</nts>
                  <ats e="T1383" id="Seg_3961" n="HIAT:non-pho" s="T1382">BRK</ats>
                  <nts id="Seg_3962" n="HIAT:ip">)</nts>
                  <nts id="Seg_3963" n="HIAT:ip">)</nts>
                  <nts id="Seg_3964" n="HIAT:ip">.</nts>
                  <nts id="Seg_3965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1390" id="Seg_3967" n="HIAT:u" s="T1383">
                  <ts e="T1384" id="Seg_3969" n="HIAT:w" s="T1383">Погодите</ts>
                  <nts id="Seg_3970" n="HIAT:ip">,</nts>
                  <nts id="Seg_3971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_3973" n="HIAT:w" s="T1384">я</ts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_3976" n="HIAT:w" s="T1385">может</ts>
                  <nts id="Seg_3977" n="HIAT:ip">,</nts>
                  <nts id="Seg_3978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3979" n="HIAT:ip">(</nts>
                  <ts e="T1387" id="Seg_3981" n="HIAT:w" s="T1386">на-</ts>
                  <nts id="Seg_3982" n="HIAT:ip">)</nts>
                  <nts id="Seg_3983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_3985" n="HIAT:w" s="T1387">на</ts>
                  <nts id="Seg_3986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1390" id="Seg_3988" n="HIAT:w" s="T1388">это</ts>
                  <nts id="Seg_3989" n="HIAT:ip">…</nts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1396" id="Seg_3991" n="sc" s="T1391">
               <ts e="T1396" id="Seg_3993" n="HIAT:u" s="T1391">
                  <ts e="T1392" id="Seg_3995" n="HIAT:w" s="T1391">Măn</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1393" id="Seg_3998" n="HIAT:w" s="T1392">plʼemʼannican</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_4001" n="HIAT:w" s="T1393">koʔbdot</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_4004" n="HIAT:w" s="T1394">tibinə</ts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_4007" n="HIAT:w" s="T1395">kalla</ts>
                  <nts id="Seg_4008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1396" id="Seg_4010" n="HIAT:w" s="T1524">dʼürbi</ts>
                  <nts id="Seg_4011" n="HIAT:ip">.</nts>
                  <nts id="Seg_4012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1402" id="Seg_4013" n="sc" s="T1397">
               <ts e="T1402" id="Seg_4015" n="HIAT:u" s="T1397">
                  <ts e="T1398" id="Seg_4017" n="HIAT:w" s="T1397">I</ts>
                  <nts id="Seg_4018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_4020" n="HIAT:w" s="T1398">dĭn</ts>
                  <nts id="Seg_4021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_4023" n="HIAT:w" s="T1399">sumna</ts>
                  <nts id="Seg_4024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_4026" n="HIAT:w" s="T1400">esseŋ</ts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_4029" n="HIAT:w" s="T1401">ibiʔi</ts>
                  <nts id="Seg_4030" n="HIAT:ip">.</nts>
                  <nts id="Seg_4031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1410" id="Seg_4032" n="sc" s="T1403">
               <ts e="T1410" id="Seg_4034" n="HIAT:u" s="T1403">
                  <ts e="T1404" id="Seg_4036" n="HIAT:w" s="T1403">Dĭgəttə</ts>
                  <nts id="Seg_4037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4038" n="HIAT:ip">(</nts>
                  <ts e="T1405" id="Seg_4040" n="HIAT:w" s="T1404">dĭ=</ts>
                  <nts id="Seg_4041" n="HIAT:ip">)</nts>
                  <nts id="Seg_4042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1406" id="Seg_4044" n="HIAT:w" s="T1405">dĭzeŋ</ts>
                  <nts id="Seg_4045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1407" id="Seg_4047" n="HIAT:w" s="T1406">kambiʔi</ts>
                  <nts id="Seg_4048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4049" n="HIAT:ip">(</nts>
                  <ts e="T1408" id="Seg_4051" n="HIAT:w" s="T1407">maʔ=</ts>
                  <nts id="Seg_4052" n="HIAT:ip">)</nts>
                  <nts id="Seg_4053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1409" id="Seg_4055" n="HIAT:w" s="T1408">maːʔnə</ts>
                  <nts id="Seg_4056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_4058" n="HIAT:w" s="T1409">dĭn</ts>
                  <nts id="Seg_4059" n="HIAT:ip">.</nts>
                  <nts id="Seg_4060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1414" id="Seg_4061" n="sc" s="T1411">
               <ts e="T1414" id="Seg_4063" n="HIAT:u" s="T1411">
                  <ts e="T1412" id="Seg_4065" n="HIAT:w" s="T1411">I</ts>
                  <nts id="Seg_4066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1413" id="Seg_4068" n="HIAT:w" s="T1412">ige</ts>
                  <nts id="Seg_4069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1414" id="Seg_4071" n="HIAT:w" s="T1413">turajʔi</ts>
                  <nts id="Seg_4072" n="HIAT:ip">.</nts>
                  <nts id="Seg_4073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1422" id="Seg_4074" n="sc" s="T1415">
               <ts e="T1422" id="Seg_4076" n="HIAT:u" s="T1415">
                  <ts e="T1416" id="Seg_4078" n="HIAT:w" s="T1415">Dĭgəttə</ts>
                  <nts id="Seg_4079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_4081" n="HIAT:w" s="T1416">dĭzeŋ</ts>
                  <nts id="Seg_4082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_4084" n="HIAT:w" s="T1417">aʔtʼi</ts>
                  <nts id="Seg_4085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1419" id="Seg_4087" n="HIAT:w" s="T1418">abiʔi</ts>
                  <nts id="Seg_4088" n="HIAT:ip">,</nts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1420" id="Seg_4091" n="HIAT:w" s="T1419">gijen</ts>
                  <nts id="Seg_4092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_4094" n="HIAT:w" s="T1420">bazaj</ts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1422" id="Seg_4097" n="HIAT:w" s="T1421">aktʼi</ts>
                  <nts id="Seg_4098" n="HIAT:ip">.</nts>
                  <nts id="Seg_4099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1433" id="Seg_4100" n="sc" s="T1423">
               <ts e="T1433" id="Seg_4102" n="HIAT:u" s="T1423">
                  <ts e="T1424" id="Seg_4104" n="HIAT:w" s="T1423">Dĭgəttə</ts>
                  <nts id="Seg_4105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_4107" n="HIAT:w" s="T1424">kambiʔi</ts>
                  <nts id="Seg_4108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4109" n="HIAT:ip">(</nts>
                  <ts e="T1426" id="Seg_4111" n="HIAT:w" s="T1425">dĭ=</ts>
                  <nts id="Seg_4112" n="HIAT:ip">)</nts>
                  <nts id="Seg_4113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1427" id="Seg_4115" n="HIAT:w" s="T1426">dĭbər</ts>
                  <nts id="Seg_4116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1428" id="Seg_4118" n="HIAT:w" s="T1427">sʼevʼerdə</ts>
                  <nts id="Seg_4119" n="HIAT:ip">,</nts>
                  <nts id="Seg_4120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_4122" n="HIAT:w" s="T1428">dĭn</ts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1430" id="Seg_4125" n="HIAT:w" s="T1429">tože</ts>
                  <nts id="Seg_4126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1431" id="Seg_4128" n="HIAT:w" s="T1430">abiʔi</ts>
                  <nts id="Seg_4129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4130" n="HIAT:ip">(</nts>
                  <ts e="T1432" id="Seg_4132" n="HIAT:w" s="T1431">aktʼi=</ts>
                  <nts id="Seg_4133" n="HIAT:ip">)</nts>
                  <nts id="Seg_4134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1433" id="Seg_4136" n="HIAT:w" s="T1432">aktʼi</ts>
                  <nts id="Seg_4137" n="HIAT:ip">.</nts>
                  <nts id="Seg_4138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1436" id="Seg_4139" n="sc" s="T1434">
               <ts e="T1436" id="Seg_4141" n="HIAT:u" s="T1434">
                  <ts e="T1435" id="Seg_4143" n="HIAT:w" s="T1434">Bazaj</ts>
                  <nts id="Seg_4144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_4146" n="HIAT:w" s="T1435">aktʼi</ts>
                  <nts id="Seg_4147" n="HIAT:ip">.</nts>
                  <nts id="Seg_4148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1442" id="Seg_4149" n="sc" s="T1437">
               <ts e="T1442" id="Seg_4151" n="HIAT:u" s="T1437">
                  <ts e="T1438" id="Seg_4153" n="HIAT:w" s="T1437">Dĭgəttə</ts>
                  <nts id="Seg_4154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_4156" n="HIAT:w" s="T1438">dĭ</ts>
                  <nts id="Seg_4157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1440" id="Seg_4159" n="HIAT:w" s="T1439">šobi</ts>
                  <nts id="Seg_4160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1441" id="Seg_4162" n="HIAT:w" s="T1440">maːʔndə</ts>
                  <nts id="Seg_4163" n="HIAT:ip">,</nts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_4166" n="HIAT:w" s="T1441">ianə</ts>
                  <nts id="Seg_4167" n="HIAT:ip">.</nts>
                  <nts id="Seg_4168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1448" id="Seg_4169" n="sc" s="T1443">
               <ts e="T1448" id="Seg_4171" n="HIAT:u" s="T1443">
                  <nts id="Seg_4172" n="HIAT:ip">(</nts>
                  <ts e="T1444" id="Seg_4174" n="HIAT:w" s="T1443">Dĭ=</ts>
                  <nts id="Seg_4175" n="HIAT:ip">)</nts>
                  <nts id="Seg_4176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_4178" n="HIAT:w" s="T1444">Dĭgəttə</ts>
                  <nts id="Seg_4179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1446" id="Seg_4181" n="HIAT:w" s="T1445">dĭ</ts>
                  <nts id="Seg_4182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_4184" n="HIAT:w" s="T1446">dön</ts>
                  <nts id="Seg_4185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1448" id="Seg_4187" n="HIAT:w" s="T1447">ibi</ts>
                  <nts id="Seg_4188" n="HIAT:ip">.</nts>
                  <nts id="Seg_4189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1457" id="Seg_4190" n="sc" s="T1449">
               <ts e="T1457" id="Seg_4192" n="HIAT:u" s="T1449">
                  <ts e="T1450" id="Seg_4194" n="HIAT:w" s="T1449">Dĭgəttə</ts>
                  <nts id="Seg_4195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1451" id="Seg_4197" n="HIAT:w" s="T1450">sazən</ts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1453" id="Seg_4200" n="HIAT:w" s="T1451">pʼaŋdəbiʔi:</ts>
                  <nts id="Seg_4201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4202" n="HIAT:ip">"</nts>
                  <ts e="T1454" id="Seg_4204" n="HIAT:w" s="T1453">Tăn</ts>
                  <nts id="Seg_4205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1455" id="Seg_4207" n="HIAT:w" s="T1454">tibi</ts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1456" id="Seg_4210" n="HIAT:w" s="T1455">bügən</ts>
                  <nts id="Seg_4211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_4213" n="HIAT:w" s="T1456">külambi</ts>
                  <nts id="Seg_4214" n="HIAT:ip">"</nts>
                  <nts id="Seg_4215" n="HIAT:ip">.</nts>
                  <nts id="Seg_4216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1461" id="Seg_4217" n="sc" s="T1458">
               <ts e="T1461" id="Seg_4219" n="HIAT:u" s="T1458">
                  <ts e="T1459" id="Seg_4221" n="HIAT:w" s="T1458">Dĭgəttə</ts>
                  <nts id="Seg_4222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1460" id="Seg_4224" n="HIAT:w" s="T1459">dĭ</ts>
                  <nts id="Seg_4225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1461" id="Seg_4227" n="HIAT:w" s="T1460">kambi</ts>
                  <nts id="Seg_4228" n="HIAT:ip">.</nts>
                  <nts id="Seg_4229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1465" id="Seg_4230" n="sc" s="T1462">
               <ts e="T1465" id="Seg_4232" n="HIAT:u" s="T1462">
                  <ts e="T1463" id="Seg_4234" n="HIAT:w" s="T1462">Dĭm</ts>
                  <nts id="Seg_4235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1464" id="Seg_4237" n="HIAT:w" s="T1463">dʼünə</ts>
                  <nts id="Seg_4238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1465" id="Seg_4240" n="HIAT:w" s="T1464">embiʔi</ts>
                  <nts id="Seg_4241" n="HIAT:ip">.</nts>
                  <nts id="Seg_4242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1471" id="Seg_4243" n="sc" s="T1466">
               <ts e="T1471" id="Seg_4245" n="HIAT:u" s="T1466">
                  <ts e="T1467" id="Seg_4247" n="HIAT:w" s="T1466">Dĭgəttə</ts>
                  <nts id="Seg_4248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1468" id="Seg_4250" n="HIAT:w" s="T1467">bazo</ts>
                  <nts id="Seg_4251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1469" id="Seg_4253" n="HIAT:w" s="T1468">dĭʔnə</ts>
                  <nts id="Seg_4254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1470" id="Seg_4256" n="HIAT:w" s="T1469">kuza</ts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471" id="Seg_4259" n="HIAT:w" s="T1470">ibi</ts>
                  <nts id="Seg_4260" n="HIAT:ip">.</nts>
                  <nts id="Seg_4261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1475" id="Seg_4262" n="sc" s="T1472">
               <ts e="T1475" id="Seg_4264" n="HIAT:u" s="T1472">
                  <ts e="T1473" id="Seg_4266" n="HIAT:w" s="T1472">Dĭn</ts>
                  <nts id="Seg_4267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1474" id="Seg_4269" n="HIAT:w" s="T1473">nagobi</ts>
                  <nts id="Seg_4270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1475" id="Seg_4272" n="HIAT:w" s="T1474">net</ts>
                  <nts id="Seg_4273" n="HIAT:ip">.</nts>
                  <nts id="Seg_4274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1484" id="Seg_4275" n="sc" s="T1476">
               <ts e="T1484" id="Seg_4277" n="HIAT:u" s="T1476">
                  <ts e="T1477" id="Seg_4279" n="HIAT:w" s="T1476">I</ts>
                  <nts id="Seg_4280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1478" id="Seg_4282" n="HIAT:w" s="T1477">tüj</ts>
                  <nts id="Seg_4283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1479" id="Seg_4285" n="HIAT:w" s="T1478">amnolaʔbəʔjə</ts>
                  <nts id="Seg_4286" n="HIAT:ip">,</nts>
                  <nts id="Seg_4287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1480" id="Seg_4289" n="HIAT:w" s="T1479">i</ts>
                  <nts id="Seg_4290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1481" id="Seg_4292" n="HIAT:w" s="T1480">dĭn</ts>
                  <nts id="Seg_4293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1482" id="Seg_4295" n="HIAT:w" s="T1481">sumna</ts>
                  <nts id="Seg_4296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1483" id="Seg_4298" n="HIAT:w" s="T1482">esseŋdə</ts>
                  <nts id="Seg_4299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1484" id="Seg_4301" n="HIAT:w" s="T1483">ibiʔi</ts>
                  <nts id="Seg_4302" n="HIAT:ip">.</nts>
                  <nts id="Seg_4303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1488" id="Seg_4304" n="sc" s="T1485">
               <ts e="T1488" id="Seg_4306" n="HIAT:u" s="T1485">
                  <ts e="T1486" id="Seg_4308" n="HIAT:w" s="T1485">Dĭ</ts>
                  <nts id="Seg_4309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1487" id="Seg_4311" n="HIAT:w" s="T1486">ibi</ts>
                  <nts id="Seg_4312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1488" id="Seg_4314" n="HIAT:w" s="T1487">dĭm</ts>
                  <nts id="Seg_4315" n="HIAT:ip">.</nts>
                  <nts id="Seg_4316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1492" id="Seg_4317" n="sc" s="T1489">
               <ts e="T1492" id="Seg_4319" n="HIAT:u" s="T1489">
                  <ts e="T1490" id="Seg_4321" n="HIAT:w" s="T1489">Ugandə</ts>
                  <nts id="Seg_4322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_4324" n="HIAT:w" s="T1490">jakše</ts>
                  <nts id="Seg_4325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1492" id="Seg_4327" n="HIAT:w" s="T1491">kuza</ts>
                  <nts id="Seg_4328" n="HIAT:ip">.</nts>
                  <nts id="Seg_4329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1508" id="Seg_4330" n="sc" s="T1493">
               <ts e="T1496" id="Seg_4332" n="HIAT:u" s="T1493">
                  <ts e="T1494" id="Seg_4334" n="HIAT:w" s="T1493">Ara</ts>
                  <nts id="Seg_4335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1495" id="Seg_4337" n="HIAT:w" s="T1494">ej</ts>
                  <nts id="Seg_4338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1496" id="Seg_4340" n="HIAT:w" s="T1495">bĭtlie</ts>
                  <nts id="Seg_4341" n="HIAT:ip">.</nts>
                  <nts id="Seg_4342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1501" id="Seg_4344" n="HIAT:u" s="T1496">
                  <ts e="T1497" id="Seg_4346" n="HIAT:w" s="T1496">Gibərdə</ts>
                  <nts id="Seg_4347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1498" id="Seg_4349" n="HIAT:w" s="T1497">ej</ts>
                  <nts id="Seg_4350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1499" id="Seg_4352" n="HIAT:w" s="T1498">mĭnlie</ts>
                  <nts id="Seg_4353" n="HIAT:ip">,</nts>
                  <nts id="Seg_4354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1500" id="Seg_4356" n="HIAT:w" s="T1499">üge</ts>
                  <nts id="Seg_4357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1501" id="Seg_4359" n="HIAT:w" s="T1500">togonorlaʔbə</ts>
                  <nts id="Seg_4360" n="HIAT:ip">.</nts>
                  <nts id="Seg_4361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1508" id="Seg_4363" n="HIAT:u" s="T1501">
                  <nts id="Seg_4364" n="HIAT:ip">(</nts>
                  <ts e="T1502" id="Seg_4366" n="HIAT:w" s="T1501">Dĭn</ts>
                  <nts id="Seg_4367" n="HIAT:ip">)</nts>
                  <nts id="Seg_4368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1503" id="Seg_4370" n="HIAT:w" s="T1502">tože</ts>
                  <nts id="Seg_4371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1504" id="Seg_4373" n="HIAT:w" s="T1503">iat</ts>
                  <nts id="Seg_4374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_4376" n="HIAT:w" s="T1504">naga</ts>
                  <nts id="Seg_4377" n="HIAT:ip">,</nts>
                  <nts id="Seg_4378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1506" id="Seg_4380" n="HIAT:w" s="T1505">tolʼkă</ts>
                  <nts id="Seg_4381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1507" id="Seg_4383" n="HIAT:w" s="T1506">onʼiʔ</ts>
                  <nts id="Seg_4384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1508" id="Seg_4386" n="HIAT:w" s="T1507">abat</ts>
                  <nts id="Seg_4387" n="HIAT:ip">.</nts>
                  <nts id="Seg_4388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1521" id="Seg_4389" n="sc" s="T1516">
               <ts e="T1521" id="Seg_4391" n="HIAT:u" s="T1516">
                  <ts e="T1517" id="Seg_4393" n="HIAT:w" s="T1516">Пускай</ts>
                  <nts id="Seg_4394" n="HIAT:ip">,</nts>
                  <nts id="Seg_4395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1519" id="Seg_4397" n="HIAT:w" s="T1517">пускай</ts>
                  <nts id="Seg_4398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1520" id="Seg_4400" n="HIAT:w" s="T1519">она</ts>
                  <nts id="Seg_4401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4402" n="HIAT:ip">(</nts>
                  <nts id="Seg_4403" n="HIAT:ip">(</nts>
                  <ats e="T1521" id="Seg_4404" n="HIAT:non-pho" s="T1520">…</ats>
                  <nts id="Seg_4405" n="HIAT:ip">)</nts>
                  <nts id="Seg_4406" n="HIAT:ip">)</nts>
                  <nts id="Seg_4407" n="HIAT:ip">.</nts>
                  <nts id="Seg_4408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T9" id="Seg_4409" n="sc" s="T1">
               <ts e="T2" id="Seg_4411" n="e" s="T1">А </ts>
               <ts e="T3" id="Seg_4413" n="e" s="T2">я </ts>
               <ts e="T4" id="Seg_4415" n="e" s="T3">уж </ts>
               <ts e="T5" id="Seg_4417" n="e" s="T4">и </ts>
               <ts e="T6" id="Seg_4419" n="e" s="T5">забыла, </ts>
               <ts e="T7" id="Seg_4421" n="e" s="T6">чего </ts>
               <ts e="T8" id="Seg_4423" n="e" s="T7">говорила </ts>
               <ts e="T9" id="Seg_4425" n="e" s="T8">((…)). </ts>
            </ts>
            <ts e="T30" id="Seg_4426" n="sc" s="T17">
               <ts e="T19" id="Seg_4428" n="e" s="T17">Вот </ts>
               <ts e="T23" id="Seg_4430" n="e" s="T19">я </ts>
               <ts e="T26" id="Seg_4432" n="e" s="T23">выпустила, </ts>
               <ts e="T27" id="Seg_4434" n="e" s="T26">сейчас </ts>
               <ts e="T28" id="Seg_4436" n="e" s="T27">переговорила </ts>
               <ts e="T29" id="Seg_4438" n="e" s="T28">и </ts>
               <ts e="T30" id="Seg_4440" n="e" s="T29">забыла. </ts>
            </ts>
            <ts e="T51" id="Seg_4441" n="sc" s="T44">
               <ts e="T45" id="Seg_4443" n="e" s="T44">А, </ts>
               <ts e="T46" id="Seg_4445" n="e" s="T45">вот </ts>
               <ts e="T47" id="Seg_4447" n="e" s="T46">чего </ts>
               <ts e="T49" id="Seg_4449" n="e" s="T47">я </ts>
               <ts e="T51" id="Seg_4451" n="e" s="T49">говорила. </ts>
            </ts>
            <ts e="T59" id="Seg_4452" n="sc" s="T52">
               <ts e="T53" id="Seg_4454" n="e" s="T52">Когда </ts>
               <ts e="T54" id="Seg_4456" n="e" s="T53">я </ts>
               <ts e="T55" id="Seg_4458" n="e" s="T54">собралась </ts>
               <ts e="T57" id="Seg_4460" n="e" s="T55">сюды </ts>
               <ts e="T59" id="Seg_4462" n="e" s="T57">ехать… </ts>
            </ts>
            <ts e="T61" id="Seg_4463" n="sc" s="T60">
               <ts e="T61" id="Seg_4465" n="e" s="T60">Пусти. </ts>
            </ts>
            <ts e="T80" id="Seg_4466" n="sc" s="T62">
               <ts e="T63" id="Seg_4468" n="e" s="T62">Когда </ts>
               <ts e="T64" id="Seg_4470" n="e" s="T63">я </ts>
               <ts e="T65" id="Seg_4472" n="e" s="T64">собралась </ts>
               <ts e="T66" id="Seg_4474" n="e" s="T65">сюды </ts>
               <ts e="T67" id="Seg_4476" n="e" s="T66">ехать, </ts>
               <ts e="T68" id="Seg_4478" n="e" s="T67">там </ts>
               <ts e="T69" id="Seg_4480" n="e" s="T68">люди </ts>
               <ts e="T70" id="Seg_4482" n="e" s="T69">говорили: </ts>
               <ts e="T71" id="Seg_4484" n="e" s="T70">"Не </ts>
               <ts e="T72" id="Seg_4486" n="e" s="T71">езди, </ts>
               <ts e="T73" id="Seg_4488" n="e" s="T72">ты </ts>
               <ts e="T74" id="Seg_4490" n="e" s="T73">помрешь </ts>
               <ts e="T75" id="Seg_4492" n="e" s="T74">(ведь)". </ts>
               <ts e="T76" id="Seg_4494" n="e" s="T75">А </ts>
               <ts e="T77" id="Seg_4496" n="e" s="T76">я </ts>
               <ts e="T78" id="Seg_4498" n="e" s="T77">сказала: </ts>
               <ts e="T79" id="Seg_4500" n="e" s="T78">"Все </ts>
               <ts e="T80" id="Seg_4502" n="e" s="T79">равно. </ts>
            </ts>
            <ts e="T84" id="Seg_4503" n="sc" s="T81">
               <ts e="T82" id="Seg_4505" n="e" s="T81">И </ts>
               <ts e="T83" id="Seg_4507" n="e" s="T82">там </ts>
               <ts e="T84" id="Seg_4509" n="e" s="T83">похоронят". </ts>
            </ts>
            <ts e="T88" id="Seg_4510" n="sc" s="T85">
               <ts e="T86" id="Seg_4512" n="e" s="T85">Все </ts>
               <ts e="T87" id="Seg_4514" n="e" s="T86">равно </ts>
               <ts e="T88" id="Seg_4516" n="e" s="T87">земля. </ts>
            </ts>
            <ts e="T94" id="Seg_4517" n="sc" s="T89">
               <ts e="T90" id="Seg_4519" n="e" s="T89">Вот </ts>
               <ts e="T91" id="Seg_4521" n="e" s="T90">это </ts>
               <ts e="T92" id="Seg_4523" n="e" s="T91">вот </ts>
               <ts e="T93" id="Seg_4525" n="e" s="T92">я </ts>
               <ts e="T94" id="Seg_4527" n="e" s="T93">говорила. </ts>
            </ts>
            <ts e="T106" id="Seg_4528" n="sc" s="T104">
               <ts e="T106" id="Seg_4530" n="e" s="T104">Ну, еще это… </ts>
            </ts>
            <ts e="T118" id="Seg_4531" n="sc" s="T108">
               <ts e="T109" id="Seg_4533" n="e" s="T108">Про </ts>
               <ts e="T110" id="Seg_4535" n="e" s="T109">это </ts>
               <ts e="T111" id="Seg_4537" n="e" s="T110">говорить, </ts>
               <ts e="T112" id="Seg_4539" n="e" s="T111">что </ts>
               <ts e="T113" id="Seg_4541" n="e" s="T112">я </ts>
               <ts e="T114" id="Seg_4543" n="e" s="T113">набрала </ts>
               <ts e="T115" id="Seg_4545" n="e" s="T114">гостинцев </ts>
               <ts e="T116" id="Seg_4547" n="e" s="T115">еще </ts>
               <ts e="T117" id="Seg_4549" n="e" s="T116">туды </ts>
               <ts e="T118" id="Seg_4551" n="e" s="T117">своим. </ts>
            </ts>
            <ts e="T127" id="Seg_4552" n="sc" s="T126">
               <ts e="T127" id="Seg_4554" n="e" s="T126">Пускайте. </ts>
            </ts>
            <ts e="T137" id="Seg_4555" n="sc" s="T128">
               <ts e="T129" id="Seg_4557" n="e" s="T128">Kamen </ts>
               <ts e="T130" id="Seg_4559" n="e" s="T129">(măn) </ts>
               <ts e="T131" id="Seg_4561" n="e" s="T130">amnobiam </ts>
               <ts e="T132" id="Seg_4563" n="e" s="T131">Tartu, </ts>
               <ts e="T133" id="Seg_4565" n="e" s="T132">kambiam, </ts>
               <ts e="T134" id="Seg_4567" n="e" s="T133">ibiem </ts>
               <ts e="T135" id="Seg_4569" n="e" s="T134">(na-) </ts>
               <ts e="T136" id="Seg_4571" n="e" s="T135">nagur </ts>
               <ts e="T137" id="Seg_4573" n="e" s="T136">platokəʔjə. </ts>
            </ts>
            <ts e="T141" id="Seg_4574" n="sc" s="T138">
               <ts e="T139" id="Seg_4576" n="e" s="T138">Bostə </ts>
               <ts e="T140" id="Seg_4578" n="e" s="T139">ildə </ts>
               <ts e="T141" id="Seg_4580" n="e" s="T140">mĭzittə. </ts>
            </ts>
            <ts e="T147" id="Seg_4581" n="sc" s="T142">
               <ts e="T143" id="Seg_4583" n="e" s="T142">Dĭgəttə </ts>
               <ts e="T144" id="Seg_4585" n="e" s="T143">döbər </ts>
               <ts e="T145" id="Seg_4587" n="e" s="T144">šobiam, </ts>
               <ts e="T146" id="Seg_4589" n="e" s="T145">ibiem </ts>
               <ts e="T147" id="Seg_4591" n="e" s="T146">piʔmeʔi. </ts>
            </ts>
            <ts e="T150" id="Seg_4592" n="sc" s="T148">
               <ts e="T149" id="Seg_4594" n="e" s="T148">Kujnek </ts>
               <ts e="T150" id="Seg_4596" n="e" s="T149">ibiem. </ts>
            </ts>
            <ts e="T153" id="Seg_4597" n="sc" s="T151">
               <ts e="T152" id="Seg_4599" n="e" s="T151">Nʼinə, </ts>
               <ts e="T153" id="Seg_4601" n="e" s="T152">plʼemʼannikamnə. </ts>
            </ts>
            <ts e="T156" id="Seg_4602" n="sc" s="T154">
               <ts e="T155" id="Seg_4604" n="e" s="T154">Koʔbdonə </ts>
               <ts e="T156" id="Seg_4606" n="e" s="T155">ibiem… </ts>
            </ts>
            <ts e="T159" id="Seg_4607" n="sc" s="T157">
               <ts e="T158" id="Seg_4609" n="e" s="T157">Platʼtʼa </ts>
               <ts e="T159" id="Seg_4611" n="e" s="T158">ibiem. </ts>
            </ts>
            <ts e="T164" id="Seg_4612" n="sc" s="T160">
               <ts e="T161" id="Seg_4614" n="e" s="T160">Dĭgəttə </ts>
               <ts e="T162" id="Seg_4616" n="e" s="T161">plʼemʼanniktə </ts>
               <ts e="T163" id="Seg_4618" n="e" s="T162">ibiem </ts>
               <ts e="T164" id="Seg_4620" n="e" s="T163">kujnek. </ts>
            </ts>
            <ts e="T166" id="Seg_4621" n="sc" s="T165">
               <ts e="T166" id="Seg_4623" n="e" s="T165">Kabarləj. </ts>
            </ts>
            <ts e="T185" id="Seg_4624" n="sc" s="T173">
               <ts e="T174" id="Seg_4626" n="e" s="T173">Когда </ts>
               <ts e="T175" id="Seg_4628" n="e" s="T174">я </ts>
               <ts e="T176" id="Seg_4630" n="e" s="T175">сюды </ts>
               <ts e="T177" id="Seg_4632" n="e" s="T176">приехала </ts>
               <ts e="T178" id="Seg_4634" n="e" s="T177">в </ts>
               <ts e="T179" id="Seg_4636" n="e" s="T178">Тарту, </ts>
               <ts e="T180" id="Seg_4638" n="e" s="T179">пошла </ts>
               <ts e="T181" id="Seg_4640" n="e" s="T180">в </ts>
               <ts e="T182" id="Seg_4642" n="e" s="T181">магазин </ts>
               <ts e="T183" id="Seg_4644" n="e" s="T182">взяла </ts>
               <ts e="T184" id="Seg_4646" n="e" s="T183">три </ts>
               <ts e="T185" id="Seg_4648" n="e" s="T184">платка. </ts>
            </ts>
            <ts e="T193" id="Seg_4649" n="sc" s="T186">
               <ts e="T187" id="Seg_4651" n="e" s="T186">Сюды </ts>
               <ts e="T188" id="Seg_4653" n="e" s="T187">приехала </ts>
               <ts e="T189" id="Seg_4655" n="e" s="T188">вот, </ts>
               <ts e="T190" id="Seg_4657" n="e" s="T189">взяла </ts>
               <ts e="T191" id="Seg_4659" n="e" s="T190">штаны, </ts>
               <ts e="T192" id="Seg_4661" n="e" s="T191">рубашку </ts>
               <ts e="T193" id="Seg_4663" n="e" s="T192">внуку. </ts>
            </ts>
            <ts e="T198" id="Seg_4664" n="sc" s="T194">
               <ts e="T195" id="Seg_4666" n="e" s="T194">Потом </ts>
               <ts e="T196" id="Seg_4668" n="e" s="T195">взяла </ts>
               <ts e="T197" id="Seg_4670" n="e" s="T196">рубашку </ts>
               <ts e="T198" id="Seg_4672" n="e" s="T197">племяннику. </ts>
            </ts>
            <ts e="T210" id="Seg_4673" n="sc" s="T199">
               <ts e="T200" id="Seg_4675" n="e" s="T199">((BRK)) </ts>
               <ts e="T201" id="Seg_4677" n="e" s="T200">Забыла </ts>
               <ts e="T202" id="Seg_4679" n="e" s="T201">еще </ts>
               <ts e="T203" id="Seg_4681" n="e" s="T202">вот </ts>
               <ts e="T204" id="Seg_4683" n="e" s="T203">один </ts>
               <ts e="T205" id="Seg_4685" n="e" s="T204">платок </ts>
               <ts e="T206" id="Seg_4687" n="e" s="T205">взяла </ts>
               <ts e="T208" id="Seg_4689" n="e" s="T206">в </ts>
               <ts e="T210" id="Seg_4691" n="e" s="T208">Красноярский. </ts>
            </ts>
            <ts e="T224" id="Seg_4692" n="sc" s="T219">
               <ts e="T220" id="Seg_4694" n="e" s="T219">Нет, </ts>
               <ts e="T221" id="Seg_4696" n="e" s="T220">это… </ts>
               <ts e="T224" id="Seg_4698" n="e" s="T221">Это… </ts>
            </ts>
            <ts e="T230" id="Seg_4699" n="sc" s="T226">
               <ts e="T228" id="Seg_4701" n="e" s="T226">Штаны, </ts>
               <ts e="T230" id="Seg_4703" n="e" s="T228">piʔmeʔi. </ts>
            </ts>
            <ts e="T237" id="Seg_4704" n="sc" s="T233">
               <ts e="T234" id="Seg_4706" n="e" s="T233">Piʔme — </ts>
               <ts e="T237" id="Seg_4708" n="e" s="T234">штаны. </ts>
            </ts>
            <ts e="T249" id="Seg_4709" n="sc" s="T239">
               <ts e="T242" id="Seg_4711" n="e" s="T239">А, </ts>
               <ts e="T244" id="Seg_4713" n="e" s="T242">(а) </ts>
               <ts e="T247" id="Seg_4715" n="e" s="T244">эти </ts>
               <ts e="T249" id="Seg_4717" n="e" s="T247">piʔme. </ts>
            </ts>
            <ts e="T253" id="Seg_4718" n="sc" s="T250">
               <ts e="T251" id="Seg_4720" n="e" s="T250">Jamaʔi — </ts>
               <ts e="T252" id="Seg_4722" n="e" s="T251">piʔme, </ts>
               <ts e="T253" id="Seg_4724" n="e" s="T252">jaʔma. </ts>
            </ts>
            <ts e="T260" id="Seg_4725" n="sc" s="T257">
               <ts e="T260" id="Seg_4727" n="e" s="T257">Вот. </ts>
            </ts>
            <ts e="T270" id="Seg_4728" n="sc" s="T261">
               <ts e="T263" id="Seg_4730" n="e" s="T261">(Это=) </ts>
               <ts e="T265" id="Seg_4732" n="e" s="T263">Это </ts>
               <ts e="T268" id="Seg_4734" n="e" s="T265">обутки - </ts>
               <ts e="T270" id="Seg_4736" n="e" s="T268">jaʔma. </ts>
            </ts>
            <ts e="T280" id="Seg_4737" n="sc" s="T271">
               <ts e="T273" id="Seg_4739" n="e" s="T271">А </ts>
               <ts e="T274" id="Seg_4741" n="e" s="T273">piʔmeʔi — </ts>
               <ts e="T275" id="Seg_4743" n="e" s="T274">это </ts>
               <ts e="T276" id="Seg_4745" n="e" s="T275">штаны. </ts>
               <ts e="T277" id="Seg_4747" n="e" s="T276">(K-) </ts>
               <ts e="T278" id="Seg_4749" n="e" s="T277">Kujnek — </ts>
               <ts e="T280" id="Seg_4751" n="e" s="T278">рубаха. </ts>
            </ts>
            <ts e="T301" id="Seg_4752" n="sc" s="T284">
               <ts e="T286" id="Seg_4754" n="e" s="T284">Ну. </ts>
               <ts e="T287" id="Seg_4756" n="e" s="T286">А </ts>
               <ts e="T288" id="Seg_4758" n="e" s="T287">вот </ts>
               <ts e="T289" id="Seg_4760" n="e" s="T288">платье </ts>
               <ts e="T290" id="Seg_4762" n="e" s="T289">не </ts>
               <ts e="T291" id="Seg_4764" n="e" s="T290">знаю, </ts>
               <ts e="T292" id="Seg_4766" n="e" s="T291">вот </ts>
               <ts e="T294" id="Seg_4768" n="e" s="T292">(этой=) </ts>
               <ts e="T296" id="Seg_4770" n="e" s="T294">Наде </ts>
               <ts e="T299" id="Seg_4772" n="e" s="T296">взяла </ts>
               <ts e="T301" id="Seg_4774" n="e" s="T299">платье. </ts>
            </ts>
            <ts e="T312" id="Seg_4775" n="sc" s="T307">
               <ts e="T310" id="Seg_4777" n="e" s="T307">Не </ts>
               <ts e="T312" id="Seg_4779" n="e" s="T310">знаю. </ts>
            </ts>
            <ts e="T344" id="Seg_4780" n="sc" s="T313">
               <ts e="T315" id="Seg_4782" n="e" s="T313">Тады </ts>
               <ts e="T317" id="Seg_4784" n="e" s="T315">не </ts>
               <ts e="T319" id="Seg_4786" n="e" s="T317">носили, </ts>
               <ts e="T321" id="Seg_4788" n="e" s="T319">они </ts>
               <ts e="T323" id="Seg_4790" n="e" s="T321">все </ts>
               <ts e="T325" id="Seg_4792" n="e" s="T323">в </ts>
               <ts e="T327" id="Seg_4794" n="e" s="T325">штанах </ts>
               <ts e="T329" id="Seg_4796" n="e" s="T327">ходили </ts>
               <ts e="T331" id="Seg_4798" n="e" s="T329">так, </ts>
               <ts e="T333" id="Seg_4800" n="e" s="T331">как </ts>
               <ts e="T335" id="Seg_4802" n="e" s="T333">вот </ts>
               <ts e="T337" id="Seg_4804" n="e" s="T335">теперь </ts>
               <ts e="T339" id="Seg_4806" n="e" s="T337">все </ts>
               <ts e="T341" id="Seg_4808" n="e" s="T339">ходят </ts>
               <ts e="T342" id="Seg_4810" n="e" s="T341">в </ts>
               <ts e="T344" id="Seg_4812" n="e" s="T342">штанах. </ts>
            </ts>
            <ts e="T348" id="Seg_4813" n="sc" s="T347">
               <ts e="T348" id="Seg_4815" n="e" s="T347">Kujnek. </ts>
            </ts>
            <ts e="T353" id="Seg_4816" n="sc" s="T349">
               <ts e="T350" id="Seg_4818" n="e" s="T349">А </ts>
               <ts e="T351" id="Seg_4820" n="e" s="T350">(с-) </ts>
               <ts e="T352" id="Seg_4822" n="e" s="T351">шапка — </ts>
               <ts e="T353" id="Seg_4824" n="e" s="T352">üžü. </ts>
            </ts>
            <ts e="T364" id="Seg_4825" n="sc" s="T359">
               <ts e="T360" id="Seg_4827" n="e" s="T359">Не </ts>
               <ts e="T362" id="Seg_4829" n="e" s="T360">знаю, </ts>
               <ts e="T363" id="Seg_4831" n="e" s="T362">что </ts>
               <ts e="T364" id="Seg_4833" n="e" s="T363">сказать. </ts>
            </ts>
            <ts e="T410" id="Seg_4834" n="sc" s="T389">
               <ts e="T390" id="Seg_4836" n="e" s="T389">Можно </ts>
               <ts e="T392" id="Seg_4838" n="e" s="T390">это </ts>
               <ts e="T394" id="Seg_4840" n="e" s="T392">рассказать, </ts>
               <ts e="T408" id="Seg_4842" n="e" s="T394">(как=) ((LAUGH)) как Александр Константинович шел ко мне, я часто смеюсь над этим, даже сегодня </ts>
               <ts e="T410" id="Seg_4844" n="e" s="T408">смеялась? </ts>
            </ts>
            <ts e="T424" id="Seg_4845" n="sc" s="T411">
               <ts e="T412" id="Seg_4847" n="e" s="T411">Он </ts>
               <ts e="T413" id="Seg_4849" n="e" s="T412">идет: </ts>
               <ts e="T414" id="Seg_4851" n="e" s="T413">"Бабушка, </ts>
               <ts e="T415" id="Seg_4853" n="e" s="T414">я </ts>
               <ts e="T416" id="Seg_4855" n="e" s="T415">иду </ts>
               <ts e="T417" id="Seg_4857" n="e" s="T416">к </ts>
               <ts e="T418" id="Seg_4859" n="e" s="T417">тебе </ts>
               <ts e="T419" id="Seg_4861" n="e" s="T418">разговаривать", </ts>
               <ts e="T420" id="Seg_4863" n="e" s="T419">а </ts>
               <ts e="T421" id="Seg_4865" n="e" s="T420">я </ts>
               <ts e="T422" id="Seg_4867" n="e" s="T421">его </ts>
               <ts e="T423" id="Seg_4869" n="e" s="T422">по-своему </ts>
               <ts e="T424" id="Seg_4871" n="e" s="T423">ругаю. </ts>
            </ts>
            <ts e="T446" id="Seg_4872" n="sc" s="T428">
               <ts e="T430" id="Seg_4874" n="e" s="T428">Слухай, </ts>
               <ts e="T432" id="Seg_4876" n="e" s="T430">слухай, </ts>
               <ts e="T433" id="Seg_4878" n="e" s="T432">сперва </ts>
               <ts e="T435" id="Seg_4880" n="e" s="T433">расскажу, </ts>
               <ts e="T436" id="Seg_4882" n="e" s="T435">как </ts>
               <ts e="T437" id="Seg_4884" n="e" s="T436">было </ts>
               <ts e="T438" id="Seg_4886" n="e" s="T437">дело. </ts>
               <ts e="T439" id="Seg_4888" n="e" s="T438">А </ts>
               <ts e="T440" id="Seg_4890" n="e" s="T439">потом </ts>
               <ts e="T441" id="Seg_4892" n="e" s="T440">опять </ts>
               <ts e="T442" id="Seg_4894" n="e" s="T441">расскажу, </ts>
               <ts e="T443" id="Seg_4896" n="e" s="T442">ты </ts>
               <ts e="T444" id="Seg_4898" n="e" s="T443">может </ts>
               <ts e="T445" id="Seg_4900" n="e" s="T444">лучше </ts>
               <ts e="T446" id="Seg_4902" n="e" s="T445">запомнишь. </ts>
            </ts>
            <ts e="T455" id="Seg_4903" n="sc" s="T447">
               <ts e="T448" id="Seg_4905" n="e" s="T447">Вот </ts>
               <ts e="T449" id="Seg_4907" n="e" s="T448">он </ts>
               <ts e="T450" id="Seg_4909" n="e" s="T449">идет: </ts>
               <ts e="T451" id="Seg_4911" n="e" s="T450">"Бабушка, </ts>
               <ts e="T452" id="Seg_4913" n="e" s="T451">я </ts>
               <ts e="T453" id="Seg_4915" n="e" s="T452">к </ts>
               <ts e="T454" id="Seg_4917" n="e" s="T453">тебе </ts>
               <ts e="T455" id="Seg_4919" n="e" s="T454">иду!" </ts>
            </ts>
            <ts e="T472" id="Seg_4920" n="sc" s="T456">
               <ts e="T457" id="Seg_4922" n="e" s="T456">(Я </ts>
               <ts e="T458" id="Seg_4924" n="e" s="T457">г-) </ts>
               <ts e="T459" id="Seg_4926" n="e" s="T458">А </ts>
               <ts e="T460" id="Seg_4928" n="e" s="T459">я </ts>
               <ts e="T461" id="Seg_4930" n="e" s="T460">ему </ts>
               <ts e="T462" id="Seg_4932" n="e" s="T461">говорю: </ts>
               <ts e="T463" id="Seg_4934" n="e" s="T462">"Ты </ts>
               <ts e="T464" id="Seg_4936" n="e" s="T463">мне </ts>
               <ts e="T465" id="Seg_4938" n="e" s="T464">надоел, </ts>
               <ts e="T466" id="Seg_4940" n="e" s="T465">убирайся </ts>
               <ts e="T467" id="Seg_4942" n="e" s="T466">отседова, </ts>
               <ts e="T468" id="Seg_4944" n="e" s="T467">не </ts>
               <ts e="T469" id="Seg_4946" n="e" s="T468">ходи", — </ts>
               <ts e="T470" id="Seg_4948" n="e" s="T469">на </ts>
               <ts e="T471" id="Seg_4950" n="e" s="T470">его </ts>
               <ts e="T472" id="Seg_4952" n="e" s="T471">говорю. </ts>
            </ts>
            <ts e="T479" id="Seg_4953" n="sc" s="T473">
               <ts e="T474" id="Seg_4955" n="e" s="T473">"Боле </ts>
               <ts e="T475" id="Seg_4957" n="e" s="T474">не </ts>
               <ts e="T476" id="Seg_4959" n="e" s="T475">буду </ts>
               <ts e="T477" id="Seg_4961" n="e" s="T476">с </ts>
               <ts e="T478" id="Seg_4963" n="e" s="T477">тобой </ts>
               <ts e="T479" id="Seg_4965" n="e" s="T478">разговаривать. </ts>
            </ts>
            <ts e="T482" id="Seg_4966" n="sc" s="T480">
               <ts e="T481" id="Seg_4968" n="e" s="T480">Уходи </ts>
               <ts e="T482" id="Seg_4970" n="e" s="T481">отседова. </ts>
            </ts>
            <ts e="T494" id="Seg_4971" n="sc" s="T483">
               <ts e="T484" id="Seg_4973" n="e" s="T483">Я </ts>
               <ts e="T485" id="Seg_4975" n="e" s="T484">тебе </ts>
               <ts e="T486" id="Seg_4977" n="e" s="T485">ничего </ts>
               <ts e="T487" id="Seg_4979" n="e" s="T486">не </ts>
               <ts e="T488" id="Seg_4981" n="e" s="T487">скажу", — </ts>
               <ts e="T489" id="Seg_4983" n="e" s="T488">вот </ts>
               <ts e="T490" id="Seg_4985" n="e" s="T489">это </ts>
               <ts e="T491" id="Seg_4987" n="e" s="T490">я </ts>
               <ts e="T492" id="Seg_4989" n="e" s="T491">ему </ts>
               <ts e="T493" id="Seg_4991" n="e" s="T492">так </ts>
               <ts e="T494" id="Seg_4993" n="e" s="T493">говорила. </ts>
            </ts>
            <ts e="T502" id="Seg_4994" n="sc" s="T495">
               <ts e="T496" id="Seg_4996" n="e" s="T495">А </ts>
               <ts e="T497" id="Seg_4998" n="e" s="T496">он </ts>
               <ts e="T498" id="Seg_5000" n="e" s="T497">идет </ts>
               <ts e="T499" id="Seg_5002" n="e" s="T498">да </ts>
               <ts e="T500" id="Seg_5004" n="e" s="T499">говорит: </ts>
               <ts e="T501" id="Seg_5006" n="e" s="T500">"Ой </ts>
               <ts e="T502" id="Seg_5008" n="e" s="T501">((DMG)). </ts>
            </ts>
            <ts e="T519" id="Seg_5009" n="sc" s="T503">
               <ts e="T504" id="Seg_5011" n="e" s="T503">Kamen </ts>
               <ts e="T505" id="Seg_5013" n="e" s="T504">dĭ </ts>
               <ts e="T506" id="Seg_5015" n="e" s="T505">šobi </ts>
               <ts e="T507" id="Seg_5017" n="e" s="T506">măna, </ts>
               <ts e="T508" id="Seg_5019" n="e" s="T507">Александр </ts>
               <ts e="T509" id="Seg_5021" n="e" s="T508">Константинович, </ts>
               <ts e="T510" id="Seg_5023" n="e" s="T509">măndə: </ts>
               <ts e="T511" id="Seg_5025" n="e" s="T510">"Urgaja, </ts>
               <ts e="T512" id="Seg_5027" n="e" s="T511">măn </ts>
               <ts e="T513" id="Seg_5029" n="e" s="T512">tănan </ts>
               <ts e="T514" id="Seg_5031" n="e" s="T513">šonəgam!" </ts>
               <ts e="T515" id="Seg_5033" n="e" s="T514">A </ts>
               <ts e="T516" id="Seg_5035" n="e" s="T515">măn </ts>
               <ts e="T517" id="Seg_5037" n="e" s="T516">măndəm: </ts>
               <ts e="T518" id="Seg_5039" n="e" s="T517">"Iʔ </ts>
               <ts e="T519" id="Seg_5041" n="e" s="T518">šoʔ. </ts>
            </ts>
            <ts e="T522" id="Seg_5042" n="sc" s="T520">
               <ts e="T521" id="Seg_5044" n="e" s="T520">(Tuʔ) </ts>
               <ts e="T522" id="Seg_5046" n="e" s="T521">măna. </ts>
            </ts>
            <ts e="T525" id="Seg_5047" n="sc" s="T523">
               <ts e="T524" id="Seg_5049" n="e" s="T523">Kanaʔ </ts>
               <ts e="T525" id="Seg_5051" n="e" s="T524">döʔə! </ts>
            </ts>
            <ts e="T529" id="Seg_5052" n="sc" s="T526">
               <ts e="T527" id="Seg_5054" n="e" s="T526">Tăn </ts>
               <ts e="T528" id="Seg_5056" n="e" s="T527">măna </ts>
               <ts e="T529" id="Seg_5058" n="e" s="T528">(надое-). </ts>
            </ts>
            <ts e="T532" id="Seg_5059" n="sc" s="T530">
               <ts e="T531" id="Seg_5061" n="e" s="T530">Tăn </ts>
               <ts e="T532" id="Seg_5063" n="e" s="T531">măna… </ts>
            </ts>
            <ts e="T537" id="Seg_5064" n="sc" s="T533">
               <ts e="T534" id="Seg_5066" n="e" s="T533">Măn </ts>
               <ts e="T535" id="Seg_5068" n="e" s="T534">tănzi </ts>
               <ts e="T536" id="Seg_5070" n="e" s="T535">ej </ts>
               <ts e="T537" id="Seg_5072" n="e" s="T536">dʼăbaktərlam. </ts>
            </ts>
            <ts e="T540" id="Seg_5073" n="sc" s="T538">
               <ts e="T539" id="Seg_5075" n="e" s="T538">Kanaʔ </ts>
               <ts e="T540" id="Seg_5077" n="e" s="T539">döʔə". </ts>
            </ts>
            <ts e="T548" id="Seg_5078" n="sc" s="T541">
               <ts e="T542" id="Seg_5080" n="e" s="T541">По-русски </ts>
               <ts e="T543" id="Seg_5082" n="e" s="T542">хорошо </ts>
               <ts e="T544" id="Seg_5084" n="e" s="T543">рассказывала, </ts>
               <ts e="T545" id="Seg_5086" n="e" s="T544">опять </ts>
               <ts e="T548" id="Seg_5088" n="e" s="T545">забыла. </ts>
            </ts>
            <ts e="T560" id="Seg_5089" n="sc" s="T555">
               <ts e="T556" id="Seg_5091" n="e" s="T555">Kamen </ts>
               <ts e="T557" id="Seg_5093" n="e" s="T556">dĭ </ts>
               <ts e="T558" id="Seg_5095" n="e" s="T557">măna </ts>
               <ts e="T559" id="Seg_5097" n="e" s="T558">(šobi=) </ts>
               <ts e="T560" id="Seg_5099" n="e" s="T559">šobi… </ts>
            </ts>
            <ts e="T566" id="Seg_5100" n="sc" s="T561">
               <ts e="T562" id="Seg_5102" n="e" s="T561">Ой, </ts>
               <ts e="T563" id="Seg_5104" n="e" s="T562">опять </ts>
               <ts e="T564" id="Seg_5106" n="e" s="T563">по-своему </ts>
               <ts e="T566" id="Seg_5108" n="e" s="T564">начала. </ts>
            </ts>
            <ts e="T586" id="Seg_5109" n="sc" s="T568">
               <ts e="T571" id="Seg_5111" n="e" s="T568">Kamen </ts>
               <ts e="T572" id="Seg_5113" n="e" s="T571">dĭ </ts>
               <ts e="T573" id="Seg_5115" n="e" s="T572">măna </ts>
               <ts e="T574" id="Seg_5117" n="e" s="T573">šobi, </ts>
               <ts e="T575" id="Seg_5119" n="e" s="T574">măn </ts>
               <ts e="T576" id="Seg_5121" n="e" s="T575">kudolbiam </ts>
               <ts e="T578" id="Seg_5123" n="e" s="T576">dĭm: </ts>
               <ts e="T579" id="Seg_5125" n="e" s="T578">"Iʔ </ts>
               <ts e="T580" id="Seg_5127" n="e" s="T579">šoʔ </ts>
               <ts e="T581" id="Seg_5129" n="e" s="T580">măna! </ts>
               <ts e="T582" id="Seg_5131" n="e" s="T581">Măn </ts>
               <ts e="T583" id="Seg_5133" n="e" s="T582">tănan </ts>
               <ts e="T584" id="Seg_5135" n="e" s="T583">ĭmbidə </ts>
               <ts e="T585" id="Seg_5137" n="e" s="T584">ej </ts>
               <ts e="T586" id="Seg_5139" n="e" s="T585">(nörbələm). </ts>
            </ts>
            <ts e="T589" id="Seg_5140" n="sc" s="T587">
               <ts e="T588" id="Seg_5142" n="e" s="T587">Kanaʔ </ts>
               <ts e="T589" id="Seg_5144" n="e" s="T588">döʔə!" </ts>
            </ts>
            <ts e="T596" id="Seg_5145" n="sc" s="T590">
               <ts e="T591" id="Seg_5147" n="e" s="T590">Dĭgəttə </ts>
               <ts e="T592" id="Seg_5149" n="e" s="T591">dĭ </ts>
               <ts e="T593" id="Seg_5151" n="e" s="T592">măndə: </ts>
               <ts e="T594" id="Seg_5153" n="e" s="T593">"Ugandə </ts>
               <ts e="T595" id="Seg_5155" n="e" s="T594">jakše </ts>
               <ts e="T596" id="Seg_5157" n="e" s="T595">dʼăbaktərial". </ts>
            </ts>
            <ts e="T603" id="Seg_5158" n="sc" s="T597">
               <ts e="T598" id="Seg_5160" n="e" s="T597">Măn </ts>
               <ts e="T599" id="Seg_5162" n="e" s="T598">măndəm: </ts>
               <ts e="T600" id="Seg_5164" n="e" s="T599">"Прямо </ts>
               <ts e="T601" id="Seg_5166" n="e" s="T600">jakše, </ts>
               <ts e="T602" id="Seg_5168" n="e" s="T601">kudolliam </ts>
               <ts e="T603" id="Seg_5170" n="e" s="T602">tănan. </ts>
            </ts>
            <ts e="T608" id="Seg_5171" n="sc" s="T604">
               <ts e="T605" id="Seg_5173" n="e" s="T604">A </ts>
               <ts e="T606" id="Seg_5175" n="e" s="T605">tăn </ts>
               <ts e="T607" id="Seg_5177" n="e" s="T606">mănlial: </ts>
               <ts e="T608" id="Seg_5179" n="e" s="T607">jakše". </ts>
            </ts>
            <ts e="T622" id="Seg_5180" n="sc" s="T609">
               <ts e="T610" id="Seg_5182" n="e" s="T609">Когды </ts>
               <ts e="T611" id="Seg_5184" n="e" s="T610">шел </ts>
               <ts e="T612" id="Seg_5186" n="e" s="T611">ко </ts>
               <ts e="T613" id="Seg_5188" n="e" s="T612">мне </ts>
               <ts e="T614" id="Seg_5190" n="e" s="T613">(Констан-) </ts>
               <ts e="T615" id="Seg_5192" n="e" s="T614">Александр </ts>
               <ts e="T616" id="Seg_5194" n="e" s="T615">Константинов, </ts>
               <ts e="T617" id="Seg_5196" n="e" s="T616">говорит: </ts>
               <ts e="T618" id="Seg_5198" n="e" s="T617">"Бабушка, </ts>
               <ts e="T619" id="Seg_5200" n="e" s="T618">я </ts>
               <ts e="T620" id="Seg_5202" n="e" s="T619">к </ts>
               <ts e="T621" id="Seg_5204" n="e" s="T620">тебе </ts>
               <ts e="T622" id="Seg_5206" n="e" s="T621">иду!" </ts>
            </ts>
            <ts e="T631" id="Seg_5207" n="sc" s="T623">
               <ts e="T624" id="Seg_5209" n="e" s="T623">А </ts>
               <ts e="T625" id="Seg_5211" n="e" s="T624">я </ts>
               <ts e="T626" id="Seg_5213" n="e" s="T625">говорю: </ts>
               <ts e="T627" id="Seg_5215" n="e" s="T626">"Не </ts>
               <ts e="T628" id="Seg_5217" n="e" s="T627">ходи, </ts>
               <ts e="T629" id="Seg_5219" n="e" s="T628">зачем </ts>
               <ts e="T630" id="Seg_5221" n="e" s="T629">ты </ts>
               <ts e="T631" id="Seg_5223" n="e" s="T630">идешь? </ts>
            </ts>
            <ts e="T643" id="Seg_5224" n="sc" s="T632">
               <ts e="T633" id="Seg_5226" n="e" s="T632">Ты </ts>
               <ts e="T634" id="Seg_5228" n="e" s="T633">мне </ts>
               <ts e="T635" id="Seg_5230" n="e" s="T634">надоел, </ts>
               <ts e="T636" id="Seg_5232" n="e" s="T635">уходи </ts>
               <ts e="T637" id="Seg_5234" n="e" s="T636">отседова, </ts>
               <ts e="T638" id="Seg_5236" n="e" s="T637">я </ts>
               <ts e="T639" id="Seg_5238" n="e" s="T638">с </ts>
               <ts e="T640" id="Seg_5240" n="e" s="T639">тобой </ts>
               <ts e="T641" id="Seg_5242" n="e" s="T640">не </ts>
               <ts e="T642" id="Seg_5244" n="e" s="T641">буду </ts>
               <ts e="T643" id="Seg_5246" n="e" s="T642">разговаривать". </ts>
            </ts>
            <ts e="T664" id="Seg_5247" n="sc" s="T644">
               <ts e="T645" id="Seg_5249" n="e" s="T644">А </ts>
               <ts e="T646" id="Seg_5251" n="e" s="T645">он </ts>
               <ts e="T647" id="Seg_5253" n="e" s="T646">говорит: </ts>
               <ts e="T648" id="Seg_5255" n="e" s="T647">"Ой, </ts>
               <ts e="T649" id="Seg_5257" n="e" s="T648">бабушка, </ts>
               <ts e="T650" id="Seg_5259" n="e" s="T649">как </ts>
               <ts e="T651" id="Seg_5261" n="e" s="T650">ты </ts>
               <ts e="T652" id="Seg_5263" n="e" s="T651">хорошо </ts>
               <ts e="T653" id="Seg_5265" n="e" s="T652">(р-) </ts>
               <ts e="T654" id="Seg_5267" n="e" s="T653">мне </ts>
               <ts e="T655" id="Seg_5269" n="e" s="T654">рассказываешь". </ts>
               <ts e="T656" id="Seg_5271" n="e" s="T655">А </ts>
               <ts e="T657" id="Seg_5273" n="e" s="T656">я </ts>
               <ts e="T658" id="Seg_5275" n="e" s="T657">ему </ts>
               <ts e="T659" id="Seg_5277" n="e" s="T658">говорю: </ts>
               <ts e="T660" id="Seg_5279" n="e" s="T659">"Прямо </ts>
               <ts e="T661" id="Seg_5281" n="e" s="T660">хорошо, </ts>
               <ts e="T662" id="Seg_5283" n="e" s="T661">я </ts>
               <ts e="T663" id="Seg_5285" n="e" s="T662">тебя </ts>
               <ts e="T664" id="Seg_5287" n="e" s="T663">ругаю". </ts>
            </ts>
            <ts e="T670" id="Seg_5288" n="sc" s="T665">
               <ts e="T666" id="Seg_5290" n="e" s="T665">А </ts>
               <ts e="T667" id="Seg_5292" n="e" s="T666">он </ts>
               <ts e="T668" id="Seg_5294" n="e" s="T667">говорит: </ts>
               <ts e="T669" id="Seg_5296" n="e" s="T668">"Ну </ts>
               <ts e="T670" id="Seg_5298" n="e" s="T669">ладно". </ts>
            </ts>
            <ts e="T674" id="Seg_5299" n="sc" s="T671">
               <ts e="T672" id="Seg_5301" n="e" s="T671">Ему </ts>
               <ts e="T673" id="Seg_5303" n="e" s="T672">(глянулося) </ts>
               <ts e="T674" id="Seg_5305" n="e" s="T673">приехать. </ts>
            </ts>
            <ts e="T681" id="Seg_5306" n="sc" s="T675">
               <ts e="T676" id="Seg_5308" n="e" s="T675">"Ну, </ts>
               <ts e="T677" id="Seg_5310" n="e" s="T676">я </ts>
               <ts e="T678" id="Seg_5312" n="e" s="T677">хотел </ts>
               <ts e="T679" id="Seg_5314" n="e" s="T678">с </ts>
               <ts e="T680" id="Seg_5316" n="e" s="T679">сыном </ts>
               <ts e="T681" id="Seg_5318" n="e" s="T680">приехать". </ts>
            </ts>
            <ts e="T686" id="Seg_5319" n="sc" s="T682">
               <ts e="T683" id="Seg_5321" n="e" s="T682">Его </ts>
               <ts e="T684" id="Seg_5323" n="e" s="T683">сын </ts>
               <ts e="T685" id="Seg_5325" n="e" s="T684">Константином </ts>
               <ts e="T686" id="Seg_5327" n="e" s="T685">звать. </ts>
            </ts>
            <ts e="T691" id="Seg_5328" n="sc" s="T687">
               <ts e="T688" id="Seg_5330" n="e" s="T687">"И </ts>
               <ts e="T689" id="Seg_5332" n="e" s="T688">теперь </ts>
               <ts e="T690" id="Seg_5334" n="e" s="T689">напрок </ts>
               <ts e="T691" id="Seg_5336" n="e" s="T690">приеду". </ts>
            </ts>
            <ts e="T701" id="Seg_5337" n="sc" s="T692">
               <ts e="T693" id="Seg_5339" n="e" s="T692">Ну, </ts>
               <ts e="T694" id="Seg_5341" n="e" s="T693">а </ts>
               <ts e="T695" id="Seg_5343" n="e" s="T694">я </ts>
               <ts e="T696" id="Seg_5345" n="e" s="T695">говорю: </ts>
               <ts e="T697" id="Seg_5347" n="e" s="T696">"Приедет, </ts>
               <ts e="T698" id="Seg_5349" n="e" s="T697">напрок </ts>
               <ts e="T699" id="Seg_5351" n="e" s="T698">может </ts>
               <ts e="T700" id="Seg_5353" n="e" s="T699">я </ts>
               <ts e="T701" id="Seg_5355" n="e" s="T700">помру". </ts>
            </ts>
            <ts e="T706" id="Seg_5356" n="sc" s="T702">
               <ts e="T703" id="Seg_5358" n="e" s="T702">Стара </ts>
               <ts e="T704" id="Seg_5360" n="e" s="T703">уже, </ts>
               <ts e="T705" id="Seg_5362" n="e" s="T704">дожидаешь </ts>
               <ts e="T706" id="Seg_5364" n="e" s="T705">смерти. </ts>
            </ts>
            <ts e="T712" id="Seg_5365" n="sc" s="T707">
               <ts e="T708" id="Seg_5367" n="e" s="T707">Ну, </ts>
               <ts e="T709" id="Seg_5369" n="e" s="T708">можно </ts>
               <ts e="T710" id="Seg_5371" n="e" s="T709">и </ts>
               <ts e="T711" id="Seg_5373" n="e" s="T710">это </ts>
               <ts e="T712" id="Seg_5375" n="e" s="T711">поговорить. </ts>
            </ts>
            <ts e="T716" id="Seg_5376" n="sc" s="T713">
               <ts e="T714" id="Seg_5378" n="e" s="T713">Ну </ts>
               <ts e="T716" id="Seg_5380" n="e" s="T714">чего? </ts>
            </ts>
            <ts e="T724" id="Seg_5381" n="sc" s="T722">
               <ts e="T723" id="Seg_5383" n="e" s="T722">Ой, </ts>
               <ts e="T724" id="Seg_5385" n="e" s="T723">господи. </ts>
            </ts>
            <ts e="T738" id="Seg_5386" n="sc" s="T728">
               <ts e="T730" id="Seg_5388" n="e" s="T728">А-а-а, </ts>
               <ts e="T733" id="Seg_5390" n="e" s="T730">(Алек-) </ts>
               <ts e="T735" id="Seg_5392" n="e" s="T733">Александр </ts>
               <ts e="T736" id="Seg_5394" n="e" s="T735">Константинович </ts>
               <ts e="T737" id="Seg_5396" n="e" s="T736">обещал </ts>
               <ts e="T738" id="Seg_5398" n="e" s="T737">приехать. </ts>
            </ts>
            <ts e="T750" id="Seg_5399" n="sc" s="T739">
               <ts e="T740" id="Seg_5401" n="e" s="T739">Написал </ts>
               <ts e="T741" id="Seg_5403" n="e" s="T740">письмо, </ts>
               <ts e="T742" id="Seg_5405" n="e" s="T741">а </ts>
               <ts e="T743" id="Seg_5407" n="e" s="T742">потом </ts>
               <ts e="T744" id="Seg_5409" n="e" s="T743">(я) </ts>
               <ts e="T745" id="Seg_5411" n="e" s="T744">ждала-ждала, </ts>
               <ts e="T746" id="Seg_5413" n="e" s="T745">он </ts>
               <ts e="T747" id="Seg_5415" n="e" s="T746">опять </ts>
               <ts e="T748" id="Seg_5417" n="e" s="T747">пишет: </ts>
               <ts e="T749" id="Seg_5419" n="e" s="T748">"Не </ts>
               <ts e="T750" id="Seg_5421" n="e" s="T749">приеду. </ts>
            </ts>
            <ts e="T759" id="Seg_5422" n="sc" s="T751">
               <ts e="T752" id="Seg_5424" n="e" s="T751">Ездил </ts>
               <ts e="T753" id="Seg_5426" n="e" s="T752">я </ts>
               <ts e="T754" id="Seg_5428" n="e" s="T753">в </ts>
               <ts e="T755" id="Seg_5430" n="e" s="T754">Москву, </ts>
               <ts e="T756" id="Seg_5432" n="e" s="T755">и </ts>
               <ts e="T757" id="Seg_5434" n="e" s="T756">на </ts>
               <ts e="T758" id="Seg_5436" n="e" s="T757">север </ts>
               <ts e="T759" id="Seg_5438" n="e" s="T758">ездил. </ts>
            </ts>
            <ts e="T765" id="Seg_5439" n="sc" s="T760">
               <ts e="T761" id="Seg_5441" n="e" s="T760">Так </ts>
               <ts e="T762" id="Seg_5443" n="e" s="T761">что </ts>
               <ts e="T763" id="Seg_5445" n="e" s="T762">не </ts>
               <ts e="T764" id="Seg_5447" n="e" s="T763">могу </ts>
               <ts e="T765" id="Seg_5449" n="e" s="T764">приехать". </ts>
            </ts>
            <ts e="T772" id="Seg_5450" n="sc" s="T766">
               <ts e="T767" id="Seg_5452" n="e" s="T766">Ну, </ts>
               <ts e="T768" id="Seg_5454" n="e" s="T767">я </ts>
               <ts e="T769" id="Seg_5456" n="e" s="T768">и </ts>
               <ts e="T770" id="Seg_5458" n="e" s="T769">не </ts>
               <ts e="T771" id="Seg_5460" n="e" s="T770">стала </ts>
               <ts e="T772" id="Seg_5462" n="e" s="T771">дожидать. </ts>
            </ts>
            <ts e="T776" id="Seg_5463" n="sc" s="T773">
               <ts e="T774" id="Seg_5465" n="e" s="T773">А </ts>
               <ts e="T775" id="Seg_5467" n="e" s="T774">потом </ts>
               <ts e="T776" id="Seg_5469" n="e" s="T775">приехал… </ts>
            </ts>
            <ts e="T778" id="Seg_5470" n="sc" s="T777">
               <ts e="T778" id="Seg_5472" n="e" s="T777">Господи. </ts>
            </ts>
            <ts e="T782" id="Seg_5473" n="sc" s="T779">
               <ts e="T780" id="Seg_5475" n="e" s="T779">И </ts>
               <ts e="T781" id="Seg_5477" n="e" s="T780">имя-то </ts>
               <ts e="T782" id="Seg_5479" n="e" s="T781">забываю. </ts>
            </ts>
            <ts e="T790" id="Seg_5480" n="sc" s="T783">
               <ts e="T784" id="Seg_5482" n="e" s="T783">Арпит, </ts>
               <ts e="T785" id="Seg_5484" n="e" s="T784">я </ts>
               <ts e="T786" id="Seg_5486" n="e" s="T785">с </ts>
               <ts e="T787" id="Seg_5488" n="e" s="T786">им </ts>
               <ts e="T788" id="Seg_5490" n="e" s="T787">собралась </ts>
               <ts e="T789" id="Seg_5492" n="e" s="T788">да </ts>
               <ts e="T790" id="Seg_5494" n="e" s="T789">уехала. </ts>
            </ts>
            <ts e="T800" id="Seg_5495" n="sc" s="T791">
               <ts e="T792" id="Seg_5497" n="e" s="T791">"Так, </ts>
               <ts e="T793" id="Seg_5499" n="e" s="T792">думаю, </ts>
               <ts e="T794" id="Seg_5501" n="e" s="T793">раз </ts>
               <ts e="T795" id="Seg_5503" n="e" s="T794">не </ts>
               <ts e="T796" id="Seg_5505" n="e" s="T795">хочет </ts>
               <ts e="T797" id="Seg_5507" n="e" s="T796">приезжать </ts>
               <ts e="T798" id="Seg_5509" n="e" s="T797">дак </ts>
               <ts e="T799" id="Seg_5511" n="e" s="T798">и </ts>
               <ts e="T800" id="Seg_5513" n="e" s="T799">уеду". </ts>
            </ts>
            <ts e="T803" id="Seg_5514" n="sc" s="T801">
               <ts e="T802" id="Seg_5516" n="e" s="T801">Приехала </ts>
               <ts e="T803" id="Seg_5518" n="e" s="T802">сюды. </ts>
            </ts>
            <ts e="T823" id="Seg_5519" n="sc" s="T819">
               <ts e="T820" id="Seg_5521" n="e" s="T819">Как </ts>
               <ts e="T821" id="Seg_5523" n="e" s="T820">не </ts>
               <ts e="T823" id="Seg_5525" n="e" s="T821">говорили? </ts>
            </ts>
            <ts e="T834" id="Seg_5526" n="sc" s="T828">
               <ts e="T829" id="Seg_5528" n="e" s="T828">Я </ts>
               <ts e="T830" id="Seg_5530" n="e" s="T829">по-русски </ts>
               <ts e="T831" id="Seg_5532" n="e" s="T830">записала, </ts>
               <ts e="T832" id="Seg_5534" n="e" s="T831">потом </ts>
               <ts e="T834" id="Seg_5536" n="e" s="T832">по-камасински. </ts>
            </ts>
            <ts e="T845" id="Seg_5537" n="sc" s="T841">
               <ts e="T842" id="Seg_5539" n="e" s="T841">Мне </ts>
               <ts e="T843" id="Seg_5541" n="e" s="T842">думается, </ts>
               <ts e="T845" id="Seg_5543" n="e" s="T843">рассказали. </ts>
            </ts>
            <ts e="T849" id="Seg_5544" n="sc" s="T847">
               <ts e="T848" id="Seg_5546" n="e" s="T847">Два </ts>
               <ts e="T849" id="Seg_5548" n="e" s="T848">раз. </ts>
            </ts>
            <ts e="T859" id="Seg_5549" n="sc" s="T850">
               <ts e="T851" id="Seg_5551" n="e" s="T850">Ну, </ts>
               <ts e="T852" id="Seg_5553" n="e" s="T851">пускай </ts>
               <ts e="T853" id="Seg_5555" n="e" s="T852">расскажу </ts>
               <ts e="T854" id="Seg_5557" n="e" s="T853">опять </ts>
               <ts e="T855" id="Seg_5559" n="e" s="T854">про </ts>
               <ts e="T857" id="Seg_5561" n="e" s="T855">это </ts>
               <ts e="T859" id="Seg_5563" n="e" s="T857">же. </ts>
            </ts>
            <ts e="T866" id="Seg_5564" n="sc" s="T860">
               <ts e="T861" id="Seg_5566" n="e" s="T860">(Kamen=) </ts>
               <ts e="T862" id="Seg_5568" n="e" s="T861">Kamen </ts>
               <ts e="T863" id="Seg_5570" n="e" s="T862">šobi </ts>
               <ts e="T864" id="Seg_5572" n="e" s="T863">Александр </ts>
               <ts e="T865" id="Seg_5574" n="e" s="T864">Константинович </ts>
               <ts e="T866" id="Seg_5576" n="e" s="T865">măna… </ts>
            </ts>
            <ts e="T873" id="Seg_5577" n="sc" s="T867">
               <ts e="T868" id="Seg_5579" n="e" s="T867">Da </ts>
               <ts e="T869" id="Seg_5581" n="e" s="T868">dĭ </ts>
               <ts e="T870" id="Seg_5583" n="e" s="T869">pʼaŋdəbi </ts>
               <ts e="T871" id="Seg_5585" n="e" s="T870">măna: </ts>
               <ts e="T872" id="Seg_5587" n="e" s="T871">"Šolam </ts>
               <ts e="T873" id="Seg_5589" n="e" s="T872">tănan". </ts>
            </ts>
            <ts e="T878" id="Seg_5590" n="sc" s="T874">
               <ts e="T875" id="Seg_5592" n="e" s="T874">Măn </ts>
               <ts e="T876" id="Seg_5594" n="e" s="T875">(edəʔpiem), </ts>
               <ts e="T877" id="Seg_5596" n="e" s="T876">edəʔpiem — </ts>
               <ts e="T878" id="Seg_5598" n="e" s="T877">naga. </ts>
            </ts>
            <ts e="T886" id="Seg_5599" n="sc" s="T879">
               <ts e="T880" id="Seg_5601" n="e" s="T879">Dĭgəttə </ts>
               <ts e="T881" id="Seg_5603" n="e" s="T880">pʼaŋdlaʔbə </ts>
               <ts e="T882" id="Seg_5605" n="e" s="T881">măna </ts>
               <ts e="T883" id="Seg_5607" n="e" s="T882">sazən: </ts>
               <ts e="T884" id="Seg_5609" n="e" s="T883">"Măn </ts>
               <ts e="T885" id="Seg_5611" n="e" s="T884">ej </ts>
               <ts e="T886" id="Seg_5613" n="e" s="T885">šolam. </ts>
            </ts>
            <ts e="T895" id="Seg_5614" n="sc" s="T887">
               <ts e="T888" id="Seg_5616" n="e" s="T887">Dö </ts>
               <ts e="T889" id="Seg_5618" n="e" s="T888">((…)) </ts>
               <ts e="T890" id="Seg_5620" n="e" s="T889">dĭ </ts>
               <ts e="T891" id="Seg_5622" n="e" s="T890">kögən </ts>
               <ts e="T892" id="Seg_5624" n="e" s="T891">(šo-) </ts>
               <ts e="T893" id="Seg_5626" n="e" s="T892">šolam </ts>
               <ts e="T894" id="Seg_5628" n="e" s="T893">(šiʔ-) </ts>
               <ts e="T895" id="Seg_5630" n="e" s="T894">tănan". </ts>
            </ts>
            <ts e="T902" id="Seg_5631" n="sc" s="T896">
               <ts e="T897" id="Seg_5633" n="e" s="T896">A </ts>
               <ts e="T898" id="Seg_5635" n="e" s="T897">măn </ts>
               <ts e="T899" id="Seg_5637" n="e" s="T898">mămbiam: </ts>
               <ts e="T900" id="Seg_5639" n="e" s="T899">"Măn </ts>
               <ts e="T901" id="Seg_5641" n="e" s="T900">možet </ts>
               <ts e="T902" id="Seg_5643" n="e" s="T901">külalləm. </ts>
            </ts>
            <ts e="T907" id="Seg_5644" n="sc" s="T903">
               <ts e="T904" id="Seg_5646" n="e" s="T903">A </ts>
               <ts e="T905" id="Seg_5648" n="e" s="T904">tăn </ts>
               <ts e="T906" id="Seg_5650" n="e" s="T905">dĭgəttə </ts>
               <ts e="T907" id="Seg_5652" n="e" s="T906">šolal". </ts>
            </ts>
            <ts e="T913" id="Seg_5653" n="sc" s="T908">
               <ts e="T909" id="Seg_5655" n="e" s="T908">Dĭgəttə </ts>
               <ts e="T910" id="Seg_5657" n="e" s="T909">oʔbdəbiam </ts>
               <ts e="T911" id="Seg_5659" n="e" s="T910">da </ts>
               <ts e="T912" id="Seg_5661" n="e" s="T911">döbər </ts>
               <ts e="T913" id="Seg_5663" n="e" s="T912">šobiam. </ts>
            </ts>
            <ts e="T915" id="Seg_5664" n="sc" s="T914">
               <ts e="T915" id="Seg_5666" n="e" s="T914">((BRK)). </ts>
            </ts>
            <ts e="T920" id="Seg_5667" n="sc" s="T916">
               <ts e="T917" id="Seg_5669" n="e" s="T916">Măn </ts>
               <ts e="T918" id="Seg_5671" n="e" s="T917">koʔbdom </ts>
               <ts e="T919" id="Seg_5673" n="e" s="T918">(tibinə) </ts>
               <ts e="T920" id="Seg_5675" n="e" s="T919">kambi. </ts>
            </ts>
            <ts e="T933" id="Seg_5676" n="sc" s="T921">
               <ts e="T922" id="Seg_5678" n="e" s="T921">Ibiem </ts>
               <ts e="T923" id="Seg_5680" n="e" s="T922">Krasnăjarskagən, </ts>
               <ts e="T924" id="Seg_5682" n="e" s="T923">dĭ </ts>
               <ts e="T926" id="Seg_5684" n="e" s="T924">măndə: </ts>
               <ts e="T927" id="Seg_5686" n="e" s="T926">"Măna </ts>
               <ts e="T928" id="Seg_5688" n="e" s="T927">onʼiʔ </ts>
               <ts e="T929" id="Seg_5690" n="e" s="T928">nʼi </ts>
               <ts e="T930" id="Seg_5692" n="e" s="T929">izittə, </ts>
               <ts e="T931" id="Seg_5694" n="e" s="T930">abanə </ts>
               <ts e="T932" id="Seg_5696" n="e" s="T931">iʔ </ts>
               <ts e="T933" id="Seg_5698" n="e" s="T932">nörbit". </ts>
            </ts>
            <ts e="T950" id="Seg_5699" n="sc" s="T934">
               <ts e="T935" id="Seg_5701" n="e" s="T934">A </ts>
               <ts e="T936" id="Seg_5703" n="e" s="T935">măn </ts>
               <ts e="T938" id="Seg_5705" n="e" s="T936">mămbiam: </ts>
               <ts e="T939" id="Seg_5707" n="e" s="T938">"Kak </ts>
               <ts e="T940" id="Seg_5709" n="e" s="T939">ej </ts>
               <ts e="T941" id="Seg_5711" n="e" s="T940">nörbəsʼtə, </ts>
               <ts e="T942" id="Seg_5713" n="e" s="T941">abat </ts>
               <ts e="T943" id="Seg_5715" n="e" s="T942">bostə </ts>
               <ts e="T944" id="Seg_5717" n="e" s="T943">ibi </ts>
               <ts e="T945" id="Seg_5719" n="e" s="T944">boskəndə </ts>
               <ts e="T946" id="Seg_5721" n="e" s="T945">(i </ts>
               <ts e="T947" id="Seg_5723" n="e" s="T946">d-) </ts>
               <ts e="T948" id="Seg_5725" n="e" s="T947">i </ts>
               <ts e="T949" id="Seg_5727" n="e" s="T948">šiʔ </ts>
               <ts e="T950" id="Seg_5729" n="e" s="T949">deʔpi". </ts>
            </ts>
            <ts e="T959" id="Seg_5730" n="sc" s="T951">
               <ts e="T952" id="Seg_5732" n="e" s="T951">Dĭgəttə </ts>
               <ts e="T953" id="Seg_5734" n="e" s="T952">šobiam </ts>
               <ts e="T954" id="Seg_5736" n="e" s="T953">abanə, </ts>
               <ts e="T955" id="Seg_5738" n="e" s="T954">nörbəbiem, </ts>
               <ts e="T956" id="Seg_5740" n="e" s="T955">dĭ </ts>
               <ts e="T957" id="Seg_5742" n="e" s="T956">pʼaŋdəbi </ts>
               <ts e="T958" id="Seg_5744" n="e" s="T957">dĭʔnə </ts>
               <ts e="T959" id="Seg_5746" n="e" s="T958">sazən. </ts>
            </ts>
            <ts e="T968" id="Seg_5747" n="sc" s="T960">
               <ts e="T961" id="Seg_5749" n="e" s="T960">Dĭgəttə </ts>
               <ts e="T962" id="Seg_5751" n="e" s="T961">dĭ </ts>
               <ts e="T963" id="Seg_5753" n="e" s="T962">dĭʔnə </ts>
               <ts e="T964" id="Seg_5755" n="e" s="T963">sazən </ts>
               <ts e="T965" id="Seg_5757" n="e" s="T964">pʼaŋdəbi, </ts>
               <ts e="T966" id="Seg_5759" n="e" s="T965">dĭ </ts>
               <ts e="T967" id="Seg_5761" n="e" s="T966">döbər </ts>
               <ts e="T968" id="Seg_5763" n="e" s="T967">šobi. </ts>
            </ts>
            <ts e="T980" id="Seg_5764" n="sc" s="T969">
               <ts e="T970" id="Seg_5766" n="e" s="T969">Dĭgəttə </ts>
               <ts e="T971" id="Seg_5768" n="e" s="T970">(uja=) </ts>
               <ts e="T972" id="Seg_5770" n="e" s="T971">šoška </ts>
               <ts e="T973" id="Seg_5772" n="e" s="T972">dʼagarbibaʔ, </ts>
               <ts e="T974" id="Seg_5774" n="e" s="T973">uja, </ts>
               <ts e="T975" id="Seg_5776" n="e" s="T974">iʔgö </ts>
               <ts e="T976" id="Seg_5778" n="e" s="T975">ipek </ts>
               <ts e="T977" id="Seg_5780" n="e" s="T976">pürbibeʔ, </ts>
               <ts e="T978" id="Seg_5782" n="e" s="T977">i </ts>
               <ts e="T979" id="Seg_5784" n="e" s="T978">il </ts>
               <ts e="T980" id="Seg_5786" n="e" s="T979">oʔbdəbibaʔ. </ts>
            </ts>
            <ts e="T984" id="Seg_5787" n="sc" s="T981">
               <ts e="T982" id="Seg_5789" n="e" s="T981">Dĭgəttə </ts>
               <ts e="T983" id="Seg_5791" n="e" s="T982">ara </ts>
               <ts e="T984" id="Seg_5793" n="e" s="T983">ibi. </ts>
            </ts>
            <ts e="T992" id="Seg_5794" n="sc" s="T985">
               <ts e="T986" id="Seg_5796" n="e" s="T985">Pʼelʼmʼenʼəʔi </ts>
               <ts e="T987" id="Seg_5798" n="e" s="T986">abibaʔ, </ts>
               <ts e="T988" id="Seg_5800" n="e" s="T987">kătletaʔi. </ts>
               <ts e="T989" id="Seg_5802" n="e" s="T988">Iʔgö </ts>
               <ts e="T990" id="Seg_5804" n="e" s="T989">(ibi=) </ts>
               <ts e="T991" id="Seg_5806" n="e" s="T990">ĭmbi </ts>
               <ts e="T992" id="Seg_5808" n="e" s="T991">ibi. </ts>
            </ts>
            <ts e="T997" id="Seg_5809" n="sc" s="T993">
               <ts e="T994" id="Seg_5811" n="e" s="T993">I </ts>
               <ts e="T995" id="Seg_5813" n="e" s="T994">il </ts>
               <ts e="T996" id="Seg_5815" n="e" s="T995">iʔgö </ts>
               <ts e="T997" id="Seg_5817" n="e" s="T996">ibiʔi. </ts>
            </ts>
            <ts e="T1000" id="Seg_5818" n="sc" s="T998">
               <ts e="T1525" id="Seg_5820" n="e" s="T998">Kazan </ts>
               <ts e="T999" id="Seg_5822" n="e" s="T1525">turagən </ts>
               <ts e="T1000" id="Seg_5824" n="e" s="T999">šobiʔi. </ts>
            </ts>
            <ts e="T1006" id="Seg_5825" n="sc" s="T1001">
               <ts e="T1002" id="Seg_5827" n="e" s="T1001">Dĭgəttə </ts>
               <ts e="T1003" id="Seg_5829" n="e" s="T1002">măn </ts>
               <ts e="T1004" id="Seg_5831" n="e" s="T1003">dĭʔnə </ts>
               <ts e="T1005" id="Seg_5833" n="e" s="T1004">ibiem </ts>
               <ts e="T1006" id="Seg_5835" n="e" s="T1005">palʼto. </ts>
            </ts>
            <ts e="T1012" id="Seg_5836" n="sc" s="T1007">
               <ts e="T1008" id="Seg_5838" n="e" s="T1007">Dĭgəttə </ts>
               <ts e="T1009" id="Seg_5840" n="e" s="T1008">aktʼa </ts>
               <ts e="T1010" id="Seg_5842" n="e" s="T1009">dĭʔnə </ts>
               <ts e="T1011" id="Seg_5844" n="e" s="T1010">(ak-) </ts>
               <ts e="T1012" id="Seg_5846" n="e" s="T1011">mĭbiem. </ts>
            </ts>
            <ts e="T1017" id="Seg_5847" n="sc" s="T1013">
               <ts e="T1014" id="Seg_5849" n="e" s="T1013">Dĭ </ts>
               <ts e="T1522" id="Seg_5851" n="e" s="T1014">kalla </ts>
               <ts e="T1015" id="Seg_5853" n="e" s="T1522">dʼürbi </ts>
               <ts e="T1016" id="Seg_5855" n="e" s="T1015">dĭ </ts>
               <ts e="T1017" id="Seg_5857" n="e" s="T1016">nʼizi. </ts>
            </ts>
            <ts e="T1028" id="Seg_5858" n="sc" s="T1018">
               <ts e="T1019" id="Seg_5860" n="e" s="T1018">Dĭbər, </ts>
               <ts e="T1020" id="Seg_5862" n="e" s="T1019">(gijen </ts>
               <ts e="T1021" id="Seg_5864" n="e" s="T1020">= </ts>
               <ts e="T1022" id="Seg_5866" n="e" s="T1021">il=) </ts>
               <ts e="T1023" id="Seg_5868" n="e" s="T1022">gijen </ts>
               <ts e="T1024" id="Seg_5870" n="e" s="T1023">il </ts>
               <ts e="T1025" id="Seg_5872" n="e" s="T1024">(dʼăbaktərzi-) </ts>
               <ts e="T1026" id="Seg_5874" n="e" s="T1025">dʼăbaktəriaʔi </ts>
               <ts e="T1027" id="Seg_5876" n="e" s="T1026">bostə </ts>
               <ts e="T1028" id="Seg_5878" n="e" s="T1027">šĭketsi. </ts>
            </ts>
            <ts e="T1049" id="Seg_5879" n="sc" s="T1029">
               <ts e="T1030" id="Seg_5881" n="e" s="T1029">Dĭgəttə </ts>
               <ts e="T1031" id="Seg_5883" n="e" s="T1030">(dĭ=) </ts>
               <ts e="T1032" id="Seg_5885" n="e" s="T1031">dĭ </ts>
               <ts e="T1033" id="Seg_5887" n="e" s="T1032">nʼin </ts>
               <ts e="T1034" id="Seg_5889" n="e" s="T1033">ia </ts>
               <ts e="T1035" id="Seg_5891" n="e" s="T1034">(dĭ- </ts>
               <ts e="T1036" id="Seg_5893" n="e" s="T1035">dĭm=) </ts>
               <ts e="T1037" id="Seg_5895" n="e" s="T1036">dĭm </ts>
               <ts e="T1038" id="Seg_5897" n="e" s="T1037">ej </ts>
               <ts e="T1039" id="Seg_5899" n="e" s="T1038">ajirbi, </ts>
               <ts e="T1040" id="Seg_5901" n="e" s="T1039">dĭgəttə </ts>
               <ts e="T1041" id="Seg_5903" n="e" s="T1040">dĭ </ts>
               <ts e="T1042" id="Seg_5905" n="e" s="T1041">döbər </ts>
               <ts e="T1044" id="Seg_5907" n="e" s="T1042">pʼaŋdlaʔbə: </ts>
               <ts e="T1045" id="Seg_5909" n="e" s="T1044">"(Kažen-) </ts>
               <ts e="T1046" id="Seg_5911" n="e" s="T1045">Kažnej </ts>
               <ts e="T1047" id="Seg_5913" n="e" s="T1046">(dʼan-) </ts>
               <ts e="T1048" id="Seg_5915" n="e" s="T1047">dʼala </ts>
               <ts e="T1049" id="Seg_5917" n="e" s="T1048">dʼorlaʔbəm. </ts>
            </ts>
            <ts e="T1057" id="Seg_5918" n="sc" s="T1050">
               <ts e="T1051" id="Seg_5920" n="e" s="T1050">Miʔ </ts>
               <ts e="T1052" id="Seg_5922" n="e" s="T1051">стали </ts>
               <ts e="T1053" id="Seg_5924" n="e" s="T1052">dĭʔnə </ts>
               <ts e="T1055" id="Seg_5926" n="e" s="T1053">pʼaŋdəsʼtə: </ts>
               <ts e="T1056" id="Seg_5928" n="e" s="T1055">"Šoʔ </ts>
               <ts e="T1057" id="Seg_5930" n="e" s="T1056">maʔnəl". </ts>
            </ts>
            <ts e="T1064" id="Seg_5931" n="sc" s="T1058">
               <ts e="T1059" id="Seg_5933" n="e" s="T1058">Dĭgəttə </ts>
               <ts e="T1060" id="Seg_5935" n="e" s="T1059">dĭ </ts>
               <ts e="T1061" id="Seg_5937" n="e" s="T1060">măndə: </ts>
               <ts e="T1062" id="Seg_5939" n="e" s="T1061">"Măn </ts>
               <ts e="T1063" id="Seg_5941" n="e" s="T1062">unnʼa </ts>
               <ts e="T1064" id="Seg_5943" n="e" s="T1063">šolam". </ts>
            </ts>
            <ts e="T1069" id="Seg_5944" n="sc" s="T1065">
               <ts e="T1066" id="Seg_5946" n="e" s="T1065">"A </ts>
               <ts e="T1067" id="Seg_5948" n="e" s="T1066">ĭmbi </ts>
               <ts e="T1068" id="Seg_5950" n="e" s="T1067">unnʼa </ts>
               <ts e="T1069" id="Seg_5952" n="e" s="T1068">šolal?" </ts>
            </ts>
            <ts e="T1076" id="Seg_5953" n="sc" s="T1070">
               <ts e="T1071" id="Seg_5955" n="e" s="T1070">A </ts>
               <ts e="T1072" id="Seg_5957" n="e" s="T1071">dĭ </ts>
               <ts e="T1073" id="Seg_5959" n="e" s="T1072">bazo </ts>
               <ts e="T1074" id="Seg_5961" n="e" s="T1073">pʼaŋdlaʔbə: </ts>
               <ts e="T1075" id="Seg_5963" n="e" s="T1074">"Aktʼa </ts>
               <ts e="T1076" id="Seg_5965" n="e" s="T1075">naga". </ts>
            </ts>
            <ts e="T1085" id="Seg_5966" n="sc" s="T1077">
               <ts e="T1078" id="Seg_5968" n="e" s="T1077">Măn </ts>
               <ts e="T1079" id="Seg_5970" n="e" s="T1078">bazo </ts>
               <ts e="T1080" id="Seg_5972" n="e" s="T1079">öʔlubiem </ts>
               <ts e="T1081" id="Seg_5974" n="e" s="T1080">(šide </ts>
               <ts e="T1082" id="Seg_5976" n="e" s="T1081">bʼeʔ=) </ts>
               <ts e="T1083" id="Seg_5978" n="e" s="T1082">šide </ts>
               <ts e="T1084" id="Seg_5980" n="e" s="T1083">biʔ </ts>
               <ts e="T1085" id="Seg_5982" n="e" s="T1084">aktʼa. </ts>
            </ts>
            <ts e="T1092" id="Seg_5983" n="sc" s="T1086">
               <ts e="T1087" id="Seg_5985" n="e" s="T1086">Dĭgəttə </ts>
               <ts e="T1088" id="Seg_5987" n="e" s="T1087">abat </ts>
               <ts e="T1089" id="Seg_5989" n="e" s="T1088">sumna </ts>
               <ts e="T1090" id="Seg_5991" n="e" s="T1089">(šide </ts>
               <ts e="T1091" id="Seg_5993" n="e" s="T1090">biʔ) </ts>
               <ts e="T1092" id="Seg_5995" n="e" s="T1091">öʔlubi. </ts>
            </ts>
            <ts e="T1097" id="Seg_5996" n="sc" s="T1093">
               <ts e="T1094" id="Seg_5998" n="e" s="T1093">Dĭgəttə </ts>
               <ts e="T1095" id="Seg_6000" n="e" s="T1094">dö </ts>
               <ts e="T1096" id="Seg_6002" n="e" s="T1095">šobi </ts>
               <ts e="T1097" id="Seg_6004" n="e" s="T1096">onʼiʔ. </ts>
            </ts>
            <ts e="T1100" id="Seg_6005" n="sc" s="T1098">
               <ts e="T1099" id="Seg_6007" n="e" s="T1098">Dĭ </ts>
               <ts e="T1100" id="Seg_6009" n="e" s="T1099">maːluʔpi. </ts>
            </ts>
            <ts e="T1107" id="Seg_6010" n="sc" s="T1101">
               <ts e="T1102" id="Seg_6012" n="e" s="T1101">Dĭgəttə </ts>
               <ts e="T1103" id="Seg_6014" n="e" s="T1102">(dĭ=) </ts>
               <ts e="T1104" id="Seg_6016" n="e" s="T1103">dĭʔnə </ts>
               <ts e="T1105" id="Seg_6018" n="e" s="T1104">dĭ </ts>
               <ts e="T1106" id="Seg_6020" n="e" s="T1105">pʼaŋdəbi, </ts>
               <ts e="T1107" id="Seg_6022" n="e" s="T1106">pʼaŋdəbi. </ts>
            </ts>
            <ts e="T1113" id="Seg_6023" n="sc" s="T1108">
               <ts e="T1109" id="Seg_6025" n="e" s="T1108">Dĭ </ts>
               <ts e="T1110" id="Seg_6027" n="e" s="T1109">(ej=) </ts>
               <ts e="T1111" id="Seg_6029" n="e" s="T1110">üge </ts>
               <ts e="T1112" id="Seg_6031" n="e" s="T1111">ej </ts>
               <ts e="T1113" id="Seg_6033" n="e" s="T1112">šolia. </ts>
            </ts>
            <ts e="T1117" id="Seg_6034" n="sc" s="T1114">
               <ts e="T1115" id="Seg_6036" n="e" s="T1114">Dĭgəttə </ts>
               <ts e="T1116" id="Seg_6038" n="e" s="T1115">dĭ </ts>
               <ts e="T1523" id="Seg_6040" n="e" s="T1116">kalla </ts>
               <ts e="T1117" id="Seg_6042" n="e" s="T1523">dʼürbi. </ts>
            </ts>
            <ts e="T1124" id="Seg_6043" n="sc" s="T1118">
               <ts e="T1119" id="Seg_6045" n="e" s="T1118">(Gil-) </ts>
               <ts e="T1120" id="Seg_6047" n="e" s="T1119">Gijen </ts>
               <ts e="T1121" id="Seg_6049" n="e" s="T1120">Lenin </ts>
               <ts e="T1122" id="Seg_6051" n="e" s="T1121">amnobi, </ts>
               <ts e="T1123" id="Seg_6053" n="e" s="T1122">dĭ </ts>
               <ts e="T1124" id="Seg_6055" n="e" s="T1123">turanə. </ts>
            </ts>
            <ts e="T1139" id="Seg_6056" n="sc" s="T1125">
               <ts e="T1126" id="Seg_6058" n="e" s="T1125">Dĭn </ts>
               <ts e="T1127" id="Seg_6060" n="e" s="T1126">stălovăjăn </ts>
               <ts e="T1128" id="Seg_6062" n="e" s="T1127">(tob- </ts>
               <ts e="T1129" id="Seg_6064" n="e" s="T1128">tomn-) </ts>
               <ts e="T1130" id="Seg_6066" n="e" s="T1129">stălovăjăn </ts>
               <ts e="T1131" id="Seg_6068" n="e" s="T1130">((…)) </ts>
               <ts e="T1132" id="Seg_6070" n="e" s="T1131">ipek </ts>
               <ts e="T1133" id="Seg_6072" n="e" s="T1132">mĭlie, </ts>
               <ts e="T1134" id="Seg_6074" n="e" s="T1133">ara </ts>
               <ts e="T1135" id="Seg_6076" n="e" s="T1134">mĭlie, </ts>
               <ts e="T1136" id="Seg_6078" n="e" s="T1135">bar </ts>
               <ts e="T1137" id="Seg_6080" n="e" s="T1136">ĭmbi </ts>
               <ts e="T1138" id="Seg_6082" n="e" s="T1137">mĭlie, </ts>
               <ts e="T1139" id="Seg_6084" n="e" s="T1138">pʼaŋdlaʔbə. </ts>
            </ts>
            <ts e="T1148" id="Seg_6085" n="sc" s="T1140">
               <ts e="T1141" id="Seg_6087" n="e" s="T1140">(Bʼeʔ </ts>
               <ts e="T1142" id="Seg_6089" n="e" s="T1141">sumn-) </ts>
               <ts e="T1143" id="Seg_6091" n="e" s="T1142">Bʼeʔ </ts>
               <ts e="T1144" id="Seg_6093" n="e" s="T1143">nagur </ts>
               <ts e="T1145" id="Seg_6095" n="e" s="T1144">i </ts>
               <ts e="T1146" id="Seg_6097" n="e" s="T1145">sumna </ts>
               <ts e="T1147" id="Seg_6099" n="e" s="T1146">aktʼa </ts>
               <ts e="T1148" id="Seg_6101" n="e" s="T1147">iliet. </ts>
            </ts>
            <ts e="T1151" id="Seg_6102" n="sc" s="T1149">
               <ts e="T1150" id="Seg_6104" n="e" s="T1149">По-русски </ts>
               <ts e="T1151" id="Seg_6106" n="e" s="T1150">можно. </ts>
            </ts>
            <ts e="T1180" id="Seg_6107" n="sc" s="T1165">
               <ts e="T1166" id="Seg_6109" n="e" s="T1165">Моя </ts>
               <ts e="T1167" id="Seg_6111" n="e" s="T1166">племянница, </ts>
               <ts e="T1168" id="Seg_6113" n="e" s="T1167">была </ts>
               <ts e="T1169" id="Seg_6115" n="e" s="T1168">я </ts>
               <ts e="T1170" id="Seg_6117" n="e" s="T1169">в </ts>
               <ts e="T1171" id="Seg_6119" n="e" s="T1170">(Красноярскем), </ts>
               <ts e="T1172" id="Seg_6121" n="e" s="T1171">она </ts>
               <ts e="T1174" id="Seg_6123" n="e" s="T1172">говорит: </ts>
               <ts e="T1175" id="Seg_6125" n="e" s="T1174">"Пишет </ts>
               <ts e="T1176" id="Seg_6127" n="e" s="T1175">мне </ts>
               <ts e="T1177" id="Seg_6129" n="e" s="T1176">парень, </ts>
               <ts e="T1178" id="Seg_6131" n="e" s="T1177">чтобы </ts>
               <ts e="T1179" id="Seg_6133" n="e" s="T1178">я </ts>
               <ts e="T1180" id="Seg_6135" n="e" s="T1179">поехала. </ts>
            </ts>
            <ts e="T1195" id="Seg_6136" n="sc" s="T1181">
               <ts e="T1182" id="Seg_6138" n="e" s="T1181">Да, </ts>
               <ts e="T1183" id="Seg_6140" n="e" s="T1182">говорит, </ts>
               <ts e="T1184" id="Seg_6142" n="e" s="T1183">отцу </ts>
               <ts e="T1185" id="Seg_6144" n="e" s="T1184">не </ts>
               <ts e="T1186" id="Seg_6146" n="e" s="T1185">сказывай", </ts>
               <ts e="T1187" id="Seg_6148" n="e" s="T1186">а </ts>
               <ts e="T1188" id="Seg_6150" n="e" s="T1187">я </ts>
               <ts e="T1189" id="Seg_6152" n="e" s="T1188">(ему </ts>
               <ts e="T1190" id="Seg_6154" n="e" s="T1189">сказала=) </ts>
               <ts e="T1191" id="Seg_6156" n="e" s="T1190">ей </ts>
               <ts e="T1192" id="Seg_6158" n="e" s="T1191">сказала:" </ts>
               <ts e="T1193" id="Seg_6160" n="e" s="T1192">Почему </ts>
               <ts e="T1194" id="Seg_6162" n="e" s="T1193">не </ts>
               <ts e="T1195" id="Seg_6164" n="e" s="T1194">сказывай? </ts>
            </ts>
            <ts e="T1204" id="Seg_6165" n="sc" s="T1196">
               <ts e="T1197" id="Seg_6167" n="e" s="T1196">Отец </ts>
               <ts e="T1198" id="Seg_6169" n="e" s="T1197">сам </ts>
               <ts e="T1199" id="Seg_6171" n="e" s="T1198">женился, </ts>
               <ts e="T1200" id="Seg_6173" n="e" s="T1199">и </ts>
               <ts e="T1201" id="Seg_6175" n="e" s="T1200">вас </ts>
               <ts e="T1202" id="Seg_6177" n="e" s="T1201">напустил </ts>
               <ts e="T1203" id="Seg_6179" n="e" s="T1202">на </ts>
               <ts e="T1204" id="Seg_6181" n="e" s="T1203">свет". </ts>
            </ts>
            <ts e="T1214" id="Seg_6182" n="sc" s="T1205">
               <ts e="T1206" id="Seg_6184" n="e" s="T1205">Я </ts>
               <ts e="T1207" id="Seg_6186" n="e" s="T1206">приехала, </ts>
               <ts e="T1208" id="Seg_6188" n="e" s="T1207">отцу </ts>
               <ts e="T1209" id="Seg_6190" n="e" s="T1208">сказала, </ts>
               <ts e="T1210" id="Seg_6192" n="e" s="T1209">он </ts>
               <ts e="T1211" id="Seg_6194" n="e" s="T1210">стал </ts>
               <ts e="T1212" id="Seg_6196" n="e" s="T1211">ей </ts>
               <ts e="T1213" id="Seg_6198" n="e" s="T1212">писать, </ts>
               <ts e="T1214" id="Seg_6200" n="e" s="T1213">разрешил. </ts>
            </ts>
            <ts e="T1229" id="Seg_6201" n="sc" s="T1215">
               <ts e="T1216" id="Seg_6203" n="e" s="T1215">(И </ts>
               <ts e="T1217" id="Seg_6205" n="e" s="T1216">он </ts>
               <ts e="T1218" id="Seg_6207" n="e" s="T1217">собра-) </ts>
               <ts e="T1219" id="Seg_6209" n="e" s="T1218">Она </ts>
               <ts e="T1220" id="Seg_6211" n="e" s="T1219">ему </ts>
               <ts e="T1221" id="Seg_6213" n="e" s="T1220">тоже </ts>
               <ts e="T1222" id="Seg_6215" n="e" s="T1221">написала, </ts>
               <ts e="T1223" id="Seg_6217" n="e" s="T1222">он </ts>
               <ts e="T1224" id="Seg_6219" n="e" s="T1223">собрался, </ts>
               <ts e="T1225" id="Seg_6221" n="e" s="T1224">приехал, </ts>
               <ts e="T1226" id="Seg_6223" n="e" s="T1225">приехали </ts>
               <ts e="T1227" id="Seg_6225" n="e" s="T1226">сюды </ts>
               <ts e="T1228" id="Seg_6227" n="e" s="T1227">к </ts>
               <ts e="T1229" id="Seg_6229" n="e" s="T1228">нам. </ts>
            </ts>
            <ts e="T1233" id="Seg_6230" n="sc" s="T1230">
               <ts e="T1231" id="Seg_6232" n="e" s="T1230">Мы </ts>
               <ts e="T1232" id="Seg_6234" n="e" s="T1231">закололи </ts>
               <ts e="T1233" id="Seg_6236" n="e" s="T1232">свинью. </ts>
            </ts>
            <ts e="T1237" id="Seg_6237" n="sc" s="T1234">
               <ts e="T1235" id="Seg_6239" n="e" s="T1234">И </ts>
               <ts e="T1236" id="Seg_6241" n="e" s="T1235">всего </ts>
               <ts e="T1237" id="Seg_6243" n="e" s="T1236">наготовили. </ts>
            </ts>
            <ts e="T1242" id="Seg_6244" n="sc" s="T1238">
               <ts e="T1239" id="Seg_6246" n="e" s="T1238">Пельменев, </ts>
               <ts e="T1240" id="Seg_6248" n="e" s="T1239">и </ts>
               <ts e="T1241" id="Seg_6250" n="e" s="T1240">котлет, </ts>
               <ts e="T1242" id="Seg_6252" n="e" s="T1241">всего. </ts>
            </ts>
            <ts e="T1247" id="Seg_6253" n="sc" s="T1243">
               <ts e="T1244" id="Seg_6255" n="e" s="T1243">И </ts>
               <ts e="T1245" id="Seg_6257" n="e" s="T1244">много </ts>
               <ts e="T1246" id="Seg_6259" n="e" s="T1245">людей </ts>
               <ts e="T1247" id="Seg_6261" n="e" s="T1246">было. </ts>
            </ts>
            <ts e="T1254" id="Seg_6262" n="sc" s="T1248">
               <ts e="T1249" id="Seg_6264" n="e" s="T1248">А </ts>
               <ts e="T1250" id="Seg_6266" n="e" s="T1249">потом </ts>
               <ts e="T1251" id="Seg_6268" n="e" s="T1250">я </ts>
               <ts e="T1252" id="Seg_6270" n="e" s="T1251">купила </ts>
               <ts e="T1253" id="Seg_6272" n="e" s="T1252">ей </ts>
               <ts e="T1254" id="Seg_6274" n="e" s="T1253">пальто. </ts>
            </ts>
            <ts e="T1262" id="Seg_6275" n="sc" s="T1255">
               <ts e="T1256" id="Seg_6277" n="e" s="T1255">Тогда </ts>
               <ts e="T1257" id="Seg_6279" n="e" s="T1256">на </ts>
               <ts e="T1258" id="Seg_6281" n="e" s="T1257">дорогу </ts>
               <ts e="T1259" id="Seg_6283" n="e" s="T1258">дала </ts>
               <ts e="T1260" id="Seg_6285" n="e" s="T1259">денег </ts>
               <ts e="T1261" id="Seg_6287" n="e" s="T1260">сто </ts>
               <ts e="T1262" id="Seg_6289" n="e" s="T1261">рублей. </ts>
            </ts>
            <ts e="T1269" id="Seg_6290" n="sc" s="T1263">
               <ts e="T1264" id="Seg_6292" n="e" s="T1263">И </ts>
               <ts e="T1265" id="Seg_6294" n="e" s="T1264">опять </ts>
               <ts e="T1266" id="Seg_6296" n="e" s="T1265">она </ts>
               <ts e="T1267" id="Seg_6298" n="e" s="T1266">(та-) </ts>
               <ts e="T1268" id="Seg_6300" n="e" s="T1267">туды </ts>
               <ts e="T1269" id="Seg_6302" n="e" s="T1268">уехала. </ts>
            </ts>
            <ts e="T1278" id="Seg_6303" n="sc" s="T1270">
               <ts e="T1271" id="Seg_6305" n="e" s="T1270">Свекровка </ts>
               <ts e="T1272" id="Seg_6307" n="e" s="T1271">ее </ts>
               <ts e="T1273" id="Seg_6309" n="e" s="T1272">не </ts>
               <ts e="T1274" id="Seg_6311" n="e" s="T1273">залюбила, </ts>
               <ts e="T1275" id="Seg_6313" n="e" s="T1274">она </ts>
               <ts e="T1276" id="Seg_6315" n="e" s="T1275">пишет, </ts>
               <ts e="T1277" id="Seg_6317" n="e" s="T1276">что </ts>
               <ts e="T1278" id="Seg_6319" n="e" s="T1277">плачу. </ts>
            </ts>
            <ts e="T1289" id="Seg_6320" n="sc" s="T1279">
               <ts e="T1280" id="Seg_6322" n="e" s="T1279">(А=) </ts>
               <ts e="T1281" id="Seg_6324" n="e" s="T1280">А </ts>
               <ts e="T1282" id="Seg_6326" n="e" s="T1281">мы </ts>
               <ts e="T1283" id="Seg_6328" n="e" s="T1282">написали </ts>
               <ts e="T1285" id="Seg_6330" n="e" s="T1283">ей: </ts>
               <ts e="T1286" id="Seg_6332" n="e" s="T1285">"Чем </ts>
               <ts e="T1287" id="Seg_6334" n="e" s="T1286">плакать, </ts>
               <ts e="T1288" id="Seg_6336" n="e" s="T1287">приезжай </ts>
               <ts e="T1289" id="Seg_6338" n="e" s="T1288">сюды". </ts>
            </ts>
            <ts e="T1295" id="Seg_6339" n="sc" s="T1290">
               <ts e="T1291" id="Seg_6341" n="e" s="T1290">Ну, </ts>
               <ts e="T1292" id="Seg_6343" n="e" s="T1291">она </ts>
               <ts e="T1293" id="Seg_6345" n="e" s="T1292">говорила, </ts>
               <ts e="T1294" id="Seg_6347" n="e" s="T1293">вдвоем </ts>
               <ts e="T1295" id="Seg_6349" n="e" s="T1294">приедут. </ts>
            </ts>
            <ts e="T1306" id="Seg_6350" n="sc" s="T1296">
               <ts e="T1297" id="Seg_6352" n="e" s="T1296">Ну, </ts>
               <ts e="T1298" id="Seg_6354" n="e" s="T1297">а </ts>
               <ts e="T1299" id="Seg_6356" n="e" s="T1298">потом </ts>
               <ts e="T1300" id="Seg_6358" n="e" s="T1299">говорит: </ts>
               <ts e="T1301" id="Seg_6360" n="e" s="T1300">"Я </ts>
               <ts e="T1302" id="Seg_6362" n="e" s="T1301">одна </ts>
               <ts e="T1303" id="Seg_6364" n="e" s="T1302">приеду", </ts>
               <ts e="T1304" id="Seg_6366" n="e" s="T1303">(на-) </ts>
               <ts e="T1305" id="Seg_6368" n="e" s="T1304">в </ts>
               <ts e="T1306" id="Seg_6370" n="e" s="T1305">письме. </ts>
            </ts>
            <ts e="T1313" id="Seg_6371" n="sc" s="T1307">
               <ts e="T1308" id="Seg_6373" n="e" s="T1307">А </ts>
               <ts e="T1309" id="Seg_6375" n="e" s="T1308">мы </ts>
               <ts e="T1310" id="Seg_6377" n="e" s="T1309">говорим: </ts>
               <ts e="T1311" id="Seg_6379" n="e" s="T1310">"Почему </ts>
               <ts e="T1312" id="Seg_6381" n="e" s="T1311">одна </ts>
               <ts e="T1313" id="Seg_6383" n="e" s="T1312">приедешь?" </ts>
            </ts>
            <ts e="T1326" id="Seg_6384" n="sc" s="T1314">
               <ts e="T1315" id="Seg_6386" n="e" s="T1314">"Ну, </ts>
               <ts e="T1316" id="Seg_6388" n="e" s="T1315">говорит, </ts>
               <ts e="T1317" id="Seg_6390" n="e" s="T1316">денег </ts>
               <ts e="T1318" id="Seg_6392" n="e" s="T1317">нету". </ts>
               <ts e="T1319" id="Seg_6394" n="e" s="T1318">Я </ts>
               <ts e="T1320" id="Seg_6396" n="e" s="T1319">опять </ts>
               <ts e="T1321" id="Seg_6398" n="e" s="T1320">сто </ts>
               <ts e="T1322" id="Seg_6400" n="e" s="T1321">рублей </ts>
               <ts e="T1323" id="Seg_6402" n="e" s="T1322">послала, </ts>
               <ts e="T1324" id="Seg_6404" n="e" s="T1323">и </ts>
               <ts e="T1325" id="Seg_6406" n="e" s="T1324">отец </ts>
               <ts e="T1326" id="Seg_6408" n="e" s="T1325">пятьдесят. </ts>
            </ts>
            <ts e="T1331" id="Seg_6409" n="sc" s="T1327">
               <ts e="T1328" id="Seg_6411" n="e" s="T1327">Ну, </ts>
               <ts e="T1329" id="Seg_6413" n="e" s="T1328">она </ts>
               <ts e="T1330" id="Seg_6415" n="e" s="T1329">приехала </ts>
               <ts e="T1331" id="Seg_6417" n="e" s="T1330">одна. </ts>
            </ts>
            <ts e="T1335" id="Seg_6418" n="sc" s="T1332">
               <ts e="T1333" id="Seg_6420" n="e" s="T1332">А </ts>
               <ts e="T1334" id="Seg_6422" n="e" s="T1333">он </ts>
               <ts e="T1335" id="Seg_6424" n="e" s="T1334">остался. </ts>
            </ts>
            <ts e="T1341" id="Seg_6425" n="sc" s="T1336">
               <ts e="T1337" id="Seg_6427" n="e" s="T1336">Она </ts>
               <ts e="T1338" id="Seg_6429" n="e" s="T1337">опять </ts>
               <ts e="T1339" id="Seg_6431" n="e" s="T1338">ему </ts>
               <ts e="T1340" id="Seg_6433" n="e" s="T1339">письмо </ts>
               <ts e="T1341" id="Seg_6435" n="e" s="T1340">писала. </ts>
            </ts>
            <ts e="T1347" id="Seg_6436" n="sc" s="T1342">
               <ts e="T1343" id="Seg_6438" n="e" s="T1342">Не </ts>
               <ts e="T1344" id="Seg_6440" n="e" s="T1343">знаю, </ts>
               <ts e="T1345" id="Seg_6442" n="e" s="T1344">приедет </ts>
               <ts e="T1346" id="Seg_6444" n="e" s="T1345">ли, </ts>
               <ts e="T1347" id="Seg_6446" n="e" s="T1346">нет. </ts>
            </ts>
            <ts e="T1364" id="Seg_6447" n="sc" s="T1348">
               <ts e="T1349" id="Seg_6449" n="e" s="T1348">А </ts>
               <ts e="T1350" id="Seg_6451" n="e" s="T1349">потом </ts>
               <ts e="T1351" id="Seg_6453" n="e" s="T1350">она </ts>
               <ts e="T1352" id="Seg_6455" n="e" s="T1351">уехала </ts>
               <ts e="T1353" id="Seg_6457" n="e" s="T1352">туды, </ts>
               <ts e="T1354" id="Seg_6459" n="e" s="T1353">где </ts>
               <ts e="T1355" id="Seg_6461" n="e" s="T1354">Ленин </ts>
               <ts e="T1356" id="Seg_6463" n="e" s="T1355">был </ts>
               <ts e="T1357" id="Seg_6465" n="e" s="T1356">в </ts>
               <ts e="T1358" id="Seg_6467" n="e" s="T1357">ссылке, </ts>
               <ts e="T1359" id="Seg_6469" n="e" s="T1358">(в </ts>
               <ts e="T1360" id="Seg_6471" n="e" s="T1359">этой </ts>
               <ts e="T1361" id="Seg_6473" n="e" s="T1360">д-) </ts>
               <ts e="T1362" id="Seg_6475" n="e" s="T1361">в </ts>
               <ts e="T1363" id="Seg_6477" n="e" s="T1362">этот </ts>
               <ts e="T1364" id="Seg_6479" n="e" s="T1363">город. </ts>
            </ts>
            <ts e="T1370" id="Seg_6480" n="sc" s="T1365">
               <ts e="T1366" id="Seg_6482" n="e" s="T1365">И </ts>
               <ts e="T1367" id="Seg_6484" n="e" s="T1366">там </ts>
               <ts e="T1368" id="Seg_6486" n="e" s="T1367">в </ts>
               <ts e="T1369" id="Seg_6488" n="e" s="T1368">столовой </ts>
               <ts e="T1370" id="Seg_6490" n="e" s="T1369">работает. </ts>
            </ts>
            <ts e="T1379" id="Seg_6491" n="sc" s="T1371">
               <ts e="T1372" id="Seg_6493" n="e" s="T1371">И </ts>
               <ts e="T1373" id="Seg_6495" n="e" s="T1372">(это=) </ts>
               <ts e="T1374" id="Seg_6497" n="e" s="T1373">выдает </ts>
               <ts e="T1375" id="Seg_6499" n="e" s="T1374">там </ts>
               <ts e="T1376" id="Seg_6501" n="e" s="T1375">водки, </ts>
               <ts e="T1377" id="Seg_6503" n="e" s="T1376">всего, </ts>
               <ts e="T1378" id="Seg_6505" n="e" s="T1377">чего </ts>
               <ts e="T1379" id="Seg_6507" n="e" s="T1378">кушать. </ts>
            </ts>
            <ts e="T1381" id="Seg_6508" n="sc" s="T1380">
               <ts e="T1381" id="Seg_6510" n="e" s="T1380">Всё. </ts>
            </ts>
            <ts e="T1390" id="Seg_6511" n="sc" s="T1382">
               <ts e="T1383" id="Seg_6513" n="e" s="T1382">((BRK)). </ts>
               <ts e="T1384" id="Seg_6515" n="e" s="T1383">Погодите, </ts>
               <ts e="T1385" id="Seg_6517" n="e" s="T1384">я </ts>
               <ts e="T1386" id="Seg_6519" n="e" s="T1385">может, </ts>
               <ts e="T1387" id="Seg_6521" n="e" s="T1386">(на-) </ts>
               <ts e="T1388" id="Seg_6523" n="e" s="T1387">на </ts>
               <ts e="T1390" id="Seg_6525" n="e" s="T1388">это… </ts>
            </ts>
            <ts e="T1396" id="Seg_6526" n="sc" s="T1391">
               <ts e="T1392" id="Seg_6528" n="e" s="T1391">Măn </ts>
               <ts e="T1393" id="Seg_6530" n="e" s="T1392">plʼemʼannican </ts>
               <ts e="T1394" id="Seg_6532" n="e" s="T1393">koʔbdot </ts>
               <ts e="T1395" id="Seg_6534" n="e" s="T1394">tibinə </ts>
               <ts e="T1524" id="Seg_6536" n="e" s="T1395">kalla </ts>
               <ts e="T1396" id="Seg_6538" n="e" s="T1524">dʼürbi. </ts>
            </ts>
            <ts e="T1402" id="Seg_6539" n="sc" s="T1397">
               <ts e="T1398" id="Seg_6541" n="e" s="T1397">I </ts>
               <ts e="T1399" id="Seg_6543" n="e" s="T1398">dĭn </ts>
               <ts e="T1400" id="Seg_6545" n="e" s="T1399">sumna </ts>
               <ts e="T1401" id="Seg_6547" n="e" s="T1400">esseŋ </ts>
               <ts e="T1402" id="Seg_6549" n="e" s="T1401">ibiʔi. </ts>
            </ts>
            <ts e="T1410" id="Seg_6550" n="sc" s="T1403">
               <ts e="T1404" id="Seg_6552" n="e" s="T1403">Dĭgəttə </ts>
               <ts e="T1405" id="Seg_6554" n="e" s="T1404">(dĭ=) </ts>
               <ts e="T1406" id="Seg_6556" n="e" s="T1405">dĭzeŋ </ts>
               <ts e="T1407" id="Seg_6558" n="e" s="T1406">kambiʔi </ts>
               <ts e="T1408" id="Seg_6560" n="e" s="T1407">(maʔ=) </ts>
               <ts e="T1409" id="Seg_6562" n="e" s="T1408">maːʔnə </ts>
               <ts e="T1410" id="Seg_6564" n="e" s="T1409">dĭn. </ts>
            </ts>
            <ts e="T1414" id="Seg_6565" n="sc" s="T1411">
               <ts e="T1412" id="Seg_6567" n="e" s="T1411">I </ts>
               <ts e="T1413" id="Seg_6569" n="e" s="T1412">ige </ts>
               <ts e="T1414" id="Seg_6571" n="e" s="T1413">turajʔi. </ts>
            </ts>
            <ts e="T1422" id="Seg_6572" n="sc" s="T1415">
               <ts e="T1416" id="Seg_6574" n="e" s="T1415">Dĭgəttə </ts>
               <ts e="T1417" id="Seg_6576" n="e" s="T1416">dĭzeŋ </ts>
               <ts e="T1418" id="Seg_6578" n="e" s="T1417">aʔtʼi </ts>
               <ts e="T1419" id="Seg_6580" n="e" s="T1418">abiʔi, </ts>
               <ts e="T1420" id="Seg_6582" n="e" s="T1419">gijen </ts>
               <ts e="T1421" id="Seg_6584" n="e" s="T1420">bazaj </ts>
               <ts e="T1422" id="Seg_6586" n="e" s="T1421">aktʼi. </ts>
            </ts>
            <ts e="T1433" id="Seg_6587" n="sc" s="T1423">
               <ts e="T1424" id="Seg_6589" n="e" s="T1423">Dĭgəttə </ts>
               <ts e="T1425" id="Seg_6591" n="e" s="T1424">kambiʔi </ts>
               <ts e="T1426" id="Seg_6593" n="e" s="T1425">(dĭ=) </ts>
               <ts e="T1427" id="Seg_6595" n="e" s="T1426">dĭbər </ts>
               <ts e="T1428" id="Seg_6597" n="e" s="T1427">sʼevʼerdə, </ts>
               <ts e="T1429" id="Seg_6599" n="e" s="T1428">dĭn </ts>
               <ts e="T1430" id="Seg_6601" n="e" s="T1429">tože </ts>
               <ts e="T1431" id="Seg_6603" n="e" s="T1430">abiʔi </ts>
               <ts e="T1432" id="Seg_6605" n="e" s="T1431">(aktʼi=) </ts>
               <ts e="T1433" id="Seg_6607" n="e" s="T1432">aktʼi. </ts>
            </ts>
            <ts e="T1436" id="Seg_6608" n="sc" s="T1434">
               <ts e="T1435" id="Seg_6610" n="e" s="T1434">Bazaj </ts>
               <ts e="T1436" id="Seg_6612" n="e" s="T1435">aktʼi. </ts>
            </ts>
            <ts e="T1442" id="Seg_6613" n="sc" s="T1437">
               <ts e="T1438" id="Seg_6615" n="e" s="T1437">Dĭgəttə </ts>
               <ts e="T1439" id="Seg_6617" n="e" s="T1438">dĭ </ts>
               <ts e="T1440" id="Seg_6619" n="e" s="T1439">šobi </ts>
               <ts e="T1441" id="Seg_6621" n="e" s="T1440">maːʔndə, </ts>
               <ts e="T1442" id="Seg_6623" n="e" s="T1441">ianə. </ts>
            </ts>
            <ts e="T1448" id="Seg_6624" n="sc" s="T1443">
               <ts e="T1444" id="Seg_6626" n="e" s="T1443">(Dĭ=) </ts>
               <ts e="T1445" id="Seg_6628" n="e" s="T1444">Dĭgəttə </ts>
               <ts e="T1446" id="Seg_6630" n="e" s="T1445">dĭ </ts>
               <ts e="T1447" id="Seg_6632" n="e" s="T1446">dön </ts>
               <ts e="T1448" id="Seg_6634" n="e" s="T1447">ibi. </ts>
            </ts>
            <ts e="T1457" id="Seg_6635" n="sc" s="T1449">
               <ts e="T1450" id="Seg_6637" n="e" s="T1449">Dĭgəttə </ts>
               <ts e="T1451" id="Seg_6639" n="e" s="T1450">sazən </ts>
               <ts e="T1453" id="Seg_6641" n="e" s="T1451">pʼaŋdəbiʔi: </ts>
               <ts e="T1454" id="Seg_6643" n="e" s="T1453">"Tăn </ts>
               <ts e="T1455" id="Seg_6645" n="e" s="T1454">tibi </ts>
               <ts e="T1456" id="Seg_6647" n="e" s="T1455">bügən </ts>
               <ts e="T1457" id="Seg_6649" n="e" s="T1456">külambi". </ts>
            </ts>
            <ts e="T1461" id="Seg_6650" n="sc" s="T1458">
               <ts e="T1459" id="Seg_6652" n="e" s="T1458">Dĭgəttə </ts>
               <ts e="T1460" id="Seg_6654" n="e" s="T1459">dĭ </ts>
               <ts e="T1461" id="Seg_6656" n="e" s="T1460">kambi. </ts>
            </ts>
            <ts e="T1465" id="Seg_6657" n="sc" s="T1462">
               <ts e="T1463" id="Seg_6659" n="e" s="T1462">Dĭm </ts>
               <ts e="T1464" id="Seg_6661" n="e" s="T1463">dʼünə </ts>
               <ts e="T1465" id="Seg_6663" n="e" s="T1464">embiʔi. </ts>
            </ts>
            <ts e="T1471" id="Seg_6664" n="sc" s="T1466">
               <ts e="T1467" id="Seg_6666" n="e" s="T1466">Dĭgəttə </ts>
               <ts e="T1468" id="Seg_6668" n="e" s="T1467">bazo </ts>
               <ts e="T1469" id="Seg_6670" n="e" s="T1468">dĭʔnə </ts>
               <ts e="T1470" id="Seg_6672" n="e" s="T1469">kuza </ts>
               <ts e="T1471" id="Seg_6674" n="e" s="T1470">ibi. </ts>
            </ts>
            <ts e="T1475" id="Seg_6675" n="sc" s="T1472">
               <ts e="T1473" id="Seg_6677" n="e" s="T1472">Dĭn </ts>
               <ts e="T1474" id="Seg_6679" n="e" s="T1473">nagobi </ts>
               <ts e="T1475" id="Seg_6681" n="e" s="T1474">net. </ts>
            </ts>
            <ts e="T1484" id="Seg_6682" n="sc" s="T1476">
               <ts e="T1477" id="Seg_6684" n="e" s="T1476">I </ts>
               <ts e="T1478" id="Seg_6686" n="e" s="T1477">tüj </ts>
               <ts e="T1479" id="Seg_6688" n="e" s="T1478">amnolaʔbəʔjə, </ts>
               <ts e="T1480" id="Seg_6690" n="e" s="T1479">i </ts>
               <ts e="T1481" id="Seg_6692" n="e" s="T1480">dĭn </ts>
               <ts e="T1482" id="Seg_6694" n="e" s="T1481">sumna </ts>
               <ts e="T1483" id="Seg_6696" n="e" s="T1482">esseŋdə </ts>
               <ts e="T1484" id="Seg_6698" n="e" s="T1483">ibiʔi. </ts>
            </ts>
            <ts e="T1488" id="Seg_6699" n="sc" s="T1485">
               <ts e="T1486" id="Seg_6701" n="e" s="T1485">Dĭ </ts>
               <ts e="T1487" id="Seg_6703" n="e" s="T1486">ibi </ts>
               <ts e="T1488" id="Seg_6705" n="e" s="T1487">dĭm. </ts>
            </ts>
            <ts e="T1492" id="Seg_6706" n="sc" s="T1489">
               <ts e="T1490" id="Seg_6708" n="e" s="T1489">Ugandə </ts>
               <ts e="T1491" id="Seg_6710" n="e" s="T1490">jakše </ts>
               <ts e="T1492" id="Seg_6712" n="e" s="T1491">kuza. </ts>
            </ts>
            <ts e="T1508" id="Seg_6713" n="sc" s="T1493">
               <ts e="T1494" id="Seg_6715" n="e" s="T1493">Ara </ts>
               <ts e="T1495" id="Seg_6717" n="e" s="T1494">ej </ts>
               <ts e="T1496" id="Seg_6719" n="e" s="T1495">bĭtlie. </ts>
               <ts e="T1497" id="Seg_6721" n="e" s="T1496">Gibərdə </ts>
               <ts e="T1498" id="Seg_6723" n="e" s="T1497">ej </ts>
               <ts e="T1499" id="Seg_6725" n="e" s="T1498">mĭnlie, </ts>
               <ts e="T1500" id="Seg_6727" n="e" s="T1499">üge </ts>
               <ts e="T1501" id="Seg_6729" n="e" s="T1500">togonorlaʔbə. </ts>
               <ts e="T1502" id="Seg_6731" n="e" s="T1501">(Dĭn) </ts>
               <ts e="T1503" id="Seg_6733" n="e" s="T1502">tože </ts>
               <ts e="T1504" id="Seg_6735" n="e" s="T1503">iat </ts>
               <ts e="T1505" id="Seg_6737" n="e" s="T1504">naga, </ts>
               <ts e="T1506" id="Seg_6739" n="e" s="T1505">tolʼkă </ts>
               <ts e="T1507" id="Seg_6741" n="e" s="T1506">onʼiʔ </ts>
               <ts e="T1508" id="Seg_6743" n="e" s="T1507">abat. </ts>
            </ts>
            <ts e="T1521" id="Seg_6744" n="sc" s="T1516">
               <ts e="T1517" id="Seg_6746" n="e" s="T1516">Пускай, </ts>
               <ts e="T1519" id="Seg_6748" n="e" s="T1517">пускай </ts>
               <ts e="T1520" id="Seg_6750" n="e" s="T1519">она </ts>
               <ts e="T1521" id="Seg_6752" n="e" s="T1520">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T9" id="Seg_6753" s="T1">PKZ_197008_09342-2bz.PKZ.001 (001)</ta>
            <ta e="T30" id="Seg_6754" s="T17">PKZ_197008_09342-2bz.PKZ.002 (004)</ta>
            <ta e="T51" id="Seg_6755" s="T44">PKZ_197008_09342-2bz.PKZ.003 (009)</ta>
            <ta e="T59" id="Seg_6756" s="T52">PKZ_197008_09342-2bz.PKZ.004 (011)</ta>
            <ta e="T61" id="Seg_6757" s="T60">PKZ_197008_09342-2bz.PKZ.005 (013)</ta>
            <ta e="T75" id="Seg_6758" s="T62">PKZ_197008_09342-2bz.PKZ.006 (014)</ta>
            <ta e="T80" id="Seg_6759" s="T75">PKZ_197008_09342-2bz.PKZ.007 (015)</ta>
            <ta e="T84" id="Seg_6760" s="T81">PKZ_197008_09342-2bz.PKZ.008 (016)</ta>
            <ta e="T88" id="Seg_6761" s="T85">PKZ_197008_09342-2bz.PKZ.009 (017)</ta>
            <ta e="T94" id="Seg_6762" s="T89">PKZ_197008_09342-2bz.PKZ.010 (018)</ta>
            <ta e="T106" id="Seg_6763" s="T104">PKZ_197008_09342-2bz.PKZ.011 (021)</ta>
            <ta e="T118" id="Seg_6764" s="T108">PKZ_197008_09342-2bz.PKZ.012 (022)</ta>
            <ta e="T127" id="Seg_6765" s="T126">PKZ_197008_09342-2bz.PKZ.013 (026)</ta>
            <ta e="T137" id="Seg_6766" s="T128">PKZ_197008_09342-2bz.PKZ.014 (027)</ta>
            <ta e="T141" id="Seg_6767" s="T138">PKZ_197008_09342-2bz.PKZ.015 (028)</ta>
            <ta e="T147" id="Seg_6768" s="T142">PKZ_197008_09342-2bz.PKZ.016 (029)</ta>
            <ta e="T150" id="Seg_6769" s="T148">PKZ_197008_09342-2bz.PKZ.017 (030)</ta>
            <ta e="T153" id="Seg_6770" s="T151">PKZ_197008_09342-2bz.PKZ.018 (031)</ta>
            <ta e="T156" id="Seg_6771" s="T154">PKZ_197008_09342-2bz.PKZ.019 (032)</ta>
            <ta e="T159" id="Seg_6772" s="T157">PKZ_197008_09342-2bz.PKZ.020 (033)</ta>
            <ta e="T164" id="Seg_6773" s="T160">PKZ_197008_09342-2bz.PKZ.021 (034)</ta>
            <ta e="T166" id="Seg_6774" s="T165">PKZ_197008_09342-2bz.PKZ.022 (035)</ta>
            <ta e="T185" id="Seg_6775" s="T173">PKZ_197008_09342-2bz.PKZ.023 (037)</ta>
            <ta e="T193" id="Seg_6776" s="T186">PKZ_197008_09342-2bz.PKZ.024 (038)</ta>
            <ta e="T198" id="Seg_6777" s="T194">PKZ_197008_09342-2bz.PKZ.025 (039)</ta>
            <ta e="T210" id="Seg_6778" s="T199">PKZ_197008_09342-2bz.PKZ.026 (040)</ta>
            <ta e="T221" id="Seg_6779" s="T219">PKZ_197008_09342-2bz.PKZ.027 (043)</ta>
            <ta e="T224" id="Seg_6780" s="T221">PKZ_197008_09342-2bz.PKZ.028 (044)</ta>
            <ta e="T230" id="Seg_6781" s="T226">PKZ_197008_09342-2bz.PKZ.029 (046)</ta>
            <ta e="T237" id="Seg_6782" s="T233">PKZ_197008_09342-2bz.PKZ.030 (048)</ta>
            <ta e="T249" id="Seg_6783" s="T239">PKZ_197008_09342-2bz.PKZ.031 (050)</ta>
            <ta e="T253" id="Seg_6784" s="T250">PKZ_197008_09342-2bz.PKZ.032 (051)</ta>
            <ta e="T260" id="Seg_6785" s="T257">PKZ_197008_09342-2bz.PKZ.033 (054)</ta>
            <ta e="T270" id="Seg_6786" s="T261">PKZ_197008_09342-2bz.PKZ.034 (055)</ta>
            <ta e="T276" id="Seg_6787" s="T271">PKZ_197008_09342-2bz.PKZ.035 (058)</ta>
            <ta e="T280" id="Seg_6788" s="T276">PKZ_197008_09342-2bz.PKZ.036 (059)</ta>
            <ta e="T286" id="Seg_6789" s="T284">PKZ_197008_09342-2bz.PKZ.037 (061)</ta>
            <ta e="T301" id="Seg_6790" s="T286">PKZ_197008_09342-2bz.PKZ.038 (062)</ta>
            <ta e="T312" id="Seg_6791" s="T307">PKZ_197008_09342-2bz.PKZ.039 (065)</ta>
            <ta e="T344" id="Seg_6792" s="T313">PKZ_197008_09342-2bz.PKZ.040 (066)</ta>
            <ta e="T348" id="Seg_6793" s="T347">PKZ_197008_09342-2bz.PKZ.041 (068)</ta>
            <ta e="T353" id="Seg_6794" s="T349">PKZ_197008_09342-2bz.PKZ.042 (069)</ta>
            <ta e="T364" id="Seg_6795" s="T359">PKZ_197008_09342-2bz.PKZ.043 (073)</ta>
            <ta e="T410" id="Seg_6796" s="T389">PKZ_197008_09342-2bz.PKZ.044 (076)</ta>
            <ta e="T424" id="Seg_6797" s="T411">PKZ_197008_09342-2bz.PKZ.045 (078)</ta>
            <ta e="T438" id="Seg_6798" s="T428">PKZ_197008_09342-2bz.PKZ.046 (081)</ta>
            <ta e="T446" id="Seg_6799" s="T438">PKZ_197008_09342-2bz.PKZ.047 (082)</ta>
            <ta e="T455" id="Seg_6800" s="T447">PKZ_197008_09342-2bz.PKZ.048 (083)</ta>
            <ta e="T472" id="Seg_6801" s="T456">PKZ_197008_09342-2bz.PKZ.049 (084)</ta>
            <ta e="T479" id="Seg_6802" s="T473">PKZ_197008_09342-2bz.PKZ.050 (085)</ta>
            <ta e="T482" id="Seg_6803" s="T480">PKZ_197008_09342-2bz.PKZ.051 (086)</ta>
            <ta e="T494" id="Seg_6804" s="T483">PKZ_197008_09342-2bz.PKZ.052 (087)</ta>
            <ta e="T502" id="Seg_6805" s="T495">PKZ_197008_09342-2bz.PKZ.053 (088)</ta>
            <ta e="T514" id="Seg_6806" s="T503">PKZ_197008_09342-2bz.PKZ.054 (089)</ta>
            <ta e="T519" id="Seg_6807" s="T514">PKZ_197008_09342-2bz.PKZ.055 (090)</ta>
            <ta e="T522" id="Seg_6808" s="T520">PKZ_197008_09342-2bz.PKZ.056 (091)</ta>
            <ta e="T525" id="Seg_6809" s="T523">PKZ_197008_09342-2bz.PKZ.057 (092)</ta>
            <ta e="T529" id="Seg_6810" s="T526">PKZ_197008_09342-2bz.PKZ.058 (093)</ta>
            <ta e="T532" id="Seg_6811" s="T530">PKZ_197008_09342-2bz.PKZ.059 (094)</ta>
            <ta e="T537" id="Seg_6812" s="T533">PKZ_197008_09342-2bz.PKZ.060 (095)</ta>
            <ta e="T540" id="Seg_6813" s="T538">PKZ_197008_09342-2bz.PKZ.061 (096)</ta>
            <ta e="T548" id="Seg_6814" s="T541">PKZ_197008_09342-2bz.PKZ.062 (097)</ta>
            <ta e="T560" id="Seg_6815" s="T555">PKZ_197008_09342-2bz.PKZ.063 (099)</ta>
            <ta e="T566" id="Seg_6816" s="T561">PKZ_197008_09342-2bz.PKZ.064 (100)</ta>
            <ta e="T581" id="Seg_6817" s="T568">PKZ_197008_09342-2bz.PKZ.065 (102) </ta>
            <ta e="T586" id="Seg_6818" s="T581">PKZ_197008_09342-2bz.PKZ.066 (104)</ta>
            <ta e="T589" id="Seg_6819" s="T587">PKZ_197008_09342-2bz.PKZ.067 (105)</ta>
            <ta e="T596" id="Seg_6820" s="T590">PKZ_197008_09342-2bz.PKZ.068 (106)</ta>
            <ta e="T603" id="Seg_6821" s="T597">PKZ_197008_09342-2bz.PKZ.069 (107)</ta>
            <ta e="T608" id="Seg_6822" s="T604">PKZ_197008_09342-2bz.PKZ.070 (108)</ta>
            <ta e="T622" id="Seg_6823" s="T609">PKZ_197008_09342-2bz.PKZ.071 (109)</ta>
            <ta e="T631" id="Seg_6824" s="T623">PKZ_197008_09342-2bz.PKZ.072 (110)</ta>
            <ta e="T643" id="Seg_6825" s="T632">PKZ_197008_09342-2bz.PKZ.073 (111)</ta>
            <ta e="T655" id="Seg_6826" s="T644">PKZ_197008_09342-2bz.PKZ.074 (112)</ta>
            <ta e="T664" id="Seg_6827" s="T655">PKZ_197008_09342-2bz.PKZ.075 (113)</ta>
            <ta e="T670" id="Seg_6828" s="T665">PKZ_197008_09342-2bz.PKZ.076 (114)</ta>
            <ta e="T674" id="Seg_6829" s="T671">PKZ_197008_09342-2bz.PKZ.077 (115)</ta>
            <ta e="T681" id="Seg_6830" s="T675">PKZ_197008_09342-2bz.PKZ.078 (116)</ta>
            <ta e="T686" id="Seg_6831" s="T682">PKZ_197008_09342-2bz.PKZ.079 (117)</ta>
            <ta e="T691" id="Seg_6832" s="T687">PKZ_197008_09342-2bz.PKZ.080 (118)</ta>
            <ta e="T701" id="Seg_6833" s="T692">PKZ_197008_09342-2bz.PKZ.081 (119)</ta>
            <ta e="T706" id="Seg_6834" s="T702">PKZ_197008_09342-2bz.PKZ.082 (120)</ta>
            <ta e="T712" id="Seg_6835" s="T707">PKZ_197008_09342-2bz.PKZ.083 (121)</ta>
            <ta e="T716" id="Seg_6836" s="T713">PKZ_197008_09342-2bz.PKZ.084 (122)</ta>
            <ta e="T724" id="Seg_6837" s="T722">PKZ_197008_09342-2bz.PKZ.085 (125)</ta>
            <ta e="T738" id="Seg_6838" s="T728">PKZ_197008_09342-2bz.PKZ.086 (127)</ta>
            <ta e="T750" id="Seg_6839" s="T739">PKZ_197008_09342-2bz.PKZ.087 (128)</ta>
            <ta e="T759" id="Seg_6840" s="T751">PKZ_197008_09342-2bz.PKZ.088 (129)</ta>
            <ta e="T765" id="Seg_6841" s="T760">PKZ_197008_09342-2bz.PKZ.089 (130)</ta>
            <ta e="T772" id="Seg_6842" s="T766">PKZ_197008_09342-2bz.PKZ.090 (131)</ta>
            <ta e="T776" id="Seg_6843" s="T773">PKZ_197008_09342-2bz.PKZ.091 (132)</ta>
            <ta e="T778" id="Seg_6844" s="T777">PKZ_197008_09342-2bz.PKZ.092 (133)</ta>
            <ta e="T782" id="Seg_6845" s="T779">PKZ_197008_09342-2bz.PKZ.093 (134)</ta>
            <ta e="T790" id="Seg_6846" s="T783">PKZ_197008_09342-2bz.PKZ.094 (135)</ta>
            <ta e="T800" id="Seg_6847" s="T791">PKZ_197008_09342-2bz.PKZ.095 (136)</ta>
            <ta e="T803" id="Seg_6848" s="T801">PKZ_197008_09342-2bz.PKZ.096 (137)</ta>
            <ta e="T823" id="Seg_6849" s="T819">PKZ_197008_09342-2bz.PKZ.097 (140)</ta>
            <ta e="T834" id="Seg_6850" s="T828">PKZ_197008_09342-2bz.PKZ.098 (142)</ta>
            <ta e="T845" id="Seg_6851" s="T841">PKZ_197008_09342-2bz.PKZ.099 (144)</ta>
            <ta e="T849" id="Seg_6852" s="T847">PKZ_197008_09342-2bz.PKZ.100 (146)</ta>
            <ta e="T859" id="Seg_6853" s="T850">PKZ_197008_09342-2bz.PKZ.101 (147)</ta>
            <ta e="T866" id="Seg_6854" s="T860">PKZ_197008_09342-2bz.PKZ.102 (149)</ta>
            <ta e="T873" id="Seg_6855" s="T867">PKZ_197008_09342-2bz.PKZ.103 (150)</ta>
            <ta e="T878" id="Seg_6856" s="T874">PKZ_197008_09342-2bz.PKZ.104 (151)</ta>
            <ta e="T886" id="Seg_6857" s="T879">PKZ_197008_09342-2bz.PKZ.105 (152)</ta>
            <ta e="T895" id="Seg_6858" s="T887">PKZ_197008_09342-2bz.PKZ.106 (153)</ta>
            <ta e="T902" id="Seg_6859" s="T896">PKZ_197008_09342-2bz.PKZ.107 (154)</ta>
            <ta e="T907" id="Seg_6860" s="T903">PKZ_197008_09342-2bz.PKZ.108 (155)</ta>
            <ta e="T913" id="Seg_6861" s="T908">PKZ_197008_09342-2bz.PKZ.109 (156)</ta>
            <ta e="T915" id="Seg_6862" s="T914">PKZ_197008_09342-2bz.PKZ.110 (157)</ta>
            <ta e="T920" id="Seg_6863" s="T916">PKZ_197008_09342-2bz.PKZ.111 (158)</ta>
            <ta e="T933" id="Seg_6864" s="T921">PKZ_197008_09342-2bz.PKZ.112 (159) </ta>
            <ta e="T950" id="Seg_6865" s="T934">PKZ_197008_09342-2bz.PKZ.113 (161) </ta>
            <ta e="T959" id="Seg_6866" s="T951">PKZ_197008_09342-2bz.PKZ.114 (163)</ta>
            <ta e="T968" id="Seg_6867" s="T960">PKZ_197008_09342-2bz.PKZ.115 (164)</ta>
            <ta e="T980" id="Seg_6868" s="T969">PKZ_197008_09342-2bz.PKZ.116 (165)</ta>
            <ta e="T984" id="Seg_6869" s="T981">PKZ_197008_09342-2bz.PKZ.117 (166)</ta>
            <ta e="T988" id="Seg_6870" s="T985">PKZ_197008_09342-2bz.PKZ.118 (167)</ta>
            <ta e="T992" id="Seg_6871" s="T988">PKZ_197008_09342-2bz.PKZ.119 (168)</ta>
            <ta e="T997" id="Seg_6872" s="T993">PKZ_197008_09342-2bz.PKZ.120 (169)</ta>
            <ta e="T1000" id="Seg_6873" s="T998">PKZ_197008_09342-2bz.PKZ.121 (170)</ta>
            <ta e="T1006" id="Seg_6874" s="T1001">PKZ_197008_09342-2bz.PKZ.122 (171)</ta>
            <ta e="T1012" id="Seg_6875" s="T1007">PKZ_197008_09342-2bz.PKZ.123 (172)</ta>
            <ta e="T1017" id="Seg_6876" s="T1013">PKZ_197008_09342-2bz.PKZ.124 (173)</ta>
            <ta e="T1028" id="Seg_6877" s="T1018">PKZ_197008_09342-2bz.PKZ.125 (174)</ta>
            <ta e="T1049" id="Seg_6878" s="T1029">PKZ_197008_09342-2bz.PKZ.126 (175) </ta>
            <ta e="T1057" id="Seg_6879" s="T1050">PKZ_197008_09342-2bz.PKZ.127 (177) </ta>
            <ta e="T1064" id="Seg_6880" s="T1058">PKZ_197008_09342-2bz.PKZ.128 (179)</ta>
            <ta e="T1069" id="Seg_6881" s="T1065">PKZ_197008_09342-2bz.PKZ.129 (180)</ta>
            <ta e="T1076" id="Seg_6882" s="T1070">PKZ_197008_09342-2bz.PKZ.130 (181)</ta>
            <ta e="T1085" id="Seg_6883" s="T1077">PKZ_197008_09342-2bz.PKZ.131 (182)</ta>
            <ta e="T1092" id="Seg_6884" s="T1086">PKZ_197008_09342-2bz.PKZ.132 (183)</ta>
            <ta e="T1097" id="Seg_6885" s="T1093">PKZ_197008_09342-2bz.PKZ.133 (184)</ta>
            <ta e="T1100" id="Seg_6886" s="T1098">PKZ_197008_09342-2bz.PKZ.134 (185)</ta>
            <ta e="T1107" id="Seg_6887" s="T1101">PKZ_197008_09342-2bz.PKZ.135 (186)</ta>
            <ta e="T1113" id="Seg_6888" s="T1108">PKZ_197008_09342-2bz.PKZ.136 (187)</ta>
            <ta e="T1117" id="Seg_6889" s="T1114">PKZ_197008_09342-2bz.PKZ.137 (188)</ta>
            <ta e="T1124" id="Seg_6890" s="T1118">PKZ_197008_09342-2bz.PKZ.138 (189)</ta>
            <ta e="T1139" id="Seg_6891" s="T1125">PKZ_197008_09342-2bz.PKZ.139 (190)</ta>
            <ta e="T1148" id="Seg_6892" s="T1140">PKZ_197008_09342-2bz.PKZ.140 (191)</ta>
            <ta e="T1151" id="Seg_6893" s="T1149">PKZ_197008_09342-2bz.PKZ.141 (192)</ta>
            <ta e="T1180" id="Seg_6894" s="T1165">PKZ_197008_09342-2bz.PKZ.142 (194) </ta>
            <ta e="T1195" id="Seg_6895" s="T1181">PKZ_197008_09342-2bz.PKZ.143 (196)</ta>
            <ta e="T1204" id="Seg_6896" s="T1196">PKZ_197008_09342-2bz.PKZ.144 (197)</ta>
            <ta e="T1214" id="Seg_6897" s="T1205">PKZ_197008_09342-2bz.PKZ.145 (198)</ta>
            <ta e="T1229" id="Seg_6898" s="T1215">PKZ_197008_09342-2bz.PKZ.146 (199)</ta>
            <ta e="T1233" id="Seg_6899" s="T1230">PKZ_197008_09342-2bz.PKZ.147 (200)</ta>
            <ta e="T1237" id="Seg_6900" s="T1234">PKZ_197008_09342-2bz.PKZ.148 (201)</ta>
            <ta e="T1242" id="Seg_6901" s="T1238">PKZ_197008_09342-2bz.PKZ.149 (202)</ta>
            <ta e="T1247" id="Seg_6902" s="T1243">PKZ_197008_09342-2bz.PKZ.150 (203)</ta>
            <ta e="T1254" id="Seg_6903" s="T1248">PKZ_197008_09342-2bz.PKZ.151 (204)</ta>
            <ta e="T1262" id="Seg_6904" s="T1255">PKZ_197008_09342-2bz.PKZ.152 (205)</ta>
            <ta e="T1269" id="Seg_6905" s="T1263">PKZ_197008_09342-2bz.PKZ.153 (206)</ta>
            <ta e="T1278" id="Seg_6906" s="T1270">PKZ_197008_09342-2bz.PKZ.154 (207)</ta>
            <ta e="T1289" id="Seg_6907" s="T1279">PKZ_197008_09342-2bz.PKZ.155 (208) </ta>
            <ta e="T1295" id="Seg_6908" s="T1290">PKZ_197008_09342-2bz.PKZ.156 (210)</ta>
            <ta e="T1306" id="Seg_6909" s="T1296">PKZ_197008_09342-2bz.PKZ.157 (211)</ta>
            <ta e="T1313" id="Seg_6910" s="T1307">PKZ_197008_09342-2bz.PKZ.158 (212)</ta>
            <ta e="T1318" id="Seg_6911" s="T1314">PKZ_197008_09342-2bz.PKZ.159 (213)</ta>
            <ta e="T1326" id="Seg_6912" s="T1318">PKZ_197008_09342-2bz.PKZ.160 (214)</ta>
            <ta e="T1331" id="Seg_6913" s="T1327">PKZ_197008_09342-2bz.PKZ.161 (215)</ta>
            <ta e="T1335" id="Seg_6914" s="T1332">PKZ_197008_09342-2bz.PKZ.162 (216)</ta>
            <ta e="T1341" id="Seg_6915" s="T1336">PKZ_197008_09342-2bz.PKZ.163 (217)</ta>
            <ta e="T1347" id="Seg_6916" s="T1342">PKZ_197008_09342-2bz.PKZ.164 (218)</ta>
            <ta e="T1364" id="Seg_6917" s="T1348">PKZ_197008_09342-2bz.PKZ.165 (219)</ta>
            <ta e="T1370" id="Seg_6918" s="T1365">PKZ_197008_09342-2bz.PKZ.166 (220)</ta>
            <ta e="T1379" id="Seg_6919" s="T1371">PKZ_197008_09342-2bz.PKZ.167 (221)</ta>
            <ta e="T1381" id="Seg_6920" s="T1380">PKZ_197008_09342-2bz.PKZ.168 (222)</ta>
            <ta e="T1383" id="Seg_6921" s="T1382">PKZ_197008_09342-2bz.PKZ.169 (223)</ta>
            <ta e="T1390" id="Seg_6922" s="T1383">PKZ_197008_09342-2bz.PKZ.170 (224)</ta>
            <ta e="T1396" id="Seg_6923" s="T1391">PKZ_197008_09342-2bz.PKZ.171 (225)</ta>
            <ta e="T1402" id="Seg_6924" s="T1397">PKZ_197008_09342-2bz.PKZ.172 (226)</ta>
            <ta e="T1410" id="Seg_6925" s="T1403">PKZ_197008_09342-2bz.PKZ.173 (227)</ta>
            <ta e="T1414" id="Seg_6926" s="T1411">PKZ_197008_09342-2bz.PKZ.174 (228)</ta>
            <ta e="T1422" id="Seg_6927" s="T1415">PKZ_197008_09342-2bz.PKZ.175 (229)</ta>
            <ta e="T1433" id="Seg_6928" s="T1423">PKZ_197008_09342-2bz.PKZ.176 (230)</ta>
            <ta e="T1436" id="Seg_6929" s="T1434">PKZ_197008_09342-2bz.PKZ.177 (231)</ta>
            <ta e="T1442" id="Seg_6930" s="T1437">PKZ_197008_09342-2bz.PKZ.178 (232)</ta>
            <ta e="T1448" id="Seg_6931" s="T1443">PKZ_197008_09342-2bz.PKZ.179 (233)</ta>
            <ta e="T1457" id="Seg_6932" s="T1449">PKZ_197008_09342-2bz.PKZ.180 (234) </ta>
            <ta e="T1461" id="Seg_6933" s="T1458">PKZ_197008_09342-2bz.PKZ.181 (236)</ta>
            <ta e="T1465" id="Seg_6934" s="T1462">PKZ_197008_09342-2bz.PKZ.182 (237)</ta>
            <ta e="T1471" id="Seg_6935" s="T1466">PKZ_197008_09342-2bz.PKZ.183 (238)</ta>
            <ta e="T1475" id="Seg_6936" s="T1472">PKZ_197008_09342-2bz.PKZ.184 (239)</ta>
            <ta e="T1484" id="Seg_6937" s="T1476">PKZ_197008_09342-2bz.PKZ.185 (240)</ta>
            <ta e="T1488" id="Seg_6938" s="T1485">PKZ_197008_09342-2bz.PKZ.186 (241)</ta>
            <ta e="T1492" id="Seg_6939" s="T1489">PKZ_197008_09342-2bz.PKZ.187 (242)</ta>
            <ta e="T1496" id="Seg_6940" s="T1493">PKZ_197008_09342-2bz.PKZ.188 (243)</ta>
            <ta e="T1501" id="Seg_6941" s="T1496">PKZ_197008_09342-2bz.PKZ.189 (244)</ta>
            <ta e="T1508" id="Seg_6942" s="T1501">PKZ_197008_09342-2bz.PKZ.190 (245)</ta>
            <ta e="T1521" id="Seg_6943" s="T1516">PKZ_197008_09342-2bz.PKZ.191 (248)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T9" id="Seg_6944" s="T1">А я уж и забыла, чего говорила ((…)). </ta>
            <ta e="T30" id="Seg_6945" s="T17">Вот я выпустила, сейчас переговорила и забыла. </ta>
            <ta e="T51" id="Seg_6946" s="T44">А, вот чего я говорила. </ta>
            <ta e="T59" id="Seg_6947" s="T52">Когда я собралась сюды ехать… </ta>
            <ta e="T61" id="Seg_6948" s="T60">Пусти. </ta>
            <ta e="T75" id="Seg_6949" s="T62">Когда я собралась сюды ехать, там люди говорили: "Не езди, ты помрешь (ведь)". </ta>
            <ta e="T80" id="Seg_6950" s="T75">А я сказала: "Все равно. </ta>
            <ta e="T84" id="Seg_6951" s="T81">И там похоронят". </ta>
            <ta e="T88" id="Seg_6952" s="T85">Все равно земля. </ta>
            <ta e="T94" id="Seg_6953" s="T89">Вот это вот я говорила. </ta>
            <ta e="T106" id="Seg_6954" s="T104">Ну, еще это… </ta>
            <ta e="T118" id="Seg_6955" s="T108">Про это говорить, что я набрала гостинцев еще туды своим. </ta>
            <ta e="T127" id="Seg_6956" s="T126">Пускайте. </ta>
            <ta e="T137" id="Seg_6957" s="T128">Kamen (măn) amnobiam Tartu, kambiam, ibiem (na-) nagur platokəʔjə. </ta>
            <ta e="T141" id="Seg_6958" s="T138">Bostə ildə mĭzittə. </ta>
            <ta e="T147" id="Seg_6959" s="T142">Dĭgəttə döbər šobiam, ibiem piʔmeʔi. </ta>
            <ta e="T150" id="Seg_6960" s="T148">Kujnek ibiem. </ta>
            <ta e="T153" id="Seg_6961" s="T151">Nʼinə, plʼemʼannikamnə. </ta>
            <ta e="T156" id="Seg_6962" s="T154">Koʔbdonə ibiem… </ta>
            <ta e="T159" id="Seg_6963" s="T157">Platʼtʼa ibiem. </ta>
            <ta e="T164" id="Seg_6964" s="T160">Dĭgəttə plʼemʼanniktə ibiem kujnek. </ta>
            <ta e="T166" id="Seg_6965" s="T165">Kabarləj. </ta>
            <ta e="T185" id="Seg_6966" s="T173">Когда я сюды приехала в Тарту, пошла в магазин взяла три платка. </ta>
            <ta e="T193" id="Seg_6967" s="T186">Сюды приехала вот, взяла штаны, рубашку внуку. </ta>
            <ta e="T198" id="Seg_6968" s="T194">Потом взяла рубашку племяннику. </ta>
            <ta e="T210" id="Seg_6969" s="T199">((BRK)) Забыла еще вот один платок взяла в Красноярский. </ta>
            <ta e="T221" id="Seg_6970" s="T219">Нет, это… </ta>
            <ta e="T224" id="Seg_6971" s="T221">Это… </ta>
            <ta e="T230" id="Seg_6972" s="T226">Штаны, piʔmeʔi. </ta>
            <ta e="T237" id="Seg_6973" s="T233">Piʔme — штаны. </ta>
            <ta e="T249" id="Seg_6974" s="T239">А, (а) эти piʔme. </ta>
            <ta e="T253" id="Seg_6975" s="T250">Jamaʔi — piʔme, jaʔma. </ta>
            <ta e="T260" id="Seg_6976" s="T257">Вот. </ta>
            <ta e="T270" id="Seg_6977" s="T261">(Это=) Это обутки - jaʔma. </ta>
            <ta e="T276" id="Seg_6978" s="T271">А piʔmeʔi — это штаны. </ta>
            <ta e="T280" id="Seg_6979" s="T276">(K-) Kujnek — рубаха. </ta>
            <ta e="T286" id="Seg_6980" s="T284">Ну. </ta>
            <ta e="T301" id="Seg_6981" s="T286">А вот платье не знаю, вот (этой=) Наде взяла платье. </ta>
            <ta e="T312" id="Seg_6982" s="T307">Не знаю. </ta>
            <ta e="T344" id="Seg_6983" s="T313">Тады не носили, они все в штанах ходили так, как вот теперь все ходят в штанах. </ta>
            <ta e="T348" id="Seg_6984" s="T347">Kujnek. </ta>
            <ta e="T353" id="Seg_6985" s="T349">А (с-) шапка — üžü. </ta>
            <ta e="T364" id="Seg_6986" s="T359">Не знаю, что сказать. </ta>
            <ta e="T410" id="Seg_6987" s="T389">Можно это рассказать, (как=) ((LAUGH)) как Александр Константинович шел ко мне, я часто смеюсь над этим, даже сегодня смеялась? </ta>
            <ta e="T424" id="Seg_6988" s="T411">Он идет: "Бабушка, я иду к тебе разговаривать", а я его по-своему ругаю. </ta>
            <ta e="T438" id="Seg_6989" s="T428">Слухай, слухай, сперва расскажу, как было дело. </ta>
            <ta e="T446" id="Seg_6990" s="T438">А потом опять расскажу, ты может лучше запомнишь. </ta>
            <ta e="T455" id="Seg_6991" s="T447">Вот он идет: "Бабушка, я к тебе иду!" </ta>
            <ta e="T472" id="Seg_6992" s="T456">(Я г-) А я ему говорю: "Ты мне надоел, убирайся отседова, не ходи", — на его говорю. </ta>
            <ta e="T479" id="Seg_6993" s="T473">"Боле не буду с тобой разговаривать. </ta>
            <ta e="T482" id="Seg_6994" s="T480">Уходи отседова. </ta>
            <ta e="T494" id="Seg_6995" s="T483">Я тебе ничего не скажу", — вот это я ему так говорила. </ta>
            <ta e="T502" id="Seg_6996" s="T495">А он идет да говорит: "Ой ((DMG)). </ta>
            <ta e="T514" id="Seg_6997" s="T503">Kamen dĭ šobi măna, Александр Константинович, măndə: "Urgaja, măn tănan šonəgam!" </ta>
            <ta e="T519" id="Seg_6998" s="T514">A măn măndəm: "Iʔ šoʔ. </ta>
            <ta e="T522" id="Seg_6999" s="T520">(Tuʔ) măna. </ta>
            <ta e="T525" id="Seg_7000" s="T523">Kanaʔ döʔə! </ta>
            <ta e="T529" id="Seg_7001" s="T526">Tăn măna (надое-). </ta>
            <ta e="T532" id="Seg_7002" s="T530">Tăn măna… </ta>
            <ta e="T537" id="Seg_7003" s="T533">Măn tănzi ej dʼăbaktərlam. </ta>
            <ta e="T540" id="Seg_7004" s="T538">Kanaʔ döʔə". </ta>
            <ta e="T548" id="Seg_7005" s="T541">По-русски хорошо рассказывала, опять забыла. </ta>
            <ta e="T560" id="Seg_7006" s="T555">Kamen dĭ măna (šobi=) šobi… </ta>
            <ta e="T566" id="Seg_7007" s="T561">Ой, опять по-своему начала. </ta>
            <ta e="T581" id="Seg_7008" s="T568">Kamen dĭ măna šobi, măn kudolbiam dĭm: "Iʔ šoʔ măna! </ta>
            <ta e="T586" id="Seg_7009" s="T581">Măn tănan ĭmbidə ej (nörbələm). </ta>
            <ta e="T589" id="Seg_7010" s="T587">Kanaʔ döʔə!" </ta>
            <ta e="T596" id="Seg_7011" s="T590">Dĭgəttə dĭ măndə: "Ugandə jakše dʼăbaktərial". </ta>
            <ta e="T603" id="Seg_7012" s="T597">Măn măndəm: "Прямо jakše, kudolliam tănan. </ta>
            <ta e="T608" id="Seg_7013" s="T604">A tăn mănlial: jakše". </ta>
            <ta e="T622" id="Seg_7014" s="T609">Когды шел ко мне (Констан-) Александр Константинов, говорит: "Бабушка, я к тебе иду!" </ta>
            <ta e="T631" id="Seg_7015" s="T623">А я говорю: "Не ходи, зачем ты идешь? </ta>
            <ta e="T643" id="Seg_7016" s="T632">Ты мне надоел, уходи отседова, я с тобой не буду разговаривать". </ta>
            <ta e="T655" id="Seg_7017" s="T644">А он говорит: "Ой, бабушка, как ты хорошо (р-) мне рассказываешь". </ta>
            <ta e="T664" id="Seg_7018" s="T655">А я ему говорю: "Прямо хорошо, я тебя ругаю". </ta>
            <ta e="T670" id="Seg_7019" s="T665">А он говорит: "Ну ладно". </ta>
            <ta e="T674" id="Seg_7020" s="T671">Ему (глянулося) приехать. </ta>
            <ta e="T681" id="Seg_7021" s="T675">"Ну, я хотел с сыном приехать". </ta>
            <ta e="T686" id="Seg_7022" s="T682">Его сын Константином звать. </ta>
            <ta e="T691" id="Seg_7023" s="T687">"И теперь напрок приеду". </ta>
            <ta e="T701" id="Seg_7024" s="T692">Ну, а я говорю: "Приедет, напрок может я помру". </ta>
            <ta e="T706" id="Seg_7025" s="T702">Стара уже, дожидаешь смерти. </ta>
            <ta e="T712" id="Seg_7026" s="T707">Ну, можно и это поговорить. </ta>
            <ta e="T716" id="Seg_7027" s="T713">Ну чего? </ta>
            <ta e="T724" id="Seg_7028" s="T722">Ой, господи. </ta>
            <ta e="T738" id="Seg_7029" s="T728">А-а-а, (Алек-) Александр Константинович обещал приехать. </ta>
            <ta e="T750" id="Seg_7030" s="T739">Написал письмо, а потом (я) ждала-ждала, он опять пишет: "Не приеду. </ta>
            <ta e="T759" id="Seg_7031" s="T751">Ездил я в Москву, и на север ездил. </ta>
            <ta e="T765" id="Seg_7032" s="T760">Так что не могу приехать". </ta>
            <ta e="T772" id="Seg_7033" s="T766">Ну, я и не стала дожидать. </ta>
            <ta e="T776" id="Seg_7034" s="T773">А потом приехал… </ta>
            <ta e="T778" id="Seg_7035" s="T777">Господи. </ta>
            <ta e="T782" id="Seg_7036" s="T779">И имя-то забываю. </ta>
            <ta e="T790" id="Seg_7037" s="T783">Арпит, я с им собралась да уехала. </ta>
            <ta e="T800" id="Seg_7038" s="T791">"Так, думаю, раз не хочет приезжать дак и уеду". </ta>
            <ta e="T803" id="Seg_7039" s="T801">Приехала сюды. </ta>
            <ta e="T823" id="Seg_7040" s="T819">Как не говорили? </ta>
            <ta e="T834" id="Seg_7041" s="T828">Я по-русски записала, потом по-камасински. </ta>
            <ta e="T845" id="Seg_7042" s="T841">Мне думается, рассказали. </ta>
            <ta e="T849" id="Seg_7043" s="T847">Два раз. </ta>
            <ta e="T859" id="Seg_7044" s="T850">Ну, пускай расскажу опять про это же. </ta>
            <ta e="T866" id="Seg_7045" s="T860">(Kamen=) Kamen šobi Александр Константинович măna… </ta>
            <ta e="T873" id="Seg_7046" s="T867">Da dĭ pʼaŋdəbi măna: "Šolam tănan". </ta>
            <ta e="T878" id="Seg_7047" s="T874">Măn (edəʔpiem), edəʔpiem — naga. </ta>
            <ta e="T886" id="Seg_7048" s="T879">Dĭgəttə pʼaŋdlaʔbə măna sazən: "Măn ej šolam. </ta>
            <ta e="T895" id="Seg_7049" s="T887">Dö ((…)) dĭ kögən (šo-) šolam (šiʔ-) tănan". </ta>
            <ta e="T902" id="Seg_7050" s="T896">A măn mămbiam: "Măn možet külalləm. </ta>
            <ta e="T907" id="Seg_7051" s="T903">A tăn dĭgəttə šolal". </ta>
            <ta e="T913" id="Seg_7052" s="T908">Dĭgəttə oʔbdəbiam da döbər šobiam. </ta>
            <ta e="T915" id="Seg_7053" s="T914">((BRK)). </ta>
            <ta e="T920" id="Seg_7054" s="T916">Măn koʔbdom (tibinə) kambi. </ta>
            <ta e="T933" id="Seg_7055" s="T921">Ibiem Krasnăjarskagən, dĭ măndə: "Măna onʼiʔ nʼi izittə, abanə iʔ nörbit". </ta>
            <ta e="T950" id="Seg_7056" s="T934">A măn mămbiam: "Kak ej nörbəsʼtə, abat bostə ibi boskəndə (i d-) i šiʔ deʔpi". </ta>
            <ta e="T959" id="Seg_7057" s="T951">Dĭgəttə šobiam abanə, nörbəbiem, dĭ pʼaŋdəbi dĭʔnə sazən. </ta>
            <ta e="T968" id="Seg_7058" s="T960">Dĭgəttə dĭ dĭʔnə sazən pʼaŋdəbi, dĭ döbər šobi. </ta>
            <ta e="T980" id="Seg_7059" s="T969">Dĭgəttə (uja=) šoška dʼagarbibaʔ, uja, iʔgö ipek pürbibeʔ, i il oʔbdəbibaʔ. </ta>
            <ta e="T984" id="Seg_7060" s="T981">Dĭgəttə ara ibi. </ta>
            <ta e="T988" id="Seg_7061" s="T985">Pʼelʼmʼenʼəʔi abibaʔ, kătletaʔi. </ta>
            <ta e="T992" id="Seg_7062" s="T988">Iʔgö (ibi=) ĭmbi ibi. </ta>
            <ta e="T997" id="Seg_7063" s="T993">I il iʔgö ibiʔi. </ta>
            <ta e="T1000" id="Seg_7064" s="T998">Kazan turagən šobiʔi. </ta>
            <ta e="T1006" id="Seg_7065" s="T1001">Dĭgəttə măn dĭʔnə ibiem palʼto. </ta>
            <ta e="T1012" id="Seg_7066" s="T1007">Dĭgəttə aktʼa dĭʔnə (ak-) mĭbiem. </ta>
            <ta e="T1017" id="Seg_7067" s="T1013">Dĭ kalla dʼürbi dĭ nʼizi. </ta>
            <ta e="T1028" id="Seg_7068" s="T1018">Dĭbər, (gijen = il=) gijen il (dʼăbaktərzi-) dʼăbaktəriaʔi bostə šĭketsi. </ta>
            <ta e="T1049" id="Seg_7069" s="T1029">Dĭgəttə (dĭ=) dĭ nʼin ia (dĭ- dĭm=) dĭm ej ajirbi, dĭgəttə dĭ döbər pʼaŋdlaʔbə: "(Kažen-) Kažnej (dʼan-) dʼala dʼorlaʔbəm. </ta>
            <ta e="T1057" id="Seg_7070" s="T1050">Miʔ стали dĭʔnə pʼaŋdəsʼtə: "Šoʔ maʔnəl". </ta>
            <ta e="T1064" id="Seg_7071" s="T1058">Dĭgəttə dĭ măndə: "Măn unnʼa šolam". </ta>
            <ta e="T1069" id="Seg_7072" s="T1065">"A ĭmbi unnʼa šolal?" </ta>
            <ta e="T1076" id="Seg_7073" s="T1070">A dĭ bazo pʼaŋdlaʔbə: "Aktʼa naga". </ta>
            <ta e="T1085" id="Seg_7074" s="T1077">Măn bazo öʔlubiem (šide bʼeʔ=) šide biʔ aktʼa. </ta>
            <ta e="T1092" id="Seg_7075" s="T1086">Dĭgəttə abat sumna (šide biʔ) öʔlubi. </ta>
            <ta e="T1097" id="Seg_7076" s="T1093">Dĭgəttə dö šobi onʼiʔ. </ta>
            <ta e="T1100" id="Seg_7077" s="T1098">Dĭ maːluʔpi. </ta>
            <ta e="T1107" id="Seg_7078" s="T1101">Dĭgəttə (dĭ=) dĭʔnə dĭ pʼaŋdəbi, pʼaŋdəbi. </ta>
            <ta e="T1113" id="Seg_7079" s="T1108">Dĭ (ej=) üge ej šolia. </ta>
            <ta e="T1117" id="Seg_7080" s="T1114">Dĭgəttə dĭ kalla dʼürbi. </ta>
            <ta e="T1124" id="Seg_7081" s="T1118">(Gil-) Gijen Lenin amnobi, dĭ turanə. </ta>
            <ta e="T1139" id="Seg_7082" s="T1125">Dĭn stălovăjăn (tob- tomn-) stălovăjăn ((…)) ipek mĭlie, ara mĭlie, bar ĭmbi mĭlie, pʼaŋdlaʔbə. </ta>
            <ta e="T1148" id="Seg_7083" s="T1140">(Bʼeʔ sumn-) Bʼeʔ nagur i sumna aktʼa iliet. </ta>
            <ta e="T1151" id="Seg_7084" s="T1149">По-русски можно. </ta>
            <ta e="T1180" id="Seg_7085" s="T1165">Моя племянница, была я в (Красноярскем), она говорит: "Пишет мне парень, чтобы я поехала. </ta>
            <ta e="T1195" id="Seg_7086" s="T1181">Да, говорит, отцу не сказывай", а я (ему сказала=) ей сказала:" Почему не сказывай? </ta>
            <ta e="T1204" id="Seg_7087" s="T1196">Отец сам женился, и вас напустил на свет". </ta>
            <ta e="T1214" id="Seg_7088" s="T1205">Я приехала, отцу сказала, он стал ей писать, разрешил. </ta>
            <ta e="T1229" id="Seg_7089" s="T1215">(И он собра-) Она ему тоже написала, он собрался, приехал, приехали сюды к нам. </ta>
            <ta e="T1233" id="Seg_7090" s="T1230">Мы закололи свинью. </ta>
            <ta e="T1237" id="Seg_7091" s="T1234">И всего наготовили. </ta>
            <ta e="T1242" id="Seg_7092" s="T1238">Пельменев, и котлет, всего. </ta>
            <ta e="T1247" id="Seg_7093" s="T1243">И много людей было. </ta>
            <ta e="T1254" id="Seg_7094" s="T1248">А потом я купила ей пальто. </ta>
            <ta e="T1262" id="Seg_7095" s="T1255">Тогда на дорогу дала денег сто рублей. </ta>
            <ta e="T1269" id="Seg_7096" s="T1263">И опять она (та-) туды уехала. </ta>
            <ta e="T1278" id="Seg_7097" s="T1270">Свекровка ее не залюбила, она пишет, что плачу. </ta>
            <ta e="T1289" id="Seg_7098" s="T1279">(А=) А мы написали ей: "Чем плакать, приезжай сюды". </ta>
            <ta e="T1295" id="Seg_7099" s="T1290">Ну, она говорила, вдвоем приедут. </ta>
            <ta e="T1306" id="Seg_7100" s="T1296">Ну, а потом говорит: "Я одна приеду", (на-) в письме. </ta>
            <ta e="T1313" id="Seg_7101" s="T1307">А мы говорим: "Почему одна приедешь?" </ta>
            <ta e="T1318" id="Seg_7102" s="T1314">"Ну, говорит, денег нету". </ta>
            <ta e="T1326" id="Seg_7103" s="T1318">Я опять сто рублей послала, и отец пятьдесят. </ta>
            <ta e="T1331" id="Seg_7104" s="T1327">Ну, она приехала одна. </ta>
            <ta e="T1335" id="Seg_7105" s="T1332">А он остался. </ta>
            <ta e="T1341" id="Seg_7106" s="T1336">Она опять ему письмо писала. </ta>
            <ta e="T1347" id="Seg_7107" s="T1342">Не знаю, приедет ли, нет. </ta>
            <ta e="T1364" id="Seg_7108" s="T1348">А потом она уехала туды, где Ленин был в ссылке, (в этой д-) в этот город. </ta>
            <ta e="T1370" id="Seg_7109" s="T1365">И там в столовой работает. </ta>
            <ta e="T1379" id="Seg_7110" s="T1371">И (это=) выдает там водки, всего, чего кушать. </ta>
            <ta e="T1381" id="Seg_7111" s="T1380">Всё. </ta>
            <ta e="T1383" id="Seg_7112" s="T1382">((BRK)). </ta>
            <ta e="T1390" id="Seg_7113" s="T1383">Погодите, я может, (на-) на это… </ta>
            <ta e="T1396" id="Seg_7114" s="T1391">Măn plʼemʼannican koʔbdot tibinə kalla dʼürbi. </ta>
            <ta e="T1402" id="Seg_7115" s="T1397">I dĭn sumna esseŋ ibiʔi. </ta>
            <ta e="T1410" id="Seg_7116" s="T1403">Dĭgəttə (dĭ=) dĭzeŋ kambiʔi (maʔ=) maːʔnə dĭn. </ta>
            <ta e="T1414" id="Seg_7117" s="T1411">I ige turajʔi. </ta>
            <ta e="T1422" id="Seg_7118" s="T1415">Dĭgəttə dĭzeŋ aʔtʼi abiʔi, gijen bazaj aktʼi. </ta>
            <ta e="T1433" id="Seg_7119" s="T1423">Dĭgəttə kambiʔi (dĭ=) dĭbər sʼevʼerdə, dĭn tože abiʔi (aktʼi=) aktʼi. </ta>
            <ta e="T1436" id="Seg_7120" s="T1434">Bazaj aktʼi. </ta>
            <ta e="T1442" id="Seg_7121" s="T1437">Dĭgəttə dĭ šobi maːʔndə, ianə. </ta>
            <ta e="T1448" id="Seg_7122" s="T1443">(Dĭ=) Dĭgəttə dĭ dön ibi. </ta>
            <ta e="T1457" id="Seg_7123" s="T1449">Dĭgəttə sazən pʼaŋdəbiʔi: "Tăn tibi bügən külambi". </ta>
            <ta e="T1461" id="Seg_7124" s="T1458">Dĭgəttə dĭ kambi. </ta>
            <ta e="T1465" id="Seg_7125" s="T1462">Dĭm dʼünə embiʔi. </ta>
            <ta e="T1471" id="Seg_7126" s="T1466">Dĭgəttə bazo dĭʔnə kuza ibi. </ta>
            <ta e="T1475" id="Seg_7127" s="T1472">Dĭn nagobi net. </ta>
            <ta e="T1484" id="Seg_7128" s="T1476">I tüj amnolaʔbəʔjə, i dĭn sumna esseŋdə ibiʔi. </ta>
            <ta e="T1488" id="Seg_7129" s="T1485">Dĭ ibi dĭm. </ta>
            <ta e="T1492" id="Seg_7130" s="T1489">Ugandə jakše kuza. </ta>
            <ta e="T1496" id="Seg_7131" s="T1493">Ara ej bĭtlie. </ta>
            <ta e="T1501" id="Seg_7132" s="T1496">Gibərdə ej mĭnlie, üge togonorlaʔbə. </ta>
            <ta e="T1508" id="Seg_7133" s="T1501">(Dĭn) tože iat naga, tolʼkă onʼiʔ abat. </ta>
            <ta e="T1521" id="Seg_7134" s="T1516">Пускай, пускай она ((…)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T129" id="Seg_7135" s="T128">kamen</ta>
            <ta e="T130" id="Seg_7136" s="T129">măn</ta>
            <ta e="T131" id="Seg_7137" s="T130">amno-bia-m</ta>
            <ta e="T132" id="Seg_7138" s="T131">Tartu</ta>
            <ta e="T133" id="Seg_7139" s="T132">kam-bia-m</ta>
            <ta e="T134" id="Seg_7140" s="T133">i-bie-m</ta>
            <ta e="T136" id="Seg_7141" s="T135">nagur</ta>
            <ta e="T137" id="Seg_7142" s="T136">platok-ə-ʔjə</ta>
            <ta e="T139" id="Seg_7143" s="T138">bos-tə</ta>
            <ta e="T140" id="Seg_7144" s="T139">il-də</ta>
            <ta e="T141" id="Seg_7145" s="T140">mĭ-zittə</ta>
            <ta e="T143" id="Seg_7146" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_7147" s="T143">döbər</ta>
            <ta e="T145" id="Seg_7148" s="T144">šo-bia-m</ta>
            <ta e="T146" id="Seg_7149" s="T145">i-bie-m</ta>
            <ta e="T147" id="Seg_7150" s="T146">piʔme-ʔi</ta>
            <ta e="T149" id="Seg_7151" s="T148">kujnek</ta>
            <ta e="T150" id="Seg_7152" s="T149">i-bie-m</ta>
            <ta e="T152" id="Seg_7153" s="T151">nʼi-nə</ta>
            <ta e="T153" id="Seg_7154" s="T152">plʼemʼannik-a-m-nə</ta>
            <ta e="T155" id="Seg_7155" s="T154">koʔbdo-nə</ta>
            <ta e="T156" id="Seg_7156" s="T155">i-bie-m</ta>
            <ta e="T158" id="Seg_7157" s="T157">platʼtʼa</ta>
            <ta e="T159" id="Seg_7158" s="T158">i-bie-m</ta>
            <ta e="T161" id="Seg_7159" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_7160" s="T161">plʼemʼannik-tə</ta>
            <ta e="T163" id="Seg_7161" s="T162">i-bie-m</ta>
            <ta e="T164" id="Seg_7162" s="T163">kujnek</ta>
            <ta e="T166" id="Seg_7163" s="T165">kabarləj</ta>
            <ta e="T230" id="Seg_7164" s="T228">piʔme-ʔi</ta>
            <ta e="T234" id="Seg_7165" s="T233">piʔme</ta>
            <ta e="T249" id="Seg_7166" s="T247">piʔme</ta>
            <ta e="T251" id="Seg_7167" s="T250">jama-ʔi</ta>
            <ta e="T252" id="Seg_7168" s="T251">piʔme</ta>
            <ta e="T253" id="Seg_7169" s="T252">jaʔma</ta>
            <ta e="T270" id="Seg_7170" s="T268">jaʔma</ta>
            <ta e="T274" id="Seg_7171" s="T273">piʔme-ʔi</ta>
            <ta e="T278" id="Seg_7172" s="T277">kujnek</ta>
            <ta e="T348" id="Seg_7173" s="T347">kujnek</ta>
            <ta e="T353" id="Seg_7174" s="T352">üžü</ta>
            <ta e="T504" id="Seg_7175" s="T503">kamen</ta>
            <ta e="T505" id="Seg_7176" s="T504">dĭ</ta>
            <ta e="T506" id="Seg_7177" s="T505">šo-bi</ta>
            <ta e="T507" id="Seg_7178" s="T506">măna</ta>
            <ta e="T510" id="Seg_7179" s="T509">măn-də</ta>
            <ta e="T511" id="Seg_7180" s="T510">urgaja</ta>
            <ta e="T512" id="Seg_7181" s="T511">măn</ta>
            <ta e="T513" id="Seg_7182" s="T512">tănan</ta>
            <ta e="T514" id="Seg_7183" s="T513">šonə-ga-m</ta>
            <ta e="T515" id="Seg_7184" s="T514">a</ta>
            <ta e="T516" id="Seg_7185" s="T515">măn</ta>
            <ta e="T517" id="Seg_7186" s="T516">măn-də-m</ta>
            <ta e="T518" id="Seg_7187" s="T517">i-ʔ</ta>
            <ta e="T519" id="Seg_7188" s="T518">šo-ʔ</ta>
            <ta e="T521" id="Seg_7189" s="T520">tu-ʔ</ta>
            <ta e="T522" id="Seg_7190" s="T521">măna</ta>
            <ta e="T524" id="Seg_7191" s="T523">kan-a-ʔ</ta>
            <ta e="T525" id="Seg_7192" s="T524">döʔə</ta>
            <ta e="T527" id="Seg_7193" s="T526">tăn</ta>
            <ta e="T528" id="Seg_7194" s="T527">măna</ta>
            <ta e="T531" id="Seg_7195" s="T530">tăn</ta>
            <ta e="T532" id="Seg_7196" s="T531">măna</ta>
            <ta e="T534" id="Seg_7197" s="T533">măn</ta>
            <ta e="T535" id="Seg_7198" s="T534">tăn-zi</ta>
            <ta e="T536" id="Seg_7199" s="T535">ej</ta>
            <ta e="T537" id="Seg_7200" s="T536">dʼăbaktər-la-m</ta>
            <ta e="T539" id="Seg_7201" s="T538">kan-a-ʔ</ta>
            <ta e="T540" id="Seg_7202" s="T539">döʔə</ta>
            <ta e="T556" id="Seg_7203" s="T555">kamen</ta>
            <ta e="T557" id="Seg_7204" s="T556">dĭ</ta>
            <ta e="T558" id="Seg_7205" s="T557">măna</ta>
            <ta e="T559" id="Seg_7206" s="T558">šo-bi</ta>
            <ta e="T560" id="Seg_7207" s="T559">šo-bi</ta>
            <ta e="T571" id="Seg_7208" s="T568">kamen</ta>
            <ta e="T572" id="Seg_7209" s="T571">dĭ</ta>
            <ta e="T573" id="Seg_7210" s="T572">măna</ta>
            <ta e="T574" id="Seg_7211" s="T573">šo-bi</ta>
            <ta e="T575" id="Seg_7212" s="T574">măn</ta>
            <ta e="T576" id="Seg_7213" s="T575">kudol-bia-m</ta>
            <ta e="T578" id="Seg_7214" s="T576">dĭ-m</ta>
            <ta e="T579" id="Seg_7215" s="T578">i-ʔ</ta>
            <ta e="T580" id="Seg_7216" s="T579">šo-ʔ</ta>
            <ta e="T581" id="Seg_7217" s="T580">măna</ta>
            <ta e="T582" id="Seg_7218" s="T581">măn</ta>
            <ta e="T583" id="Seg_7219" s="T582">tănan</ta>
            <ta e="T584" id="Seg_7220" s="T583">ĭmbi=də</ta>
            <ta e="T585" id="Seg_7221" s="T584">ej</ta>
            <ta e="T586" id="Seg_7222" s="T585">nörbə-lə-m</ta>
            <ta e="T588" id="Seg_7223" s="T587">kan-a-ʔ</ta>
            <ta e="T589" id="Seg_7224" s="T588">döʔə</ta>
            <ta e="T591" id="Seg_7225" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_7226" s="T591">dĭ</ta>
            <ta e="T593" id="Seg_7227" s="T592">măn-də</ta>
            <ta e="T594" id="Seg_7228" s="T593">ugandə</ta>
            <ta e="T595" id="Seg_7229" s="T594">jakše</ta>
            <ta e="T596" id="Seg_7230" s="T595">dʼăbaktər-ia-l</ta>
            <ta e="T598" id="Seg_7231" s="T597">măn</ta>
            <ta e="T599" id="Seg_7232" s="T598">măn-də-m</ta>
            <ta e="T601" id="Seg_7233" s="T600">jakše</ta>
            <ta e="T602" id="Seg_7234" s="T601">kudol-lia-m</ta>
            <ta e="T603" id="Seg_7235" s="T602">tănan</ta>
            <ta e="T605" id="Seg_7236" s="T604">a</ta>
            <ta e="T606" id="Seg_7237" s="T605">tăn</ta>
            <ta e="T607" id="Seg_7238" s="T606">măn-lia-l</ta>
            <ta e="T608" id="Seg_7239" s="T607">jakše</ta>
            <ta e="T861" id="Seg_7240" s="T860">kamen</ta>
            <ta e="T862" id="Seg_7241" s="T861">kamen</ta>
            <ta e="T863" id="Seg_7242" s="T862">šo-bi</ta>
            <ta e="T866" id="Seg_7243" s="T865">măna</ta>
            <ta e="T868" id="Seg_7244" s="T867">da</ta>
            <ta e="T869" id="Seg_7245" s="T868">dĭ</ta>
            <ta e="T870" id="Seg_7246" s="T869">pʼaŋdə-bi</ta>
            <ta e="T871" id="Seg_7247" s="T870">măna</ta>
            <ta e="T872" id="Seg_7248" s="T871">šo-la-m</ta>
            <ta e="T873" id="Seg_7249" s="T872">tănan</ta>
            <ta e="T875" id="Seg_7250" s="T874">măn</ta>
            <ta e="T876" id="Seg_7251" s="T875">edəʔ-pie-m</ta>
            <ta e="T877" id="Seg_7252" s="T876">edəʔ-pie-m</ta>
            <ta e="T878" id="Seg_7253" s="T877">naga</ta>
            <ta e="T880" id="Seg_7254" s="T879">dĭgəttə</ta>
            <ta e="T881" id="Seg_7255" s="T880">pʼaŋd-laʔbə</ta>
            <ta e="T882" id="Seg_7256" s="T881">măna</ta>
            <ta e="T883" id="Seg_7257" s="T882">sazən</ta>
            <ta e="T884" id="Seg_7258" s="T883">măn</ta>
            <ta e="T885" id="Seg_7259" s="T884">ej</ta>
            <ta e="T886" id="Seg_7260" s="T885">šo-la-m</ta>
            <ta e="T888" id="Seg_7261" s="T887">dö</ta>
            <ta e="T890" id="Seg_7262" s="T889">dĭ</ta>
            <ta e="T891" id="Seg_7263" s="T890">kö-gən</ta>
            <ta e="T892" id="Seg_7264" s="T891">šo</ta>
            <ta e="T893" id="Seg_7265" s="T892">šo-la-m</ta>
            <ta e="T895" id="Seg_7266" s="T894">tănan</ta>
            <ta e="T897" id="Seg_7267" s="T896">a</ta>
            <ta e="T898" id="Seg_7268" s="T897">măn</ta>
            <ta e="T899" id="Seg_7269" s="T898">măm-bia-m</ta>
            <ta e="T900" id="Seg_7270" s="T899">măn</ta>
            <ta e="T901" id="Seg_7271" s="T900">možet</ta>
            <ta e="T902" id="Seg_7272" s="T901">kü-lal-lə-m</ta>
            <ta e="T904" id="Seg_7273" s="T903">a</ta>
            <ta e="T905" id="Seg_7274" s="T904">tăn</ta>
            <ta e="T906" id="Seg_7275" s="T905">dĭgəttə</ta>
            <ta e="T907" id="Seg_7276" s="T906">šo-la-l</ta>
            <ta e="T909" id="Seg_7277" s="T908">dĭgəttə</ta>
            <ta e="T910" id="Seg_7278" s="T909">oʔbdə-bia-m</ta>
            <ta e="T911" id="Seg_7279" s="T910">da</ta>
            <ta e="T912" id="Seg_7280" s="T911">döbər</ta>
            <ta e="T913" id="Seg_7281" s="T912">šo-bia-m</ta>
            <ta e="T917" id="Seg_7282" s="T916">măn</ta>
            <ta e="T918" id="Seg_7283" s="T917">koʔbdo-m</ta>
            <ta e="T919" id="Seg_7284" s="T918">tibi-nə</ta>
            <ta e="T920" id="Seg_7285" s="T919">kam-bi</ta>
            <ta e="T922" id="Seg_7286" s="T921">i-bie-m</ta>
            <ta e="T923" id="Seg_7287" s="T922">Krasnăjarsk-a-gən</ta>
            <ta e="T924" id="Seg_7288" s="T923">dĭ</ta>
            <ta e="T926" id="Seg_7289" s="T924">măn-də</ta>
            <ta e="T927" id="Seg_7290" s="T926">măna</ta>
            <ta e="T928" id="Seg_7291" s="T927">onʼiʔ</ta>
            <ta e="T929" id="Seg_7292" s="T928">nʼi</ta>
            <ta e="T930" id="Seg_7293" s="T929">i-zittə</ta>
            <ta e="T931" id="Seg_7294" s="T930">aba-nə</ta>
            <ta e="T932" id="Seg_7295" s="T931">i-ʔ</ta>
            <ta e="T933" id="Seg_7296" s="T932">nörbi-t</ta>
            <ta e="T935" id="Seg_7297" s="T934">a</ta>
            <ta e="T936" id="Seg_7298" s="T935">măn</ta>
            <ta e="T938" id="Seg_7299" s="T936">măm-bia-m</ta>
            <ta e="T939" id="Seg_7300" s="T938">kak</ta>
            <ta e="T940" id="Seg_7301" s="T939">ej</ta>
            <ta e="T941" id="Seg_7302" s="T940">nörbə-sʼtə</ta>
            <ta e="T942" id="Seg_7303" s="T941">aba-t</ta>
            <ta e="T943" id="Seg_7304" s="T942">bos-tə</ta>
            <ta e="T944" id="Seg_7305" s="T943">i-bi</ta>
            <ta e="T945" id="Seg_7306" s="T944">bos-kəndə</ta>
            <ta e="T946" id="Seg_7307" s="T945">i</ta>
            <ta e="T948" id="Seg_7308" s="T947">i</ta>
            <ta e="T949" id="Seg_7309" s="T948">šiʔ</ta>
            <ta e="T950" id="Seg_7310" s="T949">deʔ-pi</ta>
            <ta e="T952" id="Seg_7311" s="T951">dĭgəttə</ta>
            <ta e="T953" id="Seg_7312" s="T952">šo-bia-m</ta>
            <ta e="T954" id="Seg_7313" s="T953">aba-nə</ta>
            <ta e="T955" id="Seg_7314" s="T954">nörbə-bie-m</ta>
            <ta e="T956" id="Seg_7315" s="T955">dĭ</ta>
            <ta e="T957" id="Seg_7316" s="T956">pʼaŋdə-bi</ta>
            <ta e="T958" id="Seg_7317" s="T957">dĭʔ-nə</ta>
            <ta e="T959" id="Seg_7318" s="T958">sazən</ta>
            <ta e="T961" id="Seg_7319" s="T960">dĭgəttə</ta>
            <ta e="T962" id="Seg_7320" s="T961">dĭ</ta>
            <ta e="T963" id="Seg_7321" s="T962">dĭʔ-nə</ta>
            <ta e="T964" id="Seg_7322" s="T963">sazən</ta>
            <ta e="T965" id="Seg_7323" s="T964">pʼaŋdə-bi</ta>
            <ta e="T966" id="Seg_7324" s="T965">dĭ</ta>
            <ta e="T967" id="Seg_7325" s="T966">döbər</ta>
            <ta e="T968" id="Seg_7326" s="T967">šo-bi</ta>
            <ta e="T970" id="Seg_7327" s="T969">dĭgəttə</ta>
            <ta e="T971" id="Seg_7328" s="T970">uja</ta>
            <ta e="T972" id="Seg_7329" s="T971">šoška</ta>
            <ta e="T973" id="Seg_7330" s="T972">dʼagar-bi-baʔ</ta>
            <ta e="T974" id="Seg_7331" s="T973">uja</ta>
            <ta e="T975" id="Seg_7332" s="T974">iʔgö</ta>
            <ta e="T976" id="Seg_7333" s="T975">ipek</ta>
            <ta e="T977" id="Seg_7334" s="T976">pür-bi-beʔ</ta>
            <ta e="T978" id="Seg_7335" s="T977">i</ta>
            <ta e="T979" id="Seg_7336" s="T978">il</ta>
            <ta e="T980" id="Seg_7337" s="T979">oʔbdə-bi-baʔ</ta>
            <ta e="T982" id="Seg_7338" s="T981">dĭgəttə</ta>
            <ta e="T983" id="Seg_7339" s="T982">ara</ta>
            <ta e="T984" id="Seg_7340" s="T983">i-bi</ta>
            <ta e="T986" id="Seg_7341" s="T985">pʼelʼmʼenʼ-ə-ʔi</ta>
            <ta e="T987" id="Seg_7342" s="T986">a-bi-baʔ</ta>
            <ta e="T988" id="Seg_7343" s="T987">kătleta-ʔi</ta>
            <ta e="T989" id="Seg_7344" s="T988">iʔgö</ta>
            <ta e="T990" id="Seg_7345" s="T989">i-bi</ta>
            <ta e="T991" id="Seg_7346" s="T990">ĭmbi</ta>
            <ta e="T992" id="Seg_7347" s="T991">i-bi</ta>
            <ta e="T994" id="Seg_7348" s="T993">i</ta>
            <ta e="T995" id="Seg_7349" s="T994">il</ta>
            <ta e="T996" id="Seg_7350" s="T995">iʔgö</ta>
            <ta e="T997" id="Seg_7351" s="T996">i-bi-ʔi</ta>
            <ta e="T1525" id="Seg_7352" s="T998">Kazan</ta>
            <ta e="T999" id="Seg_7353" s="T1525">tura-gən</ta>
            <ta e="T1000" id="Seg_7354" s="T999">šo-bi-ʔi</ta>
            <ta e="T1002" id="Seg_7355" s="T1001">dĭgəttə</ta>
            <ta e="T1003" id="Seg_7356" s="T1002">măn</ta>
            <ta e="T1004" id="Seg_7357" s="T1003">dĭʔ-nə</ta>
            <ta e="T1005" id="Seg_7358" s="T1004">i-bie-m</ta>
            <ta e="T1006" id="Seg_7359" s="T1005">palʼto</ta>
            <ta e="T1008" id="Seg_7360" s="T1007">dĭgəttə</ta>
            <ta e="T1009" id="Seg_7361" s="T1008">aktʼa</ta>
            <ta e="T1010" id="Seg_7362" s="T1009">dĭʔ-nə</ta>
            <ta e="T1012" id="Seg_7363" s="T1011">mĭ-bie-m</ta>
            <ta e="T1014" id="Seg_7364" s="T1013">dĭ</ta>
            <ta e="T1522" id="Seg_7365" s="T1014">kal-la</ta>
            <ta e="T1015" id="Seg_7366" s="T1522">dʼür-bi</ta>
            <ta e="T1016" id="Seg_7367" s="T1015">dĭ</ta>
            <ta e="T1017" id="Seg_7368" s="T1016">nʼi-zi</ta>
            <ta e="T1019" id="Seg_7369" s="T1018">dĭbər</ta>
            <ta e="T1020" id="Seg_7370" s="T1019">gijen</ta>
            <ta e="T1022" id="Seg_7371" s="T1021">il</ta>
            <ta e="T1023" id="Seg_7372" s="T1022">gijen</ta>
            <ta e="T1024" id="Seg_7373" s="T1023">il</ta>
            <ta e="T1025" id="Seg_7374" s="T1024">dʼăbaktərzi</ta>
            <ta e="T1026" id="Seg_7375" s="T1025">dʼăbaktər-ia-ʔi</ta>
            <ta e="T1027" id="Seg_7376" s="T1026">bos-tə</ta>
            <ta e="T1028" id="Seg_7377" s="T1027">šĭke-t-si</ta>
            <ta e="T1030" id="Seg_7378" s="T1029">dĭgəttə</ta>
            <ta e="T1031" id="Seg_7379" s="T1030">dĭ</ta>
            <ta e="T1032" id="Seg_7380" s="T1031">dĭ</ta>
            <ta e="T1033" id="Seg_7381" s="T1032">nʼi-n</ta>
            <ta e="T1034" id="Seg_7382" s="T1033">ia</ta>
            <ta e="T1036" id="Seg_7383" s="T1035">dĭ-m</ta>
            <ta e="T1037" id="Seg_7384" s="T1036">dĭ-m</ta>
            <ta e="T1038" id="Seg_7385" s="T1037">ej</ta>
            <ta e="T1039" id="Seg_7386" s="T1038">ajir-bi</ta>
            <ta e="T1040" id="Seg_7387" s="T1039">dĭgəttə</ta>
            <ta e="T1041" id="Seg_7388" s="T1040">dĭ</ta>
            <ta e="T1042" id="Seg_7389" s="T1041">döbər</ta>
            <ta e="T1044" id="Seg_7390" s="T1042">pʼaŋd-laʔbə</ta>
            <ta e="T1045" id="Seg_7391" s="T1044">kažen-</ta>
            <ta e="T1046" id="Seg_7392" s="T1045">kažnej</ta>
            <ta e="T1048" id="Seg_7393" s="T1047">dʼala</ta>
            <ta e="T1049" id="Seg_7394" s="T1048">dʼor-laʔbə-m</ta>
            <ta e="T1051" id="Seg_7395" s="T1050">miʔ</ta>
            <ta e="T1053" id="Seg_7396" s="T1052">dĭʔ-nə</ta>
            <ta e="T1055" id="Seg_7397" s="T1053">pʼaŋdə-sʼtə</ta>
            <ta e="T1056" id="Seg_7398" s="T1055">šo-ʔ</ta>
            <ta e="T1057" id="Seg_7399" s="T1056">maʔ-nə-l</ta>
            <ta e="T1059" id="Seg_7400" s="T1058">dĭgəttə</ta>
            <ta e="T1060" id="Seg_7401" s="T1059">dĭ</ta>
            <ta e="T1061" id="Seg_7402" s="T1060">măn-də</ta>
            <ta e="T1062" id="Seg_7403" s="T1061">măn</ta>
            <ta e="T1063" id="Seg_7404" s="T1062">unnʼa</ta>
            <ta e="T1064" id="Seg_7405" s="T1063">šo-la-m</ta>
            <ta e="T1066" id="Seg_7406" s="T1065">a</ta>
            <ta e="T1067" id="Seg_7407" s="T1066">ĭmbi</ta>
            <ta e="T1068" id="Seg_7408" s="T1067">unnʼa</ta>
            <ta e="T1069" id="Seg_7409" s="T1068">šo-la-l</ta>
            <ta e="T1071" id="Seg_7410" s="T1070">a</ta>
            <ta e="T1072" id="Seg_7411" s="T1071">dĭ</ta>
            <ta e="T1073" id="Seg_7412" s="T1072">bazo</ta>
            <ta e="T1074" id="Seg_7413" s="T1073">pʼaŋd-laʔbə</ta>
            <ta e="T1075" id="Seg_7414" s="T1074">aktʼa</ta>
            <ta e="T1076" id="Seg_7415" s="T1075">naga</ta>
            <ta e="T1078" id="Seg_7416" s="T1077">măn</ta>
            <ta e="T1079" id="Seg_7417" s="T1078">bazo</ta>
            <ta e="T1080" id="Seg_7418" s="T1079">öʔlu-bie-m</ta>
            <ta e="T1081" id="Seg_7419" s="T1080">šide</ta>
            <ta e="T1082" id="Seg_7420" s="T1081">bʼeʔ</ta>
            <ta e="T1083" id="Seg_7421" s="T1082">šide</ta>
            <ta e="T1084" id="Seg_7422" s="T1083">biʔ</ta>
            <ta e="T1085" id="Seg_7423" s="T1084">aktʼa</ta>
            <ta e="T1087" id="Seg_7424" s="T1086">dĭgəttə</ta>
            <ta e="T1088" id="Seg_7425" s="T1087">aba-t</ta>
            <ta e="T1089" id="Seg_7426" s="T1088">sumna</ta>
            <ta e="T1090" id="Seg_7427" s="T1089">šide</ta>
            <ta e="T1091" id="Seg_7428" s="T1090">biʔ</ta>
            <ta e="T1092" id="Seg_7429" s="T1091">öʔlu-bi</ta>
            <ta e="T1094" id="Seg_7430" s="T1093">dĭgəttə</ta>
            <ta e="T1095" id="Seg_7431" s="T1094">dö</ta>
            <ta e="T1096" id="Seg_7432" s="T1095">šo-bi</ta>
            <ta e="T1097" id="Seg_7433" s="T1096">onʼiʔ</ta>
            <ta e="T1099" id="Seg_7434" s="T1098">dĭ</ta>
            <ta e="T1100" id="Seg_7435" s="T1099">ma-luʔ-pi</ta>
            <ta e="T1102" id="Seg_7436" s="T1101">dĭgəttə</ta>
            <ta e="T1103" id="Seg_7437" s="T1102">dĭ</ta>
            <ta e="T1104" id="Seg_7438" s="T1103">dĭʔ-nə</ta>
            <ta e="T1105" id="Seg_7439" s="T1104">dĭ</ta>
            <ta e="T1106" id="Seg_7440" s="T1105">pʼaŋdə-bi</ta>
            <ta e="T1107" id="Seg_7441" s="T1106">pʼaŋdə-bi</ta>
            <ta e="T1109" id="Seg_7442" s="T1108">dĭ</ta>
            <ta e="T1110" id="Seg_7443" s="T1109">ej</ta>
            <ta e="T1111" id="Seg_7444" s="T1110">üge</ta>
            <ta e="T1112" id="Seg_7445" s="T1111">ej</ta>
            <ta e="T1113" id="Seg_7446" s="T1112">šo-lia</ta>
            <ta e="T1115" id="Seg_7447" s="T1114">dĭgəttə</ta>
            <ta e="T1116" id="Seg_7448" s="T1115">dĭ</ta>
            <ta e="T1523" id="Seg_7449" s="T1116">kal-la</ta>
            <ta e="T1117" id="Seg_7450" s="T1523">dʼür-bi</ta>
            <ta e="T1120" id="Seg_7451" s="T1119">gijen</ta>
            <ta e="T1121" id="Seg_7452" s="T1120">Lenin</ta>
            <ta e="T1122" id="Seg_7453" s="T1121">amno-bi</ta>
            <ta e="T1123" id="Seg_7454" s="T1122">dĭ</ta>
            <ta e="T1124" id="Seg_7455" s="T1123">tura-nə</ta>
            <ta e="T1126" id="Seg_7456" s="T1125">dĭn</ta>
            <ta e="T1127" id="Seg_7457" s="T1126">stălovăjă-n</ta>
            <ta e="T1130" id="Seg_7458" s="T1129">stălovăjă-n</ta>
            <ta e="T1132" id="Seg_7459" s="T1131">ipek</ta>
            <ta e="T1133" id="Seg_7460" s="T1132">mĭ-lie</ta>
            <ta e="T1134" id="Seg_7461" s="T1133">ara</ta>
            <ta e="T1135" id="Seg_7462" s="T1134">mĭ-lie</ta>
            <ta e="T1136" id="Seg_7463" s="T1135">bar</ta>
            <ta e="T1137" id="Seg_7464" s="T1136">ĭmbi</ta>
            <ta e="T1138" id="Seg_7465" s="T1137">mĭ-lie</ta>
            <ta e="T1139" id="Seg_7466" s="T1138">pʼaŋd-laʔbə</ta>
            <ta e="T1141" id="Seg_7467" s="T1140">bʼeʔ</ta>
            <ta e="T1143" id="Seg_7468" s="T1142">bʼeʔ</ta>
            <ta e="T1144" id="Seg_7469" s="T1143">nagur</ta>
            <ta e="T1145" id="Seg_7470" s="T1144">i</ta>
            <ta e="T1146" id="Seg_7471" s="T1145">sumna</ta>
            <ta e="T1147" id="Seg_7472" s="T1146">aktʼa</ta>
            <ta e="T1148" id="Seg_7473" s="T1147">i-lie-t</ta>
            <ta e="T1392" id="Seg_7474" s="T1391">măn</ta>
            <ta e="T1393" id="Seg_7475" s="T1392">plʼemʼannica-n</ta>
            <ta e="T1394" id="Seg_7476" s="T1393">koʔbdo-t</ta>
            <ta e="T1395" id="Seg_7477" s="T1394">tibi-nə</ta>
            <ta e="T1524" id="Seg_7478" s="T1395">kal-la</ta>
            <ta e="T1396" id="Seg_7479" s="T1524">dʼür-bi</ta>
            <ta e="T1398" id="Seg_7480" s="T1397">i</ta>
            <ta e="T1399" id="Seg_7481" s="T1398">dĭn</ta>
            <ta e="T1400" id="Seg_7482" s="T1399">sumna</ta>
            <ta e="T1401" id="Seg_7483" s="T1400">es-seŋ</ta>
            <ta e="T1402" id="Seg_7484" s="T1401">i-bi-ʔi</ta>
            <ta e="T1404" id="Seg_7485" s="T1403">dĭgəttə</ta>
            <ta e="T1405" id="Seg_7486" s="T1404">dĭ</ta>
            <ta e="T1406" id="Seg_7487" s="T1405">dĭ-zeŋ</ta>
            <ta e="T1407" id="Seg_7488" s="T1406">kam-bi-ʔi</ta>
            <ta e="T1408" id="Seg_7489" s="T1407">maʔ</ta>
            <ta e="T1409" id="Seg_7490" s="T1408">maːʔ-nə</ta>
            <ta e="T1410" id="Seg_7491" s="T1409">dĭn</ta>
            <ta e="T1412" id="Seg_7492" s="T1411">i</ta>
            <ta e="T1413" id="Seg_7493" s="T1412">i-ge</ta>
            <ta e="T1414" id="Seg_7494" s="T1413">turaj-ʔi</ta>
            <ta e="T1416" id="Seg_7495" s="T1415">dĭgəttə</ta>
            <ta e="T1417" id="Seg_7496" s="T1416">dĭ-zeŋ</ta>
            <ta e="T1418" id="Seg_7497" s="T1417">aʔtʼi</ta>
            <ta e="T1419" id="Seg_7498" s="T1418">a-bi-ʔi</ta>
            <ta e="T1420" id="Seg_7499" s="T1419">gijen</ta>
            <ta e="T1421" id="Seg_7500" s="T1420">baza-j</ta>
            <ta e="T1422" id="Seg_7501" s="T1421">aktʼi</ta>
            <ta e="T1424" id="Seg_7502" s="T1423">dĭgəttə</ta>
            <ta e="T1425" id="Seg_7503" s="T1424">kam-bi-ʔi</ta>
            <ta e="T1426" id="Seg_7504" s="T1425">dĭ</ta>
            <ta e="T1427" id="Seg_7505" s="T1426">dĭbər</ta>
            <ta e="T1428" id="Seg_7506" s="T1427">sʼevʼer-də</ta>
            <ta e="T1429" id="Seg_7507" s="T1428">dĭn</ta>
            <ta e="T1430" id="Seg_7508" s="T1429">tože</ta>
            <ta e="T1431" id="Seg_7509" s="T1430">a-bi-ʔi</ta>
            <ta e="T1432" id="Seg_7510" s="T1431">aktʼi</ta>
            <ta e="T1433" id="Seg_7511" s="T1432">aktʼi</ta>
            <ta e="T1435" id="Seg_7512" s="T1434">baza-j</ta>
            <ta e="T1436" id="Seg_7513" s="T1435">aktʼi</ta>
            <ta e="T1438" id="Seg_7514" s="T1437">dĭgəttə</ta>
            <ta e="T1439" id="Seg_7515" s="T1438">dĭ</ta>
            <ta e="T1440" id="Seg_7516" s="T1439">šo-bi</ta>
            <ta e="T1441" id="Seg_7517" s="T1440">maːʔ-ndə</ta>
            <ta e="T1442" id="Seg_7518" s="T1441">ia-nə</ta>
            <ta e="T1444" id="Seg_7519" s="T1443">dĭ</ta>
            <ta e="T1445" id="Seg_7520" s="T1444">dĭgəttə</ta>
            <ta e="T1446" id="Seg_7521" s="T1445">dĭ</ta>
            <ta e="T1447" id="Seg_7522" s="T1446">dön</ta>
            <ta e="T1448" id="Seg_7523" s="T1447">i-bi</ta>
            <ta e="T1450" id="Seg_7524" s="T1449">dĭgəttə</ta>
            <ta e="T1451" id="Seg_7525" s="T1450">sazən</ta>
            <ta e="T1453" id="Seg_7526" s="T1451">pʼaŋdə-bi-ʔi</ta>
            <ta e="T1454" id="Seg_7527" s="T1453">tăn</ta>
            <ta e="T1455" id="Seg_7528" s="T1454">tibi</ta>
            <ta e="T1456" id="Seg_7529" s="T1455">bü-gən</ta>
            <ta e="T1457" id="Seg_7530" s="T1456">kü-lam-bi</ta>
            <ta e="T1459" id="Seg_7531" s="T1458">dĭgəttə</ta>
            <ta e="T1460" id="Seg_7532" s="T1459">dĭ</ta>
            <ta e="T1461" id="Seg_7533" s="T1460">kam-bi</ta>
            <ta e="T1463" id="Seg_7534" s="T1462">dĭ-m</ta>
            <ta e="T1464" id="Seg_7535" s="T1463">dʼü-nə</ta>
            <ta e="T1465" id="Seg_7536" s="T1464">em-bi-ʔi</ta>
            <ta e="T1467" id="Seg_7537" s="T1466">dĭgəttə</ta>
            <ta e="T1468" id="Seg_7538" s="T1467">bazo</ta>
            <ta e="T1469" id="Seg_7539" s="T1468">dĭʔ-nə</ta>
            <ta e="T1470" id="Seg_7540" s="T1469">kuza</ta>
            <ta e="T1471" id="Seg_7541" s="T1470">i-bi</ta>
            <ta e="T1473" id="Seg_7542" s="T1472">dĭn</ta>
            <ta e="T1474" id="Seg_7543" s="T1473">nago-bi</ta>
            <ta e="T1475" id="Seg_7544" s="T1474">ne-t</ta>
            <ta e="T1477" id="Seg_7545" s="T1476">i</ta>
            <ta e="T1478" id="Seg_7546" s="T1477">tüj</ta>
            <ta e="T1479" id="Seg_7547" s="T1478">amno-laʔbə-ʔjə</ta>
            <ta e="T1480" id="Seg_7548" s="T1479">i</ta>
            <ta e="T1481" id="Seg_7549" s="T1480">dĭn</ta>
            <ta e="T1482" id="Seg_7550" s="T1481">sumna</ta>
            <ta e="T1483" id="Seg_7551" s="T1482">es-seŋ-də</ta>
            <ta e="T1484" id="Seg_7552" s="T1483">i-bi-ʔi</ta>
            <ta e="T1486" id="Seg_7553" s="T1485">dĭ</ta>
            <ta e="T1487" id="Seg_7554" s="T1486">i-bi</ta>
            <ta e="T1488" id="Seg_7555" s="T1487">dĭ-m</ta>
            <ta e="T1490" id="Seg_7556" s="T1489">ugandə</ta>
            <ta e="T1491" id="Seg_7557" s="T1490">jakše</ta>
            <ta e="T1492" id="Seg_7558" s="T1491">kuza</ta>
            <ta e="T1494" id="Seg_7559" s="T1493">ara</ta>
            <ta e="T1495" id="Seg_7560" s="T1494">ej</ta>
            <ta e="T1496" id="Seg_7561" s="T1495">bĭt-lie</ta>
            <ta e="T1497" id="Seg_7562" s="T1496">gibər=də</ta>
            <ta e="T1498" id="Seg_7563" s="T1497">ej</ta>
            <ta e="T1499" id="Seg_7564" s="T1498">mĭn-lie</ta>
            <ta e="T1500" id="Seg_7565" s="T1499">üge</ta>
            <ta e="T1501" id="Seg_7566" s="T1500">togonor-laʔbə</ta>
            <ta e="T1502" id="Seg_7567" s="T1501">dĭn</ta>
            <ta e="T1503" id="Seg_7568" s="T1502">tože</ta>
            <ta e="T1504" id="Seg_7569" s="T1503">ia-t</ta>
            <ta e="T1505" id="Seg_7570" s="T1504">naga</ta>
            <ta e="T1506" id="Seg_7571" s="T1505">tolʼkă</ta>
            <ta e="T1507" id="Seg_7572" s="T1506">onʼiʔ</ta>
            <ta e="T1508" id="Seg_7573" s="T1507">aba-t</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T129" id="Seg_7574" s="T128">kamən</ta>
            <ta e="T130" id="Seg_7575" s="T129">măn</ta>
            <ta e="T131" id="Seg_7576" s="T130">amnə-bi-m</ta>
            <ta e="T132" id="Seg_7577" s="T131">Tartu</ta>
            <ta e="T133" id="Seg_7578" s="T132">kan-bi-m</ta>
            <ta e="T134" id="Seg_7579" s="T133">i-bi-m</ta>
            <ta e="T136" id="Seg_7580" s="T135">nagur</ta>
            <ta e="T137" id="Seg_7581" s="T136">platok-ə-jəʔ</ta>
            <ta e="T139" id="Seg_7582" s="T138">bos-də</ta>
            <ta e="T140" id="Seg_7583" s="T139">il-də</ta>
            <ta e="T141" id="Seg_7584" s="T140">mĭ-zittə</ta>
            <ta e="T143" id="Seg_7585" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_7586" s="T143">döbər</ta>
            <ta e="T145" id="Seg_7587" s="T144">šo-bi-m</ta>
            <ta e="T146" id="Seg_7588" s="T145">i-bi-m</ta>
            <ta e="T147" id="Seg_7589" s="T146">piʔme-jəʔ</ta>
            <ta e="T149" id="Seg_7590" s="T148">kujnek</ta>
            <ta e="T150" id="Seg_7591" s="T149">i-bi-m</ta>
            <ta e="T152" id="Seg_7592" s="T151">nʼi-Tə</ta>
            <ta e="T153" id="Seg_7593" s="T152">plʼemʼannik-ə-m-Tə</ta>
            <ta e="T155" id="Seg_7594" s="T154">koʔbdo-Tə</ta>
            <ta e="T156" id="Seg_7595" s="T155">i-bi-m</ta>
            <ta e="T158" id="Seg_7596" s="T157">platʼtʼa</ta>
            <ta e="T159" id="Seg_7597" s="T158">i-bi-m</ta>
            <ta e="T161" id="Seg_7598" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_7599" s="T161">plʼemʼannik-Tə</ta>
            <ta e="T163" id="Seg_7600" s="T162">i-bi-m</ta>
            <ta e="T164" id="Seg_7601" s="T163">kujnek</ta>
            <ta e="T166" id="Seg_7602" s="T165">kabarləj</ta>
            <ta e="T230" id="Seg_7603" s="T228">piʔme-jəʔ</ta>
            <ta e="T234" id="Seg_7604" s="T233">piʔme</ta>
            <ta e="T249" id="Seg_7605" s="T247">piʔme</ta>
            <ta e="T251" id="Seg_7606" s="T250">jama-jəʔ</ta>
            <ta e="T252" id="Seg_7607" s="T251">piʔme</ta>
            <ta e="T253" id="Seg_7608" s="T252">jama</ta>
            <ta e="T270" id="Seg_7609" s="T268">jama</ta>
            <ta e="T274" id="Seg_7610" s="T273">piʔme-jəʔ</ta>
            <ta e="T278" id="Seg_7611" s="T277">kujnek</ta>
            <ta e="T348" id="Seg_7612" s="T347">kujnek</ta>
            <ta e="T353" id="Seg_7613" s="T352">üžü</ta>
            <ta e="T504" id="Seg_7614" s="T503">kamən</ta>
            <ta e="T505" id="Seg_7615" s="T504">dĭ</ta>
            <ta e="T506" id="Seg_7616" s="T505">šo-bi</ta>
            <ta e="T507" id="Seg_7617" s="T506">măna</ta>
            <ta e="T510" id="Seg_7618" s="T509">măn-ntə</ta>
            <ta e="T511" id="Seg_7619" s="T510">urgaja</ta>
            <ta e="T512" id="Seg_7620" s="T511">măn</ta>
            <ta e="T513" id="Seg_7621" s="T512">tănan</ta>
            <ta e="T514" id="Seg_7622" s="T513">šonə-gA-m</ta>
            <ta e="T515" id="Seg_7623" s="T514">a</ta>
            <ta e="T516" id="Seg_7624" s="T515">măn</ta>
            <ta e="T517" id="Seg_7625" s="T516">măn-ntə-m</ta>
            <ta e="T518" id="Seg_7626" s="T517">e-ʔ</ta>
            <ta e="T519" id="Seg_7627" s="T518">šo-ʔ</ta>
            <ta e="T521" id="Seg_7628" s="T520">tu-ʔ</ta>
            <ta e="T522" id="Seg_7629" s="T521">măna</ta>
            <ta e="T524" id="Seg_7630" s="T523">kan-ə-ʔ</ta>
            <ta e="T525" id="Seg_7631" s="T524">döʔə</ta>
            <ta e="T527" id="Seg_7632" s="T526">tăn</ta>
            <ta e="T528" id="Seg_7633" s="T527">măna</ta>
            <ta e="T531" id="Seg_7634" s="T530">tăn</ta>
            <ta e="T532" id="Seg_7635" s="T531">măna</ta>
            <ta e="T534" id="Seg_7636" s="T533">măn</ta>
            <ta e="T535" id="Seg_7637" s="T534">tăn-ziʔ</ta>
            <ta e="T536" id="Seg_7638" s="T535">ej</ta>
            <ta e="T537" id="Seg_7639" s="T536">tʼăbaktər-lV-m</ta>
            <ta e="T539" id="Seg_7640" s="T538">kan-ə-ʔ</ta>
            <ta e="T540" id="Seg_7641" s="T539">döʔə</ta>
            <ta e="T556" id="Seg_7642" s="T555">kamən</ta>
            <ta e="T557" id="Seg_7643" s="T556">dĭ</ta>
            <ta e="T558" id="Seg_7644" s="T557">măna</ta>
            <ta e="T559" id="Seg_7645" s="T558">šo-bi</ta>
            <ta e="T560" id="Seg_7646" s="T559">šo-bi</ta>
            <ta e="T571" id="Seg_7647" s="T568">kamən</ta>
            <ta e="T572" id="Seg_7648" s="T571">dĭ</ta>
            <ta e="T573" id="Seg_7649" s="T572">măna</ta>
            <ta e="T574" id="Seg_7650" s="T573">šo-bi</ta>
            <ta e="T575" id="Seg_7651" s="T574">măn</ta>
            <ta e="T576" id="Seg_7652" s="T575">kudonz-bi-m</ta>
            <ta e="T578" id="Seg_7653" s="T576">dĭ-m</ta>
            <ta e="T579" id="Seg_7654" s="T578">e-ʔ</ta>
            <ta e="T580" id="Seg_7655" s="T579">šo-ʔ</ta>
            <ta e="T581" id="Seg_7656" s="T580">măna</ta>
            <ta e="T582" id="Seg_7657" s="T581">măn</ta>
            <ta e="T583" id="Seg_7658" s="T582">tănan</ta>
            <ta e="T584" id="Seg_7659" s="T583">ĭmbi=də</ta>
            <ta e="T585" id="Seg_7660" s="T584">ej</ta>
            <ta e="T586" id="Seg_7661" s="T585">nörbə-lV-m</ta>
            <ta e="T588" id="Seg_7662" s="T587">kan-ə-ʔ</ta>
            <ta e="T589" id="Seg_7663" s="T588">döʔə</ta>
            <ta e="T591" id="Seg_7664" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_7665" s="T591">dĭ</ta>
            <ta e="T593" id="Seg_7666" s="T592">măn-ntə</ta>
            <ta e="T594" id="Seg_7667" s="T593">ugaːndə</ta>
            <ta e="T595" id="Seg_7668" s="T594">jakšə</ta>
            <ta e="T596" id="Seg_7669" s="T595">tʼăbaktər-liA-l</ta>
            <ta e="T598" id="Seg_7670" s="T597">măn</ta>
            <ta e="T599" id="Seg_7671" s="T598">măn-ntə-m</ta>
            <ta e="T601" id="Seg_7672" s="T600">jakšə</ta>
            <ta e="T602" id="Seg_7673" s="T601">kudonz-liA-m</ta>
            <ta e="T603" id="Seg_7674" s="T602">tănan</ta>
            <ta e="T605" id="Seg_7675" s="T604">a</ta>
            <ta e="T606" id="Seg_7676" s="T605">tăn</ta>
            <ta e="T607" id="Seg_7677" s="T606">măn-liA-l</ta>
            <ta e="T608" id="Seg_7678" s="T607">jakšə</ta>
            <ta e="T861" id="Seg_7679" s="T860">kamən</ta>
            <ta e="T862" id="Seg_7680" s="T861">kamən</ta>
            <ta e="T863" id="Seg_7681" s="T862">šo-bi</ta>
            <ta e="T866" id="Seg_7682" s="T865">măna</ta>
            <ta e="T868" id="Seg_7683" s="T867">da</ta>
            <ta e="T869" id="Seg_7684" s="T868">dĭ</ta>
            <ta e="T870" id="Seg_7685" s="T869">pʼaŋdə-bi</ta>
            <ta e="T871" id="Seg_7686" s="T870">măna</ta>
            <ta e="T872" id="Seg_7687" s="T871">šo-lV-m</ta>
            <ta e="T873" id="Seg_7688" s="T872">tănan</ta>
            <ta e="T875" id="Seg_7689" s="T874">măn</ta>
            <ta e="T876" id="Seg_7690" s="T875">edəʔ-bi-m</ta>
            <ta e="T877" id="Seg_7691" s="T876">edəʔ-bi-m</ta>
            <ta e="T878" id="Seg_7692" s="T877">naga</ta>
            <ta e="T880" id="Seg_7693" s="T879">dĭgəttə</ta>
            <ta e="T881" id="Seg_7694" s="T880">pʼaŋdə-laʔbə</ta>
            <ta e="T882" id="Seg_7695" s="T881">măna</ta>
            <ta e="T883" id="Seg_7696" s="T882">sazən</ta>
            <ta e="T884" id="Seg_7697" s="T883">măn</ta>
            <ta e="T885" id="Seg_7698" s="T884">ej</ta>
            <ta e="T886" id="Seg_7699" s="T885">šo-lV-m</ta>
            <ta e="T888" id="Seg_7700" s="T887">dö</ta>
            <ta e="T890" id="Seg_7701" s="T889">dĭ</ta>
            <ta e="T891" id="Seg_7702" s="T890">kö-Kən</ta>
            <ta e="T892" id="Seg_7703" s="T891">šo</ta>
            <ta e="T893" id="Seg_7704" s="T892">šo-lV-m</ta>
            <ta e="T895" id="Seg_7705" s="T894">tănan</ta>
            <ta e="T897" id="Seg_7706" s="T896">a</ta>
            <ta e="T898" id="Seg_7707" s="T897">măn</ta>
            <ta e="T899" id="Seg_7708" s="T898">măn-bi-m</ta>
            <ta e="T900" id="Seg_7709" s="T899">măn</ta>
            <ta e="T901" id="Seg_7710" s="T900">možet</ta>
            <ta e="T902" id="Seg_7711" s="T901">kü-laːm-lV-m</ta>
            <ta e="T904" id="Seg_7712" s="T903">a</ta>
            <ta e="T905" id="Seg_7713" s="T904">tăn</ta>
            <ta e="T906" id="Seg_7714" s="T905">dĭgəttə</ta>
            <ta e="T907" id="Seg_7715" s="T906">šo-lV-l</ta>
            <ta e="T909" id="Seg_7716" s="T908">dĭgəttə</ta>
            <ta e="T910" id="Seg_7717" s="T909">oʔbdə-bi-m</ta>
            <ta e="T911" id="Seg_7718" s="T910">da</ta>
            <ta e="T912" id="Seg_7719" s="T911">döbər</ta>
            <ta e="T913" id="Seg_7720" s="T912">šo-bi-m</ta>
            <ta e="T917" id="Seg_7721" s="T916">măn</ta>
            <ta e="T918" id="Seg_7722" s="T917">koʔbdo-m</ta>
            <ta e="T919" id="Seg_7723" s="T918">tibi-Tə</ta>
            <ta e="T920" id="Seg_7724" s="T919">kan-bi</ta>
            <ta e="T922" id="Seg_7725" s="T921">i-bi-m</ta>
            <ta e="T923" id="Seg_7726" s="T922">Krasnojarskə-ə-Kən</ta>
            <ta e="T924" id="Seg_7727" s="T923">dĭ</ta>
            <ta e="T926" id="Seg_7728" s="T924">măn-ntə</ta>
            <ta e="T927" id="Seg_7729" s="T926">măna</ta>
            <ta e="T928" id="Seg_7730" s="T927">onʼiʔ</ta>
            <ta e="T929" id="Seg_7731" s="T928">nʼi</ta>
            <ta e="T930" id="Seg_7732" s="T929">i-zittə</ta>
            <ta e="T931" id="Seg_7733" s="T930">aba-Tə</ta>
            <ta e="T932" id="Seg_7734" s="T931">e-ʔ</ta>
            <ta e="T933" id="Seg_7735" s="T932">nörbə-t</ta>
            <ta e="T935" id="Seg_7736" s="T934">a</ta>
            <ta e="T936" id="Seg_7737" s="T935">măn</ta>
            <ta e="T938" id="Seg_7738" s="T936">măn-bi-m</ta>
            <ta e="T939" id="Seg_7739" s="T938">kak</ta>
            <ta e="T940" id="Seg_7740" s="T939">ej</ta>
            <ta e="T941" id="Seg_7741" s="T940">nörbə-zittə</ta>
            <ta e="T942" id="Seg_7742" s="T941">aba-t</ta>
            <ta e="T943" id="Seg_7743" s="T942">bos-də</ta>
            <ta e="T944" id="Seg_7744" s="T943">i-bi</ta>
            <ta e="T945" id="Seg_7745" s="T944">bos-gəndə</ta>
            <ta e="T946" id="Seg_7746" s="T945">i</ta>
            <ta e="T948" id="Seg_7747" s="T947">i</ta>
            <ta e="T949" id="Seg_7748" s="T948">šiʔ</ta>
            <ta e="T950" id="Seg_7749" s="T949">det-bi</ta>
            <ta e="T952" id="Seg_7750" s="T951">dĭgəttə</ta>
            <ta e="T953" id="Seg_7751" s="T952">šo-bi-m</ta>
            <ta e="T954" id="Seg_7752" s="T953">aba-Tə</ta>
            <ta e="T955" id="Seg_7753" s="T954">nörbə-bi-m</ta>
            <ta e="T956" id="Seg_7754" s="T955">dĭ</ta>
            <ta e="T957" id="Seg_7755" s="T956">pʼaŋdə-bi</ta>
            <ta e="T958" id="Seg_7756" s="T957">dĭ-Tə</ta>
            <ta e="T959" id="Seg_7757" s="T958">sazən</ta>
            <ta e="T961" id="Seg_7758" s="T960">dĭgəttə</ta>
            <ta e="T962" id="Seg_7759" s="T961">dĭ</ta>
            <ta e="T963" id="Seg_7760" s="T962">dĭ-Tə</ta>
            <ta e="T964" id="Seg_7761" s="T963">sazən</ta>
            <ta e="T965" id="Seg_7762" s="T964">pʼaŋdə-bi</ta>
            <ta e="T966" id="Seg_7763" s="T965">dĭ</ta>
            <ta e="T967" id="Seg_7764" s="T966">döbər</ta>
            <ta e="T968" id="Seg_7765" s="T967">šo-bi</ta>
            <ta e="T970" id="Seg_7766" s="T969">dĭgəttə</ta>
            <ta e="T971" id="Seg_7767" s="T970">uja</ta>
            <ta e="T972" id="Seg_7768" s="T971">šoška</ta>
            <ta e="T973" id="Seg_7769" s="T972">dʼagar-bi-bAʔ</ta>
            <ta e="T974" id="Seg_7770" s="T973">uja</ta>
            <ta e="T975" id="Seg_7771" s="T974">iʔgö</ta>
            <ta e="T976" id="Seg_7772" s="T975">ipek</ta>
            <ta e="T977" id="Seg_7773" s="T976">pür-bi-bAʔ</ta>
            <ta e="T978" id="Seg_7774" s="T977">i</ta>
            <ta e="T979" id="Seg_7775" s="T978">il</ta>
            <ta e="T980" id="Seg_7776" s="T979">oʔbdə-bi-bAʔ</ta>
            <ta e="T982" id="Seg_7777" s="T981">dĭgəttə</ta>
            <ta e="T983" id="Seg_7778" s="T982">ara</ta>
            <ta e="T984" id="Seg_7779" s="T983">i-bi</ta>
            <ta e="T986" id="Seg_7780" s="T985">pʼelʼmʼenʼ-ə-jəʔ</ta>
            <ta e="T987" id="Seg_7781" s="T986">a-bi-bAʔ</ta>
            <ta e="T988" id="Seg_7782" s="T987">kătleta-jəʔ</ta>
            <ta e="T989" id="Seg_7783" s="T988">iʔgö</ta>
            <ta e="T990" id="Seg_7784" s="T989">i-bi</ta>
            <ta e="T991" id="Seg_7785" s="T990">ĭmbi</ta>
            <ta e="T992" id="Seg_7786" s="T991">i-bi</ta>
            <ta e="T994" id="Seg_7787" s="T993">i</ta>
            <ta e="T995" id="Seg_7788" s="T994">il</ta>
            <ta e="T996" id="Seg_7789" s="T995">iʔgö</ta>
            <ta e="T997" id="Seg_7790" s="T996">i-bi-jəʔ</ta>
            <ta e="T1525" id="Seg_7791" s="T998">Kazan</ta>
            <ta e="T999" id="Seg_7792" s="T1525">tura-Kən</ta>
            <ta e="T1000" id="Seg_7793" s="T999">šo-bi-jəʔ</ta>
            <ta e="T1002" id="Seg_7794" s="T1001">dĭgəttə</ta>
            <ta e="T1003" id="Seg_7795" s="T1002">măn</ta>
            <ta e="T1004" id="Seg_7796" s="T1003">dĭ-Tə</ta>
            <ta e="T1005" id="Seg_7797" s="T1004">i-bi-m</ta>
            <ta e="T1006" id="Seg_7798" s="T1005">palʼto</ta>
            <ta e="T1008" id="Seg_7799" s="T1007">dĭgəttə</ta>
            <ta e="T1009" id="Seg_7800" s="T1008">aktʼa</ta>
            <ta e="T1010" id="Seg_7801" s="T1009">dĭ-Tə</ta>
            <ta e="T1012" id="Seg_7802" s="T1011">mĭ-bi-m</ta>
            <ta e="T1014" id="Seg_7803" s="T1013">dĭ</ta>
            <ta e="T1522" id="Seg_7804" s="T1014">kan-lAʔ</ta>
            <ta e="T1015" id="Seg_7805" s="T1522">tʼür-bi</ta>
            <ta e="T1016" id="Seg_7806" s="T1015">dĭ</ta>
            <ta e="T1017" id="Seg_7807" s="T1016">nʼi-ziʔ</ta>
            <ta e="T1019" id="Seg_7808" s="T1018">dĭbər</ta>
            <ta e="T1020" id="Seg_7809" s="T1019">gijen</ta>
            <ta e="T1022" id="Seg_7810" s="T1021">il</ta>
            <ta e="T1023" id="Seg_7811" s="T1022">gijen</ta>
            <ta e="T1024" id="Seg_7812" s="T1023">il</ta>
            <ta e="T1025" id="Seg_7813" s="T1024">tʼăbaktər</ta>
            <ta e="T1026" id="Seg_7814" s="T1025">tʼăbaktər-liA-jəʔ</ta>
            <ta e="T1027" id="Seg_7815" s="T1026">bos-də</ta>
            <ta e="T1028" id="Seg_7816" s="T1027">šĭkə-t-ziʔ</ta>
            <ta e="T1030" id="Seg_7817" s="T1029">dĭgəttə</ta>
            <ta e="T1031" id="Seg_7818" s="T1030">dĭ</ta>
            <ta e="T1032" id="Seg_7819" s="T1031">dĭ</ta>
            <ta e="T1033" id="Seg_7820" s="T1032">nʼi-n</ta>
            <ta e="T1034" id="Seg_7821" s="T1033">ija</ta>
            <ta e="T1036" id="Seg_7822" s="T1035">dĭ-m</ta>
            <ta e="T1037" id="Seg_7823" s="T1036">dĭ-m</ta>
            <ta e="T1038" id="Seg_7824" s="T1037">ej</ta>
            <ta e="T1039" id="Seg_7825" s="T1038">ajir-bi</ta>
            <ta e="T1040" id="Seg_7826" s="T1039">dĭgəttə</ta>
            <ta e="T1041" id="Seg_7827" s="T1040">dĭ</ta>
            <ta e="T1042" id="Seg_7828" s="T1041">döbər</ta>
            <ta e="T1044" id="Seg_7829" s="T1042">pʼaŋdə-laʔbə</ta>
            <ta e="T1046" id="Seg_7830" s="T1045">kažən</ta>
            <ta e="T1048" id="Seg_7831" s="T1047">tʼala</ta>
            <ta e="T1049" id="Seg_7832" s="T1048">tʼor-laʔbə-m</ta>
            <ta e="T1051" id="Seg_7833" s="T1050">miʔ</ta>
            <ta e="T1053" id="Seg_7834" s="T1052">dĭ-Tə</ta>
            <ta e="T1055" id="Seg_7835" s="T1053">pʼaŋdə-zittə</ta>
            <ta e="T1056" id="Seg_7836" s="T1055">šo-ʔ</ta>
            <ta e="T1057" id="Seg_7837" s="T1056">maʔ-Tə-l</ta>
            <ta e="T1059" id="Seg_7838" s="T1058">dĭgəttə</ta>
            <ta e="T1060" id="Seg_7839" s="T1059">dĭ</ta>
            <ta e="T1061" id="Seg_7840" s="T1060">măn-ntə</ta>
            <ta e="T1062" id="Seg_7841" s="T1061">măn</ta>
            <ta e="T1063" id="Seg_7842" s="T1062">unʼə</ta>
            <ta e="T1064" id="Seg_7843" s="T1063">šo-lV-m</ta>
            <ta e="T1066" id="Seg_7844" s="T1065">a</ta>
            <ta e="T1067" id="Seg_7845" s="T1066">ĭmbi</ta>
            <ta e="T1068" id="Seg_7846" s="T1067">unʼə</ta>
            <ta e="T1069" id="Seg_7847" s="T1068">šo-lV-l</ta>
            <ta e="T1071" id="Seg_7848" s="T1070">a</ta>
            <ta e="T1072" id="Seg_7849" s="T1071">dĭ</ta>
            <ta e="T1073" id="Seg_7850" s="T1072">bazoʔ</ta>
            <ta e="T1074" id="Seg_7851" s="T1073">pʼaŋdə-laʔbə</ta>
            <ta e="T1075" id="Seg_7852" s="T1074">aktʼa</ta>
            <ta e="T1076" id="Seg_7853" s="T1075">naga</ta>
            <ta e="T1078" id="Seg_7854" s="T1077">măn</ta>
            <ta e="T1079" id="Seg_7855" s="T1078">bazoʔ</ta>
            <ta e="T1080" id="Seg_7856" s="T1079">öʔlu-bi-m</ta>
            <ta e="T1081" id="Seg_7857" s="T1080">šide</ta>
            <ta e="T1082" id="Seg_7858" s="T1081">biəʔ</ta>
            <ta e="T1083" id="Seg_7859" s="T1082">šide</ta>
            <ta e="T1084" id="Seg_7860" s="T1083">biəʔ</ta>
            <ta e="T1085" id="Seg_7861" s="T1084">aktʼa</ta>
            <ta e="T1087" id="Seg_7862" s="T1086">dĭgəttə</ta>
            <ta e="T1088" id="Seg_7863" s="T1087">aba-t</ta>
            <ta e="T1089" id="Seg_7864" s="T1088">sumna</ta>
            <ta e="T1090" id="Seg_7865" s="T1089">šide</ta>
            <ta e="T1091" id="Seg_7866" s="T1090">biəʔ</ta>
            <ta e="T1092" id="Seg_7867" s="T1091">öʔlu-bi</ta>
            <ta e="T1094" id="Seg_7868" s="T1093">dĭgəttə</ta>
            <ta e="T1095" id="Seg_7869" s="T1094">dö</ta>
            <ta e="T1096" id="Seg_7870" s="T1095">šo-bi</ta>
            <ta e="T1097" id="Seg_7871" s="T1096">onʼiʔ</ta>
            <ta e="T1099" id="Seg_7872" s="T1098">dĭ</ta>
            <ta e="T1100" id="Seg_7873" s="T1099">ma-luʔbdə-bi</ta>
            <ta e="T1102" id="Seg_7874" s="T1101">dĭgəttə</ta>
            <ta e="T1103" id="Seg_7875" s="T1102">dĭ</ta>
            <ta e="T1104" id="Seg_7876" s="T1103">dĭ-Tə</ta>
            <ta e="T1105" id="Seg_7877" s="T1104">dĭ</ta>
            <ta e="T1106" id="Seg_7878" s="T1105">pʼaŋdə-bi</ta>
            <ta e="T1107" id="Seg_7879" s="T1106">pʼaŋdə-bi</ta>
            <ta e="T1109" id="Seg_7880" s="T1108">dĭ</ta>
            <ta e="T1110" id="Seg_7881" s="T1109">ej</ta>
            <ta e="T1111" id="Seg_7882" s="T1110">üge</ta>
            <ta e="T1112" id="Seg_7883" s="T1111">ej</ta>
            <ta e="T1113" id="Seg_7884" s="T1112">šo-liA</ta>
            <ta e="T1115" id="Seg_7885" s="T1114">dĭgəttə</ta>
            <ta e="T1116" id="Seg_7886" s="T1115">dĭ</ta>
            <ta e="T1523" id="Seg_7887" s="T1116">kan-lAʔ</ta>
            <ta e="T1117" id="Seg_7888" s="T1523">tʼür-bi</ta>
            <ta e="T1120" id="Seg_7889" s="T1119">gijen</ta>
            <ta e="T1121" id="Seg_7890" s="T1120">Lenin</ta>
            <ta e="T1122" id="Seg_7891" s="T1121">amno-bi</ta>
            <ta e="T1123" id="Seg_7892" s="T1122">dĭ</ta>
            <ta e="T1124" id="Seg_7893" s="T1123">tura-Tə</ta>
            <ta e="T1126" id="Seg_7894" s="T1125">dĭn</ta>
            <ta e="T1127" id="Seg_7895" s="T1126">stălovăjă-n</ta>
            <ta e="T1130" id="Seg_7896" s="T1129">stălovăjă-n</ta>
            <ta e="T1132" id="Seg_7897" s="T1131">ipek</ta>
            <ta e="T1133" id="Seg_7898" s="T1132">mĭ-liA</ta>
            <ta e="T1134" id="Seg_7899" s="T1133">ara</ta>
            <ta e="T1135" id="Seg_7900" s="T1134">mĭ-liA</ta>
            <ta e="T1136" id="Seg_7901" s="T1135">bar</ta>
            <ta e="T1137" id="Seg_7902" s="T1136">ĭmbi</ta>
            <ta e="T1138" id="Seg_7903" s="T1137">mĭ-liA</ta>
            <ta e="T1139" id="Seg_7904" s="T1138">pʼaŋdə-laʔbə</ta>
            <ta e="T1141" id="Seg_7905" s="T1140">biəʔ</ta>
            <ta e="T1143" id="Seg_7906" s="T1142">biəʔ</ta>
            <ta e="T1144" id="Seg_7907" s="T1143">nagur</ta>
            <ta e="T1145" id="Seg_7908" s="T1144">i</ta>
            <ta e="T1146" id="Seg_7909" s="T1145">sumna</ta>
            <ta e="T1147" id="Seg_7910" s="T1146">aktʼa</ta>
            <ta e="T1148" id="Seg_7911" s="T1147">i-liA-t</ta>
            <ta e="T1392" id="Seg_7912" s="T1391">măn</ta>
            <ta e="T1393" id="Seg_7913" s="T1392">plemʼannica-n</ta>
            <ta e="T1394" id="Seg_7914" s="T1393">koʔbdo-t</ta>
            <ta e="T1395" id="Seg_7915" s="T1394">tibi-Tə</ta>
            <ta e="T1524" id="Seg_7916" s="T1395">kan-lAʔ</ta>
            <ta e="T1396" id="Seg_7917" s="T1524">tʼür-bi</ta>
            <ta e="T1398" id="Seg_7918" s="T1397">i</ta>
            <ta e="T1399" id="Seg_7919" s="T1398">dĭn</ta>
            <ta e="T1400" id="Seg_7920" s="T1399">sumna</ta>
            <ta e="T1401" id="Seg_7921" s="T1400">ešši-zAŋ</ta>
            <ta e="T1402" id="Seg_7922" s="T1401">i-bi-jəʔ</ta>
            <ta e="T1404" id="Seg_7923" s="T1403">dĭgəttə</ta>
            <ta e="T1405" id="Seg_7924" s="T1404">dĭ</ta>
            <ta e="T1406" id="Seg_7925" s="T1405">dĭ-zAŋ</ta>
            <ta e="T1407" id="Seg_7926" s="T1406">kan-bi-jəʔ</ta>
            <ta e="T1408" id="Seg_7927" s="T1407">maʔ</ta>
            <ta e="T1409" id="Seg_7928" s="T1408">maʔ-Tə</ta>
            <ta e="T1410" id="Seg_7929" s="T1409">dĭn</ta>
            <ta e="T1412" id="Seg_7930" s="T1411">i</ta>
            <ta e="T1413" id="Seg_7931" s="T1412">i-gA</ta>
            <ta e="T1414" id="Seg_7932" s="T1413">turaj-jəʔ</ta>
            <ta e="T1416" id="Seg_7933" s="T1415">dĭgəttə</ta>
            <ta e="T1417" id="Seg_7934" s="T1416">dĭ-zAŋ</ta>
            <ta e="T1418" id="Seg_7935" s="T1417">aʔtʼi</ta>
            <ta e="T1419" id="Seg_7936" s="T1418">a-bi-jəʔ</ta>
            <ta e="T1420" id="Seg_7937" s="T1419">gijen</ta>
            <ta e="T1421" id="Seg_7938" s="T1420">baza-j</ta>
            <ta e="T1422" id="Seg_7939" s="T1421">aʔtʼi</ta>
            <ta e="T1424" id="Seg_7940" s="T1423">dĭgəttə</ta>
            <ta e="T1425" id="Seg_7941" s="T1424">kan-bi-jəʔ</ta>
            <ta e="T1426" id="Seg_7942" s="T1425">dĭ</ta>
            <ta e="T1427" id="Seg_7943" s="T1426">dĭbər</ta>
            <ta e="T1428" id="Seg_7944" s="T1427">sʼevʼer-Tə</ta>
            <ta e="T1429" id="Seg_7945" s="T1428">dĭn</ta>
            <ta e="T1430" id="Seg_7946" s="T1429">tože</ta>
            <ta e="T1431" id="Seg_7947" s="T1430">a-bi-jəʔ</ta>
            <ta e="T1432" id="Seg_7948" s="T1431">aʔtʼi</ta>
            <ta e="T1433" id="Seg_7949" s="T1432">aʔtʼi</ta>
            <ta e="T1435" id="Seg_7950" s="T1434">baza-j</ta>
            <ta e="T1436" id="Seg_7951" s="T1435">aʔtʼi</ta>
            <ta e="T1438" id="Seg_7952" s="T1437">dĭgəttə</ta>
            <ta e="T1439" id="Seg_7953" s="T1438">dĭ</ta>
            <ta e="T1440" id="Seg_7954" s="T1439">šo-bi</ta>
            <ta e="T1441" id="Seg_7955" s="T1440">maʔ-gəndə</ta>
            <ta e="T1442" id="Seg_7956" s="T1441">ija-Tə</ta>
            <ta e="T1444" id="Seg_7957" s="T1443">dĭ</ta>
            <ta e="T1445" id="Seg_7958" s="T1444">dĭgəttə</ta>
            <ta e="T1446" id="Seg_7959" s="T1445">dĭ</ta>
            <ta e="T1447" id="Seg_7960" s="T1446">dön</ta>
            <ta e="T1448" id="Seg_7961" s="T1447">i-bi</ta>
            <ta e="T1450" id="Seg_7962" s="T1449">dĭgəttə</ta>
            <ta e="T1451" id="Seg_7963" s="T1450">sazən</ta>
            <ta e="T1453" id="Seg_7964" s="T1451">pʼaŋdə-bi-jəʔ</ta>
            <ta e="T1454" id="Seg_7965" s="T1453">tăn</ta>
            <ta e="T1455" id="Seg_7966" s="T1454">tibi</ta>
            <ta e="T1456" id="Seg_7967" s="T1455">bü-Kən</ta>
            <ta e="T1457" id="Seg_7968" s="T1456">kü-laːm-bi</ta>
            <ta e="T1459" id="Seg_7969" s="T1458">dĭgəttə</ta>
            <ta e="T1460" id="Seg_7970" s="T1459">dĭ</ta>
            <ta e="T1461" id="Seg_7971" s="T1460">kan-bi</ta>
            <ta e="T1463" id="Seg_7972" s="T1462">dĭ-m</ta>
            <ta e="T1464" id="Seg_7973" s="T1463">tʼo-Tə</ta>
            <ta e="T1465" id="Seg_7974" s="T1464">hen-bi-jəʔ</ta>
            <ta e="T1467" id="Seg_7975" s="T1466">dĭgəttə</ta>
            <ta e="T1468" id="Seg_7976" s="T1467">bazoʔ</ta>
            <ta e="T1469" id="Seg_7977" s="T1468">dĭ-Tə</ta>
            <ta e="T1470" id="Seg_7978" s="T1469">kuza</ta>
            <ta e="T1471" id="Seg_7979" s="T1470">i-bi</ta>
            <ta e="T1473" id="Seg_7980" s="T1472">dĭn</ta>
            <ta e="T1474" id="Seg_7981" s="T1473">naga-bi</ta>
            <ta e="T1475" id="Seg_7982" s="T1474">ne-t</ta>
            <ta e="T1477" id="Seg_7983" s="T1476">i</ta>
            <ta e="T1478" id="Seg_7984" s="T1477">tüj</ta>
            <ta e="T1479" id="Seg_7985" s="T1478">amno-laʔbə-jəʔ</ta>
            <ta e="T1480" id="Seg_7986" s="T1479">i</ta>
            <ta e="T1481" id="Seg_7987" s="T1480">dĭn</ta>
            <ta e="T1482" id="Seg_7988" s="T1481">sumna</ta>
            <ta e="T1483" id="Seg_7989" s="T1482">ešši-zAŋ-Tə</ta>
            <ta e="T1484" id="Seg_7990" s="T1483">i-bi-jəʔ</ta>
            <ta e="T1486" id="Seg_7991" s="T1485">dĭ</ta>
            <ta e="T1487" id="Seg_7992" s="T1486">i-bi</ta>
            <ta e="T1488" id="Seg_7993" s="T1487">dĭ-m</ta>
            <ta e="T1490" id="Seg_7994" s="T1489">ugaːndə</ta>
            <ta e="T1491" id="Seg_7995" s="T1490">jakšə</ta>
            <ta e="T1492" id="Seg_7996" s="T1491">kuza</ta>
            <ta e="T1494" id="Seg_7997" s="T1493">ara</ta>
            <ta e="T1495" id="Seg_7998" s="T1494">ej</ta>
            <ta e="T1496" id="Seg_7999" s="T1495">bĭs-liA</ta>
            <ta e="T1497" id="Seg_8000" s="T1496">gibər=də</ta>
            <ta e="T1498" id="Seg_8001" s="T1497">ej</ta>
            <ta e="T1499" id="Seg_8002" s="T1498">mĭn-liA</ta>
            <ta e="T1500" id="Seg_8003" s="T1499">üge</ta>
            <ta e="T1501" id="Seg_8004" s="T1500">togonər-laʔbə</ta>
            <ta e="T1502" id="Seg_8005" s="T1501">dĭn</ta>
            <ta e="T1503" id="Seg_8006" s="T1502">tože</ta>
            <ta e="T1504" id="Seg_8007" s="T1503">ija-t</ta>
            <ta e="T1505" id="Seg_8008" s="T1504">naga</ta>
            <ta e="T1506" id="Seg_8009" s="T1505">tolʼko</ta>
            <ta e="T1507" id="Seg_8010" s="T1506">onʼiʔ</ta>
            <ta e="T1508" id="Seg_8011" s="T1507">aba-t</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T129" id="Seg_8012" s="T128">when</ta>
            <ta e="T130" id="Seg_8013" s="T129">I.NOM</ta>
            <ta e="T131" id="Seg_8014" s="T130">sit-PST-1SG</ta>
            <ta e="T132" id="Seg_8015" s="T131">Tartu.[NOM.SG]</ta>
            <ta e="T133" id="Seg_8016" s="T132">go-PST-1SG</ta>
            <ta e="T134" id="Seg_8017" s="T133">take-PST-1SG</ta>
            <ta e="T136" id="Seg_8018" s="T135">three.[NOM.SG]</ta>
            <ta e="T137" id="Seg_8019" s="T136">scarp-EP-PL</ta>
            <ta e="T139" id="Seg_8020" s="T138">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T140" id="Seg_8021" s="T139">people-NOM/GEN/ACC.3SG</ta>
            <ta e="T141" id="Seg_8022" s="T140">give-INF.LAT</ta>
            <ta e="T143" id="Seg_8023" s="T142">then</ta>
            <ta e="T144" id="Seg_8024" s="T143">here</ta>
            <ta e="T145" id="Seg_8025" s="T144">come-PST-1SG</ta>
            <ta e="T146" id="Seg_8026" s="T145">take-PST-1SG</ta>
            <ta e="T147" id="Seg_8027" s="T146">pants-PL</ta>
            <ta e="T149" id="Seg_8028" s="T148">shirt.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8029" s="T149">take-PST-1SG</ta>
            <ta e="T152" id="Seg_8030" s="T151">boy-LAT</ta>
            <ta e="T153" id="Seg_8031" s="T152">nephew-EP-NOM/GEN/ACC.1SG-LAT</ta>
            <ta e="T155" id="Seg_8032" s="T154">daughter-LAT</ta>
            <ta e="T156" id="Seg_8033" s="T155">take-PST-1SG</ta>
            <ta e="T158" id="Seg_8034" s="T157">dress.[NOM.SG]</ta>
            <ta e="T159" id="Seg_8035" s="T158">take-PST-1SG</ta>
            <ta e="T161" id="Seg_8036" s="T160">then</ta>
            <ta e="T162" id="Seg_8037" s="T161">nephew-LAT</ta>
            <ta e="T163" id="Seg_8038" s="T162">take-PST-1SG</ta>
            <ta e="T164" id="Seg_8039" s="T163">shirt.[NOM.SG]</ta>
            <ta e="T166" id="Seg_8040" s="T165">enough</ta>
            <ta e="T230" id="Seg_8041" s="T228">pants-PL</ta>
            <ta e="T234" id="Seg_8042" s="T233">pants.[NOM.SG]</ta>
            <ta e="T249" id="Seg_8043" s="T247">pants.[NOM.SG]</ta>
            <ta e="T251" id="Seg_8044" s="T250">boot-PL</ta>
            <ta e="T252" id="Seg_8045" s="T251">pants.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8046" s="T252">boot.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8047" s="T268">boot.[NOM.SG]</ta>
            <ta e="T274" id="Seg_8048" s="T273">pants-PL</ta>
            <ta e="T278" id="Seg_8049" s="T277">shirt.[NOM.SG]</ta>
            <ta e="T348" id="Seg_8050" s="T347">shirt.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8051" s="T352">cap.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8052" s="T503">when</ta>
            <ta e="T505" id="Seg_8053" s="T504">this.[NOM.SG]</ta>
            <ta e="T506" id="Seg_8054" s="T505">come-PST.[3SG]</ta>
            <ta e="T507" id="Seg_8055" s="T506">I.LAT</ta>
            <ta e="T510" id="Seg_8056" s="T509">say-IPFVZ.[3SG]</ta>
            <ta e="T511" id="Seg_8057" s="T510">grandmother.[NOM.SG]</ta>
            <ta e="T512" id="Seg_8058" s="T511">I.NOM</ta>
            <ta e="T513" id="Seg_8059" s="T512">you.DAT</ta>
            <ta e="T514" id="Seg_8060" s="T513">come-PRS-1SG</ta>
            <ta e="T515" id="Seg_8061" s="T514">and</ta>
            <ta e="T516" id="Seg_8062" s="T515">I.NOM</ta>
            <ta e="T517" id="Seg_8063" s="T516">say-IPFVZ-1SG</ta>
            <ta e="T518" id="Seg_8064" s="T517">NEG.AUX-IMP.2SG</ta>
            <ta e="T519" id="Seg_8065" s="T518">come-CNG</ta>
            <ta e="T521" id="Seg_8066" s="T520">come-IMP.2SG</ta>
            <ta e="T522" id="Seg_8067" s="T521">I.LAT</ta>
            <ta e="T524" id="Seg_8068" s="T523">go-EP-IMP.2SG</ta>
            <ta e="T525" id="Seg_8069" s="T524">hence</ta>
            <ta e="T527" id="Seg_8070" s="T526">you.NOM</ta>
            <ta e="T528" id="Seg_8071" s="T527">I.LAT</ta>
            <ta e="T531" id="Seg_8072" s="T530">you.NOM</ta>
            <ta e="T532" id="Seg_8073" s="T531">I.LAT</ta>
            <ta e="T534" id="Seg_8074" s="T533">I.NOM</ta>
            <ta e="T535" id="Seg_8075" s="T534">you.NOM-COM</ta>
            <ta e="T536" id="Seg_8076" s="T535">NEG</ta>
            <ta e="T537" id="Seg_8077" s="T536">speak-FUT-1SG</ta>
            <ta e="T539" id="Seg_8078" s="T538">go-EP-IMP.2SG</ta>
            <ta e="T540" id="Seg_8079" s="T539">hence</ta>
            <ta e="T556" id="Seg_8080" s="T555">when</ta>
            <ta e="T557" id="Seg_8081" s="T556">this.[NOM.SG]</ta>
            <ta e="T558" id="Seg_8082" s="T557">I.LAT</ta>
            <ta e="T559" id="Seg_8083" s="T558">come-PST.[3SG]</ta>
            <ta e="T560" id="Seg_8084" s="T559">come-PST.[3SG]</ta>
            <ta e="T571" id="Seg_8085" s="T568">when</ta>
            <ta e="T572" id="Seg_8086" s="T571">this.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8087" s="T572">I.LAT</ta>
            <ta e="T574" id="Seg_8088" s="T573">come-PST.[3SG]</ta>
            <ta e="T575" id="Seg_8089" s="T574">I.NOM</ta>
            <ta e="T576" id="Seg_8090" s="T575">scold-PST-1SG</ta>
            <ta e="T578" id="Seg_8091" s="T576">this-ACC</ta>
            <ta e="T579" id="Seg_8092" s="T578">NEG.AUX-IMP.2SG</ta>
            <ta e="T580" id="Seg_8093" s="T579">come-CNG</ta>
            <ta e="T581" id="Seg_8094" s="T580">I.LAT</ta>
            <ta e="T582" id="Seg_8095" s="T581">I.NOM</ta>
            <ta e="T583" id="Seg_8096" s="T582">you.DAT</ta>
            <ta e="T584" id="Seg_8097" s="T583">what.[NOM.SG]=INDEF</ta>
            <ta e="T585" id="Seg_8098" s="T584">NEG</ta>
            <ta e="T586" id="Seg_8099" s="T585">tell-FUT-1SG</ta>
            <ta e="T588" id="Seg_8100" s="T587">go-EP-IMP.2SG</ta>
            <ta e="T589" id="Seg_8101" s="T588">hence</ta>
            <ta e="T591" id="Seg_8102" s="T590">then</ta>
            <ta e="T592" id="Seg_8103" s="T591">this.[NOM.SG]</ta>
            <ta e="T593" id="Seg_8104" s="T592">say-IPFVZ.[3SG]</ta>
            <ta e="T594" id="Seg_8105" s="T593">very</ta>
            <ta e="T595" id="Seg_8106" s="T594">good</ta>
            <ta e="T596" id="Seg_8107" s="T595">speak-PRS-2SG</ta>
            <ta e="T598" id="Seg_8108" s="T597">I.NOM</ta>
            <ta e="T599" id="Seg_8109" s="T598">say-IPFVZ-1SG</ta>
            <ta e="T601" id="Seg_8110" s="T600">good</ta>
            <ta e="T602" id="Seg_8111" s="T601">scold-PRS-1SG</ta>
            <ta e="T603" id="Seg_8112" s="T602">you.DAT</ta>
            <ta e="T605" id="Seg_8113" s="T604">and</ta>
            <ta e="T606" id="Seg_8114" s="T605">you.NOM</ta>
            <ta e="T607" id="Seg_8115" s="T606">say-PRS-2SG</ta>
            <ta e="T608" id="Seg_8116" s="T607">good</ta>
            <ta e="T861" id="Seg_8117" s="T860">when</ta>
            <ta e="T862" id="Seg_8118" s="T861">when</ta>
            <ta e="T863" id="Seg_8119" s="T862">come-PST.[3SG]</ta>
            <ta e="T866" id="Seg_8120" s="T865">I.LAT</ta>
            <ta e="T868" id="Seg_8121" s="T867">and</ta>
            <ta e="T869" id="Seg_8122" s="T868">this.[NOM.SG]</ta>
            <ta e="T870" id="Seg_8123" s="T869">write-PST.[3SG]</ta>
            <ta e="T871" id="Seg_8124" s="T870">I.LAT</ta>
            <ta e="T872" id="Seg_8125" s="T871">come-FUT-1SG</ta>
            <ta e="T873" id="Seg_8126" s="T872">you.DAT</ta>
            <ta e="T875" id="Seg_8127" s="T874">I.NOM</ta>
            <ta e="T876" id="Seg_8128" s="T875">wait-PST-1SG</ta>
            <ta e="T877" id="Seg_8129" s="T876">wait-PST-1SG</ta>
            <ta e="T878" id="Seg_8130" s="T877">NEG.EX.[3SG]</ta>
            <ta e="T880" id="Seg_8131" s="T879">then</ta>
            <ta e="T881" id="Seg_8132" s="T880">press-DUR.[3SG]</ta>
            <ta e="T882" id="Seg_8133" s="T881">I.LAT</ta>
            <ta e="T883" id="Seg_8134" s="T882">paper.[NOM.SG]</ta>
            <ta e="T884" id="Seg_8135" s="T883">I.NOM</ta>
            <ta e="T885" id="Seg_8136" s="T884">NEG</ta>
            <ta e="T886" id="Seg_8137" s="T885">come-FUT-1SG</ta>
            <ta e="T888" id="Seg_8138" s="T887">that.[NOM.SG]</ta>
            <ta e="T890" id="Seg_8139" s="T889">this.[NOM.SG]</ta>
            <ta e="T891" id="Seg_8140" s="T890">winter-LOC</ta>
            <ta e="T892" id="Seg_8141" s="T891">come</ta>
            <ta e="T893" id="Seg_8142" s="T892">come-FUT-1SG</ta>
            <ta e="T895" id="Seg_8143" s="T894">you.DAT</ta>
            <ta e="T897" id="Seg_8144" s="T896">and</ta>
            <ta e="T898" id="Seg_8145" s="T897">I.NOM</ta>
            <ta e="T899" id="Seg_8146" s="T898">say-PST-1SG</ta>
            <ta e="T900" id="Seg_8147" s="T899">I.NOM</ta>
            <ta e="T901" id="Seg_8148" s="T900">maybe</ta>
            <ta e="T902" id="Seg_8149" s="T901">die-RES-FUT-1SG</ta>
            <ta e="T904" id="Seg_8150" s="T903">and</ta>
            <ta e="T905" id="Seg_8151" s="T904">you.NOM</ta>
            <ta e="T906" id="Seg_8152" s="T905">then</ta>
            <ta e="T907" id="Seg_8153" s="T906">come-FUT-2SG</ta>
            <ta e="T909" id="Seg_8154" s="T908">then</ta>
            <ta e="T910" id="Seg_8155" s="T909">collect-PST-1SG</ta>
            <ta e="T911" id="Seg_8156" s="T910">and</ta>
            <ta e="T912" id="Seg_8157" s="T911">here</ta>
            <ta e="T913" id="Seg_8158" s="T912">come-PST-1SG</ta>
            <ta e="T917" id="Seg_8159" s="T916">I.NOM</ta>
            <ta e="T918" id="Seg_8160" s="T917">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T919" id="Seg_8161" s="T918">man-LAT</ta>
            <ta e="T920" id="Seg_8162" s="T919">go-PST.[3SG]</ta>
            <ta e="T922" id="Seg_8163" s="T921">be-PST-1SG</ta>
            <ta e="T923" id="Seg_8164" s="T922">Krasnoyarsk-EP-LOC</ta>
            <ta e="T924" id="Seg_8165" s="T923">this.[NOM.SG]</ta>
            <ta e="T926" id="Seg_8166" s="T924">say-IPFVZ.[3SG]</ta>
            <ta e="T927" id="Seg_8167" s="T926">I.LAT</ta>
            <ta e="T928" id="Seg_8168" s="T927">one.[NOM.SG]</ta>
            <ta e="T929" id="Seg_8169" s="T928">boy.[NOM.SG]</ta>
            <ta e="T930" id="Seg_8170" s="T929">take-INF.LAT</ta>
            <ta e="T931" id="Seg_8171" s="T930">father-LAT</ta>
            <ta e="T932" id="Seg_8172" s="T931">NEG.AUX-IMP.2SG</ta>
            <ta e="T933" id="Seg_8173" s="T932">tell-IMP.2SG.O</ta>
            <ta e="T935" id="Seg_8174" s="T934">and</ta>
            <ta e="T936" id="Seg_8175" s="T935">I.NOM</ta>
            <ta e="T938" id="Seg_8176" s="T936">say-PST-1SG</ta>
            <ta e="T939" id="Seg_8177" s="T938">like</ta>
            <ta e="T940" id="Seg_8178" s="T939">NEG</ta>
            <ta e="T941" id="Seg_8179" s="T940">tell-INF.LAT</ta>
            <ta e="T942" id="Seg_8180" s="T941">father-NOM/GEN.3SG</ta>
            <ta e="T943" id="Seg_8181" s="T942">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T944" id="Seg_8182" s="T943">take-PST.[3SG]</ta>
            <ta e="T945" id="Seg_8183" s="T944">self-LAT/LOC.3SG</ta>
            <ta e="T946" id="Seg_8184" s="T945">and</ta>
            <ta e="T948" id="Seg_8185" s="T947">and</ta>
            <ta e="T949" id="Seg_8186" s="T948">you.PL.NOM</ta>
            <ta e="T950" id="Seg_8187" s="T949">bring-PST.[3SG]</ta>
            <ta e="T952" id="Seg_8188" s="T951">then</ta>
            <ta e="T953" id="Seg_8189" s="T952">come-PST-1SG</ta>
            <ta e="T954" id="Seg_8190" s="T953">father-LAT</ta>
            <ta e="T955" id="Seg_8191" s="T954">tell-PST-1SG</ta>
            <ta e="T956" id="Seg_8192" s="T955">this.[NOM.SG]</ta>
            <ta e="T957" id="Seg_8193" s="T956">write-PST.[3SG]</ta>
            <ta e="T958" id="Seg_8194" s="T957">this-LAT</ta>
            <ta e="T959" id="Seg_8195" s="T958">paper.[NOM.SG]</ta>
            <ta e="T961" id="Seg_8196" s="T960">then</ta>
            <ta e="T962" id="Seg_8197" s="T961">this.[NOM.SG]</ta>
            <ta e="T963" id="Seg_8198" s="T962">this-LAT</ta>
            <ta e="T964" id="Seg_8199" s="T963">paper.[NOM.SG]</ta>
            <ta e="T965" id="Seg_8200" s="T964">write-PST.[3SG]</ta>
            <ta e="T966" id="Seg_8201" s="T965">this.[NOM.SG]</ta>
            <ta e="T967" id="Seg_8202" s="T966">here</ta>
            <ta e="T968" id="Seg_8203" s="T967">come-PST.[3SG]</ta>
            <ta e="T970" id="Seg_8204" s="T969">then</ta>
            <ta e="T971" id="Seg_8205" s="T970">meat.[NOM.SG]</ta>
            <ta e="T972" id="Seg_8206" s="T971">pig.[NOM.SG]</ta>
            <ta e="T973" id="Seg_8207" s="T972">stab-PST-1PL</ta>
            <ta e="T974" id="Seg_8208" s="T973">meat.[NOM.SG]</ta>
            <ta e="T975" id="Seg_8209" s="T974">many</ta>
            <ta e="T976" id="Seg_8210" s="T975">bread.[NOM.SG]</ta>
            <ta e="T977" id="Seg_8211" s="T976">bake-PST-1PL</ta>
            <ta e="T978" id="Seg_8212" s="T977">and</ta>
            <ta e="T979" id="Seg_8213" s="T978">people.[NOM.SG]</ta>
            <ta e="T980" id="Seg_8214" s="T979">collect-PST-1PL</ta>
            <ta e="T982" id="Seg_8215" s="T981">then</ta>
            <ta e="T983" id="Seg_8216" s="T982">vodka.[NOM.SG]</ta>
            <ta e="T984" id="Seg_8217" s="T983">be-PST.[3SG]</ta>
            <ta e="T986" id="Seg_8218" s="T985">pelmen-EP-PL</ta>
            <ta e="T987" id="Seg_8219" s="T986">make-PST-1PL</ta>
            <ta e="T988" id="Seg_8220" s="T987">meatball-NOM/GEN/ACC.3PL</ta>
            <ta e="T989" id="Seg_8221" s="T988">many</ta>
            <ta e="T990" id="Seg_8222" s="T989">be-PST.[3SG]</ta>
            <ta e="T991" id="Seg_8223" s="T990">what.[NOM.SG]</ta>
            <ta e="T992" id="Seg_8224" s="T991">be-PST.[3SG]</ta>
            <ta e="T994" id="Seg_8225" s="T993">and</ta>
            <ta e="T995" id="Seg_8226" s="T994">people.[NOM.SG]</ta>
            <ta e="T996" id="Seg_8227" s="T995">many</ta>
            <ta e="T997" id="Seg_8228" s="T996">be-PST-3PL</ta>
            <ta e="T1525" id="Seg_8229" s="T998">Aginskoe</ta>
            <ta e="T999" id="Seg_8230" s="T1525">settlement-LOC</ta>
            <ta e="T1000" id="Seg_8231" s="T999">come-PST-3PL</ta>
            <ta e="T1002" id="Seg_8232" s="T1001">then</ta>
            <ta e="T1003" id="Seg_8233" s="T1002">I.NOM</ta>
            <ta e="T1004" id="Seg_8234" s="T1003">this-LAT</ta>
            <ta e="T1005" id="Seg_8235" s="T1004">take-PST-1SG</ta>
            <ta e="T1006" id="Seg_8236" s="T1005">coat.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_8237" s="T1007">then</ta>
            <ta e="T1009" id="Seg_8238" s="T1008">money.[NOM.SG]</ta>
            <ta e="T1010" id="Seg_8239" s="T1009">this-LAT</ta>
            <ta e="T1012" id="Seg_8240" s="T1011">give-PST-1SG</ta>
            <ta e="T1014" id="Seg_8241" s="T1013">this.[NOM.SG]</ta>
            <ta e="T1522" id="Seg_8242" s="T1014">go-CVB</ta>
            <ta e="T1015" id="Seg_8243" s="T1522">disappear-PST.[3SG]</ta>
            <ta e="T1016" id="Seg_8244" s="T1015">this.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_8245" s="T1016">son-COM</ta>
            <ta e="T1019" id="Seg_8246" s="T1018">there</ta>
            <ta e="T1020" id="Seg_8247" s="T1019">where</ta>
            <ta e="T1022" id="Seg_8248" s="T1021">people.[NOM.SG]</ta>
            <ta e="T1023" id="Seg_8249" s="T1022">where</ta>
            <ta e="T1024" id="Seg_8250" s="T1023">people.[NOM.SG]</ta>
            <ta e="T1025" id="Seg_8251" s="T1024">speak</ta>
            <ta e="T1026" id="Seg_8252" s="T1025">speak-PRS-3PL</ta>
            <ta e="T1027" id="Seg_8253" s="T1026">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1028" id="Seg_8254" s="T1027">language-3SG-INS</ta>
            <ta e="T1030" id="Seg_8255" s="T1029">then</ta>
            <ta e="T1031" id="Seg_8256" s="T1030">this.[NOM.SG]</ta>
            <ta e="T1032" id="Seg_8257" s="T1031">this.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_8258" s="T1032">boy-GEN</ta>
            <ta e="T1034" id="Seg_8259" s="T1033">mother.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_8260" s="T1035">this-ACC</ta>
            <ta e="T1037" id="Seg_8261" s="T1036">this-ACC</ta>
            <ta e="T1038" id="Seg_8262" s="T1037">NEG</ta>
            <ta e="T1039" id="Seg_8263" s="T1038">pity-PST.[3SG]</ta>
            <ta e="T1040" id="Seg_8264" s="T1039">then</ta>
            <ta e="T1041" id="Seg_8265" s="T1040">this.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_8266" s="T1041">here</ta>
            <ta e="T1044" id="Seg_8267" s="T1042">press-DUR.[3SG]</ta>
            <ta e="T1046" id="Seg_8268" s="T1045">each</ta>
            <ta e="T1048" id="Seg_8269" s="T1047">day.[NOM.SG]</ta>
            <ta e="T1049" id="Seg_8270" s="T1048">cry-DUR-1SG</ta>
            <ta e="T1051" id="Seg_8271" s="T1050">we.NOM</ta>
            <ta e="T1053" id="Seg_8272" s="T1052">this-LAT</ta>
            <ta e="T1055" id="Seg_8273" s="T1053">press-INF.LAT</ta>
            <ta e="T1056" id="Seg_8274" s="T1055">come-IMP.2SG</ta>
            <ta e="T1057" id="Seg_8275" s="T1056">house-LAT-2SG</ta>
            <ta e="T1059" id="Seg_8276" s="T1058">then</ta>
            <ta e="T1060" id="Seg_8277" s="T1059">this.[NOM.SG]</ta>
            <ta e="T1061" id="Seg_8278" s="T1060">say-IPFVZ.[3SG]</ta>
            <ta e="T1062" id="Seg_8279" s="T1061">I.NOM</ta>
            <ta e="T1063" id="Seg_8280" s="T1062">alone</ta>
            <ta e="T1064" id="Seg_8281" s="T1063">come-FUT-1SG</ta>
            <ta e="T1066" id="Seg_8282" s="T1065">and</ta>
            <ta e="T1067" id="Seg_8283" s="T1066">what.[NOM.SG]</ta>
            <ta e="T1068" id="Seg_8284" s="T1067">alone</ta>
            <ta e="T1069" id="Seg_8285" s="T1068">come-FUT-2SG</ta>
            <ta e="T1071" id="Seg_8286" s="T1070">and</ta>
            <ta e="T1072" id="Seg_8287" s="T1071">this.[NOM.SG]</ta>
            <ta e="T1073" id="Seg_8288" s="T1072">again</ta>
            <ta e="T1074" id="Seg_8289" s="T1073">press-DUR.[3SG]</ta>
            <ta e="T1075" id="Seg_8290" s="T1074">money.[NOM.SG]</ta>
            <ta e="T1076" id="Seg_8291" s="T1075">NEG.EX.[3SG]</ta>
            <ta e="T1078" id="Seg_8292" s="T1077">I.NOM</ta>
            <ta e="T1079" id="Seg_8293" s="T1078">again</ta>
            <ta e="T1080" id="Seg_8294" s="T1079">send-PST-1SG</ta>
            <ta e="T1081" id="Seg_8295" s="T1080">two.[NOM.SG]</ta>
            <ta e="T1082" id="Seg_8296" s="T1081">ten.[NOM.SG]</ta>
            <ta e="T1083" id="Seg_8297" s="T1082">two.[NOM.SG]</ta>
            <ta e="T1084" id="Seg_8298" s="T1083">ten.[NOM.SG]</ta>
            <ta e="T1085" id="Seg_8299" s="T1084">money.[NOM.SG]</ta>
            <ta e="T1087" id="Seg_8300" s="T1086">then</ta>
            <ta e="T1088" id="Seg_8301" s="T1087">father-NOM/GEN.3SG</ta>
            <ta e="T1089" id="Seg_8302" s="T1088">five.[NOM.SG]</ta>
            <ta e="T1090" id="Seg_8303" s="T1089">two.[NOM.SG]</ta>
            <ta e="T1091" id="Seg_8304" s="T1090">ten.[NOM.SG]</ta>
            <ta e="T1092" id="Seg_8305" s="T1091">send-PST.[3SG]</ta>
            <ta e="T1094" id="Seg_8306" s="T1093">then</ta>
            <ta e="T1095" id="Seg_8307" s="T1094">that.[NOM.SG]</ta>
            <ta e="T1096" id="Seg_8308" s="T1095">come-PST.[3SG]</ta>
            <ta e="T1097" id="Seg_8309" s="T1096">one.[NOM.SG]</ta>
            <ta e="T1099" id="Seg_8310" s="T1098">this.[NOM.SG]</ta>
            <ta e="T1100" id="Seg_8311" s="T1099">leave-MOM-PST.[3SG]</ta>
            <ta e="T1102" id="Seg_8312" s="T1101">then</ta>
            <ta e="T1103" id="Seg_8313" s="T1102">this.[NOM.SG]</ta>
            <ta e="T1104" id="Seg_8314" s="T1103">this-LAT</ta>
            <ta e="T1105" id="Seg_8315" s="T1104">this.[NOM.SG]</ta>
            <ta e="T1106" id="Seg_8316" s="T1105">write-PST.[3SG]</ta>
            <ta e="T1107" id="Seg_8317" s="T1106">write-PST.[3SG]</ta>
            <ta e="T1109" id="Seg_8318" s="T1108">this.[NOM.SG]</ta>
            <ta e="T1110" id="Seg_8319" s="T1109">NEG</ta>
            <ta e="T1111" id="Seg_8320" s="T1110">always</ta>
            <ta e="T1112" id="Seg_8321" s="T1111">NEG</ta>
            <ta e="T1113" id="Seg_8322" s="T1112">come-PRS.[3SG]</ta>
            <ta e="T1115" id="Seg_8323" s="T1114">then</ta>
            <ta e="T1116" id="Seg_8324" s="T1115">this.[NOM.SG]</ta>
            <ta e="T1523" id="Seg_8325" s="T1116">go-CVB</ta>
            <ta e="T1117" id="Seg_8326" s="T1523">disappear-PST.[3SG]</ta>
            <ta e="T1120" id="Seg_8327" s="T1119">where</ta>
            <ta e="T1121" id="Seg_8328" s="T1120">Lenin</ta>
            <ta e="T1122" id="Seg_8329" s="T1121">live-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_8330" s="T1122">this.[NOM.SG]</ta>
            <ta e="T1124" id="Seg_8331" s="T1123">house-LAT</ta>
            <ta e="T1126" id="Seg_8332" s="T1125">there</ta>
            <ta e="T1127" id="Seg_8333" s="T1126">canteen-GEN</ta>
            <ta e="T1130" id="Seg_8334" s="T1129">canteen-GEN</ta>
            <ta e="T1132" id="Seg_8335" s="T1131">bread.[NOM.SG]</ta>
            <ta e="T1133" id="Seg_8336" s="T1132">give-PRS.[3SG]</ta>
            <ta e="T1134" id="Seg_8337" s="T1133">vodka.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_8338" s="T1134">give-PRS.[3SG]</ta>
            <ta e="T1136" id="Seg_8339" s="T1135">PTCL</ta>
            <ta e="T1137" id="Seg_8340" s="T1136">what.[NOM.SG]</ta>
            <ta e="T1138" id="Seg_8341" s="T1137">give-PRS.[3SG]</ta>
            <ta e="T1139" id="Seg_8342" s="T1138">press-DUR.[3SG]</ta>
            <ta e="T1141" id="Seg_8343" s="T1140">ten.[NOM.SG]</ta>
            <ta e="T1143" id="Seg_8344" s="T1142">ten.[NOM.SG]</ta>
            <ta e="T1144" id="Seg_8345" s="T1143">three.[NOM.SG]</ta>
            <ta e="T1145" id="Seg_8346" s="T1144">and</ta>
            <ta e="T1146" id="Seg_8347" s="T1145">five.[NOM.SG]</ta>
            <ta e="T1147" id="Seg_8348" s="T1146">money.[NOM.SG]</ta>
            <ta e="T1148" id="Seg_8349" s="T1147">take-PRS-3SG.O</ta>
            <ta e="T1392" id="Seg_8350" s="T1391">I.NOM</ta>
            <ta e="T1393" id="Seg_8351" s="T1392">niece-GEN</ta>
            <ta e="T1394" id="Seg_8352" s="T1393">daughter-NOM/GEN.3SG</ta>
            <ta e="T1395" id="Seg_8353" s="T1394">man-LAT</ta>
            <ta e="T1524" id="Seg_8354" s="T1395">go-CVB</ta>
            <ta e="T1396" id="Seg_8355" s="T1524">disappear-PST.[3SG]</ta>
            <ta e="T1398" id="Seg_8356" s="T1397">and</ta>
            <ta e="T1399" id="Seg_8357" s="T1398">there</ta>
            <ta e="T1400" id="Seg_8358" s="T1399">five.[NOM.SG]</ta>
            <ta e="T1401" id="Seg_8359" s="T1400">child-PL</ta>
            <ta e="T1402" id="Seg_8360" s="T1401">be-PST-3PL</ta>
            <ta e="T1404" id="Seg_8361" s="T1403">then</ta>
            <ta e="T1405" id="Seg_8362" s="T1404">this.[NOM.SG]</ta>
            <ta e="T1406" id="Seg_8363" s="T1405">this-PL</ta>
            <ta e="T1407" id="Seg_8364" s="T1406">go-PST-3PL</ta>
            <ta e="T1408" id="Seg_8365" s="T1407">house.[NOM.SG]</ta>
            <ta e="T1409" id="Seg_8366" s="T1408">house-LAT</ta>
            <ta e="T1410" id="Seg_8367" s="T1409">there</ta>
            <ta e="T1412" id="Seg_8368" s="T1411">and</ta>
            <ta e="T1413" id="Seg_8369" s="T1412">be-PRS.[3SG]</ta>
            <ta e="T1414" id="Seg_8370" s="T1413">%%-PL</ta>
            <ta e="T1416" id="Seg_8371" s="T1415">then</ta>
            <ta e="T1417" id="Seg_8372" s="T1416">this-PL</ta>
            <ta e="T1418" id="Seg_8373" s="T1417">road.[NOM.SG]</ta>
            <ta e="T1419" id="Seg_8374" s="T1418">make-PST-3PL</ta>
            <ta e="T1420" id="Seg_8375" s="T1419">where</ta>
            <ta e="T1421" id="Seg_8376" s="T1420">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T1422" id="Seg_8377" s="T1421">road.[NOM.SG]</ta>
            <ta e="T1424" id="Seg_8378" s="T1423">then</ta>
            <ta e="T1425" id="Seg_8379" s="T1424">go-PST-3PL</ta>
            <ta e="T1426" id="Seg_8380" s="T1425">this.[NOM.SG]</ta>
            <ta e="T1427" id="Seg_8381" s="T1426">there</ta>
            <ta e="T1428" id="Seg_8382" s="T1427">north-LAT</ta>
            <ta e="T1429" id="Seg_8383" s="T1428">there</ta>
            <ta e="T1430" id="Seg_8384" s="T1429">also</ta>
            <ta e="T1431" id="Seg_8385" s="T1430">make-PST-3PL</ta>
            <ta e="T1432" id="Seg_8386" s="T1431">road.[NOM.SG]</ta>
            <ta e="T1433" id="Seg_8387" s="T1432">road.[NOM.SG]</ta>
            <ta e="T1435" id="Seg_8388" s="T1434">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T1436" id="Seg_8389" s="T1435">road.[NOM.SG]</ta>
            <ta e="T1438" id="Seg_8390" s="T1437">then</ta>
            <ta e="T1439" id="Seg_8391" s="T1438">this.[NOM.SG]</ta>
            <ta e="T1440" id="Seg_8392" s="T1439">come-PST.[3SG]</ta>
            <ta e="T1441" id="Seg_8393" s="T1440">house-LAT/LOC.3SG</ta>
            <ta e="T1442" id="Seg_8394" s="T1441">mother-LAT</ta>
            <ta e="T1444" id="Seg_8395" s="T1443">this</ta>
            <ta e="T1445" id="Seg_8396" s="T1444">then</ta>
            <ta e="T1446" id="Seg_8397" s="T1445">this.[NOM.SG]</ta>
            <ta e="T1447" id="Seg_8398" s="T1446">here</ta>
            <ta e="T1448" id="Seg_8399" s="T1447">be-PST.[3SG]</ta>
            <ta e="T1450" id="Seg_8400" s="T1449">then</ta>
            <ta e="T1451" id="Seg_8401" s="T1450">paper.[NOM.SG]</ta>
            <ta e="T1453" id="Seg_8402" s="T1451">write-PST-3PL</ta>
            <ta e="T1454" id="Seg_8403" s="T1453">you.NOM</ta>
            <ta e="T1455" id="Seg_8404" s="T1454">man.[NOM.SG]</ta>
            <ta e="T1456" id="Seg_8405" s="T1455">water-LOC</ta>
            <ta e="T1457" id="Seg_8406" s="T1456">die-RES-PST.[3SG]</ta>
            <ta e="T1459" id="Seg_8407" s="T1458">then</ta>
            <ta e="T1460" id="Seg_8408" s="T1459">this.[NOM.SG]</ta>
            <ta e="T1461" id="Seg_8409" s="T1460">go-PST.[3SG]</ta>
            <ta e="T1463" id="Seg_8410" s="T1462">this-ACC</ta>
            <ta e="T1464" id="Seg_8411" s="T1463">place-LAT</ta>
            <ta e="T1465" id="Seg_8412" s="T1464">put-PST-3PL</ta>
            <ta e="T1467" id="Seg_8413" s="T1466">then</ta>
            <ta e="T1468" id="Seg_8414" s="T1467">again</ta>
            <ta e="T1469" id="Seg_8415" s="T1468">this-LAT</ta>
            <ta e="T1470" id="Seg_8416" s="T1469">man.[NOM.SG]</ta>
            <ta e="T1471" id="Seg_8417" s="T1470">be-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_8418" s="T1472">there</ta>
            <ta e="T1474" id="Seg_8419" s="T1473">NEG.EX-PST.[3SG]</ta>
            <ta e="T1475" id="Seg_8420" s="T1474">woman-NOM/GEN.3SG</ta>
            <ta e="T1477" id="Seg_8421" s="T1476">and</ta>
            <ta e="T1478" id="Seg_8422" s="T1477">now</ta>
            <ta e="T1479" id="Seg_8423" s="T1478">live-DUR-3PL</ta>
            <ta e="T1480" id="Seg_8424" s="T1479">and</ta>
            <ta e="T1481" id="Seg_8425" s="T1480">there</ta>
            <ta e="T1482" id="Seg_8426" s="T1481">five.[NOM.SG]</ta>
            <ta e="T1483" id="Seg_8427" s="T1482">child-PL-LAT</ta>
            <ta e="T1484" id="Seg_8428" s="T1483">be-PST-3PL</ta>
            <ta e="T1486" id="Seg_8429" s="T1485">this.[NOM.SG]</ta>
            <ta e="T1487" id="Seg_8430" s="T1486">take-PST.[3SG]</ta>
            <ta e="T1488" id="Seg_8431" s="T1487">this-ACC</ta>
            <ta e="T1490" id="Seg_8432" s="T1489">very</ta>
            <ta e="T1491" id="Seg_8433" s="T1490">good</ta>
            <ta e="T1492" id="Seg_8434" s="T1491">man.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_8435" s="T1493">vodka.[NOM.SG]</ta>
            <ta e="T1495" id="Seg_8436" s="T1494">NEG</ta>
            <ta e="T1496" id="Seg_8437" s="T1495">drink-PRS.[3SG]</ta>
            <ta e="T1497" id="Seg_8438" s="T1496">where.to=INDEF</ta>
            <ta e="T1498" id="Seg_8439" s="T1497">NEG</ta>
            <ta e="T1499" id="Seg_8440" s="T1498">go-PRS.[3SG]</ta>
            <ta e="T1500" id="Seg_8441" s="T1499">always</ta>
            <ta e="T1501" id="Seg_8442" s="T1500">work-DUR.[3SG]</ta>
            <ta e="T1502" id="Seg_8443" s="T1501">there</ta>
            <ta e="T1503" id="Seg_8444" s="T1502">also</ta>
            <ta e="T1504" id="Seg_8445" s="T1503">mother-NOM/GEN.3SG</ta>
            <ta e="T1505" id="Seg_8446" s="T1504">NEG.EX.[3SG]</ta>
            <ta e="T1506" id="Seg_8447" s="T1505">only</ta>
            <ta e="T1507" id="Seg_8448" s="T1506">one.[NOM.SG]</ta>
            <ta e="T1508" id="Seg_8449" s="T1507">father-NOM/GEN.3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T129" id="Seg_8450" s="T128">когда</ta>
            <ta e="T130" id="Seg_8451" s="T129">я.NOM</ta>
            <ta e="T131" id="Seg_8452" s="T130">сидеть-PST-1SG</ta>
            <ta e="T132" id="Seg_8453" s="T131">Тарту.[NOM.SG]</ta>
            <ta e="T133" id="Seg_8454" s="T132">пойти-PST-1SG</ta>
            <ta e="T134" id="Seg_8455" s="T133">взять-PST-1SG</ta>
            <ta e="T136" id="Seg_8456" s="T135">три.[NOM.SG]</ta>
            <ta e="T137" id="Seg_8457" s="T136">платок-EP-PL</ta>
            <ta e="T139" id="Seg_8458" s="T138">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T140" id="Seg_8459" s="T139">люди-NOM/GEN/ACC.3SG</ta>
            <ta e="T141" id="Seg_8460" s="T140">дать-INF.LAT</ta>
            <ta e="T143" id="Seg_8461" s="T142">тогда</ta>
            <ta e="T144" id="Seg_8462" s="T143">здесь</ta>
            <ta e="T145" id="Seg_8463" s="T144">прийти-PST-1SG</ta>
            <ta e="T146" id="Seg_8464" s="T145">взять-PST-1SG</ta>
            <ta e="T147" id="Seg_8465" s="T146">штаны-PL</ta>
            <ta e="T149" id="Seg_8466" s="T148">рубашка.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8467" s="T149">взять-PST-1SG</ta>
            <ta e="T152" id="Seg_8468" s="T151">мальчик-LAT</ta>
            <ta e="T153" id="Seg_8469" s="T152">племянник-EP-NOM/GEN/ACC.1SG-LAT</ta>
            <ta e="T155" id="Seg_8470" s="T154">дочь-LAT</ta>
            <ta e="T156" id="Seg_8471" s="T155">взять-PST-1SG</ta>
            <ta e="T158" id="Seg_8472" s="T157">платье.[NOM.SG]</ta>
            <ta e="T159" id="Seg_8473" s="T158">взять-PST-1SG</ta>
            <ta e="T161" id="Seg_8474" s="T160">тогда</ta>
            <ta e="T162" id="Seg_8475" s="T161">племянник-LAT</ta>
            <ta e="T163" id="Seg_8476" s="T162">взять-PST-1SG</ta>
            <ta e="T164" id="Seg_8477" s="T163">рубашка.[NOM.SG]</ta>
            <ta e="T166" id="Seg_8478" s="T165">хватит</ta>
            <ta e="T230" id="Seg_8479" s="T228">штаны-PL</ta>
            <ta e="T234" id="Seg_8480" s="T233">штаны.[NOM.SG]</ta>
            <ta e="T249" id="Seg_8481" s="T247">штаны.[NOM.SG]</ta>
            <ta e="T251" id="Seg_8482" s="T250">сапог-PL</ta>
            <ta e="T252" id="Seg_8483" s="T251">штаны.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8484" s="T252">сапог.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8485" s="T268">сапог.[NOM.SG]</ta>
            <ta e="T274" id="Seg_8486" s="T273">штаны-PL</ta>
            <ta e="T278" id="Seg_8487" s="T277">рубашка.[NOM.SG]</ta>
            <ta e="T348" id="Seg_8488" s="T347">рубашка.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8489" s="T352">шапка.[NOM.SG]</ta>
            <ta e="T504" id="Seg_8490" s="T503">когда</ta>
            <ta e="T505" id="Seg_8491" s="T504">этот.[NOM.SG]</ta>
            <ta e="T506" id="Seg_8492" s="T505">прийти-PST.[3SG]</ta>
            <ta e="T507" id="Seg_8493" s="T506">я.LAT</ta>
            <ta e="T510" id="Seg_8494" s="T509">сказать-IPFVZ.[3SG]</ta>
            <ta e="T511" id="Seg_8495" s="T510">бабушка.[NOM.SG]</ta>
            <ta e="T512" id="Seg_8496" s="T511">я.NOM</ta>
            <ta e="T513" id="Seg_8497" s="T512">ты.DAT</ta>
            <ta e="T514" id="Seg_8498" s="T513">прийти-PRS-1SG</ta>
            <ta e="T515" id="Seg_8499" s="T514">а</ta>
            <ta e="T516" id="Seg_8500" s="T515">я.NOM</ta>
            <ta e="T517" id="Seg_8501" s="T516">сказать-IPFVZ-1SG</ta>
            <ta e="T518" id="Seg_8502" s="T517">NEG.AUX-IMP.2SG</ta>
            <ta e="T519" id="Seg_8503" s="T518">прийти-CNG</ta>
            <ta e="T521" id="Seg_8504" s="T520">прийти-IMP.2SG</ta>
            <ta e="T522" id="Seg_8505" s="T521">я.LAT</ta>
            <ta e="T524" id="Seg_8506" s="T523">пойти-EP-IMP.2SG</ta>
            <ta e="T525" id="Seg_8507" s="T524">отсюда</ta>
            <ta e="T527" id="Seg_8508" s="T526">ты.NOM</ta>
            <ta e="T528" id="Seg_8509" s="T527">я.LAT</ta>
            <ta e="T531" id="Seg_8510" s="T530">ты.NOM</ta>
            <ta e="T532" id="Seg_8511" s="T531">я.LAT</ta>
            <ta e="T534" id="Seg_8512" s="T533">я.NOM</ta>
            <ta e="T535" id="Seg_8513" s="T534">ты.NOM-COM</ta>
            <ta e="T536" id="Seg_8514" s="T535">NEG</ta>
            <ta e="T537" id="Seg_8515" s="T536">говорить-FUT-1SG</ta>
            <ta e="T539" id="Seg_8516" s="T538">пойти-EP-IMP.2SG</ta>
            <ta e="T540" id="Seg_8517" s="T539">отсюда</ta>
            <ta e="T556" id="Seg_8518" s="T555">когда</ta>
            <ta e="T557" id="Seg_8519" s="T556">этот.[NOM.SG]</ta>
            <ta e="T558" id="Seg_8520" s="T557">я.LAT</ta>
            <ta e="T559" id="Seg_8521" s="T558">прийти-PST.[3SG]</ta>
            <ta e="T560" id="Seg_8522" s="T559">прийти-PST.[3SG]</ta>
            <ta e="T571" id="Seg_8523" s="T568">когда</ta>
            <ta e="T572" id="Seg_8524" s="T571">этот.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8525" s="T572">я.LAT</ta>
            <ta e="T574" id="Seg_8526" s="T573">прийти-PST.[3SG]</ta>
            <ta e="T575" id="Seg_8527" s="T574">я.NOM</ta>
            <ta e="T576" id="Seg_8528" s="T575">ругать-PST-1SG</ta>
            <ta e="T578" id="Seg_8529" s="T576">этот-ACC</ta>
            <ta e="T579" id="Seg_8530" s="T578">NEG.AUX-IMP.2SG</ta>
            <ta e="T580" id="Seg_8531" s="T579">прийти-CNG</ta>
            <ta e="T581" id="Seg_8532" s="T580">я.LAT</ta>
            <ta e="T582" id="Seg_8533" s="T581">я.NOM</ta>
            <ta e="T583" id="Seg_8534" s="T582">ты.DAT</ta>
            <ta e="T584" id="Seg_8535" s="T583">что.[NOM.SG]=INDEF</ta>
            <ta e="T585" id="Seg_8536" s="T584">NEG</ta>
            <ta e="T586" id="Seg_8537" s="T585">сказать-FUT-1SG</ta>
            <ta e="T588" id="Seg_8538" s="T587">пойти-EP-IMP.2SG</ta>
            <ta e="T589" id="Seg_8539" s="T588">отсюда</ta>
            <ta e="T591" id="Seg_8540" s="T590">тогда</ta>
            <ta e="T592" id="Seg_8541" s="T591">этот.[NOM.SG]</ta>
            <ta e="T593" id="Seg_8542" s="T592">сказать-IPFVZ.[3SG]</ta>
            <ta e="T594" id="Seg_8543" s="T593">очень</ta>
            <ta e="T595" id="Seg_8544" s="T594">хороший</ta>
            <ta e="T596" id="Seg_8545" s="T595">говорить-PRS-2SG</ta>
            <ta e="T598" id="Seg_8546" s="T597">я.NOM</ta>
            <ta e="T599" id="Seg_8547" s="T598">сказать-IPFVZ-1SG</ta>
            <ta e="T601" id="Seg_8548" s="T600">хороший</ta>
            <ta e="T602" id="Seg_8549" s="T601">ругать-PRS-1SG</ta>
            <ta e="T603" id="Seg_8550" s="T602">ты.DAT</ta>
            <ta e="T605" id="Seg_8551" s="T604">а</ta>
            <ta e="T606" id="Seg_8552" s="T605">ты.NOM</ta>
            <ta e="T607" id="Seg_8553" s="T606">сказать-PRS-2SG</ta>
            <ta e="T608" id="Seg_8554" s="T607">хороший</ta>
            <ta e="T861" id="Seg_8555" s="T860">когда</ta>
            <ta e="T862" id="Seg_8556" s="T861">когда</ta>
            <ta e="T863" id="Seg_8557" s="T862">прийти-PST.[3SG]</ta>
            <ta e="T866" id="Seg_8558" s="T865">я.LAT</ta>
            <ta e="T868" id="Seg_8559" s="T867">и</ta>
            <ta e="T869" id="Seg_8560" s="T868">этот.[NOM.SG]</ta>
            <ta e="T870" id="Seg_8561" s="T869">писать-PST.[3SG]</ta>
            <ta e="T871" id="Seg_8562" s="T870">я.LAT</ta>
            <ta e="T872" id="Seg_8563" s="T871">прийти-FUT-1SG</ta>
            <ta e="T873" id="Seg_8564" s="T872">ты.DAT</ta>
            <ta e="T875" id="Seg_8565" s="T874">я.NOM</ta>
            <ta e="T876" id="Seg_8566" s="T875">ждать-PST-1SG</ta>
            <ta e="T877" id="Seg_8567" s="T876">ждать-PST-1SG</ta>
            <ta e="T878" id="Seg_8568" s="T877">NEG.EX.[3SG]</ta>
            <ta e="T880" id="Seg_8569" s="T879">тогда</ta>
            <ta e="T881" id="Seg_8570" s="T880">нажимать-DUR.[3SG]</ta>
            <ta e="T882" id="Seg_8571" s="T881">я.LAT</ta>
            <ta e="T883" id="Seg_8572" s="T882">бумага.[NOM.SG]</ta>
            <ta e="T884" id="Seg_8573" s="T883">я.NOM</ta>
            <ta e="T885" id="Seg_8574" s="T884">NEG</ta>
            <ta e="T886" id="Seg_8575" s="T885">прийти-FUT-1SG</ta>
            <ta e="T888" id="Seg_8576" s="T887">тот.[NOM.SG]</ta>
            <ta e="T890" id="Seg_8577" s="T889">этот.[NOM.SG]</ta>
            <ta e="T891" id="Seg_8578" s="T890">зима-LOC</ta>
            <ta e="T892" id="Seg_8579" s="T891">прийти</ta>
            <ta e="T893" id="Seg_8580" s="T892">прийти-FUT-1SG</ta>
            <ta e="T895" id="Seg_8581" s="T894">ты.DAT</ta>
            <ta e="T897" id="Seg_8582" s="T896">а</ta>
            <ta e="T898" id="Seg_8583" s="T897">я.NOM</ta>
            <ta e="T899" id="Seg_8584" s="T898">сказать-PST-1SG</ta>
            <ta e="T900" id="Seg_8585" s="T899">я.NOM</ta>
            <ta e="T901" id="Seg_8586" s="T900">может.быть</ta>
            <ta e="T902" id="Seg_8587" s="T901">умереть-RES-FUT-1SG</ta>
            <ta e="T904" id="Seg_8588" s="T903">а</ta>
            <ta e="T905" id="Seg_8589" s="T904">ты.NOM</ta>
            <ta e="T906" id="Seg_8590" s="T905">тогда</ta>
            <ta e="T907" id="Seg_8591" s="T906">прийти-FUT-2SG</ta>
            <ta e="T909" id="Seg_8592" s="T908">тогда</ta>
            <ta e="T910" id="Seg_8593" s="T909">собирать-PST-1SG</ta>
            <ta e="T911" id="Seg_8594" s="T910">и</ta>
            <ta e="T912" id="Seg_8595" s="T911">здесь</ta>
            <ta e="T913" id="Seg_8596" s="T912">прийти-PST-1SG</ta>
            <ta e="T917" id="Seg_8597" s="T916">я.NOM</ta>
            <ta e="T918" id="Seg_8598" s="T917">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T919" id="Seg_8599" s="T918">мужчина-LAT</ta>
            <ta e="T920" id="Seg_8600" s="T919">пойти-PST.[3SG]</ta>
            <ta e="T922" id="Seg_8601" s="T921">быть-PST-1SG</ta>
            <ta e="T923" id="Seg_8602" s="T922">Красноярск-EP-LOC</ta>
            <ta e="T924" id="Seg_8603" s="T923">этот.[NOM.SG]</ta>
            <ta e="T926" id="Seg_8604" s="T924">сказать-IPFVZ.[3SG]</ta>
            <ta e="T927" id="Seg_8605" s="T926">я.LAT</ta>
            <ta e="T928" id="Seg_8606" s="T927">один.[NOM.SG]</ta>
            <ta e="T929" id="Seg_8607" s="T928">мальчик.[NOM.SG]</ta>
            <ta e="T930" id="Seg_8608" s="T929">взять-INF.LAT</ta>
            <ta e="T931" id="Seg_8609" s="T930">отец-LAT</ta>
            <ta e="T932" id="Seg_8610" s="T931">NEG.AUX-IMP.2SG</ta>
            <ta e="T933" id="Seg_8611" s="T932">сказать-IMP.2SG.O</ta>
            <ta e="T935" id="Seg_8612" s="T934">а</ta>
            <ta e="T936" id="Seg_8613" s="T935">я.NOM</ta>
            <ta e="T938" id="Seg_8614" s="T936">сказать-PST-1SG</ta>
            <ta e="T939" id="Seg_8615" s="T938">как</ta>
            <ta e="T940" id="Seg_8616" s="T939">NEG</ta>
            <ta e="T941" id="Seg_8617" s="T940">сказать-INF.LAT</ta>
            <ta e="T942" id="Seg_8618" s="T941">отец-NOM/GEN.3SG</ta>
            <ta e="T943" id="Seg_8619" s="T942">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T944" id="Seg_8620" s="T943">взять-PST.[3SG]</ta>
            <ta e="T945" id="Seg_8621" s="T944">сам-LAT/LOC.3SG</ta>
            <ta e="T946" id="Seg_8622" s="T945">и</ta>
            <ta e="T948" id="Seg_8623" s="T947">и</ta>
            <ta e="T949" id="Seg_8624" s="T948">вы.NOM</ta>
            <ta e="T950" id="Seg_8625" s="T949">принести-PST.[3SG]</ta>
            <ta e="T952" id="Seg_8626" s="T951">тогда</ta>
            <ta e="T953" id="Seg_8627" s="T952">прийти-PST-1SG</ta>
            <ta e="T954" id="Seg_8628" s="T953">отец-LAT</ta>
            <ta e="T955" id="Seg_8629" s="T954">сказать-PST-1SG</ta>
            <ta e="T956" id="Seg_8630" s="T955">этот.[NOM.SG]</ta>
            <ta e="T957" id="Seg_8631" s="T956">писать-PST.[3SG]</ta>
            <ta e="T958" id="Seg_8632" s="T957">этот-LAT</ta>
            <ta e="T959" id="Seg_8633" s="T958">бумага.[NOM.SG]</ta>
            <ta e="T961" id="Seg_8634" s="T960">тогда</ta>
            <ta e="T962" id="Seg_8635" s="T961">этот.[NOM.SG]</ta>
            <ta e="T963" id="Seg_8636" s="T962">этот-LAT</ta>
            <ta e="T964" id="Seg_8637" s="T963">бумага.[NOM.SG]</ta>
            <ta e="T965" id="Seg_8638" s="T964">писать-PST.[3SG]</ta>
            <ta e="T966" id="Seg_8639" s="T965">этот.[NOM.SG]</ta>
            <ta e="T967" id="Seg_8640" s="T966">здесь</ta>
            <ta e="T968" id="Seg_8641" s="T967">прийти-PST.[3SG]</ta>
            <ta e="T970" id="Seg_8642" s="T969">тогда</ta>
            <ta e="T971" id="Seg_8643" s="T970">мясо.[NOM.SG]</ta>
            <ta e="T972" id="Seg_8644" s="T971">свинья.[NOM.SG]</ta>
            <ta e="T973" id="Seg_8645" s="T972">зарезать-PST-1PL</ta>
            <ta e="T974" id="Seg_8646" s="T973">мясо.[NOM.SG]</ta>
            <ta e="T975" id="Seg_8647" s="T974">много</ta>
            <ta e="T976" id="Seg_8648" s="T975">хлеб.[NOM.SG]</ta>
            <ta e="T977" id="Seg_8649" s="T976">печь-PST-1PL</ta>
            <ta e="T978" id="Seg_8650" s="T977">и</ta>
            <ta e="T979" id="Seg_8651" s="T978">люди.[NOM.SG]</ta>
            <ta e="T980" id="Seg_8652" s="T979">собирать-PST-1PL</ta>
            <ta e="T982" id="Seg_8653" s="T981">тогда</ta>
            <ta e="T983" id="Seg_8654" s="T982">водка.[NOM.SG]</ta>
            <ta e="T984" id="Seg_8655" s="T983">быть-PST.[3SG]</ta>
            <ta e="T986" id="Seg_8656" s="T985">пелмень-EP-PL</ta>
            <ta e="T987" id="Seg_8657" s="T986">делать-PST-1PL</ta>
            <ta e="T988" id="Seg_8658" s="T987">котлета-NOM/GEN/ACC.3PL</ta>
            <ta e="T989" id="Seg_8659" s="T988">много</ta>
            <ta e="T990" id="Seg_8660" s="T989">быть-PST.[3SG]</ta>
            <ta e="T991" id="Seg_8661" s="T990">что.[NOM.SG]</ta>
            <ta e="T992" id="Seg_8662" s="T991">быть-PST.[3SG]</ta>
            <ta e="T994" id="Seg_8663" s="T993">и</ta>
            <ta e="T995" id="Seg_8664" s="T994">люди.[NOM.SG]</ta>
            <ta e="T996" id="Seg_8665" s="T995">много</ta>
            <ta e="T997" id="Seg_8666" s="T996">быть-PST-3PL</ta>
            <ta e="T1525" id="Seg_8667" s="T998">Агинское</ta>
            <ta e="T999" id="Seg_8668" s="T1525">поселение-LOC</ta>
            <ta e="T1000" id="Seg_8669" s="T999">прийти-PST-3PL</ta>
            <ta e="T1002" id="Seg_8670" s="T1001">тогда</ta>
            <ta e="T1003" id="Seg_8671" s="T1002">я.NOM</ta>
            <ta e="T1004" id="Seg_8672" s="T1003">этот-LAT</ta>
            <ta e="T1005" id="Seg_8673" s="T1004">взять-PST-1SG</ta>
            <ta e="T1006" id="Seg_8674" s="T1005">пальто.[NOM.SG]</ta>
            <ta e="T1008" id="Seg_8675" s="T1007">тогда</ta>
            <ta e="T1009" id="Seg_8676" s="T1008">деньги.[NOM.SG]</ta>
            <ta e="T1010" id="Seg_8677" s="T1009">этот-LAT</ta>
            <ta e="T1012" id="Seg_8678" s="T1011">дать-PST-1SG</ta>
            <ta e="T1014" id="Seg_8679" s="T1013">этот.[NOM.SG]</ta>
            <ta e="T1522" id="Seg_8680" s="T1014">пойти-CVB</ta>
            <ta e="T1015" id="Seg_8681" s="T1522">исчезнуть-PST.[3SG]</ta>
            <ta e="T1016" id="Seg_8682" s="T1015">этот.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_8683" s="T1016">сын-COM</ta>
            <ta e="T1019" id="Seg_8684" s="T1018">там</ta>
            <ta e="T1020" id="Seg_8685" s="T1019">где</ta>
            <ta e="T1022" id="Seg_8686" s="T1021">люди.[NOM.SG]</ta>
            <ta e="T1023" id="Seg_8687" s="T1022">где</ta>
            <ta e="T1024" id="Seg_8688" s="T1023">люди.[NOM.SG]</ta>
            <ta e="T1025" id="Seg_8689" s="T1024">говорить</ta>
            <ta e="T1026" id="Seg_8690" s="T1025">говорить-PRS-3PL</ta>
            <ta e="T1027" id="Seg_8691" s="T1026">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1028" id="Seg_8692" s="T1027">язык-3SG-INS</ta>
            <ta e="T1030" id="Seg_8693" s="T1029">тогда</ta>
            <ta e="T1031" id="Seg_8694" s="T1030">этот.[NOM.SG]</ta>
            <ta e="T1032" id="Seg_8695" s="T1031">этот.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_8696" s="T1032">мальчик-GEN</ta>
            <ta e="T1034" id="Seg_8697" s="T1033">мать.[NOM.SG]</ta>
            <ta e="T1036" id="Seg_8698" s="T1035">этот-ACC</ta>
            <ta e="T1037" id="Seg_8699" s="T1036">этот-ACC</ta>
            <ta e="T1038" id="Seg_8700" s="T1037">NEG</ta>
            <ta e="T1039" id="Seg_8701" s="T1038">жалеть-PST.[3SG]</ta>
            <ta e="T1040" id="Seg_8702" s="T1039">тогда</ta>
            <ta e="T1041" id="Seg_8703" s="T1040">этот.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_8704" s="T1041">здесь</ta>
            <ta e="T1044" id="Seg_8705" s="T1042">нажимать-DUR.[3SG]</ta>
            <ta e="T1046" id="Seg_8706" s="T1045">каждый</ta>
            <ta e="T1048" id="Seg_8707" s="T1047">день.[NOM.SG]</ta>
            <ta e="T1049" id="Seg_8708" s="T1048">плакать-DUR-1SG</ta>
            <ta e="T1051" id="Seg_8709" s="T1050">мы.NOM</ta>
            <ta e="T1053" id="Seg_8710" s="T1052">этот-LAT</ta>
            <ta e="T1055" id="Seg_8711" s="T1053">нажимать-INF.LAT</ta>
            <ta e="T1056" id="Seg_8712" s="T1055">прийти-IMP.2SG</ta>
            <ta e="T1057" id="Seg_8713" s="T1056">дом-LAT-2SG</ta>
            <ta e="T1059" id="Seg_8714" s="T1058">тогда</ta>
            <ta e="T1060" id="Seg_8715" s="T1059">этот.[NOM.SG]</ta>
            <ta e="T1061" id="Seg_8716" s="T1060">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1062" id="Seg_8717" s="T1061">я.NOM</ta>
            <ta e="T1063" id="Seg_8718" s="T1062">один</ta>
            <ta e="T1064" id="Seg_8719" s="T1063">прийти-FUT-1SG</ta>
            <ta e="T1066" id="Seg_8720" s="T1065">а</ta>
            <ta e="T1067" id="Seg_8721" s="T1066">что.[NOM.SG]</ta>
            <ta e="T1068" id="Seg_8722" s="T1067">один</ta>
            <ta e="T1069" id="Seg_8723" s="T1068">прийти-FUT-2SG</ta>
            <ta e="T1071" id="Seg_8724" s="T1070">а</ta>
            <ta e="T1072" id="Seg_8725" s="T1071">этот.[NOM.SG]</ta>
            <ta e="T1073" id="Seg_8726" s="T1072">опять</ta>
            <ta e="T1074" id="Seg_8727" s="T1073">нажимать-DUR.[3SG]</ta>
            <ta e="T1075" id="Seg_8728" s="T1074">деньги.[NOM.SG]</ta>
            <ta e="T1076" id="Seg_8729" s="T1075">NEG.EX.[3SG]</ta>
            <ta e="T1078" id="Seg_8730" s="T1077">я.NOM</ta>
            <ta e="T1079" id="Seg_8731" s="T1078">опять</ta>
            <ta e="T1080" id="Seg_8732" s="T1079">посылать-PST-1SG</ta>
            <ta e="T1081" id="Seg_8733" s="T1080">два.[NOM.SG]</ta>
            <ta e="T1082" id="Seg_8734" s="T1081">десять.[NOM.SG]</ta>
            <ta e="T1083" id="Seg_8735" s="T1082">два.[NOM.SG]</ta>
            <ta e="T1084" id="Seg_8736" s="T1083">десять.[NOM.SG]</ta>
            <ta e="T1085" id="Seg_8737" s="T1084">деньги.[NOM.SG]</ta>
            <ta e="T1087" id="Seg_8738" s="T1086">тогда</ta>
            <ta e="T1088" id="Seg_8739" s="T1087">отец-NOM/GEN.3SG</ta>
            <ta e="T1089" id="Seg_8740" s="T1088">пять.[NOM.SG]</ta>
            <ta e="T1090" id="Seg_8741" s="T1089">два.[NOM.SG]</ta>
            <ta e="T1091" id="Seg_8742" s="T1090">десять.[NOM.SG]</ta>
            <ta e="T1092" id="Seg_8743" s="T1091">посылать-PST.[3SG]</ta>
            <ta e="T1094" id="Seg_8744" s="T1093">тогда</ta>
            <ta e="T1095" id="Seg_8745" s="T1094">тот.[NOM.SG]</ta>
            <ta e="T1096" id="Seg_8746" s="T1095">прийти-PST.[3SG]</ta>
            <ta e="T1097" id="Seg_8747" s="T1096">один.[NOM.SG]</ta>
            <ta e="T1099" id="Seg_8748" s="T1098">этот.[NOM.SG]</ta>
            <ta e="T1100" id="Seg_8749" s="T1099">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1102" id="Seg_8750" s="T1101">тогда</ta>
            <ta e="T1103" id="Seg_8751" s="T1102">этот.[NOM.SG]</ta>
            <ta e="T1104" id="Seg_8752" s="T1103">этот-LAT</ta>
            <ta e="T1105" id="Seg_8753" s="T1104">этот.[NOM.SG]</ta>
            <ta e="T1106" id="Seg_8754" s="T1105">писать-PST.[3SG]</ta>
            <ta e="T1107" id="Seg_8755" s="T1106">писать-PST.[3SG]</ta>
            <ta e="T1109" id="Seg_8756" s="T1108">этот.[NOM.SG]</ta>
            <ta e="T1110" id="Seg_8757" s="T1109">NEG</ta>
            <ta e="T1111" id="Seg_8758" s="T1110">всегда</ta>
            <ta e="T1112" id="Seg_8759" s="T1111">NEG</ta>
            <ta e="T1113" id="Seg_8760" s="T1112">прийти-PRS.[3SG]</ta>
            <ta e="T1115" id="Seg_8761" s="T1114">тогда</ta>
            <ta e="T1116" id="Seg_8762" s="T1115">этот.[NOM.SG]</ta>
            <ta e="T1523" id="Seg_8763" s="T1116">пойти-CVB</ta>
            <ta e="T1117" id="Seg_8764" s="T1523">исчезнуть-PST.[3SG]</ta>
            <ta e="T1120" id="Seg_8765" s="T1119">где</ta>
            <ta e="T1121" id="Seg_8766" s="T1120">Ленин</ta>
            <ta e="T1122" id="Seg_8767" s="T1121">жить-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_8768" s="T1122">этот.[NOM.SG]</ta>
            <ta e="T1124" id="Seg_8769" s="T1123">дом-LAT</ta>
            <ta e="T1126" id="Seg_8770" s="T1125">там</ta>
            <ta e="T1127" id="Seg_8771" s="T1126">столовая-GEN</ta>
            <ta e="T1130" id="Seg_8772" s="T1129">столовая-GEN</ta>
            <ta e="T1132" id="Seg_8773" s="T1131">хлеб.[NOM.SG]</ta>
            <ta e="T1133" id="Seg_8774" s="T1132">дать-PRS.[3SG]</ta>
            <ta e="T1134" id="Seg_8775" s="T1133">водка.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_8776" s="T1134">дать-PRS.[3SG]</ta>
            <ta e="T1136" id="Seg_8777" s="T1135">PTCL</ta>
            <ta e="T1137" id="Seg_8778" s="T1136">что.[NOM.SG]</ta>
            <ta e="T1138" id="Seg_8779" s="T1137">дать-PRS.[3SG]</ta>
            <ta e="T1139" id="Seg_8780" s="T1138">нажимать-DUR.[3SG]</ta>
            <ta e="T1141" id="Seg_8781" s="T1140">десять.[NOM.SG]</ta>
            <ta e="T1143" id="Seg_8782" s="T1142">десять.[NOM.SG]</ta>
            <ta e="T1144" id="Seg_8783" s="T1143">три.[NOM.SG]</ta>
            <ta e="T1145" id="Seg_8784" s="T1144">и</ta>
            <ta e="T1146" id="Seg_8785" s="T1145">пять.[NOM.SG]</ta>
            <ta e="T1147" id="Seg_8786" s="T1146">деньги.[NOM.SG]</ta>
            <ta e="T1148" id="Seg_8787" s="T1147">взять-PRS-3SG.O</ta>
            <ta e="T1392" id="Seg_8788" s="T1391">я.NOM</ta>
            <ta e="T1393" id="Seg_8789" s="T1392">племянница-GEN</ta>
            <ta e="T1394" id="Seg_8790" s="T1393">дочь-NOM/GEN.3SG</ta>
            <ta e="T1395" id="Seg_8791" s="T1394">мужчина-LAT</ta>
            <ta e="T1524" id="Seg_8792" s="T1395">пойти-CVB</ta>
            <ta e="T1396" id="Seg_8793" s="T1524">исчезнуть-PST.[3SG]</ta>
            <ta e="T1398" id="Seg_8794" s="T1397">и</ta>
            <ta e="T1399" id="Seg_8795" s="T1398">там</ta>
            <ta e="T1400" id="Seg_8796" s="T1399">пять.[NOM.SG]</ta>
            <ta e="T1401" id="Seg_8797" s="T1400">ребенок-PL</ta>
            <ta e="T1402" id="Seg_8798" s="T1401">быть-PST-3PL</ta>
            <ta e="T1404" id="Seg_8799" s="T1403">тогда</ta>
            <ta e="T1405" id="Seg_8800" s="T1404">этот.[NOM.SG]</ta>
            <ta e="T1406" id="Seg_8801" s="T1405">этот-PL</ta>
            <ta e="T1407" id="Seg_8802" s="T1406">пойти-PST-3PL</ta>
            <ta e="T1408" id="Seg_8803" s="T1407">дом.[NOM.SG]</ta>
            <ta e="T1409" id="Seg_8804" s="T1408">дом-LAT</ta>
            <ta e="T1410" id="Seg_8805" s="T1409">там</ta>
            <ta e="T1412" id="Seg_8806" s="T1411">и</ta>
            <ta e="T1413" id="Seg_8807" s="T1412">быть-PRS.[3SG]</ta>
            <ta e="T1414" id="Seg_8808" s="T1413">%%-PL</ta>
            <ta e="T1416" id="Seg_8809" s="T1415">тогда</ta>
            <ta e="T1417" id="Seg_8810" s="T1416">этот-PL</ta>
            <ta e="T1418" id="Seg_8811" s="T1417">дорога.[NOM.SG]</ta>
            <ta e="T1419" id="Seg_8812" s="T1418">делать-PST-3PL</ta>
            <ta e="T1420" id="Seg_8813" s="T1419">где</ta>
            <ta e="T1421" id="Seg_8814" s="T1420">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T1422" id="Seg_8815" s="T1421">дорога.[NOM.SG]</ta>
            <ta e="T1424" id="Seg_8816" s="T1423">тогда</ta>
            <ta e="T1425" id="Seg_8817" s="T1424">пойти-PST-3PL</ta>
            <ta e="T1426" id="Seg_8818" s="T1425">этот.[NOM.SG]</ta>
            <ta e="T1427" id="Seg_8819" s="T1426">там</ta>
            <ta e="T1428" id="Seg_8820" s="T1427">север-LAT</ta>
            <ta e="T1429" id="Seg_8821" s="T1428">там</ta>
            <ta e="T1430" id="Seg_8822" s="T1429">тоже</ta>
            <ta e="T1431" id="Seg_8823" s="T1430">делать-PST-3PL</ta>
            <ta e="T1432" id="Seg_8824" s="T1431">дорога.[NOM.SG]</ta>
            <ta e="T1433" id="Seg_8825" s="T1432">дорога.[NOM.SG]</ta>
            <ta e="T1435" id="Seg_8826" s="T1434">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T1436" id="Seg_8827" s="T1435">дорога.[NOM.SG]</ta>
            <ta e="T1438" id="Seg_8828" s="T1437">тогда</ta>
            <ta e="T1439" id="Seg_8829" s="T1438">этот.[NOM.SG]</ta>
            <ta e="T1440" id="Seg_8830" s="T1439">прийти-PST.[3SG]</ta>
            <ta e="T1441" id="Seg_8831" s="T1440">дом-LAT/LOC.3SG</ta>
            <ta e="T1442" id="Seg_8832" s="T1441">мать-LAT</ta>
            <ta e="T1444" id="Seg_8833" s="T1443">этот</ta>
            <ta e="T1445" id="Seg_8834" s="T1444">тогда</ta>
            <ta e="T1446" id="Seg_8835" s="T1445">этот.[NOM.SG]</ta>
            <ta e="T1447" id="Seg_8836" s="T1446">здесь</ta>
            <ta e="T1448" id="Seg_8837" s="T1447">быть-PST.[3SG]</ta>
            <ta e="T1450" id="Seg_8838" s="T1449">тогда</ta>
            <ta e="T1451" id="Seg_8839" s="T1450">бумага.[NOM.SG]</ta>
            <ta e="T1453" id="Seg_8840" s="T1451">писать-PST-3PL</ta>
            <ta e="T1454" id="Seg_8841" s="T1453">ты.NOM</ta>
            <ta e="T1455" id="Seg_8842" s="T1454">мужчина.[NOM.SG]</ta>
            <ta e="T1456" id="Seg_8843" s="T1455">вода-LOC</ta>
            <ta e="T1457" id="Seg_8844" s="T1456">умереть-RES-PST.[3SG]</ta>
            <ta e="T1459" id="Seg_8845" s="T1458">тогда</ta>
            <ta e="T1460" id="Seg_8846" s="T1459">этот.[NOM.SG]</ta>
            <ta e="T1461" id="Seg_8847" s="T1460">пойти-PST.[3SG]</ta>
            <ta e="T1463" id="Seg_8848" s="T1462">этот-ACC</ta>
            <ta e="T1464" id="Seg_8849" s="T1463">место-LAT</ta>
            <ta e="T1465" id="Seg_8850" s="T1464">класть-PST-3PL</ta>
            <ta e="T1467" id="Seg_8851" s="T1466">тогда</ta>
            <ta e="T1468" id="Seg_8852" s="T1467">опять</ta>
            <ta e="T1469" id="Seg_8853" s="T1468">этот-LAT</ta>
            <ta e="T1470" id="Seg_8854" s="T1469">мужчина.[NOM.SG]</ta>
            <ta e="T1471" id="Seg_8855" s="T1470">быть-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_8856" s="T1472">там</ta>
            <ta e="T1474" id="Seg_8857" s="T1473">NEG.EX-PST.[3SG]</ta>
            <ta e="T1475" id="Seg_8858" s="T1474">женщина-NOM/GEN.3SG</ta>
            <ta e="T1477" id="Seg_8859" s="T1476">и</ta>
            <ta e="T1478" id="Seg_8860" s="T1477">сейчас</ta>
            <ta e="T1479" id="Seg_8861" s="T1478">жить-DUR-3PL</ta>
            <ta e="T1480" id="Seg_8862" s="T1479">и</ta>
            <ta e="T1481" id="Seg_8863" s="T1480">там</ta>
            <ta e="T1482" id="Seg_8864" s="T1481">пять.[NOM.SG]</ta>
            <ta e="T1483" id="Seg_8865" s="T1482">ребенок-PL-LAT</ta>
            <ta e="T1484" id="Seg_8866" s="T1483">быть-PST-3PL</ta>
            <ta e="T1486" id="Seg_8867" s="T1485">этот.[NOM.SG]</ta>
            <ta e="T1487" id="Seg_8868" s="T1486">взять-PST.[3SG]</ta>
            <ta e="T1488" id="Seg_8869" s="T1487">этот-ACC</ta>
            <ta e="T1490" id="Seg_8870" s="T1489">очень</ta>
            <ta e="T1491" id="Seg_8871" s="T1490">хороший</ta>
            <ta e="T1492" id="Seg_8872" s="T1491">мужчина.[NOM.SG]</ta>
            <ta e="T1494" id="Seg_8873" s="T1493">водка.[NOM.SG]</ta>
            <ta e="T1495" id="Seg_8874" s="T1494">NEG</ta>
            <ta e="T1496" id="Seg_8875" s="T1495">пить-PRS.[3SG]</ta>
            <ta e="T1497" id="Seg_8876" s="T1496">куда=INDEF</ta>
            <ta e="T1498" id="Seg_8877" s="T1497">NEG</ta>
            <ta e="T1499" id="Seg_8878" s="T1498">идти-PRS.[3SG]</ta>
            <ta e="T1500" id="Seg_8879" s="T1499">всегда</ta>
            <ta e="T1501" id="Seg_8880" s="T1500">работать-DUR.[3SG]</ta>
            <ta e="T1502" id="Seg_8881" s="T1501">там</ta>
            <ta e="T1503" id="Seg_8882" s="T1502">тоже</ta>
            <ta e="T1504" id="Seg_8883" s="T1503">мать-NOM/GEN.3SG</ta>
            <ta e="T1505" id="Seg_8884" s="T1504">NEG.EX.[3SG]</ta>
            <ta e="T1506" id="Seg_8885" s="T1505">только</ta>
            <ta e="T1507" id="Seg_8886" s="T1506">один.[NOM.SG]</ta>
            <ta e="T1508" id="Seg_8887" s="T1507">отец-NOM/GEN.3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T129" id="Seg_8888" s="T128">que</ta>
            <ta e="T130" id="Seg_8889" s="T129">pers</ta>
            <ta e="T131" id="Seg_8890" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_8891" s="T131">propr.[n:case]</ta>
            <ta e="T133" id="Seg_8892" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_8893" s="T133">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_8894" s="T135">num.[n:case]</ta>
            <ta e="T137" id="Seg_8895" s="T136">n-n:ins-n:num</ta>
            <ta e="T139" id="Seg_8896" s="T138">refl-n:case.poss</ta>
            <ta e="T140" id="Seg_8897" s="T139">n-n:case.poss</ta>
            <ta e="T141" id="Seg_8898" s="T140">v-v:n.fin</ta>
            <ta e="T143" id="Seg_8899" s="T142">adv</ta>
            <ta e="T144" id="Seg_8900" s="T143">adv</ta>
            <ta e="T145" id="Seg_8901" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_8902" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_8903" s="T146">n-n:num</ta>
            <ta e="T149" id="Seg_8904" s="T148">n.[n:case]</ta>
            <ta e="T150" id="Seg_8905" s="T149">v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_8906" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_8907" s="T152">n-n:ins-n:case.poss-n:case</ta>
            <ta e="T155" id="Seg_8908" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_8909" s="T155">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_8910" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_8911" s="T158">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_8912" s="T160">adv</ta>
            <ta e="T162" id="Seg_8913" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_8914" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_8915" s="T163">n.[n:case]</ta>
            <ta e="T166" id="Seg_8916" s="T165">ptcl</ta>
            <ta e="T230" id="Seg_8917" s="T228">n-n:num</ta>
            <ta e="T234" id="Seg_8918" s="T233">n.[n:case]</ta>
            <ta e="T249" id="Seg_8919" s="T247">n.[n:case]</ta>
            <ta e="T251" id="Seg_8920" s="T250">n-n:num</ta>
            <ta e="T252" id="Seg_8921" s="T251">n.[n:case]</ta>
            <ta e="T253" id="Seg_8922" s="T252">n.[n:case]</ta>
            <ta e="T270" id="Seg_8923" s="T268">n.[n:case]</ta>
            <ta e="T274" id="Seg_8924" s="T273">n-n:num</ta>
            <ta e="T278" id="Seg_8925" s="T277">n.[n:case]</ta>
            <ta e="T348" id="Seg_8926" s="T347">n.[n:case]</ta>
            <ta e="T353" id="Seg_8927" s="T352">n.[n:case]</ta>
            <ta e="T504" id="Seg_8928" s="T503">que</ta>
            <ta e="T505" id="Seg_8929" s="T504">dempro.[n:case]</ta>
            <ta e="T506" id="Seg_8930" s="T505">v-v:tense.[v:pn]</ta>
            <ta e="T507" id="Seg_8931" s="T506">pers</ta>
            <ta e="T510" id="Seg_8932" s="T509">v-v&gt;v.[v:pn]</ta>
            <ta e="T511" id="Seg_8933" s="T510">n.[n:case]</ta>
            <ta e="T512" id="Seg_8934" s="T511">pers</ta>
            <ta e="T513" id="Seg_8935" s="T512">pers</ta>
            <ta e="T514" id="Seg_8936" s="T513">v-v:tense-v:pn</ta>
            <ta e="T515" id="Seg_8937" s="T514">conj</ta>
            <ta e="T516" id="Seg_8938" s="T515">pers</ta>
            <ta e="T517" id="Seg_8939" s="T516">v-v&gt;v-v:pn</ta>
            <ta e="T518" id="Seg_8940" s="T517">aux-v:mood.pn</ta>
            <ta e="T519" id="Seg_8941" s="T518">v-v:n.fin</ta>
            <ta e="T521" id="Seg_8942" s="T520">v-v:mood.pn</ta>
            <ta e="T522" id="Seg_8943" s="T521">pers</ta>
            <ta e="T524" id="Seg_8944" s="T523">v-v:ins-v:mood.pn</ta>
            <ta e="T525" id="Seg_8945" s="T524">adv</ta>
            <ta e="T527" id="Seg_8946" s="T526">pers</ta>
            <ta e="T528" id="Seg_8947" s="T527">pers</ta>
            <ta e="T531" id="Seg_8948" s="T530">pers</ta>
            <ta e="T532" id="Seg_8949" s="T531">pers</ta>
            <ta e="T534" id="Seg_8950" s="T533">pers</ta>
            <ta e="T535" id="Seg_8951" s="T534">pers-n:case</ta>
            <ta e="T536" id="Seg_8952" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_8953" s="T536">v-v:tense-v:pn</ta>
            <ta e="T539" id="Seg_8954" s="T538">v-v:ins-v:mood.pn</ta>
            <ta e="T540" id="Seg_8955" s="T539">adv</ta>
            <ta e="T556" id="Seg_8956" s="T555">que</ta>
            <ta e="T557" id="Seg_8957" s="T556">dempro.[n:case]</ta>
            <ta e="T558" id="Seg_8958" s="T557">pers</ta>
            <ta e="T559" id="Seg_8959" s="T558">v-v:tense.[v:pn]</ta>
            <ta e="T560" id="Seg_8960" s="T559">v-v:tense.[v:pn]</ta>
            <ta e="T571" id="Seg_8961" s="T568">que</ta>
            <ta e="T572" id="Seg_8962" s="T571">dempro.[n:case]</ta>
            <ta e="T573" id="Seg_8963" s="T572">pers</ta>
            <ta e="T574" id="Seg_8964" s="T573">v-v:tense.[v:pn]</ta>
            <ta e="T575" id="Seg_8965" s="T574">pers</ta>
            <ta e="T576" id="Seg_8966" s="T575">v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_8967" s="T576">dempro-n:case</ta>
            <ta e="T579" id="Seg_8968" s="T578">aux-v:mood.pn</ta>
            <ta e="T580" id="Seg_8969" s="T579">v-v:n.fin</ta>
            <ta e="T581" id="Seg_8970" s="T580">pers</ta>
            <ta e="T582" id="Seg_8971" s="T581">pers</ta>
            <ta e="T583" id="Seg_8972" s="T582">pers</ta>
            <ta e="T584" id="Seg_8973" s="T583">que.[n:case]=ptcl</ta>
            <ta e="T585" id="Seg_8974" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_8975" s="T585">v-v:tense-v:pn</ta>
            <ta e="T588" id="Seg_8976" s="T587">v-v:ins-v:mood.pn</ta>
            <ta e="T589" id="Seg_8977" s="T588">adv</ta>
            <ta e="T591" id="Seg_8978" s="T590">adv</ta>
            <ta e="T592" id="Seg_8979" s="T591">dempro.[n:case]</ta>
            <ta e="T593" id="Seg_8980" s="T592">v-v&gt;v.[v:pn]</ta>
            <ta e="T594" id="Seg_8981" s="T593">adv</ta>
            <ta e="T595" id="Seg_8982" s="T594">adj</ta>
            <ta e="T596" id="Seg_8983" s="T595">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_8984" s="T597">pers</ta>
            <ta e="T599" id="Seg_8985" s="T598">v-v&gt;v-v:pn</ta>
            <ta e="T601" id="Seg_8986" s="T600">adj</ta>
            <ta e="T602" id="Seg_8987" s="T601">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_8988" s="T602">pers</ta>
            <ta e="T605" id="Seg_8989" s="T604">conj</ta>
            <ta e="T606" id="Seg_8990" s="T605">pers</ta>
            <ta e="T607" id="Seg_8991" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_8992" s="T607">adj</ta>
            <ta e="T861" id="Seg_8993" s="T860">que</ta>
            <ta e="T862" id="Seg_8994" s="T861">que</ta>
            <ta e="T863" id="Seg_8995" s="T862">v-v:tense.[v:pn]</ta>
            <ta e="T866" id="Seg_8996" s="T865">pers</ta>
            <ta e="T868" id="Seg_8997" s="T867">conj</ta>
            <ta e="T869" id="Seg_8998" s="T868">dempro.[n:case]</ta>
            <ta e="T870" id="Seg_8999" s="T869">v-v:tense.[v:pn]</ta>
            <ta e="T871" id="Seg_9000" s="T870">pers</ta>
            <ta e="T872" id="Seg_9001" s="T871">v-v:tense-v:pn</ta>
            <ta e="T873" id="Seg_9002" s="T872">pers</ta>
            <ta e="T875" id="Seg_9003" s="T874">pers</ta>
            <ta e="T876" id="Seg_9004" s="T875">v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_9005" s="T876">v-v:tense-v:pn</ta>
            <ta e="T878" id="Seg_9006" s="T877">v.[v:pn]</ta>
            <ta e="T880" id="Seg_9007" s="T879">adv</ta>
            <ta e="T881" id="Seg_9008" s="T880">v-v&gt;v.[v:pn]</ta>
            <ta e="T882" id="Seg_9009" s="T881">pers</ta>
            <ta e="T883" id="Seg_9010" s="T882">n.[n:case]</ta>
            <ta e="T884" id="Seg_9011" s="T883">pers</ta>
            <ta e="T885" id="Seg_9012" s="T884">ptcl</ta>
            <ta e="T886" id="Seg_9013" s="T885">v-v:tense-v:pn</ta>
            <ta e="T888" id="Seg_9014" s="T887">dempro.[n:case]</ta>
            <ta e="T890" id="Seg_9015" s="T889">dempro.[n:case]</ta>
            <ta e="T891" id="Seg_9016" s="T890">n-n:case</ta>
            <ta e="T892" id="Seg_9017" s="T891">v</ta>
            <ta e="T893" id="Seg_9018" s="T892">v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_9019" s="T894">pers</ta>
            <ta e="T897" id="Seg_9020" s="T896">conj</ta>
            <ta e="T898" id="Seg_9021" s="T897">pers</ta>
            <ta e="T899" id="Seg_9022" s="T898">v-v:tense-v:pn</ta>
            <ta e="T900" id="Seg_9023" s="T899">pers</ta>
            <ta e="T901" id="Seg_9024" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_9025" s="T901">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T904" id="Seg_9026" s="T903">conj</ta>
            <ta e="T905" id="Seg_9027" s="T904">pers</ta>
            <ta e="T906" id="Seg_9028" s="T905">adv</ta>
            <ta e="T907" id="Seg_9029" s="T906">v-v:tense-v:pn</ta>
            <ta e="T909" id="Seg_9030" s="T908">adv</ta>
            <ta e="T910" id="Seg_9031" s="T909">v-v:tense-v:pn</ta>
            <ta e="T911" id="Seg_9032" s="T910">conj</ta>
            <ta e="T912" id="Seg_9033" s="T911">adv</ta>
            <ta e="T913" id="Seg_9034" s="T912">v-v:tense-v:pn</ta>
            <ta e="T917" id="Seg_9035" s="T916">pers</ta>
            <ta e="T918" id="Seg_9036" s="T917">n-n:case.poss</ta>
            <ta e="T919" id="Seg_9037" s="T918">n-n:case</ta>
            <ta e="T920" id="Seg_9038" s="T919">v-v:tense.[v:pn]</ta>
            <ta e="T922" id="Seg_9039" s="T921">v-v:tense-v:pn</ta>
            <ta e="T923" id="Seg_9040" s="T922">propr-n:ins-n:case</ta>
            <ta e="T924" id="Seg_9041" s="T923">dempro.[n:case]</ta>
            <ta e="T926" id="Seg_9042" s="T924">v-v&gt;v.[v:pn]</ta>
            <ta e="T927" id="Seg_9043" s="T926">pers</ta>
            <ta e="T928" id="Seg_9044" s="T927">num.[n:case]</ta>
            <ta e="T929" id="Seg_9045" s="T928">n.[n:case]</ta>
            <ta e="T930" id="Seg_9046" s="T929">v-v:n.fin</ta>
            <ta e="T931" id="Seg_9047" s="T930">n-n:case</ta>
            <ta e="T932" id="Seg_9048" s="T931">aux-v:mood.pn</ta>
            <ta e="T933" id="Seg_9049" s="T932">v-v:mood.pn</ta>
            <ta e="T935" id="Seg_9050" s="T934">conj</ta>
            <ta e="T936" id="Seg_9051" s="T935">pers</ta>
            <ta e="T938" id="Seg_9052" s="T936">v-v:tense-v:pn</ta>
            <ta e="T939" id="Seg_9053" s="T938">ptcl</ta>
            <ta e="T940" id="Seg_9054" s="T939">ptcl</ta>
            <ta e="T941" id="Seg_9055" s="T940">v-v:n.fin</ta>
            <ta e="T942" id="Seg_9056" s="T941">n-n:case.poss</ta>
            <ta e="T943" id="Seg_9057" s="T942">refl-n:case.poss</ta>
            <ta e="T944" id="Seg_9058" s="T943">v-v:tense.[v:pn]</ta>
            <ta e="T945" id="Seg_9059" s="T944">refl-n:case.poss</ta>
            <ta e="T946" id="Seg_9060" s="T945">conj</ta>
            <ta e="T948" id="Seg_9061" s="T947">conj</ta>
            <ta e="T949" id="Seg_9062" s="T948">pers</ta>
            <ta e="T950" id="Seg_9063" s="T949">v-v:tense.[v:pn]</ta>
            <ta e="T952" id="Seg_9064" s="T951">adv</ta>
            <ta e="T953" id="Seg_9065" s="T952">v-v:tense-v:pn</ta>
            <ta e="T954" id="Seg_9066" s="T953">n-n:case</ta>
            <ta e="T955" id="Seg_9067" s="T954">v-v:tense-v:pn</ta>
            <ta e="T956" id="Seg_9068" s="T955">dempro.[n:case]</ta>
            <ta e="T957" id="Seg_9069" s="T956">v-v:tense.[v:pn]</ta>
            <ta e="T958" id="Seg_9070" s="T957">dempro-n:case</ta>
            <ta e="T959" id="Seg_9071" s="T958">n.[n:case]</ta>
            <ta e="T961" id="Seg_9072" s="T960">adv</ta>
            <ta e="T962" id="Seg_9073" s="T961">dempro.[n:case]</ta>
            <ta e="T963" id="Seg_9074" s="T962">dempro-n:case</ta>
            <ta e="T964" id="Seg_9075" s="T963">n.[n:case]</ta>
            <ta e="T965" id="Seg_9076" s="T964">v-v:tense.[v:pn]</ta>
            <ta e="T966" id="Seg_9077" s="T965">dempro.[n:case]</ta>
            <ta e="T967" id="Seg_9078" s="T966">adv</ta>
            <ta e="T968" id="Seg_9079" s="T967">v-v:tense.[v:pn]</ta>
            <ta e="T970" id="Seg_9080" s="T969">adv</ta>
            <ta e="T971" id="Seg_9081" s="T970">n.[n:case]</ta>
            <ta e="T972" id="Seg_9082" s="T971">n.[n:case]</ta>
            <ta e="T973" id="Seg_9083" s="T972">v-v:tense-v:pn</ta>
            <ta e="T974" id="Seg_9084" s="T973">n.[n:case]</ta>
            <ta e="T975" id="Seg_9085" s="T974">quant</ta>
            <ta e="T976" id="Seg_9086" s="T975">n.[n:case]</ta>
            <ta e="T977" id="Seg_9087" s="T976">v-v:tense-v:pn</ta>
            <ta e="T978" id="Seg_9088" s="T977">conj</ta>
            <ta e="T979" id="Seg_9089" s="T978">n.[n:case]</ta>
            <ta e="T980" id="Seg_9090" s="T979">v-v:tense-v:pn</ta>
            <ta e="T982" id="Seg_9091" s="T981">adv</ta>
            <ta e="T983" id="Seg_9092" s="T982">n.[n:case]</ta>
            <ta e="T984" id="Seg_9093" s="T983">v-v:tense.[v:pn]</ta>
            <ta e="T986" id="Seg_9094" s="T985">n-n:ins-n:num</ta>
            <ta e="T987" id="Seg_9095" s="T986">v-v:tense-v:pn</ta>
            <ta e="T988" id="Seg_9096" s="T987">n-n:case.poss</ta>
            <ta e="T989" id="Seg_9097" s="T988">quant</ta>
            <ta e="T990" id="Seg_9098" s="T989">v-v:tense.[v:pn]</ta>
            <ta e="T991" id="Seg_9099" s="T990">que.[n:case]</ta>
            <ta e="T992" id="Seg_9100" s="T991">v-v:tense.[v:pn]</ta>
            <ta e="T994" id="Seg_9101" s="T993">conj</ta>
            <ta e="T995" id="Seg_9102" s="T994">n.[n:case]</ta>
            <ta e="T996" id="Seg_9103" s="T995">quant</ta>
            <ta e="T997" id="Seg_9104" s="T996">v-v:tense-v:pn</ta>
            <ta e="T1525" id="Seg_9105" s="T998">propr</ta>
            <ta e="T999" id="Seg_9106" s="T1525">n-n:case</ta>
            <ta e="T1000" id="Seg_9107" s="T999">v-v:tense-v:pn</ta>
            <ta e="T1002" id="Seg_9108" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_9109" s="T1002">pers</ta>
            <ta e="T1004" id="Seg_9110" s="T1003">dempro-n:case</ta>
            <ta e="T1005" id="Seg_9111" s="T1004">v-v:tense-v:pn</ta>
            <ta e="T1006" id="Seg_9112" s="T1005">n.[n:case]</ta>
            <ta e="T1008" id="Seg_9113" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_9114" s="T1008">n.[n:case]</ta>
            <ta e="T1010" id="Seg_9115" s="T1009">dempro-n:case</ta>
            <ta e="T1012" id="Seg_9116" s="T1011">v-v:tense-v:pn</ta>
            <ta e="T1014" id="Seg_9117" s="T1013">dempro.[n:case]</ta>
            <ta e="T1522" id="Seg_9118" s="T1014">v-v:n-fin</ta>
            <ta e="T1015" id="Seg_9119" s="T1522">v-v:tense.[v:pn]</ta>
            <ta e="T1016" id="Seg_9120" s="T1015">dempro.[n:case]</ta>
            <ta e="T1017" id="Seg_9121" s="T1016">n-n:case</ta>
            <ta e="T1019" id="Seg_9122" s="T1018">adv</ta>
            <ta e="T1020" id="Seg_9123" s="T1019">que</ta>
            <ta e="T1022" id="Seg_9124" s="T1021">n.[n:case]</ta>
            <ta e="T1023" id="Seg_9125" s="T1022">que</ta>
            <ta e="T1024" id="Seg_9126" s="T1023">n.[n:case]</ta>
            <ta e="T1025" id="Seg_9127" s="T1024">v</ta>
            <ta e="T1026" id="Seg_9128" s="T1025">v-v:tense-v:pn</ta>
            <ta e="T1027" id="Seg_9129" s="T1026">refl-n:case.poss</ta>
            <ta e="T1028" id="Seg_9130" s="T1027">n-n:case.poss-n:case</ta>
            <ta e="T1030" id="Seg_9131" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_9132" s="T1030">dempro.[n:case]</ta>
            <ta e="T1032" id="Seg_9133" s="T1031">dempro.[n:case]</ta>
            <ta e="T1033" id="Seg_9134" s="T1032">n-n:case</ta>
            <ta e="T1034" id="Seg_9135" s="T1033">n.[n:case]</ta>
            <ta e="T1036" id="Seg_9136" s="T1035">dempro-n:case</ta>
            <ta e="T1037" id="Seg_9137" s="T1036">dempro-n:case</ta>
            <ta e="T1038" id="Seg_9138" s="T1037">ptcl</ta>
            <ta e="T1039" id="Seg_9139" s="T1038">v-v:tense.[v:pn]</ta>
            <ta e="T1040" id="Seg_9140" s="T1039">adv</ta>
            <ta e="T1041" id="Seg_9141" s="T1040">dempro.[n:case]</ta>
            <ta e="T1042" id="Seg_9142" s="T1041">adv</ta>
            <ta e="T1044" id="Seg_9143" s="T1042">v-v&gt;v.[v:pn]</ta>
            <ta e="T1046" id="Seg_9144" s="T1045">adj</ta>
            <ta e="T1048" id="Seg_9145" s="T1047">n.[n:case]</ta>
            <ta e="T1049" id="Seg_9146" s="T1048">v-v&gt;v-v:pn</ta>
            <ta e="T1051" id="Seg_9147" s="T1050">pers</ta>
            <ta e="T1053" id="Seg_9148" s="T1052">dempro-n:case</ta>
            <ta e="T1055" id="Seg_9149" s="T1053">v-v:n.fin</ta>
            <ta e="T1056" id="Seg_9150" s="T1055">v-v:mood.pn</ta>
            <ta e="T1057" id="Seg_9151" s="T1056">n-n:case-n:case.poss</ta>
            <ta e="T1059" id="Seg_9152" s="T1058">adv</ta>
            <ta e="T1060" id="Seg_9153" s="T1059">dempro.[n:case]</ta>
            <ta e="T1061" id="Seg_9154" s="T1060">v-v&gt;v.[v:pn]</ta>
            <ta e="T1062" id="Seg_9155" s="T1061">pers</ta>
            <ta e="T1063" id="Seg_9156" s="T1062">adv</ta>
            <ta e="T1064" id="Seg_9157" s="T1063">v-v:tense-v:pn</ta>
            <ta e="T1066" id="Seg_9158" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_9159" s="T1066">que.[n:case]</ta>
            <ta e="T1068" id="Seg_9160" s="T1067">adv</ta>
            <ta e="T1069" id="Seg_9161" s="T1068">v-v:tense-v:pn</ta>
            <ta e="T1071" id="Seg_9162" s="T1070">conj</ta>
            <ta e="T1072" id="Seg_9163" s="T1071">dempro.[n:case]</ta>
            <ta e="T1073" id="Seg_9164" s="T1072">adv</ta>
            <ta e="T1074" id="Seg_9165" s="T1073">v-v&gt;v.[v:pn]</ta>
            <ta e="T1075" id="Seg_9166" s="T1074">n.[n:case]</ta>
            <ta e="T1076" id="Seg_9167" s="T1075">v.[v:pn]</ta>
            <ta e="T1078" id="Seg_9168" s="T1077">pers</ta>
            <ta e="T1079" id="Seg_9169" s="T1078">adv</ta>
            <ta e="T1080" id="Seg_9170" s="T1079">v-v:tense-v:pn</ta>
            <ta e="T1081" id="Seg_9171" s="T1080">num.[n:case]</ta>
            <ta e="T1082" id="Seg_9172" s="T1081">num.[n:case]</ta>
            <ta e="T1083" id="Seg_9173" s="T1082">num.[n:case]</ta>
            <ta e="T1084" id="Seg_9174" s="T1083">num.[n:case]</ta>
            <ta e="T1085" id="Seg_9175" s="T1084">n.[n:case]</ta>
            <ta e="T1087" id="Seg_9176" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_9177" s="T1087">n-n:case.poss</ta>
            <ta e="T1089" id="Seg_9178" s="T1088">num.[n:case]</ta>
            <ta e="T1090" id="Seg_9179" s="T1089">num.[n:case]</ta>
            <ta e="T1091" id="Seg_9180" s="T1090">num.[n:case]</ta>
            <ta e="T1092" id="Seg_9181" s="T1091">v-v:tense.[v:pn]</ta>
            <ta e="T1094" id="Seg_9182" s="T1093">adv</ta>
            <ta e="T1095" id="Seg_9183" s="T1094">dempro.[n:case]</ta>
            <ta e="T1096" id="Seg_9184" s="T1095">v-v:tense.[v:pn]</ta>
            <ta e="T1097" id="Seg_9185" s="T1096">num.[n:case]</ta>
            <ta e="T1099" id="Seg_9186" s="T1098">dempro.[n:case]</ta>
            <ta e="T1100" id="Seg_9187" s="T1099">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1102" id="Seg_9188" s="T1101">adv</ta>
            <ta e="T1103" id="Seg_9189" s="T1102">dempro.[n:case]</ta>
            <ta e="T1104" id="Seg_9190" s="T1103">dempro-n:case</ta>
            <ta e="T1105" id="Seg_9191" s="T1104">dempro.[n:case]</ta>
            <ta e="T1106" id="Seg_9192" s="T1105">v-v:tense.[v:pn]</ta>
            <ta e="T1107" id="Seg_9193" s="T1106">v-v:tense.[v:pn]</ta>
            <ta e="T1109" id="Seg_9194" s="T1108">dempro.[n:case]</ta>
            <ta e="T1110" id="Seg_9195" s="T1109">ptcl</ta>
            <ta e="T1111" id="Seg_9196" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_9197" s="T1111">ptcl</ta>
            <ta e="T1113" id="Seg_9198" s="T1112">v-v:tense.[v:pn]</ta>
            <ta e="T1115" id="Seg_9199" s="T1114">adv</ta>
            <ta e="T1116" id="Seg_9200" s="T1115">dempro.[n:case]</ta>
            <ta e="T1523" id="Seg_9201" s="T1116">v-v:n-fin</ta>
            <ta e="T1117" id="Seg_9202" s="T1523">v-v:tense.[v:pn]</ta>
            <ta e="T1120" id="Seg_9203" s="T1119">que</ta>
            <ta e="T1121" id="Seg_9204" s="T1120">propr</ta>
            <ta e="T1122" id="Seg_9205" s="T1121">v-v:tense.[v:pn]</ta>
            <ta e="T1123" id="Seg_9206" s="T1122">dempro.[n:case]</ta>
            <ta e="T1124" id="Seg_9207" s="T1123">n-n:case</ta>
            <ta e="T1126" id="Seg_9208" s="T1125">adv</ta>
            <ta e="T1127" id="Seg_9209" s="T1126">n-n:case</ta>
            <ta e="T1130" id="Seg_9210" s="T1129">n-n:case</ta>
            <ta e="T1132" id="Seg_9211" s="T1131">n.[n:case]</ta>
            <ta e="T1133" id="Seg_9212" s="T1132">v-v:tense.[v:pn]</ta>
            <ta e="T1134" id="Seg_9213" s="T1133">n.[n:case]</ta>
            <ta e="T1135" id="Seg_9214" s="T1134">v-v:tense.[v:pn]</ta>
            <ta e="T1136" id="Seg_9215" s="T1135">ptcl</ta>
            <ta e="T1137" id="Seg_9216" s="T1136">que.[n:case]</ta>
            <ta e="T1138" id="Seg_9217" s="T1137">v-v:tense.[v:pn]</ta>
            <ta e="T1139" id="Seg_9218" s="T1138">v-v&gt;v.[v:pn]</ta>
            <ta e="T1141" id="Seg_9219" s="T1140">num.[n:case]</ta>
            <ta e="T1143" id="Seg_9220" s="T1142">num.[n:case]</ta>
            <ta e="T1144" id="Seg_9221" s="T1143">num.[n:case]</ta>
            <ta e="T1145" id="Seg_9222" s="T1144">conj</ta>
            <ta e="T1146" id="Seg_9223" s="T1145">num.[n:case]</ta>
            <ta e="T1147" id="Seg_9224" s="T1146">n.[n:case]</ta>
            <ta e="T1148" id="Seg_9225" s="T1147">v-v:tense-v:pn</ta>
            <ta e="T1392" id="Seg_9226" s="T1391">pers</ta>
            <ta e="T1393" id="Seg_9227" s="T1392">n-n:case</ta>
            <ta e="T1394" id="Seg_9228" s="T1393">n-n:case.poss</ta>
            <ta e="T1395" id="Seg_9229" s="T1394">n-n:case</ta>
            <ta e="T1524" id="Seg_9230" s="T1395">v-v:n-fin</ta>
            <ta e="T1396" id="Seg_9231" s="T1524">v-v:tense.[v:pn]</ta>
            <ta e="T1398" id="Seg_9232" s="T1397">conj</ta>
            <ta e="T1399" id="Seg_9233" s="T1398">adv</ta>
            <ta e="T1400" id="Seg_9234" s="T1399">num.[n:case]</ta>
            <ta e="T1401" id="Seg_9235" s="T1400">n-n:num</ta>
            <ta e="T1402" id="Seg_9236" s="T1401">v-v:tense-v:pn</ta>
            <ta e="T1404" id="Seg_9237" s="T1403">adv</ta>
            <ta e="T1405" id="Seg_9238" s="T1404">dempro.[n:case]</ta>
            <ta e="T1406" id="Seg_9239" s="T1405">dempro-n:num</ta>
            <ta e="T1407" id="Seg_9240" s="T1406">v-v:tense-v:pn</ta>
            <ta e="T1408" id="Seg_9241" s="T1407">n.[n:case]</ta>
            <ta e="T1409" id="Seg_9242" s="T1408">n-n:case</ta>
            <ta e="T1410" id="Seg_9243" s="T1409">adv</ta>
            <ta e="T1412" id="Seg_9244" s="T1411">conj</ta>
            <ta e="T1413" id="Seg_9245" s="T1412">v-v:tense.[v:pn]</ta>
            <ta e="T1414" id="Seg_9246" s="T1413">n-n:num</ta>
            <ta e="T1416" id="Seg_9247" s="T1415">adv</ta>
            <ta e="T1417" id="Seg_9248" s="T1416">dempro-n:num</ta>
            <ta e="T1418" id="Seg_9249" s="T1417">n.[n:case]</ta>
            <ta e="T1419" id="Seg_9250" s="T1418">v-v:tense-v:pn</ta>
            <ta e="T1420" id="Seg_9251" s="T1419">que</ta>
            <ta e="T1421" id="Seg_9252" s="T1420">n-n&gt;adj.[n:case]</ta>
            <ta e="T1422" id="Seg_9253" s="T1421">n.[n:case]</ta>
            <ta e="T1424" id="Seg_9254" s="T1423">adv</ta>
            <ta e="T1425" id="Seg_9255" s="T1424">v-v:tense-v:pn</ta>
            <ta e="T1426" id="Seg_9256" s="T1425">dempro.[n:case]</ta>
            <ta e="T1427" id="Seg_9257" s="T1426">adv</ta>
            <ta e="T1428" id="Seg_9258" s="T1427">n-n:case</ta>
            <ta e="T1429" id="Seg_9259" s="T1428">adv</ta>
            <ta e="T1430" id="Seg_9260" s="T1429">ptcl</ta>
            <ta e="T1431" id="Seg_9261" s="T1430">v-v:tense-v:pn</ta>
            <ta e="T1432" id="Seg_9262" s="T1431">n.[n:case]</ta>
            <ta e="T1433" id="Seg_9263" s="T1432">n.[n:case]</ta>
            <ta e="T1435" id="Seg_9264" s="T1434">n-n&gt;adj.[n:case]</ta>
            <ta e="T1436" id="Seg_9265" s="T1435">n.[n:case]</ta>
            <ta e="T1438" id="Seg_9266" s="T1437">adv</ta>
            <ta e="T1439" id="Seg_9267" s="T1438">dempro.[n:case]</ta>
            <ta e="T1440" id="Seg_9268" s="T1439">v-v:tense.[v:pn]</ta>
            <ta e="T1441" id="Seg_9269" s="T1440">n-n:case.poss</ta>
            <ta e="T1442" id="Seg_9270" s="T1441">n-n:case</ta>
            <ta e="T1444" id="Seg_9271" s="T1443">dempro</ta>
            <ta e="T1445" id="Seg_9272" s="T1444">adv</ta>
            <ta e="T1446" id="Seg_9273" s="T1445">dempro.[n:case]</ta>
            <ta e="T1447" id="Seg_9274" s="T1446">adv</ta>
            <ta e="T1448" id="Seg_9275" s="T1447">v-v:tense.[v:pn]</ta>
            <ta e="T1450" id="Seg_9276" s="T1449">adv</ta>
            <ta e="T1451" id="Seg_9277" s="T1450">n.[n:case]</ta>
            <ta e="T1453" id="Seg_9278" s="T1451">v-v:tense-v:pn</ta>
            <ta e="T1454" id="Seg_9279" s="T1453">pers</ta>
            <ta e="T1455" id="Seg_9280" s="T1454">n.[n:case]</ta>
            <ta e="T1456" id="Seg_9281" s="T1455">n-n:case</ta>
            <ta e="T1457" id="Seg_9282" s="T1456">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1459" id="Seg_9283" s="T1458">adv</ta>
            <ta e="T1460" id="Seg_9284" s="T1459">dempro.[n:case]</ta>
            <ta e="T1461" id="Seg_9285" s="T1460">v-v:tense.[v:pn]</ta>
            <ta e="T1463" id="Seg_9286" s="T1462">dempro-n:case</ta>
            <ta e="T1464" id="Seg_9287" s="T1463">n-n:case</ta>
            <ta e="T1465" id="Seg_9288" s="T1464">v-v:tense-v:pn</ta>
            <ta e="T1467" id="Seg_9289" s="T1466">adv</ta>
            <ta e="T1468" id="Seg_9290" s="T1467">adv</ta>
            <ta e="T1469" id="Seg_9291" s="T1468">dempro-n:case</ta>
            <ta e="T1470" id="Seg_9292" s="T1469">n.[n:case]</ta>
            <ta e="T1471" id="Seg_9293" s="T1470">v-v:tense.[v:pn]</ta>
            <ta e="T1473" id="Seg_9294" s="T1472">adv</ta>
            <ta e="T1474" id="Seg_9295" s="T1473">v-v:tense.[v:pn]</ta>
            <ta e="T1475" id="Seg_9296" s="T1474">n-n:case.poss</ta>
            <ta e="T1477" id="Seg_9297" s="T1476">conj</ta>
            <ta e="T1478" id="Seg_9298" s="T1477">adv</ta>
            <ta e="T1479" id="Seg_9299" s="T1478">v-v&gt;v-v:pn</ta>
            <ta e="T1480" id="Seg_9300" s="T1479">conj</ta>
            <ta e="T1481" id="Seg_9301" s="T1480">adv</ta>
            <ta e="T1482" id="Seg_9302" s="T1481">num.[n:case]</ta>
            <ta e="T1483" id="Seg_9303" s="T1482">n-n:num-n:case</ta>
            <ta e="T1484" id="Seg_9304" s="T1483">v-v:tense-v:pn</ta>
            <ta e="T1486" id="Seg_9305" s="T1485">dempro.[n:case]</ta>
            <ta e="T1487" id="Seg_9306" s="T1486">v-v:tense.[v:pn]</ta>
            <ta e="T1488" id="Seg_9307" s="T1487">dempro-n:case</ta>
            <ta e="T1490" id="Seg_9308" s="T1489">adv</ta>
            <ta e="T1491" id="Seg_9309" s="T1490">adj</ta>
            <ta e="T1492" id="Seg_9310" s="T1491">n.[n:case]</ta>
            <ta e="T1494" id="Seg_9311" s="T1493">n.[n:case]</ta>
            <ta e="T1495" id="Seg_9312" s="T1494">ptcl</ta>
            <ta e="T1496" id="Seg_9313" s="T1495">v-v:tense.[v:pn]</ta>
            <ta e="T1497" id="Seg_9314" s="T1496">que=ptcl</ta>
            <ta e="T1498" id="Seg_9315" s="T1497">ptcl</ta>
            <ta e="T1499" id="Seg_9316" s="T1498">v-v:tense.[v:pn]</ta>
            <ta e="T1500" id="Seg_9317" s="T1499">adv</ta>
            <ta e="T1501" id="Seg_9318" s="T1500">v-v&gt;v.[v:pn]</ta>
            <ta e="T1502" id="Seg_9319" s="T1501">adv</ta>
            <ta e="T1503" id="Seg_9320" s="T1502">ptcl</ta>
            <ta e="T1504" id="Seg_9321" s="T1503">n-n:case.poss</ta>
            <ta e="T1505" id="Seg_9322" s="T1504">v.[v:pn]</ta>
            <ta e="T1506" id="Seg_9323" s="T1505">adv</ta>
            <ta e="T1507" id="Seg_9324" s="T1506">num.[n:case]</ta>
            <ta e="T1508" id="Seg_9325" s="T1507">n-n:case.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T129" id="Seg_9326" s="T128">que</ta>
            <ta e="T130" id="Seg_9327" s="T129">pers</ta>
            <ta e="T131" id="Seg_9328" s="T130">v</ta>
            <ta e="T132" id="Seg_9329" s="T131">propr</ta>
            <ta e="T133" id="Seg_9330" s="T132">v</ta>
            <ta e="T134" id="Seg_9331" s="T133">v</ta>
            <ta e="T136" id="Seg_9332" s="T135">num</ta>
            <ta e="T137" id="Seg_9333" s="T136">n</ta>
            <ta e="T139" id="Seg_9334" s="T138">refl</ta>
            <ta e="T140" id="Seg_9335" s="T139">n</ta>
            <ta e="T141" id="Seg_9336" s="T140">v</ta>
            <ta e="T143" id="Seg_9337" s="T142">adv</ta>
            <ta e="T144" id="Seg_9338" s="T143">adv</ta>
            <ta e="T145" id="Seg_9339" s="T144">v</ta>
            <ta e="T146" id="Seg_9340" s="T145">v</ta>
            <ta e="T147" id="Seg_9341" s="T146">n</ta>
            <ta e="T149" id="Seg_9342" s="T148">n</ta>
            <ta e="T150" id="Seg_9343" s="T149">v</ta>
            <ta e="T152" id="Seg_9344" s="T151">n</ta>
            <ta e="T153" id="Seg_9345" s="T152">n</ta>
            <ta e="T155" id="Seg_9346" s="T154">n</ta>
            <ta e="T156" id="Seg_9347" s="T155">v</ta>
            <ta e="T158" id="Seg_9348" s="T157">n</ta>
            <ta e="T159" id="Seg_9349" s="T158">v</ta>
            <ta e="T161" id="Seg_9350" s="T160">adv</ta>
            <ta e="T162" id="Seg_9351" s="T161">n</ta>
            <ta e="T163" id="Seg_9352" s="T162">v</ta>
            <ta e="T164" id="Seg_9353" s="T163">n</ta>
            <ta e="T166" id="Seg_9354" s="T165">ptcl</ta>
            <ta e="T230" id="Seg_9355" s="T228">n</ta>
            <ta e="T234" id="Seg_9356" s="T233">n</ta>
            <ta e="T249" id="Seg_9357" s="T247">n</ta>
            <ta e="T251" id="Seg_9358" s="T250">n</ta>
            <ta e="T252" id="Seg_9359" s="T251">n</ta>
            <ta e="T253" id="Seg_9360" s="T252">n</ta>
            <ta e="T270" id="Seg_9361" s="T268">n</ta>
            <ta e="T274" id="Seg_9362" s="T273">n</ta>
            <ta e="T278" id="Seg_9363" s="T277">n</ta>
            <ta e="T348" id="Seg_9364" s="T347">n</ta>
            <ta e="T353" id="Seg_9365" s="T352">n</ta>
            <ta e="T504" id="Seg_9366" s="T503">que</ta>
            <ta e="T505" id="Seg_9367" s="T504">dempro</ta>
            <ta e="T506" id="Seg_9368" s="T505">v</ta>
            <ta e="T507" id="Seg_9369" s="T506">pers</ta>
            <ta e="T510" id="Seg_9370" s="T509">v</ta>
            <ta e="T511" id="Seg_9371" s="T510">n</ta>
            <ta e="T512" id="Seg_9372" s="T511">pers</ta>
            <ta e="T513" id="Seg_9373" s="T512">pers</ta>
            <ta e="T514" id="Seg_9374" s="T513">v</ta>
            <ta e="T515" id="Seg_9375" s="T514">conj</ta>
            <ta e="T516" id="Seg_9376" s="T515">pers</ta>
            <ta e="T517" id="Seg_9377" s="T516">v</ta>
            <ta e="T518" id="Seg_9378" s="T517">aux</ta>
            <ta e="T519" id="Seg_9379" s="T518">v</ta>
            <ta e="T521" id="Seg_9380" s="T520">v</ta>
            <ta e="T522" id="Seg_9381" s="T521">pers</ta>
            <ta e="T524" id="Seg_9382" s="T523">v</ta>
            <ta e="T525" id="Seg_9383" s="T524">adv</ta>
            <ta e="T527" id="Seg_9384" s="T526">pers</ta>
            <ta e="T528" id="Seg_9385" s="T527">pers</ta>
            <ta e="T531" id="Seg_9386" s="T530">pers</ta>
            <ta e="T532" id="Seg_9387" s="T531">pers</ta>
            <ta e="T534" id="Seg_9388" s="T533">pers</ta>
            <ta e="T535" id="Seg_9389" s="T534">pers</ta>
            <ta e="T536" id="Seg_9390" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_9391" s="T536">v</ta>
            <ta e="T539" id="Seg_9392" s="T538">v</ta>
            <ta e="T540" id="Seg_9393" s="T539">adv</ta>
            <ta e="T556" id="Seg_9394" s="T555">que</ta>
            <ta e="T557" id="Seg_9395" s="T556">dempro</ta>
            <ta e="T558" id="Seg_9396" s="T557">pers</ta>
            <ta e="T559" id="Seg_9397" s="T558">v</ta>
            <ta e="T560" id="Seg_9398" s="T559">v</ta>
            <ta e="T571" id="Seg_9399" s="T568">que</ta>
            <ta e="T572" id="Seg_9400" s="T571">dempro</ta>
            <ta e="T573" id="Seg_9401" s="T572">pers</ta>
            <ta e="T574" id="Seg_9402" s="T573">v</ta>
            <ta e="T575" id="Seg_9403" s="T574">pers</ta>
            <ta e="T576" id="Seg_9404" s="T575">v</ta>
            <ta e="T578" id="Seg_9405" s="T576">dempro</ta>
            <ta e="T579" id="Seg_9406" s="T578">aux</ta>
            <ta e="T580" id="Seg_9407" s="T579">v</ta>
            <ta e="T581" id="Seg_9408" s="T580">pers</ta>
            <ta e="T582" id="Seg_9409" s="T581">pers</ta>
            <ta e="T583" id="Seg_9410" s="T582">pers</ta>
            <ta e="T584" id="Seg_9411" s="T583">que</ta>
            <ta e="T585" id="Seg_9412" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_9413" s="T585">v</ta>
            <ta e="T588" id="Seg_9414" s="T587">v</ta>
            <ta e="T589" id="Seg_9415" s="T588">adv</ta>
            <ta e="T591" id="Seg_9416" s="T590">adv</ta>
            <ta e="T592" id="Seg_9417" s="T591">dempro</ta>
            <ta e="T593" id="Seg_9418" s="T592">v</ta>
            <ta e="T594" id="Seg_9419" s="T593">adv</ta>
            <ta e="T595" id="Seg_9420" s="T594">adj</ta>
            <ta e="T596" id="Seg_9421" s="T595">v</ta>
            <ta e="T598" id="Seg_9422" s="T597">pers</ta>
            <ta e="T599" id="Seg_9423" s="T598">v</ta>
            <ta e="T601" id="Seg_9424" s="T600">adj</ta>
            <ta e="T602" id="Seg_9425" s="T601">v</ta>
            <ta e="T603" id="Seg_9426" s="T602">pers</ta>
            <ta e="T605" id="Seg_9427" s="T604">conj</ta>
            <ta e="T606" id="Seg_9428" s="T605">pers</ta>
            <ta e="T607" id="Seg_9429" s="T606">v</ta>
            <ta e="T608" id="Seg_9430" s="T607">adj</ta>
            <ta e="T861" id="Seg_9431" s="T860">que</ta>
            <ta e="T862" id="Seg_9432" s="T861">que</ta>
            <ta e="T863" id="Seg_9433" s="T862">v</ta>
            <ta e="T866" id="Seg_9434" s="T865">pers</ta>
            <ta e="T868" id="Seg_9435" s="T867">conj</ta>
            <ta e="T869" id="Seg_9436" s="T868">dempro</ta>
            <ta e="T870" id="Seg_9437" s="T869">v</ta>
            <ta e="T871" id="Seg_9438" s="T870">pers</ta>
            <ta e="T872" id="Seg_9439" s="T871">v</ta>
            <ta e="T873" id="Seg_9440" s="T872">pers</ta>
            <ta e="T875" id="Seg_9441" s="T874">pers</ta>
            <ta e="T876" id="Seg_9442" s="T875">v</ta>
            <ta e="T877" id="Seg_9443" s="T876">v</ta>
            <ta e="T878" id="Seg_9444" s="T877">v</ta>
            <ta e="T880" id="Seg_9445" s="T879">adv</ta>
            <ta e="T881" id="Seg_9446" s="T880">v</ta>
            <ta e="T882" id="Seg_9447" s="T881">pers</ta>
            <ta e="T883" id="Seg_9448" s="T882">n</ta>
            <ta e="T884" id="Seg_9449" s="T883">pers</ta>
            <ta e="T885" id="Seg_9450" s="T884">ptcl</ta>
            <ta e="T886" id="Seg_9451" s="T885">v</ta>
            <ta e="T888" id="Seg_9452" s="T887">dempro</ta>
            <ta e="T890" id="Seg_9453" s="T889">dempro</ta>
            <ta e="T891" id="Seg_9454" s="T890">n</ta>
            <ta e="T892" id="Seg_9455" s="T891">v</ta>
            <ta e="T893" id="Seg_9456" s="T892">v</ta>
            <ta e="T895" id="Seg_9457" s="T894">pers</ta>
            <ta e="T897" id="Seg_9458" s="T896">conj</ta>
            <ta e="T898" id="Seg_9459" s="T897">pers</ta>
            <ta e="T899" id="Seg_9460" s="T898">v</ta>
            <ta e="T900" id="Seg_9461" s="T899">pers</ta>
            <ta e="T901" id="Seg_9462" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_9463" s="T901">v</ta>
            <ta e="T904" id="Seg_9464" s="T903">conj</ta>
            <ta e="T905" id="Seg_9465" s="T904">pers</ta>
            <ta e="T906" id="Seg_9466" s="T905">adv</ta>
            <ta e="T907" id="Seg_9467" s="T906">v</ta>
            <ta e="T909" id="Seg_9468" s="T908">adv</ta>
            <ta e="T910" id="Seg_9469" s="T909">v</ta>
            <ta e="T911" id="Seg_9470" s="T910">conj</ta>
            <ta e="T912" id="Seg_9471" s="T911">adv</ta>
            <ta e="T913" id="Seg_9472" s="T912">v</ta>
            <ta e="T917" id="Seg_9473" s="T916">pers</ta>
            <ta e="T918" id="Seg_9474" s="T917">n</ta>
            <ta e="T919" id="Seg_9475" s="T918">n</ta>
            <ta e="T920" id="Seg_9476" s="T919">v</ta>
            <ta e="T922" id="Seg_9477" s="T921">v</ta>
            <ta e="T923" id="Seg_9478" s="T922">propr</ta>
            <ta e="T924" id="Seg_9479" s="T923">dempro</ta>
            <ta e="T926" id="Seg_9480" s="T924">v</ta>
            <ta e="T927" id="Seg_9481" s="T926">pers</ta>
            <ta e="T928" id="Seg_9482" s="T927">num</ta>
            <ta e="T929" id="Seg_9483" s="T928">n</ta>
            <ta e="T930" id="Seg_9484" s="T929">v</ta>
            <ta e="T931" id="Seg_9485" s="T930">n</ta>
            <ta e="T932" id="Seg_9486" s="T931">aux</ta>
            <ta e="T933" id="Seg_9487" s="T932">v</ta>
            <ta e="T935" id="Seg_9488" s="T934">conj</ta>
            <ta e="T936" id="Seg_9489" s="T935">pers</ta>
            <ta e="T938" id="Seg_9490" s="T936">v</ta>
            <ta e="T939" id="Seg_9491" s="T938">ptcl</ta>
            <ta e="T940" id="Seg_9492" s="T939">ptcl</ta>
            <ta e="T941" id="Seg_9493" s="T940">v</ta>
            <ta e="T942" id="Seg_9494" s="T941">n</ta>
            <ta e="T943" id="Seg_9495" s="T942">refl</ta>
            <ta e="T944" id="Seg_9496" s="T943">v</ta>
            <ta e="T945" id="Seg_9497" s="T944">refl</ta>
            <ta e="T946" id="Seg_9498" s="T945">conj</ta>
            <ta e="T948" id="Seg_9499" s="T947">conj</ta>
            <ta e="T949" id="Seg_9500" s="T948">pers</ta>
            <ta e="T950" id="Seg_9501" s="T949">v</ta>
            <ta e="T952" id="Seg_9502" s="T951">adv</ta>
            <ta e="T953" id="Seg_9503" s="T952">v</ta>
            <ta e="T954" id="Seg_9504" s="T953">n</ta>
            <ta e="T955" id="Seg_9505" s="T954">v</ta>
            <ta e="T956" id="Seg_9506" s="T955">dempro</ta>
            <ta e="T957" id="Seg_9507" s="T956">v</ta>
            <ta e="T958" id="Seg_9508" s="T957">dempro</ta>
            <ta e="T959" id="Seg_9509" s="T958">n</ta>
            <ta e="T961" id="Seg_9510" s="T960">adv</ta>
            <ta e="T962" id="Seg_9511" s="T961">dempro</ta>
            <ta e="T963" id="Seg_9512" s="T962">dempro</ta>
            <ta e="T964" id="Seg_9513" s="T963">n</ta>
            <ta e="T965" id="Seg_9514" s="T964">v</ta>
            <ta e="T966" id="Seg_9515" s="T965">dempro</ta>
            <ta e="T967" id="Seg_9516" s="T966">adv</ta>
            <ta e="T968" id="Seg_9517" s="T967">v</ta>
            <ta e="T970" id="Seg_9518" s="T969">adv</ta>
            <ta e="T971" id="Seg_9519" s="T970">n</ta>
            <ta e="T972" id="Seg_9520" s="T971">n</ta>
            <ta e="T973" id="Seg_9521" s="T972">v</ta>
            <ta e="T974" id="Seg_9522" s="T973">n</ta>
            <ta e="T975" id="Seg_9523" s="T974">quant</ta>
            <ta e="T976" id="Seg_9524" s="T975">n</ta>
            <ta e="T977" id="Seg_9525" s="T976">v</ta>
            <ta e="T978" id="Seg_9526" s="T977">conj</ta>
            <ta e="T979" id="Seg_9527" s="T978">n</ta>
            <ta e="T980" id="Seg_9528" s="T979">v</ta>
            <ta e="T982" id="Seg_9529" s="T981">adv</ta>
            <ta e="T983" id="Seg_9530" s="T982">n</ta>
            <ta e="T984" id="Seg_9531" s="T983">v</ta>
            <ta e="T986" id="Seg_9532" s="T985">n</ta>
            <ta e="T987" id="Seg_9533" s="T986">v</ta>
            <ta e="T988" id="Seg_9534" s="T987">n</ta>
            <ta e="T989" id="Seg_9535" s="T988">quant</ta>
            <ta e="T990" id="Seg_9536" s="T989">v</ta>
            <ta e="T991" id="Seg_9537" s="T990">que</ta>
            <ta e="T992" id="Seg_9538" s="T991">v</ta>
            <ta e="T994" id="Seg_9539" s="T993">conj</ta>
            <ta e="T995" id="Seg_9540" s="T994">n</ta>
            <ta e="T996" id="Seg_9541" s="T995">quant</ta>
            <ta e="T997" id="Seg_9542" s="T996">v</ta>
            <ta e="T1525" id="Seg_9543" s="T998">propr</ta>
            <ta e="T999" id="Seg_9544" s="T1525">n</ta>
            <ta e="T1000" id="Seg_9545" s="T999">v</ta>
            <ta e="T1002" id="Seg_9546" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_9547" s="T1002">pers</ta>
            <ta e="T1004" id="Seg_9548" s="T1003">dempro</ta>
            <ta e="T1005" id="Seg_9549" s="T1004">v</ta>
            <ta e="T1006" id="Seg_9550" s="T1005">n</ta>
            <ta e="T1008" id="Seg_9551" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_9552" s="T1008">n</ta>
            <ta e="T1010" id="Seg_9553" s="T1009">dempro</ta>
            <ta e="T1012" id="Seg_9554" s="T1011">v</ta>
            <ta e="T1014" id="Seg_9555" s="T1013">dempro</ta>
            <ta e="T1522" id="Seg_9556" s="T1014">v</ta>
            <ta e="T1015" id="Seg_9557" s="T1522">v</ta>
            <ta e="T1016" id="Seg_9558" s="T1015">dempro</ta>
            <ta e="T1017" id="Seg_9559" s="T1016">n</ta>
            <ta e="T1019" id="Seg_9560" s="T1018">adv</ta>
            <ta e="T1020" id="Seg_9561" s="T1019">que</ta>
            <ta e="T1022" id="Seg_9562" s="T1021">n</ta>
            <ta e="T1023" id="Seg_9563" s="T1022">que</ta>
            <ta e="T1024" id="Seg_9564" s="T1023">n</ta>
            <ta e="T1025" id="Seg_9565" s="T1024">v</ta>
            <ta e="T1026" id="Seg_9566" s="T1025">v</ta>
            <ta e="T1027" id="Seg_9567" s="T1026">refl</ta>
            <ta e="T1028" id="Seg_9568" s="T1027">n</ta>
            <ta e="T1030" id="Seg_9569" s="T1029">adv</ta>
            <ta e="T1031" id="Seg_9570" s="T1030">dempro</ta>
            <ta e="T1032" id="Seg_9571" s="T1031">dempro</ta>
            <ta e="T1033" id="Seg_9572" s="T1032">n</ta>
            <ta e="T1034" id="Seg_9573" s="T1033">n</ta>
            <ta e="T1036" id="Seg_9574" s="T1035">dempro</ta>
            <ta e="T1037" id="Seg_9575" s="T1036">dempro</ta>
            <ta e="T1038" id="Seg_9576" s="T1037">ptcl</ta>
            <ta e="T1039" id="Seg_9577" s="T1038">v</ta>
            <ta e="T1040" id="Seg_9578" s="T1039">adv</ta>
            <ta e="T1041" id="Seg_9579" s="T1040">dempro</ta>
            <ta e="T1042" id="Seg_9580" s="T1041">adv</ta>
            <ta e="T1044" id="Seg_9581" s="T1042">v</ta>
            <ta e="T1046" id="Seg_9582" s="T1045">adj</ta>
            <ta e="T1048" id="Seg_9583" s="T1047">n</ta>
            <ta e="T1049" id="Seg_9584" s="T1048">v</ta>
            <ta e="T1051" id="Seg_9585" s="T1050">pers</ta>
            <ta e="T1053" id="Seg_9586" s="T1052">dempro</ta>
            <ta e="T1055" id="Seg_9587" s="T1053">v</ta>
            <ta e="T1056" id="Seg_9588" s="T1055">v</ta>
            <ta e="T1057" id="Seg_9589" s="T1056">n</ta>
            <ta e="T1059" id="Seg_9590" s="T1058">adv</ta>
            <ta e="T1060" id="Seg_9591" s="T1059">dempro</ta>
            <ta e="T1061" id="Seg_9592" s="T1060">v</ta>
            <ta e="T1062" id="Seg_9593" s="T1061">pers</ta>
            <ta e="T1063" id="Seg_9594" s="T1062">adv</ta>
            <ta e="T1064" id="Seg_9595" s="T1063">v</ta>
            <ta e="T1066" id="Seg_9596" s="T1065">conj</ta>
            <ta e="T1067" id="Seg_9597" s="T1066">que</ta>
            <ta e="T1068" id="Seg_9598" s="T1067">adv</ta>
            <ta e="T1069" id="Seg_9599" s="T1068">v</ta>
            <ta e="T1071" id="Seg_9600" s="T1070">conj</ta>
            <ta e="T1072" id="Seg_9601" s="T1071">dempro</ta>
            <ta e="T1073" id="Seg_9602" s="T1072">adv</ta>
            <ta e="T1074" id="Seg_9603" s="T1073">v</ta>
            <ta e="T1075" id="Seg_9604" s="T1074">n</ta>
            <ta e="T1076" id="Seg_9605" s="T1075">v</ta>
            <ta e="T1078" id="Seg_9606" s="T1077">pers</ta>
            <ta e="T1079" id="Seg_9607" s="T1078">adv</ta>
            <ta e="T1080" id="Seg_9608" s="T1079">v</ta>
            <ta e="T1081" id="Seg_9609" s="T1080">num</ta>
            <ta e="T1082" id="Seg_9610" s="T1081">num</ta>
            <ta e="T1083" id="Seg_9611" s="T1082">num</ta>
            <ta e="T1084" id="Seg_9612" s="T1083">num</ta>
            <ta e="T1085" id="Seg_9613" s="T1084">n</ta>
            <ta e="T1087" id="Seg_9614" s="T1086">adv</ta>
            <ta e="T1088" id="Seg_9615" s="T1087">n</ta>
            <ta e="T1089" id="Seg_9616" s="T1088">num</ta>
            <ta e="T1090" id="Seg_9617" s="T1089">num</ta>
            <ta e="T1091" id="Seg_9618" s="T1090">num</ta>
            <ta e="T1092" id="Seg_9619" s="T1091">v</ta>
            <ta e="T1094" id="Seg_9620" s="T1093">adv</ta>
            <ta e="T1095" id="Seg_9621" s="T1094">dempro</ta>
            <ta e="T1096" id="Seg_9622" s="T1095">v</ta>
            <ta e="T1097" id="Seg_9623" s="T1096">num</ta>
            <ta e="T1099" id="Seg_9624" s="T1098">dempro</ta>
            <ta e="T1100" id="Seg_9625" s="T1099">v</ta>
            <ta e="T1102" id="Seg_9626" s="T1101">adv</ta>
            <ta e="T1103" id="Seg_9627" s="T1102">dempro</ta>
            <ta e="T1104" id="Seg_9628" s="T1103">dempro</ta>
            <ta e="T1105" id="Seg_9629" s="T1104">dempro</ta>
            <ta e="T1106" id="Seg_9630" s="T1105">v</ta>
            <ta e="T1107" id="Seg_9631" s="T1106">v</ta>
            <ta e="T1109" id="Seg_9632" s="T1108">dempro</ta>
            <ta e="T1110" id="Seg_9633" s="T1109">ptcl</ta>
            <ta e="T1111" id="Seg_9634" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_9635" s="T1111">ptcl</ta>
            <ta e="T1113" id="Seg_9636" s="T1112">v</ta>
            <ta e="T1115" id="Seg_9637" s="T1114">adv</ta>
            <ta e="T1116" id="Seg_9638" s="T1115">dempro</ta>
            <ta e="T1523" id="Seg_9639" s="T1116">v</ta>
            <ta e="T1117" id="Seg_9640" s="T1523">v</ta>
            <ta e="T1120" id="Seg_9641" s="T1119">que</ta>
            <ta e="T1121" id="Seg_9642" s="T1120">propr</ta>
            <ta e="T1122" id="Seg_9643" s="T1121">v</ta>
            <ta e="T1123" id="Seg_9644" s="T1122">dempro</ta>
            <ta e="T1124" id="Seg_9645" s="T1123">n</ta>
            <ta e="T1126" id="Seg_9646" s="T1125">adv</ta>
            <ta e="T1127" id="Seg_9647" s="T1126">n</ta>
            <ta e="T1130" id="Seg_9648" s="T1129">n</ta>
            <ta e="T1132" id="Seg_9649" s="T1131">n</ta>
            <ta e="T1133" id="Seg_9650" s="T1132">v</ta>
            <ta e="T1134" id="Seg_9651" s="T1133">n</ta>
            <ta e="T1135" id="Seg_9652" s="T1134">v</ta>
            <ta e="T1136" id="Seg_9653" s="T1135">ptcl</ta>
            <ta e="T1137" id="Seg_9654" s="T1136">que</ta>
            <ta e="T1138" id="Seg_9655" s="T1137">v</ta>
            <ta e="T1139" id="Seg_9656" s="T1138">v</ta>
            <ta e="T1141" id="Seg_9657" s="T1140">num</ta>
            <ta e="T1143" id="Seg_9658" s="T1142">num</ta>
            <ta e="T1144" id="Seg_9659" s="T1143">num</ta>
            <ta e="T1145" id="Seg_9660" s="T1144">conj</ta>
            <ta e="T1146" id="Seg_9661" s="T1145">num</ta>
            <ta e="T1147" id="Seg_9662" s="T1146">n</ta>
            <ta e="T1148" id="Seg_9663" s="T1147">v</ta>
            <ta e="T1392" id="Seg_9664" s="T1391">pers</ta>
            <ta e="T1393" id="Seg_9665" s="T1392">n</ta>
            <ta e="T1394" id="Seg_9666" s="T1393">n</ta>
            <ta e="T1395" id="Seg_9667" s="T1394">n</ta>
            <ta e="T1524" id="Seg_9668" s="T1395">v</ta>
            <ta e="T1396" id="Seg_9669" s="T1524">v</ta>
            <ta e="T1398" id="Seg_9670" s="T1397">conj</ta>
            <ta e="T1399" id="Seg_9671" s="T1398">adv</ta>
            <ta e="T1400" id="Seg_9672" s="T1399">num</ta>
            <ta e="T1401" id="Seg_9673" s="T1400">n</ta>
            <ta e="T1402" id="Seg_9674" s="T1401">v</ta>
            <ta e="T1404" id="Seg_9675" s="T1403">adv</ta>
            <ta e="T1405" id="Seg_9676" s="T1404">dempro</ta>
            <ta e="T1406" id="Seg_9677" s="T1405">dempro</ta>
            <ta e="T1407" id="Seg_9678" s="T1406">v</ta>
            <ta e="T1408" id="Seg_9679" s="T1407">n</ta>
            <ta e="T1409" id="Seg_9680" s="T1408">n</ta>
            <ta e="T1410" id="Seg_9681" s="T1409">adv</ta>
            <ta e="T1412" id="Seg_9682" s="T1411">conj</ta>
            <ta e="T1413" id="Seg_9683" s="T1412">v</ta>
            <ta e="T1414" id="Seg_9684" s="T1413">n</ta>
            <ta e="T1416" id="Seg_9685" s="T1415">adv</ta>
            <ta e="T1417" id="Seg_9686" s="T1416">dempro</ta>
            <ta e="T1418" id="Seg_9687" s="T1417">n</ta>
            <ta e="T1419" id="Seg_9688" s="T1418">v</ta>
            <ta e="T1420" id="Seg_9689" s="T1419">que</ta>
            <ta e="T1421" id="Seg_9690" s="T1420">n</ta>
            <ta e="T1422" id="Seg_9691" s="T1421">n</ta>
            <ta e="T1424" id="Seg_9692" s="T1423">adv</ta>
            <ta e="T1425" id="Seg_9693" s="T1424">v</ta>
            <ta e="T1426" id="Seg_9694" s="T1425">dempro</ta>
            <ta e="T1427" id="Seg_9695" s="T1426">adv</ta>
            <ta e="T1428" id="Seg_9696" s="T1427">n</ta>
            <ta e="T1429" id="Seg_9697" s="T1428">adv</ta>
            <ta e="T1430" id="Seg_9698" s="T1429">ptcl</ta>
            <ta e="T1431" id="Seg_9699" s="T1430">v</ta>
            <ta e="T1432" id="Seg_9700" s="T1431">n</ta>
            <ta e="T1433" id="Seg_9701" s="T1432">n</ta>
            <ta e="T1435" id="Seg_9702" s="T1434">n</ta>
            <ta e="T1436" id="Seg_9703" s="T1435">n</ta>
            <ta e="T1438" id="Seg_9704" s="T1437">adv</ta>
            <ta e="T1439" id="Seg_9705" s="T1438">dempro</ta>
            <ta e="T1440" id="Seg_9706" s="T1439">v</ta>
            <ta e="T1441" id="Seg_9707" s="T1440">n</ta>
            <ta e="T1442" id="Seg_9708" s="T1441">n</ta>
            <ta e="T1444" id="Seg_9709" s="T1443">dempro</ta>
            <ta e="T1445" id="Seg_9710" s="T1444">adv</ta>
            <ta e="T1446" id="Seg_9711" s="T1445">dempro</ta>
            <ta e="T1447" id="Seg_9712" s="T1446">adv</ta>
            <ta e="T1448" id="Seg_9713" s="T1447">v</ta>
            <ta e="T1450" id="Seg_9714" s="T1449">adv</ta>
            <ta e="T1451" id="Seg_9715" s="T1450">n</ta>
            <ta e="T1453" id="Seg_9716" s="T1451">v</ta>
            <ta e="T1454" id="Seg_9717" s="T1453">pers</ta>
            <ta e="T1455" id="Seg_9718" s="T1454">n</ta>
            <ta e="T1456" id="Seg_9719" s="T1455">n</ta>
            <ta e="T1457" id="Seg_9720" s="T1456">v</ta>
            <ta e="T1459" id="Seg_9721" s="T1458">adv</ta>
            <ta e="T1460" id="Seg_9722" s="T1459">dempro</ta>
            <ta e="T1461" id="Seg_9723" s="T1460">v</ta>
            <ta e="T1463" id="Seg_9724" s="T1462">dempro</ta>
            <ta e="T1464" id="Seg_9725" s="T1463">n</ta>
            <ta e="T1465" id="Seg_9726" s="T1464">v</ta>
            <ta e="T1467" id="Seg_9727" s="T1466">adv</ta>
            <ta e="T1468" id="Seg_9728" s="T1467">adv</ta>
            <ta e="T1469" id="Seg_9729" s="T1468">dempro</ta>
            <ta e="T1470" id="Seg_9730" s="T1469">n</ta>
            <ta e="T1471" id="Seg_9731" s="T1470">v</ta>
            <ta e="T1473" id="Seg_9732" s="T1472">adv</ta>
            <ta e="T1474" id="Seg_9733" s="T1473">v</ta>
            <ta e="T1475" id="Seg_9734" s="T1474">n</ta>
            <ta e="T1477" id="Seg_9735" s="T1476">conj</ta>
            <ta e="T1478" id="Seg_9736" s="T1477">adv</ta>
            <ta e="T1479" id="Seg_9737" s="T1478">v</ta>
            <ta e="T1480" id="Seg_9738" s="T1479">conj</ta>
            <ta e="T1481" id="Seg_9739" s="T1480">adv</ta>
            <ta e="T1482" id="Seg_9740" s="T1481">num</ta>
            <ta e="T1483" id="Seg_9741" s="T1482">n</ta>
            <ta e="T1484" id="Seg_9742" s="T1483">v</ta>
            <ta e="T1486" id="Seg_9743" s="T1485">dempro</ta>
            <ta e="T1487" id="Seg_9744" s="T1486">v</ta>
            <ta e="T1488" id="Seg_9745" s="T1487">dempro</ta>
            <ta e="T1490" id="Seg_9746" s="T1489">adv</ta>
            <ta e="T1491" id="Seg_9747" s="T1490">adj</ta>
            <ta e="T1492" id="Seg_9748" s="T1491">n</ta>
            <ta e="T1494" id="Seg_9749" s="T1493">n</ta>
            <ta e="T1495" id="Seg_9750" s="T1494">ptcl</ta>
            <ta e="T1496" id="Seg_9751" s="T1495">v</ta>
            <ta e="T1497" id="Seg_9752" s="T1496">que</ta>
            <ta e="T1498" id="Seg_9753" s="T1497">ptcl</ta>
            <ta e="T1499" id="Seg_9754" s="T1498">v</ta>
            <ta e="T1500" id="Seg_9755" s="T1499">adv</ta>
            <ta e="T1501" id="Seg_9756" s="T1500">v</ta>
            <ta e="T1502" id="Seg_9757" s="T1501">adv</ta>
            <ta e="T1503" id="Seg_9758" s="T1502">ptcl</ta>
            <ta e="T1504" id="Seg_9759" s="T1503">n</ta>
            <ta e="T1505" id="Seg_9760" s="T1504">v</ta>
            <ta e="T1506" id="Seg_9761" s="T1505">adv</ta>
            <ta e="T1507" id="Seg_9762" s="T1506">num</ta>
            <ta e="T1508" id="Seg_9763" s="T1507">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T130" id="Seg_9764" s="T129">pro.h:A</ta>
            <ta e="T132" id="Seg_9765" s="T131">np:L</ta>
            <ta e="T133" id="Seg_9766" s="T132">0.1.h:A</ta>
            <ta e="T134" id="Seg_9767" s="T133">0.1.h:A</ta>
            <ta e="T137" id="Seg_9768" s="T136">np:Th</ta>
            <ta e="T140" id="Seg_9769" s="T139">pro.h:R</ta>
            <ta e="T143" id="Seg_9770" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_9771" s="T143">adv:L</ta>
            <ta e="T145" id="Seg_9772" s="T144">0.1.h:A</ta>
            <ta e="T146" id="Seg_9773" s="T145">0.1.h:A</ta>
            <ta e="T147" id="Seg_9774" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_9775" s="T148">np:Th</ta>
            <ta e="T150" id="Seg_9776" s="T149">0.1.h:A</ta>
            <ta e="T152" id="Seg_9777" s="T151">np.h:B</ta>
            <ta e="T153" id="Seg_9778" s="T152">np.h:B</ta>
            <ta e="T155" id="Seg_9779" s="T154">np.h:B</ta>
            <ta e="T156" id="Seg_9780" s="T155">0.1.h:A</ta>
            <ta e="T158" id="Seg_9781" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_9782" s="T158">0.1.h:A</ta>
            <ta e="T161" id="Seg_9783" s="T160">adv:Time</ta>
            <ta e="T162" id="Seg_9784" s="T161">np.h:B</ta>
            <ta e="T163" id="Seg_9785" s="T162">0.1.h:A</ta>
            <ta e="T164" id="Seg_9786" s="T163">np:Th</ta>
            <ta e="T505" id="Seg_9787" s="T504">pro.h:A</ta>
            <ta e="T507" id="Seg_9788" s="T506">pro:G</ta>
            <ta e="T512" id="Seg_9789" s="T511">pro.h:A</ta>
            <ta e="T513" id="Seg_9790" s="T512">pro:G</ta>
            <ta e="T516" id="Seg_9791" s="T515">pro.h:A</ta>
            <ta e="T518" id="Seg_9792" s="T517">0.2.h:A</ta>
            <ta e="T521" id="Seg_9793" s="T520">0.2.h:A</ta>
            <ta e="T522" id="Seg_9794" s="T521">pro:G</ta>
            <ta e="T524" id="Seg_9795" s="T523">0.2.h:A</ta>
            <ta e="T525" id="Seg_9796" s="T524">adv:G</ta>
            <ta e="T534" id="Seg_9797" s="T533">pro.h:A</ta>
            <ta e="T535" id="Seg_9798" s="T534">pro.h:Com</ta>
            <ta e="T539" id="Seg_9799" s="T538">0.2.h:A</ta>
            <ta e="T540" id="Seg_9800" s="T539">adv:G</ta>
            <ta e="T557" id="Seg_9801" s="T556">pro.h:A</ta>
            <ta e="T558" id="Seg_9802" s="T557">pro:G</ta>
            <ta e="T572" id="Seg_9803" s="T571">pro.h:A</ta>
            <ta e="T573" id="Seg_9804" s="T572">pro:G</ta>
            <ta e="T575" id="Seg_9805" s="T574">pro.h:A</ta>
            <ta e="T578" id="Seg_9806" s="T576">pro.h:R</ta>
            <ta e="T579" id="Seg_9807" s="T578">0.2.h:A</ta>
            <ta e="T581" id="Seg_9808" s="T580">pro:G</ta>
            <ta e="T582" id="Seg_9809" s="T581">pro.h:A</ta>
            <ta e="T583" id="Seg_9810" s="T582">pro.h:R</ta>
            <ta e="T584" id="Seg_9811" s="T583">pro:Th</ta>
            <ta e="T588" id="Seg_9812" s="T587">0.2.h:A</ta>
            <ta e="T589" id="Seg_9813" s="T588">adv:G</ta>
            <ta e="T591" id="Seg_9814" s="T590">adv:Time</ta>
            <ta e="T592" id="Seg_9815" s="T591">pro.h:A</ta>
            <ta e="T596" id="Seg_9816" s="T595">0.2.h:A</ta>
            <ta e="T598" id="Seg_9817" s="T597">pro.h:A</ta>
            <ta e="T602" id="Seg_9818" s="T601">0.1.h:A</ta>
            <ta e="T603" id="Seg_9819" s="T602">pro.h:R</ta>
            <ta e="T606" id="Seg_9820" s="T605">pro.h:A</ta>
            <ta e="T866" id="Seg_9821" s="T865">pro:G</ta>
            <ta e="T869" id="Seg_9822" s="T868">pro.h:A</ta>
            <ta e="T871" id="Seg_9823" s="T870">pro.h:R</ta>
            <ta e="T872" id="Seg_9824" s="T871">0.1.h:A</ta>
            <ta e="T873" id="Seg_9825" s="T872">pro:G</ta>
            <ta e="T875" id="Seg_9826" s="T874">pro.h:A</ta>
            <ta e="T880" id="Seg_9827" s="T879">adv:Time</ta>
            <ta e="T881" id="Seg_9828" s="T880">0.3.h:A</ta>
            <ta e="T882" id="Seg_9829" s="T881">pro.h:R</ta>
            <ta e="T883" id="Seg_9830" s="T882">np:P</ta>
            <ta e="T884" id="Seg_9831" s="T883">pro.h:A</ta>
            <ta e="T891" id="Seg_9832" s="T890">n:Time</ta>
            <ta e="T893" id="Seg_9833" s="T892">0.1.h:A</ta>
            <ta e="T895" id="Seg_9834" s="T894">pro:G</ta>
            <ta e="T898" id="Seg_9835" s="T897">pro.h:A</ta>
            <ta e="T900" id="Seg_9836" s="T899">pro.h:P</ta>
            <ta e="T905" id="Seg_9837" s="T904">pro.h:A</ta>
            <ta e="T906" id="Seg_9838" s="T905">adv:Time</ta>
            <ta e="T909" id="Seg_9839" s="T908">adv:Time</ta>
            <ta e="T910" id="Seg_9840" s="T909">0.1.h:A</ta>
            <ta e="T912" id="Seg_9841" s="T911">adv:L</ta>
            <ta e="T913" id="Seg_9842" s="T912">0.1.h:A</ta>
            <ta e="T917" id="Seg_9843" s="T916">pro.h:Poss</ta>
            <ta e="T918" id="Seg_9844" s="T917">np.h:A</ta>
            <ta e="T922" id="Seg_9845" s="T921">0.1.h:Th</ta>
            <ta e="T923" id="Seg_9846" s="T922">np:L</ta>
            <ta e="T924" id="Seg_9847" s="T923">pro.h:A</ta>
            <ta e="T927" id="Seg_9848" s="T926">pro.h:Th</ta>
            <ta e="T929" id="Seg_9849" s="T928">np.h:A</ta>
            <ta e="T931" id="Seg_9850" s="T930">np.h:R</ta>
            <ta e="T932" id="Seg_9851" s="T931">0.2.h:A</ta>
            <ta e="T936" id="Seg_9852" s="T935">pro.h:A</ta>
            <ta e="T942" id="Seg_9853" s="T941">np.h:A</ta>
            <ta e="T949" id="Seg_9854" s="T948">pro.h:P</ta>
            <ta e="T950" id="Seg_9855" s="T949">0.3.h:A</ta>
            <ta e="T952" id="Seg_9856" s="T951">adv:Time</ta>
            <ta e="T953" id="Seg_9857" s="T952">0.1.h:A</ta>
            <ta e="T954" id="Seg_9858" s="T953">np:G</ta>
            <ta e="T955" id="Seg_9859" s="T954">0.1.h:A</ta>
            <ta e="T956" id="Seg_9860" s="T955">pro.h:A</ta>
            <ta e="T958" id="Seg_9861" s="T957">pro.h:R</ta>
            <ta e="T959" id="Seg_9862" s="T958">np:P</ta>
            <ta e="T961" id="Seg_9863" s="T960">adv:Time</ta>
            <ta e="T962" id="Seg_9864" s="T961">pro.h:A</ta>
            <ta e="T963" id="Seg_9865" s="T962">pro.h:R</ta>
            <ta e="T964" id="Seg_9866" s="T963">np:P</ta>
            <ta e="T966" id="Seg_9867" s="T965">pro.h:A</ta>
            <ta e="T967" id="Seg_9868" s="T966">adv:L</ta>
            <ta e="T970" id="Seg_9869" s="T969">adv:Time</ta>
            <ta e="T972" id="Seg_9870" s="T971">np:P</ta>
            <ta e="T973" id="Seg_9871" s="T972">0.1.h:A</ta>
            <ta e="T976" id="Seg_9872" s="T975">np:P</ta>
            <ta e="T977" id="Seg_9873" s="T976">0.1.h:A</ta>
            <ta e="T979" id="Seg_9874" s="T978">np.h:Th</ta>
            <ta e="T980" id="Seg_9875" s="T979">0.1.h:A</ta>
            <ta e="T982" id="Seg_9876" s="T981">adv:Time</ta>
            <ta e="T983" id="Seg_9877" s="T982">np:Th</ta>
            <ta e="T986" id="Seg_9878" s="T985">np:P</ta>
            <ta e="T987" id="Seg_9879" s="T986">0.1.h:A</ta>
            <ta e="T988" id="Seg_9880" s="T987">np:P</ta>
            <ta e="T991" id="Seg_9881" s="T990">pro:Th</ta>
            <ta e="T995" id="Seg_9882" s="T994">np.h:Th</ta>
            <ta e="T999" id="Seg_9883" s="T998">np:L</ta>
            <ta e="T1000" id="Seg_9884" s="T999">0.3.h:A</ta>
            <ta e="T1002" id="Seg_9885" s="T1001">adv:Time</ta>
            <ta e="T1003" id="Seg_9886" s="T1002">pro.h:A</ta>
            <ta e="T1004" id="Seg_9887" s="T1003">pro.h:B</ta>
            <ta e="T1006" id="Seg_9888" s="T1005">np:Th</ta>
            <ta e="T1008" id="Seg_9889" s="T1007">adv:Time</ta>
            <ta e="T1009" id="Seg_9890" s="T1008">np:Th</ta>
            <ta e="T1010" id="Seg_9891" s="T1009">pro.h:R</ta>
            <ta e="T1012" id="Seg_9892" s="T1011">0.1.h:A</ta>
            <ta e="T1014" id="Seg_9893" s="T1013">pro.h:A</ta>
            <ta e="T1017" id="Seg_9894" s="T1016">np.h:Com</ta>
            <ta e="T1019" id="Seg_9895" s="T1018">adv:L</ta>
            <ta e="T1024" id="Seg_9896" s="T1023">np.h:A</ta>
            <ta e="T1028" id="Seg_9897" s="T1027">np:Ins</ta>
            <ta e="T1030" id="Seg_9898" s="T1029">adv:Time</ta>
            <ta e="T1033" id="Seg_9899" s="T1032">np.h:Poss</ta>
            <ta e="T1034" id="Seg_9900" s="T1033">np.h:E</ta>
            <ta e="T1037" id="Seg_9901" s="T1036">pro.h:B</ta>
            <ta e="T1040" id="Seg_9902" s="T1039">adv:Time</ta>
            <ta e="T1041" id="Seg_9903" s="T1040">pro.h:A</ta>
            <ta e="T1042" id="Seg_9904" s="T1041">adv:L</ta>
            <ta e="T1049" id="Seg_9905" s="T1048">0.1.h:E</ta>
            <ta e="T1051" id="Seg_9906" s="T1050">pro.h:A</ta>
            <ta e="T1053" id="Seg_9907" s="T1052">pro.h:R</ta>
            <ta e="T1056" id="Seg_9908" s="T1055">0.2.h:A</ta>
            <ta e="T1057" id="Seg_9909" s="T1056">np:G</ta>
            <ta e="T1060" id="Seg_9910" s="T1059">pro.h:A</ta>
            <ta e="T1062" id="Seg_9911" s="T1061">pro.h:A</ta>
            <ta e="T1069" id="Seg_9912" s="T1068">0.2.h:A</ta>
            <ta e="T1072" id="Seg_9913" s="T1071">pro.h:A</ta>
            <ta e="T1075" id="Seg_9914" s="T1074">np:Th</ta>
            <ta e="T1078" id="Seg_9915" s="T1077">pro.h:A</ta>
            <ta e="T1085" id="Seg_9916" s="T1084">np:Th</ta>
            <ta e="T1087" id="Seg_9917" s="T1086">adv:Time</ta>
            <ta e="T1088" id="Seg_9918" s="T1087">np.h:A</ta>
            <ta e="T1091" id="Seg_9919" s="T1090">np:Th</ta>
            <ta e="T1094" id="Seg_9920" s="T1093">adv:Time</ta>
            <ta e="T1095" id="Seg_9921" s="T1094">pro.h:A</ta>
            <ta e="T1099" id="Seg_9922" s="T1098">pro.h:A</ta>
            <ta e="T1102" id="Seg_9923" s="T1101">adv:Time</ta>
            <ta e="T1104" id="Seg_9924" s="T1103">pro.h:R</ta>
            <ta e="T1105" id="Seg_9925" s="T1104">pro.h:A</ta>
            <ta e="T1107" id="Seg_9926" s="T1106">0.3.h:A</ta>
            <ta e="T1109" id="Seg_9927" s="T1108">pro.h:A</ta>
            <ta e="T1115" id="Seg_9928" s="T1114">adv:Time</ta>
            <ta e="T1116" id="Seg_9929" s="T1115">pro.h:A</ta>
            <ta e="T1121" id="Seg_9930" s="T1120">np.h:E</ta>
            <ta e="T1124" id="Seg_9931" s="T1123">np:G</ta>
            <ta e="T1126" id="Seg_9932" s="T1125">adv:L</ta>
            <ta e="T1132" id="Seg_9933" s="T1131">np:Th</ta>
            <ta e="T1133" id="Seg_9934" s="T1132">0.3.h:A</ta>
            <ta e="T1134" id="Seg_9935" s="T1133">np:Th</ta>
            <ta e="T1135" id="Seg_9936" s="T1134">0.3.h:A</ta>
            <ta e="T1137" id="Seg_9937" s="T1136">pro:Th</ta>
            <ta e="T1138" id="Seg_9938" s="T1137">0.3.h:A</ta>
            <ta e="T1139" id="Seg_9939" s="T1138">0.3.h:A</ta>
            <ta e="T1147" id="Seg_9940" s="T1146">np:Th</ta>
            <ta e="T1148" id="Seg_9941" s="T1147">0.3.h:A</ta>
            <ta e="T1392" id="Seg_9942" s="T1391">pro.h:Poss</ta>
            <ta e="T1393" id="Seg_9943" s="T1392">np.h:Poss</ta>
            <ta e="T1394" id="Seg_9944" s="T1393">np.h:A</ta>
            <ta e="T1395" id="Seg_9945" s="T1394">np.h:Th</ta>
            <ta e="T1399" id="Seg_9946" s="T1398">adv:L</ta>
            <ta e="T1401" id="Seg_9947" s="T1400">np.h:Th</ta>
            <ta e="T1404" id="Seg_9948" s="T1403">adv:Time</ta>
            <ta e="T1406" id="Seg_9949" s="T1405">pro.h:A</ta>
            <ta e="T1409" id="Seg_9950" s="T1408">np:G</ta>
            <ta e="T1410" id="Seg_9951" s="T1409">adv:L</ta>
            <ta e="T1416" id="Seg_9952" s="T1415">adv:Time</ta>
            <ta e="T1417" id="Seg_9953" s="T1416">pro.h:A</ta>
            <ta e="T1418" id="Seg_9954" s="T1417">np:P</ta>
            <ta e="T1424" id="Seg_9955" s="T1423">adv:Time</ta>
            <ta e="T1425" id="Seg_9956" s="T1424">0.3.h:A</ta>
            <ta e="T1427" id="Seg_9957" s="T1426">adv:L</ta>
            <ta e="T1428" id="Seg_9958" s="T1427">np:G</ta>
            <ta e="T1429" id="Seg_9959" s="T1428">adv:L</ta>
            <ta e="T1431" id="Seg_9960" s="T1430">0.3.h:A</ta>
            <ta e="T1433" id="Seg_9961" s="T1432">np:P</ta>
            <ta e="T1438" id="Seg_9962" s="T1437">adv:Time</ta>
            <ta e="T1439" id="Seg_9963" s="T1438">pro.h:A</ta>
            <ta e="T1441" id="Seg_9964" s="T1440">np:G</ta>
            <ta e="T1442" id="Seg_9965" s="T1441">np:G</ta>
            <ta e="T1445" id="Seg_9966" s="T1444">adv:Time</ta>
            <ta e="T1446" id="Seg_9967" s="T1445">pro.h:Th</ta>
            <ta e="T1447" id="Seg_9968" s="T1446">adv:L</ta>
            <ta e="T1450" id="Seg_9969" s="T1449">adv:Time</ta>
            <ta e="T1451" id="Seg_9970" s="T1450">np:P</ta>
            <ta e="T1453" id="Seg_9971" s="T1451">0.3.h:A</ta>
            <ta e="T1454" id="Seg_9972" s="T1453">pro.h:Poss</ta>
            <ta e="T1455" id="Seg_9973" s="T1454">np.h:P</ta>
            <ta e="T1456" id="Seg_9974" s="T1455">np:L</ta>
            <ta e="T1459" id="Seg_9975" s="T1458">adv:Time</ta>
            <ta e="T1460" id="Seg_9976" s="T1459">pro.h:A</ta>
            <ta e="T1463" id="Seg_9977" s="T1462">pro.h:Th</ta>
            <ta e="T1464" id="Seg_9978" s="T1463">np:G</ta>
            <ta e="T1465" id="Seg_9979" s="T1464">0.3.h:A</ta>
            <ta e="T1467" id="Seg_9980" s="T1466">adv:Time</ta>
            <ta e="T1469" id="Seg_9981" s="T1468">pro.h:Th</ta>
            <ta e="T1470" id="Seg_9982" s="T1469">np.h:Th</ta>
            <ta e="T1473" id="Seg_9983" s="T1472">adv:L</ta>
            <ta e="T1475" id="Seg_9984" s="T1474">np.h:Th</ta>
            <ta e="T1478" id="Seg_9985" s="T1477">adv:Time</ta>
            <ta e="T1479" id="Seg_9986" s="T1478">0.3.h:E</ta>
            <ta e="T1481" id="Seg_9987" s="T1480">adv:L</ta>
            <ta e="T1483" id="Seg_9988" s="T1482">np.h:Th</ta>
            <ta e="T1486" id="Seg_9989" s="T1485">pro.h:A</ta>
            <ta e="T1488" id="Seg_9990" s="T1487">pro.h:Th</ta>
            <ta e="T1494" id="Seg_9991" s="T1493">np:P</ta>
            <ta e="T1496" id="Seg_9992" s="T1495">0.3.h:A</ta>
            <ta e="T1497" id="Seg_9993" s="T1496">pro:G</ta>
            <ta e="T1499" id="Seg_9994" s="T1498">0.3.h:A</ta>
            <ta e="T1501" id="Seg_9995" s="T1500">0.3.h:A</ta>
            <ta e="T1504" id="Seg_9996" s="T1503">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T130" id="Seg_9997" s="T129">pro.h:S</ta>
            <ta e="T131" id="Seg_9998" s="T130">v:pred</ta>
            <ta e="T133" id="Seg_9999" s="T132">v:pred 0.1.h:S</ta>
            <ta e="T134" id="Seg_10000" s="T133">v:pred 0.1.h:S</ta>
            <ta e="T137" id="Seg_10001" s="T136">np:O</ta>
            <ta e="T141" id="Seg_10002" s="T140">s:purp</ta>
            <ta e="T145" id="Seg_10003" s="T144">v:pred 0.1.h:S</ta>
            <ta e="T146" id="Seg_10004" s="T145">v:pred 0.1.h:S</ta>
            <ta e="T147" id="Seg_10005" s="T146">np:O</ta>
            <ta e="T149" id="Seg_10006" s="T148">np:O</ta>
            <ta e="T150" id="Seg_10007" s="T149">v:pred 0.1.h:S</ta>
            <ta e="T156" id="Seg_10008" s="T155">v:pred 0.1.h:S</ta>
            <ta e="T158" id="Seg_10009" s="T157">np:O</ta>
            <ta e="T159" id="Seg_10010" s="T158">v:pred 0.1.h:S</ta>
            <ta e="T163" id="Seg_10011" s="T162">v:pred 0.1.h:S</ta>
            <ta e="T164" id="Seg_10012" s="T163">np:O</ta>
            <ta e="T505" id="Seg_10013" s="T504">pro.h:S</ta>
            <ta e="T506" id="Seg_10014" s="T505">v:pred</ta>
            <ta e="T510" id="Seg_10015" s="T509">v:pred</ta>
            <ta e="T512" id="Seg_10016" s="T511">pro.h:S</ta>
            <ta e="T514" id="Seg_10017" s="T513">v:pred</ta>
            <ta e="T516" id="Seg_10018" s="T515">pro.h:S</ta>
            <ta e="T517" id="Seg_10019" s="T516">v:pred</ta>
            <ta e="T518" id="Seg_10020" s="T517">v:pred 0.2.h:S</ta>
            <ta e="T521" id="Seg_10021" s="T520">v:pred 0.2.h:S</ta>
            <ta e="T524" id="Seg_10022" s="T523">v:pred 0.2.h:S</ta>
            <ta e="T534" id="Seg_10023" s="T533">pro.h:S</ta>
            <ta e="T536" id="Seg_10024" s="T535">ptcl.neg</ta>
            <ta e="T537" id="Seg_10025" s="T536">v:pred</ta>
            <ta e="T539" id="Seg_10026" s="T538">v:pred 0.2.h:S</ta>
            <ta e="T557" id="Seg_10027" s="T556">pro.h:S</ta>
            <ta e="T560" id="Seg_10028" s="T559">v:pred</ta>
            <ta e="T572" id="Seg_10029" s="T571">pro.h:S</ta>
            <ta e="T574" id="Seg_10030" s="T573">v:pred</ta>
            <ta e="T575" id="Seg_10031" s="T574">pro.h:S</ta>
            <ta e="T576" id="Seg_10032" s="T575">v:pred</ta>
            <ta e="T578" id="Seg_10033" s="T576">pro.h:O</ta>
            <ta e="T579" id="Seg_10034" s="T578">v:pred 0.2.h:S</ta>
            <ta e="T582" id="Seg_10035" s="T581">pro.h:S</ta>
            <ta e="T584" id="Seg_10036" s="T583">pro:O</ta>
            <ta e="T585" id="Seg_10037" s="T584">ptcl.neg</ta>
            <ta e="T586" id="Seg_10038" s="T585">v:pred</ta>
            <ta e="T588" id="Seg_10039" s="T587">v:pred 0.2.h:S</ta>
            <ta e="T592" id="Seg_10040" s="T591">pro.h:S</ta>
            <ta e="T593" id="Seg_10041" s="T592">v:pred</ta>
            <ta e="T596" id="Seg_10042" s="T595">v:pred 0.2.h:S</ta>
            <ta e="T598" id="Seg_10043" s="T597">pro.h:S</ta>
            <ta e="T599" id="Seg_10044" s="T598">v:pred</ta>
            <ta e="T602" id="Seg_10045" s="T601">v:pred 0.1.h:S</ta>
            <ta e="T606" id="Seg_10046" s="T605">pro.h:S</ta>
            <ta e="T607" id="Seg_10047" s="T606">v:pred</ta>
            <ta e="T863" id="Seg_10048" s="T862">v:pred</ta>
            <ta e="T869" id="Seg_10049" s="T868">pro.h:S</ta>
            <ta e="T870" id="Seg_10050" s="T869">v:pred</ta>
            <ta e="T872" id="Seg_10051" s="T871">v:pred 0.1.h:S</ta>
            <ta e="T875" id="Seg_10052" s="T874">pro.h:S</ta>
            <ta e="T877" id="Seg_10053" s="T876">v:pred</ta>
            <ta e="T881" id="Seg_10054" s="T880">v:pred 0.3.h:S</ta>
            <ta e="T883" id="Seg_10055" s="T882">np:O</ta>
            <ta e="T884" id="Seg_10056" s="T883">pro.h:S</ta>
            <ta e="T885" id="Seg_10057" s="T884">ptcl.neg</ta>
            <ta e="T886" id="Seg_10058" s="T885">v:pred</ta>
            <ta e="T893" id="Seg_10059" s="T892">v:pred 0.1.h:S</ta>
            <ta e="T898" id="Seg_10060" s="T897">pro.h:S</ta>
            <ta e="T899" id="Seg_10061" s="T898">v:pred</ta>
            <ta e="T900" id="Seg_10062" s="T899">pro.h:S</ta>
            <ta e="T902" id="Seg_10063" s="T901">v:pred</ta>
            <ta e="T905" id="Seg_10064" s="T904">pro.h:S</ta>
            <ta e="T907" id="Seg_10065" s="T906">v:pred</ta>
            <ta e="T910" id="Seg_10066" s="T909">v:pred 0.1.h:S</ta>
            <ta e="T913" id="Seg_10067" s="T912">v:pred 0.1.h:S</ta>
            <ta e="T918" id="Seg_10068" s="T917">np.h:S</ta>
            <ta e="T920" id="Seg_10069" s="T919">v:pred</ta>
            <ta e="T922" id="Seg_10070" s="T921">v:pred 0.1.h:S</ta>
            <ta e="T924" id="Seg_10071" s="T923">pro.h:S</ta>
            <ta e="T926" id="Seg_10072" s="T924">v:pred</ta>
            <ta e="T929" id="Seg_10073" s="T928">np.h:S</ta>
            <ta e="T930" id="Seg_10074" s="T929">v:pred</ta>
            <ta e="T932" id="Seg_10075" s="T931">v:pred 0.2.h:S</ta>
            <ta e="T936" id="Seg_10076" s="T935">pro.h:S</ta>
            <ta e="T938" id="Seg_10077" s="T936">v:pred</ta>
            <ta e="T940" id="Seg_10078" s="T939">ptcl.neg</ta>
            <ta e="T942" id="Seg_10079" s="T941">np.h:S</ta>
            <ta e="T944" id="Seg_10080" s="T943">v:pred</ta>
            <ta e="T949" id="Seg_10081" s="T948">pro.h:O</ta>
            <ta e="T950" id="Seg_10082" s="T949">v:pred 0.3.h:S</ta>
            <ta e="T953" id="Seg_10083" s="T952">v:pred 0.1.h:S</ta>
            <ta e="T955" id="Seg_10084" s="T954">v:pred 0.1.h:S</ta>
            <ta e="T956" id="Seg_10085" s="T955">pro.h:S</ta>
            <ta e="T957" id="Seg_10086" s="T956">v:pred</ta>
            <ta e="T959" id="Seg_10087" s="T958">np:O</ta>
            <ta e="T962" id="Seg_10088" s="T961">pro.h:S</ta>
            <ta e="T964" id="Seg_10089" s="T963">np:O</ta>
            <ta e="T965" id="Seg_10090" s="T964">v:pred</ta>
            <ta e="T966" id="Seg_10091" s="T965">pro.h:S</ta>
            <ta e="T968" id="Seg_10092" s="T967">v:pred</ta>
            <ta e="T972" id="Seg_10093" s="T971">np:O</ta>
            <ta e="T973" id="Seg_10094" s="T972">v:pred 0.1.h:S</ta>
            <ta e="T976" id="Seg_10095" s="T975">np:O</ta>
            <ta e="T977" id="Seg_10096" s="T976">v:pred 0.1.h:S</ta>
            <ta e="T979" id="Seg_10097" s="T978">np.h:O</ta>
            <ta e="T980" id="Seg_10098" s="T979">v:pred 0.1.h:S</ta>
            <ta e="T983" id="Seg_10099" s="T982">np:S</ta>
            <ta e="T984" id="Seg_10100" s="T983">v:pred</ta>
            <ta e="T986" id="Seg_10101" s="T985">np:O</ta>
            <ta e="T987" id="Seg_10102" s="T986">v:pred 0.1.h:S</ta>
            <ta e="T988" id="Seg_10103" s="T987">np:O</ta>
            <ta e="T989" id="Seg_10104" s="T988">adj:pred</ta>
            <ta e="T991" id="Seg_10105" s="T990">pro:S</ta>
            <ta e="T992" id="Seg_10106" s="T991">cop</ta>
            <ta e="T995" id="Seg_10107" s="T994">np.h:S</ta>
            <ta e="T996" id="Seg_10108" s="T995">adj:pred</ta>
            <ta e="T997" id="Seg_10109" s="T996">cop</ta>
            <ta e="T1000" id="Seg_10110" s="T999">v:pred 0.3.h:S</ta>
            <ta e="T1003" id="Seg_10111" s="T1002">pro.h:S</ta>
            <ta e="T1005" id="Seg_10112" s="T1004">v:pred</ta>
            <ta e="T1006" id="Seg_10113" s="T1005">np:O</ta>
            <ta e="T1009" id="Seg_10114" s="T1008">np:O</ta>
            <ta e="T1012" id="Seg_10115" s="T1011">v:pred 0.1.h:S</ta>
            <ta e="T1014" id="Seg_10116" s="T1013">pro.h:S</ta>
            <ta e="T1522" id="Seg_10117" s="T1014">conv:pred</ta>
            <ta e="T1015" id="Seg_10118" s="T1522">v:pred</ta>
            <ta e="T1024" id="Seg_10119" s="T1023">np.h:S</ta>
            <ta e="T1026" id="Seg_10120" s="T1025">v:pred</ta>
            <ta e="T1034" id="Seg_10121" s="T1033">np.h:S</ta>
            <ta e="T1037" id="Seg_10122" s="T1036">pro.h:O</ta>
            <ta e="T1038" id="Seg_10123" s="T1037">ptcl.neg</ta>
            <ta e="T1039" id="Seg_10124" s="T1038">v:pred</ta>
            <ta e="T1041" id="Seg_10125" s="T1040">pro.h:S</ta>
            <ta e="T1044" id="Seg_10126" s="T1042">v:pred</ta>
            <ta e="T1049" id="Seg_10127" s="T1048">v:pred 0.1.h:S</ta>
            <ta e="T1051" id="Seg_10128" s="T1050">pro.h:S</ta>
            <ta e="T1055" id="Seg_10129" s="T1053">v:pred</ta>
            <ta e="T1056" id="Seg_10130" s="T1055">v:pred 0.2.h:S</ta>
            <ta e="T1060" id="Seg_10131" s="T1059">pro.h:S</ta>
            <ta e="T1061" id="Seg_10132" s="T1060">v:pred</ta>
            <ta e="T1062" id="Seg_10133" s="T1061">pro.h:S</ta>
            <ta e="T1064" id="Seg_10134" s="T1063">v:pred</ta>
            <ta e="T1069" id="Seg_10135" s="T1068">v:pred 0.2.h:S</ta>
            <ta e="T1072" id="Seg_10136" s="T1071">pro.h:S</ta>
            <ta e="T1074" id="Seg_10137" s="T1073">v:pred</ta>
            <ta e="T1075" id="Seg_10138" s="T1074">np:S</ta>
            <ta e="T1076" id="Seg_10139" s="T1075">v:pred</ta>
            <ta e="T1078" id="Seg_10140" s="T1077">pro.h:S</ta>
            <ta e="T1080" id="Seg_10141" s="T1079">v:pred</ta>
            <ta e="T1085" id="Seg_10142" s="T1084">np:O</ta>
            <ta e="T1088" id="Seg_10143" s="T1087">np.h:S</ta>
            <ta e="T1091" id="Seg_10144" s="T1090">np:O</ta>
            <ta e="T1092" id="Seg_10145" s="T1091">v:pred</ta>
            <ta e="T1095" id="Seg_10146" s="T1094">pro.h:S</ta>
            <ta e="T1096" id="Seg_10147" s="T1095">v:pred</ta>
            <ta e="T1099" id="Seg_10148" s="T1098">pro.h:S</ta>
            <ta e="T1100" id="Seg_10149" s="T1099">v:pred</ta>
            <ta e="T1105" id="Seg_10150" s="T1104">pro.h:S</ta>
            <ta e="T1106" id="Seg_10151" s="T1105">v:pred</ta>
            <ta e="T1107" id="Seg_10152" s="T1106">v:pred 0.3.h:S</ta>
            <ta e="T1109" id="Seg_10153" s="T1108">pro.h:S</ta>
            <ta e="T1112" id="Seg_10154" s="T1111">ptcl.neg</ta>
            <ta e="T1113" id="Seg_10155" s="T1112">v:pred</ta>
            <ta e="T1116" id="Seg_10156" s="T1115">pro.h:S</ta>
            <ta e="T1523" id="Seg_10157" s="T1116">conv:pred</ta>
            <ta e="T1117" id="Seg_10158" s="T1523">v:pred</ta>
            <ta e="T1121" id="Seg_10159" s="T1120">np.h:S</ta>
            <ta e="T1122" id="Seg_10160" s="T1121">v:pred</ta>
            <ta e="T1132" id="Seg_10161" s="T1131">np:O</ta>
            <ta e="T1133" id="Seg_10162" s="T1132">v:pred 0.3.h:S</ta>
            <ta e="T1134" id="Seg_10163" s="T1133">np:O</ta>
            <ta e="T1135" id="Seg_10164" s="T1134">v:pred 0.3.h:S</ta>
            <ta e="T1137" id="Seg_10165" s="T1136">pro:O</ta>
            <ta e="T1138" id="Seg_10166" s="T1137">v:pred 0.3.h:S</ta>
            <ta e="T1139" id="Seg_10167" s="T1138">v:pred 0.3.h:S</ta>
            <ta e="T1147" id="Seg_10168" s="T1146">np:O</ta>
            <ta e="T1148" id="Seg_10169" s="T1147">v:pred 0.3.h:S</ta>
            <ta e="T1394" id="Seg_10170" s="T1393">np.h:S</ta>
            <ta e="T1524" id="Seg_10171" s="T1395">conv:pred</ta>
            <ta e="T1396" id="Seg_10172" s="T1524">v:pred</ta>
            <ta e="T1401" id="Seg_10173" s="T1400">np.h:S</ta>
            <ta e="T1402" id="Seg_10174" s="T1401">v:pred</ta>
            <ta e="T1406" id="Seg_10175" s="T1405">pro.h:S</ta>
            <ta e="T1407" id="Seg_10176" s="T1406">v:pred</ta>
            <ta e="T1417" id="Seg_10177" s="T1416">pro.h:S</ta>
            <ta e="T1418" id="Seg_10178" s="T1417">np:O</ta>
            <ta e="T1419" id="Seg_10179" s="T1418">v:pred</ta>
            <ta e="T1425" id="Seg_10180" s="T1424">v:pred 0.3.h:S</ta>
            <ta e="T1431" id="Seg_10181" s="T1430">v:pred 0.3.h:S</ta>
            <ta e="T1433" id="Seg_10182" s="T1432">np:O</ta>
            <ta e="T1439" id="Seg_10183" s="T1438">pro.h:S</ta>
            <ta e="T1440" id="Seg_10184" s="T1439">v:pred</ta>
            <ta e="T1446" id="Seg_10185" s="T1445">pro.h:S</ta>
            <ta e="T1448" id="Seg_10186" s="T1447">v:pred</ta>
            <ta e="T1451" id="Seg_10187" s="T1450">np:O</ta>
            <ta e="T1453" id="Seg_10188" s="T1451">v:pred 0.3.h:S</ta>
            <ta e="T1455" id="Seg_10189" s="T1454">np.h:S</ta>
            <ta e="T1457" id="Seg_10190" s="T1456">v:pred</ta>
            <ta e="T1460" id="Seg_10191" s="T1459">pro.h:S</ta>
            <ta e="T1461" id="Seg_10192" s="T1460">v:pred</ta>
            <ta e="T1463" id="Seg_10193" s="T1462">pro.h:O</ta>
            <ta e="T1465" id="Seg_10194" s="T1464">v:pred 0.3.h:S</ta>
            <ta e="T1470" id="Seg_10195" s="T1469">np.h:S</ta>
            <ta e="T1471" id="Seg_10196" s="T1470">v:pred</ta>
            <ta e="T1474" id="Seg_10197" s="T1473">v:pred</ta>
            <ta e="T1475" id="Seg_10198" s="T1474">np.h:S</ta>
            <ta e="T1479" id="Seg_10199" s="T1478">v:pred 0.3.h:S</ta>
            <ta e="T1483" id="Seg_10200" s="T1482">np.h:S</ta>
            <ta e="T1484" id="Seg_10201" s="T1483">v:pred</ta>
            <ta e="T1486" id="Seg_10202" s="T1485">pro.h:S</ta>
            <ta e="T1487" id="Seg_10203" s="T1486">v:pred</ta>
            <ta e="T1494" id="Seg_10204" s="T1493">np:O</ta>
            <ta e="T1495" id="Seg_10205" s="T1494">ptcl.neg</ta>
            <ta e="T1496" id="Seg_10206" s="T1495">v:pred 0.3.h:S</ta>
            <ta e="T1498" id="Seg_10207" s="T1497">ptcl.neg</ta>
            <ta e="T1499" id="Seg_10208" s="T1498">v:pred 0.3.h:S</ta>
            <ta e="T1501" id="Seg_10209" s="T1500">v:pred 0.3.h:S</ta>
            <ta e="T1504" id="Seg_10210" s="T1503">np.h:S</ta>
            <ta e="T1505" id="Seg_10211" s="T1504">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T132" id="Seg_10212" s="T131">RUS:cult</ta>
            <ta e="T137" id="Seg_10213" s="T136">RUS:cult</ta>
            <ta e="T153" id="Seg_10214" s="T152">RUS:core</ta>
            <ta e="T158" id="Seg_10215" s="T157">RUS:cult</ta>
            <ta e="T162" id="Seg_10216" s="T161">RUS:core</ta>
            <ta e="T515" id="Seg_10217" s="T514">RUS:gram</ta>
            <ta e="T537" id="Seg_10218" s="T536">%TURK:core</ta>
            <ta e="T584" id="Seg_10219" s="T583">TURK:gram(INDEF)</ta>
            <ta e="T595" id="Seg_10220" s="T594">TURK:core</ta>
            <ta e="T596" id="Seg_10221" s="T595">%TURK:core</ta>
            <ta e="T601" id="Seg_10222" s="T600">TURK:core</ta>
            <ta e="T605" id="Seg_10223" s="T604">RUS:gram</ta>
            <ta e="T608" id="Seg_10224" s="T607">TURK:core</ta>
            <ta e="T868" id="Seg_10225" s="T867">RUS:gram</ta>
            <ta e="T897" id="Seg_10226" s="T896">RUS:gram</ta>
            <ta e="T901" id="Seg_10227" s="T900">RUS:mod</ta>
            <ta e="T904" id="Seg_10228" s="T903">RUS:gram</ta>
            <ta e="T911" id="Seg_10229" s="T910">RUS:gram</ta>
            <ta e="T931" id="Seg_10230" s="T930">TURK:core</ta>
            <ta e="T935" id="Seg_10231" s="T934">RUS:gram</ta>
            <ta e="T939" id="Seg_10232" s="T938">RUS:gram</ta>
            <ta e="T942" id="Seg_10233" s="T941">TURK:core</ta>
            <ta e="T946" id="Seg_10234" s="T945">RUS:gram</ta>
            <ta e="T948" id="Seg_10235" s="T947">RUS:gram</ta>
            <ta e="T954" id="Seg_10236" s="T953">TURK:core</ta>
            <ta e="T976" id="Seg_10237" s="T975">TURK:cult</ta>
            <ta e="T978" id="Seg_10238" s="T977">RUS:gram</ta>
            <ta e="T983" id="Seg_10239" s="T982">TURK:cult</ta>
            <ta e="T986" id="Seg_10240" s="T985">RUS:cult</ta>
            <ta e="T988" id="Seg_10241" s="T987">RUS:cult</ta>
            <ta e="T994" id="Seg_10242" s="T993">RUS:gram</ta>
            <ta e="T999" id="Seg_10243" s="T1525">TAT:cult</ta>
            <ta e="T1006" id="Seg_10244" s="T1005">RUS:cult</ta>
            <ta e="T1009" id="Seg_10245" s="T1008">TAT:cult</ta>
            <ta e="T1025" id="Seg_10246" s="T1024">%TURK:core</ta>
            <ta e="T1026" id="Seg_10247" s="T1025">%TURK:core</ta>
            <ta e="T1046" id="Seg_10248" s="T1045">RUS:core</ta>
            <ta e="T1066" id="Seg_10249" s="T1065">RUS:gram</ta>
            <ta e="T1071" id="Seg_10250" s="T1070">RUS:gram</ta>
            <ta e="T1073" id="Seg_10251" s="T1072">TURK:core</ta>
            <ta e="T1075" id="Seg_10252" s="T1074">TAT:cult</ta>
            <ta e="T1079" id="Seg_10253" s="T1078">TURK:core</ta>
            <ta e="T1085" id="Seg_10254" s="T1084">TAT:cult</ta>
            <ta e="T1088" id="Seg_10255" s="T1087">TURK:core</ta>
            <ta e="T1111" id="Seg_10256" s="T1110">TURK:core</ta>
            <ta e="T1121" id="Seg_10257" s="T1120">RUS:cult</ta>
            <ta e="T1124" id="Seg_10258" s="T1123">TAT:cult</ta>
            <ta e="T1127" id="Seg_10259" s="T1126">RUS:cult</ta>
            <ta e="T1130" id="Seg_10260" s="T1129">RUS:cult</ta>
            <ta e="T1132" id="Seg_10261" s="T1131">TURK:cult</ta>
            <ta e="T1134" id="Seg_10262" s="T1133">TURK:cult</ta>
            <ta e="T1136" id="Seg_10263" s="T1135">TURK:disc</ta>
            <ta e="T1145" id="Seg_10264" s="T1144">RUS:gram</ta>
            <ta e="T1147" id="Seg_10265" s="T1146">TAT:cult</ta>
            <ta e="T1393" id="Seg_10266" s="T1392">RUS:core</ta>
            <ta e="T1398" id="Seg_10267" s="T1397">RUS:gram</ta>
            <ta e="T1412" id="Seg_10268" s="T1411">RUS:gram</ta>
            <ta e="T1428" id="Seg_10269" s="T1427">RUS:core</ta>
            <ta e="T1430" id="Seg_10270" s="T1429">RUS:mod</ta>
            <ta e="T1468" id="Seg_10271" s="T1467">TURK:core</ta>
            <ta e="T1477" id="Seg_10272" s="T1476">RUS:gram</ta>
            <ta e="T1480" id="Seg_10273" s="T1479">RUS:gram</ta>
            <ta e="T1491" id="Seg_10274" s="T1490">TURK:core</ta>
            <ta e="T1494" id="Seg_10275" s="T1493">TURK:cult</ta>
            <ta e="T1497" id="Seg_10276" s="T1496">TURK:gram(INDEF)</ta>
            <ta e="T1500" id="Seg_10277" s="T1499">TURK:core</ta>
            <ta e="T1503" id="Seg_10278" s="T1502">RUS:mod</ta>
            <ta e="T1506" id="Seg_10279" s="T1505">RUS:mod</ta>
            <ta e="T1508" id="Seg_10280" s="T1507">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T9" id="Seg_10281" s="T1">RUS:ext</ta>
            <ta e="T30" id="Seg_10282" s="T17">RUS:ext</ta>
            <ta e="T51" id="Seg_10283" s="T44">RUS:ext</ta>
            <ta e="T59" id="Seg_10284" s="T52">RUS:ext</ta>
            <ta e="T61" id="Seg_10285" s="T60">RUS:ext</ta>
            <ta e="T75" id="Seg_10286" s="T62">RUS:ext</ta>
            <ta e="T80" id="Seg_10287" s="T75">RUS:ext</ta>
            <ta e="T84" id="Seg_10288" s="T81">RUS:ext</ta>
            <ta e="T88" id="Seg_10289" s="T85">RUS:ext</ta>
            <ta e="T94" id="Seg_10290" s="T89">RUS:ext</ta>
            <ta e="T106" id="Seg_10291" s="T104">RUS:ext</ta>
            <ta e="T118" id="Seg_10292" s="T108">RUS:ext</ta>
            <ta e="T127" id="Seg_10293" s="T126">RUS:ext</ta>
            <ta e="T185" id="Seg_10294" s="T173">RUS:ext</ta>
            <ta e="T193" id="Seg_10295" s="T186">RUS:ext</ta>
            <ta e="T198" id="Seg_10296" s="T194">RUS:ext</ta>
            <ta e="T210" id="Seg_10297" s="T199">RUS:ext</ta>
            <ta e="T221" id="Seg_10298" s="T219">RUS:ext</ta>
            <ta e="T224" id="Seg_10299" s="T221">RUS:ext</ta>
            <ta e="T228" id="Seg_10300" s="T226">RUS:int</ta>
            <ta e="T237" id="Seg_10301" s="T234">RUS:int</ta>
            <ta e="T249" id="Seg_10302" s="T239">RUS:ext</ta>
            <ta e="T260" id="Seg_10303" s="T257">RUS:ext</ta>
            <ta e="T270" id="Seg_10304" s="T261">RUS:ext</ta>
            <ta e="T280" id="Seg_10305" s="T271">RUS:ext</ta>
            <ta e="T286" id="Seg_10306" s="T284">RUS:ext</ta>
            <ta e="T301" id="Seg_10307" s="T286">RUS:ext</ta>
            <ta e="T312" id="Seg_10308" s="T307">RUS:ext</ta>
            <ta e="T344" id="Seg_10309" s="T313">RUS:ext</ta>
            <ta e="T364" id="Seg_10310" s="T359">RUS:ext</ta>
            <ta e="T410" id="Seg_10311" s="T389">RUS:ext</ta>
            <ta e="T424" id="Seg_10312" s="T411">RUS:ext</ta>
            <ta e="T438" id="Seg_10313" s="T428">RUS:ext</ta>
            <ta e="T446" id="Seg_10314" s="T438">RUS:ext</ta>
            <ta e="T455" id="Seg_10315" s="T447">RUS:ext</ta>
            <ta e="T472" id="Seg_10316" s="T456">RUS:ext</ta>
            <ta e="T479" id="Seg_10317" s="T473">RUS:ext</ta>
            <ta e="T482" id="Seg_10318" s="T480">RUS:ext</ta>
            <ta e="T494" id="Seg_10319" s="T483">RUS:ext</ta>
            <ta e="T502" id="Seg_10320" s="T495">RUS:ext</ta>
            <ta e="T548" id="Seg_10321" s="T541">RUS:ext</ta>
            <ta e="T566" id="Seg_10322" s="T561">RUS:ext</ta>
            <ta e="T622" id="Seg_10323" s="T609">RUS:ext</ta>
            <ta e="T631" id="Seg_10324" s="T623">RUS:ext</ta>
            <ta e="T643" id="Seg_10325" s="T632">RUS:ext</ta>
            <ta e="T655" id="Seg_10326" s="T644">RUS:ext</ta>
            <ta e="T664" id="Seg_10327" s="T655">RUS:ext</ta>
            <ta e="T670" id="Seg_10328" s="T665">RUS:ext</ta>
            <ta e="T674" id="Seg_10329" s="T671">RUS:ext</ta>
            <ta e="T681" id="Seg_10330" s="T675">RUS:ext</ta>
            <ta e="T686" id="Seg_10331" s="T682">RUS:ext</ta>
            <ta e="T691" id="Seg_10332" s="T687">RUS:ext</ta>
            <ta e="T701" id="Seg_10333" s="T692">RUS:ext</ta>
            <ta e="T706" id="Seg_10334" s="T702">RUS:ext</ta>
            <ta e="T712" id="Seg_10335" s="T707">RUS:ext</ta>
            <ta e="T716" id="Seg_10336" s="T713">RUS:ext</ta>
            <ta e="T724" id="Seg_10337" s="T722">RUS:ext</ta>
            <ta e="T738" id="Seg_10338" s="T728">RUS:ext</ta>
            <ta e="T750" id="Seg_10339" s="T739">RUS:ext</ta>
            <ta e="T759" id="Seg_10340" s="T751">RUS:ext</ta>
            <ta e="T765" id="Seg_10341" s="T760">RUS:ext</ta>
            <ta e="T772" id="Seg_10342" s="T766">RUS:ext</ta>
            <ta e="T776" id="Seg_10343" s="T773">RUS:ext</ta>
            <ta e="T778" id="Seg_10344" s="T777">RUS:ext</ta>
            <ta e="T782" id="Seg_10345" s="T779">RUS:ext</ta>
            <ta e="T790" id="Seg_10346" s="T783">RUS:ext</ta>
            <ta e="T800" id="Seg_10347" s="T791">RUS:ext</ta>
            <ta e="T803" id="Seg_10348" s="T801">RUS:ext</ta>
            <ta e="T823" id="Seg_10349" s="T819">RUS:ext</ta>
            <ta e="T834" id="Seg_10350" s="T828">RUS:ext</ta>
            <ta e="T845" id="Seg_10351" s="T841">RUS:ext</ta>
            <ta e="T849" id="Seg_10352" s="T847">RUS:ext</ta>
            <ta e="T859" id="Seg_10353" s="T850">RUS:ext</ta>
            <ta e="T1151" id="Seg_10354" s="T1149">RUS:ext</ta>
            <ta e="T1180" id="Seg_10355" s="T1165">RUS:ext</ta>
            <ta e="T1195" id="Seg_10356" s="T1181">RUS:ext</ta>
            <ta e="T1204" id="Seg_10357" s="T1196">RUS:ext</ta>
            <ta e="T1214" id="Seg_10358" s="T1205">RUS:ext</ta>
            <ta e="T1229" id="Seg_10359" s="T1215">RUS:ext</ta>
            <ta e="T1233" id="Seg_10360" s="T1230">RUS:ext</ta>
            <ta e="T1237" id="Seg_10361" s="T1234">RUS:ext</ta>
            <ta e="T1242" id="Seg_10362" s="T1238">RUS:ext</ta>
            <ta e="T1247" id="Seg_10363" s="T1243">RUS:ext</ta>
            <ta e="T1254" id="Seg_10364" s="T1248">RUS:ext</ta>
            <ta e="T1262" id="Seg_10365" s="T1255">RUS:ext</ta>
            <ta e="T1269" id="Seg_10366" s="T1263">RUS:ext</ta>
            <ta e="T1278" id="Seg_10367" s="T1270">RUS:ext</ta>
            <ta e="T1289" id="Seg_10368" s="T1279">RUS:ext</ta>
            <ta e="T1295" id="Seg_10369" s="T1290">RUS:ext</ta>
            <ta e="T1306" id="Seg_10370" s="T1296">RUS:ext</ta>
            <ta e="T1313" id="Seg_10371" s="T1307">RUS:ext</ta>
            <ta e="T1318" id="Seg_10372" s="T1314">RUS:ext</ta>
            <ta e="T1326" id="Seg_10373" s="T1318">RUS:ext</ta>
            <ta e="T1331" id="Seg_10374" s="T1327">RUS:ext</ta>
            <ta e="T1335" id="Seg_10375" s="T1332">RUS:ext</ta>
            <ta e="T1341" id="Seg_10376" s="T1336">RUS:ext</ta>
            <ta e="T1347" id="Seg_10377" s="T1342">RUS:ext</ta>
            <ta e="T1364" id="Seg_10378" s="T1348">RUS:ext</ta>
            <ta e="T1370" id="Seg_10379" s="T1365">RUS:ext</ta>
            <ta e="T1379" id="Seg_10380" s="T1371">RUS:ext</ta>
            <ta e="T1381" id="Seg_10381" s="T1380">RUS:ext</ta>
            <ta e="T1390" id="Seg_10382" s="T1383">RUS:ext</ta>
            <ta e="T1521" id="Seg_10383" s="T1516">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T137" id="Seg_10384" s="T128">Когда я жила в Тарту, я пошла и купила три платка.</ta>
            <ta e="T141" id="Seg_10385" s="T138">Чтобы подарить моим.</ta>
            <ta e="T147" id="Seg_10386" s="T142">Потом я приехала сюда, купила штаны.</ta>
            <ta e="T150" id="Seg_10387" s="T148">Рубашку купила.</ta>
            <ta e="T153" id="Seg_10388" s="T151">Сыну, племяннику.</ta>
            <ta e="T156" id="Seg_10389" s="T154">Дочери я купила…</ta>
            <ta e="T159" id="Seg_10390" s="T157">Платье купила.</ta>
            <ta e="T164" id="Seg_10391" s="T160">Племяннику купила рубашку.</ta>
            <ta e="T166" id="Seg_10392" s="T165">Хватит.</ta>
            <ta e="T253" id="Seg_10393" s="T250">Ботинки — штаны, ботинок.</ta>
            <ta e="T348" id="Seg_10394" s="T347">Рубашка.</ta>
            <ta e="T514" id="Seg_10395" s="T503">Когда он шел ко мне, Александр Константинович, он сказал: "Бабушка, я иду к тебе!"</ta>
            <ta e="T519" id="Seg_10396" s="T514">А я сказала: "Не приходи!</ta>
            <ta e="T522" id="Seg_10397" s="T520">(?) меня.</ta>
            <ta e="T525" id="Seg_10398" s="T523">Иди отсюда!</ta>
            <ta e="T529" id="Seg_10399" s="T526">Ты мне [надоел].</ta>
            <ta e="T532" id="Seg_10400" s="T530">Ты мне…</ta>
            <ta e="T537" id="Seg_10401" s="T533">Я с тобой не буду разговаривать.</ta>
            <ta e="T540" id="Seg_10402" s="T538">Иди отсюда".</ta>
            <ta e="T578" id="Seg_10403" s="T568">Когда он шел ко мне, я заругалась на него:</ta>
            <ta e="T581" id="Seg_10404" s="T578">"Не приходи ко мне!</ta>
            <ta e="T586" id="Seg_10405" s="T581">Я тебе ничего не расскажу.</ta>
            <ta e="T589" id="Seg_10406" s="T587">Иди отсюда!"</ta>
            <ta e="T596" id="Seg_10407" s="T590">Тогда он сказал: "Ты очень хорошо говоришь".</ta>
            <ta e="T603" id="Seg_10408" s="T597">Я сказала: "Прямо хорошо, я ругаюсь на тебя.</ta>
            <ta e="T608" id="Seg_10409" s="T604">А ты говоришь: хорошо".</ta>
            <ta e="T866" id="Seg_10410" s="T860">Когда Александр Константинович ехал ко мне…</ta>
            <ta e="T873" id="Seg_10411" s="T867">Он написал мне: "Я к тебе приеду".</ta>
            <ta e="T878" id="Seg_10412" s="T874">Я ждала-жала — нет.</ta>
            <ta e="T886" id="Seg_10413" s="T879">Потом пишет мне письмо: "Я не приеду.</ta>
            <ta e="T895" id="Seg_10414" s="T887">Я на (следующий год?) приеду к тебе".</ta>
            <ta e="T902" id="Seg_10415" s="T896">Я говорю: "Я может умру.</ta>
            <ta e="T907" id="Seg_10416" s="T903">А ты тогда приедешь".</ta>
            <ta e="T913" id="Seg_10417" s="T908">Тогда я собралась и приехала сюда.</ta>
            <ta e="T920" id="Seg_10418" s="T916">Моя дочка вышла замуж.</ta>
            <ta e="T926" id="Seg_10419" s="T921">Я была в Красноярске, она сказала:</ta>
            <ta e="T933" id="Seg_10420" s="T926">"Меня один парень [хочет] взять, отцу не говори".</ta>
            <ta e="T938" id="Seg_10421" s="T934">А я сказала:</ta>
            <ta e="T950" id="Seg_10422" s="T938">"Как не говорить, отец сам себе взял [жену], и вас родил".</ta>
            <ta e="T959" id="Seg_10423" s="T951">Потом я пришла к отцу, рассказала, он написал ей письмо.</ta>
            <ta e="T968" id="Seg_10424" s="T960">Потом она ему написала, он приехал сюда.</ta>
            <ta e="T980" id="Seg_10425" s="T969">Потом мы зарезали свинью, [приготовили] мясо, много хлеба напекли, собрали людей.</ta>
            <ta e="T984" id="Seg_10426" s="T981">И водка была.</ta>
            <ta e="T988" id="Seg_10427" s="T985">Мы сделали пельменей, котлет.</ta>
            <ta e="T992" id="Seg_10428" s="T988">Много все было.</ta>
            <ta e="T997" id="Seg_10429" s="T993">И людей много было.</ta>
            <ta e="T1000" id="Seg_10430" s="T998">[Из] Агинского приехали.</ta>
            <ta e="T1006" id="Seg_10431" s="T1001">Потом я купила ей пальто.</ta>
            <ta e="T1012" id="Seg_10432" s="T1007">Дала ей деньги.</ta>
            <ta e="T1017" id="Seg_10433" s="T1013">Она уехала с этим парнем.</ta>
            <ta e="T1028" id="Seg_10434" s="T1018">Туда, где люди говорят на своем языке.</ta>
            <ta e="T1044" id="Seg_10435" s="T1029">Потом мать парня ее не полюбила, она пишет сюда:</ta>
            <ta e="T1049" id="Seg_10436" s="T1044">"Я каждый день плачу.</ta>
            <ta e="T1055" id="Seg_10437" s="T1050">Мы стали ей писать:</ta>
            <ta e="T1057" id="Seg_10438" s="T1055">"Приезжай домой".</ta>
            <ta e="T1064" id="Seg_10439" s="T1058">Тогда она говорит: "Я одна приеду".</ta>
            <ta e="T1069" id="Seg_10440" s="T1065">"А почему ты одна приедешь?"</ta>
            <ta e="T1076" id="Seg_10441" s="T1070">Она опять пишет: "Денег нет".</ta>
            <ta e="T1085" id="Seg_10442" s="T1077">Я снова послала ей (двадцать?) рублей.</ta>
            <ta e="T1092" id="Seg_10443" s="T1086">Отец послал (пятьдесят?).</ta>
            <ta e="T1097" id="Seg_10444" s="T1093">Потом она приехала одна.</ta>
            <ta e="T1100" id="Seg_10445" s="T1098">Он остался.</ta>
            <ta e="T1107" id="Seg_10446" s="T1101">Потом она ему писала, писала.</ta>
            <ta e="T1113" id="Seg_10447" s="T1108">Он все не приезжает.</ta>
            <ta e="T1117" id="Seg_10448" s="T1114">Тогда она уехала.</ta>
            <ta e="T1124" id="Seg_10449" s="T1118">В ту деревню, где жил Ленин.</ta>
            <ta e="T1139" id="Seg_10450" s="T1125">Там она в столовой хлеб выдает, водку выдает, все выдает, [так] она пишет.</ta>
            <ta e="T1148" id="Seg_10451" s="T1140">Тридцать пять рублей получает.</ta>
            <ta e="T1396" id="Seg_10452" s="T1391">Дочь моей племянницы вышла замуж.</ta>
            <ta e="T1402" id="Seg_10453" s="T1397">И у нее было пять детей.</ta>
            <ta e="T1410" id="Seg_10454" s="T1403">Потом они уехали домой там.</ta>
            <ta e="T1414" id="Seg_10455" s="T1411">[?]</ta>
            <ta e="T1422" id="Seg_10456" s="T1415">Потом они строили дорогу, железную дорогу.</ta>
            <ta e="T1433" id="Seg_10457" s="T1423">Потом они поехали на север, там тоже строили дорогу.</ta>
            <ta e="T1436" id="Seg_10458" s="T1434">Железную дорогу.</ta>
            <ta e="T1442" id="Seg_10459" s="T1437">Потом она приехала домой, к матери.</ta>
            <ta e="T1448" id="Seg_10460" s="T1443">Потом она была здесь.</ta>
            <ta e="T1453" id="Seg_10461" s="T1449">Потом написали письмо:</ta>
            <ta e="T1457" id="Seg_10462" s="T1453">"Твой муж утонул".</ta>
            <ta e="T1461" id="Seg_10463" s="T1458">Тогда она поехала.</ta>
            <ta e="T1465" id="Seg_10464" s="T1462">Его похоронили.</ta>
            <ta e="T1471" id="Seg_10465" s="T1466">Потом у нее опять был муж. [?]</ta>
            <ta e="T1475" id="Seg_10466" s="T1472">У него не было жены.</ta>
            <ta e="T1484" id="Seg_10467" s="T1476">И сейчас они живут, [а] у нее было пять детей.</ta>
            <ta e="T1488" id="Seg_10468" s="T1485">Он взял ее [замуж].</ta>
            <ta e="T1492" id="Seg_10469" s="T1489">Очень хороший человек.</ta>
            <ta e="T1496" id="Seg_10470" s="T1493">Водку не пьет.</ta>
            <ta e="T1501" id="Seg_10471" s="T1496">Никуда не ходит, все время работает.</ta>
            <ta e="T1508" id="Seg_10472" s="T1501">(У него?) тоже нет матери, только отец.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T137" id="Seg_10473" s="T128">When I was in Tartu, I went and bought three scarps.</ta>
            <ta e="T141" id="Seg_10474" s="T138">To give them to my [relatives].</ta>
            <ta e="T147" id="Seg_10475" s="T142">Then I came here, I bought trousers.</ta>
            <ta e="T150" id="Seg_10476" s="T148">I bought a shirt.</ta>
            <ta e="T153" id="Seg_10477" s="T151">For my son, for my nephew.</ta>
            <ta e="T156" id="Seg_10478" s="T154">For my daughter, I bought…</ta>
            <ta e="T159" id="Seg_10479" s="T157">I bought a dress.</ta>
            <ta e="T164" id="Seg_10480" s="T160">For my nephew I bought a shirt.</ta>
            <ta e="T166" id="Seg_10481" s="T165">That's all.</ta>
            <ta e="T253" id="Seg_10482" s="T250">Shoes — trousers, shoe.</ta>
            <ta e="T348" id="Seg_10483" s="T347">Shirt.</ta>
            <ta e="T514" id="Seg_10484" s="T503">When he was coming to me, Aleksandr Konstantinovich, he said: "Grandma, I'm coming to you."</ta>
            <ta e="T519" id="Seg_10485" s="T514">And I said: "Don't come.</ta>
            <ta e="T522" id="Seg_10486" s="T520">(?) me.</ta>
            <ta e="T525" id="Seg_10487" s="T523">Go away!</ta>
            <ta e="T529" id="Seg_10488" s="T526">I'm [sick] of you.</ta>
            <ta e="T532" id="Seg_10489" s="T530">You (?) me.</ta>
            <ta e="T537" id="Seg_10490" s="T533">I won't speak with you.</ta>
            <ta e="T540" id="Seg_10491" s="T538">Go away!"</ta>
            <ta e="T578" id="Seg_10492" s="T568">When he was coming to me, I scolded at him:</ta>
            <ta e="T581" id="Seg_10493" s="T578">"Don't come to me!</ta>
            <ta e="T586" id="Seg_10494" s="T581">I won't tell anything to you.</ta>
            <ta e="T589" id="Seg_10495" s="T587">Go away!"</ta>
            <ta e="T596" id="Seg_10496" s="T590">Then he says: "You're speaking very well."</ta>
            <ta e="T603" id="Seg_10497" s="T597">I say: "Why well, I'm scolding at you.</ta>
            <ta e="T608" id="Seg_10498" s="T604">And you say: well."</ta>
            <ta e="T866" id="Seg_10499" s="T860">When Aleksandr Konstantinovich was coming to me…</ta>
            <ta e="T873" id="Seg_10500" s="T867">He wrote me: "I'll come to you."</ta>
            <ta e="T878" id="Seg_10501" s="T874">I was waiting for nothing.</ta>
            <ta e="T886" id="Seg_10502" s="T879">Then he wrote me a letter: "I won't come.</ta>
            <ta e="T895" id="Seg_10503" s="T887">I'll come to you (next year?)".</ta>
            <ta e="T902" id="Seg_10504" s="T896">I say: "Maybe I'll die.</ta>
            <ta e="T907" id="Seg_10505" s="T903">And then you''ll come."</ta>
            <ta e="T913" id="Seg_10506" s="T908">Then I packed up and came here.</ta>
            <ta e="T920" id="Seg_10507" s="T916">My daughter had married.</ta>
            <ta e="T926" id="Seg_10508" s="T921">I was in Krasnoyarsk, she said:</ta>
            <ta e="T933" id="Seg_10509" s="T926">"A boy [wants] to marry me, don't tell my father."</ta>
            <ta e="T938" id="Seg_10510" s="T934">I said:</ta>
            <ta e="T950" id="Seg_10511" s="T938">"How won't I say, your father himself took a wife, and gave birth to you."</ta>
            <ta e="T959" id="Seg_10512" s="T951">Then I came to her father, told him, he wrote her a letter.</ta>
            <ta e="T968" id="Seg_10513" s="T960">Then she wrote him a letter, he came here.</ta>
            <ta e="T980" id="Seg_10514" s="T969">Then we slaughtered a pig, [cooked] meat, baked a lot of bread, invited people.</ta>
            <ta e="T984" id="Seg_10515" s="T981">There was vodka.</ta>
            <ta e="T988" id="Seg_10516" s="T985">We made pelmenis, croquettes.</ta>
            <ta e="T992" id="Seg_10517" s="T988">There were many things.</ta>
            <ta e="T997" id="Seg_10518" s="T993">And there were many people.</ta>
            <ta e="T1000" id="Seg_10519" s="T998">[Some] came [from] Aginskoye.</ta>
            <ta e="T1006" id="Seg_10520" s="T1001">Then I bought her a coat.</ta>
            <ta e="T1012" id="Seg_10521" s="T1007">I gave her money.</ta>
            <ta e="T1017" id="Seg_10522" s="T1013">She went away with this boy.</ta>
            <ta e="T1028" id="Seg_10523" s="T1018">Where people speak their own language.</ta>
            <ta e="T1044" id="Seg_10524" s="T1029">The boy's mother didn't love her, she writes to us:</ta>
            <ta e="T1049" id="Seg_10525" s="T1044">"I cry every day.</ta>
            <ta e="T1055" id="Seg_10526" s="T1050">We wrote her:</ta>
            <ta e="T1057" id="Seg_10527" s="T1055">"Come home."</ta>
            <ta e="T1064" id="Seg_10528" s="T1058">Then she says: "I'll come alone."</ta>
            <ta e="T1069" id="Seg_10529" s="T1065">"Why will you come alone?"</ta>
            <ta e="T1076" id="Seg_10530" s="T1070">She wrote again: "I don't have money."</ta>
            <ta e="T1085" id="Seg_10531" s="T1077">I sent again (twenty?) roubles.</ta>
            <ta e="T1092" id="Seg_10532" s="T1086">Her father sent (fifty?).</ta>
            <ta e="T1097" id="Seg_10533" s="T1093">Then she came alone.</ta>
            <ta e="T1100" id="Seg_10534" s="T1098">He remained [there].</ta>
            <ta e="T1107" id="Seg_10535" s="T1101">Then she wrote to him many times.</ta>
            <ta e="T1113" id="Seg_10536" s="T1108">He never came.</ta>
            <ta e="T1117" id="Seg_10537" s="T1114">Then she went.</ta>
            <ta e="T1124" id="Seg_10538" s="T1118">To that village, where Lenin had lived.</ta>
            <ta e="T1139" id="Seg_10539" s="T1125">There she [works] in a canteen, she serves bread, she serves vodka, she serves everything, [so] she writes.</ta>
            <ta e="T1148" id="Seg_10540" s="T1140">She gets thirty-five roubles [a month].</ta>
            <ta e="T1396" id="Seg_10541" s="T1391">My niece's daughter married.</ta>
            <ta e="T1402" id="Seg_10542" s="T1397">And she had five children.</ta>
            <ta e="T1410" id="Seg_10543" s="T1403">Then they went home.</ta>
            <ta e="T1414" id="Seg_10544" s="T1411">[?]</ta>
            <ta e="T1422" id="Seg_10545" s="T1415">Then they were building a road, a railroad.</ta>
            <ta e="T1433" id="Seg_10546" s="T1423">Then they went to the North, there they were building a road, too.</ta>
            <ta e="T1436" id="Seg_10547" s="T1434">A railroad.</ta>
            <ta e="T1442" id="Seg_10548" s="T1437">Then she came home, to her mother.</ta>
            <ta e="T1448" id="Seg_10549" s="T1443">Then she was here.</ta>
            <ta e="T1453" id="Seg_10550" s="T1449">There [someone] wrote a letter:</ta>
            <ta e="T1457" id="Seg_10551" s="T1453">"Your husband has drowned."</ta>
            <ta e="T1461" id="Seg_10552" s="T1458">Then she went.</ta>
            <ta e="T1465" id="Seg_10553" s="T1462">They buried him.</ta>
            <ta e="T1471" id="Seg_10554" s="T1466">Then she had another husband. [?]</ta>
            <ta e="T1475" id="Seg_10555" s="T1472">He had no wife.</ta>
            <ta e="T1484" id="Seg_10556" s="T1476">And they still live together, though she had had five children.</ta>
            <ta e="T1488" id="Seg_10557" s="T1485">He took [= married] her.</ta>
            <ta e="T1492" id="Seg_10558" s="T1489">A very good man.</ta>
            <ta e="T1496" id="Seg_10559" s="T1493">He drinks no vodka.</ta>
            <ta e="T1501" id="Seg_10560" s="T1496">He doesn't go anywhere, he always works.</ta>
            <ta e="T1508" id="Seg_10561" s="T1501">(He?) has no mother, too, only a father.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T137" id="Seg_10562" s="T128">Als ich in Tartu war, bin ich losgegangen und habe drei Schals gekauft.</ta>
            <ta e="T141" id="Seg_10563" s="T138">Um sie meinen Verwandten zu geben.</ta>
            <ta e="T147" id="Seg_10564" s="T142">Dann bin ich hierhin gekommen, ich habe Hosen gekauft.</ta>
            <ta e="T150" id="Seg_10565" s="T148">Ich habe ein T-shirt gekauft.</ta>
            <ta e="T153" id="Seg_10566" s="T151">Für meinen Sohn, für meinen Neffen.</ta>
            <ta e="T156" id="Seg_10567" s="T154">Für meine Tochter, ich kaufte…</ta>
            <ta e="T159" id="Seg_10568" s="T157">Ich habe ein Kleid gekauft.</ta>
            <ta e="T164" id="Seg_10569" s="T160">Für meinen Neffen habe ich ein Hemd gekauft.</ta>
            <ta e="T166" id="Seg_10570" s="T165">Das ist alles.</ta>
            <ta e="T253" id="Seg_10571" s="T250">Schuhe - Hosen, Schuhe.</ta>
            <ta e="T348" id="Seg_10572" s="T347">Hemd.</ta>
            <ta e="T514" id="Seg_10573" s="T503">Als er zu mir kam, Aleksandr Konstantinovich, sagte er: "Großmutter, ich komme zu dir."</ta>
            <ta e="T519" id="Seg_10574" s="T514">Und ich sagte: "Komm nicht.</ta>
            <ta e="T522" id="Seg_10575" s="T520">(?) mir.</ta>
            <ta e="T525" id="Seg_10576" s="T523">Geh weg!</ta>
            <ta e="T529" id="Seg_10577" s="T526">Ich habe genug von dir.</ta>
            <ta e="T532" id="Seg_10578" s="T530">Du (?) mir.</ta>
            <ta e="T537" id="Seg_10579" s="T533">Ich werde nicht mit dir sprechen.</ta>
            <ta e="T540" id="Seg_10580" s="T538">Geh weg!"</ta>
            <ta e="T578" id="Seg_10581" s="T568">Als er zu mir kam, scholt ich ihn: </ta>
            <ta e="T581" id="Seg_10582" s="T578">"Komm nicht zu mir!</ta>
            <ta e="T586" id="Seg_10583" s="T581">Ich werde dir nichts sagen.</ta>
            <ta e="T589" id="Seg_10584" s="T587">Geh weg!"</ta>
            <ta e="T596" id="Seg_10585" s="T590">Dann sagte er: "Du sprichst sehr gut."</ta>
            <ta e="T603" id="Seg_10586" s="T597">Ich sage: "Warum gut, ich schimpfe mit dir.</ta>
            <ta e="T608" id="Seg_10587" s="T604">Und du sagst: gut."</ta>
            <ta e="T866" id="Seg_10588" s="T860">Als Aleksandr Konstantinovich zu mir gekommen ist…</ta>
            <ta e="T873" id="Seg_10589" s="T867">Er schrieb mir: "Ich werde zu dir kommen."</ta>
            <ta e="T878" id="Seg_10590" s="T874">Ich wartete umsonst.</ta>
            <ta e="T886" id="Seg_10591" s="T879">Dann schrieb er mir einen Brief: "Ich werde nicht kommen.</ta>
            <ta e="T895" id="Seg_10592" s="T887">Ich werde (nächstes Jahr?) zu dir kommen."</ta>
            <ta e="T902" id="Seg_10593" s="T896">Ich sage: "Vielleicht werde ich sterben.</ta>
            <ta e="T907" id="Seg_10594" s="T903">Und dann wirst du kommen."</ta>
            <ta e="T913" id="Seg_10595" s="T908">Dann habe ich zusammengepackt und bin hierhin gekommen.</ta>
            <ta e="T920" id="Seg_10596" s="T916">Meine Tochter hat geheiratet.</ta>
            <ta e="T926" id="Seg_10597" s="T921">Ich war in Krasnojarsk, sie sagte: </ta>
            <ta e="T933" id="Seg_10598" s="T926">"Ein Junge [will] mich heiraten, sag es nicht meinem Vater."</ta>
            <ta e="T938" id="Seg_10599" s="T934">Ich sagte: </ta>
            <ta e="T950" id="Seg_10600" s="T938">"Wie werde ich es nicht sagen, dein Vater hat selbst geheiratet und dich auf die Welt gebracht."</ta>
            <ta e="T959" id="Seg_10601" s="T951">Dann bin ich zu ihrem Vaer gekommen, erzählte es ihm, er schrieb ihr einen Brief.</ta>
            <ta e="T968" id="Seg_10602" s="T960">Dann schrieb sie ihm einen Brief, er kam hierhin.</ta>
            <ta e="T980" id="Seg_10603" s="T969">Dann schlachteten wir ein Schwein, [kochten] das Fleisch, bucken viel Brot, luden Leute ein.</ta>
            <ta e="T984" id="Seg_10604" s="T981">Es gab Wodka.</ta>
            <ta e="T988" id="Seg_10605" s="T985">Wir machten, Pelmenis, Frikadellen.</ta>
            <ta e="T992" id="Seg_10606" s="T988">Es gab viele Dinge.</ta>
            <ta e="T997" id="Seg_10607" s="T993">Und es waren viele Menschen dort.</ta>
            <ta e="T1000" id="Seg_10608" s="T998">[Einige] kamen [aus] Aginskoye. </ta>
            <ta e="T1006" id="Seg_10609" s="T1001">Dann kaufte ich ihr einen Mantel.</ta>
            <ta e="T1012" id="Seg_10610" s="T1007">Ich gab ihr Geld.</ta>
            <ta e="T1017" id="Seg_10611" s="T1013">Sie ging mit dem Jungen weg.</ta>
            <ta e="T1028" id="Seg_10612" s="T1018">Dorthin, wo Menschen ihre eigene Sprache sprachen.</ta>
            <ta e="T1044" id="Seg_10613" s="T1029">Die Mutter des Jungen mochte sie nicht, sie schreibt uns: </ta>
            <ta e="T1049" id="Seg_10614" s="T1044">"Ich weine jeden Tag."</ta>
            <ta e="T1055" id="Seg_10615" s="T1050">Wir schrieben ihr: </ta>
            <ta e="T1057" id="Seg_10616" s="T1055">"Komm nach Hause."</ta>
            <ta e="T1064" id="Seg_10617" s="T1058">Dann sagte sie: "Ich werde alleine kommen."</ta>
            <ta e="T1069" id="Seg_10618" s="T1065">"Warum wirst du alleine kommen?"</ta>
            <ta e="T1076" id="Seg_10619" s="T1070">Sie schrieb wieder: "Ich habe kein Geld."</ta>
            <ta e="T1085" id="Seg_10620" s="T1077">Ich habe wieder (zwanzig?) Rubel geschickt.</ta>
            <ta e="T1092" id="Seg_10621" s="T1086">Ihr Vater hat 50 Rubel geschickt.</ta>
            <ta e="T1097" id="Seg_10622" s="T1093">Dann kam sie alleine.</ta>
            <ta e="T1100" id="Seg_10623" s="T1098">Er blieb dort.</ta>
            <ta e="T1107" id="Seg_10624" s="T1101">Dann schrieb sie ihm viele Male.</ta>
            <ta e="T1113" id="Seg_10625" s="T1108">Er kam niemals.</ta>
            <ta e="T1117" id="Seg_10626" s="T1114">Dann ging sie.</ta>
            <ta e="T1124" id="Seg_10627" s="T1118">Zu dem Dorf, in dem Lenin gelebt hatte.</ta>
            <ta e="T1139" id="Seg_10628" s="T1125">Dort arbeitet sie in einer Kantine, sie serviert Brot, Wodka, alles, so schreibt sie.</ta>
            <ta e="T1148" id="Seg_10629" s="T1140">Sie bekommt fünfunddreißig Rubel [im Monat]</ta>
            <ta e="T1396" id="Seg_10630" s="T1391">Die Tochter meiner Nichte hat geheiratet.</ta>
            <ta e="T1402" id="Seg_10631" s="T1397">Und sie hatte fünf Kinder.</ta>
            <ta e="T1410" id="Seg_10632" s="T1403">Dann gingen sie nach Hause.</ta>
            <ta e="T1414" id="Seg_10633" s="T1411">[?]</ta>
            <ta e="T1422" id="Seg_10634" s="T1415">Dann bauten sie eine Straße, Schienen.</ta>
            <ta e="T1433" id="Seg_10635" s="T1423">Dann gingen sie nach Norden, dort haben sie auch Straßen gebaut.</ta>
            <ta e="T1436" id="Seg_10636" s="T1434">Schienen.</ta>
            <ta e="T1442" id="Seg_10637" s="T1437">Dann kam sie nach Hause, zu ihrer Mutter.</ta>
            <ta e="T1448" id="Seg_10638" s="T1443">Dann war sie hier.</ta>
            <ta e="T1453" id="Seg_10639" s="T1449">Hier schrieb [jemand] einen Brief: </ta>
            <ta e="T1457" id="Seg_10640" s="T1453">"Dein Ehemann ist ertrunken."</ta>
            <ta e="T1461" id="Seg_10641" s="T1458">Dann ging sie.</ta>
            <ta e="T1465" id="Seg_10642" s="T1462">Sie beerdigten ihn.</ta>
            <ta e="T1471" id="Seg_10643" s="T1466">Dann hatte sie einen anderen Ehemann. [?]</ta>
            <ta e="T1475" id="Seg_10644" s="T1472">Er hatte keine Ehefrau.</ta>
            <ta e="T1484" id="Seg_10645" s="T1476">Und sie leben immer noch zusammen, obwohl sie fünf Kinder hatte.</ta>
            <ta e="T1488" id="Seg_10646" s="T1485">Er hat sie geheiratet.</ta>
            <ta e="T1492" id="Seg_10647" s="T1489">Ein sehr guter Mann.</ta>
            <ta e="T1496" id="Seg_10648" s="T1493">Er trinkt keinen Wodka.</ta>
            <ta e="T1501" id="Seg_10649" s="T1496">Er geht nirgens hin, er arbeitet immer.</ta>
            <ta e="T1508" id="Seg_10650" s="T1501">(Er?) hat auch keine Mutter, nur einen Vater.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T603" id="Seg_10651" s="T597">[GVY:] kundolliam</ta>
            <ta e="T691" id="Seg_10652" s="T687">[GVY:] Напрок - next year.</ta>
            <ta e="T920" id="Seg_10653" s="T916">[GVY:] She means her niece, see the Russian translation below.</ta>
            <ta e="T1028" id="Seg_10654" s="T1018">[GVY:] This sentence is unclear. PKZ probably means some aboriginal village near Krasnoyarsk.</ta>
            <ta e="T1085" id="Seg_10655" s="T1077">[GVY:] The numbers are not clear. In the Russian version (see below) PKZ mentions 100 and 50 roubles, respectively.</ta>
            <ta e="T1124" id="Seg_10656" s="T1118">[GVY:] To Shushenskoye.</ta>
            <ta e="T1410" id="Seg_10657" s="T1403">[GVY:] The translation is tentative.</ta>
            <ta e="T1414" id="Seg_10658" s="T1411">[GVY:] The sentence is unclear and is missing from the Russian version (see PKZ_19700819_09343_1a). </ta>
            <ta e="T1508" id="Seg_10659" s="T1501">[GVY:] Or măn 'I…'</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T113" start="T103">
            <tli id="T103.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T285" start="T276">
            <tli id="T276.tx-KA.1" />
            <tli id="T276.tx-KA.2" />
            <tli id="T276.tx-KA.3" />
            <tli id="T276.tx-KA.4" />
            <tli id="T276.tx-KA.5" />
            <tli id="T276.tx-KA.6" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T13" id="Seg_10660" n="sc" s="T10">
               <ts e="T13" id="Seg_10662" n="HIAT:u" s="T10">
                  <nts id="Seg_10663" n="HIAT:ip">(</nts>
                  <nts id="Seg_10664" n="HIAT:ip">(</nts>
                  <ats e="T11" id="Seg_10665" n="HIAT:non-pho" s="T10">BRK</ats>
                  <nts id="Seg_10666" n="HIAT:ip">)</nts>
                  <nts id="Seg_10667" n="HIAT:ip">)</nts>
                  <nts id="Seg_10668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_10670" n="HIAT:w" s="T11">Или</ts>
                  <nts id="Seg_10671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_10673" n="HIAT:w" s="T12">так</ts>
                  <nts id="Seg_10674" n="HIAT:ip">.</nts>
                  <nts id="Seg_10675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_10676" n="sc" s="T14">
               <ts e="T16" id="Seg_10678" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_10680" n="HIAT:w" s="T14">Или</ts>
                  <nts id="Seg_10681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_10683" n="HIAT:w" s="T15">что</ts>
                  <nts id="Seg_10684" n="HIAT:ip">?</nts>
                  <nts id="Seg_10685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_10686" n="sc" s="T18">
               <ts e="T25" id="Seg_10688" n="HIAT:u" s="T18">
                  <ts e="T20" id="Seg_10690" n="HIAT:w" s="T18">Я</ts>
                  <nts id="Seg_10691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_10693" n="HIAT:w" s="T20">уж</ts>
                  <nts id="Seg_10694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_10696" n="HIAT:w" s="T21">тоже</ts>
                  <nts id="Seg_10697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_10699" n="HIAT:w" s="T22">не</ts>
                  <nts id="Seg_10700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_10702" n="HIAT:w" s="T24">помню</ts>
                  <nts id="Seg_10703" n="HIAT:ip">.</nts>
                  <nts id="Seg_10704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_10705" n="sc" s="T31">
               <ts e="T32" id="Seg_10707" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_10709" n="HIAT:w" s="T31">Ага</ts>
                  <nts id="Seg_10710" n="HIAT:ip">.</nts>
                  <nts id="Seg_10711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_10712" n="sc" s="T33">
               <ts e="T41" id="Seg_10714" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_10716" n="HIAT:w" s="T33">Ну</ts>
                  <nts id="Seg_10717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_10719" n="HIAT:w" s="T34">неважно</ts>
                  <nts id="Seg_10720" n="HIAT:ip">,</nts>
                  <nts id="Seg_10721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_10723" n="HIAT:w" s="T35">пускай</ts>
                  <nts id="Seg_10724" n="HIAT:ip">,</nts>
                  <nts id="Seg_10725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_10727" n="HIAT:w" s="T36">поговорим</ts>
                  <nts id="Seg_10728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_10730" n="HIAT:w" s="T37">пока</ts>
                  <nts id="Seg_10731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10732" n="HIAT:ip">(</nts>
                  <nts id="Seg_10733" n="HIAT:ip">(</nts>
                  <ats e="T39" id="Seg_10734" n="HIAT:non-pho" s="T38">…</ats>
                  <nts id="Seg_10735" n="HIAT:ip">)</nts>
                  <nts id="Seg_10736" n="HIAT:ip">)</nts>
                  <nts id="Seg_10737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_10739" n="HIAT:w" s="T39">что-то</ts>
                  <nts id="Seg_10740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_10742" n="HIAT:w" s="T40">новое</ts>
                  <nts id="Seg_10743" n="HIAT:ip">.</nts>
                  <nts id="Seg_10744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T43" id="Seg_10745" n="sc" s="T42">
               <ts e="T43" id="Seg_10747" n="HIAT:u" s="T42">
                  <nts id="Seg_10748" n="HIAT:ip">(</nts>
                  <nts id="Seg_10749" n="HIAT:ip">(</nts>
                  <ats e="T43" id="Seg_10750" n="HIAT:non-pho" s="T42">…</ats>
                  <nts id="Seg_10751" n="HIAT:ip">)</nts>
                  <nts id="Seg_10752" n="HIAT:ip">)</nts>
                  <nts id="Seg_10753" n="HIAT:ip">.</nts>
                  <nts id="Seg_10754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_10755" n="sc" s="T48">
               <ts e="T50" id="Seg_10757" n="HIAT:u" s="T48">
                  <ts e="T50" id="Seg_10759" n="HIAT:w" s="T48">Ага</ts>
                  <nts id="Seg_10760" n="HIAT:ip">.</nts>
                  <nts id="Seg_10761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T58" id="Seg_10762" n="sc" s="T56">
               <ts e="T58" id="Seg_10764" n="HIAT:u" s="T56">
                  <ts e="T58" id="Seg_10766" n="HIAT:w" s="T56">Ага</ts>
                  <nts id="Seg_10767" n="HIAT:ip">.</nts>
                  <nts id="Seg_10768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T96" id="Seg_10769" n="sc" s="T95">
               <ts e="T96" id="Seg_10771" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_10773" n="HIAT:w" s="T95">Хорошо</ts>
                  <nts id="Seg_10774" n="HIAT:ip">…</nts>
                  <nts id="Seg_10775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T113" id="Seg_10776" n="sc" s="T97">
               <ts e="T103" id="Seg_10778" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_10780" n="HIAT:w" s="T97">Ну</ts>
                  <nts id="Seg_10781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_10783" n="HIAT:w" s="T98">что</ts>
                  <nts id="Seg_10784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_10786" n="HIAT:w" s="T99">тебе</ts>
                  <nts id="Seg_10787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_10789" n="HIAT:w" s="T100">помирать</ts>
                  <nts id="Seg_10790" n="HIAT:ip">,</nts>
                  <nts id="Seg_10791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_10793" n="HIAT:w" s="T101">еще</ts>
                  <nts id="Seg_10794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_10796" n="HIAT:w" s="T102">молодая</ts>
                  <nts id="Seg_10797" n="HIAT:ip">.</nts>
                  <nts id="Seg_10798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_10800" n="HIAT:u" s="T103">
                  <nts id="Seg_10801" n="HIAT:ip">(</nts>
                  <nts id="Seg_10802" n="HIAT:ip">(</nts>
                  <ats e="T103.tx-KA.1" id="Seg_10803" n="HIAT:non-pho" s="T103">LAUGH</ats>
                  <nts id="Seg_10804" n="HIAT:ip">)</nts>
                  <nts id="Seg_10805" n="HIAT:ip">)</nts>
                  <nts id="Seg_10806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_10807" n="HIAT:ip">(</nts>
                  <nts id="Seg_10808" n="HIAT:ip">(</nts>
                  <ats e="T113" id="Seg_10809" n="HIAT:non-pho" s="T103.tx-KA.1">COUGH</ats>
                  <nts id="Seg_10810" n="HIAT:ip">)</nts>
                  <nts id="Seg_10811" n="HIAT:ip">)</nts>
                  <nts id="Seg_10812" n="HIAT:ip">.</nts>
                  <nts id="Seg_10813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_10814" n="sc" s="T118">
               <ts e="T120" id="Seg_10816" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_10818" n="HIAT:w" s="T118">Ну</ts>
                  <nts id="Seg_10819" n="HIAT:ip">,</nts>
                  <nts id="Seg_10820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_10822" n="HIAT:w" s="T119">ну</ts>
                  <nts id="Seg_10823" n="HIAT:ip">.</nts>
                  <nts id="Seg_10824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_10825" n="sc" s="T121">
               <ts e="T122" id="Seg_10827" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_10829" n="HIAT:w" s="T121">Обязательно</ts>
                  <nts id="Seg_10830" n="HIAT:ip">.</nts>
                  <nts id="Seg_10831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T125" id="Seg_10832" n="sc" s="T123">
               <ts e="T125" id="Seg_10834" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_10836" n="HIAT:w" s="T123">Можно</ts>
                  <nts id="Seg_10837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_10839" n="HIAT:w" s="T124">пускать</ts>
                  <nts id="Seg_10840" n="HIAT:ip">?</nts>
                  <nts id="Seg_10841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_10842" n="sc" s="T167">
               <ts e="T172" id="Seg_10844" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_10846" n="HIAT:w" s="T167">Ну</ts>
                  <nts id="Seg_10847" n="HIAT:ip">,</nts>
                  <nts id="Seg_10848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_10850" n="HIAT:w" s="T168">и</ts>
                  <nts id="Seg_10851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_10853" n="HIAT:w" s="T169">по-русски</ts>
                  <nts id="Seg_10854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_10856" n="HIAT:w" s="T170">можешь</ts>
                  <nts id="Seg_10857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_10859" n="HIAT:w" s="T171">сразу</ts>
                  <nts id="Seg_10860" n="HIAT:ip">.</nts>
                  <nts id="Seg_10861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_10862" n="sc" s="T207">
               <ts e="T213" id="Seg_10864" n="HIAT:u" s="T207">
                  <ts e="T209" id="Seg_10866" n="HIAT:w" s="T207">Ты</ts>
                  <nts id="Seg_10867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_10869" n="HIAT:w" s="T209">еще</ts>
                  <nts id="Seg_10870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_10872" n="HIAT:w" s="T211">сказала</ts>
                  <nts id="Seg_10873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_10875" n="HIAT:w" s="T212">jamaʔi</ts>
                  <nts id="Seg_10876" n="HIAT:ip">.</nts>
                  <nts id="Seg_10877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T218" id="Seg_10878" n="sc" s="T214">
               <ts e="T218" id="Seg_10880" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_10882" n="HIAT:w" s="T214">Ты</ts>
                  <nts id="Seg_10883" n="HIAT:ip">,</nts>
                  <nts id="Seg_10884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_10886" n="HIAT:w" s="T215">наверное</ts>
                  <nts id="Seg_10887" n="HIAT:ip">,</nts>
                  <nts id="Seg_10888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_10890" n="HIAT:w" s="T216">сапоги</ts>
                  <nts id="Seg_10891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_10893" n="HIAT:w" s="T217">купила</ts>
                  <nts id="Seg_10894" n="HIAT:ip">.</nts>
                  <nts id="Seg_10895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T229" id="Seg_10896" n="sc" s="T222">
               <ts e="T229" id="Seg_10898" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_10900" n="HIAT:w" s="T222">А</ts>
                  <nts id="Seg_10901" n="HIAT:ip">,</nts>
                  <nts id="Seg_10902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_10904" n="HIAT:w" s="T223">штаны</ts>
                  <nts id="Seg_10905" n="HIAT:ip">,</nts>
                  <nts id="Seg_10906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_10908" n="HIAT:w" s="T225">штаны</ts>
                  <nts id="Seg_10909" n="HIAT:ip">,</nts>
                  <nts id="Seg_10910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_10912" n="HIAT:w" s="T227">штаны</ts>
                  <nts id="Seg_10913" n="HIAT:ip">.</nts>
                  <nts id="Seg_10914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_10915" n="sc" s="T231">
               <ts e="T232" id="Seg_10917" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_10919" n="HIAT:w" s="T231">Piʔmeʔi</ts>
                  <nts id="Seg_10920" n="HIAT:ip">?</nts>
                  <nts id="Seg_10921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_10922" n="sc" s="T235">
               <ts e="T248" id="Seg_10924" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_10926" n="HIAT:w" s="T235">Ага</ts>
                  <nts id="Seg_10927" n="HIAT:ip">,</nts>
                  <nts id="Seg_10928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_10930" n="HIAT:w" s="T236">а</ts>
                  <nts id="Seg_10931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_10933" n="HIAT:w" s="T238">я</ts>
                  <nts id="Seg_10934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_10936" n="HIAT:w" s="T240">слышал</ts>
                  <nts id="Seg_10937" n="HIAT:ip">,</nts>
                  <nts id="Seg_10938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_10940" n="HIAT:w" s="T241">что</ts>
                  <nts id="Seg_10941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_10943" n="HIAT:w" s="T243">ты</ts>
                  <nts id="Seg_10944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_10946" n="HIAT:w" s="T245">сказала</ts>
                  <nts id="Seg_10947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_10949" n="HIAT:w" s="T246">jaʔmaʔi</ts>
                  <nts id="Seg_10950" n="HIAT:ip">.</nts>
                  <nts id="Seg_10951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T259" id="Seg_10952" n="sc" s="T254">
               <ts e="T256" id="Seg_10954" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_10956" n="HIAT:w" s="T254">Ja</ts>
                  <nts id="Seg_10957" n="HIAT:ip">,</nts>
                  <nts id="Seg_10958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_10960" n="HIAT:w" s="T255">ja</ts>
                  <nts id="Seg_10961" n="HIAT:ip">.</nts>
                  <nts id="Seg_10962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_10964" n="HIAT:u" s="T256">
                  <ts e="T258" id="Seg_10966" n="HIAT:w" s="T256">Ja</ts>
                  <nts id="Seg_10967" n="HIAT:ip">,</nts>
                  <nts id="Seg_10968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_10970" n="HIAT:w" s="T258">ja</ts>
                  <nts id="Seg_10971" n="HIAT:ip">…</nts>
                  <nts id="Seg_10972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T264" id="Seg_10973" n="sc" s="T262">
               <ts e="T264" id="Seg_10975" n="HIAT:u" s="T262">
                  <ts e="T264" id="Seg_10977" n="HIAT:w" s="T262">Ясно</ts>
                  <nts id="Seg_10978" n="HIAT:ip">.</nts>
                  <nts id="Seg_10979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T272" id="Seg_10980" n="sc" s="T266">
               <ts e="T272" id="Seg_10982" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_10984" n="HIAT:w" s="T266">Ага</ts>
                  <nts id="Seg_10985" n="HIAT:ip">,</nts>
                  <nts id="Seg_10986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_10988" n="HIAT:w" s="T267">обутки</ts>
                  <nts id="Seg_10989" n="HIAT:ip">,</nts>
                  <nts id="Seg_10990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_10992" n="HIAT:w" s="T269">обутки</ts>
                  <nts id="Seg_10993" n="HIAT:ip">.</nts>
                  <nts id="Seg_10994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T285" id="Seg_10995" n="sc" s="T276">
               <ts e="T285" id="Seg_10997" n="HIAT:u" s="T276">
                  <ts e="T276.tx-KA.1" id="Seg_10999" n="HIAT:w" s="T276">Мм</ts>
                  <nts id="Seg_11000" n="HIAT:ip">,</nts>
                  <nts id="Seg_11001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276.tx-KA.2" id="Seg_11003" n="HIAT:w" s="T276.tx-KA.1">мм</ts>
                  <nts id="Seg_11004" n="HIAT:ip">,</nts>
                  <nts id="Seg_11005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276.tx-KA.3" id="Seg_11007" n="HIAT:w" s="T276.tx-KA.2">это</ts>
                  <nts id="Seg_11008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276.tx-KA.4" id="Seg_11010" n="HIAT:w" s="T276.tx-KA.3">рубашка</ts>
                  <nts id="Seg_11011" n="HIAT:ip">,</nts>
                  <nts id="Seg_11012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276.tx-KA.5" id="Seg_11014" n="HIAT:w" s="T276.tx-KA.4">это</ts>
                  <nts id="Seg_11015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276.tx-KA.6" id="Seg_11017" n="HIAT:w" s="T276.tx-KA.5">еще</ts>
                  <nts id="Seg_11018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_11020" n="HIAT:w" s="T276.tx-KA.6">помню</ts>
                  <nts id="Seg_11021" n="HIAT:ip">.</nts>
                  <nts id="Seg_11022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T298" id="Seg_11023" n="sc" s="T293">
               <ts e="T298" id="Seg_11025" n="HIAT:u" s="T293">
                  <ts e="T295" id="Seg_11027" n="HIAT:w" s="T293">Ну</ts>
                  <nts id="Seg_11028" n="HIAT:ip">,</nts>
                  <nts id="Seg_11029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_11031" n="HIAT:w" s="T295">так</ts>
                  <nts id="Seg_11032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_11034" n="HIAT:w" s="T297">они</ts>
                  <nts id="Seg_11035" n="HIAT:ip">…</nts>
                  <nts id="Seg_11036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T338" id="Seg_11037" n="sc" s="T300">
               <ts e="T338" id="Seg_11039" n="HIAT:u" s="T300">
                  <ts e="T302" id="Seg_11041" n="HIAT:w" s="T300">Ну</ts>
                  <nts id="Seg_11042" n="HIAT:ip">,</nts>
                  <nts id="Seg_11043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_11045" n="HIAT:w" s="T302">по-камасински</ts>
                  <nts id="Seg_11046" n="HIAT:ip">,</nts>
                  <nts id="Seg_11047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_11049" n="HIAT:w" s="T303">наверное</ts>
                  <nts id="Seg_11050" n="HIAT:ip">,</nts>
                  <nts id="Seg_11051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_11053" n="HIAT:w" s="T304">платье</ts>
                  <nts id="Seg_11054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_11056" n="HIAT:w" s="T305">не</ts>
                  <nts id="Seg_11057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_11059" n="HIAT:w" s="T306">говорили</ts>
                  <nts id="Seg_11060" n="HIAT:ip">,</nts>
                  <nts id="Seg_11061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_11063" n="HIAT:w" s="T308">потому</ts>
                  <nts id="Seg_11064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_11066" n="HIAT:w" s="T309">что</ts>
                  <nts id="Seg_11067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11068" n="HIAT:ip">(</nts>
                  <ts e="T314" id="Seg_11070" n="HIAT:w" s="T311">это</ts>
                  <nts id="Seg_11071" n="HIAT:ip">)</nts>
                  <nts id="Seg_11072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_11074" n="HIAT:w" s="T314">платье</ts>
                  <nts id="Seg_11075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_11077" n="HIAT:w" s="T316">не</ts>
                  <nts id="Seg_11078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_11080" n="HIAT:w" s="T318">носили</ts>
                  <nts id="Seg_11081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11082" n="HIAT:ip">(</nts>
                  <nts id="Seg_11083" n="HIAT:ip">(</nts>
                  <ats e="T322" id="Seg_11084" n="HIAT:non-pho" s="T320">…</ats>
                  <nts id="Seg_11085" n="HIAT:ip">)</nts>
                  <nts id="Seg_11086" n="HIAT:ip">)</nts>
                  <nts id="Seg_11087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_11089" n="HIAT:w" s="T322">вот</ts>
                  <nts id="Seg_11090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_11092" n="HIAT:w" s="T324">именно</ts>
                  <nts id="Seg_11093" n="HIAT:ip">,</nts>
                  <nts id="Seg_11094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_11096" n="HIAT:w" s="T326">да</ts>
                  <nts id="Seg_11097" n="HIAT:ip">,</nts>
                  <nts id="Seg_11098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_11100" n="HIAT:w" s="T328">рубашка</ts>
                  <nts id="Seg_11101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_11103" n="HIAT:w" s="T330">и</ts>
                  <nts id="Seg_11104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_11106" n="HIAT:w" s="T332">штаны</ts>
                  <nts id="Seg_11107" n="HIAT:ip">,</nts>
                  <nts id="Seg_11108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_11110" n="HIAT:w" s="T334">вот-вот</ts>
                  <nts id="Seg_11111" n="HIAT:ip">,</nts>
                  <nts id="Seg_11112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_11114" n="HIAT:w" s="T336">правильно</ts>
                  <nts id="Seg_11115" n="HIAT:ip">.</nts>
                  <nts id="Seg_11116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_11117" n="sc" s="T340">
               <ts e="T346" id="Seg_11119" n="HIAT:u" s="T340">
                  <ts e="T343" id="Seg_11121" n="HIAT:w" s="T340">Kujnek</ts>
                  <nts id="Seg_11122" n="HIAT:ip">,</nts>
                  <nts id="Seg_11123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_11125" n="HIAT:w" s="T343">piʔme</ts>
                  <nts id="Seg_11126" n="HIAT:ip">,</nts>
                  <nts id="Seg_11127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_11129" n="HIAT:w" s="T345">jaʔmaʔi</ts>
                  <nts id="Seg_11130" n="HIAT:ip">.</nts>
                  <nts id="Seg_11131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T355" id="Seg_11132" n="sc" s="T354">
               <ts e="T355" id="Seg_11134" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_11136" n="HIAT:w" s="T354">Üžü</ts>
                  <nts id="Seg_11137" n="HIAT:ip">.</nts>
                  <nts id="Seg_11138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T357" id="Seg_11139" n="sc" s="T356">
               <ts e="T357" id="Seg_11141" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_11143" n="HIAT:w" s="T356">Ага</ts>
                  <nts id="Seg_11144" n="HIAT:ip">.</nts>
                  <nts id="Seg_11145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T361" id="Seg_11146" n="sc" s="T358">
               <ts e="T361" id="Seg_11148" n="HIAT:u" s="T358">
                  <nts id="Seg_11149" n="HIAT:ip">(</nts>
                  <nts id="Seg_11150" n="HIAT:ip">(</nts>
                  <ats e="T361" id="Seg_11151" n="HIAT:non-pho" s="T358">…</ats>
                  <nts id="Seg_11152" n="HIAT:ip">)</nts>
                  <nts id="Seg_11153" n="HIAT:ip">)</nts>
                  <nts id="Seg_11154" n="HIAT:ip">.</nts>
                  <nts id="Seg_11155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T387" id="Seg_11156" n="sc" s="T365">
               <ts e="T387" id="Seg_11158" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_11160" n="HIAT:w" s="T365">Ну</ts>
                  <nts id="Seg_11161" n="HIAT:ip">,</nts>
                  <nts id="Seg_11162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11163" n="HIAT:ip">(</nts>
                  <ts e="T367" id="Seg_11165" n="HIAT:w" s="T366">а=</ts>
                  <nts id="Seg_11166" n="HIAT:ip">)</nts>
                  <nts id="Seg_11167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_11169" n="HIAT:w" s="T367">ты</ts>
                  <nts id="Seg_11170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_11172" n="HIAT:w" s="T368">мне</ts>
                  <nts id="Seg_11173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_11175" n="HIAT:w" s="T369">много</ts>
                  <nts id="Seg_11176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_11178" n="HIAT:w" s="T370">по-русски</ts>
                  <nts id="Seg_11179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_11181" n="HIAT:w" s="T371">рассказывала</ts>
                  <nts id="Seg_11182" n="HIAT:ip">,</nts>
                  <nts id="Seg_11183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_11185" n="HIAT:w" s="T372">вот</ts>
                  <nts id="Seg_11186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_11188" n="HIAT:w" s="T373">когда</ts>
                  <nts id="Seg_11189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_11191" n="HIAT:w" s="T374">я</ts>
                  <nts id="Seg_11192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_11194" n="HIAT:w" s="T375">впервые</ts>
                  <nts id="Seg_11195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_11197" n="HIAT:w" s="T376">был</ts>
                  <nts id="Seg_11198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_11200" n="HIAT:w" s="T377">у</ts>
                  <nts id="Seg_11201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_11203" n="HIAT:w" s="T378">тебя</ts>
                  <nts id="Seg_11204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_11206" n="HIAT:w" s="T379">там</ts>
                  <nts id="Seg_11207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_11209" n="HIAT:w" s="T380">когда</ts>
                  <nts id="Seg_11210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_11212" n="HIAT:w" s="T381">я</ts>
                  <nts id="Seg_11213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_11215" n="HIAT:w" s="T382">приехал</ts>
                  <nts id="Seg_11216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_11218" n="HIAT:w" s="T383">из</ts>
                  <nts id="Seg_11219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_11221" n="HIAT:w" s="T384">Финляндии</ts>
                  <nts id="Seg_11222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_11224" n="HIAT:w" s="T385">в</ts>
                  <nts id="Seg_11225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_11227" n="HIAT:w" s="T386">Тарту</ts>
                  <nts id="Seg_11228" n="HIAT:ip">.</nts>
                  <nts id="Seg_11229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T390" id="Seg_11230" n="sc" s="T388">
               <ts e="T390" id="Seg_11232" n="HIAT:u" s="T388">
                  <nts id="Seg_11233" n="HIAT:ip">(</nts>
                  <nts id="Seg_11234" n="HIAT:ip">(</nts>
                  <ats e="T390" id="Seg_11235" n="HIAT:non-pho" s="T388">Вот…</ats>
                  <nts id="Seg_11236" n="HIAT:ip">)</nts>
                  <nts id="Seg_11237" n="HIAT:ip">)</nts>
                  <nts id="Seg_11238" n="HIAT:ip">.</nts>
                  <nts id="Seg_11239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T394" id="Seg_11240" n="sc" s="T393">
               <ts e="T394" id="Seg_11242" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_11244" n="HIAT:w" s="T393">А</ts>
                  <nts id="Seg_11245" n="HIAT:ip">.</nts>
                  <nts id="Seg_11246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T411" id="Seg_11247" n="sc" s="T409">
               <ts e="T411" id="Seg_11249" n="HIAT:u" s="T409">
                  <ts e="T411" id="Seg_11251" n="HIAT:w" s="T409">Ага</ts>
                  <nts id="Seg_11252" n="HIAT:ip">.</nts>
                  <nts id="Seg_11253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T426" id="Seg_11254" n="sc" s="T425">
               <ts e="T426" id="Seg_11256" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_11258" n="HIAT:w" s="T425">Ага</ts>
                  <nts id="Seg_11259" n="HIAT:ip">.</nts>
                  <nts id="Seg_11260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T434" id="Seg_11261" n="sc" s="T427">
               <ts e="T434" id="Seg_11263" n="HIAT:u" s="T427">
                  <ts e="T429" id="Seg_11265" n="HIAT:w" s="T427">Ну</ts>
                  <nts id="Seg_11266" n="HIAT:ip">,</nts>
                  <nts id="Seg_11267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_11269" n="HIAT:w" s="T429">давай</ts>
                  <nts id="Seg_11270" n="HIAT:ip">,</nts>
                  <nts id="Seg_11271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_11273" n="HIAT:w" s="T431">давай</ts>
                  <nts id="Seg_11274" n="HIAT:ip">…</nts>
                  <nts id="Seg_11275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T554" id="Seg_11276" n="sc" s="T546">
               <ts e="T554" id="Seg_11278" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_11280" n="HIAT:w" s="T546">Ну</ts>
                  <nts id="Seg_11281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_11283" n="HIAT:w" s="T547">и</ts>
                  <nts id="Seg_11284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_11286" n="HIAT:w" s="T549">повтори</ts>
                  <nts id="Seg_11287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_11289" n="HIAT:w" s="T550">по-русски</ts>
                  <nts id="Seg_11290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_11292" n="HIAT:w" s="T551">еще</ts>
                  <nts id="Seg_11293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_11295" n="HIAT:w" s="T552">раз</ts>
                  <nts id="Seg_11296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_11298" n="HIAT:w" s="T553">всё</ts>
                  <nts id="Seg_11299" n="HIAT:ip">.</nts>
                  <nts id="Seg_11300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_11301" n="sc" s="T564">
               <ts e="T570" id="Seg_11303" n="HIAT:u" s="T564">
                  <ts e="T567" id="Seg_11305" n="HIAT:w" s="T564">Ну</ts>
                  <nts id="Seg_11306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_11308" n="HIAT:w" s="T567">ничего</ts>
                  <nts id="Seg_11309" n="HIAT:ip">,</nts>
                  <nts id="Seg_11310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_11312" n="HIAT:w" s="T569">ничего</ts>
                  <nts id="Seg_11313" n="HIAT:ip">…</nts>
                  <nts id="Seg_11314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T721" id="Seg_11315" n="sc" s="T715">
               <ts e="T717" id="Seg_11317" n="HIAT:u" s="T715">
                  <ts e="T717" id="Seg_11319" n="HIAT:w" s="T715">Работает</ts>
                  <nts id="Seg_11320" n="HIAT:ip">.</nts>
                  <nts id="Seg_11321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_11323" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_11325" n="HIAT:w" s="T717">Мы</ts>
                  <nts id="Seg_11326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_11328" n="HIAT:w" s="T718">по-русски</ts>
                  <nts id="Seg_11329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_11331" n="HIAT:w" s="T719">записали</ts>
                  <nts id="Seg_11332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_11334" n="HIAT:w" s="T720">сейчас</ts>
                  <nts id="Seg_11335" n="HIAT:ip">.</nts>
                  <nts id="Seg_11336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_11337" n="sc" s="T725">
               <ts e="T734" id="Seg_11339" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_11341" n="HIAT:w" s="T725">Ну</ts>
                  <nts id="Seg_11342" n="HIAT:ip">,</nts>
                  <nts id="Seg_11343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_11345" n="HIAT:w" s="T726">как</ts>
                  <nts id="Seg_11346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_11348" n="HIAT:w" s="T727">Александр</ts>
                  <nts id="Seg_11349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_11351" n="HIAT:w" s="T729">Константинович</ts>
                  <nts id="Seg_11352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_11354" n="HIAT:w" s="T731">обещал</ts>
                  <nts id="Seg_11355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_11357" n="HIAT:w" s="T732">приехать</ts>
                  <nts id="Seg_11358" n="HIAT:ip">.</nts>
                  <nts id="Seg_11359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T811" id="Seg_11360" n="sc" s="T804">
               <ts e="T811" id="Seg_11362" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_11364" n="HIAT:w" s="T804">А</ts>
                  <nts id="Seg_11365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_11367" n="HIAT:w" s="T805">сейчас</ts>
                  <nts id="Seg_11368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_11370" n="HIAT:w" s="T806">по-камасински</ts>
                  <nts id="Seg_11371" n="HIAT:ip">,</nts>
                  <nts id="Seg_11372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_11374" n="HIAT:w" s="T807">значит</ts>
                  <nts id="Seg_11375" n="HIAT:ip">,</nts>
                  <nts id="Seg_11376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_11378" n="HIAT:w" s="T808">как</ts>
                  <nts id="Seg_11379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_11381" n="HIAT:w" s="T809">он</ts>
                  <nts id="Seg_11382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_11384" n="HIAT:w" s="T810">писал</ts>
                  <nts id="Seg_11385" n="HIAT:ip">.</nts>
                  <nts id="Seg_11386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T818" id="Seg_11387" n="sc" s="T812">
               <ts e="T818" id="Seg_11389" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_11391" n="HIAT:w" s="T812">Это</ts>
                  <nts id="Seg_11392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_11394" n="HIAT:w" s="T813">мы</ts>
                  <nts id="Seg_11395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_11397" n="HIAT:w" s="T814">еще</ts>
                  <nts id="Seg_11398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_11400" n="HIAT:w" s="T815">по-камасински</ts>
                  <nts id="Seg_11401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_11403" n="HIAT:w" s="T816">не</ts>
                  <nts id="Seg_11404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_11406" n="HIAT:w" s="T817">говорили</ts>
                  <nts id="Seg_11407" n="HIAT:ip">.</nts>
                  <nts id="Seg_11408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T827" id="Seg_11409" n="sc" s="T822">
               <ts e="T827" id="Seg_11411" n="HIAT:u" s="T822">
                  <ts e="T824" id="Seg_11413" n="HIAT:w" s="T822">Нет</ts>
                  <nts id="Seg_11414" n="HIAT:ip">,</nts>
                  <nts id="Seg_11415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_11417" n="HIAT:w" s="T824">по-русски</ts>
                  <nts id="Seg_11418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_11420" n="HIAT:w" s="T825">только</ts>
                  <nts id="Seg_11421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_11423" n="HIAT:w" s="T826">записывали</ts>
                  <nts id="Seg_11424" n="HIAT:ip">.</nts>
                  <nts id="Seg_11425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T840" id="Seg_11426" n="sc" s="T833">
               <ts e="T840" id="Seg_11428" n="HIAT:u" s="T833">
                  <ts e="T835" id="Seg_11430" n="HIAT:w" s="T833">Нет</ts>
                  <nts id="Seg_11431" n="HIAT:ip">,</nts>
                  <nts id="Seg_11432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_11434" n="HIAT:w" s="T835">это</ts>
                  <nts id="Seg_11435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_11437" n="HIAT:w" s="T836">мы</ts>
                  <nts id="Seg_11438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_11440" n="HIAT:w" s="T837">не</ts>
                  <nts id="Seg_11441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_11443" n="HIAT:w" s="T838">рассказали</ts>
                  <nts id="Seg_11444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_11446" n="HIAT:w" s="T839">по-камасински</ts>
                  <nts id="Seg_11447" n="HIAT:ip">.</nts>
                  <nts id="Seg_11448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T846" id="Seg_11449" n="sc" s="T844">
               <ts e="T846" id="Seg_11451" n="HIAT:u" s="T844">
                  <ts e="T846" id="Seg_11453" n="HIAT:w" s="T844">Нет</ts>
                  <nts id="Seg_11454" n="HIAT:ip">.</nts>
                  <nts id="Seg_11455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T858" id="Seg_11456" n="sc" s="T856">
               <ts e="T858" id="Seg_11458" n="HIAT:u" s="T856">
                  <ts e="T858" id="Seg_11460" n="HIAT:w" s="T856">Ага</ts>
                  <nts id="Seg_11461" n="HIAT:ip">.</nts>
                  <nts id="Seg_11462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1164" id="Seg_11463" n="sc" s="T1152">
               <ts e="T1164" id="Seg_11465" n="HIAT:u" s="T1152">
                  <ts e="T1153" id="Seg_11467" n="HIAT:w" s="T1152">Да</ts>
                  <nts id="Seg_11468" n="HIAT:ip">,</nts>
                  <nts id="Seg_11469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1154" id="Seg_11471" n="HIAT:w" s="T1153">и</ts>
                  <nts id="Seg_11472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_11474" n="HIAT:w" s="T1154">по-русски</ts>
                  <nts id="Seg_11475" n="HIAT:ip">,</nts>
                  <nts id="Seg_11476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_11478" n="HIAT:w" s="T1155">а</ts>
                  <nts id="Seg_11479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_11481" n="HIAT:w" s="T1156">то</ts>
                  <nts id="Seg_11482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11483" n="HIAT:ip">(</nts>
                  <ts e="T1158" id="Seg_11485" n="HIAT:w" s="T1157">это</ts>
                  <nts id="Seg_11486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_11488" n="HIAT:w" s="T1158">ты</ts>
                  <nts id="Seg_11489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1160" id="Seg_11491" n="HIAT:w" s="T1159">мне</ts>
                  <nts id="Seg_11492" n="HIAT:ip">)</nts>
                  <nts id="Seg_11493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_11495" n="HIAT:w" s="T1160">еще</ts>
                  <nts id="Seg_11496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1162" id="Seg_11498" n="HIAT:w" s="T1161">не</ts>
                  <nts id="Seg_11499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1163" id="Seg_11501" n="HIAT:w" s="T1162">рассказывала</ts>
                  <nts id="Seg_11502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_11504" n="HIAT:w" s="T1163">совсем</ts>
                  <nts id="Seg_11505" n="HIAT:ip">.</nts>
                  <nts id="Seg_11506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1514" id="Seg_11507" n="sc" s="T1509">
               <ts e="T1514" id="Seg_11509" n="HIAT:u" s="T1509">
                  <ts e="T1510" id="Seg_11511" n="HIAT:w" s="T1509">Ну</ts>
                  <nts id="Seg_11512" n="HIAT:ip">,</nts>
                  <nts id="Seg_11513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1511" id="Seg_11515" n="HIAT:w" s="T1510">и</ts>
                  <nts id="Seg_11516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1512" id="Seg_11518" n="HIAT:w" s="T1511">по-русски</ts>
                  <nts id="Seg_11519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1513" id="Seg_11521" n="HIAT:w" s="T1512">можно</ts>
                  <nts id="Seg_11522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1514" id="Seg_11524" n="HIAT:w" s="T1513">заодно</ts>
                  <nts id="Seg_11525" n="HIAT:ip">.</nts>
                  <nts id="Seg_11526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1518" id="Seg_11527" n="sc" s="T1515">
               <ts e="T1518" id="Seg_11529" n="HIAT:u" s="T1515">
                  <nts id="Seg_11530" n="HIAT:ip">(</nts>
                  <nts id="Seg_11531" n="HIAT:ip">(</nts>
                  <ats e="T1518" id="Seg_11532" n="HIAT:non-pho" s="T1515">…</ats>
                  <nts id="Seg_11533" n="HIAT:ip">)</nts>
                  <nts id="Seg_11534" n="HIAT:ip">)</nts>
                  <nts id="Seg_11535" n="HIAT:ip">.</nts>
                  <nts id="Seg_11536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T13" id="Seg_11537" n="sc" s="T10">
               <ts e="T11" id="Seg_11539" n="e" s="T10">((BRK)) </ts>
               <ts e="T12" id="Seg_11541" n="e" s="T11">Или </ts>
               <ts e="T13" id="Seg_11543" n="e" s="T12">так. </ts>
            </ts>
            <ts e="T16" id="Seg_11544" n="sc" s="T14">
               <ts e="T15" id="Seg_11546" n="e" s="T14">Или </ts>
               <ts e="T16" id="Seg_11548" n="e" s="T15">что? </ts>
            </ts>
            <ts e="T25" id="Seg_11549" n="sc" s="T18">
               <ts e="T20" id="Seg_11551" n="e" s="T18">Я </ts>
               <ts e="T21" id="Seg_11553" n="e" s="T20">уж </ts>
               <ts e="T22" id="Seg_11555" n="e" s="T21">тоже </ts>
               <ts e="T24" id="Seg_11557" n="e" s="T22">не </ts>
               <ts e="T25" id="Seg_11559" n="e" s="T24">помню. </ts>
            </ts>
            <ts e="T32" id="Seg_11560" n="sc" s="T31">
               <ts e="T32" id="Seg_11562" n="e" s="T31">Ага. </ts>
            </ts>
            <ts e="T41" id="Seg_11563" n="sc" s="T33">
               <ts e="T34" id="Seg_11565" n="e" s="T33">Ну </ts>
               <ts e="T35" id="Seg_11567" n="e" s="T34">неважно, </ts>
               <ts e="T36" id="Seg_11569" n="e" s="T35">пускай, </ts>
               <ts e="T37" id="Seg_11571" n="e" s="T36">поговорим </ts>
               <ts e="T38" id="Seg_11573" n="e" s="T37">пока </ts>
               <ts e="T39" id="Seg_11575" n="e" s="T38">((…)) </ts>
               <ts e="T40" id="Seg_11577" n="e" s="T39">что-то </ts>
               <ts e="T41" id="Seg_11579" n="e" s="T40">новое. </ts>
            </ts>
            <ts e="T43" id="Seg_11580" n="sc" s="T42">
               <ts e="T43" id="Seg_11582" n="e" s="T42">((…)). </ts>
            </ts>
            <ts e="T50" id="Seg_11583" n="sc" s="T48">
               <ts e="T50" id="Seg_11585" n="e" s="T48">Ага. </ts>
            </ts>
            <ts e="T58" id="Seg_11586" n="sc" s="T56">
               <ts e="T58" id="Seg_11588" n="e" s="T56">Ага. </ts>
            </ts>
            <ts e="T96" id="Seg_11589" n="sc" s="T95">
               <ts e="T96" id="Seg_11591" n="e" s="T95">Хорошо… </ts>
            </ts>
            <ts e="T113" id="Seg_11592" n="sc" s="T97">
               <ts e="T98" id="Seg_11594" n="e" s="T97">Ну </ts>
               <ts e="T99" id="Seg_11596" n="e" s="T98">что </ts>
               <ts e="T100" id="Seg_11598" n="e" s="T99">тебе </ts>
               <ts e="T101" id="Seg_11600" n="e" s="T100">помирать, </ts>
               <ts e="T102" id="Seg_11602" n="e" s="T101">еще </ts>
               <ts e="T103" id="Seg_11604" n="e" s="T102">молодая. </ts>
               <ts e="T113" id="Seg_11606" n="e" s="T103">((LAUGH)) ((COUGH)). </ts>
            </ts>
            <ts e="T120" id="Seg_11607" n="sc" s="T118">
               <ts e="T119" id="Seg_11609" n="e" s="T118">Ну, </ts>
               <ts e="T120" id="Seg_11611" n="e" s="T119">ну. </ts>
            </ts>
            <ts e="T122" id="Seg_11612" n="sc" s="T121">
               <ts e="T122" id="Seg_11614" n="e" s="T121">Обязательно. </ts>
            </ts>
            <ts e="T125" id="Seg_11615" n="sc" s="T123">
               <ts e="T124" id="Seg_11617" n="e" s="T123">Можно </ts>
               <ts e="T125" id="Seg_11619" n="e" s="T124">пускать? </ts>
            </ts>
            <ts e="T172" id="Seg_11620" n="sc" s="T167">
               <ts e="T168" id="Seg_11622" n="e" s="T167">Ну, </ts>
               <ts e="T169" id="Seg_11624" n="e" s="T168">и </ts>
               <ts e="T170" id="Seg_11626" n="e" s="T169">по-русски </ts>
               <ts e="T171" id="Seg_11628" n="e" s="T170">можешь </ts>
               <ts e="T172" id="Seg_11630" n="e" s="T171">сразу. </ts>
            </ts>
            <ts e="T213" id="Seg_11631" n="sc" s="T207">
               <ts e="T209" id="Seg_11633" n="e" s="T207">Ты </ts>
               <ts e="T211" id="Seg_11635" n="e" s="T209">еще </ts>
               <ts e="T212" id="Seg_11637" n="e" s="T211">сказала </ts>
               <ts e="T213" id="Seg_11639" n="e" s="T212">jamaʔi. </ts>
            </ts>
            <ts e="T218" id="Seg_11640" n="sc" s="T214">
               <ts e="T215" id="Seg_11642" n="e" s="T214">Ты, </ts>
               <ts e="T216" id="Seg_11644" n="e" s="T215">наверное, </ts>
               <ts e="T217" id="Seg_11646" n="e" s="T216">сапоги </ts>
               <ts e="T218" id="Seg_11648" n="e" s="T217">купила. </ts>
            </ts>
            <ts e="T229" id="Seg_11649" n="sc" s="T222">
               <ts e="T223" id="Seg_11651" n="e" s="T222">А, </ts>
               <ts e="T225" id="Seg_11653" n="e" s="T223">штаны, </ts>
               <ts e="T227" id="Seg_11655" n="e" s="T225">штаны, </ts>
               <ts e="T229" id="Seg_11657" n="e" s="T227">штаны. </ts>
            </ts>
            <ts e="T232" id="Seg_11658" n="sc" s="T231">
               <ts e="T232" id="Seg_11660" n="e" s="T231">Piʔmeʔi? </ts>
            </ts>
            <ts e="T248" id="Seg_11661" n="sc" s="T235">
               <ts e="T236" id="Seg_11663" n="e" s="T235">Ага, </ts>
               <ts e="T238" id="Seg_11665" n="e" s="T236">а </ts>
               <ts e="T240" id="Seg_11667" n="e" s="T238">я </ts>
               <ts e="T241" id="Seg_11669" n="e" s="T240">слышал, </ts>
               <ts e="T243" id="Seg_11671" n="e" s="T241">что </ts>
               <ts e="T245" id="Seg_11673" n="e" s="T243">ты </ts>
               <ts e="T246" id="Seg_11675" n="e" s="T245">сказала </ts>
               <ts e="T248" id="Seg_11677" n="e" s="T246">jaʔmaʔi. </ts>
            </ts>
            <ts e="T259" id="Seg_11678" n="sc" s="T254">
               <ts e="T255" id="Seg_11680" n="e" s="T254">Ja, </ts>
               <ts e="T256" id="Seg_11682" n="e" s="T255">ja. </ts>
               <ts e="T258" id="Seg_11684" n="e" s="T256">Ja, </ts>
               <ts e="T259" id="Seg_11686" n="e" s="T258">ja… </ts>
            </ts>
            <ts e="T264" id="Seg_11687" n="sc" s="T262">
               <ts e="T264" id="Seg_11689" n="e" s="T262">Ясно. </ts>
            </ts>
            <ts e="T272" id="Seg_11690" n="sc" s="T266">
               <ts e="T267" id="Seg_11692" n="e" s="T266">Ага, </ts>
               <ts e="T269" id="Seg_11694" n="e" s="T267">обутки, </ts>
               <ts e="T272" id="Seg_11696" n="e" s="T269">обутки. </ts>
            </ts>
            <ts e="T285" id="Seg_11697" n="sc" s="T276">
               <ts e="T285" id="Seg_11699" n="e" s="T276">Мм, мм, это рубашка, это еще помню. </ts>
            </ts>
            <ts e="T298" id="Seg_11700" n="sc" s="T293">
               <ts e="T295" id="Seg_11702" n="e" s="T293">Ну, </ts>
               <ts e="T297" id="Seg_11704" n="e" s="T295">так </ts>
               <ts e="T298" id="Seg_11706" n="e" s="T297">они… </ts>
            </ts>
            <ts e="T338" id="Seg_11707" n="sc" s="T300">
               <ts e="T302" id="Seg_11709" n="e" s="T300">Ну, </ts>
               <ts e="T303" id="Seg_11711" n="e" s="T302">по-камасински, </ts>
               <ts e="T304" id="Seg_11713" n="e" s="T303">наверное, </ts>
               <ts e="T305" id="Seg_11715" n="e" s="T304">платье </ts>
               <ts e="T306" id="Seg_11717" n="e" s="T305">не </ts>
               <ts e="T308" id="Seg_11719" n="e" s="T306">говорили, </ts>
               <ts e="T309" id="Seg_11721" n="e" s="T308">потому </ts>
               <ts e="T311" id="Seg_11723" n="e" s="T309">что </ts>
               <ts e="T314" id="Seg_11725" n="e" s="T311">(это) </ts>
               <ts e="T316" id="Seg_11727" n="e" s="T314">платье </ts>
               <ts e="T318" id="Seg_11729" n="e" s="T316">не </ts>
               <ts e="T320" id="Seg_11731" n="e" s="T318">носили </ts>
               <ts e="T322" id="Seg_11733" n="e" s="T320">((…)) </ts>
               <ts e="T324" id="Seg_11735" n="e" s="T322">вот </ts>
               <ts e="T326" id="Seg_11737" n="e" s="T324">именно, </ts>
               <ts e="T328" id="Seg_11739" n="e" s="T326">да, </ts>
               <ts e="T330" id="Seg_11741" n="e" s="T328">рубашка </ts>
               <ts e="T332" id="Seg_11743" n="e" s="T330">и </ts>
               <ts e="T334" id="Seg_11745" n="e" s="T332">штаны, </ts>
               <ts e="T336" id="Seg_11747" n="e" s="T334">вот-вот, </ts>
               <ts e="T338" id="Seg_11749" n="e" s="T336">правильно. </ts>
            </ts>
            <ts e="T346" id="Seg_11750" n="sc" s="T340">
               <ts e="T343" id="Seg_11752" n="e" s="T340">Kujnek, </ts>
               <ts e="T345" id="Seg_11754" n="e" s="T343">piʔme, </ts>
               <ts e="T346" id="Seg_11756" n="e" s="T345">jaʔmaʔi. </ts>
            </ts>
            <ts e="T355" id="Seg_11757" n="sc" s="T354">
               <ts e="T355" id="Seg_11759" n="e" s="T354">Üžü. </ts>
            </ts>
            <ts e="T357" id="Seg_11760" n="sc" s="T356">
               <ts e="T357" id="Seg_11762" n="e" s="T356">Ага. </ts>
            </ts>
            <ts e="T361" id="Seg_11763" n="sc" s="T358">
               <ts e="T361" id="Seg_11765" n="e" s="T358">((…)). </ts>
            </ts>
            <ts e="T387" id="Seg_11766" n="sc" s="T365">
               <ts e="T366" id="Seg_11768" n="e" s="T365">Ну, </ts>
               <ts e="T367" id="Seg_11770" n="e" s="T366">(а=) </ts>
               <ts e="T368" id="Seg_11772" n="e" s="T367">ты </ts>
               <ts e="T369" id="Seg_11774" n="e" s="T368">мне </ts>
               <ts e="T370" id="Seg_11776" n="e" s="T369">много </ts>
               <ts e="T371" id="Seg_11778" n="e" s="T370">по-русски </ts>
               <ts e="T372" id="Seg_11780" n="e" s="T371">рассказывала, </ts>
               <ts e="T373" id="Seg_11782" n="e" s="T372">вот </ts>
               <ts e="T374" id="Seg_11784" n="e" s="T373">когда </ts>
               <ts e="T375" id="Seg_11786" n="e" s="T374">я </ts>
               <ts e="T376" id="Seg_11788" n="e" s="T375">впервые </ts>
               <ts e="T377" id="Seg_11790" n="e" s="T376">был </ts>
               <ts e="T378" id="Seg_11792" n="e" s="T377">у </ts>
               <ts e="T379" id="Seg_11794" n="e" s="T378">тебя </ts>
               <ts e="T380" id="Seg_11796" n="e" s="T379">там </ts>
               <ts e="T381" id="Seg_11798" n="e" s="T380">когда </ts>
               <ts e="T382" id="Seg_11800" n="e" s="T381">я </ts>
               <ts e="T383" id="Seg_11802" n="e" s="T382">приехал </ts>
               <ts e="T384" id="Seg_11804" n="e" s="T383">из </ts>
               <ts e="T385" id="Seg_11806" n="e" s="T384">Финляндии </ts>
               <ts e="T386" id="Seg_11808" n="e" s="T385">в </ts>
               <ts e="T387" id="Seg_11810" n="e" s="T386">Тарту. </ts>
            </ts>
            <ts e="T390" id="Seg_11811" n="sc" s="T388">
               <ts e="T390" id="Seg_11813" n="e" s="T388">((Вот…)). </ts>
            </ts>
            <ts e="T394" id="Seg_11814" n="sc" s="T393">
               <ts e="T394" id="Seg_11816" n="e" s="T393">А. </ts>
            </ts>
            <ts e="T411" id="Seg_11817" n="sc" s="T409">
               <ts e="T411" id="Seg_11819" n="e" s="T409">Ага. </ts>
            </ts>
            <ts e="T426" id="Seg_11820" n="sc" s="T425">
               <ts e="T426" id="Seg_11822" n="e" s="T425">Ага. </ts>
            </ts>
            <ts e="T434" id="Seg_11823" n="sc" s="T427">
               <ts e="T429" id="Seg_11825" n="e" s="T427">Ну, </ts>
               <ts e="T431" id="Seg_11827" n="e" s="T429">давай, </ts>
               <ts e="T434" id="Seg_11829" n="e" s="T431">давай… </ts>
            </ts>
            <ts e="T554" id="Seg_11830" n="sc" s="T546">
               <ts e="T547" id="Seg_11832" n="e" s="T546">Ну </ts>
               <ts e="T549" id="Seg_11834" n="e" s="T547">и </ts>
               <ts e="T550" id="Seg_11836" n="e" s="T549">повтори </ts>
               <ts e="T551" id="Seg_11838" n="e" s="T550">по-русски </ts>
               <ts e="T552" id="Seg_11840" n="e" s="T551">еще </ts>
               <ts e="T553" id="Seg_11842" n="e" s="T552">раз </ts>
               <ts e="T554" id="Seg_11844" n="e" s="T553">всё. </ts>
            </ts>
            <ts e="T570" id="Seg_11845" n="sc" s="T564">
               <ts e="T567" id="Seg_11847" n="e" s="T564">Ну </ts>
               <ts e="T569" id="Seg_11849" n="e" s="T567">ничего, </ts>
               <ts e="T570" id="Seg_11851" n="e" s="T569">ничего… </ts>
            </ts>
            <ts e="T721" id="Seg_11852" n="sc" s="T715">
               <ts e="T717" id="Seg_11854" n="e" s="T715">Работает. </ts>
               <ts e="T718" id="Seg_11856" n="e" s="T717">Мы </ts>
               <ts e="T719" id="Seg_11858" n="e" s="T718">по-русски </ts>
               <ts e="T720" id="Seg_11860" n="e" s="T719">записали </ts>
               <ts e="T721" id="Seg_11862" n="e" s="T720">сейчас. </ts>
            </ts>
            <ts e="T734" id="Seg_11863" n="sc" s="T725">
               <ts e="T726" id="Seg_11865" n="e" s="T725">Ну, </ts>
               <ts e="T727" id="Seg_11867" n="e" s="T726">как </ts>
               <ts e="T729" id="Seg_11869" n="e" s="T727">Александр </ts>
               <ts e="T731" id="Seg_11871" n="e" s="T729">Константинович </ts>
               <ts e="T732" id="Seg_11873" n="e" s="T731">обещал </ts>
               <ts e="T734" id="Seg_11875" n="e" s="T732">приехать. </ts>
            </ts>
            <ts e="T811" id="Seg_11876" n="sc" s="T804">
               <ts e="T805" id="Seg_11878" n="e" s="T804">А </ts>
               <ts e="T806" id="Seg_11880" n="e" s="T805">сейчас </ts>
               <ts e="T807" id="Seg_11882" n="e" s="T806">по-камасински, </ts>
               <ts e="T808" id="Seg_11884" n="e" s="T807">значит, </ts>
               <ts e="T809" id="Seg_11886" n="e" s="T808">как </ts>
               <ts e="T810" id="Seg_11888" n="e" s="T809">он </ts>
               <ts e="T811" id="Seg_11890" n="e" s="T810">писал. </ts>
            </ts>
            <ts e="T818" id="Seg_11891" n="sc" s="T812">
               <ts e="T813" id="Seg_11893" n="e" s="T812">Это </ts>
               <ts e="T814" id="Seg_11895" n="e" s="T813">мы </ts>
               <ts e="T815" id="Seg_11897" n="e" s="T814">еще </ts>
               <ts e="T816" id="Seg_11899" n="e" s="T815">по-камасински </ts>
               <ts e="T817" id="Seg_11901" n="e" s="T816">не </ts>
               <ts e="T818" id="Seg_11903" n="e" s="T817">говорили. </ts>
            </ts>
            <ts e="T827" id="Seg_11904" n="sc" s="T822">
               <ts e="T824" id="Seg_11906" n="e" s="T822">Нет, </ts>
               <ts e="T825" id="Seg_11908" n="e" s="T824">по-русски </ts>
               <ts e="T826" id="Seg_11910" n="e" s="T825">только </ts>
               <ts e="T827" id="Seg_11912" n="e" s="T826">записывали. </ts>
            </ts>
            <ts e="T840" id="Seg_11913" n="sc" s="T833">
               <ts e="T835" id="Seg_11915" n="e" s="T833">Нет, </ts>
               <ts e="T836" id="Seg_11917" n="e" s="T835">это </ts>
               <ts e="T837" id="Seg_11919" n="e" s="T836">мы </ts>
               <ts e="T838" id="Seg_11921" n="e" s="T837">не </ts>
               <ts e="T839" id="Seg_11923" n="e" s="T838">рассказали </ts>
               <ts e="T840" id="Seg_11925" n="e" s="T839">по-камасински. </ts>
            </ts>
            <ts e="T846" id="Seg_11926" n="sc" s="T844">
               <ts e="T846" id="Seg_11928" n="e" s="T844">Нет. </ts>
            </ts>
            <ts e="T858" id="Seg_11929" n="sc" s="T856">
               <ts e="T858" id="Seg_11931" n="e" s="T856">Ага. </ts>
            </ts>
            <ts e="T1164" id="Seg_11932" n="sc" s="T1152">
               <ts e="T1153" id="Seg_11934" n="e" s="T1152">Да, </ts>
               <ts e="T1154" id="Seg_11936" n="e" s="T1153">и </ts>
               <ts e="T1155" id="Seg_11938" n="e" s="T1154">по-русски, </ts>
               <ts e="T1156" id="Seg_11940" n="e" s="T1155">а </ts>
               <ts e="T1157" id="Seg_11942" n="e" s="T1156">то </ts>
               <ts e="T1158" id="Seg_11944" n="e" s="T1157">(это </ts>
               <ts e="T1159" id="Seg_11946" n="e" s="T1158">ты </ts>
               <ts e="T1160" id="Seg_11948" n="e" s="T1159">мне) </ts>
               <ts e="T1161" id="Seg_11950" n="e" s="T1160">еще </ts>
               <ts e="T1162" id="Seg_11952" n="e" s="T1161">не </ts>
               <ts e="T1163" id="Seg_11954" n="e" s="T1162">рассказывала </ts>
               <ts e="T1164" id="Seg_11956" n="e" s="T1163">совсем. </ts>
            </ts>
            <ts e="T1514" id="Seg_11957" n="sc" s="T1509">
               <ts e="T1510" id="Seg_11959" n="e" s="T1509">Ну, </ts>
               <ts e="T1511" id="Seg_11961" n="e" s="T1510">и </ts>
               <ts e="T1512" id="Seg_11963" n="e" s="T1511">по-русски </ts>
               <ts e="T1513" id="Seg_11965" n="e" s="T1512">можно </ts>
               <ts e="T1514" id="Seg_11967" n="e" s="T1513">заодно. </ts>
            </ts>
            <ts e="T1518" id="Seg_11968" n="sc" s="T1515">
               <ts e="T1518" id="Seg_11970" n="e" s="T1515">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T13" id="Seg_11971" s="T10">PKZ_197008_09342-2bz.KA.001 (002)</ta>
            <ta e="T16" id="Seg_11972" s="T14">PKZ_197008_09342-2bz.KA.002 (003)</ta>
            <ta e="T25" id="Seg_11973" s="T18">PKZ_197008_09342-2bz.KA.003 (005)</ta>
            <ta e="T32" id="Seg_11974" s="T31">PKZ_197008_09342-2bz.KA.004 (006)</ta>
            <ta e="T41" id="Seg_11975" s="T33">PKZ_197008_09342-2bz.KA.005 (007)</ta>
            <ta e="T43" id="Seg_11976" s="T42">PKZ_197008_09342-2bz.KA.006 (008)</ta>
            <ta e="T50" id="Seg_11977" s="T48">PKZ_197008_09342-2bz.KA.007 (010)</ta>
            <ta e="T58" id="Seg_11978" s="T56">PKZ_197008_09342-2bz.KA.008 (012)</ta>
            <ta e="T96" id="Seg_11979" s="T95">PKZ_197008_09342-2bz.KA.009 (019)</ta>
            <ta e="T103" id="Seg_11980" s="T97">PKZ_197008_09342-2bz.KA.010 (020)</ta>
            <ta e="T113" id="Seg_11981" s="T103">PKZ_197008_09342-2bz.KA.011 (020)</ta>
            <ta e="T120" id="Seg_11982" s="T118">PKZ_197008_09342-2bz.KA.012 (023)</ta>
            <ta e="T122" id="Seg_11983" s="T121">PKZ_197008_09342-2bz.KA.013 (024)</ta>
            <ta e="T125" id="Seg_11984" s="T123">PKZ_197008_09342-2bz.KA.014 (025)</ta>
            <ta e="T172" id="Seg_11985" s="T167">PKZ_197008_09342-2bz.KA.015 (036)</ta>
            <ta e="T213" id="Seg_11986" s="T207">PKZ_197008_09342-2bz.KA.016 (041)</ta>
            <ta e="T218" id="Seg_11987" s="T214">PKZ_197008_09342-2bz.KA.017 (042)</ta>
            <ta e="T229" id="Seg_11988" s="T222">PKZ_197008_09342-2bz.KA.018 (045)</ta>
            <ta e="T232" id="Seg_11989" s="T231">PKZ_197008_09342-2bz.KA.019 (047)</ta>
            <ta e="T248" id="Seg_11990" s="T235">PKZ_197008_09342-2bz.KA.020 (049)</ta>
            <ta e="T256" id="Seg_11991" s="T254">PKZ_197008_09342-2bz.KA.021 (052)</ta>
            <ta e="T259" id="Seg_11992" s="T256">PKZ_197008_09342-2bz.KA.022 (053)</ta>
            <ta e="T264" id="Seg_11993" s="T262">PKZ_197008_09342-2bz.KA.023 (056)</ta>
            <ta e="T272" id="Seg_11994" s="T266">PKZ_197008_09342-2bz.KA.024 (057)</ta>
            <ta e="T285" id="Seg_11995" s="T276">PKZ_197008_09342-2bz.KA.025 (060)</ta>
            <ta e="T298" id="Seg_11996" s="T293">PKZ_197008_09342-2bz.KA.026 (063)</ta>
            <ta e="T338" id="Seg_11997" s="T300">PKZ_197008_09342-2bz.KA.027 (064)</ta>
            <ta e="T346" id="Seg_11998" s="T340">PKZ_197008_09342-2bz.KA.028 (067)</ta>
            <ta e="T355" id="Seg_11999" s="T354">PKZ_197008_09342-2bz.KA.029 (070)</ta>
            <ta e="T357" id="Seg_12000" s="T356">PKZ_197008_09342-2bz.KA.030 (071)</ta>
            <ta e="T361" id="Seg_12001" s="T358">PKZ_197008_09342-2bz.KA.031 (072)</ta>
            <ta e="T387" id="Seg_12002" s="T365">PKZ_197008_09342-2bz.KA.032 (074)</ta>
            <ta e="T390" id="Seg_12003" s="T388">PKZ_197008_09342-2bz.KA.033 (075)</ta>
            <ta e="T394" id="Seg_12004" s="T393">PKZ_197008_09342-2bz.KA.034 (077)</ta>
            <ta e="T411" id="Seg_12005" s="T409">PKZ_197008_09342-2bz.KA.035 (077)</ta>
            <ta e="T426" id="Seg_12006" s="T425">PKZ_197008_09342-2bz.KA.036 (079)</ta>
            <ta e="T434" id="Seg_12007" s="T427">PKZ_197008_09342-2bz.KA.037 (080)</ta>
            <ta e="T554" id="Seg_12008" s="T546">PKZ_197008_09342-2bz.KA.038 (098)</ta>
            <ta e="T570" id="Seg_12009" s="T564">PKZ_197008_09342-2bz.KA.039 (101)</ta>
            <ta e="T717" id="Seg_12010" s="T715">PKZ_197008_09342-2bz.KA.040 (123)</ta>
            <ta e="T721" id="Seg_12011" s="T717">PKZ_197008_09342-2bz.KA.041 (124)</ta>
            <ta e="T734" id="Seg_12012" s="T725">PKZ_197008_09342-2bz.KA.042 (126)</ta>
            <ta e="T811" id="Seg_12013" s="T804">PKZ_197008_09342-2bz.KA.043 (138)</ta>
            <ta e="T818" id="Seg_12014" s="T812">PKZ_197008_09342-2bz.KA.044 (139)</ta>
            <ta e="T827" id="Seg_12015" s="T822">PKZ_197008_09342-2bz.KA.045 (141)</ta>
            <ta e="T840" id="Seg_12016" s="T833">PKZ_197008_09342-2bz.KA.046 (143)</ta>
            <ta e="T846" id="Seg_12017" s="T844">PKZ_197008_09342-2bz.KA.047 (145)</ta>
            <ta e="T858" id="Seg_12018" s="T856">PKZ_197008_09342-2bz.KA.048 (148)</ta>
            <ta e="T1164" id="Seg_12019" s="T1152">PKZ_197008_09342-2bz.KA.049 (193)</ta>
            <ta e="T1514" id="Seg_12020" s="T1509">PKZ_197008_09342-2bz.KA.050 (246)</ta>
            <ta e="T1518" id="Seg_12021" s="T1515">PKZ_197008_09342-2bz.KA.051 (247)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T13" id="Seg_12022" s="T10">((BRK)) Или так. </ta>
            <ta e="T16" id="Seg_12023" s="T14">Или что? </ta>
            <ta e="T25" id="Seg_12024" s="T18">Я уж тоже не помню. </ta>
            <ta e="T32" id="Seg_12025" s="T31">Ага. </ta>
            <ta e="T41" id="Seg_12026" s="T33">Ну неважно, пускай, поговорим пока ((…)) что-то новое. </ta>
            <ta e="T43" id="Seg_12027" s="T42">((…)).</ta>
            <ta e="T50" id="Seg_12028" s="T48">Ага. </ta>
            <ta e="T58" id="Seg_12029" s="T56">Ага. </ta>
            <ta e="T96" id="Seg_12030" s="T95">Хорошо… </ta>
            <ta e="T103" id="Seg_12031" s="T97">Ну что тебе помирать, еще молодая. </ta>
            <ta e="T113" id="Seg_12032" s="T103">((LAUGH)) ((COUGH)). </ta>
            <ta e="T120" id="Seg_12033" s="T118">Ну, ну. </ta>
            <ta e="T122" id="Seg_12034" s="T121">Обязательно. </ta>
            <ta e="T125" id="Seg_12035" s="T123">Можно пускать? </ta>
            <ta e="T172" id="Seg_12036" s="T167">Ну, и по-русски можешь сразу. </ta>
            <ta e="T213" id="Seg_12037" s="T207">Ты еще сказала jamaʔi. </ta>
            <ta e="T218" id="Seg_12038" s="T214">Ты, наверное, сапоги купила. </ta>
            <ta e="T229" id="Seg_12039" s="T222">А, штаны, штаны, штаны. </ta>
            <ta e="T232" id="Seg_12040" s="T231">Piʔmeʔi? </ta>
            <ta e="T248" id="Seg_12041" s="T235">Ага, а я слышал, что ты сказала jaʔmaʔi. </ta>
            <ta e="T256" id="Seg_12042" s="T254">Ja, ja. </ta>
            <ta e="T259" id="Seg_12043" s="T256">Ja, ja… </ta>
            <ta e="T264" id="Seg_12044" s="T262">Ясно. </ta>
            <ta e="T272" id="Seg_12045" s="T266">Ага, обутки, обутки. </ta>
            <ta e="T285" id="Seg_12046" s="T276">Мм, мм, это рубашка, это еще помню. </ta>
            <ta e="T298" id="Seg_12047" s="T293">Ну, так они… </ta>
            <ta e="T338" id="Seg_12048" s="T300">Ну, по-камасински, наверное, платье не говорили, потому что (это) платье не носили ((…)) вот именно, да, рубашка и штаны, вот-вот, правильно. </ta>
            <ta e="T346" id="Seg_12049" s="T340">Kujnek, piʔme, jaʔmaʔi. </ta>
            <ta e="T355" id="Seg_12050" s="T354">Üžü. </ta>
            <ta e="T357" id="Seg_12051" s="T356">Ага. </ta>
            <ta e="T361" id="Seg_12052" s="T358">((…)). </ta>
            <ta e="T387" id="Seg_12053" s="T365">Ну, (а=) ты мне много по-русски рассказывала, вот когда я впервые был у тебя там когда я приехал из Финляндии в Тарту. </ta>
            <ta e="T390" id="Seg_12054" s="T388">((Вот…)). </ta>
            <ta e="T394" id="Seg_12055" s="T393">А. </ta>
            <ta e="T411" id="Seg_12056" s="T409">Ага. </ta>
            <ta e="T426" id="Seg_12057" s="T425">Ага. </ta>
            <ta e="T434" id="Seg_12058" s="T427">Ну, давай, давай… </ta>
            <ta e="T554" id="Seg_12059" s="T546">Ну и повтори по-русски еще раз всё. </ta>
            <ta e="T570" id="Seg_12060" s="T564">Ну ничего, ничего… </ta>
            <ta e="T717" id="Seg_12061" s="T715">Работает. </ta>
            <ta e="T721" id="Seg_12062" s="T717">Мы по-русски записали сейчас. </ta>
            <ta e="T734" id="Seg_12063" s="T725">Ну, как Александр Константинович обещал приехать. </ta>
            <ta e="T811" id="Seg_12064" s="T804">А сейчас по-камасински, значит, как он писал. </ta>
            <ta e="T818" id="Seg_12065" s="T812">Это мы еще по-камасински не говорили. </ta>
            <ta e="T827" id="Seg_12066" s="T822">Нет, по-русски только записывали. </ta>
            <ta e="T840" id="Seg_12067" s="T833">Нет, это мы не рассказали по-камасински. </ta>
            <ta e="T846" id="Seg_12068" s="T844">Нет. </ta>
            <ta e="T858" id="Seg_12069" s="T856">Ага. </ta>
            <ta e="T1164" id="Seg_12070" s="T1152">Да, и по-русски, а то (это ты мне) еще не рассказывала совсем. </ta>
            <ta e="T1514" id="Seg_12071" s="T1509">Ну, и по-русски можно заодно. </ta>
            <ta e="T1518" id="Seg_12072" s="T1515">((…)). </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T13" id="Seg_12073" s="T10">RUS:ext</ta>
            <ta e="T16" id="Seg_12074" s="T14">RUS:ext</ta>
            <ta e="T25" id="Seg_12075" s="T18">RUS:ext</ta>
            <ta e="T32" id="Seg_12076" s="T31">RUS:ext</ta>
            <ta e="T41" id="Seg_12077" s="T33">RUS:ext</ta>
            <ta e="T43" id="Seg_12078" s="T42">FIN:ext</ta>
            <ta e="T50" id="Seg_12079" s="T48">RUS:ext</ta>
            <ta e="T58" id="Seg_12080" s="T56">RUS:ext</ta>
            <ta e="T96" id="Seg_12081" s="T95">RUS:ext</ta>
            <ta e="T103" id="Seg_12082" s="T97">RUS:ext</ta>
            <ta e="T120" id="Seg_12083" s="T118">RUS:ext</ta>
            <ta e="T122" id="Seg_12084" s="T121">RUS:ext</ta>
            <ta e="T125" id="Seg_12085" s="T123">RUS:ext</ta>
            <ta e="T172" id="Seg_12086" s="T167">RUS:ext</ta>
            <ta e="T213" id="Seg_12087" s="T207">RUS:ext</ta>
            <ta e="T218" id="Seg_12088" s="T214">RUS:ext</ta>
            <ta e="T229" id="Seg_12089" s="T222">RUS:ext</ta>
            <ta e="T248" id="Seg_12090" s="T235">RUS:ext</ta>
            <ta e="T264" id="Seg_12091" s="T262">RUS:ext</ta>
            <ta e="T272" id="Seg_12092" s="T266">RUS:ext</ta>
            <ta e="T285" id="Seg_12093" s="T276">RUS:ext</ta>
            <ta e="T298" id="Seg_12094" s="T293">RUS:ext</ta>
            <ta e="T338" id="Seg_12095" s="T300">RUS:ext</ta>
            <ta e="T357" id="Seg_12096" s="T356">RUS:ext</ta>
            <ta e="T387" id="Seg_12097" s="T365">RUS:ext</ta>
            <ta e="T390" id="Seg_12098" s="T388">RUS:ext</ta>
            <ta e="T394" id="Seg_12099" s="T393">RUS:ext</ta>
            <ta e="T411" id="Seg_12100" s="T409">RUS:ext</ta>
            <ta e="T426" id="Seg_12101" s="T425">RUS:ext</ta>
            <ta e="T434" id="Seg_12102" s="T427">RUS:ext</ta>
            <ta e="T554" id="Seg_12103" s="T546">RUS:ext</ta>
            <ta e="T570" id="Seg_12104" s="T564">RUS:ext</ta>
            <ta e="T717" id="Seg_12105" s="T715">RUS:ext</ta>
            <ta e="T721" id="Seg_12106" s="T717">RUS:ext</ta>
            <ta e="T734" id="Seg_12107" s="T725">RUS:ext</ta>
            <ta e="T811" id="Seg_12108" s="T804">RUS:ext</ta>
            <ta e="T818" id="Seg_12109" s="T812">RUS:ext</ta>
            <ta e="T827" id="Seg_12110" s="T822">RUS:ext</ta>
            <ta e="T840" id="Seg_12111" s="T833">RUS:ext</ta>
            <ta e="T846" id="Seg_12112" s="T844">RUS:ext</ta>
            <ta e="T858" id="Seg_12113" s="T856">RUS:ext</ta>
            <ta e="T1164" id="Seg_12114" s="T1152">RUS:ext</ta>
            <ta e="T1514" id="Seg_12115" s="T1509">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA" />
         <annotation name="fg" tierref="fg-KA" />
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
