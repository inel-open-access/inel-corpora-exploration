<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE138B92E-C7F4-500E-FC23-ABB9695E040A">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_SU0220_rest1_misc</transcription-name>
         <referenced-file url="PKZ_196X_SU0220_rest1_misc.wav" />
         <referenced-file url="PKZ_196X_SU0220_rest1_misc.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0220_rest1_misc\PKZ_196X_SU0220_rest1_misc.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">334</ud-information>
            <ud-information attribute-name="# HIAT:w">196</ud-information>
            <ud-information attribute-name="# e">203</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">7</ud-information>
            <ud-information attribute-name="# HIAT:u">43</ud-information>
            <ud-information attribute-name="# sc">78</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.786" type="appl" />
         <tli id="T0" time="1.0634117647058825" type="intp" />
         <tli id="T3" time="1.572" type="appl" />
         <tli id="T4" time="2.358" type="appl" />
         <tli id="T5" time="3.144" type="appl" />
         <tli id="T6" time="3.93" type="appl" />
         <tli id="T7" time="5.0" type="appl" />
         <tli id="T8" time="6.07" type="appl" />
         <tli id="T9" time="6.905" type="appl" />
         <tli id="T10" time="7.614" type="appl" />
         <tli id="T11" time="8.323" type="appl" />
         <tli id="T12" time="9.032" type="appl" />
         <tli id="T13" time="9.741" type="appl" />
         <tli id="T14" time="10.45" type="appl" />
         <tli id="T15" time="11.293333333333333" type="appl" />
         <tli id="T16" time="12.136666666666667" type="appl" />
         <tli id="T17" time="12.98" type="appl" />
         <tli id="T18" time="13.012" type="appl" />
         <tli id="T19" time="13.722000000000001" type="appl" />
         <tli id="T20" time="14.432" type="appl" />
         <tli id="T21" time="14.707" type="appl" />
         <tli id="T22" time="15.670333333333334" type="appl" />
         <tli id="T23" time="16.633666666666667" type="appl" />
         <tli id="T24" time="17.597" type="appl" />
         <tli id="T25" time="17.872" type="appl" />
         <tli id="T26" time="18.7345" type="appl" />
         <tli id="T27" time="19.597" type="appl" />
         <tli id="T28" time="20.4595" type="appl" />
         <tli id="T29" time="21.322" type="appl" />
         <tli id="T30" time="21.377" type="appl" />
         <tli id="T31" time="22.241" type="appl" />
         <tli id="T32" time="23.105" type="appl" />
         <tli id="T33" time="23.969" type="appl" />
         <tli id="T34" time="24.833" type="appl" />
         <tli id="T35" time="25.697" type="appl" />
         <tli id="T36" time="26.91" type="appl" />
         <tli id="T37" time="27.632" type="appl" />
         <tli id="T38" time="28.354" type="appl" />
         <tli id="T39" time="29.076" type="appl" />
         <tli id="T40" time="29.798" type="appl" />
         <tli id="T41" time="30.52" type="appl" />
         <tli id="T42" time="32.59" type="appl" />
         <tli id="T43" time="33.43272727272728" type="appl" />
         <tli id="T44" time="34.27545454545455" type="appl" />
         <tli id="T45" time="35.11818181818182" type="appl" />
         <tli id="T46" time="35.96090909090909" type="appl" />
         <tli id="T47" time="36.803636363636365" type="appl" />
         <tli id="T48" time="37.64636363636364" type="appl" />
         <tli id="T49" time="38.48909090909091" type="appl" />
         <tli id="T50" time="39.33181818181818" type="appl" />
         <tli id="T51" time="40.17454545454545" type="appl" />
         <tli id="T52" time="41.017272727272726" type="appl" />
         <tli id="T53" time="41.86" type="appl" />
         <tli id="T54" time="42.9" type="appl" />
         <tli id="T55" time="43.49" type="appl" />
         <tli id="T56" time="43.59" type="appl" />
         <tli id="T57" time="44.122" type="appl" />
         <tli id="T58" time="44.654" type="appl" />
         <tli id="T59" time="45.186" type="appl" />
         <tli id="T60" time="45.718" type="appl" />
         <tli id="T61" time="46.25" type="appl" />
         <tli id="T62" time="46.93" type="appl" />
         <tli id="T63" time="47.77125" type="appl" />
         <tli id="T64" time="48.6125" type="appl" />
         <tli id="T65" time="49.45375" type="appl" />
         <tli id="T66" time="50.295" type="appl" />
         <tli id="T67" time="51.13625" type="appl" />
         <tli id="T68" time="51.9775" type="appl" />
         <tli id="T69" time="52.818749999999994" type="appl" />
         <tli id="T70" time="53.66" type="appl" />
         <tli id="T71" time="53.8" type="appl" />
         <tli id="T72" time="54.54" type="appl" />
         <tli id="T73" time="55.28" type="appl" />
         <tli id="T74" time="56.019999999999996" type="appl" />
         <tli id="T75" time="56.76" type="appl" />
         <tli id="T76" time="57.5" type="appl" />
         <tli id="T77" time="58.096" type="appl" />
         <tli id="T78" time="58.692" type="appl" />
         <tli id="T79" time="59.288" type="appl" />
         <tli id="T80" time="59.884" type="appl" />
         <tli id="T81" time="60.48" type="appl" />
         <tli id="T82" time="60.5" type="appl" />
         <tli id="T83" time="61.3375" type="appl" />
         <tli id="T84" time="62.175" type="appl" />
         <tli id="T85" time="63.0125" type="appl" />
         <tli id="T86" time="63.85" type="appl" />
         <tli id="T87" time="64.06" type="appl" />
         <tli id="T88" time="64.48" type="appl" />
         <tli id="T89" time="64.845" type="appl" />
         <tli id="T90" time="65.40944444444445" type="appl" />
         <tli id="T91" time="65.9738888888889" type="appl" />
         <tli id="T92" time="66.53833333333333" type="appl" />
         <tli id="T93" time="67.10277777777777" type="appl" />
         <tli id="T94" time="67.66722222222222" type="appl" />
         <tli id="T95" time="68.23166666666667" type="appl" />
         <tli id="T96" time="68.7961111111111" type="appl" />
         <tli id="T97" time="69.36055555555555" type="appl" />
         <tli id="T98" time="69.925" type="appl" />
         <tli id="T99" time="70.15" type="appl" />
         <tli id="T100" time="70.60000000000001" type="appl" />
         <tli id="T101" time="71.05000000000001" type="appl" />
         <tli id="T102" time="71.5" type="appl" />
         <tli id="T103" time="71.95" type="appl" />
         <tli id="T104" time="72.324" type="appl" />
         <tli id="T105" time="72.698" type="appl" />
         <tli id="T106" time="73.072" type="appl" />
         <tli id="T107" time="73.446" type="appl" />
         <tli id="T108" time="73.82" type="appl" />
         <tli id="T109" time="74.01" type="appl" />
         <tli id="T110" time="74.79666666666667" type="appl" />
         <tli id="T111" time="75.58333333333334" type="appl" />
         <tli id="T112" time="76.37" type="appl" />
         <tli id="T113" time="77.15666666666667" type="appl" />
         <tli id="T114" time="77.94333333333334" type="appl" />
         <tli id="T115" time="78.73" type="appl" />
         <tli id="T116" time="79.44" type="appl" />
         <tli id="T117" time="80.2175" type="appl" />
         <tli id="T236" time="80.49191176470589" type="intp" />
         <tli id="T118" time="80.995" type="appl" />
         <tli id="T119" time="81.7725" type="appl" />
         <tli id="T120" time="82.55" type="appl" />
         <tli id="T121" time="82.56" type="appl" />
         <tli id="T122" time="83.16333333333334" type="appl" />
         <tli id="T123" time="83.76666666666667" type="appl" />
         <tli id="T237" time="83.96777777777778" type="intp" />
         <tli id="T124" time="84.37" type="appl" />
         <tli id="T125" time="84.38" type="appl" />
         <tli id="T126" time="84.926" type="appl" />
         <tli id="T127" time="85.472" type="appl" />
         <tli id="T128" time="86.018" type="appl" />
         <tli id="T129" time="86.564" type="appl" />
         <tli id="T238" time="86.76875" type="intp" />
         <tli id="T130" time="87.11" type="appl" />
         <tli id="T131" time="87.13" type="appl" />
         <tli id="T132" time="87.70666666666666" type="appl" />
         <tli id="T133" time="88.28333333333333" type="appl" />
         <tli id="T134" time="88.86" type="appl" />
         <tli id="T135" time="89.43666666666667" type="appl" />
         <tli id="T136" time="90.01333333333334" type="appl" />
         <tli id="T137" time="90.59" type="appl" />
         <tli id="T138" time="91.145" type="appl" />
         <tli id="T139" time="92.00166666666667" type="appl" />
         <tli id="T140" time="92.85833333333333" type="appl" />
         <tli id="T141" time="93.715" type="appl" />
         <tli id="T142" time="94.905" type="appl" />
         <tli id="T143" time="95.72" type="appl" />
         <tli id="T144" time="96.535" type="appl" />
         <tli id="T145" time="97.35000000000001" type="appl" />
         <tli id="T146" time="98.165" type="appl" />
         <tli id="T147" time="98.77" type="appl" />
         <tli id="T148" time="100.18" type="appl" />
         <tli id="T149" time="101.62" type="appl" />
         <tli id="T150" time="102.34" type="appl" />
         <tli id="T151" time="103.06" type="appl" />
         <tli id="T152" time="103.78" type="appl" />
         <tli id="T153" time="104.5" type="appl" />
         <tli id="T239" time="104.77" type="intp" />
         <tli id="T154" time="105.22" type="appl" />
         <tli id="T155" time="105.315" type="appl" />
         <tli id="T156" time="105.73785714285714" type="appl" />
         <tli id="T157" time="106.16071428571429" type="appl" />
         <tli id="T158" time="106.58357142857143" type="appl" />
         <tli id="T159" time="107.00642857142857" type="appl" />
         <tli id="T160" time="107.42928571428571" type="appl" />
         <tli id="T161" time="107.85214285714287" type="appl" />
         <tli id="T240" time="108.00138655462186" type="intp" />
         <tli id="T162" time="108.275" type="appl" />
         <tli id="T163" time="108.29" type="appl" />
         <tli id="T164" time="109.03" type="appl" />
         <tli id="T165" time="109.77000000000001" type="appl" />
         <tli id="T166" time="110.51" type="appl" />
         <tli id="T241" time="110.77117647058824" type="intp" />
         <tli id="T167" time="111.25" type="appl" />
         <tli id="T168" time="112.435" type="appl" />
         <tli id="T169" time="112.995" type="appl" />
         <tli id="T170" time="113.555" type="appl" />
         <tli id="T171" time="114.115" type="appl" />
         <tli id="T172" time="114.675" type="appl" />
         <tli id="T173" time="115.3" type="appl" />
         <tli id="T174" time="115.73" type="appl" />
         <tli id="T175" time="116.41" type="appl" />
         <tli id="T176" time="117.04875" type="appl" />
         <tli id="T177" time="117.6875" type="appl" />
         <tli id="T178" time="118.32625" type="appl" />
         <tli id="T179" time="118.965" type="appl" />
         <tli id="T180" time="119.60374999999999" type="appl" />
         <tli id="T181" time="120.24249999999999" type="appl" />
         <tli id="T182" time="120.88125" type="appl" />
         <tli id="T183" time="121.52" type="appl" />
         <tli id="T184" time="121.64" type="appl" />
         <tli id="T185" time="122.46" type="appl" />
         <tli id="T186" time="123.28" type="appl" />
         <tli id="T187" time="124.1" type="appl" />
         <tli id="T188" time="124.92" type="appl" />
         <tli id="T189" time="125.74" type="appl" />
         <tli id="T190" time="126.56" type="appl" />
         <tli id="T191" time="127.38" type="appl" />
         <tli id="T192" time="127.7" type="appl" />
         <tli id="T193" time="127.85" type="appl" />
         <tli id="T194" time="128.215" type="appl" />
         <tli id="T195" time="128.893" type="appl" />
         <tli id="T196" time="129.571" type="appl" />
         <tli id="T197" time="130.249" type="appl" />
         <tli id="T198" time="130.927" type="appl" />
         <tli id="T199" time="131.605" type="appl" />
         <tli id="T200" time="132.44" type="appl" />
         <tli id="T201" time="133.3492857142857" type="appl" />
         <tli id="T202" time="134.25857142857143" type="appl" />
         <tli id="T203" time="135.16785714285714" type="appl" />
         <tli id="T204" time="136.07714285714286" type="appl" />
         <tli id="T205" time="136.98642857142858" type="appl" />
         <tli id="T206" time="137.8957142857143" type="appl" />
         <tli id="T207" time="138.805" type="appl" />
         <tli id="T208" time="140.34" type="appl" />
         <tli id="T209" time="140.78" type="appl" />
         <tli id="T210" time="141.737" type="appl" />
         <tli id="T211" time="142.387" type="appl" />
         <tli id="T212" time="143.037" type="appl" />
         <tli id="T213" time="143.687" type="appl" />
         <tli id="T214" time="144.337" type="appl" />
         <tli id="T215" time="144.987" type="appl" />
         <tli id="T216" time="145.637" type="appl" />
         <tli id="T217" time="146.287" type="appl" />
         <tli id="T218" time="146.527" type="appl" />
         <tli id="T219" time="147.3245" type="appl" />
         <tli id="T220" time="148.122" type="appl" />
         <tli id="T221" time="148.9195" type="appl" />
         <tli id="T222" time="149.717" type="appl" />
         <tli id="T223" time="150.457" type="appl" />
         <tli id="T224" time="151.2595" type="appl" />
         <tli id="T225" time="152.062" type="appl" />
         <tli id="T226" time="152.8645" type="appl" />
         <tli id="T227" time="153.667" type="appl" />
         <tli id="T228" time="153.987" type="appl" />
         <tli id="T229" time="154.96314285714286" type="appl" />
         <tli id="T230" time="155.93928571428572" type="appl" />
         <tli id="T231" time="156.91542857142858" type="appl" />
         <tli id="T232" time="157.8915714285714" type="appl" />
         <tli id="T233" time="158.86771428571427" type="appl" />
         <tli id="T234" time="159.84385714285713" type="appl" />
         <tli id="T235" time="160.82" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T8" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Šiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_7" n="HIAT:w" s="T2">kalla</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T0">dʼürbileʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">măn</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">oʔbdəbiam</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">bar</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Kambiam</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">nükenə</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_32" n="sc" s="T9">
               <ts e="T14" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">bar</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">măna</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">perluʔpi</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">bar</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">Šide</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">ădʼejală</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">ibiʔi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T20" id="Seg_63" n="sc" s="T18">
               <ts e="T20" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Oldʼa</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">ibiʔi</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T24" id="Seg_73" n="sc" s="T21">
               <ts e="T24" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Mašina</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">ibiʔi</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">šödörizittə</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_87" n="sc" s="T25">
               <ts e="T29" id="Seg_89" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">Iʔgö</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">ibiʔi</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">esseŋgəndə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">ibiʔi</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T35" id="Seg_104" n="sc" s="T30">
               <ts e="T35" id="Seg_106" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">Dĭgəttə</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">sʼel</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">ibiʔi</ts>
                  <nts id="Seg_115" n="HIAT:ip">,</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">bʼeʔ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">kilo</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_124" n="sc" s="T36">
               <ts e="T41" id="Seg_126" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_128" n="HIAT:w" s="T36">Šide</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_131" n="HIAT:w" s="T37">šalkovej</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_134" n="HIAT:w" s="T38">pʼel</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">onʼiʔ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_140" n="HIAT:w" s="T40">kilo</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_143" n="sc" s="T42">
               <ts e="T53" id="Seg_145" n="HIAT:u" s="T42">
                  <nts id="Seg_146" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">Na-</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_151" n="HIAT:w" s="T43">na-</ts>
                  <nts id="Seg_152" n="HIAT:ip">)</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">nagur</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">sotnʼə</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_162" n="HIAT:w" s="T46">mĭbiʔi</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_166" n="HIAT:w" s="T47">mĭbi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_169" n="HIAT:w" s="T48">dĭzeŋdə</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_172" n="HIAT:w" s="T49">predsedatelʼ</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_176" n="HIAT:w" s="T50">iʔgö</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_179" n="HIAT:w" s="T51">aktʼa</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">ibiʔi</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_185" n="sc" s="T54">
               <ts e="T55" id="Seg_187" n="HIAT:u" s="T54">
                  <nts id="Seg_188" n="HIAT:ip">(</nts>
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <ats e="T55" id="Seg_190" n="HIAT:non-pho" s="T54">BRK</ats>
                  <nts id="Seg_191" n="HIAT:ip">)</nts>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_195" n="sc" s="T56">
               <ts e="T61" id="Seg_197" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_199" n="HIAT:w" s="T56">Măn</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_202" n="HIAT:w" s="T57">kundʼo</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_205" n="HIAT:w" s="T58">dĭn</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_207" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_209" n="HIAT:w" s="T59">m-</ts>
                  <nts id="Seg_210" n="HIAT:ip">)</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">amnobiam</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T70" id="Seg_216" n="sc" s="T62">
               <ts e="T70" id="Seg_218" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_220" n="HIAT:w" s="T62">Dĭn</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_223" n="HIAT:w" s="T63">unučkat</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_226" n="HIAT:w" s="T64">bar</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_229" n="HIAT:w" s="T65">södörbi</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_232" n="HIAT:w" s="T66">fartuk</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_236" n="HIAT:w" s="T67">diʔnə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_239" n="HIAT:w" s="T68">dĭ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_242" n="HIAT:w" s="T69">nükenə</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_245" n="sc" s="T71">
               <ts e="T76" id="Seg_247" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_249" n="HIAT:w" s="T71">Dĭgəttə</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_252" n="HIAT:w" s="T72">amnobiam</ts>
                  <nts id="Seg_253" n="HIAT:ip">,</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_256" n="HIAT:w" s="T73">amnobiam</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">kunolzittə</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">axota</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_267" n="HIAT:u" s="T76">
                  <nts id="Seg_268" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_270" n="HIAT:w" s="T76">Măndəm</ts>
                  <nts id="Seg_271" n="HIAT:ip">)</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_274" n="HIAT:w" s="T77">Kalam</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_277" n="HIAT:w" s="T78">maʔnʼi</ts>
                  <nts id="Seg_278" n="HIAT:ip">,</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_281" n="HIAT:w" s="T79">kunolzittə</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_284" n="HIAT:w" s="T80">nada</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_287" n="sc" s="T82">
               <ts e="T86" id="Seg_289" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_291" n="HIAT:w" s="T82">Dĭgəttə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_294" n="HIAT:w" s="T83">šobiam</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_298" n="HIAT:w" s="T84">iʔbəbiem</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_301" n="HIAT:w" s="T85">kunolzittə</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T88" id="Seg_304" n="sc" s="T87">
               <ts e="T88" id="Seg_306" n="HIAT:u" s="T87">
                  <nts id="Seg_307" n="HIAT:ip">(</nts>
                  <nts id="Seg_308" n="HIAT:ip">(</nts>
                  <ats e="T88" id="Seg_309" n="HIAT:non-pho" s="T87">BRK</ats>
                  <nts id="Seg_310" n="HIAT:ip">)</nts>
                  <nts id="Seg_311" n="HIAT:ip">)</nts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T98" id="Seg_314" n="sc" s="T89">
               <ts e="T98" id="Seg_316" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_318" n="HIAT:w" s="T89">Dĭgəttə</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_321" n="HIAT:w" s="T90">ertən</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_323" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_325" n="HIAT:w" s="T91">s-</ts>
                  <nts id="Seg_326" n="HIAT:ip">)</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_329" n="HIAT:w" s="T92">uʔbdəbiam</ts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_333" n="HIAT:w" s="T93">onʼĭʔ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_336" n="HIAT:w" s="T94">tibi</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_339" n="HIAT:w" s="T95">šobi</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_342" n="HIAT:w" s="T96">măna</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_345" n="HIAT:w" s="T97">monoʔkosʼtə</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_348" n="sc" s="T99">
               <ts e="T103" id="Seg_350" n="HIAT:u" s="T99">
                  <nts id="Seg_351" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_353" n="HIAT:w" s="T99">Tăm</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_356" n="HIAT:w" s="T100">măna</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_359" n="HIAT:w" s="T101">kalal</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_362" n="HIAT:w" s="T102">tibinə</ts>
                  <nts id="Seg_363" n="HIAT:ip">?</nts>
                  <nts id="Seg_364" n="HIAT:ip">"</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_367" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_369" n="HIAT:w" s="T103">A</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_372" n="HIAT:w" s="T104">măn</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_375" n="HIAT:w" s="T105">măndəm:</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_377" n="HIAT:ip">"</nts>
                  <ts e="T107" id="Seg_379" n="HIAT:w" s="T106">Em</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">kanaʔ</ts>
                  <nts id="Seg_383" n="HIAT:ip">"</nts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_386" n="sc" s="T109">
               <ts e="T115" id="Seg_388" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_390" n="HIAT:w" s="T109">Măn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_393" n="HIAT:w" s="T110">il</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_396" n="HIAT:w" s="T111">ige</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_400" n="HIAT:w" s="T112">mĭŋgeʔi</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_403" n="HIAT:w" s="T113">dʼăbaktərzittə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_406" n="HIAT:w" s="T114">nudlaʔ</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_409" n="sc" s="T116">
               <ts e="T120" id="Seg_411" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_413" n="HIAT:w" s="T116">Kamen</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_416" n="HIAT:w" s="T117">kalla</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_419" n="HIAT:w" s="T236">dʼürləʔjə</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_423" n="HIAT:w" s="T118">dĭgəttə</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_426" n="HIAT:w" s="T119">kalam</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_429" n="sc" s="T121">
               <ts e="T124" id="Seg_431" n="HIAT:u" s="T121">
                  <nts id="Seg_432" n="HIAT:ip">"</nts>
                  <ts e="T122" id="Seg_434" n="HIAT:w" s="T121">A</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_437" n="HIAT:w" s="T122">kamen</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_440" n="HIAT:w" s="T123">kalla</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_443" n="HIAT:w" s="T237">dʼürləʔjə</ts>
                  <nts id="Seg_444" n="HIAT:ip">?</nts>
                  <nts id="Seg_445" n="HIAT:ip">"</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_447" n="sc" s="T125">
               <ts e="T130" id="Seg_449" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_451" n="HIAT:w" s="T125">Măn</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_454" n="HIAT:w" s="T126">ej</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_457" n="HIAT:w" s="T127">tĭmnem</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_461" n="HIAT:w" s="T128">kamən</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_464" n="HIAT:w" s="T129">kalla</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_467" n="HIAT:w" s="T238">dʼürləʔi</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_470" n="sc" s="T131">
               <ts e="T137" id="Seg_472" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_474" n="HIAT:w" s="T131">Dĭ</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_477" n="HIAT:w" s="T132">măndə:</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_479" n="HIAT:ip">"</nts>
                  <ts e="T134" id="Seg_481" n="HIAT:w" s="T133">Šüdördə</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_484" n="HIAT:w" s="T134">măna</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_488" n="HIAT:w" s="T135">per-</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_492" n="HIAT:w" s="T136">părga</ts>
                  <nts id="Seg_493" n="HIAT:ip">"</nts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_496" n="sc" s="T138">
               <ts e="T141" id="Seg_498" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_500" n="HIAT:w" s="T138">A</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">măna</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_505" n="HIAT:ip">(</nts>
                  <ts e="T141" id="Seg_507" n="HIAT:w" s="T140">algaʔndə</ts>
                  <nts id="Seg_508" n="HIAT:ip">)</nts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T146" id="Seg_511" n="sc" s="T142">
               <ts e="T146" id="Seg_513" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">Šüdörzittə</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_517" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_519" n="HIAT:w" s="T143">ɨr-</ts>
                  <nts id="Seg_520" n="HIAT:ip">)</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_523" n="HIAT:w" s="T144">ɨrɨ</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_526" n="HIAT:w" s="T145">molambiam</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_529" n="sc" s="T147">
               <ts e="T148" id="Seg_531" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_533" n="HIAT:w" s="T147">ɨrɨmniem</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T154" id="Seg_536" n="sc" s="T149">
               <ts e="T154" id="Seg_538" n="HIAT:u" s="T149">
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <nts id="Seg_540" n="HIAT:ip">(</nts>
                  <ats e="T150" id="Seg_541" n="HIAT:non-pho" s="T149">BRK</ats>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip">)</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_545" n="HIAT:ip">(</nts>
                  <ts e="T151" id="Seg_547" n="HIAT:w" s="T150">-narlaʔbə</ts>
                  <nts id="Seg_548" n="HIAT:ip">)</nts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_552" n="HIAT:w" s="T151">kamen</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_555" n="HIAT:w" s="T152">dĭzeŋ</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_558" n="HIAT:w" s="T153">kalla</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_561" n="HIAT:w" s="T239">dʼürləʔi</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T162" id="Seg_564" n="sc" s="T155">
               <ts e="T162" id="Seg_566" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_568" n="HIAT:w" s="T155">A</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_571" n="HIAT:w" s="T156">măn</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_574" n="HIAT:w" s="T157">ej</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_577" n="HIAT:w" s="T158">tĭmnem</ts>
                  <nts id="Seg_578" n="HIAT:ip">,</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_581" n="HIAT:w" s="T159">kamen</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_583" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_585" n="HIAT:w" s="T160">dʼ-</ts>
                  <nts id="Seg_586" n="HIAT:ip">)</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_589" n="HIAT:w" s="T161">kalla</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_592" n="HIAT:w" s="T240">dʼürləʔjə</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T167" id="Seg_595" n="sc" s="T163">
               <ts e="T167" id="Seg_597" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_599" n="HIAT:w" s="T163">Kanaʔ</ts>
                  <nts id="Seg_600" n="HIAT:ip">,</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_603" n="HIAT:w" s="T164">suraraʔ</ts>
                  <nts id="Seg_604" n="HIAT:ip">,</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_607" n="HIAT:w" s="T165">kamen</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_610" n="HIAT:w" s="T166">kalla</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_613" n="HIAT:w" s="T241">dʼürləʔjə</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_616" n="sc" s="T168">
               <ts e="T172" id="Seg_618" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_620" n="HIAT:w" s="T168">Dĭgəttə</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_623" n="HIAT:w" s="T169">dĭ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_626" n="HIAT:w" s="T170">ej</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_629" n="HIAT:w" s="T171">kambi</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_632" n="sc" s="T173">
               <ts e="T174" id="Seg_634" n="HIAT:u" s="T173">
                  <nts id="Seg_635" n="HIAT:ip">(</nts>
                  <nts id="Seg_636" n="HIAT:ip">(</nts>
                  <ats e="T174" id="Seg_637" n="HIAT:non-pho" s="T173">BRK</ats>
                  <nts id="Seg_638" n="HIAT:ip">)</nts>
                  <nts id="Seg_639" n="HIAT:ip">)</nts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_642" n="sc" s="T175">
               <ts e="T183" id="Seg_644" n="HIAT:u" s="T175">
                  <nts id="Seg_645" n="HIAT:ip">(</nts>
                  <ts e="T176" id="Seg_647" n="HIAT:w" s="T175">Mănavʼitʼuʔšiʔ</ts>
                  <nts id="Seg_648" n="HIAT:ip">)</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_651" n="HIAT:w" s="T176">ĭmbidə</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_654" n="HIAT:w" s="T177">ej</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_657" n="HIAT:w" s="T178">nörbəliaʔi</ts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_661" n="HIAT:w" s="T179">a</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_664" n="HIAT:w" s="T180">măn</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_667" n="HIAT:w" s="T181">ulum</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_670" n="HIAT:w" s="T182">urgo</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T191" id="Seg_673" n="sc" s="T184">
               <ts e="T191" id="Seg_675" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_677" n="HIAT:w" s="T184">Bar</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_679" n="HIAT:ip">(</nts>
                  <ts e="T186" id="Seg_681" n="HIAT:w" s="T185">tăn</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_684" n="HIAT:w" s="T186">öʔlüʔpiel=</ts>
                  <nts id="Seg_685" n="HIAT:ip">)</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_688" n="HIAT:w" s="T187">tăn</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_690" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_692" n="HIAT:w" s="T188">öʔlüʔpiem</ts>
                  <nts id="Seg_693" n="HIAT:ip">)</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_696" n="HIAT:w" s="T189">ĭmbi</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_699" n="HIAT:w" s="T190">dʼăbaktərzittə</ts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T193" id="Seg_702" n="sc" s="T192">
               <ts e="T193" id="Seg_704" n="HIAT:u" s="T192">
                  <nts id="Seg_705" n="HIAT:ip">(</nts>
                  <nts id="Seg_706" n="HIAT:ip">(</nts>
                  <ats e="T193" id="Seg_707" n="HIAT:non-pho" s="T192">BRK</ats>
                  <nts id="Seg_708" n="HIAT:ip">)</nts>
                  <nts id="Seg_709" n="HIAT:ip">)</nts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T199" id="Seg_712" n="sc" s="T194">
               <ts e="T199" id="Seg_714" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_716" n="HIAT:w" s="T194">Măn</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_719" n="HIAT:w" s="T195">ugandə</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_722" n="HIAT:w" s="T196">kələʔsəbi</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_725" n="HIAT:w" s="T197">kuza</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_728" n="HIAT:w" s="T198">igem</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T207" id="Seg_731" n="sc" s="T200">
               <ts e="T207" id="Seg_733" n="HIAT:u" s="T200">
                  <nts id="Seg_734" n="HIAT:ip">(</nts>
                  <nts id="Seg_735" n="HIAT:ip">(</nts>
                  <ats e="T201" id="Seg_736" n="HIAT:non-pho" s="T200">BRK</ats>
                  <nts id="Seg_737" n="HIAT:ip">)</nts>
                  <nts id="Seg_738" n="HIAT:ip">)</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_741" n="HIAT:w" s="T201">măn</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_744" n="HIAT:w" s="T202">šiʔnʼileʔ</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_747" n="HIAT:w" s="T203">surarlam</ts>
                  <nts id="Seg_748" n="HIAT:ip">,</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_751" n="HIAT:w" s="T204">kamen</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_754" n="HIAT:w" s="T205">maʔnʼi</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_757" n="HIAT:w" s="T206">kaləlaʔ</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_760" n="sc" s="T208">
               <ts e="T209" id="Seg_762" n="HIAT:u" s="T208">
                  <nts id="Seg_763" n="HIAT:ip">(</nts>
                  <nts id="Seg_764" n="HIAT:ip">(</nts>
                  <ats e="T209" id="Seg_765" n="HIAT:non-pho" s="T208">BRK</ats>
                  <nts id="Seg_766" n="HIAT:ip">)</nts>
                  <nts id="Seg_767" n="HIAT:ip">)</nts>
                  <nts id="Seg_768" n="HIAT:ip">.</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T217" id="Seg_770" n="sc" s="T210">
               <ts e="T217" id="Seg_772" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_774" n="HIAT:w" s="T210">Lem</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_777" n="HIAT:w" s="T211">pa</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_780" n="HIAT:w" s="T212">üdʼüge</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_783" n="HIAT:w" s="T213">ibi</ts>
                  <nts id="Seg_784" n="HIAT:ip">,</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_787" n="HIAT:w" s="T214">dĭgəttə</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_790" n="HIAT:w" s="T215">özerbi</ts>
                  <nts id="Seg_791" n="HIAT:ip">,</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_794" n="HIAT:w" s="T216">özerbi</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T222" id="Seg_797" n="sc" s="T218">
               <ts e="T222" id="Seg_799" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_801" n="HIAT:w" s="T218">Ugandə</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_803" n="HIAT:ip">(</nts>
                  <ts e="T220" id="Seg_805" n="HIAT:w" s="T219">nʼiš-</ts>
                  <nts id="Seg_806" n="HIAT:ip">)</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_809" n="HIAT:w" s="T220">nʼispək</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_812" n="HIAT:w" s="T221">molambi</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_815" n="sc" s="T223">
               <ts e="T227" id="Seg_817" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_819" n="HIAT:w" s="T223">Dĭn</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_822" n="HIAT:w" s="T224">tondə</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_825" n="HIAT:w" s="T225">možna</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_828" n="HIAT:w" s="T226">šaʔsʼittə</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T235" id="Seg_831" n="sc" s="T228">
               <ts e="T235" id="Seg_833" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_835" n="HIAT:w" s="T228">I</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_838" n="HIAT:w" s="T229">surno</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_841" n="HIAT:w" s="T230">ej</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_843" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_845" n="HIAT:w" s="T231">bĭzəjdlʼit-</ts>
                  <nts id="Seg_846" n="HIAT:ip">)</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_849" n="HIAT:w" s="T232">ej</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_852" n="HIAT:w" s="T233">bĭzəjdləl</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_855" n="HIAT:w" s="T234">tănan</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T8" id="Seg_858" n="sc" s="T1">
               <ts e="T2" id="Seg_860" n="e" s="T1">Šiʔ </ts>
               <ts e="T0" id="Seg_862" n="e" s="T2">kalla </ts>
               <ts e="T3" id="Seg_864" n="e" s="T0">dʼürbileʔ, </ts>
               <ts e="T4" id="Seg_866" n="e" s="T3">măn </ts>
               <ts e="T5" id="Seg_868" n="e" s="T4">oʔbdəbiam </ts>
               <ts e="T6" id="Seg_870" n="e" s="T5">bar. </ts>
               <ts e="T7" id="Seg_872" n="e" s="T6">Kambiam </ts>
               <ts e="T8" id="Seg_874" n="e" s="T7">nükenə. </ts>
            </ts>
            <ts e="T17" id="Seg_875" n="sc" s="T9">
               <ts e="T10" id="Seg_877" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_879" n="e" s="T10">bar </ts>
               <ts e="T12" id="Seg_881" n="e" s="T11">măna </ts>
               <ts e="T13" id="Seg_883" n="e" s="T12">perluʔpi </ts>
               <ts e="T14" id="Seg_885" n="e" s="T13">bar. </ts>
               <ts e="T15" id="Seg_887" n="e" s="T14">Šide </ts>
               <ts e="T16" id="Seg_889" n="e" s="T15">ădʼejală </ts>
               <ts e="T17" id="Seg_891" n="e" s="T16">ibiʔi. </ts>
            </ts>
            <ts e="T20" id="Seg_892" n="sc" s="T18">
               <ts e="T19" id="Seg_894" n="e" s="T18">Oldʼa </ts>
               <ts e="T20" id="Seg_896" n="e" s="T19">ibiʔi. </ts>
            </ts>
            <ts e="T24" id="Seg_897" n="sc" s="T21">
               <ts e="T22" id="Seg_899" n="e" s="T21">Mašina </ts>
               <ts e="T23" id="Seg_901" n="e" s="T22">ibiʔi, </ts>
               <ts e="T24" id="Seg_903" n="e" s="T23">šödörizittə. </ts>
            </ts>
            <ts e="T29" id="Seg_904" n="sc" s="T25">
               <ts e="T26" id="Seg_906" n="e" s="T25">Iʔgö </ts>
               <ts e="T27" id="Seg_908" n="e" s="T26">ibiʔi, </ts>
               <ts e="T28" id="Seg_910" n="e" s="T27">esseŋgəndə </ts>
               <ts e="T29" id="Seg_912" n="e" s="T28">ibiʔi. </ts>
            </ts>
            <ts e="T35" id="Seg_913" n="sc" s="T30">
               <ts e="T31" id="Seg_915" n="e" s="T30">Dĭgəttə </ts>
               <ts e="T32" id="Seg_917" n="e" s="T31">sʼel </ts>
               <ts e="T33" id="Seg_919" n="e" s="T32">ibiʔi, </ts>
               <ts e="T34" id="Seg_921" n="e" s="T33">bʼeʔ </ts>
               <ts e="T35" id="Seg_923" n="e" s="T34">kilo. </ts>
            </ts>
            <ts e="T41" id="Seg_924" n="sc" s="T36">
               <ts e="T37" id="Seg_926" n="e" s="T36">Šide </ts>
               <ts e="T38" id="Seg_928" n="e" s="T37">šalkovej </ts>
               <ts e="T39" id="Seg_930" n="e" s="T38">pʼel </ts>
               <ts e="T40" id="Seg_932" n="e" s="T39">onʼiʔ </ts>
               <ts e="T41" id="Seg_934" n="e" s="T40">kilo. </ts>
            </ts>
            <ts e="T53" id="Seg_935" n="sc" s="T42">
               <ts e="T43" id="Seg_937" n="e" s="T42">(Na- </ts>
               <ts e="T44" id="Seg_939" n="e" s="T43">na-) </ts>
               <ts e="T45" id="Seg_941" n="e" s="T44">nagur </ts>
               <ts e="T46" id="Seg_943" n="e" s="T45">sotnʼə </ts>
               <ts e="T47" id="Seg_945" n="e" s="T46">(mĭbiʔi) </ts>
               <ts e="T48" id="Seg_947" n="e" s="T47">mĭbi </ts>
               <ts e="T49" id="Seg_949" n="e" s="T48">dĭzeŋdə </ts>
               <ts e="T50" id="Seg_951" n="e" s="T49">predsedatelʼ, </ts>
               <ts e="T51" id="Seg_953" n="e" s="T50">iʔgö </ts>
               <ts e="T52" id="Seg_955" n="e" s="T51">aktʼa </ts>
               <ts e="T53" id="Seg_957" n="e" s="T52">ibiʔi. </ts>
            </ts>
            <ts e="T55" id="Seg_958" n="sc" s="T54">
               <ts e="T55" id="Seg_960" n="e" s="T54">((BRK)). </ts>
            </ts>
            <ts e="T61" id="Seg_961" n="sc" s="T56">
               <ts e="T57" id="Seg_963" n="e" s="T56">Măn </ts>
               <ts e="T58" id="Seg_965" n="e" s="T57">kundʼo </ts>
               <ts e="T59" id="Seg_967" n="e" s="T58">dĭn </ts>
               <ts e="T60" id="Seg_969" n="e" s="T59">(m-) </ts>
               <ts e="T61" id="Seg_971" n="e" s="T60">amnobiam. </ts>
            </ts>
            <ts e="T70" id="Seg_972" n="sc" s="T62">
               <ts e="T63" id="Seg_974" n="e" s="T62">Dĭn </ts>
               <ts e="T64" id="Seg_976" n="e" s="T63">unučkat </ts>
               <ts e="T65" id="Seg_978" n="e" s="T64">bar </ts>
               <ts e="T66" id="Seg_980" n="e" s="T65">södörbi </ts>
               <ts e="T67" id="Seg_982" n="e" s="T66">fartuk, </ts>
               <ts e="T68" id="Seg_984" n="e" s="T67">diʔnə </ts>
               <ts e="T69" id="Seg_986" n="e" s="T68">dĭ </ts>
               <ts e="T70" id="Seg_988" n="e" s="T69">nükenə. </ts>
            </ts>
            <ts e="T81" id="Seg_989" n="sc" s="T71">
               <ts e="T72" id="Seg_991" n="e" s="T71">Dĭgəttə </ts>
               <ts e="T73" id="Seg_993" n="e" s="T72">amnobiam, </ts>
               <ts e="T74" id="Seg_995" n="e" s="T73">amnobiam, </ts>
               <ts e="T75" id="Seg_997" n="e" s="T74">kunolzittə </ts>
               <ts e="T76" id="Seg_999" n="e" s="T75">axota. </ts>
               <ts e="T77" id="Seg_1001" n="e" s="T76">(Măndəm) </ts>
               <ts e="T78" id="Seg_1003" n="e" s="T77">Kalam </ts>
               <ts e="T79" id="Seg_1005" n="e" s="T78">maʔnʼi, </ts>
               <ts e="T80" id="Seg_1007" n="e" s="T79">kunolzittə </ts>
               <ts e="T81" id="Seg_1009" n="e" s="T80">nada. </ts>
            </ts>
            <ts e="T86" id="Seg_1010" n="sc" s="T82">
               <ts e="T83" id="Seg_1012" n="e" s="T82">Dĭgəttə </ts>
               <ts e="T84" id="Seg_1014" n="e" s="T83">šobiam, </ts>
               <ts e="T85" id="Seg_1016" n="e" s="T84">iʔbəbiem </ts>
               <ts e="T86" id="Seg_1018" n="e" s="T85">kunolzittə. </ts>
            </ts>
            <ts e="T88" id="Seg_1019" n="sc" s="T87">
               <ts e="T88" id="Seg_1021" n="e" s="T87">((BRK)). </ts>
            </ts>
            <ts e="T98" id="Seg_1022" n="sc" s="T89">
               <ts e="T90" id="Seg_1024" n="e" s="T89">Dĭgəttə </ts>
               <ts e="T91" id="Seg_1026" n="e" s="T90">ertən </ts>
               <ts e="T92" id="Seg_1028" n="e" s="T91">(s-) </ts>
               <ts e="T93" id="Seg_1030" n="e" s="T92">uʔbdəbiam, </ts>
               <ts e="T94" id="Seg_1032" n="e" s="T93">onʼĭʔ </ts>
               <ts e="T95" id="Seg_1034" n="e" s="T94">tibi </ts>
               <ts e="T96" id="Seg_1036" n="e" s="T95">šobi </ts>
               <ts e="T97" id="Seg_1038" n="e" s="T96">măna </ts>
               <ts e="T98" id="Seg_1040" n="e" s="T97">monoʔkosʼtə. </ts>
            </ts>
            <ts e="T108" id="Seg_1041" n="sc" s="T99">
               <ts e="T100" id="Seg_1043" n="e" s="T99">"Tăm </ts>
               <ts e="T101" id="Seg_1045" n="e" s="T100">măna </ts>
               <ts e="T102" id="Seg_1047" n="e" s="T101">kalal </ts>
               <ts e="T103" id="Seg_1049" n="e" s="T102">tibinə?" </ts>
               <ts e="T104" id="Seg_1051" n="e" s="T103">A </ts>
               <ts e="T105" id="Seg_1053" n="e" s="T104">măn </ts>
               <ts e="T106" id="Seg_1055" n="e" s="T105">măndəm: </ts>
               <ts e="T107" id="Seg_1057" n="e" s="T106">"Em </ts>
               <ts e="T108" id="Seg_1059" n="e" s="T107">kanaʔ". </ts>
            </ts>
            <ts e="T115" id="Seg_1060" n="sc" s="T109">
               <ts e="T110" id="Seg_1062" n="e" s="T109">Măn </ts>
               <ts e="T111" id="Seg_1064" n="e" s="T110">il </ts>
               <ts e="T112" id="Seg_1066" n="e" s="T111">ige, </ts>
               <ts e="T113" id="Seg_1068" n="e" s="T112">mĭŋgeʔi </ts>
               <ts e="T114" id="Seg_1070" n="e" s="T113">dʼăbaktərzittə </ts>
               <ts e="T115" id="Seg_1072" n="e" s="T114">nudlaʔ. </ts>
            </ts>
            <ts e="T120" id="Seg_1073" n="sc" s="T116">
               <ts e="T117" id="Seg_1075" n="e" s="T116">Kamen </ts>
               <ts e="T236" id="Seg_1077" n="e" s="T117">kalla </ts>
               <ts e="T118" id="Seg_1079" n="e" s="T236">dʼürləʔjə, </ts>
               <ts e="T119" id="Seg_1081" n="e" s="T118">dĭgəttə </ts>
               <ts e="T120" id="Seg_1083" n="e" s="T119">kalam. </ts>
            </ts>
            <ts e="T124" id="Seg_1084" n="sc" s="T121">
               <ts e="T122" id="Seg_1086" n="e" s="T121">"A </ts>
               <ts e="T123" id="Seg_1088" n="e" s="T122">kamen </ts>
               <ts e="T237" id="Seg_1090" n="e" s="T123">kalla </ts>
               <ts e="T124" id="Seg_1092" n="e" s="T237">dʼürləʔjə?" </ts>
            </ts>
            <ts e="T130" id="Seg_1093" n="sc" s="T125">
               <ts e="T126" id="Seg_1095" n="e" s="T125">Măn </ts>
               <ts e="T127" id="Seg_1097" n="e" s="T126">ej </ts>
               <ts e="T128" id="Seg_1099" n="e" s="T127">tĭmnem, </ts>
               <ts e="T129" id="Seg_1101" n="e" s="T128">kamən </ts>
               <ts e="T238" id="Seg_1103" n="e" s="T129">kalla </ts>
               <ts e="T130" id="Seg_1105" n="e" s="T238">dʼürləʔi. </ts>
            </ts>
            <ts e="T137" id="Seg_1106" n="sc" s="T131">
               <ts e="T132" id="Seg_1108" n="e" s="T131">Dĭ </ts>
               <ts e="T133" id="Seg_1110" n="e" s="T132">măndə: </ts>
               <ts e="T134" id="Seg_1112" n="e" s="T133">"Šüdördə </ts>
               <ts e="T135" id="Seg_1114" n="e" s="T134">măna </ts>
               <ts e="T136" id="Seg_1116" n="e" s="T135">(per-) </ts>
               <ts e="T137" id="Seg_1118" n="e" s="T136">părga". </ts>
            </ts>
            <ts e="T141" id="Seg_1119" n="sc" s="T138">
               <ts e="T139" id="Seg_1121" n="e" s="T138">A </ts>
               <ts e="T140" id="Seg_1123" n="e" s="T139">măna </ts>
               <ts e="T141" id="Seg_1125" n="e" s="T140">(algaʔndə). </ts>
            </ts>
            <ts e="T146" id="Seg_1126" n="sc" s="T142">
               <ts e="T143" id="Seg_1128" n="e" s="T142">Šüdörzittə </ts>
               <ts e="T144" id="Seg_1130" n="e" s="T143">(ɨr-) </ts>
               <ts e="T145" id="Seg_1132" n="e" s="T144">ɨrɨ </ts>
               <ts e="T146" id="Seg_1134" n="e" s="T145">molambiam. </ts>
            </ts>
            <ts e="T148" id="Seg_1135" n="sc" s="T147">
               <ts e="T148" id="Seg_1137" n="e" s="T147">ɨrɨmniem. </ts>
            </ts>
            <ts e="T154" id="Seg_1138" n="sc" s="T149">
               <ts e="T150" id="Seg_1140" n="e" s="T149">((BRK)) </ts>
               <ts e="T151" id="Seg_1142" n="e" s="T150">(-narlaʔbə), </ts>
               <ts e="T152" id="Seg_1144" n="e" s="T151">kamen </ts>
               <ts e="T153" id="Seg_1146" n="e" s="T152">dĭzeŋ </ts>
               <ts e="T239" id="Seg_1148" n="e" s="T153">kalla </ts>
               <ts e="T154" id="Seg_1150" n="e" s="T239">dʼürləʔi. </ts>
            </ts>
            <ts e="T162" id="Seg_1151" n="sc" s="T155">
               <ts e="T156" id="Seg_1153" n="e" s="T155">A </ts>
               <ts e="T157" id="Seg_1155" n="e" s="T156">măn </ts>
               <ts e="T158" id="Seg_1157" n="e" s="T157">ej </ts>
               <ts e="T159" id="Seg_1159" n="e" s="T158">tĭmnem, </ts>
               <ts e="T160" id="Seg_1161" n="e" s="T159">kamen </ts>
               <ts e="T161" id="Seg_1163" n="e" s="T160">(dʼ-) </ts>
               <ts e="T240" id="Seg_1165" n="e" s="T161">kalla </ts>
               <ts e="T162" id="Seg_1167" n="e" s="T240">dʼürləʔjə. </ts>
            </ts>
            <ts e="T167" id="Seg_1168" n="sc" s="T163">
               <ts e="T164" id="Seg_1170" n="e" s="T163">Kanaʔ, </ts>
               <ts e="T165" id="Seg_1172" n="e" s="T164">suraraʔ, </ts>
               <ts e="T166" id="Seg_1174" n="e" s="T165">kamen </ts>
               <ts e="T241" id="Seg_1176" n="e" s="T166">kalla </ts>
               <ts e="T167" id="Seg_1178" n="e" s="T241">dʼürləʔjə. </ts>
            </ts>
            <ts e="T172" id="Seg_1179" n="sc" s="T168">
               <ts e="T169" id="Seg_1181" n="e" s="T168">Dĭgəttə </ts>
               <ts e="T170" id="Seg_1183" n="e" s="T169">dĭ </ts>
               <ts e="T171" id="Seg_1185" n="e" s="T170">ej </ts>
               <ts e="T172" id="Seg_1187" n="e" s="T171">kambi. </ts>
            </ts>
            <ts e="T174" id="Seg_1188" n="sc" s="T173">
               <ts e="T174" id="Seg_1190" n="e" s="T173">((BRK)). </ts>
            </ts>
            <ts e="T183" id="Seg_1191" n="sc" s="T175">
               <ts e="T176" id="Seg_1193" n="e" s="T175">(Mănavʼitʼuʔšiʔ) </ts>
               <ts e="T177" id="Seg_1195" n="e" s="T176">ĭmbidə </ts>
               <ts e="T178" id="Seg_1197" n="e" s="T177">ej </ts>
               <ts e="T179" id="Seg_1199" n="e" s="T178">nörbəliaʔi, </ts>
               <ts e="T180" id="Seg_1201" n="e" s="T179">a </ts>
               <ts e="T181" id="Seg_1203" n="e" s="T180">măn </ts>
               <ts e="T182" id="Seg_1205" n="e" s="T181">ulum </ts>
               <ts e="T183" id="Seg_1207" n="e" s="T182">urgo. </ts>
            </ts>
            <ts e="T191" id="Seg_1208" n="sc" s="T184">
               <ts e="T185" id="Seg_1210" n="e" s="T184">Bar </ts>
               <ts e="T186" id="Seg_1212" n="e" s="T185">(tăn </ts>
               <ts e="T187" id="Seg_1214" n="e" s="T186">öʔlüʔpiel=) </ts>
               <ts e="T188" id="Seg_1216" n="e" s="T187">tăn </ts>
               <ts e="T189" id="Seg_1218" n="e" s="T188">(öʔlüʔpiem) </ts>
               <ts e="T190" id="Seg_1220" n="e" s="T189">ĭmbi </ts>
               <ts e="T191" id="Seg_1222" n="e" s="T190">dʼăbaktərzittə. </ts>
            </ts>
            <ts e="T193" id="Seg_1223" n="sc" s="T192">
               <ts e="T193" id="Seg_1225" n="e" s="T192">((BRK)). </ts>
            </ts>
            <ts e="T199" id="Seg_1226" n="sc" s="T194">
               <ts e="T195" id="Seg_1228" n="e" s="T194">Măn </ts>
               <ts e="T196" id="Seg_1230" n="e" s="T195">ugandə </ts>
               <ts e="T197" id="Seg_1232" n="e" s="T196">kələʔsəbi </ts>
               <ts e="T198" id="Seg_1234" n="e" s="T197">kuza </ts>
               <ts e="T199" id="Seg_1236" n="e" s="T198">igem. </ts>
            </ts>
            <ts e="T207" id="Seg_1237" n="sc" s="T200">
               <ts e="T201" id="Seg_1239" n="e" s="T200">((BRK)) </ts>
               <ts e="T202" id="Seg_1241" n="e" s="T201">măn </ts>
               <ts e="T203" id="Seg_1243" n="e" s="T202">šiʔnʼileʔ </ts>
               <ts e="T204" id="Seg_1245" n="e" s="T203">surarlam, </ts>
               <ts e="T205" id="Seg_1247" n="e" s="T204">kamen </ts>
               <ts e="T206" id="Seg_1249" n="e" s="T205">maʔnʼi </ts>
               <ts e="T207" id="Seg_1251" n="e" s="T206">kaləlaʔ. </ts>
            </ts>
            <ts e="T209" id="Seg_1252" n="sc" s="T208">
               <ts e="T209" id="Seg_1254" n="e" s="T208">((BRK)). </ts>
            </ts>
            <ts e="T217" id="Seg_1255" n="sc" s="T210">
               <ts e="T211" id="Seg_1257" n="e" s="T210">Lem </ts>
               <ts e="T212" id="Seg_1259" n="e" s="T211">pa </ts>
               <ts e="T213" id="Seg_1261" n="e" s="T212">üdʼüge </ts>
               <ts e="T214" id="Seg_1263" n="e" s="T213">ibi, </ts>
               <ts e="T215" id="Seg_1265" n="e" s="T214">dĭgəttə </ts>
               <ts e="T216" id="Seg_1267" n="e" s="T215">özerbi, </ts>
               <ts e="T217" id="Seg_1269" n="e" s="T216">özerbi. </ts>
            </ts>
            <ts e="T222" id="Seg_1270" n="sc" s="T218">
               <ts e="T219" id="Seg_1272" n="e" s="T218">Ugandə </ts>
               <ts e="T220" id="Seg_1274" n="e" s="T219">(nʼiš-) </ts>
               <ts e="T221" id="Seg_1276" n="e" s="T220">nʼispək </ts>
               <ts e="T222" id="Seg_1278" n="e" s="T221">molambi. </ts>
            </ts>
            <ts e="T227" id="Seg_1279" n="sc" s="T223">
               <ts e="T224" id="Seg_1281" n="e" s="T223">Dĭn </ts>
               <ts e="T225" id="Seg_1283" n="e" s="T224">tondə </ts>
               <ts e="T226" id="Seg_1285" n="e" s="T225">možna </ts>
               <ts e="T227" id="Seg_1287" n="e" s="T226">šaʔsʼittə. </ts>
            </ts>
            <ts e="T235" id="Seg_1288" n="sc" s="T228">
               <ts e="T229" id="Seg_1290" n="e" s="T228">I </ts>
               <ts e="T230" id="Seg_1292" n="e" s="T229">surno </ts>
               <ts e="T231" id="Seg_1294" n="e" s="T230">ej </ts>
               <ts e="T232" id="Seg_1296" n="e" s="T231">(bĭzəjdlʼit-) </ts>
               <ts e="T233" id="Seg_1298" n="e" s="T232">ej </ts>
               <ts e="T234" id="Seg_1300" n="e" s="T233">bĭzəjdləl </ts>
               <ts e="T235" id="Seg_1302" n="e" s="T234">tănan. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1303" s="T1">PKZ_196X_SU0220_rest1_misc.001 (001)</ta>
            <ta e="T8" id="Seg_1304" s="T6">PKZ_196X_SU0220_rest1_misc.002 (002)</ta>
            <ta e="T14" id="Seg_1305" s="T9">PKZ_196X_SU0220_rest1_misc.003 (003)</ta>
            <ta e="T17" id="Seg_1306" s="T14">PKZ_196X_SU0220_rest1_misc.004 (004)</ta>
            <ta e="T20" id="Seg_1307" s="T18">PKZ_196X_SU0220_rest1_misc.005 (005)</ta>
            <ta e="T24" id="Seg_1308" s="T21">PKZ_196X_SU0220_rest1_misc.006 (006)</ta>
            <ta e="T29" id="Seg_1309" s="T25">PKZ_196X_SU0220_rest1_misc.007 (007)</ta>
            <ta e="T35" id="Seg_1310" s="T30">PKZ_196X_SU0220_rest1_misc.008 (008)</ta>
            <ta e="T41" id="Seg_1311" s="T36">PKZ_196X_SU0220_rest1_misc.009 (009)</ta>
            <ta e="T53" id="Seg_1312" s="T42">PKZ_196X_SU0220_rest1_misc.010 (010)</ta>
            <ta e="T55" id="Seg_1313" s="T54">PKZ_196X_SU0220_rest1_misc.011 (011)</ta>
            <ta e="T61" id="Seg_1314" s="T56">PKZ_196X_SU0220_rest1_misc.012 (012)</ta>
            <ta e="T70" id="Seg_1315" s="T62">PKZ_196X_SU0220_rest1_misc.013 (013)</ta>
            <ta e="T76" id="Seg_1316" s="T71">PKZ_196X_SU0220_rest1_misc.014 (014)</ta>
            <ta e="T81" id="Seg_1317" s="T76">PKZ_196X_SU0220_rest1_misc.015 (015)</ta>
            <ta e="T86" id="Seg_1318" s="T82">PKZ_196X_SU0220_rest1_misc.016 (016)</ta>
            <ta e="T88" id="Seg_1319" s="T87">PKZ_196X_SU0220_rest1_misc.017 (017)</ta>
            <ta e="T98" id="Seg_1320" s="T89">PKZ_196X_SU0220_rest1_misc.018 (018)</ta>
            <ta e="T103" id="Seg_1321" s="T99">PKZ_196X_SU0220_rest1_misc.019 (019)</ta>
            <ta e="T108" id="Seg_1322" s="T103">PKZ_196X_SU0220_rest1_misc.020 (020)</ta>
            <ta e="T115" id="Seg_1323" s="T109">PKZ_196X_SU0220_rest1_misc.021 (021)</ta>
            <ta e="T120" id="Seg_1324" s="T116">PKZ_196X_SU0220_rest1_misc.022 (022)</ta>
            <ta e="T124" id="Seg_1325" s="T121">PKZ_196X_SU0220_rest1_misc.023 (023)</ta>
            <ta e="T130" id="Seg_1326" s="T125">PKZ_196X_SU0220_rest1_misc.024 (024)</ta>
            <ta e="T137" id="Seg_1327" s="T131">PKZ_196X_SU0220_rest1_misc.025 (025)</ta>
            <ta e="T141" id="Seg_1328" s="T138">PKZ_196X_SU0220_rest1_misc.026 (026)</ta>
            <ta e="T146" id="Seg_1329" s="T142">PKZ_196X_SU0220_rest1_misc.027 (027)</ta>
            <ta e="T148" id="Seg_1330" s="T147">PKZ_196X_SU0220_rest1_misc.028 (028)</ta>
            <ta e="T154" id="Seg_1331" s="T149">PKZ_196X_SU0220_rest1_misc.029 (029)</ta>
            <ta e="T162" id="Seg_1332" s="T155">PKZ_196X_SU0220_rest1_misc.030 (030)</ta>
            <ta e="T167" id="Seg_1333" s="T163">PKZ_196X_SU0220_rest1_misc.031 (031)</ta>
            <ta e="T172" id="Seg_1334" s="T168">PKZ_196X_SU0220_rest1_misc.032 (032)</ta>
            <ta e="T174" id="Seg_1335" s="T173">PKZ_196X_SU0220_rest1_misc.033 (033)</ta>
            <ta e="T183" id="Seg_1336" s="T175">PKZ_196X_SU0220_rest1_misc.034 (034)</ta>
            <ta e="T191" id="Seg_1337" s="T184">PKZ_196X_SU0220_rest1_misc.035 (035)</ta>
            <ta e="T193" id="Seg_1338" s="T192">PKZ_196X_SU0220_rest1_misc.036 (036)</ta>
            <ta e="T199" id="Seg_1339" s="T194">PKZ_196X_SU0220_rest1_misc.037 (037)</ta>
            <ta e="T207" id="Seg_1340" s="T200">PKZ_196X_SU0220_rest1_misc.038 (038)</ta>
            <ta e="T209" id="Seg_1341" s="T208">PKZ_196X_SU0220_rest1_misc.039 (039)</ta>
            <ta e="T217" id="Seg_1342" s="T210">PKZ_196X_SU0220_rest1_misc.040 (040)</ta>
            <ta e="T222" id="Seg_1343" s="T218">PKZ_196X_SU0220_rest1_misc.041 (041)</ta>
            <ta e="T227" id="Seg_1344" s="T223">PKZ_196X_SU0220_rest1_misc.042 (042)</ta>
            <ta e="T235" id="Seg_1345" s="T228">PKZ_196X_SU0220_rest1_misc.043 (043)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_1346" s="T1">Šiʔ kalla dʼürbileʔ, măn oʔbdəbiam bar. </ta>
            <ta e="T8" id="Seg_1347" s="T6">Kambiam nükenə. </ta>
            <ta e="T14" id="Seg_1348" s="T9">Dĭ bar măna perluʔpi bar. </ta>
            <ta e="T17" id="Seg_1349" s="T14">Šide ădʼejală ibiʔi. </ta>
            <ta e="T20" id="Seg_1350" s="T18">Oldʼa ibiʔi. </ta>
            <ta e="T24" id="Seg_1351" s="T21">Mašina ibiʔi, šödörizittə. </ta>
            <ta e="T29" id="Seg_1352" s="T25">Iʔgö ibiʔi, esseŋgəndə ibiʔi. </ta>
            <ta e="T35" id="Seg_1353" s="T30">Dĭgəttə sʼel ibiʔi, bʼeʔ kilo. </ta>
            <ta e="T41" id="Seg_1354" s="T36">Šide šalkovej pʼel onʼiʔ kilo. </ta>
            <ta e="T53" id="Seg_1355" s="T42">(Na- na-) nagur sotnʼə (mĭbiʔi) mĭbi dĭzeŋdə predsedatelʼ, iʔgö aktʼa ibiʔi. </ta>
            <ta e="T55" id="Seg_1356" s="T54">((BRK)). </ta>
            <ta e="T61" id="Seg_1357" s="T56">Măn kundʼo dĭn (m-) amnobiam. </ta>
            <ta e="T70" id="Seg_1358" s="T62">Dĭn unučkat bar södörbi fartuk, diʔnə dĭ nükenə. </ta>
            <ta e="T76" id="Seg_1359" s="T71">Dĭgəttə amnobiam, amnobiam, kunolzittə axota. </ta>
            <ta e="T81" id="Seg_1360" s="T76">(Măndəm) Kalam maʔnʼi, kunolzittə nada. </ta>
            <ta e="T86" id="Seg_1361" s="T82">Dĭgəttə šobiam, iʔbəbiem kunolzittə. </ta>
            <ta e="T88" id="Seg_1362" s="T87">((BRK)). </ta>
            <ta e="T98" id="Seg_1363" s="T89">Dĭgəttə ertən (s-) uʔbdəbiam, onʼĭʔ tibi šobi măna monoʔkosʼtə. </ta>
            <ta e="T103" id="Seg_1364" s="T99">"Tăm măna kalal tibinə?" </ta>
            <ta e="T108" id="Seg_1365" s="T103">A măn măndəm: "Em kanaʔ". </ta>
            <ta e="T115" id="Seg_1366" s="T109">Măn il ige, mĭŋgeʔi dʼăbaktərzittə nudlaʔ. </ta>
            <ta e="T120" id="Seg_1367" s="T116">Kamen kalla dʼürləʔjə, dĭgəttə kalam. </ta>
            <ta e="T124" id="Seg_1368" s="T121">"A kamen kalla dʼürləʔjə?" </ta>
            <ta e="T130" id="Seg_1369" s="T125">Măn ej tĭmnem, kamən kalla dʼürləʔi. </ta>
            <ta e="T137" id="Seg_1370" s="T131">Dĭ măndə: "Šüdördə măna (per-) părga". </ta>
            <ta e="T141" id="Seg_1371" s="T138">A măna (algaʔndə). </ta>
            <ta e="T146" id="Seg_1372" s="T142">Šüdörzittə (ɨr-) ɨrɨ molambiam. </ta>
            <ta e="T148" id="Seg_1373" s="T147">ɨrɨmniem. </ta>
            <ta e="T154" id="Seg_1374" s="T149">((BRK)) (-narlaʔbə), kamen dĭzeŋ kalla dʼürləʔi. </ta>
            <ta e="T162" id="Seg_1375" s="T155">A măn ej tĭmnem, kamen (dʼ-) kalla dʼürləʔjə. </ta>
            <ta e="T167" id="Seg_1376" s="T163">Kanaʔ, suraraʔ, kamen kalla dʼürləʔjə. </ta>
            <ta e="T172" id="Seg_1377" s="T168">Dĭgəttə dĭ ej kambi. </ta>
            <ta e="T174" id="Seg_1378" s="T173">((BRK)). </ta>
            <ta e="T183" id="Seg_1379" s="T175">(Mănavʼitʼuʔšiʔ) ĭmbidə ej nörbəliaʔi, a măn ulum urgo. </ta>
            <ta e="T191" id="Seg_1380" s="T184">Bar (tăn öʔlüʔpiel=) tăn (öʔlüʔpiem) ĭmbi dʼăbaktərzittə. </ta>
            <ta e="T193" id="Seg_1381" s="T192">((BRK)). </ta>
            <ta e="T199" id="Seg_1382" s="T194">Măn ugandə kələʔsəbi kuza igem. </ta>
            <ta e="T207" id="Seg_1383" s="T200">((BRK)) măn šiʔnʼileʔ surarlam, kamen maʔnʼi kaləlaʔ. </ta>
            <ta e="T209" id="Seg_1384" s="T208">((BRK)). </ta>
            <ta e="T217" id="Seg_1385" s="T210">Lem pa üdʼüge ibi, dĭgəttə özerbi, özerbi. </ta>
            <ta e="T222" id="Seg_1386" s="T218">Ugandə (nʼiš-) nʼispək molambi. </ta>
            <ta e="T227" id="Seg_1387" s="T223">Dĭn tondə možna šaʔsʼittə. </ta>
            <ta e="T235" id="Seg_1388" s="T228">I surno ej (bĭzəjdlʼit-) ej bĭzəjdləl tănan. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1389" s="T1">šiʔ</ta>
            <ta e="T0" id="Seg_1390" s="T2">kal-la</ta>
            <ta e="T3" id="Seg_1391" s="T0">dʼür-bi-leʔ</ta>
            <ta e="T4" id="Seg_1392" s="T3">măn</ta>
            <ta e="T5" id="Seg_1393" s="T4">oʔbdə-bia-m</ta>
            <ta e="T6" id="Seg_1394" s="T5">bar</ta>
            <ta e="T7" id="Seg_1395" s="T6">kam-bia-m</ta>
            <ta e="T8" id="Seg_1396" s="T7">nüke-nə</ta>
            <ta e="T10" id="Seg_1397" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1398" s="T10">bar</ta>
            <ta e="T12" id="Seg_1399" s="T11">măna</ta>
            <ta e="T13" id="Seg_1400" s="T12">per-luʔpi</ta>
            <ta e="T14" id="Seg_1401" s="T13">bar</ta>
            <ta e="T15" id="Seg_1402" s="T14">šide</ta>
            <ta e="T16" id="Seg_1403" s="T15">ădʼejală</ta>
            <ta e="T17" id="Seg_1404" s="T16">i-bi-ʔi</ta>
            <ta e="T19" id="Seg_1405" s="T18">oldʼa</ta>
            <ta e="T20" id="Seg_1406" s="T19">i-bi-ʔi</ta>
            <ta e="T22" id="Seg_1407" s="T21">mašina</ta>
            <ta e="T23" id="Seg_1408" s="T22">i-bi-ʔi</ta>
            <ta e="T24" id="Seg_1409" s="T23">šödör-i-zittə</ta>
            <ta e="T26" id="Seg_1410" s="T25">iʔgö</ta>
            <ta e="T27" id="Seg_1411" s="T26">i-bi-ʔi</ta>
            <ta e="T28" id="Seg_1412" s="T27">es-seŋ-gəndə</ta>
            <ta e="T29" id="Seg_1413" s="T28">i-bi-ʔi</ta>
            <ta e="T31" id="Seg_1414" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_1415" s="T31">sʼel</ta>
            <ta e="T33" id="Seg_1416" s="T32">i-bi-ʔi</ta>
            <ta e="T34" id="Seg_1417" s="T33">bʼeʔ</ta>
            <ta e="T35" id="Seg_1418" s="T34">kilo</ta>
            <ta e="T37" id="Seg_1419" s="T36">šide</ta>
            <ta e="T38" id="Seg_1420" s="T37">šalkovej</ta>
            <ta e="T39" id="Seg_1421" s="T38">pʼel</ta>
            <ta e="T40" id="Seg_1422" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_1423" s="T40">kilo</ta>
            <ta e="T45" id="Seg_1424" s="T44">nagur</ta>
            <ta e="T47" id="Seg_1425" s="T46">mĭ-bi-ʔi</ta>
            <ta e="T48" id="Seg_1426" s="T47">mĭ-bi</ta>
            <ta e="T49" id="Seg_1427" s="T48">dĭ-zeŋ-də</ta>
            <ta e="T50" id="Seg_1428" s="T49">predsedatelʼ</ta>
            <ta e="T51" id="Seg_1429" s="T50">iʔgö</ta>
            <ta e="T52" id="Seg_1430" s="T51">aktʼa</ta>
            <ta e="T53" id="Seg_1431" s="T52">i-bi-ʔi</ta>
            <ta e="T57" id="Seg_1432" s="T56">măn</ta>
            <ta e="T58" id="Seg_1433" s="T57">kundʼo</ta>
            <ta e="T59" id="Seg_1434" s="T58">dĭn</ta>
            <ta e="T61" id="Seg_1435" s="T60">amno-bia-m</ta>
            <ta e="T63" id="Seg_1436" s="T62">dĭn</ta>
            <ta e="T64" id="Seg_1437" s="T63">unučka-t</ta>
            <ta e="T65" id="Seg_1438" s="T64">bar</ta>
            <ta e="T66" id="Seg_1439" s="T65">södör-bi</ta>
            <ta e="T68" id="Seg_1440" s="T67">diʔ-nə</ta>
            <ta e="T69" id="Seg_1441" s="T68">dĭ</ta>
            <ta e="T70" id="Seg_1442" s="T69">nüke-nə</ta>
            <ta e="T72" id="Seg_1443" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_1444" s="T72">amno-bia-m</ta>
            <ta e="T74" id="Seg_1445" s="T73">amno-bia-m</ta>
            <ta e="T75" id="Seg_1446" s="T74">kunol-zittə</ta>
            <ta e="T76" id="Seg_1447" s="T75">axota</ta>
            <ta e="T77" id="Seg_1448" s="T76">măn-də-m</ta>
            <ta e="T78" id="Seg_1449" s="T77">ka-la-m</ta>
            <ta e="T79" id="Seg_1450" s="T78">maʔ-nʼi</ta>
            <ta e="T80" id="Seg_1451" s="T79">kunol-zittə</ta>
            <ta e="T81" id="Seg_1452" s="T80">nada</ta>
            <ta e="T83" id="Seg_1453" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1454" s="T83">šo-bia-m</ta>
            <ta e="T85" id="Seg_1455" s="T84">iʔbə-bie-m</ta>
            <ta e="T86" id="Seg_1456" s="T85">kunol-zittə</ta>
            <ta e="T90" id="Seg_1457" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1458" s="T90">ertə-n</ta>
            <ta e="T93" id="Seg_1459" s="T92">uʔbdə-bia-m</ta>
            <ta e="T94" id="Seg_1460" s="T93">onʼĭʔ</ta>
            <ta e="T95" id="Seg_1461" s="T94">tibi</ta>
            <ta e="T96" id="Seg_1462" s="T95">šo-bi</ta>
            <ta e="T97" id="Seg_1463" s="T96">măna</ta>
            <ta e="T98" id="Seg_1464" s="T97">monoʔko-sʼtə</ta>
            <ta e="T100" id="Seg_1465" s="T99">tăm</ta>
            <ta e="T101" id="Seg_1466" s="T100">măna</ta>
            <ta e="T102" id="Seg_1467" s="T101">ka-la-l</ta>
            <ta e="T103" id="Seg_1468" s="T102">tibi-nə</ta>
            <ta e="T104" id="Seg_1469" s="T103">a</ta>
            <ta e="T105" id="Seg_1470" s="T104">măn</ta>
            <ta e="T106" id="Seg_1471" s="T105">măn-də-m</ta>
            <ta e="T107" id="Seg_1472" s="T106">e-m</ta>
            <ta e="T108" id="Seg_1473" s="T107">kan-a-ʔ</ta>
            <ta e="T110" id="Seg_1474" s="T109">măn</ta>
            <ta e="T111" id="Seg_1475" s="T110">il</ta>
            <ta e="T112" id="Seg_1476" s="T111">i-ge</ta>
            <ta e="T113" id="Seg_1477" s="T112">mĭŋ-ge-ʔi</ta>
            <ta e="T114" id="Seg_1478" s="T113">dʼăbaktər-zittə</ta>
            <ta e="T115" id="Seg_1479" s="T114">nud-laʔ</ta>
            <ta e="T117" id="Seg_1480" s="T116">kamen</ta>
            <ta e="T236" id="Seg_1481" s="T117">kal-la</ta>
            <ta e="T118" id="Seg_1482" s="T236">dʼür-lə-ʔjə</ta>
            <ta e="T119" id="Seg_1483" s="T118">dĭgəttə</ta>
            <ta e="T120" id="Seg_1484" s="T119">ka-la-m</ta>
            <ta e="T122" id="Seg_1485" s="T121">a</ta>
            <ta e="T123" id="Seg_1486" s="T122">kamen</ta>
            <ta e="T237" id="Seg_1487" s="T123">kal-la</ta>
            <ta e="T124" id="Seg_1488" s="T237">dʼür-lə-ʔjə</ta>
            <ta e="T126" id="Seg_1489" s="T125">măn</ta>
            <ta e="T127" id="Seg_1490" s="T126">ej</ta>
            <ta e="T128" id="Seg_1491" s="T127">tĭmne-m</ta>
            <ta e="T129" id="Seg_1492" s="T128">kamən</ta>
            <ta e="T238" id="Seg_1493" s="T129">kal-la</ta>
            <ta e="T130" id="Seg_1494" s="T238">dʼür-lə-ʔi</ta>
            <ta e="T132" id="Seg_1495" s="T131">dĭ</ta>
            <ta e="T133" id="Seg_1496" s="T132">măn-də</ta>
            <ta e="T134" id="Seg_1497" s="T133">šüdör-də</ta>
            <ta e="T135" id="Seg_1498" s="T134">măna</ta>
            <ta e="T137" id="Seg_1499" s="T136">părga</ta>
            <ta e="T139" id="Seg_1500" s="T138">a</ta>
            <ta e="T140" id="Seg_1501" s="T139">măna</ta>
            <ta e="T141" id="Seg_1502" s="T140">algaʔndə</ta>
            <ta e="T143" id="Seg_1503" s="T142">šüdör-zittə</ta>
            <ta e="T145" id="Seg_1504" s="T144">ɨrɨ</ta>
            <ta e="T146" id="Seg_1505" s="T145">mo-lam-bia-m</ta>
            <ta e="T148" id="Seg_1506" s="T147">ɨrɨ-m-nie-m</ta>
            <ta e="T152" id="Seg_1507" s="T151">kamen</ta>
            <ta e="T153" id="Seg_1508" s="T152">dĭ-zeŋ</ta>
            <ta e="T239" id="Seg_1509" s="T153">kal-la</ta>
            <ta e="T154" id="Seg_1510" s="T239">dʼür-lə-ʔi</ta>
            <ta e="T156" id="Seg_1511" s="T155">a</ta>
            <ta e="T157" id="Seg_1512" s="T156">măn</ta>
            <ta e="T158" id="Seg_1513" s="T157">ej</ta>
            <ta e="T159" id="Seg_1514" s="T158">tĭmne-m</ta>
            <ta e="T160" id="Seg_1515" s="T159">kamen</ta>
            <ta e="T240" id="Seg_1516" s="T161">kal-la</ta>
            <ta e="T162" id="Seg_1517" s="T240">dʼür-lə-ʔjə</ta>
            <ta e="T164" id="Seg_1518" s="T163">kan-a-ʔ</ta>
            <ta e="T165" id="Seg_1519" s="T164">surar-ʔ</ta>
            <ta e="T166" id="Seg_1520" s="T165">kamen</ta>
            <ta e="T241" id="Seg_1521" s="T166">kal-la</ta>
            <ta e="T167" id="Seg_1522" s="T241">dʼür-lə-ʔjə</ta>
            <ta e="T169" id="Seg_1523" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_1524" s="T169">dĭ</ta>
            <ta e="T171" id="Seg_1525" s="T170">ej</ta>
            <ta e="T172" id="Seg_1526" s="T171">kam-bi</ta>
            <ta e="T176" id="Seg_1527" s="T175">măn-avʼitʼuʔši-ʔ</ta>
            <ta e="T177" id="Seg_1528" s="T176">ĭmbi=də</ta>
            <ta e="T178" id="Seg_1529" s="T177">ej</ta>
            <ta e="T179" id="Seg_1530" s="T178">nörbə-lia-ʔi</ta>
            <ta e="T180" id="Seg_1531" s="T179">a</ta>
            <ta e="T181" id="Seg_1532" s="T180">măn</ta>
            <ta e="T182" id="Seg_1533" s="T181">ulu-m</ta>
            <ta e="T183" id="Seg_1534" s="T182">urgo</ta>
            <ta e="T185" id="Seg_1535" s="T184">bar</ta>
            <ta e="T186" id="Seg_1536" s="T185">tăn</ta>
            <ta e="T187" id="Seg_1537" s="T186">öʔlüʔ-pie-l</ta>
            <ta e="T188" id="Seg_1538" s="T187">tăn</ta>
            <ta e="T189" id="Seg_1539" s="T188">öʔlüʔ-pie-m</ta>
            <ta e="T190" id="Seg_1540" s="T189">ĭmbi</ta>
            <ta e="T191" id="Seg_1541" s="T190">dʼăbaktər-zittə</ta>
            <ta e="T195" id="Seg_1542" s="T194">măn</ta>
            <ta e="T196" id="Seg_1543" s="T195">ugandə</ta>
            <ta e="T197" id="Seg_1544" s="T196">kələʔ-səbi</ta>
            <ta e="T198" id="Seg_1545" s="T197">kuza</ta>
            <ta e="T199" id="Seg_1546" s="T198">i-ge-m</ta>
            <ta e="T202" id="Seg_1547" s="T201">măn</ta>
            <ta e="T203" id="Seg_1548" s="T202">šiʔnʼileʔ</ta>
            <ta e="T204" id="Seg_1549" s="T203">surar-la-m</ta>
            <ta e="T205" id="Seg_1550" s="T204">kamen</ta>
            <ta e="T206" id="Seg_1551" s="T205">maʔ-nʼi</ta>
            <ta e="T207" id="Seg_1552" s="T206">ka-lə-laʔ</ta>
            <ta e="T211" id="Seg_1553" s="T210">lem</ta>
            <ta e="T212" id="Seg_1554" s="T211">pa</ta>
            <ta e="T213" id="Seg_1555" s="T212">üdʼüge</ta>
            <ta e="T214" id="Seg_1556" s="T213">i-bi</ta>
            <ta e="T215" id="Seg_1557" s="T214">dĭgəttə</ta>
            <ta e="T216" id="Seg_1558" s="T215">özer-bi</ta>
            <ta e="T217" id="Seg_1559" s="T216">özer-bi</ta>
            <ta e="T219" id="Seg_1560" s="T218">ugandə</ta>
            <ta e="T221" id="Seg_1561" s="T220">nʼispək</ta>
            <ta e="T222" id="Seg_1562" s="T221">mo-lam-bi</ta>
            <ta e="T224" id="Seg_1563" s="T223">dĭn</ta>
            <ta e="T225" id="Seg_1564" s="T224">to-ndə</ta>
            <ta e="T226" id="Seg_1565" s="T225">možna</ta>
            <ta e="T227" id="Seg_1566" s="T226">šaʔ-sʼittə</ta>
            <ta e="T229" id="Seg_1567" s="T228">i</ta>
            <ta e="T230" id="Seg_1568" s="T229">surno</ta>
            <ta e="T231" id="Seg_1569" s="T230">ej</ta>
            <ta e="T233" id="Seg_1570" s="T232">ej</ta>
            <ta e="T234" id="Seg_1571" s="T233">bĭzəj-d-lə-l</ta>
            <ta e="T235" id="Seg_1572" s="T234">tănan</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1573" s="T1">šiʔ</ta>
            <ta e="T0" id="Seg_1574" s="T2">kan-lAʔ</ta>
            <ta e="T3" id="Seg_1575" s="T0">tʼür-bi-lAʔ</ta>
            <ta e="T4" id="Seg_1576" s="T3">măn</ta>
            <ta e="T5" id="Seg_1577" s="T4">oʔbdə-bi-m</ta>
            <ta e="T6" id="Seg_1578" s="T5">bar</ta>
            <ta e="T7" id="Seg_1579" s="T6">kan-bi-m</ta>
            <ta e="T8" id="Seg_1580" s="T7">nüke-Tə</ta>
            <ta e="T10" id="Seg_1581" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1582" s="T10">bar</ta>
            <ta e="T12" id="Seg_1583" s="T11">măna</ta>
            <ta e="T13" id="Seg_1584" s="T12">pʼer-luʔbdə</ta>
            <ta e="T14" id="Seg_1585" s="T13">bar</ta>
            <ta e="T15" id="Seg_1586" s="T14">šide</ta>
            <ta e="T16" id="Seg_1587" s="T15">adʼejala</ta>
            <ta e="T17" id="Seg_1588" s="T16">i-bi-jəʔ</ta>
            <ta e="T19" id="Seg_1589" s="T18">oldʼa</ta>
            <ta e="T20" id="Seg_1590" s="T19">i-bi-jəʔ</ta>
            <ta e="T22" id="Seg_1591" s="T21">mašina</ta>
            <ta e="T23" id="Seg_1592" s="T22">i-bi-jəʔ</ta>
            <ta e="T24" id="Seg_1593" s="T23">šödör-ə-zittə</ta>
            <ta e="T26" id="Seg_1594" s="T25">iʔgö</ta>
            <ta e="T27" id="Seg_1595" s="T26">i-bi-jəʔ</ta>
            <ta e="T28" id="Seg_1596" s="T27">ešši-zAŋ-gəndə</ta>
            <ta e="T29" id="Seg_1597" s="T28">i-bi-jəʔ</ta>
            <ta e="T31" id="Seg_1598" s="T30">dĭgəttə</ta>
            <ta e="T32" id="Seg_1599" s="T31">sil</ta>
            <ta e="T33" id="Seg_1600" s="T32">i-bi-jəʔ</ta>
            <ta e="T34" id="Seg_1601" s="T33">biəʔ</ta>
            <ta e="T35" id="Seg_1602" s="T34">kilo</ta>
            <ta e="T37" id="Seg_1603" s="T36">šide</ta>
            <ta e="T38" id="Seg_1604" s="T37">šălkovej</ta>
            <ta e="T39" id="Seg_1605" s="T38">pʼel</ta>
            <ta e="T40" id="Seg_1606" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_1607" s="T40">kilo</ta>
            <ta e="T45" id="Seg_1608" s="T44">nagur</ta>
            <ta e="T47" id="Seg_1609" s="T46">mĭ-bi-jəʔ</ta>
            <ta e="T48" id="Seg_1610" s="T47">mĭ-bi</ta>
            <ta e="T49" id="Seg_1611" s="T48">dĭ-zAŋ-Tə</ta>
            <ta e="T50" id="Seg_1612" s="T49">predsʼedatʼelʼ</ta>
            <ta e="T51" id="Seg_1613" s="T50">iʔgö</ta>
            <ta e="T52" id="Seg_1614" s="T51">aktʼa</ta>
            <ta e="T53" id="Seg_1615" s="T52">i-bi-jəʔ</ta>
            <ta e="T57" id="Seg_1616" s="T56">măn</ta>
            <ta e="T58" id="Seg_1617" s="T57">kondʼo</ta>
            <ta e="T59" id="Seg_1618" s="T58">dĭn</ta>
            <ta e="T61" id="Seg_1619" s="T60">amno-bi-m</ta>
            <ta e="T63" id="Seg_1620" s="T62">dĭn</ta>
            <ta e="T64" id="Seg_1621" s="T63">vnučka-t</ta>
            <ta e="T65" id="Seg_1622" s="T64">bar</ta>
            <ta e="T66" id="Seg_1623" s="T65">šödör-bi</ta>
            <ta e="T68" id="Seg_1624" s="T67">dĭ-Tə</ta>
            <ta e="T69" id="Seg_1625" s="T68">dĭ</ta>
            <ta e="T70" id="Seg_1626" s="T69">nüke-Tə</ta>
            <ta e="T72" id="Seg_1627" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_1628" s="T72">amnə-bi-m</ta>
            <ta e="T74" id="Seg_1629" s="T73">amnə-bi-m</ta>
            <ta e="T75" id="Seg_1630" s="T74">kunol-zittə</ta>
            <ta e="T76" id="Seg_1631" s="T75">axota</ta>
            <ta e="T77" id="Seg_1632" s="T76">măn-ntə-m</ta>
            <ta e="T78" id="Seg_1633" s="T77">kan-lV-m</ta>
            <ta e="T79" id="Seg_1634" s="T78">maʔ-gənʼi</ta>
            <ta e="T80" id="Seg_1635" s="T79">kunol-zittə</ta>
            <ta e="T81" id="Seg_1636" s="T80">nadə</ta>
            <ta e="T83" id="Seg_1637" s="T82">dĭgəttə</ta>
            <ta e="T84" id="Seg_1638" s="T83">šo-bi-m</ta>
            <ta e="T85" id="Seg_1639" s="T84">iʔbə-bi-m</ta>
            <ta e="T86" id="Seg_1640" s="T85">kunol-zittə</ta>
            <ta e="T90" id="Seg_1641" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1642" s="T90">ertə-n</ta>
            <ta e="T93" id="Seg_1643" s="T92">uʔbdə-bi-m</ta>
            <ta e="T94" id="Seg_1644" s="T93">onʼiʔ</ta>
            <ta e="T95" id="Seg_1645" s="T94">tibi</ta>
            <ta e="T96" id="Seg_1646" s="T95">šo-bi</ta>
            <ta e="T97" id="Seg_1647" s="T96">măna</ta>
            <ta e="T98" id="Seg_1648" s="T97">monoʔko-zittə</ta>
            <ta e="T100" id="Seg_1649" s="T99">tăn</ta>
            <ta e="T101" id="Seg_1650" s="T100">măna</ta>
            <ta e="T102" id="Seg_1651" s="T101">kan-lV-l</ta>
            <ta e="T103" id="Seg_1652" s="T102">tibi-Tə</ta>
            <ta e="T104" id="Seg_1653" s="T103">a</ta>
            <ta e="T105" id="Seg_1654" s="T104">măn</ta>
            <ta e="T106" id="Seg_1655" s="T105">măn-ntə-m</ta>
            <ta e="T107" id="Seg_1656" s="T106">e-m</ta>
            <ta e="T108" id="Seg_1657" s="T107">kan-ə-ʔ</ta>
            <ta e="T110" id="Seg_1658" s="T109">măn</ta>
            <ta e="T111" id="Seg_1659" s="T110">il</ta>
            <ta e="T112" id="Seg_1660" s="T111">i-gA</ta>
            <ta e="T113" id="Seg_1661" s="T112">mĭn-gA-jəʔ</ta>
            <ta e="T114" id="Seg_1662" s="T113">tʼăbaktər-zittə</ta>
            <ta e="T115" id="Seg_1663" s="T114">nu-lAʔ</ta>
            <ta e="T117" id="Seg_1664" s="T116">kamən</ta>
            <ta e="T236" id="Seg_1665" s="T117">kan-lAʔ</ta>
            <ta e="T118" id="Seg_1666" s="T236">tʼür-lV-jəʔ</ta>
            <ta e="T119" id="Seg_1667" s="T118">dĭgəttə</ta>
            <ta e="T120" id="Seg_1668" s="T119">kan-lV-m</ta>
            <ta e="T122" id="Seg_1669" s="T121">a</ta>
            <ta e="T123" id="Seg_1670" s="T122">kamən</ta>
            <ta e="T237" id="Seg_1671" s="T123">kan-lAʔ</ta>
            <ta e="T124" id="Seg_1672" s="T237">tʼür-lV-jəʔ</ta>
            <ta e="T126" id="Seg_1673" s="T125">măn</ta>
            <ta e="T127" id="Seg_1674" s="T126">ej</ta>
            <ta e="T128" id="Seg_1675" s="T127">tĭmne-m</ta>
            <ta e="T129" id="Seg_1676" s="T128">kamən</ta>
            <ta e="T238" id="Seg_1677" s="T129">kan-lAʔ</ta>
            <ta e="T130" id="Seg_1678" s="T238">tʼür-lV-jəʔ</ta>
            <ta e="T132" id="Seg_1679" s="T131">dĭ</ta>
            <ta e="T133" id="Seg_1680" s="T132">măn-ntə</ta>
            <ta e="T134" id="Seg_1681" s="T133">šödör-t</ta>
            <ta e="T135" id="Seg_1682" s="T134">măna</ta>
            <ta e="T137" id="Seg_1683" s="T136">parga</ta>
            <ta e="T139" id="Seg_1684" s="T138">a</ta>
            <ta e="T140" id="Seg_1685" s="T139">măna</ta>
            <ta e="T143" id="Seg_1686" s="T142">šödör-zittə</ta>
            <ta e="T145" id="Seg_1687" s="T144">ɨrɨː</ta>
            <ta e="T146" id="Seg_1688" s="T145">mo-laːm-bi-m</ta>
            <ta e="T148" id="Seg_1689" s="T147">ɨrɨː-m-liA-m</ta>
            <ta e="T152" id="Seg_1690" s="T151">kamən</ta>
            <ta e="T153" id="Seg_1691" s="T152">dĭ-zAŋ</ta>
            <ta e="T239" id="Seg_1692" s="T153">kan-lAʔ</ta>
            <ta e="T154" id="Seg_1693" s="T239">tʼür-lV-jəʔ</ta>
            <ta e="T156" id="Seg_1694" s="T155">a</ta>
            <ta e="T157" id="Seg_1695" s="T156">măn</ta>
            <ta e="T158" id="Seg_1696" s="T157">ej</ta>
            <ta e="T159" id="Seg_1697" s="T158">tĭmne-m</ta>
            <ta e="T160" id="Seg_1698" s="T159">kamən</ta>
            <ta e="T240" id="Seg_1699" s="T161">kan-lAʔ</ta>
            <ta e="T162" id="Seg_1700" s="T240">tʼür-lV-jəʔ</ta>
            <ta e="T164" id="Seg_1701" s="T163">kan-ə-ʔ</ta>
            <ta e="T165" id="Seg_1702" s="T164">surar-ʔ</ta>
            <ta e="T166" id="Seg_1703" s="T165">kamən</ta>
            <ta e="T241" id="Seg_1704" s="T166">kan-lAʔ</ta>
            <ta e="T167" id="Seg_1705" s="T241">tʼür-lV-jəʔ</ta>
            <ta e="T169" id="Seg_1706" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_1707" s="T169">dĭ</ta>
            <ta e="T171" id="Seg_1708" s="T170">ej</ta>
            <ta e="T172" id="Seg_1709" s="T171">kan-bi</ta>
            <ta e="T176" id="Seg_1710" s="T175">măn-avʼitʼuʔši-ʔ</ta>
            <ta e="T177" id="Seg_1711" s="T176">ĭmbi=də</ta>
            <ta e="T178" id="Seg_1712" s="T177">ej</ta>
            <ta e="T179" id="Seg_1713" s="T178">nörbə-liA-jəʔ</ta>
            <ta e="T180" id="Seg_1714" s="T179">a</ta>
            <ta e="T181" id="Seg_1715" s="T180">măn</ta>
            <ta e="T182" id="Seg_1716" s="T181">ulu-m</ta>
            <ta e="T183" id="Seg_1717" s="T182">urgo</ta>
            <ta e="T185" id="Seg_1718" s="T184">bar</ta>
            <ta e="T186" id="Seg_1719" s="T185">tăn</ta>
            <ta e="T187" id="Seg_1720" s="T186">öʔlüʔ-bi-l</ta>
            <ta e="T188" id="Seg_1721" s="T187">tăn</ta>
            <ta e="T189" id="Seg_1722" s="T188">öʔlüʔ-bi-m</ta>
            <ta e="T190" id="Seg_1723" s="T189">ĭmbi</ta>
            <ta e="T191" id="Seg_1724" s="T190">tʼăbaktər-zittə</ta>
            <ta e="T195" id="Seg_1725" s="T194">măn</ta>
            <ta e="T196" id="Seg_1726" s="T195">ugaːndə</ta>
            <ta e="T197" id="Seg_1727" s="T196">kələʔ-zəbi</ta>
            <ta e="T198" id="Seg_1728" s="T197">kuza</ta>
            <ta e="T199" id="Seg_1729" s="T198">i-gA-m</ta>
            <ta e="T202" id="Seg_1730" s="T201">măn</ta>
            <ta e="T203" id="Seg_1731" s="T202">šiʔnʼileʔ</ta>
            <ta e="T204" id="Seg_1732" s="T203">surar-lV-m</ta>
            <ta e="T205" id="Seg_1733" s="T204">kamən</ta>
            <ta e="T206" id="Seg_1734" s="T205">maʔ-gənʼi</ta>
            <ta e="T207" id="Seg_1735" s="T206">kan-lV-lAʔ</ta>
            <ta e="T211" id="Seg_1736" s="T210">lem</ta>
            <ta e="T212" id="Seg_1737" s="T211">pa</ta>
            <ta e="T213" id="Seg_1738" s="T212">üdʼüge</ta>
            <ta e="T214" id="Seg_1739" s="T213">i-bi</ta>
            <ta e="T215" id="Seg_1740" s="T214">dĭgəttə</ta>
            <ta e="T216" id="Seg_1741" s="T215">özer-bi</ta>
            <ta e="T217" id="Seg_1742" s="T216">özer-bi</ta>
            <ta e="T219" id="Seg_1743" s="T218">ugaːndə</ta>
            <ta e="T221" id="Seg_1744" s="T220">nʼešpək</ta>
            <ta e="T222" id="Seg_1745" s="T221">mo-laːm-bi</ta>
            <ta e="T224" id="Seg_1746" s="T223">dĭn</ta>
            <ta e="T225" id="Seg_1747" s="T224">toʔ-gəndə</ta>
            <ta e="T226" id="Seg_1748" s="T225">možna</ta>
            <ta e="T227" id="Seg_1749" s="T226">šaʔ-zittə</ta>
            <ta e="T229" id="Seg_1750" s="T228">i</ta>
            <ta e="T230" id="Seg_1751" s="T229">surno</ta>
            <ta e="T231" id="Seg_1752" s="T230">ej</ta>
            <ta e="T233" id="Seg_1753" s="T232">ej</ta>
            <ta e="T234" id="Seg_1754" s="T233">băzəj-də-lV-l</ta>
            <ta e="T235" id="Seg_1755" s="T234">tănan</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1756" s="T1">you.PL.NOM</ta>
            <ta e="T0" id="Seg_1757" s="T2">go-CVB</ta>
            <ta e="T3" id="Seg_1758" s="T0">disappear-PST-2PL</ta>
            <ta e="T4" id="Seg_1759" s="T3">I.NOM</ta>
            <ta e="T5" id="Seg_1760" s="T4">collect-PST-1SG</ta>
            <ta e="T6" id="Seg_1761" s="T5">PTCL</ta>
            <ta e="T7" id="Seg_1762" s="T6">go-PST-1SG</ta>
            <ta e="T8" id="Seg_1763" s="T7">woman-LAT</ta>
            <ta e="T10" id="Seg_1764" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1765" s="T10">PTCL</ta>
            <ta e="T12" id="Seg_1766" s="T11">I.LAT</ta>
            <ta e="T13" id="Seg_1767" s="T12">show-MOM.[3SG]</ta>
            <ta e="T14" id="Seg_1768" s="T13">PTCL</ta>
            <ta e="T15" id="Seg_1769" s="T14">two.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1770" s="T15">blanket.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1771" s="T16">take-PST-3PL</ta>
            <ta e="T19" id="Seg_1772" s="T18">clothing.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1773" s="T19">take-PST-3PL</ta>
            <ta e="T22" id="Seg_1774" s="T21">machine.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1775" s="T22">take-PST-3PL</ta>
            <ta e="T24" id="Seg_1776" s="T23">sew-EP-INF.LAT</ta>
            <ta e="T26" id="Seg_1777" s="T25">many</ta>
            <ta e="T27" id="Seg_1778" s="T26">take-PST-3PL</ta>
            <ta e="T28" id="Seg_1779" s="T27">child-PL-LAT/LOC.3SG</ta>
            <ta e="T29" id="Seg_1780" s="T28">take-PST-3PL</ta>
            <ta e="T31" id="Seg_1781" s="T30">then</ta>
            <ta e="T32" id="Seg_1782" s="T31">fat.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1783" s="T32">take-PST-3PL</ta>
            <ta e="T34" id="Seg_1784" s="T33">ten.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1785" s="T34">kilogramm.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1786" s="T36">two.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1787" s="T37">rouble.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1788" s="T38">half.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1789" s="T39">one.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1790" s="T40">kilogramm.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1791" s="T44">three.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1792" s="T46">give-PST-3PL</ta>
            <ta e="T48" id="Seg_1793" s="T47">give-PST.[3SG]</ta>
            <ta e="T49" id="Seg_1794" s="T48">this-PL-LAT</ta>
            <ta e="T50" id="Seg_1795" s="T49">chairman.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1796" s="T50">many</ta>
            <ta e="T52" id="Seg_1797" s="T51">money.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1798" s="T52">take-PST-3PL</ta>
            <ta e="T57" id="Seg_1799" s="T56">I.NOM</ta>
            <ta e="T58" id="Seg_1800" s="T57">long.time</ta>
            <ta e="T59" id="Seg_1801" s="T58">there</ta>
            <ta e="T61" id="Seg_1802" s="T60">sit-PST-1SG</ta>
            <ta e="T63" id="Seg_1803" s="T62">there</ta>
            <ta e="T64" id="Seg_1804" s="T63">granddaughter-NOM/GEN.3SG</ta>
            <ta e="T65" id="Seg_1805" s="T64">PTCL</ta>
            <ta e="T66" id="Seg_1806" s="T65">sew-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1807" s="T67">this-LAT</ta>
            <ta e="T69" id="Seg_1808" s="T68">this.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1809" s="T69">woman-LAT</ta>
            <ta e="T72" id="Seg_1810" s="T71">then</ta>
            <ta e="T73" id="Seg_1811" s="T72">sit-PST-1SG</ta>
            <ta e="T74" id="Seg_1812" s="T73">sit-PST-1SG</ta>
            <ta e="T75" id="Seg_1813" s="T74">sleep-INF.LAT</ta>
            <ta e="T76" id="Seg_1814" s="T75">one.wants</ta>
            <ta e="T77" id="Seg_1815" s="T76">say-IPFVZ-1SG</ta>
            <ta e="T78" id="Seg_1816" s="T77">go-FUT-1SG</ta>
            <ta e="T79" id="Seg_1817" s="T78">house-LAT/LOC.1SG</ta>
            <ta e="T80" id="Seg_1818" s="T79">sleep-INF.LAT</ta>
            <ta e="T81" id="Seg_1819" s="T80">one.should</ta>
            <ta e="T83" id="Seg_1820" s="T82">then</ta>
            <ta e="T84" id="Seg_1821" s="T83">come-PST-1SG</ta>
            <ta e="T85" id="Seg_1822" s="T84">lie.down-PST-1SG</ta>
            <ta e="T86" id="Seg_1823" s="T85">sleep-INF.LAT</ta>
            <ta e="T90" id="Seg_1824" s="T89">then</ta>
            <ta e="T91" id="Seg_1825" s="T90">morning-GEN</ta>
            <ta e="T93" id="Seg_1826" s="T92">get.up-PST-1SG</ta>
            <ta e="T94" id="Seg_1827" s="T93">one</ta>
            <ta e="T95" id="Seg_1828" s="T94">man.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1829" s="T95">come-PST.[3SG]</ta>
            <ta e="T97" id="Seg_1830" s="T96">I.LAT</ta>
            <ta e="T98" id="Seg_1831" s="T97">marry-INF.LAT</ta>
            <ta e="T100" id="Seg_1832" s="T99">you.NOM</ta>
            <ta e="T101" id="Seg_1833" s="T100">I.LAT</ta>
            <ta e="T102" id="Seg_1834" s="T101">go-FUT-2SG</ta>
            <ta e="T103" id="Seg_1835" s="T102">man-LAT</ta>
            <ta e="T104" id="Seg_1836" s="T103">and</ta>
            <ta e="T105" id="Seg_1837" s="T104">I.NOM</ta>
            <ta e="T106" id="Seg_1838" s="T105">say-IPFVZ-1SG</ta>
            <ta e="T107" id="Seg_1839" s="T106">NEG.AUX-1SG</ta>
            <ta e="T108" id="Seg_1840" s="T107">go-EP-CNG</ta>
            <ta e="T110" id="Seg_1841" s="T109">I.NOM</ta>
            <ta e="T111" id="Seg_1842" s="T110">people.[NOM.SG]</ta>
            <ta e="T112" id="Seg_1843" s="T111">be-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_1844" s="T112">go-PRS-3PL</ta>
            <ta e="T114" id="Seg_1845" s="T113">speak-INF.LAT</ta>
            <ta e="T115" id="Seg_1846" s="T114">stand-CVB</ta>
            <ta e="T117" id="Seg_1847" s="T116">when</ta>
            <ta e="T236" id="Seg_1848" s="T117">go-CVB</ta>
            <ta e="T118" id="Seg_1849" s="T236">disappear-FUT-3PL</ta>
            <ta e="T119" id="Seg_1850" s="T118">then</ta>
            <ta e="T120" id="Seg_1851" s="T119">go-FUT-1SG</ta>
            <ta e="T122" id="Seg_1852" s="T121">and</ta>
            <ta e="T123" id="Seg_1853" s="T122">when</ta>
            <ta e="T237" id="Seg_1854" s="T123">go-CVB</ta>
            <ta e="T124" id="Seg_1855" s="T237">disappear-FUT-3PL</ta>
            <ta e="T126" id="Seg_1856" s="T125">I.NOM</ta>
            <ta e="T127" id="Seg_1857" s="T126">NEG</ta>
            <ta e="T128" id="Seg_1858" s="T127">know-1SG</ta>
            <ta e="T129" id="Seg_1859" s="T128">when</ta>
            <ta e="T238" id="Seg_1860" s="T129">go-CVB</ta>
            <ta e="T130" id="Seg_1861" s="T238">disappear-FUT-3PL</ta>
            <ta e="T132" id="Seg_1862" s="T131">this.[NOM.SG]</ta>
            <ta e="T133" id="Seg_1863" s="T132">say-IPFVZ.[3SG]</ta>
            <ta e="T134" id="Seg_1864" s="T133">sew-IMP.2SG.O</ta>
            <ta e="T135" id="Seg_1865" s="T134">I.LAT</ta>
            <ta e="T137" id="Seg_1866" s="T136">fur.coat.[NOM.SG]</ta>
            <ta e="T139" id="Seg_1867" s="T138">and</ta>
            <ta e="T140" id="Seg_1868" s="T139">I.ACC</ta>
            <ta e="T143" id="Seg_1869" s="T142">sew-INF.LAT</ta>
            <ta e="T145" id="Seg_1870" s="T144">lazy.[NOM.SG]</ta>
            <ta e="T146" id="Seg_1871" s="T145">become-RES-PST-1SG</ta>
            <ta e="T148" id="Seg_1872" s="T147">lazy-FACT-PRS-1SG</ta>
            <ta e="T152" id="Seg_1873" s="T151">when</ta>
            <ta e="T153" id="Seg_1874" s="T152">this-PL</ta>
            <ta e="T239" id="Seg_1875" s="T153">go-CVB</ta>
            <ta e="T154" id="Seg_1876" s="T239">disappear-FUT-3PL</ta>
            <ta e="T156" id="Seg_1877" s="T155">and</ta>
            <ta e="T157" id="Seg_1878" s="T156">I.NOM</ta>
            <ta e="T158" id="Seg_1879" s="T157">NEG</ta>
            <ta e="T159" id="Seg_1880" s="T158">know-1SG</ta>
            <ta e="T160" id="Seg_1881" s="T159">when</ta>
            <ta e="T240" id="Seg_1882" s="T161">go-CVB</ta>
            <ta e="T162" id="Seg_1883" s="T240">disappear-FUT-3PL</ta>
            <ta e="T164" id="Seg_1884" s="T163">go-EP-IMP.2SG</ta>
            <ta e="T165" id="Seg_1885" s="T164">ask-IMP.2SG</ta>
            <ta e="T166" id="Seg_1886" s="T165">when</ta>
            <ta e="T241" id="Seg_1887" s="T166">go-CVB</ta>
            <ta e="T167" id="Seg_1888" s="T241">disappear-FUT-3PL</ta>
            <ta e="T169" id="Seg_1889" s="T168">then</ta>
            <ta e="T170" id="Seg_1890" s="T169">this.[NOM.SG]</ta>
            <ta e="T171" id="Seg_1891" s="T170">NEG</ta>
            <ta e="T172" id="Seg_1892" s="T171">go-PST.[3SG]</ta>
            <ta e="T176" id="Seg_1893" s="T175">say-%%-IMP.2SG</ta>
            <ta e="T177" id="Seg_1894" s="T176">what.[NOM.SG]=INDEF</ta>
            <ta e="T178" id="Seg_1895" s="T177">NEG</ta>
            <ta e="T179" id="Seg_1896" s="T178">tell-PRS-3PL</ta>
            <ta e="T180" id="Seg_1897" s="T179">and</ta>
            <ta e="T181" id="Seg_1898" s="T180">I.NOM</ta>
            <ta e="T182" id="Seg_1899" s="T181">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_1900" s="T182">big.[NOM.SG]</ta>
            <ta e="T185" id="Seg_1901" s="T184">PTCL</ta>
            <ta e="T186" id="Seg_1902" s="T185">you.NOM</ta>
            <ta e="T187" id="Seg_1903" s="T186">%%-PST-2SG</ta>
            <ta e="T188" id="Seg_1904" s="T187">you.NOM</ta>
            <ta e="T189" id="Seg_1905" s="T188">%%-PST-1SG</ta>
            <ta e="T190" id="Seg_1906" s="T189">what.[NOM.SG]</ta>
            <ta e="T191" id="Seg_1907" s="T190">speak-INF.LAT</ta>
            <ta e="T195" id="Seg_1908" s="T194">I.NOM</ta>
            <ta e="T196" id="Seg_1909" s="T195">very</ta>
            <ta e="T197" id="Seg_1910" s="T196">slyness-ADJZ</ta>
            <ta e="T198" id="Seg_1911" s="T197">man.[NOM.SG]</ta>
            <ta e="T199" id="Seg_1912" s="T198">be-PRS-1SG</ta>
            <ta e="T202" id="Seg_1913" s="T201">I.NOM</ta>
            <ta e="T203" id="Seg_1914" s="T202">you.PL.ACC</ta>
            <ta e="T204" id="Seg_1915" s="T203">ask-FUT-1SG</ta>
            <ta e="T205" id="Seg_1916" s="T204">when</ta>
            <ta e="T206" id="Seg_1917" s="T205">tent-LAT/LOC.1SG</ta>
            <ta e="T207" id="Seg_1918" s="T206">go-FUT-2PL</ta>
            <ta e="T211" id="Seg_1919" s="T210">bird.cherry</ta>
            <ta e="T212" id="Seg_1920" s="T211">tree.[NOM.SG]</ta>
            <ta e="T213" id="Seg_1921" s="T212">small.[NOM.SG]</ta>
            <ta e="T214" id="Seg_1922" s="T213">be-PST.[3SG]</ta>
            <ta e="T215" id="Seg_1923" s="T214">then</ta>
            <ta e="T216" id="Seg_1924" s="T215">grow-PST.[3SG]</ta>
            <ta e="T217" id="Seg_1925" s="T216">grow-PST.[3SG]</ta>
            <ta e="T219" id="Seg_1926" s="T218">very</ta>
            <ta e="T221" id="Seg_1927" s="T220">thick</ta>
            <ta e="T222" id="Seg_1928" s="T221">become-RES-PST.[3SG]</ta>
            <ta e="T224" id="Seg_1929" s="T223">there</ta>
            <ta e="T225" id="Seg_1930" s="T224">edge-LAT/LOC.3SG</ta>
            <ta e="T226" id="Seg_1931" s="T225">one.can</ta>
            <ta e="T227" id="Seg_1932" s="T226">hide-INF.LAT</ta>
            <ta e="T229" id="Seg_1933" s="T228">and</ta>
            <ta e="T230" id="Seg_1934" s="T229">rain.[NOM.SG]</ta>
            <ta e="T231" id="Seg_1935" s="T230">NEG</ta>
            <ta e="T233" id="Seg_1936" s="T232">NEG</ta>
            <ta e="T234" id="Seg_1937" s="T233">wash.oneself-TR-FUT-2SG</ta>
            <ta e="T235" id="Seg_1938" s="T234">you.ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1939" s="T1">вы.NOM</ta>
            <ta e="T0" id="Seg_1940" s="T2">пойти-CVB</ta>
            <ta e="T3" id="Seg_1941" s="T0">исчезнуть-PST-2PL</ta>
            <ta e="T4" id="Seg_1942" s="T3">я.NOM</ta>
            <ta e="T5" id="Seg_1943" s="T4">собирать-PST-1SG</ta>
            <ta e="T6" id="Seg_1944" s="T5">PTCL</ta>
            <ta e="T7" id="Seg_1945" s="T6">пойти-PST-1SG</ta>
            <ta e="T8" id="Seg_1946" s="T7">женщина-LAT</ta>
            <ta e="T10" id="Seg_1947" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1948" s="T10">PTCL</ta>
            <ta e="T12" id="Seg_1949" s="T11">я.LAT</ta>
            <ta e="T13" id="Seg_1950" s="T12">показать-MOM.[3SG]</ta>
            <ta e="T14" id="Seg_1951" s="T13">PTCL</ta>
            <ta e="T15" id="Seg_1952" s="T14">два.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1953" s="T15">одеяло.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1954" s="T16">взять-PST-3PL</ta>
            <ta e="T19" id="Seg_1955" s="T18">одежда.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1956" s="T19">взять-PST-3PL</ta>
            <ta e="T22" id="Seg_1957" s="T21">машина.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1958" s="T22">взять-PST-3PL</ta>
            <ta e="T24" id="Seg_1959" s="T23">шить-EP-INF.LAT</ta>
            <ta e="T26" id="Seg_1960" s="T25">много</ta>
            <ta e="T27" id="Seg_1961" s="T26">взять-PST-3PL</ta>
            <ta e="T28" id="Seg_1962" s="T27">ребенок-PL-LAT/LOC.3SG</ta>
            <ta e="T29" id="Seg_1963" s="T28">взять-PST-3PL</ta>
            <ta e="T31" id="Seg_1964" s="T30">тогда</ta>
            <ta e="T32" id="Seg_1965" s="T31">жир.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1966" s="T32">взять-PST-3PL</ta>
            <ta e="T34" id="Seg_1967" s="T33">десять.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1968" s="T34">килограмм.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1969" s="T36">два.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1970" s="T37">рубль.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1971" s="T38">половина.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1972" s="T39">один.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1973" s="T40">килограмм.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1974" s="T44">три.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1975" s="T46">дать-PST-3PL</ta>
            <ta e="T48" id="Seg_1976" s="T47">дать-PST.[3SG]</ta>
            <ta e="T49" id="Seg_1977" s="T48">этот-PL-LAT</ta>
            <ta e="T50" id="Seg_1978" s="T49">председатель.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1979" s="T50">много</ta>
            <ta e="T52" id="Seg_1980" s="T51">деньги.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1981" s="T52">взять-PST-3PL</ta>
            <ta e="T57" id="Seg_1982" s="T56">я.NOM</ta>
            <ta e="T58" id="Seg_1983" s="T57">долго</ta>
            <ta e="T59" id="Seg_1984" s="T58">там</ta>
            <ta e="T61" id="Seg_1985" s="T60">сидеть-PST-1SG</ta>
            <ta e="T63" id="Seg_1986" s="T62">там</ta>
            <ta e="T64" id="Seg_1987" s="T63">внучка-NOM/GEN.3SG</ta>
            <ta e="T65" id="Seg_1988" s="T64">PTCL</ta>
            <ta e="T66" id="Seg_1989" s="T65">шить-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1990" s="T67">этот-LAT</ta>
            <ta e="T69" id="Seg_1991" s="T68">этот.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1992" s="T69">женщина-LAT</ta>
            <ta e="T72" id="Seg_1993" s="T71">тогда</ta>
            <ta e="T73" id="Seg_1994" s="T72">сидеть-PST-1SG</ta>
            <ta e="T74" id="Seg_1995" s="T73">сидеть-PST-1SG</ta>
            <ta e="T75" id="Seg_1996" s="T74">спать-INF.LAT</ta>
            <ta e="T76" id="Seg_1997" s="T75">хочется</ta>
            <ta e="T77" id="Seg_1998" s="T76">сказать-IPFVZ-1SG</ta>
            <ta e="T78" id="Seg_1999" s="T77">пойти-FUT-1SG</ta>
            <ta e="T79" id="Seg_2000" s="T78">дом-LAT/LOC.1SG</ta>
            <ta e="T80" id="Seg_2001" s="T79">спать-INF.LAT</ta>
            <ta e="T81" id="Seg_2002" s="T80">надо</ta>
            <ta e="T83" id="Seg_2003" s="T82">тогда</ta>
            <ta e="T84" id="Seg_2004" s="T83">прийти-PST-1SG</ta>
            <ta e="T85" id="Seg_2005" s="T84">ложиться-PST-1SG</ta>
            <ta e="T86" id="Seg_2006" s="T85">спать-INF.LAT</ta>
            <ta e="T90" id="Seg_2007" s="T89">тогда</ta>
            <ta e="T91" id="Seg_2008" s="T90">утро-GEN</ta>
            <ta e="T93" id="Seg_2009" s="T92">встать-PST-1SG</ta>
            <ta e="T94" id="Seg_2010" s="T93">один</ta>
            <ta e="T95" id="Seg_2011" s="T94">мужчина.[NOM.SG]</ta>
            <ta e="T96" id="Seg_2012" s="T95">прийти-PST.[3SG]</ta>
            <ta e="T97" id="Seg_2013" s="T96">я.LAT</ta>
            <ta e="T98" id="Seg_2014" s="T97">жениться-INF.LAT</ta>
            <ta e="T100" id="Seg_2015" s="T99">ты.NOM</ta>
            <ta e="T101" id="Seg_2016" s="T100">я.LAT</ta>
            <ta e="T102" id="Seg_2017" s="T101">пойти-FUT-2SG</ta>
            <ta e="T103" id="Seg_2018" s="T102">мужчина-LAT</ta>
            <ta e="T104" id="Seg_2019" s="T103">а</ta>
            <ta e="T105" id="Seg_2020" s="T104">я.NOM</ta>
            <ta e="T106" id="Seg_2021" s="T105">сказать-IPFVZ-1SG</ta>
            <ta e="T107" id="Seg_2022" s="T106">NEG.AUX-1SG</ta>
            <ta e="T108" id="Seg_2023" s="T107">пойти-EP-CNG</ta>
            <ta e="T110" id="Seg_2024" s="T109">я.NOM</ta>
            <ta e="T111" id="Seg_2025" s="T110">люди.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2026" s="T111">быть-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_2027" s="T112">идти-PRS-3PL</ta>
            <ta e="T114" id="Seg_2028" s="T113">говорить-INF.LAT</ta>
            <ta e="T115" id="Seg_2029" s="T114">стоять-CVB</ta>
            <ta e="T117" id="Seg_2030" s="T116">когда</ta>
            <ta e="T236" id="Seg_2031" s="T117">пойти-CVB</ta>
            <ta e="T118" id="Seg_2032" s="T236">исчезнуть-FUT-3PL</ta>
            <ta e="T119" id="Seg_2033" s="T118">тогда</ta>
            <ta e="T120" id="Seg_2034" s="T119">пойти-FUT-1SG</ta>
            <ta e="T122" id="Seg_2035" s="T121">а</ta>
            <ta e="T123" id="Seg_2036" s="T122">когда</ta>
            <ta e="T237" id="Seg_2037" s="T123">пойти-CVB</ta>
            <ta e="T124" id="Seg_2038" s="T237">исчезнуть-FUT-3PL</ta>
            <ta e="T126" id="Seg_2039" s="T125">я.NOM</ta>
            <ta e="T127" id="Seg_2040" s="T126">NEG</ta>
            <ta e="T128" id="Seg_2041" s="T127">знать-1SG</ta>
            <ta e="T129" id="Seg_2042" s="T128">когда</ta>
            <ta e="T238" id="Seg_2043" s="T129">пойти-CVB</ta>
            <ta e="T130" id="Seg_2044" s="T238">исчезнуть-FUT-3PL</ta>
            <ta e="T132" id="Seg_2045" s="T131">этот.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2046" s="T132">сказать-IPFVZ.[3SG]</ta>
            <ta e="T134" id="Seg_2047" s="T133">шить-IMP.2SG.O</ta>
            <ta e="T135" id="Seg_2048" s="T134">я.LAT</ta>
            <ta e="T137" id="Seg_2049" s="T136">парка.[NOM.SG]</ta>
            <ta e="T139" id="Seg_2050" s="T138">а</ta>
            <ta e="T140" id="Seg_2051" s="T139">я.ACC</ta>
            <ta e="T143" id="Seg_2052" s="T142">шить-INF.LAT</ta>
            <ta e="T145" id="Seg_2053" s="T144">ленивый.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2054" s="T145">стать-RES-PST-1SG</ta>
            <ta e="T148" id="Seg_2055" s="T147">ленивый-FACT-PRS-1SG</ta>
            <ta e="T152" id="Seg_2056" s="T151">когда</ta>
            <ta e="T153" id="Seg_2057" s="T152">этот-PL</ta>
            <ta e="T239" id="Seg_2058" s="T153">пойти-CVB</ta>
            <ta e="T154" id="Seg_2059" s="T239">исчезнуть-FUT-3PL</ta>
            <ta e="T156" id="Seg_2060" s="T155">а</ta>
            <ta e="T157" id="Seg_2061" s="T156">я.NOM</ta>
            <ta e="T158" id="Seg_2062" s="T157">NEG</ta>
            <ta e="T159" id="Seg_2063" s="T158">знать-1SG</ta>
            <ta e="T160" id="Seg_2064" s="T159">когда</ta>
            <ta e="T240" id="Seg_2065" s="T161">пойти-CVB</ta>
            <ta e="T162" id="Seg_2066" s="T240">исчезнуть-FUT-3PL</ta>
            <ta e="T164" id="Seg_2067" s="T163">пойти-EP-IMP.2SG</ta>
            <ta e="T165" id="Seg_2068" s="T164">спросить-IMP.2SG</ta>
            <ta e="T166" id="Seg_2069" s="T165">когда</ta>
            <ta e="T241" id="Seg_2070" s="T166">пойти-CVB</ta>
            <ta e="T167" id="Seg_2071" s="T241">исчезнуть-FUT-3PL</ta>
            <ta e="T169" id="Seg_2072" s="T168">тогда</ta>
            <ta e="T170" id="Seg_2073" s="T169">этот.[NOM.SG]</ta>
            <ta e="T171" id="Seg_2074" s="T170">NEG</ta>
            <ta e="T172" id="Seg_2075" s="T171">пойти-PST.[3SG]</ta>
            <ta e="T176" id="Seg_2076" s="T175">сказать-%%-IMP.2SG</ta>
            <ta e="T177" id="Seg_2077" s="T176">что.[NOM.SG]=INDEF</ta>
            <ta e="T178" id="Seg_2078" s="T177">NEG</ta>
            <ta e="T179" id="Seg_2079" s="T178">сказать-PRS-3PL</ta>
            <ta e="T180" id="Seg_2080" s="T179">а</ta>
            <ta e="T181" id="Seg_2081" s="T180">я.NOM</ta>
            <ta e="T182" id="Seg_2082" s="T181">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_2083" s="T182">большой.[NOM.SG]</ta>
            <ta e="T185" id="Seg_2084" s="T184">PTCL</ta>
            <ta e="T186" id="Seg_2085" s="T185">ты.NOM</ta>
            <ta e="T187" id="Seg_2086" s="T186">%%-PST-2SG</ta>
            <ta e="T188" id="Seg_2087" s="T187">ты.NOM</ta>
            <ta e="T189" id="Seg_2088" s="T188">%%-PST-1SG</ta>
            <ta e="T190" id="Seg_2089" s="T189">что.[NOM.SG]</ta>
            <ta e="T191" id="Seg_2090" s="T190">говорить-INF.LAT</ta>
            <ta e="T195" id="Seg_2091" s="T194">я.NOM</ta>
            <ta e="T196" id="Seg_2092" s="T195">очень</ta>
            <ta e="T197" id="Seg_2093" s="T196">хитрость-ADJZ</ta>
            <ta e="T198" id="Seg_2094" s="T197">мужчина.[NOM.SG]</ta>
            <ta e="T199" id="Seg_2095" s="T198">быть-PRS-1SG</ta>
            <ta e="T202" id="Seg_2096" s="T201">я.NOM</ta>
            <ta e="T203" id="Seg_2097" s="T202">вы.ACC</ta>
            <ta e="T204" id="Seg_2098" s="T203">спросить-FUT-1SG</ta>
            <ta e="T205" id="Seg_2099" s="T204">когда</ta>
            <ta e="T206" id="Seg_2100" s="T205">чум-LAT/LOC.1SG</ta>
            <ta e="T207" id="Seg_2101" s="T206">пойти-FUT-2PL</ta>
            <ta e="T211" id="Seg_2102" s="T210">черемуха</ta>
            <ta e="T212" id="Seg_2103" s="T211">дерево.[NOM.SG]</ta>
            <ta e="T213" id="Seg_2104" s="T212">маленький.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2105" s="T213">быть-PST.[3SG]</ta>
            <ta e="T215" id="Seg_2106" s="T214">тогда</ta>
            <ta e="T216" id="Seg_2107" s="T215">расти-PST.[3SG]</ta>
            <ta e="T217" id="Seg_2108" s="T216">расти-PST.[3SG]</ta>
            <ta e="T219" id="Seg_2109" s="T218">очень</ta>
            <ta e="T221" id="Seg_2110" s="T220">толстый</ta>
            <ta e="T222" id="Seg_2111" s="T221">стать-RES-PST.[3SG]</ta>
            <ta e="T224" id="Seg_2112" s="T223">там</ta>
            <ta e="T225" id="Seg_2113" s="T224">край-LAT/LOC.3SG</ta>
            <ta e="T226" id="Seg_2114" s="T225">можно</ta>
            <ta e="T227" id="Seg_2115" s="T226">спрятаться-INF.LAT</ta>
            <ta e="T229" id="Seg_2116" s="T228">и</ta>
            <ta e="T230" id="Seg_2117" s="T229">дождь.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2118" s="T230">NEG</ta>
            <ta e="T233" id="Seg_2119" s="T232">NEG</ta>
            <ta e="T234" id="Seg_2120" s="T233">мыться-TR-FUT-2SG</ta>
            <ta e="T235" id="Seg_2121" s="T234">ты.ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2122" s="T1">pers</ta>
            <ta e="T0" id="Seg_2123" s="T2">v-v:n-fin</ta>
            <ta e="T3" id="Seg_2124" s="T0">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2125" s="T3">pers</ta>
            <ta e="T5" id="Seg_2126" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_2127" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_2128" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_2129" s="T7">n-n:case</ta>
            <ta e="T10" id="Seg_2130" s="T9">dempro.[n:case]</ta>
            <ta e="T11" id="Seg_2131" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_2132" s="T11">pers</ta>
            <ta e="T13" id="Seg_2133" s="T12">v-v&gt;v.[v:pn]</ta>
            <ta e="T14" id="Seg_2134" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2135" s="T14">num.[n:case]</ta>
            <ta e="T16" id="Seg_2136" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_2137" s="T16">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_2138" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_2139" s="T19">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_2140" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_2141" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_2142" s="T23">v-v:ins-v:n.fin</ta>
            <ta e="T26" id="Seg_2143" s="T25">quant</ta>
            <ta e="T27" id="Seg_2144" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2145" s="T27">n-n:num-n:case.poss</ta>
            <ta e="T29" id="Seg_2146" s="T28">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_2147" s="T30">adv</ta>
            <ta e="T32" id="Seg_2148" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_2149" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_2150" s="T33">num.[n:case]</ta>
            <ta e="T35" id="Seg_2151" s="T34">n.[n:case]</ta>
            <ta e="T37" id="Seg_2152" s="T36">num.[n:case]</ta>
            <ta e="T38" id="Seg_2153" s="T37">n.[n:case]</ta>
            <ta e="T39" id="Seg_2154" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_2155" s="T39">num.[n:case]</ta>
            <ta e="T41" id="Seg_2156" s="T40">n.[n:case]</ta>
            <ta e="T45" id="Seg_2157" s="T44">num.[n:case]</ta>
            <ta e="T47" id="Seg_2158" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_2159" s="T47">v-v:tense.[v:pn]</ta>
            <ta e="T49" id="Seg_2160" s="T48">dempro-n:num-n:case</ta>
            <ta e="T50" id="Seg_2161" s="T49">n.[n:case]</ta>
            <ta e="T51" id="Seg_2162" s="T50">quant</ta>
            <ta e="T52" id="Seg_2163" s="T51">n.[n:case]</ta>
            <ta e="T53" id="Seg_2164" s="T52">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_2165" s="T56">pers</ta>
            <ta e="T58" id="Seg_2166" s="T57">adv</ta>
            <ta e="T59" id="Seg_2167" s="T58">adv</ta>
            <ta e="T61" id="Seg_2168" s="T60">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_2169" s="T62">adv</ta>
            <ta e="T64" id="Seg_2170" s="T63">n-n:case.poss</ta>
            <ta e="T65" id="Seg_2171" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2172" s="T65">v-v:tense.[v:pn]</ta>
            <ta e="T68" id="Seg_2173" s="T67">dempro-n:case</ta>
            <ta e="T69" id="Seg_2174" s="T68">dempro.[n:case]</ta>
            <ta e="T70" id="Seg_2175" s="T69">n-n:case</ta>
            <ta e="T72" id="Seg_2176" s="T71">adv</ta>
            <ta e="T73" id="Seg_2177" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_2178" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_2179" s="T74">v-v:n.fin</ta>
            <ta e="T76" id="Seg_2180" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_2181" s="T76">v-v&gt;v-v:pn</ta>
            <ta e="T78" id="Seg_2182" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_2183" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_2184" s="T79">v-v:n.fin</ta>
            <ta e="T81" id="Seg_2185" s="T80">ptcl</ta>
            <ta e="T83" id="Seg_2186" s="T82">adv</ta>
            <ta e="T84" id="Seg_2187" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_2188" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_2189" s="T85">v-v:n.fin</ta>
            <ta e="T90" id="Seg_2190" s="T89">adv</ta>
            <ta e="T91" id="Seg_2191" s="T90">n-n:case</ta>
            <ta e="T93" id="Seg_2192" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_2193" s="T93">num</ta>
            <ta e="T95" id="Seg_2194" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_2195" s="T95">v-v:tense.[v:pn]</ta>
            <ta e="T97" id="Seg_2196" s="T96">pers</ta>
            <ta e="T98" id="Seg_2197" s="T97">v-v:n.fin</ta>
            <ta e="T100" id="Seg_2198" s="T99">pers</ta>
            <ta e="T101" id="Seg_2199" s="T100">pers</ta>
            <ta e="T102" id="Seg_2200" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_2201" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2202" s="T103">conj</ta>
            <ta e="T105" id="Seg_2203" s="T104">pers</ta>
            <ta e="T106" id="Seg_2204" s="T105">v-v&gt;v-v:pn</ta>
            <ta e="T107" id="Seg_2205" s="T106">aux-v:pn</ta>
            <ta e="T108" id="Seg_2206" s="T107">v-v:ins-v:mood.pn</ta>
            <ta e="T110" id="Seg_2207" s="T109">pers</ta>
            <ta e="T111" id="Seg_2208" s="T110">n.[n:case]</ta>
            <ta e="T112" id="Seg_2209" s="T111">v-v:tense.[v:pn]</ta>
            <ta e="T113" id="Seg_2210" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_2211" s="T113">v-v:n.fin</ta>
            <ta e="T115" id="Seg_2212" s="T114">v-v:n.fin</ta>
            <ta e="T117" id="Seg_2213" s="T116">que</ta>
            <ta e="T236" id="Seg_2214" s="T117">v-v:n-fin</ta>
            <ta e="T118" id="Seg_2215" s="T236">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2216" s="T118">adv</ta>
            <ta e="T120" id="Seg_2217" s="T119">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2218" s="T121">conj</ta>
            <ta e="T123" id="Seg_2219" s="T122">que</ta>
            <ta e="T237" id="Seg_2220" s="T123">v-v:n-fin</ta>
            <ta e="T124" id="Seg_2221" s="T237">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_2222" s="T125">pers</ta>
            <ta e="T127" id="Seg_2223" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_2224" s="T127">v-v:pn</ta>
            <ta e="T129" id="Seg_2225" s="T128">que</ta>
            <ta e="T238" id="Seg_2226" s="T129">v-v:n-fin</ta>
            <ta e="T130" id="Seg_2227" s="T238">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2228" s="T131">dempro.[n:case]</ta>
            <ta e="T133" id="Seg_2229" s="T132">v-v&gt;v.[v:pn]</ta>
            <ta e="T134" id="Seg_2230" s="T133">v-v:mood.pn</ta>
            <ta e="T135" id="Seg_2231" s="T134">pers</ta>
            <ta e="T137" id="Seg_2232" s="T136">n.[n:case]</ta>
            <ta e="T139" id="Seg_2233" s="T138">conj</ta>
            <ta e="T140" id="Seg_2234" s="T139">pers</ta>
            <ta e="T143" id="Seg_2235" s="T142">v-v:n.fin</ta>
            <ta e="T145" id="Seg_2236" s="T144">adj.[n:case]</ta>
            <ta e="T146" id="Seg_2237" s="T145">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_2238" s="T147">adj-adj&gt;v-v:tense-v:pn</ta>
            <ta e="T152" id="Seg_2239" s="T151">que</ta>
            <ta e="T153" id="Seg_2240" s="T152">dempro-n:num</ta>
            <ta e="T239" id="Seg_2241" s="T153">v-v:n-fin</ta>
            <ta e="T154" id="Seg_2242" s="T239">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_2243" s="T155">conj</ta>
            <ta e="T157" id="Seg_2244" s="T156">pers</ta>
            <ta e="T158" id="Seg_2245" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_2246" s="T158">v-v:pn</ta>
            <ta e="T160" id="Seg_2247" s="T159">que</ta>
            <ta e="T240" id="Seg_2248" s="T161">v-v:n-fin</ta>
            <ta e="T162" id="Seg_2249" s="T240">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2250" s="T163">v-v:ins-v:mood.pn</ta>
            <ta e="T165" id="Seg_2251" s="T164">v-v:mood.pn</ta>
            <ta e="T166" id="Seg_2252" s="T165">que</ta>
            <ta e="T241" id="Seg_2253" s="T166">v-v:n-fin</ta>
            <ta e="T167" id="Seg_2254" s="T241">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_2255" s="T168">adv</ta>
            <ta e="T170" id="Seg_2256" s="T169">dempro.[n:case]</ta>
            <ta e="T171" id="Seg_2257" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_2258" s="T171">v-v:tense.[v:pn]</ta>
            <ta e="T176" id="Seg_2259" s="T175">v-v&gt;v-v:mood.pn</ta>
            <ta e="T177" id="Seg_2260" s="T176">que.[n:case]=ptcl</ta>
            <ta e="T178" id="Seg_2261" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2262" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_2263" s="T179">conj</ta>
            <ta e="T181" id="Seg_2264" s="T180">pers</ta>
            <ta e="T182" id="Seg_2265" s="T181">n-n:case.poss</ta>
            <ta e="T183" id="Seg_2266" s="T182">adj.[n:case]</ta>
            <ta e="T185" id="Seg_2267" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_2268" s="T185">pers</ta>
            <ta e="T187" id="Seg_2269" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_2270" s="T187">pers</ta>
            <ta e="T189" id="Seg_2271" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_2272" s="T189">que.[n:case]</ta>
            <ta e="T191" id="Seg_2273" s="T190">v-v:n.fin</ta>
            <ta e="T195" id="Seg_2274" s="T194">pers</ta>
            <ta e="T196" id="Seg_2275" s="T195">adv</ta>
            <ta e="T197" id="Seg_2276" s="T196">n-n&gt;adj</ta>
            <ta e="T198" id="Seg_2277" s="T197">n.[n:case]</ta>
            <ta e="T199" id="Seg_2278" s="T198">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_2279" s="T201">pers</ta>
            <ta e="T203" id="Seg_2280" s="T202">pers</ta>
            <ta e="T204" id="Seg_2281" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_2282" s="T204">que</ta>
            <ta e="T206" id="Seg_2283" s="T205">n-n:case.poss</ta>
            <ta e="T207" id="Seg_2284" s="T206">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2285" s="T210">n</ta>
            <ta e="T212" id="Seg_2286" s="T211">n.[n:case]</ta>
            <ta e="T213" id="Seg_2287" s="T212">adj.[n:case]</ta>
            <ta e="T214" id="Seg_2288" s="T213">v-v:tense.[v:pn]</ta>
            <ta e="T215" id="Seg_2289" s="T214">adv</ta>
            <ta e="T216" id="Seg_2290" s="T215">v-v:tense.[v:pn]</ta>
            <ta e="T217" id="Seg_2291" s="T216">v-v:tense.[v:pn]</ta>
            <ta e="T219" id="Seg_2292" s="T218">adv</ta>
            <ta e="T221" id="Seg_2293" s="T220">adj</ta>
            <ta e="T222" id="Seg_2294" s="T221">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T224" id="Seg_2295" s="T223">adv</ta>
            <ta e="T225" id="Seg_2296" s="T224">n-n:case.poss</ta>
            <ta e="T226" id="Seg_2297" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_2298" s="T226">v-v:n.fin</ta>
            <ta e="T229" id="Seg_2299" s="T228">conj</ta>
            <ta e="T230" id="Seg_2300" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_2301" s="T230">ptcl</ta>
            <ta e="T233" id="Seg_2302" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_2303" s="T233">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_2304" s="T234">pers</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2305" s="T1">pers</ta>
            <ta e="T0" id="Seg_2306" s="T2">v</ta>
            <ta e="T3" id="Seg_2307" s="T0">v</ta>
            <ta e="T4" id="Seg_2308" s="T3">pers</ta>
            <ta e="T5" id="Seg_2309" s="T4">v</ta>
            <ta e="T6" id="Seg_2310" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_2311" s="T6">v</ta>
            <ta e="T8" id="Seg_2312" s="T7">n</ta>
            <ta e="T10" id="Seg_2313" s="T9">dempro</ta>
            <ta e="T11" id="Seg_2314" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_2315" s="T11">pers</ta>
            <ta e="T13" id="Seg_2316" s="T12">v</ta>
            <ta e="T14" id="Seg_2317" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2318" s="T14">num</ta>
            <ta e="T16" id="Seg_2319" s="T15">n</ta>
            <ta e="T17" id="Seg_2320" s="T16">v</ta>
            <ta e="T19" id="Seg_2321" s="T18">n</ta>
            <ta e="T20" id="Seg_2322" s="T19">v</ta>
            <ta e="T22" id="Seg_2323" s="T21">n</ta>
            <ta e="T23" id="Seg_2324" s="T22">v</ta>
            <ta e="T24" id="Seg_2325" s="T23">v</ta>
            <ta e="T26" id="Seg_2326" s="T25">quant</ta>
            <ta e="T27" id="Seg_2327" s="T26">v</ta>
            <ta e="T28" id="Seg_2328" s="T27">n</ta>
            <ta e="T29" id="Seg_2329" s="T28">v</ta>
            <ta e="T31" id="Seg_2330" s="T30">adv</ta>
            <ta e="T32" id="Seg_2331" s="T31">n</ta>
            <ta e="T33" id="Seg_2332" s="T32">v</ta>
            <ta e="T34" id="Seg_2333" s="T33">num</ta>
            <ta e="T35" id="Seg_2334" s="T34">n</ta>
            <ta e="T37" id="Seg_2335" s="T36">num</ta>
            <ta e="T38" id="Seg_2336" s="T37">n</ta>
            <ta e="T39" id="Seg_2337" s="T38">n</ta>
            <ta e="T40" id="Seg_2338" s="T39">num</ta>
            <ta e="T41" id="Seg_2339" s="T40">n</ta>
            <ta e="T45" id="Seg_2340" s="T44">num</ta>
            <ta e="T47" id="Seg_2341" s="T46">v</ta>
            <ta e="T48" id="Seg_2342" s="T47">v</ta>
            <ta e="T49" id="Seg_2343" s="T48">dempro</ta>
            <ta e="T50" id="Seg_2344" s="T49">n</ta>
            <ta e="T51" id="Seg_2345" s="T50">quant</ta>
            <ta e="T52" id="Seg_2346" s="T51">n</ta>
            <ta e="T53" id="Seg_2347" s="T52">v</ta>
            <ta e="T57" id="Seg_2348" s="T56">pers</ta>
            <ta e="T58" id="Seg_2349" s="T57">adv</ta>
            <ta e="T59" id="Seg_2350" s="T58">adv</ta>
            <ta e="T61" id="Seg_2351" s="T60">v</ta>
            <ta e="T63" id="Seg_2352" s="T62">adv</ta>
            <ta e="T64" id="Seg_2353" s="T63">n</ta>
            <ta e="T65" id="Seg_2354" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2355" s="T65">v</ta>
            <ta e="T68" id="Seg_2356" s="T67">dempro</ta>
            <ta e="T69" id="Seg_2357" s="T68">dempro</ta>
            <ta e="T70" id="Seg_2358" s="T69">n</ta>
            <ta e="T72" id="Seg_2359" s="T71">adv</ta>
            <ta e="T73" id="Seg_2360" s="T72">v</ta>
            <ta e="T74" id="Seg_2361" s="T73">v</ta>
            <ta e="T75" id="Seg_2362" s="T74">v</ta>
            <ta e="T76" id="Seg_2363" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_2364" s="T76">v</ta>
            <ta e="T78" id="Seg_2365" s="T77">v</ta>
            <ta e="T79" id="Seg_2366" s="T78">n</ta>
            <ta e="T80" id="Seg_2367" s="T79">v</ta>
            <ta e="T81" id="Seg_2368" s="T80">ptcl</ta>
            <ta e="T83" id="Seg_2369" s="T82">adv</ta>
            <ta e="T84" id="Seg_2370" s="T83">v</ta>
            <ta e="T85" id="Seg_2371" s="T84">v</ta>
            <ta e="T86" id="Seg_2372" s="T85">v</ta>
            <ta e="T90" id="Seg_2373" s="T89">adv</ta>
            <ta e="T91" id="Seg_2374" s="T90">n</ta>
            <ta e="T93" id="Seg_2375" s="T92">v</ta>
            <ta e="T94" id="Seg_2376" s="T93">adj</ta>
            <ta e="T95" id="Seg_2377" s="T94">n</ta>
            <ta e="T96" id="Seg_2378" s="T95">v</ta>
            <ta e="T97" id="Seg_2379" s="T96">pers</ta>
            <ta e="T98" id="Seg_2380" s="T97">v</ta>
            <ta e="T100" id="Seg_2381" s="T99">pers</ta>
            <ta e="T101" id="Seg_2382" s="T100">pers</ta>
            <ta e="T102" id="Seg_2383" s="T101">v</ta>
            <ta e="T103" id="Seg_2384" s="T102">n</ta>
            <ta e="T104" id="Seg_2385" s="T103">conj</ta>
            <ta e="T105" id="Seg_2386" s="T104">pers</ta>
            <ta e="T106" id="Seg_2387" s="T105">v</ta>
            <ta e="T107" id="Seg_2388" s="T106">aux</ta>
            <ta e="T108" id="Seg_2389" s="T107">v</ta>
            <ta e="T110" id="Seg_2390" s="T109">pers</ta>
            <ta e="T111" id="Seg_2391" s="T110">n</ta>
            <ta e="T112" id="Seg_2392" s="T111">v</ta>
            <ta e="T113" id="Seg_2393" s="T112">v</ta>
            <ta e="T114" id="Seg_2394" s="T113">v</ta>
            <ta e="T115" id="Seg_2395" s="T114">v</ta>
            <ta e="T117" id="Seg_2396" s="T116">que</ta>
            <ta e="T236" id="Seg_2397" s="T117">v</ta>
            <ta e="T118" id="Seg_2398" s="T236">v</ta>
            <ta e="T119" id="Seg_2399" s="T118">adv</ta>
            <ta e="T120" id="Seg_2400" s="T119">v</ta>
            <ta e="T122" id="Seg_2401" s="T121">conj</ta>
            <ta e="T123" id="Seg_2402" s="T122">que</ta>
            <ta e="T237" id="Seg_2403" s="T123">v</ta>
            <ta e="T124" id="Seg_2404" s="T237">v</ta>
            <ta e="T126" id="Seg_2405" s="T125">pers</ta>
            <ta e="T127" id="Seg_2406" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_2407" s="T127">v</ta>
            <ta e="T129" id="Seg_2408" s="T128">conj</ta>
            <ta e="T238" id="Seg_2409" s="T129">v</ta>
            <ta e="T130" id="Seg_2410" s="T238">v</ta>
            <ta e="T132" id="Seg_2411" s="T131">dempro</ta>
            <ta e="T133" id="Seg_2412" s="T132">v</ta>
            <ta e="T134" id="Seg_2413" s="T133">v</ta>
            <ta e="T135" id="Seg_2414" s="T134">pers</ta>
            <ta e="T137" id="Seg_2415" s="T136">n</ta>
            <ta e="T139" id="Seg_2416" s="T138">conj</ta>
            <ta e="T140" id="Seg_2417" s="T139">pers</ta>
            <ta e="T143" id="Seg_2418" s="T142">v</ta>
            <ta e="T145" id="Seg_2419" s="T144">adj</ta>
            <ta e="T146" id="Seg_2420" s="T145">v</ta>
            <ta e="T148" id="Seg_2421" s="T147">v</ta>
            <ta e="T152" id="Seg_2422" s="T151">que</ta>
            <ta e="T153" id="Seg_2423" s="T152">dempro</ta>
            <ta e="T239" id="Seg_2424" s="T153">v</ta>
            <ta e="T154" id="Seg_2425" s="T239">v</ta>
            <ta e="T156" id="Seg_2426" s="T155">conj</ta>
            <ta e="T157" id="Seg_2427" s="T156">pers</ta>
            <ta e="T158" id="Seg_2428" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_2429" s="T158">v</ta>
            <ta e="T160" id="Seg_2430" s="T159">que</ta>
            <ta e="T240" id="Seg_2431" s="T161">v</ta>
            <ta e="T162" id="Seg_2432" s="T240">v</ta>
            <ta e="T164" id="Seg_2433" s="T163">v</ta>
            <ta e="T165" id="Seg_2434" s="T164">v</ta>
            <ta e="T166" id="Seg_2435" s="T165">que</ta>
            <ta e="T241" id="Seg_2436" s="T166">v</ta>
            <ta e="T167" id="Seg_2437" s="T241">v</ta>
            <ta e="T169" id="Seg_2438" s="T168">adv</ta>
            <ta e="T170" id="Seg_2439" s="T169">dempro</ta>
            <ta e="T171" id="Seg_2440" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_2441" s="T171">v</ta>
            <ta e="T176" id="Seg_2442" s="T175">v</ta>
            <ta e="T177" id="Seg_2443" s="T176">que</ta>
            <ta e="T178" id="Seg_2444" s="T177">ptcl</ta>
            <ta e="T179" id="Seg_2445" s="T178">v</ta>
            <ta e="T180" id="Seg_2446" s="T179">conj</ta>
            <ta e="T181" id="Seg_2447" s="T180">pers</ta>
            <ta e="T182" id="Seg_2448" s="T181">n</ta>
            <ta e="T183" id="Seg_2449" s="T182">adj</ta>
            <ta e="T185" id="Seg_2450" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_2451" s="T185">pers</ta>
            <ta e="T187" id="Seg_2452" s="T186">v</ta>
            <ta e="T188" id="Seg_2453" s="T187">pers</ta>
            <ta e="T189" id="Seg_2454" s="T188">v</ta>
            <ta e="T190" id="Seg_2455" s="T189">que</ta>
            <ta e="T191" id="Seg_2456" s="T190">v</ta>
            <ta e="T195" id="Seg_2457" s="T194">pers</ta>
            <ta e="T196" id="Seg_2458" s="T195">adv</ta>
            <ta e="T197" id="Seg_2459" s="T196">adj</ta>
            <ta e="T198" id="Seg_2460" s="T197">n</ta>
            <ta e="T199" id="Seg_2461" s="T198">v</ta>
            <ta e="T202" id="Seg_2462" s="T201">pers</ta>
            <ta e="T203" id="Seg_2463" s="T202">pers</ta>
            <ta e="T204" id="Seg_2464" s="T203">v</ta>
            <ta e="T205" id="Seg_2465" s="T204">que</ta>
            <ta e="T206" id="Seg_2466" s="T205">n</ta>
            <ta e="T207" id="Seg_2467" s="T206">v</ta>
            <ta e="T211" id="Seg_2468" s="T210">n</ta>
            <ta e="T212" id="Seg_2469" s="T211">n</ta>
            <ta e="T213" id="Seg_2470" s="T212">adj</ta>
            <ta e="T214" id="Seg_2471" s="T213">v</ta>
            <ta e="T215" id="Seg_2472" s="T214">adv</ta>
            <ta e="T216" id="Seg_2473" s="T215">v</ta>
            <ta e="T217" id="Seg_2474" s="T216">v</ta>
            <ta e="T219" id="Seg_2475" s="T218">adv</ta>
            <ta e="T221" id="Seg_2476" s="T220">adj</ta>
            <ta e="T222" id="Seg_2477" s="T221">v</ta>
            <ta e="T224" id="Seg_2478" s="T223">adv</ta>
            <ta e="T225" id="Seg_2479" s="T224">n</ta>
            <ta e="T226" id="Seg_2480" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_2481" s="T226">v</ta>
            <ta e="T229" id="Seg_2482" s="T228">conj</ta>
            <ta e="T230" id="Seg_2483" s="T229">n</ta>
            <ta e="T231" id="Seg_2484" s="T230">ptcl</ta>
            <ta e="T233" id="Seg_2485" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_2486" s="T233">v</ta>
            <ta e="T235" id="Seg_2487" s="T234">pers</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2488" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_2489" s="T3">pro.h:A</ta>
            <ta e="T7" id="Seg_2490" s="T6">0.1.h:A</ta>
            <ta e="T8" id="Seg_2491" s="T7">np:G</ta>
            <ta e="T10" id="Seg_2492" s="T9">pro.h:A</ta>
            <ta e="T12" id="Seg_2493" s="T11">pro.h:R</ta>
            <ta e="T16" id="Seg_2494" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_2495" s="T16">0.3.h:A</ta>
            <ta e="T19" id="Seg_2496" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_2497" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_2498" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_2499" s="T22">0.3.h:A</ta>
            <ta e="T26" id="Seg_2500" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_2501" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_2502" s="T27">np.h:R</ta>
            <ta e="T29" id="Seg_2503" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_2504" s="T30">adv:Time</ta>
            <ta e="T32" id="Seg_2505" s="T31">np:Th</ta>
            <ta e="T33" id="Seg_2506" s="T32">0.3.h:A</ta>
            <ta e="T45" id="Seg_2507" s="T44">np:Th</ta>
            <ta e="T49" id="Seg_2508" s="T48">pro.h:R</ta>
            <ta e="T50" id="Seg_2509" s="T49">np.h:A</ta>
            <ta e="T52" id="Seg_2510" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_2511" s="T52">0.3.h:A</ta>
            <ta e="T57" id="Seg_2512" s="T56">pro.h:A</ta>
            <ta e="T59" id="Seg_2513" s="T58">adv:L</ta>
            <ta e="T63" id="Seg_2514" s="T62">adv:L</ta>
            <ta e="T64" id="Seg_2515" s="T63">np.h:A</ta>
            <ta e="T68" id="Seg_2516" s="T67">np.h:B</ta>
            <ta e="T70" id="Seg_2517" s="T69">np.h:B</ta>
            <ta e="T72" id="Seg_2518" s="T71">adv:Time</ta>
            <ta e="T73" id="Seg_2519" s="T72">0.1.h:Th</ta>
            <ta e="T74" id="Seg_2520" s="T73">0.1.h:Th</ta>
            <ta e="T77" id="Seg_2521" s="T76">0.1.h:A</ta>
            <ta e="T78" id="Seg_2522" s="T77">0.1.h:A</ta>
            <ta e="T79" id="Seg_2523" s="T78">np:G</ta>
            <ta e="T83" id="Seg_2524" s="T82">adv:Time</ta>
            <ta e="T84" id="Seg_2525" s="T83">0.1.h:A</ta>
            <ta e="T85" id="Seg_2526" s="T84">0.1.h:A</ta>
            <ta e="T90" id="Seg_2527" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_2528" s="T90">n:Time</ta>
            <ta e="T93" id="Seg_2529" s="T92">0.1.h:A</ta>
            <ta e="T95" id="Seg_2530" s="T94">np.h:A</ta>
            <ta e="T97" id="Seg_2531" s="T96">pro.h:Th</ta>
            <ta e="T100" id="Seg_2532" s="T99">pro.h:A</ta>
            <ta e="T101" id="Seg_2533" s="T100">pro.h:Th</ta>
            <ta e="T105" id="Seg_2534" s="T104">pro.h:A</ta>
            <ta e="T107" id="Seg_2535" s="T106">0.1.h:A</ta>
            <ta e="T110" id="Seg_2536" s="T109">pro.h:Poss pro:L</ta>
            <ta e="T111" id="Seg_2537" s="T110">np.h:Th</ta>
            <ta e="T113" id="Seg_2538" s="T112">0.3.h:A</ta>
            <ta e="T118" id="Seg_2539" s="T236">0.3.h:A</ta>
            <ta e="T119" id="Seg_2540" s="T118">adv:Time</ta>
            <ta e="T120" id="Seg_2541" s="T119">0.1.h:A</ta>
            <ta e="T124" id="Seg_2542" s="T237">0.3.h:A</ta>
            <ta e="T126" id="Seg_2543" s="T125">pro.h:A</ta>
            <ta e="T130" id="Seg_2544" s="T238">0.3.h:A</ta>
            <ta e="T132" id="Seg_2545" s="T131">pro.h:A</ta>
            <ta e="T134" id="Seg_2546" s="T133">0.2.h:A</ta>
            <ta e="T135" id="Seg_2547" s="T134">pro.h:B</ta>
            <ta e="T137" id="Seg_2548" s="T136">np:P</ta>
            <ta e="T146" id="Seg_2549" s="T145">0.1.h:P</ta>
            <ta e="T148" id="Seg_2550" s="T147">0.1.h:P</ta>
            <ta e="T153" id="Seg_2551" s="T152">pro.h:A</ta>
            <ta e="T157" id="Seg_2552" s="T156">pro.h:E</ta>
            <ta e="T162" id="Seg_2553" s="T240">0.3.h:A</ta>
            <ta e="T164" id="Seg_2554" s="T163">0.2.h:A</ta>
            <ta e="T165" id="Seg_2555" s="T164">0.2.h:A</ta>
            <ta e="T167" id="Seg_2556" s="T241">0.3.h:A</ta>
            <ta e="T169" id="Seg_2557" s="T168">adv:Time</ta>
            <ta e="T170" id="Seg_2558" s="T169">pro.h:A</ta>
            <ta e="T177" id="Seg_2559" s="T176">pro:Th</ta>
            <ta e="T179" id="Seg_2560" s="T178">0.3.h:A</ta>
            <ta e="T181" id="Seg_2561" s="T180">pro.h:Poss</ta>
            <ta e="T182" id="Seg_2562" s="T181">np:Th</ta>
            <ta e="T190" id="Seg_2563" s="T189">pro:Th</ta>
            <ta e="T195" id="Seg_2564" s="T194">pro.h:Th</ta>
            <ta e="T198" id="Seg_2565" s="T197">np.h:Th</ta>
            <ta e="T202" id="Seg_2566" s="T201">pro.h:A</ta>
            <ta e="T203" id="Seg_2567" s="T202">pro.h:R</ta>
            <ta e="T206" id="Seg_2568" s="T205">np:G</ta>
            <ta e="T207" id="Seg_2569" s="T206">0.2.h:A</ta>
            <ta e="T212" id="Seg_2570" s="T211">np:Th</ta>
            <ta e="T215" id="Seg_2571" s="T214">adv:Time</ta>
            <ta e="T216" id="Seg_2572" s="T215">0.3:P</ta>
            <ta e="T217" id="Seg_2573" s="T216">0.3:P</ta>
            <ta e="T222" id="Seg_2574" s="T221">0.3:P</ta>
            <ta e="T224" id="Seg_2575" s="T223">adv:L</ta>
            <ta e="T225" id="Seg_2576" s="T224">np:L</ta>
            <ta e="T230" id="Seg_2577" s="T229">np:Th</ta>
            <ta e="T235" id="Seg_2578" s="T234">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2579" s="T1">pro.h:S</ta>
            <ta e="T0" id="Seg_2580" s="T2">conv:pred</ta>
            <ta e="T3" id="Seg_2581" s="T0">v:pred</ta>
            <ta e="T4" id="Seg_2582" s="T3">pro.h:S</ta>
            <ta e="T5" id="Seg_2583" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_2584" s="T6">v:pred 0.1.h:S</ta>
            <ta e="T10" id="Seg_2585" s="T9">pro.h:S</ta>
            <ta e="T13" id="Seg_2586" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_2587" s="T15">np:O</ta>
            <ta e="T17" id="Seg_2588" s="T16">v:pred 0.3.h:S</ta>
            <ta e="T19" id="Seg_2589" s="T18">np:O</ta>
            <ta e="T20" id="Seg_2590" s="T19">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_2591" s="T21">np:O</ta>
            <ta e="T23" id="Seg_2592" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_2593" s="T25">np:O</ta>
            <ta e="T27" id="Seg_2594" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T29" id="Seg_2595" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_2596" s="T31">np:O</ta>
            <ta e="T33" id="Seg_2597" s="T32">v:pred 0.3.h:S</ta>
            <ta e="T45" id="Seg_2598" s="T44">np:O</ta>
            <ta e="T48" id="Seg_2599" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_2600" s="T49">np.h:S</ta>
            <ta e="T52" id="Seg_2601" s="T51">np:O</ta>
            <ta e="T53" id="Seg_2602" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_2603" s="T56">pro.h:S</ta>
            <ta e="T61" id="Seg_2604" s="T60">v:pred</ta>
            <ta e="T64" id="Seg_2605" s="T63">np.h:S</ta>
            <ta e="T66" id="Seg_2606" s="T65">v:pred</ta>
            <ta e="T73" id="Seg_2607" s="T72">v:pred 0.1.h:S</ta>
            <ta e="T74" id="Seg_2608" s="T73">v:pred 0.1.h:S</ta>
            <ta e="T76" id="Seg_2609" s="T75">ptcl:pred</ta>
            <ta e="T77" id="Seg_2610" s="T76">v:pred 0.1.h:S</ta>
            <ta e="T78" id="Seg_2611" s="T77">v:pred 0.1.h:S</ta>
            <ta e="T81" id="Seg_2612" s="T80">ptcl:pred</ta>
            <ta e="T84" id="Seg_2613" s="T83">v:pred 0.1.h:S</ta>
            <ta e="T85" id="Seg_2614" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T86" id="Seg_2615" s="T85">s:purp</ta>
            <ta e="T93" id="Seg_2616" s="T92">v:pred 0.1.h:S</ta>
            <ta e="T95" id="Seg_2617" s="T94">np.h:S</ta>
            <ta e="T96" id="Seg_2618" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_2619" s="T97">s:purp</ta>
            <ta e="T100" id="Seg_2620" s="T99">pro.h:S</ta>
            <ta e="T102" id="Seg_2621" s="T101">v:pred</ta>
            <ta e="T105" id="Seg_2622" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_2623" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_2624" s="T106">v:pred 0.1.h:S</ta>
            <ta e="T111" id="Seg_2625" s="T110">np.h:S</ta>
            <ta e="T112" id="Seg_2626" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_2627" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T114" id="Seg_2628" s="T113">s:purp</ta>
            <ta e="T115" id="Seg_2629" s="T114">conv:pred</ta>
            <ta e="T236" id="Seg_2630" s="T117">conv:pred</ta>
            <ta e="T118" id="Seg_2631" s="T236">v:pred 0.3.h:S</ta>
            <ta e="T120" id="Seg_2632" s="T119">v:pred 0.1.h:S</ta>
            <ta e="T237" id="Seg_2633" s="T123">conv:pred</ta>
            <ta e="T124" id="Seg_2634" s="T237">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_2635" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_2636" s="T126">ptcl.neg</ta>
            <ta e="T128" id="Seg_2637" s="T127">v:pred</ta>
            <ta e="T238" id="Seg_2638" s="T129">conv:pred</ta>
            <ta e="T130" id="Seg_2639" s="T238">v:pred 0.3.h:S</ta>
            <ta e="T132" id="Seg_2640" s="T131">pro.h:S</ta>
            <ta e="T133" id="Seg_2641" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_2642" s="T133">v:pred 0.2.h:S</ta>
            <ta e="T137" id="Seg_2643" s="T136">np:O</ta>
            <ta e="T145" id="Seg_2644" s="T144">adj:pred</ta>
            <ta e="T146" id="Seg_2645" s="T145">cop 0.1.h:S</ta>
            <ta e="T148" id="Seg_2646" s="T147">adj:pred 0.1.h:S</ta>
            <ta e="T153" id="Seg_2647" s="T152">pro.h:S</ta>
            <ta e="T239" id="Seg_2648" s="T153">conv:pred</ta>
            <ta e="T154" id="Seg_2649" s="T239">v:pred</ta>
            <ta e="T157" id="Seg_2650" s="T156">pro.h:S</ta>
            <ta e="T158" id="Seg_2651" s="T157">ptcl.neg</ta>
            <ta e="T159" id="Seg_2652" s="T158">v:pred</ta>
            <ta e="T240" id="Seg_2653" s="T161">conv:pred</ta>
            <ta e="T162" id="Seg_2654" s="T240">v:pred 0.3.h:S</ta>
            <ta e="T164" id="Seg_2655" s="T163">v:pred 0.2.h:S</ta>
            <ta e="T165" id="Seg_2656" s="T164">v:pred 0.2.h:S</ta>
            <ta e="T241" id="Seg_2657" s="T166">conv:pred</ta>
            <ta e="T167" id="Seg_2658" s="T241">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_2659" s="T169">pro.h:S</ta>
            <ta e="T171" id="Seg_2660" s="T170">ptcl.neg</ta>
            <ta e="T172" id="Seg_2661" s="T171">v:pred</ta>
            <ta e="T177" id="Seg_2662" s="T176">pro:O</ta>
            <ta e="T178" id="Seg_2663" s="T177">ptcl.neg</ta>
            <ta e="T179" id="Seg_2664" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T182" id="Seg_2665" s="T181">np:S</ta>
            <ta e="T183" id="Seg_2666" s="T182">adj:pred</ta>
            <ta e="T190" id="Seg_2667" s="T189">pro:O</ta>
            <ta e="T195" id="Seg_2668" s="T194">pro.h:S</ta>
            <ta e="T198" id="Seg_2669" s="T197">n:pred</ta>
            <ta e="T199" id="Seg_2670" s="T198">cop</ta>
            <ta e="T202" id="Seg_2671" s="T201">pro.h:S</ta>
            <ta e="T203" id="Seg_2672" s="T202">pro.h:O</ta>
            <ta e="T204" id="Seg_2673" s="T203">v:pred</ta>
            <ta e="T207" id="Seg_2674" s="T206">v:pred 0.2.h:S</ta>
            <ta e="T212" id="Seg_2675" s="T211">np:S</ta>
            <ta e="T213" id="Seg_2676" s="T212">adj:pred</ta>
            <ta e="T214" id="Seg_2677" s="T213">cop</ta>
            <ta e="T216" id="Seg_2678" s="T215">v:pred 0.3:S</ta>
            <ta e="T217" id="Seg_2679" s="T216">v:pred 0.3:S</ta>
            <ta e="T221" id="Seg_2680" s="T220">adj:pred</ta>
            <ta e="T222" id="Seg_2681" s="T221">cop 0.3:S</ta>
            <ta e="T226" id="Seg_2682" s="T225">ptcl:pred</ta>
            <ta e="T230" id="Seg_2683" s="T229">np:S</ta>
            <ta e="T231" id="Seg_2684" s="T230">ptcl.neg</ta>
            <ta e="T233" id="Seg_2685" s="T232">ptcl.neg</ta>
            <ta e="T234" id="Seg_2686" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_2687" s="T234">pro.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_2688" s="T5">TURK:disc</ta>
            <ta e="T11" id="Seg_2689" s="T10">TURK:disc</ta>
            <ta e="T14" id="Seg_2690" s="T13">TURK:disc</ta>
            <ta e="T16" id="Seg_2691" s="T15">RUS:cult</ta>
            <ta e="T22" id="Seg_2692" s="T21">RUS:cult</ta>
            <ta e="T35" id="Seg_2693" s="T34">RUS:cult</ta>
            <ta e="T38" id="Seg_2694" s="T37">RUS:cult</ta>
            <ta e="T41" id="Seg_2695" s="T40">RUS:cult</ta>
            <ta e="T50" id="Seg_2696" s="T49">RUS:cult</ta>
            <ta e="T52" id="Seg_2697" s="T51">TAT:cult</ta>
            <ta e="T64" id="Seg_2698" s="T63">RUS:core</ta>
            <ta e="T65" id="Seg_2699" s="T64">TURK:disc</ta>
            <ta e="T67" id="Seg_2700" s="T66">RUS:cult</ta>
            <ta e="T76" id="Seg_2701" s="T75">RUS:mod</ta>
            <ta e="T81" id="Seg_2702" s="T80">RUS:mod</ta>
            <ta e="T104" id="Seg_2703" s="T103">RUS:gram</ta>
            <ta e="T114" id="Seg_2704" s="T113">%TURK:core</ta>
            <ta e="T122" id="Seg_2705" s="T121">RUS:gram</ta>
            <ta e="T139" id="Seg_2706" s="T138">RUS:gram</ta>
            <ta e="T156" id="Seg_2707" s="T155">RUS:gram</ta>
            <ta e="T177" id="Seg_2708" s="T176">TURK:gram(INDEF)</ta>
            <ta e="T180" id="Seg_2709" s="T179">RUS:gram</ta>
            <ta e="T185" id="Seg_2710" s="T184">TURK:disc</ta>
            <ta e="T191" id="Seg_2711" s="T190">%TURK:core</ta>
            <ta e="T226" id="Seg_2712" s="T225">RUS:mod</ta>
            <ta e="T229" id="Seg_2713" s="T228">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T46" id="Seg_2714" s="T45">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_2715" s="T1">[Когда] вы ушли, я собралась.</ta>
            <ta e="T8" id="Seg_2716" s="T6">Пошла к женщине.</ta>
            <ta e="T14" id="Seg_2717" s="T9">Она мне показала.</ta>
            <ta e="T17" id="Seg_2718" s="T14">Они взяли [=купили?] два одеяла.</ta>
            <ta e="T20" id="Seg_2719" s="T18">Одежду купили.</ta>
            <ta e="T24" id="Seg_2720" s="T21">Машину швейную купили.</ta>
            <ta e="T29" id="Seg_2721" s="T25">Много купили, для детей купили.</ta>
            <ta e="T35" id="Seg_2722" s="T30">Потом жир купили, десять килограмм.</ta>
            <ta e="T41" id="Seg_2723" s="T36">Два рубля с половиной один килограмм.</ta>
            <ta e="T53" id="Seg_2724" s="T42">Три сотни дал им председатель, много денег получили.</ta>
            <ta e="T61" id="Seg_2725" s="T56">Я долго там сидела.</ta>
            <ta e="T70" id="Seg_2726" s="T62">Её внучка шила фартук, для этой женщины.</ta>
            <ta e="T76" id="Seg_2727" s="T71">Я сидела, сидела, спать хочется.</ta>
            <ta e="T81" id="Seg_2728" s="T76">Говорю: «Пойду домой, спать надо».</ta>
            <ta e="T86" id="Seg_2729" s="T82">Потом пришла и легла спать.</ta>
            <ta e="T98" id="Seg_2730" s="T89">Потом утром я встала, один мужчина пришёл на мне жениться [=свататься].</ta>
            <ta e="T103" id="Seg_2731" s="T99">«Ты пойдёшь за меня замуж?»</ta>
            <ta e="T108" id="Seg_2732" s="T103">А я говорю: «Не пойду».</ta>
            <ta e="T115" id="Seg_2733" s="T109">У меня люди, приходят разговаривать, стоят. [?]</ta>
            <ta e="T120" id="Seg_2734" s="T116">Когда они уйдут, я пойду.</ta>
            <ta e="T124" id="Seg_2735" s="T121">«А когда они уйдут?»</ta>
            <ta e="T130" id="Seg_2736" s="T125">«Я не знаю, когда уйдут».</ta>
            <ta e="T137" id="Seg_2737" s="T131">Он говорит: «Сшей мне пальто».</ta>
            <ta e="T141" id="Seg_2738" s="T138">А я (не хочу?).</ta>
            <ta e="T146" id="Seg_2739" s="T142">Мне лень стало шить.</ta>
            <ta e="T148" id="Seg_2740" s="T147">Разленилась.</ta>
            <ta e="T154" id="Seg_2741" s="T149">(…) когда они уйдут.</ta>
            <ta e="T162" id="Seg_2742" s="T155">Я не знаю, когда они уйдут.</ta>
            <ta e="T167" id="Seg_2743" s="T163">Пойди и спроси, когда они уйдут.</ta>
            <ta e="T172" id="Seg_2744" s="T168">Потом он не ушёл.</ta>
            <ta e="T183" id="Seg_2745" s="T175">(?) не говори ничего, а моя голова большая. [?]</ta>
            <ta e="T191" id="Seg_2746" s="T184">(?) что рассказывать.</ta>
            <ta e="T199" id="Seg_2747" s="T194">Я очень умный человек.</ta>
            <ta e="T207" id="Seg_2748" s="T200">Я вас спрошу, когда вы домой пойдёте?</ta>
            <ta e="T217" id="Seg_2749" s="T210">Черёмуха была маленькая, потом росла, посла.</ta>
            <ta e="T222" id="Seg_2750" s="T218">Стала очень толстой.</ta>
            <ta e="T227" id="Seg_2751" s="T223">За ней можно спрятаться.</ta>
            <ta e="T235" id="Seg_2752" s="T228">И дождь [тебя] не намочит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_2753" s="T1">[When] you left, I packed up.</ta>
            <ta e="T8" id="Seg_2754" s="T6">I went to the woman.</ta>
            <ta e="T14" id="Seg_2755" s="T9">She showed me.</ta>
            <ta e="T17" id="Seg_2756" s="T14">They took [=bought?] two blankets.</ta>
            <ta e="T20" id="Seg_2757" s="T18">They bought clothes.</ta>
            <ta e="T24" id="Seg_2758" s="T21">They bought a sewing-machine.</ta>
            <ta e="T29" id="Seg_2759" s="T25">They bought much, they bought [this] for their children.</ta>
            <ta e="T35" id="Seg_2760" s="T30">Then they bought fat, ten kilograms.</ta>
            <ta e="T41" id="Seg_2761" s="T36">2 roubles and a half for one kilogramm.</ta>
            <ta e="T53" id="Seg_2762" s="T42">Three hundreds [roubles] had been given her by the chairman.</ta>
            <ta e="T61" id="Seg_2763" s="T56">I was sitting there for a long time.</ta>
            <ta e="T70" id="Seg_2764" s="T62">Her granddaughter sew this woman an apron.</ta>
            <ta e="T76" id="Seg_2765" s="T71">I was sitting some time, I wanted to sleep.</ta>
            <ta e="T81" id="Seg_2766" s="T76">I say (?): “I'm going home, it's time to sleep.”</ta>
            <ta e="T86" id="Seg_2767" s="T82">Then I came and l went to the bed.</ta>
            <ta e="T98" id="Seg_2768" s="T89">Then in the morning I got up, a man came to ask me in marriage.</ta>
            <ta e="T103" id="Seg_2769" s="T99">"Will you marry me?"</ta>
            <ta e="T108" id="Seg_2770" s="T103">And I say: "I won't."</ta>
            <ta e="T115" id="Seg_2771" s="T109">There are people at my place, they come to speak, stand. [?]</ta>
            <ta e="T120" id="Seg_2772" s="T116">When they leave, I'll go.</ta>
            <ta e="T124" id="Seg_2773" s="T121">"When will they leave?"</ta>
            <ta e="T130" id="Seg_2774" s="T125">I don't know when they will leave.</ta>
            <ta e="T137" id="Seg_2775" s="T131">He says: "Sew me a coat."</ta>
            <ta e="T141" id="Seg_2776" s="T138">But I don't want (?).</ta>
            <ta e="T146" id="Seg_2777" s="T142">I became lazy to sew.</ta>
            <ta e="T148" id="Seg_2778" s="T147">I became lazy.</ta>
            <ta e="T154" id="Seg_2779" s="T149">(…) when they will leave.</ta>
            <ta e="T162" id="Seg_2780" s="T155">I don't know, when they will leave.</ta>
            <ta e="T167" id="Seg_2781" s="T163">Go and ask, when they will leave.</ta>
            <ta e="T172" id="Seg_2782" s="T168">Then he didn't go.</ta>
            <ta e="T183" id="Seg_2783" s="T175">(?) don't tell anything, and my head is big. [?]</ta>
            <ta e="T191" id="Seg_2784" s="T184">(?) what to tell.</ta>
            <ta e="T199" id="Seg_2785" s="T194">I am a very clever person.</ta>
            <ta e="T207" id="Seg_2786" s="T200">I will ask you, when you go home?</ta>
            <ta e="T217" id="Seg_2787" s="T210">The bird-cherry tree was small, then it grew and grew.</ta>
            <ta e="T222" id="Seg_2788" s="T218">It became very thick.</ta>
            <ta e="T227" id="Seg_2789" s="T223">One can hide near it.</ta>
            <ta e="T235" id="Seg_2790" s="T228">And the rain won't wet you.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_2791" s="T1">[Als] du weggegangen bist, habe ich zusammengepackt.</ta>
            <ta e="T8" id="Seg_2792" s="T6">Ich ging zu der Frau.</ta>
            <ta e="T14" id="Seg_2793" s="T9">Sie zeigte mir.</ta>
            <ta e="T17" id="Seg_2794" s="T14">Sie haben zwei Decken genommen [=gekauft?].</ta>
            <ta e="T20" id="Seg_2795" s="T18">Sie haben Kleidung gekauft.</ta>
            <ta e="T24" id="Seg_2796" s="T21">Sie haben eine Nähmaschine gekauft.</ta>
            <ta e="T29" id="Seg_2797" s="T25">Sie haben viel gekauft, sie haben [das] für ihre Kinder gekauft.</ta>
            <ta e="T35" id="Seg_2798" s="T30">Dann haben sie Fett gekauft, zehn Kilogramm.</ta>
            <ta e="T41" id="Seg_2799" s="T36">Zweieinhalb Rubel für ein Kilogramm.</ta>
            <ta e="T53" id="Seg_2800" s="T42">Dreihundert [Rubel] waren ihr vom Vorsitzenden gegeben worden.</ta>
            <ta e="T61" id="Seg_2801" s="T56">Ich saß dort für eine lange Zeit.</ta>
            <ta e="T70" id="Seg_2802" s="T62">Ihre Enkelin nähte dieser Frau eine Schürze.</ta>
            <ta e="T76" id="Seg_2803" s="T71">Ich saß einige Zeit, ich wollte schlafen.</ta>
            <ta e="T81" id="Seg_2804" s="T76">Ich sage (?): "Ich gehe nach Hause, es ist Zeit zu schlafen."</ta>
            <ta e="T86" id="Seg_2805" s="T82">Dann kam ich und ging ins Bett.</ta>
            <ta e="T98" id="Seg_2806" s="T89">Dann am Morgen stand ich auf, ein Mann kam, um um meine Hand anzuhalten.</ta>
            <ta e="T103" id="Seg_2807" s="T99">"Willst du mich heiraten?"</ta>
            <ta e="T108" id="Seg_2808" s="T103">Und ich sage: "Will ich nicht."</ta>
            <ta e="T115" id="Seg_2809" s="T109">Es sind Leute bei mir, sie kommen um zu sprechen, stehen. [?]</ta>
            <ta e="T120" id="Seg_2810" s="T116">Wenn sie weggehen, werde ich gehen.</ta>
            <ta e="T124" id="Seg_2811" s="T121">"Wann werden sie weggehen?"</ta>
            <ta e="T130" id="Seg_2812" s="T125">Ich weiß nicht, wann sie weggehen.</ta>
            <ta e="T137" id="Seg_2813" s="T131">Er sagt: "Näh mir einen Mantel."</ta>
            <ta e="T141" id="Seg_2814" s="T138">Aber ich möchte nicht (?).</ta>
            <ta e="T146" id="Seg_2815" s="T142">Ich bin [zu] faul zum Nähen geworden.</ta>
            <ta e="T148" id="Seg_2816" s="T147">Ich bin faul geworden.</ta>
            <ta e="T154" id="Seg_2817" s="T149">(…) wann werden sie gehen.</ta>
            <ta e="T162" id="Seg_2818" s="T155">Ich weiß nicht, wann sie gehen werden.</ta>
            <ta e="T167" id="Seg_2819" s="T163">Geh und frag, wann sie gehen werden.</ta>
            <ta e="T172" id="Seg_2820" s="T168">Dann ging er nicht.</ta>
            <ta e="T183" id="Seg_2821" s="T175">(?) erzähle nichts, und mein Kopf ist groß. [?]</ta>
            <ta e="T191" id="Seg_2822" s="T184">(?) was zu erzählen.</ta>
            <ta e="T199" id="Seg_2823" s="T194">Ich bin ein sehr schlauer Mensch.</ta>
            <ta e="T207" id="Seg_2824" s="T200">Ich frage dich, wann du nach Hause gehst?</ta>
            <ta e="T217" id="Seg_2825" s="T210">Der Vogelkirschenbaum war klein, dann wuchs und wuchs er.</ta>
            <ta e="T222" id="Seg_2826" s="T218">Er wurde sehr dick.</ta>
            <ta e="T227" id="Seg_2827" s="T223">Man kann sich bei ihm verstecken.</ta>
            <ta e="T235" id="Seg_2828" s="T228">Und der Regen wird dich nicht nass machen.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T115" id="Seg_2829" s="T109">[GVY:] Unclear</ta>
            <ta e="T130" id="Seg_2830" s="T125">[GVY:] kalladʼürləʔi here, but -ʔjə in the previous sentences.</ta>
            <ta e="T183" id="Seg_2831" s="T175">[GVY:] Unclear.</ta>
            <ta e="T217" id="Seg_2832" s="T210">[GVY:] len</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T0" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T236" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T237" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T238" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T239" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T240" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T241" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
