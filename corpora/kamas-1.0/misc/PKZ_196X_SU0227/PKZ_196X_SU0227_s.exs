<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8D75FF91-B00F-C3D8-76FD-4C9898DDFA85">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_SU0227</transcription-name>
         <referenced-file url="PKZ_196X_SU0227.wav" />
         <referenced-file url="PKZ_196X_SU0227.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0227\PKZ_196X_SU0227.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1609</ud-information>
            <ud-information attribute-name="# HIAT:w">932</ud-information>
            <ud-information attribute-name="# e">965</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">38</ud-information>
            <ud-information attribute-name="# HIAT:u">205</ud-information>
            <ud-information attribute-name="# sc">346</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.221" type="appl" />
         <tli id="T2" time="1.041" type="appl" />
         <tli id="T3" time="1.861" type="appl" />
         <tli id="T4" time="2.681" type="appl" />
         <tli id="T5" time="3.1266593008792323" />
         <tli id="T6" time="4.219990058542759" />
         <tli id="T7" time="5.129" type="appl" />
         <tli id="T8" time="5.758" type="appl" />
         <tli id="T9" time="6.466651432522081" />
         <tli id="T10" time="7.079999999999999" type="appl" />
         <tli id="T11" time="7.773" type="appl" />
         <tli id="T12" time="8.466" type="appl" />
         <tli id="T13" time="9.158999999999999" type="appl" />
         <tli id="T14" time="9.852" type="appl" />
         <tli id="T15" time="10.544999999999998" type="appl" />
         <tli id="T16" time="11.238" type="appl" />
         <tli id="T17" time="11.931" type="appl" />
         <tli id="T18" time="13.11225" type="appl" />
         <tli id="T19" time="14.293499999999998" type="appl" />
         <tli id="T20" time="15.474749999999998" type="appl" />
         <tli id="T21" time="16.656" type="appl" />
         <tli id="T22" time="17.555999999999997" type="appl" />
         <tli id="T23" time="18.456" type="appl" />
         <tli id="T24" time="19.356" type="appl" />
         <tli id="T25" time="20.256" type="appl" />
         <tli id="T26" time="20.69" type="appl" />
         <tli id="T27" time="21.388333333333335" type="appl" />
         <tli id="T28" time="22.086666666666666" type="appl" />
         <tli id="T29" time="22.785" type="appl" />
         <tli id="T30" time="23.483333333333334" type="appl" />
         <tli id="T31" time="24.181666666666665" type="appl" />
         <tli id="T32" time="24.88" type="appl" />
         <tli id="T33" time="29.329" type="appl" />
         <tli id="T34" time="30.188454545454547" type="appl" />
         <tli id="T35" time="31.04790909090909" type="appl" />
         <tli id="T36" time="31.907363636363637" type="appl" />
         <tli id="T37" time="32.76681818181818" type="appl" />
         <tli id="T38" time="33.62627272727273" type="appl" />
         <tli id="T39" time="34.485727272727274" type="appl" />
         <tli id="T40" time="35.34518181818182" type="appl" />
         <tli id="T41" time="36.20463636363637" type="appl" />
         <tli id="T42" time="37.06409090909091" type="appl" />
         <tli id="T43" time="37.923545454545454" type="appl" />
         <tli id="T44" time="38.783" type="appl" />
         <tli id="T45" time="39.599000000000004" type="appl" />
         <tli id="T46" time="40.415" type="appl" />
         <tli id="T47" time="41.04333333333333" type="appl" />
         <tli id="T48" time="41.67166666666667" type="appl" />
         <tli id="T49" time="42.3" type="appl" />
         <tli id="T50" time="42.91" type="appl" />
         <tli id="T51" time="43.56957142857143" type="appl" />
         <tli id="T52" time="44.229142857142854" type="appl" />
         <tli id="T53" time="44.888714285714286" type="appl" />
         <tli id="T54" time="45.54828571428571" type="appl" />
         <tli id="T55" time="46.207857142857144" type="appl" />
         <tli id="T56" time="46.867428571428576" type="appl" />
         <tli id="T57" time="47.43988824105889" />
         <tli id="T58" time="49.911" type="appl" />
         <tli id="T59" time="50.6542" type="appl" />
         <tli id="T60" time="51.397400000000005" type="appl" />
         <tli id="T61" time="52.1406" type="appl" />
         <tli id="T62" time="52.8838" type="appl" />
         <tli id="T63" time="53.627" type="appl" />
         <tli id="T64" time="54.34" type="appl" />
         <tli id="T65" time="55.053000000000004" type="appl" />
         <tli id="T66" time="55.766000000000005" type="appl" />
         <tli id="T67" time="56.479" type="appl" />
         <tli id="T68" time="57.192" type="appl" />
         <tli id="T69" time="57.905" type="appl" />
         <tli id="T70" time="58.618" type="appl" />
         <tli id="T71" time="59.331" type="appl" />
         <tli id="T72" time="60.044" type="appl" />
         <tli id="T73" time="60.757" type="appl" />
         <tli id="T74" time="61.47" type="appl" />
         <tli id="T1134" time="61.876194738925776" type="intp" />
         <tli id="T75" time="62.553185970468746" />
         <tli id="T76" time="64.242" type="appl" />
         <tli id="T77" time="65.032" type="appl" />
         <tli id="T78" time="65.822" type="appl" />
         <tli id="T79" time="66.612" type="appl" />
         <tli id="T80" time="67.879" type="appl" />
         <tli id="T81" time="68.49683333333334" type="appl" />
         <tli id="T82" time="69.11466666666666" type="appl" />
         <tli id="T83" time="69.7325" type="appl" />
         <tli id="T84" time="70.35033333333334" type="appl" />
         <tli id="T85" time="70.96816666666666" type="appl" />
         <tli id="T86" time="71.586" type="appl" />
         <tli id="T87" time="72.17" type="appl" />
         <tli id="T88" time="72.88371428571429" type="appl" />
         <tli id="T89" time="73.59742857142857" type="appl" />
         <tli id="T90" time="74.31114285714285" type="appl" />
         <tli id="T91" time="75.02485714285714" type="appl" />
         <tli id="T92" time="75.73857142857143" type="appl" />
         <tli id="T93" time="76.45228571428571" type="appl" />
         <tli id="T94" time="77.166" type="appl" />
         <tli id="T95" time="78.19749999999999" type="appl" />
         <tli id="T96" time="79.229" type="appl" />
         <tli id="T97" time="80.26050000000001" type="appl" />
         <tli id="T98" time="81.292" type="appl" />
         <tli id="T99" time="82.321" type="appl" />
         <tli id="T100" time="83.35000000000001" type="appl" />
         <tli id="T1135" time="83.79100000000001" type="intp" />
         <tli id="T101" time="84.379" type="appl" />
         <tli id="T102" time="85.3935" type="appl" />
         <tli id="T1136" time="85.82828571428571" type="intp" />
         <tli id="T103" time="86.408" type="appl" />
         <tli id="T104" time="86.57312938425952" />
         <tli id="T105" time="87.57400000000001" type="appl" />
         <tli id="T106" time="88.498" type="appl" />
         <tli id="T107" time="89.422" type="appl" />
         <tli id="T108" time="90.346" type="appl" />
         <tli id="T109" time="91.27000000000001" type="appl" />
         <tli id="T110" time="92.194" type="appl" />
         <tli id="T111" time="93.118" type="appl" />
         <tli id="T112" time="94.042" type="appl" />
         <tli id="T113" time="94.96600000000001" type="appl" />
         <tli id="T114" time="96.59977243014943" />
         <tli id="T115" time="97.25" type="appl" />
         <tli id="T116" time="97.90933333333334" type="appl" />
         <tli id="T117" time="98.56866666666667" type="appl" />
         <tli id="T118" time="99.22800000000001" type="appl" />
         <tli id="T119" time="99.88733333333333" type="appl" />
         <tli id="T120" time="100.54666666666667" type="appl" />
         <tli id="T121" time="101.33309461271712" />
         <tli id="T122" time="101.39" type="appl" />
         <tli id="T123" time="102.06475" type="appl" />
         <tli id="T124" time="102.73949999999999" type="appl" />
         <tli id="T125" time="103.41425" type="appl" />
         <tli id="T126" time="104.1464213184123" />
         <tli id="T127" time="105.09066666666666" type="appl" />
         <tli id="T128" time="106.09233333333333" type="appl" />
         <tli id="T129" time="107.094" type="appl" />
         <tli id="T130" time="108.22666666666666" type="appl" />
         <tli id="T131" time="109.35933333333334" type="appl" />
         <tli id="T132" time="110.492" type="appl" />
         <tli id="T133" time="111.58" type="appl" />
         <tli id="T134" time="112.2775" type="appl" />
         <tli id="T135" time="112.975" type="appl" />
         <tli id="T136" time="113.6725" type="appl" />
         <tli id="T137" time="114.37" type="appl" />
         <tli id="T138" time="115.09" type="appl" />
         <tli id="T139" time="115.9884" type="appl" />
         <tli id="T140" time="116.8868" type="appl" />
         <tli id="T141" time="117.7852" type="appl" />
         <tli id="T142" time="118.6836" type="appl" />
         <tli id="T143" time="119.582" type="appl" />
         <tli id="T144" time="120.49499999999999" type="appl" />
         <tli id="T145" time="121.408" type="appl" />
         <tli id="T146" time="122.321" type="appl" />
         <tli id="T147" time="123.234" type="appl" />
         <tli id="T148" time="124.147" type="appl" />
         <tli id="T149" time="125.1730384505511" />
         <tli id="T150" time="125.38" type="appl" />
         <tli id="T151" time="126.09333333333333" type="appl" />
         <tli id="T152" time="126.80666666666666" type="appl" />
         <tli id="T153" time="127.94636525047811" />
         <tli id="T154" time="127.96866988543282" />
         <tli id="T155" time="128.846" type="appl" />
         <tli id="T156" time="129.79" type="appl" />
         <tli id="T157" time="131.029" type="appl" />
         <tli id="T158" time="131.753" type="appl" />
         <tli id="T159" time="132.477" type="appl" />
         <tli id="T160" time="133.201" type="appl" />
         <tli id="T161" time="134.1775" type="appl" />
         <tli id="T162" time="135.154" type="appl" />
         <tli id="T163" time="136.13049999999998" type="appl" />
         <tli id="T164" time="137.107" type="appl" />
         <tli id="T165" time="137.27" type="appl" />
         <tli id="T166" time="138.03857142857143" type="appl" />
         <tli id="T167" time="138.80714285714288" type="appl" />
         <tli id="T168" time="139.5757142857143" type="appl" />
         <tli id="T169" time="140.34428571428572" type="appl" />
         <tli id="T170" time="141.11285714285714" type="appl" />
         <tli id="T171" time="141.8814285714286" type="appl" />
         <tli id="T172" time="142.65" type="appl" />
         <tli id="T173" time="143.21" type="appl" />
         <tli id="T174" time="144.52" type="appl" />
         <tli id="T175" time="144.88" type="appl" />
         <tli id="T176" time="145.569" type="appl" />
         <tli id="T177" time="146.258" type="appl" />
         <tli id="T178" time="146.947" type="appl" />
         <tli id="T179" time="147.636" type="appl" />
         <tli id="T180" time="148.32500000000002" type="appl" />
         <tli id="T181" time="149.014" type="appl" />
         <tli id="T182" time="149.9484285714286" type="appl" />
         <tli id="T183" time="150.88285714285715" type="appl" />
         <tli id="T184" time="151.81728571428573" type="appl" />
         <tli id="T185" time="152.75171428571429" type="appl" />
         <tli id="T186" time="153.68614285714287" type="appl" />
         <tli id="T187" time="154.62057142857142" type="appl" />
         <tli id="T188" time="155.555" type="appl" />
         <tli id="T189" time="156.13266666666667" type="appl" />
         <tli id="T190" time="156.71033333333335" type="appl" />
         <tli id="T191" time="157.288" type="appl" />
         <tli id="T192" time="158.43" type="appl" />
         <tli id="T193" time="159.3025" type="appl" />
         <tli id="T194" time="160.175" type="appl" />
         <tli id="T195" time="161.04749999999999" type="appl" />
         <tli id="T196" time="161.92" type="appl" />
         <tli id="T197" time="162.12" type="appl" />
         <tli id="T198" time="162.90183333333334" type="appl" />
         <tli id="T199" time="163.68366666666668" type="appl" />
         <tli id="T200" time="164.46550000000002" type="appl" />
         <tli id="T201" time="165.24733333333333" type="appl" />
         <tli id="T202" time="166.02916666666667" type="appl" />
         <tli id="T203" time="166.811" type="appl" />
         <tli id="T204" time="167.2" type="appl" />
         <tli id="T205" time="168.493" type="appl" />
         <tli id="T206" time="168.74626913401949" />
         <tli id="T207" time="169.8975" type="appl" />
         <tli id="T208" time="170.985" type="appl" />
         <tli id="T209" time="171.79" type="appl" />
         <tli id="T210" time="172.56574999999998" type="appl" />
         <tli id="T211" time="173.3415" type="appl" />
         <tli id="T212" time="174.11725" type="appl" />
         <tli id="T213" time="174.893" type="appl" />
         <tli id="T214" time="175.23" type="appl" />
         <tli id="T215" time="175.765" type="appl" />
         <tli id="T216" time="176.29999999999998" type="appl" />
         <tli id="T217" time="176.83499999999998" type="appl" />
         <tli id="T218" time="177.37" type="appl" />
         <tli id="T219" time="177.905" type="appl" />
         <tli id="T220" time="178.44" type="appl" />
         <tli id="T221" time="178.975" type="appl" />
         <tli id="T222" time="179.51" type="appl" />
         <tli id="T223" time="183.77956705189297" />
         <tli id="T224" time="185.0075" type="appl" />
         <tli id="T225" time="185.835" type="appl" />
         <tli id="T226" time="186.66250000000002" type="appl" />
         <tli id="T227" time="187.49" type="appl" />
         <tli id="T228" time="187.57955809986987" />
         <tli id="T229" time="188.24333333333334" type="appl" />
         <tli id="T230" time="188.92666666666668" type="appl" />
         <tli id="T231" time="189.61" type="appl" />
         <tli id="T232" time="189.65" type="appl" />
         <tli id="T233" time="190.406" type="appl" />
         <tli id="T234" time="191.162" type="appl" />
         <tli id="T235" time="191.918" type="appl" />
         <tli id="T236" time="192.674" type="appl" />
         <tli id="T237" time="193.43" type="appl" />
         <tli id="T238" time="195.92" type="appl" />
         <tli id="T239" time="196.54333333333332" type="appl" />
         <tli id="T240" time="197.16666666666666" type="appl" />
         <tli id="T241" time="198.10619996786198" />
         <tli id="T242" time="198.20333515569746" />
         <tli id="T243" time="199.80619596300954" />
         <tli id="T244" time="202.1" type="appl" />
         <tli id="T245" time="202.80599999999998" type="appl" />
         <tli id="T246" time="203.512" type="appl" />
         <tli id="T247" time="204.218" type="appl" />
         <tli id="T248" time="204.924" type="appl" />
         <tli id="T249" time="205.839515083043" />
         <tli id="T250" time="206.15" type="appl" />
         <tli id="T251" time="207.06666666666666" type="appl" />
         <tli id="T252" time="207.98333333333335" type="appl" />
         <tli id="T253" time="209.24617372429952" />
         <tli id="T254" time="210.96" type="appl" />
         <tli id="T255" time="211.5" type="appl" />
         <tli id="T256" time="213.62" type="appl" />
         <tli id="T257" time="214.54666666666668" type="appl" />
         <tli id="T258" time="215.47333333333333" type="appl" />
         <tli id="T259" time="216.63948964045105" />
         <tli id="T260" time="216.96" type="appl" />
         <tli id="T261" time="217.8490909090909" type="appl" />
         <tli id="T262" time="218.73818181818183" type="appl" />
         <tli id="T263" time="219.62727272727273" type="appl" />
         <tli id="T264" time="220.51636363636365" type="appl" />
         <tli id="T265" time="221.40545454545455" type="appl" />
         <tli id="T266" time="222.29454545454547" type="appl" />
         <tli id="T267" time="223.18363636363637" type="appl" />
         <tli id="T268" time="224.0727272727273" type="appl" />
         <tli id="T269" time="224.9618181818182" type="appl" />
         <tli id="T270" time="225.8509090909091" type="appl" />
         <tli id="T271" time="226.91946542287275" />
         <tli id="T272" time="228.13" type="appl" />
         <tli id="T273" time="229.35" type="appl" />
         <tli id="T274" time="230.57" type="appl" />
         <tli id="T275" time="231.05" type="appl" />
         <tli id="T276" time="231.6" type="appl" />
         <tli id="T277" time="231.87" type="appl" />
         <tli id="T278" time="232.61555555555557" type="appl" />
         <tli id="T279" time="233.36111111111111" type="appl" />
         <tli id="T280" time="234.10666666666668" type="appl" />
         <tli id="T281" time="234.85222222222222" type="appl" />
         <tli id="T282" time="235.5977777777778" type="appl" />
         <tli id="T283" time="236.34333333333333" type="appl" />
         <tli id="T284" time="237.0888888888889" type="appl" />
         <tli id="T285" time="237.83444444444444" type="appl" />
         <tli id="T286" time="238.58" type="appl" />
         <tli id="T287" time="238.69" type="appl" />
         <tli id="T288" time="239.547" type="appl" />
         <tli id="T289" time="240.404" type="appl" />
         <tli id="T290" time="241.261" type="appl" />
         <tli id="T291" time="242.118" type="appl" />
         <tli id="T292" time="242.975" type="appl" />
         <tli id="T293" time="244.66609028202097" />
         <tli id="T294" time="245.4259" type="appl" />
         <tli id="T295" time="246.0718" type="appl" />
         <tli id="T296" time="246.7177" type="appl" />
         <tli id="T297" time="247.3636" type="appl" />
         <tli id="T298" time="248.0095" type="appl" />
         <tli id="T299" time="248.65540000000001" type="appl" />
         <tli id="T300" time="249.3013" type="appl" />
         <tli id="T301" time="249.9472" type="appl" />
         <tli id="T302" time="250.5931" type="appl" />
         <tli id="T303" time="251.37940780011346" />
         <tli id="T304" time="251.92" type="appl" />
         <tli id="T305" time="252.34" type="appl" />
         <tli id="T306" time="252.7" type="appl" />
         <tli id="T307" time="253.232" type="appl" />
         <tli id="T308" time="253.764" type="appl" />
         <tli id="T309" time="254.296" type="appl" />
         <tli id="T310" time="254.828" type="appl" />
         <tli id="T311" time="255.59939785865623" />
         <tli id="T312" time="255.76" type="appl" />
         <tli id="T313" time="256.16" type="appl" />
         <tli id="T314" time="256.4460625307494" />
         <tli id="T315" time="257.27299999999997" type="appl" />
         <tli id="T316" time="258.01599999999996" type="appl" />
         <tli id="T317" time="258.75899999999996" type="appl" />
         <tli id="T318" time="259.50199999999995" type="appl" />
         <tli id="T319" time="260.245" type="appl" />
         <tli id="T320" time="260.988" type="appl" />
         <tli id="T321" time="261.731" type="appl" />
         <tli id="T322" time="262.474" type="appl" />
         <tli id="T323" time="263.217" type="appl" />
         <tli id="T324" time="264.1793776459304" />
         <tli id="T325" time="264.39" type="appl" />
         <tli id="T326" time="264.58" type="appl" />
         <tli id="T327" time="264.81" type="appl" />
         <tli id="T328" time="265.472" type="appl" />
         <tli id="T329" time="266.134" type="appl" />
         <tli id="T330" time="266.796" type="appl" />
         <tli id="T331" time="267.45799999999997" type="appl" />
         <tli id="T332" time="268.12" type="appl" />
         <tli id="T333" time="268.8726999227299" />
         <tli id="T334" time="268.99" type="appl" />
         <tli id="T335" time="269.27" type="appl" />
         <tli id="T336" time="269.64" type="appl" />
         <tli id="T337" time="270.36333333333334" type="appl" />
         <tli id="T338" time="271.08666666666664" type="appl" />
         <tli id="T339" time="271.81" type="appl" />
         <tli id="T340" time="272.53333333333336" type="appl" />
         <tli id="T341" time="273.25666666666666" type="appl" />
         <tli id="T342" time="273.706672909851" />
         <tli id="T343" time="273.926021351443" />
         <tli id="T344" time="274.597" type="appl" />
         <tli id="T345" time="275.204" type="appl" />
         <tli id="T346" time="275.81100000000004" type="appl" />
         <tli id="T347" time="276.418" type="appl" />
         <tli id="T348" time="277.025" type="appl" />
         <tli id="T349" time="277.632" type="appl" />
         <tli id="T350" time="278.23900000000003" type="appl" />
         <tli id="T351" time="279.0326759878471" />
         <tli id="T352" time="280.92" type="appl" />
         <tli id="T353" time="281.19" type="appl" />
         <tli id="T354" time="282.36" type="appl" />
         <tli id="T355" time="283.02500000000003" type="appl" />
         <tli id="T356" time="283.69" type="appl" />
         <tli id="T357" time="284.355" type="appl" />
         <tli id="T358" time="285.02000000000004" type="appl" />
         <tli id="T359" time="285.685" type="appl" />
         <tli id="T360" time="286.35" type="appl" />
         <tli id="T361" time="287.69" type="appl" />
         <tli id="T362" time="288.6175" type="appl" />
         <tli id="T363" time="289.54499999999996" type="appl" />
         <tli id="T364" time="290.47249999999997" type="appl" />
         <tli id="T365" time="291.7259794182822" />
         <tli id="T366" time="292.91" type="appl" />
         <tli id="T367" time="293.67" type="appl" />
         <tli id="T368" time="296.09" type="appl" />
         <tli id="T369" time="296.4" type="appl" />
         <tli id="T370" time="296.4593016008498" />
         <tli id="T371" time="297.236" type="appl" />
         <tli id="T372" time="297.902" type="appl" />
         <tli id="T373" time="298.568" type="appl" />
         <tli id="T374" time="299.234" type="appl" />
         <tli id="T375" time="299.9" type="appl" />
         <tli id="T376" time="300.16" type="appl" />
         <tli id="T377" time="300.58" type="appl" />
         <tli id="T378" time="300.65" type="appl" />
         <tli id="T379" time="301.48999999999995" type="appl" />
         <tli id="T380" time="302.33" type="appl" />
         <tli id="T381" time="303.16999999999996" type="appl" />
         <tli id="T382" time="304.01" type="appl" />
         <tli id="T383" time="304.84999999999997" type="appl" />
         <tli id="T384" time="305.69" type="appl" />
         <tli id="T385" time="306.53" type="appl" />
         <tli id="T386" time="306.6" type="appl" />
         <tli id="T387" time="307.78200000000004" type="appl" />
         <tli id="T388" time="308.964" type="appl" />
         <tli id="T389" time="310.146" type="appl" />
         <tli id="T390" time="311.328" type="appl" />
         <tli id="T391" time="312.7392632484983" />
         <tli id="T392" time="313.94" type="appl" />
         <tli id="T393" time="314.23" type="appl" />
         <tli id="T394" time="314.25" type="appl" />
         <tli id="T395" time="314.66" type="appl" />
         <tli id="T396" time="315.07" type="appl" />
         <tli id="T397" time="315.48" type="appl" />
         <tli id="T398" time="316.115" type="appl" />
         <tli id="T399" time="316.75" type="appl" />
         <tli id="T400" time="317.385" type="appl" />
         <tli id="T401" time="318.02" type="appl" />
         <tli id="T402" time="319.155" type="appl" />
         <tli id="T403" time="320.28999999999996" type="appl" />
         <tli id="T404" time="321.42499999999995" type="appl" />
         <tli id="T405" time="322.56" type="appl" />
         <tli id="T406" time="323.695" type="appl" />
         <tli id="T407" time="324.83" type="appl" />
         <tli id="T408" time="327.36" type="appl" />
         <tli id="T409" time="327.63" type="appl" />
         <tli id="T410" time="327.68" type="appl" />
         <tli id="T411" time="328.42" type="appl" />
         <tli id="T412" time="329.15999999999997" type="appl" />
         <tli id="T413" time="329.9" type="appl" />
         <tli id="T414" time="330.78999999999996" type="appl" />
         <tli id="T415" time="331.68" type="appl" />
         <tli id="T416" time="332.57" type="appl" />
         <tli id="T417" time="333.46000000000004" type="appl" />
         <tli id="T418" time="334.35" type="appl" />
         <tli id="T419" time="334.67" type="appl" />
         <tli id="T420" time="335.47075" type="appl" />
         <tli id="T421" time="336.2715" type="appl" />
         <tli id="T422" time="337.07225" type="appl" />
         <tli id="T423" time="337.87300000000005" type="appl" />
         <tli id="T424" time="338.67375000000004" type="appl" />
         <tli id="T425" time="339.47450000000003" type="appl" />
         <tli id="T426" time="340.27525" type="appl" />
         <tli id="T427" time="341.076" type="appl" />
         <tli id="T428" time="341.83" type="appl" />
         <tli id="T429" time="342.6633333333333" type="appl" />
         <tli id="T430" time="343.49666666666667" type="appl" />
         <tli id="T431" time="344.33" type="appl" />
         <tli id="T432" time="346.0" type="appl" />
         <tli id="T433" time="347.1566666666667" type="appl" />
         <tli id="T434" time="348.31333333333333" type="appl" />
         <tli id="T435" time="349.47" type="appl" />
         <tli id="T436" time="349.5" type="appl" />
         <tli id="T437" time="350.064" type="appl" />
         <tli id="T438" time="350.628" type="appl" />
         <tli id="T439" time="351.192" type="appl" />
         <tli id="T440" time="351.756" type="appl" />
         <tli id="T441" time="352.32" type="appl" />
         <tli id="T442" time="353.09" type="appl" />
         <tli id="T443" time="353.47" type="appl" />
         <tli id="T444" time="353.86" type="appl" />
         <tli id="T445" time="354.79200000000003" type="appl" />
         <tli id="T446" time="355.724" type="appl" />
         <tli id="T447" time="356.656" type="appl" />
         <tli id="T448" time="357.588" type="appl" />
         <tli id="T449" time="358.52" type="appl" />
         <tli id="T450" time="359.452" type="appl" />
         <tli id="T451" time="360.384" type="appl" />
         <tli id="T452" time="361.31600000000003" type="appl" />
         <tli id="T453" time="362.248" type="appl" />
         <tli id="T454" time="363.18" type="appl" />
         <tli id="T455" time="363.19" type="appl" />
         <tli id="T456" time="363.93666666666667" type="appl" />
         <tli id="T457" time="364.68333333333334" type="appl" />
         <tli id="T458" time="365.43" type="appl" />
         <tli id="T459" time="365.44" type="appl" />
         <tli id="T460" time="366.1816666666667" type="appl" />
         <tli id="T461" time="366.92333333333335" type="appl" />
         <tli id="T462" time="367.66499999999996" type="appl" />
         <tli id="T463" time="368.40666666666664" type="appl" />
         <tli id="T464" time="369.1483333333333" type="appl" />
         <tli id="T465" time="369.89" type="appl" />
         <tli id="T466" time="370.16" type="appl" />
         <tli id="T467" time="371.07500000000005" type="appl" />
         <tli id="T468" time="371.99" type="appl" />
         <tli id="T469" time="372.07" type="appl" />
         <tli id="T470" time="372.852" type="appl" />
         <tli id="T471" time="373.634" type="appl" />
         <tli id="T472" time="374.416" type="appl" />
         <tli id="T473" time="375.19800000000004" type="appl" />
         <tli id="T474" time="375.98" type="appl" />
         <tli id="T475" time="376.26" type="appl" />
         <tli id="T476" time="377.41366666666664" type="appl" />
         <tli id="T477" time="378.56733333333335" type="appl" />
         <tli id="T478" time="379.721" type="appl" />
         <tli id="T479" time="380.86" type="appl" />
         <tli id="T480" time="381.14" type="appl" />
         <tli id="T481" time="381.29" type="appl" />
         <tli id="T482" time="382.17636363636365" type="appl" />
         <tli id="T483" time="383.0627272727273" type="appl" />
         <tli id="T484" time="383.94909090909096" type="appl" />
         <tli id="T485" time="384.8354545454546" type="appl" />
         <tli id="T486" time="385.7218181818182" type="appl" />
         <tli id="T487" time="386.60818181818183" type="appl" />
         <tli id="T488" time="387.49454545454546" type="appl" />
         <tli id="T489" time="388.3809090909091" type="appl" />
         <tli id="T490" time="389.26727272727277" type="appl" />
         <tli id="T491" time="390.1536363636364" type="appl" />
         <tli id="T492" time="391.04" type="appl" />
         <tli id="T493" time="391.42" type="appl" />
         <tli id="T494" time="393.17907374830384" />
         <tli id="T495" time="394.08" type="appl" />
         <tli id="T496" time="395.5883333333333" type="appl" />
         <tli id="T497" time="397.09666666666664" type="appl" />
         <tli id="T498" time="398.605" type="appl" />
         <tli id="T499" time="400.11333333333334" type="appl" />
         <tli id="T500" time="401.62166666666667" type="appl" />
         <tli id="T501" time="403.13" type="appl" />
         <tli id="T502" time="403.47" type="appl" />
         <tli id="T503" time="403.88" type="appl" />
         <tli id="T504" time="404.69" type="appl" />
         <tli id="T505" time="405.531" type="appl" />
         <tli id="T506" time="406.372" type="appl" />
         <tli id="T507" time="407.213" type="appl" />
         <tli id="T508" time="408.05400000000003" type="appl" />
         <tli id="T509" time="408.895" type="appl" />
         <tli id="T510" time="409.736" type="appl" />
         <tli id="T511" time="410.577" type="appl" />
         <tli id="T512" time="411.418" type="appl" />
         <tli id="T513" time="412.259" type="appl" />
         <tli id="T514" time="413.1" type="appl" />
         <tli id="T515" time="413.11" type="appl" />
         <tli id="T516" time="413.802" type="appl" />
         <tli id="T517" time="414.494" type="appl" />
         <tli id="T518" time="415.186" type="appl" />
         <tli id="T519" time="415.878" type="appl" />
         <tli id="T1139" time="416.1745714285714" type="intp" />
         <tli id="T520" time="416.57" type="appl" />
         <tli id="T521" time="416.77" type="appl" />
         <tli id="T522" time="417.5842857142857" type="appl" />
         <tli id="T523" time="418.3985714285714" type="appl" />
         <tli id="T524" time="419.21285714285716" type="appl" />
         <tli id="T525" time="420.02714285714285" type="appl" />
         <tli id="T526" time="420.8414285714286" type="appl" />
         <tli id="T527" time="421.6557142857143" type="appl" />
         <tli id="T528" time="422.47" type="appl" />
         <tli id="T529" time="422.95" type="appl" />
         <tli id="T530" time="423.476" type="appl" />
         <tli id="T531" time="424.002" type="appl" />
         <tli id="T532" time="424.52799999999996" type="appl" />
         <tli id="T533" time="425.054" type="appl" />
         <tli id="T534" time="425.6599868130662" />
         <tli id="T535" time="426.26" type="appl" />
         <tli id="T536" time="426.94" type="appl" />
         <tli id="T537" time="427.62" type="appl" />
         <tli id="T538" time="428.29999999999995" type="appl" />
         <tli id="T539" time="428.97999999999996" type="appl" />
         <tli id="T540" time="429.65999999999997" type="appl" />
         <tli id="T541" time="430.34" type="appl" />
         <tli id="T542" time="431.02" type="appl" />
         <tli id="T543" time="431.14" type="appl" />
         <tli id="T544" time="431.8177777777778" type="appl" />
         <tli id="T545" time="432.49555555555554" type="appl" />
         <tli id="T546" time="433.17333333333335" type="appl" />
         <tli id="T547" time="433.8511111111111" type="appl" />
         <tli id="T548" time="434.5288888888889" type="appl" />
         <tli id="T549" time="435.20666666666665" type="appl" />
         <tli id="T550" time="435.88444444444445" type="appl" />
         <tli id="T1137" time="436.15555555555557" type="intp" />
         <tli id="T551" time="436.5622222222222" type="appl" />
         <tli id="T552" time="437.24" type="appl" />
         <tli id="T553" time="437.57" type="appl" />
         <tli id="T554" time="438.32875" type="appl" />
         <tli id="T555" time="439.0875" type="appl" />
         <tli id="T556" time="439.84625" type="appl" />
         <tli id="T557" time="440.605" type="appl" />
         <tli id="T558" time="441.36375" type="appl" />
         <tli id="T559" time="442.1225" type="appl" />
         <tli id="T560" time="442.88124999999997" type="appl" />
         <tli id="T561" time="443.679996444841" />
         <tli id="T562" time="443.68332977032173" />
         <tli id="T563" time="444.39" type="appl" />
         <tli id="T564" time="445.13" type="appl" />
         <tli id="T565" time="445.76" type="appl" />
         <tli id="T566" time="446.276" type="appl" />
         <tli id="T567" time="446.792" type="appl" />
         <tli id="T568" time="447.308" type="appl" />
         <tli id="T569" time="447.82399999999996" type="appl" />
         <tli id="T570" time="448.34" type="appl" />
         <tli id="T571" time="448.48" type="appl" />
         <tli id="T572" time="449.64675" type="appl" />
         <tli id="T573" time="450.8135" type="appl" />
         <tli id="T574" time="451.98025" type="appl" />
         <tli id="T575" time="453.147" type="appl" />
         <tli id="T576" time="454.167" type="appl" />
         <tli id="T577" time="454.88199999999995" type="appl" />
         <tli id="T578" time="455.597" type="appl" />
         <tli id="T579" time="456.312" type="appl" />
         <tli id="T580" time="457.027" type="appl" />
         <tli id="T581" time="457.74199999999996" type="appl" />
         <tli id="T582" time="458.457" type="appl" />
         <tli id="T583" time="458.897" type="appl" />
         <tli id="T584" time="459.677" type="appl" />
         <tli id="T585" time="460.457" type="appl" />
         <tli id="T586" time="461.237" type="appl" />
         <tli id="T587" time="461.4" type="appl" />
         <tli id="T588" time="461.96999999999997" type="appl" />
         <tli id="T589" time="462.53999999999996" type="appl" />
         <tli id="T590" time="463.10999999999996" type="appl" />
         <tli id="T591" time="463.67999999999995" type="appl" />
         <tli id="T592" time="464.25" type="appl" />
         <tli id="T593" time="464.82" type="appl" />
         <tli id="T594" time="465.39" type="appl" />
         <tli id="T595" time="465.96" type="appl" />
         <tli id="T596" time="466.53" type="appl" />
         <tli id="T597" time="467.857" type="appl" />
         <tli id="T598" time="468.387" type="appl" />
         <tli id="T599" time="469.05" type="appl" />
         <tli id="T600" time="469.8086" type="appl" />
         <tli id="T601" time="470.5672" type="appl" />
         <tli id="T602" time="471.3258" type="appl" />
         <tli id="T603" time="472.0844" type="appl" />
         <tli id="T604" time="472.843" type="appl" />
         <tli id="T605" time="473.59" type="appl" />
         <tli id="T606" time="474.61" type="appl" />
         <tli id="T607" time="475.63" type="appl" />
         <tli id="T608" time="476.65" type="appl" />
         <tli id="T609" time="477.67" type="appl" />
         <tli id="T610" time="477.72" type="appl" />
         <tli id="T611" time="478.72333333333336" type="appl" />
         <tli id="T612" time="479.7266666666667" type="appl" />
         <tli id="T613" time="480.73" type="appl" />
         <tli id="T614" time="481.24" type="appl" />
         <tli id="T615" time="482.225" type="appl" />
         <tli id="T616" time="483.2855281420857" />
         <tli id="T617" time="483.72" type="appl" />
         <tli id="T618" time="484.65000000000003" type="appl" />
         <tli id="T619" time="485.58000000000004" type="appl" />
         <tli id="T620" time="486.51" type="appl" />
         <tli id="T621" time="487.44" type="appl" />
         <tli id="T622" time="488.13" type="appl" />
         <tli id="T623" time="488.8066666666667" type="appl" />
         <tli id="T624" time="489.48333333333335" type="appl" />
         <tli id="T625" time="490.16" type="appl" />
         <tli id="T626" time="490.9893333333334" type="appl" />
         <tli id="T627" time="491.8186666666667" type="appl" />
         <tli id="T628" time="492.648" type="appl" />
         <tli id="T629" time="493.4773333333334" type="appl" />
         <tli id="T630" time="494.3066666666667" type="appl" />
         <tli id="T631" time="495.2188333629254" />
         <tli id="T632" time="495.34" type="appl" />
         <tli id="T633" time="495.96" type="appl" />
         <tli id="T634" time="496.58" type="appl" />
         <tli id="T1138" time="496.9795603673231" type="intp" />
         <tli id="T635" time="497.6454943128615" />
         <tli id="T636" time="498.24" type="appl" />
         <tli id="T637" time="498.76" type="appl" />
         <tli id="T638" time="500.19" type="appl" />
         <tli id="T639" time="501.02166666666665" type="appl" />
         <tli id="T640" time="501.85333333333335" type="appl" />
         <tli id="T641" time="502.685" type="appl" />
         <tli id="T642" time="503.51666666666665" type="appl" />
         <tli id="T643" time="504.34833333333336" type="appl" />
         <tli id="T1140" time="504.68100000000004" type="intp" />
         <tli id="T644" time="505.18" type="appl" />
         <tli id="T645" time="505.66547541938115" />
         <tli id="T646" time="506.498" type="appl" />
         <tli id="T647" time="507.226" type="appl" />
         <tli id="T648" time="507.954" type="appl" />
         <tli id="T649" time="508.682" type="appl" />
         <tli id="T650" time="509.41" type="appl" />
         <tli id="T651" time="509.43" type="appl" />
         <tli id="T652" time="510.2345" type="appl" />
         <tli id="T653" time="511.039" type="appl" />
         <tli id="T654" time="511.84349999999995" type="appl" />
         <tli id="T655" time="512.6479999999999" type="appl" />
         <tli id="T656" time="513.4525" type="appl" />
         <tli id="T657" time="514.3454549710758" />
         <tli id="T658" time="514.52" type="appl" />
         <tli id="T659" time="515.3543636363636" type="appl" />
         <tli id="T660" time="516.1887272727273" type="appl" />
         <tli id="T661" time="517.0230909090909" type="appl" />
         <tli id="T662" time="517.8574545454545" type="appl" />
         <tli id="T663" time="518.6918181818181" type="appl" />
         <tli id="T664" time="519.5261818181818" type="appl" />
         <tli id="T665" time="520.3605454545454" type="appl" />
         <tli id="T666" time="521.194909090909" type="appl" />
         <tli id="T667" time="522.0292727272727" type="appl" />
         <tli id="T668" time="522.8636363636364" type="appl" />
         <tli id="T669" time="523.698" type="appl" />
         <tli id="T670" time="525.35" type="appl" />
         <tli id="T671" time="525.7" type="appl" />
         <tli id="T672" time="525.93" type="appl" />
         <tli id="T673" time="526.5633333333333" type="appl" />
         <tli id="T674" time="527.1966666666666" type="appl" />
         <tli id="T675" time="527.8299999999999" type="appl" />
         <tli id="T676" time="528.4633333333334" type="appl" />
         <tli id="T677" time="529.0966666666667" type="appl" />
         <tli id="T678" time="529.73" type="appl" />
         <tli id="T679" time="531.18" type="appl" />
         <tli id="T680" time="531.65" type="appl" />
         <tli id="T681" time="532.48" type="appl" />
         <tli id="T682" time="533.0" type="appl" />
         <tli id="T683" time="533.52" type="appl" />
         <tli id="T684" time="534.04" type="appl" />
         <tli id="T685" time="534.56" type="appl" />
         <tli id="T686" time="535.08" type="appl" />
         <tli id="T687" time="535.6" type="appl" />
         <tli id="T688" time="536.12" type="appl" />
         <tli id="T689" time="536.64" type="appl" />
         <tli id="T690" time="537.2720676272031" />
         <tli id="T691" time="537.79625" type="appl" />
         <tli id="T692" time="538.4325" type="appl" />
         <tli id="T693" time="539.06875" type="appl" />
         <tli id="T694" time="539.7049999999999" type="appl" />
         <tli id="T695" time="540.34125" type="appl" />
         <tli id="T696" time="540.9775" type="appl" />
         <tli id="T697" time="541.61375" type="appl" />
         <tli id="T698" time="542.4453887732207" />
         <tli id="T699" time="542.58" type="appl" />
         <tli id="T700" time="542.86" type="appl" />
         <tli id="T701" time="543.27" type="appl" />
         <tli id="T702" time="543.9514285714286" type="appl" />
         <tli id="T703" time="544.6328571428571" type="appl" />
         <tli id="T704" time="545.3142857142857" type="appl" />
         <tli id="T705" time="545.9957142857143" type="appl" />
         <tli id="T706" time="546.6771428571428" type="appl" />
         <tli id="T707" time="547.3585714285714" type="appl" />
         <tli id="T708" time="548.04" type="appl" />
         <tli id="T709" time="548.7214285714285" type="appl" />
         <tli id="T710" time="549.4028571428571" type="appl" />
         <tli id="T711" time="550.0842857142857" type="appl" />
         <tli id="T712" time="550.7657142857142" type="appl" />
         <tli id="T713" time="551.4471428571428" type="appl" />
         <tli id="T714" time="552.1285714285714" type="appl" />
         <tli id="T715" time="552.81" type="appl" />
         <tli id="T716" time="553.28" type="appl" />
         <tli id="T717" time="553.61" type="appl" />
         <tli id="T718" time="553.8520285681127" />
         <tli id="T719" time="554.56" type="appl" />
         <tli id="T720" time="555.11" type="appl" />
         <tli id="T721" time="555.66" type="appl" />
         <tli id="T722" time="556.21" type="appl" />
         <tli id="T723" time="556.76" type="appl" />
         <tli id="T724" time="557.31" type="appl" />
         <tli id="T725" time="557.86" type="appl" />
         <tli id="T726" time="558.41" type="appl" />
         <tli id="T727" time="558.44" type="appl" />
         <tli id="T728" time="559.2014285714287" type="appl" />
         <tli id="T729" time="559.9628571428572" type="appl" />
         <tli id="T730" time="560.7242857142858" type="appl" />
         <tli id="T731" time="561.4857142857143" type="appl" />
         <tli id="T732" time="562.2471428571429" type="appl" />
         <tli id="T733" time="563.0085714285714" type="appl" />
         <tli id="T734" time="563.9453381236164" />
         <tli id="T735" time="564.39" type="appl" />
         <tli id="T736" time="564.66" type="appl" />
         <tli id="T737" time="565.21" type="appl" />
         <tli id="T738" time="566.0785714285714" type="appl" />
         <tli id="T739" time="566.9471428571429" type="appl" />
         <tli id="T740" time="567.8157142857143" type="appl" />
         <tli id="T741" time="568.6842857142857" type="appl" />
         <tli id="T742" time="569.5528571428571" type="appl" />
         <tli id="T743" time="570.4214285714286" type="appl" />
         <tli id="T744" time="571.29" type="appl" />
         <tli id="T745" time="571.92" type="appl" />
         <tli id="T746" time="572.37" type="appl" />
         <tli id="T747" time="572.87" type="appl" />
         <tli id="T748" time="573.4961111111111" type="appl" />
         <tli id="T749" time="574.1222222222223" type="appl" />
         <tli id="T750" time="574.7483333333333" type="appl" />
         <tli id="T751" time="575.3744444444444" type="appl" />
         <tli id="T752" time="576.0005555555556" type="appl" />
         <tli id="T753" time="576.6266666666667" type="appl" />
         <tli id="T754" time="577.2527777777777" type="appl" />
         <tli id="T755" time="577.8788888888889" type="appl" />
         <tli id="T756" time="578.9253028337989" />
         <tli id="T757" time="579.11" type="appl" />
         <tli id="T758" time="579.47" type="appl" />
         <tli id="T759" time="580.26" type="appl" />
         <tli id="T760" time="581.8333333333334" type="appl" />
         <tli id="T761" time="583.4066666666666" type="appl" />
         <tli id="T762" time="584.98" type="appl" />
         <tli id="T763" time="585.66" type="appl" />
         <tli id="T764" time="586.511" type="appl" />
         <tli id="T765" time="587.362" type="appl" />
         <tli id="T766" time="588.213" type="appl" />
         <tli id="T767" time="589.064" type="appl" />
         <tli id="T768" time="589.915" type="appl" />
         <tli id="T769" time="590.766" type="appl" />
         <tli id="T770" time="591.9719387651862" />
         <tli id="T771" time="592.7" type="appl" />
         <tli id="T772" time="593.59" type="appl" />
         <tli id="T773" time="593.97" type="appl" />
         <tli id="T774" time="594.6925" type="appl" />
         <tli id="T775" time="595.415" type="appl" />
         <tli id="T776" time="596.1375" type="appl" />
         <tli id="T777" time="596.86" type="appl" />
         <tli id="T778" time="597.14" type="appl" />
         <tli id="T779" time="598.2336666666666" type="appl" />
         <tli id="T780" time="599.3273333333334" type="appl" />
         <tli id="T781" time="600.421" type="appl" />
         <tli id="T782" time="600.7" type="appl" />
         <tli id="T783" time="601.7466666666667" type="appl" />
         <tli id="T784" time="602.7933333333334" type="appl" />
         <tli id="T785" time="603.84" type="appl" />
         <tli id="T786" time="603.85" type="appl" />
         <tli id="T787" time="605.1614285714286" type="appl" />
         <tli id="T788" time="606.4728571428572" type="appl" />
         <tli id="T789" time="607.7842857142857" type="appl" />
         <tli id="T790" time="609.0957142857143" type="appl" />
         <tli id="T791" time="610.4071428571428" type="appl" />
         <tli id="T792" time="611.7185714285714" type="appl" />
         <tli id="T793" time="613.03" type="appl" />
         <tli id="T794" time="613.07" type="appl" />
         <tli id="T795" time="613.8266666666667" type="appl" />
         <tli id="T796" time="614.5833333333334" type="appl" />
         <tli id="T797" time="615.34" type="appl" />
         <tli id="T798" time="616.0966666666667" type="appl" />
         <tli id="T799" time="616.8533333333334" type="appl" />
         <tli id="T800" time="617.61" type="appl" />
         <tli id="T801" time="618.3666666666667" type="appl" />
         <tli id="T802" time="619.1233333333333" type="appl" />
         <tli id="T803" time="619.9118729442583" />
         <tli id="T804" time="620.8299999999999" type="appl" />
         <tli id="T805" time="621.78" type="appl" />
         <tli id="T806" time="622.73" type="appl" />
         <tli id="T807" time="623.68" type="appl" />
         <tli id="T808" time="623.84" type="appl" />
         <tli id="T809" time="626.455" type="appl" />
         <tli id="T810" time="629.07" type="appl" />
         <tli id="T811" time="631.17" type="appl" />
         <tli id="T812" time="631.43" type="appl" />
         <tli id="T813" time="631.82" type="appl" />
         <tli id="T814" time="633.0066666666667" type="appl" />
         <tli id="T815" time="634.1933333333334" type="appl" />
         <tli id="T816" time="635.3800000000001" type="appl" />
         <tli id="T817" time="636.5666666666667" type="appl" />
         <tli id="T818" time="637.7533333333333" type="appl" />
         <tli id="T819" time="638.94" type="appl" />
         <tli id="T820" time="639.41" type="appl" />
         <tli id="T821" time="640.2533333333333" type="appl" />
         <tli id="T822" time="641.0966666666667" type="appl" />
         <tli id="T823" time="641.94" type="appl" />
         <tli id="T824" time="641.99" type="appl" />
         <tli id="T825" time="642.725" type="appl" />
         <tli id="T826" time="643.46" type="appl" />
         <tli id="T827" time="643.48" type="appl" />
         <tli id="T828" time="644.282" type="appl" />
         <tli id="T829" time="645.0840000000001" type="appl" />
         <tli id="T830" time="645.886" type="appl" />
         <tli id="T831" time="646.688" type="appl" />
         <tli id="T832" time="647.49" type="appl" />
         <tli id="T833" time="647.55" type="appl" />
         <tli id="T834" time="648.3157142857142" type="appl" />
         <tli id="T835" time="649.0814285714285" type="appl" />
         <tli id="T836" time="649.8471428571428" type="appl" />
         <tli id="T837" time="650.6128571428571" type="appl" />
         <tli id="T838" time="651.3785714285714" type="appl" />
         <tli id="T839" time="652.1442857142857" type="appl" />
         <tli id="T840" time="652.91" type="appl" />
         <tli id="T841" time="654.13" type="appl" />
         <tli id="T842" time="654.4" type="appl" />
         <tli id="T843" time="654.54" type="appl" />
         <tli id="T844" time="655.182" type="appl" />
         <tli id="T845" time="655.824" type="appl" />
         <tli id="T846" time="656.466" type="appl" />
         <tli id="T847" time="657.108" type="appl" />
         <tli id="T848" time="657.75" type="appl" />
         <tli id="T849" time="657.78" type="appl" />
         <tli id="T850" time="658.874" type="appl" />
         <tli id="T851" time="659.968" type="appl" />
         <tli id="T852" time="661.062" type="appl" />
         <tli id="T853" time="662.156" type="appl" />
         <tli id="T854" time="663.25" type="appl" />
         <tli id="T855" time="663.33" type="appl" />
         <tli id="T856" time="664.1128571428571" type="appl" />
         <tli id="T857" time="664.8957142857143" type="appl" />
         <tli id="T858" time="665.6785714285714" type="appl" />
         <tli id="T859" time="666.4614285714285" type="appl" />
         <tli id="T860" time="667.2442857142856" type="appl" />
         <tli id="T861" time="668.0271428571428" type="appl" />
         <tli id="T862" time="668.81" type="appl" />
         <tli id="T863" time="669.28" type="appl" />
         <tli id="T864" time="670.1925" type="appl" />
         <tli id="T865" time="671.105" type="appl" />
         <tli id="T866" time="672.0174999999999" type="appl" />
         <tli id="T867" time="672.93" type="appl" />
         <tli id="T868" time="673.65" type="appl" />
         <tli id="T869" time="674.265" type="appl" />
         <tli id="T870" time="674.88" type="appl" />
         <tli id="T871" time="675.495" type="appl" />
         <tli id="T872" time="676.11" type="appl" />
         <tli id="T873" time="676.725" type="appl" />
         <tli id="T874" time="677.34" type="appl" />
         <tli id="T875" time="677.8583333333333" type="appl" />
         <tli id="T876" time="678.3766666666667" type="appl" />
         <tli id="T877" time="678.895" type="appl" />
         <tli id="T878" time="679.4133333333334" type="appl" />
         <tli id="T879" time="679.9316666666667" type="appl" />
         <tli id="T880" time="680.45" type="appl" />
         <tli id="T881" time="680.63" type="appl" />
         <tli id="T882" time="681.4266666666666" type="appl" />
         <tli id="T883" time="682.2233333333334" type="appl" />
         <tli id="T884" time="683.02" type="appl" />
         <tli id="T885" time="683.26" type="appl" />
         <tli id="T886" time="683.95" type="appl" />
         <tli id="T887" time="684.64" type="appl" />
         <tli id="T888" time="685.33" type="appl" />
         <tli id="T889" time="685.9166666666667" type="appl" />
         <tli id="T890" time="686.5033333333333" type="appl" />
         <tli id="T891" time="687.09" type="appl" />
         <tli id="T892" time="688.16" type="appl" />
         <tli id="T893" time="688.51" type="appl" />
         <tli id="T894" time="688.74" type="appl" />
         <tli id="T895" time="689.2477777777777" type="appl" />
         <tli id="T896" time="689.7555555555556" type="appl" />
         <tli id="T897" time="690.2633333333333" type="appl" />
         <tli id="T898" time="690.771111111111" type="appl" />
         <tli id="T899" time="691.2788888888889" type="appl" />
         <tli id="T900" time="691.7866666666666" type="appl" />
         <tli id="T901" time="692.2944444444444" type="appl" />
         <tli id="T902" time="692.8022222222222" type="appl" />
         <tli id="T903" time="693.31" type="appl" />
         <tli id="T904" time="693.36" type="appl" />
         <tli id="T905" time="693.8525" type="appl" />
         <tli id="T906" time="694.345" type="appl" />
         <tli id="T907" time="694.8375000000001" type="appl" />
         <tli id="T908" time="695.5716947047667" />
         <tli id="T909" time="696.36" type="appl" />
         <tli id="T910" time="696.888" type="appl" />
         <tli id="T911" time="697.416" type="appl" />
         <tli id="T912" time="697.944" type="appl" />
         <tli id="T913" time="698.472" type="appl" />
         <tli id="T914" time="699.3116858940914" />
         <tli id="T915" time="699.7750181359061" type="intp" />
         <tli id="T916" time="700.2383503777207" />
         <tli id="T917" time="700.365" type="appl" />
         <tli id="T918" time="701.42" type="appl" />
         <tli id="T919" time="701.54" type="appl" />
         <tli id="T920" time="702.01" type="appl" />
         <tli id="T921" time="702.48" type="appl" />
         <tli id="T922" time="702.9499999999999" type="appl" />
         <tli id="T923" time="703.42" type="appl" />
         <tli id="T924" time="703.89" type="appl" />
         <tli id="T925" time="704.0" type="appl" />
         <tli id="T926" time="704.765" type="appl" />
         <tli id="T927" time="705.53" type="appl" />
         <tli id="T928" time="705.71" type="appl" />
         <tli id="T929" time="706.28" type="appl" />
         <tli id="T930" time="706.85" type="appl" />
         <tli id="T931" time="707.4200000000001" type="appl" />
         <tli id="T932" time="707.99" type="appl" />
         <tli id="T933" time="708.07" type="appl" />
         <tli id="T934" time="708.925" type="appl" />
         <tli id="T935" time="709.9116609226585" />
         <tli id="T936" time="710.73" type="appl" />
         <tli id="T937" time="711.28" type="appl" />
         <tli id="T938" time="711.56" type="appl" />
         <tli id="T939" time="712.228" type="appl" />
         <tli id="T940" time="712.896" type="appl" />
         <tli id="T941" time="713.564" type="appl" />
         <tli id="T942" time="714.232" type="appl" />
         <tli id="T943" time="714.9" type="appl" />
         <tli id="T944" time="714.91" type="appl" />
         <tli id="T945" time="715.6922857142857" type="appl" />
         <tli id="T946" time="716.4745714285714" type="appl" />
         <tli id="T947" time="717.2568571428571" type="appl" />
         <tli id="T948" time="718.0391428571428" type="appl" />
         <tli id="T949" time="718.8214285714286" type="appl" />
         <tli id="T950" time="719.6037142857142" type="appl" />
         <tli id="T951" time="720.386" type="appl" />
         <tli id="T952" time="721.1" type="appl" />
         <tli id="T953" time="721.46" type="appl" />
         <tli id="T954" time="722.11" type="appl" />
         <tli id="T955" time="722.806" type="appl" />
         <tli id="T956" time="723.5020000000001" type="appl" />
         <tli id="T957" time="724.198" type="appl" />
         <tli id="T958" time="724.894" type="appl" />
         <tli id="T959" time="725.59" type="appl" />
         <tli id="T960" time="725.95" type="appl" />
         <tli id="T961" time="726.4685714285715" type="appl" />
         <tli id="T962" time="726.9871428571429" type="appl" />
         <tli id="T963" time="727.5057142857144" type="appl" />
         <tli id="T964" time="728.0242857142857" type="appl" />
         <tli id="T965" time="728.5428571428572" type="appl" />
         <tli id="T966" time="729.0614285714286" type="appl" />
         <tli id="T967" time="729.58" type="appl" />
         <tli id="T968" time="729.63" type="appl" />
         <tli id="T969" time="730.3085714285714" type="appl" />
         <tli id="T970" time="730.9871428571429" type="appl" />
         <tli id="T971" time="731.6657142857143" type="appl" />
         <tli id="T972" time="732.3442857142857" type="appl" />
         <tli id="T973" time="733.0228571428571" type="appl" />
         <tli id="T974" time="733.7014285714286" type="appl" />
         <tli id="T975" time="734.38" type="appl" />
         <tli id="T976" time="734.47" type="appl" />
         <tli id="T977" time="735.326" type="appl" />
         <tli id="T978" time="736.182" type="appl" />
         <tli id="T979" time="737.038" type="appl" />
         <tli id="T980" time="737.894" type="appl" />
         <tli id="T981" time="738.8366448644827" />
         <tli id="T982" time="738.8399781899634" />
         <tli id="T983" time="739.312" type="appl" />
         <tli id="T984" time="739.764" type="appl" />
         <tli id="T985" time="740.216" type="appl" />
         <tli id="T986" time="740.668" type="appl" />
         <tli id="T987" time="741.12" type="appl" />
         <tli id="T988" time="741.25" type="appl" />
         <tli id="T989" time="741.8542857142858" type="appl" />
         <tli id="T990" time="742.4585714285714" type="appl" />
         <tli id="T991" time="743.0628571428572" type="appl" />
         <tli id="T992" time="743.6671428571428" type="appl" />
         <tli id="T993" time="744.2714285714286" type="appl" />
         <tli id="T994" time="744.8757142857143" type="appl" />
         <tli id="T995" time="745.48" type="appl" />
         <tli id="T996" time="745.72" type="appl" />
         <tli id="T997" time="746.745" type="appl" />
         <tli id="T998" time="747.77" type="appl" />
         <tli id="T999" time="748.59" type="appl" />
         <tli id="T1000" time="749.1" type="appl" />
         <tli id="T1001" time="749.3" type="appl" />
         <tli id="T1002" time="749.87875" type="appl" />
         <tli id="T1003" time="750.4575" type="appl" />
         <tli id="T1004" time="751.03625" type="appl" />
         <tli id="T1005" time="751.615" type="appl" />
         <tli id="T1006" time="752.1937499999999" type="appl" />
         <tli id="T1007" time="752.7724999999999" type="appl" />
         <tli id="T1008" time="753.3512499999999" type="appl" />
         <tli id="T1009" time="754.1582233532246" />
         <tli id="T1010" time="754.27" type="appl" />
         <tli id="T1011" time="754.966" type="appl" />
         <tli id="T1012" time="755.662" type="appl" />
         <tli id="T1013" time="756.358" type="appl" />
         <tli id="T1014" time="757.054" type="appl" />
         <tli id="T1015" time="757.75" type="appl" />
         <tli id="T1016" time="757.78" type="appl" />
         <tli id="T1017" time="758.5988888888888" type="appl" />
         <tli id="T1018" time="759.4177777777777" type="appl" />
         <tli id="T1019" time="760.2366666666667" type="appl" />
         <tli id="T1020" time="761.0555555555555" type="appl" />
         <tli id="T1021" time="761.8744444444444" type="appl" />
         <tli id="T1022" time="762.6933333333333" type="appl" />
         <tli id="T1023" time="763.5122222222222" type="appl" />
         <tli id="T1024" time="764.3311111111111" type="appl" />
         <tli id="T1025" time="765.0966871677127" />
         <tli id="T1026" time="765.1000204931934" />
         <tli id="T1027" time="766.168" type="appl" />
         <tli id="T1028" time="767.176" type="appl" />
         <tli id="T1029" time="768.184" type="appl" />
         <tli id="T1030" time="769.192" type="appl" />
         <tli id="T1031" time="770.2" type="appl" />
         <tli id="T1032" time="771.98" type="appl" />
         <tli id="T1033" time="772.28" type="appl" />
         <tli id="T1034" time="772.3" type="appl" />
         <tli id="T1035" time="772.9224999999999" type="appl" />
         <tli id="T1036" time="773.545" type="appl" />
         <tli id="T1037" time="774.1675" type="appl" />
         <tli id="T1038" time="774.79" type="appl" />
         <tli id="T1039" time="775.07" type="appl" />
         <tli id="T1040" time="775.585" type="appl" />
         <tli id="T1041" time="776.1" type="appl" />
         <tli id="T1042" time="776.615" type="appl" />
         <tli id="T1043" time="777.13" type="appl" />
         <tli id="T1044" time="777.81" type="appl" />
         <tli id="T1045" time="778.49" type="appl" />
         <tli id="T1046" time="778.67" type="appl" />
         <tli id="T1047" time="779.265" type="appl" />
         <tli id="T1048" time="779.8599999999999" type="appl" />
         <tli id="T1049" time="780.4549999999999" type="appl" />
         <tli id="T1050" time="781.05" type="appl" />
         <tli id="T1051" time="781.9041666666666" type="appl" />
         <tli id="T1052" time="782.7583333333333" type="appl" />
         <tli id="T1053" time="783.6125" type="appl" />
         <tli id="T1054" time="784.4666666666666" type="appl" />
         <tli id="T1055" time="785.3208333333333" type="appl" />
         <tli id="T1056" time="786.175" type="appl" />
         <tli id="T1057" time="787.0291666666666" type="appl" />
         <tli id="T1058" time="787.8833333333333" type="appl" />
         <tli id="T1059" time="788.7375" type="appl" />
         <tli id="T1060" time="789.5916666666666" type="appl" />
         <tli id="T1061" time="790.4458333333333" type="appl" />
         <tli id="T1062" time="791.3" type="appl" />
         <tli id="T1063" time="791.42" type="appl" />
         <tli id="T1064" time="792.298" type="appl" />
         <tli id="T1065" time="793.1759999999999" type="appl" />
         <tli id="T1066" time="794.054" type="appl" />
         <tli id="T1067" time="794.9319999999999" type="appl" />
         <tli id="T1068" time="795.81" type="appl" />
         <tli id="T1069" time="795.92" type="appl" />
         <tli id="T1070" time="796.6116" type="appl" />
         <tli id="T1071" time="797.3032" type="appl" />
         <tli id="T1072" time="797.9948" type="appl" />
         <tli id="T1073" time="798.6864" type="appl" />
         <tli id="T1074" time="799.378" type="appl" />
         <tli id="T1075" time="800.34" type="appl" />
         <tli id="T1076" time="800.75" type="appl" />
         <tli id="T1077" time="801.26" type="appl" />
         <tli id="T1078" time="801.995" type="appl" />
         <tli id="T1079" time="802.73" type="appl" />
         <tli id="T1080" time="803.465" type="appl" />
         <tli id="T1081" time="804.2" type="appl" />
         <tli id="T1082" time="804.27" type="appl" />
         <tli id="T1083" time="804.795" type="appl" />
         <tli id="T1084" time="805.3199999999999" type="appl" />
         <tli id="T1085" time="805.845" type="appl" />
         <tli id="T1086" time="806.37" type="appl" />
         <tli id="T1087" time="806.844" type="appl" />
         <tli id="T1088" time="807.318" type="appl" />
         <tli id="T1089" time="807.792" type="appl" />
         <tli id="T1090" time="808.266" type="appl" />
         <tli id="T1091" time="808.74" type="appl" />
         <tli id="T1092" time="808.79" type="appl" />
         <tli id="T1093" time="809.8" type="appl" />
         <tli id="T1094" time="810.81" type="appl" />
         <tli id="T1095" time="811.8199999999999" type="appl" />
         <tli id="T1096" time="812.8299999999999" type="appl" />
         <tli id="T1097" time="813.84" type="appl" />
         <tli id="T1098" time="814.85" type="appl" />
         <tli id="T1099" time="815.86" type="appl" />
         <tli id="T1100" time="816.87" type="appl" />
         <tli id="T1101" time="817.88" type="appl" />
         <tli id="T1102" time="818.58" type="appl" />
         <tli id="T1103" time="819.2434285714286" type="appl" />
         <tli id="T1104" time="819.9068571428572" type="appl" />
         <tli id="T1105" time="820.5702857142858" type="appl" />
         <tli id="T1106" time="821.2337142857143" type="appl" />
         <tli id="T1107" time="821.8971428571429" type="appl" />
         <tli id="T1108" time="822.5605714285715" type="appl" />
         <tli id="T1109" time="823.224" type="appl" />
         <tli id="T1110" time="823.45" type="appl" />
         <tli id="T1111" time="824.1347272727273" type="appl" />
         <tli id="T1112" time="824.8194545454546" type="appl" />
         <tli id="T1113" time="825.5041818181818" type="appl" />
         <tli id="T1114" time="826.1889090909091" type="appl" />
         <tli id="T1115" time="826.8736363636364" type="appl" />
         <tli id="T1116" time="827.5583636363637" type="appl" />
         <tli id="T1117" time="828.2430909090909" type="appl" />
         <tli id="T1118" time="828.9278181818182" type="appl" />
         <tli id="T1119" time="829.6125454545454" type="appl" />
         <tli id="T1120" time="830.2972727272727" type="appl" />
         <tli id="T1121" time="830.982" type="appl" />
         <tli id="T1122" time="831.07" type="appl" />
         <tli id="T1123" time="831.6366666666667" type="appl" />
         <tli id="T1124" time="832.2033333333334" type="appl" />
         <tli id="T1125" time="832.77" type="appl" />
         <tli id="T1126" time="832.8" type="appl" />
         <tli id="T1127" time="833.7914285714286" type="appl" />
         <tli id="T1128" time="834.7828571428571" type="appl" />
         <tli id="T1129" time="835.7742857142857" type="appl" />
         <tli id="T1130" time="836.7657142857142" type="appl" />
         <tli id="T1131" time="837.7571428571429" type="appl" />
         <tli id="T1132" time="838.7485714285714" type="appl" />
         <tli id="T1133" time="839.74" type="appl" />
         <tli id="T0" time="840.124" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T486" start="T485">
            <tli id="T485.tx.1" />
         </timeline-fork>
         <timeline-fork end="T490" start="T489">
            <tli id="T489.tx.1" />
         </timeline-fork>
         <timeline-fork end="T563" start="T562">
            <tli id="T562.tx.1" />
         </timeline-fork>
         <timeline-fork end="T770" start="T769">
            <tli id="T769.tx.1" />
         </timeline-fork>
         <timeline-fork end="T859" start="T858">
            <tli id="T858.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ulardə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">nüke</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_16" n="sc" s="T6">
               <ts e="T9" id="Seg_18" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">Šide</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">koʔbdo</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_26" n="HIAT:w" s="T8">ibi</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_30" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">Šide</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">koʔbdo</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">ibi</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">dĭgəttə</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">u-</ts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">ular</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">tabun</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">ibi</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">Dĭgəttə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">tabun</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">ine</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">ibiʔi</ts>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_77" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">Dĭgəttə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">tabun</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">tüžöj</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">ibiʔi</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_91" n="sc" s="T26">
               <ts e="T32" id="Seg_93" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">Dĭgəttə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">onʼiʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_101" n="HIAT:w" s="T28">măndərbi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">a</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">onʼiʔ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">bar</ts>
                  <nts id="Seg_111" n="HIAT:ip">…</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_114" n="HIAT:u" s="T32">
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <ats e="T33" id="Seg_117" n="HIAT:non-pho" s="T32">BRK</ats>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">Dĭgəttə</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">onʼiʔ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">unu</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_130" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">mĭnzərbi=</ts>
                  <nts id="Seg_133" n="HIAT:ip">)</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">măndərbi</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">dĭn</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">kodurgən</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">a</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">onʼiʔ</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">svinʼes</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">mĭnzərbi</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_159" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">Dĭgəttə</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">deʔpi</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_168" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">I</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">kunə</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">kămnəbi</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_179" n="sc" s="T50">
               <ts e="T57" id="Seg_181" n="HIAT:u" s="T50">
                  <nts id="Seg_182" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">Ku-</ts>
                  <nts id="Seg_185" n="HIAT:ip">)</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">Kunə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">kămnəbi</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">i</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">dĭ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">bar</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">külambi</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_207" n="HIAT:u" s="T57">
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ats e="T58" id="Seg_210" n="HIAT:non-pho" s="T57">BRK</ats>
                  <nts id="Seg_211" n="HIAT:ip">)</nts>
                  <nts id="Seg_212" n="HIAT:ip">)</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">Dĭgəttə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_218" n="HIAT:w" s="T59">dĭn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_221" n="HIAT:w" s="T60">nüjnə</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">bar</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">külaːmbi</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_231" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_233" n="HIAT:w" s="T63">Dĭgəttə</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">dĭ</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">koʔbsaŋ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">ibiʔi</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">ular</ts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">ineʔi</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">i</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_255" n="HIAT:w" s="T70">tüžöjʔi</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_258" n="HIAT:w" s="T71">i</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_261" n="HIAT:w" s="T72">abandə</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_263" n="HIAT:ip">(</nts>
                  <ts e="T74" id="Seg_265" n="HIAT:w" s="T73">ka-</ts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_269" n="HIAT:w" s="T74">kalla</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_272" n="HIAT:w" s="T1134">dʼürbiʔi</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_276" n="HIAT:u" s="T75">
                  <nts id="Seg_277" n="HIAT:ip">(</nts>
                  <nts id="Seg_278" n="HIAT:ip">(</nts>
                  <ats e="T76" id="Seg_279" n="HIAT:non-pho" s="T75">BRK</ats>
                  <nts id="Seg_280" n="HIAT:ip">)</nts>
                  <nts id="Seg_281" n="HIAT:ip">)</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">Dĭgəttə</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">bar</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">skazka</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_294" n="HIAT:u" s="T79">
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <nts id="Seg_296" n="HIAT:ip">(</nts>
                  <ats e="T80" id="Seg_297" n="HIAT:non-pho" s="T79">BRK</ats>
                  <nts id="Seg_298" n="HIAT:ip">)</nts>
                  <nts id="Seg_299" n="HIAT:ip">)</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">Tumo</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_305" n="HIAT:w" s="T81">i</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_308" n="HIAT:w" s="T82">kös</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_311" n="HIAT:w" s="T83">i</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">puzɨrʼ</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">kambiʔi</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T103" id="Seg_320" n="sc" s="T87">
               <ts e="T94" id="Seg_322" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_324" n="HIAT:w" s="T87">Dʼăga</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">a</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_331" n="HIAT:w" s="T89">dĭ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_334" n="HIAT:w" s="T90">kanzittə</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_337" n="HIAT:w" s="T91">dʼăga</ts>
                  <nts id="Seg_338" n="HIAT:ip">,</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_341" n="HIAT:w" s="T92">dʼăga</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_344" n="HIAT:w" s="T93">mʼaŋnaʔbə</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_348" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_350" n="HIAT:w" s="T94">Tumo</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_353" n="HIAT:w" s="T95">bar</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_356" n="HIAT:w" s="T96">moʔi</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_359" n="HIAT:w" s="T97">embi</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_363" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_365" n="HIAT:w" s="T98">Dĭgəttə</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_368" n="HIAT:w" s="T99">tumo</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_371" n="HIAT:w" s="T100">kalla</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T1135">dʼürbi</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_378" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_380" n="HIAT:w" s="T101">Puzɨrʼ</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1136" id="Seg_383" n="HIAT:w" s="T102">kalla</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_386" n="HIAT:w" s="T1136">dʼürbi</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_389" n="sc" s="T104">
               <ts e="T114" id="Seg_391" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_393" n="HIAT:w" s="T104">A</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_395" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">köš-</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">kam-</ts>
                  <nts id="Seg_401" n="HIAT:ip">)</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_404" n="HIAT:w" s="T107">kös</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_407" n="HIAT:w" s="T108">kambi</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_410" n="HIAT:w" s="T109">i</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_413" n="HIAT:w" s="T110">bünə</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_416" n="HIAT:w" s="T111">saʔməluʔpi</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_419" n="HIAT:w" s="T112">i</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_422" n="HIAT:w" s="T113">külambi</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_425" n="sc" s="T115">
               <ts e="T121" id="Seg_427" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T115">A</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_432" n="HIAT:w" s="T116">puzɨrʼ</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_435" n="HIAT:w" s="T117">kaknarbi</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_439" n="HIAT:w" s="T118">kaknarbi</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_442" n="HIAT:w" s="T119">i</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_445" n="HIAT:w" s="T120">külambi</ts>
                  <nts id="Seg_446" n="HIAT:ip">.</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T132" id="Seg_448" n="sc" s="T122">
               <ts e="T126" id="Seg_450" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_452" n="HIAT:w" s="T122">Tumo</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_455" n="HIAT:w" s="T123">unnʼa</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_457" n="HIAT:ip">(</nts>
                  <ts e="T125" id="Seg_459" n="HIAT:w" s="T124">amno-</ts>
                  <nts id="Seg_460" n="HIAT:ip">)</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">maluʔpi</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_467" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_469" n="HIAT:w" s="T126">Dĭgəttə</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_472" n="HIAT:w" s="T127">kambi</ts>
                  <nts id="Seg_473" n="HIAT:ip">,</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_476" n="HIAT:w" s="T128">kambi</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_480" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_482" n="HIAT:w" s="T129">Esseŋ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_485" n="HIAT:w" s="T130">bar</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_488" n="HIAT:w" s="T131">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_491" n="sc" s="T133">
               <ts e="T137" id="Seg_493" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_495" n="HIAT:w" s="T133">Bü</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_498" n="HIAT:w" s="T134">konnambi</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_501" n="HIAT:w" s="T135">mu</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_504" n="HIAT:w" s="T136">bügən</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T149" id="Seg_507" n="sc" s="T138">
               <ts e="T143" id="Seg_509" n="HIAT:u" s="T138">
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <ts e="T139" id="Seg_512" n="HIAT:w" s="T138">Esseŋ</ts>
                  <nts id="Seg_513" n="HIAT:ip">,</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_515" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_517" n="HIAT:w" s="T139">ia=</ts>
                  <nts id="Seg_518" n="HIAT:ip">)</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_521" n="HIAT:w" s="T140">abat</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_524" n="HIAT:w" s="T141">idjot</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_527" n="HIAT:w" s="T142">ian</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_531" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_533" n="HIAT:w" s="T143">Kangaʔ</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_536" n="HIAT:w" s="T144">maʔnə</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_539" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_541" n="HIAT:ip">(</nts>
                  <ts e="T147" id="Seg_543" n="HIAT:w" s="T146">măndlaʔ-</ts>
                  <nts id="Seg_544" n="HIAT:ip">)</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_547" n="HIAT:w" s="T147">mănaʔ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_550" n="HIAT:w" s="T148">ianə</ts>
                  <nts id="Seg_551" n="HIAT:ip">"</nts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_554" n="sc" s="T150">
               <ts e="T153" id="Seg_556" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_558" n="HIAT:w" s="T150">Dĭzeŋ</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_561" n="HIAT:w" s="T151">nuʔməleʔ</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_564" n="HIAT:w" s="T152">šobiʔi</ts>
                  <nts id="Seg_565" n="HIAT:ip">.</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_567" n="sc" s="T154">
               <ts e="T156" id="Seg_569" n="HIAT:u" s="T154">
                  <nts id="Seg_570" n="HIAT:ip">"</nts>
                  <ts e="T155" id="Seg_572" n="HIAT:w" s="T154">Abal</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_575" n="HIAT:w" s="T155">šonəga</ts>
                  <nts id="Seg_576" n="HIAT:ip">!</nts>
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_579" n="sc" s="T157">
               <ts e="T160" id="Seg_581" n="HIAT:u" s="T157">
                  <nts id="Seg_582" n="HIAT:ip">"</nts>
                  <ts e="T158" id="Seg_584" n="HIAT:w" s="T157">No</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_587" n="HIAT:w" s="T158">kangaʔ</ts>
                  <nts id="Seg_588" n="HIAT:ip">,</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_591" n="HIAT:w" s="T159">kăštəgaʔ</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_595" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_597" n="HIAT:w" s="T160">Măn</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_600" n="HIAT:w" s="T161">tüjö</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_603" n="HIAT:w" s="T162">uja</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_606" n="HIAT:w" s="T163">mĭnzərləm</ts>
                  <nts id="Seg_607" n="HIAT:ip">"</nts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_610" n="sc" s="T165">
               <ts e="T172" id="Seg_612" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_614" n="HIAT:w" s="T165">Dĭzeŋ</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_617" n="HIAT:w" s="T166">nuʔməluʔbiʔi:</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_619" n="HIAT:ip">"</nts>
                  <ts e="T168" id="Seg_621" n="HIAT:w" s="T167">Šoʔ</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">iam</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">ujam</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_630" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_632" n="HIAT:w" s="T170">mĭnz-</ts>
                  <nts id="Seg_633" n="HIAT:ip">)</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_636" n="HIAT:w" s="T171">mĭnzərləʔbəʔjə</ts>
                  <nts id="Seg_637" n="HIAT:ip">.</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_639" n="sc" s="T173">
               <ts e="T174" id="Seg_641" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">Üjüʔi</ts>
                  <nts id="Seg_644" n="HIAT:ip">"</nts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T191" id="Seg_647" n="sc" s="T175">
               <ts e="T181" id="Seg_649" n="HIAT:u" s="T175">
                  <nts id="Seg_650" n="HIAT:ip">"</nts>
                  <ts e="T176" id="Seg_652" n="HIAT:w" s="T175">Măn</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">tĭm</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_658" n="HIAT:w" s="T177">em</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_660" n="HIAT:ip">(</nts>
                  <ts e="T179" id="Seg_662" n="HIAT:w" s="T178">na-</ts>
                  <nts id="Seg_663" n="HIAT:ip">)</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_666" n="HIAT:w" s="T179">naga</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_669" n="HIAT:w" s="T180">amzittə</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_673" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">Pušaj</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">tüžöjdə</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_681" n="HIAT:w" s="T183">bătləj</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_684" n="HIAT:w" s="T184">i</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_687" n="HIAT:w" s="T185">mĭnzərləj</ts>
                  <nts id="Seg_688" n="HIAT:ip">,</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">kubanə</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">nuldələj</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_698" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_700" n="HIAT:w" s="T188">Dĭ</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_703" n="HIAT:w" s="T189">tüžöj</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_706" n="HIAT:w" s="T190">băʔpi</ts>
                  <nts id="Seg_707" n="HIAT:ip">.</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_709" n="sc" s="T192">
               <ts e="T196" id="Seg_711" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_713" n="HIAT:w" s="T192">Uja</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_716" n="HIAT:w" s="T193">mĭnzərbi</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_720" n="HIAT:w" s="T194">kubanə</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_723" n="HIAT:w" s="T195">embi</ts>
                  <nts id="Seg_724" n="HIAT:ip">.</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T203" id="Seg_726" n="sc" s="T197">
               <ts e="T203" id="Seg_728" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_730" n="HIAT:w" s="T197">Dĭgəttə</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_733" n="HIAT:w" s="T198">esseŋ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_736" n="HIAT:w" s="T199">kambiʔi:</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_738" n="HIAT:ip">"</nts>
                  <ts e="T201" id="Seg_740" n="HIAT:w" s="T200">Šoʔ</ts>
                  <nts id="Seg_741" n="HIAT:ip">,</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_744" n="HIAT:w" s="T201">uja</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_747" n="HIAT:w" s="T202">mĭnzərlona</ts>
                  <nts id="Seg_748" n="HIAT:ip">!</nts>
                  <nts id="Seg_749" n="HIAT:ip">"</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T205" id="Seg_751" n="sc" s="T204">
               <ts e="T205" id="Seg_753" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_755" n="HIAT:w" s="T204">Šobiʔi</ts>
                  <nts id="Seg_756" n="HIAT:ip">.</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_758" n="sc" s="T206">
               <ts e="T208" id="Seg_760" n="HIAT:u" s="T206">
                  <nts id="Seg_761" n="HIAT:ip">"</nts>
                  <ts e="T207" id="Seg_763" n="HIAT:w" s="T206">Gijen</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_766" n="HIAT:w" s="T207">abam</ts>
                  <nts id="Seg_767" n="HIAT:ip">?</nts>
                  <nts id="Seg_768" n="HIAT:ip">"</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_770" n="sc" s="T209">
               <ts e="T213" id="Seg_772" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_774" n="HIAT:w" s="T209">Davaj</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_777" n="HIAT:w" s="T210">dĭzem</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_780" n="HIAT:w" s="T211">münörzittə</ts>
                  <nts id="Seg_781" n="HIAT:ip">,</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_784" n="HIAT:w" s="T212">esseŋdə</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T222" id="Seg_787" n="sc" s="T214">
               <ts e="T222" id="Seg_789" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_791" n="HIAT:w" s="T214">A</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_794" n="HIAT:w" s="T215">tumo</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_797" n="HIAT:w" s="T216">amnolaʔbə:</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_799" n="HIAT:ip">"</nts>
                  <ts e="T218" id="Seg_801" n="HIAT:w" s="T217">Iʔ</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_804" n="HIAT:w" s="T218">dʼabəroʔ</ts>
                  <nts id="Seg_805" n="HIAT:ip">,</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_808" n="HIAT:w" s="T219">măn</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_811" n="HIAT:w" s="T220">dön</ts>
                  <nts id="Seg_812" n="HIAT:ip">,</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_815" n="HIAT:w" s="T221">šobiam</ts>
                  <nts id="Seg_816" n="HIAT:ip">!</nts>
                  <nts id="Seg_817" n="HIAT:ip">"</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_819" n="sc" s="T223">
               <ts e="T227" id="Seg_821" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_823" n="HIAT:w" s="T223">Teinen</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_826" n="HIAT:w" s="T224">bar</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_829" n="HIAT:w" s="T225">sĭre</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_832" n="HIAT:w" s="T226">saʔməlaʔbə</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_835" n="sc" s="T228">
               <ts e="T231" id="Seg_837" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_839" n="HIAT:w" s="T228">I</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_842" n="HIAT:w" s="T229">beržə</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_845" n="HIAT:w" s="T230">šonəga</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T237" id="Seg_848" n="sc" s="T232">
               <ts e="T237" id="Seg_850" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_852" n="HIAT:w" s="T232">Ugandə</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_855" n="HIAT:w" s="T233">šišəge</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_857" n="HIAT:ip">(</nts>
                  <ts e="T235" id="Seg_859" n="HIAT:w" s="T234">nʼiʔnen</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_862" n="HIAT:w" s="T235">nʼiʔ-</ts>
                  <nts id="Seg_863" n="HIAT:ip">)</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_866" n="HIAT:w" s="T236">nʼiʔnen</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_869" n="sc" s="T238">
               <ts e="T241" id="Seg_871" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_873" n="HIAT:w" s="T238">Kumen</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_876" n="HIAT:w" s="T239">kö</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_879" n="HIAT:w" s="T240">eššinə</ts>
                  <nts id="Seg_880" n="HIAT:ip">?</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_882" n="sc" s="T242">
               <ts e="T243" id="Seg_884" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_886" n="HIAT:w" s="T242">Nagur</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T249" id="Seg_889" n="sc" s="T244">
               <ts e="T249" id="Seg_891" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_893" n="HIAT:w" s="T244">Dö</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_896" n="HIAT:w" s="T245">kön</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_899" n="HIAT:w" s="T246">ugandə</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_902" n="HIAT:w" s="T247">šišəge</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_905" n="HIAT:w" s="T248">ibi</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_908" n="sc" s="T250">
               <ts e="T253" id="Seg_910" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_912" n="HIAT:w" s="T250">Kăndlaʔpiʔi</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_915" n="HIAT:w" s="T251">il</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_918" n="HIAT:w" s="T252">bar</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_921" n="sc" s="T254">
               <ts e="T255" id="Seg_923" n="HIAT:u" s="T254">
                  <nts id="Seg_924" n="HIAT:ip">(</nts>
                  <nts id="Seg_925" n="HIAT:ip">(</nts>
                  <ats e="T255" id="Seg_926" n="HIAT:non-pho" s="T254">BRK</ats>
                  <nts id="Seg_927" n="HIAT:ip">)</nts>
                  <nts id="Seg_928" n="HIAT:ip">)</nts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T259" id="Seg_931" n="sc" s="T256">
               <ts e="T259" id="Seg_933" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_935" n="HIAT:w" s="T256">Noʔ</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_938" n="HIAT:w" s="T257">jaʔpiam</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_941" n="HIAT:w" s="T258">šapkuzi</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T271" id="Seg_944" n="sc" s="T260">
               <ts e="T271" id="Seg_946" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_948" n="HIAT:w" s="T260">Dĭgəttə</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_951" n="HIAT:w" s="T261">koʔlambi</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_954" n="HIAT:w" s="T262">noʔ</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_958" n="HIAT:w" s="T263">măn</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_961" n="HIAT:w" s="T264">dĭm</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_964" n="HIAT:w" s="T265">oʔbdəbiam</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_967" n="HIAT:w" s="T266">kăpnanə</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_971" n="HIAT:w" s="T267">dĭgəttə</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_974" n="HIAT:w" s="T268">ine</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_977" n="HIAT:w" s="T269">ibiem</ts>
                  <nts id="Seg_978" n="HIAT:ip">,</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_981" n="HIAT:w" s="T270">tăžerbiam</ts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T274" id="Seg_984" n="sc" s="T272">
               <ts e="T274" id="Seg_986" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_988" n="HIAT:w" s="T272">Embiem</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_991" n="HIAT:w" s="T273">zarottə</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_994" n="sc" s="T275">
               <ts e="T276" id="Seg_996" n="HIAT:u" s="T275">
                  <nts id="Seg_997" n="HIAT:ip">(</nts>
                  <nts id="Seg_998" n="HIAT:ip">(</nts>
                  <ats e="T276" id="Seg_999" n="HIAT:non-pho" s="T275">BRK</ats>
                  <nts id="Seg_1000" n="HIAT:ip">)</nts>
                  <nts id="Seg_1001" n="HIAT:ip">)</nts>
                  <nts id="Seg_1002" n="HIAT:ip">.</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T286" id="Seg_1004" n="sc" s="T277">
               <ts e="T286" id="Seg_1006" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1008" n="HIAT:w" s="T277">Dö</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1011" n="HIAT:w" s="T278">pʼen</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1014" n="HIAT:w" s="T279">sagər</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1017" n="HIAT:w" s="T280">keʔbde</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1020" n="HIAT:w" s="T281">naga</ts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1024" n="HIAT:w" s="T282">a</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1027" n="HIAT:w" s="T283">kömə</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1030" n="HIAT:w" s="T284">keʔbde</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1033" n="HIAT:w" s="T285">ibi</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_1036" n="sc" s="T287">
               <ts e="T292" id="Seg_1038" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1040" n="HIAT:w" s="T287">Il</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1043" n="HIAT:w" s="T288">iʔgö</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1046" n="HIAT:w" s="T289">tažerbiʔi</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1049" n="HIAT:w" s="T290">maːndə</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1052" n="HIAT:w" s="T291">keʔbdeʔi</ts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T303" id="Seg_1055" n="sc" s="T293">
               <ts e="T303" id="Seg_1057" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1059" n="HIAT:w" s="T293">Sanə</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1062" n="HIAT:w" s="T294">iʔgö</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1065" n="HIAT:w" s="T295">ibi</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1068" n="HIAT:w" s="T296">bar</ts>
                  <nts id="Seg_1069" n="HIAT:ip">,</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1071" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1073" n="HIAT:w" s="T297">i-</ts>
                  <nts id="Seg_1074" n="HIAT:ip">)</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1077" n="HIAT:w" s="T298">il</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1080" n="HIAT:w" s="T299">ugandə</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1083" n="HIAT:w" s="T300">iʔgö</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1086" n="HIAT:w" s="T301">oʔbdəbiʔi</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1089" n="HIAT:w" s="T302">sanə</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_1092" n="sc" s="T304">
               <ts e="T305" id="Seg_1094" n="HIAT:u" s="T304">
                  <nts id="Seg_1095" n="HIAT:ip">(</nts>
                  <nts id="Seg_1096" n="HIAT:ip">(</nts>
                  <ats e="T305" id="Seg_1097" n="HIAT:non-pho" s="T304">BRK</ats>
                  <nts id="Seg_1098" n="HIAT:ip">)</nts>
                  <nts id="Seg_1099" n="HIAT:ip">)</nts>
                  <nts id="Seg_1100" n="HIAT:ip">.</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T311" id="Seg_1102" n="sc" s="T306">
               <ts e="T311" id="Seg_1104" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1106" n="HIAT:w" s="T306">Nagur</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1109" n="HIAT:w" s="T307">pʼe</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1112" n="HIAT:w" s="T308">nagobi</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1114" n="HIAT:ip">(</nts>
                  <ts e="T310" id="Seg_1116" n="HIAT:w" s="T309">ž</ts>
                  <nts id="Seg_1117" n="HIAT:ip">)</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1120" n="HIAT:w" s="T310">sanə</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T313" id="Seg_1123" n="sc" s="T312">
               <ts e="T313" id="Seg_1125" n="HIAT:u" s="T312">
                  <nts id="Seg_1126" n="HIAT:ip">(</nts>
                  <nts id="Seg_1127" n="HIAT:ip">(</nts>
                  <ats e="T313" id="Seg_1128" n="HIAT:non-pho" s="T312">BRK</ats>
                  <nts id="Seg_1129" n="HIAT:ip">)</nts>
                  <nts id="Seg_1130" n="HIAT:ip">)</nts>
                  <nts id="Seg_1131" n="HIAT:ip">.</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T324" id="Seg_1133" n="sc" s="T314">
               <ts e="T324" id="Seg_1135" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1137" n="HIAT:w" s="T314">Sanə</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1140" n="HIAT:w" s="T315">toʔnarzittə</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1143" n="HIAT:w" s="T316">ne</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1146" n="HIAT:w" s="T317">nado</ts>
                  <nts id="Seg_1147" n="HIAT:ip">,</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1150" n="HIAT:w" s="T318">dĭzeŋ</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1153" n="HIAT:w" s="T319">bostə</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1156" n="HIAT:w" s="T320">saʔməluʔpiʔi</ts>
                  <nts id="Seg_1157" n="HIAT:ip">,</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1160" n="HIAT:w" s="T321">a</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1163" n="HIAT:w" s="T322">il</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1166" n="HIAT:w" s="T323">oʔbdəlaʔbəʔjə</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T326" id="Seg_1169" n="sc" s="T325">
               <ts e="T326" id="Seg_1171" n="HIAT:u" s="T325">
                  <nts id="Seg_1172" n="HIAT:ip">(</nts>
                  <nts id="Seg_1173" n="HIAT:ip">(</nts>
                  <ats e="T326" id="Seg_1174" n="HIAT:non-pho" s="T325">BRK</ats>
                  <nts id="Seg_1175" n="HIAT:ip">)</nts>
                  <nts id="Seg_1176" n="HIAT:ip">)</nts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T333" id="Seg_1179" n="sc" s="T327">
               <ts e="T333" id="Seg_1181" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1183" n="HIAT:w" s="T327">Dö</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1186" n="HIAT:w" s="T328">pʼen</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1189" n="HIAT:w" s="T329">len</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1192" n="HIAT:w" s="T330">keʔbde</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1195" n="HIAT:w" s="T331">nagobi</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1198" n="HIAT:w" s="T332">tože</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T335" id="Seg_1201" n="sc" s="T334">
               <ts e="T335" id="Seg_1203" n="HIAT:u" s="T334">
                  <nts id="Seg_1204" n="HIAT:ip">(</nts>
                  <nts id="Seg_1205" n="HIAT:ip">(</nts>
                  <ats e="T335" id="Seg_1206" n="HIAT:non-pho" s="T334">BRK</ats>
                  <nts id="Seg_1207" n="HIAT:ip">)</nts>
                  <nts id="Seg_1208" n="HIAT:ip">)</nts>
                  <nts id="Seg_1209" n="HIAT:ip">.</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T342" id="Seg_1211" n="sc" s="T336">
               <ts e="T342" id="Seg_1213" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1215" n="HIAT:w" s="T336">Dö</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1218" n="HIAT:w" s="T337">kö</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1221" n="HIAT:w" s="T338">ugandə</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1224" n="HIAT:w" s="T339">budəj</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1227" n="HIAT:w" s="T340">jakše</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1230" n="HIAT:w" s="T341">ibi</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T351" id="Seg_1233" n="sc" s="T343">
               <ts e="T351" id="Seg_1235" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1237" n="HIAT:w" s="T343">I</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1240" n="HIAT:w" s="T344">toltanoʔ</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1243" n="HIAT:w" s="T345">jakše</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1246" n="HIAT:w" s="T346">ibi</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1250" n="HIAT:w" s="T347">iʔgö</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1253" n="HIAT:w" s="T348">özerbi</ts>
                  <nts id="Seg_1254" n="HIAT:ip">,</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1257" n="HIAT:w" s="T349">urgo</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1260" n="HIAT:w" s="T350">ibi</ts>
                  <nts id="Seg_1261" n="HIAT:ip">.</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_1263" n="sc" s="T352">
               <ts e="T353" id="Seg_1265" n="HIAT:u" s="T352">
                  <nts id="Seg_1266" n="HIAT:ip">(</nts>
                  <nts id="Seg_1267" n="HIAT:ip">(</nts>
                  <ats e="T353" id="Seg_1268" n="HIAT:non-pho" s="T352">BRK</ats>
                  <nts id="Seg_1269" n="HIAT:ip">)</nts>
                  <nts id="Seg_1270" n="HIAT:ip">)</nts>
                  <nts id="Seg_1271" n="HIAT:ip">.</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T360" id="Seg_1273" n="sc" s="T354">
               <ts e="T360" id="Seg_1275" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1277" n="HIAT:w" s="T354">Sĭre</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1280" n="HIAT:w" s="T355">ipek</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1282" n="HIAT:ip">(</nts>
                  <ts e="T357" id="Seg_1284" n="HIAT:w" s="T356">pür-</ts>
                  <nts id="Seg_1285" n="HIAT:ip">)</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1288" n="HIAT:w" s="T357">pürlieʔi</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1291" n="HIAT:w" s="T358">il</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1294" n="HIAT:w" s="T359">ugandə</ts>
                  <nts id="Seg_1295" n="HIAT:ip">.</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_1297" n="sc" s="T361">
               <ts e="T365" id="Seg_1299" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1301" n="HIAT:w" s="T361">Köbergən</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1304" n="HIAT:w" s="T362">bar</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1307" n="HIAT:w" s="T363">külambi</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1310" n="HIAT:w" s="T364">ăgărotkən</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T367" id="Seg_1313" n="sc" s="T366">
               <ts e="T367" id="Seg_1315" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1317" n="HIAT:w" s="T366">A</ts>
                  <nts id="Seg_1318" n="HIAT:ip">…</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T369" id="Seg_1320" n="sc" s="T368">
               <ts e="T369" id="Seg_1322" n="HIAT:u" s="T368">
                  <nts id="Seg_1323" n="HIAT:ip">(</nts>
                  <nts id="Seg_1324" n="HIAT:ip">(</nts>
                  <ats e="T369" id="Seg_1325" n="HIAT:non-pho" s="T368">BRK</ats>
                  <nts id="Seg_1326" n="HIAT:ip">)</nts>
                  <nts id="Seg_1327" n="HIAT:ip">)</nts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T375" id="Seg_1330" n="sc" s="T370">
               <ts e="T375" id="Seg_1332" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1334" n="HIAT:w" s="T370">A</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1337" n="HIAT:w" s="T371">dʼijegən</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1340" n="HIAT:w" s="T372">köbergən</ts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1343" n="HIAT:w" s="T373">jakše</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1346" n="HIAT:w" s="T374">ibi</ts>
                  <nts id="Seg_1347" n="HIAT:ip">.</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T377" id="Seg_1349" n="sc" s="T376">
               <ts e="T377" id="Seg_1351" n="HIAT:u" s="T376">
                  <nts id="Seg_1352" n="HIAT:ip">(</nts>
                  <nts id="Seg_1353" n="HIAT:ip">(</nts>
                  <ats e="T377" id="Seg_1354" n="HIAT:non-pho" s="T376">BRK</ats>
                  <nts id="Seg_1355" n="HIAT:ip">)</nts>
                  <nts id="Seg_1356" n="HIAT:ip">)</nts>
                  <nts id="Seg_1357" n="HIAT:ip">.</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T385" id="Seg_1359" n="sc" s="T378">
               <ts e="T385" id="Seg_1361" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1363" n="HIAT:w" s="T378">Măn</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1365" n="HIAT:ip">(</nts>
                  <ts e="T380" id="Seg_1367" n="HIAT:w" s="T379">neg-</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1370" n="HIAT:w" s="T380">nĭg-</ts>
                  <nts id="Seg_1371" n="HIAT:ip">)</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1374" n="HIAT:w" s="T381">nĭŋgəbiem</ts>
                  <nts id="Seg_1375" n="HIAT:ip">,</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1378" n="HIAT:w" s="T382">dʼagarbiam</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1381" n="HIAT:w" s="T383">i</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1384" n="HIAT:w" s="T384">koʔlaʔpi</ts>
                  <nts id="Seg_1385" n="HIAT:ip">.</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T391" id="Seg_1387" n="sc" s="T386">
               <ts e="T391" id="Seg_1389" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1391" n="HIAT:w" s="T386">A</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1394" n="HIAT:w" s="T387">tüj</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1397" n="HIAT:w" s="T388">mĭnzərliem</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1400" n="HIAT:w" s="T389">ujazi</ts>
                  <nts id="Seg_1401" n="HIAT:ip">,</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1404" n="HIAT:w" s="T390">toltanoʔsi</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T393" id="Seg_1407" n="sc" s="T392">
               <ts e="T393" id="Seg_1409" n="HIAT:u" s="T392">
                  <nts id="Seg_1410" n="HIAT:ip">(</nts>
                  <nts id="Seg_1411" n="HIAT:ip">(</nts>
                  <ats e="T393" id="Seg_1412" n="HIAT:non-pho" s="T392">BRK</ats>
                  <nts id="Seg_1413" n="HIAT:ip">)</nts>
                  <nts id="Seg_1414" n="HIAT:ip">)</nts>
                  <nts id="Seg_1415" n="HIAT:ip">.</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T407" id="Seg_1417" n="sc" s="T394">
               <ts e="T397" id="Seg_1419" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1421" n="HIAT:w" s="T394">А</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1424" n="HIAT:w" s="T395">говорю</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1427" n="HIAT:w" s="T396">следом</ts>
                  <nts id="Seg_1428" n="HIAT:ip">.</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1431" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1433" n="HIAT:w" s="T397">Nuzaŋgən</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1435" n="HIAT:ip">(</nts>
                  <ts e="T399" id="Seg_1437" n="HIAT:w" s="T398">oʔb</ts>
                  <nts id="Seg_1438" n="HIAT:ip">)</nts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1441" n="HIAT:w" s="T399">kuza</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1444" n="HIAT:w" s="T400">külambi</ts>
                  <nts id="Seg_1445" n="HIAT:ip">.</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1448" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1450" n="HIAT:w" s="T401">Dĭzeŋ</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1453" n="HIAT:w" s="T402">bar</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1456" n="HIAT:w" s="T403">tĭlbiʔi</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1459" n="HIAT:w" s="T404">dʼü</ts>
                  <nts id="Seg_1460" n="HIAT:ip">,</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1463" n="HIAT:w" s="T405">dĭgəttə</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1466" n="HIAT:w" s="T406">abiʔi</ts>
                  <nts id="Seg_1467" n="HIAT:ip">…</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T409" id="Seg_1469" n="sc" s="T408">
               <ts e="T409" id="Seg_1471" n="HIAT:u" s="T408">
                  <nts id="Seg_1472" n="HIAT:ip">(</nts>
                  <nts id="Seg_1473" n="HIAT:ip">(</nts>
                  <ats e="T409" id="Seg_1474" n="HIAT:non-pho" s="T408">BRK</ats>
                  <nts id="Seg_1475" n="HIAT:ip">)</nts>
                  <nts id="Seg_1476" n="HIAT:ip">)</nts>
                  <nts id="Seg_1477" n="HIAT:ip">.</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T418" id="Seg_1479" n="sc" s="T410">
               <ts e="T413" id="Seg_1481" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1483" n="HIAT:w" s="T410">Dĭgəttə</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1486" n="HIAT:w" s="T411">maʔ</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1489" n="HIAT:w" s="T412">abiʔi</ts>
                  <nts id="Seg_1490" n="HIAT:ip">.</nts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1493" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1495" n="HIAT:w" s="T413">Krospa</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1498" n="HIAT:w" s="T414">abiʔi</ts>
                  <nts id="Seg_1499" n="HIAT:ip">,</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1502" n="HIAT:w" s="T415">maʔtə</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1505" n="HIAT:w" s="T416">dĭm</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1508" n="HIAT:w" s="T417">embiʔi</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T427" id="Seg_1511" n="sc" s="T419">
               <ts e="T427" id="Seg_1513" n="HIAT:u" s="T419">
                  <nts id="Seg_1514" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_1516" n="HIAT:w" s="T419">Dĭ-</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1519" n="HIAT:w" s="T420">dĭ-</ts>
                  <nts id="Seg_1520" n="HIAT:ip">)</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1523" n="HIAT:w" s="T421">Dĭ</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1526" n="HIAT:w" s="T422">dĭʔnə</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1529" n="HIAT:w" s="T423">kaŋza</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1532" n="HIAT:w" s="T424">ambiʔi</ts>
                  <nts id="Seg_1533" n="HIAT:ip">,</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1536" n="HIAT:w" s="T425">taŋgu</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1539" n="HIAT:w" s="T426">ambiʔi</ts>
                  <nts id="Seg_1540" n="HIAT:ip">.</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T431" id="Seg_1542" n="sc" s="T428">
               <ts e="T431" id="Seg_1544" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1546" n="HIAT:w" s="T428">Multuk</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1549" n="HIAT:w" s="T429">ambiʔi</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1552" n="HIAT:w" s="T430">bar</ts>
                  <nts id="Seg_1553" n="HIAT:ip">.</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T435" id="Seg_1555" n="sc" s="T432">
               <ts e="T435" id="Seg_1557" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1559" n="HIAT:w" s="T432">Aspaʔ</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1561" n="HIAT:ip">(</nts>
                  <ts e="T434" id="Seg_1563" n="HIAT:w" s="T433">ambiʔi=</ts>
                  <nts id="Seg_1564" n="HIAT:ip">)</nts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1567" n="HIAT:w" s="T434">embiʔi</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T441" id="Seg_1570" n="sc" s="T436">
               <ts e="T441" id="Seg_1572" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1574" n="HIAT:w" s="T436">Dĭ</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1577" n="HIAT:w" s="T437">dĭn</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1580" n="HIAT:w" s="T438">mĭnzərləj</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1583" n="HIAT:w" s="T439">i</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1586" n="HIAT:w" s="T440">amorləj</ts>
                  <nts id="Seg_1587" n="HIAT:ip">.</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T443" id="Seg_1589" n="sc" s="T442">
               <ts e="T443" id="Seg_1591" n="HIAT:u" s="T442">
                  <nts id="Seg_1592" n="HIAT:ip">(</nts>
                  <nts id="Seg_1593" n="HIAT:ip">(</nts>
                  <ats e="T443" id="Seg_1594" n="HIAT:non-pho" s="T442">BRK</ats>
                  <nts id="Seg_1595" n="HIAT:ip">)</nts>
                  <nts id="Seg_1596" n="HIAT:ip">)</nts>
                  <nts id="Seg_1597" n="HIAT:ip">.</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T454" id="Seg_1599" n="sc" s="T444">
               <ts e="T454" id="Seg_1601" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1603" n="HIAT:w" s="T444">Dĭgəttə</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1606" n="HIAT:w" s="T445">dĭ</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1609" n="HIAT:w" s="T446">kuzam</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1612" n="HIAT:w" s="T447">kumbiʔi</ts>
                  <nts id="Seg_1613" n="HIAT:ip">,</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1615" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_1617" n="HIAT:w" s="T448">dʼü-</ts>
                  <nts id="Seg_1618" n="HIAT:ip">)</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1621" n="HIAT:w" s="T449">dʼünə</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1624" n="HIAT:w" s="T450">embiʔi</ts>
                  <nts id="Seg_1625" n="HIAT:ip">,</nts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1628" n="HIAT:w" s="T451">dʼüzi</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1631" n="HIAT:w" s="T452">bar</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1634" n="HIAT:w" s="T453">kămnəbiʔi</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T458" id="Seg_1637" n="sc" s="T455">
               <ts e="T458" id="Seg_1639" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1641" n="HIAT:w" s="T455">Dĭgəttə</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1644" n="HIAT:w" s="T456">maːndə</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1647" n="HIAT:w" s="T457">šobiʔi</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T465" id="Seg_1650" n="sc" s="T459">
               <ts e="T465" id="Seg_1652" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1654" n="HIAT:w" s="T459">Nüdʼin</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1657" n="HIAT:w" s="T460">bar</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1660" n="HIAT:w" s="T461">men</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1663" n="HIAT:w" s="T462">sarbiʔi</ts>
                  <nts id="Seg_1664" n="HIAT:ip">,</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1667" n="HIAT:w" s="T463">da</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1670" n="HIAT:w" s="T464">ine</ts>
                  <nts id="Seg_1671" n="HIAT:ip">.</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T468" id="Seg_1673" n="sc" s="T466">
               <ts e="T468" id="Seg_1675" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1677" n="HIAT:w" s="T466">Dĭgəttə</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1680" n="HIAT:w" s="T467">kanbiʔi</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T474" id="Seg_1683" n="sc" s="T469">
               <ts e="T474" id="Seg_1685" n="HIAT:u" s="T469">
                  <nts id="Seg_1686" n="HIAT:ip">"</nts>
                  <ts e="T470" id="Seg_1688" n="HIAT:w" s="T469">Oj</ts>
                  <nts id="Seg_1689" n="HIAT:ip">,</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1692" n="HIAT:w" s="T470">šonəga</ts>
                  <nts id="Seg_1693" n="HIAT:ip">"</nts>
                  <nts id="Seg_1694" n="HIAT:ip">,</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1696" n="HIAT:ip">—</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1699" n="HIAT:w" s="T471">nuʔmələʔbəʔjə</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1702" n="HIAT:w" s="T472">turanə</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1705" n="HIAT:w" s="T473">bar</ts>
                  <nts id="Seg_1706" n="HIAT:ip">.</nts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T478" id="Seg_1708" n="sc" s="T475">
               <ts e="T478" id="Seg_1710" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1712" n="HIAT:w" s="T475">Šakku</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1715" n="HIAT:w" s="T476">ajigən</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1718" n="HIAT:w" s="T477">enləʔi</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1721" n="sc" s="T479">
               <ts e="T480" id="Seg_1723" n="HIAT:u" s="T479">
                  <nts id="Seg_1724" n="HIAT:ip">(</nts>
                  <nts id="Seg_1725" n="HIAT:ip">(</nts>
                  <ats e="T480" id="Seg_1726" n="HIAT:non-pho" s="T479">BRK</ats>
                  <nts id="Seg_1727" n="HIAT:ip">)</nts>
                  <nts id="Seg_1728" n="HIAT:ip">)</nts>
                  <nts id="Seg_1729" n="HIAT:ip">.</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_1731" n="sc" s="T481">
               <ts e="T492" id="Seg_1733" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1735" n="HIAT:w" s="T481">Men</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1738" n="HIAT:w" s="T482">sarbiʔi</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1741" n="HIAT:w" s="T483">sagər</ts>
                  <nts id="Seg_1742" n="HIAT:ip">,</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1745" n="HIAT:w" s="T484">i</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485.tx.1" id="Seg_1748" n="HIAT:w" s="T485">simat</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1751" n="HIAT:w" s="T485.tx.1">-</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1754" n="HIAT:w" s="T486">ob</ts>
                  <nts id="Seg_1755" n="HIAT:ip">,</nts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1758" n="HIAT:w" s="T487">šide</ts>
                  <nts id="Seg_1759" n="HIAT:ip">,</nts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1762" n="HIAT:w" s="T488">nagur</ts>
                  <nts id="Seg_1763" n="HIAT:ip">,</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489.tx.1" id="Seg_1766" n="HIAT:w" s="T489">teʔtə</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1769" n="HIAT:w" s="T489.tx.1">-</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1772" n="HIAT:w" s="T490">teʔtə</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1775" n="HIAT:w" s="T491">simat</ts>
                  <nts id="Seg_1776" n="HIAT:ip">.</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1778" n="sc" s="T493">
               <ts e="T494" id="Seg_1780" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1782" n="HIAT:w" s="T493">Четырехглазый</ts>
                  <nts id="Seg_1783" n="HIAT:ip">.</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T501" id="Seg_1785" n="sc" s="T495">
               <ts e="T501" id="Seg_1787" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1789" n="HIAT:w" s="T495">Šapku</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1792" n="HIAT:w" s="T496">ambiʔi</ts>
                  <nts id="Seg_1793" n="HIAT:ip">,</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1796" n="HIAT:w" s="T497">štobɨ</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1799" n="HIAT:w" s="T498">penze</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1801" n="HIAT:ip">(</nts>
                  <ts e="T500" id="Seg_1803" n="HIAT:w" s="T499">s-</ts>
                  <nts id="Seg_1804" n="HIAT:ip">)</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1807" n="HIAT:w" s="T500">dʼagarluʔpi</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T503" id="Seg_1810" n="sc" s="T502">
               <ts e="T503" id="Seg_1812" n="HIAT:u" s="T502">
                  <nts id="Seg_1813" n="HIAT:ip">(</nts>
                  <nts id="Seg_1814" n="HIAT:ip">(</nts>
                  <ats e="T503" id="Seg_1815" n="HIAT:non-pho" s="T502">BRK</ats>
                  <nts id="Seg_1816" n="HIAT:ip">)</nts>
                  <nts id="Seg_1817" n="HIAT:ip">)</nts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T514" id="Seg_1820" n="sc" s="T504">
               <ts e="T514" id="Seg_1822" n="HIAT:u" s="T504">
                  <nts id="Seg_1823" n="HIAT:ip">(</nts>
                  <ts e="T505" id="Seg_1825" n="HIAT:w" s="T504">Onʼiʔ</ts>
                  <nts id="Seg_1826" n="HIAT:ip">)</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1829" n="HIAT:w" s="T505">Onʼiʔ</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1832" n="HIAT:w" s="T506">kuza</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1834" n="HIAT:ip">(</nts>
                  <ts e="T508" id="Seg_1836" n="HIAT:w" s="T507">поселенец</ts>
                  <nts id="Seg_1837" n="HIAT:ip">)</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1840" n="HIAT:w" s="T508">miʔnʼibeʔ</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1843" n="HIAT:w" s="T509">ĭzembi</ts>
                  <nts id="Seg_1844" n="HIAT:ip">,</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1847" n="HIAT:w" s="T510">ĭzembi</ts>
                  <nts id="Seg_1848" n="HIAT:ip">,</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1851" n="HIAT:w" s="T511">dĭgəttə</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1853" n="HIAT:ip">(</nts>
                  <ts e="T513" id="Seg_1855" n="HIAT:w" s="T512">ku-</ts>
                  <nts id="Seg_1856" n="HIAT:ip">)</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1859" n="HIAT:w" s="T513">külambi</ts>
                  <nts id="Seg_1860" n="HIAT:ip">.</nts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T520" id="Seg_1862" n="sc" s="T515">
               <ts e="T520" id="Seg_1864" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1866" n="HIAT:w" s="T515">A</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1869" n="HIAT:w" s="T516">miʔ</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1871" n="HIAT:ip">(</nts>
                  <ts e="T518" id="Seg_1873" n="HIAT:w" s="T517">ka-</ts>
                  <nts id="Seg_1874" n="HIAT:ip">)</nts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1877" n="HIAT:w" s="T518">kanbibaʔ</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_1880" n="HIAT:w" s="T519">Kazan</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1883" n="HIAT:w" s="T1139">turanə</ts>
                  <nts id="Seg_1884" n="HIAT:ip">.</nts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T528" id="Seg_1886" n="sc" s="T521">
               <ts e="T528" id="Seg_1888" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_1890" n="HIAT:w" s="T521">Sazən</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1892" n="HIAT:ip">(</nts>
                  <ts e="T523" id="Seg_1894" n="HIAT:w" s="T522">udandə</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1897" n="HIAT:w" s="T523">uzan-</ts>
                  <nts id="Seg_1898" n="HIAT:ip">)</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1901" n="HIAT:w" s="T524">udandə</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1904" n="HIAT:w" s="T525">izittə</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1907" n="HIAT:w" s="T526">i</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1910" n="HIAT:w" s="T527">ulundə</ts>
                  <nts id="Seg_1911" n="HIAT:ip">.</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_1913" n="sc" s="T529">
               <ts e="T534" id="Seg_1915" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_1917" n="HIAT:w" s="T529">Dĭgəttə</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1920" n="HIAT:w" s="T530">abəs</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1923" n="HIAT:w" s="T531">miʔnʼibeʔ</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1926" n="HIAT:w" s="T532">ej</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1929" n="HIAT:w" s="T533">mĭbi</ts>
                  <nts id="Seg_1930" n="HIAT:ip">.</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_1933" n="HIAT:u" s="T534">
                  <nts id="Seg_1934" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_1936" n="HIAT:w" s="T534">M-</ts>
                  <nts id="Seg_1937" n="HIAT:ip">)</nts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1940" n="HIAT:w" s="T535">Miʔ</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1943" n="HIAT:w" s="T536">ara</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1946" n="HIAT:w" s="T537">ibibeʔ</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1949" n="HIAT:w" s="T538">четверть</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1952" n="HIAT:w" s="T539">i</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1955" n="HIAT:w" s="T540">maʔnʼi</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1958" n="HIAT:w" s="T541">šobibaʔ</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T552" id="Seg_1961" n="sc" s="T543">
               <ts e="T552" id="Seg_1963" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1965" n="HIAT:w" s="T543">Dĭgəttə</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1968" n="HIAT:w" s="T544">iam</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1971" n="HIAT:w" s="T545">abam</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1973" n="HIAT:ip">(</nts>
                  <ts e="T547" id="Seg_1975" n="HIAT:w" s="T546">ibi-</ts>
                  <nts id="Seg_1976" n="HIAT:ip">)</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1979" n="HIAT:w" s="T547">ibiʔi</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1982" n="HIAT:w" s="T548">dĭ</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1985" n="HIAT:w" s="T549">ara</ts>
                  <nts id="Seg_1986" n="HIAT:ip">,</nts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_1989" n="HIAT:w" s="T550">kalla</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1992" n="HIAT:w" s="T1137">dʼürbiʔi</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1995" n="HIAT:w" s="T551">bĭʔsittə</ts>
                  <nts id="Seg_1996" n="HIAT:ip">.</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T561" id="Seg_1998" n="sc" s="T553">
               <ts e="T561" id="Seg_2000" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2002" n="HIAT:w" s="T553">A</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2005" n="HIAT:w" s="T554">miʔ</ts>
                  <nts id="Seg_2006" n="HIAT:ip">,</nts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2009" n="HIAT:w" s="T555">ešseŋ</ts>
                  <nts id="Seg_2010" n="HIAT:ip">,</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2013" n="HIAT:w" s="T556">miʔ</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2016" n="HIAT:w" s="T557">tože</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2019" n="HIAT:w" s="T558">ej</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2022" n="HIAT:w" s="T559">maləm</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2025" n="HIAT:w" s="T560">maʔnə</ts>
                  <nts id="Seg_2026" n="HIAT:ip">.</nts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T564" id="Seg_2028" n="sc" s="T562">
               <ts e="T564" id="Seg_2030" n="HIAT:u" s="T562">
                  <nts id="Seg_2031" n="HIAT:ip">"</nts>
                  <nts id="Seg_2032" n="HIAT:ip">(</nts>
                  <ts e="T562.tx.1" id="Seg_2034" n="HIAT:w" s="T562">Kanu-</ts>
                  <nts id="Seg_2035" n="HIAT:ip">)</nts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2037" n="HIAT:ip">(</nts>
                  <nts id="Seg_2038" n="HIAT:ip">(</nts>
                  <ats e="T563" id="Seg_2039" n="HIAT:non-pho" s="T562.tx.1">NOISE</ats>
                  <nts id="Seg_2040" n="HIAT:ip">)</nts>
                  <nts id="Seg_2041" n="HIAT:ip">)</nts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2044" n="HIAT:w" s="T563">Kanžebəj</ts>
                  <nts id="Seg_2045" n="HIAT:ip">!</nts>
                  <nts id="Seg_2046" n="HIAT:ip">"</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_2048" n="sc" s="T565">
               <ts e="T570" id="Seg_2050" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2052" n="HIAT:w" s="T565">A</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2055" n="HIAT:w" s="T566">măn</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2058" n="HIAT:w" s="T567">mămbiam:</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2060" n="HIAT:ip">"</nts>
                  <ts e="T569" id="Seg_2062" n="HIAT:w" s="T568">Iʔbəʔ</ts>
                  <nts id="Seg_2063" n="HIAT:ip">,</nts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2066" n="HIAT:w" s="T569">kunolaʔ</ts>
                  <nts id="Seg_2067" n="HIAT:ip">.</nts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T575" id="Seg_2069" n="sc" s="T571">
               <ts e="T575" id="Seg_2071" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2073" n="HIAT:w" s="T571">A</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2076" n="HIAT:w" s="T572">măn</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2079" n="HIAT:w" s="T573">iʔbələm</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2082" n="HIAT:w" s="T574">šiʔnʼileʔ</ts>
                  <nts id="Seg_2083" n="HIAT:ip">.</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T582" id="Seg_2085" n="sc" s="T576">
               <ts e="T582" id="Seg_2087" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2089" n="HIAT:w" s="T576">Kamən</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2092" n="HIAT:w" s="T577">măna</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2095" n="HIAT:w" s="T578">kabarləj</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2099" n="HIAT:w" s="T579">a</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2102" n="HIAT:w" s="T580">šiʔ</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2105" n="HIAT:w" s="T581">uʔmeluʔpileʔ</ts>
                  <nts id="Seg_2106" n="HIAT:ip">.</nts>
                  <nts id="Seg_2107" n="HIAT:ip">"</nts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_2109" n="sc" s="T583">
               <ts e="T586" id="Seg_2111" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2113" n="HIAT:w" s="T583">Dĭgəttə</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2116" n="HIAT:w" s="T584">erten</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2119" n="HIAT:w" s="T585">uʔbdəbiam</ts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T596" id="Seg_2122" n="sc" s="T587">
               <ts e="T596" id="Seg_2124" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2126" n="HIAT:w" s="T587">Ot</ts>
                  <nts id="Seg_2127" n="HIAT:ip">,</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2130" n="HIAT:w" s="T588">kuiol</ts>
                  <nts id="Seg_2131" n="HIAT:ip">,</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2134" n="HIAT:w" s="T589">dĭ</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2137" n="HIAT:w" s="T590">iʔbəlaʔbə</ts>
                  <nts id="Seg_2138" n="HIAT:ip">,</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2140" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2142" n="HIAT:w" s="T591">i</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2145" n="HIAT:w" s="T592">m-</ts>
                  <nts id="Seg_2146" n="HIAT:ip">)</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2149" n="HIAT:w" s="T593">i</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2152" n="HIAT:w" s="T594">miʔ</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2155" n="HIAT:w" s="T595">kunolbibaʔ</ts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T598" id="Seg_2158" n="sc" s="T597">
               <ts e="T598" id="Seg_2160" n="HIAT:u" s="T597">
                  <nts id="Seg_2161" n="HIAT:ip">(</nts>
                  <nts id="Seg_2162" n="HIAT:ip">(</nts>
                  <ats e="T598" id="Seg_2163" n="HIAT:non-pho" s="T597">BRK</ats>
                  <nts id="Seg_2164" n="HIAT:ip">)</nts>
                  <nts id="Seg_2165" n="HIAT:ip">)</nts>
                  <nts id="Seg_2166" n="HIAT:ip">.</nts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T604" id="Seg_2168" n="sc" s="T599">
               <ts e="T604" id="Seg_2170" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2172" n="HIAT:w" s="T599">Dĭgəttə</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2175" n="HIAT:w" s="T600">dĭ</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2178" n="HIAT:w" s="T601">külambi</ts>
                  <nts id="Seg_2179" n="HIAT:ip">,</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2182" n="HIAT:w" s="T602">dĭm</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2185" n="HIAT:w" s="T603">băzəbiʔi</ts>
                  <nts id="Seg_2186" n="HIAT:ip">.</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T609" id="Seg_2188" n="sc" s="T605">
               <ts e="T609" id="Seg_2190" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2192" n="HIAT:w" s="T605">Kujnek</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2195" n="HIAT:w" s="T606">šerbiʔi</ts>
                  <nts id="Seg_2196" n="HIAT:ip">,</nts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2199" n="HIAT:w" s="T607">piʔme</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2202" n="HIAT:w" s="T608">šerbiʔi</ts>
                  <nts id="Seg_2203" n="HIAT:ip">.</nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T613" id="Seg_2205" n="sc" s="T610">
               <ts e="T613" id="Seg_2207" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2209" n="HIAT:w" s="T610">Jamaʔi</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2211" n="HIAT:ip">(</nts>
                  <ts e="T612" id="Seg_2213" n="HIAT:w" s="T611">šoʔpiʔi=</ts>
                  <nts id="Seg_2214" n="HIAT:ip">)</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2217" n="HIAT:w" s="T612">šerbiʔi</ts>
                  <nts id="Seg_2218" n="HIAT:ip">.</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T616" id="Seg_2220" n="sc" s="T614">
               <ts e="T616" id="Seg_2222" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2224" n="HIAT:w" s="T614">Dĭgəttə</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2227" n="HIAT:w" s="T615">kombiʔi</ts>
                  <nts id="Seg_2228" n="HIAT:ip">.</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T621" id="Seg_2230" n="sc" s="T617">
               <ts e="T621" id="Seg_2232" n="HIAT:u" s="T617">
                  <nts id="Seg_2233" n="HIAT:ip">(</nts>
                  <ts e="T618" id="Seg_2235" n="HIAT:w" s="T617">Dʼo-</ts>
                  <nts id="Seg_2236" n="HIAT:ip">)</nts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2239" n="HIAT:w" s="T618">D'üzi</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2242" n="HIAT:w" s="T619">kămbiʔi</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2245" n="HIAT:w" s="T620">dĭm</ts>
                  <nts id="Seg_2246" n="HIAT:ip">.</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T631" id="Seg_2248" n="sc" s="T622">
               <ts e="T625" id="Seg_2250" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2252" n="HIAT:w" s="T622">Šobiʔi</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2255" n="HIAT:w" s="T623">maʔndə</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2257" n="HIAT:ip">(</nts>
                  <ts e="T625" id="Seg_2259" n="HIAT:w" s="T624">na-</ts>
                  <nts id="Seg_2260" n="HIAT:ip">)</nts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2264" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2266" n="HIAT:w" s="T625">Ara</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2269" n="HIAT:w" s="T626">biʔpiʔi</ts>
                  <nts id="Seg_2270" n="HIAT:ip">,</nts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2273" n="HIAT:w" s="T627">ipek</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2276" n="HIAT:w" s="T628">amorbiʔi</ts>
                  <nts id="Seg_2277" n="HIAT:ip">,</nts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2280" n="HIAT:w" s="T629">uja</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2283" n="HIAT:w" s="T630">ambiʔi</ts>
                  <nts id="Seg_2284" n="HIAT:ip">.</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T635" id="Seg_2286" n="sc" s="T632">
               <ts e="T635" id="Seg_2288" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2290" n="HIAT:w" s="T632">I</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2293" n="HIAT:w" s="T633">maʔndə</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_2296" n="HIAT:w" s="T634">kalla</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2299" n="HIAT:w" s="T1138">dʼürbiʔi</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T637" id="Seg_2302" n="sc" s="T636">
               <ts e="T637" id="Seg_2304" n="HIAT:u" s="T636">
                  <nts id="Seg_2305" n="HIAT:ip">(</nts>
                  <nts id="Seg_2306" n="HIAT:ip">(</nts>
                  <ats e="T637" id="Seg_2307" n="HIAT:non-pho" s="T636">BRK</ats>
                  <nts id="Seg_2308" n="HIAT:ip">)</nts>
                  <nts id="Seg_2309" n="HIAT:ip">)</nts>
                  <nts id="Seg_2310" n="HIAT:ip">.</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T644" id="Seg_2312" n="sc" s="T638">
               <ts e="T644" id="Seg_2314" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2316" n="HIAT:w" s="T638">Măn</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2319" n="HIAT:w" s="T639">onʼiʔ</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2322" n="HIAT:w" s="T640">raz</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2324" n="HIAT:ip">(</nts>
                  <ts e="T642" id="Seg_2326" n="HIAT:w" s="T641">šo-</ts>
                  <nts id="Seg_2327" n="HIAT:ip">)</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2330" n="HIAT:w" s="T642">šobiam</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_2333" n="HIAT:w" s="T643">Kazan</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2336" n="HIAT:w" s="T1140">turagən</ts>
                  <nts id="Seg_2337" n="HIAT:ip">.</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T650" id="Seg_2339" n="sc" s="T645">
               <ts e="T650" id="Seg_2341" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2343" n="HIAT:w" s="T645">Šobiam</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2346" n="HIAT:w" s="T646">Permʼakovăn</ts>
                  <nts id="Seg_2347" n="HIAT:ip">,</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2350" n="HIAT:w" s="T647">dĭgəttə</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2353" n="HIAT:w" s="T648">maʔndə</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2356" n="HIAT:w" s="T649">šobiam</ts>
                  <nts id="Seg_2357" n="HIAT:ip">.</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T657" id="Seg_2359" n="sc" s="T651">
               <ts e="T657" id="Seg_2361" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2363" n="HIAT:w" s="T651">Tʼerməngən</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2366" n="HIAT:w" s="T652">dĭn</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2369" n="HIAT:w" s="T653">ildəm</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2372" n="HIAT:w" s="T654">šindidə</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2374" n="HIAT:ip">(</nts>
                  <ts e="T656" id="Seg_2376" n="HIAT:w" s="T655">perluʔ-</ts>
                  <nts id="Seg_2377" n="HIAT:ip">)</nts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2379" n="HIAT:ip">(</nts>
                  <ts e="T657" id="Seg_2381" n="HIAT:w" s="T656">perluʔpiʔi</ts>
                  <nts id="Seg_2382" n="HIAT:ip">)</nts>
                  <nts id="Seg_2383" n="HIAT:ip">.</nts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_2385" n="sc" s="T658">
               <ts e="T669" id="Seg_2387" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2389" n="HIAT:w" s="T658">A</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2392" n="HIAT:w" s="T659">măn</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2395" n="HIAT:w" s="T660">šobiam</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2398" n="HIAT:w" s="T661">da</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2400" n="HIAT:ip">(</nts>
                  <ts e="T663" id="Seg_2402" n="HIAT:w" s="T662">muzuruk-</ts>
                  <nts id="Seg_2403" n="HIAT:ip">)</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2406" n="HIAT:w" s="T663">dĭgəttə</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2409" n="HIAT:w" s="T664">toʔnarləm</ts>
                  <nts id="Seg_2410" n="HIAT:ip">,</nts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2413" n="HIAT:w" s="T665">bar</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2416" n="HIAT:w" s="T666">aktʼa</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2419" n="HIAT:w" s="T667">iʔgö</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2422" n="HIAT:w" s="T668">moləj</ts>
                  <nts id="Seg_2423" n="HIAT:ip">.</nts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T671" id="Seg_2425" n="sc" s="T670">
               <ts e="T671" id="Seg_2427" n="HIAT:u" s="T670">
                  <nts id="Seg_2428" n="HIAT:ip">(</nts>
                  <nts id="Seg_2429" n="HIAT:ip">(</nts>
                  <ats e="T671" id="Seg_2430" n="HIAT:non-pho" s="T670">BRK</ats>
                  <nts id="Seg_2431" n="HIAT:ip">)</nts>
                  <nts id="Seg_2432" n="HIAT:ip">)</nts>
                  <nts id="Seg_2433" n="HIAT:ip">.</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T678" id="Seg_2435" n="sc" s="T672">
               <ts e="T678" id="Seg_2437" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2439" n="HIAT:w" s="T672">Šobiam</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2441" n="HIAT:ip">(</nts>
                  <ts e="T674" id="Seg_2443" n="HIAT:w" s="T673">maʔ-</ts>
                  <nts id="Seg_2444" n="HIAT:ip">)</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2447" n="HIAT:w" s="T674">maʔndə</ts>
                  <nts id="Seg_2448" n="HIAT:ip">,</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2451" n="HIAT:w" s="T675">šindində</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2454" n="HIAT:w" s="T676">ej</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2457" n="HIAT:w" s="T677">kubiom</ts>
                  <nts id="Seg_2458" n="HIAT:ip">.</nts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T680" id="Seg_2460" n="sc" s="T679">
               <ts e="T680" id="Seg_2462" n="HIAT:u" s="T679">
                  <nts id="Seg_2463" n="HIAT:ip">(</nts>
                  <nts id="Seg_2464" n="HIAT:ip">(</nts>
                  <ats e="T680" id="Seg_2465" n="HIAT:non-pho" s="T679">BRK</ats>
                  <nts id="Seg_2466" n="HIAT:ip">)</nts>
                  <nts id="Seg_2467" n="HIAT:ip">)</nts>
                  <nts id="Seg_2468" n="HIAT:ip">.</nts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T698" id="Seg_2470" n="sc" s="T681">
               <ts e="T690" id="Seg_2472" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2474" n="HIAT:w" s="T681">Măn</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2477" n="HIAT:w" s="T682">ulum</ts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2480" n="HIAT:w" s="T683">ĭzemnie</ts>
                  <nts id="Seg_2481" n="HIAT:ip">,</nts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2484" n="HIAT:w" s="T684">simam</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2487" n="HIAT:w" s="T685">ĭzemnie</ts>
                  <nts id="Seg_2488" n="HIAT:ip">,</nts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2491" n="HIAT:w" s="T686">a</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2494" n="HIAT:w" s="T687">tăn</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2497" n="HIAT:w" s="T688">ĭmbi</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2500" n="HIAT:w" s="T689">ĭzemnie</ts>
                  <nts id="Seg_2501" n="HIAT:ip">?</nts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2504" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2506" n="HIAT:w" s="T690">A</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2509" n="HIAT:w" s="T691">măn</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2512" n="HIAT:w" s="T692">tĭmem</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2514" n="HIAT:ip">(</nts>
                  <ts e="T694" id="Seg_2516" n="HIAT:w" s="T693">ĭzembie-</ts>
                  <nts id="Seg_2517" n="HIAT:ip">)</nts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2520" n="HIAT:w" s="T694">ĭzemneʔbə</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2523" n="HIAT:w" s="T695">i</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2526" n="HIAT:w" s="T696">pĭjem</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2529" n="HIAT:w" s="T697">ĭzemneʔbə</ts>
                  <nts id="Seg_2530" n="HIAT:ip">.</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T700" id="Seg_2532" n="sc" s="T699">
               <ts e="T700" id="Seg_2534" n="HIAT:u" s="T699">
                  <nts id="Seg_2535" n="HIAT:ip">(</nts>
                  <nts id="Seg_2536" n="HIAT:ip">(</nts>
                  <ats e="T700" id="Seg_2537" n="HIAT:non-pho" s="T699">BRK</ats>
                  <nts id="Seg_2538" n="HIAT:ip">)</nts>
                  <nts id="Seg_2539" n="HIAT:ip">)</nts>
                  <nts id="Seg_2540" n="HIAT:ip">.</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T715" id="Seg_2542" n="sc" s="T701">
               <ts e="T715" id="Seg_2544" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2546" n="HIAT:w" s="T701">Ivanovičən</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2549" n="HIAT:w" s="T702">bar</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2552" n="HIAT:w" s="T703">šüjot</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2555" n="HIAT:w" s="T704">ĭzemnie</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2558" n="HIAT:w" s="T705">i</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2561" n="HIAT:w" s="T706">ujut</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2564" n="HIAT:w" s="T707">ĭzemnie</ts>
                  <nts id="Seg_2565" n="HIAT:ip">,</nts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2568" n="HIAT:w" s="T708">i</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2570" n="HIAT:ip">(</nts>
                  <ts e="T710" id="Seg_2572" n="HIAT:w" s="T709">ud-</ts>
                  <nts id="Seg_2573" n="HIAT:ip">)</nts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2575" n="HIAT:ip">(</nts>
                  <ts e="T711" id="Seg_2577" n="HIAT:w" s="T710">udazaŋdə</ts>
                  <nts id="Seg_2578" n="HIAT:ip">)</nts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2581" n="HIAT:w" s="T711">ĭzemneʔpəʔjə</ts>
                  <nts id="Seg_2582" n="HIAT:ip">,</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2585" n="HIAT:w" s="T712">bar</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2588" n="HIAT:w" s="T713">tăŋ</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2591" n="HIAT:w" s="T714">ĭzemnie</ts>
                  <nts id="Seg_2592" n="HIAT:ip">.</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T717" id="Seg_2594" n="sc" s="T716">
               <ts e="T717" id="Seg_2596" n="HIAT:u" s="T716">
                  <nts id="Seg_2597" n="HIAT:ip">(</nts>
                  <nts id="Seg_2598" n="HIAT:ip">(</nts>
                  <ats e="T717" id="Seg_2599" n="HIAT:non-pho" s="T716">BRK</ats>
                  <nts id="Seg_2600" n="HIAT:ip">)</nts>
                  <nts id="Seg_2601" n="HIAT:ip">)</nts>
                  <nts id="Seg_2602" n="HIAT:ip">.</nts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T726" id="Seg_2604" n="sc" s="T718">
               <ts e="T726" id="Seg_2606" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2608" n="HIAT:w" s="T718">Nada</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2610" n="HIAT:ip">(</nts>
                  <ts e="T720" id="Seg_2612" n="HIAT:w" s="T719">da-</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2615" n="HIAT:w" s="T720">da-</ts>
                  <nts id="Seg_2616" n="HIAT:ip">)</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2619" n="HIAT:w" s="T721">dʼazirzittə</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2622" n="HIAT:w" s="T722">dĭ</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2624" n="HIAT:ip">(</nts>
                  <ts e="T724" id="Seg_2626" n="HIAT:w" s="T723">na-</ts>
                  <nts id="Seg_2627" n="HIAT:ip">)</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2630" n="HIAT:w" s="T724">nagur</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2633" n="HIAT:w" s="T725">kuza</ts>
                  <nts id="Seg_2634" n="HIAT:ip">.</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_2636" n="sc" s="T727">
               <ts e="T734" id="Seg_2638" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2640" n="HIAT:w" s="T727">Doxtăr</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2643" n="HIAT:w" s="T728">nada</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2646" n="HIAT:w" s="T729">deʔsittə</ts>
                  <nts id="Seg_2647" n="HIAT:ip">,</nts>
                  <nts id="Seg_2648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2650" n="HIAT:w" s="T730">pušaj</ts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2653" n="HIAT:w" s="T731">dʼazirləj</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2656" n="HIAT:w" s="T732">dĭ</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2659" n="HIAT:w" s="T733">dĭzem</ts>
                  <nts id="Seg_2660" n="HIAT:ip">.</nts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T736" id="Seg_2662" n="sc" s="T735">
               <ts e="T736" id="Seg_2664" n="HIAT:u" s="T735">
                  <nts id="Seg_2665" n="HIAT:ip">(</nts>
                  <nts id="Seg_2666" n="HIAT:ip">(</nts>
                  <ats e="T736" id="Seg_2667" n="HIAT:non-pho" s="T735">BRK</ats>
                  <nts id="Seg_2668" n="HIAT:ip">)</nts>
                  <nts id="Seg_2669" n="HIAT:ip">)</nts>
                  <nts id="Seg_2670" n="HIAT:ip">.</nts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T744" id="Seg_2672" n="sc" s="T737">
               <ts e="T744" id="Seg_2674" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2676" n="HIAT:w" s="T737">Ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2679" n="HIAT:w" s="T738">mĭləj</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2682" n="HIAT:w" s="T739">bĭʔsittə</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2685" n="HIAT:w" s="T740">ali</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2688" n="HIAT:w" s="T741">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2691" n="HIAT:w" s="T742">mĭləj</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2694" n="HIAT:w" s="T743">kĭškəsʼtə</ts>
                  <nts id="Seg_2695" n="HIAT:ip">.</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T746" id="Seg_2697" n="sc" s="T745">
               <ts e="T746" id="Seg_2699" n="HIAT:u" s="T745">
                  <nts id="Seg_2700" n="HIAT:ip">(</nts>
                  <nts id="Seg_2701" n="HIAT:ip">(</nts>
                  <ats e="T746" id="Seg_2702" n="HIAT:non-pho" s="T745">BRK</ats>
                  <nts id="Seg_2703" n="HIAT:ip">)</nts>
                  <nts id="Seg_2704" n="HIAT:ip">)</nts>
                  <nts id="Seg_2705" n="HIAT:ip">.</nts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T756" id="Seg_2707" n="sc" s="T747">
               <ts e="T756" id="Seg_2709" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_2711" n="HIAT:w" s="T747">Dĭzeŋ</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2714" n="HIAT:w" s="T748">bar</ts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2717" n="HIAT:w" s="T749">tăŋ</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2720" n="HIAT:w" s="T750">kănnambiʔi</ts>
                  <nts id="Seg_2721" n="HIAT:ip">,</nts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2724" n="HIAT:w" s="T751">a</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2727" n="HIAT:w" s="T752">tuj</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2730" n="HIAT:w" s="T753">ugandə</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2733" n="HIAT:w" s="T754">tăŋ</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2736" n="HIAT:w" s="T755">ĭzemneʔbəʔjə</ts>
                  <nts id="Seg_2737" n="HIAT:ip">.</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T758" id="Seg_2739" n="sc" s="T757">
               <ts e="T758" id="Seg_2741" n="HIAT:u" s="T757">
                  <nts id="Seg_2742" n="HIAT:ip">(</nts>
                  <nts id="Seg_2743" n="HIAT:ip">(</nts>
                  <ats e="T758" id="Seg_2744" n="HIAT:non-pho" s="T757">BRK</ats>
                  <nts id="Seg_2745" n="HIAT:ip">)</nts>
                  <nts id="Seg_2746" n="HIAT:ip">)</nts>
                  <nts id="Seg_2747" n="HIAT:ip">.</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T762" id="Seg_2749" n="sc" s="T759">
               <ts e="T762" id="Seg_2751" n="HIAT:u" s="T759">
                  <ts e="T760" id="Seg_2753" n="HIAT:w" s="T759">Marejka</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2756" n="HIAT:w" s="T760">urgajanə</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2759" n="HIAT:w" s="T761">kadəldə</ts>
                  <nts id="Seg_2760" n="HIAT:ip">.</nts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T770" id="Seg_2762" n="sc" s="T763">
               <ts e="T770" id="Seg_2764" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_2766" n="HIAT:w" s="T763">Urgajanə</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2769" n="HIAT:w" s="T764">dĭrgit</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2772" n="HIAT:w" s="T765">kadəldə</ts>
                  <nts id="Seg_2773" n="HIAT:ip">,</nts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2776" n="HIAT:w" s="T766">i</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2779" n="HIAT:w" s="T767">simat</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2782" n="HIAT:w" s="T768">dĭrgit</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2784" n="HIAT:ip">(</nts>
                  <ts e="T769.tx.1" id="Seg_2786" n="HIAT:w" s="T769">urgajagəndə</ts>
                  <nts id="Seg_2787" n="HIAT:ip">)</nts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2789" n="HIAT:ip">(</nts>
                  <nts id="Seg_2790" n="HIAT:ip">(</nts>
                  <ats e="T770" id="Seg_2791" n="HIAT:non-pho" s="T769.tx.1">NOISE</ats>
                  <nts id="Seg_2792" n="HIAT:ip">)</nts>
                  <nts id="Seg_2793" n="HIAT:ip">)</nts>
                  <nts id="Seg_2794" n="HIAT:ip">.</nts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T772" id="Seg_2796" n="sc" s="T771">
               <ts e="T772" id="Seg_2798" n="HIAT:u" s="T771">
                  <nts id="Seg_2799" n="HIAT:ip">(</nts>
                  <nts id="Seg_2800" n="HIAT:ip">(</nts>
                  <ats e="T772" id="Seg_2801" n="HIAT:non-pho" s="T771">BRK</ats>
                  <nts id="Seg_2802" n="HIAT:ip">)</nts>
                  <nts id="Seg_2803" n="HIAT:ip">)</nts>
                  <nts id="Seg_2804" n="HIAT:ip">.</nts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T777" id="Seg_2806" n="sc" s="T773">
               <ts e="T777" id="Seg_2808" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2810" n="HIAT:w" s="T773">Amnobiʔi</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2813" n="HIAT:w" s="T774">nüke</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2816" n="HIAT:w" s="T775">i</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2819" n="HIAT:w" s="T776">büzʼe</ts>
                  <nts id="Seg_2820" n="HIAT:ip">.</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T781" id="Seg_2822" n="sc" s="T778">
               <ts e="T781" id="Seg_2824" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2826" n="HIAT:w" s="T778">Samən</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2829" n="HIAT:w" s="T779">bügən</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2832" n="HIAT:w" s="T780">toʔndə</ts>
                  <nts id="Seg_2833" n="HIAT:ip">.</nts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T785" id="Seg_2835" n="sc" s="T782">
               <ts e="T785" id="Seg_2837" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_2839" n="HIAT:w" s="T782">Nüke</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2842" n="HIAT:w" s="T783">ererla</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2845" n="HIAT:w" s="T784">amnolaʔpi</ts>
                  <nts id="Seg_2846" n="HIAT:ip">.</nts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T793" id="Seg_2848" n="sc" s="T786">
               <ts e="T793" id="Seg_2850" n="HIAT:u" s="T786">
                  <ts e="T787" id="Seg_2852" n="HIAT:w" s="T786">A</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2855" n="HIAT:w" s="T787">büzʼe</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2858" n="HIAT:w" s="T788">mĭmbi</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2861" n="HIAT:w" s="T789">bünə</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2864" n="HIAT:w" s="T790">kola</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2866" n="HIAT:ip">(</nts>
                  <ts e="T792" id="Seg_2868" n="HIAT:w" s="T791">dʼapitʼ-</ts>
                  <nts id="Seg_2869" n="HIAT:ip">)</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2872" n="HIAT:w" s="T792">dʼabəsʼtə</ts>
                  <nts id="Seg_2873" n="HIAT:ip">.</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T807" id="Seg_2875" n="sc" s="T794">
               <ts e="T803" id="Seg_2877" n="HIAT:u" s="T794">
                  <nts id="Seg_2878" n="HIAT:ip">(</nts>
                  <ts e="T795" id="Seg_2880" n="HIAT:w" s="T794">Onʼiʔ</ts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2883" n="HIAT:w" s="T795">raz=</ts>
                  <nts id="Seg_2884" n="HIAT:ip">)</nts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2887" n="HIAT:w" s="T796">Onʼiʔ</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2890" n="HIAT:w" s="T797">raz</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2893" n="HIAT:w" s="T798">kambi</ts>
                  <nts id="Seg_2894" n="HIAT:ip">,</nts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2897" n="HIAT:w" s="T799">dĭgəttə</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2900" n="HIAT:w" s="T800">ĭmbidə</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2903" n="HIAT:w" s="T801">ej</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2906" n="HIAT:w" s="T802">dʼaʔpi</ts>
                  <nts id="Seg_2907" n="HIAT:ip">.</nts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_2910" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_2912" n="HIAT:w" s="T803">Onʼiʔ</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2915" n="HIAT:w" s="T804">riba</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2918" n="HIAT:w" s="T805">dʼaʔpi</ts>
                  <nts id="Seg_2919" n="HIAT:ip">,</nts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2922" n="HIAT:w" s="T806">kuvas</ts>
                  <nts id="Seg_2923" n="HIAT:ip">.</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T810" id="Seg_2925" n="sc" s="T808">
               <ts e="T810" id="Seg_2927" n="HIAT:u" s="T808">
                  <nts id="Seg_2928" n="HIAT:ip">"</nts>
                  <ts e="T809" id="Seg_2930" n="HIAT:w" s="T808">Măn</ts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2933" n="HIAT:w" s="T809">pʼeleʔbə</ts>
                  <nts id="Seg_2934" n="HIAT:ip">…</nts>
                  <nts id="Seg_2935" n="HIAT:ip">"</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T812" id="Seg_2937" n="sc" s="T811">
               <ts e="T812" id="Seg_2939" n="HIAT:u" s="T811">
                  <nts id="Seg_2940" n="HIAT:ip">(</nts>
                  <nts id="Seg_2941" n="HIAT:ip">(</nts>
                  <ats e="T812" id="Seg_2942" n="HIAT:non-pho" s="T811">BRK</ats>
                  <nts id="Seg_2943" n="HIAT:ip">)</nts>
                  <nts id="Seg_2944" n="HIAT:ip">)</nts>
                  <nts id="Seg_2945" n="HIAT:ip">.</nts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T819" id="Seg_2947" n="sc" s="T813">
               <ts e="T819" id="Seg_2949" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_2951" n="HIAT:w" s="T813">Kola</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2954" n="HIAT:w" s="T814">kuvas</ts>
                  <nts id="Seg_2955" n="HIAT:ip">,</nts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2958" n="HIAT:w" s="T815">dʼăbaktərlia</ts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2961" n="HIAT:w" s="T816">kuzanə</ts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2963" n="HIAT:ip">(</nts>
                  <ts e="T818" id="Seg_2965" n="HIAT:w" s="T817">я-</ts>
                  <nts id="Seg_2966" n="HIAT:ip">)</nts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2969" n="HIAT:w" s="T818">šiketsi</ts>
                  <nts id="Seg_2970" n="HIAT:ip">.</nts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T823" id="Seg_2972" n="sc" s="T820">
               <ts e="T823" id="Seg_2974" n="HIAT:u" s="T820">
                  <nts id="Seg_2975" n="HIAT:ip">"</nts>
                  <ts e="T821" id="Seg_2977" n="HIAT:w" s="T820">Öʔleʔ</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2980" n="HIAT:w" s="T821">măna</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2983" n="HIAT:w" s="T822">bünə</ts>
                  <nts id="Seg_2984" n="HIAT:ip">!</nts>
                  <nts id="Seg_2985" n="HIAT:ip">"</nts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T826" id="Seg_2987" n="sc" s="T824">
               <ts e="T826" id="Seg_2989" n="HIAT:u" s="T824">
                  <nts id="Seg_2990" n="HIAT:ip">"</nts>
                  <ts e="T825" id="Seg_2992" n="HIAT:w" s="T824">Măn</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2995" n="HIAT:w" s="T825">öʔluʔpiam</ts>
                  <nts id="Seg_2996" n="HIAT:ip">"</nts>
                  <nts id="Seg_2997" n="HIAT:ip">.</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T832" id="Seg_2999" n="sc" s="T827">
               <ts e="T832" id="Seg_3001" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_3003" n="HIAT:w" s="T827">Šobi</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3006" n="HIAT:w" s="T828">maʔndə</ts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3009" n="HIAT:w" s="T829">da</ts>
                  <nts id="Seg_3010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3012" n="HIAT:w" s="T830">nükenə</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3015" n="HIAT:w" s="T831">dʼăbaktərlabə</ts>
                  <nts id="Seg_3016" n="HIAT:ip">.</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T840" id="Seg_3018" n="sc" s="T833">
               <ts e="T840" id="Seg_3020" n="HIAT:u" s="T833">
                  <nts id="Seg_3021" n="HIAT:ip">"</nts>
                  <ts e="T834" id="Seg_3023" n="HIAT:w" s="T833">Kola</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3026" n="HIAT:w" s="T834">bar</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3029" n="HIAT:w" s="T835">kuvas</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3032" n="HIAT:w" s="T836">ibi</ts>
                  <nts id="Seg_3033" n="HIAT:ip">,</nts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3036" n="HIAT:w" s="T837">dʼăbaktərbi</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3039" n="HIAT:w" s="T838">kak</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3042" n="HIAT:w" s="T839">kuza</ts>
                  <nts id="Seg_3043" n="HIAT:ip">"</nts>
                  <nts id="Seg_3044" n="HIAT:ip">.</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T842" id="Seg_3046" n="sc" s="T841">
               <ts e="T842" id="Seg_3048" n="HIAT:u" s="T841">
                  <nts id="Seg_3049" n="HIAT:ip">(</nts>
                  <nts id="Seg_3050" n="HIAT:ip">(</nts>
                  <ats e="T842" id="Seg_3051" n="HIAT:non-pho" s="T841">BRK</ats>
                  <nts id="Seg_3052" n="HIAT:ip">)</nts>
                  <nts id="Seg_3053" n="HIAT:ip">)</nts>
                  <nts id="Seg_3054" n="HIAT:ip">.</nts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T848" id="Seg_3056" n="sc" s="T843">
               <ts e="T848" id="Seg_3058" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3060" n="HIAT:w" s="T843">Dĭgəttə</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3063" n="HIAT:w" s="T844">dĭ</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3066" n="HIAT:w" s="T845">nüke</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3069" n="HIAT:w" s="T846">davaj</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3072" n="HIAT:w" s="T847">kudonzittə</ts>
                  <nts id="Seg_3073" n="HIAT:ip">.</nts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T854" id="Seg_3075" n="sc" s="T849">
               <ts e="T854" id="Seg_3077" n="HIAT:u" s="T849">
                  <nts id="Seg_3078" n="HIAT:ip">"</nts>
                  <ts e="T850" id="Seg_3080" n="HIAT:w" s="T849">Ĭmbi</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3083" n="HIAT:w" s="T850">tăn</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3086" n="HIAT:w" s="T851">ĭmbidə</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3089" n="HIAT:w" s="T852">ej</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3092" n="HIAT:w" s="T853">pʼebiel</ts>
                  <nts id="Seg_3093" n="HIAT:ip">?</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T862" id="Seg_3095" n="sc" s="T855">
               <ts e="T862" id="Seg_3097" n="HIAT:u" s="T855">
                  <ts e="T856" id="Seg_3099" n="HIAT:w" s="T855">Kărɨtă</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3102" n="HIAT:w" s="T856">miʔnʼibeʔ</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3105" n="HIAT:w" s="T857">naga</ts>
                  <nts id="Seg_3106" n="HIAT:ip">,</nts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3108" n="HIAT:ip">(</nts>
                  <ts e="T858.tx.1" id="Seg_3110" n="HIAT:w" s="T858">ši-</ts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3113" n="HIAT:w" s="T858.tx.1">šiz-</ts>
                  <nts id="Seg_3114" n="HIAT:ip">)</nts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3117" n="HIAT:w" s="T859">šižəbi</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3119" n="HIAT:ip">(</nts>
                  <ts e="T861" id="Seg_3121" n="HIAT:w" s="T860">kərɨ-</ts>
                  <nts id="Seg_3122" n="HIAT:ip">)</nts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3125" n="HIAT:w" s="T861">kărĭtă</ts>
                  <nts id="Seg_3126" n="HIAT:ip">"</nts>
                  <nts id="Seg_3127" n="HIAT:ip">.</nts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T867" id="Seg_3129" n="sc" s="T863">
               <ts e="T867" id="Seg_3131" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_3133" n="HIAT:w" s="T863">Dĭ</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3136" n="HIAT:w" s="T864">kambi</ts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3139" n="HIAT:w" s="T865">bazo</ts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3142" n="HIAT:w" s="T866">bünə</ts>
                  <nts id="Seg_3143" n="HIAT:ip">.</nts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T880" id="Seg_3145" n="sc" s="T868">
               <ts e="T874" id="Seg_3147" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3149" n="HIAT:w" s="T868">Dĭgəttə</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3152" n="HIAT:w" s="T869">davaj</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3155" n="HIAT:w" s="T870">kirgarzittə:</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3157" n="HIAT:ip">"</nts>
                  <ts e="T872" id="Seg_3159" n="HIAT:w" s="T871">Kola</ts>
                  <nts id="Seg_3160" n="HIAT:ip">,</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3163" n="HIAT:w" s="T872">šoʔ</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3166" n="HIAT:w" s="T873">döber</ts>
                  <nts id="Seg_3167" n="HIAT:ip">!</nts>
                  <nts id="Seg_3168" n="HIAT:ip">"</nts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3171" n="HIAT:u" s="T874">
                  <ts e="T875" id="Seg_3173" n="HIAT:w" s="T874">Dĭ</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3176" n="HIAT:w" s="T875">šobi:</ts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3178" n="HIAT:ip">"</nts>
                  <ts e="T877" id="Seg_3180" n="HIAT:w" s="T876">Ĭmbi</ts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3183" n="HIAT:w" s="T877">tănan</ts>
                  <nts id="Seg_3184" n="HIAT:ip">,</nts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3187" n="HIAT:w" s="T878">büzʼe</ts>
                  <nts id="Seg_3188" n="HIAT:ip">,</nts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3191" n="HIAT:w" s="T879">kereʔ</ts>
                  <nts id="Seg_3192" n="HIAT:ip">?</nts>
                  <nts id="Seg_3193" n="HIAT:ip">"</nts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T884" id="Seg_3195" n="sc" s="T881">
               <ts e="T884" id="Seg_3197" n="HIAT:u" s="T881">
                  <nts id="Seg_3198" n="HIAT:ip">"</nts>
                  <ts e="T882" id="Seg_3200" n="HIAT:w" s="T881">Măna</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3203" n="HIAT:w" s="T882">kereʔ</ts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3206" n="HIAT:w" s="T883">kărɨtă</ts>
                  <nts id="Seg_3207" n="HIAT:ip">.</nts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T891" id="Seg_3209" n="sc" s="T885">
               <ts e="T888" id="Seg_3211" n="HIAT:u" s="T885">
                  <ts e="T886" id="Seg_3213" n="HIAT:w" s="T885">Nükem</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3216" n="HIAT:w" s="T886">bar</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3219" n="HIAT:w" s="T887">kudonzlaʔbə</ts>
                  <nts id="Seg_3220" n="HIAT:ip">"</nts>
                  <nts id="Seg_3221" n="HIAT:ip">.</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3224" n="HIAT:u" s="T888">
                  <nts id="Seg_3225" n="HIAT:ip">"</nts>
                  <ts e="T889" id="Seg_3227" n="HIAT:w" s="T888">Nu</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3230" n="HIAT:w" s="T889">kanaʔ</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3233" n="HIAT:w" s="T890">manə</ts>
                  <nts id="Seg_3234" n="HIAT:ip">"</nts>
                  <nts id="Seg_3235" n="HIAT:ip">.</nts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T893" id="Seg_3237" n="sc" s="T892">
               <ts e="T893" id="Seg_3239" n="HIAT:u" s="T892">
                  <nts id="Seg_3240" n="HIAT:ip">(</nts>
                  <nts id="Seg_3241" n="HIAT:ip">(</nts>
                  <ats e="T893" id="Seg_3242" n="HIAT:non-pho" s="T892">BRK</ats>
                  <nts id="Seg_3243" n="HIAT:ip">)</nts>
                  <nts id="Seg_3244" n="HIAT:ip">)</nts>
                  <nts id="Seg_3245" n="HIAT:ip">.</nts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T903" id="Seg_3247" n="sc" s="T894">
               <ts e="T903" id="Seg_3249" n="HIAT:u" s="T894">
                  <ts e="T895" id="Seg_3251" n="HIAT:w" s="T894">Dĭ</ts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3254" n="HIAT:w" s="T895">nüke</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3257" n="HIAT:w" s="T896">davaj</ts>
                  <nts id="Seg_3258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3260" n="HIAT:w" s="T897">dĭm</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3263" n="HIAT:w" s="T898">kudonzittə:</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3265" n="HIAT:ip">"</nts>
                  <ts e="T900" id="Seg_3267" n="HIAT:w" s="T899">Ĭmbi</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3270" n="HIAT:w" s="T900">ej</ts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3273" n="HIAT:w" s="T901">pʼebiel</ts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3276" n="HIAT:w" s="T902">tura</ts>
                  <nts id="Seg_3277" n="HIAT:ip">?</nts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T908" id="Seg_3279" n="sc" s="T904">
               <ts e="T908" id="Seg_3281" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3283" n="HIAT:w" s="T904">Miʔ</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3286" n="HIAT:w" s="T905">tura</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3289" n="HIAT:w" s="T906">ej</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3292" n="HIAT:w" s="T907">kuvas</ts>
                  <nts id="Seg_3293" n="HIAT:ip">"</nts>
                  <nts id="Seg_3294" n="HIAT:ip">.</nts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T918" id="Seg_3296" n="sc" s="T909">
               <ts e="T916" id="Seg_3298" n="HIAT:u" s="T909">
                  <ts e="T910" id="Seg_3300" n="HIAT:w" s="T909">Dĭ</ts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3303" n="HIAT:w" s="T910">bazo</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3306" n="HIAT:w" s="T911">kambi</ts>
                  <nts id="Seg_3307" n="HIAT:ip">,</nts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3310" n="HIAT:w" s="T912">davaj</ts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3313" n="HIAT:w" s="T913">kirgarzittə:</ts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3315" n="HIAT:ip">"</nts>
                  <ts e="T915" id="Seg_3317" n="HIAT:w" s="T914">Kola</ts>
                  <nts id="Seg_3318" n="HIAT:ip">,</nts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3321" n="HIAT:w" s="T915">kola</ts>
                  <nts id="Seg_3322" n="HIAT:ip">!</nts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_3325" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_3327" n="HIAT:w" s="T916">šoʔ</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3330" n="HIAT:w" s="T917">döber</ts>
                  <nts id="Seg_3331" n="HIAT:ip">!</nts>
                  <nts id="Seg_3332" n="HIAT:ip">"</nts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T924" id="Seg_3334" n="sc" s="T919">
               <ts e="T924" id="Seg_3336" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_3338" n="HIAT:w" s="T919">Kola</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3341" n="HIAT:w" s="T920">šobi:</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3343" n="HIAT:ip">"</nts>
                  <ts e="T922" id="Seg_3345" n="HIAT:w" s="T921">Ĭmbi</ts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3348" n="HIAT:w" s="T922">tănan</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3351" n="HIAT:w" s="T923">kereʔ</ts>
                  <nts id="Seg_3352" n="HIAT:ip">?</nts>
                  <nts id="Seg_3353" n="HIAT:ip">"</nts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T927" id="Seg_3355" n="sc" s="T925">
               <ts e="T927" id="Seg_3357" n="HIAT:u" s="T925">
                  <nts id="Seg_3358" n="HIAT:ip">"</nts>
                  <ts e="T926" id="Seg_3360" n="HIAT:w" s="T925">Tura</ts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3363" n="HIAT:w" s="T926">kereʔ</ts>
                  <nts id="Seg_3364" n="HIAT:ip">.</nts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T932" id="Seg_3366" n="sc" s="T928">
               <ts e="T932" id="Seg_3368" n="HIAT:u" s="T928">
                  <ts e="T929" id="Seg_3370" n="HIAT:w" s="T928">Miʔ</ts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3373" n="HIAT:w" s="T929">turabaʔ</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3376" n="HIAT:w" s="T930">ej</ts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3379" n="HIAT:w" s="T931">kuvas</ts>
                  <nts id="Seg_3380" n="HIAT:ip">"</nts>
                  <nts id="Seg_3381" n="HIAT:ip">.</nts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T935" id="Seg_3383" n="sc" s="T933">
               <ts e="T935" id="Seg_3385" n="HIAT:u" s="T933">
                  <nts id="Seg_3386" n="HIAT:ip">"</nts>
                  <ts e="T934" id="Seg_3388" n="HIAT:w" s="T933">Kanaʔ</ts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3391" n="HIAT:w" s="T934">maʔnəl</ts>
                  <nts id="Seg_3392" n="HIAT:ip">!</nts>
                  <nts id="Seg_3393" n="HIAT:ip">"</nts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T937" id="Seg_3395" n="sc" s="T936">
               <ts e="T937" id="Seg_3397" n="HIAT:u" s="T936">
                  <nts id="Seg_3398" n="HIAT:ip">(</nts>
                  <nts id="Seg_3399" n="HIAT:ip">(</nts>
                  <ats e="T937" id="Seg_3400" n="HIAT:non-pho" s="T936">BRK</ats>
                  <nts id="Seg_3401" n="HIAT:ip">)</nts>
                  <nts id="Seg_3402" n="HIAT:ip">)</nts>
                  <nts id="Seg_3403" n="HIAT:ip">.</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T951" id="Seg_3405" n="sc" s="T938">
               <ts e="T951" id="Seg_3407" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3409" n="HIAT:w" s="T938">Dĭgəttə</ts>
                  <nts id="Seg_3410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3412" n="HIAT:w" s="T939">bazo</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3415" n="HIAT:w" s="T940">dĭ</ts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3418" n="HIAT:w" s="T941">nüke</ts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3421" n="HIAT:w" s="T942">kudonzəbi:</ts>
                  <nts id="Seg_3422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3423" n="HIAT:ip">"</nts>
                  <ts e="T945" id="Seg_3425" n="HIAT:w" s="T944">Kanaʔ</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3428" n="HIAT:w" s="T945">kolanə</ts>
                  <nts id="Seg_3429" n="HIAT:ip">,</nts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3432" n="HIAT:w" s="T946">pʼeʔ</ts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3435" n="HIAT:w" s="T947">štobɨ</ts>
                  <nts id="Seg_3436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3438" n="HIAT:w" s="T948">măn</ts>
                  <nts id="Seg_3439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3441" n="HIAT:w" s="T949">mobiam</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3444" n="HIAT:w" s="T950">kupčixăjzi</ts>
                  <nts id="Seg_3445" n="HIAT:ip">"</nts>
                  <nts id="Seg_3446" n="HIAT:ip">.</nts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T953" id="Seg_3448" n="sc" s="T952">
               <ts e="T953" id="Seg_3450" n="HIAT:u" s="T952">
                  <nts id="Seg_3451" n="HIAT:ip">(</nts>
                  <nts id="Seg_3452" n="HIAT:ip">(</nts>
                  <ats e="T953" id="Seg_3453" n="HIAT:non-pho" s="T952">BRK</ats>
                  <nts id="Seg_3454" n="HIAT:ip">)</nts>
                  <nts id="Seg_3455" n="HIAT:ip">)</nts>
                  <nts id="Seg_3456" n="HIAT:ip">.</nts>
                  <nts id="Seg_3457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T959" id="Seg_3458" n="sc" s="T954">
               <ts e="T959" id="Seg_3460" n="HIAT:u" s="T954">
                  <ts e="T955" id="Seg_3462" n="HIAT:w" s="T954">Dĭgəttə</ts>
                  <nts id="Seg_3463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3465" n="HIAT:w" s="T955">kola</ts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3468" n="HIAT:w" s="T956">mămbi:</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3470" n="HIAT:ip">"</nts>
                  <ts e="T958" id="Seg_3472" n="HIAT:w" s="T957">Kanaʔ</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3475" n="HIAT:w" s="T958">maːʔnəl</ts>
                  <nts id="Seg_3476" n="HIAT:ip">"</nts>
                  <nts id="Seg_3477" n="HIAT:ip">.</nts>
                  <nts id="Seg_3478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T975" id="Seg_3479" n="sc" s="T960">
               <ts e="T975" id="Seg_3481" n="HIAT:u" s="T960">
                  <ts e="T961" id="Seg_3483" n="HIAT:w" s="T960">Dĭ</ts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3486" n="HIAT:w" s="T961">šöbi</ts>
                  <nts id="Seg_3487" n="HIAT:ip">,</nts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3490" n="HIAT:w" s="T962">a</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3493" n="HIAT:w" s="T963">dĭ</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3496" n="HIAT:w" s="T964">nüke</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3499" n="HIAT:w" s="T965">bazo</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3502" n="HIAT:w" s="T966">kudonzlaʔbə:</ts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3504" n="HIAT:ip">"</nts>
                  <ts e="T969" id="Seg_3506" n="HIAT:w" s="T968">Kanaʔ</ts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3509" n="HIAT:w" s="T969">kolanə</ts>
                  <nts id="Seg_3510" n="HIAT:ip">,</nts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3513" n="HIAT:w" s="T970">peʔ</ts>
                  <nts id="Seg_3514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3516" n="HIAT:w" s="T971">štobɨ</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3519" n="HIAT:w" s="T972">măn</ts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3522" n="HIAT:w" s="T973">ibiem</ts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3525" n="HIAT:w" s="T974">dvărʼankajzi</ts>
                  <nts id="Seg_3526" n="HIAT:ip">"</nts>
                  <nts id="Seg_3527" n="HIAT:ip">.</nts>
                  <nts id="Seg_3528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T981" id="Seg_3529" n="sc" s="T976">
               <ts e="T981" id="Seg_3531" n="HIAT:u" s="T976">
                  <ts e="T977" id="Seg_3533" n="HIAT:w" s="T976">Dĭ</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3536" n="HIAT:w" s="T977">kambi</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3539" n="HIAT:w" s="T978">kolanə</ts>
                  <nts id="Seg_3540" n="HIAT:ip">,</nts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3543" n="HIAT:w" s="T979">kola</ts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3546" n="HIAT:w" s="T980">kirgarlaʔpi</ts>
                  <nts id="Seg_3547" n="HIAT:ip">.</nts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T987" id="Seg_3549" n="sc" s="T982">
               <ts e="T987" id="Seg_3551" n="HIAT:u" s="T982">
                  <ts e="T983" id="Seg_3553" n="HIAT:w" s="T982">Kola</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3556" n="HIAT:w" s="T983">šobi:</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3558" n="HIAT:ip">"</nts>
                  <ts e="T985" id="Seg_3560" n="HIAT:w" s="T984">Ĭmbi</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3563" n="HIAT:w" s="T985">tănan</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3566" n="HIAT:w" s="T986">kereʔ</ts>
                  <nts id="Seg_3567" n="HIAT:ip">?</nts>
                  <nts id="Seg_3568" n="HIAT:ip">"</nts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T995" id="Seg_3570" n="sc" s="T988">
               <ts e="T995" id="Seg_3572" n="HIAT:u" s="T988">
                  <nts id="Seg_3573" n="HIAT:ip">"</nts>
                  <ts e="T989" id="Seg_3575" n="HIAT:w" s="T988">Măn</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3578" n="HIAT:w" s="T989">nükem</ts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3581" n="HIAT:w" s="T990">mănde</ts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3584" n="HIAT:w" s="T991">štobɨ</ts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3587" n="HIAT:w" s="T992">măn</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3590" n="HIAT:w" s="T993">dvărʼankăjzi</ts>
                  <nts id="Seg_3591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3593" n="HIAT:w" s="T994">ibiem</ts>
                  <nts id="Seg_3594" n="HIAT:ip">"</nts>
                  <nts id="Seg_3595" n="HIAT:ip">.</nts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T998" id="Seg_3597" n="sc" s="T996">
               <ts e="T998" id="Seg_3599" n="HIAT:u" s="T996">
                  <nts id="Seg_3600" n="HIAT:ip">"</nts>
                  <ts e="T997" id="Seg_3602" n="HIAT:w" s="T996">Kanaʔ</ts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3605" n="HIAT:w" s="T997">maːʔnəl</ts>
                  <nts id="Seg_3606" n="HIAT:ip">!</nts>
                  <nts id="Seg_3607" n="HIAT:ip">"</nts>
                  <nts id="Seg_3608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1000" id="Seg_3609" n="sc" s="T999">
               <ts e="T1000" id="Seg_3611" n="HIAT:u" s="T999">
                  <nts id="Seg_3612" n="HIAT:ip">(</nts>
                  <nts id="Seg_3613" n="HIAT:ip">(</nts>
                  <ats e="T1000" id="Seg_3614" n="HIAT:non-pho" s="T999">BRK</ats>
                  <nts id="Seg_3615" n="HIAT:ip">)</nts>
                  <nts id="Seg_3616" n="HIAT:ip">)</nts>
                  <nts id="Seg_3617" n="HIAT:ip">.</nts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1009" id="Seg_3619" n="sc" s="T1001">
               <ts e="T1009" id="Seg_3621" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_3623" n="HIAT:w" s="T1001">Dĭgəttə</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3626" n="HIAT:w" s="T1002">büzʼe</ts>
                  <nts id="Seg_3627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3629" n="HIAT:w" s="T1003">šobi</ts>
                  <nts id="Seg_3630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3632" n="HIAT:w" s="T1004">maːʔndə</ts>
                  <nts id="Seg_3633" n="HIAT:ip">,</nts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3636" n="HIAT:w" s="T1005">dĭ</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3639" n="HIAT:w" s="T1006">davaj</ts>
                  <nts id="Seg_3640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3642" n="HIAT:w" s="T1007">bazo</ts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3645" n="HIAT:w" s="T1008">kudonzittə</ts>
                  <nts id="Seg_3646" n="HIAT:ip">.</nts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1015" id="Seg_3648" n="sc" s="T1010">
               <ts e="T1015" id="Seg_3650" n="HIAT:u" s="T1010">
                  <nts id="Seg_3651" n="HIAT:ip">(</nts>
                  <ts e="T1011" id="Seg_3653" n="HIAT:w" s="T1010">I</ts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_3656" n="HIAT:w" s="T1011">baʔ-</ts>
                  <nts id="Seg_3657" n="HIAT:ip">)</nts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_3660" n="HIAT:w" s="T1012">I</ts>
                  <nts id="Seg_3661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3663" n="HIAT:w" s="T1013">münörzittə</ts>
                  <nts id="Seg_3664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_3666" n="HIAT:w" s="T1014">xat'el</ts>
                  <nts id="Seg_3667" n="HIAT:ip">.</nts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1025" id="Seg_3669" n="sc" s="T1016">
               <ts e="T1025" id="Seg_3671" n="HIAT:u" s="T1016">
                  <nts id="Seg_3672" n="HIAT:ip">"</nts>
                  <ts e="T1017" id="Seg_3674" n="HIAT:w" s="T1016">Kanaʔ</ts>
                  <nts id="Seg_3675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_3677" n="HIAT:w" s="T1017">bazo</ts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_3680" n="HIAT:w" s="T1018">kolanə</ts>
                  <nts id="Seg_3681" n="HIAT:ip">,</nts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_3684" n="HIAT:w" s="T1019">peʔ</ts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_3687" n="HIAT:w" s="T1020">štobɨ</ts>
                  <nts id="Seg_3688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_3690" n="HIAT:w" s="T1021">măn</ts>
                  <nts id="Seg_3691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_3693" n="HIAT:w" s="T1022">была</ts>
                  <nts id="Seg_3694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3695" n="HIAT:ip">(</nts>
                  <ts e="T1024" id="Seg_3697" n="HIAT:w" s="T1023">гос-</ts>
                  <nts id="Seg_3698" n="HIAT:ip">)</nts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_3701" n="HIAT:w" s="T1024">государыней</ts>
                  <nts id="Seg_3702" n="HIAT:ip">.</nts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1031" id="Seg_3704" n="sc" s="T1026">
               <ts e="T1031" id="Seg_3706" n="HIAT:u" s="T1026">
                  <ts e="T1027" id="Seg_3708" n="HIAT:w" s="T1026">Štobɨ</ts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_3711" n="HIAT:w" s="T1027">jil</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_3714" n="HIAT:w" s="T1028">măna</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_3717" n="HIAT:w" s="T1029">udazaŋdə</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_3720" n="HIAT:w" s="T1030">носили</ts>
                  <nts id="Seg_3721" n="HIAT:ip">.</nts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1033" id="Seg_3723" n="sc" s="T1032">
               <ts e="T1033" id="Seg_3725" n="HIAT:u" s="T1032">
                  <nts id="Seg_3726" n="HIAT:ip">(</nts>
                  <nts id="Seg_3727" n="HIAT:ip">(</nts>
                  <ats e="T1033" id="Seg_3728" n="HIAT:non-pho" s="T1032">BRK</ats>
                  <nts id="Seg_3729" n="HIAT:ip">)</nts>
                  <nts id="Seg_3730" n="HIAT:ip">)</nts>
                  <nts id="Seg_3731" n="HIAT:ip">.</nts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1038" id="Seg_3733" n="sc" s="T1034">
               <ts e="T1038" id="Seg_3735" n="HIAT:u" s="T1034">
                  <ts e="T1035" id="Seg_3737" n="HIAT:w" s="T1034">Dĭgəttə</ts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_3740" n="HIAT:w" s="T1035">dĭ</ts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_3743" n="HIAT:w" s="T1036">šobi</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_3746" n="HIAT:w" s="T1037">bünə</ts>
                  <nts id="Seg_3747" n="HIAT:ip">.</nts>
                  <nts id="Seg_3748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1045" id="Seg_3749" n="sc" s="T1039">
               <ts e="T1043" id="Seg_3751" n="HIAT:u" s="T1039">
                  <nts id="Seg_3752" n="HIAT:ip">"</nts>
                  <ts e="T1040" id="Seg_3754" n="HIAT:w" s="T1039">Kola</ts>
                  <nts id="Seg_3755" n="HIAT:ip">,</nts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_3758" n="HIAT:w" s="T1040">kola</ts>
                  <nts id="Seg_3759" n="HIAT:ip">,</nts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_3762" n="HIAT:w" s="T1041">šoʔ</ts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_3765" n="HIAT:w" s="T1042">döber</ts>
                  <nts id="Seg_3766" n="HIAT:ip">!</nts>
                  <nts id="Seg_3767" n="HIAT:ip">"</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1045" id="Seg_3770" n="HIAT:u" s="T1043">
                  <ts e="T1044" id="Seg_3772" n="HIAT:w" s="T1043">Kola</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_3775" n="HIAT:w" s="T1044">šobi</ts>
                  <nts id="Seg_3776" n="HIAT:ip">.</nts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1062" id="Seg_3778" n="sc" s="T1046">
               <ts e="T1050" id="Seg_3780" n="HIAT:u" s="T1046">
                  <nts id="Seg_3781" n="HIAT:ip">"</nts>
                  <ts e="T1047" id="Seg_3783" n="HIAT:w" s="T1046">Ĭmbi</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_3786" n="HIAT:w" s="T1047">tănan</ts>
                  <nts id="Seg_3787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_3789" n="HIAT:w" s="T1048">kereʔ</ts>
                  <nts id="Seg_3790" n="HIAT:ip">,</nts>
                  <nts id="Seg_3791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_3793" n="HIAT:w" s="T1049">büzʼe</ts>
                  <nts id="Seg_3794" n="HIAT:ip">?</nts>
                  <nts id="Seg_3795" n="HIAT:ip">"</nts>
                  <nts id="Seg_3796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1062" id="Seg_3798" n="HIAT:u" s="T1050">
                  <nts id="Seg_3799" n="HIAT:ip">"</nts>
                  <ts e="T1051" id="Seg_3801" n="HIAT:w" s="T1050">Da</ts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_3804" n="HIAT:w" s="T1051">măn</ts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_3807" n="HIAT:w" s="T1052">nükem</ts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3809" n="HIAT:ip">(</nts>
                  <ts e="T1054" id="Seg_3811" n="HIAT:w" s="T1053">x-</ts>
                  <nts id="Seg_3812" n="HIAT:ip">)</nts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_3815" n="HIAT:w" s="T1054">măndə</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3817" n="HIAT:ip">(</nts>
                  <ts e="T1056" id="Seg_3819" n="HIAT:w" s="T1055">štobɨ</ts>
                  <nts id="Seg_3820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1057" id="Seg_3822" n="HIAT:w" s="T1056">măn</ts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_3825" n="HIAT:w" s="T1057">была=</ts>
                  <nts id="Seg_3826" n="HIAT:ip">)</nts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_3829" n="HIAT:w" s="T1058">štobɨ</ts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_3832" n="HIAT:w" s="T1059">măn</ts>
                  <nts id="Seg_3833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_3835" n="HIAT:w" s="T1060">ibiem</ts>
                  <nts id="Seg_3836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_3838" n="HIAT:w" s="T1061">tsaritsajzʼi</ts>
                  <nts id="Seg_3839" n="HIAT:ip">.</nts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1068" id="Seg_3841" n="sc" s="T1063">
               <ts e="T1068" id="Seg_3843" n="HIAT:u" s="T1063">
                  <ts e="T1064" id="Seg_3845" n="HIAT:w" s="T1063">Štobɨ</ts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_3848" n="HIAT:w" s="T1064">măna</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_3851" n="HIAT:w" s="T1065">il</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_3854" n="HIAT:w" s="T1066">udatsi</ts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_3857" n="HIAT:w" s="T1067">kunnambiʔi</ts>
                  <nts id="Seg_3858" n="HIAT:ip">"</nts>
                  <nts id="Seg_3859" n="HIAT:ip">.</nts>
                  <nts id="Seg_3860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1074" id="Seg_3861" n="sc" s="T1069">
               <ts e="T1074" id="Seg_3863" n="HIAT:u" s="T1069">
                  <nts id="Seg_3864" n="HIAT:ip">"</nts>
                  <ts e="T1070" id="Seg_3866" n="HIAT:w" s="T1069">Kanaʔ</ts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_3869" n="HIAT:w" s="T1070">maːʔnəl</ts>
                  <nts id="Seg_3870" n="HIAT:ip">,</nts>
                  <nts id="Seg_3871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_3873" n="HIAT:w" s="T1071">bar</ts>
                  <nts id="Seg_3874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_3876" n="HIAT:w" s="T1072">moləj</ts>
                  <nts id="Seg_3877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_3879" n="HIAT:w" s="T1073">tănan</ts>
                  <nts id="Seg_3880" n="HIAT:ip">"</nts>
                  <nts id="Seg_3881" n="HIAT:ip">.</nts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1076" id="Seg_3883" n="sc" s="T1075">
               <ts e="T1076" id="Seg_3885" n="HIAT:u" s="T1075">
                  <nts id="Seg_3886" n="HIAT:ip">(</nts>
                  <nts id="Seg_3887" n="HIAT:ip">(</nts>
                  <ats e="T1076" id="Seg_3888" n="HIAT:non-pho" s="T1075">BRK</ats>
                  <nts id="Seg_3889" n="HIAT:ip">)</nts>
                  <nts id="Seg_3890" n="HIAT:ip">)</nts>
                  <nts id="Seg_3891" n="HIAT:ip">.</nts>
                  <nts id="Seg_3892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1081" id="Seg_3893" n="sc" s="T1077">
               <ts e="T1081" id="Seg_3895" n="HIAT:u" s="T1077">
                  <ts e="T1078" id="Seg_3897" n="HIAT:w" s="T1077">Dĭgəttə</ts>
                  <nts id="Seg_3898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_3900" n="HIAT:w" s="T1078">büzʼe</ts>
                  <nts id="Seg_3901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_3903" n="HIAT:w" s="T1079">kambi</ts>
                  <nts id="Seg_3904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_3906" n="HIAT:w" s="T1080">bünə</ts>
                  <nts id="Seg_3907" n="HIAT:ip">.</nts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1091" id="Seg_3909" n="sc" s="T1082">
               <ts e="T1086" id="Seg_3911" n="HIAT:u" s="T1082">
                  <nts id="Seg_3912" n="HIAT:ip">"</nts>
                  <ts e="T1083" id="Seg_3914" n="HIAT:w" s="T1082">Kolaʔ</ts>
                  <nts id="Seg_3915" n="HIAT:ip">,</nts>
                  <nts id="Seg_3916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_3918" n="HIAT:w" s="T1083">kolaʔ</ts>
                  <nts id="Seg_3919" n="HIAT:ip">,</nts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_3922" n="HIAT:w" s="T1084">šoʔ</ts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_3925" n="HIAT:w" s="T1085">döber</ts>
                  <nts id="Seg_3926" n="HIAT:ip">!</nts>
                  <nts id="Seg_3927" n="HIAT:ip">"</nts>
                  <nts id="Seg_3928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1091" id="Seg_3930" n="HIAT:u" s="T1086">
                  <ts e="T1087" id="Seg_3932" n="HIAT:w" s="T1086">Kola</ts>
                  <nts id="Seg_3933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_3935" n="HIAT:w" s="T1087">šobi:</ts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3937" n="HIAT:ip">"</nts>
                  <ts e="T1089" id="Seg_3939" n="HIAT:w" s="T1088">Ĭmbi</ts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_3942" n="HIAT:w" s="T1089">tănan</ts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_3945" n="HIAT:w" s="T1090">kereʔ</ts>
                  <nts id="Seg_3946" n="HIAT:ip">?</nts>
                  <nts id="Seg_3947" n="HIAT:ip">"</nts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1101" id="Seg_3949" n="sc" s="T1092">
               <ts e="T1101" id="Seg_3951" n="HIAT:u" s="T1092">
                  <nts id="Seg_3952" n="HIAT:ip">"</nts>
                  <ts e="T1093" id="Seg_3954" n="HIAT:w" s="T1092">Da</ts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_3957" n="HIAT:w" s="T1093">măn</ts>
                  <nts id="Seg_3958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_3960" n="HIAT:w" s="T1094">nükem</ts>
                  <nts id="Seg_3961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_3963" n="HIAT:w" s="T1095">măndə</ts>
                  <nts id="Seg_3964" n="HIAT:ip">,</nts>
                  <nts id="Seg_3965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_3967" n="HIAT:w" s="T1096">štobɨ</ts>
                  <nts id="Seg_3968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_3970" n="HIAT:w" s="T1097">măn</ts>
                  <nts id="Seg_3971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_3973" n="HIAT:w" s="T1098">ibiem</ts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_3976" n="HIAT:w" s="T1099">морской</ts>
                  <nts id="Seg_3977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_3979" n="HIAT:w" s="T1100">владычицей</ts>
                  <nts id="Seg_3980" n="HIAT:ip">.</nts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1109" id="Seg_3982" n="sc" s="T1102">
               <ts e="T1109" id="Seg_3984" n="HIAT:u" s="T1102">
                  <ts e="T1103" id="Seg_3986" n="HIAT:w" s="T1102">I</ts>
                  <nts id="Seg_3987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_3989" n="HIAT:w" s="T1103">kola</ts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_3992" n="HIAT:w" s="T1104">štobɨ</ts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_3995" n="HIAT:w" s="T1105">măna</ts>
                  <nts id="Seg_3996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_3998" n="HIAT:w" s="T1106">tažerbi</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_4001" n="HIAT:w" s="T1107">bar</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_4004" n="HIAT:w" s="T1108">ĭmbi</ts>
                  <nts id="Seg_4005" n="HIAT:ip">"</nts>
                  <nts id="Seg_4006" n="HIAT:ip">.</nts>
                  <nts id="Seg_4007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1121" id="Seg_4008" n="sc" s="T1110">
               <ts e="T1121" id="Seg_4010" n="HIAT:u" s="T1110">
                  <ts e="T1111" id="Seg_4012" n="HIAT:w" s="T1110">Dĭgəttə</ts>
                  <nts id="Seg_4013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_4015" n="HIAT:w" s="T1111">kola</ts>
                  <nts id="Seg_4016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_4018" n="HIAT:w" s="T1112">kambi</ts>
                  <nts id="Seg_4019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_4021" n="HIAT:w" s="T1113">bünə</ts>
                  <nts id="Seg_4022" n="HIAT:ip">,</nts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4024" n="HIAT:ip">(</nts>
                  <ts e="T1115" id="Seg_4026" n="HIAT:w" s="T1114">ĭmbi</ts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_4029" n="HIAT:w" s="T1115">st-</ts>
                  <nts id="Seg_4030" n="HIAT:ip">)</nts>
                  <nts id="Seg_4031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_4033" n="HIAT:w" s="T1116">ĭmbidə</ts>
                  <nts id="Seg_4034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4035" n="HIAT:ip">(</nts>
                  <ts e="T1118" id="Seg_4037" n="HIAT:w" s="T1117">st-</ts>
                  <nts id="Seg_4038" n="HIAT:ip">)</nts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_4041" n="HIAT:w" s="T1118">büzʼenə</ts>
                  <nts id="Seg_4042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_4044" n="HIAT:w" s="T1119">ej</ts>
                  <nts id="Seg_4045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1121" id="Seg_4047" n="HIAT:w" s="T1120">nörbəbi</ts>
                  <nts id="Seg_4048" n="HIAT:ip">.</nts>
                  <nts id="Seg_4049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1125" id="Seg_4050" n="sc" s="T1122">
               <ts e="T1125" id="Seg_4052" n="HIAT:u" s="T1122">
                  <ts e="T1123" id="Seg_4054" n="HIAT:w" s="T1122">Dĭ</ts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_4057" n="HIAT:w" s="T1123">šobi</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4059" n="HIAT:ip">(</nts>
                  <ts e="T1125" id="Seg_4061" n="HIAT:w" s="T1124">maːʔndə</ts>
                  <nts id="Seg_4062" n="HIAT:ip">)</nts>
                  <nts id="Seg_4063" n="HIAT:ip">.</nts>
                  <nts id="Seg_4064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1133" id="Seg_4065" n="sc" s="T1126">
               <ts e="T1133" id="Seg_4067" n="HIAT:u" s="T1126">
                  <ts e="T1127" id="Seg_4069" n="HIAT:w" s="T1126">Nüke</ts>
                  <nts id="Seg_4070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_4072" n="HIAT:w" s="T1127">amnolaʔbə</ts>
                  <nts id="Seg_4073" n="HIAT:ip">,</nts>
                  <nts id="Seg_4074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_4076" n="HIAT:w" s="T1128">jererlaʔbə</ts>
                  <nts id="Seg_4077" n="HIAT:ip">,</nts>
                  <nts id="Seg_4078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_4080" n="HIAT:w" s="T1129">i</ts>
                  <nts id="Seg_4081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4082" n="HIAT:ip">(</nts>
                  <ts e="T1131" id="Seg_4084" n="HIAT:w" s="T1130">kărɨtă=</ts>
                  <nts id="Seg_4085" n="HIAT:ip">)</nts>
                  <nts id="Seg_4086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_4088" n="HIAT:w" s="T1131">kărɨtăt</ts>
                  <nts id="Seg_4089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_4091" n="HIAT:w" s="T1132">nulaʔbə</ts>
                  <nts id="Seg_4092" n="HIAT:ip">.</nts>
                  <nts id="Seg_4093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T5" id="Seg_4094" n="sc" s="T1">
               <ts e="T2" id="Seg_4096" n="e" s="T1">Šide </ts>
               <ts e="T3" id="Seg_4098" n="e" s="T2">ulardə </ts>
               <ts e="T4" id="Seg_4100" n="e" s="T3">i </ts>
               <ts e="T5" id="Seg_4102" n="e" s="T4">nüke. </ts>
            </ts>
            <ts e="T25" id="Seg_4103" n="sc" s="T6">
               <ts e="T7" id="Seg_4105" n="e" s="T6">Šide </ts>
               <ts e="T8" id="Seg_4107" n="e" s="T7">koʔbdo </ts>
               <ts e="T9" id="Seg_4109" n="e" s="T8">ibi. </ts>
               <ts e="T10" id="Seg_4111" n="e" s="T9">Šide </ts>
               <ts e="T11" id="Seg_4113" n="e" s="T10">koʔbdo </ts>
               <ts e="T12" id="Seg_4115" n="e" s="T11">ibi, </ts>
               <ts e="T13" id="Seg_4117" n="e" s="T12">dĭgəttə </ts>
               <ts e="T14" id="Seg_4119" n="e" s="T13">(u-) </ts>
               <ts e="T15" id="Seg_4121" n="e" s="T14">ular </ts>
               <ts e="T16" id="Seg_4123" n="e" s="T15">tabun </ts>
               <ts e="T17" id="Seg_4125" n="e" s="T16">ibi. </ts>
               <ts e="T18" id="Seg_4127" n="e" s="T17">Dĭgəttə </ts>
               <ts e="T19" id="Seg_4129" n="e" s="T18">tabun </ts>
               <ts e="T20" id="Seg_4131" n="e" s="T19">ine </ts>
               <ts e="T21" id="Seg_4133" n="e" s="T20">(ibiʔi). </ts>
               <ts e="T22" id="Seg_4135" n="e" s="T21">Dĭgəttə </ts>
               <ts e="T23" id="Seg_4137" n="e" s="T22">tabun </ts>
               <ts e="T24" id="Seg_4139" n="e" s="T23">tüžöj </ts>
               <ts e="T25" id="Seg_4141" n="e" s="T24">ibiʔi. </ts>
            </ts>
            <ts e="T49" id="Seg_4142" n="sc" s="T26">
               <ts e="T27" id="Seg_4144" n="e" s="T26">Dĭgəttə </ts>
               <ts e="T28" id="Seg_4146" n="e" s="T27">onʼiʔ </ts>
               <ts e="T29" id="Seg_4148" n="e" s="T28">măndərbi </ts>
               <ts e="T30" id="Seg_4150" n="e" s="T29">a </ts>
               <ts e="T31" id="Seg_4152" n="e" s="T30">onʼiʔ </ts>
               <ts e="T32" id="Seg_4154" n="e" s="T31">bar… </ts>
               <ts e="T33" id="Seg_4156" n="e" s="T32">((BRK)) </ts>
               <ts e="T34" id="Seg_4158" n="e" s="T33">Dĭgəttə </ts>
               <ts e="T35" id="Seg_4160" n="e" s="T34">onʼiʔ </ts>
               <ts e="T36" id="Seg_4162" n="e" s="T35">unu </ts>
               <ts e="T37" id="Seg_4164" n="e" s="T36">(mĭnzərbi=) </ts>
               <ts e="T38" id="Seg_4166" n="e" s="T37">măndərbi </ts>
               <ts e="T39" id="Seg_4168" n="e" s="T38">dĭn </ts>
               <ts e="T40" id="Seg_4170" n="e" s="T39">kodurgən, </ts>
               <ts e="T41" id="Seg_4172" n="e" s="T40">a </ts>
               <ts e="T42" id="Seg_4174" n="e" s="T41">onʼiʔ </ts>
               <ts e="T43" id="Seg_4176" n="e" s="T42">svinʼes </ts>
               <ts e="T44" id="Seg_4178" n="e" s="T43">mĭnzərbi. </ts>
               <ts e="T45" id="Seg_4180" n="e" s="T44">Dĭgəttə </ts>
               <ts e="T46" id="Seg_4182" n="e" s="T45">deʔpi. </ts>
               <ts e="T47" id="Seg_4184" n="e" s="T46">I </ts>
               <ts e="T48" id="Seg_4186" n="e" s="T47">kunə </ts>
               <ts e="T49" id="Seg_4188" n="e" s="T48">kămnəbi. </ts>
            </ts>
            <ts e="T86" id="Seg_4189" n="sc" s="T50">
               <ts e="T51" id="Seg_4191" n="e" s="T50">(Ku-) </ts>
               <ts e="T52" id="Seg_4193" n="e" s="T51">Kunə </ts>
               <ts e="T53" id="Seg_4195" n="e" s="T52">kămnəbi </ts>
               <ts e="T54" id="Seg_4197" n="e" s="T53">i </ts>
               <ts e="T55" id="Seg_4199" n="e" s="T54">dĭ </ts>
               <ts e="T56" id="Seg_4201" n="e" s="T55">bar </ts>
               <ts e="T57" id="Seg_4203" n="e" s="T56">külambi. </ts>
               <ts e="T58" id="Seg_4205" n="e" s="T57">((BRK)) </ts>
               <ts e="T59" id="Seg_4207" n="e" s="T58">Dĭgəttə </ts>
               <ts e="T60" id="Seg_4209" n="e" s="T59">dĭn </ts>
               <ts e="T61" id="Seg_4211" n="e" s="T60">nüjnə </ts>
               <ts e="T62" id="Seg_4213" n="e" s="T61">bar </ts>
               <ts e="T63" id="Seg_4215" n="e" s="T62">külaːmbi. </ts>
               <ts e="T64" id="Seg_4217" n="e" s="T63">Dĭgəttə </ts>
               <ts e="T65" id="Seg_4219" n="e" s="T64">dĭ </ts>
               <ts e="T66" id="Seg_4221" n="e" s="T65">koʔbsaŋ </ts>
               <ts e="T67" id="Seg_4223" n="e" s="T66">ibiʔi </ts>
               <ts e="T68" id="Seg_4225" n="e" s="T67">ular, </ts>
               <ts e="T69" id="Seg_4227" n="e" s="T68">ineʔi </ts>
               <ts e="T70" id="Seg_4229" n="e" s="T69">i </ts>
               <ts e="T71" id="Seg_4231" n="e" s="T70">tüžöjʔi </ts>
               <ts e="T72" id="Seg_4233" n="e" s="T71">i </ts>
               <ts e="T73" id="Seg_4235" n="e" s="T72">abandə </ts>
               <ts e="T74" id="Seg_4237" n="e" s="T73">(ka-) </ts>
               <ts e="T1134" id="Seg_4239" n="e" s="T74">kalla </ts>
               <ts e="T75" id="Seg_4241" n="e" s="T1134">dʼürbiʔi. </ts>
               <ts e="T76" id="Seg_4243" n="e" s="T75">((BRK)) </ts>
               <ts e="T77" id="Seg_4245" n="e" s="T76">Dĭgəttə </ts>
               <ts e="T78" id="Seg_4247" n="e" s="T77">bar </ts>
               <ts e="T79" id="Seg_4249" n="e" s="T78">skazka. </ts>
               <ts e="T80" id="Seg_4251" n="e" s="T79">((BRK)) </ts>
               <ts e="T81" id="Seg_4253" n="e" s="T80">Tumo </ts>
               <ts e="T82" id="Seg_4255" n="e" s="T81">i </ts>
               <ts e="T83" id="Seg_4257" n="e" s="T82">kös </ts>
               <ts e="T84" id="Seg_4259" n="e" s="T83">i </ts>
               <ts e="T85" id="Seg_4261" n="e" s="T84">puzɨrʼ </ts>
               <ts e="T86" id="Seg_4263" n="e" s="T85">kambiʔi. </ts>
            </ts>
            <ts e="T103" id="Seg_4264" n="sc" s="T87">
               <ts e="T88" id="Seg_4266" n="e" s="T87">Dʼăga, </ts>
               <ts e="T89" id="Seg_4268" n="e" s="T88">a </ts>
               <ts e="T90" id="Seg_4270" n="e" s="T89">dĭ </ts>
               <ts e="T91" id="Seg_4272" n="e" s="T90">kanzittə </ts>
               <ts e="T92" id="Seg_4274" n="e" s="T91">dʼăga, </ts>
               <ts e="T93" id="Seg_4276" n="e" s="T92">dʼăga </ts>
               <ts e="T94" id="Seg_4278" n="e" s="T93">mʼaŋnaʔbə. </ts>
               <ts e="T95" id="Seg_4280" n="e" s="T94">Tumo </ts>
               <ts e="T96" id="Seg_4282" n="e" s="T95">bar </ts>
               <ts e="T97" id="Seg_4284" n="e" s="T96">moʔi </ts>
               <ts e="T98" id="Seg_4286" n="e" s="T97">embi. </ts>
               <ts e="T99" id="Seg_4288" n="e" s="T98">Dĭgəttə </ts>
               <ts e="T100" id="Seg_4290" n="e" s="T99">tumo </ts>
               <ts e="T1135" id="Seg_4292" n="e" s="T100">kalla </ts>
               <ts e="T101" id="Seg_4294" n="e" s="T1135">dʼürbi. </ts>
               <ts e="T102" id="Seg_4296" n="e" s="T101">Puzɨrʼ </ts>
               <ts e="T1136" id="Seg_4298" n="e" s="T102">kalla </ts>
               <ts e="T103" id="Seg_4300" n="e" s="T1136">dʼürbi. </ts>
            </ts>
            <ts e="T114" id="Seg_4301" n="sc" s="T104">
               <ts e="T105" id="Seg_4303" n="e" s="T104">A </ts>
               <ts e="T106" id="Seg_4305" n="e" s="T105">(köš- </ts>
               <ts e="T107" id="Seg_4307" n="e" s="T106">kam-) </ts>
               <ts e="T108" id="Seg_4309" n="e" s="T107">kös </ts>
               <ts e="T109" id="Seg_4311" n="e" s="T108">kambi </ts>
               <ts e="T110" id="Seg_4313" n="e" s="T109">i </ts>
               <ts e="T111" id="Seg_4315" n="e" s="T110">bünə </ts>
               <ts e="T112" id="Seg_4317" n="e" s="T111">saʔməluʔpi </ts>
               <ts e="T113" id="Seg_4319" n="e" s="T112">i </ts>
               <ts e="T114" id="Seg_4321" n="e" s="T113">külambi. </ts>
            </ts>
            <ts e="T121" id="Seg_4322" n="sc" s="T115">
               <ts e="T116" id="Seg_4324" n="e" s="T115">A </ts>
               <ts e="T117" id="Seg_4326" n="e" s="T116">puzɨrʼ </ts>
               <ts e="T118" id="Seg_4328" n="e" s="T117">kaknarbi, </ts>
               <ts e="T119" id="Seg_4330" n="e" s="T118">kaknarbi </ts>
               <ts e="T120" id="Seg_4332" n="e" s="T119">i </ts>
               <ts e="T121" id="Seg_4334" n="e" s="T120">külambi. </ts>
            </ts>
            <ts e="T132" id="Seg_4335" n="sc" s="T122">
               <ts e="T123" id="Seg_4337" n="e" s="T122">Tumo </ts>
               <ts e="T124" id="Seg_4339" n="e" s="T123">unnʼa </ts>
               <ts e="T125" id="Seg_4341" n="e" s="T124">(amno-) </ts>
               <ts e="T126" id="Seg_4343" n="e" s="T125">maluʔpi. </ts>
               <ts e="T127" id="Seg_4345" n="e" s="T126">Dĭgəttə </ts>
               <ts e="T128" id="Seg_4347" n="e" s="T127">kambi, </ts>
               <ts e="T129" id="Seg_4349" n="e" s="T128">kambi. </ts>
               <ts e="T130" id="Seg_4351" n="e" s="T129">Esseŋ </ts>
               <ts e="T131" id="Seg_4353" n="e" s="T130">bar </ts>
               <ts e="T132" id="Seg_4355" n="e" s="T131">sʼarlaʔbəʔjə. </ts>
            </ts>
            <ts e="T137" id="Seg_4356" n="sc" s="T133">
               <ts e="T134" id="Seg_4358" n="e" s="T133">Bü </ts>
               <ts e="T135" id="Seg_4360" n="e" s="T134">konnambi </ts>
               <ts e="T136" id="Seg_4362" n="e" s="T135">mu </ts>
               <ts e="T137" id="Seg_4364" n="e" s="T136">bügən. </ts>
            </ts>
            <ts e="T149" id="Seg_4365" n="sc" s="T138">
               <ts e="T139" id="Seg_4367" n="e" s="T138">"Esseŋ, </ts>
               <ts e="T140" id="Seg_4369" n="e" s="T139">(ia=) </ts>
               <ts e="T141" id="Seg_4371" n="e" s="T140">abat </ts>
               <ts e="T142" id="Seg_4373" n="e" s="T141">idjot </ts>
               <ts e="T143" id="Seg_4375" n="e" s="T142">ian. </ts>
               <ts e="T144" id="Seg_4377" n="e" s="T143">Kangaʔ </ts>
               <ts e="T145" id="Seg_4379" n="e" s="T144">maʔnə </ts>
               <ts e="T146" id="Seg_4381" n="e" s="T145">da </ts>
               <ts e="T147" id="Seg_4383" n="e" s="T146">(măndlaʔ-) </ts>
               <ts e="T148" id="Seg_4385" n="e" s="T147">mănaʔ </ts>
               <ts e="T149" id="Seg_4387" n="e" s="T148">ianə". </ts>
            </ts>
            <ts e="T153" id="Seg_4388" n="sc" s="T150">
               <ts e="T151" id="Seg_4390" n="e" s="T150">Dĭzeŋ </ts>
               <ts e="T152" id="Seg_4392" n="e" s="T151">nuʔməleʔ </ts>
               <ts e="T153" id="Seg_4394" n="e" s="T152">šobiʔi. </ts>
            </ts>
            <ts e="T156" id="Seg_4395" n="sc" s="T154">
               <ts e="T155" id="Seg_4397" n="e" s="T154">"Abal </ts>
               <ts e="T156" id="Seg_4399" n="e" s="T155">šonəga!" </ts>
            </ts>
            <ts e="T164" id="Seg_4400" n="sc" s="T157">
               <ts e="T158" id="Seg_4402" n="e" s="T157">"No </ts>
               <ts e="T159" id="Seg_4404" n="e" s="T158">kangaʔ, </ts>
               <ts e="T160" id="Seg_4406" n="e" s="T159">kăštəgaʔ. </ts>
               <ts e="T161" id="Seg_4408" n="e" s="T160">Măn </ts>
               <ts e="T162" id="Seg_4410" n="e" s="T161">tüjö </ts>
               <ts e="T163" id="Seg_4412" n="e" s="T162">uja </ts>
               <ts e="T164" id="Seg_4414" n="e" s="T163">mĭnzərləm". </ts>
            </ts>
            <ts e="T172" id="Seg_4415" n="sc" s="T165">
               <ts e="T166" id="Seg_4417" n="e" s="T165">Dĭzeŋ </ts>
               <ts e="T167" id="Seg_4419" n="e" s="T166">nuʔməluʔbiʔi: </ts>
               <ts e="T168" id="Seg_4421" n="e" s="T167">"Šoʔ, </ts>
               <ts e="T169" id="Seg_4423" n="e" s="T168">iam </ts>
               <ts e="T170" id="Seg_4425" n="e" s="T169">ujam </ts>
               <ts e="T171" id="Seg_4427" n="e" s="T170">(mĭnz-) </ts>
               <ts e="T172" id="Seg_4429" n="e" s="T171">mĭnzərləʔbəʔjə. </ts>
            </ts>
            <ts e="T174" id="Seg_4430" n="sc" s="T173">
               <ts e="T174" id="Seg_4432" n="e" s="T173">Üjüʔi". </ts>
            </ts>
            <ts e="T191" id="Seg_4433" n="sc" s="T175">
               <ts e="T176" id="Seg_4435" n="e" s="T175">"Măn </ts>
               <ts e="T177" id="Seg_4437" n="e" s="T176">tĭm </ts>
               <ts e="T178" id="Seg_4439" n="e" s="T177">em </ts>
               <ts e="T179" id="Seg_4441" n="e" s="T178">(na-) </ts>
               <ts e="T180" id="Seg_4443" n="e" s="T179">naga </ts>
               <ts e="T181" id="Seg_4445" n="e" s="T180">amzittə. </ts>
               <ts e="T182" id="Seg_4447" n="e" s="T181">Pušaj </ts>
               <ts e="T183" id="Seg_4449" n="e" s="T182">tüžöjdə </ts>
               <ts e="T184" id="Seg_4451" n="e" s="T183">bătləj </ts>
               <ts e="T185" id="Seg_4453" n="e" s="T184">i </ts>
               <ts e="T186" id="Seg_4455" n="e" s="T185">mĭnzərləj, </ts>
               <ts e="T187" id="Seg_4457" n="e" s="T186">kubanə </ts>
               <ts e="T188" id="Seg_4459" n="e" s="T187">nuldələj. </ts>
               <ts e="T189" id="Seg_4461" n="e" s="T188">Dĭ </ts>
               <ts e="T190" id="Seg_4463" n="e" s="T189">tüžöj </ts>
               <ts e="T191" id="Seg_4465" n="e" s="T190">băʔpi. </ts>
            </ts>
            <ts e="T196" id="Seg_4466" n="sc" s="T192">
               <ts e="T193" id="Seg_4468" n="e" s="T192">Uja </ts>
               <ts e="T194" id="Seg_4470" n="e" s="T193">mĭnzərbi, </ts>
               <ts e="T195" id="Seg_4472" n="e" s="T194">kubanə </ts>
               <ts e="T196" id="Seg_4474" n="e" s="T195">embi. </ts>
            </ts>
            <ts e="T203" id="Seg_4475" n="sc" s="T197">
               <ts e="T198" id="Seg_4477" n="e" s="T197">Dĭgəttə </ts>
               <ts e="T199" id="Seg_4479" n="e" s="T198">esseŋ </ts>
               <ts e="T200" id="Seg_4481" n="e" s="T199">kambiʔi: </ts>
               <ts e="T201" id="Seg_4483" n="e" s="T200">"Šoʔ, </ts>
               <ts e="T202" id="Seg_4485" n="e" s="T201">uja </ts>
               <ts e="T203" id="Seg_4487" n="e" s="T202">mĭnzərlona!" </ts>
            </ts>
            <ts e="T205" id="Seg_4488" n="sc" s="T204">
               <ts e="T205" id="Seg_4490" n="e" s="T204">Šobiʔi. </ts>
            </ts>
            <ts e="T208" id="Seg_4491" n="sc" s="T206">
               <ts e="T207" id="Seg_4493" n="e" s="T206">"Gijen </ts>
               <ts e="T208" id="Seg_4495" n="e" s="T207">abam?" </ts>
            </ts>
            <ts e="T213" id="Seg_4496" n="sc" s="T209">
               <ts e="T210" id="Seg_4498" n="e" s="T209">Davaj </ts>
               <ts e="T211" id="Seg_4500" n="e" s="T210">dĭzem </ts>
               <ts e="T212" id="Seg_4502" n="e" s="T211">münörzittə, </ts>
               <ts e="T213" id="Seg_4504" n="e" s="T212">esseŋdə. </ts>
            </ts>
            <ts e="T222" id="Seg_4505" n="sc" s="T214">
               <ts e="T215" id="Seg_4507" n="e" s="T214">A </ts>
               <ts e="T216" id="Seg_4509" n="e" s="T215">tumo </ts>
               <ts e="T217" id="Seg_4511" n="e" s="T216">amnolaʔbə: </ts>
               <ts e="T218" id="Seg_4513" n="e" s="T217">"Iʔ </ts>
               <ts e="T219" id="Seg_4515" n="e" s="T218">dʼabəroʔ, </ts>
               <ts e="T220" id="Seg_4517" n="e" s="T219">măn </ts>
               <ts e="T221" id="Seg_4519" n="e" s="T220">dön, </ts>
               <ts e="T222" id="Seg_4521" n="e" s="T221">šobiam!" </ts>
            </ts>
            <ts e="T227" id="Seg_4522" n="sc" s="T223">
               <ts e="T224" id="Seg_4524" n="e" s="T223">Teinen </ts>
               <ts e="T225" id="Seg_4526" n="e" s="T224">bar </ts>
               <ts e="T226" id="Seg_4528" n="e" s="T225">sĭre </ts>
               <ts e="T227" id="Seg_4530" n="e" s="T226">saʔməlaʔbə. </ts>
            </ts>
            <ts e="T231" id="Seg_4531" n="sc" s="T228">
               <ts e="T229" id="Seg_4533" n="e" s="T228">I </ts>
               <ts e="T230" id="Seg_4535" n="e" s="T229">beržə </ts>
               <ts e="T231" id="Seg_4537" n="e" s="T230">šonəga. </ts>
            </ts>
            <ts e="T237" id="Seg_4538" n="sc" s="T232">
               <ts e="T233" id="Seg_4540" n="e" s="T232">Ugandə </ts>
               <ts e="T234" id="Seg_4542" n="e" s="T233">šišəge </ts>
               <ts e="T235" id="Seg_4544" n="e" s="T234">(nʼiʔnen </ts>
               <ts e="T236" id="Seg_4546" n="e" s="T235">nʼiʔ-) </ts>
               <ts e="T237" id="Seg_4548" n="e" s="T236">nʼiʔnen. </ts>
            </ts>
            <ts e="T241" id="Seg_4549" n="sc" s="T238">
               <ts e="T239" id="Seg_4551" n="e" s="T238">Kumen </ts>
               <ts e="T240" id="Seg_4553" n="e" s="T239">kö </ts>
               <ts e="T241" id="Seg_4555" n="e" s="T240">eššinə? </ts>
            </ts>
            <ts e="T243" id="Seg_4556" n="sc" s="T242">
               <ts e="T243" id="Seg_4558" n="e" s="T242">Nagur. </ts>
            </ts>
            <ts e="T249" id="Seg_4559" n="sc" s="T244">
               <ts e="T245" id="Seg_4561" n="e" s="T244">Dö </ts>
               <ts e="T246" id="Seg_4563" n="e" s="T245">kön </ts>
               <ts e="T247" id="Seg_4565" n="e" s="T246">ugandə </ts>
               <ts e="T248" id="Seg_4567" n="e" s="T247">šišəge </ts>
               <ts e="T249" id="Seg_4569" n="e" s="T248">ibi. </ts>
            </ts>
            <ts e="T253" id="Seg_4570" n="sc" s="T250">
               <ts e="T251" id="Seg_4572" n="e" s="T250">Kăndlaʔpiʔi </ts>
               <ts e="T252" id="Seg_4574" n="e" s="T251">il </ts>
               <ts e="T253" id="Seg_4576" n="e" s="T252">bar. </ts>
            </ts>
            <ts e="T255" id="Seg_4577" n="sc" s="T254">
               <ts e="T255" id="Seg_4579" n="e" s="T254">((BRK)). </ts>
            </ts>
            <ts e="T259" id="Seg_4580" n="sc" s="T256">
               <ts e="T257" id="Seg_4582" n="e" s="T256">Noʔ </ts>
               <ts e="T258" id="Seg_4584" n="e" s="T257">jaʔpiam </ts>
               <ts e="T259" id="Seg_4586" n="e" s="T258">šapkuzi. </ts>
            </ts>
            <ts e="T271" id="Seg_4587" n="sc" s="T260">
               <ts e="T261" id="Seg_4589" n="e" s="T260">Dĭgəttə </ts>
               <ts e="T262" id="Seg_4591" n="e" s="T261">koʔlambi </ts>
               <ts e="T263" id="Seg_4593" n="e" s="T262">noʔ, </ts>
               <ts e="T264" id="Seg_4595" n="e" s="T263">măn </ts>
               <ts e="T265" id="Seg_4597" n="e" s="T264">dĭm </ts>
               <ts e="T266" id="Seg_4599" n="e" s="T265">oʔbdəbiam </ts>
               <ts e="T267" id="Seg_4601" n="e" s="T266">kăpnanə, </ts>
               <ts e="T268" id="Seg_4603" n="e" s="T267">dĭgəttə </ts>
               <ts e="T269" id="Seg_4605" n="e" s="T268">ine </ts>
               <ts e="T270" id="Seg_4607" n="e" s="T269">ibiem, </ts>
               <ts e="T271" id="Seg_4609" n="e" s="T270">tăžerbiam. </ts>
            </ts>
            <ts e="T274" id="Seg_4610" n="sc" s="T272">
               <ts e="T273" id="Seg_4612" n="e" s="T272">Embiem </ts>
               <ts e="T274" id="Seg_4614" n="e" s="T273">zarottə. </ts>
            </ts>
            <ts e="T276" id="Seg_4615" n="sc" s="T275">
               <ts e="T276" id="Seg_4617" n="e" s="T275">((BRK)). </ts>
            </ts>
            <ts e="T286" id="Seg_4618" n="sc" s="T277">
               <ts e="T278" id="Seg_4620" n="e" s="T277">Dö </ts>
               <ts e="T279" id="Seg_4622" n="e" s="T278">pʼen </ts>
               <ts e="T280" id="Seg_4624" n="e" s="T279">sagər </ts>
               <ts e="T281" id="Seg_4626" n="e" s="T280">keʔbde </ts>
               <ts e="T282" id="Seg_4628" n="e" s="T281">naga, </ts>
               <ts e="T283" id="Seg_4630" n="e" s="T282">a </ts>
               <ts e="T284" id="Seg_4632" n="e" s="T283">kömə </ts>
               <ts e="T285" id="Seg_4634" n="e" s="T284">keʔbde </ts>
               <ts e="T286" id="Seg_4636" n="e" s="T285">ibi. </ts>
            </ts>
            <ts e="T292" id="Seg_4637" n="sc" s="T287">
               <ts e="T288" id="Seg_4639" n="e" s="T287">Il </ts>
               <ts e="T289" id="Seg_4641" n="e" s="T288">iʔgö </ts>
               <ts e="T290" id="Seg_4643" n="e" s="T289">tažerbiʔi </ts>
               <ts e="T291" id="Seg_4645" n="e" s="T290">maːndə </ts>
               <ts e="T292" id="Seg_4647" n="e" s="T291">keʔbdeʔi. </ts>
            </ts>
            <ts e="T303" id="Seg_4648" n="sc" s="T293">
               <ts e="T294" id="Seg_4650" n="e" s="T293">Sanə </ts>
               <ts e="T295" id="Seg_4652" n="e" s="T294">iʔgö </ts>
               <ts e="T296" id="Seg_4654" n="e" s="T295">ibi </ts>
               <ts e="T297" id="Seg_4656" n="e" s="T296">bar, </ts>
               <ts e="T298" id="Seg_4658" n="e" s="T297">(i-) </ts>
               <ts e="T299" id="Seg_4660" n="e" s="T298">il </ts>
               <ts e="T300" id="Seg_4662" n="e" s="T299">ugandə </ts>
               <ts e="T301" id="Seg_4664" n="e" s="T300">iʔgö </ts>
               <ts e="T302" id="Seg_4666" n="e" s="T301">oʔbdəbiʔi </ts>
               <ts e="T303" id="Seg_4668" n="e" s="T302">sanə. </ts>
            </ts>
            <ts e="T305" id="Seg_4669" n="sc" s="T304">
               <ts e="T305" id="Seg_4671" n="e" s="T304">((BRK)). </ts>
            </ts>
            <ts e="T311" id="Seg_4672" n="sc" s="T306">
               <ts e="T307" id="Seg_4674" n="e" s="T306">Nagur </ts>
               <ts e="T308" id="Seg_4676" n="e" s="T307">pʼe </ts>
               <ts e="T309" id="Seg_4678" n="e" s="T308">nagobi </ts>
               <ts e="T310" id="Seg_4680" n="e" s="T309">(ž) </ts>
               <ts e="T311" id="Seg_4682" n="e" s="T310">sanə. </ts>
            </ts>
            <ts e="T313" id="Seg_4683" n="sc" s="T312">
               <ts e="T313" id="Seg_4685" n="e" s="T312">((BRK)). </ts>
            </ts>
            <ts e="T324" id="Seg_4686" n="sc" s="T314">
               <ts e="T315" id="Seg_4688" n="e" s="T314">Sanə </ts>
               <ts e="T316" id="Seg_4690" n="e" s="T315">toʔnarzittə </ts>
               <ts e="T317" id="Seg_4692" n="e" s="T316">ne </ts>
               <ts e="T318" id="Seg_4694" n="e" s="T317">nado, </ts>
               <ts e="T319" id="Seg_4696" n="e" s="T318">dĭzeŋ </ts>
               <ts e="T320" id="Seg_4698" n="e" s="T319">bostə </ts>
               <ts e="T321" id="Seg_4700" n="e" s="T320">saʔməluʔpiʔi, </ts>
               <ts e="T322" id="Seg_4702" n="e" s="T321">a </ts>
               <ts e="T323" id="Seg_4704" n="e" s="T322">il </ts>
               <ts e="T324" id="Seg_4706" n="e" s="T323">oʔbdəlaʔbəʔjə. </ts>
            </ts>
            <ts e="T326" id="Seg_4707" n="sc" s="T325">
               <ts e="T326" id="Seg_4709" n="e" s="T325">((BRK)). </ts>
            </ts>
            <ts e="T333" id="Seg_4710" n="sc" s="T327">
               <ts e="T328" id="Seg_4712" n="e" s="T327">Dö </ts>
               <ts e="T329" id="Seg_4714" n="e" s="T328">pʼen </ts>
               <ts e="T330" id="Seg_4716" n="e" s="T329">len </ts>
               <ts e="T331" id="Seg_4718" n="e" s="T330">keʔbde </ts>
               <ts e="T332" id="Seg_4720" n="e" s="T331">nagobi </ts>
               <ts e="T333" id="Seg_4722" n="e" s="T332">tože. </ts>
            </ts>
            <ts e="T335" id="Seg_4723" n="sc" s="T334">
               <ts e="T335" id="Seg_4725" n="e" s="T334">((BRK)). </ts>
            </ts>
            <ts e="T342" id="Seg_4726" n="sc" s="T336">
               <ts e="T337" id="Seg_4728" n="e" s="T336">Dö </ts>
               <ts e="T338" id="Seg_4730" n="e" s="T337">kö </ts>
               <ts e="T339" id="Seg_4732" n="e" s="T338">ugandə </ts>
               <ts e="T340" id="Seg_4734" n="e" s="T339">budəj </ts>
               <ts e="T341" id="Seg_4736" n="e" s="T340">jakše </ts>
               <ts e="T342" id="Seg_4738" n="e" s="T341">ibi. </ts>
            </ts>
            <ts e="T351" id="Seg_4739" n="sc" s="T343">
               <ts e="T344" id="Seg_4741" n="e" s="T343">I </ts>
               <ts e="T345" id="Seg_4743" n="e" s="T344">toltanoʔ </ts>
               <ts e="T346" id="Seg_4745" n="e" s="T345">jakše </ts>
               <ts e="T347" id="Seg_4747" n="e" s="T346">ibi, </ts>
               <ts e="T348" id="Seg_4749" n="e" s="T347">iʔgö </ts>
               <ts e="T349" id="Seg_4751" n="e" s="T348">özerbi, </ts>
               <ts e="T350" id="Seg_4753" n="e" s="T349">urgo </ts>
               <ts e="T351" id="Seg_4755" n="e" s="T350">ibi. </ts>
            </ts>
            <ts e="T353" id="Seg_4756" n="sc" s="T352">
               <ts e="T353" id="Seg_4758" n="e" s="T352">((BRK)). </ts>
            </ts>
            <ts e="T360" id="Seg_4759" n="sc" s="T354">
               <ts e="T355" id="Seg_4761" n="e" s="T354">Sĭre </ts>
               <ts e="T356" id="Seg_4763" n="e" s="T355">ipek </ts>
               <ts e="T357" id="Seg_4765" n="e" s="T356">(pür-) </ts>
               <ts e="T358" id="Seg_4767" n="e" s="T357">pürlieʔi </ts>
               <ts e="T359" id="Seg_4769" n="e" s="T358">il </ts>
               <ts e="T360" id="Seg_4771" n="e" s="T359">ugandə. </ts>
            </ts>
            <ts e="T365" id="Seg_4772" n="sc" s="T361">
               <ts e="T362" id="Seg_4774" n="e" s="T361">Köbergən </ts>
               <ts e="T363" id="Seg_4776" n="e" s="T362">bar </ts>
               <ts e="T364" id="Seg_4778" n="e" s="T363">külambi </ts>
               <ts e="T365" id="Seg_4780" n="e" s="T364">ăgărotkən. </ts>
            </ts>
            <ts e="T367" id="Seg_4781" n="sc" s="T366">
               <ts e="T367" id="Seg_4783" n="e" s="T366">A… </ts>
            </ts>
            <ts e="T369" id="Seg_4784" n="sc" s="T368">
               <ts e="T369" id="Seg_4786" n="e" s="T368">((BRK)). </ts>
            </ts>
            <ts e="T375" id="Seg_4787" n="sc" s="T370">
               <ts e="T371" id="Seg_4789" n="e" s="T370">A </ts>
               <ts e="T372" id="Seg_4791" n="e" s="T371">dʼijegən </ts>
               <ts e="T373" id="Seg_4793" n="e" s="T372">köbergən </ts>
               <ts e="T374" id="Seg_4795" n="e" s="T373">jakše </ts>
               <ts e="T375" id="Seg_4797" n="e" s="T374">ibi. </ts>
            </ts>
            <ts e="T377" id="Seg_4798" n="sc" s="T376">
               <ts e="T377" id="Seg_4800" n="e" s="T376">((BRK)). </ts>
            </ts>
            <ts e="T385" id="Seg_4801" n="sc" s="T378">
               <ts e="T379" id="Seg_4803" n="e" s="T378">Măn </ts>
               <ts e="T380" id="Seg_4805" n="e" s="T379">(neg- </ts>
               <ts e="T381" id="Seg_4807" n="e" s="T380">nĭg-) </ts>
               <ts e="T382" id="Seg_4809" n="e" s="T381">nĭŋgəbiem, </ts>
               <ts e="T383" id="Seg_4811" n="e" s="T382">dʼagarbiam </ts>
               <ts e="T384" id="Seg_4813" n="e" s="T383">i </ts>
               <ts e="T385" id="Seg_4815" n="e" s="T384">koʔlaʔpi. </ts>
            </ts>
            <ts e="T391" id="Seg_4816" n="sc" s="T386">
               <ts e="T387" id="Seg_4818" n="e" s="T386">A </ts>
               <ts e="T388" id="Seg_4820" n="e" s="T387">tüj </ts>
               <ts e="T389" id="Seg_4822" n="e" s="T388">mĭnzərliem </ts>
               <ts e="T390" id="Seg_4824" n="e" s="T389">ujazi, </ts>
               <ts e="T391" id="Seg_4826" n="e" s="T390">toltanoʔsi. </ts>
            </ts>
            <ts e="T393" id="Seg_4827" n="sc" s="T392">
               <ts e="T393" id="Seg_4829" n="e" s="T392">((BRK)). </ts>
            </ts>
            <ts e="T407" id="Seg_4830" n="sc" s="T394">
               <ts e="T395" id="Seg_4832" n="e" s="T394">А </ts>
               <ts e="T396" id="Seg_4834" n="e" s="T395">говорю </ts>
               <ts e="T397" id="Seg_4836" n="e" s="T396">следом. </ts>
               <ts e="T398" id="Seg_4838" n="e" s="T397">Nuzaŋgən </ts>
               <ts e="T399" id="Seg_4840" n="e" s="T398">(oʔb) </ts>
               <ts e="T400" id="Seg_4842" n="e" s="T399">kuza </ts>
               <ts e="T401" id="Seg_4844" n="e" s="T400">külambi. </ts>
               <ts e="T402" id="Seg_4846" n="e" s="T401">Dĭzeŋ </ts>
               <ts e="T403" id="Seg_4848" n="e" s="T402">bar </ts>
               <ts e="T404" id="Seg_4850" n="e" s="T403">tĭlbiʔi </ts>
               <ts e="T405" id="Seg_4852" n="e" s="T404">dʼü, </ts>
               <ts e="T406" id="Seg_4854" n="e" s="T405">dĭgəttə </ts>
               <ts e="T407" id="Seg_4856" n="e" s="T406">abiʔi… </ts>
            </ts>
            <ts e="T409" id="Seg_4857" n="sc" s="T408">
               <ts e="T409" id="Seg_4859" n="e" s="T408">((BRK)). </ts>
            </ts>
            <ts e="T418" id="Seg_4860" n="sc" s="T410">
               <ts e="T411" id="Seg_4862" n="e" s="T410">Dĭgəttə </ts>
               <ts e="T412" id="Seg_4864" n="e" s="T411">maʔ </ts>
               <ts e="T413" id="Seg_4866" n="e" s="T412">abiʔi. </ts>
               <ts e="T414" id="Seg_4868" n="e" s="T413">Krospa </ts>
               <ts e="T415" id="Seg_4870" n="e" s="T414">abiʔi, </ts>
               <ts e="T416" id="Seg_4872" n="e" s="T415">maʔtə </ts>
               <ts e="T417" id="Seg_4874" n="e" s="T416">dĭm </ts>
               <ts e="T418" id="Seg_4876" n="e" s="T417">embiʔi. </ts>
            </ts>
            <ts e="T427" id="Seg_4877" n="sc" s="T419">
               <ts e="T420" id="Seg_4879" n="e" s="T419">(Dĭ- </ts>
               <ts e="T421" id="Seg_4881" n="e" s="T420">dĭ-) </ts>
               <ts e="T422" id="Seg_4883" n="e" s="T421">Dĭ </ts>
               <ts e="T423" id="Seg_4885" n="e" s="T422">dĭʔnə </ts>
               <ts e="T424" id="Seg_4887" n="e" s="T423">kaŋza </ts>
               <ts e="T425" id="Seg_4889" n="e" s="T424">ambiʔi, </ts>
               <ts e="T426" id="Seg_4891" n="e" s="T425">taŋgu </ts>
               <ts e="T427" id="Seg_4893" n="e" s="T426">ambiʔi. </ts>
            </ts>
            <ts e="T431" id="Seg_4894" n="sc" s="T428">
               <ts e="T429" id="Seg_4896" n="e" s="T428">Multuk </ts>
               <ts e="T430" id="Seg_4898" n="e" s="T429">ambiʔi </ts>
               <ts e="T431" id="Seg_4900" n="e" s="T430">bar. </ts>
            </ts>
            <ts e="T435" id="Seg_4901" n="sc" s="T432">
               <ts e="T433" id="Seg_4903" n="e" s="T432">Aspaʔ </ts>
               <ts e="T434" id="Seg_4905" n="e" s="T433">(ambiʔi=) </ts>
               <ts e="T435" id="Seg_4907" n="e" s="T434">embiʔi. </ts>
            </ts>
            <ts e="T441" id="Seg_4908" n="sc" s="T436">
               <ts e="T437" id="Seg_4910" n="e" s="T436">Dĭ </ts>
               <ts e="T438" id="Seg_4912" n="e" s="T437">dĭn </ts>
               <ts e="T439" id="Seg_4914" n="e" s="T438">mĭnzərləj </ts>
               <ts e="T440" id="Seg_4916" n="e" s="T439">i </ts>
               <ts e="T441" id="Seg_4918" n="e" s="T440">amorləj. </ts>
            </ts>
            <ts e="T443" id="Seg_4919" n="sc" s="T442">
               <ts e="T443" id="Seg_4921" n="e" s="T442">((BRK)). </ts>
            </ts>
            <ts e="T454" id="Seg_4922" n="sc" s="T444">
               <ts e="T445" id="Seg_4924" n="e" s="T444">Dĭgəttə </ts>
               <ts e="T446" id="Seg_4926" n="e" s="T445">dĭ </ts>
               <ts e="T447" id="Seg_4928" n="e" s="T446">kuzam </ts>
               <ts e="T448" id="Seg_4930" n="e" s="T447">kumbiʔi, </ts>
               <ts e="T449" id="Seg_4932" n="e" s="T448">(dʼü-) </ts>
               <ts e="T450" id="Seg_4934" n="e" s="T449">dʼünə </ts>
               <ts e="T451" id="Seg_4936" n="e" s="T450">embiʔi, </ts>
               <ts e="T452" id="Seg_4938" n="e" s="T451">dʼüzi </ts>
               <ts e="T453" id="Seg_4940" n="e" s="T452">bar </ts>
               <ts e="T454" id="Seg_4942" n="e" s="T453">kămnəbiʔi. </ts>
            </ts>
            <ts e="T458" id="Seg_4943" n="sc" s="T455">
               <ts e="T456" id="Seg_4945" n="e" s="T455">Dĭgəttə </ts>
               <ts e="T457" id="Seg_4947" n="e" s="T456">maːndə </ts>
               <ts e="T458" id="Seg_4949" n="e" s="T457">šobiʔi. </ts>
            </ts>
            <ts e="T465" id="Seg_4950" n="sc" s="T459">
               <ts e="T460" id="Seg_4952" n="e" s="T459">Nüdʼin </ts>
               <ts e="T461" id="Seg_4954" n="e" s="T460">bar </ts>
               <ts e="T462" id="Seg_4956" n="e" s="T461">men </ts>
               <ts e="T463" id="Seg_4958" n="e" s="T462">sarbiʔi, </ts>
               <ts e="T464" id="Seg_4960" n="e" s="T463">da </ts>
               <ts e="T465" id="Seg_4962" n="e" s="T464">ine. </ts>
            </ts>
            <ts e="T468" id="Seg_4963" n="sc" s="T466">
               <ts e="T467" id="Seg_4965" n="e" s="T466">Dĭgəttə </ts>
               <ts e="T468" id="Seg_4967" n="e" s="T467">kanbiʔi. </ts>
            </ts>
            <ts e="T474" id="Seg_4968" n="sc" s="T469">
               <ts e="T470" id="Seg_4970" n="e" s="T469">"Oj, </ts>
               <ts e="T471" id="Seg_4972" n="e" s="T470">šonəga", — </ts>
               <ts e="T472" id="Seg_4974" n="e" s="T471">nuʔmələʔbəʔjə </ts>
               <ts e="T473" id="Seg_4976" n="e" s="T472">turanə </ts>
               <ts e="T474" id="Seg_4978" n="e" s="T473">bar. </ts>
            </ts>
            <ts e="T478" id="Seg_4979" n="sc" s="T475">
               <ts e="T476" id="Seg_4981" n="e" s="T475">Šakku </ts>
               <ts e="T477" id="Seg_4983" n="e" s="T476">ajigən </ts>
               <ts e="T478" id="Seg_4985" n="e" s="T477">enləʔi. </ts>
            </ts>
            <ts e="T480" id="Seg_4986" n="sc" s="T479">
               <ts e="T480" id="Seg_4988" n="e" s="T479">((BRK)). </ts>
            </ts>
            <ts e="T492" id="Seg_4989" n="sc" s="T481">
               <ts e="T482" id="Seg_4991" n="e" s="T481">Men </ts>
               <ts e="T483" id="Seg_4993" n="e" s="T482">sarbiʔi </ts>
               <ts e="T484" id="Seg_4995" n="e" s="T483">sagər, </ts>
               <ts e="T485" id="Seg_4997" n="e" s="T484">i </ts>
               <ts e="T486" id="Seg_4999" n="e" s="T485">simat - </ts>
               <ts e="T487" id="Seg_5001" n="e" s="T486">ob, </ts>
               <ts e="T488" id="Seg_5003" n="e" s="T487">šide, </ts>
               <ts e="T489" id="Seg_5005" n="e" s="T488">nagur, </ts>
               <ts e="T490" id="Seg_5007" n="e" s="T489">teʔtə - </ts>
               <ts e="T491" id="Seg_5009" n="e" s="T490">teʔtə </ts>
               <ts e="T492" id="Seg_5011" n="e" s="T491">simat. </ts>
            </ts>
            <ts e="T494" id="Seg_5012" n="sc" s="T493">
               <ts e="T494" id="Seg_5014" n="e" s="T493">Четырехглазый. </ts>
            </ts>
            <ts e="T501" id="Seg_5015" n="sc" s="T495">
               <ts e="T496" id="Seg_5017" n="e" s="T495">Šapku </ts>
               <ts e="T497" id="Seg_5019" n="e" s="T496">ambiʔi, </ts>
               <ts e="T498" id="Seg_5021" n="e" s="T497">štobɨ </ts>
               <ts e="T499" id="Seg_5023" n="e" s="T498">penze </ts>
               <ts e="T500" id="Seg_5025" n="e" s="T499">(s-) </ts>
               <ts e="T501" id="Seg_5027" n="e" s="T500">dʼagarluʔpi. </ts>
            </ts>
            <ts e="T503" id="Seg_5028" n="sc" s="T502">
               <ts e="T503" id="Seg_5030" n="e" s="T502">((BRK)). </ts>
            </ts>
            <ts e="T514" id="Seg_5031" n="sc" s="T504">
               <ts e="T505" id="Seg_5033" n="e" s="T504">(Onʼiʔ) </ts>
               <ts e="T506" id="Seg_5035" n="e" s="T505">Onʼiʔ </ts>
               <ts e="T507" id="Seg_5037" n="e" s="T506">kuza </ts>
               <ts e="T508" id="Seg_5039" n="e" s="T507">(поселенец) </ts>
               <ts e="T509" id="Seg_5041" n="e" s="T508">miʔnʼibeʔ </ts>
               <ts e="T510" id="Seg_5043" n="e" s="T509">ĭzembi, </ts>
               <ts e="T511" id="Seg_5045" n="e" s="T510">ĭzembi, </ts>
               <ts e="T512" id="Seg_5047" n="e" s="T511">dĭgəttə </ts>
               <ts e="T513" id="Seg_5049" n="e" s="T512">(ku-) </ts>
               <ts e="T514" id="Seg_5051" n="e" s="T513">külambi. </ts>
            </ts>
            <ts e="T520" id="Seg_5052" n="sc" s="T515">
               <ts e="T516" id="Seg_5054" n="e" s="T515">A </ts>
               <ts e="T517" id="Seg_5056" n="e" s="T516">miʔ </ts>
               <ts e="T518" id="Seg_5058" n="e" s="T517">(ka-) </ts>
               <ts e="T519" id="Seg_5060" n="e" s="T518">kanbibaʔ </ts>
               <ts e="T1139" id="Seg_5062" n="e" s="T519">Kazan </ts>
               <ts e="T520" id="Seg_5064" n="e" s="T1139">turanə. </ts>
            </ts>
            <ts e="T528" id="Seg_5065" n="sc" s="T521">
               <ts e="T522" id="Seg_5067" n="e" s="T521">Sazən </ts>
               <ts e="T523" id="Seg_5069" n="e" s="T522">(udandə </ts>
               <ts e="T524" id="Seg_5071" n="e" s="T523">uzan-) </ts>
               <ts e="T525" id="Seg_5073" n="e" s="T524">udandə </ts>
               <ts e="T526" id="Seg_5075" n="e" s="T525">izittə </ts>
               <ts e="T527" id="Seg_5077" n="e" s="T526">i </ts>
               <ts e="T528" id="Seg_5079" n="e" s="T527">ulundə. </ts>
            </ts>
            <ts e="T542" id="Seg_5080" n="sc" s="T529">
               <ts e="T530" id="Seg_5082" n="e" s="T529">Dĭgəttə </ts>
               <ts e="T531" id="Seg_5084" n="e" s="T530">abəs </ts>
               <ts e="T532" id="Seg_5086" n="e" s="T531">miʔnʼibeʔ </ts>
               <ts e="T533" id="Seg_5088" n="e" s="T532">ej </ts>
               <ts e="T534" id="Seg_5090" n="e" s="T533">mĭbi. </ts>
               <ts e="T535" id="Seg_5092" n="e" s="T534">(M-) </ts>
               <ts e="T536" id="Seg_5094" n="e" s="T535">Miʔ </ts>
               <ts e="T537" id="Seg_5096" n="e" s="T536">ara </ts>
               <ts e="T538" id="Seg_5098" n="e" s="T537">ibibeʔ </ts>
               <ts e="T539" id="Seg_5100" n="e" s="T538">четверть </ts>
               <ts e="T540" id="Seg_5102" n="e" s="T539">i </ts>
               <ts e="T541" id="Seg_5104" n="e" s="T540">maʔnʼi </ts>
               <ts e="T542" id="Seg_5106" n="e" s="T541">šobibaʔ. </ts>
            </ts>
            <ts e="T552" id="Seg_5107" n="sc" s="T543">
               <ts e="T544" id="Seg_5109" n="e" s="T543">Dĭgəttə </ts>
               <ts e="T545" id="Seg_5111" n="e" s="T544">iam </ts>
               <ts e="T546" id="Seg_5113" n="e" s="T545">abam </ts>
               <ts e="T547" id="Seg_5115" n="e" s="T546">(ibi-) </ts>
               <ts e="T548" id="Seg_5117" n="e" s="T547">ibiʔi </ts>
               <ts e="T549" id="Seg_5119" n="e" s="T548">dĭ </ts>
               <ts e="T550" id="Seg_5121" n="e" s="T549">ara, </ts>
               <ts e="T1137" id="Seg_5123" n="e" s="T550">kalla </ts>
               <ts e="T551" id="Seg_5125" n="e" s="T1137">dʼürbiʔi </ts>
               <ts e="T552" id="Seg_5127" n="e" s="T551">bĭʔsittə. </ts>
            </ts>
            <ts e="T561" id="Seg_5128" n="sc" s="T553">
               <ts e="T554" id="Seg_5130" n="e" s="T553">A </ts>
               <ts e="T555" id="Seg_5132" n="e" s="T554">miʔ, </ts>
               <ts e="T556" id="Seg_5134" n="e" s="T555">ešseŋ, </ts>
               <ts e="T557" id="Seg_5136" n="e" s="T556">miʔ </ts>
               <ts e="T558" id="Seg_5138" n="e" s="T557">tože </ts>
               <ts e="T559" id="Seg_5140" n="e" s="T558">ej </ts>
               <ts e="T560" id="Seg_5142" n="e" s="T559">maləm </ts>
               <ts e="T561" id="Seg_5144" n="e" s="T560">maʔnə. </ts>
            </ts>
            <ts e="T564" id="Seg_5145" n="sc" s="T562">
               <ts e="T563" id="Seg_5147" n="e" s="T562">"(Kanu-) ((NOISE)) </ts>
               <ts e="T564" id="Seg_5149" n="e" s="T563">Kanžebəj!" </ts>
            </ts>
            <ts e="T570" id="Seg_5150" n="sc" s="T565">
               <ts e="T566" id="Seg_5152" n="e" s="T565">A </ts>
               <ts e="T567" id="Seg_5154" n="e" s="T566">măn </ts>
               <ts e="T568" id="Seg_5156" n="e" s="T567">mămbiam: </ts>
               <ts e="T569" id="Seg_5158" n="e" s="T568">"Iʔbəʔ, </ts>
               <ts e="T570" id="Seg_5160" n="e" s="T569">kunolaʔ. </ts>
            </ts>
            <ts e="T575" id="Seg_5161" n="sc" s="T571">
               <ts e="T572" id="Seg_5163" n="e" s="T571">A </ts>
               <ts e="T573" id="Seg_5165" n="e" s="T572">măn </ts>
               <ts e="T574" id="Seg_5167" n="e" s="T573">iʔbələm </ts>
               <ts e="T575" id="Seg_5169" n="e" s="T574">šiʔnʼileʔ. </ts>
            </ts>
            <ts e="T582" id="Seg_5170" n="sc" s="T576">
               <ts e="T577" id="Seg_5172" n="e" s="T576">Kamən </ts>
               <ts e="T578" id="Seg_5174" n="e" s="T577">măna </ts>
               <ts e="T579" id="Seg_5176" n="e" s="T578">kabarləj, </ts>
               <ts e="T580" id="Seg_5178" n="e" s="T579">a </ts>
               <ts e="T581" id="Seg_5180" n="e" s="T580">šiʔ </ts>
               <ts e="T582" id="Seg_5182" n="e" s="T581">uʔmeluʔpileʔ." </ts>
            </ts>
            <ts e="T586" id="Seg_5183" n="sc" s="T583">
               <ts e="T584" id="Seg_5185" n="e" s="T583">Dĭgəttə </ts>
               <ts e="T585" id="Seg_5187" n="e" s="T584">erten </ts>
               <ts e="T586" id="Seg_5189" n="e" s="T585">uʔbdəbiam. </ts>
            </ts>
            <ts e="T596" id="Seg_5190" n="sc" s="T587">
               <ts e="T588" id="Seg_5192" n="e" s="T587">Ot, </ts>
               <ts e="T589" id="Seg_5194" n="e" s="T588">kuiol, </ts>
               <ts e="T590" id="Seg_5196" n="e" s="T589">dĭ </ts>
               <ts e="T591" id="Seg_5198" n="e" s="T590">iʔbəlaʔbə, </ts>
               <ts e="T592" id="Seg_5200" n="e" s="T591">(i </ts>
               <ts e="T593" id="Seg_5202" n="e" s="T592">m-) </ts>
               <ts e="T594" id="Seg_5204" n="e" s="T593">i </ts>
               <ts e="T595" id="Seg_5206" n="e" s="T594">miʔ </ts>
               <ts e="T596" id="Seg_5208" n="e" s="T595">kunolbibaʔ. </ts>
            </ts>
            <ts e="T598" id="Seg_5209" n="sc" s="T597">
               <ts e="T598" id="Seg_5211" n="e" s="T597">((BRK)). </ts>
            </ts>
            <ts e="T604" id="Seg_5212" n="sc" s="T599">
               <ts e="T600" id="Seg_5214" n="e" s="T599">Dĭgəttə </ts>
               <ts e="T601" id="Seg_5216" n="e" s="T600">dĭ </ts>
               <ts e="T602" id="Seg_5218" n="e" s="T601">külambi, </ts>
               <ts e="T603" id="Seg_5220" n="e" s="T602">dĭm </ts>
               <ts e="T604" id="Seg_5222" n="e" s="T603">băzəbiʔi. </ts>
            </ts>
            <ts e="T609" id="Seg_5223" n="sc" s="T605">
               <ts e="T606" id="Seg_5225" n="e" s="T605">Kujnek </ts>
               <ts e="T607" id="Seg_5227" n="e" s="T606">šerbiʔi, </ts>
               <ts e="T608" id="Seg_5229" n="e" s="T607">piʔme </ts>
               <ts e="T609" id="Seg_5231" n="e" s="T608">šerbiʔi. </ts>
            </ts>
            <ts e="T613" id="Seg_5232" n="sc" s="T610">
               <ts e="T611" id="Seg_5234" n="e" s="T610">Jamaʔi </ts>
               <ts e="T612" id="Seg_5236" n="e" s="T611">(šoʔpiʔi=) </ts>
               <ts e="T613" id="Seg_5238" n="e" s="T612">šerbiʔi. </ts>
            </ts>
            <ts e="T616" id="Seg_5239" n="sc" s="T614">
               <ts e="T615" id="Seg_5241" n="e" s="T614">Dĭgəttə </ts>
               <ts e="T616" id="Seg_5243" n="e" s="T615">kombiʔi. </ts>
            </ts>
            <ts e="T621" id="Seg_5244" n="sc" s="T617">
               <ts e="T618" id="Seg_5246" n="e" s="T617">(Dʼo-) </ts>
               <ts e="T619" id="Seg_5248" n="e" s="T618">D'üzi </ts>
               <ts e="T620" id="Seg_5250" n="e" s="T619">kămbiʔi </ts>
               <ts e="T621" id="Seg_5252" n="e" s="T620">dĭm. </ts>
            </ts>
            <ts e="T631" id="Seg_5253" n="sc" s="T622">
               <ts e="T623" id="Seg_5255" n="e" s="T622">Šobiʔi </ts>
               <ts e="T624" id="Seg_5257" n="e" s="T623">maʔndə </ts>
               <ts e="T625" id="Seg_5259" n="e" s="T624">(na-). </ts>
               <ts e="T626" id="Seg_5261" n="e" s="T625">Ara </ts>
               <ts e="T627" id="Seg_5263" n="e" s="T626">biʔpiʔi, </ts>
               <ts e="T628" id="Seg_5265" n="e" s="T627">ipek </ts>
               <ts e="T629" id="Seg_5267" n="e" s="T628">amorbiʔi, </ts>
               <ts e="T630" id="Seg_5269" n="e" s="T629">uja </ts>
               <ts e="T631" id="Seg_5271" n="e" s="T630">ambiʔi. </ts>
            </ts>
            <ts e="T635" id="Seg_5272" n="sc" s="T632">
               <ts e="T633" id="Seg_5274" n="e" s="T632">I </ts>
               <ts e="T634" id="Seg_5276" n="e" s="T633">maʔndə </ts>
               <ts e="T1138" id="Seg_5278" n="e" s="T634">kalla </ts>
               <ts e="T635" id="Seg_5280" n="e" s="T1138">dʼürbiʔi. </ts>
            </ts>
            <ts e="T637" id="Seg_5281" n="sc" s="T636">
               <ts e="T637" id="Seg_5283" n="e" s="T636">((BRK)). </ts>
            </ts>
            <ts e="T644" id="Seg_5284" n="sc" s="T638">
               <ts e="T639" id="Seg_5286" n="e" s="T638">Măn </ts>
               <ts e="T640" id="Seg_5288" n="e" s="T639">onʼiʔ </ts>
               <ts e="T641" id="Seg_5290" n="e" s="T640">raz </ts>
               <ts e="T642" id="Seg_5292" n="e" s="T641">(šo-) </ts>
               <ts e="T643" id="Seg_5294" n="e" s="T642">šobiam </ts>
               <ts e="T1140" id="Seg_5296" n="e" s="T643">Kazan </ts>
               <ts e="T644" id="Seg_5298" n="e" s="T1140">turagən. </ts>
            </ts>
            <ts e="T650" id="Seg_5299" n="sc" s="T645">
               <ts e="T646" id="Seg_5301" n="e" s="T645">Šobiam </ts>
               <ts e="T647" id="Seg_5303" n="e" s="T646">Permʼakovăn, </ts>
               <ts e="T648" id="Seg_5305" n="e" s="T647">dĭgəttə </ts>
               <ts e="T649" id="Seg_5307" n="e" s="T648">maʔndə </ts>
               <ts e="T650" id="Seg_5309" n="e" s="T649">šobiam. </ts>
            </ts>
            <ts e="T657" id="Seg_5310" n="sc" s="T651">
               <ts e="T652" id="Seg_5312" n="e" s="T651">Tʼerməngən </ts>
               <ts e="T653" id="Seg_5314" n="e" s="T652">dĭn </ts>
               <ts e="T654" id="Seg_5316" n="e" s="T653">ildəm </ts>
               <ts e="T655" id="Seg_5318" n="e" s="T654">šindidə </ts>
               <ts e="T656" id="Seg_5320" n="e" s="T655">(perluʔ-) </ts>
               <ts e="T657" id="Seg_5322" n="e" s="T656">(perluʔpiʔi). </ts>
            </ts>
            <ts e="T669" id="Seg_5323" n="sc" s="T658">
               <ts e="T659" id="Seg_5325" n="e" s="T658">A </ts>
               <ts e="T660" id="Seg_5327" n="e" s="T659">măn </ts>
               <ts e="T661" id="Seg_5329" n="e" s="T660">šobiam </ts>
               <ts e="T662" id="Seg_5331" n="e" s="T661">da </ts>
               <ts e="T663" id="Seg_5333" n="e" s="T662">(muzuruk-) </ts>
               <ts e="T664" id="Seg_5335" n="e" s="T663">dĭgəttə </ts>
               <ts e="T665" id="Seg_5337" n="e" s="T664">toʔnarləm, </ts>
               <ts e="T666" id="Seg_5339" n="e" s="T665">bar </ts>
               <ts e="T667" id="Seg_5341" n="e" s="T666">aktʼa </ts>
               <ts e="T668" id="Seg_5343" n="e" s="T667">iʔgö </ts>
               <ts e="T669" id="Seg_5345" n="e" s="T668">moləj. </ts>
            </ts>
            <ts e="T671" id="Seg_5346" n="sc" s="T670">
               <ts e="T671" id="Seg_5348" n="e" s="T670">((BRK)). </ts>
            </ts>
            <ts e="T678" id="Seg_5349" n="sc" s="T672">
               <ts e="T673" id="Seg_5351" n="e" s="T672">Šobiam </ts>
               <ts e="T674" id="Seg_5353" n="e" s="T673">(maʔ-) </ts>
               <ts e="T675" id="Seg_5355" n="e" s="T674">maʔndə, </ts>
               <ts e="T676" id="Seg_5357" n="e" s="T675">šindində </ts>
               <ts e="T677" id="Seg_5359" n="e" s="T676">ej </ts>
               <ts e="T678" id="Seg_5361" n="e" s="T677">kubiom. </ts>
            </ts>
            <ts e="T680" id="Seg_5362" n="sc" s="T679">
               <ts e="T680" id="Seg_5364" n="e" s="T679">((BRK)). </ts>
            </ts>
            <ts e="T698" id="Seg_5365" n="sc" s="T681">
               <ts e="T682" id="Seg_5367" n="e" s="T681">Măn </ts>
               <ts e="T683" id="Seg_5369" n="e" s="T682">ulum </ts>
               <ts e="T684" id="Seg_5371" n="e" s="T683">ĭzemnie, </ts>
               <ts e="T685" id="Seg_5373" n="e" s="T684">simam </ts>
               <ts e="T686" id="Seg_5375" n="e" s="T685">ĭzemnie, </ts>
               <ts e="T687" id="Seg_5377" n="e" s="T686">a </ts>
               <ts e="T688" id="Seg_5379" n="e" s="T687">tăn </ts>
               <ts e="T689" id="Seg_5381" n="e" s="T688">ĭmbi </ts>
               <ts e="T690" id="Seg_5383" n="e" s="T689">ĭzemnie? </ts>
               <ts e="T691" id="Seg_5385" n="e" s="T690">A </ts>
               <ts e="T692" id="Seg_5387" n="e" s="T691">măn </ts>
               <ts e="T693" id="Seg_5389" n="e" s="T692">tĭmem </ts>
               <ts e="T694" id="Seg_5391" n="e" s="T693">(ĭzembie-) </ts>
               <ts e="T695" id="Seg_5393" n="e" s="T694">ĭzemneʔbə </ts>
               <ts e="T696" id="Seg_5395" n="e" s="T695">i </ts>
               <ts e="T697" id="Seg_5397" n="e" s="T696">pĭjem </ts>
               <ts e="T698" id="Seg_5399" n="e" s="T697">ĭzemneʔbə. </ts>
            </ts>
            <ts e="T700" id="Seg_5400" n="sc" s="T699">
               <ts e="T700" id="Seg_5402" n="e" s="T699">((BRK)). </ts>
            </ts>
            <ts e="T715" id="Seg_5403" n="sc" s="T701">
               <ts e="T702" id="Seg_5405" n="e" s="T701">Ivanovičən </ts>
               <ts e="T703" id="Seg_5407" n="e" s="T702">bar </ts>
               <ts e="T704" id="Seg_5409" n="e" s="T703">šüjot </ts>
               <ts e="T705" id="Seg_5411" n="e" s="T704">ĭzemnie </ts>
               <ts e="T706" id="Seg_5413" n="e" s="T705">i </ts>
               <ts e="T707" id="Seg_5415" n="e" s="T706">ujut </ts>
               <ts e="T708" id="Seg_5417" n="e" s="T707">ĭzemnie, </ts>
               <ts e="T709" id="Seg_5419" n="e" s="T708">i </ts>
               <ts e="T710" id="Seg_5421" n="e" s="T709">(ud-) </ts>
               <ts e="T711" id="Seg_5423" n="e" s="T710">(udazaŋdə) </ts>
               <ts e="T712" id="Seg_5425" n="e" s="T711">ĭzemneʔpəʔjə, </ts>
               <ts e="T713" id="Seg_5427" n="e" s="T712">bar </ts>
               <ts e="T714" id="Seg_5429" n="e" s="T713">tăŋ </ts>
               <ts e="T715" id="Seg_5431" n="e" s="T714">ĭzemnie. </ts>
            </ts>
            <ts e="T717" id="Seg_5432" n="sc" s="T716">
               <ts e="T717" id="Seg_5434" n="e" s="T716">((BRK)). </ts>
            </ts>
            <ts e="T726" id="Seg_5435" n="sc" s="T718">
               <ts e="T719" id="Seg_5437" n="e" s="T718">Nada </ts>
               <ts e="T720" id="Seg_5439" n="e" s="T719">(da- </ts>
               <ts e="T721" id="Seg_5441" n="e" s="T720">da-) </ts>
               <ts e="T722" id="Seg_5443" n="e" s="T721">dʼazirzittə </ts>
               <ts e="T723" id="Seg_5445" n="e" s="T722">dĭ </ts>
               <ts e="T724" id="Seg_5447" n="e" s="T723">(na-) </ts>
               <ts e="T725" id="Seg_5449" n="e" s="T724">nagur </ts>
               <ts e="T726" id="Seg_5451" n="e" s="T725">kuza. </ts>
            </ts>
            <ts e="T734" id="Seg_5452" n="sc" s="T727">
               <ts e="T728" id="Seg_5454" n="e" s="T727">Doxtăr </ts>
               <ts e="T729" id="Seg_5456" n="e" s="T728">nada </ts>
               <ts e="T730" id="Seg_5458" n="e" s="T729">deʔsittə, </ts>
               <ts e="T731" id="Seg_5460" n="e" s="T730">pušaj </ts>
               <ts e="T732" id="Seg_5462" n="e" s="T731">dʼazirləj </ts>
               <ts e="T733" id="Seg_5464" n="e" s="T732">dĭ </ts>
               <ts e="T734" id="Seg_5466" n="e" s="T733">dĭzem. </ts>
            </ts>
            <ts e="T736" id="Seg_5467" n="sc" s="T735">
               <ts e="T736" id="Seg_5469" n="e" s="T735">((BRK)). </ts>
            </ts>
            <ts e="T744" id="Seg_5470" n="sc" s="T737">
               <ts e="T738" id="Seg_5472" n="e" s="T737">Ĭmbi-nʼibudʼ </ts>
               <ts e="T739" id="Seg_5474" n="e" s="T738">mĭləj </ts>
               <ts e="T740" id="Seg_5476" n="e" s="T739">bĭʔsittə </ts>
               <ts e="T741" id="Seg_5478" n="e" s="T740">ali </ts>
               <ts e="T742" id="Seg_5480" n="e" s="T741">ĭmbi-nʼibudʼ </ts>
               <ts e="T743" id="Seg_5482" n="e" s="T742">mĭləj </ts>
               <ts e="T744" id="Seg_5484" n="e" s="T743">kĭškəsʼtə. </ts>
            </ts>
            <ts e="T746" id="Seg_5485" n="sc" s="T745">
               <ts e="T746" id="Seg_5487" n="e" s="T745">((BRK)). </ts>
            </ts>
            <ts e="T756" id="Seg_5488" n="sc" s="T747">
               <ts e="T748" id="Seg_5490" n="e" s="T747">Dĭzeŋ </ts>
               <ts e="T749" id="Seg_5492" n="e" s="T748">bar </ts>
               <ts e="T750" id="Seg_5494" n="e" s="T749">tăŋ </ts>
               <ts e="T751" id="Seg_5496" n="e" s="T750">kănnambiʔi, </ts>
               <ts e="T752" id="Seg_5498" n="e" s="T751">a </ts>
               <ts e="T753" id="Seg_5500" n="e" s="T752">tuj </ts>
               <ts e="T754" id="Seg_5502" n="e" s="T753">ugandə </ts>
               <ts e="T755" id="Seg_5504" n="e" s="T754">tăŋ </ts>
               <ts e="T756" id="Seg_5506" n="e" s="T755">ĭzemneʔbəʔjə. </ts>
            </ts>
            <ts e="T758" id="Seg_5507" n="sc" s="T757">
               <ts e="T758" id="Seg_5509" n="e" s="T757">((BRK)). </ts>
            </ts>
            <ts e="T762" id="Seg_5510" n="sc" s="T759">
               <ts e="T760" id="Seg_5512" n="e" s="T759">Marejka </ts>
               <ts e="T761" id="Seg_5514" n="e" s="T760">urgajanə </ts>
               <ts e="T762" id="Seg_5516" n="e" s="T761">kadəldə. </ts>
            </ts>
            <ts e="T770" id="Seg_5517" n="sc" s="T763">
               <ts e="T764" id="Seg_5519" n="e" s="T763">Urgajanə </ts>
               <ts e="T765" id="Seg_5521" n="e" s="T764">dĭrgit </ts>
               <ts e="T766" id="Seg_5523" n="e" s="T765">kadəldə, </ts>
               <ts e="T767" id="Seg_5525" n="e" s="T766">i </ts>
               <ts e="T768" id="Seg_5527" n="e" s="T767">simat </ts>
               <ts e="T769" id="Seg_5529" n="e" s="T768">dĭrgit </ts>
               <ts e="T770" id="Seg_5531" n="e" s="T769">(urgajagəndə) ((NOISE)). </ts>
            </ts>
            <ts e="T772" id="Seg_5532" n="sc" s="T771">
               <ts e="T772" id="Seg_5534" n="e" s="T771">((BRK)). </ts>
            </ts>
            <ts e="T777" id="Seg_5535" n="sc" s="T773">
               <ts e="T774" id="Seg_5537" n="e" s="T773">Amnobiʔi </ts>
               <ts e="T775" id="Seg_5539" n="e" s="T774">nüke </ts>
               <ts e="T776" id="Seg_5541" n="e" s="T775">i </ts>
               <ts e="T777" id="Seg_5543" n="e" s="T776">büzʼe. </ts>
            </ts>
            <ts e="T781" id="Seg_5544" n="sc" s="T778">
               <ts e="T779" id="Seg_5546" n="e" s="T778">Samən </ts>
               <ts e="T780" id="Seg_5548" n="e" s="T779">bügən </ts>
               <ts e="T781" id="Seg_5550" n="e" s="T780">toʔndə. </ts>
            </ts>
            <ts e="T785" id="Seg_5551" n="sc" s="T782">
               <ts e="T783" id="Seg_5553" n="e" s="T782">Nüke </ts>
               <ts e="T784" id="Seg_5555" n="e" s="T783">ererla </ts>
               <ts e="T785" id="Seg_5557" n="e" s="T784">amnolaʔpi. </ts>
            </ts>
            <ts e="T793" id="Seg_5558" n="sc" s="T786">
               <ts e="T787" id="Seg_5560" n="e" s="T786">A </ts>
               <ts e="T788" id="Seg_5562" n="e" s="T787">büzʼe </ts>
               <ts e="T789" id="Seg_5564" n="e" s="T788">mĭmbi </ts>
               <ts e="T790" id="Seg_5566" n="e" s="T789">bünə </ts>
               <ts e="T791" id="Seg_5568" n="e" s="T790">kola </ts>
               <ts e="T792" id="Seg_5570" n="e" s="T791">(dʼapitʼ-) </ts>
               <ts e="T793" id="Seg_5572" n="e" s="T792">dʼabəsʼtə. </ts>
            </ts>
            <ts e="T807" id="Seg_5573" n="sc" s="T794">
               <ts e="T795" id="Seg_5575" n="e" s="T794">(Onʼiʔ </ts>
               <ts e="T796" id="Seg_5577" n="e" s="T795">raz=) </ts>
               <ts e="T797" id="Seg_5579" n="e" s="T796">Onʼiʔ </ts>
               <ts e="T798" id="Seg_5581" n="e" s="T797">raz </ts>
               <ts e="T799" id="Seg_5583" n="e" s="T798">kambi, </ts>
               <ts e="T800" id="Seg_5585" n="e" s="T799">dĭgəttə </ts>
               <ts e="T801" id="Seg_5587" n="e" s="T800">ĭmbidə </ts>
               <ts e="T802" id="Seg_5589" n="e" s="T801">ej </ts>
               <ts e="T803" id="Seg_5591" n="e" s="T802">dʼaʔpi. </ts>
               <ts e="T804" id="Seg_5593" n="e" s="T803">Onʼiʔ </ts>
               <ts e="T805" id="Seg_5595" n="e" s="T804">riba </ts>
               <ts e="T806" id="Seg_5597" n="e" s="T805">dʼaʔpi, </ts>
               <ts e="T807" id="Seg_5599" n="e" s="T806">kuvas. </ts>
            </ts>
            <ts e="T810" id="Seg_5600" n="sc" s="T808">
               <ts e="T809" id="Seg_5602" n="e" s="T808">"Măn </ts>
               <ts e="T810" id="Seg_5604" n="e" s="T809">pʼeleʔbə…" </ts>
            </ts>
            <ts e="T812" id="Seg_5605" n="sc" s="T811">
               <ts e="T812" id="Seg_5607" n="e" s="T811">((BRK)). </ts>
            </ts>
            <ts e="T819" id="Seg_5608" n="sc" s="T813">
               <ts e="T814" id="Seg_5610" n="e" s="T813">Kola </ts>
               <ts e="T815" id="Seg_5612" n="e" s="T814">kuvas, </ts>
               <ts e="T816" id="Seg_5614" n="e" s="T815">dʼăbaktərlia </ts>
               <ts e="T817" id="Seg_5616" n="e" s="T816">kuzanə </ts>
               <ts e="T818" id="Seg_5618" n="e" s="T817">(я-) </ts>
               <ts e="T819" id="Seg_5620" n="e" s="T818">šiketsi. </ts>
            </ts>
            <ts e="T823" id="Seg_5621" n="sc" s="T820">
               <ts e="T821" id="Seg_5623" n="e" s="T820">"Öʔleʔ </ts>
               <ts e="T822" id="Seg_5625" n="e" s="T821">măna </ts>
               <ts e="T823" id="Seg_5627" n="e" s="T822">bünə!" </ts>
            </ts>
            <ts e="T826" id="Seg_5628" n="sc" s="T824">
               <ts e="T825" id="Seg_5630" n="e" s="T824">"Măn </ts>
               <ts e="T826" id="Seg_5632" n="e" s="T825">öʔluʔpiam". </ts>
            </ts>
            <ts e="T832" id="Seg_5633" n="sc" s="T827">
               <ts e="T828" id="Seg_5635" n="e" s="T827">Šobi </ts>
               <ts e="T829" id="Seg_5637" n="e" s="T828">maʔndə </ts>
               <ts e="T830" id="Seg_5639" n="e" s="T829">da </ts>
               <ts e="T831" id="Seg_5641" n="e" s="T830">nükenə </ts>
               <ts e="T832" id="Seg_5643" n="e" s="T831">dʼăbaktərlabə. </ts>
            </ts>
            <ts e="T840" id="Seg_5644" n="sc" s="T833">
               <ts e="T834" id="Seg_5646" n="e" s="T833">"Kola </ts>
               <ts e="T835" id="Seg_5648" n="e" s="T834">bar </ts>
               <ts e="T836" id="Seg_5650" n="e" s="T835">kuvas </ts>
               <ts e="T837" id="Seg_5652" n="e" s="T836">ibi, </ts>
               <ts e="T838" id="Seg_5654" n="e" s="T837">dʼăbaktərbi </ts>
               <ts e="T839" id="Seg_5656" n="e" s="T838">kak </ts>
               <ts e="T840" id="Seg_5658" n="e" s="T839">kuza". </ts>
            </ts>
            <ts e="T842" id="Seg_5659" n="sc" s="T841">
               <ts e="T842" id="Seg_5661" n="e" s="T841">((BRK)). </ts>
            </ts>
            <ts e="T848" id="Seg_5662" n="sc" s="T843">
               <ts e="T844" id="Seg_5664" n="e" s="T843">Dĭgəttə </ts>
               <ts e="T845" id="Seg_5666" n="e" s="T844">dĭ </ts>
               <ts e="T846" id="Seg_5668" n="e" s="T845">nüke </ts>
               <ts e="T847" id="Seg_5670" n="e" s="T846">davaj </ts>
               <ts e="T848" id="Seg_5672" n="e" s="T847">kudonzittə. </ts>
            </ts>
            <ts e="T854" id="Seg_5673" n="sc" s="T849">
               <ts e="T850" id="Seg_5675" n="e" s="T849">"Ĭmbi </ts>
               <ts e="T851" id="Seg_5677" n="e" s="T850">tăn </ts>
               <ts e="T852" id="Seg_5679" n="e" s="T851">ĭmbidə </ts>
               <ts e="T853" id="Seg_5681" n="e" s="T852">ej </ts>
               <ts e="T854" id="Seg_5683" n="e" s="T853">pʼebiel? </ts>
            </ts>
            <ts e="T862" id="Seg_5684" n="sc" s="T855">
               <ts e="T856" id="Seg_5686" n="e" s="T855">Kărɨtă </ts>
               <ts e="T857" id="Seg_5688" n="e" s="T856">miʔnʼibeʔ </ts>
               <ts e="T858" id="Seg_5690" n="e" s="T857">naga, </ts>
               <ts e="T859" id="Seg_5692" n="e" s="T858">(ši- šiz-) </ts>
               <ts e="T860" id="Seg_5694" n="e" s="T859">šižəbi </ts>
               <ts e="T861" id="Seg_5696" n="e" s="T860">(kərɨ-) </ts>
               <ts e="T862" id="Seg_5698" n="e" s="T861">kărĭtă". </ts>
            </ts>
            <ts e="T867" id="Seg_5699" n="sc" s="T863">
               <ts e="T864" id="Seg_5701" n="e" s="T863">Dĭ </ts>
               <ts e="T865" id="Seg_5703" n="e" s="T864">kambi </ts>
               <ts e="T866" id="Seg_5705" n="e" s="T865">bazo </ts>
               <ts e="T867" id="Seg_5707" n="e" s="T866">bünə. </ts>
            </ts>
            <ts e="T880" id="Seg_5708" n="sc" s="T868">
               <ts e="T869" id="Seg_5710" n="e" s="T868">Dĭgəttə </ts>
               <ts e="T870" id="Seg_5712" n="e" s="T869">davaj </ts>
               <ts e="T871" id="Seg_5714" n="e" s="T870">kirgarzittə: </ts>
               <ts e="T872" id="Seg_5716" n="e" s="T871">"Kola, </ts>
               <ts e="T873" id="Seg_5718" n="e" s="T872">šoʔ </ts>
               <ts e="T874" id="Seg_5720" n="e" s="T873">döber!" </ts>
               <ts e="T875" id="Seg_5722" n="e" s="T874">Dĭ </ts>
               <ts e="T876" id="Seg_5724" n="e" s="T875">šobi: </ts>
               <ts e="T877" id="Seg_5726" n="e" s="T876">"Ĭmbi </ts>
               <ts e="T878" id="Seg_5728" n="e" s="T877">tănan, </ts>
               <ts e="T879" id="Seg_5730" n="e" s="T878">büzʼe, </ts>
               <ts e="T880" id="Seg_5732" n="e" s="T879">kereʔ?" </ts>
            </ts>
            <ts e="T884" id="Seg_5733" n="sc" s="T881">
               <ts e="T882" id="Seg_5735" n="e" s="T881">"Măna </ts>
               <ts e="T883" id="Seg_5737" n="e" s="T882">kereʔ </ts>
               <ts e="T884" id="Seg_5739" n="e" s="T883">kărɨtă. </ts>
            </ts>
            <ts e="T891" id="Seg_5740" n="sc" s="T885">
               <ts e="T886" id="Seg_5742" n="e" s="T885">Nükem </ts>
               <ts e="T887" id="Seg_5744" n="e" s="T886">bar </ts>
               <ts e="T888" id="Seg_5746" n="e" s="T887">kudonzlaʔbə". </ts>
               <ts e="T889" id="Seg_5748" n="e" s="T888">"Nu </ts>
               <ts e="T890" id="Seg_5750" n="e" s="T889">kanaʔ </ts>
               <ts e="T891" id="Seg_5752" n="e" s="T890">manə". </ts>
            </ts>
            <ts e="T893" id="Seg_5753" n="sc" s="T892">
               <ts e="T893" id="Seg_5755" n="e" s="T892">((BRK)). </ts>
            </ts>
            <ts e="T903" id="Seg_5756" n="sc" s="T894">
               <ts e="T895" id="Seg_5758" n="e" s="T894">Dĭ </ts>
               <ts e="T896" id="Seg_5760" n="e" s="T895">nüke </ts>
               <ts e="T897" id="Seg_5762" n="e" s="T896">davaj </ts>
               <ts e="T898" id="Seg_5764" n="e" s="T897">dĭm </ts>
               <ts e="T899" id="Seg_5766" n="e" s="T898">kudonzittə: </ts>
               <ts e="T900" id="Seg_5768" n="e" s="T899">"Ĭmbi </ts>
               <ts e="T901" id="Seg_5770" n="e" s="T900">ej </ts>
               <ts e="T902" id="Seg_5772" n="e" s="T901">pʼebiel </ts>
               <ts e="T903" id="Seg_5774" n="e" s="T902">tura? </ts>
            </ts>
            <ts e="T908" id="Seg_5775" n="sc" s="T904">
               <ts e="T905" id="Seg_5777" n="e" s="T904">Miʔ </ts>
               <ts e="T906" id="Seg_5779" n="e" s="T905">tura </ts>
               <ts e="T907" id="Seg_5781" n="e" s="T906">ej </ts>
               <ts e="T908" id="Seg_5783" n="e" s="T907">kuvas". </ts>
            </ts>
            <ts e="T918" id="Seg_5784" n="sc" s="T909">
               <ts e="T910" id="Seg_5786" n="e" s="T909">Dĭ </ts>
               <ts e="T911" id="Seg_5788" n="e" s="T910">bazo </ts>
               <ts e="T912" id="Seg_5790" n="e" s="T911">kambi, </ts>
               <ts e="T913" id="Seg_5792" n="e" s="T912">davaj </ts>
               <ts e="T914" id="Seg_5794" n="e" s="T913">kirgarzittə: </ts>
               <ts e="T915" id="Seg_5796" n="e" s="T914">"Kola, </ts>
               <ts e="T916" id="Seg_5798" n="e" s="T915">kola! </ts>
               <ts e="T917" id="Seg_5800" n="e" s="T916">šoʔ </ts>
               <ts e="T918" id="Seg_5802" n="e" s="T917">döber!" </ts>
            </ts>
            <ts e="T924" id="Seg_5803" n="sc" s="T919">
               <ts e="T920" id="Seg_5805" n="e" s="T919">Kola </ts>
               <ts e="T921" id="Seg_5807" n="e" s="T920">šobi: </ts>
               <ts e="T922" id="Seg_5809" n="e" s="T921">"Ĭmbi </ts>
               <ts e="T923" id="Seg_5811" n="e" s="T922">tănan </ts>
               <ts e="T924" id="Seg_5813" n="e" s="T923">kereʔ?" </ts>
            </ts>
            <ts e="T927" id="Seg_5814" n="sc" s="T925">
               <ts e="T926" id="Seg_5816" n="e" s="T925">"Tura </ts>
               <ts e="T927" id="Seg_5818" n="e" s="T926">kereʔ. </ts>
            </ts>
            <ts e="T932" id="Seg_5819" n="sc" s="T928">
               <ts e="T929" id="Seg_5821" n="e" s="T928">Miʔ </ts>
               <ts e="T930" id="Seg_5823" n="e" s="T929">turabaʔ </ts>
               <ts e="T931" id="Seg_5825" n="e" s="T930">ej </ts>
               <ts e="T932" id="Seg_5827" n="e" s="T931">kuvas". </ts>
            </ts>
            <ts e="T935" id="Seg_5828" n="sc" s="T933">
               <ts e="T934" id="Seg_5830" n="e" s="T933">"Kanaʔ </ts>
               <ts e="T935" id="Seg_5832" n="e" s="T934">maʔnəl!" </ts>
            </ts>
            <ts e="T937" id="Seg_5833" n="sc" s="T936">
               <ts e="T937" id="Seg_5835" n="e" s="T936">((BRK)). </ts>
            </ts>
            <ts e="T951" id="Seg_5836" n="sc" s="T938">
               <ts e="T939" id="Seg_5838" n="e" s="T938">Dĭgəttə </ts>
               <ts e="T940" id="Seg_5840" n="e" s="T939">bazo </ts>
               <ts e="T941" id="Seg_5842" n="e" s="T940">dĭ </ts>
               <ts e="T942" id="Seg_5844" n="e" s="T941">nüke </ts>
               <ts e="T944" id="Seg_5846" n="e" s="T942">kudonzəbi: </ts>
               <ts e="T945" id="Seg_5848" n="e" s="T944">"Kanaʔ </ts>
               <ts e="T946" id="Seg_5850" n="e" s="T945">kolanə, </ts>
               <ts e="T947" id="Seg_5852" n="e" s="T946">pʼeʔ </ts>
               <ts e="T948" id="Seg_5854" n="e" s="T947">štobɨ </ts>
               <ts e="T949" id="Seg_5856" n="e" s="T948">măn </ts>
               <ts e="T950" id="Seg_5858" n="e" s="T949">mobiam </ts>
               <ts e="T951" id="Seg_5860" n="e" s="T950">kupčixăjzi". </ts>
            </ts>
            <ts e="T953" id="Seg_5861" n="sc" s="T952">
               <ts e="T953" id="Seg_5863" n="e" s="T952">((BRK)). </ts>
            </ts>
            <ts e="T959" id="Seg_5864" n="sc" s="T954">
               <ts e="T955" id="Seg_5866" n="e" s="T954">Dĭgəttə </ts>
               <ts e="T956" id="Seg_5868" n="e" s="T955">kola </ts>
               <ts e="T957" id="Seg_5870" n="e" s="T956">mămbi: </ts>
               <ts e="T958" id="Seg_5872" n="e" s="T957">"Kanaʔ </ts>
               <ts e="T959" id="Seg_5874" n="e" s="T958">maːʔnəl". </ts>
            </ts>
            <ts e="T975" id="Seg_5875" n="sc" s="T960">
               <ts e="T961" id="Seg_5877" n="e" s="T960">Dĭ </ts>
               <ts e="T962" id="Seg_5879" n="e" s="T961">šöbi, </ts>
               <ts e="T963" id="Seg_5881" n="e" s="T962">a </ts>
               <ts e="T964" id="Seg_5883" n="e" s="T963">dĭ </ts>
               <ts e="T965" id="Seg_5885" n="e" s="T964">nüke </ts>
               <ts e="T966" id="Seg_5887" n="e" s="T965">bazo </ts>
               <ts e="T968" id="Seg_5889" n="e" s="T966">kudonzlaʔbə: </ts>
               <ts e="T969" id="Seg_5891" n="e" s="T968">"Kanaʔ </ts>
               <ts e="T970" id="Seg_5893" n="e" s="T969">kolanə, </ts>
               <ts e="T971" id="Seg_5895" n="e" s="T970">peʔ </ts>
               <ts e="T972" id="Seg_5897" n="e" s="T971">štobɨ </ts>
               <ts e="T973" id="Seg_5899" n="e" s="T972">măn </ts>
               <ts e="T974" id="Seg_5901" n="e" s="T973">ibiem </ts>
               <ts e="T975" id="Seg_5903" n="e" s="T974">dvărʼankajzi". </ts>
            </ts>
            <ts e="T981" id="Seg_5904" n="sc" s="T976">
               <ts e="T977" id="Seg_5906" n="e" s="T976">Dĭ </ts>
               <ts e="T978" id="Seg_5908" n="e" s="T977">kambi </ts>
               <ts e="T979" id="Seg_5910" n="e" s="T978">kolanə, </ts>
               <ts e="T980" id="Seg_5912" n="e" s="T979">kola </ts>
               <ts e="T981" id="Seg_5914" n="e" s="T980">kirgarlaʔpi. </ts>
            </ts>
            <ts e="T987" id="Seg_5915" n="sc" s="T982">
               <ts e="T983" id="Seg_5917" n="e" s="T982">Kola </ts>
               <ts e="T984" id="Seg_5919" n="e" s="T983">šobi: </ts>
               <ts e="T985" id="Seg_5921" n="e" s="T984">"Ĭmbi </ts>
               <ts e="T986" id="Seg_5923" n="e" s="T985">tănan </ts>
               <ts e="T987" id="Seg_5925" n="e" s="T986">kereʔ?" </ts>
            </ts>
            <ts e="T995" id="Seg_5926" n="sc" s="T988">
               <ts e="T989" id="Seg_5928" n="e" s="T988">"Măn </ts>
               <ts e="T990" id="Seg_5930" n="e" s="T989">nükem </ts>
               <ts e="T991" id="Seg_5932" n="e" s="T990">mănde </ts>
               <ts e="T992" id="Seg_5934" n="e" s="T991">štobɨ </ts>
               <ts e="T993" id="Seg_5936" n="e" s="T992">măn </ts>
               <ts e="T994" id="Seg_5938" n="e" s="T993">dvărʼankăjzi </ts>
               <ts e="T995" id="Seg_5940" n="e" s="T994">ibiem". </ts>
            </ts>
            <ts e="T998" id="Seg_5941" n="sc" s="T996">
               <ts e="T997" id="Seg_5943" n="e" s="T996">"Kanaʔ </ts>
               <ts e="T998" id="Seg_5945" n="e" s="T997">maːʔnəl!" </ts>
            </ts>
            <ts e="T1000" id="Seg_5946" n="sc" s="T999">
               <ts e="T1000" id="Seg_5948" n="e" s="T999">((BRK)). </ts>
            </ts>
            <ts e="T1009" id="Seg_5949" n="sc" s="T1001">
               <ts e="T1002" id="Seg_5951" n="e" s="T1001">Dĭgəttə </ts>
               <ts e="T1003" id="Seg_5953" n="e" s="T1002">büzʼe </ts>
               <ts e="T1004" id="Seg_5955" n="e" s="T1003">šobi </ts>
               <ts e="T1005" id="Seg_5957" n="e" s="T1004">maːʔndə, </ts>
               <ts e="T1006" id="Seg_5959" n="e" s="T1005">dĭ </ts>
               <ts e="T1007" id="Seg_5961" n="e" s="T1006">davaj </ts>
               <ts e="T1008" id="Seg_5963" n="e" s="T1007">bazo </ts>
               <ts e="T1009" id="Seg_5965" n="e" s="T1008">kudonzittə. </ts>
            </ts>
            <ts e="T1015" id="Seg_5966" n="sc" s="T1010">
               <ts e="T1011" id="Seg_5968" n="e" s="T1010">(I </ts>
               <ts e="T1012" id="Seg_5970" n="e" s="T1011">baʔ-) </ts>
               <ts e="T1013" id="Seg_5972" n="e" s="T1012">I </ts>
               <ts e="T1014" id="Seg_5974" n="e" s="T1013">münörzittə </ts>
               <ts e="T1015" id="Seg_5976" n="e" s="T1014">xat'el. </ts>
            </ts>
            <ts e="T1025" id="Seg_5977" n="sc" s="T1016">
               <ts e="T1017" id="Seg_5979" n="e" s="T1016">"Kanaʔ </ts>
               <ts e="T1018" id="Seg_5981" n="e" s="T1017">bazo </ts>
               <ts e="T1019" id="Seg_5983" n="e" s="T1018">kolanə, </ts>
               <ts e="T1020" id="Seg_5985" n="e" s="T1019">peʔ </ts>
               <ts e="T1021" id="Seg_5987" n="e" s="T1020">štobɨ </ts>
               <ts e="T1022" id="Seg_5989" n="e" s="T1021">măn </ts>
               <ts e="T1023" id="Seg_5991" n="e" s="T1022">была </ts>
               <ts e="T1024" id="Seg_5993" n="e" s="T1023">(гос-) </ts>
               <ts e="T1025" id="Seg_5995" n="e" s="T1024">государыней. </ts>
            </ts>
            <ts e="T1031" id="Seg_5996" n="sc" s="T1026">
               <ts e="T1027" id="Seg_5998" n="e" s="T1026">Štobɨ </ts>
               <ts e="T1028" id="Seg_6000" n="e" s="T1027">jil </ts>
               <ts e="T1029" id="Seg_6002" n="e" s="T1028">măna </ts>
               <ts e="T1030" id="Seg_6004" n="e" s="T1029">udazaŋdə </ts>
               <ts e="T1031" id="Seg_6006" n="e" s="T1030">носили. </ts>
            </ts>
            <ts e="T1033" id="Seg_6007" n="sc" s="T1032">
               <ts e="T1033" id="Seg_6009" n="e" s="T1032">((BRK)). </ts>
            </ts>
            <ts e="T1038" id="Seg_6010" n="sc" s="T1034">
               <ts e="T1035" id="Seg_6012" n="e" s="T1034">Dĭgəttə </ts>
               <ts e="T1036" id="Seg_6014" n="e" s="T1035">dĭ </ts>
               <ts e="T1037" id="Seg_6016" n="e" s="T1036">šobi </ts>
               <ts e="T1038" id="Seg_6018" n="e" s="T1037">bünə. </ts>
            </ts>
            <ts e="T1045" id="Seg_6019" n="sc" s="T1039">
               <ts e="T1040" id="Seg_6021" n="e" s="T1039">"Kola, </ts>
               <ts e="T1041" id="Seg_6023" n="e" s="T1040">kola, </ts>
               <ts e="T1042" id="Seg_6025" n="e" s="T1041">šoʔ </ts>
               <ts e="T1043" id="Seg_6027" n="e" s="T1042">döber!" </ts>
               <ts e="T1044" id="Seg_6029" n="e" s="T1043">Kola </ts>
               <ts e="T1045" id="Seg_6031" n="e" s="T1044">šobi. </ts>
            </ts>
            <ts e="T1062" id="Seg_6032" n="sc" s="T1046">
               <ts e="T1047" id="Seg_6034" n="e" s="T1046">"Ĭmbi </ts>
               <ts e="T1048" id="Seg_6036" n="e" s="T1047">tănan </ts>
               <ts e="T1049" id="Seg_6038" n="e" s="T1048">kereʔ, </ts>
               <ts e="T1050" id="Seg_6040" n="e" s="T1049">büzʼe?" </ts>
               <ts e="T1051" id="Seg_6042" n="e" s="T1050">"Da </ts>
               <ts e="T1052" id="Seg_6044" n="e" s="T1051">măn </ts>
               <ts e="T1053" id="Seg_6046" n="e" s="T1052">nükem </ts>
               <ts e="T1054" id="Seg_6048" n="e" s="T1053">(x-) </ts>
               <ts e="T1055" id="Seg_6050" n="e" s="T1054">măndə </ts>
               <ts e="T1056" id="Seg_6052" n="e" s="T1055">(štobɨ </ts>
               <ts e="T1057" id="Seg_6054" n="e" s="T1056">măn </ts>
               <ts e="T1058" id="Seg_6056" n="e" s="T1057">была=) </ts>
               <ts e="T1059" id="Seg_6058" n="e" s="T1058">štobɨ </ts>
               <ts e="T1060" id="Seg_6060" n="e" s="T1059">măn </ts>
               <ts e="T1061" id="Seg_6062" n="e" s="T1060">ibiem </ts>
               <ts e="T1062" id="Seg_6064" n="e" s="T1061">tsaritsajzʼi. </ts>
            </ts>
            <ts e="T1068" id="Seg_6065" n="sc" s="T1063">
               <ts e="T1064" id="Seg_6067" n="e" s="T1063">Štobɨ </ts>
               <ts e="T1065" id="Seg_6069" n="e" s="T1064">măna </ts>
               <ts e="T1066" id="Seg_6071" n="e" s="T1065">il </ts>
               <ts e="T1067" id="Seg_6073" n="e" s="T1066">udatsi </ts>
               <ts e="T1068" id="Seg_6075" n="e" s="T1067">kunnambiʔi". </ts>
            </ts>
            <ts e="T1074" id="Seg_6076" n="sc" s="T1069">
               <ts e="T1070" id="Seg_6078" n="e" s="T1069">"Kanaʔ </ts>
               <ts e="T1071" id="Seg_6080" n="e" s="T1070">maːʔnəl, </ts>
               <ts e="T1072" id="Seg_6082" n="e" s="T1071">bar </ts>
               <ts e="T1073" id="Seg_6084" n="e" s="T1072">moləj </ts>
               <ts e="T1074" id="Seg_6086" n="e" s="T1073">tănan". </ts>
            </ts>
            <ts e="T1076" id="Seg_6087" n="sc" s="T1075">
               <ts e="T1076" id="Seg_6089" n="e" s="T1075">((BRK)). </ts>
            </ts>
            <ts e="T1081" id="Seg_6090" n="sc" s="T1077">
               <ts e="T1078" id="Seg_6092" n="e" s="T1077">Dĭgəttə </ts>
               <ts e="T1079" id="Seg_6094" n="e" s="T1078">büzʼe </ts>
               <ts e="T1080" id="Seg_6096" n="e" s="T1079">kambi </ts>
               <ts e="T1081" id="Seg_6098" n="e" s="T1080">bünə. </ts>
            </ts>
            <ts e="T1091" id="Seg_6099" n="sc" s="T1082">
               <ts e="T1083" id="Seg_6101" n="e" s="T1082">"Kolaʔ, </ts>
               <ts e="T1084" id="Seg_6103" n="e" s="T1083">kolaʔ, </ts>
               <ts e="T1085" id="Seg_6105" n="e" s="T1084">šoʔ </ts>
               <ts e="T1086" id="Seg_6107" n="e" s="T1085">döber!" </ts>
               <ts e="T1087" id="Seg_6109" n="e" s="T1086">Kola </ts>
               <ts e="T1088" id="Seg_6111" n="e" s="T1087">šobi: </ts>
               <ts e="T1089" id="Seg_6113" n="e" s="T1088">"Ĭmbi </ts>
               <ts e="T1090" id="Seg_6115" n="e" s="T1089">tănan </ts>
               <ts e="T1091" id="Seg_6117" n="e" s="T1090">kereʔ?" </ts>
            </ts>
            <ts e="T1101" id="Seg_6118" n="sc" s="T1092">
               <ts e="T1093" id="Seg_6120" n="e" s="T1092">"Da </ts>
               <ts e="T1094" id="Seg_6122" n="e" s="T1093">măn </ts>
               <ts e="T1095" id="Seg_6124" n="e" s="T1094">nükem </ts>
               <ts e="T1096" id="Seg_6126" n="e" s="T1095">măndə, </ts>
               <ts e="T1097" id="Seg_6128" n="e" s="T1096">štobɨ </ts>
               <ts e="T1098" id="Seg_6130" n="e" s="T1097">măn </ts>
               <ts e="T1099" id="Seg_6132" n="e" s="T1098">ibiem </ts>
               <ts e="T1100" id="Seg_6134" n="e" s="T1099">морской </ts>
               <ts e="T1101" id="Seg_6136" n="e" s="T1100">владычицей. </ts>
            </ts>
            <ts e="T1109" id="Seg_6137" n="sc" s="T1102">
               <ts e="T1103" id="Seg_6139" n="e" s="T1102">I </ts>
               <ts e="T1104" id="Seg_6141" n="e" s="T1103">kola </ts>
               <ts e="T1105" id="Seg_6143" n="e" s="T1104">štobɨ </ts>
               <ts e="T1106" id="Seg_6145" n="e" s="T1105">măna </ts>
               <ts e="T1107" id="Seg_6147" n="e" s="T1106">tažerbi </ts>
               <ts e="T1108" id="Seg_6149" n="e" s="T1107">bar </ts>
               <ts e="T1109" id="Seg_6151" n="e" s="T1108">ĭmbi". </ts>
            </ts>
            <ts e="T1121" id="Seg_6152" n="sc" s="T1110">
               <ts e="T1111" id="Seg_6154" n="e" s="T1110">Dĭgəttə </ts>
               <ts e="T1112" id="Seg_6156" n="e" s="T1111">kola </ts>
               <ts e="T1113" id="Seg_6158" n="e" s="T1112">kambi </ts>
               <ts e="T1114" id="Seg_6160" n="e" s="T1113">bünə, </ts>
               <ts e="T1115" id="Seg_6162" n="e" s="T1114">(ĭmbi </ts>
               <ts e="T1116" id="Seg_6164" n="e" s="T1115">st-) </ts>
               <ts e="T1117" id="Seg_6166" n="e" s="T1116">ĭmbidə </ts>
               <ts e="T1118" id="Seg_6168" n="e" s="T1117">(st-) </ts>
               <ts e="T1119" id="Seg_6170" n="e" s="T1118">büzʼenə </ts>
               <ts e="T1120" id="Seg_6172" n="e" s="T1119">ej </ts>
               <ts e="T1121" id="Seg_6174" n="e" s="T1120">nörbəbi. </ts>
            </ts>
            <ts e="T1125" id="Seg_6175" n="sc" s="T1122">
               <ts e="T1123" id="Seg_6177" n="e" s="T1122">Dĭ </ts>
               <ts e="T1124" id="Seg_6179" n="e" s="T1123">šobi </ts>
               <ts e="T1125" id="Seg_6181" n="e" s="T1124">(maːʔndə). </ts>
            </ts>
            <ts e="T1133" id="Seg_6182" n="sc" s="T1126">
               <ts e="T1127" id="Seg_6184" n="e" s="T1126">Nüke </ts>
               <ts e="T1128" id="Seg_6186" n="e" s="T1127">amnolaʔbə, </ts>
               <ts e="T1129" id="Seg_6188" n="e" s="T1128">jererlaʔbə, </ts>
               <ts e="T1130" id="Seg_6190" n="e" s="T1129">i </ts>
               <ts e="T1131" id="Seg_6192" n="e" s="T1130">(kărɨtă=) </ts>
               <ts e="T1132" id="Seg_6194" n="e" s="T1131">kărɨtăt </ts>
               <ts e="T1133" id="Seg_6196" n="e" s="T1132">nulaʔbə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_6197" s="T1">PKZ_196X_SU0227.001 (001)</ta>
            <ta e="T9" id="Seg_6198" s="T6">PKZ_196X_SU0227.002 (002)</ta>
            <ta e="T17" id="Seg_6199" s="T9">PKZ_196X_SU0227.003 (003)</ta>
            <ta e="T21" id="Seg_6200" s="T17">PKZ_196X_SU0227.004 (004)</ta>
            <ta e="T25" id="Seg_6201" s="T21">PKZ_196X_SU0227.005 (005)</ta>
            <ta e="T32" id="Seg_6202" s="T26">PKZ_196X_SU0227.006 (006)</ta>
            <ta e="T44" id="Seg_6203" s="T32">PKZ_196X_SU0227.007 (007)</ta>
            <ta e="T46" id="Seg_6204" s="T44">PKZ_196X_SU0227.008 (008)</ta>
            <ta e="T49" id="Seg_6205" s="T46">PKZ_196X_SU0227.009 (009)</ta>
            <ta e="T57" id="Seg_6206" s="T50">PKZ_196X_SU0227.010 (010)</ta>
            <ta e="T63" id="Seg_6207" s="T57">PKZ_196X_SU0227.011 (011)</ta>
            <ta e="T75" id="Seg_6208" s="T63">PKZ_196X_SU0227.012 (012)</ta>
            <ta e="T79" id="Seg_6209" s="T75">PKZ_196X_SU0227.013 (013)</ta>
            <ta e="T86" id="Seg_6210" s="T79">PKZ_196X_SU0227.014 (014)</ta>
            <ta e="T94" id="Seg_6211" s="T87">PKZ_196X_SU0227.015 (015)</ta>
            <ta e="T98" id="Seg_6212" s="T94">PKZ_196X_SU0227.016 (016)</ta>
            <ta e="T101" id="Seg_6213" s="T98">PKZ_196X_SU0227.017 (017)</ta>
            <ta e="T103" id="Seg_6214" s="T101">PKZ_196X_SU0227.018 (018)</ta>
            <ta e="T114" id="Seg_6215" s="T104">PKZ_196X_SU0227.019 (019)</ta>
            <ta e="T121" id="Seg_6216" s="T115">PKZ_196X_SU0227.020 (020)</ta>
            <ta e="T126" id="Seg_6217" s="T122">PKZ_196X_SU0227.021 (021)</ta>
            <ta e="T129" id="Seg_6218" s="T126">PKZ_196X_SU0227.022 (022)</ta>
            <ta e="T132" id="Seg_6219" s="T129">PKZ_196X_SU0227.023 (023)</ta>
            <ta e="T137" id="Seg_6220" s="T133">PKZ_196X_SU0227.024 (024)</ta>
            <ta e="T143" id="Seg_6221" s="T138">PKZ_196X_SU0227.025 (025)</ta>
            <ta e="T149" id="Seg_6222" s="T143">PKZ_196X_SU0227.026 (026)</ta>
            <ta e="T153" id="Seg_6223" s="T150">PKZ_196X_SU0227.027 (027)</ta>
            <ta e="T156" id="Seg_6224" s="T154">PKZ_196X_SU0227.028 (028)</ta>
            <ta e="T160" id="Seg_6225" s="T157">PKZ_196X_SU0227.029 (029)</ta>
            <ta e="T164" id="Seg_6226" s="T160">PKZ_196X_SU0227.030 (030)</ta>
            <ta e="T172" id="Seg_6227" s="T165">PKZ_196X_SU0227.031 (031)</ta>
            <ta e="T174" id="Seg_6228" s="T173">PKZ_196X_SU0227.032 (032)</ta>
            <ta e="T181" id="Seg_6229" s="T175">PKZ_196X_SU0227.033 (033)</ta>
            <ta e="T188" id="Seg_6230" s="T181">PKZ_196X_SU0227.034 (034)</ta>
            <ta e="T191" id="Seg_6231" s="T188">PKZ_196X_SU0227.035 (035)</ta>
            <ta e="T196" id="Seg_6232" s="T192">PKZ_196X_SU0227.036 (036)</ta>
            <ta e="T203" id="Seg_6233" s="T197">PKZ_196X_SU0227.037 (037)</ta>
            <ta e="T205" id="Seg_6234" s="T204">PKZ_196X_SU0227.038 (038)</ta>
            <ta e="T208" id="Seg_6235" s="T206">PKZ_196X_SU0227.039 (039)</ta>
            <ta e="T213" id="Seg_6236" s="T209">PKZ_196X_SU0227.040 (040)</ta>
            <ta e="T222" id="Seg_6237" s="T214">PKZ_196X_SU0227.041 (041)</ta>
            <ta e="T227" id="Seg_6238" s="T223">PKZ_196X_SU0227.042 (042)</ta>
            <ta e="T231" id="Seg_6239" s="T228">PKZ_196X_SU0227.043 (043)</ta>
            <ta e="T237" id="Seg_6240" s="T232">PKZ_196X_SU0227.044 (044)</ta>
            <ta e="T241" id="Seg_6241" s="T238">PKZ_196X_SU0227.045 (045)</ta>
            <ta e="T243" id="Seg_6242" s="T242">PKZ_196X_SU0227.046 (046)</ta>
            <ta e="T249" id="Seg_6243" s="T244">PKZ_196X_SU0227.047 (047)</ta>
            <ta e="T253" id="Seg_6244" s="T250">PKZ_196X_SU0227.048 (048)</ta>
            <ta e="T255" id="Seg_6245" s="T254">PKZ_196X_SU0227.049 (049)</ta>
            <ta e="T259" id="Seg_6246" s="T256">PKZ_196X_SU0227.050 (050)</ta>
            <ta e="T271" id="Seg_6247" s="T260">PKZ_196X_SU0227.051 (051)</ta>
            <ta e="T274" id="Seg_6248" s="T272">PKZ_196X_SU0227.052 (052)</ta>
            <ta e="T276" id="Seg_6249" s="T275">PKZ_196X_SU0227.053 (053)</ta>
            <ta e="T286" id="Seg_6250" s="T277">PKZ_196X_SU0227.054 (054)</ta>
            <ta e="T292" id="Seg_6251" s="T287">PKZ_196X_SU0227.055 (055)</ta>
            <ta e="T303" id="Seg_6252" s="T293">PKZ_196X_SU0227.056 (056)</ta>
            <ta e="T305" id="Seg_6253" s="T304">PKZ_196X_SU0227.057 (057)</ta>
            <ta e="T311" id="Seg_6254" s="T306">PKZ_196X_SU0227.058 (058)</ta>
            <ta e="T313" id="Seg_6255" s="T312">PKZ_196X_SU0227.059 (059)</ta>
            <ta e="T324" id="Seg_6256" s="T314">PKZ_196X_SU0227.060 (060)</ta>
            <ta e="T326" id="Seg_6257" s="T325">PKZ_196X_SU0227.061 (061)</ta>
            <ta e="T333" id="Seg_6258" s="T327">PKZ_196X_SU0227.062 (062)</ta>
            <ta e="T335" id="Seg_6259" s="T334">PKZ_196X_SU0227.063 (063)</ta>
            <ta e="T342" id="Seg_6260" s="T336">PKZ_196X_SU0227.064 (064)</ta>
            <ta e="T351" id="Seg_6261" s="T343">PKZ_196X_SU0227.065 (065)</ta>
            <ta e="T353" id="Seg_6262" s="T352">PKZ_196X_SU0227.066 (066)</ta>
            <ta e="T360" id="Seg_6263" s="T354">PKZ_196X_SU0227.067 (067)</ta>
            <ta e="T365" id="Seg_6264" s="T361">PKZ_196X_SU0227.068 (068)</ta>
            <ta e="T367" id="Seg_6265" s="T366">PKZ_196X_SU0227.069 (069)</ta>
            <ta e="T369" id="Seg_6266" s="T368">PKZ_196X_SU0227.070 (070)</ta>
            <ta e="T375" id="Seg_6267" s="T370">PKZ_196X_SU0227.071 (071)</ta>
            <ta e="T377" id="Seg_6268" s="T376">PKZ_196X_SU0227.072 (072)</ta>
            <ta e="T385" id="Seg_6269" s="T378">PKZ_196X_SU0227.073 (073)</ta>
            <ta e="T391" id="Seg_6270" s="T386">PKZ_196X_SU0227.074 (074)</ta>
            <ta e="T393" id="Seg_6271" s="T392">PKZ_196X_SU0227.075 (075)</ta>
            <ta e="T397" id="Seg_6272" s="T394">PKZ_196X_SU0227.076 (076)</ta>
            <ta e="T401" id="Seg_6273" s="T397">PKZ_196X_SU0227.077 (077)</ta>
            <ta e="T407" id="Seg_6274" s="T401">PKZ_196X_SU0227.078 (078)</ta>
            <ta e="T409" id="Seg_6275" s="T408">PKZ_196X_SU0227.079 (079)</ta>
            <ta e="T413" id="Seg_6276" s="T410">PKZ_196X_SU0227.080 (080)</ta>
            <ta e="T418" id="Seg_6277" s="T413">PKZ_196X_SU0227.081 (081)</ta>
            <ta e="T427" id="Seg_6278" s="T419">PKZ_196X_SU0227.082 (082)</ta>
            <ta e="T431" id="Seg_6279" s="T428">PKZ_196X_SU0227.083 (083)</ta>
            <ta e="T435" id="Seg_6280" s="T432">PKZ_196X_SU0227.084 (084)</ta>
            <ta e="T441" id="Seg_6281" s="T436">PKZ_196X_SU0227.085 (085)</ta>
            <ta e="T443" id="Seg_6282" s="T442">PKZ_196X_SU0227.086 (086)</ta>
            <ta e="T454" id="Seg_6283" s="T444">PKZ_196X_SU0227.087 (087)</ta>
            <ta e="T458" id="Seg_6284" s="T455">PKZ_196X_SU0227.088 (088)</ta>
            <ta e="T465" id="Seg_6285" s="T459">PKZ_196X_SU0227.089 (089)</ta>
            <ta e="T468" id="Seg_6286" s="T466">PKZ_196X_SU0227.090 (090)</ta>
            <ta e="T474" id="Seg_6287" s="T469">PKZ_196X_SU0227.091 (091)</ta>
            <ta e="T478" id="Seg_6288" s="T475">PKZ_196X_SU0227.092 (092)</ta>
            <ta e="T480" id="Seg_6289" s="T479">PKZ_196X_SU0227.093 (093)</ta>
            <ta e="T492" id="Seg_6290" s="T481">PKZ_196X_SU0227.094 (094)</ta>
            <ta e="T494" id="Seg_6291" s="T493">PKZ_196X_SU0227.095 (095)</ta>
            <ta e="T501" id="Seg_6292" s="T495">PKZ_196X_SU0227.096 (096)</ta>
            <ta e="T503" id="Seg_6293" s="T502">PKZ_196X_SU0227.097 (097)</ta>
            <ta e="T514" id="Seg_6294" s="T504">PKZ_196X_SU0227.098 (098)</ta>
            <ta e="T520" id="Seg_6295" s="T515">PKZ_196X_SU0227.099 (099)</ta>
            <ta e="T528" id="Seg_6296" s="T521">PKZ_196X_SU0227.100 (100)</ta>
            <ta e="T534" id="Seg_6297" s="T529">PKZ_196X_SU0227.101 (101)</ta>
            <ta e="T542" id="Seg_6298" s="T534">PKZ_196X_SU0227.102 (102)</ta>
            <ta e="T552" id="Seg_6299" s="T543">PKZ_196X_SU0227.103 (103)</ta>
            <ta e="T561" id="Seg_6300" s="T553">PKZ_196X_SU0227.104 (104)</ta>
            <ta e="T564" id="Seg_6301" s="T562">PKZ_196X_SU0227.105 (105)</ta>
            <ta e="T570" id="Seg_6302" s="T565">PKZ_196X_SU0227.106 (106)</ta>
            <ta e="T575" id="Seg_6303" s="T571">PKZ_196X_SU0227.107 (107)</ta>
            <ta e="T582" id="Seg_6304" s="T576">PKZ_196X_SU0227.108 (108)</ta>
            <ta e="T586" id="Seg_6305" s="T583">PKZ_196X_SU0227.109 (109)</ta>
            <ta e="T596" id="Seg_6306" s="T587">PKZ_196X_SU0227.110 (110)</ta>
            <ta e="T598" id="Seg_6307" s="T597">PKZ_196X_SU0227.111 (111)</ta>
            <ta e="T604" id="Seg_6308" s="T599">PKZ_196X_SU0227.112 (112)</ta>
            <ta e="T609" id="Seg_6309" s="T605">PKZ_196X_SU0227.113 (113)</ta>
            <ta e="T613" id="Seg_6310" s="T610">PKZ_196X_SU0227.114 (114)</ta>
            <ta e="T616" id="Seg_6311" s="T614">PKZ_196X_SU0227.115 (115)</ta>
            <ta e="T621" id="Seg_6312" s="T617">PKZ_196X_SU0227.116 (116)</ta>
            <ta e="T625" id="Seg_6313" s="T622">PKZ_196X_SU0227.117 (117)</ta>
            <ta e="T631" id="Seg_6314" s="T625">PKZ_196X_SU0227.118 (118)</ta>
            <ta e="T635" id="Seg_6315" s="T632">PKZ_196X_SU0227.119 (119)</ta>
            <ta e="T637" id="Seg_6316" s="T636">PKZ_196X_SU0227.120 (120)</ta>
            <ta e="T644" id="Seg_6317" s="T638">PKZ_196X_SU0227.121 (121)</ta>
            <ta e="T650" id="Seg_6318" s="T645">PKZ_196X_SU0227.122 (122)</ta>
            <ta e="T657" id="Seg_6319" s="T651">PKZ_196X_SU0227.123 (123)</ta>
            <ta e="T669" id="Seg_6320" s="T658">PKZ_196X_SU0227.124 (124)</ta>
            <ta e="T671" id="Seg_6321" s="T670">PKZ_196X_SU0227.125 (125)</ta>
            <ta e="T678" id="Seg_6322" s="T672">PKZ_196X_SU0227.126 (126)</ta>
            <ta e="T680" id="Seg_6323" s="T679">PKZ_196X_SU0227.127 (127)</ta>
            <ta e="T690" id="Seg_6324" s="T681">PKZ_196X_SU0227.128 (128)</ta>
            <ta e="T698" id="Seg_6325" s="T690">PKZ_196X_SU0227.129 (129)</ta>
            <ta e="T700" id="Seg_6326" s="T699">PKZ_196X_SU0227.130 (130)</ta>
            <ta e="T715" id="Seg_6327" s="T701">PKZ_196X_SU0227.131 (131)</ta>
            <ta e="T717" id="Seg_6328" s="T716">PKZ_196X_SU0227.132 (132)</ta>
            <ta e="T726" id="Seg_6329" s="T718">PKZ_196X_SU0227.133 (133)</ta>
            <ta e="T734" id="Seg_6330" s="T727">PKZ_196X_SU0227.134 (134)</ta>
            <ta e="T736" id="Seg_6331" s="T735">PKZ_196X_SU0227.135 (135)</ta>
            <ta e="T744" id="Seg_6332" s="T737">PKZ_196X_SU0227.136 (136)</ta>
            <ta e="T746" id="Seg_6333" s="T745">PKZ_196X_SU0227.137 (137)</ta>
            <ta e="T756" id="Seg_6334" s="T747">PKZ_196X_SU0227.138 (138)</ta>
            <ta e="T758" id="Seg_6335" s="T757">PKZ_196X_SU0227.139 (139)</ta>
            <ta e="T762" id="Seg_6336" s="T759">PKZ_196X_SU0227.140 (140)</ta>
            <ta e="T770" id="Seg_6337" s="T763">PKZ_196X_SU0227.141 (141)</ta>
            <ta e="T772" id="Seg_6338" s="T771">PKZ_196X_SU0227.142 (142)</ta>
            <ta e="T777" id="Seg_6339" s="T773">PKZ_196X_SU0227.143 (143)</ta>
            <ta e="T781" id="Seg_6340" s="T778">PKZ_196X_SU0227.144 (144)</ta>
            <ta e="T785" id="Seg_6341" s="T782">PKZ_196X_SU0227.145 (145)</ta>
            <ta e="T793" id="Seg_6342" s="T786">PKZ_196X_SU0227.146 (146)</ta>
            <ta e="T803" id="Seg_6343" s="T794">PKZ_196X_SU0227.147 (147)</ta>
            <ta e="T807" id="Seg_6344" s="T803">PKZ_196X_SU0227.148 (148)</ta>
            <ta e="T810" id="Seg_6345" s="T808">PKZ_196X_SU0227.149 (149)</ta>
            <ta e="T812" id="Seg_6346" s="T811">PKZ_196X_SU0227.150 (150)</ta>
            <ta e="T819" id="Seg_6347" s="T813">PKZ_196X_SU0227.151 (151)</ta>
            <ta e="T823" id="Seg_6348" s="T820">PKZ_196X_SU0227.152 (152)</ta>
            <ta e="T826" id="Seg_6349" s="T824">PKZ_196X_SU0227.153 (153)</ta>
            <ta e="T832" id="Seg_6350" s="T827">PKZ_196X_SU0227.154 (154)</ta>
            <ta e="T840" id="Seg_6351" s="T833">PKZ_196X_SU0227.155 (155)</ta>
            <ta e="T842" id="Seg_6352" s="T841">PKZ_196X_SU0227.156 (156)</ta>
            <ta e="T848" id="Seg_6353" s="T843">PKZ_196X_SU0227.157 (157)</ta>
            <ta e="T854" id="Seg_6354" s="T849">PKZ_196X_SU0227.158 (158)</ta>
            <ta e="T862" id="Seg_6355" s="T855">PKZ_196X_SU0227.159 (159)</ta>
            <ta e="T867" id="Seg_6356" s="T863">PKZ_196X_SU0227.160 (160)</ta>
            <ta e="T874" id="Seg_6357" s="T868">PKZ_196X_SU0227.161 (161)</ta>
            <ta e="T880" id="Seg_6358" s="T874">PKZ_196X_SU0227.162 (162)</ta>
            <ta e="T884" id="Seg_6359" s="T881">PKZ_196X_SU0227.163 (163)</ta>
            <ta e="T888" id="Seg_6360" s="T885">PKZ_196X_SU0227.164 (164)</ta>
            <ta e="T891" id="Seg_6361" s="T888">PKZ_196X_SU0227.165 (165)</ta>
            <ta e="T893" id="Seg_6362" s="T892">PKZ_196X_SU0227.166 (166)</ta>
            <ta e="T903" id="Seg_6363" s="T894">PKZ_196X_SU0227.167 (167)</ta>
            <ta e="T908" id="Seg_6364" s="T904">PKZ_196X_SU0227.168 (168)</ta>
            <ta e="T916" id="Seg_6365" s="T909">PKZ_196X_SU0227.169 (169) </ta>
            <ta e="T918" id="Seg_6366" s="T916">PKZ_196X_SU0227.170 (170.002)</ta>
            <ta e="T924" id="Seg_6367" s="T919">PKZ_196X_SU0227.171 (171)</ta>
            <ta e="T927" id="Seg_6368" s="T925">PKZ_196X_SU0227.172 (172)</ta>
            <ta e="T932" id="Seg_6369" s="T928">PKZ_196X_SU0227.173 (173)</ta>
            <ta e="T935" id="Seg_6370" s="T933">PKZ_196X_SU0227.174 (174)</ta>
            <ta e="T937" id="Seg_6371" s="T936">PKZ_196X_SU0227.175 (175)</ta>
            <ta e="T951" id="Seg_6372" s="T938">PKZ_196X_SU0227.176 (176) </ta>
            <ta e="T953" id="Seg_6373" s="T952">PKZ_196X_SU0227.177 (178)</ta>
            <ta e="T959" id="Seg_6374" s="T954">PKZ_196X_SU0227.178 (179)</ta>
            <ta e="T975" id="Seg_6375" s="T960">PKZ_196X_SU0227.179 (180) </ta>
            <ta e="T981" id="Seg_6376" s="T976">PKZ_196X_SU0227.180 (182)</ta>
            <ta e="T987" id="Seg_6377" s="T982">PKZ_196X_SU0227.181 (183)</ta>
            <ta e="T995" id="Seg_6378" s="T988">PKZ_196X_SU0227.182 (184)</ta>
            <ta e="T998" id="Seg_6379" s="T996">PKZ_196X_SU0227.183 (185)</ta>
            <ta e="T1000" id="Seg_6380" s="T999">PKZ_196X_SU0227.184 (186)</ta>
            <ta e="T1009" id="Seg_6381" s="T1001">PKZ_196X_SU0227.185 (187)</ta>
            <ta e="T1015" id="Seg_6382" s="T1010">PKZ_196X_SU0227.186 (188)</ta>
            <ta e="T1025" id="Seg_6383" s="T1016">PKZ_196X_SU0227.187 (189)</ta>
            <ta e="T1031" id="Seg_6384" s="T1026">PKZ_196X_SU0227.188 (190)</ta>
            <ta e="T1033" id="Seg_6385" s="T1032">PKZ_196X_SU0227.189 (191)</ta>
            <ta e="T1038" id="Seg_6386" s="T1034">PKZ_196X_SU0227.190 (192)</ta>
            <ta e="T1043" id="Seg_6387" s="T1039">PKZ_196X_SU0227.191 (193)</ta>
            <ta e="T1045" id="Seg_6388" s="T1043">PKZ_196X_SU0227.192 (194)</ta>
            <ta e="T1050" id="Seg_6389" s="T1046">PKZ_196X_SU0227.193 (195)</ta>
            <ta e="T1062" id="Seg_6390" s="T1050">PKZ_196X_SU0227.194 (196)</ta>
            <ta e="T1068" id="Seg_6391" s="T1063">PKZ_196X_SU0227.195 (197)</ta>
            <ta e="T1074" id="Seg_6392" s="T1069">PKZ_196X_SU0227.196 (198)</ta>
            <ta e="T1076" id="Seg_6393" s="T1075">PKZ_196X_SU0227.197 (199)</ta>
            <ta e="T1081" id="Seg_6394" s="T1077">PKZ_196X_SU0227.198 (200)</ta>
            <ta e="T1086" id="Seg_6395" s="T1082">PKZ_196X_SU0227.199 (201)</ta>
            <ta e="T1091" id="Seg_6396" s="T1086">PKZ_196X_SU0227.200 (202)</ta>
            <ta e="T1101" id="Seg_6397" s="T1092">PKZ_196X_SU0227.201 (203)</ta>
            <ta e="T1109" id="Seg_6398" s="T1102">PKZ_196X_SU0227.202 (204)</ta>
            <ta e="T1121" id="Seg_6399" s="T1110">PKZ_196X_SU0227.203 (205)</ta>
            <ta e="T1125" id="Seg_6400" s="T1122">PKZ_196X_SU0227.204 (206)</ta>
            <ta e="T1133" id="Seg_6401" s="T1126">PKZ_196X_SU0227.205 (207)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_6402" s="T1">Šide ulardə i nüke. </ta>
            <ta e="T9" id="Seg_6403" s="T6">Šide koʔbdo ibi. </ta>
            <ta e="T17" id="Seg_6404" s="T9">Šide koʔbdo ibi, dĭgəttə (u-) ular tabun ibi. </ta>
            <ta e="T21" id="Seg_6405" s="T17">Dĭgəttə tabun ine (ibiʔi). </ta>
            <ta e="T25" id="Seg_6406" s="T21">Dĭgəttə tabun tüžöj ibiʔi. </ta>
            <ta e="T32" id="Seg_6407" s="T26">Dĭgəttə onʼiʔ măndərbi a onʼiʔ bar… </ta>
            <ta e="T44" id="Seg_6408" s="T32">((BRK)) Dĭgəttə onʼiʔ unu (mĭnzərbi=) măndərbi dĭn kodurgən, a onʼiʔ svinʼes mĭnzərbi. </ta>
            <ta e="T46" id="Seg_6409" s="T44">Dĭgəttə deʔpi. </ta>
            <ta e="T49" id="Seg_6410" s="T46">I kunə kămnəbi. </ta>
            <ta e="T57" id="Seg_6411" s="T50">(Ku-) Kunə kămnəbi i dĭ bar külambi. </ta>
            <ta e="T63" id="Seg_6412" s="T57">((BRK)) Dĭgəttə dĭn nüjnə bar külaːmbi. </ta>
            <ta e="T75" id="Seg_6413" s="T63">Dĭgəttə dĭ koʔbsaŋ ibiʔi ular, ineʔi i tüžöjʔi i abandə (ka-) kalla dʼürbiʔi. </ta>
            <ta e="T79" id="Seg_6414" s="T75">((BRK)) Dĭgəttə bar skazka. </ta>
            <ta e="T86" id="Seg_6415" s="T79">((BRK)) Tumo i kös i puzɨrʼ kambiʔi. </ta>
            <ta e="T94" id="Seg_6416" s="T87">Dʼăga, a dĭ kanzittə dʼăga, dʼăga mʼaŋnaʔbə. </ta>
            <ta e="T98" id="Seg_6417" s="T94">Tumo bar moʔi embi. </ta>
            <ta e="T101" id="Seg_6418" s="T98">Dĭgəttə tumo kalla dʼürbi. </ta>
            <ta e="T103" id="Seg_6419" s="T101">Puzɨrʼ kalla dʼürbi. </ta>
            <ta e="T114" id="Seg_6420" s="T104">A (köš- kam-) kös kambi i bünə saʔməluʔpi i külambi. </ta>
            <ta e="T121" id="Seg_6421" s="T115">A puzɨrʼ kaknarbi, kaknarbi i külambi. </ta>
            <ta e="T126" id="Seg_6422" s="T122">Tumo unnʼa (amno-) maluʔpi. </ta>
            <ta e="T129" id="Seg_6423" s="T126">Dĭgəttə kambi, kambi. </ta>
            <ta e="T132" id="Seg_6424" s="T129">Esseŋ bar sʼarlaʔbəʔjə. </ta>
            <ta e="T137" id="Seg_6425" s="T133">Bü konnambi mu bügən. </ta>
            <ta e="T143" id="Seg_6426" s="T138">"Esseŋ, (ia=) abat idjot ian. </ta>
            <ta e="T149" id="Seg_6427" s="T143">Kangaʔ maʔnə da (măndlaʔ-) mănaʔ ianə". </ta>
            <ta e="T153" id="Seg_6428" s="T150">Dĭzeŋ nuʔməleʔ šobiʔi. </ta>
            <ta e="T156" id="Seg_6429" s="T154">"Abal šonəga!" </ta>
            <ta e="T160" id="Seg_6430" s="T157">"No kangaʔ, kăštəgaʔ. </ta>
            <ta e="T164" id="Seg_6431" s="T160">Măn tüjö uja mĭnzərləm". </ta>
            <ta e="T172" id="Seg_6432" s="T165">Dĭzeŋ nuʔməluʔbiʔi: "Šoʔ, iam ujam (mĭnz-) mĭnzərləʔbəʔjə. </ta>
            <ta e="T174" id="Seg_6433" s="T173">Üjüʔi". </ta>
            <ta e="T181" id="Seg_6434" s="T175">"Măn tĭm em (na-) naga amzittə. </ta>
            <ta e="T188" id="Seg_6435" s="T181">Pušaj tüžöjdə bătləj i mĭnzərləj, kubanə nuldələj. </ta>
            <ta e="T191" id="Seg_6436" s="T188">Dĭ tüžöj băʔpi. </ta>
            <ta e="T196" id="Seg_6437" s="T192">Uja mĭnzərbi, kubanə embi. </ta>
            <ta e="T203" id="Seg_6438" s="T197">Dĭgəttə esseŋ kambiʔi: "Šoʔ, uja mĭnzərlona!" </ta>
            <ta e="T205" id="Seg_6439" s="T204">Šobiʔi. </ta>
            <ta e="T208" id="Seg_6440" s="T206">"Gijen abam?" </ta>
            <ta e="T213" id="Seg_6441" s="T209">Davaj dĭzem münörzittə, esseŋdə. </ta>
            <ta e="T222" id="Seg_6442" s="T214">A tumo amnolaʔbə: "Iʔ dʼabəroʔ, măn dön, šobiam!" </ta>
            <ta e="T227" id="Seg_6443" s="T223">Teinen bar sĭre saʔməlaʔbə. </ta>
            <ta e="T231" id="Seg_6444" s="T228">I beržə šonəga. </ta>
            <ta e="T237" id="Seg_6445" s="T232">Ugandə šišəge (nʼiʔnen nʼiʔ-) nʼiʔnen. </ta>
            <ta e="T241" id="Seg_6446" s="T238">Kumen kö eššinə? </ta>
            <ta e="T243" id="Seg_6447" s="T242">Nagur. </ta>
            <ta e="T249" id="Seg_6448" s="T244">Dö kön ugandə šišəge ibi. </ta>
            <ta e="T253" id="Seg_6449" s="T250">Kăndlaʔpiʔi il bar. </ta>
            <ta e="T255" id="Seg_6450" s="T254">((BRK)). </ta>
            <ta e="T259" id="Seg_6451" s="T256">Noʔ jaʔpiam šapkuzi. </ta>
            <ta e="T271" id="Seg_6452" s="T260">Dĭgəttə koʔlambi noʔ, măn dĭm oʔbdəbiam kăpnanə, dĭgəttə ine ibiem, tăžerbiam. </ta>
            <ta e="T274" id="Seg_6453" s="T272">Embiem zarottə. </ta>
            <ta e="T276" id="Seg_6454" s="T275">((BRK)). </ta>
            <ta e="T286" id="Seg_6455" s="T277">Dö pʼen sagər keʔbde naga, a kömə keʔbde ibi. </ta>
            <ta e="T292" id="Seg_6456" s="T287">Il iʔgö tažerbiʔi maːndə keʔbdeʔi. </ta>
            <ta e="T303" id="Seg_6457" s="T293">Sanə iʔgö ibi bar, (i-) il ugandə iʔgö oʔbdəbiʔi sanə. </ta>
            <ta e="T305" id="Seg_6458" s="T304">((BRK)). </ta>
            <ta e="T311" id="Seg_6459" s="T306">Nagur pʼe nagobi (ž) sanə. </ta>
            <ta e="T313" id="Seg_6460" s="T312">((BRK)). </ta>
            <ta e="T324" id="Seg_6461" s="T314">Sanə toʔnarzittə ne nado, dĭzeŋ bostə saʔməluʔpiʔi, a il oʔbdəlaʔbəʔjə. </ta>
            <ta e="T326" id="Seg_6462" s="T325">((BRK)). </ta>
            <ta e="T333" id="Seg_6463" s="T327">Dö pʼen len keʔbde nagobi tože. </ta>
            <ta e="T335" id="Seg_6464" s="T334">((BRK)). </ta>
            <ta e="T342" id="Seg_6465" s="T336">Dö kö ugandə budəj jakše ibi. </ta>
            <ta e="T351" id="Seg_6466" s="T343">I toltanoʔ jakše ibi, iʔgö özerbi, urgo ibi. </ta>
            <ta e="T353" id="Seg_6467" s="T352">((BRK)). </ta>
            <ta e="T360" id="Seg_6468" s="T354">Sĭre ipek (pür-) pürlieʔi il ugandə. </ta>
            <ta e="T365" id="Seg_6469" s="T361">Köbergən bar külambi ăgărotkən. </ta>
            <ta e="T367" id="Seg_6470" s="T366">A… </ta>
            <ta e="T369" id="Seg_6471" s="T368">((BRK)). </ta>
            <ta e="T375" id="Seg_6472" s="T370">A dʼijegən köbergən jakše ibi. </ta>
            <ta e="T377" id="Seg_6473" s="T376">((BRK)). </ta>
            <ta e="T385" id="Seg_6474" s="T378">Măn (neg- nĭg-) nĭŋgəbiem, dʼagarbiam i koʔlaʔpi. </ta>
            <ta e="T391" id="Seg_6475" s="T386">A tüj mĭnzərliem ujazi, toltanoʔsi. </ta>
            <ta e="T393" id="Seg_6476" s="T392">((BRK)). </ta>
            <ta e="T397" id="Seg_6477" s="T394">А говорю следом. </ta>
            <ta e="T401" id="Seg_6478" s="T397">Nuzaŋgən (oʔb) kuza külambi. </ta>
            <ta e="T407" id="Seg_6479" s="T401">Dĭzeŋ bar tĭlbiʔi dʼü, dĭgəttə abiʔi… </ta>
            <ta e="T409" id="Seg_6480" s="T408">((BRK)). </ta>
            <ta e="T413" id="Seg_6481" s="T410">Dĭgəttə maʔ abiʔi. </ta>
            <ta e="T418" id="Seg_6482" s="T413">Krospa abiʔi, maʔtə dĭm embiʔi. </ta>
            <ta e="T427" id="Seg_6483" s="T419">(Dĭ- dĭ-) Dĭ dĭʔnə kaŋza ambiʔi, taŋgu ambiʔi. </ta>
            <ta e="T431" id="Seg_6484" s="T428">Multuk ambiʔi bar. </ta>
            <ta e="T435" id="Seg_6485" s="T432">Aspaʔ (ambiʔi=) embiʔi. </ta>
            <ta e="T441" id="Seg_6486" s="T436">Dĭ dĭn mĭnzərləj i amorləj. </ta>
            <ta e="T443" id="Seg_6487" s="T442">((BRK)). </ta>
            <ta e="T454" id="Seg_6488" s="T444">Dĭgəttə dĭ kuzam kumbiʔi, (dʼü-) dʼünə embiʔi, dʼüzi bar kămnəbiʔi. </ta>
            <ta e="T458" id="Seg_6489" s="T455">Dĭgəttə maːndə šobiʔi. </ta>
            <ta e="T465" id="Seg_6490" s="T459">Nüdʼin bar men sarbiʔi, da ine. </ta>
            <ta e="T468" id="Seg_6491" s="T466">Dĭgəttə kanbiʔi. </ta>
            <ta e="T474" id="Seg_6492" s="T469">"Oj, šonəga", — nuʔmələʔbəʔjə turanə bar. </ta>
            <ta e="T478" id="Seg_6493" s="T475">Šakku ajigən enləʔi. </ta>
            <ta e="T480" id="Seg_6494" s="T479">((BRK)). </ta>
            <ta e="T492" id="Seg_6495" s="T481">Men sarbiʔi sagər, i simat - ob, šide, nagur, teʔtə - teʔtə simat. </ta>
            <ta e="T494" id="Seg_6496" s="T493">Четырехглазый. </ta>
            <ta e="T501" id="Seg_6497" s="T495">Šapku ambiʔi, štobɨ penze (s-) dʼagarluʔpi. </ta>
            <ta e="T503" id="Seg_6498" s="T502">((BRK)). </ta>
            <ta e="T514" id="Seg_6499" s="T504">(Onʼiʔ) Onʼiʔ kuza (поселенец) miʔnʼibeʔ ĭzembi, ĭzembi, dĭgəttə (ku-) külambi. </ta>
            <ta e="T520" id="Seg_6500" s="T515">A miʔ (ka-) kanbibaʔ Kazan turanə. </ta>
            <ta e="T528" id="Seg_6501" s="T521">Sazən (udandə uzan-) udandə izittə i ulundə. </ta>
            <ta e="T534" id="Seg_6502" s="T529">Dĭgəttə abəs miʔnʼibeʔ ej mĭbi. </ta>
            <ta e="T542" id="Seg_6503" s="T534">(M-) Miʔ ara ibibeʔ четверть i maʔnʼi šobibaʔ. </ta>
            <ta e="T552" id="Seg_6504" s="T543">Dĭgəttə iam abam (ibi-) ibiʔi dĭ ara, kalla dʼürbiʔi bĭʔsittə. </ta>
            <ta e="T561" id="Seg_6505" s="T553">A miʔ, ešseŋ, miʔ tože ej maləm maʔnə. </ta>
            <ta e="T564" id="Seg_6506" s="T562">"(Kanu-) ((NOISE)) Kanžebəj!" </ta>
            <ta e="T570" id="Seg_6507" s="T565">A măn mămbiam: "Iʔbəʔ, kunolaʔ. </ta>
            <ta e="T575" id="Seg_6508" s="T571">A măn iʔbələm šiʔnʼileʔ. </ta>
            <ta e="T582" id="Seg_6509" s="T576">Kamən măna kabarləj, a šiʔ uʔmeluʔpileʔ." </ta>
            <ta e="T586" id="Seg_6510" s="T583">Dĭgəttə erten uʔbdəbiam. </ta>
            <ta e="T596" id="Seg_6511" s="T587">Ot, kuiol, dĭ iʔbəlaʔbə, (i m-) i miʔ kunolbibaʔ. </ta>
            <ta e="T598" id="Seg_6512" s="T597">((BRK)). </ta>
            <ta e="T604" id="Seg_6513" s="T599">Dĭgəttə dĭ külambi, dĭm băzəbiʔi. </ta>
            <ta e="T609" id="Seg_6514" s="T605">Kujnek šerbiʔi, piʔme šerbiʔi. </ta>
            <ta e="T613" id="Seg_6515" s="T610">Jamaʔi (šoʔpiʔi=) šerbiʔi. </ta>
            <ta e="T616" id="Seg_6516" s="T614">Dĭgəttə kombiʔi. </ta>
            <ta e="T621" id="Seg_6517" s="T617">(Dʼo-) D'üzi kămbiʔi dĭm. </ta>
            <ta e="T625" id="Seg_6518" s="T622">Šobiʔi maʔndə (na-). </ta>
            <ta e="T631" id="Seg_6519" s="T625">Ara biʔpiʔi, ipek amorbiʔi, uja ambiʔi. </ta>
            <ta e="T635" id="Seg_6520" s="T632">I maʔndə kalla dʼürbiʔi. </ta>
            <ta e="T637" id="Seg_6521" s="T636">((BRK)). </ta>
            <ta e="T644" id="Seg_6522" s="T638">Măn onʼiʔ raz (šo-) šobiam Kazan turagən. </ta>
            <ta e="T650" id="Seg_6523" s="T645">Šobiam Permʼakovăn, dĭgəttə maʔndə šobiam. </ta>
            <ta e="T657" id="Seg_6524" s="T651">Tʼerməngən dĭn ildəm šindidə (perluʔ-) (perluʔpiʔi). </ta>
            <ta e="T669" id="Seg_6525" s="T658">A măn šobiam da (muzuruk-) dĭgəttə toʔnarləm, bar aktʼa iʔgö moləj. </ta>
            <ta e="T671" id="Seg_6526" s="T670">((BRK)). </ta>
            <ta e="T678" id="Seg_6527" s="T672">Šobiam (maʔ-) maʔndə, šindində ej kubiom. </ta>
            <ta e="T680" id="Seg_6528" s="T679">((BRK)). </ta>
            <ta e="T690" id="Seg_6529" s="T681">Măn ulum ĭzemnie, simam ĭzemnie, a tăn ĭmbi ĭzemnie? </ta>
            <ta e="T698" id="Seg_6530" s="T690">A măn tĭmem (ĭzembie-) ĭzemneʔbə i pĭjem ĭzemneʔbə. </ta>
            <ta e="T700" id="Seg_6531" s="T699">((BRK)). </ta>
            <ta e="T715" id="Seg_6532" s="T701">Ivanovičən bar šüjot ĭzemnie i ujut ĭzemnie, i (ud-) (udazaŋdə) ĭzemneʔpəʔjə, bar tăŋ ĭzemnie. </ta>
            <ta e="T717" id="Seg_6533" s="T716">((BRK)). </ta>
            <ta e="T726" id="Seg_6534" s="T718">Nada (da- da-) dʼazirzittə dĭ (na-) nagur kuza. </ta>
            <ta e="T734" id="Seg_6535" s="T727">Doxtăr nada deʔsittə, pušaj dʼazirləj dĭ dĭzem. </ta>
            <ta e="T736" id="Seg_6536" s="T735">((BRK)). </ta>
            <ta e="T744" id="Seg_6537" s="T737">Ĭmbi-nʼibudʼ mĭləj bĭʔsittə ali ĭmbi-nʼibudʼ mĭləj kĭškəsʼtə. </ta>
            <ta e="T746" id="Seg_6538" s="T745">((BRK)). </ta>
            <ta e="T756" id="Seg_6539" s="T747">Dĭzeŋ bar tăŋ kănnambiʔi, a tuj ugandə tăŋ ĭzemneʔbəʔjə. </ta>
            <ta e="T758" id="Seg_6540" s="T757">((BRK)). </ta>
            <ta e="T762" id="Seg_6541" s="T759">Marejka urgajanə kadəldə. </ta>
            <ta e="T770" id="Seg_6542" s="T763">Urgajanə dĭrgit kadəldə, i simat dĭrgit (urgajagəndə) ((NOISE)). </ta>
            <ta e="T772" id="Seg_6543" s="T771">((BRK)). </ta>
            <ta e="T777" id="Seg_6544" s="T773">Amnobiʔi nüke i büzʼe. </ta>
            <ta e="T781" id="Seg_6545" s="T778">Samən bügən toʔndə. </ta>
            <ta e="T785" id="Seg_6546" s="T782">Nüke ererla amnolaʔpi. </ta>
            <ta e="T793" id="Seg_6547" s="T786">A büzʼe mĭmbi bünə kola (dʼapitʼ-) dʼabəsʼtə. </ta>
            <ta e="T803" id="Seg_6548" s="T794">(Onʼiʔ raz=) Onʼiʔ raz kambi, dĭgəttə ĭmbidə ej dʼaʔpi. </ta>
            <ta e="T807" id="Seg_6549" s="T803">Onʼiʔ riba dʼaʔpi, kuvas. </ta>
            <ta e="T810" id="Seg_6550" s="T808">"Măn pʼeleʔbə…" </ta>
            <ta e="T812" id="Seg_6551" s="T811">((BRK)). </ta>
            <ta e="T819" id="Seg_6552" s="T813">Kola kuvas, dʼăbaktərlia kuzanə (я-) šiketsi. </ta>
            <ta e="T823" id="Seg_6553" s="T820">"Öʔleʔ măna bünə!" </ta>
            <ta e="T826" id="Seg_6554" s="T824">"Măn öʔluʔpiam". </ta>
            <ta e="T832" id="Seg_6555" s="T827">Šobi maʔndə da nükenə dʼăbaktərlabə. </ta>
            <ta e="T840" id="Seg_6556" s="T833">"Kola bar kuvas ibi, dʼăbaktərbi kak kuza". </ta>
            <ta e="T842" id="Seg_6557" s="T841">((BRK)). </ta>
            <ta e="T848" id="Seg_6558" s="T843">Dĭgəttə dĭ nüke davaj kudonzittə. </ta>
            <ta e="T854" id="Seg_6559" s="T849">"Ĭmbi tăn ĭmbidə ej pʼebiel? </ta>
            <ta e="T862" id="Seg_6560" s="T855">Kărɨtă miʔnʼibeʔ naga, (ši- šiz-) šižəbi (kərɨ-) kărĭtă". </ta>
            <ta e="T867" id="Seg_6561" s="T863">Dĭ kambi bazo bünə. </ta>
            <ta e="T874" id="Seg_6562" s="T868">Dĭgəttə davaj kirgarzittə: "Kola, šoʔ döber!" </ta>
            <ta e="T880" id="Seg_6563" s="T874">Dĭ šobi: "Ĭmbi tănan, büzʼe, kereʔ?" </ta>
            <ta e="T884" id="Seg_6564" s="T881">"Măna kereʔ kărɨtă. </ta>
            <ta e="T888" id="Seg_6565" s="T885">Nükem bar kudonzlaʔbə". </ta>
            <ta e="T891" id="Seg_6566" s="T888">"Nu kanaʔ manə". </ta>
            <ta e="T893" id="Seg_6567" s="T892">((BRK)). </ta>
            <ta e="T903" id="Seg_6568" s="T894">Dĭ nüke davaj dĭm kudonzittə: "Ĭmbi ej pʼebiel tura? </ta>
            <ta e="T908" id="Seg_6569" s="T904">Miʔ tura ej kuvas". </ta>
            <ta e="T916" id="Seg_6570" s="T909">Dĭ bazo kambi, davaj kirgarzittə: "Kola, kola! </ta>
            <ta e="T918" id="Seg_6571" s="T916">šoʔ döber!" </ta>
            <ta e="T924" id="Seg_6572" s="T919">Kola šobi: "Ĭmbi tănan kereʔ?" </ta>
            <ta e="T927" id="Seg_6573" s="T925">"Tura kereʔ. </ta>
            <ta e="T932" id="Seg_6574" s="T928">Miʔ turabaʔ ej kuvas". </ta>
            <ta e="T935" id="Seg_6575" s="T933">"Kanaʔ maʔnəl!" </ta>
            <ta e="T937" id="Seg_6576" s="T936">((BRK)). </ta>
            <ta e="T951" id="Seg_6577" s="T938">Dĭgəttə bazo dĭ nüke kudonzəbi: "Kanaʔ kolanə, pʼeʔ štobɨ măn mobiam kupčixăjzi". </ta>
            <ta e="T953" id="Seg_6578" s="T952">((BRK)). </ta>
            <ta e="T959" id="Seg_6579" s="T954">Dĭgəttə kola mămbi: "Kanaʔ maːʔnəl". </ta>
            <ta e="T975" id="Seg_6580" s="T960">Dĭ šöbi, a dĭ nüke bazo kudonzlaʔbə: "Kanaʔ kolanə, peʔ štobɨ măn ibiem dvărʼankajzi". </ta>
            <ta e="T981" id="Seg_6581" s="T976">Dĭ kambi kolanə, kola kirgarlaʔpi. </ta>
            <ta e="T987" id="Seg_6582" s="T982">Kola šobi: "Ĭmbi tănan kereʔ?" </ta>
            <ta e="T995" id="Seg_6583" s="T988">"Măn nükem mănde štobɨ măn dvărʼankăjzi ibiem". </ta>
            <ta e="T998" id="Seg_6584" s="T996">"Kanaʔ maːʔnəl!" </ta>
            <ta e="T1000" id="Seg_6585" s="T999">((BRK)). </ta>
            <ta e="T1009" id="Seg_6586" s="T1001">Dĭgəttə büzʼe šobi maːʔndə, dĭ davaj bazo kudonzittə. </ta>
            <ta e="T1015" id="Seg_6587" s="T1010">(I baʔ-) I münörzittə xat'el. </ta>
            <ta e="T1025" id="Seg_6588" s="T1016">"Kanaʔ bazo kolanə, peʔ štobɨ măn была (гос-) государыней. </ta>
            <ta e="T1031" id="Seg_6589" s="T1026">Štobɨ jil măna udazaŋdə носили. </ta>
            <ta e="T1033" id="Seg_6590" s="T1032">((BRK)). </ta>
            <ta e="T1038" id="Seg_6591" s="T1034">Dĭgəttə dĭ šobi bünə. </ta>
            <ta e="T1043" id="Seg_6592" s="T1039">"Kola, kola, šoʔ döber!" </ta>
            <ta e="T1045" id="Seg_6593" s="T1043">Kola šobi. </ta>
            <ta e="T1050" id="Seg_6594" s="T1046">"Ĭmbi tănan kereʔ, büzʼe?" </ta>
            <ta e="T1062" id="Seg_6595" s="T1050">"Da măn nükem (x-) măndə (štobɨ măn была=) štobɨ măn ibiem tsaritsajzʼi. </ta>
            <ta e="T1068" id="Seg_6596" s="T1063">Štobɨ măna il udatsi kunnambiʔi". </ta>
            <ta e="T1074" id="Seg_6597" s="T1069">"Kanaʔ maːʔnəl, bar moləj tănan". </ta>
            <ta e="T1076" id="Seg_6598" s="T1075">((BRK)). </ta>
            <ta e="T1081" id="Seg_6599" s="T1077">Dĭgəttə büzʼe kambi bünə. </ta>
            <ta e="T1086" id="Seg_6600" s="T1082">"Kolaʔ, kolaʔ, šoʔ döber!" </ta>
            <ta e="T1091" id="Seg_6601" s="T1086">Kola šobi: "Ĭmbi tănan kereʔ?" </ta>
            <ta e="T1101" id="Seg_6602" s="T1092">"Da măn nükem măndə, štobɨ măn ibiem морской владычицей. </ta>
            <ta e="T1109" id="Seg_6603" s="T1102">I kola štobɨ măna tažerbi bar ĭmbi". </ta>
            <ta e="T1121" id="Seg_6604" s="T1110">Dĭgəttə kola kambi bünə, (ĭmbi st-) ĭmbidə (st-) büzʼenə ej nörbəbi. </ta>
            <ta e="T1125" id="Seg_6605" s="T1122">Dĭ šobi (maːʔndə). </ta>
            <ta e="T1133" id="Seg_6606" s="T1126">Nüke amnolaʔbə, jererlaʔbə, i (kărɨtă=) kărɨtăt nulaʔbə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_6607" s="T1">šide</ta>
            <ta e="T3" id="Seg_6608" s="T2">ular-də</ta>
            <ta e="T4" id="Seg_6609" s="T3">i</ta>
            <ta e="T5" id="Seg_6610" s="T4">nüke</ta>
            <ta e="T7" id="Seg_6611" s="T6">šide</ta>
            <ta e="T8" id="Seg_6612" s="T7">koʔbdo</ta>
            <ta e="T9" id="Seg_6613" s="T8">i-bi</ta>
            <ta e="T10" id="Seg_6614" s="T9">šide</ta>
            <ta e="T11" id="Seg_6615" s="T10">koʔbdo</ta>
            <ta e="T12" id="Seg_6616" s="T11">i-bi</ta>
            <ta e="T13" id="Seg_6617" s="T12">dĭgəttə</ta>
            <ta e="T15" id="Seg_6618" s="T14">ular</ta>
            <ta e="T16" id="Seg_6619" s="T15">tabun</ta>
            <ta e="T17" id="Seg_6620" s="T16">i-bi</ta>
            <ta e="T18" id="Seg_6621" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_6622" s="T18">tabun</ta>
            <ta e="T20" id="Seg_6623" s="T19">ine</ta>
            <ta e="T21" id="Seg_6624" s="T20">i-bi-ʔi</ta>
            <ta e="T22" id="Seg_6625" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_6626" s="T22">tabun</ta>
            <ta e="T24" id="Seg_6627" s="T23">tüžöj</ta>
            <ta e="T25" id="Seg_6628" s="T24">i-bi-ʔi</ta>
            <ta e="T27" id="Seg_6629" s="T26">dĭgəttə</ta>
            <ta e="T28" id="Seg_6630" s="T27">onʼiʔ</ta>
            <ta e="T29" id="Seg_6631" s="T28">măndə-r-bi</ta>
            <ta e="T30" id="Seg_6632" s="T29">a</ta>
            <ta e="T31" id="Seg_6633" s="T30">onʼiʔ</ta>
            <ta e="T32" id="Seg_6634" s="T31">bar</ta>
            <ta e="T34" id="Seg_6635" s="T33">dĭgəttə</ta>
            <ta e="T35" id="Seg_6636" s="T34">onʼiʔ</ta>
            <ta e="T36" id="Seg_6637" s="T35">unu</ta>
            <ta e="T37" id="Seg_6638" s="T36">mĭnzər-bi</ta>
            <ta e="T38" id="Seg_6639" s="T37">măndə-r-bi</ta>
            <ta e="T39" id="Seg_6640" s="T38">dĭn</ta>
            <ta e="T40" id="Seg_6641" s="T39">kodur-gən</ta>
            <ta e="T41" id="Seg_6642" s="T40">a</ta>
            <ta e="T42" id="Seg_6643" s="T41">onʼiʔ</ta>
            <ta e="T43" id="Seg_6644" s="T42">svinʼes</ta>
            <ta e="T44" id="Seg_6645" s="T43">mĭnzər-bi</ta>
            <ta e="T45" id="Seg_6646" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_6647" s="T45">deʔ-pi</ta>
            <ta e="T47" id="Seg_6648" s="T46">i</ta>
            <ta e="T48" id="Seg_6649" s="T47">ku-nə</ta>
            <ta e="T49" id="Seg_6650" s="T48">kămnə-bi</ta>
            <ta e="T52" id="Seg_6651" s="T51">ku-nə</ta>
            <ta e="T53" id="Seg_6652" s="T52">kămnə-bi</ta>
            <ta e="T54" id="Seg_6653" s="T53">i</ta>
            <ta e="T55" id="Seg_6654" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_6655" s="T55">bar</ta>
            <ta e="T57" id="Seg_6656" s="T56">kü-lam-bi</ta>
            <ta e="T59" id="Seg_6657" s="T58">dĭgəttə</ta>
            <ta e="T60" id="Seg_6658" s="T59">dĭ-n</ta>
            <ta e="T61" id="Seg_6659" s="T60">nüjnə</ta>
            <ta e="T62" id="Seg_6660" s="T61">bar</ta>
            <ta e="T63" id="Seg_6661" s="T62">kü-laːm-bi</ta>
            <ta e="T64" id="Seg_6662" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_6663" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_6664" s="T65">koʔb-saŋ</ta>
            <ta e="T67" id="Seg_6665" s="T66">i-bi-ʔi</ta>
            <ta e="T68" id="Seg_6666" s="T67">ular</ta>
            <ta e="T69" id="Seg_6667" s="T68">ine-ʔi</ta>
            <ta e="T70" id="Seg_6668" s="T69">i</ta>
            <ta e="T71" id="Seg_6669" s="T70">tüžöj-ʔi</ta>
            <ta e="T72" id="Seg_6670" s="T71">i</ta>
            <ta e="T73" id="Seg_6671" s="T72">aba-ndə</ta>
            <ta e="T1134" id="Seg_6672" s="T74">kal-la</ta>
            <ta e="T75" id="Seg_6673" s="T1134">dʼür-bi-ʔi</ta>
            <ta e="T77" id="Seg_6674" s="T76">dĭgəttə</ta>
            <ta e="T78" id="Seg_6675" s="T77">bar</ta>
            <ta e="T79" id="Seg_6676" s="T78">skazka</ta>
            <ta e="T81" id="Seg_6677" s="T80">tumo</ta>
            <ta e="T82" id="Seg_6678" s="T81">i</ta>
            <ta e="T83" id="Seg_6679" s="T82">kös</ta>
            <ta e="T84" id="Seg_6680" s="T83">i</ta>
            <ta e="T85" id="Seg_6681" s="T84">puzɨrʼ</ta>
            <ta e="T86" id="Seg_6682" s="T85">kam-bi-ʔi</ta>
            <ta e="T88" id="Seg_6683" s="T87">dʼăga</ta>
            <ta e="T89" id="Seg_6684" s="T88">a</ta>
            <ta e="T90" id="Seg_6685" s="T89">dĭ</ta>
            <ta e="T91" id="Seg_6686" s="T90">kan-zittə</ta>
            <ta e="T92" id="Seg_6687" s="T91">dʼăga</ta>
            <ta e="T93" id="Seg_6688" s="T92">dʼăga</ta>
            <ta e="T94" id="Seg_6689" s="T93">mʼaŋ-naʔbə</ta>
            <ta e="T95" id="Seg_6690" s="T94">tumo</ta>
            <ta e="T96" id="Seg_6691" s="T95">bar</ta>
            <ta e="T97" id="Seg_6692" s="T96">mo-ʔi</ta>
            <ta e="T98" id="Seg_6693" s="T97">em-bi</ta>
            <ta e="T99" id="Seg_6694" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_6695" s="T99">tumo</ta>
            <ta e="T1135" id="Seg_6696" s="T100">kal-la</ta>
            <ta e="T101" id="Seg_6697" s="T1135">dʼür-bi</ta>
            <ta e="T102" id="Seg_6698" s="T101">puzɨrʼ</ta>
            <ta e="T1136" id="Seg_6699" s="T102">kal-la</ta>
            <ta e="T103" id="Seg_6700" s="T1136">dʼür-bi</ta>
            <ta e="T105" id="Seg_6701" s="T104">a</ta>
            <ta e="T106" id="Seg_6702" s="T105">köš</ta>
            <ta e="T107" id="Seg_6703" s="T106">kam</ta>
            <ta e="T108" id="Seg_6704" s="T107">kös</ta>
            <ta e="T109" id="Seg_6705" s="T108">kam-bi</ta>
            <ta e="T110" id="Seg_6706" s="T109">i</ta>
            <ta e="T111" id="Seg_6707" s="T110">bü-nə</ta>
            <ta e="T112" id="Seg_6708" s="T111">saʔmə-luʔ-pi</ta>
            <ta e="T113" id="Seg_6709" s="T112">i</ta>
            <ta e="T114" id="Seg_6710" s="T113">kü-lam-bi</ta>
            <ta e="T116" id="Seg_6711" s="T115">a</ta>
            <ta e="T117" id="Seg_6712" s="T116">puzɨrʼ</ta>
            <ta e="T118" id="Seg_6713" s="T117">kaknar-bi</ta>
            <ta e="T119" id="Seg_6714" s="T118">kaknar-bi</ta>
            <ta e="T120" id="Seg_6715" s="T119">i</ta>
            <ta e="T121" id="Seg_6716" s="T120">kü-lam-bi</ta>
            <ta e="T123" id="Seg_6717" s="T122">tumo</ta>
            <ta e="T124" id="Seg_6718" s="T123">unnʼa</ta>
            <ta e="T126" id="Seg_6719" s="T125">ma-luʔ-pi</ta>
            <ta e="T127" id="Seg_6720" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_6721" s="T127">kam-bi</ta>
            <ta e="T129" id="Seg_6722" s="T128">kam-bi</ta>
            <ta e="T130" id="Seg_6723" s="T129">es-seŋ</ta>
            <ta e="T131" id="Seg_6724" s="T130">bar</ta>
            <ta e="T132" id="Seg_6725" s="T131">sʼar-laʔbə-ʔjə</ta>
            <ta e="T134" id="Seg_6726" s="T133">bü</ta>
            <ta e="T135" id="Seg_6727" s="T134">kon-nam-bi</ta>
            <ta e="T136" id="Seg_6728" s="T135">mu</ta>
            <ta e="T137" id="Seg_6729" s="T136">bü-gən</ta>
            <ta e="T139" id="Seg_6730" s="T138">es-seŋ</ta>
            <ta e="T140" id="Seg_6731" s="T139">ia</ta>
            <ta e="T141" id="Seg_6732" s="T140">aba-t</ta>
            <ta e="T143" id="Seg_6733" s="T142">ia-n</ta>
            <ta e="T144" id="Seg_6734" s="T143">kan-gaʔ</ta>
            <ta e="T145" id="Seg_6735" s="T144">maʔ-nə</ta>
            <ta e="T146" id="Seg_6736" s="T145">da</ta>
            <ta e="T147" id="Seg_6737" s="T146">măn-d-laʔ</ta>
            <ta e="T148" id="Seg_6738" s="T147">măn-a-ʔ</ta>
            <ta e="T149" id="Seg_6739" s="T148">ia-nə</ta>
            <ta e="T151" id="Seg_6740" s="T150">dĭ-zeŋ</ta>
            <ta e="T152" id="Seg_6741" s="T151">nuʔmə-leʔ</ta>
            <ta e="T153" id="Seg_6742" s="T152">šo-bi-ʔi</ta>
            <ta e="T155" id="Seg_6743" s="T154">aba-l</ta>
            <ta e="T156" id="Seg_6744" s="T155">šonə-ga</ta>
            <ta e="T158" id="Seg_6745" s="T157">no</ta>
            <ta e="T159" id="Seg_6746" s="T158">kan-gaʔ</ta>
            <ta e="T160" id="Seg_6747" s="T159">kăštə-gaʔ</ta>
            <ta e="T161" id="Seg_6748" s="T160">măn</ta>
            <ta e="T162" id="Seg_6749" s="T161">tüjö</ta>
            <ta e="T163" id="Seg_6750" s="T162">uja</ta>
            <ta e="T164" id="Seg_6751" s="T163">mĭnzər-lə-m</ta>
            <ta e="T166" id="Seg_6752" s="T165">dĭ-zeŋ</ta>
            <ta e="T167" id="Seg_6753" s="T166">nuʔmə-luʔ-bi-ʔi</ta>
            <ta e="T168" id="Seg_6754" s="T167">šo-ʔ</ta>
            <ta e="T169" id="Seg_6755" s="T168">ia-m</ta>
            <ta e="T170" id="Seg_6756" s="T169">uja-m</ta>
            <ta e="T172" id="Seg_6757" s="T171">mĭnzər-ləʔbə-ʔjə</ta>
            <ta e="T174" id="Seg_6758" s="T173">üjü-ʔi</ta>
            <ta e="T176" id="Seg_6759" s="T175">măn</ta>
            <ta e="T177" id="Seg_6760" s="T176">tĭ-m</ta>
            <ta e="T178" id="Seg_6761" s="T177">e-m</ta>
            <ta e="T180" id="Seg_6762" s="T179">naga</ta>
            <ta e="T181" id="Seg_6763" s="T180">am-zittə</ta>
            <ta e="T182" id="Seg_6764" s="T181">pušaj</ta>
            <ta e="T183" id="Seg_6765" s="T182">tüžöj-də</ta>
            <ta e="T184" id="Seg_6766" s="T183">băt-lə-j</ta>
            <ta e="T185" id="Seg_6767" s="T184">i</ta>
            <ta e="T186" id="Seg_6768" s="T185">mĭnzər-lə-j</ta>
            <ta e="T187" id="Seg_6769" s="T186">kuba-nə</ta>
            <ta e="T188" id="Seg_6770" s="T187">nuldə-lə-j</ta>
            <ta e="T189" id="Seg_6771" s="T188">dĭ</ta>
            <ta e="T190" id="Seg_6772" s="T189">tüžöj</ta>
            <ta e="T191" id="Seg_6773" s="T190">băʔ-pi</ta>
            <ta e="T193" id="Seg_6774" s="T192">uja</ta>
            <ta e="T194" id="Seg_6775" s="T193">mĭnzər-bi</ta>
            <ta e="T195" id="Seg_6776" s="T194">kuba-nə</ta>
            <ta e="T196" id="Seg_6777" s="T195">em-bi</ta>
            <ta e="T198" id="Seg_6778" s="T197">dĭgəttə</ta>
            <ta e="T199" id="Seg_6779" s="T198">es-seŋ</ta>
            <ta e="T200" id="Seg_6780" s="T199">kam-bi-ʔi</ta>
            <ta e="T201" id="Seg_6781" s="T200">šo-ʔ</ta>
            <ta e="T202" id="Seg_6782" s="T201">uja</ta>
            <ta e="T203" id="Seg_6783" s="T202">mĭnzər-l-o-na</ta>
            <ta e="T205" id="Seg_6784" s="T204">šo-bi-ʔi</ta>
            <ta e="T207" id="Seg_6785" s="T206">gijen</ta>
            <ta e="T208" id="Seg_6786" s="T207">aba-m</ta>
            <ta e="T210" id="Seg_6787" s="T209">davaj</ta>
            <ta e="T211" id="Seg_6788" s="T210">dĭ-zem</ta>
            <ta e="T212" id="Seg_6789" s="T211">münör-zittə</ta>
            <ta e="T213" id="Seg_6790" s="T212">es-seŋ-də</ta>
            <ta e="T215" id="Seg_6791" s="T214">a</ta>
            <ta e="T216" id="Seg_6792" s="T215">tumo</ta>
            <ta e="T217" id="Seg_6793" s="T216">amno-laʔbə</ta>
            <ta e="T218" id="Seg_6794" s="T217">i-ʔ</ta>
            <ta e="T219" id="Seg_6795" s="T218">dʼabəro-ʔ</ta>
            <ta e="T220" id="Seg_6796" s="T219">măn</ta>
            <ta e="T221" id="Seg_6797" s="T220">dön</ta>
            <ta e="T222" id="Seg_6798" s="T221">šo-bia-m</ta>
            <ta e="T224" id="Seg_6799" s="T223">teinen</ta>
            <ta e="T225" id="Seg_6800" s="T224">bar</ta>
            <ta e="T226" id="Seg_6801" s="T225">sĭre</ta>
            <ta e="T227" id="Seg_6802" s="T226">saʔmə-laʔbə</ta>
            <ta e="T229" id="Seg_6803" s="T228">i</ta>
            <ta e="T230" id="Seg_6804" s="T229">beržə</ta>
            <ta e="T231" id="Seg_6805" s="T230">šonə-ga</ta>
            <ta e="T233" id="Seg_6806" s="T232">ugandə</ta>
            <ta e="T234" id="Seg_6807" s="T233">šišəge</ta>
            <ta e="T235" id="Seg_6808" s="T234">nʼiʔnen</ta>
            <ta e="T237" id="Seg_6809" s="T236">nʼiʔnen</ta>
            <ta e="T239" id="Seg_6810" s="T238">kumen</ta>
            <ta e="T240" id="Seg_6811" s="T239">kö</ta>
            <ta e="T241" id="Seg_6812" s="T240">ešši-nə</ta>
            <ta e="T243" id="Seg_6813" s="T242">nagur</ta>
            <ta e="T245" id="Seg_6814" s="T244">dö</ta>
            <ta e="T246" id="Seg_6815" s="T245">kö-n</ta>
            <ta e="T247" id="Seg_6816" s="T246">ugandə</ta>
            <ta e="T248" id="Seg_6817" s="T247">šišəge</ta>
            <ta e="T249" id="Seg_6818" s="T248">i-bi</ta>
            <ta e="T251" id="Seg_6819" s="T250">kănd-laʔ-pi-ʔi</ta>
            <ta e="T252" id="Seg_6820" s="T251">il</ta>
            <ta e="T253" id="Seg_6821" s="T252">bar</ta>
            <ta e="T257" id="Seg_6822" s="T256">noʔ</ta>
            <ta e="T258" id="Seg_6823" s="T257">jaʔ-pia-m</ta>
            <ta e="T259" id="Seg_6824" s="T258">šapku-zi</ta>
            <ta e="T261" id="Seg_6825" s="T260">dĭgəttə</ta>
            <ta e="T262" id="Seg_6826" s="T261">koʔ-lam-bi</ta>
            <ta e="T263" id="Seg_6827" s="T262">noʔ</ta>
            <ta e="T264" id="Seg_6828" s="T263">măn</ta>
            <ta e="T265" id="Seg_6829" s="T264">dĭ-m</ta>
            <ta e="T266" id="Seg_6830" s="T265">oʔbdə-bia-m</ta>
            <ta e="T267" id="Seg_6831" s="T266">kăpna-nə</ta>
            <ta e="T268" id="Seg_6832" s="T267">dĭgəttə</ta>
            <ta e="T269" id="Seg_6833" s="T268">ine</ta>
            <ta e="T270" id="Seg_6834" s="T269">i-bie-m</ta>
            <ta e="T271" id="Seg_6835" s="T270">tăžer-bia-m</ta>
            <ta e="T273" id="Seg_6836" s="T272">em-bie-m</ta>
            <ta e="T274" id="Seg_6837" s="T273">zarot-tə</ta>
            <ta e="T278" id="Seg_6838" s="T277">dö</ta>
            <ta e="T279" id="Seg_6839" s="T278">pʼe-n</ta>
            <ta e="T280" id="Seg_6840" s="T279">sagər</ta>
            <ta e="T281" id="Seg_6841" s="T280">keʔbde</ta>
            <ta e="T282" id="Seg_6842" s="T281">naga</ta>
            <ta e="T283" id="Seg_6843" s="T282">a</ta>
            <ta e="T284" id="Seg_6844" s="T283">kömə</ta>
            <ta e="T285" id="Seg_6845" s="T284">keʔbde</ta>
            <ta e="T286" id="Seg_6846" s="T285">i-bi</ta>
            <ta e="T288" id="Seg_6847" s="T287">il</ta>
            <ta e="T289" id="Seg_6848" s="T288">iʔgö</ta>
            <ta e="T290" id="Seg_6849" s="T289">tažer-bi-ʔi</ta>
            <ta e="T291" id="Seg_6850" s="T290">ma-ndə</ta>
            <ta e="T292" id="Seg_6851" s="T291">keʔbde-ʔi</ta>
            <ta e="T294" id="Seg_6852" s="T293">sanə</ta>
            <ta e="T295" id="Seg_6853" s="T294">iʔgö</ta>
            <ta e="T296" id="Seg_6854" s="T295">i-bi</ta>
            <ta e="T297" id="Seg_6855" s="T296">bar</ta>
            <ta e="T299" id="Seg_6856" s="T298">il</ta>
            <ta e="T300" id="Seg_6857" s="T299">ugandə</ta>
            <ta e="T301" id="Seg_6858" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_6859" s="T301">oʔbdə-bi-ʔi</ta>
            <ta e="T303" id="Seg_6860" s="T302">sanə</ta>
            <ta e="T307" id="Seg_6861" s="T306">nagur</ta>
            <ta e="T308" id="Seg_6862" s="T307">pʼe</ta>
            <ta e="T309" id="Seg_6863" s="T308">nago-bi</ta>
            <ta e="T310" id="Seg_6864" s="T309">ž</ta>
            <ta e="T311" id="Seg_6865" s="T310">sanə</ta>
            <ta e="T315" id="Seg_6866" s="T314">sanə</ta>
            <ta e="T316" id="Seg_6867" s="T315">toʔ-nar-zittə</ta>
            <ta e="T317" id="Seg_6868" s="T316">ne</ta>
            <ta e="T319" id="Seg_6869" s="T318">dĭ-zeŋ</ta>
            <ta e="T320" id="Seg_6870" s="T319">bos-tə</ta>
            <ta e="T321" id="Seg_6871" s="T320">saʔmə-luʔ-pi-ʔi</ta>
            <ta e="T322" id="Seg_6872" s="T321">a</ta>
            <ta e="T323" id="Seg_6873" s="T322">il</ta>
            <ta e="T324" id="Seg_6874" s="T323">oʔbdə-laʔbə-ʔjə</ta>
            <ta e="T328" id="Seg_6875" s="T327">dö</ta>
            <ta e="T329" id="Seg_6876" s="T328">pʼe-n</ta>
            <ta e="T330" id="Seg_6877" s="T329">len</ta>
            <ta e="T331" id="Seg_6878" s="T330">keʔbde</ta>
            <ta e="T332" id="Seg_6879" s="T331">nago-bi</ta>
            <ta e="T333" id="Seg_6880" s="T332">tože</ta>
            <ta e="T337" id="Seg_6881" s="T336">dö</ta>
            <ta e="T338" id="Seg_6882" s="T337">kö</ta>
            <ta e="T339" id="Seg_6883" s="T338">ugandə</ta>
            <ta e="T340" id="Seg_6884" s="T339">budəj</ta>
            <ta e="T341" id="Seg_6885" s="T340">jakše</ta>
            <ta e="T342" id="Seg_6886" s="T341">i-bi</ta>
            <ta e="T344" id="Seg_6887" s="T343">i</ta>
            <ta e="T345" id="Seg_6888" s="T344">toltanoʔ</ta>
            <ta e="T346" id="Seg_6889" s="T345">jakše</ta>
            <ta e="T347" id="Seg_6890" s="T346">i-bi</ta>
            <ta e="T348" id="Seg_6891" s="T347">iʔgö</ta>
            <ta e="T349" id="Seg_6892" s="T348">özer-bi</ta>
            <ta e="T350" id="Seg_6893" s="T349">urgo</ta>
            <ta e="T351" id="Seg_6894" s="T350">i-bi</ta>
            <ta e="T355" id="Seg_6895" s="T354">sĭre</ta>
            <ta e="T356" id="Seg_6896" s="T355">ipek</ta>
            <ta e="T358" id="Seg_6897" s="T357">pür-lie-ʔi</ta>
            <ta e="T359" id="Seg_6898" s="T358">il</ta>
            <ta e="T360" id="Seg_6899" s="T359">ugandə</ta>
            <ta e="T362" id="Seg_6900" s="T361">köbergən</ta>
            <ta e="T363" id="Seg_6901" s="T362">bar</ta>
            <ta e="T364" id="Seg_6902" s="T363">kü-lam-bi</ta>
            <ta e="T365" id="Seg_6903" s="T364">ăgărot-kən</ta>
            <ta e="T367" id="Seg_6904" s="T366">a</ta>
            <ta e="T371" id="Seg_6905" s="T370">a</ta>
            <ta e="T372" id="Seg_6906" s="T371">dʼije-gən</ta>
            <ta e="T373" id="Seg_6907" s="T372">köbergən</ta>
            <ta e="T374" id="Seg_6908" s="T373">jakše</ta>
            <ta e="T375" id="Seg_6909" s="T374">i-bi</ta>
            <ta e="T379" id="Seg_6910" s="T378">măn</ta>
            <ta e="T382" id="Seg_6911" s="T381">nĭŋgə-bie-m</ta>
            <ta e="T383" id="Seg_6912" s="T382">dʼagar-bia-m</ta>
            <ta e="T384" id="Seg_6913" s="T383">i</ta>
            <ta e="T385" id="Seg_6914" s="T384">koʔ-laʔ-pi</ta>
            <ta e="T387" id="Seg_6915" s="T386">a</ta>
            <ta e="T388" id="Seg_6916" s="T387">tüj</ta>
            <ta e="T389" id="Seg_6917" s="T388">mĭnzər-lie-m</ta>
            <ta e="T390" id="Seg_6918" s="T389">uja-zi</ta>
            <ta e="T391" id="Seg_6919" s="T390">toltanoʔ-si</ta>
            <ta e="T398" id="Seg_6920" s="T397">nu-zaŋ-gən</ta>
            <ta e="T399" id="Seg_6921" s="T398">oʔb</ta>
            <ta e="T400" id="Seg_6922" s="T399">kuza</ta>
            <ta e="T401" id="Seg_6923" s="T400">kü-lam-bi</ta>
            <ta e="T402" id="Seg_6924" s="T401">dĭ-zeŋ</ta>
            <ta e="T403" id="Seg_6925" s="T402">bar</ta>
            <ta e="T404" id="Seg_6926" s="T403">tĭl-bi-ʔi</ta>
            <ta e="T405" id="Seg_6927" s="T404">dʼü</ta>
            <ta e="T406" id="Seg_6928" s="T405">dĭgəttə</ta>
            <ta e="T407" id="Seg_6929" s="T406">a-bi-ʔi</ta>
            <ta e="T411" id="Seg_6930" s="T410">dĭgəttə</ta>
            <ta e="T412" id="Seg_6931" s="T411">maʔ</ta>
            <ta e="T413" id="Seg_6932" s="T412">a-bi-ʔi</ta>
            <ta e="T414" id="Seg_6933" s="T413">krospa</ta>
            <ta e="T415" id="Seg_6934" s="T414">a-bi-ʔi</ta>
            <ta e="T416" id="Seg_6935" s="T415">maʔ-tə</ta>
            <ta e="T417" id="Seg_6936" s="T416">dĭ-m</ta>
            <ta e="T418" id="Seg_6937" s="T417">em-bi-ʔi</ta>
            <ta e="T422" id="Seg_6938" s="T421">dĭ</ta>
            <ta e="T423" id="Seg_6939" s="T422">dĭʔ-nə</ta>
            <ta e="T424" id="Seg_6940" s="T423">kaŋza</ta>
            <ta e="T425" id="Seg_6941" s="T424">am-bi-ʔi</ta>
            <ta e="T426" id="Seg_6942" s="T425">taŋgu</ta>
            <ta e="T427" id="Seg_6943" s="T426">am-bi-ʔi</ta>
            <ta e="T429" id="Seg_6944" s="T428">multuk</ta>
            <ta e="T430" id="Seg_6945" s="T429">am-bi-ʔi</ta>
            <ta e="T431" id="Seg_6946" s="T430">bar</ta>
            <ta e="T433" id="Seg_6947" s="T432">aspaʔ</ta>
            <ta e="T434" id="Seg_6948" s="T433">am-bi-ʔi</ta>
            <ta e="T435" id="Seg_6949" s="T434">em-bi-ʔi</ta>
            <ta e="T437" id="Seg_6950" s="T436">dĭ</ta>
            <ta e="T438" id="Seg_6951" s="T437">dĭn</ta>
            <ta e="T439" id="Seg_6952" s="T438">mĭnzər-lə-j</ta>
            <ta e="T440" id="Seg_6953" s="T439">i</ta>
            <ta e="T441" id="Seg_6954" s="T440">amor-lə-j</ta>
            <ta e="T445" id="Seg_6955" s="T444">dĭgəttə</ta>
            <ta e="T446" id="Seg_6956" s="T445">dĭ</ta>
            <ta e="T447" id="Seg_6957" s="T446">kuza-m</ta>
            <ta e="T448" id="Seg_6958" s="T447">kum-bi-ʔi</ta>
            <ta e="T450" id="Seg_6959" s="T449">dʼü-nə</ta>
            <ta e="T451" id="Seg_6960" s="T450">em-bi-ʔi</ta>
            <ta e="T452" id="Seg_6961" s="T451">dʼü-zi</ta>
            <ta e="T453" id="Seg_6962" s="T452">bar</ta>
            <ta e="T454" id="Seg_6963" s="T453">kămnə-bi-ʔi</ta>
            <ta e="T456" id="Seg_6964" s="T455">dĭgəttə</ta>
            <ta e="T457" id="Seg_6965" s="T456">ma-ndə</ta>
            <ta e="T458" id="Seg_6966" s="T457">šo-bi-ʔi</ta>
            <ta e="T460" id="Seg_6967" s="T459">nüdʼi-n</ta>
            <ta e="T461" id="Seg_6968" s="T460">bar</ta>
            <ta e="T462" id="Seg_6969" s="T461">men</ta>
            <ta e="T463" id="Seg_6970" s="T462">sar-bi-ʔi</ta>
            <ta e="T464" id="Seg_6971" s="T463">da</ta>
            <ta e="T465" id="Seg_6972" s="T464">ine</ta>
            <ta e="T467" id="Seg_6973" s="T466">dĭgəttə</ta>
            <ta e="T468" id="Seg_6974" s="T467">kan-bi-ʔi</ta>
            <ta e="T470" id="Seg_6975" s="T469">Oj</ta>
            <ta e="T471" id="Seg_6976" s="T470">šonə-ga</ta>
            <ta e="T472" id="Seg_6977" s="T471">nuʔmə-ləʔbə-ʔjə</ta>
            <ta e="T473" id="Seg_6978" s="T472">tura-nə</ta>
            <ta e="T474" id="Seg_6979" s="T473">bar</ta>
            <ta e="T476" id="Seg_6980" s="T475">šakku</ta>
            <ta e="T477" id="Seg_6981" s="T476">aji-gən</ta>
            <ta e="T478" id="Seg_6982" s="T477">en-lə-ʔi</ta>
            <ta e="T482" id="Seg_6983" s="T481">men</ta>
            <ta e="T483" id="Seg_6984" s="T482">sar-bi-ʔi</ta>
            <ta e="T484" id="Seg_6985" s="T483">sagər</ta>
            <ta e="T485" id="Seg_6986" s="T484">i</ta>
            <ta e="T486" id="Seg_6987" s="T485">sima-t</ta>
            <ta e="T487" id="Seg_6988" s="T486">ob</ta>
            <ta e="T488" id="Seg_6989" s="T487">šide</ta>
            <ta e="T489" id="Seg_6990" s="T488">nagur</ta>
            <ta e="T490" id="Seg_6991" s="T489">teʔtə</ta>
            <ta e="T491" id="Seg_6992" s="T490">teʔtə</ta>
            <ta e="T492" id="Seg_6993" s="T491">sima-t</ta>
            <ta e="T496" id="Seg_6994" s="T495">šapku</ta>
            <ta e="T497" id="Seg_6995" s="T496">am-bi-ʔi</ta>
            <ta e="T498" id="Seg_6996" s="T497">štobɨ</ta>
            <ta e="T499" id="Seg_6997" s="T498">penze</ta>
            <ta e="T501" id="Seg_6998" s="T500">dʼagar-luʔ-pi</ta>
            <ta e="T505" id="Seg_6999" s="T504">Onʼiʔ</ta>
            <ta e="T506" id="Seg_7000" s="T505">onʼiʔ</ta>
            <ta e="T507" id="Seg_7001" s="T506">kuza</ta>
            <ta e="T509" id="Seg_7002" s="T508">miʔnʼibeʔ</ta>
            <ta e="T510" id="Seg_7003" s="T509">ĭzem-bi</ta>
            <ta e="T511" id="Seg_7004" s="T510">ĭzem-bi</ta>
            <ta e="T512" id="Seg_7005" s="T511">dĭgəttə</ta>
            <ta e="T514" id="Seg_7006" s="T513">kü-lam-bi</ta>
            <ta e="T516" id="Seg_7007" s="T515">a</ta>
            <ta e="T517" id="Seg_7008" s="T516">miʔ</ta>
            <ta e="T519" id="Seg_7009" s="T518">kan-bi-baʔ</ta>
            <ta e="T1139" id="Seg_7010" s="T519">Kazan</ta>
            <ta e="T520" id="Seg_7011" s="T1139">tura-nə</ta>
            <ta e="T522" id="Seg_7012" s="T521">sazən</ta>
            <ta e="T523" id="Seg_7013" s="T522">uda-ndə</ta>
            <ta e="T525" id="Seg_7014" s="T524">uda-ndə</ta>
            <ta e="T526" id="Seg_7015" s="T525">i-zittə</ta>
            <ta e="T527" id="Seg_7016" s="T526">i</ta>
            <ta e="T528" id="Seg_7017" s="T527">ulu-ndə</ta>
            <ta e="T530" id="Seg_7018" s="T529">dĭgəttə</ta>
            <ta e="T531" id="Seg_7019" s="T530">abəs</ta>
            <ta e="T532" id="Seg_7020" s="T531">miʔnʼibeʔ</ta>
            <ta e="T533" id="Seg_7021" s="T532">ej</ta>
            <ta e="T534" id="Seg_7022" s="T533">mĭ-bi</ta>
            <ta e="T536" id="Seg_7023" s="T535">miʔ</ta>
            <ta e="T537" id="Seg_7024" s="T536">ara</ta>
            <ta e="T538" id="Seg_7025" s="T537">i-bi-beʔ</ta>
            <ta e="T540" id="Seg_7026" s="T539">i</ta>
            <ta e="T541" id="Seg_7027" s="T540">maʔ-nʼi</ta>
            <ta e="T542" id="Seg_7028" s="T541">šo-bi-baʔ</ta>
            <ta e="T544" id="Seg_7029" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7030" s="T544">ia-m</ta>
            <ta e="T546" id="Seg_7031" s="T545">aba-m</ta>
            <ta e="T548" id="Seg_7032" s="T547">i-bi-ʔi</ta>
            <ta e="T549" id="Seg_7033" s="T548">dĭ</ta>
            <ta e="T550" id="Seg_7034" s="T549">ara</ta>
            <ta e="T1137" id="Seg_7035" s="T550">kal-la</ta>
            <ta e="T551" id="Seg_7036" s="T1137">dʼür-bi-ʔi</ta>
            <ta e="T552" id="Seg_7037" s="T551">bĭʔ-sittə</ta>
            <ta e="T554" id="Seg_7038" s="T553">a</ta>
            <ta e="T555" id="Seg_7039" s="T554">miʔ</ta>
            <ta e="T556" id="Seg_7040" s="T555">ešse-ŋ</ta>
            <ta e="T557" id="Seg_7041" s="T556">miʔ</ta>
            <ta e="T558" id="Seg_7042" s="T557">tože</ta>
            <ta e="T559" id="Seg_7043" s="T558">ej</ta>
            <ta e="T560" id="Seg_7044" s="T559">ma-lə-m</ta>
            <ta e="T561" id="Seg_7045" s="T560">maʔ-nə</ta>
            <ta e="T564" id="Seg_7046" s="T563">kan-že-bəj</ta>
            <ta e="T566" id="Seg_7047" s="T565">a</ta>
            <ta e="T567" id="Seg_7048" s="T566">măn</ta>
            <ta e="T568" id="Seg_7049" s="T567">măm-bia-m</ta>
            <ta e="T569" id="Seg_7050" s="T568">iʔbə-ʔ</ta>
            <ta e="T570" id="Seg_7051" s="T569">kunol-a-ʔ</ta>
            <ta e="T572" id="Seg_7052" s="T571">a</ta>
            <ta e="T573" id="Seg_7053" s="T572">măn</ta>
            <ta e="T574" id="Seg_7054" s="T573">iʔbə-lə-m</ta>
            <ta e="T575" id="Seg_7055" s="T574">šiʔnʼileʔ</ta>
            <ta e="T577" id="Seg_7056" s="T576">kamən</ta>
            <ta e="T578" id="Seg_7057" s="T577">măna</ta>
            <ta e="T579" id="Seg_7058" s="T578">kabarləj</ta>
            <ta e="T580" id="Seg_7059" s="T579">a</ta>
            <ta e="T581" id="Seg_7060" s="T580">šiʔ</ta>
            <ta e="T582" id="Seg_7061" s="T581">uʔme-luʔpi-leʔ</ta>
            <ta e="T584" id="Seg_7062" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_7063" s="T584">erte-n</ta>
            <ta e="T586" id="Seg_7064" s="T585">uʔbdə-bia-m</ta>
            <ta e="T588" id="Seg_7065" s="T587">ot</ta>
            <ta e="T589" id="Seg_7066" s="T588">ku-io-l</ta>
            <ta e="T590" id="Seg_7067" s="T589">dĭ</ta>
            <ta e="T591" id="Seg_7068" s="T590">iʔbə-laʔbə</ta>
            <ta e="T592" id="Seg_7069" s="T591">i</ta>
            <ta e="T594" id="Seg_7070" s="T593">i</ta>
            <ta e="T595" id="Seg_7071" s="T594">miʔ</ta>
            <ta e="T596" id="Seg_7072" s="T595">kunol-bi-baʔ</ta>
            <ta e="T600" id="Seg_7073" s="T599">dĭgəttə</ta>
            <ta e="T601" id="Seg_7074" s="T600">dĭ</ta>
            <ta e="T602" id="Seg_7075" s="T601">kü-lam-bi</ta>
            <ta e="T603" id="Seg_7076" s="T602">dĭ-m</ta>
            <ta e="T604" id="Seg_7077" s="T603">băzə-bi-ʔi</ta>
            <ta e="T606" id="Seg_7078" s="T605">kujnek</ta>
            <ta e="T607" id="Seg_7079" s="T606">šer-bi-ʔi</ta>
            <ta e="T608" id="Seg_7080" s="T607">piʔme</ta>
            <ta e="T609" id="Seg_7081" s="T608">šer-bi-ʔi</ta>
            <ta e="T611" id="Seg_7082" s="T610">jama-ʔi</ta>
            <ta e="T612" id="Seg_7083" s="T611">šoʔ-pi-ʔi</ta>
            <ta e="T613" id="Seg_7084" s="T612">šer-bi-ʔi</ta>
            <ta e="T615" id="Seg_7085" s="T614">dĭgəttə</ta>
            <ta e="T616" id="Seg_7086" s="T615">kom-bi-ʔi</ta>
            <ta e="T619" id="Seg_7087" s="T618">d'ü-zi</ta>
            <ta e="T620" id="Seg_7088" s="T619">kăm-bi-ʔi</ta>
            <ta e="T621" id="Seg_7089" s="T620">dĭ-m</ta>
            <ta e="T623" id="Seg_7090" s="T622">šo-bi-ʔi</ta>
            <ta e="T624" id="Seg_7091" s="T623">maʔ-ndə</ta>
            <ta e="T626" id="Seg_7092" s="T625">ara</ta>
            <ta e="T627" id="Seg_7093" s="T626">biʔ-pi-ʔi</ta>
            <ta e="T628" id="Seg_7094" s="T627">ipek</ta>
            <ta e="T629" id="Seg_7095" s="T628">amor-bi-ʔi</ta>
            <ta e="T630" id="Seg_7096" s="T629">uja</ta>
            <ta e="T631" id="Seg_7097" s="T630">am-bi-ʔi</ta>
            <ta e="T633" id="Seg_7098" s="T632">i</ta>
            <ta e="T634" id="Seg_7099" s="T633">maʔ-ndə</ta>
            <ta e="T1138" id="Seg_7100" s="T634">kal-la</ta>
            <ta e="T635" id="Seg_7101" s="T1138">dʼür-bi-ʔi</ta>
            <ta e="T639" id="Seg_7102" s="T638">măn</ta>
            <ta e="T640" id="Seg_7103" s="T639">onʼiʔ</ta>
            <ta e="T641" id="Seg_7104" s="T640">raz</ta>
            <ta e="T642" id="Seg_7105" s="T641">šo</ta>
            <ta e="T643" id="Seg_7106" s="T642">šo-bia-m</ta>
            <ta e="T1140" id="Seg_7107" s="T643">Kazan</ta>
            <ta e="T644" id="Seg_7108" s="T1140">tura-gən</ta>
            <ta e="T646" id="Seg_7109" s="T645">šo-bia-m</ta>
            <ta e="T647" id="Seg_7110" s="T646">Permʼakovă-n</ta>
            <ta e="T648" id="Seg_7111" s="T647">dĭgəttə</ta>
            <ta e="T649" id="Seg_7112" s="T648">maʔ-ndə</ta>
            <ta e="T650" id="Seg_7113" s="T649">šo-bia-m</ta>
            <ta e="T652" id="Seg_7114" s="T651">tʼermən-gən</ta>
            <ta e="T653" id="Seg_7115" s="T652">dĭn</ta>
            <ta e="T654" id="Seg_7116" s="T653">ildə-m</ta>
            <ta e="T655" id="Seg_7117" s="T654">šindi=də</ta>
            <ta e="T657" id="Seg_7118" s="T656">per-luʔ-pi-ʔi</ta>
            <ta e="T659" id="Seg_7119" s="T658">a</ta>
            <ta e="T660" id="Seg_7120" s="T659">măn</ta>
            <ta e="T661" id="Seg_7121" s="T660">šo-bia-m</ta>
            <ta e="T662" id="Seg_7122" s="T661">da</ta>
            <ta e="T664" id="Seg_7123" s="T663">dĭgəttə</ta>
            <ta e="T665" id="Seg_7124" s="T664">toʔnar-lə-m</ta>
            <ta e="T666" id="Seg_7125" s="T665">bar</ta>
            <ta e="T667" id="Seg_7126" s="T666">aktʼa</ta>
            <ta e="T668" id="Seg_7127" s="T667">iʔgö</ta>
            <ta e="T669" id="Seg_7128" s="T668">mo-lə-j</ta>
            <ta e="T673" id="Seg_7129" s="T672">šo-bia-m</ta>
            <ta e="T675" id="Seg_7130" s="T674">maʔ-ndə</ta>
            <ta e="T676" id="Seg_7131" s="T675">šindi-ndə</ta>
            <ta e="T677" id="Seg_7132" s="T676">ej</ta>
            <ta e="T678" id="Seg_7133" s="T677">ku-bio-m</ta>
            <ta e="T682" id="Seg_7134" s="T681">măn</ta>
            <ta e="T683" id="Seg_7135" s="T682">ulu-m</ta>
            <ta e="T684" id="Seg_7136" s="T683">ĭzem-nie</ta>
            <ta e="T685" id="Seg_7137" s="T684">sima-m</ta>
            <ta e="T686" id="Seg_7138" s="T685">ĭzem-nie</ta>
            <ta e="T687" id="Seg_7139" s="T686">a</ta>
            <ta e="T688" id="Seg_7140" s="T687">tăn</ta>
            <ta e="T689" id="Seg_7141" s="T688">ĭmbi</ta>
            <ta e="T690" id="Seg_7142" s="T689">ĭzem-nie</ta>
            <ta e="T691" id="Seg_7143" s="T690">a</ta>
            <ta e="T692" id="Seg_7144" s="T691">măn</ta>
            <ta e="T693" id="Seg_7145" s="T692">tĭme-m</ta>
            <ta e="T694" id="Seg_7146" s="T693">ĭzem-bie</ta>
            <ta e="T695" id="Seg_7147" s="T694">ĭzem-neʔbə</ta>
            <ta e="T696" id="Seg_7148" s="T695">i</ta>
            <ta e="T697" id="Seg_7149" s="T696">pĭje-m</ta>
            <ta e="T698" id="Seg_7150" s="T697">ĭzem-neʔbə</ta>
            <ta e="T702" id="Seg_7151" s="T701">Ivanovič-ə-n</ta>
            <ta e="T703" id="Seg_7152" s="T702">bar</ta>
            <ta e="T704" id="Seg_7153" s="T703">šüjo-t</ta>
            <ta e="T705" id="Seg_7154" s="T704">ĭzem-nie</ta>
            <ta e="T706" id="Seg_7155" s="T705">i</ta>
            <ta e="T707" id="Seg_7156" s="T706">uju-t</ta>
            <ta e="T708" id="Seg_7157" s="T707">ĭzem-nie</ta>
            <ta e="T709" id="Seg_7158" s="T708">i</ta>
            <ta e="T711" id="Seg_7159" s="T710">uda-zaŋ-də</ta>
            <ta e="T712" id="Seg_7160" s="T711">ĭzem-neʔpə-ʔjə</ta>
            <ta e="T713" id="Seg_7161" s="T712">bar</ta>
            <ta e="T714" id="Seg_7162" s="T713">tăŋ</ta>
            <ta e="T715" id="Seg_7163" s="T714">ĭzem-nie</ta>
            <ta e="T719" id="Seg_7164" s="T718">nada</ta>
            <ta e="T722" id="Seg_7165" s="T721">dʼazir-zittə</ta>
            <ta e="T723" id="Seg_7166" s="T722">dĭ</ta>
            <ta e="T725" id="Seg_7167" s="T724">nagur</ta>
            <ta e="T726" id="Seg_7168" s="T725">kuza</ta>
            <ta e="T728" id="Seg_7169" s="T727">doxtăr</ta>
            <ta e="T729" id="Seg_7170" s="T728">nada</ta>
            <ta e="T730" id="Seg_7171" s="T729">deʔ-sittə</ta>
            <ta e="T731" id="Seg_7172" s="T730">pušaj</ta>
            <ta e="T732" id="Seg_7173" s="T731">dʼazir-lə-j</ta>
            <ta e="T733" id="Seg_7174" s="T732">dĭ</ta>
            <ta e="T734" id="Seg_7175" s="T733">dĭ-zem</ta>
            <ta e="T738" id="Seg_7176" s="T737">ĭmbi=nʼibudʼ</ta>
            <ta e="T739" id="Seg_7177" s="T738">mĭ-lə-j</ta>
            <ta e="T740" id="Seg_7178" s="T739">bĭʔ-sittə</ta>
            <ta e="T741" id="Seg_7179" s="T740">ali</ta>
            <ta e="T742" id="Seg_7180" s="T741">ĭmbi=nʼibudʼ</ta>
            <ta e="T743" id="Seg_7181" s="T742">mĭ-lə-j</ta>
            <ta e="T744" id="Seg_7182" s="T743">kĭškə-sʼtə</ta>
            <ta e="T748" id="Seg_7183" s="T747">dĭ-zeŋ</ta>
            <ta e="T749" id="Seg_7184" s="T748">bar</ta>
            <ta e="T750" id="Seg_7185" s="T749">tăŋ</ta>
            <ta e="T751" id="Seg_7186" s="T750">kănna-m-bi-ʔi</ta>
            <ta e="T752" id="Seg_7187" s="T751">a</ta>
            <ta e="T753" id="Seg_7188" s="T752">tuj</ta>
            <ta e="T754" id="Seg_7189" s="T753">ugandə</ta>
            <ta e="T755" id="Seg_7190" s="T754">tăŋ</ta>
            <ta e="T756" id="Seg_7191" s="T755">ĭzem-neʔbə-ʔjə</ta>
            <ta e="T760" id="Seg_7192" s="T759">Marejka</ta>
            <ta e="T761" id="Seg_7193" s="T760">urgaja-nə</ta>
            <ta e="T762" id="Seg_7194" s="T761">kadəl-də</ta>
            <ta e="T764" id="Seg_7195" s="T763">urgaja-nə</ta>
            <ta e="T765" id="Seg_7196" s="T764">dĭrgit</ta>
            <ta e="T766" id="Seg_7197" s="T765">kadəl-də</ta>
            <ta e="T767" id="Seg_7198" s="T766">i</ta>
            <ta e="T768" id="Seg_7199" s="T767">sima-t</ta>
            <ta e="T769" id="Seg_7200" s="T768">dĭrgit</ta>
            <ta e="T770" id="Seg_7201" s="T769">urgaja-gəndə</ta>
            <ta e="T774" id="Seg_7202" s="T773">amno-bi-ʔi</ta>
            <ta e="T775" id="Seg_7203" s="T774">nüke</ta>
            <ta e="T776" id="Seg_7204" s="T775">i</ta>
            <ta e="T777" id="Seg_7205" s="T776">büzʼe</ta>
            <ta e="T779" id="Seg_7206" s="T778">samən</ta>
            <ta e="T780" id="Seg_7207" s="T779">bü-gən</ta>
            <ta e="T781" id="Seg_7208" s="T780">toʔ-ndə</ta>
            <ta e="T783" id="Seg_7209" s="T782">nüke</ta>
            <ta e="T784" id="Seg_7210" s="T783">erer-la</ta>
            <ta e="T785" id="Seg_7211" s="T784">amno-laʔ-pi</ta>
            <ta e="T787" id="Seg_7212" s="T786">a</ta>
            <ta e="T788" id="Seg_7213" s="T787">büzʼe</ta>
            <ta e="T789" id="Seg_7214" s="T788">mĭm-bi</ta>
            <ta e="T790" id="Seg_7215" s="T789">bü-nə</ta>
            <ta e="T791" id="Seg_7216" s="T790">kola</ta>
            <ta e="T793" id="Seg_7217" s="T792">dʼabə-sʼtə</ta>
            <ta e="T795" id="Seg_7218" s="T794">Onʼiʔ</ta>
            <ta e="T796" id="Seg_7219" s="T795">raz</ta>
            <ta e="T797" id="Seg_7220" s="T796">onʼiʔ</ta>
            <ta e="T798" id="Seg_7221" s="T797">raz</ta>
            <ta e="T799" id="Seg_7222" s="T798">kam-bi</ta>
            <ta e="T800" id="Seg_7223" s="T799">dĭgəttə</ta>
            <ta e="T801" id="Seg_7224" s="T800">ĭmbi=də</ta>
            <ta e="T802" id="Seg_7225" s="T801">ej</ta>
            <ta e="T803" id="Seg_7226" s="T802">dʼaʔ-pi</ta>
            <ta e="T804" id="Seg_7227" s="T803">onʼiʔ</ta>
            <ta e="T805" id="Seg_7228" s="T804">riba</ta>
            <ta e="T806" id="Seg_7229" s="T805">dʼaʔ-pi</ta>
            <ta e="T807" id="Seg_7230" s="T806">kuvas</ta>
            <ta e="T809" id="Seg_7231" s="T808">măn</ta>
            <ta e="T810" id="Seg_7232" s="T809">pʼe-leʔbə</ta>
            <ta e="T814" id="Seg_7233" s="T813">kola</ta>
            <ta e="T815" id="Seg_7234" s="T814">kuvas</ta>
            <ta e="T816" id="Seg_7235" s="T815">dʼăbaktər-lia</ta>
            <ta e="T817" id="Seg_7236" s="T816">kuza-nə</ta>
            <ta e="T819" id="Seg_7237" s="T818">šike-t-si</ta>
            <ta e="T821" id="Seg_7238" s="T820">öʔ-leʔ</ta>
            <ta e="T822" id="Seg_7239" s="T821">măna</ta>
            <ta e="T823" id="Seg_7240" s="T822">bü-nə</ta>
            <ta e="T825" id="Seg_7241" s="T824">măn</ta>
            <ta e="T826" id="Seg_7242" s="T825">öʔluʔ-pia-m</ta>
            <ta e="T828" id="Seg_7243" s="T827">šo-bi</ta>
            <ta e="T829" id="Seg_7244" s="T828">maʔ-ndə</ta>
            <ta e="T830" id="Seg_7245" s="T829">da</ta>
            <ta e="T831" id="Seg_7246" s="T830">nüke-nə</ta>
            <ta e="T832" id="Seg_7247" s="T831">dʼăbaktər-labə</ta>
            <ta e="T834" id="Seg_7248" s="T833">kola</ta>
            <ta e="T835" id="Seg_7249" s="T834">bar</ta>
            <ta e="T836" id="Seg_7250" s="T835">kuvas</ta>
            <ta e="T837" id="Seg_7251" s="T836">i-bi</ta>
            <ta e="T838" id="Seg_7252" s="T837">dʼăbaktər-bi</ta>
            <ta e="T839" id="Seg_7253" s="T838">kak</ta>
            <ta e="T840" id="Seg_7254" s="T839">kuza</ta>
            <ta e="T844" id="Seg_7255" s="T843">dĭgəttə</ta>
            <ta e="T845" id="Seg_7256" s="T844">dĭ</ta>
            <ta e="T846" id="Seg_7257" s="T845">nüke</ta>
            <ta e="T847" id="Seg_7258" s="T846">davaj</ta>
            <ta e="T848" id="Seg_7259" s="T847">kudon-zittə</ta>
            <ta e="T850" id="Seg_7260" s="T849">ĭmbi</ta>
            <ta e="T851" id="Seg_7261" s="T850">tăn</ta>
            <ta e="T852" id="Seg_7262" s="T851">ĭmbi=də</ta>
            <ta e="T853" id="Seg_7263" s="T852">ej</ta>
            <ta e="T854" id="Seg_7264" s="T853">pʼe-bie-l</ta>
            <ta e="T856" id="Seg_7265" s="T855">kărɨtă</ta>
            <ta e="T857" id="Seg_7266" s="T856">miʔnʼibeʔ</ta>
            <ta e="T858" id="Seg_7267" s="T857">naga</ta>
            <ta e="T860" id="Seg_7268" s="T859">ši-žəbi</ta>
            <ta e="T862" id="Seg_7269" s="T861">kărĭtă</ta>
            <ta e="T864" id="Seg_7270" s="T863">dĭ</ta>
            <ta e="T865" id="Seg_7271" s="T864">kam-bi</ta>
            <ta e="T866" id="Seg_7272" s="T865">bazo</ta>
            <ta e="T867" id="Seg_7273" s="T866">bü-nə</ta>
            <ta e="T869" id="Seg_7274" s="T868">dĭgəttə</ta>
            <ta e="T870" id="Seg_7275" s="T869">davaj</ta>
            <ta e="T871" id="Seg_7276" s="T870">kirgar-zittə</ta>
            <ta e="T872" id="Seg_7277" s="T871">kola</ta>
            <ta e="T873" id="Seg_7278" s="T872">šo-ʔ</ta>
            <ta e="T874" id="Seg_7279" s="T873">döber</ta>
            <ta e="T875" id="Seg_7280" s="T874">dĭ</ta>
            <ta e="T876" id="Seg_7281" s="T875">šo-bi</ta>
            <ta e="T877" id="Seg_7282" s="T876">ĭmbi</ta>
            <ta e="T878" id="Seg_7283" s="T877">tănan</ta>
            <ta e="T879" id="Seg_7284" s="T878">büzʼe</ta>
            <ta e="T880" id="Seg_7285" s="T879">kereʔ</ta>
            <ta e="T882" id="Seg_7286" s="T881">măna</ta>
            <ta e="T883" id="Seg_7287" s="T882">kereʔ</ta>
            <ta e="T884" id="Seg_7288" s="T883">kărɨtă</ta>
            <ta e="T886" id="Seg_7289" s="T885">nüke-m</ta>
            <ta e="T887" id="Seg_7290" s="T886">bar</ta>
            <ta e="T888" id="Seg_7291" s="T887">kudo-nz-laʔbə</ta>
            <ta e="T889" id="Seg_7292" s="T888">nu</ta>
            <ta e="T890" id="Seg_7293" s="T889">kan-a-ʔ</ta>
            <ta e="T891" id="Seg_7294" s="T890">ma-nə</ta>
            <ta e="T895" id="Seg_7295" s="T894">dĭ</ta>
            <ta e="T896" id="Seg_7296" s="T895">nüke</ta>
            <ta e="T897" id="Seg_7297" s="T896">davaj</ta>
            <ta e="T898" id="Seg_7298" s="T897">dĭ-m</ta>
            <ta e="T899" id="Seg_7299" s="T898">kudon-zittə</ta>
            <ta e="T900" id="Seg_7300" s="T899">ĭmbi</ta>
            <ta e="T901" id="Seg_7301" s="T900">ej</ta>
            <ta e="T902" id="Seg_7302" s="T901">pʼe-bie-l</ta>
            <ta e="T903" id="Seg_7303" s="T902">tura</ta>
            <ta e="T905" id="Seg_7304" s="T904">miʔ</ta>
            <ta e="T906" id="Seg_7305" s="T905">tura</ta>
            <ta e="T907" id="Seg_7306" s="T906">ej</ta>
            <ta e="T908" id="Seg_7307" s="T907">kuvas</ta>
            <ta e="T910" id="Seg_7308" s="T909">dĭ</ta>
            <ta e="T911" id="Seg_7309" s="T910">bazo</ta>
            <ta e="T912" id="Seg_7310" s="T911">kam-bi</ta>
            <ta e="T913" id="Seg_7311" s="T912">davaj</ta>
            <ta e="T914" id="Seg_7312" s="T913">kirgar-zittə</ta>
            <ta e="T915" id="Seg_7313" s="T914">kola</ta>
            <ta e="T916" id="Seg_7314" s="T915">kola</ta>
            <ta e="T917" id="Seg_7315" s="T916">šo-ʔ</ta>
            <ta e="T918" id="Seg_7316" s="T917">döber</ta>
            <ta e="T920" id="Seg_7317" s="T919">kola</ta>
            <ta e="T921" id="Seg_7318" s="T920">šo-bi</ta>
            <ta e="T922" id="Seg_7319" s="T921">ĭmbi</ta>
            <ta e="T923" id="Seg_7320" s="T922">tănan</ta>
            <ta e="T924" id="Seg_7321" s="T923">kereʔ</ta>
            <ta e="T926" id="Seg_7322" s="T925">tura</ta>
            <ta e="T927" id="Seg_7323" s="T926">kereʔ</ta>
            <ta e="T929" id="Seg_7324" s="T928">miʔ</ta>
            <ta e="T930" id="Seg_7325" s="T929">tura-baʔ</ta>
            <ta e="T931" id="Seg_7326" s="T930">ej</ta>
            <ta e="T932" id="Seg_7327" s="T931">kuvas</ta>
            <ta e="T934" id="Seg_7328" s="T933">kan-a-ʔ</ta>
            <ta e="T935" id="Seg_7329" s="T934">maʔ-nə-l</ta>
            <ta e="T939" id="Seg_7330" s="T938">dĭgəttə</ta>
            <ta e="T940" id="Seg_7331" s="T939">bazo</ta>
            <ta e="T941" id="Seg_7332" s="T940">dĭ</ta>
            <ta e="T942" id="Seg_7333" s="T941">nüke</ta>
            <ta e="T944" id="Seg_7334" s="T942">kudo-nzə-bi</ta>
            <ta e="T945" id="Seg_7335" s="T944">kan-a-ʔ</ta>
            <ta e="T946" id="Seg_7336" s="T945">kola-nə</ta>
            <ta e="T947" id="Seg_7337" s="T946">pʼe-ʔ</ta>
            <ta e="T948" id="Seg_7338" s="T947">štobɨ</ta>
            <ta e="T949" id="Seg_7339" s="T948">măn</ta>
            <ta e="T950" id="Seg_7340" s="T949">mo-bia-m</ta>
            <ta e="T951" id="Seg_7341" s="T950">kupčixăj-zi</ta>
            <ta e="T955" id="Seg_7342" s="T954">dĭgəttə</ta>
            <ta e="T956" id="Seg_7343" s="T955">kola</ta>
            <ta e="T957" id="Seg_7344" s="T956">măm-bi</ta>
            <ta e="T958" id="Seg_7345" s="T957">kan-a-ʔ</ta>
            <ta e="T959" id="Seg_7346" s="T958">maːʔ-nə-l</ta>
            <ta e="T961" id="Seg_7347" s="T960">dĭ</ta>
            <ta e="T962" id="Seg_7348" s="T961">šö-bi</ta>
            <ta e="T963" id="Seg_7349" s="T962">a</ta>
            <ta e="T964" id="Seg_7350" s="T963">dĭ</ta>
            <ta e="T965" id="Seg_7351" s="T964">nüke</ta>
            <ta e="T966" id="Seg_7352" s="T965">bazo</ta>
            <ta e="T968" id="Seg_7353" s="T966">kudo-nz-laʔbə</ta>
            <ta e="T969" id="Seg_7354" s="T968">kan-a-ʔ</ta>
            <ta e="T970" id="Seg_7355" s="T969">kola-nə</ta>
            <ta e="T971" id="Seg_7356" s="T970">pe-ʔ</ta>
            <ta e="T972" id="Seg_7357" s="T971">štobɨ</ta>
            <ta e="T973" id="Seg_7358" s="T972">măn</ta>
            <ta e="T974" id="Seg_7359" s="T973">i-bie-m</ta>
            <ta e="T975" id="Seg_7360" s="T974">dvărʼankaj-zi</ta>
            <ta e="T977" id="Seg_7361" s="T976">dĭ</ta>
            <ta e="T978" id="Seg_7362" s="T977">kam-bi</ta>
            <ta e="T979" id="Seg_7363" s="T978">kola-nə</ta>
            <ta e="T980" id="Seg_7364" s="T979">kola</ta>
            <ta e="T981" id="Seg_7365" s="T980">kirgar-laʔpi</ta>
            <ta e="T983" id="Seg_7366" s="T982">kola</ta>
            <ta e="T984" id="Seg_7367" s="T983">šo-bi</ta>
            <ta e="T985" id="Seg_7368" s="T984">ĭmbi</ta>
            <ta e="T986" id="Seg_7369" s="T985">tănan</ta>
            <ta e="T987" id="Seg_7370" s="T986">kereʔ</ta>
            <ta e="T989" id="Seg_7371" s="T988">măn</ta>
            <ta e="T990" id="Seg_7372" s="T989">nüke-m</ta>
            <ta e="T991" id="Seg_7373" s="T990">măn-de</ta>
            <ta e="T992" id="Seg_7374" s="T991">štobɨ</ta>
            <ta e="T993" id="Seg_7375" s="T992">măn</ta>
            <ta e="T994" id="Seg_7376" s="T993">dvărʼankăj-zi</ta>
            <ta e="T995" id="Seg_7377" s="T994">i-bie-m</ta>
            <ta e="T997" id="Seg_7378" s="T996">kan-a-ʔ</ta>
            <ta e="T998" id="Seg_7379" s="T997">maːʔ-nə-l</ta>
            <ta e="T1002" id="Seg_7380" s="T1001">dĭgəttə</ta>
            <ta e="T1003" id="Seg_7381" s="T1002">büzʼe</ta>
            <ta e="T1004" id="Seg_7382" s="T1003">šo-bi</ta>
            <ta e="T1005" id="Seg_7383" s="T1004">maːʔ-ndə</ta>
            <ta e="T1006" id="Seg_7384" s="T1005">dĭ</ta>
            <ta e="T1007" id="Seg_7385" s="T1006">davaj</ta>
            <ta e="T1008" id="Seg_7386" s="T1007">bazo</ta>
            <ta e="T1009" id="Seg_7387" s="T1008">kudon-zittə</ta>
            <ta e="T1013" id="Seg_7388" s="T1012">i</ta>
            <ta e="T1014" id="Seg_7389" s="T1013">münör-zittə</ta>
            <ta e="T1015" id="Seg_7390" s="T1014">xat'el</ta>
            <ta e="T1017" id="Seg_7391" s="T1016">kan-a-ʔ</ta>
            <ta e="T1018" id="Seg_7392" s="T1017">bazo</ta>
            <ta e="T1019" id="Seg_7393" s="T1018">kola-nə</ta>
            <ta e="T1020" id="Seg_7394" s="T1019">pe-ʔ</ta>
            <ta e="T1021" id="Seg_7395" s="T1020">štobɨ</ta>
            <ta e="T1022" id="Seg_7396" s="T1021">măn</ta>
            <ta e="T1027" id="Seg_7397" s="T1026">štobɨ</ta>
            <ta e="T1028" id="Seg_7398" s="T1027">jil</ta>
            <ta e="T1029" id="Seg_7399" s="T1028">măna</ta>
            <ta e="T1030" id="Seg_7400" s="T1029">uda-zaŋ-də</ta>
            <ta e="T1035" id="Seg_7401" s="T1034">dĭgəttə</ta>
            <ta e="T1036" id="Seg_7402" s="T1035">dĭ</ta>
            <ta e="T1037" id="Seg_7403" s="T1036">šo-bi</ta>
            <ta e="T1038" id="Seg_7404" s="T1037">bü-nə</ta>
            <ta e="T1040" id="Seg_7405" s="T1039">kola</ta>
            <ta e="T1041" id="Seg_7406" s="T1040">kola</ta>
            <ta e="T1042" id="Seg_7407" s="T1041">šo-ʔ</ta>
            <ta e="T1043" id="Seg_7408" s="T1042">döber</ta>
            <ta e="T1044" id="Seg_7409" s="T1043">kola</ta>
            <ta e="T1045" id="Seg_7410" s="T1044">šo-bi</ta>
            <ta e="T1047" id="Seg_7411" s="T1046">ĭmbi</ta>
            <ta e="T1048" id="Seg_7412" s="T1047">tănan</ta>
            <ta e="T1049" id="Seg_7413" s="T1048">kereʔ</ta>
            <ta e="T1050" id="Seg_7414" s="T1049">büzʼe</ta>
            <ta e="T1051" id="Seg_7415" s="T1050">da</ta>
            <ta e="T1052" id="Seg_7416" s="T1051">măn</ta>
            <ta e="T1053" id="Seg_7417" s="T1052">nüke-m</ta>
            <ta e="T1055" id="Seg_7418" s="T1054">măn-də</ta>
            <ta e="T1056" id="Seg_7419" s="T1055">štobɨ</ta>
            <ta e="T1057" id="Seg_7420" s="T1056">măn</ta>
            <ta e="T1059" id="Seg_7421" s="T1058">štobɨ</ta>
            <ta e="T1060" id="Seg_7422" s="T1059">măn</ta>
            <ta e="T1061" id="Seg_7423" s="T1060">i-bie-m</ta>
            <ta e="T1062" id="Seg_7424" s="T1061">tsaritsaj-zʼi</ta>
            <ta e="T1064" id="Seg_7425" s="T1063">štobɨ</ta>
            <ta e="T1065" id="Seg_7426" s="T1064">măna</ta>
            <ta e="T1066" id="Seg_7427" s="T1065">il</ta>
            <ta e="T1067" id="Seg_7428" s="T1066">uda-t-si</ta>
            <ta e="T1068" id="Seg_7429" s="T1067">kun-nam-bi-ʔi</ta>
            <ta e="T1070" id="Seg_7430" s="T1069">kan-a-ʔ</ta>
            <ta e="T1071" id="Seg_7431" s="T1070">maːʔ-nə-l</ta>
            <ta e="T1072" id="Seg_7432" s="T1071">bar</ta>
            <ta e="T1073" id="Seg_7433" s="T1072">mo-lə-j</ta>
            <ta e="T1074" id="Seg_7434" s="T1073">tănan</ta>
            <ta e="T1078" id="Seg_7435" s="T1077">dĭgəttə</ta>
            <ta e="T1079" id="Seg_7436" s="T1078">büzʼe</ta>
            <ta e="T1080" id="Seg_7437" s="T1079">kam-bi</ta>
            <ta e="T1081" id="Seg_7438" s="T1080">bü-nə</ta>
            <ta e="T1083" id="Seg_7439" s="T1082">kola-ʔ</ta>
            <ta e="T1084" id="Seg_7440" s="T1083">kola-ʔ</ta>
            <ta e="T1085" id="Seg_7441" s="T1084">šo-ʔ</ta>
            <ta e="T1086" id="Seg_7442" s="T1085">döber</ta>
            <ta e="T1087" id="Seg_7443" s="T1086">kola</ta>
            <ta e="T1088" id="Seg_7444" s="T1087">šo-bi</ta>
            <ta e="T1089" id="Seg_7445" s="T1088">ĭmbi</ta>
            <ta e="T1090" id="Seg_7446" s="T1089">tănan</ta>
            <ta e="T1091" id="Seg_7447" s="T1090">kereʔ</ta>
            <ta e="T1093" id="Seg_7448" s="T1092">da</ta>
            <ta e="T1094" id="Seg_7449" s="T1093">măn</ta>
            <ta e="T1095" id="Seg_7450" s="T1094">nüke-m</ta>
            <ta e="T1096" id="Seg_7451" s="T1095">măn-də</ta>
            <ta e="T1097" id="Seg_7452" s="T1096">štobɨ</ta>
            <ta e="T1098" id="Seg_7453" s="T1097">măn</ta>
            <ta e="T1099" id="Seg_7454" s="T1098">i-bie-m</ta>
            <ta e="T1103" id="Seg_7455" s="T1102">i</ta>
            <ta e="T1104" id="Seg_7456" s="T1103">kola</ta>
            <ta e="T1105" id="Seg_7457" s="T1104">štobɨ</ta>
            <ta e="T1106" id="Seg_7458" s="T1105">măna</ta>
            <ta e="T1107" id="Seg_7459" s="T1106">tažer-bi</ta>
            <ta e="T1108" id="Seg_7460" s="T1107">bar</ta>
            <ta e="T1109" id="Seg_7461" s="T1108">ĭmbi</ta>
            <ta e="T1111" id="Seg_7462" s="T1110">dĭgəttə</ta>
            <ta e="T1112" id="Seg_7463" s="T1111">kola</ta>
            <ta e="T1113" id="Seg_7464" s="T1112">kam-bi</ta>
            <ta e="T1114" id="Seg_7465" s="T1113">bü-nə</ta>
            <ta e="T1115" id="Seg_7466" s="T1114">ĭmbi</ta>
            <ta e="T1117" id="Seg_7467" s="T1116">ĭmbi=də</ta>
            <ta e="T1119" id="Seg_7468" s="T1118">büzʼe-nə</ta>
            <ta e="T1120" id="Seg_7469" s="T1119">ej</ta>
            <ta e="T1121" id="Seg_7470" s="T1120">nörbə-bi</ta>
            <ta e="T1123" id="Seg_7471" s="T1122">dĭ</ta>
            <ta e="T1124" id="Seg_7472" s="T1123">šo-bi</ta>
            <ta e="T1125" id="Seg_7473" s="T1124">maːʔ-ndə</ta>
            <ta e="T1127" id="Seg_7474" s="T1126">nüke</ta>
            <ta e="T1128" id="Seg_7475" s="T1127">amno-laʔbə</ta>
            <ta e="T1129" id="Seg_7476" s="T1128">jerer-laʔbə</ta>
            <ta e="T1130" id="Seg_7477" s="T1129">i</ta>
            <ta e="T1131" id="Seg_7478" s="T1130">kărɨtă</ta>
            <ta e="T1132" id="Seg_7479" s="T1131">kărɨtă-t</ta>
            <ta e="T1133" id="Seg_7480" s="T1132">nu-laʔbə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_7481" s="T1">šide</ta>
            <ta e="T3" id="Seg_7482" s="T2">ular-də</ta>
            <ta e="T4" id="Seg_7483" s="T3">i</ta>
            <ta e="T5" id="Seg_7484" s="T4">nüke</ta>
            <ta e="T7" id="Seg_7485" s="T6">šide</ta>
            <ta e="T8" id="Seg_7486" s="T7">koʔbdo</ta>
            <ta e="T9" id="Seg_7487" s="T8">i-bi</ta>
            <ta e="T10" id="Seg_7488" s="T9">šide</ta>
            <ta e="T11" id="Seg_7489" s="T10">koʔbdo</ta>
            <ta e="T12" id="Seg_7490" s="T11">i-bi</ta>
            <ta e="T13" id="Seg_7491" s="T12">dĭgəttə</ta>
            <ta e="T15" id="Seg_7492" s="T14">ular</ta>
            <ta e="T16" id="Seg_7493" s="T15">tabun</ta>
            <ta e="T17" id="Seg_7494" s="T16">i-bi</ta>
            <ta e="T18" id="Seg_7495" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_7496" s="T18">tabun</ta>
            <ta e="T20" id="Seg_7497" s="T19">ine</ta>
            <ta e="T21" id="Seg_7498" s="T20">i-bi-jəʔ</ta>
            <ta e="T22" id="Seg_7499" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_7500" s="T22">tabun</ta>
            <ta e="T24" id="Seg_7501" s="T23">tüžöj</ta>
            <ta e="T25" id="Seg_7502" s="T24">i-bi-jəʔ</ta>
            <ta e="T27" id="Seg_7503" s="T26">dĭgəttə</ta>
            <ta e="T28" id="Seg_7504" s="T27">onʼiʔ</ta>
            <ta e="T29" id="Seg_7505" s="T28">măndo-r-bi</ta>
            <ta e="T30" id="Seg_7506" s="T29">a</ta>
            <ta e="T31" id="Seg_7507" s="T30">onʼiʔ</ta>
            <ta e="T32" id="Seg_7508" s="T31">bar</ta>
            <ta e="T34" id="Seg_7509" s="T33">dĭgəttə</ta>
            <ta e="T35" id="Seg_7510" s="T34">onʼiʔ</ta>
            <ta e="T36" id="Seg_7511" s="T35">unu</ta>
            <ta e="T37" id="Seg_7512" s="T36">mĭnzər-bi</ta>
            <ta e="T38" id="Seg_7513" s="T37">măndo-r-bi</ta>
            <ta e="T39" id="Seg_7514" s="T38">dĭn</ta>
            <ta e="T40" id="Seg_7515" s="T39">Kodur-Kən</ta>
            <ta e="T41" id="Seg_7516" s="T40">a</ta>
            <ta e="T42" id="Seg_7517" s="T41">onʼiʔ</ta>
            <ta e="T43" id="Seg_7518" s="T42">svinʼes</ta>
            <ta e="T44" id="Seg_7519" s="T43">mĭnzər-bi</ta>
            <ta e="T45" id="Seg_7520" s="T44">dĭgəttə</ta>
            <ta e="T46" id="Seg_7521" s="T45">det-bi</ta>
            <ta e="T47" id="Seg_7522" s="T46">i</ta>
            <ta e="T48" id="Seg_7523" s="T47">ku-Tə</ta>
            <ta e="T49" id="Seg_7524" s="T48">kămnə-bi</ta>
            <ta e="T52" id="Seg_7525" s="T51">ku-Tə</ta>
            <ta e="T53" id="Seg_7526" s="T52">kămnə-bi</ta>
            <ta e="T54" id="Seg_7527" s="T53">i</ta>
            <ta e="T55" id="Seg_7528" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_7529" s="T55">bar</ta>
            <ta e="T57" id="Seg_7530" s="T56">kü-laːm-bi</ta>
            <ta e="T59" id="Seg_7531" s="T58">dĭgəttə</ta>
            <ta e="T60" id="Seg_7532" s="T59">dĭ-n</ta>
            <ta e="T61" id="Seg_7533" s="T60">nüjnə</ta>
            <ta e="T62" id="Seg_7534" s="T61">bar</ta>
            <ta e="T63" id="Seg_7535" s="T62">kü-laːm-bi</ta>
            <ta e="T64" id="Seg_7536" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_7537" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_7538" s="T65">koʔbdo-zAŋ</ta>
            <ta e="T67" id="Seg_7539" s="T66">i-bi-jəʔ</ta>
            <ta e="T68" id="Seg_7540" s="T67">ular</ta>
            <ta e="T69" id="Seg_7541" s="T68">ine-jəʔ</ta>
            <ta e="T70" id="Seg_7542" s="T69">i</ta>
            <ta e="T71" id="Seg_7543" s="T70">tüžöj-jəʔ</ta>
            <ta e="T72" id="Seg_7544" s="T71">i</ta>
            <ta e="T73" id="Seg_7545" s="T72">aba-gəndə</ta>
            <ta e="T1134" id="Seg_7546" s="T74">kan-lAʔ</ta>
            <ta e="T75" id="Seg_7547" s="T1134">tʼür-bi-jəʔ</ta>
            <ta e="T77" id="Seg_7548" s="T76">dĭgəttə</ta>
            <ta e="T78" id="Seg_7549" s="T77">bar</ta>
            <ta e="T79" id="Seg_7550" s="T78">skazka</ta>
            <ta e="T81" id="Seg_7551" s="T80">tumo</ta>
            <ta e="T82" id="Seg_7552" s="T81">i</ta>
            <ta e="T83" id="Seg_7553" s="T82">kös</ta>
            <ta e="T84" id="Seg_7554" s="T83">i</ta>
            <ta e="T85" id="Seg_7555" s="T84">puzɨrʼ</ta>
            <ta e="T86" id="Seg_7556" s="T85">kan-bi-jəʔ</ta>
            <ta e="T88" id="Seg_7557" s="T87">tʼăga</ta>
            <ta e="T89" id="Seg_7558" s="T88">a</ta>
            <ta e="T90" id="Seg_7559" s="T89">dĭ</ta>
            <ta e="T91" id="Seg_7560" s="T90">kan-zittə</ta>
            <ta e="T92" id="Seg_7561" s="T91">tʼăga</ta>
            <ta e="T93" id="Seg_7562" s="T92">tʼăga</ta>
            <ta e="T94" id="Seg_7563" s="T93">mʼaŋ-laʔbə</ta>
            <ta e="T95" id="Seg_7564" s="T94">tumo</ta>
            <ta e="T96" id="Seg_7565" s="T95">bar</ta>
            <ta e="T97" id="Seg_7566" s="T96">mu-jəʔ</ta>
            <ta e="T98" id="Seg_7567" s="T97">hen-bi</ta>
            <ta e="T99" id="Seg_7568" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_7569" s="T99">tumo</ta>
            <ta e="T1135" id="Seg_7570" s="T100">kan-lAʔ</ta>
            <ta e="T101" id="Seg_7571" s="T1135">tʼür-bi</ta>
            <ta e="T102" id="Seg_7572" s="T101">puzɨrʼ</ta>
            <ta e="T1136" id="Seg_7573" s="T102">kan-lAʔ</ta>
            <ta e="T103" id="Seg_7574" s="T1136">tʼür-bi</ta>
            <ta e="T105" id="Seg_7575" s="T104">a</ta>
            <ta e="T106" id="Seg_7576" s="T105">kös</ta>
            <ta e="T107" id="Seg_7577" s="T106">kan</ta>
            <ta e="T108" id="Seg_7578" s="T107">kös</ta>
            <ta e="T109" id="Seg_7579" s="T108">kan-bi</ta>
            <ta e="T110" id="Seg_7580" s="T109">i</ta>
            <ta e="T111" id="Seg_7581" s="T110">bü-Tə</ta>
            <ta e="T112" id="Seg_7582" s="T111">saʔmə-luʔbdə-bi</ta>
            <ta e="T113" id="Seg_7583" s="T112">i</ta>
            <ta e="T114" id="Seg_7584" s="T113">kü-laːm-bi</ta>
            <ta e="T116" id="Seg_7585" s="T115">a</ta>
            <ta e="T117" id="Seg_7586" s="T116">puzɨrʼ</ta>
            <ta e="T118" id="Seg_7587" s="T117">kakənar-bi</ta>
            <ta e="T119" id="Seg_7588" s="T118">kakənar-bi</ta>
            <ta e="T120" id="Seg_7589" s="T119">i</ta>
            <ta e="T121" id="Seg_7590" s="T120">kü-laːm-bi</ta>
            <ta e="T123" id="Seg_7591" s="T122">tumo</ta>
            <ta e="T124" id="Seg_7592" s="T123">unʼə</ta>
            <ta e="T126" id="Seg_7593" s="T125">ma-luʔbdə-bi</ta>
            <ta e="T127" id="Seg_7594" s="T126">dĭgəttə</ta>
            <ta e="T128" id="Seg_7595" s="T127">kan-bi</ta>
            <ta e="T129" id="Seg_7596" s="T128">kan-bi</ta>
            <ta e="T130" id="Seg_7597" s="T129">ešši-zAŋ</ta>
            <ta e="T131" id="Seg_7598" s="T130">bar</ta>
            <ta e="T132" id="Seg_7599" s="T131">sʼar-laʔbə-jəʔ</ta>
            <ta e="T134" id="Seg_7600" s="T133">bü</ta>
            <ta e="T135" id="Seg_7601" s="T134">kon-laːm-bi</ta>
            <ta e="T136" id="Seg_7602" s="T135">mu</ta>
            <ta e="T137" id="Seg_7603" s="T136">bü-Kən</ta>
            <ta e="T139" id="Seg_7604" s="T138">ešši-zAŋ</ta>
            <ta e="T140" id="Seg_7605" s="T139">ija</ta>
            <ta e="T141" id="Seg_7606" s="T140">aba-t</ta>
            <ta e="T143" id="Seg_7607" s="T142">ija-n</ta>
            <ta e="T144" id="Seg_7608" s="T143">kan-KAʔ</ta>
            <ta e="T145" id="Seg_7609" s="T144">maʔ-Tə</ta>
            <ta e="T146" id="Seg_7610" s="T145">da</ta>
            <ta e="T147" id="Seg_7611" s="T146">măn-ntə-lAʔ</ta>
            <ta e="T148" id="Seg_7612" s="T147">măn-ə-ʔ</ta>
            <ta e="T149" id="Seg_7613" s="T148">ija-Tə</ta>
            <ta e="T151" id="Seg_7614" s="T150">dĭ-zAŋ</ta>
            <ta e="T152" id="Seg_7615" s="T151">nuʔmə-lAʔ</ta>
            <ta e="T153" id="Seg_7616" s="T152">šo-bi-jəʔ</ta>
            <ta e="T155" id="Seg_7617" s="T154">aba-l</ta>
            <ta e="T156" id="Seg_7618" s="T155">šonə-gA</ta>
            <ta e="T158" id="Seg_7619" s="T157">no</ta>
            <ta e="T159" id="Seg_7620" s="T158">kan-KAʔ</ta>
            <ta e="T160" id="Seg_7621" s="T159">kăštə-KAʔ</ta>
            <ta e="T161" id="Seg_7622" s="T160">măn</ta>
            <ta e="T162" id="Seg_7623" s="T161">tüjö</ta>
            <ta e="T163" id="Seg_7624" s="T162">uja</ta>
            <ta e="T164" id="Seg_7625" s="T163">mĭnzər-lV-m</ta>
            <ta e="T166" id="Seg_7626" s="T165">dĭ-zAŋ</ta>
            <ta e="T167" id="Seg_7627" s="T166">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T168" id="Seg_7628" s="T167">šo-ʔ</ta>
            <ta e="T169" id="Seg_7629" s="T168">ija-m</ta>
            <ta e="T170" id="Seg_7630" s="T169">uja-m</ta>
            <ta e="T172" id="Seg_7631" s="T171">mĭnzər-laʔbə-jəʔ</ta>
            <ta e="T174" id="Seg_7632" s="T173">üjü-jəʔ</ta>
            <ta e="T176" id="Seg_7633" s="T175">măn</ta>
            <ta e="T177" id="Seg_7634" s="T176">dĭ-m</ta>
            <ta e="T178" id="Seg_7635" s="T177">e-m</ta>
            <ta e="T180" id="Seg_7636" s="T179">naga</ta>
            <ta e="T181" id="Seg_7637" s="T180">am-zittə</ta>
            <ta e="T182" id="Seg_7638" s="T181">pušaj</ta>
            <ta e="T183" id="Seg_7639" s="T182">tüžöj-də</ta>
            <ta e="T184" id="Seg_7640" s="T183">băt-lV-j</ta>
            <ta e="T185" id="Seg_7641" s="T184">i</ta>
            <ta e="T186" id="Seg_7642" s="T185">mĭnzər-lV-j</ta>
            <ta e="T187" id="Seg_7643" s="T186">kuba-Tə</ta>
            <ta e="T188" id="Seg_7644" s="T187">nuldə-lV-j</ta>
            <ta e="T189" id="Seg_7645" s="T188">dĭ</ta>
            <ta e="T190" id="Seg_7646" s="T189">tüžöj</ta>
            <ta e="T191" id="Seg_7647" s="T190">băt-bi</ta>
            <ta e="T193" id="Seg_7648" s="T192">uja</ta>
            <ta e="T194" id="Seg_7649" s="T193">mĭnzər-bi</ta>
            <ta e="T195" id="Seg_7650" s="T194">kuba-Tə</ta>
            <ta e="T196" id="Seg_7651" s="T195">hen-bi</ta>
            <ta e="T198" id="Seg_7652" s="T197">dĭgəttə</ta>
            <ta e="T199" id="Seg_7653" s="T198">ešši-zAŋ</ta>
            <ta e="T200" id="Seg_7654" s="T199">kan-bi-jəʔ</ta>
            <ta e="T201" id="Seg_7655" s="T200">šo-ʔ</ta>
            <ta e="T202" id="Seg_7656" s="T201">uja</ta>
            <ta e="T203" id="Seg_7657" s="T202">mĭnzər-lə-o-NTA</ta>
            <ta e="T205" id="Seg_7658" s="T204">šo-bi-jəʔ</ta>
            <ta e="T207" id="Seg_7659" s="T206">gijen</ta>
            <ta e="T208" id="Seg_7660" s="T207">aba-m</ta>
            <ta e="T210" id="Seg_7661" s="T209">davaj</ta>
            <ta e="T211" id="Seg_7662" s="T210">dĭ-zem</ta>
            <ta e="T212" id="Seg_7663" s="T211">münör-zittə</ta>
            <ta e="T213" id="Seg_7664" s="T212">ešši-zAŋ-də</ta>
            <ta e="T215" id="Seg_7665" s="T214">a</ta>
            <ta e="T216" id="Seg_7666" s="T215">tumo</ta>
            <ta e="T217" id="Seg_7667" s="T216">amno-laʔbə</ta>
            <ta e="T218" id="Seg_7668" s="T217">e-ʔ</ta>
            <ta e="T219" id="Seg_7669" s="T218">tʼabəro-ʔ</ta>
            <ta e="T220" id="Seg_7670" s="T219">măn</ta>
            <ta e="T221" id="Seg_7671" s="T220">dön</ta>
            <ta e="T222" id="Seg_7672" s="T221">šo-bi-m</ta>
            <ta e="T224" id="Seg_7673" s="T223">teinen</ta>
            <ta e="T225" id="Seg_7674" s="T224">bar</ta>
            <ta e="T226" id="Seg_7675" s="T225">sĭri</ta>
            <ta e="T227" id="Seg_7676" s="T226">saʔmə-laʔbə</ta>
            <ta e="T229" id="Seg_7677" s="T228">i</ta>
            <ta e="T230" id="Seg_7678" s="T229">beržə</ta>
            <ta e="T231" id="Seg_7679" s="T230">šonə-gA</ta>
            <ta e="T233" id="Seg_7680" s="T232">ugaːndə</ta>
            <ta e="T234" id="Seg_7681" s="T233">šišəge</ta>
            <ta e="T235" id="Seg_7682" s="T234">nʼiʔnen</ta>
            <ta e="T237" id="Seg_7683" s="T236">nʼiʔnen</ta>
            <ta e="T239" id="Seg_7684" s="T238">kumən</ta>
            <ta e="T240" id="Seg_7685" s="T239">kö</ta>
            <ta e="T241" id="Seg_7686" s="T240">ešši-Tə</ta>
            <ta e="T243" id="Seg_7687" s="T242">nagur</ta>
            <ta e="T245" id="Seg_7688" s="T244">dö</ta>
            <ta e="T246" id="Seg_7689" s="T245">kö-n</ta>
            <ta e="T247" id="Seg_7690" s="T246">ugaːndə</ta>
            <ta e="T248" id="Seg_7691" s="T247">šišəge</ta>
            <ta e="T249" id="Seg_7692" s="T248">i-bi</ta>
            <ta e="T251" id="Seg_7693" s="T250">kăn-laʔbə-bi-jəʔ</ta>
            <ta e="T252" id="Seg_7694" s="T251">il</ta>
            <ta e="T253" id="Seg_7695" s="T252">bar</ta>
            <ta e="T257" id="Seg_7696" s="T256">noʔ</ta>
            <ta e="T258" id="Seg_7697" s="T257">hʼaʔ-bi-m</ta>
            <ta e="T259" id="Seg_7698" s="T258">šapku-ziʔ</ta>
            <ta e="T261" id="Seg_7699" s="T260">dĭgəttə</ta>
            <ta e="T262" id="Seg_7700" s="T261">koʔ-laːm-bi</ta>
            <ta e="T263" id="Seg_7701" s="T262">noʔ</ta>
            <ta e="T264" id="Seg_7702" s="T263">măn</ta>
            <ta e="T265" id="Seg_7703" s="T264">dĭ-m</ta>
            <ta e="T266" id="Seg_7704" s="T265">oʔbdə-bi-m</ta>
            <ta e="T267" id="Seg_7705" s="T266">kopnă-Tə</ta>
            <ta e="T268" id="Seg_7706" s="T267">dĭgəttə</ta>
            <ta e="T269" id="Seg_7707" s="T268">ine</ta>
            <ta e="T270" id="Seg_7708" s="T269">i-bi-m</ta>
            <ta e="T271" id="Seg_7709" s="T270">tažor-bi-m</ta>
            <ta e="T273" id="Seg_7710" s="T272">hen-bi-m</ta>
            <ta e="T274" id="Seg_7711" s="T273">zarot-Tə</ta>
            <ta e="T278" id="Seg_7712" s="T277">dö</ta>
            <ta e="T279" id="Seg_7713" s="T278">pʼe-n</ta>
            <ta e="T280" id="Seg_7714" s="T279">sagər</ta>
            <ta e="T281" id="Seg_7715" s="T280">keʔbde</ta>
            <ta e="T282" id="Seg_7716" s="T281">naga</ta>
            <ta e="T283" id="Seg_7717" s="T282">a</ta>
            <ta e="T284" id="Seg_7718" s="T283">kömə</ta>
            <ta e="T285" id="Seg_7719" s="T284">keʔbde</ta>
            <ta e="T286" id="Seg_7720" s="T285">i-bi</ta>
            <ta e="T288" id="Seg_7721" s="T287">il</ta>
            <ta e="T289" id="Seg_7722" s="T288">iʔgö</ta>
            <ta e="T290" id="Seg_7723" s="T289">tažor-bi-jəʔ</ta>
            <ta e="T291" id="Seg_7724" s="T290">maʔ-gəndə</ta>
            <ta e="T292" id="Seg_7725" s="T291">keʔbde-jəʔ</ta>
            <ta e="T294" id="Seg_7726" s="T293">sanə</ta>
            <ta e="T295" id="Seg_7727" s="T294">iʔgö</ta>
            <ta e="T296" id="Seg_7728" s="T295">i-bi</ta>
            <ta e="T297" id="Seg_7729" s="T296">bar</ta>
            <ta e="T299" id="Seg_7730" s="T298">il</ta>
            <ta e="T300" id="Seg_7731" s="T299">ugaːndə</ta>
            <ta e="T301" id="Seg_7732" s="T300">iʔgö</ta>
            <ta e="T302" id="Seg_7733" s="T301">oʔbdə-bi-jəʔ</ta>
            <ta e="T303" id="Seg_7734" s="T302">sanə</ta>
            <ta e="T307" id="Seg_7735" s="T306">nagur</ta>
            <ta e="T308" id="Seg_7736" s="T307">pʼe</ta>
            <ta e="T309" id="Seg_7737" s="T308">naga-bi</ta>
            <ta e="T310" id="Seg_7738" s="T309">že</ta>
            <ta e="T311" id="Seg_7739" s="T310">sanə</ta>
            <ta e="T315" id="Seg_7740" s="T314">sanə</ta>
            <ta e="T316" id="Seg_7741" s="T315">toʔ-nar-zittə</ta>
            <ta e="T319" id="Seg_7742" s="T318">dĭ-zAŋ</ta>
            <ta e="T320" id="Seg_7743" s="T319">bos-də</ta>
            <ta e="T321" id="Seg_7744" s="T320">saʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T322" id="Seg_7745" s="T321">a</ta>
            <ta e="T323" id="Seg_7746" s="T322">il</ta>
            <ta e="T324" id="Seg_7747" s="T323">oʔbdə-laʔbə-jəʔ</ta>
            <ta e="T328" id="Seg_7748" s="T327">dö</ta>
            <ta e="T329" id="Seg_7749" s="T328">pʼe-n</ta>
            <ta e="T330" id="Seg_7750" s="T329">lem</ta>
            <ta e="T331" id="Seg_7751" s="T330">keʔbde</ta>
            <ta e="T332" id="Seg_7752" s="T331">naga-bi</ta>
            <ta e="T333" id="Seg_7753" s="T332">tože</ta>
            <ta e="T337" id="Seg_7754" s="T336">dö</ta>
            <ta e="T338" id="Seg_7755" s="T337">kö</ta>
            <ta e="T339" id="Seg_7756" s="T338">ugaːndə</ta>
            <ta e="T340" id="Seg_7757" s="T339">budəj</ta>
            <ta e="T341" id="Seg_7758" s="T340">jakšə</ta>
            <ta e="T342" id="Seg_7759" s="T341">i-bi</ta>
            <ta e="T344" id="Seg_7760" s="T343">i</ta>
            <ta e="T345" id="Seg_7761" s="T344">toltano</ta>
            <ta e="T346" id="Seg_7762" s="T345">jakšə</ta>
            <ta e="T347" id="Seg_7763" s="T346">i-bi</ta>
            <ta e="T348" id="Seg_7764" s="T347">iʔgö</ta>
            <ta e="T349" id="Seg_7765" s="T348">özer-bi</ta>
            <ta e="T350" id="Seg_7766" s="T349">urgo</ta>
            <ta e="T351" id="Seg_7767" s="T350">i-bi</ta>
            <ta e="T355" id="Seg_7768" s="T354">sĭri</ta>
            <ta e="T356" id="Seg_7769" s="T355">ipek</ta>
            <ta e="T358" id="Seg_7770" s="T357">pür-liA-jəʔ</ta>
            <ta e="T359" id="Seg_7771" s="T358">il</ta>
            <ta e="T360" id="Seg_7772" s="T359">ugaːndə</ta>
            <ta e="T362" id="Seg_7773" s="T361">köbergən</ta>
            <ta e="T363" id="Seg_7774" s="T362">bar</ta>
            <ta e="T364" id="Seg_7775" s="T363">kü-laːm-bi</ta>
            <ta e="T365" id="Seg_7776" s="T364">ăgărot-Kən</ta>
            <ta e="T367" id="Seg_7777" s="T366">a</ta>
            <ta e="T371" id="Seg_7778" s="T370">a</ta>
            <ta e="T372" id="Seg_7779" s="T371">dʼije-Kən</ta>
            <ta e="T373" id="Seg_7780" s="T372">köbergən</ta>
            <ta e="T374" id="Seg_7781" s="T373">jakšə</ta>
            <ta e="T375" id="Seg_7782" s="T374">i-bi</ta>
            <ta e="T379" id="Seg_7783" s="T378">măn</ta>
            <ta e="T382" id="Seg_7784" s="T381">nĭŋgə-bi-m</ta>
            <ta e="T383" id="Seg_7785" s="T382">dʼagar-bi-m</ta>
            <ta e="T384" id="Seg_7786" s="T383">i</ta>
            <ta e="T385" id="Seg_7787" s="T384">koʔ-laʔbə-bi</ta>
            <ta e="T387" id="Seg_7788" s="T386">a</ta>
            <ta e="T388" id="Seg_7789" s="T387">tüj</ta>
            <ta e="T389" id="Seg_7790" s="T388">mĭnzər-liA-m</ta>
            <ta e="T390" id="Seg_7791" s="T389">uja-ziʔ</ta>
            <ta e="T391" id="Seg_7792" s="T390">toltano-ziʔ</ta>
            <ta e="T398" id="Seg_7793" s="T397">nu-zAŋ-Kən</ta>
            <ta e="T399" id="Seg_7794" s="T398">oʔb</ta>
            <ta e="T400" id="Seg_7795" s="T399">kuza</ta>
            <ta e="T401" id="Seg_7796" s="T400">kü-laːm-bi</ta>
            <ta e="T402" id="Seg_7797" s="T401">dĭ-zAŋ</ta>
            <ta e="T403" id="Seg_7798" s="T402">bar</ta>
            <ta e="T404" id="Seg_7799" s="T403">tĭl-bi-jəʔ</ta>
            <ta e="T405" id="Seg_7800" s="T404">tʼo</ta>
            <ta e="T406" id="Seg_7801" s="T405">dĭgəttə</ta>
            <ta e="T407" id="Seg_7802" s="T406">a-bi-jəʔ</ta>
            <ta e="T411" id="Seg_7803" s="T410">dĭgəttə</ta>
            <ta e="T412" id="Seg_7804" s="T411">maʔ</ta>
            <ta e="T413" id="Seg_7805" s="T412">a-bi-jəʔ</ta>
            <ta e="T414" id="Seg_7806" s="T413">krospa</ta>
            <ta e="T415" id="Seg_7807" s="T414">a-bi-jəʔ</ta>
            <ta e="T416" id="Seg_7808" s="T415">maʔ-Tə</ta>
            <ta e="T417" id="Seg_7809" s="T416">dĭ-m</ta>
            <ta e="T418" id="Seg_7810" s="T417">hen-bi-jəʔ</ta>
            <ta e="T422" id="Seg_7811" s="T421">dĭ</ta>
            <ta e="T423" id="Seg_7812" s="T422">dĭ-Tə</ta>
            <ta e="T424" id="Seg_7813" s="T423">kanza</ta>
            <ta e="T425" id="Seg_7814" s="T424">am-bi-jəʔ</ta>
            <ta e="T426" id="Seg_7815" s="T425">taŋgu</ta>
            <ta e="T427" id="Seg_7816" s="T426">am-bi-jəʔ</ta>
            <ta e="T429" id="Seg_7817" s="T428">multuk</ta>
            <ta e="T430" id="Seg_7818" s="T429">am-bi-jəʔ</ta>
            <ta e="T431" id="Seg_7819" s="T430">bar</ta>
            <ta e="T433" id="Seg_7820" s="T432">aspaʔ</ta>
            <ta e="T434" id="Seg_7821" s="T433">am-bi-jəʔ</ta>
            <ta e="T435" id="Seg_7822" s="T434">hen-bi-jəʔ</ta>
            <ta e="T437" id="Seg_7823" s="T436">dĭ</ta>
            <ta e="T438" id="Seg_7824" s="T437">dĭn</ta>
            <ta e="T439" id="Seg_7825" s="T438">mĭnzər-lV-j</ta>
            <ta e="T440" id="Seg_7826" s="T439">i</ta>
            <ta e="T441" id="Seg_7827" s="T440">amor-lV-j</ta>
            <ta e="T445" id="Seg_7828" s="T444">dĭgəttə</ta>
            <ta e="T446" id="Seg_7829" s="T445">dĭ</ta>
            <ta e="T447" id="Seg_7830" s="T446">kuza-m</ta>
            <ta e="T448" id="Seg_7831" s="T447">kun-bi-jəʔ</ta>
            <ta e="T450" id="Seg_7832" s="T449">tʼo-Tə</ta>
            <ta e="T451" id="Seg_7833" s="T450">hen-bi-jəʔ</ta>
            <ta e="T452" id="Seg_7834" s="T451">tʼo-ziʔ</ta>
            <ta e="T453" id="Seg_7835" s="T452">bar</ta>
            <ta e="T454" id="Seg_7836" s="T453">kămnə-bi-jəʔ</ta>
            <ta e="T456" id="Seg_7837" s="T455">dĭgəttə</ta>
            <ta e="T457" id="Seg_7838" s="T456">maʔ-gəndə</ta>
            <ta e="T458" id="Seg_7839" s="T457">šo-bi-jəʔ</ta>
            <ta e="T460" id="Seg_7840" s="T459">nüdʼi-n</ta>
            <ta e="T461" id="Seg_7841" s="T460">bar</ta>
            <ta e="T462" id="Seg_7842" s="T461">men</ta>
            <ta e="T463" id="Seg_7843" s="T462">sar-bi-jəʔ</ta>
            <ta e="T464" id="Seg_7844" s="T463">da</ta>
            <ta e="T465" id="Seg_7845" s="T464">ine</ta>
            <ta e="T467" id="Seg_7846" s="T466">dĭgəttə</ta>
            <ta e="T468" id="Seg_7847" s="T467">kan-bi-jəʔ</ta>
            <ta e="T470" id="Seg_7848" s="T469">Oj</ta>
            <ta e="T471" id="Seg_7849" s="T470">šonə-gA</ta>
            <ta e="T472" id="Seg_7850" s="T471">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T473" id="Seg_7851" s="T472">tura-Tə</ta>
            <ta e="T474" id="Seg_7852" s="T473">bar</ta>
            <ta e="T476" id="Seg_7853" s="T475">šapku</ta>
            <ta e="T477" id="Seg_7854" s="T476">ajə-Kən</ta>
            <ta e="T478" id="Seg_7855" s="T477">hen-lV-jəʔ</ta>
            <ta e="T482" id="Seg_7856" s="T481">men</ta>
            <ta e="T483" id="Seg_7857" s="T482">sar-bi-jəʔ</ta>
            <ta e="T484" id="Seg_7858" s="T483">sagər</ta>
            <ta e="T485" id="Seg_7859" s="T484">i</ta>
            <ta e="T486" id="Seg_7860" s="T485">sima-t</ta>
            <ta e="T487" id="Seg_7861" s="T486">oʔb</ta>
            <ta e="T488" id="Seg_7862" s="T487">šide</ta>
            <ta e="T489" id="Seg_7863" s="T488">nagur</ta>
            <ta e="T490" id="Seg_7864" s="T489">teʔdə</ta>
            <ta e="T491" id="Seg_7865" s="T490">teʔdə</ta>
            <ta e="T492" id="Seg_7866" s="T491">sima-t</ta>
            <ta e="T496" id="Seg_7867" s="T495">šapku</ta>
            <ta e="T497" id="Seg_7868" s="T496">am-bi-jəʔ</ta>
            <ta e="T498" id="Seg_7869" s="T497">štobɨ</ta>
            <ta e="T499" id="Seg_7870" s="T498">penzi</ta>
            <ta e="T501" id="Seg_7871" s="T500">dʼagar-luʔbdə-bi</ta>
            <ta e="T505" id="Seg_7872" s="T504">onʼiʔ</ta>
            <ta e="T506" id="Seg_7873" s="T505">onʼiʔ</ta>
            <ta e="T507" id="Seg_7874" s="T506">kuza</ta>
            <ta e="T509" id="Seg_7875" s="T508">miʔnʼibeʔ</ta>
            <ta e="T510" id="Seg_7876" s="T509">ĭzem-bi</ta>
            <ta e="T511" id="Seg_7877" s="T510">ĭzem-bi</ta>
            <ta e="T512" id="Seg_7878" s="T511">dĭgəttə</ta>
            <ta e="T514" id="Seg_7879" s="T513">kü-laːm-bi</ta>
            <ta e="T516" id="Seg_7880" s="T515">a</ta>
            <ta e="T517" id="Seg_7881" s="T516">miʔ</ta>
            <ta e="T519" id="Seg_7882" s="T518">kan-bi-bAʔ</ta>
            <ta e="T1139" id="Seg_7883" s="T519">Kazan</ta>
            <ta e="T520" id="Seg_7884" s="T1139">tura-Tə</ta>
            <ta e="T522" id="Seg_7885" s="T521">sazən</ta>
            <ta e="T523" id="Seg_7886" s="T522">uda-gəndə</ta>
            <ta e="T525" id="Seg_7887" s="T524">uda-gəndə</ta>
            <ta e="T526" id="Seg_7888" s="T525">i-zittə</ta>
            <ta e="T527" id="Seg_7889" s="T526">i</ta>
            <ta e="T528" id="Seg_7890" s="T527">ulu-gəndə</ta>
            <ta e="T530" id="Seg_7891" s="T529">dĭgəttə</ta>
            <ta e="T531" id="Seg_7892" s="T530">abəs</ta>
            <ta e="T532" id="Seg_7893" s="T531">miʔnʼibeʔ</ta>
            <ta e="T533" id="Seg_7894" s="T532">ej</ta>
            <ta e="T534" id="Seg_7895" s="T533">mĭ-bi</ta>
            <ta e="T536" id="Seg_7896" s="T535">miʔ</ta>
            <ta e="T537" id="Seg_7897" s="T536">ara</ta>
            <ta e="T538" id="Seg_7898" s="T537">i-bi-bAʔ</ta>
            <ta e="T540" id="Seg_7899" s="T539">i</ta>
            <ta e="T541" id="Seg_7900" s="T540">maʔ-gənʼi</ta>
            <ta e="T542" id="Seg_7901" s="T541">šo-bi-bAʔ</ta>
            <ta e="T544" id="Seg_7902" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7903" s="T544">ija-m</ta>
            <ta e="T546" id="Seg_7904" s="T545">aba-m</ta>
            <ta e="T548" id="Seg_7905" s="T547">i-bi-jəʔ</ta>
            <ta e="T549" id="Seg_7906" s="T548">dĭ</ta>
            <ta e="T550" id="Seg_7907" s="T549">ara</ta>
            <ta e="T1137" id="Seg_7908" s="T550">kan-lAʔ</ta>
            <ta e="T551" id="Seg_7909" s="T1137">tʼür-bi-jəʔ</ta>
            <ta e="T552" id="Seg_7910" s="T551">bĭs-zittə</ta>
            <ta e="T554" id="Seg_7911" s="T553">a</ta>
            <ta e="T555" id="Seg_7912" s="T554">miʔ</ta>
            <ta e="T556" id="Seg_7913" s="T555">ešši-zAŋ</ta>
            <ta e="T557" id="Seg_7914" s="T556">miʔ</ta>
            <ta e="T558" id="Seg_7915" s="T557">tože</ta>
            <ta e="T559" id="Seg_7916" s="T558">ej</ta>
            <ta e="T560" id="Seg_7917" s="T559">ma-lV-m</ta>
            <ta e="T561" id="Seg_7918" s="T560">maʔ-Tə</ta>
            <ta e="T564" id="Seg_7919" s="T563">kan-že-bəj</ta>
            <ta e="T566" id="Seg_7920" s="T565">a</ta>
            <ta e="T567" id="Seg_7921" s="T566">măn</ta>
            <ta e="T568" id="Seg_7922" s="T567">măn-bi-m</ta>
            <ta e="T569" id="Seg_7923" s="T568">iʔbə-ʔ</ta>
            <ta e="T570" id="Seg_7924" s="T569">kunol-ə-ʔ</ta>
            <ta e="T572" id="Seg_7925" s="T571">a</ta>
            <ta e="T573" id="Seg_7926" s="T572">măn</ta>
            <ta e="T574" id="Seg_7927" s="T573">iʔbə-lV-m</ta>
            <ta e="T575" id="Seg_7928" s="T574">šiʔnʼileʔ</ta>
            <ta e="T577" id="Seg_7929" s="T576">kamən</ta>
            <ta e="T578" id="Seg_7930" s="T577">măna</ta>
            <ta e="T579" id="Seg_7931" s="T578">kabarləj</ta>
            <ta e="T580" id="Seg_7932" s="T579">a</ta>
            <ta e="T581" id="Seg_7933" s="T580">šiʔ</ta>
            <ta e="T582" id="Seg_7934" s="T581">üʔmə-luʔbdə-lAʔ</ta>
            <ta e="T584" id="Seg_7935" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_7936" s="T584">ertə-n</ta>
            <ta e="T586" id="Seg_7937" s="T585">uʔbdə-bi-m</ta>
            <ta e="T588" id="Seg_7938" s="T587">vot</ta>
            <ta e="T589" id="Seg_7939" s="T588">ku-liA-l</ta>
            <ta e="T590" id="Seg_7940" s="T589">dĭ</ta>
            <ta e="T591" id="Seg_7941" s="T590">iʔbə-laʔbə</ta>
            <ta e="T592" id="Seg_7942" s="T591">i</ta>
            <ta e="T594" id="Seg_7943" s="T593">i</ta>
            <ta e="T595" id="Seg_7944" s="T594">miʔ</ta>
            <ta e="T596" id="Seg_7945" s="T595">kunol-bi-bAʔ</ta>
            <ta e="T600" id="Seg_7946" s="T599">dĭgəttə</ta>
            <ta e="T601" id="Seg_7947" s="T600">dĭ</ta>
            <ta e="T602" id="Seg_7948" s="T601">kü-laːm-bi</ta>
            <ta e="T603" id="Seg_7949" s="T602">dĭ-m</ta>
            <ta e="T604" id="Seg_7950" s="T603">bazə-bi-jəʔ</ta>
            <ta e="T606" id="Seg_7951" s="T605">kujnek</ta>
            <ta e="T607" id="Seg_7952" s="T606">šer-bi-jəʔ</ta>
            <ta e="T608" id="Seg_7953" s="T607">piʔme</ta>
            <ta e="T609" id="Seg_7954" s="T608">šer-bi-jəʔ</ta>
            <ta e="T611" id="Seg_7955" s="T610">jama-jəʔ</ta>
            <ta e="T612" id="Seg_7956" s="T611">šöʔ-bi-jəʔ</ta>
            <ta e="T613" id="Seg_7957" s="T612">šer-bi-jəʔ</ta>
            <ta e="T615" id="Seg_7958" s="T614">dĭgəttə</ta>
            <ta e="T616" id="Seg_7959" s="T615">kon-bi-jəʔ</ta>
            <ta e="T619" id="Seg_7960" s="T618">tʼo-ziʔ</ta>
            <ta e="T620" id="Seg_7961" s="T619">kămnə-bi-jəʔ</ta>
            <ta e="T621" id="Seg_7962" s="T620">dĭ-m</ta>
            <ta e="T623" id="Seg_7963" s="T622">šo-bi-jəʔ</ta>
            <ta e="T624" id="Seg_7964" s="T623">maʔ-gəndə</ta>
            <ta e="T626" id="Seg_7965" s="T625">ara</ta>
            <ta e="T627" id="Seg_7966" s="T626">bĭs-bi-jəʔ</ta>
            <ta e="T628" id="Seg_7967" s="T627">ipek</ta>
            <ta e="T629" id="Seg_7968" s="T628">amor-bi-jəʔ</ta>
            <ta e="T630" id="Seg_7969" s="T629">uja</ta>
            <ta e="T631" id="Seg_7970" s="T630">am-bi-jəʔ</ta>
            <ta e="T633" id="Seg_7971" s="T632">i</ta>
            <ta e="T634" id="Seg_7972" s="T633">maʔ-gəndə</ta>
            <ta e="T1138" id="Seg_7973" s="T634">kan-lAʔ</ta>
            <ta e="T635" id="Seg_7974" s="T1138">tʼür-bi-jəʔ</ta>
            <ta e="T639" id="Seg_7975" s="T638">măn</ta>
            <ta e="T640" id="Seg_7976" s="T639">onʼiʔ</ta>
            <ta e="T641" id="Seg_7977" s="T640">raz</ta>
            <ta e="T642" id="Seg_7978" s="T641">šo</ta>
            <ta e="T643" id="Seg_7979" s="T642">šo-bi-m</ta>
            <ta e="T1140" id="Seg_7980" s="T643">Kazan</ta>
            <ta e="T644" id="Seg_7981" s="T1140">tura-Kən</ta>
            <ta e="T646" id="Seg_7982" s="T645">šo-bi-m</ta>
            <ta e="T647" id="Seg_7983" s="T646">Permʼakovo-Tə</ta>
            <ta e="T648" id="Seg_7984" s="T647">dĭgəttə</ta>
            <ta e="T649" id="Seg_7985" s="T648">maʔ-gəndə</ta>
            <ta e="T650" id="Seg_7986" s="T649">šo-bi-m</ta>
            <ta e="T652" id="Seg_7987" s="T651">tʼermən-Kən</ta>
            <ta e="T653" id="Seg_7988" s="T652">dĭn</ta>
            <ta e="T654" id="Seg_7989" s="T653">ildə-m</ta>
            <ta e="T655" id="Seg_7990" s="T654">šində=də</ta>
            <ta e="T657" id="Seg_7991" s="T656">pʼer-luʔbdə-bi-jəʔ</ta>
            <ta e="T659" id="Seg_7992" s="T658">a</ta>
            <ta e="T660" id="Seg_7993" s="T659">măn</ta>
            <ta e="T661" id="Seg_7994" s="T660">šo-bi-m</ta>
            <ta e="T662" id="Seg_7995" s="T661">da</ta>
            <ta e="T664" id="Seg_7996" s="T663">dĭgəttə</ta>
            <ta e="T665" id="Seg_7997" s="T664">toʔnar-lV-m</ta>
            <ta e="T666" id="Seg_7998" s="T665">bar</ta>
            <ta e="T667" id="Seg_7999" s="T666">aktʼa</ta>
            <ta e="T668" id="Seg_8000" s="T667">iʔgö</ta>
            <ta e="T669" id="Seg_8001" s="T668">mo-lV-j</ta>
            <ta e="T673" id="Seg_8002" s="T672">šo-bi-m</ta>
            <ta e="T675" id="Seg_8003" s="T674">maʔ-gəndə</ta>
            <ta e="T676" id="Seg_8004" s="T675">šində-gəndə</ta>
            <ta e="T677" id="Seg_8005" s="T676">ej</ta>
            <ta e="T678" id="Seg_8006" s="T677">ku-bi-m</ta>
            <ta e="T682" id="Seg_8007" s="T681">măn</ta>
            <ta e="T683" id="Seg_8008" s="T682">ulu-m</ta>
            <ta e="T684" id="Seg_8009" s="T683">ĭzem-liA</ta>
            <ta e="T685" id="Seg_8010" s="T684">sima-m</ta>
            <ta e="T686" id="Seg_8011" s="T685">ĭzem-liA</ta>
            <ta e="T687" id="Seg_8012" s="T686">a</ta>
            <ta e="T688" id="Seg_8013" s="T687">tăn</ta>
            <ta e="T689" id="Seg_8014" s="T688">ĭmbi</ta>
            <ta e="T690" id="Seg_8015" s="T689">ĭzem-liA</ta>
            <ta e="T691" id="Seg_8016" s="T690">a</ta>
            <ta e="T692" id="Seg_8017" s="T691">măn</ta>
            <ta e="T693" id="Seg_8018" s="T692">tĭme-m</ta>
            <ta e="T694" id="Seg_8019" s="T693">ĭzem-bi</ta>
            <ta e="T695" id="Seg_8020" s="T694">ĭzem-laʔbə</ta>
            <ta e="T696" id="Seg_8021" s="T695">i</ta>
            <ta e="T697" id="Seg_8022" s="T696">püje-m</ta>
            <ta e="T698" id="Seg_8023" s="T697">ĭzem-laʔbə</ta>
            <ta e="T702" id="Seg_8024" s="T701">Ivanovič-ə-n</ta>
            <ta e="T703" id="Seg_8025" s="T702">bar</ta>
            <ta e="T704" id="Seg_8026" s="T703">šüjoŋ-t</ta>
            <ta e="T705" id="Seg_8027" s="T704">ĭzem-liA</ta>
            <ta e="T706" id="Seg_8028" s="T705">i</ta>
            <ta e="T707" id="Seg_8029" s="T706">üjü-t</ta>
            <ta e="T708" id="Seg_8030" s="T707">ĭzem-liA</ta>
            <ta e="T709" id="Seg_8031" s="T708">i</ta>
            <ta e="T711" id="Seg_8032" s="T710">uda-zAŋ-də</ta>
            <ta e="T712" id="Seg_8033" s="T711">ĭzem-laʔbə-jəʔ</ta>
            <ta e="T713" id="Seg_8034" s="T712">bar</ta>
            <ta e="T714" id="Seg_8035" s="T713">tăŋ</ta>
            <ta e="T715" id="Seg_8036" s="T714">ĭzem-liA</ta>
            <ta e="T719" id="Seg_8037" s="T718">nadə</ta>
            <ta e="T722" id="Seg_8038" s="T721">tʼezer-zittə</ta>
            <ta e="T723" id="Seg_8039" s="T722">dĭ</ta>
            <ta e="T725" id="Seg_8040" s="T724">nagur</ta>
            <ta e="T726" id="Seg_8041" s="T725">kuza</ta>
            <ta e="T728" id="Seg_8042" s="T727">doktăr</ta>
            <ta e="T729" id="Seg_8043" s="T728">nadə</ta>
            <ta e="T730" id="Seg_8044" s="T729">det-zittə</ta>
            <ta e="T731" id="Seg_8045" s="T730">pušaj</ta>
            <ta e="T732" id="Seg_8046" s="T731">tʼezer-lV-j</ta>
            <ta e="T733" id="Seg_8047" s="T732">dĭ</ta>
            <ta e="T734" id="Seg_8048" s="T733">dĭ-zem</ta>
            <ta e="T738" id="Seg_8049" s="T737">ĭmbi=nʼibudʼ</ta>
            <ta e="T739" id="Seg_8050" s="T738">mĭ-lV-j</ta>
            <ta e="T740" id="Seg_8051" s="T739">bĭs-zittə</ta>
            <ta e="T741" id="Seg_8052" s="T740">aľi</ta>
            <ta e="T742" id="Seg_8053" s="T741">ĭmbi=nʼibudʼ</ta>
            <ta e="T743" id="Seg_8054" s="T742">mĭ-lV-j</ta>
            <ta e="T744" id="Seg_8055" s="T743">kĭškə-zittə</ta>
            <ta e="T748" id="Seg_8056" s="T747">dĭ-zAŋ</ta>
            <ta e="T749" id="Seg_8057" s="T748">bar</ta>
            <ta e="T750" id="Seg_8058" s="T749">tăŋ</ta>
            <ta e="T751" id="Seg_8059" s="T750">kănna-m-bi-jəʔ</ta>
            <ta e="T752" id="Seg_8060" s="T751">a</ta>
            <ta e="T753" id="Seg_8061" s="T752">tüj</ta>
            <ta e="T754" id="Seg_8062" s="T753">ugaːndə</ta>
            <ta e="T755" id="Seg_8063" s="T754">tăŋ</ta>
            <ta e="T756" id="Seg_8064" s="T755">ĭzem-laʔbə-jəʔ</ta>
            <ta e="T760" id="Seg_8065" s="T759">Marejka</ta>
            <ta e="T761" id="Seg_8066" s="T760">urgaja-Tə</ta>
            <ta e="T762" id="Seg_8067" s="T761">kadul-də</ta>
            <ta e="T764" id="Seg_8068" s="T763">urgaja-Tə</ta>
            <ta e="T765" id="Seg_8069" s="T764">dĭrgit</ta>
            <ta e="T766" id="Seg_8070" s="T765">kadul-də</ta>
            <ta e="T767" id="Seg_8071" s="T766">i</ta>
            <ta e="T768" id="Seg_8072" s="T767">sima-t</ta>
            <ta e="T769" id="Seg_8073" s="T768">dĭrgit</ta>
            <ta e="T770" id="Seg_8074" s="T769">urgaja-gəndə</ta>
            <ta e="T774" id="Seg_8075" s="T773">amno-bi-jəʔ</ta>
            <ta e="T775" id="Seg_8076" s="T774">nüke</ta>
            <ta e="T776" id="Seg_8077" s="T775">i</ta>
            <ta e="T777" id="Seg_8078" s="T776">büzʼe</ta>
            <ta e="T779" id="Seg_8079" s="T778">samən</ta>
            <ta e="T780" id="Seg_8080" s="T779">bü-Kən</ta>
            <ta e="T781" id="Seg_8081" s="T780">toʔ-gəndə</ta>
            <ta e="T783" id="Seg_8082" s="T782">nüke</ta>
            <ta e="T784" id="Seg_8083" s="T783">erer-lAʔ</ta>
            <ta e="T785" id="Seg_8084" s="T784">amno-laʔbə-bi</ta>
            <ta e="T787" id="Seg_8085" s="T786">a</ta>
            <ta e="T788" id="Seg_8086" s="T787">büzʼe</ta>
            <ta e="T789" id="Seg_8087" s="T788">mĭn-bi</ta>
            <ta e="T790" id="Seg_8088" s="T789">bü-Tə</ta>
            <ta e="T791" id="Seg_8089" s="T790">kola</ta>
            <ta e="T793" id="Seg_8090" s="T792">dʼabə-zittə</ta>
            <ta e="T795" id="Seg_8091" s="T794">onʼiʔ</ta>
            <ta e="T796" id="Seg_8092" s="T795">raz</ta>
            <ta e="T797" id="Seg_8093" s="T796">onʼiʔ</ta>
            <ta e="T798" id="Seg_8094" s="T797">raz</ta>
            <ta e="T799" id="Seg_8095" s="T798">kan-bi</ta>
            <ta e="T800" id="Seg_8096" s="T799">dĭgəttə</ta>
            <ta e="T801" id="Seg_8097" s="T800">ĭmbi=də</ta>
            <ta e="T802" id="Seg_8098" s="T801">ej</ta>
            <ta e="T803" id="Seg_8099" s="T802">dʼabə-bi</ta>
            <ta e="T804" id="Seg_8100" s="T803">onʼiʔ</ta>
            <ta e="T805" id="Seg_8101" s="T804">riba</ta>
            <ta e="T806" id="Seg_8102" s="T805">dʼabə-bi</ta>
            <ta e="T807" id="Seg_8103" s="T806">kuvas</ta>
            <ta e="T809" id="Seg_8104" s="T808">măn</ta>
            <ta e="T810" id="Seg_8105" s="T809">pi-laʔbə</ta>
            <ta e="T814" id="Seg_8106" s="T813">kola</ta>
            <ta e="T815" id="Seg_8107" s="T814">kuvas</ta>
            <ta e="T816" id="Seg_8108" s="T815">tʼăbaktər-liA</ta>
            <ta e="T817" id="Seg_8109" s="T816">kuza-Tə</ta>
            <ta e="T819" id="Seg_8110" s="T818">šĭkə-t-ziʔ</ta>
            <ta e="T821" id="Seg_8111" s="T820">öʔ-lAʔ</ta>
            <ta e="T822" id="Seg_8112" s="T821">măna</ta>
            <ta e="T823" id="Seg_8113" s="T822">bü-Tə</ta>
            <ta e="T825" id="Seg_8114" s="T824">măn</ta>
            <ta e="T826" id="Seg_8115" s="T825">öʔlu-bi-m</ta>
            <ta e="T828" id="Seg_8116" s="T827">šo-bi</ta>
            <ta e="T829" id="Seg_8117" s="T828">maʔ-gəndə</ta>
            <ta e="T830" id="Seg_8118" s="T829">da</ta>
            <ta e="T831" id="Seg_8119" s="T830">nüke-Tə</ta>
            <ta e="T832" id="Seg_8120" s="T831">tʼăbaktər-laʔbə</ta>
            <ta e="T834" id="Seg_8121" s="T833">kola</ta>
            <ta e="T835" id="Seg_8122" s="T834">bar</ta>
            <ta e="T836" id="Seg_8123" s="T835">kuvas</ta>
            <ta e="T837" id="Seg_8124" s="T836">i-bi</ta>
            <ta e="T838" id="Seg_8125" s="T837">tʼăbaktər-bi</ta>
            <ta e="T839" id="Seg_8126" s="T838">kak</ta>
            <ta e="T840" id="Seg_8127" s="T839">kuza</ta>
            <ta e="T844" id="Seg_8128" s="T843">dĭgəttə</ta>
            <ta e="T845" id="Seg_8129" s="T844">dĭ</ta>
            <ta e="T846" id="Seg_8130" s="T845">nüke</ta>
            <ta e="T847" id="Seg_8131" s="T846">davaj</ta>
            <ta e="T848" id="Seg_8132" s="T847">kudonz-zittə</ta>
            <ta e="T850" id="Seg_8133" s="T849">ĭmbi</ta>
            <ta e="T851" id="Seg_8134" s="T850">tăn</ta>
            <ta e="T852" id="Seg_8135" s="T851">ĭmbi=də</ta>
            <ta e="T853" id="Seg_8136" s="T852">ej</ta>
            <ta e="T854" id="Seg_8137" s="T853">pi-bi-l</ta>
            <ta e="T856" id="Seg_8138" s="T855">karɨta</ta>
            <ta e="T857" id="Seg_8139" s="T856">miʔnʼibeʔ</ta>
            <ta e="T858" id="Seg_8140" s="T857">naga</ta>
            <ta e="T860" id="Seg_8141" s="T859">ši-zəbi</ta>
            <ta e="T862" id="Seg_8142" s="T861">karɨta</ta>
            <ta e="T864" id="Seg_8143" s="T863">dĭ</ta>
            <ta e="T865" id="Seg_8144" s="T864">kan-bi</ta>
            <ta e="T866" id="Seg_8145" s="T865">bazoʔ</ta>
            <ta e="T867" id="Seg_8146" s="T866">bü-Tə</ta>
            <ta e="T869" id="Seg_8147" s="T868">dĭgəttə</ta>
            <ta e="T870" id="Seg_8148" s="T869">davaj</ta>
            <ta e="T871" id="Seg_8149" s="T870">kirgaːr-zittə</ta>
            <ta e="T872" id="Seg_8150" s="T871">kola</ta>
            <ta e="T873" id="Seg_8151" s="T872">šo-ʔ</ta>
            <ta e="T874" id="Seg_8152" s="T873">döbər</ta>
            <ta e="T875" id="Seg_8153" s="T874">dĭ</ta>
            <ta e="T876" id="Seg_8154" s="T875">šo-bi</ta>
            <ta e="T877" id="Seg_8155" s="T876">ĭmbi</ta>
            <ta e="T878" id="Seg_8156" s="T877">tănan</ta>
            <ta e="T879" id="Seg_8157" s="T878">büzʼe</ta>
            <ta e="T880" id="Seg_8158" s="T879">kereʔ</ta>
            <ta e="T882" id="Seg_8159" s="T881">măna</ta>
            <ta e="T883" id="Seg_8160" s="T882">kereʔ</ta>
            <ta e="T884" id="Seg_8161" s="T883">karɨta</ta>
            <ta e="T886" id="Seg_8162" s="T885">nüke-m</ta>
            <ta e="T887" id="Seg_8163" s="T886">bar</ta>
            <ta e="T888" id="Seg_8164" s="T887">kudo-nzə-laʔbə</ta>
            <ta e="T889" id="Seg_8165" s="T888">nu</ta>
            <ta e="T890" id="Seg_8166" s="T889">kan-ə-ʔ</ta>
            <ta e="T891" id="Seg_8167" s="T890">maʔ-Tə</ta>
            <ta e="T895" id="Seg_8168" s="T894">dĭ</ta>
            <ta e="T896" id="Seg_8169" s="T895">nüke</ta>
            <ta e="T897" id="Seg_8170" s="T896">davaj</ta>
            <ta e="T898" id="Seg_8171" s="T897">dĭ-m</ta>
            <ta e="T899" id="Seg_8172" s="T898">kudonz-zittə</ta>
            <ta e="T900" id="Seg_8173" s="T899">ĭmbi</ta>
            <ta e="T901" id="Seg_8174" s="T900">ej</ta>
            <ta e="T902" id="Seg_8175" s="T901">pi-bi-l</ta>
            <ta e="T903" id="Seg_8176" s="T902">tura</ta>
            <ta e="T905" id="Seg_8177" s="T904">miʔ</ta>
            <ta e="T906" id="Seg_8178" s="T905">tura</ta>
            <ta e="T907" id="Seg_8179" s="T906">ej</ta>
            <ta e="T908" id="Seg_8180" s="T907">kuvas</ta>
            <ta e="T910" id="Seg_8181" s="T909">dĭ</ta>
            <ta e="T911" id="Seg_8182" s="T910">bazoʔ</ta>
            <ta e="T912" id="Seg_8183" s="T911">kan-bi</ta>
            <ta e="T913" id="Seg_8184" s="T912">davaj</ta>
            <ta e="T914" id="Seg_8185" s="T913">kirgaːr-zittə</ta>
            <ta e="T915" id="Seg_8186" s="T914">kola</ta>
            <ta e="T916" id="Seg_8187" s="T915">kola</ta>
            <ta e="T917" id="Seg_8188" s="T916">šo-ʔ</ta>
            <ta e="T918" id="Seg_8189" s="T917">döbər</ta>
            <ta e="T920" id="Seg_8190" s="T919">kola</ta>
            <ta e="T921" id="Seg_8191" s="T920">šo-bi</ta>
            <ta e="T922" id="Seg_8192" s="T921">ĭmbi</ta>
            <ta e="T923" id="Seg_8193" s="T922">tănan</ta>
            <ta e="T924" id="Seg_8194" s="T923">kereʔ</ta>
            <ta e="T926" id="Seg_8195" s="T925">tura</ta>
            <ta e="T927" id="Seg_8196" s="T926">kereʔ</ta>
            <ta e="T929" id="Seg_8197" s="T928">miʔ</ta>
            <ta e="T930" id="Seg_8198" s="T929">tura-bAʔ</ta>
            <ta e="T931" id="Seg_8199" s="T930">ej</ta>
            <ta e="T932" id="Seg_8200" s="T931">kuvas</ta>
            <ta e="T934" id="Seg_8201" s="T933">kan-ə-ʔ</ta>
            <ta e="T935" id="Seg_8202" s="T934">maʔ-Tə-l</ta>
            <ta e="T939" id="Seg_8203" s="T938">dĭgəttə</ta>
            <ta e="T940" id="Seg_8204" s="T939">bazoʔ</ta>
            <ta e="T941" id="Seg_8205" s="T940">dĭ</ta>
            <ta e="T942" id="Seg_8206" s="T941">nüke</ta>
            <ta e="T944" id="Seg_8207" s="T942">kudo-nzə-bi</ta>
            <ta e="T945" id="Seg_8208" s="T944">kan-ə-ʔ</ta>
            <ta e="T946" id="Seg_8209" s="T945">kola-Tə</ta>
            <ta e="T947" id="Seg_8210" s="T946">pi-ʔ</ta>
            <ta e="T948" id="Seg_8211" s="T947">štobɨ</ta>
            <ta e="T949" id="Seg_8212" s="T948">măn</ta>
            <ta e="T950" id="Seg_8213" s="T949">mo-bi-m</ta>
            <ta e="T951" id="Seg_8214" s="T950">kupčixăj-ziʔ</ta>
            <ta e="T955" id="Seg_8215" s="T954">dĭgəttə</ta>
            <ta e="T956" id="Seg_8216" s="T955">kola</ta>
            <ta e="T957" id="Seg_8217" s="T956">măn-bi</ta>
            <ta e="T958" id="Seg_8218" s="T957">kan-ə-ʔ</ta>
            <ta e="T959" id="Seg_8219" s="T958">maʔ-Tə-l</ta>
            <ta e="T961" id="Seg_8220" s="T960">dĭ</ta>
            <ta e="T962" id="Seg_8221" s="T961">šo-bi</ta>
            <ta e="T963" id="Seg_8222" s="T962">a</ta>
            <ta e="T964" id="Seg_8223" s="T963">dĭ</ta>
            <ta e="T965" id="Seg_8224" s="T964">nüke</ta>
            <ta e="T966" id="Seg_8225" s="T965">bazoʔ</ta>
            <ta e="T968" id="Seg_8226" s="T966">kudo-nzə-laʔbə</ta>
            <ta e="T969" id="Seg_8227" s="T968">kan-ə-ʔ</ta>
            <ta e="T970" id="Seg_8228" s="T969">kola-Tə</ta>
            <ta e="T971" id="Seg_8229" s="T970">pi-ʔ</ta>
            <ta e="T972" id="Seg_8230" s="T971">štobɨ</ta>
            <ta e="T973" id="Seg_8231" s="T972">măn</ta>
            <ta e="T974" id="Seg_8232" s="T973">i-bi-m</ta>
            <ta e="T975" id="Seg_8233" s="T974">dvărʼankaj-ziʔ</ta>
            <ta e="T977" id="Seg_8234" s="T976">dĭ</ta>
            <ta e="T978" id="Seg_8235" s="T977">kan-bi</ta>
            <ta e="T979" id="Seg_8236" s="T978">kola-Tə</ta>
            <ta e="T980" id="Seg_8237" s="T979">kola</ta>
            <ta e="T981" id="Seg_8238" s="T980">kirgaːr-laʔpi</ta>
            <ta e="T983" id="Seg_8239" s="T982">kola</ta>
            <ta e="T984" id="Seg_8240" s="T983">šo-bi</ta>
            <ta e="T985" id="Seg_8241" s="T984">ĭmbi</ta>
            <ta e="T986" id="Seg_8242" s="T985">tănan</ta>
            <ta e="T987" id="Seg_8243" s="T986">kereʔ</ta>
            <ta e="T989" id="Seg_8244" s="T988">măn</ta>
            <ta e="T990" id="Seg_8245" s="T989">nüke-m</ta>
            <ta e="T991" id="Seg_8246" s="T990">măn-ntə</ta>
            <ta e="T992" id="Seg_8247" s="T991">štobɨ</ta>
            <ta e="T993" id="Seg_8248" s="T992">măn</ta>
            <ta e="T994" id="Seg_8249" s="T993">dvărʼankaj-ziʔ</ta>
            <ta e="T995" id="Seg_8250" s="T994">i-bi-m</ta>
            <ta e="T997" id="Seg_8251" s="T996">kan-ə-ʔ</ta>
            <ta e="T998" id="Seg_8252" s="T997">maʔ-Tə-l</ta>
            <ta e="T1002" id="Seg_8253" s="T1001">dĭgəttə</ta>
            <ta e="T1003" id="Seg_8254" s="T1002">büzʼe</ta>
            <ta e="T1004" id="Seg_8255" s="T1003">šo-bi</ta>
            <ta e="T1005" id="Seg_8256" s="T1004">maʔ-gəndə</ta>
            <ta e="T1006" id="Seg_8257" s="T1005">dĭ</ta>
            <ta e="T1007" id="Seg_8258" s="T1006">davaj</ta>
            <ta e="T1008" id="Seg_8259" s="T1007">bazoʔ</ta>
            <ta e="T1009" id="Seg_8260" s="T1008">kudonz-zittə</ta>
            <ta e="T1013" id="Seg_8261" s="T1012">i</ta>
            <ta e="T1014" id="Seg_8262" s="T1013">münör-zittə</ta>
            <ta e="T1015" id="Seg_8263" s="T1014">xatʼel</ta>
            <ta e="T1017" id="Seg_8264" s="T1016">kan-ə-ʔ</ta>
            <ta e="T1018" id="Seg_8265" s="T1017">bazoʔ</ta>
            <ta e="T1019" id="Seg_8266" s="T1018">kola-Tə</ta>
            <ta e="T1020" id="Seg_8267" s="T1019">pi-ʔ</ta>
            <ta e="T1021" id="Seg_8268" s="T1020">štobɨ</ta>
            <ta e="T1022" id="Seg_8269" s="T1021">măn</ta>
            <ta e="T1027" id="Seg_8270" s="T1026">štobɨ</ta>
            <ta e="T1028" id="Seg_8271" s="T1027">il</ta>
            <ta e="T1029" id="Seg_8272" s="T1028">măna</ta>
            <ta e="T1030" id="Seg_8273" s="T1029">uda-zAŋ-də</ta>
            <ta e="T1035" id="Seg_8274" s="T1034">dĭgəttə</ta>
            <ta e="T1036" id="Seg_8275" s="T1035">dĭ</ta>
            <ta e="T1037" id="Seg_8276" s="T1036">šo-bi</ta>
            <ta e="T1038" id="Seg_8277" s="T1037">bü-Tə</ta>
            <ta e="T1040" id="Seg_8278" s="T1039">kola</ta>
            <ta e="T1041" id="Seg_8279" s="T1040">kola</ta>
            <ta e="T1042" id="Seg_8280" s="T1041">šo-ʔ</ta>
            <ta e="T1043" id="Seg_8281" s="T1042">döbər</ta>
            <ta e="T1044" id="Seg_8282" s="T1043">kola</ta>
            <ta e="T1045" id="Seg_8283" s="T1044">šo-bi</ta>
            <ta e="T1047" id="Seg_8284" s="T1046">ĭmbi</ta>
            <ta e="T1048" id="Seg_8285" s="T1047">tănan</ta>
            <ta e="T1049" id="Seg_8286" s="T1048">kereʔ</ta>
            <ta e="T1050" id="Seg_8287" s="T1049">büzʼe</ta>
            <ta e="T1051" id="Seg_8288" s="T1050">da</ta>
            <ta e="T1052" id="Seg_8289" s="T1051">măn</ta>
            <ta e="T1053" id="Seg_8290" s="T1052">nüke-m</ta>
            <ta e="T1055" id="Seg_8291" s="T1054">măn-ntə</ta>
            <ta e="T1056" id="Seg_8292" s="T1055">štobɨ</ta>
            <ta e="T1057" id="Seg_8293" s="T1056">măn</ta>
            <ta e="T1059" id="Seg_8294" s="T1058">štobɨ</ta>
            <ta e="T1060" id="Seg_8295" s="T1059">măn</ta>
            <ta e="T1061" id="Seg_8296" s="T1060">i-bi-m</ta>
            <ta e="T1062" id="Seg_8297" s="T1061">tsaritsa-ziʔ</ta>
            <ta e="T1064" id="Seg_8298" s="T1063">štobɨ</ta>
            <ta e="T1065" id="Seg_8299" s="T1064">măna</ta>
            <ta e="T1066" id="Seg_8300" s="T1065">il</ta>
            <ta e="T1067" id="Seg_8301" s="T1066">uda-t-ziʔ</ta>
            <ta e="T1068" id="Seg_8302" s="T1067">kun-laːm-bi-jəʔ</ta>
            <ta e="T1070" id="Seg_8303" s="T1069">kan-ə-ʔ</ta>
            <ta e="T1071" id="Seg_8304" s="T1070">maʔ-Tə-l</ta>
            <ta e="T1072" id="Seg_8305" s="T1071">bar</ta>
            <ta e="T1073" id="Seg_8306" s="T1072">mo-lV-j</ta>
            <ta e="T1074" id="Seg_8307" s="T1073">tănan</ta>
            <ta e="T1078" id="Seg_8308" s="T1077">dĭgəttə</ta>
            <ta e="T1079" id="Seg_8309" s="T1078">büzʼe</ta>
            <ta e="T1080" id="Seg_8310" s="T1079">kan-bi</ta>
            <ta e="T1081" id="Seg_8311" s="T1080">bü-Tə</ta>
            <ta e="T1083" id="Seg_8312" s="T1082">kola-jəʔ</ta>
            <ta e="T1084" id="Seg_8313" s="T1083">kola-jəʔ</ta>
            <ta e="T1085" id="Seg_8314" s="T1084">šo-ʔ</ta>
            <ta e="T1086" id="Seg_8315" s="T1085">döbər</ta>
            <ta e="T1087" id="Seg_8316" s="T1086">kola</ta>
            <ta e="T1088" id="Seg_8317" s="T1087">šo-bi</ta>
            <ta e="T1089" id="Seg_8318" s="T1088">ĭmbi</ta>
            <ta e="T1090" id="Seg_8319" s="T1089">tănan</ta>
            <ta e="T1091" id="Seg_8320" s="T1090">kereʔ</ta>
            <ta e="T1093" id="Seg_8321" s="T1092">da</ta>
            <ta e="T1094" id="Seg_8322" s="T1093">măn</ta>
            <ta e="T1095" id="Seg_8323" s="T1094">nüke-m</ta>
            <ta e="T1096" id="Seg_8324" s="T1095">măn-ntə</ta>
            <ta e="T1097" id="Seg_8325" s="T1096">štobɨ</ta>
            <ta e="T1098" id="Seg_8326" s="T1097">măn</ta>
            <ta e="T1099" id="Seg_8327" s="T1098">i-bi-m</ta>
            <ta e="T1103" id="Seg_8328" s="T1102">i</ta>
            <ta e="T1104" id="Seg_8329" s="T1103">kola</ta>
            <ta e="T1105" id="Seg_8330" s="T1104">štobɨ</ta>
            <ta e="T1106" id="Seg_8331" s="T1105">măna</ta>
            <ta e="T1107" id="Seg_8332" s="T1106">tažor-bi</ta>
            <ta e="T1108" id="Seg_8333" s="T1107">bar</ta>
            <ta e="T1109" id="Seg_8334" s="T1108">ĭmbi</ta>
            <ta e="T1111" id="Seg_8335" s="T1110">dĭgəttə</ta>
            <ta e="T1112" id="Seg_8336" s="T1111">kola</ta>
            <ta e="T1113" id="Seg_8337" s="T1112">kan-bi</ta>
            <ta e="T1114" id="Seg_8338" s="T1113">bü-Tə</ta>
            <ta e="T1115" id="Seg_8339" s="T1114">ĭmbi</ta>
            <ta e="T1117" id="Seg_8340" s="T1116">ĭmbi=də</ta>
            <ta e="T1119" id="Seg_8341" s="T1118">büzʼe-Tə</ta>
            <ta e="T1120" id="Seg_8342" s="T1119">ej</ta>
            <ta e="T1121" id="Seg_8343" s="T1120">nörbə-bi</ta>
            <ta e="T1123" id="Seg_8344" s="T1122">dĭ</ta>
            <ta e="T1124" id="Seg_8345" s="T1123">šo-bi</ta>
            <ta e="T1125" id="Seg_8346" s="T1124">maʔ-gəndə</ta>
            <ta e="T1127" id="Seg_8347" s="T1126">nüke</ta>
            <ta e="T1128" id="Seg_8348" s="T1127">amno-laʔbə</ta>
            <ta e="T1129" id="Seg_8349" s="T1128">erer-laʔbə</ta>
            <ta e="T1130" id="Seg_8350" s="T1129">i</ta>
            <ta e="T1131" id="Seg_8351" s="T1130">karɨta</ta>
            <ta e="T1132" id="Seg_8352" s="T1131">karɨta-t</ta>
            <ta e="T1133" id="Seg_8353" s="T1132">nu-laʔbə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_8354" s="T1">two.[NOM.SG]</ta>
            <ta e="T3" id="Seg_8355" s="T2">sheep-NOM/GEN/ACC.3SG</ta>
            <ta e="T4" id="Seg_8356" s="T3">and</ta>
            <ta e="T5" id="Seg_8357" s="T4">woman.[NOM.SG]</ta>
            <ta e="T7" id="Seg_8358" s="T6">two.[NOM.SG]</ta>
            <ta e="T8" id="Seg_8359" s="T7">girl.[NOM.SG]</ta>
            <ta e="T9" id="Seg_8360" s="T8">be-PST.[3SG]</ta>
            <ta e="T10" id="Seg_8361" s="T9">two.[NOM.SG]</ta>
            <ta e="T11" id="Seg_8362" s="T10">girl.[NOM.SG]</ta>
            <ta e="T12" id="Seg_8363" s="T11">be-PST.[3SG]</ta>
            <ta e="T13" id="Seg_8364" s="T12">then</ta>
            <ta e="T15" id="Seg_8365" s="T14">sheep.[NOM.SG]</ta>
            <ta e="T16" id="Seg_8366" s="T15">herd.[NOM.SG]</ta>
            <ta e="T17" id="Seg_8367" s="T16">be-PST.[3SG]</ta>
            <ta e="T18" id="Seg_8368" s="T17">then</ta>
            <ta e="T19" id="Seg_8369" s="T18">herd.[NOM.SG]</ta>
            <ta e="T20" id="Seg_8370" s="T19">horse.[NOM.SG]</ta>
            <ta e="T21" id="Seg_8371" s="T20">be-PST-3PL</ta>
            <ta e="T22" id="Seg_8372" s="T21">then</ta>
            <ta e="T23" id="Seg_8373" s="T22">herd.[NOM.SG]</ta>
            <ta e="T24" id="Seg_8374" s="T23">cow.[NOM.SG]</ta>
            <ta e="T25" id="Seg_8375" s="T24">be-PST-3PL</ta>
            <ta e="T27" id="Seg_8376" s="T26">then</ta>
            <ta e="T28" id="Seg_8377" s="T27">one.[NOM.SG]</ta>
            <ta e="T29" id="Seg_8378" s="T28">look-FRQ-PST.[3SG]</ta>
            <ta e="T30" id="Seg_8379" s="T29">and</ta>
            <ta e="T31" id="Seg_8380" s="T30">one.[NOM.SG]</ta>
            <ta e="T32" id="Seg_8381" s="T31">PTCL</ta>
            <ta e="T34" id="Seg_8382" s="T33">then</ta>
            <ta e="T35" id="Seg_8383" s="T34">one.[NOM.SG]</ta>
            <ta e="T36" id="Seg_8384" s="T35">louse.[NOM.SG]</ta>
            <ta e="T37" id="Seg_8385" s="T36">boil-PST.[3SG]</ta>
            <ta e="T38" id="Seg_8386" s="T37">look-FRQ-PST.[3SG]</ta>
            <ta e="T39" id="Seg_8387" s="T38">there</ta>
            <ta e="T40" id="Seg_8388" s="T39">Kodur-LOC</ta>
            <ta e="T41" id="Seg_8389" s="T40">and</ta>
            <ta e="T42" id="Seg_8390" s="T41">one.[NOM.SG]</ta>
            <ta e="T43" id="Seg_8391" s="T42">lead</ta>
            <ta e="T44" id="Seg_8392" s="T43">boil-PST.[3SG]</ta>
            <ta e="T45" id="Seg_8393" s="T44">then</ta>
            <ta e="T46" id="Seg_8394" s="T45">bring-PST.[3SG]</ta>
            <ta e="T47" id="Seg_8395" s="T46">and</ta>
            <ta e="T48" id="Seg_8396" s="T47">ear-LAT</ta>
            <ta e="T49" id="Seg_8397" s="T48">pour-PST.[3SG]</ta>
            <ta e="T52" id="Seg_8398" s="T51">ear-LAT</ta>
            <ta e="T53" id="Seg_8399" s="T52">pour-PST.[3SG]</ta>
            <ta e="T54" id="Seg_8400" s="T53">and</ta>
            <ta e="T55" id="Seg_8401" s="T54">this.[NOM.SG]</ta>
            <ta e="T56" id="Seg_8402" s="T55">PTCL</ta>
            <ta e="T57" id="Seg_8403" s="T56">die-RES-PST.[3SG]</ta>
            <ta e="T59" id="Seg_8404" s="T58">then</ta>
            <ta e="T60" id="Seg_8405" s="T59">this-GEN</ta>
            <ta e="T61" id="Seg_8406" s="T60">song.[NOM.SG]</ta>
            <ta e="T62" id="Seg_8407" s="T61">PTCL</ta>
            <ta e="T63" id="Seg_8408" s="T62">die-RES-PST.[3SG]</ta>
            <ta e="T64" id="Seg_8409" s="T63">then</ta>
            <ta e="T65" id="Seg_8410" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_8411" s="T65">daughter-PL</ta>
            <ta e="T67" id="Seg_8412" s="T66">take-PST-3PL</ta>
            <ta e="T68" id="Seg_8413" s="T67">sheep.[NOM.SG]</ta>
            <ta e="T69" id="Seg_8414" s="T68">horse-PL</ta>
            <ta e="T70" id="Seg_8415" s="T69">and</ta>
            <ta e="T71" id="Seg_8416" s="T70">cow-PL</ta>
            <ta e="T72" id="Seg_8417" s="T71">and</ta>
            <ta e="T73" id="Seg_8418" s="T72">father-LAT/LOC.3SG</ta>
            <ta e="T1134" id="Seg_8419" s="T74">go-CVB</ta>
            <ta e="T75" id="Seg_8420" s="T1134">disappear-PST-3PL</ta>
            <ta e="T77" id="Seg_8421" s="T76">then</ta>
            <ta e="T78" id="Seg_8422" s="T77">all</ta>
            <ta e="T79" id="Seg_8423" s="T78">tale.[NOM.SG]</ta>
            <ta e="T81" id="Seg_8424" s="T80">mouse.[NOM.SG]</ta>
            <ta e="T82" id="Seg_8425" s="T81">and</ta>
            <ta e="T83" id="Seg_8426" s="T82">coal.[NOM.SG]</ta>
            <ta e="T84" id="Seg_8427" s="T83">and</ta>
            <ta e="T85" id="Seg_8428" s="T84">bladder.[NOM.SG]</ta>
            <ta e="T86" id="Seg_8429" s="T85">go-PST-3PL</ta>
            <ta e="T88" id="Seg_8430" s="T87">river.[NOM.SG]</ta>
            <ta e="T89" id="Seg_8431" s="T88">and</ta>
            <ta e="T90" id="Seg_8432" s="T89">this.[NOM.SG]</ta>
            <ta e="T91" id="Seg_8433" s="T90">go-INF.LAT</ta>
            <ta e="T92" id="Seg_8434" s="T91">river.[NOM.SG]</ta>
            <ta e="T93" id="Seg_8435" s="T92">river.[NOM.SG]</ta>
            <ta e="T94" id="Seg_8436" s="T93">flow-DUR.[3SG]</ta>
            <ta e="T95" id="Seg_8437" s="T94">mouse.[NOM.SG]</ta>
            <ta e="T96" id="Seg_8438" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_8439" s="T96">branch-PL</ta>
            <ta e="T98" id="Seg_8440" s="T97">put-PST.[3SG]</ta>
            <ta e="T99" id="Seg_8441" s="T98">then</ta>
            <ta e="T100" id="Seg_8442" s="T99">mouse.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_8443" s="T100">go-CVB</ta>
            <ta e="T101" id="Seg_8444" s="T1135">disappear-PST.[3SG]</ta>
            <ta e="T102" id="Seg_8445" s="T101">bladder.[NOM.SG]</ta>
            <ta e="T1136" id="Seg_8446" s="T102">go-CVB</ta>
            <ta e="T103" id="Seg_8447" s="T1136">disappear-PST.[3SG]</ta>
            <ta e="T105" id="Seg_8448" s="T104">and</ta>
            <ta e="T106" id="Seg_8449" s="T105">coal</ta>
            <ta e="T107" id="Seg_8450" s="T106">go</ta>
            <ta e="T108" id="Seg_8451" s="T107">coal.[NOM.SG]</ta>
            <ta e="T109" id="Seg_8452" s="T108">go-PST.[3SG]</ta>
            <ta e="T110" id="Seg_8453" s="T109">and</ta>
            <ta e="T111" id="Seg_8454" s="T110">water-LAT</ta>
            <ta e="T112" id="Seg_8455" s="T111">fall-MOM-PST.[3SG]</ta>
            <ta e="T113" id="Seg_8456" s="T112">and</ta>
            <ta e="T114" id="Seg_8457" s="T113">die-RES-PST.[3SG]</ta>
            <ta e="T116" id="Seg_8458" s="T115">and</ta>
            <ta e="T117" id="Seg_8459" s="T116">bladder.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8460" s="T117">laugh-PST.[3SG]</ta>
            <ta e="T119" id="Seg_8461" s="T118">laugh-PST.[3SG]</ta>
            <ta e="T120" id="Seg_8462" s="T119">and</ta>
            <ta e="T121" id="Seg_8463" s="T120">die-RES-PST.[3SG]</ta>
            <ta e="T123" id="Seg_8464" s="T122">mouse.[NOM.SG]</ta>
            <ta e="T124" id="Seg_8465" s="T123">alone</ta>
            <ta e="T126" id="Seg_8466" s="T125">remain-MOM-PST.[3SG]</ta>
            <ta e="T127" id="Seg_8467" s="T126">then</ta>
            <ta e="T128" id="Seg_8468" s="T127">go-PST.[3SG]</ta>
            <ta e="T129" id="Seg_8469" s="T128">go-PST.[3SG]</ta>
            <ta e="T130" id="Seg_8470" s="T129">child-PL</ta>
            <ta e="T131" id="Seg_8471" s="T130">PTCL</ta>
            <ta e="T132" id="Seg_8472" s="T131">play-DUR-3PL</ta>
            <ta e="T134" id="Seg_8473" s="T133">water.[NOM.SG]</ta>
            <ta e="T135" id="Seg_8474" s="T134">take.away-RES-PST.[3SG]</ta>
            <ta e="T136" id="Seg_8475" s="T135">branch.[NOM.SG]</ta>
            <ta e="T137" id="Seg_8476" s="T136">water-LOC</ta>
            <ta e="T139" id="Seg_8477" s="T138">child-PL</ta>
            <ta e="T140" id="Seg_8478" s="T139">mother</ta>
            <ta e="T141" id="Seg_8479" s="T140">father-NOM/GEN.3SG</ta>
            <ta e="T143" id="Seg_8480" s="T142">mother-GEN</ta>
            <ta e="T144" id="Seg_8481" s="T143">go-IMP.2PL</ta>
            <ta e="T145" id="Seg_8482" s="T144">tent-LAT</ta>
            <ta e="T146" id="Seg_8483" s="T145">and</ta>
            <ta e="T147" id="Seg_8484" s="T146">say-IPFVZ-CVB</ta>
            <ta e="T148" id="Seg_8485" s="T147">say-EP-IMP.2SG</ta>
            <ta e="T149" id="Seg_8486" s="T148">mother-LAT</ta>
            <ta e="T151" id="Seg_8487" s="T150">this-PL</ta>
            <ta e="T152" id="Seg_8488" s="T151">run-CVB</ta>
            <ta e="T153" id="Seg_8489" s="T152">come-PST-3PL</ta>
            <ta e="T155" id="Seg_8490" s="T154">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T156" id="Seg_8491" s="T155">come-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_8492" s="T157">well</ta>
            <ta e="T159" id="Seg_8493" s="T158">go-IMP.2PL</ta>
            <ta e="T160" id="Seg_8494" s="T159">call-IMP.2PL</ta>
            <ta e="T161" id="Seg_8495" s="T160">I.NOM</ta>
            <ta e="T162" id="Seg_8496" s="T161">soon</ta>
            <ta e="T163" id="Seg_8497" s="T162">meat.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8498" s="T163">boil-FUT-1SG</ta>
            <ta e="T166" id="Seg_8499" s="T165">this-PL</ta>
            <ta e="T167" id="Seg_8500" s="T166">run-MOM-PST-3PL</ta>
            <ta e="T168" id="Seg_8501" s="T167">come-IMP.2SG</ta>
            <ta e="T169" id="Seg_8502" s="T168">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T170" id="Seg_8503" s="T169">meat-ACC</ta>
            <ta e="T172" id="Seg_8504" s="T171">boil-DUR-3PL</ta>
            <ta e="T174" id="Seg_8505" s="T173">foot-NOM/GEN/ACC.3PL</ta>
            <ta e="T176" id="Seg_8506" s="T175">I.NOM</ta>
            <ta e="T177" id="Seg_8507" s="T176">this-ACC</ta>
            <ta e="T178" id="Seg_8508" s="T177">NEG.AUX-1SG</ta>
            <ta e="T180" id="Seg_8509" s="T179">NEG.EX.[3SG]</ta>
            <ta e="T181" id="Seg_8510" s="T180">eat-INF.LAT</ta>
            <ta e="T182" id="Seg_8511" s="T181">JUSS</ta>
            <ta e="T183" id="Seg_8512" s="T182">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_8513" s="T183">cut-FUT-3SG</ta>
            <ta e="T185" id="Seg_8514" s="T184">and</ta>
            <ta e="T186" id="Seg_8515" s="T185">boil-FUT-3SG</ta>
            <ta e="T187" id="Seg_8516" s="T186">skin-LAT</ta>
            <ta e="T188" id="Seg_8517" s="T187">place-FUT-3SG</ta>
            <ta e="T189" id="Seg_8518" s="T188">this.[NOM.SG]</ta>
            <ta e="T190" id="Seg_8519" s="T189">cow.[NOM.SG]</ta>
            <ta e="T191" id="Seg_8520" s="T190">cut-PST.[3SG]</ta>
            <ta e="T193" id="Seg_8521" s="T192">meat.[NOM.SG]</ta>
            <ta e="T194" id="Seg_8522" s="T193">boil-PST.[3SG]</ta>
            <ta e="T195" id="Seg_8523" s="T194">skin-LAT</ta>
            <ta e="T196" id="Seg_8524" s="T195">put-PST.[3SG]</ta>
            <ta e="T198" id="Seg_8525" s="T197">then</ta>
            <ta e="T199" id="Seg_8526" s="T198">child-PL</ta>
            <ta e="T200" id="Seg_8527" s="T199">go-PST-3PL</ta>
            <ta e="T201" id="Seg_8528" s="T200">come-IMP.2SG</ta>
            <ta e="T202" id="Seg_8529" s="T201">meat.[NOM.SG]</ta>
            <ta e="T203" id="Seg_8530" s="T202">boil-TR-DETR-PTCP.[NOM.SG]</ta>
            <ta e="T205" id="Seg_8531" s="T204">come-PST-3PL</ta>
            <ta e="T207" id="Seg_8532" s="T206">where</ta>
            <ta e="T208" id="Seg_8533" s="T207">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T210" id="Seg_8534" s="T209">INCH</ta>
            <ta e="T211" id="Seg_8535" s="T210">this-ACC.PL</ta>
            <ta e="T212" id="Seg_8536" s="T211">beat-INF.LAT</ta>
            <ta e="T213" id="Seg_8537" s="T212">child-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T215" id="Seg_8538" s="T214">and</ta>
            <ta e="T216" id="Seg_8539" s="T215">mouse.[NOM.SG]</ta>
            <ta e="T217" id="Seg_8540" s="T216">sit-DUR.[3SG]</ta>
            <ta e="T218" id="Seg_8541" s="T217">NEG.AUX-IMP.2SG</ta>
            <ta e="T219" id="Seg_8542" s="T218">fight-CNG</ta>
            <ta e="T220" id="Seg_8543" s="T219">I.NOM</ta>
            <ta e="T221" id="Seg_8544" s="T220">here</ta>
            <ta e="T222" id="Seg_8545" s="T221">come-PST-1SG</ta>
            <ta e="T224" id="Seg_8546" s="T223">today</ta>
            <ta e="T225" id="Seg_8547" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_8548" s="T225">snow.[NOM.SG]</ta>
            <ta e="T227" id="Seg_8549" s="T226">fall-DUR.[3SG]</ta>
            <ta e="T229" id="Seg_8550" s="T228">and</ta>
            <ta e="T230" id="Seg_8551" s="T229">wind.[NOM.SG]</ta>
            <ta e="T231" id="Seg_8552" s="T230">come-PRS.[3SG]</ta>
            <ta e="T233" id="Seg_8553" s="T232">very</ta>
            <ta e="T234" id="Seg_8554" s="T233">cold.[NOM.SG]</ta>
            <ta e="T235" id="Seg_8555" s="T234">outside</ta>
            <ta e="T237" id="Seg_8556" s="T236">outside</ta>
            <ta e="T239" id="Seg_8557" s="T238">how.much</ta>
            <ta e="T240" id="Seg_8558" s="T239">winter.[NOM.SG]</ta>
            <ta e="T241" id="Seg_8559" s="T240">child-LAT</ta>
            <ta e="T243" id="Seg_8560" s="T242">three.[NOM.SG]</ta>
            <ta e="T245" id="Seg_8561" s="T244">that.[NOM.SG]</ta>
            <ta e="T246" id="Seg_8562" s="T245">winter-LOC.ADV</ta>
            <ta e="T247" id="Seg_8563" s="T246">very</ta>
            <ta e="T248" id="Seg_8564" s="T247">cold.[NOM.SG]</ta>
            <ta e="T249" id="Seg_8565" s="T248">be-PST.[3SG]</ta>
            <ta e="T251" id="Seg_8566" s="T250">freeze-DUR-PST-3PL</ta>
            <ta e="T252" id="Seg_8567" s="T251">people.[NOM.SG]</ta>
            <ta e="T253" id="Seg_8568" s="T252">PTCL</ta>
            <ta e="T257" id="Seg_8569" s="T256">grass.[NOM.SG]</ta>
            <ta e="T258" id="Seg_8570" s="T257">cut-PST-1SG</ta>
            <ta e="T259" id="Seg_8571" s="T258">scythe-INS</ta>
            <ta e="T261" id="Seg_8572" s="T260">then</ta>
            <ta e="T262" id="Seg_8573" s="T261">dry-RES-PST.[3SG]</ta>
            <ta e="T263" id="Seg_8574" s="T262">grass.[NOM.SG]</ta>
            <ta e="T264" id="Seg_8575" s="T263">I.NOM</ta>
            <ta e="T265" id="Seg_8576" s="T264">this-ACC</ta>
            <ta e="T266" id="Seg_8577" s="T265">collect-PST-1SG</ta>
            <ta e="T267" id="Seg_8578" s="T266">haystack-LAT</ta>
            <ta e="T268" id="Seg_8579" s="T267">then</ta>
            <ta e="T269" id="Seg_8580" s="T268">horse.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8581" s="T269">take-PST-1SG</ta>
            <ta e="T271" id="Seg_8582" s="T270">carry-PST-1SG</ta>
            <ta e="T273" id="Seg_8583" s="T272">put-PST-1SG</ta>
            <ta e="T274" id="Seg_8584" s="T273">zarod-LAT</ta>
            <ta e="T278" id="Seg_8585" s="T277">that.[NOM.SG]</ta>
            <ta e="T279" id="Seg_8586" s="T278">year-LOC.ADV</ta>
            <ta e="T280" id="Seg_8587" s="T279">black.[NOM.SG]</ta>
            <ta e="T281" id="Seg_8588" s="T280">berry.[NOM.SG]</ta>
            <ta e="T282" id="Seg_8589" s="T281">NEG.EX.[3SG]</ta>
            <ta e="T283" id="Seg_8590" s="T282">and</ta>
            <ta e="T284" id="Seg_8591" s="T283">red.[NOM.SG]</ta>
            <ta e="T285" id="Seg_8592" s="T284">berry.[NOM.SG]</ta>
            <ta e="T286" id="Seg_8593" s="T285">be-PST.[3SG]</ta>
            <ta e="T288" id="Seg_8594" s="T287">people.[NOM.SG]</ta>
            <ta e="T289" id="Seg_8595" s="T288">many</ta>
            <ta e="T290" id="Seg_8596" s="T289">carry-PST-3PL</ta>
            <ta e="T291" id="Seg_8597" s="T290">house-LAT/LOC.3SG</ta>
            <ta e="T292" id="Seg_8598" s="T291">berry-PL</ta>
            <ta e="T294" id="Seg_8599" s="T293">pine.nut.[NOM.SG]</ta>
            <ta e="T295" id="Seg_8600" s="T294">many</ta>
            <ta e="T296" id="Seg_8601" s="T295">be-PST.[3SG]</ta>
            <ta e="T297" id="Seg_8602" s="T296">PTCL</ta>
            <ta e="T299" id="Seg_8603" s="T298">people.[NOM.SG]</ta>
            <ta e="T300" id="Seg_8604" s="T299">very</ta>
            <ta e="T301" id="Seg_8605" s="T300">many</ta>
            <ta e="T302" id="Seg_8606" s="T301">collect-PST-3PL</ta>
            <ta e="T303" id="Seg_8607" s="T302">pine.nut.[NOM.SG]</ta>
            <ta e="T307" id="Seg_8608" s="T306">three.[NOM.SG]</ta>
            <ta e="T308" id="Seg_8609" s="T307">year.[NOM.SG]</ta>
            <ta e="T309" id="Seg_8610" s="T308">NEG.EX-PST.[3SG]</ta>
            <ta e="T310" id="Seg_8611" s="T309">PTCL</ta>
            <ta e="T311" id="Seg_8612" s="T310">pine.nut.[NOM.SG]</ta>
            <ta e="T315" id="Seg_8613" s="T314">pine.nut.[NOM.SG]</ta>
            <ta e="T316" id="Seg_8614" s="T315">jolt-MULT-INF.LAT</ta>
            <ta e="T319" id="Seg_8615" s="T318">this-PL</ta>
            <ta e="T320" id="Seg_8616" s="T319">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T321" id="Seg_8617" s="T320">fall-MOM-PST-3PL</ta>
            <ta e="T322" id="Seg_8618" s="T321">and</ta>
            <ta e="T323" id="Seg_8619" s="T322">people.[NOM.SG]</ta>
            <ta e="T324" id="Seg_8620" s="T323">collect-DUR-3PL</ta>
            <ta e="T328" id="Seg_8621" s="T327">that.[NOM.SG]</ta>
            <ta e="T329" id="Seg_8622" s="T328">year-LOC.ADV</ta>
            <ta e="T330" id="Seg_8623" s="T329">bird.cherry.[NOM.SG]</ta>
            <ta e="T331" id="Seg_8624" s="T330">berry.[NOM.SG]</ta>
            <ta e="T332" id="Seg_8625" s="T331">NEG.EX-PST.[3SG]</ta>
            <ta e="T333" id="Seg_8626" s="T332">also</ta>
            <ta e="T337" id="Seg_8627" s="T336">that.[NOM.SG]</ta>
            <ta e="T338" id="Seg_8628" s="T337">winter.[NOM.SG]</ta>
            <ta e="T339" id="Seg_8629" s="T338">very</ta>
            <ta e="T340" id="Seg_8630" s="T339">wheat.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8631" s="T340">good.[NOM.SG]</ta>
            <ta e="T342" id="Seg_8632" s="T341">be-PST.[3SG]</ta>
            <ta e="T344" id="Seg_8633" s="T343">and</ta>
            <ta e="T345" id="Seg_8634" s="T344">potato.[NOM.SG]</ta>
            <ta e="T346" id="Seg_8635" s="T345">good.[NOM.SG]</ta>
            <ta e="T347" id="Seg_8636" s="T346">be-PST.[3SG]</ta>
            <ta e="T348" id="Seg_8637" s="T347">many</ta>
            <ta e="T349" id="Seg_8638" s="T348">grow-PST.[3SG]</ta>
            <ta e="T350" id="Seg_8639" s="T349">big.[NOM.SG]</ta>
            <ta e="T351" id="Seg_8640" s="T350">be-PST.[3SG]</ta>
            <ta e="T355" id="Seg_8641" s="T354">snow.[NOM.SG]</ta>
            <ta e="T356" id="Seg_8642" s="T355">bread.[NOM.SG]</ta>
            <ta e="T358" id="Seg_8643" s="T357">bake-PRS-3PL</ta>
            <ta e="T359" id="Seg_8644" s="T358">people.[NOM.SG]</ta>
            <ta e="T360" id="Seg_8645" s="T359">very</ta>
            <ta e="T362" id="Seg_8646" s="T361">onion.[NOM.SG]</ta>
            <ta e="T363" id="Seg_8647" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_8648" s="T363">die-RES-PST.[3SG]</ta>
            <ta e="T365" id="Seg_8649" s="T364">plant.garden-LOC</ta>
            <ta e="T367" id="Seg_8650" s="T366">and</ta>
            <ta e="T371" id="Seg_8651" s="T370">and</ta>
            <ta e="T372" id="Seg_8652" s="T371">forest-LOC</ta>
            <ta e="T373" id="Seg_8653" s="T372">onion.[NOM.SG]</ta>
            <ta e="T374" id="Seg_8654" s="T373">good.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8655" s="T374">be-PST.[3SG]</ta>
            <ta e="T379" id="Seg_8656" s="T378">I.NOM</ta>
            <ta e="T382" id="Seg_8657" s="T381">tear-PST-1SG</ta>
            <ta e="T383" id="Seg_8658" s="T382">stab-PST-1SG</ta>
            <ta e="T384" id="Seg_8659" s="T383">and</ta>
            <ta e="T385" id="Seg_8660" s="T384">dry-DUR-PST.[3SG]</ta>
            <ta e="T387" id="Seg_8661" s="T386">and</ta>
            <ta e="T388" id="Seg_8662" s="T387">now</ta>
            <ta e="T389" id="Seg_8663" s="T388">boil-PRS-1SG</ta>
            <ta e="T390" id="Seg_8664" s="T389">meat-INS</ta>
            <ta e="T391" id="Seg_8665" s="T390">potato-INS</ta>
            <ta e="T398" id="Seg_8666" s="T397">Kamassian-PL-LOC</ta>
            <ta e="T399" id="Seg_8667" s="T398">one.[NOM.SG]</ta>
            <ta e="T400" id="Seg_8668" s="T399">man.[NOM.SG]</ta>
            <ta e="T401" id="Seg_8669" s="T400">die-RES-PST.[3SG]</ta>
            <ta e="T402" id="Seg_8670" s="T401">this-PL</ta>
            <ta e="T403" id="Seg_8671" s="T402">PTCL</ta>
            <ta e="T404" id="Seg_8672" s="T403">dig-PST-3PL</ta>
            <ta e="T405" id="Seg_8673" s="T404">earth.[NOM.SG]</ta>
            <ta e="T406" id="Seg_8674" s="T405">then</ta>
            <ta e="T407" id="Seg_8675" s="T406">make-PST-3PL</ta>
            <ta e="T411" id="Seg_8676" s="T410">then</ta>
            <ta e="T412" id="Seg_8677" s="T411">tent.[NOM.SG]</ta>
            <ta e="T413" id="Seg_8678" s="T412">make-PST-3PL</ta>
            <ta e="T414" id="Seg_8679" s="T413">cross.[NOM.SG]</ta>
            <ta e="T415" id="Seg_8680" s="T414">make-PST-3PL</ta>
            <ta e="T416" id="Seg_8681" s="T415">tent-LAT</ta>
            <ta e="T417" id="Seg_8682" s="T416">this-ACC</ta>
            <ta e="T418" id="Seg_8683" s="T417">put-PST-3PL</ta>
            <ta e="T422" id="Seg_8684" s="T421">this.[NOM.SG]</ta>
            <ta e="T423" id="Seg_8685" s="T422">this-LAT</ta>
            <ta e="T424" id="Seg_8686" s="T423">pipe.[NOM.SG]</ta>
            <ta e="T425" id="Seg_8687" s="T424">eat-PST-3PL</ta>
            <ta e="T426" id="Seg_8688" s="T425">tobacco.[NOM.SG]</ta>
            <ta e="T427" id="Seg_8689" s="T426">eat-PST-3PL</ta>
            <ta e="T429" id="Seg_8690" s="T428">gun.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8691" s="T429">eat-PST-3PL</ta>
            <ta e="T431" id="Seg_8692" s="T430">PTCL</ta>
            <ta e="T433" id="Seg_8693" s="T432">cauldron.[NOM.SG]</ta>
            <ta e="T434" id="Seg_8694" s="T433">eat-PST-3PL</ta>
            <ta e="T435" id="Seg_8695" s="T434">put-PST-3PL</ta>
            <ta e="T437" id="Seg_8696" s="T436">this.[NOM.SG]</ta>
            <ta e="T438" id="Seg_8697" s="T437">there</ta>
            <ta e="T439" id="Seg_8698" s="T438">boil-FUT-3SG</ta>
            <ta e="T440" id="Seg_8699" s="T439">and</ta>
            <ta e="T441" id="Seg_8700" s="T440">eat-FUT-3SG</ta>
            <ta e="T445" id="Seg_8701" s="T444">then</ta>
            <ta e="T446" id="Seg_8702" s="T445">this.[NOM.SG]</ta>
            <ta e="T447" id="Seg_8703" s="T446">man-ACC</ta>
            <ta e="T448" id="Seg_8704" s="T447">bring-PST-3PL</ta>
            <ta e="T450" id="Seg_8705" s="T449">place-LAT</ta>
            <ta e="T451" id="Seg_8706" s="T450">put-PST-3PL</ta>
            <ta e="T452" id="Seg_8707" s="T451">earth-INS</ta>
            <ta e="T453" id="Seg_8708" s="T452">PTCL</ta>
            <ta e="T454" id="Seg_8709" s="T453">pour-PST-3PL</ta>
            <ta e="T456" id="Seg_8710" s="T455">then</ta>
            <ta e="T457" id="Seg_8711" s="T456">tent-LAT/LOC.3SG</ta>
            <ta e="T458" id="Seg_8712" s="T457">come-PST-3PL</ta>
            <ta e="T460" id="Seg_8713" s="T459">evening-LOC.ADV</ta>
            <ta e="T461" id="Seg_8714" s="T460">PTCL</ta>
            <ta e="T462" id="Seg_8715" s="T461">dog.[NOM.SG]</ta>
            <ta e="T463" id="Seg_8716" s="T462">bind-PST-3PL</ta>
            <ta e="T464" id="Seg_8717" s="T463">and</ta>
            <ta e="T465" id="Seg_8718" s="T464">horse.[NOM.SG]</ta>
            <ta e="T467" id="Seg_8719" s="T466">then</ta>
            <ta e="T468" id="Seg_8720" s="T467">go-PST-3PL</ta>
            <ta e="T470" id="Seg_8721" s="T469">oh</ta>
            <ta e="T471" id="Seg_8722" s="T470">come-PRS.[3SG]</ta>
            <ta e="T472" id="Seg_8723" s="T471">run-DUR-3PL</ta>
            <ta e="T473" id="Seg_8724" s="T472">house-LAT</ta>
            <ta e="T474" id="Seg_8725" s="T473">PTCL</ta>
            <ta e="T476" id="Seg_8726" s="T475">scythe.[NOM.SG]</ta>
            <ta e="T477" id="Seg_8727" s="T476">door-LOC</ta>
            <ta e="T478" id="Seg_8728" s="T477">put-FUT-3PL</ta>
            <ta e="T482" id="Seg_8729" s="T481">dog.[NOM.SG]</ta>
            <ta e="T483" id="Seg_8730" s="T482">bind-PST-3PL</ta>
            <ta e="T484" id="Seg_8731" s="T483">black.[NOM.SG]</ta>
            <ta e="T485" id="Seg_8732" s="T484">and</ta>
            <ta e="T486" id="Seg_8733" s="T485">eye-NOM/GEN.3SG</ta>
            <ta e="T487" id="Seg_8734" s="T486">one.[3SG]</ta>
            <ta e="T488" id="Seg_8735" s="T487">two.[NOM.SG]</ta>
            <ta e="T489" id="Seg_8736" s="T488">three.[NOM.SG]</ta>
            <ta e="T490" id="Seg_8737" s="T489">four.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8738" s="T490">four.[NOM.SG]</ta>
            <ta e="T492" id="Seg_8739" s="T491">eye-NOM/GEN.3SG</ta>
            <ta e="T496" id="Seg_8740" s="T495">scythe.[NOM.SG]</ta>
            <ta e="T497" id="Seg_8741" s="T496">set-PST-3PL</ta>
            <ta e="T498" id="Seg_8742" s="T497">so.that</ta>
            <ta e="T499" id="Seg_8743" s="T498">%snake.[NOM.SG]</ta>
            <ta e="T501" id="Seg_8744" s="T500">stab-MOM-PST.[3SG]</ta>
            <ta e="T505" id="Seg_8745" s="T504">single</ta>
            <ta e="T506" id="Seg_8746" s="T505">one.[NOM.SG]</ta>
            <ta e="T507" id="Seg_8747" s="T506">man.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8748" s="T508">we.LAT</ta>
            <ta e="T510" id="Seg_8749" s="T509">hurt-PST.[3SG]</ta>
            <ta e="T511" id="Seg_8750" s="T510">hurt-PST.[3SG]</ta>
            <ta e="T512" id="Seg_8751" s="T511">then</ta>
            <ta e="T514" id="Seg_8752" s="T513">die-RES-PST.[3SG]</ta>
            <ta e="T516" id="Seg_8753" s="T515">and</ta>
            <ta e="T517" id="Seg_8754" s="T516">we.NOM</ta>
            <ta e="T519" id="Seg_8755" s="T518">go-PST-1PL</ta>
            <ta e="T1139" id="Seg_8756" s="T519">Aginskoe</ta>
            <ta e="T520" id="Seg_8757" s="T1139">settlement-LAT</ta>
            <ta e="T522" id="Seg_8758" s="T521">paper.[NOM.SG]</ta>
            <ta e="T523" id="Seg_8759" s="T522">hand-LAT/LOC.3SG</ta>
            <ta e="T525" id="Seg_8760" s="T524">hand-LAT/LOC.3SG</ta>
            <ta e="T526" id="Seg_8761" s="T525">take-INF.LAT</ta>
            <ta e="T527" id="Seg_8762" s="T526">and</ta>
            <ta e="T528" id="Seg_8763" s="T527">head-LAT/LOC.3SG</ta>
            <ta e="T530" id="Seg_8764" s="T529">then</ta>
            <ta e="T531" id="Seg_8765" s="T530">priest.[NOM.SG]</ta>
            <ta e="T532" id="Seg_8766" s="T531">we.LAT</ta>
            <ta e="T533" id="Seg_8767" s="T532">NEG</ta>
            <ta e="T534" id="Seg_8768" s="T533">give-PST.[3SG]</ta>
            <ta e="T536" id="Seg_8769" s="T535">we.NOM</ta>
            <ta e="T537" id="Seg_8770" s="T536">vodka.[NOM.SG]</ta>
            <ta e="T538" id="Seg_8771" s="T537">take-PST-1PL</ta>
            <ta e="T540" id="Seg_8772" s="T539">and</ta>
            <ta e="T541" id="Seg_8773" s="T540">house-LAT/LOC.1SG</ta>
            <ta e="T542" id="Seg_8774" s="T541">come-PST-1PL</ta>
            <ta e="T544" id="Seg_8775" s="T543">then</ta>
            <ta e="T545" id="Seg_8776" s="T544">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T546" id="Seg_8777" s="T545">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T548" id="Seg_8778" s="T547">take-PST-3PL</ta>
            <ta e="T549" id="Seg_8779" s="T548">this.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8780" s="T549">vodka.[NOM.SG]</ta>
            <ta e="T1137" id="Seg_8781" s="T550">go-CVB</ta>
            <ta e="T551" id="Seg_8782" s="T1137">disappear-PST-3PL</ta>
            <ta e="T552" id="Seg_8783" s="T551">drink-INF.LAT</ta>
            <ta e="T554" id="Seg_8784" s="T553">and</ta>
            <ta e="T555" id="Seg_8785" s="T554">we.NOM</ta>
            <ta e="T556" id="Seg_8786" s="T555">child-PL</ta>
            <ta e="T557" id="Seg_8787" s="T556">we.NOM</ta>
            <ta e="T558" id="Seg_8788" s="T557">also</ta>
            <ta e="T559" id="Seg_8789" s="T558">NEG</ta>
            <ta e="T560" id="Seg_8790" s="T559">remain-FUT-1SG</ta>
            <ta e="T561" id="Seg_8791" s="T560">house-LAT</ta>
            <ta e="T564" id="Seg_8792" s="T563">go-CONJ-1DU</ta>
            <ta e="T566" id="Seg_8793" s="T565">and</ta>
            <ta e="T567" id="Seg_8794" s="T566">I.NOM</ta>
            <ta e="T568" id="Seg_8795" s="T567">say-PST-1SG</ta>
            <ta e="T569" id="Seg_8796" s="T568">lie.down-IMP.2SG</ta>
            <ta e="T570" id="Seg_8797" s="T569">sleep-EP-IMP.2SG</ta>
            <ta e="T572" id="Seg_8798" s="T571">and</ta>
            <ta e="T573" id="Seg_8799" s="T572">I.NOM</ta>
            <ta e="T574" id="Seg_8800" s="T573">lie.down-FUT-1SG</ta>
            <ta e="T575" id="Seg_8801" s="T574">you.PL.ACC</ta>
            <ta e="T577" id="Seg_8802" s="T576">when</ta>
            <ta e="T578" id="Seg_8803" s="T577">I.LAT</ta>
            <ta e="T579" id="Seg_8804" s="T578">enough</ta>
            <ta e="T580" id="Seg_8805" s="T579">and</ta>
            <ta e="T581" id="Seg_8806" s="T580">you.PL.NOM</ta>
            <ta e="T582" id="Seg_8807" s="T581">run-MOM-CVB</ta>
            <ta e="T584" id="Seg_8808" s="T583">then</ta>
            <ta e="T585" id="Seg_8809" s="T584">morning-LOC.ADV</ta>
            <ta e="T586" id="Seg_8810" s="T585">get.up-PST-1SG</ta>
            <ta e="T588" id="Seg_8811" s="T587">look</ta>
            <ta e="T589" id="Seg_8812" s="T588">see-PRS-2SG</ta>
            <ta e="T590" id="Seg_8813" s="T589">this.[NOM.SG]</ta>
            <ta e="T591" id="Seg_8814" s="T590">lie.down-DUR.[3SG]</ta>
            <ta e="T592" id="Seg_8815" s="T591">and</ta>
            <ta e="T594" id="Seg_8816" s="T593">and</ta>
            <ta e="T595" id="Seg_8817" s="T594">we.NOM</ta>
            <ta e="T596" id="Seg_8818" s="T595">sleep-PST-1PL</ta>
            <ta e="T600" id="Seg_8819" s="T599">then</ta>
            <ta e="T601" id="Seg_8820" s="T600">this.[NOM.SG]</ta>
            <ta e="T602" id="Seg_8821" s="T601">die-RES-PST.[3SG]</ta>
            <ta e="T603" id="Seg_8822" s="T602">this-ACC</ta>
            <ta e="T604" id="Seg_8823" s="T603">wash-PST-3PL</ta>
            <ta e="T606" id="Seg_8824" s="T605">shirt.[NOM.SG]</ta>
            <ta e="T607" id="Seg_8825" s="T606">dress-PST-3PL</ta>
            <ta e="T608" id="Seg_8826" s="T607">pants.[NOM.SG]</ta>
            <ta e="T609" id="Seg_8827" s="T608">dress-PST-3PL</ta>
            <ta e="T611" id="Seg_8828" s="T610">boot-PL</ta>
            <ta e="T612" id="Seg_8829" s="T611">sew-PST-3PL</ta>
            <ta e="T613" id="Seg_8830" s="T612">dress-PST-3PL</ta>
            <ta e="T615" id="Seg_8831" s="T614">then</ta>
            <ta e="T616" id="Seg_8832" s="T615">take.away-PST-3PL</ta>
            <ta e="T619" id="Seg_8833" s="T618">earth-INS</ta>
            <ta e="T620" id="Seg_8834" s="T619">pour-PST-3PL</ta>
            <ta e="T621" id="Seg_8835" s="T620">this-ACC</ta>
            <ta e="T623" id="Seg_8836" s="T622">come-PST-3PL</ta>
            <ta e="T624" id="Seg_8837" s="T623">house-LAT/LOC.3SG</ta>
            <ta e="T626" id="Seg_8838" s="T625">vodka.[NOM.SG]</ta>
            <ta e="T627" id="Seg_8839" s="T626">drink-PST-3PL</ta>
            <ta e="T628" id="Seg_8840" s="T627">bread.[NOM.SG]</ta>
            <ta e="T629" id="Seg_8841" s="T628">eat-PST-3PL</ta>
            <ta e="T630" id="Seg_8842" s="T629">meat.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8843" s="T630">eat-PST-3PL</ta>
            <ta e="T633" id="Seg_8844" s="T632">and</ta>
            <ta e="T634" id="Seg_8845" s="T633">house-LAT/LOC.3SG</ta>
            <ta e="T1138" id="Seg_8846" s="T634">go-CVB</ta>
            <ta e="T635" id="Seg_8847" s="T1138">disappear-PST-3PL</ta>
            <ta e="T639" id="Seg_8848" s="T638">I.NOM</ta>
            <ta e="T640" id="Seg_8849" s="T639">one.[NOM.SG]</ta>
            <ta e="T641" id="Seg_8850" s="T640">time.[NOM.SG]</ta>
            <ta e="T642" id="Seg_8851" s="T641">come</ta>
            <ta e="T643" id="Seg_8852" s="T642">come-PST-1SG</ta>
            <ta e="T1140" id="Seg_8853" s="T643">Aginskoe</ta>
            <ta e="T644" id="Seg_8854" s="T1140">settlement-LOC</ta>
            <ta e="T646" id="Seg_8855" s="T645">come-PST-1SG</ta>
            <ta e="T647" id="Seg_8856" s="T646">Permyakovo-LAT</ta>
            <ta e="T648" id="Seg_8857" s="T647">then</ta>
            <ta e="T649" id="Seg_8858" s="T648">house-LAT/LOC.3SG</ta>
            <ta e="T650" id="Seg_8859" s="T649">come-PST-1SG</ta>
            <ta e="T652" id="Seg_8860" s="T651">mill-LOC</ta>
            <ta e="T653" id="Seg_8861" s="T652">there</ta>
            <ta e="T654" id="Seg_8862" s="T653">%%-1SG</ta>
            <ta e="T655" id="Seg_8863" s="T654">who.[NOM.SG]=INDEF</ta>
            <ta e="T657" id="Seg_8864" s="T656">show-MOM-PST-3PL</ta>
            <ta e="T659" id="Seg_8865" s="T658">and</ta>
            <ta e="T660" id="Seg_8866" s="T659">I.NOM</ta>
            <ta e="T661" id="Seg_8867" s="T660">come-PST-1SG</ta>
            <ta e="T662" id="Seg_8868" s="T661">and</ta>
            <ta e="T664" id="Seg_8869" s="T663">then</ta>
            <ta e="T665" id="Seg_8870" s="T664">%%-FUT-1SG</ta>
            <ta e="T666" id="Seg_8871" s="T665">PTCL</ta>
            <ta e="T667" id="Seg_8872" s="T666">money.[NOM.SG]</ta>
            <ta e="T668" id="Seg_8873" s="T667">many</ta>
            <ta e="T669" id="Seg_8874" s="T668">become-FUT-3SG</ta>
            <ta e="T673" id="Seg_8875" s="T672">come-PST-1SG</ta>
            <ta e="T675" id="Seg_8876" s="T674">house-LAT/LOC.3SG</ta>
            <ta e="T676" id="Seg_8877" s="T675">who-LAT/LOC.3SG</ta>
            <ta e="T677" id="Seg_8878" s="T676">NEG</ta>
            <ta e="T678" id="Seg_8879" s="T677">see-PST-1SG</ta>
            <ta e="T682" id="Seg_8880" s="T681">I.NOM</ta>
            <ta e="T683" id="Seg_8881" s="T682">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T684" id="Seg_8882" s="T683">hurt-PRS.[3SG]</ta>
            <ta e="T685" id="Seg_8883" s="T684">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T686" id="Seg_8884" s="T685">hurt-PRS.[3SG]</ta>
            <ta e="T687" id="Seg_8885" s="T686">and</ta>
            <ta e="T688" id="Seg_8886" s="T687">you.NOM</ta>
            <ta e="T689" id="Seg_8887" s="T688">what.[NOM.SG]</ta>
            <ta e="T690" id="Seg_8888" s="T689">hurt-PRS.[3SG]</ta>
            <ta e="T691" id="Seg_8889" s="T690">and</ta>
            <ta e="T692" id="Seg_8890" s="T691">I.GEN</ta>
            <ta e="T693" id="Seg_8891" s="T692">tooth-NOM/GEN/ACC.1SG</ta>
            <ta e="T694" id="Seg_8892" s="T693">hurt-PST.[3SG]</ta>
            <ta e="T695" id="Seg_8893" s="T694">hurt-DUR.[3SG]</ta>
            <ta e="T696" id="Seg_8894" s="T695">and</ta>
            <ta e="T697" id="Seg_8895" s="T696">nose-NOM/GEN/ACC.1SG</ta>
            <ta e="T698" id="Seg_8896" s="T697">hurt-DUR.[3SG]</ta>
            <ta e="T702" id="Seg_8897" s="T701">Ivanovich-EP-GEN</ta>
            <ta e="T703" id="Seg_8898" s="T702">PTCL</ta>
            <ta e="T704" id="Seg_8899" s="T703">kidney-NOM/GEN.3SG</ta>
            <ta e="T705" id="Seg_8900" s="T704">hurt-PRS.[3SG]</ta>
            <ta e="T706" id="Seg_8901" s="T705">and</ta>
            <ta e="T707" id="Seg_8902" s="T706">foot-NOM/GEN.3SG</ta>
            <ta e="T708" id="Seg_8903" s="T707">hurt-PRS.[3SG]</ta>
            <ta e="T709" id="Seg_8904" s="T708">and</ta>
            <ta e="T711" id="Seg_8905" s="T710">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T712" id="Seg_8906" s="T711">hurt-DUR-3PL</ta>
            <ta e="T713" id="Seg_8907" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_8908" s="T713">strongly</ta>
            <ta e="T715" id="Seg_8909" s="T714">hurt-PRS.[3SG]</ta>
            <ta e="T719" id="Seg_8910" s="T718">one.should</ta>
            <ta e="T722" id="Seg_8911" s="T721">heal-INF.LAT</ta>
            <ta e="T723" id="Seg_8912" s="T722">this.[NOM.SG]</ta>
            <ta e="T725" id="Seg_8913" s="T724">three.[NOM.SG]</ta>
            <ta e="T726" id="Seg_8914" s="T725">man.[NOM.SG]</ta>
            <ta e="T728" id="Seg_8915" s="T727">doctor.[NOM.SG]</ta>
            <ta e="T729" id="Seg_8916" s="T728">one.should</ta>
            <ta e="T730" id="Seg_8917" s="T729">bring-INF.LAT</ta>
            <ta e="T731" id="Seg_8918" s="T730">JUSS</ta>
            <ta e="T732" id="Seg_8919" s="T731">heal-FUT-3SG</ta>
            <ta e="T733" id="Seg_8920" s="T732">this.[NOM.SG]</ta>
            <ta e="T734" id="Seg_8921" s="T733">this-ACC.PL</ta>
            <ta e="T738" id="Seg_8922" s="T737">what=INDEF</ta>
            <ta e="T739" id="Seg_8923" s="T738">give-FUT-3SG</ta>
            <ta e="T740" id="Seg_8924" s="T739">drink-INF.LAT</ta>
            <ta e="T741" id="Seg_8925" s="T740">or</ta>
            <ta e="T742" id="Seg_8926" s="T741">what=INDEF</ta>
            <ta e="T743" id="Seg_8927" s="T742">give-FUT-3SG</ta>
            <ta e="T744" id="Seg_8928" s="T743">rub-INF.LAT</ta>
            <ta e="T748" id="Seg_8929" s="T747">this-PL</ta>
            <ta e="T749" id="Seg_8930" s="T748">PTCL</ta>
            <ta e="T750" id="Seg_8931" s="T749">strongly</ta>
            <ta e="T751" id="Seg_8932" s="T750">freeze-FACT-PST-3PL</ta>
            <ta e="T752" id="Seg_8933" s="T751">and</ta>
            <ta e="T753" id="Seg_8934" s="T752">now</ta>
            <ta e="T754" id="Seg_8935" s="T753">very</ta>
            <ta e="T755" id="Seg_8936" s="T754">strongly</ta>
            <ta e="T756" id="Seg_8937" s="T755">hurt-DUR-3PL</ta>
            <ta e="T760" id="Seg_8938" s="T759">Marejka.[NOM.SG]</ta>
            <ta e="T761" id="Seg_8939" s="T760">grandmother-LAT</ta>
            <ta e="T762" id="Seg_8940" s="T761">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T764" id="Seg_8941" s="T763">grandmother-LAT</ta>
            <ta e="T765" id="Seg_8942" s="T764">such.[NOM.SG]</ta>
            <ta e="T766" id="Seg_8943" s="T765">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T767" id="Seg_8944" s="T766">and</ta>
            <ta e="T768" id="Seg_8945" s="T767">eye-NOM/GEN.3SG</ta>
            <ta e="T769" id="Seg_8946" s="T768">such.[NOM.SG]</ta>
            <ta e="T770" id="Seg_8947" s="T769">grandmother-LAT/LOC.3SG</ta>
            <ta e="T774" id="Seg_8948" s="T773">live-PST-3PL</ta>
            <ta e="T775" id="Seg_8949" s="T774">woman.[NOM.SG]</ta>
            <ta e="T776" id="Seg_8950" s="T775">and</ta>
            <ta e="T777" id="Seg_8951" s="T776">man.[NOM.SG]</ta>
            <ta e="T779" id="Seg_8952" s="T778">%%</ta>
            <ta e="T780" id="Seg_8953" s="T779">water-LOC</ta>
            <ta e="T781" id="Seg_8954" s="T780">edge-LAT/LOC.3SG</ta>
            <ta e="T783" id="Seg_8955" s="T782">woman.[NOM.SG]</ta>
            <ta e="T784" id="Seg_8956" s="T783">spin-CVB</ta>
            <ta e="T785" id="Seg_8957" s="T784">sit-DUR-PST.[3SG]</ta>
            <ta e="T787" id="Seg_8958" s="T786">and</ta>
            <ta e="T788" id="Seg_8959" s="T787">man.[NOM.SG]</ta>
            <ta e="T789" id="Seg_8960" s="T788">go-PST.[3SG]</ta>
            <ta e="T790" id="Seg_8961" s="T789">water-LAT</ta>
            <ta e="T791" id="Seg_8962" s="T790">fish.[NOM.SG]</ta>
            <ta e="T793" id="Seg_8963" s="T792">capture-INF.LAT</ta>
            <ta e="T795" id="Seg_8964" s="T794">single</ta>
            <ta e="T796" id="Seg_8965" s="T795">time.[NOM.SG]</ta>
            <ta e="T797" id="Seg_8966" s="T796">one.[NOM.SG]</ta>
            <ta e="T798" id="Seg_8967" s="T797">time.[NOM.SG]</ta>
            <ta e="T799" id="Seg_8968" s="T798">go-PST.[3SG]</ta>
            <ta e="T800" id="Seg_8969" s="T799">then</ta>
            <ta e="T801" id="Seg_8970" s="T800">what.[NOM.SG]=INDEF</ta>
            <ta e="T802" id="Seg_8971" s="T801">NEG</ta>
            <ta e="T803" id="Seg_8972" s="T802">capture-PST.[3SG]</ta>
            <ta e="T804" id="Seg_8973" s="T803">one.[NOM.SG]</ta>
            <ta e="T805" id="Seg_8974" s="T804">fish.[NOM.SG]</ta>
            <ta e="T806" id="Seg_8975" s="T805">capture-PST.[3SG]</ta>
            <ta e="T807" id="Seg_8976" s="T806">beautiful.[NOM.SG]</ta>
            <ta e="T809" id="Seg_8977" s="T808">I.NOM</ta>
            <ta e="T810" id="Seg_8978" s="T809">ask-DUR.[3SG]</ta>
            <ta e="T814" id="Seg_8979" s="T813">fish.[NOM.SG]</ta>
            <ta e="T815" id="Seg_8980" s="T814">beautiful.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8981" s="T815">speak-PRS.[3SG]</ta>
            <ta e="T817" id="Seg_8982" s="T816">man-LAT</ta>
            <ta e="T819" id="Seg_8983" s="T818">language-3SG-INS</ta>
            <ta e="T821" id="Seg_8984" s="T820">let-CVB</ta>
            <ta e="T822" id="Seg_8985" s="T821">I.LAT</ta>
            <ta e="T823" id="Seg_8986" s="T822">water-LAT</ta>
            <ta e="T825" id="Seg_8987" s="T824">I.NOM</ta>
            <ta e="T826" id="Seg_8988" s="T825">send-PST-1SG</ta>
            <ta e="T828" id="Seg_8989" s="T827">come-PST.[3SG]</ta>
            <ta e="T829" id="Seg_8990" s="T828">house-LAT/LOC.3SG</ta>
            <ta e="T830" id="Seg_8991" s="T829">and</ta>
            <ta e="T831" id="Seg_8992" s="T830">woman-LAT</ta>
            <ta e="T832" id="Seg_8993" s="T831">speak-DUR.[3SG]</ta>
            <ta e="T834" id="Seg_8994" s="T833">fish.[NOM.SG]</ta>
            <ta e="T835" id="Seg_8995" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_8996" s="T835">beautiful.[NOM.SG]</ta>
            <ta e="T837" id="Seg_8997" s="T836">be-PST.[3SG]</ta>
            <ta e="T838" id="Seg_8998" s="T837">speak-PST.[3SG]</ta>
            <ta e="T839" id="Seg_8999" s="T838">like</ta>
            <ta e="T840" id="Seg_9000" s="T839">man.[NOM.SG]</ta>
            <ta e="T844" id="Seg_9001" s="T843">then</ta>
            <ta e="T845" id="Seg_9002" s="T844">this.[NOM.SG]</ta>
            <ta e="T846" id="Seg_9003" s="T845">woman.[NOM.SG]</ta>
            <ta e="T847" id="Seg_9004" s="T846">INCH</ta>
            <ta e="T848" id="Seg_9005" s="T847">scold-INF.LAT</ta>
            <ta e="T850" id="Seg_9006" s="T849">what.[NOM.SG]</ta>
            <ta e="T851" id="Seg_9007" s="T850">you.NOM</ta>
            <ta e="T852" id="Seg_9008" s="T851">what.[NOM.SG]=INDEF</ta>
            <ta e="T853" id="Seg_9009" s="T852">NEG</ta>
            <ta e="T854" id="Seg_9010" s="T853">look.for-PST-2SG</ta>
            <ta e="T856" id="Seg_9011" s="T855">trough.[NOM.SG]</ta>
            <ta e="T857" id="Seg_9012" s="T856">we.LAT</ta>
            <ta e="T858" id="Seg_9013" s="T857">NEG.EX.[3SG]</ta>
            <ta e="T860" id="Seg_9014" s="T859">hole-ADJZ</ta>
            <ta e="T862" id="Seg_9015" s="T861">trough.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9016" s="T863">this.[NOM.SG]</ta>
            <ta e="T865" id="Seg_9017" s="T864">go-PST.[3SG]</ta>
            <ta e="T866" id="Seg_9018" s="T865">again</ta>
            <ta e="T867" id="Seg_9019" s="T866">water-LAT</ta>
            <ta e="T869" id="Seg_9020" s="T868">then</ta>
            <ta e="T870" id="Seg_9021" s="T869">INCH</ta>
            <ta e="T871" id="Seg_9022" s="T870">shout-INF.LAT</ta>
            <ta e="T872" id="Seg_9023" s="T871">fish.[NOM.SG]</ta>
            <ta e="T873" id="Seg_9024" s="T872">come-IMP.2SG</ta>
            <ta e="T874" id="Seg_9025" s="T873">here</ta>
            <ta e="T875" id="Seg_9026" s="T874">this.[NOM.SG]</ta>
            <ta e="T876" id="Seg_9027" s="T875">come-PST.[3SG]</ta>
            <ta e="T877" id="Seg_9028" s="T876">what.[NOM.SG]</ta>
            <ta e="T878" id="Seg_9029" s="T877">you.DAT</ta>
            <ta e="T879" id="Seg_9030" s="T878">man.[NOM.SG]</ta>
            <ta e="T880" id="Seg_9031" s="T879">one.needs</ta>
            <ta e="T882" id="Seg_9032" s="T881">I.LAT</ta>
            <ta e="T883" id="Seg_9033" s="T882">one.needs</ta>
            <ta e="T884" id="Seg_9034" s="T883">trough.[NOM.SG]</ta>
            <ta e="T886" id="Seg_9035" s="T885">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T887" id="Seg_9036" s="T886">PTCL</ta>
            <ta e="T888" id="Seg_9037" s="T887">scold-DES-DUR.[3SG]</ta>
            <ta e="T889" id="Seg_9038" s="T888">well</ta>
            <ta e="T890" id="Seg_9039" s="T889">go-EP-IMP.2SG</ta>
            <ta e="T891" id="Seg_9040" s="T890">house-LAT</ta>
            <ta e="T895" id="Seg_9041" s="T894">this.[NOM.SG]</ta>
            <ta e="T896" id="Seg_9042" s="T895">woman.[NOM.SG]</ta>
            <ta e="T897" id="Seg_9043" s="T896">INCH</ta>
            <ta e="T898" id="Seg_9044" s="T897">this-ACC</ta>
            <ta e="T899" id="Seg_9045" s="T898">scold-INF.LAT</ta>
            <ta e="T900" id="Seg_9046" s="T899">what.[NOM.SG]</ta>
            <ta e="T901" id="Seg_9047" s="T900">NEG</ta>
            <ta e="T902" id="Seg_9048" s="T901">look.for-PST-2SG</ta>
            <ta e="T903" id="Seg_9049" s="T902">house.[NOM.SG]</ta>
            <ta e="T905" id="Seg_9050" s="T904">we.GEN</ta>
            <ta e="T906" id="Seg_9051" s="T905">house.[NOM.SG]</ta>
            <ta e="T907" id="Seg_9052" s="T906">NEG</ta>
            <ta e="T908" id="Seg_9053" s="T907">beautiful.[NOM.SG]</ta>
            <ta e="T910" id="Seg_9054" s="T909">this.[NOM.SG]</ta>
            <ta e="T911" id="Seg_9055" s="T910">again</ta>
            <ta e="T912" id="Seg_9056" s="T911">go-PST.[3SG]</ta>
            <ta e="T913" id="Seg_9057" s="T912">INCH</ta>
            <ta e="T914" id="Seg_9058" s="T913">shout-INF.LAT</ta>
            <ta e="T915" id="Seg_9059" s="T914">fish.[NOM.SG]</ta>
            <ta e="T916" id="Seg_9060" s="T915">fish.[NOM.SG]</ta>
            <ta e="T917" id="Seg_9061" s="T916">come-IMP.2SG</ta>
            <ta e="T918" id="Seg_9062" s="T917">here</ta>
            <ta e="T920" id="Seg_9063" s="T919">fish.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9064" s="T920">come-PST.[3SG]</ta>
            <ta e="T922" id="Seg_9065" s="T921">what.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9066" s="T922">you.DAT</ta>
            <ta e="T924" id="Seg_9067" s="T923">one.needs</ta>
            <ta e="T926" id="Seg_9068" s="T925">house.[NOM.SG]</ta>
            <ta e="T927" id="Seg_9069" s="T926">one.needs</ta>
            <ta e="T929" id="Seg_9070" s="T928">we.GEN</ta>
            <ta e="T930" id="Seg_9071" s="T929">house-NOM/GEN/ACC.1PL</ta>
            <ta e="T931" id="Seg_9072" s="T930">NEG</ta>
            <ta e="T932" id="Seg_9073" s="T931">beautiful.[NOM.SG]</ta>
            <ta e="T934" id="Seg_9074" s="T933">go-EP-IMP.2SG</ta>
            <ta e="T935" id="Seg_9075" s="T934">house-LAT-2SG</ta>
            <ta e="T939" id="Seg_9076" s="T938">then</ta>
            <ta e="T940" id="Seg_9077" s="T939">again</ta>
            <ta e="T941" id="Seg_9078" s="T940">this.[NOM.SG]</ta>
            <ta e="T942" id="Seg_9079" s="T941">woman.[NOM.SG]</ta>
            <ta e="T944" id="Seg_9080" s="T942">scold-DES-PST.[3SG]</ta>
            <ta e="T945" id="Seg_9081" s="T944">go-EP-IMP.2SG</ta>
            <ta e="T946" id="Seg_9082" s="T945">fish-LAT</ta>
            <ta e="T947" id="Seg_9083" s="T946">ask-IMP.2SG</ta>
            <ta e="T948" id="Seg_9084" s="T947">so.that</ta>
            <ta e="T949" id="Seg_9085" s="T948">I.NOM</ta>
            <ta e="T950" id="Seg_9086" s="T949">become-PST-1SG</ta>
            <ta e="T951" id="Seg_9087" s="T950">merchant.wife.INS-INS</ta>
            <ta e="T955" id="Seg_9088" s="T954">then</ta>
            <ta e="T956" id="Seg_9089" s="T955">fish.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9090" s="T956">say-PST.[3SG]</ta>
            <ta e="T958" id="Seg_9091" s="T957">go-EP-IMP.2SG</ta>
            <ta e="T959" id="Seg_9092" s="T958">house-LAT-2SG</ta>
            <ta e="T961" id="Seg_9093" s="T960">this.[NOM.SG]</ta>
            <ta e="T962" id="Seg_9094" s="T961">come-PST.[3SG]</ta>
            <ta e="T963" id="Seg_9095" s="T962">and</ta>
            <ta e="T964" id="Seg_9096" s="T963">this.[NOM.SG]</ta>
            <ta e="T965" id="Seg_9097" s="T964">woman.[NOM.SG]</ta>
            <ta e="T966" id="Seg_9098" s="T965">again</ta>
            <ta e="T968" id="Seg_9099" s="T966">scold-DES-DUR.[3SG]</ta>
            <ta e="T969" id="Seg_9100" s="T968">go-EP-IMP.2SG</ta>
            <ta e="T970" id="Seg_9101" s="T969">fish-LAT</ta>
            <ta e="T971" id="Seg_9102" s="T970">look.for-IMP.2SG</ta>
            <ta e="T972" id="Seg_9103" s="T971">so.that</ta>
            <ta e="T973" id="Seg_9104" s="T972">I.NOM</ta>
            <ta e="T974" id="Seg_9105" s="T973">be-PST-1SG</ta>
            <ta e="T975" id="Seg_9106" s="T974">noblewoman.INS-INS</ta>
            <ta e="T977" id="Seg_9107" s="T976">this.[NOM.SG]</ta>
            <ta e="T978" id="Seg_9108" s="T977">go-PST.[3SG]</ta>
            <ta e="T979" id="Seg_9109" s="T978">fish-LAT</ta>
            <ta e="T980" id="Seg_9110" s="T979">fish.[NOM.SG]</ta>
            <ta e="T981" id="Seg_9111" s="T980">shout-DUR.PST.[3SG]</ta>
            <ta e="T983" id="Seg_9112" s="T982">fish.[NOM.SG]</ta>
            <ta e="T984" id="Seg_9113" s="T983">come-PST.[3SG]</ta>
            <ta e="T985" id="Seg_9114" s="T984">what.[NOM.SG]</ta>
            <ta e="T986" id="Seg_9115" s="T985">you.DAT</ta>
            <ta e="T987" id="Seg_9116" s="T986">one.needs</ta>
            <ta e="T989" id="Seg_9117" s="T988">I.NOM</ta>
            <ta e="T990" id="Seg_9118" s="T989">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T991" id="Seg_9119" s="T990">say-IPFVZ.[3SG]</ta>
            <ta e="T992" id="Seg_9120" s="T991">so.that</ta>
            <ta e="T993" id="Seg_9121" s="T992">I.NOM</ta>
            <ta e="T994" id="Seg_9122" s="T993">noblewoman.INS-INS</ta>
            <ta e="T995" id="Seg_9123" s="T994">be-PST-1SG</ta>
            <ta e="T997" id="Seg_9124" s="T996">go-EP-IMP.2SG</ta>
            <ta e="T998" id="Seg_9125" s="T997">house-LAT-2SG</ta>
            <ta e="T1002" id="Seg_9126" s="T1001">then</ta>
            <ta e="T1003" id="Seg_9127" s="T1002">man.[NOM.SG]</ta>
            <ta e="T1004" id="Seg_9128" s="T1003">come-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_9129" s="T1004">house-LAT/LOC.3SG</ta>
            <ta e="T1006" id="Seg_9130" s="T1005">this.[NOM.SG]</ta>
            <ta e="T1007" id="Seg_9131" s="T1006">INCH</ta>
            <ta e="T1008" id="Seg_9132" s="T1007">again</ta>
            <ta e="T1009" id="Seg_9133" s="T1008">scold-INF.LAT</ta>
            <ta e="T1013" id="Seg_9134" s="T1012">and</ta>
            <ta e="T1014" id="Seg_9135" s="T1013">beat-INF.LAT</ta>
            <ta e="T1015" id="Seg_9136" s="T1014">want.PST.M.SG</ta>
            <ta e="T1017" id="Seg_9137" s="T1016">go-EP-IMP.2SG</ta>
            <ta e="T1018" id="Seg_9138" s="T1017">again</ta>
            <ta e="T1019" id="Seg_9139" s="T1018">fish-LAT</ta>
            <ta e="T1020" id="Seg_9140" s="T1019">look.for-IMP.2SG</ta>
            <ta e="T1021" id="Seg_9141" s="T1020">so.that</ta>
            <ta e="T1022" id="Seg_9142" s="T1021">I.NOM</ta>
            <ta e="T1027" id="Seg_9143" s="T1026">so.that</ta>
            <ta e="T1028" id="Seg_9144" s="T1027">people.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_9145" s="T1028">I.LAT</ta>
            <ta e="T1030" id="Seg_9146" s="T1029">hand-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1035" id="Seg_9147" s="T1034">then</ta>
            <ta e="T1036" id="Seg_9148" s="T1035">this.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_9149" s="T1036">come-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_9150" s="T1037">water-LAT</ta>
            <ta e="T1040" id="Seg_9151" s="T1039">fish.[NOM.SG]</ta>
            <ta e="T1041" id="Seg_9152" s="T1040">fish.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_9153" s="T1041">come-IMP.2SG</ta>
            <ta e="T1043" id="Seg_9154" s="T1042">here</ta>
            <ta e="T1044" id="Seg_9155" s="T1043">fish.[NOM.SG]</ta>
            <ta e="T1045" id="Seg_9156" s="T1044">come-PST.[3SG]</ta>
            <ta e="T1047" id="Seg_9157" s="T1046">what.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_9158" s="T1047">you.DAT</ta>
            <ta e="T1049" id="Seg_9159" s="T1048">one.needs</ta>
            <ta e="T1050" id="Seg_9160" s="T1049">man.[NOM.SG]</ta>
            <ta e="T1051" id="Seg_9161" s="T1050">and</ta>
            <ta e="T1052" id="Seg_9162" s="T1051">I.NOM</ta>
            <ta e="T1053" id="Seg_9163" s="T1052">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1055" id="Seg_9164" s="T1054">say-IPFVZ.[3SG]</ta>
            <ta e="T1056" id="Seg_9165" s="T1055">so.that</ta>
            <ta e="T1057" id="Seg_9166" s="T1056">I.NOM</ta>
            <ta e="T1059" id="Seg_9167" s="T1058">so.that</ta>
            <ta e="T1060" id="Seg_9168" s="T1059">I.NOM</ta>
            <ta e="T1061" id="Seg_9169" s="T1060">be-PST-1SG</ta>
            <ta e="T1062" id="Seg_9170" s="T1061">tsarina.INS-INS</ta>
            <ta e="T1064" id="Seg_9171" s="T1063">so.that</ta>
            <ta e="T1065" id="Seg_9172" s="T1064">I.LAT</ta>
            <ta e="T1066" id="Seg_9173" s="T1065">people.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_9174" s="T1066">hand-3SG-INS</ta>
            <ta e="T1068" id="Seg_9175" s="T1067">bring-RES-PST-3PL</ta>
            <ta e="T1070" id="Seg_9176" s="T1069">go-EP-IMP.2SG</ta>
            <ta e="T1071" id="Seg_9177" s="T1070">house-LAT-2SG</ta>
            <ta e="T1072" id="Seg_9178" s="T1071">PTCL</ta>
            <ta e="T1073" id="Seg_9179" s="T1072">become-FUT-3SG</ta>
            <ta e="T1074" id="Seg_9180" s="T1073">you.DAT</ta>
            <ta e="T1078" id="Seg_9181" s="T1077">then</ta>
            <ta e="T1079" id="Seg_9182" s="T1078">man.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_9183" s="T1079">go-PST.[3SG]</ta>
            <ta e="T1081" id="Seg_9184" s="T1080">water-LAT</ta>
            <ta e="T1083" id="Seg_9185" s="T1082">fish-PL</ta>
            <ta e="T1084" id="Seg_9186" s="T1083">fish-PL</ta>
            <ta e="T1085" id="Seg_9187" s="T1084">come-IMP.2SG</ta>
            <ta e="T1086" id="Seg_9188" s="T1085">here</ta>
            <ta e="T1087" id="Seg_9189" s="T1086">fish.[NOM.SG]</ta>
            <ta e="T1088" id="Seg_9190" s="T1087">come-PST.[3SG]</ta>
            <ta e="T1089" id="Seg_9191" s="T1088">what.[NOM.SG]</ta>
            <ta e="T1090" id="Seg_9192" s="T1089">you.DAT</ta>
            <ta e="T1091" id="Seg_9193" s="T1090">one.needs</ta>
            <ta e="T1093" id="Seg_9194" s="T1092">and</ta>
            <ta e="T1094" id="Seg_9195" s="T1093">I.NOM</ta>
            <ta e="T1095" id="Seg_9196" s="T1094">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1096" id="Seg_9197" s="T1095">say-IPFVZ.[3SG]</ta>
            <ta e="T1097" id="Seg_9198" s="T1096">so.that</ta>
            <ta e="T1098" id="Seg_9199" s="T1097">I.NOM</ta>
            <ta e="T1099" id="Seg_9200" s="T1098">be-PST-1SG</ta>
            <ta e="T1103" id="Seg_9201" s="T1102">and</ta>
            <ta e="T1104" id="Seg_9202" s="T1103">fish.[NOM.SG]</ta>
            <ta e="T1105" id="Seg_9203" s="T1104">so.that</ta>
            <ta e="T1106" id="Seg_9204" s="T1105">I.LAT</ta>
            <ta e="T1107" id="Seg_9205" s="T1106">carry-PST.[3SG]</ta>
            <ta e="T1108" id="Seg_9206" s="T1107">PTCL</ta>
            <ta e="T1109" id="Seg_9207" s="T1108">what.[NOM.SG]</ta>
            <ta e="T1111" id="Seg_9208" s="T1110">then</ta>
            <ta e="T1112" id="Seg_9209" s="T1111">fish.[NOM.SG]</ta>
            <ta e="T1113" id="Seg_9210" s="T1112">go-PST.[3SG]</ta>
            <ta e="T1114" id="Seg_9211" s="T1113">water-LAT</ta>
            <ta e="T1115" id="Seg_9212" s="T1114">what.[NOM.SG]</ta>
            <ta e="T1117" id="Seg_9213" s="T1116">what.[NOM.SG]=INDEF</ta>
            <ta e="T1119" id="Seg_9214" s="T1118">man-LAT</ta>
            <ta e="T1120" id="Seg_9215" s="T1119">NEG</ta>
            <ta e="T1121" id="Seg_9216" s="T1120">tell-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_9217" s="T1122">this.[NOM.SG]</ta>
            <ta e="T1124" id="Seg_9218" s="T1123">come-PST.[3SG]</ta>
            <ta e="T1125" id="Seg_9219" s="T1124">tent-LAT/LOC.3SG</ta>
            <ta e="T1127" id="Seg_9220" s="T1126">woman.[NOM.SG]</ta>
            <ta e="T1128" id="Seg_9221" s="T1127">sit-DUR.[3SG]</ta>
            <ta e="T1129" id="Seg_9222" s="T1128">spin-DUR.[3SG]</ta>
            <ta e="T1130" id="Seg_9223" s="T1129">and</ta>
            <ta e="T1131" id="Seg_9224" s="T1130">trough.[NOM.SG]</ta>
            <ta e="T1132" id="Seg_9225" s="T1131">trough-NOM/GEN.3SG</ta>
            <ta e="T1133" id="Seg_9226" s="T1132">stand-DUR.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_9227" s="T1">два.[NOM.SG]</ta>
            <ta e="T3" id="Seg_9228" s="T2">овца-NOM/GEN/ACC.3SG</ta>
            <ta e="T4" id="Seg_9229" s="T3">и</ta>
            <ta e="T5" id="Seg_9230" s="T4">женщина.[NOM.SG]</ta>
            <ta e="T7" id="Seg_9231" s="T6">два.[NOM.SG]</ta>
            <ta e="T8" id="Seg_9232" s="T7">девушка.[NOM.SG]</ta>
            <ta e="T9" id="Seg_9233" s="T8">быть-PST.[3SG]</ta>
            <ta e="T10" id="Seg_9234" s="T9">два.[NOM.SG]</ta>
            <ta e="T11" id="Seg_9235" s="T10">девушка.[NOM.SG]</ta>
            <ta e="T12" id="Seg_9236" s="T11">быть-PST.[3SG]</ta>
            <ta e="T13" id="Seg_9237" s="T12">тогда</ta>
            <ta e="T15" id="Seg_9238" s="T14">овца.[NOM.SG]</ta>
            <ta e="T16" id="Seg_9239" s="T15">табун.[NOM.SG]</ta>
            <ta e="T17" id="Seg_9240" s="T16">быть-PST.[3SG]</ta>
            <ta e="T18" id="Seg_9241" s="T17">тогда</ta>
            <ta e="T19" id="Seg_9242" s="T18">табун.[NOM.SG]</ta>
            <ta e="T20" id="Seg_9243" s="T19">лошадь.[NOM.SG]</ta>
            <ta e="T21" id="Seg_9244" s="T20">быть-PST-3PL</ta>
            <ta e="T22" id="Seg_9245" s="T21">тогда</ta>
            <ta e="T23" id="Seg_9246" s="T22">табун.[NOM.SG]</ta>
            <ta e="T24" id="Seg_9247" s="T23">корова.[NOM.SG]</ta>
            <ta e="T25" id="Seg_9248" s="T24">быть-PST-3PL</ta>
            <ta e="T27" id="Seg_9249" s="T26">тогда</ta>
            <ta e="T28" id="Seg_9250" s="T27">один.[NOM.SG]</ta>
            <ta e="T29" id="Seg_9251" s="T28">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T30" id="Seg_9252" s="T29">а</ta>
            <ta e="T31" id="Seg_9253" s="T30">один.[NOM.SG]</ta>
            <ta e="T32" id="Seg_9254" s="T31">PTCL</ta>
            <ta e="T34" id="Seg_9255" s="T33">тогда</ta>
            <ta e="T35" id="Seg_9256" s="T34">один.[NOM.SG]</ta>
            <ta e="T36" id="Seg_9257" s="T35">вошь.[NOM.SG]</ta>
            <ta e="T37" id="Seg_9258" s="T36">кипятить-PST.[3SG]</ta>
            <ta e="T38" id="Seg_9259" s="T37">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T39" id="Seg_9260" s="T38">там</ta>
            <ta e="T40" id="Seg_9261" s="T39">Кодур-LOC</ta>
            <ta e="T41" id="Seg_9262" s="T40">а</ta>
            <ta e="T42" id="Seg_9263" s="T41">один.[NOM.SG]</ta>
            <ta e="T43" id="Seg_9264" s="T42">свинец</ta>
            <ta e="T44" id="Seg_9265" s="T43">кипятить-PST.[3SG]</ta>
            <ta e="T45" id="Seg_9266" s="T44">тогда</ta>
            <ta e="T46" id="Seg_9267" s="T45">принести-PST.[3SG]</ta>
            <ta e="T47" id="Seg_9268" s="T46">и</ta>
            <ta e="T48" id="Seg_9269" s="T47">ухо-LAT</ta>
            <ta e="T49" id="Seg_9270" s="T48">лить-PST.[3SG]</ta>
            <ta e="T52" id="Seg_9271" s="T51">ухо-LAT</ta>
            <ta e="T53" id="Seg_9272" s="T52">лить-PST.[3SG]</ta>
            <ta e="T54" id="Seg_9273" s="T53">и</ta>
            <ta e="T55" id="Seg_9274" s="T54">этот.[NOM.SG]</ta>
            <ta e="T56" id="Seg_9275" s="T55">PTCL</ta>
            <ta e="T57" id="Seg_9276" s="T56">умереть-RES-PST.[3SG]</ta>
            <ta e="T59" id="Seg_9277" s="T58">тогда</ta>
            <ta e="T60" id="Seg_9278" s="T59">этот-GEN</ta>
            <ta e="T61" id="Seg_9279" s="T60">песня.[NOM.SG]</ta>
            <ta e="T62" id="Seg_9280" s="T61">PTCL</ta>
            <ta e="T63" id="Seg_9281" s="T62">умереть-RES-PST.[3SG]</ta>
            <ta e="T64" id="Seg_9282" s="T63">тогда</ta>
            <ta e="T65" id="Seg_9283" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_9284" s="T65">дочь-PL</ta>
            <ta e="T67" id="Seg_9285" s="T66">взять-PST-3PL</ta>
            <ta e="T68" id="Seg_9286" s="T67">овца.[NOM.SG]</ta>
            <ta e="T69" id="Seg_9287" s="T68">лошадь-PL</ta>
            <ta e="T70" id="Seg_9288" s="T69">и</ta>
            <ta e="T71" id="Seg_9289" s="T70">корова-PL</ta>
            <ta e="T72" id="Seg_9290" s="T71">и</ta>
            <ta e="T73" id="Seg_9291" s="T72">отец-LAT/LOC.3SG</ta>
            <ta e="T1134" id="Seg_9292" s="T74">пойти-CVB</ta>
            <ta e="T75" id="Seg_9293" s="T1134">исчезнуть-PST-3PL</ta>
            <ta e="T77" id="Seg_9294" s="T76">тогда</ta>
            <ta e="T78" id="Seg_9295" s="T77">весь</ta>
            <ta e="T79" id="Seg_9296" s="T78">сказка.[NOM.SG]</ta>
            <ta e="T81" id="Seg_9297" s="T80">мышь.[NOM.SG]</ta>
            <ta e="T82" id="Seg_9298" s="T81">и</ta>
            <ta e="T83" id="Seg_9299" s="T82">уголь.[NOM.SG]</ta>
            <ta e="T84" id="Seg_9300" s="T83">и</ta>
            <ta e="T85" id="Seg_9301" s="T84">пузырь.[NOM.SG]</ta>
            <ta e="T86" id="Seg_9302" s="T85">пойти-PST-3PL</ta>
            <ta e="T88" id="Seg_9303" s="T87">река.[NOM.SG]</ta>
            <ta e="T89" id="Seg_9304" s="T88">а</ta>
            <ta e="T90" id="Seg_9305" s="T89">этот.[NOM.SG]</ta>
            <ta e="T91" id="Seg_9306" s="T90">пойти-INF.LAT</ta>
            <ta e="T92" id="Seg_9307" s="T91">река.[NOM.SG]</ta>
            <ta e="T93" id="Seg_9308" s="T92">река.[NOM.SG]</ta>
            <ta e="T94" id="Seg_9309" s="T93">течь-DUR.[3SG]</ta>
            <ta e="T95" id="Seg_9310" s="T94">мышь.[NOM.SG]</ta>
            <ta e="T96" id="Seg_9311" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_9312" s="T96">ветка-PL</ta>
            <ta e="T98" id="Seg_9313" s="T97">класть-PST.[3SG]</ta>
            <ta e="T99" id="Seg_9314" s="T98">тогда</ta>
            <ta e="T100" id="Seg_9315" s="T99">мышь.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_9316" s="T100">пойти-CVB</ta>
            <ta e="T101" id="Seg_9317" s="T1135">исчезнуть-PST.[3SG]</ta>
            <ta e="T102" id="Seg_9318" s="T101">пузырь.[NOM.SG]</ta>
            <ta e="T1136" id="Seg_9319" s="T102">пойти-CVB</ta>
            <ta e="T103" id="Seg_9320" s="T1136">исчезнуть-PST.[3SG]</ta>
            <ta e="T105" id="Seg_9321" s="T104">а</ta>
            <ta e="T106" id="Seg_9322" s="T105">уголь</ta>
            <ta e="T107" id="Seg_9323" s="T106">пойти</ta>
            <ta e="T108" id="Seg_9324" s="T107">уголь.[NOM.SG]</ta>
            <ta e="T109" id="Seg_9325" s="T108">пойти-PST.[3SG]</ta>
            <ta e="T110" id="Seg_9326" s="T109">и</ta>
            <ta e="T111" id="Seg_9327" s="T110">вода-LAT</ta>
            <ta e="T112" id="Seg_9328" s="T111">упасть-MOM-PST.[3SG]</ta>
            <ta e="T113" id="Seg_9329" s="T112">и</ta>
            <ta e="T114" id="Seg_9330" s="T113">умереть-RES-PST.[3SG]</ta>
            <ta e="T116" id="Seg_9331" s="T115">а</ta>
            <ta e="T117" id="Seg_9332" s="T116">пузырь.[NOM.SG]</ta>
            <ta e="T118" id="Seg_9333" s="T117">смеяться-PST.[3SG]</ta>
            <ta e="T119" id="Seg_9334" s="T118">смеяться-PST.[3SG]</ta>
            <ta e="T120" id="Seg_9335" s="T119">и</ta>
            <ta e="T121" id="Seg_9336" s="T120">умереть-RES-PST.[3SG]</ta>
            <ta e="T123" id="Seg_9337" s="T122">мышь.[NOM.SG]</ta>
            <ta e="T124" id="Seg_9338" s="T123">один</ta>
            <ta e="T126" id="Seg_9339" s="T125">остаться-MOM-PST.[3SG]</ta>
            <ta e="T127" id="Seg_9340" s="T126">тогда</ta>
            <ta e="T128" id="Seg_9341" s="T127">пойти-PST.[3SG]</ta>
            <ta e="T129" id="Seg_9342" s="T128">пойти-PST.[3SG]</ta>
            <ta e="T130" id="Seg_9343" s="T129">ребенок-PL</ta>
            <ta e="T131" id="Seg_9344" s="T130">PTCL</ta>
            <ta e="T132" id="Seg_9345" s="T131">играть-DUR-3PL</ta>
            <ta e="T134" id="Seg_9346" s="T133">вода.[NOM.SG]</ta>
            <ta e="T135" id="Seg_9347" s="T134">унести-RES-PST.[3SG]</ta>
            <ta e="T136" id="Seg_9348" s="T135">ветка.[NOM.SG]</ta>
            <ta e="T137" id="Seg_9349" s="T136">вода-LOC</ta>
            <ta e="T139" id="Seg_9350" s="T138">ребенок-PL</ta>
            <ta e="T140" id="Seg_9351" s="T139">мать</ta>
            <ta e="T141" id="Seg_9352" s="T140">отец-NOM/GEN.3SG</ta>
            <ta e="T143" id="Seg_9353" s="T142">мать-GEN</ta>
            <ta e="T144" id="Seg_9354" s="T143">пойти-IMP.2PL</ta>
            <ta e="T145" id="Seg_9355" s="T144">чум-LAT</ta>
            <ta e="T146" id="Seg_9356" s="T145">и</ta>
            <ta e="T147" id="Seg_9357" s="T146">сказать-IPFVZ-CVB</ta>
            <ta e="T148" id="Seg_9358" s="T147">сказать-EP-IMP.2SG</ta>
            <ta e="T149" id="Seg_9359" s="T148">мать-LAT</ta>
            <ta e="T151" id="Seg_9360" s="T150">этот-PL</ta>
            <ta e="T152" id="Seg_9361" s="T151">бежать-CVB</ta>
            <ta e="T153" id="Seg_9362" s="T152">прийти-PST-3PL</ta>
            <ta e="T155" id="Seg_9363" s="T154">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T156" id="Seg_9364" s="T155">прийти-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_9365" s="T157">ну</ta>
            <ta e="T159" id="Seg_9366" s="T158">пойти-IMP.2PL</ta>
            <ta e="T160" id="Seg_9367" s="T159">позвать-IMP.2PL</ta>
            <ta e="T161" id="Seg_9368" s="T160">я.NOM</ta>
            <ta e="T162" id="Seg_9369" s="T161">скоро</ta>
            <ta e="T163" id="Seg_9370" s="T162">мясо.[NOM.SG]</ta>
            <ta e="T164" id="Seg_9371" s="T163">кипятить-FUT-1SG</ta>
            <ta e="T166" id="Seg_9372" s="T165">этот-PL</ta>
            <ta e="T167" id="Seg_9373" s="T166">бежать-MOM-PST-3PL</ta>
            <ta e="T168" id="Seg_9374" s="T167">прийти-IMP.2SG</ta>
            <ta e="T169" id="Seg_9375" s="T168">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T170" id="Seg_9376" s="T169">мясо-ACC</ta>
            <ta e="T172" id="Seg_9377" s="T171">кипятить-DUR-3PL</ta>
            <ta e="T174" id="Seg_9378" s="T173">нога-NOM/GEN/ACC.3PL</ta>
            <ta e="T176" id="Seg_9379" s="T175">я.NOM</ta>
            <ta e="T177" id="Seg_9380" s="T176">этот-ACC</ta>
            <ta e="T178" id="Seg_9381" s="T177">NEG.AUX-1SG</ta>
            <ta e="T180" id="Seg_9382" s="T179">NEG.EX.[3SG]</ta>
            <ta e="T181" id="Seg_9383" s="T180">съесть-INF.LAT</ta>
            <ta e="T182" id="Seg_9384" s="T181">JUSS</ta>
            <ta e="T183" id="Seg_9385" s="T182">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T184" id="Seg_9386" s="T183">резать-FUT-3SG</ta>
            <ta e="T185" id="Seg_9387" s="T184">и</ta>
            <ta e="T186" id="Seg_9388" s="T185">кипятить-FUT-3SG</ta>
            <ta e="T187" id="Seg_9389" s="T186">кожа-LAT</ta>
            <ta e="T188" id="Seg_9390" s="T187">поставить-FUT-3SG</ta>
            <ta e="T189" id="Seg_9391" s="T188">этот.[NOM.SG]</ta>
            <ta e="T190" id="Seg_9392" s="T189">корова.[NOM.SG]</ta>
            <ta e="T191" id="Seg_9393" s="T190">резать-PST.[3SG]</ta>
            <ta e="T193" id="Seg_9394" s="T192">мясо.[NOM.SG]</ta>
            <ta e="T194" id="Seg_9395" s="T193">кипятить-PST.[3SG]</ta>
            <ta e="T195" id="Seg_9396" s="T194">кожа-LAT</ta>
            <ta e="T196" id="Seg_9397" s="T195">класть-PST.[3SG]</ta>
            <ta e="T198" id="Seg_9398" s="T197">тогда</ta>
            <ta e="T199" id="Seg_9399" s="T198">ребенок-PL</ta>
            <ta e="T200" id="Seg_9400" s="T199">пойти-PST-3PL</ta>
            <ta e="T201" id="Seg_9401" s="T200">прийти-IMP.2SG</ta>
            <ta e="T202" id="Seg_9402" s="T201">мясо.[NOM.SG]</ta>
            <ta e="T203" id="Seg_9403" s="T202">кипятить-TR-DETR-PTCP.[NOM.SG]</ta>
            <ta e="T205" id="Seg_9404" s="T204">прийти-PST-3PL</ta>
            <ta e="T207" id="Seg_9405" s="T206">где</ta>
            <ta e="T208" id="Seg_9406" s="T207">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T210" id="Seg_9407" s="T209">INCH</ta>
            <ta e="T211" id="Seg_9408" s="T210">этот-ACC.PL</ta>
            <ta e="T212" id="Seg_9409" s="T211">бить-INF.LAT</ta>
            <ta e="T213" id="Seg_9410" s="T212">ребенок-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T215" id="Seg_9411" s="T214">а</ta>
            <ta e="T216" id="Seg_9412" s="T215">мышь.[NOM.SG]</ta>
            <ta e="T217" id="Seg_9413" s="T216">сидеть-DUR.[3SG]</ta>
            <ta e="T218" id="Seg_9414" s="T217">NEG.AUX-IMP.2SG</ta>
            <ta e="T219" id="Seg_9415" s="T218">бороться-CNG</ta>
            <ta e="T220" id="Seg_9416" s="T219">я.NOM</ta>
            <ta e="T221" id="Seg_9417" s="T220">здесь</ta>
            <ta e="T222" id="Seg_9418" s="T221">прийти-PST-1SG</ta>
            <ta e="T224" id="Seg_9419" s="T223">сегодня</ta>
            <ta e="T225" id="Seg_9420" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_9421" s="T225">снег.[NOM.SG]</ta>
            <ta e="T227" id="Seg_9422" s="T226">упасть-DUR.[3SG]</ta>
            <ta e="T229" id="Seg_9423" s="T228">и</ta>
            <ta e="T230" id="Seg_9424" s="T229">ветер.[NOM.SG]</ta>
            <ta e="T231" id="Seg_9425" s="T230">прийти-PRS.[3SG]</ta>
            <ta e="T233" id="Seg_9426" s="T232">очень</ta>
            <ta e="T234" id="Seg_9427" s="T233">холодный.[NOM.SG]</ta>
            <ta e="T235" id="Seg_9428" s="T234">снаружи</ta>
            <ta e="T237" id="Seg_9429" s="T236">снаружи</ta>
            <ta e="T239" id="Seg_9430" s="T238">сколько</ta>
            <ta e="T240" id="Seg_9431" s="T239">зима.[NOM.SG]</ta>
            <ta e="T241" id="Seg_9432" s="T240">ребенок-LAT</ta>
            <ta e="T243" id="Seg_9433" s="T242">три.[NOM.SG]</ta>
            <ta e="T245" id="Seg_9434" s="T244">тот.[NOM.SG]</ta>
            <ta e="T246" id="Seg_9435" s="T245">зима-LOC.ADV</ta>
            <ta e="T247" id="Seg_9436" s="T246">очень</ta>
            <ta e="T248" id="Seg_9437" s="T247">холодный.[NOM.SG]</ta>
            <ta e="T249" id="Seg_9438" s="T248">быть-PST.[3SG]</ta>
            <ta e="T251" id="Seg_9439" s="T250">мерзнуть-DUR-PST-3PL</ta>
            <ta e="T252" id="Seg_9440" s="T251">люди.[NOM.SG]</ta>
            <ta e="T253" id="Seg_9441" s="T252">PTCL</ta>
            <ta e="T257" id="Seg_9442" s="T256">трава.[NOM.SG]</ta>
            <ta e="T258" id="Seg_9443" s="T257">резать-PST-1SG</ta>
            <ta e="T259" id="Seg_9444" s="T258">коса-INS</ta>
            <ta e="T261" id="Seg_9445" s="T260">тогда</ta>
            <ta e="T262" id="Seg_9446" s="T261">сушить-RES-PST.[3SG]</ta>
            <ta e="T263" id="Seg_9447" s="T262">трава.[NOM.SG]</ta>
            <ta e="T264" id="Seg_9448" s="T263">я.NOM</ta>
            <ta e="T265" id="Seg_9449" s="T264">этот-ACC</ta>
            <ta e="T266" id="Seg_9450" s="T265">собирать-PST-1SG</ta>
            <ta e="T267" id="Seg_9451" s="T266">копна-LAT</ta>
            <ta e="T268" id="Seg_9452" s="T267">тогда</ta>
            <ta e="T269" id="Seg_9453" s="T268">лошадь.[NOM.SG]</ta>
            <ta e="T270" id="Seg_9454" s="T269">взять-PST-1SG</ta>
            <ta e="T271" id="Seg_9455" s="T270">носить-PST-1SG</ta>
            <ta e="T273" id="Seg_9456" s="T272">класть-PST-1SG</ta>
            <ta e="T274" id="Seg_9457" s="T273">зарод-LAT</ta>
            <ta e="T278" id="Seg_9458" s="T277">тот.[NOM.SG]</ta>
            <ta e="T279" id="Seg_9459" s="T278">год-LOC.ADV</ta>
            <ta e="T280" id="Seg_9460" s="T279">черный.[NOM.SG]</ta>
            <ta e="T281" id="Seg_9461" s="T280">ягода.[NOM.SG]</ta>
            <ta e="T282" id="Seg_9462" s="T281">NEG.EX.[3SG]</ta>
            <ta e="T283" id="Seg_9463" s="T282">а</ta>
            <ta e="T284" id="Seg_9464" s="T283">красный.[NOM.SG]</ta>
            <ta e="T285" id="Seg_9465" s="T284">ягода.[NOM.SG]</ta>
            <ta e="T286" id="Seg_9466" s="T285">быть-PST.[3SG]</ta>
            <ta e="T288" id="Seg_9467" s="T287">люди.[NOM.SG]</ta>
            <ta e="T289" id="Seg_9468" s="T288">много</ta>
            <ta e="T290" id="Seg_9469" s="T289">носить-PST-3PL</ta>
            <ta e="T291" id="Seg_9470" s="T290">дом-LAT/LOC.3SG</ta>
            <ta e="T292" id="Seg_9471" s="T291">ягода-PL</ta>
            <ta e="T294" id="Seg_9472" s="T293">кедровый.орех.[NOM.SG]</ta>
            <ta e="T295" id="Seg_9473" s="T294">много</ta>
            <ta e="T296" id="Seg_9474" s="T295">быть-PST.[3SG]</ta>
            <ta e="T297" id="Seg_9475" s="T296">PTCL</ta>
            <ta e="T299" id="Seg_9476" s="T298">люди.[NOM.SG]</ta>
            <ta e="T300" id="Seg_9477" s="T299">очень</ta>
            <ta e="T301" id="Seg_9478" s="T300">много</ta>
            <ta e="T302" id="Seg_9479" s="T301">собирать-PST-3PL</ta>
            <ta e="T303" id="Seg_9480" s="T302">кедровый.орех.[NOM.SG]</ta>
            <ta e="T307" id="Seg_9481" s="T306">три.[NOM.SG]</ta>
            <ta e="T308" id="Seg_9482" s="T307">год.[NOM.SG]</ta>
            <ta e="T309" id="Seg_9483" s="T308">NEG.EX-PST.[3SG]</ta>
            <ta e="T310" id="Seg_9484" s="T309">же</ta>
            <ta e="T311" id="Seg_9485" s="T310">кедровый.орех.[NOM.SG]</ta>
            <ta e="T315" id="Seg_9486" s="T314">кедровый.орех.[NOM.SG]</ta>
            <ta e="T316" id="Seg_9487" s="T315">трясти-MULT-INF.LAT</ta>
            <ta e="T319" id="Seg_9488" s="T318">этот-PL</ta>
            <ta e="T320" id="Seg_9489" s="T319">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T321" id="Seg_9490" s="T320">упасть-MOM-PST-3PL</ta>
            <ta e="T322" id="Seg_9491" s="T321">а</ta>
            <ta e="T323" id="Seg_9492" s="T322">люди.[NOM.SG]</ta>
            <ta e="T324" id="Seg_9493" s="T323">собирать-DUR-3PL</ta>
            <ta e="T328" id="Seg_9494" s="T327">тот.[NOM.SG]</ta>
            <ta e="T329" id="Seg_9495" s="T328">год-LOC.ADV</ta>
            <ta e="T330" id="Seg_9496" s="T329">черемуха.[NOM.SG]</ta>
            <ta e="T331" id="Seg_9497" s="T330">ягода.[NOM.SG]</ta>
            <ta e="T332" id="Seg_9498" s="T331">NEG.EX-PST.[3SG]</ta>
            <ta e="T333" id="Seg_9499" s="T332">тоже</ta>
            <ta e="T337" id="Seg_9500" s="T336">тот.[NOM.SG]</ta>
            <ta e="T338" id="Seg_9501" s="T337">зима.[NOM.SG]</ta>
            <ta e="T339" id="Seg_9502" s="T338">очень</ta>
            <ta e="T340" id="Seg_9503" s="T339">пшеница.[NOM.SG]</ta>
            <ta e="T341" id="Seg_9504" s="T340">хороший.[NOM.SG]</ta>
            <ta e="T342" id="Seg_9505" s="T341">быть-PST.[3SG]</ta>
            <ta e="T344" id="Seg_9506" s="T343">и</ta>
            <ta e="T345" id="Seg_9507" s="T344">картофель.[NOM.SG]</ta>
            <ta e="T346" id="Seg_9508" s="T345">хороший.[NOM.SG]</ta>
            <ta e="T347" id="Seg_9509" s="T346">быть-PST.[3SG]</ta>
            <ta e="T348" id="Seg_9510" s="T347">много</ta>
            <ta e="T349" id="Seg_9511" s="T348">расти-PST.[3SG]</ta>
            <ta e="T350" id="Seg_9512" s="T349">большой.[NOM.SG]</ta>
            <ta e="T351" id="Seg_9513" s="T350">быть-PST.[3SG]</ta>
            <ta e="T355" id="Seg_9514" s="T354">снег.[NOM.SG]</ta>
            <ta e="T356" id="Seg_9515" s="T355">хлеб.[NOM.SG]</ta>
            <ta e="T358" id="Seg_9516" s="T357">печь-PRS-3PL</ta>
            <ta e="T359" id="Seg_9517" s="T358">люди.[NOM.SG]</ta>
            <ta e="T360" id="Seg_9518" s="T359">очень</ta>
            <ta e="T362" id="Seg_9519" s="T361">лук.[NOM.SG]</ta>
            <ta e="T363" id="Seg_9520" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_9521" s="T363">умереть-RES-PST.[3SG]</ta>
            <ta e="T365" id="Seg_9522" s="T364">огород-LOC</ta>
            <ta e="T367" id="Seg_9523" s="T366">а</ta>
            <ta e="T371" id="Seg_9524" s="T370">а</ta>
            <ta e="T372" id="Seg_9525" s="T371">лес-LOC</ta>
            <ta e="T373" id="Seg_9526" s="T372">лук.[NOM.SG]</ta>
            <ta e="T374" id="Seg_9527" s="T373">хороший.[NOM.SG]</ta>
            <ta e="T375" id="Seg_9528" s="T374">быть-PST.[3SG]</ta>
            <ta e="T379" id="Seg_9529" s="T378">я.NOM</ta>
            <ta e="T382" id="Seg_9530" s="T381">рвать-PST-1SG</ta>
            <ta e="T383" id="Seg_9531" s="T382">зарезать-PST-1SG</ta>
            <ta e="T384" id="Seg_9532" s="T383">и</ta>
            <ta e="T385" id="Seg_9533" s="T384">сушить-DUR-PST.[3SG]</ta>
            <ta e="T387" id="Seg_9534" s="T386">а</ta>
            <ta e="T388" id="Seg_9535" s="T387">сейчас</ta>
            <ta e="T389" id="Seg_9536" s="T388">кипятить-PRS-1SG</ta>
            <ta e="T390" id="Seg_9537" s="T389">мясо-INS</ta>
            <ta e="T391" id="Seg_9538" s="T390">картофель-INS</ta>
            <ta e="T398" id="Seg_9539" s="T397">камасинец-PL-LOC</ta>
            <ta e="T399" id="Seg_9540" s="T398">один.[NOM.SG]</ta>
            <ta e="T400" id="Seg_9541" s="T399">мужчина.[NOM.SG]</ta>
            <ta e="T401" id="Seg_9542" s="T400">умереть-RES-PST.[3SG]</ta>
            <ta e="T402" id="Seg_9543" s="T401">этот-PL</ta>
            <ta e="T403" id="Seg_9544" s="T402">PTCL</ta>
            <ta e="T404" id="Seg_9545" s="T403">копать-PST-3PL</ta>
            <ta e="T405" id="Seg_9546" s="T404">земля.[NOM.SG]</ta>
            <ta e="T406" id="Seg_9547" s="T405">тогда</ta>
            <ta e="T407" id="Seg_9548" s="T406">делать-PST-3PL</ta>
            <ta e="T411" id="Seg_9549" s="T410">тогда</ta>
            <ta e="T412" id="Seg_9550" s="T411">чум.[NOM.SG]</ta>
            <ta e="T413" id="Seg_9551" s="T412">делать-PST-3PL</ta>
            <ta e="T414" id="Seg_9552" s="T413">крест.[NOM.SG]</ta>
            <ta e="T415" id="Seg_9553" s="T414">делать-PST-3PL</ta>
            <ta e="T416" id="Seg_9554" s="T415">чум-LAT</ta>
            <ta e="T417" id="Seg_9555" s="T416">этот-ACC</ta>
            <ta e="T418" id="Seg_9556" s="T417">класть-PST-3PL</ta>
            <ta e="T422" id="Seg_9557" s="T421">этот.[NOM.SG]</ta>
            <ta e="T423" id="Seg_9558" s="T422">этот-LAT</ta>
            <ta e="T424" id="Seg_9559" s="T423">трубка.[NOM.SG]</ta>
            <ta e="T425" id="Seg_9560" s="T424">съесть-PST-3PL</ta>
            <ta e="T426" id="Seg_9561" s="T425">табак.[NOM.SG]</ta>
            <ta e="T427" id="Seg_9562" s="T426">съесть-PST-3PL</ta>
            <ta e="T429" id="Seg_9563" s="T428">ружье.[NOM.SG]</ta>
            <ta e="T430" id="Seg_9564" s="T429">съесть-PST-3PL</ta>
            <ta e="T431" id="Seg_9565" s="T430">PTCL</ta>
            <ta e="T433" id="Seg_9566" s="T432">котел.[NOM.SG]</ta>
            <ta e="T434" id="Seg_9567" s="T433">съесть-PST-3PL</ta>
            <ta e="T435" id="Seg_9568" s="T434">класть-PST-3PL</ta>
            <ta e="T437" id="Seg_9569" s="T436">этот.[NOM.SG]</ta>
            <ta e="T438" id="Seg_9570" s="T437">там</ta>
            <ta e="T439" id="Seg_9571" s="T438">кипятить-FUT-3SG</ta>
            <ta e="T440" id="Seg_9572" s="T439">и</ta>
            <ta e="T441" id="Seg_9573" s="T440">есть-FUT-3SG</ta>
            <ta e="T445" id="Seg_9574" s="T444">тогда</ta>
            <ta e="T446" id="Seg_9575" s="T445">этот.[NOM.SG]</ta>
            <ta e="T447" id="Seg_9576" s="T446">мужчина-ACC</ta>
            <ta e="T448" id="Seg_9577" s="T447">нести-PST-3PL</ta>
            <ta e="T450" id="Seg_9578" s="T449">место-LAT</ta>
            <ta e="T451" id="Seg_9579" s="T450">класть-PST-3PL</ta>
            <ta e="T452" id="Seg_9580" s="T451">земля-INS</ta>
            <ta e="T453" id="Seg_9581" s="T452">PTCL</ta>
            <ta e="T454" id="Seg_9582" s="T453">лить-PST-3PL</ta>
            <ta e="T456" id="Seg_9583" s="T455">тогда</ta>
            <ta e="T457" id="Seg_9584" s="T456">чум-LAT/LOC.3SG</ta>
            <ta e="T458" id="Seg_9585" s="T457">прийти-PST-3PL</ta>
            <ta e="T460" id="Seg_9586" s="T459">вечер-LOC.ADV</ta>
            <ta e="T461" id="Seg_9587" s="T460">PTCL</ta>
            <ta e="T462" id="Seg_9588" s="T461">собака.[NOM.SG]</ta>
            <ta e="T463" id="Seg_9589" s="T462">завязать-PST-3PL</ta>
            <ta e="T464" id="Seg_9590" s="T463">и</ta>
            <ta e="T465" id="Seg_9591" s="T464">лошадь.[NOM.SG]</ta>
            <ta e="T467" id="Seg_9592" s="T466">тогда</ta>
            <ta e="T468" id="Seg_9593" s="T467">пойти-PST-3PL</ta>
            <ta e="T470" id="Seg_9594" s="T469">ой</ta>
            <ta e="T471" id="Seg_9595" s="T470">прийти-PRS.[3SG]</ta>
            <ta e="T472" id="Seg_9596" s="T471">бежать-DUR-3PL</ta>
            <ta e="T473" id="Seg_9597" s="T472">дом-LAT</ta>
            <ta e="T474" id="Seg_9598" s="T473">PTCL</ta>
            <ta e="T476" id="Seg_9599" s="T475">коса.[NOM.SG]</ta>
            <ta e="T477" id="Seg_9600" s="T476">дверь-LOC</ta>
            <ta e="T478" id="Seg_9601" s="T477">класть-FUT-3PL</ta>
            <ta e="T482" id="Seg_9602" s="T481">собака.[NOM.SG]</ta>
            <ta e="T483" id="Seg_9603" s="T482">завязать-PST-3PL</ta>
            <ta e="T484" id="Seg_9604" s="T483">черный.[NOM.SG]</ta>
            <ta e="T485" id="Seg_9605" s="T484">и</ta>
            <ta e="T486" id="Seg_9606" s="T485">глаз-NOM/GEN.3SG</ta>
            <ta e="T487" id="Seg_9607" s="T486">один.[3SG]</ta>
            <ta e="T488" id="Seg_9608" s="T487">два.[NOM.SG]</ta>
            <ta e="T489" id="Seg_9609" s="T488">три.[NOM.SG]</ta>
            <ta e="T490" id="Seg_9610" s="T489">четыре.[NOM.SG]</ta>
            <ta e="T491" id="Seg_9611" s="T490">четыре.[NOM.SG]</ta>
            <ta e="T492" id="Seg_9612" s="T491">глаз-NOM/GEN.3SG</ta>
            <ta e="T496" id="Seg_9613" s="T495">коса.[NOM.SG]</ta>
            <ta e="T497" id="Seg_9614" s="T496">поставить-PST-3PL</ta>
            <ta e="T498" id="Seg_9615" s="T497">чтобы</ta>
            <ta e="T499" id="Seg_9616" s="T498">%змея.[NOM.SG]</ta>
            <ta e="T501" id="Seg_9617" s="T500">зарезать-MOM-PST.[3SG]</ta>
            <ta e="T505" id="Seg_9618" s="T504">один</ta>
            <ta e="T506" id="Seg_9619" s="T505">один.[NOM.SG]</ta>
            <ta e="T507" id="Seg_9620" s="T506">мужчина.[NOM.SG]</ta>
            <ta e="T509" id="Seg_9621" s="T508">мы.LAT</ta>
            <ta e="T510" id="Seg_9622" s="T509">болеть-PST.[3SG]</ta>
            <ta e="T511" id="Seg_9623" s="T510">болеть-PST.[3SG]</ta>
            <ta e="T512" id="Seg_9624" s="T511">тогда</ta>
            <ta e="T514" id="Seg_9625" s="T513">умереть-RES-PST.[3SG]</ta>
            <ta e="T516" id="Seg_9626" s="T515">а</ta>
            <ta e="T517" id="Seg_9627" s="T516">мы.NOM</ta>
            <ta e="T519" id="Seg_9628" s="T518">пойти-PST-1PL</ta>
            <ta e="T1139" id="Seg_9629" s="T519">Агинское</ta>
            <ta e="T520" id="Seg_9630" s="T1139">поселение-LAT</ta>
            <ta e="T522" id="Seg_9631" s="T521">бумага.[NOM.SG]</ta>
            <ta e="T523" id="Seg_9632" s="T522">рука-LAT/LOC.3SG</ta>
            <ta e="T525" id="Seg_9633" s="T524">рука-LAT/LOC.3SG</ta>
            <ta e="T526" id="Seg_9634" s="T525">взять-INF.LAT</ta>
            <ta e="T527" id="Seg_9635" s="T526">и</ta>
            <ta e="T528" id="Seg_9636" s="T527">голова-LAT/LOC.3SG</ta>
            <ta e="T530" id="Seg_9637" s="T529">тогда</ta>
            <ta e="T531" id="Seg_9638" s="T530">священник.[NOM.SG]</ta>
            <ta e="T532" id="Seg_9639" s="T531">мы.LAT</ta>
            <ta e="T533" id="Seg_9640" s="T532">NEG</ta>
            <ta e="T534" id="Seg_9641" s="T533">дать-PST.[3SG]</ta>
            <ta e="T536" id="Seg_9642" s="T535">мы.NOM</ta>
            <ta e="T537" id="Seg_9643" s="T536">водка.[NOM.SG]</ta>
            <ta e="T538" id="Seg_9644" s="T537">взять-PST-1PL</ta>
            <ta e="T540" id="Seg_9645" s="T539">и</ta>
            <ta e="T541" id="Seg_9646" s="T540">дом-LAT/LOC.1SG</ta>
            <ta e="T542" id="Seg_9647" s="T541">прийти-PST-1PL</ta>
            <ta e="T544" id="Seg_9648" s="T543">тогда</ta>
            <ta e="T545" id="Seg_9649" s="T544">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T546" id="Seg_9650" s="T545">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T548" id="Seg_9651" s="T547">взять-PST-3PL</ta>
            <ta e="T549" id="Seg_9652" s="T548">этот.[NOM.SG]</ta>
            <ta e="T550" id="Seg_9653" s="T549">водка.[NOM.SG]</ta>
            <ta e="T1137" id="Seg_9654" s="T550">пойти-CVB</ta>
            <ta e="T551" id="Seg_9655" s="T1137">исчезнуть-PST-3PL</ta>
            <ta e="T552" id="Seg_9656" s="T551">пить-INF.LAT</ta>
            <ta e="T554" id="Seg_9657" s="T553">а</ta>
            <ta e="T555" id="Seg_9658" s="T554">мы.NOM</ta>
            <ta e="T556" id="Seg_9659" s="T555">ребенок-PL</ta>
            <ta e="T557" id="Seg_9660" s="T556">мы.NOM</ta>
            <ta e="T558" id="Seg_9661" s="T557">тоже</ta>
            <ta e="T559" id="Seg_9662" s="T558">NEG</ta>
            <ta e="T560" id="Seg_9663" s="T559">остаться-FUT-1SG</ta>
            <ta e="T561" id="Seg_9664" s="T560">дом-LAT</ta>
            <ta e="T564" id="Seg_9665" s="T563">пойти-CONJ-1DU</ta>
            <ta e="T566" id="Seg_9666" s="T565">а</ta>
            <ta e="T567" id="Seg_9667" s="T566">я.NOM</ta>
            <ta e="T568" id="Seg_9668" s="T567">сказать-PST-1SG</ta>
            <ta e="T569" id="Seg_9669" s="T568">ложиться-IMP.2SG</ta>
            <ta e="T570" id="Seg_9670" s="T569">спать-EP-IMP.2SG</ta>
            <ta e="T572" id="Seg_9671" s="T571">а</ta>
            <ta e="T573" id="Seg_9672" s="T572">я.NOM</ta>
            <ta e="T574" id="Seg_9673" s="T573">ложиться-FUT-1SG</ta>
            <ta e="T575" id="Seg_9674" s="T574">вы.ACC</ta>
            <ta e="T577" id="Seg_9675" s="T576">когда</ta>
            <ta e="T578" id="Seg_9676" s="T577">я.LAT</ta>
            <ta e="T579" id="Seg_9677" s="T578">хватит</ta>
            <ta e="T580" id="Seg_9678" s="T579">а</ta>
            <ta e="T581" id="Seg_9679" s="T580">вы.NOM</ta>
            <ta e="T582" id="Seg_9680" s="T581">бежать-MOM-CVB</ta>
            <ta e="T584" id="Seg_9681" s="T583">тогда</ta>
            <ta e="T585" id="Seg_9682" s="T584">утро-LOC.ADV</ta>
            <ta e="T586" id="Seg_9683" s="T585">встать-PST-1SG</ta>
            <ta e="T588" id="Seg_9684" s="T587">вот</ta>
            <ta e="T589" id="Seg_9685" s="T588">видеть-PRS-2SG</ta>
            <ta e="T590" id="Seg_9686" s="T589">этот.[NOM.SG]</ta>
            <ta e="T591" id="Seg_9687" s="T590">ложиться-DUR.[3SG]</ta>
            <ta e="T592" id="Seg_9688" s="T591">и</ta>
            <ta e="T594" id="Seg_9689" s="T593">и</ta>
            <ta e="T595" id="Seg_9690" s="T594">мы.NOM</ta>
            <ta e="T596" id="Seg_9691" s="T595">спать-PST-1PL</ta>
            <ta e="T600" id="Seg_9692" s="T599">тогда</ta>
            <ta e="T601" id="Seg_9693" s="T600">этот.[NOM.SG]</ta>
            <ta e="T602" id="Seg_9694" s="T601">умереть-RES-PST.[3SG]</ta>
            <ta e="T603" id="Seg_9695" s="T602">этот-ACC</ta>
            <ta e="T604" id="Seg_9696" s="T603">мыть-PST-3PL</ta>
            <ta e="T606" id="Seg_9697" s="T605">рубашка.[NOM.SG]</ta>
            <ta e="T607" id="Seg_9698" s="T606">надеть-PST-3PL</ta>
            <ta e="T608" id="Seg_9699" s="T607">штаны.[NOM.SG]</ta>
            <ta e="T609" id="Seg_9700" s="T608">надеть-PST-3PL</ta>
            <ta e="T611" id="Seg_9701" s="T610">сапог-PL</ta>
            <ta e="T612" id="Seg_9702" s="T611">шить-PST-3PL</ta>
            <ta e="T613" id="Seg_9703" s="T612">надеть-PST-3PL</ta>
            <ta e="T615" id="Seg_9704" s="T614">тогда</ta>
            <ta e="T616" id="Seg_9705" s="T615">унести-PST-3PL</ta>
            <ta e="T619" id="Seg_9706" s="T618">земля-INS</ta>
            <ta e="T620" id="Seg_9707" s="T619">лить-PST-3PL</ta>
            <ta e="T621" id="Seg_9708" s="T620">этот-ACC</ta>
            <ta e="T623" id="Seg_9709" s="T622">прийти-PST-3PL</ta>
            <ta e="T624" id="Seg_9710" s="T623">дом-LAT/LOC.3SG</ta>
            <ta e="T626" id="Seg_9711" s="T625">водка.[NOM.SG]</ta>
            <ta e="T627" id="Seg_9712" s="T626">пить-PST-3PL</ta>
            <ta e="T628" id="Seg_9713" s="T627">хлеб.[NOM.SG]</ta>
            <ta e="T629" id="Seg_9714" s="T628">есть-PST-3PL</ta>
            <ta e="T630" id="Seg_9715" s="T629">мясо.[NOM.SG]</ta>
            <ta e="T631" id="Seg_9716" s="T630">съесть-PST-3PL</ta>
            <ta e="T633" id="Seg_9717" s="T632">и</ta>
            <ta e="T634" id="Seg_9718" s="T633">дом-LAT/LOC.3SG</ta>
            <ta e="T1138" id="Seg_9719" s="T634">пойти-CVB</ta>
            <ta e="T635" id="Seg_9720" s="T1138">исчезнуть-PST-3PL</ta>
            <ta e="T639" id="Seg_9721" s="T638">я.NOM</ta>
            <ta e="T640" id="Seg_9722" s="T639">один.[NOM.SG]</ta>
            <ta e="T641" id="Seg_9723" s="T640">раз.[NOM.SG]</ta>
            <ta e="T642" id="Seg_9724" s="T641">прийти</ta>
            <ta e="T643" id="Seg_9725" s="T642">прийти-PST-1SG</ta>
            <ta e="T1140" id="Seg_9726" s="T643">Агинское</ta>
            <ta e="T644" id="Seg_9727" s="T1140">поселение-LOC</ta>
            <ta e="T646" id="Seg_9728" s="T645">прийти-PST-1SG</ta>
            <ta e="T647" id="Seg_9729" s="T646">Пермяково-LAT</ta>
            <ta e="T648" id="Seg_9730" s="T647">тогда</ta>
            <ta e="T649" id="Seg_9731" s="T648">дом-LAT/LOC.3SG</ta>
            <ta e="T650" id="Seg_9732" s="T649">прийти-PST-1SG</ta>
            <ta e="T652" id="Seg_9733" s="T651">мельница-LOC</ta>
            <ta e="T653" id="Seg_9734" s="T652">там</ta>
            <ta e="T654" id="Seg_9735" s="T653">%%-1SG</ta>
            <ta e="T655" id="Seg_9736" s="T654">кто.[NOM.SG]=INDEF</ta>
            <ta e="T657" id="Seg_9737" s="T656">показать-MOM-PST-3PL</ta>
            <ta e="T659" id="Seg_9738" s="T658">а</ta>
            <ta e="T660" id="Seg_9739" s="T659">я.NOM</ta>
            <ta e="T661" id="Seg_9740" s="T660">прийти-PST-1SG</ta>
            <ta e="T662" id="Seg_9741" s="T661">и</ta>
            <ta e="T664" id="Seg_9742" s="T663">тогда</ta>
            <ta e="T665" id="Seg_9743" s="T664">%%-FUT-1SG</ta>
            <ta e="T666" id="Seg_9744" s="T665">PTCL</ta>
            <ta e="T667" id="Seg_9745" s="T666">деньги.[NOM.SG]</ta>
            <ta e="T668" id="Seg_9746" s="T667">много</ta>
            <ta e="T669" id="Seg_9747" s="T668">стать-FUT-3SG</ta>
            <ta e="T673" id="Seg_9748" s="T672">прийти-PST-1SG</ta>
            <ta e="T675" id="Seg_9749" s="T674">дом-LAT/LOC.3SG</ta>
            <ta e="T676" id="Seg_9750" s="T675">кто-LAT/LOC.3SG</ta>
            <ta e="T677" id="Seg_9751" s="T676">NEG</ta>
            <ta e="T678" id="Seg_9752" s="T677">видеть-PST-1SG</ta>
            <ta e="T682" id="Seg_9753" s="T681">я.NOM</ta>
            <ta e="T683" id="Seg_9754" s="T682">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T684" id="Seg_9755" s="T683">болеть-PRS.[3SG]</ta>
            <ta e="T685" id="Seg_9756" s="T684">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T686" id="Seg_9757" s="T685">болеть-PRS.[3SG]</ta>
            <ta e="T687" id="Seg_9758" s="T686">а</ta>
            <ta e="T688" id="Seg_9759" s="T687">ты.NOM</ta>
            <ta e="T689" id="Seg_9760" s="T688">что.[NOM.SG]</ta>
            <ta e="T690" id="Seg_9761" s="T689">болеть-PRS.[3SG]</ta>
            <ta e="T691" id="Seg_9762" s="T690">а</ta>
            <ta e="T692" id="Seg_9763" s="T691">я.GEN</ta>
            <ta e="T693" id="Seg_9764" s="T692">зуб-NOM/GEN/ACC.1SG</ta>
            <ta e="T694" id="Seg_9765" s="T693">болеть-PST.[3SG]</ta>
            <ta e="T695" id="Seg_9766" s="T694">болеть-DUR.[3SG]</ta>
            <ta e="T696" id="Seg_9767" s="T695">и</ta>
            <ta e="T697" id="Seg_9768" s="T696">нос-NOM/GEN/ACC.1SG</ta>
            <ta e="T698" id="Seg_9769" s="T697">болеть-DUR.[3SG]</ta>
            <ta e="T702" id="Seg_9770" s="T701">Иванович-EP-GEN</ta>
            <ta e="T703" id="Seg_9771" s="T702">PTCL</ta>
            <ta e="T704" id="Seg_9772" s="T703">почка-NOM/GEN.3SG</ta>
            <ta e="T705" id="Seg_9773" s="T704">болеть-PRS.[3SG]</ta>
            <ta e="T706" id="Seg_9774" s="T705">и</ta>
            <ta e="T707" id="Seg_9775" s="T706">нога-NOM/GEN.3SG</ta>
            <ta e="T708" id="Seg_9776" s="T707">болеть-PRS.[3SG]</ta>
            <ta e="T709" id="Seg_9777" s="T708">и</ta>
            <ta e="T711" id="Seg_9778" s="T710">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T712" id="Seg_9779" s="T711">болеть-DUR-3PL</ta>
            <ta e="T713" id="Seg_9780" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_9781" s="T713">сильно</ta>
            <ta e="T715" id="Seg_9782" s="T714">болеть-PRS.[3SG]</ta>
            <ta e="T719" id="Seg_9783" s="T718">надо</ta>
            <ta e="T722" id="Seg_9784" s="T721">вылечить-INF.LAT</ta>
            <ta e="T723" id="Seg_9785" s="T722">этот.[NOM.SG]</ta>
            <ta e="T725" id="Seg_9786" s="T724">три.[NOM.SG]</ta>
            <ta e="T726" id="Seg_9787" s="T725">мужчина.[NOM.SG]</ta>
            <ta e="T728" id="Seg_9788" s="T727">доктор.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9789" s="T728">надо</ta>
            <ta e="T730" id="Seg_9790" s="T729">принести-INF.LAT</ta>
            <ta e="T731" id="Seg_9791" s="T730">JUSS</ta>
            <ta e="T732" id="Seg_9792" s="T731">вылечить-FUT-3SG</ta>
            <ta e="T733" id="Seg_9793" s="T732">этот.[NOM.SG]</ta>
            <ta e="T734" id="Seg_9794" s="T733">этот-ACC.PL</ta>
            <ta e="T738" id="Seg_9795" s="T737">что=INDEF</ta>
            <ta e="T739" id="Seg_9796" s="T738">дать-FUT-3SG</ta>
            <ta e="T740" id="Seg_9797" s="T739">пить-INF.LAT</ta>
            <ta e="T741" id="Seg_9798" s="T740">или</ta>
            <ta e="T742" id="Seg_9799" s="T741">что=INDEF</ta>
            <ta e="T743" id="Seg_9800" s="T742">дать-FUT-3SG</ta>
            <ta e="T744" id="Seg_9801" s="T743">тереть-INF.LAT</ta>
            <ta e="T748" id="Seg_9802" s="T747">этот-PL</ta>
            <ta e="T749" id="Seg_9803" s="T748">PTCL</ta>
            <ta e="T750" id="Seg_9804" s="T749">сильно</ta>
            <ta e="T751" id="Seg_9805" s="T750">мерзнуть-FACT-PST-3PL</ta>
            <ta e="T752" id="Seg_9806" s="T751">а</ta>
            <ta e="T753" id="Seg_9807" s="T752">сейчас</ta>
            <ta e="T754" id="Seg_9808" s="T753">очень</ta>
            <ta e="T755" id="Seg_9809" s="T754">сильно</ta>
            <ta e="T756" id="Seg_9810" s="T755">болеть-DUR-3PL</ta>
            <ta e="T760" id="Seg_9811" s="T759">Марейка.[NOM.SG]</ta>
            <ta e="T761" id="Seg_9812" s="T760">бабушка-LAT</ta>
            <ta e="T762" id="Seg_9813" s="T761">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T764" id="Seg_9814" s="T763">бабушка-LAT</ta>
            <ta e="T765" id="Seg_9815" s="T764">такой.[NOM.SG]</ta>
            <ta e="T766" id="Seg_9816" s="T765">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T767" id="Seg_9817" s="T766">и</ta>
            <ta e="T768" id="Seg_9818" s="T767">глаз-NOM/GEN.3SG</ta>
            <ta e="T769" id="Seg_9819" s="T768">такой.[NOM.SG]</ta>
            <ta e="T770" id="Seg_9820" s="T769">бабушка-LAT/LOC.3SG</ta>
            <ta e="T774" id="Seg_9821" s="T773">жить-PST-3PL</ta>
            <ta e="T775" id="Seg_9822" s="T774">женщина.[NOM.SG]</ta>
            <ta e="T776" id="Seg_9823" s="T775">и</ta>
            <ta e="T777" id="Seg_9824" s="T776">мужчина.[NOM.SG]</ta>
            <ta e="T779" id="Seg_9825" s="T778">%%</ta>
            <ta e="T780" id="Seg_9826" s="T779">вода-LOC</ta>
            <ta e="T781" id="Seg_9827" s="T780">край-LAT/LOC.3SG</ta>
            <ta e="T783" id="Seg_9828" s="T782">женщина.[NOM.SG]</ta>
            <ta e="T784" id="Seg_9829" s="T783">прясть-CVB</ta>
            <ta e="T785" id="Seg_9830" s="T784">сидеть-DUR-PST.[3SG]</ta>
            <ta e="T787" id="Seg_9831" s="T786">а</ta>
            <ta e="T788" id="Seg_9832" s="T787">мужчина.[NOM.SG]</ta>
            <ta e="T789" id="Seg_9833" s="T788">идти-PST.[3SG]</ta>
            <ta e="T790" id="Seg_9834" s="T789">вода-LAT</ta>
            <ta e="T791" id="Seg_9835" s="T790">рыба.[NOM.SG]</ta>
            <ta e="T793" id="Seg_9836" s="T792">ловить-INF.LAT</ta>
            <ta e="T795" id="Seg_9837" s="T794">один</ta>
            <ta e="T796" id="Seg_9838" s="T795">раз.[NOM.SG]</ta>
            <ta e="T797" id="Seg_9839" s="T796">один.[NOM.SG]</ta>
            <ta e="T798" id="Seg_9840" s="T797">раз.[NOM.SG]</ta>
            <ta e="T799" id="Seg_9841" s="T798">пойти-PST.[3SG]</ta>
            <ta e="T800" id="Seg_9842" s="T799">тогда</ta>
            <ta e="T801" id="Seg_9843" s="T800">что.[NOM.SG]=INDEF</ta>
            <ta e="T802" id="Seg_9844" s="T801">NEG</ta>
            <ta e="T803" id="Seg_9845" s="T802">ловить-PST.[3SG]</ta>
            <ta e="T804" id="Seg_9846" s="T803">один.[NOM.SG]</ta>
            <ta e="T805" id="Seg_9847" s="T804">рыба.[NOM.SG]</ta>
            <ta e="T806" id="Seg_9848" s="T805">ловить-PST.[3SG]</ta>
            <ta e="T807" id="Seg_9849" s="T806">красивый.[NOM.SG]</ta>
            <ta e="T809" id="Seg_9850" s="T808">я.NOM</ta>
            <ta e="T810" id="Seg_9851" s="T809">спросить-DUR.[3SG]</ta>
            <ta e="T814" id="Seg_9852" s="T813">рыба.[NOM.SG]</ta>
            <ta e="T815" id="Seg_9853" s="T814">красивый.[NOM.SG]</ta>
            <ta e="T816" id="Seg_9854" s="T815">говорить-PRS.[3SG]</ta>
            <ta e="T817" id="Seg_9855" s="T816">мужчина-LAT</ta>
            <ta e="T819" id="Seg_9856" s="T818">язык-3SG-INS</ta>
            <ta e="T821" id="Seg_9857" s="T820">пускать-CVB</ta>
            <ta e="T822" id="Seg_9858" s="T821">я.LAT</ta>
            <ta e="T823" id="Seg_9859" s="T822">вода-LAT</ta>
            <ta e="T825" id="Seg_9860" s="T824">я.NOM</ta>
            <ta e="T826" id="Seg_9861" s="T825">посылать-PST-1SG</ta>
            <ta e="T828" id="Seg_9862" s="T827">прийти-PST.[3SG]</ta>
            <ta e="T829" id="Seg_9863" s="T828">дом-LAT/LOC.3SG</ta>
            <ta e="T830" id="Seg_9864" s="T829">и</ta>
            <ta e="T831" id="Seg_9865" s="T830">женщина-LAT</ta>
            <ta e="T832" id="Seg_9866" s="T831">говорить-DUR.[3SG]</ta>
            <ta e="T834" id="Seg_9867" s="T833">рыба.[NOM.SG]</ta>
            <ta e="T835" id="Seg_9868" s="T834">PTCL</ta>
            <ta e="T836" id="Seg_9869" s="T835">красивый.[NOM.SG]</ta>
            <ta e="T837" id="Seg_9870" s="T836">быть-PST.[3SG]</ta>
            <ta e="T838" id="Seg_9871" s="T837">говорить-PST.[3SG]</ta>
            <ta e="T839" id="Seg_9872" s="T838">как</ta>
            <ta e="T840" id="Seg_9873" s="T839">мужчина.[NOM.SG]</ta>
            <ta e="T844" id="Seg_9874" s="T843">тогда</ta>
            <ta e="T845" id="Seg_9875" s="T844">этот.[NOM.SG]</ta>
            <ta e="T846" id="Seg_9876" s="T845">женщина.[NOM.SG]</ta>
            <ta e="T847" id="Seg_9877" s="T846">INCH</ta>
            <ta e="T848" id="Seg_9878" s="T847">ругать-INF.LAT</ta>
            <ta e="T850" id="Seg_9879" s="T849">что.[NOM.SG]</ta>
            <ta e="T851" id="Seg_9880" s="T850">ты.NOM</ta>
            <ta e="T852" id="Seg_9881" s="T851">что.[NOM.SG]=INDEF</ta>
            <ta e="T853" id="Seg_9882" s="T852">NEG</ta>
            <ta e="T854" id="Seg_9883" s="T853">искать-PST-2SG</ta>
            <ta e="T856" id="Seg_9884" s="T855">корыто.[NOM.SG]</ta>
            <ta e="T857" id="Seg_9885" s="T856">мы.LAT</ta>
            <ta e="T858" id="Seg_9886" s="T857">NEG.EX.[3SG]</ta>
            <ta e="T860" id="Seg_9887" s="T859">отверстие-ADJZ</ta>
            <ta e="T862" id="Seg_9888" s="T861">корыто.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9889" s="T863">этот.[NOM.SG]</ta>
            <ta e="T865" id="Seg_9890" s="T864">пойти-PST.[3SG]</ta>
            <ta e="T866" id="Seg_9891" s="T865">опять</ta>
            <ta e="T867" id="Seg_9892" s="T866">вода-LAT</ta>
            <ta e="T869" id="Seg_9893" s="T868">тогда</ta>
            <ta e="T870" id="Seg_9894" s="T869">INCH</ta>
            <ta e="T871" id="Seg_9895" s="T870">кричать-INF.LAT</ta>
            <ta e="T872" id="Seg_9896" s="T871">рыба.[NOM.SG]</ta>
            <ta e="T873" id="Seg_9897" s="T872">прийти-IMP.2SG</ta>
            <ta e="T874" id="Seg_9898" s="T873">здесь</ta>
            <ta e="T875" id="Seg_9899" s="T874">этот.[NOM.SG]</ta>
            <ta e="T876" id="Seg_9900" s="T875">прийти-PST.[3SG]</ta>
            <ta e="T877" id="Seg_9901" s="T876">что.[NOM.SG]</ta>
            <ta e="T878" id="Seg_9902" s="T877">ты.DAT</ta>
            <ta e="T879" id="Seg_9903" s="T878">мужчина.[NOM.SG]</ta>
            <ta e="T880" id="Seg_9904" s="T879">нужно</ta>
            <ta e="T882" id="Seg_9905" s="T881">я.LAT</ta>
            <ta e="T883" id="Seg_9906" s="T882">нужно</ta>
            <ta e="T884" id="Seg_9907" s="T883">корыто.[NOM.SG]</ta>
            <ta e="T886" id="Seg_9908" s="T885">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T887" id="Seg_9909" s="T886">PTCL</ta>
            <ta e="T888" id="Seg_9910" s="T887">ругать-DES-DUR.[3SG]</ta>
            <ta e="T889" id="Seg_9911" s="T888">ну</ta>
            <ta e="T890" id="Seg_9912" s="T889">пойти-EP-IMP.2SG</ta>
            <ta e="T891" id="Seg_9913" s="T890">дом-LAT</ta>
            <ta e="T895" id="Seg_9914" s="T894">этот.[NOM.SG]</ta>
            <ta e="T896" id="Seg_9915" s="T895">женщина.[NOM.SG]</ta>
            <ta e="T897" id="Seg_9916" s="T896">INCH</ta>
            <ta e="T898" id="Seg_9917" s="T897">этот-ACC</ta>
            <ta e="T899" id="Seg_9918" s="T898">ругать-INF.LAT</ta>
            <ta e="T900" id="Seg_9919" s="T899">что.[NOM.SG]</ta>
            <ta e="T901" id="Seg_9920" s="T900">NEG</ta>
            <ta e="T902" id="Seg_9921" s="T901">искать-PST-2SG</ta>
            <ta e="T903" id="Seg_9922" s="T902">дом.[NOM.SG]</ta>
            <ta e="T905" id="Seg_9923" s="T904">мы.GEN</ta>
            <ta e="T906" id="Seg_9924" s="T905">дом.[NOM.SG]</ta>
            <ta e="T907" id="Seg_9925" s="T906">NEG</ta>
            <ta e="T908" id="Seg_9926" s="T907">красивый.[NOM.SG]</ta>
            <ta e="T910" id="Seg_9927" s="T909">этот.[NOM.SG]</ta>
            <ta e="T911" id="Seg_9928" s="T910">опять</ta>
            <ta e="T912" id="Seg_9929" s="T911">пойти-PST.[3SG]</ta>
            <ta e="T913" id="Seg_9930" s="T912">INCH</ta>
            <ta e="T914" id="Seg_9931" s="T913">кричать-INF.LAT</ta>
            <ta e="T915" id="Seg_9932" s="T914">рыба.[NOM.SG]</ta>
            <ta e="T916" id="Seg_9933" s="T915">рыба.[NOM.SG]</ta>
            <ta e="T917" id="Seg_9934" s="T916">прийти-IMP.2SG</ta>
            <ta e="T918" id="Seg_9935" s="T917">здесь</ta>
            <ta e="T920" id="Seg_9936" s="T919">рыба.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9937" s="T920">прийти-PST.[3SG]</ta>
            <ta e="T922" id="Seg_9938" s="T921">что.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9939" s="T922">ты.DAT</ta>
            <ta e="T924" id="Seg_9940" s="T923">нужно</ta>
            <ta e="T926" id="Seg_9941" s="T925">дом.[NOM.SG]</ta>
            <ta e="T927" id="Seg_9942" s="T926">нужно</ta>
            <ta e="T929" id="Seg_9943" s="T928">мы.GEN</ta>
            <ta e="T930" id="Seg_9944" s="T929">дом-NOM/GEN/ACC.1PL</ta>
            <ta e="T931" id="Seg_9945" s="T930">NEG</ta>
            <ta e="T932" id="Seg_9946" s="T931">красивый.[NOM.SG]</ta>
            <ta e="T934" id="Seg_9947" s="T933">пойти-EP-IMP.2SG</ta>
            <ta e="T935" id="Seg_9948" s="T934">дом-LAT-2SG</ta>
            <ta e="T939" id="Seg_9949" s="T938">тогда</ta>
            <ta e="T940" id="Seg_9950" s="T939">опять</ta>
            <ta e="T941" id="Seg_9951" s="T940">этот.[NOM.SG]</ta>
            <ta e="T942" id="Seg_9952" s="T941">женщина.[NOM.SG]</ta>
            <ta e="T944" id="Seg_9953" s="T942">ругать-DES-PST.[3SG]</ta>
            <ta e="T945" id="Seg_9954" s="T944">пойти-EP-IMP.2SG</ta>
            <ta e="T946" id="Seg_9955" s="T945">рыба-LAT</ta>
            <ta e="T947" id="Seg_9956" s="T946">спросить-IMP.2SG</ta>
            <ta e="T948" id="Seg_9957" s="T947">чтобы</ta>
            <ta e="T949" id="Seg_9958" s="T948">я.NOM</ta>
            <ta e="T950" id="Seg_9959" s="T949">стать-PST-1SG</ta>
            <ta e="T951" id="Seg_9960" s="T950">купчиха.INS-INS</ta>
            <ta e="T955" id="Seg_9961" s="T954">тогда</ta>
            <ta e="T956" id="Seg_9962" s="T955">рыба.[NOM.SG]</ta>
            <ta e="T957" id="Seg_9963" s="T956">сказать-PST.[3SG]</ta>
            <ta e="T958" id="Seg_9964" s="T957">пойти-EP-IMP.2SG</ta>
            <ta e="T959" id="Seg_9965" s="T958">дом-LAT-2SG</ta>
            <ta e="T961" id="Seg_9966" s="T960">этот.[NOM.SG]</ta>
            <ta e="T962" id="Seg_9967" s="T961">прийти-PST.[3SG]</ta>
            <ta e="T963" id="Seg_9968" s="T962">а</ta>
            <ta e="T964" id="Seg_9969" s="T963">этот.[NOM.SG]</ta>
            <ta e="T965" id="Seg_9970" s="T964">женщина.[NOM.SG]</ta>
            <ta e="T966" id="Seg_9971" s="T965">опять</ta>
            <ta e="T968" id="Seg_9972" s="T966">ругать-DES-DUR.[3SG]</ta>
            <ta e="T969" id="Seg_9973" s="T968">пойти-EP-IMP.2SG</ta>
            <ta e="T970" id="Seg_9974" s="T969">рыба-LAT</ta>
            <ta e="T971" id="Seg_9975" s="T970">искать-IMP.2SG</ta>
            <ta e="T972" id="Seg_9976" s="T971">чтобы</ta>
            <ta e="T973" id="Seg_9977" s="T972">я.NOM</ta>
            <ta e="T974" id="Seg_9978" s="T973">быть-PST-1SG</ta>
            <ta e="T975" id="Seg_9979" s="T974">дворянка.INS-INS</ta>
            <ta e="T977" id="Seg_9980" s="T976">этот.[NOM.SG]</ta>
            <ta e="T978" id="Seg_9981" s="T977">пойти-PST.[3SG]</ta>
            <ta e="T979" id="Seg_9982" s="T978">рыба-LAT</ta>
            <ta e="T980" id="Seg_9983" s="T979">рыба.[NOM.SG]</ta>
            <ta e="T981" id="Seg_9984" s="T980">кричать-DUR.PST.[3SG]</ta>
            <ta e="T983" id="Seg_9985" s="T982">рыба.[NOM.SG]</ta>
            <ta e="T984" id="Seg_9986" s="T983">прийти-PST.[3SG]</ta>
            <ta e="T985" id="Seg_9987" s="T984">что.[NOM.SG]</ta>
            <ta e="T986" id="Seg_9988" s="T985">ты.DAT</ta>
            <ta e="T987" id="Seg_9989" s="T986">нужно</ta>
            <ta e="T989" id="Seg_9990" s="T988">я.NOM</ta>
            <ta e="T990" id="Seg_9991" s="T989">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T991" id="Seg_9992" s="T990">сказать-IPFVZ.[3SG]</ta>
            <ta e="T992" id="Seg_9993" s="T991">чтобы</ta>
            <ta e="T993" id="Seg_9994" s="T992">я.NOM</ta>
            <ta e="T994" id="Seg_9995" s="T993">дворянка.INS-INS</ta>
            <ta e="T995" id="Seg_9996" s="T994">быть-PST-1SG</ta>
            <ta e="T997" id="Seg_9997" s="T996">пойти-EP-IMP.2SG</ta>
            <ta e="T998" id="Seg_9998" s="T997">дом-LAT-2SG</ta>
            <ta e="T1002" id="Seg_9999" s="T1001">тогда</ta>
            <ta e="T1003" id="Seg_10000" s="T1002">мужчина.[NOM.SG]</ta>
            <ta e="T1004" id="Seg_10001" s="T1003">прийти-PST.[3SG]</ta>
            <ta e="T1005" id="Seg_10002" s="T1004">дом-LAT/LOC.3SG</ta>
            <ta e="T1006" id="Seg_10003" s="T1005">этот.[NOM.SG]</ta>
            <ta e="T1007" id="Seg_10004" s="T1006">INCH</ta>
            <ta e="T1008" id="Seg_10005" s="T1007">опять</ta>
            <ta e="T1009" id="Seg_10006" s="T1008">ругать-INF.LAT</ta>
            <ta e="T1013" id="Seg_10007" s="T1012">и</ta>
            <ta e="T1014" id="Seg_10008" s="T1013">бить-INF.LAT</ta>
            <ta e="T1015" id="Seg_10009" s="T1014">хотеть.PST.M.SG</ta>
            <ta e="T1017" id="Seg_10010" s="T1016">пойти-EP-IMP.2SG</ta>
            <ta e="T1018" id="Seg_10011" s="T1017">опять</ta>
            <ta e="T1019" id="Seg_10012" s="T1018">рыба-LAT</ta>
            <ta e="T1020" id="Seg_10013" s="T1019">искать-IMP.2SG</ta>
            <ta e="T1021" id="Seg_10014" s="T1020">чтобы</ta>
            <ta e="T1022" id="Seg_10015" s="T1021">я.NOM</ta>
            <ta e="T1027" id="Seg_10016" s="T1026">чтобы</ta>
            <ta e="T1028" id="Seg_10017" s="T1027">люди.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_10018" s="T1028">я.LAT</ta>
            <ta e="T1030" id="Seg_10019" s="T1029">рука-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T1035" id="Seg_10020" s="T1034">тогда</ta>
            <ta e="T1036" id="Seg_10021" s="T1035">этот.[NOM.SG]</ta>
            <ta e="T1037" id="Seg_10022" s="T1036">прийти-PST.[3SG]</ta>
            <ta e="T1038" id="Seg_10023" s="T1037">вода-LAT</ta>
            <ta e="T1040" id="Seg_10024" s="T1039">рыба.[NOM.SG]</ta>
            <ta e="T1041" id="Seg_10025" s="T1040">рыба.[NOM.SG]</ta>
            <ta e="T1042" id="Seg_10026" s="T1041">прийти-IMP.2SG</ta>
            <ta e="T1043" id="Seg_10027" s="T1042">здесь</ta>
            <ta e="T1044" id="Seg_10028" s="T1043">рыба.[NOM.SG]</ta>
            <ta e="T1045" id="Seg_10029" s="T1044">прийти-PST.[3SG]</ta>
            <ta e="T1047" id="Seg_10030" s="T1046">что.[NOM.SG]</ta>
            <ta e="T1048" id="Seg_10031" s="T1047">ты.DAT</ta>
            <ta e="T1049" id="Seg_10032" s="T1048">нужно</ta>
            <ta e="T1050" id="Seg_10033" s="T1049">мужчина.[NOM.SG]</ta>
            <ta e="T1051" id="Seg_10034" s="T1050">и</ta>
            <ta e="T1052" id="Seg_10035" s="T1051">я.NOM</ta>
            <ta e="T1053" id="Seg_10036" s="T1052">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1055" id="Seg_10037" s="T1054">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1056" id="Seg_10038" s="T1055">чтобы</ta>
            <ta e="T1057" id="Seg_10039" s="T1056">я.NOM</ta>
            <ta e="T1059" id="Seg_10040" s="T1058">чтобы</ta>
            <ta e="T1060" id="Seg_10041" s="T1059">я.NOM</ta>
            <ta e="T1061" id="Seg_10042" s="T1060">быть-PST-1SG</ta>
            <ta e="T1062" id="Seg_10043" s="T1061">царица.INS-INS</ta>
            <ta e="T1064" id="Seg_10044" s="T1063">чтобы</ta>
            <ta e="T1065" id="Seg_10045" s="T1064">я.LAT</ta>
            <ta e="T1066" id="Seg_10046" s="T1065">люди.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_10047" s="T1066">рука-3SG-INS</ta>
            <ta e="T1068" id="Seg_10048" s="T1067">нести-RES-PST-3PL</ta>
            <ta e="T1070" id="Seg_10049" s="T1069">пойти-EP-IMP.2SG</ta>
            <ta e="T1071" id="Seg_10050" s="T1070">дом-LAT-2SG</ta>
            <ta e="T1072" id="Seg_10051" s="T1071">PTCL</ta>
            <ta e="T1073" id="Seg_10052" s="T1072">стать-FUT-3SG</ta>
            <ta e="T1074" id="Seg_10053" s="T1073">ты.DAT</ta>
            <ta e="T1078" id="Seg_10054" s="T1077">тогда</ta>
            <ta e="T1079" id="Seg_10055" s="T1078">мужчина.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_10056" s="T1079">пойти-PST.[3SG]</ta>
            <ta e="T1081" id="Seg_10057" s="T1080">вода-LAT</ta>
            <ta e="T1083" id="Seg_10058" s="T1082">рыба-PL</ta>
            <ta e="T1084" id="Seg_10059" s="T1083">рыба-PL</ta>
            <ta e="T1085" id="Seg_10060" s="T1084">прийти-IMP.2SG</ta>
            <ta e="T1086" id="Seg_10061" s="T1085">здесь</ta>
            <ta e="T1087" id="Seg_10062" s="T1086">рыба.[NOM.SG]</ta>
            <ta e="T1088" id="Seg_10063" s="T1087">прийти-PST.[3SG]</ta>
            <ta e="T1089" id="Seg_10064" s="T1088">что.[NOM.SG]</ta>
            <ta e="T1090" id="Seg_10065" s="T1089">ты.DAT</ta>
            <ta e="T1091" id="Seg_10066" s="T1090">нужно</ta>
            <ta e="T1093" id="Seg_10067" s="T1092">и</ta>
            <ta e="T1094" id="Seg_10068" s="T1093">я.NOM</ta>
            <ta e="T1095" id="Seg_10069" s="T1094">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1096" id="Seg_10070" s="T1095">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1097" id="Seg_10071" s="T1096">чтобы</ta>
            <ta e="T1098" id="Seg_10072" s="T1097">я.NOM</ta>
            <ta e="T1099" id="Seg_10073" s="T1098">быть-PST-1SG</ta>
            <ta e="T1103" id="Seg_10074" s="T1102">и</ta>
            <ta e="T1104" id="Seg_10075" s="T1103">рыба.[NOM.SG]</ta>
            <ta e="T1105" id="Seg_10076" s="T1104">чтобы</ta>
            <ta e="T1106" id="Seg_10077" s="T1105">я.LAT</ta>
            <ta e="T1107" id="Seg_10078" s="T1106">носить-PST.[3SG]</ta>
            <ta e="T1108" id="Seg_10079" s="T1107">PTCL</ta>
            <ta e="T1109" id="Seg_10080" s="T1108">что.[NOM.SG]</ta>
            <ta e="T1111" id="Seg_10081" s="T1110">тогда</ta>
            <ta e="T1112" id="Seg_10082" s="T1111">рыба.[NOM.SG]</ta>
            <ta e="T1113" id="Seg_10083" s="T1112">пойти-PST.[3SG]</ta>
            <ta e="T1114" id="Seg_10084" s="T1113">вода-LAT</ta>
            <ta e="T1115" id="Seg_10085" s="T1114">что.[NOM.SG]</ta>
            <ta e="T1117" id="Seg_10086" s="T1116">что.[NOM.SG]=INDEF</ta>
            <ta e="T1119" id="Seg_10087" s="T1118">мужчина-LAT</ta>
            <ta e="T1120" id="Seg_10088" s="T1119">NEG</ta>
            <ta e="T1121" id="Seg_10089" s="T1120">сказать-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_10090" s="T1122">этот.[NOM.SG]</ta>
            <ta e="T1124" id="Seg_10091" s="T1123">прийти-PST.[3SG]</ta>
            <ta e="T1125" id="Seg_10092" s="T1124">чум-LAT/LOC.3SG</ta>
            <ta e="T1127" id="Seg_10093" s="T1126">женщина.[NOM.SG]</ta>
            <ta e="T1128" id="Seg_10094" s="T1127">сидеть-DUR.[3SG]</ta>
            <ta e="T1129" id="Seg_10095" s="T1128">прясть-DUR.[3SG]</ta>
            <ta e="T1130" id="Seg_10096" s="T1129">и</ta>
            <ta e="T1131" id="Seg_10097" s="T1130">корыто.[NOM.SG]</ta>
            <ta e="T1132" id="Seg_10098" s="T1131">корыто-NOM/GEN.3SG</ta>
            <ta e="T1133" id="Seg_10099" s="T1132">стоять-DUR.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_10100" s="T1">num.[n:case]</ta>
            <ta e="T3" id="Seg_10101" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_10102" s="T3">conj</ta>
            <ta e="T5" id="Seg_10103" s="T4">n.[n:case]</ta>
            <ta e="T7" id="Seg_10104" s="T6">num.[n:case]</ta>
            <ta e="T8" id="Seg_10105" s="T7">n.[n:case]</ta>
            <ta e="T9" id="Seg_10106" s="T8">v-v:tense.[v:pn]</ta>
            <ta e="T10" id="Seg_10107" s="T9">num.[n:case]</ta>
            <ta e="T11" id="Seg_10108" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_10109" s="T11">v-v:tense.[v:pn]</ta>
            <ta e="T13" id="Seg_10110" s="T12">adv</ta>
            <ta e="T15" id="Seg_10111" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_10112" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_10113" s="T16">v-v:tense.[v:pn]</ta>
            <ta e="T18" id="Seg_10114" s="T17">adv</ta>
            <ta e="T19" id="Seg_10115" s="T18">n.[n:case]</ta>
            <ta e="T20" id="Seg_10116" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_10117" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_10118" s="T21">adv</ta>
            <ta e="T23" id="Seg_10119" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_10120" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_10121" s="T24">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_10122" s="T26">adv</ta>
            <ta e="T28" id="Seg_10123" s="T27">num.[n:case]</ta>
            <ta e="T29" id="Seg_10124" s="T28">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T30" id="Seg_10125" s="T29">conj</ta>
            <ta e="T31" id="Seg_10126" s="T30">num.[n:case]</ta>
            <ta e="T32" id="Seg_10127" s="T31">ptcl</ta>
            <ta e="T34" id="Seg_10128" s="T33">adv</ta>
            <ta e="T35" id="Seg_10129" s="T34">num.[n:case]</ta>
            <ta e="T36" id="Seg_10130" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_10131" s="T36">v-v:tense.[v:pn]</ta>
            <ta e="T38" id="Seg_10132" s="T37">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T39" id="Seg_10133" s="T38">adv</ta>
            <ta e="T40" id="Seg_10134" s="T39">propr-n:case</ta>
            <ta e="T41" id="Seg_10135" s="T40">conj</ta>
            <ta e="T42" id="Seg_10136" s="T41">num.[n:case]</ta>
            <ta e="T43" id="Seg_10137" s="T42">n</ta>
            <ta e="T44" id="Seg_10138" s="T43">v-v:tense.[v:pn]</ta>
            <ta e="T45" id="Seg_10139" s="T44">adv</ta>
            <ta e="T46" id="Seg_10140" s="T45">v-v:tense.[v:pn]</ta>
            <ta e="T47" id="Seg_10141" s="T46">conj</ta>
            <ta e="T48" id="Seg_10142" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_10143" s="T48">v-v:tense.[v:pn]</ta>
            <ta e="T52" id="Seg_10144" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_10145" s="T52">v-v:tense.[v:pn]</ta>
            <ta e="T54" id="Seg_10146" s="T53">conj</ta>
            <ta e="T55" id="Seg_10147" s="T54">dempro.[n:case]</ta>
            <ta e="T56" id="Seg_10148" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_10149" s="T56">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T59" id="Seg_10150" s="T58">adv</ta>
            <ta e="T60" id="Seg_10151" s="T59">dempro-n:case</ta>
            <ta e="T61" id="Seg_10152" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_10153" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_10154" s="T62">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T64" id="Seg_10155" s="T63">adv</ta>
            <ta e="T65" id="Seg_10156" s="T64">dempro.[n:case]</ta>
            <ta e="T66" id="Seg_10157" s="T65">n-n:num</ta>
            <ta e="T67" id="Seg_10158" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_10159" s="T67">n.[n:case]</ta>
            <ta e="T69" id="Seg_10160" s="T68">n-n:num</ta>
            <ta e="T70" id="Seg_10161" s="T69">conj</ta>
            <ta e="T71" id="Seg_10162" s="T70">n-n:num</ta>
            <ta e="T72" id="Seg_10163" s="T71">conj</ta>
            <ta e="T73" id="Seg_10164" s="T72">n-n:case.poss</ta>
            <ta e="T1134" id="Seg_10165" s="T74">v-v:n-fin</ta>
            <ta e="T75" id="Seg_10166" s="T1134">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_10167" s="T76">adv</ta>
            <ta e="T78" id="Seg_10168" s="T77">quant</ta>
            <ta e="T79" id="Seg_10169" s="T78">n.[n:case]</ta>
            <ta e="T81" id="Seg_10170" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_10171" s="T81">conj</ta>
            <ta e="T83" id="Seg_10172" s="T82">n.[n:case]</ta>
            <ta e="T84" id="Seg_10173" s="T83">conj</ta>
            <ta e="T85" id="Seg_10174" s="T84">n.[n:case]</ta>
            <ta e="T86" id="Seg_10175" s="T85">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_10176" s="T87">n.[n:case]</ta>
            <ta e="T89" id="Seg_10177" s="T88">conj</ta>
            <ta e="T90" id="Seg_10178" s="T89">dempro.[n:case]</ta>
            <ta e="T91" id="Seg_10179" s="T90">v-v:n.fin</ta>
            <ta e="T92" id="Seg_10180" s="T91">n.[n:case]</ta>
            <ta e="T93" id="Seg_10181" s="T92">n.[n:case]</ta>
            <ta e="T94" id="Seg_10182" s="T93">v-v&gt;v.[v:pn]</ta>
            <ta e="T95" id="Seg_10183" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_10184" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_10185" s="T96">n-n:num</ta>
            <ta e="T98" id="Seg_10186" s="T97">v-v:tense.[v:pn]</ta>
            <ta e="T99" id="Seg_10187" s="T98">adv</ta>
            <ta e="T100" id="Seg_10188" s="T99">n.[n:case]</ta>
            <ta e="T1135" id="Seg_10189" s="T100">v-v:n-fin</ta>
            <ta e="T101" id="Seg_10190" s="T1135">v-v:tense.[v:pn]</ta>
            <ta e="T102" id="Seg_10191" s="T101">n.[n:case]</ta>
            <ta e="T1136" id="Seg_10192" s="T102">v-v:n-fin</ta>
            <ta e="T103" id="Seg_10193" s="T1136">v-v:tense.[v:pn]</ta>
            <ta e="T105" id="Seg_10194" s="T104">conj</ta>
            <ta e="T106" id="Seg_10195" s="T105">n</ta>
            <ta e="T107" id="Seg_10196" s="T106">v</ta>
            <ta e="T108" id="Seg_10197" s="T107">n.[n:case]</ta>
            <ta e="T109" id="Seg_10198" s="T108">v-v:tense.[v:pn]</ta>
            <ta e="T110" id="Seg_10199" s="T109">conj</ta>
            <ta e="T111" id="Seg_10200" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_10201" s="T111">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T113" id="Seg_10202" s="T112">conj</ta>
            <ta e="T114" id="Seg_10203" s="T113">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T116" id="Seg_10204" s="T115">conj</ta>
            <ta e="T117" id="Seg_10205" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_10206" s="T117">v-v:tense.[v:pn]</ta>
            <ta e="T119" id="Seg_10207" s="T118">v-v:tense.[v:pn]</ta>
            <ta e="T120" id="Seg_10208" s="T119">conj</ta>
            <ta e="T121" id="Seg_10209" s="T120">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T123" id="Seg_10210" s="T122">n.[n:case]</ta>
            <ta e="T124" id="Seg_10211" s="T123">adv</ta>
            <ta e="T126" id="Seg_10212" s="T125">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T127" id="Seg_10213" s="T126">adv</ta>
            <ta e="T128" id="Seg_10214" s="T127">v-v:tense.[v:pn]</ta>
            <ta e="T129" id="Seg_10215" s="T128">v-v:tense.[v:pn]</ta>
            <ta e="T130" id="Seg_10216" s="T129">n-n:num</ta>
            <ta e="T131" id="Seg_10217" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_10218" s="T131">v-v&gt;v-v:pn</ta>
            <ta e="T134" id="Seg_10219" s="T133">n.[n:case]</ta>
            <ta e="T135" id="Seg_10220" s="T134">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T136" id="Seg_10221" s="T135">n.[n:case]</ta>
            <ta e="T137" id="Seg_10222" s="T136">n-n:case</ta>
            <ta e="T139" id="Seg_10223" s="T138">n-n:num</ta>
            <ta e="T140" id="Seg_10224" s="T139">n</ta>
            <ta e="T141" id="Seg_10225" s="T140">n-n:case.poss</ta>
            <ta e="T143" id="Seg_10226" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_10227" s="T143">v-v:mood.pn</ta>
            <ta e="T145" id="Seg_10228" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_10229" s="T145">conj</ta>
            <ta e="T147" id="Seg_10230" s="T146">v-v&gt;v-v:n.fin</ta>
            <ta e="T148" id="Seg_10231" s="T147">v-v:ins-v:mood.pn</ta>
            <ta e="T149" id="Seg_10232" s="T148">n-n:case</ta>
            <ta e="T151" id="Seg_10233" s="T150">dempro-n:num</ta>
            <ta e="T152" id="Seg_10234" s="T151">v-v:n.fin</ta>
            <ta e="T153" id="Seg_10235" s="T152">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_10236" s="T154">n-n:case.poss</ta>
            <ta e="T156" id="Seg_10237" s="T155">v-v:tense.[v:pn]</ta>
            <ta e="T158" id="Seg_10238" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_10239" s="T158">v-v:mood.pn</ta>
            <ta e="T160" id="Seg_10240" s="T159">v-v:mood.pn</ta>
            <ta e="T161" id="Seg_10241" s="T160">pers</ta>
            <ta e="T162" id="Seg_10242" s="T161">adv</ta>
            <ta e="T163" id="Seg_10243" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_10244" s="T163">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_10245" s="T165">dempro-n:num</ta>
            <ta e="T167" id="Seg_10246" s="T166">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_10247" s="T167">v-v:mood.pn</ta>
            <ta e="T169" id="Seg_10248" s="T168">n-n:case.poss</ta>
            <ta e="T170" id="Seg_10249" s="T169">n-n:case</ta>
            <ta e="T172" id="Seg_10250" s="T171">v-v&gt;v-v:pn</ta>
            <ta e="T174" id="Seg_10251" s="T173">n-n:case.poss</ta>
            <ta e="T176" id="Seg_10252" s="T175">pers</ta>
            <ta e="T177" id="Seg_10253" s="T176">dempro-n:case</ta>
            <ta e="T178" id="Seg_10254" s="T177">aux-v:pn</ta>
            <ta e="T180" id="Seg_10255" s="T179">v.[v:pn]</ta>
            <ta e="T181" id="Seg_10256" s="T180">v-v:n.fin</ta>
            <ta e="T182" id="Seg_10257" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_10258" s="T182">n-n:case.poss</ta>
            <ta e="T184" id="Seg_10259" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_10260" s="T184">conj</ta>
            <ta e="T186" id="Seg_10261" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_10262" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_10263" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_10264" s="T188">dempro.[n:case]</ta>
            <ta e="T190" id="Seg_10265" s="T189">n.[n:case]</ta>
            <ta e="T191" id="Seg_10266" s="T190">v-v:tense.[v:pn]</ta>
            <ta e="T193" id="Seg_10267" s="T192">n.[n:case]</ta>
            <ta e="T194" id="Seg_10268" s="T193">v-v:tense.[v:pn]</ta>
            <ta e="T195" id="Seg_10269" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_10270" s="T195">v-v:tense.[v:pn]</ta>
            <ta e="T198" id="Seg_10271" s="T197">adv</ta>
            <ta e="T199" id="Seg_10272" s="T198">n-n:num</ta>
            <ta e="T200" id="Seg_10273" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_10274" s="T200">v-v:mood.pn</ta>
            <ta e="T202" id="Seg_10275" s="T201">n.[n:case]</ta>
            <ta e="T203" id="Seg_10276" s="T202">v-v&gt;v-v&gt;v-v:n.fin.[n:case]</ta>
            <ta e="T205" id="Seg_10277" s="T204">v-v:tense-v:pn</ta>
            <ta e="T207" id="Seg_10278" s="T206">que</ta>
            <ta e="T208" id="Seg_10279" s="T207">n-n:case.poss</ta>
            <ta e="T210" id="Seg_10280" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_10281" s="T210">dempro-n:case</ta>
            <ta e="T212" id="Seg_10282" s="T211">v-v:n.fin</ta>
            <ta e="T213" id="Seg_10283" s="T212">n-n:num-n:case.poss</ta>
            <ta e="T215" id="Seg_10284" s="T214">conj</ta>
            <ta e="T216" id="Seg_10285" s="T215">n.[n:case]</ta>
            <ta e="T217" id="Seg_10286" s="T216">v-v&gt;v.[v:pn]</ta>
            <ta e="T218" id="Seg_10287" s="T217">aux-v:mood.pn</ta>
            <ta e="T219" id="Seg_10288" s="T218">v-v:n.fin</ta>
            <ta e="T220" id="Seg_10289" s="T219">pers</ta>
            <ta e="T221" id="Seg_10290" s="T220">adv</ta>
            <ta e="T222" id="Seg_10291" s="T221">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_10292" s="T223">adv</ta>
            <ta e="T225" id="Seg_10293" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_10294" s="T225">n.[n:case]</ta>
            <ta e="T227" id="Seg_10295" s="T226">v-v&gt;v.[v:pn]</ta>
            <ta e="T229" id="Seg_10296" s="T228">conj</ta>
            <ta e="T230" id="Seg_10297" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_10298" s="T230">v-v:tense.[v:pn]</ta>
            <ta e="T233" id="Seg_10299" s="T232">adv</ta>
            <ta e="T234" id="Seg_10300" s="T233">adj.[n:case]</ta>
            <ta e="T235" id="Seg_10301" s="T234">adv</ta>
            <ta e="T237" id="Seg_10302" s="T236">adv</ta>
            <ta e="T239" id="Seg_10303" s="T238">adv</ta>
            <ta e="T240" id="Seg_10304" s="T239">n.[n:case]</ta>
            <ta e="T241" id="Seg_10305" s="T240">n-n:case</ta>
            <ta e="T243" id="Seg_10306" s="T242">num.[n:case]</ta>
            <ta e="T245" id="Seg_10307" s="T244">dempro.[n:case]</ta>
            <ta e="T246" id="Seg_10308" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_10309" s="T246">adv</ta>
            <ta e="T248" id="Seg_10310" s="T247">adj.[n:case]</ta>
            <ta e="T249" id="Seg_10311" s="T248">v-v:tense.[v:pn]</ta>
            <ta e="T251" id="Seg_10312" s="T250">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_10313" s="T251">n.[n:case]</ta>
            <ta e="T253" id="Seg_10314" s="T252">ptcl</ta>
            <ta e="T257" id="Seg_10315" s="T256">n.[n:case]</ta>
            <ta e="T258" id="Seg_10316" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_10317" s="T258">n-n:case</ta>
            <ta e="T261" id="Seg_10318" s="T260">adv</ta>
            <ta e="T262" id="Seg_10319" s="T261">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T263" id="Seg_10320" s="T262">n.[n:case]</ta>
            <ta e="T264" id="Seg_10321" s="T263">pers</ta>
            <ta e="T265" id="Seg_10322" s="T264">dempro-n:case</ta>
            <ta e="T266" id="Seg_10323" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_10324" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_10325" s="T267">adv</ta>
            <ta e="T269" id="Seg_10326" s="T268">n.[n:case]</ta>
            <ta e="T270" id="Seg_10327" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_10328" s="T270">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_10329" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_10330" s="T273">n-n:case</ta>
            <ta e="T278" id="Seg_10331" s="T277">dempro.[n:case]</ta>
            <ta e="T279" id="Seg_10332" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_10333" s="T279">adj.[n:case]</ta>
            <ta e="T281" id="Seg_10334" s="T280">n.[n:case]</ta>
            <ta e="T282" id="Seg_10335" s="T281">v.[v:pn]</ta>
            <ta e="T283" id="Seg_10336" s="T282">conj</ta>
            <ta e="T284" id="Seg_10337" s="T283">adj.[n:case]</ta>
            <ta e="T285" id="Seg_10338" s="T284">n.[n:case]</ta>
            <ta e="T286" id="Seg_10339" s="T285">v-v:tense.[v:pn]</ta>
            <ta e="T288" id="Seg_10340" s="T287">n.[n:case]</ta>
            <ta e="T289" id="Seg_10341" s="T288">quant</ta>
            <ta e="T290" id="Seg_10342" s="T289">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_10343" s="T290">n-n:case.poss</ta>
            <ta e="T292" id="Seg_10344" s="T291">n-n:num</ta>
            <ta e="T294" id="Seg_10345" s="T293">n.[n:case]</ta>
            <ta e="T295" id="Seg_10346" s="T294">quant</ta>
            <ta e="T296" id="Seg_10347" s="T295">v-v:tense.[v:pn]</ta>
            <ta e="T297" id="Seg_10348" s="T296">ptcl</ta>
            <ta e="T299" id="Seg_10349" s="T298">n.[n:case]</ta>
            <ta e="T300" id="Seg_10350" s="T299">adv</ta>
            <ta e="T301" id="Seg_10351" s="T300">quant</ta>
            <ta e="T302" id="Seg_10352" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_10353" s="T302">n.[n:case]</ta>
            <ta e="T307" id="Seg_10354" s="T306">num.[n:case]</ta>
            <ta e="T308" id="Seg_10355" s="T307">n.[n:case]</ta>
            <ta e="T309" id="Seg_10356" s="T308">v-v:tense.[v:pn]</ta>
            <ta e="T310" id="Seg_10357" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_10358" s="T310">n.[n:case]</ta>
            <ta e="T315" id="Seg_10359" s="T314">n.[n:case]</ta>
            <ta e="T316" id="Seg_10360" s="T315">v-v&gt;v-v:n.fin</ta>
            <ta e="T319" id="Seg_10361" s="T318">dempro-n:num</ta>
            <ta e="T320" id="Seg_10362" s="T319">refl-n:case.poss</ta>
            <ta e="T321" id="Seg_10363" s="T320">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_10364" s="T321">conj</ta>
            <ta e="T323" id="Seg_10365" s="T322">n.[n:case]</ta>
            <ta e="T324" id="Seg_10366" s="T323">v-v&gt;v-v:pn</ta>
            <ta e="T328" id="Seg_10367" s="T327">dempro.[n:case]</ta>
            <ta e="T329" id="Seg_10368" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_10369" s="T329">n.[n:case]</ta>
            <ta e="T331" id="Seg_10370" s="T330">n.[n:case]</ta>
            <ta e="T332" id="Seg_10371" s="T331">v-v:tense.[v:pn]</ta>
            <ta e="T333" id="Seg_10372" s="T332">ptcl</ta>
            <ta e="T337" id="Seg_10373" s="T336">dempro.[n:case]</ta>
            <ta e="T338" id="Seg_10374" s="T337">n.[n:case]</ta>
            <ta e="T339" id="Seg_10375" s="T338">adv</ta>
            <ta e="T340" id="Seg_10376" s="T339">n.[n:case]</ta>
            <ta e="T341" id="Seg_10377" s="T340">adj.[n:case]</ta>
            <ta e="T342" id="Seg_10378" s="T341">v-v:tense.[v:pn]</ta>
            <ta e="T344" id="Seg_10379" s="T343">conj</ta>
            <ta e="T345" id="Seg_10380" s="T344">n.[n:case]</ta>
            <ta e="T346" id="Seg_10381" s="T345">adj.[n:case]</ta>
            <ta e="T347" id="Seg_10382" s="T346">v-v:tense.[v:pn]</ta>
            <ta e="T348" id="Seg_10383" s="T347">quant</ta>
            <ta e="T349" id="Seg_10384" s="T348">v-v:tense.[v:pn]</ta>
            <ta e="T350" id="Seg_10385" s="T349">adj.[n:case]</ta>
            <ta e="T351" id="Seg_10386" s="T350">v-v:tense.[v:pn]</ta>
            <ta e="T355" id="Seg_10387" s="T354">n.[n:case]</ta>
            <ta e="T356" id="Seg_10388" s="T355">n.[n:case]</ta>
            <ta e="T358" id="Seg_10389" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_10390" s="T358">n.[n:case]</ta>
            <ta e="T360" id="Seg_10391" s="T359">adv</ta>
            <ta e="T362" id="Seg_10392" s="T361">n.[n:case]</ta>
            <ta e="T363" id="Seg_10393" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_10394" s="T363">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T365" id="Seg_10395" s="T364">n-n:case</ta>
            <ta e="T367" id="Seg_10396" s="T366">conj</ta>
            <ta e="T371" id="Seg_10397" s="T370">conj</ta>
            <ta e="T372" id="Seg_10398" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_10399" s="T372">n.[n:case]</ta>
            <ta e="T374" id="Seg_10400" s="T373">adj.[n:case]</ta>
            <ta e="T375" id="Seg_10401" s="T374">v-v:tense.[v:pn]</ta>
            <ta e="T379" id="Seg_10402" s="T378">pers</ta>
            <ta e="T382" id="Seg_10403" s="T381">v-v:tense-v:pn</ta>
            <ta e="T383" id="Seg_10404" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_10405" s="T383">conj</ta>
            <ta e="T385" id="Seg_10406" s="T384">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T387" id="Seg_10407" s="T386">conj</ta>
            <ta e="T388" id="Seg_10408" s="T387">adv</ta>
            <ta e="T389" id="Seg_10409" s="T388">v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_10410" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_10411" s="T390">n-n:case</ta>
            <ta e="T398" id="Seg_10412" s="T397">n-n:num-n:case</ta>
            <ta e="T399" id="Seg_10413" s="T398">num.[n:case]</ta>
            <ta e="T400" id="Seg_10414" s="T399">n.[n:case]</ta>
            <ta e="T401" id="Seg_10415" s="T400">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T402" id="Seg_10416" s="T401">dempro-n:num</ta>
            <ta e="T403" id="Seg_10417" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_10418" s="T403">v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_10419" s="T404">n.[n:case]</ta>
            <ta e="T406" id="Seg_10420" s="T405">adv</ta>
            <ta e="T407" id="Seg_10421" s="T406">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_10422" s="T410">adv</ta>
            <ta e="T412" id="Seg_10423" s="T411">n.[n:case]</ta>
            <ta e="T413" id="Seg_10424" s="T412">v-v:tense-v:pn</ta>
            <ta e="T414" id="Seg_10425" s="T413">n.[n:case]</ta>
            <ta e="T415" id="Seg_10426" s="T414">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_10427" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_10428" s="T416">dempro-n:case</ta>
            <ta e="T418" id="Seg_10429" s="T417">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_10430" s="T421">dempro.[n:case]</ta>
            <ta e="T423" id="Seg_10431" s="T422">dempro-n:case</ta>
            <ta e="T424" id="Seg_10432" s="T423">n.[n:case]</ta>
            <ta e="T425" id="Seg_10433" s="T424">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_10434" s="T425">n.[n:case]</ta>
            <ta e="T427" id="Seg_10435" s="T426">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_10436" s="T428">n.[n:case]</ta>
            <ta e="T430" id="Seg_10437" s="T429">v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_10438" s="T430">ptcl</ta>
            <ta e="T433" id="Seg_10439" s="T432">n.[n:case]</ta>
            <ta e="T434" id="Seg_10440" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_10441" s="T434">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_10442" s="T436">dempro.[n:case]</ta>
            <ta e="T438" id="Seg_10443" s="T437">adv</ta>
            <ta e="T439" id="Seg_10444" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_10445" s="T439">conj</ta>
            <ta e="T441" id="Seg_10446" s="T440">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_10447" s="T444">adv</ta>
            <ta e="T446" id="Seg_10448" s="T445">dempro.[n:case]</ta>
            <ta e="T447" id="Seg_10449" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_10450" s="T447">v-v:tense-v:pn</ta>
            <ta e="T450" id="Seg_10451" s="T449">n-n:case</ta>
            <ta e="T451" id="Seg_10452" s="T450">v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_10453" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_10454" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_10455" s="T453">v-v:tense-v:pn</ta>
            <ta e="T456" id="Seg_10456" s="T455">adv</ta>
            <ta e="T457" id="Seg_10457" s="T456">n-n:case.poss</ta>
            <ta e="T458" id="Seg_10458" s="T457">v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_10459" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_10460" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_10461" s="T461">n.[n:case]</ta>
            <ta e="T463" id="Seg_10462" s="T462">v-v:tense-v:pn</ta>
            <ta e="T464" id="Seg_10463" s="T463">conj</ta>
            <ta e="T465" id="Seg_10464" s="T464">n.[n:case]</ta>
            <ta e="T467" id="Seg_10465" s="T466">adv</ta>
            <ta e="T468" id="Seg_10466" s="T467">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_10467" s="T469">interj</ta>
            <ta e="T471" id="Seg_10468" s="T470">v-v:tense.[v:pn]</ta>
            <ta e="T472" id="Seg_10469" s="T471">v-v&gt;v-v:pn</ta>
            <ta e="T473" id="Seg_10470" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_10471" s="T473">ptcl</ta>
            <ta e="T476" id="Seg_10472" s="T475">n.[n:case]</ta>
            <ta e="T477" id="Seg_10473" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_10474" s="T477">v-v:tense-v:pn</ta>
            <ta e="T482" id="Seg_10475" s="T481">n.[n:case]</ta>
            <ta e="T483" id="Seg_10476" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_10477" s="T483">adj.[n:case]</ta>
            <ta e="T485" id="Seg_10478" s="T484">conj</ta>
            <ta e="T486" id="Seg_10479" s="T485">n-n:case.poss</ta>
            <ta e="T487" id="Seg_10480" s="T486">num.[n:case]</ta>
            <ta e="T488" id="Seg_10481" s="T487">num.[n:case]</ta>
            <ta e="T489" id="Seg_10482" s="T488">num.[n:case]</ta>
            <ta e="T490" id="Seg_10483" s="T489">num.[n:case]</ta>
            <ta e="T491" id="Seg_10484" s="T490">num.[n:case]</ta>
            <ta e="T492" id="Seg_10485" s="T491">n-n:case.poss</ta>
            <ta e="T496" id="Seg_10486" s="T495">n.[n:case]</ta>
            <ta e="T497" id="Seg_10487" s="T496">v-v:tense-v:pn</ta>
            <ta e="T498" id="Seg_10488" s="T497">conj</ta>
            <ta e="T499" id="Seg_10489" s="T498">n.[n:case]</ta>
            <ta e="T501" id="Seg_10490" s="T500">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T505" id="Seg_10491" s="T504">adj</ta>
            <ta e="T506" id="Seg_10492" s="T505">num.[n:case]</ta>
            <ta e="T507" id="Seg_10493" s="T506">n.[n:case]</ta>
            <ta e="T509" id="Seg_10494" s="T508">pers</ta>
            <ta e="T510" id="Seg_10495" s="T509">v-v:tense.[v:pn]</ta>
            <ta e="T511" id="Seg_10496" s="T510">v-v:tense.[v:pn]</ta>
            <ta e="T512" id="Seg_10497" s="T511">adv</ta>
            <ta e="T514" id="Seg_10498" s="T513">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T516" id="Seg_10499" s="T515">conj</ta>
            <ta e="T517" id="Seg_10500" s="T516">pers</ta>
            <ta e="T519" id="Seg_10501" s="T518">v-v:tense-v:pn</ta>
            <ta e="T1139" id="Seg_10502" s="T519">propr</ta>
            <ta e="T520" id="Seg_10503" s="T1139">n-n:case</ta>
            <ta e="T522" id="Seg_10504" s="T521">n.[n:case]</ta>
            <ta e="T523" id="Seg_10505" s="T522">n-n:case.poss</ta>
            <ta e="T525" id="Seg_10506" s="T524">n-n:case.poss</ta>
            <ta e="T526" id="Seg_10507" s="T525">v-v:n.fin</ta>
            <ta e="T527" id="Seg_10508" s="T526">conj</ta>
            <ta e="T528" id="Seg_10509" s="T527">n-n:case.poss</ta>
            <ta e="T530" id="Seg_10510" s="T529">adv</ta>
            <ta e="T531" id="Seg_10511" s="T530">n.[n:case]</ta>
            <ta e="T532" id="Seg_10512" s="T531">pers</ta>
            <ta e="T533" id="Seg_10513" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_10514" s="T533">v-v:tense.[v:pn]</ta>
            <ta e="T536" id="Seg_10515" s="T535">pers</ta>
            <ta e="T537" id="Seg_10516" s="T536">n.[n:case]</ta>
            <ta e="T538" id="Seg_10517" s="T537">v-v:tense-v:pn</ta>
            <ta e="T540" id="Seg_10518" s="T539">conj</ta>
            <ta e="T541" id="Seg_10519" s="T540">n-n:case.poss</ta>
            <ta e="T542" id="Seg_10520" s="T541">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_10521" s="T543">adv</ta>
            <ta e="T545" id="Seg_10522" s="T544">n-n:case.poss</ta>
            <ta e="T546" id="Seg_10523" s="T545">n-n:case.poss</ta>
            <ta e="T548" id="Seg_10524" s="T547">v-v:tense-v:pn</ta>
            <ta e="T549" id="Seg_10525" s="T548">dempro.[n:case]</ta>
            <ta e="T550" id="Seg_10526" s="T549">n.[n:case]</ta>
            <ta e="T1137" id="Seg_10527" s="T550">v-v:n-fin</ta>
            <ta e="T551" id="Seg_10528" s="T1137">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_10529" s="T551">v-v:n.fin</ta>
            <ta e="T554" id="Seg_10530" s="T553">conj</ta>
            <ta e="T555" id="Seg_10531" s="T554">pers</ta>
            <ta e="T556" id="Seg_10532" s="T555">n-n:num</ta>
            <ta e="T557" id="Seg_10533" s="T556">pers</ta>
            <ta e="T558" id="Seg_10534" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_10535" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_10536" s="T559">v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_10537" s="T560">n-n:case</ta>
            <ta e="T564" id="Seg_10538" s="T563">v-v:mood-v:pn</ta>
            <ta e="T566" id="Seg_10539" s="T565">conj</ta>
            <ta e="T567" id="Seg_10540" s="T566">pers</ta>
            <ta e="T568" id="Seg_10541" s="T567">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_10542" s="T568">v-v:mood.pn</ta>
            <ta e="T570" id="Seg_10543" s="T569">v-v:ins-v:mood.pn</ta>
            <ta e="T572" id="Seg_10544" s="T571">conj</ta>
            <ta e="T573" id="Seg_10545" s="T572">pers</ta>
            <ta e="T574" id="Seg_10546" s="T573">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_10547" s="T574">pers</ta>
            <ta e="T577" id="Seg_10548" s="T576">que</ta>
            <ta e="T578" id="Seg_10549" s="T577">pers</ta>
            <ta e="T579" id="Seg_10550" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_10551" s="T579">conj</ta>
            <ta e="T581" id="Seg_10552" s="T580">pers</ta>
            <ta e="T582" id="Seg_10553" s="T581">v-v&gt;v-v:n.fin</ta>
            <ta e="T584" id="Seg_10554" s="T583">adv</ta>
            <ta e="T585" id="Seg_10555" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_10556" s="T585">v-v:tense-v:pn</ta>
            <ta e="T588" id="Seg_10557" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_10558" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_10559" s="T589">dempro.[n:case]</ta>
            <ta e="T591" id="Seg_10560" s="T590">v-v&gt;v.[v:pn]</ta>
            <ta e="T592" id="Seg_10561" s="T591">conj</ta>
            <ta e="T594" id="Seg_10562" s="T593">conj</ta>
            <ta e="T595" id="Seg_10563" s="T594">pers</ta>
            <ta e="T596" id="Seg_10564" s="T595">v-v:tense-v:pn</ta>
            <ta e="T600" id="Seg_10565" s="T599">adv</ta>
            <ta e="T601" id="Seg_10566" s="T600">dempro.[n:case]</ta>
            <ta e="T602" id="Seg_10567" s="T601">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T603" id="Seg_10568" s="T602">dempro-n:case</ta>
            <ta e="T604" id="Seg_10569" s="T603">v-v:tense-v:pn</ta>
            <ta e="T606" id="Seg_10570" s="T605">n.[n:case]</ta>
            <ta e="T607" id="Seg_10571" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_10572" s="T607">n.[n:case]</ta>
            <ta e="T609" id="Seg_10573" s="T608">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_10574" s="T610">n-n:num</ta>
            <ta e="T612" id="Seg_10575" s="T611">v-v:tense-v:pn</ta>
            <ta e="T613" id="Seg_10576" s="T612">v-v:tense-v:pn</ta>
            <ta e="T615" id="Seg_10577" s="T614">adv</ta>
            <ta e="T616" id="Seg_10578" s="T615">v-v:tense-v:pn</ta>
            <ta e="T619" id="Seg_10579" s="T618">n-n:case</ta>
            <ta e="T620" id="Seg_10580" s="T619">v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_10581" s="T620">dempro-n:case</ta>
            <ta e="T623" id="Seg_10582" s="T622">v-v:tense-v:pn</ta>
            <ta e="T624" id="Seg_10583" s="T623">n-n:case.poss</ta>
            <ta e="T626" id="Seg_10584" s="T625">n.[n:case]</ta>
            <ta e="T627" id="Seg_10585" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_10586" s="T627">n.[n:case]</ta>
            <ta e="T629" id="Seg_10587" s="T628">v-v:tense-v:pn</ta>
            <ta e="T630" id="Seg_10588" s="T629">n.[n:case]</ta>
            <ta e="T631" id="Seg_10589" s="T630">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_10590" s="T632">conj</ta>
            <ta e="T634" id="Seg_10591" s="T633">n-n:case.poss</ta>
            <ta e="T1138" id="Seg_10592" s="T634">v-v:n-fin</ta>
            <ta e="T635" id="Seg_10593" s="T1138">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_10594" s="T638">pers</ta>
            <ta e="T640" id="Seg_10595" s="T639">num.[n:case]</ta>
            <ta e="T641" id="Seg_10596" s="T640">n.[n:case]</ta>
            <ta e="T642" id="Seg_10597" s="T641">v</ta>
            <ta e="T643" id="Seg_10598" s="T642">v-v:tense-v:pn</ta>
            <ta e="T1140" id="Seg_10599" s="T643">propr</ta>
            <ta e="T644" id="Seg_10600" s="T1140">n-n:case</ta>
            <ta e="T646" id="Seg_10601" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_10602" s="T646">propr-n:case</ta>
            <ta e="T648" id="Seg_10603" s="T647">adv</ta>
            <ta e="T649" id="Seg_10604" s="T648">n-n:case.poss</ta>
            <ta e="T650" id="Seg_10605" s="T649">v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_10606" s="T651">n-n:case</ta>
            <ta e="T653" id="Seg_10607" s="T652">adv</ta>
            <ta e="T654" id="Seg_10608" s="T653">v-v:pn</ta>
            <ta e="T655" id="Seg_10609" s="T654">que.[n:case]=ptcl</ta>
            <ta e="T657" id="Seg_10610" s="T656">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T659" id="Seg_10611" s="T658">conj</ta>
            <ta e="T660" id="Seg_10612" s="T659">pers</ta>
            <ta e="T661" id="Seg_10613" s="T660">v-v:tense-v:pn</ta>
            <ta e="T662" id="Seg_10614" s="T661">conj</ta>
            <ta e="T664" id="Seg_10615" s="T663">adv</ta>
            <ta e="T665" id="Seg_10616" s="T664">v-v:tense-v:pn</ta>
            <ta e="T666" id="Seg_10617" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_10618" s="T666">n.[n:case]</ta>
            <ta e="T668" id="Seg_10619" s="T667">quant</ta>
            <ta e="T669" id="Seg_10620" s="T668">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10621" s="T672">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_10622" s="T674">n-n:case.poss</ta>
            <ta e="T676" id="Seg_10623" s="T675">que-n:case.poss</ta>
            <ta e="T677" id="Seg_10624" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_10625" s="T677">v-v:tense-v:pn</ta>
            <ta e="T682" id="Seg_10626" s="T681">pers</ta>
            <ta e="T683" id="Seg_10627" s="T682">n-n:case.poss</ta>
            <ta e="T684" id="Seg_10628" s="T683">v-v:tense.[v:pn]</ta>
            <ta e="T685" id="Seg_10629" s="T684">n-n:case.poss</ta>
            <ta e="T686" id="Seg_10630" s="T685">v-v:tense.[v:pn]</ta>
            <ta e="T687" id="Seg_10631" s="T686">conj</ta>
            <ta e="T688" id="Seg_10632" s="T687">pers</ta>
            <ta e="T689" id="Seg_10633" s="T688">que.[n:case]</ta>
            <ta e="T690" id="Seg_10634" s="T689">v-v:tense.[v:pn]</ta>
            <ta e="T691" id="Seg_10635" s="T690">conj</ta>
            <ta e="T692" id="Seg_10636" s="T691">pers</ta>
            <ta e="T693" id="Seg_10637" s="T692">n-n:case.poss</ta>
            <ta e="T694" id="Seg_10638" s="T693">v-v:tense.[v:pn]</ta>
            <ta e="T695" id="Seg_10639" s="T694">v-v&gt;v.[v:pn]</ta>
            <ta e="T696" id="Seg_10640" s="T695">conj</ta>
            <ta e="T697" id="Seg_10641" s="T696">n-n:case.poss</ta>
            <ta e="T698" id="Seg_10642" s="T697">v-v&gt;v.[v:pn]</ta>
            <ta e="T702" id="Seg_10643" s="T701">propr-n:ins-n:case</ta>
            <ta e="T703" id="Seg_10644" s="T702">ptcl</ta>
            <ta e="T704" id="Seg_10645" s="T703">n-n:case.poss</ta>
            <ta e="T705" id="Seg_10646" s="T704">v-v:tense.[v:pn]</ta>
            <ta e="T706" id="Seg_10647" s="T705">conj</ta>
            <ta e="T707" id="Seg_10648" s="T706">n-n:case.poss</ta>
            <ta e="T708" id="Seg_10649" s="T707">v-v:tense.[v:pn]</ta>
            <ta e="T709" id="Seg_10650" s="T708">conj</ta>
            <ta e="T711" id="Seg_10651" s="T710">n-n:num-n:case.poss</ta>
            <ta e="T712" id="Seg_10652" s="T711">v-v&gt;v-v:pn</ta>
            <ta e="T713" id="Seg_10653" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_10654" s="T713">adv</ta>
            <ta e="T715" id="Seg_10655" s="T714">v-v:tense.[v:pn]</ta>
            <ta e="T719" id="Seg_10656" s="T718">ptcl</ta>
            <ta e="T722" id="Seg_10657" s="T721">v-v:n.fin</ta>
            <ta e="T723" id="Seg_10658" s="T722">dempro.[n:case]</ta>
            <ta e="T725" id="Seg_10659" s="T724">num.[n:case]</ta>
            <ta e="T726" id="Seg_10660" s="T725">n.[n:case]</ta>
            <ta e="T728" id="Seg_10661" s="T727">n.[n:case]</ta>
            <ta e="T729" id="Seg_10662" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_10663" s="T729">v-v:n.fin</ta>
            <ta e="T731" id="Seg_10664" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_10665" s="T731">v-v:tense-v:pn</ta>
            <ta e="T733" id="Seg_10666" s="T732">dempro.[n:case]</ta>
            <ta e="T734" id="Seg_10667" s="T733">dempro-n:case</ta>
            <ta e="T738" id="Seg_10668" s="T737">que=ptcl</ta>
            <ta e="T739" id="Seg_10669" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_10670" s="T739">v-v:n.fin</ta>
            <ta e="T741" id="Seg_10671" s="T740">conj</ta>
            <ta e="T742" id="Seg_10672" s="T741">que=ptcl</ta>
            <ta e="T743" id="Seg_10673" s="T742">v-v:tense-v:pn</ta>
            <ta e="T744" id="Seg_10674" s="T743">v-v:n.fin</ta>
            <ta e="T748" id="Seg_10675" s="T747">dempro-n:num</ta>
            <ta e="T749" id="Seg_10676" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_10677" s="T749">adv</ta>
            <ta e="T751" id="Seg_10678" s="T750">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_10679" s="T751">conj</ta>
            <ta e="T753" id="Seg_10680" s="T752">adv</ta>
            <ta e="T754" id="Seg_10681" s="T753">adv</ta>
            <ta e="T755" id="Seg_10682" s="T754">adv</ta>
            <ta e="T756" id="Seg_10683" s="T755">v-v&gt;v-v:pn</ta>
            <ta e="T760" id="Seg_10684" s="T759">propr.[n:case]</ta>
            <ta e="T761" id="Seg_10685" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_10686" s="T761">n-n:case.poss</ta>
            <ta e="T764" id="Seg_10687" s="T763">n-n:case</ta>
            <ta e="T765" id="Seg_10688" s="T764">adj.[n:case]</ta>
            <ta e="T766" id="Seg_10689" s="T765">n-n:case.poss</ta>
            <ta e="T767" id="Seg_10690" s="T766">conj</ta>
            <ta e="T768" id="Seg_10691" s="T767">n-n:case.poss</ta>
            <ta e="T769" id="Seg_10692" s="T768">adj.[n:case]</ta>
            <ta e="T770" id="Seg_10693" s="T769">n-n:case.poss</ta>
            <ta e="T774" id="Seg_10694" s="T773">v-v:tense-v:pn</ta>
            <ta e="T775" id="Seg_10695" s="T774">n.[n:case]</ta>
            <ta e="T776" id="Seg_10696" s="T775">conj</ta>
            <ta e="T777" id="Seg_10697" s="T776">n.[n:case]</ta>
            <ta e="T779" id="Seg_10698" s="T778">%%</ta>
            <ta e="T780" id="Seg_10699" s="T779">n-n:case</ta>
            <ta e="T781" id="Seg_10700" s="T780">n-n:case.poss</ta>
            <ta e="T783" id="Seg_10701" s="T782">n.[n:case]</ta>
            <ta e="T784" id="Seg_10702" s="T783">v-v:n.fin</ta>
            <ta e="T785" id="Seg_10703" s="T784">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T787" id="Seg_10704" s="T786">conj</ta>
            <ta e="T788" id="Seg_10705" s="T787">n.[n:case]</ta>
            <ta e="T789" id="Seg_10706" s="T788">v-v:tense.[v:pn]</ta>
            <ta e="T790" id="Seg_10707" s="T789">n-n:case</ta>
            <ta e="T791" id="Seg_10708" s="T790">n.[n:case]</ta>
            <ta e="T793" id="Seg_10709" s="T792">v-v:n.fin</ta>
            <ta e="T795" id="Seg_10710" s="T794">adj</ta>
            <ta e="T796" id="Seg_10711" s="T795">n.[n:case]</ta>
            <ta e="T797" id="Seg_10712" s="T796">num.[n:case]</ta>
            <ta e="T798" id="Seg_10713" s="T797">n.[n:case]</ta>
            <ta e="T799" id="Seg_10714" s="T798">v-v:tense.[v:pn]</ta>
            <ta e="T800" id="Seg_10715" s="T799">adv</ta>
            <ta e="T801" id="Seg_10716" s="T800">que.[n:case]=ptcl</ta>
            <ta e="T802" id="Seg_10717" s="T801">ptcl</ta>
            <ta e="T803" id="Seg_10718" s="T802">v-v:tense.[v:pn]</ta>
            <ta e="T804" id="Seg_10719" s="T803">num.[n:case]</ta>
            <ta e="T805" id="Seg_10720" s="T804">n.[n:case]</ta>
            <ta e="T806" id="Seg_10721" s="T805">v-v:tense.[v:pn]</ta>
            <ta e="T807" id="Seg_10722" s="T806">adj.[n:case]</ta>
            <ta e="T809" id="Seg_10723" s="T808">pers</ta>
            <ta e="T810" id="Seg_10724" s="T809">v-v&gt;v.[v:pn]</ta>
            <ta e="T814" id="Seg_10725" s="T813">n.[n:case]</ta>
            <ta e="T815" id="Seg_10726" s="T814">adj.[n:case]</ta>
            <ta e="T816" id="Seg_10727" s="T815">v-v:tense.[v:pn]</ta>
            <ta e="T817" id="Seg_10728" s="T816">n-n:case</ta>
            <ta e="T819" id="Seg_10729" s="T818">n-n:case.poss-n:case</ta>
            <ta e="T821" id="Seg_10730" s="T820">v-v:n.fin</ta>
            <ta e="T822" id="Seg_10731" s="T821">pers</ta>
            <ta e="T823" id="Seg_10732" s="T822">n-n:case</ta>
            <ta e="T825" id="Seg_10733" s="T824">pers</ta>
            <ta e="T826" id="Seg_10734" s="T825">v-v:tense-v:pn</ta>
            <ta e="T828" id="Seg_10735" s="T827">v-v:tense.[v:pn]</ta>
            <ta e="T829" id="Seg_10736" s="T828">n-n:case.poss</ta>
            <ta e="T830" id="Seg_10737" s="T829">conj</ta>
            <ta e="T831" id="Seg_10738" s="T830">n-n:case</ta>
            <ta e="T832" id="Seg_10739" s="T831">v-v&gt;v.[v:pn]</ta>
            <ta e="T834" id="Seg_10740" s="T833">n.[n:case]</ta>
            <ta e="T835" id="Seg_10741" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_10742" s="T835">adj.[n:case]</ta>
            <ta e="T837" id="Seg_10743" s="T836">v-v:tense.[v:pn]</ta>
            <ta e="T838" id="Seg_10744" s="T837">v-v:tense.[v:pn]</ta>
            <ta e="T839" id="Seg_10745" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_10746" s="T839">n.[n:case]</ta>
            <ta e="T844" id="Seg_10747" s="T843">adv</ta>
            <ta e="T845" id="Seg_10748" s="T844">dempro.[n:case]</ta>
            <ta e="T846" id="Seg_10749" s="T845">n.[n:case]</ta>
            <ta e="T847" id="Seg_10750" s="T846">ptcl</ta>
            <ta e="T848" id="Seg_10751" s="T847">v-v:n.fin</ta>
            <ta e="T850" id="Seg_10752" s="T849">que.[n:case]</ta>
            <ta e="T851" id="Seg_10753" s="T850">pers</ta>
            <ta e="T852" id="Seg_10754" s="T851">que.[n:case]=ptcl</ta>
            <ta e="T853" id="Seg_10755" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_10756" s="T853">v-v:tense-v:pn</ta>
            <ta e="T856" id="Seg_10757" s="T855">n.[n:case]</ta>
            <ta e="T857" id="Seg_10758" s="T856">pers</ta>
            <ta e="T858" id="Seg_10759" s="T857">v.[v:pn]</ta>
            <ta e="T860" id="Seg_10760" s="T859">n-n&gt;adj</ta>
            <ta e="T862" id="Seg_10761" s="T861">n.[n:case]</ta>
            <ta e="T864" id="Seg_10762" s="T863">dempro.[n:case]</ta>
            <ta e="T865" id="Seg_10763" s="T864">v-v:tense.[v:pn]</ta>
            <ta e="T866" id="Seg_10764" s="T865">adv</ta>
            <ta e="T867" id="Seg_10765" s="T866">n-n:case</ta>
            <ta e="T869" id="Seg_10766" s="T868">adv</ta>
            <ta e="T870" id="Seg_10767" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_10768" s="T870">v-v:n.fin</ta>
            <ta e="T872" id="Seg_10769" s="T871">n.[n:case]</ta>
            <ta e="T873" id="Seg_10770" s="T872">v-v:mood.pn</ta>
            <ta e="T874" id="Seg_10771" s="T873">adv</ta>
            <ta e="T875" id="Seg_10772" s="T874">dempro.[n:case]</ta>
            <ta e="T876" id="Seg_10773" s="T875">v-v:tense.[v:pn]</ta>
            <ta e="T877" id="Seg_10774" s="T876">que.[n:case]</ta>
            <ta e="T878" id="Seg_10775" s="T877">pers</ta>
            <ta e="T879" id="Seg_10776" s="T878">n.[n:case]</ta>
            <ta e="T880" id="Seg_10777" s="T879">adv</ta>
            <ta e="T882" id="Seg_10778" s="T881">pers</ta>
            <ta e="T883" id="Seg_10779" s="T882">adv</ta>
            <ta e="T884" id="Seg_10780" s="T883">n.[n:case]</ta>
            <ta e="T886" id="Seg_10781" s="T885">n-n:case.poss</ta>
            <ta e="T887" id="Seg_10782" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_10783" s="T887">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T889" id="Seg_10784" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_10785" s="T889">v-v:ins-v:mood.pn</ta>
            <ta e="T891" id="Seg_10786" s="T890">n-n:case</ta>
            <ta e="T895" id="Seg_10787" s="T894">dempro.[n:case]</ta>
            <ta e="T896" id="Seg_10788" s="T895">n.[n:case]</ta>
            <ta e="T897" id="Seg_10789" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_10790" s="T897">dempro-n:case</ta>
            <ta e="T899" id="Seg_10791" s="T898">v-v:n.fin</ta>
            <ta e="T900" id="Seg_10792" s="T899">que.[n:case]</ta>
            <ta e="T901" id="Seg_10793" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_10794" s="T901">v-v:tense-v:pn</ta>
            <ta e="T903" id="Seg_10795" s="T902">n.[n:case]</ta>
            <ta e="T905" id="Seg_10796" s="T904">pers</ta>
            <ta e="T906" id="Seg_10797" s="T905">n.[n:case]</ta>
            <ta e="T907" id="Seg_10798" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_10799" s="T907">adj.[n:case]</ta>
            <ta e="T910" id="Seg_10800" s="T909">dempro.[n:case]</ta>
            <ta e="T911" id="Seg_10801" s="T910">adv</ta>
            <ta e="T912" id="Seg_10802" s="T911">v-v:tense.[v:pn]</ta>
            <ta e="T913" id="Seg_10803" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_10804" s="T913">v-v:n.fin</ta>
            <ta e="T915" id="Seg_10805" s="T914">n.[n:case]</ta>
            <ta e="T916" id="Seg_10806" s="T915">n.[n:case]</ta>
            <ta e="T917" id="Seg_10807" s="T916">v-v:mood.pn</ta>
            <ta e="T918" id="Seg_10808" s="T917">adv</ta>
            <ta e="T920" id="Seg_10809" s="T919">n.[n:case]</ta>
            <ta e="T921" id="Seg_10810" s="T920">v-v:tense.[v:pn]</ta>
            <ta e="T922" id="Seg_10811" s="T921">que.[n:case]</ta>
            <ta e="T923" id="Seg_10812" s="T922">pers</ta>
            <ta e="T924" id="Seg_10813" s="T923">adv</ta>
            <ta e="T926" id="Seg_10814" s="T925">n.[n:case]</ta>
            <ta e="T927" id="Seg_10815" s="T926">adv</ta>
            <ta e="T929" id="Seg_10816" s="T928">pers</ta>
            <ta e="T930" id="Seg_10817" s="T929">n-n:case.poss</ta>
            <ta e="T931" id="Seg_10818" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_10819" s="T931">adj.[n:case]</ta>
            <ta e="T934" id="Seg_10820" s="T933">v-v:ins-v:mood.pn</ta>
            <ta e="T935" id="Seg_10821" s="T934">n-n:case-n:case.poss</ta>
            <ta e="T939" id="Seg_10822" s="T938">adv</ta>
            <ta e="T940" id="Seg_10823" s="T939">adv</ta>
            <ta e="T941" id="Seg_10824" s="T940">dempro.[n:case]</ta>
            <ta e="T942" id="Seg_10825" s="T941">n.[n:case]</ta>
            <ta e="T944" id="Seg_10826" s="T942">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T945" id="Seg_10827" s="T944">v-v:ins-v:mood.pn</ta>
            <ta e="T946" id="Seg_10828" s="T945">n-n:case</ta>
            <ta e="T947" id="Seg_10829" s="T946">v-v:mood.pn</ta>
            <ta e="T948" id="Seg_10830" s="T947">conj</ta>
            <ta e="T949" id="Seg_10831" s="T948">pers</ta>
            <ta e="T950" id="Seg_10832" s="T949">v-v:tense-v:pn</ta>
            <ta e="T951" id="Seg_10833" s="T950">n-n:case</ta>
            <ta e="T955" id="Seg_10834" s="T954">adv</ta>
            <ta e="T956" id="Seg_10835" s="T955">n.[n:case]</ta>
            <ta e="T957" id="Seg_10836" s="T956">v-v:tense.[v:pn]</ta>
            <ta e="T958" id="Seg_10837" s="T957">v-v:ins-v:mood.pn</ta>
            <ta e="T959" id="Seg_10838" s="T958">n-n:case-n:case.poss</ta>
            <ta e="T961" id="Seg_10839" s="T960">dempro.[n:case]</ta>
            <ta e="T962" id="Seg_10840" s="T961">v-v:tense.[v:pn]</ta>
            <ta e="T963" id="Seg_10841" s="T962">conj</ta>
            <ta e="T964" id="Seg_10842" s="T963">dempro.[n:case]</ta>
            <ta e="T965" id="Seg_10843" s="T964">n.[n:case]</ta>
            <ta e="T966" id="Seg_10844" s="T965">adv</ta>
            <ta e="T968" id="Seg_10845" s="T966">v-v&gt;v-v&gt;v.[v:pn]</ta>
            <ta e="T969" id="Seg_10846" s="T968">v-v:ins-v:mood.pn</ta>
            <ta e="T970" id="Seg_10847" s="T969">n-n:case</ta>
            <ta e="T971" id="Seg_10848" s="T970">v-v:mood.pn</ta>
            <ta e="T972" id="Seg_10849" s="T971">conj</ta>
            <ta e="T973" id="Seg_10850" s="T972">pers</ta>
            <ta e="T974" id="Seg_10851" s="T973">v-v:tense-v:pn</ta>
            <ta e="T975" id="Seg_10852" s="T974">n-n:case</ta>
            <ta e="T977" id="Seg_10853" s="T976">dempro.[n:case]</ta>
            <ta e="T978" id="Seg_10854" s="T977">v-v:tense.[v:pn]</ta>
            <ta e="T979" id="Seg_10855" s="T978">n-n:case</ta>
            <ta e="T980" id="Seg_10856" s="T979">n.[n:case]</ta>
            <ta e="T981" id="Seg_10857" s="T980">v-v:tense.[v:pn]</ta>
            <ta e="T983" id="Seg_10858" s="T982">n.[n:case]</ta>
            <ta e="T984" id="Seg_10859" s="T983">v-v:tense.[v:pn]</ta>
            <ta e="T985" id="Seg_10860" s="T984">que.[n:case]</ta>
            <ta e="T986" id="Seg_10861" s="T985">pers</ta>
            <ta e="T987" id="Seg_10862" s="T986">adv</ta>
            <ta e="T989" id="Seg_10863" s="T988">pers</ta>
            <ta e="T990" id="Seg_10864" s="T989">n-n:case.poss</ta>
            <ta e="T991" id="Seg_10865" s="T990">v-v&gt;v.[v:pn]</ta>
            <ta e="T992" id="Seg_10866" s="T991">conj</ta>
            <ta e="T993" id="Seg_10867" s="T992">pers</ta>
            <ta e="T994" id="Seg_10868" s="T993">n-n:case</ta>
            <ta e="T995" id="Seg_10869" s="T994">v-v:tense-v:pn</ta>
            <ta e="T997" id="Seg_10870" s="T996">v-v:ins-v:mood.pn</ta>
            <ta e="T998" id="Seg_10871" s="T997">n-n:case-n:case.poss</ta>
            <ta e="T1002" id="Seg_10872" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_10873" s="T1002">n.[n:case]</ta>
            <ta e="T1004" id="Seg_10874" s="T1003">v-v:tense.[v:pn]</ta>
            <ta e="T1005" id="Seg_10875" s="T1004">n-n:case.poss</ta>
            <ta e="T1006" id="Seg_10876" s="T1005">dempro.[n:case]</ta>
            <ta e="T1007" id="Seg_10877" s="T1006">ptcl</ta>
            <ta e="T1008" id="Seg_10878" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_10879" s="T1008">v-v:n.fin</ta>
            <ta e="T1013" id="Seg_10880" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_10881" s="T1013">v-v:n.fin</ta>
            <ta e="T1015" id="Seg_10882" s="T1014">v</ta>
            <ta e="T1017" id="Seg_10883" s="T1016">v-v:ins-v:mood.pn</ta>
            <ta e="T1018" id="Seg_10884" s="T1017">adv</ta>
            <ta e="T1019" id="Seg_10885" s="T1018">n-n:case</ta>
            <ta e="T1020" id="Seg_10886" s="T1019">v-v:mood.pn</ta>
            <ta e="T1021" id="Seg_10887" s="T1020">conj</ta>
            <ta e="T1022" id="Seg_10888" s="T1021">pers</ta>
            <ta e="T1027" id="Seg_10889" s="T1026">conj</ta>
            <ta e="T1028" id="Seg_10890" s="T1027">n.[n:case]</ta>
            <ta e="T1029" id="Seg_10891" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_10892" s="T1029">n-n:num-n:case.poss</ta>
            <ta e="T1035" id="Seg_10893" s="T1034">adv</ta>
            <ta e="T1036" id="Seg_10894" s="T1035">dempro.[n:case]</ta>
            <ta e="T1037" id="Seg_10895" s="T1036">v-v:tense.[v:pn]</ta>
            <ta e="T1038" id="Seg_10896" s="T1037">n-n:case</ta>
            <ta e="T1040" id="Seg_10897" s="T1039">n.[n:case]</ta>
            <ta e="T1041" id="Seg_10898" s="T1040">n.[n:case]</ta>
            <ta e="T1042" id="Seg_10899" s="T1041">v-v:mood.pn</ta>
            <ta e="T1043" id="Seg_10900" s="T1042">adv</ta>
            <ta e="T1044" id="Seg_10901" s="T1043">n.[n:case]</ta>
            <ta e="T1045" id="Seg_10902" s="T1044">v-v:tense.[v:pn]</ta>
            <ta e="T1047" id="Seg_10903" s="T1046">que.[n:case]</ta>
            <ta e="T1048" id="Seg_10904" s="T1047">pers</ta>
            <ta e="T1049" id="Seg_10905" s="T1048">adv</ta>
            <ta e="T1050" id="Seg_10906" s="T1049">n.[n:case]</ta>
            <ta e="T1051" id="Seg_10907" s="T1050">conj</ta>
            <ta e="T1052" id="Seg_10908" s="T1051">pers</ta>
            <ta e="T1053" id="Seg_10909" s="T1052">n-n:case.poss</ta>
            <ta e="T1055" id="Seg_10910" s="T1054">v-v&gt;v.[v:pn]</ta>
            <ta e="T1056" id="Seg_10911" s="T1055">conj</ta>
            <ta e="T1057" id="Seg_10912" s="T1056">pers</ta>
            <ta e="T1059" id="Seg_10913" s="T1058">conj</ta>
            <ta e="T1060" id="Seg_10914" s="T1059">pers</ta>
            <ta e="T1061" id="Seg_10915" s="T1060">v-v:tense-v:pn</ta>
            <ta e="T1062" id="Seg_10916" s="T1061">n-n:case</ta>
            <ta e="T1064" id="Seg_10917" s="T1063">conj</ta>
            <ta e="T1065" id="Seg_10918" s="T1064">pers</ta>
            <ta e="T1066" id="Seg_10919" s="T1065">n.[n:case]</ta>
            <ta e="T1067" id="Seg_10920" s="T1066">n-n:case.poss-n:case</ta>
            <ta e="T1068" id="Seg_10921" s="T1067">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1070" id="Seg_10922" s="T1069">v-v:ins-v:mood.pn</ta>
            <ta e="T1071" id="Seg_10923" s="T1070">n-n:case-n:case.poss</ta>
            <ta e="T1072" id="Seg_10924" s="T1071">ptcl</ta>
            <ta e="T1073" id="Seg_10925" s="T1072">v-v:tense-v:pn</ta>
            <ta e="T1074" id="Seg_10926" s="T1073">pers</ta>
            <ta e="T1078" id="Seg_10927" s="T1077">adv</ta>
            <ta e="T1079" id="Seg_10928" s="T1078">n.[n:case]</ta>
            <ta e="T1080" id="Seg_10929" s="T1079">v-v:tense.[v:pn]</ta>
            <ta e="T1081" id="Seg_10930" s="T1080">n-n:case</ta>
            <ta e="T1083" id="Seg_10931" s="T1082">n-n:num</ta>
            <ta e="T1084" id="Seg_10932" s="T1083">n-n:num</ta>
            <ta e="T1085" id="Seg_10933" s="T1084">v-v:mood.pn</ta>
            <ta e="T1086" id="Seg_10934" s="T1085">adv</ta>
            <ta e="T1087" id="Seg_10935" s="T1086">n.[n:case]</ta>
            <ta e="T1088" id="Seg_10936" s="T1087">v-v:tense.[v:pn]</ta>
            <ta e="T1089" id="Seg_10937" s="T1088">que.[n:case]</ta>
            <ta e="T1090" id="Seg_10938" s="T1089">pers</ta>
            <ta e="T1091" id="Seg_10939" s="T1090">adv</ta>
            <ta e="T1093" id="Seg_10940" s="T1092">conj</ta>
            <ta e="T1094" id="Seg_10941" s="T1093">pers</ta>
            <ta e="T1095" id="Seg_10942" s="T1094">n-n:case.poss</ta>
            <ta e="T1096" id="Seg_10943" s="T1095">v-v&gt;v.[v:pn]</ta>
            <ta e="T1097" id="Seg_10944" s="T1096">conj</ta>
            <ta e="T1098" id="Seg_10945" s="T1097">pers</ta>
            <ta e="T1099" id="Seg_10946" s="T1098">v-v:tense-v:pn</ta>
            <ta e="T1103" id="Seg_10947" s="T1102">conj</ta>
            <ta e="T1104" id="Seg_10948" s="T1103">n.[n:case]</ta>
            <ta e="T1105" id="Seg_10949" s="T1104">conj</ta>
            <ta e="T1106" id="Seg_10950" s="T1105">pers</ta>
            <ta e="T1107" id="Seg_10951" s="T1106">v-v:tense.[v:pn]</ta>
            <ta e="T1108" id="Seg_10952" s="T1107">ptcl</ta>
            <ta e="T1109" id="Seg_10953" s="T1108">que.[n:case]</ta>
            <ta e="T1111" id="Seg_10954" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_10955" s="T1111">n.[n:case]</ta>
            <ta e="T1113" id="Seg_10956" s="T1112">v-v:tense.[v:pn]</ta>
            <ta e="T1114" id="Seg_10957" s="T1113">n-n:case</ta>
            <ta e="T1115" id="Seg_10958" s="T1114">que.[n:case]</ta>
            <ta e="T1117" id="Seg_10959" s="T1116">que.[n:case]=ptcl</ta>
            <ta e="T1119" id="Seg_10960" s="T1118">n-n:case</ta>
            <ta e="T1120" id="Seg_10961" s="T1119">ptcl</ta>
            <ta e="T1121" id="Seg_10962" s="T1120">v-v:tense.[v:pn]</ta>
            <ta e="T1123" id="Seg_10963" s="T1122">dempro.[n:case]</ta>
            <ta e="T1124" id="Seg_10964" s="T1123">v-v:tense.[v:pn]</ta>
            <ta e="T1125" id="Seg_10965" s="T1124">n-n:case.poss</ta>
            <ta e="T1127" id="Seg_10966" s="T1126">n.[n:case]</ta>
            <ta e="T1128" id="Seg_10967" s="T1127">v-v&gt;v.[v:pn]</ta>
            <ta e="T1129" id="Seg_10968" s="T1128">v-v&gt;v.[v:pn]</ta>
            <ta e="T1130" id="Seg_10969" s="T1129">conj</ta>
            <ta e="T1131" id="Seg_10970" s="T1130">n.[n:case]</ta>
            <ta e="T1132" id="Seg_10971" s="T1131">n-n:case.poss</ta>
            <ta e="T1133" id="Seg_10972" s="T1132">v-v&gt;v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_10973" s="T1">num</ta>
            <ta e="T3" id="Seg_10974" s="T2">n</ta>
            <ta e="T4" id="Seg_10975" s="T3">conj</ta>
            <ta e="T5" id="Seg_10976" s="T4">n</ta>
            <ta e="T7" id="Seg_10977" s="T6">num</ta>
            <ta e="T8" id="Seg_10978" s="T7">n</ta>
            <ta e="T9" id="Seg_10979" s="T8">v</ta>
            <ta e="T10" id="Seg_10980" s="T9">num</ta>
            <ta e="T11" id="Seg_10981" s="T10">n</ta>
            <ta e="T12" id="Seg_10982" s="T11">v</ta>
            <ta e="T13" id="Seg_10983" s="T12">adv</ta>
            <ta e="T15" id="Seg_10984" s="T14">n</ta>
            <ta e="T16" id="Seg_10985" s="T15">n</ta>
            <ta e="T17" id="Seg_10986" s="T16">v</ta>
            <ta e="T18" id="Seg_10987" s="T17">adv</ta>
            <ta e="T19" id="Seg_10988" s="T18">n</ta>
            <ta e="T20" id="Seg_10989" s="T19">n</ta>
            <ta e="T21" id="Seg_10990" s="T20">v</ta>
            <ta e="T22" id="Seg_10991" s="T21">adv</ta>
            <ta e="T23" id="Seg_10992" s="T22">n</ta>
            <ta e="T24" id="Seg_10993" s="T23">n</ta>
            <ta e="T25" id="Seg_10994" s="T24">v</ta>
            <ta e="T27" id="Seg_10995" s="T26">adv</ta>
            <ta e="T28" id="Seg_10996" s="T27">num</ta>
            <ta e="T29" id="Seg_10997" s="T28">v</ta>
            <ta e="T30" id="Seg_10998" s="T29">conj</ta>
            <ta e="T31" id="Seg_10999" s="T30">num</ta>
            <ta e="T32" id="Seg_11000" s="T31">ptcl</ta>
            <ta e="T34" id="Seg_11001" s="T33">adv</ta>
            <ta e="T35" id="Seg_11002" s="T34">num</ta>
            <ta e="T36" id="Seg_11003" s="T35">n</ta>
            <ta e="T37" id="Seg_11004" s="T36">v</ta>
            <ta e="T38" id="Seg_11005" s="T37">v</ta>
            <ta e="T39" id="Seg_11006" s="T38">adv</ta>
            <ta e="T40" id="Seg_11007" s="T39">propr</ta>
            <ta e="T41" id="Seg_11008" s="T40">conj</ta>
            <ta e="T42" id="Seg_11009" s="T41">num</ta>
            <ta e="T43" id="Seg_11010" s="T42">n</ta>
            <ta e="T44" id="Seg_11011" s="T43">v</ta>
            <ta e="T45" id="Seg_11012" s="T44">adv</ta>
            <ta e="T46" id="Seg_11013" s="T45">v</ta>
            <ta e="T47" id="Seg_11014" s="T46">conj</ta>
            <ta e="T48" id="Seg_11015" s="T47">n</ta>
            <ta e="T49" id="Seg_11016" s="T48">v</ta>
            <ta e="T52" id="Seg_11017" s="T51">n</ta>
            <ta e="T53" id="Seg_11018" s="T52">v</ta>
            <ta e="T54" id="Seg_11019" s="T53">conj</ta>
            <ta e="T55" id="Seg_11020" s="T54">dempro</ta>
            <ta e="T56" id="Seg_11021" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_11022" s="T56">v</ta>
            <ta e="T59" id="Seg_11023" s="T58">adv</ta>
            <ta e="T60" id="Seg_11024" s="T59">dempro</ta>
            <ta e="T61" id="Seg_11025" s="T60">n</ta>
            <ta e="T62" id="Seg_11026" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_11027" s="T62">v</ta>
            <ta e="T64" id="Seg_11028" s="T63">adv</ta>
            <ta e="T65" id="Seg_11029" s="T64">dempro</ta>
            <ta e="T66" id="Seg_11030" s="T65">n</ta>
            <ta e="T67" id="Seg_11031" s="T66">v</ta>
            <ta e="T68" id="Seg_11032" s="T67">n</ta>
            <ta e="T69" id="Seg_11033" s="T68">n</ta>
            <ta e="T70" id="Seg_11034" s="T69">conj</ta>
            <ta e="T71" id="Seg_11035" s="T70">n</ta>
            <ta e="T72" id="Seg_11036" s="T71">conj</ta>
            <ta e="T73" id="Seg_11037" s="T72">n</ta>
            <ta e="T1134" id="Seg_11038" s="T74">v</ta>
            <ta e="T75" id="Seg_11039" s="T1134">v</ta>
            <ta e="T77" id="Seg_11040" s="T76">adv</ta>
            <ta e="T78" id="Seg_11041" s="T77">quant</ta>
            <ta e="T79" id="Seg_11042" s="T78">n</ta>
            <ta e="T81" id="Seg_11043" s="T80">n</ta>
            <ta e="T82" id="Seg_11044" s="T81">conj</ta>
            <ta e="T83" id="Seg_11045" s="T82">n</ta>
            <ta e="T84" id="Seg_11046" s="T83">conj</ta>
            <ta e="T85" id="Seg_11047" s="T84">n</ta>
            <ta e="T86" id="Seg_11048" s="T85">v</ta>
            <ta e="T88" id="Seg_11049" s="T87">n</ta>
            <ta e="T89" id="Seg_11050" s="T88">conj</ta>
            <ta e="T90" id="Seg_11051" s="T89">dempro</ta>
            <ta e="T91" id="Seg_11052" s="T90">v</ta>
            <ta e="T92" id="Seg_11053" s="T91">n</ta>
            <ta e="T93" id="Seg_11054" s="T92">n</ta>
            <ta e="T94" id="Seg_11055" s="T93">v</ta>
            <ta e="T95" id="Seg_11056" s="T94">n</ta>
            <ta e="T96" id="Seg_11057" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_11058" s="T96">n</ta>
            <ta e="T98" id="Seg_11059" s="T97">v</ta>
            <ta e="T99" id="Seg_11060" s="T98">adv</ta>
            <ta e="T100" id="Seg_11061" s="T99">n</ta>
            <ta e="T1135" id="Seg_11062" s="T100">v</ta>
            <ta e="T101" id="Seg_11063" s="T1135">v</ta>
            <ta e="T102" id="Seg_11064" s="T101">n</ta>
            <ta e="T1136" id="Seg_11065" s="T102">v</ta>
            <ta e="T103" id="Seg_11066" s="T1136">v</ta>
            <ta e="T105" id="Seg_11067" s="T104">conj</ta>
            <ta e="T106" id="Seg_11068" s="T105">n</ta>
            <ta e="T107" id="Seg_11069" s="T106">v</ta>
            <ta e="T108" id="Seg_11070" s="T107">n</ta>
            <ta e="T109" id="Seg_11071" s="T108">v</ta>
            <ta e="T110" id="Seg_11072" s="T109">conj</ta>
            <ta e="T111" id="Seg_11073" s="T110">n</ta>
            <ta e="T112" id="Seg_11074" s="T111">v</ta>
            <ta e="T113" id="Seg_11075" s="T112">conj</ta>
            <ta e="T114" id="Seg_11076" s="T113">v</ta>
            <ta e="T116" id="Seg_11077" s="T115">conj</ta>
            <ta e="T117" id="Seg_11078" s="T116">n</ta>
            <ta e="T118" id="Seg_11079" s="T117">v</ta>
            <ta e="T119" id="Seg_11080" s="T118">v</ta>
            <ta e="T120" id="Seg_11081" s="T119">conj</ta>
            <ta e="T121" id="Seg_11082" s="T120">v</ta>
            <ta e="T123" id="Seg_11083" s="T122">n</ta>
            <ta e="T124" id="Seg_11084" s="T123">adv</ta>
            <ta e="T126" id="Seg_11085" s="T125">v</ta>
            <ta e="T127" id="Seg_11086" s="T126">adv</ta>
            <ta e="T128" id="Seg_11087" s="T127">v</ta>
            <ta e="T129" id="Seg_11088" s="T128">v</ta>
            <ta e="T130" id="Seg_11089" s="T129">n</ta>
            <ta e="T131" id="Seg_11090" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_11091" s="T131">v</ta>
            <ta e="T134" id="Seg_11092" s="T133">n</ta>
            <ta e="T135" id="Seg_11093" s="T134">v</ta>
            <ta e="T136" id="Seg_11094" s="T135">n</ta>
            <ta e="T137" id="Seg_11095" s="T136">n</ta>
            <ta e="T139" id="Seg_11096" s="T138">n</ta>
            <ta e="T140" id="Seg_11097" s="T139">n</ta>
            <ta e="T141" id="Seg_11098" s="T140">n</ta>
            <ta e="T143" id="Seg_11099" s="T142">n</ta>
            <ta e="T144" id="Seg_11100" s="T143">v</ta>
            <ta e="T145" id="Seg_11101" s="T144">n</ta>
            <ta e="T146" id="Seg_11102" s="T145">conj</ta>
            <ta e="T147" id="Seg_11103" s="T146">adv</ta>
            <ta e="T148" id="Seg_11104" s="T147">v</ta>
            <ta e="T149" id="Seg_11105" s="T148">n</ta>
            <ta e="T151" id="Seg_11106" s="T150">dempro</ta>
            <ta e="T152" id="Seg_11107" s="T151">v</ta>
            <ta e="T153" id="Seg_11108" s="T152">v</ta>
            <ta e="T155" id="Seg_11109" s="T154">n</ta>
            <ta e="T156" id="Seg_11110" s="T155">v</ta>
            <ta e="T158" id="Seg_11111" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_11112" s="T158">v</ta>
            <ta e="T160" id="Seg_11113" s="T159">v</ta>
            <ta e="T161" id="Seg_11114" s="T160">pers</ta>
            <ta e="T162" id="Seg_11115" s="T161">adv</ta>
            <ta e="T163" id="Seg_11116" s="T162">n</ta>
            <ta e="T164" id="Seg_11117" s="T163">v</ta>
            <ta e="T166" id="Seg_11118" s="T165">dempro</ta>
            <ta e="T167" id="Seg_11119" s="T166">v</ta>
            <ta e="T168" id="Seg_11120" s="T167">v</ta>
            <ta e="T169" id="Seg_11121" s="T168">n</ta>
            <ta e="T170" id="Seg_11122" s="T169">n</ta>
            <ta e="T172" id="Seg_11123" s="T171">v</ta>
            <ta e="T174" id="Seg_11124" s="T173">n</ta>
            <ta e="T176" id="Seg_11125" s="T175">pers</ta>
            <ta e="T177" id="Seg_11126" s="T176">dempro</ta>
            <ta e="T178" id="Seg_11127" s="T177">aux</ta>
            <ta e="T180" id="Seg_11128" s="T179">v</ta>
            <ta e="T181" id="Seg_11129" s="T180">v</ta>
            <ta e="T182" id="Seg_11130" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_11131" s="T182">n</ta>
            <ta e="T184" id="Seg_11132" s="T183">v</ta>
            <ta e="T185" id="Seg_11133" s="T184">conj</ta>
            <ta e="T186" id="Seg_11134" s="T185">v</ta>
            <ta e="T187" id="Seg_11135" s="T186">n</ta>
            <ta e="T188" id="Seg_11136" s="T187">v</ta>
            <ta e="T189" id="Seg_11137" s="T188">dempro</ta>
            <ta e="T190" id="Seg_11138" s="T189">n</ta>
            <ta e="T191" id="Seg_11139" s="T190">v</ta>
            <ta e="T193" id="Seg_11140" s="T192">n</ta>
            <ta e="T194" id="Seg_11141" s="T193">v</ta>
            <ta e="T195" id="Seg_11142" s="T194">n</ta>
            <ta e="T196" id="Seg_11143" s="T195">v</ta>
            <ta e="T198" id="Seg_11144" s="T197">adv</ta>
            <ta e="T199" id="Seg_11145" s="T198">n</ta>
            <ta e="T200" id="Seg_11146" s="T199">v</ta>
            <ta e="T201" id="Seg_11147" s="T200">v</ta>
            <ta e="T202" id="Seg_11148" s="T201">n</ta>
            <ta e="T203" id="Seg_11149" s="T202">adj</ta>
            <ta e="T205" id="Seg_11150" s="T204">v</ta>
            <ta e="T207" id="Seg_11151" s="T206">que</ta>
            <ta e="T208" id="Seg_11152" s="T207">n</ta>
            <ta e="T210" id="Seg_11153" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_11154" s="T210">dempro</ta>
            <ta e="T212" id="Seg_11155" s="T211">v</ta>
            <ta e="T213" id="Seg_11156" s="T212">n</ta>
            <ta e="T215" id="Seg_11157" s="T214">conj</ta>
            <ta e="T216" id="Seg_11158" s="T215">n</ta>
            <ta e="T217" id="Seg_11159" s="T216">v</ta>
            <ta e="T218" id="Seg_11160" s="T217">aux</ta>
            <ta e="T219" id="Seg_11161" s="T218">v</ta>
            <ta e="T220" id="Seg_11162" s="T219">pers</ta>
            <ta e="T221" id="Seg_11163" s="T220">adv</ta>
            <ta e="T222" id="Seg_11164" s="T221">v</ta>
            <ta e="T224" id="Seg_11165" s="T223">adv</ta>
            <ta e="T225" id="Seg_11166" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_11167" s="T225">n</ta>
            <ta e="T227" id="Seg_11168" s="T226">v</ta>
            <ta e="T229" id="Seg_11169" s="T228">conj</ta>
            <ta e="T230" id="Seg_11170" s="T229">n</ta>
            <ta e="T231" id="Seg_11171" s="T230">v</ta>
            <ta e="T233" id="Seg_11172" s="T232">adv</ta>
            <ta e="T234" id="Seg_11173" s="T233">adj</ta>
            <ta e="T235" id="Seg_11174" s="T234">adv</ta>
            <ta e="T237" id="Seg_11175" s="T236">adv</ta>
            <ta e="T239" id="Seg_11176" s="T238">adv</ta>
            <ta e="T240" id="Seg_11177" s="T239">n</ta>
            <ta e="T241" id="Seg_11178" s="T240">n</ta>
            <ta e="T243" id="Seg_11179" s="T242">num</ta>
            <ta e="T245" id="Seg_11180" s="T244">dempro</ta>
            <ta e="T246" id="Seg_11181" s="T245">n</ta>
            <ta e="T247" id="Seg_11182" s="T246">adv</ta>
            <ta e="T248" id="Seg_11183" s="T247">adj</ta>
            <ta e="T249" id="Seg_11184" s="T248">v</ta>
            <ta e="T251" id="Seg_11185" s="T250">v</ta>
            <ta e="T252" id="Seg_11186" s="T251">n</ta>
            <ta e="T253" id="Seg_11187" s="T252">ptcl</ta>
            <ta e="T257" id="Seg_11188" s="T256">n</ta>
            <ta e="T258" id="Seg_11189" s="T257">v</ta>
            <ta e="T259" id="Seg_11190" s="T258">n</ta>
            <ta e="T261" id="Seg_11191" s="T260">adv</ta>
            <ta e="T262" id="Seg_11192" s="T261">v</ta>
            <ta e="T263" id="Seg_11193" s="T262">n</ta>
            <ta e="T264" id="Seg_11194" s="T263">pers</ta>
            <ta e="T265" id="Seg_11195" s="T264">dempro</ta>
            <ta e="T266" id="Seg_11196" s="T265">v</ta>
            <ta e="T267" id="Seg_11197" s="T266">n</ta>
            <ta e="T268" id="Seg_11198" s="T267">adv</ta>
            <ta e="T269" id="Seg_11199" s="T268">n</ta>
            <ta e="T270" id="Seg_11200" s="T269">v</ta>
            <ta e="T271" id="Seg_11201" s="T270">v</ta>
            <ta e="T273" id="Seg_11202" s="T272">v</ta>
            <ta e="T274" id="Seg_11203" s="T273">n</ta>
            <ta e="T278" id="Seg_11204" s="T277">dempro</ta>
            <ta e="T279" id="Seg_11205" s="T278">n</ta>
            <ta e="T280" id="Seg_11206" s="T279">adj</ta>
            <ta e="T281" id="Seg_11207" s="T280">n</ta>
            <ta e="T282" id="Seg_11208" s="T281">v</ta>
            <ta e="T283" id="Seg_11209" s="T282">conj</ta>
            <ta e="T284" id="Seg_11210" s="T283">adj</ta>
            <ta e="T285" id="Seg_11211" s="T284">n</ta>
            <ta e="T286" id="Seg_11212" s="T285">v</ta>
            <ta e="T288" id="Seg_11213" s="T287">n</ta>
            <ta e="T289" id="Seg_11214" s="T288">quant</ta>
            <ta e="T290" id="Seg_11215" s="T289">v</ta>
            <ta e="T291" id="Seg_11216" s="T290">n</ta>
            <ta e="T292" id="Seg_11217" s="T291">n</ta>
            <ta e="T294" id="Seg_11218" s="T293">n</ta>
            <ta e="T295" id="Seg_11219" s="T294">quant</ta>
            <ta e="T296" id="Seg_11220" s="T295">v</ta>
            <ta e="T297" id="Seg_11221" s="T296">ptcl</ta>
            <ta e="T299" id="Seg_11222" s="T298">n</ta>
            <ta e="T300" id="Seg_11223" s="T299">adv</ta>
            <ta e="T301" id="Seg_11224" s="T300">quant</ta>
            <ta e="T302" id="Seg_11225" s="T301">v</ta>
            <ta e="T303" id="Seg_11226" s="T302">n</ta>
            <ta e="T307" id="Seg_11227" s="T306">num</ta>
            <ta e="T308" id="Seg_11228" s="T307">n</ta>
            <ta e="T309" id="Seg_11229" s="T308">v</ta>
            <ta e="T310" id="Seg_11230" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_11231" s="T310">n</ta>
            <ta e="T315" id="Seg_11232" s="T314">n</ta>
            <ta e="T316" id="Seg_11233" s="T315">v</ta>
            <ta e="T317" id="Seg_11234" s="T316">n</ta>
            <ta e="T319" id="Seg_11235" s="T318">dempro</ta>
            <ta e="T320" id="Seg_11236" s="T319">refl</ta>
            <ta e="T321" id="Seg_11237" s="T320">v</ta>
            <ta e="T322" id="Seg_11238" s="T321">conj</ta>
            <ta e="T323" id="Seg_11239" s="T322">n</ta>
            <ta e="T324" id="Seg_11240" s="T323">v</ta>
            <ta e="T328" id="Seg_11241" s="T327">dempro</ta>
            <ta e="T329" id="Seg_11242" s="T328">n</ta>
            <ta e="T330" id="Seg_11243" s="T329">n</ta>
            <ta e="T331" id="Seg_11244" s="T330">n</ta>
            <ta e="T332" id="Seg_11245" s="T331">v</ta>
            <ta e="T333" id="Seg_11246" s="T332">ptcl</ta>
            <ta e="T337" id="Seg_11247" s="T336">dempro</ta>
            <ta e="T338" id="Seg_11248" s="T337">n</ta>
            <ta e="T339" id="Seg_11249" s="T338">adv</ta>
            <ta e="T340" id="Seg_11250" s="T339">n</ta>
            <ta e="T341" id="Seg_11251" s="T340">adj</ta>
            <ta e="T342" id="Seg_11252" s="T341">v</ta>
            <ta e="T344" id="Seg_11253" s="T343">conj</ta>
            <ta e="T345" id="Seg_11254" s="T344">n</ta>
            <ta e="T346" id="Seg_11255" s="T345">adj</ta>
            <ta e="T347" id="Seg_11256" s="T346">v</ta>
            <ta e="T348" id="Seg_11257" s="T347">quant</ta>
            <ta e="T349" id="Seg_11258" s="T348">v</ta>
            <ta e="T350" id="Seg_11259" s="T349">adj</ta>
            <ta e="T351" id="Seg_11260" s="T350">v</ta>
            <ta e="T355" id="Seg_11261" s="T354">n</ta>
            <ta e="T356" id="Seg_11262" s="T355">n</ta>
            <ta e="T358" id="Seg_11263" s="T357">v</ta>
            <ta e="T359" id="Seg_11264" s="T358">n</ta>
            <ta e="T360" id="Seg_11265" s="T359">adv</ta>
            <ta e="T362" id="Seg_11266" s="T361">n</ta>
            <ta e="T363" id="Seg_11267" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_11268" s="T363">v</ta>
            <ta e="T365" id="Seg_11269" s="T364">n</ta>
            <ta e="T367" id="Seg_11270" s="T366">conj</ta>
            <ta e="T371" id="Seg_11271" s="T370">conj</ta>
            <ta e="T372" id="Seg_11272" s="T371">n</ta>
            <ta e="T373" id="Seg_11273" s="T372">n</ta>
            <ta e="T374" id="Seg_11274" s="T373">adj</ta>
            <ta e="T375" id="Seg_11275" s="T374">v</ta>
            <ta e="T379" id="Seg_11276" s="T378">pers</ta>
            <ta e="T382" id="Seg_11277" s="T381">v</ta>
            <ta e="T383" id="Seg_11278" s="T382">v</ta>
            <ta e="T384" id="Seg_11279" s="T383">conj</ta>
            <ta e="T385" id="Seg_11280" s="T384">v</ta>
            <ta e="T387" id="Seg_11281" s="T386">conj</ta>
            <ta e="T388" id="Seg_11282" s="T387">adv</ta>
            <ta e="T389" id="Seg_11283" s="T388">v</ta>
            <ta e="T390" id="Seg_11284" s="T389">v</ta>
            <ta e="T391" id="Seg_11285" s="T390">v</ta>
            <ta e="T398" id="Seg_11286" s="T397">n</ta>
            <ta e="T399" id="Seg_11287" s="T398">num</ta>
            <ta e="T400" id="Seg_11288" s="T399">n</ta>
            <ta e="T401" id="Seg_11289" s="T400">v</ta>
            <ta e="T402" id="Seg_11290" s="T401">dempro</ta>
            <ta e="T403" id="Seg_11291" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_11292" s="T403">v</ta>
            <ta e="T405" id="Seg_11293" s="T404">n</ta>
            <ta e="T406" id="Seg_11294" s="T405">adv</ta>
            <ta e="T407" id="Seg_11295" s="T406">v</ta>
            <ta e="T411" id="Seg_11296" s="T410">adv</ta>
            <ta e="T412" id="Seg_11297" s="T411">n</ta>
            <ta e="T413" id="Seg_11298" s="T412">v</ta>
            <ta e="T414" id="Seg_11299" s="T413">n</ta>
            <ta e="T415" id="Seg_11300" s="T414">v</ta>
            <ta e="T416" id="Seg_11301" s="T415">n</ta>
            <ta e="T417" id="Seg_11302" s="T416">dempro</ta>
            <ta e="T418" id="Seg_11303" s="T417">v</ta>
            <ta e="T422" id="Seg_11304" s="T421">dempro</ta>
            <ta e="T423" id="Seg_11305" s="T422">dempro</ta>
            <ta e="T424" id="Seg_11306" s="T423">n</ta>
            <ta e="T425" id="Seg_11307" s="T424">v</ta>
            <ta e="T426" id="Seg_11308" s="T425">n</ta>
            <ta e="T427" id="Seg_11309" s="T426">v</ta>
            <ta e="T429" id="Seg_11310" s="T428">n</ta>
            <ta e="T430" id="Seg_11311" s="T429">v</ta>
            <ta e="T431" id="Seg_11312" s="T430">ptcl</ta>
            <ta e="T433" id="Seg_11313" s="T432">n</ta>
            <ta e="T434" id="Seg_11314" s="T433">v</ta>
            <ta e="T435" id="Seg_11315" s="T434">v</ta>
            <ta e="T437" id="Seg_11316" s="T436">dempro</ta>
            <ta e="T438" id="Seg_11317" s="T437">adv</ta>
            <ta e="T439" id="Seg_11318" s="T438">v</ta>
            <ta e="T440" id="Seg_11319" s="T439">conj</ta>
            <ta e="T441" id="Seg_11320" s="T440">v</ta>
            <ta e="T445" id="Seg_11321" s="T444">adv</ta>
            <ta e="T446" id="Seg_11322" s="T445">dempro</ta>
            <ta e="T447" id="Seg_11323" s="T446">n</ta>
            <ta e="T448" id="Seg_11324" s="T447">v</ta>
            <ta e="T450" id="Seg_11325" s="T449">n</ta>
            <ta e="T451" id="Seg_11326" s="T450">v</ta>
            <ta e="T452" id="Seg_11327" s="T451">n</ta>
            <ta e="T453" id="Seg_11328" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_11329" s="T453">v</ta>
            <ta e="T456" id="Seg_11330" s="T455">adv</ta>
            <ta e="T457" id="Seg_11331" s="T456">n</ta>
            <ta e="T458" id="Seg_11332" s="T457">v</ta>
            <ta e="T460" id="Seg_11333" s="T459">n</ta>
            <ta e="T461" id="Seg_11334" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_11335" s="T461">n</ta>
            <ta e="T463" id="Seg_11336" s="T462">v</ta>
            <ta e="T464" id="Seg_11337" s="T463">conj</ta>
            <ta e="T465" id="Seg_11338" s="T464">n</ta>
            <ta e="T467" id="Seg_11339" s="T466">adv</ta>
            <ta e="T468" id="Seg_11340" s="T467">v</ta>
            <ta e="T470" id="Seg_11341" s="T469">interj</ta>
            <ta e="T471" id="Seg_11342" s="T470">v</ta>
            <ta e="T472" id="Seg_11343" s="T471">v</ta>
            <ta e="T473" id="Seg_11344" s="T472">n</ta>
            <ta e="T474" id="Seg_11345" s="T473">ptcl</ta>
            <ta e="T476" id="Seg_11346" s="T475">n</ta>
            <ta e="T477" id="Seg_11347" s="T476">n</ta>
            <ta e="T478" id="Seg_11348" s="T477">v</ta>
            <ta e="T482" id="Seg_11349" s="T481">n</ta>
            <ta e="T483" id="Seg_11350" s="T482">v</ta>
            <ta e="T484" id="Seg_11351" s="T483">adj</ta>
            <ta e="T485" id="Seg_11352" s="T484">conj</ta>
            <ta e="T486" id="Seg_11353" s="T485">n</ta>
            <ta e="T487" id="Seg_11354" s="T486">num</ta>
            <ta e="T488" id="Seg_11355" s="T487">num</ta>
            <ta e="T489" id="Seg_11356" s="T488">num</ta>
            <ta e="T490" id="Seg_11357" s="T489">num</ta>
            <ta e="T491" id="Seg_11358" s="T490">num</ta>
            <ta e="T492" id="Seg_11359" s="T491">n</ta>
            <ta e="T496" id="Seg_11360" s="T495">n</ta>
            <ta e="T497" id="Seg_11361" s="T496">v</ta>
            <ta e="T498" id="Seg_11362" s="T497">conj</ta>
            <ta e="T499" id="Seg_11363" s="T498">n</ta>
            <ta e="T501" id="Seg_11364" s="T500">v</ta>
            <ta e="T505" id="Seg_11365" s="T504">adj</ta>
            <ta e="T506" id="Seg_11366" s="T505">num</ta>
            <ta e="T507" id="Seg_11367" s="T506">n</ta>
            <ta e="T509" id="Seg_11368" s="T508">pers</ta>
            <ta e="T510" id="Seg_11369" s="T509">v</ta>
            <ta e="T511" id="Seg_11370" s="T510">v</ta>
            <ta e="T512" id="Seg_11371" s="T511">adv</ta>
            <ta e="T514" id="Seg_11372" s="T513">v</ta>
            <ta e="T516" id="Seg_11373" s="T515">conj</ta>
            <ta e="T517" id="Seg_11374" s="T516">pers</ta>
            <ta e="T519" id="Seg_11375" s="T518">v</ta>
            <ta e="T1139" id="Seg_11376" s="T519">propr</ta>
            <ta e="T520" id="Seg_11377" s="T1139">n</ta>
            <ta e="T522" id="Seg_11378" s="T521">n</ta>
            <ta e="T523" id="Seg_11379" s="T522">n</ta>
            <ta e="T525" id="Seg_11380" s="T524">n</ta>
            <ta e="T526" id="Seg_11381" s="T525">v</ta>
            <ta e="T527" id="Seg_11382" s="T526">conj</ta>
            <ta e="T528" id="Seg_11383" s="T527">n</ta>
            <ta e="T530" id="Seg_11384" s="T529">adv</ta>
            <ta e="T531" id="Seg_11385" s="T530">n</ta>
            <ta e="T532" id="Seg_11386" s="T531">pers</ta>
            <ta e="T533" id="Seg_11387" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_11388" s="T533">v</ta>
            <ta e="T536" id="Seg_11389" s="T535">pers</ta>
            <ta e="T537" id="Seg_11390" s="T536">n</ta>
            <ta e="T538" id="Seg_11391" s="T537">v</ta>
            <ta e="T540" id="Seg_11392" s="T539">conj</ta>
            <ta e="T541" id="Seg_11393" s="T540">n</ta>
            <ta e="T542" id="Seg_11394" s="T541">v</ta>
            <ta e="T544" id="Seg_11395" s="T543">adv</ta>
            <ta e="T545" id="Seg_11396" s="T544">n</ta>
            <ta e="T546" id="Seg_11397" s="T545">n</ta>
            <ta e="T548" id="Seg_11398" s="T547">v</ta>
            <ta e="T549" id="Seg_11399" s="T548">dempro</ta>
            <ta e="T550" id="Seg_11400" s="T549">n</ta>
            <ta e="T1137" id="Seg_11401" s="T550">v</ta>
            <ta e="T551" id="Seg_11402" s="T1137">v</ta>
            <ta e="T552" id="Seg_11403" s="T551">v</ta>
            <ta e="T554" id="Seg_11404" s="T553">conj</ta>
            <ta e="T555" id="Seg_11405" s="T554">pers</ta>
            <ta e="T556" id="Seg_11406" s="T555">adv</ta>
            <ta e="T557" id="Seg_11407" s="T556">pers</ta>
            <ta e="T558" id="Seg_11408" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_11409" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_11410" s="T559">v</ta>
            <ta e="T561" id="Seg_11411" s="T560">n</ta>
            <ta e="T564" id="Seg_11412" s="T563">v</ta>
            <ta e="T566" id="Seg_11413" s="T565">conj</ta>
            <ta e="T567" id="Seg_11414" s="T566">pers</ta>
            <ta e="T568" id="Seg_11415" s="T567">v</ta>
            <ta e="T569" id="Seg_11416" s="T568">v</ta>
            <ta e="T570" id="Seg_11417" s="T569">v</ta>
            <ta e="T572" id="Seg_11418" s="T571">conj</ta>
            <ta e="T573" id="Seg_11419" s="T572">pers</ta>
            <ta e="T574" id="Seg_11420" s="T573">v</ta>
            <ta e="T575" id="Seg_11421" s="T574">pers</ta>
            <ta e="T577" id="Seg_11422" s="T576">conj</ta>
            <ta e="T578" id="Seg_11423" s="T577">pers</ta>
            <ta e="T579" id="Seg_11424" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_11425" s="T579">conj</ta>
            <ta e="T581" id="Seg_11426" s="T580">pers</ta>
            <ta e="T582" id="Seg_11427" s="T581">v</ta>
            <ta e="T584" id="Seg_11428" s="T583">adv</ta>
            <ta e="T585" id="Seg_11429" s="T584">n</ta>
            <ta e="T586" id="Seg_11430" s="T585">v</ta>
            <ta e="T588" id="Seg_11431" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_11432" s="T588">v</ta>
            <ta e="T590" id="Seg_11433" s="T589">dempro</ta>
            <ta e="T591" id="Seg_11434" s="T590">v</ta>
            <ta e="T592" id="Seg_11435" s="T591">conj</ta>
            <ta e="T594" id="Seg_11436" s="T593">conj</ta>
            <ta e="T595" id="Seg_11437" s="T594">pers</ta>
            <ta e="T596" id="Seg_11438" s="T595">v</ta>
            <ta e="T600" id="Seg_11439" s="T599">adv</ta>
            <ta e="T601" id="Seg_11440" s="T600">dempro</ta>
            <ta e="T602" id="Seg_11441" s="T601">v</ta>
            <ta e="T603" id="Seg_11442" s="T602">dempro</ta>
            <ta e="T604" id="Seg_11443" s="T603">v</ta>
            <ta e="T606" id="Seg_11444" s="T605">n</ta>
            <ta e="T607" id="Seg_11445" s="T606">v</ta>
            <ta e="T608" id="Seg_11446" s="T607">n</ta>
            <ta e="T609" id="Seg_11447" s="T608">v</ta>
            <ta e="T611" id="Seg_11448" s="T610">n</ta>
            <ta e="T612" id="Seg_11449" s="T611">v</ta>
            <ta e="T613" id="Seg_11450" s="T612">v</ta>
            <ta e="T615" id="Seg_11451" s="T614">adv</ta>
            <ta e="T616" id="Seg_11452" s="T615">v</ta>
            <ta e="T619" id="Seg_11453" s="T618">n</ta>
            <ta e="T620" id="Seg_11454" s="T619">v</ta>
            <ta e="T621" id="Seg_11455" s="T620">dempro</ta>
            <ta e="T623" id="Seg_11456" s="T622">v</ta>
            <ta e="T624" id="Seg_11457" s="T623">n</ta>
            <ta e="T626" id="Seg_11458" s="T625">n</ta>
            <ta e="T627" id="Seg_11459" s="T626">v</ta>
            <ta e="T628" id="Seg_11460" s="T627">n</ta>
            <ta e="T629" id="Seg_11461" s="T628">v</ta>
            <ta e="T630" id="Seg_11462" s="T629">n</ta>
            <ta e="T631" id="Seg_11463" s="T630">v</ta>
            <ta e="T633" id="Seg_11464" s="T632">conj</ta>
            <ta e="T634" id="Seg_11465" s="T633">n</ta>
            <ta e="T1138" id="Seg_11466" s="T634">v</ta>
            <ta e="T635" id="Seg_11467" s="T1138">v</ta>
            <ta e="T639" id="Seg_11468" s="T638">pers</ta>
            <ta e="T640" id="Seg_11469" s="T639">num</ta>
            <ta e="T641" id="Seg_11470" s="T640">n</ta>
            <ta e="T642" id="Seg_11471" s="T641">v</ta>
            <ta e="T643" id="Seg_11472" s="T642">v</ta>
            <ta e="T1140" id="Seg_11473" s="T643">propr</ta>
            <ta e="T644" id="Seg_11474" s="T1140">n</ta>
            <ta e="T646" id="Seg_11475" s="T645">v</ta>
            <ta e="T647" id="Seg_11476" s="T646">propr</ta>
            <ta e="T648" id="Seg_11477" s="T647">adv</ta>
            <ta e="T649" id="Seg_11478" s="T648">n</ta>
            <ta e="T650" id="Seg_11479" s="T649">v</ta>
            <ta e="T652" id="Seg_11480" s="T651">n</ta>
            <ta e="T653" id="Seg_11481" s="T652">adv</ta>
            <ta e="T654" id="Seg_11482" s="T653">v</ta>
            <ta e="T655" id="Seg_11483" s="T654">que</ta>
            <ta e="T657" id="Seg_11484" s="T656">v</ta>
            <ta e="T659" id="Seg_11485" s="T658">conj</ta>
            <ta e="T660" id="Seg_11486" s="T659">pers</ta>
            <ta e="T661" id="Seg_11487" s="T660">v</ta>
            <ta e="T662" id="Seg_11488" s="T661">conj</ta>
            <ta e="T664" id="Seg_11489" s="T663">adv</ta>
            <ta e="T665" id="Seg_11490" s="T664">v</ta>
            <ta e="T666" id="Seg_11491" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_11492" s="T666">n</ta>
            <ta e="T668" id="Seg_11493" s="T667">quant</ta>
            <ta e="T669" id="Seg_11494" s="T668">v</ta>
            <ta e="T673" id="Seg_11495" s="T672">v</ta>
            <ta e="T675" id="Seg_11496" s="T674">n</ta>
            <ta e="T676" id="Seg_11497" s="T675">que</ta>
            <ta e="T677" id="Seg_11498" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_11499" s="T677">v</ta>
            <ta e="T682" id="Seg_11500" s="T681">pers</ta>
            <ta e="T683" id="Seg_11501" s="T682">n</ta>
            <ta e="T684" id="Seg_11502" s="T683">v</ta>
            <ta e="T685" id="Seg_11503" s="T684">n</ta>
            <ta e="T686" id="Seg_11504" s="T685">v</ta>
            <ta e="T687" id="Seg_11505" s="T686">conj</ta>
            <ta e="T688" id="Seg_11506" s="T687">pers</ta>
            <ta e="T689" id="Seg_11507" s="T688">que</ta>
            <ta e="T690" id="Seg_11508" s="T689">v</ta>
            <ta e="T691" id="Seg_11509" s="T690">conj</ta>
            <ta e="T692" id="Seg_11510" s="T691">pers</ta>
            <ta e="T693" id="Seg_11511" s="T692">n</ta>
            <ta e="T694" id="Seg_11512" s="T693">v</ta>
            <ta e="T695" id="Seg_11513" s="T694">v</ta>
            <ta e="T696" id="Seg_11514" s="T695">conj</ta>
            <ta e="T697" id="Seg_11515" s="T696">n</ta>
            <ta e="T698" id="Seg_11516" s="T697">v</ta>
            <ta e="T702" id="Seg_11517" s="T701">propr</ta>
            <ta e="T703" id="Seg_11518" s="T702">ptcl</ta>
            <ta e="T704" id="Seg_11519" s="T703">v</ta>
            <ta e="T705" id="Seg_11520" s="T704">v</ta>
            <ta e="T706" id="Seg_11521" s="T705">conj</ta>
            <ta e="T707" id="Seg_11522" s="T706">n</ta>
            <ta e="T708" id="Seg_11523" s="T707">v</ta>
            <ta e="T709" id="Seg_11524" s="T708">conj</ta>
            <ta e="T711" id="Seg_11525" s="T710">n</ta>
            <ta e="T712" id="Seg_11526" s="T711">v</ta>
            <ta e="T713" id="Seg_11527" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_11528" s="T713">adv</ta>
            <ta e="T715" id="Seg_11529" s="T714">v</ta>
            <ta e="T719" id="Seg_11530" s="T718">ptcl</ta>
            <ta e="T722" id="Seg_11531" s="T721">v</ta>
            <ta e="T723" id="Seg_11532" s="T722">dempro</ta>
            <ta e="T725" id="Seg_11533" s="T724">num</ta>
            <ta e="T726" id="Seg_11534" s="T725">n</ta>
            <ta e="T728" id="Seg_11535" s="T727">n</ta>
            <ta e="T729" id="Seg_11536" s="T728">ptcl</ta>
            <ta e="T730" id="Seg_11537" s="T729">v</ta>
            <ta e="T731" id="Seg_11538" s="T730">ptcl</ta>
            <ta e="T732" id="Seg_11539" s="T731">v</ta>
            <ta e="T733" id="Seg_11540" s="T732">dempro</ta>
            <ta e="T734" id="Seg_11541" s="T733">dempro</ta>
            <ta e="T738" id="Seg_11542" s="T737">que</ta>
            <ta e="T739" id="Seg_11543" s="T738">v</ta>
            <ta e="T740" id="Seg_11544" s="T739">v</ta>
            <ta e="T741" id="Seg_11545" s="T740">conj</ta>
            <ta e="T742" id="Seg_11546" s="T741">que</ta>
            <ta e="T743" id="Seg_11547" s="T742">v</ta>
            <ta e="T744" id="Seg_11548" s="T743">v</ta>
            <ta e="T748" id="Seg_11549" s="T747">dempro</ta>
            <ta e="T749" id="Seg_11550" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_11551" s="T749">adv</ta>
            <ta e="T751" id="Seg_11552" s="T750">v</ta>
            <ta e="T752" id="Seg_11553" s="T751">conj</ta>
            <ta e="T753" id="Seg_11554" s="T752">adv</ta>
            <ta e="T754" id="Seg_11555" s="T753">adv</ta>
            <ta e="T755" id="Seg_11556" s="T754">adv</ta>
            <ta e="T756" id="Seg_11557" s="T755">v</ta>
            <ta e="T760" id="Seg_11558" s="T759">propr</ta>
            <ta e="T761" id="Seg_11559" s="T760">n</ta>
            <ta e="T762" id="Seg_11560" s="T761">n</ta>
            <ta e="T764" id="Seg_11561" s="T763">n</ta>
            <ta e="T765" id="Seg_11562" s="T764">adj</ta>
            <ta e="T766" id="Seg_11563" s="T765">n</ta>
            <ta e="T767" id="Seg_11564" s="T766">conj</ta>
            <ta e="T768" id="Seg_11565" s="T767">n</ta>
            <ta e="T769" id="Seg_11566" s="T768">adj</ta>
            <ta e="T770" id="Seg_11567" s="T769">n</ta>
            <ta e="T774" id="Seg_11568" s="T773">v</ta>
            <ta e="T775" id="Seg_11569" s="T774">n</ta>
            <ta e="T776" id="Seg_11570" s="T775">conj</ta>
            <ta e="T777" id="Seg_11571" s="T776">n</ta>
            <ta e="T780" id="Seg_11572" s="T779">n</ta>
            <ta e="T781" id="Seg_11573" s="T780">n</ta>
            <ta e="T783" id="Seg_11574" s="T782">n</ta>
            <ta e="T784" id="Seg_11575" s="T783">adv</ta>
            <ta e="T785" id="Seg_11576" s="T784">v</ta>
            <ta e="T787" id="Seg_11577" s="T786">conj</ta>
            <ta e="T788" id="Seg_11578" s="T787">n</ta>
            <ta e="T789" id="Seg_11579" s="T788">v</ta>
            <ta e="T790" id="Seg_11580" s="T789">n</ta>
            <ta e="T791" id="Seg_11581" s="T790">n</ta>
            <ta e="T793" id="Seg_11582" s="T792">v</ta>
            <ta e="T795" id="Seg_11583" s="T794">adj</ta>
            <ta e="T796" id="Seg_11584" s="T795">n</ta>
            <ta e="T797" id="Seg_11585" s="T796">num</ta>
            <ta e="T798" id="Seg_11586" s="T797">n</ta>
            <ta e="T799" id="Seg_11587" s="T798">v</ta>
            <ta e="T800" id="Seg_11588" s="T799">adv</ta>
            <ta e="T801" id="Seg_11589" s="T800">que</ta>
            <ta e="T802" id="Seg_11590" s="T801">ptcl</ta>
            <ta e="T803" id="Seg_11591" s="T802">v</ta>
            <ta e="T804" id="Seg_11592" s="T803">num</ta>
            <ta e="T805" id="Seg_11593" s="T804">n</ta>
            <ta e="T806" id="Seg_11594" s="T805">v</ta>
            <ta e="T807" id="Seg_11595" s="T806">adj</ta>
            <ta e="T809" id="Seg_11596" s="T808">pers</ta>
            <ta e="T810" id="Seg_11597" s="T809">v</ta>
            <ta e="T814" id="Seg_11598" s="T813">n</ta>
            <ta e="T815" id="Seg_11599" s="T814">adj</ta>
            <ta e="T816" id="Seg_11600" s="T815">v</ta>
            <ta e="T817" id="Seg_11601" s="T816">n</ta>
            <ta e="T819" id="Seg_11602" s="T818">n</ta>
            <ta e="T821" id="Seg_11603" s="T820">v</ta>
            <ta e="T822" id="Seg_11604" s="T821">pers</ta>
            <ta e="T823" id="Seg_11605" s="T822">n</ta>
            <ta e="T825" id="Seg_11606" s="T824">pers</ta>
            <ta e="T826" id="Seg_11607" s="T825">v</ta>
            <ta e="T828" id="Seg_11608" s="T827">v</ta>
            <ta e="T829" id="Seg_11609" s="T828">n</ta>
            <ta e="T830" id="Seg_11610" s="T829">conj</ta>
            <ta e="T831" id="Seg_11611" s="T830">n</ta>
            <ta e="T832" id="Seg_11612" s="T831">v</ta>
            <ta e="T834" id="Seg_11613" s="T833">n</ta>
            <ta e="T835" id="Seg_11614" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_11615" s="T835">adj</ta>
            <ta e="T837" id="Seg_11616" s="T836">v</ta>
            <ta e="T838" id="Seg_11617" s="T837">v</ta>
            <ta e="T839" id="Seg_11618" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_11619" s="T839">n</ta>
            <ta e="T844" id="Seg_11620" s="T843">adv</ta>
            <ta e="T845" id="Seg_11621" s="T844">dempro</ta>
            <ta e="T846" id="Seg_11622" s="T845">n</ta>
            <ta e="T847" id="Seg_11623" s="T846">ptcl</ta>
            <ta e="T848" id="Seg_11624" s="T847">v</ta>
            <ta e="T850" id="Seg_11625" s="T849">que</ta>
            <ta e="T851" id="Seg_11626" s="T850">pers</ta>
            <ta e="T852" id="Seg_11627" s="T851">que</ta>
            <ta e="T853" id="Seg_11628" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_11629" s="T853">v</ta>
            <ta e="T856" id="Seg_11630" s="T855">n</ta>
            <ta e="T857" id="Seg_11631" s="T856">pers</ta>
            <ta e="T858" id="Seg_11632" s="T857">v</ta>
            <ta e="T860" id="Seg_11633" s="T859">adj</ta>
            <ta e="T862" id="Seg_11634" s="T861">n</ta>
            <ta e="T864" id="Seg_11635" s="T863">dempro</ta>
            <ta e="T865" id="Seg_11636" s="T864">v</ta>
            <ta e="T866" id="Seg_11637" s="T865">adv</ta>
            <ta e="T867" id="Seg_11638" s="T866">n</ta>
            <ta e="T869" id="Seg_11639" s="T868">adv</ta>
            <ta e="T870" id="Seg_11640" s="T869">ptcl</ta>
            <ta e="T871" id="Seg_11641" s="T870">v</ta>
            <ta e="T872" id="Seg_11642" s="T871">n</ta>
            <ta e="T873" id="Seg_11643" s="T872">v</ta>
            <ta e="T874" id="Seg_11644" s="T873">adv</ta>
            <ta e="T875" id="Seg_11645" s="T874">dempro</ta>
            <ta e="T876" id="Seg_11646" s="T875">v</ta>
            <ta e="T877" id="Seg_11647" s="T876">que</ta>
            <ta e="T878" id="Seg_11648" s="T877">pers</ta>
            <ta e="T879" id="Seg_11649" s="T878">n</ta>
            <ta e="T880" id="Seg_11650" s="T879">adv</ta>
            <ta e="T882" id="Seg_11651" s="T881">pers</ta>
            <ta e="T883" id="Seg_11652" s="T882">adv</ta>
            <ta e="T884" id="Seg_11653" s="T883">n</ta>
            <ta e="T886" id="Seg_11654" s="T885">n</ta>
            <ta e="T887" id="Seg_11655" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_11656" s="T887">v</ta>
            <ta e="T889" id="Seg_11657" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_11658" s="T889">v</ta>
            <ta e="T891" id="Seg_11659" s="T890">n</ta>
            <ta e="T895" id="Seg_11660" s="T894">dempro</ta>
            <ta e="T896" id="Seg_11661" s="T895">n</ta>
            <ta e="T897" id="Seg_11662" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11663" s="T897">dempro</ta>
            <ta e="T899" id="Seg_11664" s="T898">v</ta>
            <ta e="T900" id="Seg_11665" s="T899">que</ta>
            <ta e="T901" id="Seg_11666" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_11667" s="T901">v</ta>
            <ta e="T903" id="Seg_11668" s="T902">n</ta>
            <ta e="T905" id="Seg_11669" s="T904">pers</ta>
            <ta e="T906" id="Seg_11670" s="T905">n</ta>
            <ta e="T907" id="Seg_11671" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_11672" s="T907">adj</ta>
            <ta e="T910" id="Seg_11673" s="T909">dempro</ta>
            <ta e="T911" id="Seg_11674" s="T910">adv</ta>
            <ta e="T912" id="Seg_11675" s="T911">v</ta>
            <ta e="T913" id="Seg_11676" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_11677" s="T913">v</ta>
            <ta e="T915" id="Seg_11678" s="T914">n</ta>
            <ta e="T916" id="Seg_11679" s="T915">n</ta>
            <ta e="T917" id="Seg_11680" s="T916">v</ta>
            <ta e="T918" id="Seg_11681" s="T917">adv</ta>
            <ta e="T920" id="Seg_11682" s="T919">n</ta>
            <ta e="T921" id="Seg_11683" s="T920">v</ta>
            <ta e="T922" id="Seg_11684" s="T921">que</ta>
            <ta e="T923" id="Seg_11685" s="T922">pers</ta>
            <ta e="T924" id="Seg_11686" s="T923">adv</ta>
            <ta e="T926" id="Seg_11687" s="T925">n</ta>
            <ta e="T927" id="Seg_11688" s="T926">adv</ta>
            <ta e="T929" id="Seg_11689" s="T928">pers</ta>
            <ta e="T930" id="Seg_11690" s="T929">n</ta>
            <ta e="T931" id="Seg_11691" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_11692" s="T931">adj</ta>
            <ta e="T934" id="Seg_11693" s="T933">v</ta>
            <ta e="T935" id="Seg_11694" s="T934">n</ta>
            <ta e="T939" id="Seg_11695" s="T938">adv</ta>
            <ta e="T940" id="Seg_11696" s="T939">adv</ta>
            <ta e="T941" id="Seg_11697" s="T940">dempro</ta>
            <ta e="T942" id="Seg_11698" s="T941">n</ta>
            <ta e="T944" id="Seg_11699" s="T942">v</ta>
            <ta e="T945" id="Seg_11700" s="T944">v</ta>
            <ta e="T946" id="Seg_11701" s="T945">n</ta>
            <ta e="T947" id="Seg_11702" s="T946">v</ta>
            <ta e="T948" id="Seg_11703" s="T947">conj</ta>
            <ta e="T949" id="Seg_11704" s="T948">pers</ta>
            <ta e="T950" id="Seg_11705" s="T949">v</ta>
            <ta e="T951" id="Seg_11706" s="T950">n</ta>
            <ta e="T955" id="Seg_11707" s="T954">adv</ta>
            <ta e="T956" id="Seg_11708" s="T955">n</ta>
            <ta e="T957" id="Seg_11709" s="T956">v</ta>
            <ta e="T958" id="Seg_11710" s="T957">v</ta>
            <ta e="T959" id="Seg_11711" s="T958">n</ta>
            <ta e="T961" id="Seg_11712" s="T960">dempro</ta>
            <ta e="T962" id="Seg_11713" s="T961">v</ta>
            <ta e="T963" id="Seg_11714" s="T962">conj</ta>
            <ta e="T964" id="Seg_11715" s="T963">dempro</ta>
            <ta e="T965" id="Seg_11716" s="T964">n</ta>
            <ta e="T966" id="Seg_11717" s="T965">adv</ta>
            <ta e="T968" id="Seg_11718" s="T966">v</ta>
            <ta e="T969" id="Seg_11719" s="T968">v</ta>
            <ta e="T970" id="Seg_11720" s="T969">n</ta>
            <ta e="T971" id="Seg_11721" s="T970">v</ta>
            <ta e="T972" id="Seg_11722" s="T971">conj</ta>
            <ta e="T973" id="Seg_11723" s="T972">pers</ta>
            <ta e="T974" id="Seg_11724" s="T973">v</ta>
            <ta e="T975" id="Seg_11725" s="T974">n</ta>
            <ta e="T977" id="Seg_11726" s="T976">dempro</ta>
            <ta e="T978" id="Seg_11727" s="T977">v</ta>
            <ta e="T979" id="Seg_11728" s="T978">n</ta>
            <ta e="T980" id="Seg_11729" s="T979">n</ta>
            <ta e="T981" id="Seg_11730" s="T980">v</ta>
            <ta e="T983" id="Seg_11731" s="T982">n</ta>
            <ta e="T984" id="Seg_11732" s="T983">v</ta>
            <ta e="T985" id="Seg_11733" s="T984">que</ta>
            <ta e="T986" id="Seg_11734" s="T985">pers</ta>
            <ta e="T987" id="Seg_11735" s="T986">adv</ta>
            <ta e="T989" id="Seg_11736" s="T988">pers</ta>
            <ta e="T990" id="Seg_11737" s="T989">n</ta>
            <ta e="T991" id="Seg_11738" s="T990">v</ta>
            <ta e="T992" id="Seg_11739" s="T991">conj</ta>
            <ta e="T993" id="Seg_11740" s="T992">pers</ta>
            <ta e="T994" id="Seg_11741" s="T993">n</ta>
            <ta e="T995" id="Seg_11742" s="T994">v</ta>
            <ta e="T997" id="Seg_11743" s="T996">v</ta>
            <ta e="T998" id="Seg_11744" s="T997">n</ta>
            <ta e="T1002" id="Seg_11745" s="T1001">adv</ta>
            <ta e="T1003" id="Seg_11746" s="T1002">n</ta>
            <ta e="T1004" id="Seg_11747" s="T1003">v</ta>
            <ta e="T1005" id="Seg_11748" s="T1004">n</ta>
            <ta e="T1006" id="Seg_11749" s="T1005">dempro</ta>
            <ta e="T1007" id="Seg_11750" s="T1006">ptcl</ta>
            <ta e="T1008" id="Seg_11751" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_11752" s="T1008">v</ta>
            <ta e="T1013" id="Seg_11753" s="T1012">conj</ta>
            <ta e="T1014" id="Seg_11754" s="T1013">v</ta>
            <ta e="T1015" id="Seg_11755" s="T1014">v</ta>
            <ta e="T1017" id="Seg_11756" s="T1016">v</ta>
            <ta e="T1018" id="Seg_11757" s="T1017">adv</ta>
            <ta e="T1019" id="Seg_11758" s="T1018">n</ta>
            <ta e="T1020" id="Seg_11759" s="T1019">v</ta>
            <ta e="T1021" id="Seg_11760" s="T1020">conj</ta>
            <ta e="T1022" id="Seg_11761" s="T1021">pers</ta>
            <ta e="T1027" id="Seg_11762" s="T1026">conj</ta>
            <ta e="T1028" id="Seg_11763" s="T1027">n</ta>
            <ta e="T1029" id="Seg_11764" s="T1028">pers</ta>
            <ta e="T1030" id="Seg_11765" s="T1029">n</ta>
            <ta e="T1035" id="Seg_11766" s="T1034">adv</ta>
            <ta e="T1036" id="Seg_11767" s="T1035">dempro</ta>
            <ta e="T1037" id="Seg_11768" s="T1036">v</ta>
            <ta e="T1038" id="Seg_11769" s="T1037">n</ta>
            <ta e="T1040" id="Seg_11770" s="T1039">n</ta>
            <ta e="T1041" id="Seg_11771" s="T1040">n</ta>
            <ta e="T1042" id="Seg_11772" s="T1041">v</ta>
            <ta e="T1043" id="Seg_11773" s="T1042">adv</ta>
            <ta e="T1044" id="Seg_11774" s="T1043">n</ta>
            <ta e="T1045" id="Seg_11775" s="T1044">v</ta>
            <ta e="T1047" id="Seg_11776" s="T1046">que</ta>
            <ta e="T1048" id="Seg_11777" s="T1047">pers</ta>
            <ta e="T1049" id="Seg_11778" s="T1048">adv</ta>
            <ta e="T1050" id="Seg_11779" s="T1049">n</ta>
            <ta e="T1051" id="Seg_11780" s="T1050">conj</ta>
            <ta e="T1052" id="Seg_11781" s="T1051">pers</ta>
            <ta e="T1053" id="Seg_11782" s="T1052">n</ta>
            <ta e="T1055" id="Seg_11783" s="T1054">v</ta>
            <ta e="T1056" id="Seg_11784" s="T1055">conj</ta>
            <ta e="T1057" id="Seg_11785" s="T1056">pers</ta>
            <ta e="T1059" id="Seg_11786" s="T1058">conj</ta>
            <ta e="T1060" id="Seg_11787" s="T1059">pers</ta>
            <ta e="T1061" id="Seg_11788" s="T1060">v</ta>
            <ta e="T1062" id="Seg_11789" s="T1061">n</ta>
            <ta e="T1064" id="Seg_11790" s="T1063">conj</ta>
            <ta e="T1065" id="Seg_11791" s="T1064">pers</ta>
            <ta e="T1066" id="Seg_11792" s="T1065">n</ta>
            <ta e="T1067" id="Seg_11793" s="T1066">n</ta>
            <ta e="T1068" id="Seg_11794" s="T1067">v</ta>
            <ta e="T1070" id="Seg_11795" s="T1069">v</ta>
            <ta e="T1071" id="Seg_11796" s="T1070">n</ta>
            <ta e="T1072" id="Seg_11797" s="T1071">ptcl</ta>
            <ta e="T1073" id="Seg_11798" s="T1072">v</ta>
            <ta e="T1074" id="Seg_11799" s="T1073">pers</ta>
            <ta e="T1078" id="Seg_11800" s="T1077">adv</ta>
            <ta e="T1079" id="Seg_11801" s="T1078">n</ta>
            <ta e="T1080" id="Seg_11802" s="T1079">v</ta>
            <ta e="T1081" id="Seg_11803" s="T1080">n</ta>
            <ta e="T1083" id="Seg_11804" s="T1082">n</ta>
            <ta e="T1084" id="Seg_11805" s="T1083">n</ta>
            <ta e="T1085" id="Seg_11806" s="T1084">v</ta>
            <ta e="T1086" id="Seg_11807" s="T1085">adv</ta>
            <ta e="T1087" id="Seg_11808" s="T1086">n</ta>
            <ta e="T1088" id="Seg_11809" s="T1087">v</ta>
            <ta e="T1089" id="Seg_11810" s="T1088">que</ta>
            <ta e="T1090" id="Seg_11811" s="T1089">pers</ta>
            <ta e="T1091" id="Seg_11812" s="T1090">adv</ta>
            <ta e="T1093" id="Seg_11813" s="T1092">conj</ta>
            <ta e="T1094" id="Seg_11814" s="T1093">pers</ta>
            <ta e="T1095" id="Seg_11815" s="T1094">n</ta>
            <ta e="T1096" id="Seg_11816" s="T1095">v</ta>
            <ta e="T1097" id="Seg_11817" s="T1096">conj</ta>
            <ta e="T1098" id="Seg_11818" s="T1097">pers</ta>
            <ta e="T1099" id="Seg_11819" s="T1098">v</ta>
            <ta e="T1103" id="Seg_11820" s="T1102">conj</ta>
            <ta e="T1104" id="Seg_11821" s="T1103">n</ta>
            <ta e="T1105" id="Seg_11822" s="T1104">conj</ta>
            <ta e="T1106" id="Seg_11823" s="T1105">pers</ta>
            <ta e="T1107" id="Seg_11824" s="T1106">v</ta>
            <ta e="T1108" id="Seg_11825" s="T1107">ptcl</ta>
            <ta e="T1109" id="Seg_11826" s="T1108">que</ta>
            <ta e="T1111" id="Seg_11827" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_11828" s="T1111">n</ta>
            <ta e="T1113" id="Seg_11829" s="T1112">v</ta>
            <ta e="T1114" id="Seg_11830" s="T1113">n</ta>
            <ta e="T1115" id="Seg_11831" s="T1114">que</ta>
            <ta e="T1117" id="Seg_11832" s="T1116">que</ta>
            <ta e="T1119" id="Seg_11833" s="T1118">n</ta>
            <ta e="T1120" id="Seg_11834" s="T1119">ptcl</ta>
            <ta e="T1121" id="Seg_11835" s="T1120">v</ta>
            <ta e="T1123" id="Seg_11836" s="T1122">dempro</ta>
            <ta e="T1124" id="Seg_11837" s="T1123">v</ta>
            <ta e="T1125" id="Seg_11838" s="T1124">n</ta>
            <ta e="T1127" id="Seg_11839" s="T1126">n</ta>
            <ta e="T1128" id="Seg_11840" s="T1127">v</ta>
            <ta e="T1129" id="Seg_11841" s="T1128">v</ta>
            <ta e="T1130" id="Seg_11842" s="T1129">conj</ta>
            <ta e="T1131" id="Seg_11843" s="T1130">n</ta>
            <ta e="T1132" id="Seg_11844" s="T1131">n</ta>
            <ta e="T1133" id="Seg_11845" s="T1132">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T8" id="Seg_11846" s="T7">np.h:Th</ta>
            <ta e="T11" id="Seg_11847" s="T10">np.h:Th</ta>
            <ta e="T13" id="Seg_11848" s="T12">adv:Time</ta>
            <ta e="T16" id="Seg_11849" s="T15">np:Th</ta>
            <ta e="T18" id="Seg_11850" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_11851" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_11852" s="T21">adv:Time</ta>
            <ta e="T23" id="Seg_11853" s="T22">np:Th</ta>
            <ta e="T27" id="Seg_11854" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_11855" s="T27">np.h:A</ta>
            <ta e="T34" id="Seg_11856" s="T33">adv:Time</ta>
            <ta e="T39" id="Seg_11857" s="T38">adv:L</ta>
            <ta e="T40" id="Seg_11858" s="T39">np:L</ta>
            <ta e="T43" id="Seg_11859" s="T42">np:P</ta>
            <ta e="T44" id="Seg_11860" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_11861" s="T44">adv:Time</ta>
            <ta e="T46" id="Seg_11862" s="T45">0.3.h:A</ta>
            <ta e="T49" id="Seg_11863" s="T48">0.3.h:A</ta>
            <ta e="T52" id="Seg_11864" s="T51">np:G</ta>
            <ta e="T53" id="Seg_11865" s="T52">0.3.h:A</ta>
            <ta e="T55" id="Seg_11866" s="T54">pro.h:P</ta>
            <ta e="T59" id="Seg_11867" s="T58">adv:Time</ta>
            <ta e="T60" id="Seg_11868" s="T59">pro.h:Poss</ta>
            <ta e="T61" id="Seg_11869" s="T60">np:P</ta>
            <ta e="T64" id="Seg_11870" s="T63">adv:Time</ta>
            <ta e="T66" id="Seg_11871" s="T65">np.h:A</ta>
            <ta e="T68" id="Seg_11872" s="T67">np:Th</ta>
            <ta e="T69" id="Seg_11873" s="T68">np:Th</ta>
            <ta e="T71" id="Seg_11874" s="T70">np:Th</ta>
            <ta e="T73" id="Seg_11875" s="T72">np:G</ta>
            <ta e="T75" id="Seg_11876" s="T1134">0.3.h:A</ta>
            <ta e="T77" id="Seg_11877" s="T76">adv:Time</ta>
            <ta e="T79" id="Seg_11878" s="T78">np:Th</ta>
            <ta e="T81" id="Seg_11879" s="T80">np.h:A</ta>
            <ta e="T83" id="Seg_11880" s="T82">np.h:A</ta>
            <ta e="T85" id="Seg_11881" s="T84">np.h:A</ta>
            <ta e="T90" id="Seg_11882" s="T89">pro.h:A</ta>
            <ta e="T92" id="Seg_11883" s="T91">np:G</ta>
            <ta e="T93" id="Seg_11884" s="T92">np:Th</ta>
            <ta e="T95" id="Seg_11885" s="T94">np.h:A</ta>
            <ta e="T97" id="Seg_11886" s="T96">np:Th</ta>
            <ta e="T99" id="Seg_11887" s="T98">adv:Time</ta>
            <ta e="T100" id="Seg_11888" s="T99">np.h:A</ta>
            <ta e="T102" id="Seg_11889" s="T101">np.h:A</ta>
            <ta e="T108" id="Seg_11890" s="T107">np.h:A</ta>
            <ta e="T111" id="Seg_11891" s="T110">np:G</ta>
            <ta e="T112" id="Seg_11892" s="T111">0.3.h:E</ta>
            <ta e="T114" id="Seg_11893" s="T113">0.3.h:P</ta>
            <ta e="T117" id="Seg_11894" s="T116">np.h:A</ta>
            <ta e="T119" id="Seg_11895" s="T118">0.3.h:A</ta>
            <ta e="T121" id="Seg_11896" s="T120">0.3.h:P</ta>
            <ta e="T123" id="Seg_11897" s="T122">np.h:Th</ta>
            <ta e="T127" id="Seg_11898" s="T126">adv:Time</ta>
            <ta e="T128" id="Seg_11899" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_11900" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_11901" s="T129">np.h:A</ta>
            <ta e="T134" id="Seg_11902" s="T133">np:Th</ta>
            <ta e="T136" id="Seg_11903" s="T135">np:Th</ta>
            <ta e="T137" id="Seg_11904" s="T136">np:L</ta>
            <ta e="T141" id="Seg_11905" s="T140">np.h:A</ta>
            <ta e="T143" id="Seg_11906" s="T142">np.h:Poss</ta>
            <ta e="T144" id="Seg_11907" s="T143">0.2.h:A</ta>
            <ta e="T145" id="Seg_11908" s="T144">np:G</ta>
            <ta e="T148" id="Seg_11909" s="T147">0.2.h:A</ta>
            <ta e="T149" id="Seg_11910" s="T148">np.h:R</ta>
            <ta e="T151" id="Seg_11911" s="T150">pro.h:A</ta>
            <ta e="T155" id="Seg_11912" s="T154">np.h:A 0.2.h:Poss</ta>
            <ta e="T159" id="Seg_11913" s="T158">0.2.h:A</ta>
            <ta e="T160" id="Seg_11914" s="T159">0.2.h:A</ta>
            <ta e="T161" id="Seg_11915" s="T160">pro.h:A</ta>
            <ta e="T162" id="Seg_11916" s="T161">adv:Time</ta>
            <ta e="T163" id="Seg_11917" s="T162">np:P</ta>
            <ta e="T166" id="Seg_11918" s="T165">pro.h:A</ta>
            <ta e="T168" id="Seg_11919" s="T167">0.2.h:A</ta>
            <ta e="T169" id="Seg_11920" s="T168">np.h:A</ta>
            <ta e="T170" id="Seg_11921" s="T169">np:P</ta>
            <ta e="T176" id="Seg_11922" s="T175">pro.h:E</ta>
            <ta e="T177" id="Seg_11923" s="T176">pro:P</ta>
            <ta e="T183" id="Seg_11924" s="T182">np:P</ta>
            <ta e="T184" id="Seg_11925" s="T183">0.3.h:A</ta>
            <ta e="T186" id="Seg_11926" s="T185">0.3.h:A</ta>
            <ta e="T187" id="Seg_11927" s="T186">np:G</ta>
            <ta e="T188" id="Seg_11928" s="T187">0.3.h:A</ta>
            <ta e="T189" id="Seg_11929" s="T188">pro.h:A</ta>
            <ta e="T190" id="Seg_11930" s="T189">np:P</ta>
            <ta e="T193" id="Seg_11931" s="T192">np:P</ta>
            <ta e="T194" id="Seg_11932" s="T193">0.3.h:A</ta>
            <ta e="T195" id="Seg_11933" s="T194">np:G</ta>
            <ta e="T196" id="Seg_11934" s="T195">0.3.h:A</ta>
            <ta e="T198" id="Seg_11935" s="T197">adv:Time</ta>
            <ta e="T199" id="Seg_11936" s="T198">np.h:A</ta>
            <ta e="T201" id="Seg_11937" s="T200">0.2.h:A</ta>
            <ta e="T202" id="Seg_11938" s="T201">np:P</ta>
            <ta e="T205" id="Seg_11939" s="T204">0.3.h:A</ta>
            <ta e="T207" id="Seg_11940" s="T206">pro:L</ta>
            <ta e="T208" id="Seg_11941" s="T207">np:Th 0.1.h:Poss</ta>
            <ta e="T211" id="Seg_11942" s="T210">pro.h:A</ta>
            <ta e="T213" id="Seg_11943" s="T212">np.h:E</ta>
            <ta e="T216" id="Seg_11944" s="T215">np.h:Th</ta>
            <ta e="T218" id="Seg_11945" s="T217">0.2.h:A</ta>
            <ta e="T220" id="Seg_11946" s="T219">np.h:Th</ta>
            <ta e="T221" id="Seg_11947" s="T220">adv:L</ta>
            <ta e="T222" id="Seg_11948" s="T221">0.1.h:A</ta>
            <ta e="T224" id="Seg_11949" s="T223">adv:Time</ta>
            <ta e="T226" id="Seg_11950" s="T225">np:Th</ta>
            <ta e="T230" id="Seg_11951" s="T229">np:Th</ta>
            <ta e="T240" id="Seg_11952" s="T239">np:Th</ta>
            <ta e="T241" id="Seg_11953" s="T240">pro.h:B</ta>
            <ta e="T246" id="Seg_11954" s="T245">np:Th</ta>
            <ta e="T252" id="Seg_11955" s="T251">np.h:E</ta>
            <ta e="T257" id="Seg_11956" s="T256">np:P</ta>
            <ta e="T258" id="Seg_11957" s="T257">0.1.h:A</ta>
            <ta e="T259" id="Seg_11958" s="T258">np:Ins</ta>
            <ta e="T261" id="Seg_11959" s="T260">adv:Time</ta>
            <ta e="T263" id="Seg_11960" s="T262">np:P</ta>
            <ta e="T264" id="Seg_11961" s="T263">pro.h:A</ta>
            <ta e="T265" id="Seg_11962" s="T264">pro:Th</ta>
            <ta e="T267" id="Seg_11963" s="T266">np:G</ta>
            <ta e="T268" id="Seg_11964" s="T267">adv:Time</ta>
            <ta e="T269" id="Seg_11965" s="T268">np:Th</ta>
            <ta e="T270" id="Seg_11966" s="T269">0.1.h:A</ta>
            <ta e="T271" id="Seg_11967" s="T270">0.1.h:A</ta>
            <ta e="T273" id="Seg_11968" s="T272">0.1.h:A</ta>
            <ta e="T274" id="Seg_11969" s="T273">np:G</ta>
            <ta e="T279" id="Seg_11970" s="T278">n:Time</ta>
            <ta e="T281" id="Seg_11971" s="T280">np:Th</ta>
            <ta e="T285" id="Seg_11972" s="T284">np:Th</ta>
            <ta e="T288" id="Seg_11973" s="T287">np.h:A</ta>
            <ta e="T291" id="Seg_11974" s="T290">np:G</ta>
            <ta e="T292" id="Seg_11975" s="T291">np:Th</ta>
            <ta e="T294" id="Seg_11976" s="T293">np:Th</ta>
            <ta e="T299" id="Seg_11977" s="T298">np.h:A</ta>
            <ta e="T303" id="Seg_11978" s="T302">np:Th</ta>
            <ta e="T311" id="Seg_11979" s="T310">np:Th</ta>
            <ta e="T319" id="Seg_11980" s="T318">pro:Th</ta>
            <ta e="T323" id="Seg_11981" s="T322">np.h:A</ta>
            <ta e="T329" id="Seg_11982" s="T328">n:Time</ta>
            <ta e="T331" id="Seg_11983" s="T330">np:Th</ta>
            <ta e="T338" id="Seg_11984" s="T337">n:Time</ta>
            <ta e="T340" id="Seg_11985" s="T339">np:Th</ta>
            <ta e="T345" id="Seg_11986" s="T344">np:Th</ta>
            <ta e="T348" id="Seg_11987" s="T347">np:P</ta>
            <ta e="T351" id="Seg_11988" s="T350">0.3:Th</ta>
            <ta e="T356" id="Seg_11989" s="T355">np:P</ta>
            <ta e="T359" id="Seg_11990" s="T358">np.h:A</ta>
            <ta e="T362" id="Seg_11991" s="T361">np:P</ta>
            <ta e="T365" id="Seg_11992" s="T364">np:L</ta>
            <ta e="T372" id="Seg_11993" s="T371">np:L</ta>
            <ta e="T373" id="Seg_11994" s="T372">np:Th</ta>
            <ta e="T379" id="Seg_11995" s="T378">pro.h:A</ta>
            <ta e="T383" id="Seg_11996" s="T382">0.1.h:A</ta>
            <ta e="T385" id="Seg_11997" s="T384">0.3.h:P</ta>
            <ta e="T388" id="Seg_11998" s="T387">adv:Time</ta>
            <ta e="T389" id="Seg_11999" s="T388">0.1.h:A</ta>
            <ta e="T390" id="Seg_12000" s="T389">np:Ins</ta>
            <ta e="T391" id="Seg_12001" s="T390">np:Ins</ta>
            <ta e="T398" id="Seg_12002" s="T397">np:L</ta>
            <ta e="T400" id="Seg_12003" s="T399">np.h:P</ta>
            <ta e="T402" id="Seg_12004" s="T401">pro.h:A</ta>
            <ta e="T405" id="Seg_12005" s="T404">np:P</ta>
            <ta e="T406" id="Seg_12006" s="T405">adv:Time</ta>
            <ta e="T407" id="Seg_12007" s="T406">0.3.h:A</ta>
            <ta e="T411" id="Seg_12008" s="T410">adv:Time</ta>
            <ta e="T412" id="Seg_12009" s="T411">np:P</ta>
            <ta e="T413" id="Seg_12010" s="T412">0.3.h:A</ta>
            <ta e="T414" id="Seg_12011" s="T413">np:P</ta>
            <ta e="T415" id="Seg_12012" s="T414">0.3.h:A</ta>
            <ta e="T416" id="Seg_12013" s="T415">np:G</ta>
            <ta e="T417" id="Seg_12014" s="T416">pro.h:Th</ta>
            <ta e="T418" id="Seg_12015" s="T417">0.3.h:A</ta>
            <ta e="T422" id="Seg_12016" s="T421">pro.h:A</ta>
            <ta e="T423" id="Seg_12017" s="T422">pro:G</ta>
            <ta e="T424" id="Seg_12018" s="T423">np:Th</ta>
            <ta e="T426" id="Seg_12019" s="T425">np:Th</ta>
            <ta e="T427" id="Seg_12020" s="T426">0.3.h:A</ta>
            <ta e="T429" id="Seg_12021" s="T428">np:Th</ta>
            <ta e="T430" id="Seg_12022" s="T429">0.3.h:A</ta>
            <ta e="T433" id="Seg_12023" s="T432">np:Th</ta>
            <ta e="T435" id="Seg_12024" s="T434">0.3.h:A</ta>
            <ta e="T437" id="Seg_12025" s="T436">pro:A</ta>
            <ta e="T438" id="Seg_12026" s="T437">adv:L</ta>
            <ta e="T441" id="Seg_12027" s="T440">0.3:A</ta>
            <ta e="T445" id="Seg_12028" s="T444">adv:Time</ta>
            <ta e="T446" id="Seg_12029" s="T445">pro.h:A</ta>
            <ta e="T447" id="Seg_12030" s="T446">np.h:Th</ta>
            <ta e="T450" id="Seg_12031" s="T449">np:G</ta>
            <ta e="T451" id="Seg_12032" s="T450">0.3.h:A</ta>
            <ta e="T452" id="Seg_12033" s="T451">np:Ins</ta>
            <ta e="T454" id="Seg_12034" s="T453">0.3.h:A</ta>
            <ta e="T456" id="Seg_12035" s="T455">adv:Time</ta>
            <ta e="T457" id="Seg_12036" s="T456">np:G</ta>
            <ta e="T458" id="Seg_12037" s="T457">0.3.h:A</ta>
            <ta e="T460" id="Seg_12038" s="T459">n:Time</ta>
            <ta e="T462" id="Seg_12039" s="T461">np:Th</ta>
            <ta e="T463" id="Seg_12040" s="T462">0.3.h:A</ta>
            <ta e="T465" id="Seg_12041" s="T464">np:Th</ta>
            <ta e="T467" id="Seg_12042" s="T466">adv:Time</ta>
            <ta e="T468" id="Seg_12043" s="T467">0.3.h:A</ta>
            <ta e="T471" id="Seg_12044" s="T470">0.3.h:A</ta>
            <ta e="T472" id="Seg_12045" s="T471">0.3.h:A</ta>
            <ta e="T473" id="Seg_12046" s="T472">np:G</ta>
            <ta e="T476" id="Seg_12047" s="T475">np:Th</ta>
            <ta e="T477" id="Seg_12048" s="T476">np:L</ta>
            <ta e="T478" id="Seg_12049" s="T477">0.3.h:A</ta>
            <ta e="T482" id="Seg_12050" s="T481">np:Th</ta>
            <ta e="T483" id="Seg_12051" s="T482">0.3.h:A</ta>
            <ta e="T486" id="Seg_12052" s="T485">np:Th 0.3:Poss</ta>
            <ta e="T496" id="Seg_12053" s="T495">np:Th</ta>
            <ta e="T497" id="Seg_12054" s="T496">0.3.h:A</ta>
            <ta e="T499" id="Seg_12055" s="T498">np:E</ta>
            <ta e="T507" id="Seg_12056" s="T506">np.h:E</ta>
            <ta e="T509" id="Seg_12057" s="T508">pro:L</ta>
            <ta e="T511" id="Seg_12058" s="T510">0.3.h:E</ta>
            <ta e="T512" id="Seg_12059" s="T511">adv:Time</ta>
            <ta e="T514" id="Seg_12060" s="T513">0.3.h:P</ta>
            <ta e="T517" id="Seg_12061" s="T516">pro.h:A</ta>
            <ta e="T520" id="Seg_12062" s="T519">np:G</ta>
            <ta e="T522" id="Seg_12063" s="T521">np:Th</ta>
            <ta e="T525" id="Seg_12064" s="T524">np:G</ta>
            <ta e="T528" id="Seg_12065" s="T527">np:G</ta>
            <ta e="T530" id="Seg_12066" s="T529">adv:Time</ta>
            <ta e="T531" id="Seg_12067" s="T530">np.h:A</ta>
            <ta e="T532" id="Seg_12068" s="T531">pro.h:R</ta>
            <ta e="T536" id="Seg_12069" s="T535">pro.h:A</ta>
            <ta e="T537" id="Seg_12070" s="T536">np:Th</ta>
            <ta e="T541" id="Seg_12071" s="T540">np:G</ta>
            <ta e="T542" id="Seg_12072" s="T541">0.1.h:A</ta>
            <ta e="T544" id="Seg_12073" s="T543">adv:Time</ta>
            <ta e="T545" id="Seg_12074" s="T544">np.h:A</ta>
            <ta e="T546" id="Seg_12075" s="T545">np.h:A</ta>
            <ta e="T550" id="Seg_12076" s="T549">np:Th</ta>
            <ta e="T551" id="Seg_12077" s="T1137">0.3.h:A</ta>
            <ta e="T555" id="Seg_12078" s="T554">pro.h:Th</ta>
            <ta e="T561" id="Seg_12079" s="T560">np:L</ta>
            <ta e="T564" id="Seg_12080" s="T563">0.1.h:A</ta>
            <ta e="T567" id="Seg_12081" s="T566">pro.h:A</ta>
            <ta e="T569" id="Seg_12082" s="T568">0.2.h:A</ta>
            <ta e="T570" id="Seg_12083" s="T569">0.2.h:E</ta>
            <ta e="T573" id="Seg_12084" s="T572">pro.h:A</ta>
            <ta e="T575" id="Seg_12085" s="T574">pro.h:Com</ta>
            <ta e="T581" id="Seg_12086" s="T580">pro.h:A</ta>
            <ta e="T584" id="Seg_12087" s="T583">adv:Time</ta>
            <ta e="T585" id="Seg_12088" s="T584">n:Time</ta>
            <ta e="T586" id="Seg_12089" s="T585">0.1.h:A</ta>
            <ta e="T589" id="Seg_12090" s="T588">0.2.h:E</ta>
            <ta e="T590" id="Seg_12091" s="T589">pro.h:Th</ta>
            <ta e="T595" id="Seg_12092" s="T594">pro.h:E</ta>
            <ta e="T600" id="Seg_12093" s="T599">adv:Time</ta>
            <ta e="T601" id="Seg_12094" s="T600">pro.h:P</ta>
            <ta e="T603" id="Seg_12095" s="T602">pro.h:P</ta>
            <ta e="T604" id="Seg_12096" s="T603">0.3.h:A</ta>
            <ta e="T606" id="Seg_12097" s="T605">np:Th</ta>
            <ta e="T607" id="Seg_12098" s="T606">0.3.h:A</ta>
            <ta e="T608" id="Seg_12099" s="T607">np:Th</ta>
            <ta e="T609" id="Seg_12100" s="T608">0.3.h:A</ta>
            <ta e="T611" id="Seg_12101" s="T610">np:Th</ta>
            <ta e="T613" id="Seg_12102" s="T612">0.3.h:A</ta>
            <ta e="T615" id="Seg_12103" s="T614">adv:Time</ta>
            <ta e="T616" id="Seg_12104" s="T615">0.3.h:A</ta>
            <ta e="T619" id="Seg_12105" s="T618">np:Ins</ta>
            <ta e="T620" id="Seg_12106" s="T619">0.3.h:A</ta>
            <ta e="T621" id="Seg_12107" s="T620">pro:Th</ta>
            <ta e="T623" id="Seg_12108" s="T622">0.3.h:A</ta>
            <ta e="T624" id="Seg_12109" s="T623">np:G</ta>
            <ta e="T626" id="Seg_12110" s="T625">np:P</ta>
            <ta e="T627" id="Seg_12111" s="T626">0.3.h:A</ta>
            <ta e="T628" id="Seg_12112" s="T627">np:P</ta>
            <ta e="T629" id="Seg_12113" s="T628">0.3.h:A</ta>
            <ta e="T630" id="Seg_12114" s="T629">np:P</ta>
            <ta e="T631" id="Seg_12115" s="T630">0.3.h:A</ta>
            <ta e="T634" id="Seg_12116" s="T633">np:G</ta>
            <ta e="T635" id="Seg_12117" s="T1138">0.3.h:A</ta>
            <ta e="T639" id="Seg_12118" s="T638">pro.h:A</ta>
            <ta e="T644" id="Seg_12119" s="T643">np:L</ta>
            <ta e="T646" id="Seg_12120" s="T645">0.1.h:A</ta>
            <ta e="T647" id="Seg_12121" s="T646">np:G</ta>
            <ta e="T648" id="Seg_12122" s="T647">adv:Time</ta>
            <ta e="T649" id="Seg_12123" s="T648">np:G</ta>
            <ta e="T650" id="Seg_12124" s="T649">0.1.h:A</ta>
            <ta e="T652" id="Seg_12125" s="T651">np:L</ta>
            <ta e="T653" id="Seg_12126" s="T652">adv:L</ta>
            <ta e="T660" id="Seg_12127" s="T659">pro.h:A</ta>
            <ta e="T664" id="Seg_12128" s="T663">adv:Time</ta>
            <ta e="T673" id="Seg_12129" s="T672">0.1.h:A</ta>
            <ta e="T675" id="Seg_12130" s="T674">np:G</ta>
            <ta e="T676" id="Seg_12131" s="T675">pro.h:Th</ta>
            <ta e="T678" id="Seg_12132" s="T677">0.1.h:E</ta>
            <ta e="T682" id="Seg_12133" s="T681">pro.h:Poss</ta>
            <ta e="T683" id="Seg_12134" s="T682">np:E</ta>
            <ta e="T685" id="Seg_12135" s="T684">np:E</ta>
            <ta e="T688" id="Seg_12136" s="T687">pro.h:Poss</ta>
            <ta e="T689" id="Seg_12137" s="T688">pro:E</ta>
            <ta e="T692" id="Seg_12138" s="T691">pro.h:Poss</ta>
            <ta e="T693" id="Seg_12139" s="T692">np:E</ta>
            <ta e="T697" id="Seg_12140" s="T696">np:E</ta>
            <ta e="T702" id="Seg_12141" s="T701">np.h:Poss</ta>
            <ta e="T704" id="Seg_12142" s="T703">np:E</ta>
            <ta e="T707" id="Seg_12143" s="T706">np:E</ta>
            <ta e="T715" id="Seg_12144" s="T714">0.3.h:E</ta>
            <ta e="T726" id="Seg_12145" s="T725">np.h:P</ta>
            <ta e="T728" id="Seg_12146" s="T727">np.h:Th</ta>
            <ta e="T733" id="Seg_12147" s="T732">pro.h:A</ta>
            <ta e="T734" id="Seg_12148" s="T733">pro.h:B</ta>
            <ta e="T738" id="Seg_12149" s="T737">pro:Th</ta>
            <ta e="T739" id="Seg_12150" s="T738">0.3.h:A</ta>
            <ta e="T742" id="Seg_12151" s="T741">pro:Th</ta>
            <ta e="T743" id="Seg_12152" s="T742">0.3.h:A</ta>
            <ta e="T748" id="Seg_12153" s="T747">pro.h:E</ta>
            <ta e="T753" id="Seg_12154" s="T752">adv:Time</ta>
            <ta e="T756" id="Seg_12155" s="T755">0.3.h:E</ta>
            <ta e="T761" id="Seg_12156" s="T760">np.h:Poss</ta>
            <ta e="T764" id="Seg_12157" s="T763">np.h:Poss</ta>
            <ta e="T766" id="Seg_12158" s="T765">np:Th</ta>
            <ta e="T768" id="Seg_12159" s="T767">np:Th</ta>
            <ta e="T775" id="Seg_12160" s="T774">np.h:E</ta>
            <ta e="T777" id="Seg_12161" s="T776">np.h:E</ta>
            <ta e="T780" id="Seg_12162" s="T779">np:L</ta>
            <ta e="T781" id="Seg_12163" s="T780">np:L</ta>
            <ta e="T783" id="Seg_12164" s="T782">np.h:A</ta>
            <ta e="T788" id="Seg_12165" s="T787">np.h:A</ta>
            <ta e="T790" id="Seg_12166" s="T789">np:G</ta>
            <ta e="T791" id="Seg_12167" s="T790">np:P</ta>
            <ta e="T799" id="Seg_12168" s="T798">0.3.h:A</ta>
            <ta e="T800" id="Seg_12169" s="T799">adv:Time</ta>
            <ta e="T801" id="Seg_12170" s="T800">pro:Th</ta>
            <ta e="T803" id="Seg_12171" s="T802">0.3.h:A</ta>
            <ta e="T805" id="Seg_12172" s="T804">np.h:Th</ta>
            <ta e="T806" id="Seg_12173" s="T805">0.3.h:A</ta>
            <ta e="T809" id="Seg_12174" s="T808">pro.h:A</ta>
            <ta e="T814" id="Seg_12175" s="T813">np.h:Th</ta>
            <ta e="T816" id="Seg_12176" s="T815">0.3.h:A</ta>
            <ta e="T817" id="Seg_12177" s="T816">pro.h:R</ta>
            <ta e="T819" id="Seg_12178" s="T818">np:Ins</ta>
            <ta e="T822" id="Seg_12179" s="T821">pro.h:Th</ta>
            <ta e="T823" id="Seg_12180" s="T822">np:G</ta>
            <ta e="T825" id="Seg_12181" s="T824">pro.h:A</ta>
            <ta e="T828" id="Seg_12182" s="T827">0.3.h:A</ta>
            <ta e="T829" id="Seg_12183" s="T828">np:G</ta>
            <ta e="T831" id="Seg_12184" s="T830">np.h:R</ta>
            <ta e="T832" id="Seg_12185" s="T831">0.3.h:A</ta>
            <ta e="T834" id="Seg_12186" s="T833">np.h:Th</ta>
            <ta e="T838" id="Seg_12187" s="T837">0.3.h:A</ta>
            <ta e="T844" id="Seg_12188" s="T843">adv:Time</ta>
            <ta e="T846" id="Seg_12189" s="T845">np.h:A</ta>
            <ta e="T851" id="Seg_12190" s="T850">pro.h:A</ta>
            <ta e="T852" id="Seg_12191" s="T851">pro:Th</ta>
            <ta e="T856" id="Seg_12192" s="T855">np:Th</ta>
            <ta e="T857" id="Seg_12193" s="T856">pro.h:B</ta>
            <ta e="T864" id="Seg_12194" s="T863">pro.h:A</ta>
            <ta e="T867" id="Seg_12195" s="T866">np:G</ta>
            <ta e="T869" id="Seg_12196" s="T868">adv:Time</ta>
            <ta e="T873" id="Seg_12197" s="T872">0.2.h:A</ta>
            <ta e="T874" id="Seg_12198" s="T873">adv:G</ta>
            <ta e="T875" id="Seg_12199" s="T874">pro.h:A</ta>
            <ta e="T877" id="Seg_12200" s="T876">pro:Th</ta>
            <ta e="T878" id="Seg_12201" s="T877">pro.h:B</ta>
            <ta e="T882" id="Seg_12202" s="T881">pro.h:B</ta>
            <ta e="T884" id="Seg_12203" s="T883">np:Th</ta>
            <ta e="T886" id="Seg_12204" s="T885">np.h:A</ta>
            <ta e="T890" id="Seg_12205" s="T889">0.2.h:A</ta>
            <ta e="T891" id="Seg_12206" s="T890">np:G</ta>
            <ta e="T896" id="Seg_12207" s="T895">np.h:A</ta>
            <ta e="T898" id="Seg_12208" s="T897">pro.h:R</ta>
            <ta e="T902" id="Seg_12209" s="T901">0.2.h:A</ta>
            <ta e="T903" id="Seg_12210" s="T902">np:Th</ta>
            <ta e="T905" id="Seg_12211" s="T904">pro.h:Poss</ta>
            <ta e="T906" id="Seg_12212" s="T905">np:Th</ta>
            <ta e="T910" id="Seg_12213" s="T909">pro.h:A</ta>
            <ta e="T917" id="Seg_12214" s="T916">0.2.h:A</ta>
            <ta e="T918" id="Seg_12215" s="T917">adv:G</ta>
            <ta e="T920" id="Seg_12216" s="T919">np.h:A</ta>
            <ta e="T922" id="Seg_12217" s="T921">pro:Th</ta>
            <ta e="T923" id="Seg_12218" s="T922">pro.h:B</ta>
            <ta e="T926" id="Seg_12219" s="T925">np:Th</ta>
            <ta e="T929" id="Seg_12220" s="T928">pro.h:Poss</ta>
            <ta e="T930" id="Seg_12221" s="T929">np:Th</ta>
            <ta e="T934" id="Seg_12222" s="T933">0.2.h:A</ta>
            <ta e="T935" id="Seg_12223" s="T934">np:G</ta>
            <ta e="T939" id="Seg_12224" s="T938">adv:Time</ta>
            <ta e="T942" id="Seg_12225" s="T941">np.h:A</ta>
            <ta e="T945" id="Seg_12226" s="T944">0.2.h:A</ta>
            <ta e="T946" id="Seg_12227" s="T945">np:G</ta>
            <ta e="T947" id="Seg_12228" s="T946">0.2.h:A</ta>
            <ta e="T949" id="Seg_12229" s="T948">pro.h:P</ta>
            <ta e="T951" id="Seg_12230" s="T950">np.h:Th</ta>
            <ta e="T955" id="Seg_12231" s="T954">adv:Time</ta>
            <ta e="T956" id="Seg_12232" s="T955">np.h:A</ta>
            <ta e="T958" id="Seg_12233" s="T957">0.2.h:A</ta>
            <ta e="T959" id="Seg_12234" s="T958">np:G</ta>
            <ta e="T961" id="Seg_12235" s="T960">pro.h:A</ta>
            <ta e="T965" id="Seg_12236" s="T964">np.h:A</ta>
            <ta e="T969" id="Seg_12237" s="T968">0.2.h:A</ta>
            <ta e="T970" id="Seg_12238" s="T969">np:G</ta>
            <ta e="T971" id="Seg_12239" s="T970">0.2.h:A</ta>
            <ta e="T973" id="Seg_12240" s="T972">pro.h:P</ta>
            <ta e="T977" id="Seg_12241" s="T976">pro.h:A</ta>
            <ta e="T979" id="Seg_12242" s="T978">np:G</ta>
            <ta e="T980" id="Seg_12243" s="T979">np.h:R</ta>
            <ta e="T981" id="Seg_12244" s="T980">0.3.h:A</ta>
            <ta e="T983" id="Seg_12245" s="T982">np.h:A</ta>
            <ta e="T985" id="Seg_12246" s="T984">pro:Th</ta>
            <ta e="T986" id="Seg_12247" s="T985">pro.h:B</ta>
            <ta e="T989" id="Seg_12248" s="T988">pro.h:Poss</ta>
            <ta e="T990" id="Seg_12249" s="T989">np.h:A</ta>
            <ta e="T993" id="Seg_12250" s="T992">pro.h:P</ta>
            <ta e="T994" id="Seg_12251" s="T993">np.h:Th</ta>
            <ta e="T997" id="Seg_12252" s="T996">0.2.h:A</ta>
            <ta e="T998" id="Seg_12253" s="T997">np:G</ta>
            <ta e="T1002" id="Seg_12254" s="T1001">adv:Time</ta>
            <ta e="T1003" id="Seg_12255" s="T1002">np.h:A</ta>
            <ta e="T1005" id="Seg_12256" s="T1004">np:G</ta>
            <ta e="T1006" id="Seg_12257" s="T1005">pro.h:A</ta>
            <ta e="T1017" id="Seg_12258" s="T1016">0.2.h:A</ta>
            <ta e="T1019" id="Seg_12259" s="T1018">np:G</ta>
            <ta e="T1020" id="Seg_12260" s="T1019">0.2.h:A</ta>
            <ta e="T1022" id="Seg_12261" s="T1021">pro.h:P</ta>
            <ta e="T1028" id="Seg_12262" s="T1027">np.h:A</ta>
            <ta e="T1029" id="Seg_12263" s="T1028">pro.h:Th</ta>
            <ta e="T1030" id="Seg_12264" s="T1029">np:L 0.3.h:Poss</ta>
            <ta e="T1035" id="Seg_12265" s="T1034">adv:Time</ta>
            <ta e="T1036" id="Seg_12266" s="T1035">pro.h:A</ta>
            <ta e="T1038" id="Seg_12267" s="T1037">np:G</ta>
            <ta e="T1042" id="Seg_12268" s="T1041">0.2.h:A</ta>
            <ta e="T1043" id="Seg_12269" s="T1042">adv:L</ta>
            <ta e="T1044" id="Seg_12270" s="T1043">np.h:A</ta>
            <ta e="T1047" id="Seg_12271" s="T1046">pro:Th</ta>
            <ta e="T1048" id="Seg_12272" s="T1047">pro.h:B</ta>
            <ta e="T1052" id="Seg_12273" s="T1051">pro.h:Poss</ta>
            <ta e="T1053" id="Seg_12274" s="T1052">np.h:A</ta>
            <ta e="T1060" id="Seg_12275" s="T1059">pro.h:P</ta>
            <ta e="T1066" id="Seg_12276" s="T1065">np.h:A</ta>
            <ta e="T1067" id="Seg_12277" s="T1066">np:Ins</ta>
            <ta e="T1070" id="Seg_12278" s="T1069">0.2.h:A</ta>
            <ta e="T1071" id="Seg_12279" s="T1070">np:G</ta>
            <ta e="T1073" id="Seg_12280" s="T1072">0.3:P</ta>
            <ta e="T1074" id="Seg_12281" s="T1073">pro.h:B</ta>
            <ta e="T1078" id="Seg_12282" s="T1077">adv:Time</ta>
            <ta e="T1079" id="Seg_12283" s="T1078">np.h:A</ta>
            <ta e="T1081" id="Seg_12284" s="T1080">np:G</ta>
            <ta e="T1085" id="Seg_12285" s="T1084">0.2.h:A</ta>
            <ta e="T1086" id="Seg_12286" s="T1085">adv:G</ta>
            <ta e="T1087" id="Seg_12287" s="T1086">np.h:A</ta>
            <ta e="T1089" id="Seg_12288" s="T1088">pro:Th</ta>
            <ta e="T1090" id="Seg_12289" s="T1089">pro.h:B</ta>
            <ta e="T1094" id="Seg_12290" s="T1093">pro.h:Poss</ta>
            <ta e="T1095" id="Seg_12291" s="T1094">np.h:A</ta>
            <ta e="T1098" id="Seg_12292" s="T1097">pro.h:P</ta>
            <ta e="T1104" id="Seg_12293" s="T1103">np.h:Th</ta>
            <ta e="T1106" id="Seg_12294" s="T1105">pro.h:B</ta>
            <ta e="T1109" id="Seg_12295" s="T1108">pro:Th</ta>
            <ta e="T1111" id="Seg_12296" s="T1110">adv:Time</ta>
            <ta e="T1112" id="Seg_12297" s="T1111">np.h:A</ta>
            <ta e="T1114" id="Seg_12298" s="T1113">np:G</ta>
            <ta e="T1117" id="Seg_12299" s="T1116">pro:Th</ta>
            <ta e="T1119" id="Seg_12300" s="T1118">np.h:R</ta>
            <ta e="T1121" id="Seg_12301" s="T1120">0.3.h:A</ta>
            <ta e="T1123" id="Seg_12302" s="T1122">pro.h:A</ta>
            <ta e="T1125" id="Seg_12303" s="T1124">np:G</ta>
            <ta e="T1127" id="Seg_12304" s="T1126">np.h:E</ta>
            <ta e="T1129" id="Seg_12305" s="T1128">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T8" id="Seg_12306" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_12307" s="T8">v:pred</ta>
            <ta e="T11" id="Seg_12308" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_12309" s="T11">v:pred</ta>
            <ta e="T16" id="Seg_12310" s="T15">np:S</ta>
            <ta e="T17" id="Seg_12311" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_12312" s="T18">np:S</ta>
            <ta e="T21" id="Seg_12313" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_12314" s="T22">np:S</ta>
            <ta e="T25" id="Seg_12315" s="T24">v:pred</ta>
            <ta e="T28" id="Seg_12316" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_12317" s="T28">v:pred</ta>
            <ta e="T43" id="Seg_12318" s="T42">np:O</ta>
            <ta e="T44" id="Seg_12319" s="T43">v:pred 0.3.h:S</ta>
            <ta e="T46" id="Seg_12320" s="T45">v:pred 0.3.h:S</ta>
            <ta e="T49" id="Seg_12321" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T53" id="Seg_12322" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T55" id="Seg_12323" s="T54">pro.h:S</ta>
            <ta e="T57" id="Seg_12324" s="T56">v:pred</ta>
            <ta e="T61" id="Seg_12325" s="T60">np:S</ta>
            <ta e="T63" id="Seg_12326" s="T62">v:pred</ta>
            <ta e="T66" id="Seg_12327" s="T65">np.h:S</ta>
            <ta e="T67" id="Seg_12328" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_12329" s="T67">np:O</ta>
            <ta e="T69" id="Seg_12330" s="T68">np:O</ta>
            <ta e="T71" id="Seg_12331" s="T70">np:O</ta>
            <ta e="T1134" id="Seg_12332" s="T74">conv:pred</ta>
            <ta e="T75" id="Seg_12333" s="T1134">v:pred 0.3.h:S</ta>
            <ta e="T78" id="Seg_12334" s="T77">adj:pred</ta>
            <ta e="T79" id="Seg_12335" s="T78">np:S</ta>
            <ta e="T81" id="Seg_12336" s="T80">np.h:S</ta>
            <ta e="T83" id="Seg_12337" s="T82">np.h:S</ta>
            <ta e="T85" id="Seg_12338" s="T84">np.h:S</ta>
            <ta e="T86" id="Seg_12339" s="T85">v:pred</ta>
            <ta e="T90" id="Seg_12340" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_12341" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_12342" s="T92">np:S</ta>
            <ta e="T94" id="Seg_12343" s="T93">v:pred</ta>
            <ta e="T95" id="Seg_12344" s="T94">np.h:S</ta>
            <ta e="T97" id="Seg_12345" s="T96">np:O</ta>
            <ta e="T98" id="Seg_12346" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_12347" s="T99">np.h:S</ta>
            <ta e="T1135" id="Seg_12348" s="T100">conv:pred</ta>
            <ta e="T101" id="Seg_12349" s="T1135">v:pred</ta>
            <ta e="T102" id="Seg_12350" s="T101">np.h:S</ta>
            <ta e="T1136" id="Seg_12351" s="T102">conv:pred</ta>
            <ta e="T103" id="Seg_12352" s="T1136">v:pred</ta>
            <ta e="T108" id="Seg_12353" s="T107">np.h:S</ta>
            <ta e="T109" id="Seg_12354" s="T108">v:pred</ta>
            <ta e="T112" id="Seg_12355" s="T111">v:pred 0.3.h:S</ta>
            <ta e="T114" id="Seg_12356" s="T113">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_12357" s="T116">np.h:S</ta>
            <ta e="T118" id="Seg_12358" s="T117">v:pred</ta>
            <ta e="T119" id="Seg_12359" s="T118">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_12360" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_12361" s="T122">np.h:S</ta>
            <ta e="T126" id="Seg_12362" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_12363" s="T127">v:pred 0.3.h:S</ta>
            <ta e="T129" id="Seg_12364" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_12365" s="T129">np.h:S</ta>
            <ta e="T132" id="Seg_12366" s="T131">v:pred</ta>
            <ta e="T134" id="Seg_12367" s="T133">np:S</ta>
            <ta e="T135" id="Seg_12368" s="T134">v:pred</ta>
            <ta e="T136" id="Seg_12369" s="T135">np:O</ta>
            <ta e="T141" id="Seg_12370" s="T140">np.h:S</ta>
            <ta e="T144" id="Seg_12371" s="T143">v:pred 0.2.h:S</ta>
            <ta e="T148" id="Seg_12372" s="T147">v:pred 0.2.h:S</ta>
            <ta e="T151" id="Seg_12373" s="T150">pro.h:S</ta>
            <ta e="T152" id="Seg_12374" s="T151">conv:pred</ta>
            <ta e="T153" id="Seg_12375" s="T152">v:pred</ta>
            <ta e="T155" id="Seg_12376" s="T154">np.h:S</ta>
            <ta e="T156" id="Seg_12377" s="T155">v:pred</ta>
            <ta e="T159" id="Seg_12378" s="T158">v:pred 0.2.h:S</ta>
            <ta e="T160" id="Seg_12379" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_12380" s="T160">pro.h:S</ta>
            <ta e="T163" id="Seg_12381" s="T162">np:O</ta>
            <ta e="T164" id="Seg_12382" s="T163">v:pred</ta>
            <ta e="T166" id="Seg_12383" s="T165">pro.h:S</ta>
            <ta e="T167" id="Seg_12384" s="T166">v:pred</ta>
            <ta e="T168" id="Seg_12385" s="T167">v:pred 0.2.h:S</ta>
            <ta e="T169" id="Seg_12386" s="T168">np.h:S</ta>
            <ta e="T170" id="Seg_12387" s="T169">np:O</ta>
            <ta e="T172" id="Seg_12388" s="T171">v:pred</ta>
            <ta e="T176" id="Seg_12389" s="T175">pro.h:S</ta>
            <ta e="T177" id="Seg_12390" s="T176">pro:O</ta>
            <ta e="T178" id="Seg_12391" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_12392" s="T179">v:pred</ta>
            <ta e="T182" id="Seg_12393" s="T181">ptcl:pred</ta>
            <ta e="T183" id="Seg_12394" s="T182">np:O</ta>
            <ta e="T184" id="Seg_12395" s="T183">v:pred 0.3.h:S</ta>
            <ta e="T186" id="Seg_12396" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T188" id="Seg_12397" s="T187">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_12398" s="T188">pro.h:S</ta>
            <ta e="T190" id="Seg_12399" s="T189">np:O</ta>
            <ta e="T191" id="Seg_12400" s="T190">v:pred</ta>
            <ta e="T193" id="Seg_12401" s="T192">np:O</ta>
            <ta e="T194" id="Seg_12402" s="T193">v:pred 0.3.h:S</ta>
            <ta e="T196" id="Seg_12403" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T199" id="Seg_12404" s="T198">np.h:S</ta>
            <ta e="T200" id="Seg_12405" s="T199">v:pred</ta>
            <ta e="T201" id="Seg_12406" s="T200"> 0.2.h:S v:pred</ta>
            <ta e="T202" id="Seg_12407" s="T201">np:S</ta>
            <ta e="T203" id="Seg_12408" s="T202">adj:pred</ta>
            <ta e="T205" id="Seg_12409" s="T204">v:pred 0.3.h:S</ta>
            <ta e="T210" id="Seg_12410" s="T209">ptcl:pred</ta>
            <ta e="T213" id="Seg_12411" s="T212">np.h:O</ta>
            <ta e="T216" id="Seg_12412" s="T215">np.h:S</ta>
            <ta e="T217" id="Seg_12413" s="T216">v:pred</ta>
            <ta e="T218" id="Seg_12414" s="T217">v:pred 0.2.h:S</ta>
            <ta e="T220" id="Seg_12415" s="T219">np.h:S</ta>
            <ta e="T221" id="Seg_12416" s="T220">adj:pred</ta>
            <ta e="T222" id="Seg_12417" s="T221">v:pred 0.1.h:S</ta>
            <ta e="T226" id="Seg_12418" s="T225">np:S</ta>
            <ta e="T227" id="Seg_12419" s="T226">v:pred</ta>
            <ta e="T230" id="Seg_12420" s="T229">np:S</ta>
            <ta e="T231" id="Seg_12421" s="T230">v:pred</ta>
            <ta e="T234" id="Seg_12422" s="T233">adj:pred</ta>
            <ta e="T240" id="Seg_12423" s="T239">np:S</ta>
            <ta e="T246" id="Seg_12424" s="T245">np:S</ta>
            <ta e="T248" id="Seg_12425" s="T247">adj:pred</ta>
            <ta e="T249" id="Seg_12426" s="T248">cop</ta>
            <ta e="T251" id="Seg_12427" s="T250">v:pred</ta>
            <ta e="T252" id="Seg_12428" s="T251">np.h:S</ta>
            <ta e="T257" id="Seg_12429" s="T256">np:O</ta>
            <ta e="T258" id="Seg_12430" s="T257">v:pred 0.1.h:S</ta>
            <ta e="T262" id="Seg_12431" s="T261">v:pred </ta>
            <ta e="T263" id="Seg_12432" s="T262">np:S</ta>
            <ta e="T264" id="Seg_12433" s="T263">pro.h:S</ta>
            <ta e="T265" id="Seg_12434" s="T264">pro:O</ta>
            <ta e="T266" id="Seg_12435" s="T265">v:pred</ta>
            <ta e="T269" id="Seg_12436" s="T268">np:O</ta>
            <ta e="T270" id="Seg_12437" s="T269">v:pred 0.1.h:S</ta>
            <ta e="T271" id="Seg_12438" s="T270">v:pred 0.1.h:S</ta>
            <ta e="T273" id="Seg_12439" s="T272">v:pred 0.1.h:S</ta>
            <ta e="T281" id="Seg_12440" s="T280">np:S</ta>
            <ta e="T282" id="Seg_12441" s="T281">v:pred</ta>
            <ta e="T285" id="Seg_12442" s="T284">np:S</ta>
            <ta e="T286" id="Seg_12443" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_12444" s="T287">np.h:S</ta>
            <ta e="T290" id="Seg_12445" s="T289">v:pred</ta>
            <ta e="T292" id="Seg_12446" s="T291">np:O</ta>
            <ta e="T294" id="Seg_12447" s="T293">np:S</ta>
            <ta e="T295" id="Seg_12448" s="T294">adj:pred</ta>
            <ta e="T296" id="Seg_12449" s="T295">cop</ta>
            <ta e="T299" id="Seg_12450" s="T298">np.h:S</ta>
            <ta e="T302" id="Seg_12451" s="T301">v:pred</ta>
            <ta e="T303" id="Seg_12452" s="T302">np:O</ta>
            <ta e="T309" id="Seg_12453" s="T308">v:pred </ta>
            <ta e="T311" id="Seg_12454" s="T310">np:S</ta>
            <ta e="T319" id="Seg_12455" s="T318">pro:S</ta>
            <ta e="T321" id="Seg_12456" s="T320">v:pred</ta>
            <ta e="T323" id="Seg_12457" s="T322">np.h:S</ta>
            <ta e="T324" id="Seg_12458" s="T323">v:pred</ta>
            <ta e="T331" id="Seg_12459" s="T330">np:S</ta>
            <ta e="T332" id="Seg_12460" s="T331">v:pred</ta>
            <ta e="T340" id="Seg_12461" s="T339">np:S</ta>
            <ta e="T341" id="Seg_12462" s="T340">adj:pred</ta>
            <ta e="T342" id="Seg_12463" s="T341">cop</ta>
            <ta e="T345" id="Seg_12464" s="T344">np:S</ta>
            <ta e="T346" id="Seg_12465" s="T345">adj:pred</ta>
            <ta e="T347" id="Seg_12466" s="T346">cop</ta>
            <ta e="T348" id="Seg_12467" s="T347">np:S</ta>
            <ta e="T349" id="Seg_12468" s="T348">v:pred</ta>
            <ta e="T350" id="Seg_12469" s="T349">adj:pred</ta>
            <ta e="T351" id="Seg_12470" s="T350">cop 0.3:S</ta>
            <ta e="T356" id="Seg_12471" s="T355">np:O</ta>
            <ta e="T358" id="Seg_12472" s="T357">v:pred</ta>
            <ta e="T359" id="Seg_12473" s="T358">np.h:S</ta>
            <ta e="T362" id="Seg_12474" s="T361">np:S</ta>
            <ta e="T364" id="Seg_12475" s="T363">v:pred</ta>
            <ta e="T373" id="Seg_12476" s="T372">np:S</ta>
            <ta e="T374" id="Seg_12477" s="T373">adj:pred</ta>
            <ta e="T375" id="Seg_12478" s="T374">cop</ta>
            <ta e="T379" id="Seg_12479" s="T378">pro.h:S</ta>
            <ta e="T382" id="Seg_12480" s="T381">v:pred</ta>
            <ta e="T383" id="Seg_12481" s="T382">v:pred 0.1.h:S</ta>
            <ta e="T385" id="Seg_12482" s="T384">v:pred 0.3:S</ta>
            <ta e="T389" id="Seg_12483" s="T388">v:pred 0.1.h:S</ta>
            <ta e="T400" id="Seg_12484" s="T399">np.h:S</ta>
            <ta e="T401" id="Seg_12485" s="T400">v:pred</ta>
            <ta e="T402" id="Seg_12486" s="T401">pro.h:S</ta>
            <ta e="T404" id="Seg_12487" s="T403">v:pred</ta>
            <ta e="T405" id="Seg_12488" s="T404">np:O</ta>
            <ta e="T407" id="Seg_12489" s="T406">v:pred 0.3.h:S</ta>
            <ta e="T412" id="Seg_12490" s="T411">np:O</ta>
            <ta e="T413" id="Seg_12491" s="T412">v:pred 0.3.h:S</ta>
            <ta e="T414" id="Seg_12492" s="T413">np:O</ta>
            <ta e="T415" id="Seg_12493" s="T414">v:pred 0.3.h:S</ta>
            <ta e="T417" id="Seg_12494" s="T416">pro.h:O</ta>
            <ta e="T418" id="Seg_12495" s="T417">v:pred 0.3.h:S</ta>
            <ta e="T422" id="Seg_12496" s="T421">pro.h:S</ta>
            <ta e="T424" id="Seg_12497" s="T423">np:O</ta>
            <ta e="T425" id="Seg_12498" s="T424">v:pred</ta>
            <ta e="T426" id="Seg_12499" s="T425">np:O</ta>
            <ta e="T427" id="Seg_12500" s="T426">v:pred 0.3.h:S</ta>
            <ta e="T429" id="Seg_12501" s="T428">np:O</ta>
            <ta e="T430" id="Seg_12502" s="T429">v:pred 0.3.h:S</ta>
            <ta e="T433" id="Seg_12503" s="T432">np:O</ta>
            <ta e="T435" id="Seg_12504" s="T434">v:pred 0.3.h:S</ta>
            <ta e="T437" id="Seg_12505" s="T436">pro:S</ta>
            <ta e="T439" id="Seg_12506" s="T438">v:pred</ta>
            <ta e="T441" id="Seg_12507" s="T440">v:pred 0.3:S</ta>
            <ta e="T446" id="Seg_12508" s="T445">pro.h:S</ta>
            <ta e="T447" id="Seg_12509" s="T446">np.h:O</ta>
            <ta e="T448" id="Seg_12510" s="T447">v:pred</ta>
            <ta e="T451" id="Seg_12511" s="T450">v:pred 0.3.h:S</ta>
            <ta e="T454" id="Seg_12512" s="T453">v:pred 0.3.h:S</ta>
            <ta e="T458" id="Seg_12513" s="T457">v:pred 0.3.h:S</ta>
            <ta e="T462" id="Seg_12514" s="T461">np:O</ta>
            <ta e="T463" id="Seg_12515" s="T462">v:pred 0.3.h:S</ta>
            <ta e="T465" id="Seg_12516" s="T464">np:O</ta>
            <ta e="T468" id="Seg_12517" s="T467">v:pred 0.3.h:S</ta>
            <ta e="T471" id="Seg_12518" s="T470">v:pred 0.3.h:S</ta>
            <ta e="T472" id="Seg_12519" s="T471">v:pred 0.3.h:S</ta>
            <ta e="T476" id="Seg_12520" s="T475">np:O</ta>
            <ta e="T478" id="Seg_12521" s="T477">v:pred 0.3.h:S</ta>
            <ta e="T482" id="Seg_12522" s="T481">np:S</ta>
            <ta e="T483" id="Seg_12523" s="T482">v:pred 0.3.h:S s:rel</ta>
            <ta e="T484" id="Seg_12524" s="T483">adj:pred</ta>
            <ta e="T486" id="Seg_12525" s="T485">np:S</ta>
            <ta e="T496" id="Seg_12526" s="T495">np:O</ta>
            <ta e="T497" id="Seg_12527" s="T496">v:pred 0.3.h:S</ta>
            <ta e="T499" id="Seg_12528" s="T498">np:S</ta>
            <ta e="T501" id="Seg_12529" s="T500">v:pred</ta>
            <ta e="T507" id="Seg_12530" s="T506">np.h:S</ta>
            <ta e="T510" id="Seg_12531" s="T509">v:pred</ta>
            <ta e="T511" id="Seg_12532" s="T510">v:pred 0.3.h:S</ta>
            <ta e="T514" id="Seg_12533" s="T513">v:pred 0.3.h:S</ta>
            <ta e="T517" id="Seg_12534" s="T516">pro.h:S</ta>
            <ta e="T519" id="Seg_12535" s="T518">v:pred</ta>
            <ta e="T522" id="Seg_12536" s="T521">np:O</ta>
            <ta e="T526" id="Seg_12537" s="T525">s:purp</ta>
            <ta e="T531" id="Seg_12538" s="T530">np.h:S</ta>
            <ta e="T533" id="Seg_12539" s="T532">ptcl.neg</ta>
            <ta e="T534" id="Seg_12540" s="T533">v:pred</ta>
            <ta e="T536" id="Seg_12541" s="T535">pro.h:S</ta>
            <ta e="T537" id="Seg_12542" s="T536">np:O</ta>
            <ta e="T538" id="Seg_12543" s="T537">v:pred</ta>
            <ta e="T542" id="Seg_12544" s="T541">v:pred 0.1.h:S</ta>
            <ta e="T545" id="Seg_12545" s="T544">np.h:S</ta>
            <ta e="T546" id="Seg_12546" s="T545">np.h:S</ta>
            <ta e="T548" id="Seg_12547" s="T547">v:pred</ta>
            <ta e="T550" id="Seg_12548" s="T549">np:O</ta>
            <ta e="T1137" id="Seg_12549" s="T550">conv:pred</ta>
            <ta e="T551" id="Seg_12550" s="T1137">v:pred 0.3.h:S</ta>
            <ta e="T552" id="Seg_12551" s="T551">s:purp</ta>
            <ta e="T555" id="Seg_12552" s="T554">pro.h:S</ta>
            <ta e="T559" id="Seg_12553" s="T558">ptcl.neg</ta>
            <ta e="T560" id="Seg_12554" s="T559">v:pred</ta>
            <ta e="T564" id="Seg_12555" s="T563">v:pred 0.1.h:S</ta>
            <ta e="T567" id="Seg_12556" s="T566">pro.h:S</ta>
            <ta e="T568" id="Seg_12557" s="T567">v:pred</ta>
            <ta e="T569" id="Seg_12558" s="T568">v:pred 0.2.h:S</ta>
            <ta e="T570" id="Seg_12559" s="T569">v:pred 0.2.h:S</ta>
            <ta e="T573" id="Seg_12560" s="T572">pro.h:S</ta>
            <ta e="T574" id="Seg_12561" s="T573">v:pred</ta>
            <ta e="T577" id="Seg_12562" s="T576">s:cond</ta>
            <ta e="T579" id="Seg_12563" s="T578">adj:pred</ta>
            <ta e="T581" id="Seg_12564" s="T580">pro.h:S</ta>
            <ta e="T582" id="Seg_12565" s="T581">conv:pred</ta>
            <ta e="T586" id="Seg_12566" s="T585">v:pred 0.1.h:S</ta>
            <ta e="T588" id="Seg_12567" s="T587">ptcl:pred</ta>
            <ta e="T589" id="Seg_12568" s="T588">v:pred 0.2.h:S</ta>
            <ta e="T590" id="Seg_12569" s="T589">pro.h:S</ta>
            <ta e="T591" id="Seg_12570" s="T590">v:pred</ta>
            <ta e="T595" id="Seg_12571" s="T594">pro.h:S</ta>
            <ta e="T596" id="Seg_12572" s="T595">v:pred</ta>
            <ta e="T601" id="Seg_12573" s="T600">pro.h:S</ta>
            <ta e="T602" id="Seg_12574" s="T601">v:pred</ta>
            <ta e="T603" id="Seg_12575" s="T602">pro.h:O</ta>
            <ta e="T604" id="Seg_12576" s="T603">v:pred 0.3.h:S</ta>
            <ta e="T606" id="Seg_12577" s="T605">np:O</ta>
            <ta e="T607" id="Seg_12578" s="T606">v:pred 0.3.h:S</ta>
            <ta e="T608" id="Seg_12579" s="T607">np:O</ta>
            <ta e="T609" id="Seg_12580" s="T608">v:pred 0.3.h:S</ta>
            <ta e="T611" id="Seg_12581" s="T610">np:O</ta>
            <ta e="T613" id="Seg_12582" s="T612">v:pred 0.3.h:S</ta>
            <ta e="T616" id="Seg_12583" s="T615">v:pred 0.3.h:S</ta>
            <ta e="T620" id="Seg_12584" s="T619">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_12585" s="T620">pro:O</ta>
            <ta e="T623" id="Seg_12586" s="T622">v:pred 0.3.h:S</ta>
            <ta e="T626" id="Seg_12587" s="T625">np:O</ta>
            <ta e="T627" id="Seg_12588" s="T626">v:pred 0.3.h:S</ta>
            <ta e="T628" id="Seg_12589" s="T627">np:O</ta>
            <ta e="T629" id="Seg_12590" s="T628">v:pred 0.3.h:S</ta>
            <ta e="T630" id="Seg_12591" s="T629">np:O</ta>
            <ta e="T631" id="Seg_12592" s="T630">v:pred 0.3.h:S</ta>
            <ta e="T1138" id="Seg_12593" s="T634">conv:pred</ta>
            <ta e="T635" id="Seg_12594" s="T1138">v:pred 0.3.h:S</ta>
            <ta e="T639" id="Seg_12595" s="T638">pro.h:S</ta>
            <ta e="T643" id="Seg_12596" s="T642">v:pred</ta>
            <ta e="T646" id="Seg_12597" s="T645">v:pred 0.1.h:S</ta>
            <ta e="T650" id="Seg_12598" s="T649">v:pred 0.1.h:S</ta>
            <ta e="T660" id="Seg_12599" s="T659">pro.h:S</ta>
            <ta e="T661" id="Seg_12600" s="T660">v:pred</ta>
            <ta e="T673" id="Seg_12601" s="T672">v:pred 0.1.h:S</ta>
            <ta e="T676" id="Seg_12602" s="T675">pro.h:O</ta>
            <ta e="T677" id="Seg_12603" s="T676">ptcl.neg</ta>
            <ta e="T678" id="Seg_12604" s="T677">v:pred 0.1.h:S</ta>
            <ta e="T683" id="Seg_12605" s="T682">np:S</ta>
            <ta e="T684" id="Seg_12606" s="T683">v:pred</ta>
            <ta e="T685" id="Seg_12607" s="T684">np:S</ta>
            <ta e="T686" id="Seg_12608" s="T685">v:pred</ta>
            <ta e="T689" id="Seg_12609" s="T688">pro:S</ta>
            <ta e="T690" id="Seg_12610" s="T689">v:pred</ta>
            <ta e="T693" id="Seg_12611" s="T692">np:S</ta>
            <ta e="T695" id="Seg_12612" s="T694">v:pred</ta>
            <ta e="T697" id="Seg_12613" s="T696">np:S</ta>
            <ta e="T698" id="Seg_12614" s="T697">v:pred</ta>
            <ta e="T704" id="Seg_12615" s="T703">np:S</ta>
            <ta e="T705" id="Seg_12616" s="T704">v:pred</ta>
            <ta e="T707" id="Seg_12617" s="T706">np:S</ta>
            <ta e="T708" id="Seg_12618" s="T707">v:pred</ta>
            <ta e="T711" id="Seg_12619" s="T710">np:S</ta>
            <ta e="T712" id="Seg_12620" s="T711">v:pred</ta>
            <ta e="T715" id="Seg_12621" s="T714">v:pred 0.3.h:S</ta>
            <ta e="T719" id="Seg_12622" s="T718">ptcl:pred</ta>
            <ta e="T726" id="Seg_12623" s="T725">np.h:O</ta>
            <ta e="T728" id="Seg_12624" s="T727">np.h:O</ta>
            <ta e="T729" id="Seg_12625" s="T728">ptcl:pred</ta>
            <ta e="T731" id="Seg_12626" s="T730">ptcl:pred</ta>
            <ta e="T734" id="Seg_12627" s="T733">pro.h:O</ta>
            <ta e="T738" id="Seg_12628" s="T737">pro:O</ta>
            <ta e="T739" id="Seg_12629" s="T738">v:pred 0.3.h:S</ta>
            <ta e="T740" id="Seg_12630" s="T739">s:purp</ta>
            <ta e="T742" id="Seg_12631" s="T741">pro:O</ta>
            <ta e="T743" id="Seg_12632" s="T742">v:pred 0.3.h:S</ta>
            <ta e="T744" id="Seg_12633" s="T743">s:purp</ta>
            <ta e="T748" id="Seg_12634" s="T747">pro.h:S</ta>
            <ta e="T751" id="Seg_12635" s="T750">v:pred</ta>
            <ta e="T756" id="Seg_12636" s="T755">v:pred 0.3.h:S</ta>
            <ta e="T765" id="Seg_12637" s="T764">adj:pred</ta>
            <ta e="T766" id="Seg_12638" s="T765">np:S</ta>
            <ta e="T768" id="Seg_12639" s="T767">np:S</ta>
            <ta e="T769" id="Seg_12640" s="T768">adj:pred</ta>
            <ta e="T774" id="Seg_12641" s="T773">v:pred</ta>
            <ta e="T775" id="Seg_12642" s="T774">np.h:S</ta>
            <ta e="T777" id="Seg_12643" s="T776">np.h:S</ta>
            <ta e="T783" id="Seg_12644" s="T782">np.h:S</ta>
            <ta e="T784" id="Seg_12645" s="T783">conv:pred</ta>
            <ta e="T785" id="Seg_12646" s="T784">v:pred</ta>
            <ta e="T788" id="Seg_12647" s="T787">np.h:S</ta>
            <ta e="T789" id="Seg_12648" s="T788">v:pred</ta>
            <ta e="T791" id="Seg_12649" s="T790">np:O</ta>
            <ta e="T793" id="Seg_12650" s="T792">s:purp</ta>
            <ta e="T799" id="Seg_12651" s="T798">v:pred 0.3.h:S</ta>
            <ta e="T801" id="Seg_12652" s="T800">pro:O</ta>
            <ta e="T802" id="Seg_12653" s="T801">ptcl.neg</ta>
            <ta e="T803" id="Seg_12654" s="T802">v:pred 0.3.h:S</ta>
            <ta e="T805" id="Seg_12655" s="T804">np.h:O</ta>
            <ta e="T806" id="Seg_12656" s="T805">v:pred 0.3.h:S</ta>
            <ta e="T809" id="Seg_12657" s="T808">pro.h:S</ta>
            <ta e="T810" id="Seg_12658" s="T809">v:pred</ta>
            <ta e="T814" id="Seg_12659" s="T813">np.h:S</ta>
            <ta e="T815" id="Seg_12660" s="T814">adj:pred</ta>
            <ta e="T816" id="Seg_12661" s="T815">v:pred 0.3.h:S</ta>
            <ta e="T821" id="Seg_12662" s="T820">conv:pred</ta>
            <ta e="T825" id="Seg_12663" s="T824">pro.h:S</ta>
            <ta e="T826" id="Seg_12664" s="T825">v:pred</ta>
            <ta e="T828" id="Seg_12665" s="T827">v:pred 0.3.h:S</ta>
            <ta e="T832" id="Seg_12666" s="T831">v:pred 0.3.h:S</ta>
            <ta e="T834" id="Seg_12667" s="T833">np.h:S</ta>
            <ta e="T836" id="Seg_12668" s="T835">adj:pred</ta>
            <ta e="T837" id="Seg_12669" s="T836">cop</ta>
            <ta e="T838" id="Seg_12670" s="T837">v:pred 0.3.h:S</ta>
            <ta e="T847" id="Seg_12671" s="T846">ptcl:pred</ta>
            <ta e="T851" id="Seg_12672" s="T850">pro.h:S</ta>
            <ta e="T852" id="Seg_12673" s="T851">pro:O</ta>
            <ta e="T853" id="Seg_12674" s="T852">ptcl.neg</ta>
            <ta e="T854" id="Seg_12675" s="T853">v:pred</ta>
            <ta e="T856" id="Seg_12676" s="T855">np:S</ta>
            <ta e="T858" id="Seg_12677" s="T857">v:pred</ta>
            <ta e="T864" id="Seg_12678" s="T863">pro.h:S</ta>
            <ta e="T865" id="Seg_12679" s="T864">v:pred</ta>
            <ta e="T870" id="Seg_12680" s="T869">ptcl:pred</ta>
            <ta e="T873" id="Seg_12681" s="T872">v:pred 0.2.h:S</ta>
            <ta e="T875" id="Seg_12682" s="T874">pro.h:S</ta>
            <ta e="T876" id="Seg_12683" s="T875">v:pred</ta>
            <ta e="T877" id="Seg_12684" s="T876">pro:S</ta>
            <ta e="T880" id="Seg_12685" s="T879">adj:pred</ta>
            <ta e="T883" id="Seg_12686" s="T882">adj:pred</ta>
            <ta e="T884" id="Seg_12687" s="T883">np:S</ta>
            <ta e="T886" id="Seg_12688" s="T885">np.h:S</ta>
            <ta e="T888" id="Seg_12689" s="T887">v:pred</ta>
            <ta e="T890" id="Seg_12690" s="T889">v:pred 0.2.h:S</ta>
            <ta e="T897" id="Seg_12691" s="T896">ptcl:pred</ta>
            <ta e="T901" id="Seg_12692" s="T900">ptcl.neg</ta>
            <ta e="T902" id="Seg_12693" s="T901">v:pred 0.2.h:S</ta>
            <ta e="T903" id="Seg_12694" s="T902">np:O</ta>
            <ta e="T906" id="Seg_12695" s="T905">np:S</ta>
            <ta e="T907" id="Seg_12696" s="T906">ptcl.neg</ta>
            <ta e="T908" id="Seg_12697" s="T907">adj:pred</ta>
            <ta e="T910" id="Seg_12698" s="T909">pro.h:S</ta>
            <ta e="T912" id="Seg_12699" s="T911">v:pred</ta>
            <ta e="T913" id="Seg_12700" s="T912">ptcl:pred</ta>
            <ta e="T917" id="Seg_12701" s="T916">v:pred 0.2.h:S</ta>
            <ta e="T920" id="Seg_12702" s="T919">np.h:S</ta>
            <ta e="T921" id="Seg_12703" s="T920">v:pred</ta>
            <ta e="T922" id="Seg_12704" s="T921">pro:S</ta>
            <ta e="T924" id="Seg_12705" s="T923">adj:pred</ta>
            <ta e="T926" id="Seg_12706" s="T925">np:S</ta>
            <ta e="T927" id="Seg_12707" s="T926">adj:pred</ta>
            <ta e="T930" id="Seg_12708" s="T929">np:S</ta>
            <ta e="T931" id="Seg_12709" s="T930">ptcl.neg</ta>
            <ta e="T932" id="Seg_12710" s="T931">adj:pred</ta>
            <ta e="T934" id="Seg_12711" s="T933">v:pred 0.2.h:S</ta>
            <ta e="T942" id="Seg_12712" s="T941">np.h:S</ta>
            <ta e="T944" id="Seg_12713" s="T942">v:pred</ta>
            <ta e="T945" id="Seg_12714" s="T944">v:pred 0.2.h:S</ta>
            <ta e="T947" id="Seg_12715" s="T946">v:pred 0.2.h:S</ta>
            <ta e="T949" id="Seg_12716" s="T948">pro.h:S</ta>
            <ta e="T950" id="Seg_12717" s="T949">cop</ta>
            <ta e="T951" id="Seg_12718" s="T950">n:pred</ta>
            <ta e="T956" id="Seg_12719" s="T955">np.h:S</ta>
            <ta e="T957" id="Seg_12720" s="T956">v:pred</ta>
            <ta e="T958" id="Seg_12721" s="T957">v:pred 0.2.h:S</ta>
            <ta e="T961" id="Seg_12722" s="T960">pro.h:S</ta>
            <ta e="T962" id="Seg_12723" s="T961">v:pred</ta>
            <ta e="T965" id="Seg_12724" s="T964">np.h:S</ta>
            <ta e="T968" id="Seg_12725" s="T966">v:pred</ta>
            <ta e="T969" id="Seg_12726" s="T968">v:pred 0.2.h:S</ta>
            <ta e="T971" id="Seg_12727" s="T970">v:pred 0.2.h:S</ta>
            <ta e="T973" id="Seg_12728" s="T972">pro.h:S</ta>
            <ta e="T974" id="Seg_12729" s="T973">cop</ta>
            <ta e="T975" id="Seg_12730" s="T974">n:pred</ta>
            <ta e="T977" id="Seg_12731" s="T976">pro.h:S</ta>
            <ta e="T978" id="Seg_12732" s="T977">v:pred</ta>
            <ta e="T981" id="Seg_12733" s="T980">v:pred 0.3.h:S</ta>
            <ta e="T983" id="Seg_12734" s="T982">np.h:S</ta>
            <ta e="T984" id="Seg_12735" s="T983">v:pred</ta>
            <ta e="T985" id="Seg_12736" s="T984">pro:S</ta>
            <ta e="T987" id="Seg_12737" s="T986">adj:pred</ta>
            <ta e="T990" id="Seg_12738" s="T989">np.h:S</ta>
            <ta e="T991" id="Seg_12739" s="T990">v:pred</ta>
            <ta e="T993" id="Seg_12740" s="T992">pro.h:S</ta>
            <ta e="T995" id="Seg_12741" s="T994">v:pred</ta>
            <ta e="T997" id="Seg_12742" s="T996">v:pred 0.2.h:S</ta>
            <ta e="T1003" id="Seg_12743" s="T1002">np.h:S</ta>
            <ta e="T1004" id="Seg_12744" s="T1003">v:pred</ta>
            <ta e="T1007" id="Seg_12745" s="T1006">ptcl:pred</ta>
            <ta e="T1015" id="Seg_12746" s="T1014">ptcl:pred</ta>
            <ta e="T1017" id="Seg_12747" s="T1016">v:pred 0.2.h:S</ta>
            <ta e="T1020" id="Seg_12748" s="T1019">v:pred 0.2.h:S</ta>
            <ta e="T1022" id="Seg_12749" s="T1021">pro.h:S</ta>
            <ta e="T1028" id="Seg_12750" s="T1027">np.h:S</ta>
            <ta e="T1036" id="Seg_12751" s="T1035">pro.h:S</ta>
            <ta e="T1037" id="Seg_12752" s="T1036">v:pred</ta>
            <ta e="T1042" id="Seg_12753" s="T1041">v:pred 0.2.h:S</ta>
            <ta e="T1044" id="Seg_12754" s="T1043">np.h:S</ta>
            <ta e="T1045" id="Seg_12755" s="T1044">v:pred</ta>
            <ta e="T1047" id="Seg_12756" s="T1046">pro:S</ta>
            <ta e="T1049" id="Seg_12757" s="T1048">adj:pred</ta>
            <ta e="T1053" id="Seg_12758" s="T1052">np.h:S</ta>
            <ta e="T1055" id="Seg_12759" s="T1054">v:pred</ta>
            <ta e="T1060" id="Seg_12760" s="T1059">pro.h:S</ta>
            <ta e="T1061" id="Seg_12761" s="T1060">v:pred</ta>
            <ta e="T1066" id="Seg_12762" s="T1065">np.h:S</ta>
            <ta e="T1068" id="Seg_12763" s="T1067">v:pred</ta>
            <ta e="T1070" id="Seg_12764" s="T1069">v:pred 0.2.h:S</ta>
            <ta e="T1073" id="Seg_12765" s="T1072">v:pred 0.3:S</ta>
            <ta e="T1079" id="Seg_12766" s="T1078">np.h:S</ta>
            <ta e="T1080" id="Seg_12767" s="T1079">v:pred</ta>
            <ta e="T1085" id="Seg_12768" s="T1084">v:pred 0.2.h:S</ta>
            <ta e="T1087" id="Seg_12769" s="T1086">np.h:S</ta>
            <ta e="T1088" id="Seg_12770" s="T1087">v:pred</ta>
            <ta e="T1089" id="Seg_12771" s="T1088">pro:S</ta>
            <ta e="T1091" id="Seg_12772" s="T1090">adj:pred</ta>
            <ta e="T1095" id="Seg_12773" s="T1094">np.h:S</ta>
            <ta e="T1096" id="Seg_12774" s="T1095">v:pred</ta>
            <ta e="T1098" id="Seg_12775" s="T1097">pro.h:S</ta>
            <ta e="T1099" id="Seg_12776" s="T1098">v:pred</ta>
            <ta e="T1104" id="Seg_12777" s="T1103">np.h:S</ta>
            <ta e="T1107" id="Seg_12778" s="T1106">v:pred</ta>
            <ta e="T1109" id="Seg_12779" s="T1108">pro:O</ta>
            <ta e="T1112" id="Seg_12780" s="T1111">np.h:S</ta>
            <ta e="T1113" id="Seg_12781" s="T1112">v:pred</ta>
            <ta e="T1117" id="Seg_12782" s="T1116">pro:O</ta>
            <ta e="T1120" id="Seg_12783" s="T1119">ptcl.neg</ta>
            <ta e="T1121" id="Seg_12784" s="T1120">v:pred 0.3.h:S</ta>
            <ta e="T1123" id="Seg_12785" s="T1122">pro.h:S</ta>
            <ta e="T1124" id="Seg_12786" s="T1123">v:pred</ta>
            <ta e="T1127" id="Seg_12787" s="T1126">np.h:S</ta>
            <ta e="T1128" id="Seg_12788" s="T1127">v:pred</ta>
            <ta e="T1129" id="Seg_12789" s="T1128">v:pred 0.3.h:S</ta>
            <ta e="T1132" id="Seg_12790" s="T1131">np:S</ta>
            <ta e="T1133" id="Seg_12791" s="T1132">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_12792" s="T3">RUS:gram</ta>
            <ta e="T16" id="Seg_12793" s="T15">RUS:cult</ta>
            <ta e="T19" id="Seg_12794" s="T18">RUS:cult</ta>
            <ta e="T23" id="Seg_12795" s="T22">RUS:cult</ta>
            <ta e="T24" id="Seg_12796" s="T23">TURK:cult</ta>
            <ta e="T30" id="Seg_12797" s="T29">RUS:gram</ta>
            <ta e="T32" id="Seg_12798" s="T31">TURK:disc</ta>
            <ta e="T41" id="Seg_12799" s="T40">RUS:gram</ta>
            <ta e="T43" id="Seg_12800" s="T42">RUS:cult</ta>
            <ta e="T47" id="Seg_12801" s="T46">RUS:gram</ta>
            <ta e="T54" id="Seg_12802" s="T53">RUS:gram</ta>
            <ta e="T56" id="Seg_12803" s="T55">TURK:disc</ta>
            <ta e="T62" id="Seg_12804" s="T61">TURK:disc</ta>
            <ta e="T70" id="Seg_12805" s="T69">RUS:gram</ta>
            <ta e="T71" id="Seg_12806" s="T70">TURK:cult</ta>
            <ta e="T72" id="Seg_12807" s="T71">RUS:gram</ta>
            <ta e="T73" id="Seg_12808" s="T72">TURK:core</ta>
            <ta e="T78" id="Seg_12809" s="T77">TURK:disc</ta>
            <ta e="T79" id="Seg_12810" s="T78">RUS:core</ta>
            <ta e="T82" id="Seg_12811" s="T81">RUS:gram</ta>
            <ta e="T84" id="Seg_12812" s="T83">RUS:gram</ta>
            <ta e="T85" id="Seg_12813" s="T84">RUS:core</ta>
            <ta e="T89" id="Seg_12814" s="T88">RUS:gram</ta>
            <ta e="T96" id="Seg_12815" s="T95">TURK:disc</ta>
            <ta e="T102" id="Seg_12816" s="T101">RUS:core</ta>
            <ta e="T105" id="Seg_12817" s="T104">RUS:gram</ta>
            <ta e="T110" id="Seg_12818" s="T109">RUS:gram</ta>
            <ta e="T113" id="Seg_12819" s="T112">RUS:gram</ta>
            <ta e="T116" id="Seg_12820" s="T115">RUS:gram</ta>
            <ta e="T117" id="Seg_12821" s="T116">RUS:core</ta>
            <ta e="T120" id="Seg_12822" s="T119">RUS:gram</ta>
            <ta e="T131" id="Seg_12823" s="T130">TURK:disc</ta>
            <ta e="T141" id="Seg_12824" s="T140">TURK:core</ta>
            <ta e="T146" id="Seg_12825" s="T145">RUS:gram</ta>
            <ta e="T155" id="Seg_12826" s="T154">TURK:core</ta>
            <ta e="T158" id="Seg_12827" s="T157">RUS:disc</ta>
            <ta e="T182" id="Seg_12828" s="T181">RUS:gram</ta>
            <ta e="T183" id="Seg_12829" s="T182">TURK:cult</ta>
            <ta e="T185" id="Seg_12830" s="T184">RUS:gram</ta>
            <ta e="T190" id="Seg_12831" s="T189">TURK:cult</ta>
            <ta e="T208" id="Seg_12832" s="T207">TURK:core</ta>
            <ta e="T210" id="Seg_12833" s="T209">RUS:gram</ta>
            <ta e="T215" id="Seg_12834" s="T214">RUS:gram</ta>
            <ta e="T225" id="Seg_12835" s="T224">TURK:disc</ta>
            <ta e="T229" id="Seg_12836" s="T228">RUS:gram</ta>
            <ta e="T253" id="Seg_12837" s="T252">TURK:disc</ta>
            <ta e="T267" id="Seg_12838" s="T266">RUS:cult</ta>
            <ta e="T274" id="Seg_12839" s="T273">RUS:cult</ta>
            <ta e="T283" id="Seg_12840" s="T282">RUS:gram</ta>
            <ta e="T297" id="Seg_12841" s="T296">TURK:disc</ta>
            <ta e="T310" id="Seg_12842" s="T309">RUS:disc</ta>
            <ta e="T322" id="Seg_12843" s="T321">RUS:gram</ta>
            <ta e="T333" id="Seg_12844" s="T332">RUS:mod</ta>
            <ta e="T341" id="Seg_12845" s="T340">TURK:core</ta>
            <ta e="T344" id="Seg_12846" s="T343">RUS:gram</ta>
            <ta e="T346" id="Seg_12847" s="T345">TURK:core</ta>
            <ta e="T356" id="Seg_12848" s="T355">TURK:cult</ta>
            <ta e="T363" id="Seg_12849" s="T362">TURK:disc</ta>
            <ta e="T365" id="Seg_12850" s="T364">RUS:cult</ta>
            <ta e="T367" id="Seg_12851" s="T366">RUS:gram</ta>
            <ta e="T371" id="Seg_12852" s="T370">RUS:gram</ta>
            <ta e="T374" id="Seg_12853" s="T373">TURK:core</ta>
            <ta e="T384" id="Seg_12854" s="T383">RUS:gram</ta>
            <ta e="T387" id="Seg_12855" s="T386">RUS:gram</ta>
            <ta e="T403" id="Seg_12856" s="T402">TURK:disc</ta>
            <ta e="T426" id="Seg_12857" s="T425">TURK:cult</ta>
            <ta e="T429" id="Seg_12858" s="T428">TURK:cult</ta>
            <ta e="T431" id="Seg_12859" s="T430">TURK:disc</ta>
            <ta e="T440" id="Seg_12860" s="T439">RUS:gram</ta>
            <ta e="T453" id="Seg_12861" s="T452">TURK:disc</ta>
            <ta e="T461" id="Seg_12862" s="T460">TURK:disc</ta>
            <ta e="T464" id="Seg_12863" s="T463">RUS:gram</ta>
            <ta e="T473" id="Seg_12864" s="T472">TAT:cult</ta>
            <ta e="T474" id="Seg_12865" s="T473">TURK:disc</ta>
            <ta e="T485" id="Seg_12866" s="T484">RUS:gram</ta>
            <ta e="T498" id="Seg_12867" s="T497">RUS:gram</ta>
            <ta e="T516" id="Seg_12868" s="T515">RUS:gram</ta>
            <ta e="T520" id="Seg_12869" s="T1139">TAT:cult</ta>
            <ta e="T527" id="Seg_12870" s="T526">RUS:gram</ta>
            <ta e="T531" id="Seg_12871" s="T530">TURK:cult</ta>
            <ta e="T537" id="Seg_12872" s="T536">TURK:cult</ta>
            <ta e="T540" id="Seg_12873" s="T539">RUS:gram</ta>
            <ta e="T546" id="Seg_12874" s="T545">TURK:core</ta>
            <ta e="T550" id="Seg_12875" s="T549">TURK:cult</ta>
            <ta e="T554" id="Seg_12876" s="T553">RUS:gram</ta>
            <ta e="T558" id="Seg_12877" s="T557">RUS:mod</ta>
            <ta e="T566" id="Seg_12878" s="T565">RUS:gram</ta>
            <ta e="T572" id="Seg_12879" s="T571">RUS:gram</ta>
            <ta e="T580" id="Seg_12880" s="T579">RUS:gram</ta>
            <ta e="T588" id="Seg_12881" s="T587">RUS:mod</ta>
            <ta e="T592" id="Seg_12882" s="T591">RUS:gram</ta>
            <ta e="T594" id="Seg_12883" s="T593">RUS:gram</ta>
            <ta e="T626" id="Seg_12884" s="T625">TURK:cult</ta>
            <ta e="T628" id="Seg_12885" s="T627">TURK:cult</ta>
            <ta e="T633" id="Seg_12886" s="T632">RUS:gram</ta>
            <ta e="T641" id="Seg_12887" s="T640">RUS:core</ta>
            <ta e="T644" id="Seg_12888" s="T1140">TAT:cult</ta>
            <ta e="T652" id="Seg_12889" s="T651">TURK:cult</ta>
            <ta e="T655" id="Seg_12890" s="T654">TURK:gram(INDEF)</ta>
            <ta e="T659" id="Seg_12891" s="T658">RUS:gram</ta>
            <ta e="T662" id="Seg_12892" s="T661">RUS:gram</ta>
            <ta e="T666" id="Seg_12893" s="T665">TURK:disc</ta>
            <ta e="T667" id="Seg_12894" s="T666">TAT:cult</ta>
            <ta e="T687" id="Seg_12895" s="T686">RUS:gram</ta>
            <ta e="T691" id="Seg_12896" s="T690">RUS:gram</ta>
            <ta e="T696" id="Seg_12897" s="T695">RUS:gram</ta>
            <ta e="T703" id="Seg_12898" s="T702">TURK:disc</ta>
            <ta e="T706" id="Seg_12899" s="T705">RUS:gram</ta>
            <ta e="T709" id="Seg_12900" s="T708">RUS:gram</ta>
            <ta e="T713" id="Seg_12901" s="T712">TURK:disc</ta>
            <ta e="T719" id="Seg_12902" s="T718">RUS:mod</ta>
            <ta e="T728" id="Seg_12903" s="T727">RUS:cult</ta>
            <ta e="T729" id="Seg_12904" s="T728">RUS:mod</ta>
            <ta e="T731" id="Seg_12905" s="T730">RUS:gram</ta>
            <ta e="T738" id="Seg_12906" s="T737">RUS:gram(INDEF)</ta>
            <ta e="T741" id="Seg_12907" s="T740">RUS:gram</ta>
            <ta e="T742" id="Seg_12908" s="T741">RUS:gram(INDEF)</ta>
            <ta e="T749" id="Seg_12909" s="T748">TURK:disc</ta>
            <ta e="T752" id="Seg_12910" s="T751">RUS:gram</ta>
            <ta e="T767" id="Seg_12911" s="T766">RUS:gram</ta>
            <ta e="T776" id="Seg_12912" s="T775">RUS:gram</ta>
            <ta e="T787" id="Seg_12913" s="T786">RUS:gram</ta>
            <ta e="T796" id="Seg_12914" s="T795">RUS:core</ta>
            <ta e="T798" id="Seg_12915" s="T797">RUS:core</ta>
            <ta e="T801" id="Seg_12916" s="T800">TURK:gram(INDEF)</ta>
            <ta e="T805" id="Seg_12917" s="T804">RUS:core</ta>
            <ta e="T816" id="Seg_12918" s="T815">%TURK:core</ta>
            <ta e="T830" id="Seg_12919" s="T829">RUS:gram</ta>
            <ta e="T832" id="Seg_12920" s="T831">%TURK:core</ta>
            <ta e="T835" id="Seg_12921" s="T834">TURK:disc</ta>
            <ta e="T838" id="Seg_12922" s="T837">%TURK:core</ta>
            <ta e="T839" id="Seg_12923" s="T838">RUS:gram</ta>
            <ta e="T847" id="Seg_12924" s="T846">RUS:gram</ta>
            <ta e="T852" id="Seg_12925" s="T851">TURK:gram(INDEF)</ta>
            <ta e="T856" id="Seg_12926" s="T855">RUS:cult</ta>
            <ta e="T862" id="Seg_12927" s="T861">RUS:cult</ta>
            <ta e="T866" id="Seg_12928" s="T865">TURK:core</ta>
            <ta e="T870" id="Seg_12929" s="T869">RUS:gram</ta>
            <ta e="T880" id="Seg_12930" s="T879">TURK:core</ta>
            <ta e="T883" id="Seg_12931" s="T882">TURK:core</ta>
            <ta e="T884" id="Seg_12932" s="T883">RUS:cult</ta>
            <ta e="T887" id="Seg_12933" s="T886">TURK:disc</ta>
            <ta e="T889" id="Seg_12934" s="T888">RUS:disc</ta>
            <ta e="T897" id="Seg_12935" s="T896">RUS:gram</ta>
            <ta e="T903" id="Seg_12936" s="T902">TAT:cult</ta>
            <ta e="T906" id="Seg_12937" s="T905">TAT:cult</ta>
            <ta e="T911" id="Seg_12938" s="T910">TURK:core</ta>
            <ta e="T913" id="Seg_12939" s="T912">RUS:gram</ta>
            <ta e="T924" id="Seg_12940" s="T923">TURK:core</ta>
            <ta e="T926" id="Seg_12941" s="T925">TAT:cult</ta>
            <ta e="T927" id="Seg_12942" s="T926">TURK:core</ta>
            <ta e="T930" id="Seg_12943" s="T929">TAT:cult</ta>
            <ta e="T940" id="Seg_12944" s="T939">TURK:core</ta>
            <ta e="T948" id="Seg_12945" s="T947">RUS:gram</ta>
            <ta e="T951" id="Seg_12946" s="T950">RUS:cult</ta>
            <ta e="T963" id="Seg_12947" s="T962">RUS:gram</ta>
            <ta e="T966" id="Seg_12948" s="T965">TURK:core</ta>
            <ta e="T972" id="Seg_12949" s="T971">RUS:gram</ta>
            <ta e="T975" id="Seg_12950" s="T974">RUS:cult</ta>
            <ta e="T987" id="Seg_12951" s="T986">TURK:core</ta>
            <ta e="T992" id="Seg_12952" s="T991">RUS:gram</ta>
            <ta e="T994" id="Seg_12953" s="T993">RUS:cult</ta>
            <ta e="T1007" id="Seg_12954" s="T1006">RUS:gram</ta>
            <ta e="T1008" id="Seg_12955" s="T1007">TURK:core</ta>
            <ta e="T1013" id="Seg_12956" s="T1012">RUS:gram</ta>
            <ta e="T1015" id="Seg_12957" s="T1014">RUS:mod</ta>
            <ta e="T1018" id="Seg_12958" s="T1017">TURK:core</ta>
            <ta e="T1021" id="Seg_12959" s="T1020">RUS:gram</ta>
            <ta e="T1027" id="Seg_12960" s="T1026">RUS:gram</ta>
            <ta e="T1049" id="Seg_12961" s="T1048">TURK:core</ta>
            <ta e="T1051" id="Seg_12962" s="T1050">RUS:gram</ta>
            <ta e="T1056" id="Seg_12963" s="T1055">RUS:gram</ta>
            <ta e="T1059" id="Seg_12964" s="T1058">RUS:gram</ta>
            <ta e="T1062" id="Seg_12965" s="T1061">RUS:cult</ta>
            <ta e="T1064" id="Seg_12966" s="T1063">RUS:gram</ta>
            <ta e="T1072" id="Seg_12967" s="T1071">TURK:disc</ta>
            <ta e="T1091" id="Seg_12968" s="T1090">TURK:core</ta>
            <ta e="T1093" id="Seg_12969" s="T1092">RUS:gram</ta>
            <ta e="T1097" id="Seg_12970" s="T1096">RUS:gram</ta>
            <ta e="T1103" id="Seg_12971" s="T1102">RUS:gram</ta>
            <ta e="T1105" id="Seg_12972" s="T1104">RUS:gram</ta>
            <ta e="T1108" id="Seg_12973" s="T1107">TURK:disc</ta>
            <ta e="T1117" id="Seg_12974" s="T1116">TURK:gram(INDEF)</ta>
            <ta e="T1130" id="Seg_12975" s="T1129">RUS:gram</ta>
            <ta e="T1131" id="Seg_12976" s="T1130">RUS:cult</ta>
            <ta e="T1132" id="Seg_12977" s="T1131">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T588" id="Seg_12978" s="T587">inCdel</ta>
            <ta e="T728" id="Seg_12979" s="T727">Csub Vsub</ta>
            <ta e="T731" id="Seg_12980" s="T730">medCdel Csub</ta>
            <ta e="T805" id="Seg_12981" s="T804">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T16" id="Seg_12982" s="T15">dir:bare</ta>
            <ta e="T19" id="Seg_12983" s="T18">dir:bare</ta>
            <ta e="T23" id="Seg_12984" s="T22">dir:bare</ta>
            <ta e="T24" id="Seg_12985" s="T23">dir:bare</ta>
            <ta e="T267" id="Seg_12986" s="T266">dir:infl</ta>
            <ta e="T274" id="Seg_12987" s="T273">dir:infl</ta>
            <ta e="T365" id="Seg_12988" s="T364">dir:infl</ta>
            <ta e="T641" id="Seg_12989" s="T640">dir:bare</ta>
            <ta e="T728" id="Seg_12990" s="T727">dir:bare</ta>
            <ta e="T729" id="Seg_12991" s="T728">dir:bare</ta>
            <ta e="T731" id="Seg_12992" s="T730">dir:bare</ta>
            <ta e="T805" id="Seg_12993" s="T804">dir:bare</ta>
            <ta e="T856" id="Seg_12994" s="T855">dir:bare</ta>
            <ta e="T862" id="Seg_12995" s="T861">dir:bare</ta>
            <ta e="T884" id="Seg_12996" s="T883">dir:bare</ta>
            <ta e="T951" id="Seg_12997" s="T950">parad:infl</ta>
            <ta e="T975" id="Seg_12998" s="T974">parad:infl</ta>
            <ta e="T994" id="Seg_12999" s="T993">parad:infl</ta>
            <ta e="T1015" id="Seg_13000" s="T1014">parad:bare</ta>
            <ta e="T1062" id="Seg_13001" s="T1061">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T397" id="Seg_13002" s="T394">RUS:ext</ta>
            <ta e="T494" id="Seg_13003" s="T493">RUS:ext</ta>
            <ta e="T508" id="Seg_13004" s="T507">RUS:int</ta>
            <ta e="T539" id="Seg_13005" s="T538">RUS:int</ta>
            <ta e="T818" id="Seg_13006" s="T817">RUS:int</ta>
            <ta e="T1025" id="Seg_13007" s="T1022">RUS:int</ta>
            <ta e="T1031" id="Seg_13008" s="T1030">RUS:int</ta>
            <ta e="T1054" id="Seg_13009" s="T1053">RUS:int</ta>
            <ta e="T1058" id="Seg_13010" s="T1057">RUS:int</ta>
            <ta e="T1101" id="Seg_13011" s="T1099">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_13012" s="T1">…Две овцы и женщина.</ta>
            <ta e="T9" id="Seg_13013" s="T6">Жили две девушки.</ta>
            <ta e="T17" id="Seg_13014" s="T9">Жили две девушки, [у них] был стадо овец.</ta>
            <ta e="T21" id="Seg_13015" s="T17">[У них] был табун лошадей.</ta>
            <ta e="T25" id="Seg_13016" s="T21">[У них] было стадо коров.</ta>
            <ta e="T32" id="Seg_13017" s="T26">Тогда одна [девушка] смотрела, а другая…</ta>
            <ta e="T44" id="Seg_13018" s="T32">Тогда одна [девушка] вшей искала у Кодура, а другая свинец растопила.</ta>
            <ta e="T46" id="Seg_13019" s="T44">Потом принесла.</ta>
            <ta e="T49" id="Seg_13020" s="T46">И в ухо налила.</ta>
            <ta e="T57" id="Seg_13021" s="T50">В ухо налила, и он умер.</ta>
            <ta e="T63" id="Seg_13022" s="T57">Тогда его песня закончилась [=умерла].</ta>
            <ta e="T75" id="Seg_13023" s="T63">Девушки взяли овец, лошадей, коров и ушли к своему отцу.</ta>
            <ta e="T79" id="Seg_13024" s="T75">[Вот] вся сказка.</ta>
            <ta e="T86" id="Seg_13025" s="T79">Шли мышь, уголек и пузырь.</ta>
            <ta e="T94" id="Seg_13026" s="T87">[Дошли до] реки, река течет.</ta>
            <ta e="T98" id="Seg_13027" s="T94">Мышь положила ветки.</ta>
            <ta e="T101" id="Seg_13028" s="T98">Потом мышь перешла [через реку].</ta>
            <ta e="T103" id="Seg_13029" s="T101">Пузырь перешел.</ta>
            <ta e="T114" id="Seg_13030" s="T104">Уголек пошел и упал в воду и умер.</ta>
            <ta e="T121" id="Seg_13031" s="T115">А пузырь смеялся-смеялся и умер.</ta>
            <ta e="T126" id="Seg_13032" s="T122">Мышь одна осталась.</ta>
            <ta e="T129" id="Seg_13033" s="T126">Она шла-шла.</ta>
            <ta e="T132" id="Seg_13034" s="T129">Дети играют.</ta>
            <ta e="T137" id="Seg_13035" s="T133">Вода унесла, ветка на воде (?).</ta>
            <ta e="T143" id="Seg_13036" s="T138">"Дети, ваш отец вашей матери идет.</ta>
            <ta e="T149" id="Seg_13037" s="T143">Идите домой и скажите матери!"</ta>
            <ta e="T153" id="Seg_13038" s="T150">Они побежали.</ta>
            <ta e="T156" id="Seg_13039" s="T154">"Твой отец идет!"</ta>
            <ta e="T160" id="Seg_13040" s="T157">"Ну, идите позовите [его].</ta>
            <ta e="T164" id="Seg_13041" s="T160">Я сейчас мясо сварю".</ta>
            <ta e="T172" id="Seg_13042" s="T165">Они побежали: "Иди сюда, мать мясо варит.</ta>
            <ta e="T174" id="Seg_13043" s="T173">Ноги".</ta>
            <ta e="T181" id="Seg_13044" s="T175">Я не (хочу?) это есть.</ta>
            <ta e="T188" id="Seg_13045" s="T181">Пусть она зарежет корову, сварит [ее] и поставит на шкуру.</ta>
            <ta e="T191" id="Seg_13046" s="T188">Она зарезала корову.</ta>
            <ta e="T196" id="Seg_13047" s="T192">Мясо сварила, на шкуру положила.</ta>
            <ta e="T203" id="Seg_13048" s="T197">Тогда дети пошли: "Приходи, мясо готово!"</ta>
            <ta e="T205" id="Seg_13049" s="T204">Они пришли.</ta>
            <ta e="T208" id="Seg_13050" s="T206">"Где мой отец?</ta>
            <ta e="T213" id="Seg_13051" s="T209">Стала бить детей.</ta>
            <ta e="T222" id="Seg_13052" s="T214">А мышь сидит: "Не бей [их], я здесь, я пришел!"</ta>
            <ta e="T227" id="Seg_13053" s="T223">Сегодня идет снег.</ta>
            <ta e="T231" id="Seg_13054" s="T228">И ветер дует.</ta>
            <ta e="T237" id="Seg_13055" s="T232">Очень холодно на улице.</ta>
            <ta e="T241" id="Seg_13056" s="T238">Сколько лет ребенку?</ta>
            <ta e="T243" id="Seg_13057" s="T242">Три [года].</ta>
            <ta e="T249" id="Seg_13058" s="T244">Этой зимой было очень холодно.</ta>
            <ta e="T253" id="Seg_13059" s="T250">Люди замерзали.</ta>
            <ta e="T259" id="Seg_13060" s="T256">Я косила траву косой.</ta>
            <ta e="T271" id="Seg_13061" s="T260">Потом трава высохла, я собрала ее в копны, потом я взяла лошадь, перевезла [траву].</ta>
            <ta e="T274" id="Seg_13062" s="T272">Положила ее в зарод.</ta>
            <ta e="T286" id="Seg_13063" s="T277">В этом году черных ягод не было, а красные ягоды были.</ta>
            <ta e="T292" id="Seg_13064" s="T287">Люди много принесли домой ягод.</ta>
            <ta e="T303" id="Seg_13065" s="T293">Орехов много было, очень много орехов собрали.</ta>
            <ta e="T311" id="Seg_13066" s="T306">Три года не было же орехов.</ta>
            <ta e="T324" id="Seg_13067" s="T314">Орехи не надо трясти, они сами падают, а люди собирают.</ta>
            <ta e="T333" id="Seg_13068" s="T327">В этом году черемухи тоже не было.</ta>
            <ta e="T342" id="Seg_13069" s="T336">Пшеница в этом году была очень хорошая.</ta>
            <ta e="T351" id="Seg_13070" s="T343">И картошка была хорошая, много выросло, она большая была.</ta>
            <ta e="T360" id="Seg_13071" s="T354">Люди пекут много белого хлеба.</ta>
            <ta e="T365" id="Seg_13072" s="T361">В огороде лук весь погиб.</ta>
            <ta e="T367" id="Seg_13073" s="T366">А…</ta>
            <ta e="T375" id="Seg_13074" s="T370">А в лесу лук был хороший.</ta>
            <ta e="T385" id="Seg_13075" s="T378">Я его нарвала, нарезала, он высох.</ta>
            <ta e="T391" id="Seg_13076" s="T386">А сейчас я его варю с мясом, с картошкой.</ta>
            <ta e="T397" id="Seg_13077" s="T394">…А говорю следом. </ta>
            <ta e="T401" id="Seg_13078" s="T397">У камасинцев один человек умер.</ta>
            <ta e="T407" id="Seg_13079" s="T401">Они выкопали землю, потом сделали…</ta>
            <ta e="T413" id="Seg_13080" s="T410">Потом сделали гроб.</ta>
            <ta e="T418" id="Seg_13081" s="T413">Сделали крест, положили его в гроб.</ta>
            <ta e="T427" id="Seg_13082" s="T419">Положили ему трубку, положили табак.</ta>
            <ta e="T431" id="Seg_13083" s="T428">Ружье положили.</ta>
            <ta e="T435" id="Seg_13084" s="T432">Котел положили.</ta>
            <ta e="T441" id="Seg_13085" s="T436">Он сможет там готовить и есть.</ta>
            <ta e="T454" id="Seg_13086" s="T444">Потом этого человека отнесли [на кладбище], в землю положили, землёй засыпали.</ta>
            <ta e="T458" id="Seg_13087" s="T455">Потом пришли домой.</ta>
            <ta e="T465" id="Seg_13088" s="T459">Вечером привязали собак и лошадь.</ta>
            <ta e="T468" id="Seg_13089" s="T466">Потом пошли.</ta>
            <ta e="T474" id="Seg_13090" s="T469">"Ой, идёт!" — вбежали в дом.</ta>
            <ta e="T478" id="Seg_13091" s="T475">У дверей (косу?) положили.</ta>
            <ta e="T492" id="Seg_13092" s="T481">Собаку привязали чёрную, и глаз — один, два, три, четыре — четыре глаза.</ta>
            <ta e="T494" id="Seg_13093" s="T493">Четырехглазый. </ta>
            <ta e="T501" id="Seg_13094" s="T495">Косу положили так, чтобы дух зарезался.</ta>
            <ta e="T514" id="Seg_13095" s="T504">Один человек, поселенец, у нас болел, болел, потом умер.</ta>
            <ta e="T520" id="Seg_13096" s="T515">А мы пошли в Агинское.</ta>
            <ta e="T528" id="Seg_13097" s="T521">Бумаги взять в руку и на голову.</ta>
            <ta e="T534" id="Seg_13098" s="T529">Священник нам не дал.</ta>
            <ta e="T542" id="Seg_13099" s="T534">Мы купили четверть водки и вернулись домой.</ta>
            <ta e="T552" id="Seg_13100" s="T543">Тогда мать с отцом взяли эту водку, пошли пить.</ta>
            <ta e="T561" id="Seg_13101" s="T553">А мы, дети, мы тоже дома не останемся.</ta>
            <ta e="T564" id="Seg_13102" s="T562">"Пойдём!"</ta>
            <ta e="T570" id="Seg_13103" s="T565">Я говорю: "Ложись, спи.</ta>
            <ta e="T575" id="Seg_13104" s="T571">Я с вами лягу.</ta>
            <ta e="T582" id="Seg_13105" s="T576">Если он меня схватит, вы убежите".</ta>
            <ta e="T586" id="Seg_13106" s="T583">Утром я встала.</ta>
            <ta e="T596" id="Seg_13107" s="T587">Вот, (видишь?): он лежит, и мы поспали.</ta>
            <ta e="T604" id="Seg_13108" s="T599">Потом он умер, его помыли.</ta>
            <ta e="T609" id="Seg_13109" s="T605">Надели [на него] рубашку, надели штаны.</ta>
            <ta e="T613" id="Seg_13110" s="T610">Сапоги надели.</ta>
            <ta e="T616" id="Seg_13111" s="T614">Потом понесли.</ta>
            <ta e="T621" id="Seg_13112" s="T617">Засыпали его землёй.</ta>
            <ta e="T625" id="Seg_13113" s="T622">Пришли домой.</ta>
            <ta e="T631" id="Seg_13114" s="T625">Водку пили, хлеб ели, мясо ели.</ta>
            <ta e="T635" id="Seg_13115" s="T632">И по домам разошлись.</ta>
            <ta e="T644" id="Seg_13116" s="T638">Однажды я пришла в Агинское.</ta>
            <ta e="T650" id="Seg_13117" s="T645">Пришла в Пермяково, потом домой пришла.</ta>
            <ta e="T657" id="Seg_13118" s="T651">[?]</ta>
            <ta e="T669" id="Seg_13119" s="T658">[?]</ta>
            <ta e="T678" id="Seg_13120" s="T672">Пришла домой, никого не вижу.</ta>
            <ta e="T690" id="Seg_13121" s="T681">У меня голова болит, глаза болят, а у тебя что болит?</ta>
            <ta e="T698" id="Seg_13122" s="T690">У меня зубы болят и нос болит.</ta>
            <ta e="T715" id="Seg_13123" s="T701">У Ивановича почки болят, и ноги болят, и руки болят, он очень болен.</ta>
            <ta e="T726" id="Seg_13124" s="T718">Надо лечить этих троих людей.</ta>
            <ta e="T734" id="Seg_13125" s="T727">Надо доктора привести, пусть он их вылечит.</ta>
            <ta e="T744" id="Seg_13126" s="T737">Что-нибудь даст выпить или чем-нибудь помазать.</ta>
            <ta e="T756" id="Seg_13127" s="T747">Они сильно замёрзли, а теперь сочень сильно болеют.</ta>
            <ta e="T762" id="Seg_13128" s="T759">Лицо бабушки Марейки.</ta>
            <ta e="T770" id="Seg_13129" s="T763">У бабушки такое лицо, и глаза такие у бабушки.</ta>
            <ta e="T777" id="Seg_13130" s="T773">Жили жена и муж.</ta>
            <ta e="T781" id="Seg_13131" s="T778">У самого берега (реки?).</ta>
            <ta e="T785" id="Seg_13132" s="T782">Женщина сидела и пряла.</ta>
            <ta e="T793" id="Seg_13133" s="T786">Мужчина ходил на реку ловить рыбу.</ta>
            <ta e="T803" id="Seg_13134" s="T794">Один раз пошёл, тогда ничего не поймал.</ta>
            <ta e="T807" id="Seg_13135" s="T803">[В другой раз] одну рыбу поймал, красивую.</ta>
            <ta e="T810" id="Seg_13136" s="T808">"Я прошу…"</ta>
            <ta e="T819" id="Seg_13137" s="T813">Рыба красивая, говорит человеческим языком.</ta>
            <ta e="T823" id="Seg_13138" s="T820">"Отпусти меня в воду!"</ta>
            <ta e="T826" id="Seg_13139" s="T824">"Отпущу".</ta>
            <ta e="T832" id="Seg_13140" s="T827">Он пришёл домой и жене говорит.</ta>
            <ta e="T840" id="Seg_13141" s="T833">"Рыба была красивая, говорила как человек".</ta>
            <ta e="T848" id="Seg_13142" s="T843">Потом эта женщина стала ругаться.</ta>
            <ta e="T854" id="Seg_13143" s="T849">"Почему ты ничего не попросил?</ta>
            <ta e="T862" id="Seg_13144" s="T855">У нас корыта нет, [наше] корыто дырявое".</ta>
            <ta e="T867" id="Seg_13145" s="T863">Он пошёл опять к реке.</ta>
            <ta e="T874" id="Seg_13146" s="T868">Стал кричать: "Рыба, иди сюда!"</ta>
            <ta e="T880" id="Seg_13147" s="T874">Она приплыла: "Что тебе, старик, нужно?"</ta>
            <ta e="T884" id="Seg_13148" s="T881">"Мне корыто нужно.</ta>
            <ta e="T888" id="Seg_13149" s="T885">Жена моя ругается".</ta>
            <ta e="T891" id="Seg_13150" s="T888">"Ну, иди домой".</ta>
            <ta e="T903" id="Seg_13151" s="T894">Эта женщина давай его ругать: "Почему дом не попросил?</ta>
            <ta e="T908" id="Seg_13152" s="T904">Наш дом некрасивый".</ta>
            <ta e="T914" id="Seg_13153" s="T909">Он снова пошёл, стал кричать:</ta>
            <ta e="T916" id="Seg_13154" s="T914">"Рыба, рыба!</ta>
            <ta e="T918" id="Seg_13155" s="T916">Иди сюда!"</ta>
            <ta e="T924" id="Seg_13156" s="T919">Рыба приплыла: "Что тебе нужно?"</ta>
            <ta e="T927" id="Seg_13157" s="T925">"Дом нужен.</ta>
            <ta e="T932" id="Seg_13158" s="T928">Наш дом некрасивый".</ta>
            <ta e="T935" id="Seg_13159" s="T933">"Иди домой!"</ta>
            <ta e="T944" id="Seg_13160" s="T938">Потом опять эта женщина ругается:</ta>
            <ta e="T951" id="Seg_13161" s="T944">"Иди к рыбе, попроси, чтобы я стала купчихой".</ta>
            <ta e="T959" id="Seg_13162" s="T954">Потом рыба говорит: "Иди домой!"</ta>
            <ta e="T968" id="Seg_13163" s="T960">Он пришёл, а эта женщина опять ругается:</ta>
            <ta e="T975" id="Seg_13164" s="T968">"Иди к рыбе, проси, чтобы я дворянкой была".</ta>
            <ta e="T981" id="Seg_13165" s="T976">Он пришёл к рыбе, (позвал рыбу?).</ta>
            <ta e="T987" id="Seg_13166" s="T982">Рыба приплыла: "Что тебе нужно?"</ta>
            <ta e="T995" id="Seg_13167" s="T988">"Моя жена говорит, чтобы я была дворянкой [=что хочет быть дворянкой]".</ta>
            <ta e="T998" id="Seg_13168" s="T996">"Иди домой!"</ta>
            <ta e="T1009" id="Seg_13169" s="T1001">Потом муж пришёл домой, она снова стала ругаться.</ta>
            <ta e="T1015" id="Seg_13170" s="T1010">И хотела [его] побить.</ta>
            <ta e="T1025" id="Seg_13171" s="T1016">"Иди опять к рыбе, проси, чтобы я была государыней.</ta>
            <ta e="T1031" id="Seg_13172" s="T1026">Чтобы люди меня на руках носили.</ta>
            <ta e="T1038" id="Seg_13173" s="T1034">Потом он пошёл к реке.</ta>
            <ta e="T1043" id="Seg_13174" s="T1039">"Рыба, иди сюда!"</ta>
            <ta e="T1045" id="Seg_13175" s="T1043">Рыба приплыла.</ta>
            <ta e="T1050" id="Seg_13176" s="T1046">"Что тебе нужно, старик?"</ta>
            <ta e="T1062" id="Seg_13177" s="T1050">"Да моя жена говорит, чтобы я была царицей [=что хочет быть царицей].</ta>
            <ta e="T1068" id="Seg_13178" s="T1063">Чтобы люди меня [=её] на руках носили."</ta>
            <ta e="T1074" id="Seg_13179" s="T1069">"Иди домой, у тебя всё будет".</ta>
            <ta e="T1081" id="Seg_13180" s="T1077">Потом старик пошёл к реке.</ta>
            <ta e="T1086" id="Seg_13181" s="T1082">"Рыба, иди сюда!"</ta>
            <ta e="T1091" id="Seg_13182" s="T1086">Рыба приплыла: "Что тебе нужно?"</ta>
            <ta e="T1101" id="Seg_13183" s="T1092">"Да моя жена говорит, чтобы я была морской владычицей [=что хочет быть морской владычицей].</ta>
            <ta e="T1109" id="Seg_13184" s="T1102">И рыба чтобы мне [=ей] (всё приносила?)".</ta>
            <ta e="T1121" id="Seg_13185" s="T1110">Тогда рыба уплыла, ничего старику не сказала.</ta>
            <ta e="T1125" id="Seg_13186" s="T1122">Он пришёл домой.</ta>
            <ta e="T1133" id="Seg_13187" s="T1126">Женщина сидит, прядёт, и [перед ней] стоит её корыто.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_13188" s="T1">…Two sheep and a woman.</ta>
            <ta e="T9" id="Seg_13189" s="T6">There were two girls.</ta>
            <ta e="T17" id="Seg_13190" s="T9">There were two girls, [they] had a herd of sheep.</ta>
            <ta e="T21" id="Seg_13191" s="T17">[They] had a herd of horses.</ta>
            <ta e="T25" id="Seg_13192" s="T21">[They had] a herd of cows.</ta>
            <ta e="T32" id="Seg_13193" s="T26">One [girl] was looking and the other…</ta>
            <ta e="T44" id="Seg_13194" s="T32">One [girl] was searching for louses in Kodur's [head], and the other melted some lead.</ta>
            <ta e="T46" id="Seg_13195" s="T44">Then she brought [it].</ta>
            <ta e="T49" id="Seg_13196" s="T46">And poured in the ear.</ta>
            <ta e="T57" id="Seg_13197" s="T50">Poured in the ear, and that one died.</ta>
            <ta e="T63" id="Seg_13198" s="T57">Then his song ended [=died].</ta>
            <ta e="T75" id="Seg_13199" s="T63">Then the girls took the sheep, the horses, the cows and went to their father.</ta>
            <ta e="T79" id="Seg_13200" s="T75">The tale is over.</ta>
            <ta e="T86" id="Seg_13201" s="T79">A mouse, a coal and a bubble went.</ta>
            <ta e="T94" id="Seg_13202" s="T87">[They came] to a river. a river flows.</ta>
            <ta e="T98" id="Seg_13203" s="T94">The mouse put twigs.</ta>
            <ta e="T101" id="Seg_13204" s="T98">Then the mouse went.</ta>
            <ta e="T103" id="Seg_13205" s="T101">The bubble went.</ta>
            <ta e="T114" id="Seg_13206" s="T104">The coal went and fell in the water and died.</ta>
            <ta e="T121" id="Seg_13207" s="T115">And the bubble laughed, laughed and died [= burst].</ta>
            <ta e="T126" id="Seg_13208" s="T122">The mouse remained alone.</ta>
            <ta e="T129" id="Seg_13209" s="T126">Then it walked for some time.</ta>
            <ta e="T132" id="Seg_13210" s="T129">Children are playing.</ta>
            <ta e="T137" id="Seg_13211" s="T133">Water carried, a branch on the water (?).</ta>
            <ta e="T143" id="Seg_13212" s="T138">"Children, [your] mother's father is coming.</ta>
            <ta e="T149" id="Seg_13213" s="T143">Go home and tell mother!"</ta>
            <ta e="T153" id="Seg_13214" s="T150">They ran.</ta>
            <ta e="T156" id="Seg_13215" s="T154">"Your father's coming!"</ta>
            <ta e="T160" id="Seg_13216" s="T157">"Well, go and call [him].</ta>
            <ta e="T164" id="Seg_13217" s="T160">I'll now cook meat."</ta>
            <ta e="T172" id="Seg_13218" s="T165">They ran: "Come, [our] mother is cooking meat.</ta>
            <ta e="T174" id="Seg_13219" s="T173">Legs."</ta>
            <ta e="T181" id="Seg_13220" s="T175">"I don't (want?) eat it.</ta>
            <ta e="T188" id="Seg_13221" s="T181">Let her slaughter a cow and cook [it] and put [it] on the skin.</ta>
            <ta e="T191" id="Seg_13222" s="T188">She slaughtered a cow.</ta>
            <ta e="T196" id="Seg_13223" s="T192">She cooked meat, put it on the skin.</ta>
            <ta e="T203" id="Seg_13224" s="T197">Then the children went: "Come, the meat is ready!"</ta>
            <ta e="T205" id="Seg_13225" s="T204">They came.</ta>
            <ta e="T208" id="Seg_13226" s="T206">"Where is my father?"</ta>
            <ta e="T213" id="Seg_13227" s="T209">She started beating the children.</ta>
            <ta e="T222" id="Seg_13228" s="T214">The mouse is (sitting?): "Don't beat [them], I'm here, I've come!"</ta>
            <ta e="T227" id="Seg_13229" s="T223">Today it snows.</ta>
            <ta e="T231" id="Seg_13230" s="T228">And wind is coming [= blowing].</ta>
            <ta e="T237" id="Seg_13231" s="T232">It is very cold outside.</ta>
            <ta e="T241" id="Seg_13232" s="T238">How old is the child?</ta>
            <ta e="T243" id="Seg_13233" s="T242">Three [years].</ta>
            <ta e="T249" id="Seg_13234" s="T244">It was very cold this winter.</ta>
            <ta e="T253" id="Seg_13235" s="T250">People were freezing.</ta>
            <ta e="T259" id="Seg_13236" s="T256">I cut grass with a scythe.</ta>
            <ta e="T271" id="Seg_13237" s="T260">Then the grass dried, I gathered it in haycocks, then I took a horse, I carried [the grass].</ta>
            <ta e="T274" id="Seg_13238" s="T272">I laid [it] in zarods.</ta>
            <ta e="T286" id="Seg_13239" s="T277">This year, there's no black berries, but there were red berries.</ta>
            <ta e="T292" id="Seg_13240" s="T287">People brought home much berries.</ta>
            <ta e="T303" id="Seg_13241" s="T293">There were much nuts, people gathered very much nuts.</ta>
            <ta e="T311" id="Seg_13242" s="T306">There were no nuts for three years, you know.</ta>
            <ta e="T324" id="Seg_13243" s="T314">You don't have to shake nuts down, they fall themselves, and people collect them.</ta>
            <ta e="T333" id="Seg_13244" s="T327">There were no bird berries this year, too.</ta>
            <ta e="T342" id="Seg_13245" s="T336">The wheat was very good this year.</ta>
            <ta e="T351" id="Seg_13246" s="T343">And potatoes were good, there grew a lot of them, they were big.</ta>
            <ta e="T360" id="Seg_13247" s="T354">People bake much white bread.</ta>
            <ta e="T365" id="Seg_13248" s="T361">In the garden, the onions have all died.</ta>
            <ta e="T367" id="Seg_13249" s="T366">And…</ta>
            <ta e="T375" id="Seg_13250" s="T370">And in the forest the onions were good.</ta>
            <ta e="T385" id="Seg_13251" s="T378">I gathered it, chopped, and it was drying.</ta>
            <ta e="T391" id="Seg_13252" s="T386">Now I cook it with meat, mit potatoes.</ta>
            <ta e="T397" id="Seg_13253" s="T394">…And I speak after that.</ta>
            <ta e="T401" id="Seg_13254" s="T397">One person among the Kamassians died.</ta>
            <ta e="T407" id="Seg_13255" s="T401">They dug ground, then made…</ta>
            <ta e="T413" id="Seg_13256" s="T410">Then they built a coffin.</ta>
            <ta e="T418" id="Seg_13257" s="T413">They made a cross, put him in the coffin.</ta>
            <ta e="T427" id="Seg_13258" s="T419">They put him a pipe [in the grave], they put tobacco.</ta>
            <ta e="T431" id="Seg_13259" s="T428">They put the gun.</ta>
            <ta e="T435" id="Seg_13260" s="T432">They put a kettle.</ta>
            <ta e="T441" id="Seg_13261" s="T436">He'll cook and eat there.</ta>
            <ta e="T454" id="Seg_13262" s="T444">Then they carried this person [to the cemetery], put him into the ground, covered [him] with earth.</ta>
            <ta e="T458" id="Seg_13263" s="T455">Then they came home.</ta>
            <ta e="T465" id="Seg_13264" s="T459">In the evening, they attached the dogs and the horse.</ta>
            <ta e="T468" id="Seg_13265" s="T466">Then they went.</ta>
            <ta e="T474" id="Seg_13266" s="T469">"Oh, he's coming!" ‒ they run in the house.</ta>
            <ta e="T478" id="Seg_13267" s="T475">They put a (scythe?) at the door.</ta>
            <ta e="T492" id="Seg_13268" s="T481">The dog [that they have] attached is black, and eyes ‒ one, two, three, four ‒ [with] four eyes.</ta>
            <ta e="T494" id="Seg_13269" s="T493">Four-eyed.</ta>
            <ta e="T501" id="Seg_13270" s="T495">They put the scythe so that the ghost will cut [oneself].</ta>
            <ta e="T514" id="Seg_13271" s="T504">[There was] one person, a colonist, by us, he was ill for some time, then he died.</ta>
            <ta e="T520" id="Seg_13272" s="T515">We went to Aginskoye.</ta>
            <ta e="T528" id="Seg_13273" s="T521">To take papers for the hand and the head.</ta>
            <ta e="T534" id="Seg_13274" s="T529">The priest didn't give [them] to us.</ta>
            <ta e="T542" id="Seg_13275" s="T534">We bought a quarter of vodka and returned home.</ta>
            <ta e="T552" id="Seg_13276" s="T543">Our parents took this vodka and went to drink.</ta>
            <ta e="T561" id="Seg_13277" s="T553">And we, the children, we won't remain home, either.</ta>
            <ta e="T564" id="Seg_13278" s="T562">"Let's go!"</ta>
            <ta e="T570" id="Seg_13279" s="T565">I said: "Lay down, sleep.</ta>
            <ta e="T575" id="Seg_13280" s="T571">I'll lay down with you.</ta>
            <ta e="T582" id="Seg_13281" s="T576">If he grabs me, you'll run away."</ta>
            <ta e="T586" id="Seg_13282" s="T583">In the morning I got up.</ta>
            <ta e="T596" id="Seg_13283" s="T587">(I see?): he's lying, and we have slept.</ta>
            <ta e="T604" id="Seg_13284" s="T599">Then he died, [people] washed him.</ta>
            <ta e="T609" id="Seg_13285" s="T605">Put [him] a shirt and trousers on.</ta>
            <ta e="T613" id="Seg_13286" s="T610">Put him shoes on.</ta>
            <ta e="T616" id="Seg_13287" s="T614">Then carried [him].</ta>
            <ta e="T621" id="Seg_13288" s="T617">Covered him with earth.</ta>
            <ta e="T625" id="Seg_13289" s="T622">Came home.</ta>
            <ta e="T631" id="Seg_13290" s="T625">Drank vodka, ate bread, ate meat.</ta>
            <ta e="T635" id="Seg_13291" s="T632">And went home.</ta>
            <ta e="T644" id="Seg_13292" s="T638">Once I came to Aginskoye.</ta>
            <ta e="T650" id="Seg_13293" s="T645">I came to Permyakovo, then I came home.</ta>
            <ta e="T657" id="Seg_13294" s="T651">[?]</ta>
            <ta e="T669" id="Seg_13295" s="T658">[?]</ta>
            <ta e="T678" id="Seg_13296" s="T672">I came home, I didn't see anyone.</ta>
            <ta e="T690" id="Seg_13297" s="T681">My head is aching, my eyes are aching, and where are you hurt?</ta>
            <ta e="T698" id="Seg_13298" s="T690">My teeth are aching and my nose is aching.</ta>
            <ta e="T715" id="Seg_13299" s="T701">Ivanovich' kidney is aching and a leg is aching, and arms are aching, he's very ill.</ta>
            <ta e="T726" id="Seg_13300" s="T718">These three persons must be treated.</ta>
            <ta e="T734" id="Seg_13301" s="T727">[We] should bring a doctor, let him treat them.</ta>
            <ta e="T744" id="Seg_13302" s="T737">He'll give something to drink or he'll give something to oint.</ta>
            <ta e="T756" id="Seg_13303" s="T747">They were very cold, and now they are very ill.</ta>
            <ta e="T762" id="Seg_13304" s="T759">Granny Mareyka's face.</ta>
            <ta e="T770" id="Seg_13305" s="T763">Granny has such a face, and her eyes are like this.</ta>
            <ta e="T777" id="Seg_13306" s="T773">There lived a woman and a man.</ta>
            <ta e="T781" id="Seg_13307" s="T778">Just near a (sea?) shore.</ta>
            <ta e="T785" id="Seg_13308" s="T782">The woman was sitting and spinning.</ta>
            <ta e="T793" id="Seg_13309" s="T786">The man went to the river to catch fish.</ta>
            <ta e="T803" id="Seg_13310" s="T794">He went once, he didn't catch anything.</ta>
            <ta e="T807" id="Seg_13311" s="T803">[Next time] he caught one fish, beautiful.</ta>
            <ta e="T810" id="Seg_13312" s="T808">"I'm asking…"</ta>
            <ta e="T819" id="Seg_13313" s="T813">The fish is beautiful, it speaks in the human language.</ta>
            <ta e="T823" id="Seg_13314" s="T820">"Let me go in the water!"</ta>
            <ta e="T826" id="Seg_13315" s="T824">"I let."</ta>
            <ta e="T832" id="Seg_13316" s="T827">He came home and tells his wife.</ta>
            <ta e="T840" id="Seg_13317" s="T833">"The fish was beautiful, it spoke like a human."</ta>
            <ta e="T848" id="Seg_13318" s="T843">Then the woman started scolding.</ta>
            <ta e="T854" id="Seg_13319" s="T849">"Why didn't you ask for anything?</ta>
            <ta e="T862" id="Seg_13320" s="T855">We have no washtub, [our] washtub has holes."</ta>
            <ta e="T867" id="Seg_13321" s="T863">He went to the river again.</ta>
            <ta e="T874" id="Seg_13322" s="T868">He began to shout: "Fish, come here!"</ta>
            <ta e="T880" id="Seg_13323" s="T874">It came: "What do you need, old man?"</ta>
            <ta e="T884" id="Seg_13324" s="T881">"I need a washtub.</ta>
            <ta e="T888" id="Seg_13325" s="T885">My wife is scolding."</ta>
            <ta e="T891" id="Seg_13326" s="T888">"Well, go home."</ta>
            <ta e="T903" id="Seg_13327" s="T894">The woman scolded him: "Why didn't you ask for a house?</ta>
            <ta e="T908" id="Seg_13328" s="T904">Our house is not beautiful."</ta>
            <ta e="T914" id="Seg_13329" s="T909">He went [there] again and began to shout:</ta>
            <ta e="T916" id="Seg_13330" s="T914">"Fish, fish!</ta>
            <ta e="T918" id="Seg_13331" s="T916">Come here!"</ta>
            <ta e="T924" id="Seg_13332" s="T919">The fish came: "What do you need?"</ta>
            <ta e="T927" id="Seg_13333" s="T925">"I need a house.</ta>
            <ta e="T932" id="Seg_13334" s="T928">Our house isn't beautiful."</ta>
            <ta e="T935" id="Seg_13335" s="T933">"Go home!"</ta>
            <ta e="T944" id="Seg_13336" s="T938">Then this woman is scolding again:</ta>
            <ta e="T951" id="Seg_13337" s="T944">"Go to the fish, ask [her] to make me [like] a merchant's wife."</ta>
            <ta e="T959" id="Seg_13338" s="T954">Then the fish said: "Go home!"</ta>
            <ta e="T968" id="Seg_13339" s="T960">He came, and the woman is scolding again:</ta>
            <ta e="T975" id="Seg_13340" s="T968">"Go to the fish, ask [her] to make me a noblewoman."</ta>
            <ta e="T981" id="Seg_13341" s="T976">He came to the fish, (called the fish?).</ta>
            <ta e="T987" id="Seg_13342" s="T982">The fish came: "What do you need?"</ta>
            <ta e="T995" id="Seg_13343" s="T988">"My wife says that [she wants] to be a noblewoman."</ta>
            <ta e="T998" id="Seg_13344" s="T996">"Go home!"</ta>
            <ta e="T1009" id="Seg_13345" s="T1001">The man came home, she scolded him again.</ta>
            <ta e="T1015" id="Seg_13346" s="T1010">And wanted to beat [him].</ta>
            <ta e="T1025" id="Seg_13347" s="T1016">"Go to the fish, ask to make me a queen.</ta>
            <ta e="T1031" id="Seg_13348" s="T1026">So that people would carry me in the arms.</ta>
            <ta e="T1038" id="Seg_13349" s="T1034">Then he went to the river.</ta>
            <ta e="T1043" id="Seg_13350" s="T1039">"Fish, come here!"</ta>
            <ta e="T1045" id="Seg_13351" s="T1043">The fish came.</ta>
            <ta e="T1050" id="Seg_13352" s="T1046">"What do you want, old man?"</ta>
            <ta e="T1062" id="Seg_13353" s="T1050">"My wife says that [she wants] to be a queen.</ta>
            <ta e="T1068" id="Seg_13354" s="T1063">[And] people to carry [her] in their arms."</ta>
            <ta e="T1074" id="Seg_13355" s="T1069">"Go home, you'll have everything."</ta>
            <ta e="T1081" id="Seg_13356" s="T1077">Then the man went to the water.</ta>
            <ta e="T1086" id="Seg_13357" s="T1082">"Fish, come here!"</ta>
            <ta e="T1091" id="Seg_13358" s="T1086">The fish came: "What do you want?"</ta>
            <ta e="T1101" id="Seg_13359" s="T1092">"My wife says that [she wants] to be the mistress of the seas.</ta>
            <ta e="T1109" id="Seg_13360" s="T1102">And the fish to (bring [her] everything?)."</ta>
            <ta e="T1121" id="Seg_13361" s="T1110">Then the fish went in the water, she said nothing to the old man.</ta>
            <ta e="T1125" id="Seg_13362" s="T1122">He came home.</ta>
            <ta e="T1133" id="Seg_13363" s="T1126">The woman is sitting, spinning, and her washtub stands [before her].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_13364" s="T1">…Zwei Schafe und eine Frau.</ta>
            <ta e="T9" id="Seg_13365" s="T6">Es waren zwei Mädchen.</ta>
            <ta e="T17" id="Seg_13366" s="T9">Es waren zwei Mädchen, [sie] hatten eine Herde Schafen.</ta>
            <ta e="T21" id="Seg_13367" s="T17">[Sie hatten] eine Herde Pferden.</ta>
            <ta e="T25" id="Seg_13368" s="T21">[Sie hatten] eine Herde Kühe.</ta>
            <ta e="T32" id="Seg_13369" s="T26">Ein [Mädchen] sah, und das andere…</ta>
            <ta e="T44" id="Seg_13370" s="T32">Ein [Mädchen] suchte nach Lausen in Kodurs [Kopf] und das andere schmolz Blei.</ta>
            <ta e="T46" id="Seg_13371" s="T44">Dann brachte sie [es].</ta>
            <ta e="T49" id="Seg_13372" s="T46">Und goss es ins Ohr.</ta>
            <ta e="T57" id="Seg_13373" s="T50">Goss es ins Ohr, das andere starb.</ta>
            <ta e="T63" id="Seg_13374" s="T57">Dann endet [=starb] sein Lied.</ta>
            <ta e="T75" id="Seg_13375" s="T63">Dann nahmen die Mädchen das Schaf, die Pferde, die Kühe und gingen zu ihrem Vater. </ta>
            <ta e="T79" id="Seg_13376" s="T75">Das Märchen ist vorbei.</ta>
            <ta e="T86" id="Seg_13377" s="T79">Eine Maus, ein Stück Kohle und eine Blase gingen. </ta>
            <ta e="T94" id="Seg_13378" s="T87">[Sie kamen] zu einem Fluss, ein Fluss fließt.</ta>
            <ta e="T98" id="Seg_13379" s="T94">Die Maus legt Zweige.</ta>
            <ta e="T101" id="Seg_13380" s="T98">Dann ging die Maus.</ta>
            <ta e="T103" id="Seg_13381" s="T101">Die Blase ging.</ta>
            <ta e="T114" id="Seg_13382" s="T104">Die Kohle ging und fiel ins Wasser und starb.</ta>
            <ta e="T121" id="Seg_13383" s="T115">Und die Blase lachte, lachte und starb [= platzte].</ta>
            <ta e="T126" id="Seg_13384" s="T122">Die Maus blieb alleine.</ta>
            <ta e="T129" id="Seg_13385" s="T126">Dann lief sie einiger Zeit.</ta>
            <ta e="T132" id="Seg_13386" s="T129">Kinder spielen.</ta>
            <ta e="T137" id="Seg_13387" s="T133">Wasser floss, ein Ast im Wasser (?).</ta>
            <ta e="T143" id="Seg_13388" s="T138">„Kinder, [euer] Mutters Vater kommt.</ta>
            <ta e="T149" id="Seg_13389" s="T143">Geht nach Hause und erzählt Mutter!“</ta>
            <ta e="T153" id="Seg_13390" s="T150">Sie liefen.</ta>
            <ta e="T156" id="Seg_13391" s="T154">„Dein Vater kommt!“</ta>
            <ta e="T160" id="Seg_13392" s="T157">„Na, geht und ruft [ihn].</ta>
            <ta e="T164" id="Seg_13393" s="T160">Ich koche gleich Fleisch.“</ta>
            <ta e="T172" id="Seg_13394" s="T165">Sie liefen: „Komm, [unsere] Mutter kocht Fleisch.</ta>
            <ta e="T174" id="Seg_13395" s="T173">Beine.“</ta>
            <ta e="T181" id="Seg_13396" s="T175">„Ich (will?) es nicht essen.</ta>
            <ta e="T188" id="Seg_13397" s="T181">Lass sie eine Kuh schlachten und [sie] kochen und lege [es] auf die Haut.</ta>
            <ta e="T191" id="Seg_13398" s="T188">Sie schlachtete eine Kuh.</ta>
            <ta e="T196" id="Seg_13399" s="T192">Sie kochte Fleisch, legte es auf die Haut.</ta>
            <ta e="T203" id="Seg_13400" s="T197">Dann gingen die Kinder: „Komm, das Fleisch ist fertig!“</ta>
            <ta e="T205" id="Seg_13401" s="T204">Sie kamen.</ta>
            <ta e="T208" id="Seg_13402" s="T206">„Wo ist mein Vater?“</ta>
            <ta e="T213" id="Seg_13403" s="T209">Sie fing an, die Kinder zu schlagen.</ta>
            <ta e="T222" id="Seg_13404" s="T214">Die Maus (sitzt?): „Schlag [sie] nicht, ich bin hier, ich komme!“</ta>
            <ta e="T227" id="Seg_13405" s="T223">Heute schneit es.</ta>
            <ta e="T231" id="Seg_13406" s="T228">Und Wind kommt [= weht].</ta>
            <ta e="T237" id="Seg_13407" s="T232">Es ist draußen sehr kalt.</ta>
            <ta e="T241" id="Seg_13408" s="T238">Wie alt ist das Kind?</ta>
            <ta e="T243" id="Seg_13409" s="T242">Drei [Jahre].</ta>
            <ta e="T249" id="Seg_13410" s="T244">Diesen Winter war es sehr kalt. </ta>
            <ta e="T253" id="Seg_13411" s="T250">Leute froren.</ta>
            <ta e="T259" id="Seg_13412" s="T256">Ich mähte Gras mit einer Sense.</ta>
            <ta e="T271" id="Seg_13413" s="T260">Dann trocknete das Gras, ich sammelte es in Heuhaufen, dann nahm ich ein Pferd, ich fahrte [das Gras].</ta>
            <ta e="T274" id="Seg_13414" s="T272">Ich legte [es] in Zarod.</ta>
            <ta e="T286" id="Seg_13415" s="T277">Dieses Jahr gibt es keine schwarze Beeren, aber es waren rote Beeren.</ta>
            <ta e="T292" id="Seg_13416" s="T287">Leute brachten viele Beeren nach Hause. </ta>
            <ta e="T303" id="Seg_13417" s="T293">Es waren viele Nüsse, Leute sammelten viele Nüsse.</ta>
            <ta e="T311" id="Seg_13418" s="T306">Es waren drei Jahre lang keine Nüsse, weißt du.</ta>
            <ta e="T324" id="Seg_13419" s="T314">Man muss die Nüsse nicht herunter schütteln, sie fallen von alleine, und Leute sammeln sie.</ta>
            <ta e="T333" id="Seg_13420" s="T327">Es waren dieses Jahr auch keine Vogelbeeren.</ta>
            <ta e="T342" id="Seg_13421" s="T336">Der Weizen war diesen Jahr sehr gut.</ta>
            <ta e="T351" id="Seg_13422" s="T343">Und Kartoffeln waren gut, es wuchsen viele, sie waren groß.</ta>
            <ta e="T360" id="Seg_13423" s="T354">Leute backen viel Weißbrot.</ta>
            <ta e="T365" id="Seg_13424" s="T361">Im Garten sind alle Zwiebel gestorben.</ta>
            <ta e="T367" id="Seg_13425" s="T366">Und…</ta>
            <ta e="T375" id="Seg_13426" s="T370">Und im Wald waren die Zwiebel gut.</ta>
            <ta e="T385" id="Seg_13427" s="T378">Ich sammelte es, hackte es, es starb.</ta>
            <ta e="T391" id="Seg_13428" s="T386">Nun koche ich sie mit Fleisch, mit Kartoffel.</ta>
            <ta e="T397" id="Seg_13429" s="T394">…Und danach rede ich.</ta>
            <ta e="T401" id="Seg_13430" s="T397">Eine Person unter den Kamassier starb.</ta>
            <ta e="T407" id="Seg_13431" s="T401">Sie hoben Erde aus, dann machten…</ta>
            <ta e="T413" id="Seg_13432" s="T410">Dann bauten sie einen Sarg.</ta>
            <ta e="T418" id="Seg_13433" s="T413">Sie machten ein Kreuz, legten ihn im Sarg.</ta>
            <ta e="T427" id="Seg_13434" s="T419">Sie legten ihm eine Pfeife [ins Grab], sie legten Tabak.</ta>
            <ta e="T431" id="Seg_13435" s="T428">Sie legten das Gewehr.</ta>
            <ta e="T435" id="Seg_13436" s="T432">Sie legten einen Kessel.</ta>
            <ta e="T441" id="Seg_13437" s="T436">Er wird dort kochen und essen.</ta>
            <ta e="T454" id="Seg_13438" s="T444">Dann trugen sie diese Person [zum Friedhof], legte ihn in die Erde, bedeckten [ihn] mit Erde.</ta>
            <ta e="T458" id="Seg_13439" s="T455">Dann kamen sie nach Hause.</ta>
            <ta e="T465" id="Seg_13440" s="T459">Am Abend spannten sie die Hunde und Pferde an.</ta>
            <ta e="T468" id="Seg_13441" s="T466">Dann fuhren sie los.</ta>
            <ta e="T474" id="Seg_13442" s="T469">"Oh, er kommt!" ‒ sie rannten ins Haus.</ta>
            <ta e="T478" id="Seg_13443" s="T475">Sie hängten eine (Sense?) an die Tür.</ta>
            <ta e="T492" id="Seg_13444" s="T481">Der Hund, [den sie] angespannt haben, ist schwarz, und die Augen ‒ eins, zwei, drei, vier ‒ [mit] vier Augen.</ta>
            <ta e="T494" id="Seg_13445" s="T493">Vieräugig.</ta>
            <ta e="T501" id="Seg_13446" s="T495">Sie haben die Sense so aufgehängt, dass der Geist sich selbst schneiden wird.</ta>
            <ta e="T514" id="Seg_13447" s="T504">Es gab eine Person, einen Kolonisten, bei uns, er war eine Zeit krank, dann starb er.</ta>
            <ta e="T520" id="Seg_13448" s="T515">Wir fuhren nach Aginskoye.</ta>
            <ta e="T528" id="Seg_13449" s="T521">Um die Blätter für die Hände und den Kopf zu nehmen.</ta>
            <ta e="T534" id="Seg_13450" s="T529">Der Priester gab sie uns nicht.</ta>
            <ta e="T542" id="Seg_13451" s="T534">Wir kauften ein Viertel Wodka und kamen wieder nach Hause.</ta>
            <ta e="T552" id="Seg_13452" s="T543">Unsere Eltern nahmen den Wodka und gingen um zu trinken.</ta>
            <ta e="T561" id="Seg_13453" s="T553">Und wir, die Kinder, blieben auch nicht zuhause.</ta>
            <ta e="T564" id="Seg_13454" s="T562">"Lass uns gehen!"</ta>
            <ta e="T570" id="Seg_13455" s="T565">Ich sagte: "Leg dich hin, schlaf.</ta>
            <ta e="T575" id="Seg_13456" s="T571">Ich lege mich zu dir.</ta>
            <ta e="T582" id="Seg_13457" s="T576">Wenn er mich greift, läufst du weg."</ta>
            <ta e="T586" id="Seg_13458" s="T583">Am morgen stand ich auf.</ta>
            <ta e="T596" id="Seg_13459" s="T587">(Ich sehe?): Er liegt dort und wir haben geschlafen.</ta>
            <ta e="T604" id="Seg_13460" s="T599">Dann starb er, [Menschen] wuschen ihn.</ta>
            <ta e="T609" id="Seg_13461" s="T605">Zogen [ihm] ein Hemd und eine Hose an.</ta>
            <ta e="T613" id="Seg_13462" s="T610">Zogen ihm Schuhe an.</ta>
            <ta e="T616" id="Seg_13463" s="T614">Trugen [ihn] dann.</ta>
            <ta e="T621" id="Seg_13464" s="T617">Bedeckten ihn mit Erde.</ta>
            <ta e="T625" id="Seg_13465" s="T622">Kamen nach Hause.</ta>
            <ta e="T631" id="Seg_13466" s="T625">Tranken Wodka, aßen Brot und Fleisch.</ta>
            <ta e="T635" id="Seg_13467" s="T632">Und gingen nach Hause.</ta>
            <ta e="T644" id="Seg_13468" s="T638">Einst kam ich nach Aginskoye. </ta>
            <ta e="T650" id="Seg_13469" s="T645">Ich kam nach Permyakovo, dann kam ich nach Hause.</ta>
            <ta e="T657" id="Seg_13470" s="T651">[?]</ta>
            <ta e="T669" id="Seg_13471" s="T658">[?]</ta>
            <ta e="T678" id="Seg_13472" s="T672">Ich kam nach Hause, ich sah niemanden.</ta>
            <ta e="T690" id="Seg_13473" s="T681">Mein Kopf tut weh, meine Augen tun weh, und wo bist du verletzt?</ta>
            <ta e="T698" id="Seg_13474" s="T690">Meine Zähne tun weh und meine Nase tut weh.</ta>
            <ta e="T715" id="Seg_13475" s="T701">Ivanovichs Niere tut weh und ein Beint ut weh und Arme tun weh, er ist sehr krank.</ta>
            <ta e="T726" id="Seg_13476" s="T718">Diese drei Personen müssen behandelt werden.</ta>
            <ta e="T734" id="Seg_13477" s="T727">[Wir] sollten einen Doktor holen, ihn sie behandeln lassen.</ta>
            <ta e="T744" id="Seg_13478" s="T737">Er wird ihnen etwas zu trinken geben oder er gibt ihnen etwas zum einsalben.</ta>
            <ta e="T756" id="Seg_13479" s="T747">Sie waren sehr kalt und jetzt sind sie sehr krank.</ta>
            <ta e="T762" id="Seg_13480" s="T759">Großmutter Marejkas Gesicht.</ta>
            <ta e="T770" id="Seg_13481" s="T763">Großmutter hat so ein Gesicht und ihre Augen sind so.</ta>
            <ta e="T777" id="Seg_13482" s="T773">Es lebte einmal eine Frau und ein Mann.</ta>
            <ta e="T781" id="Seg_13483" s="T778">Direkt am Wasser.</ta>
            <ta e="T785" id="Seg_13484" s="T782">Die Frauß saß und sponn.</ta>
            <ta e="T793" id="Seg_13485" s="T786">Der Mann ging zum Fluss um Fische zu fangen.</ta>
            <ta e="T803" id="Seg_13486" s="T794">Er ging einmal, er fing nichts.</ta>
            <ta e="T807" id="Seg_13487" s="T803">[Nächstes Mal] ging er einen Fisch, schön.</ta>
            <ta e="T810" id="Seg_13488" s="T808">"Ich frage…"</ta>
            <ta e="T819" id="Seg_13489" s="T813">Der Fishc ist schön, er spricht die menschliche Sprache.</ta>
            <ta e="T823" id="Seg_13490" s="T820">"Lass mich ins Wasser zurück."</ta>
            <ta e="T826" id="Seg_13491" s="T824">"Ich lasse dich."</ta>
            <ta e="T832" id="Seg_13492" s="T827">Er kam nach Hause und erzählte es seiner Frau.</ta>
            <ta e="T840" id="Seg_13493" s="T833">"Der Fisch war schön, er sprach wie ein Mensch."</ta>
            <ta e="T848" id="Seg_13494" s="T843">Dann begann die Frau zu schimpfen.</ta>
            <ta e="T854" id="Seg_13495" s="T849">"Warum hast du nicht um etwas gebeten?</ta>
            <ta e="T862" id="Seg_13496" s="T855">Wir haben keine Waschschüssel, [unsere] Waschschüssel hat Löchter."</ta>
            <ta e="T867" id="Seg_13497" s="T863">Er ging wieder zum Fluss.</ta>
            <ta e="T874" id="Seg_13498" s="T868">Er begann zu rufen: "Fisch, komm her!"</ta>
            <ta e="T880" id="Seg_13499" s="T874">Er kam: "Was brauchst du, alter Mann?"</ta>
            <ta e="T884" id="Seg_13500" s="T881">"Ich brauche eine Waschschüssel.</ta>
            <ta e="T888" id="Seg_13501" s="T885">Meine Frau schimpft."</ta>
            <ta e="T891" id="Seg_13502" s="T888">"Nun, geh nach Hause."</ta>
            <ta e="T903" id="Seg_13503" s="T894">Die Frau schimpfte mit ihm: "Warum hast du nicht um ein Haus gebeten?</ta>
            <ta e="T908" id="Seg_13504" s="T904">Unser Haus ist nicht schön."</ta>
            <ta e="T914" id="Seg_13505" s="T909">Er ging wieder dorthin und begann zu schreien:</ta>
            <ta e="T916" id="Seg_13506" s="T914">"Fisch, fisch!</ta>
            <ta e="T918" id="Seg_13507" s="T916">Komm her!"</ta>
            <ta e="T924" id="Seg_13508" s="T919">Der Fisch kam: "Was brauchst du?"</ta>
            <ta e="T927" id="Seg_13509" s="T925">"Ich brauche ein Haus.</ta>
            <ta e="T932" id="Seg_13510" s="T928">Unser Haus ist nicht schön."</ta>
            <ta e="T935" id="Seg_13511" s="T933">"Geh nach Hause!"</ta>
            <ta e="T944" id="Seg_13512" s="T938">Dann schimpfte diese Frau wieder:</ta>
            <ta e="T951" id="Seg_13513" s="T944">"Geh zum Fisch, bitte ihn mich wie eine Kaufmannsfrau zu machen."</ta>
            <ta e="T959" id="Seg_13514" s="T954">Dann sagte der Fisch: "Geh nach Hause!"</ta>
            <ta e="T968" id="Seg_13515" s="T960">Er kam, und die Frau schimpfte wieder:</ta>
            <ta e="T975" id="Seg_13516" s="T968">"Geh zum Fisch, bitte ihn mich zu einer Edelfrau zu machen."</ta>
            <ta e="T981" id="Seg_13517" s="T976">Er ging zum Fisch, (rief den Fisch?).</ta>
            <ta e="T987" id="Seg_13518" s="T982">Der Fisch kam: "Was brauchst du?"</ta>
            <ta e="T995" id="Seg_13519" s="T988">"Meine Frau sagt, dass [sie] eine Edelfrau sein möchte."</ta>
            <ta e="T998" id="Seg_13520" s="T996">"Geh nach Hause!"</ta>
            <ta e="T1009" id="Seg_13521" s="T1001">Dann kam der Mann nach Hause, sie schimpfte wieder mit ihm.</ta>
            <ta e="T1015" id="Seg_13522" s="T1010">Und wollte [ihn] schlagen.</ta>
            <ta e="T1025" id="Seg_13523" s="T1016">"Geh zum Fisch, bitte ihn mich zu einer Königin zu machen.</ta>
            <ta e="T1031" id="Seg_13524" s="T1026">Damit die Leute mich in den Armen tragen.</ta>
            <ta e="T1038" id="Seg_13525" s="T1034">Dann ging er zum Fluss.</ta>
            <ta e="T1043" id="Seg_13526" s="T1039">"Fisch, komm her!"</ta>
            <ta e="T1045" id="Seg_13527" s="T1043">Der Fisch kam.</ta>
            <ta e="T1050" id="Seg_13528" s="T1046">"Was willst du, alter Mann?"</ta>
            <ta e="T1062" id="Seg_13529" s="T1050">"Meine Frau sagt, dass [sie] eine Königin sein [möchte].</ta>
            <ta e="T1068" id="Seg_13530" s="T1063">Und Leute [sie] in ihren Armen tragen."</ta>
            <ta e="T1074" id="Seg_13531" s="T1069">"Geh nach Hause, du wirst alles haben."</ta>
            <ta e="T1081" id="Seg_13532" s="T1077">Der Mann ging zum Wasser.</ta>
            <ta e="T1086" id="Seg_13533" s="T1082">"Fisch, komm her!"</ta>
            <ta e="T1091" id="Seg_13534" s="T1086">Der Fisch kam: "Was willst du?"</ta>
            <ta e="T1101" id="Seg_13535" s="T1092">"Meine Frau sagst, dass sie die Herrscherin des Meeres sein möchte.</ta>
            <ta e="T1109" id="Seg_13536" s="T1102">Und dass der Fisch [ihr] (alles bringt?)."</ta>
            <ta e="T1121" id="Seg_13537" s="T1110">Dann ging der Fisch ins Wasser, sagte nichts zum alten Mann.</ta>
            <ta e="T1125" id="Seg_13538" s="T1122">Er kam nach Hause.</ta>
            <ta e="T1133" id="Seg_13539" s="T1126">Die Frau sitzt dort, spinnt und ihre Waschschüssel steht [vor ihr].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_13540" s="T1">[AAV] continued from PKZ_196X_SU0222, tape AEDKL SU0222</ta>
            <ta e="T21" id="Seg_13541" s="T17">[GVY:] Or 'they took a herd of horses'?</ta>
            <ta e="T44" id="Seg_13542" s="T32">[GVY:] Unclear. [AAV] One was searching for louses in K.'s head, while the other melted some lead and poured into his ear?</ta>
            <ta e="T86" id="Seg_13543" s="T79">[KlT:] Donner, Tale 1</ta>
            <ta e="T94" id="Seg_13544" s="T87">[GVY:] The syntax is unclear.</ta>
            <ta e="T137" id="Seg_13545" s="T133">[GVY:] Unclear. The mouse was sitting on a branch and the water was carrying it?</ta>
            <ta e="T153" id="Seg_13546" s="T150">[GVY:] sobiʔi.</ta>
            <ta e="T174" id="Seg_13547" s="T173">[GVY:] meat from legs? In the original tale, the woman was cooking fish.</ta>
            <ta e="T274" id="Seg_13548" s="T272">[GVY:] Зарод (Russ.) - an oblong heap of hay.</ta>
            <ta e="T351" id="Seg_13549" s="T343">[GVY:] toltanoʔ is pronounced in a Russian manner, with the reduction of vowels.</ta>
            <ta e="T365" id="Seg_13550" s="T361">[GVY:] köbergən - a kind of onion (cf. Khakas köbIrgen)</ta>
            <ta e="T427" id="Seg_13551" s="T419">[GVY:] ambiʔi = embiʔi; PKZ corrects herself later.</ta>
            <ta e="T478" id="Seg_13552" s="T475">[GVY:] šakku = šapku 'scythe'?</ta>
            <ta e="T492" id="Seg_13553" s="T481">[GVY:] Probably with two spots above the eyes.</ta>
            <ta e="T501" id="Seg_13554" s="T495">[GVY:] ambiʔi = embiʔi. </ta>
            <ta e="T528" id="Seg_13555" s="T521">[GVY:] pieces of paper with prayers that were put in the hand and on the head of the deceased.</ta>
            <ta e="T561" id="Seg_13556" s="T553">[GVY:] with the deceased. Apparently, PKZ was there with her younger brothers or sisters.</ta>
            <ta e="T657" id="Seg_13557" s="T651">[GVY:] These two sentences are unclear.</ta>
            <ta e="T777" id="Seg_13558" s="T773">[GVY:] Pushkin's tale about a fisherman and a goldfish.</ta>
            <ta e="T810" id="Seg_13559" s="T808">[WNB] the verbal ending is wrong</ta>
            <ta e="T951" id="Seg_13560" s="T944">[GVY:] kupčixăjzi - apparently the Russian INSTR купчихой + the Kamassian INSTR suffix -zi.</ta>
            <ta e="T981" id="Seg_13561" s="T976">[GVY:] The syntax of the second part is unclear.</ta>
            <ta e="T995" id="Seg_13562" s="T988">[GVY:] This sentence is syntactically coherent, cf. a similar sentence below.</ta>
            <ta e="T1062" id="Seg_13563" s="T1050">[GVY:] tsaritsazzʼi</ta>
            <ta e="T1081" id="Seg_13564" s="T1077">[GVY:] a part of the story is missing.</ta>
            <ta e="T1109" id="Seg_13565" s="T1102">[AAV] In Pushkin's tale "быть на посылках" = "run errands".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
