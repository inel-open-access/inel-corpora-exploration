<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID65481B6C-22BA-9F60-3410-D5E57AB69CE3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0208.wav" />
         <referenced-file url="PKZ_1964_SU0208.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0208\PKZ_1964_SU0208.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1257</ud-information>
            <ud-information attribute-name="# HIAT:w">818</ud-information>
            <ud-information attribute-name="# e">806</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">6</ud-information>
            <ud-information attribute-name="# HIAT:u">199</ud-information>
            <ud-information attribute-name="# sc">18</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.055" type="appl" />
         <tli id="T1" time="0.795" type="appl" />
         <tli id="T2" time="1.535" type="appl" />
         <tli id="T3" time="2.275" type="appl" />
         <tli id="T4" time="8.166622968604933" />
         <tli id="T830" time="8.3289672264537" type="intp" />
         <tli id="T831" time="8.491311484302468" type="intp" />
         <tli id="T832" time="8.653655742151233" type="intp" />
         <tli id="T5" time="8.816" type="appl" />
         <tli id="T6" time="9.392" type="appl" />
         <tli id="T7" time="9.969" type="appl" />
         <tli id="T8" time="10.545" type="appl" />
         <tli id="T9" time="11.121" type="appl" />
         <tli id="T10" time="11.698" type="appl" />
         <tli id="T11" time="12.274" type="appl" />
         <tli id="T12" time="14.41325621071336" />
         <tli id="T833" time="15.10217080714224" type="intp" />
         <tli id="T834" time="15.79108540357112" type="intp" />
         <tli id="T13" time="16.48" type="appl" />
         <tli id="T14" time="23.299875326754485" />
         <tli id="T835" time="23.348937663377242" type="intp" />
         <tli id="T836" time="23.398" type="appl" />
         <tli id="T15" time="24.174" type="appl" />
         <tli id="T16" time="24.875" type="appl" />
         <tli id="T17" time="25.577" type="appl" />
         <tli id="T18" time="26.279" type="appl" />
         <tli id="T19" time="26.98" type="appl" />
         <tli id="T20" time="29.119844185197028" />
         <tli id="T837" time="29.35922945679802" type="intp" />
         <tli id="T838" time="29.59861472839901" type="intp" />
         <tli id="T21" time="29.838" type="appl" />
         <tli id="T22" time="30.661" type="appl" />
         <tli id="T23" time="31.484" type="appl" />
         <tli id="T24" time="32.307" type="appl" />
         <tli id="T25" time="34.35981614709374" />
         <tli id="T26" time="34.828" type="appl" />
         <tli id="T27" time="35.48" type="appl" />
         <tli id="T28" time="36.131" type="appl" />
         <tli id="T29" time="36.782" type="appl" />
         <tli id="T30" time="37.433" type="appl" />
         <tli id="T31" time="38.085" type="appl" />
         <tli id="T32" time="40.86644799799857" />
         <tli id="T839" time="40.992223998999286" type="intp" />
         <tli id="T840" time="41.118" type="appl" />
         <tli id="T33" time="41.487" type="appl" />
         <tli id="T34" time="42.033" type="appl" />
         <tli id="T35" time="42.578" type="appl" />
         <tli id="T36" time="43.123" type="appl" />
         <tli id="T37" time="43.669" type="appl" />
         <tli id="T38" time="45.76642177916153" />
         <tli id="T841" time="45.99294785277436" type="intp" />
         <tli id="T842" time="46.21947392638718" type="intp" />
         <tli id="T39" time="46.446" type="appl" />
         <tli id="T40" time="47.25" type="appl" />
         <tli id="T41" time="48.055" type="appl" />
         <tli id="T42" time="48.859" type="appl" />
         <tli id="T43" time="49.87306647194572" />
         <tli id="T44" time="50.543" type="appl" />
         <tli id="T45" time="51.423" type="appl" />
         <tli id="T46" time="52.303" type="appl" />
         <tli id="T47" time="54.25304303551588" />
         <tli id="T843" time="54.564362023677255" type="intp" />
         <tli id="T844" time="54.87568101183862" type="intp" />
         <tli id="T48" time="55.187" type="appl" />
         <tli id="T49" time="56.249" type="appl" />
         <tli id="T50" time="59.333015853537894" />
         <tli id="T845" time="59.36676189015342" type="intp" />
         <tli id="T846" time="59.400507926768945" type="intp" />
         <tli id="T51" time="59.43425396338448" type="intp" />
         <tli id="T52" time="59.468" type="appl" />
         <tli id="T53" time="59.665" type="appl" />
         <tli id="T54" time="59.861" type="appl" />
         <tli id="T55" time="60.058" type="appl" />
         <tli id="T56" time="60.254" type="appl" />
         <tli id="T57" time="60.45" type="appl" />
         <tli id="T58" time="70.90628726047518" />
         <tli id="T847" time="71.04742980838014" type="intp" />
         <tli id="T848" time="71.1885723562851" type="intp" />
         <tli id="T849" time="71.32971490419007" type="intp" />
         <tli id="T850" time="71.47085745209503" type="intp" />
         <tli id="T59" time="71.612" type="appl" />
         <tli id="T60" time="72.398" type="appl" />
         <tli id="T61" time="73.183" type="appl" />
         <tli id="T62" time="74.066" type="appl" />
         <tli id="T63" time="74.95" type="appl" />
         <tli id="T64" time="75.833" type="appl" />
         <tli id="T65" time="76.667" type="appl" />
         <tli id="T66" time="77.83291686405111" />
         <tli id="T67" time="78.683" type="appl" />
         <tli id="T68" time="79.425" type="appl" />
         <tli id="T69" time="80.167" type="appl" />
         <tli id="T70" time="80.908" type="appl" />
         <tli id="T71" time="81.65" type="appl" />
         <tli id="T72" time="82.392" type="appl" />
         <tli id="T73" time="83.134" type="appl" />
         <tli id="T851" time="83.797" type="appl" />
         <tli id="T74" time="85.11287791035036" />
         <tli id="T852" time="85.53693895517517" type="intp" />
         <tli id="T75" time="85.961" type="appl" />
         <tli id="T76" time="86.799" type="appl" />
         <tli id="T77" time="87.85952988019952" />
         <tli id="T78" time="88.503" type="appl" />
         <tli id="T79" time="89.15" type="appl" />
         <tli id="T80" time="93.66616547665252" />
         <tli id="T853" time="94.529" type="appl" />
         <tli id="T854" time="95.091" type="appl" />
         <tli id="T81" time="95.248" type="appl" />
         <tli id="T82" time="96.606" type="appl" />
         <tli id="T83" time="97.964" type="appl" />
         <tli id="T84" time="99.57946716902197" />
         <tli id="T85" time="101.09" type="appl" />
         <tli id="T855" time="102.328" type="appl" />
         <tli id="T86" time="102.85278298745872" />
         <tli id="T856" time="103.576" type="appl" />
         <tli id="T87" time="104.053" type="appl" />
         <tli id="T88" time="104.885" type="appl" />
         <tli id="T89" time="105.716" type="appl" />
         <tli id="T90" time="106.548" type="appl" />
         <tli id="T91" time="108.47941954705264" />
         <tli id="T92" time="109.348" type="appl" />
         <tli id="T93" time="109.916" type="appl" />
         <tli id="T94" time="111.96606755732236" />
         <tli id="T857" time="111.98303377866118" type="intp" />
         <tli id="T858" time="112.0" type="appl" />
         <tli id="T95" time="112.928" type="appl" />
         <tli id="T96" time="114.7193861581663" />
         <tli id="T97" time="115.928" type="appl" />
         <tli id="T98" time="116.838" type="appl" />
         <tli id="T99" time="117.748" type="appl" />
         <tli id="T100" time="120.28602303880722" />
         <tli id="T859" time="120.35451151940362" type="intp" />
         <tli id="T860" time="120.423" type="appl" />
         <tli id="T101" time="121.154" type="appl" />
         <tli id="T102" time="121.931" type="appl" />
         <tli id="T103" time="122.707" type="appl" />
         <tli id="T104" time="123.484" type="appl" />
         <tli id="T861" time="124.167" type="appl" />
         <tli id="T105" time="125.05933083106936" />
         <tli id="T862" time="125.415" type="appl" />
         <tli id="T106" time="126.157" type="appl" />
         <tli id="T107" time="126.7" type="appl" />
         <tli id="T108" time="127.243" type="appl" />
         <tli id="T109" time="127.786" type="appl" />
         <tli id="T110" time="128.329" type="appl" />
         <tli id="T111" time="128.872" type="appl" />
         <tli id="T112" time="131.22596450124044" />
         <tli id="T863" time="131.45730966749363" type="intp" />
         <tli id="T864" time="131.6886548337468" type="intp" />
         <tli id="T113" time="131.92" type="appl" />
         <tli id="T114" time="132.6992899510867" />
         <tli id="T115" time="133.851" type="appl" />
         <tli id="T865" time="135.024" type="appl" />
         <tli id="T116" time="135.68594063674794" />
         <tli id="T866" time="135.865939673607" type="intp" />
         <tli id="T117" time="136.04593871046606" />
         <tli id="T118" time="136.864" type="appl" />
         <tli id="T119" time="137.71259645916095" />
         <tli id="T120" time="138.643" type="appl" />
         <tli id="T121" time="139.573" type="appl" />
         <tli id="T122" time="140.503" type="appl" />
         <tli id="T123" time="143.91922991530066" />
         <tli id="T867" time="143.93261495765034" type="intp" />
         <tli id="T868" time="143.946" type="appl" />
         <tli id="T124" time="144.695" type="appl" />
         <tli id="T125" time="145.605" type="appl" />
         <tli id="T126" time="146.516" type="appl" />
         <tli id="T127" time="147.214" type="appl" />
         <tli id="T128" time="147.913" type="appl" />
         <tli id="T129" time="148.611" type="appl" />
         <tli id="T130" time="149.31" type="appl" />
         <tli id="T131" time="150.008" type="appl" />
         <tli id="T132" time="160.8858057970146" />
         <tli id="T869" time="161.14687053134307" type="intp" />
         <tli id="T870" time="161.40793526567154" type="intp" />
         <tli id="T133" time="161.669" type="appl" />
         <tli id="T134" time="162.541" type="appl" />
         <tli id="T135" time="163.413" type="appl" />
         <tli id="T136" time="164.4924531651903" />
         <tli id="T137" time="165.072" type="appl" />
         <tli id="T138" time="165.698" type="appl" />
         <tli id="T139" time="166.324" type="appl" />
         <tli id="T140" time="166.951" type="appl" />
         <tli id="T141" time="167.577" type="appl" />
         <tli id="T142" time="168.4990983930528" />
         <tli id="T871" time="168.53" type="appl" />
         <tli id="T872" time="169.217" type="appl" />
         <tli id="T143" time="169.666" type="appl" />
         <tli id="T144" time="170.678" type="appl" />
         <tli id="T145" time="171.691" type="appl" />
         <tli id="T146" time="174.14573484563113" />
         <tli id="T147" time="175.892" type="appl" />
         <tli id="T148" time="177.435" type="appl" />
         <tli id="T873" time="178.763" type="appl" />
         <tli id="T149" time="184.672345186388" />
         <tli id="T874" time="185.17217259319398" type="intp" />
         <tli id="T150" time="185.672" type="appl" />
         <tli id="T151" time="186.645" type="appl" />
         <tli id="T152" time="187.618" type="appl" />
         <tli id="T875" time="188.434" type="appl" />
         <tli id="T153" time="189.27898720378064" />
         <tli id="T876" time="189.37" type="appl" />
         <tli id="T154" time="190.04" type="appl" />
         <tli id="T155" time="190.808" type="appl" />
         <tli id="T156" time="191.575" type="appl" />
         <tli id="T157" time="192.343" type="appl" />
         <tli id="T158" time="193.111" type="appl" />
         <tli id="T159" time="193.878" type="appl" />
         <tli id="T160" time="194.646" type="appl" />
         <tli id="T161" time="196.70561413196506" />
         <tli id="T162" time="197.916" type="appl" />
         <tli id="T163" time="199.1656009690387" />
         <tli id="T164" time="200.614" type="appl" />
         <tli id="T165" time="202.094" type="appl" />
         <tli id="T166" time="203.573" type="appl" />
         <tli id="T167" time="205.052" type="appl" />
         <tli id="T168" time="205.883" type="appl" />
         <tli id="T169" time="206.713" type="appl" />
         <tli id="T170" time="207.7388884283252" />
         <tli id="T171" time="208.769" type="appl" />
         <tli id="T172" time="209.691" type="appl" />
         <tli id="T173" time="210.613" type="appl" />
         <tli id="T174" time="211.208" type="appl" />
         <tli id="T175" time="211.802" type="appl" />
         <tli id="T176" time="212.397" type="appl" />
         <tli id="T177" time="212.992" type="appl" />
         <tli id="T178" time="213.587" type="appl" />
         <tli id="T179" time="214.181" type="appl" />
         <tli id="T180" time="219.0454945954713" />
         <tli id="T877" time="219.29438215260973" type="intp" />
         <tli id="T878" time="219.54326970974816" type="intp" />
         <tli id="T181" time="219.7921572668866" />
         <tli id="T182" time="220.87215148804086" />
         <tli id="T183" time="225.0987955387311" />
         <tli id="T879" time="225.11089776936555" type="intp" />
         <tli id="T880" time="225.123" type="appl" />
         <tli id="T184" time="225.806" type="appl" />
         <tli id="T185" time="226.548" type="appl" />
         <tli id="T186" time="227.29" type="appl" />
         <tli id="T187" time="228.032" type="appl" />
         <tli id="T881" time="228.118" type="appl" />
         <tli id="T882" time="229.491" type="appl" />
         <tli id="T188" time="231.222" type="appl" />
         <tli id="T189" time="231.622" type="appl" />
         <tli id="T190" time="232.023" type="appl" />
         <tli id="T883" time="232.299" type="appl" />
         <tli id="T191" time="232.38542321602512" />
         <tli id="T884" time="232.735" type="appl" />
         <tli id="T192" time="233.48" type="appl" />
         <tli id="T193" time="234.375" type="appl" />
         <tli id="T194" time="235.179" type="appl" />
         <tli id="T195" time="235.984" type="appl" />
         <tli id="T885" time="236.666" type="appl" />
         <tli id="T196" time="237.82539410776522" />
         <tli id="T886" time="238.2536970538826" type="intp" />
         <tli id="T197" time="238.682" type="appl" />
         <tli id="T198" time="239.589" type="appl" />
         <tli id="T199" time="240.267" type="appl" />
         <tli id="T200" time="240.945" type="appl" />
         <tli id="T201" time="241.624" type="appl" />
         <tli id="T202" time="242.302" type="appl" />
         <tli id="T203" time="244.42535879259697" />
         <tli id="T887" time="244.7279058617313" type="intp" />
         <tli id="T888" time="245.03045293086564" type="intp" />
         <tli id="T204" time="245.333" type="appl" />
         <tli id="T205" time="246.12" type="appl" />
         <tli id="T206" time="246.907" type="appl" />
         <tli id="T207" time="247.695" type="appl" />
         <tli id="T208" time="248.482" type="appl" />
         <tli id="T209" time="249.269" type="appl" />
         <tli id="T210" time="250.50532625983595" />
         <tli id="T211" time="251.724" type="appl" />
         <tli id="T212" time="253.97197437712128" />
         <tli id="T889" time="254.14798718856065" type="intp" />
         <tli id="T890" time="254.324" type="appl" />
         <tli id="T213" time="255.398" type="appl" />
         <tli id="T214" time="256.49" type="appl" />
         <tli id="T215" time="257.582" type="appl" />
         <tli id="T216" time="261.1852691134727" />
         <tli id="T891" time="261.3461794089818" type="intp" />
         <tli id="T892" time="261.5070897044909" type="intp" />
         <tli id="T217" time="261.668" type="appl" />
         <tli id="T218" time="262.308" type="appl" />
         <tli id="T219" time="262.948" type="appl" />
         <tli id="T220" time="263.587" type="appl" />
         <tli id="T221" time="264.226" type="appl" />
         <tli id="T222" time="264.866" type="appl" />
         <tli id="T223" time="265.838" type="appl" />
         <tli id="T224" time="266.809" type="appl" />
         <tli id="T225" time="267.78" type="appl" />
         <tli id="T226" time="269.5318911189367" />
         <tli id="T893" time="269.5714455594683" type="intp" />
         <tli id="T894" time="269.611" type="appl" />
         <tli id="T227" time="269.916" type="appl" />
         <tli id="T228" time="270.584" type="appl" />
         <tli id="T229" time="271.253" type="appl" />
         <tli id="T230" time="271.921" type="appl" />
         <tli id="T231" time="274.0852000883711" />
         <tli id="T232" time="274.819" type="appl" />
         <tli id="T233" time="275.593" type="appl" />
         <tli id="T234" time="276.368" type="appl" />
         <tli id="T235" time="277.142" type="appl" />
         <tli id="T236" time="278.0718454232493" />
         <tli id="T237" time="278.636" type="appl" />
         <tli id="T238" time="279.17" type="appl" />
         <tli id="T239" time="279.705" type="appl" />
         <tli id="T240" time="281.71182594639896" />
         <tli id="T895" time="281.8352172975993" type="intp" />
         <tli id="T896" time="281.95860864879967" type="intp" />
         <tli id="T241" time="282.082" type="appl" />
         <tli id="T242" time="283.61848241090587" />
         <tli id="T243" time="284.586" type="appl" />
         <tli id="T244" time="285.469" type="appl" />
         <tli id="T245" time="286.42" type="appl" />
         <tli id="T246" time="290.5651119074662" />
         <tli id="T897" time="290.8364079383108" type="intp" />
         <tli id="T898" time="291.1077039691554" type="intp" />
         <tli id="T247" time="291.379" type="appl" />
         <tli id="T248" time="292.229" type="appl" />
         <tli id="T249" time="293.078" type="appl" />
         <tli id="T250" time="293.928" type="appl" />
         <tli id="T251" time="295.67175124946726" />
         <tli id="T899" time="295.7443756247336" type="intp" />
         <tli id="T900" time="295.817" type="appl" />
         <tli id="T252" time="296.427" type="appl" />
         <tli id="T253" time="297.162" type="appl" />
         <tli id="T254" time="297.898" type="appl" />
         <tli id="T255" time="298.633" type="appl" />
         <tli id="T256" time="300.9183898423588" />
         <tli id="T901" time="301.13725989490587" type="intp" />
         <tli id="T902" time="301.3561299474529" type="intp" />
         <tli id="T257" time="301.575" type="appl" />
         <tli id="T258" time="302.229" type="appl" />
         <tli id="T259" time="302.884" type="appl" />
         <tli id="T260" time="303.539" type="appl" />
         <tli id="T261" time="304.194" type="appl" />
         <tli id="T262" time="304.848" type="appl" />
         <tli id="T903" time="305.489" type="appl" />
         <tli id="T263" time="307.3583553833158" />
         <tli id="T904" time="307.361" type="appl" />
         <tli id="T264" time="307.92501901787205" />
         <tli id="T265" time="308.5850154863553" />
         <tli id="T266" time="310.00500788824337" />
         <tli id="T267" time="311.5649995410218" />
         <tli id="T268" time="318.31163010773867" />
         <tli id="T269" time="318.77829427737316" />
         <tli id="T270" time="319.2049586610391" />
         <tli id="T271" time="319.6449563066945" />
         <tli id="T272" time="320.30495277517764" />
         <tli id="T273" time="321.77827822502394" />
         <tli id="T274" time="322.667" type="appl" />
         <tli id="T275" time="324.7982620656591" />
         <tli id="T905" time="324.98850804377275" type="intp" />
         <tli id="T906" time="325.1787540218864" type="intp" />
         <tli id="T276" time="325.369" type="appl" />
         <tli id="T277" time="326.059" type="appl" />
         <tli id="T278" time="326.75" type="appl" />
         <tli id="T279" time="327.441" type="appl" />
         <tli id="T280" time="328.132" type="appl" />
         <tli id="T281" time="328.822" type="appl" />
         <tli id="T282" time="329.73156900179595" />
         <tli id="T283" time="330.834" type="appl" />
         <tli id="T284" time="331.721" type="appl" />
         <tli id="T285" time="332.608" type="appl" />
         <tli id="T286" time="333.93821315950186" />
         <tli id="T907" time="334.2831065797509" type="intp" />
         <tli id="T908" time="334.628" type="appl" />
         <tli id="T287" time="335.207" type="appl" />
         <tli id="T288" time="336.015" type="appl" />
         <tli id="T289" time="337.2848619188812" />
         <tli id="T290" time="338.497" type="appl" />
         <tli id="T291" time="339.85818148286614" />
         <tli id="T292" time="340.923" type="appl" />
         <tli id="T293" time="341.933" type="appl" />
         <tli id="T294" time="343.4248290650731" />
         <tli id="T295" time="344.206" type="appl" />
         <tli id="T296" time="344.795" type="appl" />
         <tli id="T297" time="345.385" type="appl" />
         <tli id="T298" time="345.974" type="appl" />
         <tli id="T299" time="346.563" type="appl" />
         <tli id="T300" time="347.432" type="appl" />
         <tli id="T301" time="348.206" type="appl" />
         <tli id="T302" time="349.454" type="appl" />
         <tli id="T303" time="350.702" type="appl" />
         <tli id="T304" time="351.949" type="appl" />
         <tli id="T305" time="354.0247723467726" />
         <tli id="T306" time="354.963" type="appl" />
         <tli id="T307" time="355.78" type="appl" />
         <tli id="T308" time="356.8647571505487" />
         <tli id="T309" time="357.589" type="appl" />
         <tli id="T310" time="358.354" type="appl" />
         <tli id="T311" time="359.118" type="appl" />
         <tli id="T312" time="361.2914001310823" />
         <tli id="T909" time="361.59393342072156" type="intp" />
         <tli id="T910" time="361.89646671036076" type="intp" />
         <tli id="T313" time="362.199" type="appl" />
         <tli id="T314" time="363.018" type="appl" />
         <tli id="T315" time="363.836" type="appl" />
         <tli id="T911" time="364.64" type="appl" />
         <tli id="T316" time="366.86470364271804" />
         <tli id="T912" time="367.20985182135905" type="intp" />
         <tli id="T317" time="367.555" type="appl" />
         <tli id="T318" time="368.309" type="appl" />
         <tli id="T319" time="369.064" type="appl" />
         <tli id="T320" time="369.818" type="appl" />
         <tli id="T321" time="370.573" type="appl" />
         <tli id="T322" time="371.619" type="appl" />
         <tli id="T323" time="372.398" type="appl" />
         <tli id="T324" time="373.669" type="appl" />
         <tli id="T325" time="374.707" type="appl" />
         <tli id="T326" time="375.746" type="appl" />
         <tli id="T327" time="376.733" type="appl" />
         <tli id="T328" time="377.696" type="appl" />
         <tli id="T329" time="378.658" type="appl" />
         <tli id="T330" time="379.621" type="appl" />
         <tli id="T331" time="380.583" type="appl" />
         <tli id="T332" time="381.546" type="appl" />
         <tli id="T333" time="382.508" type="appl" />
         <tli id="T913" time="382.735" type="appl" />
         <tli id="T334" time="383.182" type="appl" />
         <tli id="T914" time="383.421" type="appl" />
         <tli id="T335" time="383.597" type="appl" />
         <tli id="T336" time="384.012" type="appl" />
         <tli id="T337" time="384.5579423028629" />
         <tli id="T915" time="384.856" type="appl" />
         <tli id="T916" time="385.917" type="appl" />
         <tli id="T338" time="386.35" type="appl" />
         <tli id="T339" time="387.734" type="appl" />
         <tli id="T340" time="388.461" type="appl" />
         <tli id="T341" time="389.187" type="appl" />
         <tli id="T342" time="389.914" type="appl" />
         <tli id="T343" time="390.67790955607046" />
         <tli id="T344" time="391.496" type="appl" />
         <tli id="T345" time="392.351" type="appl" />
         <tli id="T346" time="393.6445603487474" />
         <tli id="T347" time="394.358" type="appl" />
         <tli id="T348" time="395.097" type="appl" />
         <tli id="T349" time="395.837" type="appl" />
         <tli id="T350" time="396.576" type="appl" />
         <tli id="T351" time="397.315" type="appl" />
         <tli id="T352" time="398.054" type="appl" />
         <tli id="T353" time="398.793" type="appl" />
         <tli id="T354" time="399.533" type="appl" />
         <tli id="T355" time="400.272" type="appl" />
         <tli id="T356" time="401.011" type="appl" />
         <tli id="T357" time="401.75" type="appl" />
         <tli id="T358" time="402.489" type="appl" />
         <tli id="T359" time="403.229" type="appl" />
         <tli id="T360" time="403.968" type="appl" />
         <tli id="T361" time="404.707" type="appl" />
         <tli id="T362" time="409.145" type="appl" />
         <tli id="T363" time="413.99778477580935" />
         <tli id="T364" time="415.519" type="appl" />
         <tli id="T365" time="416.543" type="appl" />
         <tli id="T366" time="418.0177632656614" />
         <tli id="T367" time="419.118" type="appl" />
         <tli id="T368" time="420.098" type="appl" />
         <tli id="T369" time="421.078" type="appl" />
         <tli id="T370" time="423.07106955970426" />
         <tli id="T917" time="423.33753477985215" type="intp" />
         <tli id="T918" time="423.604" type="appl" />
         <tli id="T371" time="424.318" type="appl" />
         <tli id="T372" time="425.326" type="appl" />
         <tli id="T373" time="426.14" type="appl" />
         <tli id="T374" time="426.953" type="appl" />
         <tli id="T375" time="427.766" type="appl" />
         <tli id="T376" time="429.9043663293533" />
         <tli id="T919" time="430.21124421956887" type="intp" />
         <tli id="T920" time="430.51812210978443" type="intp" />
         <tli id="T377" time="430.825" type="appl" />
         <tli id="T378" time="431.617" type="appl" />
         <tli id="T379" time="432.408" type="appl" />
         <tli id="T380" time="432.882" type="appl" />
         <tli id="T381" time="433.356" type="appl" />
         <tli id="T382" time="433.83" type="appl" />
         <tli id="T383" time="434.304" type="appl" />
         <tli id="T384" time="434.778" type="appl" />
         <tli id="T385" time="435.356" type="appl" />
         <tli id="T386" time="435.93" type="appl" />
         <tli id="T387" time="436.505" type="appl" />
         <tli id="T388" time="437.079" type="appl" />
         <tli id="T389" time="437.654" type="appl" />
         <tli id="T390" time="438.299" type="appl" />
         <tli id="T391" time="438.76" type="appl" />
         <tli id="T392" time="439.22" type="appl" />
         <tli id="T393" time="439.681" type="appl" />
         <tli id="T394" time="440.141" type="appl" />
         <tli id="T395" time="440.9576405186978" />
         <tli id="T921" time="441.2033202593489" type="intp" />
         <tli id="T922" time="441.449" type="appl" />
         <tli id="T396" time="441.897" type="appl" />
         <tli id="T397" time="442.73" type="appl" />
         <tli id="T398" time="443.564" type="appl" />
         <tli id="T399" time="444.9776190085498" />
         <tli id="T400" time="446.016" type="appl" />
         <tli id="T401" time="446.856" type="appl" />
         <tli id="T402" time="448.6242661626942" />
         <tli id="T923" time="448.7491330813471" type="intp" />
         <tli id="T924" time="448.874" type="appl" />
         <tli id="T403" time="449.402" type="appl" />
         <tli id="T404" time="450.17" type="appl" />
         <tli id="T405" time="450.939" type="appl" />
         <tli id="T406" time="452.1775804829117" />
         <tli id="T407" time="453.085" type="appl" />
         <tli id="T408" time="453.985" type="appl" />
         <tli id="T409" time="454.886" type="appl" />
         <tli id="T925" time="455.675" type="appl" />
         <tli id="T410" time="455.786" type="appl" />
         <tli id="T926" time="456.486" type="appl" />
         <tli id="T411" time="457.447" type="appl" />
         <tli id="T412" time="458.382" type="appl" />
         <tli id="T413" time="459.317" type="appl" />
         <tli id="T927" time="460.105" type="appl" />
         <tli id="T414" time="460.6175353223026" />
         <tli id="T928" time="461.166" type="appl" />
         <tli id="T415" time="462.084" type="appl" />
         <tli id="T416" time="462.769" type="appl" />
         <tli id="T417" time="463.453" type="appl" />
         <tli id="T418" time="464.3308487863949" />
         <tli id="T419" time="465.293" type="appl" />
         <tli id="T420" time="466.206" type="appl" />
         <tli id="T421" time="471.2374784969864" />
         <tli id="T929" time="471.2557392484932" type="intp" />
         <tli id="T930" time="471.274" type="appl" />
         <tli id="T422" time="472.031" type="appl" />
         <tli id="T423" time="472.923" type="appl" />
         <tli id="T424" time="473.815" type="appl" />
         <tli id="T425" time="474.707" type="appl" />
         <tli id="T426" time="475.457" type="appl" />
         <tli id="T427" time="476.032" type="appl" />
         <tli id="T428" time="476.606" type="appl" />
         <tli id="T429" time="477.181" type="appl" />
         <tli id="T430" time="477.756" type="appl" />
         <tli id="T431" time="479.3441017866383" />
         <tli id="T931" time="479.36505089331916" type="intp" />
         <tli id="T932" time="479.386" type="appl" />
         <tli id="T432" time="480.406" type="appl" />
         <tli id="T433" time="481.174" type="appl" />
         <tli id="T434" time="481.942" type="appl" />
         <tli id="T435" time="482.711" type="appl" />
         <tli id="T436" time="483.48" type="appl" />
         <tli id="T437" time="484.93073856026365" />
         <tli id="T933" time="485.0593692801318" type="intp" />
         <tli id="T934" time="485.188" type="appl" />
         <tli id="T438" time="485.753" type="appl" />
         <tli id="T439" time="486.479" type="appl" />
         <tli id="T440" time="487.204" type="appl" />
         <tli id="T441" time="487.93" type="appl" />
         <tli id="T442" time="488.655" type="appl" />
         <tli id="T443" time="489.381" type="appl" />
         <tli id="T444" time="494.9240184214381" />
         <tli id="T935" time="495.109" type="appl" />
         <tli id="T445" time="496.03" type="appl" />
         <tli id="T446" time="496.705" type="appl" />
         <tli id="T936" time="496.856" type="appl" />
         <tli id="T447" time="497.428" type="appl" />
         <tli id="T937" time="497.855" type="appl" />
         <tli id="T448" time="498.152" type="appl" />
         <tli id="T938" time="498.354" type="appl" />
         <tli id="T449" time="498.754" type="appl" />
         <tli id="T450" time="499.347" type="appl" />
         <tli id="T451" time="499.939" type="appl" />
         <tli id="T452" time="500.532" type="appl" />
         <tli id="T453" time="501.124" type="appl" />
         <tli id="T454" time="502.7706431022937" />
         <tli id="T939" time="502.7773215511469" type="intp" />
         <tli id="T940" time="502.784" type="appl" />
         <tli id="T455" time="503.516" type="appl" />
         <tli id="T456" time="504.206" type="appl" />
         <tli id="T457" time="504.895" type="appl" />
         <tli id="T458" time="505.585" type="appl" />
         <tli id="T459" time="506.275" type="appl" />
         <tli id="T460" time="506.964" type="appl" />
         <tli id="T461" time="507.654" type="appl" />
         <tli id="T941" time="508.275" type="appl" />
         <tli id="T462" time="509.4572739900575" />
         <tli id="T942" time="509.91363699502875" type="intp" />
         <tli id="T463" time="510.37" type="appl" />
         <tli id="T464" time="511.226" type="appl" />
         <tli id="T465" time="512.083" type="appl" />
         <tli id="T466" time="512.939" type="appl" />
         <tli id="T467" time="513.796" type="appl" />
         <tli id="T468" time="514.653" type="appl" />
         <tli id="T469" time="515.509" type="appl" />
         <tli id="T470" time="516.366" type="appl" />
         <tli id="T471" time="517.223" type="appl" />
         <tli id="T472" time="518.079" type="appl" />
         <tli id="T473" time="518.936" type="appl" />
         <tli id="T474" time="519.792" type="appl" />
         <tli id="T475" time="521.0905450759478" />
         <tli id="T476" time="521.798" type="appl" />
         <tli id="T477" time="522.371" type="appl" />
         <tli id="T478" time="522.943" type="appl" />
         <tli id="T479" time="523.516" type="appl" />
         <tli id="T480" time="524.7171923371078" />
         <tli id="T481" time="525.71" type="appl" />
         <tli id="T482" time="526.567" type="appl" />
         <tli id="T483" time="527.425" type="appl" />
         <tli id="T484" time="527.964" type="appl" />
         <tli id="T485" time="528.502" type="appl" />
         <tli id="T486" time="529.041" type="appl" />
         <tli id="T487" time="529.579" type="appl" />
         <tli id="T488" time="532.4171511360781" />
         <tli id="T943" time="532.5967674240521" type="intp" />
         <tli id="T944" time="532.776383712026" type="intp" />
         <tli id="T489" time="532.956" type="appl" />
         <tli id="T490" time="533.592" type="appl" />
         <tli id="T491" time="534.229" type="appl" />
         <tli id="T492" time="534.866" type="appl" />
         <tli id="T493" time="535.502" type="appl" />
         <tli id="T494" time="536.139" type="appl" />
         <tli id="T495" time="536.775" type="appl" />
         <tli id="T496" time="538.5437850202807" />
         <tli id="T945" time="538.7345233468537" type="intp" />
         <tli id="T946" time="538.9252616734268" type="intp" />
         <tli id="T497" time="539.116" type="appl" />
         <tli id="T498" time="539.723" type="appl" />
         <tli id="T499" time="540.33" type="appl" />
         <tli id="T500" time="540.937" type="appl" />
         <tli id="T501" time="541.544" type="appl" />
         <tli id="T502" time="542.151" type="appl" />
         <tli id="T503" time="542.758" type="appl" />
         <tli id="T504" time="547.0837393245932" />
         <tli id="T947" time="547.3404928830622" type="intp" />
         <tli id="T948" time="547.5972464415311" type="intp" />
         <tli id="T505" time="547.854" type="appl" />
         <tli id="T506" time="548.376" type="appl" />
         <tli id="T507" time="548.897" type="appl" />
         <tli id="T508" time="549.706" type="appl" />
         <tli id="T509" time="550.448" type="appl" />
         <tli id="T510" time="551.19" type="appl" />
         <tli id="T511" time="551.932" type="appl" />
         <tli id="T512" time="552.674" type="appl" />
         <tli id="T513" time="553.862" type="appl" />
         <tli id="T514" time="556.7036878500602" />
         <tli id="T949" time="556.8897919000401" type="intp" />
         <tli id="T950" time="557.07589595002" type="intp" />
         <tli id="T515" time="557.262" type="appl" />
         <tli id="T516" time="557.849" type="appl" />
         <tli id="T517" time="558.437" type="appl" />
         <tli id="T518" time="559.025" type="appl" />
         <tli id="T519" time="559.613" type="appl" />
         <tli id="T520" time="560.2" type="appl" />
         <tli id="T521" time="560.788" type="appl" />
         <tli id="T522" time="561.73" type="appl" />
         <tli id="T523" time="562.277" type="appl" />
         <tli id="T524" time="562.824" type="appl" />
         <tli id="T525" time="563.443" type="appl" />
         <tli id="T526" time="564.062" type="appl" />
         <tli id="T527" time="564.681" type="appl" />
         <tli id="T528" time="565.3" type="appl" />
         <tli id="T529" time="566.671" type="appl" />
         <tli id="T530" time="567.702" type="appl" />
         <tli id="T531" time="568.733" type="appl" />
         <tli id="T532" time="570.492" type="appl" />
         <tli id="T533" time="571.88" type="appl" />
         <tli id="T534" time="573.268" type="appl" />
         <tli id="T535" time="573.923" type="appl" />
         <tli id="T536" time="574.578" type="appl" />
         <tli id="T537" time="575.233" type="appl" />
         <tli id="T538" time="575.887" type="appl" />
         <tli id="T539" time="576.542" type="appl" />
         <tli id="T540" time="577.7769084245582" />
         <tli id="T541" time="579.008" type="appl" />
         <tli id="T542" time="579.984" type="appl" />
         <tli id="T543" time="580.96" type="appl" />
         <tli id="T544" time="582.068" type="appl" />
         <tli id="T545" time="584.0702080836301" />
         <tli id="T546" time="585.107" type="appl" />
         <tli id="T547" time="586.4635286107559" />
         <tli id="T548" time="587.508" type="appl" />
         <tli id="T549" time="590.2301751228064" />
         <tli id="T951" time="590.4217834152042" type="intp" />
         <tli id="T952" time="590.6133917076021" type="intp" />
         <tli id="T550" time="590.805" type="appl" />
         <tli id="T551" time="591.514" type="appl" />
         <tli id="T552" time="592.224" type="appl" />
         <tli id="T553" time="592.933" type="appl" />
         <tli id="T554" time="594.393" type="appl" />
         <tli id="T555" time="595.34" type="appl" />
         <tli id="T556" time="596.286" type="appl" />
         <tli id="T557" time="597.233" type="appl" />
         <tli id="T558" time="598.18" type="appl" />
         <tli id="T559" time="598.852" type="appl" />
         <tli id="T560" time="599.514" type="appl" />
         <tli id="T561" time="602.8567742269189" />
         <tli id="T562" time="607.9567469379251" />
         <tli id="T953" time="607.9698734689625" type="intp" />
         <tli id="T954" time="607.983" type="appl" />
         <tli id="T563" time="608.74" type="appl" />
         <tli id="T564" time="609.299" type="appl" />
         <tli id="T565" time="609.858" type="appl" />
         <tli id="T566" time="610.416" type="appl" />
         <tli id="T567" time="611.144" type="appl" />
         <tli id="T568" time="611.872" type="appl" />
         <tli id="T569" time="612.599" type="appl" />
         <tli id="T570" time="613.327" type="appl" />
         <tli id="T571" time="614.283379751971" />
         <tli id="T572" time="615.154" type="appl" />
         <tli id="T573" time="615.976" type="appl" />
         <tli id="T574" time="617.3433633785748" />
         <tli id="T575" time="618.591" type="appl" />
         <tli id="T576" time="619.91" type="appl" />
         <tli id="T955" time="620.899" type="appl" />
         <tli id="T577" time="624.4433253880151" />
         <tli id="T956" time="625.0056626940075" type="intp" />
         <tli id="T578" time="625.568" type="appl" />
         <tli id="T579" time="627.9299733982848" />
         <tli id="T580" time="628.737" type="appl" />
         <tli id="T581" time="629.352" type="appl" />
         <tli id="T582" time="629.968" type="appl" />
         <tli id="T583" time="630.978" type="appl" />
         <tli id="T584" time="631.348" type="appl" />
         <tli id="T585" time="631.719" type="appl" />
         <tli id="T957" time="631.881" type="appl" />
         <tli id="T586" time="632.089" type="appl" />
         <tli id="T587" time="632.6166149876146" />
         <tli id="T958" time="632.941" type="appl" />
         <tli id="T588" time="633.403" type="appl" />
         <tli id="T589" time="633.988" type="appl" />
         <tli id="T590" time="634.574" type="appl" />
         <tli id="T591" time="635.6099323042708" />
         <tli id="T592" time="637.606" type="appl" />
         <tli id="T593" time="639.235" type="appl" />
         <tli id="T594" time="640.865" type="appl" />
         <tli id="T595" time="642.494" type="appl" />
         <tli id="T596" time="644.123" type="appl" />
         <tli id="T597" time="645.13" type="appl" />
         <tli id="T959" time="645.67" type="appl" />
         <tli id="T598" time="645.902" type="appl" />
         <tli id="T599" time="647.1698704492185" />
         <tli id="T960" time="647.5514352246092" type="intp" />
         <tli id="T600" time="647.933" type="appl" />
         <tli id="T601" time="648.696" type="appl" />
         <tli id="T602" time="649.459" type="appl" />
         <tli id="T603" time="650.222" type="appl" />
         <tli id="T604" time="650.986" type="appl" />
         <tli id="T605" time="651.749" type="appl" />
         <tli id="T606" time="652.512" type="appl" />
         <tli id="T607" time="653.275" type="appl" />
         <tli id="T608" time="654.038" type="appl" />
         <tli id="T609" time="662.5697880471592" />
         <tli id="T961" time="662.8785253647728" type="intp" />
         <tli id="T962" time="663.1872626823864" type="intp" />
         <tli id="T610" time="663.496" type="appl" />
         <tli id="T611" time="664.3697784157497" />
         <tli id="T612" time="664.96" type="appl" />
         <tli id="T613" time="665.439" type="appl" />
         <tli id="T614" time="665.918" type="appl" />
         <tli id="T615" time="666.397" type="appl" />
         <tli id="T616" time="667.1830966955466" />
         <tli id="T617" time="669.2497523039283" />
         <tli id="T618" time="670.238" type="appl" />
         <tli id="T619" time="671.063" type="appl" />
         <tli id="T620" time="671.888" type="appl" />
         <tli id="T621" time="673.1497314358744" />
         <tli id="T963" time="673.4488657179372" type="intp" />
         <tli id="T964" time="673.748" type="appl" />
         <tli id="T622" time="674.309" type="appl" />
         <tli id="T623" time="675.125" type="appl" />
         <tli id="T624" time="675.94" type="appl" />
         <tli id="T625" time="676.52" type="appl" />
         <tli id="T626" time="677.101" type="appl" />
         <tli id="T627" time="677.681" type="appl" />
         <tli id="T628" time="678.262" type="appl" />
         <tli id="T629" time="679.809695799659" />
         <tli id="T965" time="680.0244638664393" type="intp" />
         <tli id="T966" time="680.2392319332196" type="intp" />
         <tli id="T630" time="680.454" type="appl" />
         <tli id="T631" time="681.048" type="appl" />
         <tli id="T632" time="681.642" type="appl" />
         <tli id="T633" time="682.236" type="appl" />
         <tli id="T634" time="682.83" type="appl" />
         <tli id="T635" time="683.424" type="appl" />
         <tli id="T636" time="684.018" type="appl" />
         <tli id="T637" time="684.612" type="appl" />
         <tli id="T638" time="685.206" type="appl" />
         <tli id="T639" time="685.8" type="appl" />
         <tli id="T640" time="686.394" type="appl" />
         <tli id="T967" time="686.976" type="appl" />
         <tli id="T641" time="686.988" type="appl" />
         <tli id="T968" time="687.787" type="appl" />
         <tli id="T642" time="688.446" type="appl" />
         <tli id="T643" time="689.225" type="appl" />
         <tli id="T644" time="690.081" type="appl" />
         <tli id="T645" time="690.856" type="appl" />
         <tli id="T646" time="693.116291265239" />
         <tli id="T969" time="693.2281456326195" type="intp" />
         <tli id="T970" time="693.34" type="appl" />
         <tli id="T647" time="693.918" type="appl" />
         <tli id="T648" time="694.635" type="appl" />
         <tli id="T649" time="695.6496110432553" />
         <tli id="T650" time="696.288" type="appl" />
         <tli id="T651" time="696.901" type="appl" />
         <tli id="T652" time="697.514" type="appl" />
         <tli id="T653" time="698.127" type="appl" />
         <tli id="T654" time="698.74" type="appl" />
         <tli id="T655" time="699.587" type="appl" />
         <tli id="T971" time="699.954" type="appl" />
         <tli id="T656" time="700.405" type="appl" />
         <tli id="T972" time="701.015" type="appl" />
         <tli id="T657" time="701.703" type="appl" />
         <tli id="T658" time="702.618" type="appl" />
         <tli id="T659" time="703.7162345469385" />
         <tli id="T660" time="704.497" type="appl" />
         <tli id="T661" time="705.216" type="appl" />
         <tli id="T662" time="705.934" type="appl" />
         <tli id="T663" time="707.3628817010829" />
         <tli id="T973" time="707.6204408505414" type="intp" />
         <tli id="T974" time="707.878" type="appl" />
         <tli id="T664" time="708.438" type="appl" />
         <tli id="T665" time="709.142" type="appl" />
         <tli id="T666" time="711.656192061721" />
         <tli id="T975" time="711.913794707814" type="intp" />
         <tli id="T976" time="712.171397353907" type="intp" />
         <tli id="T667" time="712.429" type="appl" />
         <tli id="T668" time="713.163" type="appl" />
         <tli id="T669" time="713.897" type="appl" />
         <tli id="T670" time="714.63" type="appl" />
         <tli id="T671" time="715.364" type="appl" />
         <tli id="T672" time="716.098" type="appl" />
         <tli id="T673" time="716.832" type="appl" />
         <tli id="T674" time="723.1294640037365" />
         <tli id="T977" time="723.1472320018682" type="intp" />
         <tli id="T978" time="723.165" type="appl" />
         <tli id="T675" time="724.147" type="appl" />
         <tli id="T676" time="725.062" type="appl" />
         <tli id="T677" time="725.976" type="appl" />
         <tli id="T678" time="726.624" type="appl" />
         <tli id="T679" time="727.271" type="appl" />
         <tli id="T979" time="727.72" type="appl" />
         <tli id="T680" time="729.0294324341165" />
         <tli id="T980" time="729.4677162170583" type="intp" />
         <tli id="T681" time="729.906" type="appl" />
         <tli id="T682" time="730.802" type="appl" />
         <tli id="T683" time="731.697" type="appl" />
         <tli id="T684" time="732.592" type="appl" />
         <tli id="T981" time="733.149" type="appl" />
         <tli id="T982" time="734.085" type="appl" />
         <tli id="T685" time="734.195" type="appl" />
         <tli id="T686" time="735.602730594969" />
         <tli id="T687" time="736.401" type="appl" />
         <tli id="T688" time="737.029" type="appl" />
         <tli id="T689" time="737.657" type="appl" />
         <tli id="T690" time="738.285" type="appl" />
         <tli id="T691" time="738.914" type="appl" />
         <tli id="T692" time="739.542" type="appl" />
         <tli id="T693" time="740.17" type="appl" />
         <tli id="T694" time="740.798" type="appl" />
         <tli id="T695" time="742.3093613757172" />
         <tli id="T983" time="742.4739075838115" type="intp" />
         <tli id="T984" time="742.6384537919057" type="intp" />
         <tli id="T696" time="742.803" type="appl" />
         <tli id="T697" time="743.493" type="appl" />
         <tli id="T698" time="744.184" type="appl" />
         <tli id="T699" time="744.875" type="appl" />
         <tli id="T700" time="745.565" type="appl" />
         <tli id="T701" time="748.2159964370919" />
         <tli id="T985" time="748.4886642913946" type="intp" />
         <tli id="T986" time="748.7613321456972" type="intp" />
         <tli id="T702" time="749.034" type="appl" />
         <tli id="T703" time="749.805" type="appl" />
         <tli id="T704" time="750.576" type="appl" />
         <tli id="T705" time="751.346" type="appl" />
         <tli id="T706" time="752.117" type="appl" />
         <tli id="T707" time="753.1293034802445" />
         <tli id="T708" time="754.078" type="appl" />
         <tli id="T709" time="754.834" type="appl" />
         <tli id="T710" time="757.4026139478982" />
         <tli id="T987" time="757.8730759652655" type="intp" />
         <tli id="T988" time="758.3435379826327" type="intp" />
         <tli id="T711" time="758.814" type="appl" />
         <tli id="T712" time="760.041" type="appl" />
         <tli id="T713" time="760.62" type="appl" />
         <tli id="T714" time="761.198" type="appl" />
         <tli id="T715" time="761.777" type="appl" />
         <tli id="T716" time="762.356" type="appl" />
         <tli id="T717" time="762.934" type="appl" />
         <tli id="T718" time="766.6692310306418" />
         <tli id="T989" time="766.9938206870945" type="intp" />
         <tli id="T990" time="767.3184103435473" type="intp" />
         <tli id="T719" time="767.643" type="appl" />
         <tli id="T720" time="768.425" type="appl" />
         <tli id="T721" time="769.207" type="appl" />
         <tli id="T722" time="769.989" type="appl" />
         <tli id="T723" time="770.771" type="appl" />
         <tli id="T724" time="772.2958675902356" />
         <tli id="T991" time="772.5069117268238" type="intp" />
         <tli id="T992" time="772.7179558634118" type="intp" />
         <tli id="T725" time="772.929" type="appl" />
         <tli id="T726" time="773.537" type="appl" />
         <tli id="T727" time="774.146" type="appl" />
         <tli id="T728" time="774.754" type="appl" />
         <tli id="T729" time="776.5825113198789" />
         <tli id="T993" time="776.6417556599395" type="intp" />
         <tli id="T994" time="776.701" type="appl" />
         <tli id="T730" time="777.372" type="appl" />
         <tli id="T731" time="778.1" type="appl" />
         <tli id="T732" time="778.827" type="appl" />
         <tli id="T733" time="780.0758259611433" />
         <tli id="T734" time="781.849" type="appl" />
         <tli id="T735" time="783.409" type="appl" />
         <tli id="T736" time="784.969" type="appl" />
         <tli id="T737" time="786.53" type="appl" />
         <tli id="T738" time="787.678" type="appl" />
         <tli id="T739" time="788.546" type="appl" />
         <tli id="T740" time="789.414" type="appl" />
         <tli id="T741" time="790.282" type="appl" />
         <tli id="T742" time="791.292" type="appl" />
         <tli id="T743" time="792.301" type="appl" />
         <tli id="T744" time="793.31" type="appl" />
         <tli id="T745" time="794.32" type="appl" />
         <tli id="T746" time="795.237" type="appl" />
         <tli id="T747" time="795.907" type="appl" />
         <tli id="T748" time="796.578" type="appl" />
         <tli id="T749" time="797.248" type="appl" />
         <tli id="T750" time="797.919" type="appl" />
         <tli id="T995" time="798.477" type="appl" />
         <tli id="T751" time="798.8690587354268" />
         <tli id="T996" time="798.976" type="appl" />
         <tli id="T752" time="799.574" type="appl" />
         <tli id="T753" time="800.234" type="appl" />
         <tli id="T754" time="800.894" type="appl" />
         <tli id="T755" time="801.555" type="appl" />
         <tli id="T756" time="802.216" type="appl" />
         <tli id="T757" time="803.9290316604645" />
         <tli id="T997" time="803.9485158302323" type="intp" />
         <tli id="T998" time="803.968" type="appl" />
         <tli id="T758" time="804.795" type="appl" />
         <tli id="T759" time="805.458" type="appl" />
         <tli id="T760" time="806.122" type="appl" />
         <tli id="T761" time="807.182347585917" />
         <tli id="T762" time="808.098" type="appl" />
         <tli id="T763" time="808.772" type="appl" />
         <tli id="T764" time="809.445" type="appl" />
         <tli id="T765" time="811.288992278701" />
         <tli id="T766" time="812.503" type="appl" />
         <tli id="T767" time="813.8356453187068" />
         <tli id="T768" time="814.79" type="appl" />
         <tli id="T769" time="818.9556179226977" />
         <tli id="T999" time="819.1360786151317" type="intp" />
         <tli id="T1000" time="819.3165393075658" type="intp" />
         <tli id="T770" time="819.497" type="appl" />
         <tli id="T771" time="820.097" type="appl" />
         <tli id="T772" time="820.696" type="appl" />
         <tli id="T773" time="821.296" type="appl" />
         <tli id="T774" time="821.896" type="appl" />
         <tli id="T775" time="823.3022613312938" />
         <tli id="T776" time="824.212" type="appl" />
         <tli id="T777" time="824.989" type="appl" />
         <tli id="T778" time="825.766" type="appl" />
         <tli id="T779" time="827.108907629313" />
         <tli id="T780" time="828.522" type="appl" />
         <tli id="T781" time="830.6288887945565" />
         <tli id="T1001" time="830.6824443972782" type="intp" />
         <tli id="T1002" time="830.736" type="appl" />
         <tli id="T782" time="831.658" type="appl" />
         <tli id="T783" time="832.649" type="appl" />
         <tli id="T784" time="833.64" type="appl" />
         <tli id="T785" time="834.631" type="appl" />
         <tli id="T786" time="836.4155244980252" />
         <tli id="T787" time="837.37" type="appl" />
         <tli id="T788" time="838.6488458812764" />
         <tli id="T789" time="839.484" type="appl" />
         <tli id="T790" time="840.5621689767781" />
         <tli id="T1003" time="840.9381126511854" type="intp" />
         <tli id="T1004" time="841.3140563255927" type="intp" />
         <tli id="T791" time="841.69" type="appl" />
         <tli id="T792" time="842.811" type="appl" />
         <tli id="T793" time="843.932" type="appl" />
         <tli id="T794" time="845.052" type="appl" />
         <tli id="T795" time="846.173" type="appl" />
         <tli id="T796" time="847.294" type="appl" />
         <tli id="T797" time="850.242117181198" />
         <tli id="T1005" time="850.4780781207986" type="intp" />
         <tli id="T1006" time="850.7140390603993" type="intp" />
         <tli id="T798" time="850.95" type="appl" />
         <tli id="T799" time="851.666" type="appl" />
         <tli id="T800" time="852.383" type="appl" />
         <tli id="T801" time="853.099" type="appl" />
         <tli id="T802" time="855.5687553460267" />
         <tli id="T803" time="856.526" type="appl" />
         <tli id="T804" time="857.016" type="appl" />
         <tli id="T805" time="857.505" type="appl" />
         <tli id="T1007" time="857.878" type="appl" />
         <tli id="T806" time="858.8154046404845" />
         <tli id="T1008" time="859.2977023202423" type="intp" />
         <tli id="T807" time="859.78" type="appl" />
         <tli id="T808" time="860.69" type="appl" />
         <tli id="T809" time="861.601" type="appl" />
         <tli id="T810" time="862.511" type="appl" />
         <tli id="T811" time="863.2" type="appl" />
         <tli id="T812" time="863.888" type="appl" />
         <tli id="T813" time="864.577" type="appl" />
         <tli id="T814" time="865.266" type="appl" />
         <tli id="T815" time="865.954" type="appl" />
         <tli id="T1009" time="866.176" type="appl" />
         <tli id="T816" time="866.643" type="appl" />
         <tli id="T1010" time="866.8" type="appl" />
         <tli id="T817" time="867.492" type="appl" />
         <tli id="T818" time="868.342" type="appl" />
         <tli id="T819" time="869.4753476011371" />
         <tli id="T820" time="870.341" type="appl" />
         <tli id="T821" time="871.271" type="appl" />
         <tli id="T1011" time="871.736" type="intp" />
         <tli id="T822" time="872.201" type="appl" />
         <tli id="T823" time="873.131" type="appl" />
         <tli id="T824" time="873.812" type="appl" />
         <tli id="T825" time="874.494" type="appl" />
         <tli id="T826" time="875.175" type="appl" />
         <tli id="T828" time="876.035" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T338" start="T337">
            <tli id="T337.tx-KA.1" />
            <tli id="T337.tx-KA.2" />
            <tli id="T337.tx-KA.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klaudia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">viies</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T269" id="Seg_17" n="sc" s="T268">
               <ts e="T269" id="Seg_19" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_21" n="HIAT:w" s="T268">Рубить</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T338" id="Seg_24" n="sc" s="T337">
               <ts e="T338" id="Seg_26" n="HIAT:u" s="T337">
                  <ts e="T337.tx-KA.1" id="Seg_28" n="HIAT:w" s="T337">Ну</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337.tx-KA.2" id="Seg_31" n="HIAT:w" s="T337.tx-KA.1">постарайся</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337.tx-KA.3" id="Seg_34" n="HIAT:w" s="T337.tx-KA.2">ещё</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <nts id="Seg_37" n="HIAT:ip">(</nts>
                  <ats e="T338" id="Seg_38" n="HIAT:non-pho" s="T337.tx-KA.3">…</ats>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip">)</nts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T446" id="Seg_43" n="sc" s="T444">
               <ts e="T446" id="Seg_45" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_47" n="HIAT:w" s="T444">Ну</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_51" n="HIAT:w" s="T445">ничего</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T751" id="Seg_54" n="sc" s="T750">
               <ts e="T751" id="Seg_56" n="HIAT:u" s="T750">
                  <ts e="T751" id="Seg_58" n="HIAT:w" s="T750">Mhmh</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_61" n="sc" s="T0">
               <ts e="T4" id="Seg_63" n="e" s="T0">Klaudia Plotnikova, viies lint. </ts>
            </ts>
            <ts e="T269" id="Seg_64" n="sc" s="T268">
               <ts e="T269" id="Seg_66" n="e" s="T268">Рубить. </ts>
            </ts>
            <ts e="T338" id="Seg_67" n="sc" s="T337">
               <ts e="T338" id="Seg_69" n="e" s="T337">Ну постарайся ещё ((…)). </ts>
            </ts>
            <ts e="T446" id="Seg_70" n="sc" s="T444">
               <ts e="T445" id="Seg_72" n="e" s="T444">Ну, </ts>
               <ts e="T446" id="Seg_74" n="e" s="T445">ничего. </ts>
            </ts>
            <ts e="T751" id="Seg_75" n="sc" s="T750">
               <ts e="T751" id="Seg_77" n="e" s="T750">Mhmh. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_78" s="T0">PKZ_1964_SU0208.KA.001 (001)</ta>
            <ta e="T269" id="Seg_79" s="T268">PKZ_1964_SU0208.KA.002 (019.004)</ta>
            <ta e="T338" id="Seg_80" s="T337">PKZ_1964_SU0208.KA.003 (023)</ta>
            <ta e="T446" id="Seg_81" s="T444">PKZ_1964_SU0208.KA.004 (033)</ta>
            <ta e="T751" id="Seg_82" s="T750">PKZ_1964_SU0208.KA.005 (060)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_83" s="T0">Klaudia Plotnikova, viies lint. </ta>
            <ta e="T269" id="Seg_84" s="T268">Рубить. </ta>
            <ta e="T338" id="Seg_85" s="T337">Ну постарайся ещё ((…)). </ta>
            <ta e="T446" id="Seg_86" s="T444">Ну, ничего. </ta>
            <ta e="T751" id="Seg_87" s="T750">Mhmh. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_88" s="T0">EST:ext</ta>
            <ta e="T269" id="Seg_89" s="T268">RUS:ext</ta>
            <ta e="T338" id="Seg_90" s="T337">RUS:ext</ta>
            <ta e="T446" id="Seg_91" s="T444">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_92" s="T0">Клавдия Плотникова, пятая пленка.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_93" s="T0">Klavdiya Plotnikova, fifth tape.</ta>
            <ta e="T269" id="Seg_94" s="T268">To chop.</ta>
            <ta e="T338" id="Seg_95" s="T337">Try again, will you (…).</ta>
            <ta e="T446" id="Seg_96" s="T444">Well, that's alright.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_97" s="T0">Klavdiya Plotnikova, fünftes Band.</ta>
            <ta e="T269" id="Seg_98" s="T268">Zu hacken.</ta>
            <ta e="T338" id="Seg_99" s="T337">Versuch noch einmal, wirst du (…). </ta>
            <ta e="T446" id="Seg_100" s="T444">Also, das ist gut so.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T191" start="T187">
            <tli id="T187.tx-PKZ.1" />
            <tli id="T187.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T207" start="T206">
            <tli id="T206.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T260" start="T259">
            <tli id="T259.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T448" start="T446">
            <tli id="T446.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T587" start="T582">
            <tli id="T582.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T599" start="T596">
            <tli id="T596.tx-PKZ.1" />
            <tli id="T596.tx-PKZ.2" />
            <tli id="T596.tx-PKZ.3" />
         </timeline-fork>
         <timeline-fork end="T656" start="T654">
            <tli id="T654.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T805" start="T802">
            <tli id="T802.tx-PKZ.1" />
            <tli id="T802.tx-PKZ.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T337" id="Seg_101" n="sc" s="T4">
               <ts e="T12" id="Seg_103" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_105" n="HIAT:w" s="T4">Sʼimat</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_108" n="HIAT:w" s="T5">ej</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_111" n="HIAT:w" s="T6">kuvas</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_115" n="HIAT:w" s="T7">onʼiʔ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_118" n="HIAT:w" s="T8">döbər</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_121" n="HIAT:w" s="T9">mandolaʔbə</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_125" n="HIAT:w" s="T10">onʼiʔ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_128" n="HIAT:w" s="T11">dibər</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_132" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_134" n="HIAT:w" s="T12">Sʼimat</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <nts id="Seg_137" n="HIAT:ip">(</nts>
                  <ats e="T14" id="Seg_138" n="HIAT:non-pho" s="T13">…</ats>
                  <nts id="Seg_139" n="HIAT:ip">)</nts>
                  <nts id="Seg_140" n="HIAT:ip">)</nts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_144" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_146" n="HIAT:w" s="T14">Măn</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_149" n="HIAT:w" s="T15">aŋbə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_152" n="HIAT:w" s="T16">bar</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_155" n="HIAT:w" s="T17">kajlaʔbə</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_159" n="HIAT:w" s="T18">kunolzittə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_162" n="HIAT:w" s="T19">axota</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_166" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_168" n="HIAT:w" s="T20">Miʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_171" n="HIAT:w" s="T21">šidegöʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_173" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_175" n="HIAT:w" s="T22">kambivaʔ-</ts>
                  <nts id="Seg_176" n="HIAT:ip">)</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_179" n="HIAT:w" s="T23">kambibaʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_182" n="HIAT:w" s="T24">aktʼinə</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_186" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_188" n="HIAT:w" s="T25">Dĭ</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_190" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_192" n="HIAT:w" s="T26">ka-</ts>
                  <nts id="Seg_193" n="HIAT:ip">)</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_196" n="HIAT:w" s="T27">kambi</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_199" n="HIAT:w" s="T28">nalʼeva</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_203" n="HIAT:w" s="T29">măn</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_206" n="HIAT:w" s="T30">kambiam</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_209" n="HIAT:w" s="T31">naprava</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_213" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_215" n="HIAT:w" s="T32">Kăda</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_218" n="HIAT:w" s="T33">dĭm</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_221" n="HIAT:w" s="T34">kăštəliaʔi</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_225" n="HIAT:w" s="T35">măn</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_228" n="HIAT:w" s="T36">ej</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_231" n="HIAT:w" s="T37">tĭmnem</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_235" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_237" n="HIAT:w" s="T38">Čolaʔi</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_240" n="HIAT:w" s="T39">bar</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_243" n="HIAT:w" s="T40">nʼamga</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_246" n="HIAT:w" s="T41">tʼerə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_249" n="HIAT:w" s="T42">detleʔi</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_253" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_255" n="HIAT:w" s="T43">I</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_258" n="HIAT:w" s="T44">takšənə</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_260" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_262" n="HIAT:w" s="T45">en-</ts>
                  <nts id="Seg_263" n="HIAT:ip">)</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_266" n="HIAT:w" s="T46">endləʔbəʔjə</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_270" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_272" n="HIAT:w" s="T47">Ugaːndə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_275" n="HIAT:w" s="T48">tăŋ</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_278" n="HIAT:w" s="T49">togonorlaʔbəʔjə</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_282" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_284" n="HIAT:w" s="T50">Dʼijegən</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_286" n="HIAT:ip">(</nts>
                  <nts id="Seg_287" n="HIAT:ip">(</nts>
                  <ats e="T52" id="Seg_288" n="HIAT:non-pho" s="T51">…</ats>
                  <nts id="Seg_289" n="HIAT:ip">)</nts>
                  <nts id="Seg_290" n="HIAT:ip">)</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_293" n="HIAT:w" s="T52">Dʼijegən</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_296" n="HIAT:w" s="T53">ej</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_299" n="HIAT:w" s="T54">amnolaʔbəʔjə</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_303" n="HIAT:w" s="T55">bügən</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_306" n="HIAT:w" s="T56">ej</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_309" n="HIAT:w" s="T57">amnolaʔbə</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_313" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_315" n="HIAT:w" s="T58">Ugaːndə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_318" n="HIAT:w" s="T59">urgo</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_321" n="HIAT:w" s="T60">măja</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_325" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_327" n="HIAT:w" s="T61">Kăda</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_330" n="HIAT:w" s="T62">dĭʔnə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_333" n="HIAT:w" s="T63">kanzittə</ts>
                  <nts id="Seg_334" n="HIAT:ip">?</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_337" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_339" n="HIAT:w" s="T64">Ej</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_342" n="HIAT:w" s="T65">tĭmnem</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_346" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_348" n="HIAT:w" s="T66">Măndlial</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_351" n="HIAT:w" s="T67">dăk</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_355" n="HIAT:w" s="T68">bar</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_357" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_359" n="HIAT:w" s="T69">užu-</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_362" n="HIAT:w" s="T70">uzu-</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_365" n="HIAT:w" s="T71">saʔmə=</ts>
                  <nts id="Seg_366" n="HIAT:ip">)</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_369" n="HIAT:w" s="T72">üžü</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_372" n="HIAT:w" s="T73">saʔməluʔləj</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_376" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_378" n="HIAT:w" s="T74">Kambiʔi</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_381" n="HIAT:w" s="T75">dʼü</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_384" n="HIAT:w" s="T76">tĭlzittə</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_388" n="HIAT:u" s="T77">
                  <nts id="Seg_389" n="HIAT:ip">(</nts>
                  <ts e="T78" id="Seg_391" n="HIAT:w" s="T77">A-</ts>
                  <nts id="Seg_392" n="HIAT:ip">)</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_395" n="HIAT:w" s="T78">Aʔtʼi</ts>
                  <nts id="Seg_396" n="HIAT:ip">…</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_399" n="HIAT:u" s="T80">
                  <nts id="Seg_400" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_402" n="HIAT:w" s="T80">A-</ts>
                  <nts id="Seg_403" n="HIAT:ip">)</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_406" n="HIAT:w" s="T81">Kambiʔi</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_409" n="HIAT:w" s="T82">dʼü</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_412" n="HIAT:w" s="T83">tĭlzittə</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_416" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_418" n="HIAT:w" s="T84">Aʔtʼi</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_421" n="HIAT:w" s="T85">tʼazirzittə</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_425" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_427" n="HIAT:w" s="T86">Gibər</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_431" n="HIAT:w" s="T87">an-</ts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_435" n="HIAT:w" s="T88">dĭzeŋ</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_438" n="HIAT:w" s="T89">kambiʔi</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_442" n="HIAT:w" s="T90">kuŋgəŋ</ts>
                  <nts id="Seg_443" n="HIAT:ip">?</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_446" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_448" n="HIAT:w" s="T91">Dʼok</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_452" n="HIAT:w" s="T92">ej</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_455" n="HIAT:w" s="T93">kuŋgə</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_459" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_461" n="HIAT:w" s="T94">Šobiʔi</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_464" n="HIAT:w" s="T95">inezʼiʔi</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_468" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_470" n="HIAT:w" s="T96">Ej</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_473" n="HIAT:w" s="T97">kuŋgəŋ</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_476" n="HIAT:w" s="T98">turagən</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_479" n="HIAT:w" s="T99">nubiʔi</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_483" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_485" n="HIAT:w" s="T100">Daže</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_488" n="HIAT:w" s="T101">ej</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_491" n="HIAT:w" s="T102">surarbiʔi</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_495" n="HIAT:w" s="T103">bar</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_498" n="HIAT:w" s="T104">nubiʔi</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_502" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_504" n="HIAT:w" s="T105">Kanaʔ</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_506" n="HIAT:ip">(</nts>
                  <ts e="T107" id="Seg_508" n="HIAT:w" s="T106">măn</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_511" n="HIAT:w" s="T107">turagəʔ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_514" n="HIAT:w" s="T108">i</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_517" n="HIAT:w" s="T109">dön</ts>
                  <nts id="Seg_518" n="HIAT:ip">)</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_521" n="HIAT:w" s="T110">iʔ</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_524" n="HIAT:w" s="T111">nuʔ</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_528" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_530" n="HIAT:w" s="T112">Ej</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_533" n="HIAT:w" s="T113">kambiʔi</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_537" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_539" n="HIAT:w" s="T114">Turanə</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_542" n="HIAT:w" s="T115">saʔluʔpiʔi</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_546" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_548" n="HIAT:w" s="T116">I</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_551" n="HIAT:w" s="T117">alomniaʔi</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_554" n="HIAT:w" s="T118">bar</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_558" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_560" n="HIAT:w" s="T119">Turanə</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_563" n="HIAT:w" s="T120">nʼuʔdə</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_566" n="HIAT:w" s="T121">bar</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_569" n="HIAT:w" s="T122">suʔmiluʔpiʔi</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_573" n="HIAT:u" s="T123">
                  <nts id="Seg_574" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_576" n="HIAT:w" s="T123">Sagən</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_579" n="HIAT:w" s="T124">nüjüʔi</ts>
                  <nts id="Seg_580" n="HIAT:ip">)</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_583" n="HIAT:w" s="T125">naga</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_587" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_589" n="HIAT:w" s="T126">Gibərdə</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_592" n="HIAT:w" s="T127">kalla</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_595" n="HIAT:w" s="T128">dʼürbiʔi</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_598" n="HIAT:w" s="T129">alʼi</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_601" n="HIAT:w" s="T130">kubiʔi</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_604" n="HIAT:w" s="T131">bar</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_608" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_610" n="HIAT:w" s="T132">Măna</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_613" n="HIAT:w" s="T133">tože</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_616" n="HIAT:w" s="T134">pizʼiʔ</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_619" n="HIAT:w" s="T135">münörleʔbəlaʔ</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_623" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_625" n="HIAT:w" s="T136">Măna</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_628" n="HIAT:w" s="T137">tože</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_631" n="HIAT:w" s="T138">pi</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_633" n="HIAT:ip">(</nts>
                  <ts e="T140" id="Seg_635" n="HIAT:w" s="T139">ellə-</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_638" n="HIAT:w" s="T140">el=</ts>
                  <nts id="Seg_639" n="HIAT:ip">)</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_642" n="HIAT:w" s="T141">elləl</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_646" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_648" n="HIAT:w" s="T142">Il</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_651" n="HIAT:w" s="T143">jakšə</ts>
                  <nts id="Seg_652" n="HIAT:ip">,</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_655" n="HIAT:w" s="T144">ej</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_658" n="HIAT:w" s="T145">šamniabaʔ</ts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_662" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_664" n="HIAT:w" s="T146">Jakšə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_666" n="HIAT:ip">(</nts>
                  <ts e="T148" id="Seg_668" n="HIAT:w" s="T147">dʼăbaktərlabəʔ-</ts>
                  <nts id="Seg_669" n="HIAT:ip">)</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_672" n="HIAT:w" s="T148">dʼăbaktərlaʔbəbaʔ</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_676" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_678" n="HIAT:w" s="T149">Šənap</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_681" n="HIAT:w" s="T150">dʼăbaktərlaʔbəm</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_685" n="HIAT:w" s="T151">ej</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_688" n="HIAT:w" s="T152">šamniam</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_692" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_694" n="HIAT:w" s="T153">Komu</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_697" n="HIAT:w" s="T154">červəʔi</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_699" n="HIAT:ip">(</nts>
                  <ts e="T157" id="Seg_701" n="HIAT:w" s="T156">oʔbdəbiam</ts>
                  <nts id="Seg_702" n="HIAT:ip">,</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_705" n="HIAT:w" s="T157">büzʼiʔ</ts>
                  <nts id="Seg_706" n="HIAT:ip">)</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_708" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_710" n="HIAT:w" s="T158">dĭm</ts>
                  <nts id="Seg_711" n="HIAT:ip">)</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_713" n="HIAT:ip">(</nts>
                  <ts e="T160" id="Seg_715" n="HIAT:w" s="T159">mazə-</ts>
                  <nts id="Seg_716" n="HIAT:ip">)</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_719" n="HIAT:w" s="T160">bəzəbiam</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_723" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_725" n="HIAT:w" s="T161">Takšənə</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_728" n="HIAT:w" s="T162">embiem</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_732" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_734" n="HIAT:w" s="T163">Dĭbər</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_736" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_738" n="HIAT:w" s="T164">ejü=</ts>
                  <nts id="Seg_739" n="HIAT:ip">)</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_742" n="HIAT:w" s="T165">ejü</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_745" n="HIAT:w" s="T166">nuldəbiam</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_749" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_751" n="HIAT:w" s="T167">Dibər</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_754" n="HIAT:w" s="T168">bü</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_757" n="HIAT:w" s="T169">molaːmbi</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_761" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_763" n="HIAT:w" s="T170">Dĭgəttə</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_766" n="HIAT:w" s="T171">udam</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_769" n="HIAT:w" s="T172">kĭškəbiem</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_773" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_775" n="HIAT:w" s="T173">Udam</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_778" n="HIAT:w" s="T174">ugaːndə</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_781" n="HIAT:w" s="T175">tăŋ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_784" n="HIAT:w" s="T176">ĭzembiʔi</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_788" n="HIAT:w" s="T177">tüj</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_791" n="HIAT:w" s="T178">ej</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_794" n="HIAT:w" s="T179">ĭzemnieʔi</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_798" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_800" n="HIAT:w" s="T180">Smatri</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_804" n="HIAT:w" s="T181">kudaj</ts>
                  <nts id="Seg_805" n="HIAT:ip">…</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_808" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_810" n="HIAT:w" s="T183">Smatri</ts>
                  <nts id="Seg_811" n="HIAT:ip">,</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_814" n="HIAT:w" s="T184">kudaj</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_817" n="HIAT:w" s="T185">iʔ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_820" n="HIAT:w" s="T186">nöməlaʔ</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_824" n="HIAT:u" s="T187">
                  <ts e="T187.tx-PKZ.1" id="Seg_826" n="HIAT:w" s="T187">Погоди</ts>
                  <nts id="Seg_827" n="HIAT:ip">,</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187.tx-PKZ.2" id="Seg_830" n="HIAT:w" s="T187.tx-PKZ.1">как</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_832" n="HIAT:ip">(</nts>
                  <ts e="T191" id="Seg_834" n="HIAT:w" s="T187.tx-PKZ.2">это</ts>
                  <nts id="Seg_835" n="HIAT:ip">)</nts>
                  <nts id="Seg_836" n="HIAT:ip">…</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_839" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_841" n="HIAT:w" s="T191">Kanaʔ</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_844" n="HIAT:w" s="T192">eneidənenə</ts>
                  <nts id="Seg_845" n="HIAT:ip">!</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_848" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_850" n="HIAT:w" s="T193">Eneidənen</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_852" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_854" n="HIAT:w" s="T194">amnu-</ts>
                  <nts id="Seg_855" n="HIAT:ip">)</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_858" n="HIAT:w" s="T195">amnut</ts>
                  <nts id="Seg_859" n="HIAT:ip">!</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_862" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_864" n="HIAT:w" s="T196">Tüžöjən</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_867" n="HIAT:w" s="T197">amnut</ts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_871" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_873" n="HIAT:w" s="T198">Dĭ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_876" n="HIAT:w" s="T199">bar</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_879" n="HIAT:w" s="T200">tănan</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_881" n="HIAT:ip">(</nts>
                  <ts e="T202" id="Seg_883" n="HIAT:w" s="T201">muʔ-</ts>
                  <nts id="Seg_884" n="HIAT:ip">)</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_887" n="HIAT:w" s="T202">muʔləj</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_891" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_893" n="HIAT:w" s="T203">Sʼenʼiʔi</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_895" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_897" n="HIAT:w" s="T204">š-</ts>
                  <nts id="Seg_898" n="HIAT:ip">)</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_901" n="HIAT:w" s="T205">săbərəjdə</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206.tx-PKZ.1" id="Seg_905" n="HIAT:w" s="T206">a</ts>
                  <nts id="Seg_906" n="HIAT:ip">_</nts>
                  <ts e="T207" id="Seg_908" n="HIAT:w" s="T206.tx-PKZ.1">to</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_911" n="HIAT:w" s="T207">ugaːndə</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_914" n="HIAT:w" s="T208">iʔgö</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_917" n="HIAT:w" s="T209">balgaš</ts>
                  <nts id="Seg_918" n="HIAT:ip">.</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_921" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_923" n="HIAT:w" s="T210">Krilso</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_926" n="HIAT:w" s="T211">săbərəjdə</ts>
                  <nts id="Seg_927" n="HIAT:ip">!</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_930" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_932" n="HIAT:w" s="T212">Anbardə</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_934" n="HIAT:ip">(</nts>
                  <ts e="T214" id="Seg_936" n="HIAT:w" s="T213">un</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_939" n="HIAT:w" s="T214">ilem</ts>
                  <nts id="Seg_940" n="HIAT:ip">)</nts>
                  <nts id="Seg_941" n="HIAT:ip">,</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_944" n="HIAT:w" s="T215">embiem</ts>
                  <nts id="Seg_945" n="HIAT:ip">.</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_948" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_950" n="HIAT:w" s="T216">Măn</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_953" n="HIAT:w" s="T217">dĭm</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_956" n="HIAT:w" s="T218">kuʔpiom</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_960" n="HIAT:w" s="T219">măn</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_963" n="HIAT:w" s="T220">šaldə</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_966" n="HIAT:w" s="T221">iʔgö</ts>
                  <nts id="Seg_967" n="HIAT:ip">.</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_970" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_972" n="HIAT:w" s="T222">Măn</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_975" n="HIAT:w" s="T223">ugaːndə</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_978" n="HIAT:w" s="T224">kuštu</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_981" n="HIAT:w" s="T225">igem</ts>
                  <nts id="Seg_982" n="HIAT:ip">.</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_985" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_987" n="HIAT:w" s="T226">Dĭ</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_990" n="HIAT:w" s="T227">kuza</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_993" n="HIAT:w" s="T228">bar</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_996" n="HIAT:w" s="T229">sagəšdə</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_999" n="HIAT:w" s="T230">naga</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_1003" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_1005" n="HIAT:w" s="T231">Šĭket</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1008" n="HIAT:w" s="T232">ige</ts>
                  <nts id="Seg_1009" n="HIAT:ip">,</nts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_1012" n="HIAT:w" s="T233">dʼăbaktərzittə</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1015" n="HIAT:w" s="T234">ej</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_1018" n="HIAT:w" s="T235">molia</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_1022" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_1024" n="HIAT:w" s="T236">Kut</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_1027" n="HIAT:w" s="T237">ige</ts>
                  <nts id="Seg_1028" n="HIAT:ip">,</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1031" n="HIAT:w" s="T238">ej</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_1034" n="HIAT:w" s="T239">nünnie</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_1038" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_1040" n="HIAT:w" s="T240">Iʔ</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1043" n="HIAT:w" s="T241">parloʔ</ts>
                  <nts id="Seg_1044" n="HIAT:ip">!</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_1047" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_1049" n="HIAT:w" s="T242">Jakšə</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1052" n="HIAT:w" s="T243">amnoʔ</ts>
                  <nts id="Seg_1053" n="HIAT:ip">!</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_1056" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_1058" n="HIAT:w" s="T244">Ĭmbi</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1061" n="HIAT:w" s="T245">parloraʔbəl</ts>
                  <nts id="Seg_1062" n="HIAT:ip">?</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_1065" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_1067" n="HIAT:w" s="T246">Bostə</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1070" n="HIAT:w" s="T247">šĭkem</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1073" n="HIAT:w" s="T248">nünniöm</ts>
                  <nts id="Seg_1074" n="HIAT:ip">,</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1077" n="HIAT:w" s="T249">jakšə</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1080" n="HIAT:w" s="T250">dʼăbaktərliam</ts>
                  <nts id="Seg_1081" n="HIAT:ip">.</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1084" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_1086" n="HIAT:w" s="T251">Šoška</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1089" n="HIAT:w" s="T252">šonəga</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1092" n="HIAT:w" s="T253">i</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1095" n="HIAT:w" s="T254">kuremnaʔbə</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1098" n="HIAT:w" s="T255">bar</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1102" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1104" n="HIAT:w" s="T256">Nada</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1107" n="HIAT:w" s="T257">dĭm</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1110" n="HIAT:w" s="T258">bădəsʼtə</ts>
                  <nts id="Seg_1111" n="HIAT:ip">,</nts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259.tx-PKZ.1" id="Seg_1114" n="HIAT:w" s="T259">a</ts>
                  <nts id="Seg_1115" n="HIAT:ip">_</nts>
                  <ts e="T260" id="Seg_1117" n="HIAT:w" s="T259.tx-PKZ.1">to</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1120" n="HIAT:w" s="T260">dĭ</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1123" n="HIAT:w" s="T261">püjölia</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1126" n="HIAT:w" s="T262">bar</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_1130" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1132" n="HIAT:w" s="T263">Nada</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1134" n="HIAT:ip">(</nts>
                  <ts e="T265" id="Seg_1136" n="HIAT:w" s="T264">poʔ-</ts>
                  <nts id="Seg_1137" n="HIAT:ip">)</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1140" n="HIAT:w" s="T265">baltu</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1143" n="HIAT:w" s="T266">puʔməsʼtə</ts>
                  <nts id="Seg_1144" n="HIAT:ip">,</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1147" n="HIAT:w" s="T267">paʔi</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1149" n="HIAT:ip">(</nts>
                  <nts id="Seg_1150" n="HIAT:ip">(</nts>
                  <ats e="T269" id="Seg_1151" n="HIAT:non-pho" s="T268">PAUSE</ats>
                  <nts id="Seg_1152" n="HIAT:ip">)</nts>
                  <nts id="Seg_1153" n="HIAT:ip">)</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1155" n="HIAT:ip">(</nts>
                  <ts e="T270" id="Seg_1157" n="HIAT:w" s="T269">jaʔi-</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1160" n="HIAT:w" s="T270">jai-</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1163" n="HIAT:w" s="T271">jaʔi-</ts>
                  <nts id="Seg_1164" n="HIAT:ip">)</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1167" n="HIAT:w" s="T272">jaʔsʼittə</ts>
                  <nts id="Seg_1168" n="HIAT:ip">.</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1171" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_1173" n="HIAT:w" s="T273">Pʼeš</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1176" n="HIAT:w" s="T274">nenzittə</ts>
                  <nts id="Seg_1177" n="HIAT:ip">.</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1180" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1182" n="HIAT:w" s="T275">Pʼila</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1185" n="HIAT:w" s="T276">deppiʔi</ts>
                  <nts id="Seg_1186" n="HIAT:ip">,</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1189" n="HIAT:w" s="T277">ej</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1192" n="HIAT:w" s="T278">tĭmneʔi</ts>
                  <nts id="Seg_1193" n="HIAT:ip">,</nts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1196" n="HIAT:w" s="T279">ĭmbi</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1199" n="HIAT:w" s="T280">dĭzʼiʔ</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1202" n="HIAT:w" s="T281">azittə</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1206" n="HIAT:u" s="T282">
                  <nts id="Seg_1207" n="HIAT:ip">(</nts>
                  <ts e="T283" id="Seg_1209" n="HIAT:w" s="T282">Arla-</ts>
                  <nts id="Seg_1210" n="HIAT:ip">)</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1213" n="HIAT:w" s="T283">Varlam</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1216" n="HIAT:w" s="T284">šoləj</ts>
                  <nts id="Seg_1217" n="HIAT:ip">,</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1220" n="HIAT:w" s="T285">tʼazirləj</ts>
                  <nts id="Seg_1221" n="HIAT:ip">.</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1224" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1226" n="HIAT:w" s="T286">Bazaj</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1229" n="HIAT:w" s="T287">pʼeš</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1232" n="HIAT:w" s="T288">deppiʔi</ts>
                  <nts id="Seg_1233" n="HIAT:ip">.</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1236" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1238" n="HIAT:w" s="T289">Turanə</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1241" n="HIAT:w" s="T290">nuldəbiʔi</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1245" n="HIAT:u" s="T291">
                  <nts id="Seg_1246" n="HIAT:ip">(</nts>
                  <ts e="T292" id="Seg_1248" n="HIAT:w" s="T291">Šoj-</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1251" n="HIAT:w" s="T292">šoj-</ts>
                  <nts id="Seg_1252" n="HIAT:ip">)</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1255" n="HIAT:w" s="T293">Šojdʼonə</ts>
                  <nts id="Seg_1256" n="HIAT:ip">.</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1259" n="HIAT:u" s="T294">
                  <nts id="Seg_1260" n="HIAT:ip">(</nts>
                  <ts e="T295" id="Seg_1262" n="HIAT:w" s="T294">Pasə</ts>
                  <nts id="Seg_1263" n="HIAT:ip">)</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1266" n="HIAT:w" s="T295">šojdʼo</ts>
                  <nts id="Seg_1267" n="HIAT:ip">,</nts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1270" n="HIAT:w" s="T296">dĭ</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1273" n="HIAT:w" s="T297">bar</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1276" n="HIAT:w" s="T298">nendluʔpi</ts>
                  <nts id="Seg_1277" n="HIAT:ip">.</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1280" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1282" n="HIAT:w" s="T299">Bər</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1285" n="HIAT:w" s="T300">šobi</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1289" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1291" n="HIAT:w" s="T301">Dĭgəttə</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1294" n="HIAT:w" s="T302">bar</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1297" n="HIAT:w" s="T303">əʔpiʔi</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1300" n="HIAT:w" s="T304">nʼiʔdə</ts>
                  <nts id="Seg_1301" n="HIAT:ip">.</nts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1304" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1306" n="HIAT:w" s="T305">Ugaːndə</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1309" n="HIAT:w" s="T306">bər</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1312" n="HIAT:w" s="T307">iʔgö</ts>
                  <nts id="Seg_1313" n="HIAT:ip">.</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1316" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1318" n="HIAT:w" s="T308">Varlam</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1321" n="HIAT:w" s="T309">šoləj</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1324" n="HIAT:w" s="T310">i</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1327" n="HIAT:w" s="T311">dʼazirləj</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1331" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1333" n="HIAT:w" s="T312">Bar</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1336" n="HIAT:w" s="T313">bər</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1339" n="HIAT:w" s="T314">sʼimam</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1342" n="HIAT:w" s="T315">amnalaʔbəʔjə</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1346" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1348" n="HIAT:w" s="T316">Dĭgəttə</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1351" n="HIAT:w" s="T317">Varlam</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1354" n="HIAT:w" s="T318">šobi</ts>
                  <nts id="Seg_1355" n="HIAT:ip">,</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1358" n="HIAT:w" s="T319">pi</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1361" n="HIAT:w" s="T320">deʔpi</ts>
                  <nts id="Seg_1362" n="HIAT:ip">.</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1365" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1367" n="HIAT:w" s="T321">Dʼünə</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1370" n="HIAT:w" s="T322">embi</ts>
                  <nts id="Seg_1371" n="HIAT:ip">.</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1374" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1376" n="HIAT:w" s="T323">I</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1379" n="HIAT:w" s="T324">pʼešdə</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1382" n="HIAT:w" s="T325">nuldəbi</ts>
                  <nts id="Seg_1383" n="HIAT:ip">.</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1386" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1388" n="HIAT:w" s="T326">Dĭgəttə</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1391" n="HIAT:w" s="T327">pʼeš</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1394" n="HIAT:w" s="T328">nendəbiʔi</ts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1398" n="HIAT:w" s="T329">amnolaʔbəʔjə</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1401" n="HIAT:w" s="T330">i</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1403" n="HIAT:ip">(</nts>
                  <ts e="T332" id="Seg_1405" n="HIAT:w" s="T331">ejü-</ts>
                  <nts id="Seg_1406" n="HIAT:ip">)</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1409" n="HIAT:w" s="T332">ejüleʔbəʔjə</ts>
                  <nts id="Seg_1410" n="HIAT:ip">.</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1413" n="HIAT:u" s="T333">
                  <ts e="T335" id="Seg_1415" n="HIAT:w" s="T333">чтобы</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1418" n="HIAT:w" s="T335">не</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1421" n="HIAT:w" s="T336">ошибиться</ts>
                  <nts id="Seg_1422" n="HIAT:ip">.</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T444" id="Seg_1424" n="sc" s="T338">
               <ts e="T343" id="Seg_1426" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1428" n="HIAT:w" s="T338">Părgam</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1431" n="HIAT:w" s="T339">bar</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1433" n="HIAT:ip">(</nts>
                  <ts e="T341" id="Seg_1435" n="HIAT:w" s="T340">dʼügən=</ts>
                  <nts id="Seg_1436" n="HIAT:ip">)</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1439" n="HIAT:w" s="T341">dʼügən</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1442" n="HIAT:w" s="T342">iʔböbi</ts>
                  <nts id="Seg_1443" n="HIAT:ip">.</nts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1446" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1448" n="HIAT:w" s="T343">Üjüʔi</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1451" n="HIAT:w" s="T344">bar</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1454" n="HIAT:w" s="T345">kĭškəbiʔi</ts>
                  <nts id="Seg_1455" n="HIAT:ip">.</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1458" n="HIAT:u" s="T346">
                  <nts id="Seg_1459" n="HIAT:ip">(</nts>
                  <ts e="T347" id="Seg_1461" n="HIAT:w" s="T346">Dĭn=</ts>
                  <nts id="Seg_1462" n="HIAT:ip">)</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1465" n="HIAT:w" s="T347">Dĭn</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1468" n="HIAT:w" s="T348">bar</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1471" n="HIAT:w" s="T349">balgaš</ts>
                  <nts id="Seg_1472" n="HIAT:ip">,</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1475" n="HIAT:w" s="T350">măn</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1478" n="HIAT:w" s="T351">dĭm</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1480" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1482" n="HIAT:w" s="T352">ibiel=</ts>
                  <nts id="Seg_1483" n="HIAT:ip">)</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1486" n="HIAT:w" s="T353">ibiem</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1489" n="HIAT:w" s="T354">da</ts>
                  <nts id="Seg_1490" n="HIAT:ip">,</nts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1493" n="HIAT:w" s="T355">bünə</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1495" n="HIAT:ip">(</nts>
                  <ts e="T357" id="Seg_1497" n="HIAT:w" s="T356">kumbial=</ts>
                  <nts id="Seg_1498" n="HIAT:ip">)</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1501" n="HIAT:w" s="T357">kumbiam</ts>
                  <nts id="Seg_1502" n="HIAT:ip">,</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1505" n="HIAT:w" s="T358">štobɨ</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1508" n="HIAT:w" s="T359">dʼüʔpi</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1511" n="HIAT:w" s="T360">mobi</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1515" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1517" n="HIAT:w" s="T361">Dĭgəttə</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1520" n="HIAT:w" s="T362">toʔnarbiam</ts>
                  <nts id="Seg_1521" n="HIAT:ip">.</nts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1524" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1526" n="HIAT:w" s="T363">Bəzəbiam</ts>
                  <nts id="Seg_1527" n="HIAT:ip">.</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1530" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1532" n="HIAT:w" s="T364">Dĭgəttə</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1535" n="HIAT:w" s="T365">edəbiem</ts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1539" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1541" n="HIAT:w" s="T366">Bü</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1544" n="HIAT:w" s="T367">bar</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1547" n="HIAT:w" s="T368">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1550" n="HIAT:w" s="T369">dĭgəʔ</ts>
                  <nts id="Seg_1551" n="HIAT:ip">.</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1554" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1556" n="HIAT:w" s="T370">Dʼaga</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1559" n="HIAT:w" s="T371">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_1560" n="HIAT:ip">.</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1563" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1565" n="HIAT:w" s="T372">I</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1568" n="HIAT:w" s="T373">kuza</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1571" n="HIAT:w" s="T374">nuʔməleʔbə</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1574" n="HIAT:w" s="T375">büjleʔ</ts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1578" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1580" n="HIAT:w" s="T376">Girgitdə</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1583" n="HIAT:w" s="T377">il</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1586" n="HIAT:w" s="T378">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_1587" n="HIAT:ip">.</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_1590" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1592" n="HIAT:w" s="T379">Măn</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1594" n="HIAT:ip">(</nts>
                  <ts e="T381" id="Seg_1596" n="HIAT:w" s="T380">dĭm=</ts>
                  <nts id="Seg_1597" n="HIAT:ip">)</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1600" n="HIAT:w" s="T381">dĭzem</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1603" n="HIAT:w" s="T382">ej</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1606" n="HIAT:w" s="T383">tĭmnem</ts>
                  <nts id="Seg_1607" n="HIAT:ip">.</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1610" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_1612" n="HIAT:w" s="T384">Ĭmbi</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1614" n="HIAT:ip">(</nts>
                  <ts e="T386" id="Seg_1616" n="HIAT:w" s="T385">dĭ-</ts>
                  <nts id="Seg_1617" n="HIAT:ip">)</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1620" n="HIAT:w" s="T386">dĭzeŋ</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1623" n="HIAT:w" s="T387">dĭn</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1626" n="HIAT:w" s="T388">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_1627" n="HIAT:ip">.</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1630" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1632" n="HIAT:w" s="T389">Da</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1635" n="HIAT:w" s="T390">dĭ</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1638" n="HIAT:w" s="T391">nʼi</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1641" n="HIAT:w" s="T392">da</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1644" n="HIAT:w" s="T393">koʔbdo</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1647" n="HIAT:w" s="T394">šonəgaʔi</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1651" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1653" n="HIAT:w" s="T395">Nada</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1656" n="HIAT:w" s="T396">sut</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1659" n="HIAT:w" s="T397">kămnasʼtə</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1662" n="HIAT:w" s="T398">šojdʼonə</ts>
                  <nts id="Seg_1663" n="HIAT:ip">.</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1666" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1668" n="HIAT:w" s="T399">Puskaj</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1671" n="HIAT:w" s="T400">namzəga</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1674" n="HIAT:w" s="T401">moləj</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1678" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_1680" n="HIAT:w" s="T402">Tüjö</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1683" n="HIAT:w" s="T403">kalla</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1685" n="HIAT:ip">(</nts>
                  <ts e="T405" id="Seg_1687" n="HIAT:w" s="T404">dʼürbi-</ts>
                  <nts id="Seg_1688" n="HIAT:ip">)</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1691" n="HIAT:w" s="T405">dʼürbibeʔ</ts>
                  <nts id="Seg_1692" n="HIAT:ip">.</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1695" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1697" n="HIAT:w" s="T406">No</ts>
                  <nts id="Seg_1698" n="HIAT:ip">,</nts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1700" n="HIAT:ip">(</nts>
                  <ts e="T408" id="Seg_1702" n="HIAT:w" s="T407">kanaʔ=</ts>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1706" n="HIAT:w" s="T408">kangaʔ</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1709" n="HIAT:w" s="T409">kudajzʼiʔ</ts>
                  <nts id="Seg_1710" n="HIAT:ip">.</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1713" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1715" n="HIAT:w" s="T410">Miʔ</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1718" n="HIAT:w" s="T411">ugaːndə</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1721" n="HIAT:w" s="T412">iʔgö</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1724" n="HIAT:w" s="T413">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_1725" n="HIAT:ip">.</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1728" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1730" n="HIAT:w" s="T414">Tüj</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1733" n="HIAT:w" s="T415">kanzittə</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1736" n="HIAT:w" s="T416">nada</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1739" n="HIAT:w" s="T417">maːndə</ts>
                  <nts id="Seg_1740" n="HIAT:ip">.</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1743" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1745" n="HIAT:w" s="T418">Da</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1748" n="HIAT:w" s="T419">kunolzittə</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1751" n="HIAT:w" s="T420">iʔbəsʼtə</ts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1755" n="HIAT:u" s="T421">
                  <nts id="Seg_1756" n="HIAT:ip">(</nts>
                  <ts e="T422" id="Seg_1758" n="HIAT:w" s="T421">Iʔ</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1761" n="HIAT:w" s="T422">maktanarəbaʔ-</ts>
                  <nts id="Seg_1762" n="HIAT:ip">)</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1765" n="HIAT:w" s="T423">Iʔ</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1768" n="HIAT:w" s="T424">maktanarəʔ</ts>
                  <nts id="Seg_1769" n="HIAT:ip">.</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1772" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1774" n="HIAT:w" s="T425">Ĭmbidə</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1777" n="HIAT:w" s="T426">ej</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1780" n="HIAT:w" s="T427">abial</ts>
                  <nts id="Seg_1781" n="HIAT:ip">,</nts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1784" n="HIAT:w" s="T428">a</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1786" n="HIAT:ip">(</nts>
                  <ts e="T430" id="Seg_1788" n="HIAT:w" s="T429">bar</ts>
                  <nts id="Seg_1789" n="HIAT:ip">)</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1792" n="HIAT:w" s="T430">maktanarbial</ts>
                  <nts id="Seg_1793" n="HIAT:ip">.</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1796" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1798" n="HIAT:w" s="T431">Kunolzittə</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1801" n="HIAT:w" s="T432">iʔbəsʼtə</ts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1805" n="HIAT:w" s="T433">a</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1808" n="HIAT:w" s="T434">karəldʼan</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1810" n="HIAT:ip">(</nts>
                  <ts e="T436" id="Seg_1812" n="HIAT:w" s="T435">šagəštə</ts>
                  <nts id="Seg_1813" n="HIAT:ip">)</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1816" n="HIAT:w" s="T436">šoləj</ts>
                  <nts id="Seg_1817" n="HIAT:ip">.</nts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1820" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1822" n="HIAT:w" s="T437">Kuznektə</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1825" n="HIAT:w" s="T438">kuza</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1828" n="HIAT:w" s="T439">šobi</ts>
                  <nts id="Seg_1829" n="HIAT:ip">,</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1832" n="HIAT:w" s="T440">a</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1835" n="HIAT:w" s="T441">măn</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1837" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1839" n="HIAT:w" s="T442">pi</ts>
                  <nts id="Seg_1840" n="HIAT:ip">)</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1842" n="HIAT:ip">(</nts>
                  <ts e="T444" id="Seg_1844" n="HIAT:w" s="T443">kabarluʔpiam</ts>
                  <nts id="Seg_1845" n="HIAT:ip">)</nts>
                  <nts id="Seg_1846" n="HIAT:ip">.</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_1848" n="sc" s="T446">
               <ts e="T448" id="Seg_1850" n="HIAT:u" s="T446">
                  <ts e="T446.tx-PKZ.1" id="Seg_1852" n="HIAT:w" s="T446">Запри</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1854" n="HIAT:ip">(</nts>
                  <nts id="Seg_1855" n="HIAT:ip">(</nts>
                  <ats e="T448" id="Seg_1856" n="HIAT:non-pho" s="T446.tx-PKZ.1">DMG</ats>
                  <nts id="Seg_1857" n="HIAT:ip">)</nts>
                  <nts id="Seg_1858" n="HIAT:ip">)</nts>
                  <nts id="Seg_1859" n="HIAT:ip">.</nts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1862" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1864" n="HIAT:w" s="T448">Pi</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1867" n="HIAT:w" s="T449">kabarluʔpiom</ts>
                  <nts id="Seg_1868" n="HIAT:ip">,</nts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1871" n="HIAT:w" s="T450">i</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1874" n="HIAT:w" s="T451">kuʔpiom</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1877" n="HIAT:w" s="T452">dĭ</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1880" n="HIAT:w" s="T453">kuzam</ts>
                  <nts id="Seg_1881" n="HIAT:ip">.</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1884" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1886" n="HIAT:w" s="T454">Kuza</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1889" n="HIAT:w" s="T455">kuzam</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1892" n="HIAT:w" s="T456">bar</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1895" n="HIAT:w" s="T457">dagajzʼiʔ</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1898" n="HIAT:w" s="T458">dʼagarluʔpi</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1901" n="HIAT:w" s="T459">i</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1904" n="HIAT:w" s="T460">dĭ</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1907" n="HIAT:w" s="T461">külaːmbi</ts>
                  <nts id="Seg_1908" n="HIAT:ip">.</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1911" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1913" n="HIAT:w" s="T462">Dĭn</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1916" n="HIAT:w" s="T463">onʼiʔ</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1919" n="HIAT:w" s="T464">ne</ts>
                  <nts id="Seg_1920" n="HIAT:ip">,</nts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1923" n="HIAT:w" s="T465">Kazan</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1926" n="HIAT:w" s="T466">turagən</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1929" n="HIAT:w" s="T467">onʼiʔ</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1932" n="HIAT:w" s="T468">ne</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1935" n="HIAT:w" s="T469">tibibə</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1938" n="HIAT:w" s="T470">bar</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1941" n="HIAT:w" s="T471">jaʔpi</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1944" n="HIAT:w" s="T472">baltuzʼiʔ</ts>
                  <nts id="Seg_1945" n="HIAT:ip">,</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1948" n="HIAT:w" s="T473">ulut</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1951" n="HIAT:w" s="T474">sajjaʔpi</ts>
                  <nts id="Seg_1952" n="HIAT:ip">.</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1955" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1957" n="HIAT:w" s="T475">Dĭn</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1960" n="HIAT:w" s="T476">tibi</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1963" n="HIAT:w" s="T477">onʼiʔ</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1966" n="HIAT:w" s="T478">kunolluʔpi</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1969" n="HIAT:w" s="T479">bar</ts>
                  <nts id="Seg_1970" n="HIAT:ip">.</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1973" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1975" n="HIAT:w" s="T480">Ugaːndə</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1977" n="HIAT:ip">(</nts>
                  <ts e="T482" id="Seg_1979" n="HIAT:w" s="T481">pimn-</ts>
                  <nts id="Seg_1980" n="HIAT:ip">)</nts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1983" n="HIAT:w" s="T482">pimbiem</ts>
                  <nts id="Seg_1984" n="HIAT:ip">.</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1987" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_1989" n="HIAT:w" s="T483">Kăda</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1992" n="HIAT:w" s="T484">măna</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1995" n="HIAT:w" s="T485">ulum</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1998" n="HIAT:w" s="T486">ej</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_2001" n="HIAT:w" s="T487">jaʔpi</ts>
                  <nts id="Seg_2002" n="HIAT:ip">.</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2005" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_2007" n="HIAT:w" s="T488">Dĭ</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2010" n="HIAT:w" s="T489">bar</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2013" n="HIAT:w" s="T490">nanəʔzəbi</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_2016" n="HIAT:w" s="T491">ibi</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2019" n="HIAT:w" s="T492">i</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2022" n="HIAT:w" s="T493">šide</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_2025" n="HIAT:w" s="T494">esseŋ</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2028" n="HIAT:w" s="T495">maluʔpiʔi</ts>
                  <nts id="Seg_2029" n="HIAT:ip">.</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_2032" n="HIAT:u" s="T496">
                  <nts id="Seg_2033" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_2035" n="HIAT:w" s="T496">Uždə-</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2038" n="HIAT:w" s="T497">užə-</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_2041" n="HIAT:w" s="T498">u-</ts>
                  <nts id="Seg_2042" n="HIAT:ip">)</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2045" n="HIAT:w" s="T499">Bar</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2048" n="HIAT:w" s="T500">sumna</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2051" n="HIAT:w" s="T501">pʼe</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2054" n="HIAT:w" s="T502">kalla</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2057" n="HIAT:w" s="T503">dʼürbi</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2061" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_2063" n="HIAT:w" s="T504">Taldʼen</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2066" n="HIAT:w" s="T505">ej</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2069" n="HIAT:w" s="T506">ibiel</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_2073" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2075" n="HIAT:w" s="T507">Teinen</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2077" n="HIAT:ip">(</nts>
                  <ts e="T509" id="Seg_2079" n="HIAT:w" s="T508">šol</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2082" n="HIAT:w" s="T509">-</ts>
                  <nts id="Seg_2083" n="HIAT:ip">)</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2086" n="HIAT:w" s="T510">šobial</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2089" n="HIAT:w" s="T511">nʼilgösʼtə</ts>
                  <nts id="Seg_2090" n="HIAT:ip">.</nts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T514" id="Seg_2093" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_2095" n="HIAT:w" s="T512">Ĭmbi</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2098" n="HIAT:w" s="T513">dʼăbaktərlaʔbəbaʔ</ts>
                  <nts id="Seg_2099" n="HIAT:ip">?</nts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_2102" n="HIAT:u" s="T514">
                  <ts e="T515" id="Seg_2104" n="HIAT:w" s="T514">Măn</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2107" n="HIAT:w" s="T515">teinen</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2110" n="HIAT:w" s="T516">kudaj</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2112" n="HIAT:ip">(</nts>
                  <ts e="T518" id="Seg_2114" n="HIAT:w" s="T517">numanə</ts>
                  <nts id="Seg_2115" n="HIAT:ip">)</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2117" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_2119" n="HIAT:w" s="T518">üzə-</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2122" n="HIAT:w" s="T519">üz-</ts>
                  <nts id="Seg_2123" n="HIAT:ip">)</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2126" n="HIAT:w" s="T520">üzləm</ts>
                  <nts id="Seg_2127" n="HIAT:ip">.</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2130" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_2132" n="HIAT:w" s="T521">Kudaj</ts>
                  <nts id="Seg_2133" n="HIAT:ip">,</nts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2136" n="HIAT:w" s="T522">šoʔ</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2139" n="HIAT:w" s="T523">măna</ts>
                  <nts id="Seg_2140" n="HIAT:ip">!</nts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2143" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2145" n="HIAT:w" s="T524">Nʼilgöt</ts>
                  <nts id="Seg_2146" n="HIAT:ip">,</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2149" n="HIAT:w" s="T525">kăda</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2152" n="HIAT:w" s="T526">măn</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2155" n="HIAT:w" s="T527">üzliem</ts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2159" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_2161" n="HIAT:w" s="T528">Sĭjbə</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2163" n="HIAT:ip">(</nts>
                  <ts e="T530" id="Seg_2165" n="HIAT:w" s="T529">măndərdə-</ts>
                  <nts id="Seg_2166" n="HIAT:ip">)</nts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2169" n="HIAT:w" s="T530">măndəʔ</ts>
                  <nts id="Seg_2170" n="HIAT:ip">.</nts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2173" n="HIAT:u" s="T531">
                  <nts id="Seg_2174" n="HIAT:ip">(</nts>
                  <ts e="T532" id="Seg_2176" n="HIAT:w" s="T531">Padʼi</ts>
                  <nts id="Seg_2177" n="HIAT:ip">)</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2179" n="HIAT:ip">(</nts>
                  <ts e="T533" id="Seg_2181" n="HIAT:w" s="T532">b-</ts>
                  <nts id="Seg_2182" n="HIAT:ip">)</nts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2185" n="HIAT:w" s="T533">arəliaʔ</ts>
                  <nts id="Seg_2186" n="HIAT:ip">!</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_2189" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_2191" n="HIAT:w" s="T534">A</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2194" n="HIAT:w" s="T535">ej</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2197" n="HIAT:w" s="T536">arə</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2200" n="HIAT:w" s="T537">dăk</ts>
                  <nts id="Seg_2201" n="HIAT:ip">,</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2204" n="HIAT:w" s="T538">bəzit</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2207" n="HIAT:w" s="T539">kemzʼiʔ</ts>
                  <nts id="Seg_2208" n="HIAT:ip">!</nts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_2211" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2213" n="HIAT:w" s="T540">Tăn</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2216" n="HIAT:w" s="T541">pagən</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2219" n="HIAT:w" s="T542">edəbiel</ts>
                  <nts id="Seg_2220" n="HIAT:ip">.</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2223" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_2225" n="HIAT:w" s="T543">Kembə</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2228" n="HIAT:w" s="T544">mʼaŋnaʔpi</ts>
                  <nts id="Seg_2229" n="HIAT:ip">.</nts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_2232" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2234" n="HIAT:w" s="T545">Bəzit</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2237" n="HIAT:w" s="T546">măna</ts>
                  <nts id="Seg_2238" n="HIAT:ip">!</nts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2241" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_2243" n="HIAT:w" s="T547">Jakšə</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2246" n="HIAT:w" s="T548">bəzit</ts>
                  <nts id="Seg_2247" n="HIAT:ip">!</nts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2250" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2252" n="HIAT:w" s="T549">Tăn</ts>
                  <nts id="Seg_2253" n="HIAT:ip">,</nts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2256" n="HIAT:w" s="T550">kudaj</ts>
                  <nts id="Seg_2257" n="HIAT:ip">,</nts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2260" n="HIAT:w" s="T551">šaldə</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2263" n="HIAT:w" s="T552">iʔgö</ts>
                  <nts id="Seg_2264" n="HIAT:ip">.</nts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2267" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2269" n="HIAT:w" s="T553">Bar</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2271" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2273" n="HIAT:w" s="T554">i-</ts>
                  <nts id="Seg_2274" n="HIAT:ip">)</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2277" n="HIAT:w" s="T555">iləm</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2279" n="HIAT:ip">(</nts>
                  <ts e="T557" id="Seg_2281" n="HIAT:w" s="T556">tʼa-</ts>
                  <nts id="Seg_2282" n="HIAT:ip">)</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2285" n="HIAT:w" s="T557">ajirlaʔbəl</ts>
                  <nts id="Seg_2286" n="HIAT:ip">.</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_2289" n="HIAT:u" s="T558">
                  <nts id="Seg_2290" n="HIAT:ip">(</nts>
                  <ts e="T559" id="Seg_2292" n="HIAT:w" s="T558">M-</ts>
                  <nts id="Seg_2293" n="HIAT:ip">)</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2296" n="HIAT:w" s="T559">Ipek</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2299" n="HIAT:w" s="T560">mĭliel</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2303" n="HIAT:u" s="T561">
                  <nts id="Seg_2304" n="HIAT:ip">(</nts>
                  <ts e="T562" id="Seg_2306" n="HIAT:w" s="T561">Kuj-</ts>
                  <nts id="Seg_2307" n="HIAT:ip">)</nts>
                  <nts id="Seg_2308" n="HIAT:ip">.</nts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2311" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2313" n="HIAT:w" s="T562">Tăn</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2316" n="HIAT:w" s="T563">kudaj</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2319" n="HIAT:w" s="T564">šaldə</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2322" n="HIAT:w" s="T565">iʔgö</ts>
                  <nts id="Seg_2323" n="HIAT:ip">.</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2326" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2328" n="HIAT:w" s="T566">Tăn</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2330" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_2332" n="HIAT:w" s="T567">il=</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2335" n="HIAT:w" s="T568">il=</ts>
                  <nts id="Seg_2336" n="HIAT:ip">)</nts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2339" n="HIAT:w" s="T569">il</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2342" n="HIAT:w" s="T570">ajirial</ts>
                  <nts id="Seg_2343" n="HIAT:ip">.</nts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2346" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2348" n="HIAT:w" s="T571">Ipek</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2351" n="HIAT:w" s="T572">dĭzeŋdə</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2354" n="HIAT:w" s="T573">mĭliel</ts>
                  <nts id="Seg_2355" n="HIAT:ip">.</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2358" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2360" n="HIAT:w" s="T574">Bar</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2363" n="HIAT:w" s="T575">kujnek</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2366" n="HIAT:w" s="T576">mĭliel</ts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2370" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2372" n="HIAT:w" s="T577">Bü</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2375" n="HIAT:w" s="T578">mĭliel</ts>
                  <nts id="Seg_2376" n="HIAT:ip">.</nts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2379" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2381" n="HIAT:w" s="T579">I</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2384" n="HIAT:w" s="T580">măna</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2387" n="HIAT:w" s="T581">ajiraʔ</ts>
                  <nts id="Seg_2388" n="HIAT:ip">!</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2391" n="HIAT:u" s="T582">
                  <ts e="T582.tx-PKZ.1" id="Seg_2393" n="HIAT:w" s="T582">Чего-то</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2395" n="HIAT:ip">(</nts>
                  <ts e="T587" id="Seg_2397" n="HIAT:w" s="T582.tx-PKZ.1">никак</ts>
                  <nts id="Seg_2398" n="HIAT:ip">)</nts>
                  <nts id="Seg_2399" n="HIAT:ip">…</nts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2402" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2404" n="HIAT:w" s="T587">Tăn</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2407" n="HIAT:w" s="T588">bar</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2410" n="HIAT:w" s="T589">ejü</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2413" n="HIAT:w" s="T590">öʔliel</ts>
                  <nts id="Seg_2414" n="HIAT:ip">.</nts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2417" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_2419" n="HIAT:w" s="T591">Surno</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2422" n="HIAT:w" s="T592">mĭliel</ts>
                  <nts id="Seg_2423" n="HIAT:ip">,</nts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2426" n="HIAT:w" s="T593">štobɨ</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2429" n="HIAT:w" s="T594">ipek</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2432" n="HIAT:w" s="T595">özerleʔpi</ts>
                  <nts id="Seg_2433" n="HIAT:ip">.</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_2436" n="HIAT:u" s="T596">
                  <ts e="T596.tx-PKZ.1" id="Seg_2438" n="HIAT:w" s="T596">Чтобы</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596.tx-PKZ.2" id="Seg_2441" n="HIAT:w" s="T596.tx-PKZ.1">хлеб</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2443" n="HIAT:ip">(</nts>
                  <ts e="T596.tx-PKZ.3" id="Seg_2445" n="HIAT:w" s="T596.tx-PKZ.2">рос</ts>
                  <nts id="Seg_2446" n="HIAT:ip">/</nts>
                  <ts e="T599" id="Seg_2448" n="HIAT:w" s="T596.tx-PKZ.3">рожь</ts>
                  <nts id="Seg_2449" n="HIAT:ip">)</nts>
                  <nts id="Seg_2450" n="HIAT:ip">…</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2453" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2455" n="HIAT:w" s="T599">Kudajzʼiʔ</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2458" n="HIAT:w" s="T600">jakšə</ts>
                  <nts id="Seg_2459" n="HIAT:ip">,</nts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2462" n="HIAT:w" s="T601">gibər</ts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2465" n="HIAT:w" s="T602">kallal</ts>
                  <nts id="Seg_2466" n="HIAT:ip">,</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2469" n="HIAT:w" s="T603">kudajzʼiʔ</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2471" n="HIAT:ip">(</nts>
                  <ts e="T605" id="Seg_2473" n="HIAT:w" s="T604">vez-</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2476" n="HIAT:w" s="T605">vez-</ts>
                  <nts id="Seg_2477" n="HIAT:ip">)</nts>
                  <nts id="Seg_2478" n="HIAT:ip">;</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2481" n="HIAT:w" s="T606">ugaːndə</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2484" n="HIAT:w" s="T607">jakšə</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2487" n="HIAT:w" s="T608">kudajzʼiʔ</ts>
                  <nts id="Seg_2488" n="HIAT:ip">.</nts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2491" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_2493" n="HIAT:w" s="T609">Beržə</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2496" n="HIAT:w" s="T610">saʔməluʔpi</ts>
                  <nts id="Seg_2497" n="HIAT:ip">.</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2500" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2502" n="HIAT:w" s="T611">Măn</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2505" n="HIAT:w" s="T612">ibiem</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2508" n="HIAT:w" s="T613">da</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2511" n="HIAT:w" s="T614">turanə</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2514" n="HIAT:w" s="T615">deʔpiem</ts>
                  <nts id="Seg_2515" n="HIAT:ip">.</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2518" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2520" n="HIAT:w" s="T616">Jakšə</ts>
                  <nts id="Seg_2521" n="HIAT:ip">.</nts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2524" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2526" n="HIAT:w" s="T617">Surno</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2529" n="HIAT:w" s="T618">šonəga</ts>
                  <nts id="Seg_2530" n="HIAT:ip">,</nts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2533" n="HIAT:w" s="T619">ipek</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2536" n="HIAT:w" s="T620">özerləj</ts>
                  <nts id="Seg_2537" n="HIAT:ip">.</nts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2540" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2542" n="HIAT:w" s="T621">Măn</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2545" n="HIAT:w" s="T622">dĭʔnə</ts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2548" n="HIAT:w" s="T623">kabažarbiam</ts>
                  <nts id="Seg_2549" n="HIAT:ip">.</nts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2552" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2554" n="HIAT:w" s="T624">A</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2557" n="HIAT:w" s="T625">dĭ</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2560" n="HIAT:w" s="T626">măna</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2563" n="HIAT:w" s="T627">ej</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2566" n="HIAT:w" s="T628">kabažarlaʔbə</ts>
                  <nts id="Seg_2567" n="HIAT:ip">.</nts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2570" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2572" n="HIAT:w" s="T629">Ej</ts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2575" n="HIAT:w" s="T630">mĭlie</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2578" n="HIAT:w" s="T631">măna</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2581" n="HIAT:w" s="T632">togonorzittə</ts>
                  <nts id="Seg_2582" n="HIAT:ip">,</nts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2585" n="HIAT:w" s="T633">măn</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2588" n="HIAT:w" s="T634">bɨ</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2591" n="HIAT:w" s="T635">togonorbiam</ts>
                  <nts id="Seg_2592" n="HIAT:ip">,</nts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2595" n="HIAT:w" s="T636">a</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2598" n="HIAT:w" s="T637">dĭ</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2601" n="HIAT:w" s="T638">üge</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2603" n="HIAT:ip">(</nts>
                  <ts e="T640" id="Seg_2605" n="HIAT:w" s="T639">puʔlaʔbə</ts>
                  <nts id="Seg_2606" n="HIAT:ip">)</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2609" n="HIAT:w" s="T640">măna</ts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2613" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2615" n="HIAT:w" s="T641">Kurojək</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2618" n="HIAT:w" s="T642">bar</ts>
                  <nts id="Seg_2619" n="HIAT:ip">.</nts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2622" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2624" n="HIAT:w" s="T643">Ujuzʼiʔ</ts>
                  <nts id="Seg_2625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2627" n="HIAT:w" s="T644">bar</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2630" n="HIAT:w" s="T645">tonuʔpi</ts>
                  <nts id="Seg_2631" n="HIAT:ip">.</nts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2634" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2636" n="HIAT:w" s="T646">Ugaːndə</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2639" n="HIAT:w" s="T647">kurojək</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2642" n="HIAT:w" s="T648">men</ts>
                  <nts id="Seg_2643" n="HIAT:ip">.</nts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2646" n="HIAT:u" s="T649">
                  <nts id="Seg_2647" n="HIAT:ip">(</nts>
                  <ts e="T650" id="Seg_2649" n="HIAT:w" s="T649">Bal-</ts>
                  <nts id="Seg_2650" n="HIAT:ip">)</nts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2653" n="HIAT:w" s="T650">Bar</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2655" n="HIAT:ip">(</nts>
                  <ts e="T652" id="Seg_2657" n="HIAT:w" s="T651">tal-</ts>
                  <nts id="Seg_2658" n="HIAT:ip">)</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2661" n="HIAT:w" s="T652">talbəlia</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2664" n="HIAT:w" s="T653">il</ts>
                  <nts id="Seg_2665" n="HIAT:ip">.</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2668" n="HIAT:u" s="T654">
                  <ts e="T654.tx-PKZ.1" id="Seg_2670" n="HIAT:w" s="T654">Людей</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2673" n="HIAT:w" s="T654.tx-PKZ.1">кусает</ts>
                  <nts id="Seg_2674" n="HIAT:ip">.</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2677" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2679" n="HIAT:w" s="T656">Ugaːndə</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2682" n="HIAT:w" s="T657">kurojək</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2685" n="HIAT:w" s="T658">men</ts>
                  <nts id="Seg_2686" n="HIAT:ip">.</nts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2689" n="HIAT:u" s="T659">
                  <nts id="Seg_2690" n="HIAT:ip">(</nts>
                  <ts e="T660" id="Seg_2692" n="HIAT:w" s="T659">Ba-</ts>
                  <nts id="Seg_2693" n="HIAT:ip">)</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2696" n="HIAT:w" s="T660">Bar</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2699" n="HIAT:w" s="T661">il</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2702" n="HIAT:w" s="T662">talbərlaʔbə</ts>
                  <nts id="Seg_2703" n="HIAT:ip">.</nts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2706" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2708" n="HIAT:w" s="T663">I</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2711" n="HIAT:w" s="T664">măna</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2714" n="HIAT:w" s="T665">talbərbi</ts>
                  <nts id="Seg_2715" n="HIAT:ip">.</nts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2718" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_2720" n="HIAT:w" s="T666">Măn</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2723" n="HIAT:w" s="T667">šiʔ</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2726" n="HIAT:w" s="T668">edəʔpiem</ts>
                  <nts id="Seg_2727" n="HIAT:ip">,</nts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2730" n="HIAT:w" s="T669">šiʔ</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2733" n="HIAT:w" s="T670">ej</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2736" n="HIAT:w" s="T671">šobilaʔ</ts>
                  <nts id="Seg_2737" n="HIAT:ip">,</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2740" n="HIAT:w" s="T672">kundʼo</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2743" n="HIAT:w" s="T673">nagobilaʔ</ts>
                  <nts id="Seg_2744" n="HIAT:ip">.</nts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2747" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2749" n="HIAT:w" s="T674">Takšəgəʔ</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2752" n="HIAT:w" s="T675">ibiem</ts>
                  <nts id="Seg_2753" n="HIAT:ip">,</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2756" n="HIAT:w" s="T676">bəzəbiam</ts>
                  <nts id="Seg_2757" n="HIAT:ip">.</nts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2760" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2762" n="HIAT:w" s="T677">I</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2765" n="HIAT:w" s="T678">bazoʔ</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2768" n="HIAT:w" s="T679">embiem</ts>
                  <nts id="Seg_2769" n="HIAT:ip">.</nts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2772" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_2774" n="HIAT:w" s="T680">Teinöbam</ts>
                  <nts id="Seg_2775" n="HIAT:ip">,</nts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2778" n="HIAT:w" s="T681">teinöbam</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2781" n="HIAT:w" s="T682">teinen</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2784" n="HIAT:w" s="T683">bar</ts>
                  <nts id="Seg_2785" n="HIAT:ip">.</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2788" n="HIAT:u" s="T684">
                  <ts e="T686" id="Seg_2790" n="HIAT:w" s="T684">никак</ts>
                  <nts id="Seg_2791" n="HIAT:ip">.</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2794" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2796" n="HIAT:w" s="T686">Bünə</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2799" n="HIAT:w" s="T687">ej</ts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2802" n="HIAT:w" s="T688">saʔməlial</ts>
                  <nts id="Seg_2803" n="HIAT:ip">,</nts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2806" n="HIAT:w" s="T689">nʼuʔdun</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2809" n="HIAT:w" s="T690">bar</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2812" n="HIAT:w" s="T691">mĭŋgə</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2815" n="HIAT:w" s="T692">udazʼiʔ</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2818" n="HIAT:w" s="T693">i</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2821" n="HIAT:w" s="T694">üjüzʼiʔ</ts>
                  <nts id="Seg_2822" n="HIAT:ip">.</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2825" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2827" n="HIAT:w" s="T695">Bügən</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2830" n="HIAT:w" s="T696">bar</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2833" n="HIAT:w" s="T697">kandəgal</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2836" n="HIAT:w" s="T698">udazʼiʔ</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2839" n="HIAT:w" s="T699">i</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2842" n="HIAT:w" s="T700">üjüzʼiʔ</ts>
                  <nts id="Seg_2843" n="HIAT:ip">.</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2846" n="HIAT:u" s="T701">
                  <nts id="Seg_2847" n="HIAT:ip">(</nts>
                  <ts e="T702" id="Seg_2849" n="HIAT:w" s="T701">Lotka=</ts>
                  <nts id="Seg_2850" n="HIAT:ip">)</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2853" n="HIAT:w" s="T702">Lotkaizʼiʔ</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2856" n="HIAT:w" s="T703">bar</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2858" n="HIAT:ip">(</nts>
                  <ts e="T705" id="Seg_2860" n="HIAT:w" s="T704">bün-</ts>
                  <nts id="Seg_2861" n="HIAT:ip">)</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2864" n="HIAT:w" s="T705">büʔnə</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2867" n="HIAT:w" s="T706">kandəga</ts>
                  <nts id="Seg_2868" n="HIAT:ip">.</nts>
                  <nts id="Seg_2869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2871" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2873" n="HIAT:w" s="T707">Kola</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2876" n="HIAT:w" s="T708">dʼabəlaʔbə</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2879" n="HIAT:w" s="T709">bar</ts>
                  <nts id="Seg_2880" n="HIAT:ip">.</nts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2883" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2885" n="HIAT:w" s="T710">Nagurgöʔ</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2887" n="HIAT:ip">(</nts>
                  <ts e="T712" id="Seg_2889" n="HIAT:w" s="T711">tʼakamnəbaʔ</ts>
                  <nts id="Seg_2890" n="HIAT:ip">)</nts>
                  <nts id="Seg_2891" n="HIAT:ip">.</nts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2894" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2896" n="HIAT:w" s="T712">Onʼiʔ</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2899" n="HIAT:w" s="T713">tănan</ts>
                  <nts id="Seg_2900" n="HIAT:ip">,</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2903" n="HIAT:w" s="T714">onʼiʔ</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2906" n="HIAT:w" s="T715">măna</ts>
                  <nts id="Seg_2907" n="HIAT:ip">,</nts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2910" n="HIAT:w" s="T716">onʼiʔ</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2913" n="HIAT:w" s="T717">dĭʔnə</ts>
                  <nts id="Seg_2914" n="HIAT:ip">.</nts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2917" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2919" n="HIAT:w" s="T718">Šide</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2922" n="HIAT:w" s="T719">pʼel</ts>
                  <nts id="Seg_2923" n="HIAT:ip">,</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2926" n="HIAT:w" s="T720">nagur</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2929" n="HIAT:w" s="T721">pʼel</ts>
                  <nts id="Seg_2930" n="HIAT:ip">,</nts>
                  <nts id="Seg_2931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2933" n="HIAT:w" s="T722">teʔtə</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2936" n="HIAT:w" s="T723">pʼel</ts>
                  <nts id="Seg_2937" n="HIAT:ip">.</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2940" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2942" n="HIAT:w" s="T724">Dăra</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2944" n="HIAT:ip">(</nts>
                  <ts e="T726" id="Seg_2946" n="HIAT:w" s="T725">dĭ=</ts>
                  <nts id="Seg_2947" n="HIAT:ip">)</nts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2950" n="HIAT:w" s="T726">dĭzeŋ</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2953" n="HIAT:w" s="T727">bar</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2956" n="HIAT:w" s="T728">amnobiʔi</ts>
                  <nts id="Seg_2957" n="HIAT:ip">.</nts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2960" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2962" n="HIAT:w" s="T729">Onʼiʔ</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2965" n="HIAT:w" s="T730">inegən</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2968" n="HIAT:w" s="T731">măn</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2971" n="HIAT:w" s="T732">amnobiam</ts>
                  <nts id="Seg_2972" n="HIAT:ip">.</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_2975" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2977" n="HIAT:w" s="T733">Šide</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2980" n="HIAT:w" s="T734">inegən</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2983" n="HIAT:w" s="T735">nem</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2986" n="HIAT:w" s="T736">amnəbi</ts>
                  <nts id="Seg_2987" n="HIAT:ip">.</nts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2990" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_2992" n="HIAT:w" s="T737">Nagur</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2995" n="HIAT:w" s="T738">inegən</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2998" n="HIAT:w" s="T739">nʼim</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_3001" n="HIAT:w" s="T740">amnəbi</ts>
                  <nts id="Seg_3002" n="HIAT:ip">.</nts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_3005" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_3007" n="HIAT:w" s="T741">Teʔtə</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3010" n="HIAT:w" s="T742">inegən</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3013" n="HIAT:w" s="T743">koʔbdom</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3016" n="HIAT:w" s="T744">amnəbi</ts>
                  <nts id="Seg_3017" n="HIAT:ip">.</nts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T750" id="Seg_3020" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_3022" n="HIAT:w" s="T745">A</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3025" n="HIAT:w" s="T746">sumna</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3028" n="HIAT:w" s="T747">inegən</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3031" n="HIAT:w" s="T748">urgoja</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3034" n="HIAT:w" s="T749">amnəbi</ts>
                  <nts id="Seg_3035" n="HIAT:ip">.</nts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T828" id="Seg_3037" n="sc" s="T751">
               <ts e="T757" id="Seg_3039" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_3041" n="HIAT:w" s="T751">Dĭ</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3044" n="HIAT:w" s="T752">nʼi</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3047" n="HIAT:w" s="T753">šobi</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3050" n="HIAT:w" s="T754">miʔnʼibeʔ</ts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3053" n="HIAT:w" s="T755">kürzittə</ts>
                  <nts id="Seg_3054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3056" n="HIAT:w" s="T756">bar</ts>
                  <nts id="Seg_3057" n="HIAT:ip">.</nts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_3060" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_3062" n="HIAT:w" s="T757">Büžü</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3065" n="HIAT:w" s="T758">bar</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3068" n="HIAT:w" s="T759">šĭšəge</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_3071" n="HIAT:w" s="T760">moləj</ts>
                  <nts id="Seg_3072" n="HIAT:ip">.</nts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_3075" n="HIAT:u" s="T761">
                  <nts id="Seg_3076" n="HIAT:ip">(</nts>
                  <ts e="T762" id="Seg_3078" n="HIAT:w" s="T761">Sĭ-</ts>
                  <nts id="Seg_3079" n="HIAT:ip">)</nts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3082" n="HIAT:w" s="T762">Sĭre</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3085" n="HIAT:w" s="T763">bar</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3088" n="HIAT:w" s="T764">saʔmələj</ts>
                  <nts id="Seg_3089" n="HIAT:ip">.</nts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3092" n="HIAT:u" s="T765">
                  <ts e="T766" id="Seg_3094" n="HIAT:w" s="T765">Ugaːndə</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3097" n="HIAT:w" s="T766">šĭšəge</ts>
                  <nts id="Seg_3098" n="HIAT:ip">.</nts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_3101" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3103" n="HIAT:w" s="T767">Kănnaləl</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3106" n="HIAT:w" s="T768">bar</ts>
                  <nts id="Seg_3107" n="HIAT:ip">.</nts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3110" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_3112" n="HIAT:w" s="T769">Büžü</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3114" n="HIAT:ip">(</nts>
                  <ts e="T771" id="Seg_3116" n="HIAT:w" s="T770">bəj-</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3119" n="HIAT:w" s="T771">ar-</ts>
                  <nts id="Seg_3120" n="HIAT:ip">)</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3123" n="HIAT:w" s="T772">bar</ts>
                  <nts id="Seg_3124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3126" n="HIAT:w" s="T773">ejü</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3129" n="HIAT:w" s="T774">moləj</ts>
                  <nts id="Seg_3130" n="HIAT:ip">.</nts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3133" n="HIAT:u" s="T775">
                  <ts e="T776" id="Seg_3135" n="HIAT:w" s="T775">Dĭgəttə</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3138" n="HIAT:w" s="T776">il</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3140" n="HIAT:ip">(</nts>
                  <ts e="T778" id="Seg_3142" n="HIAT:w" s="T777">tarirluʔpiʔi</ts>
                  <nts id="Seg_3143" n="HIAT:ip">)</nts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3146" n="HIAT:w" s="T778">bar</ts>
                  <nts id="Seg_3147" n="HIAT:ip">.</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3150" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3152" n="HIAT:w" s="T779">Budəj</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3155" n="HIAT:w" s="T780">kuʔluʔjəʔ</ts>
                  <nts id="Seg_3156" n="HIAT:ip">.</nts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T786" id="Seg_3159" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3161" n="HIAT:w" s="T781">Ugaːndə</ts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3164" n="HIAT:w" s="T782">kuŋgem</ts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3167" n="HIAT:w" s="T783">nʼergölaʔbə</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3169" n="HIAT:ip">(</nts>
                  <ts e="T785" id="Seg_3171" n="HIAT:w" s="T784">nʼiʔn-</ts>
                  <nts id="Seg_3172" n="HIAT:ip">)</nts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3175" n="HIAT:w" s="T785">nʼuʔnə</ts>
                  <nts id="Seg_3176" n="HIAT:ip">.</nts>
                  <nts id="Seg_3177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3179" n="HIAT:u" s="T786">
                  <ts e="T787" id="Seg_3181" n="HIAT:w" s="T786">Ej</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3184" n="HIAT:w" s="T787">idlia</ts>
                  <nts id="Seg_3185" n="HIAT:ip">.</nts>
                  <nts id="Seg_3186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3188" n="HIAT:u" s="T788">
                  <ts e="T790" id="Seg_3190" n="HIAT:w" s="T788">I</ts>
                  <nts id="Seg_3191" n="HIAT:ip">…</nts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_3194" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3196" n="HIAT:w" s="T790">Inegəʔ</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3198" n="HIAT:ip">(</nts>
                  <ts e="T792" id="Seg_3200" n="HIAT:w" s="T791">inezʼ-</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3203" n="HIAT:w" s="T792">inezeŋ-</ts>
                  <nts id="Seg_3204" n="HIAT:ip">)</nts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3207" n="HIAT:w" s="T793">inegəʔ</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3210" n="HIAT:w" s="T794">bar</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3213" n="HIAT:w" s="T795">dʼünə</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3216" n="HIAT:w" s="T796">saʔməluʔpi</ts>
                  <nts id="Seg_3217" n="HIAT:ip">.</nts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3220" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_3222" n="HIAT:w" s="T797">Nʼuʔdə</ts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3225" n="HIAT:w" s="T798">bar</ts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3228" n="HIAT:w" s="T799">nada</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3231" n="HIAT:w" s="T800">kanzittə</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3234" n="HIAT:w" s="T801">ugaːndə</ts>
                  <nts id="Seg_3235" n="HIAT:ip">.</nts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_3238" n="HIAT:u" s="T802">
                  <ts e="T802.tx-PKZ.1" id="Seg_3240" n="HIAT:w" s="T802">Больше</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802.tx-PKZ.2" id="Seg_3243" n="HIAT:w" s="T802.tx-PKZ.1">не</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3246" n="HIAT:w" s="T802.tx-PKZ.2">знаю</ts>
                  <nts id="Seg_3247" n="HIAT:ip">.</nts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T806" id="Seg_3250" n="HIAT:u" s="T805">
                  <nts id="Seg_3251" n="HIAT:ip">(</nts>
                  <nts id="Seg_3252" n="HIAT:ip">(</nts>
                  <ats e="T806" id="Seg_3253" n="HIAT:non-pho" s="T805">DMG</ats>
                  <nts id="Seg_3254" n="HIAT:ip">)</nts>
                  <nts id="Seg_3255" n="HIAT:ip">)</nts>
                  <nts id="Seg_3256" n="HIAT:ip">.</nts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_3259" n="HIAT:u" s="T806">
                  <ts e="T807" id="Seg_3261" n="HIAT:w" s="T806">Ugaːndə</ts>
                  <nts id="Seg_3262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3263" n="HIAT:ip">(</nts>
                  <ts e="T808" id="Seg_3265" n="HIAT:w" s="T807">kudaj</ts>
                  <nts id="Seg_3266" n="HIAT:ip">)</nts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3269" n="HIAT:w" s="T808">nʼuʔdə</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3272" n="HIAT:w" s="T809">amnolaʔbə</ts>
                  <nts id="Seg_3273" n="HIAT:ip">.</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3276" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_3278" n="HIAT:w" s="T810">Măjagən</ts>
                  <nts id="Seg_3279" n="HIAT:ip">,</nts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3281" n="HIAT:ip">(</nts>
                  <ts e="T812" id="Seg_3283" n="HIAT:w" s="T811">uʔ-</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3286" n="HIAT:w" s="T812">uj-</ts>
                  <nts id="Seg_3287" n="HIAT:ip">)</nts>
                  <nts id="Seg_3288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3290" n="HIAT:w" s="T813">urgo</ts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3292" n="HIAT:ip">(</nts>
                  <ts e="T815" id="Seg_3294" n="HIAT:w" s="T814">uj-</ts>
                  <nts id="Seg_3295" n="HIAT:ip">)</nts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3298" n="HIAT:w" s="T815">măja</ts>
                  <nts id="Seg_3299" n="HIAT:ip">.</nts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3302" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3304" n="HIAT:w" s="T816">Ulum</ts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3307" n="HIAT:w" s="T817">bar</ts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3309" n="HIAT:ip">(</nts>
                  <ts e="T819" id="Seg_3311" n="HIAT:w" s="T818">tʼukumnie</ts>
                  <nts id="Seg_3312" n="HIAT:ip">)</nts>
                  <nts id="Seg_3313" n="HIAT:ip">.</nts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3316" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3318" n="HIAT:w" s="T819">Kădaʔliom</ts>
                  <nts id="Seg_3319" n="HIAT:ip">,</nts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3322" n="HIAT:w" s="T820">naverna</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3325" n="HIAT:w" s="T821">unu</ts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3328" n="HIAT:w" s="T822">iʔgö</ts>
                  <nts id="Seg_3329" n="HIAT:ip">.</nts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3332" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3334" n="HIAT:w" s="T823">Nada</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3337" n="HIAT:w" s="T824">dĭzeŋ</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3340" n="HIAT:w" s="T825">bar</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3343" n="HIAT:w" s="T826">kutzʼittə</ts>
                  <nts id="Seg_3344" n="HIAT:ip">.</nts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T337" id="Seg_3346" n="sc" s="T4">
               <ts e="T5" id="Seg_3348" n="e" s="T4">Sʼimat </ts>
               <ts e="T6" id="Seg_3350" n="e" s="T5">ej </ts>
               <ts e="T7" id="Seg_3352" n="e" s="T6">kuvas, </ts>
               <ts e="T8" id="Seg_3354" n="e" s="T7">onʼiʔ </ts>
               <ts e="T9" id="Seg_3356" n="e" s="T8">döbər </ts>
               <ts e="T10" id="Seg_3358" n="e" s="T9">mandolaʔbə, </ts>
               <ts e="T11" id="Seg_3360" n="e" s="T10">onʼiʔ </ts>
               <ts e="T12" id="Seg_3362" n="e" s="T11">dibər. </ts>
               <ts e="T13" id="Seg_3364" n="e" s="T12">Sʼimat </ts>
               <ts e="T14" id="Seg_3366" n="e" s="T13">((…)). </ts>
               <ts e="T15" id="Seg_3368" n="e" s="T14">Măn </ts>
               <ts e="T16" id="Seg_3370" n="e" s="T15">aŋbə </ts>
               <ts e="T17" id="Seg_3372" n="e" s="T16">bar </ts>
               <ts e="T18" id="Seg_3374" n="e" s="T17">kajlaʔbə, </ts>
               <ts e="T19" id="Seg_3376" n="e" s="T18">kunolzittə </ts>
               <ts e="T20" id="Seg_3378" n="e" s="T19">axota. </ts>
               <ts e="T21" id="Seg_3380" n="e" s="T20">Miʔ </ts>
               <ts e="T22" id="Seg_3382" n="e" s="T21">šidegöʔ </ts>
               <ts e="T23" id="Seg_3384" n="e" s="T22">(kambivaʔ-) </ts>
               <ts e="T24" id="Seg_3386" n="e" s="T23">kambibaʔ </ts>
               <ts e="T25" id="Seg_3388" n="e" s="T24">aktʼinə. </ts>
               <ts e="T26" id="Seg_3390" n="e" s="T25">Dĭ </ts>
               <ts e="T27" id="Seg_3392" n="e" s="T26">(ka-) </ts>
               <ts e="T28" id="Seg_3394" n="e" s="T27">kambi </ts>
               <ts e="T29" id="Seg_3396" n="e" s="T28">nalʼeva, </ts>
               <ts e="T30" id="Seg_3398" n="e" s="T29">măn </ts>
               <ts e="T31" id="Seg_3400" n="e" s="T30">kambiam </ts>
               <ts e="T32" id="Seg_3402" n="e" s="T31">naprava. </ts>
               <ts e="T33" id="Seg_3404" n="e" s="T32">Kăda </ts>
               <ts e="T34" id="Seg_3406" n="e" s="T33">dĭm </ts>
               <ts e="T35" id="Seg_3408" n="e" s="T34">kăštəliaʔi, </ts>
               <ts e="T36" id="Seg_3410" n="e" s="T35">măn </ts>
               <ts e="T37" id="Seg_3412" n="e" s="T36">ej </ts>
               <ts e="T38" id="Seg_3414" n="e" s="T37">tĭmnem. </ts>
               <ts e="T39" id="Seg_3416" n="e" s="T38">Čolaʔi </ts>
               <ts e="T40" id="Seg_3418" n="e" s="T39">bar </ts>
               <ts e="T41" id="Seg_3420" n="e" s="T40">nʼamga </ts>
               <ts e="T42" id="Seg_3422" n="e" s="T41">tʼerə </ts>
               <ts e="T43" id="Seg_3424" n="e" s="T42">detleʔi. </ts>
               <ts e="T44" id="Seg_3426" n="e" s="T43">I </ts>
               <ts e="T45" id="Seg_3428" n="e" s="T44">takšənə </ts>
               <ts e="T46" id="Seg_3430" n="e" s="T45">(en-) </ts>
               <ts e="T47" id="Seg_3432" n="e" s="T46">endləʔbəʔjə. </ts>
               <ts e="T48" id="Seg_3434" n="e" s="T47">Ugaːndə </ts>
               <ts e="T49" id="Seg_3436" n="e" s="T48">tăŋ </ts>
               <ts e="T50" id="Seg_3438" n="e" s="T49">togonorlaʔbəʔjə. </ts>
               <ts e="T51" id="Seg_3440" n="e" s="T50">Dʼijegən </ts>
               <ts e="T52" id="Seg_3442" n="e" s="T51">((…)) </ts>
               <ts e="T53" id="Seg_3444" n="e" s="T52">Dʼijegən </ts>
               <ts e="T54" id="Seg_3446" n="e" s="T53">ej </ts>
               <ts e="T55" id="Seg_3448" n="e" s="T54">amnolaʔbəʔjə, </ts>
               <ts e="T56" id="Seg_3450" n="e" s="T55">bügən </ts>
               <ts e="T57" id="Seg_3452" n="e" s="T56">ej </ts>
               <ts e="T58" id="Seg_3454" n="e" s="T57">amnolaʔbə. </ts>
               <ts e="T59" id="Seg_3456" n="e" s="T58">Ugaːndə </ts>
               <ts e="T60" id="Seg_3458" n="e" s="T59">urgo </ts>
               <ts e="T61" id="Seg_3460" n="e" s="T60">măja. </ts>
               <ts e="T62" id="Seg_3462" n="e" s="T61">Kăda </ts>
               <ts e="T63" id="Seg_3464" n="e" s="T62">dĭʔnə </ts>
               <ts e="T64" id="Seg_3466" n="e" s="T63">kanzittə? </ts>
               <ts e="T65" id="Seg_3468" n="e" s="T64">Ej </ts>
               <ts e="T66" id="Seg_3470" n="e" s="T65">tĭmnem. </ts>
               <ts e="T67" id="Seg_3472" n="e" s="T66">Măndlial </ts>
               <ts e="T68" id="Seg_3474" n="e" s="T67">dăk, </ts>
               <ts e="T69" id="Seg_3476" n="e" s="T68">bar </ts>
               <ts e="T70" id="Seg_3478" n="e" s="T69">(užu- </ts>
               <ts e="T71" id="Seg_3480" n="e" s="T70">uzu- </ts>
               <ts e="T72" id="Seg_3482" n="e" s="T71">saʔmə=) </ts>
               <ts e="T73" id="Seg_3484" n="e" s="T72">üžü </ts>
               <ts e="T74" id="Seg_3486" n="e" s="T73">saʔməluʔləj. </ts>
               <ts e="T75" id="Seg_3488" n="e" s="T74">Kambiʔi </ts>
               <ts e="T76" id="Seg_3490" n="e" s="T75">dʼü </ts>
               <ts e="T77" id="Seg_3492" n="e" s="T76">tĭlzittə. </ts>
               <ts e="T78" id="Seg_3494" n="e" s="T77">(A-) </ts>
               <ts e="T80" id="Seg_3496" n="e" s="T78">Aʔtʼi… </ts>
               <ts e="T81" id="Seg_3498" n="e" s="T80">(A-) </ts>
               <ts e="T82" id="Seg_3500" n="e" s="T81">Kambiʔi </ts>
               <ts e="T83" id="Seg_3502" n="e" s="T82">dʼü </ts>
               <ts e="T84" id="Seg_3504" n="e" s="T83">tĭlzittə. </ts>
               <ts e="T85" id="Seg_3506" n="e" s="T84">Aʔtʼi </ts>
               <ts e="T86" id="Seg_3508" n="e" s="T85">tʼazirzittə. </ts>
               <ts e="T87" id="Seg_3510" n="e" s="T86">Gibər </ts>
               <ts e="T88" id="Seg_3512" n="e" s="T87">(an-) </ts>
               <ts e="T89" id="Seg_3514" n="e" s="T88">dĭzeŋ </ts>
               <ts e="T90" id="Seg_3516" n="e" s="T89">kambiʔi, </ts>
               <ts e="T91" id="Seg_3518" n="e" s="T90">kuŋgəŋ? </ts>
               <ts e="T92" id="Seg_3520" n="e" s="T91">Dʼok, </ts>
               <ts e="T93" id="Seg_3522" n="e" s="T92">ej </ts>
               <ts e="T94" id="Seg_3524" n="e" s="T93">kuŋgə. </ts>
               <ts e="T95" id="Seg_3526" n="e" s="T94">Šobiʔi </ts>
               <ts e="T96" id="Seg_3528" n="e" s="T95">inezʼiʔi. </ts>
               <ts e="T97" id="Seg_3530" n="e" s="T96">Ej </ts>
               <ts e="T98" id="Seg_3532" n="e" s="T97">kuŋgəŋ </ts>
               <ts e="T99" id="Seg_3534" n="e" s="T98">turagən </ts>
               <ts e="T100" id="Seg_3536" n="e" s="T99">nubiʔi. </ts>
               <ts e="T101" id="Seg_3538" n="e" s="T100">Daže </ts>
               <ts e="T102" id="Seg_3540" n="e" s="T101">ej </ts>
               <ts e="T103" id="Seg_3542" n="e" s="T102">surarbiʔi, </ts>
               <ts e="T104" id="Seg_3544" n="e" s="T103">bar </ts>
               <ts e="T105" id="Seg_3546" n="e" s="T104">nubiʔi. </ts>
               <ts e="T106" id="Seg_3548" n="e" s="T105">Kanaʔ </ts>
               <ts e="T107" id="Seg_3550" n="e" s="T106">(măn </ts>
               <ts e="T108" id="Seg_3552" n="e" s="T107">turagəʔ </ts>
               <ts e="T109" id="Seg_3554" n="e" s="T108">i </ts>
               <ts e="T110" id="Seg_3556" n="e" s="T109">dön) </ts>
               <ts e="T111" id="Seg_3558" n="e" s="T110">iʔ </ts>
               <ts e="T112" id="Seg_3560" n="e" s="T111">nuʔ. </ts>
               <ts e="T113" id="Seg_3562" n="e" s="T112">Ej </ts>
               <ts e="T114" id="Seg_3564" n="e" s="T113">kambiʔi. </ts>
               <ts e="T115" id="Seg_3566" n="e" s="T114">Turanə </ts>
               <ts e="T116" id="Seg_3568" n="e" s="T115">saʔluʔpiʔi. </ts>
               <ts e="T117" id="Seg_3570" n="e" s="T116">I </ts>
               <ts e="T118" id="Seg_3572" n="e" s="T117">alomniaʔi </ts>
               <ts e="T119" id="Seg_3574" n="e" s="T118">bar. </ts>
               <ts e="T120" id="Seg_3576" n="e" s="T119">Turanə </ts>
               <ts e="T121" id="Seg_3578" n="e" s="T120">nʼuʔdə </ts>
               <ts e="T122" id="Seg_3580" n="e" s="T121">bar </ts>
               <ts e="T123" id="Seg_3582" n="e" s="T122">suʔmiluʔpiʔi. </ts>
               <ts e="T124" id="Seg_3584" n="e" s="T123">(Sagən </ts>
               <ts e="T125" id="Seg_3586" n="e" s="T124">nüjüʔi) </ts>
               <ts e="T126" id="Seg_3588" n="e" s="T125">naga. </ts>
               <ts e="T127" id="Seg_3590" n="e" s="T126">Gibərdə </ts>
               <ts e="T128" id="Seg_3592" n="e" s="T127">kalla </ts>
               <ts e="T129" id="Seg_3594" n="e" s="T128">dʼürbiʔi </ts>
               <ts e="T130" id="Seg_3596" n="e" s="T129">alʼi </ts>
               <ts e="T131" id="Seg_3598" n="e" s="T130">kubiʔi </ts>
               <ts e="T132" id="Seg_3600" n="e" s="T131">bar. </ts>
               <ts e="T133" id="Seg_3602" n="e" s="T132">Măna </ts>
               <ts e="T134" id="Seg_3604" n="e" s="T133">tože </ts>
               <ts e="T135" id="Seg_3606" n="e" s="T134">pizʼiʔ </ts>
               <ts e="T136" id="Seg_3608" n="e" s="T135">münörleʔbəlaʔ. </ts>
               <ts e="T137" id="Seg_3610" n="e" s="T136">Măna </ts>
               <ts e="T138" id="Seg_3612" n="e" s="T137">tože </ts>
               <ts e="T139" id="Seg_3614" n="e" s="T138">pi </ts>
               <ts e="T140" id="Seg_3616" n="e" s="T139">(ellə- </ts>
               <ts e="T141" id="Seg_3618" n="e" s="T140">el=) </ts>
               <ts e="T142" id="Seg_3620" n="e" s="T141">elləl. </ts>
               <ts e="T143" id="Seg_3622" n="e" s="T142">Il </ts>
               <ts e="T144" id="Seg_3624" n="e" s="T143">jakšə, </ts>
               <ts e="T145" id="Seg_3626" n="e" s="T144">ej </ts>
               <ts e="T146" id="Seg_3628" n="e" s="T145">šamniabaʔ. </ts>
               <ts e="T147" id="Seg_3630" n="e" s="T146">Jakšə </ts>
               <ts e="T148" id="Seg_3632" n="e" s="T147">(dʼăbaktərlabəʔ-) </ts>
               <ts e="T149" id="Seg_3634" n="e" s="T148">dʼăbaktərlaʔbəbaʔ. </ts>
               <ts e="T150" id="Seg_3636" n="e" s="T149">Šənap </ts>
               <ts e="T151" id="Seg_3638" n="e" s="T150">dʼăbaktərlaʔbəm, </ts>
               <ts e="T152" id="Seg_3640" n="e" s="T151">ej </ts>
               <ts e="T153" id="Seg_3642" n="e" s="T152">šamniam. </ts>
               <ts e="T154" id="Seg_3644" n="e" s="T153">Komu </ts>
               <ts e="T156" id="Seg_3646" n="e" s="T154">červəʔi </ts>
               <ts e="T157" id="Seg_3648" n="e" s="T156">(oʔbdəbiam, </ts>
               <ts e="T158" id="Seg_3650" n="e" s="T157">büzʼiʔ) </ts>
               <ts e="T159" id="Seg_3652" n="e" s="T158">(dĭm) </ts>
               <ts e="T160" id="Seg_3654" n="e" s="T159">(mazə-) </ts>
               <ts e="T161" id="Seg_3656" n="e" s="T160">bəzəbiam. </ts>
               <ts e="T162" id="Seg_3658" n="e" s="T161">Takšənə </ts>
               <ts e="T163" id="Seg_3660" n="e" s="T162">embiem. </ts>
               <ts e="T164" id="Seg_3662" n="e" s="T163">Dĭbər </ts>
               <ts e="T165" id="Seg_3664" n="e" s="T164">(ejü=) </ts>
               <ts e="T166" id="Seg_3666" n="e" s="T165">ejü </ts>
               <ts e="T167" id="Seg_3668" n="e" s="T166">nuldəbiam. </ts>
               <ts e="T168" id="Seg_3670" n="e" s="T167">Dibər </ts>
               <ts e="T169" id="Seg_3672" n="e" s="T168">bü </ts>
               <ts e="T170" id="Seg_3674" n="e" s="T169">molaːmbi. </ts>
               <ts e="T171" id="Seg_3676" n="e" s="T170">Dĭgəttə </ts>
               <ts e="T172" id="Seg_3678" n="e" s="T171">udam </ts>
               <ts e="T173" id="Seg_3680" n="e" s="T172">kĭškəbiem. </ts>
               <ts e="T174" id="Seg_3682" n="e" s="T173">Udam </ts>
               <ts e="T175" id="Seg_3684" n="e" s="T174">ugaːndə </ts>
               <ts e="T176" id="Seg_3686" n="e" s="T175">tăŋ </ts>
               <ts e="T177" id="Seg_3688" n="e" s="T176">ĭzembiʔi, </ts>
               <ts e="T178" id="Seg_3690" n="e" s="T177">tüj </ts>
               <ts e="T179" id="Seg_3692" n="e" s="T178">ej </ts>
               <ts e="T180" id="Seg_3694" n="e" s="T179">ĭzemnieʔi. </ts>
               <ts e="T181" id="Seg_3696" n="e" s="T180">Smatri, </ts>
               <ts e="T183" id="Seg_3698" n="e" s="T181">kudaj… </ts>
               <ts e="T184" id="Seg_3700" n="e" s="T183">Smatri, </ts>
               <ts e="T185" id="Seg_3702" n="e" s="T184">kudaj </ts>
               <ts e="T186" id="Seg_3704" n="e" s="T185">iʔ </ts>
               <ts e="T187" id="Seg_3706" n="e" s="T186">nöməlaʔ. </ts>
               <ts e="T191" id="Seg_3708" n="e" s="T187">Погоди, как (это)… </ts>
               <ts e="T192" id="Seg_3710" n="e" s="T191">Kanaʔ </ts>
               <ts e="T193" id="Seg_3712" n="e" s="T192">eneidənenə! </ts>
               <ts e="T194" id="Seg_3714" n="e" s="T193">Eneidənen </ts>
               <ts e="T195" id="Seg_3716" n="e" s="T194">(amnu-) </ts>
               <ts e="T196" id="Seg_3718" n="e" s="T195">amnut! </ts>
               <ts e="T197" id="Seg_3720" n="e" s="T196">Tüžöjən </ts>
               <ts e="T198" id="Seg_3722" n="e" s="T197">amnut. </ts>
               <ts e="T199" id="Seg_3724" n="e" s="T198">Dĭ </ts>
               <ts e="T200" id="Seg_3726" n="e" s="T199">bar </ts>
               <ts e="T201" id="Seg_3728" n="e" s="T200">tănan </ts>
               <ts e="T202" id="Seg_3730" n="e" s="T201">(muʔ-) </ts>
               <ts e="T203" id="Seg_3732" n="e" s="T202">muʔləj. </ts>
               <ts e="T204" id="Seg_3734" n="e" s="T203">Sʼenʼiʔi </ts>
               <ts e="T205" id="Seg_3736" n="e" s="T204">(š-) </ts>
               <ts e="T206" id="Seg_3738" n="e" s="T205">săbərəjdə, </ts>
               <ts e="T207" id="Seg_3740" n="e" s="T206">a_to </ts>
               <ts e="T208" id="Seg_3742" n="e" s="T207">ugaːndə </ts>
               <ts e="T209" id="Seg_3744" n="e" s="T208">iʔgö </ts>
               <ts e="T210" id="Seg_3746" n="e" s="T209">balgaš. </ts>
               <ts e="T211" id="Seg_3748" n="e" s="T210">Krilso </ts>
               <ts e="T212" id="Seg_3750" n="e" s="T211">săbərəjdə! </ts>
               <ts e="T213" id="Seg_3752" n="e" s="T212">Anbardə </ts>
               <ts e="T214" id="Seg_3754" n="e" s="T213">(un </ts>
               <ts e="T215" id="Seg_3756" n="e" s="T214">ilem), </ts>
               <ts e="T216" id="Seg_3758" n="e" s="T215">embiem. </ts>
               <ts e="T217" id="Seg_3760" n="e" s="T216">Măn </ts>
               <ts e="T218" id="Seg_3762" n="e" s="T217">dĭm </ts>
               <ts e="T219" id="Seg_3764" n="e" s="T218">kuʔpiom, </ts>
               <ts e="T220" id="Seg_3766" n="e" s="T219">măn </ts>
               <ts e="T221" id="Seg_3768" n="e" s="T220">šaldə </ts>
               <ts e="T222" id="Seg_3770" n="e" s="T221">iʔgö. </ts>
               <ts e="T223" id="Seg_3772" n="e" s="T222">Măn </ts>
               <ts e="T224" id="Seg_3774" n="e" s="T223">ugaːndə </ts>
               <ts e="T225" id="Seg_3776" n="e" s="T224">kuštu </ts>
               <ts e="T226" id="Seg_3778" n="e" s="T225">igem. </ts>
               <ts e="T227" id="Seg_3780" n="e" s="T226">Dĭ </ts>
               <ts e="T228" id="Seg_3782" n="e" s="T227">kuza </ts>
               <ts e="T229" id="Seg_3784" n="e" s="T228">bar </ts>
               <ts e="T230" id="Seg_3786" n="e" s="T229">sagəšdə </ts>
               <ts e="T231" id="Seg_3788" n="e" s="T230">naga. </ts>
               <ts e="T232" id="Seg_3790" n="e" s="T231">Šĭket </ts>
               <ts e="T233" id="Seg_3792" n="e" s="T232">ige, </ts>
               <ts e="T234" id="Seg_3794" n="e" s="T233">dʼăbaktərzittə </ts>
               <ts e="T235" id="Seg_3796" n="e" s="T234">ej </ts>
               <ts e="T236" id="Seg_3798" n="e" s="T235">molia. </ts>
               <ts e="T237" id="Seg_3800" n="e" s="T236">Kut </ts>
               <ts e="T238" id="Seg_3802" n="e" s="T237">ige, </ts>
               <ts e="T239" id="Seg_3804" n="e" s="T238">ej </ts>
               <ts e="T240" id="Seg_3806" n="e" s="T239">nünnie. </ts>
               <ts e="T241" id="Seg_3808" n="e" s="T240">Iʔ </ts>
               <ts e="T242" id="Seg_3810" n="e" s="T241">parloʔ! </ts>
               <ts e="T243" id="Seg_3812" n="e" s="T242">Jakšə </ts>
               <ts e="T244" id="Seg_3814" n="e" s="T243">amnoʔ! </ts>
               <ts e="T245" id="Seg_3816" n="e" s="T244">Ĭmbi </ts>
               <ts e="T246" id="Seg_3818" n="e" s="T245">parloraʔbəl? </ts>
               <ts e="T247" id="Seg_3820" n="e" s="T246">Bostə </ts>
               <ts e="T248" id="Seg_3822" n="e" s="T247">šĭkem </ts>
               <ts e="T249" id="Seg_3824" n="e" s="T248">nünniöm, </ts>
               <ts e="T250" id="Seg_3826" n="e" s="T249">jakšə </ts>
               <ts e="T251" id="Seg_3828" n="e" s="T250">dʼăbaktərliam. </ts>
               <ts e="T252" id="Seg_3830" n="e" s="T251">Šoška </ts>
               <ts e="T253" id="Seg_3832" n="e" s="T252">šonəga </ts>
               <ts e="T254" id="Seg_3834" n="e" s="T253">i </ts>
               <ts e="T255" id="Seg_3836" n="e" s="T254">kuremnaʔbə </ts>
               <ts e="T256" id="Seg_3838" n="e" s="T255">bar. </ts>
               <ts e="T257" id="Seg_3840" n="e" s="T256">Nada </ts>
               <ts e="T258" id="Seg_3842" n="e" s="T257">dĭm </ts>
               <ts e="T259" id="Seg_3844" n="e" s="T258">bădəsʼtə, </ts>
               <ts e="T260" id="Seg_3846" n="e" s="T259">a_to </ts>
               <ts e="T261" id="Seg_3848" n="e" s="T260">dĭ </ts>
               <ts e="T262" id="Seg_3850" n="e" s="T261">püjölia </ts>
               <ts e="T263" id="Seg_3852" n="e" s="T262">bar. </ts>
               <ts e="T264" id="Seg_3854" n="e" s="T263">Nada </ts>
               <ts e="T265" id="Seg_3856" n="e" s="T264">(poʔ-) </ts>
               <ts e="T266" id="Seg_3858" n="e" s="T265">baltu </ts>
               <ts e="T267" id="Seg_3860" n="e" s="T266">puʔməsʼtə, </ts>
               <ts e="T268" id="Seg_3862" n="e" s="T267">paʔi </ts>
               <ts e="T269" id="Seg_3864" n="e" s="T268">((PAUSE)) </ts>
               <ts e="T270" id="Seg_3866" n="e" s="T269">(jaʔi- </ts>
               <ts e="T271" id="Seg_3868" n="e" s="T270">jai- </ts>
               <ts e="T272" id="Seg_3870" n="e" s="T271">jaʔi-) </ts>
               <ts e="T273" id="Seg_3872" n="e" s="T272">jaʔsʼittə. </ts>
               <ts e="T274" id="Seg_3874" n="e" s="T273">Pʼeš </ts>
               <ts e="T275" id="Seg_3876" n="e" s="T274">nenzittə. </ts>
               <ts e="T276" id="Seg_3878" n="e" s="T275">Pʼila </ts>
               <ts e="T277" id="Seg_3880" n="e" s="T276">deppiʔi, </ts>
               <ts e="T278" id="Seg_3882" n="e" s="T277">ej </ts>
               <ts e="T279" id="Seg_3884" n="e" s="T278">tĭmneʔi, </ts>
               <ts e="T280" id="Seg_3886" n="e" s="T279">ĭmbi </ts>
               <ts e="T281" id="Seg_3888" n="e" s="T280">dĭzʼiʔ </ts>
               <ts e="T282" id="Seg_3890" n="e" s="T281">azittə. </ts>
               <ts e="T283" id="Seg_3892" n="e" s="T282">(Arla-) </ts>
               <ts e="T284" id="Seg_3894" n="e" s="T283">Varlam </ts>
               <ts e="T285" id="Seg_3896" n="e" s="T284">šoləj, </ts>
               <ts e="T286" id="Seg_3898" n="e" s="T285">tʼazirləj. </ts>
               <ts e="T287" id="Seg_3900" n="e" s="T286">Bazaj </ts>
               <ts e="T288" id="Seg_3902" n="e" s="T287">pʼeš </ts>
               <ts e="T289" id="Seg_3904" n="e" s="T288">deppiʔi. </ts>
               <ts e="T290" id="Seg_3906" n="e" s="T289">Turanə </ts>
               <ts e="T291" id="Seg_3908" n="e" s="T290">nuldəbiʔi. </ts>
               <ts e="T292" id="Seg_3910" n="e" s="T291">(Šoj- </ts>
               <ts e="T293" id="Seg_3912" n="e" s="T292">šoj-) </ts>
               <ts e="T294" id="Seg_3914" n="e" s="T293">Šojdʼonə. </ts>
               <ts e="T295" id="Seg_3916" n="e" s="T294">(Pasə) </ts>
               <ts e="T296" id="Seg_3918" n="e" s="T295">šojdʼo, </ts>
               <ts e="T297" id="Seg_3920" n="e" s="T296">dĭ </ts>
               <ts e="T298" id="Seg_3922" n="e" s="T297">bar </ts>
               <ts e="T299" id="Seg_3924" n="e" s="T298">nendluʔpi. </ts>
               <ts e="T300" id="Seg_3926" n="e" s="T299">Bər </ts>
               <ts e="T301" id="Seg_3928" n="e" s="T300">šobi. </ts>
               <ts e="T302" id="Seg_3930" n="e" s="T301">Dĭgəttə </ts>
               <ts e="T303" id="Seg_3932" n="e" s="T302">bar </ts>
               <ts e="T304" id="Seg_3934" n="e" s="T303">əʔpiʔi </ts>
               <ts e="T305" id="Seg_3936" n="e" s="T304">nʼiʔdə. </ts>
               <ts e="T306" id="Seg_3938" n="e" s="T305">Ugaːndə </ts>
               <ts e="T307" id="Seg_3940" n="e" s="T306">bər </ts>
               <ts e="T308" id="Seg_3942" n="e" s="T307">iʔgö. </ts>
               <ts e="T309" id="Seg_3944" n="e" s="T308">Varlam </ts>
               <ts e="T310" id="Seg_3946" n="e" s="T309">šoləj </ts>
               <ts e="T311" id="Seg_3948" n="e" s="T310">i </ts>
               <ts e="T312" id="Seg_3950" n="e" s="T311">dʼazirləj. </ts>
               <ts e="T313" id="Seg_3952" n="e" s="T312">Bar </ts>
               <ts e="T314" id="Seg_3954" n="e" s="T313">bər </ts>
               <ts e="T315" id="Seg_3956" n="e" s="T314">sʼimam </ts>
               <ts e="T316" id="Seg_3958" n="e" s="T315">amnalaʔbəʔjə. </ts>
               <ts e="T317" id="Seg_3960" n="e" s="T316">Dĭgəttə </ts>
               <ts e="T318" id="Seg_3962" n="e" s="T317">Varlam </ts>
               <ts e="T319" id="Seg_3964" n="e" s="T318">šobi, </ts>
               <ts e="T320" id="Seg_3966" n="e" s="T319">pi </ts>
               <ts e="T321" id="Seg_3968" n="e" s="T320">deʔpi. </ts>
               <ts e="T322" id="Seg_3970" n="e" s="T321">Dʼünə </ts>
               <ts e="T323" id="Seg_3972" n="e" s="T322">embi. </ts>
               <ts e="T324" id="Seg_3974" n="e" s="T323">I </ts>
               <ts e="T325" id="Seg_3976" n="e" s="T324">pʼešdə </ts>
               <ts e="T326" id="Seg_3978" n="e" s="T325">nuldəbi. </ts>
               <ts e="T327" id="Seg_3980" n="e" s="T326">Dĭgəttə </ts>
               <ts e="T328" id="Seg_3982" n="e" s="T327">pʼeš </ts>
               <ts e="T329" id="Seg_3984" n="e" s="T328">nendəbiʔi, </ts>
               <ts e="T330" id="Seg_3986" n="e" s="T329">amnolaʔbəʔjə </ts>
               <ts e="T331" id="Seg_3988" n="e" s="T330">i </ts>
               <ts e="T332" id="Seg_3990" n="e" s="T331">(ejü-) </ts>
               <ts e="T333" id="Seg_3992" n="e" s="T332">ejüleʔbəʔjə. </ts>
               <ts e="T335" id="Seg_3994" n="e" s="T333">чтобы </ts>
               <ts e="T336" id="Seg_3996" n="e" s="T335">не </ts>
               <ts e="T337" id="Seg_3998" n="e" s="T336">ошибиться. </ts>
            </ts>
            <ts e="T444" id="Seg_3999" n="sc" s="T338">
               <ts e="T339" id="Seg_4001" n="e" s="T338">Părgam </ts>
               <ts e="T340" id="Seg_4003" n="e" s="T339">bar </ts>
               <ts e="T341" id="Seg_4005" n="e" s="T340">(dʼügən=) </ts>
               <ts e="T342" id="Seg_4007" n="e" s="T341">dʼügən </ts>
               <ts e="T343" id="Seg_4009" n="e" s="T342">iʔböbi. </ts>
               <ts e="T344" id="Seg_4011" n="e" s="T343">Üjüʔi </ts>
               <ts e="T345" id="Seg_4013" n="e" s="T344">bar </ts>
               <ts e="T346" id="Seg_4015" n="e" s="T345">kĭškəbiʔi. </ts>
               <ts e="T347" id="Seg_4017" n="e" s="T346">(Dĭn=) </ts>
               <ts e="T348" id="Seg_4019" n="e" s="T347">Dĭn </ts>
               <ts e="T349" id="Seg_4021" n="e" s="T348">bar </ts>
               <ts e="T350" id="Seg_4023" n="e" s="T349">balgaš, </ts>
               <ts e="T351" id="Seg_4025" n="e" s="T350">măn </ts>
               <ts e="T352" id="Seg_4027" n="e" s="T351">dĭm </ts>
               <ts e="T353" id="Seg_4029" n="e" s="T352">(ibiel=) </ts>
               <ts e="T354" id="Seg_4031" n="e" s="T353">ibiem </ts>
               <ts e="T355" id="Seg_4033" n="e" s="T354">da, </ts>
               <ts e="T356" id="Seg_4035" n="e" s="T355">bünə </ts>
               <ts e="T357" id="Seg_4037" n="e" s="T356">(kumbial=) </ts>
               <ts e="T358" id="Seg_4039" n="e" s="T357">kumbiam, </ts>
               <ts e="T359" id="Seg_4041" n="e" s="T358">štobɨ </ts>
               <ts e="T360" id="Seg_4043" n="e" s="T359">dʼüʔpi </ts>
               <ts e="T361" id="Seg_4045" n="e" s="T360">mobi. </ts>
               <ts e="T362" id="Seg_4047" n="e" s="T361">Dĭgəttə </ts>
               <ts e="T363" id="Seg_4049" n="e" s="T362">toʔnarbiam. </ts>
               <ts e="T364" id="Seg_4051" n="e" s="T363">Bəzəbiam. </ts>
               <ts e="T365" id="Seg_4053" n="e" s="T364">Dĭgəttə </ts>
               <ts e="T366" id="Seg_4055" n="e" s="T365">edəbiem. </ts>
               <ts e="T367" id="Seg_4057" n="e" s="T366">Bü </ts>
               <ts e="T368" id="Seg_4059" n="e" s="T367">bar </ts>
               <ts e="T369" id="Seg_4061" n="e" s="T368">mʼaŋŋaʔbə </ts>
               <ts e="T370" id="Seg_4063" n="e" s="T369">dĭgəʔ. </ts>
               <ts e="T371" id="Seg_4065" n="e" s="T370">Dʼaga </ts>
               <ts e="T372" id="Seg_4067" n="e" s="T371">mʼaŋŋaʔbə. </ts>
               <ts e="T373" id="Seg_4069" n="e" s="T372">I </ts>
               <ts e="T374" id="Seg_4071" n="e" s="T373">kuza </ts>
               <ts e="T375" id="Seg_4073" n="e" s="T374">nuʔməleʔbə </ts>
               <ts e="T376" id="Seg_4075" n="e" s="T375">büjleʔ. </ts>
               <ts e="T377" id="Seg_4077" n="e" s="T376">Girgitdə </ts>
               <ts e="T378" id="Seg_4079" n="e" s="T377">il </ts>
               <ts e="T379" id="Seg_4081" n="e" s="T378">mĭlleʔbəʔjə. </ts>
               <ts e="T380" id="Seg_4083" n="e" s="T379">Măn </ts>
               <ts e="T381" id="Seg_4085" n="e" s="T380">(dĭm=) </ts>
               <ts e="T382" id="Seg_4087" n="e" s="T381">dĭzem </ts>
               <ts e="T383" id="Seg_4089" n="e" s="T382">ej </ts>
               <ts e="T384" id="Seg_4091" n="e" s="T383">tĭmnem. </ts>
               <ts e="T385" id="Seg_4093" n="e" s="T384">Ĭmbi </ts>
               <ts e="T386" id="Seg_4095" n="e" s="T385">(dĭ-) </ts>
               <ts e="T387" id="Seg_4097" n="e" s="T386">dĭzeŋ </ts>
               <ts e="T388" id="Seg_4099" n="e" s="T387">dĭn </ts>
               <ts e="T389" id="Seg_4101" n="e" s="T388">mĭlleʔbəʔjə. </ts>
               <ts e="T390" id="Seg_4103" n="e" s="T389">Da </ts>
               <ts e="T391" id="Seg_4105" n="e" s="T390">dĭ </ts>
               <ts e="T392" id="Seg_4107" n="e" s="T391">nʼi </ts>
               <ts e="T393" id="Seg_4109" n="e" s="T392">da </ts>
               <ts e="T394" id="Seg_4111" n="e" s="T393">koʔbdo </ts>
               <ts e="T395" id="Seg_4113" n="e" s="T394">šonəgaʔi. </ts>
               <ts e="T396" id="Seg_4115" n="e" s="T395">Nada </ts>
               <ts e="T397" id="Seg_4117" n="e" s="T396">sut </ts>
               <ts e="T398" id="Seg_4119" n="e" s="T397">kămnasʼtə </ts>
               <ts e="T399" id="Seg_4121" n="e" s="T398">šojdʼonə. </ts>
               <ts e="T400" id="Seg_4123" n="e" s="T399">Puskaj </ts>
               <ts e="T401" id="Seg_4125" n="e" s="T400">namzəga </ts>
               <ts e="T402" id="Seg_4127" n="e" s="T401">moləj. </ts>
               <ts e="T403" id="Seg_4129" n="e" s="T402">Tüjö </ts>
               <ts e="T404" id="Seg_4131" n="e" s="T403">kalla </ts>
               <ts e="T405" id="Seg_4133" n="e" s="T404">(dʼürbi-) </ts>
               <ts e="T406" id="Seg_4135" n="e" s="T405">dʼürbibeʔ. </ts>
               <ts e="T407" id="Seg_4137" n="e" s="T406">No, </ts>
               <ts e="T408" id="Seg_4139" n="e" s="T407">(kanaʔ=) </ts>
               <ts e="T409" id="Seg_4141" n="e" s="T408">kangaʔ </ts>
               <ts e="T410" id="Seg_4143" n="e" s="T409">kudajzʼiʔ. </ts>
               <ts e="T411" id="Seg_4145" n="e" s="T410">Miʔ </ts>
               <ts e="T412" id="Seg_4147" n="e" s="T411">ugaːndə </ts>
               <ts e="T413" id="Seg_4149" n="e" s="T412">iʔgö </ts>
               <ts e="T414" id="Seg_4151" n="e" s="T413">dʼăbaktərbibaʔ. </ts>
               <ts e="T415" id="Seg_4153" n="e" s="T414">Tüj </ts>
               <ts e="T416" id="Seg_4155" n="e" s="T415">kanzittə </ts>
               <ts e="T417" id="Seg_4157" n="e" s="T416">nada </ts>
               <ts e="T418" id="Seg_4159" n="e" s="T417">maːndə. </ts>
               <ts e="T419" id="Seg_4161" n="e" s="T418">Da </ts>
               <ts e="T420" id="Seg_4163" n="e" s="T419">kunolzittə </ts>
               <ts e="T421" id="Seg_4165" n="e" s="T420">iʔbəsʼtə. </ts>
               <ts e="T422" id="Seg_4167" n="e" s="T421">(Iʔ </ts>
               <ts e="T423" id="Seg_4169" n="e" s="T422">maktanarəbaʔ-) </ts>
               <ts e="T424" id="Seg_4171" n="e" s="T423">Iʔ </ts>
               <ts e="T425" id="Seg_4173" n="e" s="T424">maktanarəʔ. </ts>
               <ts e="T426" id="Seg_4175" n="e" s="T425">Ĭmbidə </ts>
               <ts e="T427" id="Seg_4177" n="e" s="T426">ej </ts>
               <ts e="T428" id="Seg_4179" n="e" s="T427">abial, </ts>
               <ts e="T429" id="Seg_4181" n="e" s="T428">a </ts>
               <ts e="T430" id="Seg_4183" n="e" s="T429">(bar) </ts>
               <ts e="T431" id="Seg_4185" n="e" s="T430">maktanarbial. </ts>
               <ts e="T432" id="Seg_4187" n="e" s="T431">Kunolzittə </ts>
               <ts e="T433" id="Seg_4189" n="e" s="T432">iʔbəsʼtə, </ts>
               <ts e="T434" id="Seg_4191" n="e" s="T433">a </ts>
               <ts e="T435" id="Seg_4193" n="e" s="T434">karəldʼan </ts>
               <ts e="T436" id="Seg_4195" n="e" s="T435">(šagəštə) </ts>
               <ts e="T437" id="Seg_4197" n="e" s="T436">šoləj. </ts>
               <ts e="T438" id="Seg_4199" n="e" s="T437">Kuznektə </ts>
               <ts e="T439" id="Seg_4201" n="e" s="T438">kuza </ts>
               <ts e="T440" id="Seg_4203" n="e" s="T439">šobi, </ts>
               <ts e="T441" id="Seg_4205" n="e" s="T440">a </ts>
               <ts e="T442" id="Seg_4207" n="e" s="T441">măn </ts>
               <ts e="T443" id="Seg_4209" n="e" s="T442">(pi) </ts>
               <ts e="T444" id="Seg_4211" n="e" s="T443">(kabarluʔpiam). </ts>
            </ts>
            <ts e="T750" id="Seg_4212" n="sc" s="T446">
               <ts e="T448" id="Seg_4214" n="e" s="T446">Запри ((DMG)). </ts>
               <ts e="T449" id="Seg_4216" n="e" s="T448">Pi </ts>
               <ts e="T450" id="Seg_4218" n="e" s="T449">kabarluʔpiom, </ts>
               <ts e="T451" id="Seg_4220" n="e" s="T450">i </ts>
               <ts e="T452" id="Seg_4222" n="e" s="T451">kuʔpiom </ts>
               <ts e="T453" id="Seg_4224" n="e" s="T452">dĭ </ts>
               <ts e="T454" id="Seg_4226" n="e" s="T453">kuzam. </ts>
               <ts e="T455" id="Seg_4228" n="e" s="T454">Kuza </ts>
               <ts e="T456" id="Seg_4230" n="e" s="T455">kuzam </ts>
               <ts e="T457" id="Seg_4232" n="e" s="T456">bar </ts>
               <ts e="T458" id="Seg_4234" n="e" s="T457">dagajzʼiʔ </ts>
               <ts e="T459" id="Seg_4236" n="e" s="T458">dʼagarluʔpi </ts>
               <ts e="T460" id="Seg_4238" n="e" s="T459">i </ts>
               <ts e="T461" id="Seg_4240" n="e" s="T460">dĭ </ts>
               <ts e="T462" id="Seg_4242" n="e" s="T461">külaːmbi. </ts>
               <ts e="T463" id="Seg_4244" n="e" s="T462">Dĭn </ts>
               <ts e="T464" id="Seg_4246" n="e" s="T463">onʼiʔ </ts>
               <ts e="T465" id="Seg_4248" n="e" s="T464">ne, </ts>
               <ts e="T466" id="Seg_4250" n="e" s="T465">Kazan </ts>
               <ts e="T467" id="Seg_4252" n="e" s="T466">turagən </ts>
               <ts e="T468" id="Seg_4254" n="e" s="T467">onʼiʔ </ts>
               <ts e="T469" id="Seg_4256" n="e" s="T468">ne </ts>
               <ts e="T470" id="Seg_4258" n="e" s="T469">tibibə </ts>
               <ts e="T471" id="Seg_4260" n="e" s="T470">bar </ts>
               <ts e="T472" id="Seg_4262" n="e" s="T471">jaʔpi </ts>
               <ts e="T473" id="Seg_4264" n="e" s="T472">baltuzʼiʔ, </ts>
               <ts e="T474" id="Seg_4266" n="e" s="T473">ulut </ts>
               <ts e="T475" id="Seg_4268" n="e" s="T474">sajjaʔpi. </ts>
               <ts e="T476" id="Seg_4270" n="e" s="T475">Dĭn </ts>
               <ts e="T477" id="Seg_4272" n="e" s="T476">tibi </ts>
               <ts e="T478" id="Seg_4274" n="e" s="T477">onʼiʔ </ts>
               <ts e="T479" id="Seg_4276" n="e" s="T478">kunolluʔpi </ts>
               <ts e="T480" id="Seg_4278" n="e" s="T479">bar. </ts>
               <ts e="T481" id="Seg_4280" n="e" s="T480">Ugaːndə </ts>
               <ts e="T482" id="Seg_4282" n="e" s="T481">(pimn-) </ts>
               <ts e="T483" id="Seg_4284" n="e" s="T482">pimbiem. </ts>
               <ts e="T484" id="Seg_4286" n="e" s="T483">Kăda </ts>
               <ts e="T485" id="Seg_4288" n="e" s="T484">măna </ts>
               <ts e="T486" id="Seg_4290" n="e" s="T485">ulum </ts>
               <ts e="T487" id="Seg_4292" n="e" s="T486">ej </ts>
               <ts e="T488" id="Seg_4294" n="e" s="T487">jaʔpi. </ts>
               <ts e="T489" id="Seg_4296" n="e" s="T488">Dĭ </ts>
               <ts e="T490" id="Seg_4298" n="e" s="T489">bar </ts>
               <ts e="T491" id="Seg_4300" n="e" s="T490">nanəʔzəbi </ts>
               <ts e="T492" id="Seg_4302" n="e" s="T491">ibi </ts>
               <ts e="T493" id="Seg_4304" n="e" s="T492">i </ts>
               <ts e="T494" id="Seg_4306" n="e" s="T493">šide </ts>
               <ts e="T495" id="Seg_4308" n="e" s="T494">esseŋ </ts>
               <ts e="T496" id="Seg_4310" n="e" s="T495">maluʔpiʔi. </ts>
               <ts e="T497" id="Seg_4312" n="e" s="T496">(Uždə- </ts>
               <ts e="T498" id="Seg_4314" n="e" s="T497">užə- </ts>
               <ts e="T499" id="Seg_4316" n="e" s="T498">u-) </ts>
               <ts e="T500" id="Seg_4318" n="e" s="T499">Bar </ts>
               <ts e="T501" id="Seg_4320" n="e" s="T500">sumna </ts>
               <ts e="T502" id="Seg_4322" n="e" s="T501">pʼe </ts>
               <ts e="T503" id="Seg_4324" n="e" s="T502">kalla </ts>
               <ts e="T504" id="Seg_4326" n="e" s="T503">dʼürbi. </ts>
               <ts e="T505" id="Seg_4328" n="e" s="T504">Taldʼen </ts>
               <ts e="T506" id="Seg_4330" n="e" s="T505">ej </ts>
               <ts e="T507" id="Seg_4332" n="e" s="T506">ibiel. </ts>
               <ts e="T508" id="Seg_4334" n="e" s="T507">Teinen </ts>
               <ts e="T509" id="Seg_4336" n="e" s="T508">(šol </ts>
               <ts e="T510" id="Seg_4338" n="e" s="T509">-) </ts>
               <ts e="T511" id="Seg_4340" n="e" s="T510">šobial </ts>
               <ts e="T512" id="Seg_4342" n="e" s="T511">nʼilgösʼtə. </ts>
               <ts e="T513" id="Seg_4344" n="e" s="T512">Ĭmbi </ts>
               <ts e="T514" id="Seg_4346" n="e" s="T513">dʼăbaktərlaʔbəbaʔ? </ts>
               <ts e="T515" id="Seg_4348" n="e" s="T514">Măn </ts>
               <ts e="T516" id="Seg_4350" n="e" s="T515">teinen </ts>
               <ts e="T517" id="Seg_4352" n="e" s="T516">kudaj </ts>
               <ts e="T518" id="Seg_4354" n="e" s="T517">(numanə) </ts>
               <ts e="T519" id="Seg_4356" n="e" s="T518">(üzə- </ts>
               <ts e="T520" id="Seg_4358" n="e" s="T519">üz-) </ts>
               <ts e="T521" id="Seg_4360" n="e" s="T520">üzləm. </ts>
               <ts e="T522" id="Seg_4362" n="e" s="T521">Kudaj, </ts>
               <ts e="T523" id="Seg_4364" n="e" s="T522">šoʔ </ts>
               <ts e="T524" id="Seg_4366" n="e" s="T523">măna! </ts>
               <ts e="T525" id="Seg_4368" n="e" s="T524">Nʼilgöt, </ts>
               <ts e="T526" id="Seg_4370" n="e" s="T525">kăda </ts>
               <ts e="T527" id="Seg_4372" n="e" s="T526">măn </ts>
               <ts e="T528" id="Seg_4374" n="e" s="T527">üzliem. </ts>
               <ts e="T529" id="Seg_4376" n="e" s="T528">Sĭjbə </ts>
               <ts e="T530" id="Seg_4378" n="e" s="T529">(măndərdə-) </ts>
               <ts e="T531" id="Seg_4380" n="e" s="T530">măndəʔ. </ts>
               <ts e="T532" id="Seg_4382" n="e" s="T531">(Padʼi) </ts>
               <ts e="T533" id="Seg_4384" n="e" s="T532">(b-) </ts>
               <ts e="T534" id="Seg_4386" n="e" s="T533">arəliaʔ! </ts>
               <ts e="T535" id="Seg_4388" n="e" s="T534">A </ts>
               <ts e="T536" id="Seg_4390" n="e" s="T535">ej </ts>
               <ts e="T537" id="Seg_4392" n="e" s="T536">arə </ts>
               <ts e="T538" id="Seg_4394" n="e" s="T537">dăk, </ts>
               <ts e="T539" id="Seg_4396" n="e" s="T538">bəzit </ts>
               <ts e="T540" id="Seg_4398" n="e" s="T539">kemzʼiʔ! </ts>
               <ts e="T541" id="Seg_4400" n="e" s="T540">Tăn </ts>
               <ts e="T542" id="Seg_4402" n="e" s="T541">pagən </ts>
               <ts e="T543" id="Seg_4404" n="e" s="T542">edəbiel. </ts>
               <ts e="T544" id="Seg_4406" n="e" s="T543">Kembə </ts>
               <ts e="T545" id="Seg_4408" n="e" s="T544">mʼaŋnaʔpi. </ts>
               <ts e="T546" id="Seg_4410" n="e" s="T545">Bəzit </ts>
               <ts e="T547" id="Seg_4412" n="e" s="T546">măna! </ts>
               <ts e="T548" id="Seg_4414" n="e" s="T547">Jakšə </ts>
               <ts e="T549" id="Seg_4416" n="e" s="T548">bəzit! </ts>
               <ts e="T550" id="Seg_4418" n="e" s="T549">Tăn, </ts>
               <ts e="T551" id="Seg_4420" n="e" s="T550">kudaj, </ts>
               <ts e="T552" id="Seg_4422" n="e" s="T551">šaldə </ts>
               <ts e="T553" id="Seg_4424" n="e" s="T552">iʔgö. </ts>
               <ts e="T554" id="Seg_4426" n="e" s="T553">Bar </ts>
               <ts e="T555" id="Seg_4428" n="e" s="T554">(i-) </ts>
               <ts e="T556" id="Seg_4430" n="e" s="T555">iləm </ts>
               <ts e="T557" id="Seg_4432" n="e" s="T556">(tʼa-) </ts>
               <ts e="T558" id="Seg_4434" n="e" s="T557">ajirlaʔbəl. </ts>
               <ts e="T559" id="Seg_4436" n="e" s="T558">(M-) </ts>
               <ts e="T560" id="Seg_4438" n="e" s="T559">Ipek </ts>
               <ts e="T561" id="Seg_4440" n="e" s="T560">mĭliel. </ts>
               <ts e="T562" id="Seg_4442" n="e" s="T561">(Kuj-). </ts>
               <ts e="T563" id="Seg_4444" n="e" s="T562">Tăn </ts>
               <ts e="T564" id="Seg_4446" n="e" s="T563">kudaj </ts>
               <ts e="T565" id="Seg_4448" n="e" s="T564">šaldə </ts>
               <ts e="T566" id="Seg_4450" n="e" s="T565">iʔgö. </ts>
               <ts e="T567" id="Seg_4452" n="e" s="T566">Tăn </ts>
               <ts e="T568" id="Seg_4454" n="e" s="T567">(il= </ts>
               <ts e="T569" id="Seg_4456" n="e" s="T568">il=) </ts>
               <ts e="T570" id="Seg_4458" n="e" s="T569">il </ts>
               <ts e="T571" id="Seg_4460" n="e" s="T570">ajirial. </ts>
               <ts e="T572" id="Seg_4462" n="e" s="T571">Ipek </ts>
               <ts e="T573" id="Seg_4464" n="e" s="T572">dĭzeŋdə </ts>
               <ts e="T574" id="Seg_4466" n="e" s="T573">mĭliel. </ts>
               <ts e="T575" id="Seg_4468" n="e" s="T574">Bar </ts>
               <ts e="T576" id="Seg_4470" n="e" s="T575">kujnek </ts>
               <ts e="T577" id="Seg_4472" n="e" s="T576">mĭliel. </ts>
               <ts e="T578" id="Seg_4474" n="e" s="T577">Bü </ts>
               <ts e="T579" id="Seg_4476" n="e" s="T578">mĭliel. </ts>
               <ts e="T580" id="Seg_4478" n="e" s="T579">I </ts>
               <ts e="T581" id="Seg_4480" n="e" s="T580">măna </ts>
               <ts e="T582" id="Seg_4482" n="e" s="T581">ajiraʔ! </ts>
               <ts e="T587" id="Seg_4484" n="e" s="T582">Чего-то (никак)… </ts>
               <ts e="T588" id="Seg_4486" n="e" s="T587">Tăn </ts>
               <ts e="T589" id="Seg_4488" n="e" s="T588">bar </ts>
               <ts e="T590" id="Seg_4490" n="e" s="T589">ejü </ts>
               <ts e="T591" id="Seg_4492" n="e" s="T590">öʔliel. </ts>
               <ts e="T592" id="Seg_4494" n="e" s="T591">Surno </ts>
               <ts e="T593" id="Seg_4496" n="e" s="T592">mĭliel, </ts>
               <ts e="T594" id="Seg_4498" n="e" s="T593">štobɨ </ts>
               <ts e="T595" id="Seg_4500" n="e" s="T594">ipek </ts>
               <ts e="T596" id="Seg_4502" n="e" s="T595">özerleʔpi. </ts>
               <ts e="T599" id="Seg_4504" n="e" s="T596">Чтобы хлеб (рос/рожь)… </ts>
               <ts e="T600" id="Seg_4506" n="e" s="T599">Kudajzʼiʔ </ts>
               <ts e="T601" id="Seg_4508" n="e" s="T600">jakšə, </ts>
               <ts e="T602" id="Seg_4510" n="e" s="T601">gibər </ts>
               <ts e="T603" id="Seg_4512" n="e" s="T602">kallal, </ts>
               <ts e="T604" id="Seg_4514" n="e" s="T603">kudajzʼiʔ </ts>
               <ts e="T605" id="Seg_4516" n="e" s="T604">(vez- </ts>
               <ts e="T606" id="Seg_4518" n="e" s="T605">vez-); </ts>
               <ts e="T607" id="Seg_4520" n="e" s="T606">ugaːndə </ts>
               <ts e="T608" id="Seg_4522" n="e" s="T607">jakšə </ts>
               <ts e="T609" id="Seg_4524" n="e" s="T608">kudajzʼiʔ. </ts>
               <ts e="T610" id="Seg_4526" n="e" s="T609">Beržə </ts>
               <ts e="T611" id="Seg_4528" n="e" s="T610">saʔməluʔpi. </ts>
               <ts e="T612" id="Seg_4530" n="e" s="T611">Măn </ts>
               <ts e="T613" id="Seg_4532" n="e" s="T612">ibiem </ts>
               <ts e="T614" id="Seg_4534" n="e" s="T613">da </ts>
               <ts e="T615" id="Seg_4536" n="e" s="T614">turanə </ts>
               <ts e="T616" id="Seg_4538" n="e" s="T615">deʔpiem. </ts>
               <ts e="T617" id="Seg_4540" n="e" s="T616">Jakšə. </ts>
               <ts e="T618" id="Seg_4542" n="e" s="T617">Surno </ts>
               <ts e="T619" id="Seg_4544" n="e" s="T618">šonəga, </ts>
               <ts e="T620" id="Seg_4546" n="e" s="T619">ipek </ts>
               <ts e="T621" id="Seg_4548" n="e" s="T620">özerləj. </ts>
               <ts e="T622" id="Seg_4550" n="e" s="T621">Măn </ts>
               <ts e="T623" id="Seg_4552" n="e" s="T622">dĭʔnə </ts>
               <ts e="T624" id="Seg_4554" n="e" s="T623">kabažarbiam. </ts>
               <ts e="T625" id="Seg_4556" n="e" s="T624">A </ts>
               <ts e="T626" id="Seg_4558" n="e" s="T625">dĭ </ts>
               <ts e="T627" id="Seg_4560" n="e" s="T626">măna </ts>
               <ts e="T628" id="Seg_4562" n="e" s="T627">ej </ts>
               <ts e="T629" id="Seg_4564" n="e" s="T628">kabažarlaʔbə. </ts>
               <ts e="T630" id="Seg_4566" n="e" s="T629">Ej </ts>
               <ts e="T631" id="Seg_4568" n="e" s="T630">mĭlie </ts>
               <ts e="T632" id="Seg_4570" n="e" s="T631">măna </ts>
               <ts e="T633" id="Seg_4572" n="e" s="T632">togonorzittə, </ts>
               <ts e="T634" id="Seg_4574" n="e" s="T633">măn </ts>
               <ts e="T635" id="Seg_4576" n="e" s="T634">bɨ </ts>
               <ts e="T636" id="Seg_4578" n="e" s="T635">togonorbiam, </ts>
               <ts e="T637" id="Seg_4580" n="e" s="T636">a </ts>
               <ts e="T638" id="Seg_4582" n="e" s="T637">dĭ </ts>
               <ts e="T639" id="Seg_4584" n="e" s="T638">üge </ts>
               <ts e="T640" id="Seg_4586" n="e" s="T639">(puʔlaʔbə) </ts>
               <ts e="T641" id="Seg_4588" n="e" s="T640">măna. </ts>
               <ts e="T642" id="Seg_4590" n="e" s="T641">Kurojək </ts>
               <ts e="T643" id="Seg_4592" n="e" s="T642">bar. </ts>
               <ts e="T644" id="Seg_4594" n="e" s="T643">Ujuzʼiʔ </ts>
               <ts e="T645" id="Seg_4596" n="e" s="T644">bar </ts>
               <ts e="T646" id="Seg_4598" n="e" s="T645">tonuʔpi. </ts>
               <ts e="T647" id="Seg_4600" n="e" s="T646">Ugaːndə </ts>
               <ts e="T648" id="Seg_4602" n="e" s="T647">kurojək </ts>
               <ts e="T649" id="Seg_4604" n="e" s="T648">men. </ts>
               <ts e="T650" id="Seg_4606" n="e" s="T649">(Bal-) </ts>
               <ts e="T651" id="Seg_4608" n="e" s="T650">Bar </ts>
               <ts e="T652" id="Seg_4610" n="e" s="T651">(tal-) </ts>
               <ts e="T653" id="Seg_4612" n="e" s="T652">talbəlia </ts>
               <ts e="T654" id="Seg_4614" n="e" s="T653">il. </ts>
               <ts e="T656" id="Seg_4616" n="e" s="T654">Людей кусает. </ts>
               <ts e="T657" id="Seg_4618" n="e" s="T656">Ugaːndə </ts>
               <ts e="T658" id="Seg_4620" n="e" s="T657">kurojək </ts>
               <ts e="T659" id="Seg_4622" n="e" s="T658">men. </ts>
               <ts e="T660" id="Seg_4624" n="e" s="T659">(Ba-) </ts>
               <ts e="T661" id="Seg_4626" n="e" s="T660">Bar </ts>
               <ts e="T662" id="Seg_4628" n="e" s="T661">il </ts>
               <ts e="T663" id="Seg_4630" n="e" s="T662">talbərlaʔbə. </ts>
               <ts e="T664" id="Seg_4632" n="e" s="T663">I </ts>
               <ts e="T665" id="Seg_4634" n="e" s="T664">măna </ts>
               <ts e="T666" id="Seg_4636" n="e" s="T665">talbərbi. </ts>
               <ts e="T667" id="Seg_4638" n="e" s="T666">Măn </ts>
               <ts e="T668" id="Seg_4640" n="e" s="T667">šiʔ </ts>
               <ts e="T669" id="Seg_4642" n="e" s="T668">edəʔpiem, </ts>
               <ts e="T670" id="Seg_4644" n="e" s="T669">šiʔ </ts>
               <ts e="T671" id="Seg_4646" n="e" s="T670">ej </ts>
               <ts e="T672" id="Seg_4648" n="e" s="T671">šobilaʔ, </ts>
               <ts e="T673" id="Seg_4650" n="e" s="T672">kundʼo </ts>
               <ts e="T674" id="Seg_4652" n="e" s="T673">nagobilaʔ. </ts>
               <ts e="T675" id="Seg_4654" n="e" s="T674">Takšəgəʔ </ts>
               <ts e="T676" id="Seg_4656" n="e" s="T675">ibiem, </ts>
               <ts e="T677" id="Seg_4658" n="e" s="T676">bəzəbiam. </ts>
               <ts e="T678" id="Seg_4660" n="e" s="T677">I </ts>
               <ts e="T679" id="Seg_4662" n="e" s="T678">bazoʔ </ts>
               <ts e="T680" id="Seg_4664" n="e" s="T679">embiem. </ts>
               <ts e="T681" id="Seg_4666" n="e" s="T680">Teinöbam, </ts>
               <ts e="T682" id="Seg_4668" n="e" s="T681">teinöbam </ts>
               <ts e="T683" id="Seg_4670" n="e" s="T682">teinen </ts>
               <ts e="T684" id="Seg_4672" n="e" s="T683">bar. </ts>
               <ts e="T686" id="Seg_4674" n="e" s="T684">никак. </ts>
               <ts e="T687" id="Seg_4676" n="e" s="T686">Bünə </ts>
               <ts e="T688" id="Seg_4678" n="e" s="T687">ej </ts>
               <ts e="T689" id="Seg_4680" n="e" s="T688">saʔməlial, </ts>
               <ts e="T690" id="Seg_4682" n="e" s="T689">nʼuʔdun </ts>
               <ts e="T691" id="Seg_4684" n="e" s="T690">bar </ts>
               <ts e="T692" id="Seg_4686" n="e" s="T691">mĭŋgə </ts>
               <ts e="T693" id="Seg_4688" n="e" s="T692">udazʼiʔ </ts>
               <ts e="T694" id="Seg_4690" n="e" s="T693">i </ts>
               <ts e="T695" id="Seg_4692" n="e" s="T694">üjüzʼiʔ. </ts>
               <ts e="T696" id="Seg_4694" n="e" s="T695">Bügən </ts>
               <ts e="T697" id="Seg_4696" n="e" s="T696">bar </ts>
               <ts e="T698" id="Seg_4698" n="e" s="T697">kandəgal </ts>
               <ts e="T699" id="Seg_4700" n="e" s="T698">udazʼiʔ </ts>
               <ts e="T700" id="Seg_4702" n="e" s="T699">i </ts>
               <ts e="T701" id="Seg_4704" n="e" s="T700">üjüzʼiʔ. </ts>
               <ts e="T702" id="Seg_4706" n="e" s="T701">(Lotka=) </ts>
               <ts e="T703" id="Seg_4708" n="e" s="T702">Lotkaizʼiʔ </ts>
               <ts e="T704" id="Seg_4710" n="e" s="T703">bar </ts>
               <ts e="T705" id="Seg_4712" n="e" s="T704">(bün-) </ts>
               <ts e="T706" id="Seg_4714" n="e" s="T705">büʔnə </ts>
               <ts e="T707" id="Seg_4716" n="e" s="T706">kandəga. </ts>
               <ts e="T708" id="Seg_4718" n="e" s="T707">Kola </ts>
               <ts e="T709" id="Seg_4720" n="e" s="T708">dʼabəlaʔbə </ts>
               <ts e="T710" id="Seg_4722" n="e" s="T709">bar. </ts>
               <ts e="T711" id="Seg_4724" n="e" s="T710">Nagurgöʔ </ts>
               <ts e="T712" id="Seg_4726" n="e" s="T711">(tʼakamnəbaʔ). </ts>
               <ts e="T713" id="Seg_4728" n="e" s="T712">Onʼiʔ </ts>
               <ts e="T714" id="Seg_4730" n="e" s="T713">tănan, </ts>
               <ts e="T715" id="Seg_4732" n="e" s="T714">onʼiʔ </ts>
               <ts e="T716" id="Seg_4734" n="e" s="T715">măna, </ts>
               <ts e="T717" id="Seg_4736" n="e" s="T716">onʼiʔ </ts>
               <ts e="T718" id="Seg_4738" n="e" s="T717">dĭʔnə. </ts>
               <ts e="T719" id="Seg_4740" n="e" s="T718">Šide </ts>
               <ts e="T720" id="Seg_4742" n="e" s="T719">pʼel, </ts>
               <ts e="T721" id="Seg_4744" n="e" s="T720">nagur </ts>
               <ts e="T722" id="Seg_4746" n="e" s="T721">pʼel, </ts>
               <ts e="T723" id="Seg_4748" n="e" s="T722">teʔtə </ts>
               <ts e="T724" id="Seg_4750" n="e" s="T723">pʼel. </ts>
               <ts e="T725" id="Seg_4752" n="e" s="T724">Dăra </ts>
               <ts e="T726" id="Seg_4754" n="e" s="T725">(dĭ=) </ts>
               <ts e="T727" id="Seg_4756" n="e" s="T726">dĭzeŋ </ts>
               <ts e="T728" id="Seg_4758" n="e" s="T727">bar </ts>
               <ts e="T729" id="Seg_4760" n="e" s="T728">amnobiʔi. </ts>
               <ts e="T730" id="Seg_4762" n="e" s="T729">Onʼiʔ </ts>
               <ts e="T731" id="Seg_4764" n="e" s="T730">inegən </ts>
               <ts e="T732" id="Seg_4766" n="e" s="T731">măn </ts>
               <ts e="T733" id="Seg_4768" n="e" s="T732">amnobiam. </ts>
               <ts e="T734" id="Seg_4770" n="e" s="T733">Šide </ts>
               <ts e="T735" id="Seg_4772" n="e" s="T734">inegən </ts>
               <ts e="T736" id="Seg_4774" n="e" s="T735">nem </ts>
               <ts e="T737" id="Seg_4776" n="e" s="T736">amnəbi. </ts>
               <ts e="T738" id="Seg_4778" n="e" s="T737">Nagur </ts>
               <ts e="T739" id="Seg_4780" n="e" s="T738">inegən </ts>
               <ts e="T740" id="Seg_4782" n="e" s="T739">nʼim </ts>
               <ts e="T741" id="Seg_4784" n="e" s="T740">amnəbi. </ts>
               <ts e="T742" id="Seg_4786" n="e" s="T741">Teʔtə </ts>
               <ts e="T743" id="Seg_4788" n="e" s="T742">inegən </ts>
               <ts e="T744" id="Seg_4790" n="e" s="T743">koʔbdom </ts>
               <ts e="T745" id="Seg_4792" n="e" s="T744">amnəbi. </ts>
               <ts e="T746" id="Seg_4794" n="e" s="T745">A </ts>
               <ts e="T747" id="Seg_4796" n="e" s="T746">sumna </ts>
               <ts e="T748" id="Seg_4798" n="e" s="T747">inegən </ts>
               <ts e="T749" id="Seg_4800" n="e" s="T748">urgoja </ts>
               <ts e="T750" id="Seg_4802" n="e" s="T749">amnəbi. </ts>
            </ts>
            <ts e="T828" id="Seg_4803" n="sc" s="T751">
               <ts e="T752" id="Seg_4805" n="e" s="T751">Dĭ </ts>
               <ts e="T753" id="Seg_4807" n="e" s="T752">nʼi </ts>
               <ts e="T754" id="Seg_4809" n="e" s="T753">šobi </ts>
               <ts e="T755" id="Seg_4811" n="e" s="T754">miʔnʼibeʔ </ts>
               <ts e="T756" id="Seg_4813" n="e" s="T755">kürzittə </ts>
               <ts e="T757" id="Seg_4815" n="e" s="T756">bar. </ts>
               <ts e="T758" id="Seg_4817" n="e" s="T757">Büžü </ts>
               <ts e="T759" id="Seg_4819" n="e" s="T758">bar </ts>
               <ts e="T760" id="Seg_4821" n="e" s="T759">šĭšəge </ts>
               <ts e="T761" id="Seg_4823" n="e" s="T760">moləj. </ts>
               <ts e="T762" id="Seg_4825" n="e" s="T761">(Sĭ-) </ts>
               <ts e="T763" id="Seg_4827" n="e" s="T762">Sĭre </ts>
               <ts e="T764" id="Seg_4829" n="e" s="T763">bar </ts>
               <ts e="T765" id="Seg_4831" n="e" s="T764">saʔmələj. </ts>
               <ts e="T766" id="Seg_4833" n="e" s="T765">Ugaːndə </ts>
               <ts e="T767" id="Seg_4835" n="e" s="T766">šĭšəge. </ts>
               <ts e="T768" id="Seg_4837" n="e" s="T767">Kănnaləl </ts>
               <ts e="T769" id="Seg_4839" n="e" s="T768">bar. </ts>
               <ts e="T770" id="Seg_4841" n="e" s="T769">Büžü </ts>
               <ts e="T771" id="Seg_4843" n="e" s="T770">(bəj- </ts>
               <ts e="T772" id="Seg_4845" n="e" s="T771">ar-) </ts>
               <ts e="T773" id="Seg_4847" n="e" s="T772">bar </ts>
               <ts e="T774" id="Seg_4849" n="e" s="T773">ejü </ts>
               <ts e="T775" id="Seg_4851" n="e" s="T774">moləj. </ts>
               <ts e="T776" id="Seg_4853" n="e" s="T775">Dĭgəttə </ts>
               <ts e="T777" id="Seg_4855" n="e" s="T776">il </ts>
               <ts e="T778" id="Seg_4857" n="e" s="T777">(tarirluʔpiʔi) </ts>
               <ts e="T779" id="Seg_4859" n="e" s="T778">bar. </ts>
               <ts e="T780" id="Seg_4861" n="e" s="T779">Budəj </ts>
               <ts e="T781" id="Seg_4863" n="e" s="T780">kuʔluʔjəʔ. </ts>
               <ts e="T782" id="Seg_4865" n="e" s="T781">Ugaːndə </ts>
               <ts e="T783" id="Seg_4867" n="e" s="T782">kuŋgem </ts>
               <ts e="T784" id="Seg_4869" n="e" s="T783">nʼergölaʔbə </ts>
               <ts e="T785" id="Seg_4871" n="e" s="T784">(nʼiʔn-) </ts>
               <ts e="T786" id="Seg_4873" n="e" s="T785">nʼuʔnə. </ts>
               <ts e="T787" id="Seg_4875" n="e" s="T786">Ej </ts>
               <ts e="T788" id="Seg_4877" n="e" s="T787">idlia. </ts>
               <ts e="T790" id="Seg_4879" n="e" s="T788">I… </ts>
               <ts e="T791" id="Seg_4881" n="e" s="T790">Inegəʔ </ts>
               <ts e="T792" id="Seg_4883" n="e" s="T791">(inezʼ- </ts>
               <ts e="T793" id="Seg_4885" n="e" s="T792">inezeŋ-) </ts>
               <ts e="T794" id="Seg_4887" n="e" s="T793">inegəʔ </ts>
               <ts e="T795" id="Seg_4889" n="e" s="T794">bar </ts>
               <ts e="T796" id="Seg_4891" n="e" s="T795">dʼünə </ts>
               <ts e="T797" id="Seg_4893" n="e" s="T796">saʔməluʔpi. </ts>
               <ts e="T798" id="Seg_4895" n="e" s="T797">Nʼuʔdə </ts>
               <ts e="T799" id="Seg_4897" n="e" s="T798">bar </ts>
               <ts e="T800" id="Seg_4899" n="e" s="T799">nada </ts>
               <ts e="T801" id="Seg_4901" n="e" s="T800">kanzittə </ts>
               <ts e="T802" id="Seg_4903" n="e" s="T801">ugaːndə. </ts>
               <ts e="T805" id="Seg_4905" n="e" s="T802">Больше не знаю. </ts>
               <ts e="T806" id="Seg_4907" n="e" s="T805">((DMG)). </ts>
               <ts e="T807" id="Seg_4909" n="e" s="T806">Ugaːndə </ts>
               <ts e="T808" id="Seg_4911" n="e" s="T807">(kudaj) </ts>
               <ts e="T809" id="Seg_4913" n="e" s="T808">nʼuʔdə </ts>
               <ts e="T810" id="Seg_4915" n="e" s="T809">amnolaʔbə. </ts>
               <ts e="T811" id="Seg_4917" n="e" s="T810">Măjagən, </ts>
               <ts e="T812" id="Seg_4919" n="e" s="T811">(uʔ- </ts>
               <ts e="T813" id="Seg_4921" n="e" s="T812">uj-) </ts>
               <ts e="T814" id="Seg_4923" n="e" s="T813">urgo </ts>
               <ts e="T815" id="Seg_4925" n="e" s="T814">(uj-) </ts>
               <ts e="T816" id="Seg_4927" n="e" s="T815">măja. </ts>
               <ts e="T817" id="Seg_4929" n="e" s="T816">Ulum </ts>
               <ts e="T818" id="Seg_4931" n="e" s="T817">bar </ts>
               <ts e="T819" id="Seg_4933" n="e" s="T818">(tʼukumnie). </ts>
               <ts e="T820" id="Seg_4935" n="e" s="T819">Kădaʔliom, </ts>
               <ts e="T821" id="Seg_4937" n="e" s="T820">naverna </ts>
               <ts e="T822" id="Seg_4939" n="e" s="T821">unu </ts>
               <ts e="T823" id="Seg_4941" n="e" s="T822">iʔgö. </ts>
               <ts e="T824" id="Seg_4943" n="e" s="T823">Nada </ts>
               <ts e="T825" id="Seg_4945" n="e" s="T824">dĭzeŋ </ts>
               <ts e="T826" id="Seg_4947" n="e" s="T825">bar </ts>
               <ts e="T828" id="Seg_4949" n="e" s="T826">kutzʼittə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T12" id="Seg_4950" s="T4">PKZ_1964_SU0208.PKZ.001 (002.001)</ta>
            <ta e="T14" id="Seg_4951" s="T12">PKZ_1964_SU0208.PKZ.002 (002.002)</ta>
            <ta e="T20" id="Seg_4952" s="T14">PKZ_1964_SU0208.PKZ.003 (003)</ta>
            <ta e="T25" id="Seg_4953" s="T20">PKZ_1964_SU0208.PKZ.004 (004.001)</ta>
            <ta e="T32" id="Seg_4954" s="T25">PKZ_1964_SU0208.PKZ.005 (004.002)</ta>
            <ta e="T38" id="Seg_4955" s="T32">PKZ_1964_SU0208.PKZ.006 (005)</ta>
            <ta e="T43" id="Seg_4956" s="T38">PKZ_1964_SU0208.PKZ.007 (006.001)</ta>
            <ta e="T47" id="Seg_4957" s="T43">PKZ_1964_SU0208.PKZ.008 (006.002)</ta>
            <ta e="T50" id="Seg_4958" s="T47">PKZ_1964_SU0208.PKZ.009 (006.003)</ta>
            <ta e="T58" id="Seg_4959" s="T50">PKZ_1964_SU0208.PKZ.010 (006.004)</ta>
            <ta e="T61" id="Seg_4960" s="T58">PKZ_1964_SU0208.PKZ.011 (007.001)</ta>
            <ta e="T64" id="Seg_4961" s="T61">PKZ_1964_SU0208.PKZ.012 (007.002)</ta>
            <ta e="T66" id="Seg_4962" s="T64">PKZ_1964_SU0208.PKZ.013 (007.003)</ta>
            <ta e="T74" id="Seg_4963" s="T66">PKZ_1964_SU0208.PKZ.014 (007.004)</ta>
            <ta e="T77" id="Seg_4964" s="T74">PKZ_1964_SU0208.PKZ.015 (008.001)</ta>
            <ta e="T80" id="Seg_4965" s="T77">PKZ_1964_SU0208.PKZ.016 (008.002)</ta>
            <ta e="T84" id="Seg_4966" s="T80">PKZ_1964_SU0208.PKZ.017 (008.003)</ta>
            <ta e="T86" id="Seg_4967" s="T84">PKZ_1964_SU0208.PKZ.018 (008.004)</ta>
            <ta e="T91" id="Seg_4968" s="T86">PKZ_1964_SU0208.PKZ.019 (009.001)</ta>
            <ta e="T94" id="Seg_4969" s="T91">PKZ_1964_SU0208.PKZ.020 (009.002)</ta>
            <ta e="T96" id="Seg_4970" s="T94">PKZ_1964_SU0208.PKZ.021 (009.003)</ta>
            <ta e="T100" id="Seg_4971" s="T96">PKZ_1964_SU0208.PKZ.022 (009.004)</ta>
            <ta e="T105" id="Seg_4972" s="T100">PKZ_1964_SU0208.PKZ.023 (009.005)</ta>
            <ta e="T112" id="Seg_4973" s="T105">PKZ_1964_SU0208.PKZ.024 (009.006)</ta>
            <ta e="T114" id="Seg_4974" s="T112">PKZ_1964_SU0208.PKZ.025 (009.007)</ta>
            <ta e="T116" id="Seg_4975" s="T114">PKZ_1964_SU0208.PKZ.026 (009.008)</ta>
            <ta e="T119" id="Seg_4976" s="T116">PKZ_1964_SU0208.PKZ.027 (009.009)</ta>
            <ta e="T123" id="Seg_4977" s="T119">PKZ_1964_SU0208.PKZ.028 (009.010)</ta>
            <ta e="T126" id="Seg_4978" s="T123">PKZ_1964_SU0208.PKZ.029 (009.011)</ta>
            <ta e="T132" id="Seg_4979" s="T126">PKZ_1964_SU0208.PKZ.030 (009.012)</ta>
            <ta e="T136" id="Seg_4980" s="T132">PKZ_1964_SU0208.PKZ.031 (010.001)</ta>
            <ta e="T142" id="Seg_4981" s="T136">PKZ_1964_SU0208.PKZ.032 (010.002)</ta>
            <ta e="T146" id="Seg_4982" s="T142">PKZ_1964_SU0208.PKZ.033 (011.001)</ta>
            <ta e="T149" id="Seg_4983" s="T146">PKZ_1964_SU0208.PKZ.034 (011.002)</ta>
            <ta e="T153" id="Seg_4984" s="T149">PKZ_1964_SU0208.PKZ.035 (011.003)</ta>
            <ta e="T161" id="Seg_4985" s="T153">PKZ_1964_SU0208.PKZ.036 (012.001)</ta>
            <ta e="T163" id="Seg_4986" s="T161">PKZ_1964_SU0208.PKZ.037 (012.002)</ta>
            <ta e="T167" id="Seg_4987" s="T163">PKZ_1964_SU0208.PKZ.038 (012.003)</ta>
            <ta e="T170" id="Seg_4988" s="T167">PKZ_1964_SU0208.PKZ.039 (012.004)</ta>
            <ta e="T173" id="Seg_4989" s="T170">PKZ_1964_SU0208.PKZ.040 (012.005)</ta>
            <ta e="T180" id="Seg_4990" s="T173">PKZ_1964_SU0208.PKZ.041 (012.006)</ta>
            <ta e="T183" id="Seg_4991" s="T180">PKZ_1964_SU0208.PKZ.042 (013.001)</ta>
            <ta e="T187" id="Seg_4992" s="T183">PKZ_1964_SU0208.PKZ.043 (013.002)</ta>
            <ta e="T191" id="Seg_4993" s="T187">PKZ_1964_SU0208.PKZ.044 (013.003)</ta>
            <ta e="T193" id="Seg_4994" s="T191">PKZ_1964_SU0208.PKZ.045 (013.004)</ta>
            <ta e="T196" id="Seg_4995" s="T193">PKZ_1964_SU0208.PKZ.046 (013.005)</ta>
            <ta e="T198" id="Seg_4996" s="T196">PKZ_1964_SU0208.PKZ.047 (013.006)</ta>
            <ta e="T203" id="Seg_4997" s="T198">PKZ_1964_SU0208.PKZ.048 (013.007)</ta>
            <ta e="T210" id="Seg_4998" s="T203">PKZ_1964_SU0208.PKZ.049 (014.001)</ta>
            <ta e="T212" id="Seg_4999" s="T210">PKZ_1964_SU0208.PKZ.050 (014.002)</ta>
            <ta e="T216" id="Seg_5000" s="T212">PKZ_1964_SU0208.PKZ.051 (014.003)</ta>
            <ta e="T222" id="Seg_5001" s="T216">PKZ_1964_SU0208.PKZ.052 (015.001)</ta>
            <ta e="T226" id="Seg_5002" s="T222">PKZ_1964_SU0208.PKZ.053 (015.002)</ta>
            <ta e="T231" id="Seg_5003" s="T226">PKZ_1964_SU0208.PKZ.054 (016.001)</ta>
            <ta e="T236" id="Seg_5004" s="T231">PKZ_1964_SU0208.PKZ.055 (016.002)</ta>
            <ta e="T240" id="Seg_5005" s="T236">PKZ_1964_SU0208.PKZ.056 (016.003)</ta>
            <ta e="T242" id="Seg_5006" s="T240">PKZ_1964_SU0208.PKZ.057 (017.001)</ta>
            <ta e="T244" id="Seg_5007" s="T242">PKZ_1964_SU0208.PKZ.058 (017.002)</ta>
            <ta e="T246" id="Seg_5008" s="T244">PKZ_1964_SU0208.PKZ.059 (017.003)</ta>
            <ta e="T251" id="Seg_5009" s="T246">PKZ_1964_SU0208.PKZ.060 (018)</ta>
            <ta e="T256" id="Seg_5010" s="T251">PKZ_1964_SU0208.PKZ.061 (019.001)</ta>
            <ta e="T263" id="Seg_5011" s="T256">PKZ_1964_SU0208.PKZ.062 (019.002)</ta>
            <ta e="T273" id="Seg_5012" s="T263">PKZ_1964_SU0208.PKZ.063 (019.003)</ta>
            <ta e="T275" id="Seg_5013" s="T273">PKZ_1964_SU0208.PKZ.064 (019.005)</ta>
            <ta e="T282" id="Seg_5014" s="T275">PKZ_1964_SU0208.PKZ.065 (020.001)</ta>
            <ta e="T286" id="Seg_5015" s="T282">PKZ_1964_SU0208.PKZ.066 (020.002)</ta>
            <ta e="T289" id="Seg_5016" s="T286">PKZ_1964_SU0208.PKZ.067 (021.001)</ta>
            <ta e="T291" id="Seg_5017" s="T289">PKZ_1964_SU0208.PKZ.068 (021.002)</ta>
            <ta e="T294" id="Seg_5018" s="T291">PKZ_1964_SU0208.PKZ.069 (021.003)</ta>
            <ta e="T299" id="Seg_5019" s="T294">PKZ_1964_SU0208.PKZ.070 (021.004)</ta>
            <ta e="T301" id="Seg_5020" s="T299">PKZ_1964_SU0208.PKZ.071 (021.005)</ta>
            <ta e="T305" id="Seg_5021" s="T301">PKZ_1964_SU0208.PKZ.072 (021.006)</ta>
            <ta e="T308" id="Seg_5022" s="T305">PKZ_1964_SU0208.PKZ.073 (021.007)</ta>
            <ta e="T312" id="Seg_5023" s="T308">PKZ_1964_SU0208.PKZ.074 (021.008)</ta>
            <ta e="T316" id="Seg_5024" s="T312">PKZ_1964_SU0208.PKZ.075 (021.009)</ta>
            <ta e="T321" id="Seg_5025" s="T316">PKZ_1964_SU0208.PKZ.076 (021.010)</ta>
            <ta e="T323" id="Seg_5026" s="T321">PKZ_1964_SU0208.PKZ.077 (021.011)</ta>
            <ta e="T326" id="Seg_5027" s="T323">PKZ_1964_SU0208.PKZ.078 (021.012)</ta>
            <ta e="T333" id="Seg_5028" s="T326">PKZ_1964_SU0208.PKZ.079 (021.013)</ta>
            <ta e="T337" id="Seg_5029" s="T333">PKZ_1964_SU0208.PKZ.080 (022)</ta>
            <ta e="T343" id="Seg_5030" s="T338">PKZ_1964_SU0208.PKZ.081 (024.001)</ta>
            <ta e="T346" id="Seg_5031" s="T343">PKZ_1964_SU0208.PKZ.082 (024.002)</ta>
            <ta e="T361" id="Seg_5032" s="T346">PKZ_1964_SU0208.PKZ.083 (025)</ta>
            <ta e="T363" id="Seg_5033" s="T361">PKZ_1964_SU0208.PKZ.084 (026.001)</ta>
            <ta e="T364" id="Seg_5034" s="T363">PKZ_1964_SU0208.PKZ.085 (026.002)</ta>
            <ta e="T366" id="Seg_5035" s="T364">PKZ_1964_SU0208.PKZ.086 (026.003)</ta>
            <ta e="T370" id="Seg_5036" s="T366">PKZ_1964_SU0208.PKZ.087 (026.004)</ta>
            <ta e="T372" id="Seg_5037" s="T370">PKZ_1964_SU0208.PKZ.088 (026.005)</ta>
            <ta e="T376" id="Seg_5038" s="T372">PKZ_1964_SU0208.PKZ.089 (027.001)</ta>
            <ta e="T379" id="Seg_5039" s="T376">PKZ_1964_SU0208.PKZ.090 (027.002)</ta>
            <ta e="T384" id="Seg_5040" s="T379">PKZ_1964_SU0208.PKZ.091 (027.003)</ta>
            <ta e="T389" id="Seg_5041" s="T384">PKZ_1964_SU0208.PKZ.092 (027.004)</ta>
            <ta e="T395" id="Seg_5042" s="T389">PKZ_1964_SU0208.PKZ.093 (027.005)</ta>
            <ta e="T399" id="Seg_5043" s="T395">PKZ_1964_SU0208.PKZ.094 (028.001)</ta>
            <ta e="T402" id="Seg_5044" s="T399">PKZ_1964_SU0208.PKZ.095 (028.002)</ta>
            <ta e="T406" id="Seg_5045" s="T402">PKZ_1964_SU0208.PKZ.096 (029.001)</ta>
            <ta e="T410" id="Seg_5046" s="T406">PKZ_1964_SU0208.PKZ.097 (029.002)</ta>
            <ta e="T414" id="Seg_5047" s="T410">PKZ_1964_SU0208.PKZ.098 (029.003)</ta>
            <ta e="T418" id="Seg_5048" s="T414">PKZ_1964_SU0208.PKZ.099 (029.004)</ta>
            <ta e="T421" id="Seg_5049" s="T418">PKZ_1964_SU0208.PKZ.100 (029.005)</ta>
            <ta e="T425" id="Seg_5050" s="T421">PKZ_1964_SU0208.PKZ.101 (030.001)</ta>
            <ta e="T431" id="Seg_5051" s="T425">PKZ_1964_SU0208.PKZ.102 (030.002)</ta>
            <ta e="T437" id="Seg_5052" s="T431">PKZ_1964_SU0208.PKZ.103 (031)</ta>
            <ta e="T444" id="Seg_5053" s="T437">PKZ_1964_SU0208.PKZ.104 (032)</ta>
            <ta e="T448" id="Seg_5054" s="T446">PKZ_1964_SU0208.PKZ.105 (034)</ta>
            <ta e="T454" id="Seg_5055" s="T448">PKZ_1964_SU0208.PKZ.106 (035.001)</ta>
            <ta e="T462" id="Seg_5056" s="T454">PKZ_1964_SU0208.PKZ.107 (035.002)</ta>
            <ta e="T475" id="Seg_5057" s="T462">PKZ_1964_SU0208.PKZ.108 (036.001)</ta>
            <ta e="T480" id="Seg_5058" s="T475">PKZ_1964_SU0208.PKZ.109 (036.002)</ta>
            <ta e="T483" id="Seg_5059" s="T480">PKZ_1964_SU0208.PKZ.110 (036.003)</ta>
            <ta e="T488" id="Seg_5060" s="T483">PKZ_1964_SU0208.PKZ.111 (036.004)</ta>
            <ta e="T496" id="Seg_5061" s="T488">PKZ_1964_SU0208.PKZ.112 (036.005)</ta>
            <ta e="T504" id="Seg_5062" s="T496">PKZ_1964_SU0208.PKZ.113 (036.006)</ta>
            <ta e="T507" id="Seg_5063" s="T504">PKZ_1964_SU0208.PKZ.114 (037.001)</ta>
            <ta e="T512" id="Seg_5064" s="T507">PKZ_1964_SU0208.PKZ.115 (037.002)</ta>
            <ta e="T514" id="Seg_5065" s="T512">PKZ_1964_SU0208.PKZ.116 (037.003)</ta>
            <ta e="T521" id="Seg_5066" s="T514">PKZ_1964_SU0208.PKZ.117 (038.001)</ta>
            <ta e="T524" id="Seg_5067" s="T521">PKZ_1964_SU0208.PKZ.118 (038.002)</ta>
            <ta e="T528" id="Seg_5068" s="T524">PKZ_1964_SU0208.PKZ.119 (038.003)</ta>
            <ta e="T531" id="Seg_5069" s="T528">PKZ_1964_SU0208.PKZ.120 (038.004)</ta>
            <ta e="T534" id="Seg_5070" s="T531">PKZ_1964_SU0208.PKZ.121 (038.005)</ta>
            <ta e="T540" id="Seg_5071" s="T534">PKZ_1964_SU0208.PKZ.122 (038.006)</ta>
            <ta e="T543" id="Seg_5072" s="T540">PKZ_1964_SU0208.PKZ.123 (038.007)</ta>
            <ta e="T545" id="Seg_5073" s="T543">PKZ_1964_SU0208.PKZ.124 (038.008)</ta>
            <ta e="T547" id="Seg_5074" s="T545">PKZ_1964_SU0208.PKZ.125 (038.009)</ta>
            <ta e="T549" id="Seg_5075" s="T547">PKZ_1964_SU0208.PKZ.126 (038.010)</ta>
            <ta e="T553" id="Seg_5076" s="T549">PKZ_1964_SU0208.PKZ.127 (038.011)</ta>
            <ta e="T558" id="Seg_5077" s="T553">PKZ_1964_SU0208.PKZ.128 (038.012)</ta>
            <ta e="T561" id="Seg_5078" s="T558">PKZ_1964_SU0208.PKZ.129 (038.013)</ta>
            <ta e="T562" id="Seg_5079" s="T561">PKZ_1964_SU0208.PKZ.130 (038.014)</ta>
            <ta e="T566" id="Seg_5080" s="T562">PKZ_1964_SU0208.PKZ.131 (038.015)</ta>
            <ta e="T571" id="Seg_5081" s="T566">PKZ_1964_SU0208.PKZ.132 (038.016)</ta>
            <ta e="T574" id="Seg_5082" s="T571">PKZ_1964_SU0208.PKZ.133 (038.017)</ta>
            <ta e="T577" id="Seg_5083" s="T574">PKZ_1964_SU0208.PKZ.134 (038.018)</ta>
            <ta e="T579" id="Seg_5084" s="T577">PKZ_1964_SU0208.PKZ.135 (038.019)</ta>
            <ta e="T582" id="Seg_5085" s="T579">PKZ_1964_SU0208.PKZ.136 (038.020)</ta>
            <ta e="T587" id="Seg_5086" s="T582">PKZ_1964_SU0208.PKZ.137 (038.021)</ta>
            <ta e="T591" id="Seg_5087" s="T587">PKZ_1964_SU0208.PKZ.138 (038.022)</ta>
            <ta e="T596" id="Seg_5088" s="T591">PKZ_1964_SU0208.PKZ.139 (038.023)</ta>
            <ta e="T599" id="Seg_5089" s="T596">PKZ_1964_SU0208.PKZ.140 (038.024)</ta>
            <ta e="T609" id="Seg_5090" s="T599">PKZ_1964_SU0208.PKZ.141 (039)</ta>
            <ta e="T611" id="Seg_5091" s="T609">PKZ_1964_SU0208.PKZ.142 (040)</ta>
            <ta e="T616" id="Seg_5092" s="T611">PKZ_1964_SU0208.PKZ.143 (041)</ta>
            <ta e="T617" id="Seg_5093" s="T616">PKZ_1964_SU0208.PKZ.144 (042)</ta>
            <ta e="T621" id="Seg_5094" s="T617">PKZ_1964_SU0208.PKZ.145 (043)</ta>
            <ta e="T624" id="Seg_5095" s="T621">PKZ_1964_SU0208.PKZ.146 (044.001)</ta>
            <ta e="T629" id="Seg_5096" s="T624">PKZ_1964_SU0208.PKZ.147 (044.002)</ta>
            <ta e="T641" id="Seg_5097" s="T629">PKZ_1964_SU0208.PKZ.148 (045)</ta>
            <ta e="T643" id="Seg_5098" s="T641">PKZ_1964_SU0208.PKZ.149 (046)</ta>
            <ta e="T646" id="Seg_5099" s="T643">PKZ_1964_SU0208.PKZ.150 (047)</ta>
            <ta e="T649" id="Seg_5100" s="T646">PKZ_1964_SU0208.PKZ.151 (048.001)</ta>
            <ta e="T654" id="Seg_5101" s="T649">PKZ_1964_SU0208.PKZ.152 (048.002)</ta>
            <ta e="T656" id="Seg_5102" s="T654">PKZ_1964_SU0208.PKZ.153 (048.003)</ta>
            <ta e="T659" id="Seg_5103" s="T656">PKZ_1964_SU0208.PKZ.154 (048.004)</ta>
            <ta e="T663" id="Seg_5104" s="T659">PKZ_1964_SU0208.PKZ.155 (048.005)</ta>
            <ta e="T666" id="Seg_5105" s="T663">PKZ_1964_SU0208.PKZ.156 (048.006)</ta>
            <ta e="T674" id="Seg_5106" s="T666">PKZ_1964_SU0208.PKZ.157 (049)</ta>
            <ta e="T677" id="Seg_5107" s="T674">PKZ_1964_SU0208.PKZ.158 (050.001)</ta>
            <ta e="T680" id="Seg_5108" s="T677">PKZ_1964_SU0208.PKZ.159 (050.002)</ta>
            <ta e="T684" id="Seg_5109" s="T680">PKZ_1964_SU0208.PKZ.160 (051)</ta>
            <ta e="T686" id="Seg_5110" s="T684">PKZ_1964_SU0208.PKZ.161 (052)</ta>
            <ta e="T695" id="Seg_5111" s="T686">PKZ_1964_SU0208.PKZ.162 (053.001)</ta>
            <ta e="T701" id="Seg_5112" s="T695">PKZ_1964_SU0208.PKZ.163 (053.002)</ta>
            <ta e="T707" id="Seg_5113" s="T701">PKZ_1964_SU0208.PKZ.164 (054.001)</ta>
            <ta e="T710" id="Seg_5114" s="T707">PKZ_1964_SU0208.PKZ.165 (054.002)</ta>
            <ta e="T712" id="Seg_5115" s="T710">PKZ_1964_SU0208.PKZ.166 (055)</ta>
            <ta e="T718" id="Seg_5116" s="T712">PKZ_1964_SU0208.PKZ.167 (056)</ta>
            <ta e="T724" id="Seg_5117" s="T718">PKZ_1964_SU0208.PKZ.168 (057)</ta>
            <ta e="T729" id="Seg_5118" s="T724">PKZ_1964_SU0208.PKZ.169 (058)</ta>
            <ta e="T733" id="Seg_5119" s="T729">PKZ_1964_SU0208.PKZ.170 (059.001)</ta>
            <ta e="T737" id="Seg_5120" s="T733">PKZ_1964_SU0208.PKZ.171 (059.002)</ta>
            <ta e="T741" id="Seg_5121" s="T737">PKZ_1964_SU0208.PKZ.172 (059.003)</ta>
            <ta e="T745" id="Seg_5122" s="T741">PKZ_1964_SU0208.PKZ.173 (059.004)</ta>
            <ta e="T750" id="Seg_5123" s="T745">PKZ_1964_SU0208.PKZ.174 (059.005)</ta>
            <ta e="T757" id="Seg_5124" s="T751">PKZ_1964_SU0208.PKZ.175 (061)</ta>
            <ta e="T761" id="Seg_5125" s="T757">PKZ_1964_SU0208.PKZ.176 (062.001)</ta>
            <ta e="T765" id="Seg_5126" s="T761">PKZ_1964_SU0208.PKZ.177 (062.002)</ta>
            <ta e="T767" id="Seg_5127" s="T765">PKZ_1964_SU0208.PKZ.178 (062.003)</ta>
            <ta e="T769" id="Seg_5128" s="T767">PKZ_1964_SU0208.PKZ.179 (062.004)</ta>
            <ta e="T775" id="Seg_5129" s="T769">PKZ_1964_SU0208.PKZ.180 (063.001)</ta>
            <ta e="T779" id="Seg_5130" s="T775">PKZ_1964_SU0208.PKZ.181 (063.002)</ta>
            <ta e="T781" id="Seg_5131" s="T779">PKZ_1964_SU0208.PKZ.182 (063.003)</ta>
            <ta e="T786" id="Seg_5132" s="T781">PKZ_1964_SU0208.PKZ.183 (064)</ta>
            <ta e="T788" id="Seg_5133" s="T786">PKZ_1964_SU0208.PKZ.184 (065.001)</ta>
            <ta e="T790" id="Seg_5134" s="T788">PKZ_1964_SU0208.PKZ.185 (065.002)</ta>
            <ta e="T797" id="Seg_5135" s="T790">PKZ_1964_SU0208.PKZ.186 (065.003)</ta>
            <ta e="T802" id="Seg_5136" s="T797">PKZ_1964_SU0208.PKZ.187 (066)</ta>
            <ta e="T805" id="Seg_5137" s="T802">PKZ_1964_SU0208.PKZ.188 (067)</ta>
            <ta e="T806" id="Seg_5138" s="T805">PKZ_1964_SU0208.PKZ.189 (068)</ta>
            <ta e="T810" id="Seg_5139" s="T806">PKZ_1964_SU0208.PKZ.190 (069)</ta>
            <ta e="T816" id="Seg_5140" s="T810">PKZ_1964_SU0208.PKZ.191 (070)</ta>
            <ta e="T819" id="Seg_5141" s="T816">PKZ_1964_SU0208.PKZ.192 (071)</ta>
            <ta e="T823" id="Seg_5142" s="T819">PKZ_1964_SU0208.PKZ.193 (072)</ta>
            <ta e="T828" id="Seg_5143" s="T823">PKZ_1964_SU0208.PKZ.194 (073)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T12" id="Seg_5144" s="T4">Sʼimat ej kuvas, onʼiʔ döbər mandolaʔbə, onʼiʔ dibər. </ta>
            <ta e="T14" id="Seg_5145" s="T12">((DMG)) Sʼimat ((…)).</ta>
            <ta e="T20" id="Seg_5146" s="T14">Măn aŋbə bar kajlaʔbə, kunolzittə axota. </ta>
            <ta e="T25" id="Seg_5147" s="T20">Miʔ šidegöʔ (kambivaʔ-) kambibaʔ aktʼinə. </ta>
            <ta e="T32" id="Seg_5148" s="T25">Dĭ (ka-) kambi nalʼeva, măn kambiam naprava. </ta>
            <ta e="T38" id="Seg_5149" s="T32">Kăda dĭm kăštəliaʔi, măn ej tĭmnem. </ta>
            <ta e="T43" id="Seg_5150" s="T38">Čolaʔi bar nʼamga tʼerə detleʔi. </ta>
            <ta e="T47" id="Seg_5151" s="T43">I takšənə (en-) endləʔbəʔjə. </ta>
            <ta e="T50" id="Seg_5152" s="T47">Ugaːndə tăŋ togonorlaʔbəʔjə. </ta>
            <ta e="T58" id="Seg_5153" s="T50">Dʼijegən ((…)) Dʼijegən ei amnolaʔbəʔjə, bügən ei amnolaʔbə. </ta>
            <ta e="T61" id="Seg_5154" s="T58">Ugaːndə urgo măja. </ta>
            <ta e="T64" id="Seg_5155" s="T61">Kăda dĭʔnə kanzittə? </ta>
            <ta e="T66" id="Seg_5156" s="T64">Ej tĭmnem. </ta>
            <ta e="T74" id="Seg_5157" s="T66">Măndlial dăk, bar (užu- uzu- saʔmə=) üžü saʔməluʔləj. </ta>
            <ta e="T77" id="Seg_5158" s="T74">Kambiʔi dʼü tĭlzittə. </ta>
            <ta e="T80" id="Seg_5159" s="T77">(A-) Aʔtʼi … </ta>
            <ta e="T84" id="Seg_5160" s="T80">(A-) ((DMG)) Kambiʔi dʼü tĭlzittə. </ta>
            <ta e="T86" id="Seg_5161" s="T84">Aʔtʼi tʼazirzittə. </ta>
            <ta e="T91" id="Seg_5162" s="T86">((DMG)) Gibər (an-) dĭzeŋ kambiʔi, kuŋgəŋ? </ta>
            <ta e="T94" id="Seg_5163" s="T91">Dʼok, ej kuŋgə. </ta>
            <ta e="T96" id="Seg_5164" s="T94">Šobiʔi inezʼiʔi. </ta>
            <ta e="T100" id="Seg_5165" s="T96">Ej kuŋgəŋ turagən nubiʔi. </ta>
            <ta e="T105" id="Seg_5166" s="T100">Daže ej surarbiʔi, bar nubiʔi. </ta>
            <ta e="T112" id="Seg_5167" s="T105">Kanaʔ ((DMG)) (măn turagəʔ i dön) iʔ nuʔ.</ta>
            <ta e="T114" id="Seg_5168" s="T112">Ej kambiʔi. </ta>
            <ta e="T116" id="Seg_5169" s="T114">Turanə saʔluʔpiʔi. </ta>
            <ta e="T119" id="Seg_5170" s="T116">I alomniaʔi bar. </ta>
            <ta e="T123" id="Seg_5171" s="T119">Turanə nʼuʔdə bar suʔmiluʔpiʔi. </ta>
            <ta e="T126" id="Seg_5172" s="T123">(Sagən nüjüʔi) naga. </ta>
            <ta e="T132" id="Seg_5173" s="T126">Gibərdə kalla dʼürbiʔi alʼi kubiʔi bar. </ta>
            <ta e="T136" id="Seg_5174" s="T132">Măna tože pizʼiʔ münörleʔbəlaʔ. </ta>
            <ta e="T142" id="Seg_5175" s="T136">Măna tože pi (ellə- el=) elləl. </ta>
            <ta e="T146" id="Seg_5176" s="T142">Il jakšə, ej šamniabaʔ. </ta>
            <ta e="T149" id="Seg_5177" s="T146">Jakšə (dʼăbaktərlabəʔ-) dʼăbaktərlaʔbəbaʔ. </ta>
            <ta e="T153" id="Seg_5178" s="T149">Šənap dʼăbaktərlaʔbəm, ej šamniam. </ta>
            <ta e="T161" id="Seg_5179" s="T153">Komu červəʔi ((DMG)) (oʔbdəbiam, büzʼiʔ) (dĭm) (mazə-) bəzəbiam. </ta>
            <ta e="T163" id="Seg_5180" s="T161">Takšənə embiem. </ta>
            <ta e="T167" id="Seg_5181" s="T163">Dĭbər (ejü=) ejü nuldəbiam. </ta>
            <ta e="T170" id="Seg_5182" s="T167">Dibər bü molaːmbi. </ta>
            <ta e="T173" id="Seg_5183" s="T170">Dĭgəttə udam kĭškəbiem. </ta>
            <ta e="T180" id="Seg_5184" s="T173">Udam ugaːndə tăŋ ĭzembiʔi, tüj ej ĭzemnieʔi. </ta>
            <ta e="T183" id="Seg_5185" s="T180">Smatri, kudaj … </ta>
            <ta e="T187" id="Seg_5186" s="T183">Smatri, kudaj iʔ nöməlaʔ. </ta>
            <ta e="T191" id="Seg_5187" s="T187">Погоди, как (это)… </ta>
            <ta e="T193" id="Seg_5188" s="T191">((DMG)) Kanaʔ eneidənenə! </ta>
            <ta e="T196" id="Seg_5189" s="T193">Eneidənen (amnu-) amnut! </ta>
            <ta e="T198" id="Seg_5190" s="T196">Tüžöjən amnut. </ta>
            <ta e="T203" id="Seg_5191" s="T198">Dĭ bar tănan (muʔ-) muʔləj. </ta>
            <ta e="T210" id="Seg_5192" s="T203">Sʼenʼiʔi (š-) săbərəjdə, a to ugaːndə iʔgö balgaš. </ta>
            <ta e="T212" id="Seg_5193" s="T210">Krilso săbərəjdə! </ta>
            <ta e="T216" id="Seg_5194" s="T212">Anbardə (unə ilem), embiem. </ta>
            <ta e="T222" id="Seg_5195" s="T216">Măn dĭm kuʔpiom, măn šaldə iʔgö. </ta>
            <ta e="T226" id="Seg_5196" s="T222">Măn ugaːndə kuštu igem. </ta>
            <ta e="T231" id="Seg_5197" s="T226">Dĭ kuza bar sagəšdə naga. </ta>
            <ta e="T236" id="Seg_5198" s="T231">Šĭket ige, dʼăbaktərzittə ej molia. </ta>
            <ta e="T240" id="Seg_5199" s="T236">Kut ige, ej nünnie. </ta>
            <ta e="T242" id="Seg_5200" s="T240">Iʔ parloʔ! </ta>
            <ta e="T244" id="Seg_5201" s="T242">Jakšə amnoʔ! </ta>
            <ta e="T246" id="Seg_5202" s="T244">Ĭmbi parloraʔbəl? </ta>
            <ta e="T251" id="Seg_5203" s="T246">Bostə šĭkem nünniöm, jakšə dʼăbaktərliam. </ta>
            <ta e="T256" id="Seg_5204" s="T251">Šoška šonəga i kuremnaʔbə bar. </ta>
            <ta e="T263" id="Seg_5205" s="T256">Nada dĭm bădəsʼtə, a to dĭ püjölia bar. </ta>
            <ta e="T273" id="Seg_5206" s="T263">Nada (poʔ-) baltu puʔməsʼtə, paʔi ((PAUSE)) (ja- jaʔ- jaʔi-) jaʔsʼittə. </ta>
            <ta e="T275" id="Seg_5207" s="T273">Pʼeš nenzittə. </ta>
            <ta e="T282" id="Seg_5208" s="T275">Pʼila deppiʔi, ej tĭmneʔi, ĭmbi dĭzʼiʔ azittə. </ta>
            <ta e="T286" id="Seg_5209" s="T282">(Arla-) Varlam šoləj, tʼazirləj. </ta>
            <ta e="T289" id="Seg_5210" s="T286">Bazaj pʼeš deppiʔi. </ta>
            <ta e="T291" id="Seg_5211" s="T289">Turanə nuldəbiʔi. </ta>
            <ta e="T294" id="Seg_5212" s="T291">(Šoj- šoj-) Šojdʼonə. </ta>
            <ta e="T299" id="Seg_5213" s="T294">(Pasə) šojdʼo, dĭ bar nendluʔpi. </ta>
            <ta e="T301" id="Seg_5214" s="T299">Bər šobi. </ta>
            <ta e="T305" id="Seg_5215" s="T301">Dĭgəttə bar əʔpiʔi nʼiʔdə. </ta>
            <ta e="T308" id="Seg_5216" s="T305">Ugaːndə bər iʔgö. </ta>
            <ta e="T312" id="Seg_5217" s="T308">Varlam šoləj i dʼazirləj. </ta>
            <ta e="T316" id="Seg_5218" s="T312">Bar bər sʼimam amnalaʔbəʔjə. </ta>
            <ta e="T321" id="Seg_5219" s="T316">Dĭgəttə Varlam šobi, pi deʔpi. </ta>
            <ta e="T323" id="Seg_5220" s="T321">Dʼünə embi. </ta>
            <ta e="T326" id="Seg_5221" s="T323">I pʼešdə nuldəbi. </ta>
            <ta e="T333" id="Seg_5222" s="T326">Dĭgəttə pʼeš nendəbiʔi, amnolaʔbəʔjə i (ejü-) ejüleʔbəʔjə. </ta>
            <ta e="T337" id="Seg_5223" s="T333">((DMG)) чтобы не ошибиться. </ta>
            <ta e="T343" id="Seg_5224" s="T338">Părgam bar (dʼügən=) dʼügən iʔböbi. </ta>
            <ta e="T346" id="Seg_5225" s="T343">Üjüʔi bar kĭškəbiʔi. </ta>
            <ta e="T361" id="Seg_5226" s="T346">(Dĭn=) Dĭn bar balgaš, măn dĭm (ibiel=) ibiem da, bünə (kumbial=) kumbiam, štobɨ dʼüʔpi mobi. </ta>
            <ta e="T363" id="Seg_5227" s="T361">Dĭgəttə toʔnarbiam. </ta>
            <ta e="T364" id="Seg_5228" s="T363">Bəzəbiam. </ta>
            <ta e="T366" id="Seg_5229" s="T364">Dĭgəttə edəbiem. </ta>
            <ta e="T370" id="Seg_5230" s="T366">Bü bar mʼaŋŋaʔbə dĭgəʔ. </ta>
            <ta e="T372" id="Seg_5231" s="T370">Dʼaga mʼaŋŋaʔbə. </ta>
            <ta e="T376" id="Seg_5232" s="T372">I kuza nuʔməleʔbə büjleʔ. </ta>
            <ta e="T379" id="Seg_5233" s="T376">Girgitdə il mĭlleʔbəʔjə. </ta>
            <ta e="T384" id="Seg_5234" s="T379">Măn (dĭm=) dĭzem ej tĭmnem. </ta>
            <ta e="T389" id="Seg_5235" s="T384">Ĭmbi (dĭ-) dĭzeŋ dĭn mĭlleʔbəʔjə. </ta>
            <ta e="T395" id="Seg_5236" s="T389">Da dĭ nʼi da koʔbdo šonəgaʔi. </ta>
            <ta e="T399" id="Seg_5237" s="T395">((DMG)) Nada sut kămnasʼtə šojdʼonə. </ta>
            <ta e="T402" id="Seg_5238" s="T399">Puskaj namzəga moləj. </ta>
            <ta e="T406" id="Seg_5239" s="T402">Tüjö kalla (dʼürbi-) dʼürbibeʔ. </ta>
            <ta e="T410" id="Seg_5240" s="T406">No, (kanaʔ=) kangaʔ kudajzʼiʔ. </ta>
            <ta e="T414" id="Seg_5241" s="T410">Miʔ ugaːndə iʔgö dʼăbaktərbibaʔ. </ta>
            <ta e="T418" id="Seg_5242" s="T414">Tüj kanzittə nada maːndə. </ta>
            <ta e="T421" id="Seg_5243" s="T418">Da kunolzittə iʔbəsʼtə. </ta>
            <ta e="T425" id="Seg_5244" s="T421">(Iʔ maktanarəbaʔ-) Iʔ maktanarəʔ. </ta>
            <ta e="T431" id="Seg_5245" s="T425">Ĭmbidə ej ibial, a (bar) maktanarbial. </ta>
            <ta e="T437" id="Seg_5246" s="T431">Kunolzittə iʔbəsʼtə, a karəldʼan (šagəštə) šoləj. </ta>
            <ta e="T444" id="Seg_5247" s="T437">Kuznektə kuza šobi, a măn (pi) (kabarluʔpiam). </ta>
            <ta e="T448" id="Seg_5248" s="T446">Запри ((DMG)). </ta>
            <ta e="T454" id="Seg_5249" s="T448">Pi kabarluʔpiom, i kuʔpiom dĭ kuzam. </ta>
            <ta e="T462" id="Seg_5250" s="T454">Kuza kuzam bar dagajzʼiʔ dʼagarluʔpi i dĭ külaːmbi. </ta>
            <ta e="T475" id="Seg_5251" s="T462">Dĭn onʼiʔ ne, Kazan turagən onʼiʔ ne tibibə bar jaʔpi baltuzʼiʔ, ulut sajjaʔpi. </ta>
            <ta e="T480" id="Seg_5252" s="T475">Dĭn tibi onʼiʔ kunolluʔpi bar. </ta>
            <ta e="T483" id="Seg_5253" s="T480">Ugaːndə (pimn-) pimbiem. </ta>
            <ta e="T488" id="Seg_5254" s="T483">Kăda măna ulum ej jaʔpi. </ta>
            <ta e="T496" id="Seg_5255" s="T488">Dĭ bar nanəʔzəbi ibi i šide esseŋ maluʔpiʔi. </ta>
            <ta e="T504" id="Seg_5256" s="T496">(Uždə- užə- u-) Bar sumna pʼe kalla dʼürbi. </ta>
            <ta e="T507" id="Seg_5257" s="T504">Taldʼen ej ibiel. </ta>
            <ta e="T512" id="Seg_5258" s="T507">Teinen (šol -) šobial nʼilgösʼtə. </ta>
            <ta e="T514" id="Seg_5259" s="T512">Ĭmbi dʼăbaktərlaʔbəbaʔ? </ta>
            <ta e="T521" id="Seg_5260" s="T514">Măn teinen kudaj (numanə) (üzə- üz-) üzləm. </ta>
            <ta e="T524" id="Seg_5261" s="T521">Kudaj, šoʔ măna! </ta>
            <ta e="T528" id="Seg_5262" s="T524">Nʼilgöt, kăda măn üzliem. </ta>
            <ta e="T531" id="Seg_5263" s="T528">Sĭjbə (măndərdə-) măndəʔ. </ta>
            <ta e="T534" id="Seg_5264" s="T531">(Padʼi) (b-) arəliaʔ! </ta>
            <ta e="T540" id="Seg_5265" s="T534">A ej arə dăk, bəzit kemzʼiʔ! </ta>
            <ta e="T543" id="Seg_5266" s="T540">Tăn pagən edəbiel. </ta>
            <ta e="T545" id="Seg_5267" s="T543">Kembə mʼaŋnaʔpi. </ta>
            <ta e="T547" id="Seg_5268" s="T545">Bəzit măna! </ta>
            <ta e="T549" id="Seg_5269" s="T547">Jakšə bəzit! </ta>
            <ta e="T553" id="Seg_5270" s="T549">Tăn, kudaj, šaldə iʔgö. </ta>
            <ta e="T558" id="Seg_5271" s="T553">Bar (i-) iləm (tʼa-) ajirlaʔbəl. </ta>
            <ta e="T561" id="Seg_5272" s="T558">(M-) Ipek mĭliel. </ta>
            <ta e="T562" id="Seg_5273" s="T561">(Kuj-). </ta>
            <ta e="T566" id="Seg_5274" s="T562">Tăn kudaj šaldə iʔgö. </ta>
            <ta e="T571" id="Seg_5275" s="T566">Tăn (il= il=) il ajirial. </ta>
            <ta e="T574" id="Seg_5276" s="T571">Ipek dĭzeŋdə mĭliel. </ta>
            <ta e="T577" id="Seg_5277" s="T574">Bar kujnek mĭliel. </ta>
            <ta e="T579" id="Seg_5278" s="T577">Bü mĭliel. </ta>
            <ta e="T582" id="Seg_5279" s="T579">I măna ajiraʔ! </ta>
            <ta e="T587" id="Seg_5280" s="T582">Чего-то (никак) … </ta>
            <ta e="T591" id="Seg_5281" s="T587">Tăn bar ejü öʔliel. </ta>
            <ta e="T596" id="Seg_5282" s="T591">Surno mĭliel, štobɨ ipek özerleʔpi. </ta>
            <ta e="T599" id="Seg_5283" s="T596">Чтобы хлеб (рос/рожь)… ((BRK)). </ta>
            <ta e="T609" id="Seg_5284" s="T599">Kudajzʼiʔ jakšə, gibər kallal, kudajzʼiʔ (vez- vez-); ugaːndə jakšə kudajzʼiʔ. </ta>
            <ta e="T611" id="Seg_5285" s="T609">Beržə saʔməluʔpi. </ta>
            <ta e="T616" id="Seg_5286" s="T611">Măn ibiem da turanə deʔpiem. </ta>
            <ta e="T617" id="Seg_5287" s="T616">Jakšə. </ta>
            <ta e="T621" id="Seg_5288" s="T617">Surno šonəga, ipek özerləj. </ta>
            <ta e="T624" id="Seg_5289" s="T621">Măn dĭʔnə kabažarbiam. </ta>
            <ta e="T629" id="Seg_5290" s="T624">A dĭ măna ej kabažarlaʔbə. </ta>
            <ta e="T641" id="Seg_5291" s="T629">Ej mĭlie măna togonorzittə, măn bɨ togonorbiam, a dĭ üge (puʔlaʔbə) măna. </ta>
            <ta e="T643" id="Seg_5292" s="T641">Kurojək bar. </ta>
            <ta e="T646" id="Seg_5293" s="T643">Ujuzʼiʔ bar tonuʔpi. </ta>
            <ta e="T649" id="Seg_5294" s="T646">Ugaːndə kurojək men. </ta>
            <ta e="T654" id="Seg_5295" s="T649">(Bal-) Bar (tal-) talbəlia il. </ta>
            <ta e="T656" id="Seg_5296" s="T654">Людей кусает. </ta>
            <ta e="T659" id="Seg_5297" s="T656">((DMG)) Ugaːndə kurojək men. </ta>
            <ta e="T663" id="Seg_5298" s="T659">(Ba-) Bar il talbərlaʔbə. </ta>
            <ta e="T666" id="Seg_5299" s="T663">I măna talbərbi. </ta>
            <ta e="T674" id="Seg_5300" s="T666">Măn šiʔ edəʔpiem, šiʔ ej šobilaʔ, kundʼo nagobilaʔ. </ta>
            <ta e="T677" id="Seg_5301" s="T674">Takšəgəʔ ibiem, bəzəbiam. </ta>
            <ta e="T680" id="Seg_5302" s="T677">I bazoʔ embiem. </ta>
            <ta e="T684" id="Seg_5303" s="T680">Teinöbam, teinöbam teinen bar. </ta>
            <ta e="T686" id="Seg_5304" s="T684">((DMG)) никак. </ta>
            <ta e="T695" id="Seg_5305" s="T686">Bünə ej saʔməlial, nʼuʔdun bar mĭŋgə udazʼiʔ i üjüzʼiʔ. </ta>
            <ta e="T701" id="Seg_5306" s="T695">Bügən bar kandəgal udazʼiʔ i üjüzʼiʔ. </ta>
            <ta e="T707" id="Seg_5307" s="T701">(Lotka=) Lotkaizʼiʔ bar (bün-) büʔnə kandəga. </ta>
            <ta e="T710" id="Seg_5308" s="T707">Kola dʼabəlaʔbə bar. </ta>
            <ta e="T712" id="Seg_5309" s="T710">Nagurgöʔ (tʼakamnəbaʔ). </ta>
            <ta e="T718" id="Seg_5310" s="T712">Onʼiʔ tănan, onʼiʔ măna, onʼiʔ dĭʔnə. </ta>
            <ta e="T724" id="Seg_5311" s="T718">Šide pʼel, nagur pʼel, teʔtə pʼel. </ta>
            <ta e="T729" id="Seg_5312" s="T724">Dăra (dĭ=) dĭzeŋ bar amnobiʔi. </ta>
            <ta e="T733" id="Seg_5313" s="T729">Onʼiʔ inegən măn amnobiam. </ta>
            <ta e="T737" id="Seg_5314" s="T733">Šide inegən nem amnəbi. </ta>
            <ta e="T741" id="Seg_5315" s="T737">Nagur inegən nʼim amnəbi. </ta>
            <ta e="T745" id="Seg_5316" s="T741">Teʔtə inegən koʔbdom amnəbi. </ta>
            <ta e="T750" id="Seg_5317" s="T745">A sumna inegən urgoja amnəbi. </ta>
            <ta e="T757" id="Seg_5318" s="T751">Dĭ nʼi šobi miʔnʼibeʔ kürzittə bar. </ta>
            <ta e="T761" id="Seg_5319" s="T757">Büžü bar šĭšəge moləj. </ta>
            <ta e="T765" id="Seg_5320" s="T761">(Sĭ-) Sĭre bar saʔmələj. </ta>
            <ta e="T767" id="Seg_5321" s="T765">Ugaːndə šĭšəge. </ta>
            <ta e="T769" id="Seg_5322" s="T767">Kănnaləl bar. </ta>
            <ta e="T775" id="Seg_5323" s="T769">Büžü (bəj- ar-) bar ejü moləj. </ta>
            <ta e="T779" id="Seg_5324" s="T775">Dĭgəttə il (tarirluʔpiʔi) bar. </ta>
            <ta e="T781" id="Seg_5325" s="T779">Budəj kuʔluʔjəʔ. </ta>
            <ta e="T786" id="Seg_5326" s="T781">Ugaːndə kuŋgem nʼergölaʔbə (nʼiʔn-) nʼuʔnə. </ta>
            <ta e="T788" id="Seg_5327" s="T786">Ej idlia. </ta>
            <ta e="T790" id="Seg_5328" s="T788">I … </ta>
            <ta e="T797" id="Seg_5329" s="T790">Inegəʔ (inezʼ- inezeŋ-) inegəʔ bar dʼünə saʔməluʔpi. </ta>
            <ta e="T802" id="Seg_5330" s="T797">Nʼuʔdə bar nada kanzittə ugaːndə. </ta>
            <ta e="T805" id="Seg_5331" s="T802">Больше не знаю. </ta>
            <ta e="T806" id="Seg_5332" s="T805">((DMG)). </ta>
            <ta e="T810" id="Seg_5333" s="T806">Ugaːndə (kudaj) nʼuʔdə amnolaʔbə. </ta>
            <ta e="T816" id="Seg_5334" s="T810">Măjagən, (uʔ- uj-) urgo (uj-) măja. </ta>
            <ta e="T819" id="Seg_5335" s="T816">Ulum bar (tʼukumnie). </ta>
            <ta e="T823" id="Seg_5336" s="T819">Kădaʔliom, naverna unu iʔgö. </ta>
            <ta e="T828" id="Seg_5337" s="T823">Nada dĭzeŋ bar kutzʼittə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_5338" s="T4">sʼima-t</ta>
            <ta e="T6" id="Seg_5339" s="T5">ej</ta>
            <ta e="T7" id="Seg_5340" s="T6">kuvas</ta>
            <ta e="T8" id="Seg_5341" s="T7">onʼiʔ</ta>
            <ta e="T9" id="Seg_5342" s="T8">döbər</ta>
            <ta e="T10" id="Seg_5343" s="T9">mando-laʔbə</ta>
            <ta e="T11" id="Seg_5344" s="T10">onʼiʔ</ta>
            <ta e="T12" id="Seg_5345" s="T11">dibər</ta>
            <ta e="T13" id="Seg_5346" s="T12">sʼima-t</ta>
            <ta e="T15" id="Seg_5347" s="T14">măn</ta>
            <ta e="T16" id="Seg_5348" s="T15">aŋ-bə</ta>
            <ta e="T17" id="Seg_5349" s="T16">bar</ta>
            <ta e="T18" id="Seg_5350" s="T17">kaj-laʔbə</ta>
            <ta e="T19" id="Seg_5351" s="T18">kunol-zittə</ta>
            <ta e="T20" id="Seg_5352" s="T19">axota</ta>
            <ta e="T21" id="Seg_5353" s="T20">miʔ</ta>
            <ta e="T22" id="Seg_5354" s="T21">šide-göʔ</ta>
            <ta e="T24" id="Seg_5355" s="T23">kam-bi-baʔ</ta>
            <ta e="T25" id="Seg_5356" s="T24">aktʼi-nə</ta>
            <ta e="T26" id="Seg_5357" s="T25">dĭ</ta>
            <ta e="T28" id="Seg_5358" s="T27">kam-bi</ta>
            <ta e="T29" id="Seg_5359" s="T28">nalʼeva</ta>
            <ta e="T30" id="Seg_5360" s="T29">măn</ta>
            <ta e="T31" id="Seg_5361" s="T30">kam-bia-m</ta>
            <ta e="T32" id="Seg_5362" s="T31">naprava</ta>
            <ta e="T33" id="Seg_5363" s="T32">kăda</ta>
            <ta e="T34" id="Seg_5364" s="T33">dĭ-m</ta>
            <ta e="T35" id="Seg_5365" s="T34">kăštə-lia-ʔi</ta>
            <ta e="T36" id="Seg_5366" s="T35">măn</ta>
            <ta e="T37" id="Seg_5367" s="T36">ej</ta>
            <ta e="T38" id="Seg_5368" s="T37">tĭmne-m</ta>
            <ta e="T39" id="Seg_5369" s="T38">čola-ʔi</ta>
            <ta e="T40" id="Seg_5370" s="T39">bar</ta>
            <ta e="T41" id="Seg_5371" s="T40">nʼamga</ta>
            <ta e="T42" id="Seg_5372" s="T41">tʼerə</ta>
            <ta e="T43" id="Seg_5373" s="T42">det-le-ʔi</ta>
            <ta e="T44" id="Seg_5374" s="T43">i</ta>
            <ta e="T45" id="Seg_5375" s="T44">takšə-nə</ta>
            <ta e="T47" id="Seg_5376" s="T46">end-ləʔbə-ʔjə</ta>
            <ta e="T48" id="Seg_5377" s="T47">ugaːndə</ta>
            <ta e="T49" id="Seg_5378" s="T48">tăŋ</ta>
            <ta e="T50" id="Seg_5379" s="T49">togonor-laʔbə-ʔjə</ta>
            <ta e="T51" id="Seg_5380" s="T50">dʼije-gən</ta>
            <ta e="T53" id="Seg_5381" s="T52">dʼije-gən</ta>
            <ta e="T54" id="Seg_5382" s="T53">ej</ta>
            <ta e="T55" id="Seg_5383" s="T54">amno-laʔbə-ʔjə</ta>
            <ta e="T56" id="Seg_5384" s="T55">bü-gən</ta>
            <ta e="T57" id="Seg_5385" s="T56">ej</ta>
            <ta e="T58" id="Seg_5386" s="T57">amno-laʔbə</ta>
            <ta e="T59" id="Seg_5387" s="T58">ugaːndə</ta>
            <ta e="T60" id="Seg_5388" s="T59">urgo</ta>
            <ta e="T61" id="Seg_5389" s="T60">măja</ta>
            <ta e="T62" id="Seg_5390" s="T61">kăda</ta>
            <ta e="T63" id="Seg_5391" s="T62">dĭʔ-nə</ta>
            <ta e="T64" id="Seg_5392" s="T63">kan-zittə</ta>
            <ta e="T65" id="Seg_5393" s="T64">ej</ta>
            <ta e="T66" id="Seg_5394" s="T65">tĭmne-m</ta>
            <ta e="T67" id="Seg_5395" s="T66">mănd-lia-l</ta>
            <ta e="T68" id="Seg_5396" s="T67">dăk</ta>
            <ta e="T69" id="Seg_5397" s="T68">bar</ta>
            <ta e="T72" id="Seg_5398" s="T71">saʔmə</ta>
            <ta e="T73" id="Seg_5399" s="T72">üžü</ta>
            <ta e="T74" id="Seg_5400" s="T73">saʔmə-luʔ-lə-j</ta>
            <ta e="T75" id="Seg_5401" s="T74">kam-bi-ʔi</ta>
            <ta e="T76" id="Seg_5402" s="T75">dʼü</ta>
            <ta e="T77" id="Seg_5403" s="T76">tĭl-zittə</ta>
            <ta e="T80" id="Seg_5404" s="T78">aʔtʼi</ta>
            <ta e="T82" id="Seg_5405" s="T81">kam-bi-ʔi</ta>
            <ta e="T83" id="Seg_5406" s="T82">dʼü</ta>
            <ta e="T84" id="Seg_5407" s="T83">tĭl-zittə</ta>
            <ta e="T85" id="Seg_5408" s="T84">aʔtʼi</ta>
            <ta e="T86" id="Seg_5409" s="T85">tʼazir-zittə</ta>
            <ta e="T87" id="Seg_5410" s="T86">gibər</ta>
            <ta e="T89" id="Seg_5411" s="T88">dĭ-zeŋ</ta>
            <ta e="T90" id="Seg_5412" s="T89">kam-bi-ʔi</ta>
            <ta e="T91" id="Seg_5413" s="T90">kuŋgə-ŋ</ta>
            <ta e="T92" id="Seg_5414" s="T91">dʼok</ta>
            <ta e="T93" id="Seg_5415" s="T92">ej</ta>
            <ta e="T94" id="Seg_5416" s="T93">kuŋgə</ta>
            <ta e="T95" id="Seg_5417" s="T94">šo-bi-ʔi</ta>
            <ta e="T96" id="Seg_5418" s="T95">ine-zʼi-ʔi</ta>
            <ta e="T97" id="Seg_5419" s="T96">ej</ta>
            <ta e="T98" id="Seg_5420" s="T97">kuŋgə-ŋ</ta>
            <ta e="T99" id="Seg_5421" s="T98">tura-gən</ta>
            <ta e="T100" id="Seg_5422" s="T99">nu-bi-ʔi</ta>
            <ta e="T101" id="Seg_5423" s="T100">daže</ta>
            <ta e="T102" id="Seg_5424" s="T101">ej</ta>
            <ta e="T103" id="Seg_5425" s="T102">surar-bi-ʔi</ta>
            <ta e="T104" id="Seg_5426" s="T103">bar</ta>
            <ta e="T105" id="Seg_5427" s="T104">nu-bi-ʔi</ta>
            <ta e="T106" id="Seg_5428" s="T105">kan-a-ʔ</ta>
            <ta e="T107" id="Seg_5429" s="T106">măn</ta>
            <ta e="T108" id="Seg_5430" s="T107">tura-gəʔ</ta>
            <ta e="T109" id="Seg_5431" s="T108">i</ta>
            <ta e="T110" id="Seg_5432" s="T109">dön</ta>
            <ta e="T111" id="Seg_5433" s="T110">i-ʔ</ta>
            <ta e="T112" id="Seg_5434" s="T111">nu-ʔ</ta>
            <ta e="T113" id="Seg_5435" s="T112">ej</ta>
            <ta e="T114" id="Seg_5436" s="T113">kam-bi-ʔi</ta>
            <ta e="T115" id="Seg_5437" s="T114">tura-nə</ta>
            <ta e="T116" id="Seg_5438" s="T115">saʔ-luʔ-pi-ʔi</ta>
            <ta e="T117" id="Seg_5439" s="T116">i</ta>
            <ta e="T118" id="Seg_5440" s="T117">alom-nia-ʔi</ta>
            <ta e="T119" id="Seg_5441" s="T118">bar</ta>
            <ta e="T120" id="Seg_5442" s="T119">tura-nə</ta>
            <ta e="T121" id="Seg_5443" s="T120">nʼuʔdə</ta>
            <ta e="T122" id="Seg_5444" s="T121">bar</ta>
            <ta e="T123" id="Seg_5445" s="T122">suʔmi-luʔ-pi-ʔi</ta>
            <ta e="T124" id="Seg_5446" s="T123">sagən</ta>
            <ta e="T125" id="Seg_5447" s="T124">nüjü-ʔi</ta>
            <ta e="T126" id="Seg_5448" s="T125">naga</ta>
            <ta e="T127" id="Seg_5449" s="T126">gibər=də</ta>
            <ta e="T128" id="Seg_5450" s="T127">kal-la</ta>
            <ta e="T129" id="Seg_5451" s="T128">dʼür-bi-ʔi</ta>
            <ta e="T130" id="Seg_5452" s="T129">alʼi</ta>
            <ta e="T131" id="Seg_5453" s="T130">ku-bi-ʔi</ta>
            <ta e="T132" id="Seg_5454" s="T131">bar</ta>
            <ta e="T133" id="Seg_5455" s="T132">măna</ta>
            <ta e="T134" id="Seg_5456" s="T133">tože</ta>
            <ta e="T135" id="Seg_5457" s="T134">pi-zʼiʔ</ta>
            <ta e="T136" id="Seg_5458" s="T135">münör-leʔbə-laʔ</ta>
            <ta e="T137" id="Seg_5459" s="T136">măna</ta>
            <ta e="T138" id="Seg_5460" s="T137">tože</ta>
            <ta e="T139" id="Seg_5461" s="T138">pi</ta>
            <ta e="T141" id="Seg_5462" s="T140">el</ta>
            <ta e="T142" id="Seg_5463" s="T141">el-lə-l</ta>
            <ta e="T143" id="Seg_5464" s="T142">il</ta>
            <ta e="T144" id="Seg_5465" s="T143">jakšə</ta>
            <ta e="T145" id="Seg_5466" s="T144">ej</ta>
            <ta e="T146" id="Seg_5467" s="T145">šam-nia-baʔ</ta>
            <ta e="T147" id="Seg_5468" s="T146">jakšə</ta>
            <ta e="T149" id="Seg_5469" s="T148">dʼăbaktər-laʔbə-baʔ</ta>
            <ta e="T150" id="Seg_5470" s="T149">šənap</ta>
            <ta e="T151" id="Seg_5471" s="T150">dʼăbaktər-laʔbə-m</ta>
            <ta e="T152" id="Seg_5472" s="T151">ej</ta>
            <ta e="T153" id="Seg_5473" s="T152">šam-nia-m</ta>
            <ta e="T154" id="Seg_5474" s="T153">komu</ta>
            <ta e="T156" id="Seg_5475" s="T154">červə-ʔi</ta>
            <ta e="T157" id="Seg_5476" s="T156">oʔbdə-bia-m</ta>
            <ta e="T158" id="Seg_5477" s="T157">bü-zʼiʔ</ta>
            <ta e="T159" id="Seg_5478" s="T158">dĭ-m</ta>
            <ta e="T161" id="Seg_5479" s="T160">bəzə-bia-m</ta>
            <ta e="T162" id="Seg_5480" s="T161">takšə-nə</ta>
            <ta e="T163" id="Seg_5481" s="T162">em-bie-m</ta>
            <ta e="T164" id="Seg_5482" s="T163">dĭbər</ta>
            <ta e="T165" id="Seg_5483" s="T164">ejü</ta>
            <ta e="T166" id="Seg_5484" s="T165">ejü</ta>
            <ta e="T167" id="Seg_5485" s="T166">nuldə-bia-m</ta>
            <ta e="T168" id="Seg_5486" s="T167">dibər</ta>
            <ta e="T169" id="Seg_5487" s="T168">bü</ta>
            <ta e="T170" id="Seg_5488" s="T169">mo-laːm-bi</ta>
            <ta e="T171" id="Seg_5489" s="T170">dĭgəttə</ta>
            <ta e="T172" id="Seg_5490" s="T171">uda-m</ta>
            <ta e="T173" id="Seg_5491" s="T172">kĭškə-bie-m</ta>
            <ta e="T174" id="Seg_5492" s="T173">uda-m</ta>
            <ta e="T175" id="Seg_5493" s="T174">ugaːndə</ta>
            <ta e="T176" id="Seg_5494" s="T175">tăŋ</ta>
            <ta e="T177" id="Seg_5495" s="T176">ĭzem-bi-ʔi</ta>
            <ta e="T178" id="Seg_5496" s="T177">tüj</ta>
            <ta e="T179" id="Seg_5497" s="T178">ej</ta>
            <ta e="T180" id="Seg_5498" s="T179">ĭzem-nie-ʔi</ta>
            <ta e="T181" id="Seg_5499" s="T180">smatri</ta>
            <ta e="T183" id="Seg_5500" s="T181">kudaj</ta>
            <ta e="T184" id="Seg_5501" s="T183">smatri</ta>
            <ta e="T185" id="Seg_5502" s="T184">kudaj</ta>
            <ta e="T186" id="Seg_5503" s="T185">i-ʔ</ta>
            <ta e="T187" id="Seg_5504" s="T186">nöməl-a-ʔ</ta>
            <ta e="T192" id="Seg_5505" s="T191">kan-a-ʔ</ta>
            <ta e="T193" id="Seg_5506" s="T192">eneidəne-nə</ta>
            <ta e="T194" id="Seg_5507" s="T193">eneidəne-n</ta>
            <ta e="T196" id="Seg_5508" s="T195">amnu-t</ta>
            <ta e="T197" id="Seg_5509" s="T196">tüžöj-ə-n</ta>
            <ta e="T198" id="Seg_5510" s="T197">amnu-t</ta>
            <ta e="T199" id="Seg_5511" s="T198">dĭ</ta>
            <ta e="T200" id="Seg_5512" s="T199">bar</ta>
            <ta e="T201" id="Seg_5513" s="T200">tănan</ta>
            <ta e="T203" id="Seg_5514" s="T202">muʔ-lə-j</ta>
            <ta e="T204" id="Seg_5515" s="T203">sʼenʼi-ʔi</ta>
            <ta e="T206" id="Seg_5516" s="T205">săbərəj-də</ta>
            <ta e="T207" id="Seg_5517" s="T206">ato</ta>
            <ta e="T208" id="Seg_5518" s="T207">ugaːndə</ta>
            <ta e="T209" id="Seg_5519" s="T208">iʔgö</ta>
            <ta e="T210" id="Seg_5520" s="T209">balgaš</ta>
            <ta e="T211" id="Seg_5521" s="T210">krilso</ta>
            <ta e="T212" id="Seg_5522" s="T211">săbərəj-də</ta>
            <ta e="T213" id="Seg_5523" s="T212">anbar-də</ta>
            <ta e="T214" id="Seg_5524" s="T213">un</ta>
            <ta e="T215" id="Seg_5525" s="T214">i-le-m</ta>
            <ta e="T216" id="Seg_5526" s="T215">em-bie-m</ta>
            <ta e="T217" id="Seg_5527" s="T216">măn</ta>
            <ta e="T218" id="Seg_5528" s="T217">dĭ-m</ta>
            <ta e="T219" id="Seg_5529" s="T218">kuʔ-pio-m</ta>
            <ta e="T220" id="Seg_5530" s="T219">măn</ta>
            <ta e="T221" id="Seg_5531" s="T220">šal-də</ta>
            <ta e="T222" id="Seg_5532" s="T221">iʔgö</ta>
            <ta e="T223" id="Seg_5533" s="T222">măn</ta>
            <ta e="T224" id="Seg_5534" s="T223">ugaːndə</ta>
            <ta e="T225" id="Seg_5535" s="T224">kuštu</ta>
            <ta e="T226" id="Seg_5536" s="T225">i-ge-m</ta>
            <ta e="T227" id="Seg_5537" s="T226">dĭ</ta>
            <ta e="T228" id="Seg_5538" s="T227">kuza</ta>
            <ta e="T229" id="Seg_5539" s="T228">bar</ta>
            <ta e="T230" id="Seg_5540" s="T229">sagəš-də</ta>
            <ta e="T231" id="Seg_5541" s="T230">naga</ta>
            <ta e="T232" id="Seg_5542" s="T231">šĭke-t</ta>
            <ta e="T233" id="Seg_5543" s="T232">i-ge</ta>
            <ta e="T234" id="Seg_5544" s="T233">dʼăbaktər-zittə</ta>
            <ta e="T235" id="Seg_5545" s="T234">ej</ta>
            <ta e="T236" id="Seg_5546" s="T235">mo-lia</ta>
            <ta e="T237" id="Seg_5547" s="T236">ku-t</ta>
            <ta e="T238" id="Seg_5548" s="T237">i-ge</ta>
            <ta e="T239" id="Seg_5549" s="T238">ej</ta>
            <ta e="T240" id="Seg_5550" s="T239">nün-nie</ta>
            <ta e="T241" id="Seg_5551" s="T240">i-ʔ</ta>
            <ta e="T242" id="Seg_5552" s="T241">parlo-ʔ</ta>
            <ta e="T243" id="Seg_5553" s="T242">jakšə</ta>
            <ta e="T244" id="Seg_5554" s="T243">amno-ʔ</ta>
            <ta e="T245" id="Seg_5555" s="T244">ĭmbi</ta>
            <ta e="T246" id="Seg_5556" s="T245">parlo-raʔbə-l</ta>
            <ta e="T247" id="Seg_5557" s="T246">bos-tə</ta>
            <ta e="T248" id="Seg_5558" s="T247">šĭke-m</ta>
            <ta e="T249" id="Seg_5559" s="T248">nün-niö-m</ta>
            <ta e="T250" id="Seg_5560" s="T249">jakšə</ta>
            <ta e="T251" id="Seg_5561" s="T250">dʼăbaktər-lia-m</ta>
            <ta e="T252" id="Seg_5562" s="T251">šoška</ta>
            <ta e="T253" id="Seg_5563" s="T252">šonə-ga</ta>
            <ta e="T254" id="Seg_5564" s="T253">i</ta>
            <ta e="T255" id="Seg_5565" s="T254">kurem-naʔbə</ta>
            <ta e="T256" id="Seg_5566" s="T255">bar</ta>
            <ta e="T257" id="Seg_5567" s="T256">nada</ta>
            <ta e="T258" id="Seg_5568" s="T257">dĭ-m</ta>
            <ta e="T259" id="Seg_5569" s="T258">bădə-sʼtə</ta>
            <ta e="T260" id="Seg_5570" s="T259">ato</ta>
            <ta e="T261" id="Seg_5571" s="T260">dĭ</ta>
            <ta e="T262" id="Seg_5572" s="T261">püjö-lia</ta>
            <ta e="T263" id="Seg_5573" s="T262">bar</ta>
            <ta e="T264" id="Seg_5574" s="T263">nada</ta>
            <ta e="T266" id="Seg_5575" s="T265">baltu</ta>
            <ta e="T267" id="Seg_5576" s="T266">puʔmə-sʼtə</ta>
            <ta e="T268" id="Seg_5577" s="T267">pa-ʔi</ta>
            <ta e="T273" id="Seg_5578" s="T272">jaʔ-sʼittə</ta>
            <ta e="T274" id="Seg_5579" s="T273">pʼeš</ta>
            <ta e="T275" id="Seg_5580" s="T274">nen-zittə</ta>
            <ta e="T276" id="Seg_5581" s="T275">pʼila</ta>
            <ta e="T277" id="Seg_5582" s="T276">dep-pi-ʔi</ta>
            <ta e="T278" id="Seg_5583" s="T277">ej</ta>
            <ta e="T279" id="Seg_5584" s="T278">tĭmne-ʔi</ta>
            <ta e="T280" id="Seg_5585" s="T279">ĭmbi</ta>
            <ta e="T281" id="Seg_5586" s="T280">dĭ-zʼiʔ</ta>
            <ta e="T282" id="Seg_5587" s="T281">a-zittə</ta>
            <ta e="T284" id="Seg_5588" s="T283">Varlam</ta>
            <ta e="T285" id="Seg_5589" s="T284">šo-lə-j</ta>
            <ta e="T286" id="Seg_5590" s="T285">tʼazir-lə-j</ta>
            <ta e="T287" id="Seg_5591" s="T286">baza-j</ta>
            <ta e="T288" id="Seg_5592" s="T287">pʼeš</ta>
            <ta e="T289" id="Seg_5593" s="T288">dep-pi-ʔi</ta>
            <ta e="T290" id="Seg_5594" s="T289">tura-nə</ta>
            <ta e="T291" id="Seg_5595" s="T290">nuldə-bi-ʔi</ta>
            <ta e="T294" id="Seg_5596" s="T293">šojdʼo-nə</ta>
            <ta e="T295" id="Seg_5597" s="T294">pasə</ta>
            <ta e="T296" id="Seg_5598" s="T295">šojdʼo</ta>
            <ta e="T297" id="Seg_5599" s="T296">dĭ</ta>
            <ta e="T298" id="Seg_5600" s="T297">bar</ta>
            <ta e="T299" id="Seg_5601" s="T298">nend-luʔ-pi</ta>
            <ta e="T300" id="Seg_5602" s="T299">bər</ta>
            <ta e="T301" id="Seg_5603" s="T300">šo-bi</ta>
            <ta e="T302" id="Seg_5604" s="T301">dĭgəttə</ta>
            <ta e="T303" id="Seg_5605" s="T302">bar</ta>
            <ta e="T304" id="Seg_5606" s="T303">əʔ-pi-ʔi</ta>
            <ta e="T305" id="Seg_5607" s="T304">nʼiʔdə</ta>
            <ta e="T306" id="Seg_5608" s="T305">ugaːndə</ta>
            <ta e="T307" id="Seg_5609" s="T306">bər</ta>
            <ta e="T308" id="Seg_5610" s="T307">iʔgö</ta>
            <ta e="T309" id="Seg_5611" s="T308">Varlam</ta>
            <ta e="T310" id="Seg_5612" s="T309">šo-lə-j</ta>
            <ta e="T311" id="Seg_5613" s="T310">i</ta>
            <ta e="T312" id="Seg_5614" s="T311">dʼazir-lə-j</ta>
            <ta e="T313" id="Seg_5615" s="T312">bar</ta>
            <ta e="T314" id="Seg_5616" s="T313">bər</ta>
            <ta e="T315" id="Seg_5617" s="T314">sʼima-m</ta>
            <ta e="T316" id="Seg_5618" s="T315">amna-laʔbə-ʔjə</ta>
            <ta e="T317" id="Seg_5619" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_5620" s="T317">Varlam</ta>
            <ta e="T319" id="Seg_5621" s="T318">šo-bi</ta>
            <ta e="T320" id="Seg_5622" s="T319">pi</ta>
            <ta e="T321" id="Seg_5623" s="T320">deʔ-pi</ta>
            <ta e="T322" id="Seg_5624" s="T321">dʼü-nə</ta>
            <ta e="T323" id="Seg_5625" s="T322">em-bi</ta>
            <ta e="T324" id="Seg_5626" s="T323">i</ta>
            <ta e="T325" id="Seg_5627" s="T324">pʼeš-də</ta>
            <ta e="T326" id="Seg_5628" s="T325">nuldə-bi</ta>
            <ta e="T327" id="Seg_5629" s="T326">dĭgəttə</ta>
            <ta e="T328" id="Seg_5630" s="T327">pʼeš</ta>
            <ta e="T329" id="Seg_5631" s="T328">nendə-bi-ʔi</ta>
            <ta e="T330" id="Seg_5632" s="T329">amno-laʔbə-ʔjə</ta>
            <ta e="T331" id="Seg_5633" s="T330">i</ta>
            <ta e="T333" id="Seg_5634" s="T332">ejü-leʔbə-ʔjə</ta>
            <ta e="T339" id="Seg_5635" s="T338">părga-m</ta>
            <ta e="T340" id="Seg_5636" s="T339">bar</ta>
            <ta e="T341" id="Seg_5637" s="T340">dʼü-gən</ta>
            <ta e="T342" id="Seg_5638" s="T341">dʼü-gən</ta>
            <ta e="T343" id="Seg_5639" s="T342">iʔbö-bi</ta>
            <ta e="T344" id="Seg_5640" s="T343">üjü-ʔi</ta>
            <ta e="T345" id="Seg_5641" s="T344">bar</ta>
            <ta e="T346" id="Seg_5642" s="T345">kĭškə-bi-ʔi</ta>
            <ta e="T347" id="Seg_5643" s="T346">dĭn</ta>
            <ta e="T348" id="Seg_5644" s="T347">dĭn</ta>
            <ta e="T349" id="Seg_5645" s="T348">bar</ta>
            <ta e="T350" id="Seg_5646" s="T349">balgaš</ta>
            <ta e="T351" id="Seg_5647" s="T350">măn</ta>
            <ta e="T352" id="Seg_5648" s="T351">dĭ-m</ta>
            <ta e="T353" id="Seg_5649" s="T352">i-bie-l</ta>
            <ta e="T354" id="Seg_5650" s="T353">i-bie-m</ta>
            <ta e="T355" id="Seg_5651" s="T354">da</ta>
            <ta e="T356" id="Seg_5652" s="T355">bü-nə</ta>
            <ta e="T357" id="Seg_5653" s="T356">kum-bia-l</ta>
            <ta e="T358" id="Seg_5654" s="T357">kum-bia-m</ta>
            <ta e="T359" id="Seg_5655" s="T358">štobɨ</ta>
            <ta e="T360" id="Seg_5656" s="T359">dʼüʔpi</ta>
            <ta e="T361" id="Seg_5657" s="T360">mo-bi</ta>
            <ta e="T362" id="Seg_5658" s="T361">dĭgəttə</ta>
            <ta e="T363" id="Seg_5659" s="T362">toʔ-nar-bia-m</ta>
            <ta e="T364" id="Seg_5660" s="T363">bəzə-bia-m</ta>
            <ta e="T365" id="Seg_5661" s="T364">dĭgəttə</ta>
            <ta e="T366" id="Seg_5662" s="T365">edə-bie-m</ta>
            <ta e="T367" id="Seg_5663" s="T366">bü</ta>
            <ta e="T368" id="Seg_5664" s="T367">bar</ta>
            <ta e="T369" id="Seg_5665" s="T368">mʼaŋ-ŋaʔbə</ta>
            <ta e="T370" id="Seg_5666" s="T369">dĭ-gəʔ</ta>
            <ta e="T371" id="Seg_5667" s="T370">dʼaga</ta>
            <ta e="T372" id="Seg_5668" s="T371">mʼaŋ-ŋaʔbə</ta>
            <ta e="T373" id="Seg_5669" s="T372">i</ta>
            <ta e="T374" id="Seg_5670" s="T373">kuza</ta>
            <ta e="T375" id="Seg_5671" s="T374">nuʔmə-leʔbə</ta>
            <ta e="T376" id="Seg_5672" s="T375">bü-j-leʔ</ta>
            <ta e="T377" id="Seg_5673" s="T376">girgit=də</ta>
            <ta e="T378" id="Seg_5674" s="T377">il</ta>
            <ta e="T379" id="Seg_5675" s="T378">mĭl-leʔbə-ʔjə</ta>
            <ta e="T380" id="Seg_5676" s="T379">măn</ta>
            <ta e="T381" id="Seg_5677" s="T380">dĭ-m</ta>
            <ta e="T382" id="Seg_5678" s="T381">dĭ-zem</ta>
            <ta e="T383" id="Seg_5679" s="T382">ej</ta>
            <ta e="T384" id="Seg_5680" s="T383">tĭmne-m</ta>
            <ta e="T385" id="Seg_5681" s="T384">ĭmbi</ta>
            <ta e="T387" id="Seg_5682" s="T386">dĭ-zeŋ</ta>
            <ta e="T388" id="Seg_5683" s="T387">dĭn</ta>
            <ta e="T389" id="Seg_5684" s="T388">mĭl-leʔbə-ʔjə</ta>
            <ta e="T390" id="Seg_5685" s="T389">da</ta>
            <ta e="T391" id="Seg_5686" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_5687" s="T391">nʼi</ta>
            <ta e="T393" id="Seg_5688" s="T392">da</ta>
            <ta e="T394" id="Seg_5689" s="T393">koʔbdo</ta>
            <ta e="T395" id="Seg_5690" s="T394">šonə-ga-ʔi</ta>
            <ta e="T396" id="Seg_5691" s="T395">nada</ta>
            <ta e="T397" id="Seg_5692" s="T396">sut</ta>
            <ta e="T398" id="Seg_5693" s="T397">kămna-sʼtə</ta>
            <ta e="T399" id="Seg_5694" s="T398">šojdʼo-nə</ta>
            <ta e="T400" id="Seg_5695" s="T399">puskaj</ta>
            <ta e="T401" id="Seg_5696" s="T400">namzəga</ta>
            <ta e="T402" id="Seg_5697" s="T401">mo-lə-j</ta>
            <ta e="T403" id="Seg_5698" s="T402">tüjö</ta>
            <ta e="T404" id="Seg_5699" s="T403">kal-la</ta>
            <ta e="T406" id="Seg_5700" s="T405">dʼür-bi-beʔ</ta>
            <ta e="T407" id="Seg_5701" s="T406">no</ta>
            <ta e="T408" id="Seg_5702" s="T407">kan-a-ʔ</ta>
            <ta e="T409" id="Seg_5703" s="T408">kan-gaʔ</ta>
            <ta e="T410" id="Seg_5704" s="T409">kudaj-zʼiʔ</ta>
            <ta e="T411" id="Seg_5705" s="T410">miʔ</ta>
            <ta e="T412" id="Seg_5706" s="T411">ugaːndə</ta>
            <ta e="T413" id="Seg_5707" s="T412">iʔgö</ta>
            <ta e="T414" id="Seg_5708" s="T413">dʼăbaktər-bi-baʔ</ta>
            <ta e="T415" id="Seg_5709" s="T414">tüj</ta>
            <ta e="T416" id="Seg_5710" s="T415">kan-zittə</ta>
            <ta e="T417" id="Seg_5711" s="T416">nada</ta>
            <ta e="T418" id="Seg_5712" s="T417">maː-ndə</ta>
            <ta e="T419" id="Seg_5713" s="T418">da</ta>
            <ta e="T420" id="Seg_5714" s="T419">kunol-zittə</ta>
            <ta e="T421" id="Seg_5715" s="T420">iʔbə-sʼtə</ta>
            <ta e="T422" id="Seg_5716" s="T421">i-ʔ</ta>
            <ta e="T424" id="Seg_5717" s="T423">i-ʔ</ta>
            <ta e="T425" id="Seg_5718" s="T424">makta-nar-ə-ʔ</ta>
            <ta e="T426" id="Seg_5719" s="T425">ĭmbi=də</ta>
            <ta e="T427" id="Seg_5720" s="T426">ej</ta>
            <ta e="T428" id="Seg_5721" s="T427">a-bia-l</ta>
            <ta e="T429" id="Seg_5722" s="T428">a</ta>
            <ta e="T430" id="Seg_5723" s="T429">bar</ta>
            <ta e="T431" id="Seg_5724" s="T430">makta-nar-bia-l</ta>
            <ta e="T432" id="Seg_5725" s="T431">kunol-zittə</ta>
            <ta e="T433" id="Seg_5726" s="T432">iʔbə-sʼtə</ta>
            <ta e="T434" id="Seg_5727" s="T433">a</ta>
            <ta e="T435" id="Seg_5728" s="T434">karəldʼan</ta>
            <ta e="T436" id="Seg_5729" s="T435">šagəš-tə</ta>
            <ta e="T437" id="Seg_5730" s="T436">šo-lə-j</ta>
            <ta e="T438" id="Seg_5731" s="T437">kuznek-tə</ta>
            <ta e="T439" id="Seg_5732" s="T438">kuza</ta>
            <ta e="T440" id="Seg_5733" s="T439">šo-bi</ta>
            <ta e="T441" id="Seg_5734" s="T440">a</ta>
            <ta e="T442" id="Seg_5735" s="T441">măn</ta>
            <ta e="T443" id="Seg_5736" s="T442">pi</ta>
            <ta e="T444" id="Seg_5737" s="T443">kabar-luʔ-pia-m</ta>
            <ta e="T449" id="Seg_5738" s="T448">pi</ta>
            <ta e="T450" id="Seg_5739" s="T449">kabar-luʔ-pio-m</ta>
            <ta e="T451" id="Seg_5740" s="T450">i</ta>
            <ta e="T452" id="Seg_5741" s="T451">kuʔ-pio-m</ta>
            <ta e="T453" id="Seg_5742" s="T452">dĭ</ta>
            <ta e="T454" id="Seg_5743" s="T453">kuza-m</ta>
            <ta e="T455" id="Seg_5744" s="T454">kuza</ta>
            <ta e="T456" id="Seg_5745" s="T455">kuza-m</ta>
            <ta e="T457" id="Seg_5746" s="T456">bar</ta>
            <ta e="T458" id="Seg_5747" s="T457">dagaj-zʼiʔ</ta>
            <ta e="T459" id="Seg_5748" s="T458">dʼagar-luʔ-pi</ta>
            <ta e="T460" id="Seg_5749" s="T459">i</ta>
            <ta e="T461" id="Seg_5750" s="T460">dĭ</ta>
            <ta e="T462" id="Seg_5751" s="T461">kü-laːm-bi</ta>
            <ta e="T463" id="Seg_5752" s="T462">dĭn</ta>
            <ta e="T464" id="Seg_5753" s="T463">onʼiʔ</ta>
            <ta e="T465" id="Seg_5754" s="T464">ne</ta>
            <ta e="T466" id="Seg_5755" s="T465">Kazan</ta>
            <ta e="T467" id="Seg_5756" s="T466">tura-gən</ta>
            <ta e="T468" id="Seg_5757" s="T467">onʼiʔ</ta>
            <ta e="T469" id="Seg_5758" s="T468">ne</ta>
            <ta e="T470" id="Seg_5759" s="T469">tibi-bə</ta>
            <ta e="T471" id="Seg_5760" s="T470">bar</ta>
            <ta e="T472" id="Seg_5761" s="T471">jaʔ-pi</ta>
            <ta e="T473" id="Seg_5762" s="T472">baltu-zʼiʔ</ta>
            <ta e="T474" id="Seg_5763" s="T473">ulu-t</ta>
            <ta e="T475" id="Seg_5764" s="T474">saj-jaʔ-pi</ta>
            <ta e="T476" id="Seg_5765" s="T475">dĭ-n</ta>
            <ta e="T477" id="Seg_5766" s="T476">tibi</ta>
            <ta e="T478" id="Seg_5767" s="T477">onʼiʔ</ta>
            <ta e="T479" id="Seg_5768" s="T478">kunol-luʔ-pi</ta>
            <ta e="T480" id="Seg_5769" s="T479">bar</ta>
            <ta e="T481" id="Seg_5770" s="T480">ugaːndə</ta>
            <ta e="T483" id="Seg_5771" s="T482">pim-bie-m</ta>
            <ta e="T484" id="Seg_5772" s="T483">kăda</ta>
            <ta e="T485" id="Seg_5773" s="T484">măna</ta>
            <ta e="T486" id="Seg_5774" s="T485">ulu-m</ta>
            <ta e="T487" id="Seg_5775" s="T486">ej</ta>
            <ta e="T488" id="Seg_5776" s="T487">jaʔ-pi</ta>
            <ta e="T489" id="Seg_5777" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_5778" s="T489">bar</ta>
            <ta e="T491" id="Seg_5779" s="T490">nanəʔzəbi</ta>
            <ta e="T492" id="Seg_5780" s="T491">i-bi</ta>
            <ta e="T493" id="Seg_5781" s="T492">i</ta>
            <ta e="T494" id="Seg_5782" s="T493">šide</ta>
            <ta e="T495" id="Seg_5783" s="T494">es-seŋ</ta>
            <ta e="T496" id="Seg_5784" s="T495">ma-luʔ-pi-ʔi</ta>
            <ta e="T500" id="Seg_5785" s="T499">bar</ta>
            <ta e="T501" id="Seg_5786" s="T500">sumna</ta>
            <ta e="T502" id="Seg_5787" s="T501">pʼe</ta>
            <ta e="T503" id="Seg_5788" s="T502">kal-la</ta>
            <ta e="T504" id="Seg_5789" s="T503">dʼür-bi</ta>
            <ta e="T505" id="Seg_5790" s="T504">taldʼen</ta>
            <ta e="T506" id="Seg_5791" s="T505">ej</ta>
            <ta e="T507" id="Seg_5792" s="T506">i-bie-l</ta>
            <ta e="T508" id="Seg_5793" s="T507">teinen</ta>
            <ta e="T511" id="Seg_5794" s="T510">šo-bia-l</ta>
            <ta e="T512" id="Seg_5795" s="T511">nʼilgö-sʼtə</ta>
            <ta e="T513" id="Seg_5796" s="T512">ĭmbi</ta>
            <ta e="T514" id="Seg_5797" s="T513">dʼăbaktər-laʔbə-baʔ</ta>
            <ta e="T515" id="Seg_5798" s="T514">măn</ta>
            <ta e="T516" id="Seg_5799" s="T515">teinen</ta>
            <ta e="T517" id="Seg_5800" s="T516">kudaj</ta>
            <ta e="T518" id="Seg_5801" s="T517">num-anə</ta>
            <ta e="T521" id="Seg_5802" s="T520">üz-lə-m</ta>
            <ta e="T522" id="Seg_5803" s="T521">kudaj</ta>
            <ta e="T523" id="Seg_5804" s="T522">šo-ʔ</ta>
            <ta e="T524" id="Seg_5805" s="T523">măna</ta>
            <ta e="T525" id="Seg_5806" s="T524">nʼilgö-t</ta>
            <ta e="T526" id="Seg_5807" s="T525">kăda</ta>
            <ta e="T527" id="Seg_5808" s="T526">măn</ta>
            <ta e="T528" id="Seg_5809" s="T527">üz-lie-m</ta>
            <ta e="T529" id="Seg_5810" s="T528">sĭj-bə</ta>
            <ta e="T531" id="Seg_5811" s="T530">măndə-ʔ</ta>
            <ta e="T532" id="Seg_5812" s="T531">padʼi</ta>
            <ta e="T534" id="Seg_5813" s="T533">arə-lia-ʔ</ta>
            <ta e="T535" id="Seg_5814" s="T534">a</ta>
            <ta e="T536" id="Seg_5815" s="T535">ej</ta>
            <ta e="T537" id="Seg_5816" s="T536">arə</ta>
            <ta e="T538" id="Seg_5817" s="T537">dăk</ta>
            <ta e="T539" id="Seg_5818" s="T538">bəzi-t</ta>
            <ta e="T540" id="Seg_5819" s="T539">kem-zʼiʔ</ta>
            <ta e="T541" id="Seg_5820" s="T540">tăn</ta>
            <ta e="T542" id="Seg_5821" s="T541">pa-gən</ta>
            <ta e="T543" id="Seg_5822" s="T542">edə-bie-l</ta>
            <ta e="T544" id="Seg_5823" s="T543">kem-bə</ta>
            <ta e="T545" id="Seg_5824" s="T544">mʼaŋ-naʔpi</ta>
            <ta e="T546" id="Seg_5825" s="T545">bəzi-t</ta>
            <ta e="T547" id="Seg_5826" s="T546">măna</ta>
            <ta e="T548" id="Seg_5827" s="T547">jakšə</ta>
            <ta e="T549" id="Seg_5828" s="T548">bəzi-t</ta>
            <ta e="T550" id="Seg_5829" s="T549">tăn</ta>
            <ta e="T551" id="Seg_5830" s="T550">kudaj</ta>
            <ta e="T552" id="Seg_5831" s="T551">šal-də</ta>
            <ta e="T553" id="Seg_5832" s="T552">iʔgö</ta>
            <ta e="T554" id="Seg_5833" s="T553">bar</ta>
            <ta e="T556" id="Seg_5834" s="T555">il-ə-m</ta>
            <ta e="T558" id="Seg_5835" s="T557">ajir-laʔbə-l</ta>
            <ta e="T560" id="Seg_5836" s="T559">ipek</ta>
            <ta e="T561" id="Seg_5837" s="T560">mĭ-lie-l</ta>
            <ta e="T563" id="Seg_5838" s="T562">tăn</ta>
            <ta e="T564" id="Seg_5839" s="T563">kudaj</ta>
            <ta e="T565" id="Seg_5840" s="T564">šal-də</ta>
            <ta e="T566" id="Seg_5841" s="T565">iʔgö</ta>
            <ta e="T567" id="Seg_5842" s="T566">tăn</ta>
            <ta e="T568" id="Seg_5843" s="T567">il</ta>
            <ta e="T569" id="Seg_5844" s="T568">il</ta>
            <ta e="T570" id="Seg_5845" s="T569">il</ta>
            <ta e="T571" id="Seg_5846" s="T570">ajir-ia-l</ta>
            <ta e="T572" id="Seg_5847" s="T571">ipek</ta>
            <ta e="T573" id="Seg_5848" s="T572">dĭ-zeŋ-də</ta>
            <ta e="T574" id="Seg_5849" s="T573">mĭ-lie-l</ta>
            <ta e="T575" id="Seg_5850" s="T574">bar</ta>
            <ta e="T576" id="Seg_5851" s="T575">kujnek</ta>
            <ta e="T577" id="Seg_5852" s="T576">mĭ-lie-l</ta>
            <ta e="T578" id="Seg_5853" s="T577">bü</ta>
            <ta e="T579" id="Seg_5854" s="T578">mĭ-lie-l</ta>
            <ta e="T580" id="Seg_5855" s="T579">i</ta>
            <ta e="T581" id="Seg_5856" s="T580">măna</ta>
            <ta e="T582" id="Seg_5857" s="T581">ajir-a-ʔ</ta>
            <ta e="T588" id="Seg_5858" s="T587">tăn</ta>
            <ta e="T589" id="Seg_5859" s="T588">bar</ta>
            <ta e="T590" id="Seg_5860" s="T589">ejü</ta>
            <ta e="T591" id="Seg_5861" s="T590">öʔ-lie-l</ta>
            <ta e="T592" id="Seg_5862" s="T591">surno</ta>
            <ta e="T593" id="Seg_5863" s="T592">mĭ-lie-l</ta>
            <ta e="T594" id="Seg_5864" s="T593">štobɨ</ta>
            <ta e="T595" id="Seg_5865" s="T594">ipek</ta>
            <ta e="T596" id="Seg_5866" s="T595">özer-leʔpi</ta>
            <ta e="T600" id="Seg_5867" s="T599">kudaj-zʼiʔ</ta>
            <ta e="T601" id="Seg_5868" s="T600">jakšə</ta>
            <ta e="T602" id="Seg_5869" s="T601">gibər</ta>
            <ta e="T603" id="Seg_5870" s="T602">kal-la-l</ta>
            <ta e="T604" id="Seg_5871" s="T603">kudaj-zʼiʔ</ta>
            <ta e="T607" id="Seg_5872" s="T606">ugaːndə</ta>
            <ta e="T608" id="Seg_5873" s="T607">jakšə</ta>
            <ta e="T609" id="Seg_5874" s="T608">kudaj-zʼiʔ</ta>
            <ta e="T610" id="Seg_5875" s="T609">beržə</ta>
            <ta e="T611" id="Seg_5876" s="T610">saʔmə-luʔ-pi</ta>
            <ta e="T612" id="Seg_5877" s="T611">măn</ta>
            <ta e="T613" id="Seg_5878" s="T612">i-bie-m</ta>
            <ta e="T614" id="Seg_5879" s="T613">da</ta>
            <ta e="T615" id="Seg_5880" s="T614">tura-nə</ta>
            <ta e="T616" id="Seg_5881" s="T615">deʔ-pie-m</ta>
            <ta e="T617" id="Seg_5882" s="T616">jakšə</ta>
            <ta e="T618" id="Seg_5883" s="T617">surno</ta>
            <ta e="T619" id="Seg_5884" s="T618">šonə-ga</ta>
            <ta e="T620" id="Seg_5885" s="T619">ipek</ta>
            <ta e="T621" id="Seg_5886" s="T620">özer-lə-j</ta>
            <ta e="T622" id="Seg_5887" s="T621">măn</ta>
            <ta e="T623" id="Seg_5888" s="T622">dĭʔ-nə</ta>
            <ta e="T624" id="Seg_5889" s="T623">kabažar-bia-m</ta>
            <ta e="T625" id="Seg_5890" s="T624">a</ta>
            <ta e="T626" id="Seg_5891" s="T625">dĭ</ta>
            <ta e="T627" id="Seg_5892" s="T626">măna</ta>
            <ta e="T628" id="Seg_5893" s="T627">ej</ta>
            <ta e="T629" id="Seg_5894" s="T628">kabažar-laʔbə</ta>
            <ta e="T630" id="Seg_5895" s="T629">ej</ta>
            <ta e="T631" id="Seg_5896" s="T630">mĭ-lie</ta>
            <ta e="T632" id="Seg_5897" s="T631">măna</ta>
            <ta e="T633" id="Seg_5898" s="T632">togonor-zittə</ta>
            <ta e="T634" id="Seg_5899" s="T633">măn</ta>
            <ta e="T635" id="Seg_5900" s="T634">bɨ</ta>
            <ta e="T636" id="Seg_5901" s="T635">togonor-bia-m</ta>
            <ta e="T637" id="Seg_5902" s="T636">a</ta>
            <ta e="T638" id="Seg_5903" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_5904" s="T638">üge</ta>
            <ta e="T640" id="Seg_5905" s="T639">puʔ-laʔbə</ta>
            <ta e="T641" id="Seg_5906" s="T640">măna</ta>
            <ta e="T642" id="Seg_5907" s="T641">kurojək</ta>
            <ta e="T643" id="Seg_5908" s="T642">bar</ta>
            <ta e="T644" id="Seg_5909" s="T643">uju-zʼiʔ</ta>
            <ta e="T645" id="Seg_5910" s="T644">bar</ta>
            <ta e="T646" id="Seg_5911" s="T645">tonuʔ-pi</ta>
            <ta e="T647" id="Seg_5912" s="T646">ugaːndə</ta>
            <ta e="T648" id="Seg_5913" s="T647">kurojək</ta>
            <ta e="T649" id="Seg_5914" s="T648">men</ta>
            <ta e="T651" id="Seg_5915" s="T650">bar</ta>
            <ta e="T653" id="Seg_5916" s="T652">talbə-lia</ta>
            <ta e="T654" id="Seg_5917" s="T653">il</ta>
            <ta e="T657" id="Seg_5918" s="T656">ugaːndə</ta>
            <ta e="T658" id="Seg_5919" s="T657">kurojək</ta>
            <ta e="T659" id="Seg_5920" s="T658">men</ta>
            <ta e="T661" id="Seg_5921" s="T660">bar</ta>
            <ta e="T662" id="Seg_5922" s="T661">il</ta>
            <ta e="T663" id="Seg_5923" s="T662">talbə-r-laʔbə</ta>
            <ta e="T664" id="Seg_5924" s="T663">i</ta>
            <ta e="T665" id="Seg_5925" s="T664">măna</ta>
            <ta e="T666" id="Seg_5926" s="T665">talbə-r-bi</ta>
            <ta e="T667" id="Seg_5927" s="T666">măn</ta>
            <ta e="T668" id="Seg_5928" s="T667">šiʔ</ta>
            <ta e="T669" id="Seg_5929" s="T668">edəʔ-pie-m</ta>
            <ta e="T670" id="Seg_5930" s="T669">šiʔ</ta>
            <ta e="T671" id="Seg_5931" s="T670">ej</ta>
            <ta e="T672" id="Seg_5932" s="T671">šo-bi-laʔ</ta>
            <ta e="T673" id="Seg_5933" s="T672">kundʼo</ta>
            <ta e="T674" id="Seg_5934" s="T673">nago-bi-laʔ</ta>
            <ta e="T675" id="Seg_5935" s="T674">takšə-gəʔ</ta>
            <ta e="T676" id="Seg_5936" s="T675">i-bie-m</ta>
            <ta e="T677" id="Seg_5937" s="T676">bəzə-bia-m</ta>
            <ta e="T678" id="Seg_5938" s="T677">i</ta>
            <ta e="T679" id="Seg_5939" s="T678">bazoʔ</ta>
            <ta e="T680" id="Seg_5940" s="T679">em-bie-m</ta>
            <ta e="T681" id="Seg_5941" s="T680">teinöbam</ta>
            <ta e="T682" id="Seg_5942" s="T681">teinöbam</ta>
            <ta e="T683" id="Seg_5943" s="T682">teinen</ta>
            <ta e="T684" id="Seg_5944" s="T683">bar</ta>
            <ta e="T687" id="Seg_5945" s="T686">bü-nə</ta>
            <ta e="T688" id="Seg_5946" s="T687">ej</ta>
            <ta e="T689" id="Seg_5947" s="T688">saʔmə-lia-l</ta>
            <ta e="T690" id="Seg_5948" s="T689">nʼuʔdun</ta>
            <ta e="T691" id="Seg_5949" s="T690">bar</ta>
            <ta e="T692" id="Seg_5950" s="T691">mĭŋ-gə</ta>
            <ta e="T693" id="Seg_5951" s="T692">uda-zʼiʔ</ta>
            <ta e="T694" id="Seg_5952" s="T693">i</ta>
            <ta e="T695" id="Seg_5953" s="T694">üjü-zʼiʔ</ta>
            <ta e="T696" id="Seg_5954" s="T695">bü-gən</ta>
            <ta e="T697" id="Seg_5955" s="T696">bar</ta>
            <ta e="T698" id="Seg_5956" s="T697">kandə-ga-l</ta>
            <ta e="T699" id="Seg_5957" s="T698">uda-zʼiʔ</ta>
            <ta e="T700" id="Seg_5958" s="T699">i</ta>
            <ta e="T701" id="Seg_5959" s="T700">üjü-zʼiʔ</ta>
            <ta e="T702" id="Seg_5960" s="T701">lotka</ta>
            <ta e="T703" id="Seg_5961" s="T702">lotka-i-zʼiʔ</ta>
            <ta e="T704" id="Seg_5962" s="T703">bar</ta>
            <ta e="T706" id="Seg_5963" s="T705">büʔ-nə</ta>
            <ta e="T707" id="Seg_5964" s="T706">kandə-ga</ta>
            <ta e="T708" id="Seg_5965" s="T707">kola</ta>
            <ta e="T709" id="Seg_5966" s="T708">dʼabə-laʔbə</ta>
            <ta e="T710" id="Seg_5967" s="T709">bar</ta>
            <ta e="T711" id="Seg_5968" s="T710">nagur-göʔ</ta>
            <ta e="T712" id="Seg_5969" s="T711">tʼakam-nə-baʔ</ta>
            <ta e="T713" id="Seg_5970" s="T712">onʼiʔ</ta>
            <ta e="T714" id="Seg_5971" s="T713">tănan</ta>
            <ta e="T715" id="Seg_5972" s="T714">onʼiʔ</ta>
            <ta e="T716" id="Seg_5973" s="T715">măna</ta>
            <ta e="T717" id="Seg_5974" s="T716">onʼiʔ</ta>
            <ta e="T718" id="Seg_5975" s="T717">dĭʔ-nə</ta>
            <ta e="T719" id="Seg_5976" s="T718">šide</ta>
            <ta e="T720" id="Seg_5977" s="T719">pʼel</ta>
            <ta e="T721" id="Seg_5978" s="T720">nagur</ta>
            <ta e="T722" id="Seg_5979" s="T721">pʼel</ta>
            <ta e="T723" id="Seg_5980" s="T722">teʔtə</ta>
            <ta e="T724" id="Seg_5981" s="T723">pʼel</ta>
            <ta e="T725" id="Seg_5982" s="T724">dăra</ta>
            <ta e="T726" id="Seg_5983" s="T725">dĭ</ta>
            <ta e="T727" id="Seg_5984" s="T726">dĭ-zeŋ</ta>
            <ta e="T728" id="Seg_5985" s="T727">bar</ta>
            <ta e="T729" id="Seg_5986" s="T728">amno-bi-ʔi</ta>
            <ta e="T730" id="Seg_5987" s="T729">onʼiʔ</ta>
            <ta e="T731" id="Seg_5988" s="T730">ine-gən</ta>
            <ta e="T732" id="Seg_5989" s="T731">măn</ta>
            <ta e="T733" id="Seg_5990" s="T732">amno-bia-m</ta>
            <ta e="T734" id="Seg_5991" s="T733">šide</ta>
            <ta e="T735" id="Seg_5992" s="T734">ine-gən</ta>
            <ta e="T736" id="Seg_5993" s="T735">ne-m</ta>
            <ta e="T737" id="Seg_5994" s="T736">amnə-bi</ta>
            <ta e="T738" id="Seg_5995" s="T737">nagur</ta>
            <ta e="T739" id="Seg_5996" s="T738">ine-gən</ta>
            <ta e="T740" id="Seg_5997" s="T739">nʼi-m</ta>
            <ta e="T741" id="Seg_5998" s="T740">amnə-bi</ta>
            <ta e="T742" id="Seg_5999" s="T741">teʔtə</ta>
            <ta e="T743" id="Seg_6000" s="T742">ine-gən</ta>
            <ta e="T744" id="Seg_6001" s="T743">koʔbdo-m</ta>
            <ta e="T745" id="Seg_6002" s="T744">amnə-bi</ta>
            <ta e="T746" id="Seg_6003" s="T745">a</ta>
            <ta e="T747" id="Seg_6004" s="T746">sumna</ta>
            <ta e="T748" id="Seg_6005" s="T747">ine-gən</ta>
            <ta e="T749" id="Seg_6006" s="T748">urgoja</ta>
            <ta e="T750" id="Seg_6007" s="T749">amnə-bi</ta>
            <ta e="T752" id="Seg_6008" s="T751">dĭ</ta>
            <ta e="T753" id="Seg_6009" s="T752">nʼi</ta>
            <ta e="T754" id="Seg_6010" s="T753">šo-bi</ta>
            <ta e="T755" id="Seg_6011" s="T754">miʔnʼibeʔ</ta>
            <ta e="T756" id="Seg_6012" s="T755">kür-zittə</ta>
            <ta e="T757" id="Seg_6013" s="T756">bar</ta>
            <ta e="T758" id="Seg_6014" s="T757">büžü</ta>
            <ta e="T759" id="Seg_6015" s="T758">bar</ta>
            <ta e="T760" id="Seg_6016" s="T759">šĭšəge</ta>
            <ta e="T761" id="Seg_6017" s="T760">mo-lə-j</ta>
            <ta e="T763" id="Seg_6018" s="T762">sĭre</ta>
            <ta e="T764" id="Seg_6019" s="T763">bar</ta>
            <ta e="T765" id="Seg_6020" s="T764">saʔmə-lə-j</ta>
            <ta e="T766" id="Seg_6021" s="T765">ugaːndə</ta>
            <ta e="T767" id="Seg_6022" s="T766">šĭšəge</ta>
            <ta e="T768" id="Seg_6023" s="T767">kănna-lə-l</ta>
            <ta e="T769" id="Seg_6024" s="T768">bar</ta>
            <ta e="T770" id="Seg_6025" s="T769">büžü</ta>
            <ta e="T773" id="Seg_6026" s="T772">bar</ta>
            <ta e="T774" id="Seg_6027" s="T773">ejü</ta>
            <ta e="T775" id="Seg_6028" s="T774">mo-lə-j</ta>
            <ta e="T776" id="Seg_6029" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_6030" s="T776">il</ta>
            <ta e="T778" id="Seg_6031" s="T777">tarir-luʔ-pi-ʔi</ta>
            <ta e="T779" id="Seg_6032" s="T778">bar</ta>
            <ta e="T780" id="Seg_6033" s="T779">budəj</ta>
            <ta e="T781" id="Seg_6034" s="T780">kuʔ-luʔ-jəʔ</ta>
            <ta e="T782" id="Seg_6035" s="T781">ugaːndə</ta>
            <ta e="T783" id="Seg_6036" s="T782">kuŋgem</ta>
            <ta e="T784" id="Seg_6037" s="T783">nʼergö-laʔbə</ta>
            <ta e="T786" id="Seg_6038" s="T785">nʼuʔ-nə</ta>
            <ta e="T787" id="Seg_6039" s="T786">ej</ta>
            <ta e="T788" id="Seg_6040" s="T787">id-lia</ta>
            <ta e="T790" id="Seg_6041" s="T788">i</ta>
            <ta e="T791" id="Seg_6042" s="T790">ine-gəʔ</ta>
            <ta e="T794" id="Seg_6043" s="T793">ine-gəʔ</ta>
            <ta e="T795" id="Seg_6044" s="T794">bar</ta>
            <ta e="T796" id="Seg_6045" s="T795">dʼü-nə</ta>
            <ta e="T797" id="Seg_6046" s="T796">saʔmə-luʔ-pi</ta>
            <ta e="T798" id="Seg_6047" s="T797">nʼuʔdə</ta>
            <ta e="T799" id="Seg_6048" s="T798">bar</ta>
            <ta e="T800" id="Seg_6049" s="T799">nada</ta>
            <ta e="T801" id="Seg_6050" s="T800">kan-zittə</ta>
            <ta e="T802" id="Seg_6051" s="T801">ugaːndə</ta>
            <ta e="T807" id="Seg_6052" s="T806">ugaːndə</ta>
            <ta e="T808" id="Seg_6053" s="T807">kudaj</ta>
            <ta e="T809" id="Seg_6054" s="T808">nʼuʔdə</ta>
            <ta e="T810" id="Seg_6055" s="T809">amno-laʔbə</ta>
            <ta e="T811" id="Seg_6056" s="T810">măja-gən</ta>
            <ta e="T814" id="Seg_6057" s="T813">urgo</ta>
            <ta e="T816" id="Seg_6058" s="T815">măja</ta>
            <ta e="T817" id="Seg_6059" s="T816">ulu-m</ta>
            <ta e="T818" id="Seg_6060" s="T817">bar</ta>
            <ta e="T819" id="Seg_6061" s="T818">tʼukum-nie</ta>
            <ta e="T820" id="Seg_6062" s="T819">kădaʔ-lio-m</ta>
            <ta e="T821" id="Seg_6063" s="T820">naverna</ta>
            <ta e="T822" id="Seg_6064" s="T821">unu</ta>
            <ta e="T823" id="Seg_6065" s="T822">iʔgö</ta>
            <ta e="T824" id="Seg_6066" s="T823">nada</ta>
            <ta e="T825" id="Seg_6067" s="T824">dĭ-zeŋ</ta>
            <ta e="T826" id="Seg_6068" s="T825">bar</ta>
            <ta e="T828" id="Seg_6069" s="T826">kut-zʼittə</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_6070" s="T4">sima-t</ta>
            <ta e="T6" id="Seg_6071" s="T5">ej</ta>
            <ta e="T7" id="Seg_6072" s="T6">kuvas</ta>
            <ta e="T8" id="Seg_6073" s="T7">onʼiʔ</ta>
            <ta e="T9" id="Seg_6074" s="T8">döbər</ta>
            <ta e="T10" id="Seg_6075" s="T9">măndo-laʔbə</ta>
            <ta e="T11" id="Seg_6076" s="T10">onʼiʔ</ta>
            <ta e="T12" id="Seg_6077" s="T11">dĭbər</ta>
            <ta e="T13" id="Seg_6078" s="T12">sima-t</ta>
            <ta e="T15" id="Seg_6079" s="T14">măn</ta>
            <ta e="T16" id="Seg_6080" s="T15">aŋ-m</ta>
            <ta e="T17" id="Seg_6081" s="T16">bar</ta>
            <ta e="T18" id="Seg_6082" s="T17">kaj-laʔbə</ta>
            <ta e="T19" id="Seg_6083" s="T18">kunol-zittə</ta>
            <ta e="T20" id="Seg_6084" s="T19">axota</ta>
            <ta e="T21" id="Seg_6085" s="T20">miʔ</ta>
            <ta e="T22" id="Seg_6086" s="T21">šide-göʔ</ta>
            <ta e="T24" id="Seg_6087" s="T23">kan-bi-bAʔ</ta>
            <ta e="T25" id="Seg_6088" s="T24">aʔtʼi-Tə</ta>
            <ta e="T26" id="Seg_6089" s="T25">dĭ</ta>
            <ta e="T28" id="Seg_6090" s="T27">kan-bi</ta>
            <ta e="T29" id="Seg_6091" s="T28">nalʼeva</ta>
            <ta e="T30" id="Seg_6092" s="T29">măn</ta>
            <ta e="T31" id="Seg_6093" s="T30">kan-bi-m</ta>
            <ta e="T32" id="Seg_6094" s="T31">naprava</ta>
            <ta e="T33" id="Seg_6095" s="T32">kădaʔ</ta>
            <ta e="T34" id="Seg_6096" s="T33">dĭ-m</ta>
            <ta e="T35" id="Seg_6097" s="T34">kăštə-liA-jəʔ</ta>
            <ta e="T36" id="Seg_6098" s="T35">măn</ta>
            <ta e="T37" id="Seg_6099" s="T36">ej</ta>
            <ta e="T38" id="Seg_6100" s="T37">tĭmne-m</ta>
            <ta e="T39" id="Seg_6101" s="T38">čola-jəʔ</ta>
            <ta e="T40" id="Seg_6102" s="T39">bar</ta>
            <ta e="T41" id="Seg_6103" s="T40">nʼamga</ta>
            <ta e="T42" id="Seg_6104" s="T41">tʼerə</ta>
            <ta e="T43" id="Seg_6105" s="T42">det-lV-jəʔ</ta>
            <ta e="T44" id="Seg_6106" s="T43">i</ta>
            <ta e="T45" id="Seg_6107" s="T44">takše-Tə</ta>
            <ta e="T47" id="Seg_6108" s="T46">hen-laʔbə-jəʔ</ta>
            <ta e="T48" id="Seg_6109" s="T47">ugaːndə</ta>
            <ta e="T49" id="Seg_6110" s="T48">tăŋ</ta>
            <ta e="T50" id="Seg_6111" s="T49">togonər-laʔbə-jəʔ</ta>
            <ta e="T51" id="Seg_6112" s="T50">dʼije-Kən</ta>
            <ta e="T53" id="Seg_6113" s="T52">dʼije-Kən</ta>
            <ta e="T54" id="Seg_6114" s="T53">ej</ta>
            <ta e="T55" id="Seg_6115" s="T54">amno-laʔbə-jəʔ</ta>
            <ta e="T56" id="Seg_6116" s="T55">bü-Kən</ta>
            <ta e="T57" id="Seg_6117" s="T56">ej</ta>
            <ta e="T58" id="Seg_6118" s="T57">amno-laʔbə</ta>
            <ta e="T59" id="Seg_6119" s="T58">ugaːndə</ta>
            <ta e="T60" id="Seg_6120" s="T59">urgo</ta>
            <ta e="T61" id="Seg_6121" s="T60">măja</ta>
            <ta e="T62" id="Seg_6122" s="T61">kădaʔ</ta>
            <ta e="T63" id="Seg_6123" s="T62">dĭ-Tə</ta>
            <ta e="T64" id="Seg_6124" s="T63">kan-zittə</ta>
            <ta e="T65" id="Seg_6125" s="T64">ej</ta>
            <ta e="T66" id="Seg_6126" s="T65">tĭmne-m</ta>
            <ta e="T67" id="Seg_6127" s="T66">măndo-liA-l</ta>
            <ta e="T68" id="Seg_6128" s="T67">tak</ta>
            <ta e="T69" id="Seg_6129" s="T68">bar</ta>
            <ta e="T72" id="Seg_6130" s="T71">saʔmə</ta>
            <ta e="T73" id="Seg_6131" s="T72">üžü</ta>
            <ta e="T74" id="Seg_6132" s="T73">saʔmə-luʔbdə-lV-j</ta>
            <ta e="T75" id="Seg_6133" s="T74">kan-bi-jəʔ</ta>
            <ta e="T76" id="Seg_6134" s="T75">tʼo</ta>
            <ta e="T77" id="Seg_6135" s="T76">tĭl-zittə</ta>
            <ta e="T80" id="Seg_6136" s="T78">aʔtʼi</ta>
            <ta e="T82" id="Seg_6137" s="T81">kan-bi-jəʔ</ta>
            <ta e="T83" id="Seg_6138" s="T82">tʼo</ta>
            <ta e="T84" id="Seg_6139" s="T83">tĭl-zittə</ta>
            <ta e="T85" id="Seg_6140" s="T84">aʔtʼi</ta>
            <ta e="T86" id="Seg_6141" s="T85">tʼezer-zittə</ta>
            <ta e="T87" id="Seg_6142" s="T86">gibər</ta>
            <ta e="T89" id="Seg_6143" s="T88">dĭ-zAŋ</ta>
            <ta e="T90" id="Seg_6144" s="T89">kan-bi-jəʔ</ta>
            <ta e="T91" id="Seg_6145" s="T90">kuŋgə-ŋ</ta>
            <ta e="T92" id="Seg_6146" s="T91">dʼok</ta>
            <ta e="T93" id="Seg_6147" s="T92">ej</ta>
            <ta e="T94" id="Seg_6148" s="T93">kuŋgə</ta>
            <ta e="T95" id="Seg_6149" s="T94">šo-bi-jəʔ</ta>
            <ta e="T96" id="Seg_6150" s="T95">ine-ziʔ-jəʔ</ta>
            <ta e="T97" id="Seg_6151" s="T96">ej</ta>
            <ta e="T98" id="Seg_6152" s="T97">kuŋgə-ŋ</ta>
            <ta e="T99" id="Seg_6153" s="T98">tura-Kən</ta>
            <ta e="T100" id="Seg_6154" s="T99">nu-bi-jəʔ</ta>
            <ta e="T101" id="Seg_6155" s="T100">daže</ta>
            <ta e="T102" id="Seg_6156" s="T101">ej</ta>
            <ta e="T103" id="Seg_6157" s="T102">surar-bi-jəʔ</ta>
            <ta e="T104" id="Seg_6158" s="T103">bar</ta>
            <ta e="T105" id="Seg_6159" s="T104">nu-bi-jəʔ</ta>
            <ta e="T106" id="Seg_6160" s="T105">kan-ə-ʔ</ta>
            <ta e="T107" id="Seg_6161" s="T106">măn</ta>
            <ta e="T108" id="Seg_6162" s="T107">tura-gəʔ</ta>
            <ta e="T109" id="Seg_6163" s="T108">i</ta>
            <ta e="T110" id="Seg_6164" s="T109">dön</ta>
            <ta e="T111" id="Seg_6165" s="T110">e-ʔ</ta>
            <ta e="T112" id="Seg_6166" s="T111">nu-ʔ</ta>
            <ta e="T113" id="Seg_6167" s="T112">ej</ta>
            <ta e="T114" id="Seg_6168" s="T113">kan-bi-jəʔ</ta>
            <ta e="T115" id="Seg_6169" s="T114">tura-Tə</ta>
            <ta e="T116" id="Seg_6170" s="T115">šaʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T117" id="Seg_6171" s="T116">i</ta>
            <ta e="T118" id="Seg_6172" s="T117">alom-liA-jəʔ</ta>
            <ta e="T119" id="Seg_6173" s="T118">bar</ta>
            <ta e="T120" id="Seg_6174" s="T119">tura-Tə</ta>
            <ta e="T121" id="Seg_6175" s="T120">nʼuʔdə</ta>
            <ta e="T122" id="Seg_6176" s="T121">bar</ta>
            <ta e="T123" id="Seg_6177" s="T122">süʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T124" id="Seg_6178" s="T123">sagən</ta>
            <ta e="T125" id="Seg_6179" s="T124">nüjü-jəʔ</ta>
            <ta e="T126" id="Seg_6180" s="T125">naga</ta>
            <ta e="T127" id="Seg_6181" s="T126">gibər=də</ta>
            <ta e="T128" id="Seg_6182" s="T127">kan-lAʔ</ta>
            <ta e="T129" id="Seg_6183" s="T128">tʼür-bi-jəʔ</ta>
            <ta e="T130" id="Seg_6184" s="T129">aľi</ta>
            <ta e="T131" id="Seg_6185" s="T130">kü-bi-jəʔ</ta>
            <ta e="T132" id="Seg_6186" s="T131">bar</ta>
            <ta e="T133" id="Seg_6187" s="T132">măna</ta>
            <ta e="T134" id="Seg_6188" s="T133">tože</ta>
            <ta e="T135" id="Seg_6189" s="T134">pi-ziʔ</ta>
            <ta e="T136" id="Seg_6190" s="T135">münör-laʔbə-lAʔ</ta>
            <ta e="T137" id="Seg_6191" s="T136">măna</ta>
            <ta e="T138" id="Seg_6192" s="T137">tože</ta>
            <ta e="T139" id="Seg_6193" s="T138">pi</ta>
            <ta e="T141" id="Seg_6194" s="T140">hen</ta>
            <ta e="T142" id="Seg_6195" s="T141">hen-lV-l</ta>
            <ta e="T143" id="Seg_6196" s="T142">il</ta>
            <ta e="T144" id="Seg_6197" s="T143">jakšə</ta>
            <ta e="T145" id="Seg_6198" s="T144">ej</ta>
            <ta e="T146" id="Seg_6199" s="T145">šʼaːm-liA-bAʔ</ta>
            <ta e="T147" id="Seg_6200" s="T146">jakšə</ta>
            <ta e="T149" id="Seg_6201" s="T148">tʼăbaktər-laʔbə-bAʔ</ta>
            <ta e="T150" id="Seg_6202" s="T149">šenap</ta>
            <ta e="T151" id="Seg_6203" s="T150">tʼăbaktər-laʔbə-m</ta>
            <ta e="T152" id="Seg_6204" s="T151">ej</ta>
            <ta e="T153" id="Seg_6205" s="T152">šʼaːm-liA-m</ta>
            <ta e="T154" id="Seg_6206" s="T153">kömə</ta>
            <ta e="T156" id="Seg_6207" s="T154">červə-jəʔ</ta>
            <ta e="T157" id="Seg_6208" s="T156">oʔbdə-bi-m</ta>
            <ta e="T158" id="Seg_6209" s="T157">bü-ziʔ</ta>
            <ta e="T159" id="Seg_6210" s="T158">dĭ-m</ta>
            <ta e="T161" id="Seg_6211" s="T160">bazə-bi-m</ta>
            <ta e="T162" id="Seg_6212" s="T161">takše-Tə</ta>
            <ta e="T163" id="Seg_6213" s="T162">hen-bi-m</ta>
            <ta e="T164" id="Seg_6214" s="T163">dĭbər</ta>
            <ta e="T165" id="Seg_6215" s="T164">ejü</ta>
            <ta e="T166" id="Seg_6216" s="T165">ejü</ta>
            <ta e="T167" id="Seg_6217" s="T166">nuldə-bi-m</ta>
            <ta e="T168" id="Seg_6218" s="T167">dĭbər</ta>
            <ta e="T169" id="Seg_6219" s="T168">bü</ta>
            <ta e="T170" id="Seg_6220" s="T169">mo-laːm-bi</ta>
            <ta e="T171" id="Seg_6221" s="T170">dĭgəttə</ta>
            <ta e="T172" id="Seg_6222" s="T171">uda-m</ta>
            <ta e="T173" id="Seg_6223" s="T172">kĭškə-bi-m</ta>
            <ta e="T174" id="Seg_6224" s="T173">uda-m</ta>
            <ta e="T175" id="Seg_6225" s="T174">ugaːndə</ta>
            <ta e="T176" id="Seg_6226" s="T175">tăŋ</ta>
            <ta e="T177" id="Seg_6227" s="T176">ĭzem-bi-jəʔ</ta>
            <ta e="T178" id="Seg_6228" s="T177">tüj</ta>
            <ta e="T179" id="Seg_6229" s="T178">ej</ta>
            <ta e="T180" id="Seg_6230" s="T179">ĭzem-liA-jəʔ</ta>
            <ta e="T181" id="Seg_6231" s="T180">smatri</ta>
            <ta e="T183" id="Seg_6232" s="T181">kudaj</ta>
            <ta e="T184" id="Seg_6233" s="T183">smatri</ta>
            <ta e="T185" id="Seg_6234" s="T184">kudaj</ta>
            <ta e="T186" id="Seg_6235" s="T185">e-ʔ</ta>
            <ta e="T187" id="Seg_6236" s="T186">nöməl-ə-ʔ</ta>
            <ta e="T192" id="Seg_6237" s="T191">kan-ə-ʔ</ta>
            <ta e="T193" id="Seg_6238" s="T192">eneidəne-Tə</ta>
            <ta e="T194" id="Seg_6239" s="T193">eneidəne-n</ta>
            <ta e="T196" id="Seg_6240" s="T195">amnu-t</ta>
            <ta e="T197" id="Seg_6241" s="T196">tüžöj-ə-n</ta>
            <ta e="T198" id="Seg_6242" s="T197">amnu-t</ta>
            <ta e="T199" id="Seg_6243" s="T198">dĭ</ta>
            <ta e="T200" id="Seg_6244" s="T199">bar</ta>
            <ta e="T201" id="Seg_6245" s="T200">tănan</ta>
            <ta e="T203" id="Seg_6246" s="T202">müʔbdə-lV-j</ta>
            <ta e="T204" id="Seg_6247" s="T203">sʼenʼi-jəʔ</ta>
            <ta e="T206" id="Seg_6248" s="T205">săbərəjʔdə-t</ta>
            <ta e="T207" id="Seg_6249" s="T206">ato</ta>
            <ta e="T208" id="Seg_6250" s="T207">ugaːndə</ta>
            <ta e="T209" id="Seg_6251" s="T208">iʔgö</ta>
            <ta e="T210" id="Seg_6252" s="T209">balgaš</ta>
            <ta e="T211" id="Seg_6253" s="T210">krilso</ta>
            <ta e="T212" id="Seg_6254" s="T211">săbərəjʔdə-t</ta>
            <ta e="T213" id="Seg_6255" s="T212">anbar-Tə</ta>
            <ta e="T214" id="Seg_6256" s="T213">un</ta>
            <ta e="T215" id="Seg_6257" s="T214">i-lV-m</ta>
            <ta e="T216" id="Seg_6258" s="T215">hen-bi-m</ta>
            <ta e="T217" id="Seg_6259" s="T216">măn</ta>
            <ta e="T218" id="Seg_6260" s="T217">dĭ-m</ta>
            <ta e="T219" id="Seg_6261" s="T218">kut-bi-m</ta>
            <ta e="T220" id="Seg_6262" s="T219">măn</ta>
            <ta e="T221" id="Seg_6263" s="T220">šăg-də</ta>
            <ta e="T222" id="Seg_6264" s="T221">iʔgö</ta>
            <ta e="T223" id="Seg_6265" s="T222">măn</ta>
            <ta e="T224" id="Seg_6266" s="T223">ugaːndə</ta>
            <ta e="T225" id="Seg_6267" s="T224">kuštü</ta>
            <ta e="T226" id="Seg_6268" s="T225">i-gA-m</ta>
            <ta e="T227" id="Seg_6269" s="T226">dĭ</ta>
            <ta e="T228" id="Seg_6270" s="T227">kuza</ta>
            <ta e="T229" id="Seg_6271" s="T228">bar</ta>
            <ta e="T230" id="Seg_6272" s="T229">sagəš-də</ta>
            <ta e="T231" id="Seg_6273" s="T230">naga</ta>
            <ta e="T232" id="Seg_6274" s="T231">šĭkə-t</ta>
            <ta e="T233" id="Seg_6275" s="T232">i-gA</ta>
            <ta e="T234" id="Seg_6276" s="T233">tʼăbaktər-zittə</ta>
            <ta e="T235" id="Seg_6277" s="T234">ej</ta>
            <ta e="T236" id="Seg_6278" s="T235">mo-liA</ta>
            <ta e="T237" id="Seg_6279" s="T236">ku-t</ta>
            <ta e="T238" id="Seg_6280" s="T237">i-gA</ta>
            <ta e="T239" id="Seg_6281" s="T238">ej</ta>
            <ta e="T240" id="Seg_6282" s="T239">nünə-liA</ta>
            <ta e="T241" id="Seg_6283" s="T240">e-ʔ</ta>
            <ta e="T242" id="Seg_6284" s="T241">parlo-ʔ</ta>
            <ta e="T243" id="Seg_6285" s="T242">jakšə</ta>
            <ta e="T244" id="Seg_6286" s="T243">amnə-ʔ</ta>
            <ta e="T245" id="Seg_6287" s="T244">ĭmbi</ta>
            <ta e="T246" id="Seg_6288" s="T245">parlo-laʔbə-l</ta>
            <ta e="T247" id="Seg_6289" s="T246">bos-də</ta>
            <ta e="T248" id="Seg_6290" s="T247">šĭkə-m</ta>
            <ta e="T249" id="Seg_6291" s="T248">nünə-liA-m</ta>
            <ta e="T250" id="Seg_6292" s="T249">jakšə</ta>
            <ta e="T251" id="Seg_6293" s="T250">tʼăbaktər-liA-m</ta>
            <ta e="T252" id="Seg_6294" s="T251">šoška</ta>
            <ta e="T253" id="Seg_6295" s="T252">šonə-gA</ta>
            <ta e="T254" id="Seg_6296" s="T253">i</ta>
            <ta e="T255" id="Seg_6297" s="T254">kurem-laʔbə</ta>
            <ta e="T256" id="Seg_6298" s="T255">bar</ta>
            <ta e="T257" id="Seg_6299" s="T256">nadə</ta>
            <ta e="T258" id="Seg_6300" s="T257">dĭ-m</ta>
            <ta e="T259" id="Seg_6301" s="T258">bădə-zittə</ta>
            <ta e="T260" id="Seg_6302" s="T259">ato</ta>
            <ta e="T261" id="Seg_6303" s="T260">dĭ</ta>
            <ta e="T262" id="Seg_6304" s="T261">püjö-liA</ta>
            <ta e="T263" id="Seg_6305" s="T262">bar</ta>
            <ta e="T264" id="Seg_6306" s="T263">nadə</ta>
            <ta e="T266" id="Seg_6307" s="T265">baltu</ta>
            <ta e="T267" id="Seg_6308" s="T266">puʔmə-zittə</ta>
            <ta e="T268" id="Seg_6309" s="T267">pa-jəʔ</ta>
            <ta e="T273" id="Seg_6310" s="T272">hʼaʔ-zittə</ta>
            <ta e="T274" id="Seg_6311" s="T273">pʼeːš</ta>
            <ta e="T275" id="Seg_6312" s="T274">nen-zittə</ta>
            <ta e="T276" id="Seg_6313" s="T275">pʼila</ta>
            <ta e="T277" id="Seg_6314" s="T276">det-bi-jəʔ</ta>
            <ta e="T278" id="Seg_6315" s="T277">ej</ta>
            <ta e="T279" id="Seg_6316" s="T278">tĭmne-jəʔ</ta>
            <ta e="T280" id="Seg_6317" s="T279">ĭmbi</ta>
            <ta e="T281" id="Seg_6318" s="T280">dĭ-ziʔ</ta>
            <ta e="T282" id="Seg_6319" s="T281">a-zittə</ta>
            <ta e="T284" id="Seg_6320" s="T283">Varlam</ta>
            <ta e="T285" id="Seg_6321" s="T284">šo-lV-j</ta>
            <ta e="T286" id="Seg_6322" s="T285">tʼezer-lV-j</ta>
            <ta e="T287" id="Seg_6323" s="T286">baza-j</ta>
            <ta e="T288" id="Seg_6324" s="T287">pʼeːš</ta>
            <ta e="T289" id="Seg_6325" s="T288">det-bi-jəʔ</ta>
            <ta e="T290" id="Seg_6326" s="T289">tura-Tə</ta>
            <ta e="T291" id="Seg_6327" s="T290">nuldə-bi-jəʔ</ta>
            <ta e="T294" id="Seg_6328" s="T293">šojdʼo-Tə</ta>
            <ta e="T295" id="Seg_6329" s="T294">pasə</ta>
            <ta e="T296" id="Seg_6330" s="T295">šojdʼo</ta>
            <ta e="T297" id="Seg_6331" s="T296">dĭ</ta>
            <ta e="T298" id="Seg_6332" s="T297">bar</ta>
            <ta e="T299" id="Seg_6333" s="T298">nend-luʔbdə-bi</ta>
            <ta e="T300" id="Seg_6334" s="T299">bor</ta>
            <ta e="T301" id="Seg_6335" s="T300">šo-bi</ta>
            <ta e="T302" id="Seg_6336" s="T301">dĭgəttə</ta>
            <ta e="T303" id="Seg_6337" s="T302">bar</ta>
            <ta e="T304" id="Seg_6338" s="T303">öʔ-bi-jəʔ</ta>
            <ta e="T305" id="Seg_6339" s="T304">nʼiʔdə</ta>
            <ta e="T306" id="Seg_6340" s="T305">ugaːndə</ta>
            <ta e="T307" id="Seg_6341" s="T306">bor</ta>
            <ta e="T308" id="Seg_6342" s="T307">iʔgö</ta>
            <ta e="T309" id="Seg_6343" s="T308">Varlam</ta>
            <ta e="T310" id="Seg_6344" s="T309">šo-lV-j</ta>
            <ta e="T311" id="Seg_6345" s="T310">i</ta>
            <ta e="T312" id="Seg_6346" s="T311">tʼezer-lV-j</ta>
            <ta e="T313" id="Seg_6347" s="T312">bar</ta>
            <ta e="T314" id="Seg_6348" s="T313">bor</ta>
            <ta e="T315" id="Seg_6349" s="T314">sima-m</ta>
            <ta e="T316" id="Seg_6350" s="T315">amnə-laʔbə-jəʔ</ta>
            <ta e="T317" id="Seg_6351" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_6352" s="T317">Varlam</ta>
            <ta e="T319" id="Seg_6353" s="T318">šo-bi</ta>
            <ta e="T320" id="Seg_6354" s="T319">pi</ta>
            <ta e="T321" id="Seg_6355" s="T320">det-bi</ta>
            <ta e="T322" id="Seg_6356" s="T321">tʼo-Tə</ta>
            <ta e="T323" id="Seg_6357" s="T322">hen-bi</ta>
            <ta e="T324" id="Seg_6358" s="T323">i</ta>
            <ta e="T325" id="Seg_6359" s="T324">pʼeːš-də</ta>
            <ta e="T326" id="Seg_6360" s="T325">nuldə-bi</ta>
            <ta e="T327" id="Seg_6361" s="T326">dĭgəttə</ta>
            <ta e="T328" id="Seg_6362" s="T327">pʼeːš</ta>
            <ta e="T329" id="Seg_6363" s="T328">nendə-bi-jəʔ</ta>
            <ta e="T330" id="Seg_6364" s="T329">amno-laʔbə-jəʔ</ta>
            <ta e="T331" id="Seg_6365" s="T330">i</ta>
            <ta e="T333" id="Seg_6366" s="T332">ejü-laʔbə-jəʔ</ta>
            <ta e="T339" id="Seg_6367" s="T338">parga-m</ta>
            <ta e="T340" id="Seg_6368" s="T339">bar</ta>
            <ta e="T341" id="Seg_6369" s="T340">tʼo-Kən</ta>
            <ta e="T342" id="Seg_6370" s="T341">tʼo-Kən</ta>
            <ta e="T343" id="Seg_6371" s="T342">iʔbö-bi</ta>
            <ta e="T344" id="Seg_6372" s="T343">üjü-jəʔ</ta>
            <ta e="T345" id="Seg_6373" s="T344">bar</ta>
            <ta e="T346" id="Seg_6374" s="T345">kĭškə-bi-jəʔ</ta>
            <ta e="T347" id="Seg_6375" s="T346">dĭn</ta>
            <ta e="T348" id="Seg_6376" s="T347">dĭn</ta>
            <ta e="T349" id="Seg_6377" s="T348">bar</ta>
            <ta e="T350" id="Seg_6378" s="T349">balgaš</ta>
            <ta e="T351" id="Seg_6379" s="T350">măn</ta>
            <ta e="T352" id="Seg_6380" s="T351">dĭ-m</ta>
            <ta e="T353" id="Seg_6381" s="T352">i-bi-l</ta>
            <ta e="T354" id="Seg_6382" s="T353">i-bi-m</ta>
            <ta e="T355" id="Seg_6383" s="T354">da</ta>
            <ta e="T356" id="Seg_6384" s="T355">bü-Tə</ta>
            <ta e="T357" id="Seg_6385" s="T356">kun-bi-l</ta>
            <ta e="T358" id="Seg_6386" s="T357">kun-bi-m</ta>
            <ta e="T359" id="Seg_6387" s="T358">štobɨ</ta>
            <ta e="T360" id="Seg_6388" s="T359">dʼüʔpi</ta>
            <ta e="T361" id="Seg_6389" s="T360">mo-bi</ta>
            <ta e="T362" id="Seg_6390" s="T361">dĭgəttə</ta>
            <ta e="T363" id="Seg_6391" s="T362">toʔbdə-nar-bi-m</ta>
            <ta e="T364" id="Seg_6392" s="T363">bazə-bi-m</ta>
            <ta e="T365" id="Seg_6393" s="T364">dĭgəttə</ta>
            <ta e="T366" id="Seg_6394" s="T365">edə-bi-m</ta>
            <ta e="T367" id="Seg_6395" s="T366">bü</ta>
            <ta e="T368" id="Seg_6396" s="T367">bar</ta>
            <ta e="T369" id="Seg_6397" s="T368">mʼaŋ-laʔbə</ta>
            <ta e="T370" id="Seg_6398" s="T369">dĭ-gəʔ</ta>
            <ta e="T371" id="Seg_6399" s="T370">tʼăga</ta>
            <ta e="T372" id="Seg_6400" s="T371">mʼaŋ-laʔbə</ta>
            <ta e="T373" id="Seg_6401" s="T372">i</ta>
            <ta e="T374" id="Seg_6402" s="T373">kuza</ta>
            <ta e="T375" id="Seg_6403" s="T374">nuʔmə-laʔbə</ta>
            <ta e="T376" id="Seg_6404" s="T375">bü-j-lAʔ</ta>
            <ta e="T377" id="Seg_6405" s="T376">girgit=də</ta>
            <ta e="T378" id="Seg_6406" s="T377">il</ta>
            <ta e="T379" id="Seg_6407" s="T378">mĭn-laʔbə-jəʔ</ta>
            <ta e="T380" id="Seg_6408" s="T379">măn</ta>
            <ta e="T381" id="Seg_6409" s="T380">dĭ-m</ta>
            <ta e="T382" id="Seg_6410" s="T381">dĭ-zem</ta>
            <ta e="T383" id="Seg_6411" s="T382">ej</ta>
            <ta e="T384" id="Seg_6412" s="T383">tĭmne-m</ta>
            <ta e="T385" id="Seg_6413" s="T384">ĭmbi</ta>
            <ta e="T387" id="Seg_6414" s="T386">dĭ-zAŋ</ta>
            <ta e="T388" id="Seg_6415" s="T387">dĭn</ta>
            <ta e="T389" id="Seg_6416" s="T388">mĭn-laʔbə-jəʔ</ta>
            <ta e="T390" id="Seg_6417" s="T389">da</ta>
            <ta e="T391" id="Seg_6418" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_6419" s="T391">nʼi</ta>
            <ta e="T393" id="Seg_6420" s="T392">da</ta>
            <ta e="T394" id="Seg_6421" s="T393">koʔbdo</ta>
            <ta e="T395" id="Seg_6422" s="T394">šonə-gA-jəʔ</ta>
            <ta e="T396" id="Seg_6423" s="T395">nadə</ta>
            <ta e="T397" id="Seg_6424" s="T396">süt</ta>
            <ta e="T398" id="Seg_6425" s="T397">kămnə-zittə</ta>
            <ta e="T399" id="Seg_6426" s="T398">šojdʼo-Tə</ta>
            <ta e="T400" id="Seg_6427" s="T399">puskaj</ta>
            <ta e="T401" id="Seg_6428" s="T400">namzəga</ta>
            <ta e="T402" id="Seg_6429" s="T401">mo-lV-j</ta>
            <ta e="T403" id="Seg_6430" s="T402">tüjö</ta>
            <ta e="T404" id="Seg_6431" s="T403">kan-lAʔ</ta>
            <ta e="T406" id="Seg_6432" s="T405">tʼür-bi-bAʔ</ta>
            <ta e="T407" id="Seg_6433" s="T406">no</ta>
            <ta e="T408" id="Seg_6434" s="T407">kan-ə-ʔ</ta>
            <ta e="T409" id="Seg_6435" s="T408">kan-KAʔ</ta>
            <ta e="T410" id="Seg_6436" s="T409">kudaj-ziʔ</ta>
            <ta e="T411" id="Seg_6437" s="T410">miʔ</ta>
            <ta e="T412" id="Seg_6438" s="T411">ugaːndə</ta>
            <ta e="T413" id="Seg_6439" s="T412">iʔgö</ta>
            <ta e="T414" id="Seg_6440" s="T413">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T415" id="Seg_6441" s="T414">tüj</ta>
            <ta e="T416" id="Seg_6442" s="T415">kan-zittə</ta>
            <ta e="T417" id="Seg_6443" s="T416">nadə</ta>
            <ta e="T418" id="Seg_6444" s="T417">maʔ-gəndə</ta>
            <ta e="T419" id="Seg_6445" s="T418">da</ta>
            <ta e="T420" id="Seg_6446" s="T419">kunol-zittə</ta>
            <ta e="T421" id="Seg_6447" s="T420">iʔbö-zittə</ta>
            <ta e="T422" id="Seg_6448" s="T421">e-ʔ</ta>
            <ta e="T424" id="Seg_6449" s="T423">e-ʔ</ta>
            <ta e="T425" id="Seg_6450" s="T424">makta-nar-ə-ʔ</ta>
            <ta e="T426" id="Seg_6451" s="T425">ĭmbi=də</ta>
            <ta e="T427" id="Seg_6452" s="T426">ej</ta>
            <ta e="T428" id="Seg_6453" s="T427">a-bi-l</ta>
            <ta e="T429" id="Seg_6454" s="T428">a</ta>
            <ta e="T430" id="Seg_6455" s="T429">bar</ta>
            <ta e="T431" id="Seg_6456" s="T430">makta-nar-bi-l</ta>
            <ta e="T432" id="Seg_6457" s="T431">kunol-zittə</ta>
            <ta e="T433" id="Seg_6458" s="T432">iʔbö-šte</ta>
            <ta e="T434" id="Seg_6459" s="T433">a</ta>
            <ta e="T435" id="Seg_6460" s="T434">karəldʼaːn</ta>
            <ta e="T436" id="Seg_6461" s="T435">sagəš-Tə</ta>
            <ta e="T437" id="Seg_6462" s="T436">šo-lV-j</ta>
            <ta e="T438" id="Seg_6463" s="T437">kuznek-Tə</ta>
            <ta e="T439" id="Seg_6464" s="T438">kuza</ta>
            <ta e="T440" id="Seg_6465" s="T439">šo-bi</ta>
            <ta e="T441" id="Seg_6466" s="T440">a</ta>
            <ta e="T442" id="Seg_6467" s="T441">măn</ta>
            <ta e="T443" id="Seg_6468" s="T442">pi</ta>
            <ta e="T444" id="Seg_6469" s="T443">kabar-luʔbdə-bi-m</ta>
            <ta e="T449" id="Seg_6470" s="T448">pi</ta>
            <ta e="T450" id="Seg_6471" s="T449">kabar-luʔbdə-bi-m</ta>
            <ta e="T451" id="Seg_6472" s="T450">i</ta>
            <ta e="T452" id="Seg_6473" s="T451">kut-bi-m</ta>
            <ta e="T453" id="Seg_6474" s="T452">dĭ</ta>
            <ta e="T454" id="Seg_6475" s="T453">kuza-m</ta>
            <ta e="T455" id="Seg_6476" s="T454">kuza</ta>
            <ta e="T456" id="Seg_6477" s="T455">kuza-m</ta>
            <ta e="T457" id="Seg_6478" s="T456">bar</ta>
            <ta e="T458" id="Seg_6479" s="T457">tagaj-ziʔ</ta>
            <ta e="T459" id="Seg_6480" s="T458">dʼagar-luʔbdə-bi</ta>
            <ta e="T460" id="Seg_6481" s="T459">i</ta>
            <ta e="T461" id="Seg_6482" s="T460">dĭ</ta>
            <ta e="T462" id="Seg_6483" s="T461">kü-laːm-bi</ta>
            <ta e="T463" id="Seg_6484" s="T462">dĭn</ta>
            <ta e="T464" id="Seg_6485" s="T463">onʼiʔ</ta>
            <ta e="T465" id="Seg_6486" s="T464">ne</ta>
            <ta e="T466" id="Seg_6487" s="T465">Kazan</ta>
            <ta e="T467" id="Seg_6488" s="T466">tura-Kən</ta>
            <ta e="T468" id="Seg_6489" s="T467">onʼiʔ</ta>
            <ta e="T469" id="Seg_6490" s="T468">ne</ta>
            <ta e="T470" id="Seg_6491" s="T469">tibi-bə</ta>
            <ta e="T471" id="Seg_6492" s="T470">bar</ta>
            <ta e="T472" id="Seg_6493" s="T471">hʼaʔ-bi</ta>
            <ta e="T473" id="Seg_6494" s="T472">baltu-ziʔ</ta>
            <ta e="T474" id="Seg_6495" s="T473">ulu-t</ta>
            <ta e="T475" id="Seg_6496" s="T474">săj-hʼaʔ-bi</ta>
            <ta e="T476" id="Seg_6497" s="T475">dĭ-n</ta>
            <ta e="T477" id="Seg_6498" s="T476">tibi</ta>
            <ta e="T478" id="Seg_6499" s="T477">onʼiʔ</ta>
            <ta e="T479" id="Seg_6500" s="T478">kunol-luʔbdə-bi</ta>
            <ta e="T480" id="Seg_6501" s="T479">bar</ta>
            <ta e="T481" id="Seg_6502" s="T480">ugaːndə</ta>
            <ta e="T483" id="Seg_6503" s="T482">pim-bi-m</ta>
            <ta e="T484" id="Seg_6504" s="T483">kădaʔ</ta>
            <ta e="T485" id="Seg_6505" s="T484">măna</ta>
            <ta e="T486" id="Seg_6506" s="T485">ulu-m</ta>
            <ta e="T487" id="Seg_6507" s="T486">ej</ta>
            <ta e="T488" id="Seg_6508" s="T487">hʼaʔ-bi</ta>
            <ta e="T489" id="Seg_6509" s="T488">dĭ</ta>
            <ta e="T490" id="Seg_6510" s="T489">bar</ta>
            <ta e="T491" id="Seg_6511" s="T490">nanəʔzəbi</ta>
            <ta e="T492" id="Seg_6512" s="T491">i-bi</ta>
            <ta e="T493" id="Seg_6513" s="T492">i</ta>
            <ta e="T494" id="Seg_6514" s="T493">šide</ta>
            <ta e="T495" id="Seg_6515" s="T494">ešši-zAŋ</ta>
            <ta e="T496" id="Seg_6516" s="T495">ma-luʔbdə-bi-jəʔ</ta>
            <ta e="T500" id="Seg_6517" s="T499">bar</ta>
            <ta e="T501" id="Seg_6518" s="T500">sumna</ta>
            <ta e="T502" id="Seg_6519" s="T501">pʼe</ta>
            <ta e="T503" id="Seg_6520" s="T502">kan-lAʔ</ta>
            <ta e="T504" id="Seg_6521" s="T503">tʼür-bi</ta>
            <ta e="T505" id="Seg_6522" s="T504">taldʼen</ta>
            <ta e="T506" id="Seg_6523" s="T505">ej</ta>
            <ta e="T507" id="Seg_6524" s="T506">i-bi-l</ta>
            <ta e="T508" id="Seg_6525" s="T507">teinen</ta>
            <ta e="T511" id="Seg_6526" s="T510">šo-bi-l</ta>
            <ta e="T512" id="Seg_6527" s="T511">nʼilgö-zittə</ta>
            <ta e="T513" id="Seg_6528" s="T512">ĭmbi</ta>
            <ta e="T514" id="Seg_6529" s="T513">tʼăbaktər-laʔbə-bAʔ</ta>
            <ta e="T515" id="Seg_6530" s="T514">măn</ta>
            <ta e="T516" id="Seg_6531" s="T515">teinen</ta>
            <ta e="T517" id="Seg_6532" s="T516">kudaj</ta>
            <ta e="T518" id="Seg_6533" s="T517">num-Tə</ta>
            <ta e="T521" id="Seg_6534" s="T520">üzə-lV-m</ta>
            <ta e="T522" id="Seg_6535" s="T521">kudaj</ta>
            <ta e="T523" id="Seg_6536" s="T522">šo-ʔ</ta>
            <ta e="T524" id="Seg_6537" s="T523">măna</ta>
            <ta e="T525" id="Seg_6538" s="T524">nʼilgö-t</ta>
            <ta e="T526" id="Seg_6539" s="T525">kădaʔ</ta>
            <ta e="T527" id="Seg_6540" s="T526">măn</ta>
            <ta e="T528" id="Seg_6541" s="T527">üzə-liA-m</ta>
            <ta e="T529" id="Seg_6542" s="T528">sĭj-m</ta>
            <ta e="T531" id="Seg_6543" s="T530">măndo-ʔ</ta>
            <ta e="T532" id="Seg_6544" s="T531">padʼi</ta>
            <ta e="T534" id="Seg_6545" s="T533">arə-liA-jəʔ</ta>
            <ta e="T535" id="Seg_6546" s="T534">a</ta>
            <ta e="T536" id="Seg_6547" s="T535">ej</ta>
            <ta e="T537" id="Seg_6548" s="T536">arə</ta>
            <ta e="T538" id="Seg_6549" s="T537">tak</ta>
            <ta e="T539" id="Seg_6550" s="T538">bazə-t</ta>
            <ta e="T540" id="Seg_6551" s="T539">kem-ziʔ</ta>
            <ta e="T541" id="Seg_6552" s="T540">tăn</ta>
            <ta e="T542" id="Seg_6553" s="T541">pa-Kən</ta>
            <ta e="T543" id="Seg_6554" s="T542">edə-bi-l</ta>
            <ta e="T544" id="Seg_6555" s="T543">kem-m</ta>
            <ta e="T545" id="Seg_6556" s="T544">mʼaŋ-laʔpi</ta>
            <ta e="T546" id="Seg_6557" s="T545">bazə-t</ta>
            <ta e="T547" id="Seg_6558" s="T546">măna</ta>
            <ta e="T548" id="Seg_6559" s="T547">jakšə</ta>
            <ta e="T549" id="Seg_6560" s="T548">bazə-t</ta>
            <ta e="T550" id="Seg_6561" s="T549">tăn</ta>
            <ta e="T551" id="Seg_6562" s="T550">kudaj</ta>
            <ta e="T552" id="Seg_6563" s="T551">šal-l</ta>
            <ta e="T553" id="Seg_6564" s="T552">iʔgö</ta>
            <ta e="T554" id="Seg_6565" s="T553">bar</ta>
            <ta e="T556" id="Seg_6566" s="T555">il-ə-m</ta>
            <ta e="T558" id="Seg_6567" s="T557">ajir-laʔbə-l</ta>
            <ta e="T560" id="Seg_6568" s="T559">ipek</ta>
            <ta e="T561" id="Seg_6569" s="T560">mĭ-liA-l</ta>
            <ta e="T563" id="Seg_6570" s="T562">tăn</ta>
            <ta e="T564" id="Seg_6571" s="T563">kudaj</ta>
            <ta e="T565" id="Seg_6572" s="T564">šal-l</ta>
            <ta e="T566" id="Seg_6573" s="T565">iʔgö</ta>
            <ta e="T567" id="Seg_6574" s="T566">tăn</ta>
            <ta e="T568" id="Seg_6575" s="T567">il</ta>
            <ta e="T569" id="Seg_6576" s="T568">il</ta>
            <ta e="T570" id="Seg_6577" s="T569">il</ta>
            <ta e="T571" id="Seg_6578" s="T570">ajir-liA-l</ta>
            <ta e="T572" id="Seg_6579" s="T571">ipek</ta>
            <ta e="T573" id="Seg_6580" s="T572">dĭ-zAŋ-Tə</ta>
            <ta e="T574" id="Seg_6581" s="T573">mĭ-liA-l</ta>
            <ta e="T575" id="Seg_6582" s="T574">bar</ta>
            <ta e="T576" id="Seg_6583" s="T575">kujnek</ta>
            <ta e="T577" id="Seg_6584" s="T576">mĭ-liA-l</ta>
            <ta e="T578" id="Seg_6585" s="T577">bü</ta>
            <ta e="T579" id="Seg_6586" s="T578">mĭ-liA-l</ta>
            <ta e="T580" id="Seg_6587" s="T579">i</ta>
            <ta e="T581" id="Seg_6588" s="T580">măna</ta>
            <ta e="T582" id="Seg_6589" s="T581">ajir-ə-ʔ</ta>
            <ta e="T588" id="Seg_6590" s="T587">tăn</ta>
            <ta e="T589" id="Seg_6591" s="T588">bar</ta>
            <ta e="T590" id="Seg_6592" s="T589">ejü</ta>
            <ta e="T591" id="Seg_6593" s="T590">öʔ-liA-l</ta>
            <ta e="T592" id="Seg_6594" s="T591">surno</ta>
            <ta e="T593" id="Seg_6595" s="T592">mĭ-liA-l</ta>
            <ta e="T594" id="Seg_6596" s="T593">štobɨ</ta>
            <ta e="T595" id="Seg_6597" s="T594">ipek</ta>
            <ta e="T596" id="Seg_6598" s="T595">özer-laʔpi</ta>
            <ta e="T600" id="Seg_6599" s="T599">kudaj-ziʔ</ta>
            <ta e="T601" id="Seg_6600" s="T600">jakšə</ta>
            <ta e="T602" id="Seg_6601" s="T601">gibər</ta>
            <ta e="T603" id="Seg_6602" s="T602">kan-lV-l</ta>
            <ta e="T604" id="Seg_6603" s="T603">kudaj-ziʔ</ta>
            <ta e="T607" id="Seg_6604" s="T606">ugaːndə</ta>
            <ta e="T608" id="Seg_6605" s="T607">jakšə</ta>
            <ta e="T609" id="Seg_6606" s="T608">kudaj-ziʔ</ta>
            <ta e="T610" id="Seg_6607" s="T609">beržə</ta>
            <ta e="T611" id="Seg_6608" s="T610">saʔmə-luʔbdə-bi</ta>
            <ta e="T612" id="Seg_6609" s="T611">măn</ta>
            <ta e="T613" id="Seg_6610" s="T612">i-bi-m</ta>
            <ta e="T614" id="Seg_6611" s="T613">da</ta>
            <ta e="T615" id="Seg_6612" s="T614">tura-Tə</ta>
            <ta e="T616" id="Seg_6613" s="T615">det-bi-m</ta>
            <ta e="T617" id="Seg_6614" s="T616">jakšə</ta>
            <ta e="T618" id="Seg_6615" s="T617">surno</ta>
            <ta e="T619" id="Seg_6616" s="T618">šonə-gA</ta>
            <ta e="T620" id="Seg_6617" s="T619">ipek</ta>
            <ta e="T621" id="Seg_6618" s="T620">özer-lV-j</ta>
            <ta e="T622" id="Seg_6619" s="T621">măn</ta>
            <ta e="T623" id="Seg_6620" s="T622">dĭ-Tə</ta>
            <ta e="T624" id="Seg_6621" s="T623">kabažar-bi-m</ta>
            <ta e="T625" id="Seg_6622" s="T624">a</ta>
            <ta e="T626" id="Seg_6623" s="T625">dĭ</ta>
            <ta e="T627" id="Seg_6624" s="T626">măna</ta>
            <ta e="T628" id="Seg_6625" s="T627">ej</ta>
            <ta e="T629" id="Seg_6626" s="T628">kabažar-laʔbə</ta>
            <ta e="T630" id="Seg_6627" s="T629">ej</ta>
            <ta e="T631" id="Seg_6628" s="T630">mĭ-liA</ta>
            <ta e="T632" id="Seg_6629" s="T631">măna</ta>
            <ta e="T633" id="Seg_6630" s="T632">togonər-zittə</ta>
            <ta e="T634" id="Seg_6631" s="T633">măn</ta>
            <ta e="T635" id="Seg_6632" s="T634">bɨ</ta>
            <ta e="T636" id="Seg_6633" s="T635">togonər-bi-m</ta>
            <ta e="T637" id="Seg_6634" s="T636">a</ta>
            <ta e="T638" id="Seg_6635" s="T637">dĭ</ta>
            <ta e="T639" id="Seg_6636" s="T638">üge</ta>
            <ta e="T640" id="Seg_6637" s="T639">puʔ-laʔbə</ta>
            <ta e="T641" id="Seg_6638" s="T640">măna</ta>
            <ta e="T642" id="Seg_6639" s="T641">kurojok</ta>
            <ta e="T643" id="Seg_6640" s="T642">bar</ta>
            <ta e="T644" id="Seg_6641" s="T643">üjü-ziʔ</ta>
            <ta e="T645" id="Seg_6642" s="T644">bar</ta>
            <ta e="T646" id="Seg_6643" s="T645">tonuʔ-bi</ta>
            <ta e="T647" id="Seg_6644" s="T646">ugaːndə</ta>
            <ta e="T648" id="Seg_6645" s="T647">kurojok</ta>
            <ta e="T649" id="Seg_6646" s="T648">men</ta>
            <ta e="T651" id="Seg_6647" s="T650">bar</ta>
            <ta e="T653" id="Seg_6648" s="T652">talbə-liA</ta>
            <ta e="T654" id="Seg_6649" s="T653">il</ta>
            <ta e="T657" id="Seg_6650" s="T656">ugaːndə</ta>
            <ta e="T658" id="Seg_6651" s="T657">kurojok</ta>
            <ta e="T659" id="Seg_6652" s="T658">men</ta>
            <ta e="T661" id="Seg_6653" s="T660">bar</ta>
            <ta e="T662" id="Seg_6654" s="T661">il</ta>
            <ta e="T663" id="Seg_6655" s="T662">talbə-r-laʔbə</ta>
            <ta e="T664" id="Seg_6656" s="T663">i</ta>
            <ta e="T665" id="Seg_6657" s="T664">măna</ta>
            <ta e="T666" id="Seg_6658" s="T665">talbə-r-bi</ta>
            <ta e="T667" id="Seg_6659" s="T666">măn</ta>
            <ta e="T668" id="Seg_6660" s="T667">šiʔ</ta>
            <ta e="T669" id="Seg_6661" s="T668">edəʔ-bi-m</ta>
            <ta e="T670" id="Seg_6662" s="T669">šiʔ</ta>
            <ta e="T671" id="Seg_6663" s="T670">ej</ta>
            <ta e="T672" id="Seg_6664" s="T671">šo-bi-lAʔ</ta>
            <ta e="T673" id="Seg_6665" s="T672">kondʼo</ta>
            <ta e="T674" id="Seg_6666" s="T673">naga-bi-lAʔ</ta>
            <ta e="T675" id="Seg_6667" s="T674">takše-gəʔ</ta>
            <ta e="T676" id="Seg_6668" s="T675">i-bi-m</ta>
            <ta e="T677" id="Seg_6669" s="T676">bazə-bi-m</ta>
            <ta e="T678" id="Seg_6670" s="T677">i</ta>
            <ta e="T679" id="Seg_6671" s="T678">bazoʔ</ta>
            <ta e="T680" id="Seg_6672" s="T679">hen-bi-m</ta>
            <ta e="T681" id="Seg_6673" s="T680">teinöbam</ta>
            <ta e="T682" id="Seg_6674" s="T681">teinöbam</ta>
            <ta e="T683" id="Seg_6675" s="T682">teinen</ta>
            <ta e="T684" id="Seg_6676" s="T683">bar</ta>
            <ta e="T687" id="Seg_6677" s="T686">bü-Tə</ta>
            <ta e="T688" id="Seg_6678" s="T687">ej</ta>
            <ta e="T689" id="Seg_6679" s="T688">saʔmə-liA-l</ta>
            <ta e="T690" id="Seg_6680" s="T689">nʼuʔdun</ta>
            <ta e="T691" id="Seg_6681" s="T690">bar</ta>
            <ta e="T692" id="Seg_6682" s="T691">mĭn-KV</ta>
            <ta e="T693" id="Seg_6683" s="T692">uda-ziʔ</ta>
            <ta e="T694" id="Seg_6684" s="T693">i</ta>
            <ta e="T695" id="Seg_6685" s="T694">üjü-ziʔ</ta>
            <ta e="T696" id="Seg_6686" s="T695">bü-Kən</ta>
            <ta e="T697" id="Seg_6687" s="T696">bar</ta>
            <ta e="T698" id="Seg_6688" s="T697">kandə-gA-l</ta>
            <ta e="T699" id="Seg_6689" s="T698">uda-ziʔ</ta>
            <ta e="T700" id="Seg_6690" s="T699">i</ta>
            <ta e="T701" id="Seg_6691" s="T700">üjü-ziʔ</ta>
            <ta e="T702" id="Seg_6692" s="T701">lotka</ta>
            <ta e="T703" id="Seg_6693" s="T702">lotka-jəʔ-ziʔ</ta>
            <ta e="T704" id="Seg_6694" s="T703">bar</ta>
            <ta e="T706" id="Seg_6695" s="T705">bü-Tə</ta>
            <ta e="T707" id="Seg_6696" s="T706">kandə-gA</ta>
            <ta e="T708" id="Seg_6697" s="T707">kola</ta>
            <ta e="T709" id="Seg_6698" s="T708">dʼabə-laʔbə</ta>
            <ta e="T710" id="Seg_6699" s="T709">bar</ta>
            <ta e="T711" id="Seg_6700" s="T710">nagur-göʔ</ta>
            <ta e="T712" id="Seg_6701" s="T711">tʼakam-lV-bAʔ</ta>
            <ta e="T713" id="Seg_6702" s="T712">onʼiʔ</ta>
            <ta e="T714" id="Seg_6703" s="T713">tănan</ta>
            <ta e="T715" id="Seg_6704" s="T714">onʼiʔ</ta>
            <ta e="T716" id="Seg_6705" s="T715">măna</ta>
            <ta e="T717" id="Seg_6706" s="T716">onʼiʔ</ta>
            <ta e="T718" id="Seg_6707" s="T717">dĭ-Tə</ta>
            <ta e="T719" id="Seg_6708" s="T718">šide</ta>
            <ta e="T720" id="Seg_6709" s="T719">pʼel</ta>
            <ta e="T721" id="Seg_6710" s="T720">nagur</ta>
            <ta e="T722" id="Seg_6711" s="T721">pʼel</ta>
            <ta e="T723" id="Seg_6712" s="T722">teʔdə</ta>
            <ta e="T724" id="Seg_6713" s="T723">pʼel</ta>
            <ta e="T725" id="Seg_6714" s="T724">dăra</ta>
            <ta e="T726" id="Seg_6715" s="T725">dĭ</ta>
            <ta e="T727" id="Seg_6716" s="T726">dĭ-zAŋ</ta>
            <ta e="T728" id="Seg_6717" s="T727">bar</ta>
            <ta e="T729" id="Seg_6718" s="T728">amnə-bi-jəʔ</ta>
            <ta e="T730" id="Seg_6719" s="T729">onʼiʔ</ta>
            <ta e="T731" id="Seg_6720" s="T730">ine-Kən</ta>
            <ta e="T732" id="Seg_6721" s="T731">măn</ta>
            <ta e="T733" id="Seg_6722" s="T732">amnə-bi-m</ta>
            <ta e="T734" id="Seg_6723" s="T733">šide</ta>
            <ta e="T735" id="Seg_6724" s="T734">ine-Kən</ta>
            <ta e="T736" id="Seg_6725" s="T735">ne-m</ta>
            <ta e="T737" id="Seg_6726" s="T736">amnə-bi</ta>
            <ta e="T738" id="Seg_6727" s="T737">nagur</ta>
            <ta e="T739" id="Seg_6728" s="T738">ine-Kən</ta>
            <ta e="T740" id="Seg_6729" s="T739">nʼi-m</ta>
            <ta e="T741" id="Seg_6730" s="T740">amnə-bi</ta>
            <ta e="T742" id="Seg_6731" s="T741">teʔdə</ta>
            <ta e="T743" id="Seg_6732" s="T742">ine-Kən</ta>
            <ta e="T744" id="Seg_6733" s="T743">koʔbdo-m</ta>
            <ta e="T745" id="Seg_6734" s="T744">amnə-bi</ta>
            <ta e="T746" id="Seg_6735" s="T745">a</ta>
            <ta e="T747" id="Seg_6736" s="T746">sumna</ta>
            <ta e="T748" id="Seg_6737" s="T747">ine-Kən</ta>
            <ta e="T749" id="Seg_6738" s="T748">urgaja</ta>
            <ta e="T750" id="Seg_6739" s="T749">amnə-bi</ta>
            <ta e="T752" id="Seg_6740" s="T751">dĭ</ta>
            <ta e="T753" id="Seg_6741" s="T752">nʼi</ta>
            <ta e="T754" id="Seg_6742" s="T753">šo-bi</ta>
            <ta e="T755" id="Seg_6743" s="T754">miʔnʼibeʔ</ta>
            <ta e="T756" id="Seg_6744" s="T755">kür-zittə</ta>
            <ta e="T757" id="Seg_6745" s="T756">bar</ta>
            <ta e="T758" id="Seg_6746" s="T757">büžü</ta>
            <ta e="T759" id="Seg_6747" s="T758">bar</ta>
            <ta e="T760" id="Seg_6748" s="T759">šišəge</ta>
            <ta e="T761" id="Seg_6749" s="T760">mo-lV-j</ta>
            <ta e="T763" id="Seg_6750" s="T762">sĭri</ta>
            <ta e="T764" id="Seg_6751" s="T763">bar</ta>
            <ta e="T765" id="Seg_6752" s="T764">saʔmə-lV-j</ta>
            <ta e="T766" id="Seg_6753" s="T765">ugaːndə</ta>
            <ta e="T767" id="Seg_6754" s="T766">šišəge</ta>
            <ta e="T768" id="Seg_6755" s="T767">kănna-lV-l</ta>
            <ta e="T769" id="Seg_6756" s="T768">bar</ta>
            <ta e="T770" id="Seg_6757" s="T769">büžü</ta>
            <ta e="T773" id="Seg_6758" s="T772">bar</ta>
            <ta e="T774" id="Seg_6759" s="T773">ejü</ta>
            <ta e="T775" id="Seg_6760" s="T774">mo-lV-j</ta>
            <ta e="T776" id="Seg_6761" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_6762" s="T776">il</ta>
            <ta e="T778" id="Seg_6763" s="T777">tajər-luʔbdə-bi-jəʔ</ta>
            <ta e="T779" id="Seg_6764" s="T778">bar</ta>
            <ta e="T780" id="Seg_6765" s="T779">budəj</ta>
            <ta e="T781" id="Seg_6766" s="T780">kuʔ-luʔbdə-jəʔ</ta>
            <ta e="T782" id="Seg_6767" s="T781">ugaːndə</ta>
            <ta e="T783" id="Seg_6768" s="T782">kuŋgə</ta>
            <ta e="T784" id="Seg_6769" s="T783">nʼergö-laʔbə</ta>
            <ta e="T786" id="Seg_6770" s="T785">nʼuʔ-Tə</ta>
            <ta e="T787" id="Seg_6771" s="T786">ej</ta>
            <ta e="T788" id="Seg_6772" s="T787">ed-liA</ta>
            <ta e="T790" id="Seg_6773" s="T788">i</ta>
            <ta e="T791" id="Seg_6774" s="T790">ine-gəʔ</ta>
            <ta e="T794" id="Seg_6775" s="T793">ine-gəʔ</ta>
            <ta e="T795" id="Seg_6776" s="T794">bar</ta>
            <ta e="T796" id="Seg_6777" s="T795">tʼo-Tə</ta>
            <ta e="T797" id="Seg_6778" s="T796">saʔmə-luʔbdə-bi</ta>
            <ta e="T798" id="Seg_6779" s="T797">nʼuʔdə</ta>
            <ta e="T799" id="Seg_6780" s="T798">bar</ta>
            <ta e="T800" id="Seg_6781" s="T799">nadə</ta>
            <ta e="T801" id="Seg_6782" s="T800">kan-zittə</ta>
            <ta e="T802" id="Seg_6783" s="T801">ugaːndə</ta>
            <ta e="T807" id="Seg_6784" s="T806">ugaːndə</ta>
            <ta e="T808" id="Seg_6785" s="T807">kudaj</ta>
            <ta e="T809" id="Seg_6786" s="T808">nʼuʔdə</ta>
            <ta e="T810" id="Seg_6787" s="T809">amno-laʔbə</ta>
            <ta e="T811" id="Seg_6788" s="T810">măja-Kən</ta>
            <ta e="T814" id="Seg_6789" s="T813">urgo</ta>
            <ta e="T816" id="Seg_6790" s="T815">măja</ta>
            <ta e="T817" id="Seg_6791" s="T816">ulu-m</ta>
            <ta e="T818" id="Seg_6792" s="T817">bar</ta>
            <ta e="T819" id="Seg_6793" s="T818">dʼükəm-liA</ta>
            <ta e="T820" id="Seg_6794" s="T819">kadaʔ-liA-m</ta>
            <ta e="T821" id="Seg_6795" s="T820">naverna</ta>
            <ta e="T822" id="Seg_6796" s="T821">unu</ta>
            <ta e="T823" id="Seg_6797" s="T822">iʔgö</ta>
            <ta e="T824" id="Seg_6798" s="T823">nadə</ta>
            <ta e="T825" id="Seg_6799" s="T824">dĭ-zAŋ</ta>
            <ta e="T826" id="Seg_6800" s="T825">bar</ta>
            <ta e="T828" id="Seg_6801" s="T826">kut-zittə</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_6802" s="T4">eye-NOM/GEN.3SG</ta>
            <ta e="T6" id="Seg_6803" s="T5">NEG</ta>
            <ta e="T7" id="Seg_6804" s="T6">beautiful.[NOM.SG]</ta>
            <ta e="T8" id="Seg_6805" s="T7">one.[NOM.SG]</ta>
            <ta e="T9" id="Seg_6806" s="T8">here</ta>
            <ta e="T10" id="Seg_6807" s="T9">look-DUR.[3SG]</ta>
            <ta e="T11" id="Seg_6808" s="T10">single.[NOM.SG]</ta>
            <ta e="T12" id="Seg_6809" s="T11">there</ta>
            <ta e="T13" id="Seg_6810" s="T12">eye-NOM/GEN.3SG</ta>
            <ta e="T15" id="Seg_6811" s="T14">I.NOM</ta>
            <ta e="T16" id="Seg_6812" s="T15">mouth-NOM/GEN/ACC.1SG</ta>
            <ta e="T17" id="Seg_6813" s="T16">all</ta>
            <ta e="T18" id="Seg_6814" s="T17">close-DUR.[3SG]</ta>
            <ta e="T19" id="Seg_6815" s="T18">sleep-INF.LAT</ta>
            <ta e="T20" id="Seg_6816" s="T19">one.wants</ta>
            <ta e="T21" id="Seg_6817" s="T20">we.NOM</ta>
            <ta e="T22" id="Seg_6818" s="T21">two-COLL</ta>
            <ta e="T24" id="Seg_6819" s="T23">go-PST-1PL</ta>
            <ta e="T25" id="Seg_6820" s="T24">road-LAT</ta>
            <ta e="T26" id="Seg_6821" s="T25">this.[NOM.SG]</ta>
            <ta e="T28" id="Seg_6822" s="T27">go-PST.[3SG]</ta>
            <ta e="T29" id="Seg_6823" s="T28">to.the.left</ta>
            <ta e="T30" id="Seg_6824" s="T29">I.NOM</ta>
            <ta e="T31" id="Seg_6825" s="T30">go-PST-1SG</ta>
            <ta e="T32" id="Seg_6826" s="T31">to.the.right</ta>
            <ta e="T33" id="Seg_6827" s="T32">how</ta>
            <ta e="T34" id="Seg_6828" s="T33">this-ACC</ta>
            <ta e="T35" id="Seg_6829" s="T34">call-PRS-3PL</ta>
            <ta e="T36" id="Seg_6830" s="T35">I.NOM</ta>
            <ta e="T37" id="Seg_6831" s="T36">NEG</ta>
            <ta e="T38" id="Seg_6832" s="T37">know-1SG</ta>
            <ta e="T39" id="Seg_6833" s="T38">bee-PL</ta>
            <ta e="T40" id="Seg_6834" s="T39">all</ta>
            <ta e="T41" id="Seg_6835" s="T40">sweet.[NOM.SG]</ta>
            <ta e="T42" id="Seg_6836" s="T41">item.[NOM.SG]</ta>
            <ta e="T43" id="Seg_6837" s="T42">bring-FUT-3PL</ta>
            <ta e="T44" id="Seg_6838" s="T43">and</ta>
            <ta e="T45" id="Seg_6839" s="T44">cup-LAT</ta>
            <ta e="T47" id="Seg_6840" s="T46">put-DUR-3PL</ta>
            <ta e="T48" id="Seg_6841" s="T47">very</ta>
            <ta e="T49" id="Seg_6842" s="T48">strongly</ta>
            <ta e="T50" id="Seg_6843" s="T49">work-DUR-3PL</ta>
            <ta e="T51" id="Seg_6844" s="T50">forest-LOC</ta>
            <ta e="T53" id="Seg_6845" s="T52">forest-LOC</ta>
            <ta e="T54" id="Seg_6846" s="T53">NEG</ta>
            <ta e="T55" id="Seg_6847" s="T54">live-DUR-3PL</ta>
            <ta e="T56" id="Seg_6848" s="T55">water-LOC</ta>
            <ta e="T57" id="Seg_6849" s="T56">NEG</ta>
            <ta e="T58" id="Seg_6850" s="T57">live-DUR.[3SG]</ta>
            <ta e="T59" id="Seg_6851" s="T58">very</ta>
            <ta e="T60" id="Seg_6852" s="T59">big.[NOM.SG]</ta>
            <ta e="T61" id="Seg_6853" s="T60">mountain.[NOM.SG]</ta>
            <ta e="T62" id="Seg_6854" s="T61">how</ta>
            <ta e="T63" id="Seg_6855" s="T62">this-LAT</ta>
            <ta e="T64" id="Seg_6856" s="T63">go-INF.LAT</ta>
            <ta e="T65" id="Seg_6857" s="T64">NEG</ta>
            <ta e="T66" id="Seg_6858" s="T65">know-1SG</ta>
            <ta e="T67" id="Seg_6859" s="T66">look-PRS-2SG</ta>
            <ta e="T68" id="Seg_6860" s="T67">so</ta>
            <ta e="T69" id="Seg_6861" s="T68">all</ta>
            <ta e="T72" id="Seg_6862" s="T71">fall</ta>
            <ta e="T73" id="Seg_6863" s="T72">hat.[NOM.SG]</ta>
            <ta e="T74" id="Seg_6864" s="T73">fall-MOM-FUT-3SG</ta>
            <ta e="T75" id="Seg_6865" s="T74">go-PST-3PL</ta>
            <ta e="T76" id="Seg_6866" s="T75">earth.[NOM.SG]</ta>
            <ta e="T77" id="Seg_6867" s="T76">dig-INF.LAT</ta>
            <ta e="T80" id="Seg_6868" s="T78">road.[NOM.SG]</ta>
            <ta e="T82" id="Seg_6869" s="T81">go-PST-3PL</ta>
            <ta e="T83" id="Seg_6870" s="T82">earth.[NOM.SG]</ta>
            <ta e="T84" id="Seg_6871" s="T83">dig-INF.LAT</ta>
            <ta e="T85" id="Seg_6872" s="T84">road.[NOM.SG]</ta>
            <ta e="T86" id="Seg_6873" s="T85">heal-INF.LAT</ta>
            <ta e="T87" id="Seg_6874" s="T86">where.to</ta>
            <ta e="T89" id="Seg_6875" s="T88">this-PL</ta>
            <ta e="T90" id="Seg_6876" s="T89">go-PST-3PL</ta>
            <ta e="T91" id="Seg_6877" s="T90">far-LAT.ADV</ta>
            <ta e="T92" id="Seg_6878" s="T91">no</ta>
            <ta e="T93" id="Seg_6879" s="T92">NEG</ta>
            <ta e="T94" id="Seg_6880" s="T93">far</ta>
            <ta e="T95" id="Seg_6881" s="T94">come-PST-3PL</ta>
            <ta e="T96" id="Seg_6882" s="T95">horse-INS-PL</ta>
            <ta e="T97" id="Seg_6883" s="T96">NEG</ta>
            <ta e="T98" id="Seg_6884" s="T97">far-LAT.ADV</ta>
            <ta e="T99" id="Seg_6885" s="T98">house-LOC</ta>
            <ta e="T100" id="Seg_6886" s="T99">stand-PST-3PL</ta>
            <ta e="T101" id="Seg_6887" s="T100">even</ta>
            <ta e="T102" id="Seg_6888" s="T101">NEG</ta>
            <ta e="T103" id="Seg_6889" s="T102">ask-PST-3PL</ta>
            <ta e="T104" id="Seg_6890" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_6891" s="T104">stand-PST-3PL</ta>
            <ta e="T106" id="Seg_6892" s="T105">go-EP-IMP.2SG</ta>
            <ta e="T107" id="Seg_6893" s="T106">I.GEN</ta>
            <ta e="T108" id="Seg_6894" s="T107">house-ABL</ta>
            <ta e="T109" id="Seg_6895" s="T108">and</ta>
            <ta e="T110" id="Seg_6896" s="T109">here</ta>
            <ta e="T111" id="Seg_6897" s="T110">NEG.AUX-IMP.2SG</ta>
            <ta e="T112" id="Seg_6898" s="T111">stand-CNG</ta>
            <ta e="T113" id="Seg_6899" s="T112">NEG</ta>
            <ta e="T114" id="Seg_6900" s="T113">go-PST-3PL</ta>
            <ta e="T115" id="Seg_6901" s="T114">house-LAT</ta>
            <ta e="T116" id="Seg_6902" s="T115">hide-MOM-PST-3PL</ta>
            <ta e="T117" id="Seg_6903" s="T116">and</ta>
            <ta e="T118" id="Seg_6904" s="T117">make.noise-PRS-3PL</ta>
            <ta e="T119" id="Seg_6905" s="T118">PTCL</ta>
            <ta e="T120" id="Seg_6906" s="T119">house-LAT</ta>
            <ta e="T121" id="Seg_6907" s="T120">up</ta>
            <ta e="T122" id="Seg_6908" s="T121">PTCL</ta>
            <ta e="T123" id="Seg_6909" s="T122">jump-MOM-PST-3PL</ta>
            <ta e="T124" id="Seg_6910" s="T123">%%</ta>
            <ta e="T125" id="Seg_6911" s="T124">breast-PL</ta>
            <ta e="T126" id="Seg_6912" s="T125">NEG.EX.[3SG]</ta>
            <ta e="T127" id="Seg_6913" s="T126">where.to=INDEF</ta>
            <ta e="T128" id="Seg_6914" s="T127">go-CVB</ta>
            <ta e="T129" id="Seg_6915" s="T128">disappear-PST-3PL</ta>
            <ta e="T130" id="Seg_6916" s="T129">or</ta>
            <ta e="T131" id="Seg_6917" s="T130">die-PST-3PL</ta>
            <ta e="T132" id="Seg_6918" s="T131">all</ta>
            <ta e="T133" id="Seg_6919" s="T132">I.LAT</ta>
            <ta e="T134" id="Seg_6920" s="T133">also</ta>
            <ta e="T135" id="Seg_6921" s="T134">stone-INS</ta>
            <ta e="T136" id="Seg_6922" s="T135">beat-DUR-2PL</ta>
            <ta e="T137" id="Seg_6923" s="T136">I.LAT</ta>
            <ta e="T138" id="Seg_6924" s="T137">also</ta>
            <ta e="T139" id="Seg_6925" s="T138">stone.[NOM.SG]</ta>
            <ta e="T141" id="Seg_6926" s="T140">put</ta>
            <ta e="T142" id="Seg_6927" s="T141">put-FUT-2SG</ta>
            <ta e="T143" id="Seg_6928" s="T142">people.[NOM.SG]</ta>
            <ta e="T144" id="Seg_6929" s="T143">good.[NOM.SG]</ta>
            <ta e="T145" id="Seg_6930" s="T144">NEG</ta>
            <ta e="T146" id="Seg_6931" s="T145">lie-PRS-1PL</ta>
            <ta e="T147" id="Seg_6932" s="T146">good.[NOM.SG]</ta>
            <ta e="T149" id="Seg_6933" s="T148">speak-DUR-1PL</ta>
            <ta e="T150" id="Seg_6934" s="T149">true</ta>
            <ta e="T151" id="Seg_6935" s="T150">speak-DUR-1SG</ta>
            <ta e="T152" id="Seg_6936" s="T151">NEG</ta>
            <ta e="T153" id="Seg_6937" s="T152">lie-PRS-1SG</ta>
            <ta e="T154" id="Seg_6938" s="T153">red.[NOM.SG]</ta>
            <ta e="T156" id="Seg_6939" s="T154">worm-PL</ta>
            <ta e="T157" id="Seg_6940" s="T156">collect-PST-1SG</ta>
            <ta e="T158" id="Seg_6941" s="T157">water-INS</ta>
            <ta e="T159" id="Seg_6942" s="T158">this-ACC</ta>
            <ta e="T161" id="Seg_6943" s="T160">wash-PST-1SG</ta>
            <ta e="T162" id="Seg_6944" s="T161">cup-LAT</ta>
            <ta e="T163" id="Seg_6945" s="T162">put-PST-1SG</ta>
            <ta e="T164" id="Seg_6946" s="T163">there</ta>
            <ta e="T165" id="Seg_6947" s="T164">warm.[NOM.SG]</ta>
            <ta e="T166" id="Seg_6948" s="T165">warm.[NOM.SG]</ta>
            <ta e="T167" id="Seg_6949" s="T166">place-PST-1SG</ta>
            <ta e="T168" id="Seg_6950" s="T167">there</ta>
            <ta e="T169" id="Seg_6951" s="T168">water.[NOM.SG]</ta>
            <ta e="T170" id="Seg_6952" s="T169">become-RES-PST.[3SG]</ta>
            <ta e="T171" id="Seg_6953" s="T170">then</ta>
            <ta e="T172" id="Seg_6954" s="T171">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T173" id="Seg_6955" s="T172">rub-PST-1SG</ta>
            <ta e="T174" id="Seg_6956" s="T173">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T175" id="Seg_6957" s="T174">very</ta>
            <ta e="T176" id="Seg_6958" s="T175">strongly</ta>
            <ta e="T177" id="Seg_6959" s="T176">hurt-PST-3PL</ta>
            <ta e="T178" id="Seg_6960" s="T177">now</ta>
            <ta e="T179" id="Seg_6961" s="T178">NEG</ta>
            <ta e="T180" id="Seg_6962" s="T179">hurt-PRS-3PL</ta>
            <ta e="T181" id="Seg_6963" s="T180">look</ta>
            <ta e="T183" id="Seg_6964" s="T181">God.[NOM.SG]</ta>
            <ta e="T184" id="Seg_6965" s="T183">look</ta>
            <ta e="T185" id="Seg_6966" s="T184">God.[NOM.SG]</ta>
            <ta e="T186" id="Seg_6967" s="T185">NEG.AUX-IMP.2SG</ta>
            <ta e="T187" id="Seg_6968" s="T186">forget-EP-CNG</ta>
            <ta e="T192" id="Seg_6969" s="T191">go-EP-IMP.2SG</ta>
            <ta e="T193" id="Seg_6970" s="T192">devil-LAT</ta>
            <ta e="T194" id="Seg_6971" s="T193">devil-GEN</ta>
            <ta e="T196" id="Seg_6972" s="T195">horn-NOM/GEN.3SG</ta>
            <ta e="T197" id="Seg_6973" s="T196">cow-EP-GEN</ta>
            <ta e="T198" id="Seg_6974" s="T197">horn-NOM/GEN.3SG</ta>
            <ta e="T199" id="Seg_6975" s="T198">this.[NOM.SG]</ta>
            <ta e="T200" id="Seg_6976" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_6977" s="T200">you.DAT</ta>
            <ta e="T203" id="Seg_6978" s="T202">push-FUT-3SG</ta>
            <ta e="T204" id="Seg_6979" s="T203">hallway-PL</ta>
            <ta e="T206" id="Seg_6980" s="T205">sweep-IMP.2SG.O</ta>
            <ta e="T207" id="Seg_6981" s="T206">otherwise</ta>
            <ta e="T208" id="Seg_6982" s="T207">very</ta>
            <ta e="T209" id="Seg_6983" s="T208">many</ta>
            <ta e="T210" id="Seg_6984" s="T209">dirty.[NOM.SG]</ta>
            <ta e="T211" id="Seg_6985" s="T210">porch</ta>
            <ta e="T212" id="Seg_6986" s="T211">sweep-IMP.2SG.O</ta>
            <ta e="T213" id="Seg_6987" s="T212">barn-LAT</ta>
            <ta e="T214" id="Seg_6988" s="T213">flour.[NOM.SG]</ta>
            <ta e="T215" id="Seg_6989" s="T214">take-FUT-1SG</ta>
            <ta e="T216" id="Seg_6990" s="T215">put-PST-1SG</ta>
            <ta e="T217" id="Seg_6991" s="T216">I.NOM</ta>
            <ta e="T218" id="Seg_6992" s="T217">this-ACC</ta>
            <ta e="T219" id="Seg_6993" s="T218">kill-PST-1SG</ta>
            <ta e="T220" id="Seg_6994" s="T219">I.NOM</ta>
            <ta e="T221" id="Seg_6995" s="T220">strengh-NOM/GEN/ACC.3SG</ta>
            <ta e="T222" id="Seg_6996" s="T221">many</ta>
            <ta e="T223" id="Seg_6997" s="T222">I.NOM</ta>
            <ta e="T224" id="Seg_6998" s="T223">very</ta>
            <ta e="T225" id="Seg_6999" s="T224">powerful.[NOM.SG]</ta>
            <ta e="T226" id="Seg_7000" s="T225">be-PRS-1SG</ta>
            <ta e="T227" id="Seg_7001" s="T226">this.[NOM.SG]</ta>
            <ta e="T228" id="Seg_7002" s="T227">man.[NOM.SG]</ta>
            <ta e="T229" id="Seg_7003" s="T228">PTCL</ta>
            <ta e="T230" id="Seg_7004" s="T229">mind-NOM/GEN/ACC.3SG</ta>
            <ta e="T231" id="Seg_7005" s="T230">NEG.EX.[3SG]</ta>
            <ta e="T232" id="Seg_7006" s="T231">tongue-NOM/GEN.3SG</ta>
            <ta e="T233" id="Seg_7007" s="T232">be-PRS.[3SG]</ta>
            <ta e="T234" id="Seg_7008" s="T233">speak-INF.LAT</ta>
            <ta e="T235" id="Seg_7009" s="T234">NEG</ta>
            <ta e="T236" id="Seg_7010" s="T235">can-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_7011" s="T236">ear-NOM/GEN.3SG</ta>
            <ta e="T238" id="Seg_7012" s="T237">be-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_7013" s="T238">NEG</ta>
            <ta e="T240" id="Seg_7014" s="T239">hear-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_7015" s="T240">NEG.AUX-IMP.2SG</ta>
            <ta e="T242" id="Seg_7016" s="T241">turn-CNG</ta>
            <ta e="T243" id="Seg_7017" s="T242">good.[NOM.SG]</ta>
            <ta e="T244" id="Seg_7018" s="T243">sit-IMP.2SG</ta>
            <ta e="T245" id="Seg_7019" s="T244">what.[NOM.SG]</ta>
            <ta e="T246" id="Seg_7020" s="T245">turn-DUR-2SG</ta>
            <ta e="T247" id="Seg_7021" s="T246">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T248" id="Seg_7022" s="T247">language-ACC</ta>
            <ta e="T249" id="Seg_7023" s="T248">hear-PRS-1SG</ta>
            <ta e="T250" id="Seg_7024" s="T249">good.[NOM.SG]</ta>
            <ta e="T251" id="Seg_7025" s="T250">speak-PRS-1SG</ta>
            <ta e="T252" id="Seg_7026" s="T251">pig.[NOM.SG]</ta>
            <ta e="T253" id="Seg_7027" s="T252">come-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_7028" s="T253">and</ta>
            <ta e="T255" id="Seg_7029" s="T254">shout-DUR.[3SG]</ta>
            <ta e="T256" id="Seg_7030" s="T255">PTCL</ta>
            <ta e="T257" id="Seg_7031" s="T256">one.should</ta>
            <ta e="T258" id="Seg_7032" s="T257">this-ACC</ta>
            <ta e="T259" id="Seg_7033" s="T258">feed-INF.LAT</ta>
            <ta e="T260" id="Seg_7034" s="T259">otherwise</ta>
            <ta e="T261" id="Seg_7035" s="T260">this.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7036" s="T261">be.hungry-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_7037" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_7038" s="T263">one.should</ta>
            <ta e="T266" id="Seg_7039" s="T265">axe.[NOM.SG]</ta>
            <ta e="T267" id="Seg_7040" s="T266">sharpen-INF.LAT</ta>
            <ta e="T268" id="Seg_7041" s="T267">tree-PL</ta>
            <ta e="T273" id="Seg_7042" s="T272">cut-INF.LAT</ta>
            <ta e="T274" id="Seg_7043" s="T273">stove</ta>
            <ta e="T275" id="Seg_7044" s="T274">burn-INF.LAT</ta>
            <ta e="T276" id="Seg_7045" s="T275">saw.[NOM.SG]</ta>
            <ta e="T277" id="Seg_7046" s="T276">bring-PST-3PL</ta>
            <ta e="T278" id="Seg_7047" s="T277">NEG</ta>
            <ta e="T279" id="Seg_7048" s="T278">know-3PL</ta>
            <ta e="T280" id="Seg_7049" s="T279">what.[NOM.SG]</ta>
            <ta e="T281" id="Seg_7050" s="T280">this-INS</ta>
            <ta e="T282" id="Seg_7051" s="T281">make-INF.LAT</ta>
            <ta e="T284" id="Seg_7052" s="T283">Varlam.[NOM.SG]</ta>
            <ta e="T285" id="Seg_7053" s="T284">come-FUT-3SG</ta>
            <ta e="T286" id="Seg_7054" s="T285">heal-FUT-3SG</ta>
            <ta e="T287" id="Seg_7055" s="T286">iron-ADJZ.[NOM.SG]</ta>
            <ta e="T288" id="Seg_7056" s="T287">stove.[NOM.SG]</ta>
            <ta e="T289" id="Seg_7057" s="T288">bring-PST-3PL</ta>
            <ta e="T290" id="Seg_7058" s="T289">house-LAT</ta>
            <ta e="T291" id="Seg_7059" s="T290">set.up-PST-3PL</ta>
            <ta e="T294" id="Seg_7060" s="T293">birchbark.vessel-LAT</ta>
            <ta e="T295" id="Seg_7061" s="T294">%%</ta>
            <ta e="T296" id="Seg_7062" s="T295">birchbark.vessel.[NOM.SG]</ta>
            <ta e="T297" id="Seg_7063" s="T296">this.[NOM.SG]</ta>
            <ta e="T298" id="Seg_7064" s="T297">PTCL</ta>
            <ta e="T299" id="Seg_7065" s="T298">burn-MOM-PST.[3SG]</ta>
            <ta e="T300" id="Seg_7066" s="T299">smoke.[NOM.SG]</ta>
            <ta e="T301" id="Seg_7067" s="T300">come-PST.[3SG]</ta>
            <ta e="T302" id="Seg_7068" s="T301">then</ta>
            <ta e="T303" id="Seg_7069" s="T302">all</ta>
            <ta e="T304" id="Seg_7070" s="T303">let-PST-3PL</ta>
            <ta e="T305" id="Seg_7071" s="T304">outwards</ta>
            <ta e="T306" id="Seg_7072" s="T305">very</ta>
            <ta e="T307" id="Seg_7073" s="T306">smoke.[NOM.SG]</ta>
            <ta e="T308" id="Seg_7074" s="T307">many</ta>
            <ta e="T309" id="Seg_7075" s="T308">Varlam.[NOM.SG]</ta>
            <ta e="T310" id="Seg_7076" s="T309">come-FUT-3SG</ta>
            <ta e="T311" id="Seg_7077" s="T310">and</ta>
            <ta e="T312" id="Seg_7078" s="T311">heal-FUT-3SG</ta>
            <ta e="T313" id="Seg_7079" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_7080" s="T313">smoke.[NOM.SG]</ta>
            <ta e="T315" id="Seg_7081" s="T314">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T316" id="Seg_7082" s="T315">sit-DUR-3PL</ta>
            <ta e="T317" id="Seg_7083" s="T316">then</ta>
            <ta e="T318" id="Seg_7084" s="T317">Varlam.[NOM.SG]</ta>
            <ta e="T319" id="Seg_7085" s="T318">come-PST.[3SG]</ta>
            <ta e="T320" id="Seg_7086" s="T319">stone.[NOM.SG]</ta>
            <ta e="T321" id="Seg_7087" s="T320">bring-PST.[3SG]</ta>
            <ta e="T322" id="Seg_7088" s="T321">place-LAT</ta>
            <ta e="T323" id="Seg_7089" s="T322">put-PST.[3SG]</ta>
            <ta e="T324" id="Seg_7090" s="T323">and</ta>
            <ta e="T325" id="Seg_7091" s="T324">stove-NOM/GEN/ACC.3SG</ta>
            <ta e="T326" id="Seg_7092" s="T325">place-PST.[3SG]</ta>
            <ta e="T327" id="Seg_7093" s="T326">then</ta>
            <ta e="T328" id="Seg_7094" s="T327">stove.[NOM.SG]</ta>
            <ta e="T329" id="Seg_7095" s="T328">light-PST-3PL</ta>
            <ta e="T330" id="Seg_7096" s="T329">sit-DUR-3PL</ta>
            <ta e="T331" id="Seg_7097" s="T330">and</ta>
            <ta e="T333" id="Seg_7098" s="T332">warm-DUR-3PL</ta>
            <ta e="T339" id="Seg_7099" s="T338">fur.coat-NOM/GEN/ACC.1SG</ta>
            <ta e="T340" id="Seg_7100" s="T339">PTCL</ta>
            <ta e="T341" id="Seg_7101" s="T340">container-LOC</ta>
            <ta e="T342" id="Seg_7102" s="T341">earth-LOC</ta>
            <ta e="T343" id="Seg_7103" s="T342">lie-PST.[3SG]</ta>
            <ta e="T344" id="Seg_7104" s="T343">foot-PL</ta>
            <ta e="T345" id="Seg_7105" s="T344">PTCL</ta>
            <ta e="T346" id="Seg_7106" s="T345">rub-PST-3PL</ta>
            <ta e="T347" id="Seg_7107" s="T346">there</ta>
            <ta e="T348" id="Seg_7108" s="T347">there</ta>
            <ta e="T349" id="Seg_7109" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_7110" s="T349">dirty.[NOM.SG]</ta>
            <ta e="T351" id="Seg_7111" s="T350">I.NOM</ta>
            <ta e="T352" id="Seg_7112" s="T351">this-ACC</ta>
            <ta e="T353" id="Seg_7113" s="T352">be-PST-2SG</ta>
            <ta e="T354" id="Seg_7114" s="T353">take-PST-1SG</ta>
            <ta e="T355" id="Seg_7115" s="T354">and</ta>
            <ta e="T356" id="Seg_7116" s="T355">water-LAT</ta>
            <ta e="T357" id="Seg_7117" s="T356">bring-PST-2SG</ta>
            <ta e="T358" id="Seg_7118" s="T357">bring-PST-1SG</ta>
            <ta e="T359" id="Seg_7119" s="T358">so.that</ta>
            <ta e="T360" id="Seg_7120" s="T359">wet.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7121" s="T360">become-PST.[3SG]</ta>
            <ta e="T362" id="Seg_7122" s="T361">then</ta>
            <ta e="T363" id="Seg_7123" s="T362">hit-MULT-PST-1SG</ta>
            <ta e="T364" id="Seg_7124" s="T363">wash-PST-1SG</ta>
            <ta e="T365" id="Seg_7125" s="T364">then</ta>
            <ta e="T366" id="Seg_7126" s="T365">hang.up-PST-1SG</ta>
            <ta e="T367" id="Seg_7127" s="T366">water.[NOM.SG]</ta>
            <ta e="T368" id="Seg_7128" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_7129" s="T368">flow-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_7130" s="T369">this-ABL</ta>
            <ta e="T371" id="Seg_7131" s="T370">river.[NOM.SG]</ta>
            <ta e="T372" id="Seg_7132" s="T371">flow-DUR.[3SG]</ta>
            <ta e="T373" id="Seg_7133" s="T372">and</ta>
            <ta e="T374" id="Seg_7134" s="T373">man.[NOM.SG]</ta>
            <ta e="T375" id="Seg_7135" s="T374">run-DUR.[3SG]</ta>
            <ta e="T376" id="Seg_7136" s="T375">water-VBLZ-CVB</ta>
            <ta e="T377" id="Seg_7137" s="T376">what.kind=INDEF</ta>
            <ta e="T378" id="Seg_7138" s="T377">people.[NOM.SG]</ta>
            <ta e="T379" id="Seg_7139" s="T378">go-DUR-3PL</ta>
            <ta e="T380" id="Seg_7140" s="T379">I.NOM</ta>
            <ta e="T381" id="Seg_7141" s="T380">this-ACC</ta>
            <ta e="T382" id="Seg_7142" s="T381">this-ACC.PL</ta>
            <ta e="T383" id="Seg_7143" s="T382">NEG</ta>
            <ta e="T384" id="Seg_7144" s="T383">know-1SG</ta>
            <ta e="T385" id="Seg_7145" s="T384">what.[NOM.SG]</ta>
            <ta e="T387" id="Seg_7146" s="T386">this-PL</ta>
            <ta e="T388" id="Seg_7147" s="T387">there</ta>
            <ta e="T389" id="Seg_7148" s="T388">go-DUR-3PL</ta>
            <ta e="T390" id="Seg_7149" s="T389">and</ta>
            <ta e="T391" id="Seg_7150" s="T390">this.[NOM.SG]</ta>
            <ta e="T392" id="Seg_7151" s="T391">boy.[NOM.SG]</ta>
            <ta e="T393" id="Seg_7152" s="T392">and</ta>
            <ta e="T394" id="Seg_7153" s="T393">girl.[NOM.SG]</ta>
            <ta e="T395" id="Seg_7154" s="T394">come-PRS-3PL</ta>
            <ta e="T396" id="Seg_7155" s="T395">one.should</ta>
            <ta e="T397" id="Seg_7156" s="T396">milk.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7157" s="T397">pour-INF.LAT</ta>
            <ta e="T399" id="Seg_7158" s="T398">birchbark.vessel-LAT</ta>
            <ta e="T400" id="Seg_7159" s="T399">JUSS</ta>
            <ta e="T401" id="Seg_7160" s="T400">sour.[NOM.SG]</ta>
            <ta e="T402" id="Seg_7161" s="T401">become-FUT-3SG</ta>
            <ta e="T403" id="Seg_7162" s="T402">soon</ta>
            <ta e="T404" id="Seg_7163" s="T403">go-CVB</ta>
            <ta e="T406" id="Seg_7164" s="T405">disappear-PST-1PL</ta>
            <ta e="T407" id="Seg_7165" s="T406">well</ta>
            <ta e="T408" id="Seg_7166" s="T407">go-EP-IMP.2SG</ta>
            <ta e="T409" id="Seg_7167" s="T408">go-IMP.2PL</ta>
            <ta e="T410" id="Seg_7168" s="T409">God-INS</ta>
            <ta e="T411" id="Seg_7169" s="T410">we.NOM</ta>
            <ta e="T412" id="Seg_7170" s="T411">very</ta>
            <ta e="T413" id="Seg_7171" s="T412">many</ta>
            <ta e="T414" id="Seg_7172" s="T413">speak-PST-1PL</ta>
            <ta e="T415" id="Seg_7173" s="T414">now</ta>
            <ta e="T416" id="Seg_7174" s="T415">go-INF.LAT</ta>
            <ta e="T417" id="Seg_7175" s="T416">one.should</ta>
            <ta e="T418" id="Seg_7176" s="T417">tent-LAT/LOC.3SG</ta>
            <ta e="T419" id="Seg_7177" s="T418">and</ta>
            <ta e="T420" id="Seg_7178" s="T419">sleep-INF.LAT</ta>
            <ta e="T421" id="Seg_7179" s="T420">lie-INF.LAT</ta>
            <ta e="T422" id="Seg_7180" s="T421">NEG.AUX-IMP.2SG</ta>
            <ta e="T424" id="Seg_7181" s="T423">NEG.AUX-IMP.2SG</ta>
            <ta e="T425" id="Seg_7182" s="T424">boast-MULT-EP-CNG</ta>
            <ta e="T426" id="Seg_7183" s="T425">what.[NOM.SG]=INDEF</ta>
            <ta e="T427" id="Seg_7184" s="T426">NEG</ta>
            <ta e="T428" id="Seg_7185" s="T427">make-PST-2SG</ta>
            <ta e="T429" id="Seg_7186" s="T428">and</ta>
            <ta e="T430" id="Seg_7187" s="T429">PTCL</ta>
            <ta e="T431" id="Seg_7188" s="T430">boast-MULT-PST-2SG</ta>
            <ta e="T432" id="Seg_7189" s="T431">sleep-INF.LAT</ta>
            <ta e="T433" id="Seg_7190" s="T432">lie-OPT.SG</ta>
            <ta e="T434" id="Seg_7191" s="T433">and</ta>
            <ta e="T435" id="Seg_7192" s="T434">tomorrow</ta>
            <ta e="T436" id="Seg_7193" s="T435">mind-LAT</ta>
            <ta e="T437" id="Seg_7194" s="T436">come-FUT-3SG</ta>
            <ta e="T438" id="Seg_7195" s="T437">%%-LAT</ta>
            <ta e="T439" id="Seg_7196" s="T438">man.[NOM.SG]</ta>
            <ta e="T440" id="Seg_7197" s="T439">come-PST.[3SG]</ta>
            <ta e="T441" id="Seg_7198" s="T440">and</ta>
            <ta e="T442" id="Seg_7199" s="T441">I.NOM</ta>
            <ta e="T443" id="Seg_7200" s="T442">stone.[NOM.SG]</ta>
            <ta e="T444" id="Seg_7201" s="T443">grab-MOM-PST-1SG</ta>
            <ta e="T449" id="Seg_7202" s="T448">stone.[NOM.SG]</ta>
            <ta e="T450" id="Seg_7203" s="T449">grab-MOM-PST-1SG</ta>
            <ta e="T451" id="Seg_7204" s="T450">and</ta>
            <ta e="T452" id="Seg_7205" s="T451">kill-PST-1SG</ta>
            <ta e="T453" id="Seg_7206" s="T452">this.[NOM.SG]</ta>
            <ta e="T454" id="Seg_7207" s="T453">man-ACC</ta>
            <ta e="T455" id="Seg_7208" s="T454">man.[NOM.SG]</ta>
            <ta e="T456" id="Seg_7209" s="T455">man-ACC</ta>
            <ta e="T457" id="Seg_7210" s="T456">PTCL</ta>
            <ta e="T458" id="Seg_7211" s="T457">knife-INS</ta>
            <ta e="T459" id="Seg_7212" s="T458">stab-MOM-PST.[3SG]</ta>
            <ta e="T460" id="Seg_7213" s="T459">and</ta>
            <ta e="T461" id="Seg_7214" s="T460">this.[NOM.SG]</ta>
            <ta e="T462" id="Seg_7215" s="T461">die-RES-PST</ta>
            <ta e="T463" id="Seg_7216" s="T462">there</ta>
            <ta e="T464" id="Seg_7217" s="T463">single.[NOM.SG]</ta>
            <ta e="T465" id="Seg_7218" s="T464">woman.[NOM.SG]</ta>
            <ta e="T466" id="Seg_7219" s="T465">Aginskoe</ta>
            <ta e="T467" id="Seg_7220" s="T466">settlement-LOC</ta>
            <ta e="T468" id="Seg_7221" s="T467">single.[NOM.SG]</ta>
            <ta e="T469" id="Seg_7222" s="T468">woman.[NOM.SG]</ta>
            <ta e="T470" id="Seg_7223" s="T469">man-ACC.3SG</ta>
            <ta e="T471" id="Seg_7224" s="T470">PTCL</ta>
            <ta e="T472" id="Seg_7225" s="T471">cut-PST.[3SG]</ta>
            <ta e="T473" id="Seg_7226" s="T472">axe-INS</ta>
            <ta e="T474" id="Seg_7227" s="T473">head-NOM/GEN.3SG</ta>
            <ta e="T475" id="Seg_7228" s="T474">off-cut-PST.[3SG]</ta>
            <ta e="T476" id="Seg_7229" s="T475">this-GEN</ta>
            <ta e="T477" id="Seg_7230" s="T476">man.[NOM.SG]</ta>
            <ta e="T478" id="Seg_7231" s="T477">single.[NOM.SG]</ta>
            <ta e="T479" id="Seg_7232" s="T478">sleep-MOM-PST.[3SG]</ta>
            <ta e="T480" id="Seg_7233" s="T479">PTCL</ta>
            <ta e="T481" id="Seg_7234" s="T480">very</ta>
            <ta e="T483" id="Seg_7235" s="T482">fear-PST-1SG</ta>
            <ta e="T484" id="Seg_7236" s="T483">how</ta>
            <ta e="T485" id="Seg_7237" s="T484">I.LAT</ta>
            <ta e="T486" id="Seg_7238" s="T485">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T487" id="Seg_7239" s="T486">NEG</ta>
            <ta e="T488" id="Seg_7240" s="T487">cut-PST.[3SG]</ta>
            <ta e="T489" id="Seg_7241" s="T488">this.[NOM.SG]</ta>
            <ta e="T490" id="Seg_7242" s="T489">PTCL</ta>
            <ta e="T491" id="Seg_7243" s="T490">pregnant.[NOM.SG]</ta>
            <ta e="T492" id="Seg_7244" s="T491">be-PST.[3SG]</ta>
            <ta e="T493" id="Seg_7245" s="T492">and</ta>
            <ta e="T494" id="Seg_7246" s="T493">two.[NOM.SG]</ta>
            <ta e="T495" id="Seg_7247" s="T494">child-PL</ta>
            <ta e="T496" id="Seg_7248" s="T495">remain-MOM-PST-3PL</ta>
            <ta e="T500" id="Seg_7249" s="T499">PTCL</ta>
            <ta e="T501" id="Seg_7250" s="T500">five.[NOM.SG]</ta>
            <ta e="T502" id="Seg_7251" s="T501">year.[NOM.SG]</ta>
            <ta e="T503" id="Seg_7252" s="T502">go-CVB</ta>
            <ta e="T504" id="Seg_7253" s="T503">disappear-PST.[3SG]</ta>
            <ta e="T505" id="Seg_7254" s="T504">yesterday</ta>
            <ta e="T506" id="Seg_7255" s="T505">NEG</ta>
            <ta e="T507" id="Seg_7256" s="T506">be-PST-2SG</ta>
            <ta e="T508" id="Seg_7257" s="T507">today</ta>
            <ta e="T511" id="Seg_7258" s="T510">come-PST-2SG</ta>
            <ta e="T512" id="Seg_7259" s="T511">listen-INF.LAT</ta>
            <ta e="T513" id="Seg_7260" s="T512">what.[NOM.SG]</ta>
            <ta e="T514" id="Seg_7261" s="T513">speak-DUR-1PL</ta>
            <ta e="T515" id="Seg_7262" s="T514">I.NOM</ta>
            <ta e="T516" id="Seg_7263" s="T515">today</ta>
            <ta e="T517" id="Seg_7264" s="T516">God.[NOM.SG]</ta>
            <ta e="T518" id="Seg_7265" s="T517">God-LAT</ta>
            <ta e="T521" id="Seg_7266" s="T520">pray-FUT-1SG</ta>
            <ta e="T522" id="Seg_7267" s="T521">God.[NOM.SG]</ta>
            <ta e="T523" id="Seg_7268" s="T522">come-IMP.2SG</ta>
            <ta e="T524" id="Seg_7269" s="T523">I.LAT</ta>
            <ta e="T525" id="Seg_7270" s="T524">listen-IMP.2SG.O</ta>
            <ta e="T526" id="Seg_7271" s="T525">how</ta>
            <ta e="T527" id="Seg_7272" s="T526">I.NOM</ta>
            <ta e="T528" id="Seg_7273" s="T527">pray-PRS-1SG</ta>
            <ta e="T529" id="Seg_7274" s="T528">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T531" id="Seg_7275" s="T530">look-IMP.2SG</ta>
            <ta e="T532" id="Seg_7276" s="T531">probably</ta>
            <ta e="T534" id="Seg_7277" s="T533">be.clean-PRS-3PL</ta>
            <ta e="T535" id="Seg_7278" s="T534">and</ta>
            <ta e="T536" id="Seg_7279" s="T535">NEG</ta>
            <ta e="T537" id="Seg_7280" s="T536">clean.[NOM.SG]</ta>
            <ta e="T538" id="Seg_7281" s="T537">so</ta>
            <ta e="T539" id="Seg_7282" s="T538">wash-IMP.2SG.O</ta>
            <ta e="T540" id="Seg_7283" s="T539">blood-INS</ta>
            <ta e="T541" id="Seg_7284" s="T540">you.NOM</ta>
            <ta e="T542" id="Seg_7285" s="T541">tree-LOC</ta>
            <ta e="T543" id="Seg_7286" s="T542">hang.up-PST-2SG</ta>
            <ta e="T544" id="Seg_7287" s="T543">blood-NOM/GEN/ACC.1SG</ta>
            <ta e="T545" id="Seg_7288" s="T544">flow-DUR.PST.[3SG]</ta>
            <ta e="T546" id="Seg_7289" s="T545">wash-IMP.2SG.O</ta>
            <ta e="T547" id="Seg_7290" s="T546">I.ACC</ta>
            <ta e="T548" id="Seg_7291" s="T547">good.[NOM.SG]</ta>
            <ta e="T549" id="Seg_7292" s="T548">wash-IMP.2SG.O</ta>
            <ta e="T550" id="Seg_7293" s="T549">you.NOM</ta>
            <ta e="T551" id="Seg_7294" s="T550">God.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7295" s="T551">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T553" id="Seg_7296" s="T552">many</ta>
            <ta e="T554" id="Seg_7297" s="T553">all</ta>
            <ta e="T556" id="Seg_7298" s="T555">people-EP-ACC</ta>
            <ta e="T558" id="Seg_7299" s="T557">pity-DUR-2SG</ta>
            <ta e="T560" id="Seg_7300" s="T559">bread.[NOM.SG]</ta>
            <ta e="T561" id="Seg_7301" s="T560">give-PRS-2SG</ta>
            <ta e="T563" id="Seg_7302" s="T562">you.NOM</ta>
            <ta e="T564" id="Seg_7303" s="T563">God.[NOM.SG]</ta>
            <ta e="T565" id="Seg_7304" s="T564">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T566" id="Seg_7305" s="T565">many</ta>
            <ta e="T567" id="Seg_7306" s="T566">you.NOM</ta>
            <ta e="T568" id="Seg_7307" s="T567">people.[NOM.SG]</ta>
            <ta e="T569" id="Seg_7308" s="T568">people.[NOM.SG]</ta>
            <ta e="T570" id="Seg_7309" s="T569">people.[NOM.SG]</ta>
            <ta e="T571" id="Seg_7310" s="T570">pity-PRS-2SG</ta>
            <ta e="T572" id="Seg_7311" s="T571">bread.[NOM.SG]</ta>
            <ta e="T573" id="Seg_7312" s="T572">this-PL-LAT</ta>
            <ta e="T574" id="Seg_7313" s="T573">give-PRS-2SG</ta>
            <ta e="T575" id="Seg_7314" s="T574">PTCL</ta>
            <ta e="T576" id="Seg_7315" s="T575">shirt.[NOM.SG]</ta>
            <ta e="T577" id="Seg_7316" s="T576">give-PRS-2SG</ta>
            <ta e="T578" id="Seg_7317" s="T577">water.[NOM.SG]</ta>
            <ta e="T579" id="Seg_7318" s="T578">give-PRS-2SG</ta>
            <ta e="T580" id="Seg_7319" s="T579">and</ta>
            <ta e="T581" id="Seg_7320" s="T580">I.ACC</ta>
            <ta e="T582" id="Seg_7321" s="T581">pity-EP-IMP.2SG</ta>
            <ta e="T588" id="Seg_7322" s="T587">you.NOM</ta>
            <ta e="T589" id="Seg_7323" s="T588">PTCL</ta>
            <ta e="T590" id="Seg_7324" s="T589">warm.[NOM.SG]</ta>
            <ta e="T591" id="Seg_7325" s="T590">let-PRS-2SG</ta>
            <ta e="T592" id="Seg_7326" s="T591">rain.[NOM.SG]</ta>
            <ta e="T593" id="Seg_7327" s="T592">give-PRS-2SG</ta>
            <ta e="T594" id="Seg_7328" s="T593">so.that</ta>
            <ta e="T595" id="Seg_7329" s="T594">bread.[NOM.SG]</ta>
            <ta e="T596" id="Seg_7330" s="T595">grow-DUR.PST.[3SG]</ta>
            <ta e="T600" id="Seg_7331" s="T599">God-INS</ta>
            <ta e="T601" id="Seg_7332" s="T600">good.[NOM.SG]</ta>
            <ta e="T602" id="Seg_7333" s="T601">where.to</ta>
            <ta e="T603" id="Seg_7334" s="T602">go-FUT-2SG</ta>
            <ta e="T604" id="Seg_7335" s="T603">God-INS</ta>
            <ta e="T607" id="Seg_7336" s="T606">very</ta>
            <ta e="T608" id="Seg_7337" s="T607">good.[NOM.SG]</ta>
            <ta e="T609" id="Seg_7338" s="T608">God-INS</ta>
            <ta e="T610" id="Seg_7339" s="T609">wind.[NOM.SG]</ta>
            <ta e="T611" id="Seg_7340" s="T610">fall-MOM-PST.[3SG]</ta>
            <ta e="T612" id="Seg_7341" s="T611">I.NOM</ta>
            <ta e="T613" id="Seg_7342" s="T612">take-PST-1SG</ta>
            <ta e="T614" id="Seg_7343" s="T613">and</ta>
            <ta e="T615" id="Seg_7344" s="T614">house-LAT</ta>
            <ta e="T616" id="Seg_7345" s="T615">bring-PST-1SG</ta>
            <ta e="T617" id="Seg_7346" s="T616">good.[NOM.SG]</ta>
            <ta e="T618" id="Seg_7347" s="T617">rain.[NOM.SG]</ta>
            <ta e="T619" id="Seg_7348" s="T618">come-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_7349" s="T619">bread.[NOM.SG]</ta>
            <ta e="T621" id="Seg_7350" s="T620">grow-FUT-3SG</ta>
            <ta e="T622" id="Seg_7351" s="T621">I.NOM</ta>
            <ta e="T623" id="Seg_7352" s="T622">this-LAT</ta>
            <ta e="T624" id="Seg_7353" s="T623">help-PST-1SG</ta>
            <ta e="T625" id="Seg_7354" s="T624">and</ta>
            <ta e="T626" id="Seg_7355" s="T625">this.[NOM.SG]</ta>
            <ta e="T627" id="Seg_7356" s="T626">I.LAT</ta>
            <ta e="T628" id="Seg_7357" s="T627">NEG</ta>
            <ta e="T629" id="Seg_7358" s="T628">help-DUR.[3SG]</ta>
            <ta e="T630" id="Seg_7359" s="T629">NEG</ta>
            <ta e="T631" id="Seg_7360" s="T630">give-PRS.[3SG]</ta>
            <ta e="T632" id="Seg_7361" s="T631">I.LAT</ta>
            <ta e="T633" id="Seg_7362" s="T632">work-INF.LAT</ta>
            <ta e="T634" id="Seg_7363" s="T633">I.NOM</ta>
            <ta e="T635" id="Seg_7364" s="T634">IRREAL</ta>
            <ta e="T636" id="Seg_7365" s="T635">work-PST-1SG</ta>
            <ta e="T637" id="Seg_7366" s="T636">and</ta>
            <ta e="T638" id="Seg_7367" s="T637">this.[NOM.SG]</ta>
            <ta e="T639" id="Seg_7368" s="T638">always</ta>
            <ta e="T640" id="Seg_7369" s="T639">hinder-DUR.[3SG]</ta>
            <ta e="T641" id="Seg_7370" s="T640">I.LAT</ta>
            <ta e="T642" id="Seg_7371" s="T641">angry.[NOM.SG]</ta>
            <ta e="T643" id="Seg_7372" s="T642">PTCL</ta>
            <ta e="T644" id="Seg_7373" s="T643">foot-INS</ta>
            <ta e="T645" id="Seg_7374" s="T644">PTCL</ta>
            <ta e="T646" id="Seg_7375" s="T645">stamp-PST.[3SG]</ta>
            <ta e="T647" id="Seg_7376" s="T646">very</ta>
            <ta e="T648" id="Seg_7377" s="T647">angry.[NOM.SG]</ta>
            <ta e="T649" id="Seg_7378" s="T648">dog.[NOM.SG]</ta>
            <ta e="T651" id="Seg_7379" s="T650">all</ta>
            <ta e="T653" id="Seg_7380" s="T652">bite-PRS.[3SG]</ta>
            <ta e="T654" id="Seg_7381" s="T653">people.[NOM.SG]</ta>
            <ta e="T657" id="Seg_7382" s="T656">very</ta>
            <ta e="T658" id="Seg_7383" s="T657">angry.[NOM.SG]</ta>
            <ta e="T659" id="Seg_7384" s="T658">dog.[NOM.SG]</ta>
            <ta e="T661" id="Seg_7385" s="T660">all</ta>
            <ta e="T662" id="Seg_7386" s="T661">people.[NOM.SG]</ta>
            <ta e="T663" id="Seg_7387" s="T662">bite-FRQ-DUR.[3SG]</ta>
            <ta e="T664" id="Seg_7388" s="T663">and</ta>
            <ta e="T665" id="Seg_7389" s="T664">I.ACC</ta>
            <ta e="T666" id="Seg_7390" s="T665">bite-FRQ-PST.[3SG]</ta>
            <ta e="T667" id="Seg_7391" s="T666">I.NOM</ta>
            <ta e="T668" id="Seg_7392" s="T667">you.PL.NOM</ta>
            <ta e="T669" id="Seg_7393" s="T668">wait-PST-1SG</ta>
            <ta e="T670" id="Seg_7394" s="T669">you.PL.NOM</ta>
            <ta e="T671" id="Seg_7395" s="T670">NEG</ta>
            <ta e="T672" id="Seg_7396" s="T671">come-PST-2PL</ta>
            <ta e="T673" id="Seg_7397" s="T672">long.time</ta>
            <ta e="T674" id="Seg_7398" s="T673">NEG.EX-PST-2PL</ta>
            <ta e="T675" id="Seg_7399" s="T674">cup-ABL</ta>
            <ta e="T676" id="Seg_7400" s="T675">take-PST-1SG</ta>
            <ta e="T677" id="Seg_7401" s="T676">wash-PST-1SG</ta>
            <ta e="T678" id="Seg_7402" s="T677">and</ta>
            <ta e="T679" id="Seg_7403" s="T678">again</ta>
            <ta e="T680" id="Seg_7404" s="T679">put-PST-1SG</ta>
            <ta e="T681" id="Seg_7405" s="T680">%%</ta>
            <ta e="T682" id="Seg_7406" s="T681">%%</ta>
            <ta e="T683" id="Seg_7407" s="T682">today</ta>
            <ta e="T684" id="Seg_7408" s="T683">PTCL</ta>
            <ta e="T687" id="Seg_7409" s="T686">water-LAT</ta>
            <ta e="T688" id="Seg_7410" s="T687">NEG</ta>
            <ta e="T689" id="Seg_7411" s="T688">fall-PRS-2SG</ta>
            <ta e="T690" id="Seg_7412" s="T689">%%</ta>
            <ta e="T691" id="Seg_7413" s="T690">PTCL</ta>
            <ta e="T692" id="Seg_7414" s="T691">go-IMP</ta>
            <ta e="T693" id="Seg_7415" s="T692">hand-INS</ta>
            <ta e="T694" id="Seg_7416" s="T693">and</ta>
            <ta e="T695" id="Seg_7417" s="T694">foot-INS</ta>
            <ta e="T696" id="Seg_7418" s="T695">water-LOC</ta>
            <ta e="T697" id="Seg_7419" s="T696">PTCL</ta>
            <ta e="T698" id="Seg_7420" s="T697">walk-PRS-2SG</ta>
            <ta e="T699" id="Seg_7421" s="T698">hand-INS</ta>
            <ta e="T700" id="Seg_7422" s="T699">and</ta>
            <ta e="T701" id="Seg_7423" s="T700">foot-INS</ta>
            <ta e="T702" id="Seg_7424" s="T701">boat</ta>
            <ta e="T703" id="Seg_7425" s="T702">boat-PL-INS</ta>
            <ta e="T704" id="Seg_7426" s="T703">PTCL</ta>
            <ta e="T706" id="Seg_7427" s="T705">water-LAT</ta>
            <ta e="T707" id="Seg_7428" s="T706">walk-PRS.[3SG]</ta>
            <ta e="T708" id="Seg_7429" s="T707">fish.[NOM.SG]</ta>
            <ta e="T709" id="Seg_7430" s="T708">capture-DUR.[3SG]</ta>
            <ta e="T710" id="Seg_7431" s="T709">PTCL</ta>
            <ta e="T711" id="Seg_7432" s="T710">three-COLL</ta>
            <ta e="T712" id="Seg_7433" s="T711">%%-FUT-1PL</ta>
            <ta e="T713" id="Seg_7434" s="T712">single.[NOM.SG]</ta>
            <ta e="T714" id="Seg_7435" s="T713">you.DAT</ta>
            <ta e="T715" id="Seg_7436" s="T714">single.[NOM.SG]</ta>
            <ta e="T716" id="Seg_7437" s="T715">I.LAT</ta>
            <ta e="T717" id="Seg_7438" s="T716">single.[NOM.SG]</ta>
            <ta e="T718" id="Seg_7439" s="T717">this-LAT</ta>
            <ta e="T719" id="Seg_7440" s="T718">two.[NOM.SG]</ta>
            <ta e="T720" id="Seg_7441" s="T719">part.[NOM.SG]</ta>
            <ta e="T721" id="Seg_7442" s="T720">three.[NOM.SG]</ta>
            <ta e="T722" id="Seg_7443" s="T721">part.[NOM.SG]</ta>
            <ta e="T723" id="Seg_7444" s="T722">four</ta>
            <ta e="T724" id="Seg_7445" s="T723">part.[NOM.SG]</ta>
            <ta e="T725" id="Seg_7446" s="T724">along</ta>
            <ta e="T726" id="Seg_7447" s="T725">this.[NOM.SG]</ta>
            <ta e="T727" id="Seg_7448" s="T726">this-PL</ta>
            <ta e="T728" id="Seg_7449" s="T727">PTCL</ta>
            <ta e="T729" id="Seg_7450" s="T728">sit-PST-3PL</ta>
            <ta e="T730" id="Seg_7451" s="T729">single.[NOM.SG]</ta>
            <ta e="T731" id="Seg_7452" s="T730">horse-LOC</ta>
            <ta e="T732" id="Seg_7453" s="T731">I.NOM</ta>
            <ta e="T733" id="Seg_7454" s="T732">sit-PST-1SG</ta>
            <ta e="T734" id="Seg_7455" s="T733">two.[NOM.SG]</ta>
            <ta e="T735" id="Seg_7456" s="T734">horse-LOC</ta>
            <ta e="T736" id="Seg_7457" s="T735">woman-ACC</ta>
            <ta e="T737" id="Seg_7458" s="T736">sit.down-PST.[3SG]</ta>
            <ta e="T738" id="Seg_7459" s="T737">three.[NOM.SG]</ta>
            <ta e="T739" id="Seg_7460" s="T738">horse-LOC</ta>
            <ta e="T740" id="Seg_7461" s="T739">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T741" id="Seg_7462" s="T740">sit.down-PST.[3SG]</ta>
            <ta e="T742" id="Seg_7463" s="T741">four</ta>
            <ta e="T743" id="Seg_7464" s="T742">horse-LOC</ta>
            <ta e="T744" id="Seg_7465" s="T743">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T745" id="Seg_7466" s="T744">sit.down-PST.[3SG]</ta>
            <ta e="T746" id="Seg_7467" s="T745">and</ta>
            <ta e="T747" id="Seg_7468" s="T746">five.[NOM.SG]</ta>
            <ta e="T748" id="Seg_7469" s="T747">horse-LOC</ta>
            <ta e="T749" id="Seg_7470" s="T748">grandmother.[NOM.SG]</ta>
            <ta e="T750" id="Seg_7471" s="T749">sit.down-PST.[3SG]</ta>
            <ta e="T752" id="Seg_7472" s="T751">this.[NOM.SG]</ta>
            <ta e="T753" id="Seg_7473" s="T752">boy.[NOM.SG]</ta>
            <ta e="T754" id="Seg_7474" s="T753">come-PST.[3SG]</ta>
            <ta e="T755" id="Seg_7475" s="T754">we.ACC</ta>
            <ta e="T756" id="Seg_7476" s="T755">take.picture-INF.LAT</ta>
            <ta e="T757" id="Seg_7477" s="T756">PTCL</ta>
            <ta e="T758" id="Seg_7478" s="T757">soon</ta>
            <ta e="T759" id="Seg_7479" s="T758">PTCL</ta>
            <ta e="T760" id="Seg_7480" s="T759">cold.[NOM.SG]</ta>
            <ta e="T761" id="Seg_7481" s="T760">become-FUT-3SG</ta>
            <ta e="T763" id="Seg_7482" s="T762">snow.[NOM.SG]</ta>
            <ta e="T764" id="Seg_7483" s="T763">PTCL</ta>
            <ta e="T765" id="Seg_7484" s="T764">fall-FUT-3SG</ta>
            <ta e="T766" id="Seg_7485" s="T765">very</ta>
            <ta e="T767" id="Seg_7486" s="T766">cold.[NOM.SG]</ta>
            <ta e="T768" id="Seg_7487" s="T767">freeze-FUT-2SG</ta>
            <ta e="T769" id="Seg_7488" s="T768">PTCL</ta>
            <ta e="T770" id="Seg_7489" s="T769">soon</ta>
            <ta e="T773" id="Seg_7490" s="T772">all</ta>
            <ta e="T774" id="Seg_7491" s="T773">warm.[NOM.SG]</ta>
            <ta e="T775" id="Seg_7492" s="T774">become-FUT-3SG</ta>
            <ta e="T776" id="Seg_7493" s="T775">then</ta>
            <ta e="T777" id="Seg_7494" s="T776">people.[NOM.SG]</ta>
            <ta e="T778" id="Seg_7495" s="T777">plough-MOM-PST-3PL</ta>
            <ta e="T779" id="Seg_7496" s="T778">PTCL</ta>
            <ta e="T780" id="Seg_7497" s="T779">wheat</ta>
            <ta e="T781" id="Seg_7498" s="T780">sow-MOM-3PL</ta>
            <ta e="T782" id="Seg_7499" s="T781">very</ta>
            <ta e="T783" id="Seg_7500" s="T782">far</ta>
            <ta e="T784" id="Seg_7501" s="T783">fly-DUR.[3SG]</ta>
            <ta e="T786" id="Seg_7502" s="T785">top-LAT</ta>
            <ta e="T787" id="Seg_7503" s="T786">NEG</ta>
            <ta e="T788" id="Seg_7504" s="T787">be.visible-PRS.[3SG]</ta>
            <ta e="T790" id="Seg_7505" s="T788">and</ta>
            <ta e="T791" id="Seg_7506" s="T790">horse-ABL</ta>
            <ta e="T794" id="Seg_7507" s="T793">horse-ABL</ta>
            <ta e="T795" id="Seg_7508" s="T794">PTCL</ta>
            <ta e="T796" id="Seg_7509" s="T795">place-LAT</ta>
            <ta e="T797" id="Seg_7510" s="T796">fall-MOM-PST.[3SG]</ta>
            <ta e="T798" id="Seg_7511" s="T797">up</ta>
            <ta e="T799" id="Seg_7512" s="T798">PTCL</ta>
            <ta e="T800" id="Seg_7513" s="T799">one.should</ta>
            <ta e="T801" id="Seg_7514" s="T800">go-INF.LAT</ta>
            <ta e="T802" id="Seg_7515" s="T801">very</ta>
            <ta e="T807" id="Seg_7516" s="T806">very</ta>
            <ta e="T808" id="Seg_7517" s="T807">God.[NOM.SG]</ta>
            <ta e="T809" id="Seg_7518" s="T808">up</ta>
            <ta e="T810" id="Seg_7519" s="T809">live-DUR.[3SG]</ta>
            <ta e="T811" id="Seg_7520" s="T810">mountain-LOC</ta>
            <ta e="T814" id="Seg_7521" s="T813">big.[NOM.SG]</ta>
            <ta e="T816" id="Seg_7522" s="T815">mountain.[NOM.SG]</ta>
            <ta e="T817" id="Seg_7523" s="T816">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T818" id="Seg_7524" s="T817">PTCL</ta>
            <ta e="T819" id="Seg_7525" s="T818">itch-PRS.[3SG]</ta>
            <ta e="T820" id="Seg_7526" s="T819">scratch-PRS-1SG</ta>
            <ta e="T821" id="Seg_7527" s="T820">probably</ta>
            <ta e="T822" id="Seg_7528" s="T821">louse.[NOM.SG]</ta>
            <ta e="T823" id="Seg_7529" s="T822">many</ta>
            <ta e="T824" id="Seg_7530" s="T823">one.should</ta>
            <ta e="T825" id="Seg_7531" s="T824">this-PL</ta>
            <ta e="T826" id="Seg_7532" s="T825">PTCL</ta>
            <ta e="T828" id="Seg_7533" s="T826">kill-INF.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_7534" s="T4">глаз-NOM/GEN.3SG</ta>
            <ta e="T6" id="Seg_7535" s="T5">NEG</ta>
            <ta e="T7" id="Seg_7536" s="T6">красивый.[NOM.SG]</ta>
            <ta e="T8" id="Seg_7537" s="T7">один.[NOM.SG]</ta>
            <ta e="T9" id="Seg_7538" s="T8">здесь</ta>
            <ta e="T10" id="Seg_7539" s="T9">смотреть-DUR.[3SG]</ta>
            <ta e="T11" id="Seg_7540" s="T10">один.[NOM.SG]</ta>
            <ta e="T12" id="Seg_7541" s="T11">там</ta>
            <ta e="T13" id="Seg_7542" s="T12">глаз-NOM/GEN.3SG</ta>
            <ta e="T15" id="Seg_7543" s="T14">я.NOM</ta>
            <ta e="T16" id="Seg_7544" s="T15">рот-NOM/GEN/ACC.1SG</ta>
            <ta e="T17" id="Seg_7545" s="T16">весь</ta>
            <ta e="T18" id="Seg_7546" s="T17">закрыть-DUR.[3SG]</ta>
            <ta e="T19" id="Seg_7547" s="T18">спать-INF.LAT</ta>
            <ta e="T20" id="Seg_7548" s="T19">хочется</ta>
            <ta e="T21" id="Seg_7549" s="T20">мы.NOM</ta>
            <ta e="T22" id="Seg_7550" s="T21">два-COLL</ta>
            <ta e="T24" id="Seg_7551" s="T23">пойти-PST-1PL</ta>
            <ta e="T25" id="Seg_7552" s="T24">дорога-LAT</ta>
            <ta e="T26" id="Seg_7553" s="T25">этот.[NOM.SG]</ta>
            <ta e="T28" id="Seg_7554" s="T27">пойти-PST.[3SG]</ta>
            <ta e="T29" id="Seg_7555" s="T28">налево</ta>
            <ta e="T30" id="Seg_7556" s="T29">я.NOM</ta>
            <ta e="T31" id="Seg_7557" s="T30">пойти-PST-1SG</ta>
            <ta e="T32" id="Seg_7558" s="T31">направо</ta>
            <ta e="T33" id="Seg_7559" s="T32">как</ta>
            <ta e="T34" id="Seg_7560" s="T33">этот-ACC</ta>
            <ta e="T35" id="Seg_7561" s="T34">позвать-PRS-3PL</ta>
            <ta e="T36" id="Seg_7562" s="T35">я.NOM</ta>
            <ta e="T37" id="Seg_7563" s="T36">NEG</ta>
            <ta e="T38" id="Seg_7564" s="T37">знать-1SG</ta>
            <ta e="T39" id="Seg_7565" s="T38">пчела-PL</ta>
            <ta e="T40" id="Seg_7566" s="T39">весь</ta>
            <ta e="T41" id="Seg_7567" s="T40">сладкий.[NOM.SG]</ta>
            <ta e="T42" id="Seg_7568" s="T41">вещь.[NOM.SG]</ta>
            <ta e="T43" id="Seg_7569" s="T42">принести-FUT-3PL</ta>
            <ta e="T44" id="Seg_7570" s="T43">и</ta>
            <ta e="T45" id="Seg_7571" s="T44">чашка-LAT</ta>
            <ta e="T47" id="Seg_7572" s="T46">класть-DUR-3PL</ta>
            <ta e="T48" id="Seg_7573" s="T47">очень</ta>
            <ta e="T49" id="Seg_7574" s="T48">сильно</ta>
            <ta e="T50" id="Seg_7575" s="T49">работать-DUR-3PL</ta>
            <ta e="T51" id="Seg_7576" s="T50">лес-LOC</ta>
            <ta e="T53" id="Seg_7577" s="T52">лес-LOC</ta>
            <ta e="T54" id="Seg_7578" s="T53">NEG</ta>
            <ta e="T55" id="Seg_7579" s="T54">жить-DUR-3PL</ta>
            <ta e="T56" id="Seg_7580" s="T55">вода-LOC</ta>
            <ta e="T57" id="Seg_7581" s="T56">NEG</ta>
            <ta e="T58" id="Seg_7582" s="T57">жить-DUR.[3SG]</ta>
            <ta e="T59" id="Seg_7583" s="T58">очень</ta>
            <ta e="T60" id="Seg_7584" s="T59">большой.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7585" s="T60">гора.[NOM.SG]</ta>
            <ta e="T62" id="Seg_7586" s="T61">как</ta>
            <ta e="T63" id="Seg_7587" s="T62">этот-LAT</ta>
            <ta e="T64" id="Seg_7588" s="T63">пойти-INF.LAT</ta>
            <ta e="T65" id="Seg_7589" s="T64">NEG</ta>
            <ta e="T66" id="Seg_7590" s="T65">знать-1SG</ta>
            <ta e="T67" id="Seg_7591" s="T66">смотреть-PRS-2SG</ta>
            <ta e="T68" id="Seg_7592" s="T67">так</ta>
            <ta e="T69" id="Seg_7593" s="T68">весь</ta>
            <ta e="T72" id="Seg_7594" s="T71">упасть</ta>
            <ta e="T73" id="Seg_7595" s="T72">шапка.[NOM.SG]</ta>
            <ta e="T74" id="Seg_7596" s="T73">упасть-MOM-FUT-3SG</ta>
            <ta e="T75" id="Seg_7597" s="T74">пойти-PST-3PL</ta>
            <ta e="T76" id="Seg_7598" s="T75">земля.[NOM.SG]</ta>
            <ta e="T77" id="Seg_7599" s="T76">копать-INF.LAT</ta>
            <ta e="T80" id="Seg_7600" s="T78">дорога.[NOM.SG]</ta>
            <ta e="T82" id="Seg_7601" s="T81">пойти-PST-3PL</ta>
            <ta e="T83" id="Seg_7602" s="T82">земля.[NOM.SG]</ta>
            <ta e="T84" id="Seg_7603" s="T83">копать-INF.LAT</ta>
            <ta e="T85" id="Seg_7604" s="T84">дорога.[NOM.SG]</ta>
            <ta e="T86" id="Seg_7605" s="T85">вылечить-INF.LAT</ta>
            <ta e="T87" id="Seg_7606" s="T86">куда</ta>
            <ta e="T89" id="Seg_7607" s="T88">этот-PL</ta>
            <ta e="T90" id="Seg_7608" s="T89">пойти-PST-3PL</ta>
            <ta e="T91" id="Seg_7609" s="T90">далеко-LAT.ADV</ta>
            <ta e="T92" id="Seg_7610" s="T91">нет</ta>
            <ta e="T93" id="Seg_7611" s="T92">NEG</ta>
            <ta e="T94" id="Seg_7612" s="T93">далеко</ta>
            <ta e="T95" id="Seg_7613" s="T94">прийти-PST-3PL</ta>
            <ta e="T96" id="Seg_7614" s="T95">лошадь-INS-PL</ta>
            <ta e="T97" id="Seg_7615" s="T96">NEG</ta>
            <ta e="T98" id="Seg_7616" s="T97">далеко-LAT.ADV</ta>
            <ta e="T99" id="Seg_7617" s="T98">дом-LOC</ta>
            <ta e="T100" id="Seg_7618" s="T99">стоять-PST-3PL</ta>
            <ta e="T101" id="Seg_7619" s="T100">даже</ta>
            <ta e="T102" id="Seg_7620" s="T101">NEG</ta>
            <ta e="T103" id="Seg_7621" s="T102">спросить-PST-3PL</ta>
            <ta e="T104" id="Seg_7622" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_7623" s="T104">стоять-PST-3PL</ta>
            <ta e="T106" id="Seg_7624" s="T105">пойти-EP-IMP.2SG</ta>
            <ta e="T107" id="Seg_7625" s="T106">я.GEN</ta>
            <ta e="T108" id="Seg_7626" s="T107">дом-ABL</ta>
            <ta e="T109" id="Seg_7627" s="T108">и</ta>
            <ta e="T110" id="Seg_7628" s="T109">здесь</ta>
            <ta e="T111" id="Seg_7629" s="T110">NEG.AUX-IMP.2SG</ta>
            <ta e="T112" id="Seg_7630" s="T111">стоять-CNG</ta>
            <ta e="T113" id="Seg_7631" s="T112">NEG</ta>
            <ta e="T114" id="Seg_7632" s="T113">пойти-PST-3PL</ta>
            <ta e="T115" id="Seg_7633" s="T114">дом-LAT</ta>
            <ta e="T116" id="Seg_7634" s="T115">спрятаться-MOM-PST-3PL</ta>
            <ta e="T117" id="Seg_7635" s="T116">и</ta>
            <ta e="T118" id="Seg_7636" s="T117">шуметь-PRS-3PL</ta>
            <ta e="T119" id="Seg_7637" s="T118">PTCL</ta>
            <ta e="T120" id="Seg_7638" s="T119">дом-LAT</ta>
            <ta e="T121" id="Seg_7639" s="T120">вверх</ta>
            <ta e="T122" id="Seg_7640" s="T121">PTCL</ta>
            <ta e="T123" id="Seg_7641" s="T122">прыгнуть-MOM-PST-3PL</ta>
            <ta e="T124" id="Seg_7642" s="T123">%%</ta>
            <ta e="T125" id="Seg_7643" s="T124">грудь-PL</ta>
            <ta e="T126" id="Seg_7644" s="T125">NEG.EX.[3SG]</ta>
            <ta e="T127" id="Seg_7645" s="T126">куда=INDEF</ta>
            <ta e="T128" id="Seg_7646" s="T127">пойти-CVB</ta>
            <ta e="T129" id="Seg_7647" s="T128">исчезнуть-PST-3PL</ta>
            <ta e="T130" id="Seg_7648" s="T129">или</ta>
            <ta e="T131" id="Seg_7649" s="T130">умереть-PST-3PL</ta>
            <ta e="T132" id="Seg_7650" s="T131">весь</ta>
            <ta e="T133" id="Seg_7651" s="T132">я.LAT</ta>
            <ta e="T134" id="Seg_7652" s="T133">тоже</ta>
            <ta e="T135" id="Seg_7653" s="T134">камень-INS</ta>
            <ta e="T136" id="Seg_7654" s="T135">бить-DUR-2PL</ta>
            <ta e="T137" id="Seg_7655" s="T136">я.LAT</ta>
            <ta e="T138" id="Seg_7656" s="T137">тоже</ta>
            <ta e="T139" id="Seg_7657" s="T138">камень.[NOM.SG]</ta>
            <ta e="T141" id="Seg_7658" s="T140">класть</ta>
            <ta e="T142" id="Seg_7659" s="T141">класть-FUT-2SG</ta>
            <ta e="T143" id="Seg_7660" s="T142">люди.[NOM.SG]</ta>
            <ta e="T144" id="Seg_7661" s="T143">хороший.[NOM.SG]</ta>
            <ta e="T145" id="Seg_7662" s="T144">NEG</ta>
            <ta e="T146" id="Seg_7663" s="T145">лгать-PRS-1PL</ta>
            <ta e="T147" id="Seg_7664" s="T146">хороший.[NOM.SG]</ta>
            <ta e="T149" id="Seg_7665" s="T148">говорить-DUR-1PL</ta>
            <ta e="T150" id="Seg_7666" s="T149">верный</ta>
            <ta e="T151" id="Seg_7667" s="T150">говорить-DUR-1SG</ta>
            <ta e="T152" id="Seg_7668" s="T151">NEG</ta>
            <ta e="T153" id="Seg_7669" s="T152">лгать-PRS-1SG</ta>
            <ta e="T154" id="Seg_7670" s="T153">красный.[NOM.SG]</ta>
            <ta e="T156" id="Seg_7671" s="T154">червь-PL</ta>
            <ta e="T157" id="Seg_7672" s="T156">собирать-PST-1SG</ta>
            <ta e="T158" id="Seg_7673" s="T157">вода-INS</ta>
            <ta e="T159" id="Seg_7674" s="T158">этот-ACC</ta>
            <ta e="T161" id="Seg_7675" s="T160">мыть-PST-1SG</ta>
            <ta e="T162" id="Seg_7676" s="T161">чашка-LAT</ta>
            <ta e="T163" id="Seg_7677" s="T162">класть-PST-1SG</ta>
            <ta e="T164" id="Seg_7678" s="T163">там</ta>
            <ta e="T165" id="Seg_7679" s="T164">теплый.[NOM.SG]</ta>
            <ta e="T166" id="Seg_7680" s="T165">теплый.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7681" s="T166">поставить-PST-1SG</ta>
            <ta e="T168" id="Seg_7682" s="T167">там</ta>
            <ta e="T169" id="Seg_7683" s="T168">вода.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7684" s="T169">стать-RES-PST.[3SG]</ta>
            <ta e="T171" id="Seg_7685" s="T170">тогда</ta>
            <ta e="T172" id="Seg_7686" s="T171">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T173" id="Seg_7687" s="T172">тереть-PST-1SG</ta>
            <ta e="T174" id="Seg_7688" s="T173">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T175" id="Seg_7689" s="T174">очень</ta>
            <ta e="T176" id="Seg_7690" s="T175">сильно</ta>
            <ta e="T177" id="Seg_7691" s="T176">болеть-PST-3PL</ta>
            <ta e="T178" id="Seg_7692" s="T177">сейчас</ta>
            <ta e="T179" id="Seg_7693" s="T178">NEG</ta>
            <ta e="T180" id="Seg_7694" s="T179">болеть-PRS-3PL</ta>
            <ta e="T181" id="Seg_7695" s="T180">смотри</ta>
            <ta e="T183" id="Seg_7696" s="T181">бог.[NOM.SG]</ta>
            <ta e="T184" id="Seg_7697" s="T183">смотри</ta>
            <ta e="T185" id="Seg_7698" s="T184">бог.[NOM.SG]</ta>
            <ta e="T186" id="Seg_7699" s="T185">NEG.AUX-IMP.2SG</ta>
            <ta e="T187" id="Seg_7700" s="T186">забыть-EP-CNG</ta>
            <ta e="T192" id="Seg_7701" s="T191">пойти-EP-IMP.2SG</ta>
            <ta e="T193" id="Seg_7702" s="T192">черт-LAT</ta>
            <ta e="T194" id="Seg_7703" s="T193">черт-GEN</ta>
            <ta e="T196" id="Seg_7704" s="T195">рог-NOM/GEN.3SG</ta>
            <ta e="T197" id="Seg_7705" s="T196">корова-EP-GEN</ta>
            <ta e="T198" id="Seg_7706" s="T197">рог-NOM/GEN.3SG</ta>
            <ta e="T199" id="Seg_7707" s="T198">этот.[NOM.SG]</ta>
            <ta e="T200" id="Seg_7708" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_7709" s="T200">ты.DAT</ta>
            <ta e="T203" id="Seg_7710" s="T202">вставить-FUT-3SG</ta>
            <ta e="T204" id="Seg_7711" s="T203">сени-PL</ta>
            <ta e="T206" id="Seg_7712" s="T205">мести-IMP.2SG.O</ta>
            <ta e="T207" id="Seg_7713" s="T206">а.то</ta>
            <ta e="T208" id="Seg_7714" s="T207">очень</ta>
            <ta e="T209" id="Seg_7715" s="T208">много</ta>
            <ta e="T210" id="Seg_7716" s="T209">грязный.[NOM.SG]</ta>
            <ta e="T211" id="Seg_7717" s="T210">крыльцо</ta>
            <ta e="T212" id="Seg_7718" s="T211">мести-IMP.2SG.O</ta>
            <ta e="T213" id="Seg_7719" s="T212">амбар-LAT</ta>
            <ta e="T214" id="Seg_7720" s="T213">мука.[NOM.SG]</ta>
            <ta e="T215" id="Seg_7721" s="T214">взять-FUT-1SG</ta>
            <ta e="T216" id="Seg_7722" s="T215">класть-PST-1SG</ta>
            <ta e="T217" id="Seg_7723" s="T216">я.NOM</ta>
            <ta e="T218" id="Seg_7724" s="T217">этот-ACC</ta>
            <ta e="T219" id="Seg_7725" s="T218">убить-PST-1SG</ta>
            <ta e="T220" id="Seg_7726" s="T219">я.NOM</ta>
            <ta e="T221" id="Seg_7727" s="T220">сила-NOM/GEN/ACC.3SG</ta>
            <ta e="T222" id="Seg_7728" s="T221">много</ta>
            <ta e="T223" id="Seg_7729" s="T222">я.NOM</ta>
            <ta e="T224" id="Seg_7730" s="T223">очень</ta>
            <ta e="T225" id="Seg_7731" s="T224">сильный.[NOM.SG]</ta>
            <ta e="T226" id="Seg_7732" s="T225">быть-PRS-1SG</ta>
            <ta e="T227" id="Seg_7733" s="T226">этот.[NOM.SG]</ta>
            <ta e="T228" id="Seg_7734" s="T227">мужчина.[NOM.SG]</ta>
            <ta e="T229" id="Seg_7735" s="T228">PTCL</ta>
            <ta e="T230" id="Seg_7736" s="T229">ум-NOM/GEN/ACC.3SG</ta>
            <ta e="T231" id="Seg_7737" s="T230">NEG.EX.[3SG]</ta>
            <ta e="T232" id="Seg_7738" s="T231">язык-NOM/GEN.3SG</ta>
            <ta e="T233" id="Seg_7739" s="T232">быть-PRS.[3SG]</ta>
            <ta e="T234" id="Seg_7740" s="T233">говорить-INF.LAT</ta>
            <ta e="T235" id="Seg_7741" s="T234">NEG</ta>
            <ta e="T236" id="Seg_7742" s="T235">мочь-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_7743" s="T236">ухо-NOM/GEN.3SG</ta>
            <ta e="T238" id="Seg_7744" s="T237">быть-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_7745" s="T238">NEG</ta>
            <ta e="T240" id="Seg_7746" s="T239">слышать-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_7747" s="T240">NEG.AUX-IMP.2SG</ta>
            <ta e="T242" id="Seg_7748" s="T241">поворачиваться-CNG</ta>
            <ta e="T243" id="Seg_7749" s="T242">хороший.[NOM.SG]</ta>
            <ta e="T244" id="Seg_7750" s="T243">сидеть-IMP.2SG</ta>
            <ta e="T245" id="Seg_7751" s="T244">что.[NOM.SG]</ta>
            <ta e="T246" id="Seg_7752" s="T245">поворачиваться-DUR-2SG</ta>
            <ta e="T247" id="Seg_7753" s="T246">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T248" id="Seg_7754" s="T247">язык-ACC</ta>
            <ta e="T249" id="Seg_7755" s="T248">слышать-PRS-1SG</ta>
            <ta e="T250" id="Seg_7756" s="T249">хороший.[NOM.SG]</ta>
            <ta e="T251" id="Seg_7757" s="T250">говорить-PRS-1SG</ta>
            <ta e="T252" id="Seg_7758" s="T251">свинья.[NOM.SG]</ta>
            <ta e="T253" id="Seg_7759" s="T252">прийти-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_7760" s="T253">и</ta>
            <ta e="T255" id="Seg_7761" s="T254">кричать-DUR.[3SG]</ta>
            <ta e="T256" id="Seg_7762" s="T255">PTCL</ta>
            <ta e="T257" id="Seg_7763" s="T256">надо</ta>
            <ta e="T258" id="Seg_7764" s="T257">этот-ACC</ta>
            <ta e="T259" id="Seg_7765" s="T258">кормить-INF.LAT</ta>
            <ta e="T260" id="Seg_7766" s="T259">а.то</ta>
            <ta e="T261" id="Seg_7767" s="T260">этот.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7768" s="T261">быть.голодным-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_7769" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_7770" s="T263">надо</ta>
            <ta e="T266" id="Seg_7771" s="T265">топор.[NOM.SG]</ta>
            <ta e="T267" id="Seg_7772" s="T266">точить-INF.LAT</ta>
            <ta e="T268" id="Seg_7773" s="T267">дерево-PL</ta>
            <ta e="T273" id="Seg_7774" s="T272">резать-INF.LAT</ta>
            <ta e="T274" id="Seg_7775" s="T273">печь</ta>
            <ta e="T275" id="Seg_7776" s="T274">гореть-INF.LAT</ta>
            <ta e="T276" id="Seg_7777" s="T275">пила.[NOM.SG]</ta>
            <ta e="T277" id="Seg_7778" s="T276">принести-PST-3PL</ta>
            <ta e="T278" id="Seg_7779" s="T277">NEG</ta>
            <ta e="T279" id="Seg_7780" s="T278">знать-3PL</ta>
            <ta e="T280" id="Seg_7781" s="T279">что.[NOM.SG]</ta>
            <ta e="T281" id="Seg_7782" s="T280">этот-INS</ta>
            <ta e="T282" id="Seg_7783" s="T281">делать-INF.LAT</ta>
            <ta e="T284" id="Seg_7784" s="T283">Варлам.[NOM.SG]</ta>
            <ta e="T285" id="Seg_7785" s="T284">прийти-FUT-3SG</ta>
            <ta e="T286" id="Seg_7786" s="T285">вылечить-FUT-3SG</ta>
            <ta e="T287" id="Seg_7787" s="T286">железо-ADJZ.[NOM.SG]</ta>
            <ta e="T288" id="Seg_7788" s="T287">печь.[NOM.SG]</ta>
            <ta e="T289" id="Seg_7789" s="T288">принести-PST-3PL</ta>
            <ta e="T290" id="Seg_7790" s="T289">дом-LAT</ta>
            <ta e="T291" id="Seg_7791" s="T290">установить-PST-3PL</ta>
            <ta e="T294" id="Seg_7792" s="T293">туес-LAT</ta>
            <ta e="T295" id="Seg_7793" s="T294">%%</ta>
            <ta e="T296" id="Seg_7794" s="T295">туес.[NOM.SG]</ta>
            <ta e="T297" id="Seg_7795" s="T296">этот.[NOM.SG]</ta>
            <ta e="T298" id="Seg_7796" s="T297">PTCL</ta>
            <ta e="T299" id="Seg_7797" s="T298">гореть-MOM-PST.[3SG]</ta>
            <ta e="T300" id="Seg_7798" s="T299">дым.[NOM.SG]</ta>
            <ta e="T301" id="Seg_7799" s="T300">прийти-PST.[3SG]</ta>
            <ta e="T302" id="Seg_7800" s="T301">тогда</ta>
            <ta e="T303" id="Seg_7801" s="T302">весь</ta>
            <ta e="T304" id="Seg_7802" s="T303">пускать-PST-3PL</ta>
            <ta e="T305" id="Seg_7803" s="T304">наружу</ta>
            <ta e="T306" id="Seg_7804" s="T305">очень</ta>
            <ta e="T307" id="Seg_7805" s="T306">дым.[NOM.SG]</ta>
            <ta e="T308" id="Seg_7806" s="T307">много</ta>
            <ta e="T309" id="Seg_7807" s="T308">Варлам.[NOM.SG]</ta>
            <ta e="T310" id="Seg_7808" s="T309">прийти-FUT-3SG</ta>
            <ta e="T311" id="Seg_7809" s="T310">и</ta>
            <ta e="T312" id="Seg_7810" s="T311">вылечить-FUT-3SG</ta>
            <ta e="T313" id="Seg_7811" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_7812" s="T313">дым.[NOM.SG]</ta>
            <ta e="T315" id="Seg_7813" s="T314">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T316" id="Seg_7814" s="T315">сидеть-DUR-3PL</ta>
            <ta e="T317" id="Seg_7815" s="T316">тогда</ta>
            <ta e="T318" id="Seg_7816" s="T317">Варлам.[NOM.SG]</ta>
            <ta e="T319" id="Seg_7817" s="T318">прийти-PST.[3SG]</ta>
            <ta e="T320" id="Seg_7818" s="T319">камень.[NOM.SG]</ta>
            <ta e="T321" id="Seg_7819" s="T320">принести-PST.[3SG]</ta>
            <ta e="T322" id="Seg_7820" s="T321">место-LAT</ta>
            <ta e="T323" id="Seg_7821" s="T322">класть-PST.[3SG]</ta>
            <ta e="T324" id="Seg_7822" s="T323">и</ta>
            <ta e="T325" id="Seg_7823" s="T324">печь-NOM/GEN/ACC.3SG</ta>
            <ta e="T326" id="Seg_7824" s="T325">поставить-PST.[3SG]</ta>
            <ta e="T327" id="Seg_7825" s="T326">тогда</ta>
            <ta e="T328" id="Seg_7826" s="T327">печь.[NOM.SG]</ta>
            <ta e="T329" id="Seg_7827" s="T328">светить-PST-3PL</ta>
            <ta e="T330" id="Seg_7828" s="T329">сидеть-DUR-3PL</ta>
            <ta e="T331" id="Seg_7829" s="T330">и</ta>
            <ta e="T333" id="Seg_7830" s="T332">теплый-DUR-3PL</ta>
            <ta e="T339" id="Seg_7831" s="T338">парка-NOM/GEN/ACC.1SG</ta>
            <ta e="T340" id="Seg_7832" s="T339">PTCL</ta>
            <ta e="T341" id="Seg_7833" s="T340">корзина-LOC</ta>
            <ta e="T342" id="Seg_7834" s="T341">земля-LOC</ta>
            <ta e="T343" id="Seg_7835" s="T342">лежать-PST.[3SG]</ta>
            <ta e="T344" id="Seg_7836" s="T343">нога-PL</ta>
            <ta e="T345" id="Seg_7837" s="T344">PTCL</ta>
            <ta e="T346" id="Seg_7838" s="T345">тереть-PST-3PL</ta>
            <ta e="T347" id="Seg_7839" s="T346">там</ta>
            <ta e="T348" id="Seg_7840" s="T347">там</ta>
            <ta e="T349" id="Seg_7841" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_7842" s="T349">грязный.[NOM.SG]</ta>
            <ta e="T351" id="Seg_7843" s="T350">я.NOM</ta>
            <ta e="T352" id="Seg_7844" s="T351">этот-ACC</ta>
            <ta e="T353" id="Seg_7845" s="T352">быть-PST-2SG</ta>
            <ta e="T354" id="Seg_7846" s="T353">взять-PST-1SG</ta>
            <ta e="T355" id="Seg_7847" s="T354">и</ta>
            <ta e="T356" id="Seg_7848" s="T355">вода-LAT</ta>
            <ta e="T357" id="Seg_7849" s="T356">нести-PST-2SG</ta>
            <ta e="T358" id="Seg_7850" s="T357">нести-PST-1SG</ta>
            <ta e="T359" id="Seg_7851" s="T358">чтобы</ta>
            <ta e="T360" id="Seg_7852" s="T359">влажный.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7853" s="T360">стать-PST.[3SG]</ta>
            <ta e="T362" id="Seg_7854" s="T361">тогда</ta>
            <ta e="T363" id="Seg_7855" s="T362">ударить-MULT-PST-1SG</ta>
            <ta e="T364" id="Seg_7856" s="T363">мыть-PST-1SG</ta>
            <ta e="T365" id="Seg_7857" s="T364">тогда</ta>
            <ta e="T366" id="Seg_7858" s="T365">вешать-PST-1SG</ta>
            <ta e="T367" id="Seg_7859" s="T366">вода.[NOM.SG]</ta>
            <ta e="T368" id="Seg_7860" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_7861" s="T368">течь-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_7862" s="T369">этот-ABL</ta>
            <ta e="T371" id="Seg_7863" s="T370">река.[NOM.SG]</ta>
            <ta e="T372" id="Seg_7864" s="T371">течь-DUR.[3SG]</ta>
            <ta e="T373" id="Seg_7865" s="T372">и</ta>
            <ta e="T374" id="Seg_7866" s="T373">мужчина.[NOM.SG]</ta>
            <ta e="T375" id="Seg_7867" s="T374">бежать-DUR.[3SG]</ta>
            <ta e="T376" id="Seg_7868" s="T375">вода-VBLZ-CVB</ta>
            <ta e="T377" id="Seg_7869" s="T376">какой=INDEF</ta>
            <ta e="T378" id="Seg_7870" s="T377">люди.[NOM.SG]</ta>
            <ta e="T379" id="Seg_7871" s="T378">идти-DUR-3PL</ta>
            <ta e="T380" id="Seg_7872" s="T379">я.NOM</ta>
            <ta e="T381" id="Seg_7873" s="T380">этот-ACC</ta>
            <ta e="T382" id="Seg_7874" s="T381">этот-ACC.PL</ta>
            <ta e="T383" id="Seg_7875" s="T382">NEG</ta>
            <ta e="T384" id="Seg_7876" s="T383">знать-1SG</ta>
            <ta e="T385" id="Seg_7877" s="T384">что.[NOM.SG]</ta>
            <ta e="T387" id="Seg_7878" s="T386">этот-PL</ta>
            <ta e="T388" id="Seg_7879" s="T387">там</ta>
            <ta e="T389" id="Seg_7880" s="T388">идти-DUR-3PL</ta>
            <ta e="T390" id="Seg_7881" s="T389">и</ta>
            <ta e="T391" id="Seg_7882" s="T390">этот.[NOM.SG]</ta>
            <ta e="T392" id="Seg_7883" s="T391">мальчик.[NOM.SG]</ta>
            <ta e="T393" id="Seg_7884" s="T392">и</ta>
            <ta e="T394" id="Seg_7885" s="T393">девушка.[NOM.SG]</ta>
            <ta e="T395" id="Seg_7886" s="T394">прийти-PRS-3PL</ta>
            <ta e="T396" id="Seg_7887" s="T395">надо</ta>
            <ta e="T397" id="Seg_7888" s="T396">молоко.[NOM.SG]</ta>
            <ta e="T398" id="Seg_7889" s="T397">лить-INF.LAT</ta>
            <ta e="T399" id="Seg_7890" s="T398">туес-LAT</ta>
            <ta e="T400" id="Seg_7891" s="T399">JUSS</ta>
            <ta e="T401" id="Seg_7892" s="T400">кислый.[NOM.SG]</ta>
            <ta e="T402" id="Seg_7893" s="T401">стать-FUT-3SG</ta>
            <ta e="T403" id="Seg_7894" s="T402">скоро</ta>
            <ta e="T404" id="Seg_7895" s="T403">пойти-CVB</ta>
            <ta e="T406" id="Seg_7896" s="T405">исчезнуть-PST-1PL</ta>
            <ta e="T407" id="Seg_7897" s="T406">ну</ta>
            <ta e="T408" id="Seg_7898" s="T407">пойти-EP-IMP.2SG</ta>
            <ta e="T409" id="Seg_7899" s="T408">пойти-IMP.2PL</ta>
            <ta e="T410" id="Seg_7900" s="T409">Бог-INS</ta>
            <ta e="T411" id="Seg_7901" s="T410">мы.NOM</ta>
            <ta e="T412" id="Seg_7902" s="T411">очень</ta>
            <ta e="T413" id="Seg_7903" s="T412">много</ta>
            <ta e="T414" id="Seg_7904" s="T413">говорить-PST-1PL</ta>
            <ta e="T415" id="Seg_7905" s="T414">сейчас</ta>
            <ta e="T416" id="Seg_7906" s="T415">пойти-INF.LAT</ta>
            <ta e="T417" id="Seg_7907" s="T416">надо</ta>
            <ta e="T418" id="Seg_7908" s="T417">чум-LAT/LOC.3SG</ta>
            <ta e="T419" id="Seg_7909" s="T418">и</ta>
            <ta e="T420" id="Seg_7910" s="T419">спать-INF.LAT</ta>
            <ta e="T421" id="Seg_7911" s="T420">лежать-INF.LAT</ta>
            <ta e="T422" id="Seg_7912" s="T421">NEG.AUX-IMP.2SG</ta>
            <ta e="T424" id="Seg_7913" s="T423">NEG.AUX-IMP.2SG</ta>
            <ta e="T425" id="Seg_7914" s="T424">хвастаться-MULT-EP-CNG</ta>
            <ta e="T426" id="Seg_7915" s="T425">что.[NOM.SG]=INDEF</ta>
            <ta e="T427" id="Seg_7916" s="T426">NEG</ta>
            <ta e="T428" id="Seg_7917" s="T427">делать-PST-2SG</ta>
            <ta e="T429" id="Seg_7918" s="T428">а</ta>
            <ta e="T430" id="Seg_7919" s="T429">PTCL</ta>
            <ta e="T431" id="Seg_7920" s="T430">хвастаться-MULT-PST-2SG</ta>
            <ta e="T432" id="Seg_7921" s="T431">спать-INF.LAT</ta>
            <ta e="T433" id="Seg_7922" s="T432">лежать-OPT.SG</ta>
            <ta e="T434" id="Seg_7923" s="T433">а</ta>
            <ta e="T435" id="Seg_7924" s="T434">завтра</ta>
            <ta e="T436" id="Seg_7925" s="T435">ум-LAT</ta>
            <ta e="T437" id="Seg_7926" s="T436">прийти-FUT-3SG</ta>
            <ta e="T438" id="Seg_7927" s="T437">%%-LAT</ta>
            <ta e="T439" id="Seg_7928" s="T438">мужчина.[NOM.SG]</ta>
            <ta e="T440" id="Seg_7929" s="T439">прийти-PST.[3SG]</ta>
            <ta e="T441" id="Seg_7930" s="T440">а</ta>
            <ta e="T442" id="Seg_7931" s="T441">я.NOM</ta>
            <ta e="T443" id="Seg_7932" s="T442">камень.[NOM.SG]</ta>
            <ta e="T444" id="Seg_7933" s="T443">хватать-MOM-PST-1SG</ta>
            <ta e="T449" id="Seg_7934" s="T448">камень.[NOM.SG]</ta>
            <ta e="T450" id="Seg_7935" s="T449">хватать-MOM-PST-1SG</ta>
            <ta e="T451" id="Seg_7936" s="T450">и</ta>
            <ta e="T452" id="Seg_7937" s="T451">убить-PST-1SG</ta>
            <ta e="T453" id="Seg_7938" s="T452">этот.[NOM.SG]</ta>
            <ta e="T454" id="Seg_7939" s="T453">мужчина-ACC</ta>
            <ta e="T455" id="Seg_7940" s="T454">мужчина.[NOM.SG]</ta>
            <ta e="T456" id="Seg_7941" s="T455">мужчина-ACC</ta>
            <ta e="T457" id="Seg_7942" s="T456">PTCL</ta>
            <ta e="T458" id="Seg_7943" s="T457">нож-INS</ta>
            <ta e="T459" id="Seg_7944" s="T458">зарезать-MOM-PST.[3SG]</ta>
            <ta e="T460" id="Seg_7945" s="T459">и</ta>
            <ta e="T461" id="Seg_7946" s="T460">этот.[NOM.SG]</ta>
            <ta e="T462" id="Seg_7947" s="T461">умереть-RES-PST</ta>
            <ta e="T463" id="Seg_7948" s="T462">там</ta>
            <ta e="T464" id="Seg_7949" s="T463">один.[NOM.SG]</ta>
            <ta e="T465" id="Seg_7950" s="T464">женщина.[NOM.SG]</ta>
            <ta e="T466" id="Seg_7951" s="T465">Агинское</ta>
            <ta e="T467" id="Seg_7952" s="T466">поселение-LOC</ta>
            <ta e="T468" id="Seg_7953" s="T467">один.[NOM.SG]</ta>
            <ta e="T469" id="Seg_7954" s="T468">женщина.[NOM.SG]</ta>
            <ta e="T470" id="Seg_7955" s="T469">человек-ACC.3SG</ta>
            <ta e="T471" id="Seg_7956" s="T470">PTCL</ta>
            <ta e="T472" id="Seg_7957" s="T471">резать-PST.[3SG]</ta>
            <ta e="T473" id="Seg_7958" s="T472">топор-INS</ta>
            <ta e="T474" id="Seg_7959" s="T473">голова-NOM/GEN.3SG</ta>
            <ta e="T475" id="Seg_7960" s="T474">от-резать-PST.[3SG]</ta>
            <ta e="T476" id="Seg_7961" s="T475">этот-GEN</ta>
            <ta e="T477" id="Seg_7962" s="T476">человек.[NOM.SG]</ta>
            <ta e="T478" id="Seg_7963" s="T477">один.[NOM.SG]</ta>
            <ta e="T479" id="Seg_7964" s="T478">спать-MOM-PST.[3SG]</ta>
            <ta e="T480" id="Seg_7965" s="T479">PTCL</ta>
            <ta e="T481" id="Seg_7966" s="T480">очень</ta>
            <ta e="T483" id="Seg_7967" s="T482">бояться-PST-1SG</ta>
            <ta e="T484" id="Seg_7968" s="T483">как</ta>
            <ta e="T485" id="Seg_7969" s="T484">я.LAT</ta>
            <ta e="T486" id="Seg_7970" s="T485">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T487" id="Seg_7971" s="T486">NEG</ta>
            <ta e="T488" id="Seg_7972" s="T487">резать-PST.[3SG]</ta>
            <ta e="T489" id="Seg_7973" s="T488">этот.[NOM.SG]</ta>
            <ta e="T490" id="Seg_7974" s="T489">PTCL</ta>
            <ta e="T491" id="Seg_7975" s="T490">беременная.[NOM.SG]</ta>
            <ta e="T492" id="Seg_7976" s="T491">быть-PST.[3SG]</ta>
            <ta e="T493" id="Seg_7977" s="T492">и</ta>
            <ta e="T494" id="Seg_7978" s="T493">два.[NOM.SG]</ta>
            <ta e="T495" id="Seg_7979" s="T494">ребенок-PL</ta>
            <ta e="T496" id="Seg_7980" s="T495">остаться-MOM-PST-3PL</ta>
            <ta e="T500" id="Seg_7981" s="T499">PTCL</ta>
            <ta e="T501" id="Seg_7982" s="T500">пять.[NOM.SG]</ta>
            <ta e="T502" id="Seg_7983" s="T501">год.[NOM.SG]</ta>
            <ta e="T503" id="Seg_7984" s="T502">пойти-CVB</ta>
            <ta e="T504" id="Seg_7985" s="T503">исчезнуть-PST.[3SG]</ta>
            <ta e="T505" id="Seg_7986" s="T504">вчера</ta>
            <ta e="T506" id="Seg_7987" s="T505">NEG</ta>
            <ta e="T507" id="Seg_7988" s="T506">быть-PST-2SG</ta>
            <ta e="T508" id="Seg_7989" s="T507">сегодня</ta>
            <ta e="T511" id="Seg_7990" s="T510">прийти-PST-2SG</ta>
            <ta e="T512" id="Seg_7991" s="T511">слушать-INF.LAT</ta>
            <ta e="T513" id="Seg_7992" s="T512">что.[NOM.SG]</ta>
            <ta e="T514" id="Seg_7993" s="T513">говорить-DUR-1PL</ta>
            <ta e="T515" id="Seg_7994" s="T514">я.NOM</ta>
            <ta e="T516" id="Seg_7995" s="T515">сегодня</ta>
            <ta e="T517" id="Seg_7996" s="T516">бог.[NOM.SG]</ta>
            <ta e="T518" id="Seg_7997" s="T517">бог-LAT</ta>
            <ta e="T521" id="Seg_7998" s="T520">молиться-FUT-1SG</ta>
            <ta e="T522" id="Seg_7999" s="T521">бог.[NOM.SG]</ta>
            <ta e="T523" id="Seg_8000" s="T522">прийти-IMP.2SG</ta>
            <ta e="T524" id="Seg_8001" s="T523">я.LAT</ta>
            <ta e="T525" id="Seg_8002" s="T524">слушать-IMP.2SG.O</ta>
            <ta e="T526" id="Seg_8003" s="T525">как</ta>
            <ta e="T527" id="Seg_8004" s="T526">я.NOM</ta>
            <ta e="T528" id="Seg_8005" s="T527">молиться-PRS-1SG</ta>
            <ta e="T529" id="Seg_8006" s="T528">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T531" id="Seg_8007" s="T530">смотреть-IMP.2SG</ta>
            <ta e="T532" id="Seg_8008" s="T531">наверное</ta>
            <ta e="T534" id="Seg_8009" s="T533">быть.чистым-PRS-3PL</ta>
            <ta e="T535" id="Seg_8010" s="T534">а</ta>
            <ta e="T536" id="Seg_8011" s="T535">NEG</ta>
            <ta e="T537" id="Seg_8012" s="T536">чистый.[NOM.SG]</ta>
            <ta e="T538" id="Seg_8013" s="T537">так</ta>
            <ta e="T539" id="Seg_8014" s="T538">мыть-IMP.2SG.O</ta>
            <ta e="T540" id="Seg_8015" s="T539">кровь-INS</ta>
            <ta e="T541" id="Seg_8016" s="T540">ты.NOM</ta>
            <ta e="T542" id="Seg_8017" s="T541">дерево-LOC</ta>
            <ta e="T543" id="Seg_8018" s="T542">вешать-PST-2SG</ta>
            <ta e="T544" id="Seg_8019" s="T543">кровь-NOM/GEN/ACC.1SG</ta>
            <ta e="T545" id="Seg_8020" s="T544">течь-DUR.PST.[3SG]</ta>
            <ta e="T546" id="Seg_8021" s="T545">мыть-IMP.2SG.O</ta>
            <ta e="T547" id="Seg_8022" s="T546">я.ACC</ta>
            <ta e="T548" id="Seg_8023" s="T547">хороший.[NOM.SG]</ta>
            <ta e="T549" id="Seg_8024" s="T548">мыть-IMP.2SG.O</ta>
            <ta e="T550" id="Seg_8025" s="T549">ты.NOM</ta>
            <ta e="T551" id="Seg_8026" s="T550">бог.[NOM.SG]</ta>
            <ta e="T552" id="Seg_8027" s="T551">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T553" id="Seg_8028" s="T552">много</ta>
            <ta e="T554" id="Seg_8029" s="T553">весь</ta>
            <ta e="T556" id="Seg_8030" s="T555">люди-EP-ACC</ta>
            <ta e="T558" id="Seg_8031" s="T557">жалеть-DUR-2SG</ta>
            <ta e="T560" id="Seg_8032" s="T559">хлеб.[NOM.SG]</ta>
            <ta e="T561" id="Seg_8033" s="T560">дать-PRS-2SG</ta>
            <ta e="T563" id="Seg_8034" s="T562">ты.NOM</ta>
            <ta e="T564" id="Seg_8035" s="T563">бог.[NOM.SG]</ta>
            <ta e="T565" id="Seg_8036" s="T564">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T566" id="Seg_8037" s="T565">много</ta>
            <ta e="T567" id="Seg_8038" s="T566">ты.NOM</ta>
            <ta e="T568" id="Seg_8039" s="T567">люди.[NOM.SG]</ta>
            <ta e="T569" id="Seg_8040" s="T568">люди.[NOM.SG]</ta>
            <ta e="T570" id="Seg_8041" s="T569">люди.[NOM.SG]</ta>
            <ta e="T571" id="Seg_8042" s="T570">жалеть-PRS-2SG</ta>
            <ta e="T572" id="Seg_8043" s="T571">хлеб.[NOM.SG]</ta>
            <ta e="T573" id="Seg_8044" s="T572">этот-PL-LAT</ta>
            <ta e="T574" id="Seg_8045" s="T573">дать-PRS-2SG</ta>
            <ta e="T575" id="Seg_8046" s="T574">PTCL</ta>
            <ta e="T576" id="Seg_8047" s="T575">рубашка.[NOM.SG]</ta>
            <ta e="T577" id="Seg_8048" s="T576">дать-PRS-2SG</ta>
            <ta e="T578" id="Seg_8049" s="T577">вода.[NOM.SG]</ta>
            <ta e="T579" id="Seg_8050" s="T578">дать-PRS-2SG</ta>
            <ta e="T580" id="Seg_8051" s="T579">и</ta>
            <ta e="T581" id="Seg_8052" s="T580">я.ACC</ta>
            <ta e="T582" id="Seg_8053" s="T581">жалеть-EP-IMP.2SG</ta>
            <ta e="T588" id="Seg_8054" s="T587">ты.NOM</ta>
            <ta e="T589" id="Seg_8055" s="T588">PTCL</ta>
            <ta e="T590" id="Seg_8056" s="T589">теплый.[NOM.SG]</ta>
            <ta e="T591" id="Seg_8057" s="T590">пускать-PRS-2SG</ta>
            <ta e="T592" id="Seg_8058" s="T591">дождь.[NOM.SG]</ta>
            <ta e="T593" id="Seg_8059" s="T592">дать-PRS-2SG</ta>
            <ta e="T594" id="Seg_8060" s="T593">чтобы</ta>
            <ta e="T595" id="Seg_8061" s="T594">хлеб.[NOM.SG]</ta>
            <ta e="T596" id="Seg_8062" s="T595">расти-DUR.PST.[3SG]</ta>
            <ta e="T600" id="Seg_8063" s="T599">бог-INS</ta>
            <ta e="T601" id="Seg_8064" s="T600">хороший.[NOM.SG]</ta>
            <ta e="T602" id="Seg_8065" s="T601">куда</ta>
            <ta e="T603" id="Seg_8066" s="T602">пойти-FUT-2SG</ta>
            <ta e="T604" id="Seg_8067" s="T603">Бог-INS</ta>
            <ta e="T607" id="Seg_8068" s="T606">очень</ta>
            <ta e="T608" id="Seg_8069" s="T607">хороший.[NOM.SG]</ta>
            <ta e="T609" id="Seg_8070" s="T608">Бог-INS</ta>
            <ta e="T610" id="Seg_8071" s="T609">ветер.[NOM.SG]</ta>
            <ta e="T611" id="Seg_8072" s="T610">упасть-MOM-PST.[3SG]</ta>
            <ta e="T612" id="Seg_8073" s="T611">я.NOM</ta>
            <ta e="T613" id="Seg_8074" s="T612">взять-PST-1SG</ta>
            <ta e="T614" id="Seg_8075" s="T613">и</ta>
            <ta e="T615" id="Seg_8076" s="T614">дом-LAT</ta>
            <ta e="T616" id="Seg_8077" s="T615">принести-PST-1SG</ta>
            <ta e="T617" id="Seg_8078" s="T616">хороший.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8079" s="T617">дождь.[NOM.SG]</ta>
            <ta e="T619" id="Seg_8080" s="T618">прийти-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_8081" s="T619">хлеб.[NOM.SG]</ta>
            <ta e="T621" id="Seg_8082" s="T620">расти-FUT-3SG</ta>
            <ta e="T622" id="Seg_8083" s="T621">я.NOM</ta>
            <ta e="T623" id="Seg_8084" s="T622">этот-LAT</ta>
            <ta e="T624" id="Seg_8085" s="T623">помогать-PST-1SG</ta>
            <ta e="T625" id="Seg_8086" s="T624">а</ta>
            <ta e="T626" id="Seg_8087" s="T625">этот.[NOM.SG]</ta>
            <ta e="T627" id="Seg_8088" s="T626">я.LAT</ta>
            <ta e="T628" id="Seg_8089" s="T627">NEG</ta>
            <ta e="T629" id="Seg_8090" s="T628">помогать-DUR.[3SG]</ta>
            <ta e="T630" id="Seg_8091" s="T629">NEG</ta>
            <ta e="T631" id="Seg_8092" s="T630">дать-PRS.[3SG]</ta>
            <ta e="T632" id="Seg_8093" s="T631">я.LAT</ta>
            <ta e="T633" id="Seg_8094" s="T632">работать-INF.LAT</ta>
            <ta e="T634" id="Seg_8095" s="T633">я.NOM</ta>
            <ta e="T635" id="Seg_8096" s="T634">IRREAL</ta>
            <ta e="T636" id="Seg_8097" s="T635">работать-PST-1SG</ta>
            <ta e="T637" id="Seg_8098" s="T636">а</ta>
            <ta e="T638" id="Seg_8099" s="T637">этот.[NOM.SG]</ta>
            <ta e="T639" id="Seg_8100" s="T638">всегда</ta>
            <ta e="T640" id="Seg_8101" s="T639">мешать-DUR.[3SG]</ta>
            <ta e="T641" id="Seg_8102" s="T640">я.LAT</ta>
            <ta e="T642" id="Seg_8103" s="T641">сердитый.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8104" s="T642">PTCL</ta>
            <ta e="T644" id="Seg_8105" s="T643">нога-INS</ta>
            <ta e="T645" id="Seg_8106" s="T644">PTCL</ta>
            <ta e="T646" id="Seg_8107" s="T645">топнуть-PST.[3SG]</ta>
            <ta e="T647" id="Seg_8108" s="T646">очень</ta>
            <ta e="T648" id="Seg_8109" s="T647">сердитый.[NOM.SG]</ta>
            <ta e="T649" id="Seg_8110" s="T648">собака.[NOM.SG]</ta>
            <ta e="T651" id="Seg_8111" s="T650">весь</ta>
            <ta e="T653" id="Seg_8112" s="T652">укусить-PRS.[3SG]</ta>
            <ta e="T654" id="Seg_8113" s="T653">люди.[NOM.SG]</ta>
            <ta e="T657" id="Seg_8114" s="T656">очень</ta>
            <ta e="T658" id="Seg_8115" s="T657">сердитый.[NOM.SG]</ta>
            <ta e="T659" id="Seg_8116" s="T658">собака.[NOM.SG]</ta>
            <ta e="T661" id="Seg_8117" s="T660">весь</ta>
            <ta e="T662" id="Seg_8118" s="T661">люди.[NOM.SG]</ta>
            <ta e="T663" id="Seg_8119" s="T662">укусить-FRQ-DUR.[3SG]</ta>
            <ta e="T664" id="Seg_8120" s="T663">и</ta>
            <ta e="T665" id="Seg_8121" s="T664">я.ACC</ta>
            <ta e="T666" id="Seg_8122" s="T665">укусить-FRQ-PST.[3SG]</ta>
            <ta e="T667" id="Seg_8123" s="T666">я.NOM</ta>
            <ta e="T668" id="Seg_8124" s="T667">вы.NOM</ta>
            <ta e="T669" id="Seg_8125" s="T668">ждать-PST-1SG</ta>
            <ta e="T670" id="Seg_8126" s="T669">вы.NOM</ta>
            <ta e="T671" id="Seg_8127" s="T670">NEG</ta>
            <ta e="T672" id="Seg_8128" s="T671">прийти-PST-2PL</ta>
            <ta e="T673" id="Seg_8129" s="T672">долго</ta>
            <ta e="T674" id="Seg_8130" s="T673">NEG.EX-PST-2PL</ta>
            <ta e="T675" id="Seg_8131" s="T674">чашка-ABL</ta>
            <ta e="T676" id="Seg_8132" s="T675">взять-PST-1SG</ta>
            <ta e="T677" id="Seg_8133" s="T676">мыть-PST-1SG</ta>
            <ta e="T678" id="Seg_8134" s="T677">и</ta>
            <ta e="T679" id="Seg_8135" s="T678">опять</ta>
            <ta e="T680" id="Seg_8136" s="T679">класть-PST-1SG</ta>
            <ta e="T681" id="Seg_8137" s="T680">%%</ta>
            <ta e="T682" id="Seg_8138" s="T681">%%</ta>
            <ta e="T683" id="Seg_8139" s="T682">сегодня</ta>
            <ta e="T684" id="Seg_8140" s="T683">PTCL</ta>
            <ta e="T687" id="Seg_8141" s="T686">вода-LAT</ta>
            <ta e="T688" id="Seg_8142" s="T687">NEG</ta>
            <ta e="T689" id="Seg_8143" s="T688">упасть-PRS-2SG</ta>
            <ta e="T690" id="Seg_8144" s="T689">%%</ta>
            <ta e="T691" id="Seg_8145" s="T690">PTCL</ta>
            <ta e="T692" id="Seg_8146" s="T691">идти-IMP</ta>
            <ta e="T693" id="Seg_8147" s="T692">рука-INS</ta>
            <ta e="T694" id="Seg_8148" s="T693">и</ta>
            <ta e="T695" id="Seg_8149" s="T694">нога-INS</ta>
            <ta e="T696" id="Seg_8150" s="T695">вода-LOC</ta>
            <ta e="T697" id="Seg_8151" s="T696">PTCL</ta>
            <ta e="T698" id="Seg_8152" s="T697">идти-PRS-2SG</ta>
            <ta e="T699" id="Seg_8153" s="T698">рука-INS</ta>
            <ta e="T700" id="Seg_8154" s="T699">и</ta>
            <ta e="T701" id="Seg_8155" s="T700">нога-INS</ta>
            <ta e="T702" id="Seg_8156" s="T701">лодка</ta>
            <ta e="T703" id="Seg_8157" s="T702">лодка-PL-INS</ta>
            <ta e="T704" id="Seg_8158" s="T703">PTCL</ta>
            <ta e="T706" id="Seg_8159" s="T705">вода-LAT</ta>
            <ta e="T707" id="Seg_8160" s="T706">идти-PRS.[3SG]</ta>
            <ta e="T708" id="Seg_8161" s="T707">рыба.[NOM.SG]</ta>
            <ta e="T709" id="Seg_8162" s="T708">ловить-DUR.[3SG]</ta>
            <ta e="T710" id="Seg_8163" s="T709">PTCL</ta>
            <ta e="T711" id="Seg_8164" s="T710">три-COLL</ta>
            <ta e="T712" id="Seg_8165" s="T711">%%-FUT-1PL</ta>
            <ta e="T713" id="Seg_8166" s="T712">один.[NOM.SG]</ta>
            <ta e="T714" id="Seg_8167" s="T713">ты.DAT</ta>
            <ta e="T715" id="Seg_8168" s="T714">один.[NOM.SG]</ta>
            <ta e="T716" id="Seg_8169" s="T715">я.LAT</ta>
            <ta e="T717" id="Seg_8170" s="T716">один.[NOM.SG]</ta>
            <ta e="T718" id="Seg_8171" s="T717">этот-LAT</ta>
            <ta e="T719" id="Seg_8172" s="T718">два.[NOM.SG]</ta>
            <ta e="T720" id="Seg_8173" s="T719">часть.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8174" s="T720">три.[NOM.SG]</ta>
            <ta e="T722" id="Seg_8175" s="T721">часть.[NOM.SG]</ta>
            <ta e="T723" id="Seg_8176" s="T722">четыре</ta>
            <ta e="T724" id="Seg_8177" s="T723">часть.[NOM.SG]</ta>
            <ta e="T725" id="Seg_8178" s="T724">вдоль</ta>
            <ta e="T726" id="Seg_8179" s="T725">этот.[NOM.SG]</ta>
            <ta e="T727" id="Seg_8180" s="T726">этот-PL</ta>
            <ta e="T728" id="Seg_8181" s="T727">PTCL</ta>
            <ta e="T729" id="Seg_8182" s="T728">сидеть-PST-3PL</ta>
            <ta e="T730" id="Seg_8183" s="T729">один.[NOM.SG]</ta>
            <ta e="T731" id="Seg_8184" s="T730">лошадь-LOC</ta>
            <ta e="T732" id="Seg_8185" s="T731">я.NOM</ta>
            <ta e="T733" id="Seg_8186" s="T732">сидеть-PST-1SG</ta>
            <ta e="T734" id="Seg_8187" s="T733">два.[NOM.SG]</ta>
            <ta e="T735" id="Seg_8188" s="T734">лошадь-LOC</ta>
            <ta e="T736" id="Seg_8189" s="T735">женщина-ACC</ta>
            <ta e="T737" id="Seg_8190" s="T736">сесть-PST.[3SG]</ta>
            <ta e="T738" id="Seg_8191" s="T737">три.[NOM.SG]</ta>
            <ta e="T739" id="Seg_8192" s="T738">лошадь-LOC</ta>
            <ta e="T740" id="Seg_8193" s="T739">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T741" id="Seg_8194" s="T740">сесть-PST.[3SG]</ta>
            <ta e="T742" id="Seg_8195" s="T741">четыре</ta>
            <ta e="T743" id="Seg_8196" s="T742">лошадь-LOC</ta>
            <ta e="T744" id="Seg_8197" s="T743">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T745" id="Seg_8198" s="T744">сесть-PST.[3SG]</ta>
            <ta e="T746" id="Seg_8199" s="T745">а</ta>
            <ta e="T747" id="Seg_8200" s="T746">пять.[NOM.SG]</ta>
            <ta e="T748" id="Seg_8201" s="T747">лошадь-LOC</ta>
            <ta e="T749" id="Seg_8202" s="T748">бабушка.[NOM.SG]</ta>
            <ta e="T750" id="Seg_8203" s="T749">сесть-PST.[3SG]</ta>
            <ta e="T752" id="Seg_8204" s="T751">этот.[NOM.SG]</ta>
            <ta e="T753" id="Seg_8205" s="T752">мальчик.[NOM.SG]</ta>
            <ta e="T754" id="Seg_8206" s="T753">прийти-PST.[3SG]</ta>
            <ta e="T755" id="Seg_8207" s="T754">мы.ACC</ta>
            <ta e="T756" id="Seg_8208" s="T755">фотографировать-INF.LAT</ta>
            <ta e="T757" id="Seg_8209" s="T756">PTCL</ta>
            <ta e="T758" id="Seg_8210" s="T757">скоро</ta>
            <ta e="T759" id="Seg_8211" s="T758">PTCL</ta>
            <ta e="T760" id="Seg_8212" s="T759">холодный.[NOM.SG]</ta>
            <ta e="T761" id="Seg_8213" s="T760">стать-FUT-3SG</ta>
            <ta e="T763" id="Seg_8214" s="T762">снег.[NOM.SG]</ta>
            <ta e="T764" id="Seg_8215" s="T763">PTCL</ta>
            <ta e="T765" id="Seg_8216" s="T764">упасть-FUT-3SG</ta>
            <ta e="T766" id="Seg_8217" s="T765">очень</ta>
            <ta e="T767" id="Seg_8218" s="T766">холодный.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8219" s="T767">мерзнуть-FUT-2SG</ta>
            <ta e="T769" id="Seg_8220" s="T768">PTCL</ta>
            <ta e="T770" id="Seg_8221" s="T769">скоро</ta>
            <ta e="T773" id="Seg_8222" s="T772">весь</ta>
            <ta e="T774" id="Seg_8223" s="T773">теплый.[NOM.SG]</ta>
            <ta e="T775" id="Seg_8224" s="T774">стать-FUT-3SG</ta>
            <ta e="T776" id="Seg_8225" s="T775">тогда</ta>
            <ta e="T777" id="Seg_8226" s="T776">люди.[NOM.SG]</ta>
            <ta e="T778" id="Seg_8227" s="T777">плуг-MOM-PST-3PL</ta>
            <ta e="T779" id="Seg_8228" s="T778">PTCL</ta>
            <ta e="T780" id="Seg_8229" s="T779">мука</ta>
            <ta e="T781" id="Seg_8230" s="T780">сеять-MOM-3PL</ta>
            <ta e="T782" id="Seg_8231" s="T781">очень</ta>
            <ta e="T783" id="Seg_8232" s="T782">далеко</ta>
            <ta e="T784" id="Seg_8233" s="T783">лететь-DUR.[3SG]</ta>
            <ta e="T786" id="Seg_8234" s="T785">верх-LAT</ta>
            <ta e="T787" id="Seg_8235" s="T786">NEG</ta>
            <ta e="T788" id="Seg_8236" s="T787">быть.видным-PRS.[3SG]</ta>
            <ta e="T790" id="Seg_8237" s="T788">и</ta>
            <ta e="T791" id="Seg_8238" s="T790">лошадь-ABL</ta>
            <ta e="T794" id="Seg_8239" s="T793">лошадь-ABL</ta>
            <ta e="T795" id="Seg_8240" s="T794">PTCL</ta>
            <ta e="T796" id="Seg_8241" s="T795">место-LAT</ta>
            <ta e="T797" id="Seg_8242" s="T796">упасть-MOM-PST.[3SG]</ta>
            <ta e="T798" id="Seg_8243" s="T797">вверх</ta>
            <ta e="T799" id="Seg_8244" s="T798">PTCL</ta>
            <ta e="T800" id="Seg_8245" s="T799">надо</ta>
            <ta e="T801" id="Seg_8246" s="T800">пойти-INF.LAT</ta>
            <ta e="T802" id="Seg_8247" s="T801">очень</ta>
            <ta e="T807" id="Seg_8248" s="T806">очень</ta>
            <ta e="T808" id="Seg_8249" s="T807">бог.[NOM.SG]</ta>
            <ta e="T809" id="Seg_8250" s="T808">вверх</ta>
            <ta e="T810" id="Seg_8251" s="T809">жить-DUR.[3SG]</ta>
            <ta e="T811" id="Seg_8252" s="T810">гора-LOC</ta>
            <ta e="T814" id="Seg_8253" s="T813">большой.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8254" s="T815">гора.[NOM.SG]</ta>
            <ta e="T817" id="Seg_8255" s="T816">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T818" id="Seg_8256" s="T817">PTCL</ta>
            <ta e="T819" id="Seg_8257" s="T818">чесаться-PRS.[3SG]</ta>
            <ta e="T820" id="Seg_8258" s="T819">чесать-PRS-1SG</ta>
            <ta e="T821" id="Seg_8259" s="T820">наверное</ta>
            <ta e="T822" id="Seg_8260" s="T821">вошь.[NOM.SG]</ta>
            <ta e="T823" id="Seg_8261" s="T822">много</ta>
            <ta e="T824" id="Seg_8262" s="T823">надо</ta>
            <ta e="T825" id="Seg_8263" s="T824">этот-PL</ta>
            <ta e="T826" id="Seg_8264" s="T825">PTCL</ta>
            <ta e="T828" id="Seg_8265" s="T826">убить-INF.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_8266" s="T4">n-n:case.poss</ta>
            <ta e="T6" id="Seg_8267" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_8268" s="T6">adj-n:case</ta>
            <ta e="T8" id="Seg_8269" s="T7">num-n:case</ta>
            <ta e="T9" id="Seg_8270" s="T8">adv</ta>
            <ta e="T10" id="Seg_8271" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_8272" s="T10">adj-n:case</ta>
            <ta e="T12" id="Seg_8273" s="T11">adv</ta>
            <ta e="T13" id="Seg_8274" s="T12">n-n:case.poss</ta>
            <ta e="T15" id="Seg_8275" s="T14">pers</ta>
            <ta e="T16" id="Seg_8276" s="T15">n-n:case.poss</ta>
            <ta e="T17" id="Seg_8277" s="T16">quant</ta>
            <ta e="T18" id="Seg_8278" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_8279" s="T18">v-v:n.fin</ta>
            <ta e="T20" id="Seg_8280" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_8281" s="T20">pers</ta>
            <ta e="T22" id="Seg_8282" s="T21">num-num&gt;num</ta>
            <ta e="T24" id="Seg_8283" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_8284" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_8285" s="T25">dempro-n:case</ta>
            <ta e="T28" id="Seg_8286" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_8287" s="T28">adv</ta>
            <ta e="T30" id="Seg_8288" s="T29">pers</ta>
            <ta e="T31" id="Seg_8289" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_8290" s="T31">adv</ta>
            <ta e="T33" id="Seg_8291" s="T32">que</ta>
            <ta e="T34" id="Seg_8292" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_8293" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_8294" s="T35">pers</ta>
            <ta e="T37" id="Seg_8295" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_8296" s="T37">v-v:pn</ta>
            <ta e="T39" id="Seg_8297" s="T38">n-n:num</ta>
            <ta e="T40" id="Seg_8298" s="T39">quant</ta>
            <ta e="T41" id="Seg_8299" s="T40">adj-n:case</ta>
            <ta e="T42" id="Seg_8300" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_8301" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_8302" s="T43">conj</ta>
            <ta e="T45" id="Seg_8303" s="T44">n-n:case</ta>
            <ta e="T47" id="Seg_8304" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_8305" s="T47">adv</ta>
            <ta e="T49" id="Seg_8306" s="T48">adv</ta>
            <ta e="T50" id="Seg_8307" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_8308" s="T50">n-n:case</ta>
            <ta e="T53" id="Seg_8309" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_8310" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_8311" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_8312" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_8313" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8314" s="T57">v-v&gt;v-v:pn</ta>
            <ta e="T59" id="Seg_8315" s="T58">adv</ta>
            <ta e="T60" id="Seg_8316" s="T59">adj-n:case</ta>
            <ta e="T61" id="Seg_8317" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_8318" s="T61">que</ta>
            <ta e="T63" id="Seg_8319" s="T62">dempro-n:case</ta>
            <ta e="T64" id="Seg_8320" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_8321" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_8322" s="T65">v-v:pn</ta>
            <ta e="T67" id="Seg_8323" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_8324" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_8325" s="T68">quant</ta>
            <ta e="T72" id="Seg_8326" s="T71">v</ta>
            <ta e="T73" id="Seg_8327" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_8328" s="T73">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_8329" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_8330" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_8331" s="T76">v-v:n.fin</ta>
            <ta e="T80" id="Seg_8332" s="T78">n-n:case</ta>
            <ta e="T82" id="Seg_8333" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_8334" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_8335" s="T83">v-v:n.fin</ta>
            <ta e="T85" id="Seg_8336" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_8337" s="T85">v-v:n.fin</ta>
            <ta e="T87" id="Seg_8338" s="T86">que</ta>
            <ta e="T89" id="Seg_8339" s="T88">dempro-n:num</ta>
            <ta e="T90" id="Seg_8340" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_8341" s="T90">adv-adv&gt;adv</ta>
            <ta e="T92" id="Seg_8342" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_8343" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_8344" s="T93">adv</ta>
            <ta e="T95" id="Seg_8345" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_8346" s="T95">n-n:case-n:num</ta>
            <ta e="T97" id="Seg_8347" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_8348" s="T97">adv-adv&gt;adv</ta>
            <ta e="T99" id="Seg_8349" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_8350" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_8351" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_8352" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_8353" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_8354" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_8355" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_8356" s="T105">v-v:ins-v:mood.pn</ta>
            <ta e="T107" id="Seg_8357" s="T106">pers</ta>
            <ta e="T108" id="Seg_8358" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_8359" s="T108">conj</ta>
            <ta e="T110" id="Seg_8360" s="T109">adv</ta>
            <ta e="T111" id="Seg_8361" s="T110">aux-v:mood.pn</ta>
            <ta e="T112" id="Seg_8362" s="T111">v-v:n.fin</ta>
            <ta e="T113" id="Seg_8363" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_8364" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_8365" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_8366" s="T115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_8367" s="T116">conj</ta>
            <ta e="T118" id="Seg_8368" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_8369" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_8370" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_8371" s="T120">adv</ta>
            <ta e="T122" id="Seg_8372" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_8373" s="T122">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_8374" s="T123">%%</ta>
            <ta e="T125" id="Seg_8375" s="T124">n-n:num</ta>
            <ta e="T126" id="Seg_8376" s="T125">v-v:pn</ta>
            <ta e="T127" id="Seg_8377" s="T126">que=ptcl</ta>
            <ta e="T128" id="Seg_8378" s="T127">v-v:n.fin</ta>
            <ta e="T129" id="Seg_8379" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_8380" s="T129">conj</ta>
            <ta e="T131" id="Seg_8381" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_8382" s="T131">quant</ta>
            <ta e="T133" id="Seg_8383" s="T132">pers</ta>
            <ta e="T134" id="Seg_8384" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_8385" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_8386" s="T135">v-v&gt;v-v:pn</ta>
            <ta e="T137" id="Seg_8387" s="T136">pers</ta>
            <ta e="T138" id="Seg_8388" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_8389" s="T138">n-n:case</ta>
            <ta e="T141" id="Seg_8390" s="T140">v</ta>
            <ta e="T142" id="Seg_8391" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_8392" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_8393" s="T143">adj-n:case</ta>
            <ta e="T145" id="Seg_8394" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_8395" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_8396" s="T146">adj-n:case</ta>
            <ta e="T149" id="Seg_8397" s="T148">v-v&gt;v-v:pn</ta>
            <ta e="T150" id="Seg_8398" s="T149">adj</ta>
            <ta e="T151" id="Seg_8399" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_8400" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_8401" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_8402" s="T153">adj-n:case</ta>
            <ta e="T156" id="Seg_8403" s="T154">n-n:num</ta>
            <ta e="T157" id="Seg_8404" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_8405" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_8406" s="T158">dempro-n:case</ta>
            <ta e="T161" id="Seg_8407" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_8408" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_8409" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_8410" s="T163">adv</ta>
            <ta e="T165" id="Seg_8411" s="T164">adj-n:case</ta>
            <ta e="T166" id="Seg_8412" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_8413" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_8414" s="T167">adv</ta>
            <ta e="T169" id="Seg_8415" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_8416" s="T169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_8417" s="T170">adv</ta>
            <ta e="T172" id="Seg_8418" s="T171">n-n:case.poss</ta>
            <ta e="T173" id="Seg_8419" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_8420" s="T173">n-n:case.poss</ta>
            <ta e="T175" id="Seg_8421" s="T174">adv</ta>
            <ta e="T176" id="Seg_8422" s="T175">adv</ta>
            <ta e="T177" id="Seg_8423" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_8424" s="T177">adv</ta>
            <ta e="T179" id="Seg_8425" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8426" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_8427" s="T180">ptcl</ta>
            <ta e="T183" id="Seg_8428" s="T181">n-n:case</ta>
            <ta e="T184" id="Seg_8429" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_8430" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_8431" s="T185">aux-v:mood.pn</ta>
            <ta e="T187" id="Seg_8432" s="T186">v-v:ins-v:n.fin</ta>
            <ta e="T192" id="Seg_8433" s="T191">v-v:ins-v:mood.pn</ta>
            <ta e="T193" id="Seg_8434" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_8435" s="T193">n-n:case</ta>
            <ta e="T196" id="Seg_8436" s="T195">n-n:case.poss</ta>
            <ta e="T197" id="Seg_8437" s="T196">n-n:ins-n:case</ta>
            <ta e="T198" id="Seg_8438" s="T197">n-n:case.poss</ta>
            <ta e="T199" id="Seg_8439" s="T198">dempro-n:case</ta>
            <ta e="T200" id="Seg_8440" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_8441" s="T200">pers</ta>
            <ta e="T203" id="Seg_8442" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_8443" s="T203">n-n:num</ta>
            <ta e="T206" id="Seg_8444" s="T205">v-v:mood.pn</ta>
            <ta e="T207" id="Seg_8445" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_8446" s="T207">adv</ta>
            <ta e="T209" id="Seg_8447" s="T208">quant</ta>
            <ta e="T210" id="Seg_8448" s="T209">adj-n:case</ta>
            <ta e="T211" id="Seg_8449" s="T210">n</ta>
            <ta e="T212" id="Seg_8450" s="T211">v-v:mood.pn</ta>
            <ta e="T213" id="Seg_8451" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_8452" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_8453" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_8454" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_8455" s="T216">pers</ta>
            <ta e="T218" id="Seg_8456" s="T217">dempro-n:case</ta>
            <ta e="T219" id="Seg_8457" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_8458" s="T219">pers</ta>
            <ta e="T221" id="Seg_8459" s="T220">n-n:case.poss</ta>
            <ta e="T222" id="Seg_8460" s="T221">quant</ta>
            <ta e="T223" id="Seg_8461" s="T222">pers</ta>
            <ta e="T224" id="Seg_8462" s="T223">adv</ta>
            <ta e="T225" id="Seg_8463" s="T224">adj-n:case</ta>
            <ta e="T226" id="Seg_8464" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_8465" s="T226">dempro-n:case</ta>
            <ta e="T228" id="Seg_8466" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_8467" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_8468" s="T229">n-n:case.poss</ta>
            <ta e="T231" id="Seg_8469" s="T230">v-v:pn</ta>
            <ta e="T232" id="Seg_8470" s="T231">n-n:case.poss</ta>
            <ta e="T233" id="Seg_8471" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_8472" s="T233">v-v:n.fin</ta>
            <ta e="T235" id="Seg_8473" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_8474" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_8475" s="T236">n-n:case.poss</ta>
            <ta e="T238" id="Seg_8476" s="T237">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_8477" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_8478" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_8479" s="T240">aux-v:mood.pn</ta>
            <ta e="T242" id="Seg_8480" s="T241">v-v:n.fin</ta>
            <ta e="T243" id="Seg_8481" s="T242">adj-n:case</ta>
            <ta e="T244" id="Seg_8482" s="T243">v-v:mood.pn</ta>
            <ta e="T245" id="Seg_8483" s="T244">que-n:case</ta>
            <ta e="T246" id="Seg_8484" s="T245">v-v&gt;v-v:pn</ta>
            <ta e="T247" id="Seg_8485" s="T246">refl-n:case.poss</ta>
            <ta e="T248" id="Seg_8486" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_8487" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_8488" s="T249">adj-n:case</ta>
            <ta e="T251" id="Seg_8489" s="T250">v-v:tense-v:pn</ta>
            <ta e="T252" id="Seg_8490" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_8491" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_8492" s="T253">conj</ta>
            <ta e="T255" id="Seg_8493" s="T254">v-v&gt;v-v:pn</ta>
            <ta e="T256" id="Seg_8494" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_8495" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_8496" s="T257">dempro-n:case</ta>
            <ta e="T259" id="Seg_8497" s="T258">v-v:n.fin</ta>
            <ta e="T260" id="Seg_8498" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_8499" s="T260">dempro-n:case</ta>
            <ta e="T262" id="Seg_8500" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_8501" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_8502" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_8503" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_8504" s="T266">v-v:n.fin</ta>
            <ta e="T268" id="Seg_8505" s="T267">n-n:num</ta>
            <ta e="T273" id="Seg_8506" s="T272">v-v:n.fin</ta>
            <ta e="T274" id="Seg_8507" s="T273">n</ta>
            <ta e="T275" id="Seg_8508" s="T274">v-v:n.fin</ta>
            <ta e="T276" id="Seg_8509" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_8510" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_8511" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_8512" s="T278">v-v:pn</ta>
            <ta e="T280" id="Seg_8513" s="T279">que-n:case</ta>
            <ta e="T281" id="Seg_8514" s="T280">dempro-n:case</ta>
            <ta e="T282" id="Seg_8515" s="T281">v-v:n.fin</ta>
            <ta e="T284" id="Seg_8516" s="T283">propr-n:case</ta>
            <ta e="T285" id="Seg_8517" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_8518" s="T285">v-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_8519" s="T286">n-n&gt;adj-n:case</ta>
            <ta e="T288" id="Seg_8520" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_8521" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_8522" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_8523" s="T290">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_8524" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_8525" s="T294">%%</ta>
            <ta e="T296" id="Seg_8526" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_8527" s="T296">dempro-n:case</ta>
            <ta e="T298" id="Seg_8528" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_8529" s="T298">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_8530" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_8531" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_8532" s="T301">adv</ta>
            <ta e="T303" id="Seg_8533" s="T302">quant</ta>
            <ta e="T304" id="Seg_8534" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_8535" s="T304">adv</ta>
            <ta e="T306" id="Seg_8536" s="T305">adv</ta>
            <ta e="T307" id="Seg_8537" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_8538" s="T307">quant</ta>
            <ta e="T309" id="Seg_8539" s="T308">propr-n:case</ta>
            <ta e="T310" id="Seg_8540" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_8541" s="T310">conj</ta>
            <ta e="T312" id="Seg_8542" s="T311">v-v:tense-v:pn</ta>
            <ta e="T313" id="Seg_8543" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_8544" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_8545" s="T314">n-n:case.poss</ta>
            <ta e="T316" id="Seg_8546" s="T315">v-v&gt;v-v:pn</ta>
            <ta e="T317" id="Seg_8547" s="T316">adv</ta>
            <ta e="T318" id="Seg_8548" s="T317">propr-n:case</ta>
            <ta e="T319" id="Seg_8549" s="T318">v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_8550" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_8551" s="T320">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_8552" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_8553" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_8554" s="T323">conj</ta>
            <ta e="T325" id="Seg_8555" s="T324">n-n:case.poss</ta>
            <ta e="T326" id="Seg_8556" s="T325">v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_8557" s="T326">adv</ta>
            <ta e="T328" id="Seg_8558" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_8559" s="T328">v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_8560" s="T329">v-v&gt;v-v:pn</ta>
            <ta e="T331" id="Seg_8561" s="T330">conj</ta>
            <ta e="T333" id="Seg_8562" s="T332">adj-v&gt;v-v:pn</ta>
            <ta e="T339" id="Seg_8563" s="T338">n-n:case.poss</ta>
            <ta e="T340" id="Seg_8564" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_8565" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_8566" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_8567" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_8568" s="T343">n-n:num</ta>
            <ta e="T345" id="Seg_8569" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_8570" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_8571" s="T346">adv</ta>
            <ta e="T348" id="Seg_8572" s="T347">adv</ta>
            <ta e="T349" id="Seg_8573" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_8574" s="T349">adj-n:case</ta>
            <ta e="T351" id="Seg_8575" s="T350">pers</ta>
            <ta e="T352" id="Seg_8576" s="T351">dempro-n:case</ta>
            <ta e="T353" id="Seg_8577" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_8578" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_8579" s="T354">conj</ta>
            <ta e="T356" id="Seg_8580" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_8581" s="T356">v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_8582" s="T357">v-v:tense-v:pn</ta>
            <ta e="T359" id="Seg_8583" s="T358">conj</ta>
            <ta e="T360" id="Seg_8584" s="T359">adj-n:case</ta>
            <ta e="T361" id="Seg_8585" s="T360">v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_8586" s="T361">adv</ta>
            <ta e="T363" id="Seg_8587" s="T362">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T364" id="Seg_8588" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_8589" s="T364">adv</ta>
            <ta e="T366" id="Seg_8590" s="T365">v-v:tense-v:pn</ta>
            <ta e="T367" id="Seg_8591" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_8592" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8593" s="T368">v-v&gt;v-v:pn</ta>
            <ta e="T370" id="Seg_8594" s="T369">dempro-n:case</ta>
            <ta e="T371" id="Seg_8595" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_8596" s="T371">v-v&gt;v-v:pn</ta>
            <ta e="T373" id="Seg_8597" s="T372">conj</ta>
            <ta e="T374" id="Seg_8598" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_8599" s="T374">v-v&gt;v-v:pn</ta>
            <ta e="T376" id="Seg_8600" s="T375">n-n&gt;v-v:n.fin</ta>
            <ta e="T377" id="Seg_8601" s="T376">que=ptcl</ta>
            <ta e="T378" id="Seg_8602" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_8603" s="T378">v-v&gt;v-v:pn</ta>
            <ta e="T380" id="Seg_8604" s="T379">pers</ta>
            <ta e="T381" id="Seg_8605" s="T380">dempro-n:case</ta>
            <ta e="T382" id="Seg_8606" s="T381">dempro-n:case</ta>
            <ta e="T383" id="Seg_8607" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_8608" s="T383">v-v:pn</ta>
            <ta e="T385" id="Seg_8609" s="T384">que-n:case</ta>
            <ta e="T387" id="Seg_8610" s="T386">dempro-n:num</ta>
            <ta e="T388" id="Seg_8611" s="T387">adv</ta>
            <ta e="T389" id="Seg_8612" s="T388">v-v&gt;v-v:pn</ta>
            <ta e="T390" id="Seg_8613" s="T389">conj</ta>
            <ta e="T391" id="Seg_8614" s="T390">dempro-n:case</ta>
            <ta e="T392" id="Seg_8615" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_8616" s="T392">conj</ta>
            <ta e="T394" id="Seg_8617" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_8618" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_8619" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_8620" s="T396">n.[n:case]</ta>
            <ta e="T398" id="Seg_8621" s="T397">v-v:n.fin</ta>
            <ta e="T399" id="Seg_8622" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_8623" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_8624" s="T400">adj-n:case</ta>
            <ta e="T402" id="Seg_8625" s="T401">v-v:tense-v:pn</ta>
            <ta e="T403" id="Seg_8626" s="T402">adv</ta>
            <ta e="T404" id="Seg_8627" s="T403">v-v:n.fin</ta>
            <ta e="T406" id="Seg_8628" s="T405">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_8629" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_8630" s="T407">v-v:ins-v:mood.pn</ta>
            <ta e="T409" id="Seg_8631" s="T408">v-v:mood.pn</ta>
            <ta e="T410" id="Seg_8632" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_8633" s="T410">pers</ta>
            <ta e="T412" id="Seg_8634" s="T411">adv</ta>
            <ta e="T413" id="Seg_8635" s="T412">quant</ta>
            <ta e="T414" id="Seg_8636" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_8637" s="T414">adv</ta>
            <ta e="T416" id="Seg_8638" s="T415">v-v:n.fin</ta>
            <ta e="T417" id="Seg_8639" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_8640" s="T417">n-n:case.poss</ta>
            <ta e="T419" id="Seg_8641" s="T418">conj</ta>
            <ta e="T420" id="Seg_8642" s="T419">v-v:n.fin</ta>
            <ta e="T421" id="Seg_8643" s="T420">v-v:n.fin</ta>
            <ta e="T422" id="Seg_8644" s="T421">aux-v:mood.pn</ta>
            <ta e="T424" id="Seg_8645" s="T423">aux-v:mood.pn</ta>
            <ta e="T425" id="Seg_8646" s="T424">v-v&gt;v-v:ins-v:n.fin</ta>
            <ta e="T426" id="Seg_8647" s="T425">que-n:case=ptcl</ta>
            <ta e="T427" id="Seg_8648" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_8649" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_8650" s="T428">conj</ta>
            <ta e="T430" id="Seg_8651" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_8652" s="T430">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_8653" s="T431">v-v:n.fin</ta>
            <ta e="T433" id="Seg_8654" s="T432">v-v:mood</ta>
            <ta e="T434" id="Seg_8655" s="T433">conj</ta>
            <ta e="T435" id="Seg_8656" s="T434">adv</ta>
            <ta e="T436" id="Seg_8657" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_8658" s="T436">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_8659" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_8660" s="T438">n-n:case</ta>
            <ta e="T440" id="Seg_8661" s="T439">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_8662" s="T440">conj</ta>
            <ta e="T442" id="Seg_8663" s="T441">pers</ta>
            <ta e="T443" id="Seg_8664" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8665" s="T443">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T449" id="Seg_8666" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_8667" s="T449">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T451" id="Seg_8668" s="T450">conj</ta>
            <ta e="T452" id="Seg_8669" s="T451">v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_8670" s="T452">dempro-n:case</ta>
            <ta e="T454" id="Seg_8671" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_8672" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_8673" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_8674" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_8675" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_8676" s="T458">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T460" id="Seg_8677" s="T459">conj</ta>
            <ta e="T461" id="Seg_8678" s="T460">dempro-n:case</ta>
            <ta e="T462" id="Seg_8679" s="T461">v-v&gt;v-v:tense</ta>
            <ta e="T463" id="Seg_8680" s="T462">adv</ta>
            <ta e="T464" id="Seg_8681" s="T463">adj-n:case</ta>
            <ta e="T465" id="Seg_8682" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_8683" s="T465">propr</ta>
            <ta e="T467" id="Seg_8684" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_8685" s="T467">adj-n:case</ta>
            <ta e="T469" id="Seg_8686" s="T468">n-n:case</ta>
            <ta e="T470" id="Seg_8687" s="T469">n-n:case.poss</ta>
            <ta e="T471" id="Seg_8688" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_8689" s="T471">v-v:tense-v:pn</ta>
            <ta e="T473" id="Seg_8690" s="T472">n-n:case</ta>
            <ta e="T474" id="Seg_8691" s="T473">n-n:case.poss</ta>
            <ta e="T475" id="Seg_8692" s="T474">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_8693" s="T475">dempro-n:case</ta>
            <ta e="T477" id="Seg_8694" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_8695" s="T477">adj-n:case</ta>
            <ta e="T479" id="Seg_8696" s="T478">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T480" id="Seg_8697" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_8698" s="T480">adv</ta>
            <ta e="T483" id="Seg_8699" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_8700" s="T483">que</ta>
            <ta e="T485" id="Seg_8701" s="T484">pers</ta>
            <ta e="T486" id="Seg_8702" s="T485">n-n:case.poss</ta>
            <ta e="T487" id="Seg_8703" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_8704" s="T487">v-v:tense-v:pn</ta>
            <ta e="T489" id="Seg_8705" s="T488">dempro-n:case</ta>
            <ta e="T490" id="Seg_8706" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_8707" s="T490">adj-n:case</ta>
            <ta e="T492" id="Seg_8708" s="T491">v-v:tense-v:pn</ta>
            <ta e="T493" id="Seg_8709" s="T492">conj</ta>
            <ta e="T494" id="Seg_8710" s="T493">num-n:case</ta>
            <ta e="T495" id="Seg_8711" s="T494">n-n:num</ta>
            <ta e="T496" id="Seg_8712" s="T495">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T500" id="Seg_8713" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_8714" s="T500">num-n:case</ta>
            <ta e="T502" id="Seg_8715" s="T501">n-n:case</ta>
            <ta e="T503" id="Seg_8716" s="T502">v-v:n.fin</ta>
            <ta e="T504" id="Seg_8717" s="T503">v-v:tense-v:pn</ta>
            <ta e="T505" id="Seg_8718" s="T504">adv</ta>
            <ta e="T506" id="Seg_8719" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_8720" s="T506">v-v:tense-v:pn</ta>
            <ta e="T508" id="Seg_8721" s="T507">adv</ta>
            <ta e="T511" id="Seg_8722" s="T510">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_8723" s="T511">v-v:n.fin</ta>
            <ta e="T513" id="Seg_8724" s="T512">que-n:case</ta>
            <ta e="T514" id="Seg_8725" s="T513">v-v&gt;v-v:pn</ta>
            <ta e="T515" id="Seg_8726" s="T514">pers</ta>
            <ta e="T516" id="Seg_8727" s="T515">adv</ta>
            <ta e="T517" id="Seg_8728" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_8729" s="T517">n-n:case</ta>
            <ta e="T521" id="Seg_8730" s="T520">v-v:tense-v:pn</ta>
            <ta e="T522" id="Seg_8731" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_8732" s="T522">v-v:mood.pn</ta>
            <ta e="T524" id="Seg_8733" s="T523">pers</ta>
            <ta e="T525" id="Seg_8734" s="T524">v-v:mood.pn</ta>
            <ta e="T526" id="Seg_8735" s="T525">que</ta>
            <ta e="T527" id="Seg_8736" s="T526">pers</ta>
            <ta e="T528" id="Seg_8737" s="T527">v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_8738" s="T528">n-n:case.poss</ta>
            <ta e="T531" id="Seg_8739" s="T530">v-v:mood.pn</ta>
            <ta e="T532" id="Seg_8740" s="T531">ptcl</ta>
            <ta e="T534" id="Seg_8741" s="T533">v-v:tense-v:pn</ta>
            <ta e="T535" id="Seg_8742" s="T534">conj</ta>
            <ta e="T536" id="Seg_8743" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_8744" s="T536">adj-n:case</ta>
            <ta e="T538" id="Seg_8745" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_8746" s="T538">v-v:mood.pn</ta>
            <ta e="T540" id="Seg_8747" s="T539">n-n:case</ta>
            <ta e="T541" id="Seg_8748" s="T540">pers</ta>
            <ta e="T542" id="Seg_8749" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_8750" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_8751" s="T543">n-n:case.poss</ta>
            <ta e="T545" id="Seg_8752" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_8753" s="T545">v-v:mood.pn</ta>
            <ta e="T547" id="Seg_8754" s="T546">pers</ta>
            <ta e="T548" id="Seg_8755" s="T547">adj-n:case</ta>
            <ta e="T549" id="Seg_8756" s="T548">v-v:mood.pn</ta>
            <ta e="T550" id="Seg_8757" s="T549">pers</ta>
            <ta e="T551" id="Seg_8758" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_8759" s="T551">n-n:case.poss</ta>
            <ta e="T553" id="Seg_8760" s="T552">quant</ta>
            <ta e="T554" id="Seg_8761" s="T553">quant</ta>
            <ta e="T556" id="Seg_8762" s="T555">n-n:ins-n:case</ta>
            <ta e="T558" id="Seg_8763" s="T557">v-v&gt;v-v:pn</ta>
            <ta e="T560" id="Seg_8764" s="T559">n-n:case</ta>
            <ta e="T561" id="Seg_8765" s="T560">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_8766" s="T562">pers</ta>
            <ta e="T564" id="Seg_8767" s="T563">n-n:case</ta>
            <ta e="T565" id="Seg_8768" s="T564">n-n:case.poss</ta>
            <ta e="T566" id="Seg_8769" s="T565">quant</ta>
            <ta e="T567" id="Seg_8770" s="T566">pers</ta>
            <ta e="T568" id="Seg_8771" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_8772" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_8773" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_8774" s="T570">v-v:tense-v:pn</ta>
            <ta e="T572" id="Seg_8775" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_8776" s="T572">dempro-n:num-n:case</ta>
            <ta e="T574" id="Seg_8777" s="T573">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_8778" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_8779" s="T575">n-n:case</ta>
            <ta e="T577" id="Seg_8780" s="T576">v-v:tense-v:pn</ta>
            <ta e="T578" id="Seg_8781" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_8782" s="T578">v-v:tense-v:pn</ta>
            <ta e="T580" id="Seg_8783" s="T579">conj</ta>
            <ta e="T581" id="Seg_8784" s="T580">pers</ta>
            <ta e="T582" id="Seg_8785" s="T581">v-v:ins-v:mood.pn</ta>
            <ta e="T588" id="Seg_8786" s="T587">pers</ta>
            <ta e="T589" id="Seg_8787" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_8788" s="T589">adj-n:case</ta>
            <ta e="T591" id="Seg_8789" s="T590">v-v:tense-v:pn</ta>
            <ta e="T592" id="Seg_8790" s="T591">n-n:case</ta>
            <ta e="T593" id="Seg_8791" s="T592">v-v:tense-v:pn</ta>
            <ta e="T594" id="Seg_8792" s="T593">conj</ta>
            <ta e="T595" id="Seg_8793" s="T594">n-n:case</ta>
            <ta e="T596" id="Seg_8794" s="T595">v-v:tense-v:pn</ta>
            <ta e="T600" id="Seg_8795" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_8796" s="T600">adj-n:case</ta>
            <ta e="T602" id="Seg_8797" s="T601">que</ta>
            <ta e="T603" id="Seg_8798" s="T602">v-v:tense-v:pn</ta>
            <ta e="T604" id="Seg_8799" s="T603">n-n:case</ta>
            <ta e="T607" id="Seg_8800" s="T606">adv</ta>
            <ta e="T608" id="Seg_8801" s="T607">adj-n:case</ta>
            <ta e="T609" id="Seg_8802" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_8803" s="T609">n-n:case</ta>
            <ta e="T611" id="Seg_8804" s="T610">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_8805" s="T611">pers</ta>
            <ta e="T613" id="Seg_8806" s="T612">v-v:tense-v:pn</ta>
            <ta e="T614" id="Seg_8807" s="T613">conj</ta>
            <ta e="T615" id="Seg_8808" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_8809" s="T615">v-v:tense-v:pn</ta>
            <ta e="T617" id="Seg_8810" s="T616">adj-n:case</ta>
            <ta e="T618" id="Seg_8811" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_8812" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_8813" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_8814" s="T620">v-v:tense-v:pn</ta>
            <ta e="T622" id="Seg_8815" s="T621">pers</ta>
            <ta e="T623" id="Seg_8816" s="T622">dempro-n:case</ta>
            <ta e="T624" id="Seg_8817" s="T623">v-v:tense-v:pn</ta>
            <ta e="T625" id="Seg_8818" s="T624">conj</ta>
            <ta e="T626" id="Seg_8819" s="T625">dempro-n:case</ta>
            <ta e="T627" id="Seg_8820" s="T626">pers</ta>
            <ta e="T628" id="Seg_8821" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_8822" s="T628">v-v&gt;v-v:pn</ta>
            <ta e="T630" id="Seg_8823" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_8824" s="T630">v-v:tense-v:pn</ta>
            <ta e="T632" id="Seg_8825" s="T631">pers</ta>
            <ta e="T633" id="Seg_8826" s="T632">v-v:n.fin</ta>
            <ta e="T634" id="Seg_8827" s="T633">pers</ta>
            <ta e="T635" id="Seg_8828" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_8829" s="T635">v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_8830" s="T636">conj</ta>
            <ta e="T638" id="Seg_8831" s="T637">dempro-n:case</ta>
            <ta e="T639" id="Seg_8832" s="T638">adv</ta>
            <ta e="T640" id="Seg_8833" s="T639">v-v&gt;v-v:pn</ta>
            <ta e="T641" id="Seg_8834" s="T640">pers</ta>
            <ta e="T642" id="Seg_8835" s="T641">adj-n:case</ta>
            <ta e="T643" id="Seg_8836" s="T642">ptcl</ta>
            <ta e="T644" id="Seg_8837" s="T643">n-n:case</ta>
            <ta e="T645" id="Seg_8838" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_8839" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_8840" s="T646">adv</ta>
            <ta e="T648" id="Seg_8841" s="T647">adj-n:case</ta>
            <ta e="T649" id="Seg_8842" s="T648">n-n:case</ta>
            <ta e="T651" id="Seg_8843" s="T650">quant</ta>
            <ta e="T653" id="Seg_8844" s="T652">v-v:tense-v:pn</ta>
            <ta e="T654" id="Seg_8845" s="T653">n-n:case</ta>
            <ta e="T657" id="Seg_8846" s="T656">adv</ta>
            <ta e="T658" id="Seg_8847" s="T657">adj-n:case</ta>
            <ta e="T659" id="Seg_8848" s="T658">n-n:case</ta>
            <ta e="T661" id="Seg_8849" s="T660">quant</ta>
            <ta e="T662" id="Seg_8850" s="T661">n-n:case</ta>
            <ta e="T663" id="Seg_8851" s="T662">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T664" id="Seg_8852" s="T663">conj</ta>
            <ta e="T665" id="Seg_8853" s="T664">pers</ta>
            <ta e="T666" id="Seg_8854" s="T665">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_8855" s="T666">pers</ta>
            <ta e="T668" id="Seg_8856" s="T667">pers</ta>
            <ta e="T669" id="Seg_8857" s="T668">v-v:tense-v:pn</ta>
            <ta e="T670" id="Seg_8858" s="T669">pers</ta>
            <ta e="T671" id="Seg_8859" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_8860" s="T671">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_8861" s="T672">adv</ta>
            <ta e="T674" id="Seg_8862" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_8863" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_8864" s="T675">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_8865" s="T676">v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_8866" s="T677">conj</ta>
            <ta e="T679" id="Seg_8867" s="T678">adv</ta>
            <ta e="T680" id="Seg_8868" s="T679">v-v:tense-v:pn</ta>
            <ta e="T681" id="Seg_8869" s="T680">%%</ta>
            <ta e="T682" id="Seg_8870" s="T681">%%</ta>
            <ta e="T683" id="Seg_8871" s="T682">adv</ta>
            <ta e="T684" id="Seg_8872" s="T683">ptcl</ta>
            <ta e="T687" id="Seg_8873" s="T686">n-n:case</ta>
            <ta e="T688" id="Seg_8874" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_8875" s="T688">v-v:tense-v:pn</ta>
            <ta e="T690" id="Seg_8876" s="T689">%%</ta>
            <ta e="T691" id="Seg_8877" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_8878" s="T691">v-v:mood</ta>
            <ta e="T693" id="Seg_8879" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_8880" s="T693">conj</ta>
            <ta e="T695" id="Seg_8881" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_8882" s="T695">n-n:case</ta>
            <ta e="T697" id="Seg_8883" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_8884" s="T697">v-v:tense-v:pn</ta>
            <ta e="T699" id="Seg_8885" s="T698">n-n:case</ta>
            <ta e="T700" id="Seg_8886" s="T699">conj</ta>
            <ta e="T701" id="Seg_8887" s="T700">n-n:case</ta>
            <ta e="T702" id="Seg_8888" s="T701">n</ta>
            <ta e="T703" id="Seg_8889" s="T702">n-n:num-n:case</ta>
            <ta e="T704" id="Seg_8890" s="T703">ptcl</ta>
            <ta e="T706" id="Seg_8891" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_8892" s="T706">v-v:tense-v:pn</ta>
            <ta e="T708" id="Seg_8893" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_8894" s="T708">v-v&gt;v-v:pn</ta>
            <ta e="T710" id="Seg_8895" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_8896" s="T710">num-num&gt;num</ta>
            <ta e="T712" id="Seg_8897" s="T711">v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_8898" s="T712">adj-n:case</ta>
            <ta e="T714" id="Seg_8899" s="T713">pers</ta>
            <ta e="T715" id="Seg_8900" s="T714">adj-n:case</ta>
            <ta e="T716" id="Seg_8901" s="T715">pers</ta>
            <ta e="T717" id="Seg_8902" s="T716">adj-n:case</ta>
            <ta e="T718" id="Seg_8903" s="T717">dempro-n:case</ta>
            <ta e="T719" id="Seg_8904" s="T718">num-n:case</ta>
            <ta e="T720" id="Seg_8905" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_8906" s="T720">num-n:case</ta>
            <ta e="T722" id="Seg_8907" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_8908" s="T722">num</ta>
            <ta e="T724" id="Seg_8909" s="T723">n-n:case</ta>
            <ta e="T725" id="Seg_8910" s="T724">post</ta>
            <ta e="T726" id="Seg_8911" s="T725">dempro-n:case</ta>
            <ta e="T727" id="Seg_8912" s="T726">dempro-n:num</ta>
            <ta e="T728" id="Seg_8913" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_8914" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_8915" s="T729">adj-n:case</ta>
            <ta e="T731" id="Seg_8916" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_8917" s="T731">pers</ta>
            <ta e="T733" id="Seg_8918" s="T732">v-v:tense-v:pn</ta>
            <ta e="T734" id="Seg_8919" s="T733">num-n:case</ta>
            <ta e="T735" id="Seg_8920" s="T734">n-n:case</ta>
            <ta e="T736" id="Seg_8921" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_8922" s="T736">v-v:tense-v:pn</ta>
            <ta e="T738" id="Seg_8923" s="T737">num-n:case</ta>
            <ta e="T739" id="Seg_8924" s="T738">n-n:case</ta>
            <ta e="T740" id="Seg_8925" s="T739">n-n:case.poss</ta>
            <ta e="T741" id="Seg_8926" s="T740">v-v:tense-v:pn</ta>
            <ta e="T742" id="Seg_8927" s="T741">num</ta>
            <ta e="T743" id="Seg_8928" s="T742">n-n:case</ta>
            <ta e="T744" id="Seg_8929" s="T743">n-n:case.poss</ta>
            <ta e="T745" id="Seg_8930" s="T744">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_8931" s="T745">conj</ta>
            <ta e="T747" id="Seg_8932" s="T746">num-n:case</ta>
            <ta e="T748" id="Seg_8933" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_8934" s="T748">n-n:case</ta>
            <ta e="T750" id="Seg_8935" s="T749">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_8936" s="T751">dempro-n:case</ta>
            <ta e="T753" id="Seg_8937" s="T752">n-n:case</ta>
            <ta e="T754" id="Seg_8938" s="T753">v-v:tense-v:pn</ta>
            <ta e="T755" id="Seg_8939" s="T754">pers</ta>
            <ta e="T756" id="Seg_8940" s="T755">v-v:n.fin</ta>
            <ta e="T757" id="Seg_8941" s="T756">ptcl</ta>
            <ta e="T758" id="Seg_8942" s="T757">adv</ta>
            <ta e="T759" id="Seg_8943" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_8944" s="T759">adj-n:case</ta>
            <ta e="T761" id="Seg_8945" s="T760">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_8946" s="T762">n-n:case</ta>
            <ta e="T764" id="Seg_8947" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_8948" s="T764">v-v:tense-v:pn</ta>
            <ta e="T766" id="Seg_8949" s="T765">adv</ta>
            <ta e="T767" id="Seg_8950" s="T766">adj-n:case</ta>
            <ta e="T768" id="Seg_8951" s="T767">v-v:tense-v:pn</ta>
            <ta e="T769" id="Seg_8952" s="T768">ptcl</ta>
            <ta e="T770" id="Seg_8953" s="T769">adv</ta>
            <ta e="T773" id="Seg_8954" s="T772">quant</ta>
            <ta e="T774" id="Seg_8955" s="T773">adj-n:case</ta>
            <ta e="T775" id="Seg_8956" s="T774">v-v:tense-v:pn</ta>
            <ta e="T776" id="Seg_8957" s="T775">adv</ta>
            <ta e="T777" id="Seg_8958" s="T776">n-n:case</ta>
            <ta e="T778" id="Seg_8959" s="T777">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T779" id="Seg_8960" s="T778">ptcl</ta>
            <ta e="T780" id="Seg_8961" s="T779">n</ta>
            <ta e="T781" id="Seg_8962" s="T780">v-v&gt;v-v:pn</ta>
            <ta e="T782" id="Seg_8963" s="T781">adv</ta>
            <ta e="T783" id="Seg_8964" s="T782">adv</ta>
            <ta e="T784" id="Seg_8965" s="T783">v-v&gt;v-v:pn</ta>
            <ta e="T786" id="Seg_8966" s="T785">n-n:case</ta>
            <ta e="T787" id="Seg_8967" s="T786">ptcl</ta>
            <ta e="T788" id="Seg_8968" s="T787">v-v:tense-v:pn</ta>
            <ta e="T790" id="Seg_8969" s="T788">conj</ta>
            <ta e="T791" id="Seg_8970" s="T790">n-n:case</ta>
            <ta e="T794" id="Seg_8971" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_8972" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_8973" s="T795">n-n:case</ta>
            <ta e="T797" id="Seg_8974" s="T796">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T798" id="Seg_8975" s="T797">adv</ta>
            <ta e="T799" id="Seg_8976" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_8977" s="T799">ptcl</ta>
            <ta e="T801" id="Seg_8978" s="T800">v-v:n.fin</ta>
            <ta e="T802" id="Seg_8979" s="T801">adv</ta>
            <ta e="T807" id="Seg_8980" s="T806">adv</ta>
            <ta e="T808" id="Seg_8981" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_8982" s="T808">adv</ta>
            <ta e="T810" id="Seg_8983" s="T809">v-v&gt;v-v:pn</ta>
            <ta e="T811" id="Seg_8984" s="T810">n-n:case</ta>
            <ta e="T814" id="Seg_8985" s="T813">adj-n:case</ta>
            <ta e="T816" id="Seg_8986" s="T815">n-n:case</ta>
            <ta e="T817" id="Seg_8987" s="T816">n-n:case.poss</ta>
            <ta e="T818" id="Seg_8988" s="T817">ptcl</ta>
            <ta e="T819" id="Seg_8989" s="T818">v-v:tense-v:pn</ta>
            <ta e="T820" id="Seg_8990" s="T819">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_8991" s="T820">adv</ta>
            <ta e="T822" id="Seg_8992" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_8993" s="T822">quant</ta>
            <ta e="T824" id="Seg_8994" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_8995" s="T824">dempro-n:num</ta>
            <ta e="T826" id="Seg_8996" s="T825">ptcl</ta>
            <ta e="T828" id="Seg_8997" s="T826">v-v:n.fin</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_8998" s="T4">n</ta>
            <ta e="T6" id="Seg_8999" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_9000" s="T6">adj</ta>
            <ta e="T8" id="Seg_9001" s="T7">num</ta>
            <ta e="T9" id="Seg_9002" s="T8">adv</ta>
            <ta e="T10" id="Seg_9003" s="T9">v</ta>
            <ta e="T11" id="Seg_9004" s="T10">adj</ta>
            <ta e="T12" id="Seg_9005" s="T11">adv</ta>
            <ta e="T13" id="Seg_9006" s="T12">n</ta>
            <ta e="T15" id="Seg_9007" s="T14">pers</ta>
            <ta e="T16" id="Seg_9008" s="T15">n</ta>
            <ta e="T17" id="Seg_9009" s="T16">quant</ta>
            <ta e="T18" id="Seg_9010" s="T17">v</ta>
            <ta e="T19" id="Seg_9011" s="T18">v</ta>
            <ta e="T20" id="Seg_9012" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_9013" s="T20">pers</ta>
            <ta e="T22" id="Seg_9014" s="T21">num</ta>
            <ta e="T24" id="Seg_9015" s="T23">v</ta>
            <ta e="T25" id="Seg_9016" s="T24">n</ta>
            <ta e="T26" id="Seg_9017" s="T25">dempro</ta>
            <ta e="T28" id="Seg_9018" s="T27">v</ta>
            <ta e="T29" id="Seg_9019" s="T28">adv</ta>
            <ta e="T30" id="Seg_9020" s="T29">pers</ta>
            <ta e="T31" id="Seg_9021" s="T30">v</ta>
            <ta e="T32" id="Seg_9022" s="T31">adv</ta>
            <ta e="T33" id="Seg_9023" s="T32">que</ta>
            <ta e="T34" id="Seg_9024" s="T33">dempro</ta>
            <ta e="T35" id="Seg_9025" s="T34">v</ta>
            <ta e="T36" id="Seg_9026" s="T35">pers</ta>
            <ta e="T37" id="Seg_9027" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_9028" s="T37">v</ta>
            <ta e="T39" id="Seg_9029" s="T38">n</ta>
            <ta e="T40" id="Seg_9030" s="T39">quant</ta>
            <ta e="T41" id="Seg_9031" s="T40">adj</ta>
            <ta e="T42" id="Seg_9032" s="T41">n</ta>
            <ta e="T43" id="Seg_9033" s="T42">v</ta>
            <ta e="T44" id="Seg_9034" s="T43">conj</ta>
            <ta e="T45" id="Seg_9035" s="T44">n</ta>
            <ta e="T47" id="Seg_9036" s="T46">v</ta>
            <ta e="T48" id="Seg_9037" s="T47">adv</ta>
            <ta e="T49" id="Seg_9038" s="T48">adv</ta>
            <ta e="T50" id="Seg_9039" s="T49">v</ta>
            <ta e="T51" id="Seg_9040" s="T50">n</ta>
            <ta e="T53" id="Seg_9041" s="T52">n</ta>
            <ta e="T54" id="Seg_9042" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_9043" s="T54">v</ta>
            <ta e="T56" id="Seg_9044" s="T55">n</ta>
            <ta e="T57" id="Seg_9045" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9046" s="T57">v</ta>
            <ta e="T59" id="Seg_9047" s="T58">adv</ta>
            <ta e="T60" id="Seg_9048" s="T59">adj</ta>
            <ta e="T61" id="Seg_9049" s="T60">n</ta>
            <ta e="T62" id="Seg_9050" s="T61">que</ta>
            <ta e="T63" id="Seg_9051" s="T62">dempro</ta>
            <ta e="T64" id="Seg_9052" s="T63">v</ta>
            <ta e="T65" id="Seg_9053" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_9054" s="T65">v</ta>
            <ta e="T67" id="Seg_9055" s="T66">v</ta>
            <ta e="T68" id="Seg_9056" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9057" s="T68">quant</ta>
            <ta e="T72" id="Seg_9058" s="T71">v</ta>
            <ta e="T73" id="Seg_9059" s="T72">n</ta>
            <ta e="T74" id="Seg_9060" s="T73">v</ta>
            <ta e="T75" id="Seg_9061" s="T74">v</ta>
            <ta e="T76" id="Seg_9062" s="T75">n</ta>
            <ta e="T77" id="Seg_9063" s="T76">v</ta>
            <ta e="T80" id="Seg_9064" s="T78">n</ta>
            <ta e="T82" id="Seg_9065" s="T81">v</ta>
            <ta e="T83" id="Seg_9066" s="T82">n</ta>
            <ta e="T84" id="Seg_9067" s="T83">v</ta>
            <ta e="T85" id="Seg_9068" s="T84">n</ta>
            <ta e="T86" id="Seg_9069" s="T85">v</ta>
            <ta e="T87" id="Seg_9070" s="T86">que</ta>
            <ta e="T89" id="Seg_9071" s="T88">dempro</ta>
            <ta e="T90" id="Seg_9072" s="T89">v</ta>
            <ta e="T91" id="Seg_9073" s="T90">adv</ta>
            <ta e="T92" id="Seg_9074" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_9075" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_9076" s="T93">adv</ta>
            <ta e="T95" id="Seg_9077" s="T94">v</ta>
            <ta e="T96" id="Seg_9078" s="T95">n</ta>
            <ta e="T97" id="Seg_9079" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_9080" s="T97">adv</ta>
            <ta e="T99" id="Seg_9081" s="T98">n</ta>
            <ta e="T100" id="Seg_9082" s="T99">v</ta>
            <ta e="T101" id="Seg_9083" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_9084" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_9085" s="T102">v</ta>
            <ta e="T104" id="Seg_9086" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_9087" s="T104">v</ta>
            <ta e="T106" id="Seg_9088" s="T105">v</ta>
            <ta e="T107" id="Seg_9089" s="T106">pers</ta>
            <ta e="T108" id="Seg_9090" s="T107">n</ta>
            <ta e="T109" id="Seg_9091" s="T108">conj</ta>
            <ta e="T110" id="Seg_9092" s="T109">adv</ta>
            <ta e="T111" id="Seg_9093" s="T110">aux</ta>
            <ta e="T112" id="Seg_9094" s="T111">v</ta>
            <ta e="T113" id="Seg_9095" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_9096" s="T113">v</ta>
            <ta e="T115" id="Seg_9097" s="T114">n</ta>
            <ta e="T116" id="Seg_9098" s="T115">v</ta>
            <ta e="T117" id="Seg_9099" s="T116">conj</ta>
            <ta e="T118" id="Seg_9100" s="T117">v</ta>
            <ta e="T119" id="Seg_9101" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_9102" s="T119">n</ta>
            <ta e="T121" id="Seg_9103" s="T120">adv</ta>
            <ta e="T122" id="Seg_9104" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_9105" s="T122">v</ta>
            <ta e="T125" id="Seg_9106" s="T124">n</ta>
            <ta e="T126" id="Seg_9107" s="T125">v</ta>
            <ta e="T127" id="Seg_9108" s="T126">que</ta>
            <ta e="T128" id="Seg_9109" s="T127">v</ta>
            <ta e="T129" id="Seg_9110" s="T128">v</ta>
            <ta e="T130" id="Seg_9111" s="T129">conj</ta>
            <ta e="T131" id="Seg_9112" s="T130">v</ta>
            <ta e="T132" id="Seg_9113" s="T131">quant</ta>
            <ta e="T133" id="Seg_9114" s="T132">pers</ta>
            <ta e="T134" id="Seg_9115" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_9116" s="T134">n</ta>
            <ta e="T136" id="Seg_9117" s="T135">v</ta>
            <ta e="T137" id="Seg_9118" s="T136">pers</ta>
            <ta e="T138" id="Seg_9119" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_9120" s="T138">n</ta>
            <ta e="T141" id="Seg_9121" s="T140">v</ta>
            <ta e="T142" id="Seg_9122" s="T141">v</ta>
            <ta e="T143" id="Seg_9123" s="T142">n</ta>
            <ta e="T144" id="Seg_9124" s="T143">adj</ta>
            <ta e="T145" id="Seg_9125" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_9126" s="T145">v</ta>
            <ta e="T147" id="Seg_9127" s="T146">adj</ta>
            <ta e="T149" id="Seg_9128" s="T148">v</ta>
            <ta e="T150" id="Seg_9129" s="T149">adj</ta>
            <ta e="T151" id="Seg_9130" s="T150">v</ta>
            <ta e="T152" id="Seg_9131" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_9132" s="T152">v</ta>
            <ta e="T154" id="Seg_9133" s="T153">adj</ta>
            <ta e="T156" id="Seg_9134" s="T154">n</ta>
            <ta e="T157" id="Seg_9135" s="T156">v</ta>
            <ta e="T158" id="Seg_9136" s="T157">n</ta>
            <ta e="T159" id="Seg_9137" s="T158">dempro</ta>
            <ta e="T161" id="Seg_9138" s="T160">v</ta>
            <ta e="T162" id="Seg_9139" s="T161">n</ta>
            <ta e="T163" id="Seg_9140" s="T162">v</ta>
            <ta e="T164" id="Seg_9141" s="T163">adv</ta>
            <ta e="T165" id="Seg_9142" s="T164">adj</ta>
            <ta e="T166" id="Seg_9143" s="T165">adj</ta>
            <ta e="T167" id="Seg_9144" s="T166">v</ta>
            <ta e="T168" id="Seg_9145" s="T167">adv</ta>
            <ta e="T169" id="Seg_9146" s="T168">n</ta>
            <ta e="T170" id="Seg_9147" s="T169">v</ta>
            <ta e="T171" id="Seg_9148" s="T170">adv</ta>
            <ta e="T172" id="Seg_9149" s="T171">n</ta>
            <ta e="T173" id="Seg_9150" s="T172">v</ta>
            <ta e="T174" id="Seg_9151" s="T173">n</ta>
            <ta e="T175" id="Seg_9152" s="T174">adv</ta>
            <ta e="T176" id="Seg_9153" s="T175">adv</ta>
            <ta e="T177" id="Seg_9154" s="T176">v</ta>
            <ta e="T178" id="Seg_9155" s="T177">adv</ta>
            <ta e="T179" id="Seg_9156" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9157" s="T179">v</ta>
            <ta e="T181" id="Seg_9158" s="T180">ptcl</ta>
            <ta e="T183" id="Seg_9159" s="T181">n</ta>
            <ta e="T184" id="Seg_9160" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_9161" s="T184">n</ta>
            <ta e="T186" id="Seg_9162" s="T185">aux</ta>
            <ta e="T187" id="Seg_9163" s="T186">v</ta>
            <ta e="T192" id="Seg_9164" s="T191">v</ta>
            <ta e="T193" id="Seg_9165" s="T192">n</ta>
            <ta e="T194" id="Seg_9166" s="T193">n</ta>
            <ta e="T196" id="Seg_9167" s="T195">n</ta>
            <ta e="T197" id="Seg_9168" s="T196">n</ta>
            <ta e="T198" id="Seg_9169" s="T197">n</ta>
            <ta e="T199" id="Seg_9170" s="T198">dempro</ta>
            <ta e="T200" id="Seg_9171" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_9172" s="T200">pers</ta>
            <ta e="T203" id="Seg_9173" s="T202">adj</ta>
            <ta e="T204" id="Seg_9174" s="T203">n</ta>
            <ta e="T206" id="Seg_9175" s="T205">v</ta>
            <ta e="T207" id="Seg_9176" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_9177" s="T207">adv</ta>
            <ta e="T209" id="Seg_9178" s="T208">quant</ta>
            <ta e="T210" id="Seg_9179" s="T209">adj</ta>
            <ta e="T211" id="Seg_9180" s="T210">n</ta>
            <ta e="T212" id="Seg_9181" s="T211">v</ta>
            <ta e="T213" id="Seg_9182" s="T212">n</ta>
            <ta e="T214" id="Seg_9183" s="T213">n</ta>
            <ta e="T215" id="Seg_9184" s="T214">v</ta>
            <ta e="T216" id="Seg_9185" s="T215">v</ta>
            <ta e="T217" id="Seg_9186" s="T216">pers</ta>
            <ta e="T218" id="Seg_9187" s="T217">dempro</ta>
            <ta e="T219" id="Seg_9188" s="T218">v</ta>
            <ta e="T220" id="Seg_9189" s="T219">pers</ta>
            <ta e="T221" id="Seg_9190" s="T220">n</ta>
            <ta e="T222" id="Seg_9191" s="T221">quant</ta>
            <ta e="T223" id="Seg_9192" s="T222">pers</ta>
            <ta e="T224" id="Seg_9193" s="T223">adv</ta>
            <ta e="T225" id="Seg_9194" s="T224">adj</ta>
            <ta e="T226" id="Seg_9195" s="T225">v</ta>
            <ta e="T227" id="Seg_9196" s="T226">dempro</ta>
            <ta e="T228" id="Seg_9197" s="T227">n</ta>
            <ta e="T229" id="Seg_9198" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_9199" s="T229">n</ta>
            <ta e="T231" id="Seg_9200" s="T230">v</ta>
            <ta e="T232" id="Seg_9201" s="T231">n</ta>
            <ta e="T233" id="Seg_9202" s="T232">v</ta>
            <ta e="T234" id="Seg_9203" s="T233">v</ta>
            <ta e="T235" id="Seg_9204" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_9205" s="T235">v</ta>
            <ta e="T237" id="Seg_9206" s="T236">n</ta>
            <ta e="T238" id="Seg_9207" s="T237">v</ta>
            <ta e="T239" id="Seg_9208" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_9209" s="T239">v</ta>
            <ta e="T241" id="Seg_9210" s="T240">aux</ta>
            <ta e="T242" id="Seg_9211" s="T241">v</ta>
            <ta e="T243" id="Seg_9212" s="T242">adj</ta>
            <ta e="T244" id="Seg_9213" s="T243">v</ta>
            <ta e="T245" id="Seg_9214" s="T244">que</ta>
            <ta e="T246" id="Seg_9215" s="T245">v</ta>
            <ta e="T247" id="Seg_9216" s="T246">refl</ta>
            <ta e="T248" id="Seg_9217" s="T247">n</ta>
            <ta e="T249" id="Seg_9218" s="T248">v</ta>
            <ta e="T250" id="Seg_9219" s="T249">adj</ta>
            <ta e="T251" id="Seg_9220" s="T250">v</ta>
            <ta e="T252" id="Seg_9221" s="T251">n</ta>
            <ta e="T253" id="Seg_9222" s="T252">v</ta>
            <ta e="T254" id="Seg_9223" s="T253">conj</ta>
            <ta e="T255" id="Seg_9224" s="T254">v</ta>
            <ta e="T256" id="Seg_9225" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_9226" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_9227" s="T257">dempro</ta>
            <ta e="T259" id="Seg_9228" s="T258">v</ta>
            <ta e="T260" id="Seg_9229" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_9230" s="T260">dempro</ta>
            <ta e="T262" id="Seg_9231" s="T261">v</ta>
            <ta e="T263" id="Seg_9232" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_9233" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_9234" s="T265">n</ta>
            <ta e="T267" id="Seg_9235" s="T266">v</ta>
            <ta e="T268" id="Seg_9236" s="T267">n</ta>
            <ta e="T273" id="Seg_9237" s="T272">v</ta>
            <ta e="T274" id="Seg_9238" s="T273">n</ta>
            <ta e="T275" id="Seg_9239" s="T274">v</ta>
            <ta e="T276" id="Seg_9240" s="T275">n</ta>
            <ta e="T277" id="Seg_9241" s="T276">v</ta>
            <ta e="T278" id="Seg_9242" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_9243" s="T278">v</ta>
            <ta e="T280" id="Seg_9244" s="T279">que</ta>
            <ta e="T281" id="Seg_9245" s="T280">dempro</ta>
            <ta e="T282" id="Seg_9246" s="T281">v</ta>
            <ta e="T284" id="Seg_9247" s="T283">propr</ta>
            <ta e="T285" id="Seg_9248" s="T284">v</ta>
            <ta e="T286" id="Seg_9249" s="T285">v</ta>
            <ta e="T287" id="Seg_9250" s="T286">n</ta>
            <ta e="T288" id="Seg_9251" s="T287">n</ta>
            <ta e="T289" id="Seg_9252" s="T288">v</ta>
            <ta e="T290" id="Seg_9253" s="T289">n</ta>
            <ta e="T291" id="Seg_9254" s="T290">v</ta>
            <ta e="T294" id="Seg_9255" s="T293">n</ta>
            <ta e="T296" id="Seg_9256" s="T295">n</ta>
            <ta e="T297" id="Seg_9257" s="T296">dempro</ta>
            <ta e="T298" id="Seg_9258" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_9259" s="T298">v</ta>
            <ta e="T300" id="Seg_9260" s="T299">n</ta>
            <ta e="T301" id="Seg_9261" s="T300">v</ta>
            <ta e="T302" id="Seg_9262" s="T301">adv</ta>
            <ta e="T303" id="Seg_9263" s="T302">quant</ta>
            <ta e="T304" id="Seg_9264" s="T303">v</ta>
            <ta e="T305" id="Seg_9265" s="T304">adv</ta>
            <ta e="T306" id="Seg_9266" s="T305">adv</ta>
            <ta e="T307" id="Seg_9267" s="T306">n</ta>
            <ta e="T308" id="Seg_9268" s="T307">quant</ta>
            <ta e="T309" id="Seg_9269" s="T308">propr</ta>
            <ta e="T310" id="Seg_9270" s="T309">v</ta>
            <ta e="T311" id="Seg_9271" s="T310">conj</ta>
            <ta e="T312" id="Seg_9272" s="T311">v</ta>
            <ta e="T313" id="Seg_9273" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_9274" s="T313">n</ta>
            <ta e="T315" id="Seg_9275" s="T314">n</ta>
            <ta e="T316" id="Seg_9276" s="T315">v</ta>
            <ta e="T317" id="Seg_9277" s="T316">adv</ta>
            <ta e="T318" id="Seg_9278" s="T317">propr</ta>
            <ta e="T319" id="Seg_9279" s="T318">v</ta>
            <ta e="T320" id="Seg_9280" s="T319">n</ta>
            <ta e="T321" id="Seg_9281" s="T320">v</ta>
            <ta e="T322" id="Seg_9282" s="T321">n</ta>
            <ta e="T323" id="Seg_9283" s="T322">v</ta>
            <ta e="T324" id="Seg_9284" s="T323">conj</ta>
            <ta e="T325" id="Seg_9285" s="T324">n</ta>
            <ta e="T326" id="Seg_9286" s="T325">v</ta>
            <ta e="T327" id="Seg_9287" s="T326">adv</ta>
            <ta e="T328" id="Seg_9288" s="T327">n</ta>
            <ta e="T329" id="Seg_9289" s="T328">v</ta>
            <ta e="T330" id="Seg_9290" s="T329">v</ta>
            <ta e="T331" id="Seg_9291" s="T330">conj</ta>
            <ta e="T333" id="Seg_9292" s="T332">v</ta>
            <ta e="T339" id="Seg_9293" s="T338">n</ta>
            <ta e="T340" id="Seg_9294" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_9295" s="T340">n</ta>
            <ta e="T342" id="Seg_9296" s="T341">n</ta>
            <ta e="T343" id="Seg_9297" s="T342">v</ta>
            <ta e="T344" id="Seg_9298" s="T343">n</ta>
            <ta e="T345" id="Seg_9299" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_9300" s="T345">v</ta>
            <ta e="T347" id="Seg_9301" s="T346">adv</ta>
            <ta e="T348" id="Seg_9302" s="T347">adv</ta>
            <ta e="T349" id="Seg_9303" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_9304" s="T349">adj</ta>
            <ta e="T351" id="Seg_9305" s="T350">pers</ta>
            <ta e="T352" id="Seg_9306" s="T351">dempro</ta>
            <ta e="T353" id="Seg_9307" s="T352">v</ta>
            <ta e="T354" id="Seg_9308" s="T353">v</ta>
            <ta e="T355" id="Seg_9309" s="T354">conj</ta>
            <ta e="T356" id="Seg_9310" s="T355">n</ta>
            <ta e="T357" id="Seg_9311" s="T356">v</ta>
            <ta e="T358" id="Seg_9312" s="T357">v</ta>
            <ta e="T359" id="Seg_9313" s="T358">conj</ta>
            <ta e="T360" id="Seg_9314" s="T359">adj</ta>
            <ta e="T361" id="Seg_9315" s="T360">v</ta>
            <ta e="T362" id="Seg_9316" s="T361">adv</ta>
            <ta e="T363" id="Seg_9317" s="T362">v</ta>
            <ta e="T364" id="Seg_9318" s="T363">v</ta>
            <ta e="T365" id="Seg_9319" s="T364">adv</ta>
            <ta e="T366" id="Seg_9320" s="T365">v</ta>
            <ta e="T367" id="Seg_9321" s="T366">n</ta>
            <ta e="T368" id="Seg_9322" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_9323" s="T368">v</ta>
            <ta e="T370" id="Seg_9324" s="T369">dempro</ta>
            <ta e="T371" id="Seg_9325" s="T370">n</ta>
            <ta e="T372" id="Seg_9326" s="T371">v</ta>
            <ta e="T373" id="Seg_9327" s="T372">conj</ta>
            <ta e="T374" id="Seg_9328" s="T373">n</ta>
            <ta e="T375" id="Seg_9329" s="T374">v</ta>
            <ta e="T376" id="Seg_9330" s="T375">v</ta>
            <ta e="T377" id="Seg_9331" s="T376">que</ta>
            <ta e="T378" id="Seg_9332" s="T377">n</ta>
            <ta e="T379" id="Seg_9333" s="T378">v</ta>
            <ta e="T380" id="Seg_9334" s="T379">pers</ta>
            <ta e="T381" id="Seg_9335" s="T380">dempro</ta>
            <ta e="T382" id="Seg_9336" s="T381">dempro</ta>
            <ta e="T383" id="Seg_9337" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_9338" s="T383">v</ta>
            <ta e="T385" id="Seg_9339" s="T384">que</ta>
            <ta e="T387" id="Seg_9340" s="T386">dempro</ta>
            <ta e="T388" id="Seg_9341" s="T387">adv</ta>
            <ta e="T389" id="Seg_9342" s="T388">v</ta>
            <ta e="T390" id="Seg_9343" s="T389">conj</ta>
            <ta e="T391" id="Seg_9344" s="T390">dempro</ta>
            <ta e="T392" id="Seg_9345" s="T391">n</ta>
            <ta e="T393" id="Seg_9346" s="T392">conj</ta>
            <ta e="T394" id="Seg_9347" s="T393">n</ta>
            <ta e="T395" id="Seg_9348" s="T394">v</ta>
            <ta e="T396" id="Seg_9349" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_9350" s="T396">n</ta>
            <ta e="T398" id="Seg_9351" s="T397">v</ta>
            <ta e="T399" id="Seg_9352" s="T398">n</ta>
            <ta e="T400" id="Seg_9353" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_9354" s="T400">adj</ta>
            <ta e="T402" id="Seg_9355" s="T401">v</ta>
            <ta e="T403" id="Seg_9356" s="T402">adv</ta>
            <ta e="T404" id="Seg_9357" s="T403">v</ta>
            <ta e="T406" id="Seg_9358" s="T405">v</ta>
            <ta e="T407" id="Seg_9359" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_9360" s="T407">v</ta>
            <ta e="T409" id="Seg_9361" s="T408">v</ta>
            <ta e="T410" id="Seg_9362" s="T409">n</ta>
            <ta e="T411" id="Seg_9363" s="T410">pers</ta>
            <ta e="T412" id="Seg_9364" s="T411">adv</ta>
            <ta e="T413" id="Seg_9365" s="T412">quant</ta>
            <ta e="T414" id="Seg_9366" s="T413">v</ta>
            <ta e="T415" id="Seg_9367" s="T414">adv</ta>
            <ta e="T416" id="Seg_9368" s="T415">v</ta>
            <ta e="T417" id="Seg_9369" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_9370" s="T417">n</ta>
            <ta e="T419" id="Seg_9371" s="T418">conj</ta>
            <ta e="T420" id="Seg_9372" s="T419">v</ta>
            <ta e="T421" id="Seg_9373" s="T420">v</ta>
            <ta e="T422" id="Seg_9374" s="T421">aux</ta>
            <ta e="T424" id="Seg_9375" s="T423">aux</ta>
            <ta e="T425" id="Seg_9376" s="T424">v</ta>
            <ta e="T426" id="Seg_9377" s="T425">que</ta>
            <ta e="T427" id="Seg_9378" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_9379" s="T427">v</ta>
            <ta e="T429" id="Seg_9380" s="T428">conj</ta>
            <ta e="T430" id="Seg_9381" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_9382" s="T430">v</ta>
            <ta e="T432" id="Seg_9383" s="T431">v</ta>
            <ta e="T433" id="Seg_9384" s="T432">v</ta>
            <ta e="T434" id="Seg_9385" s="T433">conj</ta>
            <ta e="T435" id="Seg_9386" s="T434">adv</ta>
            <ta e="T436" id="Seg_9387" s="T435">n</ta>
            <ta e="T437" id="Seg_9388" s="T436">v</ta>
            <ta e="T438" id="Seg_9389" s="T437">n</ta>
            <ta e="T439" id="Seg_9390" s="T438">n</ta>
            <ta e="T440" id="Seg_9391" s="T439">v</ta>
            <ta e="T441" id="Seg_9392" s="T440">conj</ta>
            <ta e="T442" id="Seg_9393" s="T441">pers</ta>
            <ta e="T443" id="Seg_9394" s="T442">n</ta>
            <ta e="T444" id="Seg_9395" s="T443">v</ta>
            <ta e="T449" id="Seg_9396" s="T448">n</ta>
            <ta e="T450" id="Seg_9397" s="T449">v</ta>
            <ta e="T451" id="Seg_9398" s="T450">conj</ta>
            <ta e="T452" id="Seg_9399" s="T451">v</ta>
            <ta e="T453" id="Seg_9400" s="T452">dempro</ta>
            <ta e="T454" id="Seg_9401" s="T453">n</ta>
            <ta e="T455" id="Seg_9402" s="T454">n</ta>
            <ta e="T456" id="Seg_9403" s="T455">n</ta>
            <ta e="T457" id="Seg_9404" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_9405" s="T457">n</ta>
            <ta e="T459" id="Seg_9406" s="T458">v</ta>
            <ta e="T460" id="Seg_9407" s="T459">conj</ta>
            <ta e="T461" id="Seg_9408" s="T460">dempro</ta>
            <ta e="T462" id="Seg_9409" s="T461">v</ta>
            <ta e="T463" id="Seg_9410" s="T462">adv</ta>
            <ta e="T464" id="Seg_9411" s="T463">adj</ta>
            <ta e="T465" id="Seg_9412" s="T464">n</ta>
            <ta e="T466" id="Seg_9413" s="T465">propr</ta>
            <ta e="T467" id="Seg_9414" s="T466">n</ta>
            <ta e="T468" id="Seg_9415" s="T467">adj</ta>
            <ta e="T469" id="Seg_9416" s="T468">n</ta>
            <ta e="T470" id="Seg_9417" s="T469">n</ta>
            <ta e="T471" id="Seg_9418" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_9419" s="T471">v</ta>
            <ta e="T473" id="Seg_9420" s="T472">n</ta>
            <ta e="T474" id="Seg_9421" s="T473">n</ta>
            <ta e="T475" id="Seg_9422" s="T474">v</ta>
            <ta e="T476" id="Seg_9423" s="T475">dempro</ta>
            <ta e="T477" id="Seg_9424" s="T476">n</ta>
            <ta e="T478" id="Seg_9425" s="T477">adj</ta>
            <ta e="T479" id="Seg_9426" s="T478">v</ta>
            <ta e="T480" id="Seg_9427" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_9428" s="T480">adv</ta>
            <ta e="T483" id="Seg_9429" s="T482">v</ta>
            <ta e="T484" id="Seg_9430" s="T483">que</ta>
            <ta e="T485" id="Seg_9431" s="T484">pers</ta>
            <ta e="T486" id="Seg_9432" s="T485">n</ta>
            <ta e="T487" id="Seg_9433" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_9434" s="T487">v</ta>
            <ta e="T489" id="Seg_9435" s="T488">dempro</ta>
            <ta e="T490" id="Seg_9436" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_9437" s="T490">adj</ta>
            <ta e="T492" id="Seg_9438" s="T491">v</ta>
            <ta e="T493" id="Seg_9439" s="T492">conj</ta>
            <ta e="T494" id="Seg_9440" s="T493">num</ta>
            <ta e="T495" id="Seg_9441" s="T494">n</ta>
            <ta e="T496" id="Seg_9442" s="T495">v</ta>
            <ta e="T500" id="Seg_9443" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_9444" s="T500">num</ta>
            <ta e="T502" id="Seg_9445" s="T501">n</ta>
            <ta e="T503" id="Seg_9446" s="T502">v</ta>
            <ta e="T504" id="Seg_9447" s="T503">v</ta>
            <ta e="T505" id="Seg_9448" s="T504">adv</ta>
            <ta e="T506" id="Seg_9449" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_9450" s="T506">v</ta>
            <ta e="T508" id="Seg_9451" s="T507">adv</ta>
            <ta e="T511" id="Seg_9452" s="T510">v</ta>
            <ta e="T512" id="Seg_9453" s="T511">v</ta>
            <ta e="T513" id="Seg_9454" s="T512">que</ta>
            <ta e="T514" id="Seg_9455" s="T513">v</ta>
            <ta e="T515" id="Seg_9456" s="T514">pers</ta>
            <ta e="T516" id="Seg_9457" s="T515">adv</ta>
            <ta e="T517" id="Seg_9458" s="T516">n</ta>
            <ta e="T518" id="Seg_9459" s="T517">n</ta>
            <ta e="T521" id="Seg_9460" s="T520">v</ta>
            <ta e="T522" id="Seg_9461" s="T521">n</ta>
            <ta e="T523" id="Seg_9462" s="T522">v</ta>
            <ta e="T524" id="Seg_9463" s="T523">pers</ta>
            <ta e="T525" id="Seg_9464" s="T524">v</ta>
            <ta e="T526" id="Seg_9465" s="T525">que</ta>
            <ta e="T527" id="Seg_9466" s="T526">pers</ta>
            <ta e="T528" id="Seg_9467" s="T527">v</ta>
            <ta e="T529" id="Seg_9468" s="T528">n</ta>
            <ta e="T531" id="Seg_9469" s="T530">v</ta>
            <ta e="T532" id="Seg_9470" s="T531">ptcl</ta>
            <ta e="T534" id="Seg_9471" s="T533">v</ta>
            <ta e="T535" id="Seg_9472" s="T534">conj</ta>
            <ta e="T536" id="Seg_9473" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_9474" s="T536">adj</ta>
            <ta e="T538" id="Seg_9475" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_9476" s="T538">v</ta>
            <ta e="T540" id="Seg_9477" s="T539">n</ta>
            <ta e="T541" id="Seg_9478" s="T540">pers</ta>
            <ta e="T542" id="Seg_9479" s="T541">n</ta>
            <ta e="T543" id="Seg_9480" s="T542">v</ta>
            <ta e="T544" id="Seg_9481" s="T543">n</ta>
            <ta e="T545" id="Seg_9482" s="T544">v</ta>
            <ta e="T546" id="Seg_9483" s="T545">v</ta>
            <ta e="T547" id="Seg_9484" s="T546">pers</ta>
            <ta e="T548" id="Seg_9485" s="T547">adj</ta>
            <ta e="T549" id="Seg_9486" s="T548">v</ta>
            <ta e="T550" id="Seg_9487" s="T549">pers</ta>
            <ta e="T551" id="Seg_9488" s="T550">n</ta>
            <ta e="T552" id="Seg_9489" s="T551">n</ta>
            <ta e="T553" id="Seg_9490" s="T552">quant</ta>
            <ta e="T554" id="Seg_9491" s="T553">quant</ta>
            <ta e="T556" id="Seg_9492" s="T555">n</ta>
            <ta e="T558" id="Seg_9493" s="T557">v</ta>
            <ta e="T560" id="Seg_9494" s="T559">n</ta>
            <ta e="T561" id="Seg_9495" s="T560">v</ta>
            <ta e="T563" id="Seg_9496" s="T562">pers</ta>
            <ta e="T564" id="Seg_9497" s="T563">n</ta>
            <ta e="T565" id="Seg_9498" s="T564">n</ta>
            <ta e="T566" id="Seg_9499" s="T565">quant</ta>
            <ta e="T567" id="Seg_9500" s="T566">pers</ta>
            <ta e="T568" id="Seg_9501" s="T567">n</ta>
            <ta e="T569" id="Seg_9502" s="T568">n</ta>
            <ta e="T570" id="Seg_9503" s="T569">n</ta>
            <ta e="T571" id="Seg_9504" s="T570">v</ta>
            <ta e="T572" id="Seg_9505" s="T571">n</ta>
            <ta e="T573" id="Seg_9506" s="T572">dempro</ta>
            <ta e="T574" id="Seg_9507" s="T573">v</ta>
            <ta e="T575" id="Seg_9508" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_9509" s="T575">n</ta>
            <ta e="T577" id="Seg_9510" s="T576">v</ta>
            <ta e="T578" id="Seg_9511" s="T577">n</ta>
            <ta e="T579" id="Seg_9512" s="T578">v</ta>
            <ta e="T580" id="Seg_9513" s="T579">conj</ta>
            <ta e="T581" id="Seg_9514" s="T580">pers</ta>
            <ta e="T582" id="Seg_9515" s="T581">v</ta>
            <ta e="T588" id="Seg_9516" s="T587">pers</ta>
            <ta e="T589" id="Seg_9517" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9518" s="T589">adj</ta>
            <ta e="T591" id="Seg_9519" s="T590">v</ta>
            <ta e="T592" id="Seg_9520" s="T591">n</ta>
            <ta e="T593" id="Seg_9521" s="T592">v</ta>
            <ta e="T594" id="Seg_9522" s="T593">conj</ta>
            <ta e="T595" id="Seg_9523" s="T594">n</ta>
            <ta e="T596" id="Seg_9524" s="T595">v</ta>
            <ta e="T600" id="Seg_9525" s="T599">n</ta>
            <ta e="T601" id="Seg_9526" s="T600">adj</ta>
            <ta e="T602" id="Seg_9527" s="T601">que</ta>
            <ta e="T603" id="Seg_9528" s="T602">v</ta>
            <ta e="T604" id="Seg_9529" s="T603">n</ta>
            <ta e="T607" id="Seg_9530" s="T606">adv</ta>
            <ta e="T608" id="Seg_9531" s="T607">adj</ta>
            <ta e="T609" id="Seg_9532" s="T608">n</ta>
            <ta e="T610" id="Seg_9533" s="T609">n</ta>
            <ta e="T611" id="Seg_9534" s="T610">v</ta>
            <ta e="T612" id="Seg_9535" s="T611">pers</ta>
            <ta e="T613" id="Seg_9536" s="T612">v</ta>
            <ta e="T614" id="Seg_9537" s="T613">conj</ta>
            <ta e="T615" id="Seg_9538" s="T614">n</ta>
            <ta e="T616" id="Seg_9539" s="T615">v</ta>
            <ta e="T617" id="Seg_9540" s="T616">adj</ta>
            <ta e="T618" id="Seg_9541" s="T617">n</ta>
            <ta e="T619" id="Seg_9542" s="T618">v</ta>
            <ta e="T620" id="Seg_9543" s="T619">n</ta>
            <ta e="T621" id="Seg_9544" s="T620">v</ta>
            <ta e="T622" id="Seg_9545" s="T621">pers</ta>
            <ta e="T623" id="Seg_9546" s="T622">dempro</ta>
            <ta e="T624" id="Seg_9547" s="T623">v</ta>
            <ta e="T625" id="Seg_9548" s="T624">conj</ta>
            <ta e="T626" id="Seg_9549" s="T625">dempro</ta>
            <ta e="T627" id="Seg_9550" s="T626">pers</ta>
            <ta e="T628" id="Seg_9551" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_9552" s="T628">v</ta>
            <ta e="T630" id="Seg_9553" s="T629">ptcl</ta>
            <ta e="T631" id="Seg_9554" s="T630">v</ta>
            <ta e="T632" id="Seg_9555" s="T631">pers</ta>
            <ta e="T633" id="Seg_9556" s="T632">v</ta>
            <ta e="T634" id="Seg_9557" s="T633">pers</ta>
            <ta e="T635" id="Seg_9558" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_9559" s="T635">v</ta>
            <ta e="T637" id="Seg_9560" s="T636">conj</ta>
            <ta e="T638" id="Seg_9561" s="T637">dempro</ta>
            <ta e="T639" id="Seg_9562" s="T638">adv</ta>
            <ta e="T640" id="Seg_9563" s="T639">v</ta>
            <ta e="T641" id="Seg_9564" s="T640">pers</ta>
            <ta e="T642" id="Seg_9565" s="T641">adj</ta>
            <ta e="T643" id="Seg_9566" s="T642">ptcl</ta>
            <ta e="T644" id="Seg_9567" s="T643">n</ta>
            <ta e="T645" id="Seg_9568" s="T644">ptcl</ta>
            <ta e="T646" id="Seg_9569" s="T645">v</ta>
            <ta e="T647" id="Seg_9570" s="T646">adv</ta>
            <ta e="T648" id="Seg_9571" s="T647">adj</ta>
            <ta e="T649" id="Seg_9572" s="T648">n</ta>
            <ta e="T651" id="Seg_9573" s="T650">quant</ta>
            <ta e="T653" id="Seg_9574" s="T652">v</ta>
            <ta e="T654" id="Seg_9575" s="T653">n</ta>
            <ta e="T657" id="Seg_9576" s="T656">adv</ta>
            <ta e="T658" id="Seg_9577" s="T657">adj</ta>
            <ta e="T659" id="Seg_9578" s="T658">n</ta>
            <ta e="T661" id="Seg_9579" s="T660">quant</ta>
            <ta e="T662" id="Seg_9580" s="T661">n</ta>
            <ta e="T663" id="Seg_9581" s="T662">v</ta>
            <ta e="T664" id="Seg_9582" s="T663">conj</ta>
            <ta e="T665" id="Seg_9583" s="T664">pers</ta>
            <ta e="T666" id="Seg_9584" s="T665">v</ta>
            <ta e="T667" id="Seg_9585" s="T666">pers</ta>
            <ta e="T668" id="Seg_9586" s="T667">pers</ta>
            <ta e="T669" id="Seg_9587" s="T668">v</ta>
            <ta e="T670" id="Seg_9588" s="T669">pers</ta>
            <ta e="T671" id="Seg_9589" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_9590" s="T671">v</ta>
            <ta e="T673" id="Seg_9591" s="T672">adv</ta>
            <ta e="T674" id="Seg_9592" s="T673">v</ta>
            <ta e="T675" id="Seg_9593" s="T674">n</ta>
            <ta e="T676" id="Seg_9594" s="T675">v</ta>
            <ta e="T677" id="Seg_9595" s="T676">v</ta>
            <ta e="T678" id="Seg_9596" s="T677">conj</ta>
            <ta e="T679" id="Seg_9597" s="T678">adv</ta>
            <ta e="T680" id="Seg_9598" s="T679">v</ta>
            <ta e="T683" id="Seg_9599" s="T682">adv</ta>
            <ta e="T684" id="Seg_9600" s="T683">ptcl</ta>
            <ta e="T687" id="Seg_9601" s="T686">n</ta>
            <ta e="T688" id="Seg_9602" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_9603" s="T688">v</ta>
            <ta e="T691" id="Seg_9604" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_9605" s="T691">v</ta>
            <ta e="T693" id="Seg_9606" s="T692">n</ta>
            <ta e="T694" id="Seg_9607" s="T693">conj</ta>
            <ta e="T695" id="Seg_9608" s="T694">n</ta>
            <ta e="T696" id="Seg_9609" s="T695">n</ta>
            <ta e="T697" id="Seg_9610" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_9611" s="T697">v</ta>
            <ta e="T699" id="Seg_9612" s="T698">n</ta>
            <ta e="T700" id="Seg_9613" s="T699">conj</ta>
            <ta e="T701" id="Seg_9614" s="T700">n</ta>
            <ta e="T702" id="Seg_9615" s="T701">n</ta>
            <ta e="T703" id="Seg_9616" s="T702">n</ta>
            <ta e="T704" id="Seg_9617" s="T703">ptcl</ta>
            <ta e="T706" id="Seg_9618" s="T705">n</ta>
            <ta e="T707" id="Seg_9619" s="T706">v</ta>
            <ta e="T708" id="Seg_9620" s="T707">n</ta>
            <ta e="T709" id="Seg_9621" s="T708">v</ta>
            <ta e="T710" id="Seg_9622" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_9623" s="T710">num</ta>
            <ta e="T712" id="Seg_9624" s="T711">v</ta>
            <ta e="T713" id="Seg_9625" s="T712">adj</ta>
            <ta e="T714" id="Seg_9626" s="T713">pers</ta>
            <ta e="T715" id="Seg_9627" s="T714">adj</ta>
            <ta e="T716" id="Seg_9628" s="T715">pers</ta>
            <ta e="T717" id="Seg_9629" s="T716">adj</ta>
            <ta e="T718" id="Seg_9630" s="T717">dempro</ta>
            <ta e="T719" id="Seg_9631" s="T718">num</ta>
            <ta e="T720" id="Seg_9632" s="T719">n</ta>
            <ta e="T721" id="Seg_9633" s="T720">num</ta>
            <ta e="T722" id="Seg_9634" s="T721">n</ta>
            <ta e="T723" id="Seg_9635" s="T722">num</ta>
            <ta e="T724" id="Seg_9636" s="T723">n</ta>
            <ta e="T725" id="Seg_9637" s="T724">post</ta>
            <ta e="T726" id="Seg_9638" s="T725">dempro</ta>
            <ta e="T727" id="Seg_9639" s="T726">dempro</ta>
            <ta e="T728" id="Seg_9640" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9641" s="T728">v</ta>
            <ta e="T730" id="Seg_9642" s="T729">adj</ta>
            <ta e="T731" id="Seg_9643" s="T730">n</ta>
            <ta e="T732" id="Seg_9644" s="T731">pers</ta>
            <ta e="T733" id="Seg_9645" s="T732">v</ta>
            <ta e="T734" id="Seg_9646" s="T733">num</ta>
            <ta e="T735" id="Seg_9647" s="T734">n</ta>
            <ta e="T736" id="Seg_9648" s="T735">n</ta>
            <ta e="T737" id="Seg_9649" s="T736">v</ta>
            <ta e="T738" id="Seg_9650" s="T737">num</ta>
            <ta e="T739" id="Seg_9651" s="T738">n</ta>
            <ta e="T740" id="Seg_9652" s="T739">n</ta>
            <ta e="T741" id="Seg_9653" s="T740">v</ta>
            <ta e="T742" id="Seg_9654" s="T741">num</ta>
            <ta e="T743" id="Seg_9655" s="T742">n</ta>
            <ta e="T744" id="Seg_9656" s="T743">n</ta>
            <ta e="T745" id="Seg_9657" s="T744">v</ta>
            <ta e="T746" id="Seg_9658" s="T745">conj</ta>
            <ta e="T747" id="Seg_9659" s="T746">num</ta>
            <ta e="T748" id="Seg_9660" s="T747">n</ta>
            <ta e="T749" id="Seg_9661" s="T748">n</ta>
            <ta e="T750" id="Seg_9662" s="T749">v</ta>
            <ta e="T752" id="Seg_9663" s="T751">dempro</ta>
            <ta e="T753" id="Seg_9664" s="T752">n</ta>
            <ta e="T754" id="Seg_9665" s="T753">v</ta>
            <ta e="T755" id="Seg_9666" s="T754">pers</ta>
            <ta e="T756" id="Seg_9667" s="T755">v</ta>
            <ta e="T757" id="Seg_9668" s="T756">ptcl</ta>
            <ta e="T758" id="Seg_9669" s="T757">adv</ta>
            <ta e="T759" id="Seg_9670" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_9671" s="T759">adj</ta>
            <ta e="T761" id="Seg_9672" s="T760">v</ta>
            <ta e="T763" id="Seg_9673" s="T762">n</ta>
            <ta e="T764" id="Seg_9674" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_9675" s="T764">v</ta>
            <ta e="T766" id="Seg_9676" s="T765">adv</ta>
            <ta e="T767" id="Seg_9677" s="T766">adj</ta>
            <ta e="T768" id="Seg_9678" s="T767">v</ta>
            <ta e="T769" id="Seg_9679" s="T768">ptcl</ta>
            <ta e="T770" id="Seg_9680" s="T769">adv</ta>
            <ta e="T773" id="Seg_9681" s="T772">quant</ta>
            <ta e="T774" id="Seg_9682" s="T773">adj</ta>
            <ta e="T775" id="Seg_9683" s="T774">v</ta>
            <ta e="T776" id="Seg_9684" s="T775">adv</ta>
            <ta e="T777" id="Seg_9685" s="T776">n</ta>
            <ta e="T778" id="Seg_9686" s="T777">v</ta>
            <ta e="T779" id="Seg_9687" s="T778">ptcl</ta>
            <ta e="T780" id="Seg_9688" s="T779">n</ta>
            <ta e="T781" id="Seg_9689" s="T780">v</ta>
            <ta e="T782" id="Seg_9690" s="T781">adv</ta>
            <ta e="T783" id="Seg_9691" s="T782">adv</ta>
            <ta e="T784" id="Seg_9692" s="T783">v</ta>
            <ta e="T786" id="Seg_9693" s="T785">n</ta>
            <ta e="T787" id="Seg_9694" s="T786">ptcl</ta>
            <ta e="T788" id="Seg_9695" s="T787">v</ta>
            <ta e="T790" id="Seg_9696" s="T788">conj</ta>
            <ta e="T791" id="Seg_9697" s="T790">n</ta>
            <ta e="T794" id="Seg_9698" s="T793">n</ta>
            <ta e="T795" id="Seg_9699" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_9700" s="T795">n</ta>
            <ta e="T797" id="Seg_9701" s="T796">v</ta>
            <ta e="T798" id="Seg_9702" s="T797">adv</ta>
            <ta e="T799" id="Seg_9703" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_9704" s="T799">ptcl</ta>
            <ta e="T801" id="Seg_9705" s="T800">v</ta>
            <ta e="T802" id="Seg_9706" s="T801">adv</ta>
            <ta e="T807" id="Seg_9707" s="T806">adv</ta>
            <ta e="T808" id="Seg_9708" s="T807">n</ta>
            <ta e="T809" id="Seg_9709" s="T808">adv</ta>
            <ta e="T810" id="Seg_9710" s="T809">v</ta>
            <ta e="T811" id="Seg_9711" s="T810">n</ta>
            <ta e="T814" id="Seg_9712" s="T813">adj</ta>
            <ta e="T816" id="Seg_9713" s="T815">n</ta>
            <ta e="T817" id="Seg_9714" s="T816">n</ta>
            <ta e="T818" id="Seg_9715" s="T817">ptcl</ta>
            <ta e="T819" id="Seg_9716" s="T818">v</ta>
            <ta e="T820" id="Seg_9717" s="T819">v</ta>
            <ta e="T821" id="Seg_9718" s="T820">adv</ta>
            <ta e="T822" id="Seg_9719" s="T821">n</ta>
            <ta e="T823" id="Seg_9720" s="T822">quant</ta>
            <ta e="T824" id="Seg_9721" s="T823">ptcl</ta>
            <ta e="T825" id="Seg_9722" s="T824">dempro</ta>
            <ta e="T826" id="Seg_9723" s="T825">ptcl</ta>
            <ta e="T828" id="Seg_9724" s="T826">v</ta>
         </annotation>
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T191" id="Seg_9725" s="T187">RUS:ext</ta>
            <ta e="T337" id="Seg_9726" s="T333">RUS:ext</ta>
            <ta e="T448" id="Seg_9727" s="T446">RUS:ext</ta>
            <ta e="T587" id="Seg_9728" s="T582">RUS:ext</ta>
            <ta e="T599" id="Seg_9729" s="T596">RUS:ext</ta>
            <ta e="T656" id="Seg_9730" s="T654">RUS:ext</ta>
            <ta e="T686" id="Seg_9731" s="T684">RUS:ext</ta>
            <ta e="T805" id="Seg_9732" s="T802">RUS:ext</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T5" id="Seg_9733" s="T4">np:Th 0.3.h:Poss</ta>
            <ta e="T8" id="Seg_9734" s="T7">np:A</ta>
            <ta e="T9" id="Seg_9735" s="T8">adv:L</ta>
            <ta e="T11" id="Seg_9736" s="T10">np:A</ta>
            <ta e="T12" id="Seg_9737" s="T11">adv:L</ta>
            <ta e="T15" id="Seg_9738" s="T14">pro.h:Poss</ta>
            <ta e="T16" id="Seg_9739" s="T15">np:Th</ta>
            <ta e="T21" id="Seg_9740" s="T20">pro.h:A</ta>
            <ta e="T25" id="Seg_9741" s="T24">np:L</ta>
            <ta e="T26" id="Seg_9742" s="T25">pro.h:A</ta>
            <ta e="T29" id="Seg_9743" s="T28">adv:G</ta>
            <ta e="T30" id="Seg_9744" s="T29">pro.h:A</ta>
            <ta e="T32" id="Seg_9745" s="T31">adv:G</ta>
            <ta e="T34" id="Seg_9746" s="T33">pro.h:Th</ta>
            <ta e="T35" id="Seg_9747" s="T34">0.3.h:A</ta>
            <ta e="T36" id="Seg_9748" s="T35">pro.h:E</ta>
            <ta e="T39" id="Seg_9749" s="T38">np:A</ta>
            <ta e="T42" id="Seg_9750" s="T41">np:Th</ta>
            <ta e="T45" id="Seg_9751" s="T44">np:G</ta>
            <ta e="T47" id="Seg_9752" s="T46">0.3.h:A</ta>
            <ta e="T50" id="Seg_9753" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_9754" s="T50">np:L</ta>
            <ta e="T53" id="Seg_9755" s="T52">np:L</ta>
            <ta e="T55" id="Seg_9756" s="T54">0.3.h:E</ta>
            <ta e="T56" id="Seg_9757" s="T55">np:L</ta>
            <ta e="T58" id="Seg_9758" s="T57">0.3.h:E</ta>
            <ta e="T66" id="Seg_9759" s="T65">0.1.h:E</ta>
            <ta e="T67" id="Seg_9760" s="T66">0.2.h:A</ta>
            <ta e="T73" id="Seg_9761" s="T72">np:Th</ta>
            <ta e="T75" id="Seg_9762" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_9763" s="T75">np:P</ta>
            <ta e="T82" id="Seg_9764" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_9765" s="T82">np:P</ta>
            <ta e="T85" id="Seg_9766" s="T84">np:P</ta>
            <ta e="T87" id="Seg_9767" s="T86">pro:G</ta>
            <ta e="T89" id="Seg_9768" s="T88">pro.h:A</ta>
            <ta e="T95" id="Seg_9769" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_9770" s="T95">np:Ins</ta>
            <ta e="T99" id="Seg_9771" s="T98">np:L</ta>
            <ta e="T100" id="Seg_9772" s="T99">0.3.h:Th</ta>
            <ta e="T103" id="Seg_9773" s="T102">0.3.h:A</ta>
            <ta e="T105" id="Seg_9774" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_9775" s="T105">0.2.h:A</ta>
            <ta e="T111" id="Seg_9776" s="T110">0.2.h:A</ta>
            <ta e="T114" id="Seg_9777" s="T113">0.3.h:A</ta>
            <ta e="T115" id="Seg_9778" s="T114">np:L</ta>
            <ta e="T116" id="Seg_9779" s="T115">0.3.h:A</ta>
            <ta e="T118" id="Seg_9780" s="T117">0.3.h:A</ta>
            <ta e="T120" id="Seg_9781" s="T119">np:So</ta>
            <ta e="T123" id="Seg_9782" s="T122">0.3.h:A</ta>
            <ta e="T125" id="Seg_9783" s="T124">np:Th</ta>
            <ta e="T127" id="Seg_9784" s="T126">pro:G</ta>
            <ta e="T129" id="Seg_9785" s="T128">0.3.h:A</ta>
            <ta e="T131" id="Seg_9786" s="T130">0.3.h:P</ta>
            <ta e="T133" id="Seg_9787" s="T132">pro:G</ta>
            <ta e="T135" id="Seg_9788" s="T134">np:Ins</ta>
            <ta e="T136" id="Seg_9789" s="T135">0.2.h:A</ta>
            <ta e="T137" id="Seg_9790" s="T136">pro:G</ta>
            <ta e="T139" id="Seg_9791" s="T138">np:Th</ta>
            <ta e="T142" id="Seg_9792" s="T141">0.2.h:A</ta>
            <ta e="T143" id="Seg_9793" s="T142">np.h:Th</ta>
            <ta e="T146" id="Seg_9794" s="T145">0.1.h:A</ta>
            <ta e="T149" id="Seg_9795" s="T148">0.1.h:A</ta>
            <ta e="T151" id="Seg_9796" s="T150">0.1.h:A</ta>
            <ta e="T153" id="Seg_9797" s="T152">0.1.h:A</ta>
            <ta e="T156" id="Seg_9798" s="T154">np:Th</ta>
            <ta e="T157" id="Seg_9799" s="T156">0.1.h:A</ta>
            <ta e="T158" id="Seg_9800" s="T157">np:Ins</ta>
            <ta e="T159" id="Seg_9801" s="T158">pro:P</ta>
            <ta e="T161" id="Seg_9802" s="T160">0.1.h:A</ta>
            <ta e="T162" id="Seg_9803" s="T161">np:G</ta>
            <ta e="T163" id="Seg_9804" s="T162">0.1.h:A</ta>
            <ta e="T164" id="Seg_9805" s="T163">adv:L</ta>
            <ta e="T167" id="Seg_9806" s="T166">0.1.h:A</ta>
            <ta e="T168" id="Seg_9807" s="T167">adv:L</ta>
            <ta e="T170" id="Seg_9808" s="T169">0.3:P</ta>
            <ta e="T171" id="Seg_9809" s="T170">adv:Time</ta>
            <ta e="T172" id="Seg_9810" s="T171">np:Th 0.1.h:Poss</ta>
            <ta e="T173" id="Seg_9811" s="T172">0.1.h:A</ta>
            <ta e="T174" id="Seg_9812" s="T173">np:E 0.1.h:Poss</ta>
            <ta e="T178" id="Seg_9813" s="T177">adv:Time</ta>
            <ta e="T180" id="Seg_9814" s="T179">0.3:E</ta>
            <ta e="T185" id="Seg_9815" s="T184">np:Th</ta>
            <ta e="T186" id="Seg_9816" s="T185">0.2.h:E</ta>
            <ta e="T192" id="Seg_9817" s="T191">0.2.h:A</ta>
            <ta e="T193" id="Seg_9818" s="T192">np:G</ta>
            <ta e="T194" id="Seg_9819" s="T193">np:Poss</ta>
            <ta e="T197" id="Seg_9820" s="T196">np.h:Poss</ta>
            <ta e="T199" id="Seg_9821" s="T198">pro:A</ta>
            <ta e="T201" id="Seg_9822" s="T200">pro:Pth</ta>
            <ta e="T204" id="Seg_9823" s="T203">np:P</ta>
            <ta e="T206" id="Seg_9824" s="T205">0.2.h:A</ta>
            <ta e="T211" id="Seg_9825" s="T210">np:P</ta>
            <ta e="T212" id="Seg_9826" s="T211">0.2.h:A</ta>
            <ta e="T213" id="Seg_9827" s="T212">np:G</ta>
            <ta e="T214" id="Seg_9828" s="T213">np:Th</ta>
            <ta e="T215" id="Seg_9829" s="T214">0.1.h:A</ta>
            <ta e="T216" id="Seg_9830" s="T215">0.1.h:A</ta>
            <ta e="T217" id="Seg_9831" s="T216">pro.h:A</ta>
            <ta e="T218" id="Seg_9832" s="T217">pro:P</ta>
            <ta e="T220" id="Seg_9833" s="T219">pro.h:Poss</ta>
            <ta e="T221" id="Seg_9834" s="T220">np:Th</ta>
            <ta e="T223" id="Seg_9835" s="T222">pro.h:Th</ta>
            <ta e="T228" id="Seg_9836" s="T227">np.h:Poss</ta>
            <ta e="T230" id="Seg_9837" s="T229">np:Th</ta>
            <ta e="T232" id="Seg_9838" s="T231">np:Th 0.3.h:Poss</ta>
            <ta e="T236" id="Seg_9839" s="T235">0.3.h:A</ta>
            <ta e="T237" id="Seg_9840" s="T236">np:Th 0.3.h:Poss</ta>
            <ta e="T240" id="Seg_9841" s="T239">0.3.h:A</ta>
            <ta e="T241" id="Seg_9842" s="T240">0.2.h:A</ta>
            <ta e="T244" id="Seg_9843" s="T243">0.2.h:A</ta>
            <ta e="T246" id="Seg_9844" s="T245">0.2.h:A</ta>
            <ta e="T248" id="Seg_9845" s="T247">np:Th</ta>
            <ta e="T249" id="Seg_9846" s="T248">0.1.h:E</ta>
            <ta e="T251" id="Seg_9847" s="T250">0.1.h:A</ta>
            <ta e="T252" id="Seg_9848" s="T251">np:A</ta>
            <ta e="T255" id="Seg_9849" s="T254">0.3:A</ta>
            <ta e="T258" id="Seg_9850" s="T257">pro:R</ta>
            <ta e="T261" id="Seg_9851" s="T260">pro:Th</ta>
            <ta e="T266" id="Seg_9852" s="T265">np:P</ta>
            <ta e="T268" id="Seg_9853" s="T267">np:P</ta>
            <ta e="T274" id="Seg_9854" s="T273">np:P</ta>
            <ta e="T276" id="Seg_9855" s="T275">np:Th</ta>
            <ta e="T277" id="Seg_9856" s="T276">0.3.h:A</ta>
            <ta e="T279" id="Seg_9857" s="T278">0.3.h:E</ta>
            <ta e="T281" id="Seg_9858" s="T280">pro:Ins</ta>
            <ta e="T284" id="Seg_9859" s="T283">np.h:A</ta>
            <ta e="T286" id="Seg_9860" s="T285">0.3.h:A</ta>
            <ta e="T288" id="Seg_9861" s="T287">np:Th</ta>
            <ta e="T289" id="Seg_9862" s="T288">0.3.h:A</ta>
            <ta e="T290" id="Seg_9863" s="T289">np:L</ta>
            <ta e="T291" id="Seg_9864" s="T290">0.3.h:A</ta>
            <ta e="T297" id="Seg_9865" s="T296">pro:P</ta>
            <ta e="T300" id="Seg_9866" s="T299">np:A</ta>
            <ta e="T302" id="Seg_9867" s="T301">adv:Time</ta>
            <ta e="T303" id="Seg_9868" s="T302">np:Th</ta>
            <ta e="T304" id="Seg_9869" s="T303">0.3.h:A</ta>
            <ta e="T305" id="Seg_9870" s="T304">adv:L</ta>
            <ta e="T309" id="Seg_9871" s="T308">np.h:A</ta>
            <ta e="T312" id="Seg_9872" s="T311">0.3.h:A</ta>
            <ta e="T314" id="Seg_9873" s="T313">np:Th</ta>
            <ta e="T315" id="Seg_9874" s="T314">np:L 0.1.h:Poss</ta>
            <ta e="T317" id="Seg_9875" s="T316">adv:Time</ta>
            <ta e="T318" id="Seg_9876" s="T317">np.h:A</ta>
            <ta e="T320" id="Seg_9877" s="T319">np:Th</ta>
            <ta e="T321" id="Seg_9878" s="T320">0.3.h:A</ta>
            <ta e="T322" id="Seg_9879" s="T321">np:L</ta>
            <ta e="T323" id="Seg_9880" s="T322">0.3.h:A</ta>
            <ta e="T325" id="Seg_9881" s="T324">np:Th</ta>
            <ta e="T326" id="Seg_9882" s="T325">0.3.h:A</ta>
            <ta e="T327" id="Seg_9883" s="T326">adv:Time</ta>
            <ta e="T328" id="Seg_9884" s="T327">np:P</ta>
            <ta e="T329" id="Seg_9885" s="T328">0.3.h:A</ta>
            <ta e="T330" id="Seg_9886" s="T329">0.3.h:Th</ta>
            <ta e="T333" id="Seg_9887" s="T332">0.3.h:E</ta>
            <ta e="T339" id="Seg_9888" s="T338">np:Th 0.1.h:Poss</ta>
            <ta e="T342" id="Seg_9889" s="T341">np:L</ta>
            <ta e="T344" id="Seg_9890" s="T343">np:A</ta>
            <ta e="T348" id="Seg_9891" s="T347">adv:L</ta>
            <ta e="T351" id="Seg_9892" s="T350">pro.h:A</ta>
            <ta e="T352" id="Seg_9893" s="T351">pro:Th</ta>
            <ta e="T356" id="Seg_9894" s="T355">np:G</ta>
            <ta e="T358" id="Seg_9895" s="T357">0.1.h:A</ta>
            <ta e="T361" id="Seg_9896" s="T360">0.3:P</ta>
            <ta e="T362" id="Seg_9897" s="T361">adv:Time</ta>
            <ta e="T363" id="Seg_9898" s="T362">0.1.h:A</ta>
            <ta e="T364" id="Seg_9899" s="T363">0.1.h:A</ta>
            <ta e="T365" id="Seg_9900" s="T364">adv:Time</ta>
            <ta e="T366" id="Seg_9901" s="T365">0.1.h:A</ta>
            <ta e="T367" id="Seg_9902" s="T366">np:Th</ta>
            <ta e="T370" id="Seg_9903" s="T369">pro:So</ta>
            <ta e="T371" id="Seg_9904" s="T370">np:Th</ta>
            <ta e="T374" id="Seg_9905" s="T373">np.h:A</ta>
            <ta e="T378" id="Seg_9906" s="T377">np.h:A</ta>
            <ta e="T380" id="Seg_9907" s="T379">pro.h:E</ta>
            <ta e="T382" id="Seg_9908" s="T381">pro.h:Th</ta>
            <ta e="T387" id="Seg_9909" s="T386">pro.h:A</ta>
            <ta e="T388" id="Seg_9910" s="T387">adv:L</ta>
            <ta e="T392" id="Seg_9911" s="T391">np.h:A</ta>
            <ta e="T394" id="Seg_9912" s="T393">np.h:A</ta>
            <ta e="T397" id="Seg_9913" s="T396">np:Th</ta>
            <ta e="T399" id="Seg_9914" s="T398">np:G</ta>
            <ta e="T402" id="Seg_9915" s="T401">0.3:P</ta>
            <ta e="T406" id="Seg_9916" s="T405">0.1.h:A</ta>
            <ta e="T409" id="Seg_9917" s="T408">0.2.h:A</ta>
            <ta e="T410" id="Seg_9918" s="T409">np:Ins</ta>
            <ta e="T411" id="Seg_9919" s="T410">pro.h:A</ta>
            <ta e="T415" id="Seg_9920" s="T414">adv:Time</ta>
            <ta e="T418" id="Seg_9921" s="T417">np:G</ta>
            <ta e="T424" id="Seg_9922" s="T423">0.2.h:A</ta>
            <ta e="T426" id="Seg_9923" s="T425">pro:Th</ta>
            <ta e="T428" id="Seg_9924" s="T427">0.2.h:A</ta>
            <ta e="T431" id="Seg_9925" s="T430">0.2.h:A</ta>
            <ta e="T435" id="Seg_9926" s="T434">adv:Time</ta>
            <ta e="T436" id="Seg_9927" s="T435">np:G</ta>
            <ta e="T437" id="Seg_9928" s="T436">0.3:A</ta>
            <ta e="T439" id="Seg_9929" s="T438">np.h:A</ta>
            <ta e="T442" id="Seg_9930" s="T441">pro.h:A</ta>
            <ta e="T443" id="Seg_9931" s="T442">np:Th</ta>
            <ta e="T449" id="Seg_9932" s="T448">np:Th0.1.h:A</ta>
            <ta e="T452" id="Seg_9933" s="T451">0.1.h:A</ta>
            <ta e="T454" id="Seg_9934" s="T453">np.h:P</ta>
            <ta e="T455" id="Seg_9935" s="T454">np.h:A</ta>
            <ta e="T456" id="Seg_9936" s="T455">np.h:P</ta>
            <ta e="T458" id="Seg_9937" s="T457">np:Ins</ta>
            <ta e="T461" id="Seg_9938" s="T460">pro.h:P</ta>
            <ta e="T463" id="Seg_9939" s="T462">adv:L</ta>
            <ta e="T467" id="Seg_9940" s="T466">np:L</ta>
            <ta e="T469" id="Seg_9941" s="T468">np.h:A</ta>
            <ta e="T470" id="Seg_9942" s="T469">np.h:P</ta>
            <ta e="T473" id="Seg_9943" s="T472">np:Ins</ta>
            <ta e="T474" id="Seg_9944" s="T473">np:P 0.3.h:Poss</ta>
            <ta e="T475" id="Seg_9945" s="T474">0.3.h:A</ta>
            <ta e="T476" id="Seg_9946" s="T475">pro.h:Poss</ta>
            <ta e="T477" id="Seg_9947" s="T476">np.h:E</ta>
            <ta e="T483" id="Seg_9948" s="T482">0.1.h:E</ta>
            <ta e="T485" id="Seg_9949" s="T484">pro.h:Poss</ta>
            <ta e="T486" id="Seg_9950" s="T485">np:P</ta>
            <ta e="T488" id="Seg_9951" s="T487">0.3.h:A</ta>
            <ta e="T489" id="Seg_9952" s="T488">pro.h:Th</ta>
            <ta e="T495" id="Seg_9953" s="T494">np.h:Th</ta>
            <ta e="T502" id="Seg_9954" s="T501">np:Th</ta>
            <ta e="T505" id="Seg_9955" s="T504">adv:Time</ta>
            <ta e="T507" id="Seg_9956" s="T506">0.2.h:Th</ta>
            <ta e="T508" id="Seg_9957" s="T507">adv:Time</ta>
            <ta e="T511" id="Seg_9958" s="T510">0.2.h:A</ta>
            <ta e="T513" id="Seg_9959" s="T512">pro:Th</ta>
            <ta e="T514" id="Seg_9960" s="T513">0.1.h:A</ta>
            <ta e="T515" id="Seg_9961" s="T514">pro.h:A</ta>
            <ta e="T516" id="Seg_9962" s="T515">adv:Time</ta>
            <ta e="T517" id="Seg_9963" s="T516">np:R</ta>
            <ta e="T523" id="Seg_9964" s="T522">0.2:A</ta>
            <ta e="T524" id="Seg_9965" s="T523">pro:G</ta>
            <ta e="T525" id="Seg_9966" s="T524">0.2:E</ta>
            <ta e="T527" id="Seg_9967" s="T526">pro.h:A</ta>
            <ta e="T529" id="Seg_9968" s="T528">np:Th</ta>
            <ta e="T531" id="Seg_9969" s="T530">0.2.h:A</ta>
            <ta e="T534" id="Seg_9970" s="T533">0.3:Th</ta>
            <ta e="T539" id="Seg_9971" s="T538">0.2:A</ta>
            <ta e="T540" id="Seg_9972" s="T539">np:Ins</ta>
            <ta e="T541" id="Seg_9973" s="T540">pro.h:Th</ta>
            <ta e="T542" id="Seg_9974" s="T541">np:L</ta>
            <ta e="T544" id="Seg_9975" s="T543">np:Th</ta>
            <ta e="T546" id="Seg_9976" s="T545">0.2.h:A</ta>
            <ta e="T547" id="Seg_9977" s="T546">pro.h:P</ta>
            <ta e="T549" id="Seg_9978" s="T548">0.2.h:A</ta>
            <ta e="T552" id="Seg_9979" s="T551">np:Th 0.2.h:Poss</ta>
            <ta e="T556" id="Seg_9980" s="T555">np.h:B</ta>
            <ta e="T558" id="Seg_9981" s="T557">0.2:E</ta>
            <ta e="T560" id="Seg_9982" s="T559">np:Th</ta>
            <ta e="T561" id="Seg_9983" s="T560">0.2:A</ta>
            <ta e="T565" id="Seg_9984" s="T564">np:Th 0.2:Poss</ta>
            <ta e="T567" id="Seg_9985" s="T566">pro.h:E</ta>
            <ta e="T570" id="Seg_9986" s="T569">np.h:B</ta>
            <ta e="T572" id="Seg_9987" s="T571">np:Th</ta>
            <ta e="T573" id="Seg_9988" s="T572">pro.h:R</ta>
            <ta e="T574" id="Seg_9989" s="T573">0.2.h:A</ta>
            <ta e="T576" id="Seg_9990" s="T575">np:Th</ta>
            <ta e="T577" id="Seg_9991" s="T576">0.2:A</ta>
            <ta e="T578" id="Seg_9992" s="T577">np:Th</ta>
            <ta e="T579" id="Seg_9993" s="T578">0.2:A</ta>
            <ta e="T581" id="Seg_9994" s="T580">pro.h:B</ta>
            <ta e="T582" id="Seg_9995" s="T581">0.2:E</ta>
            <ta e="T588" id="Seg_9996" s="T587">pro:A</ta>
            <ta e="T590" id="Seg_9997" s="T589">np:Th</ta>
            <ta e="T592" id="Seg_9998" s="T591">np:Th</ta>
            <ta e="T593" id="Seg_9999" s="T592">0.2:A</ta>
            <ta e="T595" id="Seg_10000" s="T594">np:P</ta>
            <ta e="T600" id="Seg_10001" s="T599">np:Com</ta>
            <ta e="T602" id="Seg_10002" s="T601">pro:G</ta>
            <ta e="T603" id="Seg_10003" s="T602">0.2.h:A</ta>
            <ta e="T604" id="Seg_10004" s="T603">np:Com</ta>
            <ta e="T609" id="Seg_10005" s="T608">np:Com</ta>
            <ta e="T610" id="Seg_10006" s="T609">np:Th</ta>
            <ta e="T612" id="Seg_10007" s="T611">pro.h:A</ta>
            <ta e="T615" id="Seg_10008" s="T614">np:G</ta>
            <ta e="T616" id="Seg_10009" s="T615">0.1.h:A</ta>
            <ta e="T618" id="Seg_10010" s="T617">np:Th</ta>
            <ta e="T620" id="Seg_10011" s="T619">np:P</ta>
            <ta e="T622" id="Seg_10012" s="T621">pro.h:A</ta>
            <ta e="T623" id="Seg_10013" s="T622">pro.h:B</ta>
            <ta e="T626" id="Seg_10014" s="T625">pro.h:A</ta>
            <ta e="T627" id="Seg_10015" s="T626">pro.h:B</ta>
            <ta e="T631" id="Seg_10016" s="T630">0.3.h:A</ta>
            <ta e="T632" id="Seg_10017" s="T631">pro.h:R</ta>
            <ta e="T634" id="Seg_10018" s="T633">pro.h:A</ta>
            <ta e="T638" id="Seg_10019" s="T637">pro.h:A</ta>
            <ta e="T641" id="Seg_10020" s="T640">pro.h:B</ta>
            <ta e="T644" id="Seg_10021" s="T643">np:Ins</ta>
            <ta e="T646" id="Seg_10022" s="T645">0.3.h:A</ta>
            <ta e="T653" id="Seg_10023" s="T652">0.3:A</ta>
            <ta e="T654" id="Seg_10024" s="T653">np.h:E</ta>
            <ta e="T662" id="Seg_10025" s="T661">np.h:E</ta>
            <ta e="T663" id="Seg_10026" s="T662">0.3:A</ta>
            <ta e="T665" id="Seg_10027" s="T664">pro.h:E</ta>
            <ta e="T666" id="Seg_10028" s="T665">0.3:A</ta>
            <ta e="T667" id="Seg_10029" s="T666">pro.h:A</ta>
            <ta e="T668" id="Seg_10030" s="T667">pro.h:B</ta>
            <ta e="T670" id="Seg_10031" s="T669">pro.h:A</ta>
            <ta e="T674" id="Seg_10032" s="T673">0.2.h:Th</ta>
            <ta e="T675" id="Seg_10033" s="T674">np:So</ta>
            <ta e="T676" id="Seg_10034" s="T675">0.1.h:A</ta>
            <ta e="T677" id="Seg_10035" s="T676">0.1.h:A</ta>
            <ta e="T680" id="Seg_10036" s="T679">0.1.h:A</ta>
            <ta e="T683" id="Seg_10037" s="T682">adv:Time</ta>
            <ta e="T687" id="Seg_10038" s="T686">np:G</ta>
            <ta e="T689" id="Seg_10039" s="T688">0.2.h:E</ta>
            <ta e="T693" id="Seg_10040" s="T692">np:Ins</ta>
            <ta e="T695" id="Seg_10041" s="T694">np:Ins</ta>
            <ta e="T696" id="Seg_10042" s="T695">np:G</ta>
            <ta e="T698" id="Seg_10043" s="T697">0.2.h:A</ta>
            <ta e="T699" id="Seg_10044" s="T698">np:Ins</ta>
            <ta e="T701" id="Seg_10045" s="T700">np:Ins</ta>
            <ta e="T703" id="Seg_10046" s="T702">np:Ins</ta>
            <ta e="T706" id="Seg_10047" s="T705">np:G</ta>
            <ta e="T707" id="Seg_10048" s="T706">0.3.h:A</ta>
            <ta e="T708" id="Seg_10049" s="T707">np:P</ta>
            <ta e="T709" id="Seg_10050" s="T708">0.3.h:A</ta>
            <ta e="T714" id="Seg_10051" s="T713">pro.h:B</ta>
            <ta e="T716" id="Seg_10052" s="T715">pro.h:B</ta>
            <ta e="T718" id="Seg_10053" s="T717">pro.h:B</ta>
            <ta e="T727" id="Seg_10054" s="T726">pro.h:Th</ta>
            <ta e="T731" id="Seg_10055" s="T730">np:L</ta>
            <ta e="T732" id="Seg_10056" s="T731">pro.h:A</ta>
            <ta e="T735" id="Seg_10057" s="T734">np:L</ta>
            <ta e="T736" id="Seg_10058" s="T735">np.h:Th</ta>
            <ta e="T739" id="Seg_10059" s="T738">np:L</ta>
            <ta e="T740" id="Seg_10060" s="T739">np.h:Th</ta>
            <ta e="T743" id="Seg_10061" s="T742">np:L</ta>
            <ta e="T744" id="Seg_10062" s="T743">np.h:Th</ta>
            <ta e="T748" id="Seg_10063" s="T747">np:L</ta>
            <ta e="T749" id="Seg_10064" s="T748">np.h:Th</ta>
            <ta e="T753" id="Seg_10065" s="T752">np.h:A</ta>
            <ta e="T755" id="Seg_10066" s="T754">pro.h:Th</ta>
            <ta e="T763" id="Seg_10067" s="T762">np:Th</ta>
            <ta e="T768" id="Seg_10068" s="T767">0.2.h:E</ta>
            <ta e="T776" id="Seg_10069" s="T775">adv:Time</ta>
            <ta e="T777" id="Seg_10070" s="T776">np.h:A</ta>
            <ta e="T780" id="Seg_10071" s="T779">np:Th</ta>
            <ta e="T781" id="Seg_10072" s="T780">0.3.h:A</ta>
            <ta e="T784" id="Seg_10073" s="T783">0.3:A</ta>
            <ta e="T786" id="Seg_10074" s="T785">np:L</ta>
            <ta e="T788" id="Seg_10075" s="T787">0.3:Th</ta>
            <ta e="T791" id="Seg_10076" s="T790">np:So</ta>
            <ta e="T794" id="Seg_10077" s="T793">np:So</ta>
            <ta e="T796" id="Seg_10078" s="T795">np:G</ta>
            <ta e="T797" id="Seg_10079" s="T796">0.3.h:E</ta>
            <ta e="T798" id="Seg_10080" s="T797">adv:G</ta>
            <ta e="T808" id="Seg_10081" s="T807">np:E</ta>
            <ta e="T809" id="Seg_10082" s="T808">adv:L</ta>
            <ta e="T811" id="Seg_10083" s="T810">np:L</ta>
            <ta e="T816" id="Seg_10084" s="T815">np:L</ta>
            <ta e="T817" id="Seg_10085" s="T816">np:E 0.1.h:Poss</ta>
            <ta e="T820" id="Seg_10086" s="T819">0.1.h:A</ta>
            <ta e="T825" id="Seg_10087" s="T824">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T5" id="Seg_10088" s="T4">np:S</ta>
            <ta e="T6" id="Seg_10089" s="T5">ptcl.neg</ta>
            <ta e="T7" id="Seg_10090" s="T6">adj:pred</ta>
            <ta e="T8" id="Seg_10091" s="T7">np:S</ta>
            <ta e="T10" id="Seg_10092" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_10093" s="T10">np:S</ta>
            <ta e="T16" id="Seg_10094" s="T15">np:S</ta>
            <ta e="T18" id="Seg_10095" s="T17">v:pred</ta>
            <ta e="T20" id="Seg_10096" s="T19">ptcl:pred</ta>
            <ta e="T21" id="Seg_10097" s="T20">pro.h:S</ta>
            <ta e="T24" id="Seg_10098" s="T23">v:pred</ta>
            <ta e="T26" id="Seg_10099" s="T25">pro.h:S</ta>
            <ta e="T28" id="Seg_10100" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_10101" s="T29">pro.h:S</ta>
            <ta e="T31" id="Seg_10102" s="T30">v:pred</ta>
            <ta e="T34" id="Seg_10103" s="T33">pro.h:O</ta>
            <ta e="T35" id="Seg_10104" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T36" id="Seg_10105" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_10106" s="T36">ptcl.neg</ta>
            <ta e="T38" id="Seg_10107" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_10108" s="T38">np:S</ta>
            <ta e="T42" id="Seg_10109" s="T41">np:O</ta>
            <ta e="T43" id="Seg_10110" s="T42">v:pred</ta>
            <ta e="T47" id="Seg_10111" s="T46">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_10112" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_10113" s="T53">ptcl.neg</ta>
            <ta e="T55" id="Seg_10114" s="T54">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_10115" s="T56">ptcl.neg</ta>
            <ta e="T58" id="Seg_10116" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_10117" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_10118" s="T64">ptcl.neg</ta>
            <ta e="T66" id="Seg_10119" s="T65">v:pred 0.1.h:S</ta>
            <ta e="T67" id="Seg_10120" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T73" id="Seg_10121" s="T72">np:S</ta>
            <ta e="T74" id="Seg_10122" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_10123" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_10124" s="T75">np:O</ta>
            <ta e="T77" id="Seg_10125" s="T76">s:purp</ta>
            <ta e="T82" id="Seg_10126" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_10127" s="T82">np:O</ta>
            <ta e="T84" id="Seg_10128" s="T83">s:purp</ta>
            <ta e="T85" id="Seg_10129" s="T84">np:O</ta>
            <ta e="T86" id="Seg_10130" s="T85">s:purp</ta>
            <ta e="T89" id="Seg_10131" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_10132" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_10133" s="T91">ptcl.neg</ta>
            <ta e="T93" id="Seg_10134" s="T92">ptcl.neg</ta>
            <ta e="T95" id="Seg_10135" s="T94">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_10136" s="T96">ptcl.neg</ta>
            <ta e="T100" id="Seg_10137" s="T99">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_10138" s="T101">ptcl.neg</ta>
            <ta e="T103" id="Seg_10139" s="T102">v:pred 0.3.h:S</ta>
            <ta e="T105" id="Seg_10140" s="T104">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_10141" s="T105">v:pred 0.2.h:S</ta>
            <ta e="T111" id="Seg_10142" s="T110">v:pred 0.2.h:S</ta>
            <ta e="T113" id="Seg_10143" s="T112">ptcl.neg</ta>
            <ta e="T114" id="Seg_10144" s="T113">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_10145" s="T115">v:pred 0.3.h:S</ta>
            <ta e="T118" id="Seg_10146" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_10147" s="T122">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_10148" s="T124">np:S</ta>
            <ta e="T126" id="Seg_10149" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_10150" s="T127">conv:pred</ta>
            <ta e="T129" id="Seg_10151" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T131" id="Seg_10152" s="T130">v:pred 0.3.h:S</ta>
            <ta e="T136" id="Seg_10153" s="T135">v:pred 0.2.h:S</ta>
            <ta e="T139" id="Seg_10154" s="T138">np:O</ta>
            <ta e="T142" id="Seg_10155" s="T141">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_10156" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_10157" s="T143">adj:pred</ta>
            <ta e="T145" id="Seg_10158" s="T144">ptcl.neg</ta>
            <ta e="T146" id="Seg_10159" s="T145">v:pred 0.1.h:S</ta>
            <ta e="T149" id="Seg_10160" s="T148">v:pred 0.1.h:S</ta>
            <ta e="T151" id="Seg_10161" s="T150">v:pred 0.1.h:S</ta>
            <ta e="T152" id="Seg_10162" s="T151">ptcl.neg</ta>
            <ta e="T153" id="Seg_10163" s="T152">v:pred 0.1.h:S</ta>
            <ta e="T156" id="Seg_10164" s="T154">np:O</ta>
            <ta e="T157" id="Seg_10165" s="T156">v:pred 0.1.h:S</ta>
            <ta e="T159" id="Seg_10166" s="T158">pro:O</ta>
            <ta e="T161" id="Seg_10167" s="T160">v:pred 0.1.h:S</ta>
            <ta e="T163" id="Seg_10168" s="T162">v:pred 0.1.h:S</ta>
            <ta e="T167" id="Seg_10169" s="T166">v:pred 0.1.h:S</ta>
            <ta e="T169" id="Seg_10170" s="T168">n:pred</ta>
            <ta e="T170" id="Seg_10171" s="T169">cop 0.3:S</ta>
            <ta e="T172" id="Seg_10172" s="T171">np:O</ta>
            <ta e="T173" id="Seg_10173" s="T172">v:pred 0.1.h:S</ta>
            <ta e="T174" id="Seg_10174" s="T173">np:S</ta>
            <ta e="T177" id="Seg_10175" s="T176">v:pred</ta>
            <ta e="T179" id="Seg_10176" s="T178">ptcl.neg</ta>
            <ta e="T180" id="Seg_10177" s="T179">v:pred 0.3:S</ta>
            <ta e="T185" id="Seg_10178" s="T184">np:O</ta>
            <ta e="T186" id="Seg_10179" s="T185">v:pred 0.2.h:S</ta>
            <ta e="T192" id="Seg_10180" s="T191">v:pred 0.2.h:S</ta>
            <ta e="T199" id="Seg_10181" s="T198">pro:S</ta>
            <ta e="T203" id="Seg_10182" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_10183" s="T203">np:O </ta>
            <ta e="T206" id="Seg_10184" s="T205">v:pred 0.2.h:S</ta>
            <ta e="T210" id="Seg_10185" s="T209">adj:pred</ta>
            <ta e="T211" id="Seg_10186" s="T210">np:O</ta>
            <ta e="T212" id="Seg_10187" s="T211">v:pred 0.2.h:S</ta>
            <ta e="T214" id="Seg_10188" s="T213">np:O</ta>
            <ta e="T215" id="Seg_10189" s="T214">v:pred 0.1.h:S</ta>
            <ta e="T216" id="Seg_10190" s="T215">v:pred 0.1.h:S</ta>
            <ta e="T217" id="Seg_10191" s="T216">pro.h:S</ta>
            <ta e="T218" id="Seg_10192" s="T217">pro:O</ta>
            <ta e="T219" id="Seg_10193" s="T218">v:pred</ta>
            <ta e="T221" id="Seg_10194" s="T220">np:S</ta>
            <ta e="T222" id="Seg_10195" s="T221">adj:pred</ta>
            <ta e="T223" id="Seg_10196" s="T222">pro.h:S</ta>
            <ta e="T225" id="Seg_10197" s="T224">adj:pred</ta>
            <ta e="T226" id="Seg_10198" s="T225">cop</ta>
            <ta e="T230" id="Seg_10199" s="T229">np:S</ta>
            <ta e="T231" id="Seg_10200" s="T230">v:pred</ta>
            <ta e="T232" id="Seg_10201" s="T231">np:S</ta>
            <ta e="T233" id="Seg_10202" s="T232">v:pred</ta>
            <ta e="T235" id="Seg_10203" s="T234">ptcl.neg</ta>
            <ta e="T236" id="Seg_10204" s="T235">v:pred 0.3.h:S</ta>
            <ta e="T237" id="Seg_10205" s="T236">np:S</ta>
            <ta e="T238" id="Seg_10206" s="T237">v:pred</ta>
            <ta e="T239" id="Seg_10207" s="T238">ptcl.neg</ta>
            <ta e="T240" id="Seg_10208" s="T239">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_10209" s="T240">v:pred 0.2.h:S</ta>
            <ta e="T244" id="Seg_10210" s="T243">v:pred 0.2.h:S</ta>
            <ta e="T246" id="Seg_10211" s="T245">v:pred 0.2.h:S</ta>
            <ta e="T248" id="Seg_10212" s="T247">np:O</ta>
            <ta e="T249" id="Seg_10213" s="T248">v:pred 0.1.h:S</ta>
            <ta e="T251" id="Seg_10214" s="T250">v:pred 0.1.h:S</ta>
            <ta e="T252" id="Seg_10215" s="T251">np:S</ta>
            <ta e="T253" id="Seg_10216" s="T252">v:pred</ta>
            <ta e="T255" id="Seg_10217" s="T254">v:pred 0.3:S</ta>
            <ta e="T257" id="Seg_10218" s="T256">ptcl:pred</ta>
            <ta e="T258" id="Seg_10219" s="T257">pro:O</ta>
            <ta e="T261" id="Seg_10220" s="T260">pro:S</ta>
            <ta e="T262" id="Seg_10221" s="T261">v:pred</ta>
            <ta e="T264" id="Seg_10222" s="T263">ptcl:pred</ta>
            <ta e="T266" id="Seg_10223" s="T265">np:O</ta>
            <ta e="T268" id="Seg_10224" s="T267">np:O</ta>
            <ta e="T273" id="Seg_10225" s="T272">s:purp</ta>
            <ta e="T274" id="Seg_10226" s="T273">np:O</ta>
            <ta e="T275" id="Seg_10227" s="T274">s:purp</ta>
            <ta e="T276" id="Seg_10228" s="T275">np:O</ta>
            <ta e="T277" id="Seg_10229" s="T276">v:pred 0.3.h:S</ta>
            <ta e="T278" id="Seg_10230" s="T277">ptcl.neg</ta>
            <ta e="T279" id="Seg_10231" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T282" id="Seg_10232" s="T281">s:purp</ta>
            <ta e="T284" id="Seg_10233" s="T283">np.h:S</ta>
            <ta e="T285" id="Seg_10234" s="T284">v:pred</ta>
            <ta e="T286" id="Seg_10235" s="T285">v:pred 0.3.h:S</ta>
            <ta e="T288" id="Seg_10236" s="T287">np:O</ta>
            <ta e="T289" id="Seg_10237" s="T288">v:pred 0.3.h:S</ta>
            <ta e="T291" id="Seg_10238" s="T290">v:pred 0.3.h:S</ta>
            <ta e="T297" id="Seg_10239" s="T296">pro:S</ta>
            <ta e="T299" id="Seg_10240" s="T298">v:pred</ta>
            <ta e="T300" id="Seg_10241" s="T299">np:S</ta>
            <ta e="T301" id="Seg_10242" s="T300">v:pred</ta>
            <ta e="T303" id="Seg_10243" s="T302">np:O</ta>
            <ta e="T304" id="Seg_10244" s="T303">v:pred 0.3.h:S</ta>
            <ta e="T309" id="Seg_10245" s="T308">np.h:S</ta>
            <ta e="T310" id="Seg_10246" s="T309">v:pred</ta>
            <ta e="T312" id="Seg_10247" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T314" id="Seg_10248" s="T313">np:S</ta>
            <ta e="T316" id="Seg_10249" s="T315">v:pred</ta>
            <ta e="T318" id="Seg_10250" s="T317">np.h:S</ta>
            <ta e="T319" id="Seg_10251" s="T318">v:pred</ta>
            <ta e="T320" id="Seg_10252" s="T319">np:O</ta>
            <ta e="T321" id="Seg_10253" s="T320">v:pred 0.3.h:S</ta>
            <ta e="T323" id="Seg_10254" s="T322">v:pred 0.3.h:S</ta>
            <ta e="T325" id="Seg_10255" s="T324">np:O</ta>
            <ta e="T326" id="Seg_10256" s="T325">v:pred 0.3.h:S</ta>
            <ta e="T328" id="Seg_10257" s="T327">np:O</ta>
            <ta e="T329" id="Seg_10258" s="T328">v:pred 0.3.h:S</ta>
            <ta e="T330" id="Seg_10259" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T333" id="Seg_10260" s="T332">v:pred 0.3.h:S</ta>
            <ta e="T339" id="Seg_10261" s="T338">np:S</ta>
            <ta e="T343" id="Seg_10262" s="T342">v:pred</ta>
            <ta e="T344" id="Seg_10263" s="T343">np:S</ta>
            <ta e="T346" id="Seg_10264" s="T345">v:pred</ta>
            <ta e="T350" id="Seg_10265" s="T349">adj:pred</ta>
            <ta e="T351" id="Seg_10266" s="T350">pro.h:S</ta>
            <ta e="T352" id="Seg_10267" s="T351">pro:O</ta>
            <ta e="T354" id="Seg_10268" s="T353">v:pred</ta>
            <ta e="T358" id="Seg_10269" s="T357">v:pred 0.1.h:S</ta>
            <ta e="T360" id="Seg_10270" s="T359">adj:pred</ta>
            <ta e="T361" id="Seg_10271" s="T360">cop 0.3:S</ta>
            <ta e="T363" id="Seg_10272" s="T362">v:pred 0.1.h:S</ta>
            <ta e="T364" id="Seg_10273" s="T363">v:pred 0.1.h:S</ta>
            <ta e="T366" id="Seg_10274" s="T365">v:pred 0.1.h:S</ta>
            <ta e="T367" id="Seg_10275" s="T366">np:S</ta>
            <ta e="T369" id="Seg_10276" s="T368">v:pred</ta>
            <ta e="T371" id="Seg_10277" s="T370">np:S</ta>
            <ta e="T372" id="Seg_10278" s="T371">v:pred</ta>
            <ta e="T374" id="Seg_10279" s="T373">np.h:S</ta>
            <ta e="T375" id="Seg_10280" s="T374">v:pred</ta>
            <ta e="T376" id="Seg_10281" s="T375">conv:pred s:purp</ta>
            <ta e="T378" id="Seg_10282" s="T377">np.h:S</ta>
            <ta e="T379" id="Seg_10283" s="T378">v:pred</ta>
            <ta e="T380" id="Seg_10284" s="T379">pro.h:S</ta>
            <ta e="T382" id="Seg_10285" s="T381">pro.h:O</ta>
            <ta e="T383" id="Seg_10286" s="T382">ptcl.neg</ta>
            <ta e="T384" id="Seg_10287" s="T383">v:pred</ta>
            <ta e="T387" id="Seg_10288" s="T386">pro.h:S</ta>
            <ta e="T389" id="Seg_10289" s="T388">v:pred</ta>
            <ta e="T392" id="Seg_10290" s="T391">np.h:S</ta>
            <ta e="T394" id="Seg_10291" s="T393">np.h:S</ta>
            <ta e="T395" id="Seg_10292" s="T394">v:pred</ta>
            <ta e="T396" id="Seg_10293" s="T395">ptcl:pred</ta>
            <ta e="T397" id="Seg_10294" s="T396">np:O</ta>
            <ta e="T398" id="Seg_10295" s="T397">v:pred</ta>
            <ta e="T400" id="Seg_10296" s="T399">ptcl:pred</ta>
            <ta e="T401" id="Seg_10297" s="T400">adj:pred</ta>
            <ta e="T402" id="Seg_10298" s="T401">cop 0.3:S</ta>
            <ta e="T404" id="Seg_10299" s="T403">conv:pred</ta>
            <ta e="T406" id="Seg_10300" s="T405">v:pred 0.1.h:S</ta>
            <ta e="T409" id="Seg_10301" s="T408">v:pred 0.2.h:S</ta>
            <ta e="T411" id="Seg_10302" s="T410">pro.h:S</ta>
            <ta e="T414" id="Seg_10303" s="T413">v:pred</ta>
            <ta e="T417" id="Seg_10304" s="T416">ptcl:pred</ta>
            <ta e="T420" id="Seg_10305" s="T419">s:purp</ta>
            <ta e="T424" id="Seg_10306" s="T423">v:pred 0.2.h:S</ta>
            <ta e="T426" id="Seg_10307" s="T425">pro:O</ta>
            <ta e="T427" id="Seg_10308" s="T426">ptcl.neg</ta>
            <ta e="T428" id="Seg_10309" s="T427">v:pred 0.2.h:S</ta>
            <ta e="T431" id="Seg_10310" s="T430">v:pred 0.2.h:S</ta>
            <ta e="T432" id="Seg_10311" s="T431">s:purp</ta>
            <ta e="T433" id="Seg_10312" s="T432">v:pred</ta>
            <ta e="T437" id="Seg_10313" s="T436">v:pred 0.3:S</ta>
            <ta e="T439" id="Seg_10314" s="T438">np.h:S</ta>
            <ta e="T440" id="Seg_10315" s="T439">v:pred</ta>
            <ta e="T442" id="Seg_10316" s="T441">pro.h:S</ta>
            <ta e="T443" id="Seg_10317" s="T442">np:O</ta>
            <ta e="T444" id="Seg_10318" s="T443">v:pred</ta>
            <ta e="T449" id="Seg_10319" s="T448">np:O</ta>
            <ta e="T450" id="Seg_10320" s="T449">v:pred 0.1.h:S</ta>
            <ta e="T452" id="Seg_10321" s="T451">v:pred 0.1.h:S</ta>
            <ta e="T454" id="Seg_10322" s="T453">np.h:O</ta>
            <ta e="T455" id="Seg_10323" s="T454">np.h:S</ta>
            <ta e="T456" id="Seg_10324" s="T455">np.h:O</ta>
            <ta e="T459" id="Seg_10325" s="T458">v:pred</ta>
            <ta e="T461" id="Seg_10326" s="T460">pro.h:S</ta>
            <ta e="T462" id="Seg_10327" s="T461">v:pred</ta>
            <ta e="T469" id="Seg_10328" s="T468">np.h:S</ta>
            <ta e="T470" id="Seg_10329" s="T469">np.h:O</ta>
            <ta e="T472" id="Seg_10330" s="T471">v:pred</ta>
            <ta e="T474" id="Seg_10331" s="T473">np:O</ta>
            <ta e="T475" id="Seg_10332" s="T474">v:pred 0.3.h:S</ta>
            <ta e="T477" id="Seg_10333" s="T476">np.h:S</ta>
            <ta e="T479" id="Seg_10334" s="T478">v:pred</ta>
            <ta e="T483" id="Seg_10335" s="T482">v:pred 0.1.h:S</ta>
            <ta e="T486" id="Seg_10336" s="T485">np:O</ta>
            <ta e="T487" id="Seg_10337" s="T486">ptcl.neg</ta>
            <ta e="T488" id="Seg_10338" s="T487">v:pred 0.3.h:S</ta>
            <ta e="T489" id="Seg_10339" s="T488">pro.h:S</ta>
            <ta e="T491" id="Seg_10340" s="T490">adj:pred</ta>
            <ta e="T492" id="Seg_10341" s="T491">cop</ta>
            <ta e="T495" id="Seg_10342" s="T494">np.h:S</ta>
            <ta e="T496" id="Seg_10343" s="T495">v:pred</ta>
            <ta e="T502" id="Seg_10344" s="T501">np:S</ta>
            <ta e="T503" id="Seg_10345" s="T502">conv:pred</ta>
            <ta e="T504" id="Seg_10346" s="T503">v:pred</ta>
            <ta e="T506" id="Seg_10347" s="T505">ptcl.neg</ta>
            <ta e="T507" id="Seg_10348" s="T506">v:pred 0.2.h:S</ta>
            <ta e="T511" id="Seg_10349" s="T510">v:pred 0.2.h:S</ta>
            <ta e="T512" id="Seg_10350" s="T511">s:purp</ta>
            <ta e="T513" id="Seg_10351" s="T512">pro:O</ta>
            <ta e="T514" id="Seg_10352" s="T513">v:pred 0.1.h:S</ta>
            <ta e="T515" id="Seg_10353" s="T514">pro.h:S</ta>
            <ta e="T521" id="Seg_10354" s="T520">v:pred</ta>
            <ta e="T523" id="Seg_10355" s="T522">v:pred 0.2:S</ta>
            <ta e="T525" id="Seg_10356" s="T524">v:pred 0.2:S</ta>
            <ta e="T527" id="Seg_10357" s="T526">pro.h:S</ta>
            <ta e="T528" id="Seg_10358" s="T527">v:pred</ta>
            <ta e="T529" id="Seg_10359" s="T528">np:O</ta>
            <ta e="T531" id="Seg_10360" s="T530">v:pred 0.2.h:S</ta>
            <ta e="T534" id="Seg_10361" s="T533">v:pred 0.3:S</ta>
            <ta e="T536" id="Seg_10362" s="T535">ptcl.neg</ta>
            <ta e="T537" id="Seg_10363" s="T536">adj:pred</ta>
            <ta e="T539" id="Seg_10364" s="T538">v:pred 0.2:S</ta>
            <ta e="T541" id="Seg_10365" s="T540">pro.h:S</ta>
            <ta e="T543" id="Seg_10366" s="T542">v:pred</ta>
            <ta e="T544" id="Seg_10367" s="T543">np:S</ta>
            <ta e="T545" id="Seg_10368" s="T544">v:pred</ta>
            <ta e="T546" id="Seg_10369" s="T545">v:pred 0.2.h:S</ta>
            <ta e="T547" id="Seg_10370" s="T546">pro.h:O</ta>
            <ta e="T549" id="Seg_10371" s="T548">v:pred 0.2.h:S</ta>
            <ta e="T552" id="Seg_10372" s="T551">np:S</ta>
            <ta e="T553" id="Seg_10373" s="T552">adj:pred</ta>
            <ta e="T556" id="Seg_10374" s="T555">np.h:O</ta>
            <ta e="T558" id="Seg_10375" s="T557">v:pred 0.2:S</ta>
            <ta e="T560" id="Seg_10376" s="T559">np:O</ta>
            <ta e="T561" id="Seg_10377" s="T560">v:pred 0.2:S</ta>
            <ta e="T565" id="Seg_10378" s="T564">np:S</ta>
            <ta e="T566" id="Seg_10379" s="T565">adj:pred</ta>
            <ta e="T567" id="Seg_10380" s="T566">pro.h:S</ta>
            <ta e="T570" id="Seg_10381" s="T569">np.h:O</ta>
            <ta e="T571" id="Seg_10382" s="T570">v:pred</ta>
            <ta e="T572" id="Seg_10383" s="T571">np:O</ta>
            <ta e="T574" id="Seg_10384" s="T573">v:pred 0.2:S</ta>
            <ta e="T576" id="Seg_10385" s="T575">np:O</ta>
            <ta e="T577" id="Seg_10386" s="T576">v:pred 0.2:S</ta>
            <ta e="T578" id="Seg_10387" s="T577">np:O</ta>
            <ta e="T579" id="Seg_10388" s="T578">v:pred 0.2:S</ta>
            <ta e="T581" id="Seg_10389" s="T580">pro.h:O</ta>
            <ta e="T582" id="Seg_10390" s="T581">v:pred 0.2:S</ta>
            <ta e="T588" id="Seg_10391" s="T587">pro:S</ta>
            <ta e="T590" id="Seg_10392" s="T589">np:O</ta>
            <ta e="T591" id="Seg_10393" s="T590">v:pred</ta>
            <ta e="T592" id="Seg_10394" s="T591">np:O</ta>
            <ta e="T593" id="Seg_10395" s="T592">v:pred 0.2:S</ta>
            <ta e="T594" id="Seg_10396" s="T593">s:purp</ta>
            <ta e="T595" id="Seg_10397" s="T594">np:S</ta>
            <ta e="T596" id="Seg_10398" s="T595">v:pred</ta>
            <ta e="T601" id="Seg_10399" s="T600">adj:pred</ta>
            <ta e="T603" id="Seg_10400" s="T602">v:pred 0.2.h:S</ta>
            <ta e="T608" id="Seg_10401" s="T607">adj:pred</ta>
            <ta e="T610" id="Seg_10402" s="T609">np:S</ta>
            <ta e="T611" id="Seg_10403" s="T610">v:pred</ta>
            <ta e="T612" id="Seg_10404" s="T611">pro.h:S</ta>
            <ta e="T613" id="Seg_10405" s="T612">v:pred</ta>
            <ta e="T616" id="Seg_10406" s="T615">v:pred 0.1.h:S</ta>
            <ta e="T618" id="Seg_10407" s="T617">np:S</ta>
            <ta e="T619" id="Seg_10408" s="T618">v:pred</ta>
            <ta e="T620" id="Seg_10409" s="T619">np:S</ta>
            <ta e="T621" id="Seg_10410" s="T620">v:pred</ta>
            <ta e="T622" id="Seg_10411" s="T621">pro.h:S</ta>
            <ta e="T624" id="Seg_10412" s="T623">v:pred</ta>
            <ta e="T626" id="Seg_10413" s="T625">pro.h:S</ta>
            <ta e="T628" id="Seg_10414" s="T627">ptcl.neg</ta>
            <ta e="T629" id="Seg_10415" s="T628">v:pred</ta>
            <ta e="T630" id="Seg_10416" s="T629">ptcl.neg</ta>
            <ta e="T631" id="Seg_10417" s="T630">v:pred 0.3.h:S</ta>
            <ta e="T634" id="Seg_10418" s="T633">pro.h:S</ta>
            <ta e="T636" id="Seg_10419" s="T635">v:pred</ta>
            <ta e="T638" id="Seg_10420" s="T637">pro.h:S</ta>
            <ta e="T640" id="Seg_10421" s="T639">v:pred</ta>
            <ta e="T642" id="Seg_10422" s="T641">adj:pred</ta>
            <ta e="T646" id="Seg_10423" s="T645">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_10424" s="T652">v:pred 0.3:S</ta>
            <ta e="T654" id="Seg_10425" s="T653">np.h:O</ta>
            <ta e="T662" id="Seg_10426" s="T661">np.h:O</ta>
            <ta e="T663" id="Seg_10427" s="T662">v:pred 0.3:S</ta>
            <ta e="T665" id="Seg_10428" s="T664">pro.h:O</ta>
            <ta e="T666" id="Seg_10429" s="T665">v:pred 0.3:S</ta>
            <ta e="T667" id="Seg_10430" s="T666">pro.h:S</ta>
            <ta e="T669" id="Seg_10431" s="T668">v:pred</ta>
            <ta e="T670" id="Seg_10432" s="T669">pro.h:S</ta>
            <ta e="T671" id="Seg_10433" s="T670">ptcl.neg</ta>
            <ta e="T672" id="Seg_10434" s="T671">v:pred</ta>
            <ta e="T674" id="Seg_10435" s="T673">v:pred 0.2.h:S</ta>
            <ta e="T676" id="Seg_10436" s="T675">v:pred 0.1.h:S</ta>
            <ta e="T677" id="Seg_10437" s="T676">v:pred 0.1.h:S</ta>
            <ta e="T680" id="Seg_10438" s="T679">v:pred 0.1.h:S</ta>
            <ta e="T688" id="Seg_10439" s="T687">ptcl.neg</ta>
            <ta e="T689" id="Seg_10440" s="T688">v:pred 0.2.h:S</ta>
            <ta e="T692" id="Seg_10441" s="T691">v:pred</ta>
            <ta e="T698" id="Seg_10442" s="T697">v:pred 0.2.h:S</ta>
            <ta e="T707" id="Seg_10443" s="T706">v:pred 0.3.h:S</ta>
            <ta e="T708" id="Seg_10444" s="T707">np:O</ta>
            <ta e="T709" id="Seg_10445" s="T708">v:pred 0.3.h:S</ta>
            <ta e="T727" id="Seg_10446" s="T726">pro.h:S</ta>
            <ta e="T729" id="Seg_10447" s="T728">v:pred</ta>
            <ta e="T732" id="Seg_10448" s="T731">pro.h:S</ta>
            <ta e="T733" id="Seg_10449" s="T732">v:pred</ta>
            <ta e="T736" id="Seg_10450" s="T735">np.h:S</ta>
            <ta e="T737" id="Seg_10451" s="T736">v:pred</ta>
            <ta e="T740" id="Seg_10452" s="T739">np.h:S</ta>
            <ta e="T741" id="Seg_10453" s="T740">v:pred</ta>
            <ta e="T744" id="Seg_10454" s="T743">np.h:S</ta>
            <ta e="T745" id="Seg_10455" s="T744">v:pred</ta>
            <ta e="T749" id="Seg_10456" s="T748">np.h:S</ta>
            <ta e="T750" id="Seg_10457" s="T749">v:pred</ta>
            <ta e="T753" id="Seg_10458" s="T752">np.h:S</ta>
            <ta e="T754" id="Seg_10459" s="T753">v:pred</ta>
            <ta e="T755" id="Seg_10460" s="T754">pro.h:O</ta>
            <ta e="T756" id="Seg_10461" s="T755">s:purp</ta>
            <ta e="T760" id="Seg_10462" s="T759">adj:pred</ta>
            <ta e="T761" id="Seg_10463" s="T760">v:pred 0.3:S</ta>
            <ta e="T763" id="Seg_10464" s="T762">np:S</ta>
            <ta e="T765" id="Seg_10465" s="T764">v:pred</ta>
            <ta e="T768" id="Seg_10466" s="T767">v:pred 0.2.h:S</ta>
            <ta e="T774" id="Seg_10467" s="T773">adj:pred</ta>
            <ta e="T775" id="Seg_10468" s="T774">cop 0.3:S</ta>
            <ta e="T777" id="Seg_10469" s="T776">np.h:S</ta>
            <ta e="T778" id="Seg_10470" s="T777">v:pred</ta>
            <ta e="T780" id="Seg_10471" s="T779">np:O</ta>
            <ta e="T781" id="Seg_10472" s="T780">v:pred 0.3.h:S</ta>
            <ta e="T784" id="Seg_10473" s="T783">v:pred 0.3:S</ta>
            <ta e="T787" id="Seg_10474" s="T786">ptcl.neg</ta>
            <ta e="T788" id="Seg_10475" s="T787">v:pred 0.3:S</ta>
            <ta e="T797" id="Seg_10476" s="T796">v:pred 0.3.h:S</ta>
            <ta e="T800" id="Seg_10477" s="T799">ptcl:pred</ta>
            <ta e="T808" id="Seg_10478" s="T807">np:S</ta>
            <ta e="T810" id="Seg_10479" s="T809">v:pred</ta>
            <ta e="T817" id="Seg_10480" s="T816">np:S</ta>
            <ta e="T819" id="Seg_10481" s="T818">v:pred</ta>
            <ta e="T820" id="Seg_10482" s="T819">v:pred 0.1.h:S</ta>
            <ta e="T824" id="Seg_10483" s="T823">ptcl:pred</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T17" id="Seg_10484" s="T16">TURK:disc</ta>
            <ta e="T20" id="Seg_10485" s="T19">RUS:mod</ta>
            <ta e="T29" id="Seg_10486" s="T28">RUS:core</ta>
            <ta e="T32" id="Seg_10487" s="T31">RUS:core</ta>
            <ta e="T39" id="Seg_10488" s="T38">RUS:cult</ta>
            <ta e="T40" id="Seg_10489" s="T39">TURK:disc</ta>
            <ta e="T68" id="Seg_10490" s="T67">RUS:gram</ta>
            <ta e="T69" id="Seg_10491" s="T68">TURK:disc</ta>
            <ta e="T92" id="Seg_10492" s="T91">TURK:disc</ta>
            <ta e="T99" id="Seg_10493" s="T98">TAT:cult</ta>
            <ta e="T101" id="Seg_10494" s="T100">RUS:mod</ta>
            <ta e="T104" id="Seg_10495" s="T103">TURK:disc</ta>
            <ta e="T108" id="Seg_10496" s="T107">TAT:cult</ta>
            <ta e="T115" id="Seg_10497" s="T114">TAT:cult</ta>
            <ta e="T119" id="Seg_10498" s="T118">TURK:disc</ta>
            <ta e="T120" id="Seg_10499" s="T119">TAT:cult</ta>
            <ta e="T122" id="Seg_10500" s="T121">TURK:disc</ta>
            <ta e="T127" id="Seg_10501" s="T126">TURK:gram(INDEF)</ta>
            <ta e="T130" id="Seg_10502" s="T129">RUS:gram</ta>
            <ta e="T132" id="Seg_10503" s="T131">TURK:disc</ta>
            <ta e="T134" id="Seg_10504" s="T133">RUS:mod</ta>
            <ta e="T138" id="Seg_10505" s="T137">RUS:mod</ta>
            <ta e="T144" id="Seg_10506" s="T143">TURK:core</ta>
            <ta e="T147" id="Seg_10507" s="T146">TURK:core</ta>
            <ta e="T149" id="Seg_10508" s="T148">%TURK:core</ta>
            <ta e="T151" id="Seg_10509" s="T150">%TURK:core</ta>
            <ta e="T156" id="Seg_10510" s="T154">RUS:cult</ta>
            <ta e="T197" id="Seg_10511" s="T196">TURK:cult</ta>
            <ta e="T200" id="Seg_10512" s="T199">TURK:disc</ta>
            <ta e="T204" id="Seg_10513" s="T203">RUS:cult</ta>
            <ta e="T207" id="Seg_10514" s="T206">RUS:gram</ta>
            <ta e="T211" id="Seg_10515" s="T210">RUS:cult</ta>
            <ta e="T213" id="Seg_10516" s="T212">RUS:cult</ta>
            <ta e="T229" id="Seg_10517" s="T228">TURK:disc</ta>
            <ta e="T234" id="Seg_10518" s="T233">%TURK:core</ta>
            <ta e="T243" id="Seg_10519" s="T242">TURK:core</ta>
            <ta e="T250" id="Seg_10520" s="T249">TURK:core</ta>
            <ta e="T251" id="Seg_10521" s="T250">%TURK:core</ta>
            <ta e="T256" id="Seg_10522" s="T255">TURK:disc</ta>
            <ta e="T257" id="Seg_10523" s="T256">RUS:mod</ta>
            <ta e="T260" id="Seg_10524" s="T259">RUS:gram</ta>
            <ta e="T263" id="Seg_10525" s="T262">TURK:disc</ta>
            <ta e="T264" id="Seg_10526" s="T263">RUS:mod</ta>
            <ta e="T274" id="Seg_10527" s="T273">RUS:cult</ta>
            <ta e="T276" id="Seg_10528" s="T275">RUS:cult</ta>
            <ta e="T288" id="Seg_10529" s="T287">RUS:cult</ta>
            <ta e="T290" id="Seg_10530" s="T289">TAT:cult</ta>
            <ta e="T298" id="Seg_10531" s="T297">TURK:disc</ta>
            <ta e="T303" id="Seg_10532" s="T302">TURK:disc</ta>
            <ta e="T313" id="Seg_10533" s="T312">TURK:disc</ta>
            <ta e="T325" id="Seg_10534" s="T324">RUS:cult</ta>
            <ta e="T328" id="Seg_10535" s="T327">RUS:cult</ta>
            <ta e="T340" id="Seg_10536" s="T339">TURK:disc</ta>
            <ta e="T345" id="Seg_10537" s="T344">TURK:disc</ta>
            <ta e="T349" id="Seg_10538" s="T348">TURK:disc</ta>
            <ta e="T355" id="Seg_10539" s="T354">RUS:gram</ta>
            <ta e="T359" id="Seg_10540" s="T358">RUS:gram</ta>
            <ta e="T368" id="Seg_10541" s="T367">TURK:disc</ta>
            <ta e="T377" id="Seg_10542" s="T376">TURK:gram(INDEF)</ta>
            <ta e="T390" id="Seg_10543" s="T389">RUS:gram</ta>
            <ta e="T393" id="Seg_10544" s="T392">RUS:gram</ta>
            <ta e="T396" id="Seg_10545" s="T395">RUS:mod</ta>
            <ta e="T397" id="Seg_10546" s="T396">TURK:cult</ta>
            <ta e="T400" id="Seg_10547" s="T399">RUS:mod</ta>
            <ta e="T407" id="Seg_10548" s="T406">RUS:disc</ta>
            <ta e="T410" id="Seg_10549" s="T409">TURK:cult</ta>
            <ta e="T414" id="Seg_10550" s="T413">%TURK:core</ta>
            <ta e="T417" id="Seg_10551" s="T416">RUS:mod</ta>
            <ta e="T419" id="Seg_10552" s="T418">RUS:gram</ta>
            <ta e="T426" id="Seg_10553" s="T425">TURK:gram(INDEF)</ta>
            <ta e="T429" id="Seg_10554" s="T428">RUS:gram</ta>
            <ta e="T430" id="Seg_10555" s="T429">TURK:disc</ta>
            <ta e="T434" id="Seg_10556" s="T433">RUS:gram</ta>
            <ta e="T441" id="Seg_10557" s="T440">RUS:gram</ta>
            <ta e="T457" id="Seg_10558" s="T456">TURK:disc</ta>
            <ta e="T467" id="Seg_10559" s="T466">TAT:cult</ta>
            <ta e="T471" id="Seg_10560" s="T470">TURK:disc</ta>
            <ta e="T480" id="Seg_10561" s="T479">TURK:disc</ta>
            <ta e="T490" id="Seg_10562" s="T489">TURK:disc</ta>
            <ta e="T500" id="Seg_10563" s="T499">TURK:disc</ta>
            <ta e="T514" id="Seg_10564" s="T513">%TURK:core</ta>
            <ta e="T532" id="Seg_10565" s="T531">RUS:mod</ta>
            <ta e="T535" id="Seg_10566" s="T534">RUS:gram</ta>
            <ta e="T538" id="Seg_10567" s="T537">RUS:gram</ta>
            <ta e="T548" id="Seg_10568" s="T547">TURK:core</ta>
            <ta e="T554" id="Seg_10569" s="T553">TURK:disc</ta>
            <ta e="T575" id="Seg_10570" s="T574">TURK:disc</ta>
            <ta e="T589" id="Seg_10571" s="T588">TURK:disc</ta>
            <ta e="T594" id="Seg_10572" s="T593">RUS:gram</ta>
            <ta e="T601" id="Seg_10573" s="T600">TURK:core</ta>
            <ta e="T604" id="Seg_10574" s="T603">TURK:cult</ta>
            <ta e="T608" id="Seg_10575" s="T607">TURK:core</ta>
            <ta e="T609" id="Seg_10576" s="T608">TURK:cult</ta>
            <ta e="T614" id="Seg_10577" s="T613">RUS:gram</ta>
            <ta e="T615" id="Seg_10578" s="T614">TAT:cult</ta>
            <ta e="T617" id="Seg_10579" s="T616">TURK:core</ta>
            <ta e="T625" id="Seg_10580" s="T624">RUS:gram</ta>
            <ta e="T635" id="Seg_10581" s="T634">RUS:gram</ta>
            <ta e="T637" id="Seg_10582" s="T636">RUS:gram</ta>
            <ta e="T643" id="Seg_10583" s="T642">TURK:disc</ta>
            <ta e="T645" id="Seg_10584" s="T644">TURK:disc</ta>
            <ta e="T651" id="Seg_10585" s="T650">TURK:disc</ta>
            <ta e="T661" id="Seg_10586" s="T660">TURK:disc</ta>
            <ta e="T684" id="Seg_10587" s="T683">TURK:disc</ta>
            <ta e="T691" id="Seg_10588" s="T690">TURK:disc</ta>
            <ta e="T697" id="Seg_10589" s="T696">TURK:disc</ta>
            <ta e="T702" id="Seg_10590" s="T701">RUS:cult</ta>
            <ta e="T703" id="Seg_10591" s="T702">RUS:cult</ta>
            <ta e="T704" id="Seg_10592" s="T703">TURK:disc</ta>
            <ta e="T710" id="Seg_10593" s="T709">TURK:disc</ta>
            <ta e="T728" id="Seg_10594" s="T727">TURK:disc</ta>
            <ta e="T746" id="Seg_10595" s="T745">RUS:gram</ta>
            <ta e="T756" id="Seg_10596" s="T755">RUS:calq</ta>
            <ta e="T757" id="Seg_10597" s="T756">TURK:disc</ta>
            <ta e="T759" id="Seg_10598" s="T758">TURK:disc</ta>
            <ta e="T764" id="Seg_10599" s="T763">TURK:disc</ta>
            <ta e="T769" id="Seg_10600" s="T768">TURK:disc</ta>
            <ta e="T773" id="Seg_10601" s="T772">TURK:disc</ta>
            <ta e="T779" id="Seg_10602" s="T778">TURK:disc</ta>
            <ta e="T795" id="Seg_10603" s="T794">TURK:disc</ta>
            <ta e="T799" id="Seg_10604" s="T798">TURK:disc</ta>
            <ta e="T800" id="Seg_10605" s="T799">RUS:mod</ta>
            <ta e="T818" id="Seg_10606" s="T817">TURK:disc</ta>
            <ta e="T821" id="Seg_10607" s="T820">RUS:mod</ta>
            <ta e="T824" id="Seg_10608" s="T823">RUS:mod</ta>
            <ta e="T826" id="Seg_10609" s="T825">TURK:disc</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T12" id="Seg_10610" s="T4">У него/нее глаза некрасивые, один смотрит сюда, другой туда.</ta>
            <ta e="T14" id="Seg_10611" s="T12">Его глаза (?).</ta>
            <ta e="T20" id="Seg_10612" s="T14">У меня рот закрывается, спать хочу.</ta>
            <ta e="T25" id="Seg_10613" s="T20">Мы вдвоем вышли на дорогу.</ta>
            <ta e="T32" id="Seg_10614" s="T25">Он пошел налево, я пошла направо.</ta>
            <ta e="T38" id="Seg_10615" s="T32">Как его/ее зовут, я не знаю.</ta>
            <ta e="T43" id="Seg_10616" s="T38">Пчелы приносят сладкие вещи.</ta>
            <ta e="T47" id="Seg_10617" s="T43">И кладут в чашки.</ta>
            <ta e="T50" id="Seg_10618" s="T47">Они очень тяжело работают.</ta>
            <ta e="T58" id="Seg_10619" s="T50">Они в лесу не живут, они в воде не живут</ta>
            <ta e="T61" id="Seg_10620" s="T58">Очень большая гора.</ta>
            <ta e="T64" id="Seg_10621" s="T61">Как туда дойти?</ta>
            <ta e="T66" id="Seg_10622" s="T64">Я не знаю.</ta>
            <ta e="T74" id="Seg_10623" s="T66">Если посмотришь [на нее], шапка упадет.</ta>
            <ta e="T77" id="Seg_10624" s="T74">Они пошли копать землю.</ta>
            <ta e="T80" id="Seg_10625" s="T77">Дорога…</ta>
            <ta e="T84" id="Seg_10626" s="T80">Они пошли копать землю.</ta>
            <ta e="T86" id="Seg_10627" s="T84">Чинить дорогу.</ta>
            <ta e="T91" id="Seg_10628" s="T86">Куда они пошли, далеко?</ta>
            <ta e="T94" id="Seg_10629" s="T91">Нет, недалеко.</ta>
            <ta e="T96" id="Seg_10630" s="T94">Они поехали на лошадях.</ta>
            <ta e="T100" id="Seg_10631" s="T96">Недалеко остановились в доме.</ta>
            <ta e="T105" id="Seg_10632" s="T100">Даже не спросили, просто стояли.</ta>
            <ta e="T112" id="Seg_10633" s="T105">Иди из моего дома, и не стой здесь!</ta>
            <ta e="T114" id="Seg_10634" s="T112">Они не ушли.</ta>
            <ta e="T116" id="Seg_10635" s="T114">Они спрятались в доме.</ta>
            <ta e="T119" id="Seg_10636" s="T116">И шумят.</ta>
            <ta e="T123" id="Seg_10637" s="T119">Залезли на крышу дома. [?]</ta>
            <ta e="T126" id="Seg_10638" s="T123">Нет (?).</ta>
            <ta e="T132" id="Seg_10639" s="T126">Куда-то все ушли или умерли.</ta>
            <ta e="T136" id="Seg_10640" s="T132">Вы меня тоже бьете камнями.</ta>
            <ta e="T142" id="Seg_10641" s="T136">Ты тоже бросишь в меня камнем. [?]</ta>
            <ta e="T146" id="Seg_10642" s="T142">Люди хорошие, мы не врем.</ta>
            <ta e="T149" id="Seg_10643" s="T146">Мы говорим хорошо.</ta>
            <ta e="T153" id="Seg_10644" s="T149">Я говорю правду, я не вру.</ta>
            <ta e="T161" id="Seg_10645" s="T153">Я собрала красных червей, водой [их] помыла.</ta>
            <ta e="T163" id="Seg_10646" s="T161">Положила их в миску.</ta>
            <ta e="T167" id="Seg_10647" s="T163">Потом их поставила в теплое место. [?]</ta>
            <ta e="T170" id="Seg_10648" s="T167">Они там стали [как] суп.</ta>
            <ta e="T173" id="Seg_10649" s="T170">Потом я натерла руки.</ta>
            <ta e="T180" id="Seg_10650" s="T173">У меня руки очень болели, теперь не болят.</ta>
            <ta e="T183" id="Seg_10651" s="T180">Смотри, Бог…</ta>
            <ta e="T187" id="Seg_10652" s="T183">Смотри, Бога не забывай.</ta>
            <ta e="T191" id="Seg_10653" s="T187">Погоди, как это… </ta>
            <ta e="T193" id="Seg_10654" s="T191">Иди к черту!</ta>
            <ta e="T196" id="Seg_10655" s="T193">Черта рога!</ta>
            <ta e="T198" id="Seg_10656" s="T196">Коровий рог.</ta>
            <ta e="T203" id="Seg_10657" s="T198">Он тебя проткнет.</ta>
            <ta e="T210" id="Seg_10658" s="T203">Подмети сени, а очень много грязи.</ta>
            <ta e="T212" id="Seg_10659" s="T210">Подмети крыльцо!</ta>
            <ta e="T216" id="Seg_10660" s="T212">Я взяли муку в амбар, положила.</ta>
            <ta e="T222" id="Seg_10661" s="T216">Я его убил, у меня силы много.</ta>
            <ta e="T226" id="Seg_10662" s="T222">Я очень сильный.</ta>
            <ta e="T231" id="Seg_10663" s="T226">У этого человека ума нет.</ta>
            <ta e="T236" id="Seg_10664" s="T231">Язык есть, говорить не может.</ta>
            <ta e="T240" id="Seg_10665" s="T236">Уши есть, не слышит.</ta>
            <ta e="T242" id="Seg_10666" s="T240">Не крутись!</ta>
            <ta e="T244" id="Seg_10667" s="T242">Сиди хорошо!</ta>
            <ta e="T246" id="Seg_10668" s="T244">Что ты крутишься?</ta>
            <ta e="T251" id="Seg_10669" s="T246">Я слышу [= понимаю] свой язык, я хорошо говорю.</ta>
            <ta e="T256" id="Seg_10670" s="T251">Свинья пришла и хрюкает.</ta>
            <ta e="T263" id="Seg_10671" s="T256">Надо ее покормить, а то она голодная.</ta>
            <ta e="T273" id="Seg_10672" s="T263">Надо топор поточить, [чтобы] дрова рубить.</ta>
            <ta e="T275" id="Seg_10673" s="T273">Чтобы печь топить.</ta>
            <ta e="T282" id="Seg_10674" s="T275">Пилу принесли, не знают, что с ней делать.</ta>
            <ta e="T286" id="Seg_10675" s="T282">Варлам придет, починит ее.</ta>
            <ta e="T289" id="Seg_10676" s="T286">Железную печь принесли.</ta>
            <ta e="T291" id="Seg_10677" s="T289">В дом поставили.</ta>
            <ta e="T294" id="Seg_10678" s="T291">На берестяной (поддон?)</ta>
            <ta e="T295" id="Seg_10679" s="T294">(?) поддон, он загорелся.</ta>
            <ta e="T301" id="Seg_10680" s="T299">Дым пошел.</ta>
            <ta e="T305" id="Seg_10681" s="T301">Они его весь выпустили.</ta>
            <ta e="T308" id="Seg_10682" s="T305">Очень много дыма.</ta>
            <ta e="T312" id="Seg_10683" s="T308">Варлам придет и починит.</ta>
            <ta e="T316" id="Seg_10684" s="T312">Дым глаза ест.</ta>
            <ta e="T321" id="Seg_10685" s="T316">Потом Варлам пришел, принес камень.</ta>
            <ta e="T323" id="Seg_10686" s="T321">Положил на землю.</ta>
            <ta e="T326" id="Seg_10687" s="T323">И печь поставил [на камень].</ta>
            <ta e="T333" id="Seg_10688" s="T326">Потом они затопили печь, сидят и греются.</ta>
            <ta e="T337" id="Seg_10689" s="T333">(…) чтобы не ошибиться.</ta>
            <ta e="T343" id="Seg_10690" s="T338">Моя шуба лежит на земле.</ta>
            <ta e="T346" id="Seg_10691" s="T343">Ногами вытертая.</ta>
            <ta e="T361" id="Seg_10692" s="T346">Там грязь, я ее взяла, в воду отнесла, чтобы она была влажная.</ta>
            <ta e="T363" id="Seg_10693" s="T361">Потом я ее выбила.</ta>
            <ta e="T364" id="Seg_10694" s="T363">Помыла</ta>
            <ta e="T366" id="Seg_10695" s="T364">Потом повесила.</ta>
            <ta e="T370" id="Seg_10696" s="T366">С нее вода течет.</ta>
            <ta e="T372" id="Seg_10697" s="T370">Река течет.</ta>
            <ta e="T376" id="Seg_10698" s="T372">И человек бежит за водой.</ta>
            <ta e="T379" id="Seg_10699" s="T376">Какие-то люди ходят.</ta>
            <ta e="T384" id="Seg_10700" s="T379">Я их не знаю.</ta>
            <ta e="T389" id="Seg_10701" s="T384">Почему они здесь ходят?</ta>
            <ta e="T395" id="Seg_10702" s="T389">И этот парень и девушка идут сюда.</ta>
            <ta e="T399" id="Seg_10703" s="T395">Мне надо молоко перелить в туес.</ta>
            <ta e="T402" id="Seg_10704" s="T399">Пусть скиснет.</ta>
            <ta e="T406" id="Seg_10705" s="T402">Сейчас мы уйдем.</ta>
            <ta e="T410" id="Seg_10706" s="T406">Ну, идите с богом.</ta>
            <ta e="T414" id="Seg_10707" s="T410">Мы очень много поговорили.</ta>
            <ta e="T418" id="Seg_10708" s="T414">Теперь идти надо домой.</ta>
            <ta e="T421" id="Seg_10709" s="T418">И спать ложиться.</ta>
            <ta e="T425" id="Seg_10710" s="T421">Не хвастайтся!</ta>
            <ta e="T431" id="Seg_10711" s="T425">Ты ничего не сделал, а хвастался.</ta>
            <ta e="T437" id="Seg_10712" s="T431">Спать лечь, а завтра ум придет [=утро вечера мудренее].</ta>
            <ta e="T444" id="Seg_10713" s="T437">К окну подошел человек, а я схватила камень.</ta>
            <ta e="T448" id="Seg_10714" s="T446">Запри (?).</ta>
            <ta e="T454" id="Seg_10715" s="T448">Я схватила камень и убила этого человека.</ta>
            <ta e="T462" id="Seg_10716" s="T454">Человек человека ударил ножом, и тот умер.</ta>
            <ta e="T475" id="Seg_10717" s="T462">В Агинском одна женщина мужа топором зарубила, голову ему отрубила.</ta>
            <ta e="T480" id="Seg_10718" s="T475">Ее муж один заснул.</ta>
            <ta e="T483" id="Seg_10719" s="T480">Я очень испугалась.</ta>
            <ta e="T488" id="Seg_10720" s="T483">Как она мне голову не отрубила.</ta>
            <ta e="T496" id="Seg_10721" s="T488">Она была беременная, и двое детей остались.</ta>
            <ta e="T504" id="Seg_10722" s="T496">Пять лет прошло.</ta>
            <ta e="T507" id="Seg_10723" s="T504">Тебя вчера не было.</ta>
            <ta e="T512" id="Seg_10724" s="T507">Сегодня ты пришел слушать.</ta>
            <ta e="T514" id="Seg_10725" s="T512">Что мы говорим?</ta>
            <ta e="T521" id="Seg_10726" s="T514">Я сегодня буду Богу молиться.</ta>
            <ta e="T524" id="Seg_10727" s="T521">Боже, приди ко мне!</ta>
            <ta e="T528" id="Seg_10728" s="T524">Послушай, как я молюсь.</ta>
            <ta e="T531" id="Seg_10729" s="T528">Посмотри на мое сердце!</ta>
            <ta e="T534" id="Seg_10730" s="T531">Может быть, оно чистое.</ta>
            <ta e="T540" id="Seg_10731" s="T534">А если оно не чистое, омой его кровью!</ta>
            <ta e="T543" id="Seg_10732" s="T540">Ты висел на кресте.</ta>
            <ta e="T545" id="Seg_10733" s="T543">Кровь текла.</ta>
            <ta e="T547" id="Seg_10734" s="T545">Помой меня!</ta>
            <ta e="T549" id="Seg_10735" s="T547">Хорошо помой!</ta>
            <ta e="T553" id="Seg_10736" s="T549">У тебя, Боже, много силы.</ta>
            <ta e="T558" id="Seg_10737" s="T553">Ты всех людей любишь.</ta>
            <ta e="T561" id="Seg_10738" s="T558">Ты даешь хлеб.</ta>
            <ta e="T566" id="Seg_10739" s="T562">У тебя, Боже, много силы.</ta>
            <ta e="T571" id="Seg_10740" s="T566">Ты заботишься о людях.</ta>
            <ta e="T574" id="Seg_10741" s="T571">Ты даешь им хлеб.</ta>
            <ta e="T577" id="Seg_10742" s="T574">Рубахи даешь.</ta>
            <ta e="T579" id="Seg_10743" s="T577">Воду даешь.</ta>
            <ta e="T582" id="Seg_10744" s="T579">И меня пожалей!</ta>
            <ta e="T587" id="Seg_10745" s="T582">Чего-то (никак)… </ta>
            <ta e="T591" id="Seg_10746" s="T587">Ты посылаешь тепло.</ta>
            <ta e="T596" id="Seg_10747" s="T591">Ты даешь дождь, чтобы хлеб рос.</ta>
            <ta e="T599" id="Seg_10748" s="T596">Чтобы хлеб (рос/рожь)…</ta>
            <ta e="T609" id="Seg_10749" s="T599">С Богом хорошо, куда ты ни пойдешь, очень хорошо с Богом.</ta>
            <ta e="T611" id="Seg_10750" s="T609">Ветер стих (?)</ta>
            <ta e="T616" id="Seg_10751" s="T611">Я (взяла?) и принесла домой.</ta>
            <ta e="T617" id="Seg_10752" s="T616">Хорошо.</ta>
            <ta e="T621" id="Seg_10753" s="T617">Дождь идет, хлеб будет расти.</ta>
            <ta e="T624" id="Seg_10754" s="T621">Я ему/ей помог.</ta>
            <ta e="T629" id="Seg_10755" s="T624">А он(а) мне не помог(ла).</ta>
            <ta e="T641" id="Seg_10756" s="T629">Не дает мне работать, я бы работала, а он(а) вечно мешает мне.</ta>
            <ta e="T643" id="Seg_10757" s="T641">Сердитый.</ta>
            <ta e="T646" id="Seg_10758" s="T643">Ногами топает.</ta>
            <ta e="T649" id="Seg_10759" s="T646">Очень сердитая собака.</ta>
            <ta e="T654" id="Seg_10760" s="T649">Кусает людей.</ta>
            <ta e="T656" id="Seg_10761" s="T654">Людей кусает. </ta>
            <ta e="T659" id="Seg_10762" s="T656">Очень сердитая собака.</ta>
            <ta e="T663" id="Seg_10763" s="T659">Всех людей кусает.</ta>
            <ta e="T666" id="Seg_10764" s="T663">И меня укусила.</ta>
            <ta e="T674" id="Seg_10765" s="T666">Я вас ждала, вы не приходили, вас долго не было.</ta>
            <ta e="T677" id="Seg_10766" s="T674">Я взяла [воду] из миски, помылась.</ta>
            <ta e="T680" id="Seg_10767" s="T677">И назад поставила.</ta>
            <ta e="T684" id="Seg_10768" s="T680">(?) сегодня.</ta>
            <ta e="T686" id="Seg_10769" s="T684">(…) никак.</ta>
            <ta e="T695" id="Seg_10770" s="T686">В воду не упади, двигайся руками и ногами. [?]</ta>
            <ta e="T701" id="Seg_10771" s="T695">В воде ты плывешь с помощью рук и ног.</ta>
            <ta e="T707" id="Seg_10772" s="T701">На лодце плывет по воде.</ta>
            <ta e="T710" id="Seg_10773" s="T707">Рыбу ловит.</ta>
            <ta e="T712" id="Seg_10774" s="T710">Мы втроем (?).</ta>
            <ta e="T718" id="Seg_10775" s="T712">Один тебе, один мне, один ему.</ta>
            <ta e="T724" id="Seg_10776" s="T718">Две части, три части, четыре части.</ta>
            <ta e="T729" id="Seg_10777" s="T724">Так они все съели.</ta>
            <ta e="T733" id="Seg_10778" s="T729">Я сидел на одной лошади.</ta>
            <ta e="T737" id="Seg_10779" s="T733">На другой лошади сидела моя жена.</ta>
            <ta e="T741" id="Seg_10780" s="T737">На третьей лошади сидел мой сын.</ta>
            <ta e="T745" id="Seg_10781" s="T741">На четвертой лошади сидела моя дочь.</ta>
            <ta e="T750" id="Seg_10782" s="T745">А на пятой лошади сидела бабушка.</ta>
            <ta e="T757" id="Seg_10783" s="T751">Этот парень пришел, чтобы нас сфотографировать.</ta>
            <ta e="T761" id="Seg_10784" s="T757">Скоро станет холодно.</ta>
            <ta e="T765" id="Seg_10785" s="T761">Снег пойдет.</ta>
            <ta e="T767" id="Seg_10786" s="T765">Очень холодно.</ta>
            <ta e="T769" id="Seg_10787" s="T767">Ты замерзнешь.</ta>
            <ta e="T775" id="Seg_10788" s="T769">Скоро станет тепло.</ta>
            <ta e="T779" id="Seg_10789" s="T775">Тогда люди (станут?) пахать.</ta>
            <ta e="T781" id="Seg_10790" s="T779">(Будут?) сеять пшеницу.</ta>
            <ta e="T786" id="Seg_10791" s="T781">Очень далеко улетел наверх. [?]</ta>
            <ta e="T788" id="Seg_10792" s="T786">Не видно.</ta>
            <ta e="T790" id="Seg_10793" s="T788">И…</ta>
            <ta e="T797" id="Seg_10794" s="T790">С лошади на землю упал.</ta>
            <ta e="T802" id="Seg_10795" s="T797">Надо очень высоко идти.</ta>
            <ta e="T805" id="Seg_10796" s="T802">Больше не знаю. </ta>
            <ta e="T810" id="Seg_10797" s="T806">Очень высоко Бог живет. [?]</ta>
            <ta e="T816" id="Seg_10798" s="T810">На горе, на большой горе.</ta>
            <ta e="T819" id="Seg_10799" s="T816">Голова у меня чешется.</ta>
            <ta e="T823" id="Seg_10800" s="T819">Я чешусь, наверное вшей много.</ta>
            <ta e="T828" id="Seg_10801" s="T823">Надо их всех убить.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T12" id="Seg_10802" s="T4">Seine/Ihre Augen sind nicht schön, eins schaut her, das Andere dort hin.</ta>
            <ta e="T14" id="Seg_10803" s="T12">Seine Auge (?).</ta>
            <ta e="T20" id="Seg_10804" s="T14">Mein Mund schließt sich, ich möchte schlafen.</ta>
            <ta e="T25" id="Seg_10805" s="T20">Zwei von uns kamen auf der Straße.</ta>
            <ta e="T32" id="Seg_10806" s="T25">Er/Sie ging nach links, ich ging nach rechts.</ta>
            <ta e="T38" id="Seg_10807" s="T32">Wie ist sein/ihr Name, ich weis nicht.</ta>
            <ta e="T43" id="Seg_10808" s="T38">Die Bienen bringen Süßes.</ta>
            <ta e="T47" id="Seg_10809" s="T43">Und sie tun es in Becher.</ta>
            <ta e="T50" id="Seg_10810" s="T47">Sie arbeiten sehr hart.</ta>
            <ta e="T58" id="Seg_10811" s="T50">Sie leben nicht in der Taiga, sie leben nicht im Wasser.</ta>
            <ta e="T61" id="Seg_10812" s="T58">Ein sehr großer Berg.</ta>
            <ta e="T64" id="Seg_10813" s="T61">Wie dort hingehen?</ta>
            <ta e="T66" id="Seg_10814" s="T64">Ich weis nicht.</ta>
            <ta e="T74" id="Seg_10815" s="T66">Wenn du [ihn] anschaust, wird dein Hut herunterfallen.</ta>
            <ta e="T77" id="Seg_10816" s="T74">Sie gingen, um die Erde zu graben.</ta>
            <ta e="T80" id="Seg_10817" s="T77">Die Straße…</ta>
            <ta e="T84" id="Seg_10818" s="T80">Sie gingen, um die Erde zu graben.</ta>
            <ta e="T86" id="Seg_10819" s="T84">Um die Straße zu reparieren.</ta>
            <ta e="T91" id="Seg_10820" s="T86">Wo gingen sie hin, weit weg?</ta>
            <ta e="T94" id="Seg_10821" s="T91">Nein, nicht weit.</ta>
            <ta e="T96" id="Seg_10822" s="T94">Sie kamen mit Pferden.</ta>
            <ta e="T100" id="Seg_10823" s="T96">Nicht weit weg, wohnten sie in einem Haus.</ta>
            <ta e="T105" id="Seg_10824" s="T100">Sie fragten noch nicht einmal, sie standen nur.</ta>
            <ta e="T112" id="Seg_10825" s="T105">Geh raus aus meinem Haus, und steh nicht hier herum!</ta>
            <ta e="T114" id="Seg_10826" s="T112">Sie gingen nicht.</ta>
            <ta e="T116" id="Seg_10827" s="T114">Sie versteckten sich im Haus.</ta>
            <ta e="T119" id="Seg_10828" s="T116">Und alle machen krach.</ta>
            <ta e="T123" id="Seg_10829" s="T119">Sie sprangen kletterten aus das Haus. [?]</ta>
            <ta e="T126" id="Seg_10830" s="T123">Es sind keine (?).</ta>
            <ta e="T132" id="Seg_10831" s="T126">Sie gingen alle irgendwo hin oder starben.</ta>
            <ta e="T136" id="Seg_10832" s="T132">Ihr trifft mich auch mit Steinen.</ta>
            <ta e="T142" id="Seg_10833" s="T136">Du wirst mir auch einen Stein zukommen lassen. [?]</ta>
            <ta e="T146" id="Seg_10834" s="T142">Die Leute sind gut, wir lügen nicht.</ta>
            <ta e="T149" id="Seg_10835" s="T146">Wir sprechen gut.</ta>
            <ta e="T153" id="Seg_10836" s="T149">Ich sage die Wahrheit, ich lüge nicht.</ta>
            <ta e="T161" id="Seg_10837" s="T153">Ich sammelte rote Maden, wusch [sie] mit Wasser.</ta>
            <ta e="T163" id="Seg_10838" s="T161">Ich tat sie in einen Becher.</ta>
            <ta e="T167" id="Seg_10839" s="T163">Dann setzte ich sie in einem warmen Platz. [?]</ta>
            <ta e="T170" id="Seg_10840" s="T167">Dort wurden sie [wie] Suppe.</ta>
            <ta e="T173" id="Seg_10841" s="T170">Dann rieb ich mir die Hand.</ta>
            <ta e="T180" id="Seg_10842" s="T173">Meine Hand tat mir sie weh, jetzt tut sie mir nicht weh.</ta>
            <ta e="T183" id="Seg_10843" s="T180">Schau, Gott…</ta>
            <ta e="T187" id="Seg_10844" s="T183">Schau, vergiß nicht Gott.</ta>
            <ta e="T191" id="Seg_10845" s="T187">Warte, wie geht es…</ta>
            <ta e="T193" id="Seg_10846" s="T191">Geh zum Teufel!</ta>
            <ta e="T196" id="Seg_10847" s="T193">Teufels Hörner!</ta>
            <ta e="T198" id="Seg_10848" s="T196">Kuhhorn.</ta>
            <ta e="T203" id="Seg_10849" s="T198">Es wird dich durchbohren.</ta>
            <ta e="T210" id="Seg_10850" s="T203">Kehr den Gang, weil er sehr schmutzig ist.</ta>
            <ta e="T212" id="Seg_10851" s="T210">Kehr die Treppe!</ta>
            <ta e="T216" id="Seg_10852" s="T212">Ich nahm Mehl zur Scheune, ich stellte es dort hin.</ta>
            <ta e="T222" id="Seg_10853" s="T216">Ich tötete es, ich habe viele Kraft.</ta>
            <ta e="T226" id="Seg_10854" s="T222">Ich bin sehr kräftig.</ta>
            <ta e="T231" id="Seg_10855" s="T226">Dieser Mann hat keinen Verstand.</ta>
            <ta e="T236" id="Seg_10856" s="T231">Er hat eine Zunge, [aber] er kann nicht reden.</ta>
            <ta e="T240" id="Seg_10857" s="T236">Er hat Ohren, [aber] er hört nicht.</ta>
            <ta e="T242" id="Seg_10858" s="T240">Dreh dich nicht um!</ta>
            <ta e="T244" id="Seg_10859" s="T242">Sitz artig!</ta>
            <ta e="T246" id="Seg_10860" s="T244">Warum drehst du dich um?</ta>
            <ta e="T251" id="Seg_10861" s="T246">Ich höre [= verstehe] meine Sprache, ich spreche gut.</ta>
            <ta e="T256" id="Seg_10862" s="T251">Das Schwein kommt und grunzt.</ta>
            <ta e="T263" id="Seg_10863" s="T256">Ich muß es füttern, sonst bleibt es hungrig.</ta>
            <ta e="T273" id="Seg_10864" s="T263">Ich muß die Axt schärfen, um Holz zu hacken.</ta>
            <ta e="T275" id="Seg_10865" s="T273">Um den Ofen zu heizen.</ta>
            <ta e="T282" id="Seg_10866" s="T275">Sie brachten eine Säge, sie wissen nicht, was damit anzufangen.</ta>
            <ta e="T286" id="Seg_10867" s="T282">Varlam wird kommen, er wird sie reparieren.</ta>
            <ta e="T289" id="Seg_10868" s="T286">Sie brachten einen Eisenofen.</ta>
            <ta e="T291" id="Seg_10869" s="T289">Sie stellten ihn ins Haus.</ta>
            <ta e="T294" id="Seg_10870" s="T291">Auf das Birkenrindenfaß.</ta>
            <ta e="T299" id="Seg_10871" s="T294">(?) das Faß, es fing Feuer.</ta>
            <ta e="T301" id="Seg_10872" s="T299">Es kam Rauch auf.</ta>
            <ta e="T305" id="Seg_10873" s="T301">Dann ließen sie alles raus.</ta>
            <ta e="T308" id="Seg_10874" s="T305">Viel Rauch.</ta>
            <ta e="T312" id="Seg_10875" s="T308">Varlam wird kommen, und es richten.</ta>
            <ta e="T316" id="Seg_10876" s="T312">Der Rauch brennt in meinen Augen.</ta>
            <ta e="T321" id="Seg_10877" s="T316">Dann kam Varlam, er brachte (einen?) Stein.</ta>
            <ta e="T323" id="Seg_10878" s="T321">Er legte ihn auf den Boden.</ta>
            <ta e="T326" id="Seg_10879" s="T323">Und setzte den Ofen [auf den Stein].</ta>
            <ta e="T333" id="Seg_10880" s="T326">Dann machten sie im Ofen Feuer, sie saßen und wärmten sich.</ta>
            <ta e="T337" id="Seg_10881" s="T333">(…) kein Fehler zu machen.</ta>
            <ta e="T343" id="Seg_10882" s="T338">Mein Pelzmantel lag auf dem Boden.</ta>
            <ta e="T346" id="Seg_10883" s="T343">Die Füße der Leute machten ihn schmutzig.</ta>
            <ta e="T361" id="Seg_10884" s="T346">Er war schmutzig, ich nahm ihn, und brachte ihn ans Wasser, damit er naß wird.</ta>
            <ta e="T363" id="Seg_10885" s="T361">Dann klopfte ich ihn [sauber].</ta>
            <ta e="T364" id="Seg_10886" s="T363">Ich wusch ihn.</ta>
            <ta e="T366" id="Seg_10887" s="T364">Dann hing ich ihn auf.</ta>
            <ta e="T370" id="Seg_10888" s="T366">Wasser fließt von ihm.</ta>
            <ta e="T372" id="Seg_10889" s="T370">Der Fluß fließt.</ta>
            <ta e="T376" id="Seg_10890" s="T372">Und ein Mann rennt um Wasser zu holen.</ta>
            <ta e="T379" id="Seg_10891" s="T376">Irgendwelche Leute laufen hier rum.</ta>
            <ta e="T384" id="Seg_10892" s="T379">Ich kenne sie nicht.</ta>
            <ta e="T389" id="Seg_10893" s="T384">Warum laufen sie hier rum?</ta>
            <ta e="T395" id="Seg_10894" s="T389">Und dieser Junge und das Mädchen kommen.</ta>
            <ta e="T399" id="Seg_10895" s="T395">Ich muß Milch ins Gefäß schütten.</ta>
            <ta e="T402" id="Seg_10896" s="T399">Laß sie sauer werden.</ta>
            <ta e="T406" id="Seg_10897" s="T402">Jetzt werden wir aufbrechen.</ta>
            <ta e="T410" id="Seg_10898" s="T406">Alo, dann gute Reise!</ta>
            <ta e="T414" id="Seg_10899" s="T410">Wir sprachen sehr viel.</ta>
            <ta e="T418" id="Seg_10900" s="T414">Nun sollte ich nach Hause gehen.</ta>
            <ta e="T421" id="Seg_10901" s="T418">Und mich zum Schlafen hinlegen.</ta>
            <ta e="T425" id="Seg_10902" s="T421">Gib nicht an!</ta>
            <ta e="T431" id="Seg_10903" s="T425">Du hast gar nichts gemacht, du warst nur am Angeben.</ta>
            <ta e="T437" id="Seg_10904" s="T431">Ich werde mich zum Schlafen hinlegen, und morgen wird der Verstand kommen [= der Morgen ist klüger als der Abend].</ta>
            <ta e="T444" id="Seg_10905" s="T437">Ein Mann kam zum Fenster, und ich schnappte einen Stein.</ta>
            <ta e="T448" id="Seg_10906" s="T446">Schließ ab (?).</ta>
            <ta e="T454" id="Seg_10907" s="T448">Ich schnappte mir einen Ast und tötete diesen Mann.</ta>
            <ta e="T462" id="Seg_10908" s="T454">Ein Mann stach einen anderen Mann mit einem Messer, und er starb.</ta>
            <ta e="T475" id="Seg_10909" s="T462">Es ist eine Frau, in Aginskoye schlug eine Frau ihren Mann mit einer Axt und schlug seinen Kopf ab.</ta>
            <ta e="T480" id="Seg_10910" s="T475">Ihr Mann schlief alleine eine.</ta>
            <ta e="T483" id="Seg_10911" s="T480">Mir wurde sehr bange.</ta>
            <ta e="T488" id="Seg_10912" s="T483">Wie hat sie meinen Kopf nicht abgeschlagen [?].</ta>
            <ta e="T496" id="Seg_10913" s="T488">Sie war schwanger, und zwei Kinder wurden auf sich gestellt.</ta>
            <ta e="T504" id="Seg_10914" s="T496">Fünf Jahre vergingen schon [seitdem].</ta>
            <ta e="T507" id="Seg_10915" s="T504">Gestern warst du nicht [da].</ta>
            <ta e="T512" id="Seg_10916" s="T507">Heute bist du zum Zuhören gekommen.</ta>
            <ta e="T514" id="Seg_10917" s="T512">Worüber sprechen (/sagen) wir?</ta>
            <ta e="T521" id="Seg_10918" s="T514">Ich werde heute zu Gott beten.</ta>
            <ta e="T524" id="Seg_10919" s="T521">Gott, komm zu mir!</ta>
            <ta e="T528" id="Seg_10920" s="T524">Hör, wie ich bete.</ta>
            <ta e="T531" id="Seg_10921" s="T528">Schau auf mein Herz!</ta>
            <ta e="T534" id="Seg_10922" s="T531">Vielleicht is es rein.</ta>
            <ta e="T540" id="Seg_10923" s="T534">Doch wenn es nicht rein ist, wasche es mit Blut!</ta>
            <ta e="T543" id="Seg_10924" s="T540">Du hingst am Holz.</ta>
            <ta e="T545" id="Seg_10925" s="T543">Blut floß.</ta>
            <ta e="T547" id="Seg_10926" s="T545">Wasch mich!</ta>
            <ta e="T549" id="Seg_10927" s="T547">Wasch mich gut!</ta>
            <ta e="T553" id="Seg_10928" s="T549">Gott, Du hast viel Macht.</ta>
            <ta e="T558" id="Seg_10929" s="T553">Du liebst alle Menschen.</ta>
            <ta e="T561" id="Seg_10930" s="T558">Du schenkst Brot.</ta>
            <ta e="T566" id="Seg_10931" s="T562">Du, Gott, hast viel Macht.</ta>
            <ta e="T571" id="Seg_10932" s="T566">Du sorgst Dich um Menschen.</ta>
            <ta e="T574" id="Seg_10933" s="T571">Du schenkst ihnen Brot.</ta>
            <ta e="T577" id="Seg_10934" s="T574">Du schenkst Hemden.</ta>
            <ta e="T579" id="Seg_10935" s="T577">Du schenkst Wasser.</ta>
            <ta e="T582" id="Seg_10936" s="T579">Und sorgst dich um mich!</ta>
            <ta e="T587" id="Seg_10937" s="T582">Irgendwie (?)…</ta>
            <ta e="T591" id="Seg_10938" s="T587">Du schickst Wärme</ta>
            <ta e="T596" id="Seg_10939" s="T591">Du gibst Regen, damit Korn wuchs.</ta>
            <ta e="T599" id="Seg_10940" s="T596">Damit Korn (wuchs?)…</ta>
            <ta e="T609" id="Seg_10941" s="T599">Es ist gut mit Gott [zu sein], wo auch immer du gehst, mit Gott (überall?), es ist sehr gut mit Gott [zu sein].</ta>
            <ta e="T611" id="Seg_10942" s="T609">Der Wind beruhigte sich. [?]</ta>
            <ta e="T616" id="Seg_10943" s="T611">Ich (nahm es?) und brachte es nach Hause.</ta>
            <ta e="T617" id="Seg_10944" s="T616">Gut.</ta>
            <ta e="T621" id="Seg_10945" s="T617">Es regnet, Korn wird wachsen.</ta>
            <ta e="T624" id="Seg_10946" s="T621">Ich half ihm/ihr.</ta>
            <ta e="T629" id="Seg_10947" s="T624">Aber sie/er hilft mir nicht.</ta>
            <ta e="T641" id="Seg_10948" s="T629">Sie/Er läßt mich nicht arbeiten, ich würde arbeiten, doch sie/er (stört?) immer.</ta>
            <ta e="T643" id="Seg_10949" s="T641">[Sie/Er?] ist wütend.</ta>
            <ta e="T646" id="Seg_10950" s="T643">[Sie/Er?] stampft mit dem Fuß.</ta>
            <ta e="T649" id="Seg_10951" s="T646">Sehr wütender Hund.</ta>
            <ta e="T654" id="Seg_10952" s="T649">Er beißt Leute.</ta>
            <ta e="T656" id="Seg_10953" s="T654">Er beißt Leute.</ta>
            <ta e="T659" id="Seg_10954" s="T656">Sehr wütender Hund.</ta>
            <ta e="T663" id="Seg_10955" s="T659">Er beißt [oft] Leute.</ta>
            <ta e="T666" id="Seg_10956" s="T663">Und er biß mich auch.</ta>
            <ta e="T674" id="Seg_10957" s="T666">Ich wartete auf dich, du kamst nicht, du warst langer Zeit nicht da.</ta>
            <ta e="T677" id="Seg_10958" s="T674">Ich nahm [Wasser] von der Tasse, ich wusch [mich].</ta>
            <ta e="T680" id="Seg_10959" s="T677">Und ich tat es zurück.</ta>
            <ta e="T684" id="Seg_10960" s="T680">(?) today.</ta>
            <ta e="T686" id="Seg_10961" s="T684">(…) auf keiner Weise.</ta>
            <ta e="T695" id="Seg_10962" s="T686">Fall nicht ins Wasser, beweg dich mit Händen und Füßen. [?]</ta>
            <ta e="T701" id="Seg_10963" s="T695">Ins Wasser gehst du mit Händen und Füßen.</ta>
            <ta e="T707" id="Seg_10964" s="T701">Mit einem Boot kann man aufs Wasser.</ta>
            <ta e="T710" id="Seg_10965" s="T707">[Jemand] fängt Fische.</ta>
            <ta e="T712" id="Seg_10966" s="T710">Wir (?) drei zusammen.</ta>
            <ta e="T718" id="Seg_10967" s="T712">Einen für dich, einen für mich, einen für ihn/ihr.</ta>
            <ta e="T724" id="Seg_10968" s="T718">Zwei Stücke, drei Stücke, vier Stücke.</ta>
            <ta e="T729" id="Seg_10969" s="T724">So saßen sie alle.</ta>
            <ta e="T733" id="Seg_10970" s="T729">Ich saß auf einem Pferd.</ta>
            <ta e="T737" id="Seg_10971" s="T733">Auf dem zweiten Pferd saß meine Frau.</ta>
            <ta e="T741" id="Seg_10972" s="T737">Auf dem dritten Pferd saß mein Sohn.</ta>
            <ta e="T745" id="Seg_10973" s="T741">Auf dem vierten Pferd saß meine Tochter.</ta>
            <ta e="T750" id="Seg_10974" s="T745">Und auf dem fünften Pferd saß Großmutter.</ta>
            <ta e="T757" id="Seg_10975" s="T751">Dieser Junge kam, um uns zu fotografieren.</ta>
            <ta e="T761" id="Seg_10976" s="T757">Bald wird es kalt werden.</ta>
            <ta e="T765" id="Seg_10977" s="T761">Es wird schneien.</ta>
            <ta e="T767" id="Seg_10978" s="T765">Sehr kalt.</ta>
            <ta e="T769" id="Seg_10979" s="T767">Dir wird es frieren.</ta>
            <ta e="T775" id="Seg_10980" s="T769">Bald wird es warm werden.</ta>
            <ta e="T779" id="Seg_10981" s="T775">Dann (werden?) Leute anfangen zu pflügen.</ta>
            <ta e="T781" id="Seg_10982" s="T779">Sie (werden?) Weizen säen.</ta>
            <ta e="T786" id="Seg_10983" s="T781">Es fliegt sehr weit über uns. [?]</ta>
            <ta e="T788" id="Seg_10984" s="T786">Es is nicht sichtbar.</ta>
            <ta e="T790" id="Seg_10985" s="T788">Und…</ta>
            <ta e="T797" id="Seg_10986" s="T790">Sie/Er fiel vom Pferd zum Boden.</ta>
            <ta e="T802" id="Seg_10987" s="T797">Man muß sehr hochsteigen.</ta>
            <ta e="T805" id="Seg_10988" s="T802">Ich weiß nicht mehr.</ta>
            <ta e="T810" id="Seg_10989" s="T806">Gott lebt sehr hoch über uns. [?]</ta>
            <ta e="T816" id="Seg_10990" s="T810">Auf einem Berg, einem großen Berg.</ta>
            <ta e="T819" id="Seg_10991" s="T816">Mein Kopf juckt mir.</ta>
            <ta e="T823" id="Seg_10992" s="T819">Ich bürste, vielleicht sind es viele Läuse.</ta>
            <ta e="T828" id="Seg_10993" s="T823">Ich müßte sie alle töten.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T12" id="Seg_10994" s="T4">His/her eyes are not beautiful, one is looking here, one there.</ta>
            <ta e="T14" id="Seg_10995" s="T12">His eyes (?).</ta>
            <ta e="T20" id="Seg_10996" s="T14">My mouth is closing, I want to sleep.</ta>
            <ta e="T25" id="Seg_10997" s="T20">Two of us came up to the road.</ta>
            <ta e="T32" id="Seg_10998" s="T25">S/he went to the left, I went to the right.</ta>
            <ta e="T38" id="Seg_10999" s="T32">What's his/her name, I don't know.</ta>
            <ta e="T43" id="Seg_11000" s="T38">The bees bring sweet things.</ta>
            <ta e="T47" id="Seg_11001" s="T43">And they put it in the cups.</ta>
            <ta e="T50" id="Seg_11002" s="T47">They work very hard.</ta>
            <ta e="T58" id="Seg_11003" s="T50">They don't live in the taiga, they don't live in water.</ta>
            <ta e="T61" id="Seg_11004" s="T58">A very big mountain.</ta>
            <ta e="T64" id="Seg_11005" s="T61">How to go there?</ta>
            <ta e="T66" id="Seg_11006" s="T64">I don't know.</ta>
            <ta e="T74" id="Seg_11007" s="T66">When you look [at it], your hat will fall down.</ta>
            <ta e="T77" id="Seg_11008" s="T74">They went to dig the ground.</ta>
            <ta e="T80" id="Seg_11009" s="T77">The road…</ta>
            <ta e="T84" id="Seg_11010" s="T80">They went to dig the ground.</ta>
            <ta e="T86" id="Seg_11011" s="T84">To repair the road.</ta>
            <ta e="T91" id="Seg_11012" s="T86">Where did they go, far away?</ta>
            <ta e="T94" id="Seg_11013" s="T91">No, not far.</ta>
            <ta e="T96" id="Seg_11014" s="T94">They came with horses.</ta>
            <ta e="T100" id="Seg_11015" s="T96">Not far away they stayed in a house.</ta>
            <ta e="T105" id="Seg_11016" s="T100">They did not even ask, they were just standing.</ta>
            <ta e="T112" id="Seg_11017" s="T105">Go out of my house, and don’t stand here!</ta>
            <ta e="T114" id="Seg_11018" s="T112">They did not go.</ta>
            <ta e="T116" id="Seg_11019" s="T114">They hid in the house.</ta>
            <ta e="T119" id="Seg_11020" s="T116">And all are making noise.</ta>
            <ta e="T123" id="Seg_11021" s="T119">They climbed on the roof of the house. [?]</ta>
            <ta e="T126" id="Seg_11022" s="T123">There are no (?).</ta>
            <ta e="T132" id="Seg_11023" s="T126">They all went somewhere or died.</ta>
            <ta e="T136" id="Seg_11024" s="T132">You are also hitting me with stones.</ta>
            <ta e="T142" id="Seg_11025" s="T136">You will also throw a stone to me. [?]</ta>
            <ta e="T146" id="Seg_11026" s="T142">People are good, we do not lie.</ta>
            <ta e="T149" id="Seg_11027" s="T146">We speak well.</ta>
            <ta e="T153" id="Seg_11028" s="T149">I tell the truth, I don't lie.</ta>
            <ta e="T161" id="Seg_11029" s="T153">I collected red maggots, washed [them] with water.</ta>
            <ta e="T163" id="Seg_11030" s="T161">I put them in a cup.</ta>
            <ta e="T167" id="Seg_11031" s="T163">Then I put them into a warm place. [?]</ta>
            <ta e="T170" id="Seg_11032" s="T167">There they became [like] broth.</ta>
            <ta e="T173" id="Seg_11033" s="T170">Then I rubbed my hand.</ta>
            <ta e="T180" id="Seg_11034" s="T173">My hand was hurting a lot, now it does not hurt.</ta>
            <ta e="T183" id="Seg_11035" s="T180">Look, God…</ta>
            <ta e="T187" id="Seg_11036" s="T183">Look, do not forget God.</ta>
            <ta e="T191" id="Seg_11037" s="T187">Wait, how does it go…</ta>
            <ta e="T193" id="Seg_11038" s="T191">Go to the devil!</ta>
            <ta e="T196" id="Seg_11039" s="T193">Devil’s horns!</ta>
            <ta e="T198" id="Seg_11040" s="T196">Cow’s horn.</ta>
            <ta e="T203" id="Seg_11041" s="T198">It will gore you.</ta>
            <ta e="T210" id="Seg_11042" s="T203">Sweep the hallway, because it is very dirty.</ta>
            <ta e="T212" id="Seg_11043" s="T210">Sweep the stairs!</ta>
            <ta e="T216" id="Seg_11044" s="T212">I took flour to the barn, I put it there.</ta>
            <ta e="T222" id="Seg_11045" s="T216">I killed it, I have a lot of force.</ta>
            <ta e="T226" id="Seg_11046" s="T222">I am very strong.</ta>
            <ta e="T231" id="Seg_11047" s="T226">This man has no mind.</ta>
            <ta e="T236" id="Seg_11048" s="T231">He has a tongue, [but] he cannot talk.</ta>
            <ta e="T240" id="Seg_11049" s="T236">He has ears, [but] he does not hear.</ta>
            <ta e="T242" id="Seg_11050" s="T240">Don’t turn around!</ta>
            <ta e="T244" id="Seg_11051" s="T242">Sit nicely!</ta>
            <ta e="T246" id="Seg_11052" s="T244">Why are you turing around?</ta>
            <ta e="T251" id="Seg_11053" s="T246">I hear [=understand] my language, I speak well.</ta>
            <ta e="T256" id="Seg_11054" s="T251">The pig comes and oinks.</ta>
            <ta e="T263" id="Seg_11055" s="T256">I need to feed it, otherwise it will go hungry.</ta>
            <ta e="T273" id="Seg_11056" s="T263">I need to sharpen the axe to chop wood.</ta>
            <ta e="T275" id="Seg_11057" s="T273">To heat the furnace.</ta>
            <ta e="T282" id="Seg_11058" s="T275">They brought a saw, they don't know what to do with it.</ta>
            <ta e="T286" id="Seg_11059" s="T282">Varlam will come, he will fix it.</ta>
            <ta e="T289" id="Seg_11060" s="T286">They brought an iron stove.</ta>
            <ta e="T291" id="Seg_11061" s="T289">They set it up into the house.</ta>
            <ta e="T294" id="Seg_11062" s="T291">On the birchbark barrel.</ta>
            <ta e="T299" id="Seg_11063" s="T294">(?) the barrel, it caught fire.</ta>
            <ta e="T301" id="Seg_11064" s="T299">There came smoke.</ta>
            <ta e="T305" id="Seg_11065" s="T301">Then they let it all out.</ta>
            <ta e="T308" id="Seg_11066" s="T305">Lots of smoke.</ta>
            <ta e="T312" id="Seg_11067" s="T308">Varlam will come and fix it.</ta>
            <ta e="T316" id="Seg_11068" s="T312">The smoke is getting into my eyes.</ta>
            <ta e="T321" id="Seg_11069" s="T316">Then Varlam came, he brought (a?) stone.</ta>
            <ta e="T323" id="Seg_11070" s="T321">He put it on the ground.</ta>
            <ta e="T326" id="Seg_11071" s="T323">And set the stove [on the stone].</ta>
            <ta e="T333" id="Seg_11072" s="T326">Then they lit the stove, they were sitting and getting warm.</ta>
            <ta e="T337" id="Seg_11073" s="T333">(…) not to make a mistake.</ta>
            <ta e="T343" id="Seg_11074" s="T338">My fur coat was lying on the ground.</ta>
            <ta e="T346" id="Seg_11075" s="T343">People's feet made it dirty.</ta>
            <ta e="T361" id="Seg_11076" s="T346">It is dirty, I took it, and I brought it to the water, for it to become wet.</ta>
            <ta e="T363" id="Seg_11077" s="T361">Then I beat it [clean].</ta>
            <ta e="T364" id="Seg_11078" s="T363">I washed it.</ta>
            <ta e="T366" id="Seg_11079" s="T364">Then I hung it.</ta>
            <ta e="T370" id="Seg_11080" s="T366">Water is flowing from it.</ta>
            <ta e="T372" id="Seg_11081" s="T370">The river is flowing.</ta>
            <ta e="T376" id="Seg_11082" s="T372">And a man is running to fetch water.</ta>
            <ta e="T379" id="Seg_11083" s="T376">What kind of people are going around.</ta>
            <ta e="T384" id="Seg_11084" s="T379">I don't know them.</ta>
            <ta e="T389" id="Seg_11085" s="T384">Why are they going around here?</ta>
            <ta e="T395" id="Seg_11086" s="T389">And this boy and the girl are coming.</ta>
            <ta e="T399" id="Seg_11087" s="T395">I need to pour milk into the vessel.</ta>
            <ta e="T402" id="Seg_11088" s="T399">Let it become sour.</ta>
            <ta e="T406" id="Seg_11089" s="T402">Now we will leave.</ta>
            <ta e="T410" id="Seg_11090" s="T406">Well, then fare well!</ta>
            <ta e="T414" id="Seg_11091" s="T410">We spoke very much.</ta>
            <ta e="T418" id="Seg_11092" s="T414">Now I should go home.</ta>
            <ta e="T421" id="Seg_11093" s="T418">And lie down to sleep.</ta>
            <ta e="T425" id="Seg_11094" s="T421">Don't boast!</ta>
            <ta e="T431" id="Seg_11095" s="T425">You didn't do anything, you were only boasting.</ta>
            <ta e="T437" id="Seg_11096" s="T431">I'll lie down to sleep, and tomorrow my mind will come [= I'll sleep on it].</ta>
            <ta e="T444" id="Seg_11097" s="T437">A man came to the window, and I grabbed a stone.</ta>
            <ta e="T448" id="Seg_11098" s="T446">Lock up (?).</ta>
            <ta e="T454" id="Seg_11099" s="T448">I grabbed a log and killed this man.</ta>
            <ta e="T462" id="Seg_11100" s="T454">A man stabbed another man with a knife, and he died.</ta>
            <ta e="T475" id="Seg_11101" s="T462">There is a woman, in Aginskoye one woman hit her husband with an axe and cut off his head.</ta>
            <ta e="T480" id="Seg_11102" s="T475">Her husband fell asleep alone.</ta>
            <ta e="T483" id="Seg_11103" s="T480">I got very frightened.</ta>
            <ta e="T488" id="Seg_11104" s="T483">How she didn’t cut off my head [?].</ta>
            <ta e="T496" id="Seg_11105" s="T488">She was pregnant, and two children were left alone.</ta>
            <ta e="T504" id="Seg_11106" s="T496">Five years already passed [since then].</ta>
            <ta e="T507" id="Seg_11107" s="T504">Yesterday you were not [there].</ta>
            <ta e="T512" id="Seg_11108" s="T507">Today you came to listen.</ta>
            <ta e="T514" id="Seg_11109" s="T512">What are we talking [about] (/saying)?</ta>
            <ta e="T521" id="Seg_11110" s="T514">I will pray to God today.</ta>
            <ta e="T524" id="Seg_11111" s="T521">God, come to me!</ta>
            <ta e="T528" id="Seg_11112" s="T524">Hear how I pray.</ta>
            <ta e="T531" id="Seg_11113" s="T528">Look at my heart!</ta>
            <ta e="T534" id="Seg_11114" s="T531">Perhaps it is clean.</ta>
            <ta e="T540" id="Seg_11115" s="T534">But if it is not clean, wash it with blood!</ta>
            <ta e="T543" id="Seg_11116" s="T540">You hung on the wood.</ta>
            <ta e="T545" id="Seg_11117" s="T543">Blood was flowing.</ta>
            <ta e="T547" id="Seg_11118" s="T545">Wash me!</ta>
            <ta e="T549" id="Seg_11119" s="T547">Wash me well!</ta>
            <ta e="T553" id="Seg_11120" s="T549">God, you have a lot of power.</ta>
            <ta e="T558" id="Seg_11121" s="T553">You love all people.</ta>
            <ta e="T561" id="Seg_11122" s="T558">You give bread.</ta>
            <ta e="T566" id="Seg_11123" s="T562">You, God, have a lot of power.</ta>
            <ta e="T571" id="Seg_11124" s="T566">You care for people.</ta>
            <ta e="T574" id="Seg_11125" s="T571">You give them bread.</ta>
            <ta e="T577" id="Seg_11126" s="T574">You give shirts.</ta>
            <ta e="T579" id="Seg_11127" s="T577">You give water.</ta>
            <ta e="T582" id="Seg_11128" s="T579">And care for me!</ta>
            <ta e="T587" id="Seg_11129" s="T582">Somehow (?)…</ta>
            <ta e="T591" id="Seg_11130" s="T587">You send warmth.</ta>
            <ta e="T596" id="Seg_11131" s="T591">You give rain, so that grain grew.</ta>
            <ta e="T599" id="Seg_11132" s="T596">So that grain (grew?)…</ta>
            <ta e="T609" id="Seg_11133" s="T599">It is good [to be] with God, wherever you go, with God (everywhere?), it is very good [to be] with God.</ta>
            <ta e="T611" id="Seg_11134" s="T609">The wind calmed down. [?]</ta>
            <ta e="T616" id="Seg_11135" s="T611">I (took it?) and brought it in the house.</ta>
            <ta e="T617" id="Seg_11136" s="T616">Good.</ta>
            <ta e="T621" id="Seg_11137" s="T617">It rains, grain will grow.</ta>
            <ta e="T624" id="Seg_11138" s="T621">I helped him/her.</ta>
            <ta e="T629" id="Seg_11139" s="T624">But s/he is not helping me.</ta>
            <ta e="T641" id="Seg_11140" s="T629">S/he does not let me work, I would work, but s/he is always (disturbing?) me.</ta>
            <ta e="T643" id="Seg_11141" s="T641">[S/he?] is angry.</ta>
            <ta e="T646" id="Seg_11142" s="T643">[S/he] stamped with the foot.</ta>
            <ta e="T649" id="Seg_11143" s="T646">Very angry dog.</ta>
            <ta e="T654" id="Seg_11144" s="T649">It bites people.</ta>
            <ta e="T656" id="Seg_11145" s="T654">It bites people.</ta>
            <ta e="T659" id="Seg_11146" s="T656">Very angry dog.</ta>
            <ta e="T663" id="Seg_11147" s="T659">It bites people [often].</ta>
            <ta e="T666" id="Seg_11148" s="T663">And it bit me too.</ta>
            <ta e="T674" id="Seg_11149" s="T666">I waited for you, you did not come, you were not there for a long time.</ta>
            <ta e="T677" id="Seg_11150" s="T674">I took [water] from the cup, I washed [myself].</ta>
            <ta e="T680" id="Seg_11151" s="T677">And I put it back.</ta>
            <ta e="T684" id="Seg_11152" s="T680">(?) heute.</ta>
            <ta e="T686" id="Seg_11153" s="T684">(…) in no way.</ta>
            <ta e="T695" id="Seg_11154" s="T686">Do not fall into the water, move with your hands and feet. [?]</ta>
            <ta e="T701" id="Seg_11155" s="T695">In the water, you go with hands and feet.</ta>
            <ta e="T707" id="Seg_11156" s="T701">With a boat one can go on the water.</ta>
            <ta e="T710" id="Seg_11157" s="T707">[Someone] is catching fish.</ta>
            <ta e="T712" id="Seg_11158" s="T710">We will (?) three together.</ta>
            <ta e="T718" id="Seg_11159" s="T712">One for you, one for me, one for him/her.</ta>
            <ta e="T724" id="Seg_11160" s="T718">Two pieces, three pieces, four pieces.</ta>
            <ta e="T729" id="Seg_11161" s="T724">So they all were seated.</ta>
            <ta e="T733" id="Seg_11162" s="T729">I sat on one horse.</ta>
            <ta e="T737" id="Seg_11163" s="T733">On the second horse, my wife sat.</ta>
            <ta e="T741" id="Seg_11164" s="T737">On the third horse, my son sat.</ta>
            <ta e="T745" id="Seg_11165" s="T741">On the fourth horse, my daughter sat.</ta>
            <ta e="T750" id="Seg_11166" s="T745">And on the fifth horse, grandmother sat.</ta>
            <ta e="T757" id="Seg_11167" s="T751">This boy came to take a picture of us.</ta>
            <ta e="T761" id="Seg_11168" s="T757">Soon it will become cold.</ta>
            <ta e="T765" id="Seg_11169" s="T761">It will snow.</ta>
            <ta e="T767" id="Seg_11170" s="T765">Very cold.</ta>
            <ta e="T769" id="Seg_11171" s="T767">You will freeze.</ta>
            <ta e="T775" id="Seg_11172" s="T769">Soon it will become warm.</ta>
            <ta e="T779" id="Seg_11173" s="T775">Then people (will start?) ploughing.</ta>
            <ta e="T781" id="Seg_11174" s="T779">They (will? sow wheat.</ta>
            <ta e="T786" id="Seg_11175" s="T781">It is flying very far above. [?]</ta>
            <ta e="T788" id="Seg_11176" s="T786">It is not visible.</ta>
            <ta e="T790" id="Seg_11177" s="T788">And…</ta>
            <ta e="T797" id="Seg_11178" s="T790">S/he fell from the horse on the ground.</ta>
            <ta e="T802" id="Seg_11179" s="T797">One needs to go up very high.</ta>
            <ta e="T805" id="Seg_11180" s="T802">I don't know any more.</ta>
            <ta e="T810" id="Seg_11181" s="T806">God lives very high above. [?]</ta>
            <ta e="T816" id="Seg_11182" s="T810">On a mountain, a big mountain.</ta>
            <ta e="T819" id="Seg_11183" s="T816">My head itches.</ta>
            <ta e="T823" id="Seg_11184" s="T819">I brush, perhaps there are many lice.</ta>
            <ta e="T828" id="Seg_11185" s="T823">I should kill them all.</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T47" id="Seg_11186" s="T43">[GVY:] -d- in endləʔbəʔjə may be just an occasional epenthesis between n and l.</ta>
            <ta e="T58" id="Seg_11187" s="T50">An alternative translation could be 'Spirit[s] live in the taiga, spirit lives in the water', if ej (should be rather eje) is 'master, spirit'.</ta>
            <ta e="T74" id="Seg_11188" s="T66">[GVY:] The omission of schwa in măndlial (= măndəlial) may be occasional.</ta>
            <ta e="T112" id="Seg_11189" s="T105">[AAV] sound seriously damaged</ta>
            <ta e="T126" id="Seg_11190" s="T123">Should be sagər süjöʔi 'black birds'? Or šagən nüjnüʔi 'in the house (there are no) songs'? </ta>
            <ta e="T161" id="Seg_11191" s="T153">[AAV] sound severly damaged; b- (not d-) after "büzʼiʔ"</ta>
            <ta e="T167" id="Seg_11192" s="T163">[AAV]: put them into a warm place?</ta>
            <ta e="T193" id="Seg_11193" s="T191">[KlT]: lit. "invisible spirit"</ta>
            <ta e="T210" id="Seg_11194" s="T203">Сени Ru. ’hall’.</ta>
            <ta e="T212" id="Seg_11195" s="T210">Крыльцо Ru. '(outside) stairs'.</ta>
            <ta e="T231" id="Seg_11196" s="T226">[KlT]: i.e. 'he is crazy'</ta>
            <ta e="T282" id="Seg_11197" s="T275">Пила Ru. 'saw'.</ta>
            <ta e="T299" id="Seg_11198" s="T294">[GVY:] Or basə?</ta>
            <ta e="T301" id="Seg_11199" s="T299">Double-check the vowel.</ta>
            <ta e="T316" id="Seg_11200" s="T312">[GVY:] amnalaʔbəʔjə = amnaʔbəʔjə [eat-DUR-3PL]</ta>
            <ta e="T346" id="Seg_11201" s="T343">[KlT]: Lit. "Feet pressed it."</ta>
            <ta e="T410" id="Seg_11202" s="T406">[KlT]: lit. "go with God"</ta>
            <ta e="T437" id="Seg_11203" s="T431">[KlT:] Contamination sagəš ’mind’ + šag ’strength’ (?).</ta>
            <ta e="T454" id="Seg_11204" s="T448">[AAV]: "pi" 'wood' or 'stone'?</ta>
            <ta e="T488" id="Seg_11205" s="T483">[AAV]: unclear. "What if?" ?</ta>
            <ta e="T521" id="Seg_11206" s="T514">[GVY:] numan (üzə- üz-) üzləm</ta>
            <ta e="T534" id="Seg_11207" s="T531">[AAV]: Why 3PL?</ta>
            <ta e="T543" id="Seg_11208" s="T540">[KlT]: i.e. on the cross.</ta>
            <ta e="T577" id="Seg_11209" s="T574">[AAV]: i.e. clothes.</ta>
            <ta e="T609" id="Seg_11210" s="T599">[GVY:] Probably she was going to say Russian vezde 'everywhere'.</ta>
            <ta e="T611" id="Seg_11211" s="T609">[AAV]: unclear. Some object fell down?</ta>
            <ta e="T677" id="Seg_11212" s="T674">[AAV]: Is it surely water? 'washed it' or 'washed myself'? </ta>
            <ta e="T684" id="Seg_11213" s="T680">Possibly means tenöbiam 'I thought'.</ta>
            <ta e="T695" id="Seg_11214" s="T686">[GVY:] Unclear</ta>
            <ta e="T712" id="Seg_11215" s="T710">[GVY:] Maybe 'We will divide it [the fish] between the three of us'?</ta>
            <ta e="T737" id="Seg_11216" s="T733">[KlT]: lit. 'two' instead of 'second'</ta>
            <ta e="T741" id="Seg_11217" s="T737">[KlT]: lit. 'three' instead of 'third'</ta>
            <ta e="T745" id="Seg_11218" s="T741">[KlT]: lit. 'four' instead of 'fourth'</ta>
            <ta e="T750" id="Seg_11219" s="T745">[KlT]: lit. 'five' instead of 'fifth'</ta>
            <ta e="T757" id="Seg_11220" s="T751">[KlT:] AK. transcr: = to photograph.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T851" />
            <conversion-tli id="T74" />
            <conversion-tli id="T852" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T855" />
            <conversion-tli id="T86" />
            <conversion-tli id="T856" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T861" />
            <conversion-tli id="T105" />
            <conversion-tli id="T862" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T865" />
            <conversion-tli id="T116" />
            <conversion-tli id="T866" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T873" />
            <conversion-tli id="T149" />
            <conversion-tli id="T874" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T875" />
            <conversion-tli id="T153" />
            <conversion-tli id="T876" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T883" />
            <conversion-tli id="T191" />
            <conversion-tli id="T884" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T885" />
            <conversion-tli id="T196" />
            <conversion-tli id="T886" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T903" />
            <conversion-tli id="T263" />
            <conversion-tli id="T904" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T911" />
            <conversion-tli id="T316" />
            <conversion-tli id="T912" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T913" />
            <conversion-tli id="T334" />
            <conversion-tli id="T914" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T925" />
            <conversion-tli id="T410" />
            <conversion-tli id="T926" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T927" />
            <conversion-tli id="T414" />
            <conversion-tli id="T928" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T935" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T936" />
            <conversion-tli id="T447" />
            <conversion-tli id="T937" />
            <conversion-tli id="T448" />
            <conversion-tli id="T938" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T941" />
            <conversion-tli id="T462" />
            <conversion-tli id="T942" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T955" />
            <conversion-tli id="T577" />
            <conversion-tli id="T956" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T957" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T958" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T959" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T960" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T967" />
            <conversion-tli id="T641" />
            <conversion-tli id="T968" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T971" />
            <conversion-tli id="T656" />
            <conversion-tli id="T972" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T979" />
            <conversion-tli id="T680" />
            <conversion-tli id="T980" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T995" />
            <conversion-tli id="T751" />
            <conversion-tli id="T996" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T806" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T816" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T828" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
