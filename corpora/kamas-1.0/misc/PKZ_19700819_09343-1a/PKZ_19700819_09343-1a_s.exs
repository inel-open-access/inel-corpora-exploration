<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID53789E0C-596F-2B86-EC85-2945B3E86376">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_19700819_09343_2a</transcription-name>
         <referenced-file url="PKZ_19700819_09343-1a.wav" />
         <referenced-file url="PKZ_19700819_09343-1a.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_19700819_09343-1a\PKZ_19700819_09343-1a.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">2457</ud-information>
            <ud-information attribute-name="# HIAT:w">1594</ud-information>
            <ud-information attribute-name="# e">1619</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">29</ud-information>
            <ud-information attribute-name="# HIAT:u">288</ud-information>
            <ud-information attribute-name="# sc">514</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.89" type="appl" />
         <tli id="T2" time="1.5766666666666667" type="appl" />
         <tli id="T3" time="2.263333333333333" type="appl" />
         <tli id="T4" time="2.39" type="appl" />
         <tli id="T5" time="2.85" type="appl" />
         <tli id="T6" time="2.95" type="appl" />
         <tli id="T7" time="3.3100000000000005" type="appl" />
         <tli id="T8" time="3.7700000000000005" type="appl" />
         <tli id="T9" time="4.23" type="appl" />
         <tli id="T10" time="4.33" type="appl" />
         <tli id="T11" time="10.01" type="appl" />
         <tli id="T12" time="10.55" type="appl" />
         <tli id="T13" time="20.59" type="appl" />
         <tli id="T14" time="21.36" type="appl" />
         <tli id="T15" time="22.35" type="appl" />
         <tli id="T16" time="23.03" type="appl" />
         <tli id="T17" time="23.655" type="appl" />
         <tli id="T18" time="24.28" type="appl" />
         <tli id="T19" time="24.905" type="appl" />
         <tli id="T20" time="25.53" type="appl" />
         <tli id="T21" time="26.155" type="appl" />
         <tli id="T22" time="26.78" type="appl" />
         <tli id="T23" time="26.82" type="appl" />
         <tli id="T24" time="27.6265" type="appl" />
         <tli id="T25" time="28.433" type="appl" />
         <tli id="T26" time="29.2395" type="appl" />
         <tli id="T27" time="30.046" type="appl" />
         <tli id="T28" time="30.8525" type="appl" />
         <tli id="T29" time="31.659" type="appl" />
         <tli id="T30" time="32.4655" type="appl" />
         <tli id="T31" time="33.272" type="appl" />
         <tli id="T32" time="33.32" type="appl" />
         <tli id="T33" time="33.985875" type="appl" />
         <tli id="T34" time="34.65175" type="appl" />
         <tli id="T35" time="35.317625" type="appl" />
         <tli id="T36" time="35.9835" type="appl" />
         <tli id="T37" time="36.649375" type="appl" />
         <tli id="T38" time="37.31525" type="appl" />
         <tli id="T39" time="37.981125" type="appl" />
         <tli id="T40" time="38.647" type="appl" />
         <tli id="T41" time="39.08" type="appl" />
         <tli id="T42" time="39.848571428571425" type="appl" />
         <tli id="T43" time="40.61714285714286" type="appl" />
         <tli id="T44" time="41.385714285714286" type="appl" />
         <tli id="T45" time="42.15428571428571" type="appl" />
         <tli id="T46" time="42.92285714285714" type="appl" />
         <tli id="T47" time="43.691428571428574" type="appl" />
         <tli id="T48" time="44.46" type="appl" />
         <tli id="T49" time="44.65991139592096" />
         <tli id="T51" time="45.7" type="appl" />
         <tli id="T52" time="46.315" type="appl" />
         <tli id="T53" time="46.93" type="appl" />
         <tli id="T54" time="48.63" type="appl" />
         <tli id="T55" time="49.41225" type="appl" />
         <tli id="T56" time="50.194500000000005" type="appl" />
         <tli id="T57" time="50.97675" type="appl" />
         <tli id="T58" time="51.759" type="appl" />
         <tli id="T59" time="51.96" type="appl" />
         <tli id="T60" time="52.57833333333333" type="appl" />
         <tli id="T61" time="53.196666666666665" type="appl" />
         <tli id="T62" time="53.815" type="appl" />
         <tli id="T63" time="54.43333333333334" type="appl" />
         <tli id="T64" time="55.05166666666667" type="appl" />
         <tli id="T65" time="55.67" type="appl" />
         <tli id="T66" time="56.01" type="appl" />
         <tli id="T67" time="57.03" type="appl" />
         <tli id="T68" time="57.766552059360365" />
         <tli id="T69" time="58.68" type="appl" />
         <tli id="T70" time="59.88" type="appl" />
         <tli id="T71" time="60.42333333333333" type="appl" />
         <tli id="T72" time="60.96666666666667" type="appl" />
         <tli id="T73" time="61.510000000000005" type="appl" />
         <tli id="T74" time="62.053333333333335" type="appl" />
         <tli id="T75" time="62.596666666666664" type="appl" />
         <tli id="T76" time="63.14" type="appl" />
         <tli id="T77" time="63.2" type="appl" />
         <tli id="T78" time="63.74" type="appl" />
         <tli id="T79" time="64.28" type="appl" />
         <tli id="T80" time="64.82000000000001" type="appl" />
         <tli id="T81" time="65.36" type="appl" />
         <tli id="T82" time="66.20653531465757" />
         <tli id="T83" time="66.33" type="appl" />
         <tli id="T84" time="66.61" type="appl" />
         <tli id="T85" time="67.01" type="appl" />
         <tli id="T86" time="67.0475" type="appl" />
         <tli id="T87" time="67.66653241806206" />
         <tli id="T88" time="67.68451620903103" type="intp" />
         <tli id="T89" time="67.7025" type="appl" />
         <tli id="T90" time="67.9225" type="appl" />
         <tli id="T91" time="68.10499999999999" type="appl" />
         <tli id="T92" time="68.36" type="appl" />
         <tli id="T93" time="68.5075" type="appl" />
         <tli id="T94" time="68.91" type="appl" />
         <tli id="T95" time="69.2444" type="appl" />
         <tli id="T96" time="69.56" type="appl" />
         <tli id="T97" time="69.5788" type="appl" />
         <tli id="T98" time="69.91319999999999" type="appl" />
         <tli id="T99" time="69.91538461538462" type="appl" />
         <tli id="T100" time="70.24759999999999" type="appl" />
         <tli id="T101" time="70.27076923076923" type="appl" />
         <tli id="T102" time="70.582" type="appl" />
         <tli id="T103" time="70.62615384615385" type="appl" />
         <tli id="T104" time="70.98153846153846" type="appl" />
         <tli id="T105" time="71.33692307692309" type="appl" />
         <tli id="T106" time="71.6923076923077" type="appl" />
         <tli id="T107" time="72.04769230769232" type="appl" />
         <tli id="T108" time="72.40307692307692" type="appl" />
         <tli id="T109" time="72.48" type="appl" />
         <tli id="T110" time="72.75846153846155" type="appl" />
         <tli id="T111" time="72.82666666666667" type="appl" />
         <tli id="T112" time="73.11384615384615" type="appl" />
         <tli id="T113" time="73.17333333333333" type="appl" />
         <tli id="T114" time="73.46923076923078" type="appl" />
         <tli id="T115" time="73.52000000000001" type="appl" />
         <tli id="T116" time="73.82461538461538" type="appl" />
         <tli id="T117" time="73.86666666666667" type="appl" />
         <tli id="T118" time="74.18" type="appl" />
         <tli id="T119" time="74.21333333333334" type="appl" />
         <tli id="T120" time="74.51" type="appl" />
         <tli id="T121" time="74.56" type="appl" />
         <tli id="T122" time="75.10333333333334" type="appl" />
         <tli id="T123" time="75.15" type="appl" />
         <tli id="T124" time="75.59233333333334" type="appl" />
         <tli id="T125" time="75.69666666666667" type="appl" />
         <tli id="T126" time="76.03466666666667" type="appl" />
         <tli id="T127" time="76.28999999999999" type="appl" />
         <tli id="T128" time="76.477" type="appl" />
         <tli id="T129" time="76.88333333333333" type="appl" />
         <tli id="T130" time="76.95" type="appl" />
         <tli id="T131" time="77.47666666666666" type="appl" />
         <tli id="T132" time="77.545" type="appl" />
         <tli id="T133" time="78.07" type="appl" />
         <tli id="T134" time="78.08" type="appl" />
         <tli id="T135" time="78.14" type="appl" />
         <tli id="T136" time="78.625" type="appl" />
         <tli id="T137" time="79.17" type="appl" />
         <tli id="T138" time="79.35" type="appl" />
         <tli id="T139" time="80.16" type="appl" />
         <tli id="T140" time="80.97" type="appl" />
         <tli id="T141" time="81.78" type="appl" />
         <tli id="T142" time="82.34666666666666" type="appl" />
         <tli id="T143" time="82.91333333333334" type="appl" />
         <tli id="T144" time="83.43" type="appl" />
         <tli id="T145" time="83.48" type="appl" />
         <tli id="T146" time="83.9475" type="appl" />
         <tli id="T147" time="84.465" type="appl" />
         <tli id="T148" time="84.9825" type="appl" />
         <tli id="T149" time="85.5" type="appl" />
         <tli id="T150" time="85.93666666666667" type="appl" />
         <tli id="T151" time="86.32" type="appl" />
         <tli id="T152" time="86.37333333333333" type="appl" />
         <tli id="T153" time="86.81" type="appl" />
         <tli id="T154" time="87.24666666666667" type="appl" />
         <tli id="T155" time="87.55" type="appl" />
         <tli id="T156" time="87.68333333333334" type="appl" />
         <tli id="T157" time="88.12" type="appl" />
         <tli id="T158" time="88.55666666666667" type="appl" />
         <tli id="T159" time="88.99333333333334" type="appl" />
         <tli id="T160" time="89.43" type="appl" />
         <tli id="T161" time="89.54" type="appl" />
         <tli id="T162" time="89.88777777777779" type="appl" />
         <tli id="T163" time="90.23555555555556" type="appl" />
         <tli id="T164" time="90.58333333333334" type="appl" />
         <tli id="T165" time="90.93111111111112" type="appl" />
         <tli id="T166" time="91.27888888888889" type="appl" />
         <tli id="T167" time="91.62666666666667" type="appl" />
         <tli id="T168" time="91.97444444444444" type="appl" />
         <tli id="T169" time="92.32222222222222" type="appl" />
         <tli id="T170" time="92.67" type="appl" />
         <tli id="T171" time="93.52" type="appl" />
         <tli id="T172" time="94.35" type="appl" />
         <tli id="T173" time="94.73" type="appl" />
         <tli id="T174" time="95.12285714285714" type="appl" />
         <tli id="T175" time="95.18" type="appl" />
         <tli id="T176" time="95.5157142857143" type="appl" />
         <tli id="T177" time="95.90857142857143" type="appl" />
         <tli id="T178" time="96.30142857142857" type="appl" />
         <tli id="T179" time="96.69428571428571" type="appl" />
         <tli id="T180" time="97.08714285714287" type="appl" />
         <tli id="T181" time="97.48" type="appl" />
         <tli id="T182" time="97.6" type="appl" />
         <tli id="T183" time="98.32499999999999" type="appl" />
         <tli id="T184" time="99.05" type="appl" />
         <tli id="T185" time="99.31" type="appl" />
         <tli id="T186" time="100.17" type="appl" />
         <tli id="T187" time="101.28" type="appl" />
         <tli id="T188" time="101.88714285714286" type="appl" />
         <tli id="T189" time="102.49428571428571" type="appl" />
         <tli id="T190" time="103.10142857142857" type="appl" />
         <tli id="T191" time="103.70857142857143" type="appl" />
         <tli id="T192" time="104.3157142857143" type="appl" />
         <tli id="T193" time="104.92285714285714" type="appl" />
         <tli id="T194" time="105.53" type="appl" />
         <tli id="T195" time="107.55" type="appl" />
         <tli id="T196" time="107.825" type="appl" />
         <tli id="T197" time="108.1" type="appl" />
         <tli id="T198" time="108.375" type="appl" />
         <tli id="T199" time="108.64999999999999" type="appl" />
         <tli id="T200" time="108.925" type="appl" />
         <tli id="T201" time="109.19999999999999" type="appl" />
         <tli id="T202" time="109.475" type="appl" />
         <tli id="T203" time="109.75" type="appl" />
         <tli id="T204" time="110.02499999999999" type="appl" />
         <tli id="T205" time="110.3" type="appl" />
         <tli id="T206" time="110.57499999999999" type="appl" />
         <tli id="T207" time="110.73083401231332" type="intp" />
         <tli id="T208" time="110.88666802462664" />
         <tli id="T209" time="111.2375" type="appl" />
         <tli id="T210" time="111.615" type="appl" />
         <tli id="T211" time="111.9925" type="appl" />
         <tli id="T212" time="112.37" type="appl" />
         <tli id="T213" time="112.7475" type="appl" />
         <tli id="T214" time="113.125" type="appl" />
         <tli id="T215" time="113.5025" type="appl" />
         <tli id="T216" time="113.72" type="appl" />
         <tli id="T217" time="113.88" type="appl" />
         <tli id="T218" time="114.2575" type="appl" />
         <tli id="T219" time="114.27" type="appl" />
         <tli id="T220" time="114.635" type="appl" />
         <tli id="T221" time="114.82" type="appl" />
         <tli id="T222" time="115.0125" type="appl" />
         <tli id="T223" time="115.37" type="appl" />
         <tli id="T224" time="115.76643698912785" />
         <tli id="T225" time="115.87" type="appl" />
         <tli id="T226" time="115.97310324577414" />
         <tli id="T227" time="116.9597679549242" />
         <tli id="T228" time="117.19" type="appl" />
         <tli id="T229" time="117.64" type="appl" />
         <tli id="T230" time="118.09" type="appl" />
         <tli id="T231" time="118.54" type="appl" />
         <tli id="T232" time="118.71" type="appl" />
         <tli id="T233" time="119.1104" type="appl" />
         <tli id="T234" time="119.5108" type="appl" />
         <tli id="T235" time="119.9112" type="appl" />
         <tli id="T236" time="120.3116" type="appl" />
         <tli id="T237" time="120.55" type="appl" />
         <tli id="T238" time="120.58642742639474" />
         <tli id="T239" time="121.235" type="appl" />
         <tli id="T240" time="121.9" type="appl" />
         <tli id="T241" time="121.95975803507658" />
         <tli id="T242" time="122.39" type="appl" />
         <tli id="T243" time="123.09" type="appl" />
         <tli id="T244" time="123.47" type="appl" />
         <tli id="T245" time="123.974" type="appl" />
         <tli id="T246" time="124.478" type="appl" />
         <tli id="T247" time="124.982" type="appl" />
         <tli id="T248" time="125.48599999999999" type="appl" />
         <tli id="T249" time="125.99" type="appl" />
         <tli id="T250" time="126.92" type="appl" />
         <tli id="T251" time="127.82332713107776" />
         <tli id="T252" time="127.82641306245534" />
         <tli id="T253" time="128.575" type="appl" />
         <tli id="T254" time="129.2" type="appl" />
         <tli id="T255" time="129.45" type="appl" />
         <tli id="T256" time="130.08499999999998" type="appl" />
         <tli id="T258" time="130.642" />
         <tli id="T259" time="131.40466666666669" type="appl" />
         <tli id="T260" time="132.0113333333333" type="appl" />
         <tli id="T261" time="132.618" type="appl" />
         <tli id="T262" time="133.65799999999996" type="appl" />
         <tli id="T263" time="134.69799999999998" type="appl" />
         <tli id="T264" time="134.918" type="appl" />
         <tli id="T265" time="135.868" type="appl" />
         <tli id="T266" time="136.81799999999998" type="appl" />
         <tli id="T267" time="137.56799999999998" type="appl" />
         <tli id="T268" time="138.31799999999998" type="appl" />
         <tli id="T269" time="138.33800000000002" type="appl" />
         <tli id="T270" time="139.18600000000004" type="appl" />
         <tli id="T271" time="140.03400000000005" type="appl" />
         <tli id="T272" time="140.882" type="appl" />
         <tli id="T273" time="141.73000000000002" type="appl" />
         <tli id="T274" time="142.57800000000003" type="appl" />
         <tli id="T275" time="142.63799999999998" type="appl" />
         <tli id="T276" time="143.118" type="appl" />
         <tli id="T277" time="143.598" type="appl" />
         <tli id="T278" time="144.07799999999997" type="appl" />
         <tli id="T279" time="144.418" type="appl" />
         <tli id="T280" time="145.31745250376957" />
         <tli id="T281" time="145.35122574146354" type="intp" />
         <tli id="T282" time="145.3849989791575" type="intp" />
         <tli id="T283" time="145.41877221685147" type="intp" />
         <tli id="T284" time="145.45254545454543" type="appl" />
         <tli id="T285" time="145.58800000000002" type="appl" />
         <tli id="T286" time="145.8998181818182" type="appl" />
         <tli id="T287" time="145.978" type="appl" />
         <tli id="T288" time="146.34709090909092" type="appl" />
         <tli id="T289" time="146.368" type="appl" />
         <tli id="T290" time="146.75799999999998" type="appl" />
         <tli id="T291" time="146.79436363636364" type="appl" />
         <tli id="T292" time="146.95800000000003" type="appl" />
         <tli id="T293" time="147.24163636363636" type="appl" />
         <tli id="T294" time="147.68890909090908" type="appl" />
         <tli id="T295" time="147.858" type="appl" />
         <tli id="T296" time="148.1361818181818" type="appl" />
         <tli id="T297" time="148.58345454545457" type="appl" />
         <tli id="T298" time="148.75799999999998" type="appl" />
         <tli id="T299" time="149.0307272727273" type="appl" />
         <tli id="T300" time="149.478" type="appl" />
         <tli id="T301" time="149.538" type="appl" />
         <tli id="T302" time="150.01299999999998" type="appl" />
         <tli id="T303" time="150.488" type="appl" />
         <tli id="T304" time="150.96300000000002" type="appl" />
         <tli id="T305" time="151.25132093946536" />
         <tli id="T306" time="151.85000000000002" type="appl" />
         <tli id="T307" time="152.5606666666667" type="appl" />
         <tli id="T308" time="152.598" type="appl" />
         <tli id="T309" time="152.89800000000002" type="appl" />
         <tli id="T310" time="153.19800000000004" type="appl" />
         <tli id="T311" time="153.27133333333336" type="appl" />
         <tli id="T312" time="153.498" type="appl" />
         <tli id="T313" time="153.798" type="appl" />
         <tli id="T314" time="153.98200000000003" type="appl" />
         <tli id="T315" time="154.098" type="appl" />
         <tli id="T316" time="154.39800000000002" type="appl" />
         <tli id="T317" time="154.69800000000004" type="appl" />
         <tli id="T318" time="154.99800000000005" type="appl" />
         <tli id="T319" time="155.298" type="appl" />
         <tli id="T320" time="155.598" type="appl" />
         <tli id="T321" time="155.89800000000002" type="appl" />
         <tli id="T322" time="155.918" type="appl" />
         <tli id="T323" time="156.67800288140575" />
         <tli id="T324" time="156.70371572641716" type="intp" />
         <tli id="T325" time="156.72942857142857" type="appl" />
         <tli id="T326" time="157.10085714285714" type="appl" />
         <tli id="T327" time="157.16466666666668" type="appl" />
         <tli id="T328" time="157.4722857142857" type="appl" />
         <tli id="T329" time="157.788" type="appl" />
         <tli id="T330" time="157.84371428571433" type="appl" />
         <tli id="T331" time="158.2151428571429" type="appl" />
         <tli id="T332" time="158.41133333333335" type="appl" />
         <tli id="T333" time="158.58657142857146" type="appl" />
         <tli id="T334" time="159.1046647336397" />
         <tli id="T335" time="159.15466463444125" />
         <tli id="T336" time="159.73799681045898" />
         <tli id="T337" time="159.858" type="appl" />
         <tli id="T338" time="160.438" type="appl" />
         <tli id="T339" time="161.01799999999997" type="appl" />
         <tli id="T340" time="161.598" type="appl" />
         <tli id="T341" time="162.178" type="appl" />
         <tli id="T342" time="162.75799999999998" type="appl" />
         <tli id="T343" time="163.483" type="appl" />
         <tli id="T344" time="164.20800000000003" type="appl" />
         <tli id="T345" time="164.933" type="appl" />
         <tli id="T346" time="165.65800000000002" type="appl" />
         <tli id="T347" time="165.938" type="appl" />
         <tli id="T348" time="166.69800000000004" type="appl" />
         <tli id="T349" time="167.53800737711168" />
         <tli id="T350" time="168.01800000000003" type="appl" />
         <tli id="T351" time="168.57799999999997" type="appl" />
         <tli id="T352" time="169.13799999999998" type="appl" />
         <tli id="T353" time="169.498" type="appl" />
         <tli id="T354" time="170.058" type="appl" />
         <tli id="T355" time="170.618" type="appl" />
         <tli id="T356" time="171.178" type="appl" />
         <tli id="T357" time="171.738" type="appl" />
         <tli id="T358" time="172.298" type="appl" />
         <tli id="T359" time="172.878" type="appl" />
         <tli id="T360" time="173.428" type="appl" />
         <tli id="T361" time="173.978" type="appl" />
         <tli id="T362" time="174.52800000000002" type="appl" />
         <tli id="T363" time="175.07800000000003" type="appl" />
         <tli id="T364" time="175.70300000000003" type="appl" />
         <tli id="T365" time="176.32800000000003" type="appl" />
         <tli id="T366" time="176.918" type="appl" />
         <tli id="T367" time="176.95300000000003" type="appl" />
         <tli id="T368" time="177.64465399245967" />
         <tli id="T369" time="177.858" type="appl" />
         <tli id="T370" time="178.358" type="appl" />
         <tli id="T371" time="178.51800000000003" type="appl" />
         <tli id="T372" time="178.62466666666666" type="appl" />
         <tli id="T373" time="178.89133333333336" type="appl" />
         <tli id="T374" time="179.118" type="appl" />
         <tli id="T375" time="179.15800000000002" type="appl" />
         <tli id="T376" time="179.71800000000002" type="appl" />
         <tli id="T377" time="180.27800000000002" type="appl" />
         <tli id="T378" time="180.83800000000002" type="appl" />
         <tli id="T379" time="181.39800000000002" type="appl" />
         <tli id="T380" time="181.95800000000003" type="appl" />
         <tli id="T381" time="182.51799999999997" type="appl" />
         <tli id="T382" time="183.07799999999997" type="appl" />
         <tli id="T383" time="183.63799999999998" type="appl" />
         <tli id="T384" time="184.19799999999998" type="appl" />
         <tli id="T385" time="184.90466563045595" />
         <tli id="T386" time="186.11534031184277" />
         <tli id="T387" time="186.74900000000002" type="appl" />
         <tli id="T388" time="187.51600000000002" type="appl" />
         <tli id="T389" time="188.17000000000002" type="appl" />
         <tli id="T390" time="188.28300000000002" type="appl" />
         <tli id="T391" time="189.05" type="appl" />
         <tli id="T392" time="189.09000000000003" type="appl" />
         <tli id="T393" time="189.67736449488137" />
         <tli id="T394" time="191.156" type="appl" />
         <tli id="T395" time="191.45" type="appl" />
         <tli id="T396" time="192.3272" type="appl" />
         <tli id="T397" time="193.20440000000002" type="appl" />
         <tli id="T398" time="194.08159999999998" type="appl" />
         <tli id="T399" time="194.9588" type="appl" />
         <tli id="T400" time="195.836" type="appl" />
         <tli id="T401" time="196.78199999999998" type="appl" />
         <tli id="T402" time="197.67899999999997" type="appl" />
         <tli id="T403" time="198.57599999999996" type="appl" />
         <tli id="T404" time="199.473" type="appl" />
         <tli id="T405" time="200.37" type="appl" />
         <tli id="T406" time="200.41000000000003" type="appl" />
         <tli id="T407" time="201.223" type="appl" />
         <tli id="T408" time="202.036" type="appl" />
         <tli id="T409" time="202.849" type="appl" />
         <tli id="T410" time="203.66199999999998" type="appl" />
         <tli id="T411" time="203.702" type="appl" />
         <tli id="T412" time="204.336" type="appl" />
         <tli id="T413" time="204.97000000000003" type="appl" />
         <tli id="T414" time="205.68533333333335" type="appl" />
         <tli id="T415" time="206.40066666666667" type="appl" />
         <tli id="T416" time="207.11599999999999" type="appl" />
         <tli id="T417" time="207.142" type="appl" />
         <tli id="T418" time="207.789" type="appl" />
         <tli id="T419" time="208.73066002698198" />
         <tli id="T420" time="209.464" type="appl" />
         <tli id="T421" time="210.47050000000002" type="appl" />
         <tli id="T422" time="211.47700000000003" type="appl" />
         <tli id="T423" time="212.4835" type="appl" />
         <tli id="T424" time="213.49" type="appl" />
         <tli id="T425" time="213.50400000000002" type="appl" />
         <tli id="T426" time="214.368" type="appl" />
         <tli id="T427" time="215.23200000000003" type="appl" />
         <tli id="T428" time="216.096" type="appl" />
         <tli id="T429" time="216.96000000000004" type="appl" />
         <tli id="T430" time="217.824" type="appl" />
         <tli id="T431" time="217.89" type="appl" />
         <tli id="T432" time="218.50799999999998" type="appl" />
         <tli id="T433" time="219.12599999999998" type="appl" />
         <tli id="T434" time="219.74399999999997" type="appl" />
         <tli id="T435" time="220.36200000000002" type="appl" />
         <tli id="T436" time="220.98000000000002" type="appl" />
         <tli id="T437" time="221.598" type="appl" />
         <tli id="T438" time="221.61" type="appl" />
         <tli id="T439" time="222.06333333333333" type="appl" />
         <tli id="T440" time="222.5166666666667" type="appl" />
         <tli id="T441" time="222.97000000000003" type="appl" />
         <tli id="T442" time="222.98399999999998" type="appl" />
         <tli id="T443" time="223.44279999999998" type="appl" />
         <tli id="T444" time="223.90159999999997" type="appl" />
         <tli id="T445" time="224.36040000000003" type="appl" />
         <tli id="T446" time="224.81920000000002" type="appl" />
         <tli id="T447" time="225.27800000000002" type="appl" />
         <tli id="T448" time="226.038" type="appl" />
         <tli id="T449" time="227.0845" type="appl" />
         <tli id="T450" time="228.13100000000003" type="appl" />
         <tli id="T451" time="229.1775" type="appl" />
         <tli id="T452" time="230.224" type="appl" />
         <tli id="T453" time="231.156" type="appl" />
         <tli id="T454" time="232.06240000000003" type="appl" />
         <tli id="T455" time="232.9688" type="appl" />
         <tli id="T456" time="233.8752" type="appl" />
         <tli id="T457" time="234.78160000000003" type="appl" />
         <tli id="T458" time="235.68800000000005" type="appl" />
         <tli id="T459" time="236.5944" type="appl" />
         <tli id="T460" time="237.50080000000003" type="appl" />
         <tli id="T461" time="238.40720000000005" type="appl" />
         <tli id="T462" time="239.3136" type="appl" />
         <tli id="T463" time="240.22000000000003" type="appl" />
         <tli id="T464" time="240.98399999999998" type="appl" />
         <tli id="T465" time="242.1762857142857" type="appl" />
         <tli id="T466" time="243.36857142857139" type="appl" />
         <tli id="T467" time="244.56085714285712" type="appl" />
         <tli id="T468" time="245.75314285714285" type="appl" />
         <tli id="T469" time="246.94542857142858" type="appl" />
         <tli id="T470" time="248.13771428571425" type="appl" />
         <tli id="T471" time="249.32999999999998" type="appl" />
         <tli id="T472" time="249.476" type="appl" />
         <tli id="T473" time="250.14800000000002" type="appl" />
         <tli id="T474" time="250.82" type="appl" />
         <tli id="T475" time="251.49200000000002" type="appl" />
         <tli id="T476" time="252.164" type="appl" />
         <tli id="T477" time="252.836" type="appl" />
         <tli id="T478" time="253.50799999999998" type="appl" />
         <tli id="T479" time="254.18" type="appl" />
         <tli id="T480" time="254.85199999999998" type="appl" />
         <tli id="T481" time="255.524" type="appl" />
         <tli id="T482" time="256.19599999999997" type="appl" />
         <tli id="T483" time="256.868" type="appl" />
         <tli id="T484" time="257.53999999999996" type="appl" />
         <tli id="T485" time="258.212" type="appl" />
         <tli id="T486" time="258.88399999999996" type="appl" />
         <tli id="T487" time="259.556" type="appl" />
         <tli id="T488" time="260.836" type="appl" />
         <tli id="T489" time="261.9108" type="appl" />
         <tli id="T490" time="262.9856" type="appl" />
         <tli id="T491" time="264.0604" type="appl" />
         <tli id="T492" time="265.1352" type="appl" />
         <tli id="T493" time="266.21" type="appl" />
         <tli id="T494" time="266.61" type="appl" />
         <tli id="T495" time="267.12416666666667" type="appl" />
         <tli id="T496" time="267.6383333333334" type="appl" />
         <tli id="T497" time="268.15250000000003" type="appl" />
         <tli id="T498" time="268.6666666666667" type="appl" />
         <tli id="T499" time="269.18083333333334" type="appl" />
         <tli id="T500" time="269.695" type="appl" />
         <tli id="T501" time="270.2091666666667" type="appl" />
         <tli id="T502" time="270.72333333333336" type="appl" />
         <tli id="T503" time="271.2375" type="appl" />
         <tli id="T504" time="271.7516666666667" type="appl" />
         <tli id="T505" time="272.2658333333334" type="appl" />
         <tli id="T506" time="272.66386651853026" />
         <tli id="T507" time="274.934" type="appl" />
         <tli id="T508" time="275.564" type="appl" />
         <tli id="T509" time="276.194" type="appl" />
         <tli id="T510" time="276.824" type="appl" />
         <tli id="T511" time="277.454" type="appl" />
         <tli id="T512" time="277.854" type="appl" />
         <tli id="T513" time="278.2695" type="appl" />
         <tli id="T514" time="278.685" type="appl" />
         <tli id="T515" time="279.1005" type="appl" />
         <tli id="T516" time="279.516" type="appl" />
         <tli id="T517" time="279.93149999999997" type="appl" />
         <tli id="T518" time="280.347" type="appl" />
         <tli id="T519" time="280.7625" type="appl" />
         <tli id="T520" time="281.178" type="appl" />
         <tli id="T521" time="281.5935" type="appl" />
         <tli id="T522" time="282.009" type="appl" />
         <tli id="T523" time="282.4245" type="appl" />
         <tli id="T524" time="282.84" type="appl" />
         <tli id="T525" time="283.2555" type="appl" />
         <tli id="T526" time="283.671" type="appl" />
         <tli id="T527" time="284.0865" type="appl" />
         <tli id="T528" time="284.54199399430723" />
         <tli id="T529" time="285.062" type="appl" />
         <tli id="T530" time="285.622" type="appl" />
         <tli id="T531" time="286.182" type="appl" />
         <tli id="T532" time="286.742" type="appl" />
         <tli id="T533" time="287.302" type="appl" />
         <tli id="T534" time="287.356" type="appl" />
         <tli id="T535" time="287.85166666666663" type="appl" />
         <tli id="T536" time="288.3473333333333" type="appl" />
         <tli id="T537" time="288.843" type="appl" />
         <tli id="T538" time="289.33866666666665" type="appl" />
         <tli id="T539" time="289.8343333333333" type="appl" />
         <tli id="T540" time="290.33" type="appl" />
         <tli id="T541" time="291.33" type="appl" />
         <tli id="T542" time="292.022" type="appl" />
         <tli id="T543" time="292.714" type="appl" />
         <tli id="T544" time="293.406" type="appl" />
         <tli id="T545" time="294.098" type="appl" />
         <tli id="T546" time="294.78999999999996" type="appl" />
         <tli id="T547" time="295.48199999999997" type="appl" />
         <tli id="T548" time="296.174" type="appl" />
         <tli id="T549" time="296.866" type="appl" />
         <tli id="T550" time="297.558" type="appl" />
         <tli id="T551" time="297.85" type="appl" />
         <tli id="T552" time="298.5997142857143" type="appl" />
         <tli id="T553" time="299.3494285714286" type="appl" />
         <tli id="T554" time="300.09914285714285" type="appl" />
         <tli id="T555" time="300.8488571428572" type="appl" />
         <tli id="T556" time="301.59857142857146" type="appl" />
         <tli id="T557" time="302.34828571428574" type="appl" />
         <tli id="T558" time="303.098" type="appl" />
         <tli id="T559" time="303.81" type="appl" />
         <tli id="T560" time="304.4714" type="appl" />
         <tli id="T561" time="305.1328" type="appl" />
         <tli id="T562" time="305.7942" type="appl" />
         <tli id="T563" time="306.4556" type="appl" />
         <tli id="T564" time="307.117" type="appl" />
         <tli id="T565" time="307.7784" type="appl" />
         <tli id="T566" time="308.4398" type="appl" />
         <tli id="T567" time="309.1012" type="appl" />
         <tli id="T568" time="309.76259999999996" type="appl" />
         <tli id="T569" time="310.27712522815654" />
         <tli id="T570" time="311.036" type="appl" />
         <tli id="T571" time="313.358" />
         <tli id="T257" time="316.447" type="appl" />
         <tli id="T573" time="317.06899999999996" type="appl" />
         <tli id="T574" time="317.69100000000003" type="appl" />
         <tli id="T575" time="318.313" type="appl" />
         <tli id="T576" time="318.935" type="appl" />
         <tli id="T577" time="319.557" type="appl" />
         <tli id="T578" time="320.179" type="appl" />
         <tli id="T579" time="320.80100000000004" type="appl" />
         <tli id="T580" time="321.423" type="appl" />
         <tli id="T581" time="322.04499999999996" type="appl" />
         <tli id="T582" time="322.66700000000003" type="appl" />
         <tli id="T583" time="322.977" type="appl" />
         <tli id="T584" time="323.68100000000004" type="appl" />
         <tli id="T585" time="324.385" type="appl" />
         <tli id="T586" time="325.089" type="appl" />
         <tli id="T587" time="325.793" type="appl" />
         <tli id="T588" time="326.49699999999996" type="appl" />
         <tli id="T589" time="326.683" type="appl" />
         <tli id="T590" time="329.2203333333333" type="appl" />
         <tli id="T591" time="331.75766666666664" type="appl" />
         <tli id="T592" time="334.29499999999996" type="appl" />
         <tli id="T593" time="336.83233333333334" type="appl" />
         <tli id="T594" time="339.3696666666667" type="appl" />
         <tli id="T595" time="341.90700000000004" type="appl" />
         <tli id="T596" time="341.927" type="appl" />
         <tli id="T597" time="342.48042857142855" type="appl" />
         <tli id="T598" time="343.03385714285713" type="appl" />
         <tli id="T599" time="343.5872857142857" type="appl" />
         <tli id="T600" time="344.14071428571424" type="appl" />
         <tli id="T601" time="344.6941428571429" type="appl" />
         <tli id="T602" time="345.2475714285714" type="appl" />
         <tli id="T603" time="345.801" type="appl" />
         <tli id="T604" time="346.0231614157607" />
         <tli id="T605" time="346.68033333333335" type="appl" />
         <tli id="T606" time="347.47366666666665" type="appl" />
         <tli id="T607" time="348.26700000000005" type="appl" />
         <tli id="T608" time="349.06033333333335" type="appl" />
         <tli id="T609" time="349.85366666666664" type="appl" />
         <tli id="T610" time="350.8764851202285" />
         <tli id="T611" time="351.73699999999997" type="appl" />
         <tli id="T612" time="352.1886666666667" type="appl" />
         <tli id="T613" time="352.6403333333333" type="appl" />
         <tli id="T614" time="353.092" type="appl" />
         <tli id="T615" time="353.5436666666667" type="appl" />
         <tli id="T616" time="353.9953333333333" type="appl" />
         <tli id="T617" time="354.447" type="appl" />
         <tli id="T618" time="354.597" type="appl" />
         <tli id="T619" time="355.27712499999996" type="appl" />
         <tli id="T620" time="355.95725000000004" type="appl" />
         <tli id="T621" time="356.637375" type="appl" />
         <tli id="T622" time="357.3175" type="appl" />
         <tli id="T623" time="357.99762499999997" type="appl" />
         <tli id="T624" time="358.67774999999995" type="appl" />
         <tli id="T625" time="359.35787500000004" type="appl" />
         <tli id="T626" time="360.1964666296326" />
         <tli id="T627" time="361.52700000000004" type="appl" />
         <tli id="T628" time="361.90144444444445" type="appl" />
         <tli id="T629" time="362.27588888888886" type="appl" />
         <tli id="T630" time="362.6503333333334" type="appl" />
         <tli id="T631" time="363.0247777777778" type="appl" />
         <tli id="T632" time="363.3992222222222" type="appl" />
         <tli id="T633" time="363.7736666666667" type="appl" />
         <tli id="T634" time="364.1481111111111" type="appl" />
         <tli id="T635" time="364.5225555555555" type="appl" />
         <tli id="T636" time="364.89700000000005" type="appl" />
         <tli id="T637" time="365.27144444444446" type="appl" />
         <tli id="T638" time="365.64588888888886" type="appl" />
         <tli id="T639" time="366.0203333333334" type="appl" />
         <tli id="T640" time="366.3947777777778" type="appl" />
         <tli id="T641" time="366.7692222222222" type="appl" />
         <tli id="T642" time="367.1436666666667" type="appl" />
         <tli id="T643" time="367.5181111111111" type="appl" />
         <tli id="T644" time="367.89255555555553" type="appl" />
         <tli id="T645" time="368.2097840647567" />
         <tli id="T646" time="369.51700000000005" type="appl" />
         <tli id="T647" time="370.08418181818183" type="appl" />
         <tli id="T648" time="370.6513636363636" type="appl" />
         <tli id="T649" time="371.2185454545455" type="appl" />
         <tli id="T650" time="371.7857272727273" type="appl" />
         <tli id="T651" time="372.35290909090907" type="appl" />
         <tli id="T652" time="372.92009090909096" type="appl" />
         <tli id="T653" time="373.48727272727274" type="appl" />
         <tli id="T654" time="374.0544545454545" type="appl" />
         <tli id="T655" time="374.6216363636364" type="appl" />
         <tli id="T656" time="375.1888181818182" type="appl" />
         <tli id="T657" time="375.756" type="appl" />
         <tli id="T658" time="375.75699999999995" type="appl" />
         <tli id="T659" time="376.3803333333333" type="appl" />
         <tli id="T660" time="377.0036666666666" type="appl" />
         <tli id="T661" time="377.62699999999995" type="appl" />
         <tli id="T662" time="378.2503333333333" type="appl" />
         <tli id="T663" time="378.8736666666666" type="appl" />
         <tli id="T664" time="379.8364276644711" />
         <tli id="T665" time="381.35699999999997" type="appl" />
         <tli id="T666" time="381.9053333333334" type="appl" />
         <tli id="T667" time="382.45366666666666" type="appl" />
         <tli id="T668" time="383.00199999999995" type="appl" />
         <tli id="T669" time="383.55033333333336" type="appl" />
         <tli id="T670" time="384.09866666666665" type="appl" />
         <tli id="T671" time="384.64700000000005" type="appl" />
         <tli id="T672" time="384.847" type="appl" />
         <tli id="T673" time="385.2792222222222" type="appl" />
         <tli id="T674" time="385.7114444444444" type="appl" />
         <tli id="T675" time="386.1436666666667" type="appl" />
         <tli id="T676" time="386.5758888888889" type="appl" />
         <tli id="T677" time="387.00811111111113" type="appl" />
         <tli id="T678" time="387.44033333333334" type="appl" />
         <tli id="T679" time="387.87255555555555" type="appl" />
         <tli id="T680" time="388.087" type="appl" />
         <tli id="T681" time="388.30477777777776" type="appl" />
         <tli id="T682" time="388.55325000000005" type="appl" />
         <tli id="T683" time="388.73699999999997" type="appl" />
         <tli id="T684" time="389.0195" type="appl" />
         <tli id="T685" time="389.48575000000005" type="appl" />
         <tli id="T686" time="389.952" type="appl" />
         <tli id="T687" time="390.41824999999994" type="appl" />
         <tli id="T688" time="390.8845" type="appl" />
         <tli id="T689" time="391.35074999999995" type="appl" />
         <tli id="T690" time="391.817" type="appl" />
         <tli id="T691" time="391.837" type="appl" />
         <tli id="T692" time="392.39700000000005" type="appl" />
         <tli id="T693" time="392.957" type="appl" />
         <tli id="T694" time="393.51700000000005" type="appl" />
         <tli id="T695" time="394.077" type="appl" />
         <tli id="T696" time="394.087" type="appl" />
         <tli id="T697" time="394.37015789473685" type="appl" />
         <tli id="T698" time="394.6533157894737" type="appl" />
         <tli id="T699" time="394.93647368421057" type="appl" />
         <tli id="T700" time="395.2196315789474" type="appl" />
         <tli id="T701" time="395.5027894736842" type="appl" />
         <tli id="T702" time="395.78594736842103" type="appl" />
         <tli id="T703" time="396.0691052631579" type="appl" />
         <tli id="T704" time="396.35226315789475" type="appl" />
         <tli id="T705" time="396.6354210526316" type="appl" />
         <tli id="T706" time="396.9185789473685" type="appl" />
         <tli id="T707" time="397.20173684210533" type="appl" />
         <tli id="T708" time="397.4848947368421" type="appl" />
         <tli id="T709" time="397.76805263157894" type="appl" />
         <tli id="T710" time="398.0512105263158" type="appl" />
         <tli id="T711" time="398.33436842105266" type="appl" />
         <tli id="T712" time="398.6175263157895" type="appl" />
         <tli id="T713" time="398.9006842105264" type="appl" />
         <tli id="T714" time="399.1838421052631" type="appl" />
         <tli id="T715" time="399.467" type="appl" />
         <tli id="T716" time="399.91033333333337" type="appl" />
         <tli id="T717" time="400.35366666666664" type="appl" />
         <tli id="T718" time="400.797" type="appl" />
         <tli id="T719" time="402.31638306483615" />
         <tli id="T720" time="402.73199999999997" type="appl" />
         <tli id="T721" time="403.28700000000003" type="appl" />
         <tli id="T722" time="403.842" type="appl" />
         <tli id="T723" time="404.39700000000005" type="appl" />
         <tli id="T724" time="406.88699999999994" type="appl" />
         <tli id="T725" time="407.41099999999994" type="appl" />
         <tli id="T726" time="407.93499999999995" type="appl" />
         <tli id="T727" time="408.45899999999995" type="appl" />
         <tli id="T728" time="408.98299999999995" type="appl" />
         <tli id="T729" time="409.50699999999995" type="appl" />
         <tli id="T730" time="409.543656755368" />
         <tli id="T731" time="409.8328333333334" type="appl" />
         <tli id="T732" time="410.1286666666666" type="appl" />
         <tli id="T733" time="410.42449999999997" type="appl" />
         <tli id="T734" time="410.7203333333333" type="appl" />
         <tli id="T735" time="411.01616666666666" type="appl" />
         <tli id="T736" time="411.312" type="appl" />
         <tli id="T737" time="411.60783333333336" type="appl" />
         <tli id="T738" time="411.9036666666667" type="appl" />
         <tli id="T739" time="412.19950000000006" type="appl" />
         <tli id="T740" time="412.4953333333333" type="appl" />
         <tli id="T741" time="412.79116666666664" type="appl" />
         <tli id="T742" time="413.087" type="appl" />
         <tli id="T743" time="413.35699999999997" type="appl" />
         <tli id="T744" time="414.0178461538461" type="appl" />
         <tli id="T745" time="414.67869230769236" type="appl" />
         <tli id="T746" time="415.3395384615385" type="appl" />
         <tli id="T747" time="416.00038461538463" type="appl" />
         <tli id="T748" time="416.66123076923077" type="appl" />
         <tli id="T749" time="417.3220769230769" type="appl" />
         <tli id="T750" time="417.98292307692304" type="appl" />
         <tli id="T751" time="418.6437692307692" type="appl" />
         <tli id="T752" time="419.30461538461543" type="appl" />
         <tli id="T753" time="419.96546153846157" type="appl" />
         <tli id="T754" time="420.6263076923077" type="appl" />
         <tli id="T755" time="421.28715384615384" type="appl" />
         <tli id="T756" time="422.14967704944047" />
         <tli id="T757" time="422.437" type="appl" />
         <tli id="T758" time="422.83449999999993" type="appl" />
         <tli id="T759" time="423.23199999999997" type="appl" />
         <tli id="T760" time="423.6295" type="appl" />
         <tli id="T761" time="424.02700000000004" type="appl" />
         <tli id="T762" time="424.42449999999997" type="appl" />
         <tli id="T763" time="424.822" type="appl" />
         <tli id="T764" time="425.21950000000004" type="appl" />
         <tli id="T765" time="425.61699999999996" type="appl" />
         <tli id="T766" time="426.0145" type="appl" />
         <tli id="T767" time="426.41200000000003" type="appl" />
         <tli id="T768" time="426.80950000000007" type="appl" />
         <tli id="T769" time="427.207" type="appl" />
         <tli id="T770" time="427.337" type="appl" />
         <tli id="T771" time="427.76324999999997" type="appl" />
         <tli id="T772" time="428.18950000000007" type="appl" />
         <tli id="T773" time="428.61575000000005" type="appl" />
         <tli id="T774" time="429.04200000000003" type="appl" />
         <tli id="T775" time="429.46825" type="appl" />
         <tli id="T776" time="429.8945" type="appl" />
         <tli id="T777" time="430.32075" type="appl" />
         <tli id="T778" time="430.74699999999996" type="appl" />
         <tli id="T779" time="431.17325000000005" type="appl" />
         <tli id="T780" time="431.59950000000003" type="appl" />
         <tli id="T781" time="432.02575" type="appl" />
         <tli id="T782" time="432.452" type="appl" />
         <tli id="T783" time="432.87825" type="appl" />
         <tli id="T784" time="433.30449999999996" type="appl" />
         <tli id="T785" time="433.73075000000006" type="appl" />
         <tli id="T786" time="434.15700000000004" type="appl" />
         <tli id="T787" time="434.6763188635822" />
         <tli id="T788" time="435.1395" type="appl" />
         <tli id="T789" time="435.76199999999994" type="appl" />
         <tli id="T790" time="436.3845" type="appl" />
         <tli id="T791" time="437.00699999999995" type="appl" />
         <tli id="T792" time="437.047" type="appl" />
         <tli id="T793" time="437.515" type="appl" />
         <tli id="T794" time="437.98299999999995" type="appl" />
         <tli id="T795" time="438.451" type="appl" />
         <tli id="T796" time="438.919" type="appl" />
         <tli id="T797" time="439.38699999999994" type="appl" />
         <tli id="T798" time="440.39700000000005" type="appl" />
         <tli id="T799" time="440.86699999999996" type="appl" />
         <tli id="T800" time="441.337" type="appl" />
         <tli id="T801" time="441.807" type="appl" />
         <tli id="T802" time="442.27700000000004" type="appl" />
         <tli id="T803" time="442.8985714285715" type="appl" />
         <tli id="T804" time="443.5201428571429" type="appl" />
         <tli id="T805" time="444.14171428571433" type="appl" />
         <tli id="T806" time="444.76328571428576" type="appl" />
         <tli id="T807" time="445.3848571428572" type="appl" />
         <tli id="T808" time="446.0064285714286" type="appl" />
         <tli id="T809" time="446.62800000000004" type="appl" />
         <tli id="T810" time="446.827" type="appl" />
         <tli id="T811" time="447.3281111111112" type="appl" />
         <tli id="T812" time="447.82922222222226" type="appl" />
         <tli id="T813" time="448.33033333333333" type="appl" />
         <tli id="T814" time="448.8314444444445" type="appl" />
         <tli id="T815" time="449.3325555555556" type="appl" />
         <tli id="T816" time="449.83366666666666" type="appl" />
         <tli id="T817" time="450.33477777777784" type="appl" />
         <tli id="T818" time="450.8358888888889" type="appl" />
         <tli id="T819" time="451.337" type="appl" />
         <tli id="T820" time="451.837" type="appl" />
         <tli id="T821" time="452.41609090909094" type="appl" />
         <tli id="T822" time="452.9951818181819" type="appl" />
         <tli id="T823" time="453.5742727272727" type="appl" />
         <tli id="T824" time="454.1533636363637" type="appl" />
         <tli id="T825" time="454.7324545454545" type="appl" />
         <tli id="T826" time="455.31154545454547" type="appl" />
         <tli id="T827" time="455.8906363636364" type="appl" />
         <tli id="T828" time="456.46972727272725" type="appl" />
         <tli id="T829" time="457.0488181818182" type="appl" />
         <tli id="T830" time="457.62790909090916" type="appl" />
         <tli id="T831" time="458.4229384175859" />
         <tli id="T832" time="458.62699999999995" type="appl" />
         <tli id="T833" time="459.16033333333337" type="appl" />
         <tli id="T834" time="459.6936666666667" type="appl" />
         <tli id="T835" time="460.227" type="appl" />
         <tli id="T836" time="460.7603333333333" type="appl" />
         <tli id="T837" time="461.2936666666667" type="appl" />
         <tli id="T838" time="461.827" type="appl" />
         <tli id="T839" time="462.3603333333333" type="appl" />
         <tli id="T840" time="462.8936666666666" type="appl" />
         <tli id="T841" time="463.427" type="appl" />
         <tli id="T842" time="463.63699999999994" type="appl" />
         <tli id="T843" time="464.38699999999994" type="appl" />
         <tli id="T844" time="465.13699999999994" type="appl" />
         <tli id="T845" time="465.9603453385706" />
         <tli id="T846" time="466.077" type="appl" />
         <tli id="T847" time="466.6515454545455" type="appl" />
         <tli id="T848" time="467.2260909090909" type="appl" />
         <tli id="T849" time="467.8006363636364" type="appl" />
         <tli id="T850" time="468.3751818181819" type="appl" />
         <tli id="T851" time="468.9497272727273" type="appl" />
         <tli id="T852" time="469.5242727272728" type="appl" />
         <tli id="T853" time="470.09881818181816" type="appl" />
         <tli id="T854" time="470.67336363636366" type="appl" />
         <tli id="T855" time="471.24790909090916" type="appl" />
         <tli id="T856" time="471.82245454545455" type="appl" />
         <tli id="T857" time="472.39700000000005" type="appl" />
         <tli id="T858" time="472.85699999999997" type="appl" />
         <tli id="T859" time="473.278" type="appl" />
         <tli id="T860" time="473.69899999999996" type="appl" />
         <tli id="T861" time="474.12" type="appl" />
         <tli id="T862" time="474.54099999999994" type="appl" />
         <tli id="T863" time="474.962" type="appl" />
         <tli id="T864" time="475.38300000000004" type="appl" />
         <tli id="T865" time="475.804" type="appl" />
         <tli id="T866" time="476.225" type="appl" />
         <tli id="T867" time="476.64599999999996" type="appl" />
         <tli id="T868" time="477.16290123799695" />
         <tli id="T869" time="477.61699999999996" type="appl" />
         <tli id="T870" time="478.0153333333333" type="appl" />
         <tli id="T871" time="478.4136666666667" type="appl" />
         <tli id="T872" time="478.812" type="appl" />
         <tli id="T873" time="479.2103333333333" type="appl" />
         <tli id="T874" time="479.60866666666664" type="appl" />
         <tli id="T875" time="480.0803433665359" />
         <tli id="T876" time="480.7203333333333" type="appl" />
         <tli id="T877" time="481.4336666666667" type="appl" />
         <tli id="T878" time="482.14700000000005" type="appl" />
         <tli id="T879" time="482.65700000000004" type="appl" />
         <tli id="T880" time="483.15887499999997" type="appl" />
         <tli id="T881" time="483.66075" type="appl" />
         <tli id="T882" time="484.16262500000005" type="appl" />
         <tli id="T883" time="484.6645" type="appl" />
         <tli id="T884" time="485.166375" type="appl" />
         <tli id="T885" time="485.66824999999994" type="appl" />
         <tli id="T886" time="486.170125" type="appl" />
         <tli id="T887" time="486.672" type="appl" />
         <tli id="T888" time="487.17387499999995" type="appl" />
         <tli id="T889" time="487.67575" type="appl" />
         <tli id="T890" time="488.17762500000003" type="appl" />
         <tli id="T891" time="488.67949999999996" type="appl" />
         <tli id="T892" time="489.181375" type="appl" />
         <tli id="T893" time="489.68324999999993" type="appl" />
         <tli id="T894" time="490.18512499999997" type="appl" />
         <tli id="T895" time="490.687" type="appl" />
         <tli id="T896" time="491.307" type="appl" />
         <tli id="T897" time="492.37199999999996" type="appl" />
         <tli id="T898" time="493.437" type="appl" />
         <tli id="T899" time="494.50199999999995" type="appl" />
         <tli id="T900" time="495.567" type="appl" />
         <tli id="T901" time="496.63199999999995" type="appl" />
         <tli id="T902" time="497.697" type="appl" />
         <tli id="T903" time="498.927" type="appl" />
         <tli id="T904" time="499.45477777777774" type="appl" />
         <tli id="T905" time="499.98255555555556" type="appl" />
         <tli id="T906" time="500.5103333333333" type="appl" />
         <tli id="T907" time="501.0381111111111" type="appl" />
         <tli id="T908" time="501.56588888888894" type="appl" />
         <tli id="T909" time="502.09366666666665" type="appl" />
         <tli id="T910" time="502.6214444444445" type="appl" />
         <tli id="T911" time="503.1492222222222" type="appl" />
         <tli id="T912" time="503.677" type="appl" />
         <tli id="T913" time="503.687" type="appl" />
         <tli id="T914" time="504.351" type="appl" />
         <tli id="T915" time="505.015" type="appl" />
         <tli id="T916" time="505.679" type="appl" />
         <tli id="T917" time="506.34299999999996" type="appl" />
         <tli id="T918" time="507.00699999999995" type="appl" />
         <tli id="T919" time="507.28700000000003" type="appl" />
         <tli id="T920" time="507.74157142857143" type="appl" />
         <tli id="T921" time="508.19614285714283" type="appl" />
         <tli id="T922" time="508.65071428571434" type="appl" />
         <tli id="T923" time="509.10528571428574" type="appl" />
         <tli id="T924" time="509.55985714285714" type="appl" />
         <tli id="T925" time="510.01442857142854" type="appl" />
         <tli id="T926" time="510.34950206332826" />
         <tli id="T927" time="510.85699999999997" type="appl" />
         <tli id="T928" time="511.77200000000005" type="appl" />
         <tli id="T929" time="512.687" type="appl" />
         <tli id="T930" time="513.602" type="appl" />
         <tli id="T931" time="514.5169999999999" type="appl" />
         <tli id="T932" time="515.432" type="appl" />
         <tli id="T933" time="516.347" type="appl" />
         <tli id="T934" time="517.262" type="appl" />
         <tli id="T935" time="518.177" type="appl" />
         <tli id="T936" time="519.092" type="appl" />
         <tli id="T937" time="520.1228160066662" />
         <tli id="T938" time="520.4024545454545" type="appl" />
         <tli id="T939" time="520.7979090909091" type="appl" />
         <tli id="T940" time="521.1933636363636" type="appl" />
         <tli id="T941" time="521.5888181818182" type="appl" />
         <tli id="T942" time="521.9842727272727" type="appl" />
         <tli id="T943" time="522.3797272727272" type="appl" />
         <tli id="T944" time="522.7751818181819" type="appl" />
         <tli id="T945" time="523.1706363636364" type="appl" />
         <tli id="T946" time="523.5660909090909" type="appl" />
         <tli id="T947" time="523.9615454545454" type="appl" />
         <tli id="T948" time="524.4436668092128" />
         <tli id="T949" time="524.9555714285714" type="appl" />
         <tli id="T950" time="525.5541428571429" type="appl" />
         <tli id="T951" time="526.1527142857143" type="appl" />
         <tli id="T952" time="526.7512857142857" type="appl" />
         <tli id="T953" time="527.3498571428571" type="appl" />
         <tli id="T954" time="527.9484285714286" type="appl" />
         <tli id="T955" time="528.547" type="appl" />
         <tli id="T956" time="528.797" type="appl" />
         <tli id="T957" time="529.287" type="appl" />
         <tli id="T958" time="529.777" type="appl" />
         <tli id="T959" time="530.2669999999999" type="appl" />
         <tli id="T960" time="530.757" type="appl" />
         <tli id="T961" time="531.247" type="appl" />
         <tli id="T962" time="531.547" type="appl" />
         <tli id="T963" time="532.467" type="appl" />
         <tli id="T964" time="533.387" type="appl" />
         <tli id="T965" time="534.077" type="appl" />
         <tli id="T966" time="534.558125" type="appl" />
         <tli id="T967" time="535.03925" type="appl" />
         <tli id="T968" time="535.5203750000001" type="appl" />
         <tli id="T969" time="536.0015" type="appl" />
         <tli id="T970" time="536.482625" type="appl" />
         <tli id="T971" time="536.96375" type="appl" />
         <tli id="T972" time="537.444875" type="appl" />
         <tli id="T973" time="537.926" type="appl" />
         <tli id="T974" time="539.587" type="appl" />
         <tli id="T975" time="539.9836666666666" type="appl" />
         <tli id="T976" time="540.3803333333334" type="appl" />
         <tli id="T977" time="540.777" type="appl" />
         <tli id="T978" time="541.1736666666667" type="appl" />
         <tli id="T979" time="541.5703333333333" type="appl" />
         <tli id="T980" time="541.967" type="appl" />
         <tli id="T981" time="542.037" type="appl" />
         <tli id="T982" time="542.5045" type="appl" />
         <tli id="T983" time="542.972" type="appl" />
         <tli id="T984" time="543.4395" type="appl" />
         <tli id="T985" time="543.907" type="appl" />
         <tli id="T986" time="544.3745" type="appl" />
         <tli id="T987" time="544.842" type="appl" />
         <tli id="T988" time="545.3095" type="appl" />
         <tli id="T989" time="545.9160981668122" />
         <tli id="T990" time="546.387" type="appl" />
         <tli id="T991" time="546.8303333333333" type="appl" />
         <tli id="T992" time="547.2736666666666" type="appl" />
         <tli id="T993" time="547.717" type="appl" />
         <tli id="T994" time="548.1603333333333" type="appl" />
         <tli id="T995" time="548.6036666666666" type="appl" />
         <tli id="T996" time="549.047" type="appl" />
         <tli id="T997" time="549.4903333333333" type="appl" />
         <tli id="T998" time="549.9336666666667" type="appl" />
         <tli id="T999" time="550.377" type="appl" />
         <tli id="T1000" time="550.8203333333333" type="appl" />
         <tli id="T1001" time="551.2636666666666" type="appl" />
         <tli id="T1002" time="551.707" type="appl" />
         <tli id="T1003" time="552.1503333333333" type="appl" />
         <tli id="T1004" time="552.5936666666666" type="appl" />
         <tli id="T1005" time="553.037" type="appl" />
         <tli id="T1006" time="553.4803333333333" type="appl" />
         <tli id="T1007" time="553.9236666666667" type="appl" />
         <tli id="T1008" time="554.367" type="appl" />
         <tli id="T1009" time="554.857" type="appl" />
         <tli id="T1010" time="555.342" type="appl" />
         <tli id="T1011" time="555.827" type="appl" />
         <tli id="T1012" time="556.312" type="appl" />
         <tli id="T1013" time="556.797" type="appl" />
         <tli id="T1014" time="558.627" type="appl" />
         <tli id="T1015" time="559.047" type="appl" />
         <tli id="T1016" time="559.467" type="appl" />
         <tli id="T1017" time="559.887" type="appl" />
         <tli id="T1018" time="560.307" type="appl" />
         <tli id="T1019" time="560.727" type="appl" />
         <tli id="T1020" time="561.1469999999999" type="appl" />
         <tli id="T1021" time="561.567" type="appl" />
         <tli id="T1022" time="561.987" type="appl" />
         <tli id="T1023" time="562.407" type="appl" />
         <tli id="T1024" time="562.827" type="appl" />
         <tli id="T1025" time="563.247" type="appl" />
         <tli id="T1026" time="564.257" type="appl" />
         <tli id="T1027" time="564.822" type="appl" />
         <tli id="T1028" time="565.387" type="appl" />
         <tli id="T1029" time="565.952" type="appl" />
         <tli id="T1030" time="566.5169999999999" type="appl" />
         <tli id="T1031" time="567.082" type="appl" />
         <tli id="T1032" time="567.647" type="appl" />
         <tli id="T1033" time="568.212" type="appl" />
         <tli id="T1034" time="568.8893859217523" />
         <tli id="T1035" time="568.947" type="appl" />
         <tli id="T1036" time="569.375" type="appl" />
         <tli id="T1037" time="569.803" type="appl" />
         <tli id="T1038" time="570.231" type="appl" />
         <tli id="T1039" time="570.659" type="appl" />
         <tli id="T1040" time="571.087" type="appl" />
         <tli id="T1041" time="571.515" type="appl" />
         <tli id="T1042" time="571.943" type="appl" />
         <tli id="T1043" time="572.371" type="appl" />
         <tli id="T1044" time="572.799" type="appl" />
         <tli id="T1045" time="573.3003146705867" />
         <tli id="T1046" time="573.367" type="appl" />
         <tli id="T1047" time="573.9036666666667" type="appl" />
         <tli id="T1048" time="574.4403333333333" type="appl" />
         <tli id="T1049" time="574.977" type="appl" />
         <tli id="T1050" time="575.5136666666666" type="appl" />
         <tli id="T1051" time="576.0503333333334" type="appl" />
         <tli id="T1052" time="576.587" type="appl" />
         <tli id="T1053" time="577.1236666666666" type="appl" />
         <tli id="T1054" time="577.6603333333333" type="appl" />
         <tli id="T1055" time="578.197" type="appl" />
         <tli id="T1056" time="578.727" type="appl" />
         <tli id="T1057" time="579.3197777777777" type="appl" />
         <tli id="T1058" time="579.9125555555555" type="appl" />
         <tli id="T1059" time="580.5053333333333" type="appl" />
         <tli id="T1060" time="581.0981111111112" type="appl" />
         <tli id="T1061" time="581.6908888888889" type="appl" />
         <tli id="T1062" time="582.2836666666667" type="appl" />
         <tli id="T1063" time="582.8764444444445" type="appl" />
         <tli id="T1064" time="583.4692222222222" type="appl" />
         <tli id="T1065" time="584.062" type="appl" />
         <tli id="T1066" time="584.6547777777778" type="appl" />
         <tli id="T1067" time="585.2475555555556" type="appl" />
         <tli id="T1068" time="585.8403333333333" type="appl" />
         <tli id="T1069" time="586.4331111111111" type="appl" />
         <tli id="T1070" time="587.0258888888889" type="appl" />
         <tli id="T1071" time="587.6186666666666" type="appl" />
         <tli id="T1072" time="588.2114444444445" type="appl" />
         <tli id="T1073" time="588.8042222222223" type="appl" />
         <tli id="T1074" time="589.5693448932625" />
         <tli id="T1075" time="592.257" type="appl" />
         <tli id="T1076" time="592.7094999999999" type="appl" />
         <tli id="T1077" time="593.1619999999999" type="appl" />
         <tli id="T1078" time="593.397" type="appl" />
         <tli id="T1079" time="593.6145" type="appl" />
         <tli id="T1080" time="593.7724545454546" type="appl" />
         <tli id="T1081" time="594.067" type="appl" />
         <tli id="T1082" time="594.1479090909091" type="appl" />
         <tli id="T1083" time="594.5233636363637" type="appl" />
         <tli id="T1084" time="594.8988181818182" type="appl" />
         <tli id="T1085" time="595.2742727272728" type="appl" />
         <tli id="T1086" time="595.6497272727273" type="appl" />
         <tli id="T1087" time="596.0251818181819" type="appl" />
         <tli id="T1088" time="596.4006363636364" type="appl" />
         <tli id="T1089" time="596.776090909091" type="appl" />
         <tli id="T1090" time="596.947" type="appl" />
         <tli id="T1091" time="597.1515454545455" type="appl" />
         <tli id="T1092" time="597.3763636363636" type="appl" />
         <tli id="T1093" time="597.527" type="appl" />
         <tli id="T1094" time="597.8057272727273" type="appl" />
         <tli id="T1095" time="598.2350909090909" type="appl" />
         <tli id="T1096" time="598.6644545454545" type="appl" />
         <tli id="T1097" time="599.0938181818182" type="appl" />
         <tli id="T1098" time="599.5231818181818" type="appl" />
         <tli id="T1099" time="599.9525454545454" type="appl" />
         <tli id="T1100" time="600.3819090909091" type="appl" />
         <tli id="T1101" time="600.8112727272727" type="appl" />
         <tli id="T1102" time="601.2406363636363" type="appl" />
         <tli id="T1103" time="601.67" type="appl" />
         <tli id="T1104" time="602.937" type="appl" />
         <tli id="T1105" time="603.3864666666666" type="appl" />
         <tli id="T1106" time="603.8359333333333" type="appl" />
         <tli id="T1107" time="604.2854" type="appl" />
         <tli id="T1108" time="604.7348666666667" type="appl" />
         <tli id="T1109" time="605.1843333333333" type="appl" />
         <tli id="T1110" time="605.6338" type="appl" />
         <tli id="T1111" time="606.0832666666666" type="appl" />
         <tli id="T1112" time="606.5327333333333" type="appl" />
         <tli id="T1113" time="606.9821999999999" type="appl" />
         <tli id="T1114" time="607.4316666666666" type="appl" />
         <tli id="T1115" time="607.8811333333333" type="appl" />
         <tli id="T1116" time="608.3306" type="appl" />
         <tli id="T1117" time="608.7800666666666" type="appl" />
         <tli id="T1118" time="609.2295333333333" type="appl" />
         <tli id="T1119" time="609.679" type="appl" />
         <tli id="T1120" time="609.797" type="appl" />
         <tli id="T1121" time="610.3603333333333" type="appl" />
         <tli id="T1122" time="610.9236666666667" type="appl" />
         <tli id="T1123" time="611.6026345131339" />
         <tli id="T1124" time="611.817" type="appl" />
         <tli id="T1125" time="612.3854" type="appl" />
         <tli id="T1126" time="612.9538" type="appl" />
         <tli id="T1127" time="613.5222" type="appl" />
         <tli id="T1128" time="614.0906" type="appl" />
         <tli id="T1129" time="614.659" type="appl" />
         <tli id="T1130" time="615.2274" type="appl" />
         <tli id="T1131" time="615.7958" type="appl" />
         <tli id="T1132" time="616.3642" type="appl" />
         <tli id="T1133" time="616.9326" type="appl" />
         <tli id="T1134" time="617.501" type="appl" />
         <tli id="T1135" time="617.947" type="appl" />
         <tli id="T1136" time="618.6086666666666" type="appl" />
         <tli id="T1137" time="619.2703333333333" type="appl" />
         <tli id="T1138" time="619.932" type="appl" />
         <tli id="T1139" time="620.5936666666666" type="appl" />
         <tli id="T1140" time="621.2553333333333" type="appl" />
         <tli id="T1141" time="621.9169999999999" type="appl" />
         <tli id="T1142" time="622.137" type="appl" />
         <tli id="T1143" time="622.6845" type="appl" />
         <tli id="T1144" time="623.232" type="appl" />
         <tli id="T1145" time="623.7795" type="appl" />
         <tli id="T1146" time="624.327" type="appl" />
         <tli id="T1147" time="624.607" type="appl" />
         <tli id="T1148" time="625.136" type="appl" />
         <tli id="T1149" time="625.665" type="appl" />
         <tli id="T1150" time="626.194" type="appl" />
         <tli id="T1151" time="626.723" type="appl" />
         <tli id="T1152" time="627.252" type="appl" />
         <tli id="T1153" time="627.781" type="appl" />
         <tli id="T1154" time="628.31" type="appl" />
         <tli id="T1155" time="628.8389999999999" type="appl" />
         <tli id="T1156" time="629.3679999999999" type="appl" />
         <tli id="T1157" time="629.8969999999999" type="appl" />
         <tli id="T1158" time="630.2769999999999" type="appl" />
         <tli id="T1159" time="630.6569999999999" type="appl" />
         <tli id="T1160" time="631.0369999999999" type="appl" />
         <tli id="T1161" time="631.6425947543846" />
         <tli id="T1162" time="631.877" type="appl" />
         <tli id="T1163" time="632.3630909090908" type="appl" />
         <tli id="T1164" time="632.8491818181818" type="appl" />
         <tli id="T1165" time="633.3352727272727" type="appl" />
         <tli id="T1166" time="633.8213636363636" type="appl" />
         <tli id="T1167" time="634.3074545454544" type="appl" />
         <tli id="T1168" time="634.7935454545454" type="appl" />
         <tli id="T1169" time="635.2796363636363" type="appl" />
         <tli id="T1170" time="635.7657272727272" type="appl" />
         <tli id="T1171" time="636.2518181818181" type="appl" />
         <tli id="T1172" time="636.737909090909" type="appl" />
         <tli id="T1173" time="637.4025833267201" />
         <tli id="T1174" time="637.717" type="appl" />
         <tli id="T1175" time="638.3145" type="appl" />
         <tli id="T1176" time="638.912" type="appl" />
         <tli id="T1177" time="639.5095" type="appl" />
         <tli id="T1178" time="640.107" type="appl" />
         <tli id="T1179" time="640.307" type="appl" />
         <tli id="T1180" time="640.707" type="appl" />
         <tli id="T1181" time="641.107" type="appl" />
         <tli id="T1182" time="641.5070000000001" type="appl" />
         <tli id="T1183" time="641.907" type="appl" />
         <tli id="T1184" time="642.307" type="appl" />
         <tli id="T1185" time="642.707" type="appl" />
         <tli id="T1186" time="643.197" type="appl" />
         <tli id="T1187" time="643.6192222222222" type="appl" />
         <tli id="T1188" time="644.0414444444444" type="appl" />
         <tli id="T1189" time="644.4636666666667" type="appl" />
         <tli id="T1190" time="644.8858888888889" type="appl" />
         <tli id="T1191" time="645.3081111111111" type="appl" />
         <tli id="T1192" time="645.7303333333333" type="appl" />
         <tli id="T1193" time="646.1525555555555" type="appl" />
         <tli id="T1194" time="646.5747777777777" type="appl" />
         <tli id="T1195" time="647.0369912956436" />
         <tli id="T1196" time="647.0436579490838" />
         <tli id="T1197" time="647.6554615384615" type="appl" />
         <tli id="T1198" time="648.2939230769231" type="appl" />
         <tli id="T1199" time="648.9323846153845" type="appl" />
         <tli id="T1200" time="649.5708461538461" type="appl" />
         <tli id="T1201" time="650.2093076923077" type="appl" />
         <tli id="T1202" time="650.8477692307692" type="appl" />
         <tli id="T1203" time="651.4862307692307" type="appl" />
         <tli id="T1204" time="652.1246923076923" type="appl" />
         <tli id="T1205" time="652.7631538461538" type="appl" />
         <tli id="T1206" time="653.4016153846154" type="appl" />
         <tli id="T1207" time="654.0400769230769" type="appl" />
         <tli id="T1208" time="654.6785384615384" type="appl" />
         <tli id="T1209" time="655.6892137131973" />
         <tli id="T1210" time="656.6569999999999" type="appl" />
         <tli id="T1211" time="657.1755714285714" type="appl" />
         <tli id="T1212" time="657.6941428571428" type="appl" />
         <tli id="T1213" time="658.2127142857142" type="appl" />
         <tli id="T1214" time="658.7312857142856" type="appl" />
         <tli id="T1215" time="659.2498571428571" type="appl" />
         <tli id="T1216" time="659.7684285714284" type="appl" />
         <tli id="T1217" time="660.2869999999999" type="appl" />
         <tli id="T1218" time="660.8803333333333" type="appl" />
         <tli id="T1219" time="661.4736666666665" type="appl" />
         <tli id="T1220" time="662.0669999999999" type="appl" />
         <tli id="T1221" time="662.6603333333333" type="appl" />
         <tli id="T1222" time="663.2536666666666" type="appl" />
         <tli id="T1223" time="663.847" type="appl" />
         <tli id="T1224" time="664.4403333333332" type="appl" />
         <tli id="T1225" time="665.0336666666666" type="appl" />
         <tli id="T1226" time="665.627" type="appl" />
         <tli id="T1227" time="666.2203333333332" type="appl" />
         <tli id="T1228" time="666.8136666666666" type="appl" />
         <tli id="T1229" time="667.4069999999999" type="appl" />
         <tli id="T1230" time="668.8891875247996" />
         <tli id="T1231" time="669.5758528291404" />
         <tli id="T1232" time="669.967" type="appl" />
         <tli id="T1233" time="670.597" type="appl" />
         <tli id="T1234" time="671.3691826045552" />
         <tli id="T1235" time="671.637" type="appl" />
         <tli id="T1236" time="672.1641428571428" type="appl" />
         <tli id="T1237" time="672.6912857142856" type="appl" />
         <tli id="T1238" time="673.2184285714285" type="appl" />
         <tli id="T1239" time="673.7455714285715" type="appl" />
         <tli id="T1240" time="674.2727142857143" type="appl" />
         <tli id="T1241" time="674.7998571428571" type="appl" />
         <tli id="T1242" time="675.327" type="appl" />
         <tli id="T1243" time="675.732" type="appl" />
         <tli id="T1244" time="676.137" type="appl" />
         <tli id="T1245" time="676.5419999999999" type="appl" />
         <tli id="T1246" time="676.947" type="appl" />
         <tli id="T1247" time="677.352" type="appl" />
         <tli id="T1248" time="677.8758363621935" />
         <tli id="T1249" time="678.577" type="appl" />
         <tli id="T1250" time="679.1569999999999" type="appl" />
         <tli id="T1251" time="679.457" type="appl" />
         <tli id="T1252" time="679.927" type="appl" />
         <tli id="T1253" time="681.127" type="appl" />
         <tli id="T1254" time="681.3636666666666" type="appl" />
         <tli id="T1255" time="681.6003333333333" type="appl" />
         <tli id="T1256" time="681.837" type="appl" />
         <tli id="T1257" time="681.857" type="appl" />
         <tli id="T1258" time="682.6044999999999" type="appl" />
         <tli id="T1259" time="683.352" type="appl" />
         <tli id="T1260" time="684.0995" type="appl" />
         <tli id="T1261" time="684.847" type="appl" />
         <tli id="T1262" time="685.087" type="appl" />
         <tli id="T1263" time="685.6895" type="appl" />
         <tli id="T1264" time="686.292" type="appl" />
         <tli id="T1265" time="686.8945" type="appl" />
         <tli id="T1266" time="687.497" type="appl" />
         <tli id="T1267" time="688.0995" type="appl" />
         <tli id="T1268" time="688.702" type="appl" />
         <tli id="T1269" time="689.3045" type="appl" />
         <tli id="T1270" time="689.907" type="appl" />
         <tli id="T1271" time="690.20825" type="intp" />
         <tli id="T1272" time="690.5095" type="appl" />
         <tli id="T1273" time="691.112" type="appl" />
         <tli id="T1274" time="691.7145" type="appl" />
         <tli id="T1275" time="692.317" type="appl" />
         <tli id="T1276" time="692.947" type="appl" />
         <tli id="T1277" time="693.44825" type="appl" />
         <tli id="T1278" time="693.9495" type="appl" />
         <tli id="T1279" time="694.45075" type="appl" />
         <tli id="T1280" time="694.952" type="appl" />
         <tli id="T1281" time="695.45325" type="appl" />
         <tli id="T1282" time="695.9545" type="appl" />
         <tli id="T1283" time="696.45575" type="appl" />
         <tli id="T1284" time="696.930329808584" />
         <tli id="T1285" time="697.442" type="appl" />
         <tli id="T1286" time="697.927" type="appl" />
         <tli id="T1287" time="698.412" type="appl" />
         <tli id="T1288" time="698.8969999999999" type="appl" />
         <tli id="T1289" time="699.382" type="appl" />
         <tli id="T1290" time="700.0024591302277" />
         <tli id="T1291" time="700.257" type="appl" />
         <tli id="T1292" time="700.9544999999999" type="appl" />
         <tli id="T1293" time="701.652" type="appl" />
         <tli id="T1294" time="702.3495" type="appl" />
         <tli id="T1295" time="703.2557860090468" />
         <tli id="T1296" time="704.227" type="appl" />
         <tli id="T1297" time="705.5036666666666" type="appl" />
         <tli id="T1298" time="706.7803333333333" type="appl" />
         <tli id="T1299" time="708.057" type="appl" />
         <tli id="T1300" time="709.3336666666667" type="appl" />
         <tli id="T1301" time="710.6103333333333" type="appl" />
         <tli id="T1302" time="711.2486666666666" type="intp" />
         <tli id="T1303" time="711.887" type="appl" />
         <tli id="T1304" time="712.457" type="appl" />
         <tli id="T1305" time="713.3779999999999" type="appl" />
         <tli id="T1306" time="714.299" type="appl" />
         <tli id="T1307" time="715.22" type="appl" />
         <tli id="T1308" time="716.141" type="appl" />
         <tli id="T1309" time="717.0619999999999" type="appl" />
         <tli id="T1310" time="717.983" type="appl" />
         <tli id="T1311" time="718.904" type="appl" />
         <tli id="T1312" time="719.8249999999999" type="appl" />
         <tli id="T1313" time="720.7459999999999" type="appl" />
         <tli id="T1314" time="721.6669999999999" type="appl" />
         <tli id="T1315" time="721.677" type="appl" />
         <tli id="T1316" time="722.3706363636364" type="appl" />
         <tli id="T1317" time="723.0642727272727" type="appl" />
         <tli id="T1318" time="723.7579090909092" type="appl" />
         <tli id="T1319" time="724.4515454545455" type="appl" />
         <tli id="T1320" time="725.1451818181819" type="appl" />
         <tli id="T1321" time="725.8388181818182" type="appl" />
         <tli id="T1322" time="726.5324545454546" type="appl" />
         <tli id="T1323" time="727.2260909090909" type="appl" />
         <tli id="T1324" time="727.9197272727273" type="appl" />
         <tli id="T1325" time="728.6133636363636" type="appl" />
         <tli id="T1326" time="729.0957347432744" />
         <tli id="T1327" time="729.437" type="appl" />
         <tli id="T1328" time="730.777" type="appl" />
         <tli id="T1329" time="732.117" type="appl" />
         <tli id="T1330" time="733.457" type="appl" />
         <tli id="T1331" time="734.797" type="appl" />
         <tli id="T1332" time="736.1370000000001" type="appl" />
         <tli id="T1333" time="737.477" type="appl" />
         <tli id="T1334" time="738.817" type="appl" />
         <tli id="T1335" time="739.327" type="appl" />
         <tli id="T1336" time="740.152" type="appl" />
         <tli id="T1337" time="740.977" type="appl" />
         <tli id="T1338" time="741.0169999999999" type="appl" />
         <tli id="T1339" time="741.787" type="appl" />
         <tli id="T1340" time="742.557" type="appl" />
         <tli id="T1341" time="743.217" type="appl" />
         <tli id="T1342" time="743.813" type="appl" />
         <tli id="T1343" time="744.409" type="appl" />
         <tli id="T1344" time="745.005" type="appl" />
         <tli id="T1345" time="745.601" type="appl" />
         <tli id="T1346" time="746.197" type="appl" />
         <tli id="T1347" time="746.367" type="appl" />
         <tli id="T1348" time="747.2194999999999" type="appl" />
         <tli id="T1349" time="748.0719999999999" type="appl" />
         <tli id="T1350" time="748.9245" type="appl" />
         <tli id="T1351" time="749.7769999999999" type="appl" />
         <tli id="T1352" time="749.807" type="appl" />
         <tli id="T1353" time="750.367" type="appl" />
         <tli id="T1354" time="750.927" type="appl" />
         <tli id="T1355" time="751.4870000000001" type="appl" />
         <tli id="T1356" time="752.047" type="appl" />
         <tli id="T1357" time="752.347" type="appl" />
         <tli id="T1358" time="752.9145" type="appl" />
         <tli id="T1359" time="753.482" type="appl" />
         <tli id="T1360" time="754.0495" type="appl" />
         <tli id="T1361" time="754.617" type="appl" />
         <tli id="T1362" time="754.627" type="appl" />
         <tli id="T1363" time="755.4369999999999" type="appl" />
         <tli id="T1364" time="756.247" type="appl" />
         <tli id="T1365" time="757.057" type="appl" />
         <tli id="T1366" time="757.867" type="appl" />
         <tli id="T1367" time="758.6769999999999" type="appl" />
         <tli id="T1368" time="759.487" type="appl" />
         <tli id="T1369" time="759.937" type="appl" />
         <tli id="T1370" time="760.4179090909091" type="appl" />
         <tli id="T1371" time="760.8988181818182" type="appl" />
         <tli id="T1372" time="761.3797272727272" type="appl" />
         <tli id="T1373" time="761.8606363636363" type="appl" />
         <tli id="T1374" time="762.3415454545454" type="appl" />
         <tli id="T1375" time="762.8224545454545" type="appl" />
         <tli id="T1376" time="763.3033636363637" type="appl" />
         <tli id="T1377" time="763.7842727272728" type="appl" />
         <tli id="T1378" time="764.2651818181818" type="appl" />
         <tli id="T1379" time="764.7460909090909" type="appl" />
         <tli id="T1380" time="765.3536836417992" />
         <tli id="T1381" time="765.7379999999999" type="appl" />
         <tli id="T1382" time="766.249" type="appl" />
         <tli id="T1383" time="766.76" type="appl" />
         <tli id="T1384" time="767.271" type="appl" />
         <tli id="T1385" time="767.347" type="appl" />
         <tli id="T1386" time="767.917" type="appl" />
         <tli id="T1387" time="768.487" type="appl" />
         <tli id="T1388" time="769.1023220379335" />
         <tli id="T1389" time="769.6045" type="appl" />
         <tli id="T1390" time="770.152" type="appl" />
         <tli id="T1391" time="770.6995" type="appl" />
         <tli id="T1392" time="771.247" type="appl" />
         <tli id="T1393" time="772.2669999999999" type="appl" />
         <tli id="T1394" time="772.9803333333333" type="appl" />
         <tli id="T1395" time="773.6936666666666" type="appl" />
         <tli id="T1396" time="774.0503333333332" type="intp" />
         <tli id="T1397" time="774.4069999999999" type="appl" />
         <tli id="T1398" time="774.457" type="appl" />
         <tli id="T1399" time="774.942" type="appl" />
         <tli id="T1400" time="775.427" type="appl" />
         <tli id="T1401" time="775.912" type="appl" />
         <tli id="T1402" time="776.3969999999999" type="appl" />
         <tli id="T1403" time="776.882" type="appl" />
         <tli id="T1404" time="777.367" type="appl" />
         <tli id="T1405" time="778.0553333333332" type="appl" />
         <tli id="T1406" time="778.7436666666666" type="appl" />
         <tli id="T1407" time="779.432" type="appl" />
         <tli id="T1408" time="780.1203333333333" type="appl" />
         <tli id="T1409" time="780.8086666666666" type="appl" />
         <tli id="T1410" time="781.497" type="appl" />
         <tli id="T1411" time="782.9169999999999" type="appl" />
         <tli id="T1412" time="783.752" type="appl" />
         <tli id="T1413" time="784.587" type="appl" />
         <tli id="T1414" time="785.097" type="appl" />
         <tli id="T1415" time="787.027" type="appl" />
         <tli id="T1416" time="788.957" type="appl" />
         <tli id="T1417" time="790.887" type="appl" />
         <tli id="T1418" time="792.817" type="appl" />
         <tli id="T1419" time="793.127" type="appl" />
         <tli id="T1420" time="794.012" type="appl" />
         <tli id="T1421" time="794.8969999999999" type="appl" />
         <tli id="T1422" time="795.0369999999999" type="appl" />
         <tli id="T1423" time="795.8394999999999" type="appl" />
         <tli id="T1424" time="796.6419999999999" type="appl" />
         <tli id="T1425" time="797.4445" type="appl" />
         <tli id="T1426" time="798.3003370265131" />
         <tli id="T1427" time="798.4069999999999" type="appl" />
         <tli id="T1428" time="799.3069999999999" type="appl" />
         <tli id="T1429" time="800.2069999999999" type="appl" />
         <tli id="T1430" time="800.6569999999999" type="intp" />
         <tli id="T1431" time="801.107" type="appl" />
         <tli id="T1432" time="802.007" type="appl" />
         <tli id="T1433" time="802.127" type="appl" />
         <tli id="T1434" time="802.8153333333332" type="appl" />
         <tli id="T1435" time="803.5036666666666" type="appl" />
         <tli id="T1436" time="804.192" type="appl" />
         <tli id="T1437" time="804.8803333333333" type="appl" />
         <tli id="T1438" time="805.5686666666666" type="appl" />
         <tli id="T1439" time="806.257" type="appl" />
         <tli id="T1440" time="806.477" type="appl" />
         <tli id="T1441" time="807.2214444444444" type="appl" />
         <tli id="T1442" time="807.9658888888889" type="appl" />
         <tli id="T1443" time="808.7103333333333" type="appl" />
         <tli id="T1444" time="809.4547777777777" type="appl" />
         <tli id="T1445" time="810.1992222222223" type="appl" />
         <tli id="T1446" time="810.9436666666667" type="appl" />
         <tli id="T1447" time="811.6881111111111" type="appl" />
         <tli id="T1448" time="812.4325555555556" type="appl" />
         <tli id="T1449" time="813.177" type="appl" />
         <tli id="T1450" time="814.0403333333334" type="appl" />
         <tli id="T1451" time="814.9036666666666" type="appl" />
         <tli id="T1452" time="815.7669999999999" type="appl" />
         <tli id="T1453" time="815.987" type="appl" />
         <tli id="T1454" time="816.572" type="appl" />
         <tli id="T1455" time="817.1569999999999" type="appl" />
         <tli id="T1456" time="817.742" type="appl" />
         <tli id="T1457" time="818.327" type="appl" />
         <tli id="T1458" time="818.9119999999999" type="appl" />
         <tli id="T1459" time="819.497" type="appl" />
         <tli id="T1460" time="820.0819999999999" type="appl" />
         <tli id="T1461" time="820.6669999999999" type="appl" />
         <tli id="T1462" time="820.857" type="appl" />
         <tli id="T1463" time="822.6703407604058" />
         <tli id="T1464" time="822.7869999999999" type="appl" />
         <tli id="T1465" time="823.6254615384614" type="appl" />
         <tli id="T1466" time="824.463923076923" type="appl" />
         <tli id="T1467" time="825.3023846153845" type="appl" />
         <tli id="T1468" time="826.1408461538462" type="appl" />
         <tli id="T1469" time="826.9793076923077" type="appl" />
         <tli id="T1470" time="827.8177692307692" type="appl" />
         <tli id="T1471" time="828.6562307692308" type="appl" />
         <tli id="T1472" time="829.4946923076923" type="appl" />
         <tli id="T1473" time="830.3331538461538" type="appl" />
         <tli id="T1474" time="831.1716153846154" type="appl" />
         <tli id="T1475" time="832.0100769230769" type="appl" />
         <tli id="T1476" time="832.8485384615385" type="appl" />
         <tli id="T1477" time="833.687" type="appl" />
         <tli id="T1478" time="837.297" type="appl" />
         <tli id="T1479" time="837.6436443871022" />
         <tli id="T1480" time="837.7669999999999" type="appl" />
         <tli id="T1481" time="838.1519999999999" type="appl" />
         <tli id="T1482" time="838.547" type="appl" />
         <tli id="T1483" time="838.942" type="appl" />
         <tli id="T1484" time="839.0269999999999" type="appl" />
         <tli id="T1485" time="839.337" type="appl" />
         <tli id="T1486" time="839.577" type="appl" />
         <tli id="T1487" time="840.1569999999999" type="appl" />
         <tli id="T1488" time="840.187" type="appl" />
         <tli id="T1489" time="840.9245" type="appl" />
         <tli id="T1490" time="841.662" type="appl" />
         <tli id="T1491" time="842.3995" type="appl" />
         <tli id="T1492" time="843.137" type="appl" />
         <tli id="T1493" time="844.1569999999999" type="appl" />
         <tli id="T1494" time="844.6569999999999" type="appl" />
         <tli id="T1495" time="844.957" type="appl" />
         <tli id="T1496" time="845.1569999999999" type="appl" />
         <tli id="T1497" time="845.6569999999999" type="appl" />
         <tli id="T1498" time="845.702" type="appl" />
         <tli id="T1499" time="846.447" type="appl" />
         <tli id="T1500" time="847.192" type="appl" />
         <tli id="T1501" time="847.237" type="appl" />
         <tli id="T1502" time="847.6836666666666" type="appl" />
         <tli id="T1503" time="847.937" type="appl" />
         <tli id="T1504" time="848.1303333333333" type="appl" />
         <tli id="T1505" time="848.577" type="appl" />
         <tli id="T1506" time="848.682" type="appl" />
         <tli id="T1507" time="849.0236666666666" type="appl" />
         <tli id="T1508" time="849.427" type="appl" />
         <tli id="T1509" time="849.4703333333332" type="appl" />
         <tli id="T1510" time="849.9169999999999" type="appl" />
         <tli id="T1511" time="850.387" type="appl" />
         <tli id="T1512" time="850.7869999999999" type="appl" />
         <tli id="T1513" time="851.627" type="appl" />
         <tli id="T1514" time="853.1154886913737" />
         <tli id="T1515" time="853.4169999999999" type="appl" />
         <tli id="T1516" time="854.3419999999999" type="appl" />
         <tli id="T1517" time="855.3821508610428" />
         <tli id="T1518" time="855.5269999999999" type="appl" />
         <tli id="T1519" time="855.8736666666666" type="appl" />
         <tli id="T1520" time="856.2203333333333" type="appl" />
         <tli id="T1521" time="856.567" type="appl" />
         <tli id="T1522" time="857.062" type="appl" />
         <tli id="T1523" time="857.557" type="appl" />
         <tli id="T1524" time="858.052" type="appl" />
         <tli id="T1525" time="858.547" type="appl" />
         <tli id="T1526" time="859.127" type="appl" />
         <tli id="T1527" time="859.747" type="appl" />
         <tli id="T1528" time="861.1569999999999" type="appl" />
         <tli id="T1529" time="861.9169999999999" type="appl" />
         <tli id="T1530" time="862.4169999999999" type="appl" />
         <tli id="T1531" time="862.9169999999999" type="appl" />
         <tli id="T1532" time="863.4169999999999" type="appl" />
         <tli id="T1533" time="863.9169999999999" type="appl" />
         <tli id="T1534" time="864.4169999999999" type="appl" />
         <tli id="T1535" time="864.9169999999999" type="appl" />
         <tli id="T1536" time="865.4169999999999" type="appl" />
         <tli id="T1537" time="865.9069999999999" type="appl" />
         <tli id="T1538" time="865.9169999999999" type="appl" />
         <tli id="T1539" time="866.817" type="appl" />
         <tli id="T1540" time="867.4995" type="appl" />
         <tli id="T1541" time="868.182" type="appl" />
         <tli id="T1542" time="868.8645" type="appl" />
         <tli id="T1543" time="869.547" type="appl" />
         <tli id="T1544" time="869.617" type="appl" />
         <tli id="T1545" time="870.2579999999999" type="appl" />
         <tli id="T1546" time="870.487" type="appl" />
         <tli id="T1547" time="870.899" type="appl" />
         <tli id="T1548" time="871.54" type="appl" />
         <tli id="T1549" time="872.389" type="appl" />
         <tli id="T1550" time="872.945" type="appl" />
         <tli id="T1551" time="873.501" type="appl" />
         <tli id="T1552" time="874.057" type="appl" />
         <tli id="T1553" time="876.645" type="appl" />
         <tli id="T1554" time="877.039" type="appl" />
         <tli id="T1555" time="877.377" type="appl" />
         <tli id="T1556" time="877.7936666666666" type="appl" />
         <tli id="T1557" time="878.2103333333333" type="appl" />
         <tli id="T1558" time="878.627" type="appl" />
         <tli id="T1559" time="879.0436666666666" type="appl" />
         <tli id="T1560" time="879.4603333333333" type="appl" />
         <tli id="T1561" time="879.877" type="appl" />
         <tli id="T1562" time="880.097" type="appl" />
         <tli id="T1563" time="881.8354317117688" />
         <tli id="T1564" time="883.387" type="appl" />
         <tli id="T1565" time="883.877" type="appl" />
         <tli id="T1566" time="883.987" type="appl" />
         <tli id="T1567" time="884.2703333333333" type="appl" />
         <tli id="T1568" time="884.587" type="appl" />
         <tli id="T1569" time="884.6636666666666" type="appl" />
         <tli id="T1570" time="885.057" type="appl" />
         <tli id="T1571" time="885.317" type="appl" />
         <tli id="T1572" time="885.4503333333333" type="appl" />
         <tli id="T1573" time="885.8436666666666" type="appl" />
         <tli id="T1574" time="886.097" type="appl" />
         <tli id="T1575" time="886.237" type="appl" />
         <tli id="T1576" time="887.1569999999999" type="appl" />
         <tli id="T1577" time="887.6136666666666" type="appl" />
         <tli id="T1578" time="888.0703333333332" type="appl" />
         <tli id="T1579" time="888.5669808565541" />
         <tli id="T1580" time="888.8886666666666" type="appl" />
         <tli id="T1581" time="889.2503333333333" type="appl" />
         <tli id="T1582" time="889.612" type="appl" />
         <tli id="T1583" time="889.9736666666666" type="appl" />
         <tli id="T1584" time="890.3353333333333" type="appl" />
         <tli id="T1585" time="890.697" type="appl" />
         <tli id="T1586" time="891.477" type="appl" />
         <tli id="T1587" time="892.2814999999999" type="appl" />
         <tli id="T1588" time="893.086" type="appl" />
         <tli id="T1589" time="893.8905" type="appl" />
         <tli id="T1590" time="894.6949999999999" type="appl" />
         <tli id="T1591" time="894.847" type="appl" />
         <tli id="T1592" time="895.3914444444445" type="appl" />
         <tli id="T1593" time="895.9358888888888" type="appl" />
         <tli id="T1594" time="896.4803333333333" type="appl" />
         <tli id="T1595" time="897.0247777777778" type="appl" />
         <tli id="T1596" time="897.5692222222222" type="appl" />
         <tli id="T1597" time="898.1136666666666" type="appl" />
         <tli id="T1598" time="898.6581111111111" type="appl" />
         <tli id="T1599" time="899.2025555555555" type="appl" />
         <tli id="T1600" time="899.747" type="appl" />
         <tli id="T1601" time="900.057" type="appl" />
         <tli id="T1602" time="900.677" type="appl" />
         <tli id="T1603" time="901.297" type="appl" />
         <tli id="T1604" time="901.917" type="appl" />
         <tli id="T1605" time="902.5369999999999" type="appl" />
         <tli id="T1606" time="903.1569999999999" type="appl" />
         <tli id="T1607" time="903.7769999999999" type="appl" />
         <tli id="T1608" time="904.4303348007475" />
         <tli id="T1609" time="904.497" type="appl" />
         <tli id="T1610" time="905.1569999999999" type="appl" />
         <tli id="T1611" time="905.817" type="appl" />
         <tli id="T1612" time="906.477" type="appl" />
         <tli id="T1613" time="906.8553820728514" />
         <tli id="T1614" time="907.489" type="appl" />
         <tli id="T1615" time="907.881" type="appl" />
         <tli id="T1616" time="908.273" type="appl" />
         <tli id="T1617" time="908.665" type="appl" />
         <tli id="T1618" time="909.3487104594874" />
         <tli id="T1619" time="909.707" type="appl" />
         <tli id="T1620" time="910.65575" type="appl" />
         <tli id="T1621" time="911.6045" type="appl" />
         <tli id="T1622" time="912.55325" type="appl" />
         <tli id="T1623" time="913.502" type="appl" />
         <tli id="T1624" time="914.45075" type="appl" />
         <tli id="T1625" time="915.3995" type="appl" />
         <tli id="T1626" time="916.34825" type="appl" />
         <tli id="T1627" time="917.297" type="appl" />
         <tli id="T1628" time="918.697" type="appl" />
         <tli id="T1629" time="919.2302666666667" type="appl" />
         <tli id="T1630" time="919.7635333333334" type="appl" />
         <tli id="T1631" time="920.2968" type="appl" />
         <tli id="T1632" time="920.8300666666667" type="appl" />
         <tli id="T1633" time="921.3633333333333" type="appl" />
         <tli id="T1634" time="921.8966" type="appl" />
         <tli id="T1635" time="922.4298666666667" type="appl" />
         <tli id="T1636" time="922.9631333333333" type="appl" />
         <tli id="T1637" time="923.4964" type="appl" />
         <tli id="T1638" time="924.0296666666667" type="appl" />
         <tli id="T1639" time="924.5629333333334" type="appl" />
         <tli id="T1640" time="925.0962000000001" type="appl" />
         <tli id="T1641" time="925.6294666666666" type="appl" />
         <tli id="T1642" time="926.1627333333333" type="appl" />
         <tli id="T1643" time="926.696" type="appl" />
         <tli id="T1644" time="927.307" type="appl" />
         <tli id="T1645" time="927.797" type="appl" />
         <tli id="T1646" time="928.2869999999999" type="appl" />
         <tli id="T1647" time="928.607" type="appl" />
         <tli id="T1648" time="929.4369999999999" type="appl" />
         <tli id="T1649" time="930.2669999999999" type="appl" />
         <tli id="T1650" time="930.867" type="appl" />
         <tli id="T1651" time="931.5581111111111" type="appl" />
         <tli id="T1652" time="932.2492222222222" type="appl" />
         <tli id="T1653" time="932.9403333333333" type="appl" />
         <tli id="T1654" time="933.6314444444445" type="appl" />
         <tli id="T1655" time="934.3225555555555" type="appl" />
         <tli id="T1656" time="935.0136666666666" type="appl" />
         <tli id="T1657" time="935.7047777777777" type="appl" />
         <tli id="T1658" time="936.3958888888889" type="appl" />
         <tli id="T1659" time="937.2486551067375" />
         <tli id="T1660" time="937.387" type="appl" />
         <tli id="T1661" time="937.9836666666666" type="appl" />
         <tli id="T1662" time="938.5803333333333" type="appl" />
         <tli id="T1663" time="939.1769999999999" type="appl" />
         <tli id="T1664" time="939.7736666666666" type="appl" />
         <tli id="T1665" time="940.3703333333333" type="appl" />
         <tli id="T1666" time="941.0469809042933" />
         <tli id="T1667" time="941.0503142310134" />
         <tli id="T1668" time="941.5269999999999" type="appl" />
         <tli id="T1669" time="942.057" type="appl" />
         <tli id="T1670" time="942.587" type="appl" />
         <tli id="T1671" time="943.117" type="appl" />
         <tli id="T1672" time="943.927" type="appl" />
         <tli id="T1673" time="944.3870000000001" type="appl" />
         <tli id="T1674" time="944.847" type="appl" />
         <tli id="T1675" time="945.307" type="appl" />
         <tli id="T1676" time="945.7669999999999" type="appl" />
         <tli id="T1677" time="946.227" type="appl" />
         <tli id="T1678" time="946.687" type="appl" />
         <tli id="T1679" time="947.1469999999999" type="appl" />
         <tli id="T1680" time="947.607" type="appl" />
         <tli id="T1681" time="948.067" type="appl" />
         <tli id="T1682" time="948.5269999999999" type="appl" />
         <tli id="T1683" time="948.987" type="appl" />
         <tli id="T1684" time="949.4469999999999" type="appl" />
         <tli id="T1685" time="949.9069999999999" type="appl" />
         <tli id="T1686" time="949.927" type="appl" />
         <tli id="T1687" time="950.393" type="appl" />
         <tli id="T1688" time="950.859" type="appl" />
         <tli id="T1689" time="951.3249999999999" type="appl" />
         <tli id="T1690" time="951.7909999999999" type="appl" />
         <tli id="T1691" time="952.257" type="appl" />
         <tli id="T1692" time="953.797" type="appl" />
         <tli id="T1693" time="954.652" type="appl" />
         <tli id="T1694" time="955.507" type="appl" />
         <tli id="T1695" time="955.9169999999999" type="appl" />
         <tli id="T1696" time="958.127" type="appl" />
         <tli id="T1697" time="959.487" type="appl" />
         <tli id="T1698" time="960.4725555555556" type="appl" />
         <tli id="T1699" time="961.4581111111111" type="appl" />
         <tli id="T1700" time="962.4436666666667" type="appl" />
         <tli id="T1701" time="963.4292222222222" type="appl" />
         <tli id="T1702" time="964.4147777777778" type="appl" />
         <tli id="T1703" time="965.4003333333333" type="appl" />
         <tli id="T1704" time="966.3858888888889" type="appl" />
         <tli id="T1705" time="967.3714444444444" type="appl" />
         <tli id="T1706" time="968.357" type="appl" />
         <tli id="T1707" time="968.557" type="appl" />
         <tli id="T1708" time="970.011" type="appl" />
         <tli id="T1709" time="971.465" type="appl" />
         <tli id="T1710" time="972.919" type="appl" />
         <tli id="T1711" time="974.373" type="appl" />
         <tli id="T1712" time="975.920349216456" />
         <tli id="T1713" time="976.2669999999999" type="appl" />
         <tli id="T1714" time="977.2819999999999" type="appl" />
         <tli id="T1715" time="978.2969999999999" type="appl" />
         <tli id="T1716" time="979.3119999999999" type="appl" />
         <tli id="T1717" time="980.327" type="appl" />
         <tli id="T1718" time="981.342" type="appl" />
         <tli id="T1719" time="982.357" type="appl" />
         <tli id="T1720" time="982.387" type="appl" />
         <tli id="T1721" time="984.1303333333333" type="appl" />
         <tli id="T1722" time="985.8736666666666" type="appl" />
         <tli id="T1723" time="986.637" type="appl" />
         <tli id="T1724" time="987.2769999999999" type="appl" />
         <tli id="T1725" time="987.617" type="appl" />
         <tli id="T1726" time="989.3603333333333" type="appl" />
         <tli id="T1727" time="991.1036666666666" type="appl" />
         <tli id="T1728" time="992.847" type="appl" />
         <tli id="T1729" time="994.467" type="appl" />
         <tli id="T1730" time="996.1469999999999" type="appl" />
         <tli id="T1731" time="997.827" type="appl" />
         <tli id="T1732" time="1000.007" type="appl" />
         <tli id="T1733" time="1000.6803333333332" type="appl" />
         <tli id="T1734" time="1001.3536666666666" type="appl" />
         <tli id="T1735" time="1002.027" type="appl" />
         <tli id="T1736" time="1002.7003333333333" type="appl" />
         <tli id="T1737" time="1003.3736666666666" type="appl" />
         <tli id="T1738" time="1004.047" type="appl" />
         <tli id="T1739" time="1004.707" type="appl" />
         <tli id="T1740" time="1005.737" type="appl" />
         <tli id="T1741" time="1006.767" type="appl" />
         <tli id="T1742" time="1007.797" type="appl" />
         <tli id="T1743" time="1007.947" type="appl" />
         <tli id="T0" time="1008.078" />
         <tli id="T1744" time="1008.597" type="appl" />
         <tli id="T1745" time="1009.247" type="appl" />
         <tli id="T1746" time="1009.8969999999999" type="appl" />
         <tli id="T1747" time="1010.547" type="appl" />
         <tli id="T1748" time="1011.197" type="appl" />
         <tli id="T1749" time="1011.847" type="appl" />
         <tli id="T1750" time="1012.6951720895167" />
         <tli id="T1751" time="1012.947" type="appl" />
         <tli id="T1752" time="1013.4703333333333" type="appl" />
         <tli id="T1753" time="1013.9936666666666" type="appl" />
         <tli id="T1754" time="1014.517" type="appl" />
         <tli id="T1755" time="1015.0403333333334" type="appl" />
         <tli id="T1756" time="1015.5636666666667" type="appl" />
         <tli id="T1757" time="1016.087" type="appl" />
         <tli id="T1758" time="1016.6103333333333" type="appl" />
         <tli id="T1759" time="1017.1336666666666" type="appl" />
         <tli id="T1760" time="1017.6569999999999" type="appl" />
         <tli id="T1761" time="1018.1803333333334" type="appl" />
         <tli id="T1762" time="1018.7036666666667" type="appl" />
         <tli id="T1763" time="1019.227" type="appl" />
         <tli id="T1764" time="1019.487" type="appl" />
         <tli id="T1765" time="1020.0153333333333" type="appl" />
         <tli id="T1766" time="1020.5436666666666" type="appl" />
         <tli id="T1767" time="1021.0719999999999" type="appl" />
         <tli id="T1768" time="1021.6003333333333" type="appl" />
         <tli id="T1769" time="1022.1286666666666" type="appl" />
         <tli id="T1770" time="1022.6569999999999" type="appl" />
         <tli id="T1771" time="1024.957" type="appl" />
         <tli id="T1772" time="1025.8418126735974" />
         <tli id="T1773" time="1026.13325" type="appl" />
         <tli id="T1774" time="1026.5195" type="appl" />
         <tli id="T1775" time="1026.9057500000001" type="appl" />
         <tli id="T1776" time="1027.292" type="appl" />
         <tli id="T1777" time="1027.67825" type="appl" />
         <tli id="T1778" time="1028.0645" type="appl" />
         <tli id="T1779" time="1028.45075" type="appl" />
         <tli id="T1780" time="1028.8970149454906" />
         <tli id="T1781" time="1028.9003482722105" />
         <tli id="T1782" time="1029.1255714285714" type="appl" />
         <tli id="T1783" time="1029.404142857143" type="appl" />
         <tli id="T1784" time="1029.6827142857144" type="appl" />
         <tli id="T1785" time="1029.9612857142859" type="appl" />
         <tli id="T1786" time="1030.2398571428573" type="appl" />
         <tli id="T1787" time="1030.5184285714286" type="appl" />
         <tli id="T1788" time="1030.797" type="appl" />
         <tli id="T1789" time="1031.172" type="appl" />
         <tli id="T1790" time="1031.547" type="appl" />
         <tli id="T1791" time="1031.922" type="appl" />
         <tli id="T1792" time="1032.297" type="appl" />
         <tli id="T1793" time="1032.672" type="appl" />
         <tli id="T1794" time="1033.047" type="appl" />
         <tli id="T1795" time="1033.422" type="appl" />
         <tli id="T1796" time="1035.517" type="appl" />
         <tli id="T1797" time="1036.027" type="appl" />
         <tli id="T1798" time="1036.537" type="appl" />
         <tli id="T1799" time="1038.707" type="appl" />
         <tli id="T1800" time="1039.6385" type="appl" />
         <tli id="T1801" time="1040.57" type="appl" />
         <tli id="T1802" time="1041.413" type="appl" />
         <tli id="T1803" time="1042.256" type="appl" />
         <tli id="T1804" time="1043.099" type="appl" />
         <tli id="T1805" time="1043.942" type="appl" />
         <tli id="T1806" time="1044.785" type="appl" />
         <tli id="T1807" time="1045.628" type="appl" />
         <tli id="T1808" time="1046.471" type="appl" />
         <tli id="T1809" time="1047.314" type="appl" />
         <tli id="T1810" time="1047.457" type="appl" />
         <tli id="T1811" time="1048.137" type="appl" />
         <tli id="T1812" time="1048.817" type="appl" />
         <tli id="T1813" time="1049.497" type="appl" />
         <tli id="T1814" time="1050.177" type="appl" />
         <tli id="T1815" time="1050.857" type="appl" />
         <tli id="T1816" time="1051.537" type="appl" />
         <tli id="T1817" time="1051.847" type="appl" />
         <tli id="T1818" time="1052.7596666666666" type="appl" />
         <tli id="T1819" time="1053.6723333333334" type="appl" />
         <tli id="T1820" time="1054.585" type="appl" />
         <tli id="T1821" time="1055.4976666666666" type="appl" />
         <tli id="T1822" time="1056.4103333333333" type="appl" />
         <tli id="T1823" time="1057.5150831680025" />
         <tli id="T1824" time="1060.457" type="appl" />
         <tli id="T1825" time="1061.987" type="appl" />
         <tli id="T1826" time="1063.517" type="appl" />
         <tli id="T1827" time="1065.047" type="appl" />
         <tli id="T1828" time="1065.107" type="appl" />
         <tli id="T1829" time="1065.7203333333334" type="appl" />
         <tli id="T1830" time="1066.3336666666667" type="appl" />
         <tli id="T1831" time="1066.9470000000001" type="appl" />
         <tli id="T1832" time="1066.997" type="appl" />
         <tli id="T1833" time="1067.6970000000001" type="appl" />
         <tli id="T1834" time="1068.397" type="appl" />
         <tli id="T1835" time="1069.097" type="appl" />
         <tli id="T1836" time="1069.227" type="appl" />
         <tli id="T1837" time="1069.8303333333333" type="appl" />
         <tli id="T1838" time="1070.4336666666668" type="appl" />
         <tli id="T1839" time="1071.037" type="appl" />
         <tli id="T1840" time="1071.6403333333333" type="appl" />
         <tli id="T1841" time="1072.2436666666667" type="appl" />
         <tli id="T1842" time="1072.847" type="appl" />
         <tli id="T1843" time="1072.867" type="appl" />
         <tli id="T1844" time="1073.872" type="appl" />
         <tli id="T1845" time="1074.877" type="appl" />
         <tli id="T1846" time="1077.6770000000001" type="appl" />
         <tli id="T1847" time="1078.1470000000002" type="appl" />
         <tli id="T1848" time="1078.617" type="appl" />
         <tli id="T1849" time="1079.087" type="appl" />
         <tli id="T1850" time="1079.6617058963575" />
         <tli id="T1851" time="1080.069" type="appl" />
         <tli id="T1852" time="1080.6009999999999" type="appl" />
         <tli id="T1853" time="1081.133" type="appl" />
         <tli id="T1854" time="1081.665" type="appl" />
         <tli id="T1855" time="1082.197" type="appl" />
         <tli id="T1856" time="1082.729" type="appl" />
         <tli id="T1857" time="1083.261" type="appl" />
         <tli id="T1858" time="1083.7930000000001" type="appl" />
         <tli id="T1859" time="1084.325" type="appl" />
         <tli id="T1860" time="1084.857" type="appl" />
         <tli id="T1861" time="1085.9470000000001" type="appl" />
         <tli id="T1862" time="1086.6936666666668" type="appl" />
         <tli id="T1863" time="1087.4403333333335" type="appl" />
         <tli id="T1864" time="1088.1870000000001" type="appl" />
         <tli id="T1865" time="1088.364" type="appl" />
         <tli id="T1866" time="1089.4416864931354" />
         <tli id="T1867" time="1090.387" type="appl" />
         <tli id="T1868" time="1090.957" type="appl" />
         <tli id="T1869" time="1091.527" type="appl" />
         <tli id="T1870" time="1092.097" type="appl" />
         <tli id="T1871" time="1092.6670000000001" type="appl" />
         <tli id="T1872" time="1093.237" type="appl" />
         <tli id="T1873" time="1093.807" type="appl" />
         <tli id="T1874" time="1094.057" type="appl" />
         <tli id="T1875" time="1094.8903333333335" type="appl" />
         <tli id="T1876" time="1095.7236666666668" type="appl" />
         <tli id="T1877" time="1096.557" type="appl" />
         <tli id="T1878" time="1099.435" />
         <tli id="T572" time="1885.514" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T52" start="T51">
            <tli id="T51.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1418" start="T1417">
            <tli id="T1417.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Кое-чего</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">позабывала</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_10" n="HIAT:w" s="T3">я</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_13" n="sc" s="T16">
               <ts e="T22" id="Seg_15" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_17" n="HIAT:w" s="T16">У</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_20" n="HIAT:w" s="T17">моей</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_23" n="HIAT:w" s="T18">племянницы</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_26" n="HIAT:w" s="T19">дочка</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_29" n="HIAT:w" s="T20">вышла</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_32" n="HIAT:w" s="T21">замуж</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_35" n="sc" s="T23">
               <ts e="T31" id="Seg_37" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_39" n="HIAT:w" s="T23">И</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_42" n="HIAT:w" s="T24">поехали</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_45" n="HIAT:w" s="T25">туды</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_48" n="HIAT:w" s="T26">по</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_51" n="HIAT:w" s="T27">Мане</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_54" n="HIAT:w" s="T28">строить</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_57" n="HIAT:w" s="T29">железную</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_60" n="HIAT:w" s="T30">дорогу</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T40" id="Seg_63" n="sc" s="T32">
               <ts e="T40" id="Seg_65" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_67" n="HIAT:w" s="T32">И</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_70" n="HIAT:w" s="T33">оттедова</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_72" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_74" n="HIAT:w" s="T34">уех-</ts>
                  <nts id="Seg_75" n="HIAT:ip">)</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_78" n="HIAT:w" s="T35">всё</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_81" n="HIAT:w" s="T36">строили</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_84" n="HIAT:w" s="T37">дотель</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_87" n="HIAT:w" s="T38">до</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_90" n="HIAT:w" s="T39">севера</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_93" n="sc" s="T41">
               <ts e="T48" id="Seg_95" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_97" n="HIAT:w" s="T41">И</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_100" n="HIAT:w" s="T42">потом</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_103" n="HIAT:w" s="T43">она</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_106" n="HIAT:w" s="T44">приехала</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_109" n="HIAT:w" s="T45">к</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_112" n="HIAT:w" s="T46">матери</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_116" n="HIAT:w" s="T47">гостить</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_119" n="sc" s="T49">
               <ts e="T53" id="Seg_121" n="HIAT:u" s="T49">
                  <nts id="Seg_122" n="HIAT:ip">(</nts>
                  <nts id="Seg_123" n="HIAT:ip">(</nts>
                  <ats e="T51" id="Seg_124" n="HIAT:non-pho" s="T49">BRK</ats>
                  <nts id="Seg_125" n="HIAT:ip">)</nts>
                  <nts id="Seg_126" n="HIAT:ip">)</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_128" n="HIAT:ip">(</nts>
                  <ts e="T51.tx-PKZ.1" id="Seg_130" n="HIAT:w" s="T51">-ла</ts>
                  <nts id="Seg_131" n="HIAT:ip">)</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_134" n="HIAT:w" s="T51.tx-PKZ.1">его</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_137" n="HIAT:w" s="T52">похоронила</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T58" id="Seg_140" n="sc" s="T54">
               <ts e="T58" id="Seg_142" n="HIAT:u" s="T54">
                  <nts id="Seg_143" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_145" n="HIAT:w" s="T54">И</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_148" n="HIAT:w" s="T55">тог-</ts>
                  <nts id="Seg_149" n="HIAT:ip">)</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_152" n="HIAT:w" s="T56">Ну</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_155" n="HIAT:w" s="T57">это</ts>
                  <nts id="Seg_156" n="HIAT:ip">…</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T65" id="Seg_158" n="sc" s="T59">
               <ts e="T65" id="Seg_160" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_162" n="HIAT:w" s="T59">Я</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_165" n="HIAT:w" s="T60">по-русски</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_167" n="HIAT:ip">(</nts>
                  <ts e="T62" id="Seg_169" n="HIAT:w" s="T61">ска-</ts>
                  <nts id="Seg_170" n="HIAT:ip">)</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_173" n="HIAT:w" s="T62">сказать</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_177" n="HIAT:w" s="T63">опять</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_180" n="HIAT:w" s="T64">забуду</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T69" id="Seg_183" n="sc" s="T68">
               <ts e="T69" id="Seg_185" n="HIAT:u" s="T68">
                  <nts id="Seg_186" n="HIAT:ip">(</nts>
                  <nts id="Seg_187" n="HIAT:ip">(</nts>
                  <ats e="T69" id="Seg_188" n="HIAT:non-pho" s="T68">BRK</ats>
                  <nts id="Seg_189" n="HIAT:ip">)</nts>
                  <nts id="Seg_190" n="HIAT:ip">)</nts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T82" id="Seg_193" n="sc" s="T77">
               <ts e="T82" id="Seg_195" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_197" n="HIAT:w" s="T77">У</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_200" n="HIAT:w" s="T78">вас</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_203" n="HIAT:w" s="T79">какой-то</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_206" n="HIAT:w" s="T80">ветер</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ats e="T82" id="Seg_210" n="HIAT:non-pho" s="T81">NOISE</ats>
                  <nts id="Seg_211" n="HIAT:ip">)</nts>
                  <nts id="Seg_212" n="HIAT:ip">)</nts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T92" id="Seg_215" n="sc" s="T84">
               <ts e="T92" id="Seg_217" n="HIAT:u" s="T84">
                  <ts e="T86" id="Seg_219" n="HIAT:w" s="T84">У</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_222" n="HIAT:w" s="T86">нас</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_225" n="HIAT:w" s="T88">сухой</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_228" n="HIAT:w" s="T90">ветер</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_231" n="sc" s="T96">
               <ts e="T118" id="Seg_233" n="HIAT:u" s="T96">
                  <ts e="T99" id="Seg_235" n="HIAT:w" s="T96">Да</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_240" n="HIAT:w" s="T99">ши-</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_244" n="HIAT:w" s="T101">шибко</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_247" n="HIAT:w" s="T103">у</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_250" n="HIAT:w" s="T104">вас</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_253" n="HIAT:w" s="T105">ветер</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_257" n="HIAT:w" s="T106">у</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_260" n="HIAT:w" s="T107">нас</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_263" n="HIAT:w" s="T108">такого</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_266" n="HIAT:w" s="T110">нету</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_270" n="HIAT:w" s="T112">как</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_273" n="HIAT:w" s="T114">у</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_275" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_277" n="HIAT:w" s="T116">вас</ts>
                  <nts id="Seg_278" n="HIAT:ip">)</nts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T133" id="Seg_281" n="sc" s="T120">
               <ts e="T133" id="Seg_283" n="HIAT:u" s="T120">
                  <ts e="T122" id="Seg_285" n="HIAT:w" s="T120">Видала</ts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_289" n="HIAT:w" s="T122">только</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_291" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_293" n="HIAT:w" s="T125">с-</ts>
                  <nts id="Seg_294" n="HIAT:ip">)</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_297" n="HIAT:w" s="T127">далеко</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_301" n="HIAT:w" s="T129">с</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_304" n="HIAT:w" s="T131">далека</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_307" n="sc" s="T134">
               <ts e="T137" id="Seg_309" n="HIAT:u" s="T134">
                  <ts e="T136" id="Seg_311" n="HIAT:w" s="T134">Там</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_314" n="HIAT:w" s="T136">была</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_317" n="sc" s="T138">
               <ts e="T141" id="Seg_319" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_321" n="HIAT:w" s="T138">Где</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_324" n="HIAT:w" s="T139">Маргарита</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_327" n="HIAT:w" s="T140">толстая</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_330" n="sc" s="T144">
               <ts e="T149" id="Seg_332" n="HIAT:u" s="T144">
                  <ts e="T146" id="Seg_334" n="HIAT:w" s="T144">За</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_337" n="HIAT:w" s="T146">её</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_340" n="HIAT:w" s="T147">проходила</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_343" n="HIAT:w" s="T148">туды</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_347" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_349" n="HIAT:w" s="T149">Ну</ts>
                  <nts id="Seg_350" n="HIAT:ip">,</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_353" n="HIAT:w" s="T150">видать</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_356" n="HIAT:w" s="T152">так</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_359" n="HIAT:w" s="T153">далеко</ts>
                  <nts id="Seg_360" n="HIAT:ip">,</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_363" n="HIAT:w" s="T154">но</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_366" n="HIAT:w" s="T156">близко</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_369" n="HIAT:w" s="T157">не</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_372" n="HIAT:w" s="T158">ходили</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_376" n="HIAT:w" s="T159">далеко</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_379" n="sc" s="T173">
               <ts e="T181" id="Seg_381" n="HIAT:u" s="T173">
                  <nts id="Seg_382" n="HIAT:ip">(</nts>
                  <nts id="Seg_383" n="HIAT:ip">(</nts>
                  <ats e="T174" id="Seg_384" n="HIAT:non-pho" s="T173">DMG</ats>
                  <nts id="Seg_385" n="HIAT:ip">)</nts>
                  <nts id="Seg_386" n="HIAT:ip">)</nts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_390" n="HIAT:w" s="T174">а</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_393" n="HIAT:w" s="T176">вышло</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_396" n="HIAT:w" s="T177">ли</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_400" n="HIAT:w" s="T178">нет</ts>
                  <nts id="Seg_401" n="HIAT:ip">,</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_404" n="HIAT:w" s="T179">не</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_407" n="HIAT:w" s="T180">знаю</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T184" id="Seg_410" n="sc" s="T182">
               <ts e="T184" id="Seg_412" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_414" n="HIAT:w" s="T182">По-русски</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_417" n="HIAT:w" s="T183">говорить</ts>
                  <nts id="Seg_418" n="HIAT:ip">?</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T194" id="Seg_420" n="sc" s="T187">
               <ts e="T194" id="Seg_422" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_424" n="HIAT:w" s="T187">У</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_426" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_428" n="HIAT:w" s="T188">м-</ts>
                  <nts id="Seg_429" n="HIAT:ip">)</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_432" n="HIAT:w" s="T189">моёй</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_435" n="HIAT:w" s="T190">племянницы</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_438" n="HIAT:w" s="T191">дочка</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_441" n="HIAT:w" s="T192">вышла</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_444" n="HIAT:w" s="T193">замуж</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T207" id="Seg_447" n="sc" s="T195">
               <ts e="T207" id="Seg_449" n="HIAT:u" s="T195">
                  <nts id="Seg_450" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_452" n="HIAT:w" s="T195">У</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_455" n="HIAT:w" s="T196">н-</ts>
                  <nts id="Seg_456" n="HIAT:ip">)</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_458" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_460" n="HIAT:w" s="T197">А</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_463" n="HIAT:w" s="T198">ты</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_467" n="HIAT:w" s="T199">говорит</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_470" n="HIAT:w" s="T200">=</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_473" n="HIAT:w" s="T201">А</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_476" n="HIAT:w" s="T202">я</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_479" n="HIAT:w" s="T203">говорю</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_482" n="HIAT:w" s="T204">а</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_485" n="HIAT:w" s="T205">вы</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_488" n="HIAT:w" s="T206">как</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_492" n="sc" s="T208">
               <ts e="T224" id="Seg_494" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_496" n="HIAT:w" s="T208">Я</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_500" n="HIAT:w" s="T209">говорит</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_504" n="HIAT:w" s="T210">учительница</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_507" n="HIAT:w" s="T211">Агафон</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_510" n="HIAT:w" s="T212">Ивановича</ts>
                  <nts id="Seg_511" n="HIAT:ip">,</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_513" n="HIAT:ip">(</nts>
                  <nts id="Seg_514" n="HIAT:ip">(</nts>
                  <ats e="T214" id="Seg_515" n="HIAT:non-pho" s="T213">DMG</ats>
                  <nts id="Seg_516" n="HIAT:ip">)</nts>
                  <nts id="Seg_517" n="HIAT:ip">)</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_520" n="HIAT:w" s="T214">он</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_523" n="HIAT:w" s="T215">мне</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_526" n="HIAT:w" s="T217">рассказывает</ts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_530" n="HIAT:w" s="T218">ну</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_533" n="HIAT:w" s="T220">я</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_536" n="HIAT:w" s="T222">по-русски</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_539" n="sc" s="T225">
               <ts e="T227" id="Seg_541" n="HIAT:u" s="T225">
                  <ts e="T227" id="Seg_543" n="HIAT:w" s="T225">Ага</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_546" n="sc" s="T228">
               <ts e="T231" id="Seg_548" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_550" n="HIAT:w" s="T228">Ну</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_553" n="HIAT:w" s="T229">я</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_555" n="HIAT:ip">(</nts>
                  <nts id="Seg_556" n="HIAT:ip">(</nts>
                  <ats e="T231" id="Seg_557" n="HIAT:non-pho" s="T230">…</ats>
                  <nts id="Seg_558" n="HIAT:ip">)</nts>
                  <nts id="Seg_559" n="HIAT:ip">)</nts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_562" n="sc" s="T232">
               <ts e="T238" id="Seg_564" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_566" n="HIAT:w" s="T232">Тут</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_569" n="HIAT:w" s="T233">посидели</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_572" n="HIAT:w" s="T234">там</ts>
                  <nts id="Seg_573" n="HIAT:ip">,</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_576" n="HIAT:w" s="T235">разговаривали</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_578" n="HIAT:ip">(</nts>
                  <nts id="Seg_579" n="HIAT:ip">(</nts>
                  <ats e="T238" id="Seg_580" n="HIAT:non-pho" s="T236">DMG</ats>
                  <nts id="Seg_581" n="HIAT:ip">)</nts>
                  <nts id="Seg_582" n="HIAT:ip">)</nts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_585" n="sc" s="T240">
               <ts e="T242" id="Seg_587" n="HIAT:u" s="T240">
                  <ts e="T242" id="Seg_589" n="HIAT:w" s="T240">Ну</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_593" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_595" n="HIAT:w" s="T242">Там</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T249" id="Seg_598" n="sc" s="T244">
               <ts e="T249" id="Seg_600" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_602" n="HIAT:w" s="T244">Мой</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_605" n="HIAT:w" s="T245">брат</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_608" n="HIAT:w" s="T246">сродный</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_611" n="HIAT:w" s="T247">туды</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_614" n="HIAT:w" s="T248">ездил</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T251" id="Seg_617" n="sc" s="T250">
               <ts e="T251" id="Seg_619" n="HIAT:u" s="T250">
                  <nts id="Seg_620" n="HIAT:ip">(</nts>
                  <nts id="Seg_621" n="HIAT:ip">(</nts>
                  <ats e="T251" id="Seg_622" n="HIAT:non-pho" s="T250">BRK</ats>
                  <nts id="Seg_623" n="HIAT:ip">)</nts>
                  <nts id="Seg_624" n="HIAT:ip">)</nts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T254" id="Seg_627" n="sc" s="T252">
               <ts e="T254" id="Seg_629" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_631" n="HIAT:w" s="T252">Ну</ts>
                  <nts id="Seg_632" n="HIAT:ip">,</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_635" n="HIAT:w" s="T253">это</ts>
                  <nts id="Seg_636" n="HIAT:ip">…</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T263" id="Seg_638" n="sc" s="T255">
               <ts e="T258" id="Seg_640" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_642" n="HIAT:w" s="T255">Мальчишка</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <nts id="Seg_645" n="HIAT:ip">(</nts>
                  <ats e="T258" id="Seg_646" n="HIAT:non-pho" s="T256">…</ats>
                  <nts id="Seg_647" n="HIAT:ip">)</nts>
                  <nts id="Seg_648" n="HIAT:ip">)</nts>
                  <nts id="Seg_649" n="HIAT:ip">.</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_652" n="HIAT:u" s="T258">
                  <nts id="Seg_653" n="HIAT:ip">(</nts>
                  <ts e="T259" id="Seg_655" n="HIAT:w" s="T258">никак</ts>
                  <nts id="Seg_656" n="HIAT:ip">)</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_659" n="HIAT:w" s="T259">не</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_662" n="HIAT:w" s="T260">выговорю</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_666" n="HIAT:u" s="T261">
                  <nts id="Seg_667" n="HIAT:ip">(</nts>
                  <ts e="T262" id="Seg_669" n="HIAT:w" s="T261">Iže-</ts>
                  <nts id="Seg_670" n="HIAT:ip">)</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_673" n="HIAT:w" s="T262">Izeŋ</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T268" id="Seg_676" n="sc" s="T264">
               <ts e="T266" id="Seg_678" n="HIAT:u" s="T264">
                  <nts id="Seg_679" n="HIAT:ip">(</nts>
                  <ts e="T265" id="Seg_681" n="HIAT:w" s="T264">Iž-</ts>
                  <nts id="Seg_682" n="HIAT:ip">)</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_685" n="HIAT:w" s="T265">Izeŋ</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_689" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_691" n="HIAT:w" s="T266">Мать</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_694" n="HIAT:w" s="T267">где</ts>
                  <nts id="Seg_695" n="HIAT:ip">?</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T274" id="Seg_697" n="sc" s="T269">
               <ts e="T274" id="Seg_699" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_701" n="HIAT:w" s="T269">Sargol</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_703" n="HIAT:ip">(</nts>
                  <ts e="T271" id="Seg_705" n="HIAT:w" s="T270">tura-</ts>
                  <nts id="Seg_706" n="HIAT:ip">)</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_709" n="HIAT:w" s="T271">Sargol</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_712" n="HIAT:w" s="T272">derʼevnʼanə</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_715" n="HIAT:w" s="T273">barustan</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_718" n="sc" s="T275">
               <ts e="T280" id="Seg_720" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_722" n="HIAT:w" s="T275">В</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_725" n="HIAT:w" s="T276">Сарголову</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_728" n="HIAT:w" s="T277">деревню</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_731" n="HIAT:w" s="T278">ушла</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_735" n="HIAT:u" s="T280">
                  <ts e="T282" id="Seg_737" n="HIAT:w" s="T280">Я</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_740" n="HIAT:w" s="T282">тот</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_743" n="HIAT:w" s="T284">язык</ts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_747" n="HIAT:w" s="T286">вот</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_750" n="HIAT:w" s="T288">так</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_753" n="HIAT:w" s="T291">говорили</ts>
                  <nts id="Seg_754" n="HIAT:ip">,</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_757" n="HIAT:w" s="T293">а</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_760" n="HIAT:w" s="T294">плохо</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_763" n="HIAT:w" s="T296">знаю</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_766" n="HIAT:w" s="T297">я</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_769" n="HIAT:w" s="T299">его</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_772" n="sc" s="T306">
               <ts e="T314" id="Seg_774" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_776" n="HIAT:w" s="T306">И</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_779" n="HIAT:w" s="T307">на</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_782" n="HIAT:w" s="T311">его</ts>
                  <nts id="Seg_783" n="HIAT:ip">…</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T336" id="Seg_785" n="sc" s="T322">
               <ts e="T336" id="Seg_787" n="HIAT:u" s="T322">
                  <ts e="T324" id="Seg_789" n="HIAT:w" s="T322">Ну</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_792" n="HIAT:w" s="T324">вот</ts>
                  <nts id="Seg_793" n="HIAT:ip">,</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_796" n="HIAT:w" s="T327">и</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_799" n="HIAT:w" s="T329">уфимские</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_802" n="HIAT:w" s="T332">есть</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_805" n="HIAT:w" s="T335">татары</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_808" n="sc" s="T337">
               <ts e="T342" id="Seg_810" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_812" n="HIAT:w" s="T337">Казанские</ts>
                  <nts id="Seg_813" n="HIAT:ip">,</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_816" n="HIAT:w" s="T338">они</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_819" n="HIAT:w" s="T339">на</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_822" n="HIAT:w" s="T340">этот</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_825" n="HIAT:w" s="T341">язык</ts>
                  <nts id="Seg_826" n="HIAT:ip">.</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_829" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_831" n="HIAT:w" s="T342">Вот</ts>
                  <nts id="Seg_832" n="HIAT:ip">,</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_835" n="HIAT:w" s="T343">один</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_838" n="HIAT:w" s="T344">раз</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_841" n="HIAT:w" s="T345">приехали</ts>
                  <nts id="Seg_842" n="HIAT:ip">.</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T352" id="Seg_844" n="sc" s="T347">
               <ts e="T349" id="Seg_846" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_848" n="HIAT:w" s="T347">Трое</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_851" n="HIAT:w" s="T348">приехали</ts>
                  <nts id="Seg_852" n="HIAT:ip">.</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_855" n="HIAT:u" s="T349">
                  <ts e="T350" id="Seg_857" n="HIAT:w" s="T349">Кто</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_860" n="HIAT:w" s="T350">там</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_863" n="HIAT:w" s="T351">приехал</ts>
                  <nts id="Seg_864" n="HIAT:ip">?</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_866" n="sc" s="T353">
               <ts e="T358" id="Seg_868" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_870" n="HIAT:w" s="T353">Говорит:</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_873" n="HIAT:w" s="T354">бер</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_876" n="HIAT:w" s="T355">жид</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_880" n="HIAT:w" s="T356">бер</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_883" n="HIAT:w" s="T357">татарин</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T368" id="Seg_886" n="sc" s="T359">
               <ts e="T363" id="Seg_888" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_890" n="HIAT:w" s="T359">Один</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_893" n="HIAT:w" s="T360">жид</ts>
                  <nts id="Seg_894" n="HIAT:ip">,</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_897" n="HIAT:w" s="T361">один</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_900" n="HIAT:w" s="T362">татарин</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_904" n="HIAT:u" s="T363">
                  <nts id="Seg_905" n="HIAT:ip">(</nts>
                  <nts id="Seg_906" n="HIAT:ip">(</nts>
                  <ats e="T364" id="Seg_907" n="HIAT:non-pho" s="T363">…</ats>
                  <nts id="Seg_908" n="HIAT:ip">)</nts>
                  <nts id="Seg_909" n="HIAT:ip">)</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_912" n="HIAT:w" s="T364">на</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_915" n="HIAT:w" s="T365">тот</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_918" n="HIAT:w" s="T367">язык</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T385" id="Seg_921" n="sc" s="T370">
               <ts e="T375" id="Seg_923" n="HIAT:u" s="T370">
                  <ts e="T372" id="Seg_925" n="HIAT:w" s="T370">На</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_928" n="HIAT:w" s="T372">тот</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_931" n="HIAT:w" s="T373">язык</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_935" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_937" n="HIAT:w" s="T375">А</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_940" n="HIAT:w" s="T376">по</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_943" n="HIAT:w" s="T377">нашему:</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_946" n="HIAT:w" s="T378">Onʼiʔ</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_949" n="HIAT:w" s="T379">žid</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_952" n="HIAT:w" s="T380">šobi</ts>
                  <nts id="Seg_953" n="HIAT:ip">,</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_956" n="HIAT:w" s="T381">onʼiʔ</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_959" n="HIAT:w" s="T382">nu</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_962" n="HIAT:w" s="T383">kuza</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_965" n="HIAT:w" s="T384">šobi</ts>
                  <nts id="Seg_966" n="HIAT:ip">.</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T391" id="Seg_968" n="sc" s="T386">
               <ts e="T391" id="Seg_970" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_972" n="HIAT:w" s="T386">Один</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_975" n="HIAT:w" s="T387">жид</ts>
                  <nts id="Seg_976" n="HIAT:ip">,</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_979" n="HIAT:w" s="T388">один</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_982" n="HIAT:w" s="T390">татарин</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T394" id="Seg_985" n="sc" s="T393">
               <ts e="T394" id="Seg_987" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_989" n="HIAT:w" s="T393">Nuzaŋ</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T400" id="Seg_992" n="sc" s="T395">
               <ts e="T400" id="Seg_994" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_996" n="HIAT:w" s="T395">Dĭgəttə</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_999" n="HIAT:w" s="T396">plʼemʼannʼica</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1002" n="HIAT:w" s="T397">măna</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1005" n="HIAT:w" s="T398">noʔ</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1008" n="HIAT:w" s="T399">kambi</ts>
                  <nts id="Seg_1009" n="HIAT:ip">.</nts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T405" id="Seg_1011" n="sc" s="T401">
               <ts e="T405" id="Seg_1013" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1015" n="HIAT:w" s="T401">Miʔ</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1018" n="HIAT:w" s="T402">dĭ</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1021" n="HIAT:w" s="T403">kopnaʔi</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1024" n="HIAT:w" s="T404">embibeʔ</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_1027" n="sc" s="T406">
               <ts e="T410" id="Seg_1029" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1031" n="HIAT:w" s="T406">Dĭgəttə</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1034" n="HIAT:w" s="T407">onʼiʔ</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1036" n="HIAT:ip">(</nts>
                  <ts e="T409" id="Seg_1038" n="HIAT:w" s="T408">u-</ts>
                  <nts id="Seg_1039" n="HIAT:ip">)</nts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1042" n="HIAT:w" s="T409">üzəbi</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T416" id="Seg_1045" n="sc" s="T411">
               <ts e="T413" id="Seg_1047" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1049" n="HIAT:w" s="T411">Măn</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1052" n="HIAT:w" s="T412">mămbiam</ts>
                  <nts id="Seg_1053" n="HIAT:ip">…</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1056" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1058" n="HIAT:w" s="T413">Mĭʔ</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1061" n="HIAT:w" s="T414">nagurbəʔ</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1064" n="HIAT:w" s="T415">ibibeʔ</ts>
                  <nts id="Seg_1065" n="HIAT:ip">.</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_1067" n="sc" s="T417">
               <ts e="T424" id="Seg_1069" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1071" n="HIAT:w" s="T417">Măn</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1074" n="HIAT:w" s="T418">mămbiam:</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1076" n="HIAT:ip">"</nts>
                  <ts e="T421" id="Seg_1078" n="HIAT:w" s="T420">No</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1081" n="HIAT:w" s="T421">kut</ts>
                  <nts id="Seg_1082" n="HIAT:ip">,</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1085" n="HIAT:w" s="T422">kodu</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1088" n="HIAT:w" s="T423">saʔməlupi</ts>
                  <nts id="Seg_1089" n="HIAT:ip">"</nts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T430" id="Seg_1092" n="sc" s="T425">
               <ts e="T430" id="Seg_1094" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1096" n="HIAT:w" s="T425">Măn</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1099" n="HIAT:w" s="T426">mămbiam:</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1101" n="HIAT:ip">"</nts>
                  <ts e="T428" id="Seg_1103" n="HIAT:w" s="T427">Šində-nʼibudʼ</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1105" n="HIAT:ip">(</nts>
                  <ts e="T429" id="Seg_1107" n="HIAT:w" s="T428">kuna-</ts>
                  <nts id="Seg_1108" n="HIAT:ip">)</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1111" n="HIAT:w" s="T429">külalləj</ts>
                  <nts id="Seg_1112" n="HIAT:ip">"</nts>
                  <nts id="Seg_1113" n="HIAT:ip">.</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T437" id="Seg_1115" n="sc" s="T431">
               <ts e="T437" id="Seg_1117" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1119" n="HIAT:w" s="T431">Dĭgəttə</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1122" n="HIAT:w" s="T432">măn</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1125" n="HIAT:w" s="T433">mălliam:</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1127" n="HIAT:ip">"</nts>
                  <ts e="T435" id="Seg_1129" n="HIAT:w" s="T434">Dĭ</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1132" n="HIAT:w" s="T435">măn</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1135" n="HIAT:w" s="T436">külalləm</ts>
                  <nts id="Seg_1136" n="HIAT:ip">.</nts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T441" id="Seg_1138" n="sc" s="T438">
               <ts e="T441" id="Seg_1140" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1142" n="HIAT:w" s="T438">Măn</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1145" n="HIAT:w" s="T439">dʼaktə</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1148" n="HIAT:w" s="T440">vedʼ</ts>
                  <nts id="Seg_1149" n="HIAT:ip">.</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T447" id="Seg_1151" n="sc" s="T442">
               <ts e="T447" id="Seg_1153" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1155" n="HIAT:w" s="T442">A</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1158" n="HIAT:w" s="T443">šiʔ</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1161" n="HIAT:w" s="T444">išo</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1164" n="HIAT:w" s="T445">ej</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1167" n="HIAT:w" s="T446">dʼaktə</ts>
                  <nts id="Seg_1168" n="HIAT:ip">.</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T452" id="Seg_1170" n="sc" s="T448">
               <ts e="T452" id="Seg_1172" n="HIAT:u" s="T448">
                  <nts id="Seg_1173" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_1175" n="HIAT:w" s="T448">Dĭg-</ts>
                  <nts id="Seg_1176" n="HIAT:ip">)</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1179" n="HIAT:w" s="T449">Dĭgəttə</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1182" n="HIAT:w" s="T450">maʔnʼibaʔ</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1185" n="HIAT:w" s="T451">šobibaʔ</ts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T463" id="Seg_1188" n="sc" s="T453">
               <ts e="T463" id="Seg_1190" n="HIAT:u" s="T453">
                  <nts id="Seg_1191" n="HIAT:ip">(</nts>
                  <ts e="T454" id="Seg_1193" n="HIAT:w" s="T453">Măn=</ts>
                  <nts id="Seg_1194" n="HIAT:ip">)</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1197" n="HIAT:w" s="T454">Kamen</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1199" n="HIAT:ip">(</nts>
                  <ts e="T456" id="Seg_1201" n="HIAT:w" s="T455">dĭ=</ts>
                  <nts id="Seg_1202" n="HIAT:ip">)</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1205" n="HIAT:w" s="T456">dĭ</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1208" n="HIAT:w" s="T457">šobi</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1211" n="HIAT:w" s="T458">döbər</ts>
                  <nts id="Seg_1212" n="HIAT:ip">,</nts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1215" n="HIAT:w" s="T459">măn</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1218" n="HIAT:w" s="T460">mĭmbiem</ts>
                  <nts id="Seg_1219" n="HIAT:ip">,</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1222" n="HIAT:w" s="T461">köbürgən</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1225" n="HIAT:w" s="T462">nĭŋgəbiem</ts>
                  <nts id="Seg_1226" n="HIAT:ip">.</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T471" id="Seg_1228" n="sc" s="T464">
               <ts e="T471" id="Seg_1230" n="HIAT:u" s="T464">
                  <nts id="Seg_1231" n="HIAT:ip">(</nts>
                  <ts e="T465" id="Seg_1233" n="HIAT:w" s="T464">Š-</ts>
                  <nts id="Seg_1234" n="HIAT:ip">)</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1237" n="HIAT:w" s="T465">Šomi</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1240" n="HIAT:w" s="T466">pa</ts>
                  <nts id="Seg_1241" n="HIAT:ip">,</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1244" n="HIAT:w" s="T467">beržə</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1247" n="HIAT:w" s="T468">nagobi</ts>
                  <nts id="Seg_1248" n="HIAT:ip">,</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1251" n="HIAT:w" s="T469">dʼünə</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1254" n="HIAT:w" s="T470">saʔməluʔpi</ts>
                  <nts id="Seg_1255" n="HIAT:ip">.</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T487" id="Seg_1257" n="sc" s="T472">
               <ts e="T487" id="Seg_1259" n="HIAT:u" s="T472">
                  <nts id="Seg_1260" n="HIAT:ip">(</nts>
                  <ts e="T473" id="Seg_1262" n="HIAT:w" s="T472">Măn=</ts>
                  <nts id="Seg_1263" n="HIAT:ip">)</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1266" n="HIAT:w" s="T473">Măn</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1268" n="HIAT:ip">(</nts>
                  <ts e="T475" id="Seg_1270" n="HIAT:w" s="T474">u</ts>
                  <nts id="Seg_1271" n="HIAT:ip">)</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1274" n="HIAT:w" s="T475">šindinədə</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1277" n="HIAT:w" s="T476">ej</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1280" n="HIAT:w" s="T477">mămbiam</ts>
                  <nts id="Seg_1281" n="HIAT:ip">,</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1284" n="HIAT:w" s="T478">a</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1287" n="HIAT:w" s="T479">dĭgəttə</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1290" n="HIAT:w" s="T480">dĭn</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1293" n="HIAT:w" s="T481">tibit</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1296" n="HIAT:w" s="T482">kamen</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1299" n="HIAT:w" s="T483">bügən</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1302" n="HIAT:w" s="T484">külambi</ts>
                  <nts id="Seg_1303" n="HIAT:ip">,</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1306" n="HIAT:w" s="T485">măn</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1309" n="HIAT:w" s="T486">mămbiam</ts>
                  <nts id="Seg_1310" n="HIAT:ip">.</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T493" id="Seg_1312" n="sc" s="T488">
               <ts e="T493" id="Seg_1314" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1316" n="HIAT:w" s="T488">Когда</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1319" n="HIAT:w" s="T489">еёный</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1322" n="HIAT:w" s="T490">муж</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1325" n="HIAT:w" s="T491">утонул</ts>
                  <nts id="Seg_1326" n="HIAT:ip">,</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1329" n="HIAT:w" s="T492">я</ts>
                  <nts id="Seg_1330" n="HIAT:ip">…</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T506" id="Seg_1332" n="sc" s="T494">
               <ts e="T506" id="Seg_1334" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1336" n="HIAT:w" s="T494">Она</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1339" n="HIAT:w" s="T495">у</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1342" n="HIAT:w" s="T496">меня</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1345" n="HIAT:w" s="T497">работала</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1348" n="HIAT:w" s="T498">на</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1351" n="HIAT:w" s="T499">покосе</ts>
                  <nts id="Seg_1352" n="HIAT:ip">,</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1355" n="HIAT:w" s="T500">копну</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1358" n="HIAT:w" s="T501">поклала</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1361" n="HIAT:w" s="T502">хорошую</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1365" n="HIAT:w" s="T503">это</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1368" n="HIAT:w" s="T504">я</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1371" n="HIAT:w" s="T505">рассказывала</ts>
                  <nts id="Seg_1372" n="HIAT:ip">.</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T511" id="Seg_1374" n="sc" s="T507">
               <ts e="T511" id="Seg_1376" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1378" n="HIAT:w" s="T507">Она</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1381" n="HIAT:w" s="T508">стояла</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1384" n="HIAT:w" s="T509">и</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1387" n="HIAT:w" s="T510">упала</ts>
                  <nts id="Seg_1388" n="HIAT:ip">.</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T540" id="Seg_1390" n="sc" s="T512">
               <ts e="T528" id="Seg_1392" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1394" n="HIAT:w" s="T512">А</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1397" n="HIAT:w" s="T513">нас</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1400" n="HIAT:w" s="T514">было</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1403" n="HIAT:w" s="T515">трое</ts>
                  <nts id="Seg_1404" n="HIAT:ip">,</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1406" n="HIAT:ip">(</nts>
                  <ts e="T517" id="Seg_1408" n="HIAT:w" s="T516">а</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1411" n="HIAT:w" s="T517">я</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1414" n="HIAT:w" s="T518">говорю=</ts>
                  <nts id="Seg_1415" n="HIAT:ip">)</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1418" n="HIAT:w" s="T519">а</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1421" n="HIAT:w" s="T520">я</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1423" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_1425" n="HIAT:w" s="T521">ска-</ts>
                  <nts id="Seg_1426" n="HIAT:ip">)</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1429" n="HIAT:w" s="T522">сказала:</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1431" n="HIAT:ip">"</nts>
                  <ts e="T524" id="Seg_1433" n="HIAT:w" s="T523">Это</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1436" n="HIAT:w" s="T524">кто-то</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1439" n="HIAT:w" s="T525">из</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1442" n="HIAT:w" s="T526">нас</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1445" n="HIAT:w" s="T527">помрет</ts>
                  <nts id="Seg_1446" n="HIAT:ip">"</nts>
                  <nts id="Seg_1447" n="HIAT:ip">.</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_1450" n="HIAT:u" s="T528">
                  <ts e="T529" id="Seg_1452" n="HIAT:w" s="T528">А</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1455" n="HIAT:w" s="T529">тады</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1458" n="HIAT:w" s="T530">думаю</ts>
                  <nts id="Seg_1459" n="HIAT:ip">,</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1462" n="HIAT:w" s="T531">говорю</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1465" n="HIAT:w" s="T532">имя:</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1467" n="HIAT:ip">"</nts>
                  <ts e="T535" id="Seg_1469" n="HIAT:w" s="T534">Это</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1472" n="HIAT:w" s="T535">я</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1475" n="HIAT:w" s="T536">помру</ts>
                  <nts id="Seg_1476" n="HIAT:ip">,</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1479" n="HIAT:w" s="T537">я</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1482" n="HIAT:w" s="T538">старше</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1485" n="HIAT:w" s="T539">вас</ts>
                  <nts id="Seg_1486" n="HIAT:ip">"</nts>
                  <nts id="Seg_1487" n="HIAT:ip">.</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T550" id="Seg_1489" n="sc" s="T541">
               <ts e="T550" id="Seg_1491" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1493" n="HIAT:w" s="T541">А</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1496" n="HIAT:w" s="T542">тагды</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1499" n="HIAT:w" s="T543">ходила</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1502" n="HIAT:w" s="T544">я</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1505" n="HIAT:w" s="T545">на</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1508" n="HIAT:w" s="T546">поле</ts>
                  <nts id="Seg_1509" n="HIAT:ip">,</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1512" n="HIAT:w" s="T547">лук</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1515" n="HIAT:w" s="T548">рвала</ts>
                  <nts id="Seg_1516" n="HIAT:ip">,</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1519" n="HIAT:w" s="T549">полевой</ts>
                  <nts id="Seg_1520" n="HIAT:ip">.</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T558" id="Seg_1522" n="sc" s="T551">
               <ts e="T558" id="Seg_1524" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_1526" n="HIAT:w" s="T551">И</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1529" n="HIAT:w" s="T552">ветру</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1532" n="HIAT:w" s="T553">не</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1535" n="HIAT:w" s="T554">было</ts>
                  <nts id="Seg_1536" n="HIAT:ip">,</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1539" n="HIAT:w" s="T555">листвяга</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1542" n="HIAT:w" s="T556">упала</ts>
                  <nts id="Seg_1543" n="HIAT:ip">,</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1546" n="HIAT:w" s="T557">толстая</ts>
                  <nts id="Seg_1547" n="HIAT:ip">!</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T569" id="Seg_1549" n="sc" s="T559">
               <ts e="T569" id="Seg_1551" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_1553" n="HIAT:w" s="T559">Я</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1556" n="HIAT:w" s="T560">так</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1559" n="HIAT:w" s="T561">подумала:</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1562" n="HIAT:w" s="T562">кто-то</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1565" n="HIAT:w" s="T563">помрет</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1569" n="HIAT:w" s="T564">и</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1572" n="HIAT:w" s="T565">правда</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1575" n="HIAT:w" s="T566">помер</ts>
                  <nts id="Seg_1576" n="HIAT:ip">,</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1579" n="HIAT:w" s="T567">вот</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1582" n="HIAT:w" s="T568">этот</ts>
                  <nts id="Seg_1583" n="HIAT:ip">.</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_1585" n="sc" s="T570">
               <ts e="T571" id="Seg_1587" n="HIAT:u" s="T570">
                  <nts id="Seg_1588" n="HIAT:ip">(</nts>
                  <nts id="Seg_1589" n="HIAT:ip">(</nts>
                  <ats e="T571" id="Seg_1590" n="HIAT:non-pho" s="T570">BRK</ats>
                  <nts id="Seg_1591" n="HIAT:ip">)</nts>
                  <nts id="Seg_1592" n="HIAT:ip">)</nts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T582" id="Seg_1595" n="sc" s="T257">
               <ts e="T582" id="Seg_1597" n="HIAT:u" s="T257">
                  <ts e="T573" id="Seg_1599" n="HIAT:w" s="T257">Kamen</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1602" n="HIAT:w" s="T573">măn</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1605" n="HIAT:w" s="T574">abam</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1608" n="HIAT:w" s="T575">šobi</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1611" n="HIAT:ip">(</nts>
                  <ts e="T577" id="Seg_1613" n="HIAT:w" s="T576">dĭn</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1616" n="HIAT:w" s="T577">turagən</ts>
                  <nts id="Seg_1617" n="HIAT:ip">)</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1620" n="HIAT:w" s="T578">dĭ</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1623" n="HIAT:w" s="T579">turandə</ts>
                  <nts id="Seg_1624" n="HIAT:ip">,</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1627" n="HIAT:w" s="T580">Spasskaːnə</ts>
                  <nts id="Seg_1628" n="HIAT:ip">,</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1631" n="HIAT:w" s="T581">maːʔnʼi</ts>
                  <nts id="Seg_1632" n="HIAT:ip">.</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T588" id="Seg_1634" n="sc" s="T583">
               <ts e="T588" id="Seg_1636" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_1638" n="HIAT:w" s="T583">Dĭgəttə</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1641" n="HIAT:w" s="T584">šolaʔbə</ts>
                  <nts id="Seg_1642" n="HIAT:ip">,</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1645" n="HIAT:w" s="T585">šomi</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1648" n="HIAT:w" s="T586">pa</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1651" n="HIAT:w" s="T587">nulaʔbə</ts>
                  <nts id="Seg_1652" n="HIAT:ip">.</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T595" id="Seg_1654" n="sc" s="T589">
               <ts e="T595" id="Seg_1656" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_1658" n="HIAT:w" s="T589">Dĭgəttə</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1661" n="HIAT:w" s="T590">pagə</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1664" n="HIAT:w" s="T591">сучок</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1666" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_1668" n="HIAT:w" s="T592">üz-</ts>
                  <nts id="Seg_1669" n="HIAT:ip">)</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1672" n="HIAT:w" s="T593">dʼünə</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1675" n="HIAT:w" s="T594">saʔməluʔpi</ts>
                  <nts id="Seg_1676" n="HIAT:ip">.</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T603" id="Seg_1678" n="sc" s="T596">
               <ts e="T603" id="Seg_1680" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_1682" n="HIAT:w" s="T596">Dĭgəttə</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1684" n="HIAT:ip">(</nts>
                  <ts e="T598" id="Seg_1686" n="HIAT:w" s="T597">šo-</ts>
                  <nts id="Seg_1687" n="HIAT:ip">)</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1690" n="HIAT:w" s="T598">dĭ</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1693" n="HIAT:w" s="T599">šobi</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1696" n="HIAT:w" s="T600">maːʔndə</ts>
                  <nts id="Seg_1697" n="HIAT:ip">,</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1700" n="HIAT:w" s="T601">măna</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1703" n="HIAT:w" s="T602">nörbəlie</ts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T610" id="Seg_1706" n="sc" s="T604">
               <ts e="T610" id="Seg_1708" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_1710" n="HIAT:w" s="T604">Măn</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1712" n="HIAT:ip">(</nts>
                  <ts e="T606" id="Seg_1714" n="HIAT:w" s="T605">măn-</ts>
                  <nts id="Seg_1715" n="HIAT:ip">)</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1718" n="HIAT:w" s="T606">mămbiam:</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1721" n="HIAT:w" s="T607">šindidə</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_1724" n="HIAT:w" s="T608">esseŋdə</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1727" n="HIAT:w" s="T609">külalləj</ts>
                  <nts id="Seg_1728" n="HIAT:ip">.</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T617" id="Seg_1730" n="sc" s="T611">
               <ts e="T617" id="Seg_1732" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_1734" n="HIAT:w" s="T611">I</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1737" n="HIAT:w" s="T612">dĭgəttə</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1740" n="HIAT:w" s="T613">dăre</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1743" n="HIAT:w" s="T614">dĭ</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1746" n="HIAT:w" s="T615">i</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_1749" n="HIAT:w" s="T616">ibi</ts>
                  <nts id="Seg_1750" n="HIAT:ip">.</nts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T626" id="Seg_1752" n="sc" s="T618">
               <ts e="T626" id="Seg_1754" n="HIAT:u" s="T618">
                  <nts id="Seg_1755" n="HIAT:ip">(</nts>
                  <ts e="T619" id="Seg_1757" n="HIAT:w" s="T618">Măn=</ts>
                  <nts id="Seg_1758" n="HIAT:ip">)</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_1761" n="HIAT:w" s="T619">Măn</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_1764" n="HIAT:w" s="T620">nʼim</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_1767" n="HIAT:w" s="T621">kambi</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1770" n="HIAT:w" s="T622">bügən</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_1773" n="HIAT:w" s="T623">i</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_1776" n="HIAT:w" s="T624">dĭn</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1779" n="HIAT:w" s="T625">külambi</ts>
                  <nts id="Seg_1780" n="HIAT:ip">.</nts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T671" id="Seg_1782" n="sc" s="T665">
               <ts e="T671" id="Seg_1784" n="HIAT:u" s="T665">
                  <ts e="T666" id="Seg_1786" n="HIAT:w" s="T665">Я</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_1789" n="HIAT:w" s="T666">забыла</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_1792" n="HIAT:w" s="T667">вчера</ts>
                  <nts id="Seg_1793" n="HIAT:ip">,</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_1796" n="HIAT:w" s="T668">про</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_1799" n="HIAT:w" s="T669">это</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_1802" n="HIAT:w" s="T670">говорила</ts>
                  <nts id="Seg_1803" n="HIAT:ip">.</nts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T683" id="Seg_1805" n="sc" s="T672">
               <ts e="T683" id="Seg_1807" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_1809" n="HIAT:w" s="T672">Про</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_1812" n="HIAT:w" s="T673">себя</ts>
                  <nts id="Seg_1813" n="HIAT:ip">,</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_1816" n="HIAT:w" s="T674">я</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_1819" n="HIAT:w" s="T675">замуж</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_1822" n="HIAT:w" s="T676">шла</ts>
                  <nts id="Seg_1823" n="HIAT:ip">,</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_1826" n="HIAT:w" s="T677">забыла</ts>
                  <nts id="Seg_1827" n="HIAT:ip">,</nts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_1830" n="HIAT:w" s="T678">говорила</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_1833" n="HIAT:w" s="T679">ли</ts>
                  <nts id="Seg_1834" n="HIAT:ip">,</nts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_1837" n="HIAT:w" s="T681">нет</ts>
                  <nts id="Seg_1838" n="HIAT:ip">.</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_1840" n="sc" s="T719">
               <ts e="T723" id="Seg_1842" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_1844" n="HIAT:w" s="T719">Как</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_1847" n="HIAT:w" s="T720">мой</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_1850" n="HIAT:w" s="T721">муж</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_1853" n="HIAT:w" s="T722">утонул</ts>
                  <nts id="Seg_1854" n="HIAT:ip">.</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T729" id="Seg_1856" n="sc" s="T724">
               <ts e="T729" id="Seg_1858" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_1860" n="HIAT:w" s="T724">Ну</ts>
                  <nts id="Seg_1861" n="HIAT:ip">,</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_1864" n="HIAT:w" s="T725">это</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_1867" n="HIAT:w" s="T726">он</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_1870" n="HIAT:w" s="T727">говорит</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_1874" n="HIAT:w" s="T728">нет</ts>
                  <nts id="Seg_1875" n="HIAT:ip">?</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T756" id="Seg_1877" n="sc" s="T743">
               <ts e="T756" id="Seg_1879" n="HIAT:u" s="T743">
                  <ts e="T744" id="Seg_1881" n="HIAT:w" s="T743">Ну</ts>
                  <nts id="Seg_1882" n="HIAT:ip">,</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_1885" n="HIAT:w" s="T744">когда</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_1888" n="HIAT:w" s="T745">я</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_1891" n="HIAT:w" s="T746">была</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_1894" n="HIAT:w" s="T747">девушкой</ts>
                  <nts id="Seg_1895" n="HIAT:ip">,</nts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_1898" n="HIAT:w" s="T748">Пьянково</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_1901" n="HIAT:w" s="T749">деревня</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_1904" n="HIAT:w" s="T750">была</ts>
                  <nts id="Seg_1905" n="HIAT:ip">,</nts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_1908" n="HIAT:w" s="T751">там</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_1911" n="HIAT:w" s="T752">парень</ts>
                  <nts id="Seg_1912" n="HIAT:ip">,</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_1915" n="HIAT:w" s="T753">звали</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_1918" n="HIAT:w" s="T754">его</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_1921" n="HIAT:w" s="T755">Алексеем</ts>
                  <nts id="Seg_1922" n="HIAT:ip">.</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T769" id="Seg_1924" n="sc" s="T757">
               <ts e="T769" id="Seg_1926" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_1928" n="HIAT:w" s="T757">Мы</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_1931" n="HIAT:w" s="T758">с</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_1934" n="HIAT:w" s="T759">им</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_1937" n="HIAT:w" s="T760">дружили</ts>
                  <nts id="Seg_1938" n="HIAT:ip">,</nts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1940" n="HIAT:ip">(</nts>
                  <ts e="T762" id="Seg_1942" n="HIAT:w" s="T761">я=</ts>
                  <nts id="Seg_1943" n="HIAT:ip">)</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_1946" n="HIAT:w" s="T762">я</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_1949" n="HIAT:w" s="T763">когда</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_1952" n="HIAT:w" s="T764">туды</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_1955" n="HIAT:w" s="T765">ездила</ts>
                  <nts id="Seg_1956" n="HIAT:ip">,</nts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_1959" n="HIAT:w" s="T766">а</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_1962" n="HIAT:w" s="T767">он</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_1965" n="HIAT:w" s="T768">сюды</ts>
                  <nts id="Seg_1966" n="HIAT:ip">.</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T786" id="Seg_1968" n="sc" s="T770">
               <ts e="T786" id="Seg_1970" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_1972" n="HIAT:w" s="T770">А</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_1975" n="HIAT:w" s="T771">потом</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_1978" n="HIAT:w" s="T772">у</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_1981" n="HIAT:w" s="T773">его</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1983" n="HIAT:ip">(</nts>
                  <ts e="T775" id="Seg_1985" n="HIAT:w" s="T774">мать=</ts>
                  <nts id="Seg_1986" n="HIAT:ip">)</nts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_1989" n="HIAT:w" s="T775">отец</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_1992" n="HIAT:w" s="T776">помер</ts>
                  <nts id="Seg_1993" n="HIAT:ip">,</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_1996" n="HIAT:w" s="T777">а</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_1999" n="HIAT:w" s="T778">мать</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2002" n="HIAT:w" s="T779">взяла</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2005" n="HIAT:w" s="T780">замуж</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2008" n="HIAT:w" s="T781">ушла</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2012" n="HIAT:w" s="T782">а</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2015" n="HIAT:w" s="T783">он</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2018" n="HIAT:w" s="T784">остался</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2021" n="HIAT:w" s="T785">один</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T791" id="Seg_2024" n="sc" s="T787">
               <ts e="T791" id="Seg_2026" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_2028" n="HIAT:w" s="T787">Ему</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2031" n="HIAT:w" s="T788">семнадцать</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2034" n="HIAT:w" s="T789">лет</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2037" n="HIAT:w" s="T790">было</ts>
                  <nts id="Seg_2038" n="HIAT:ip">.</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T797" id="Seg_2040" n="sc" s="T792">
               <ts e="T797" id="Seg_2042" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_2044" n="HIAT:w" s="T792">И</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2047" n="HIAT:w" s="T793">мне</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2050" n="HIAT:w" s="T794">семнадцать</ts>
                  <nts id="Seg_2051" n="HIAT:ip">,</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2054" n="HIAT:w" s="T795">молода</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2057" n="HIAT:w" s="T796">была</ts>
                  <nts id="Seg_2058" n="HIAT:ip">!</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T809" id="Seg_2060" n="sc" s="T798">
               <ts e="T802" id="Seg_2062" n="HIAT:u" s="T798">
                  <ts e="T799" id="Seg_2064" n="HIAT:w" s="T798">Ну</ts>
                  <nts id="Seg_2065" n="HIAT:ip">,</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2068" n="HIAT:w" s="T799">и</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2071" n="HIAT:w" s="T800">он</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2074" n="HIAT:w" s="T801">приехал</ts>
                  <nts id="Seg_2075" n="HIAT:ip">.</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_2078" n="HIAT:u" s="T802">
                  <nts id="Seg_2079" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_2081" n="HIAT:w" s="T802">Я=</ts>
                  <nts id="Seg_2082" n="HIAT:ip">)</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2085" n="HIAT:w" s="T803">Я</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2088" n="HIAT:w" s="T804">ему</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2091" n="HIAT:w" s="T805">говорю:</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2093" n="HIAT:ip">"</nts>
                  <ts e="T807" id="Seg_2095" n="HIAT:w" s="T806">Сватай</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2099" n="HIAT:w" s="T807">отдадут</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2102" n="HIAT:w" s="T808">родители</ts>
                  <nts id="Seg_2103" n="HIAT:ip">"</nts>
                  <nts id="Seg_2104" n="HIAT:ip">.</nts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T819" id="Seg_2106" n="sc" s="T810">
               <ts e="T819" id="Seg_2108" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_2110" n="HIAT:w" s="T810">Он</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2113" n="HIAT:w" s="T811">говорит:</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2115" n="HIAT:ip">"</nts>
                  <ts e="T813" id="Seg_2117" n="HIAT:w" s="T812">Нет</ts>
                  <nts id="Seg_2118" n="HIAT:ip">,</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2121" n="HIAT:w" s="T813">не</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2124" n="HIAT:w" s="T814">отдадут</ts>
                  <nts id="Seg_2125" n="HIAT:ip">,</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2128" n="HIAT:w" s="T815">скажут:</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2131" n="HIAT:w" s="T816">ты</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2134" n="HIAT:w" s="T817">молодая</ts>
                  <nts id="Seg_2135" n="HIAT:ip">,</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2138" n="HIAT:w" s="T818">поедем</ts>
                  <nts id="Seg_2139" n="HIAT:ip">!</nts>
                  <nts id="Seg_2140" n="HIAT:ip">"</nts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T831" id="Seg_2142" n="sc" s="T820">
               <ts e="T831" id="Seg_2144" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2146" n="HIAT:w" s="T820">И</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2149" n="HIAT:w" s="T821">я</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2152" n="HIAT:w" s="T822">собрала</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2155" n="HIAT:w" s="T823">платье</ts>
                  <nts id="Seg_2156" n="HIAT:ip">,</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2159" n="HIAT:w" s="T824">связала</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2162" n="HIAT:w" s="T825">узел</ts>
                  <nts id="Seg_2163" n="HIAT:ip">,</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2166" n="HIAT:w" s="T826">ночью</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2169" n="HIAT:w" s="T827">оседлали</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2172" n="HIAT:w" s="T828">коня</ts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2175" n="HIAT:w" s="T829">и</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2178" n="HIAT:w" s="T830">поехали</ts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T841" id="Seg_2181" n="sc" s="T832">
               <ts e="T841" id="Seg_2183" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_2185" n="HIAT:w" s="T832">Не</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2188" n="HIAT:w" s="T833">поехали</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2191" n="HIAT:w" s="T834">тут</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2194" n="HIAT:w" s="T835">прямой</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2197" n="HIAT:w" s="T836">дорогой</ts>
                  <nts id="Seg_2198" n="HIAT:ip">,</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2201" n="HIAT:w" s="T837">а</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2204" n="HIAT:w" s="T838">поехали</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2207" n="HIAT:w" s="T839">через</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2210" n="HIAT:w" s="T840">Агинско</ts>
                  <nts id="Seg_2211" n="HIAT:ip">.</nts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T845" id="Seg_2213" n="sc" s="T842">
               <ts e="T845" id="Seg_2215" n="HIAT:u" s="T842">
                  <ts e="T843" id="Seg_2217" n="HIAT:w" s="T842">Ночью</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2220" n="HIAT:w" s="T843">ехали-ехали</ts>
                  <nts id="Seg_2221" n="HIAT:ip">,</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2224" n="HIAT:w" s="T844">ой</ts>
                  <nts id="Seg_2225" n="HIAT:ip">.</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T857" id="Seg_2227" n="sc" s="T846">
               <ts e="T857" id="Seg_2229" n="HIAT:u" s="T846">
                  <ts e="T847" id="Seg_2231" n="HIAT:w" s="T846">Потом</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2234" n="HIAT:w" s="T847">приехали</ts>
                  <nts id="Seg_2235" n="HIAT:ip">,</nts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2238" n="HIAT:w" s="T848">там</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2241" n="HIAT:w" s="T849">деревня</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2243" n="HIAT:ip">(</nts>
                  <ts e="T851" id="Seg_2245" n="HIAT:w" s="T850">Ка-</ts>
                  <nts id="Seg_2246" n="HIAT:ip">)</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2249" n="HIAT:w" s="T851">Каптырлык</ts>
                  <nts id="Seg_2250" n="HIAT:ip">,</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2253" n="HIAT:w" s="T852">русский</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2256" n="HIAT:w" s="T853">народ</ts>
                  <nts id="Seg_2257" n="HIAT:ip">,</nts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2260" n="HIAT:w" s="T854">там</ts>
                  <nts id="Seg_2261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2263" n="HIAT:w" s="T855">ночевали</ts>
                  <nts id="Seg_2264" n="HIAT:ip">,</nts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2267" n="HIAT:w" s="T856">оттель</ts>
                  <nts id="Seg_2268" n="HIAT:ip">…</nts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T868" id="Seg_2270" n="sc" s="T858">
               <ts e="T868" id="Seg_2272" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_2274" n="HIAT:w" s="T858">И</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2277" n="HIAT:w" s="T859">там</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2280" n="HIAT:w" s="T860">считай</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2283" n="HIAT:w" s="T861">целый</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2286" n="HIAT:w" s="T862">день</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2289" n="HIAT:w" s="T863">пробыли</ts>
                  <nts id="Seg_2290" n="HIAT:ip">,</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2293" n="HIAT:w" s="T864">он</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2296" n="HIAT:w" s="T865">не</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2299" n="HIAT:w" s="T866">ехал</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2302" n="HIAT:w" s="T867">домой</ts>
                  <nts id="Seg_2303" n="HIAT:ip">.</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T878" id="Seg_2305" n="sc" s="T869">
               <ts e="T875" id="Seg_2307" n="HIAT:u" s="T869">
                  <ts e="T870" id="Seg_2309" n="HIAT:w" s="T869">Или</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2312" n="HIAT:w" s="T870">совестился</ts>
                  <nts id="Seg_2313" n="HIAT:ip">,</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2315" n="HIAT:ip">(</nts>
                  <ts e="T872" id="Seg_2317" n="HIAT:w" s="T871">или</ts>
                  <nts id="Seg_2318" n="HIAT:ip">)</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2321" n="HIAT:w" s="T872">чего</ts>
                  <nts id="Seg_2322" n="HIAT:ip">,</nts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2325" n="HIAT:w" s="T873">не</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2328" n="HIAT:w" s="T874">знаю</ts>
                  <nts id="Seg_2329" n="HIAT:ip">.</nts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_2332" n="HIAT:u" s="T875">
                  <ts e="T876" id="Seg_2334" n="HIAT:w" s="T875">Потом</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2337" n="HIAT:w" s="T876">приехали</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2340" n="HIAT:w" s="T877">вечером</ts>
                  <nts id="Seg_2341" n="HIAT:ip">.</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T895" id="Seg_2343" n="sc" s="T879">
               <ts e="T895" id="Seg_2345" n="HIAT:u" s="T879">
                  <ts e="T880" id="Seg_2347" n="HIAT:w" s="T879">И</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2350" n="HIAT:w" s="T880">он</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2352" n="HIAT:ip">(</nts>
                  <ts e="T882" id="Seg_2354" n="HIAT:w" s="T881">рас-</ts>
                  <nts id="Seg_2355" n="HIAT:ip">)</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2358" n="HIAT:w" s="T882">расседлал</ts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2361" n="HIAT:w" s="T883">коня</ts>
                  <nts id="Seg_2362" n="HIAT:ip">,</nts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_2365" n="HIAT:w" s="T884">зашли</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_2368" n="HIAT:w" s="T885">в</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_2371" n="HIAT:w" s="T886">дом</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_2374" n="HIAT:w" s="T887">и</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_2377" n="HIAT:w" s="T888">тут</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2380" n="HIAT:w" s="T889">спали</ts>
                  <nts id="Seg_2381" n="HIAT:ip">,</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_2384" n="HIAT:w" s="T890">и</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_2387" n="HIAT:w" s="T891">там</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2390" n="HIAT:w" s="T892">я</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2393" n="HIAT:w" s="T893">жила</ts>
                  <nts id="Seg_2394" n="HIAT:ip">,</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_2397" n="HIAT:w" s="T894">всё</ts>
                  <nts id="Seg_2398" n="HIAT:ip">.</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T902" id="Seg_2400" n="sc" s="T896">
               <ts e="T902" id="Seg_2402" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_2404" n="HIAT:w" s="T896">А</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_2407" n="HIAT:w" s="T897">тагды</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_2410" n="HIAT:w" s="T898">приехал</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2412" n="HIAT:ip">(</nts>
                  <ts e="T900" id="Seg_2414" n="HIAT:w" s="T899">е-</ts>
                  <nts id="Seg_2415" n="HIAT:ip">)</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2418" n="HIAT:w" s="T900">евонный</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_2421" n="HIAT:w" s="T901">сродственник</ts>
                  <nts id="Seg_2422" n="HIAT:ip">.</nts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T912" id="Seg_2424" n="sc" s="T903">
               <ts e="T912" id="Seg_2426" n="HIAT:u" s="T903">
                  <nts id="Seg_2427" n="HIAT:ip">(</nts>
                  <ts e="T904" id="Seg_2429" n="HIAT:w" s="T903">Зва-</ts>
                  <nts id="Seg_2430" n="HIAT:ip">)</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2432" n="HIAT:ip">(</nts>
                  <ts e="T905" id="Seg_2434" n="HIAT:w" s="T904">О-</ts>
                  <nts id="Seg_2435" n="HIAT:ip">)</nts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2438" n="HIAT:w" s="T905">Он</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_2441" n="HIAT:w" s="T906">глаза</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_2444" n="HIAT:w" s="T907">потерял</ts>
                  <nts id="Seg_2445" n="HIAT:ip">,</nts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_2448" n="HIAT:w" s="T908">не</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2451" n="HIAT:w" s="T909">видел</ts>
                  <nts id="Seg_2452" n="HIAT:ip">,</nts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2455" n="HIAT:w" s="T910">дядя</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_2458" n="HIAT:w" s="T911">его</ts>
                  <nts id="Seg_2459" n="HIAT:ip">.</nts>
                  <nts id="Seg_2460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T918" id="Seg_2461" n="sc" s="T913">
               <ts e="T918" id="Seg_2463" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_2465" n="HIAT:w" s="T913">Сказал:</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_2468" n="HIAT:w" s="T914">приезжай</ts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_2471" n="HIAT:w" s="T915">помоги</ts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_2474" n="HIAT:w" s="T916">сено</ts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_2477" n="HIAT:w" s="T917">косить</ts>
                  <nts id="Seg_2478" n="HIAT:ip">.</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T926" id="Seg_2480" n="sc" s="T919">
               <ts e="T926" id="Seg_2482" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_2484" n="HIAT:w" s="T919">Ему</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_2487" n="HIAT:w" s="T920">туды</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_2490" n="HIAT:w" s="T921">в</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_2493" n="HIAT:w" s="T922">нашу</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_2496" n="HIAT:w" s="T923">деревню</ts>
                  <nts id="Seg_2497" n="HIAT:ip">,</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_2500" n="HIAT:w" s="T924">в</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_2503" n="HIAT:w" s="T925">Абалаков</ts>
                  <nts id="Seg_2504" n="HIAT:ip">.</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T955" id="Seg_2506" n="sc" s="T927">
               <ts e="T937" id="Seg_2508" n="HIAT:u" s="T927">
                  <nts id="Seg_2509" n="HIAT:ip">(</nts>
                  <ts e="T928" id="Seg_2511" n="HIAT:w" s="T927">Тогда</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_2514" n="HIAT:w" s="T928">он=</ts>
                  <nts id="Seg_2515" n="HIAT:ip">)</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_2518" n="HIAT:w" s="T929">Тогда</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_2521" n="HIAT:w" s="T930">он</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_2524" n="HIAT:w" s="T931">привез</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_2527" n="HIAT:w" s="T932">девушку</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_2530" n="HIAT:w" s="T933">там</ts>
                  <nts id="Seg_2531" n="HIAT:ip">,</nts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_2534" n="HIAT:w" s="T934">с</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_2537" n="HIAT:w" s="T935">другой</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_2540" n="HIAT:w" s="T936">деревни</ts>
                  <nts id="Seg_2541" n="HIAT:ip">.</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_2544" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_2546" n="HIAT:w" s="T937">Ну</ts>
                  <nts id="Seg_2547" n="HIAT:ip">,</nts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_2550" n="HIAT:w" s="T938">мне</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_2553" n="HIAT:w" s="T939">сказали</ts>
                  <nts id="Seg_2554" n="HIAT:ip">,</nts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_2557" n="HIAT:w" s="T940">что</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_2560" n="HIAT:w" s="T941">он</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_2563" n="HIAT:w" s="T942">хочет</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_2566" n="HIAT:w" s="T943">ее</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_2569" n="HIAT:w" s="T944">брать</ts>
                  <nts id="Seg_2570" n="HIAT:ip">,</nts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_2573" n="HIAT:w" s="T945">а</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_2576" n="HIAT:w" s="T946">тебя</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_2579" n="HIAT:w" s="T947">прогнать</ts>
                  <nts id="Seg_2580" n="HIAT:ip">.</nts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_2583" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_2585" n="HIAT:w" s="T948">Я</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_2588" n="HIAT:w" s="T949">собралась</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_2591" n="HIAT:w" s="T950">да</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_2594" n="HIAT:w" s="T951">с</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_2597" n="HIAT:w" s="T952">евонным</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_2600" n="HIAT:w" s="T953">дядем</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_2603" n="HIAT:w" s="T954">поехала</ts>
                  <nts id="Seg_2604" n="HIAT:ip">.</nts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T961" id="Seg_2606" n="sc" s="T956">
               <ts e="T961" id="Seg_2608" n="HIAT:u" s="T956">
                  <ts e="T957" id="Seg_2610" n="HIAT:w" s="T956">Он</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_2613" n="HIAT:w" s="T957">меня</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_2616" n="HIAT:w" s="T958">догнал</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_2619" n="HIAT:w" s="T959">и</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_2622" n="HIAT:w" s="T960">воротил</ts>
                  <nts id="Seg_2623" n="HIAT:ip">.</nts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T964" id="Seg_2625" n="sc" s="T962">
               <ts e="T964" id="Seg_2627" n="HIAT:u" s="T962">
                  <ts e="T963" id="Seg_2629" n="HIAT:w" s="T962">Назад</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_2632" n="HIAT:w" s="T963">обратно</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T973" id="Seg_2635" n="sc" s="T965">
               <ts e="T973" id="Seg_2637" n="HIAT:u" s="T965">
                  <ts e="T966" id="Seg_2639" n="HIAT:w" s="T965">Ну</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_2642" n="HIAT:w" s="T966">а</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_2645" n="HIAT:w" s="T967">потом</ts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_2648" n="HIAT:w" s="T968">после</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_2651" n="HIAT:w" s="T969">этого</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_2654" n="HIAT:w" s="T970">мама</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_2657" n="HIAT:w" s="T971">туды</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_2660" n="HIAT:w" s="T972">приехала</ts>
                  <nts id="Seg_2661" n="HIAT:ip">.</nts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T980" id="Seg_2663" n="sc" s="T974">
               <ts e="T980" id="Seg_2665" n="HIAT:u" s="T974">
                  <ts e="T975" id="Seg_2667" n="HIAT:w" s="T974">А</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_2670" n="HIAT:w" s="T975">я</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_2673" n="HIAT:w" s="T976">тады</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_2676" n="HIAT:w" s="T977">пошла</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_2679" n="HIAT:w" s="T978">к</ts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_2682" n="HIAT:w" s="T979">маме</ts>
                  <nts id="Seg_2683" n="HIAT:ip">.</nts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T989" id="Seg_2685" n="sc" s="T981">
               <ts e="T989" id="Seg_2687" n="HIAT:u" s="T981">
                  <ts e="T982" id="Seg_2689" n="HIAT:w" s="T981">А</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_2692" n="HIAT:w" s="T982">мама</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_2695" n="HIAT:w" s="T983">заехала</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_2698" n="HIAT:w" s="T984">к</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_2701" n="HIAT:w" s="T985">племяннику</ts>
                  <nts id="Seg_2702" n="HIAT:ip">,</nts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_2705" n="HIAT:w" s="T986">который</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_2708" n="HIAT:w" s="T987">вот</ts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_2711" n="HIAT:w" s="T988">сейчас</ts>
                  <nts id="Seg_2712" n="HIAT:ip">…</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1008" id="Seg_2714" n="sc" s="T990">
               <ts e="T1008" id="Seg_2716" n="HIAT:u" s="T990">
                  <ts e="T991" id="Seg_2718" n="HIAT:w" s="T990">К</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_2721" n="HIAT:w" s="T991">еёному</ts>
                  <nts id="Seg_2722" n="HIAT:ip">,</nts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_2725" n="HIAT:w" s="T992">ее</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_2728" n="HIAT:w" s="T993">племянник</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_2731" n="HIAT:w" s="T994">был</ts>
                  <nts id="Seg_2732" n="HIAT:ip">,</nts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_2735" n="HIAT:w" s="T995">Иваном</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_2738" n="HIAT:w" s="T996">его</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_2741" n="HIAT:w" s="T997">звали</ts>
                  <nts id="Seg_2742" n="HIAT:ip">,</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_2745" n="HIAT:w" s="T998">который</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_2748" n="HIAT:w" s="T999">сейчас</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_2751" n="HIAT:w" s="T1000">в</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_2754" n="HIAT:w" s="T1001">Пьянково</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_2757" n="HIAT:w" s="T1002">его</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_2760" n="HIAT:w" s="T1003">дочка</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_2763" n="HIAT:w" s="T1004">живет</ts>
                  <nts id="Seg_2764" n="HIAT:ip">,</nts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_2767" n="HIAT:w" s="T1005">она</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_2770" n="HIAT:w" s="T1006">мне</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_2773" n="HIAT:w" s="T1007">племянница</ts>
                  <nts id="Seg_2774" n="HIAT:ip">.</nts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1013" id="Seg_2776" n="sc" s="T1009">
               <ts e="T1013" id="Seg_2778" n="HIAT:u" s="T1009">
                  <ts e="T1010" id="Seg_2780" n="HIAT:w" s="T1009">Ну</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_2783" n="HIAT:w" s="T1010">и</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_2786" n="HIAT:w" s="T1011">мама</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_2789" n="HIAT:w" s="T1012">это</ts>
                  <nts id="Seg_2790" n="HIAT:ip">…</nts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1025" id="Seg_2792" n="sc" s="T1014">
               <ts e="T1025" id="Seg_2794" n="HIAT:u" s="T1014">
                  <ts e="T1015" id="Seg_2796" n="HIAT:w" s="T1014">Пришла</ts>
                  <nts id="Seg_2797" n="HIAT:ip">,</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_2800" n="HIAT:w" s="T1015">а</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_2803" n="HIAT:w" s="T1016">он</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_2806" n="HIAT:w" s="T1017">опять</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_2809" n="HIAT:w" s="T1018">пришел</ts>
                  <nts id="Seg_2810" n="HIAT:ip">,</nts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_2813" n="HIAT:w" s="T1019">меня</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_2816" n="HIAT:w" s="T1020">за</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_2819" n="HIAT:w" s="T1021">руку</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_2822" n="HIAT:w" s="T1022">взял</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_2825" n="HIAT:w" s="T1023">и</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_2828" n="HIAT:w" s="T1024">увел</ts>
                  <nts id="Seg_2829" n="HIAT:ip">.</nts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1034" id="Seg_2831" n="sc" s="T1026">
               <ts e="T1034" id="Seg_2833" n="HIAT:u" s="T1026">
                  <ts e="T1027" id="Seg_2835" n="HIAT:w" s="T1026">Ну</ts>
                  <nts id="Seg_2836" n="HIAT:ip">,</nts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_2839" n="HIAT:w" s="T1027">потом</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_2842" n="HIAT:w" s="T1028">мы</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_2845" n="HIAT:w" s="T1029">легли</ts>
                  <nts id="Seg_2846" n="HIAT:ip">,</nts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_2849" n="HIAT:w" s="T1030">а</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_2852" n="HIAT:w" s="T1031">тогда</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_2855" n="HIAT:w" s="T1032">они</ts>
                  <nts id="Seg_2856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_2858" n="HIAT:w" s="T1033">пришли</ts>
                  <nts id="Seg_2859" n="HIAT:ip">.</nts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1045" id="Seg_2861" n="sc" s="T1035">
               <ts e="T1045" id="Seg_2863" n="HIAT:u" s="T1035">
                  <nts id="Seg_2864" n="HIAT:ip">"</nts>
                  <ts e="T1036" id="Seg_2866" n="HIAT:w" s="T1035">Если</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_2869" n="HIAT:w" s="T1036">вы</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_2872" n="HIAT:w" s="T1037">будете</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1039" id="Seg_2875" n="HIAT:w" s="T1038">жить</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1040" id="Seg_2878" n="HIAT:w" s="T1039">дак</ts>
                  <nts id="Seg_2879" n="HIAT:ip">,</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_2882" n="HIAT:w" s="T1040">мама</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_2885" n="HIAT:w" s="T1041">говорит</ts>
                  <nts id="Seg_2886" n="HIAT:ip">,</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_2889" n="HIAT:w" s="T1042">я</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_2892" n="HIAT:w" s="T1043">благословлю</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_2895" n="HIAT:w" s="T1044">вас</ts>
                  <nts id="Seg_2896" n="HIAT:ip">"</nts>
                  <nts id="Seg_2897" n="HIAT:ip">.</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1055" id="Seg_2899" n="sc" s="T1046">
               <ts e="T1055" id="Seg_2901" n="HIAT:u" s="T1046">
                  <ts e="T1047" id="Seg_2903" n="HIAT:w" s="T1046">Ну</ts>
                  <nts id="Seg_2904" n="HIAT:ip">,</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_2907" n="HIAT:w" s="T1047">тут</ts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_2910" n="HIAT:w" s="T1048">благословила</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_2913" n="HIAT:w" s="T1049">она</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_2916" n="HIAT:w" s="T1050">нас</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2918" n="HIAT:ip">(</nts>
                  <ts e="T1052" id="Seg_2920" n="HIAT:w" s="T1051">на</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_2923" n="HIAT:w" s="T1052">завтре</ts>
                  <nts id="Seg_2924" n="HIAT:ip">)</nts>
                  <nts id="Seg_2925" n="HIAT:ip">,</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_2928" n="HIAT:w" s="T1053">тоже</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_2931" n="HIAT:w" s="T1054">тут</ts>
                  <nts id="Seg_2932" n="HIAT:ip">.</nts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1074" id="Seg_2934" n="sc" s="T1056">
               <ts e="T1074" id="Seg_2936" n="HIAT:u" s="T1056">
                  <ts e="T1057" id="Seg_2938" n="HIAT:w" s="T1056">К</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_2941" n="HIAT:w" s="T1057">нам</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_2944" n="HIAT:w" s="T1058">пришли</ts>
                  <nts id="Seg_2945" n="HIAT:ip">,</nts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_2948" n="HIAT:w" s="T1059">этот</ts>
                  <nts id="Seg_2949" n="HIAT:ip">,</nts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1061" id="Seg_2952" n="HIAT:w" s="T1060">брат</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_2955" n="HIAT:w" s="T1061">сродный</ts>
                  <nts id="Seg_2956" n="HIAT:ip">,</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_2959" n="HIAT:w" s="T1062">невестка</ts>
                  <nts id="Seg_2960" n="HIAT:ip">,</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_2963" n="HIAT:w" s="T1063">мы</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_2966" n="HIAT:w" s="T1064">тут</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_2969" n="HIAT:w" s="T1065">чаю</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2971" n="HIAT:ip">(</nts>
                  <ts e="T1067" id="Seg_2973" n="HIAT:w" s="T1066">п-</ts>
                  <nts id="Seg_2974" n="HIAT:ip">)</nts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_2977" n="HIAT:w" s="T1067">попили</ts>
                  <nts id="Seg_2978" n="HIAT:ip">,</nts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_2981" n="HIAT:w" s="T1068">они</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_2984" n="HIAT:w" s="T1069">тогда</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_2987" n="HIAT:w" s="T1070">ушли</ts>
                  <nts id="Seg_2988" n="HIAT:ip">,</nts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1072" id="Seg_2991" n="HIAT:w" s="T1071">и</ts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_2994" n="HIAT:w" s="T1072">мама</ts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_2997" n="HIAT:w" s="T1073">уехала</ts>
                  <nts id="Seg_2998" n="HIAT:ip">.</nts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1081" id="Seg_3000" n="sc" s="T1075">
               <ts e="T1081" id="Seg_3002" n="HIAT:u" s="T1075">
                  <ts e="T1076" id="Seg_3004" n="HIAT:w" s="T1075">Ну</ts>
                  <nts id="Seg_3005" n="HIAT:ip">,</nts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_3008" n="HIAT:w" s="T1076">теперь</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_3011" n="HIAT:w" s="T1077">можно</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_3014" n="HIAT:w" s="T1079">по-камасински</ts>
                  <nts id="Seg_3015" n="HIAT:ip">.</nts>
                  <nts id="Seg_3016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1103" id="Seg_3017" n="sc" s="T1090">
               <ts e="T1103" id="Seg_3019" n="HIAT:u" s="T1090">
                  <ts e="T1092" id="Seg_3021" n="HIAT:w" s="T1090">Ну</ts>
                  <nts id="Seg_3022" n="HIAT:ip">,</nts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_3025" n="HIAT:w" s="T1092">потом</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3027" n="HIAT:ip">(</nts>
                  <ts e="T1095" id="Seg_3029" n="HIAT:w" s="T1094">мы=</ts>
                  <nts id="Seg_3030" n="HIAT:ip">)</nts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_3033" n="HIAT:w" s="T1095">про</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_3036" n="HIAT:w" s="T1096">это</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1098" id="Seg_3039" n="HIAT:w" s="T1097">надо</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_3042" n="HIAT:w" s="T1098">сказать</ts>
                  <nts id="Seg_3043" n="HIAT:ip">,</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_3046" n="HIAT:w" s="T1099">что</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1101" id="Seg_3049" n="HIAT:w" s="T1100">он</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_3052" n="HIAT:w" s="T1101">туды</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_3055" n="HIAT:w" s="T1102">поехал</ts>
                  <nts id="Seg_3056" n="HIAT:ip">.</nts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1119" id="Seg_3058" n="sc" s="T1104">
               <ts e="T1119" id="Seg_3060" n="HIAT:u" s="T1104">
                  <ts e="T1105" id="Seg_3062" n="HIAT:w" s="T1104">Тогда</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_3065" n="HIAT:w" s="T1105">он</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_3068" n="HIAT:w" s="T1106">меня</ts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_3071" n="HIAT:w" s="T1107">оставил</ts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_3074" n="HIAT:w" s="T1108">дома</ts>
                  <nts id="Seg_3075" n="HIAT:ip">,</nts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1110" id="Seg_3078" n="HIAT:w" s="T1109">не</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_3081" n="HIAT:w" s="T1110">взял</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_3084" n="HIAT:w" s="T1111">с</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_3087" n="HIAT:w" s="T1112">собой</ts>
                  <nts id="Seg_3088" n="HIAT:ip">,</nts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_3091" n="HIAT:w" s="T1113">поехал</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_3094" n="HIAT:w" s="T1114">туды</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_3097" n="HIAT:w" s="T1115">к</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_3100" n="HIAT:w" s="T1116">нам</ts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1118" id="Seg_3103" n="HIAT:w" s="T1117">в</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_3106" n="HIAT:w" s="T1118">Абалаково</ts>
                  <nts id="Seg_3107" n="HIAT:ip">.</nts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1123" id="Seg_3109" n="sc" s="T1120">
               <ts e="T1123" id="Seg_3111" n="HIAT:u" s="T1120">
                  <ts e="T1121" id="Seg_3113" n="HIAT:w" s="T1120">Дяде</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_3116" n="HIAT:w" s="T1121">сено</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_3119" n="HIAT:w" s="T1122">косить</ts>
                  <nts id="Seg_3120" n="HIAT:ip">.</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1134" id="Seg_3122" n="sc" s="T1124">
               <ts e="T1134" id="Seg_3124" n="HIAT:u" s="T1124">
                  <ts e="T1125" id="Seg_3126" n="HIAT:w" s="T1124">И</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1126" id="Seg_3129" n="HIAT:w" s="T1125">как</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_3132" n="HIAT:w" s="T1126">раз</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_3135" n="HIAT:w" s="T1127">был</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_3138" n="HIAT:w" s="T1128">день</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_3141" n="HIAT:w" s="T1129">такой</ts>
                  <nts id="Seg_3142" n="HIAT:ip">,</nts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3144" n="HIAT:ip">(</nts>
                  <ts e="T1131" id="Seg_3146" n="HIAT:w" s="T1130">пра-</ts>
                  <nts id="Seg_3147" n="HIAT:ip">)</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_3150" n="HIAT:w" s="T1131">праздник</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_3153" n="HIAT:w" s="T1132">Ивана</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_3156" n="HIAT:w" s="T1133">Купала</ts>
                  <nts id="Seg_3157" n="HIAT:ip">.</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1141" id="Seg_3159" n="sc" s="T1135">
               <ts e="T1141" id="Seg_3161" n="HIAT:u" s="T1135">
                  <ts e="T1136" id="Seg_3163" n="HIAT:w" s="T1135">Они</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_3166" n="HIAT:w" s="T1136">по</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_3169" n="HIAT:w" s="T1137">деревне</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_3172" n="HIAT:w" s="T1138">ходили</ts>
                  <nts id="Seg_3173" n="HIAT:ip">,</nts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_3176" n="HIAT:w" s="T1139">обливались</ts>
                  <nts id="Seg_3177" n="HIAT:ip">,</nts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_3180" n="HIAT:w" s="T1140">обливалися</ts>
                  <nts id="Seg_3181" n="HIAT:ip">.</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1146" id="Seg_3183" n="sc" s="T1142">
               <ts e="T1146" id="Seg_3185" n="HIAT:u" s="T1142">
                  <ts e="T1143" id="Seg_3187" n="HIAT:w" s="T1142">А</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_3190" n="HIAT:w" s="T1143">тады</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_3193" n="HIAT:w" s="T1144">собрались</ts>
                  <nts id="Seg_3194" n="HIAT:ip">,</nts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_3197" n="HIAT:w" s="T1145">пошли</ts>
                  <nts id="Seg_3198" n="HIAT:ip">.</nts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1161" id="Seg_3200" n="sc" s="T1147">
               <ts e="T1157" id="Seg_3202" n="HIAT:u" s="T1147">
                  <ts e="T1148" id="Seg_3204" n="HIAT:w" s="T1147">Там</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_3207" n="HIAT:w" s="T1148">еще</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_3210" n="HIAT:w" s="T1149">ниже</ts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1151" id="Seg_3213" n="HIAT:w" s="T1150">этой</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_3216" n="HIAT:w" s="T1151">мельницы</ts>
                  <nts id="Seg_3217" n="HIAT:ip">,</nts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1153" id="Seg_3220" n="HIAT:w" s="T1152">где</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1154" id="Seg_3223" n="HIAT:w" s="T1153">живут</ts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1155" id="Seg_3226" n="HIAT:w" s="T1154">эстонцы</ts>
                  <nts id="Seg_3227" n="HIAT:ip">,</nts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_3230" n="HIAT:w" s="T1155">другая</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1157" id="Seg_3233" n="HIAT:w" s="T1156">мельница</ts>
                  <nts id="Seg_3234" n="HIAT:ip">.</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1161" id="Seg_3237" n="HIAT:u" s="T1157">
                  <ts e="T1158" id="Seg_3239" n="HIAT:w" s="T1157">Пошли</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_3242" n="HIAT:w" s="T1158">туды</ts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1160" id="Seg_3245" n="HIAT:w" s="T1159">в</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_3248" n="HIAT:w" s="T1160">пруд</ts>
                  <nts id="Seg_3249" n="HIAT:ip">.</nts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1173" id="Seg_3251" n="sc" s="T1162">
               <ts e="T1173" id="Seg_3253" n="HIAT:u" s="T1162">
                  <ts e="T1163" id="Seg_3255" n="HIAT:w" s="T1162">И</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_3258" n="HIAT:w" s="T1163">пошли</ts>
                  <nts id="Seg_3259" n="HIAT:ip">,</nts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1165" id="Seg_3262" n="HIAT:w" s="T1164">пошли</ts>
                  <nts id="Seg_3263" n="HIAT:ip">,</nts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1166" id="Seg_3266" n="HIAT:w" s="T1165">и</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_3269" n="HIAT:w" s="T1166">глыбоко</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_3272" n="HIAT:w" s="T1167">зашли</ts>
                  <nts id="Seg_3273" n="HIAT:ip">,</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1169" id="Seg_3276" n="HIAT:w" s="T1168">а</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_3279" n="HIAT:w" s="T1169">там</ts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1171" id="Seg_3282" n="HIAT:w" s="T1170">из</ts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_3285" n="HIAT:w" s="T1171">земли</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1173" id="Seg_3288" n="HIAT:w" s="T1172">родник</ts>
                  <nts id="Seg_3289" n="HIAT:ip">.</nts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1178" id="Seg_3291" n="sc" s="T1174">
               <ts e="T1178" id="Seg_3293" n="HIAT:u" s="T1174">
                  <ts e="T1175" id="Seg_3295" n="HIAT:w" s="T1174">Они</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_3298" n="HIAT:w" s="T1175">зачали</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1177" id="Seg_3301" n="HIAT:w" s="T1176">двое</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_3304" n="HIAT:w" s="T1177">тонуть</ts>
                  <nts id="Seg_3305" n="HIAT:ip">.</nts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1185" id="Seg_3307" n="sc" s="T1179">
               <ts e="T1185" id="Seg_3309" n="HIAT:u" s="T1179">
                  <ts e="T1180" id="Seg_3311" n="HIAT:w" s="T1179">А</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_3314" n="HIAT:w" s="T1180">с</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_3317" n="HIAT:w" s="T1181">имя</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_3320" n="HIAT:w" s="T1182">был</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_3323" n="HIAT:w" s="T1183">один</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_3326" n="HIAT:w" s="T1184">парень</ts>
                  <nts id="Seg_3327" n="HIAT:ip">.</nts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1195" id="Seg_3329" n="sc" s="T1186">
               <ts e="T1195" id="Seg_3331" n="HIAT:u" s="T1186">
                  <ts e="T1187" id="Seg_3333" n="HIAT:w" s="T1186">Он</ts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_3336" n="HIAT:w" s="T1187">подал</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1189" id="Seg_3339" n="HIAT:w" s="T1188">жердь</ts>
                  <nts id="Seg_3340" n="HIAT:ip">,</nts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1190" id="Seg_3343" n="HIAT:w" s="T1189">эту</ts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1191" id="Seg_3346" n="HIAT:w" s="T1190">выдернул</ts>
                  <nts id="Seg_3347" n="HIAT:ip">,</nts>
                  <nts id="Seg_3348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_3350" n="HIAT:w" s="T1191">а</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1193" id="Seg_3353" n="HIAT:w" s="T1192">его</ts>
                  <nts id="Seg_3354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1194" id="Seg_3356" n="HIAT:w" s="T1193">не</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1195" id="Seg_3359" n="HIAT:w" s="T1194">стало</ts>
                  <nts id="Seg_3360" n="HIAT:ip">.</nts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1209" id="Seg_3362" n="sc" s="T1196">
               <ts e="T1209" id="Seg_3364" n="HIAT:u" s="T1196">
                  <ts e="T1197" id="Seg_3366" n="HIAT:w" s="T1196">Пока</ts>
                  <nts id="Seg_3367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_3369" n="HIAT:w" s="T1197">он</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_3372" n="HIAT:w" s="T1198">побежал</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_3375" n="HIAT:w" s="T1199">в</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_3378" n="HIAT:w" s="T1200">деревню</ts>
                  <nts id="Seg_3379" n="HIAT:ip">,</nts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_3382" n="HIAT:w" s="T1201">да</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_3385" n="HIAT:w" s="T1202">отпустили</ts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_3388" n="HIAT:w" s="T1203">воду</ts>
                  <nts id="Seg_3389" n="HIAT:ip">,</nts>
                  <nts id="Seg_3390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1205" id="Seg_3392" n="HIAT:w" s="T1204">вытащили</ts>
                  <nts id="Seg_3393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1206" id="Seg_3395" n="HIAT:w" s="T1205">его</ts>
                  <nts id="Seg_3396" n="HIAT:ip">,</nts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1207" id="Seg_3399" n="HIAT:w" s="T1206">откачивали-откачивали</ts>
                  <nts id="Seg_3400" n="HIAT:ip">,</nts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1208" id="Seg_3403" n="HIAT:w" s="T1207">не</ts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1209" id="Seg_3406" n="HIAT:w" s="T1208">откачали</ts>
                  <nts id="Seg_3407" n="HIAT:ip">.</nts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1229" id="Seg_3409" n="sc" s="T1210">
               <ts e="T1217" id="Seg_3411" n="HIAT:u" s="T1210">
                  <ts e="T1211" id="Seg_3413" n="HIAT:w" s="T1210">А</ts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1212" id="Seg_3416" n="HIAT:w" s="T1211">потом</ts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_3419" n="HIAT:w" s="T1212">привезли</ts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_3422" n="HIAT:w" s="T1213">туды</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1215" id="Seg_3425" n="HIAT:w" s="T1214">мертвого</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1216" id="Seg_3428" n="HIAT:w" s="T1215">и</ts>
                  <nts id="Seg_3429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1217" id="Seg_3431" n="HIAT:w" s="T1216">схоронили</ts>
                  <nts id="Seg_3432" n="HIAT:ip">.</nts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1229" id="Seg_3435" n="HIAT:u" s="T1217">
                  <nts id="Seg_3436" n="HIAT:ip">(</nts>
                  <ts e="T1218" id="Seg_3438" n="HIAT:w" s="T1217">Я</ts>
                  <nts id="Seg_3439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1219" id="Seg_3441" n="HIAT:w" s="T1218">=</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1220" id="Seg_3444" n="HIAT:w" s="T1219">я=</ts>
                  <nts id="Seg_3445" n="HIAT:ip">)</nts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1221" id="Seg_3448" n="HIAT:w" s="T1220">Я</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_3451" n="HIAT:w" s="T1221">его</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_3454" n="HIAT:w" s="T1222">хоронила</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_3458" n="HIAT:w" s="T1223">а</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_3461" n="HIAT:w" s="T1224">оттель</ts>
                  <nts id="Seg_3462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_3464" n="HIAT:w" s="T1225">домой</ts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1227" id="Seg_3467" n="HIAT:w" s="T1226">вернулась</ts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1228" id="Seg_3470" n="HIAT:w" s="T1227">обратно</ts>
                  <nts id="Seg_3471" n="HIAT:ip">,</nts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_3474" n="HIAT:w" s="T1228">приехала</ts>
                  <nts id="Seg_3475" n="HIAT:ip">.</nts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1248" id="Seg_3477" n="sc" s="T1235">
               <ts e="T1242" id="Seg_3479" n="HIAT:u" s="T1235">
                  <ts e="T1236" id="Seg_3481" n="HIAT:w" s="T1235">Ну</ts>
                  <nts id="Seg_3482" n="HIAT:ip">,</nts>
                  <nts id="Seg_3483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1237" id="Seg_3485" n="HIAT:w" s="T1236">переживания</ts>
                  <nts id="Seg_3486" n="HIAT:ip">,</nts>
                  <nts id="Seg_3487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1238" id="Seg_3489" n="HIAT:w" s="T1237">вот</ts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1239" id="Seg_3492" n="HIAT:w" s="T1238">у</ts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_3495" n="HIAT:w" s="T1239">меня</ts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1241" id="Seg_3498" n="HIAT:w" s="T1240">голова</ts>
                  <nts id="Seg_3499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_3501" n="HIAT:w" s="T1241">трясется</ts>
                  <nts id="Seg_3502" n="HIAT:ip">.</nts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1248" id="Seg_3505" n="HIAT:u" s="T1242">
                  <ts e="T1243" id="Seg_3507" n="HIAT:w" s="T1242">Много</ts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_3510" n="HIAT:w" s="T1243">печали</ts>
                  <nts id="Seg_3511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1245" id="Seg_3513" n="HIAT:w" s="T1244">в</ts>
                  <nts id="Seg_3514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_3516" n="HIAT:w" s="T1245">голове</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_3519" n="HIAT:w" s="T1246">в</ts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1248" id="Seg_3522" n="HIAT:w" s="T1247">моёй</ts>
                  <nts id="Seg_3523" n="HIAT:ip">.</nts>
                  <nts id="Seg_3524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1250" id="Seg_3525" n="sc" s="T1249">
               <ts e="T1250" id="Seg_3527" n="HIAT:u" s="T1249">
                  <ts e="T1250" id="Seg_3529" n="HIAT:w" s="T1249">Вот</ts>
                  <nts id="Seg_3530" n="HIAT:ip">.</nts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1275" id="Seg_3532" n="sc" s="T1262">
               <ts e="T1275" id="Seg_3534" n="HIAT:u" s="T1262">
                  <ts e="T1263" id="Seg_3536" n="HIAT:w" s="T1262">Kamen</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_3539" n="HIAT:w" s="T1263">dĭ</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_3542" n="HIAT:w" s="T1264">šobi</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1266" id="Seg_3545" n="HIAT:w" s="T1265">măna:</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3547" n="HIAT:ip">"</nts>
                  <ts e="T1267" id="Seg_3549" n="HIAT:w" s="T1266">Kanžəbəj</ts>
                  <nts id="Seg_3550" n="HIAT:ip">,</nts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1268" id="Seg_3553" n="HIAT:w" s="T1267">dĭn</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_3556" n="HIAT:w" s="T1268">iat</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1270" id="Seg_3559" n="HIAT:w" s="T1269">tibinə</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1271" id="Seg_3562" n="HIAT:w" s="T1270">kalla</ts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1272" id="Seg_3565" n="HIAT:w" s="T1271">dʼürbi</ts>
                  <nts id="Seg_3566" n="HIAT:ip">,</nts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_3569" n="HIAT:w" s="T1272">măn</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_3572" n="HIAT:w" s="T1273">unnʼa</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1275" id="Seg_3575" n="HIAT:w" s="T1274">amnolaʔbiom</ts>
                  <nts id="Seg_3576" n="HIAT:ip">"</nts>
                  <nts id="Seg_3577" n="HIAT:ip">.</nts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1290" id="Seg_3579" n="sc" s="T1276">
               <ts e="T1284" id="Seg_3581" n="HIAT:u" s="T1276">
                  <ts e="T1277" id="Seg_3583" n="HIAT:w" s="T1276">Măn</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1278" id="Seg_3586" n="HIAT:w" s="T1277">mămbiam:</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3588" n="HIAT:ip">"</nts>
                  <ts e="T1279" id="Seg_3590" n="HIAT:w" s="T1278">Kanaʔ</ts>
                  <nts id="Seg_3591" n="HIAT:ip">,</nts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1280" id="Seg_3594" n="HIAT:w" s="T1279">măna</ts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_3597" n="HIAT:w" s="T1280">monoʔkoʔ</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1282" id="Seg_3600" n="HIAT:w" s="T1281">abanə</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_3603" n="HIAT:w" s="T1282">da</ts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_3606" n="HIAT:w" s="T1283">ianə</ts>
                  <nts id="Seg_3607" n="HIAT:ip">"</nts>
                  <nts id="Seg_3608" n="HIAT:ip">.</nts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1290" id="Seg_3611" n="HIAT:u" s="T1284">
                  <ts e="T1285" id="Seg_3613" n="HIAT:w" s="T1284">Dĭ</ts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1286" id="Seg_3616" n="HIAT:w" s="T1285">măndə:</ts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3618" n="HIAT:ip">"</nts>
                  <ts e="T1287" id="Seg_3620" n="HIAT:w" s="T1286">Dĭzeŋ</ts>
                  <nts id="Seg_3621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1288" id="Seg_3623" n="HIAT:w" s="T1287">ej</ts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1289" id="Seg_3626" n="HIAT:w" s="T1288">mĭləʔi</ts>
                  <nts id="Seg_3627" n="HIAT:ip">,</nts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1290" id="Seg_3630" n="HIAT:w" s="T1289">kanžəbəj</ts>
                  <nts id="Seg_3631" n="HIAT:ip">!</nts>
                  <nts id="Seg_3632" n="HIAT:ip">"</nts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1295" id="Seg_3634" n="sc" s="T1291">
               <ts e="T1295" id="Seg_3636" n="HIAT:u" s="T1291">
                  <ts e="T1292" id="Seg_3638" n="HIAT:w" s="T1291">Dĭgəttə</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_3641" n="HIAT:w" s="T1292">măn</ts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1294" id="Seg_3644" n="HIAT:w" s="T1293">oldʼam</ts>
                  <nts id="Seg_3645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1295" id="Seg_3647" n="HIAT:w" s="T1294">oʔbdəbiom</ts>
                  <nts id="Seg_3648" n="HIAT:ip">.</nts>
                  <nts id="Seg_3649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1303" id="Seg_3650" n="sc" s="T1296">
               <ts e="T1303" id="Seg_3652" n="HIAT:u" s="T1296">
                  <ts e="T1297" id="Seg_3654" n="HIAT:w" s="T1296">Nüdʼin</ts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1298" id="Seg_3657" n="HIAT:w" s="T1297">inem</ts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1299" id="Seg_3660" n="HIAT:w" s="T1298">kondlaʔpibaʔ</ts>
                  <nts id="Seg_3661" n="HIAT:ip">,</nts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_3664" n="HIAT:w" s="T1299">amnəbibaʔ</ts>
                  <nts id="Seg_3665" n="HIAT:ip">,</nts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1301" id="Seg_3668" n="HIAT:w" s="T1300">kambibaʔ</ts>
                  <nts id="Seg_3669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1302" id="Seg_3671" n="HIAT:w" s="T1301">Kazan</ts>
                  <nts id="Seg_3672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1303" id="Seg_3674" n="HIAT:w" s="T1302">turanə</ts>
                  <nts id="Seg_3675" n="HIAT:ip">.</nts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1314" id="Seg_3677" n="sc" s="T1304">
               <ts e="T1314" id="Seg_3679" n="HIAT:u" s="T1304">
                  <ts e="T1305" id="Seg_3681" n="HIAT:w" s="T1304">Bar</ts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1306" id="Seg_3684" n="HIAT:w" s="T1305">nüdʼi</ts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1307" id="Seg_3687" n="HIAT:w" s="T1306">dĭn</ts>
                  <nts id="Seg_3688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1308" id="Seg_3690" n="HIAT:w" s="T1307">kunolbibaʔ</ts>
                  <nts id="Seg_3691" n="HIAT:ip">,</nts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1309" id="Seg_3694" n="HIAT:w" s="T1308">dĭgəttə</ts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1310" id="Seg_3697" n="HIAT:w" s="T1309">šobibaʔ</ts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1311" id="Seg_3700" n="HIAT:w" s="T1310">bazo</ts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3702" n="HIAT:ip">(</nts>
                  <ts e="T1312" id="Seg_3704" n="HIAT:w" s="T1311">dĭʔnə</ts>
                  <nts id="Seg_3705" n="HIAT:ip">)</nts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1313" id="Seg_3708" n="HIAT:w" s="T1312">turanə</ts>
                  <nts id="Seg_3709" n="HIAT:ip">,</nts>
                  <nts id="Seg_3710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1314" id="Seg_3712" n="HIAT:w" s="T1313">Kaptɨrlɨktə</ts>
                  <nts id="Seg_3713" n="HIAT:ip">.</nts>
                  <nts id="Seg_3714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1326" id="Seg_3715" n="sc" s="T1315">
               <ts e="T1326" id="Seg_3717" n="HIAT:u" s="T1315">
                  <ts e="T1316" id="Seg_3719" n="HIAT:w" s="T1315">Dĭn</ts>
                  <nts id="Seg_3720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1317" id="Seg_3722" n="HIAT:w" s="T1316">selaj</ts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3724" n="HIAT:ip">(</nts>
                  <ts e="T1318" id="Seg_3726" n="HIAT:w" s="T1317">dʼal-</ts>
                  <nts id="Seg_3727" n="HIAT:ip">)</nts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1319" id="Seg_3730" n="HIAT:w" s="T1318">dʼala</ts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1320" id="Seg_3733" n="HIAT:w" s="T1319">ibibeʔ</ts>
                  <nts id="Seg_3734" n="HIAT:ip">,</nts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1321" id="Seg_3737" n="HIAT:w" s="T1320">dĭgəttə</ts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1322" id="Seg_3740" n="HIAT:w" s="T1321">nüdʼin</ts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1323" id="Seg_3743" n="HIAT:w" s="T1322">šobibaʔ</ts>
                  <nts id="Seg_3744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3745" n="HIAT:ip">(</nts>
                  <ts e="T1324" id="Seg_3747" n="HIAT:w" s="T1323">dĭ=</ts>
                  <nts id="Seg_3748" n="HIAT:ip">)</nts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1325" id="Seg_3751" n="HIAT:w" s="T1324">dĭn</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1326" id="Seg_3754" n="HIAT:w" s="T1325">maːʔnə</ts>
                  <nts id="Seg_3755" n="HIAT:ip">.</nts>
                  <nts id="Seg_3756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1334" id="Seg_3757" n="sc" s="T1327">
               <ts e="T1334" id="Seg_3759" n="HIAT:u" s="T1327">
                  <ts e="T1328" id="Seg_3761" n="HIAT:w" s="T1327">Dĭgəttə</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1329" id="Seg_3764" n="HIAT:w" s="T1328">dĭ</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1330" id="Seg_3767" n="HIAT:w" s="T1329">ertən</ts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1331" id="Seg_3770" n="HIAT:w" s="T1330">šobi</ts>
                  <nts id="Seg_3771" n="HIAT:ip">,</nts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1332" id="Seg_3774" n="HIAT:w" s="T1331">dĭn</ts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1333" id="Seg_3777" n="HIAT:w" s="T1332">kazak</ts>
                  <nts id="Seg_3778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1334" id="Seg_3780" n="HIAT:w" s="T1333">iat</ts>
                  <nts id="Seg_3781" n="HIAT:ip">.</nts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1337" id="Seg_3783" n="sc" s="T1335">
               <ts e="T1337" id="Seg_3785" n="HIAT:u" s="T1335">
                  <ts e="T1336" id="Seg_3787" n="HIAT:w" s="T1335">Dĭgəttə</ts>
                  <nts id="Seg_3788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1337" id="Seg_3790" n="HIAT:w" s="T1336">uʔbdəbibaʔ</ts>
                  <nts id="Seg_3791" n="HIAT:ip">.</nts>
                  <nts id="Seg_3792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1340" id="Seg_3793" n="sc" s="T1338">
               <ts e="T1340" id="Seg_3795" n="HIAT:u" s="T1338">
                  <ts e="T1339" id="Seg_3797" n="HIAT:w" s="T1338">Dĭn</ts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1340" id="Seg_3800" n="HIAT:w" s="T1339">amnobiam</ts>
                  <nts id="Seg_3801" n="HIAT:ip">.</nts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1346" id="Seg_3803" n="sc" s="T1341">
               <ts e="T1346" id="Seg_3805" n="HIAT:u" s="T1341">
                  <nts id="Seg_3806" n="HIAT:ip">(</nts>
                  <ts e="T1342" id="Seg_3808" n="HIAT:w" s="T1341">N-</ts>
                  <nts id="Seg_3809" n="HIAT:ip">)</nts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1343" id="Seg_3812" n="HIAT:w" s="T1342">Noʔ</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1344" id="Seg_3815" n="HIAT:w" s="T1343">jaʔpibaʔ</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1345" id="Seg_3818" n="HIAT:w" s="T1344">dĭn</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1346" id="Seg_3821" n="HIAT:w" s="T1345">bar</ts>
                  <nts id="Seg_3822" n="HIAT:ip">.</nts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1351" id="Seg_3824" n="sc" s="T1347">
               <ts e="T1351" id="Seg_3826" n="HIAT:u" s="T1347">
                  <ts e="T1348" id="Seg_3828" n="HIAT:w" s="T1347">Dĭgəttə</ts>
                  <nts id="Seg_3829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1349" id="Seg_3831" n="HIAT:w" s="T1348">dĭn</ts>
                  <nts id="Seg_3832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1350" id="Seg_3834" n="HIAT:w" s="T1349">dʼadʼat</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1351" id="Seg_3837" n="HIAT:w" s="T1350">šobi</ts>
                  <nts id="Seg_3838" n="HIAT:ip">.</nts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1356" id="Seg_3840" n="sc" s="T1352">
               <ts e="T1356" id="Seg_3842" n="HIAT:u" s="T1352">
                  <nts id="Seg_3843" n="HIAT:ip">"</nts>
                  <ts e="T1353" id="Seg_3845" n="HIAT:w" s="T1352">Šoʔ</ts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1354" id="Seg_3848" n="HIAT:w" s="T1353">măna</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1355" id="Seg_3851" n="HIAT:w" s="T1354">noʔ</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1356" id="Seg_3854" n="HIAT:w" s="T1355">jaʔsittə</ts>
                  <nts id="Seg_3855" n="HIAT:ip">"</nts>
                  <nts id="Seg_3856" n="HIAT:ip">.</nts>
                  <nts id="Seg_3857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1361" id="Seg_3858" n="sc" s="T1357">
               <ts e="T1361" id="Seg_3860" n="HIAT:u" s="T1357">
                  <ts e="T1358" id="Seg_3862" n="HIAT:w" s="T1357">Dĭgəttə</ts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1359" id="Seg_3865" n="HIAT:w" s="T1358">dĭ</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1360" id="Seg_3868" n="HIAT:w" s="T1359">koʔbdom</ts>
                  <nts id="Seg_3869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1361" id="Seg_3871" n="HIAT:w" s="T1360">deʔpi</ts>
                  <nts id="Seg_3872" n="HIAT:ip">.</nts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1368" id="Seg_3874" n="sc" s="T1362">
               <ts e="T1368" id="Seg_3876" n="HIAT:u" s="T1362">
                  <ts e="T1363" id="Seg_3878" n="HIAT:w" s="T1362">Măna</ts>
                  <nts id="Seg_3879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1364" id="Seg_3881" n="HIAT:w" s="T1363">nörbəbiʔi</ts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1365" id="Seg_3884" n="HIAT:w" s="T1364">dĭ</ts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1366" id="Seg_3887" n="HIAT:w" s="T1365">dĭzi</ts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3889" n="HIAT:ip">(</nts>
                  <ts e="T1367" id="Seg_3891" n="HIAT:w" s="T1366">amnozittə=</ts>
                  <nts id="Seg_3892" n="HIAT:ip">)</nts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1368" id="Seg_3895" n="HIAT:w" s="T1367">amnoləj</ts>
                  <nts id="Seg_3896" n="HIAT:ip">.</nts>
                  <nts id="Seg_3897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1384" id="Seg_3898" n="sc" s="T1369">
               <ts e="T1380" id="Seg_3900" n="HIAT:u" s="T1369">
                  <ts e="T1370" id="Seg_3902" n="HIAT:w" s="T1369">Măn</ts>
                  <nts id="Seg_3903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1371" id="Seg_3905" n="HIAT:w" s="T1370">bar</ts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1372" id="Seg_3908" n="HIAT:w" s="T1371">oldʼam</ts>
                  <nts id="Seg_3909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1373" id="Seg_3911" n="HIAT:w" s="T1372">ibiem</ts>
                  <nts id="Seg_3912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1374" id="Seg_3914" n="HIAT:w" s="T1373">i</ts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3916" n="HIAT:ip">(</nts>
                  <ts e="T1375" id="Seg_3918" n="HIAT:w" s="T1374">dĭ</ts>
                  <nts id="Seg_3919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1376" id="Seg_3921" n="HIAT:w" s="T1375">=</ts>
                  <nts id="Seg_3922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1377" id="Seg_3924" n="HIAT:w" s="T1376">dĭ=</ts>
                  <nts id="Seg_3925" n="HIAT:ip">)</nts>
                  <nts id="Seg_3926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1378" id="Seg_3928" n="HIAT:w" s="T1377">dĭn</ts>
                  <nts id="Seg_3929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1379" id="Seg_3931" n="HIAT:w" s="T1378">dʼadʼazi</ts>
                  <nts id="Seg_3932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1380" id="Seg_3934" n="HIAT:w" s="T1379">kambiam</ts>
                  <nts id="Seg_3935" n="HIAT:ip">.</nts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1384" id="Seg_3938" n="HIAT:u" s="T1380">
                  <nts id="Seg_3939" n="HIAT:ip">(</nts>
                  <ts e="T1381" id="Seg_3941" n="HIAT:w" s="T1380">Dĭ=</ts>
                  <nts id="Seg_3942" n="HIAT:ip">)</nts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1382" id="Seg_3945" n="HIAT:w" s="T1381">Dĭ</ts>
                  <nts id="Seg_3946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_3948" n="HIAT:w" s="T1382">bĭdəbi</ts>
                  <nts id="Seg_3949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1384" id="Seg_3951" n="HIAT:w" s="T1383">măna</ts>
                  <nts id="Seg_3952" n="HIAT:ip">.</nts>
                  <nts id="Seg_3953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1392" id="Seg_3954" n="sc" s="T1385">
               <ts e="T1388" id="Seg_3956" n="HIAT:u" s="T1385">
                  <ts e="T1386" id="Seg_3958" n="HIAT:w" s="T1385">Bazo</ts>
                  <nts id="Seg_3959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1387" id="Seg_3961" n="HIAT:w" s="T1386">deʔpi</ts>
                  <nts id="Seg_3962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_3964" n="HIAT:w" s="T1387">maʔndə</ts>
                  <nts id="Seg_3965" n="HIAT:ip">.</nts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1392" id="Seg_3968" n="HIAT:u" s="T1388">
                  <ts e="T1389" id="Seg_3970" n="HIAT:w" s="T1388">Dĭgəttə</ts>
                  <nts id="Seg_3971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1390" id="Seg_3973" n="HIAT:w" s="T1389">măn</ts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1391" id="Seg_3976" n="HIAT:w" s="T1390">iam</ts>
                  <nts id="Seg_3977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_3979" n="HIAT:w" s="T1391">šobi</ts>
                  <nts id="Seg_3980" n="HIAT:ip">.</nts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1397" id="Seg_3982" n="sc" s="T1393">
               <ts e="T1397" id="Seg_3984" n="HIAT:u" s="T1393">
                  <ts e="T1394" id="Seg_3986" n="HIAT:w" s="T1393">Măn</ts>
                  <nts id="Seg_3987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_3989" n="HIAT:w" s="T1394">bazo</ts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1396" id="Seg_3992" n="HIAT:w" s="T1395">kalla</ts>
                  <nts id="Seg_3993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1397" id="Seg_3995" n="HIAT:w" s="T1396">dʼürbim</ts>
                  <nts id="Seg_3996" n="HIAT:ip">.</nts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1410" id="Seg_3998" n="sc" s="T1398">
               <ts e="T1404" id="Seg_4000" n="HIAT:u" s="T1398">
                  <ts e="T1399" id="Seg_4002" n="HIAT:w" s="T1398">Dĭ</ts>
                  <nts id="Seg_4003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_4005" n="HIAT:w" s="T1399">šobi</ts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_4008" n="HIAT:w" s="T1400">da</ts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_4011" n="HIAT:w" s="T1401">măna</ts>
                  <nts id="Seg_4012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1403" id="Seg_4014" n="HIAT:w" s="T1402">kundlambi</ts>
                  <nts id="Seg_4015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1404" id="Seg_4017" n="HIAT:w" s="T1403">maʔnʼi</ts>
                  <nts id="Seg_4018" n="HIAT:ip">.</nts>
                  <nts id="Seg_4019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1410" id="Seg_4021" n="HIAT:u" s="T1404">
                  <ts e="T1405" id="Seg_4023" n="HIAT:w" s="T1404">Iʔbəbeʔ</ts>
                  <nts id="Seg_4024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1406" id="Seg_4026" n="HIAT:w" s="T1405">kunolzittə</ts>
                  <nts id="Seg_4027" n="HIAT:ip">,</nts>
                  <nts id="Seg_4028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1407" id="Seg_4030" n="HIAT:w" s="T1406">dĭgəttə</ts>
                  <nts id="Seg_4031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1408" id="Seg_4033" n="HIAT:w" s="T1407">dĭzeŋ</ts>
                  <nts id="Seg_4034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4035" n="HIAT:ip">(</nts>
                  <ts e="T1409" id="Seg_4037" n="HIAT:w" s="T1408">s-</ts>
                  <nts id="Seg_4038" n="HIAT:ip">)</nts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_4041" n="HIAT:w" s="T1409">šobiʔi</ts>
                  <nts id="Seg_4042" n="HIAT:ip">.</nts>
                  <nts id="Seg_4043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1413" id="Seg_4044" n="sc" s="T1411">
               <ts e="T1413" id="Seg_4046" n="HIAT:u" s="T1411">
                  <nts id="Seg_4047" n="HIAT:ip">"</nts>
                  <ts e="T1412" id="Seg_4049" n="HIAT:w" s="T1411">No</ts>
                  <nts id="Seg_4050" n="HIAT:ip">,</nts>
                  <nts id="Seg_4051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1413" id="Seg_4053" n="HIAT:w" s="T1412">amnogaʔ</ts>
                  <nts id="Seg_4054" n="HIAT:ip">"</nts>
                  <nts id="Seg_4055" n="HIAT:ip">.</nts>
                  <nts id="Seg_4056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1418" id="Seg_4057" n="sc" s="T1414">
               <ts e="T1418" id="Seg_4059" n="HIAT:u" s="T1414">
                  <ts e="T1415" id="Seg_4061" n="HIAT:w" s="T1414">Dĭgəttə</ts>
                  <nts id="Seg_4062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1416" id="Seg_4064" n="HIAT:w" s="T1415">miʔnibeʔ</ts>
                  <nts id="Seg_4065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417" id="Seg_4067" n="HIAT:w" s="T1416">kudajdə</ts>
                  <nts id="Seg_4068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1417.tx-PKZ.1" id="Seg_4070" n="HIAT:w" s="T1417">numan</ts>
                  <nts id="Seg_4071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1418" id="Seg_4073" n="HIAT:w" s="T1417.tx-PKZ.1">üzəbiʔi</ts>
                  <nts id="Seg_4074" n="HIAT:ip">.</nts>
                  <nts id="Seg_4075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1421" id="Seg_4076" n="sc" s="T1419">
               <ts e="T1421" id="Seg_4078" n="HIAT:u" s="T1419">
                  <ts e="T1420" id="Seg_4080" n="HIAT:w" s="T1419">Dĭgəttə</ts>
                  <nts id="Seg_4081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1421" id="Seg_4083" n="HIAT:w" s="T1420">kanbiʔi</ts>
                  <nts id="Seg_4084" n="HIAT:ip">.</nts>
                  <nts id="Seg_4085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1426" id="Seg_4086" n="sc" s="T1422">
               <ts e="T1426" id="Seg_4088" n="HIAT:u" s="T1422">
                  <ts e="T1423" id="Seg_4090" n="HIAT:w" s="T1422">Karəldʼan</ts>
                  <nts id="Seg_4091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1424" id="Seg_4093" n="HIAT:w" s="T1423">uʔbdəbiʔi</ts>
                  <nts id="Seg_4094" n="HIAT:ip">,</nts>
                  <nts id="Seg_4095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1425" id="Seg_4097" n="HIAT:w" s="T1424">miʔnʼibeʔ</ts>
                  <nts id="Seg_4098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1426" id="Seg_4100" n="HIAT:w" s="T1425">šobiʔi</ts>
                  <nts id="Seg_4101" n="HIAT:ip">.</nts>
                  <nts id="Seg_4102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1432" id="Seg_4103" n="sc" s="T1427">
               <ts e="T1432" id="Seg_4105" n="HIAT:u" s="T1427">
                  <ts e="T1428" id="Seg_4107" n="HIAT:w" s="T1427">Amorbiʔi</ts>
                  <nts id="Seg_4108" n="HIAT:ip">,</nts>
                  <nts id="Seg_4109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1429" id="Seg_4111" n="HIAT:w" s="T1428">dĭgəttə</ts>
                  <nts id="Seg_4112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1430" id="Seg_4114" n="HIAT:w" s="T1429">kalla</ts>
                  <nts id="Seg_4115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1431" id="Seg_4117" n="HIAT:w" s="T1430">dʼürbi</ts>
                  <nts id="Seg_4118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1432" id="Seg_4120" n="HIAT:w" s="T1431">maːʔndə</ts>
                  <nts id="Seg_4121" n="HIAT:ip">.</nts>
                  <nts id="Seg_4122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1439" id="Seg_4123" n="sc" s="T1433">
               <ts e="T1439" id="Seg_4125" n="HIAT:u" s="T1433">
                  <ts e="T1434" id="Seg_4127" n="HIAT:w" s="T1433">Dĭgəttə</ts>
                  <nts id="Seg_4128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1435" id="Seg_4130" n="HIAT:w" s="T1434">dĭ</ts>
                  <nts id="Seg_4131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1436" id="Seg_4133" n="HIAT:w" s="T1435">kambi</ts>
                  <nts id="Seg_4134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1437" id="Seg_4136" n="HIAT:w" s="T1436">dʼadʼanə</ts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1438" id="Seg_4139" n="HIAT:w" s="T1437">noʔ</ts>
                  <nts id="Seg_4140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1439" id="Seg_4142" n="HIAT:w" s="T1438">jaʔsittə</ts>
                  <nts id="Seg_4143" n="HIAT:ip">.</nts>
                  <nts id="Seg_4144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1452" id="Seg_4145" n="sc" s="T1440">
               <ts e="T1449" id="Seg_4147" n="HIAT:u" s="T1440">
                  <ts e="T1441" id="Seg_4149" n="HIAT:w" s="T1440">I</ts>
                  <nts id="Seg_4150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1442" id="Seg_4152" n="HIAT:w" s="T1441">dĭn</ts>
                  <nts id="Seg_4153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1443" id="Seg_4155" n="HIAT:w" s="T1442">dʼala</ts>
                  <nts id="Seg_4156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4157" n="HIAT:ip">(</nts>
                  <ts e="T1444" id="Seg_4159" n="HIAT:w" s="T1443">dĭg-</ts>
                  <nts id="Seg_4160" n="HIAT:ip">)</nts>
                  <nts id="Seg_4161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1445" id="Seg_4163" n="HIAT:w" s="T1444">dĭrgit</ts>
                  <nts id="Seg_4164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1446" id="Seg_4166" n="HIAT:w" s="T1445">ibi</ts>
                  <nts id="Seg_4167" n="HIAT:ip">,</nts>
                  <nts id="Seg_4168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1447" id="Seg_4170" n="HIAT:w" s="T1446">dĭ</ts>
                  <nts id="Seg_4171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4172" n="HIAT:ip">(</nts>
                  <ts e="T1448" id="Seg_4174" n="HIAT:w" s="T1447">büzəzi</ts>
                  <nts id="Seg_4175" n="HIAT:ip">)</nts>
                  <nts id="Seg_4176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1449" id="Seg_4178" n="HIAT:w" s="T1448">boskəndə</ts>
                  <nts id="Seg_4179" n="HIAT:ip">.</nts>
                  <nts id="Seg_4180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1452" id="Seg_4182" n="HIAT:u" s="T1449">
                  <ts e="T1450" id="Seg_4184" n="HIAT:w" s="T1449">Gəttə</ts>
                  <nts id="Seg_4185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1451" id="Seg_4187" n="HIAT:w" s="T1450">kambiʔi</ts>
                  <nts id="Seg_4188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1452" id="Seg_4190" n="HIAT:w" s="T1451">bünə</ts>
                  <nts id="Seg_4191" n="HIAT:ip">.</nts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1461" id="Seg_4193" n="sc" s="T1453">
               <ts e="T1461" id="Seg_4195" n="HIAT:u" s="T1453">
                  <ts e="T1454" id="Seg_4197" n="HIAT:w" s="T1453">I</ts>
                  <nts id="Seg_4198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1455" id="Seg_4200" n="HIAT:w" s="T1454">dĭn</ts>
                  <nts id="Seg_4201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1456" id="Seg_4203" n="HIAT:w" s="T1455">šübiʔi</ts>
                  <nts id="Seg_4204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1457" id="Seg_4206" n="HIAT:w" s="T1456">bünə</ts>
                  <nts id="Seg_4207" n="HIAT:ip">,</nts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1458" id="Seg_4210" n="HIAT:w" s="T1457">onʼiʔ</ts>
                  <nts id="Seg_4211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1459" id="Seg_4213" n="HIAT:w" s="T1458">koʔbdo</ts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1460" id="Seg_4216" n="HIAT:w" s="T1459">i</ts>
                  <nts id="Seg_4217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1461" id="Seg_4219" n="HIAT:w" s="T1460">dĭ</ts>
                  <nts id="Seg_4220" n="HIAT:ip">.</nts>
                  <nts id="Seg_4221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1463" id="Seg_4222" n="sc" s="T1462">
               <ts e="T1463" id="Seg_4224" n="HIAT:u" s="T1462">
                  <ts e="T1463" id="Seg_4226" n="HIAT:w" s="T1462">bügən</ts>
                  <nts id="Seg_4227" n="HIAT:ip">.</nts>
                  <nts id="Seg_4228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1477" id="Seg_4229" n="sc" s="T1464">
               <ts e="T1477" id="Seg_4231" n="HIAT:u" s="T1464">
                  <ts e="T1465" id="Seg_4233" n="HIAT:w" s="T1464">Dĭgəttə</ts>
                  <nts id="Seg_4234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1466" id="Seg_4236" n="HIAT:w" s="T1465">pa</ts>
                  <nts id="Seg_4237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1467" id="Seg_4239" n="HIAT:w" s="T1466">onʼiʔ</ts>
                  <nts id="Seg_4240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1468" id="Seg_4242" n="HIAT:w" s="T1467">nʼi</ts>
                  <nts id="Seg_4243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1469" id="Seg_4245" n="HIAT:w" s="T1468">mĭbi</ts>
                  <nts id="Seg_4246" n="HIAT:ip">,</nts>
                  <nts id="Seg_4247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1470" id="Seg_4249" n="HIAT:w" s="T1469">dĭ</ts>
                  <nts id="Seg_4250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1471" id="Seg_4252" n="HIAT:w" s="T1470">koʔbdom</ts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1472" id="Seg_4255" n="HIAT:w" s="T1471">deʔpi</ts>
                  <nts id="Seg_4256" n="HIAT:ip">,</nts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1473" id="Seg_4259" n="HIAT:w" s="T1472">a</ts>
                  <nts id="Seg_4260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1474" id="Seg_4262" n="HIAT:w" s="T1473">dĭ</ts>
                  <nts id="Seg_4263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1475" id="Seg_4265" n="HIAT:w" s="T1474">külaːmbi</ts>
                  <nts id="Seg_4266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4267" n="HIAT:ip">(</nts>
                  <ts e="T1476" id="Seg_4269" n="HIAT:w" s="T1475">bünən-</ts>
                  <nts id="Seg_4270" n="HIAT:ip">)</nts>
                  <nts id="Seg_4271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1477" id="Seg_4273" n="HIAT:w" s="T1476">bügən</ts>
                  <nts id="Seg_4274" n="HIAT:ip">.</nts>
                  <nts id="Seg_4275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1480" id="Seg_4276" n="sc" s="T1478">
               <ts e="T1480" id="Seg_4278" n="HIAT:u" s="T1478">
                  <nts id="Seg_4279" n="HIAT:ip">(</nts>
                  <nts id="Seg_4280" n="HIAT:ip">(</nts>
                  <ats e="T1480" id="Seg_4281" n="HIAT:non-pho" s="T1478">DMG</ats>
                  <nts id="Seg_4282" n="HIAT:ip">)</nts>
                  <nts id="Seg_4283" n="HIAT:ip">)</nts>
                  <nts id="Seg_4284" n="HIAT:ip">.</nts>
                  <nts id="Seg_4285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1492" id="Seg_4286" n="sc" s="T1484">
               <ts e="T1488" id="Seg_4288" n="HIAT:u" s="T1484">
                  <ts e="T1488" id="Seg_4290" n="HIAT:w" s="T1484">Погодите</ts>
                  <nts id="Seg_4291" n="HIAT:ip">.</nts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1492" id="Seg_4294" n="HIAT:u" s="T1488">
                  <ts e="T1489" id="Seg_4296" n="HIAT:w" s="T1488">Чего</ts>
                  <nts id="Seg_4297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1490" id="Seg_4299" n="HIAT:w" s="T1489">рассказать</ts>
                  <nts id="Seg_4300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1491" id="Seg_4302" n="HIAT:w" s="T1490">коротенько</ts>
                  <nts id="Seg_4303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1492" id="Seg_4305" n="HIAT:w" s="T1491">такое</ts>
                  <nts id="Seg_4306" n="HIAT:ip">.</nts>
                  <nts id="Seg_4307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1508" id="Seg_4308" n="sc" s="T1495">
               <ts e="T1508" id="Seg_4310" n="HIAT:u" s="T1495">
                  <ts e="T1498" id="Seg_4312" n="HIAT:w" s="T1495">Nu</ts>
                  <nts id="Seg_4313" n="HIAT:ip">,</nts>
                  <nts id="Seg_4314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4315" n="HIAT:ip">(</nts>
                  <ts e="T1499" id="Seg_4317" n="HIAT:w" s="T1498">karəlʼdʼa-</ts>
                  <nts id="Seg_4318" n="HIAT:ip">)</nts>
                  <nts id="Seg_4319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1500" id="Seg_4321" n="HIAT:w" s="T1499">karəlʼdʼan</ts>
                  <nts id="Seg_4322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4323" n="HIAT:ip">(</nts>
                  <ts e="T1503" id="Seg_4325" n="HIAT:w" s="T1500">š-</ts>
                  <nts id="Seg_4326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1506" id="Seg_4328" n="HIAT:w" s="T1503">šo-</ts>
                  <nts id="Seg_4329" n="HIAT:ip">)</nts>
                  <nts id="Seg_4330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1508" id="Seg_4332" n="HIAT:w" s="T1506">karəlʼdʼan</ts>
                  <nts id="Seg_4333" n="HIAT:ip">…</nts>
                  <nts id="Seg_4334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1512" id="Seg_4335" n="sc" s="T1511">
               <ts e="T1512" id="Seg_4337" n="HIAT:u" s="T1511">
                  <nts id="Seg_4338" n="HIAT:ip">(</nts>
                  <nts id="Seg_4339" n="HIAT:ip">(</nts>
                  <ats e="T1512" id="Seg_4340" n="HIAT:non-pho" s="T1511">BRK</ats>
                  <nts id="Seg_4341" n="HIAT:ip">)</nts>
                  <nts id="Seg_4342" n="HIAT:ip">)</nts>
                  <nts id="Seg_4343" n="HIAT:ip">.</nts>
                  <nts id="Seg_4344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1514" id="Seg_4345" n="sc" s="T1513">
               <ts e="T1514" id="Seg_4347" n="HIAT:u" s="T1513">
                  <ts e="T1514" id="Seg_4349" n="HIAT:w" s="T1513">Кончилось</ts>
                  <nts id="Seg_4350" n="HIAT:ip">.</nts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1521" id="Seg_4352" n="sc" s="T1518">
               <ts e="T1521" id="Seg_4354" n="HIAT:u" s="T1518">
                  <ts e="T1519" id="Seg_4356" n="HIAT:w" s="T1518">А</ts>
                  <nts id="Seg_4357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1520" id="Seg_4359" n="HIAT:w" s="T1519">там</ts>
                  <nts id="Seg_4360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1521" id="Seg_4362" n="HIAT:w" s="T1520">же</ts>
                  <nts id="Seg_4363" n="HIAT:ip">…</nts>
                  <nts id="Seg_4364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1538" id="Seg_4365" n="sc" s="T1529">
               <ts e="T1538" id="Seg_4367" n="HIAT:u" s="T1529">
                  <nts id="Seg_4368" n="HIAT:ip">(</nts>
                  <ts e="T1530" id="Seg_4370" n="HIAT:w" s="T1529">А</ts>
                  <nts id="Seg_4371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1531" id="Seg_4373" n="HIAT:w" s="T1530">по-</ts>
                  <nts id="Seg_4374" n="HIAT:ip">)</nts>
                  <nts id="Seg_4375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1532" id="Seg_4377" n="HIAT:w" s="T1531">А</ts>
                  <nts id="Seg_4378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1533" id="Seg_4380" n="HIAT:w" s="T1532">по-русски</ts>
                  <nts id="Seg_4381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1534" id="Seg_4383" n="HIAT:w" s="T1533">это</ts>
                  <nts id="Seg_4384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1535" id="Seg_4386" n="HIAT:w" s="T1534">я</ts>
                  <nts id="Seg_4387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1536" id="Seg_4389" n="HIAT:w" s="T1535">говорю</ts>
                  <nts id="Seg_4390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1538" id="Seg_4392" n="HIAT:w" s="T1536">приедет</ts>
                  <nts id="Seg_4393" n="HIAT:ip">…</nts>
                  <nts id="Seg_4394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1543" id="Seg_4395" n="sc" s="T1539">
               <ts e="T1543" id="Seg_4397" n="HIAT:u" s="T1539">
                  <ts e="T1540" id="Seg_4399" n="HIAT:w" s="T1539">Приедет</ts>
                  <nts id="Seg_4400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1541" id="Seg_4402" n="HIAT:w" s="T1540">мой</ts>
                  <nts id="Seg_4403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1542" id="Seg_4405" n="HIAT:w" s="T1541">брат</ts>
                  <nts id="Seg_4406" n="HIAT:ip">,</nts>
                  <nts id="Seg_4407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1543" id="Seg_4409" n="HIAT:w" s="T1542">Арпит</ts>
                  <nts id="Seg_4410" n="HIAT:ip">.</nts>
                  <nts id="Seg_4411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1548" id="Seg_4412" n="sc" s="T1544">
               <ts e="T1548" id="Seg_4414" n="HIAT:u" s="T1544">
                  <ts e="T1545" id="Seg_4416" n="HIAT:w" s="T1544">Вот</ts>
                  <nts id="Seg_4417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1547" id="Seg_4419" n="HIAT:w" s="T1545">по-русски</ts>
                  <nts id="Seg_4420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1548" id="Seg_4422" n="HIAT:w" s="T1547">теперь</ts>
                  <nts id="Seg_4423" n="HIAT:ip">.</nts>
                  <nts id="Seg_4424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1552" id="Seg_4425" n="sc" s="T1549">
               <ts e="T1552" id="Seg_4427" n="HIAT:u" s="T1549">
                  <ts e="T1550" id="Seg_4429" n="HIAT:w" s="T1549">Ты</ts>
                  <nts id="Seg_4430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1551" id="Seg_4432" n="HIAT:w" s="T1550">будешь</ts>
                  <nts id="Seg_4433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1552" id="Seg_4435" n="HIAT:w" s="T1551">рад</ts>
                  <nts id="Seg_4436" n="HIAT:ip">.</nts>
                  <nts id="Seg_4437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1563" id="Seg_4438" n="sc" s="T1562">
               <ts e="T1563" id="Seg_4440" n="HIAT:u" s="T1562">
                  <ts e="T1563" id="Seg_4442" n="HIAT:w" s="T1562">А</ts>
                  <nts id="Seg_4443" n="HIAT:ip">…</nts>
                  <nts id="Seg_4444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1568" id="Seg_4445" n="sc" s="T1564">
               <ts e="T1568" id="Seg_4447" n="HIAT:u" s="T1564">
                  <ts e="T1566" id="Seg_4449" n="HIAT:w" s="T1564">Опять</ts>
                  <nts id="Seg_4450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4451" n="HIAT:ip">(</nts>
                  <ts e="T1568" id="Seg_4453" n="HIAT:w" s="T1566">по-кам-</ts>
                  <nts id="Seg_4454" n="HIAT:ip">)</nts>
                  <nts id="Seg_4455" n="HIAT:ip">.</nts>
                  <nts id="Seg_4456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1574" id="Seg_4457" n="sc" s="T1571">
               <ts e="T1574" id="Seg_4459" n="HIAT:u" s="T1571">
                  <ts e="T1574" id="Seg_4461" n="HIAT:w" s="T1571">Ну-ну</ts>
                  <nts id="Seg_4462" n="HIAT:ip">.</nts>
                  <nts id="Seg_4463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1579" id="Seg_4464" n="sc" s="T1576">
               <ts e="T1579" id="Seg_4466" n="HIAT:u" s="T1576">
                  <ts e="T1577" id="Seg_4468" n="HIAT:w" s="T1576">Ну</ts>
                  <nts id="Seg_4469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1578" id="Seg_4471" n="HIAT:w" s="T1577">наладил</ts>
                  <nts id="Seg_4472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1579" id="Seg_4474" n="HIAT:w" s="T1578">ты</ts>
                  <nts id="Seg_4475" n="HIAT:ip">?</nts>
                  <nts id="Seg_4476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1590" id="Seg_4477" n="sc" s="T1586">
               <ts e="T1590" id="Seg_4479" n="HIAT:u" s="T1586">
                  <ts e="T1587" id="Seg_4481" n="HIAT:w" s="T1586">Măn</ts>
                  <nts id="Seg_4482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1588" id="Seg_4484" n="HIAT:w" s="T1587">karəlʼdʼan</ts>
                  <nts id="Seg_4485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1589" id="Seg_4487" n="HIAT:w" s="T1588">edəʔleʔbəm</ts>
                  <nts id="Seg_4488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1590" id="Seg_4490" n="HIAT:w" s="T1589">Arpitəm</ts>
                  <nts id="Seg_4491" n="HIAT:ip">.</nts>
                  <nts id="Seg_4492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1600" id="Seg_4493" n="sc" s="T1591">
               <ts e="T1600" id="Seg_4495" n="HIAT:u" s="T1591">
                  <ts e="T1592" id="Seg_4497" n="HIAT:w" s="T1591">Dĭ</ts>
                  <nts id="Seg_4498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4499" n="HIAT:ip">(</nts>
                  <ts e="T1593" id="Seg_4501" n="HIAT:w" s="T1592">š-</ts>
                  <nts id="Seg_4502" n="HIAT:ip">)</nts>
                  <nts id="Seg_4503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1594" id="Seg_4505" n="HIAT:w" s="T1593">mămbi:</ts>
                  <nts id="Seg_4506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4507" n="HIAT:ip">"</nts>
                  <ts e="T1595" id="Seg_4509" n="HIAT:w" s="T1594">Šolam</ts>
                  <nts id="Seg_4510" n="HIAT:ip">,</nts>
                  <nts id="Seg_4511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1596" id="Seg_4513" n="HIAT:w" s="T1595">iləm</ts>
                  <nts id="Seg_4514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1597" id="Seg_4516" n="HIAT:w" s="T1596">tănan</ts>
                  <nts id="Seg_4517" n="HIAT:ip">,</nts>
                  <nts id="Seg_4518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1598" id="Seg_4520" n="HIAT:w" s="T1597">kanžəbəj</ts>
                  <nts id="Seg_4521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1599" id="Seg_4523" n="HIAT:w" s="T1598">măn</ts>
                  <nts id="Seg_4524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1600" id="Seg_4526" n="HIAT:w" s="T1599">ianə</ts>
                  <nts id="Seg_4527" n="HIAT:ip">"</nts>
                  <nts id="Seg_4528" n="HIAT:ip">.</nts>
                  <nts id="Seg_4529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1608" id="Seg_4530" n="sc" s="T1601">
               <ts e="T1608" id="Seg_4532" n="HIAT:u" s="T1601">
                  <ts e="T1602" id="Seg_4534" n="HIAT:w" s="T1601">Dĭgəttə</ts>
                  <nts id="Seg_4535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1603" id="Seg_4537" n="HIAT:w" s="T1602">măn</ts>
                  <nts id="Seg_4538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1604" id="Seg_4540" n="HIAT:w" s="T1603">gibərdə</ts>
                  <nts id="Seg_4541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1605" id="Seg_4543" n="HIAT:w" s="T1604">ej</ts>
                  <nts id="Seg_4544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1606" id="Seg_4546" n="HIAT:w" s="T1605">kalam</ts>
                  <nts id="Seg_4547" n="HIAT:ip">,</nts>
                  <nts id="Seg_4548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4549" n="HIAT:ip">(</nts>
                  <ts e="T1607" id="Seg_4551" n="HIAT:w" s="T1606">edəʔləm</ts>
                  <nts id="Seg_4552" n="HIAT:ip">)</nts>
                  <nts id="Seg_4553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1608" id="Seg_4555" n="HIAT:w" s="T1607">dĭm</ts>
                  <nts id="Seg_4556" n="HIAT:ip">.</nts>
                  <nts id="Seg_4557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1612" id="Seg_4558" n="sc" s="T1609">
               <ts e="T1612" id="Seg_4560" n="HIAT:u" s="T1609">
                  <ts e="T1610" id="Seg_4562" n="HIAT:w" s="T1609">Kamen</ts>
                  <nts id="Seg_4563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1611" id="Seg_4565" n="HIAT:w" s="T1610">dĭ</ts>
                  <nts id="Seg_4566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1612" id="Seg_4568" n="HIAT:w" s="T1611">šoləj</ts>
                  <nts id="Seg_4569" n="HIAT:ip">.</nts>
                  <nts id="Seg_4570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1627" id="Seg_4571" n="sc" s="T1619">
               <ts e="T1627" id="Seg_4573" n="HIAT:u" s="T1619">
                  <ts e="T1620" id="Seg_4575" n="HIAT:w" s="T1619">Я</ts>
                  <nts id="Seg_4576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1621" id="Seg_4578" n="HIAT:w" s="T1620">завтра</ts>
                  <nts id="Seg_4579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1622" id="Seg_4581" n="HIAT:w" s="T1621">буду</ts>
                  <nts id="Seg_4582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1623" id="Seg_4584" n="HIAT:w" s="T1622">дожидать</ts>
                  <nts id="Seg_4585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4586" n="HIAT:ip">(</nts>
                  <ts e="T1624" id="Seg_4588" n="HIAT:w" s="T1623">э-</ts>
                  <nts id="Seg_4589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1625" id="Seg_4591" n="HIAT:w" s="T1624">этого=</ts>
                  <nts id="Seg_4592" n="HIAT:ip">)</nts>
                  <nts id="Seg_4593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4594" n="HIAT:ip">(</nts>
                  <ts e="T1626" id="Seg_4596" n="HIAT:w" s="T1625">Ар-</ts>
                  <nts id="Seg_4597" n="HIAT:ip">)</nts>
                  <nts id="Seg_4598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1627" id="Seg_4600" n="HIAT:w" s="T1626">Арпита</ts>
                  <nts id="Seg_4601" n="HIAT:ip">.</nts>
                  <nts id="Seg_4602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1643" id="Seg_4603" n="sc" s="T1628">
               <ts e="T1643" id="Seg_4605" n="HIAT:u" s="T1628">
                  <ts e="T1629" id="Seg_4607" n="HIAT:w" s="T1628">Никуды</ts>
                  <nts id="Seg_4608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1630" id="Seg_4610" n="HIAT:w" s="T1629">не</ts>
                  <nts id="Seg_4611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1631" id="Seg_4613" n="HIAT:w" s="T1630">буду</ts>
                  <nts id="Seg_4614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1632" id="Seg_4616" n="HIAT:w" s="T1631">идти</ts>
                  <nts id="Seg_4617" n="HIAT:ip">,</nts>
                  <nts id="Seg_4618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1633" id="Seg_4620" n="HIAT:w" s="T1632">он</ts>
                  <nts id="Seg_4621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1634" id="Seg_4623" n="HIAT:w" s="T1633">приедет</ts>
                  <nts id="Seg_4624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1635" id="Seg_4626" n="HIAT:w" s="T1634">и</ts>
                  <nts id="Seg_4627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4628" n="HIAT:ip">(</nts>
                  <ts e="T1636" id="Seg_4630" n="HIAT:w" s="T1635">пойд-</ts>
                  <nts id="Seg_4631" n="HIAT:ip">)</nts>
                  <nts id="Seg_4632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1637" id="Seg_4634" n="HIAT:w" s="T1636">поедем</ts>
                  <nts id="Seg_4635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4636" n="HIAT:ip">(</nts>
                  <ts e="T1638" id="Seg_4638" n="HIAT:w" s="T1637">с-</ts>
                  <nts id="Seg_4639" n="HIAT:ip">)</nts>
                  <nts id="Seg_4640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1639" id="Seg_4642" n="HIAT:w" s="T1638">с</ts>
                  <nts id="Seg_4643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1640" id="Seg_4645" n="HIAT:w" s="T1639">им</ts>
                  <nts id="Seg_4646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1641" id="Seg_4648" n="HIAT:w" s="T1640">к</ts>
                  <nts id="Seg_4649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1642" id="Seg_4651" n="HIAT:w" s="T1641">его</ts>
                  <nts id="Seg_4652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1643" id="Seg_4654" n="HIAT:w" s="T1642">матери</ts>
                  <nts id="Seg_4655" n="HIAT:ip">.</nts>
                  <nts id="Seg_4656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1646" id="Seg_4657" n="sc" s="T1644">
               <ts e="T1646" id="Seg_4659" n="HIAT:u" s="T1644">
                  <ts e="T1645" id="Seg_4661" n="HIAT:w" s="T1644">Вот</ts>
                  <nts id="Seg_4662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1646" id="Seg_4664" n="HIAT:w" s="T1645">это</ts>
                  <nts id="Seg_4665" n="HIAT:ip">.</nts>
                  <nts id="Seg_4666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1649" id="Seg_4667" n="sc" s="T1647">
               <ts e="T1649" id="Seg_4669" n="HIAT:u" s="T1647">
                  <nts id="Seg_4670" n="HIAT:ip">(</nts>
                  <nts id="Seg_4671" n="HIAT:ip">(</nts>
                  <ats e="T1648" id="Seg_4672" n="HIAT:non-pho" s="T1647">BRK</ats>
                  <nts id="Seg_4673" n="HIAT:ip">)</nts>
                  <nts id="Seg_4674" n="HIAT:ip">)</nts>
                  <nts id="Seg_4675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1649" id="Seg_4677" n="HIAT:w" s="T1648">коротенько</ts>
                  <nts id="Seg_4678" n="HIAT:ip">.</nts>
                  <nts id="Seg_4679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1659" id="Seg_4680" n="sc" s="T1650">
               <ts e="T1659" id="Seg_4682" n="HIAT:u" s="T1650">
                  <ts e="T1651" id="Seg_4684" n="HIAT:w" s="T1650">Măn</ts>
                  <nts id="Seg_4685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1652" id="Seg_4687" n="HIAT:w" s="T1651">teinen</ts>
                  <nts id="Seg_4688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4689" n="HIAT:ip">(</nts>
                  <ts e="T1653" id="Seg_4691" n="HIAT:w" s="T1652">koum-</ts>
                  <nts id="Seg_4692" n="HIAT:ip">)</nts>
                  <nts id="Seg_4693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1654" id="Seg_4695" n="HIAT:w" s="T1653">kunolbiam</ts>
                  <nts id="Seg_4696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1655" id="Seg_4698" n="HIAT:w" s="T1654">i</ts>
                  <nts id="Seg_4699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1656" id="Seg_4701" n="HIAT:w" s="T1655">dʼodənʼi</ts>
                  <nts id="Seg_4702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1657" id="Seg_4704" n="HIAT:w" s="T1656">kubiam</ts>
                  <nts id="Seg_4705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1658" id="Seg_4707" n="HIAT:w" s="T1657">onʼiʔ</ts>
                  <nts id="Seg_4708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1659" id="Seg_4710" n="HIAT:w" s="T1658">kuza</ts>
                  <nts id="Seg_4711" n="HIAT:ip">.</nts>
                  <nts id="Seg_4712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1666" id="Seg_4713" n="sc" s="T1660">
               <ts e="T1666" id="Seg_4715" n="HIAT:u" s="T1660">
                  <ts e="T1661" id="Seg_4717" n="HIAT:w" s="T1660">Dĭ</ts>
                  <nts id="Seg_4718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1662" id="Seg_4720" n="HIAT:w" s="T1661">măna</ts>
                  <nts id="Seg_4721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4722" n="HIAT:ip">(</nts>
                  <ts e="T1663" id="Seg_4724" n="HIAT:w" s="T1662">k-</ts>
                  <nts id="Seg_4725" n="HIAT:ip">)</nts>
                  <nts id="Seg_4726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1664" id="Seg_4728" n="HIAT:w" s="T1663">măndə:</ts>
                  <nts id="Seg_4729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4730" n="HIAT:ip">"</nts>
                  <ts e="T1665" id="Seg_4732" n="HIAT:w" s="T1664">Kanžəbəj</ts>
                  <nts id="Seg_4733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1666" id="Seg_4735" n="HIAT:w" s="T1665">gibər-nʼibudʼ</ts>
                  <nts id="Seg_4736" n="HIAT:ip">"</nts>
                  <nts id="Seg_4737" n="HIAT:ip">.</nts>
                  <nts id="Seg_4738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1671" id="Seg_4739" n="sc" s="T1667">
               <ts e="T1671" id="Seg_4741" n="HIAT:u" s="T1667">
                  <ts e="T1668" id="Seg_4743" n="HIAT:w" s="T1667">Măn</ts>
                  <nts id="Seg_4744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1669" id="Seg_4746" n="HIAT:w" s="T1668">măndəm:</ts>
                  <nts id="Seg_4747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4748" n="HIAT:ip">"</nts>
                  <ts e="T1670" id="Seg_4750" n="HIAT:w" s="T1669">Ej</ts>
                  <nts id="Seg_4751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1671" id="Seg_4753" n="HIAT:w" s="T1670">kalam</ts>
                  <nts id="Seg_4754" n="HIAT:ip">"</nts>
                  <nts id="Seg_4755" n="HIAT:ip">.</nts>
                  <nts id="Seg_4756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1685" id="Seg_4757" n="sc" s="T1672">
               <ts e="T1685" id="Seg_4759" n="HIAT:u" s="T1672">
                  <ts e="T1673" id="Seg_4761" n="HIAT:w" s="T1672">Я</ts>
                  <nts id="Seg_4762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1674" id="Seg_4764" n="HIAT:w" s="T1673">сегодня</ts>
                  <nts id="Seg_4765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1675" id="Seg_4767" n="HIAT:w" s="T1674">спала</ts>
                  <nts id="Seg_4768" n="HIAT:ip">,</nts>
                  <nts id="Seg_4769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1676" id="Seg_4771" n="HIAT:w" s="T1675">видела</ts>
                  <nts id="Seg_4772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1677" id="Seg_4774" n="HIAT:w" s="T1676">во</ts>
                  <nts id="Seg_4775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1678" id="Seg_4777" n="HIAT:w" s="T1677">сне</ts>
                  <nts id="Seg_4778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4779" n="HIAT:ip">—</nts>
                  <nts id="Seg_4780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1679" id="Seg_4782" n="HIAT:w" s="T1678">один</ts>
                  <nts id="Seg_4783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1680" id="Seg_4785" n="HIAT:w" s="T1679">человек</ts>
                  <nts id="Seg_4786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1681" id="Seg_4788" n="HIAT:w" s="T1680">зовет</ts>
                  <nts id="Seg_4789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1682" id="Seg_4791" n="HIAT:w" s="T1681">меня:</ts>
                  <nts id="Seg_4792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4793" n="HIAT:ip">"</nts>
                  <ts e="T1683" id="Seg_4795" n="HIAT:w" s="T1682">Пойдем</ts>
                  <nts id="Seg_4796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1684" id="Seg_4798" n="HIAT:w" s="T1683">со</ts>
                  <nts id="Seg_4799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1685" id="Seg_4801" n="HIAT:w" s="T1684">мной</ts>
                  <nts id="Seg_4802" n="HIAT:ip">"</nts>
                  <nts id="Seg_4803" n="HIAT:ip">.</nts>
                  <nts id="Seg_4804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1691" id="Seg_4805" n="sc" s="T1686">
               <ts e="T1691" id="Seg_4807" n="HIAT:u" s="T1686">
                  <ts e="T1687" id="Seg_4809" n="HIAT:w" s="T1686">А</ts>
                  <nts id="Seg_4810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1688" id="Seg_4812" n="HIAT:w" s="T1687">я</ts>
                  <nts id="Seg_4813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1689" id="Seg_4815" n="HIAT:w" s="T1688">говорю:</ts>
                  <nts id="Seg_4816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4817" n="HIAT:ip">"</nts>
                  <ts e="T1690" id="Seg_4819" n="HIAT:w" s="T1689">Не</ts>
                  <nts id="Seg_4820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1691" id="Seg_4822" n="HIAT:w" s="T1690">пойду</ts>
                  <nts id="Seg_4823" n="HIAT:ip">"</nts>
                  <nts id="Seg_4824" n="HIAT:ip">.</nts>
                  <nts id="Seg_4825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1694" id="Seg_4826" n="sc" s="T1692">
               <ts e="T1694" id="Seg_4828" n="HIAT:u" s="T1692">
                  <ts e="T1693" id="Seg_4830" n="HIAT:w" s="T1692">Кончилось</ts>
                  <nts id="Seg_4831" n="HIAT:ip">,</nts>
                  <nts id="Seg_4832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1694" id="Seg_4834" n="HIAT:w" s="T1693">нет</ts>
                  <nts id="Seg_4835" n="HIAT:ip">?</nts>
                  <nts id="Seg_4836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1696" id="Seg_4837" n="sc" s="T1695">
               <ts e="T1696" id="Seg_4839" n="HIAT:u" s="T1695">
                  <nts id="Seg_4840" n="HIAT:ip">(</nts>
                  <nts id="Seg_4841" n="HIAT:ip">(</nts>
                  <ats e="T1696" id="Seg_4842" n="HIAT:non-pho" s="T1695">BRK</ats>
                  <nts id="Seg_4843" n="HIAT:ip">)</nts>
                  <nts id="Seg_4844" n="HIAT:ip">)</nts>
                  <nts id="Seg_4845" n="HIAT:ip">.</nts>
                  <nts id="Seg_4846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1706" id="Seg_4847" n="sc" s="T1697">
               <ts e="T1706" id="Seg_4849" n="HIAT:u" s="T1697">
                  <ts e="T1698" id="Seg_4851" n="HIAT:w" s="T1697">Măn</ts>
                  <nts id="Seg_4852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1699" id="Seg_4854" n="HIAT:w" s="T1698">kunolbiam</ts>
                  <nts id="Seg_4855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1700" id="Seg_4857" n="HIAT:w" s="T1699">taldʼen</ts>
                  <nts id="Seg_4858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1701" id="Seg_4860" n="HIAT:w" s="T1700">i</ts>
                  <nts id="Seg_4861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1702" id="Seg_4863" n="HIAT:w" s="T1701">dʼodənʼi</ts>
                  <nts id="Seg_4864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1703" id="Seg_4866" n="HIAT:w" s="T1702">kubiom</ts>
                  <nts id="Seg_4867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1704" id="Seg_4869" n="HIAT:w" s="T1703">bostə</ts>
                  <nts id="Seg_4870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1705" id="Seg_4872" n="HIAT:w" s="T1704">sʼestranə</ts>
                  <nts id="Seg_4873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1706" id="Seg_4875" n="HIAT:w" s="T1705">tibi</ts>
                  <nts id="Seg_4876" n="HIAT:ip">.</nts>
                  <nts id="Seg_4877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1712" id="Seg_4878" n="sc" s="T1707">
               <ts e="T1712" id="Seg_4880" n="HIAT:u" s="T1707">
                  <nts id="Seg_4881" n="HIAT:ip">(</nts>
                  <ts e="T1708" id="Seg_4883" n="HIAT:w" s="T1707">De-</ts>
                  <nts id="Seg_4884" n="HIAT:ip">)</nts>
                  <nts id="Seg_4885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1709" id="Seg_4887" n="HIAT:w" s="T1708">Dĭgəttə</ts>
                  <nts id="Seg_4888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1710" id="Seg_4890" n="HIAT:w" s="T1709">kambiam</ts>
                  <nts id="Seg_4891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1711" id="Seg_4893" n="HIAT:w" s="T1710">šiʔnileʔ</ts>
                  <nts id="Seg_4894" n="HIAT:ip">,</nts>
                  <nts id="Seg_4895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1712" id="Seg_4897" n="HIAT:w" s="T1711">dʼăbaktərbiam</ts>
                  <nts id="Seg_4898" n="HIAT:ip">.</nts>
                  <nts id="Seg_4899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1719" id="Seg_4900" n="sc" s="T1713">
               <ts e="T1719" id="Seg_4902" n="HIAT:u" s="T1713">
                  <nts id="Seg_4903" n="HIAT:ip">(</nts>
                  <ts e="T1714" id="Seg_4905" n="HIAT:w" s="T1713">Măl-</ts>
                  <nts id="Seg_4906" n="HIAT:ip">)</nts>
                  <nts id="Seg_4907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1715" id="Seg_4909" n="HIAT:w" s="T1714">Dĭgəttə</ts>
                  <nts id="Seg_4910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1716" id="Seg_4912" n="HIAT:w" s="T1715">mămbiam:</ts>
                  <nts id="Seg_4913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1717" id="Seg_4915" n="HIAT:w" s="T1716">šindidə</ts>
                  <nts id="Seg_4916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1718" id="Seg_4918" n="HIAT:w" s="T1717">dʼăbaktərləj</ts>
                  <nts id="Seg_4919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1719" id="Seg_4921" n="HIAT:w" s="T1718">mănzi</ts>
                  <nts id="Seg_4922" n="HIAT:ip">.</nts>
                  <nts id="Seg_4923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1728" id="Seg_4924" n="sc" s="T1720">
               <ts e="T1728" id="Seg_4926" n="HIAT:u" s="T1720">
                  <ts e="T1721" id="Seg_4928" n="HIAT:w" s="T1720">Dĭgəttə</ts>
                  <nts id="Seg_4929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4930" n="HIAT:ip">(</nts>
                  <ts e="T1722" id="Seg_4932" n="HIAT:w" s="T1721">koŋg-</ts>
                  <nts id="Seg_4933" n="HIAT:ip">)</nts>
                  <nts id="Seg_4934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1725" id="Seg_4936" n="HIAT:w" s="T1722">koŋ</ts>
                  <nts id="Seg_4937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1726" id="Seg_4939" n="HIAT:w" s="T1725">šobiʔi</ts>
                  <nts id="Seg_4940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1727" id="Seg_4942" n="HIAT:w" s="T1726">dʼăbaktərzittə</ts>
                  <nts id="Seg_4943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1728" id="Seg_4945" n="HIAT:w" s="T1727">mănzi</ts>
                  <nts id="Seg_4946" n="HIAT:ip">.</nts>
                  <nts id="Seg_4947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1731" id="Seg_4948" n="sc" s="T1729">
               <ts e="T1731" id="Seg_4950" n="HIAT:u" s="T1729">
                  <ts e="T1730" id="Seg_4952" n="HIAT:w" s="T1729">Dĭgəttə</ts>
                  <nts id="Seg_4953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1731" id="Seg_4955" n="HIAT:w" s="T1730">dĭn</ts>
                  <nts id="Seg_4956" n="HIAT:ip">…</nts>
                  <nts id="Seg_4957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1738" id="Seg_4958" n="sc" s="T1732">
               <ts e="T1738" id="Seg_4960" n="HIAT:u" s="T1732">
                  <ts e="T1733" id="Seg_4962" n="HIAT:w" s="T1732">Я</ts>
                  <nts id="Seg_4963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1734" id="Seg_4965" n="HIAT:w" s="T1733">видала</ts>
                  <nts id="Seg_4966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1735" id="Seg_4968" n="HIAT:w" s="T1734">во</ts>
                  <nts id="Seg_4969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1736" id="Seg_4971" n="HIAT:w" s="T1735">сне</ts>
                  <nts id="Seg_4972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1737" id="Seg_4974" n="HIAT:w" s="T1736">своего</ts>
                  <nts id="Seg_4975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1738" id="Seg_4977" n="HIAT:w" s="T1737">зятя</ts>
                  <nts id="Seg_4978" n="HIAT:ip">.</nts>
                  <nts id="Seg_4979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1742" id="Seg_4980" n="sc" s="T1739">
               <ts e="T1742" id="Seg_4982" n="HIAT:u" s="T1739">
                  <ts e="T1740" id="Seg_4984" n="HIAT:w" s="T1739">И</ts>
                  <nts id="Seg_4985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1741" id="Seg_4987" n="HIAT:w" s="T1740">пошла</ts>
                  <nts id="Seg_4988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1742" id="Seg_4990" n="HIAT:w" s="T1741">туды</ts>
                  <nts id="Seg_4991" n="HIAT:ip">.</nts>
                  <nts id="Seg_4992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1750" id="Seg_4993" n="sc" s="T1743">
               <ts e="T1750" id="Seg_4995" n="HIAT:u" s="T1743">
                  <ts e="T1744" id="Seg_4997" n="HIAT:w" s="T1743">Там</ts>
                  <nts id="Seg_4998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1745" id="Seg_5000" n="HIAT:w" s="T1744">приехали</ts>
                  <nts id="Seg_5001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1746" id="Seg_5003" n="HIAT:w" s="T1745">начальники</ts>
                  <nts id="Seg_5004" n="HIAT:ip">,</nts>
                  <nts id="Seg_5005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1747" id="Seg_5007" n="HIAT:w" s="T1746">и</ts>
                  <nts id="Seg_5008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1748" id="Seg_5010" n="HIAT:w" s="T1747">с</ts>
                  <nts id="Seg_5011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1749" id="Seg_5013" n="HIAT:w" s="T1748">имя</ts>
                  <nts id="Seg_5014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1750" id="Seg_5016" n="HIAT:w" s="T1749">разговаривала</ts>
                  <nts id="Seg_5017" n="HIAT:ip">.</nts>
                  <nts id="Seg_5018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1763" id="Seg_5019" n="sc" s="T1751">
               <ts e="T1763" id="Seg_5021" n="HIAT:u" s="T1751">
                  <ts e="T1752" id="Seg_5023" n="HIAT:w" s="T1751">И</ts>
                  <nts id="Seg_5024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1753" id="Seg_5026" n="HIAT:w" s="T1752">еще</ts>
                  <nts id="Seg_5027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1754" id="Seg_5029" n="HIAT:w" s="T1753">и</ts>
                  <nts id="Seg_5030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1755" id="Seg_5032" n="HIAT:w" s="T1754">дома</ts>
                  <nts id="Seg_5033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1756" id="Seg_5035" n="HIAT:w" s="T1755">сказала:</ts>
                  <nts id="Seg_5036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5037" n="HIAT:ip">"</nts>
                  <nts id="Seg_5038" n="HIAT:ip">(</nts>
                  <ts e="T1757" id="Seg_5040" n="HIAT:w" s="T1756">С</ts>
                  <nts id="Seg_5041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1758" id="Seg_5043" n="HIAT:w" s="T1757">к-</ts>
                  <nts id="Seg_5044" n="HIAT:ip">)</nts>
                  <nts id="Seg_5045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1759" id="Seg_5047" n="HIAT:w" s="T1758">С</ts>
                  <nts id="Seg_5048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1760" id="Seg_5050" n="HIAT:w" s="T1759">каким-то</ts>
                  <nts id="Seg_5051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1761" id="Seg_5053" n="HIAT:w" s="T1760">начальником</ts>
                  <nts id="Seg_5054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1762" id="Seg_5056" n="HIAT:w" s="T1761">буду</ts>
                  <nts id="Seg_5057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1763" id="Seg_5059" n="HIAT:w" s="T1762">разговаривать</ts>
                  <nts id="Seg_5060" n="HIAT:ip">"</nts>
                  <nts id="Seg_5061" n="HIAT:ip">.</nts>
                  <nts id="Seg_5062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1770" id="Seg_5063" n="sc" s="T1764">
               <ts e="T1770" id="Seg_5065" n="HIAT:u" s="T1764">
                  <ts e="T1765" id="Seg_5067" n="HIAT:w" s="T1764">Вот</ts>
                  <nts id="Seg_5068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1766" id="Seg_5070" n="HIAT:w" s="T1765">это</ts>
                  <nts id="Seg_5071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1767" id="Seg_5073" n="HIAT:w" s="T1766">я</ts>
                  <nts id="Seg_5074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1768" id="Seg_5076" n="HIAT:w" s="T1767">снила</ts>
                  <nts id="Seg_5077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1769" id="Seg_5079" n="HIAT:w" s="T1768">сон</ts>
                  <nts id="Seg_5080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1770" id="Seg_5082" n="HIAT:w" s="T1769">такой</ts>
                  <nts id="Seg_5083" n="HIAT:ip">.</nts>
                  <nts id="Seg_5084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1809" id="Seg_5085" n="sc" s="T1799">
               <ts e="T1801" id="Seg_5087" n="HIAT:u" s="T1799">
                  <ts e="T1800" id="Seg_5089" n="HIAT:w" s="T1799">Miʔ</ts>
                  <nts id="Seg_5090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1801" id="Seg_5092" n="HIAT:w" s="T1800">mĭmbibeʔ</ts>
                  <nts id="Seg_5093" n="HIAT:ip">.</nts>
                  <nts id="Seg_5094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1809" id="Seg_5096" n="HIAT:u" s="T1801">
                  <ts e="T1802" id="Seg_5098" n="HIAT:w" s="T1801">Dĭ</ts>
                  <nts id="Seg_5099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1803" id="Seg_5101" n="HIAT:w" s="T1802">nüke</ts>
                  <nts id="Seg_5102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1804" id="Seg_5104" n="HIAT:w" s="T1803">ibi</ts>
                  <nts id="Seg_5105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1805" id="Seg_5107" n="HIAT:w" s="T1804">dʼala</ts>
                  <nts id="Seg_5108" n="HIAT:ip">,</nts>
                  <nts id="Seg_5109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1806" id="Seg_5111" n="HIAT:w" s="T1805">kamen</ts>
                  <nts id="Seg_5112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1807" id="Seg_5114" n="HIAT:w" s="T1806">dĭ</ts>
                  <nts id="Seg_5115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1808" id="Seg_5117" n="HIAT:w" s="T1807">iat</ts>
                  <nts id="Seg_5118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1809" id="Seg_5120" n="HIAT:w" s="T1808">deʔpi</ts>
                  <nts id="Seg_5121" n="HIAT:ip">.</nts>
                  <nts id="Seg_5122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1816" id="Seg_5123" n="sc" s="T1810">
               <ts e="T1816" id="Seg_5125" n="HIAT:u" s="T1810">
                  <ts e="T1811" id="Seg_5127" n="HIAT:w" s="T1810">Măn</ts>
                  <nts id="Seg_5128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1812" id="Seg_5130" n="HIAT:w" s="T1811">mĭmbiem</ts>
                  <nts id="Seg_5131" n="HIAT:ip">,</nts>
                  <nts id="Seg_5132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1813" id="Seg_5134" n="HIAT:w" s="T1812">nagur</ts>
                  <nts id="Seg_5135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1814" id="Seg_5137" n="HIAT:w" s="T1813">aktʼa</ts>
                  <nts id="Seg_5138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1815" id="Seg_5140" n="HIAT:w" s="T1814">dĭʔnə</ts>
                  <nts id="Seg_5141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1816" id="Seg_5143" n="HIAT:w" s="T1815">mĭbiem</ts>
                  <nts id="Seg_5144" n="HIAT:ip">.</nts>
                  <nts id="Seg_5145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1823" id="Seg_5146" n="sc" s="T1817">
               <ts e="T1823" id="Seg_5148" n="HIAT:u" s="T1817">
                  <ts e="T1818" id="Seg_5150" n="HIAT:w" s="T1817">Dĭ</ts>
                  <nts id="Seg_5151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5152" n="HIAT:ip">(</nts>
                  <ts e="T1819" id="Seg_5154" n="HIAT:w" s="T1818">kădeš-</ts>
                  <nts id="Seg_5155" n="HIAT:ip">)</nts>
                  <nts id="Seg_5156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5157" n="HIAT:ip">(</nts>
                  <ts e="T1820" id="Seg_5159" n="HIAT:w" s="T1819">miʔnʼibeʔ</ts>
                  <nts id="Seg_5160" n="HIAT:ip">)</nts>
                  <nts id="Seg_5161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1821" id="Seg_5163" n="HIAT:w" s="T1820">bădəbi</ts>
                  <nts id="Seg_5164" n="HIAT:ip">,</nts>
                  <nts id="Seg_5165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1822" id="Seg_5167" n="HIAT:w" s="T1821">munujʔ</ts>
                  <nts id="Seg_5168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1823" id="Seg_5170" n="HIAT:w" s="T1822">embi</ts>
                  <nts id="Seg_5171" n="HIAT:ip">.</nts>
                  <nts id="Seg_5172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1827" id="Seg_5173" n="sc" s="T1824">
               <ts e="T1827" id="Seg_5175" n="HIAT:u" s="T1824">
                  <ts e="T1825" id="Seg_5177" n="HIAT:w" s="T1824">Kajaʔ</ts>
                  <nts id="Seg_5178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1826" id="Seg_5180" n="HIAT:w" s="T1825">embi</ts>
                  <nts id="Seg_5181" n="HIAT:ip">,</nts>
                  <nts id="Seg_5182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1827" id="Seg_5184" n="HIAT:w" s="T1826">köbərgen</ts>
                  <nts id="Seg_5185" n="HIAT:ip">.</nts>
                  <nts id="Seg_5186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1831" id="Seg_5187" n="sc" s="T1828">
               <ts e="T1831" id="Seg_5189" n="HIAT:u" s="T1828">
                  <ts e="T1829" id="Seg_5191" n="HIAT:w" s="T1828">Măndə:</ts>
                  <nts id="Seg_5192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5193" n="HIAT:ip">"</nts>
                  <ts e="T1830" id="Seg_5195" n="HIAT:w" s="T1829">Amaʔ</ts>
                  <nts id="Seg_5196" n="HIAT:ip">,</nts>
                  <nts id="Seg_5197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1831" id="Seg_5199" n="HIAT:w" s="T1830">amaʔ</ts>
                  <nts id="Seg_5200" n="HIAT:ip">"</nts>
                  <nts id="Seg_5201" n="HIAT:ip">.</nts>
                  <nts id="Seg_5202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1835" id="Seg_5203" n="sc" s="T1832">
               <ts e="T1835" id="Seg_5205" n="HIAT:u" s="T1832">
                  <ts e="T1833" id="Seg_5207" n="HIAT:w" s="T1832">Măn</ts>
                  <nts id="Seg_5208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1834" id="Seg_5210" n="HIAT:w" s="T1833">ambiam</ts>
                  <nts id="Seg_5211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1835" id="Seg_5213" n="HIAT:w" s="T1834">idʼiʔeʔe</ts>
                  <nts id="Seg_5214" n="HIAT:ip">.</nts>
                  <nts id="Seg_5215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1842" id="Seg_5216" n="sc" s="T1836">
               <ts e="T1842" id="Seg_5218" n="HIAT:u" s="T1836">
                  <ts e="T1837" id="Seg_5220" n="HIAT:w" s="T1836">Dĭgəttə</ts>
                  <nts id="Seg_5221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1838" id="Seg_5223" n="HIAT:w" s="T1837">onʼiʔ</ts>
                  <nts id="Seg_5224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1839" id="Seg_5226" n="HIAT:w" s="T1838">takše</ts>
                  <nts id="Seg_5227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1840" id="Seg_5229" n="HIAT:w" s="T1839">segi</ts>
                  <nts id="Seg_5230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1841" id="Seg_5232" n="HIAT:w" s="T1840">bü</ts>
                  <nts id="Seg_5233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1842" id="Seg_5235" n="HIAT:w" s="T1841">biʔpiem</ts>
                  <nts id="Seg_5236" n="HIAT:ip">.</nts>
                  <nts id="Seg_5237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1845" id="Seg_5238" n="sc" s="T1843">
               <ts e="T1845" id="Seg_5240" n="HIAT:u" s="T1843">
                  <ts e="T1844" id="Seg_5242" n="HIAT:w" s="T1843">Mămbiam:</ts>
                  <nts id="Seg_5243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5244" n="HIAT:ip">"</nts>
                  <ts e="T1845" id="Seg_5246" n="HIAT:w" s="T1844">Kabarləj</ts>
                  <nts id="Seg_5247" n="HIAT:ip">"</nts>
                  <nts id="Seg_5248" n="HIAT:ip">.</nts>
                  <nts id="Seg_5249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1860" id="Seg_5250" n="sc" s="T1850">
               <ts e="T1860" id="Seg_5252" n="HIAT:u" s="T1850">
                  <ts e="T1851" id="Seg_5254" n="HIAT:w" s="T1850">Тогда</ts>
                  <nts id="Seg_5255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1852" id="Seg_5257" n="HIAT:w" s="T1851">она</ts>
                  <nts id="Seg_5258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1853" id="Seg_5260" n="HIAT:w" s="T1852">сказала</ts>
                  <nts id="Seg_5261" n="HIAT:ip">,</nts>
                  <nts id="Seg_5262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1854" id="Seg_5264" n="HIAT:w" s="T1853">что</ts>
                  <nts id="Seg_5265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5266" n="HIAT:ip">"</nts>
                  <ts e="T1855" id="Seg_5268" n="HIAT:w" s="T1854">Пей</ts>
                  <nts id="Seg_5269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1856" id="Seg_5271" n="HIAT:w" s="T1855">чай</ts>
                  <nts id="Seg_5272" n="HIAT:ip">"</nts>
                  <nts id="Seg_5273" n="HIAT:ip">,</nts>
                  <nts id="Seg_5274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1857" id="Seg_5276" n="HIAT:w" s="T1856">а</ts>
                  <nts id="Seg_5277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1858" id="Seg_5279" n="HIAT:w" s="T1857">я</ts>
                  <nts id="Seg_5280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1859" id="Seg_5282" n="HIAT:w" s="T1858">говорю:</ts>
                  <nts id="Seg_5283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5284" n="HIAT:ip">"</nts>
                  <ts e="T1860" id="Seg_5286" n="HIAT:w" s="T1859">Хватит</ts>
                  <nts id="Seg_5287" n="HIAT:ip">"</nts>
                  <nts id="Seg_5288" n="HIAT:ip">.</nts>
                  <nts id="Seg_5289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1864" id="Seg_5290" n="sc" s="T1861">
               <ts e="T1864" id="Seg_5292" n="HIAT:u" s="T1861">
                  <ts e="T1862" id="Seg_5294" n="HIAT:w" s="T1861">Собрала</ts>
                  <nts id="Seg_5295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1863" id="Seg_5297" n="HIAT:w" s="T1862">на</ts>
                  <nts id="Seg_5298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5299" n="HIAT:ip">(</nts>
                  <ts e="T1864" id="Seg_5301" n="HIAT:w" s="T1863">стол</ts>
                  <nts id="Seg_5302" n="HIAT:ip">)</nts>
                  <nts id="Seg_5303" n="HIAT:ip">.</nts>
                  <nts id="Seg_5304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1866" id="Seg_5305" n="sc" s="T1865">
               <ts e="T1866" id="Seg_5307" n="HIAT:u" s="T1865">
                  <ts e="T1866" id="Seg_5309" n="HIAT:w" s="T1865">Господи</ts>
                  <nts id="Seg_5310" n="HIAT:ip">!</nts>
                  <nts id="Seg_5311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1873" id="Seg_5312" n="sc" s="T1867">
               <ts e="T1873" id="Seg_5314" n="HIAT:u" s="T1867">
                  <ts e="T1868" id="Seg_5316" n="HIAT:w" s="T1867">Пришли</ts>
                  <nts id="Seg_5317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1869" id="Seg_5319" n="HIAT:w" s="T1868">к</ts>
                  <nts id="Seg_5320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1870" id="Seg_5322" n="HIAT:w" s="T1869">ей</ts>
                  <nts id="Seg_5323" n="HIAT:ip">,</nts>
                  <nts id="Seg_5324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1871" id="Seg_5326" n="HIAT:w" s="T1870">она</ts>
                  <nts id="Seg_5327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1872" id="Seg_5329" n="HIAT:w" s="T1871">была</ts>
                  <nts id="Seg_5330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1873" id="Seg_5332" n="HIAT:w" s="T1872">именинница</ts>
                  <nts id="Seg_5333" n="HIAT:ip">.</nts>
                  <nts id="Seg_5334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1877" id="Seg_5335" n="sc" s="T1874">
               <ts e="T1877" id="Seg_5337" n="HIAT:u" s="T1874">
                  <ts e="T1875" id="Seg_5339" n="HIAT:w" s="T1874">Принесли</ts>
                  <nts id="Seg_5340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1876" id="Seg_5342" n="HIAT:w" s="T1875">ей</ts>
                  <nts id="Seg_5343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1877" id="Seg_5345" n="HIAT:w" s="T1876">подарки</ts>
                  <nts id="Seg_5346" n="HIAT:ip">.</nts>
                  <nts id="Seg_5347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T6" id="Seg_5348" n="sc" s="T1">
               <ts e="T2" id="Seg_5350" n="e" s="T1">Кое-чего </ts>
               <ts e="T3" id="Seg_5352" n="e" s="T2">позабывала </ts>
               <ts e="T6" id="Seg_5354" n="e" s="T3">я. </ts>
            </ts>
            <ts e="T22" id="Seg_5355" n="sc" s="T16">
               <ts e="T17" id="Seg_5357" n="e" s="T16">У </ts>
               <ts e="T18" id="Seg_5359" n="e" s="T17">моей </ts>
               <ts e="T19" id="Seg_5361" n="e" s="T18">племянницы </ts>
               <ts e="T20" id="Seg_5363" n="e" s="T19">дочка </ts>
               <ts e="T21" id="Seg_5365" n="e" s="T20">вышла </ts>
               <ts e="T22" id="Seg_5367" n="e" s="T21">замуж. </ts>
            </ts>
            <ts e="T31" id="Seg_5368" n="sc" s="T23">
               <ts e="T24" id="Seg_5370" n="e" s="T23">И </ts>
               <ts e="T25" id="Seg_5372" n="e" s="T24">поехали </ts>
               <ts e="T26" id="Seg_5374" n="e" s="T25">туды </ts>
               <ts e="T27" id="Seg_5376" n="e" s="T26">по </ts>
               <ts e="T28" id="Seg_5378" n="e" s="T27">Мане </ts>
               <ts e="T29" id="Seg_5380" n="e" s="T28">строить </ts>
               <ts e="T30" id="Seg_5382" n="e" s="T29">железную </ts>
               <ts e="T31" id="Seg_5384" n="e" s="T30">дорогу. </ts>
            </ts>
            <ts e="T40" id="Seg_5385" n="sc" s="T32">
               <ts e="T33" id="Seg_5387" n="e" s="T32">И </ts>
               <ts e="T34" id="Seg_5389" n="e" s="T33">оттедова </ts>
               <ts e="T35" id="Seg_5391" n="e" s="T34">(уех-) </ts>
               <ts e="T36" id="Seg_5393" n="e" s="T35">всё </ts>
               <ts e="T37" id="Seg_5395" n="e" s="T36">строили </ts>
               <ts e="T38" id="Seg_5397" n="e" s="T37">дотель </ts>
               <ts e="T39" id="Seg_5399" n="e" s="T38">до </ts>
               <ts e="T40" id="Seg_5401" n="e" s="T39">севера. </ts>
            </ts>
            <ts e="T48" id="Seg_5402" n="sc" s="T41">
               <ts e="T42" id="Seg_5404" n="e" s="T41">И </ts>
               <ts e="T43" id="Seg_5406" n="e" s="T42">потом </ts>
               <ts e="T44" id="Seg_5408" n="e" s="T43">она </ts>
               <ts e="T45" id="Seg_5410" n="e" s="T44">приехала </ts>
               <ts e="T46" id="Seg_5412" n="e" s="T45">к </ts>
               <ts e="T47" id="Seg_5414" n="e" s="T46">матери, </ts>
               <ts e="T48" id="Seg_5416" n="e" s="T47">гостить. </ts>
            </ts>
            <ts e="T53" id="Seg_5417" n="sc" s="T49">
               <ts e="T51" id="Seg_5419" n="e" s="T49">((BRK)) </ts>
               <ts e="T52" id="Seg_5421" n="e" s="T51">(-ла) его </ts>
               <ts e="T53" id="Seg_5423" n="e" s="T52">похоронила. </ts>
            </ts>
            <ts e="T58" id="Seg_5424" n="sc" s="T54">
               <ts e="T55" id="Seg_5426" n="e" s="T54">(И </ts>
               <ts e="T56" id="Seg_5428" n="e" s="T55">тог-) </ts>
               <ts e="T57" id="Seg_5430" n="e" s="T56">Ну </ts>
               <ts e="T58" id="Seg_5432" n="e" s="T57">это… </ts>
            </ts>
            <ts e="T65" id="Seg_5433" n="sc" s="T59">
               <ts e="T60" id="Seg_5435" n="e" s="T59">Я </ts>
               <ts e="T61" id="Seg_5437" n="e" s="T60">по-русски </ts>
               <ts e="T62" id="Seg_5439" n="e" s="T61">(ска-) </ts>
               <ts e="T63" id="Seg_5441" n="e" s="T62">сказать, </ts>
               <ts e="T64" id="Seg_5443" n="e" s="T63">опять </ts>
               <ts e="T65" id="Seg_5445" n="e" s="T64">забуду. </ts>
            </ts>
            <ts e="T69" id="Seg_5446" n="sc" s="T68">
               <ts e="T69" id="Seg_5448" n="e" s="T68">((BRK)). </ts>
            </ts>
            <ts e="T82" id="Seg_5449" n="sc" s="T77">
               <ts e="T78" id="Seg_5451" n="e" s="T77">У </ts>
               <ts e="T79" id="Seg_5453" n="e" s="T78">вас </ts>
               <ts e="T80" id="Seg_5455" n="e" s="T79">какой-то </ts>
               <ts e="T81" id="Seg_5457" n="e" s="T80">ветер </ts>
               <ts e="T82" id="Seg_5459" n="e" s="T81">((NOISE)). </ts>
            </ts>
            <ts e="T92" id="Seg_5460" n="sc" s="T84">
               <ts e="T86" id="Seg_5462" n="e" s="T84">У </ts>
               <ts e="T88" id="Seg_5464" n="e" s="T86">нас </ts>
               <ts e="T90" id="Seg_5466" n="e" s="T88">сухой </ts>
               <ts e="T92" id="Seg_5468" n="e" s="T90">ветер. </ts>
            </ts>
            <ts e="T118" id="Seg_5469" n="sc" s="T96">
               <ts e="T99" id="Seg_5471" n="e" s="T96">Да, </ts>
               <ts e="T101" id="Seg_5473" n="e" s="T99">(ши-) </ts>
               <ts e="T103" id="Seg_5475" n="e" s="T101">шибко </ts>
               <ts e="T104" id="Seg_5477" n="e" s="T103">у </ts>
               <ts e="T105" id="Seg_5479" n="e" s="T104">вас </ts>
               <ts e="T106" id="Seg_5481" n="e" s="T105">ветер, </ts>
               <ts e="T107" id="Seg_5483" n="e" s="T106">у </ts>
               <ts e="T108" id="Seg_5485" n="e" s="T107">нас </ts>
               <ts e="T110" id="Seg_5487" n="e" s="T108">такого </ts>
               <ts e="T112" id="Seg_5489" n="e" s="T110">нету, </ts>
               <ts e="T114" id="Seg_5491" n="e" s="T112">как </ts>
               <ts e="T116" id="Seg_5493" n="e" s="T114">у </ts>
               <ts e="T118" id="Seg_5495" n="e" s="T116">(вас). </ts>
            </ts>
            <ts e="T133" id="Seg_5496" n="sc" s="T120">
               <ts e="T122" id="Seg_5498" n="e" s="T120">Видала, </ts>
               <ts e="T125" id="Seg_5500" n="e" s="T122">только </ts>
               <ts e="T127" id="Seg_5502" n="e" s="T125">(с-) </ts>
               <ts e="T129" id="Seg_5504" n="e" s="T127">далеко, </ts>
               <ts e="T131" id="Seg_5506" n="e" s="T129">с </ts>
               <ts e="T133" id="Seg_5508" n="e" s="T131">далека. </ts>
            </ts>
            <ts e="T137" id="Seg_5509" n="sc" s="T134">
               <ts e="T136" id="Seg_5511" n="e" s="T134">Там </ts>
               <ts e="T137" id="Seg_5513" n="e" s="T136">была. </ts>
            </ts>
            <ts e="T141" id="Seg_5514" n="sc" s="T138">
               <ts e="T139" id="Seg_5516" n="e" s="T138">Где </ts>
               <ts e="T140" id="Seg_5518" n="e" s="T139">Маргарита </ts>
               <ts e="T141" id="Seg_5520" n="e" s="T140">толстая. </ts>
            </ts>
            <ts e="T160" id="Seg_5521" n="sc" s="T144">
               <ts e="T146" id="Seg_5523" n="e" s="T144">За </ts>
               <ts e="T147" id="Seg_5525" n="e" s="T146">её </ts>
               <ts e="T148" id="Seg_5527" n="e" s="T147">проходила </ts>
               <ts e="T149" id="Seg_5529" n="e" s="T148">туды. </ts>
               <ts e="T150" id="Seg_5531" n="e" s="T149">Ну, </ts>
               <ts e="T152" id="Seg_5533" n="e" s="T150">видать </ts>
               <ts e="T153" id="Seg_5535" n="e" s="T152">так </ts>
               <ts e="T154" id="Seg_5537" n="e" s="T153">далеко, </ts>
               <ts e="T156" id="Seg_5539" n="e" s="T154">но </ts>
               <ts e="T157" id="Seg_5541" n="e" s="T156">близко </ts>
               <ts e="T158" id="Seg_5543" n="e" s="T157">не </ts>
               <ts e="T159" id="Seg_5545" n="e" s="T158">ходили, </ts>
               <ts e="T160" id="Seg_5547" n="e" s="T159">далеко. </ts>
            </ts>
            <ts e="T181" id="Seg_5548" n="sc" s="T173">
               <ts e="T174" id="Seg_5550" n="e" s="T173">((DMG)), </ts>
               <ts e="T176" id="Seg_5552" n="e" s="T174">а </ts>
               <ts e="T177" id="Seg_5554" n="e" s="T176">вышло </ts>
               <ts e="T178" id="Seg_5556" n="e" s="T177">ли, </ts>
               <ts e="T179" id="Seg_5558" n="e" s="T178">нет, </ts>
               <ts e="T180" id="Seg_5560" n="e" s="T179">не </ts>
               <ts e="T181" id="Seg_5562" n="e" s="T180">знаю. </ts>
            </ts>
            <ts e="T184" id="Seg_5563" n="sc" s="T182">
               <ts e="T183" id="Seg_5565" n="e" s="T182">По-русски </ts>
               <ts e="T184" id="Seg_5567" n="e" s="T183">говорить? </ts>
            </ts>
            <ts e="T194" id="Seg_5568" n="sc" s="T187">
               <ts e="T188" id="Seg_5570" n="e" s="T187">У </ts>
               <ts e="T189" id="Seg_5572" n="e" s="T188">(м-) </ts>
               <ts e="T190" id="Seg_5574" n="e" s="T189">моёй </ts>
               <ts e="T191" id="Seg_5576" n="e" s="T190">племянницы </ts>
               <ts e="T192" id="Seg_5578" n="e" s="T191">дочка </ts>
               <ts e="T193" id="Seg_5580" n="e" s="T192">вышла </ts>
               <ts e="T194" id="Seg_5582" n="e" s="T193">замуж. </ts>
            </ts>
            <ts e="T207" id="Seg_5583" n="sc" s="T195">
               <ts e="T196" id="Seg_5585" n="e" s="T195">(У </ts>
               <ts e="T197" id="Seg_5587" n="e" s="T196">н-) </ts>
               <ts e="T198" id="Seg_5589" n="e" s="T197">(А </ts>
               <ts e="T199" id="Seg_5591" n="e" s="T198">ты, </ts>
               <ts e="T200" id="Seg_5593" n="e" s="T199">говорит </ts>
               <ts e="T201" id="Seg_5595" n="e" s="T200">= </ts>
               <ts e="T202" id="Seg_5597" n="e" s="T201">А </ts>
               <ts e="T203" id="Seg_5599" n="e" s="T202">я </ts>
               <ts e="T204" id="Seg_5601" n="e" s="T203">говорю </ts>
               <ts e="T205" id="Seg_5603" n="e" s="T204">а </ts>
               <ts e="T206" id="Seg_5605" n="e" s="T205">вы </ts>
               <ts e="T207" id="Seg_5607" n="e" s="T206">как). </ts>
            </ts>
            <ts e="T224" id="Seg_5608" n="sc" s="T208">
               <ts e="T209" id="Seg_5610" n="e" s="T208">Я, </ts>
               <ts e="T210" id="Seg_5612" n="e" s="T209">говорит, </ts>
               <ts e="T211" id="Seg_5614" n="e" s="T210">учительница </ts>
               <ts e="T212" id="Seg_5616" n="e" s="T211">Агафон </ts>
               <ts e="T213" id="Seg_5618" n="e" s="T212">Ивановича, </ts>
               <ts e="T214" id="Seg_5620" n="e" s="T213">((DMG)) </ts>
               <ts e="T215" id="Seg_5622" n="e" s="T214">он </ts>
               <ts e="T217" id="Seg_5624" n="e" s="T215">мне </ts>
               <ts e="T218" id="Seg_5626" n="e" s="T217">рассказывает, </ts>
               <ts e="T220" id="Seg_5628" n="e" s="T218">ну </ts>
               <ts e="T222" id="Seg_5630" n="e" s="T220">я </ts>
               <ts e="T224" id="Seg_5632" n="e" s="T222">по-русски. </ts>
            </ts>
            <ts e="T227" id="Seg_5633" n="sc" s="T225">
               <ts e="T227" id="Seg_5635" n="e" s="T225">Ага. </ts>
            </ts>
            <ts e="T231" id="Seg_5636" n="sc" s="T228">
               <ts e="T229" id="Seg_5638" n="e" s="T228">Ну </ts>
               <ts e="T230" id="Seg_5640" n="e" s="T229">я </ts>
               <ts e="T231" id="Seg_5642" n="e" s="T230">((…)). </ts>
            </ts>
            <ts e="T238" id="Seg_5643" n="sc" s="T232">
               <ts e="T233" id="Seg_5645" n="e" s="T232">Тут </ts>
               <ts e="T234" id="Seg_5647" n="e" s="T233">посидели </ts>
               <ts e="T235" id="Seg_5649" n="e" s="T234">там, </ts>
               <ts e="T236" id="Seg_5651" n="e" s="T235">разговаривали </ts>
               <ts e="T238" id="Seg_5653" n="e" s="T236">((DMG)). </ts>
            </ts>
            <ts e="T243" id="Seg_5654" n="sc" s="T240">
               <ts e="T242" id="Seg_5656" n="e" s="T240">Ну. </ts>
               <ts e="T243" id="Seg_5658" n="e" s="T242">Там. </ts>
            </ts>
            <ts e="T249" id="Seg_5659" n="sc" s="T244">
               <ts e="T245" id="Seg_5661" n="e" s="T244">Мой </ts>
               <ts e="T246" id="Seg_5663" n="e" s="T245">брат </ts>
               <ts e="T247" id="Seg_5665" n="e" s="T246">сродный </ts>
               <ts e="T248" id="Seg_5667" n="e" s="T247">туды </ts>
               <ts e="T249" id="Seg_5669" n="e" s="T248">ездил. </ts>
            </ts>
            <ts e="T251" id="Seg_5670" n="sc" s="T250">
               <ts e="T251" id="Seg_5672" n="e" s="T250">((BRK)). </ts>
            </ts>
            <ts e="T254" id="Seg_5673" n="sc" s="T252">
               <ts e="T253" id="Seg_5675" n="e" s="T252">Ну, </ts>
               <ts e="T254" id="Seg_5677" n="e" s="T253">это… </ts>
            </ts>
            <ts e="T263" id="Seg_5678" n="sc" s="T255">
               <ts e="T256" id="Seg_5680" n="e" s="T255">Мальчишка </ts>
               <ts e="T258" id="Seg_5682" n="e" s="T256">((…)). </ts>
               <ts e="T259" id="Seg_5684" n="e" s="T258">(никак) </ts>
               <ts e="T260" id="Seg_5686" n="e" s="T259">не </ts>
               <ts e="T261" id="Seg_5688" n="e" s="T260">выговорю. </ts>
               <ts e="T262" id="Seg_5690" n="e" s="T261">(Iže-) </ts>
               <ts e="T263" id="Seg_5692" n="e" s="T262">Izeŋ. </ts>
            </ts>
            <ts e="T268" id="Seg_5693" n="sc" s="T264">
               <ts e="T265" id="Seg_5695" n="e" s="T264">(Iž-) </ts>
               <ts e="T266" id="Seg_5697" n="e" s="T265">Izeŋ. </ts>
               <ts e="T267" id="Seg_5699" n="e" s="T266">Мать </ts>
               <ts e="T268" id="Seg_5701" n="e" s="T267">где? </ts>
            </ts>
            <ts e="T274" id="Seg_5702" n="sc" s="T269">
               <ts e="T270" id="Seg_5704" n="e" s="T269">Sargol </ts>
               <ts e="T271" id="Seg_5706" n="e" s="T270">(tura-) </ts>
               <ts e="T272" id="Seg_5708" n="e" s="T271">Sargol </ts>
               <ts e="T273" id="Seg_5710" n="e" s="T272">derʼevnʼanə </ts>
               <ts e="T274" id="Seg_5712" n="e" s="T273">barustan. </ts>
            </ts>
            <ts e="T300" id="Seg_5713" n="sc" s="T275">
               <ts e="T276" id="Seg_5715" n="e" s="T275">В </ts>
               <ts e="T277" id="Seg_5717" n="e" s="T276">Сарголову </ts>
               <ts e="T278" id="Seg_5719" n="e" s="T277">деревню </ts>
               <ts e="T280" id="Seg_5721" n="e" s="T278">ушла. </ts>
               <ts e="T282" id="Seg_5723" n="e" s="T280">Я </ts>
               <ts e="T284" id="Seg_5725" n="e" s="T282">тот </ts>
               <ts e="T286" id="Seg_5727" n="e" s="T284">язык, </ts>
               <ts e="T288" id="Seg_5729" n="e" s="T286">вот </ts>
               <ts e="T291" id="Seg_5731" n="e" s="T288">так </ts>
               <ts e="T293" id="Seg_5733" n="e" s="T291">говорили, </ts>
               <ts e="T294" id="Seg_5735" n="e" s="T293">а </ts>
               <ts e="T296" id="Seg_5737" n="e" s="T294">плохо </ts>
               <ts e="T297" id="Seg_5739" n="e" s="T296">знаю </ts>
               <ts e="T299" id="Seg_5741" n="e" s="T297">я </ts>
               <ts e="T300" id="Seg_5743" n="e" s="T299">его. </ts>
            </ts>
            <ts e="T314" id="Seg_5744" n="sc" s="T306">
               <ts e="T307" id="Seg_5746" n="e" s="T306">И </ts>
               <ts e="T311" id="Seg_5748" n="e" s="T307">на </ts>
               <ts e="T314" id="Seg_5750" n="e" s="T311">его… </ts>
            </ts>
            <ts e="T336" id="Seg_5751" n="sc" s="T322">
               <ts e="T324" id="Seg_5753" n="e" s="T322">Ну </ts>
               <ts e="T327" id="Seg_5755" n="e" s="T324">вот, </ts>
               <ts e="T329" id="Seg_5757" n="e" s="T327">и </ts>
               <ts e="T332" id="Seg_5759" n="e" s="T329">уфимские </ts>
               <ts e="T335" id="Seg_5761" n="e" s="T332">есть </ts>
               <ts e="T336" id="Seg_5763" n="e" s="T335">татары. </ts>
            </ts>
            <ts e="T346" id="Seg_5764" n="sc" s="T337">
               <ts e="T338" id="Seg_5766" n="e" s="T337">Казанские, </ts>
               <ts e="T339" id="Seg_5768" n="e" s="T338">они </ts>
               <ts e="T340" id="Seg_5770" n="e" s="T339">на </ts>
               <ts e="T341" id="Seg_5772" n="e" s="T340">этот </ts>
               <ts e="T342" id="Seg_5774" n="e" s="T341">язык. </ts>
               <ts e="T343" id="Seg_5776" n="e" s="T342">Вот, </ts>
               <ts e="T344" id="Seg_5778" n="e" s="T343">один </ts>
               <ts e="T345" id="Seg_5780" n="e" s="T344">раз </ts>
               <ts e="T346" id="Seg_5782" n="e" s="T345">приехали. </ts>
            </ts>
            <ts e="T352" id="Seg_5783" n="sc" s="T347">
               <ts e="T348" id="Seg_5785" n="e" s="T347">Трое </ts>
               <ts e="T349" id="Seg_5787" n="e" s="T348">приехали. </ts>
               <ts e="T350" id="Seg_5789" n="e" s="T349">Кто </ts>
               <ts e="T351" id="Seg_5791" n="e" s="T350">там </ts>
               <ts e="T352" id="Seg_5793" n="e" s="T351">приехал? </ts>
            </ts>
            <ts e="T358" id="Seg_5794" n="sc" s="T353">
               <ts e="T354" id="Seg_5796" n="e" s="T353">Говорит: </ts>
               <ts e="T355" id="Seg_5798" n="e" s="T354">бер </ts>
               <ts e="T356" id="Seg_5800" n="e" s="T355">жид, </ts>
               <ts e="T357" id="Seg_5802" n="e" s="T356">бер </ts>
               <ts e="T358" id="Seg_5804" n="e" s="T357">татарин. </ts>
            </ts>
            <ts e="T368" id="Seg_5805" n="sc" s="T359">
               <ts e="T360" id="Seg_5807" n="e" s="T359">Один </ts>
               <ts e="T361" id="Seg_5809" n="e" s="T360">жид, </ts>
               <ts e="T362" id="Seg_5811" n="e" s="T361">один </ts>
               <ts e="T363" id="Seg_5813" n="e" s="T362">татарин. </ts>
               <ts e="T364" id="Seg_5815" n="e" s="T363">((…)) </ts>
               <ts e="T365" id="Seg_5817" n="e" s="T364">на </ts>
               <ts e="T367" id="Seg_5819" n="e" s="T365">тот </ts>
               <ts e="T368" id="Seg_5821" n="e" s="T367">язык. </ts>
            </ts>
            <ts e="T385" id="Seg_5822" n="sc" s="T370">
               <ts e="T372" id="Seg_5824" n="e" s="T370">На </ts>
               <ts e="T373" id="Seg_5826" n="e" s="T372">тот </ts>
               <ts e="T375" id="Seg_5828" n="e" s="T373">язык. </ts>
               <ts e="T376" id="Seg_5830" n="e" s="T375">А </ts>
               <ts e="T377" id="Seg_5832" n="e" s="T376">по </ts>
               <ts e="T378" id="Seg_5834" n="e" s="T377">нашему: </ts>
               <ts e="T379" id="Seg_5836" n="e" s="T378">Onʼiʔ </ts>
               <ts e="T380" id="Seg_5838" n="e" s="T379">žid </ts>
               <ts e="T381" id="Seg_5840" n="e" s="T380">šobi, </ts>
               <ts e="T382" id="Seg_5842" n="e" s="T381">onʼiʔ </ts>
               <ts e="T383" id="Seg_5844" n="e" s="T382">nu </ts>
               <ts e="T384" id="Seg_5846" n="e" s="T383">kuza </ts>
               <ts e="T385" id="Seg_5848" n="e" s="T384">šobi. </ts>
            </ts>
            <ts e="T391" id="Seg_5849" n="sc" s="T386">
               <ts e="T387" id="Seg_5851" n="e" s="T386">Один </ts>
               <ts e="T388" id="Seg_5853" n="e" s="T387">жид, </ts>
               <ts e="T390" id="Seg_5855" n="e" s="T388">один </ts>
               <ts e="T391" id="Seg_5857" n="e" s="T390">татарин. </ts>
            </ts>
            <ts e="T394" id="Seg_5858" n="sc" s="T393">
               <ts e="T394" id="Seg_5860" n="e" s="T393">Nuzaŋ. </ts>
            </ts>
            <ts e="T400" id="Seg_5861" n="sc" s="T395">
               <ts e="T396" id="Seg_5863" n="e" s="T395">Dĭgəttə </ts>
               <ts e="T397" id="Seg_5865" n="e" s="T396">plʼemʼannʼica </ts>
               <ts e="T398" id="Seg_5867" n="e" s="T397">măna </ts>
               <ts e="T399" id="Seg_5869" n="e" s="T398">noʔ </ts>
               <ts e="T400" id="Seg_5871" n="e" s="T399">kambi. </ts>
            </ts>
            <ts e="T405" id="Seg_5872" n="sc" s="T401">
               <ts e="T402" id="Seg_5874" n="e" s="T401">Miʔ </ts>
               <ts e="T403" id="Seg_5876" n="e" s="T402">dĭ </ts>
               <ts e="T404" id="Seg_5878" n="e" s="T403">kopnaʔi </ts>
               <ts e="T405" id="Seg_5880" n="e" s="T404">embibeʔ. </ts>
            </ts>
            <ts e="T410" id="Seg_5881" n="sc" s="T406">
               <ts e="T407" id="Seg_5883" n="e" s="T406">Dĭgəttə </ts>
               <ts e="T408" id="Seg_5885" n="e" s="T407">onʼiʔ </ts>
               <ts e="T409" id="Seg_5887" n="e" s="T408">(u-) </ts>
               <ts e="T410" id="Seg_5889" n="e" s="T409">üzəbi. </ts>
            </ts>
            <ts e="T416" id="Seg_5890" n="sc" s="T411">
               <ts e="T412" id="Seg_5892" n="e" s="T411">Măn </ts>
               <ts e="T413" id="Seg_5894" n="e" s="T412">mămbiam… </ts>
               <ts e="T414" id="Seg_5896" n="e" s="T413">Mĭʔ </ts>
               <ts e="T415" id="Seg_5898" n="e" s="T414">nagurbəʔ </ts>
               <ts e="T416" id="Seg_5900" n="e" s="T415">ibibeʔ. </ts>
            </ts>
            <ts e="T424" id="Seg_5901" n="sc" s="T417">
               <ts e="T418" id="Seg_5903" n="e" s="T417">Măn </ts>
               <ts e="T420" id="Seg_5905" n="e" s="T418">mămbiam: </ts>
               <ts e="T421" id="Seg_5907" n="e" s="T420">"No </ts>
               <ts e="T422" id="Seg_5909" n="e" s="T421">kut, </ts>
               <ts e="T423" id="Seg_5911" n="e" s="T422">kodu </ts>
               <ts e="T424" id="Seg_5913" n="e" s="T423">saʔməlupi". </ts>
            </ts>
            <ts e="T430" id="Seg_5914" n="sc" s="T425">
               <ts e="T426" id="Seg_5916" n="e" s="T425">Măn </ts>
               <ts e="T427" id="Seg_5918" n="e" s="T426">mămbiam: </ts>
               <ts e="T428" id="Seg_5920" n="e" s="T427">"Šində-nʼibudʼ </ts>
               <ts e="T429" id="Seg_5922" n="e" s="T428">(kuna-) </ts>
               <ts e="T430" id="Seg_5924" n="e" s="T429">külalləj". </ts>
            </ts>
            <ts e="T437" id="Seg_5925" n="sc" s="T431">
               <ts e="T432" id="Seg_5927" n="e" s="T431">Dĭgəttə </ts>
               <ts e="T433" id="Seg_5929" n="e" s="T432">măn </ts>
               <ts e="T434" id="Seg_5931" n="e" s="T433">mălliam: </ts>
               <ts e="T435" id="Seg_5933" n="e" s="T434">"Dĭ </ts>
               <ts e="T436" id="Seg_5935" n="e" s="T435">măn </ts>
               <ts e="T437" id="Seg_5937" n="e" s="T436">külalləm. </ts>
            </ts>
            <ts e="T441" id="Seg_5938" n="sc" s="T438">
               <ts e="T439" id="Seg_5940" n="e" s="T438">Măn </ts>
               <ts e="T440" id="Seg_5942" n="e" s="T439">dʼaktə </ts>
               <ts e="T441" id="Seg_5944" n="e" s="T440">vedʼ. </ts>
            </ts>
            <ts e="T447" id="Seg_5945" n="sc" s="T442">
               <ts e="T443" id="Seg_5947" n="e" s="T442">A </ts>
               <ts e="T444" id="Seg_5949" n="e" s="T443">šiʔ </ts>
               <ts e="T445" id="Seg_5951" n="e" s="T444">išo </ts>
               <ts e="T446" id="Seg_5953" n="e" s="T445">ej </ts>
               <ts e="T447" id="Seg_5955" n="e" s="T446">dʼaktə. </ts>
            </ts>
            <ts e="T452" id="Seg_5956" n="sc" s="T448">
               <ts e="T449" id="Seg_5958" n="e" s="T448">(Dĭg-) </ts>
               <ts e="T450" id="Seg_5960" n="e" s="T449">Dĭgəttə </ts>
               <ts e="T451" id="Seg_5962" n="e" s="T450">maʔnʼibaʔ </ts>
               <ts e="T452" id="Seg_5964" n="e" s="T451">šobibaʔ. </ts>
            </ts>
            <ts e="T463" id="Seg_5965" n="sc" s="T453">
               <ts e="T454" id="Seg_5967" n="e" s="T453">(Măn=) </ts>
               <ts e="T455" id="Seg_5969" n="e" s="T454">Kamen </ts>
               <ts e="T456" id="Seg_5971" n="e" s="T455">(dĭ=) </ts>
               <ts e="T457" id="Seg_5973" n="e" s="T456">dĭ </ts>
               <ts e="T458" id="Seg_5975" n="e" s="T457">šobi </ts>
               <ts e="T459" id="Seg_5977" n="e" s="T458">döbər, </ts>
               <ts e="T460" id="Seg_5979" n="e" s="T459">măn </ts>
               <ts e="T461" id="Seg_5981" n="e" s="T460">mĭmbiem, </ts>
               <ts e="T462" id="Seg_5983" n="e" s="T461">köbürgən </ts>
               <ts e="T463" id="Seg_5985" n="e" s="T462">nĭŋgəbiem. </ts>
            </ts>
            <ts e="T471" id="Seg_5986" n="sc" s="T464">
               <ts e="T465" id="Seg_5988" n="e" s="T464">(Š-) </ts>
               <ts e="T466" id="Seg_5990" n="e" s="T465">Šomi </ts>
               <ts e="T467" id="Seg_5992" n="e" s="T466">pa, </ts>
               <ts e="T468" id="Seg_5994" n="e" s="T467">beržə </ts>
               <ts e="T469" id="Seg_5996" n="e" s="T468">nagobi, </ts>
               <ts e="T470" id="Seg_5998" n="e" s="T469">dʼünə </ts>
               <ts e="T471" id="Seg_6000" n="e" s="T470">saʔməluʔpi. </ts>
            </ts>
            <ts e="T487" id="Seg_6001" n="sc" s="T472">
               <ts e="T473" id="Seg_6003" n="e" s="T472">(Măn=) </ts>
               <ts e="T474" id="Seg_6005" n="e" s="T473">Măn </ts>
               <ts e="T475" id="Seg_6007" n="e" s="T474">(u) </ts>
               <ts e="T476" id="Seg_6009" n="e" s="T475">šindinədə </ts>
               <ts e="T477" id="Seg_6011" n="e" s="T476">ej </ts>
               <ts e="T478" id="Seg_6013" n="e" s="T477">mămbiam, </ts>
               <ts e="T479" id="Seg_6015" n="e" s="T478">a </ts>
               <ts e="T480" id="Seg_6017" n="e" s="T479">dĭgəttə </ts>
               <ts e="T481" id="Seg_6019" n="e" s="T480">dĭn </ts>
               <ts e="T482" id="Seg_6021" n="e" s="T481">tibit </ts>
               <ts e="T483" id="Seg_6023" n="e" s="T482">kamen </ts>
               <ts e="T484" id="Seg_6025" n="e" s="T483">bügən </ts>
               <ts e="T485" id="Seg_6027" n="e" s="T484">külambi, </ts>
               <ts e="T486" id="Seg_6029" n="e" s="T485">măn </ts>
               <ts e="T487" id="Seg_6031" n="e" s="T486">mămbiam. </ts>
            </ts>
            <ts e="T493" id="Seg_6032" n="sc" s="T488">
               <ts e="T489" id="Seg_6034" n="e" s="T488">Когда </ts>
               <ts e="T490" id="Seg_6036" n="e" s="T489">еёный </ts>
               <ts e="T491" id="Seg_6038" n="e" s="T490">муж </ts>
               <ts e="T492" id="Seg_6040" n="e" s="T491">утонул, </ts>
               <ts e="T493" id="Seg_6042" n="e" s="T492">я… </ts>
            </ts>
            <ts e="T506" id="Seg_6043" n="sc" s="T494">
               <ts e="T495" id="Seg_6045" n="e" s="T494">Она </ts>
               <ts e="T496" id="Seg_6047" n="e" s="T495">у </ts>
               <ts e="T497" id="Seg_6049" n="e" s="T496">меня </ts>
               <ts e="T498" id="Seg_6051" n="e" s="T497">работала </ts>
               <ts e="T499" id="Seg_6053" n="e" s="T498">на </ts>
               <ts e="T500" id="Seg_6055" n="e" s="T499">покосе, </ts>
               <ts e="T501" id="Seg_6057" n="e" s="T500">копну </ts>
               <ts e="T502" id="Seg_6059" n="e" s="T501">поклала </ts>
               <ts e="T503" id="Seg_6061" n="e" s="T502">хорошую, </ts>
               <ts e="T504" id="Seg_6063" n="e" s="T503">это </ts>
               <ts e="T505" id="Seg_6065" n="e" s="T504">я </ts>
               <ts e="T506" id="Seg_6067" n="e" s="T505">рассказывала. </ts>
            </ts>
            <ts e="T511" id="Seg_6068" n="sc" s="T507">
               <ts e="T508" id="Seg_6070" n="e" s="T507">Она </ts>
               <ts e="T509" id="Seg_6072" n="e" s="T508">стояла </ts>
               <ts e="T510" id="Seg_6074" n="e" s="T509">и </ts>
               <ts e="T511" id="Seg_6076" n="e" s="T510">упала. </ts>
            </ts>
            <ts e="T540" id="Seg_6077" n="sc" s="T512">
               <ts e="T513" id="Seg_6079" n="e" s="T512">А </ts>
               <ts e="T514" id="Seg_6081" n="e" s="T513">нас </ts>
               <ts e="T515" id="Seg_6083" n="e" s="T514">было </ts>
               <ts e="T516" id="Seg_6085" n="e" s="T515">трое, </ts>
               <ts e="T517" id="Seg_6087" n="e" s="T516">(а </ts>
               <ts e="T518" id="Seg_6089" n="e" s="T517">я </ts>
               <ts e="T519" id="Seg_6091" n="e" s="T518">говорю=) </ts>
               <ts e="T520" id="Seg_6093" n="e" s="T519">а </ts>
               <ts e="T521" id="Seg_6095" n="e" s="T520">я </ts>
               <ts e="T522" id="Seg_6097" n="e" s="T521">(ска-) </ts>
               <ts e="T523" id="Seg_6099" n="e" s="T522">сказала: </ts>
               <ts e="T524" id="Seg_6101" n="e" s="T523">"Это </ts>
               <ts e="T525" id="Seg_6103" n="e" s="T524">кто-то </ts>
               <ts e="T526" id="Seg_6105" n="e" s="T525">из </ts>
               <ts e="T527" id="Seg_6107" n="e" s="T526">нас </ts>
               <ts e="T528" id="Seg_6109" n="e" s="T527">помрет". </ts>
               <ts e="T529" id="Seg_6111" n="e" s="T528">А </ts>
               <ts e="T530" id="Seg_6113" n="e" s="T529">тады </ts>
               <ts e="T531" id="Seg_6115" n="e" s="T530">думаю, </ts>
               <ts e="T532" id="Seg_6117" n="e" s="T531">говорю </ts>
               <ts e="T534" id="Seg_6119" n="e" s="T532">имя: </ts>
               <ts e="T535" id="Seg_6121" n="e" s="T534">"Это </ts>
               <ts e="T536" id="Seg_6123" n="e" s="T535">я </ts>
               <ts e="T537" id="Seg_6125" n="e" s="T536">помру, </ts>
               <ts e="T538" id="Seg_6127" n="e" s="T537">я </ts>
               <ts e="T539" id="Seg_6129" n="e" s="T538">старше </ts>
               <ts e="T540" id="Seg_6131" n="e" s="T539">вас". </ts>
            </ts>
            <ts e="T550" id="Seg_6132" n="sc" s="T541">
               <ts e="T542" id="Seg_6134" n="e" s="T541">А </ts>
               <ts e="T543" id="Seg_6136" n="e" s="T542">тагды </ts>
               <ts e="T544" id="Seg_6138" n="e" s="T543">ходила </ts>
               <ts e="T545" id="Seg_6140" n="e" s="T544">я </ts>
               <ts e="T546" id="Seg_6142" n="e" s="T545">на </ts>
               <ts e="T547" id="Seg_6144" n="e" s="T546">поле, </ts>
               <ts e="T548" id="Seg_6146" n="e" s="T547">лук </ts>
               <ts e="T549" id="Seg_6148" n="e" s="T548">рвала, </ts>
               <ts e="T550" id="Seg_6150" n="e" s="T549">полевой. </ts>
            </ts>
            <ts e="T558" id="Seg_6151" n="sc" s="T551">
               <ts e="T552" id="Seg_6153" n="e" s="T551">И </ts>
               <ts e="T553" id="Seg_6155" n="e" s="T552">ветру </ts>
               <ts e="T554" id="Seg_6157" n="e" s="T553">не </ts>
               <ts e="T555" id="Seg_6159" n="e" s="T554">было, </ts>
               <ts e="T556" id="Seg_6161" n="e" s="T555">листвяга </ts>
               <ts e="T557" id="Seg_6163" n="e" s="T556">упала, </ts>
               <ts e="T558" id="Seg_6165" n="e" s="T557">толстая! </ts>
            </ts>
            <ts e="T569" id="Seg_6166" n="sc" s="T559">
               <ts e="T560" id="Seg_6168" n="e" s="T559">Я </ts>
               <ts e="T561" id="Seg_6170" n="e" s="T560">так </ts>
               <ts e="T562" id="Seg_6172" n="e" s="T561">подумала: </ts>
               <ts e="T563" id="Seg_6174" n="e" s="T562">кто-то </ts>
               <ts e="T564" id="Seg_6176" n="e" s="T563">помрет, </ts>
               <ts e="T565" id="Seg_6178" n="e" s="T564">и </ts>
               <ts e="T566" id="Seg_6180" n="e" s="T565">правда </ts>
               <ts e="T567" id="Seg_6182" n="e" s="T566">помер, </ts>
               <ts e="T568" id="Seg_6184" n="e" s="T567">вот </ts>
               <ts e="T569" id="Seg_6186" n="e" s="T568">этот. </ts>
            </ts>
            <ts e="T571" id="Seg_6187" n="sc" s="T570">
               <ts e="T571" id="Seg_6189" n="e" s="T570">((BRK)). </ts>
            </ts>
            <ts e="T582" id="Seg_6190" n="sc" s="T257">
               <ts e="T573" id="Seg_6192" n="e" s="T257">Kamen </ts>
               <ts e="T574" id="Seg_6194" n="e" s="T573">măn </ts>
               <ts e="T575" id="Seg_6196" n="e" s="T574">abam </ts>
               <ts e="T576" id="Seg_6198" n="e" s="T575">šobi, </ts>
               <ts e="T577" id="Seg_6200" n="e" s="T576">(dĭn </ts>
               <ts e="T578" id="Seg_6202" n="e" s="T577">turagən) </ts>
               <ts e="T579" id="Seg_6204" n="e" s="T578">dĭ </ts>
               <ts e="T580" id="Seg_6206" n="e" s="T579">turandə, </ts>
               <ts e="T581" id="Seg_6208" n="e" s="T580">Spasskaːnə, </ts>
               <ts e="T582" id="Seg_6210" n="e" s="T581">maːʔnʼi. </ts>
            </ts>
            <ts e="T588" id="Seg_6211" n="sc" s="T583">
               <ts e="T584" id="Seg_6213" n="e" s="T583">Dĭgəttə </ts>
               <ts e="T585" id="Seg_6215" n="e" s="T584">šolaʔbə, </ts>
               <ts e="T586" id="Seg_6217" n="e" s="T585">šomi </ts>
               <ts e="T587" id="Seg_6219" n="e" s="T586">pa </ts>
               <ts e="T588" id="Seg_6221" n="e" s="T587">nulaʔbə. </ts>
            </ts>
            <ts e="T595" id="Seg_6222" n="sc" s="T589">
               <ts e="T590" id="Seg_6224" n="e" s="T589">Dĭgəttə </ts>
               <ts e="T591" id="Seg_6226" n="e" s="T590">pagə </ts>
               <ts e="T592" id="Seg_6228" n="e" s="T591">сучок </ts>
               <ts e="T593" id="Seg_6230" n="e" s="T592">(üz-) </ts>
               <ts e="T594" id="Seg_6232" n="e" s="T593">dʼünə </ts>
               <ts e="T595" id="Seg_6234" n="e" s="T594">saʔməluʔpi. </ts>
            </ts>
            <ts e="T603" id="Seg_6235" n="sc" s="T596">
               <ts e="T597" id="Seg_6237" n="e" s="T596">Dĭgəttə </ts>
               <ts e="T598" id="Seg_6239" n="e" s="T597">(šo-) </ts>
               <ts e="T599" id="Seg_6241" n="e" s="T598">dĭ </ts>
               <ts e="T600" id="Seg_6243" n="e" s="T599">šobi </ts>
               <ts e="T601" id="Seg_6245" n="e" s="T600">maːʔndə, </ts>
               <ts e="T602" id="Seg_6247" n="e" s="T601">măna </ts>
               <ts e="T603" id="Seg_6249" n="e" s="T602">nörbəlie. </ts>
            </ts>
            <ts e="T610" id="Seg_6250" n="sc" s="T604">
               <ts e="T605" id="Seg_6252" n="e" s="T604">Măn </ts>
               <ts e="T606" id="Seg_6254" n="e" s="T605">(măn-) </ts>
               <ts e="T607" id="Seg_6256" n="e" s="T606">mămbiam: </ts>
               <ts e="T608" id="Seg_6258" n="e" s="T607">šindidə </ts>
               <ts e="T609" id="Seg_6260" n="e" s="T608">esseŋdə </ts>
               <ts e="T610" id="Seg_6262" n="e" s="T609">külalləj. </ts>
            </ts>
            <ts e="T617" id="Seg_6263" n="sc" s="T611">
               <ts e="T612" id="Seg_6265" n="e" s="T611">I </ts>
               <ts e="T613" id="Seg_6267" n="e" s="T612">dĭgəttə </ts>
               <ts e="T614" id="Seg_6269" n="e" s="T613">dăre </ts>
               <ts e="T615" id="Seg_6271" n="e" s="T614">dĭ </ts>
               <ts e="T616" id="Seg_6273" n="e" s="T615">i </ts>
               <ts e="T617" id="Seg_6275" n="e" s="T616">ibi. </ts>
            </ts>
            <ts e="T626" id="Seg_6276" n="sc" s="T618">
               <ts e="T619" id="Seg_6278" n="e" s="T618">(Măn=) </ts>
               <ts e="T620" id="Seg_6280" n="e" s="T619">Măn </ts>
               <ts e="T621" id="Seg_6282" n="e" s="T620">nʼim </ts>
               <ts e="T622" id="Seg_6284" n="e" s="T621">kambi </ts>
               <ts e="T623" id="Seg_6286" n="e" s="T622">bügən </ts>
               <ts e="T624" id="Seg_6288" n="e" s="T623">i </ts>
               <ts e="T625" id="Seg_6290" n="e" s="T624">dĭn </ts>
               <ts e="T626" id="Seg_6292" n="e" s="T625">külambi. </ts>
            </ts>
            <ts e="T671" id="Seg_6293" n="sc" s="T665">
               <ts e="T666" id="Seg_6295" n="e" s="T665">Я </ts>
               <ts e="T667" id="Seg_6297" n="e" s="T666">забыла </ts>
               <ts e="T668" id="Seg_6299" n="e" s="T667">вчера, </ts>
               <ts e="T669" id="Seg_6301" n="e" s="T668">про </ts>
               <ts e="T670" id="Seg_6303" n="e" s="T669">это </ts>
               <ts e="T671" id="Seg_6305" n="e" s="T670">говорила. </ts>
            </ts>
            <ts e="T683" id="Seg_6306" n="sc" s="T672">
               <ts e="T673" id="Seg_6308" n="e" s="T672">Про </ts>
               <ts e="T674" id="Seg_6310" n="e" s="T673">себя, </ts>
               <ts e="T675" id="Seg_6312" n="e" s="T674">я </ts>
               <ts e="T676" id="Seg_6314" n="e" s="T675">замуж </ts>
               <ts e="T677" id="Seg_6316" n="e" s="T676">шла, </ts>
               <ts e="T678" id="Seg_6318" n="e" s="T677">забыла, </ts>
               <ts e="T679" id="Seg_6320" n="e" s="T678">говорила </ts>
               <ts e="T681" id="Seg_6322" n="e" s="T679">ли, </ts>
               <ts e="T683" id="Seg_6324" n="e" s="T681">нет. </ts>
            </ts>
            <ts e="T723" id="Seg_6325" n="sc" s="T719">
               <ts e="T720" id="Seg_6327" n="e" s="T719">Как </ts>
               <ts e="T721" id="Seg_6329" n="e" s="T720">мой </ts>
               <ts e="T722" id="Seg_6331" n="e" s="T721">муж </ts>
               <ts e="T723" id="Seg_6333" n="e" s="T722">утонул. </ts>
            </ts>
            <ts e="T729" id="Seg_6334" n="sc" s="T724">
               <ts e="T725" id="Seg_6336" n="e" s="T724">Ну, </ts>
               <ts e="T726" id="Seg_6338" n="e" s="T725">это </ts>
               <ts e="T727" id="Seg_6340" n="e" s="T726">он </ts>
               <ts e="T728" id="Seg_6342" n="e" s="T727">говорит, </ts>
               <ts e="T729" id="Seg_6344" n="e" s="T728">нет? </ts>
            </ts>
            <ts e="T756" id="Seg_6345" n="sc" s="T743">
               <ts e="T744" id="Seg_6347" n="e" s="T743">Ну, </ts>
               <ts e="T745" id="Seg_6349" n="e" s="T744">когда </ts>
               <ts e="T746" id="Seg_6351" n="e" s="T745">я </ts>
               <ts e="T747" id="Seg_6353" n="e" s="T746">была </ts>
               <ts e="T748" id="Seg_6355" n="e" s="T747">девушкой, </ts>
               <ts e="T749" id="Seg_6357" n="e" s="T748">Пьянково </ts>
               <ts e="T750" id="Seg_6359" n="e" s="T749">деревня </ts>
               <ts e="T751" id="Seg_6361" n="e" s="T750">была, </ts>
               <ts e="T752" id="Seg_6363" n="e" s="T751">там </ts>
               <ts e="T753" id="Seg_6365" n="e" s="T752">парень, </ts>
               <ts e="T754" id="Seg_6367" n="e" s="T753">звали </ts>
               <ts e="T755" id="Seg_6369" n="e" s="T754">его </ts>
               <ts e="T756" id="Seg_6371" n="e" s="T755">Алексеем. </ts>
            </ts>
            <ts e="T769" id="Seg_6372" n="sc" s="T757">
               <ts e="T758" id="Seg_6374" n="e" s="T757">Мы </ts>
               <ts e="T759" id="Seg_6376" n="e" s="T758">с </ts>
               <ts e="T760" id="Seg_6378" n="e" s="T759">им </ts>
               <ts e="T761" id="Seg_6380" n="e" s="T760">дружили, </ts>
               <ts e="T762" id="Seg_6382" n="e" s="T761">(я=) </ts>
               <ts e="T763" id="Seg_6384" n="e" s="T762">я </ts>
               <ts e="T764" id="Seg_6386" n="e" s="T763">когда </ts>
               <ts e="T765" id="Seg_6388" n="e" s="T764">туды </ts>
               <ts e="T766" id="Seg_6390" n="e" s="T765">ездила, </ts>
               <ts e="T767" id="Seg_6392" n="e" s="T766">а </ts>
               <ts e="T768" id="Seg_6394" n="e" s="T767">он </ts>
               <ts e="T769" id="Seg_6396" n="e" s="T768">сюды. </ts>
            </ts>
            <ts e="T786" id="Seg_6397" n="sc" s="T770">
               <ts e="T771" id="Seg_6399" n="e" s="T770">А </ts>
               <ts e="T772" id="Seg_6401" n="e" s="T771">потом </ts>
               <ts e="T773" id="Seg_6403" n="e" s="T772">у </ts>
               <ts e="T774" id="Seg_6405" n="e" s="T773">его </ts>
               <ts e="T775" id="Seg_6407" n="e" s="T774">(мать=) </ts>
               <ts e="T776" id="Seg_6409" n="e" s="T775">отец </ts>
               <ts e="T777" id="Seg_6411" n="e" s="T776">помер, </ts>
               <ts e="T778" id="Seg_6413" n="e" s="T777">а </ts>
               <ts e="T779" id="Seg_6415" n="e" s="T778">мать </ts>
               <ts e="T780" id="Seg_6417" n="e" s="T779">взяла </ts>
               <ts e="T781" id="Seg_6419" n="e" s="T780">замуж </ts>
               <ts e="T782" id="Seg_6421" n="e" s="T781">ушла, </ts>
               <ts e="T783" id="Seg_6423" n="e" s="T782">а </ts>
               <ts e="T784" id="Seg_6425" n="e" s="T783">он </ts>
               <ts e="T785" id="Seg_6427" n="e" s="T784">остался </ts>
               <ts e="T786" id="Seg_6429" n="e" s="T785">один. </ts>
            </ts>
            <ts e="T791" id="Seg_6430" n="sc" s="T787">
               <ts e="T788" id="Seg_6432" n="e" s="T787">Ему </ts>
               <ts e="T789" id="Seg_6434" n="e" s="T788">семнадцать </ts>
               <ts e="T790" id="Seg_6436" n="e" s="T789">лет </ts>
               <ts e="T791" id="Seg_6438" n="e" s="T790">было. </ts>
            </ts>
            <ts e="T797" id="Seg_6439" n="sc" s="T792">
               <ts e="T793" id="Seg_6441" n="e" s="T792">И </ts>
               <ts e="T794" id="Seg_6443" n="e" s="T793">мне </ts>
               <ts e="T795" id="Seg_6445" n="e" s="T794">семнадцать, </ts>
               <ts e="T796" id="Seg_6447" n="e" s="T795">молода </ts>
               <ts e="T797" id="Seg_6449" n="e" s="T796">была! </ts>
            </ts>
            <ts e="T809" id="Seg_6450" n="sc" s="T798">
               <ts e="T799" id="Seg_6452" n="e" s="T798">Ну, </ts>
               <ts e="T800" id="Seg_6454" n="e" s="T799">и </ts>
               <ts e="T801" id="Seg_6456" n="e" s="T800">он </ts>
               <ts e="T802" id="Seg_6458" n="e" s="T801">приехал. </ts>
               <ts e="T803" id="Seg_6460" n="e" s="T802">(Я=) </ts>
               <ts e="T804" id="Seg_6462" n="e" s="T803">Я </ts>
               <ts e="T805" id="Seg_6464" n="e" s="T804">ему </ts>
               <ts e="T806" id="Seg_6466" n="e" s="T805">говорю: </ts>
               <ts e="T807" id="Seg_6468" n="e" s="T806">"Сватай, </ts>
               <ts e="T808" id="Seg_6470" n="e" s="T807">отдадут </ts>
               <ts e="T809" id="Seg_6472" n="e" s="T808">родители". </ts>
            </ts>
            <ts e="T819" id="Seg_6473" n="sc" s="T810">
               <ts e="T811" id="Seg_6475" n="e" s="T810">Он </ts>
               <ts e="T812" id="Seg_6477" n="e" s="T811">говорит: </ts>
               <ts e="T813" id="Seg_6479" n="e" s="T812">"Нет, </ts>
               <ts e="T814" id="Seg_6481" n="e" s="T813">не </ts>
               <ts e="T815" id="Seg_6483" n="e" s="T814">отдадут, </ts>
               <ts e="T816" id="Seg_6485" n="e" s="T815">скажут: </ts>
               <ts e="T817" id="Seg_6487" n="e" s="T816">ты </ts>
               <ts e="T818" id="Seg_6489" n="e" s="T817">молодая, </ts>
               <ts e="T819" id="Seg_6491" n="e" s="T818">поедем!" </ts>
            </ts>
            <ts e="T831" id="Seg_6492" n="sc" s="T820">
               <ts e="T821" id="Seg_6494" n="e" s="T820">И </ts>
               <ts e="T822" id="Seg_6496" n="e" s="T821">я </ts>
               <ts e="T823" id="Seg_6498" n="e" s="T822">собрала </ts>
               <ts e="T824" id="Seg_6500" n="e" s="T823">платье, </ts>
               <ts e="T825" id="Seg_6502" n="e" s="T824">связала </ts>
               <ts e="T826" id="Seg_6504" n="e" s="T825">узел, </ts>
               <ts e="T827" id="Seg_6506" n="e" s="T826">ночью </ts>
               <ts e="T828" id="Seg_6508" n="e" s="T827">оседлали </ts>
               <ts e="T829" id="Seg_6510" n="e" s="T828">коня </ts>
               <ts e="T830" id="Seg_6512" n="e" s="T829">и </ts>
               <ts e="T831" id="Seg_6514" n="e" s="T830">поехали. </ts>
            </ts>
            <ts e="T841" id="Seg_6515" n="sc" s="T832">
               <ts e="T833" id="Seg_6517" n="e" s="T832">Не </ts>
               <ts e="T834" id="Seg_6519" n="e" s="T833">поехали </ts>
               <ts e="T835" id="Seg_6521" n="e" s="T834">тут </ts>
               <ts e="T836" id="Seg_6523" n="e" s="T835">прямой </ts>
               <ts e="T837" id="Seg_6525" n="e" s="T836">дорогой, </ts>
               <ts e="T838" id="Seg_6527" n="e" s="T837">а </ts>
               <ts e="T839" id="Seg_6529" n="e" s="T838">поехали </ts>
               <ts e="T840" id="Seg_6531" n="e" s="T839">через </ts>
               <ts e="T841" id="Seg_6533" n="e" s="T840">Агинско. </ts>
            </ts>
            <ts e="T845" id="Seg_6534" n="sc" s="T842">
               <ts e="T843" id="Seg_6536" n="e" s="T842">Ночью </ts>
               <ts e="T844" id="Seg_6538" n="e" s="T843">ехали-ехали, </ts>
               <ts e="T845" id="Seg_6540" n="e" s="T844">ой. </ts>
            </ts>
            <ts e="T857" id="Seg_6541" n="sc" s="T846">
               <ts e="T847" id="Seg_6543" n="e" s="T846">Потом </ts>
               <ts e="T848" id="Seg_6545" n="e" s="T847">приехали, </ts>
               <ts e="T849" id="Seg_6547" n="e" s="T848">там </ts>
               <ts e="T850" id="Seg_6549" n="e" s="T849">деревня </ts>
               <ts e="T851" id="Seg_6551" n="e" s="T850">(Ка-) </ts>
               <ts e="T852" id="Seg_6553" n="e" s="T851">Каптырлык, </ts>
               <ts e="T853" id="Seg_6555" n="e" s="T852">русский </ts>
               <ts e="T854" id="Seg_6557" n="e" s="T853">народ, </ts>
               <ts e="T855" id="Seg_6559" n="e" s="T854">там </ts>
               <ts e="T856" id="Seg_6561" n="e" s="T855">ночевали, </ts>
               <ts e="T857" id="Seg_6563" n="e" s="T856">оттель… </ts>
            </ts>
            <ts e="T868" id="Seg_6564" n="sc" s="T858">
               <ts e="T859" id="Seg_6566" n="e" s="T858">И </ts>
               <ts e="T860" id="Seg_6568" n="e" s="T859">там </ts>
               <ts e="T861" id="Seg_6570" n="e" s="T860">считай </ts>
               <ts e="T862" id="Seg_6572" n="e" s="T861">целый </ts>
               <ts e="T863" id="Seg_6574" n="e" s="T862">день </ts>
               <ts e="T864" id="Seg_6576" n="e" s="T863">пробыли, </ts>
               <ts e="T865" id="Seg_6578" n="e" s="T864">он </ts>
               <ts e="T866" id="Seg_6580" n="e" s="T865">не </ts>
               <ts e="T867" id="Seg_6582" n="e" s="T866">ехал </ts>
               <ts e="T868" id="Seg_6584" n="e" s="T867">домой. </ts>
            </ts>
            <ts e="T878" id="Seg_6585" n="sc" s="T869">
               <ts e="T870" id="Seg_6587" n="e" s="T869">Или </ts>
               <ts e="T871" id="Seg_6589" n="e" s="T870">совестился, </ts>
               <ts e="T872" id="Seg_6591" n="e" s="T871">(или) </ts>
               <ts e="T873" id="Seg_6593" n="e" s="T872">чего, </ts>
               <ts e="T874" id="Seg_6595" n="e" s="T873">не </ts>
               <ts e="T875" id="Seg_6597" n="e" s="T874">знаю. </ts>
               <ts e="T876" id="Seg_6599" n="e" s="T875">Потом </ts>
               <ts e="T877" id="Seg_6601" n="e" s="T876">приехали </ts>
               <ts e="T878" id="Seg_6603" n="e" s="T877">вечером. </ts>
            </ts>
            <ts e="T895" id="Seg_6604" n="sc" s="T879">
               <ts e="T880" id="Seg_6606" n="e" s="T879">И </ts>
               <ts e="T881" id="Seg_6608" n="e" s="T880">он </ts>
               <ts e="T882" id="Seg_6610" n="e" s="T881">(рас-) </ts>
               <ts e="T883" id="Seg_6612" n="e" s="T882">расседлал </ts>
               <ts e="T884" id="Seg_6614" n="e" s="T883">коня, </ts>
               <ts e="T885" id="Seg_6616" n="e" s="T884">зашли </ts>
               <ts e="T886" id="Seg_6618" n="e" s="T885">в </ts>
               <ts e="T887" id="Seg_6620" n="e" s="T886">дом </ts>
               <ts e="T888" id="Seg_6622" n="e" s="T887">и </ts>
               <ts e="T889" id="Seg_6624" n="e" s="T888">тут </ts>
               <ts e="T890" id="Seg_6626" n="e" s="T889">спали, </ts>
               <ts e="T891" id="Seg_6628" n="e" s="T890">и </ts>
               <ts e="T892" id="Seg_6630" n="e" s="T891">там </ts>
               <ts e="T893" id="Seg_6632" n="e" s="T892">я </ts>
               <ts e="T894" id="Seg_6634" n="e" s="T893">жила, </ts>
               <ts e="T895" id="Seg_6636" n="e" s="T894">всё. </ts>
            </ts>
            <ts e="T902" id="Seg_6637" n="sc" s="T896">
               <ts e="T897" id="Seg_6639" n="e" s="T896">А </ts>
               <ts e="T898" id="Seg_6641" n="e" s="T897">тагды </ts>
               <ts e="T899" id="Seg_6643" n="e" s="T898">приехал </ts>
               <ts e="T900" id="Seg_6645" n="e" s="T899">(е-) </ts>
               <ts e="T901" id="Seg_6647" n="e" s="T900">евонный </ts>
               <ts e="T902" id="Seg_6649" n="e" s="T901">сродственник. </ts>
            </ts>
            <ts e="T912" id="Seg_6650" n="sc" s="T903">
               <ts e="T904" id="Seg_6652" n="e" s="T903">(Зва-) </ts>
               <ts e="T905" id="Seg_6654" n="e" s="T904">(О-) </ts>
               <ts e="T906" id="Seg_6656" n="e" s="T905">Он </ts>
               <ts e="T907" id="Seg_6658" n="e" s="T906">глаза </ts>
               <ts e="T908" id="Seg_6660" n="e" s="T907">потерял, </ts>
               <ts e="T909" id="Seg_6662" n="e" s="T908">не </ts>
               <ts e="T910" id="Seg_6664" n="e" s="T909">видел, </ts>
               <ts e="T911" id="Seg_6666" n="e" s="T910">дядя </ts>
               <ts e="T912" id="Seg_6668" n="e" s="T911">его. </ts>
            </ts>
            <ts e="T918" id="Seg_6669" n="sc" s="T913">
               <ts e="T914" id="Seg_6671" n="e" s="T913">Сказал: </ts>
               <ts e="T915" id="Seg_6673" n="e" s="T914">приезжай </ts>
               <ts e="T916" id="Seg_6675" n="e" s="T915">помоги </ts>
               <ts e="T917" id="Seg_6677" n="e" s="T916">сено </ts>
               <ts e="T918" id="Seg_6679" n="e" s="T917">косить. </ts>
            </ts>
            <ts e="T926" id="Seg_6680" n="sc" s="T919">
               <ts e="T920" id="Seg_6682" n="e" s="T919">Ему </ts>
               <ts e="T921" id="Seg_6684" n="e" s="T920">туды </ts>
               <ts e="T922" id="Seg_6686" n="e" s="T921">в </ts>
               <ts e="T923" id="Seg_6688" n="e" s="T922">нашу </ts>
               <ts e="T924" id="Seg_6690" n="e" s="T923">деревню, </ts>
               <ts e="T925" id="Seg_6692" n="e" s="T924">в </ts>
               <ts e="T926" id="Seg_6694" n="e" s="T925">Абалаков. </ts>
            </ts>
            <ts e="T955" id="Seg_6695" n="sc" s="T927">
               <ts e="T928" id="Seg_6697" n="e" s="T927">(Тогда </ts>
               <ts e="T929" id="Seg_6699" n="e" s="T928">он=) </ts>
               <ts e="T930" id="Seg_6701" n="e" s="T929">Тогда </ts>
               <ts e="T931" id="Seg_6703" n="e" s="T930">он </ts>
               <ts e="T932" id="Seg_6705" n="e" s="T931">привез </ts>
               <ts e="T933" id="Seg_6707" n="e" s="T932">девушку </ts>
               <ts e="T934" id="Seg_6709" n="e" s="T933">там, </ts>
               <ts e="T935" id="Seg_6711" n="e" s="T934">с </ts>
               <ts e="T936" id="Seg_6713" n="e" s="T935">другой </ts>
               <ts e="T937" id="Seg_6715" n="e" s="T936">деревни. </ts>
               <ts e="T938" id="Seg_6717" n="e" s="T937">Ну, </ts>
               <ts e="T939" id="Seg_6719" n="e" s="T938">мне </ts>
               <ts e="T940" id="Seg_6721" n="e" s="T939">сказали, </ts>
               <ts e="T941" id="Seg_6723" n="e" s="T940">что </ts>
               <ts e="T942" id="Seg_6725" n="e" s="T941">он </ts>
               <ts e="T943" id="Seg_6727" n="e" s="T942">хочет </ts>
               <ts e="T944" id="Seg_6729" n="e" s="T943">ее </ts>
               <ts e="T945" id="Seg_6731" n="e" s="T944">брать, </ts>
               <ts e="T946" id="Seg_6733" n="e" s="T945">а </ts>
               <ts e="T947" id="Seg_6735" n="e" s="T946">тебя </ts>
               <ts e="T948" id="Seg_6737" n="e" s="T947">прогнать. </ts>
               <ts e="T949" id="Seg_6739" n="e" s="T948">Я </ts>
               <ts e="T950" id="Seg_6741" n="e" s="T949">собралась </ts>
               <ts e="T951" id="Seg_6743" n="e" s="T950">да </ts>
               <ts e="T952" id="Seg_6745" n="e" s="T951">с </ts>
               <ts e="T953" id="Seg_6747" n="e" s="T952">евонным </ts>
               <ts e="T954" id="Seg_6749" n="e" s="T953">дядем </ts>
               <ts e="T955" id="Seg_6751" n="e" s="T954">поехала. </ts>
            </ts>
            <ts e="T961" id="Seg_6752" n="sc" s="T956">
               <ts e="T957" id="Seg_6754" n="e" s="T956">Он </ts>
               <ts e="T958" id="Seg_6756" n="e" s="T957">меня </ts>
               <ts e="T959" id="Seg_6758" n="e" s="T958">догнал </ts>
               <ts e="T960" id="Seg_6760" n="e" s="T959">и </ts>
               <ts e="T961" id="Seg_6762" n="e" s="T960">воротил. </ts>
            </ts>
            <ts e="T964" id="Seg_6763" n="sc" s="T962">
               <ts e="T963" id="Seg_6765" n="e" s="T962">Назад </ts>
               <ts e="T964" id="Seg_6767" n="e" s="T963">обратно. </ts>
            </ts>
            <ts e="T973" id="Seg_6768" n="sc" s="T965">
               <ts e="T966" id="Seg_6770" n="e" s="T965">Ну </ts>
               <ts e="T967" id="Seg_6772" n="e" s="T966">а </ts>
               <ts e="T968" id="Seg_6774" n="e" s="T967">потом </ts>
               <ts e="T969" id="Seg_6776" n="e" s="T968">после </ts>
               <ts e="T970" id="Seg_6778" n="e" s="T969">этого </ts>
               <ts e="T971" id="Seg_6780" n="e" s="T970">мама </ts>
               <ts e="T972" id="Seg_6782" n="e" s="T971">туды </ts>
               <ts e="T973" id="Seg_6784" n="e" s="T972">приехала. </ts>
            </ts>
            <ts e="T980" id="Seg_6785" n="sc" s="T974">
               <ts e="T975" id="Seg_6787" n="e" s="T974">А </ts>
               <ts e="T976" id="Seg_6789" n="e" s="T975">я </ts>
               <ts e="T977" id="Seg_6791" n="e" s="T976">тады </ts>
               <ts e="T978" id="Seg_6793" n="e" s="T977">пошла </ts>
               <ts e="T979" id="Seg_6795" n="e" s="T978">к </ts>
               <ts e="T980" id="Seg_6797" n="e" s="T979">маме. </ts>
            </ts>
            <ts e="T989" id="Seg_6798" n="sc" s="T981">
               <ts e="T982" id="Seg_6800" n="e" s="T981">А </ts>
               <ts e="T983" id="Seg_6802" n="e" s="T982">мама </ts>
               <ts e="T984" id="Seg_6804" n="e" s="T983">заехала </ts>
               <ts e="T985" id="Seg_6806" n="e" s="T984">к </ts>
               <ts e="T986" id="Seg_6808" n="e" s="T985">племяннику, </ts>
               <ts e="T987" id="Seg_6810" n="e" s="T986">который </ts>
               <ts e="T988" id="Seg_6812" n="e" s="T987">вот </ts>
               <ts e="T989" id="Seg_6814" n="e" s="T988">сейчас… </ts>
            </ts>
            <ts e="T1008" id="Seg_6815" n="sc" s="T990">
               <ts e="T991" id="Seg_6817" n="e" s="T990">К </ts>
               <ts e="T992" id="Seg_6819" n="e" s="T991">еёному, </ts>
               <ts e="T993" id="Seg_6821" n="e" s="T992">ее </ts>
               <ts e="T994" id="Seg_6823" n="e" s="T993">племянник </ts>
               <ts e="T995" id="Seg_6825" n="e" s="T994">был, </ts>
               <ts e="T996" id="Seg_6827" n="e" s="T995">Иваном </ts>
               <ts e="T997" id="Seg_6829" n="e" s="T996">его </ts>
               <ts e="T998" id="Seg_6831" n="e" s="T997">звали, </ts>
               <ts e="T999" id="Seg_6833" n="e" s="T998">который </ts>
               <ts e="T1000" id="Seg_6835" n="e" s="T999">сейчас </ts>
               <ts e="T1001" id="Seg_6837" n="e" s="T1000">в </ts>
               <ts e="T1002" id="Seg_6839" n="e" s="T1001">Пьянково </ts>
               <ts e="T1003" id="Seg_6841" n="e" s="T1002">его </ts>
               <ts e="T1004" id="Seg_6843" n="e" s="T1003">дочка </ts>
               <ts e="T1005" id="Seg_6845" n="e" s="T1004">живет, </ts>
               <ts e="T1006" id="Seg_6847" n="e" s="T1005">она </ts>
               <ts e="T1007" id="Seg_6849" n="e" s="T1006">мне </ts>
               <ts e="T1008" id="Seg_6851" n="e" s="T1007">племянница. </ts>
            </ts>
            <ts e="T1013" id="Seg_6852" n="sc" s="T1009">
               <ts e="T1010" id="Seg_6854" n="e" s="T1009">Ну </ts>
               <ts e="T1011" id="Seg_6856" n="e" s="T1010">и </ts>
               <ts e="T1012" id="Seg_6858" n="e" s="T1011">мама </ts>
               <ts e="T1013" id="Seg_6860" n="e" s="T1012">это… </ts>
            </ts>
            <ts e="T1025" id="Seg_6861" n="sc" s="T1014">
               <ts e="T1015" id="Seg_6863" n="e" s="T1014">Пришла, </ts>
               <ts e="T1016" id="Seg_6865" n="e" s="T1015">а </ts>
               <ts e="T1017" id="Seg_6867" n="e" s="T1016">он </ts>
               <ts e="T1018" id="Seg_6869" n="e" s="T1017">опять </ts>
               <ts e="T1019" id="Seg_6871" n="e" s="T1018">пришел, </ts>
               <ts e="T1020" id="Seg_6873" n="e" s="T1019">меня </ts>
               <ts e="T1021" id="Seg_6875" n="e" s="T1020">за </ts>
               <ts e="T1022" id="Seg_6877" n="e" s="T1021">руку </ts>
               <ts e="T1023" id="Seg_6879" n="e" s="T1022">взял </ts>
               <ts e="T1024" id="Seg_6881" n="e" s="T1023">и </ts>
               <ts e="T1025" id="Seg_6883" n="e" s="T1024">увел. </ts>
            </ts>
            <ts e="T1034" id="Seg_6884" n="sc" s="T1026">
               <ts e="T1027" id="Seg_6886" n="e" s="T1026">Ну, </ts>
               <ts e="T1028" id="Seg_6888" n="e" s="T1027">потом </ts>
               <ts e="T1029" id="Seg_6890" n="e" s="T1028">мы </ts>
               <ts e="T1030" id="Seg_6892" n="e" s="T1029">легли, </ts>
               <ts e="T1031" id="Seg_6894" n="e" s="T1030">а </ts>
               <ts e="T1032" id="Seg_6896" n="e" s="T1031">тогда </ts>
               <ts e="T1033" id="Seg_6898" n="e" s="T1032">они </ts>
               <ts e="T1034" id="Seg_6900" n="e" s="T1033">пришли. </ts>
            </ts>
            <ts e="T1045" id="Seg_6901" n="sc" s="T1035">
               <ts e="T1036" id="Seg_6903" n="e" s="T1035">"Если </ts>
               <ts e="T1037" id="Seg_6905" n="e" s="T1036">вы </ts>
               <ts e="T1038" id="Seg_6907" n="e" s="T1037">будете </ts>
               <ts e="T1039" id="Seg_6909" n="e" s="T1038">жить </ts>
               <ts e="T1040" id="Seg_6911" n="e" s="T1039">дак, </ts>
               <ts e="T1041" id="Seg_6913" n="e" s="T1040">мама </ts>
               <ts e="T1042" id="Seg_6915" n="e" s="T1041">говорит, </ts>
               <ts e="T1043" id="Seg_6917" n="e" s="T1042">я </ts>
               <ts e="T1044" id="Seg_6919" n="e" s="T1043">благословлю </ts>
               <ts e="T1045" id="Seg_6921" n="e" s="T1044">вас". </ts>
            </ts>
            <ts e="T1055" id="Seg_6922" n="sc" s="T1046">
               <ts e="T1047" id="Seg_6924" n="e" s="T1046">Ну, </ts>
               <ts e="T1048" id="Seg_6926" n="e" s="T1047">тут </ts>
               <ts e="T1049" id="Seg_6928" n="e" s="T1048">благословила </ts>
               <ts e="T1050" id="Seg_6930" n="e" s="T1049">она </ts>
               <ts e="T1051" id="Seg_6932" n="e" s="T1050">нас </ts>
               <ts e="T1052" id="Seg_6934" n="e" s="T1051">(на </ts>
               <ts e="T1053" id="Seg_6936" n="e" s="T1052">завтре), </ts>
               <ts e="T1054" id="Seg_6938" n="e" s="T1053">тоже </ts>
               <ts e="T1055" id="Seg_6940" n="e" s="T1054">тут. </ts>
            </ts>
            <ts e="T1074" id="Seg_6941" n="sc" s="T1056">
               <ts e="T1057" id="Seg_6943" n="e" s="T1056">К </ts>
               <ts e="T1058" id="Seg_6945" n="e" s="T1057">нам </ts>
               <ts e="T1059" id="Seg_6947" n="e" s="T1058">пришли, </ts>
               <ts e="T1060" id="Seg_6949" n="e" s="T1059">этот, </ts>
               <ts e="T1061" id="Seg_6951" n="e" s="T1060">брат </ts>
               <ts e="T1062" id="Seg_6953" n="e" s="T1061">сродный, </ts>
               <ts e="T1063" id="Seg_6955" n="e" s="T1062">невестка, </ts>
               <ts e="T1064" id="Seg_6957" n="e" s="T1063">мы </ts>
               <ts e="T1065" id="Seg_6959" n="e" s="T1064">тут </ts>
               <ts e="T1066" id="Seg_6961" n="e" s="T1065">чаю </ts>
               <ts e="T1067" id="Seg_6963" n="e" s="T1066">(п-) </ts>
               <ts e="T1068" id="Seg_6965" n="e" s="T1067">попили, </ts>
               <ts e="T1069" id="Seg_6967" n="e" s="T1068">они </ts>
               <ts e="T1070" id="Seg_6969" n="e" s="T1069">тогда </ts>
               <ts e="T1071" id="Seg_6971" n="e" s="T1070">ушли, </ts>
               <ts e="T1072" id="Seg_6973" n="e" s="T1071">и </ts>
               <ts e="T1073" id="Seg_6975" n="e" s="T1072">мама </ts>
               <ts e="T1074" id="Seg_6977" n="e" s="T1073">уехала. </ts>
            </ts>
            <ts e="T1081" id="Seg_6978" n="sc" s="T1075">
               <ts e="T1076" id="Seg_6980" n="e" s="T1075">Ну, </ts>
               <ts e="T1077" id="Seg_6982" n="e" s="T1076">теперь </ts>
               <ts e="T1079" id="Seg_6984" n="e" s="T1077">можно </ts>
               <ts e="T1081" id="Seg_6986" n="e" s="T1079">по-камасински. </ts>
            </ts>
            <ts e="T1103" id="Seg_6987" n="sc" s="T1090">
               <ts e="T1092" id="Seg_6989" n="e" s="T1090">Ну, </ts>
               <ts e="T1094" id="Seg_6991" n="e" s="T1092">потом </ts>
               <ts e="T1095" id="Seg_6993" n="e" s="T1094">(мы=) </ts>
               <ts e="T1096" id="Seg_6995" n="e" s="T1095">про </ts>
               <ts e="T1097" id="Seg_6997" n="e" s="T1096">это </ts>
               <ts e="T1098" id="Seg_6999" n="e" s="T1097">надо </ts>
               <ts e="T1099" id="Seg_7001" n="e" s="T1098">сказать, </ts>
               <ts e="T1100" id="Seg_7003" n="e" s="T1099">что </ts>
               <ts e="T1101" id="Seg_7005" n="e" s="T1100">он </ts>
               <ts e="T1102" id="Seg_7007" n="e" s="T1101">туды </ts>
               <ts e="T1103" id="Seg_7009" n="e" s="T1102">поехал. </ts>
            </ts>
            <ts e="T1119" id="Seg_7010" n="sc" s="T1104">
               <ts e="T1105" id="Seg_7012" n="e" s="T1104">Тогда </ts>
               <ts e="T1106" id="Seg_7014" n="e" s="T1105">он </ts>
               <ts e="T1107" id="Seg_7016" n="e" s="T1106">меня </ts>
               <ts e="T1108" id="Seg_7018" n="e" s="T1107">оставил </ts>
               <ts e="T1109" id="Seg_7020" n="e" s="T1108">дома, </ts>
               <ts e="T1110" id="Seg_7022" n="e" s="T1109">не </ts>
               <ts e="T1111" id="Seg_7024" n="e" s="T1110">взял </ts>
               <ts e="T1112" id="Seg_7026" n="e" s="T1111">с </ts>
               <ts e="T1113" id="Seg_7028" n="e" s="T1112">собой, </ts>
               <ts e="T1114" id="Seg_7030" n="e" s="T1113">поехал </ts>
               <ts e="T1115" id="Seg_7032" n="e" s="T1114">туды </ts>
               <ts e="T1116" id="Seg_7034" n="e" s="T1115">к </ts>
               <ts e="T1117" id="Seg_7036" n="e" s="T1116">нам </ts>
               <ts e="T1118" id="Seg_7038" n="e" s="T1117">в </ts>
               <ts e="T1119" id="Seg_7040" n="e" s="T1118">Абалаково. </ts>
            </ts>
            <ts e="T1123" id="Seg_7041" n="sc" s="T1120">
               <ts e="T1121" id="Seg_7043" n="e" s="T1120">Дяде </ts>
               <ts e="T1122" id="Seg_7045" n="e" s="T1121">сено </ts>
               <ts e="T1123" id="Seg_7047" n="e" s="T1122">косить. </ts>
            </ts>
            <ts e="T1134" id="Seg_7048" n="sc" s="T1124">
               <ts e="T1125" id="Seg_7050" n="e" s="T1124">И </ts>
               <ts e="T1126" id="Seg_7052" n="e" s="T1125">как </ts>
               <ts e="T1127" id="Seg_7054" n="e" s="T1126">раз </ts>
               <ts e="T1128" id="Seg_7056" n="e" s="T1127">был </ts>
               <ts e="T1129" id="Seg_7058" n="e" s="T1128">день </ts>
               <ts e="T1130" id="Seg_7060" n="e" s="T1129">такой, </ts>
               <ts e="T1131" id="Seg_7062" n="e" s="T1130">(пра-) </ts>
               <ts e="T1132" id="Seg_7064" n="e" s="T1131">праздник </ts>
               <ts e="T1133" id="Seg_7066" n="e" s="T1132">Ивана </ts>
               <ts e="T1134" id="Seg_7068" n="e" s="T1133">Купала. </ts>
            </ts>
            <ts e="T1141" id="Seg_7069" n="sc" s="T1135">
               <ts e="T1136" id="Seg_7071" n="e" s="T1135">Они </ts>
               <ts e="T1137" id="Seg_7073" n="e" s="T1136">по </ts>
               <ts e="T1138" id="Seg_7075" n="e" s="T1137">деревне </ts>
               <ts e="T1139" id="Seg_7077" n="e" s="T1138">ходили, </ts>
               <ts e="T1140" id="Seg_7079" n="e" s="T1139">обливались, </ts>
               <ts e="T1141" id="Seg_7081" n="e" s="T1140">обливалися. </ts>
            </ts>
            <ts e="T1146" id="Seg_7082" n="sc" s="T1142">
               <ts e="T1143" id="Seg_7084" n="e" s="T1142">А </ts>
               <ts e="T1144" id="Seg_7086" n="e" s="T1143">тады </ts>
               <ts e="T1145" id="Seg_7088" n="e" s="T1144">собрались, </ts>
               <ts e="T1146" id="Seg_7090" n="e" s="T1145">пошли. </ts>
            </ts>
            <ts e="T1161" id="Seg_7091" n="sc" s="T1147">
               <ts e="T1148" id="Seg_7093" n="e" s="T1147">Там </ts>
               <ts e="T1149" id="Seg_7095" n="e" s="T1148">еще </ts>
               <ts e="T1150" id="Seg_7097" n="e" s="T1149">ниже </ts>
               <ts e="T1151" id="Seg_7099" n="e" s="T1150">этой </ts>
               <ts e="T1152" id="Seg_7101" n="e" s="T1151">мельницы, </ts>
               <ts e="T1153" id="Seg_7103" n="e" s="T1152">где </ts>
               <ts e="T1154" id="Seg_7105" n="e" s="T1153">живут </ts>
               <ts e="T1155" id="Seg_7107" n="e" s="T1154">эстонцы, </ts>
               <ts e="T1156" id="Seg_7109" n="e" s="T1155">другая </ts>
               <ts e="T1157" id="Seg_7111" n="e" s="T1156">мельница. </ts>
               <ts e="T1158" id="Seg_7113" n="e" s="T1157">Пошли </ts>
               <ts e="T1159" id="Seg_7115" n="e" s="T1158">туды </ts>
               <ts e="T1160" id="Seg_7117" n="e" s="T1159">в </ts>
               <ts e="T1161" id="Seg_7119" n="e" s="T1160">пруд. </ts>
            </ts>
            <ts e="T1173" id="Seg_7120" n="sc" s="T1162">
               <ts e="T1163" id="Seg_7122" n="e" s="T1162">И </ts>
               <ts e="T1164" id="Seg_7124" n="e" s="T1163">пошли, </ts>
               <ts e="T1165" id="Seg_7126" n="e" s="T1164">пошли, </ts>
               <ts e="T1166" id="Seg_7128" n="e" s="T1165">и </ts>
               <ts e="T1167" id="Seg_7130" n="e" s="T1166">глыбоко </ts>
               <ts e="T1168" id="Seg_7132" n="e" s="T1167">зашли, </ts>
               <ts e="T1169" id="Seg_7134" n="e" s="T1168">а </ts>
               <ts e="T1170" id="Seg_7136" n="e" s="T1169">там </ts>
               <ts e="T1171" id="Seg_7138" n="e" s="T1170">из </ts>
               <ts e="T1172" id="Seg_7140" n="e" s="T1171">земли </ts>
               <ts e="T1173" id="Seg_7142" n="e" s="T1172">родник. </ts>
            </ts>
            <ts e="T1178" id="Seg_7143" n="sc" s="T1174">
               <ts e="T1175" id="Seg_7145" n="e" s="T1174">Они </ts>
               <ts e="T1176" id="Seg_7147" n="e" s="T1175">зачали </ts>
               <ts e="T1177" id="Seg_7149" n="e" s="T1176">двое </ts>
               <ts e="T1178" id="Seg_7151" n="e" s="T1177">тонуть. </ts>
            </ts>
            <ts e="T1185" id="Seg_7152" n="sc" s="T1179">
               <ts e="T1180" id="Seg_7154" n="e" s="T1179">А </ts>
               <ts e="T1181" id="Seg_7156" n="e" s="T1180">с </ts>
               <ts e="T1182" id="Seg_7158" n="e" s="T1181">имя </ts>
               <ts e="T1183" id="Seg_7160" n="e" s="T1182">был </ts>
               <ts e="T1184" id="Seg_7162" n="e" s="T1183">один </ts>
               <ts e="T1185" id="Seg_7164" n="e" s="T1184">парень. </ts>
            </ts>
            <ts e="T1195" id="Seg_7165" n="sc" s="T1186">
               <ts e="T1187" id="Seg_7167" n="e" s="T1186">Он </ts>
               <ts e="T1188" id="Seg_7169" n="e" s="T1187">подал </ts>
               <ts e="T1189" id="Seg_7171" n="e" s="T1188">жердь, </ts>
               <ts e="T1190" id="Seg_7173" n="e" s="T1189">эту </ts>
               <ts e="T1191" id="Seg_7175" n="e" s="T1190">выдернул, </ts>
               <ts e="T1192" id="Seg_7177" n="e" s="T1191">а </ts>
               <ts e="T1193" id="Seg_7179" n="e" s="T1192">его </ts>
               <ts e="T1194" id="Seg_7181" n="e" s="T1193">не </ts>
               <ts e="T1195" id="Seg_7183" n="e" s="T1194">стало. </ts>
            </ts>
            <ts e="T1209" id="Seg_7184" n="sc" s="T1196">
               <ts e="T1197" id="Seg_7186" n="e" s="T1196">Пока </ts>
               <ts e="T1198" id="Seg_7188" n="e" s="T1197">он </ts>
               <ts e="T1199" id="Seg_7190" n="e" s="T1198">побежал </ts>
               <ts e="T1200" id="Seg_7192" n="e" s="T1199">в </ts>
               <ts e="T1201" id="Seg_7194" n="e" s="T1200">деревню, </ts>
               <ts e="T1202" id="Seg_7196" n="e" s="T1201">да </ts>
               <ts e="T1203" id="Seg_7198" n="e" s="T1202">отпустили </ts>
               <ts e="T1204" id="Seg_7200" n="e" s="T1203">воду, </ts>
               <ts e="T1205" id="Seg_7202" n="e" s="T1204">вытащили </ts>
               <ts e="T1206" id="Seg_7204" n="e" s="T1205">его, </ts>
               <ts e="T1207" id="Seg_7206" n="e" s="T1206">откачивали-откачивали, </ts>
               <ts e="T1208" id="Seg_7208" n="e" s="T1207">не </ts>
               <ts e="T1209" id="Seg_7210" n="e" s="T1208">откачали. </ts>
            </ts>
            <ts e="T1229" id="Seg_7211" n="sc" s="T1210">
               <ts e="T1211" id="Seg_7213" n="e" s="T1210">А </ts>
               <ts e="T1212" id="Seg_7215" n="e" s="T1211">потом </ts>
               <ts e="T1213" id="Seg_7217" n="e" s="T1212">привезли </ts>
               <ts e="T1214" id="Seg_7219" n="e" s="T1213">туды </ts>
               <ts e="T1215" id="Seg_7221" n="e" s="T1214">мертвого </ts>
               <ts e="T1216" id="Seg_7223" n="e" s="T1215">и </ts>
               <ts e="T1217" id="Seg_7225" n="e" s="T1216">схоронили. </ts>
               <ts e="T1218" id="Seg_7227" n="e" s="T1217">(Я </ts>
               <ts e="T1219" id="Seg_7229" n="e" s="T1218">= </ts>
               <ts e="T1220" id="Seg_7231" n="e" s="T1219">я=) </ts>
               <ts e="T1221" id="Seg_7233" n="e" s="T1220">Я </ts>
               <ts e="T1222" id="Seg_7235" n="e" s="T1221">его </ts>
               <ts e="T1223" id="Seg_7237" n="e" s="T1222">хоронила, </ts>
               <ts e="T1224" id="Seg_7239" n="e" s="T1223">а </ts>
               <ts e="T1225" id="Seg_7241" n="e" s="T1224">оттель </ts>
               <ts e="T1226" id="Seg_7243" n="e" s="T1225">домой </ts>
               <ts e="T1227" id="Seg_7245" n="e" s="T1226">вернулась </ts>
               <ts e="T1228" id="Seg_7247" n="e" s="T1227">обратно, </ts>
               <ts e="T1229" id="Seg_7249" n="e" s="T1228">приехала. </ts>
            </ts>
            <ts e="T1248" id="Seg_7250" n="sc" s="T1235">
               <ts e="T1236" id="Seg_7252" n="e" s="T1235">Ну, </ts>
               <ts e="T1237" id="Seg_7254" n="e" s="T1236">переживания, </ts>
               <ts e="T1238" id="Seg_7256" n="e" s="T1237">вот </ts>
               <ts e="T1239" id="Seg_7258" n="e" s="T1238">у </ts>
               <ts e="T1240" id="Seg_7260" n="e" s="T1239">меня </ts>
               <ts e="T1241" id="Seg_7262" n="e" s="T1240">голова </ts>
               <ts e="T1242" id="Seg_7264" n="e" s="T1241">трясется. </ts>
               <ts e="T1243" id="Seg_7266" n="e" s="T1242">Много </ts>
               <ts e="T1244" id="Seg_7268" n="e" s="T1243">печали </ts>
               <ts e="T1245" id="Seg_7270" n="e" s="T1244">в </ts>
               <ts e="T1246" id="Seg_7272" n="e" s="T1245">голове </ts>
               <ts e="T1247" id="Seg_7274" n="e" s="T1246">в </ts>
               <ts e="T1248" id="Seg_7276" n="e" s="T1247">моёй. </ts>
            </ts>
            <ts e="T1250" id="Seg_7277" n="sc" s="T1249">
               <ts e="T1250" id="Seg_7279" n="e" s="T1249">Вот. </ts>
            </ts>
            <ts e="T1275" id="Seg_7280" n="sc" s="T1262">
               <ts e="T1263" id="Seg_7282" n="e" s="T1262">Kamen </ts>
               <ts e="T1264" id="Seg_7284" n="e" s="T1263">dĭ </ts>
               <ts e="T1265" id="Seg_7286" n="e" s="T1264">šobi </ts>
               <ts e="T1266" id="Seg_7288" n="e" s="T1265">măna: </ts>
               <ts e="T1267" id="Seg_7290" n="e" s="T1266">"Kanžəbəj, </ts>
               <ts e="T1268" id="Seg_7292" n="e" s="T1267">dĭn </ts>
               <ts e="T1269" id="Seg_7294" n="e" s="T1268">iat </ts>
               <ts e="T1270" id="Seg_7296" n="e" s="T1269">tibinə </ts>
               <ts e="T1271" id="Seg_7298" n="e" s="T1270">kalla </ts>
               <ts e="T1272" id="Seg_7300" n="e" s="T1271">dʼürbi, </ts>
               <ts e="T1273" id="Seg_7302" n="e" s="T1272">măn </ts>
               <ts e="T1274" id="Seg_7304" n="e" s="T1273">unnʼa </ts>
               <ts e="T1275" id="Seg_7306" n="e" s="T1274">amnolaʔbiom". </ts>
            </ts>
            <ts e="T1290" id="Seg_7307" n="sc" s="T1276">
               <ts e="T1277" id="Seg_7309" n="e" s="T1276">Măn </ts>
               <ts e="T1278" id="Seg_7311" n="e" s="T1277">mămbiam: </ts>
               <ts e="T1279" id="Seg_7313" n="e" s="T1278">"Kanaʔ, </ts>
               <ts e="T1280" id="Seg_7315" n="e" s="T1279">măna </ts>
               <ts e="T1281" id="Seg_7317" n="e" s="T1280">monoʔkoʔ </ts>
               <ts e="T1282" id="Seg_7319" n="e" s="T1281">abanə </ts>
               <ts e="T1283" id="Seg_7321" n="e" s="T1282">da </ts>
               <ts e="T1284" id="Seg_7323" n="e" s="T1283">ianə". </ts>
               <ts e="T1285" id="Seg_7325" n="e" s="T1284">Dĭ </ts>
               <ts e="T1286" id="Seg_7327" n="e" s="T1285">măndə: </ts>
               <ts e="T1287" id="Seg_7329" n="e" s="T1286">"Dĭzeŋ </ts>
               <ts e="T1288" id="Seg_7331" n="e" s="T1287">ej </ts>
               <ts e="T1289" id="Seg_7333" n="e" s="T1288">mĭləʔi, </ts>
               <ts e="T1290" id="Seg_7335" n="e" s="T1289">kanžəbəj!" </ts>
            </ts>
            <ts e="T1295" id="Seg_7336" n="sc" s="T1291">
               <ts e="T1292" id="Seg_7338" n="e" s="T1291">Dĭgəttə </ts>
               <ts e="T1293" id="Seg_7340" n="e" s="T1292">măn </ts>
               <ts e="T1294" id="Seg_7342" n="e" s="T1293">oldʼam </ts>
               <ts e="T1295" id="Seg_7344" n="e" s="T1294">oʔbdəbiom. </ts>
            </ts>
            <ts e="T1303" id="Seg_7345" n="sc" s="T1296">
               <ts e="T1297" id="Seg_7347" n="e" s="T1296">Nüdʼin </ts>
               <ts e="T1298" id="Seg_7349" n="e" s="T1297">inem </ts>
               <ts e="T1299" id="Seg_7351" n="e" s="T1298">kondlaʔpibaʔ, </ts>
               <ts e="T1300" id="Seg_7353" n="e" s="T1299">amnəbibaʔ, </ts>
               <ts e="T1301" id="Seg_7355" n="e" s="T1300">kambibaʔ </ts>
               <ts e="T1302" id="Seg_7357" n="e" s="T1301">Kazan </ts>
               <ts e="T1303" id="Seg_7359" n="e" s="T1302">turanə. </ts>
            </ts>
            <ts e="T1314" id="Seg_7360" n="sc" s="T1304">
               <ts e="T1305" id="Seg_7362" n="e" s="T1304">Bar </ts>
               <ts e="T1306" id="Seg_7364" n="e" s="T1305">nüdʼi </ts>
               <ts e="T1307" id="Seg_7366" n="e" s="T1306">dĭn </ts>
               <ts e="T1308" id="Seg_7368" n="e" s="T1307">kunolbibaʔ, </ts>
               <ts e="T1309" id="Seg_7370" n="e" s="T1308">dĭgəttə </ts>
               <ts e="T1310" id="Seg_7372" n="e" s="T1309">šobibaʔ </ts>
               <ts e="T1311" id="Seg_7374" n="e" s="T1310">bazo </ts>
               <ts e="T1312" id="Seg_7376" n="e" s="T1311">(dĭʔnə) </ts>
               <ts e="T1313" id="Seg_7378" n="e" s="T1312">turanə, </ts>
               <ts e="T1314" id="Seg_7380" n="e" s="T1313">Kaptɨrlɨktə. </ts>
            </ts>
            <ts e="T1326" id="Seg_7381" n="sc" s="T1315">
               <ts e="T1316" id="Seg_7383" n="e" s="T1315">Dĭn </ts>
               <ts e="T1317" id="Seg_7385" n="e" s="T1316">selaj </ts>
               <ts e="T1318" id="Seg_7387" n="e" s="T1317">(dʼal-) </ts>
               <ts e="T1319" id="Seg_7389" n="e" s="T1318">dʼala </ts>
               <ts e="T1320" id="Seg_7391" n="e" s="T1319">ibibeʔ, </ts>
               <ts e="T1321" id="Seg_7393" n="e" s="T1320">dĭgəttə </ts>
               <ts e="T1322" id="Seg_7395" n="e" s="T1321">nüdʼin </ts>
               <ts e="T1323" id="Seg_7397" n="e" s="T1322">šobibaʔ </ts>
               <ts e="T1324" id="Seg_7399" n="e" s="T1323">(dĭ=) </ts>
               <ts e="T1325" id="Seg_7401" n="e" s="T1324">dĭn </ts>
               <ts e="T1326" id="Seg_7403" n="e" s="T1325">maːʔnə. </ts>
            </ts>
            <ts e="T1334" id="Seg_7404" n="sc" s="T1327">
               <ts e="T1328" id="Seg_7406" n="e" s="T1327">Dĭgəttə </ts>
               <ts e="T1329" id="Seg_7408" n="e" s="T1328">dĭ </ts>
               <ts e="T1330" id="Seg_7410" n="e" s="T1329">ertən </ts>
               <ts e="T1331" id="Seg_7412" n="e" s="T1330">šobi, </ts>
               <ts e="T1332" id="Seg_7414" n="e" s="T1331">dĭn </ts>
               <ts e="T1333" id="Seg_7416" n="e" s="T1332">kazak </ts>
               <ts e="T1334" id="Seg_7418" n="e" s="T1333">iat. </ts>
            </ts>
            <ts e="T1337" id="Seg_7419" n="sc" s="T1335">
               <ts e="T1336" id="Seg_7421" n="e" s="T1335">Dĭgəttə </ts>
               <ts e="T1337" id="Seg_7423" n="e" s="T1336">uʔbdəbibaʔ. </ts>
            </ts>
            <ts e="T1340" id="Seg_7424" n="sc" s="T1338">
               <ts e="T1339" id="Seg_7426" n="e" s="T1338">Dĭn </ts>
               <ts e="T1340" id="Seg_7428" n="e" s="T1339">amnobiam. </ts>
            </ts>
            <ts e="T1346" id="Seg_7429" n="sc" s="T1341">
               <ts e="T1342" id="Seg_7431" n="e" s="T1341">(N-) </ts>
               <ts e="T1343" id="Seg_7433" n="e" s="T1342">Noʔ </ts>
               <ts e="T1344" id="Seg_7435" n="e" s="T1343">jaʔpibaʔ </ts>
               <ts e="T1345" id="Seg_7437" n="e" s="T1344">dĭn </ts>
               <ts e="T1346" id="Seg_7439" n="e" s="T1345">bar. </ts>
            </ts>
            <ts e="T1351" id="Seg_7440" n="sc" s="T1347">
               <ts e="T1348" id="Seg_7442" n="e" s="T1347">Dĭgəttə </ts>
               <ts e="T1349" id="Seg_7444" n="e" s="T1348">dĭn </ts>
               <ts e="T1350" id="Seg_7446" n="e" s="T1349">dʼadʼat </ts>
               <ts e="T1351" id="Seg_7448" n="e" s="T1350">šobi. </ts>
            </ts>
            <ts e="T1356" id="Seg_7449" n="sc" s="T1352">
               <ts e="T1353" id="Seg_7451" n="e" s="T1352">"Šoʔ </ts>
               <ts e="T1354" id="Seg_7453" n="e" s="T1353">măna </ts>
               <ts e="T1355" id="Seg_7455" n="e" s="T1354">noʔ </ts>
               <ts e="T1356" id="Seg_7457" n="e" s="T1355">jaʔsittə". </ts>
            </ts>
            <ts e="T1361" id="Seg_7458" n="sc" s="T1357">
               <ts e="T1358" id="Seg_7460" n="e" s="T1357">Dĭgəttə </ts>
               <ts e="T1359" id="Seg_7462" n="e" s="T1358">dĭ </ts>
               <ts e="T1360" id="Seg_7464" n="e" s="T1359">koʔbdom </ts>
               <ts e="T1361" id="Seg_7466" n="e" s="T1360">deʔpi. </ts>
            </ts>
            <ts e="T1368" id="Seg_7467" n="sc" s="T1362">
               <ts e="T1363" id="Seg_7469" n="e" s="T1362">Măna </ts>
               <ts e="T1364" id="Seg_7471" n="e" s="T1363">nörbəbiʔi </ts>
               <ts e="T1365" id="Seg_7473" n="e" s="T1364">dĭ </ts>
               <ts e="T1366" id="Seg_7475" n="e" s="T1365">dĭzi </ts>
               <ts e="T1367" id="Seg_7477" n="e" s="T1366">(amnozittə=) </ts>
               <ts e="T1368" id="Seg_7479" n="e" s="T1367">amnoləj. </ts>
            </ts>
            <ts e="T1384" id="Seg_7480" n="sc" s="T1369">
               <ts e="T1370" id="Seg_7482" n="e" s="T1369">Măn </ts>
               <ts e="T1371" id="Seg_7484" n="e" s="T1370">bar </ts>
               <ts e="T1372" id="Seg_7486" n="e" s="T1371">oldʼam </ts>
               <ts e="T1373" id="Seg_7488" n="e" s="T1372">ibiem </ts>
               <ts e="T1374" id="Seg_7490" n="e" s="T1373">i </ts>
               <ts e="T1375" id="Seg_7492" n="e" s="T1374">(dĭ </ts>
               <ts e="T1376" id="Seg_7494" n="e" s="T1375">= </ts>
               <ts e="T1377" id="Seg_7496" n="e" s="T1376">dĭ=) </ts>
               <ts e="T1378" id="Seg_7498" n="e" s="T1377">dĭn </ts>
               <ts e="T1379" id="Seg_7500" n="e" s="T1378">dʼadʼazi </ts>
               <ts e="T1380" id="Seg_7502" n="e" s="T1379">kambiam. </ts>
               <ts e="T1381" id="Seg_7504" n="e" s="T1380">(Dĭ=) </ts>
               <ts e="T1382" id="Seg_7506" n="e" s="T1381">Dĭ </ts>
               <ts e="T1383" id="Seg_7508" n="e" s="T1382">bĭdəbi </ts>
               <ts e="T1384" id="Seg_7510" n="e" s="T1383">măna. </ts>
            </ts>
            <ts e="T1392" id="Seg_7511" n="sc" s="T1385">
               <ts e="T1386" id="Seg_7513" n="e" s="T1385">Bazo </ts>
               <ts e="T1387" id="Seg_7515" n="e" s="T1386">deʔpi </ts>
               <ts e="T1388" id="Seg_7517" n="e" s="T1387">maʔndə. </ts>
               <ts e="T1389" id="Seg_7519" n="e" s="T1388">Dĭgəttə </ts>
               <ts e="T1390" id="Seg_7521" n="e" s="T1389">măn </ts>
               <ts e="T1391" id="Seg_7523" n="e" s="T1390">iam </ts>
               <ts e="T1392" id="Seg_7525" n="e" s="T1391">šobi. </ts>
            </ts>
            <ts e="T1397" id="Seg_7526" n="sc" s="T1393">
               <ts e="T1394" id="Seg_7528" n="e" s="T1393">Măn </ts>
               <ts e="T1395" id="Seg_7530" n="e" s="T1394">bazo </ts>
               <ts e="T1396" id="Seg_7532" n="e" s="T1395">kalla </ts>
               <ts e="T1397" id="Seg_7534" n="e" s="T1396">dʼürbim. </ts>
            </ts>
            <ts e="T1410" id="Seg_7535" n="sc" s="T1398">
               <ts e="T1399" id="Seg_7537" n="e" s="T1398">Dĭ </ts>
               <ts e="T1400" id="Seg_7539" n="e" s="T1399">šobi </ts>
               <ts e="T1401" id="Seg_7541" n="e" s="T1400">da </ts>
               <ts e="T1402" id="Seg_7543" n="e" s="T1401">măna </ts>
               <ts e="T1403" id="Seg_7545" n="e" s="T1402">kundlambi </ts>
               <ts e="T1404" id="Seg_7547" n="e" s="T1403">maʔnʼi. </ts>
               <ts e="T1405" id="Seg_7549" n="e" s="T1404">Iʔbəbeʔ </ts>
               <ts e="T1406" id="Seg_7551" n="e" s="T1405">kunolzittə, </ts>
               <ts e="T1407" id="Seg_7553" n="e" s="T1406">dĭgəttə </ts>
               <ts e="T1408" id="Seg_7555" n="e" s="T1407">dĭzeŋ </ts>
               <ts e="T1409" id="Seg_7557" n="e" s="T1408">(s-) </ts>
               <ts e="T1410" id="Seg_7559" n="e" s="T1409">šobiʔi. </ts>
            </ts>
            <ts e="T1413" id="Seg_7560" n="sc" s="T1411">
               <ts e="T1412" id="Seg_7562" n="e" s="T1411">"No, </ts>
               <ts e="T1413" id="Seg_7564" n="e" s="T1412">amnogaʔ". </ts>
            </ts>
            <ts e="T1418" id="Seg_7565" n="sc" s="T1414">
               <ts e="T1415" id="Seg_7567" n="e" s="T1414">Dĭgəttə </ts>
               <ts e="T1416" id="Seg_7569" n="e" s="T1415">miʔnibeʔ </ts>
               <ts e="T1417" id="Seg_7571" n="e" s="T1416">kudajdə </ts>
               <ts e="T1418" id="Seg_7573" n="e" s="T1417">numan üzəbiʔi. </ts>
            </ts>
            <ts e="T1421" id="Seg_7574" n="sc" s="T1419">
               <ts e="T1420" id="Seg_7576" n="e" s="T1419">Dĭgəttə </ts>
               <ts e="T1421" id="Seg_7578" n="e" s="T1420">kanbiʔi. </ts>
            </ts>
            <ts e="T1426" id="Seg_7579" n="sc" s="T1422">
               <ts e="T1423" id="Seg_7581" n="e" s="T1422">Karəldʼan </ts>
               <ts e="T1424" id="Seg_7583" n="e" s="T1423">uʔbdəbiʔi, </ts>
               <ts e="T1425" id="Seg_7585" n="e" s="T1424">miʔnʼibeʔ </ts>
               <ts e="T1426" id="Seg_7587" n="e" s="T1425">šobiʔi. </ts>
            </ts>
            <ts e="T1432" id="Seg_7588" n="sc" s="T1427">
               <ts e="T1428" id="Seg_7590" n="e" s="T1427">Amorbiʔi, </ts>
               <ts e="T1429" id="Seg_7592" n="e" s="T1428">dĭgəttə </ts>
               <ts e="T1430" id="Seg_7594" n="e" s="T1429">kalla </ts>
               <ts e="T1431" id="Seg_7596" n="e" s="T1430">dʼürbi </ts>
               <ts e="T1432" id="Seg_7598" n="e" s="T1431">maːʔndə. </ts>
            </ts>
            <ts e="T1439" id="Seg_7599" n="sc" s="T1433">
               <ts e="T1434" id="Seg_7601" n="e" s="T1433">Dĭgəttə </ts>
               <ts e="T1435" id="Seg_7603" n="e" s="T1434">dĭ </ts>
               <ts e="T1436" id="Seg_7605" n="e" s="T1435">kambi </ts>
               <ts e="T1437" id="Seg_7607" n="e" s="T1436">dʼadʼanə </ts>
               <ts e="T1438" id="Seg_7609" n="e" s="T1437">noʔ </ts>
               <ts e="T1439" id="Seg_7611" n="e" s="T1438">jaʔsittə. </ts>
            </ts>
            <ts e="T1452" id="Seg_7612" n="sc" s="T1440">
               <ts e="T1441" id="Seg_7614" n="e" s="T1440">I </ts>
               <ts e="T1442" id="Seg_7616" n="e" s="T1441">dĭn </ts>
               <ts e="T1443" id="Seg_7618" n="e" s="T1442">dʼala </ts>
               <ts e="T1444" id="Seg_7620" n="e" s="T1443">(dĭg-) </ts>
               <ts e="T1445" id="Seg_7622" n="e" s="T1444">dĭrgit </ts>
               <ts e="T1446" id="Seg_7624" n="e" s="T1445">ibi, </ts>
               <ts e="T1447" id="Seg_7626" n="e" s="T1446">dĭ </ts>
               <ts e="T1448" id="Seg_7628" n="e" s="T1447">(büzəzi) </ts>
               <ts e="T1449" id="Seg_7630" n="e" s="T1448">boskəndə. </ts>
               <ts e="T1450" id="Seg_7632" n="e" s="T1449">Gəttə </ts>
               <ts e="T1451" id="Seg_7634" n="e" s="T1450">kambiʔi </ts>
               <ts e="T1452" id="Seg_7636" n="e" s="T1451">bünə. </ts>
            </ts>
            <ts e="T1461" id="Seg_7637" n="sc" s="T1453">
               <ts e="T1454" id="Seg_7639" n="e" s="T1453">I </ts>
               <ts e="T1455" id="Seg_7641" n="e" s="T1454">dĭn </ts>
               <ts e="T1456" id="Seg_7643" n="e" s="T1455">šübiʔi </ts>
               <ts e="T1457" id="Seg_7645" n="e" s="T1456">bünə, </ts>
               <ts e="T1458" id="Seg_7647" n="e" s="T1457">onʼiʔ </ts>
               <ts e="T1459" id="Seg_7649" n="e" s="T1458">koʔbdo </ts>
               <ts e="T1460" id="Seg_7651" n="e" s="T1459">i </ts>
               <ts e="T1461" id="Seg_7653" n="e" s="T1460">dĭ. </ts>
            </ts>
            <ts e="T1463" id="Seg_7654" n="sc" s="T1462">
               <ts e="T1463" id="Seg_7656" n="e" s="T1462">bügən. </ts>
            </ts>
            <ts e="T1477" id="Seg_7657" n="sc" s="T1464">
               <ts e="T1465" id="Seg_7659" n="e" s="T1464">Dĭgəttə </ts>
               <ts e="T1466" id="Seg_7661" n="e" s="T1465">pa </ts>
               <ts e="T1467" id="Seg_7663" n="e" s="T1466">onʼiʔ </ts>
               <ts e="T1468" id="Seg_7665" n="e" s="T1467">nʼi </ts>
               <ts e="T1469" id="Seg_7667" n="e" s="T1468">mĭbi, </ts>
               <ts e="T1470" id="Seg_7669" n="e" s="T1469">dĭ </ts>
               <ts e="T1471" id="Seg_7671" n="e" s="T1470">koʔbdom </ts>
               <ts e="T1472" id="Seg_7673" n="e" s="T1471">deʔpi, </ts>
               <ts e="T1473" id="Seg_7675" n="e" s="T1472">a </ts>
               <ts e="T1474" id="Seg_7677" n="e" s="T1473">dĭ </ts>
               <ts e="T1475" id="Seg_7679" n="e" s="T1474">külaːmbi </ts>
               <ts e="T1476" id="Seg_7681" n="e" s="T1475">(bünən-) </ts>
               <ts e="T1477" id="Seg_7683" n="e" s="T1476">bügən. </ts>
            </ts>
            <ts e="T1480" id="Seg_7684" n="sc" s="T1478">
               <ts e="T1480" id="Seg_7686" n="e" s="T1478">((DMG)). </ts>
            </ts>
            <ts e="T1492" id="Seg_7687" n="sc" s="T1484">
               <ts e="T1488" id="Seg_7689" n="e" s="T1484">Погодите. </ts>
               <ts e="T1489" id="Seg_7691" n="e" s="T1488">Чего </ts>
               <ts e="T1490" id="Seg_7693" n="e" s="T1489">рассказать </ts>
               <ts e="T1491" id="Seg_7695" n="e" s="T1490">коротенько </ts>
               <ts e="T1492" id="Seg_7697" n="e" s="T1491">такое. </ts>
            </ts>
            <ts e="T1508" id="Seg_7698" n="sc" s="T1495">
               <ts e="T1498" id="Seg_7700" n="e" s="T1495">Nu, </ts>
               <ts e="T1499" id="Seg_7702" n="e" s="T1498">(karəlʼdʼa-) </ts>
               <ts e="T1500" id="Seg_7704" n="e" s="T1499">karəlʼdʼan </ts>
               <ts e="T1503" id="Seg_7706" n="e" s="T1500">(š- </ts>
               <ts e="T1506" id="Seg_7708" n="e" s="T1503">šo-) </ts>
               <ts e="T1508" id="Seg_7710" n="e" s="T1506">karəlʼdʼan… </ts>
            </ts>
            <ts e="T1512" id="Seg_7711" n="sc" s="T1511">
               <ts e="T1512" id="Seg_7713" n="e" s="T1511">((BRK)). </ts>
            </ts>
            <ts e="T1514" id="Seg_7714" n="sc" s="T1513">
               <ts e="T1514" id="Seg_7716" n="e" s="T1513">Кончилось. </ts>
            </ts>
            <ts e="T1521" id="Seg_7717" n="sc" s="T1518">
               <ts e="T1519" id="Seg_7719" n="e" s="T1518">А </ts>
               <ts e="T1520" id="Seg_7721" n="e" s="T1519">там </ts>
               <ts e="T1521" id="Seg_7723" n="e" s="T1520">же… </ts>
            </ts>
            <ts e="T1538" id="Seg_7724" n="sc" s="T1529">
               <ts e="T1530" id="Seg_7726" n="e" s="T1529">(А </ts>
               <ts e="T1531" id="Seg_7728" n="e" s="T1530">по-) </ts>
               <ts e="T1532" id="Seg_7730" n="e" s="T1531">А </ts>
               <ts e="T1533" id="Seg_7732" n="e" s="T1532">по-русски </ts>
               <ts e="T1534" id="Seg_7734" n="e" s="T1533">это </ts>
               <ts e="T1535" id="Seg_7736" n="e" s="T1534">я </ts>
               <ts e="T1536" id="Seg_7738" n="e" s="T1535">говорю </ts>
               <ts e="T1538" id="Seg_7740" n="e" s="T1536">приедет… </ts>
            </ts>
            <ts e="T1543" id="Seg_7741" n="sc" s="T1539">
               <ts e="T1540" id="Seg_7743" n="e" s="T1539">Приедет </ts>
               <ts e="T1541" id="Seg_7745" n="e" s="T1540">мой </ts>
               <ts e="T1542" id="Seg_7747" n="e" s="T1541">брат, </ts>
               <ts e="T1543" id="Seg_7749" n="e" s="T1542">Арпит. </ts>
            </ts>
            <ts e="T1548" id="Seg_7750" n="sc" s="T1544">
               <ts e="T1545" id="Seg_7752" n="e" s="T1544">Вот </ts>
               <ts e="T1547" id="Seg_7754" n="e" s="T1545">по-русски </ts>
               <ts e="T1548" id="Seg_7756" n="e" s="T1547">теперь. </ts>
            </ts>
            <ts e="T1552" id="Seg_7757" n="sc" s="T1549">
               <ts e="T1550" id="Seg_7759" n="e" s="T1549">Ты </ts>
               <ts e="T1551" id="Seg_7761" n="e" s="T1550">будешь </ts>
               <ts e="T1552" id="Seg_7763" n="e" s="T1551">рад. </ts>
            </ts>
            <ts e="T1563" id="Seg_7764" n="sc" s="T1562">
               <ts e="T1563" id="Seg_7766" n="e" s="T1562">А… </ts>
            </ts>
            <ts e="T1568" id="Seg_7767" n="sc" s="T1564">
               <ts e="T1566" id="Seg_7769" n="e" s="T1564">Опять </ts>
               <ts e="T1568" id="Seg_7771" n="e" s="T1566">(по-кам-). </ts>
            </ts>
            <ts e="T1574" id="Seg_7772" n="sc" s="T1571">
               <ts e="T1574" id="Seg_7774" n="e" s="T1571">Ну-ну. </ts>
            </ts>
            <ts e="T1579" id="Seg_7775" n="sc" s="T1576">
               <ts e="T1577" id="Seg_7777" n="e" s="T1576">Ну </ts>
               <ts e="T1578" id="Seg_7779" n="e" s="T1577">наладил </ts>
               <ts e="T1579" id="Seg_7781" n="e" s="T1578">ты? </ts>
            </ts>
            <ts e="T1590" id="Seg_7782" n="sc" s="T1586">
               <ts e="T1587" id="Seg_7784" n="e" s="T1586">Măn </ts>
               <ts e="T1588" id="Seg_7786" n="e" s="T1587">karəlʼdʼan </ts>
               <ts e="T1589" id="Seg_7788" n="e" s="T1588">edəʔleʔbəm </ts>
               <ts e="T1590" id="Seg_7790" n="e" s="T1589">Arpitəm. </ts>
            </ts>
            <ts e="T1600" id="Seg_7791" n="sc" s="T1591">
               <ts e="T1592" id="Seg_7793" n="e" s="T1591">Dĭ </ts>
               <ts e="T1593" id="Seg_7795" n="e" s="T1592">(š-) </ts>
               <ts e="T1594" id="Seg_7797" n="e" s="T1593">mămbi: </ts>
               <ts e="T1595" id="Seg_7799" n="e" s="T1594">"Šolam, </ts>
               <ts e="T1596" id="Seg_7801" n="e" s="T1595">iləm </ts>
               <ts e="T1597" id="Seg_7803" n="e" s="T1596">tănan, </ts>
               <ts e="T1598" id="Seg_7805" n="e" s="T1597">kanžəbəj </ts>
               <ts e="T1599" id="Seg_7807" n="e" s="T1598">măn </ts>
               <ts e="T1600" id="Seg_7809" n="e" s="T1599">ianə". </ts>
            </ts>
            <ts e="T1608" id="Seg_7810" n="sc" s="T1601">
               <ts e="T1602" id="Seg_7812" n="e" s="T1601">Dĭgəttə </ts>
               <ts e="T1603" id="Seg_7814" n="e" s="T1602">măn </ts>
               <ts e="T1604" id="Seg_7816" n="e" s="T1603">gibərdə </ts>
               <ts e="T1605" id="Seg_7818" n="e" s="T1604">ej </ts>
               <ts e="T1606" id="Seg_7820" n="e" s="T1605">kalam, </ts>
               <ts e="T1607" id="Seg_7822" n="e" s="T1606">(edəʔləm) </ts>
               <ts e="T1608" id="Seg_7824" n="e" s="T1607">dĭm. </ts>
            </ts>
            <ts e="T1612" id="Seg_7825" n="sc" s="T1609">
               <ts e="T1610" id="Seg_7827" n="e" s="T1609">Kamen </ts>
               <ts e="T1611" id="Seg_7829" n="e" s="T1610">dĭ </ts>
               <ts e="T1612" id="Seg_7831" n="e" s="T1611">šoləj. </ts>
            </ts>
            <ts e="T1627" id="Seg_7832" n="sc" s="T1619">
               <ts e="T1620" id="Seg_7834" n="e" s="T1619">Я </ts>
               <ts e="T1621" id="Seg_7836" n="e" s="T1620">завтра </ts>
               <ts e="T1622" id="Seg_7838" n="e" s="T1621">буду </ts>
               <ts e="T1623" id="Seg_7840" n="e" s="T1622">дожидать </ts>
               <ts e="T1624" id="Seg_7842" n="e" s="T1623">(э- </ts>
               <ts e="T1625" id="Seg_7844" n="e" s="T1624">этого=) </ts>
               <ts e="T1626" id="Seg_7846" n="e" s="T1625">(Ар-) </ts>
               <ts e="T1627" id="Seg_7848" n="e" s="T1626">Арпита. </ts>
            </ts>
            <ts e="T1643" id="Seg_7849" n="sc" s="T1628">
               <ts e="T1629" id="Seg_7851" n="e" s="T1628">Никуды </ts>
               <ts e="T1630" id="Seg_7853" n="e" s="T1629">не </ts>
               <ts e="T1631" id="Seg_7855" n="e" s="T1630">буду </ts>
               <ts e="T1632" id="Seg_7857" n="e" s="T1631">идти, </ts>
               <ts e="T1633" id="Seg_7859" n="e" s="T1632">он </ts>
               <ts e="T1634" id="Seg_7861" n="e" s="T1633">приедет </ts>
               <ts e="T1635" id="Seg_7863" n="e" s="T1634">и </ts>
               <ts e="T1636" id="Seg_7865" n="e" s="T1635">(пойд-) </ts>
               <ts e="T1637" id="Seg_7867" n="e" s="T1636">поедем </ts>
               <ts e="T1638" id="Seg_7869" n="e" s="T1637">(с-) </ts>
               <ts e="T1639" id="Seg_7871" n="e" s="T1638">с </ts>
               <ts e="T1640" id="Seg_7873" n="e" s="T1639">им </ts>
               <ts e="T1641" id="Seg_7875" n="e" s="T1640">к </ts>
               <ts e="T1642" id="Seg_7877" n="e" s="T1641">его </ts>
               <ts e="T1643" id="Seg_7879" n="e" s="T1642">матери. </ts>
            </ts>
            <ts e="T1646" id="Seg_7880" n="sc" s="T1644">
               <ts e="T1645" id="Seg_7882" n="e" s="T1644">Вот </ts>
               <ts e="T1646" id="Seg_7884" n="e" s="T1645">это. </ts>
            </ts>
            <ts e="T1649" id="Seg_7885" n="sc" s="T1647">
               <ts e="T1648" id="Seg_7887" n="e" s="T1647">((BRK)) </ts>
               <ts e="T1649" id="Seg_7889" n="e" s="T1648">коротенько. </ts>
            </ts>
            <ts e="T1659" id="Seg_7890" n="sc" s="T1650">
               <ts e="T1651" id="Seg_7892" n="e" s="T1650">Măn </ts>
               <ts e="T1652" id="Seg_7894" n="e" s="T1651">teinen </ts>
               <ts e="T1653" id="Seg_7896" n="e" s="T1652">(koum-) </ts>
               <ts e="T1654" id="Seg_7898" n="e" s="T1653">kunolbiam </ts>
               <ts e="T1655" id="Seg_7900" n="e" s="T1654">i </ts>
               <ts e="T1656" id="Seg_7902" n="e" s="T1655">dʼodənʼi </ts>
               <ts e="T1657" id="Seg_7904" n="e" s="T1656">kubiam </ts>
               <ts e="T1658" id="Seg_7906" n="e" s="T1657">onʼiʔ </ts>
               <ts e="T1659" id="Seg_7908" n="e" s="T1658">kuza. </ts>
            </ts>
            <ts e="T1666" id="Seg_7909" n="sc" s="T1660">
               <ts e="T1661" id="Seg_7911" n="e" s="T1660">Dĭ </ts>
               <ts e="T1662" id="Seg_7913" n="e" s="T1661">măna </ts>
               <ts e="T1663" id="Seg_7915" n="e" s="T1662">(k-) </ts>
               <ts e="T1664" id="Seg_7917" n="e" s="T1663">măndə: </ts>
               <ts e="T1665" id="Seg_7919" n="e" s="T1664">"Kanžəbəj </ts>
               <ts e="T1666" id="Seg_7921" n="e" s="T1665">gibər-nʼibudʼ". </ts>
            </ts>
            <ts e="T1671" id="Seg_7922" n="sc" s="T1667">
               <ts e="T1668" id="Seg_7924" n="e" s="T1667">Măn </ts>
               <ts e="T1669" id="Seg_7926" n="e" s="T1668">măndəm: </ts>
               <ts e="T1670" id="Seg_7928" n="e" s="T1669">"Ej </ts>
               <ts e="T1671" id="Seg_7930" n="e" s="T1670">kalam". </ts>
            </ts>
            <ts e="T1685" id="Seg_7931" n="sc" s="T1672">
               <ts e="T1673" id="Seg_7933" n="e" s="T1672">Я </ts>
               <ts e="T1674" id="Seg_7935" n="e" s="T1673">сегодня </ts>
               <ts e="T1675" id="Seg_7937" n="e" s="T1674">спала, </ts>
               <ts e="T1676" id="Seg_7939" n="e" s="T1675">видела </ts>
               <ts e="T1677" id="Seg_7941" n="e" s="T1676">во </ts>
               <ts e="T1678" id="Seg_7943" n="e" s="T1677">сне — </ts>
               <ts e="T1679" id="Seg_7945" n="e" s="T1678">один </ts>
               <ts e="T1680" id="Seg_7947" n="e" s="T1679">человек </ts>
               <ts e="T1681" id="Seg_7949" n="e" s="T1680">зовет </ts>
               <ts e="T1682" id="Seg_7951" n="e" s="T1681">меня: </ts>
               <ts e="T1683" id="Seg_7953" n="e" s="T1682">"Пойдем </ts>
               <ts e="T1684" id="Seg_7955" n="e" s="T1683">со </ts>
               <ts e="T1685" id="Seg_7957" n="e" s="T1684">мной". </ts>
            </ts>
            <ts e="T1691" id="Seg_7958" n="sc" s="T1686">
               <ts e="T1687" id="Seg_7960" n="e" s="T1686">А </ts>
               <ts e="T1688" id="Seg_7962" n="e" s="T1687">я </ts>
               <ts e="T1689" id="Seg_7964" n="e" s="T1688">говорю: </ts>
               <ts e="T1690" id="Seg_7966" n="e" s="T1689">"Не </ts>
               <ts e="T1691" id="Seg_7968" n="e" s="T1690">пойду". </ts>
            </ts>
            <ts e="T1694" id="Seg_7969" n="sc" s="T1692">
               <ts e="T1693" id="Seg_7971" n="e" s="T1692">Кончилось, </ts>
               <ts e="T1694" id="Seg_7973" n="e" s="T1693">нет? </ts>
            </ts>
            <ts e="T1696" id="Seg_7974" n="sc" s="T1695">
               <ts e="T1696" id="Seg_7976" n="e" s="T1695">((BRK)). </ts>
            </ts>
            <ts e="T1706" id="Seg_7977" n="sc" s="T1697">
               <ts e="T1698" id="Seg_7979" n="e" s="T1697">Măn </ts>
               <ts e="T1699" id="Seg_7981" n="e" s="T1698">kunolbiam </ts>
               <ts e="T1700" id="Seg_7983" n="e" s="T1699">taldʼen </ts>
               <ts e="T1701" id="Seg_7985" n="e" s="T1700">i </ts>
               <ts e="T1702" id="Seg_7987" n="e" s="T1701">dʼodənʼi </ts>
               <ts e="T1703" id="Seg_7989" n="e" s="T1702">kubiom </ts>
               <ts e="T1704" id="Seg_7991" n="e" s="T1703">bostə </ts>
               <ts e="T1705" id="Seg_7993" n="e" s="T1704">sʼestranə </ts>
               <ts e="T1706" id="Seg_7995" n="e" s="T1705">tibi. </ts>
            </ts>
            <ts e="T1712" id="Seg_7996" n="sc" s="T1707">
               <ts e="T1708" id="Seg_7998" n="e" s="T1707">(De-) </ts>
               <ts e="T1709" id="Seg_8000" n="e" s="T1708">Dĭgəttə </ts>
               <ts e="T1710" id="Seg_8002" n="e" s="T1709">kambiam </ts>
               <ts e="T1711" id="Seg_8004" n="e" s="T1710">šiʔnileʔ, </ts>
               <ts e="T1712" id="Seg_8006" n="e" s="T1711">dʼăbaktərbiam. </ts>
            </ts>
            <ts e="T1719" id="Seg_8007" n="sc" s="T1713">
               <ts e="T1714" id="Seg_8009" n="e" s="T1713">(Măl-) </ts>
               <ts e="T1715" id="Seg_8011" n="e" s="T1714">Dĭgəttə </ts>
               <ts e="T1716" id="Seg_8013" n="e" s="T1715">mămbiam: </ts>
               <ts e="T1717" id="Seg_8015" n="e" s="T1716">šindidə </ts>
               <ts e="T1718" id="Seg_8017" n="e" s="T1717">dʼăbaktərləj </ts>
               <ts e="T1719" id="Seg_8019" n="e" s="T1718">mănzi. </ts>
            </ts>
            <ts e="T1728" id="Seg_8020" n="sc" s="T1720">
               <ts e="T1721" id="Seg_8022" n="e" s="T1720">Dĭgəttə </ts>
               <ts e="T1722" id="Seg_8024" n="e" s="T1721">(koŋg-) </ts>
               <ts e="T1725" id="Seg_8026" n="e" s="T1722">koŋ </ts>
               <ts e="T1726" id="Seg_8028" n="e" s="T1725">šobiʔi </ts>
               <ts e="T1727" id="Seg_8030" n="e" s="T1726">dʼăbaktərzittə </ts>
               <ts e="T1728" id="Seg_8032" n="e" s="T1727">mănzi. </ts>
            </ts>
            <ts e="T1731" id="Seg_8033" n="sc" s="T1729">
               <ts e="T1730" id="Seg_8035" n="e" s="T1729">Dĭgəttə </ts>
               <ts e="T1731" id="Seg_8037" n="e" s="T1730">dĭn… </ts>
            </ts>
            <ts e="T1738" id="Seg_8038" n="sc" s="T1732">
               <ts e="T1733" id="Seg_8040" n="e" s="T1732">Я </ts>
               <ts e="T1734" id="Seg_8042" n="e" s="T1733">видала </ts>
               <ts e="T1735" id="Seg_8044" n="e" s="T1734">во </ts>
               <ts e="T1736" id="Seg_8046" n="e" s="T1735">сне </ts>
               <ts e="T1737" id="Seg_8048" n="e" s="T1736">своего </ts>
               <ts e="T1738" id="Seg_8050" n="e" s="T1737">зятя. </ts>
            </ts>
            <ts e="T1742" id="Seg_8051" n="sc" s="T1739">
               <ts e="T1740" id="Seg_8053" n="e" s="T1739">И </ts>
               <ts e="T1741" id="Seg_8055" n="e" s="T1740">пошла </ts>
               <ts e="T1742" id="Seg_8057" n="e" s="T1741">туды. </ts>
            </ts>
            <ts e="T1750" id="Seg_8058" n="sc" s="T1743">
               <ts e="T1744" id="Seg_8060" n="e" s="T1743">Там </ts>
               <ts e="T1745" id="Seg_8062" n="e" s="T1744">приехали </ts>
               <ts e="T1746" id="Seg_8064" n="e" s="T1745">начальники, </ts>
               <ts e="T1747" id="Seg_8066" n="e" s="T1746">и </ts>
               <ts e="T1748" id="Seg_8068" n="e" s="T1747">с </ts>
               <ts e="T1749" id="Seg_8070" n="e" s="T1748">имя </ts>
               <ts e="T1750" id="Seg_8072" n="e" s="T1749">разговаривала. </ts>
            </ts>
            <ts e="T1763" id="Seg_8073" n="sc" s="T1751">
               <ts e="T1752" id="Seg_8075" n="e" s="T1751">И </ts>
               <ts e="T1753" id="Seg_8077" n="e" s="T1752">еще </ts>
               <ts e="T1754" id="Seg_8079" n="e" s="T1753">и </ts>
               <ts e="T1755" id="Seg_8081" n="e" s="T1754">дома </ts>
               <ts e="T1756" id="Seg_8083" n="e" s="T1755">сказала: </ts>
               <ts e="T1757" id="Seg_8085" n="e" s="T1756">"(С </ts>
               <ts e="T1758" id="Seg_8087" n="e" s="T1757">к-) </ts>
               <ts e="T1759" id="Seg_8089" n="e" s="T1758">С </ts>
               <ts e="T1760" id="Seg_8091" n="e" s="T1759">каким-то </ts>
               <ts e="T1761" id="Seg_8093" n="e" s="T1760">начальником </ts>
               <ts e="T1762" id="Seg_8095" n="e" s="T1761">буду </ts>
               <ts e="T1763" id="Seg_8097" n="e" s="T1762">разговаривать". </ts>
            </ts>
            <ts e="T1770" id="Seg_8098" n="sc" s="T1764">
               <ts e="T1765" id="Seg_8100" n="e" s="T1764">Вот </ts>
               <ts e="T1766" id="Seg_8102" n="e" s="T1765">это </ts>
               <ts e="T1767" id="Seg_8104" n="e" s="T1766">я </ts>
               <ts e="T1768" id="Seg_8106" n="e" s="T1767">снила </ts>
               <ts e="T1769" id="Seg_8108" n="e" s="T1768">сон </ts>
               <ts e="T1770" id="Seg_8110" n="e" s="T1769">такой. </ts>
            </ts>
            <ts e="T1809" id="Seg_8111" n="sc" s="T1799">
               <ts e="T1800" id="Seg_8113" n="e" s="T1799">Miʔ </ts>
               <ts e="T1801" id="Seg_8115" n="e" s="T1800">mĭmbibeʔ. </ts>
               <ts e="T1802" id="Seg_8117" n="e" s="T1801">Dĭ </ts>
               <ts e="T1803" id="Seg_8119" n="e" s="T1802">nüke </ts>
               <ts e="T1804" id="Seg_8121" n="e" s="T1803">ibi </ts>
               <ts e="T1805" id="Seg_8123" n="e" s="T1804">dʼala, </ts>
               <ts e="T1806" id="Seg_8125" n="e" s="T1805">kamen </ts>
               <ts e="T1807" id="Seg_8127" n="e" s="T1806">dĭ </ts>
               <ts e="T1808" id="Seg_8129" n="e" s="T1807">iat </ts>
               <ts e="T1809" id="Seg_8131" n="e" s="T1808">deʔpi. </ts>
            </ts>
            <ts e="T1816" id="Seg_8132" n="sc" s="T1810">
               <ts e="T1811" id="Seg_8134" n="e" s="T1810">Măn </ts>
               <ts e="T1812" id="Seg_8136" n="e" s="T1811">mĭmbiem, </ts>
               <ts e="T1813" id="Seg_8138" n="e" s="T1812">nagur </ts>
               <ts e="T1814" id="Seg_8140" n="e" s="T1813">aktʼa </ts>
               <ts e="T1815" id="Seg_8142" n="e" s="T1814">dĭʔnə </ts>
               <ts e="T1816" id="Seg_8144" n="e" s="T1815">mĭbiem. </ts>
            </ts>
            <ts e="T1823" id="Seg_8145" n="sc" s="T1817">
               <ts e="T1818" id="Seg_8147" n="e" s="T1817">Dĭ </ts>
               <ts e="T1819" id="Seg_8149" n="e" s="T1818">(kădeš-) </ts>
               <ts e="T1820" id="Seg_8151" n="e" s="T1819">(miʔnʼibeʔ) </ts>
               <ts e="T1821" id="Seg_8153" n="e" s="T1820">bădəbi, </ts>
               <ts e="T1822" id="Seg_8155" n="e" s="T1821">munujʔ </ts>
               <ts e="T1823" id="Seg_8157" n="e" s="T1822">embi. </ts>
            </ts>
            <ts e="T1827" id="Seg_8158" n="sc" s="T1824">
               <ts e="T1825" id="Seg_8160" n="e" s="T1824">Kajaʔ </ts>
               <ts e="T1826" id="Seg_8162" n="e" s="T1825">embi, </ts>
               <ts e="T1827" id="Seg_8164" n="e" s="T1826">köbərgen. </ts>
            </ts>
            <ts e="T1831" id="Seg_8165" n="sc" s="T1828">
               <ts e="T1829" id="Seg_8167" n="e" s="T1828">Măndə: </ts>
               <ts e="T1830" id="Seg_8169" n="e" s="T1829">"Amaʔ, </ts>
               <ts e="T1831" id="Seg_8171" n="e" s="T1830">amaʔ". </ts>
            </ts>
            <ts e="T1835" id="Seg_8172" n="sc" s="T1832">
               <ts e="T1833" id="Seg_8174" n="e" s="T1832">Măn </ts>
               <ts e="T1834" id="Seg_8176" n="e" s="T1833">ambiam </ts>
               <ts e="T1835" id="Seg_8178" n="e" s="T1834">idʼiʔeʔe. </ts>
            </ts>
            <ts e="T1842" id="Seg_8179" n="sc" s="T1836">
               <ts e="T1837" id="Seg_8181" n="e" s="T1836">Dĭgəttə </ts>
               <ts e="T1838" id="Seg_8183" n="e" s="T1837">onʼiʔ </ts>
               <ts e="T1839" id="Seg_8185" n="e" s="T1838">takše </ts>
               <ts e="T1840" id="Seg_8187" n="e" s="T1839">segi </ts>
               <ts e="T1841" id="Seg_8189" n="e" s="T1840">bü </ts>
               <ts e="T1842" id="Seg_8191" n="e" s="T1841">biʔpiem. </ts>
            </ts>
            <ts e="T1845" id="Seg_8192" n="sc" s="T1843">
               <ts e="T1844" id="Seg_8194" n="e" s="T1843">Mămbiam: </ts>
               <ts e="T1845" id="Seg_8196" n="e" s="T1844">"Kabarləj". </ts>
            </ts>
            <ts e="T1860" id="Seg_8197" n="sc" s="T1850">
               <ts e="T1851" id="Seg_8199" n="e" s="T1850">Тогда </ts>
               <ts e="T1852" id="Seg_8201" n="e" s="T1851">она </ts>
               <ts e="T1853" id="Seg_8203" n="e" s="T1852">сказала, </ts>
               <ts e="T1854" id="Seg_8205" n="e" s="T1853">что </ts>
               <ts e="T1855" id="Seg_8207" n="e" s="T1854">"Пей </ts>
               <ts e="T1856" id="Seg_8209" n="e" s="T1855">чай", </ts>
               <ts e="T1857" id="Seg_8211" n="e" s="T1856">а </ts>
               <ts e="T1858" id="Seg_8213" n="e" s="T1857">я </ts>
               <ts e="T1859" id="Seg_8215" n="e" s="T1858">говорю: </ts>
               <ts e="T1860" id="Seg_8217" n="e" s="T1859">"Хватит". </ts>
            </ts>
            <ts e="T1864" id="Seg_8218" n="sc" s="T1861">
               <ts e="T1862" id="Seg_8220" n="e" s="T1861">Собрала </ts>
               <ts e="T1863" id="Seg_8222" n="e" s="T1862">на </ts>
               <ts e="T1864" id="Seg_8224" n="e" s="T1863">(стол). </ts>
            </ts>
            <ts e="T1866" id="Seg_8225" n="sc" s="T1865">
               <ts e="T1866" id="Seg_8227" n="e" s="T1865">Господи! </ts>
            </ts>
            <ts e="T1873" id="Seg_8228" n="sc" s="T1867">
               <ts e="T1868" id="Seg_8230" n="e" s="T1867">Пришли </ts>
               <ts e="T1869" id="Seg_8232" n="e" s="T1868">к </ts>
               <ts e="T1870" id="Seg_8234" n="e" s="T1869">ей, </ts>
               <ts e="T1871" id="Seg_8236" n="e" s="T1870">она </ts>
               <ts e="T1872" id="Seg_8238" n="e" s="T1871">была </ts>
               <ts e="T1873" id="Seg_8240" n="e" s="T1872">именинница. </ts>
            </ts>
            <ts e="T1877" id="Seg_8241" n="sc" s="T1874">
               <ts e="T1875" id="Seg_8243" n="e" s="T1874">Принесли </ts>
               <ts e="T1876" id="Seg_8245" n="e" s="T1875">ей </ts>
               <ts e="T1877" id="Seg_8247" n="e" s="T1876">подарки. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T6" id="Seg_8248" s="T1">PKZ_19700819_09343-1a.PKZ.001 (001)</ta>
            <ta e="T22" id="Seg_8249" s="T16">PKZ_19700819_09343-1a.PKZ.002 (006)</ta>
            <ta e="T31" id="Seg_8250" s="T23">PKZ_19700819_09343-1a.PKZ.003 (007)</ta>
            <ta e="T40" id="Seg_8251" s="T32">PKZ_19700819_09343-1a.PKZ.004 (008)</ta>
            <ta e="T48" id="Seg_8252" s="T41">PKZ_19700819_09343-1a.PKZ.005 (009)</ta>
            <ta e="T53" id="Seg_8253" s="T49">PKZ_19700819_09343-1a.PKZ.006 (010)</ta>
            <ta e="T58" id="Seg_8254" s="T54">PKZ_19700819_09343-1a.PKZ.007 (011)</ta>
            <ta e="T65" id="Seg_8255" s="T59">PKZ_19700819_09343-1a.PKZ.008 (012)</ta>
            <ta e="T69" id="Seg_8256" s="T68">PKZ_19700819_09343-1a.PKZ.009 (014)</ta>
            <ta e="T82" id="Seg_8257" s="T77">PKZ_19700819_09343-1a.PKZ.010 (016)</ta>
            <ta e="T92" id="Seg_8258" s="T84">PKZ_19700819_09343-1a.PKZ.011 (018)</ta>
            <ta e="T118" id="Seg_8259" s="T96">PKZ_19700819_09343-1a.PKZ.012 (021)</ta>
            <ta e="T133" id="Seg_8260" s="T120">PKZ_19700819_09343-1a.PKZ.013 (023)</ta>
            <ta e="T137" id="Seg_8261" s="T134">PKZ_19700819_09343-1a.PKZ.014 (026)</ta>
            <ta e="T141" id="Seg_8262" s="T138">PKZ_19700819_09343-1a.PKZ.015 (027)</ta>
            <ta e="T149" id="Seg_8263" s="T144">PKZ_19700819_09343-1a.PKZ.016 (029)</ta>
            <ta e="T160" id="Seg_8264" s="T149">PKZ_19700819_09343-1a.PKZ.017 (030)</ta>
            <ta e="T181" id="Seg_8265" s="T173">PKZ_19700819_09343-1a.PKZ.018 (034)</ta>
            <ta e="T184" id="Seg_8266" s="T182">PKZ_19700819_09343-1a.PKZ.019 (035)</ta>
            <ta e="T194" id="Seg_8267" s="T187">PKZ_19700819_09343-1a.PKZ.020 (037)</ta>
            <ta e="T207" id="Seg_8268" s="T195">PKZ_19700819_09343-1a.PKZ.021 (038)</ta>
            <ta e="T224" id="Seg_8269" s="T208">PKZ_19700819_09343-1a.PKZ.022 (039)</ta>
            <ta e="T227" id="Seg_8270" s="T225">PKZ_19700819_09343-1a.PKZ.023 (041)</ta>
            <ta e="T231" id="Seg_8271" s="T228">PKZ_19700819_09343-1a.PKZ.024 (042)</ta>
            <ta e="T238" id="Seg_8272" s="T232">PKZ_19700819_09343-1a.PKZ.025 (043)</ta>
            <ta e="T242" id="Seg_8273" s="T240">PKZ_19700819_09343-1a.PKZ.026 (045)</ta>
            <ta e="T243" id="Seg_8274" s="T242">PKZ_19700819_09343-1a.PKZ.027 (046)</ta>
            <ta e="T249" id="Seg_8275" s="T244">PKZ_19700819_09343-1a.PKZ.028 (047)</ta>
            <ta e="T251" id="Seg_8276" s="T250">PKZ_19700819_09343-1a.PKZ.029 (048)</ta>
            <ta e="T254" id="Seg_8277" s="T252">PKZ_19700819_09343-1a.PKZ.030 (049)</ta>
            <ta e="T258" id="Seg_8278" s="T255">PKZ_19700819_09343-1a.PKZ.031 (050)</ta>
            <ta e="T261" id="Seg_8279" s="T258">PKZ_19700819_09343-1a.PKZ.032 (051)</ta>
            <ta e="T263" id="Seg_8280" s="T261">PKZ_19700819_09343-1a.PKZ.033 (052)</ta>
            <ta e="T266" id="Seg_8281" s="T264">PKZ_19700819_09343-1a.PKZ.034 (053)</ta>
            <ta e="T268" id="Seg_8282" s="T266">PKZ_19700819_09343-1a.PKZ.035 (054)</ta>
            <ta e="T274" id="Seg_8283" s="T269">PKZ_19700819_09343-1a.PKZ.036 (055)</ta>
            <ta e="T280" id="Seg_8284" s="T275">PKZ_19700819_09343-1a.PKZ.037 (056)</ta>
            <ta e="T300" id="Seg_8285" s="T280">PKZ_19700819_09343-1a.PKZ.038 (058)</ta>
            <ta e="T314" id="Seg_8286" s="T306">PKZ_19700819_09343-1a.PKZ.039 (061)</ta>
            <ta e="T336" id="Seg_8287" s="T322">PKZ_19700819_09343-1a.PKZ.040 (063)</ta>
            <ta e="T342" id="Seg_8288" s="T337">PKZ_19700819_09343-1a.PKZ.041 (065)</ta>
            <ta e="T346" id="Seg_8289" s="T342">PKZ_19700819_09343-1a.PKZ.042 (066)</ta>
            <ta e="T349" id="Seg_8290" s="T347">PKZ_19700819_09343-1a.PKZ.043 (067)</ta>
            <ta e="T352" id="Seg_8291" s="T349">PKZ_19700819_09343-1a.PKZ.044 (068)</ta>
            <ta e="T358" id="Seg_8292" s="T353">PKZ_19700819_09343-1a.PKZ.045 (069)</ta>
            <ta e="T363" id="Seg_8293" s="T359">PKZ_19700819_09343-1a.PKZ.046 (070)</ta>
            <ta e="T368" id="Seg_8294" s="T363">PKZ_19700819_09343-1a.PKZ.047 (071)</ta>
            <ta e="T375" id="Seg_8295" s="T370">PKZ_19700819_09343-1a.PKZ.048 (073)</ta>
            <ta e="T385" id="Seg_8296" s="T375">PKZ_19700819_09343-1a.PKZ.049 (075)</ta>
            <ta e="T391" id="Seg_8297" s="T386">PKZ_19700819_09343-1a.PKZ.050 (076)</ta>
            <ta e="T394" id="Seg_8298" s="T393">PKZ_19700819_09343-1a.PKZ.051 (078)</ta>
            <ta e="T400" id="Seg_8299" s="T395">PKZ_19700819_09343-1a.PKZ.052 (079)</ta>
            <ta e="T405" id="Seg_8300" s="T401">PKZ_19700819_09343-1a.PKZ.053 (080)</ta>
            <ta e="T410" id="Seg_8301" s="T406">PKZ_19700819_09343-1a.PKZ.054 (081)</ta>
            <ta e="T413" id="Seg_8302" s="T411">PKZ_19700819_09343-1a.PKZ.055 (082)</ta>
            <ta e="T416" id="Seg_8303" s="T413">PKZ_19700819_09343-1a.PKZ.056 (083)</ta>
            <ta e="T424" id="Seg_8304" s="T417">PKZ_19700819_09343-1a.PKZ.057 (084) </ta>
            <ta e="T430" id="Seg_8305" s="T425">PKZ_19700819_09343-1a.PKZ.058 (086)</ta>
            <ta e="T437" id="Seg_8306" s="T431">PKZ_19700819_09343-1a.PKZ.059 (087)</ta>
            <ta e="T441" id="Seg_8307" s="T438">PKZ_19700819_09343-1a.PKZ.060 (088)</ta>
            <ta e="T447" id="Seg_8308" s="T442">PKZ_19700819_09343-1a.PKZ.061 (089)</ta>
            <ta e="T452" id="Seg_8309" s="T448">PKZ_19700819_09343-1a.PKZ.062 (090)</ta>
            <ta e="T463" id="Seg_8310" s="T453">PKZ_19700819_09343-1a.PKZ.063 (091)</ta>
            <ta e="T471" id="Seg_8311" s="T464">PKZ_19700819_09343-1a.PKZ.064 (092)</ta>
            <ta e="T487" id="Seg_8312" s="T472">PKZ_19700819_09343-1a.PKZ.065 (093)</ta>
            <ta e="T493" id="Seg_8313" s="T488">PKZ_19700819_09343-1a.PKZ.066 (094)</ta>
            <ta e="T506" id="Seg_8314" s="T494">PKZ_19700819_09343-1a.PKZ.067 (095)</ta>
            <ta e="T511" id="Seg_8315" s="T507">PKZ_19700819_09343-1a.PKZ.068 (096)</ta>
            <ta e="T528" id="Seg_8316" s="T512">PKZ_19700819_09343-1a.PKZ.069 (097)</ta>
            <ta e="T540" id="Seg_8317" s="T528">PKZ_19700819_09343-1a.PKZ.070 (098) </ta>
            <ta e="T550" id="Seg_8318" s="T541">PKZ_19700819_09343-1a.PKZ.071 (100)</ta>
            <ta e="T558" id="Seg_8319" s="T551">PKZ_19700819_09343-1a.PKZ.072 (101)</ta>
            <ta e="T569" id="Seg_8320" s="T559">PKZ_19700819_09343-1a.PKZ.073 (102)</ta>
            <ta e="T571" id="Seg_8321" s="T570">PKZ_19700819_09343-1a.PKZ.074 (103)</ta>
            <ta e="T582" id="Seg_8322" s="T257">PKZ_19700819_09343-1a.PKZ.075 (104)</ta>
            <ta e="T588" id="Seg_8323" s="T583">PKZ_19700819_09343-1a.PKZ.076 (105)</ta>
            <ta e="T595" id="Seg_8324" s="T589">PKZ_19700819_09343-1a.PKZ.077 (106)</ta>
            <ta e="T603" id="Seg_8325" s="T596">PKZ_19700819_09343-1a.PKZ.078 (107)</ta>
            <ta e="T610" id="Seg_8326" s="T604">PKZ_19700819_09343-1a.PKZ.079 (108)</ta>
            <ta e="T617" id="Seg_8327" s="T611">PKZ_19700819_09343-1a.PKZ.080 (109)</ta>
            <ta e="T626" id="Seg_8328" s="T618">PKZ_19700819_09343-1a.PKZ.081 (110)</ta>
            <ta e="T671" id="Seg_8329" s="T665">PKZ_19700819_09343-1a.PKZ.082 (114)</ta>
            <ta e="T683" id="Seg_8330" s="T672">PKZ_19700819_09343-1a.PKZ.083 (115)</ta>
            <ta e="T723" id="Seg_8331" s="T719">PKZ_19700819_09343-1a.PKZ.084 (120)</ta>
            <ta e="T729" id="Seg_8332" s="T724">PKZ_19700819_09343-1a.PKZ.085 (121)</ta>
            <ta e="T756" id="Seg_8333" s="T743">PKZ_19700819_09343-1a.PKZ.086 (123)</ta>
            <ta e="T769" id="Seg_8334" s="T757">PKZ_19700819_09343-1a.PKZ.087 (124)</ta>
            <ta e="T786" id="Seg_8335" s="T770">PKZ_19700819_09343-1a.PKZ.088 (125)</ta>
            <ta e="T791" id="Seg_8336" s="T787">PKZ_19700819_09343-1a.PKZ.089 (126)</ta>
            <ta e="T797" id="Seg_8337" s="T792">PKZ_19700819_09343-1a.PKZ.090 (127)</ta>
            <ta e="T802" id="Seg_8338" s="T798">PKZ_19700819_09343-1a.PKZ.091 (128)</ta>
            <ta e="T809" id="Seg_8339" s="T802">PKZ_19700819_09343-1a.PKZ.092 (129)</ta>
            <ta e="T819" id="Seg_8340" s="T810">PKZ_19700819_09343-1a.PKZ.093 (130)</ta>
            <ta e="T831" id="Seg_8341" s="T820">PKZ_19700819_09343-1a.PKZ.094 (131)</ta>
            <ta e="T841" id="Seg_8342" s="T832">PKZ_19700819_09343-1a.PKZ.095 (132)</ta>
            <ta e="T845" id="Seg_8343" s="T842">PKZ_19700819_09343-1a.PKZ.096 (133)</ta>
            <ta e="T857" id="Seg_8344" s="T846">PKZ_19700819_09343-1a.PKZ.097 (134)</ta>
            <ta e="T868" id="Seg_8345" s="T858">PKZ_19700819_09343-1a.PKZ.098 (135)</ta>
            <ta e="T875" id="Seg_8346" s="T869">PKZ_19700819_09343-1a.PKZ.099 (136)</ta>
            <ta e="T878" id="Seg_8347" s="T875">PKZ_19700819_09343-1a.PKZ.100 (137)</ta>
            <ta e="T895" id="Seg_8348" s="T879">PKZ_19700819_09343-1a.PKZ.101 (138)</ta>
            <ta e="T902" id="Seg_8349" s="T896">PKZ_19700819_09343-1a.PKZ.102 (139)</ta>
            <ta e="T912" id="Seg_8350" s="T903">PKZ_19700819_09343-1a.PKZ.103 (140)</ta>
            <ta e="T918" id="Seg_8351" s="T913">PKZ_19700819_09343-1a.PKZ.104 (141)</ta>
            <ta e="T926" id="Seg_8352" s="T919">PKZ_19700819_09343-1a.PKZ.105 (142)</ta>
            <ta e="T937" id="Seg_8353" s="T927">PKZ_19700819_09343-1a.PKZ.106 (143)</ta>
            <ta e="T948" id="Seg_8354" s="T937">PKZ_19700819_09343-1a.PKZ.107 (144)</ta>
            <ta e="T955" id="Seg_8355" s="T948">PKZ_19700819_09343-1a.PKZ.108 (145)</ta>
            <ta e="T961" id="Seg_8356" s="T956">PKZ_19700819_09343-1a.PKZ.109 (146)</ta>
            <ta e="T964" id="Seg_8357" s="T962">PKZ_19700819_09343-1a.PKZ.110 (147)</ta>
            <ta e="T973" id="Seg_8358" s="T965">PKZ_19700819_09343-1a.PKZ.111 (148)</ta>
            <ta e="T980" id="Seg_8359" s="T974">PKZ_19700819_09343-1a.PKZ.112 (149)</ta>
            <ta e="T989" id="Seg_8360" s="T981">PKZ_19700819_09343-1a.PKZ.113 (150)</ta>
            <ta e="T1008" id="Seg_8361" s="T990">PKZ_19700819_09343-1a.PKZ.114 (151)</ta>
            <ta e="T1013" id="Seg_8362" s="T1009">PKZ_19700819_09343-1a.PKZ.115 (152)</ta>
            <ta e="T1025" id="Seg_8363" s="T1014">PKZ_19700819_09343-1a.PKZ.116 (153)</ta>
            <ta e="T1034" id="Seg_8364" s="T1026">PKZ_19700819_09343-1a.PKZ.117 (154)</ta>
            <ta e="T1045" id="Seg_8365" s="T1035">PKZ_19700819_09343-1a.PKZ.118 (155)</ta>
            <ta e="T1055" id="Seg_8366" s="T1046">PKZ_19700819_09343-1a.PKZ.119 (156)</ta>
            <ta e="T1074" id="Seg_8367" s="T1056">PKZ_19700819_09343-1a.PKZ.120 (157)</ta>
            <ta e="T1081" id="Seg_8368" s="T1075">PKZ_19700819_09343-1a.PKZ.121 (158)</ta>
            <ta e="T1103" id="Seg_8369" s="T1090">PKZ_19700819_09343-1a.PKZ.122 (160)</ta>
            <ta e="T1119" id="Seg_8370" s="T1104">PKZ_19700819_09343-1a.PKZ.123 (161)</ta>
            <ta e="T1123" id="Seg_8371" s="T1120">PKZ_19700819_09343-1a.PKZ.124 (162)</ta>
            <ta e="T1134" id="Seg_8372" s="T1124">PKZ_19700819_09343-1a.PKZ.125 (163)</ta>
            <ta e="T1141" id="Seg_8373" s="T1135">PKZ_19700819_09343-1a.PKZ.126 (164)</ta>
            <ta e="T1146" id="Seg_8374" s="T1142">PKZ_19700819_09343-1a.PKZ.127 (165)</ta>
            <ta e="T1157" id="Seg_8375" s="T1147">PKZ_19700819_09343-1a.PKZ.128 (166)</ta>
            <ta e="T1161" id="Seg_8376" s="T1157">PKZ_19700819_09343-1a.PKZ.129 (167)</ta>
            <ta e="T1173" id="Seg_8377" s="T1162">PKZ_19700819_09343-1a.PKZ.130 (168)</ta>
            <ta e="T1178" id="Seg_8378" s="T1174">PKZ_19700819_09343-1a.PKZ.131 (169)</ta>
            <ta e="T1185" id="Seg_8379" s="T1179">PKZ_19700819_09343-1a.PKZ.132 (170)</ta>
            <ta e="T1195" id="Seg_8380" s="T1186">PKZ_19700819_09343-1a.PKZ.133 (171)</ta>
            <ta e="T1209" id="Seg_8381" s="T1196">PKZ_19700819_09343-1a.PKZ.134 (172)</ta>
            <ta e="T1217" id="Seg_8382" s="T1210">PKZ_19700819_09343-1a.PKZ.135 (173)</ta>
            <ta e="T1229" id="Seg_8383" s="T1217">PKZ_19700819_09343-1a.PKZ.136 (174)</ta>
            <ta e="T1242" id="Seg_8384" s="T1235">PKZ_19700819_09343-1a.PKZ.137 (177)</ta>
            <ta e="T1248" id="Seg_8385" s="T1242">PKZ_19700819_09343-1a.PKZ.138 (178)</ta>
            <ta e="T1250" id="Seg_8386" s="T1249">PKZ_19700819_09343-1a.PKZ.139 (179)</ta>
            <ta e="T1275" id="Seg_8387" s="T1262">PKZ_19700819_09343-1a.PKZ.140 (183)</ta>
            <ta e="T1284" id="Seg_8388" s="T1276">PKZ_19700819_09343-1a.PKZ.141 (184)</ta>
            <ta e="T1290" id="Seg_8389" s="T1284">PKZ_19700819_09343-1a.PKZ.142 (185)</ta>
            <ta e="T1295" id="Seg_8390" s="T1291">PKZ_19700819_09343-1a.PKZ.143 (186)</ta>
            <ta e="T1303" id="Seg_8391" s="T1296">PKZ_19700819_09343-1a.PKZ.144 (187)</ta>
            <ta e="T1314" id="Seg_8392" s="T1304">PKZ_19700819_09343-1a.PKZ.145 (188)</ta>
            <ta e="T1326" id="Seg_8393" s="T1315">PKZ_19700819_09343-1a.PKZ.146 (189)</ta>
            <ta e="T1334" id="Seg_8394" s="T1327">PKZ_19700819_09343-1a.PKZ.147 (190)</ta>
            <ta e="T1337" id="Seg_8395" s="T1335">PKZ_19700819_09343-1a.PKZ.148 (191)</ta>
            <ta e="T1340" id="Seg_8396" s="T1338">PKZ_19700819_09343-1a.PKZ.149 (192)</ta>
            <ta e="T1346" id="Seg_8397" s="T1341">PKZ_19700819_09343-1a.PKZ.150 (193)</ta>
            <ta e="T1351" id="Seg_8398" s="T1347">PKZ_19700819_09343-1a.PKZ.151 (194)</ta>
            <ta e="T1356" id="Seg_8399" s="T1352">PKZ_19700819_09343-1a.PKZ.152 (195)</ta>
            <ta e="T1361" id="Seg_8400" s="T1357">PKZ_19700819_09343-1a.PKZ.153 (196)</ta>
            <ta e="T1368" id="Seg_8401" s="T1362">PKZ_19700819_09343-1a.PKZ.154 (197)</ta>
            <ta e="T1380" id="Seg_8402" s="T1369">PKZ_19700819_09343-1a.PKZ.155 (198)</ta>
            <ta e="T1384" id="Seg_8403" s="T1380">PKZ_19700819_09343-1a.PKZ.156 (199)</ta>
            <ta e="T1388" id="Seg_8404" s="T1385">PKZ_19700819_09343-1a.PKZ.157 (200)</ta>
            <ta e="T1392" id="Seg_8405" s="T1388">PKZ_19700819_09343-1a.PKZ.158 (201)</ta>
            <ta e="T1397" id="Seg_8406" s="T1393">PKZ_19700819_09343-1a.PKZ.159 (202)</ta>
            <ta e="T1404" id="Seg_8407" s="T1398">PKZ_19700819_09343-1a.PKZ.160 (203)</ta>
            <ta e="T1410" id="Seg_8408" s="T1404">PKZ_19700819_09343-1a.PKZ.161 (204)</ta>
            <ta e="T1413" id="Seg_8409" s="T1411">PKZ_19700819_09343-1a.PKZ.162 (205)</ta>
            <ta e="T1418" id="Seg_8410" s="T1414">PKZ_19700819_09343-1a.PKZ.163 (206)</ta>
            <ta e="T1421" id="Seg_8411" s="T1419">PKZ_19700819_09343-1a.PKZ.164 (207)</ta>
            <ta e="T1426" id="Seg_8412" s="T1422">PKZ_19700819_09343-1a.PKZ.165 (208)</ta>
            <ta e="T1432" id="Seg_8413" s="T1427">PKZ_19700819_09343-1a.PKZ.166 (209)</ta>
            <ta e="T1439" id="Seg_8414" s="T1433">PKZ_19700819_09343-1a.PKZ.167 (210)</ta>
            <ta e="T1449" id="Seg_8415" s="T1440">PKZ_19700819_09343-1a.PKZ.168 (211)</ta>
            <ta e="T1452" id="Seg_8416" s="T1449">PKZ_19700819_09343-1a.PKZ.169 (212)</ta>
            <ta e="T1461" id="Seg_8417" s="T1453">PKZ_19700819_09343-1a.PKZ.170 (213)</ta>
            <ta e="T1463" id="Seg_8418" s="T1462">PKZ_19700819_09343-1a.PKZ.171 (214)</ta>
            <ta e="T1477" id="Seg_8419" s="T1464">PKZ_19700819_09343-1a.PKZ.172 (215)</ta>
            <ta e="T1480" id="Seg_8420" s="T1478">PKZ_19700819_09343-1a.PKZ.173 (216)</ta>
            <ta e="T1488" id="Seg_8421" s="T1484">PKZ_19700819_09343-1a.PKZ.174 (218)</ta>
            <ta e="T1492" id="Seg_8422" s="T1488">PKZ_19700819_09343-1a.PKZ.175 (220)</ta>
            <ta e="T1508" id="Seg_8423" s="T1495">PKZ_19700819_09343-1a.PKZ.176 (222)</ta>
            <ta e="T1512" id="Seg_8424" s="T1511">PKZ_19700819_09343-1a.PKZ.177 (224)</ta>
            <ta e="T1514" id="Seg_8425" s="T1513">PKZ_19700819_09343-1a.PKZ.178 (225)</ta>
            <ta e="T1521" id="Seg_8426" s="T1518">PKZ_19700819_09343-1a.PKZ.179 (227)</ta>
            <ta e="T1538" id="Seg_8427" s="T1529">PKZ_19700819_09343-1a.PKZ.180 (231)</ta>
            <ta e="T1543" id="Seg_8428" s="T1539">PKZ_19700819_09343-1a.PKZ.181 (232)</ta>
            <ta e="T1548" id="Seg_8429" s="T1544">PKZ_19700819_09343-1a.PKZ.182 (233)</ta>
            <ta e="T1552" id="Seg_8430" s="T1549">PKZ_19700819_09343-1a.PKZ.183 (235)</ta>
            <ta e="T1563" id="Seg_8431" s="T1562">PKZ_19700819_09343-1a.PKZ.184 (238)</ta>
            <ta e="T1568" id="Seg_8432" s="T1564">PKZ_19700819_09343-1a.PKZ.185 (239)</ta>
            <ta e="T1574" id="Seg_8433" s="T1571">PKZ_19700819_09343-1a.PKZ.186 (241)</ta>
            <ta e="T1579" id="Seg_8434" s="T1576">PKZ_19700819_09343-1a.PKZ.187 (242)</ta>
            <ta e="T1590" id="Seg_8435" s="T1586">PKZ_19700819_09343-1a.PKZ.188 (244)</ta>
            <ta e="T1600" id="Seg_8436" s="T1591">PKZ_19700819_09343-1a.PKZ.189 (245)</ta>
            <ta e="T1608" id="Seg_8437" s="T1601">PKZ_19700819_09343-1a.PKZ.190 (246)</ta>
            <ta e="T1612" id="Seg_8438" s="T1609">PKZ_19700819_09343-1a.PKZ.191 (247)</ta>
            <ta e="T1627" id="Seg_8439" s="T1619">PKZ_19700819_09343-1a.PKZ.192 (249)</ta>
            <ta e="T1643" id="Seg_8440" s="T1628">PKZ_19700819_09343-1a.PKZ.193 (250)</ta>
            <ta e="T1646" id="Seg_8441" s="T1644">PKZ_19700819_09343-1a.PKZ.194 (251)</ta>
            <ta e="T1649" id="Seg_8442" s="T1647">PKZ_19700819_09343-1a.PKZ.195 (252)</ta>
            <ta e="T1659" id="Seg_8443" s="T1650">PKZ_19700819_09343-1a.PKZ.196 (253)</ta>
            <ta e="T1666" id="Seg_8444" s="T1660">PKZ_19700819_09343-1a.PKZ.197 (254)</ta>
            <ta e="T1671" id="Seg_8445" s="T1667">PKZ_19700819_09343-1a.PKZ.198 (255)</ta>
            <ta e="T1685" id="Seg_8446" s="T1672">PKZ_19700819_09343-1a.PKZ.199 (256)</ta>
            <ta e="T1691" id="Seg_8447" s="T1686">PKZ_19700819_09343-1a.PKZ.200 (257)</ta>
            <ta e="T1694" id="Seg_8448" s="T1692">PKZ_19700819_09343-1a.PKZ.201 (258)</ta>
            <ta e="T1696" id="Seg_8449" s="T1695">PKZ_19700819_09343-1a.PKZ.202 (259)</ta>
            <ta e="T1706" id="Seg_8450" s="T1697">PKZ_19700819_09343-1a.PKZ.203 (260)</ta>
            <ta e="T1712" id="Seg_8451" s="T1707">PKZ_19700819_09343-1a.PKZ.204 (261)</ta>
            <ta e="T1719" id="Seg_8452" s="T1713">PKZ_19700819_09343-1a.PKZ.205 (262)</ta>
            <ta e="T1728" id="Seg_8453" s="T1720">PKZ_19700819_09343-1a.PKZ.206 (263)</ta>
            <ta e="T1731" id="Seg_8454" s="T1729">PKZ_19700819_09343-1a.PKZ.207 (265)</ta>
            <ta e="T1738" id="Seg_8455" s="T1732">PKZ_19700819_09343-1a.PKZ.208 (266)</ta>
            <ta e="T1742" id="Seg_8456" s="T1739">PKZ_19700819_09343-1a.PKZ.209 (267)</ta>
            <ta e="T1750" id="Seg_8457" s="T1743">PKZ_19700819_09343-1a.PKZ.210 (268)</ta>
            <ta e="T1763" id="Seg_8458" s="T1751">PKZ_19700819_09343-1a.PKZ.211 (269)</ta>
            <ta e="T1770" id="Seg_8459" s="T1764">PKZ_19700819_09343-1a.PKZ.212 (270)</ta>
            <ta e="T1801" id="Seg_8460" s="T1799">PKZ_19700819_09343-1a.PKZ.213 (276)</ta>
            <ta e="T1809" id="Seg_8461" s="T1801">PKZ_19700819_09343-1a.PKZ.214 (277)</ta>
            <ta e="T1816" id="Seg_8462" s="T1810">PKZ_19700819_09343-1a.PKZ.215 (278)</ta>
            <ta e="T1823" id="Seg_8463" s="T1817">PKZ_19700819_09343-1a.PKZ.216 (279)</ta>
            <ta e="T1827" id="Seg_8464" s="T1824">PKZ_19700819_09343-1a.PKZ.217 (280)</ta>
            <ta e="T1831" id="Seg_8465" s="T1828">PKZ_19700819_09343-1a.PKZ.218 (281)</ta>
            <ta e="T1835" id="Seg_8466" s="T1832">PKZ_19700819_09343-1a.PKZ.219 (282)</ta>
            <ta e="T1842" id="Seg_8467" s="T1836">PKZ_19700819_09343-1a.PKZ.220 (283)</ta>
            <ta e="T1845" id="Seg_8468" s="T1843">PKZ_19700819_09343-1a.PKZ.221 (284)</ta>
            <ta e="T1860" id="Seg_8469" s="T1850">PKZ_19700819_09343-1a.PKZ.222 (286)</ta>
            <ta e="T1864" id="Seg_8470" s="T1861">PKZ_19700819_09343-1a.PKZ.223 (287)</ta>
            <ta e="T1866" id="Seg_8471" s="T1865">PKZ_19700819_09343-1a.PKZ.224 (288)</ta>
            <ta e="T1873" id="Seg_8472" s="T1867">PKZ_19700819_09343-1a.PKZ.225 (289)</ta>
            <ta e="T1877" id="Seg_8473" s="T1874">PKZ_19700819_09343-1a.PKZ.226 (290)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T6" id="Seg_8474" s="T1">Кое-чего позабывала я. </ta>
            <ta e="T22" id="Seg_8475" s="T16">У моей племянницы дочка вышла замуж. </ta>
            <ta e="T31" id="Seg_8476" s="T23">И поехали туды по Мане строить железную дорогу. </ta>
            <ta e="T40" id="Seg_8477" s="T32">И оттедова (уех-) всё строили дотель до севера. </ta>
            <ta e="T48" id="Seg_8478" s="T41">И потом она приехала к матери, гостить. </ta>
            <ta e="T53" id="Seg_8479" s="T49">((BRK)) (-ла) его похоронила. </ta>
            <ta e="T58" id="Seg_8480" s="T54">(И тог-) Ну это… </ta>
            <ta e="T65" id="Seg_8481" s="T59">Я по-русски (ска-) сказать, опять забуду. </ta>
            <ta e="T69" id="Seg_8482" s="T68">((BRK)). </ta>
            <ta e="T82" id="Seg_8483" s="T77">У вас какой-то ветер ((NOISE)). </ta>
            <ta e="T92" id="Seg_8484" s="T84">У нас сухой ветер. </ta>
            <ta e="T118" id="Seg_8485" s="T96">Да, (ши-) шибко у вас ветер, у нас такого нету, как у (вас). </ta>
            <ta e="T133" id="Seg_8486" s="T120">Видала, только (с-) далеко, с далека. </ta>
            <ta e="T137" id="Seg_8487" s="T134">Там была. </ta>
            <ta e="T141" id="Seg_8488" s="T138">Где Маргарита толстая. </ta>
            <ta e="T149" id="Seg_8489" s="T144">За её проходила туды. </ta>
            <ta e="T160" id="Seg_8490" s="T149">Ну, видать так далеко, но близко не ходили, далеко. </ta>
            <ta e="T181" id="Seg_8491" s="T173">((DMG)), а вышло ли, нет, не знаю. </ta>
            <ta e="T184" id="Seg_8492" s="T182">По-русски говорить? </ta>
            <ta e="T194" id="Seg_8493" s="T187">У (м-) моёй племянницы дочка вышла замуж. </ta>
            <ta e="T207" id="Seg_8494" s="T195">(У н-) (А ты, говорит = А я говорю а вы как). </ta>
            <ta e="T224" id="Seg_8495" s="T208">Я, говорит, учительница Агафон Ивановича, ((DMG)) он мне рассказывает, ну я по-русски. </ta>
            <ta e="T227" id="Seg_8496" s="T225">Ага. </ta>
            <ta e="T231" id="Seg_8497" s="T228">Ну я ((…)). ((DMG))</ta>
            <ta e="T238" id="Seg_8498" s="T232">Тут посидели там, разговаривали ((DMG)). </ta>
            <ta e="T242" id="Seg_8499" s="T240">Ну. </ta>
            <ta e="T243" id="Seg_8500" s="T242">Там. </ta>
            <ta e="T249" id="Seg_8501" s="T244">Мой брат сродный туды ездил. </ta>
            <ta e="T251" id="Seg_8502" s="T250">((BRK)). </ta>
            <ta e="T254" id="Seg_8503" s="T252">Ну, это… </ta>
            <ta e="T258" id="Seg_8504" s="T255">Мальчишка ((…)). </ta>
            <ta e="T261" id="Seg_8505" s="T258">(никак) не выговорю. </ta>
            <ta e="T263" id="Seg_8506" s="T261">(Iže-) Izeŋ. </ta>
            <ta e="T266" id="Seg_8507" s="T264">(Iž-) Izeŋ. </ta>
            <ta e="T268" id="Seg_8508" s="T266">Мать где? </ta>
            <ta e="T274" id="Seg_8509" s="T269">Sargol (tura-) Sargol derʼevnʼanə barustan. </ta>
            <ta e="T280" id="Seg_8510" s="T275">В Сарголову деревню ушла. </ta>
            <ta e="T300" id="Seg_8511" s="T280">Я тот язык, вот так говорили, а плохо знаю я его. </ta>
            <ta e="T314" id="Seg_8512" s="T306">И на его… </ta>
            <ta e="T336" id="Seg_8513" s="T322">Ну вот, и уфимские есть татары. </ta>
            <ta e="T342" id="Seg_8514" s="T337">Казанские, они на этот язык. </ta>
            <ta e="T346" id="Seg_8515" s="T342">Вот, один раз приехали. </ta>
            <ta e="T349" id="Seg_8516" s="T347">Трое приехали. </ta>
            <ta e="T352" id="Seg_8517" s="T349">Кто там приехал? </ta>
            <ta e="T358" id="Seg_8518" s="T353">Говорит: бер жид, бер татарин. </ta>
            <ta e="T363" id="Seg_8519" s="T359">Один жид, один татарин. </ta>
            <ta e="T368" id="Seg_8520" s="T363">((…)) на тот язык. </ta>
            <ta e="T375" id="Seg_8521" s="T370">На тот язык. </ta>
            <ta e="T385" id="Seg_8522" s="T375">А по нашему: Onʼiʔ žid šobi, onʼiʔ nu kuza šobi. </ta>
            <ta e="T391" id="Seg_8523" s="T386">Один жид, один татарин. </ta>
            <ta e="T394" id="Seg_8524" s="T393">Nuzaŋ. </ta>
            <ta e="T400" id="Seg_8525" s="T395">Dĭgəttə plʼemʼannʼica măna noʔ kambi. </ta>
            <ta e="T405" id="Seg_8526" s="T401">Miʔ dĭ kopnaʔi embibeʔ. </ta>
            <ta e="T410" id="Seg_8527" s="T406">Dĭgəttə onʼiʔ (u-) üzəbi. </ta>
            <ta e="T413" id="Seg_8528" s="T411">Măn mămbiam… </ta>
            <ta e="T416" id="Seg_8529" s="T413">Mĭʔ nagurbəʔ ibibeʔ. </ta>
            <ta e="T424" id="Seg_8530" s="T417">Măn mămbiam: "No kut, kodu saʔməlupi". </ta>
            <ta e="T430" id="Seg_8531" s="T425">Măn mămbiam: "Šində-nʼibudʼ (kuna-) külalləj". </ta>
            <ta e="T437" id="Seg_8532" s="T431">Dĭgəttə măn mălliam: "Dĭ măn külalləm. </ta>
            <ta e="T441" id="Seg_8533" s="T438">Măn dʼaktə vedʼ. </ta>
            <ta e="T447" id="Seg_8534" s="T442">A šiʔ išo ej dʼaktə. </ta>
            <ta e="T452" id="Seg_8535" s="T448">(Dĭg-) Dĭgəttə maʔnʼibaʔ šobibaʔ. </ta>
            <ta e="T463" id="Seg_8536" s="T453">(Măn=) Kamen (dĭ=) dĭ šobi döbər, măn mĭmbiem, köbürgən nĭŋgəbiem. </ta>
            <ta e="T471" id="Seg_8537" s="T464">(Š-) Šomi pa, beržə nagobi, dʼünə saʔməluʔpi. </ta>
            <ta e="T487" id="Seg_8538" s="T472">(Măn=) Măn (u) šindinədə ej mămbiam, a dĭgəttə dĭn tibit kamen bügən külambi, măn mămbiam. </ta>
            <ta e="T493" id="Seg_8539" s="T488">Когда еёный муж утонул, я… </ta>
            <ta e="T506" id="Seg_8540" s="T494">Она у меня работала на покосе, копну поклала хорошую, это я рассказывала. </ta>
            <ta e="T511" id="Seg_8541" s="T507">Она стояла и упала. </ta>
            <ta e="T528" id="Seg_8542" s="T512">А нас было трое, (а я говорю=) а я (ска-) сказала: "Это кто-то из нас помрет". </ta>
            <ta e="T540" id="Seg_8543" s="T528">А тады думаю, говорю имя: "Это я помру, я старше вас". </ta>
            <ta e="T550" id="Seg_8544" s="T541">А тагды ходила я на поле, лук рвала, полевой. </ta>
            <ta e="T558" id="Seg_8545" s="T551">И ветру не было, листвяга упала, толстая! </ta>
            <ta e="T569" id="Seg_8546" s="T559">Я так подумала: кто-то помрет, и правда помер, вот этот. </ta>
            <ta e="T571" id="Seg_8547" s="T570">((BRK)). </ta>
            <ta e="T582" id="Seg_8548" s="T257">Kamen măn abam šobi, (dĭn turagən) dĭ turandə, Spasskaːnə, maːʔnʼi. </ta>
            <ta e="T588" id="Seg_8549" s="T583">Dĭgəttə šolaʔbə, šomi pa nulaʔbə. </ta>
            <ta e="T595" id="Seg_8550" s="T589">Dĭgəttə pagə сучок (üz-) dʼünə saʔməluʔpi. </ta>
            <ta e="T603" id="Seg_8551" s="T596">Dĭgəttə (šo-) dĭ šobi maːʔndə, măna nörbəlie. </ta>
            <ta e="T610" id="Seg_8552" s="T604">Măn (măn-) mămbiam: šindidə esseŋdə külalləj. </ta>
            <ta e="T617" id="Seg_8553" s="T611">I dĭgəttə dăre dĭ i ibi. </ta>
            <ta e="T626" id="Seg_8554" s="T618">(Măn=) Măn nʼim kambi bügən i dĭn külambi. </ta>
            <ta e="T671" id="Seg_8555" s="T665">Я забыла вчера, про это говорила. </ta>
            <ta e="T683" id="Seg_8556" s="T672">Про себя, я замуж шла, забыла, говорила ли, нет. </ta>
            <ta e="T723" id="Seg_8557" s="T719">Как мой муж утонул. </ta>
            <ta e="T729" id="Seg_8558" s="T724">Ну, это он говорит, нет? </ta>
            <ta e="T756" id="Seg_8559" s="T743">Ну, когда я была девушкой, Пьянково деревня была, там парень, звали его Алексеем. </ta>
            <ta e="T769" id="Seg_8560" s="T757">Мы с им дружили, (я=) я когда туды ездила, а он сюды. </ta>
            <ta e="T786" id="Seg_8561" s="T770">А потом у его (мать=) отец помер, а мать взяла замуж ушла, а он остался один. </ta>
            <ta e="T791" id="Seg_8562" s="T787">Ему семнадцать лет было. </ta>
            <ta e="T797" id="Seg_8563" s="T792">И мне семнадцать, молода была! </ta>
            <ta e="T802" id="Seg_8564" s="T798">Ну, и он приехал. </ta>
            <ta e="T809" id="Seg_8565" s="T802">(Я=) Я ему говорю: "Сватай, отдадут родители". </ta>
            <ta e="T819" id="Seg_8566" s="T810">Он говорит: "Нет, не отдадут, скажут: ты молодая, поедем!" </ta>
            <ta e="T831" id="Seg_8567" s="T820">И я собрала платье, связала узел, ночью оседлали коня и поехали. </ta>
            <ta e="T841" id="Seg_8568" s="T832">Не поехали тут прямой дорогой, а поехали через Агинско. </ta>
            <ta e="T845" id="Seg_8569" s="T842">Ночью ехали-ехали, ой. </ta>
            <ta e="T857" id="Seg_8570" s="T846">Потом приехали, там деревня (Ка-) Каптырлык, русский народ, там ночевали, оттель… </ta>
            <ta e="T868" id="Seg_8571" s="T858">И там считай целый день пробыли, он не ехал домой. </ta>
            <ta e="T875" id="Seg_8572" s="T869">Или совестился, (или) чего, не знаю. </ta>
            <ta e="T878" id="Seg_8573" s="T875">Потом приехали вечером. </ta>
            <ta e="T895" id="Seg_8574" s="T879">И он (рас-) расседлал коня, зашли в дом и тут спали, и там я жила, всё. </ta>
            <ta e="T902" id="Seg_8575" s="T896">А тагды приехал (е-) евонный сродственник. </ta>
            <ta e="T912" id="Seg_8576" s="T903">(Зва-) (О-) Он глаза потерял, не видел, дядя его. </ta>
            <ta e="T918" id="Seg_8577" s="T913">Сказал: приезжай помоги сено косить. </ta>
            <ta e="T926" id="Seg_8578" s="T919">Ему туды в нашу деревню, в Абалаков. </ta>
            <ta e="T937" id="Seg_8579" s="T927">(Тогда он=) Тогда он привез девушку там, с другой деревни. </ta>
            <ta e="T948" id="Seg_8580" s="T937">Ну, мне сказали, что он хочет ее брать, а тебя прогнать. </ta>
            <ta e="T955" id="Seg_8581" s="T948">Я собралась да с евонным дядем поехала. </ta>
            <ta e="T961" id="Seg_8582" s="T956">Он меня догнал и воротил. </ta>
            <ta e="T964" id="Seg_8583" s="T962">Назад обратно. </ta>
            <ta e="T973" id="Seg_8584" s="T965">Ну а потом после этого мама туды приехала. </ta>
            <ta e="T980" id="Seg_8585" s="T974">А я тады пошла к маме. </ta>
            <ta e="T989" id="Seg_8586" s="T981">А мама заехала к племяннику, который вот сейчас… </ta>
            <ta e="T1008" id="Seg_8587" s="T990">К еёному, ее племянник был, Иваном его звали, который сейчас в Пьянково его дочка живет, она мне племянница. </ta>
            <ta e="T1013" id="Seg_8588" s="T1009">Ну и мама это… </ta>
            <ta e="T1025" id="Seg_8589" s="T1014">Пришла, а он опять пришел, меня за руку взял и увел. </ta>
            <ta e="T1034" id="Seg_8590" s="T1026">Ну, потом мы легли, а тогда они пришли. </ta>
            <ta e="T1045" id="Seg_8591" s="T1035">"Если вы будете жить дак, мама говорит, я благословлю вас". </ta>
            <ta e="T1055" id="Seg_8592" s="T1046">Ну, тут благословила она нас (на завтре), тоже тут. </ta>
            <ta e="T1074" id="Seg_8593" s="T1056">К нам пришли, этот, брат сродный, невестка, мы тут чаю (п-) попили, они тогда ушли, и мама уехала. </ta>
            <ta e="T1081" id="Seg_8594" s="T1075">Ну, теперь можно по-камасински. </ta>
            <ta e="T1103" id="Seg_8595" s="T1090">Ну, потом (мы=) про это надо сказать, что он туды поехал. </ta>
            <ta e="T1119" id="Seg_8596" s="T1104">Тогда он меня оставил дома, не взял с собой, поехал туды к нам в Абалаково. </ta>
            <ta e="T1123" id="Seg_8597" s="T1120">Дяде сено косить. </ta>
            <ta e="T1134" id="Seg_8598" s="T1124">И как раз был день такой, (пра-) праздник Ивана Купала. </ta>
            <ta e="T1141" id="Seg_8599" s="T1135">Они по деревне ходили, обливались, обливалися. </ta>
            <ta e="T1146" id="Seg_8600" s="T1142">А тады собрались, пошли. </ta>
            <ta e="T1157" id="Seg_8601" s="T1147">Там еще ниже этой мельницы, где живут эстонцы, другая мельница. </ta>
            <ta e="T1161" id="Seg_8602" s="T1157">Пошли туды в пруд. </ta>
            <ta e="T1173" id="Seg_8603" s="T1162">И пошли, пошли, и глыбоко зашли, а там из земли родник. </ta>
            <ta e="T1178" id="Seg_8604" s="T1174">Они зачали двое тонуть. </ta>
            <ta e="T1185" id="Seg_8605" s="T1179">А с имя был один парень. </ta>
            <ta e="T1195" id="Seg_8606" s="T1186">Он подал жердь, эту выдернул, а его не стало. </ta>
            <ta e="T1209" id="Seg_8607" s="T1196">Пока он побежал в деревню, да отпустили воду, вытащили его, откачивали-откачивали, не откачали. </ta>
            <ta e="T1217" id="Seg_8608" s="T1210">А потом привезли туды мертвого и схоронили. </ta>
            <ta e="T1229" id="Seg_8609" s="T1217">(Я = я=) Я его хоронила, а оттель домой вернулась обратно, приехала. </ta>
            <ta e="T1242" id="Seg_8610" s="T1235">Ну, переживания, вот у меня голова трясется. </ta>
            <ta e="T1248" id="Seg_8611" s="T1242">Много печали в голове в моёй. </ta>
            <ta e="T1250" id="Seg_8612" s="T1249">Вот. </ta>
            <ta e="T1275" id="Seg_8613" s="T1262">Kamen dĭ šobi măna: "Kanžəbəj, dĭn iat tibinə kalla dʼürbi, măn unnʼa amnolaʔbiom". </ta>
            <ta e="T1284" id="Seg_8614" s="T1276">Măn mămbiam: "Kanaʔ, măna monoʔkoʔ abanə da ianə". </ta>
            <ta e="T1290" id="Seg_8615" s="T1284">Dĭ măndə: "Dĭzeŋ ej mĭləʔi, kanžəbəj!" </ta>
            <ta e="T1295" id="Seg_8616" s="T1291">Dĭgəttə măn oldʼam oʔbdəbiom. </ta>
            <ta e="T1303" id="Seg_8617" s="T1296">Nüdʼin inem kondlaʔpibaʔ, amnəbibaʔ, kambibaʔ Kazan turanə. </ta>
            <ta e="T1314" id="Seg_8618" s="T1304">Bar nüdʼi dĭn kunolbibaʔ, dĭgəttə šobibaʔ bazo (dĭʔnə) turanə, Kaptɨrlɨktə. </ta>
            <ta e="T1326" id="Seg_8619" s="T1315">Dĭn selaj (dʼal-) dʼala ibibeʔ, dĭgəttə nüdʼin šobibaʔ (dĭ=) dĭn maːʔnə. </ta>
            <ta e="T1334" id="Seg_8620" s="T1327">Dĭgəttə dĭ ertən šobi, dĭn kazak iat. </ta>
            <ta e="T1337" id="Seg_8621" s="T1335">Dĭgəttə uʔbdəbibaʔ. </ta>
            <ta e="T1340" id="Seg_8622" s="T1338">Dĭn amnobiam. </ta>
            <ta e="T1346" id="Seg_8623" s="T1341">(N-) Noʔ jaʔpibaʔ dĭn bar. </ta>
            <ta e="T1351" id="Seg_8624" s="T1347">Dĭgəttə dĭn dʼadʼat šobi. </ta>
            <ta e="T1356" id="Seg_8625" s="T1352">"Šoʔ măna noʔ jaʔsittə". </ta>
            <ta e="T1361" id="Seg_8626" s="T1357">Dĭgəttə dĭ koʔbdom deʔpi. </ta>
            <ta e="T1368" id="Seg_8627" s="T1362">Măna nörbəbiʔi dĭ dĭzi (amnozittə=) amnoləj. </ta>
            <ta e="T1380" id="Seg_8628" s="T1369">Măn bar oldʼam ibiem i (dĭ = dĭ=) dĭn dʼadʼazi kambiam. </ta>
            <ta e="T1384" id="Seg_8629" s="T1380">(Dĭ=) Dĭ bĭdəbi măna. </ta>
            <ta e="T1388" id="Seg_8630" s="T1385">Bazo deʔpi maʔndə. </ta>
            <ta e="T1392" id="Seg_8631" s="T1388">Dĭgəttə măn iam šobi. </ta>
            <ta e="T1397" id="Seg_8632" s="T1393">Măn bazo kalla dʼürbim. </ta>
            <ta e="T1404" id="Seg_8633" s="T1398">Dĭ šobi da măna kundlambi maʔnʼi. </ta>
            <ta e="T1410" id="Seg_8634" s="T1404">Iʔbəbeʔ kunolzittə, dĭgəttə dĭzeŋ (s-) šobiʔi. </ta>
            <ta e="T1413" id="Seg_8635" s="T1411">"No, amnogaʔ". </ta>
            <ta e="T1418" id="Seg_8636" s="T1414">Dĭgəttə miʔnibeʔ kudajdə numan üzəbiʔi. </ta>
            <ta e="T1421" id="Seg_8637" s="T1419">Dĭgəttə kanbiʔi. </ta>
            <ta e="T1426" id="Seg_8638" s="T1422">Karəldʼan uʔbdəbiʔi, miʔnʼibeʔ šobiʔi. </ta>
            <ta e="T1432" id="Seg_8639" s="T1427">Amorbiʔi, dĭgəttə kalla dʼürbi maːʔndə. </ta>
            <ta e="T1439" id="Seg_8640" s="T1433">Dĭgəttə dĭ kambi dʼadʼanə noʔ jaʔsittə. </ta>
            <ta e="T1449" id="Seg_8641" s="T1440">I dĭn dʼala (dĭg-) dĭrgit ibi, dĭ (büzəzi) boskəndə. </ta>
            <ta e="T1452" id="Seg_8642" s="T1449">Gəttə kambiʔi bünə. </ta>
            <ta e="T1461" id="Seg_8643" s="T1453">I dĭn šübiʔi bünə, onʼiʔ koʔbdo i dĭ. </ta>
            <ta e="T1463" id="Seg_8644" s="T1462">bügən. </ta>
            <ta e="T1477" id="Seg_8645" s="T1464">Dĭgəttə pa onʼiʔ nʼi mĭbi, dĭ koʔbdom deʔpi, a dĭ külaːmbi (bünən-) bügən. </ta>
            <ta e="T1480" id="Seg_8646" s="T1478">((DMG)). </ta>
            <ta e="T1488" id="Seg_8647" s="T1484">Погодите. </ta>
            <ta e="T1492" id="Seg_8648" s="T1488">Чего рассказать коротенько такое. </ta>
            <ta e="T1508" id="Seg_8649" s="T1495">Nu, (karəlʼdʼa-) karəlʼdʼan (š- šo-) karəlʼdʼan… </ta>
            <ta e="T1512" id="Seg_8650" s="T1511">((BRK)). </ta>
            <ta e="T1514" id="Seg_8651" s="T1513">Кончилось. </ta>
            <ta e="T1521" id="Seg_8652" s="T1518">А там же… </ta>
            <ta e="T1538" id="Seg_8653" s="T1529">(А по-) А по-русски это я говорю приедет… </ta>
            <ta e="T1543" id="Seg_8654" s="T1539">Приедет мой брат, Арпит. </ta>
            <ta e="T1548" id="Seg_8655" s="T1544">Вот по-русски теперь. </ta>
            <ta e="T1552" id="Seg_8656" s="T1549">Ты будешь рад. </ta>
            <ta e="T1563" id="Seg_8657" s="T1562">А… </ta>
            <ta e="T1568" id="Seg_8658" s="T1564">Опять (по-кам-). </ta>
            <ta e="T1574" id="Seg_8659" s="T1571">Ну-ну. </ta>
            <ta e="T1579" id="Seg_8660" s="T1576">Ну наладил ты? </ta>
            <ta e="T1590" id="Seg_8661" s="T1586">Măn karəlʼdʼan edəʔleʔbəm Arpitəm. </ta>
            <ta e="T1600" id="Seg_8662" s="T1591">Dĭ (š-) mămbi: "Šolam, iləm tănan, kanžəbəj măn ianə". </ta>
            <ta e="T1608" id="Seg_8663" s="T1601">Dĭgəttə măn gibərdə ej kalam, (edəʔləm) dĭm. </ta>
            <ta e="T1612" id="Seg_8664" s="T1609">Kamen dĭ šoləj. </ta>
            <ta e="T1627" id="Seg_8665" s="T1619">Я завтра буду дожидать (э- этого=) (Ар-) Арпита. </ta>
            <ta e="T1643" id="Seg_8666" s="T1628">Никуды не буду идти, он приедет и (пойд-) поедем (с-) с им к его матери. </ta>
            <ta e="T1646" id="Seg_8667" s="T1644">Вот это. </ta>
            <ta e="T1649" id="Seg_8668" s="T1647">((BRK)) коротенько. </ta>
            <ta e="T1659" id="Seg_8669" s="T1650">Măn teinen (koum-) kunolbiam i dʼodənʼi kubiam onʼiʔ kuza. </ta>
            <ta e="T1666" id="Seg_8670" s="T1660">Dĭ măna (k-) măndə: "Kanžəbəj gibər-nʼibudʼ". </ta>
            <ta e="T1671" id="Seg_8671" s="T1667">Măn măndəm: "Ej kalam". </ta>
            <ta e="T1685" id="Seg_8672" s="T1672">Я сегодня спала, видела во сне — один человек зовет меня: "Пойдем со мной". </ta>
            <ta e="T1691" id="Seg_8673" s="T1686">А я говорю: "Не пойду". </ta>
            <ta e="T1694" id="Seg_8674" s="T1692">Кончилось, нет? </ta>
            <ta e="T1696" id="Seg_8675" s="T1695">((BRK)). </ta>
            <ta e="T1706" id="Seg_8676" s="T1697">Măn kunolbiam taldʼen i dʼodənʼi kubiom bostə sʼestranə tibi. </ta>
            <ta e="T1712" id="Seg_8677" s="T1707">(De-) Dĭgəttə kambiam šiʔnileʔ, dʼăbaktərbiam. </ta>
            <ta e="T1719" id="Seg_8678" s="T1713">(Măl-) Dĭgəttə mămbiam: šindidə dʼăbaktərləj mănzi. </ta>
            <ta e="T1728" id="Seg_8679" s="T1720">Dĭgəttə (koŋg-) koŋ šobiʔi dʼăbaktərzittə mănzi. </ta>
            <ta e="T1731" id="Seg_8680" s="T1729">Dĭgəttə dĭn… </ta>
            <ta e="T1738" id="Seg_8681" s="T1732">Я видала во сне своего зятя. </ta>
            <ta e="T1742" id="Seg_8682" s="T1739">И пошла туды. </ta>
            <ta e="T1750" id="Seg_8683" s="T1743">Там приехали начальники, и с имя разговаривала. </ta>
            <ta e="T1763" id="Seg_8684" s="T1751">И еще и дома сказала: "(С к-) С каким-то начальником буду разговаривать". </ta>
            <ta e="T1770" id="Seg_8685" s="T1764">Вот это я снила сон такой. </ta>
            <ta e="T1801" id="Seg_8686" s="T1799">Miʔ mĭmbibeʔ. </ta>
            <ta e="T1809" id="Seg_8687" s="T1801">Dĭ nüke ibi dʼala, kamen dĭ iat deʔpi. </ta>
            <ta e="T1816" id="Seg_8688" s="T1810">Măn mĭmbiem, nagur aktʼa dĭʔnə mĭbiem. </ta>
            <ta e="T1823" id="Seg_8689" s="T1817">Dĭ (kădeš-) (miʔnʼibeʔ) bădəbi, munujʔ embi. </ta>
            <ta e="T1827" id="Seg_8690" s="T1824">Kajaʔ embi, köbərgen. </ta>
            <ta e="T1831" id="Seg_8691" s="T1828">Măndə: "Amaʔ, amaʔ". </ta>
            <ta e="T1835" id="Seg_8692" s="T1832">Măn ambiam idʼiʔeʔe. </ta>
            <ta e="T1842" id="Seg_8693" s="T1836">Dĭgəttə onʼiʔ takše segi bü biʔpiem. </ta>
            <ta e="T1845" id="Seg_8694" s="T1843">Mămbiam: "Kabarləj". </ta>
            <ta e="T1860" id="Seg_8695" s="T1850">Тогда она сказала, что "Пей чай", а я говорю: "Хватит". </ta>
            <ta e="T1864" id="Seg_8696" s="T1861">Собрала на (стол). </ta>
            <ta e="T1866" id="Seg_8697" s="T1865">Господи! </ta>
            <ta e="T1873" id="Seg_8698" s="T1867">Пришли к ей, она была именинница. </ta>
            <ta e="T1877" id="Seg_8699" s="T1874">Принесли ей подарки. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T379" id="Seg_8700" s="T378">Onʼiʔ</ta>
            <ta e="T380" id="Seg_8701" s="T379">žid</ta>
            <ta e="T381" id="Seg_8702" s="T380">šo-bi</ta>
            <ta e="T382" id="Seg_8703" s="T381">onʼiʔ</ta>
            <ta e="T383" id="Seg_8704" s="T382">nu</ta>
            <ta e="T384" id="Seg_8705" s="T383">kuza</ta>
            <ta e="T385" id="Seg_8706" s="T384">šo-bi</ta>
            <ta e="T394" id="Seg_8707" s="T393">nu-zaŋ</ta>
            <ta e="T396" id="Seg_8708" s="T395">dĭgəttə</ta>
            <ta e="T397" id="Seg_8709" s="T396">plʼemʼannʼica</ta>
            <ta e="T398" id="Seg_8710" s="T397">măna</ta>
            <ta e="T399" id="Seg_8711" s="T398">noʔ</ta>
            <ta e="T400" id="Seg_8712" s="T399">kam-bi</ta>
            <ta e="T402" id="Seg_8713" s="T401">miʔ</ta>
            <ta e="T403" id="Seg_8714" s="T402">dĭ</ta>
            <ta e="T404" id="Seg_8715" s="T403">kopna-ʔi</ta>
            <ta e="T405" id="Seg_8716" s="T404">em-bi-beʔ</ta>
            <ta e="T407" id="Seg_8717" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_8718" s="T407">onʼiʔ</ta>
            <ta e="T410" id="Seg_8719" s="T409">üzə-bi</ta>
            <ta e="T412" id="Seg_8720" s="T411">măn</ta>
            <ta e="T413" id="Seg_8721" s="T412">măm-bia-m</ta>
            <ta e="T414" id="Seg_8722" s="T413">mĭ-ʔ</ta>
            <ta e="T415" id="Seg_8723" s="T414">nagur-bəʔ</ta>
            <ta e="T416" id="Seg_8724" s="T415">i-bi-beʔ</ta>
            <ta e="T418" id="Seg_8725" s="T417">măn</ta>
            <ta e="T420" id="Seg_8726" s="T418">măm-bia-m</ta>
            <ta e="T421" id="Seg_8727" s="T420">no</ta>
            <ta e="T422" id="Seg_8728" s="T421">ku-t</ta>
            <ta e="T423" id="Seg_8729" s="T422">ko-du</ta>
            <ta e="T424" id="Seg_8730" s="T423">saʔmə-lu-pi</ta>
            <ta e="T426" id="Seg_8731" s="T425">măn</ta>
            <ta e="T427" id="Seg_8732" s="T426">măm-bia-m</ta>
            <ta e="T428" id="Seg_8733" s="T427">šində=nʼibudʼ</ta>
            <ta e="T430" id="Seg_8734" s="T429">kü-lal-lə-j</ta>
            <ta e="T432" id="Seg_8735" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_8736" s="T432">măn</ta>
            <ta e="T434" id="Seg_8737" s="T433">măl-lia-m</ta>
            <ta e="T435" id="Seg_8738" s="T434">dĭ</ta>
            <ta e="T436" id="Seg_8739" s="T435">măn</ta>
            <ta e="T437" id="Seg_8740" s="T436">kü-lal-lə-m</ta>
            <ta e="T439" id="Seg_8741" s="T438">măn</ta>
            <ta e="T440" id="Seg_8742" s="T439">dʼaktə</ta>
            <ta e="T441" id="Seg_8743" s="T440">vedʼ</ta>
            <ta e="T443" id="Seg_8744" s="T442">a</ta>
            <ta e="T444" id="Seg_8745" s="T443">šiʔ</ta>
            <ta e="T445" id="Seg_8746" s="T444">išo</ta>
            <ta e="T446" id="Seg_8747" s="T445">ej</ta>
            <ta e="T447" id="Seg_8748" s="T446">dʼaktə</ta>
            <ta e="T450" id="Seg_8749" s="T449">dĭgəttə</ta>
            <ta e="T451" id="Seg_8750" s="T450">maʔ-nʼibaʔ</ta>
            <ta e="T452" id="Seg_8751" s="T451">šo-bi-baʔ</ta>
            <ta e="T454" id="Seg_8752" s="T453">măn</ta>
            <ta e="T455" id="Seg_8753" s="T454">kamen</ta>
            <ta e="T456" id="Seg_8754" s="T455">dĭ</ta>
            <ta e="T457" id="Seg_8755" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_8756" s="T457">šo-bi</ta>
            <ta e="T459" id="Seg_8757" s="T458">döbər</ta>
            <ta e="T460" id="Seg_8758" s="T459">măn</ta>
            <ta e="T461" id="Seg_8759" s="T460">mĭm-bie-m</ta>
            <ta e="T462" id="Seg_8760" s="T461">köbürgən</ta>
            <ta e="T463" id="Seg_8761" s="T462">nĭŋgə-bie-m</ta>
            <ta e="T466" id="Seg_8762" s="T465">šomi</ta>
            <ta e="T467" id="Seg_8763" s="T466">pa</ta>
            <ta e="T468" id="Seg_8764" s="T467">beržə</ta>
            <ta e="T469" id="Seg_8765" s="T468">nago-bi</ta>
            <ta e="T470" id="Seg_8766" s="T469">dʼü-nə</ta>
            <ta e="T471" id="Seg_8767" s="T470">saʔmə-luʔ-pi</ta>
            <ta e="T473" id="Seg_8768" s="T472">măn</ta>
            <ta e="T474" id="Seg_8769" s="T473">măn</ta>
            <ta e="T475" id="Seg_8770" s="T474">u</ta>
            <ta e="T476" id="Seg_8771" s="T475">šindi-nə=də</ta>
            <ta e="T477" id="Seg_8772" s="T476">ej</ta>
            <ta e="T478" id="Seg_8773" s="T477">măm-bia-m</ta>
            <ta e="T479" id="Seg_8774" s="T478">a</ta>
            <ta e="T480" id="Seg_8775" s="T479">dĭgəttə</ta>
            <ta e="T481" id="Seg_8776" s="T480">dĭn</ta>
            <ta e="T482" id="Seg_8777" s="T481">tibi-t</ta>
            <ta e="T483" id="Seg_8778" s="T482">kamen</ta>
            <ta e="T484" id="Seg_8779" s="T483">bü-gən</ta>
            <ta e="T485" id="Seg_8780" s="T484">kü-lam-bi</ta>
            <ta e="T486" id="Seg_8781" s="T485">măn</ta>
            <ta e="T487" id="Seg_8782" s="T486">măm-bia-m</ta>
            <ta e="T573" id="Seg_8783" s="T257">kamen</ta>
            <ta e="T574" id="Seg_8784" s="T573">măn</ta>
            <ta e="T575" id="Seg_8785" s="T574">aba-m</ta>
            <ta e="T576" id="Seg_8786" s="T575">šo-bi</ta>
            <ta e="T577" id="Seg_8787" s="T576">dĭn</ta>
            <ta e="T578" id="Seg_8788" s="T577">tura-gən</ta>
            <ta e="T579" id="Seg_8789" s="T578">dĭ</ta>
            <ta e="T580" id="Seg_8790" s="T579">tura-ndə</ta>
            <ta e="T581" id="Seg_8791" s="T580">Spasskaː-nə</ta>
            <ta e="T582" id="Seg_8792" s="T581">maːʔ-nʼi</ta>
            <ta e="T584" id="Seg_8793" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_8794" s="T584">šo-laʔbə</ta>
            <ta e="T586" id="Seg_8795" s="T585">šomi</ta>
            <ta e="T587" id="Seg_8796" s="T586">pa</ta>
            <ta e="T588" id="Seg_8797" s="T587">nu-laʔbə</ta>
            <ta e="T590" id="Seg_8798" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_8799" s="T590">pa-gə</ta>
            <ta e="T594" id="Seg_8800" s="T593">dʼü-nə</ta>
            <ta e="T595" id="Seg_8801" s="T594">saʔmə-luʔ-pi</ta>
            <ta e="T597" id="Seg_8802" s="T596">dĭgəttə</ta>
            <ta e="T598" id="Seg_8803" s="T597">šo</ta>
            <ta e="T599" id="Seg_8804" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_8805" s="T599">šo-bi</ta>
            <ta e="T601" id="Seg_8806" s="T600">maːʔ-ndə</ta>
            <ta e="T602" id="Seg_8807" s="T601">măna</ta>
            <ta e="T603" id="Seg_8808" s="T602">nörbə-lie</ta>
            <ta e="T605" id="Seg_8809" s="T604">măn</ta>
            <ta e="T607" id="Seg_8810" s="T606">măm-bia-m</ta>
            <ta e="T608" id="Seg_8811" s="T607">šindi=də</ta>
            <ta e="T609" id="Seg_8812" s="T608">es-seŋ-də</ta>
            <ta e="T610" id="Seg_8813" s="T609">kü-lal-lə-j</ta>
            <ta e="T612" id="Seg_8814" s="T611">i</ta>
            <ta e="T613" id="Seg_8815" s="T612">dĭgəttə</ta>
            <ta e="T614" id="Seg_8816" s="T613">dăre</ta>
            <ta e="T615" id="Seg_8817" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_8818" s="T615">i</ta>
            <ta e="T617" id="Seg_8819" s="T616">i-bi</ta>
            <ta e="T619" id="Seg_8820" s="T618">măn</ta>
            <ta e="T620" id="Seg_8821" s="T619">măn</ta>
            <ta e="T621" id="Seg_8822" s="T620">nʼi-m</ta>
            <ta e="T622" id="Seg_8823" s="T621">kam-bi</ta>
            <ta e="T623" id="Seg_8824" s="T622">bü-gən</ta>
            <ta e="T624" id="Seg_8825" s="T623">i</ta>
            <ta e="T625" id="Seg_8826" s="T624">dĭn</ta>
            <ta e="T626" id="Seg_8827" s="T625">kü-lam-bi</ta>
            <ta e="T1263" id="Seg_8828" s="T1262">kamen</ta>
            <ta e="T1264" id="Seg_8829" s="T1263">dĭ</ta>
            <ta e="T1265" id="Seg_8830" s="T1264">šo-bi</ta>
            <ta e="T1266" id="Seg_8831" s="T1265">măna</ta>
            <ta e="T1267" id="Seg_8832" s="T1266">kan-žə-bəj</ta>
            <ta e="T1268" id="Seg_8833" s="T1267">dĭn</ta>
            <ta e="T1269" id="Seg_8834" s="T1268">ia-t</ta>
            <ta e="T1270" id="Seg_8835" s="T1269">tibi-nə</ta>
            <ta e="T1271" id="Seg_8836" s="T1270">kal-la</ta>
            <ta e="T1272" id="Seg_8837" s="T1271">dʼür-bi</ta>
            <ta e="T1273" id="Seg_8838" s="T1272">măn</ta>
            <ta e="T1274" id="Seg_8839" s="T1273">unnʼa</ta>
            <ta e="T1275" id="Seg_8840" s="T1274">amno-laʔbio-m</ta>
            <ta e="T1277" id="Seg_8841" s="T1276">măn</ta>
            <ta e="T1278" id="Seg_8842" s="T1277">măm-bia-m</ta>
            <ta e="T1279" id="Seg_8843" s="T1278">kan-a-ʔ</ta>
            <ta e="T1280" id="Seg_8844" s="T1279">măna</ta>
            <ta e="T1281" id="Seg_8845" s="T1280">monoʔko-ʔ</ta>
            <ta e="T1282" id="Seg_8846" s="T1281">aba-nə</ta>
            <ta e="T1283" id="Seg_8847" s="T1282">da</ta>
            <ta e="T1284" id="Seg_8848" s="T1283">ia-nə</ta>
            <ta e="T1285" id="Seg_8849" s="T1284">dĭ</ta>
            <ta e="T1286" id="Seg_8850" s="T1285">măn-də</ta>
            <ta e="T1287" id="Seg_8851" s="T1286">dĭ-zeŋ</ta>
            <ta e="T1288" id="Seg_8852" s="T1287">ej</ta>
            <ta e="T1289" id="Seg_8853" s="T1288">mĭ-lə-ʔi</ta>
            <ta e="T1290" id="Seg_8854" s="T1289">kan-žə-bəj</ta>
            <ta e="T1292" id="Seg_8855" s="T1291">dĭgəttə</ta>
            <ta e="T1293" id="Seg_8856" s="T1292">măn</ta>
            <ta e="T1294" id="Seg_8857" s="T1293">oldʼa-m</ta>
            <ta e="T1295" id="Seg_8858" s="T1294">oʔbdə-bio-m</ta>
            <ta e="T1297" id="Seg_8859" s="T1296">nüdʼi-n</ta>
            <ta e="T1298" id="Seg_8860" s="T1297">ine-m</ta>
            <ta e="T1299" id="Seg_8861" s="T1298">kon-d-laʔpi-baʔ</ta>
            <ta e="T1300" id="Seg_8862" s="T1299">amnə-bi-baʔ</ta>
            <ta e="T1301" id="Seg_8863" s="T1300">kam-bi-baʔ</ta>
            <ta e="T1302" id="Seg_8864" s="T1301">Kazan</ta>
            <ta e="T1303" id="Seg_8865" s="T1302">tura-nə</ta>
            <ta e="T1305" id="Seg_8866" s="T1304">bar</ta>
            <ta e="T1306" id="Seg_8867" s="T1305">nüdʼi</ta>
            <ta e="T1307" id="Seg_8868" s="T1306">dĭn</ta>
            <ta e="T1308" id="Seg_8869" s="T1307">kunol-bi-baʔ</ta>
            <ta e="T1309" id="Seg_8870" s="T1308">dĭgəttə</ta>
            <ta e="T1310" id="Seg_8871" s="T1309">šo-bi-baʔ</ta>
            <ta e="T1311" id="Seg_8872" s="T1310">bazo</ta>
            <ta e="T1312" id="Seg_8873" s="T1311">dĭʔ-nə</ta>
            <ta e="T1313" id="Seg_8874" s="T1312">tura-nə</ta>
            <ta e="T1314" id="Seg_8875" s="T1313">Kaptɨrlɨk-tə</ta>
            <ta e="T1316" id="Seg_8876" s="T1315">dĭn</ta>
            <ta e="T1317" id="Seg_8877" s="T1316">selaj</ta>
            <ta e="T1319" id="Seg_8878" s="T1318">dʼala</ta>
            <ta e="T1320" id="Seg_8879" s="T1319">i-bi-beʔ</ta>
            <ta e="T1321" id="Seg_8880" s="T1320">dĭgəttə</ta>
            <ta e="T1322" id="Seg_8881" s="T1321">nüdʼi-n</ta>
            <ta e="T1323" id="Seg_8882" s="T1322">šo-bi-baʔ</ta>
            <ta e="T1324" id="Seg_8883" s="T1323">dĭ</ta>
            <ta e="T1325" id="Seg_8884" s="T1324">dĭn</ta>
            <ta e="T1326" id="Seg_8885" s="T1325">maːʔ-nə</ta>
            <ta e="T1328" id="Seg_8886" s="T1327">dĭgəttə</ta>
            <ta e="T1329" id="Seg_8887" s="T1328">dĭ</ta>
            <ta e="T1330" id="Seg_8888" s="T1329">ertə-n</ta>
            <ta e="T1331" id="Seg_8889" s="T1330">šo-bi</ta>
            <ta e="T1332" id="Seg_8890" s="T1331">dĭn</ta>
            <ta e="T1333" id="Seg_8891" s="T1332">kazak</ta>
            <ta e="T1334" id="Seg_8892" s="T1333">ia-t</ta>
            <ta e="T1336" id="Seg_8893" s="T1335">dĭgəttə</ta>
            <ta e="T1337" id="Seg_8894" s="T1336">uʔbdə-bi-baʔ</ta>
            <ta e="T1339" id="Seg_8895" s="T1338">dĭn</ta>
            <ta e="T1340" id="Seg_8896" s="T1339">amno-bia-m</ta>
            <ta e="T1343" id="Seg_8897" s="T1342">noʔ</ta>
            <ta e="T1344" id="Seg_8898" s="T1343">jaʔ-pi-baʔ</ta>
            <ta e="T1345" id="Seg_8899" s="T1344">dĭn</ta>
            <ta e="T1346" id="Seg_8900" s="T1345">bar</ta>
            <ta e="T1348" id="Seg_8901" s="T1347">dĭgəttə</ta>
            <ta e="T1349" id="Seg_8902" s="T1348">dĭn</ta>
            <ta e="T1350" id="Seg_8903" s="T1349">dʼadʼa-t</ta>
            <ta e="T1351" id="Seg_8904" s="T1350">šo-bi</ta>
            <ta e="T1353" id="Seg_8905" s="T1352">šo-ʔ</ta>
            <ta e="T1354" id="Seg_8906" s="T1353">măna</ta>
            <ta e="T1355" id="Seg_8907" s="T1354">noʔ</ta>
            <ta e="T1356" id="Seg_8908" s="T1355">jaʔ-sittə</ta>
            <ta e="T1358" id="Seg_8909" s="T1357">dĭgəttə</ta>
            <ta e="T1359" id="Seg_8910" s="T1358">dĭ</ta>
            <ta e="T1360" id="Seg_8911" s="T1359">koʔbdo-m</ta>
            <ta e="T1361" id="Seg_8912" s="T1360">deʔ-pi</ta>
            <ta e="T1363" id="Seg_8913" s="T1362">măna</ta>
            <ta e="T1364" id="Seg_8914" s="T1363">nörbə-bi-ʔi</ta>
            <ta e="T1365" id="Seg_8915" s="T1364">dĭ</ta>
            <ta e="T1366" id="Seg_8916" s="T1365">dĭ-zi</ta>
            <ta e="T1367" id="Seg_8917" s="T1366">amno-zittə</ta>
            <ta e="T1368" id="Seg_8918" s="T1367">amno-lə-j</ta>
            <ta e="T1370" id="Seg_8919" s="T1369">măn</ta>
            <ta e="T1371" id="Seg_8920" s="T1370">bar</ta>
            <ta e="T1372" id="Seg_8921" s="T1371">oldʼa-m</ta>
            <ta e="T1373" id="Seg_8922" s="T1372">i-bie-m</ta>
            <ta e="T1374" id="Seg_8923" s="T1373">i</ta>
            <ta e="T1375" id="Seg_8924" s="T1374">dĭ</ta>
            <ta e="T1377" id="Seg_8925" s="T1376">dĭ</ta>
            <ta e="T1378" id="Seg_8926" s="T1377">dĭn</ta>
            <ta e="T1379" id="Seg_8927" s="T1378">dʼadʼa-zi</ta>
            <ta e="T1380" id="Seg_8928" s="T1379">kam-bia-m</ta>
            <ta e="T1381" id="Seg_8929" s="T1380">dĭ</ta>
            <ta e="T1382" id="Seg_8930" s="T1381">dĭ</ta>
            <ta e="T1383" id="Seg_8931" s="T1382">bĭdə-bi</ta>
            <ta e="T1384" id="Seg_8932" s="T1383">măna</ta>
            <ta e="T1386" id="Seg_8933" s="T1385">bazo</ta>
            <ta e="T1387" id="Seg_8934" s="T1386">deʔ-pi</ta>
            <ta e="T1388" id="Seg_8935" s="T1387">maʔ-ndə</ta>
            <ta e="T1389" id="Seg_8936" s="T1388">dĭgəttə</ta>
            <ta e="T1390" id="Seg_8937" s="T1389">măn</ta>
            <ta e="T1391" id="Seg_8938" s="T1390">ia-m</ta>
            <ta e="T1392" id="Seg_8939" s="T1391">šo-bi</ta>
            <ta e="T1394" id="Seg_8940" s="T1393">măn</ta>
            <ta e="T1395" id="Seg_8941" s="T1394">bazo</ta>
            <ta e="T1396" id="Seg_8942" s="T1395">kal-la</ta>
            <ta e="T1397" id="Seg_8943" s="T1396">dʼür-bi-m</ta>
            <ta e="T1399" id="Seg_8944" s="T1398">dĭ</ta>
            <ta e="T1400" id="Seg_8945" s="T1399">šo-bi</ta>
            <ta e="T1401" id="Seg_8946" s="T1400">da</ta>
            <ta e="T1402" id="Seg_8947" s="T1401">măna</ta>
            <ta e="T1403" id="Seg_8948" s="T1402">kund-lam-bi</ta>
            <ta e="T1404" id="Seg_8949" s="T1403">maʔ-nʼi</ta>
            <ta e="T1405" id="Seg_8950" s="T1404">iʔbə-beʔ</ta>
            <ta e="T1406" id="Seg_8951" s="T1405">kunol-zittə</ta>
            <ta e="T1407" id="Seg_8952" s="T1406">dĭgəttə</ta>
            <ta e="T1408" id="Seg_8953" s="T1407">dĭ-zeŋ</ta>
            <ta e="T1410" id="Seg_8954" s="T1409">šo-bi-ʔi</ta>
            <ta e="T1412" id="Seg_8955" s="T1411">no</ta>
            <ta e="T1413" id="Seg_8956" s="T1412">amno-gaʔ</ta>
            <ta e="T1415" id="Seg_8957" s="T1414">dĭgəttə</ta>
            <ta e="T1416" id="Seg_8958" s="T1415">miʔnibeʔ</ta>
            <ta e="T1417" id="Seg_8959" s="T1416">kudaj-də</ta>
            <ta e="T1418" id="Seg_8960" s="T1417">numan üzə-bi-ʔi</ta>
            <ta e="T1420" id="Seg_8961" s="T1419">dĭgəttə</ta>
            <ta e="T1421" id="Seg_8962" s="T1420">kan-bi-ʔi</ta>
            <ta e="T1423" id="Seg_8963" s="T1422">karəldʼan</ta>
            <ta e="T1424" id="Seg_8964" s="T1423">uʔbdə-bi-ʔi</ta>
            <ta e="T1425" id="Seg_8965" s="T1424">miʔnʼibeʔ</ta>
            <ta e="T1426" id="Seg_8966" s="T1425">šo-bi-ʔi</ta>
            <ta e="T1428" id="Seg_8967" s="T1427">amor-bi-ʔi</ta>
            <ta e="T1429" id="Seg_8968" s="T1428">dĭgəttə</ta>
            <ta e="T1430" id="Seg_8969" s="T1429">kal-la</ta>
            <ta e="T1431" id="Seg_8970" s="T1430">dʼür-bi</ta>
            <ta e="T1432" id="Seg_8971" s="T1431">maːʔ-ndə</ta>
            <ta e="T1434" id="Seg_8972" s="T1433">dĭgəttə</ta>
            <ta e="T1435" id="Seg_8973" s="T1434">dĭ</ta>
            <ta e="T1436" id="Seg_8974" s="T1435">kam-bi</ta>
            <ta e="T1437" id="Seg_8975" s="T1436">dʼadʼa-nə</ta>
            <ta e="T1438" id="Seg_8976" s="T1437">noʔ</ta>
            <ta e="T1439" id="Seg_8977" s="T1438">jaʔ-sittə</ta>
            <ta e="T1441" id="Seg_8978" s="T1440">i</ta>
            <ta e="T1442" id="Seg_8979" s="T1441">dĭn</ta>
            <ta e="T1443" id="Seg_8980" s="T1442">dʼala</ta>
            <ta e="T1445" id="Seg_8981" s="T1444">dĭrgit</ta>
            <ta e="T1446" id="Seg_8982" s="T1445">i-bi</ta>
            <ta e="T1447" id="Seg_8983" s="T1446">dĭ</ta>
            <ta e="T1448" id="Seg_8984" s="T1447">bü-zə-zi</ta>
            <ta e="T1449" id="Seg_8985" s="T1448">bos-kəndə</ta>
            <ta e="T1450" id="Seg_8986" s="T1449">gəttə</ta>
            <ta e="T1451" id="Seg_8987" s="T1450">kam-bi-ʔi</ta>
            <ta e="T1452" id="Seg_8988" s="T1451">bü-nə</ta>
            <ta e="T1454" id="Seg_8989" s="T1453">i</ta>
            <ta e="T1455" id="Seg_8990" s="T1454">dĭn</ta>
            <ta e="T1456" id="Seg_8991" s="T1455">šü-bi-ʔi</ta>
            <ta e="T1457" id="Seg_8992" s="T1456">bü-nə</ta>
            <ta e="T1458" id="Seg_8993" s="T1457">onʼiʔ</ta>
            <ta e="T1459" id="Seg_8994" s="T1458">koʔbdo</ta>
            <ta e="T1460" id="Seg_8995" s="T1459">i</ta>
            <ta e="T1461" id="Seg_8996" s="T1460">dĭ</ta>
            <ta e="T1463" id="Seg_8997" s="T1462">bü-gən</ta>
            <ta e="T1465" id="Seg_8998" s="T1464">dĭgəttə</ta>
            <ta e="T1466" id="Seg_8999" s="T1465">pa</ta>
            <ta e="T1467" id="Seg_9000" s="T1466">onʼiʔ</ta>
            <ta e="T1468" id="Seg_9001" s="T1467">nʼi</ta>
            <ta e="T1469" id="Seg_9002" s="T1468">mĭ-bi</ta>
            <ta e="T1470" id="Seg_9003" s="T1469">dĭ</ta>
            <ta e="T1471" id="Seg_9004" s="T1470">koʔbdo-m</ta>
            <ta e="T1472" id="Seg_9005" s="T1471">deʔ-pi</ta>
            <ta e="T1473" id="Seg_9006" s="T1472">a</ta>
            <ta e="T1474" id="Seg_9007" s="T1473">dĭ</ta>
            <ta e="T1475" id="Seg_9008" s="T1474">kü-laːm-bi</ta>
            <ta e="T1477" id="Seg_9009" s="T1476">bü-gən</ta>
            <ta e="T1498" id="Seg_9010" s="T1495">nu</ta>
            <ta e="T1500" id="Seg_9011" s="T1499">karəlʼdʼan</ta>
            <ta e="T1506" id="Seg_9012" s="T1503">šo</ta>
            <ta e="T1508" id="Seg_9013" s="T1506">karəlʼdʼan</ta>
            <ta e="T1587" id="Seg_9014" s="T1586">măn</ta>
            <ta e="T1588" id="Seg_9015" s="T1587">karəlʼdʼan</ta>
            <ta e="T1589" id="Seg_9016" s="T1588">edəʔ-leʔbə-m</ta>
            <ta e="T1590" id="Seg_9017" s="T1589">Arpit-ə-m</ta>
            <ta e="T1592" id="Seg_9018" s="T1591">dĭ</ta>
            <ta e="T1594" id="Seg_9019" s="T1593">măm-bi</ta>
            <ta e="T1595" id="Seg_9020" s="T1594">šo-la-m</ta>
            <ta e="T1596" id="Seg_9021" s="T1595">il-m</ta>
            <ta e="T1597" id="Seg_9022" s="T1596">tănan</ta>
            <ta e="T1598" id="Seg_9023" s="T1597">kan-žə-bəj</ta>
            <ta e="T1599" id="Seg_9024" s="T1598">măn</ta>
            <ta e="T1600" id="Seg_9025" s="T1599">ia-nə</ta>
            <ta e="T1602" id="Seg_9026" s="T1601">dĭgəttə</ta>
            <ta e="T1603" id="Seg_9027" s="T1602">măn</ta>
            <ta e="T1604" id="Seg_9028" s="T1603">gibər=də</ta>
            <ta e="T1605" id="Seg_9029" s="T1604">ej</ta>
            <ta e="T1606" id="Seg_9030" s="T1605">ka-la-m</ta>
            <ta e="T1607" id="Seg_9031" s="T1606">edəʔ-lə-m</ta>
            <ta e="T1608" id="Seg_9032" s="T1607">dĭ-m</ta>
            <ta e="T1610" id="Seg_9033" s="T1609">kamen</ta>
            <ta e="T1611" id="Seg_9034" s="T1610">dĭ</ta>
            <ta e="T1612" id="Seg_9035" s="T1611">šo-lə-j</ta>
            <ta e="T1651" id="Seg_9036" s="T1650">măn</ta>
            <ta e="T1652" id="Seg_9037" s="T1651">teinen</ta>
            <ta e="T1654" id="Seg_9038" s="T1653">kunol-bia-m</ta>
            <ta e="T1655" id="Seg_9039" s="T1654">i</ta>
            <ta e="T1656" id="Seg_9040" s="T1655">dʼodə-nʼi</ta>
            <ta e="T1657" id="Seg_9041" s="T1656">ku-bia-m</ta>
            <ta e="T1658" id="Seg_9042" s="T1657">onʼiʔ</ta>
            <ta e="T1659" id="Seg_9043" s="T1658">kuza</ta>
            <ta e="T1661" id="Seg_9044" s="T1660">dĭ</ta>
            <ta e="T1662" id="Seg_9045" s="T1661">măna</ta>
            <ta e="T1664" id="Seg_9046" s="T1663">măn-də</ta>
            <ta e="T1665" id="Seg_9047" s="T1664">kan-žə-bəj</ta>
            <ta e="T1666" id="Seg_9048" s="T1665">gibər=nʼibudʼ</ta>
            <ta e="T1668" id="Seg_9049" s="T1667">măn</ta>
            <ta e="T1669" id="Seg_9050" s="T1668">măn-də-m</ta>
            <ta e="T1670" id="Seg_9051" s="T1669">ej</ta>
            <ta e="T1671" id="Seg_9052" s="T1670">ka-la-m</ta>
            <ta e="T1698" id="Seg_9053" s="T1697">măn</ta>
            <ta e="T1699" id="Seg_9054" s="T1698">kunol-bia-m</ta>
            <ta e="T1700" id="Seg_9055" s="T1699">taldʼen</ta>
            <ta e="T1701" id="Seg_9056" s="T1700">i</ta>
            <ta e="T1702" id="Seg_9057" s="T1701">dʼodə-nʼi</ta>
            <ta e="T1703" id="Seg_9058" s="T1702">ku-bio-m</ta>
            <ta e="T1704" id="Seg_9059" s="T1703">bos-tə</ta>
            <ta e="T1705" id="Seg_9060" s="T1704">sʼestra-nə</ta>
            <ta e="T1706" id="Seg_9061" s="T1705">tibi</ta>
            <ta e="T1709" id="Seg_9062" s="T1708">dĭgəttə</ta>
            <ta e="T1710" id="Seg_9063" s="T1709">kam-bia-m</ta>
            <ta e="T1711" id="Seg_9064" s="T1710">šiʔnileʔ</ta>
            <ta e="T1712" id="Seg_9065" s="T1711">dʼăbaktər-bia-m</ta>
            <ta e="T1715" id="Seg_9066" s="T1714">dĭgəttə</ta>
            <ta e="T1716" id="Seg_9067" s="T1715">măm-bia-m</ta>
            <ta e="T1717" id="Seg_9068" s="T1716">šindi=də</ta>
            <ta e="T1718" id="Seg_9069" s="T1717">dʼăbaktər-lə-j</ta>
            <ta e="T1719" id="Seg_9070" s="T1718">măn-zi</ta>
            <ta e="T1721" id="Seg_9071" s="T1720">dĭgəttə</ta>
            <ta e="T1725" id="Seg_9072" s="T1722">koŋ</ta>
            <ta e="T1726" id="Seg_9073" s="T1725">šo-bi-ʔi</ta>
            <ta e="T1727" id="Seg_9074" s="T1726">dʼăbaktər-zittə</ta>
            <ta e="T1728" id="Seg_9075" s="T1727">măn-zi</ta>
            <ta e="T1730" id="Seg_9076" s="T1729">dĭgəttə</ta>
            <ta e="T1731" id="Seg_9077" s="T1730">dĭn</ta>
            <ta e="T1800" id="Seg_9078" s="T1799">miʔ</ta>
            <ta e="T1801" id="Seg_9079" s="T1800">mĭm-bi-beʔ</ta>
            <ta e="T1802" id="Seg_9080" s="T1801">dĭ</ta>
            <ta e="T1803" id="Seg_9081" s="T1802">nüke</ta>
            <ta e="T1804" id="Seg_9082" s="T1803">i-bi</ta>
            <ta e="T1805" id="Seg_9083" s="T1804">dʼala</ta>
            <ta e="T1806" id="Seg_9084" s="T1805">kamen</ta>
            <ta e="T1807" id="Seg_9085" s="T1806">dĭ</ta>
            <ta e="T1808" id="Seg_9086" s="T1807">ia-t</ta>
            <ta e="T1809" id="Seg_9087" s="T1808">deʔ-pi</ta>
            <ta e="T1811" id="Seg_9088" s="T1810">măn</ta>
            <ta e="T1812" id="Seg_9089" s="T1811">mĭm-bie-m</ta>
            <ta e="T1813" id="Seg_9090" s="T1812">nagur</ta>
            <ta e="T1814" id="Seg_9091" s="T1813">aktʼa</ta>
            <ta e="T1815" id="Seg_9092" s="T1814">dĭʔ-nə</ta>
            <ta e="T1816" id="Seg_9093" s="T1815">mĭ-bie-m</ta>
            <ta e="T1818" id="Seg_9094" s="T1817">dĭ</ta>
            <ta e="T1820" id="Seg_9095" s="T1819">miʔnʼibeʔ</ta>
            <ta e="T1821" id="Seg_9096" s="T1820">bădə-bi</ta>
            <ta e="T1822" id="Seg_9097" s="T1821">munuj-ʔ</ta>
            <ta e="T1823" id="Seg_9098" s="T1822">em-bi</ta>
            <ta e="T1825" id="Seg_9099" s="T1824">kajaʔ</ta>
            <ta e="T1826" id="Seg_9100" s="T1825">em-bi</ta>
            <ta e="T1827" id="Seg_9101" s="T1826">köbərgen</ta>
            <ta e="T1829" id="Seg_9102" s="T1828">măn-də</ta>
            <ta e="T1830" id="Seg_9103" s="T1829">am-a-ʔ</ta>
            <ta e="T1831" id="Seg_9104" s="T1830">am-a-ʔ</ta>
            <ta e="T1833" id="Seg_9105" s="T1832">măn</ta>
            <ta e="T1834" id="Seg_9106" s="T1833">am-bia-m</ta>
            <ta e="T1835" id="Seg_9107" s="T1834">idʼiʔeʔe</ta>
            <ta e="T1837" id="Seg_9108" s="T1836">dĭgəttə</ta>
            <ta e="T1838" id="Seg_9109" s="T1837">onʼiʔ</ta>
            <ta e="T1839" id="Seg_9110" s="T1838">takše</ta>
            <ta e="T1840" id="Seg_9111" s="T1839">segi</ta>
            <ta e="T1841" id="Seg_9112" s="T1840">bü</ta>
            <ta e="T1842" id="Seg_9113" s="T1841">biʔ-pie-m</ta>
            <ta e="T1844" id="Seg_9114" s="T1843">măm-bia-m</ta>
            <ta e="T1845" id="Seg_9115" s="T1844">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T379" id="Seg_9116" s="T378">onʼiʔ</ta>
            <ta e="T380" id="Seg_9117" s="T379">žid</ta>
            <ta e="T381" id="Seg_9118" s="T380">šo-bi</ta>
            <ta e="T382" id="Seg_9119" s="T381">onʼiʔ</ta>
            <ta e="T383" id="Seg_9120" s="T382">nu</ta>
            <ta e="T384" id="Seg_9121" s="T383">kuza</ta>
            <ta e="T385" id="Seg_9122" s="T384">šo-bi</ta>
            <ta e="T394" id="Seg_9123" s="T393">nu-zAŋ</ta>
            <ta e="T396" id="Seg_9124" s="T395">dĭgəttə</ta>
            <ta e="T397" id="Seg_9125" s="T396">plemʼannica</ta>
            <ta e="T398" id="Seg_9126" s="T397">măna</ta>
            <ta e="T399" id="Seg_9127" s="T398">noʔ</ta>
            <ta e="T400" id="Seg_9128" s="T399">kan-bi</ta>
            <ta e="T402" id="Seg_9129" s="T401">miʔ</ta>
            <ta e="T403" id="Seg_9130" s="T402">dĭ</ta>
            <ta e="T404" id="Seg_9131" s="T403">kopnă-jəʔ</ta>
            <ta e="T405" id="Seg_9132" s="T404">hen-bi-bAʔ</ta>
            <ta e="T407" id="Seg_9133" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_9134" s="T407">onʼiʔ</ta>
            <ta e="T410" id="Seg_9135" s="T409">üzə-bi</ta>
            <ta e="T412" id="Seg_9136" s="T411">măn</ta>
            <ta e="T413" id="Seg_9137" s="T412">măn-bi-m</ta>
            <ta e="T414" id="Seg_9138" s="T413">mĭ-ʔ</ta>
            <ta e="T415" id="Seg_9139" s="T414">nagur-bAʔ</ta>
            <ta e="T416" id="Seg_9140" s="T415">i-bi-bAʔ</ta>
            <ta e="T418" id="Seg_9141" s="T417">măn</ta>
            <ta e="T420" id="Seg_9142" s="T418">măn-bi-m</ta>
            <ta e="T421" id="Seg_9143" s="T420">no</ta>
            <ta e="T422" id="Seg_9144" s="T421">ku-t</ta>
            <ta e="T423" id="Seg_9145" s="T422">ku-du</ta>
            <ta e="T424" id="Seg_9146" s="T423">saʔmə-lV-bi</ta>
            <ta e="T426" id="Seg_9147" s="T425">măn</ta>
            <ta e="T427" id="Seg_9148" s="T426">măn-bi-m</ta>
            <ta e="T428" id="Seg_9149" s="T427">šində=nʼibudʼ</ta>
            <ta e="T430" id="Seg_9150" s="T429">kü-laːm-lV-j</ta>
            <ta e="T432" id="Seg_9151" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_9152" s="T432">măn</ta>
            <ta e="T434" id="Seg_9153" s="T433">măn-liA-m</ta>
            <ta e="T435" id="Seg_9154" s="T434">dĭ</ta>
            <ta e="T436" id="Seg_9155" s="T435">măn</ta>
            <ta e="T437" id="Seg_9156" s="T436">kü-laːm-lV-m</ta>
            <ta e="T439" id="Seg_9157" s="T438">măn</ta>
            <ta e="T440" id="Seg_9158" s="T439">tʼaktə</ta>
            <ta e="T441" id="Seg_9159" s="T440">vedʼ</ta>
            <ta e="T443" id="Seg_9160" s="T442">a</ta>
            <ta e="T444" id="Seg_9161" s="T443">šiʔ</ta>
            <ta e="T445" id="Seg_9162" s="T444">ĭššo</ta>
            <ta e="T446" id="Seg_9163" s="T445">ej</ta>
            <ta e="T447" id="Seg_9164" s="T446">tʼaktə</ta>
            <ta e="T450" id="Seg_9165" s="T449">dĭgəttə</ta>
            <ta e="T451" id="Seg_9166" s="T450">maʔ-gənʼibAʔ</ta>
            <ta e="T452" id="Seg_9167" s="T451">šo-bi-bAʔ</ta>
            <ta e="T454" id="Seg_9168" s="T453">măn</ta>
            <ta e="T455" id="Seg_9169" s="T454">kamən</ta>
            <ta e="T456" id="Seg_9170" s="T455">dĭ</ta>
            <ta e="T457" id="Seg_9171" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_9172" s="T457">šo-bi</ta>
            <ta e="T459" id="Seg_9173" s="T458">döbər</ta>
            <ta e="T460" id="Seg_9174" s="T459">măn</ta>
            <ta e="T461" id="Seg_9175" s="T460">mĭn-bi-m</ta>
            <ta e="T462" id="Seg_9176" s="T461">köbergən</ta>
            <ta e="T463" id="Seg_9177" s="T462">nĭŋgə-bi-m</ta>
            <ta e="T466" id="Seg_9178" s="T465">šomi</ta>
            <ta e="T467" id="Seg_9179" s="T466">pa</ta>
            <ta e="T468" id="Seg_9180" s="T467">beržə</ta>
            <ta e="T469" id="Seg_9181" s="T468">naga-bi</ta>
            <ta e="T470" id="Seg_9182" s="T469">tʼo-Tə</ta>
            <ta e="T471" id="Seg_9183" s="T470">saʔmə-luʔbdə-bi</ta>
            <ta e="T473" id="Seg_9184" s="T472">măn</ta>
            <ta e="T474" id="Seg_9185" s="T473">măn</ta>
            <ta e="T475" id="Seg_9186" s="T474">u</ta>
            <ta e="T476" id="Seg_9187" s="T475">šində-Tə=də</ta>
            <ta e="T477" id="Seg_9188" s="T476">ej</ta>
            <ta e="T478" id="Seg_9189" s="T477">măn-bi-m</ta>
            <ta e="T479" id="Seg_9190" s="T478">a</ta>
            <ta e="T480" id="Seg_9191" s="T479">dĭgəttə</ta>
            <ta e="T481" id="Seg_9192" s="T480">dĭn</ta>
            <ta e="T482" id="Seg_9193" s="T481">tibi-t</ta>
            <ta e="T483" id="Seg_9194" s="T482">kamən</ta>
            <ta e="T484" id="Seg_9195" s="T483">bü-Kən</ta>
            <ta e="T485" id="Seg_9196" s="T484">kü-laːm-bi</ta>
            <ta e="T486" id="Seg_9197" s="T485">măn</ta>
            <ta e="T487" id="Seg_9198" s="T486">măn-bi-m</ta>
            <ta e="T573" id="Seg_9199" s="T257">kamən</ta>
            <ta e="T574" id="Seg_9200" s="T573">măn</ta>
            <ta e="T575" id="Seg_9201" s="T574">aba-m</ta>
            <ta e="T576" id="Seg_9202" s="T575">šo-bi</ta>
            <ta e="T577" id="Seg_9203" s="T576">dĭn</ta>
            <ta e="T578" id="Seg_9204" s="T577">tura-Kən</ta>
            <ta e="T579" id="Seg_9205" s="T578">dĭ</ta>
            <ta e="T580" id="Seg_9206" s="T579">tura-gəndə</ta>
            <ta e="T581" id="Seg_9207" s="T580">Spasskaː-Tə</ta>
            <ta e="T582" id="Seg_9208" s="T581">maʔ-gənʼi</ta>
            <ta e="T584" id="Seg_9209" s="T583">dĭgəttə</ta>
            <ta e="T585" id="Seg_9210" s="T584">šo-laʔbə</ta>
            <ta e="T586" id="Seg_9211" s="T585">šomi</ta>
            <ta e="T587" id="Seg_9212" s="T586">pa</ta>
            <ta e="T588" id="Seg_9213" s="T587">nu-laʔbə</ta>
            <ta e="T590" id="Seg_9214" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_9215" s="T590">pa-gəʔ</ta>
            <ta e="T594" id="Seg_9216" s="T593">tʼo-Tə</ta>
            <ta e="T595" id="Seg_9217" s="T594">saʔmə-luʔbdə-bi</ta>
            <ta e="T597" id="Seg_9218" s="T596">dĭgəttə</ta>
            <ta e="T598" id="Seg_9219" s="T597">šo</ta>
            <ta e="T599" id="Seg_9220" s="T598">dĭ</ta>
            <ta e="T600" id="Seg_9221" s="T599">šo-bi</ta>
            <ta e="T601" id="Seg_9222" s="T600">maʔ-gəndə</ta>
            <ta e="T602" id="Seg_9223" s="T601">măna</ta>
            <ta e="T603" id="Seg_9224" s="T602">nörbə-liA</ta>
            <ta e="T605" id="Seg_9225" s="T604">măn</ta>
            <ta e="T607" id="Seg_9226" s="T606">măn-bi-m</ta>
            <ta e="T608" id="Seg_9227" s="T607">šində=də</ta>
            <ta e="T609" id="Seg_9228" s="T608">ešši-zAŋ-Tə</ta>
            <ta e="T610" id="Seg_9229" s="T609">kü-laːm-lV-j</ta>
            <ta e="T612" id="Seg_9230" s="T611">i</ta>
            <ta e="T613" id="Seg_9231" s="T612">dĭgəttə</ta>
            <ta e="T614" id="Seg_9232" s="T613">dărəʔ</ta>
            <ta e="T615" id="Seg_9233" s="T614">dĭ</ta>
            <ta e="T616" id="Seg_9234" s="T615">i</ta>
            <ta e="T617" id="Seg_9235" s="T616">i-bi</ta>
            <ta e="T619" id="Seg_9236" s="T618">măn</ta>
            <ta e="T620" id="Seg_9237" s="T619">măn</ta>
            <ta e="T621" id="Seg_9238" s="T620">nʼi-m</ta>
            <ta e="T622" id="Seg_9239" s="T621">kan-bi</ta>
            <ta e="T623" id="Seg_9240" s="T622">bü-Kən</ta>
            <ta e="T624" id="Seg_9241" s="T623">i</ta>
            <ta e="T625" id="Seg_9242" s="T624">dĭn</ta>
            <ta e="T626" id="Seg_9243" s="T625">kü-laːm-bi</ta>
            <ta e="T1263" id="Seg_9244" s="T1262">kamən</ta>
            <ta e="T1264" id="Seg_9245" s="T1263">dĭ</ta>
            <ta e="T1265" id="Seg_9246" s="T1264">šo-bi</ta>
            <ta e="T1266" id="Seg_9247" s="T1265">măna</ta>
            <ta e="T1267" id="Seg_9248" s="T1266">kan-žə-bəj</ta>
            <ta e="T1268" id="Seg_9249" s="T1267">dĭn</ta>
            <ta e="T1269" id="Seg_9250" s="T1268">ija-t</ta>
            <ta e="T1270" id="Seg_9251" s="T1269">tibi-Tə</ta>
            <ta e="T1271" id="Seg_9252" s="T1270">kan-lAʔ</ta>
            <ta e="T1272" id="Seg_9253" s="T1271">tʼür-bi</ta>
            <ta e="T1273" id="Seg_9254" s="T1272">măn</ta>
            <ta e="T1274" id="Seg_9255" s="T1273">unʼə</ta>
            <ta e="T1275" id="Seg_9256" s="T1274">amno-laʔbə-m</ta>
            <ta e="T1277" id="Seg_9257" s="T1276">măn</ta>
            <ta e="T1278" id="Seg_9258" s="T1277">măn-bi-m</ta>
            <ta e="T1279" id="Seg_9259" s="T1278">kan-ə-ʔ</ta>
            <ta e="T1280" id="Seg_9260" s="T1279">măna</ta>
            <ta e="T1281" id="Seg_9261" s="T1280">monoʔko-ʔ</ta>
            <ta e="T1282" id="Seg_9262" s="T1281">aba-Tə</ta>
            <ta e="T1283" id="Seg_9263" s="T1282">da</ta>
            <ta e="T1284" id="Seg_9264" s="T1283">ija-Tə</ta>
            <ta e="T1285" id="Seg_9265" s="T1284">dĭ</ta>
            <ta e="T1286" id="Seg_9266" s="T1285">măn-ntə</ta>
            <ta e="T1287" id="Seg_9267" s="T1286">dĭ-zAŋ</ta>
            <ta e="T1288" id="Seg_9268" s="T1287">ej</ta>
            <ta e="T1289" id="Seg_9269" s="T1288">mĭ-lV-jəʔ</ta>
            <ta e="T1290" id="Seg_9270" s="T1289">kan-žə-bəj</ta>
            <ta e="T1292" id="Seg_9271" s="T1291">dĭgəttə</ta>
            <ta e="T1293" id="Seg_9272" s="T1292">măn</ta>
            <ta e="T1294" id="Seg_9273" s="T1293">oldʼa-m</ta>
            <ta e="T1295" id="Seg_9274" s="T1294">oʔbdə-bi-m</ta>
            <ta e="T1297" id="Seg_9275" s="T1296">nüdʼi-n</ta>
            <ta e="T1298" id="Seg_9276" s="T1297">ine-m</ta>
            <ta e="T1299" id="Seg_9277" s="T1298">kon-də-laʔpi-bAʔ</ta>
            <ta e="T1300" id="Seg_9278" s="T1299">amnə-bi-bAʔ</ta>
            <ta e="T1301" id="Seg_9279" s="T1300">kan-bi-bAʔ</ta>
            <ta e="T1302" id="Seg_9280" s="T1301">Kazan</ta>
            <ta e="T1303" id="Seg_9281" s="T1302">tura-Tə</ta>
            <ta e="T1305" id="Seg_9282" s="T1304">bar</ta>
            <ta e="T1306" id="Seg_9283" s="T1305">nüdʼi</ta>
            <ta e="T1307" id="Seg_9284" s="T1306">dĭn</ta>
            <ta e="T1308" id="Seg_9285" s="T1307">kunol-bi-bAʔ</ta>
            <ta e="T1309" id="Seg_9286" s="T1308">dĭgəttə</ta>
            <ta e="T1310" id="Seg_9287" s="T1309">šo-bi-bAʔ</ta>
            <ta e="T1311" id="Seg_9288" s="T1310">bazoʔ</ta>
            <ta e="T1312" id="Seg_9289" s="T1311">dĭ-Tə</ta>
            <ta e="T1313" id="Seg_9290" s="T1312">tura-Tə</ta>
            <ta e="T1314" id="Seg_9291" s="T1313">Kaptɨrlɨk-Tə</ta>
            <ta e="T1316" id="Seg_9292" s="T1315">dĭn</ta>
            <ta e="T1317" id="Seg_9293" s="T1316">celɨj</ta>
            <ta e="T1319" id="Seg_9294" s="T1318">tʼala</ta>
            <ta e="T1320" id="Seg_9295" s="T1319">i-bi-bAʔ</ta>
            <ta e="T1321" id="Seg_9296" s="T1320">dĭgəttə</ta>
            <ta e="T1322" id="Seg_9297" s="T1321">nüdʼi-n</ta>
            <ta e="T1323" id="Seg_9298" s="T1322">šo-bi-bAʔ</ta>
            <ta e="T1324" id="Seg_9299" s="T1323">dĭ</ta>
            <ta e="T1325" id="Seg_9300" s="T1324">dĭn</ta>
            <ta e="T1326" id="Seg_9301" s="T1325">maʔ-Tə</ta>
            <ta e="T1328" id="Seg_9302" s="T1327">dĭgəttə</ta>
            <ta e="T1329" id="Seg_9303" s="T1328">dĭ</ta>
            <ta e="T1330" id="Seg_9304" s="T1329">ertə-n</ta>
            <ta e="T1331" id="Seg_9305" s="T1330">šo-bi</ta>
            <ta e="T1332" id="Seg_9306" s="T1331">dĭn</ta>
            <ta e="T1333" id="Seg_9307" s="T1332">kazak</ta>
            <ta e="T1334" id="Seg_9308" s="T1333">ija-t</ta>
            <ta e="T1336" id="Seg_9309" s="T1335">dĭgəttə</ta>
            <ta e="T1337" id="Seg_9310" s="T1336">uʔbdə-bi-bAʔ</ta>
            <ta e="T1339" id="Seg_9311" s="T1338">dĭn</ta>
            <ta e="T1340" id="Seg_9312" s="T1339">amnə-bi-m</ta>
            <ta e="T1343" id="Seg_9313" s="T1342">noʔ</ta>
            <ta e="T1344" id="Seg_9314" s="T1343">hʼaʔ-bi-bAʔ</ta>
            <ta e="T1345" id="Seg_9315" s="T1344">dĭn</ta>
            <ta e="T1346" id="Seg_9316" s="T1345">bar</ta>
            <ta e="T1348" id="Seg_9317" s="T1347">dĭgəttə</ta>
            <ta e="T1349" id="Seg_9318" s="T1348">dĭn</ta>
            <ta e="T1350" id="Seg_9319" s="T1349">dʼadʼa-t</ta>
            <ta e="T1351" id="Seg_9320" s="T1350">šo-bi</ta>
            <ta e="T1353" id="Seg_9321" s="T1352">šo-ʔ</ta>
            <ta e="T1354" id="Seg_9322" s="T1353">măna</ta>
            <ta e="T1355" id="Seg_9323" s="T1354">noʔ</ta>
            <ta e="T1356" id="Seg_9324" s="T1355">hʼaʔ-zittə</ta>
            <ta e="T1358" id="Seg_9325" s="T1357">dĭgəttə</ta>
            <ta e="T1359" id="Seg_9326" s="T1358">dĭ</ta>
            <ta e="T1360" id="Seg_9327" s="T1359">koʔbdo-m</ta>
            <ta e="T1361" id="Seg_9328" s="T1360">det-bi</ta>
            <ta e="T1363" id="Seg_9329" s="T1362">măna</ta>
            <ta e="T1364" id="Seg_9330" s="T1363">nörbə-bi-jəʔ</ta>
            <ta e="T1365" id="Seg_9331" s="T1364">dĭ</ta>
            <ta e="T1366" id="Seg_9332" s="T1365">dĭ-ziʔ</ta>
            <ta e="T1367" id="Seg_9333" s="T1366">amno-zittə</ta>
            <ta e="T1368" id="Seg_9334" s="T1367">amno-lV-j</ta>
            <ta e="T1370" id="Seg_9335" s="T1369">măn</ta>
            <ta e="T1371" id="Seg_9336" s="T1370">bar</ta>
            <ta e="T1372" id="Seg_9337" s="T1371">oldʼa-m</ta>
            <ta e="T1373" id="Seg_9338" s="T1372">i-bi-m</ta>
            <ta e="T1374" id="Seg_9339" s="T1373">i</ta>
            <ta e="T1375" id="Seg_9340" s="T1374">dĭ</ta>
            <ta e="T1377" id="Seg_9341" s="T1376">dĭ</ta>
            <ta e="T1378" id="Seg_9342" s="T1377">dĭn</ta>
            <ta e="T1379" id="Seg_9343" s="T1378">dʼadʼa-ziʔ</ta>
            <ta e="T1380" id="Seg_9344" s="T1379">kan-bi-m</ta>
            <ta e="T1381" id="Seg_9345" s="T1380">dĭ</ta>
            <ta e="T1382" id="Seg_9346" s="T1381">dĭ</ta>
            <ta e="T1383" id="Seg_9347" s="T1382">bĭdə-bi</ta>
            <ta e="T1384" id="Seg_9348" s="T1383">măna</ta>
            <ta e="T1386" id="Seg_9349" s="T1385">bazoʔ</ta>
            <ta e="T1387" id="Seg_9350" s="T1386">det-bi</ta>
            <ta e="T1388" id="Seg_9351" s="T1387">maʔ-gəndə</ta>
            <ta e="T1389" id="Seg_9352" s="T1388">dĭgəttə</ta>
            <ta e="T1390" id="Seg_9353" s="T1389">măn</ta>
            <ta e="T1391" id="Seg_9354" s="T1390">ija-m</ta>
            <ta e="T1392" id="Seg_9355" s="T1391">šo-bi</ta>
            <ta e="T1394" id="Seg_9356" s="T1393">măn</ta>
            <ta e="T1395" id="Seg_9357" s="T1394">bazoʔ</ta>
            <ta e="T1396" id="Seg_9358" s="T1395">kan-lAʔ</ta>
            <ta e="T1397" id="Seg_9359" s="T1396">tʼür-bi-m</ta>
            <ta e="T1399" id="Seg_9360" s="T1398">dĭ</ta>
            <ta e="T1400" id="Seg_9361" s="T1399">šo-bi</ta>
            <ta e="T1401" id="Seg_9362" s="T1400">da</ta>
            <ta e="T1402" id="Seg_9363" s="T1401">măna</ta>
            <ta e="T1403" id="Seg_9364" s="T1402">kun-laːm-bi</ta>
            <ta e="T1404" id="Seg_9365" s="T1403">maʔ-gənʼi</ta>
            <ta e="T1405" id="Seg_9366" s="T1404">iʔbə-bAʔ</ta>
            <ta e="T1406" id="Seg_9367" s="T1405">kunol-zittə</ta>
            <ta e="T1407" id="Seg_9368" s="T1406">dĭgəttə</ta>
            <ta e="T1408" id="Seg_9369" s="T1407">dĭ-zAŋ</ta>
            <ta e="T1410" id="Seg_9370" s="T1409">šo-bi-jəʔ</ta>
            <ta e="T1412" id="Seg_9371" s="T1411">no</ta>
            <ta e="T1413" id="Seg_9372" s="T1412">amno-KAʔ</ta>
            <ta e="T1415" id="Seg_9373" s="T1414">dĭgəttə</ta>
            <ta e="T1416" id="Seg_9374" s="T1415">miʔnʼibeʔ</ta>
            <ta e="T1417" id="Seg_9375" s="T1416">kudaj-Tə</ta>
            <ta e="T1418" id="Seg_9376" s="T1417">numan üzə-bi-jəʔ</ta>
            <ta e="T1420" id="Seg_9377" s="T1419">dĭgəttə</ta>
            <ta e="T1421" id="Seg_9378" s="T1420">kan-bi-jəʔ</ta>
            <ta e="T1423" id="Seg_9379" s="T1422">karəldʼaːn</ta>
            <ta e="T1424" id="Seg_9380" s="T1423">uʔbdə-bi-jəʔ</ta>
            <ta e="T1425" id="Seg_9381" s="T1424">miʔnʼibeʔ</ta>
            <ta e="T1426" id="Seg_9382" s="T1425">šo-bi-jəʔ</ta>
            <ta e="T1428" id="Seg_9383" s="T1427">amor-bi-jəʔ</ta>
            <ta e="T1429" id="Seg_9384" s="T1428">dĭgəttə</ta>
            <ta e="T1430" id="Seg_9385" s="T1429">kan-lAʔ</ta>
            <ta e="T1431" id="Seg_9386" s="T1430">tʼür-bi</ta>
            <ta e="T1432" id="Seg_9387" s="T1431">maʔ-gəndə</ta>
            <ta e="T1434" id="Seg_9388" s="T1433">dĭgəttə</ta>
            <ta e="T1435" id="Seg_9389" s="T1434">dĭ</ta>
            <ta e="T1436" id="Seg_9390" s="T1435">kan-bi</ta>
            <ta e="T1437" id="Seg_9391" s="T1436">dʼadʼa-Tə</ta>
            <ta e="T1438" id="Seg_9392" s="T1437">noʔ</ta>
            <ta e="T1439" id="Seg_9393" s="T1438">hʼaʔ-zittə</ta>
            <ta e="T1441" id="Seg_9394" s="T1440">i</ta>
            <ta e="T1442" id="Seg_9395" s="T1441">dĭn</ta>
            <ta e="T1443" id="Seg_9396" s="T1442">tʼala</ta>
            <ta e="T1445" id="Seg_9397" s="T1444">dĭrgit</ta>
            <ta e="T1446" id="Seg_9398" s="T1445">i-bi</ta>
            <ta e="T1447" id="Seg_9399" s="T1446">dĭ</ta>
            <ta e="T1448" id="Seg_9400" s="T1447">bü-nzə-ziʔ</ta>
            <ta e="T1449" id="Seg_9401" s="T1448">bos-gəndə</ta>
            <ta e="T1450" id="Seg_9402" s="T1449">dĭgəttə</ta>
            <ta e="T1451" id="Seg_9403" s="T1450">kan-bi-jəʔ</ta>
            <ta e="T1452" id="Seg_9404" s="T1451">bü-Tə</ta>
            <ta e="T1454" id="Seg_9405" s="T1453">i</ta>
            <ta e="T1455" id="Seg_9406" s="T1454">dĭn</ta>
            <ta e="T1456" id="Seg_9407" s="T1455">šü-bi-jəʔ</ta>
            <ta e="T1457" id="Seg_9408" s="T1456">bü-Tə</ta>
            <ta e="T1458" id="Seg_9409" s="T1457">onʼiʔ</ta>
            <ta e="T1459" id="Seg_9410" s="T1458">koʔbdo</ta>
            <ta e="T1460" id="Seg_9411" s="T1459">i</ta>
            <ta e="T1461" id="Seg_9412" s="T1460">dĭ</ta>
            <ta e="T1463" id="Seg_9413" s="T1462">bü-Kən</ta>
            <ta e="T1465" id="Seg_9414" s="T1464">dĭgəttə</ta>
            <ta e="T1466" id="Seg_9415" s="T1465">pa</ta>
            <ta e="T1467" id="Seg_9416" s="T1466">onʼiʔ</ta>
            <ta e="T1468" id="Seg_9417" s="T1467">nʼi</ta>
            <ta e="T1469" id="Seg_9418" s="T1468">mĭ-bi</ta>
            <ta e="T1470" id="Seg_9419" s="T1469">dĭ</ta>
            <ta e="T1471" id="Seg_9420" s="T1470">koʔbdo-m</ta>
            <ta e="T1472" id="Seg_9421" s="T1471">det-bi</ta>
            <ta e="T1473" id="Seg_9422" s="T1472">a</ta>
            <ta e="T1474" id="Seg_9423" s="T1473">dĭ</ta>
            <ta e="T1475" id="Seg_9424" s="T1474">kü-laːm-bi</ta>
            <ta e="T1477" id="Seg_9425" s="T1476">bü-Kən</ta>
            <ta e="T1498" id="Seg_9426" s="T1495">nu</ta>
            <ta e="T1500" id="Seg_9427" s="T1499">karəldʼaːn</ta>
            <ta e="T1506" id="Seg_9428" s="T1503">šo</ta>
            <ta e="T1508" id="Seg_9429" s="T1506">karəldʼaːn</ta>
            <ta e="T1587" id="Seg_9430" s="T1586">măn</ta>
            <ta e="T1588" id="Seg_9431" s="T1587">karəldʼaːn</ta>
            <ta e="T1589" id="Seg_9432" s="T1588">edəʔ-laʔbə-m</ta>
            <ta e="T1590" id="Seg_9433" s="T1589">Arpit-ə-m</ta>
            <ta e="T1592" id="Seg_9434" s="T1591">dĭ</ta>
            <ta e="T1594" id="Seg_9435" s="T1593">măn-bi</ta>
            <ta e="T1595" id="Seg_9436" s="T1594">šo-lV-m</ta>
            <ta e="T1596" id="Seg_9437" s="T1595">il-m</ta>
            <ta e="T1597" id="Seg_9438" s="T1596">tănan</ta>
            <ta e="T1598" id="Seg_9439" s="T1597">kan-žə-bəj</ta>
            <ta e="T1599" id="Seg_9440" s="T1598">măn</ta>
            <ta e="T1600" id="Seg_9441" s="T1599">ija-Tə</ta>
            <ta e="T1602" id="Seg_9442" s="T1601">dĭgəttə</ta>
            <ta e="T1603" id="Seg_9443" s="T1602">măn</ta>
            <ta e="T1604" id="Seg_9444" s="T1603">gibər=də</ta>
            <ta e="T1605" id="Seg_9445" s="T1604">ej</ta>
            <ta e="T1606" id="Seg_9446" s="T1605">kan-lV-m</ta>
            <ta e="T1607" id="Seg_9447" s="T1606">edəʔ-lV-m</ta>
            <ta e="T1608" id="Seg_9448" s="T1607">dĭ-m</ta>
            <ta e="T1610" id="Seg_9449" s="T1609">kamən</ta>
            <ta e="T1611" id="Seg_9450" s="T1610">dĭ</ta>
            <ta e="T1612" id="Seg_9451" s="T1611">šo-lV-j</ta>
            <ta e="T1651" id="Seg_9452" s="T1650">măn</ta>
            <ta e="T1652" id="Seg_9453" s="T1651">teinen</ta>
            <ta e="T1654" id="Seg_9454" s="T1653">kunol-bi-m</ta>
            <ta e="T1655" id="Seg_9455" s="T1654">i</ta>
            <ta e="T1656" id="Seg_9456" s="T1655">dʼodə-gənʼi</ta>
            <ta e="T1657" id="Seg_9457" s="T1656">ku-bi-m</ta>
            <ta e="T1658" id="Seg_9458" s="T1657">onʼiʔ</ta>
            <ta e="T1659" id="Seg_9459" s="T1658">kuza</ta>
            <ta e="T1661" id="Seg_9460" s="T1660">dĭ</ta>
            <ta e="T1662" id="Seg_9461" s="T1661">măna</ta>
            <ta e="T1664" id="Seg_9462" s="T1663">măn-ntə</ta>
            <ta e="T1665" id="Seg_9463" s="T1664">kan-žə-bəj</ta>
            <ta e="T1666" id="Seg_9464" s="T1665">gibər=nʼibudʼ</ta>
            <ta e="T1668" id="Seg_9465" s="T1667">măn</ta>
            <ta e="T1669" id="Seg_9466" s="T1668">măn-ntə-m</ta>
            <ta e="T1670" id="Seg_9467" s="T1669">ej</ta>
            <ta e="T1671" id="Seg_9468" s="T1670">kan-lV-m</ta>
            <ta e="T1698" id="Seg_9469" s="T1697">măn</ta>
            <ta e="T1699" id="Seg_9470" s="T1698">kunol-bi-m</ta>
            <ta e="T1700" id="Seg_9471" s="T1699">taldʼen</ta>
            <ta e="T1701" id="Seg_9472" s="T1700">i</ta>
            <ta e="T1702" id="Seg_9473" s="T1701">dʼodə-gənʼi</ta>
            <ta e="T1703" id="Seg_9474" s="T1702">ku-bi-m</ta>
            <ta e="T1704" id="Seg_9475" s="T1703">bos-də</ta>
            <ta e="T1705" id="Seg_9476" s="T1704">sʼestra-nə</ta>
            <ta e="T1706" id="Seg_9477" s="T1705">tibi</ta>
            <ta e="T1709" id="Seg_9478" s="T1708">dĭgəttə</ta>
            <ta e="T1710" id="Seg_9479" s="T1709">kan-bi-m</ta>
            <ta e="T1711" id="Seg_9480" s="T1710">šiʔnʼileʔ</ta>
            <ta e="T1712" id="Seg_9481" s="T1711">tʼăbaktər-bi-m</ta>
            <ta e="T1715" id="Seg_9482" s="T1714">dĭgəttə</ta>
            <ta e="T1716" id="Seg_9483" s="T1715">măn-bi-m</ta>
            <ta e="T1717" id="Seg_9484" s="T1716">šində=də</ta>
            <ta e="T1718" id="Seg_9485" s="T1717">tʼăbaktər-lV-j</ta>
            <ta e="T1719" id="Seg_9486" s="T1718">măn-ziʔ</ta>
            <ta e="T1721" id="Seg_9487" s="T1720">dĭgəttə</ta>
            <ta e="T1725" id="Seg_9488" s="T1722">koŋ</ta>
            <ta e="T1726" id="Seg_9489" s="T1725">šo-bi-jəʔ</ta>
            <ta e="T1727" id="Seg_9490" s="T1726">tʼăbaktər-zittə</ta>
            <ta e="T1728" id="Seg_9491" s="T1727">măn-ziʔ</ta>
            <ta e="T1730" id="Seg_9492" s="T1729">dĭgəttə</ta>
            <ta e="T1731" id="Seg_9493" s="T1730">dĭn</ta>
            <ta e="T1800" id="Seg_9494" s="T1799">miʔ</ta>
            <ta e="T1801" id="Seg_9495" s="T1800">mĭn-bi-bAʔ</ta>
            <ta e="T1802" id="Seg_9496" s="T1801">dĭ</ta>
            <ta e="T1803" id="Seg_9497" s="T1802">nüke</ta>
            <ta e="T1804" id="Seg_9498" s="T1803">i-bi</ta>
            <ta e="T1805" id="Seg_9499" s="T1804">tʼala</ta>
            <ta e="T1806" id="Seg_9500" s="T1805">kamən</ta>
            <ta e="T1807" id="Seg_9501" s="T1806">dĭ</ta>
            <ta e="T1808" id="Seg_9502" s="T1807">ija-t</ta>
            <ta e="T1809" id="Seg_9503" s="T1808">det-bi</ta>
            <ta e="T1811" id="Seg_9504" s="T1810">măn</ta>
            <ta e="T1812" id="Seg_9505" s="T1811">mĭn-bi-m</ta>
            <ta e="T1813" id="Seg_9506" s="T1812">nagur</ta>
            <ta e="T1814" id="Seg_9507" s="T1813">aktʼa</ta>
            <ta e="T1815" id="Seg_9508" s="T1814">dĭ-Tə</ta>
            <ta e="T1816" id="Seg_9509" s="T1815">mĭ-bi-m</ta>
            <ta e="T1818" id="Seg_9510" s="T1817">dĭ</ta>
            <ta e="T1820" id="Seg_9511" s="T1819">miʔnʼibeʔ</ta>
            <ta e="T1821" id="Seg_9512" s="T1820">bădə-bi</ta>
            <ta e="T1822" id="Seg_9513" s="T1821">munəj-jəʔ</ta>
            <ta e="T1823" id="Seg_9514" s="T1822">hen-bi</ta>
            <ta e="T1825" id="Seg_9515" s="T1824">kajaʔ</ta>
            <ta e="T1826" id="Seg_9516" s="T1825">hen-bi</ta>
            <ta e="T1827" id="Seg_9517" s="T1826">köbergən</ta>
            <ta e="T1829" id="Seg_9518" s="T1828">măn-ntə</ta>
            <ta e="T1830" id="Seg_9519" s="T1829">am-ə-ʔ</ta>
            <ta e="T1831" id="Seg_9520" s="T1830">am-ə-ʔ</ta>
            <ta e="T1833" id="Seg_9521" s="T1832">măn</ta>
            <ta e="T1834" id="Seg_9522" s="T1833">am-bi-m</ta>
            <ta e="T1835" id="Seg_9523" s="T1834">idʼiʔeʔe</ta>
            <ta e="T1837" id="Seg_9524" s="T1836">dĭgəttə</ta>
            <ta e="T1838" id="Seg_9525" s="T1837">onʼiʔ</ta>
            <ta e="T1839" id="Seg_9526" s="T1838">takše</ta>
            <ta e="T1840" id="Seg_9527" s="T1839">segi</ta>
            <ta e="T1841" id="Seg_9528" s="T1840">bü</ta>
            <ta e="T1842" id="Seg_9529" s="T1841">bĭs-bi-m</ta>
            <ta e="T1844" id="Seg_9530" s="T1843">măn-bi-m</ta>
            <ta e="T1845" id="Seg_9531" s="T1844">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T379" id="Seg_9532" s="T378">single</ta>
            <ta e="T380" id="Seg_9533" s="T379">Jew.[NOM.SG]</ta>
            <ta e="T381" id="Seg_9534" s="T380">come-PST.[3SG]</ta>
            <ta e="T382" id="Seg_9535" s="T381">one.[NOM.SG]</ta>
            <ta e="T383" id="Seg_9536" s="T382">Tatar</ta>
            <ta e="T384" id="Seg_9537" s="T383">man.[NOM.SG]</ta>
            <ta e="T385" id="Seg_9538" s="T384">come-PST.[3SG]</ta>
            <ta e="T394" id="Seg_9539" s="T393">Tatar-PL</ta>
            <ta e="T396" id="Seg_9540" s="T395">then</ta>
            <ta e="T397" id="Seg_9541" s="T396">niece.[NOM.SG]</ta>
            <ta e="T398" id="Seg_9542" s="T397">I.LAT</ta>
            <ta e="T399" id="Seg_9543" s="T398">grass.[NOM.SG]</ta>
            <ta e="T400" id="Seg_9544" s="T399">go-PST.[3SG]</ta>
            <ta e="T402" id="Seg_9545" s="T401">we.NOM</ta>
            <ta e="T403" id="Seg_9546" s="T402">this.[NOM.SG]</ta>
            <ta e="T404" id="Seg_9547" s="T403">haystack-PL</ta>
            <ta e="T405" id="Seg_9548" s="T404">put-PST-1PL</ta>
            <ta e="T407" id="Seg_9549" s="T406">then</ta>
            <ta e="T408" id="Seg_9550" s="T407">one.[NOM.SG]</ta>
            <ta e="T410" id="Seg_9551" s="T409">fall-PST.[3SG]</ta>
            <ta e="T412" id="Seg_9552" s="T411">I.NOM</ta>
            <ta e="T413" id="Seg_9553" s="T412">say-PST-1SG</ta>
            <ta e="T414" id="Seg_9554" s="T413">give-IMP.2SG</ta>
            <ta e="T415" id="Seg_9555" s="T414">three-NOM/GEN/ACC.1PL</ta>
            <ta e="T416" id="Seg_9556" s="T415">be-PST-1PL</ta>
            <ta e="T418" id="Seg_9557" s="T417">I.NOM</ta>
            <ta e="T420" id="Seg_9558" s="T418">say-PST-1SG</ta>
            <ta e="T421" id="Seg_9559" s="T420">well</ta>
            <ta e="T422" id="Seg_9560" s="T421">see-IMP.2SG.O</ta>
            <ta e="T423" id="Seg_9561" s="T422">see-%%</ta>
            <ta e="T424" id="Seg_9562" s="T423">fall-FUT-PST</ta>
            <ta e="T426" id="Seg_9563" s="T425">I.NOM</ta>
            <ta e="T427" id="Seg_9564" s="T426">say-PST-1SG</ta>
            <ta e="T428" id="Seg_9565" s="T427">who.[NOM.SG]=INDEF</ta>
            <ta e="T430" id="Seg_9566" s="T429">die-RES-FUT-3SG</ta>
            <ta e="T432" id="Seg_9567" s="T431">then</ta>
            <ta e="T433" id="Seg_9568" s="T432">I.NOM</ta>
            <ta e="T434" id="Seg_9569" s="T433">say-PRS-1SG</ta>
            <ta e="T435" id="Seg_9570" s="T434">this</ta>
            <ta e="T436" id="Seg_9571" s="T435">I.NOM</ta>
            <ta e="T437" id="Seg_9572" s="T436">die-RES-FUT-1SG</ta>
            <ta e="T439" id="Seg_9573" s="T438">I.NOM</ta>
            <ta e="T440" id="Seg_9574" s="T439">old.[NOM.SG]</ta>
            <ta e="T441" id="Seg_9575" s="T440">you.know</ta>
            <ta e="T443" id="Seg_9576" s="T442">and</ta>
            <ta e="T444" id="Seg_9577" s="T443">you.PL.NOM</ta>
            <ta e="T445" id="Seg_9578" s="T444">more</ta>
            <ta e="T446" id="Seg_9579" s="T445">NEG</ta>
            <ta e="T447" id="Seg_9580" s="T446">old.[NOM.SG]</ta>
            <ta e="T450" id="Seg_9581" s="T449">then</ta>
            <ta e="T451" id="Seg_9582" s="T450">tent-LAT/LOC.1PL</ta>
            <ta e="T452" id="Seg_9583" s="T451">come-PST-1PL</ta>
            <ta e="T454" id="Seg_9584" s="T453">I.NOM</ta>
            <ta e="T455" id="Seg_9585" s="T454">when</ta>
            <ta e="T456" id="Seg_9586" s="T455">this.[NOM.SG]</ta>
            <ta e="T457" id="Seg_9587" s="T456">this.[NOM.SG]</ta>
            <ta e="T458" id="Seg_9588" s="T457">come-PST.[3SG]</ta>
            <ta e="T459" id="Seg_9589" s="T458">here</ta>
            <ta e="T460" id="Seg_9590" s="T459">I.NOM</ta>
            <ta e="T461" id="Seg_9591" s="T460">go-PST-1SG</ta>
            <ta e="T462" id="Seg_9592" s="T461">onion.[NOM.SG]</ta>
            <ta e="T463" id="Seg_9593" s="T462">tear-PST-1SG</ta>
            <ta e="T466" id="Seg_9594" s="T465">larch.[NOM.SG]</ta>
            <ta e="T467" id="Seg_9595" s="T466">tree.[NOM.SG]</ta>
            <ta e="T468" id="Seg_9596" s="T467">wind.[NOM.SG]</ta>
            <ta e="T469" id="Seg_9597" s="T468">NEG.EX-PST.[3SG]</ta>
            <ta e="T470" id="Seg_9598" s="T469">place-LAT</ta>
            <ta e="T471" id="Seg_9599" s="T470">fall-MOM-PST.[3SG]</ta>
            <ta e="T473" id="Seg_9600" s="T472">I.NOM</ta>
            <ta e="T474" id="Seg_9601" s="T473">I.NOM</ta>
            <ta e="T475" id="Seg_9602" s="T474">%%</ta>
            <ta e="T476" id="Seg_9603" s="T475">who-LAT=INDEF</ta>
            <ta e="T477" id="Seg_9604" s="T476">NEG</ta>
            <ta e="T478" id="Seg_9605" s="T477">say-PST-1SG</ta>
            <ta e="T479" id="Seg_9606" s="T478">and</ta>
            <ta e="T480" id="Seg_9607" s="T479">then</ta>
            <ta e="T481" id="Seg_9608" s="T480">there</ta>
            <ta e="T482" id="Seg_9609" s="T481">man-NOM/GEN.3SG</ta>
            <ta e="T483" id="Seg_9610" s="T482">when</ta>
            <ta e="T484" id="Seg_9611" s="T483">water-LOC</ta>
            <ta e="T485" id="Seg_9612" s="T484">die-RES-PST.[3SG]</ta>
            <ta e="T486" id="Seg_9613" s="T485">I.NOM</ta>
            <ta e="T487" id="Seg_9614" s="T486">say-PST-1SG</ta>
            <ta e="T573" id="Seg_9615" s="T257">when</ta>
            <ta e="T574" id="Seg_9616" s="T573">I.NOM</ta>
            <ta e="T575" id="Seg_9617" s="T574">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T576" id="Seg_9618" s="T575">come-PST.[3SG]</ta>
            <ta e="T577" id="Seg_9619" s="T576">there</ta>
            <ta e="T578" id="Seg_9620" s="T577">settlement-LOC</ta>
            <ta e="T579" id="Seg_9621" s="T578">this.[NOM.SG]</ta>
            <ta e="T580" id="Seg_9622" s="T579">settlement-LAT/LOC.3SG</ta>
            <ta e="T581" id="Seg_9623" s="T580">Spasskoe-LAT</ta>
            <ta e="T582" id="Seg_9624" s="T581">house-LAT/LOC.1SG</ta>
            <ta e="T584" id="Seg_9625" s="T583">then</ta>
            <ta e="T585" id="Seg_9626" s="T584">come-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_9627" s="T585">larch.[NOM.SG]</ta>
            <ta e="T587" id="Seg_9628" s="T586">tree.[NOM.SG]</ta>
            <ta e="T588" id="Seg_9629" s="T587">stand-DUR.[3SG]</ta>
            <ta e="T590" id="Seg_9630" s="T589">then</ta>
            <ta e="T591" id="Seg_9631" s="T590">tree-ABL</ta>
            <ta e="T594" id="Seg_9632" s="T593">place-LAT</ta>
            <ta e="T595" id="Seg_9633" s="T594">fall-MOM-PST.[3SG]</ta>
            <ta e="T597" id="Seg_9634" s="T596">then</ta>
            <ta e="T598" id="Seg_9635" s="T597">come</ta>
            <ta e="T599" id="Seg_9636" s="T598">this.[NOM.SG]</ta>
            <ta e="T600" id="Seg_9637" s="T599">come-PST.[3SG]</ta>
            <ta e="T601" id="Seg_9638" s="T600">house-LAT/LOC.3SG</ta>
            <ta e="T602" id="Seg_9639" s="T601">I.LAT</ta>
            <ta e="T603" id="Seg_9640" s="T602">tell-PRS.[3SG]</ta>
            <ta e="T605" id="Seg_9641" s="T604">I.NOM</ta>
            <ta e="T607" id="Seg_9642" s="T606">say-PST-1SG</ta>
            <ta e="T608" id="Seg_9643" s="T607">who.[NOM.SG]=INDEF</ta>
            <ta e="T609" id="Seg_9644" s="T608">child-PL-LAT</ta>
            <ta e="T610" id="Seg_9645" s="T609">die-RES-FUT-3SG</ta>
            <ta e="T612" id="Seg_9646" s="T611">and</ta>
            <ta e="T613" id="Seg_9647" s="T612">then</ta>
            <ta e="T614" id="Seg_9648" s="T613">so</ta>
            <ta e="T615" id="Seg_9649" s="T614">this.[NOM.SG]</ta>
            <ta e="T616" id="Seg_9650" s="T615">and</ta>
            <ta e="T617" id="Seg_9651" s="T616">be-PST.[3SG]</ta>
            <ta e="T619" id="Seg_9652" s="T618">I.NOM</ta>
            <ta e="T620" id="Seg_9653" s="T619">I.GEN</ta>
            <ta e="T621" id="Seg_9654" s="T620">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_9655" s="T621">go-PST.[3SG]</ta>
            <ta e="T623" id="Seg_9656" s="T622">water-LOC</ta>
            <ta e="T624" id="Seg_9657" s="T623">and</ta>
            <ta e="T625" id="Seg_9658" s="T624">there</ta>
            <ta e="T626" id="Seg_9659" s="T625">die-RES-PST.[3SG]</ta>
            <ta e="T1263" id="Seg_9660" s="T1262">when</ta>
            <ta e="T1264" id="Seg_9661" s="T1263">this.[NOM.SG]</ta>
            <ta e="T1265" id="Seg_9662" s="T1264">come-PST.[3SG]</ta>
            <ta e="T1266" id="Seg_9663" s="T1265">I.LAT</ta>
            <ta e="T1267" id="Seg_9664" s="T1266">go-OPT.DU/PL-1DU</ta>
            <ta e="T1268" id="Seg_9665" s="T1267">there</ta>
            <ta e="T1269" id="Seg_9666" s="T1268">mother-NOM/GEN.3SG</ta>
            <ta e="T1270" id="Seg_9667" s="T1269">man-LAT</ta>
            <ta e="T1271" id="Seg_9668" s="T1270">go-CVB</ta>
            <ta e="T1272" id="Seg_9669" s="T1271">disappear-PST.[3SG]</ta>
            <ta e="T1273" id="Seg_9670" s="T1272">I.NOM</ta>
            <ta e="T1274" id="Seg_9671" s="T1273">alone</ta>
            <ta e="T1275" id="Seg_9672" s="T1274">live-DUR-1SG</ta>
            <ta e="T1277" id="Seg_9673" s="T1276">I.NOM</ta>
            <ta e="T1278" id="Seg_9674" s="T1277">say-PST-1SG</ta>
            <ta e="T1279" id="Seg_9675" s="T1278">go-EP-IMP.2SG</ta>
            <ta e="T1280" id="Seg_9676" s="T1279">I.LAT</ta>
            <ta e="T1281" id="Seg_9677" s="T1280">marry-IMP.2SG</ta>
            <ta e="T1282" id="Seg_9678" s="T1281">father-LAT</ta>
            <ta e="T1283" id="Seg_9679" s="T1282">and</ta>
            <ta e="T1284" id="Seg_9680" s="T1283">mother-LAT</ta>
            <ta e="T1285" id="Seg_9681" s="T1284">this.[NOM.SG]</ta>
            <ta e="T1286" id="Seg_9682" s="T1285">say-IPFVZ.[3SG]</ta>
            <ta e="T1287" id="Seg_9683" s="T1286">this-PL</ta>
            <ta e="T1288" id="Seg_9684" s="T1287">NEG</ta>
            <ta e="T1289" id="Seg_9685" s="T1288">give-FUT-3PL</ta>
            <ta e="T1290" id="Seg_9686" s="T1289">go-OPT.DU/PL-1DU</ta>
            <ta e="T1292" id="Seg_9687" s="T1291">then</ta>
            <ta e="T1293" id="Seg_9688" s="T1292">I.NOM</ta>
            <ta e="T1294" id="Seg_9689" s="T1293">clothing-ACC</ta>
            <ta e="T1295" id="Seg_9690" s="T1294">collect-PST-1SG</ta>
            <ta e="T1297" id="Seg_9691" s="T1296">evening-LOC.ADV</ta>
            <ta e="T1298" id="Seg_9692" s="T1297">horse-ACC</ta>
            <ta e="T1299" id="Seg_9693" s="T1298">take.away-TR-DUR.PST-1PL</ta>
            <ta e="T1300" id="Seg_9694" s="T1299">sit-PST-1PL</ta>
            <ta e="T1301" id="Seg_9695" s="T1300">go-PST-1PL</ta>
            <ta e="T1302" id="Seg_9696" s="T1301">Aginskoe</ta>
            <ta e="T1303" id="Seg_9697" s="T1302">settlement-LAT</ta>
            <ta e="T1305" id="Seg_9698" s="T1304">PTCL</ta>
            <ta e="T1306" id="Seg_9699" s="T1305">evening.[NOM.SG]</ta>
            <ta e="T1307" id="Seg_9700" s="T1306">there</ta>
            <ta e="T1308" id="Seg_9701" s="T1307">sleep-PST-1PL</ta>
            <ta e="T1309" id="Seg_9702" s="T1308">then</ta>
            <ta e="T1310" id="Seg_9703" s="T1309">come-PST-1PL</ta>
            <ta e="T1311" id="Seg_9704" s="T1310">again</ta>
            <ta e="T1312" id="Seg_9705" s="T1311">this-LAT</ta>
            <ta e="T1313" id="Seg_9706" s="T1312">house-LAT</ta>
            <ta e="T1314" id="Seg_9707" s="T1313">Kaptyrlyk-LAT</ta>
            <ta e="T1316" id="Seg_9708" s="T1315">there</ta>
            <ta e="T1317" id="Seg_9709" s="T1316">whole</ta>
            <ta e="T1319" id="Seg_9710" s="T1318">day.[NOM.SG]</ta>
            <ta e="T1320" id="Seg_9711" s="T1319">be-PST-1PL</ta>
            <ta e="T1321" id="Seg_9712" s="T1320">then</ta>
            <ta e="T1322" id="Seg_9713" s="T1321">evening-LOC.ADV</ta>
            <ta e="T1323" id="Seg_9714" s="T1322">come-PST-1PL</ta>
            <ta e="T1324" id="Seg_9715" s="T1323">this.[NOM.SG]</ta>
            <ta e="T1325" id="Seg_9716" s="T1324">there</ta>
            <ta e="T1326" id="Seg_9717" s="T1325">house-LAT</ta>
            <ta e="T1328" id="Seg_9718" s="T1327">then</ta>
            <ta e="T1329" id="Seg_9719" s="T1328">this.[NOM.SG]</ta>
            <ta e="T1330" id="Seg_9720" s="T1329">morning-LOC.ADV</ta>
            <ta e="T1331" id="Seg_9721" s="T1330">come-PST.[3SG]</ta>
            <ta e="T1332" id="Seg_9722" s="T1331">there</ta>
            <ta e="T1333" id="Seg_9723" s="T1332">Russian</ta>
            <ta e="T1334" id="Seg_9724" s="T1333">mother-NOM/GEN.3SG</ta>
            <ta e="T1336" id="Seg_9725" s="T1335">then</ta>
            <ta e="T1337" id="Seg_9726" s="T1336">get.up-PST-1PL</ta>
            <ta e="T1339" id="Seg_9727" s="T1338">there</ta>
            <ta e="T1340" id="Seg_9728" s="T1339">sit-PST-1SG</ta>
            <ta e="T1343" id="Seg_9729" s="T1342">grass.[NOM.SG]</ta>
            <ta e="T1344" id="Seg_9730" s="T1343">cut-PST-1PL</ta>
            <ta e="T1345" id="Seg_9731" s="T1344">there</ta>
            <ta e="T1346" id="Seg_9732" s="T1345">PTCL</ta>
            <ta e="T1348" id="Seg_9733" s="T1347">then</ta>
            <ta e="T1349" id="Seg_9734" s="T1348">there</ta>
            <ta e="T1350" id="Seg_9735" s="T1349">uncle-NOM/GEN.3SG</ta>
            <ta e="T1351" id="Seg_9736" s="T1350">come-PST.[3SG]</ta>
            <ta e="T1353" id="Seg_9737" s="T1352">come-IMP.2SG</ta>
            <ta e="T1354" id="Seg_9738" s="T1353">I.LAT</ta>
            <ta e="T1355" id="Seg_9739" s="T1354">grass.[NOM.SG]</ta>
            <ta e="T1356" id="Seg_9740" s="T1355">cut-INF.LAT</ta>
            <ta e="T1358" id="Seg_9741" s="T1357">then</ta>
            <ta e="T1359" id="Seg_9742" s="T1358">this.[NOM.SG]</ta>
            <ta e="T1360" id="Seg_9743" s="T1359">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T1361" id="Seg_9744" s="T1360">bring-PST.[3SG]</ta>
            <ta e="T1363" id="Seg_9745" s="T1362">I.LAT</ta>
            <ta e="T1364" id="Seg_9746" s="T1363">tell-PST-3PL</ta>
            <ta e="T1365" id="Seg_9747" s="T1364">this.[NOM.SG]</ta>
            <ta e="T1366" id="Seg_9748" s="T1365">this-COM</ta>
            <ta e="T1367" id="Seg_9749" s="T1366">live-INF.LAT</ta>
            <ta e="T1368" id="Seg_9750" s="T1367">live-FUT-3SG</ta>
            <ta e="T1370" id="Seg_9751" s="T1369">I.NOM</ta>
            <ta e="T1371" id="Seg_9752" s="T1370">PTCL</ta>
            <ta e="T1372" id="Seg_9753" s="T1371">clothing-ACC</ta>
            <ta e="T1373" id="Seg_9754" s="T1372">take-PST-1SG</ta>
            <ta e="T1374" id="Seg_9755" s="T1373">and</ta>
            <ta e="T1375" id="Seg_9756" s="T1374">this.[NOM.SG]</ta>
            <ta e="T1377" id="Seg_9757" s="T1376">this.[NOM.SG]</ta>
            <ta e="T1378" id="Seg_9758" s="T1377">there</ta>
            <ta e="T1379" id="Seg_9759" s="T1378">uncle-COM</ta>
            <ta e="T1380" id="Seg_9760" s="T1379">go-PST-1SG</ta>
            <ta e="T1381" id="Seg_9761" s="T1380">this</ta>
            <ta e="T1382" id="Seg_9762" s="T1381">this.[NOM.SG]</ta>
            <ta e="T1383" id="Seg_9763" s="T1382">catch.up-PST.[3SG]</ta>
            <ta e="T1384" id="Seg_9764" s="T1383">I.LAT</ta>
            <ta e="T1386" id="Seg_9765" s="T1385">again</ta>
            <ta e="T1387" id="Seg_9766" s="T1386">bring-PST.[3SG]</ta>
            <ta e="T1388" id="Seg_9767" s="T1387">house-LAT/LOC.3SG</ta>
            <ta e="T1389" id="Seg_9768" s="T1388">then</ta>
            <ta e="T1390" id="Seg_9769" s="T1389">I.NOM</ta>
            <ta e="T1391" id="Seg_9770" s="T1390">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T1392" id="Seg_9771" s="T1391">come-PST.[3SG]</ta>
            <ta e="T1394" id="Seg_9772" s="T1393">I.NOM</ta>
            <ta e="T1395" id="Seg_9773" s="T1394">again</ta>
            <ta e="T1396" id="Seg_9774" s="T1395">go-CVB</ta>
            <ta e="T1397" id="Seg_9775" s="T1396">disappear-PST-1SG</ta>
            <ta e="T1399" id="Seg_9776" s="T1398">this.[NOM.SG]</ta>
            <ta e="T1400" id="Seg_9777" s="T1399">come-PST.[3SG]</ta>
            <ta e="T1401" id="Seg_9778" s="T1400">and</ta>
            <ta e="T1402" id="Seg_9779" s="T1401">I.LAT</ta>
            <ta e="T1403" id="Seg_9780" s="T1402">bring-RES-PST.[3SG]</ta>
            <ta e="T1404" id="Seg_9781" s="T1403">house-LAT/LOC.1SG</ta>
            <ta e="T1405" id="Seg_9782" s="T1404">lie.down-1PL</ta>
            <ta e="T1406" id="Seg_9783" s="T1405">sleep-INF.LAT</ta>
            <ta e="T1407" id="Seg_9784" s="T1406">then</ta>
            <ta e="T1408" id="Seg_9785" s="T1407">this-PL</ta>
            <ta e="T1410" id="Seg_9786" s="T1409">come-PST-3PL</ta>
            <ta e="T1412" id="Seg_9787" s="T1411">well</ta>
            <ta e="T1413" id="Seg_9788" s="T1412">live-IMP.2PL</ta>
            <ta e="T1415" id="Seg_9789" s="T1414">then</ta>
            <ta e="T1416" id="Seg_9790" s="T1415">we.ACC</ta>
            <ta e="T1417" id="Seg_9791" s="T1416">God-LAT</ta>
            <ta e="T1418" id="Seg_9792" s="T1417">pray-PST-3PL</ta>
            <ta e="T1420" id="Seg_9793" s="T1419">then</ta>
            <ta e="T1421" id="Seg_9794" s="T1420">go-PST-3PL</ta>
            <ta e="T1423" id="Seg_9795" s="T1422">tomorrow</ta>
            <ta e="T1424" id="Seg_9796" s="T1423">get.up-PST-3PL</ta>
            <ta e="T1425" id="Seg_9797" s="T1424">we.LAT</ta>
            <ta e="T1426" id="Seg_9798" s="T1425">come-PST-3PL</ta>
            <ta e="T1428" id="Seg_9799" s="T1427">eat-PST-3PL</ta>
            <ta e="T1429" id="Seg_9800" s="T1428">then</ta>
            <ta e="T1430" id="Seg_9801" s="T1429">go-CVB</ta>
            <ta e="T1431" id="Seg_9802" s="T1430">disappear-PST.[3SG]</ta>
            <ta e="T1432" id="Seg_9803" s="T1431">house-LAT/LOC.3SG</ta>
            <ta e="T1434" id="Seg_9804" s="T1433">then</ta>
            <ta e="T1435" id="Seg_9805" s="T1434">this.[NOM.SG]</ta>
            <ta e="T1436" id="Seg_9806" s="T1435">go-PST.[3SG]</ta>
            <ta e="T1437" id="Seg_9807" s="T1436">uncle-LAT</ta>
            <ta e="T1438" id="Seg_9808" s="T1437">grass.[NOM.SG]</ta>
            <ta e="T1439" id="Seg_9809" s="T1438">cut-INF.LAT</ta>
            <ta e="T1441" id="Seg_9810" s="T1440">and</ta>
            <ta e="T1442" id="Seg_9811" s="T1441">there</ta>
            <ta e="T1443" id="Seg_9812" s="T1442">day.[NOM.SG]</ta>
            <ta e="T1445" id="Seg_9813" s="T1444">such.[NOM.SG]</ta>
            <ta e="T1446" id="Seg_9814" s="T1445">be-PST.[3SG]</ta>
            <ta e="T1447" id="Seg_9815" s="T1446">this.[NOM.SG]</ta>
            <ta e="T1448" id="Seg_9816" s="T1447">water-%%-INS</ta>
            <ta e="T1449" id="Seg_9817" s="T1448">self-LAT/LOC.3SG</ta>
            <ta e="T1450" id="Seg_9818" s="T1449">then</ta>
            <ta e="T1451" id="Seg_9819" s="T1450">go-PST-3PL</ta>
            <ta e="T1452" id="Seg_9820" s="T1451">water-LAT</ta>
            <ta e="T1454" id="Seg_9821" s="T1453">and</ta>
            <ta e="T1455" id="Seg_9822" s="T1454">there</ta>
            <ta e="T1456" id="Seg_9823" s="T1455">enter-PST-3PL</ta>
            <ta e="T1457" id="Seg_9824" s="T1456">river-LAT</ta>
            <ta e="T1458" id="Seg_9825" s="T1457">one.[NOM.SG]</ta>
            <ta e="T1459" id="Seg_9826" s="T1458">girl.[NOM.SG]</ta>
            <ta e="T1460" id="Seg_9827" s="T1459">and</ta>
            <ta e="T1461" id="Seg_9828" s="T1460">this.[NOM.SG]</ta>
            <ta e="T1463" id="Seg_9829" s="T1462">river-LOC</ta>
            <ta e="T1465" id="Seg_9830" s="T1464">then</ta>
            <ta e="T1466" id="Seg_9831" s="T1465">tree.[NOM.SG]</ta>
            <ta e="T1467" id="Seg_9832" s="T1466">one.[NOM.SG]</ta>
            <ta e="T1468" id="Seg_9833" s="T1467">boy.[NOM.SG]</ta>
            <ta e="T1469" id="Seg_9834" s="T1468">give-PST.[3SG]</ta>
            <ta e="T1470" id="Seg_9835" s="T1469">this.[NOM.SG]</ta>
            <ta e="T1471" id="Seg_9836" s="T1470">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T1472" id="Seg_9837" s="T1471">bring-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_9838" s="T1472">and</ta>
            <ta e="T1474" id="Seg_9839" s="T1473">this.[NOM.SG]</ta>
            <ta e="T1475" id="Seg_9840" s="T1474">die-RES-PST.[3SG]</ta>
            <ta e="T1477" id="Seg_9841" s="T1476">river-LOC</ta>
            <ta e="T1498" id="Seg_9842" s="T1495">well</ta>
            <ta e="T1500" id="Seg_9843" s="T1499">tomorrow</ta>
            <ta e="T1506" id="Seg_9844" s="T1503">come</ta>
            <ta e="T1508" id="Seg_9845" s="T1506">tomorrow</ta>
            <ta e="T1587" id="Seg_9846" s="T1586">I.NOM</ta>
            <ta e="T1588" id="Seg_9847" s="T1587">tomorrow</ta>
            <ta e="T1589" id="Seg_9848" s="T1588">wait-DUR-1SG</ta>
            <ta e="T1590" id="Seg_9849" s="T1589">Arpit-EP-ACC</ta>
            <ta e="T1592" id="Seg_9850" s="T1591">this.[NOM.SG]</ta>
            <ta e="T1594" id="Seg_9851" s="T1593">say-PST.[3SG]</ta>
            <ta e="T1595" id="Seg_9852" s="T1594">come-FUT-1SG</ta>
            <ta e="T1596" id="Seg_9853" s="T1595">people-ACC</ta>
            <ta e="T1597" id="Seg_9854" s="T1596">you.DAT</ta>
            <ta e="T1598" id="Seg_9855" s="T1597">go-OPT.DU/PL-1DU</ta>
            <ta e="T1599" id="Seg_9856" s="T1598">I.NOM</ta>
            <ta e="T1600" id="Seg_9857" s="T1599">mother-LAT</ta>
            <ta e="T1602" id="Seg_9858" s="T1601">then</ta>
            <ta e="T1603" id="Seg_9859" s="T1602">I.NOM</ta>
            <ta e="T1604" id="Seg_9860" s="T1603">where.to=INDEF</ta>
            <ta e="T1605" id="Seg_9861" s="T1604">NEG</ta>
            <ta e="T1606" id="Seg_9862" s="T1605">go-FUT-1SG</ta>
            <ta e="T1607" id="Seg_9863" s="T1606">wait-FUT-1SG</ta>
            <ta e="T1608" id="Seg_9864" s="T1607">this-ACC</ta>
            <ta e="T1610" id="Seg_9865" s="T1609">when</ta>
            <ta e="T1611" id="Seg_9866" s="T1610">this.[NOM.SG]</ta>
            <ta e="T1612" id="Seg_9867" s="T1611">come-FUT-3SG</ta>
            <ta e="T1651" id="Seg_9868" s="T1650">I.NOM</ta>
            <ta e="T1652" id="Seg_9869" s="T1651">today</ta>
            <ta e="T1654" id="Seg_9870" s="T1653">sleep-PST-1SG</ta>
            <ta e="T1655" id="Seg_9871" s="T1654">and</ta>
            <ta e="T1656" id="Seg_9872" s="T1655">dream-LAT/LOC.1SG</ta>
            <ta e="T1657" id="Seg_9873" s="T1656">see-PST-1SG</ta>
            <ta e="T1658" id="Seg_9874" s="T1657">one.[NOM.SG]</ta>
            <ta e="T1659" id="Seg_9875" s="T1658">man.[NOM.SG]</ta>
            <ta e="T1661" id="Seg_9876" s="T1660">this.[NOM.SG]</ta>
            <ta e="T1662" id="Seg_9877" s="T1661">I.LAT</ta>
            <ta e="T1664" id="Seg_9878" s="T1663">say-IPFVZ.[3SG]</ta>
            <ta e="T1665" id="Seg_9879" s="T1664">go-OPT.DU/PL-1DU</ta>
            <ta e="T1666" id="Seg_9880" s="T1665">where.to=INDEF</ta>
            <ta e="T1668" id="Seg_9881" s="T1667">I.NOM</ta>
            <ta e="T1669" id="Seg_9882" s="T1668">say-IPFVZ-1SG</ta>
            <ta e="T1670" id="Seg_9883" s="T1669">NEG</ta>
            <ta e="T1671" id="Seg_9884" s="T1670">go-FUT-1SG</ta>
            <ta e="T1698" id="Seg_9885" s="T1697">I.NOM</ta>
            <ta e="T1699" id="Seg_9886" s="T1698">sleep-PST-1SG</ta>
            <ta e="T1700" id="Seg_9887" s="T1699">yesterday</ta>
            <ta e="T1701" id="Seg_9888" s="T1700">and</ta>
            <ta e="T1702" id="Seg_9889" s="T1701">dream-LAT/LOC.1SG</ta>
            <ta e="T1703" id="Seg_9890" s="T1702">see-PST-1SG</ta>
            <ta e="T1704" id="Seg_9891" s="T1703">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1705" id="Seg_9892" s="T1704">sister-GEN.1SG</ta>
            <ta e="T1706" id="Seg_9893" s="T1705">man.[NOM.SG]</ta>
            <ta e="T1709" id="Seg_9894" s="T1708">then</ta>
            <ta e="T1710" id="Seg_9895" s="T1709">go-PST-1SG</ta>
            <ta e="T1711" id="Seg_9896" s="T1710">you.PL.LAT</ta>
            <ta e="T1712" id="Seg_9897" s="T1711">speak-PST-1SG</ta>
            <ta e="T1715" id="Seg_9898" s="T1714">then</ta>
            <ta e="T1716" id="Seg_9899" s="T1715">say-PST-1SG</ta>
            <ta e="T1717" id="Seg_9900" s="T1716">who.[NOM.SG]=INDEF</ta>
            <ta e="T1718" id="Seg_9901" s="T1717">speak-FUT-3SG</ta>
            <ta e="T1719" id="Seg_9902" s="T1718">I.NOM-INS</ta>
            <ta e="T1721" id="Seg_9903" s="T1720">then</ta>
            <ta e="T1725" id="Seg_9904" s="T1722">chief.[NOM.SG]</ta>
            <ta e="T1726" id="Seg_9905" s="T1725">come-PST-3PL</ta>
            <ta e="T1727" id="Seg_9906" s="T1726">speak-INF.LAT</ta>
            <ta e="T1728" id="Seg_9907" s="T1727">I.NOM-INS</ta>
            <ta e="T1730" id="Seg_9908" s="T1729">then</ta>
            <ta e="T1731" id="Seg_9909" s="T1730">there</ta>
            <ta e="T1800" id="Seg_9910" s="T1799">we.NOM</ta>
            <ta e="T1801" id="Seg_9911" s="T1800">go-PST-1PL</ta>
            <ta e="T1802" id="Seg_9912" s="T1801">this.[NOM.SG]</ta>
            <ta e="T1803" id="Seg_9913" s="T1802">woman.[NOM.SG]</ta>
            <ta e="T1804" id="Seg_9914" s="T1803">be-PST.[3SG]</ta>
            <ta e="T1805" id="Seg_9915" s="T1804">day.[NOM.SG]</ta>
            <ta e="T1806" id="Seg_9916" s="T1805">when</ta>
            <ta e="T1807" id="Seg_9917" s="T1806">this.[NOM.SG]</ta>
            <ta e="T1808" id="Seg_9918" s="T1807">mother-NOM/GEN.3SG</ta>
            <ta e="T1809" id="Seg_9919" s="T1808">bring-PST.[3SG]</ta>
            <ta e="T1811" id="Seg_9920" s="T1810">I.NOM</ta>
            <ta e="T1812" id="Seg_9921" s="T1811">go-PST-1SG</ta>
            <ta e="T1813" id="Seg_9922" s="T1812">three.[NOM.SG]</ta>
            <ta e="T1814" id="Seg_9923" s="T1813">money.[NOM.SG]</ta>
            <ta e="T1815" id="Seg_9924" s="T1814">this-LAT</ta>
            <ta e="T1816" id="Seg_9925" s="T1815">give-PST-1SG</ta>
            <ta e="T1818" id="Seg_9926" s="T1817">this.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_9927" s="T1819">we.LAT</ta>
            <ta e="T1821" id="Seg_9928" s="T1820">feed-PST.[3SG]</ta>
            <ta e="T1822" id="Seg_9929" s="T1821">egg-PL</ta>
            <ta e="T1823" id="Seg_9930" s="T1822">put-PST.[3SG]</ta>
            <ta e="T1825" id="Seg_9931" s="T1824">butter.[NOM.SG]</ta>
            <ta e="T1826" id="Seg_9932" s="T1825">put-PST.[3SG]</ta>
            <ta e="T1827" id="Seg_9933" s="T1826">onion</ta>
            <ta e="T1829" id="Seg_9934" s="T1828">say-IPFVZ.[3SG]</ta>
            <ta e="T1830" id="Seg_9935" s="T1829">eat-EP-IMP.2SG</ta>
            <ta e="T1831" id="Seg_9936" s="T1830">eat-EP-IMP.2SG</ta>
            <ta e="T1833" id="Seg_9937" s="T1832">I.NOM</ta>
            <ta e="T1834" id="Seg_9938" s="T1833">eat-PST-1SG</ta>
            <ta e="T1835" id="Seg_9939" s="T1834">a.few</ta>
            <ta e="T1837" id="Seg_9940" s="T1836">then</ta>
            <ta e="T1838" id="Seg_9941" s="T1837">one.[NOM.SG]</ta>
            <ta e="T1839" id="Seg_9942" s="T1838">cup.[NOM.SG]</ta>
            <ta e="T1840" id="Seg_9943" s="T1839">yellow.[NOM.SG]</ta>
            <ta e="T1841" id="Seg_9944" s="T1840">water.[NOM.SG]</ta>
            <ta e="T1842" id="Seg_9945" s="T1841">drink-PST-1SG</ta>
            <ta e="T1844" id="Seg_9946" s="T1843">say-PST-1SG</ta>
            <ta e="T1845" id="Seg_9947" s="T1844">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T379" id="Seg_9948" s="T378">один</ta>
            <ta e="T380" id="Seg_9949" s="T379">жид.[NOM.SG]</ta>
            <ta e="T381" id="Seg_9950" s="T380">прийти-PST.[3SG]</ta>
            <ta e="T382" id="Seg_9951" s="T381">один.[NOM.SG]</ta>
            <ta e="T383" id="Seg_9952" s="T382">татарин</ta>
            <ta e="T384" id="Seg_9953" s="T383">мужчина.[NOM.SG]</ta>
            <ta e="T385" id="Seg_9954" s="T384">прийти-PST.[3SG]</ta>
            <ta e="T394" id="Seg_9955" s="T393">татарин-PL</ta>
            <ta e="T396" id="Seg_9956" s="T395">тогда</ta>
            <ta e="T397" id="Seg_9957" s="T396">племянница.[NOM.SG]</ta>
            <ta e="T398" id="Seg_9958" s="T397">я.LAT</ta>
            <ta e="T399" id="Seg_9959" s="T398">трава.[NOM.SG]</ta>
            <ta e="T400" id="Seg_9960" s="T399">пойти-PST.[3SG]</ta>
            <ta e="T402" id="Seg_9961" s="T401">мы.NOM</ta>
            <ta e="T403" id="Seg_9962" s="T402">этот.[NOM.SG]</ta>
            <ta e="T404" id="Seg_9963" s="T403">копна-PL</ta>
            <ta e="T405" id="Seg_9964" s="T404">класть-PST-1PL</ta>
            <ta e="T407" id="Seg_9965" s="T406">тогда</ta>
            <ta e="T408" id="Seg_9966" s="T407">один.[NOM.SG]</ta>
            <ta e="T410" id="Seg_9967" s="T409">упасть-PST.[3SG]</ta>
            <ta e="T412" id="Seg_9968" s="T411">я.NOM</ta>
            <ta e="T413" id="Seg_9969" s="T412">сказать-PST-1SG</ta>
            <ta e="T414" id="Seg_9970" s="T413">дать-IMP.2SG</ta>
            <ta e="T415" id="Seg_9971" s="T414">три-NOM/GEN/ACC.1PL</ta>
            <ta e="T416" id="Seg_9972" s="T415">быть-PST-1PL</ta>
            <ta e="T418" id="Seg_9973" s="T417">я.NOM</ta>
            <ta e="T420" id="Seg_9974" s="T418">сказать-PST-1SG</ta>
            <ta e="T421" id="Seg_9975" s="T420">ну</ta>
            <ta e="T422" id="Seg_9976" s="T421">видеть-IMP.2SG.O</ta>
            <ta e="T423" id="Seg_9977" s="T422">видеть-%%</ta>
            <ta e="T424" id="Seg_9978" s="T423">упасть-FUT-PST</ta>
            <ta e="T426" id="Seg_9979" s="T425">я.NOM</ta>
            <ta e="T427" id="Seg_9980" s="T426">сказать-PST-1SG</ta>
            <ta e="T428" id="Seg_9981" s="T427">кто.[NOM.SG]=INDEF</ta>
            <ta e="T430" id="Seg_9982" s="T429">умереть-RES-FUT-3SG</ta>
            <ta e="T432" id="Seg_9983" s="T431">тогда</ta>
            <ta e="T433" id="Seg_9984" s="T432">я.NOM</ta>
            <ta e="T434" id="Seg_9985" s="T433">сказать-PRS-1SG</ta>
            <ta e="T435" id="Seg_9986" s="T434">этот</ta>
            <ta e="T436" id="Seg_9987" s="T435">я.NOM</ta>
            <ta e="T437" id="Seg_9988" s="T436">умереть-RES-FUT-1SG</ta>
            <ta e="T439" id="Seg_9989" s="T438">я.NOM</ta>
            <ta e="T440" id="Seg_9990" s="T439">старый.[NOM.SG]</ta>
            <ta e="T441" id="Seg_9991" s="T440">ведь</ta>
            <ta e="T443" id="Seg_9992" s="T442">а</ta>
            <ta e="T444" id="Seg_9993" s="T443">вы.NOM</ta>
            <ta e="T445" id="Seg_9994" s="T444">еще</ta>
            <ta e="T446" id="Seg_9995" s="T445">NEG</ta>
            <ta e="T447" id="Seg_9996" s="T446">старый.[NOM.SG]</ta>
            <ta e="T450" id="Seg_9997" s="T449">тогда</ta>
            <ta e="T451" id="Seg_9998" s="T450">чум-LAT/LOC.1PL</ta>
            <ta e="T452" id="Seg_9999" s="T451">прийти-PST-1PL</ta>
            <ta e="T454" id="Seg_10000" s="T453">я.NOM</ta>
            <ta e="T455" id="Seg_10001" s="T454">когда</ta>
            <ta e="T456" id="Seg_10002" s="T455">этот.[NOM.SG]</ta>
            <ta e="T457" id="Seg_10003" s="T456">этот.[NOM.SG]</ta>
            <ta e="T458" id="Seg_10004" s="T457">прийти-PST.[3SG]</ta>
            <ta e="T459" id="Seg_10005" s="T458">здесь</ta>
            <ta e="T460" id="Seg_10006" s="T459">я.NOM</ta>
            <ta e="T461" id="Seg_10007" s="T460">идти-PST-1SG</ta>
            <ta e="T462" id="Seg_10008" s="T461">лук.[NOM.SG]</ta>
            <ta e="T463" id="Seg_10009" s="T462">рвать-PST-1SG</ta>
            <ta e="T466" id="Seg_10010" s="T465">лиственница.[NOM.SG]</ta>
            <ta e="T467" id="Seg_10011" s="T466">дерево.[NOM.SG]</ta>
            <ta e="T468" id="Seg_10012" s="T467">ветер.[NOM.SG]</ta>
            <ta e="T469" id="Seg_10013" s="T468">NEG.EX-PST.[3SG]</ta>
            <ta e="T470" id="Seg_10014" s="T469">место-LAT</ta>
            <ta e="T471" id="Seg_10015" s="T470">упасть-MOM-PST.[3SG]</ta>
            <ta e="T473" id="Seg_10016" s="T472">я.NOM</ta>
            <ta e="T474" id="Seg_10017" s="T473">я.NOM</ta>
            <ta e="T475" id="Seg_10018" s="T474">%%</ta>
            <ta e="T476" id="Seg_10019" s="T475">кто-LAT=INDEF</ta>
            <ta e="T477" id="Seg_10020" s="T476">NEG</ta>
            <ta e="T478" id="Seg_10021" s="T477">сказать-PST-1SG</ta>
            <ta e="T479" id="Seg_10022" s="T478">а</ta>
            <ta e="T480" id="Seg_10023" s="T479">тогда</ta>
            <ta e="T481" id="Seg_10024" s="T480">там</ta>
            <ta e="T482" id="Seg_10025" s="T481">мужчина-NOM/GEN.3SG</ta>
            <ta e="T483" id="Seg_10026" s="T482">когда</ta>
            <ta e="T484" id="Seg_10027" s="T483">вода-LOC</ta>
            <ta e="T485" id="Seg_10028" s="T484">умереть-RES-PST.[3SG]</ta>
            <ta e="T486" id="Seg_10029" s="T485">я.NOM</ta>
            <ta e="T487" id="Seg_10030" s="T486">сказать-PST-1SG</ta>
            <ta e="T573" id="Seg_10031" s="T257">когда</ta>
            <ta e="T574" id="Seg_10032" s="T573">я.NOM</ta>
            <ta e="T575" id="Seg_10033" s="T574">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T576" id="Seg_10034" s="T575">прийти-PST.[3SG]</ta>
            <ta e="T577" id="Seg_10035" s="T576">там</ta>
            <ta e="T578" id="Seg_10036" s="T577">поселение-LOC</ta>
            <ta e="T579" id="Seg_10037" s="T578">этот.[NOM.SG]</ta>
            <ta e="T580" id="Seg_10038" s="T579">поселение-LAT/LOC.3SG</ta>
            <ta e="T581" id="Seg_10039" s="T580">Спасское-LAT</ta>
            <ta e="T582" id="Seg_10040" s="T581">дом-LAT/LOC.1SG</ta>
            <ta e="T584" id="Seg_10041" s="T583">тогда</ta>
            <ta e="T585" id="Seg_10042" s="T584">прийти-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_10043" s="T585">лиственница.[NOM.SG]</ta>
            <ta e="T587" id="Seg_10044" s="T586">дерево.[NOM.SG]</ta>
            <ta e="T588" id="Seg_10045" s="T587">стоять-DUR.[3SG]</ta>
            <ta e="T590" id="Seg_10046" s="T589">тогда</ta>
            <ta e="T591" id="Seg_10047" s="T590">дерево-ABL</ta>
            <ta e="T594" id="Seg_10048" s="T593">место-LAT</ta>
            <ta e="T595" id="Seg_10049" s="T594">упасть-MOM-PST.[3SG]</ta>
            <ta e="T597" id="Seg_10050" s="T596">тогда</ta>
            <ta e="T598" id="Seg_10051" s="T597">прийти</ta>
            <ta e="T599" id="Seg_10052" s="T598">этот.[NOM.SG]</ta>
            <ta e="T600" id="Seg_10053" s="T599">прийти-PST.[3SG]</ta>
            <ta e="T601" id="Seg_10054" s="T600">дом-LAT/LOC.3SG</ta>
            <ta e="T602" id="Seg_10055" s="T601">я.LAT</ta>
            <ta e="T603" id="Seg_10056" s="T602">сказать-PRS.[3SG]</ta>
            <ta e="T605" id="Seg_10057" s="T604">я.NOM</ta>
            <ta e="T607" id="Seg_10058" s="T606">сказать-PST-1SG</ta>
            <ta e="T608" id="Seg_10059" s="T607">кто.[NOM.SG]=INDEF</ta>
            <ta e="T609" id="Seg_10060" s="T608">ребенок-PL-LAT</ta>
            <ta e="T610" id="Seg_10061" s="T609">умереть-RES-FUT-3SG</ta>
            <ta e="T612" id="Seg_10062" s="T611">и</ta>
            <ta e="T613" id="Seg_10063" s="T612">тогда</ta>
            <ta e="T614" id="Seg_10064" s="T613">так</ta>
            <ta e="T615" id="Seg_10065" s="T614">этот.[NOM.SG]</ta>
            <ta e="T616" id="Seg_10066" s="T615">и</ta>
            <ta e="T617" id="Seg_10067" s="T616">быть-PST.[3SG]</ta>
            <ta e="T619" id="Seg_10068" s="T618">я.NOM</ta>
            <ta e="T620" id="Seg_10069" s="T619">я.GEN</ta>
            <ta e="T621" id="Seg_10070" s="T620">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T622" id="Seg_10071" s="T621">пойти-PST.[3SG]</ta>
            <ta e="T623" id="Seg_10072" s="T622">вода-LOC</ta>
            <ta e="T624" id="Seg_10073" s="T623">и</ta>
            <ta e="T625" id="Seg_10074" s="T624">там</ta>
            <ta e="T626" id="Seg_10075" s="T625">умереть-RES-PST.[3SG]</ta>
            <ta e="T1263" id="Seg_10076" s="T1262">когда</ta>
            <ta e="T1264" id="Seg_10077" s="T1263">этот.[NOM.SG]</ta>
            <ta e="T1265" id="Seg_10078" s="T1264">прийти-PST.[3SG]</ta>
            <ta e="T1266" id="Seg_10079" s="T1265">я.LAT</ta>
            <ta e="T1267" id="Seg_10080" s="T1266">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1268" id="Seg_10081" s="T1267">там</ta>
            <ta e="T1269" id="Seg_10082" s="T1268">мать-NOM/GEN.3SG</ta>
            <ta e="T1270" id="Seg_10083" s="T1269">мужчина-LAT</ta>
            <ta e="T1271" id="Seg_10084" s="T1270">пойти-CVB</ta>
            <ta e="T1272" id="Seg_10085" s="T1271">исчезнуть-PST.[3SG]</ta>
            <ta e="T1273" id="Seg_10086" s="T1272">я.NOM</ta>
            <ta e="T1274" id="Seg_10087" s="T1273">один</ta>
            <ta e="T1275" id="Seg_10088" s="T1274">жить-DUR-1SG</ta>
            <ta e="T1277" id="Seg_10089" s="T1276">я.NOM</ta>
            <ta e="T1278" id="Seg_10090" s="T1277">сказать-PST-1SG</ta>
            <ta e="T1279" id="Seg_10091" s="T1278">пойти-EP-IMP.2SG</ta>
            <ta e="T1280" id="Seg_10092" s="T1279">я.LAT</ta>
            <ta e="T1281" id="Seg_10093" s="T1280">жениться-IMP.2SG</ta>
            <ta e="T1282" id="Seg_10094" s="T1281">отец-LAT</ta>
            <ta e="T1283" id="Seg_10095" s="T1282">и</ta>
            <ta e="T1284" id="Seg_10096" s="T1283">мать-LAT</ta>
            <ta e="T1285" id="Seg_10097" s="T1284">этот.[NOM.SG]</ta>
            <ta e="T1286" id="Seg_10098" s="T1285">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1287" id="Seg_10099" s="T1286">этот-PL</ta>
            <ta e="T1288" id="Seg_10100" s="T1287">NEG</ta>
            <ta e="T1289" id="Seg_10101" s="T1288">дать-FUT-3PL</ta>
            <ta e="T1290" id="Seg_10102" s="T1289">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1292" id="Seg_10103" s="T1291">тогда</ta>
            <ta e="T1293" id="Seg_10104" s="T1292">я.NOM</ta>
            <ta e="T1294" id="Seg_10105" s="T1293">одежда-ACC</ta>
            <ta e="T1295" id="Seg_10106" s="T1294">собирать-PST-1SG</ta>
            <ta e="T1297" id="Seg_10107" s="T1296">вечер-LOC.ADV</ta>
            <ta e="T1298" id="Seg_10108" s="T1297">лошадь-ACC</ta>
            <ta e="T1299" id="Seg_10109" s="T1298">унести-TR-DUR.PST-1PL</ta>
            <ta e="T1300" id="Seg_10110" s="T1299">сидеть-PST-1PL</ta>
            <ta e="T1301" id="Seg_10111" s="T1300">пойти-PST-1PL</ta>
            <ta e="T1302" id="Seg_10112" s="T1301">Агинское</ta>
            <ta e="T1303" id="Seg_10113" s="T1302">поселение-LAT</ta>
            <ta e="T1305" id="Seg_10114" s="T1304">PTCL</ta>
            <ta e="T1306" id="Seg_10115" s="T1305">вечер.[NOM.SG]</ta>
            <ta e="T1307" id="Seg_10116" s="T1306">там</ta>
            <ta e="T1308" id="Seg_10117" s="T1307">спать-PST-1PL</ta>
            <ta e="T1309" id="Seg_10118" s="T1308">тогда</ta>
            <ta e="T1310" id="Seg_10119" s="T1309">прийти-PST-1PL</ta>
            <ta e="T1311" id="Seg_10120" s="T1310">опять</ta>
            <ta e="T1312" id="Seg_10121" s="T1311">этот-LAT</ta>
            <ta e="T1313" id="Seg_10122" s="T1312">дом-LAT</ta>
            <ta e="T1314" id="Seg_10123" s="T1313">Каптырлык-LAT</ta>
            <ta e="T1316" id="Seg_10124" s="T1315">там</ta>
            <ta e="T1317" id="Seg_10125" s="T1316">целый</ta>
            <ta e="T1319" id="Seg_10126" s="T1318">день.[NOM.SG]</ta>
            <ta e="T1320" id="Seg_10127" s="T1319">быть-PST-1PL</ta>
            <ta e="T1321" id="Seg_10128" s="T1320">тогда</ta>
            <ta e="T1322" id="Seg_10129" s="T1321">вечер-LOC.ADV</ta>
            <ta e="T1323" id="Seg_10130" s="T1322">прийти-PST-1PL</ta>
            <ta e="T1324" id="Seg_10131" s="T1323">этот.[NOM.SG]</ta>
            <ta e="T1325" id="Seg_10132" s="T1324">там</ta>
            <ta e="T1326" id="Seg_10133" s="T1325">дом-LAT</ta>
            <ta e="T1328" id="Seg_10134" s="T1327">тогда</ta>
            <ta e="T1329" id="Seg_10135" s="T1328">этот.[NOM.SG]</ta>
            <ta e="T1330" id="Seg_10136" s="T1329">утро-LOC.ADV</ta>
            <ta e="T1331" id="Seg_10137" s="T1330">прийти-PST.[3SG]</ta>
            <ta e="T1332" id="Seg_10138" s="T1331">там</ta>
            <ta e="T1333" id="Seg_10139" s="T1332">русский</ta>
            <ta e="T1334" id="Seg_10140" s="T1333">мать-NOM/GEN.3SG</ta>
            <ta e="T1336" id="Seg_10141" s="T1335">тогда</ta>
            <ta e="T1337" id="Seg_10142" s="T1336">встать-PST-1PL</ta>
            <ta e="T1339" id="Seg_10143" s="T1338">там</ta>
            <ta e="T1340" id="Seg_10144" s="T1339">сидеть-PST-1SG</ta>
            <ta e="T1343" id="Seg_10145" s="T1342">трава.[NOM.SG]</ta>
            <ta e="T1344" id="Seg_10146" s="T1343">резать-PST-1PL</ta>
            <ta e="T1345" id="Seg_10147" s="T1344">там</ta>
            <ta e="T1346" id="Seg_10148" s="T1345">PTCL</ta>
            <ta e="T1348" id="Seg_10149" s="T1347">тогда</ta>
            <ta e="T1349" id="Seg_10150" s="T1348">там</ta>
            <ta e="T1350" id="Seg_10151" s="T1349">дядя-NOM/GEN.3SG</ta>
            <ta e="T1351" id="Seg_10152" s="T1350">прийти-PST.[3SG]</ta>
            <ta e="T1353" id="Seg_10153" s="T1352">прийти-IMP.2SG</ta>
            <ta e="T1354" id="Seg_10154" s="T1353">я.LAT</ta>
            <ta e="T1355" id="Seg_10155" s="T1354">трава.[NOM.SG]</ta>
            <ta e="T1356" id="Seg_10156" s="T1355">резать-INF.LAT</ta>
            <ta e="T1358" id="Seg_10157" s="T1357">тогда</ta>
            <ta e="T1359" id="Seg_10158" s="T1358">этот.[NOM.SG]</ta>
            <ta e="T1360" id="Seg_10159" s="T1359">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T1361" id="Seg_10160" s="T1360">принести-PST.[3SG]</ta>
            <ta e="T1363" id="Seg_10161" s="T1362">я.LAT</ta>
            <ta e="T1364" id="Seg_10162" s="T1363">сказать-PST-3PL</ta>
            <ta e="T1365" id="Seg_10163" s="T1364">этот.[NOM.SG]</ta>
            <ta e="T1366" id="Seg_10164" s="T1365">этот-COM</ta>
            <ta e="T1367" id="Seg_10165" s="T1366">жить-INF.LAT</ta>
            <ta e="T1368" id="Seg_10166" s="T1367">жить-FUT-3SG</ta>
            <ta e="T1370" id="Seg_10167" s="T1369">я.NOM</ta>
            <ta e="T1371" id="Seg_10168" s="T1370">PTCL</ta>
            <ta e="T1372" id="Seg_10169" s="T1371">одежда-ACC</ta>
            <ta e="T1373" id="Seg_10170" s="T1372">взять-PST-1SG</ta>
            <ta e="T1374" id="Seg_10171" s="T1373">и</ta>
            <ta e="T1375" id="Seg_10172" s="T1374">этот.[NOM.SG]</ta>
            <ta e="T1377" id="Seg_10173" s="T1376">этот.[NOM.SG]</ta>
            <ta e="T1378" id="Seg_10174" s="T1377">там</ta>
            <ta e="T1379" id="Seg_10175" s="T1378">дядя-COM</ta>
            <ta e="T1380" id="Seg_10176" s="T1379">пойти-PST-1SG</ta>
            <ta e="T1381" id="Seg_10177" s="T1380">этот</ta>
            <ta e="T1382" id="Seg_10178" s="T1381">этот.[NOM.SG]</ta>
            <ta e="T1383" id="Seg_10179" s="T1382">догонять-PST.[3SG]</ta>
            <ta e="T1384" id="Seg_10180" s="T1383">я.LAT</ta>
            <ta e="T1386" id="Seg_10181" s="T1385">опять</ta>
            <ta e="T1387" id="Seg_10182" s="T1386">принести-PST.[3SG]</ta>
            <ta e="T1388" id="Seg_10183" s="T1387">дом-LAT/LOC.3SG</ta>
            <ta e="T1389" id="Seg_10184" s="T1388">тогда</ta>
            <ta e="T1390" id="Seg_10185" s="T1389">я.NOM</ta>
            <ta e="T1391" id="Seg_10186" s="T1390">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T1392" id="Seg_10187" s="T1391">прийти-PST.[3SG]</ta>
            <ta e="T1394" id="Seg_10188" s="T1393">я.NOM</ta>
            <ta e="T1395" id="Seg_10189" s="T1394">опять</ta>
            <ta e="T1396" id="Seg_10190" s="T1395">пойти-CVB</ta>
            <ta e="T1397" id="Seg_10191" s="T1396">исчезнуть-PST-1SG</ta>
            <ta e="T1399" id="Seg_10192" s="T1398">этот.[NOM.SG]</ta>
            <ta e="T1400" id="Seg_10193" s="T1399">прийти-PST.[3SG]</ta>
            <ta e="T1401" id="Seg_10194" s="T1400">и</ta>
            <ta e="T1402" id="Seg_10195" s="T1401">я.LAT</ta>
            <ta e="T1403" id="Seg_10196" s="T1402">нести-RES-PST.[3SG]</ta>
            <ta e="T1404" id="Seg_10197" s="T1403">дом-LAT/LOC.1SG</ta>
            <ta e="T1405" id="Seg_10198" s="T1404">ложиться-1PL</ta>
            <ta e="T1406" id="Seg_10199" s="T1405">спать-INF.LAT</ta>
            <ta e="T1407" id="Seg_10200" s="T1406">тогда</ta>
            <ta e="T1408" id="Seg_10201" s="T1407">этот-PL</ta>
            <ta e="T1410" id="Seg_10202" s="T1409">прийти-PST-3PL</ta>
            <ta e="T1412" id="Seg_10203" s="T1411">ну</ta>
            <ta e="T1413" id="Seg_10204" s="T1412">жить-IMP.2PL</ta>
            <ta e="T1415" id="Seg_10205" s="T1414">тогда</ta>
            <ta e="T1416" id="Seg_10206" s="T1415">мы.ACC</ta>
            <ta e="T1417" id="Seg_10207" s="T1416">Бог-LAT</ta>
            <ta e="T1418" id="Seg_10208" s="T1417">молиться-PST-3PL</ta>
            <ta e="T1420" id="Seg_10209" s="T1419">тогда</ta>
            <ta e="T1421" id="Seg_10210" s="T1420">пойти-PST-3PL</ta>
            <ta e="T1423" id="Seg_10211" s="T1422">завтра</ta>
            <ta e="T1424" id="Seg_10212" s="T1423">встать-PST-3PL</ta>
            <ta e="T1425" id="Seg_10213" s="T1424">мы.LAT</ta>
            <ta e="T1426" id="Seg_10214" s="T1425">прийти-PST-3PL</ta>
            <ta e="T1428" id="Seg_10215" s="T1427">есть-PST-3PL</ta>
            <ta e="T1429" id="Seg_10216" s="T1428">тогда</ta>
            <ta e="T1430" id="Seg_10217" s="T1429">пойти-CVB</ta>
            <ta e="T1431" id="Seg_10218" s="T1430">исчезнуть-PST.[3SG]</ta>
            <ta e="T1432" id="Seg_10219" s="T1431">дом-LAT/LOC.3SG</ta>
            <ta e="T1434" id="Seg_10220" s="T1433">тогда</ta>
            <ta e="T1435" id="Seg_10221" s="T1434">этот.[NOM.SG]</ta>
            <ta e="T1436" id="Seg_10222" s="T1435">пойти-PST.[3SG]</ta>
            <ta e="T1437" id="Seg_10223" s="T1436">дядя-LAT</ta>
            <ta e="T1438" id="Seg_10224" s="T1437">трава.[NOM.SG]</ta>
            <ta e="T1439" id="Seg_10225" s="T1438">резать-INF.LAT</ta>
            <ta e="T1441" id="Seg_10226" s="T1440">и</ta>
            <ta e="T1442" id="Seg_10227" s="T1441">там</ta>
            <ta e="T1443" id="Seg_10228" s="T1442">день.[NOM.SG]</ta>
            <ta e="T1445" id="Seg_10229" s="T1444">такой.[NOM.SG]</ta>
            <ta e="T1446" id="Seg_10230" s="T1445">быть-PST.[3SG]</ta>
            <ta e="T1447" id="Seg_10231" s="T1446">этот.[NOM.SG]</ta>
            <ta e="T1448" id="Seg_10232" s="T1447">вода-%%-INS</ta>
            <ta e="T1449" id="Seg_10233" s="T1448">сам-LAT/LOC.3SG</ta>
            <ta e="T1450" id="Seg_10234" s="T1449">тогда</ta>
            <ta e="T1451" id="Seg_10235" s="T1450">пойти-PST-3PL</ta>
            <ta e="T1452" id="Seg_10236" s="T1451">вода-LAT</ta>
            <ta e="T1454" id="Seg_10237" s="T1453">и</ta>
            <ta e="T1455" id="Seg_10238" s="T1454">там</ta>
            <ta e="T1456" id="Seg_10239" s="T1455">войти-PST-3PL</ta>
            <ta e="T1457" id="Seg_10240" s="T1456">река-LAT</ta>
            <ta e="T1458" id="Seg_10241" s="T1457">один.[NOM.SG]</ta>
            <ta e="T1459" id="Seg_10242" s="T1458">девушка.[NOM.SG]</ta>
            <ta e="T1460" id="Seg_10243" s="T1459">и</ta>
            <ta e="T1461" id="Seg_10244" s="T1460">этот.[NOM.SG]</ta>
            <ta e="T1463" id="Seg_10245" s="T1462">река-LOC</ta>
            <ta e="T1465" id="Seg_10246" s="T1464">тогда</ta>
            <ta e="T1466" id="Seg_10247" s="T1465">дерево.[NOM.SG]</ta>
            <ta e="T1467" id="Seg_10248" s="T1466">один.[NOM.SG]</ta>
            <ta e="T1468" id="Seg_10249" s="T1467">мальчик.[NOM.SG]</ta>
            <ta e="T1469" id="Seg_10250" s="T1468">дать-PST.[3SG]</ta>
            <ta e="T1470" id="Seg_10251" s="T1469">этот.[NOM.SG]</ta>
            <ta e="T1471" id="Seg_10252" s="T1470">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T1472" id="Seg_10253" s="T1471">принести-PST.[3SG]</ta>
            <ta e="T1473" id="Seg_10254" s="T1472">а</ta>
            <ta e="T1474" id="Seg_10255" s="T1473">этот.[NOM.SG]</ta>
            <ta e="T1475" id="Seg_10256" s="T1474">умереть-RES-PST.[3SG]</ta>
            <ta e="T1477" id="Seg_10257" s="T1476">река-LOC</ta>
            <ta e="T1498" id="Seg_10258" s="T1495">ну</ta>
            <ta e="T1500" id="Seg_10259" s="T1499">завтра</ta>
            <ta e="T1506" id="Seg_10260" s="T1503">прийти</ta>
            <ta e="T1508" id="Seg_10261" s="T1506">завтра</ta>
            <ta e="T1587" id="Seg_10262" s="T1586">я.NOM</ta>
            <ta e="T1588" id="Seg_10263" s="T1587">завтра</ta>
            <ta e="T1589" id="Seg_10264" s="T1588">ждать-DUR-1SG</ta>
            <ta e="T1590" id="Seg_10265" s="T1589">Арпит-EP-ACC</ta>
            <ta e="T1592" id="Seg_10266" s="T1591">этот.[NOM.SG]</ta>
            <ta e="T1594" id="Seg_10267" s="T1593">сказать-PST.[3SG]</ta>
            <ta e="T1595" id="Seg_10268" s="T1594">прийти-FUT-1SG</ta>
            <ta e="T1596" id="Seg_10269" s="T1595">люди-ACC</ta>
            <ta e="T1597" id="Seg_10270" s="T1596">ты.DAT</ta>
            <ta e="T1598" id="Seg_10271" s="T1597">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1599" id="Seg_10272" s="T1598">я.NOM</ta>
            <ta e="T1600" id="Seg_10273" s="T1599">мать-LAT</ta>
            <ta e="T1602" id="Seg_10274" s="T1601">тогда</ta>
            <ta e="T1603" id="Seg_10275" s="T1602">я.NOM</ta>
            <ta e="T1604" id="Seg_10276" s="T1603">куда=INDEF</ta>
            <ta e="T1605" id="Seg_10277" s="T1604">NEG</ta>
            <ta e="T1606" id="Seg_10278" s="T1605">пойти-FUT-1SG</ta>
            <ta e="T1607" id="Seg_10279" s="T1606">ждать-FUT-1SG</ta>
            <ta e="T1608" id="Seg_10280" s="T1607">этот-ACC</ta>
            <ta e="T1610" id="Seg_10281" s="T1609">когда</ta>
            <ta e="T1611" id="Seg_10282" s="T1610">этот.[NOM.SG]</ta>
            <ta e="T1612" id="Seg_10283" s="T1611">прийти-FUT-3SG</ta>
            <ta e="T1651" id="Seg_10284" s="T1650">я.NOM</ta>
            <ta e="T1652" id="Seg_10285" s="T1651">сегодня</ta>
            <ta e="T1654" id="Seg_10286" s="T1653">спать-PST-1SG</ta>
            <ta e="T1655" id="Seg_10287" s="T1654">и</ta>
            <ta e="T1656" id="Seg_10288" s="T1655">сон-LAT/LOC.1SG</ta>
            <ta e="T1657" id="Seg_10289" s="T1656">видеть-PST-1SG</ta>
            <ta e="T1658" id="Seg_10290" s="T1657">один.[NOM.SG]</ta>
            <ta e="T1659" id="Seg_10291" s="T1658">мужчина.[NOM.SG]</ta>
            <ta e="T1661" id="Seg_10292" s="T1660">этот.[NOM.SG]</ta>
            <ta e="T1662" id="Seg_10293" s="T1661">я.LAT</ta>
            <ta e="T1664" id="Seg_10294" s="T1663">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1665" id="Seg_10295" s="T1664">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1666" id="Seg_10296" s="T1665">куда=INDEF</ta>
            <ta e="T1668" id="Seg_10297" s="T1667">я.NOM</ta>
            <ta e="T1669" id="Seg_10298" s="T1668">сказать-IPFVZ-1SG</ta>
            <ta e="T1670" id="Seg_10299" s="T1669">NEG</ta>
            <ta e="T1671" id="Seg_10300" s="T1670">пойти-FUT-1SG</ta>
            <ta e="T1698" id="Seg_10301" s="T1697">я.NOM</ta>
            <ta e="T1699" id="Seg_10302" s="T1698">спать-PST-1SG</ta>
            <ta e="T1700" id="Seg_10303" s="T1699">вчера</ta>
            <ta e="T1701" id="Seg_10304" s="T1700">и</ta>
            <ta e="T1702" id="Seg_10305" s="T1701">сон-LAT/LOC.1SG</ta>
            <ta e="T1703" id="Seg_10306" s="T1702">видеть-PST-1SG</ta>
            <ta e="T1704" id="Seg_10307" s="T1703">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1705" id="Seg_10308" s="T1704">сестра-GEN.1SG</ta>
            <ta e="T1706" id="Seg_10309" s="T1705">мужчина.[NOM.SG]</ta>
            <ta e="T1709" id="Seg_10310" s="T1708">тогда</ta>
            <ta e="T1710" id="Seg_10311" s="T1709">пойти-PST-1SG</ta>
            <ta e="T1711" id="Seg_10312" s="T1710">вы.LAT</ta>
            <ta e="T1712" id="Seg_10313" s="T1711">говорить-PST-1SG</ta>
            <ta e="T1715" id="Seg_10314" s="T1714">тогда</ta>
            <ta e="T1716" id="Seg_10315" s="T1715">сказать-PST-1SG</ta>
            <ta e="T1717" id="Seg_10316" s="T1716">кто.[NOM.SG]=INDEF</ta>
            <ta e="T1718" id="Seg_10317" s="T1717">говорить-FUT-3SG</ta>
            <ta e="T1719" id="Seg_10318" s="T1718">я.NOM-INS</ta>
            <ta e="T1721" id="Seg_10319" s="T1720">тогда</ta>
            <ta e="T1725" id="Seg_10320" s="T1722">вождь.[NOM.SG]</ta>
            <ta e="T1726" id="Seg_10321" s="T1725">прийти-PST-3PL</ta>
            <ta e="T1727" id="Seg_10322" s="T1726">говорить-INF.LAT</ta>
            <ta e="T1728" id="Seg_10323" s="T1727">я.NOM-INS</ta>
            <ta e="T1730" id="Seg_10324" s="T1729">тогда</ta>
            <ta e="T1731" id="Seg_10325" s="T1730">там</ta>
            <ta e="T1800" id="Seg_10326" s="T1799">мы.NOM</ta>
            <ta e="T1801" id="Seg_10327" s="T1800">идти-PST-1PL</ta>
            <ta e="T1802" id="Seg_10328" s="T1801">этот.[NOM.SG]</ta>
            <ta e="T1803" id="Seg_10329" s="T1802">женщина.[NOM.SG]</ta>
            <ta e="T1804" id="Seg_10330" s="T1803">быть-PST.[3SG]</ta>
            <ta e="T1805" id="Seg_10331" s="T1804">день.[NOM.SG]</ta>
            <ta e="T1806" id="Seg_10332" s="T1805">когда</ta>
            <ta e="T1807" id="Seg_10333" s="T1806">этот.[NOM.SG]</ta>
            <ta e="T1808" id="Seg_10334" s="T1807">мать-NOM/GEN.3SG</ta>
            <ta e="T1809" id="Seg_10335" s="T1808">принести-PST.[3SG]</ta>
            <ta e="T1811" id="Seg_10336" s="T1810">я.NOM</ta>
            <ta e="T1812" id="Seg_10337" s="T1811">идти-PST-1SG</ta>
            <ta e="T1813" id="Seg_10338" s="T1812">три.[NOM.SG]</ta>
            <ta e="T1814" id="Seg_10339" s="T1813">деньги.[NOM.SG]</ta>
            <ta e="T1815" id="Seg_10340" s="T1814">этот-LAT</ta>
            <ta e="T1816" id="Seg_10341" s="T1815">дать-PST-1SG</ta>
            <ta e="T1818" id="Seg_10342" s="T1817">этот.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_10343" s="T1819">мы.LAT</ta>
            <ta e="T1821" id="Seg_10344" s="T1820">кормить-PST.[3SG]</ta>
            <ta e="T1822" id="Seg_10345" s="T1821">яйцо-PL</ta>
            <ta e="T1823" id="Seg_10346" s="T1822">класть-PST.[3SG]</ta>
            <ta e="T1825" id="Seg_10347" s="T1824">масло.[NOM.SG]</ta>
            <ta e="T1826" id="Seg_10348" s="T1825">класть-PST.[3SG]</ta>
            <ta e="T1827" id="Seg_10349" s="T1826">лук</ta>
            <ta e="T1829" id="Seg_10350" s="T1828">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1830" id="Seg_10351" s="T1829">съесть-EP-IMP.2SG</ta>
            <ta e="T1831" id="Seg_10352" s="T1830">съесть-EP-IMP.2SG</ta>
            <ta e="T1833" id="Seg_10353" s="T1832">я.NOM</ta>
            <ta e="T1834" id="Seg_10354" s="T1833">съесть-PST-1SG</ta>
            <ta e="T1835" id="Seg_10355" s="T1834">немного</ta>
            <ta e="T1837" id="Seg_10356" s="T1836">тогда</ta>
            <ta e="T1838" id="Seg_10357" s="T1837">один.[NOM.SG]</ta>
            <ta e="T1839" id="Seg_10358" s="T1838">чашка.[NOM.SG]</ta>
            <ta e="T1840" id="Seg_10359" s="T1839">желтый.[NOM.SG]</ta>
            <ta e="T1841" id="Seg_10360" s="T1840">вода.[NOM.SG]</ta>
            <ta e="T1842" id="Seg_10361" s="T1841">пить-PST-1SG</ta>
            <ta e="T1844" id="Seg_10362" s="T1843">сказать-PST-1SG</ta>
            <ta e="T1845" id="Seg_10363" s="T1844">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T379" id="Seg_10364" s="T378">adj</ta>
            <ta e="T380" id="Seg_10365" s="T379">n.[n:case]</ta>
            <ta e="T381" id="Seg_10366" s="T380">v-v:tense.[v:pn]</ta>
            <ta e="T382" id="Seg_10367" s="T381">num.[n:case]</ta>
            <ta e="T383" id="Seg_10368" s="T382">n</ta>
            <ta e="T384" id="Seg_10369" s="T383">n.[n:case]</ta>
            <ta e="T385" id="Seg_10370" s="T384">v-v:tense.[v:pn]</ta>
            <ta e="T394" id="Seg_10371" s="T393">n-n:num</ta>
            <ta e="T396" id="Seg_10372" s="T395">adv</ta>
            <ta e="T397" id="Seg_10373" s="T396">n.[n:case]</ta>
            <ta e="T398" id="Seg_10374" s="T397">pers</ta>
            <ta e="T399" id="Seg_10375" s="T398">n.[n:case]</ta>
            <ta e="T400" id="Seg_10376" s="T399">v-v:tense.[v:pn]</ta>
            <ta e="T402" id="Seg_10377" s="T401">pers</ta>
            <ta e="T403" id="Seg_10378" s="T402">dempro.[n:case]</ta>
            <ta e="T404" id="Seg_10379" s="T403">n-n:num</ta>
            <ta e="T405" id="Seg_10380" s="T404">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_10381" s="T406">adv</ta>
            <ta e="T408" id="Seg_10382" s="T407">num.[n:case]</ta>
            <ta e="T410" id="Seg_10383" s="T409">v-v:tense.[v:pn]</ta>
            <ta e="T412" id="Seg_10384" s="T411">pers</ta>
            <ta e="T413" id="Seg_10385" s="T412">v-v:tense-v:pn</ta>
            <ta e="T414" id="Seg_10386" s="T413">v-v:mood.pn</ta>
            <ta e="T415" id="Seg_10387" s="T414">num-n:case.poss</ta>
            <ta e="T416" id="Seg_10388" s="T415">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_10389" s="T417">pers</ta>
            <ta e="T420" id="Seg_10390" s="T418">v-v:tense-v:pn</ta>
            <ta e="T421" id="Seg_10391" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_10392" s="T421">v-v:mood.pn</ta>
            <ta e="T423" id="Seg_10393" s="T422">v-v&gt;v</ta>
            <ta e="T424" id="Seg_10394" s="T423">v-v:tense-v:tense</ta>
            <ta e="T426" id="Seg_10395" s="T425">pers</ta>
            <ta e="T427" id="Seg_10396" s="T426">v-v:tense-v:pn</ta>
            <ta e="T428" id="Seg_10397" s="T427">que=ptcl</ta>
            <ta e="T430" id="Seg_10398" s="T429">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_10399" s="T431">adv</ta>
            <ta e="T433" id="Seg_10400" s="T432">pers</ta>
            <ta e="T434" id="Seg_10401" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_10402" s="T434">dempro</ta>
            <ta e="T436" id="Seg_10403" s="T435">pers</ta>
            <ta e="T437" id="Seg_10404" s="T436">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_10405" s="T438">pers</ta>
            <ta e="T440" id="Seg_10406" s="T439">adj.[n:case]</ta>
            <ta e="T441" id="Seg_10407" s="T440">ptcl</ta>
            <ta e="T443" id="Seg_10408" s="T442">conj</ta>
            <ta e="T444" id="Seg_10409" s="T443">pers</ta>
            <ta e="T445" id="Seg_10410" s="T444">adv</ta>
            <ta e="T446" id="Seg_10411" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_10412" s="T446">adj.[n:case]</ta>
            <ta e="T450" id="Seg_10413" s="T449">adv</ta>
            <ta e="T451" id="Seg_10414" s="T450">n-n:case.poss</ta>
            <ta e="T452" id="Seg_10415" s="T451">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_10416" s="T453">pers</ta>
            <ta e="T455" id="Seg_10417" s="T454">que</ta>
            <ta e="T456" id="Seg_10418" s="T455">dempro.[n:case]</ta>
            <ta e="T457" id="Seg_10419" s="T456">dempro.[n:case]</ta>
            <ta e="T458" id="Seg_10420" s="T457">v-v:tense.[v:pn]</ta>
            <ta e="T459" id="Seg_10421" s="T458">adv</ta>
            <ta e="T460" id="Seg_10422" s="T459">pers</ta>
            <ta e="T461" id="Seg_10423" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_10424" s="T461">n.[n:case]</ta>
            <ta e="T463" id="Seg_10425" s="T462">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_10426" s="T465">n.[n:case]</ta>
            <ta e="T467" id="Seg_10427" s="T466">n.[n:case]</ta>
            <ta e="T468" id="Seg_10428" s="T467">n.[n:case]</ta>
            <ta e="T469" id="Seg_10429" s="T468">v-v:tense.[v:pn]</ta>
            <ta e="T470" id="Seg_10430" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_10431" s="T470">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T473" id="Seg_10432" s="T472">pers</ta>
            <ta e="T474" id="Seg_10433" s="T473">pers</ta>
            <ta e="T475" id="Seg_10434" s="T474">%%</ta>
            <ta e="T476" id="Seg_10435" s="T475">que-n:case=ptcl</ta>
            <ta e="T477" id="Seg_10436" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_10437" s="T477">v-v:tense-v:pn</ta>
            <ta e="T479" id="Seg_10438" s="T478">conj</ta>
            <ta e="T480" id="Seg_10439" s="T479">adv</ta>
            <ta e="T481" id="Seg_10440" s="T480">adv</ta>
            <ta e="T482" id="Seg_10441" s="T481">n-n:case.poss</ta>
            <ta e="T483" id="Seg_10442" s="T482">que</ta>
            <ta e="T484" id="Seg_10443" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_10444" s="T484">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T486" id="Seg_10445" s="T485">pers</ta>
            <ta e="T487" id="Seg_10446" s="T486">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_10447" s="T257">que</ta>
            <ta e="T574" id="Seg_10448" s="T573">pers</ta>
            <ta e="T575" id="Seg_10449" s="T574">n-n:case.poss</ta>
            <ta e="T576" id="Seg_10450" s="T575">v-v:tense.[v:pn]</ta>
            <ta e="T577" id="Seg_10451" s="T576">adv</ta>
            <ta e="T578" id="Seg_10452" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_10453" s="T578">dempro.[n:case]</ta>
            <ta e="T580" id="Seg_10454" s="T579">n-n:case.poss</ta>
            <ta e="T581" id="Seg_10455" s="T580">propr-n:case</ta>
            <ta e="T582" id="Seg_10456" s="T581">n-n:case.poss</ta>
            <ta e="T584" id="Seg_10457" s="T583">adv</ta>
            <ta e="T585" id="Seg_10458" s="T584">v-v&gt;v.[v:pn]</ta>
            <ta e="T586" id="Seg_10459" s="T585">n.[n:case]</ta>
            <ta e="T587" id="Seg_10460" s="T586">n.[n:case]</ta>
            <ta e="T588" id="Seg_10461" s="T587">v-v&gt;v.[v:pn]</ta>
            <ta e="T590" id="Seg_10462" s="T589">adv</ta>
            <ta e="T591" id="Seg_10463" s="T590">n-n:case</ta>
            <ta e="T594" id="Seg_10464" s="T593">n-n:case</ta>
            <ta e="T595" id="Seg_10465" s="T594">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T597" id="Seg_10466" s="T596">adv</ta>
            <ta e="T598" id="Seg_10467" s="T597">v</ta>
            <ta e="T599" id="Seg_10468" s="T598">dempro.[n:case]</ta>
            <ta e="T600" id="Seg_10469" s="T599">v-v:tense.[v:pn]</ta>
            <ta e="T601" id="Seg_10470" s="T600">n-n:case.poss</ta>
            <ta e="T602" id="Seg_10471" s="T601">pers</ta>
            <ta e="T603" id="Seg_10472" s="T602">v-v:tense.[v:pn]</ta>
            <ta e="T605" id="Seg_10473" s="T604">pers</ta>
            <ta e="T607" id="Seg_10474" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_10475" s="T607">que.[n:case]=ptcl</ta>
            <ta e="T609" id="Seg_10476" s="T608">n-n:num-n:case</ta>
            <ta e="T610" id="Seg_10477" s="T609">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_10478" s="T611">conj</ta>
            <ta e="T613" id="Seg_10479" s="T612">adv</ta>
            <ta e="T614" id="Seg_10480" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_10481" s="T614">dempro.[n:case]</ta>
            <ta e="T616" id="Seg_10482" s="T615">conj</ta>
            <ta e="T617" id="Seg_10483" s="T616">v-v:tense.[v:pn]</ta>
            <ta e="T619" id="Seg_10484" s="T618">pers</ta>
            <ta e="T620" id="Seg_10485" s="T619">pers</ta>
            <ta e="T621" id="Seg_10486" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_10487" s="T621">v-v:tense.[v:pn]</ta>
            <ta e="T623" id="Seg_10488" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_10489" s="T623">conj</ta>
            <ta e="T625" id="Seg_10490" s="T624">adv</ta>
            <ta e="T626" id="Seg_10491" s="T625">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1263" id="Seg_10492" s="T1262">que</ta>
            <ta e="T1264" id="Seg_10493" s="T1263">dempro.[n:case]</ta>
            <ta e="T1265" id="Seg_10494" s="T1264">v-v:tense.[v:pn]</ta>
            <ta e="T1266" id="Seg_10495" s="T1265">pers</ta>
            <ta e="T1267" id="Seg_10496" s="T1266">v-v:mood-v:pn</ta>
            <ta e="T1268" id="Seg_10497" s="T1267">adv</ta>
            <ta e="T1269" id="Seg_10498" s="T1268">n-n:case.poss</ta>
            <ta e="T1270" id="Seg_10499" s="T1269">n-n:case</ta>
            <ta e="T1271" id="Seg_10500" s="T1270">v-v:n-fin</ta>
            <ta e="T1272" id="Seg_10501" s="T1271">v-v:tense.[v:pn]</ta>
            <ta e="T1273" id="Seg_10502" s="T1272">pers</ta>
            <ta e="T1274" id="Seg_10503" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_10504" s="T1274">v-v&gt;v-v:pn</ta>
            <ta e="T1277" id="Seg_10505" s="T1276">pers</ta>
            <ta e="T1278" id="Seg_10506" s="T1277">v-v:tense-v:pn</ta>
            <ta e="T1279" id="Seg_10507" s="T1278">v-v:ins-v:mood.pn</ta>
            <ta e="T1280" id="Seg_10508" s="T1279">pers</ta>
            <ta e="T1281" id="Seg_10509" s="T1280">v-v:mood.pn</ta>
            <ta e="T1282" id="Seg_10510" s="T1281">n-n:case</ta>
            <ta e="T1283" id="Seg_10511" s="T1282">conj</ta>
            <ta e="T1284" id="Seg_10512" s="T1283">n-n:case</ta>
            <ta e="T1285" id="Seg_10513" s="T1284">dempro.[n:case]</ta>
            <ta e="T1286" id="Seg_10514" s="T1285">v-v&gt;v.[v:pn]</ta>
            <ta e="T1287" id="Seg_10515" s="T1286">dempro-n:num</ta>
            <ta e="T1288" id="Seg_10516" s="T1287">ptcl</ta>
            <ta e="T1289" id="Seg_10517" s="T1288">v-v:tense-v:pn</ta>
            <ta e="T1290" id="Seg_10518" s="T1289">v-v:mood-v:pn</ta>
            <ta e="T1292" id="Seg_10519" s="T1291">adv</ta>
            <ta e="T1293" id="Seg_10520" s="T1292">pers</ta>
            <ta e="T1294" id="Seg_10521" s="T1293">n-n:case</ta>
            <ta e="T1295" id="Seg_10522" s="T1294">v-v:tense-v:pn</ta>
            <ta e="T1297" id="Seg_10523" s="T1296">n-n:case</ta>
            <ta e="T1298" id="Seg_10524" s="T1297">n-n:case</ta>
            <ta e="T1299" id="Seg_10525" s="T1298">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1300" id="Seg_10526" s="T1299">v-v:tense-v:pn</ta>
            <ta e="T1301" id="Seg_10527" s="T1300">v-v:tense-v:pn</ta>
            <ta e="T1302" id="Seg_10528" s="T1301">propr</ta>
            <ta e="T1303" id="Seg_10529" s="T1302">n-n:case</ta>
            <ta e="T1305" id="Seg_10530" s="T1304">ptcl</ta>
            <ta e="T1306" id="Seg_10531" s="T1305">n.[n:case]</ta>
            <ta e="T1307" id="Seg_10532" s="T1306">adv</ta>
            <ta e="T1308" id="Seg_10533" s="T1307">v-v:tense-v:pn</ta>
            <ta e="T1309" id="Seg_10534" s="T1308">adv</ta>
            <ta e="T1310" id="Seg_10535" s="T1309">v-v:tense-v:pn</ta>
            <ta e="T1311" id="Seg_10536" s="T1310">adv</ta>
            <ta e="T1312" id="Seg_10537" s="T1311">dempro-n:case</ta>
            <ta e="T1313" id="Seg_10538" s="T1312">n-n:case</ta>
            <ta e="T1314" id="Seg_10539" s="T1313">propr-n:case</ta>
            <ta e="T1316" id="Seg_10540" s="T1315">adv</ta>
            <ta e="T1317" id="Seg_10541" s="T1316">adj</ta>
            <ta e="T1319" id="Seg_10542" s="T1318">n.[n:case]</ta>
            <ta e="T1320" id="Seg_10543" s="T1319">v-v:tense-v:pn</ta>
            <ta e="T1321" id="Seg_10544" s="T1320">adv</ta>
            <ta e="T1322" id="Seg_10545" s="T1321">n-n:case</ta>
            <ta e="T1323" id="Seg_10546" s="T1322">v-v:tense-v:pn</ta>
            <ta e="T1324" id="Seg_10547" s="T1323">dempro.[n:case]</ta>
            <ta e="T1325" id="Seg_10548" s="T1324">adv</ta>
            <ta e="T1326" id="Seg_10549" s="T1325">n-n:case</ta>
            <ta e="T1328" id="Seg_10550" s="T1327">adv</ta>
            <ta e="T1329" id="Seg_10551" s="T1328">dempro.[n:case]</ta>
            <ta e="T1330" id="Seg_10552" s="T1329">n-n:case</ta>
            <ta e="T1331" id="Seg_10553" s="T1330">v-v:tense.[v:pn]</ta>
            <ta e="T1332" id="Seg_10554" s="T1331">adv</ta>
            <ta e="T1333" id="Seg_10555" s="T1332">n</ta>
            <ta e="T1334" id="Seg_10556" s="T1333">n-n:case.poss</ta>
            <ta e="T1336" id="Seg_10557" s="T1335">adv</ta>
            <ta e="T1337" id="Seg_10558" s="T1336">v-v:tense-v:pn</ta>
            <ta e="T1339" id="Seg_10559" s="T1338">adv</ta>
            <ta e="T1340" id="Seg_10560" s="T1339">v-v:tense-v:pn</ta>
            <ta e="T1343" id="Seg_10561" s="T1342">n.[n:case]</ta>
            <ta e="T1344" id="Seg_10562" s="T1343">v-v:tense-v:pn</ta>
            <ta e="T1345" id="Seg_10563" s="T1344">adv</ta>
            <ta e="T1346" id="Seg_10564" s="T1345">ptcl</ta>
            <ta e="T1348" id="Seg_10565" s="T1347">adv</ta>
            <ta e="T1349" id="Seg_10566" s="T1348">adv</ta>
            <ta e="T1350" id="Seg_10567" s="T1349">n-n:case.poss</ta>
            <ta e="T1351" id="Seg_10568" s="T1350">v-v:tense.[v:pn]</ta>
            <ta e="T1353" id="Seg_10569" s="T1352">v-v:mood.pn</ta>
            <ta e="T1354" id="Seg_10570" s="T1353">pers</ta>
            <ta e="T1355" id="Seg_10571" s="T1354">n.[n:case]</ta>
            <ta e="T1356" id="Seg_10572" s="T1355">v-v:n.fin</ta>
            <ta e="T1358" id="Seg_10573" s="T1357">adv</ta>
            <ta e="T1359" id="Seg_10574" s="T1358">dempro.[n:case]</ta>
            <ta e="T1360" id="Seg_10575" s="T1359">n-n:case.poss</ta>
            <ta e="T1361" id="Seg_10576" s="T1360">v-v:tense.[v:pn]</ta>
            <ta e="T1363" id="Seg_10577" s="T1362">pers</ta>
            <ta e="T1364" id="Seg_10578" s="T1363">v-v:tense-v:pn</ta>
            <ta e="T1365" id="Seg_10579" s="T1364">dempro.[n:case]</ta>
            <ta e="T1366" id="Seg_10580" s="T1365">dempro-n:case</ta>
            <ta e="T1367" id="Seg_10581" s="T1366">v-v:n.fin</ta>
            <ta e="T1368" id="Seg_10582" s="T1367">v-v:tense-v:pn</ta>
            <ta e="T1370" id="Seg_10583" s="T1369">pers</ta>
            <ta e="T1371" id="Seg_10584" s="T1370">ptcl</ta>
            <ta e="T1372" id="Seg_10585" s="T1371">n-n:case</ta>
            <ta e="T1373" id="Seg_10586" s="T1372">v-v:tense-v:pn</ta>
            <ta e="T1374" id="Seg_10587" s="T1373">conj</ta>
            <ta e="T1375" id="Seg_10588" s="T1374">dempro.[n:case]</ta>
            <ta e="T1377" id="Seg_10589" s="T1376">dempro.[n:case]</ta>
            <ta e="T1378" id="Seg_10590" s="T1377">adv</ta>
            <ta e="T1379" id="Seg_10591" s="T1378">n-n:case</ta>
            <ta e="T1380" id="Seg_10592" s="T1379">v-v:tense-v:pn</ta>
            <ta e="T1381" id="Seg_10593" s="T1380">dempro</ta>
            <ta e="T1382" id="Seg_10594" s="T1381">dempro.[n:case]</ta>
            <ta e="T1383" id="Seg_10595" s="T1382">v-v:tense.[v:pn]</ta>
            <ta e="T1384" id="Seg_10596" s="T1383">pers</ta>
            <ta e="T1386" id="Seg_10597" s="T1385">adv</ta>
            <ta e="T1387" id="Seg_10598" s="T1386">v-v:tense.[v:pn]</ta>
            <ta e="T1388" id="Seg_10599" s="T1387">n-n:case.poss</ta>
            <ta e="T1389" id="Seg_10600" s="T1388">adv</ta>
            <ta e="T1390" id="Seg_10601" s="T1389">pers</ta>
            <ta e="T1391" id="Seg_10602" s="T1390">n-n:case.poss</ta>
            <ta e="T1392" id="Seg_10603" s="T1391">v-v:tense.[v:pn]</ta>
            <ta e="T1394" id="Seg_10604" s="T1393">pers</ta>
            <ta e="T1395" id="Seg_10605" s="T1394">adv</ta>
            <ta e="T1396" id="Seg_10606" s="T1395">v-v:n-fin</ta>
            <ta e="T1397" id="Seg_10607" s="T1396">v-v:tense-v:pn</ta>
            <ta e="T1399" id="Seg_10608" s="T1398">dempro.[n:case]</ta>
            <ta e="T1400" id="Seg_10609" s="T1399">v-v:tense.[v:pn]</ta>
            <ta e="T1401" id="Seg_10610" s="T1400">conj</ta>
            <ta e="T1402" id="Seg_10611" s="T1401">pers</ta>
            <ta e="T1403" id="Seg_10612" s="T1402">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1404" id="Seg_10613" s="T1403">n-n:case.poss</ta>
            <ta e="T1405" id="Seg_10614" s="T1404">v-v:pn</ta>
            <ta e="T1406" id="Seg_10615" s="T1405">v-v:n.fin</ta>
            <ta e="T1407" id="Seg_10616" s="T1406">adv</ta>
            <ta e="T1408" id="Seg_10617" s="T1407">dempro-n:num</ta>
            <ta e="T1410" id="Seg_10618" s="T1409">v-v:tense-v:pn</ta>
            <ta e="T1412" id="Seg_10619" s="T1411">ptcl</ta>
            <ta e="T1413" id="Seg_10620" s="T1412">v-v:mood.pn</ta>
            <ta e="T1415" id="Seg_10621" s="T1414">adv</ta>
            <ta e="T1416" id="Seg_10622" s="T1415">pers</ta>
            <ta e="T1417" id="Seg_10623" s="T1416">n-n:case</ta>
            <ta e="T1418" id="Seg_10624" s="T1417">v-v:tense-v:pn</ta>
            <ta e="T1420" id="Seg_10625" s="T1419">adv</ta>
            <ta e="T1421" id="Seg_10626" s="T1420">v-v:tense-v:pn</ta>
            <ta e="T1423" id="Seg_10627" s="T1422">adv</ta>
            <ta e="T1424" id="Seg_10628" s="T1423">v-v:tense-v:pn</ta>
            <ta e="T1425" id="Seg_10629" s="T1424">pers</ta>
            <ta e="T1426" id="Seg_10630" s="T1425">v-v:tense-v:pn</ta>
            <ta e="T1428" id="Seg_10631" s="T1427">v-v:tense-v:pn</ta>
            <ta e="T1429" id="Seg_10632" s="T1428">adv</ta>
            <ta e="T1430" id="Seg_10633" s="T1429">v-v:n-fin</ta>
            <ta e="T1431" id="Seg_10634" s="T1430">v-v:tense.[v:pn]</ta>
            <ta e="T1432" id="Seg_10635" s="T1431">n-n:case.poss</ta>
            <ta e="T1434" id="Seg_10636" s="T1433">adv</ta>
            <ta e="T1435" id="Seg_10637" s="T1434">dempro.[n:case]</ta>
            <ta e="T1436" id="Seg_10638" s="T1435">v-v:tense.[v:pn]</ta>
            <ta e="T1437" id="Seg_10639" s="T1436">n-n:case</ta>
            <ta e="T1438" id="Seg_10640" s="T1437">n.[n:case]</ta>
            <ta e="T1439" id="Seg_10641" s="T1438">v-v:n.fin</ta>
            <ta e="T1441" id="Seg_10642" s="T1440">conj</ta>
            <ta e="T1442" id="Seg_10643" s="T1441">adv</ta>
            <ta e="T1443" id="Seg_10644" s="T1442">n.[n:case]</ta>
            <ta e="T1445" id="Seg_10645" s="T1444">adj.[n:case]</ta>
            <ta e="T1446" id="Seg_10646" s="T1445">v-v:tense.[v:pn]</ta>
            <ta e="T1447" id="Seg_10647" s="T1446">dempro.[n:case]</ta>
            <ta e="T1448" id="Seg_10648" s="T1447">n-%%-n:case</ta>
            <ta e="T1449" id="Seg_10649" s="T1448">refl-n:case.poss</ta>
            <ta e="T1450" id="Seg_10650" s="T1449">adv</ta>
            <ta e="T1451" id="Seg_10651" s="T1450">v-v:tense-v:pn</ta>
            <ta e="T1452" id="Seg_10652" s="T1451">n-n:case</ta>
            <ta e="T1454" id="Seg_10653" s="T1453">conj</ta>
            <ta e="T1455" id="Seg_10654" s="T1454">adv</ta>
            <ta e="T1456" id="Seg_10655" s="T1455">v-v:tense-v:pn</ta>
            <ta e="T1457" id="Seg_10656" s="T1456">n-n:case</ta>
            <ta e="T1458" id="Seg_10657" s="T1457">num.[n:case]</ta>
            <ta e="T1459" id="Seg_10658" s="T1458">n.[n:case]</ta>
            <ta e="T1460" id="Seg_10659" s="T1459">conj</ta>
            <ta e="T1461" id="Seg_10660" s="T1460">dempro.[n:case]</ta>
            <ta e="T1463" id="Seg_10661" s="T1462">n-n:case</ta>
            <ta e="T1465" id="Seg_10662" s="T1464">adv</ta>
            <ta e="T1466" id="Seg_10663" s="T1465">n.[n:case]</ta>
            <ta e="T1467" id="Seg_10664" s="T1466">num.[n:case]</ta>
            <ta e="T1468" id="Seg_10665" s="T1467">n.[n:case]</ta>
            <ta e="T1469" id="Seg_10666" s="T1468">v-v:tense.[v:pn]</ta>
            <ta e="T1470" id="Seg_10667" s="T1469">dempro.[n:case]</ta>
            <ta e="T1471" id="Seg_10668" s="T1470">n-n:case.poss</ta>
            <ta e="T1472" id="Seg_10669" s="T1471">v-v:tense.[v:pn]</ta>
            <ta e="T1473" id="Seg_10670" s="T1472">conj</ta>
            <ta e="T1474" id="Seg_10671" s="T1473">dempro.[n:case]</ta>
            <ta e="T1475" id="Seg_10672" s="T1474">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T1477" id="Seg_10673" s="T1476">n-n:case</ta>
            <ta e="T1498" id="Seg_10674" s="T1495">ptcl</ta>
            <ta e="T1500" id="Seg_10675" s="T1499">adv</ta>
            <ta e="T1506" id="Seg_10676" s="T1503">v</ta>
            <ta e="T1508" id="Seg_10677" s="T1506">adv</ta>
            <ta e="T1587" id="Seg_10678" s="T1586">pers</ta>
            <ta e="T1588" id="Seg_10679" s="T1587">adv</ta>
            <ta e="T1589" id="Seg_10680" s="T1588">v-v&gt;v-v:pn</ta>
            <ta e="T1590" id="Seg_10681" s="T1589">propr-n:ins-n:case</ta>
            <ta e="T1592" id="Seg_10682" s="T1591">dempro.[n:case]</ta>
            <ta e="T1594" id="Seg_10683" s="T1593">v-v:tense.[v:pn]</ta>
            <ta e="T1595" id="Seg_10684" s="T1594">v-v:tense-v:pn</ta>
            <ta e="T1596" id="Seg_10685" s="T1595">n-n:case</ta>
            <ta e="T1597" id="Seg_10686" s="T1596">pers</ta>
            <ta e="T1598" id="Seg_10687" s="T1597">v-v:mood-v:pn</ta>
            <ta e="T1599" id="Seg_10688" s="T1598">pers</ta>
            <ta e="T1600" id="Seg_10689" s="T1599">n-n:case</ta>
            <ta e="T1602" id="Seg_10690" s="T1601">adv</ta>
            <ta e="T1603" id="Seg_10691" s="T1602">pers</ta>
            <ta e="T1604" id="Seg_10692" s="T1603">que=ptcl</ta>
            <ta e="T1605" id="Seg_10693" s="T1604">ptcl</ta>
            <ta e="T1606" id="Seg_10694" s="T1605">v-v:tense-v:pn</ta>
            <ta e="T1607" id="Seg_10695" s="T1606">v-v:tense-v:pn</ta>
            <ta e="T1608" id="Seg_10696" s="T1607">dempro-n:case</ta>
            <ta e="T1610" id="Seg_10697" s="T1609">que</ta>
            <ta e="T1611" id="Seg_10698" s="T1610">dempro.[n:case]</ta>
            <ta e="T1612" id="Seg_10699" s="T1611">v-v:tense-v:pn</ta>
            <ta e="T1651" id="Seg_10700" s="T1650">pers</ta>
            <ta e="T1652" id="Seg_10701" s="T1651">adv</ta>
            <ta e="T1654" id="Seg_10702" s="T1653">v-v:tense-v:pn</ta>
            <ta e="T1655" id="Seg_10703" s="T1654">conj</ta>
            <ta e="T1656" id="Seg_10704" s="T1655">n-n:case.poss</ta>
            <ta e="T1657" id="Seg_10705" s="T1656">v-v:tense-v:pn</ta>
            <ta e="T1658" id="Seg_10706" s="T1657">num.[n:case]</ta>
            <ta e="T1659" id="Seg_10707" s="T1658">n.[n:case]</ta>
            <ta e="T1661" id="Seg_10708" s="T1660">dempro.[n:case]</ta>
            <ta e="T1662" id="Seg_10709" s="T1661">pers</ta>
            <ta e="T1664" id="Seg_10710" s="T1663">v-v&gt;v.[v:pn]</ta>
            <ta e="T1665" id="Seg_10711" s="T1664">v-v:mood-v:pn</ta>
            <ta e="T1666" id="Seg_10712" s="T1665">que=ptcl</ta>
            <ta e="T1668" id="Seg_10713" s="T1667">pers</ta>
            <ta e="T1669" id="Seg_10714" s="T1668">v-v&gt;v-v:pn</ta>
            <ta e="T1670" id="Seg_10715" s="T1669">ptcl</ta>
            <ta e="T1671" id="Seg_10716" s="T1670">v-v:tense-v:pn</ta>
            <ta e="T1698" id="Seg_10717" s="T1697">pers</ta>
            <ta e="T1699" id="Seg_10718" s="T1698">v-v:tense-v:pn</ta>
            <ta e="T1700" id="Seg_10719" s="T1699">adv</ta>
            <ta e="T1701" id="Seg_10720" s="T1700">conj</ta>
            <ta e="T1702" id="Seg_10721" s="T1701">n-n:case.poss</ta>
            <ta e="T1703" id="Seg_10722" s="T1702">v-v:tense-v:pn</ta>
            <ta e="T1704" id="Seg_10723" s="T1703">refl-n:case.poss</ta>
            <ta e="T1705" id="Seg_10724" s="T1704">n-n:case.poss</ta>
            <ta e="T1706" id="Seg_10725" s="T1705">n.[n:case]</ta>
            <ta e="T1709" id="Seg_10726" s="T1708">adv</ta>
            <ta e="T1710" id="Seg_10727" s="T1709">v-v:tense-v:pn</ta>
            <ta e="T1711" id="Seg_10728" s="T1710">pers</ta>
            <ta e="T1712" id="Seg_10729" s="T1711">v-v:tense-v:pn</ta>
            <ta e="T1715" id="Seg_10730" s="T1714">adv</ta>
            <ta e="T1716" id="Seg_10731" s="T1715">v-v:tense-v:pn</ta>
            <ta e="T1717" id="Seg_10732" s="T1716">que.[n:case]=ptcl</ta>
            <ta e="T1718" id="Seg_10733" s="T1717">v-v:tense-v:pn</ta>
            <ta e="T1719" id="Seg_10734" s="T1718">pers-n:case</ta>
            <ta e="T1721" id="Seg_10735" s="T1720">adv</ta>
            <ta e="T1725" id="Seg_10736" s="T1722">n.[n:case]</ta>
            <ta e="T1726" id="Seg_10737" s="T1725">v-v:tense-v:pn</ta>
            <ta e="T1727" id="Seg_10738" s="T1726">v-v:n.fin</ta>
            <ta e="T1728" id="Seg_10739" s="T1727">pers-n:case</ta>
            <ta e="T1730" id="Seg_10740" s="T1729">adv</ta>
            <ta e="T1731" id="Seg_10741" s="T1730">adv</ta>
            <ta e="T1800" id="Seg_10742" s="T1799">pers</ta>
            <ta e="T1801" id="Seg_10743" s="T1800">v-v:tense-v:pn</ta>
            <ta e="T1802" id="Seg_10744" s="T1801">dempro.[n:case]</ta>
            <ta e="T1803" id="Seg_10745" s="T1802">n.[n:case]</ta>
            <ta e="T1804" id="Seg_10746" s="T1803">v-v:tense.[v:pn]</ta>
            <ta e="T1805" id="Seg_10747" s="T1804">n.[n:case]</ta>
            <ta e="T1806" id="Seg_10748" s="T1805">que</ta>
            <ta e="T1807" id="Seg_10749" s="T1806">dempro.[n:case]</ta>
            <ta e="T1808" id="Seg_10750" s="T1807">n-n:case.poss</ta>
            <ta e="T1809" id="Seg_10751" s="T1808">v-v:tense.[v:pn]</ta>
            <ta e="T1811" id="Seg_10752" s="T1810">pers</ta>
            <ta e="T1812" id="Seg_10753" s="T1811">v-v:tense-v:pn</ta>
            <ta e="T1813" id="Seg_10754" s="T1812">num.[n:case]</ta>
            <ta e="T1814" id="Seg_10755" s="T1813">n.[n:case]</ta>
            <ta e="T1815" id="Seg_10756" s="T1814">dempro-n:case</ta>
            <ta e="T1816" id="Seg_10757" s="T1815">v-v:tense-v:pn</ta>
            <ta e="T1818" id="Seg_10758" s="T1817">dempro.[n:case]</ta>
            <ta e="T1820" id="Seg_10759" s="T1819">pers</ta>
            <ta e="T1821" id="Seg_10760" s="T1820">v-v:tense.[v:pn]</ta>
            <ta e="T1822" id="Seg_10761" s="T1821">n-n:num</ta>
            <ta e="T1823" id="Seg_10762" s="T1822">v-v:tense.[v:pn]</ta>
            <ta e="T1825" id="Seg_10763" s="T1824">n.[n:case]</ta>
            <ta e="T1826" id="Seg_10764" s="T1825">v-v:tense.[v:pn]</ta>
            <ta e="T1827" id="Seg_10765" s="T1826">n</ta>
            <ta e="T1829" id="Seg_10766" s="T1828">v-v&gt;v.[v:pn]</ta>
            <ta e="T1830" id="Seg_10767" s="T1829">v-v:ins-v:mood.pn</ta>
            <ta e="T1831" id="Seg_10768" s="T1830">v-v:ins-v:mood.pn</ta>
            <ta e="T1833" id="Seg_10769" s="T1832">pers</ta>
            <ta e="T1834" id="Seg_10770" s="T1833">v-v:tense-v:pn</ta>
            <ta e="T1835" id="Seg_10771" s="T1834">adv</ta>
            <ta e="T1837" id="Seg_10772" s="T1836">adv</ta>
            <ta e="T1838" id="Seg_10773" s="T1837">num.[n:case]</ta>
            <ta e="T1839" id="Seg_10774" s="T1838">n.[n:case]</ta>
            <ta e="T1840" id="Seg_10775" s="T1839">adj.[n:case]</ta>
            <ta e="T1841" id="Seg_10776" s="T1840">n.[n:case]</ta>
            <ta e="T1842" id="Seg_10777" s="T1841">v-v:tense-v:pn</ta>
            <ta e="T1844" id="Seg_10778" s="T1843">v-v:tense-v:pn</ta>
            <ta e="T1845" id="Seg_10779" s="T1844">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T379" id="Seg_10780" s="T378">adj</ta>
            <ta e="T380" id="Seg_10781" s="T379">n</ta>
            <ta e="T381" id="Seg_10782" s="T380">v</ta>
            <ta e="T382" id="Seg_10783" s="T381">num</ta>
            <ta e="T383" id="Seg_10784" s="T382">n</ta>
            <ta e="T384" id="Seg_10785" s="T383">n</ta>
            <ta e="T385" id="Seg_10786" s="T384">v</ta>
            <ta e="T394" id="Seg_10787" s="T393">n</ta>
            <ta e="T396" id="Seg_10788" s="T395">adv</ta>
            <ta e="T397" id="Seg_10789" s="T396">n</ta>
            <ta e="T398" id="Seg_10790" s="T397">pers</ta>
            <ta e="T399" id="Seg_10791" s="T398">n</ta>
            <ta e="T400" id="Seg_10792" s="T399">v</ta>
            <ta e="T402" id="Seg_10793" s="T401">pers</ta>
            <ta e="T403" id="Seg_10794" s="T402">dempro</ta>
            <ta e="T404" id="Seg_10795" s="T403">n</ta>
            <ta e="T405" id="Seg_10796" s="T404">v</ta>
            <ta e="T407" id="Seg_10797" s="T406">adv</ta>
            <ta e="T408" id="Seg_10798" s="T407">num</ta>
            <ta e="T410" id="Seg_10799" s="T409">v</ta>
            <ta e="T412" id="Seg_10800" s="T411">pers</ta>
            <ta e="T413" id="Seg_10801" s="T412">v</ta>
            <ta e="T414" id="Seg_10802" s="T413">pers</ta>
            <ta e="T415" id="Seg_10803" s="T414">num</ta>
            <ta e="T416" id="Seg_10804" s="T415">v</ta>
            <ta e="T418" id="Seg_10805" s="T417">pers</ta>
            <ta e="T420" id="Seg_10806" s="T418">v</ta>
            <ta e="T421" id="Seg_10807" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_10808" s="T421">v</ta>
            <ta e="T423" id="Seg_10809" s="T422">v</ta>
            <ta e="T424" id="Seg_10810" s="T423">v</ta>
            <ta e="T426" id="Seg_10811" s="T425">pers</ta>
            <ta e="T427" id="Seg_10812" s="T426">v</ta>
            <ta e="T428" id="Seg_10813" s="T427">que</ta>
            <ta e="T430" id="Seg_10814" s="T429">v</ta>
            <ta e="T432" id="Seg_10815" s="T431">adv</ta>
            <ta e="T433" id="Seg_10816" s="T432">pers</ta>
            <ta e="T434" id="Seg_10817" s="T433">v</ta>
            <ta e="T435" id="Seg_10818" s="T434">dempro</ta>
            <ta e="T436" id="Seg_10819" s="T435">pers</ta>
            <ta e="T437" id="Seg_10820" s="T436">v</ta>
            <ta e="T439" id="Seg_10821" s="T438">pers</ta>
            <ta e="T440" id="Seg_10822" s="T439">adj</ta>
            <ta e="T441" id="Seg_10823" s="T440">ptcl</ta>
            <ta e="T443" id="Seg_10824" s="T442">conj</ta>
            <ta e="T444" id="Seg_10825" s="T443">pers</ta>
            <ta e="T445" id="Seg_10826" s="T444">adv</ta>
            <ta e="T446" id="Seg_10827" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_10828" s="T446">adj</ta>
            <ta e="T450" id="Seg_10829" s="T449">adv</ta>
            <ta e="T452" id="Seg_10830" s="T451">v</ta>
            <ta e="T454" id="Seg_10831" s="T453">pers</ta>
            <ta e="T455" id="Seg_10832" s="T454">que</ta>
            <ta e="T456" id="Seg_10833" s="T455">dempro</ta>
            <ta e="T457" id="Seg_10834" s="T456">dempro</ta>
            <ta e="T458" id="Seg_10835" s="T457">v</ta>
            <ta e="T459" id="Seg_10836" s="T458">adv</ta>
            <ta e="T460" id="Seg_10837" s="T459">pers</ta>
            <ta e="T461" id="Seg_10838" s="T460">v</ta>
            <ta e="T462" id="Seg_10839" s="T461">n</ta>
            <ta e="T463" id="Seg_10840" s="T462">v</ta>
            <ta e="T466" id="Seg_10841" s="T465">n</ta>
            <ta e="T467" id="Seg_10842" s="T466">n</ta>
            <ta e="T468" id="Seg_10843" s="T467">n</ta>
            <ta e="T469" id="Seg_10844" s="T468">v</ta>
            <ta e="T470" id="Seg_10845" s="T469">n</ta>
            <ta e="T471" id="Seg_10846" s="T470">v</ta>
            <ta e="T473" id="Seg_10847" s="T472">pers</ta>
            <ta e="T474" id="Seg_10848" s="T473">pers</ta>
            <ta e="T476" id="Seg_10849" s="T475">que</ta>
            <ta e="T477" id="Seg_10850" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_10851" s="T477">v</ta>
            <ta e="T479" id="Seg_10852" s="T478">conj</ta>
            <ta e="T480" id="Seg_10853" s="T479">adv</ta>
            <ta e="T481" id="Seg_10854" s="T480">adv</ta>
            <ta e="T482" id="Seg_10855" s="T481">n</ta>
            <ta e="T483" id="Seg_10856" s="T482">que</ta>
            <ta e="T484" id="Seg_10857" s="T483">n</ta>
            <ta e="T485" id="Seg_10858" s="T484">v</ta>
            <ta e="T486" id="Seg_10859" s="T485">pers</ta>
            <ta e="T487" id="Seg_10860" s="T486">v</ta>
            <ta e="T573" id="Seg_10861" s="T257">que</ta>
            <ta e="T574" id="Seg_10862" s="T573">pers</ta>
            <ta e="T575" id="Seg_10863" s="T574">n</ta>
            <ta e="T576" id="Seg_10864" s="T575">v</ta>
            <ta e="T577" id="Seg_10865" s="T576">adv</ta>
            <ta e="T578" id="Seg_10866" s="T577">n</ta>
            <ta e="T579" id="Seg_10867" s="T578">dempro</ta>
            <ta e="T580" id="Seg_10868" s="T579">n</ta>
            <ta e="T581" id="Seg_10869" s="T580">propr</ta>
            <ta e="T582" id="Seg_10870" s="T581">n</ta>
            <ta e="T584" id="Seg_10871" s="T583">adv</ta>
            <ta e="T585" id="Seg_10872" s="T584">v</ta>
            <ta e="T586" id="Seg_10873" s="T585">n</ta>
            <ta e="T587" id="Seg_10874" s="T586">n</ta>
            <ta e="T588" id="Seg_10875" s="T587">v</ta>
            <ta e="T590" id="Seg_10876" s="T589">adv</ta>
            <ta e="T591" id="Seg_10877" s="T590">n</ta>
            <ta e="T594" id="Seg_10878" s="T593">n</ta>
            <ta e="T595" id="Seg_10879" s="T594">v</ta>
            <ta e="T597" id="Seg_10880" s="T596">adv</ta>
            <ta e="T598" id="Seg_10881" s="T597">v</ta>
            <ta e="T599" id="Seg_10882" s="T598">dempro</ta>
            <ta e="T600" id="Seg_10883" s="T599">v</ta>
            <ta e="T601" id="Seg_10884" s="T600">n</ta>
            <ta e="T602" id="Seg_10885" s="T601">pers</ta>
            <ta e="T603" id="Seg_10886" s="T602">v</ta>
            <ta e="T605" id="Seg_10887" s="T604">pers</ta>
            <ta e="T607" id="Seg_10888" s="T606">v</ta>
            <ta e="T608" id="Seg_10889" s="T607">que</ta>
            <ta e="T609" id="Seg_10890" s="T608">n</ta>
            <ta e="T610" id="Seg_10891" s="T609">v</ta>
            <ta e="T612" id="Seg_10892" s="T611">conj</ta>
            <ta e="T613" id="Seg_10893" s="T612">adv</ta>
            <ta e="T614" id="Seg_10894" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_10895" s="T614">dempro</ta>
            <ta e="T616" id="Seg_10896" s="T615">conj</ta>
            <ta e="T617" id="Seg_10897" s="T616">v</ta>
            <ta e="T619" id="Seg_10898" s="T618">pers</ta>
            <ta e="T620" id="Seg_10899" s="T619">pers</ta>
            <ta e="T621" id="Seg_10900" s="T620">n</ta>
            <ta e="T622" id="Seg_10901" s="T621">v</ta>
            <ta e="T623" id="Seg_10902" s="T622">n</ta>
            <ta e="T624" id="Seg_10903" s="T623">conj</ta>
            <ta e="T625" id="Seg_10904" s="T624">adv</ta>
            <ta e="T626" id="Seg_10905" s="T625">v</ta>
            <ta e="T1263" id="Seg_10906" s="T1262">que</ta>
            <ta e="T1264" id="Seg_10907" s="T1263">dempro</ta>
            <ta e="T1265" id="Seg_10908" s="T1264">v</ta>
            <ta e="T1266" id="Seg_10909" s="T1265">pers</ta>
            <ta e="T1267" id="Seg_10910" s="T1266">v</ta>
            <ta e="T1268" id="Seg_10911" s="T1267">adv</ta>
            <ta e="T1269" id="Seg_10912" s="T1268">n</ta>
            <ta e="T1270" id="Seg_10913" s="T1269">n</ta>
            <ta e="T1271" id="Seg_10914" s="T1270">v</ta>
            <ta e="T1272" id="Seg_10915" s="T1271">v</ta>
            <ta e="T1273" id="Seg_10916" s="T1272">pers</ta>
            <ta e="T1274" id="Seg_10917" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_10918" s="T1274">v</ta>
            <ta e="T1277" id="Seg_10919" s="T1276">pers</ta>
            <ta e="T1278" id="Seg_10920" s="T1277">v</ta>
            <ta e="T1279" id="Seg_10921" s="T1278">v</ta>
            <ta e="T1280" id="Seg_10922" s="T1279">pers</ta>
            <ta e="T1281" id="Seg_10923" s="T1280">v</ta>
            <ta e="T1282" id="Seg_10924" s="T1281">n</ta>
            <ta e="T1283" id="Seg_10925" s="T1282">conj</ta>
            <ta e="T1284" id="Seg_10926" s="T1283">n</ta>
            <ta e="T1285" id="Seg_10927" s="T1284">dempro</ta>
            <ta e="T1286" id="Seg_10928" s="T1285">v</ta>
            <ta e="T1287" id="Seg_10929" s="T1286">dempro</ta>
            <ta e="T1288" id="Seg_10930" s="T1287">ptcl</ta>
            <ta e="T1289" id="Seg_10931" s="T1288">v</ta>
            <ta e="T1290" id="Seg_10932" s="T1289">v</ta>
            <ta e="T1292" id="Seg_10933" s="T1291">adv</ta>
            <ta e="T1293" id="Seg_10934" s="T1292">pers</ta>
            <ta e="T1294" id="Seg_10935" s="T1293">n</ta>
            <ta e="T1295" id="Seg_10936" s="T1294">v</ta>
            <ta e="T1297" id="Seg_10937" s="T1296">n</ta>
            <ta e="T1298" id="Seg_10938" s="T1297">n</ta>
            <ta e="T1299" id="Seg_10939" s="T1298">v</ta>
            <ta e="T1300" id="Seg_10940" s="T1299">v</ta>
            <ta e="T1301" id="Seg_10941" s="T1300">v</ta>
            <ta e="T1302" id="Seg_10942" s="T1301">propr</ta>
            <ta e="T1303" id="Seg_10943" s="T1302">n</ta>
            <ta e="T1305" id="Seg_10944" s="T1304">ptcl</ta>
            <ta e="T1306" id="Seg_10945" s="T1305">n</ta>
            <ta e="T1307" id="Seg_10946" s="T1306">adv</ta>
            <ta e="T1308" id="Seg_10947" s="T1307">v</ta>
            <ta e="T1309" id="Seg_10948" s="T1308">adv</ta>
            <ta e="T1310" id="Seg_10949" s="T1309">v</ta>
            <ta e="T1311" id="Seg_10950" s="T1310">adv</ta>
            <ta e="T1312" id="Seg_10951" s="T1311">dempro</ta>
            <ta e="T1313" id="Seg_10952" s="T1312">n</ta>
            <ta e="T1314" id="Seg_10953" s="T1313">propr</ta>
            <ta e="T1316" id="Seg_10954" s="T1315">adv</ta>
            <ta e="T1317" id="Seg_10955" s="T1316">adj</ta>
            <ta e="T1319" id="Seg_10956" s="T1318">n</ta>
            <ta e="T1320" id="Seg_10957" s="T1319">v</ta>
            <ta e="T1321" id="Seg_10958" s="T1320">adv</ta>
            <ta e="T1322" id="Seg_10959" s="T1321">n</ta>
            <ta e="T1323" id="Seg_10960" s="T1322">v</ta>
            <ta e="T1324" id="Seg_10961" s="T1323">dempro</ta>
            <ta e="T1325" id="Seg_10962" s="T1324">adv</ta>
            <ta e="T1326" id="Seg_10963" s="T1325">n</ta>
            <ta e="T1328" id="Seg_10964" s="T1327">adv</ta>
            <ta e="T1329" id="Seg_10965" s="T1328">dempro</ta>
            <ta e="T1330" id="Seg_10966" s="T1329">n</ta>
            <ta e="T1331" id="Seg_10967" s="T1330">v</ta>
            <ta e="T1332" id="Seg_10968" s="T1331">adv</ta>
            <ta e="T1333" id="Seg_10969" s="T1332">n</ta>
            <ta e="T1334" id="Seg_10970" s="T1333">n</ta>
            <ta e="T1336" id="Seg_10971" s="T1335">adv</ta>
            <ta e="T1337" id="Seg_10972" s="T1336">v</ta>
            <ta e="T1339" id="Seg_10973" s="T1338">adv</ta>
            <ta e="T1340" id="Seg_10974" s="T1339">v</ta>
            <ta e="T1343" id="Seg_10975" s="T1342">n</ta>
            <ta e="T1344" id="Seg_10976" s="T1343">v</ta>
            <ta e="T1345" id="Seg_10977" s="T1344">adv</ta>
            <ta e="T1346" id="Seg_10978" s="T1345">ptcl</ta>
            <ta e="T1348" id="Seg_10979" s="T1347">adv</ta>
            <ta e="T1349" id="Seg_10980" s="T1348">adv</ta>
            <ta e="T1350" id="Seg_10981" s="T1349">n</ta>
            <ta e="T1351" id="Seg_10982" s="T1350">v</ta>
            <ta e="T1353" id="Seg_10983" s="T1352">v</ta>
            <ta e="T1354" id="Seg_10984" s="T1353">pers</ta>
            <ta e="T1355" id="Seg_10985" s="T1354">n</ta>
            <ta e="T1356" id="Seg_10986" s="T1355">v</ta>
            <ta e="T1358" id="Seg_10987" s="T1357">adv</ta>
            <ta e="T1359" id="Seg_10988" s="T1358">dempro</ta>
            <ta e="T1360" id="Seg_10989" s="T1359">n</ta>
            <ta e="T1361" id="Seg_10990" s="T1360">v</ta>
            <ta e="T1363" id="Seg_10991" s="T1362">pers</ta>
            <ta e="T1364" id="Seg_10992" s="T1363">v</ta>
            <ta e="T1365" id="Seg_10993" s="T1364">dempro</ta>
            <ta e="T1366" id="Seg_10994" s="T1365">dempro</ta>
            <ta e="T1367" id="Seg_10995" s="T1366">v</ta>
            <ta e="T1368" id="Seg_10996" s="T1367">v</ta>
            <ta e="T1370" id="Seg_10997" s="T1369">pers</ta>
            <ta e="T1371" id="Seg_10998" s="T1370">ptcl</ta>
            <ta e="T1372" id="Seg_10999" s="T1371">n</ta>
            <ta e="T1373" id="Seg_11000" s="T1372">v</ta>
            <ta e="T1374" id="Seg_11001" s="T1373">conj</ta>
            <ta e="T1375" id="Seg_11002" s="T1374">dempro</ta>
            <ta e="T1377" id="Seg_11003" s="T1376">dempro</ta>
            <ta e="T1378" id="Seg_11004" s="T1377">adv</ta>
            <ta e="T1379" id="Seg_11005" s="T1378">n</ta>
            <ta e="T1380" id="Seg_11006" s="T1379">v</ta>
            <ta e="T1381" id="Seg_11007" s="T1380">dempro</ta>
            <ta e="T1382" id="Seg_11008" s="T1381">dempro</ta>
            <ta e="T1383" id="Seg_11009" s="T1382">v</ta>
            <ta e="T1384" id="Seg_11010" s="T1383">pers</ta>
            <ta e="T1386" id="Seg_11011" s="T1385">adv</ta>
            <ta e="T1387" id="Seg_11012" s="T1386">v</ta>
            <ta e="T1388" id="Seg_11013" s="T1387">n</ta>
            <ta e="T1389" id="Seg_11014" s="T1388">adv</ta>
            <ta e="T1390" id="Seg_11015" s="T1389">pers</ta>
            <ta e="T1391" id="Seg_11016" s="T1390">n</ta>
            <ta e="T1392" id="Seg_11017" s="T1391">v</ta>
            <ta e="T1394" id="Seg_11018" s="T1393">pers</ta>
            <ta e="T1395" id="Seg_11019" s="T1394">adv</ta>
            <ta e="T1396" id="Seg_11020" s="T1395">v</ta>
            <ta e="T1397" id="Seg_11021" s="T1396">v</ta>
            <ta e="T1399" id="Seg_11022" s="T1398">dempro</ta>
            <ta e="T1400" id="Seg_11023" s="T1399">v</ta>
            <ta e="T1401" id="Seg_11024" s="T1400">conj</ta>
            <ta e="T1402" id="Seg_11025" s="T1401">pers</ta>
            <ta e="T1403" id="Seg_11026" s="T1402">v</ta>
            <ta e="T1404" id="Seg_11027" s="T1403">n</ta>
            <ta e="T1405" id="Seg_11028" s="T1404">v</ta>
            <ta e="T1406" id="Seg_11029" s="T1405">v</ta>
            <ta e="T1407" id="Seg_11030" s="T1406">adv</ta>
            <ta e="T1408" id="Seg_11031" s="T1407">dempro</ta>
            <ta e="T1410" id="Seg_11032" s="T1409">v</ta>
            <ta e="T1412" id="Seg_11033" s="T1411">ptcl</ta>
            <ta e="T1413" id="Seg_11034" s="T1412">v</ta>
            <ta e="T1415" id="Seg_11035" s="T1414">adv</ta>
            <ta e="T1416" id="Seg_11036" s="T1415">pers</ta>
            <ta e="T1417" id="Seg_11037" s="T1416">n</ta>
            <ta e="T1418" id="Seg_11038" s="T1417">v</ta>
            <ta e="T1420" id="Seg_11039" s="T1419">adv</ta>
            <ta e="T1421" id="Seg_11040" s="T1420">v</ta>
            <ta e="T1423" id="Seg_11041" s="T1422">adv</ta>
            <ta e="T1424" id="Seg_11042" s="T1423">v</ta>
            <ta e="T1425" id="Seg_11043" s="T1424">pers</ta>
            <ta e="T1426" id="Seg_11044" s="T1425">v</ta>
            <ta e="T1428" id="Seg_11045" s="T1427">v</ta>
            <ta e="T1429" id="Seg_11046" s="T1428">adv</ta>
            <ta e="T1430" id="Seg_11047" s="T1429">v</ta>
            <ta e="T1431" id="Seg_11048" s="T1430">v</ta>
            <ta e="T1432" id="Seg_11049" s="T1431">n</ta>
            <ta e="T1434" id="Seg_11050" s="T1433">adv</ta>
            <ta e="T1435" id="Seg_11051" s="T1434">dempro</ta>
            <ta e="T1436" id="Seg_11052" s="T1435">v</ta>
            <ta e="T1437" id="Seg_11053" s="T1436">n</ta>
            <ta e="T1438" id="Seg_11054" s="T1437">n</ta>
            <ta e="T1439" id="Seg_11055" s="T1438">v</ta>
            <ta e="T1441" id="Seg_11056" s="T1440">conj</ta>
            <ta e="T1442" id="Seg_11057" s="T1441">adv</ta>
            <ta e="T1443" id="Seg_11058" s="T1442">n</ta>
            <ta e="T1445" id="Seg_11059" s="T1444">adj</ta>
            <ta e="T1446" id="Seg_11060" s="T1445">v</ta>
            <ta e="T1447" id="Seg_11061" s="T1446">dempro</ta>
            <ta e="T1448" id="Seg_11062" s="T1447">n</ta>
            <ta e="T1449" id="Seg_11063" s="T1448">refl</ta>
            <ta e="T1450" id="Seg_11064" s="T1449">adv</ta>
            <ta e="T1451" id="Seg_11065" s="T1450">v</ta>
            <ta e="T1452" id="Seg_11066" s="T1451">n</ta>
            <ta e="T1454" id="Seg_11067" s="T1453">conj</ta>
            <ta e="T1455" id="Seg_11068" s="T1454">adv</ta>
            <ta e="T1456" id="Seg_11069" s="T1455">v</ta>
            <ta e="T1457" id="Seg_11070" s="T1456">n</ta>
            <ta e="T1458" id="Seg_11071" s="T1457">num</ta>
            <ta e="T1459" id="Seg_11072" s="T1458">n</ta>
            <ta e="T1460" id="Seg_11073" s="T1459">conj</ta>
            <ta e="T1461" id="Seg_11074" s="T1460">dempro</ta>
            <ta e="T1463" id="Seg_11075" s="T1462">n</ta>
            <ta e="T1465" id="Seg_11076" s="T1464">adv</ta>
            <ta e="T1466" id="Seg_11077" s="T1465">n</ta>
            <ta e="T1467" id="Seg_11078" s="T1466">num</ta>
            <ta e="T1468" id="Seg_11079" s="T1467">n</ta>
            <ta e="T1469" id="Seg_11080" s="T1468">v</ta>
            <ta e="T1470" id="Seg_11081" s="T1469">dempro</ta>
            <ta e="T1471" id="Seg_11082" s="T1470">n</ta>
            <ta e="T1472" id="Seg_11083" s="T1471">v</ta>
            <ta e="T1473" id="Seg_11084" s="T1472">conj</ta>
            <ta e="T1474" id="Seg_11085" s="T1473">dempro</ta>
            <ta e="T1475" id="Seg_11086" s="T1474">v</ta>
            <ta e="T1477" id="Seg_11087" s="T1476">n</ta>
            <ta e="T1498" id="Seg_11088" s="T1495">ptcl</ta>
            <ta e="T1500" id="Seg_11089" s="T1499">adv</ta>
            <ta e="T1506" id="Seg_11090" s="T1503">v</ta>
            <ta e="T1508" id="Seg_11091" s="T1506">adv</ta>
            <ta e="T1587" id="Seg_11092" s="T1586">pers</ta>
            <ta e="T1588" id="Seg_11093" s="T1587">adv</ta>
            <ta e="T1589" id="Seg_11094" s="T1588">v</ta>
            <ta e="T1590" id="Seg_11095" s="T1589">propr</ta>
            <ta e="T1592" id="Seg_11096" s="T1591">dempro</ta>
            <ta e="T1594" id="Seg_11097" s="T1593">v</ta>
            <ta e="T1595" id="Seg_11098" s="T1594">v</ta>
            <ta e="T1596" id="Seg_11099" s="T1595">n</ta>
            <ta e="T1597" id="Seg_11100" s="T1596">pers</ta>
            <ta e="T1598" id="Seg_11101" s="T1597">v</ta>
            <ta e="T1599" id="Seg_11102" s="T1598">pers</ta>
            <ta e="T1600" id="Seg_11103" s="T1599">n</ta>
            <ta e="T1602" id="Seg_11104" s="T1601">adv</ta>
            <ta e="T1603" id="Seg_11105" s="T1602">pers</ta>
            <ta e="T1604" id="Seg_11106" s="T1603">que</ta>
            <ta e="T1605" id="Seg_11107" s="T1604">ptcl</ta>
            <ta e="T1606" id="Seg_11108" s="T1605">v</ta>
            <ta e="T1607" id="Seg_11109" s="T1606">v</ta>
            <ta e="T1608" id="Seg_11110" s="T1607">dempro</ta>
            <ta e="T1610" id="Seg_11111" s="T1609">que</ta>
            <ta e="T1611" id="Seg_11112" s="T1610">dempro</ta>
            <ta e="T1612" id="Seg_11113" s="T1611">v</ta>
            <ta e="T1651" id="Seg_11114" s="T1650">pers</ta>
            <ta e="T1652" id="Seg_11115" s="T1651">adv</ta>
            <ta e="T1654" id="Seg_11116" s="T1653">v</ta>
            <ta e="T1655" id="Seg_11117" s="T1654">conj</ta>
            <ta e="T1656" id="Seg_11118" s="T1655">n</ta>
            <ta e="T1657" id="Seg_11119" s="T1656">v</ta>
            <ta e="T1658" id="Seg_11120" s="T1657">num</ta>
            <ta e="T1659" id="Seg_11121" s="T1658">n</ta>
            <ta e="T1661" id="Seg_11122" s="T1660">dempro</ta>
            <ta e="T1662" id="Seg_11123" s="T1661">pers</ta>
            <ta e="T1664" id="Seg_11124" s="T1663">v</ta>
            <ta e="T1665" id="Seg_11125" s="T1664">v</ta>
            <ta e="T1666" id="Seg_11126" s="T1665">que</ta>
            <ta e="T1668" id="Seg_11127" s="T1667">pers</ta>
            <ta e="T1669" id="Seg_11128" s="T1668">v</ta>
            <ta e="T1670" id="Seg_11129" s="T1669">ptcl</ta>
            <ta e="T1671" id="Seg_11130" s="T1670">v</ta>
            <ta e="T1698" id="Seg_11131" s="T1697">pers</ta>
            <ta e="T1699" id="Seg_11132" s="T1698">v</ta>
            <ta e="T1700" id="Seg_11133" s="T1699">adv</ta>
            <ta e="T1701" id="Seg_11134" s="T1700">conj</ta>
            <ta e="T1702" id="Seg_11135" s="T1701">n</ta>
            <ta e="T1703" id="Seg_11136" s="T1702">v</ta>
            <ta e="T1704" id="Seg_11137" s="T1703">refl</ta>
            <ta e="T1705" id="Seg_11138" s="T1704">n</ta>
            <ta e="T1706" id="Seg_11139" s="T1705">n</ta>
            <ta e="T1709" id="Seg_11140" s="T1708">adv</ta>
            <ta e="T1710" id="Seg_11141" s="T1709">v</ta>
            <ta e="T1711" id="Seg_11142" s="T1710">pers</ta>
            <ta e="T1712" id="Seg_11143" s="T1711">v</ta>
            <ta e="T1715" id="Seg_11144" s="T1714">adv</ta>
            <ta e="T1716" id="Seg_11145" s="T1715">v</ta>
            <ta e="T1717" id="Seg_11146" s="T1716">que</ta>
            <ta e="T1718" id="Seg_11147" s="T1717">v</ta>
            <ta e="T1719" id="Seg_11148" s="T1718">pers</ta>
            <ta e="T1721" id="Seg_11149" s="T1720">adv</ta>
            <ta e="T1725" id="Seg_11150" s="T1722">n</ta>
            <ta e="T1726" id="Seg_11151" s="T1725">v</ta>
            <ta e="T1727" id="Seg_11152" s="T1726">v</ta>
            <ta e="T1728" id="Seg_11153" s="T1727">pers</ta>
            <ta e="T1730" id="Seg_11154" s="T1729">adv</ta>
            <ta e="T1731" id="Seg_11155" s="T1730">adv</ta>
            <ta e="T1800" id="Seg_11156" s="T1799">pers</ta>
            <ta e="T1801" id="Seg_11157" s="T1800">v</ta>
            <ta e="T1802" id="Seg_11158" s="T1801">dempro</ta>
            <ta e="T1803" id="Seg_11159" s="T1802">n</ta>
            <ta e="T1804" id="Seg_11160" s="T1803">v</ta>
            <ta e="T1805" id="Seg_11161" s="T1804">n</ta>
            <ta e="T1806" id="Seg_11162" s="T1805">que</ta>
            <ta e="T1807" id="Seg_11163" s="T1806">dempro</ta>
            <ta e="T1808" id="Seg_11164" s="T1807">n</ta>
            <ta e="T1809" id="Seg_11165" s="T1808">v</ta>
            <ta e="T1811" id="Seg_11166" s="T1810">pers</ta>
            <ta e="T1812" id="Seg_11167" s="T1811">v</ta>
            <ta e="T1813" id="Seg_11168" s="T1812">num</ta>
            <ta e="T1814" id="Seg_11169" s="T1813">n</ta>
            <ta e="T1815" id="Seg_11170" s="T1814">dempro</ta>
            <ta e="T1816" id="Seg_11171" s="T1815">v</ta>
            <ta e="T1818" id="Seg_11172" s="T1817">dempro</ta>
            <ta e="T1820" id="Seg_11173" s="T1819">pers</ta>
            <ta e="T1821" id="Seg_11174" s="T1820">v</ta>
            <ta e="T1822" id="Seg_11175" s="T1821">n</ta>
            <ta e="T1823" id="Seg_11176" s="T1822">v</ta>
            <ta e="T1825" id="Seg_11177" s="T1824">n</ta>
            <ta e="T1826" id="Seg_11178" s="T1825">v</ta>
            <ta e="T1827" id="Seg_11179" s="T1826">n</ta>
            <ta e="T1829" id="Seg_11180" s="T1828">v</ta>
            <ta e="T1830" id="Seg_11181" s="T1829">v</ta>
            <ta e="T1831" id="Seg_11182" s="T1830">v</ta>
            <ta e="T1833" id="Seg_11183" s="T1832">pers</ta>
            <ta e="T1834" id="Seg_11184" s="T1833">v</ta>
            <ta e="T1835" id="Seg_11185" s="T1834">adj</ta>
            <ta e="T1837" id="Seg_11186" s="T1836">adv</ta>
            <ta e="T1838" id="Seg_11187" s="T1837">num</ta>
            <ta e="T1839" id="Seg_11188" s="T1838">n</ta>
            <ta e="T1840" id="Seg_11189" s="T1839">adj</ta>
            <ta e="T1841" id="Seg_11190" s="T1840">n</ta>
            <ta e="T1842" id="Seg_11191" s="T1841">v</ta>
            <ta e="T1844" id="Seg_11192" s="T1843">v</ta>
            <ta e="T1845" id="Seg_11193" s="T1844">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T380" id="Seg_11194" s="T379">np.h:A</ta>
            <ta e="T384" id="Seg_11195" s="T383">np.h:A</ta>
            <ta e="T396" id="Seg_11196" s="T395">adv:Time</ta>
            <ta e="T397" id="Seg_11197" s="T396">np.h:A</ta>
            <ta e="T398" id="Seg_11198" s="T397">pro:G</ta>
            <ta e="T402" id="Seg_11199" s="T401">pro.h:A</ta>
            <ta e="T403" id="Seg_11200" s="T402">pro:Th</ta>
            <ta e="T407" id="Seg_11201" s="T406">adv:Time</ta>
            <ta e="T408" id="Seg_11202" s="T407">pro:Th</ta>
            <ta e="T412" id="Seg_11203" s="T411">pro.h:A</ta>
            <ta e="T414" id="Seg_11204" s="T413">0.2.h:A</ta>
            <ta e="T415" id="Seg_11205" s="T414">np.h:Th</ta>
            <ta e="T418" id="Seg_11206" s="T417">pro.h:A</ta>
            <ta e="T422" id="Seg_11207" s="T421">0.2.h:E</ta>
            <ta e="T426" id="Seg_11208" s="T425">pro.h:A</ta>
            <ta e="T428" id="Seg_11209" s="T427">pro.h:P</ta>
            <ta e="T432" id="Seg_11210" s="T431">adv:Time</ta>
            <ta e="T433" id="Seg_11211" s="T432">pro.h:A</ta>
            <ta e="T436" id="Seg_11212" s="T435">pro.h:P</ta>
            <ta e="T439" id="Seg_11213" s="T438">pro.h:Th</ta>
            <ta e="T444" id="Seg_11214" s="T443">pro.h:Th</ta>
            <ta e="T450" id="Seg_11215" s="T449">adv:Time</ta>
            <ta e="T451" id="Seg_11216" s="T450">np:G</ta>
            <ta e="T452" id="Seg_11217" s="T451">0.1.h:A</ta>
            <ta e="T457" id="Seg_11218" s="T456">pro.h:A</ta>
            <ta e="T459" id="Seg_11219" s="T458">adv:L</ta>
            <ta e="T460" id="Seg_11220" s="T459">pro.h:A</ta>
            <ta e="T462" id="Seg_11221" s="T461">np:P</ta>
            <ta e="T463" id="Seg_11222" s="T462">0.1.h:A</ta>
            <ta e="T468" id="Seg_11223" s="T467">np:Th</ta>
            <ta e="T470" id="Seg_11224" s="T469">np:G</ta>
            <ta e="T471" id="Seg_11225" s="T470">0.3:Th</ta>
            <ta e="T474" id="Seg_11226" s="T473">pro.h:A</ta>
            <ta e="T476" id="Seg_11227" s="T475">pro.h:R</ta>
            <ta e="T480" id="Seg_11228" s="T479">adv:Time</ta>
            <ta e="T481" id="Seg_11229" s="T480">adv:L</ta>
            <ta e="T482" id="Seg_11230" s="T481">np.h:P</ta>
            <ta e="T484" id="Seg_11231" s="T483">np:L</ta>
            <ta e="T486" id="Seg_11232" s="T485">pro.h:A</ta>
            <ta e="T574" id="Seg_11233" s="T573">pro.h:Poss</ta>
            <ta e="T575" id="Seg_11234" s="T574">np.h:A</ta>
            <ta e="T580" id="Seg_11235" s="T579">np:G</ta>
            <ta e="T581" id="Seg_11236" s="T580">np:G</ta>
            <ta e="T582" id="Seg_11237" s="T581">np:G</ta>
            <ta e="T584" id="Seg_11238" s="T583">adv:Time</ta>
            <ta e="T585" id="Seg_11239" s="T584">0.3.h:A</ta>
            <ta e="T587" id="Seg_11240" s="T586">np:Th</ta>
            <ta e="T590" id="Seg_11241" s="T589">adv:Time</ta>
            <ta e="T591" id="Seg_11242" s="T590">np:So</ta>
            <ta e="T594" id="Seg_11243" s="T593">np:G</ta>
            <ta e="T595" id="Seg_11244" s="T594">0.3:Th</ta>
            <ta e="T597" id="Seg_11245" s="T596">adv:Time</ta>
            <ta e="T599" id="Seg_11246" s="T598">pro.h:A</ta>
            <ta e="T601" id="Seg_11247" s="T600">np:G</ta>
            <ta e="T602" id="Seg_11248" s="T601">pro.h:R</ta>
            <ta e="T603" id="Seg_11249" s="T602">0.3.h:A</ta>
            <ta e="T605" id="Seg_11250" s="T604">pro.h:A</ta>
            <ta e="T608" id="Seg_11251" s="T607">pro.h:P</ta>
            <ta e="T613" id="Seg_11252" s="T612">adv:Time</ta>
            <ta e="T615" id="Seg_11253" s="T614">0.3:Th</ta>
            <ta e="T620" id="Seg_11254" s="T619">pro.h:Poss</ta>
            <ta e="T621" id="Seg_11255" s="T620">np.h:A</ta>
            <ta e="T623" id="Seg_11256" s="T622">np:G</ta>
            <ta e="T625" id="Seg_11257" s="T624">adv:L</ta>
            <ta e="T626" id="Seg_11258" s="T625">0.3.h:P</ta>
            <ta e="T1264" id="Seg_11259" s="T1263">pro.h:A</ta>
            <ta e="T1266" id="Seg_11260" s="T1265">pro:G</ta>
            <ta e="T1267" id="Seg_11261" s="T1266">0.1.h:A</ta>
            <ta e="T1268" id="Seg_11262" s="T1267">adv:L</ta>
            <ta e="T1269" id="Seg_11263" s="T1268">np.h:A</ta>
            <ta e="T1270" id="Seg_11264" s="T1269">np:G</ta>
            <ta e="T1273" id="Seg_11265" s="T1272">pro.h:E</ta>
            <ta e="T1277" id="Seg_11266" s="T1276">pro.h:A</ta>
            <ta e="T1279" id="Seg_11267" s="T1278">0.2.h:A</ta>
            <ta e="T1280" id="Seg_11268" s="T1279">pro:G</ta>
            <ta e="T1281" id="Seg_11269" s="T1280">0.2.h:A</ta>
            <ta e="T1285" id="Seg_11270" s="T1284">pro.h:A</ta>
            <ta e="T1287" id="Seg_11271" s="T1286">pro.h:A</ta>
            <ta e="T1290" id="Seg_11272" s="T1289">0.1.h:A</ta>
            <ta e="T1292" id="Seg_11273" s="T1291">adv:Time</ta>
            <ta e="T1293" id="Seg_11274" s="T1292">pro.h:A</ta>
            <ta e="T1294" id="Seg_11275" s="T1293">np:Th</ta>
            <ta e="T1297" id="Seg_11276" s="T1296">n:Time</ta>
            <ta e="T1298" id="Seg_11277" s="T1297">np:Th</ta>
            <ta e="T1299" id="Seg_11278" s="T1298">0.1.h:A</ta>
            <ta e="T1300" id="Seg_11279" s="T1299">0.1.h:A</ta>
            <ta e="T1301" id="Seg_11280" s="T1300">0.1.h:A</ta>
            <ta e="T1303" id="Seg_11281" s="T1301">np:G</ta>
            <ta e="T1306" id="Seg_11282" s="T1305">n:Time</ta>
            <ta e="T1307" id="Seg_11283" s="T1306">adv:L</ta>
            <ta e="T1308" id="Seg_11284" s="T1307">0.1.h:E</ta>
            <ta e="T1309" id="Seg_11285" s="T1308">adv:Time</ta>
            <ta e="T1310" id="Seg_11286" s="T1309">0.1.h:A</ta>
            <ta e="T1313" id="Seg_11287" s="T1312">np:G</ta>
            <ta e="T1314" id="Seg_11288" s="T1313">np:G</ta>
            <ta e="T1316" id="Seg_11289" s="T1315">adv:L</ta>
            <ta e="T1320" id="Seg_11290" s="T1319">0.1.h:Th</ta>
            <ta e="T1321" id="Seg_11291" s="T1320">adv:Time</ta>
            <ta e="T1322" id="Seg_11292" s="T1321">n:Time</ta>
            <ta e="T1323" id="Seg_11293" s="T1322">0.1.h:A</ta>
            <ta e="T1325" id="Seg_11294" s="T1324">adv:L</ta>
            <ta e="T1326" id="Seg_11295" s="T1325">np:G</ta>
            <ta e="T1328" id="Seg_11296" s="T1327">adv:Time</ta>
            <ta e="T1330" id="Seg_11297" s="T1329">n:Time</ta>
            <ta e="T1332" id="Seg_11298" s="T1331">adv:L</ta>
            <ta e="T1334" id="Seg_11299" s="T1333">np.h:A</ta>
            <ta e="T1336" id="Seg_11300" s="T1335">adv:Time</ta>
            <ta e="T1337" id="Seg_11301" s="T1336">0.1.h:A</ta>
            <ta e="T1339" id="Seg_11302" s="T1338">adv:L</ta>
            <ta e="T1340" id="Seg_11303" s="T1339">0.1.h:E</ta>
            <ta e="T1343" id="Seg_11304" s="T1342">np:P</ta>
            <ta e="T1344" id="Seg_11305" s="T1343">0.1.h:A</ta>
            <ta e="T1345" id="Seg_11306" s="T1344">adv:L</ta>
            <ta e="T1348" id="Seg_11307" s="T1347">adv:Time</ta>
            <ta e="T1349" id="Seg_11308" s="T1348">adv:L</ta>
            <ta e="T1350" id="Seg_11309" s="T1349">np.h:A</ta>
            <ta e="T1353" id="Seg_11310" s="T1352">0.2.h:A</ta>
            <ta e="T1354" id="Seg_11311" s="T1353">pro:G</ta>
            <ta e="T1355" id="Seg_11312" s="T1354">np:P</ta>
            <ta e="T1358" id="Seg_11313" s="T1357">adv:Time</ta>
            <ta e="T1359" id="Seg_11314" s="T1358">pro.h:A</ta>
            <ta e="T1360" id="Seg_11315" s="T1359">np.h:Th</ta>
            <ta e="T1363" id="Seg_11316" s="T1362">pro.h:R</ta>
            <ta e="T1364" id="Seg_11317" s="T1363">0.3.h:A</ta>
            <ta e="T1365" id="Seg_11318" s="T1364">pro.h:E</ta>
            <ta e="T1366" id="Seg_11319" s="T1365">pro.h:Com</ta>
            <ta e="T1370" id="Seg_11320" s="T1369">pro.h:A</ta>
            <ta e="T1372" id="Seg_11321" s="T1371">np:Th</ta>
            <ta e="T1378" id="Seg_11322" s="T1377">adv:L</ta>
            <ta e="T1379" id="Seg_11323" s="T1378">np:Com</ta>
            <ta e="T1380" id="Seg_11324" s="T1379">0.1.h:A</ta>
            <ta e="T1382" id="Seg_11325" s="T1381">pro.h:A</ta>
            <ta e="T1384" id="Seg_11326" s="T1383">pro:G</ta>
            <ta e="T1387" id="Seg_11327" s="T1386">0.3.h:A</ta>
            <ta e="T1388" id="Seg_11328" s="T1387">np:G</ta>
            <ta e="T1389" id="Seg_11329" s="T1388">adv:Time</ta>
            <ta e="T1390" id="Seg_11330" s="T1389">pro.h:Poss</ta>
            <ta e="T1391" id="Seg_11331" s="T1390">np.h:A</ta>
            <ta e="T1394" id="Seg_11332" s="T1393">pro.h:A</ta>
            <ta e="T1399" id="Seg_11333" s="T1398">pro.h:A</ta>
            <ta e="T1402" id="Seg_11334" s="T1401">pro.h:Th</ta>
            <ta e="T1403" id="Seg_11335" s="T1402">0.3.h:A</ta>
            <ta e="T1404" id="Seg_11336" s="T1403">np:G</ta>
            <ta e="T1405" id="Seg_11337" s="T1404">0.1.h:A</ta>
            <ta e="T1407" id="Seg_11338" s="T1406">adv:Time</ta>
            <ta e="T1408" id="Seg_11339" s="T1407">pro.h:A</ta>
            <ta e="T1413" id="Seg_11340" s="T1412">0.2.h:E</ta>
            <ta e="T1415" id="Seg_11341" s="T1414">adv:Time</ta>
            <ta e="T1416" id="Seg_11342" s="T1415">pro.h:B</ta>
            <ta e="T1417" id="Seg_11343" s="T1416">np:R</ta>
            <ta e="T1418" id="Seg_11344" s="T1417">0.3.h:A</ta>
            <ta e="T1420" id="Seg_11345" s="T1419">adv:Time</ta>
            <ta e="T1421" id="Seg_11346" s="T1420">0.3.h:A</ta>
            <ta e="T1423" id="Seg_11347" s="T1422">adv:Time</ta>
            <ta e="T1424" id="Seg_11348" s="T1423">0.3.h:A</ta>
            <ta e="T1425" id="Seg_11349" s="T1424">pro:G</ta>
            <ta e="T1426" id="Seg_11350" s="T1425">0.3.h:A</ta>
            <ta e="T1428" id="Seg_11351" s="T1427">0.3.h:A</ta>
            <ta e="T1429" id="Seg_11352" s="T1428">adv:Time</ta>
            <ta e="T1431" id="Seg_11353" s="T1430">0.3.h:A</ta>
            <ta e="T1432" id="Seg_11354" s="T1431">np:G</ta>
            <ta e="T1434" id="Seg_11355" s="T1433">adv:Time</ta>
            <ta e="T1435" id="Seg_11356" s="T1434">pro.h:A</ta>
            <ta e="T1437" id="Seg_11357" s="T1436">np:G</ta>
            <ta e="T1438" id="Seg_11358" s="T1437">np:P</ta>
            <ta e="T1442" id="Seg_11359" s="T1441">adv:L</ta>
            <ta e="T1443" id="Seg_11360" s="T1442">n:Time</ta>
            <ta e="T1449" id="Seg_11361" s="T1448">pro:L</ta>
            <ta e="T1450" id="Seg_11362" s="T1449">adv:Time</ta>
            <ta e="T1451" id="Seg_11363" s="T1450">0.3.h:A</ta>
            <ta e="T1452" id="Seg_11364" s="T1451">np:G</ta>
            <ta e="T1455" id="Seg_11365" s="T1454">adv:L</ta>
            <ta e="T1456" id="Seg_11366" s="T1455">0.3.h:A</ta>
            <ta e="T1457" id="Seg_11367" s="T1456">np:G</ta>
            <ta e="T1459" id="Seg_11368" s="T1458">np.h:A</ta>
            <ta e="T1461" id="Seg_11369" s="T1460">pro.h:A</ta>
            <ta e="T1463" id="Seg_11370" s="T1462">np:L</ta>
            <ta e="T1465" id="Seg_11371" s="T1464">adv:Time</ta>
            <ta e="T1466" id="Seg_11372" s="T1465">np:Th</ta>
            <ta e="T1468" id="Seg_11373" s="T1467">np.h:A</ta>
            <ta e="T1470" id="Seg_11374" s="T1469">pro.h:A</ta>
            <ta e="T1471" id="Seg_11375" s="T1470">np.h:Th</ta>
            <ta e="T1474" id="Seg_11376" s="T1473">pro.h:P</ta>
            <ta e="T1477" id="Seg_11377" s="T1476">np:L</ta>
            <ta e="T1500" id="Seg_11378" s="T1499">adv:Time</ta>
            <ta e="T1508" id="Seg_11379" s="T1506">adv:Time</ta>
            <ta e="T1587" id="Seg_11380" s="T1586">pro.h:A</ta>
            <ta e="T1588" id="Seg_11381" s="T1587">adv:Time</ta>
            <ta e="T1590" id="Seg_11382" s="T1589">np.h:Th</ta>
            <ta e="T1592" id="Seg_11383" s="T1591">pro.h:A</ta>
            <ta e="T1595" id="Seg_11384" s="T1594">0.1.h:A</ta>
            <ta e="T1596" id="Seg_11385" s="T1595">np.h:Th</ta>
            <ta e="T1598" id="Seg_11386" s="T1597">0.1.h:A</ta>
            <ta e="T1599" id="Seg_11387" s="T1598">pro.h:Poss</ta>
            <ta e="T1600" id="Seg_11388" s="T1599">np:G</ta>
            <ta e="T1602" id="Seg_11389" s="T1601">adv:Time</ta>
            <ta e="T1603" id="Seg_11390" s="T1602">pro.h:A</ta>
            <ta e="T1604" id="Seg_11391" s="T1603">pro:G</ta>
            <ta e="T1607" id="Seg_11392" s="T1606">0.1.h:A</ta>
            <ta e="T1608" id="Seg_11393" s="T1607">pro.h:Th</ta>
            <ta e="T1611" id="Seg_11394" s="T1610">pro.h:A</ta>
            <ta e="T1651" id="Seg_11395" s="T1650">pro.h:E</ta>
            <ta e="T1652" id="Seg_11396" s="T1651">adv:Time</ta>
            <ta e="T1656" id="Seg_11397" s="T1655">np:L</ta>
            <ta e="T1657" id="Seg_11398" s="T1656">0.1.h:E</ta>
            <ta e="T1659" id="Seg_11399" s="T1658">np:Th</ta>
            <ta e="T1661" id="Seg_11400" s="T1660">pro.h:A</ta>
            <ta e="T1662" id="Seg_11401" s="T1661">pro.h:R</ta>
            <ta e="T1665" id="Seg_11402" s="T1664">0.1.h:A</ta>
            <ta e="T1666" id="Seg_11403" s="T1665">pro:G</ta>
            <ta e="T1668" id="Seg_11404" s="T1667">pro.h:A</ta>
            <ta e="T1671" id="Seg_11405" s="T1670">0.1.h:A</ta>
            <ta e="T1698" id="Seg_11406" s="T1697">pro.h:E</ta>
            <ta e="T1700" id="Seg_11407" s="T1699">adv:Time</ta>
            <ta e="T1702" id="Seg_11408" s="T1701">np:L</ta>
            <ta e="T1703" id="Seg_11409" s="T1702">0.1.h:E</ta>
            <ta e="T1704" id="Seg_11410" s="T1703">pro.h:Poss</ta>
            <ta e="T1705" id="Seg_11411" s="T1704">np.h:Poss</ta>
            <ta e="T1706" id="Seg_11412" s="T1705">np.h:Th</ta>
            <ta e="T1709" id="Seg_11413" s="T1708">adv:Time</ta>
            <ta e="T1710" id="Seg_11414" s="T1709">0.1.h:A</ta>
            <ta e="T1711" id="Seg_11415" s="T1710">pro:G</ta>
            <ta e="T1712" id="Seg_11416" s="T1711">0.1.h:A</ta>
            <ta e="T1715" id="Seg_11417" s="T1714">adv:Time</ta>
            <ta e="T1716" id="Seg_11418" s="T1715">0.1.h:A</ta>
            <ta e="T1717" id="Seg_11419" s="T1716">pro.h:A</ta>
            <ta e="T1719" id="Seg_11420" s="T1718">pro.h:R</ta>
            <ta e="T1721" id="Seg_11421" s="T1720">adv:Time</ta>
            <ta e="T1725" id="Seg_11422" s="T1722">np.h:A</ta>
            <ta e="T1728" id="Seg_11423" s="T1727">pro.h:R</ta>
            <ta e="T1800" id="Seg_11424" s="T1799">pro.h:A</ta>
            <ta e="T1803" id="Seg_11425" s="T1802">np.h:Poss</ta>
            <ta e="T1805" id="Seg_11426" s="T1804">np:Th</ta>
            <ta e="T1808" id="Seg_11427" s="T1807">np.h:A 0.3.h:Poss</ta>
            <ta e="T1811" id="Seg_11428" s="T1810">pro.h:A</ta>
            <ta e="T1814" id="Seg_11429" s="T1813">np:Th</ta>
            <ta e="T1815" id="Seg_11430" s="T1814">pro.h:R</ta>
            <ta e="T1816" id="Seg_11431" s="T1815">0.1.h:A</ta>
            <ta e="T1818" id="Seg_11432" s="T1817">pro.h:A</ta>
            <ta e="T1822" id="Seg_11433" s="T1821">np:Th</ta>
            <ta e="T1823" id="Seg_11434" s="T1822">0.3.h:A</ta>
            <ta e="T1825" id="Seg_11435" s="T1824">np:Th</ta>
            <ta e="T1826" id="Seg_11436" s="T1825">0.3.h:A</ta>
            <ta e="T1827" id="Seg_11437" s="T1826">np:Th</ta>
            <ta e="T1829" id="Seg_11438" s="T1828">0.3.h:A</ta>
            <ta e="T1830" id="Seg_11439" s="T1829">0.2.h:A</ta>
            <ta e="T1831" id="Seg_11440" s="T1830">0.2.h:A</ta>
            <ta e="T1833" id="Seg_11441" s="T1832">pro.h:A</ta>
            <ta e="T1835" id="Seg_11442" s="T1834">np:P</ta>
            <ta e="T1837" id="Seg_11443" s="T1836">adv:Time</ta>
            <ta e="T1841" id="Seg_11444" s="T1840">np:P</ta>
            <ta e="T1842" id="Seg_11445" s="T1841">0.1.h:A</ta>
            <ta e="T1844" id="Seg_11446" s="T1843">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T380" id="Seg_11447" s="T379">np.h:S</ta>
            <ta e="T381" id="Seg_11448" s="T380">v:pred</ta>
            <ta e="T384" id="Seg_11449" s="T383">np.h:S</ta>
            <ta e="T385" id="Seg_11450" s="T384">v:pred</ta>
            <ta e="T397" id="Seg_11451" s="T396">np.h:S</ta>
            <ta e="T400" id="Seg_11452" s="T399">v:pred</ta>
            <ta e="T402" id="Seg_11453" s="T401">pro.h:S</ta>
            <ta e="T403" id="Seg_11454" s="T402">pro:O</ta>
            <ta e="T405" id="Seg_11455" s="T404">v:pred</ta>
            <ta e="T408" id="Seg_11456" s="T407">pro:S</ta>
            <ta e="T410" id="Seg_11457" s="T409">v:pred</ta>
            <ta e="T412" id="Seg_11458" s="T411">pro.h:S</ta>
            <ta e="T413" id="Seg_11459" s="T412">v:pred</ta>
            <ta e="T414" id="Seg_11460" s="T413">v:pred 0.2.h:S</ta>
            <ta e="T415" id="Seg_11461" s="T414">np.h:S</ta>
            <ta e="T416" id="Seg_11462" s="T415">v:pred</ta>
            <ta e="T418" id="Seg_11463" s="T417">pro.h:S</ta>
            <ta e="T420" id="Seg_11464" s="T418">v:pred</ta>
            <ta e="T422" id="Seg_11465" s="T421">v:pred 0.2.h:S</ta>
            <ta e="T424" id="Seg_11466" s="T423">v:pred</ta>
            <ta e="T426" id="Seg_11467" s="T425">pro.h:S</ta>
            <ta e="T427" id="Seg_11468" s="T426">v:pred</ta>
            <ta e="T428" id="Seg_11469" s="T427">pro.h:S</ta>
            <ta e="T430" id="Seg_11470" s="T429">v:pred</ta>
            <ta e="T433" id="Seg_11471" s="T432">pro.h:S</ta>
            <ta e="T434" id="Seg_11472" s="T433">v:pred</ta>
            <ta e="T436" id="Seg_11473" s="T435">pro.h:S</ta>
            <ta e="T437" id="Seg_11474" s="T436">v:pred</ta>
            <ta e="T439" id="Seg_11475" s="T438">pro.h:S</ta>
            <ta e="T440" id="Seg_11476" s="T439">adj:pred</ta>
            <ta e="T441" id="Seg_11477" s="T440">ptcl:pred</ta>
            <ta e="T444" id="Seg_11478" s="T443">pro.h:S</ta>
            <ta e="T446" id="Seg_11479" s="T445">ptcl.neg</ta>
            <ta e="T447" id="Seg_11480" s="T446">adj:pred</ta>
            <ta e="T452" id="Seg_11481" s="T451">v:pred 0.1.h:S</ta>
            <ta e="T455" id="Seg_11482" s="T454">s:temp</ta>
            <ta e="T457" id="Seg_11483" s="T456">pro.h:S</ta>
            <ta e="T458" id="Seg_11484" s="T457">v:pred</ta>
            <ta e="T460" id="Seg_11485" s="T459">pro.h:S</ta>
            <ta e="T461" id="Seg_11486" s="T460">v:pred</ta>
            <ta e="T462" id="Seg_11487" s="T461">np:O</ta>
            <ta e="T463" id="Seg_11488" s="T462">v:pred 0.1.h:S</ta>
            <ta e="T468" id="Seg_11489" s="T467">np:S</ta>
            <ta e="T469" id="Seg_11490" s="T468">v:pred</ta>
            <ta e="T471" id="Seg_11491" s="T470">v:pred 0.3:S</ta>
            <ta e="T474" id="Seg_11492" s="T473">pro.h:S</ta>
            <ta e="T477" id="Seg_11493" s="T476">ptcl.neg</ta>
            <ta e="T478" id="Seg_11494" s="T477">v:pred</ta>
            <ta e="T482" id="Seg_11495" s="T481">np.h:S</ta>
            <ta e="T485" id="Seg_11496" s="T484">v:pred</ta>
            <ta e="T486" id="Seg_11497" s="T485">pro.h:S</ta>
            <ta e="T487" id="Seg_11498" s="T486">v:pred</ta>
            <ta e="T575" id="Seg_11499" s="T574">np.h:S</ta>
            <ta e="T576" id="Seg_11500" s="T575">v:pred</ta>
            <ta e="T585" id="Seg_11501" s="T584">v:pred 0.3.h:S</ta>
            <ta e="T587" id="Seg_11502" s="T586">np:S</ta>
            <ta e="T588" id="Seg_11503" s="T587">v:pred</ta>
            <ta e="T595" id="Seg_11504" s="T594">v:pred 0.3:S</ta>
            <ta e="T599" id="Seg_11505" s="T598">pro.h:S</ta>
            <ta e="T600" id="Seg_11506" s="T599">v:pred</ta>
            <ta e="T603" id="Seg_11507" s="T602">v:pred 0.3.h:S</ta>
            <ta e="T605" id="Seg_11508" s="T604">pro.h:S</ta>
            <ta e="T607" id="Seg_11509" s="T606">v:pred</ta>
            <ta e="T608" id="Seg_11510" s="T607">pro.h:S</ta>
            <ta e="T610" id="Seg_11511" s="T609">v:pred</ta>
            <ta e="T615" id="Seg_11512" s="T614">0.3:S</ta>
            <ta e="T617" id="Seg_11513" s="T616">v:pred</ta>
            <ta e="T621" id="Seg_11514" s="T620">np.h:S</ta>
            <ta e="T622" id="Seg_11515" s="T621">v:pred</ta>
            <ta e="T626" id="Seg_11516" s="T625">v:pred 0.3.h:S</ta>
            <ta e="T1264" id="Seg_11517" s="T1263">pro.h:S</ta>
            <ta e="T1265" id="Seg_11518" s="T1264">v:pred</ta>
            <ta e="T1267" id="Seg_11519" s="T1266">v:pred 0.1.h:S</ta>
            <ta e="T1269" id="Seg_11520" s="T1268">np.h:S</ta>
            <ta e="T1271" id="Seg_11521" s="T1270">conv:pred</ta>
            <ta e="T1272" id="Seg_11522" s="T1271">v:pred</ta>
            <ta e="T1273" id="Seg_11523" s="T1272">pro.h:S</ta>
            <ta e="T1275" id="Seg_11524" s="T1274">v:pred</ta>
            <ta e="T1277" id="Seg_11525" s="T1276">pro.h:S</ta>
            <ta e="T1278" id="Seg_11526" s="T1277">v:pred</ta>
            <ta e="T1279" id="Seg_11527" s="T1278">v:pred 0.2.h:S</ta>
            <ta e="T1281" id="Seg_11528" s="T1280">v:pred 0.2.h:S</ta>
            <ta e="T1285" id="Seg_11529" s="T1284">pro.h:S</ta>
            <ta e="T1286" id="Seg_11530" s="T1285">v:pred</ta>
            <ta e="T1287" id="Seg_11531" s="T1286">pro.h:S</ta>
            <ta e="T1288" id="Seg_11532" s="T1287">ptcl.neg</ta>
            <ta e="T1289" id="Seg_11533" s="T1288">v:pred</ta>
            <ta e="T1290" id="Seg_11534" s="T1289">v:pred 0.1.h:S</ta>
            <ta e="T1293" id="Seg_11535" s="T1292">pro.h:S</ta>
            <ta e="T1294" id="Seg_11536" s="T1293">np:O</ta>
            <ta e="T1295" id="Seg_11537" s="T1294">v:pred</ta>
            <ta e="T1298" id="Seg_11538" s="T1297">np:O</ta>
            <ta e="T1299" id="Seg_11539" s="T1298">v:pred 0.1.h:S</ta>
            <ta e="T1300" id="Seg_11540" s="T1299">v:pred 0.1.h:S</ta>
            <ta e="T1301" id="Seg_11541" s="T1300">v:pred 0.1.h:S</ta>
            <ta e="T1308" id="Seg_11542" s="T1307">v:pred 0.1.h:S</ta>
            <ta e="T1310" id="Seg_11543" s="T1309">v:pred 0.1.h:S</ta>
            <ta e="T1320" id="Seg_11544" s="T1319">v:pred 0.1.h:S</ta>
            <ta e="T1323" id="Seg_11545" s="T1322">v:pred 0.1.h:S</ta>
            <ta e="T1331" id="Seg_11546" s="T1330">v:pred</ta>
            <ta e="T1334" id="Seg_11547" s="T1333">np.h:S</ta>
            <ta e="T1337" id="Seg_11548" s="T1336">v:pred 0.1.h:S</ta>
            <ta e="T1340" id="Seg_11549" s="T1339">v:pred 0.1.h:S</ta>
            <ta e="T1343" id="Seg_11550" s="T1342">np:O</ta>
            <ta e="T1344" id="Seg_11551" s="T1343">v:pred 0.1.h:S</ta>
            <ta e="T1350" id="Seg_11552" s="T1349">np.h:S</ta>
            <ta e="T1351" id="Seg_11553" s="T1350">v:pred</ta>
            <ta e="T1353" id="Seg_11554" s="T1352">v:pred 0.2.h:S</ta>
            <ta e="T1355" id="Seg_11555" s="T1354">np:O</ta>
            <ta e="T1356" id="Seg_11556" s="T1355">s:purp</ta>
            <ta e="T1359" id="Seg_11557" s="T1358">pro.h:S</ta>
            <ta e="T1360" id="Seg_11558" s="T1359">np.h:O</ta>
            <ta e="T1361" id="Seg_11559" s="T1360">v:pred</ta>
            <ta e="T1364" id="Seg_11560" s="T1363">v:pred 0.3.h:S</ta>
            <ta e="T1365" id="Seg_11561" s="T1364">pro.h:S</ta>
            <ta e="T1368" id="Seg_11562" s="T1367">v:pred</ta>
            <ta e="T1370" id="Seg_11563" s="T1369">pro.h:S</ta>
            <ta e="T1372" id="Seg_11564" s="T1371">np:O</ta>
            <ta e="T1373" id="Seg_11565" s="T1372">v:pred</ta>
            <ta e="T1380" id="Seg_11566" s="T1379">v:pred 0.1.h:S</ta>
            <ta e="T1382" id="Seg_11567" s="T1381">pro.h:S</ta>
            <ta e="T1383" id="Seg_11568" s="T1382">v:pred</ta>
            <ta e="T1387" id="Seg_11569" s="T1386">v:pred 0.3.h:S</ta>
            <ta e="T1391" id="Seg_11570" s="T1390">np.h:S</ta>
            <ta e="T1392" id="Seg_11571" s="T1391">v:pred</ta>
            <ta e="T1394" id="Seg_11572" s="T1393">pro.h:S</ta>
            <ta e="T1396" id="Seg_11573" s="T1395">conv:pred</ta>
            <ta e="T1397" id="Seg_11574" s="T1396">v:pred</ta>
            <ta e="T1399" id="Seg_11575" s="T1398">pro.h:S</ta>
            <ta e="T1400" id="Seg_11576" s="T1399">v:pred</ta>
            <ta e="T1403" id="Seg_11577" s="T1402">v:pred 0.3.h:S</ta>
            <ta e="T1405" id="Seg_11578" s="T1404">v:pred 0.1.h:S</ta>
            <ta e="T1406" id="Seg_11579" s="T1405">s:purp</ta>
            <ta e="T1408" id="Seg_11580" s="T1407">pro.h:S</ta>
            <ta e="T1410" id="Seg_11581" s="T1409">v:pred</ta>
            <ta e="T1413" id="Seg_11582" s="T1412">v:pred 0.2.h:S</ta>
            <ta e="T1416" id="Seg_11583" s="T1415">pro.h:O</ta>
            <ta e="T1418" id="Seg_11584" s="T1417">v:pred 0.3.h:S</ta>
            <ta e="T1421" id="Seg_11585" s="T1420">v:pred 0.3.h:S</ta>
            <ta e="T1424" id="Seg_11586" s="T1423">v:pred 0.3.h:S</ta>
            <ta e="T1426" id="Seg_11587" s="T1425">v:pred 0.3.h:S</ta>
            <ta e="T1428" id="Seg_11588" s="T1427">v:pred 0.3.h:S</ta>
            <ta e="T1430" id="Seg_11589" s="T1429">conv:pred</ta>
            <ta e="T1431" id="Seg_11590" s="T1430">v:pred 0.3.h:S</ta>
            <ta e="T1435" id="Seg_11591" s="T1434">pro.h:S</ta>
            <ta e="T1436" id="Seg_11592" s="T1435">v:pred</ta>
            <ta e="T1438" id="Seg_11593" s="T1437">np:O</ta>
            <ta e="T1439" id="Seg_11594" s="T1438">s:purp</ta>
            <ta e="T1443" id="Seg_11595" s="T1442">np:S</ta>
            <ta e="T1446" id="Seg_11596" s="T1445">v:pred</ta>
            <ta e="T1447" id="Seg_11597" s="T1446">pro.h:S</ta>
            <ta e="T1451" id="Seg_11598" s="T1450">v:pred 0.3.h:S</ta>
            <ta e="T1456" id="Seg_11599" s="T1455">v:pred 0.3.h:S</ta>
            <ta e="T1459" id="Seg_11600" s="T1458">np.h:S</ta>
            <ta e="T1461" id="Seg_11601" s="T1460">pro.h:S</ta>
            <ta e="T1466" id="Seg_11602" s="T1465">np:O</ta>
            <ta e="T1468" id="Seg_11603" s="T1467">np.h:S</ta>
            <ta e="T1469" id="Seg_11604" s="T1468">v:pred</ta>
            <ta e="T1470" id="Seg_11605" s="T1469">pro.h:S</ta>
            <ta e="T1471" id="Seg_11606" s="T1470">np.h:O</ta>
            <ta e="T1472" id="Seg_11607" s="T1471">v:pred</ta>
            <ta e="T1474" id="Seg_11608" s="T1473">pro.h:S</ta>
            <ta e="T1475" id="Seg_11609" s="T1474">v:pred</ta>
            <ta e="T1587" id="Seg_11610" s="T1586">pro.h:S</ta>
            <ta e="T1589" id="Seg_11611" s="T1588">v:pred</ta>
            <ta e="T1592" id="Seg_11612" s="T1591">pro.h:S</ta>
            <ta e="T1594" id="Seg_11613" s="T1593">v:pred</ta>
            <ta e="T1595" id="Seg_11614" s="T1594">v:pred 0.1.h:S</ta>
            <ta e="T1596" id="Seg_11615" s="T1595">np.h:O</ta>
            <ta e="T1598" id="Seg_11616" s="T1597">v:pred 0.1.h:S</ta>
            <ta e="T1603" id="Seg_11617" s="T1602">pro.h:S</ta>
            <ta e="T1605" id="Seg_11618" s="T1604">ptcl.neg</ta>
            <ta e="T1606" id="Seg_11619" s="T1605">v:pred</ta>
            <ta e="T1607" id="Seg_11620" s="T1606">v:pred 0.1.h:S</ta>
            <ta e="T1611" id="Seg_11621" s="T1610">pro.h:S</ta>
            <ta e="T1612" id="Seg_11622" s="T1611">v:pred</ta>
            <ta e="T1651" id="Seg_11623" s="T1650">pro.h:S</ta>
            <ta e="T1654" id="Seg_11624" s="T1653">v:pred</ta>
            <ta e="T1657" id="Seg_11625" s="T1656">v:pred 0.1.h:S</ta>
            <ta e="T1659" id="Seg_11626" s="T1658">np:O</ta>
            <ta e="T1661" id="Seg_11627" s="T1660">pro.h:S</ta>
            <ta e="T1664" id="Seg_11628" s="T1663">v:pred</ta>
            <ta e="T1665" id="Seg_11629" s="T1664">v:pred 0.1.h:S</ta>
            <ta e="T1668" id="Seg_11630" s="T1667">pro.h:S</ta>
            <ta e="T1669" id="Seg_11631" s="T1668">v:pred</ta>
            <ta e="T1670" id="Seg_11632" s="T1669">ptcl.neg</ta>
            <ta e="T1671" id="Seg_11633" s="T1670">v:pred 0.1.h:S</ta>
            <ta e="T1698" id="Seg_11634" s="T1697">pro.h:S</ta>
            <ta e="T1699" id="Seg_11635" s="T1698">v:pred</ta>
            <ta e="T1703" id="Seg_11636" s="T1702">v:pred 0.1.h:S</ta>
            <ta e="T1706" id="Seg_11637" s="T1705">np.h:O</ta>
            <ta e="T1710" id="Seg_11638" s="T1709">v:pred 0.1.h:S</ta>
            <ta e="T1712" id="Seg_11639" s="T1711">v:pred 0.1.h:S</ta>
            <ta e="T1716" id="Seg_11640" s="T1715">v:pred 0.1.h:S</ta>
            <ta e="T1717" id="Seg_11641" s="T1716">pro.h:S</ta>
            <ta e="T1718" id="Seg_11642" s="T1717">v:pred</ta>
            <ta e="T1725" id="Seg_11643" s="T1722">np.h:S</ta>
            <ta e="T1726" id="Seg_11644" s="T1725">v:pred</ta>
            <ta e="T1727" id="Seg_11645" s="T1726">s:purp</ta>
            <ta e="T1800" id="Seg_11646" s="T1799">pro.h:S</ta>
            <ta e="T1801" id="Seg_11647" s="T1800">v:pred</ta>
            <ta e="T1804" id="Seg_11648" s="T1803">v:pred</ta>
            <ta e="T1805" id="Seg_11649" s="T1804">np:S</ta>
            <ta e="T1808" id="Seg_11650" s="T1807">np.h:S</ta>
            <ta e="T1809" id="Seg_11651" s="T1808">v:pred</ta>
            <ta e="T1811" id="Seg_11652" s="T1810">pro.h:S</ta>
            <ta e="T1812" id="Seg_11653" s="T1811">v:pred</ta>
            <ta e="T1814" id="Seg_11654" s="T1813">np:O</ta>
            <ta e="T1816" id="Seg_11655" s="T1815">v:pred 0.1.h:S</ta>
            <ta e="T1818" id="Seg_11656" s="T1817">pro.h:S</ta>
            <ta e="T1821" id="Seg_11657" s="T1820">v:pred</ta>
            <ta e="T1822" id="Seg_11658" s="T1821">np:O</ta>
            <ta e="T1823" id="Seg_11659" s="T1822">v:pred 0.3.h:S</ta>
            <ta e="T1825" id="Seg_11660" s="T1824">np:O</ta>
            <ta e="T1826" id="Seg_11661" s="T1825">v:pred 0.3.h:S</ta>
            <ta e="T1827" id="Seg_11662" s="T1826">np:O</ta>
            <ta e="T1829" id="Seg_11663" s="T1828">v:pred 0.3.h:S</ta>
            <ta e="T1830" id="Seg_11664" s="T1829">v:pred 0.2.h:S</ta>
            <ta e="T1831" id="Seg_11665" s="T1830">v:pred 0.2.h:S</ta>
            <ta e="T1833" id="Seg_11666" s="T1832">pro.h:S</ta>
            <ta e="T1834" id="Seg_11667" s="T1833">v:pred</ta>
            <ta e="T1835" id="Seg_11668" s="T1834">np:O</ta>
            <ta e="T1841" id="Seg_11669" s="T1840">np:O</ta>
            <ta e="T1842" id="Seg_11670" s="T1841">v:pred 0.1.h:S</ta>
            <ta e="T1844" id="Seg_11671" s="T1843">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T380" id="Seg_11672" s="T379">RUS:cult</ta>
            <ta e="T383" id="Seg_11673" s="T382">RUS:disc</ta>
            <ta e="T397" id="Seg_11674" s="T396">RUS:core</ta>
            <ta e="T404" id="Seg_11675" s="T403">RUS:cult</ta>
            <ta e="T421" id="Seg_11676" s="T420">RUS:disc</ta>
            <ta e="T428" id="Seg_11677" s="T427">RUS:gram(INDEF)</ta>
            <ta e="T441" id="Seg_11678" s="T440">RUS:mod</ta>
            <ta e="T443" id="Seg_11679" s="T442">RUS:gram</ta>
            <ta e="T445" id="Seg_11680" s="T444">RUS:mod</ta>
            <ta e="T476" id="Seg_11681" s="T475">TURK:gram(INDEF)</ta>
            <ta e="T479" id="Seg_11682" s="T478">RUS:gram</ta>
            <ta e="T575" id="Seg_11683" s="T574">TURK:core</ta>
            <ta e="T578" id="Seg_11684" s="T577">TAT:cult</ta>
            <ta e="T580" id="Seg_11685" s="T579">TAT:cult</ta>
            <ta e="T581" id="Seg_11686" s="T580">RUS:cult</ta>
            <ta e="T608" id="Seg_11687" s="T607">TURK:gram(INDEF)</ta>
            <ta e="T612" id="Seg_11688" s="T611">RUS:gram</ta>
            <ta e="T616" id="Seg_11689" s="T615">RUS:gram</ta>
            <ta e="T624" id="Seg_11690" s="T623">RUS:gram</ta>
            <ta e="T1282" id="Seg_11691" s="T1281">TURK:core</ta>
            <ta e="T1283" id="Seg_11692" s="T1282">RUS:gram</ta>
            <ta e="T1303" id="Seg_11693" s="T1302">TAT:cult</ta>
            <ta e="T1305" id="Seg_11694" s="T1304">TURK:disc</ta>
            <ta e="T1311" id="Seg_11695" s="T1310">TURK:core</ta>
            <ta e="T1313" id="Seg_11696" s="T1312">TAT:cult</ta>
            <ta e="T1314" id="Seg_11697" s="T1313">RUS:cult</ta>
            <ta e="T1317" id="Seg_11698" s="T1316">RUS:core</ta>
            <ta e="T1333" id="Seg_11699" s="T1332">RUS:cult</ta>
            <ta e="T1346" id="Seg_11700" s="T1345">TURK:disc</ta>
            <ta e="T1350" id="Seg_11701" s="T1349">RUS:core</ta>
            <ta e="T1371" id="Seg_11702" s="T1370">TURK:disc</ta>
            <ta e="T1374" id="Seg_11703" s="T1373">RUS:gram</ta>
            <ta e="T1379" id="Seg_11704" s="T1378">RUS:core</ta>
            <ta e="T1386" id="Seg_11705" s="T1385">TURK:core</ta>
            <ta e="T1395" id="Seg_11706" s="T1394">TURK:core</ta>
            <ta e="T1401" id="Seg_11707" s="T1400">RUS:gram</ta>
            <ta e="T1412" id="Seg_11708" s="T1411">RUS:disc</ta>
            <ta e="T1417" id="Seg_11709" s="T1416">TURK:cult</ta>
            <ta e="T1437" id="Seg_11710" s="T1436">RUS:core</ta>
            <ta e="T1441" id="Seg_11711" s="T1440">RUS:gram</ta>
            <ta e="T1454" id="Seg_11712" s="T1453">RUS:gram</ta>
            <ta e="T1460" id="Seg_11713" s="T1459">RUS:gram</ta>
            <ta e="T1473" id="Seg_11714" s="T1472">RUS:gram</ta>
            <ta e="T1498" id="Seg_11715" s="T1495">RUS:disc</ta>
            <ta e="T1604" id="Seg_11716" s="T1603">TURK:gram(INDEF)</ta>
            <ta e="T1655" id="Seg_11717" s="T1654">RUS:gram</ta>
            <ta e="T1666" id="Seg_11718" s="T1665">RUS:gram(INDEF)</ta>
            <ta e="T1701" id="Seg_11719" s="T1700">RUS:gram</ta>
            <ta e="T1705" id="Seg_11720" s="T1704">RUS:core</ta>
            <ta e="T1712" id="Seg_11721" s="T1711">%TURK:core</ta>
            <ta e="T1717" id="Seg_11722" s="T1716">TURK:gram(INDEF)</ta>
            <ta e="T1718" id="Seg_11723" s="T1717">%TURK:core</ta>
            <ta e="T1725" id="Seg_11724" s="T1722">TURK:cult</ta>
            <ta e="T1727" id="Seg_11725" s="T1726">%TURK:core</ta>
            <ta e="T1814" id="Seg_11726" s="T1813">TAT:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T6" id="Seg_11727" s="T1">RUS:ext</ta>
            <ta e="T22" id="Seg_11728" s="T16">RUS:ext</ta>
            <ta e="T31" id="Seg_11729" s="T23">RUS:ext</ta>
            <ta e="T40" id="Seg_11730" s="T32">RUS:ext</ta>
            <ta e="T48" id="Seg_11731" s="T41">RUS:ext</ta>
            <ta e="T53" id="Seg_11732" s="T49">RUS:ext</ta>
            <ta e="T58" id="Seg_11733" s="T54">RUS:ext</ta>
            <ta e="T65" id="Seg_11734" s="T59">RUS:ext</ta>
            <ta e="T82" id="Seg_11735" s="T77">RUS:ext</ta>
            <ta e="T92" id="Seg_11736" s="T84">RUS:ext</ta>
            <ta e="T118" id="Seg_11737" s="T96">RUS:ext</ta>
            <ta e="T133" id="Seg_11738" s="T120">RUS:ext</ta>
            <ta e="T137" id="Seg_11739" s="T134">RUS:ext</ta>
            <ta e="T141" id="Seg_11740" s="T138">RUS:ext</ta>
            <ta e="T149" id="Seg_11741" s="T144">RUS:ext</ta>
            <ta e="T160" id="Seg_11742" s="T149">RUS:ext</ta>
            <ta e="T181" id="Seg_11743" s="T173">RUS:ext</ta>
            <ta e="T184" id="Seg_11744" s="T182">RUS:ext</ta>
            <ta e="T194" id="Seg_11745" s="T187">RUS:ext</ta>
            <ta e="T207" id="Seg_11746" s="T195">RUS:ext</ta>
            <ta e="T224" id="Seg_11747" s="T208">RUS:ext</ta>
            <ta e="T227" id="Seg_11748" s="T225">RUS:ext</ta>
            <ta e="T231" id="Seg_11749" s="T228">RUS:ext</ta>
            <ta e="T238" id="Seg_11750" s="T232">RUS:ext</ta>
            <ta e="T242" id="Seg_11751" s="T240">RUS:ext</ta>
            <ta e="T243" id="Seg_11752" s="T242">RUS:ext</ta>
            <ta e="T249" id="Seg_11753" s="T244">RUS:ext</ta>
            <ta e="T254" id="Seg_11754" s="T252">RUS:ext</ta>
            <ta e="T258" id="Seg_11755" s="T255">RUS:ext</ta>
            <ta e="T261" id="Seg_11756" s="T258">RUS:ext</ta>
            <ta e="T268" id="Seg_11757" s="T266">RUS:ext</ta>
            <ta e="T280" id="Seg_11758" s="T275">RUS:ext</ta>
            <ta e="T300" id="Seg_11759" s="T280">RUS:ext</ta>
            <ta e="T314" id="Seg_11760" s="T306">RUS:ext</ta>
            <ta e="T336" id="Seg_11761" s="T322">RUS:ext</ta>
            <ta e="T342" id="Seg_11762" s="T337">RUS:ext</ta>
            <ta e="T346" id="Seg_11763" s="T342">RUS:ext</ta>
            <ta e="T349" id="Seg_11764" s="T347">RUS:ext</ta>
            <ta e="T352" id="Seg_11765" s="T349">RUS:ext</ta>
            <ta e="T358" id="Seg_11766" s="T353">RUS:ext</ta>
            <ta e="T363" id="Seg_11767" s="T359">RUS:ext</ta>
            <ta e="T368" id="Seg_11768" s="T363">RUS:ext</ta>
            <ta e="T375" id="Seg_11769" s="T370">RUS:ext</ta>
            <ta e="T378" id="Seg_11770" s="T375">RUS:int.ins</ta>
            <ta e="T391" id="Seg_11771" s="T386">RUS:ext</ta>
            <ta e="T493" id="Seg_11772" s="T488">RUS:ext</ta>
            <ta e="T506" id="Seg_11773" s="T494">RUS:ext</ta>
            <ta e="T511" id="Seg_11774" s="T507">RUS:ext</ta>
            <ta e="T528" id="Seg_11775" s="T512">RUS:ext</ta>
            <ta e="T540" id="Seg_11776" s="T528">RUS:ext</ta>
            <ta e="T550" id="Seg_11777" s="T541">RUS:ext</ta>
            <ta e="T558" id="Seg_11778" s="T551">RUS:ext</ta>
            <ta e="T569" id="Seg_11779" s="T559">RUS:ext</ta>
            <ta e="T671" id="Seg_11780" s="T665">RUS:ext</ta>
            <ta e="T683" id="Seg_11781" s="T672">RUS:ext</ta>
            <ta e="T723" id="Seg_11782" s="T719">RUS:ext</ta>
            <ta e="T729" id="Seg_11783" s="T724">RUS:ext</ta>
            <ta e="T756" id="Seg_11784" s="T743">RUS:ext</ta>
            <ta e="T769" id="Seg_11785" s="T757">RUS:ext</ta>
            <ta e="T786" id="Seg_11786" s="T770">RUS:ext</ta>
            <ta e="T791" id="Seg_11787" s="T787">RUS:ext</ta>
            <ta e="T797" id="Seg_11788" s="T792">RUS:ext</ta>
            <ta e="T802" id="Seg_11789" s="T798">RUS:ext</ta>
            <ta e="T809" id="Seg_11790" s="T802">RUS:ext</ta>
            <ta e="T819" id="Seg_11791" s="T810">RUS:ext</ta>
            <ta e="T831" id="Seg_11792" s="T820">RUS:ext</ta>
            <ta e="T841" id="Seg_11793" s="T832">RUS:ext</ta>
            <ta e="T845" id="Seg_11794" s="T842">RUS:ext</ta>
            <ta e="T857" id="Seg_11795" s="T846">RUS:ext</ta>
            <ta e="T868" id="Seg_11796" s="T858">RUS:ext</ta>
            <ta e="T875" id="Seg_11797" s="T869">RUS:ext</ta>
            <ta e="T878" id="Seg_11798" s="T875">RUS:ext</ta>
            <ta e="T895" id="Seg_11799" s="T879">RUS:ext</ta>
            <ta e="T902" id="Seg_11800" s="T896">RUS:ext</ta>
            <ta e="T912" id="Seg_11801" s="T903">RUS:ext</ta>
            <ta e="T918" id="Seg_11802" s="T913">RUS:ext</ta>
            <ta e="T926" id="Seg_11803" s="T919">RUS:ext</ta>
            <ta e="T937" id="Seg_11804" s="T927">RUS:ext</ta>
            <ta e="T948" id="Seg_11805" s="T937">RUS:ext</ta>
            <ta e="T955" id="Seg_11806" s="T948">RUS:ext</ta>
            <ta e="T961" id="Seg_11807" s="T956">RUS:ext</ta>
            <ta e="T964" id="Seg_11808" s="T962">RUS:ext</ta>
            <ta e="T973" id="Seg_11809" s="T965">RUS:ext</ta>
            <ta e="T980" id="Seg_11810" s="T974">RUS:ext</ta>
            <ta e="T989" id="Seg_11811" s="T981">RUS:ext</ta>
            <ta e="T1008" id="Seg_11812" s="T990">RUS:ext</ta>
            <ta e="T1013" id="Seg_11813" s="T1009">RUS:ext</ta>
            <ta e="T1025" id="Seg_11814" s="T1014">RUS:ext</ta>
            <ta e="T1034" id="Seg_11815" s="T1026">RUS:ext</ta>
            <ta e="T1045" id="Seg_11816" s="T1035">RUS:ext</ta>
            <ta e="T1055" id="Seg_11817" s="T1046">RUS:ext</ta>
            <ta e="T1074" id="Seg_11818" s="T1056">RUS:ext</ta>
            <ta e="T1081" id="Seg_11819" s="T1075">RUS:ext</ta>
            <ta e="T1103" id="Seg_11820" s="T1090">RUS:ext</ta>
            <ta e="T1119" id="Seg_11821" s="T1104">RUS:ext</ta>
            <ta e="T1123" id="Seg_11822" s="T1120">RUS:ext</ta>
            <ta e="T1134" id="Seg_11823" s="T1124">RUS:ext</ta>
            <ta e="T1141" id="Seg_11824" s="T1135">RUS:ext</ta>
            <ta e="T1146" id="Seg_11825" s="T1142">RUS:ext</ta>
            <ta e="T1157" id="Seg_11826" s="T1147">RUS:ext</ta>
            <ta e="T1161" id="Seg_11827" s="T1157">RUS:ext</ta>
            <ta e="T1173" id="Seg_11828" s="T1162">RUS:ext</ta>
            <ta e="T1178" id="Seg_11829" s="T1174">RUS:ext</ta>
            <ta e="T1185" id="Seg_11830" s="T1179">RUS:ext</ta>
            <ta e="T1195" id="Seg_11831" s="T1186">RUS:ext</ta>
            <ta e="T1209" id="Seg_11832" s="T1196">RUS:ext</ta>
            <ta e="T1217" id="Seg_11833" s="T1210">RUS:ext</ta>
            <ta e="T1229" id="Seg_11834" s="T1217">RUS:ext</ta>
            <ta e="T1242" id="Seg_11835" s="T1235">RUS:ext</ta>
            <ta e="T1248" id="Seg_11836" s="T1242">RUS:ext</ta>
            <ta e="T1250" id="Seg_11837" s="T1249">RUS:ext</ta>
            <ta e="T1488" id="Seg_11838" s="T1484">RUS:ext</ta>
            <ta e="T1492" id="Seg_11839" s="T1488">RUS:ext</ta>
            <ta e="T1514" id="Seg_11840" s="T1513">RUS:ext</ta>
            <ta e="T1521" id="Seg_11841" s="T1518">RUS:ext</ta>
            <ta e="T1538" id="Seg_11842" s="T1529">RUS:ext</ta>
            <ta e="T1543" id="Seg_11843" s="T1539">RUS:ext</ta>
            <ta e="T1548" id="Seg_11844" s="T1544">RUS:ext</ta>
            <ta e="T1552" id="Seg_11845" s="T1549">RUS:ext</ta>
            <ta e="T1563" id="Seg_11846" s="T1562">RUS:ext</ta>
            <ta e="T1568" id="Seg_11847" s="T1564">RUS:ext</ta>
            <ta e="T1574" id="Seg_11848" s="T1571">RUS:ext</ta>
            <ta e="T1579" id="Seg_11849" s="T1576">RUS:ext</ta>
            <ta e="T1627" id="Seg_11850" s="T1619">RUS:ext</ta>
            <ta e="T1643" id="Seg_11851" s="T1628">RUS:ext</ta>
            <ta e="T1646" id="Seg_11852" s="T1644">RUS:ext</ta>
            <ta e="T1649" id="Seg_11853" s="T1647">RUS:ext</ta>
            <ta e="T1685" id="Seg_11854" s="T1672">RUS:ext</ta>
            <ta e="T1691" id="Seg_11855" s="T1686">RUS:ext</ta>
            <ta e="T1694" id="Seg_11856" s="T1692">RUS:ext</ta>
            <ta e="T1738" id="Seg_11857" s="T1732">RUS:ext</ta>
            <ta e="T1742" id="Seg_11858" s="T1739">RUS:ext</ta>
            <ta e="T1750" id="Seg_11859" s="T1743">RUS:ext</ta>
            <ta e="T1763" id="Seg_11860" s="T1751">RUS:ext</ta>
            <ta e="T1770" id="Seg_11861" s="T1764">RUS:ext</ta>
            <ta e="T1860" id="Seg_11862" s="T1850">RUS:ext</ta>
            <ta e="T1864" id="Seg_11863" s="T1861">RUS:ext</ta>
            <ta e="T1866" id="Seg_11864" s="T1865">RUS:ext</ta>
            <ta e="T1873" id="Seg_11865" s="T1867">RUS:ext</ta>
            <ta e="T1877" id="Seg_11866" s="T1874">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T385" id="Seg_11867" s="T375">А по-нашему: "Один жид пришёл, один татарин пришёл".</ta>
            <ta e="T394" id="Seg_11868" s="T393">Татары.</ta>
            <ta e="T400" id="Seg_11869" s="T395">Потом племянница ко мне пошла траву [косить].</ta>
            <ta e="T405" id="Seg_11870" s="T401">Мы копны делали.</ta>
            <ta e="T410" id="Seg_11871" s="T406">Одна упала.</ta>
            <ta e="T413" id="Seg_11872" s="T411">Я сказала…</ta>
            <ta e="T416" id="Seg_11873" s="T413">Нас трое было.</ta>
            <ta e="T420" id="Seg_11874" s="T417">Я сказала:</ta>
            <ta e="T424" id="Seg_11875" s="T420">"Смотри, (?) упала".</ta>
            <ta e="T430" id="Seg_11876" s="T425">Я сказала: "Кто-нибудь умрет.</ta>
            <ta e="T437" id="Seg_11877" s="T431">Тогда я говорю: "Это я умру.</ta>
            <ta e="T441" id="Seg_11878" s="T438">Я ведь старая.</ta>
            <ta e="T447" id="Seg_11879" s="T442">А вы еще не старые.</ta>
            <ta e="T452" id="Seg_11880" s="T448">Потом мы пришли домой.</ta>
            <ta e="T463" id="Seg_11881" s="T453">Когда она туда пришла, я пошла сорвать лук.</ta>
            <ta e="T471" id="Seg_11882" s="T464">Лиственница… ветра не было, она упала на землю.</ta>
            <ta e="T487" id="Seg_11883" s="T472">Я никому не сказала, а потом, когда ее муж утонул, я сказала.</ta>
            <ta e="T582" id="Seg_11884" s="T257">Когда мой отец пришел в ту деревню, в Спасское, к нам домой.</ta>
            <ta e="T588" id="Seg_11885" s="T583">Он идет, лиственница падает.</ta>
            <ta e="T595" id="Seg_11886" s="T589">Потом с дерева сучок упал на землю.</ta>
            <ta e="T603" id="Seg_11887" s="T596">Потом он пришел домой, и мне рассказывает.</ta>
            <ta e="T610" id="Seg_11888" s="T604">Я сказала: кто-то из детей умрет.</ta>
            <ta e="T617" id="Seg_11889" s="T611">И потом так и было.</ta>
            <ta e="T626" id="Seg_11890" s="T618">Мой сын пошел на реку и утонул.</ta>
            <ta e="T1275" id="Seg_11891" s="T1262">Когда он ко мне пришел: "Поедем, мать вышла замуж, я один живу".</ta>
            <ta e="T1284" id="Seg_11892" s="T1276">Я сказала: "Иди посватай меня к родителям".</ta>
            <ta e="T1290" id="Seg_11893" s="T1284">Он говорит: "Они не дадут, поедем".</ta>
            <ta e="T1295" id="Seg_11894" s="T1291">Тогда я одежду собрала.</ta>
            <ta e="T1303" id="Seg_11895" s="T1296">Ночью мы взяли лошадь, сели и поехали в Агинское.</ta>
            <ta e="T1314" id="Seg_11896" s="T1304">Целую ночь там спали, потом приехали опять в ту деревню, в Каптырлык.</ta>
            <ta e="T1326" id="Seg_11897" s="T1315">Там мы были целый день, потом ночью приехали к нему домой.</ta>
            <ta e="T1334" id="Seg_11898" s="T1327">Потом утром пришла его крестная.</ta>
            <ta e="T1337" id="Seg_11899" s="T1335">Потом мы встали.</ta>
            <ta e="T1340" id="Seg_11900" s="T1338">Я там жила.</ta>
            <ta e="T1346" id="Seg_11901" s="T1341">Мы косили там траву.</ta>
            <ta e="T1351" id="Seg_11902" s="T1347">Потом пришел его дядя.</ta>
            <ta e="T1356" id="Seg_11903" s="T1352">"Приходи ко мне косить траву".</ta>
            <ta e="T1361" id="Seg_11904" s="T1357">[А] он привез девушку.</ta>
            <ta e="T1368" id="Seg_11905" s="T1362">Мне сказали, что он будет с ней жить.</ta>
            <ta e="T1380" id="Seg_11906" s="T1369">Я взяла одежду и уехала с его дядей.</ta>
            <ta e="T1384" id="Seg_11907" s="T1380">Он догнал меня.</ta>
            <ta e="T1388" id="Seg_11908" s="T1385">Опять привез домой.</ta>
            <ta e="T1392" id="Seg_11909" s="T1388">Потом моя мать приехала.</ta>
            <ta e="T1397" id="Seg_11910" s="T1393">Я опять ушла.</ta>
            <ta e="T1404" id="Seg_11911" s="T1398">Он пришел и меня увел домой.</ta>
            <ta e="T1410" id="Seg_11912" s="T1404">Мы пошли спать, потом они пришли.</ta>
            <ta e="T1413" id="Seg_11913" s="T1411">"Ну, живите".</ta>
            <ta e="T1418" id="Seg_11914" s="T1414">Потом они нас благословили.</ta>
            <ta e="T1421" id="Seg_11915" s="T1419">Потом уехали.</ta>
            <ta e="T1426" id="Seg_11916" s="T1422">На следующий день встали, пришли к нам.</ta>
            <ta e="T1432" id="Seg_11917" s="T1427">Поели, потом [она] уехала домой.</ta>
            <ta e="T1439" id="Seg_11918" s="T1433">Потом он пошел к дяде траву косить.</ta>
            <ta e="T1449" id="Seg_11919" s="T1440">И там… был такой день, он (обливался?).</ta>
            <ta e="T1452" id="Seg_11920" s="T1449">Потом пошли к реке.</ta>
            <ta e="T1461" id="Seg_11921" s="T1453">И там зашли в воду, одна девушка и он.</ta>
            <ta e="T1463" id="Seg_11922" s="T1462">[стали тонуть.]</ta>
            <ta e="T1477" id="Seg_11923" s="T1464">Один парень бросил им палку, эту девушку вытащил, а он утонул.</ta>
            <ta e="T1508" id="Seg_11924" s="T1495">Ну, завтра…</ta>
            <ta e="T1590" id="Seg_11925" s="T1586">Завтра я буду ждать Арпада.</ta>
            <ta e="T1600" id="Seg_11926" s="T1591">Он сказал: "Я приду, возьму тебя, и мы поедем к моей матери".</ta>
            <ta e="T1608" id="Seg_11927" s="T1601">Так что я никуда не пойду, буду ждать его.</ta>
            <ta e="T1612" id="Seg_11928" s="T1609">Когда он придет.</ta>
            <ta e="T1659" id="Seg_11929" s="T1650">Я сегодня спала и во сне видела одного человека.</ta>
            <ta e="T1666" id="Seg_11930" s="T1660">Он мне сказал: "Пойдем куда-нибудь".</ta>
            <ta e="T1671" id="Seg_11931" s="T1667">Я говорю: "Не пойду".</ta>
            <ta e="T1706" id="Seg_11932" s="T1697">Вчера я спала и видела во сне мужа моей сестры.</ta>
            <ta e="T1712" id="Seg_11933" s="T1707">Потом я пошла к вам, разговаривала.</ta>
            <ta e="T1719" id="Seg_11934" s="T1713">Потом я сказала: кто-то будет говорить со мной.</ta>
            <ta e="T1728" id="Seg_11935" s="T1720">Потом начальник[и] пришли поговорить со мной.</ta>
            <ta e="T1731" id="Seg_11936" s="T1729">Потом там…</ta>
            <ta e="T1801" id="Seg_11937" s="T1799">Мы пошли.</ta>
            <ta e="T1809" id="Seg_11938" s="T1801">Это был у той женщины день, когда ее мать родила.</ta>
            <ta e="T1816" id="Seg_11939" s="T1810">Я пошла, дала ей три рубля.</ta>
            <ta e="T1823" id="Seg_11940" s="T1817">Она нас нагнала, дала нам яйца.</ta>
            <ta e="T1827" id="Seg_11941" s="T1824">Масло положила, лук.</ta>
            <ta e="T1831" id="Seg_11942" s="T1828">Говорит: "Ешь, ешь".</ta>
            <ta e="T1835" id="Seg_11943" s="T1832">Я немножко поела.</ta>
            <ta e="T1842" id="Seg_11944" s="T1836">Потом выпила одну чашку чая.</ta>
            <ta e="T1845" id="Seg_11945" s="T1843">Сказала: "Хватит".</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T6" id="Seg_11946" s="T1">I forgot something.</ta>
            <ta e="T22" id="Seg_11947" s="T16">My niece's daughter married.</ta>
            <ta e="T31" id="Seg_11948" s="T23">And they went to Mana, to build train tracks.</ta>
            <ta e="T40" id="Seg_11949" s="T32">And there everyone built a (?) in the north.</ta>
            <ta e="T48" id="Seg_11950" s="T41">And then they came to their mother, to visit her.</ta>
            <ta e="T53" id="Seg_11951" s="T49">She buried him.</ta>
            <ta e="T58" id="Seg_11952" s="T54">Well, that is...</ta>
            <ta e="T65" id="Seg_11953" s="T59">I say it in russian, I forget it again.</ta>
            <ta e="T82" id="Seg_11954" s="T77">At your [place], there is a kind of wind.</ta>
            <ta e="T92" id="Seg_11955" s="T84">At our [place] there is a dry wind.</ta>
            <ta e="T118" id="Seg_11956" s="T96">Yes, at your [place] there is much wind, no at our place, not like at your place.</ta>
            <ta e="T133" id="Seg_11957" s="T120">I only saw it from afar. </ta>
            <ta e="T137" id="Seg_11958" s="T134">There I have been.</ta>
            <ta e="T141" id="Seg_11959" s="T138">Where the fat Margaret is.</ta>
            <ta e="T149" id="Seg_11960" s="T144">Therefore I went there.</ta>
            <ta e="T160" id="Seg_11961" s="T149">Well, to see this far, but didn't went close.</ta>
            <ta e="T181" id="Seg_11962" s="T173">But going outside, no, I don't know.</ta>
            <ta e="T184" id="Seg_11963" s="T182">Speak russian?</ta>
            <ta e="T194" id="Seg_11964" s="T187">My niece's daughter married.</ta>
            <ta e="T207" id="Seg_11965" s="T195">And I said, and you like (?)</ta>
            <ta e="T224" id="Seg_11966" s="T208">I, says Teacher Agafon Iwanowitsch, (...) he says to me, well, in russian...</ta>
            <ta e="T227" id="Seg_11967" s="T225">Yes.</ta>
            <ta e="T231" id="Seg_11968" s="T228">Well I (...).</ta>
            <ta e="T238" id="Seg_11969" s="T232">Then we sat there and talked (...).</ta>
            <ta e="T242" id="Seg_11970" s="T240">Well.</ta>
            <ta e="T243" id="Seg_11971" s="T242">There.</ta>
            <ta e="T249" id="Seg_11972" s="T244">A brother of mine went there.</ta>
            <ta e="T254" id="Seg_11973" s="T252">Well, that is ...</ta>
            <ta e="T258" id="Seg_11974" s="T255">Boy (...).</ta>
            <ta e="T261" id="Seg_11975" s="T258">(in no case) I will not say it.</ta>
            <ta e="T268" id="Seg_11976" s="T266">Where is Mother?</ta>
            <ta e="T280" id="Seg_11977" s="T275">She went to Sargalov.</ta>
            <ta e="T300" id="Seg_11978" s="T280">That language, which they speak there, I speak it badly.</ta>
            <ta e="T314" id="Seg_11979" s="T306">And on his...</ta>
            <ta e="T336" id="Seg_11980" s="T322">Well, in Ufa there are Tatars.</ta>
            <ta e="T342" id="Seg_11981" s="T337">In Kazan they speak this language.</ta>
            <ta e="T346" id="Seg_11982" s="T342">Well, once came here.</ta>
            <ta e="T349" id="Seg_11983" s="T347">Three came.</ta>
            <ta e="T352" id="Seg_11984" s="T349">Who came there?</ta>
            <ta e="T358" id="Seg_11985" s="T353">He says: One jude, one tartar.</ta>
            <ta e="T363" id="Seg_11986" s="T359">One jude, one tartar.</ta>
            <ta e="T368" id="Seg_11987" s="T363">(...) in this language.</ta>
            <ta e="T375" id="Seg_11988" s="T370">In this language.</ta>
            <ta e="T385" id="Seg_11989" s="T375">In our language: "One Jew came and one Tatar came".</ta>
            <ta e="T391" id="Seg_11990" s="T386">One Jude, one Tartar.</ta>
            <ta e="T394" id="Seg_11991" s="T393">Tatars</ta>
            <ta e="T400" id="Seg_11992" s="T395">Then my niece went to me [to mow] grass.</ta>
            <ta e="T405" id="Seg_11993" s="T401">We out [it] in stacks. </ta>
            <ta e="T410" id="Seg_11994" s="T406">One [stack] fell.</ta>
            <ta e="T413" id="Seg_11995" s="T411">I said…</ta>
            <ta e="T416" id="Seg_11996" s="T413">There were three of us.</ta>
            <ta e="T420" id="Seg_11997" s="T417">I said:</ta>
            <ta e="T424" id="Seg_11998" s="T420">"Look, (?) has fallen."</ta>
            <ta e="T430" id="Seg_11999" s="T425">I said: "Somebody will die."</ta>
            <ta e="T437" id="Seg_12000" s="T431">Then I say: "I will die.</ta>
            <ta e="T441" id="Seg_12001" s="T438">I am old.</ta>
            <ta e="T447" id="Seg_12002" s="T442">And you aren't old yet.</ta>
            <ta e="T452" id="Seg_12003" s="T448">Then we came home.</ta>
            <ta e="T463" id="Seg_12004" s="T453">When she came there, I went to pick bear leek.</ta>
            <ta e="T471" id="Seg_12005" s="T464">A larch tree… there was no wind, it fell down.</ta>
            <ta e="T487" id="Seg_12006" s="T472">I din't tell anyone, and later, when her husband drowned, I told.</ta>
            <ta e="T493" id="Seg_12007" s="T488">When her husband drowned, I ...</ta>
            <ta e="T506" id="Seg_12008" s="T494">She worked at my [place] at mowing, she was great at making haystacks, that's what I said.</ta>
            <ta e="T511" id="Seg_12009" s="T507">She stood and fell.</ta>
            <ta e="T528" id="Seg_12010" s="T512">We were three of us and I said: 'One of us will die.'</ta>
            <ta e="T540" id="Seg_12011" s="T528">And I think, tell them: 'I will die, I am older than you.'</ta>
            <ta e="T550" id="Seg_12012" s="T541">And then I went to the field, picking Onion, on the field.</ta>
            <ta e="T558" id="Seg_12013" s="T551">There was no wind, the leaf fell.</ta>
            <ta e="T569" id="Seg_12014" s="T559">And I thought: 'Someine will die.'</ta>
            <ta e="T582" id="Seg_12015" s="T257">When my father came to that village, to Spasskoe, to our place.</ta>
            <ta e="T588" id="Seg_12016" s="T583">He's coming, a larch tree falls.</ta>
            <ta e="T595" id="Seg_12017" s="T589">Then a branch fell from the tree to the ground.</ta>
            <ta e="T603" id="Seg_12018" s="T596">Then he came home and tells me.</ta>
            <ta e="T610" id="Seg_12019" s="T604">I said: one of the children will die.</ta>
            <ta e="T617" id="Seg_12020" s="T611">And it was so.</ta>
            <ta e="T626" id="Seg_12021" s="T618">My son went to the river and died there.</ta>
            <ta e="T671" id="Seg_12022" s="T665">I forgot it yesterday, I spoke about it.</ta>
            <ta e="T683" id="Seg_12023" s="T672">About me, I married, I forgot, if I spoke about it or not?</ta>
            <ta e="T723" id="Seg_12024" s="T719">How my husband drowned.</ta>
            <ta e="T729" id="Seg_12025" s="T724">Well, he says that, no?</ta>
            <ta e="T756" id="Seg_12026" s="T743">Well, when I was a girl, there was the village Pjankowo, there was a man, his name was Alexej.</ta>
            <ta e="T769" id="Seg_12027" s="T757">We were friends with him, when I was there, he was here.</ta>
            <ta e="T786" id="Seg_12028" s="T770">And then his (mother =) father died, and his mother married, she left him and he was left alone.</ta>
            <ta e="T791" id="Seg_12029" s="T787">He was seventeen year old.</ta>
            <ta e="T797" id="Seg_12030" s="T792">And I was seventenn, I was young!</ta>
            <ta e="T802" id="Seg_12031" s="T798">Well, and he came.</ta>
            <ta e="T809" id="Seg_12032" s="T802">I said him: 'Go to my parents and ask for my hand.</ta>
            <ta e="T819" id="Seg_12033" s="T810">He says: 'No, they will not let you go free, they will say: you are young, let us go.</ta>
            <ta e="T831" id="Seg_12034" s="T820">And I gathered my clothes, tied them in a bundle, mounted on a horse at night and went away.</ta>
            <ta e="T841" id="Seg_12035" s="T832">We didn't come here on the direct way, but came through Aginsko.</ta>
            <ta e="T845" id="Seg_12036" s="T842">We rode, rode at night, oh.</ta>
            <ta e="T857" id="Seg_12037" s="T846">Then we arrived, there the village Kaptyrlyk, the russian people, spent the night there.</ta>
            <ta e="T868" id="Seg_12038" s="T858">And there we stayed the wohle day, he didn't went home.</ta>
            <ta e="T875" id="Seg_12039" s="T869">Whether due to the conscience, I don't know.</ta>
            <ta e="T878" id="Seg_12040" s="T875">Then we arrived in the evening.</ta>
            <ta e="T895" id="Seg_12041" s="T879">And he saddled the horse, we went into the house and then slept, and there I lived, that is all.</ta>
            <ta e="T902" id="Seg_12042" s="T896">And then came the (?) relative.</ta>
            <ta e="T912" id="Seg_12043" s="T903">He lost his eyes, didn't see his uncle.</ta>
            <ta e="T918" id="Seg_12044" s="T913">He said: Come, help cutting the hay.</ta>
            <ta e="T926" id="Seg_12045" s="T919">He is in our village, in Abalakov.</ta>
            <ta e="T937" id="Seg_12046" s="T927">Then he brought the girl from another village there.</ta>
            <ta e="T948" id="Seg_12047" s="T937">I was told, that he wants to take her with him and drive you out.</ta>
            <ta e="T955" id="Seg_12048" s="T948">I packed and went with the (?) uncle.</ta>
            <ta e="T961" id="Seg_12049" s="T956">He cought up to me and took me back.</ta>
            <ta e="T964" id="Seg_12050" s="T962">Back.</ta>
            <ta e="T973" id="Seg_12051" s="T965">Well, then my mother came here.</ta>
            <ta e="T980" id="Seg_12052" s="T974">And I went to mum.</ta>
            <ta e="T989" id="Seg_12053" s="T981">And mum came to her nephew, who now....</ta>
            <ta e="T1008" id="Seg_12054" s="T990">To her it was her nephew, his same was Ivan, who is no living in Pjankowo, she is my niece.</ta>
            <ta e="T1013" id="Seg_12055" s="T1009">Well, mum is...</ta>
            <ta e="T1025" id="Seg_12056" s="T1014">She came, and he came back, took my hand and led me away.</ta>
            <ta e="T1034" id="Seg_12057" s="T1026">Well, than we laid down and then they came.</ta>
            <ta e="T1045" id="Seg_12058" s="T1035">'If you live like that', says mother, 'I will bless you.'</ta>
            <ta e="T1055" id="Seg_12059" s="T1046">Here she blessed us (tomorrow), too.</ta>
            <ta e="T1074" id="Seg_12060" s="T1056">They came to us, this brother-in-law, this sister-in-law, we drank tea here, they then went away, and my mother went away.</ta>
            <ta e="T1081" id="Seg_12061" s="T1075">Well, now I can say it in kamas.</ta>
            <ta e="T1103" id="Seg_12062" s="T1090">Well, then we still have to say, that he went there.</ta>
            <ta e="T1119" id="Seg_12063" s="T1104">Then he left me at home, didn't take me with him, went to us to Abalakovo.</ta>
            <ta e="T1123" id="Seg_12064" s="T1120">The uncle mowed the hay.</ta>
            <ta e="T1134" id="Seg_12065" s="T1124">And it was such a day, a (big) holiday for Ivan Kupala.</ta>
            <ta e="T1141" id="Seg_12066" s="T1135">They ran through the village, poured water over each other.</ta>
            <ta e="T1146" id="Seg_12067" s="T1142">And they gathered, went.</ta>
            <ta e="T1157" id="Seg_12068" s="T1147">There is another mill than this mill, in which Estonian live.</ta>
            <ta e="T1161" id="Seg_12069" s="T1157">She went there to the pond.</ta>
            <ta e="T1173" id="Seg_12070" s="T1162">They went, went, went loudly, and there was a well in the earth.</ta>
            <ta e="T1178" id="Seg_12071" s="T1174">Two (drowned.)</ta>
            <ta e="T1185" id="Seg_12072" s="T1179">And there was one with him.</ta>
            <ta e="T1195" id="Seg_12073" s="T1186">He had a stake, to pull him out, but he was gone.</ta>
            <ta e="T1209" id="Seg_12074" s="T1196">While he was going to the village, they pulled him out of the water, pumped, pumped, pumped not.</ta>
            <ta e="T1217" id="Seg_12075" s="T1210">And then they brought the dead and buried them.</ta>
            <ta e="T1229" id="Seg_12076" s="T1217">I buried him and came back home.</ta>
            <ta e="T1242" id="Seg_12077" s="T1235">Well, my head quivers at the thought.</ta>
            <ta e="T1248" id="Seg_12078" s="T1242">There is a lot of sadness in my head.</ta>
            <ta e="T1250" id="Seg_12079" s="T1249">Well.</ta>
            <ta e="T1275" id="Seg_12080" s="T1262">When he came to me: "Come, [my] mother has married, I'm living alone."</ta>
            <ta e="T1284" id="Seg_12081" s="T1276">I said: "Go ask my parents to give me in marriage."</ta>
            <ta e="T1290" id="Seg_12082" s="T1284">He says: "They won't give you, let's go."</ta>
            <ta e="T1295" id="Seg_12083" s="T1291">Then I gathered my clothes.</ta>
            <ta e="T1303" id="Seg_12084" s="T1296">In the night we took a horse, sat [on it], went to Aginskoye.</ta>
            <ta e="T1314" id="Seg_12085" s="T1304">We slept there the whole night, then we came again to that village, to Kaptyrlyk.</ta>
            <ta e="T1326" id="Seg_12086" s="T1315">There we were the whole day, then in the night we came to his place.</ta>
            <ta e="T1334" id="Seg_12087" s="T1327">Then, in the morning came his stepmother.</ta>
            <ta e="T1337" id="Seg_12088" s="T1335">Then we got up.</ta>
            <ta e="T1340" id="Seg_12089" s="T1338">I lived there.</ta>
            <ta e="T1346" id="Seg_12090" s="T1341">We mowed grass there.</ta>
            <ta e="T1351" id="Seg_12091" s="T1347">Then came his uncle.</ta>
            <ta e="T1356" id="Seg_12092" s="T1352">"Come to me to mow grass."</ta>
            <ta e="T1361" id="Seg_12093" s="T1357">He [had] brought a girl.</ta>
            <ta e="T1368" id="Seg_12094" s="T1362">I was told that he was going to live with her.</ta>
            <ta e="T1380" id="Seg_12095" s="T1369">I took all my clothes and went away with his uncle.</ta>
            <ta e="T1384" id="Seg_12096" s="T1380">He caught me up.</ta>
            <ta e="T1388" id="Seg_12097" s="T1385">Brought me home again.</ta>
            <ta e="T1392" id="Seg_12098" s="T1388">Then my mother came.</ta>
            <ta e="T1397" id="Seg_12099" s="T1393">I went away again.</ta>
            <ta e="T1404" id="Seg_12100" s="T1398">He came and brought me home.</ta>
            <ta e="T1410" id="Seg_12101" s="T1404">We went to sleep, then they came.</ta>
            <ta e="T1413" id="Seg_12102" s="T1411">"Well, live [together]."</ta>
            <ta e="T1418" id="Seg_12103" s="T1414">Then they blessed us.</ta>
            <ta e="T1421" id="Seg_12104" s="T1419">Then they went away.</ta>
            <ta e="T1426" id="Seg_12105" s="T1422">The next day they got up and came to us.</ta>
            <ta e="T1432" id="Seg_12106" s="T1427">They ate, then [she] went home.</ta>
            <ta e="T1439" id="Seg_12107" s="T1433">Then he went to his uncle to mow grass.</ta>
            <ta e="T1449" id="Seg_12108" s="T1440">And there… it was such a day, he (poured water over himself?).</ta>
            <ta e="T1452" id="Seg_12109" s="T1449">Then they went to the river.</ta>
            <ta e="T1461" id="Seg_12110" s="T1453">And there they entered the river, a girl and he.</ta>
            <ta e="T1463" id="Seg_12111" s="T1462">[they began drowning.]</ta>
            <ta e="T1477" id="Seg_12112" s="T1464">One boy gave [them] a twig, he brought [out] this girl, and that one drowned.</ta>
            <ta e="T1488" id="Seg_12113" s="T1484">Wait a moment</ta>
            <ta e="T1492" id="Seg_12114" s="T1488">What is to be said shortly.</ta>
            <ta e="T1508" id="Seg_12115" s="T1495">Tomorrow…</ta>
            <ta e="T1514" id="Seg_12116" s="T1513">It is over.</ta>
            <ta e="T1521" id="Seg_12117" s="T1518">And there...</ta>
            <ta e="T1538" id="Seg_12118" s="T1529">But in russian is say, will come....</ta>
            <ta e="T1543" id="Seg_12119" s="T1539">My brother will come, Arpad.</ta>
            <ta e="T1548" id="Seg_12120" s="T1544">Now in russian.</ta>
            <ta e="T1552" id="Seg_12121" s="T1549">You will be pleased.</ta>
            <ta e="T1563" id="Seg_12122" s="T1562">But</ta>
            <ta e="T1568" id="Seg_12123" s="T1564">Again (in kamas) </ta>
            <ta e="T1574" id="Seg_12124" s="T1571">Well, well</ta>
            <ta e="T1579" id="Seg_12125" s="T1576">Well, did you resolve it?</ta>
            <ta e="T1590" id="Seg_12126" s="T1586">Tomorrow I'll be waiting for Arpad.</ta>
            <ta e="T1600" id="Seg_12127" s="T1591">He said: "I'll come, take you and we'll go to my mother."</ta>
            <ta e="T1608" id="Seg_12128" s="T1601">So I won't go anywhere, I'll be waiting for him.</ta>
            <ta e="T1612" id="Seg_12129" s="T1609">For him to come.</ta>
            <ta e="T1627" id="Seg_12130" s="T1619">Tomorrow I will wait for Arpad.</ta>
            <ta e="T1643" id="Seg_12131" s="T1628">I will not got anywhere, he will come and I will go with him to his mother.</ta>
            <ta e="T1646" id="Seg_12132" s="T1644">This here.</ta>
            <ta e="T1649" id="Seg_12133" s="T1647">short.</ta>
            <ta e="T1659" id="Seg_12134" s="T1650">Today I was sleeping and dreamed of a man.</ta>
            <ta e="T1666" id="Seg_12135" s="T1660">He said to me: "Let's go somewhere."</ta>
            <ta e="T1671" id="Seg_12136" s="T1667">I said: "I won't go."</ta>
            <ta e="T1685" id="Seg_12137" s="T1672">Today I slept, say in a dream  - someone calls me: 'Come with me.'</ta>
            <ta e="T1691" id="Seg_12138" s="T1686">And I say: 'I will not go.'</ta>
            <ta e="T1694" id="Seg_12139" s="T1692">It ended, no?</ta>
            <ta e="T1706" id="Seg_12140" s="T1697">Yesterday I was sleeping and dreamed of my sister's husband.</ta>
            <ta e="T1712" id="Seg_12141" s="T1707">Then I went to you, I spoke.</ta>
            <ta e="T1719" id="Seg_12142" s="T1713">Then I said: someone will speak to me.</ta>
            <ta e="T1728" id="Seg_12143" s="T1720">Then chief[s] came to speak with me.</ta>
            <ta e="T1731" id="Seg_12144" s="T1729">Then there…</ta>
            <ta e="T1738" id="Seg_12145" s="T1732">In a dream I saw ma son-in-law.</ta>
            <ta e="T1742" id="Seg_12146" s="T1739">And I went there.</ta>
            <ta e="T1750" id="Seg_12147" s="T1743">The bosses arrived and I spoke to them.</ta>
            <ta e="T1763" id="Seg_12148" s="T1751">And I also said at home: 'I will speak to a boss.'</ta>
            <ta e="T1770" id="Seg_12149" s="T1764">This is what i dreamt.</ta>
            <ta e="T1801" id="Seg_12150" s="T1799">We went.</ta>
            <ta e="T1809" id="Seg_12151" s="T1801">It was that woman's day when her mother gave birth to her.</ta>
            <ta e="T1816" id="Seg_12152" s="T1810">I went and gave her three roubles.</ta>
            <ta e="T1823" id="Seg_12153" s="T1817">She caught us up, put [=gave] us eggs.</ta>
            <ta e="T1827" id="Seg_12154" s="T1824">She put butter and onion.</ta>
            <ta e="T1831" id="Seg_12155" s="T1828">She says: "Eat, eat."</ta>
            <ta e="T1835" id="Seg_12156" s="T1832">I ate a little.</ta>
            <ta e="T1842" id="Seg_12157" s="T1836">Then I drank one cup of tea.</ta>
            <ta e="T1845" id="Seg_12158" s="T1843">I said: "Enough."</ta>
            <ta e="T1860" id="Seg_12159" s="T1850">Then she said: 'Drink tea' and I say: 'Enough.'</ta>
            <ta e="T1864" id="Seg_12160" s="T1861">Build on the (table).</ta>
            <ta e="T1866" id="Seg_12161" s="T1865">Oh my god.</ta>
            <ta e="T1873" id="Seg_12162" s="T1867">We came to her, she was a birthdaychild.</ta>
            <ta e="T1877" id="Seg_12163" s="T1874">We brought her presents.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T6" id="Seg_12164" s="T1">Ich habe etwas vergessen.</ta>
            <ta e="T22" id="Seg_12165" s="T16">Die Tochter meiner Nichte hat geheiratet.</ta>
            <ta e="T31" id="Seg_12166" s="T23">Und sie gingen nach Mana, um eine Eisenbahn zu bauen.</ta>
            <ta e="T40" id="Seg_12167" s="T32">Und von dort bauten alle ein (?) im Norden.</ta>
            <ta e="T48" id="Seg_12168" s="T41">Und dann kam sie zu ihrer Mutter, um sie zu besuchen.</ta>
            <ta e="T53" id="Seg_12169" s="T49">Sie begrub ihn.</ta>
            <ta e="T58" id="Seg_12170" s="T54">Nun das ist…</ta>
            <ta e="T65" id="Seg_12171" s="T59">Ich sage es auf Russisch, ich vergesse es wieder.</ta>
            <ta e="T82" id="Seg_12172" s="T77">Bei euch gibt es eine Art Wind. </ta>
            <ta e="T92" id="Seg_12173" s="T84">Bei uns gibt es trockenen Wind. </ta>
            <ta e="T118" id="Seg_12174" s="T96">Ja, bei euch gibt es viel Wind, bei uns nicht, nicht wie bei euch. </ta>
            <ta e="T133" id="Seg_12175" s="T120">Ich habe es nur von Weitem gesehen. </ta>
            <ta e="T137" id="Seg_12176" s="T134">Dort war ich. </ta>
            <ta e="T141" id="Seg_12177" s="T138">Wo die fette Margaret ist.</ta>
            <ta e="T149" id="Seg_12178" s="T144">Deswegen bin ich dortin gegangen.</ta>
            <ta e="T160" id="Seg_12179" s="T149">Na ja, um so weit zu sehen, aber ging nicht nah ran.</ta>
            <ta e="T181" id="Seg_12180" s="T173">Aber rausgehen, nein, ich weiß nicht.</ta>
            <ta e="T184" id="Seg_12181" s="T182">Russisch sprechen?</ta>
            <ta e="T194" id="Seg_12182" s="T187">Die Tochter meiner Nichte hat geheiratet.</ta>
            <ta e="T207" id="Seg_12183" s="T195">Und ich sage, und ihr wie (?)</ta>
            <ta e="T224" id="Seg_12184" s="T208">Ich, sagt Lehrer Agafon Iwanowitsch, (…) er sagt mir, nun, ich auf Russisch… </ta>
            <ta e="T227" id="Seg_12185" s="T225">Ja.</ta>
            <ta e="T231" id="Seg_12186" s="T228">Nun ich (…).</ta>
            <ta e="T238" id="Seg_12187" s="T232">Dann saßen wir da und unterhielten uns (…).</ta>
            <ta e="T242" id="Seg_12188" s="T240">Nun. </ta>
            <ta e="T243" id="Seg_12189" s="T242">Dort.</ta>
            <ta e="T249" id="Seg_12190" s="T244">Ein Bruder von mir ist hingegangen.</ta>
            <ta e="T254" id="Seg_12191" s="T252">Nun, das ist …</ta>
            <ta e="T258" id="Seg_12192" s="T255">Junge (…).</ta>
            <ta e="T261" id="Seg_12193" s="T258">(auf keinen Fall) Ich werde es nicht aussprechen.</ta>
            <ta e="T268" id="Seg_12194" s="T266">Wo ist Mutter?</ta>
            <ta e="T280" id="Seg_12195" s="T275">Sie ging nach Sargolov.</ta>
            <ta e="T300" id="Seg_12196" s="T280">Diese Sprache, die sie dort sprechen, ich kann sie nur schlecht.</ta>
            <ta e="T314" id="Seg_12197" s="T306">Und auf seiner …</ta>
            <ta e="T336" id="Seg_12198" s="T322">Nun, in Ufa gibt es Tataren.</ta>
            <ta e="T342" id="Seg_12199" s="T337">In Kazan sprechen sie diese Sprache. </ta>
            <ta e="T346" id="Seg_12200" s="T342">Nun, einmal hier angekommen.</ta>
            <ta e="T349" id="Seg_12201" s="T347">Drei sind angekommen. </ta>
            <ta e="T352" id="Seg_12202" s="T349">Wer ist dorthin gekommen?</ta>
            <ta e="T358" id="Seg_12203" s="T353">Er sagt: Ein Jude, ein Tartar. </ta>
            <ta e="T363" id="Seg_12204" s="T359">Ein Jude, ein Tartar. </ta>
            <ta e="T368" id="Seg_12205" s="T363">(…) in dieser Sprache.</ta>
            <ta e="T375" id="Seg_12206" s="T370">In dieser Sprache. </ta>
            <ta e="T385" id="Seg_12207" s="T375">In unserer Sprache: "Ein Jude kam und ein Tatar kam".</ta>
            <ta e="T391" id="Seg_12208" s="T386">Ein Jude, ein Tartar. </ta>
            <ta e="T394" id="Seg_12209" s="T393">Tataren.</ta>
            <ta e="T400" id="Seg_12210" s="T395">Dann ist meine Nichte zu mir gekommen, um das Gras zu mähen.</ta>
            <ta e="T405" id="Seg_12211" s="T401">Wir haben daraus Heuhaufen gemacht.</ta>
            <ta e="T410" id="Seg_12212" s="T406">Einer ist umgefallen.</ta>
            <ta e="T413" id="Seg_12213" s="T411">Ich sagte…</ta>
            <ta e="T416" id="Seg_12214" s="T413">Da waren drei von uns.</ta>
            <ta e="T420" id="Seg_12215" s="T417">Ich sagte: </ta>
            <ta e="T424" id="Seg_12216" s="T420">"Schaut, (?) ist gefallen."</ta>
            <ta e="T430" id="Seg_12217" s="T425">Ich sagte: "Jemand wird sterben."</ta>
            <ta e="T437" id="Seg_12218" s="T431">Dann sagte ich: "Ich werde sterben.</ta>
            <ta e="T441" id="Seg_12219" s="T438">Ich bin alt.</ta>
            <ta e="T447" id="Seg_12220" s="T442">Und du bist noch nicht alt.</ta>
            <ta e="T452" id="Seg_12221" s="T448">Dann sind wir nach Hause gekommen.</ta>
            <ta e="T463" id="Seg_12222" s="T453">Als sie dahin gekommen ist, ging ich um Bärlauch zu pflücken.</ta>
            <ta e="T471" id="Seg_12223" s="T464">Eine Lerche… es gab keinen Wind, sie fiel um.</ta>
            <ta e="T487" id="Seg_12224" s="T472">Ich habe es niemanden gesagt, und später, als ihr Ehemann ertrank, sagte ich es. </ta>
            <ta e="T493" id="Seg_12225" s="T488">Als ihr Mann ertrank, ich…</ta>
            <ta e="T506" id="Seg_12226" s="T494">Sie arbeitete bei mir beim Mähen, sie war gut Heuhaufen zu machen, das habe ich gesagt. </ta>
            <ta e="T511" id="Seg_12227" s="T507">Sie stand und fiel.</ta>
            <ta e="T528" id="Seg_12228" s="T512">Und wir waren zu dritt und ich sagte: "Einer von uns wird sterben."</ta>
            <ta e="T540" id="Seg_12229" s="T528">Und ich denke, sage zu ihnen: "Ich werde sterben, ich bin älter als ihr."</ta>
            <ta e="T550" id="Seg_12230" s="T541">Und dann bin ich aufs Feld gegangen, Zwiebeln pflücken, auf dem Feld. </ta>
            <ta e="T558" id="Seg_12231" s="T551">Es gab keinen Wind, das Blatt fiel.</ta>
            <ta e="T569" id="Seg_12232" s="T559">Und ich dachte: "Jemand wird sterben."</ta>
            <ta e="T582" id="Seg_12233" s="T257">Als mein Vater in unser Dorf, nach Spasskoe, zu unserem Platz, kam.</ta>
            <ta e="T588" id="Seg_12234" s="T583">Er kommt, eine Lerche fällt um.</ta>
            <ta e="T595" id="Seg_12235" s="T589">Dann fiel ein Ast vom Baum auf den Boden.</ta>
            <ta e="T603" id="Seg_12236" s="T596">Dann kam er nach Hause und erzählt es mir.</ta>
            <ta e="T610" id="Seg_12237" s="T604">Ich sagte: Eins der Kinder wird sterben.</ta>
            <ta e="T617" id="Seg_12238" s="T611">Und so war es.</ta>
            <ta e="T626" id="Seg_12239" s="T618">Mein Sohn ging zum Fluss und starb dort.</ta>
            <ta e="T671" id="Seg_12240" s="T665">Ich habe es gestern vergessen, ich habe darüber gesprochen.</ta>
            <ta e="T683" id="Seg_12241" s="T672">Über mich, ich habe geheiratet, ich habe vergessen, ob ich darüber geredet habe oder nicht?</ta>
            <ta e="T723" id="Seg_12242" s="T719">Wie mein Mann ertrunken ist.</ta>
            <ta e="T729" id="Seg_12243" s="T724">Nun, das sagt er, nein?</ta>
            <ta e="T756" id="Seg_12244" s="T743">Nun, als ich ein Mädchen war, war dort das Dorf Pjankowo, dort war ein Mann, sein Name war Alexej.</ta>
            <ta e="T769" id="Seg_12245" s="T757">Wir waren mit ihm befreundet, als ich dort war, war er hier.</ta>
            <ta e="T786" id="Seg_12246" s="T770">Und dann starb sein (Mutter =) Vater, und seine Mutter heiratete, sie verließ ihn und er wurde allein gelassen.</ta>
            <ta e="T791" id="Seg_12247" s="T787">Er war siebzehn Jahre alt.</ta>
            <ta e="T797" id="Seg_12248" s="T792">Und ich bin siebzehn, ich war jung!</ta>
            <ta e="T802" id="Seg_12249" s="T798">Nun, und er kam.</ta>
            <ta e="T809" id="Seg_12250" s="T802">Ich sagte zu ihm: "Geh zu meinen Eltern und halte um meine Hand an."</ta>
            <ta e="T819" id="Seg_12251" s="T810">Er sagt: "Nein, sie werden dich nicht freigeben, sie werden sagen: du bist jung, lass uns gehen!"</ta>
            <ta e="T831" id="Seg_12252" s="T820">Und ich sammelte meine Kleider zusammen, band sie in ein Bündel, stieg nachts auf ein Pferd und ging. </ta>
            <ta e="T841" id="Seg_12253" s="T832">Wir sind nicht auf dem direkten Weg hierher gekommen, sondern sind durch Aginsko gekommen.</ta>
            <ta e="T845" id="Seg_12254" s="T842">Wir ritten, ritten nachts, oh. </ta>
            <ta e="T857" id="Seg_12255" s="T846">Dann kamen wir an, dort das Dorf Kaptyrlyk, das russische Volk, verbrachte die Nacht dort.</ta>
            <ta e="T868" id="Seg_12256" s="T858">Und dort blieben wir den ganzen Tag, er ging nicht nach Hause. </ta>
            <ta e="T875" id="Seg_12257" s="T869">Ob wegen des Gewissens oder was, ich weiß es nicht. </ta>
            <ta e="T878" id="Seg_12258" s="T875">Dann kamen wir am Abend an.</ta>
            <ta e="T895" id="Seg_12259" s="T879">Und er sattelte das Pferd, wir ging ins Haus und schlief dann, und dort lebte ich, das ist alles.</ta>
            <ta e="T902" id="Seg_12260" s="T896">Und dann kam die (?) Verwandte. </ta>
            <ta e="T912" id="Seg_12261" s="T903">Er verlor seine Augen, sah seinen Onkel nicht. </ta>
            <ta e="T918" id="Seg_12262" s="T913">Er sagte: Komm, hilf, das Heu zu schneiden.</ta>
            <ta e="T926" id="Seg_12263" s="T919">Er ist in unserem Dorf, in Abalakov.</ta>
            <ta e="T937" id="Seg_12264" s="T927">Dann brachte er das Mädchen aus einem anderen Dorf dorthin.</ta>
            <ta e="T948" id="Seg_12265" s="T937">Mir wurde gesagt, dass er sie mitnehmen und dich vertreiben will.</ta>
            <ta e="T955" id="Seg_12266" s="T948">Ich packte und ging mit dem (?) Onkel.</ta>
            <ta e="T961" id="Seg_12267" s="T956">Er holte mich ein und brachte mich zurück.</ta>
            <ta e="T964" id="Seg_12268" s="T962">Zurück. </ta>
            <ta e="T973" id="Seg_12269" s="T965">Nun, danach kam meine Mutter hierher.</ta>
            <ta e="T980" id="Seg_12270" s="T974">Und ich ging zu Mama.</ta>
            <ta e="T989" id="Seg_12271" s="T981">Und Mama kam zu ihrem Neffen, der jetzt …</ta>
            <ta e="T1008" id="Seg_12272" s="T990">Für sie war es ihr Neffe, sein Name war Ivan, der jetzt in Pjankowo lebt, sie ist meine Nichte.</ta>
            <ta e="T1013" id="Seg_12273" s="T1009">Nun, Mama ist …</ta>
            <ta e="T1025" id="Seg_12274" s="T1014">Sie kam, und er kam wieder, nahm meine Hand und führte mich weg.</ta>
            <ta e="T1034" id="Seg_12275" s="T1026">Nun, dann legten wir uns hin und dann kamen sie. </ta>
            <ta e="T1045" id="Seg_12276" s="T1035">"Wenn ihr so lebt", sagt Mutter, "werde ich euch segnen."</ta>
            <ta e="T1055" id="Seg_12277" s="T1046">Hier hat sie uns (morgen) auch gesegnet.</ta>
            <ta e="T1074" id="Seg_12278" s="T1056">Sie sind zu uns gekommen, dieser Schwager, diese Schwägerin, wir haben hier Tee getrunken, sie sind dann gegangen, und meine Mutter ist gegangen.</ta>
            <ta e="T1081" id="Seg_12279" s="T1075">Nun, jetzt kann ich es auf Kamassisch sagen. </ta>
            <ta e="T1103" id="Seg_12280" s="T1090">Na, dann müssen wir noch sagen, dass er dorthin gegangen ist.</ta>
            <ta e="T1119" id="Seg_12281" s="T1104">Dann ließ er mich zu Hause, nahm mich nicht mit, ging zu uns nach Abalakovo.</ta>
            <ta e="T1123" id="Seg_12282" s="T1120">Der Onkel mäht das Heu.</ta>
            <ta e="T1134" id="Seg_12283" s="T1124">Und es war so ein Tag, ein (großer) Feiertag von Ivan Kupala.</ta>
            <ta e="T1141" id="Seg_12284" s="T1135">Sie liefen durch das Dorf, machten sich gegenseitig nass und überschütteten sich.</ta>
            <ta e="T1146" id="Seg_12285" s="T1142">Und sie haben sich versammelt, gingen. </ta>
            <ta e="T1157" id="Seg_12286" s="T1147">Es gibt noch eine andere Mühle als diese Mühle, in der Esten leben.</ta>
            <ta e="T1161" id="Seg_12287" s="T1157">Sie gingen dorthin zum Teich. </ta>
            <ta e="T1173" id="Seg_12288" s="T1162">Sie gingen, gingen, gingen laut, und dort kam eine Quelle aus der Erde. </ta>
            <ta e="T1178" id="Seg_12289" s="T1174">Zwei (ertranken?).</ta>
            <ta e="T1185" id="Seg_12290" s="T1179">Und da war einer bei ihm.</ta>
            <ta e="T1195" id="Seg_12291" s="T1186">Er hatte eine Stange, um ihn herauszuziehen, aber er war weg. </ta>
            <ta e="T1209" id="Seg_12292" s="T1196">Während er ins Dorf ging, zogen sie ihn aus dem Wasser, pumpten, pumpten, pumpten nicht. </ta>
            <ta e="T1217" id="Seg_12293" s="T1210">Und dann brachten sie die Toten und begruben sie.</ta>
            <ta e="T1229" id="Seg_12294" s="T1217">Ich begrub ihn und kam zurück nach Hause.</ta>
            <ta e="T1242" id="Seg_12295" s="T1235">Nun, mein Kopf zittert bei dem Gedanken. </ta>
            <ta e="T1248" id="Seg_12296" s="T1242">Es ist viel Traurigkeit in meinem Kopf.</ta>
            <ta e="T1250" id="Seg_12297" s="T1249">Nun.</ta>
            <ta e="T1275" id="Seg_12298" s="T1262">Als er zu mir kam: "Komm, [meine] Mutter hat geheiratet, ich lebe alleine."</ta>
            <ta e="T1284" id="Seg_12299" s="T1276">Ich sagte: "Geh zu meinen Eltern und halte um meine Hand an."</ta>
            <ta e="T1290" id="Seg_12300" s="T1284">Er sagt: "Sie werden dich nicht freigeben, lass uns gehen."</ta>
            <ta e="T1295" id="Seg_12301" s="T1291">Dann nahm ich meine Sachen.</ta>
            <ta e="T1303" id="Seg_12302" s="T1296">In der Nacht nahmen wir ein Pferd, saßen auf, ritten nach Aginskoye. </ta>
            <ta e="T1314" id="Seg_12303" s="T1304">Wir schließen die ganze Nacht, dann kamen wir wieder zu diesem Dorf, nach Kaptyrlyk.</ta>
            <ta e="T1326" id="Seg_12304" s="T1315">Dort waren wir die ganze Nacht, als es Nacht wurde, kamen wir an seinem Haus an. </ta>
            <ta e="T1334" id="Seg_12305" s="T1327">Dann, am morgen, kam seine Stiefmutter.</ta>
            <ta e="T1337" id="Seg_12306" s="T1335">Dann standen wir auf.</ta>
            <ta e="T1340" id="Seg_12307" s="T1338">Ich lebte dort.</ta>
            <ta e="T1346" id="Seg_12308" s="T1341">Wir haben das Gras dort gemäht.</ta>
            <ta e="T1351" id="Seg_12309" s="T1347">Dann kam sein Onkel.</ta>
            <ta e="T1356" id="Seg_12310" s="T1352">"Kommt zu mir und mäht das Gras."</ta>
            <ta e="T1361" id="Seg_12311" s="T1357">Er hatte ein Mädchen gebracht.</ta>
            <ta e="T1368" id="Seg_12312" s="T1362">Mir wurde gesagt, dass er mit ihr leben würde.</ta>
            <ta e="T1380" id="Seg_12313" s="T1369">Ich nahm all meine Kleidung und ging mit seinem Onkel weg.</ta>
            <ta e="T1384" id="Seg_12314" s="T1380">Er fing mich ab.</ta>
            <ta e="T1388" id="Seg_12315" s="T1385">Brachte mich wieder nach Hause.</ta>
            <ta e="T1392" id="Seg_12316" s="T1388">Dann kam meine Mutter.</ta>
            <ta e="T1397" id="Seg_12317" s="T1393">Ich ging wieder weg.</ta>
            <ta e="T1404" id="Seg_12318" s="T1398">Er kam und brachte mich wieder nach Hause.</ta>
            <ta e="T1410" id="Seg_12319" s="T1404">Wir gingen schlafen, dann kamen sie.</ta>
            <ta e="T1413" id="Seg_12320" s="T1411">"Gut, lebt [zusammen]."</ta>
            <ta e="T1418" id="Seg_12321" s="T1414">Dann segneten sie uns.</ta>
            <ta e="T1421" id="Seg_12322" s="T1419">Dann gingen sie fort.</ta>
            <ta e="T1426" id="Seg_12323" s="T1422">Am nächsten Tag standen sie auf und kamen zu uns.</ta>
            <ta e="T1432" id="Seg_12324" s="T1427">Sie aßen, dann ging [sie] nach Hause.</ta>
            <ta e="T1439" id="Seg_12325" s="T1433">Dann ging er zu seinem Onkel um Gras zu mähen.</ta>
            <ta e="T1449" id="Seg_12326" s="T1440">Und dort… es war so ein Tag, er (schüttete Wasser über sichselbst?).</ta>
            <ta e="T1452" id="Seg_12327" s="T1449">Dann gingen sie zum Fluss.</ta>
            <ta e="T1461" id="Seg_12328" s="T1453">Und dort stiegen sie in den Fluss, ein Mädchen und er.</ta>
            <ta e="T1463" id="Seg_12329" s="T1462">[sie fingen an unterzugehen.]</ta>
            <ta e="T1477" id="Seg_12330" s="T1464">Ein Junge gab [ihnen] ein Ast, er holte dieses Mädchen [raus], und er ertrank.</ta>
            <ta e="T1488" id="Seg_12331" s="T1484">Warte einen Moment.</ta>
            <ta e="T1492" id="Seg_12332" s="T1488">Was in Kürze zu sagen ist.</ta>
            <ta e="T1508" id="Seg_12333" s="T1495">Morgen…</ta>
            <ta e="T1514" id="Seg_12334" s="T1513">Es ist vorbei.</ta>
            <ta e="T1521" id="Seg_12335" s="T1518">Und da …</ta>
            <ta e="T1538" id="Seg_12336" s="T1529">Aber auf Russisch sage ich, wird komme…</ta>
            <ta e="T1543" id="Seg_12337" s="T1539">Mein Bruder wird kommen, Arpad.</ta>
            <ta e="T1548" id="Seg_12338" s="T1544">Jetzt auf Russisch. </ta>
            <ta e="T1552" id="Seg_12339" s="T1549">Du wirst dich freuen. </ta>
            <ta e="T1563" id="Seg_12340" s="T1562">Aber…</ta>
            <ta e="T1568" id="Seg_12341" s="T1564">Noch einmal (auf Kam-).</ta>
            <ta e="T1574" id="Seg_12342" s="T1571">Nun, nun. </ta>
            <ta e="T1579" id="Seg_12343" s="T1576">Na, hast du das behoben?</ta>
            <ta e="T1590" id="Seg_12344" s="T1586">Morgen werde ich auf Arpad warten.</ta>
            <ta e="T1600" id="Seg_12345" s="T1591">Er sagte: "Ich werde kommen, dich nehmen und wir werden zu meiner Mutter gehen."</ta>
            <ta e="T1608" id="Seg_12346" s="T1601">Also werde ich nirgends hingehen, ich werde auf ihn warten.</ta>
            <ta e="T1612" id="Seg_12347" s="T1609">Darauf, dass er kommt.</ta>
            <ta e="T1627" id="Seg_12348" s="T1619">Ich werde morgen auf Arpad warten.</ta>
            <ta e="T1643" id="Seg_12349" s="T1628">Ich werde nirgendwo hingehen, er wird kommen und ich werde mit ihm zu seiner Mutter gehen.</ta>
            <ta e="T1646" id="Seg_12350" s="T1644">Das hier.</ta>
            <ta e="T1649" id="Seg_12351" s="T1647">kurz.</ta>
            <ta e="T1659" id="Seg_12352" s="T1650">Heute habe ich geschlafen und habe von einem Mann geträumt.</ta>
            <ta e="T1666" id="Seg_12353" s="T1660">Er sagte zu mir: "Lass uns irgendwo hingehen."</ta>
            <ta e="T1671" id="Seg_12354" s="T1667">Ich sagte: "Ich werde nicht gehen."</ta>
            <ta e="T1685" id="Seg_12355" s="T1672">Heute habe ich geschlafen, in einem Traum gesehen - einer ruft mich: "Komm mit mir."</ta>
            <ta e="T1691" id="Seg_12356" s="T1686">Und ich sage: "Ich werde nicht gehen."</ta>
            <ta e="T1694" id="Seg_12357" s="T1692">Es endete, nein?</ta>
            <ta e="T1706" id="Seg_12358" s="T1697">Gestern habe ich geschlafen und von dem Ehemann meiner Schwester geträumt.</ta>
            <ta e="T1712" id="Seg_12359" s="T1707">Dann bin ich zu dir gegangen, ich habe gesprochen.</ta>
            <ta e="T1719" id="Seg_12360" s="T1713">Dann habe ich gesagt: Jemand wird mit mir sprechen.</ta>
            <ta e="T1728" id="Seg_12361" s="T1720">Dann kamen die Chefs um mit mir zu sprechen.</ta>
            <ta e="T1731" id="Seg_12362" s="T1729">Dann dort…</ta>
            <ta e="T1738" id="Seg_12363" s="T1732">In einem Traum sah ich meinen Schwiegersohn.</ta>
            <ta e="T1742" id="Seg_12364" s="T1739">Und ging dorthin.</ta>
            <ta e="T1750" id="Seg_12365" s="T1743">Die Chefs kamen dort an und ich sprach mit ihnen.</ta>
            <ta e="T1763" id="Seg_12366" s="T1751">Und ich sagte auch zu Hause: "Ich werde mit einem Chef sprechen."</ta>
            <ta e="T1770" id="Seg_12367" s="T1764">Das ist es, was ich träumte. </ta>
            <ta e="T1801" id="Seg_12368" s="T1799">Wir gingen.</ta>
            <ta e="T1809" id="Seg_12369" s="T1801">Es war dieser Frauentag an dem ihre Mutter sie geboren hatte.</ta>
            <ta e="T1816" id="Seg_12370" s="T1810">Ich ging und gab ihr drei Rubel.</ta>
            <ta e="T1823" id="Seg_12371" s="T1817">Sie fing uns ab, gab uns Eier.</ta>
            <ta e="T1827" id="Seg_12372" s="T1824">Sie gab uns Butter und Zwiebeln.</ta>
            <ta e="T1831" id="Seg_12373" s="T1828">Sie sagte: "Iss, iss."</ta>
            <ta e="T1835" id="Seg_12374" s="T1832">Ich aß ein bisschen.</ta>
            <ta e="T1842" id="Seg_12375" s="T1836">Dann trank ich eine Tasse Tee. </ta>
            <ta e="T1845" id="Seg_12376" s="T1843">Ich sagte: "Genug."</ta>
            <ta e="T1860" id="Seg_12377" s="T1850">Dann sagte sie: "Trinke Tee" und ich sage: "Genug."</ta>
            <ta e="T1864" id="Seg_12378" s="T1861">Aufgebaut auf (Tisch).</ta>
            <ta e="T1866" id="Seg_12379" s="T1865">Oh mein Gott! </ta>
            <ta e="T1873" id="Seg_12380" s="T1867">Wir kamen zu ihr, sie war ein Geburtstagskind.</ta>
            <ta e="T1877" id="Seg_12381" s="T1874">Wir brachten ihr Geschenke. </ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T261" id="Seg_12382" s="T258">[GVY:] 02:11-03:42, the recording must be played at 50% speed.</ta>
            <ta e="T266" id="Seg_12383" s="T264">TATAR</ta>
            <ta e="T274" id="Seg_12384" s="T269">TATAR</ta>
            <ta e="T463" id="Seg_12385" s="T453">[GVY:] nĭŋgəmbiem</ta>
            <ta e="T582" id="Seg_12386" s="T257">[GVY:] From here, normal speed again.</ta>
            <ta e="T1314" id="Seg_12387" s="T1304">[GVY:] dĭʔnən. Kunolbibaʔ = kambibaʔ 'we were riding the whole night'?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T89" start="T87">
            <tli id="T87.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T172" start="T171">
            <tli id="T171.tx-KA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T9" id="Seg_12388" n="sc" s="T4">
               <ts e="T9" id="Seg_12390" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_12392" n="HIAT:w" s="T4">Да</ts>
                  <nts id="Seg_12393" n="HIAT:ip">,</nts>
                  <nts id="Seg_12394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_12396" n="HIAT:w" s="T5">ну</ts>
                  <nts id="Seg_12397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_12399" n="HIAT:w" s="T7">ничего</ts>
                  <nts id="Seg_12400" n="HIAT:ip">,</nts>
                  <nts id="Seg_12401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_12403" n="HIAT:w" s="T8">ничего</ts>
                  <nts id="Seg_12404" n="HIAT:ip">.</nts>
                  <nts id="Seg_12405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T11" id="Seg_12406" n="sc" s="T10">
               <ts e="T11" id="Seg_12408" n="HIAT:u" s="T10">
                  <nts id="Seg_12409" n="HIAT:ip">(</nts>
                  <nts id="Seg_12410" n="HIAT:ip">(</nts>
                  <ats e="T11" id="Seg_12411" n="HIAT:non-pho" s="T10">…</ats>
                  <nts id="Seg_12412" n="HIAT:ip">)</nts>
                  <nts id="Seg_12413" n="HIAT:ip">)</nts>
                  <nts id="Seg_12414" n="HIAT:ip">.</nts>
                  <nts id="Seg_12415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T13" id="Seg_12416" n="sc" s="T12">
               <ts e="T13" id="Seg_12418" n="HIAT:u" s="T12">
                  <nts id="Seg_12419" n="HIAT:ip">(</nts>
                  <nts id="Seg_12420" n="HIAT:ip">(</nts>
                  <ats e="T13" id="Seg_12421" n="HIAT:non-pho" s="T12">…</ats>
                  <nts id="Seg_12422" n="HIAT:ip">)</nts>
                  <nts id="Seg_12423" n="HIAT:ip">)</nts>
                  <nts id="Seg_12424" n="HIAT:ip">.</nts>
                  <nts id="Seg_12425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_12426" n="sc" s="T14">
               <ts e="T15" id="Seg_12428" n="HIAT:u" s="T14">
                  <nts id="Seg_12429" n="HIAT:ip">(</nts>
                  <nts id="Seg_12430" n="HIAT:ip">(</nts>
                  <ats e="T15" id="Seg_12431" n="HIAT:non-pho" s="T14">…</ats>
                  <nts id="Seg_12432" n="HIAT:ip">)</nts>
                  <nts id="Seg_12433" n="HIAT:ip">)</nts>
                  <nts id="Seg_12434" n="HIAT:ip">.</nts>
                  <nts id="Seg_12435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T67" id="Seg_12436" n="sc" s="T66">
               <ts e="T67" id="Seg_12438" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_12440" n="HIAT:w" s="T66">Ага</ts>
                  <nts id="Seg_12441" n="HIAT:ip">.</nts>
                  <nts id="Seg_12442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_12443" n="sc" s="T70">
               <ts e="T76" id="Seg_12445" n="HIAT:u" s="T70">
                  <nts id="Seg_12446" n="HIAT:ip">(</nts>
                  <nts id="Seg_12447" n="HIAT:ip">(</nts>
                  <ats e="T71" id="Seg_12448" n="HIAT:non-pho" s="T70">DMG</ats>
                  <nts id="Seg_12449" n="HIAT:ip">)</nts>
                  <nts id="Seg_12450" n="HIAT:ip">)</nts>
                  <nts id="Seg_12451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_12453" n="HIAT:w" s="T71">зараза</ts>
                  <nts id="Seg_12454" n="HIAT:ip">,</nts>
                  <nts id="Seg_12455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_12457" n="HIAT:w" s="T72">в</ts>
                  <nts id="Seg_12458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_12460" n="HIAT:w" s="T73">общем</ts>
                  <nts id="Seg_12461" n="HIAT:ip">,</nts>
                  <nts id="Seg_12462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_12464" n="HIAT:w" s="T74">такая</ts>
                  <nts id="Seg_12465" n="HIAT:ip">,</nts>
                  <nts id="Seg_12466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_12468" n="HIAT:w" s="T75">по-моему</ts>
                  <nts id="Seg_12469" n="HIAT:ip">.</nts>
                  <nts id="Seg_12470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T85" id="Seg_12471" n="sc" s="T83">
               <ts e="T85" id="Seg_12473" n="HIAT:u" s="T83">
                  <ts e="T85" id="Seg_12475" n="HIAT:w" s="T83">Да</ts>
                  <nts id="Seg_12476" n="HIAT:ip">.</nts>
                  <nts id="Seg_12477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_12478" n="sc" s="T87">
               <ts e="T94" id="Seg_12480" n="HIAT:u" s="T87">
                  <ts e="T87.tx-KA.1" id="Seg_12482" n="HIAT:w" s="T87">У</ts>
                  <nts id="Seg_12483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_12485" n="HIAT:w" s="T87.tx-KA.1">нас</ts>
                  <nts id="Seg_12486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_12488" n="HIAT:w" s="T89">очень</ts>
                  <nts id="Seg_12489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_12491" n="HIAT:w" s="T91">сыро</ts>
                  <nts id="Seg_12492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_12494" n="HIAT:w" s="T93">здесь</ts>
                  <nts id="Seg_12495" n="HIAT:ip">.</nts>
                  <nts id="Seg_12496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_12498" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_12500" n="HIAT:w" s="T94">Потому</ts>
                  <nts id="Seg_12501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_12503" n="HIAT:w" s="T95">что</ts>
                  <nts id="Seg_12504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_12506" n="HIAT:w" s="T97">прямо</ts>
                  <nts id="Seg_12507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_12509" n="HIAT:w" s="T98">море</ts>
                  <nts id="Seg_12510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12511" n="HIAT:ip">(</nts>
                  <nts id="Seg_12512" n="HIAT:ip">(</nts>
                  <ats e="T102" id="Seg_12513" n="HIAT:non-pho" s="T100">…</ats>
                  <nts id="Seg_12514" n="HIAT:ip">)</nts>
                  <nts id="Seg_12515" n="HIAT:ip">)</nts>
                  <nts id="Seg_12516" n="HIAT:ip">.</nts>
                  <nts id="Seg_12517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_12518" n="sc" s="T109">
               <ts e="T121" id="Seg_12520" n="HIAT:u" s="T109">
                  <ts e="T111" id="Seg_12522" n="HIAT:w" s="T109">Ты</ts>
                  <nts id="Seg_12523" n="HIAT:ip">,</nts>
                  <nts id="Seg_12524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_12526" n="HIAT:w" s="T111">наверное</ts>
                  <nts id="Seg_12527" n="HIAT:ip">,</nts>
                  <nts id="Seg_12528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_12530" n="HIAT:w" s="T113">море</ts>
                  <nts id="Seg_12531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_12533" n="HIAT:w" s="T115">видела</ts>
                  <nts id="Seg_12534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_12536" n="HIAT:w" s="T117">здесь</ts>
                  <nts id="Seg_12537" n="HIAT:ip">,</nts>
                  <nts id="Seg_12538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_12540" n="HIAT:w" s="T119">да</ts>
                  <nts id="Seg_12541" n="HIAT:ip">?</nts>
                  <nts id="Seg_12542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T128" id="Seg_12543" n="sc" s="T123">
               <ts e="T128" id="Seg_12545" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_12547" n="HIAT:w" s="T123">На</ts>
                  <nts id="Seg_12548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_12550" n="HIAT:w" s="T124">берегу</ts>
                  <nts id="Seg_12551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_12553" n="HIAT:w" s="T126">была</ts>
                  <nts id="Seg_12554" n="HIAT:ip">?</nts>
                  <nts id="Seg_12555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_12556" n="sc" s="T130">
               <ts e="T135" id="Seg_12558" n="HIAT:u" s="T130">
                  <ts e="T132" id="Seg_12560" n="HIAT:w" s="T130">Ага</ts>
                  <nts id="Seg_12561" n="HIAT:ip">,</nts>
                  <nts id="Seg_12562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_12564" n="HIAT:w" s="T132">ага</ts>
                  <nts id="Seg_12565" n="HIAT:ip">.</nts>
                  <nts id="Seg_12566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_12567" n="sc" s="T141">
               <ts e="T145" id="Seg_12569" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_12571" n="HIAT:w" s="T141">Ага-ага-ага</ts>
                  <nts id="Seg_12572" n="HIAT:ip">,</nts>
                  <nts id="Seg_12573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_12575" n="HIAT:w" s="T142">вот</ts>
                  <nts id="Seg_12576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12577" n="HIAT:ip">(</nts>
                  <nts id="Seg_12578" n="HIAT:ip">(</nts>
                  <ats e="T145" id="Seg_12579" n="HIAT:non-pho" s="T143">…</ats>
                  <nts id="Seg_12580" n="HIAT:ip">)</nts>
                  <nts id="Seg_12581" n="HIAT:ip">)</nts>
                  <nts id="Seg_12582" n="HIAT:ip">.</nts>
                  <nts id="Seg_12583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T155" id="Seg_12584" n="sc" s="T151">
               <ts e="T155" id="Seg_12586" n="HIAT:u" s="T151">
                  <ts e="T155" id="Seg_12588" n="HIAT:w" s="T151">Да</ts>
                  <nts id="Seg_12589" n="HIAT:ip">…</nts>
                  <nts id="Seg_12590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_12591" n="sc" s="T161">
               <ts e="T170" id="Seg_12593" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_12595" n="HIAT:w" s="T161">Да</ts>
                  <nts id="Seg_12596" n="HIAT:ip">,</nts>
                  <nts id="Seg_12597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12598" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_12600" n="HIAT:w" s="T162">вот</ts>
                  <nts id="Seg_12601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_12603" n="HIAT:w" s="T163">и</ts>
                  <nts id="Seg_12604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_12606" n="HIAT:w" s="T164">я=</ts>
                  <nts id="Seg_12607" n="HIAT:ip">)</nts>
                  <nts id="Seg_12608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_12610" n="HIAT:w" s="T165">от</ts>
                  <nts id="Seg_12611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_12613" n="HIAT:w" s="T166">моря</ts>
                  <nts id="Seg_12614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_12616" n="HIAT:w" s="T167">и</ts>
                  <nts id="Seg_12617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_12619" n="HIAT:w" s="T168">сыро</ts>
                  <nts id="Seg_12620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_12622" n="HIAT:w" s="T169">получается</ts>
                  <nts id="Seg_12623" n="HIAT:ip">.</nts>
                  <nts id="Seg_12624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T175" id="Seg_12625" n="sc" s="T171">
               <ts e="T175" id="Seg_12627" n="HIAT:u" s="T171">
                  <ts e="T171.tx-KA.1" id="Seg_12629" n="HIAT:w" s="T171">Как</ts>
                  <nts id="Seg_12630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12631" n="HIAT:ip">(</nts>
                  <nts id="Seg_12632" n="HIAT:ip">(</nts>
                  <ats e="T172" id="Seg_12633" n="HIAT:non-pho" s="T171.tx-KA.1">PAUSE</ats>
                  <nts id="Seg_12634" n="HIAT:ip">)</nts>
                  <nts id="Seg_12635" n="HIAT:ip">)</nts>
                  <nts id="Seg_12636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_12638" n="HIAT:w" s="T172">море</ts>
                  <nts id="Seg_12639" n="HIAT:ip">…</nts>
                  <nts id="Seg_12640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T186" id="Seg_12641" n="sc" s="T185">
               <ts e="T186" id="Seg_12643" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_12645" n="HIAT:w" s="T185">Да</ts>
                  <nts id="Seg_12646" n="HIAT:ip">.</nts>
                  <nts id="Seg_12647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T226" id="Seg_12648" n="sc" s="T216">
               <ts e="T226" id="Seg_12650" n="HIAT:u" s="T216">
                  <ts e="T219" id="Seg_12652" n="HIAT:w" s="T216">Да</ts>
                  <nts id="Seg_12653" n="HIAT:ip">,</nts>
                  <nts id="Seg_12654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_12656" n="HIAT:w" s="T219">это</ts>
                  <nts id="Seg_12657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12658" n="HIAT:ip">(</nts>
                  <nts id="Seg_12659" n="HIAT:ip">(</nts>
                  <ats e="T223" id="Seg_12660" n="HIAT:non-pho" s="T221">…</ats>
                  <nts id="Seg_12661" n="HIAT:ip">)</nts>
                  <nts id="Seg_12662" n="HIAT:ip">)</nts>
                  <nts id="Seg_12663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_12665" n="HIAT:w" s="T223">Ивановна</ts>
                  <nts id="Seg_12666" n="HIAT:ip">.</nts>
                  <nts id="Seg_12667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_12668" n="sc" s="T237">
               <ts e="T241" id="Seg_12670" n="HIAT:u" s="T237">
                  <ts e="T239" id="Seg_12672" n="HIAT:w" s="T237">Наподобие</ts>
                  <nts id="Seg_12673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_12675" n="HIAT:w" s="T239">степного</ts>
                  <nts id="Seg_12676" n="HIAT:ip">.</nts>
                  <nts id="Seg_12677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_12678" n="sc" s="T279">
               <ts e="T290" id="Seg_12680" n="HIAT:u" s="T279">
                  <ts e="T281" id="Seg_12682" n="HIAT:w" s="T279">Ага</ts>
                  <nts id="Seg_12683" n="HIAT:ip">,</nts>
                  <nts id="Seg_12684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_12686" n="HIAT:w" s="T281">значит</ts>
                  <nts id="Seg_12687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_12689" n="HIAT:w" s="T283">вот</ts>
                  <nts id="Seg_12690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_12692" n="HIAT:w" s="T285">это</ts>
                  <nts id="Seg_12693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_12695" n="HIAT:w" s="T287">на</ts>
                  <nts id="Seg_12696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_12698" n="HIAT:w" s="T289">степном</ts>
                  <nts id="Seg_12699" n="HIAT:ip">.</nts>
                  <nts id="Seg_12700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T298" id="Seg_12701" n="sc" s="T292">
               <ts e="T298" id="Seg_12703" n="HIAT:u" s="T292">
                  <ts e="T295" id="Seg_12705" n="HIAT:w" s="T292">На</ts>
                  <nts id="Seg_12706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_12708" n="HIAT:w" s="T295">степном</ts>
                  <nts id="Seg_12709" n="HIAT:ip">.</nts>
                  <nts id="Seg_12710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_12711" n="sc" s="T301">
               <ts e="T305" id="Seg_12713" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_12715" n="HIAT:w" s="T301">Ну</ts>
                  <nts id="Seg_12716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_12718" n="HIAT:w" s="T302">это</ts>
                  <nts id="Seg_12719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_12721" n="HIAT:w" s="T303">и</ts>
                  <nts id="Seg_12722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_12724" n="HIAT:w" s="T304">неинтересно</ts>
                  <nts id="Seg_12725" n="HIAT:ip">.</nts>
                  <nts id="Seg_12726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_12727" n="sc" s="T308">
               <ts e="T321" id="Seg_12729" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_12731" n="HIAT:w" s="T308">Потому</ts>
                  <nts id="Seg_12732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_12734" n="HIAT:w" s="T309">что</ts>
                  <nts id="Seg_12735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12736" n="HIAT:ip">(</nts>
                  <ts e="T312" id="Seg_12738" n="HIAT:w" s="T310">на</ts>
                  <nts id="Seg_12739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_12741" n="HIAT:w" s="T312">этом=</ts>
                  <nts id="Seg_12742" n="HIAT:ip">)</nts>
                  <nts id="Seg_12743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_12745" n="HIAT:w" s="T313">на</ts>
                  <nts id="Seg_12746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_12748" n="HIAT:w" s="T315">этом</ts>
                  <nts id="Seg_12749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_12751" n="HIAT:w" s="T316">языке</ts>
                  <nts id="Seg_12752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_12754" n="HIAT:w" s="T317">еще</ts>
                  <nts id="Seg_12755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_12757" n="HIAT:w" s="T318">сейчас</ts>
                  <nts id="Seg_12758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_12760" n="HIAT:w" s="T319">многие</ts>
                  <nts id="Seg_12761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_12763" n="HIAT:w" s="T320">говорят</ts>
                  <nts id="Seg_12764" n="HIAT:ip">.</nts>
                  <nts id="Seg_12765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_12766" n="sc" s="T323">
               <ts e="T334" id="Seg_12768" n="HIAT:u" s="T323">
                  <ts e="T325" id="Seg_12770" n="HIAT:w" s="T323">Вот</ts>
                  <nts id="Seg_12771" n="HIAT:ip">,</nts>
                  <nts id="Seg_12772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_12774" n="HIAT:w" s="T325">а</ts>
                  <nts id="Seg_12775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_12777" n="HIAT:w" s="T326">на</ts>
                  <nts id="Seg_12778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_12780" n="HIAT:w" s="T328">камасинском</ts>
                  <nts id="Seg_12781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_12783" n="HIAT:w" s="T330">никто</ts>
                  <nts id="Seg_12784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_12786" n="HIAT:w" s="T331">не</ts>
                  <nts id="Seg_12787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_12789" n="HIAT:w" s="T333">говорит</ts>
                  <nts id="Seg_12790" n="HIAT:ip">.</nts>
                  <nts id="Seg_12791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T369" id="Seg_12792" n="sc" s="T366">
               <ts e="T369" id="Seg_12794" n="HIAT:u" s="T366">
                  <ts e="T369" id="Seg_12796" n="HIAT:w" s="T366">Ага</ts>
                  <nts id="Seg_12797" n="HIAT:ip">.</nts>
                  <nts id="Seg_12798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_12799" n="sc" s="T371">
               <ts e="T374" id="Seg_12801" n="HIAT:u" s="T371">
                  <ts e="T374" id="Seg_12803" n="HIAT:w" s="T371">Ага</ts>
                  <nts id="Seg_12804" n="HIAT:ip">.</nts>
                  <nts id="Seg_12805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T392" id="Seg_12806" n="sc" s="T389">
               <ts e="T392" id="Seg_12808" n="HIAT:u" s="T389">
                  <ts e="T392" id="Seg_12810" n="HIAT:w" s="T389">Ага</ts>
                  <nts id="Seg_12811" n="HIAT:ip">.</nts>
                  <nts id="Seg_12812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T645" id="Seg_12813" n="sc" s="T627">
               <ts e="T645" id="Seg_12815" n="HIAT:u" s="T627">
                  <nts id="Seg_12816" n="HIAT:ip">(</nts>
                  <nts id="Seg_12817" n="HIAT:ip">(</nts>
                  <ats e="T628" id="Seg_12818" n="HIAT:non-pho" s="T627">DMG</ats>
                  <nts id="Seg_12819" n="HIAT:ip">)</nts>
                  <nts id="Seg_12820" n="HIAT:ip">)</nts>
                  <nts id="Seg_12821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_12823" n="HIAT:w" s="T628">Таким</ts>
                  <nts id="Seg_12824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_12826" n="HIAT:w" s="T629">образом</ts>
                  <nts id="Seg_12827" n="HIAT:ip">,</nts>
                  <nts id="Seg_12828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_12830" n="HIAT:w" s="T630">что</ts>
                  <nts id="Seg_12831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_12833" n="HIAT:w" s="T631">он</ts>
                  <nts id="Seg_12834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_12836" n="HIAT:w" s="T632">будет</ts>
                  <nts id="Seg_12837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_12839" n="HIAT:w" s="T633">идти</ts>
                  <nts id="Seg_12840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_12842" n="HIAT:w" s="T634">все</ts>
                  <nts id="Seg_12843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_12845" n="HIAT:w" s="T635">время</ts>
                  <nts id="Seg_12846" n="HIAT:ip">,</nts>
                  <nts id="Seg_12847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_12849" n="HIAT:w" s="T636">все</ts>
                  <nts id="Seg_12850" n="HIAT:ip">,</nts>
                  <nts id="Seg_12851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_12853" n="HIAT:w" s="T637">что</ts>
                  <nts id="Seg_12854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_12856" n="HIAT:w" s="T638">расскажешь</ts>
                  <nts id="Seg_12857" n="HIAT:ip">,</nts>
                  <nts id="Seg_12858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_12860" n="HIAT:w" s="T639">будет</ts>
                  <nts id="Seg_12861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_12863" n="HIAT:w" s="T640">все</ts>
                  <nts id="Seg_12864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_12866" n="HIAT:w" s="T641">время</ts>
                  <nts id="Seg_12867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_12869" n="HIAT:w" s="T642">записывать</ts>
                  <nts id="Seg_12870" n="HIAT:ip">,</nts>
                  <nts id="Seg_12871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_12873" n="HIAT:w" s="T643">все</ts>
                  <nts id="Seg_12874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_12876" n="HIAT:w" s="T644">хорошо</ts>
                  <nts id="Seg_12877" n="HIAT:ip">.</nts>
                  <nts id="Seg_12878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T657" id="Seg_12879" n="sc" s="T646">
               <ts e="T657" id="Seg_12881" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_12883" n="HIAT:w" s="T646">Потому</ts>
                  <nts id="Seg_12884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_12886" n="HIAT:w" s="T647">что</ts>
                  <nts id="Seg_12887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_12889" n="HIAT:w" s="T648">ты</ts>
                  <nts id="Seg_12890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_12892" n="HIAT:w" s="T649">сначала</ts>
                  <nts id="Seg_12893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_12895" n="HIAT:w" s="T650">по-русски</ts>
                  <nts id="Seg_12896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_12898" n="HIAT:w" s="T651">расскажешь</ts>
                  <nts id="Seg_12899" n="HIAT:ip">,</nts>
                  <nts id="Seg_12900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_12902" n="HIAT:w" s="T652">а</ts>
                  <nts id="Seg_12903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_12905" n="HIAT:w" s="T653">потом</ts>
                  <nts id="Seg_12906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_12908" n="HIAT:w" s="T654">просто</ts>
                  <nts id="Seg_12909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_12911" n="HIAT:w" s="T655">повторишь</ts>
                  <nts id="Seg_12912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_12914" n="HIAT:w" s="T656">по-камасински</ts>
                  <nts id="Seg_12915" n="HIAT:ip">.</nts>
                  <nts id="Seg_12916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T664" id="Seg_12917" n="sc" s="T658">
               <ts e="T664" id="Seg_12919" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_12921" n="HIAT:w" s="T658">Чтобы</ts>
                  <nts id="Seg_12922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_12924" n="HIAT:w" s="T659">тебе</ts>
                  <nts id="Seg_12925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_12927" n="HIAT:w" s="T660">не</ts>
                  <nts id="Seg_12928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_12930" n="HIAT:w" s="T661">несколько</ts>
                  <nts id="Seg_12931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_12933" n="HIAT:w" s="T662">раз</ts>
                  <nts id="Seg_12934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_12936" n="HIAT:w" s="T663">рассказывать</ts>
                  <nts id="Seg_12937" n="HIAT:ip">.</nts>
                  <nts id="Seg_12938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T690" id="Seg_12939" n="sc" s="T680">
               <ts e="T690" id="Seg_12941" n="HIAT:u" s="T680">
                  <ts e="T682" id="Seg_12943" n="HIAT:w" s="T680">Нет</ts>
                  <nts id="Seg_12944" n="HIAT:ip">,</nts>
                  <nts id="Seg_12945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_12947" n="HIAT:w" s="T682">нет-нет-нет</ts>
                  <nts id="Seg_12948" n="HIAT:ip">,</nts>
                  <nts id="Seg_12949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_12951" n="HIAT:w" s="T684">это</ts>
                  <nts id="Seg_12952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_12954" n="HIAT:w" s="T685">мы</ts>
                  <nts id="Seg_12955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_12957" n="HIAT:w" s="T686">вчера</ts>
                  <nts id="Seg_12958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_12960" n="HIAT:w" s="T687">не</ts>
                  <nts id="Seg_12961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_12963" n="HIAT:w" s="T688">говорили</ts>
                  <nts id="Seg_12964" n="HIAT:ip">,</nts>
                  <nts id="Seg_12965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_12967" n="HIAT:w" s="T689">нет-нет</ts>
                  <nts id="Seg_12968" n="HIAT:ip">.</nts>
                  <nts id="Seg_12969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T695" id="Seg_12970" n="sc" s="T691">
               <ts e="T695" id="Seg_12972" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_12974" n="HIAT:w" s="T691">Вот</ts>
                  <nts id="Seg_12975" n="HIAT:ip">,</nts>
                  <nts id="Seg_12976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_12978" n="HIAT:w" s="T692">самое</ts>
                  <nts id="Seg_12979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_12981" n="HIAT:w" s="T693">хорошо</ts>
                  <nts id="Seg_12982" n="HIAT:ip">,</nts>
                  <nts id="Seg_12983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_12985" n="HIAT:w" s="T694">хорошо</ts>
                  <nts id="Seg_12986" n="HIAT:ip">.</nts>
                  <nts id="Seg_12987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T718" id="Seg_12988" n="sc" s="T696">
               <ts e="T715" id="Seg_12990" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_12992" n="HIAT:w" s="T696">Я</ts>
                  <nts id="Seg_12993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_12995" n="HIAT:w" s="T697">даже</ts>
                  <nts id="Seg_12996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_12998" n="HIAT:w" s="T698">на</ts>
                  <nts id="Seg_12999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_13001" n="HIAT:w" s="T699">этот</ts>
                  <nts id="Seg_13002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_13004" n="HIAT:w" s="T700">счет</ts>
                  <nts id="Seg_13005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_13007" n="HIAT:w" s="T701">хорошо</ts>
                  <nts id="Seg_13008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_13010" n="HIAT:w" s="T702">не</ts>
                  <nts id="Seg_13011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_13013" n="HIAT:w" s="T703">помню</ts>
                  <nts id="Seg_13014" n="HIAT:ip">,</nts>
                  <nts id="Seg_13015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_13017" n="HIAT:w" s="T704">мы</ts>
                  <nts id="Seg_13018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_13020" n="HIAT:w" s="T705">вообще</ts>
                  <nts id="Seg_13021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_13023" n="HIAT:w" s="T706">об</ts>
                  <nts id="Seg_13024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_13026" n="HIAT:w" s="T707">этом</ts>
                  <nts id="Seg_13027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_13029" n="HIAT:w" s="T708">в</ts>
                  <nts id="Seg_13030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_13032" n="HIAT:w" s="T709">Сибири</ts>
                  <nts id="Seg_13033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_13035" n="HIAT:w" s="T710">говорили</ts>
                  <nts id="Seg_13036" n="HIAT:ip">,</nts>
                  <nts id="Seg_13037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_13039" n="HIAT:w" s="T711">не</ts>
                  <nts id="Seg_13040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_13042" n="HIAT:w" s="T712">говорили</ts>
                  <nts id="Seg_13043" n="HIAT:ip">,</nts>
                  <nts id="Seg_13044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_13046" n="HIAT:w" s="T713">в</ts>
                  <nts id="Seg_13047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_13049" n="HIAT:w" s="T714">Абалаково</ts>
                  <nts id="Seg_13050" n="HIAT:ip">.</nts>
                  <nts id="Seg_13051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_13053" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_13055" n="HIAT:w" s="T715">Это</ts>
                  <nts id="Seg_13056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_13058" n="HIAT:w" s="T716">очень</ts>
                  <nts id="Seg_13059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_13061" n="HIAT:w" s="T717">интересно</ts>
                  <nts id="Seg_13062" n="HIAT:ip">.</nts>
                  <nts id="Seg_13063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T742" id="Seg_13064" n="sc" s="T730">
               <ts e="T742" id="Seg_13066" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_13068" n="HIAT:w" s="T730">Да</ts>
                  <nts id="Seg_13069" n="HIAT:ip">,</nts>
                  <nts id="Seg_13070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_13072" n="HIAT:w" s="T731">нет</ts>
                  <nts id="Seg_13073" n="HIAT:ip">,</nts>
                  <nts id="Seg_13074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_13076" n="HIAT:w" s="T732">можно</ts>
                  <nts id="Seg_13077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_13079" n="HIAT:w" s="T733">все</ts>
                  <nts id="Seg_13080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_13082" n="HIAT:w" s="T734">время</ts>
                  <nts id="Seg_13083" n="HIAT:ip">,</nts>
                  <nts id="Seg_13084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_13086" n="HIAT:w" s="T735">сейчас</ts>
                  <nts id="Seg_13087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_13089" n="HIAT:w" s="T736">всё</ts>
                  <nts id="Seg_13090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_13092" n="HIAT:w" s="T737">запишет</ts>
                  <nts id="Seg_13093" n="HIAT:ip">,</nts>
                  <nts id="Seg_13094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_13096" n="HIAT:w" s="T738">что</ts>
                  <nts id="Seg_13097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_13099" n="HIAT:w" s="T739">мы</ts>
                  <nts id="Seg_13100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_13102" n="HIAT:w" s="T740">здесь</ts>
                  <nts id="Seg_13103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_13105" n="HIAT:w" s="T741">говорим</ts>
                  <nts id="Seg_13106" n="HIAT:ip">.</nts>
                  <nts id="Seg_13107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1093" id="Seg_13108" n="sc" s="T1078">
               <ts e="T1093" id="Seg_13110" n="HIAT:u" s="T1078">
                  <ts e="T1080" id="Seg_13112" n="HIAT:w" s="T1078">Да</ts>
                  <nts id="Seg_13113" n="HIAT:ip">,</nts>
                  <nts id="Seg_13114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_13116" n="HIAT:w" s="T1080">сейчас</ts>
                  <nts id="Seg_13117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_13119" n="HIAT:w" s="T1082">можно</ts>
                  <nts id="Seg_13120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_13121" n="HIAT:ip">(</nts>
                  <ts e="T1084" id="Seg_13123" n="HIAT:w" s="T1083">по-ка-</ts>
                  <nts id="Seg_13124" n="HIAT:ip">)</nts>
                  <nts id="Seg_13125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_13127" n="HIAT:w" s="T1084">это</ts>
                  <nts id="Seg_13128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_13130" n="HIAT:w" s="T1085">ты</ts>
                  <nts id="Seg_13131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_13133" n="HIAT:w" s="T1086">совсем</ts>
                  <nts id="Seg_13134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_13136" n="HIAT:w" s="T1087">не</ts>
                  <nts id="Seg_13137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_13139" n="HIAT:w" s="T1088">рассказывала</ts>
                  <nts id="Seg_13140" n="HIAT:ip">,</nts>
                  <nts id="Seg_13141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_13143" n="HIAT:w" s="T1089">очень</ts>
                  <nts id="Seg_13144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_13146" n="HIAT:w" s="T1091">интересно</ts>
                  <nts id="Seg_13147" n="HIAT:ip">.</nts>
                  <nts id="Seg_13148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1231" id="Seg_13149" n="sc" s="T1230">
               <ts e="T1231" id="Seg_13151" n="HIAT:u" s="T1230">
                  <ts e="T1231" id="Seg_13153" n="HIAT:w" s="T1230">Да</ts>
                  <nts id="Seg_13154" n="HIAT:ip">.</nts>
                  <nts id="Seg_13155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1234" id="Seg_13156" n="sc" s="T1232">
               <ts e="T1234" id="Seg_13158" n="HIAT:u" s="T1232">
                  <ts e="T1233" id="Seg_13160" n="HIAT:w" s="T1232">Печальная</ts>
                  <nts id="Seg_13161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1234" id="Seg_13163" n="HIAT:w" s="T1233">история</ts>
                  <nts id="Seg_13164" n="HIAT:ip">.</nts>
                  <nts id="Seg_13165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1252" id="Seg_13166" n="sc" s="T1251">
               <ts e="T1252" id="Seg_13168" n="HIAT:u" s="T1251">
                  <ts e="T1252" id="Seg_13170" n="HIAT:w" s="T1251">Да</ts>
                  <nts id="Seg_13171" n="HIAT:ip">.</nts>
                  <nts id="Seg_13172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1256" id="Seg_13173" n="sc" s="T1253">
               <ts e="T1256" id="Seg_13175" n="HIAT:u" s="T1253">
                  <ts e="T1254" id="Seg_13177" n="HIAT:w" s="T1253">Ну</ts>
                  <nts id="Seg_13178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1255" id="Seg_13180" n="HIAT:w" s="T1254">что</ts>
                  <nts id="Seg_13181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1256" id="Seg_13183" n="HIAT:w" s="T1255">ж</ts>
                  <nts id="Seg_13184" n="HIAT:ip">.</nts>
                  <nts id="Seg_13185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1261" id="Seg_13186" n="sc" s="T1257">
               <ts e="T1261" id="Seg_13188" n="HIAT:u" s="T1257">
                  <ts e="T1258" id="Seg_13190" n="HIAT:w" s="T1257">Может</ts>
                  <nts id="Seg_13191" n="HIAT:ip">,</nts>
                  <nts id="Seg_13192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_13194" n="HIAT:w" s="T1258">постараешься</ts>
                  <nts id="Seg_13195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1260" id="Seg_13197" n="HIAT:w" s="T1259">рассказать</ts>
                  <nts id="Seg_13198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_13200" n="HIAT:w" s="T1260">по-камасински</ts>
                  <nts id="Seg_13201" n="HIAT:ip">?</nts>
                  <nts id="Seg_13202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1485" id="Seg_13203" n="sc" s="T1479">
               <ts e="T1485" id="Seg_13205" n="HIAT:u" s="T1479">
                  <ts e="T1481" id="Seg_13207" n="HIAT:w" s="T1479">Сейчас</ts>
                  <nts id="Seg_13208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1482" id="Seg_13210" n="HIAT:w" s="T1481">закрыто</ts>
                  <nts id="Seg_13211" n="HIAT:ip">,</nts>
                  <nts id="Seg_13212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1483" id="Seg_13214" n="HIAT:w" s="T1482">сейчас</ts>
                  <nts id="Seg_13215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1485" id="Seg_13217" n="HIAT:w" s="T1483">пустим</ts>
                  <nts id="Seg_13218" n="HIAT:ip">.</nts>
                  <nts id="Seg_13219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1487" id="Seg_13220" n="sc" s="T1486">
               <ts e="T1487" id="Seg_13222" n="HIAT:u" s="T1486">
                  <ts e="T1487" id="Seg_13224" n="HIAT:w" s="T1486">Ага</ts>
                  <nts id="Seg_13225" n="HIAT:ip">.</nts>
                  <nts id="Seg_13226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1497" id="Seg_13227" n="sc" s="T1493">
               <ts e="T1497" id="Seg_13229" n="HIAT:u" s="T1493">
                  <ts e="T1494" id="Seg_13231" n="HIAT:w" s="T1493">Ну</ts>
                  <nts id="Seg_13232" n="HIAT:ip">,</nts>
                  <nts id="Seg_13233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1496" id="Seg_13235" n="HIAT:w" s="T1494">это</ts>
                  <nts id="Seg_13236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1497" id="Seg_13238" n="HIAT:w" s="T1496">знаешь</ts>
                  <nts id="Seg_13239" n="HIAT:ip">…</nts>
                  <nts id="Seg_13240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1510" id="Seg_13241" n="sc" s="T1501">
               <ts e="T1510" id="Seg_13243" n="HIAT:u" s="T1501">
                  <ts e="T1502" id="Seg_13245" n="HIAT:w" s="T1501">Ага</ts>
                  <nts id="Seg_13246" n="HIAT:ip">,</nts>
                  <nts id="Seg_13247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1504" id="Seg_13249" n="HIAT:w" s="T1502">сейчас</ts>
                  <nts id="Seg_13250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1505" id="Seg_13252" n="HIAT:w" s="T1504">пустим</ts>
                  <nts id="Seg_13253" n="HIAT:ip">,</nts>
                  <nts id="Seg_13254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1507" id="Seg_13256" n="HIAT:w" s="T1505">пустим</ts>
                  <nts id="Seg_13257" n="HIAT:ip">,</nts>
                  <nts id="Seg_13258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1509" id="Seg_13260" n="HIAT:w" s="T1507">пустим</ts>
                  <nts id="Seg_13261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1510" id="Seg_13263" n="HIAT:w" s="T1509">сейчас</ts>
                  <nts id="Seg_13264" n="HIAT:ip">.</nts>
                  <nts id="Seg_13265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1517" id="Seg_13266" n="sc" s="T1515">
               <ts e="T1517" id="Seg_13268" n="HIAT:u" s="T1515">
                  <ts e="T1516" id="Seg_13270" n="HIAT:w" s="T1515">Повтори</ts>
                  <nts id="Seg_13271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1517" id="Seg_13273" n="HIAT:w" s="T1516">по-русски</ts>
                  <nts id="Seg_13274" n="HIAT:ip">.</nts>
                  <nts id="Seg_13275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1525" id="Seg_13276" n="sc" s="T1521">
               <ts e="T1525" id="Seg_13278" n="HIAT:u" s="T1521">
                  <ts e="T1522" id="Seg_13280" n="HIAT:w" s="T1521">Нет</ts>
                  <nts id="Seg_13281" n="HIAT:ip">,</nts>
                  <nts id="Seg_13282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1523" id="Seg_13284" n="HIAT:w" s="T1522">есть</ts>
                  <nts id="Seg_13285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1524" id="Seg_13287" n="HIAT:w" s="T1523">еще</ts>
                  <nts id="Seg_13288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_13289" n="HIAT:ip">(</nts>
                  <nts id="Seg_13290" n="HIAT:ip">(</nts>
                  <ats e="T1525" id="Seg_13291" n="HIAT:non-pho" s="T1524">…</ats>
                  <nts id="Seg_13292" n="HIAT:ip">)</nts>
                  <nts id="Seg_13293" n="HIAT:ip">)</nts>
                  <nts id="Seg_13294" n="HIAT:ip">.</nts>
                  <nts id="Seg_13295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1527" id="Seg_13296" n="sc" s="T1526">
               <ts e="T1527" id="Seg_13298" n="HIAT:u" s="T1526">
                  <ts e="T1527" id="Seg_13300" n="HIAT:w" s="T1526">Ja</ts>
                  <nts id="Seg_13301" n="HIAT:ip">.</nts>
                  <nts id="Seg_13302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1537" id="Seg_13303" n="sc" s="T1528">
               <ts e="T1537" id="Seg_13305" n="HIAT:u" s="T1528">
                  <nts id="Seg_13306" n="HIAT:ip">(</nts>
                  <nts id="Seg_13307" n="HIAT:ip">(</nts>
                  <ats e="T1537" id="Seg_13308" n="HIAT:non-pho" s="T1528">…</ats>
                  <nts id="Seg_13309" n="HIAT:ip">)</nts>
                  <nts id="Seg_13310" n="HIAT:ip">)</nts>
                  <nts id="Seg_13311" n="HIAT:ip">.</nts>
                  <nts id="Seg_13312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1554" id="Seg_13313" n="sc" s="T1546">
               <ts e="T1553" id="Seg_13315" n="HIAT:u" s="T1546">
                  <nts id="Seg_13316" n="HIAT:ip">(</nts>
                  <nts id="Seg_13317" n="HIAT:ip">(</nts>
                  <ats e="T1553" id="Seg_13318" n="HIAT:non-pho" s="T1546">…</ats>
                  <nts id="Seg_13319" n="HIAT:ip">)</nts>
                  <nts id="Seg_13320" n="HIAT:ip">)</nts>
                  <nts id="Seg_13321" n="HIAT:ip">.</nts>
                  <nts id="Seg_13322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1554" id="Seg_13324" n="HIAT:u" s="T1553">
                  <nts id="Seg_13325" n="HIAT:ip">(</nts>
                  <nts id="Seg_13326" n="HIAT:ip">(</nts>
                  <ats e="T1554" id="Seg_13327" n="HIAT:non-pho" s="T1553">BRK</ats>
                  <nts id="Seg_13328" n="HIAT:ip">)</nts>
                  <nts id="Seg_13329" n="HIAT:ip">)</nts>
                  <nts id="Seg_13330" n="HIAT:ip">.</nts>
                  <nts id="Seg_13331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1561" id="Seg_13332" n="sc" s="T1555">
               <ts e="T1561" id="Seg_13334" n="HIAT:u" s="T1555">
                  <ts e="T1556" id="Seg_13336" n="HIAT:w" s="T1555">Снова</ts>
                  <nts id="Seg_13337" n="HIAT:ip">,</nts>
                  <nts id="Seg_13338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1557" id="Seg_13340" n="HIAT:w" s="T1556">значит</ts>
                  <nts id="Seg_13341" n="HIAT:ip">,</nts>
                  <nts id="Seg_13342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1558" id="Seg_13344" n="HIAT:w" s="T1557">по-камасински</ts>
                  <nts id="Seg_13345" n="HIAT:ip">,</nts>
                  <nts id="Seg_13346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1559" id="Seg_13348" n="HIAT:w" s="T1558">он</ts>
                  <nts id="Seg_13349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1560" id="Seg_13351" n="HIAT:w" s="T1559">не</ts>
                  <nts id="Seg_13352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1561" id="Seg_13354" n="HIAT:w" s="T1560">записал</ts>
                  <nts id="Seg_13355" n="HIAT:ip">.</nts>
                  <nts id="Seg_13356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1575" id="Seg_13357" n="sc" s="T1565">
               <ts e="T1575" id="Seg_13359" n="HIAT:u" s="T1565">
                  <ts e="T1567" id="Seg_13361" n="HIAT:w" s="T1565">Это</ts>
                  <nts id="Seg_13362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1569" id="Seg_13364" n="HIAT:w" s="T1567">мы</ts>
                  <nts id="Seg_13365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1570" id="Seg_13367" n="HIAT:w" s="T1569">насчет</ts>
                  <nts id="Seg_13368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1572" id="Seg_13370" n="HIAT:w" s="T1570">Арпада</ts>
                  <nts id="Seg_13371" n="HIAT:ip">,</nts>
                  <nts id="Seg_13372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1573" id="Seg_13374" n="HIAT:w" s="T1572">кажется</ts>
                  <nts id="Seg_13375" n="HIAT:ip">,</nts>
                  <nts id="Seg_13376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1575" id="Seg_13378" n="HIAT:w" s="T1573">говорили</ts>
                  <nts id="Seg_13379" n="HIAT:ip">.</nts>
                  <nts id="Seg_13380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1585" id="Seg_13381" n="sc" s="T1579">
               <ts e="T1585" id="Seg_13383" n="HIAT:u" s="T1579">
                  <ts e="T1580" id="Seg_13385" n="HIAT:w" s="T1579">Да</ts>
                  <nts id="Seg_13386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1581" id="Seg_13388" n="HIAT:w" s="T1580">всё</ts>
                  <nts id="Seg_13389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1582" id="Seg_13391" n="HIAT:w" s="T1581">в</ts>
                  <nts id="Seg_13392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1583" id="Seg_13394" n="HIAT:w" s="T1582">порядке</ts>
                  <nts id="Seg_13395" n="HIAT:ip">,</nts>
                  <nts id="Seg_13396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1584" id="Seg_13398" n="HIAT:w" s="T1583">можно</ts>
                  <nts id="Seg_13399" n="HIAT:ip">,</nts>
                  <nts id="Seg_13400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1585" id="Seg_13402" n="HIAT:w" s="T1584">пожалуйста</ts>
                  <nts id="Seg_13403" n="HIAT:ip">.</nts>
                  <nts id="Seg_13404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1618" id="Seg_13405" n="sc" s="T1613">
               <ts e="T1618" id="Seg_13407" n="HIAT:u" s="T1613">
                  <ts e="T1614" id="Seg_13409" n="HIAT:w" s="T1613">Ну</ts>
                  <nts id="Seg_13410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1615" id="Seg_13412" n="HIAT:w" s="T1614">вот</ts>
                  <nts id="Seg_13413" n="HIAT:ip">,</nts>
                  <nts id="Seg_13414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1616" id="Seg_13416" n="HIAT:w" s="T1615">и</ts>
                  <nts id="Seg_13417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1617" id="Seg_13419" n="HIAT:w" s="T1616">повтори</ts>
                  <nts id="Seg_13420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1618" id="Seg_13422" n="HIAT:w" s="T1617">по-русски</ts>
                  <nts id="Seg_13423" n="HIAT:ip">.</nts>
                  <nts id="Seg_13424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1724" id="Seg_13425" n="sc" s="T1723">
               <ts e="T1724" id="Seg_13427" n="HIAT:u" s="T1723">
                  <ts e="T1724" id="Seg_13429" n="HIAT:w" s="T1723">Koŋ</ts>
                  <nts id="Seg_13430" n="HIAT:ip">?</nts>
                  <nts id="Seg_13431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1780" id="Seg_13432" n="sc" s="T1771">
               <ts e="T1772" id="Seg_13434" n="HIAT:u" s="T1771">
                  <ts e="T1772" id="Seg_13436" n="HIAT:w" s="T1771">Да</ts>
                  <nts id="Seg_13437" n="HIAT:ip">…</nts>
                  <nts id="Seg_13438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1780" id="Seg_13440" n="HIAT:u" s="T1772">
                  <ts e="T1773" id="Seg_13442" n="HIAT:w" s="T1772">Ну</ts>
                  <nts id="Seg_13443" n="HIAT:ip">,</nts>
                  <nts id="Seg_13444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1774" id="Seg_13446" n="HIAT:w" s="T1773">тут</ts>
                  <nts id="Seg_13447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1775" id="Seg_13449" n="HIAT:w" s="T1774">еще</ts>
                  <nts id="Seg_13450" n="HIAT:ip">,</nts>
                  <nts id="Seg_13451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1776" id="Seg_13453" n="HIAT:w" s="T1775">наверное</ts>
                  <nts id="Seg_13454" n="HIAT:ip">,</nts>
                  <nts id="Seg_13455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1777" id="Seg_13457" n="HIAT:w" s="T1776">что-то</ts>
                  <nts id="Seg_13458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1778" id="Seg_13460" n="HIAT:w" s="T1777">коротенькое</ts>
                  <nts id="Seg_13461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1779" id="Seg_13463" n="HIAT:w" s="T1778">успеешь</ts>
                  <nts id="Seg_13464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1780" id="Seg_13466" n="HIAT:w" s="T1779">сказать</ts>
                  <nts id="Seg_13467" n="HIAT:ip">.</nts>
                  <nts id="Seg_13468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1795" id="Seg_13469" n="sc" s="T1781">
               <ts e="T1788" id="Seg_13471" n="HIAT:u" s="T1781">
                  <ts e="T1782" id="Seg_13473" n="HIAT:w" s="T1781">Машина</ts>
                  <nts id="Seg_13474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1783" id="Seg_13476" n="HIAT:w" s="T1782">у</ts>
                  <nts id="Seg_13477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1784" id="Seg_13479" n="HIAT:w" s="T1783">нас</ts>
                  <nts id="Seg_13480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1785" id="Seg_13482" n="HIAT:w" s="T1784">пока</ts>
                  <nts id="Seg_13483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1786" id="Seg_13485" n="HIAT:w" s="T1785">что</ts>
                  <nts id="Seg_13486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1787" id="Seg_13488" n="HIAT:w" s="T1786">идет</ts>
                  <nts id="Seg_13489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_13490" n="HIAT:ip">(</nts>
                  <nts id="Seg_13491" n="HIAT:ip">(</nts>
                  <ats e="T1788" id="Seg_13492" n="HIAT:non-pho" s="T1787">…</ats>
                  <nts id="Seg_13493" n="HIAT:ip">)</nts>
                  <nts id="Seg_13494" n="HIAT:ip">)</nts>
                  <nts id="Seg_13495" n="HIAT:ip">.</nts>
                  <nts id="Seg_13496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1795" id="Seg_13498" n="HIAT:u" s="T1788">
                  <ts e="T1789" id="Seg_13500" n="HIAT:w" s="T1788">У</ts>
                  <nts id="Seg_13501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1790" id="Seg_13503" n="HIAT:w" s="T1789">него</ts>
                  <nts id="Seg_13504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1791" id="Seg_13506" n="HIAT:w" s="T1790">еще</ts>
                  <nts id="Seg_13507" n="HIAT:ip">,</nts>
                  <nts id="Seg_13508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1792" id="Seg_13510" n="HIAT:w" s="T1791">наверное</ts>
                  <nts id="Seg_13511" n="HIAT:ip">,</nts>
                  <nts id="Seg_13512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1793" id="Seg_13514" n="HIAT:w" s="T1792">несколько</ts>
                  <nts id="Seg_13515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1794" id="Seg_13517" n="HIAT:w" s="T1793">минут</ts>
                  <nts id="Seg_13518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1795" id="Seg_13520" n="HIAT:w" s="T1794">осталось</ts>
                  <nts id="Seg_13521" n="HIAT:ip">.</nts>
                  <nts id="Seg_13522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1798" id="Seg_13523" n="sc" s="T1796">
               <ts e="T1798" id="Seg_13525" n="HIAT:u" s="T1796">
                  <ts e="T1797" id="Seg_13527" n="HIAT:w" s="T1796">Хоть</ts>
                  <nts id="Seg_13528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1798" id="Seg_13530" n="HIAT:w" s="T1797">что</ts>
                  <nts id="Seg_13531" n="HIAT:ip">.</nts>
                  <nts id="Seg_13532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1849" id="Seg_13533" n="sc" s="T1846">
               <ts e="T1849" id="Seg_13535" n="HIAT:u" s="T1846">
                  <ts e="T1847" id="Seg_13537" n="HIAT:w" s="T1846">Ну</ts>
                  <nts id="Seg_13538" n="HIAT:ip">,</nts>
                  <nts id="Seg_13539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1848" id="Seg_13541" n="HIAT:w" s="T1847">и</ts>
                  <nts id="Seg_13542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1849" id="Seg_13544" n="HIAT:w" s="T1848">по-русски</ts>
                  <nts id="Seg_13545" n="HIAT:ip">…</nts>
                  <nts id="Seg_13546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T9" id="Seg_13547" n="sc" s="T4">
               <ts e="T5" id="Seg_13549" n="e" s="T4">Да, </ts>
               <ts e="T7" id="Seg_13551" n="e" s="T5">ну </ts>
               <ts e="T8" id="Seg_13553" n="e" s="T7">ничего, </ts>
               <ts e="T9" id="Seg_13555" n="e" s="T8">ничего. </ts>
            </ts>
            <ts e="T11" id="Seg_13556" n="sc" s="T10">
               <ts e="T11" id="Seg_13558" n="e" s="T10">((…)). </ts>
            </ts>
            <ts e="T13" id="Seg_13559" n="sc" s="T12">
               <ts e="T13" id="Seg_13561" n="e" s="T12">((…)). </ts>
            </ts>
            <ts e="T15" id="Seg_13562" n="sc" s="T14">
               <ts e="T15" id="Seg_13564" n="e" s="T14">((…)). </ts>
            </ts>
            <ts e="T67" id="Seg_13565" n="sc" s="T66">
               <ts e="T67" id="Seg_13567" n="e" s="T66">Ага. </ts>
            </ts>
            <ts e="T76" id="Seg_13568" n="sc" s="T70">
               <ts e="T71" id="Seg_13570" n="e" s="T70">((DMG)) </ts>
               <ts e="T72" id="Seg_13572" n="e" s="T71">зараза, </ts>
               <ts e="T73" id="Seg_13574" n="e" s="T72">в </ts>
               <ts e="T74" id="Seg_13576" n="e" s="T73">общем, </ts>
               <ts e="T75" id="Seg_13578" n="e" s="T74">такая, </ts>
               <ts e="T76" id="Seg_13580" n="e" s="T75">по-моему. </ts>
            </ts>
            <ts e="T85" id="Seg_13581" n="sc" s="T83">
               <ts e="T85" id="Seg_13583" n="e" s="T83">Да. </ts>
            </ts>
            <ts e="T102" id="Seg_13584" n="sc" s="T87">
               <ts e="T89" id="Seg_13586" n="e" s="T87">У нас </ts>
               <ts e="T91" id="Seg_13588" n="e" s="T89">очень </ts>
               <ts e="T93" id="Seg_13590" n="e" s="T91">сыро </ts>
               <ts e="T94" id="Seg_13592" n="e" s="T93">здесь. </ts>
               <ts e="T95" id="Seg_13594" n="e" s="T94">Потому </ts>
               <ts e="T97" id="Seg_13596" n="e" s="T95">что </ts>
               <ts e="T98" id="Seg_13598" n="e" s="T97">прямо </ts>
               <ts e="T100" id="Seg_13600" n="e" s="T98">море </ts>
               <ts e="T102" id="Seg_13602" n="e" s="T100">((…)). </ts>
            </ts>
            <ts e="T121" id="Seg_13603" n="sc" s="T109">
               <ts e="T111" id="Seg_13605" n="e" s="T109">Ты, </ts>
               <ts e="T113" id="Seg_13607" n="e" s="T111">наверное, </ts>
               <ts e="T115" id="Seg_13609" n="e" s="T113">море </ts>
               <ts e="T117" id="Seg_13611" n="e" s="T115">видела </ts>
               <ts e="T119" id="Seg_13613" n="e" s="T117">здесь, </ts>
               <ts e="T121" id="Seg_13615" n="e" s="T119">да? </ts>
            </ts>
            <ts e="T128" id="Seg_13616" n="sc" s="T123">
               <ts e="T124" id="Seg_13618" n="e" s="T123">На </ts>
               <ts e="T126" id="Seg_13620" n="e" s="T124">берегу </ts>
               <ts e="T128" id="Seg_13622" n="e" s="T126">была? </ts>
            </ts>
            <ts e="T135" id="Seg_13623" n="sc" s="T130">
               <ts e="T132" id="Seg_13625" n="e" s="T130">Ага, </ts>
               <ts e="T135" id="Seg_13627" n="e" s="T132">ага. </ts>
            </ts>
            <ts e="T145" id="Seg_13628" n="sc" s="T141">
               <ts e="T142" id="Seg_13630" n="e" s="T141">Ага-ага-ага, </ts>
               <ts e="T143" id="Seg_13632" n="e" s="T142">вот </ts>
               <ts e="T145" id="Seg_13634" n="e" s="T143">((…)). </ts>
            </ts>
            <ts e="T155" id="Seg_13635" n="sc" s="T151">
               <ts e="T155" id="Seg_13637" n="e" s="T151">Да… </ts>
            </ts>
            <ts e="T170" id="Seg_13638" n="sc" s="T161">
               <ts e="T162" id="Seg_13640" n="e" s="T161">Да, </ts>
               <ts e="T163" id="Seg_13642" n="e" s="T162">(вот </ts>
               <ts e="T164" id="Seg_13644" n="e" s="T163">и </ts>
               <ts e="T165" id="Seg_13646" n="e" s="T164">я=) </ts>
               <ts e="T166" id="Seg_13648" n="e" s="T165">от </ts>
               <ts e="T167" id="Seg_13650" n="e" s="T166">моря </ts>
               <ts e="T168" id="Seg_13652" n="e" s="T167">и </ts>
               <ts e="T169" id="Seg_13654" n="e" s="T168">сыро </ts>
               <ts e="T170" id="Seg_13656" n="e" s="T169">получается. </ts>
            </ts>
            <ts e="T175" id="Seg_13657" n="sc" s="T171">
               <ts e="T172" id="Seg_13659" n="e" s="T171">Как ((PAUSE)) </ts>
               <ts e="T175" id="Seg_13661" n="e" s="T172">море… </ts>
            </ts>
            <ts e="T186" id="Seg_13662" n="sc" s="T185">
               <ts e="T186" id="Seg_13664" n="e" s="T185">Да. </ts>
            </ts>
            <ts e="T226" id="Seg_13665" n="sc" s="T216">
               <ts e="T219" id="Seg_13667" n="e" s="T216">Да, </ts>
               <ts e="T221" id="Seg_13669" n="e" s="T219">это </ts>
               <ts e="T223" id="Seg_13671" n="e" s="T221">((…)) </ts>
               <ts e="T226" id="Seg_13673" n="e" s="T223">Ивановна. </ts>
            </ts>
            <ts e="T241" id="Seg_13674" n="sc" s="T237">
               <ts e="T239" id="Seg_13676" n="e" s="T237">Наподобие </ts>
               <ts e="T241" id="Seg_13678" n="e" s="T239">степного. </ts>
            </ts>
            <ts e="T290" id="Seg_13679" n="sc" s="T279">
               <ts e="T281" id="Seg_13681" n="e" s="T279">Ага, </ts>
               <ts e="T283" id="Seg_13683" n="e" s="T281">значит </ts>
               <ts e="T285" id="Seg_13685" n="e" s="T283">вот </ts>
               <ts e="T287" id="Seg_13687" n="e" s="T285">это </ts>
               <ts e="T289" id="Seg_13689" n="e" s="T287">на </ts>
               <ts e="T290" id="Seg_13691" n="e" s="T289">степном. </ts>
            </ts>
            <ts e="T298" id="Seg_13692" n="sc" s="T292">
               <ts e="T295" id="Seg_13694" n="e" s="T292">На </ts>
               <ts e="T298" id="Seg_13696" n="e" s="T295">степном. </ts>
            </ts>
            <ts e="T305" id="Seg_13697" n="sc" s="T301">
               <ts e="T302" id="Seg_13699" n="e" s="T301">Ну </ts>
               <ts e="T303" id="Seg_13701" n="e" s="T302">это </ts>
               <ts e="T304" id="Seg_13703" n="e" s="T303">и </ts>
               <ts e="T305" id="Seg_13705" n="e" s="T304">неинтересно. </ts>
            </ts>
            <ts e="T321" id="Seg_13706" n="sc" s="T308">
               <ts e="T309" id="Seg_13708" n="e" s="T308">Потому </ts>
               <ts e="T310" id="Seg_13710" n="e" s="T309">что </ts>
               <ts e="T312" id="Seg_13712" n="e" s="T310">(на </ts>
               <ts e="T313" id="Seg_13714" n="e" s="T312">этом=) </ts>
               <ts e="T315" id="Seg_13716" n="e" s="T313">на </ts>
               <ts e="T316" id="Seg_13718" n="e" s="T315">этом </ts>
               <ts e="T317" id="Seg_13720" n="e" s="T316">языке </ts>
               <ts e="T318" id="Seg_13722" n="e" s="T317">еще </ts>
               <ts e="T319" id="Seg_13724" n="e" s="T318">сейчас </ts>
               <ts e="T320" id="Seg_13726" n="e" s="T319">многие </ts>
               <ts e="T321" id="Seg_13728" n="e" s="T320">говорят. </ts>
            </ts>
            <ts e="T334" id="Seg_13729" n="sc" s="T323">
               <ts e="T325" id="Seg_13731" n="e" s="T323">Вот, </ts>
               <ts e="T326" id="Seg_13733" n="e" s="T325">а </ts>
               <ts e="T328" id="Seg_13735" n="e" s="T326">на </ts>
               <ts e="T330" id="Seg_13737" n="e" s="T328">камасинском </ts>
               <ts e="T331" id="Seg_13739" n="e" s="T330">никто </ts>
               <ts e="T333" id="Seg_13741" n="e" s="T331">не </ts>
               <ts e="T334" id="Seg_13743" n="e" s="T333">говорит. </ts>
            </ts>
            <ts e="T369" id="Seg_13744" n="sc" s="T366">
               <ts e="T369" id="Seg_13746" n="e" s="T366">Ага. </ts>
            </ts>
            <ts e="T374" id="Seg_13747" n="sc" s="T371">
               <ts e="T374" id="Seg_13749" n="e" s="T371">Ага. </ts>
            </ts>
            <ts e="T392" id="Seg_13750" n="sc" s="T389">
               <ts e="T392" id="Seg_13752" n="e" s="T389">Ага. </ts>
            </ts>
            <ts e="T645" id="Seg_13753" n="sc" s="T627">
               <ts e="T628" id="Seg_13755" n="e" s="T627">((DMG)) </ts>
               <ts e="T629" id="Seg_13757" n="e" s="T628">Таким </ts>
               <ts e="T630" id="Seg_13759" n="e" s="T629">образом, </ts>
               <ts e="T631" id="Seg_13761" n="e" s="T630">что </ts>
               <ts e="T632" id="Seg_13763" n="e" s="T631">он </ts>
               <ts e="T633" id="Seg_13765" n="e" s="T632">будет </ts>
               <ts e="T634" id="Seg_13767" n="e" s="T633">идти </ts>
               <ts e="T635" id="Seg_13769" n="e" s="T634">все </ts>
               <ts e="T636" id="Seg_13771" n="e" s="T635">время, </ts>
               <ts e="T637" id="Seg_13773" n="e" s="T636">все, </ts>
               <ts e="T638" id="Seg_13775" n="e" s="T637">что </ts>
               <ts e="T639" id="Seg_13777" n="e" s="T638">расскажешь, </ts>
               <ts e="T640" id="Seg_13779" n="e" s="T639">будет </ts>
               <ts e="T641" id="Seg_13781" n="e" s="T640">все </ts>
               <ts e="T642" id="Seg_13783" n="e" s="T641">время </ts>
               <ts e="T643" id="Seg_13785" n="e" s="T642">записывать, </ts>
               <ts e="T644" id="Seg_13787" n="e" s="T643">все </ts>
               <ts e="T645" id="Seg_13789" n="e" s="T644">хорошо. </ts>
            </ts>
            <ts e="T657" id="Seg_13790" n="sc" s="T646">
               <ts e="T647" id="Seg_13792" n="e" s="T646">Потому </ts>
               <ts e="T648" id="Seg_13794" n="e" s="T647">что </ts>
               <ts e="T649" id="Seg_13796" n="e" s="T648">ты </ts>
               <ts e="T650" id="Seg_13798" n="e" s="T649">сначала </ts>
               <ts e="T651" id="Seg_13800" n="e" s="T650">по-русски </ts>
               <ts e="T652" id="Seg_13802" n="e" s="T651">расскажешь, </ts>
               <ts e="T653" id="Seg_13804" n="e" s="T652">а </ts>
               <ts e="T654" id="Seg_13806" n="e" s="T653">потом </ts>
               <ts e="T655" id="Seg_13808" n="e" s="T654">просто </ts>
               <ts e="T656" id="Seg_13810" n="e" s="T655">повторишь </ts>
               <ts e="T657" id="Seg_13812" n="e" s="T656">по-камасински. </ts>
            </ts>
            <ts e="T664" id="Seg_13813" n="sc" s="T658">
               <ts e="T659" id="Seg_13815" n="e" s="T658">Чтобы </ts>
               <ts e="T660" id="Seg_13817" n="e" s="T659">тебе </ts>
               <ts e="T661" id="Seg_13819" n="e" s="T660">не </ts>
               <ts e="T662" id="Seg_13821" n="e" s="T661">несколько </ts>
               <ts e="T663" id="Seg_13823" n="e" s="T662">раз </ts>
               <ts e="T664" id="Seg_13825" n="e" s="T663">рассказывать. </ts>
            </ts>
            <ts e="T690" id="Seg_13826" n="sc" s="T680">
               <ts e="T682" id="Seg_13828" n="e" s="T680">Нет, </ts>
               <ts e="T684" id="Seg_13830" n="e" s="T682">нет-нет-нет, </ts>
               <ts e="T685" id="Seg_13832" n="e" s="T684">это </ts>
               <ts e="T686" id="Seg_13834" n="e" s="T685">мы </ts>
               <ts e="T687" id="Seg_13836" n="e" s="T686">вчера </ts>
               <ts e="T688" id="Seg_13838" n="e" s="T687">не </ts>
               <ts e="T689" id="Seg_13840" n="e" s="T688">говорили, </ts>
               <ts e="T690" id="Seg_13842" n="e" s="T689">нет-нет. </ts>
            </ts>
            <ts e="T695" id="Seg_13843" n="sc" s="T691">
               <ts e="T692" id="Seg_13845" n="e" s="T691">Вот, </ts>
               <ts e="T693" id="Seg_13847" n="e" s="T692">самое </ts>
               <ts e="T694" id="Seg_13849" n="e" s="T693">хорошо, </ts>
               <ts e="T695" id="Seg_13851" n="e" s="T694">хорошо. </ts>
            </ts>
            <ts e="T718" id="Seg_13852" n="sc" s="T696">
               <ts e="T697" id="Seg_13854" n="e" s="T696">Я </ts>
               <ts e="T698" id="Seg_13856" n="e" s="T697">даже </ts>
               <ts e="T699" id="Seg_13858" n="e" s="T698">на </ts>
               <ts e="T700" id="Seg_13860" n="e" s="T699">этот </ts>
               <ts e="T701" id="Seg_13862" n="e" s="T700">счет </ts>
               <ts e="T702" id="Seg_13864" n="e" s="T701">хорошо </ts>
               <ts e="T703" id="Seg_13866" n="e" s="T702">не </ts>
               <ts e="T704" id="Seg_13868" n="e" s="T703">помню, </ts>
               <ts e="T705" id="Seg_13870" n="e" s="T704">мы </ts>
               <ts e="T706" id="Seg_13872" n="e" s="T705">вообще </ts>
               <ts e="T707" id="Seg_13874" n="e" s="T706">об </ts>
               <ts e="T708" id="Seg_13876" n="e" s="T707">этом </ts>
               <ts e="T709" id="Seg_13878" n="e" s="T708">в </ts>
               <ts e="T710" id="Seg_13880" n="e" s="T709">Сибири </ts>
               <ts e="T711" id="Seg_13882" n="e" s="T710">говорили, </ts>
               <ts e="T712" id="Seg_13884" n="e" s="T711">не </ts>
               <ts e="T713" id="Seg_13886" n="e" s="T712">говорили, </ts>
               <ts e="T714" id="Seg_13888" n="e" s="T713">в </ts>
               <ts e="T715" id="Seg_13890" n="e" s="T714">Абалаково. </ts>
               <ts e="T716" id="Seg_13892" n="e" s="T715">Это </ts>
               <ts e="T717" id="Seg_13894" n="e" s="T716">очень </ts>
               <ts e="T718" id="Seg_13896" n="e" s="T717">интересно. </ts>
            </ts>
            <ts e="T742" id="Seg_13897" n="sc" s="T730">
               <ts e="T731" id="Seg_13899" n="e" s="T730">Да, </ts>
               <ts e="T732" id="Seg_13901" n="e" s="T731">нет, </ts>
               <ts e="T733" id="Seg_13903" n="e" s="T732">можно </ts>
               <ts e="T734" id="Seg_13905" n="e" s="T733">все </ts>
               <ts e="T735" id="Seg_13907" n="e" s="T734">время, </ts>
               <ts e="T736" id="Seg_13909" n="e" s="T735">сейчас </ts>
               <ts e="T737" id="Seg_13911" n="e" s="T736">всё </ts>
               <ts e="T738" id="Seg_13913" n="e" s="T737">запишет, </ts>
               <ts e="T739" id="Seg_13915" n="e" s="T738">что </ts>
               <ts e="T740" id="Seg_13917" n="e" s="T739">мы </ts>
               <ts e="T741" id="Seg_13919" n="e" s="T740">здесь </ts>
               <ts e="T742" id="Seg_13921" n="e" s="T741">говорим. </ts>
            </ts>
            <ts e="T1093" id="Seg_13922" n="sc" s="T1078">
               <ts e="T1080" id="Seg_13924" n="e" s="T1078">Да, </ts>
               <ts e="T1082" id="Seg_13926" n="e" s="T1080">сейчас </ts>
               <ts e="T1083" id="Seg_13928" n="e" s="T1082">можно </ts>
               <ts e="T1084" id="Seg_13930" n="e" s="T1083">(по-ка-) </ts>
               <ts e="T1085" id="Seg_13932" n="e" s="T1084">это </ts>
               <ts e="T1086" id="Seg_13934" n="e" s="T1085">ты </ts>
               <ts e="T1087" id="Seg_13936" n="e" s="T1086">совсем </ts>
               <ts e="T1088" id="Seg_13938" n="e" s="T1087">не </ts>
               <ts e="T1089" id="Seg_13940" n="e" s="T1088">рассказывала, </ts>
               <ts e="T1091" id="Seg_13942" n="e" s="T1089">очень </ts>
               <ts e="T1093" id="Seg_13944" n="e" s="T1091">интересно. </ts>
            </ts>
            <ts e="T1231" id="Seg_13945" n="sc" s="T1230">
               <ts e="T1231" id="Seg_13947" n="e" s="T1230">Да. </ts>
            </ts>
            <ts e="T1234" id="Seg_13948" n="sc" s="T1232">
               <ts e="T1233" id="Seg_13950" n="e" s="T1232">Печальная </ts>
               <ts e="T1234" id="Seg_13952" n="e" s="T1233">история. </ts>
            </ts>
            <ts e="T1252" id="Seg_13953" n="sc" s="T1251">
               <ts e="T1252" id="Seg_13955" n="e" s="T1251">Да. </ts>
            </ts>
            <ts e="T1256" id="Seg_13956" n="sc" s="T1253">
               <ts e="T1254" id="Seg_13958" n="e" s="T1253">Ну </ts>
               <ts e="T1255" id="Seg_13960" n="e" s="T1254">что </ts>
               <ts e="T1256" id="Seg_13962" n="e" s="T1255">ж. </ts>
            </ts>
            <ts e="T1261" id="Seg_13963" n="sc" s="T1257">
               <ts e="T1258" id="Seg_13965" n="e" s="T1257">Может, </ts>
               <ts e="T1259" id="Seg_13967" n="e" s="T1258">постараешься </ts>
               <ts e="T1260" id="Seg_13969" n="e" s="T1259">рассказать </ts>
               <ts e="T1261" id="Seg_13971" n="e" s="T1260">по-камасински? </ts>
            </ts>
            <ts e="T1485" id="Seg_13972" n="sc" s="T1479">
               <ts e="T1481" id="Seg_13974" n="e" s="T1479">Сейчас </ts>
               <ts e="T1482" id="Seg_13976" n="e" s="T1481">закрыто, </ts>
               <ts e="T1483" id="Seg_13978" n="e" s="T1482">сейчас </ts>
               <ts e="T1485" id="Seg_13980" n="e" s="T1483">пустим. </ts>
            </ts>
            <ts e="T1487" id="Seg_13981" n="sc" s="T1486">
               <ts e="T1487" id="Seg_13983" n="e" s="T1486">Ага. </ts>
            </ts>
            <ts e="T1497" id="Seg_13984" n="sc" s="T1493">
               <ts e="T1494" id="Seg_13986" n="e" s="T1493">Ну, </ts>
               <ts e="T1496" id="Seg_13988" n="e" s="T1494">это </ts>
               <ts e="T1497" id="Seg_13990" n="e" s="T1496">знаешь… </ts>
            </ts>
            <ts e="T1510" id="Seg_13991" n="sc" s="T1501">
               <ts e="T1502" id="Seg_13993" n="e" s="T1501">Ага, </ts>
               <ts e="T1504" id="Seg_13995" n="e" s="T1502">сейчас </ts>
               <ts e="T1505" id="Seg_13997" n="e" s="T1504">пустим, </ts>
               <ts e="T1507" id="Seg_13999" n="e" s="T1505">пустим, </ts>
               <ts e="T1509" id="Seg_14001" n="e" s="T1507">пустим </ts>
               <ts e="T1510" id="Seg_14003" n="e" s="T1509">сейчас. </ts>
            </ts>
            <ts e="T1517" id="Seg_14004" n="sc" s="T1515">
               <ts e="T1516" id="Seg_14006" n="e" s="T1515">Повтори </ts>
               <ts e="T1517" id="Seg_14008" n="e" s="T1516">по-русски. </ts>
            </ts>
            <ts e="T1525" id="Seg_14009" n="sc" s="T1521">
               <ts e="T1522" id="Seg_14011" n="e" s="T1521">Нет, </ts>
               <ts e="T1523" id="Seg_14013" n="e" s="T1522">есть </ts>
               <ts e="T1524" id="Seg_14015" n="e" s="T1523">еще </ts>
               <ts e="T1525" id="Seg_14017" n="e" s="T1524">((…)). </ts>
            </ts>
            <ts e="T1527" id="Seg_14018" n="sc" s="T1526">
               <ts e="T1527" id="Seg_14020" n="e" s="T1526">Ja. </ts>
            </ts>
            <ts e="T1537" id="Seg_14021" n="sc" s="T1528">
               <ts e="T1537" id="Seg_14023" n="e" s="T1528">((…)). </ts>
            </ts>
            <ts e="T1554" id="Seg_14024" n="sc" s="T1546">
               <ts e="T1553" id="Seg_14026" n="e" s="T1546">((…)). </ts>
               <ts e="T1554" id="Seg_14028" n="e" s="T1553">((BRK)). </ts>
            </ts>
            <ts e="T1561" id="Seg_14029" n="sc" s="T1555">
               <ts e="T1556" id="Seg_14031" n="e" s="T1555">Снова, </ts>
               <ts e="T1557" id="Seg_14033" n="e" s="T1556">значит, </ts>
               <ts e="T1558" id="Seg_14035" n="e" s="T1557">по-камасински, </ts>
               <ts e="T1559" id="Seg_14037" n="e" s="T1558">он </ts>
               <ts e="T1560" id="Seg_14039" n="e" s="T1559">не </ts>
               <ts e="T1561" id="Seg_14041" n="e" s="T1560">записал. </ts>
            </ts>
            <ts e="T1575" id="Seg_14042" n="sc" s="T1565">
               <ts e="T1567" id="Seg_14044" n="e" s="T1565">Это </ts>
               <ts e="T1569" id="Seg_14046" n="e" s="T1567">мы </ts>
               <ts e="T1570" id="Seg_14048" n="e" s="T1569">насчет </ts>
               <ts e="T1572" id="Seg_14050" n="e" s="T1570">Арпада, </ts>
               <ts e="T1573" id="Seg_14052" n="e" s="T1572">кажется, </ts>
               <ts e="T1575" id="Seg_14054" n="e" s="T1573">говорили. </ts>
            </ts>
            <ts e="T1585" id="Seg_14055" n="sc" s="T1579">
               <ts e="T1580" id="Seg_14057" n="e" s="T1579">Да </ts>
               <ts e="T1581" id="Seg_14059" n="e" s="T1580">всё </ts>
               <ts e="T1582" id="Seg_14061" n="e" s="T1581">в </ts>
               <ts e="T1583" id="Seg_14063" n="e" s="T1582">порядке, </ts>
               <ts e="T1584" id="Seg_14065" n="e" s="T1583">можно, </ts>
               <ts e="T1585" id="Seg_14067" n="e" s="T1584">пожалуйста. </ts>
            </ts>
            <ts e="T1618" id="Seg_14068" n="sc" s="T1613">
               <ts e="T1614" id="Seg_14070" n="e" s="T1613">Ну </ts>
               <ts e="T1615" id="Seg_14072" n="e" s="T1614">вот, </ts>
               <ts e="T1616" id="Seg_14074" n="e" s="T1615">и </ts>
               <ts e="T1617" id="Seg_14076" n="e" s="T1616">повтори </ts>
               <ts e="T1618" id="Seg_14078" n="e" s="T1617">по-русски. </ts>
            </ts>
            <ts e="T1724" id="Seg_14079" n="sc" s="T1723">
               <ts e="T1724" id="Seg_14081" n="e" s="T1723">Koŋ? </ts>
            </ts>
            <ts e="T1780" id="Seg_14082" n="sc" s="T1771">
               <ts e="T1772" id="Seg_14084" n="e" s="T1771">Да… </ts>
               <ts e="T1773" id="Seg_14086" n="e" s="T1772">Ну, </ts>
               <ts e="T1774" id="Seg_14088" n="e" s="T1773">тут </ts>
               <ts e="T1775" id="Seg_14090" n="e" s="T1774">еще, </ts>
               <ts e="T1776" id="Seg_14092" n="e" s="T1775">наверное, </ts>
               <ts e="T1777" id="Seg_14094" n="e" s="T1776">что-то </ts>
               <ts e="T1778" id="Seg_14096" n="e" s="T1777">коротенькое </ts>
               <ts e="T1779" id="Seg_14098" n="e" s="T1778">успеешь </ts>
               <ts e="T1780" id="Seg_14100" n="e" s="T1779">сказать. </ts>
            </ts>
            <ts e="T1795" id="Seg_14101" n="sc" s="T1781">
               <ts e="T1782" id="Seg_14103" n="e" s="T1781">Машина </ts>
               <ts e="T1783" id="Seg_14105" n="e" s="T1782">у </ts>
               <ts e="T1784" id="Seg_14107" n="e" s="T1783">нас </ts>
               <ts e="T1785" id="Seg_14109" n="e" s="T1784">пока </ts>
               <ts e="T1786" id="Seg_14111" n="e" s="T1785">что </ts>
               <ts e="T1787" id="Seg_14113" n="e" s="T1786">идет </ts>
               <ts e="T1788" id="Seg_14115" n="e" s="T1787">((…)). </ts>
               <ts e="T1789" id="Seg_14117" n="e" s="T1788">У </ts>
               <ts e="T1790" id="Seg_14119" n="e" s="T1789">него </ts>
               <ts e="T1791" id="Seg_14121" n="e" s="T1790">еще, </ts>
               <ts e="T1792" id="Seg_14123" n="e" s="T1791">наверное, </ts>
               <ts e="T1793" id="Seg_14125" n="e" s="T1792">несколько </ts>
               <ts e="T1794" id="Seg_14127" n="e" s="T1793">минут </ts>
               <ts e="T1795" id="Seg_14129" n="e" s="T1794">осталось. </ts>
            </ts>
            <ts e="T1798" id="Seg_14130" n="sc" s="T1796">
               <ts e="T1797" id="Seg_14132" n="e" s="T1796">Хоть </ts>
               <ts e="T1798" id="Seg_14134" n="e" s="T1797">что. </ts>
            </ts>
            <ts e="T1849" id="Seg_14135" n="sc" s="T1846">
               <ts e="T1847" id="Seg_14137" n="e" s="T1846">Ну, </ts>
               <ts e="T1848" id="Seg_14139" n="e" s="T1847">и </ts>
               <ts e="T1849" id="Seg_14141" n="e" s="T1848">по-русски… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T9" id="Seg_14142" s="T4">PKZ_19700819_09343-1a.KA.001 (002)</ta>
            <ta e="T11" id="Seg_14143" s="T10">PKZ_19700819_09343-1a.KA.002 (003)</ta>
            <ta e="T13" id="Seg_14144" s="T12">PKZ_19700819_09343-1a.KA.003 (004)</ta>
            <ta e="T15" id="Seg_14145" s="T14">PKZ_19700819_09343-1a.KA.004 (005)</ta>
            <ta e="T67" id="Seg_14146" s="T66">PKZ_19700819_09343-1a.KA.005 (013)</ta>
            <ta e="T76" id="Seg_14147" s="T70">PKZ_19700819_09343-1a.KA.006 (015)</ta>
            <ta e="T85" id="Seg_14148" s="T83">PKZ_19700819_09343-1a.KA.007 (017)</ta>
            <ta e="T94" id="Seg_14149" s="T87">PKZ_19700819_09343-1a.KA.008 (019)</ta>
            <ta e="T102" id="Seg_14150" s="T94">PKZ_19700819_09343-1a.KA.009 (020)</ta>
            <ta e="T121" id="Seg_14151" s="T109">PKZ_19700819_09343-1a.KA.010 (022)</ta>
            <ta e="T128" id="Seg_14152" s="T123">PKZ_19700819_09343-1a.KA.011 (024)</ta>
            <ta e="T135" id="Seg_14153" s="T130">PKZ_19700819_09343-1a.KA.012 (025)</ta>
            <ta e="T145" id="Seg_14154" s="T141">PKZ_19700819_09343-1a.KA.013 (028)</ta>
            <ta e="T155" id="Seg_14155" s="T151">PKZ_19700819_09343-1a.KA.014 (031)</ta>
            <ta e="T170" id="Seg_14156" s="T161">PKZ_19700819_09343-1a.KA.015 (032)</ta>
            <ta e="T175" id="Seg_14157" s="T171">PKZ_19700819_09343-1a.KA.016 (033)</ta>
            <ta e="T186" id="Seg_14158" s="T185">PKZ_19700819_09343-1a.KA.017 (036)</ta>
            <ta e="T226" id="Seg_14159" s="T216">PKZ_19700819_09343-1a.KA.018 (040)</ta>
            <ta e="T241" id="Seg_14160" s="T237">PKZ_19700819_09343-1a.KA.019 (044)</ta>
            <ta e="T290" id="Seg_14161" s="T279">PKZ_19700819_09343-1a.KA.020 (057)</ta>
            <ta e="T298" id="Seg_14162" s="T292">PKZ_19700819_09343-1a.KA.021 (059)</ta>
            <ta e="T305" id="Seg_14163" s="T301">PKZ_19700819_09343-1a.KA.022 (060)</ta>
            <ta e="T321" id="Seg_14164" s="T308">PKZ_19700819_09343-1a.KA.023 (062)</ta>
            <ta e="T334" id="Seg_14165" s="T323">PKZ_19700819_09343-1a.KA.024 (064)</ta>
            <ta e="T369" id="Seg_14166" s="T366">PKZ_19700819_09343-1a.KA.025 (072)</ta>
            <ta e="T374" id="Seg_14167" s="T371">PKZ_19700819_09343-1a.KA.026 (074)</ta>
            <ta e="T392" id="Seg_14168" s="T389">PKZ_19700819_09343-1a.KA.027 (077)</ta>
            <ta e="T645" id="Seg_14169" s="T627">PKZ_19700819_09343-1a.KA.028 (111)</ta>
            <ta e="T657" id="Seg_14170" s="T646">PKZ_19700819_09343-1a.KA.029 (112)</ta>
            <ta e="T664" id="Seg_14171" s="T658">PKZ_19700819_09343-1a.KA.030 (113)</ta>
            <ta e="T690" id="Seg_14172" s="T680">PKZ_19700819_09343-1a.KA.031 (116)</ta>
            <ta e="T695" id="Seg_14173" s="T691">PKZ_19700819_09343-1a.KA.032 (117)</ta>
            <ta e="T715" id="Seg_14174" s="T696">PKZ_19700819_09343-1a.KA.033 (118)</ta>
            <ta e="T718" id="Seg_14175" s="T715">PKZ_19700819_09343-1a.KA.034 (119)</ta>
            <ta e="T742" id="Seg_14176" s="T730">PKZ_19700819_09343-1a.KA.035 (122)</ta>
            <ta e="T1093" id="Seg_14177" s="T1078">PKZ_19700819_09343-1a.KA.036 (159)</ta>
            <ta e="T1231" id="Seg_14178" s="T1230">PKZ_19700819_09343-1a.KA.037 (175)</ta>
            <ta e="T1234" id="Seg_14179" s="T1232">PKZ_19700819_09343-1a.KA.038 (176)</ta>
            <ta e="T1252" id="Seg_14180" s="T1251">PKZ_19700819_09343-1a.KA.039 (180)</ta>
            <ta e="T1256" id="Seg_14181" s="T1253">PKZ_19700819_09343-1a.KA.040 (181)</ta>
            <ta e="T1261" id="Seg_14182" s="T1257">PKZ_19700819_09343-1a.KA.041 (182)</ta>
            <ta e="T1485" id="Seg_14183" s="T1479">PKZ_19700819_09343-1a.KA.042 (217)</ta>
            <ta e="T1487" id="Seg_14184" s="T1486">PKZ_19700819_09343-1a.KA.043 (219)</ta>
            <ta e="T1497" id="Seg_14185" s="T1493">PKZ_19700819_09343-1a.KA.044 (221)</ta>
            <ta e="T1510" id="Seg_14186" s="T1501">PKZ_19700819_09343-1a.KA.045 (223)</ta>
            <ta e="T1517" id="Seg_14187" s="T1515">PKZ_19700819_09343-1a.KA.046 (226)</ta>
            <ta e="T1525" id="Seg_14188" s="T1521">PKZ_19700819_09343-1a.KA.047 (228)</ta>
            <ta e="T1527" id="Seg_14189" s="T1526">PKZ_19700819_09343-1a.KA.048 (229)</ta>
            <ta e="T1537" id="Seg_14190" s="T1528">PKZ_19700819_09343-1a.KA.049 (230)</ta>
            <ta e="T1553" id="Seg_14191" s="T1546">PKZ_19700819_09343-1a.KA.050 (234)</ta>
            <ta e="T1554" id="Seg_14192" s="T1553">PKZ_19700819_09343-1a.KA.051 (236)</ta>
            <ta e="T1561" id="Seg_14193" s="T1555">PKZ_19700819_09343-1a.KA.052 (237)</ta>
            <ta e="T1575" id="Seg_14194" s="T1565">PKZ_19700819_09343-1a.KA.053 (240)</ta>
            <ta e="T1585" id="Seg_14195" s="T1579">PKZ_19700819_09343-1a.KA.054 (243)</ta>
            <ta e="T1618" id="Seg_14196" s="T1613">PKZ_19700819_09343-1a.KA.055 (248)</ta>
            <ta e="T1724" id="Seg_14197" s="T1723">PKZ_19700819_09343-1a.KA.056 (264)</ta>
            <ta e="T1772" id="Seg_14198" s="T1771">PKZ_19700819_09343-1a.KA.057 (271)</ta>
            <ta e="T1780" id="Seg_14199" s="T1772">PKZ_19700819_09343-1a.KA.058 (272)</ta>
            <ta e="T1788" id="Seg_14200" s="T1781">PKZ_19700819_09343-1a.KA.059 (273)</ta>
            <ta e="T1795" id="Seg_14201" s="T1788">PKZ_19700819_09343-1a.KA.060 (274)</ta>
            <ta e="T1798" id="Seg_14202" s="T1796">PKZ_19700819_09343-1a.KA.061 (275)</ta>
            <ta e="T1849" id="Seg_14203" s="T1846">PKZ_19700819_09343-1a.KA.062 (285)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T9" id="Seg_14204" s="T4">Да, ну ничего, ничего. </ta>
            <ta e="T11" id="Seg_14205" s="T10">((…)). </ta>
            <ta e="T13" id="Seg_14206" s="T12">((…)). </ta>
            <ta e="T15" id="Seg_14207" s="T14">((…)). </ta>
            <ta e="T67" id="Seg_14208" s="T66">Ага. </ta>
            <ta e="T76" id="Seg_14209" s="T70">((DMG)) зараза, в общем, такая, по-моему. </ta>
            <ta e="T85" id="Seg_14210" s="T83">Да. </ta>
            <ta e="T94" id="Seg_14211" s="T87">У нас очень сыро здесь. </ta>
            <ta e="T102" id="Seg_14212" s="T94">Потому что прямо море ((…)). </ta>
            <ta e="T121" id="Seg_14213" s="T109">Ты, наверное, море видела здесь, да? </ta>
            <ta e="T128" id="Seg_14214" s="T123">На берегу была? </ta>
            <ta e="T135" id="Seg_14215" s="T130">Ага, ага. </ta>
            <ta e="T145" id="Seg_14216" s="T141">Ага-ага-ага, вот ((…)). </ta>
            <ta e="T155" id="Seg_14217" s="T151">Да… </ta>
            <ta e="T170" id="Seg_14218" s="T161">Да, (вот и я=) от моря и сыро получается.</ta>
            <ta e="T175" id="Seg_14219" s="T171">Как ((PAUSE)) море…</ta>
            <ta e="T186" id="Seg_14220" s="T185">Да. </ta>
            <ta e="T226" id="Seg_14221" s="T216">Да, это ((…)) Ивановна. </ta>
            <ta e="T241" id="Seg_14222" s="T237">Наподобие степного. </ta>
            <ta e="T290" id="Seg_14223" s="T279">Ага, значит вот это на степном. </ta>
            <ta e="T298" id="Seg_14224" s="T292">На степном. </ta>
            <ta e="T305" id="Seg_14225" s="T301">Ну это и неинтересно. </ta>
            <ta e="T321" id="Seg_14226" s="T308">Потому что (на этом=) на этом языке еще сейчас многие говорят. </ta>
            <ta e="T334" id="Seg_14227" s="T323">Вот, а на камасинском никто не говорит. </ta>
            <ta e="T369" id="Seg_14228" s="T366">Ага. </ta>
            <ta e="T374" id="Seg_14229" s="T371">Ага. </ta>
            <ta e="T392" id="Seg_14230" s="T389">Ага. </ta>
            <ta e="T645" id="Seg_14231" s="T627">((DMG)) Таким образом, что он будет идти все время, все, что расскажешь, будет все время записывать, все хорошо. </ta>
            <ta e="T657" id="Seg_14232" s="T646">Потому что ты сначала по-русски расскажешь, а потом просто повторишь по-камасински. </ta>
            <ta e="T664" id="Seg_14233" s="T658">Чтобы тебе не ((PAUSE)) несколько раз рассказывать. </ta>
            <ta e="T690" id="Seg_14234" s="T680">Нет, нет-нет-нет, это мы вчера не говорили, нет-нет. </ta>
            <ta e="T695" id="Seg_14235" s="T691">Вот, самое хорошо, хорошо. </ta>
            <ta e="T715" id="Seg_14236" s="T696">Я даже на этот счет хорошо не помню, мы вообще об этом в Сибири говорили, не говорили, в Абалаково. </ta>
            <ta e="T718" id="Seg_14237" s="T715">Это очень интересно. </ta>
            <ta e="T742" id="Seg_14238" s="T730">Да, нет, можно все время, сейчас всё запишет, что мы здесь говорим. </ta>
            <ta e="T1093" id="Seg_14239" s="T1078">Да, сейчас можно (по-ка-) это ты совсем не рассказывала, очень интересно. </ta>
            <ta e="T1231" id="Seg_14240" s="T1230">Да. </ta>
            <ta e="T1234" id="Seg_14241" s="T1232">Печальная история. </ta>
            <ta e="T1252" id="Seg_14242" s="T1251">Да. </ta>
            <ta e="T1256" id="Seg_14243" s="T1253">Ну что ж. </ta>
            <ta e="T1261" id="Seg_14244" s="T1257">Может, постараешься рассказать по-камасински? </ta>
            <ta e="T1485" id="Seg_14245" s="T1479">Сейчас закрыто, сейчас пустим. </ta>
            <ta e="T1487" id="Seg_14246" s="T1486">Ага. </ta>
            <ta e="T1497" id="Seg_14247" s="T1493">Ну, это знаешь… </ta>
            <ta e="T1510" id="Seg_14248" s="T1501">Ага, сейчас пустим, пустим, пустим сейчас. </ta>
            <ta e="T1517" id="Seg_14249" s="T1515">Повтори по-русски. </ta>
            <ta e="T1525" id="Seg_14250" s="T1521">Нет, есть еще ((…)). </ta>
            <ta e="T1527" id="Seg_14251" s="T1526">Ja. </ta>
            <ta e="T1537" id="Seg_14252" s="T1528">((…)). </ta>
            <ta e="T1553" id="Seg_14253" s="T1546">((…)). </ta>
            <ta e="T1554" id="Seg_14254" s="T1553">((BRK)). </ta>
            <ta e="T1561" id="Seg_14255" s="T1555">Снова, значит, по-камасински, он не записал. </ta>
            <ta e="T1575" id="Seg_14256" s="T1565">Это мы насчет Арпада, кажется, говорили. </ta>
            <ta e="T1585" id="Seg_14257" s="T1579">Да всё в порядке, можно, пожалуйста. </ta>
            <ta e="T1618" id="Seg_14258" s="T1613">Ну вот, и повтори по-русски. </ta>
            <ta e="T1724" id="Seg_14259" s="T1723">Koŋ? </ta>
            <ta e="T1772" id="Seg_14260" s="T1771">Да… </ta>
            <ta e="T1780" id="Seg_14261" s="T1772">Ну, тут еще, наверное, что-то коротенькое успеешь сказать. </ta>
            <ta e="T1788" id="Seg_14262" s="T1781">Машина у нас пока что идет ((…)). </ta>
            <ta e="T1795" id="Seg_14263" s="T1788">У него еще, наверное, несколько минут осталось. </ta>
            <ta e="T1798" id="Seg_14264" s="T1796">Хоть что. </ta>
            <ta e="T1849" id="Seg_14265" s="T1846">Ну, и по-русски… </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T9" id="Seg_14266" s="T4">RUS:ext</ta>
            <ta e="T11" id="Seg_14267" s="T10">FIN:ext</ta>
            <ta e="T13" id="Seg_14268" s="T12">FIN:ext</ta>
            <ta e="T15" id="Seg_14269" s="T14">FIN:ext</ta>
            <ta e="T67" id="Seg_14270" s="T66">RUS:ext</ta>
            <ta e="T76" id="Seg_14271" s="T70">RUS:ext</ta>
            <ta e="T85" id="Seg_14272" s="T83">RUS:ext</ta>
            <ta e="T94" id="Seg_14273" s="T87">RUS:ext</ta>
            <ta e="T102" id="Seg_14274" s="T94">RUS:ext</ta>
            <ta e="T121" id="Seg_14275" s="T109">RUS:ext</ta>
            <ta e="T128" id="Seg_14276" s="T123">RUS:ext</ta>
            <ta e="T135" id="Seg_14277" s="T130">RUS:ext</ta>
            <ta e="T145" id="Seg_14278" s="T141">RUS:ext</ta>
            <ta e="T155" id="Seg_14279" s="T151">RUS:ext</ta>
            <ta e="T170" id="Seg_14280" s="T161">RUS:ext</ta>
            <ta e="T175" id="Seg_14281" s="T171">RUS:ext</ta>
            <ta e="T186" id="Seg_14282" s="T185">RUS:ext</ta>
            <ta e="T226" id="Seg_14283" s="T216">RUS:ext</ta>
            <ta e="T241" id="Seg_14284" s="T237">RUS:ext</ta>
            <ta e="T290" id="Seg_14285" s="T279">RUS:ext</ta>
            <ta e="T298" id="Seg_14286" s="T292">RUS:ext</ta>
            <ta e="T305" id="Seg_14287" s="T301">RUS:ext</ta>
            <ta e="T321" id="Seg_14288" s="T308">RUS:ext</ta>
            <ta e="T334" id="Seg_14289" s="T323">RUS:ext</ta>
            <ta e="T369" id="Seg_14290" s="T366">RUS:ext</ta>
            <ta e="T374" id="Seg_14291" s="T371">RUS:ext</ta>
            <ta e="T392" id="Seg_14292" s="T389">RUS:ext</ta>
            <ta e="T645" id="Seg_14293" s="T627">RUS:ext</ta>
            <ta e="T657" id="Seg_14294" s="T646">RUS:ext</ta>
            <ta e="T664" id="Seg_14295" s="T658">RUS:ext</ta>
            <ta e="T690" id="Seg_14296" s="T680">RUS:ext</ta>
            <ta e="T695" id="Seg_14297" s="T691">RUS:ext</ta>
            <ta e="T715" id="Seg_14298" s="T696">RUS:ext</ta>
            <ta e="T718" id="Seg_14299" s="T715">RUS:ext</ta>
            <ta e="T742" id="Seg_14300" s="T730">RUS:ext</ta>
            <ta e="T1093" id="Seg_14301" s="T1078">RUS:ext</ta>
            <ta e="T1231" id="Seg_14302" s="T1230">RUS:ext</ta>
            <ta e="T1234" id="Seg_14303" s="T1232">RUS:ext</ta>
            <ta e="T1252" id="Seg_14304" s="T1251">RUS:ext</ta>
            <ta e="T1256" id="Seg_14305" s="T1253">RUS:ext</ta>
            <ta e="T1261" id="Seg_14306" s="T1257">RUS:ext</ta>
            <ta e="T1485" id="Seg_14307" s="T1479">RUS:ext</ta>
            <ta e="T1487" id="Seg_14308" s="T1486">RUS:ext</ta>
            <ta e="T1497" id="Seg_14309" s="T1493">RUS:ext</ta>
            <ta e="T1510" id="Seg_14310" s="T1501">RUS:ext</ta>
            <ta e="T1517" id="Seg_14311" s="T1515">RUS:ext</ta>
            <ta e="T1524" id="Seg_14312" s="T1521">RUS:ext</ta>
            <ta e="T1525" id="Seg_14313" s="T1524">FIN:ext</ta>
            <ta e="T1537" id="Seg_14314" s="T1528">FIN:ext</ta>
            <ta e="T1553" id="Seg_14315" s="T1546">FIN:ext</ta>
            <ta e="T1561" id="Seg_14316" s="T1555">RUS:ext</ta>
            <ta e="T1575" id="Seg_14317" s="T1565">RUS:ext</ta>
            <ta e="T1585" id="Seg_14318" s="T1579">RUS:ext</ta>
            <ta e="T1618" id="Seg_14319" s="T1613">RUS:ext</ta>
            <ta e="T1772" id="Seg_14320" s="T1771">RUS:ext</ta>
            <ta e="T1780" id="Seg_14321" s="T1772">RUS:ext</ta>
            <ta e="T1788" id="Seg_14322" s="T1781">RUS:ext</ta>
            <ta e="T1795" id="Seg_14323" s="T1788">RUS:ext</ta>
            <ta e="T1798" id="Seg_14324" s="T1796">RUS:ext</ta>
            <ta e="T1849" id="Seg_14325" s="T1846">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA">
            <ta e="T9" id="Seg_14326" s="T4">Yes, weill, nothing, nothing.</ta>
            <ta e="T67" id="Seg_14327" s="T66">Yes.</ta>
            <ta e="T76" id="Seg_14328" s="T70">An infection, in general, in my opinion.</ta>
            <ta e="T85" id="Seg_14329" s="T83">Yes.</ta>
            <ta e="T94" id="Seg_14330" s="T87">At our [place] it is very humid.</ta>
            <ta e="T102" id="Seg_14331" s="T94">Because we are near the ocean.</ta>
            <ta e="T121" id="Seg_14332" s="T109">You have probably seen the ocean, right?</ta>
            <ta e="T128" id="Seg_14333" s="T123">Have you been to the shore?</ta>
            <ta e="T135" id="Seg_14334" s="T130">Yes, yes.</ta>
            <ta e="T145" id="Seg_14335" s="T141">Aha, aha, aha, well (...).</ta>
            <ta e="T155" id="Seg_14336" s="T151">Yes...</ta>
            <ta e="T170" id="Seg_14337" s="T161">Yes, there it will get humid from the ocean.</ta>
            <ta e="T175" id="Seg_14338" s="T171">Like... ocean...</ta>
            <ta e="T186" id="Seg_14339" s="T185">Yes.</ta>
            <ta e="T226" id="Seg_14340" s="T216">Yes, that is (...) Iwanowna.</ta>
            <ta e="T241" id="Seg_14341" s="T237">Like veld.</ta>
            <ta e="T290" id="Seg_14342" s="T279">Yes, then it is there in the veld.</ta>
            <ta e="T298" id="Seg_14343" s="T292">In the veld.</ta>
            <ta e="T305" id="Seg_14344" s="T301">Well, that is not interesting.</ta>
            <ta e="T321" id="Seg_14345" s="T308">Because (in this =) many people still speak that language. </ta>
            <ta e="T334" id="Seg_14346" s="T323">Well, nobody speaks in kamas.</ta>
            <ta e="T369" id="Seg_14347" s="T366">Yes.</ta>
            <ta e="T374" id="Seg_14348" s="T371">Yes.</ta>
            <ta e="T392" id="Seg_14349" s="T389">Yes.</ta>
            <ta e="T645" id="Seg_14350" s="T627">So, that he goes the whole time, everything, what you say, records the whole time, everything is fine.</ta>
            <ta e="T657" id="Seg_14351" s="T646">Because you first say it in russian und then repeat it in kamas.</ta>
            <ta e="T664" id="Seg_14352" s="T658">Therefore I won't tell you multiple times.</ta>
            <ta e="T690" id="Seg_14353" s="T680">No, no, no, no, we didn't say that yesterday, no, no.</ta>
            <ta e="T695" id="Seg_14354" s="T691">Well, good, good.</ta>
            <ta e="T715" id="Seg_14355" s="T696">I can't remember well, we spoke about sibiria in general and about Abalakovo, we didn't speak at all.</ta>
            <ta e="T718" id="Seg_14356" s="T715">That is very interesting.</ta>
            <ta e="T742" id="Seg_14357" s="T730">Yes, no, you can talk the whole time, now everything is recorded, what we say here.</ta>
            <ta e="T1093" id="Seg_14358" s="T1078">Yes, now you can say it, what you didn't say, very interesting.</ta>
            <ta e="T1231" id="Seg_14359" s="T1230">Yes.</ta>
            <ta e="T1234" id="Seg_14360" s="T1232">A sad story.</ta>
            <ta e="T1252" id="Seg_14361" s="T1251">Yes.</ta>
            <ta e="T1256" id="Seg_14362" s="T1253">Well then.</ta>
            <ta e="T1261" id="Seg_14363" s="T1257">Maybe you can tray saying it in kamas?</ta>
            <ta e="T1485" id="Seg_14364" s="T1479">Now it is over, let's go.</ta>
            <ta e="T1487" id="Seg_14365" s="T1486">Yes.</ta>
            <ta e="T1497" id="Seg_14366" s="T1493">Well, you know that...</ta>
            <ta e="T1510" id="Seg_14367" s="T1501">Yes, let's got, let's go, let's go.</ta>
            <ta e="T1517" id="Seg_14368" s="T1515">Repeat it in russian.</ta>
            <ta e="T1525" id="Seg_14369" s="T1521">No, there is still (...).</ta>
            <ta e="T1561" id="Seg_14370" s="T1555">Again, well he didn't record in kamas.</ta>
            <ta e="T1575" id="Seg_14371" s="T1565">This is what we supposedly said about Arapad.</ta>
            <ta e="T1585" id="Seg_14372" s="T1579">Yes, everything is finde.</ta>
            <ta e="T1618" id="Seg_14373" s="T1613">Now, repeat it in russian.</ta>
            <ta e="T1772" id="Seg_14374" s="T1771">Yes...</ta>
            <ta e="T1780" id="Seg_14375" s="T1772">Well, there is probably time for you to say something short.</ta>
            <ta e="T1788" id="Seg_14376" s="T1781">The machine is still on (...).</ta>
            <ta e="T1795" id="Seg_14377" s="T1788">There is probabply still a few minutes of time left.</ta>
            <ta e="T1798" id="Seg_14378" s="T1796">At least.</ta>
            <ta e="T1849" id="Seg_14379" s="T1846">Now, in russian.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T9" id="Seg_14380" s="T4">Ja, nun, nichts, nichts.</ta>
            <ta e="T67" id="Seg_14381" s="T66">Ja.</ta>
            <ta e="T76" id="Seg_14382" s="T70">Eine Infektion, im allgemeinen, meiner Meinung nach. </ta>
            <ta e="T85" id="Seg_14383" s="T83">Ja.</ta>
            <ta e="T94" id="Seg_14384" s="T87">Bei uns ist es sehr feucht. </ta>
            <ta e="T102" id="Seg_14385" s="T94">Weil wir nahe am Meer sind. </ta>
            <ta e="T121" id="Seg_14386" s="T109">Du hast wahrscheinlich das Meer gesehen, ja?</ta>
            <ta e="T128" id="Seg_14387" s="T123">Warst du am Ufer?</ta>
            <ta e="T135" id="Seg_14388" s="T130">Ja, ja.</ta>
            <ta e="T145" id="Seg_14389" s="T141">Aha, aha, aha, nun (…).</ta>
            <ta e="T155" id="Seg_14390" s="T151">Ja…</ta>
            <ta e="T170" id="Seg_14391" s="T161">Ja, da wird es feucht vom Meer. </ta>
            <ta e="T175" id="Seg_14392" s="T171">Wie… Meer…</ta>
            <ta e="T186" id="Seg_14393" s="T185">Ja.</ta>
            <ta e="T226" id="Seg_14394" s="T216">Ja, das ist (…) Iwanowna.</ta>
            <ta e="T241" id="Seg_14395" s="T237">Wie Steppe.</ta>
            <ta e="T290" id="Seg_14396" s="T279">Ja, dann ist das in der Steppe.</ta>
            <ta e="T298" id="Seg_14397" s="T292">In der Steppe.</ta>
            <ta e="T305" id="Seg_14398" s="T301">Nun, das ist nicht interessant.</ta>
            <ta e="T321" id="Seg_14399" s="T308">Weil (in diesem =) noch viele Leute diese Sprache sprechen.</ta>
            <ta e="T334" id="Seg_14400" s="T323">Nun, auf Kamassisch spricht niemand. </ta>
            <ta e="T369" id="Seg_14401" s="T366">Ja.</ta>
            <ta e="T374" id="Seg_14402" s="T371">Ja.</ta>
            <ta e="T392" id="Seg_14403" s="T389">Ja.</ta>
            <ta e="T645" id="Seg_14404" s="T627">So, dass er die ganze Zeit geht, alles, was du erzählst, die ganze Zeit aufzeichnet, alles in Ordnung ist.</ta>
            <ta e="T657" id="Seg_14405" s="T646">Weil du es zu erst auf Russisch erzählst und es dann auf Kamassisch wiederholst. </ta>
            <ta e="T664" id="Seg_14406" s="T658">Deswegen sage ich es dir nicht mehrmals. </ta>
            <ta e="T690" id="Seg_14407" s="T680">Nein, nein, nein, nein, das haben wir gestern nicht gesagt, nein, nein.</ta>
            <ta e="T695" id="Seg_14408" s="T691">Nun, gut, gut. </ta>
            <ta e="T715" id="Seg_14409" s="T696">Ich kann mich nicht gut erinnern, wir haben allgemein über Sibirien gesprochen und über Abalakovo haben wir gar nicht gesprochen. </ta>
            <ta e="T718" id="Seg_14410" s="T715">Das ist sehr interessant. </ta>
            <ta e="T742" id="Seg_14411" s="T730">Ja, nein, du kannst die ganze Zeit reden, jetzt wird alles aufgezeichnet, was wir hier sagen.</ta>
            <ta e="T1093" id="Seg_14412" s="T1078">Ja, jetzt kannst du das sagen, was du nicht gesagt hast, sehr interessant. </ta>
            <ta e="T1231" id="Seg_14413" s="T1230">Ja.</ta>
            <ta e="T1234" id="Seg_14414" s="T1232">Eine traurige Geschichte.</ta>
            <ta e="T1252" id="Seg_14415" s="T1251">Ja.</ta>
            <ta e="T1256" id="Seg_14416" s="T1253">Na dann.</ta>
            <ta e="T1261" id="Seg_14417" s="T1257">Vielleicht versuchst du es auf Kamassisch zu sagen?</ta>
            <ta e="T1485" id="Seg_14418" s="T1479">Jetzt ist es zu ende, jetzt lass uns gehen. </ta>
            <ta e="T1487" id="Seg_14419" s="T1486">Ja.</ta>
            <ta e="T1497" id="Seg_14420" s="T1493">Nun, du weißt das …</ta>
            <ta e="T1510" id="Seg_14421" s="T1501">Ja, lass uns jetzt gehen, lass uns gehen, lass uns jetzt gehen.</ta>
            <ta e="T1517" id="Seg_14422" s="T1515">Wiederhole es auf Russisch. </ta>
            <ta e="T1525" id="Seg_14423" s="T1521">Nein, es gibt immer noch (…). </ta>
            <ta e="T1561" id="Seg_14424" s="T1555">Noch mal, also er hat nicht auf Kamassisch aufgenommen. </ta>
            <ta e="T1575" id="Seg_14425" s="T1565">Das scheinen wir über Arpad gesagt zu haben.</ta>
            <ta e="T1585" id="Seg_14426" s="T1579">Ja, es ist alles in Ordnung.</ta>
            <ta e="T1618" id="Seg_14427" s="T1613">Nun, und auf Russisch wiederholen.</ta>
            <ta e="T1772" id="Seg_14428" s="T1771">Ja…</ta>
            <ta e="T1780" id="Seg_14429" s="T1772">Nun, es gibt wahrscheinlich noch Zeit für dich etwas Kurzes zu sagen. </ta>
            <ta e="T1788" id="Seg_14430" s="T1781">Die Maschine läuft noch (…).</ta>
            <ta e="T1795" id="Seg_14431" s="T1788">Es gibt wahrscheinlich noch ein paar Minuten Zeit.</ta>
            <ta e="T1798" id="Seg_14432" s="T1796">Mindestens. </ta>
            <ta e="T1849" id="Seg_14433" s="T1846">Nun, auf Russisch. </ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T257" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
            <conversion-tli id="T1306" />
            <conversion-tli id="T1307" />
            <conversion-tli id="T1308" />
            <conversion-tli id="T1309" />
            <conversion-tli id="T1310" />
            <conversion-tli id="T1311" />
            <conversion-tli id="T1312" />
            <conversion-tli id="T1313" />
            <conversion-tli id="T1314" />
            <conversion-tli id="T1315" />
            <conversion-tli id="T1316" />
            <conversion-tli id="T1317" />
            <conversion-tli id="T1318" />
            <conversion-tli id="T1319" />
            <conversion-tli id="T1320" />
            <conversion-tli id="T1321" />
            <conversion-tli id="T1322" />
            <conversion-tli id="T1323" />
            <conversion-tli id="T1324" />
            <conversion-tli id="T1325" />
            <conversion-tli id="T1326" />
            <conversion-tli id="T1327" />
            <conversion-tli id="T1328" />
            <conversion-tli id="T1329" />
            <conversion-tli id="T1330" />
            <conversion-tli id="T1331" />
            <conversion-tli id="T1332" />
            <conversion-tli id="T1333" />
            <conversion-tli id="T1334" />
            <conversion-tli id="T1335" />
            <conversion-tli id="T1336" />
            <conversion-tli id="T1337" />
            <conversion-tli id="T1338" />
            <conversion-tli id="T1339" />
            <conversion-tli id="T1340" />
            <conversion-tli id="T1341" />
            <conversion-tli id="T1342" />
            <conversion-tli id="T1343" />
            <conversion-tli id="T1344" />
            <conversion-tli id="T1345" />
            <conversion-tli id="T1346" />
            <conversion-tli id="T1347" />
            <conversion-tli id="T1348" />
            <conversion-tli id="T1349" />
            <conversion-tli id="T1350" />
            <conversion-tli id="T1351" />
            <conversion-tli id="T1352" />
            <conversion-tli id="T1353" />
            <conversion-tli id="T1354" />
            <conversion-tli id="T1355" />
            <conversion-tli id="T1356" />
            <conversion-tli id="T1357" />
            <conversion-tli id="T1358" />
            <conversion-tli id="T1359" />
            <conversion-tli id="T1360" />
            <conversion-tli id="T1361" />
            <conversion-tli id="T1362" />
            <conversion-tli id="T1363" />
            <conversion-tli id="T1364" />
            <conversion-tli id="T1365" />
            <conversion-tli id="T1366" />
            <conversion-tli id="T1367" />
            <conversion-tli id="T1368" />
            <conversion-tli id="T1369" />
            <conversion-tli id="T1370" />
            <conversion-tli id="T1371" />
            <conversion-tli id="T1372" />
            <conversion-tli id="T1373" />
            <conversion-tli id="T1374" />
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
            <conversion-tli id="T1413" />
            <conversion-tli id="T1414" />
            <conversion-tli id="T1415" />
            <conversion-tli id="T1416" />
            <conversion-tli id="T1417" />
            <conversion-tli id="T1418" />
            <conversion-tli id="T1419" />
            <conversion-tli id="T1420" />
            <conversion-tli id="T1421" />
            <conversion-tli id="T1422" />
            <conversion-tli id="T1423" />
            <conversion-tli id="T1424" />
            <conversion-tli id="T1425" />
            <conversion-tli id="T1426" />
            <conversion-tli id="T1427" />
            <conversion-tli id="T1428" />
            <conversion-tli id="T1429" />
            <conversion-tli id="T1430" />
            <conversion-tli id="T1431" />
            <conversion-tli id="T1432" />
            <conversion-tli id="T1433" />
            <conversion-tli id="T1434" />
            <conversion-tli id="T1435" />
            <conversion-tli id="T1436" />
            <conversion-tli id="T1437" />
            <conversion-tli id="T1438" />
            <conversion-tli id="T1439" />
            <conversion-tli id="T1440" />
            <conversion-tli id="T1441" />
            <conversion-tli id="T1442" />
            <conversion-tli id="T1443" />
            <conversion-tli id="T1444" />
            <conversion-tli id="T1445" />
            <conversion-tli id="T1446" />
            <conversion-tli id="T1447" />
            <conversion-tli id="T1448" />
            <conversion-tli id="T1449" />
            <conversion-tli id="T1450" />
            <conversion-tli id="T1451" />
            <conversion-tli id="T1452" />
            <conversion-tli id="T1453" />
            <conversion-tli id="T1454" />
            <conversion-tli id="T1455" />
            <conversion-tli id="T1456" />
            <conversion-tli id="T1457" />
            <conversion-tli id="T1458" />
            <conversion-tli id="T1459" />
            <conversion-tli id="T1460" />
            <conversion-tli id="T1461" />
            <conversion-tli id="T1462" />
            <conversion-tli id="T1463" />
            <conversion-tli id="T1464" />
            <conversion-tli id="T1465" />
            <conversion-tli id="T1466" />
            <conversion-tli id="T1467" />
            <conversion-tli id="T1468" />
            <conversion-tli id="T1469" />
            <conversion-tli id="T1470" />
            <conversion-tli id="T1471" />
            <conversion-tli id="T1472" />
            <conversion-tli id="T1473" />
            <conversion-tli id="T1474" />
            <conversion-tli id="T1475" />
            <conversion-tli id="T1476" />
            <conversion-tli id="T1477" />
            <conversion-tli id="T1478" />
            <conversion-tli id="T1479" />
            <conversion-tli id="T1480" />
            <conversion-tli id="T1481" />
            <conversion-tli id="T1482" />
            <conversion-tli id="T1483" />
            <conversion-tli id="T1484" />
            <conversion-tli id="T1485" />
            <conversion-tli id="T1486" />
            <conversion-tli id="T1487" />
            <conversion-tli id="T1488" />
            <conversion-tli id="T1489" />
            <conversion-tli id="T1490" />
            <conversion-tli id="T1491" />
            <conversion-tli id="T1492" />
            <conversion-tli id="T1493" />
            <conversion-tli id="T1494" />
            <conversion-tli id="T1495" />
            <conversion-tli id="T1496" />
            <conversion-tli id="T1497" />
            <conversion-tli id="T1498" />
            <conversion-tli id="T1499" />
            <conversion-tli id="T1500" />
            <conversion-tli id="T1501" />
            <conversion-tli id="T1502" />
            <conversion-tli id="T1503" />
            <conversion-tli id="T1504" />
            <conversion-tli id="T1505" />
            <conversion-tli id="T1506" />
            <conversion-tli id="T1507" />
            <conversion-tli id="T1508" />
            <conversion-tli id="T1509" />
            <conversion-tli id="T1510" />
            <conversion-tli id="T1511" />
            <conversion-tli id="T1512" />
            <conversion-tli id="T1513" />
            <conversion-tli id="T1514" />
            <conversion-tli id="T1515" />
            <conversion-tli id="T1516" />
            <conversion-tli id="T1517" />
            <conversion-tli id="T1518" />
            <conversion-tli id="T1519" />
            <conversion-tli id="T1520" />
            <conversion-tli id="T1521" />
            <conversion-tli id="T1522" />
            <conversion-tli id="T1523" />
            <conversion-tli id="T1524" />
            <conversion-tli id="T1525" />
            <conversion-tli id="T1526" />
            <conversion-tli id="T1527" />
            <conversion-tli id="T1528" />
            <conversion-tli id="T1529" />
            <conversion-tli id="T1530" />
            <conversion-tli id="T1531" />
            <conversion-tli id="T1532" />
            <conversion-tli id="T1533" />
            <conversion-tli id="T1534" />
            <conversion-tli id="T1535" />
            <conversion-tli id="T1536" />
            <conversion-tli id="T1537" />
            <conversion-tli id="T1538" />
            <conversion-tli id="T1539" />
            <conversion-tli id="T1540" />
            <conversion-tli id="T1541" />
            <conversion-tli id="T1542" />
            <conversion-tli id="T1543" />
            <conversion-tli id="T1544" />
            <conversion-tli id="T1545" />
            <conversion-tli id="T1546" />
            <conversion-tli id="T1547" />
            <conversion-tli id="T1548" />
            <conversion-tli id="T1549" />
            <conversion-tli id="T1550" />
            <conversion-tli id="T1551" />
            <conversion-tli id="T1552" />
            <conversion-tli id="T1553" />
            <conversion-tli id="T1554" />
            <conversion-tli id="T1555" />
            <conversion-tli id="T1556" />
            <conversion-tli id="T1557" />
            <conversion-tli id="T1558" />
            <conversion-tli id="T1559" />
            <conversion-tli id="T1560" />
            <conversion-tli id="T1561" />
            <conversion-tli id="T1562" />
            <conversion-tli id="T1563" />
            <conversion-tli id="T1564" />
            <conversion-tli id="T1565" />
            <conversion-tli id="T1566" />
            <conversion-tli id="T1567" />
            <conversion-tli id="T1568" />
            <conversion-tli id="T1569" />
            <conversion-tli id="T1570" />
            <conversion-tli id="T1571" />
            <conversion-tli id="T1572" />
            <conversion-tli id="T1573" />
            <conversion-tli id="T1574" />
            <conversion-tli id="T1575" />
            <conversion-tli id="T1576" />
            <conversion-tli id="T1577" />
            <conversion-tli id="T1578" />
            <conversion-tli id="T1579" />
            <conversion-tli id="T1580" />
            <conversion-tli id="T1581" />
            <conversion-tli id="T1582" />
            <conversion-tli id="T1583" />
            <conversion-tli id="T1584" />
            <conversion-tli id="T1585" />
            <conversion-tli id="T1586" />
            <conversion-tli id="T1587" />
            <conversion-tli id="T1588" />
            <conversion-tli id="T1589" />
            <conversion-tli id="T1590" />
            <conversion-tli id="T1591" />
            <conversion-tli id="T1592" />
            <conversion-tli id="T1593" />
            <conversion-tli id="T1594" />
            <conversion-tli id="T1595" />
            <conversion-tli id="T1596" />
            <conversion-tli id="T1597" />
            <conversion-tli id="T1598" />
            <conversion-tli id="T1599" />
            <conversion-tli id="T1600" />
            <conversion-tli id="T1601" />
            <conversion-tli id="T1602" />
            <conversion-tli id="T1603" />
            <conversion-tli id="T1604" />
            <conversion-tli id="T1605" />
            <conversion-tli id="T1606" />
            <conversion-tli id="T1607" />
            <conversion-tli id="T1608" />
            <conversion-tli id="T1609" />
            <conversion-tli id="T1610" />
            <conversion-tli id="T1611" />
            <conversion-tli id="T1612" />
            <conversion-tli id="T1613" />
            <conversion-tli id="T1614" />
            <conversion-tli id="T1615" />
            <conversion-tli id="T1616" />
            <conversion-tli id="T1617" />
            <conversion-tli id="T1618" />
            <conversion-tli id="T1619" />
            <conversion-tli id="T1620" />
            <conversion-tli id="T1621" />
            <conversion-tli id="T1622" />
            <conversion-tli id="T1623" />
            <conversion-tli id="T1624" />
            <conversion-tli id="T1625" />
            <conversion-tli id="T1626" />
            <conversion-tli id="T1627" />
            <conversion-tli id="T1628" />
            <conversion-tli id="T1629" />
            <conversion-tli id="T1630" />
            <conversion-tli id="T1631" />
            <conversion-tli id="T1632" />
            <conversion-tli id="T1633" />
            <conversion-tli id="T1634" />
            <conversion-tli id="T1635" />
            <conversion-tli id="T1636" />
            <conversion-tli id="T1637" />
            <conversion-tli id="T1638" />
            <conversion-tli id="T1639" />
            <conversion-tli id="T1640" />
            <conversion-tli id="T1641" />
            <conversion-tli id="T1642" />
            <conversion-tli id="T1643" />
            <conversion-tli id="T1644" />
            <conversion-tli id="T1645" />
            <conversion-tli id="T1646" />
            <conversion-tli id="T1647" />
            <conversion-tli id="T1648" />
            <conversion-tli id="T1649" />
            <conversion-tli id="T1650" />
            <conversion-tli id="T1651" />
            <conversion-tli id="T1652" />
            <conversion-tli id="T1653" />
            <conversion-tli id="T1654" />
            <conversion-tli id="T1655" />
            <conversion-tli id="T1656" />
            <conversion-tli id="T1657" />
            <conversion-tli id="T1658" />
            <conversion-tli id="T1659" />
            <conversion-tli id="T1660" />
            <conversion-tli id="T1661" />
            <conversion-tli id="T1662" />
            <conversion-tli id="T1663" />
            <conversion-tli id="T1664" />
            <conversion-tli id="T1665" />
            <conversion-tli id="T1666" />
            <conversion-tli id="T1667" />
            <conversion-tli id="T1668" />
            <conversion-tli id="T1669" />
            <conversion-tli id="T1670" />
            <conversion-tli id="T1671" />
            <conversion-tli id="T1672" />
            <conversion-tli id="T1673" />
            <conversion-tli id="T1674" />
            <conversion-tli id="T1675" />
            <conversion-tli id="T1676" />
            <conversion-tli id="T1677" />
            <conversion-tli id="T1678" />
            <conversion-tli id="T1679" />
            <conversion-tli id="T1680" />
            <conversion-tli id="T1681" />
            <conversion-tli id="T1682" />
            <conversion-tli id="T1683" />
            <conversion-tli id="T1684" />
            <conversion-tli id="T1685" />
            <conversion-tli id="T1686" />
            <conversion-tli id="T1687" />
            <conversion-tli id="T1688" />
            <conversion-tli id="T1689" />
            <conversion-tli id="T1690" />
            <conversion-tli id="T1691" />
            <conversion-tli id="T1692" />
            <conversion-tli id="T1693" />
            <conversion-tli id="T1694" />
            <conversion-tli id="T1695" />
            <conversion-tli id="T1696" />
            <conversion-tli id="T1697" />
            <conversion-tli id="T1698" />
            <conversion-tli id="T1699" />
            <conversion-tli id="T1700" />
            <conversion-tli id="T1701" />
            <conversion-tli id="T1702" />
            <conversion-tli id="T1703" />
            <conversion-tli id="T1704" />
            <conversion-tli id="T1705" />
            <conversion-tli id="T1706" />
            <conversion-tli id="T1707" />
            <conversion-tli id="T1708" />
            <conversion-tli id="T1709" />
            <conversion-tli id="T1710" />
            <conversion-tli id="T1711" />
            <conversion-tli id="T1712" />
            <conversion-tli id="T1713" />
            <conversion-tli id="T1714" />
            <conversion-tli id="T1715" />
            <conversion-tli id="T1716" />
            <conversion-tli id="T1717" />
            <conversion-tli id="T1718" />
            <conversion-tli id="T1719" />
            <conversion-tli id="T1720" />
            <conversion-tli id="T1721" />
            <conversion-tli id="T1722" />
            <conversion-tli id="T1723" />
            <conversion-tli id="T1724" />
            <conversion-tli id="T1725" />
            <conversion-tli id="T1726" />
            <conversion-tli id="T1727" />
            <conversion-tli id="T1728" />
            <conversion-tli id="T1729" />
            <conversion-tli id="T1730" />
            <conversion-tli id="T1731" />
            <conversion-tli id="T1732" />
            <conversion-tli id="T1733" />
            <conversion-tli id="T1734" />
            <conversion-tli id="T1735" />
            <conversion-tli id="T1736" />
            <conversion-tli id="T1737" />
            <conversion-tli id="T1738" />
            <conversion-tli id="T1739" />
            <conversion-tli id="T1740" />
            <conversion-tli id="T1741" />
            <conversion-tli id="T1742" />
            <conversion-tli id="T1743" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1744" />
            <conversion-tli id="T1745" />
            <conversion-tli id="T1746" />
            <conversion-tli id="T1747" />
            <conversion-tli id="T1748" />
            <conversion-tli id="T1749" />
            <conversion-tli id="T1750" />
            <conversion-tli id="T1751" />
            <conversion-tli id="T1752" />
            <conversion-tli id="T1753" />
            <conversion-tli id="T1754" />
            <conversion-tli id="T1755" />
            <conversion-tli id="T1756" />
            <conversion-tli id="T1757" />
            <conversion-tli id="T1758" />
            <conversion-tli id="T1759" />
            <conversion-tli id="T1760" />
            <conversion-tli id="T1761" />
            <conversion-tli id="T1762" />
            <conversion-tli id="T1763" />
            <conversion-tli id="T1764" />
            <conversion-tli id="T1765" />
            <conversion-tli id="T1766" />
            <conversion-tli id="T1767" />
            <conversion-tli id="T1768" />
            <conversion-tli id="T1769" />
            <conversion-tli id="T1770" />
            <conversion-tli id="T1771" />
            <conversion-tli id="T1772" />
            <conversion-tli id="T1773" />
            <conversion-tli id="T1774" />
            <conversion-tli id="T1775" />
            <conversion-tli id="T1776" />
            <conversion-tli id="T1777" />
            <conversion-tli id="T1778" />
            <conversion-tli id="T1779" />
            <conversion-tli id="T1780" />
            <conversion-tli id="T1781" />
            <conversion-tli id="T1782" />
            <conversion-tli id="T1783" />
            <conversion-tli id="T1784" />
            <conversion-tli id="T1785" />
            <conversion-tli id="T1786" />
            <conversion-tli id="T1787" />
            <conversion-tli id="T1788" />
            <conversion-tli id="T1789" />
            <conversion-tli id="T1790" />
            <conversion-tli id="T1791" />
            <conversion-tli id="T1792" />
            <conversion-tli id="T1793" />
            <conversion-tli id="T1794" />
            <conversion-tli id="T1795" />
            <conversion-tli id="T1796" />
            <conversion-tli id="T1797" />
            <conversion-tli id="T1798" />
            <conversion-tli id="T1799" />
            <conversion-tli id="T1800" />
            <conversion-tli id="T1801" />
            <conversion-tli id="T1802" />
            <conversion-tli id="T1803" />
            <conversion-tli id="T1804" />
            <conversion-tli id="T1805" />
            <conversion-tli id="T1806" />
            <conversion-tli id="T1807" />
            <conversion-tli id="T1808" />
            <conversion-tli id="T1809" />
            <conversion-tli id="T1810" />
            <conversion-tli id="T1811" />
            <conversion-tli id="T1812" />
            <conversion-tli id="T1813" />
            <conversion-tli id="T1814" />
            <conversion-tli id="T1815" />
            <conversion-tli id="T1816" />
            <conversion-tli id="T1817" />
            <conversion-tli id="T1818" />
            <conversion-tli id="T1819" />
            <conversion-tli id="T1820" />
            <conversion-tli id="T1821" />
            <conversion-tli id="T1822" />
            <conversion-tli id="T1823" />
            <conversion-tli id="T1824" />
            <conversion-tli id="T1825" />
            <conversion-tli id="T1826" />
            <conversion-tli id="T1827" />
            <conversion-tli id="T1828" />
            <conversion-tli id="T1829" />
            <conversion-tli id="T1830" />
            <conversion-tli id="T1831" />
            <conversion-tli id="T1832" />
            <conversion-tli id="T1833" />
            <conversion-tli id="T1834" />
            <conversion-tli id="T1835" />
            <conversion-tli id="T1836" />
            <conversion-tli id="T1837" />
            <conversion-tli id="T1838" />
            <conversion-tli id="T1839" />
            <conversion-tli id="T1840" />
            <conversion-tli id="T1841" />
            <conversion-tli id="T1842" />
            <conversion-tli id="T1843" />
            <conversion-tli id="T1844" />
            <conversion-tli id="T1845" />
            <conversion-tli id="T1846" />
            <conversion-tli id="T1847" />
            <conversion-tli id="T1848" />
            <conversion-tli id="T1849" />
            <conversion-tli id="T1850" />
            <conversion-tli id="T1851" />
            <conversion-tli id="T1852" />
            <conversion-tli id="T1853" />
            <conversion-tli id="T1854" />
            <conversion-tli id="T1855" />
            <conversion-tli id="T1856" />
            <conversion-tli id="T1857" />
            <conversion-tli id="T1858" />
            <conversion-tli id="T1859" />
            <conversion-tli id="T1860" />
            <conversion-tli id="T1861" />
            <conversion-tli id="T1862" />
            <conversion-tli id="T1863" />
            <conversion-tli id="T1864" />
            <conversion-tli id="T1865" />
            <conversion-tli id="T1866" />
            <conversion-tli id="T1867" />
            <conversion-tli id="T1868" />
            <conversion-tli id="T1869" />
            <conversion-tli id="T1870" />
            <conversion-tli id="T1871" />
            <conversion-tli id="T1872" />
            <conversion-tli id="T1873" />
            <conversion-tli id="T1874" />
            <conversion-tli id="T1875" />
            <conversion-tli id="T1876" />
            <conversion-tli id="T1877" />
            <conversion-tli id="T1878" />
            <conversion-tli id="T572" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
