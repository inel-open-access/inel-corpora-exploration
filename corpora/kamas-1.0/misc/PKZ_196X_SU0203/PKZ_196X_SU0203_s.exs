<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5C3D429F-3AE3-7AED-6F6E-EE78C92BFF34">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0203.wav" />
         <referenced-file url="PKZ_196X_SU0203.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0203\PKZ_196X_SU0203.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1535</ud-information>
            <ud-information attribute-name="# HIAT:w">871</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">40</ud-information>
            <ud-information attribute-name="# e">889</ud-information>
            <ud-information attribute-name="# HIAT:u">306</ud-information>
            <ud-information attribute-name="# sc">26</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MAK">
            <abbreviation>MAK</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.03" type="appl" />
         <tli id="T1" time="0.709" type="appl" />
         <tli id="T2" time="1.389" type="appl" />
         <tli id="T3" time="2.068" type="appl" />
         <tli id="T4" time="2.748" type="appl" />
         <tli id="T5" time="3.427" type="appl" />
         <tli id="T6" time="4.107" type="appl" />
         <tli id="T7" time="4.786" type="appl" />
         <tli id="T8" time="5.466" type="appl" />
         <tli id="T9" time="6.145" type="appl" />
         <tli id="T10" time="6.825" type="appl" />
         <tli id="T11" time="7.504" type="appl" />
         <tli id="T12" time="8.184" type="appl" />
         <tli id="T13" time="8.83993967316138" />
         <tli id="T14" time="9.279936670468055" />
         <tli id="T15" time="10.222" type="appl" />
         <tli id="T16" time="10.443" type="intp" />
         <tli id="T17" time="10.664" type="appl" />
         <tli id="T18" time="11.676" type="appl" />
         <tli id="T19" time="12.688" type="appl" />
         <tli id="T20" time="13.561" type="appl" />
         <tli id="T21" time="14.435" type="appl" />
         <tli id="T22" time="15.64655988907222" />
         <tli id="T23" time="16.694" type="appl" />
         <tli id="T24" time="17.628" type="appl" />
         <tli id="T25" time="18.908" type="appl" />
         <tli id="T26" time="19.97" type="appl" />
         <tli id="T27" time="21.333187748202427" />
         <tli id="T28" time="22.062" type="appl" />
         <tli id="T29" time="22.766" type="appl" />
         <tli id="T30" time="23.471" type="appl" />
         <tli id="T31" time="24.175" type="appl" />
         <tli id="T32" time="24.88" type="appl" />
         <tli id="T33" time="25.65" type="appl" />
         <tli id="T34" time="26.419" type="appl" />
         <tli id="T35" time="27.215" type="appl" />
         <tli id="T36" time="28.011" type="appl" />
         <tli id="T37" time="33.11310735791296" />
         <tli id="T38" time="34.101" type="appl" />
         <tli id="T39" time="36.92641466790415" />
         <tli id="T40" time="37.914" type="appl" />
         <tli id="T41" time="38.728" type="appl" />
         <tli id="T42" time="39.541" type="appl" />
         <tli id="T43" time="40.592" type="appl" />
         <tli id="T44" time="41.412" type="appl" />
         <tli id="T45" time="42.232" type="appl" />
         <tli id="T46" time="43.137" type="appl" />
         <tli id="T47" time="43.952" type="appl" />
         <tli id="T48" time="44.767" type="appl" />
         <tli id="T49" time="45.578" type="appl" />
         <tli id="T50" time="46.389" type="appl" />
         <tli id="T51" time="47.2" type="appl" />
         <tli id="T52" time="47.993" type="appl" />
         <tli id="T53" time="48.787" type="appl" />
         <tli id="T54" time="49.61966137808458" />
         <tli id="T55" time="51.022" type="appl" />
         <tli id="T56" time="51.798" type="appl" />
         <tli id="T57" time="52.77963981328707" />
         <tli id="T58" time="54.221" type="appl" />
         <tli id="T59" time="55.551" type="appl" />
         <tli id="T60" time="57.486274360234226" />
         <tli id="T61" time="58.83" type="appl" />
         <tli id="T62" time="60.37958794858419" />
         <tli id="T63" time="61.301" type="appl" />
         <tli id="T64" time="62.188" type="appl" />
         <tli id="T65" time="63.026236553595545" />
         <tli id="T66" time="63.626" type="appl" />
         <tli id="T67" time="64.177" type="appl" />
         <tli id="T68" time="64.728" type="appl" />
         <tli id="T69" time="65.279" type="appl" />
         <tli id="T70" time="65.685" type="appl" />
         <tli id="T71" time="66.091" type="appl" />
         <tli id="T72" time="66.497" type="appl" />
         <tli id="T73" time="66.903" type="appl" />
         <tli id="T74" time="67.309" type="appl" />
         <tli id="T75" time="67.866" type="appl" />
         <tli id="T76" time="68.422" type="appl" />
         <tli id="T77" time="68.978" type="appl" />
         <tli id="T78" time="69.535" type="appl" />
         <tli id="T79" time="69.985" type="appl" />
         <tli id="T80" time="70.435" type="appl" />
         <tli id="T81" time="70.886" type="appl" />
         <tli id="T82" time="71.81950987855774" />
         <tli id="T83" time="73.20616708219089" />
         <tli id="T84" time="74.75282319393555" />
         <tli id="T85" time="75.54" type="appl" />
         <tli id="T86" time="76.133" type="appl" />
         <tli id="T87" time="76.725" type="appl" />
         <tli id="T88" time="77.318" type="appl" />
         <tli id="T89" time="77.91" type="appl" />
         <tli id="T90" time="78.503" type="appl" />
         <tli id="T91" time="79.095" type="appl" />
         <tli id="T92" time="79.688" type="appl" />
         <tli id="T93" time="80.2666657738796" />
         <tli id="T94" time="80.744" type="appl" />
         <tli id="T95" time="81.206" type="appl" />
         <tli id="T96" time="81.668" type="appl" />
         <tli id="T97" time="82.129" type="appl" />
         <tli id="T98" time="82.78" type="appl" />
         <tli id="T99" time="83.213" type="appl" />
         <tli id="T100" time="83.647" type="appl" />
         <tli id="T101" time="84.08" type="appl" />
         <tli id="T102" time="84.514" type="appl" />
         <tli id="T103" time="85.095" type="appl" />
         <tli id="T104" time="85.675" type="appl" />
         <tli id="T105" time="86.256" type="appl" />
         <tli id="T106" time="86.913" type="appl" />
         <tli id="T107" time="87.38" type="appl" />
         <tli id="T108" time="87.848" type="appl" />
         <tli id="T109" time="88.315" type="appl" />
         <tli id="T110" time="89.53272233073707" />
         <tli id="T111" time="90.21" type="appl" />
         <tli id="T112" time="90.634" type="appl" />
         <tli id="T113" time="91.058" type="appl" />
         <tli id="T114" time="91.71937407492781" />
         <tli id="T115" time="92.9193658857642" />
         <tli id="T116" time="95.79267961060022" />
         <tli id="T117" time="96.913" type="appl" />
         <tli id="T118" time="97.833" type="appl" />
         <tli id="T119" time="99.09932371157159" />
         <tli id="T120" time="100.008" type="appl" />
         <tli id="T121" time="100.659" type="appl" />
         <tli id="T122" time="101.58597340847143" />
         <tli id="T123" time="102.317" type="appl" />
         <tli id="T124" time="102.943" type="appl" />
         <tli id="T125" time="103.568" type="appl" />
         <tli id="T126" time="104.045" type="appl" />
         <tli id="T127" time="104.523" type="appl" />
         <tli id="T128" time="105.0" type="appl" />
         <tli id="T129" time="105.634" type="appl" />
         <tli id="T130" time="106.266" type="appl" />
         <tli id="T131" time="106.899" type="appl" />
         <tli id="T132" time="107.532" type="appl" />
         <tli id="T133" time="108.165" type="appl" />
         <tli id="T134" time="108.797" type="appl" />
         <tli id="T135" time="109.43" type="appl" />
         <tli id="T136" time="110.363" type="appl" />
         <tli id="T137" time="111.002" type="appl" />
         <tli id="T138" time="111.64" type="appl" />
         <tli id="T139" time="112.31799652356064" />
         <tli id="T140" time="113.222" type="appl" />
         <tli id="T141" time="114.9458822357832" />
         <tli id="T142" time="116.329" type="appl" />
         <tli id="T143" time="117.3" type="appl" />
         <tli id="T144" time="118.93252169622853" />
         <tli id="T145" time="119.798" type="appl" />
         <tli id="T146" time="120.564" type="appl" />
         <tli id="T147" time="121.644" type="appl" />
         <tli id="T911" time="122.06442857142858" type="intp" />
         <tli id="T148" time="122.625" type="appl" />
         <tli id="T149" time="124.645816040044" />
         <tli id="T150" time="125.978" type="appl" />
         <tli id="T151" time="126.836" type="appl" />
         <tli id="T152" time="127.649" type="appl" />
         <tli id="T153" time="128.461" type="appl" />
         <tli id="T154" time="129.8657804171823" />
         <tli id="T155" time="130.729" type="appl" />
         <tli id="T156" time="131.386" type="appl" />
         <tli id="T157" time="132.21909769065587" />
         <tli id="T158" time="133.079" type="appl" />
         <tli id="T159" time="133.817" type="appl" />
         <tli id="T160" time="134.554" type="appl" />
         <tli id="T161" time="135.56166602563437" />
         <tli id="T162" time="143.11902330575305" />
         <tli id="T163" time="144.108" type="appl" />
         <tli id="T164" time="144.885" type="appl" />
         <tli id="T165" time="145.662" type="appl" />
         <tli id="T166" time="146.439" type="appl" />
         <tli id="T167" time="147.216" type="appl" />
         <tli id="T168" time="147.993" type="appl" />
         <tli id="T169" time="148.77" type="appl" />
         <tli id="T170" time="152.18562809873907" />
         <tli id="T171" time="152.988" type="appl" />
         <tli id="T172" time="153.27228734966312" />
         <tli id="T173" time="155.85226974296137" />
         <tli id="T174" time="156.732" type="appl" />
         <tli id="T175" time="157.466" type="appl" />
         <tli id="T176" time="158.2" type="appl" />
         <tli id="T177" time="158.934" type="appl" />
         <tli id="T178" time="159.669" type="appl" />
         <tli id="T179" time="160.403" type="appl" />
         <tli id="T180" time="161.137" type="appl" />
         <tli id="T181" time="161.871" type="appl" />
         <tli id="T182" time="162.477" type="appl" />
         <tli id="T183" time="163.084" type="appl" />
         <tli id="T184" time="163.69" type="appl" />
         <tli id="T185" time="164.261" type="appl" />
         <tli id="T186" time="164.822" type="appl" />
         <tli id="T187" time="165.384" type="appl" />
         <tli id="T188" time="166.20553242201083" />
         <tli id="T189" time="167.3255247787915" />
         <tli id="T190" time="169.9588401414602" />
         <tli id="T191" time="170.735" type="appl" />
         <tli id="T192" time="171.317" type="appl" />
         <tli id="T193" time="171.898" type="appl" />
         <tli id="T194" time="172.81882062395363" />
         <tli id="T195" time="173.687" type="appl" />
         <tli id="T196" time="174.543" type="appl" />
         <tli id="T197" time="175.331" type="appl" />
         <tli id="T198" time="176.118" type="appl" />
         <tli id="T199" time="176.906" type="appl" />
         <tli id="T200" time="177.693" type="appl" />
         <tli id="T201" time="178.481" type="appl" />
         <tli id="T202" time="179.597" type="appl" />
         <tli id="T203" time="181.07876425521073" />
         <tli id="T204" time="181.796" type="appl" />
         <tli id="T205" time="182.441" type="appl" />
         <tli id="T206" time="184.57207374897888" />
         <tli id="T207" time="185.898" type="appl" />
         <tli id="T208" time="187.05" type="appl" />
         <tli id="T209" time="188.61871279996603" />
         <tli id="T210" time="189.255" type="appl" />
         <tli id="T211" time="189.811" type="appl" />
         <tli id="T212" time="190.78536468064286" />
         <tli id="T213" time="191.425" type="appl" />
         <tli id="T912" time="191.7439230769231" type="intp" />
         <tli id="T214" time="192.116" type="appl" />
         <tli id="T215" time="192.807" type="appl" />
         <tli id="T216" time="193.61201205727966" />
         <tli id="T217" time="194.18" type="appl" />
         <tli id="T218" time="194.861" type="appl" />
         <tli id="T219" time="195.543" type="appl" />
         <tli id="T220" time="196.723" type="appl" />
         <tli id="T221" time="197.43198598844214" />
         <tli id="T222" time="198.112" type="appl" />
         <tli id="T223" time="198.683" type="appl" />
         <tli id="T224" time="199.253" type="appl" />
         <tli id="T225" time="200.26" type="appl" />
         <tli id="T226" time="201.67862367456868" />
         <tli id="T227" time="202.944" type="appl" />
         <tli id="T228" time="204.137" type="appl" />
         <tli id="T229" time="204.954" type="appl" />
         <tli id="T230" time="206.03192729943626" />
         <tli id="T231" time="206.788" type="appl" />
         <tli id="T232" time="207.456" type="appl" />
         <tli id="T233" time="208.12499895308358" />
         <tli id="T234" time="208.69" type="appl" />
         <tli id="T235" time="209.255" type="appl" />
         <tli id="T236" time="209.82" type="appl" />
         <tli id="T237" time="210.385" type="appl" />
         <tli id="T238" time="211.224" type="appl" />
         <tli id="T239" time="212.063" type="appl" />
         <tli id="T240" time="212.902" type="appl" />
         <tli id="T241" time="217.63851475869265" />
         <tli id="T242" time="218.694" type="appl" />
         <tli id="T243" time="219.539" type="appl" />
         <tli id="T244" time="220.384" type="appl" />
         <tli id="T245" time="221.228" type="appl" />
         <tli id="T246" time="222.073" type="appl" />
         <tli id="T247" time="222.918" type="appl" />
         <tli id="T248" time="223.763" type="appl" />
         <tli id="T249" time="224.713" type="appl" />
         <tli id="T250" time="225.65846002778247" />
         <tli id="T910" time="226.09523001389124" type="intp" />
         <tli id="T251" time="226.532" type="appl" />
         <tli id="T252" time="227.411" type="appl" />
         <tli id="T253" time="228.29" type="appl" />
         <tli id="T254" time="229.40510112606054" />
         <tli id="T255" time="230.503" type="appl" />
         <tli id="T256" time="231.404" type="appl" />
         <tli id="T257" time="232.214" type="appl" />
         <tli id="T258" time="233.35174085947799" />
         <tli id="T259" time="234.834" type="appl" />
         <tli id="T260" time="237.70504448434554" />
         <tli id="T261" time="238.566" type="appl" />
         <tli id="T262" time="239.42503274654436" />
         <tli id="T263" time="241.79168326236058" />
         <tli id="T264" time="242.746" type="appl" />
         <tli id="T265" time="243.535" type="appl" />
         <tli id="T266" time="244.324" type="appl" />
         <tli id="T267" time="245.16" type="appl" />
         <tli id="T268" time="245.9249883885748" />
         <tli id="T269" time="246.914" type="appl" />
         <tli id="T270" time="247.831" type="appl" />
         <tli id="T271" time="248.748" type="appl" />
         <tli id="T272" time="249.664" type="appl" />
         <tli id="T273" time="250.581" type="appl" />
         <tli id="T274" time="251.498" type="appl" />
         <tli id="T275" time="252.415" type="appl" />
         <tli id="T276" time="252.89" type="appl" />
         <tli id="T277" time="253.355" type="appl" />
         <tli id="T278" time="253.82" type="appl" />
         <tli id="T279" time="258.0715721627076" />
         <tli id="T280" time="259.95822595418923" />
         <tli id="T281" time="261.152" type="appl" />
         <tli id="T282" time="262.3115432276628" />
         <tli id="T283" time="263.146" type="appl" />
         <tli id="T284" time="263.799" type="appl" />
         <tli id="T285" time="264.452" type="appl" />
         <tli id="T286" time="265.066003596929" />
         <tli id="T287" time="265.604" type="appl" />
         <tli id="T288" time="266.102" type="appl" />
         <tli id="T289" time="266.599" type="appl" />
         <tli id="T290" time="267.097" type="appl" />
         <tli id="T291" time="267.84" type="appl" />
         <tli id="T292" time="268.97149777780476" />
         <tli id="T293" time="270.03" type="appl" />
         <tli id="T294" time="270.957" type="appl" />
         <tli id="T295" time="272.0314768954375" />
         <tli id="T296" time="273.23146870627386" />
         <tli id="T297" time="274.27812823017007" />
         <tli id="T298" time="274.971" type="appl" />
         <tli id="T299" time="275.54" type="appl" />
         <tli id="T300" time="276.11" type="appl" />
         <tli id="T301" time="276.679" type="appl" />
         <tli id="T302" time="277.249" type="appl" />
         <tli id="T303" time="277.952" type="appl" />
         <tli id="T304" time="278.654" type="appl" />
         <tli id="T305" time="279.357" type="appl" />
         <tli id="T306" time="280.059" type="appl" />
         <tli id="T307" time="282.10474148529187" />
         <tli id="T308" time="283.161" type="appl" />
         <tli id="T309" time="283.934" type="appl" />
         <tli id="T310" time="284.708" type="appl" />
         <tli id="T311" time="285.67138381194445" />
         <tli id="T312" time="286.312" type="appl" />
         <tli id="T313" time="286.867" type="appl" />
         <tli id="T314" time="287.422" type="appl" />
         <tli id="T315" time="288.8646953530035" />
         <tli id="T316" time="289.592" type="appl" />
         <tli id="T317" time="290.348" type="appl" />
         <tli id="T318" time="291.105" type="appl" />
         <tli id="T319" time="291.876" type="appl" />
         <tli id="T320" time="292.7046691476799" />
         <tli id="T321" time="293.984" type="appl" />
         <tli id="T322" time="295.127" type="appl" />
         <tli id="T323" time="296.27" type="appl" />
         <tli id="T324" time="298.06463256941583" />
         <tli id="T325" time="298.837" type="appl" />
         <tli id="T326" time="299.487" type="appl" />
         <tli id="T327" time="300.138" type="appl" />
         <tli id="T328" time="300.788" type="appl" />
         <tli id="T329" time="301.8246069100365" />
         <tli id="T330" time="303.091264932586" />
         <tli id="T331" time="304.202" type="appl" />
         <tli id="T332" time="305.241" type="appl" />
         <tli id="T333" time="306.6179075322107" />
         <tli id="T334" time="307.302" type="appl" />
         <tli id="T335" time="307.937" type="appl" />
         <tli id="T336" time="308.571" type="appl" />
         <tli id="T337" time="309.30455586424995" />
         <tli id="T338" time="309.858" type="appl" />
         <tli id="T339" time="310.562" type="appl" />
         <tli id="T340" time="311.266" type="appl" />
         <tli id="T341" time="312.2712022854844" />
         <tli id="T342" time="313.422" type="appl" />
         <tli id="T343" time="314.496" type="appl" />
         <tli id="T344" time="316.17784229187396" />
         <tli id="T345" time="317.17" type="appl" />
         <tli id="T346" time="317.973" type="appl" />
         <tli id="T347" time="318.775" type="appl" />
         <tli id="T348" time="319.578" type="appl" />
         <tli id="T349" time="320.166" type="appl" />
         <tli id="T350" time="320.754" type="appl" />
         <tli id="T351" time="321.34866637936267" />
         <tli id="T352" time="321.926" type="appl" />
         <tli id="T353" time="322.509" type="appl" />
         <tli id="T354" time="323.388" type="appl" />
         <tli id="T355" time="324.267" type="appl" />
         <tli id="T356" time="325.145" type="appl" />
         <tli id="T357" time="326.024" type="appl" />
         <tli id="T358" time="326.903" type="appl" />
         <tli id="T359" time="328.1444272943813" />
         <tli id="T360" time="328.977" type="appl" />
         <tli id="T361" time="329.571" type="appl" />
         <tli id="T362" time="330.165" type="appl" />
         <tli id="T363" time="330.80440914173516" />
         <tli id="T364" time="331.899" type="appl" />
         <tli id="T365" time="332.599" type="appl" />
         <tli id="T366" time="333.281" type="appl" />
         <tli id="T367" time="333.964" type="appl" />
         <tli id="T368" time="335.361" type="appl" />
         <tli id="T369" time="336.22" type="appl" />
         <tli id="T370" time="337.46436369187717" />
         <tli id="T371" time="338.416" type="appl" />
         <tli id="T372" time="340.76434117167724" />
         <tli id="T373" time="341.523" type="appl" />
         <tli id="T374" time="342.24" type="appl" />
         <tli id="T375" time="342.957" type="appl" />
         <tli id="T376" time="344.75098063212255" />
         <tli id="T377" time="346.1309712145844" />
         <tli id="T378" time="347.5509615240741" />
         <tli id="T379" time="348.225" type="appl" />
         <tli id="T380" time="348.976" type="appl" />
         <tli id="T381" time="349.726" type="appl" />
         <tli id="T382" time="350.476" type="appl" />
         <tli id="T383" time="351.187" type="appl" />
         <tli id="T384" time="351.862" type="appl" />
         <tli id="T385" time="352.99759102103707" />
         <tli id="T386" time="354.282" type="appl" />
         <tli id="T387" time="355.318" type="appl" />
         <tli id="T388" time="356.355" type="appl" />
         <tli id="T389" time="357.392" type="appl" />
         <tli id="T390" time="358.429" type="appl" />
         <tli id="T391" time="359.465" type="appl" />
         <tli id="T392" time="360.502" type="appl" />
         <tli id="T393" time="362.317527418533" />
         <tli id="T394" time="363.637518410453" />
         <tli id="T395" time="364.29751390641303" />
         <tli id="T396" time="365.312" type="appl" />
         <tli id="T397" time="366.398" type="appl" />
         <tli id="T398" time="367.483" type="appl" />
         <tli id="T399" time="368.21" type="appl" />
         <tli id="T400" time="368.825" type="appl" />
         <tli id="T401" time="369.3543283552768" />
         <tli id="T402" time="369.955" type="appl" />
         <tli id="T403" time="370.469" type="appl" />
         <tli id="T404" time="370.984" type="appl" />
         <tli id="T405" time="371.498" type="appl" />
         <tli id="T406" time="372.19745999441926" />
         <tli id="T407" time="373.104" type="appl" />
         <tli id="T408" time="373.846" type="appl" />
         <tli id="T409" time="374.588" type="appl" />
         <tli id="T410" time="375.33" type="appl" />
         <tli id="T411" time="376.05" type="appl" />
         <tli id="T412" time="376.762" type="appl" />
         <tli id="T413" time="377.475" type="appl" />
         <tli id="T414" time="379.5240766616925" />
         <tli id="T415" time="380.447" type="appl" />
         <tli id="T416" time="381.115" type="appl" />
         <tli id="T417" time="381.783" type="appl" />
         <tli id="T418" time="382.451" type="appl" />
         <tli id="T419" time="383.119" type="appl" />
         <tli id="T420" time="384.42404322260774" />
         <tli id="T421" time="385.19" type="appl" />
         <tli id="T422" time="385.839" type="appl" />
         <tli id="T423" time="386.488" type="appl" />
         <tli id="T424" time="387.0706918276191" />
         <tli id="T425" time="387.9906855492604" />
         <tli id="T426" time="388.685" type="appl" />
         <tli id="T427" time="389.439" type="appl" />
         <tli id="T428" time="390.192" type="appl" />
         <tli id="T429" time="390.859" type="appl" />
         <tli id="T430" time="391.521" type="appl" />
         <tli id="T431" time="392.183" type="appl" />
         <tli id="T432" time="392.845" type="appl" />
         <tli id="T433" time="393.507" type="appl" />
         <tli id="T434" time="394.35" type="appl" />
         <tli id="T435" time="396.26396242286006" />
         <tli id="T436" time="397.272" type="appl" />
         <tli id="T437" time="398.008" type="appl" />
         <tli id="T438" time="398.745" type="appl" />
         <tli id="T439" time="399.481" type="appl" />
         <tli id="T440" time="400.218" type="appl" />
         <tli id="T441" time="401.209" type="appl" />
         <tli id="T442" time="403.5172462572489" />
         <tli id="T443" time="404.38" type="appl" />
         <tli id="T444" time="404.95" type="appl" />
         <tli id="T445" time="405.519" type="appl" />
         <tli id="T446" time="406.81722373704895" />
         <tli id="T447" time="408.193" type="appl" />
         <tli id="T448" time="408.73" type="appl" />
         <tli id="T449" time="409.268" type="appl" />
         <tli id="T450" time="409.805" type="appl" />
         <tli id="T451" time="411.29719316417146" />
         <tli id="T452" time="412.045" type="appl" />
         <tli id="T453" time="412.875" type="appl" />
         <tli id="T454" time="413.706" type="appl" />
         <tli id="T455" time="414.536" type="appl" />
         <tli id="T456" time="415.367" type="appl" />
         <tli id="T457" time="416.151" type="appl" />
         <tli id="T458" time="416.818" type="appl" />
         <tli id="T459" time="417.485" type="appl" />
         <tli id="T460" time="418.327" type="appl" />
         <tli id="T461" time="419.071" type="appl" />
         <tli id="T462" time="419.990467171564" />
         <tli id="T463" time="420.927" type="appl" />
         <tli id="T464" time="421.817" type="appl" />
         <tli id="T465" time="422.707" type="appl" />
         <tli id="T466" time="423.367" type="appl" />
         <tli id="T467" time="424.023" type="appl" />
         <tli id="T468" time="424.989" type="appl" />
         <tli id="T469" time="425.955" type="appl" />
         <tli id="T470" time="426.92" type="appl" />
         <tli id="T471" time="427.886" type="appl" />
         <tli id="T472" time="428.686" type="appl" />
         <tli id="T473" time="429.43" type="appl" />
         <tli id="T474" time="430.174" type="appl" />
         <tli id="T475" time="430.918" type="appl" />
         <tli id="T476" time="431.662" type="appl" />
         <tli id="T477" time="432.686" type="appl" />
         <tli id="T478" time="433.648" type="appl" />
         <tli id="T479" time="434.61" type="appl" />
         <tli id="T480" time="435.488" type="appl" />
         <tli id="T481" time="436.9370181890423" />
         <tli id="T482" time="437.315" type="appl" />
         <tli id="T483" time="437.632" type="appl" />
         <tli id="T484" time="437.948" type="appl" />
         <tli id="T485" time="438.7570057688108" />
         <tli id="T486" time="439.395" type="appl" />
         <tli id="T487" time="440.166" type="appl" />
         <tli id="T488" time="440.937" type="appl" />
         <tli id="T489" time="441.613" type="appl" />
         <tli id="T490" time="442.288" type="appl" />
         <tli id="T491" time="442.964" type="appl" />
         <tli id="T492" time="443.64" type="appl" />
         <tli id="T493" time="444.674" type="appl" />
         <tli id="T494" time="445.91029028562997" />
         <tli id="T495" time="448.2369410744183" />
         <tli id="T496" time="449.077" type="appl" />
         <tli id="T497" time="449.805" type="appl" />
         <tli id="T498" time="450.78359036186" />
         <tli id="T499" time="451.633" type="appl" />
         <tli id="T500" time="452.38" type="appl" />
         <tli id="T501" time="453.128" type="appl" />
         <tli id="T502" time="453.875" type="appl" />
         <tli id="T503" time="454.622" type="appl" />
         <tli id="T504" time="455.12897737408656" />
         <tli id="T505" time="456.76354955252793" />
         <tli id="T506" time="457.294" type="appl" />
         <tli id="T507" time="457.859" type="appl" />
         <tli id="T508" time="458.423" type="appl" />
         <tli id="T509" time="459.131" type="appl" />
         <tli id="T510" time="459.838" type="appl" />
         <tli id="T511" time="460.4793575278887" />
         <tli id="T512" time="461.66351611344317" />
         <tli id="T513" time="462.355" type="appl" />
         <tli id="T514" time="462.98" type="appl" />
         <tli id="T515" time="463.9301673116897" />
         <tli id="T516" time="464.984" type="appl" />
         <tli id="T517" time="465.988" type="appl" />
         <tli id="T518" time="466.991" type="appl" />
         <tli id="T519" time="467.663" type="appl" />
         <tli id="T520" time="468.335" type="appl" />
         <tli id="T521" time="469.007" type="appl" />
         <tli id="T522" time="471.3901164023892" />
         <tli id="T523" time="472.73677387899454" />
         <tli id="T524" time="474.53009497407777" />
         <tli id="T525" time="475.491" type="appl" />
         <tli id="T526" time="476.333" type="appl" />
         <tli id="T527" time="477.174" type="appl" />
         <tli id="T528" time="478.016" type="appl" />
         <tli id="T529" time="478.857" type="appl" />
         <tli id="T530" time="479.699" type="appl" />
         <tli id="T531" time="481.75004570261" />
         <tli id="T532" time="482.988" type="appl" />
         <tli id="T533" time="488.27666782932573" />
         <tli id="T534" time="489.001" type="appl" />
         <tli id="T535" time="489.356" type="appl" />
         <tli id="T536" time="489.712" type="appl" />
         <tli id="T537" time="490.068" type="appl" />
         <tli id="T538" time="490.424" type="appl" />
         <tli id="T539" time="490.779" type="appl" />
         <tli id="T540" time="491.18331466001825" />
         <tli id="T541" time="491.692" type="appl" />
         <tli id="T542" time="492.4233061978826" />
         <tli id="T543" time="492.794" type="appl" />
         <tli id="T544" time="493.21" type="appl" />
         <tli id="T545" time="493.627" type="appl" />
         <tli id="T546" time="494.044" type="appl" />
         <tli id="T547" time="494.46" type="appl" />
         <tli id="T548" time="494.877" type="appl" />
         <tli id="T549" time="495.293" type="appl" />
         <tli id="T550" time="496.0366148727343" />
         <tli id="T551" time="498.99659467279736" />
         <tli id="T552" time="499.5" type="appl" />
         <tli id="T553" time="500.005" type="appl" />
         <tli id="T554" time="500.51" type="appl" />
         <tli id="T555" time="501.015" type="appl" />
         <tli id="T556" time="501.52" type="appl" />
         <tli id="T557" time="502.025" type="appl" />
         <tli id="T558" time="502.53" type="appl" />
         <tli id="T559" time="503.22323249541" />
         <tli id="T560" time="504.72322225895556" />
         <tli id="T561" time="505.376" type="appl" />
         <tli id="T562" time="505.969" type="appl" />
         <tli id="T563" time="506.562" type="appl" />
         <tli id="T564" time="507.1139871935398" />
         <tli id="T565" time="507.8" type="appl" />
         <tli id="T566" time="508.447" type="appl" />
         <tli id="T567" time="509.093" type="appl" />
         <tli id="T568" time="510.604" type="appl" />
         <tli id="T569" time="511.701" type="appl" />
         <tli id="T570" time="512.507" type="appl" />
         <tli id="T571" time="513.213" type="appl" />
         <tli id="T572" time="513.92" type="appl" />
         <tli id="T573" time="514.67" type="appl" />
         <tli id="T574" time="515.419" type="appl" />
         <tli id="T575" time="516.472" type="appl" />
         <tli id="T576" time="517.8831324511278" />
         <tli id="T577" time="518.882" type="appl" />
         <tli id="T578" time="519.7866611274494" />
         <tli id="T579" time="521.056" type="appl" />
         <tli id="T580" time="522.8164321178997" />
         <tli id="T581" time="525.163082770202" />
         <tli id="T582" time="526.039" type="appl" />
         <tli id="T583" time="526.75" type="appl" />
         <tli id="T584" time="527.4933272844654" />
         <tli id="T585" time="528.046" type="appl" />
         <tli id="T586" time="528.631" type="appl" />
         <tli id="T587" time="529.217" type="appl" />
         <tli id="T588" time="529.665" type="appl" />
         <tli id="T589" time="530.114" type="appl" />
         <tli id="T590" time="530.9163768410453" />
         <tli id="T591" time="531.56" type="appl" />
         <tli id="T592" time="532.139" type="appl" />
         <tli id="T593" time="532.717" type="appl" />
         <tli id="T594" time="533.296" type="appl" />
         <tli id="T595" time="533.874" type="appl" />
         <tli id="T596" time="534.453" type="appl" />
         <tli id="T597" time="535.2496806023989" />
         <tli id="T598" time="536.08" type="appl" />
         <tli id="T599" time="536.797" type="appl" />
         <tli id="T600" time="537.889662586239" />
         <tli id="T601" time="538.423" type="appl" />
         <tli id="T602" time="538.932" type="appl" />
         <tli id="T603" time="539.442" type="appl" />
         <tli id="T604" time="539.952" type="appl" />
         <tli id="T605" time="540.461" type="appl" />
         <tli id="T606" time="541.2763061412661" />
         <tli id="T607" time="542.731" type="appl" />
         <tli id="T608" time="543.7029562476241" />
         <tli id="T609" time="544.468" type="appl" />
         <tli id="T610" time="545.224" type="appl" />
         <tli id="T611" time="545.9189827913509" />
         <tli id="T612" time="546.704" type="appl" />
         <tli id="T613" time="547.429" type="appl" />
         <tli id="T614" time="548.154" type="appl" />
         <tli id="T615" time="548.879" type="appl" />
         <tli id="T616" time="549.603" type="appl" />
         <tli id="T617" time="550.328" type="appl" />
         <tli id="T618" time="551.053" type="appl" />
         <tli id="T619" time="551.778" type="appl" />
         <tli id="T620" time="552.4230009056577" />
         <tli id="T621" time="553.156" type="appl" />
         <tli id="T622" time="553.809" type="appl" />
         <tli id="T623" time="554.461" type="appl" />
         <tli id="T624" time="555.114" type="appl" />
         <tli id="T625" time="556.6295346988005" />
         <tli id="T626" time="557.416" type="appl" />
         <tli id="T627" time="558.074" type="appl" />
         <tli id="T628" time="558.731" type="appl" />
         <tli id="T629" time="559.5295149083217" />
         <tli id="T630" time="563.961" type="appl" />
         <tli id="T631" time="564.682" type="appl" />
         <tli id="T632" time="565.115" type="appl" />
         <tli id="T633" time="565.548" type="appl" />
         <tli id="T634" time="566.1161366255793" />
         <tli id="T635" time="566.862" type="appl" />
         <tli id="T636" time="567.373" type="appl" />
         <tli id="T637" time="567.885" type="appl" />
         <tli id="T638" time="568.396" type="appl" />
         <tli id="T639" time="569.472" type="appl" />
         <tli id="T640" time="570.549" type="appl" />
         <tli id="T641" time="571.6" type="appl" />
         <tli id="T642" time="572.652" type="appl" />
         <tli id="T643" time="575.3627401901907" />
         <tli id="T644" time="576.534" type="appl" />
         <tli id="T645" time="577.501" type="appl" />
         <tli id="T646" time="578.469" type="appl" />
         <tli id="T647" time="579.543" type="appl" />
         <tli id="T648" time="580.612" type="appl" />
         <tli id="T649" time="581.68" type="appl" />
         <tli id="T650" time="582.749" type="appl" />
         <tli id="T651" time="584.9893411615672" />
         <tli id="T652" time="585.872" type="appl" />
         <tli id="T653" time="586.591" type="appl" />
         <tli id="T654" time="587.532" type="appl" />
         <tli id="T655" time="588.472" type="appl" />
         <tli id="T656" time="589.412" type="appl" />
         <tli id="T657" time="590.8093014441237" />
         <tli id="T658" time="591.579" type="appl" />
         <tli id="T659" time="592.25" type="appl" />
         <tli id="T660" time="593.2092850657964" />
         <tli id="T661" time="593.838" type="appl" />
         <tli id="T662" time="594.6" type="appl" />
         <tli id="T663" time="595.363" type="appl" />
         <tli id="T664" time="596.126" type="appl" />
         <tli id="T665" time="596.889" type="appl" />
         <tli id="T666" time="597.651" type="appl" />
         <tli id="T667" time="599.5092420726874" />
         <tli id="T668" time="601.013" type="appl" />
         <tli id="T669" time="602.313" type="appl" />
         <tli id="T670" time="603.614" type="appl" />
         <tli id="T671" time="605.1758700683037" />
         <tli id="T672" time="606.3158622885983" />
         <tli id="T673" time="606.984" type="appl" />
         <tli id="T674" time="607.686" type="appl" />
         <tli id="T675" time="608.389" type="appl" />
         <tli id="T676" time="609.211" type="appl" />
         <tli id="T677" time="610.2825018855297" />
         <tli id="T678" time="611.315" type="appl" />
         <tli id="T679" time="612.28" type="appl" />
         <tli id="T680" time="613.084" type="appl" />
         <tli id="T681" time="613.888" type="appl" />
         <tli id="T682" time="614.692" type="appl" />
         <tli id="T683" time="615.5557992320385" />
         <tli id="T684" time="617.076" type="appl" />
         <tli id="T685" time="621.3957593781088" />
         <tli id="T686" time="622.267" type="appl" />
         <tli id="T687" time="622.851" type="appl" />
         <tli id="T688" time="623.435" type="appl" />
         <tli id="T689" time="624.018" type="appl" />
         <tli id="T690" time="624.602" type="appl" />
         <tli id="T691" time="625.186" type="appl" />
         <tli id="T692" time="626.4890579529922" />
         <tli id="T693" time="627.245" type="appl" />
         <tli id="T694" time="627.956" type="appl" />
         <tli id="T695" time="628.8357086052945" />
         <tli id="T696" time="630.926" type="appl" />
         <tli id="T697" time="631.321" type="appl" />
         <tli id="T698" time="631.9690205558117" />
         <tli id="T699" time="632.76" type="appl" />
         <tli id="T700" time="633.468" type="appl" />
         <tli id="T701" time="635.4623300495798" />
         <tli id="T702" time="636.058" type="appl" />
         <tli id="T703" time="637.3689837045754" />
         <tli id="T704" time="639.1223050726309" />
         <tli id="T705" time="640.184" type="appl" />
         <tli id="T706" time="641.003" type="appl" />
         <tli id="T707" time="641.822" type="appl" />
         <tli id="T708" time="642.642" type="appl" />
         <tli id="T709" time="643.462" type="appl" />
         <tli id="T710" time="644.281" type="appl" />
         <tli id="T711" time="644.935" type="appl" />
         <tli id="T712" time="645.59" type="appl" />
         <tli id="T713" time="646.244" type="appl" />
         <tli id="T714" time="647.022251160637" />
         <tli id="T715" time="652.931" type="appl" />
         <tli id="T716" time="653.336" type="appl" />
         <tli id="T717" time="653.742" type="appl" />
         <tli id="T718" time="654.147" type="appl" />
         <tli id="T719" time="657.0088496752642" />
         <tli id="T720" time="657.949" type="appl" />
         <tli id="T721" time="658.927" type="appl" />
         <tli id="T722" time="660.6221583501161" />
         <tli id="T723" time="661.2954870884187" />
         <tli id="T724" time="661.9554825843787" />
         <tli id="T725" time="665.4287922146328" />
         <tli id="T726" time="666.234" type="appl" />
         <tli id="T727" time="666.835" type="appl" />
         <tli id="T728" time="667.437" type="appl" />
         <tli id="T729" time="670.0354274440103" />
         <tli id="T730" time="671.902081371978" />
         <tli id="T731" time="672.2154125670298" />
         <tli id="T732" time="672.4287444445118" />
         <tli id="T733" time="673.6687359823761" />
         <tli id="T734" time="673.6754026035475" />
         <tli id="T735" time="676.79" type="appl" />
         <tli id="T736" time="677.061" type="appl" />
         <tli id="T737" time="677.332" type="appl" />
         <tli id="T738" time="677.603" type="appl" />
         <tli id="T739" time="677.875" type="appl" />
         <tli id="T740" time="678.146" type="appl" />
         <tli id="T741" time="678.417" type="appl" />
         <tli id="T742" time="679.6820282789007" />
         <tli id="T743" time="680.221" type="appl" />
         <tli id="T744" time="680.74" type="appl" />
         <tli id="T745" time="682.6420080789637" />
         <tli id="T746" time="683.316" type="appl" />
         <tli id="T747" time="684.081" type="appl" />
         <tli id="T748" time="684.846" type="appl" />
         <tli id="T749" time="685.397" type="appl" />
         <tli id="T750" time="685.947" type="appl" />
         <tli id="T751" time="686.498" type="appl" />
         <tli id="T752" time="687.048" type="appl" />
         <tli id="T753" time="687.599" type="appl" />
         <tli id="T754" time="688.8419657682851" />
         <tli id="T755" time="689.75" type="appl" />
         <tli id="T756" time="690.586" type="appl" />
         <tli id="T757" time="691.422" type="appl" />
         <tli id="T758" time="692.258" type="appl" />
         <tli id="T759" time="693.094" type="appl" />
         <tli id="T760" time="694.8685913071523" />
         <tli id="T761" time="696.195" type="appl" />
         <tli id="T762" time="697.7952380013588" />
         <tli id="T763" time="703.712" type="appl" />
         <tli id="T764" time="704.31" type="appl" />
         <tli id="T765" time="704.907" type="appl" />
         <tli id="T766" time="705.505" type="appl" />
         <tli id="T767" time="706.5055" type="intp" />
         <tli id="T768" time="707.506" type="appl" />
         <tli id="T769" time="708.562" type="intp" />
         <tli id="T770" time="709.618" type="appl" />
         <tli id="T771" time="710.4545" type="intp" />
         <tli id="T772" time="711.291" type="appl" />
         <tli id="T773" time="711.716" type="appl" />
         <tli id="T774" time="712.625" type="intp" />
         <tli id="T775" time="713.534" type="appl" />
         <tli id="T776" time="713.716" type="appl" />
         <tli id="T777" time="714.502" type="appl" />
         <tli id="T778" time="715.282" type="appl" />
         <tli id="T779" time="716.548443356263" />
         <tli id="T780" time="717.174" type="appl" />
         <tli id="T781" time="717.787" type="appl" />
         <tli id="T782" time="718.501" type="appl" />
         <tli id="T783" time="719.215" type="appl" />
         <tli id="T784" time="719.8756602335052" />
         <tli id="T785" time="720.808" type="appl" />
         <tli id="T786" time="721.935073262684" />
         <tli id="T787" time="723.3683968145165" />
         <tli id="T788" time="724.153" type="appl" />
         <tli id="T789" time="724.886" type="appl" />
         <tli id="T790" time="726.188377569982" />
         <tli id="T791" time="726.96" type="appl" />
         <tli id="T792" time="727.524" type="appl" />
         <tli id="T793" time="728.089" type="appl" />
         <tli id="T794" time="728.654" type="appl" />
         <tli id="T795" time="729.218" type="appl" />
         <tli id="T796" time="729.79632169811" />
         <tli id="T797" time="730.725" type="appl" />
         <tli id="T798" time="731.666" type="appl" />
         <tli id="T799" time="732.608" type="appl" />
         <tli id="T800" time="733.482" type="appl" />
         <tli id="T801" time="734.7083194269203" />
         <tli id="T802" time="735.827" type="appl" />
         <tli id="T803" time="736.684" type="appl" />
         <tli id="T804" time="737.542" type="appl" />
         <tli id="T805" time="738.399" type="appl" />
         <tli id="T806" time="739.574952881979" />
         <tli id="T807" time="740.582" type="appl" />
         <tli id="T808" time="741.56" type="appl" />
         <tli id="T809" time="742.7415979383527" />
         <tli id="T810" time="744.209" type="appl" />
         <tli id="T811" time="745.6749112537307" />
         <tli id="T812" time="747.6548977416106" />
         <tli id="T813" time="748.391" type="appl" />
         <tli id="T814" time="749.016" type="appl" />
         <tli id="T815" time="749.641" type="appl" />
         <tli id="T816" time="750.266" type="appl" />
         <tli id="T817" time="750.891" type="appl" />
         <tli id="T818" time="751.516" type="appl" />
         <tli id="T819" time="752.141" type="appl" />
         <tli id="T820" time="752.935" type="appl" />
         <tli id="T821" time="753.664" type="appl" />
         <tli id="T822" time="754.4015183669796" />
         <tli id="T823" time="755.9615077210669" />
         <tli id="T824" time="756.834835094509" />
         <tli id="T825" time="757.95" type="appl" />
         <tli id="T826" time="759.2814850643811" />
         <tli id="T827" time="760.528" type="appl" />
         <tli id="T828" time="761.722" type="appl" />
         <tli id="T829" time="763.193" type="appl" />
         <tli id="T830" time="764.244" type="appl" />
         <tli id="T831" time="765.6547749041564" />
         <tli id="T832" time="766.644" type="appl" />
         <tli id="T833" time="767.51" type="appl" />
         <tli id="T834" time="768.388089584395" />
         <tli id="T835" time="769.558" type="appl" />
         <tli id="T836" time="770.345" type="appl" />
         <tli id="T837" time="770.98" type="appl" />
         <tli id="T838" time="771.615" type="appl" />
         <tli id="T839" time="772.25" type="appl" />
         <tli id="T840" time="773.2613896606249" />
         <tli id="T841" time="773.917" type="appl" />
         <tli id="T842" time="774.551" type="appl" />
         <tli id="T843" time="775.4213749201303" />
         <tli id="T844" time="776.343" type="appl" />
         <tli id="T845" time="777.118" type="appl" />
         <tli id="T846" time="777.894" type="appl" />
         <tli id="T847" time="778.67" type="appl" />
         <tli id="T848" time="779.461" type="appl" />
         <tli id="T849" time="780.251" type="appl" />
         <tli id="T850" time="781.042" type="appl" />
         <tli id="T851" time="782.365" type="appl" />
         <tli id="T852" time="783.9346501558973" />
         <tli id="T853" time="784.512" type="appl" />
         <tli id="T854" time="785.096" type="appl" />
         <tli id="T855" time="785.68" type="appl" />
         <tli id="T856" time="786.264" type="appl" />
         <tli id="T857" time="786.849" type="appl" />
         <tli id="T858" time="787.433" type="appl" />
         <tli id="T859" time="788.017" type="appl" />
         <tli id="T860" time="788.601" type="appl" />
         <tli id="T861" time="789.185" type="appl" />
         <tli id="T862" time="790.009" type="appl" />
         <tli id="T863" time="790.5763235807962" />
         <tli id="T864" time="791.422" type="appl" />
         <tli id="T865" time="792.26" type="appl" />
         <tli id="T866" time="793.099" type="appl" />
         <tli id="T867" time="793.937" type="appl" />
         <tli id="T868" time="794.9212418462216" />
         <tli id="T869" time="796.3345655345399" />
         <tli id="T870" time="797.354558573751" />
         <tli id="T871" time="798.014" type="appl" />
         <tli id="T872" time="798.644653936356" />
         <tli id="T873" time="799.326" type="appl" />
         <tli id="T874" time="799.995" type="appl" />
         <tli id="T875" time="800.664" type="appl" />
         <tli id="T876" time="801.4811970787939" />
         <tli id="T877" time="803.0678529175665" />
         <tli id="T878" time="804.116" type="appl" />
         <tli id="T879" time="805.126" type="appl" />
         <tli id="T880" time="806.2878309433107" />
         <tli id="T881" time="807.432" type="appl" />
         <tli id="T882" time="809.4011430303141" />
         <tli id="T883" time="809.891" type="appl" />
         <tli id="T884" time="810.261" type="appl" />
         <tli id="T885" time="810.631" type="appl" />
         <tli id="T886" time="811.001" type="appl" />
         <tli id="T887" time="811.371" type="appl" />
         <tli id="T888" time="811.741" type="appl" />
         <tli id="T889" time="812.111" type="appl" />
         <tli id="T890" time="812.6277876772297" />
         <tli id="T891" time="813.035" type="appl" />
         <tli id="T892" time="813.46" type="appl" />
         <tli id="T893" time="813.886" type="appl" />
         <tli id="T894" time="814.311" type="appl" />
         <tli id="T895" time="814.736" type="appl" />
         <tli id="T896" time="815.3744355998108" />
         <tli id="T897" time="816.16" type="appl" />
         <tli id="T898" time="816.756" type="appl" />
         <tli id="T899" time="817.352" type="appl" />
         <tli id="T900" time="817.948" type="appl" />
         <tli id="T901" time="818.783" type="appl" />
         <tli id="T902" time="819.568" type="appl" />
         <tli id="T903" time="820.353" type="appl" />
         <tli id="T904" time="820.819" type="appl" />
         <tli id="T905" time="821.284" type="appl" />
         <tli id="T906" time="821.75" type="appl" />
         <tli id="T907" time="822.8477179328529" />
         <tli id="T908" time="824.132" type="appl" />
         <tli id="T909" time="824.254" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-MAK"
                      id="tx-MAK"
                      speaker="MAK"
                      type="t">
         <timeline-fork end="T15" start="T0">
            <tli id="T0.tx-MAK.1" />
            <tli id="T0.tx-MAK.2" />
            <tli id="T0.tx-MAK.3" />
            <tli id="T0.tx-MAK.4" />
            <tli id="T0.tx-MAK.5" />
            <tli id="T0.tx-MAK.6" />
            <tli id="T0.tx-MAK.7" />
            <tli id="T0.tx-MAK.8" />
            <tli id="T0.tx-MAK.9" />
            <tli id="T0.tx-MAK.10" />
            <tli id="T0.tx-MAK.11" />
            <tli id="T0.tx-MAK.12" />
            <tli id="T0.tx-MAK.13" />
            <tli id="T0.tx-MAK.14" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-MAK">
            <ts e="T15" id="Seg_0" n="sc" s="T0">
               <ts e="T15" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-MAK.1" id="Seg_4" n="HIAT:w" s="T0">Запись</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.2" id="Seg_7" n="HIAT:w" s="T0.tx-MAK.1">камасинского</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.3" id="Seg_10" n="HIAT:w" s="T0.tx-MAK.2">языка</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.4" id="Seg_14" n="HIAT:w" s="T0.tx-MAK.3">сделанная</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.5" id="Seg_17" n="HIAT:w" s="T0.tx-MAK.4">в</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.6" id="Seg_20" n="HIAT:w" s="T0.tx-MAK.5">деревне</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.7" id="Seg_23" n="HIAT:w" s="T0.tx-MAK.6">Абалаково</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.8" id="Seg_26" n="HIAT:w" s="T0.tx-MAK.7">Рыбинского</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.9" id="Seg_29" n="HIAT:w" s="T0.tx-MAK.8">района</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.10" id="Seg_32" n="HIAT:w" s="T0.tx-MAK.9">Красноярского</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.11" id="Seg_35" n="HIAT:w" s="T0.tx-MAK.10">края</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.12" id="Seg_38" n="HIAT:w" s="T0.tx-MAK.11">у</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.13" id="Seg_41" n="HIAT:w" s="T0.tx-MAK.12">Клавдии</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-MAK.14" id="Seg_44" n="HIAT:w" s="T0.tx-MAK.13">Захаровны</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T0.tx-MAK.14">Плотниковой</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-MAK">
            <ts e="T15" id="Seg_50" n="sc" s="T0">
               <ts e="T15" id="Seg_52" n="e" s="T0">Запись камасинского языка, сделанная в деревне Абалаково Рыбинского района Красноярского края у Клавдии Захаровны Плотниковой. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-MAK">
            <ta e="T15" id="Seg_53" s="T0">PKZ_196X_SU0203.MAK.001 (001)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-MAK">
            <ta e="T15" id="Seg_54" s="T0">Запись камасинского языка, сделанная в деревне Абалаково Рыбинского района Красноярского края у Клавдии Захаровны Плотниковой. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-MAK" />
         <annotation name="fr" tierref="fr-MAK" />
         <annotation name="fe" tierref="fe-MAK" />
         <annotation name="fg" tierref="fg-MAK" />
         <annotation name="nt" tierref="nt-MAK" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T172" start="T170">
            <tli id="T170.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T185" start="T184">
            <tli id="T184.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T217" start="T216">
            <tli id="T216.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T770" start="T769">
            <tli id="T769.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T785" start="T784">
            <tli id="T784.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T901" start="T900">
            <tli id="T900.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T533" id="Seg_55" n="sc" s="T13">
               <ts e="T19" id="Seg_57" n="HIAT:u" s="T13">
                  <nts id="Seg_58" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T13">Dʼaziraʔ</ts>
                  <nts id="Seg_61" n="HIAT:ip">)</nts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">tüjö</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">dʼăbaktərzittə</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">nada</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_75" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">Dĭgəttə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">talerdə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">kalam</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_87" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">Dʼü</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">talerzittə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_96" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">Dĭgəttə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">ipek</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">kuʔsittə</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_108" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">özerləj</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">urgo</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">özerləj</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">bar</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_127" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">Sĭre</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">moləj</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_136" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">Dĭgəttə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">püzittə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">nada</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_148" n="HIAT:u" s="T37">
                  <nts id="Seg_149" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">Pag-</ts>
                  <nts id="Seg_152" n="HIAT:ip">)</nts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_156" n="HIAT:u" s="T38">
                  <nts id="Seg_157" n="HIAT:ip">(</nts>
                  <nts id="Seg_158" n="HIAT:ip">(</nts>
                  <ats e="T39" id="Seg_159" n="HIAT:non-pho" s="T38">BRK</ats>
                  <nts id="Seg_160" n="HIAT:ip">)</nts>
                  <nts id="Seg_161" n="HIAT:ip">)</nts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_165" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_167" n="HIAT:w" s="T39">Dĭgəttə</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_170" n="HIAT:w" s="T40">toʔnarzittə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_173" n="HIAT:w" s="T41">nada</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_177" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_179" n="HIAT:w" s="T42">Dĭgəttə</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_182" n="HIAT:w" s="T43">băranə</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_185" n="HIAT:w" s="T44">kămnasʼtə</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_189" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_191" n="HIAT:w" s="T45">Dĭgəttə</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">nada</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">nʼeʔsittə</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_201" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_203" n="HIAT:w" s="T48">Dĭgəttə</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_206" n="HIAT:w" s="T49">nada</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_209" n="HIAT:w" s="T50">ilgərzittə</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_213" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_215" n="HIAT:w" s="T51">Dĭgəttə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_218" n="HIAT:w" s="T52">nuldəsʼtə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_221" n="HIAT:w" s="T53">nada</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_225" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_227" n="HIAT:w" s="T54">Dĭgəttə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">pürzittə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">nada</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_237" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_239" n="HIAT:w" s="T57">Dĭgəttə</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_242" n="HIAT:w" s="T58">amzittə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_245" n="HIAT:w" s="T59">nada</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_249" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_251" n="HIAT:w" s="T60">Bar</ts>
                  <nts id="Seg_252" n="HIAT:ip">!</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_255" n="HIAT:u" s="T61">
                  <nts id="Seg_256" n="HIAT:ip">(</nts>
                  <nts id="Seg_257" n="HIAT:ip">(</nts>
                  <ats e="T62" id="Seg_258" n="HIAT:non-pho" s="T61">BRK</ats>
                  <nts id="Seg_259" n="HIAT:ip">)</nts>
                  <nts id="Seg_260" n="HIAT:ip">)</nts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_264" n="HIAT:u" s="T62">
                  <nts id="Seg_265" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_267" n="HIAT:w" s="T62">Tăn=</ts>
                  <nts id="Seg_268" n="HIAT:ip">)</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_271" n="HIAT:w" s="T63">Tăn</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_274" n="HIAT:w" s="T64">kandlaʔbəl</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_278" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_280" n="HIAT:w" s="T65">Ĭmbi</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_283" n="HIAT:w" s="T66">măna</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_286" n="HIAT:w" s="T67">ej</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_289" n="HIAT:w" s="T68">nörbəbiel</ts>
                  <nts id="Seg_290" n="HIAT:ip">?</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_293" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_295" n="HIAT:w" s="T69">Măn</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_298" n="HIAT:w" s="T70">bɨ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_301" n="HIAT:w" s="T71">noʔ</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_303" n="HIAT:ip">(</nts>
                  <ts e="T73" id="Seg_305" n="HIAT:w" s="T72">iʔ-</ts>
                  <nts id="Seg_306" n="HIAT:ip">)</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_309" n="HIAT:w" s="T73">mĭbiem</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_313" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_315" n="HIAT:w" s="T74">Dĭn</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_318" n="HIAT:w" s="T75">măn</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_321" n="HIAT:w" s="T76">tugazaŋbə</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_324" n="HIAT:w" s="T77">ige</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_328" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_330" n="HIAT:w" s="T78">Tăn</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_333" n="HIAT:w" s="T79">bɨ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_336" n="HIAT:w" s="T80">kubiol</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_339" n="HIAT:w" s="T81">dĭm</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_343" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_345" n="HIAT:w" s="T82">Kabarləj</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_349" n="HIAT:u" s="T83">
                  <nts id="Seg_350" n="HIAT:ip">(</nts>
                  <nts id="Seg_351" n="HIAT:ip">(</nts>
                  <ats e="T84" id="Seg_352" n="HIAT:non-pho" s="T83">BRK</ats>
                  <nts id="Seg_353" n="HIAT:ip">)</nts>
                  <nts id="Seg_354" n="HIAT:ip">)</nts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_358" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_360" n="HIAT:w" s="T84">Tăn</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_363" n="HIAT:w" s="T85">ĭmbi</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_366" n="HIAT:w" s="T86">măna</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_369" n="HIAT:w" s="T87">ej</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_373" n="HIAT:w" s="T88">nörbəliel-</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_377" n="HIAT:w" s="T89">nörbəbiel</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_381" n="HIAT:w" s="T90">što</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_384" n="HIAT:w" s="T91">kandəgal</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_387" n="HIAT:w" s="T92">Krasnojarskə</ts>
                  <nts id="Seg_388" n="HIAT:ip">?</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_391" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_393" n="HIAT:w" s="T93">Dĭn</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_396" n="HIAT:w" s="T94">măn</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_399" n="HIAT:w" s="T95">tuganbə</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_402" n="HIAT:w" s="T96">ige</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_406" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_408" n="HIAT:w" s="T97">Măn</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_411" n="HIAT:w" s="T98">bɨ</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_414" n="HIAT:w" s="T99">noʔ</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_417" n="HIAT:w" s="T100">tănan</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_420" n="HIAT:w" s="T101">mĭbiem</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_424" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_426" n="HIAT:w" s="T102">Dĭn</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_429" n="HIAT:w" s="T103">sĭjdə</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_432" n="HIAT:w" s="T104">ĭzembie</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_436" n="HIAT:u" s="T105">
                  <nts id="Seg_437" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_439" n="HIAT:w" s="T105">Dĭ</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_442" n="HIAT:w" s="T106">bɨ-</ts>
                  <nts id="Seg_443" n="HIAT:ip">)</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_446" n="HIAT:w" s="T107">Dĭ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_449" n="HIAT:w" s="T108">bɨ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_452" n="HIAT:w" s="T109">bĭʔpi</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_456" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_458" n="HIAT:w" s="T110">Tăn</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_461" n="HIAT:w" s="T111">bɨ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_464" n="HIAT:w" s="T112">kubiol</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_467" n="HIAT:w" s="T113">dĭm</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_471" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_473" n="HIAT:w" s="T114">Kabarləj</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_477" n="HIAT:u" s="T115">
                  <nts id="Seg_478" n="HIAT:ip">(</nts>
                  <nts id="Seg_479" n="HIAT:ip">(</nts>
                  <ats e="T116" id="Seg_480" n="HIAT:non-pho" s="T115">BRK</ats>
                  <nts id="Seg_481" n="HIAT:ip">)</nts>
                  <nts id="Seg_482" n="HIAT:ip">)</nts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_486" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_488" n="HIAT:w" s="T116">Amnaʔ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_491" n="HIAT:w" s="T117">toltanoʔ</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_494" n="HIAT:w" s="T118">amzittə</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_498" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_500" n="HIAT:w" s="T119">A</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_503" n="HIAT:w" s="T120">girgit</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_506" n="HIAT:w" s="T121">toltanoʔ</ts>
                  <nts id="Seg_507" n="HIAT:ip">?</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_510" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_512" n="HIAT:w" s="T122">Da</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_515" n="HIAT:w" s="T123">măn</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_518" n="HIAT:w" s="T124">kĭškəbiem</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_522" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_524" n="HIAT:w" s="T125">Dĭgəttə</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_527" n="HIAT:w" s="T126">un</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_530" n="HIAT:w" s="T127">ibi</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_534" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_536" n="HIAT:w" s="T128">Măn</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_539" n="HIAT:w" s="T129">dĭgəttə</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_542" n="HIAT:w" s="T130">sĭreʔpne</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_545" n="HIAT:w" s="T131">embiem</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_548" n="HIAT:w" s="T132">i</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_551" n="HIAT:w" s="T133">keʔbde</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_554" n="HIAT:w" s="T134">embiem</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_558" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_560" n="HIAT:w" s="T135">Dʼibige</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_562" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_564" n="HIAT:w" s="T136">ku-</ts>
                  <nts id="Seg_565" n="HIAT:ip">)</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_568" n="HIAT:w" s="T137">bü</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_571" n="HIAT:w" s="T138">kămnabiam</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_575" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_577" n="HIAT:w" s="T139">Dĭgəttə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_580" n="HIAT:w" s="T140">mĭnzərbiem</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_584" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_586" n="HIAT:w" s="T141">Amnaʔ</ts>
                  <nts id="Seg_587" n="HIAT:ip">,</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_590" n="HIAT:w" s="T142">amoraʔ</ts>
                  <nts id="Seg_591" n="HIAT:ip">!</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_594" n="HIAT:u" s="T143">
                  <nts id="Seg_595" n="HIAT:ip">(</nts>
                  <nts id="Seg_596" n="HIAT:ip">(</nts>
                  <ats e="T144" id="Seg_597" n="HIAT:non-pho" s="T143">BRK</ats>
                  <nts id="Seg_598" n="HIAT:ip">)</nts>
                  <nts id="Seg_599" n="HIAT:ip">)</nts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_603" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_605" n="HIAT:w" s="T144">Nʼi</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_608" n="HIAT:w" s="T145">naga</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_612" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_614" n="HIAT:w" s="T146">Gibərdə</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_617" n="HIAT:w" s="T147">kalla</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_620" n="HIAT:w" s="T911">dʼürbi</ts>
                  <nts id="Seg_621" n="HIAT:ip">?</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_624" n="HIAT:u" s="T148">
                  <nts id="Seg_625" n="HIAT:ip">(</nts>
                  <ts e="T149" id="Seg_627" n="HIAT:w" s="T148">Nad-</ts>
                  <nts id="Seg_628" n="HIAT:ip">)</nts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_632" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_634" n="HIAT:w" s="T149">Măndərzittə</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_637" n="HIAT:w" s="T150">nada</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_641" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_643" n="HIAT:w" s="T151">Možet</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_646" n="HIAT:w" s="T152">šindi-nʼibudʼ</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_649" n="HIAT:w" s="T153">kuʔpi</ts>
                  <nts id="Seg_650" n="HIAT:ip">?</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_653" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_655" n="HIAT:w" s="T154">Možet</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_658" n="HIAT:w" s="T155">urgaːba</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_661" n="HIAT:w" s="T156">ambi</ts>
                  <nts id="Seg_662" n="HIAT:ip">.</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_665" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_667" n="HIAT:w" s="T157">Ajiriom</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_670" n="HIAT:w" s="T158">bar</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_673" n="HIAT:w" s="T159">ugaːndə</ts>
                  <nts id="Seg_674" n="HIAT:ip">.</nts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_677" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_679" n="HIAT:w" s="T160">Kabarləj</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_683" n="HIAT:u" s="T161">
                  <nts id="Seg_684" n="HIAT:ip">(</nts>
                  <nts id="Seg_685" n="HIAT:ip">(</nts>
                  <ats e="T162" id="Seg_686" n="HIAT:non-pho" s="T161">BRK</ats>
                  <nts id="Seg_687" n="HIAT:ip">)</nts>
                  <nts id="Seg_688" n="HIAT:ip">)</nts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_692" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_694" n="HIAT:w" s="T162">Oʔb</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_698" n="HIAT:w" s="T163">šide</ts>
                  <nts id="Seg_699" n="HIAT:ip">,</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_702" n="HIAT:w" s="T164">nagur</ts>
                  <nts id="Seg_703" n="HIAT:ip">,</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_706" n="HIAT:w" s="T165">teʔtə</ts>
                  <nts id="Seg_707" n="HIAT:ip">,</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_710" n="HIAT:w" s="T166">sumna</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_714" n="HIAT:w" s="T167">sejʔpü</ts>
                  <nts id="Seg_715" n="HIAT:ip">,</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_718" n="HIAT:w" s="T168">šinteʔtə</ts>
                  <nts id="Seg_719" n="HIAT:ip">,</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_722" n="HIAT:w" s="T169">bʼeʔ</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_726" n="HIAT:u" s="T170">
                  <ts e="T170.tx-PKZ.1" id="Seg_728" n="HIAT:w" s="T170">Сбилась</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_731" n="HIAT:w" s="T170.tx-PKZ.1">я</ts>
                  <nts id="Seg_732" n="HIAT:ip">.</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_735" n="HIAT:u" s="T172">
                  <nts id="Seg_736" n="HIAT:ip">(</nts>
                  <nts id="Seg_737" n="HIAT:ip">(</nts>
                  <ats e="T173" id="Seg_738" n="HIAT:non-pho" s="T172">BRK</ats>
                  <nts id="Seg_739" n="HIAT:ip">)</nts>
                  <nts id="Seg_740" n="HIAT:ip">)</nts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_744" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_746" n="HIAT:w" s="T173">Kăbɨ</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_749" n="HIAT:w" s="T174">bostə</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_751" n="HIAT:ip">(</nts>
                  <ts e="T176" id="Seg_753" n="HIAT:w" s="T175">i-</ts>
                  <nts id="Seg_754" n="HIAT:ip">)</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_757" n="HIAT:w" s="T176">inezeŋdə</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_760" n="HIAT:w" s="T177">ibiʔi</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_764" n="HIAT:w" s="T178">dĭgəttə</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_767" n="HIAT:w" s="T179">tajirzittə</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_770" n="HIAT:w" s="T180">možnă</ts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_774" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_776" n="HIAT:w" s="T181">I</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_779" n="HIAT:w" s="T182">toltanoʔ</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_782" n="HIAT:w" s="T183">amzittə</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_786" n="HIAT:u" s="T184">
                  <ts e="T184.tx-PKZ.1" id="Seg_788" n="HIAT:w" s="T184">A</ts>
                  <nts id="Seg_789" n="HIAT:ip">_</nts>
                  <ts e="T185" id="Seg_791" n="HIAT:w" s="T184.tx-PKZ.1">to</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_794" n="HIAT:w" s="T185">bostə</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_797" n="HIAT:w" s="T186">ineʔi</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_800" n="HIAT:w" s="T187">naga</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_804" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_806" n="HIAT:w" s="T188">Bar</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_810" n="HIAT:u" s="T189">
                  <nts id="Seg_811" n="HIAT:ip">(</nts>
                  <nts id="Seg_812" n="HIAT:ip">(</nts>
                  <ats e="T190" id="Seg_813" n="HIAT:non-pho" s="T189">BRK</ats>
                  <nts id="Seg_814" n="HIAT:ip">)</nts>
                  <nts id="Seg_815" n="HIAT:ip">)</nts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_819" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_821" n="HIAT:w" s="T190">Măn</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_824" n="HIAT:w" s="T191">meim</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_827" n="HIAT:w" s="T192">bar</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_830" n="HIAT:w" s="T193">jezerik</ts>
                  <nts id="Seg_831" n="HIAT:ip">.</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_834" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_836" n="HIAT:w" s="T194">Ara</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_839" n="HIAT:w" s="T195">bĭʔpi</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_843" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_845" n="HIAT:w" s="T196">Šobi</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_848" n="HIAT:w" s="T197">bar</ts>
                  <nts id="Seg_849" n="HIAT:ip">,</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_852" n="HIAT:w" s="T198">kudonzəbiʔi</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_855" n="HIAT:w" s="T199">bostə</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_858" n="HIAT:w" s="T200">tibizʼiʔ</ts>
                  <nts id="Seg_859" n="HIAT:ip">.</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_862" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_864" n="HIAT:w" s="T201">Dʼabrobiʔi</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_867" n="HIAT:w" s="T202">bar</ts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_871" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_873" n="HIAT:w" s="T203">Ugaːndə</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_876" n="HIAT:w" s="T204">dĭ</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_879" n="HIAT:w" s="T205">kurojok</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_883" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_885" n="HIAT:w" s="T206">Vsʼakă</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_887" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_889" n="HIAT:w" s="T207">kudonzə-</ts>
                  <nts id="Seg_890" n="HIAT:ip">)</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_893" n="HIAT:w" s="T208">kudonzlaʔpi</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_897" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_899" n="HIAT:w" s="T209">Kăde</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_902" n="HIAT:w" s="T210">dĭʔnə</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_905" n="HIAT:w" s="T211">kereʔ</ts>
                  <nts id="Seg_906" n="HIAT:ip">.</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_909" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_911" n="HIAT:w" s="T212">Bazoʔ</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_914" n="HIAT:w" s="T213">kalla</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_917" n="HIAT:w" s="T912">dʼürbi</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_920" n="HIAT:w" s="T214">ara</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_923" n="HIAT:w" s="T215">bĭʔsittə</ts>
                  <nts id="Seg_924" n="HIAT:ip">.</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_927" n="HIAT:u" s="T216">
                  <ts e="T216.tx-PKZ.1" id="Seg_929" n="HIAT:w" s="T216">A</ts>
                  <nts id="Seg_930" n="HIAT:ip">_</nts>
                  <ts e="T217" id="Seg_932" n="HIAT:w" s="T216.tx-PKZ.1">to</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_935" n="HIAT:w" s="T217">dĭʔnə</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_938" n="HIAT:w" s="T218">amga</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_942" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_944" n="HIAT:w" s="T219">Kabarləj</ts>
                  <nts id="Seg_945" n="HIAT:ip">.</nts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_948" n="HIAT:u" s="T220">
                  <nts id="Seg_949" n="HIAT:ip">(</nts>
                  <nts id="Seg_950" n="HIAT:ip">(</nts>
                  <ats e="T221" id="Seg_951" n="HIAT:non-pho" s="T220">BRK</ats>
                  <nts id="Seg_952" n="HIAT:ip">)</nts>
                  <nts id="Seg_953" n="HIAT:ip">)</nts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_957" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_959" n="HIAT:w" s="T221">Šiʔ</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_962" n="HIAT:w" s="T222">dʼijegən</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_965" n="HIAT:w" s="T223">ibileʔ</ts>
                  <nts id="Seg_966" n="HIAT:ip">.</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_969" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_971" n="HIAT:w" s="T224">Gijen</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_974" n="HIAT:w" s="T225">kunolbilaʔ</ts>
                  <nts id="Seg_975" n="HIAT:ip">?</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_978" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_980" n="HIAT:w" s="T226">Kunolbibaʔ</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_983" n="HIAT:w" s="T227">pălatkagən</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_987" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_989" n="HIAT:w" s="T228">Urgaːba</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_992" n="HIAT:w" s="T229">šobi</ts>
                  <nts id="Seg_993" n="HIAT:ip">.</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_996" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_998" n="HIAT:w" s="T230">Bar</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_1001" n="HIAT:w" s="T231">mĭmbi</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1004" n="HIAT:w" s="T232">dön</ts>
                  <nts id="Seg_1005" n="HIAT:ip">.</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_1008" n="HIAT:u" s="T233">
                  <nts id="Seg_1009" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_1011" n="HIAT:w" s="T233">M-</ts>
                  <nts id="Seg_1012" n="HIAT:ip">)</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1015" n="HIAT:w" s="T234">Miʔ</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_1018" n="HIAT:w" s="T235">ugaːndə</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_1021" n="HIAT:w" s="T236">pimbibeʔ</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_1025" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_1027" n="HIAT:w" s="T237">Dĭ</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1030" n="HIAT:w" s="T238">tože</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_1033" n="HIAT:w" s="T239">pimnie</ts>
                  <nts id="Seg_1034" n="HIAT:ip">,</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1037" n="HIAT:w" s="T240">multukdə</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_1041" n="HIAT:u" s="T241">
                  <nts id="Seg_1042" n="HIAT:ip">(</nts>
                  <ts e="T242" id="Seg_1044" n="HIAT:w" s="T241">E-</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_1047" n="HIAT:w" s="T242">ej-</ts>
                  <nts id="Seg_1048" n="HIAT:ip">)</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1051" n="HIAT:w" s="T243">Ejem</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1054" n="HIAT:w" s="T244">nada</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1057" n="HIAT:w" s="T245">surarzittə</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1059" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_1061" n="HIAT:w" s="T246">iʔbəzittə</ts>
                  <nts id="Seg_1062" n="HIAT:ip">)</nts>
                  <nts id="Seg_1063" n="HIAT:ip">,</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1065" n="HIAT:ip">(</nts>
                  <ts e="T248" id="Seg_1067" n="HIAT:w" s="T247">kunolzittə</ts>
                  <nts id="Seg_1068" n="HIAT:ip">)</nts>
                  <nts id="Seg_1069" n="HIAT:ip">.</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_1072" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_1074" n="HIAT:w" s="T248">Kabarləj</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_1078" n="HIAT:u" s="T249">
                  <nts id="Seg_1079" n="HIAT:ip">(</nts>
                  <nts id="Seg_1080" n="HIAT:ip">(</nts>
                  <ats e="T250" id="Seg_1081" n="HIAT:non-pho" s="T249">BRK</ats>
                  <nts id="Seg_1082" n="HIAT:ip">)</nts>
                  <nts id="Seg_1083" n="HIAT:ip">)</nts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_1087" n="HIAT:u" s="T250">
                  <ts e="T910" id="Seg_1089" n="HIAT:w" s="T250">Курочка</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1092" n="HIAT:w" s="T910">lunʼkam</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1095" n="HIAT:w" s="T251">bar</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1098" n="HIAT:w" s="T252">monoʔkolaʔbə</ts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1102" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_1104" n="HIAT:w" s="T254">Bar</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1107" n="HIAT:w" s="T255">pünörleʔbə</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_1111" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1113" n="HIAT:w" s="T256">Bar</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1116" n="HIAT:w" s="T257">münörleʔbə</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1120" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_1122" n="HIAT:w" s="T258">Bar</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1124" n="HIAT:ip">(</nts>
                  <ts e="T260" id="Seg_1126" n="HIAT:w" s="T259">burerleʔbə</ts>
                  <nts id="Seg_1127" n="HIAT:ip">)</nts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_1131" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1133" n="HIAT:w" s="T260">Daška</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1136" n="HIAT:w" s="T261">bar</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1140" n="HIAT:u" s="T262">
                  <nts id="Seg_1141" n="HIAT:ip">(</nts>
                  <nts id="Seg_1142" n="HIAT:ip">(</nts>
                  <ats e="T263" id="Seg_1143" n="HIAT:non-pho" s="T262">BRK</ats>
                  <nts id="Seg_1144" n="HIAT:ip">)</nts>
                  <nts id="Seg_1145" n="HIAT:ip">)</nts>
                  <nts id="Seg_1146" n="HIAT:ip">.</nts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1149" n="HIAT:u" s="T263">
                  <nts id="Seg_1150" n="HIAT:ip">(</nts>
                  <ts e="T264" id="Seg_1152" n="HIAT:w" s="T263">Ĭmbej-</ts>
                  <nts id="Seg_1153" n="HIAT:ip">)</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1156" n="HIAT:w" s="T264">Ĭmbi</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1159" n="HIAT:w" s="T265">pimniel</ts>
                  <nts id="Seg_1160" n="HIAT:ip">?</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1163" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1165" n="HIAT:w" s="T266">Kanaʔ</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1168" n="HIAT:w" s="T267">unnʼa</ts>
                  <nts id="Seg_1169" n="HIAT:ip">!</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1172" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1174" n="HIAT:w" s="T268">Măn</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1177" n="HIAT:w" s="T269">unnʼa</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1180" n="HIAT:w" s="T270">mĭmbiem</ts>
                  <nts id="Seg_1181" n="HIAT:ip">,</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1184" n="HIAT:w" s="T271">šindinədə</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1186" n="HIAT:ip">(</nts>
                  <ts e="T273" id="Seg_1188" n="HIAT:w" s="T272">ej-</ts>
                  <nts id="Seg_1189" n="HIAT:ip">)</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1192" n="HIAT:w" s="T273">ej</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1195" n="HIAT:w" s="T274">pimniem</ts>
                  <nts id="Seg_1196" n="HIAT:ip">.</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1199" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_1201" n="HIAT:w" s="T275">A</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1204" n="HIAT:w" s="T276">tăn</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1207" n="HIAT:w" s="T277">üge</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1210" n="HIAT:w" s="T278">pimneʔbəl</ts>
                  <nts id="Seg_1211" n="HIAT:ip">.</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1214" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1216" n="HIAT:w" s="T279">Nöməlleʔpiem</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1220" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1222" n="HIAT:w" s="T280">Хватит</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1226" n="HIAT:u" s="T281">
                  <nts id="Seg_1227" n="HIAT:ip">(</nts>
                  <nts id="Seg_1228" n="HIAT:ip">(</nts>
                  <ats e="T282" id="Seg_1229" n="HIAT:non-pho" s="T281">DMG</ats>
                  <nts id="Seg_1230" n="HIAT:ip">)</nts>
                  <nts id="Seg_1231" n="HIAT:ip">)</nts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1235" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1237" n="HIAT:w" s="T282">Dĭzeŋ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1240" n="HIAT:w" s="T283">ej</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1243" n="HIAT:w" s="T284">tĭmneʔi</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1246" n="HIAT:w" s="T285">dʼăbaktərzittə</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1250" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1252" n="HIAT:w" s="T286">A</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1255" n="HIAT:w" s="T287">măn</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1258" n="HIAT:w" s="T288">bar</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1261" n="HIAT:w" s="T289">dʼăbaktərlaʔbəm</ts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1265" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1267" n="HIAT:w" s="T290">Iam</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1270" n="HIAT:w" s="T291">surarbiam</ts>
                  <nts id="Seg_1271" n="HIAT:ip">.</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1274" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1276" n="HIAT:w" s="T292">Dʼăbaktərzittə</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1279" n="HIAT:w" s="T293">axota</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1282" n="HIAT:w" s="T294">măna</ts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1286" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1288" n="HIAT:w" s="T295">Bar</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1292" n="HIAT:u" s="T296">
                  <nts id="Seg_1293" n="HIAT:ip">(</nts>
                  <nts id="Seg_1294" n="HIAT:ip">(</nts>
                  <ats e="T297" id="Seg_1295" n="HIAT:non-pho" s="T296">BRK</ats>
                  <nts id="Seg_1296" n="HIAT:ip">)</nts>
                  <nts id="Seg_1297" n="HIAT:ip">)</nts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1301" n="HIAT:u" s="T297">
                  <nts id="Seg_1302" n="HIAT:ip">(</nts>
                  <ts e="T298" id="Seg_1304" n="HIAT:w" s="T297">Dĭn=</ts>
                  <nts id="Seg_1305" n="HIAT:ip">)</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1308" n="HIAT:w" s="T298">Dön</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1311" n="HIAT:w" s="T299">bar</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1314" n="HIAT:w" s="T300">dʼirəgəʔi</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1317" n="HIAT:w" s="T301">naga</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1321" n="HIAT:u" s="T302">
                  <nts id="Seg_1322" n="HIAT:ip">(</nts>
                  <ts e="T303" id="Seg_1324" n="HIAT:w" s="T302">A</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1327" n="HIAT:w" s="T303">onʼ-</ts>
                  <nts id="Seg_1328" n="HIAT:ip">)</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1331" n="HIAT:w" s="T304">Onʼiʔ</ts>
                  <nts id="Seg_1332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1334" n="HIAT:w" s="T305">nuzaŋ</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1337" n="HIAT:w" s="T306">amnolaʔbəʔjə</ts>
                  <nts id="Seg_1338" n="HIAT:ip">.</nts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1341" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1343" n="HIAT:w" s="T307">Dĭzeŋ</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1346" n="HIAT:w" s="T308">bar</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1349" n="HIAT:w" s="T309">dʼirəgəʔi</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1352" n="HIAT:w" s="T310">pimnieʔjə</ts>
                  <nts id="Seg_1353" n="HIAT:ip">.</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1356" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1358" n="HIAT:w" s="T311">Bar</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1360" n="HIAT:ip">(</nts>
                  <ts e="T313" id="Seg_1362" n="HIAT:w" s="T312">em-</ts>
                  <nts id="Seg_1363" n="HIAT:ip">)</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1366" n="HIAT:w" s="T313">ej</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1369" n="HIAT:w" s="T314">dʼăbaktərlam</ts>
                  <nts id="Seg_1370" n="HIAT:ip">.</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1373" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1375" n="HIAT:w" s="T315">Tüjö</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1378" n="HIAT:w" s="T316">băra</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1381" n="HIAT:w" s="T317">detlem</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1385" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1387" n="HIAT:w" s="T318">Tăn</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1390" n="HIAT:w" s="T319">dʼabolal</ts>
                  <nts id="Seg_1391" n="HIAT:ip">.</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1394" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1396" n="HIAT:w" s="T320">Măn</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1399" n="HIAT:w" s="T321">toltanoʔ</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1402" n="HIAT:w" s="T322">kămnabiam</ts>
                  <nts id="Seg_1403" n="HIAT:ip">.</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_1406" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1408" n="HIAT:w" s="T323">Amnogaʔ</ts>
                  <nts id="Seg_1409" n="HIAT:ip">!</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1412" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1414" n="HIAT:w" s="T324">Măn</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1417" n="HIAT:w" s="T325">iššo</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1420" n="HIAT:w" s="T326">šide</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1423" n="HIAT:w" s="T327">aspaʔ</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1426" n="HIAT:w" s="T328">detlem</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1430" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1432" n="HIAT:w" s="T329">Păʔlam</ts>
                  <nts id="Seg_1433" n="HIAT:ip">.</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1436" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1438" n="HIAT:w" s="T330">Dĭgəttə</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1441" n="HIAT:w" s="T331">dʼăbaktərzittə</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1444" n="HIAT:w" s="T332">možna</ts>
                  <nts id="Seg_1445" n="HIAT:ip">.</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1448" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1450" n="HIAT:w" s="T333">Bar</ts>
                  <nts id="Seg_1451" n="HIAT:ip">,</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1454" n="HIAT:w" s="T334">ej</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1457" n="HIAT:w" s="T335">dʼăbaktəriam</ts>
                  <nts id="Seg_1458" n="HIAT:ip">.</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1461" n="HIAT:u" s="T336">
                  <nts id="Seg_1462" n="HIAT:ip">(</nts>
                  <nts id="Seg_1463" n="HIAT:ip">(</nts>
                  <ats e="T337" id="Seg_1464" n="HIAT:non-pho" s="T336">BRK</ats>
                  <nts id="Seg_1465" n="HIAT:ip">)</nts>
                  <nts id="Seg_1466" n="HIAT:ip">)</nts>
                  <nts id="Seg_1467" n="HIAT:ip">.</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1470" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1472" n="HIAT:w" s="T337">Teinen</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1475" n="HIAT:w" s="T338">ugaːndə</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1478" n="HIAT:w" s="T339">dʼibige</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1481" n="HIAT:w" s="T340">dʼala</ts>
                  <nts id="Seg_1482" n="HIAT:ip">.</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1485" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1487" n="HIAT:w" s="T341">Bar</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1490" n="HIAT:w" s="T342">ipek</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1493" n="HIAT:w" s="T343">nendləj</ts>
                  <nts id="Seg_1494" n="HIAT:ip">.</nts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1497" n="HIAT:u" s="T344">
                  <nts id="Seg_1498" n="HIAT:ip">(</nts>
                  <ts e="T345" id="Seg_1500" n="HIAT:w" s="T344">Ej-</ts>
                  <nts id="Seg_1501" n="HIAT:ip">)</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1504" n="HIAT:w" s="T345">ipek</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1507" n="HIAT:w" s="T346">ej</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1510" n="HIAT:w" s="T347">özerləj</ts>
                  <nts id="Seg_1511" n="HIAT:ip">.</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1514" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1516" n="HIAT:w" s="T348">Dĭgəttə</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1519" n="HIAT:w" s="T349">naga</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1522" n="HIAT:w" s="T350">ipek</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1526" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1528" n="HIAT:w" s="T351">Ĭmbi</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1531" n="HIAT:w" s="T352">amzittə</ts>
                  <nts id="Seg_1532" n="HIAT:ip">.</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1535" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1537" n="HIAT:w" s="T353">Külaːmbiam</ts>
                  <nts id="Seg_1538" n="HIAT:ip">,</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1540" n="HIAT:ip">(</nts>
                  <ts e="T355" id="Seg_1542" n="HIAT:w" s="T354">ku-</ts>
                  <nts id="Seg_1543" n="HIAT:ip">)</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1546" n="HIAT:w" s="T355">küzittə</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1549" n="HIAT:w" s="T356">možnă</ts>
                  <nts id="Seg_1550" n="HIAT:ip">,</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1553" n="HIAT:w" s="T357">ipekziʔ</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1556" n="HIAT:w" s="T358">naga</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1560" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1562" n="HIAT:w" s="T359">Bar</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1565" n="HIAT:w" s="T360">ibə</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1569" n="HIAT:w" s="T361">ej</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1572" n="HIAT:w" s="T362">dʼăbaktərlam</ts>
                  <nts id="Seg_1573" n="HIAT:ip">.</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1576" n="HIAT:u" s="T363">
                  <nts id="Seg_1577" n="HIAT:ip">(</nts>
                  <nts id="Seg_1578" n="HIAT:ip">(</nts>
                  <ats e="T364" id="Seg_1579" n="HIAT:non-pho" s="T363">BRK</ats>
                  <nts id="Seg_1580" n="HIAT:ip">)</nts>
                  <nts id="Seg_1581" n="HIAT:ip">)</nts>
                  <nts id="Seg_1582" n="HIAT:ip">.</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1585" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1587" n="HIAT:w" s="T364">Tăn</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1590" n="HIAT:w" s="T365">ugaːndə</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1593" n="HIAT:w" s="T366">sagər</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1597" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1599" n="HIAT:w" s="T367">Bazəjdaʔ</ts>
                  <nts id="Seg_1600" n="HIAT:ip">!</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1603" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1605" n="HIAT:w" s="T368">Multʼanə</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1608" n="HIAT:w" s="T369">kanaʔ</ts>
                  <nts id="Seg_1609" n="HIAT:ip">!</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1612" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1614" n="HIAT:w" s="T370">Sabən</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1617" n="HIAT:w" s="T371">iʔ</ts>
                  <nts id="Seg_1618" n="HIAT:ip">.</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1621" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1623" n="HIAT:w" s="T372">Dĭgəttə</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1626" n="HIAT:w" s="T373">kujnek</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1629" n="HIAT:w" s="T374">iʔ</ts>
                  <nts id="Seg_1630" n="HIAT:ip">!</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1633" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1635" n="HIAT:w" s="T375">Šerdə</ts>
                  <nts id="Seg_1636" n="HIAT:ip">!</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1639" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1641" n="HIAT:w" s="T376">Kabarləj</ts>
                  <nts id="Seg_1642" n="HIAT:ip">.</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1645" n="HIAT:u" s="T377">
                  <nts id="Seg_1646" n="HIAT:ip">(</nts>
                  <nts id="Seg_1647" n="HIAT:ip">(</nts>
                  <ats e="T378" id="Seg_1648" n="HIAT:non-pho" s="T377">BRK</ats>
                  <nts id="Seg_1649" n="HIAT:ip">)</nts>
                  <nts id="Seg_1650" n="HIAT:ip">)</nts>
                  <nts id="Seg_1651" n="HIAT:ip">.</nts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1654" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1656" n="HIAT:w" s="T378">Ugaːndə</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1659" n="HIAT:w" s="T379">kuvas</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1662" n="HIAT:w" s="T380">dĭgəttə</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1665" n="HIAT:w" s="T381">molal</ts>
                  <nts id="Seg_1666" n="HIAT:ip">!</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1669" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1671" n="HIAT:w" s="T382">Kujnek</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1674" n="HIAT:w" s="T383">šeriel</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1677" n="HIAT:w" s="T384">dăk</ts>
                  <nts id="Seg_1678" n="HIAT:ip">.</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1681" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1683" n="HIAT:w" s="T385">Bar</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1686" n="HIAT:w" s="T386">koʔbsaŋ</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1689" n="HIAT:w" s="T387">tănan</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1691" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1693" n="HIAT:w" s="T388">могут</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1696" n="HIAT:w" s="T389">-</ts>
                  <nts id="Seg_1697" n="HIAT:ip">)</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1699" n="HIAT:ip">(</nts>
                  <ts e="T391" id="Seg_1701" n="HIAT:w" s="T390">pănarləʔi</ts>
                  <nts id="Seg_1702" n="HIAT:ip">)</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1705" n="HIAT:w" s="T391">bar</ts>
                  <nts id="Seg_1706" n="HIAT:ip">.</nts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1709" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1711" n="HIAT:w" s="T392">Kuvas</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1715" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1717" n="HIAT:w" s="T393">Kabarləj</ts>
                  <nts id="Seg_1718" n="HIAT:ip">.</nts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1721" n="HIAT:u" s="T394">
                  <nts id="Seg_1722" n="HIAT:ip">(</nts>
                  <nts id="Seg_1723" n="HIAT:ip">(</nts>
                  <ats e="T395" id="Seg_1724" n="HIAT:non-pho" s="T394">BRK</ats>
                  <nts id="Seg_1725" n="HIAT:ip">)</nts>
                  <nts id="Seg_1726" n="HIAT:ip">)</nts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_1730" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1732" n="HIAT:w" s="T395">Dĭzeŋ</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1735" n="HIAT:w" s="T396">Krasnojarskəʔi</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1738" n="HIAT:w" s="T397">kambiʔi</ts>
                  <nts id="Seg_1739" n="HIAT:ip">.</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1742" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_1744" n="HIAT:w" s="T398">Măna</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1747" n="HIAT:w" s="T399">ej</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1750" n="HIAT:w" s="T400">nörbəʔi</ts>
                  <nts id="Seg_1751" n="HIAT:ip">.</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1754" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1756" n="HIAT:w" s="T401">Măn</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1759" n="HIAT:w" s="T402">măn</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1762" n="HIAT:w" s="T403">dĭn</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1765" n="HIAT:w" s="T404">tuganbə</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1768" n="HIAT:w" s="T405">ige</ts>
                  <nts id="Seg_1769" n="HIAT:ip">.</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1772" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1774" n="HIAT:w" s="T406">Măn</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1777" n="HIAT:w" s="T407">bɨ</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1780" n="HIAT:w" s="T408">noʔ</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1783" n="HIAT:w" s="T409">mĭbiem</ts>
                  <nts id="Seg_1784" n="HIAT:ip">.</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1787" n="HIAT:u" s="T410">
                  <nts id="Seg_1788" n="HIAT:ip">(</nts>
                  <ts e="T411" id="Seg_1790" n="HIAT:w" s="T410">Dĭn=</ts>
                  <nts id="Seg_1791" n="HIAT:ip">)</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1794" n="HIAT:w" s="T411">Dĭn</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1797" n="HIAT:w" s="T412">sĭjdə</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1800" n="HIAT:w" s="T413">ĭzemnie</ts>
                  <nts id="Seg_1801" n="HIAT:ip">.</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1804" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1806" n="HIAT:w" s="T414">Dĭzeŋ</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1809" n="HIAT:w" s="T415">bar</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1812" n="HIAT:w" s="T416">kubiʔi</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1815" n="HIAT:w" s="T417">bɨ</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1818" n="HIAT:w" s="T418">dĭm</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1820" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_1822" n="HIAT:w" s="T419">dĭ-</ts>
                  <nts id="Seg_1823" n="HIAT:ip">)</nts>
                  <nts id="Seg_1824" n="HIAT:ip">.</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1827" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1829" n="HIAT:w" s="T420">Bar</ts>
                  <nts id="Seg_1830" n="HIAT:ip">,</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1833" n="HIAT:w" s="T421">daška</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1836" n="HIAT:w" s="T422">ej</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1839" n="HIAT:w" s="T423">dʼăbaktərlam</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1843" n="HIAT:u" s="T424">
                  <nts id="Seg_1844" n="HIAT:ip">(</nts>
                  <nts id="Seg_1845" n="HIAT:ip">(</nts>
                  <ats e="T425" id="Seg_1846" n="HIAT:non-pho" s="T424">BRK</ats>
                  <nts id="Seg_1847" n="HIAT:ip">)</nts>
                  <nts id="Seg_1848" n="HIAT:ip">)</nts>
                  <nts id="Seg_1849" n="HIAT:ip">.</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1852" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1854" n="HIAT:w" s="T425">Sagər</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1857" n="HIAT:w" s="T426">ne</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1860" n="HIAT:w" s="T427">šobi</ts>
                  <nts id="Seg_1861" n="HIAT:ip">.</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1864" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1866" n="HIAT:w" s="T428">Sĭre</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1869" n="HIAT:w" s="T429">kuza</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1872" n="HIAT:w" s="T430">măndə:</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1875" n="HIAT:w" s="T431">izittə</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1878" n="HIAT:w" s="T432">nada</ts>
                  <nts id="Seg_1879" n="HIAT:ip">.</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1882" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1884" n="HIAT:w" s="T433">Kanžəbəj</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1887" n="HIAT:w" s="T434">obberəj</ts>
                  <nts id="Seg_1888" n="HIAT:ip">.</nts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1891" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1893" n="HIAT:w" s="T435">Aktʼigən</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1896" n="HIAT:w" s="T436">bar</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1898" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_1900" n="HIAT:w" s="T437">kunol-</ts>
                  <nts id="Seg_1901" n="HIAT:ip">)</nts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1904" n="HIAT:w" s="T438">kunolžəbəj</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1907" n="HIAT:w" s="T439">bar</ts>
                  <nts id="Seg_1908" n="HIAT:ip">.</nts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1911" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1913" n="HIAT:w" s="T440">Panaržəbəj</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1916" n="HIAT:w" s="T441">bar</ts>
                  <nts id="Seg_1917" n="HIAT:ip">.</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1920" n="HIAT:u" s="T442">
                  <ts e="T443" id="Seg_1922" n="HIAT:w" s="T442">Tibil</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1925" n="HIAT:w" s="T443">bar</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1928" n="HIAT:w" s="T444">münörləj</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1931" n="HIAT:w" s="T445">tănan</ts>
                  <nts id="Seg_1932" n="HIAT:ip">.</nts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1935" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1937" n="HIAT:w" s="T446">Kabarləj</ts>
                  <nts id="Seg_1938" n="HIAT:ip">.</nts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1941" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1943" n="HIAT:w" s="T447">Daška</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1946" n="HIAT:w" s="T448">ej</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1949" n="HIAT:w" s="T449">dʼăbaktərlam</ts>
                  <nts id="Seg_1950" n="HIAT:ip">.</nts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1953" n="HIAT:u" s="T450">
                  <nts id="Seg_1954" n="HIAT:ip">(</nts>
                  <nts id="Seg_1955" n="HIAT:ip">(</nts>
                  <ats e="T451" id="Seg_1956" n="HIAT:non-pho" s="T450">BRK</ats>
                  <nts id="Seg_1957" n="HIAT:ip">)</nts>
                  <nts id="Seg_1958" n="HIAT:ip">)</nts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1962" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1964" n="HIAT:w" s="T451">Nada</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1967" n="HIAT:w" s="T452">măna</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1970" n="HIAT:w" s="T453">ešši</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1972" n="HIAT:ip">(</nts>
                  <ts e="T455" id="Seg_1974" n="HIAT:w" s="T454">măn</ts>
                  <nts id="Seg_1975" n="HIAT:ip">)</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1978" n="HIAT:w" s="T455">băzəjsʼtə</ts>
                  <nts id="Seg_1979" n="HIAT:ip">.</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1982" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1984" n="HIAT:w" s="T456">Dĭ</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1987" n="HIAT:w" s="T457">ugaːndə</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1990" n="HIAT:w" s="T458">sagər</ts>
                  <nts id="Seg_1991" n="HIAT:ip">.</nts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1994" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1996" n="HIAT:w" s="T459">Bar</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1999" n="HIAT:w" s="T460">tüʔpi</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_2002" n="HIAT:w" s="T461">iʔgö</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_2006" n="HIAT:u" s="T462">
                  <nts id="Seg_2007" n="HIAT:ip">(</nts>
                  <ts e="T463" id="Seg_2009" n="HIAT:w" s="T462">Kĭškiem-</ts>
                  <nts id="Seg_2010" n="HIAT:ip">)</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_2013" n="HIAT:w" s="T463">bar</ts>
                  <nts id="Seg_2014" n="HIAT:ip">,</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_2017" n="HIAT:w" s="T464">kĭškəbiem</ts>
                  <nts id="Seg_2018" n="HIAT:ip">.</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_2021" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_2023" n="HIAT:w" s="T465">Dĭ</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_2026" n="HIAT:w" s="T466">kolaːmbi</ts>
                  <nts id="Seg_2027" n="HIAT:ip">.</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_2030" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_2032" n="HIAT:w" s="T467">Dĭgəttə</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_2035" n="HIAT:w" s="T468">nada</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_2038" n="HIAT:w" s="T469">kürzittə</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_2041" n="HIAT:w" s="T470">dĭm</ts>
                  <nts id="Seg_2042" n="HIAT:ip">.</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_2045" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_2047" n="HIAT:w" s="T471">Dĭgəttə</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2049" n="HIAT:ip">(</nts>
                  <ts e="T473" id="Seg_2051" n="HIAT:w" s="T472">am-</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_2054" n="HIAT:w" s="T473">amzit-</ts>
                  <nts id="Seg_2055" n="HIAT:ip">)</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2058" n="HIAT:w" s="T474">amorzittə</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_2061" n="HIAT:w" s="T475">nada</ts>
                  <nts id="Seg_2062" n="HIAT:ip">.</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_2065" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_2067" n="HIAT:w" s="T476">Dĭgəttə</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2069" n="HIAT:ip">(</nts>
                  <ts e="T478" id="Seg_2071" n="HIAT:w" s="T477">ku-</ts>
                  <nts id="Seg_2072" n="HIAT:ip">)</nts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_2075" n="HIAT:w" s="T478">kunolzittə</ts>
                  <nts id="Seg_2076" n="HIAT:ip">.</nts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_2079" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_2081" n="HIAT:w" s="T479">Ordə</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_2084" n="HIAT:w" s="T480">eššim</ts>
                  <nts id="Seg_2085" n="HIAT:ip">.</nts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_2088" n="HIAT:u" s="T481">
                  <ts e="T483" id="Seg_2090" n="HIAT:w" s="T481">Bar-</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_2093" n="HIAT:w" s="T483">-gaʔ</ts>
                  <nts id="Seg_2094" n="HIAT:ip">.</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_2097" n="HIAT:u" s="T484">
                  <nts id="Seg_2098" n="HIAT:ip">(</nts>
                  <nts id="Seg_2099" n="HIAT:ip">(</nts>
                  <ats e="T485" id="Seg_2100" n="HIAT:non-pho" s="T484">DMG</ats>
                  <nts id="Seg_2101" n="HIAT:ip">)</nts>
                  <nts id="Seg_2102" n="HIAT:ip">)</nts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_2106" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_2108" n="HIAT:w" s="T485">Teinen</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_2111" n="HIAT:w" s="T486">urgo</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_2114" n="HIAT:w" s="T487">dʼala</ts>
                  <nts id="Seg_2115" n="HIAT:ip">.</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_2118" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_2120" n="HIAT:w" s="T488">A</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2123" n="HIAT:w" s="T489">il</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2126" n="HIAT:w" s="T490">üge</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_2129" n="HIAT:w" s="T491">togonorlaʔbəʔjə</ts>
                  <nts id="Seg_2130" n="HIAT:ip">.</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_2133" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_2135" n="HIAT:w" s="T492">Toltanoʔ</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2138" n="HIAT:w" s="T493">amlaʔbəʔjə</ts>
                  <nts id="Seg_2139" n="HIAT:ip">.</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_2142" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_2144" n="HIAT:w" s="T494">Tajirlaʔbəʔjə</ts>
                  <nts id="Seg_2145" n="HIAT:ip">.</nts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_2148" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_2150" n="HIAT:w" s="T495">A</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_2153" n="HIAT:w" s="T496">miʔ</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2156" n="HIAT:w" s="T497">amnolaʔbəbaʔ</ts>
                  <nts id="Seg_2157" n="HIAT:ip">.</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_2160" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_2162" n="HIAT:w" s="T498">Kabarləj</ts>
                  <nts id="Seg_2163" n="HIAT:ip">,</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2166" n="HIAT:w" s="T499">daška</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2168" n="HIAT:ip">(</nts>
                  <ts e="T501" id="Seg_2170" n="HIAT:w" s="T500">ej=</ts>
                  <nts id="Seg_2171" n="HIAT:ip">)</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2174" n="HIAT:w" s="T501">dʼăbaktərzittə</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2177" n="HIAT:w" s="T502">nʼe</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2180" n="HIAT:w" s="T503">axota</ts>
                  <nts id="Seg_2181" n="HIAT:ip">.</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_2184" n="HIAT:u" s="T504">
                  <nts id="Seg_2185" n="HIAT:ip">(</nts>
                  <nts id="Seg_2186" n="HIAT:ip">(</nts>
                  <ats e="T505" id="Seg_2187" n="HIAT:non-pho" s="T504">BRK</ats>
                  <nts id="Seg_2188" n="HIAT:ip">)</nts>
                  <nts id="Seg_2189" n="HIAT:ip">)</nts>
                  <nts id="Seg_2190" n="HIAT:ip">.</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_2193" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2195" n="HIAT:w" s="T505">Teinen</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2198" n="HIAT:w" s="T506">urgo</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2201" n="HIAT:w" s="T507">dʼala</ts>
                  <nts id="Seg_2202" n="HIAT:ip">.</nts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_2205" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_2207" n="HIAT:w" s="T508">Măna</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2210" n="HIAT:w" s="T509">tuganbə</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2213" n="HIAT:w" s="T510">kăštəbiʔi</ts>
                  <nts id="Seg_2214" n="HIAT:ip">.</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_2217" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_2219" n="HIAT:w" s="T511">Kanžəbəj</ts>
                  <nts id="Seg_2220" n="HIAT:ip">.</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_2223" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_2225" n="HIAT:w" s="T512">Dĭn</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2228" n="HIAT:w" s="T513">ara</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2231" n="HIAT:w" s="T514">bĭtləbəj</ts>
                  <nts id="Seg_2232" n="HIAT:ip">.</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T518" id="Seg_2235" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_2237" n="HIAT:w" s="T515">Dĭn</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2240" n="HIAT:w" s="T516">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2243" n="HIAT:w" s="T517">garmonʼənə</ts>
                  <nts id="Seg_2244" n="HIAT:ip">.</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_2247" n="HIAT:u" s="T518">
                  <nts id="Seg_2248" n="HIAT:ip">(</nts>
                  <ts e="T519" id="Seg_2250" n="HIAT:w" s="T518">M-</ts>
                  <nts id="Seg_2251" n="HIAT:ip">)</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2254" n="HIAT:w" s="T519">Miʔ</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2257" n="HIAT:w" s="T520">bar</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2260" n="HIAT:w" s="T521">suʔməleʔbəbeʔ</ts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T523" id="Seg_2264" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_2266" n="HIAT:w" s="T522">Bar</ts>
                  <nts id="Seg_2267" n="HIAT:ip">.</nts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2270" n="HIAT:u" s="T523">
                  <nts id="Seg_2271" n="HIAT:ip">(</nts>
                  <nts id="Seg_2272" n="HIAT:ip">(</nts>
                  <ats e="T524" id="Seg_2273" n="HIAT:non-pho" s="T523">BRK</ats>
                  <nts id="Seg_2274" n="HIAT:ip">)</nts>
                  <nts id="Seg_2275" n="HIAT:ip">)</nts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2279" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2281" n="HIAT:w" s="T524">Oʔb</ts>
                  <nts id="Seg_2282" n="HIAT:ip">,</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2285" n="HIAT:w" s="T525">oʔb</ts>
                  <nts id="Seg_2286" n="HIAT:ip">,</nts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2289" n="HIAT:w" s="T526">šide</ts>
                  <nts id="Seg_2290" n="HIAT:ip">,</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2293" n="HIAT:w" s="T527">nagur</ts>
                  <nts id="Seg_2294" n="HIAT:ip">,</nts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2297" n="HIAT:w" s="T528">teʔtə</ts>
                  <nts id="Seg_2298" n="HIAT:ip">,</nts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2301" n="HIAT:w" s="T529">sumna</ts>
                  <nts id="Seg_2302" n="HIAT:ip">,</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2305" n="HIAT:w" s="T530">sejʔpü</ts>
                  <nts id="Seg_2306" n="HIAT:ip">.</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_2309" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2311" n="HIAT:w" s="T531">Amitun</ts>
                  <nts id="Seg_2312" n="HIAT:ip">,</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2315" n="HIAT:w" s="T532">šinteʔtə</ts>
                  <nts id="Seg_2316" n="HIAT:ip">.</nts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T541" id="Seg_2318" n="sc" s="T540">
               <ts e="T541" id="Seg_2320" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2322" n="HIAT:w" s="T540">Bʼeʔ</ts>
                  <nts id="Seg_2323" n="HIAT:ip">.</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T629" id="Seg_2325" n="sc" s="T542">
               <ts e="T550" id="Seg_2327" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2329" n="HIAT:w" s="T542">Bʼeʔ</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2332" n="HIAT:w" s="T543">onʼiʔ</ts>
                  <nts id="Seg_2333" n="HIAT:ip">,</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2336" n="HIAT:w" s="T544">bʼeʔ</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2339" n="HIAT:w" s="T545">šide</ts>
                  <nts id="Seg_2340" n="HIAT:ip">,</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2343" n="HIAT:w" s="T546">bʼeʔ</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2346" n="HIAT:w" s="T547">sumna</ts>
                  <nts id="Seg_2347" n="HIAT:ip">,</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2350" n="HIAT:w" s="T548">bʼeʔ</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2353" n="HIAT:w" s="T549">muktuʔ</ts>
                  <nts id="Seg_2354" n="HIAT:ip">.</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2357" n="HIAT:u" s="T550">
                  <nts id="Seg_2358" n="HIAT:ip">(</nts>
                  <nts id="Seg_2359" n="HIAT:ip">(</nts>
                  <ats e="T551" id="Seg_2360" n="HIAT:non-pho" s="T550">BRK</ats>
                  <nts id="Seg_2361" n="HIAT:ip">)</nts>
                  <nts id="Seg_2362" n="HIAT:ip">)</nts>
                  <nts id="Seg_2363" n="HIAT:ip">.</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2366" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2368" n="HIAT:w" s="T551">Măn</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2371" n="HIAT:w" s="T552">unnʼa</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2374" n="HIAT:w" s="T553">šobiam</ts>
                  <nts id="Seg_2375" n="HIAT:ip">,</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2378" n="HIAT:w" s="T554">a</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2381" n="HIAT:w" s="T555">dĭzeŋ</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2384" n="HIAT:w" s="T556">bar</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2387" n="HIAT:w" s="T557">sumna</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2390" n="HIAT:w" s="T558">šobiʔi</ts>
                  <nts id="Seg_2391" n="HIAT:ip">.</nts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2394" n="HIAT:u" s="T559">
                  <nts id="Seg_2395" n="HIAT:ip">(</nts>
                  <nts id="Seg_2396" n="HIAT:ip">(</nts>
                  <ats e="T560" id="Seg_2397" n="HIAT:non-pho" s="T559">BRK</ats>
                  <nts id="Seg_2398" n="HIAT:ip">)</nts>
                  <nts id="Seg_2399" n="HIAT:ip">)</nts>
                  <nts id="Seg_2400" n="HIAT:ip">.</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_2403" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2405" n="HIAT:w" s="T560">Măn</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2408" n="HIAT:w" s="T561">mĭj</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2410" n="HIAT:ip">(</nts>
                  <ts e="T563" id="Seg_2412" n="HIAT:w" s="T562">mĭr-</ts>
                  <nts id="Seg_2413" n="HIAT:ip">)</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2416" n="HIAT:w" s="T563">mĭnzəriem</ts>
                  <nts id="Seg_2417" n="HIAT:ip">.</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_2420" n="HIAT:u" s="T564">
                  <ts e="T565" id="Seg_2422" n="HIAT:w" s="T564">Ugaːndə</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2425" n="HIAT:w" s="T565">jakšə</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2428" n="HIAT:w" s="T566">mĭj</ts>
                  <nts id="Seg_2429" n="HIAT:ip">.</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T569" id="Seg_2432" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_2434" n="HIAT:w" s="T567">Nʼamga</ts>
                  <nts id="Seg_2435" n="HIAT:ip">,</nts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2438" n="HIAT:w" s="T568">ujazʼiʔ</ts>
                  <nts id="Seg_2439" n="HIAT:ip">.</nts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2442" n="HIAT:u" s="T569">
                  <ts e="T570" id="Seg_2444" n="HIAT:w" s="T569">Köbergən</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2447" n="HIAT:w" s="T570">embiem</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2450" n="HIAT:w" s="T571">bar</ts>
                  <nts id="Seg_2451" n="HIAT:ip">.</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2454" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2456" n="HIAT:w" s="T572">Tus</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2459" n="HIAT:w" s="T573">embiem</ts>
                  <nts id="Seg_2460" n="HIAT:ip">.</nts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2463" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2465" n="HIAT:w" s="T574">Toltanoʔ</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2468" n="HIAT:w" s="T575">ambiem</ts>
                  <nts id="Seg_2469" n="HIAT:ip">.</nts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2472" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2474" n="HIAT:w" s="T576">Amnaʔ</ts>
                  <nts id="Seg_2475" n="HIAT:ip">,</nts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2478" n="HIAT:w" s="T577">amoraʔ</ts>
                  <nts id="Seg_2479" n="HIAT:ip">!</nts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2482" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2484" n="HIAT:w" s="T578">Obberəj</ts>
                  <nts id="Seg_2485" n="HIAT:ip">.</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2488" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2490" n="HIAT:w" s="T579">Kabarləj</ts>
                  <nts id="Seg_2491" n="HIAT:ip">.</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2494" n="HIAT:u" s="T580">
                  <nts id="Seg_2495" n="HIAT:ip">(</nts>
                  <nts id="Seg_2496" n="HIAT:ip">(</nts>
                  <ats e="T581" id="Seg_2497" n="HIAT:non-pho" s="T580">BRK</ats>
                  <nts id="Seg_2498" n="HIAT:ip">)</nts>
                  <nts id="Seg_2499" n="HIAT:ip">)</nts>
                  <nts id="Seg_2500" n="HIAT:ip">.</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2503" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2505" n="HIAT:w" s="T581">Ugaːndə</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2508" n="HIAT:w" s="T582">kuštü</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2511" n="HIAT:w" s="T583">kuza</ts>
                  <nts id="Seg_2512" n="HIAT:ip">.</nts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2515" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2517" n="HIAT:w" s="T584">Măn</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2520" n="HIAT:w" s="T585">dĭʔnə</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2523" n="HIAT:w" s="T586">pimniem</ts>
                  <nts id="Seg_2524" n="HIAT:ip">.</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2527" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_2529" n="HIAT:w" s="T587">Dĭ</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2532" n="HIAT:w" s="T588">bar</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2535" n="HIAT:w" s="T589">kutləj</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2539" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2541" n="HIAT:w" s="T590">Măn</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2544" n="HIAT:w" s="T591">šaʔlaːmbiam</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2547" n="HIAT:w" s="T592">štobɨ</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2550" n="HIAT:w" s="T593">dĭ</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2553" n="HIAT:w" s="T594">măna</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2556" n="HIAT:w" s="T595">ej</ts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2559" n="HIAT:w" s="T596">kubi</ts>
                  <nts id="Seg_2560" n="HIAT:ip">.</nts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2563" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2565" n="HIAT:w" s="T597">Măn</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2568" n="HIAT:w" s="T598">dʼijenə</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2571" n="HIAT:w" s="T599">nuʔməluʔləm</ts>
                  <nts id="Seg_2572" n="HIAT:ip">.</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2575" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2577" n="HIAT:w" s="T600">Dĭ</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2580" n="HIAT:w" s="T601">măna</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2582" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_2584" n="HIAT:w" s="T602">ej=</ts>
                  <nts id="Seg_2585" n="HIAT:ip">)</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2588" n="HIAT:w" s="T603">dĭn</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2591" n="HIAT:w" s="T604">ej</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2594" n="HIAT:w" s="T605">kuləj</ts>
                  <nts id="Seg_2595" n="HIAT:ip">.</nts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_2598" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2600" n="HIAT:w" s="T606">Kabarləj</ts>
                  <nts id="Seg_2601" n="HIAT:ip">.</nts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2604" n="HIAT:u" s="T607">
                  <nts id="Seg_2605" n="HIAT:ip">(</nts>
                  <nts id="Seg_2606" n="HIAT:ip">(</nts>
                  <ats e="T608" id="Seg_2607" n="HIAT:non-pho" s="T607">BRK</ats>
                  <nts id="Seg_2608" n="HIAT:ip">)</nts>
                  <nts id="Seg_2609" n="HIAT:ip">)</nts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2613" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2615" n="HIAT:w" s="T608">Tibizeŋ</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2618" n="HIAT:w" s="T609">iʔgö</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2621" n="HIAT:w" s="T610">oʔbdəbiʔi</ts>
                  <nts id="Seg_2622" n="HIAT:ip">.</nts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2625" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2627" n="HIAT:w" s="T611">Ĭmbidə</ts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2630" n="HIAT:w" s="T612">bar</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2633" n="HIAT:w" s="T613">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_2634" n="HIAT:ip">,</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2637" n="HIAT:w" s="T614">nada</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2640" n="HIAT:w" s="T615">kanzittə</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2643" n="HIAT:w" s="T616">nʼilgösʼtə</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2645" n="HIAT:ip">(</nts>
                  <ts e="T618" id="Seg_2647" n="HIAT:w" s="T617">ĭm-</ts>
                  <nts id="Seg_2648" n="HIAT:ip">)</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2651" n="HIAT:w" s="T618">ĭmbi</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2654" n="HIAT:w" s="T619">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_2655" n="HIAT:ip">.</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T625" id="Seg_2658" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_2660" n="HIAT:w" s="T620">Măn</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2663" n="HIAT:w" s="T621">dĭgəttə</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2665" n="HIAT:ip">(</nts>
                  <ts e="T623" id="Seg_2667" n="HIAT:w" s="T622">tĭmnə-</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2670" n="HIAT:w" s="T623">tĭmzit-</ts>
                  <nts id="Seg_2671" n="HIAT:ip">)</nts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2674" n="HIAT:w" s="T624">tĭmlem</ts>
                  <nts id="Seg_2675" n="HIAT:ip">.</nts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2678" n="HIAT:u" s="T625">
                  <ts e="T626" id="Seg_2680" n="HIAT:w" s="T625">Bar</ts>
                  <nts id="Seg_2681" n="HIAT:ip">,</nts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2684" n="HIAT:w" s="T626">daška</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2687" n="HIAT:w" s="T627">ej</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2690" n="HIAT:w" s="T628">dʼăbaktərliam</ts>
                  <nts id="Seg_2691" n="HIAT:ip">.</nts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T695" id="Seg_2693" n="sc" s="T634">
               <ts e="T638" id="Seg_2695" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2697" n="HIAT:w" s="T634">Dĭ</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2700" n="HIAT:w" s="T635">kuza</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2703" n="HIAT:w" s="T636">bar</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2706" n="HIAT:w" s="T637">jezerik</ts>
                  <nts id="Seg_2707" n="HIAT:ip">.</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2710" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2712" n="HIAT:w" s="T638">Inegən</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2715" n="HIAT:w" s="T639">amnoluʔpi</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2719" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2721" n="HIAT:w" s="T640">Dĭgəttə</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2724" n="HIAT:w" s="T641">üzəbi</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2727" n="HIAT:w" s="T642">dʼünə</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2731" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2733" n="HIAT:w" s="T643">Dĭgəttə</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2736" n="HIAT:w" s="T644">inet</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2739" n="HIAT:w" s="T645">nuʔməluʔpi</ts>
                  <nts id="Seg_2740" n="HIAT:ip">.</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T651" id="Seg_2743" n="HIAT:u" s="T646">
                  <nts id="Seg_2744" n="HIAT:ip">(</nts>
                  <ts e="T647" id="Seg_2746" n="HIAT:w" s="T646">Üžü</ts>
                  <nts id="Seg_2747" n="HIAT:ip">,</nts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2750" n="HIAT:w" s="T647">üžü</ts>
                  <nts id="Seg_2751" n="HIAT:ip">,</nts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2754" n="HIAT:w" s="T648">üžüd-</ts>
                  <nts id="Seg_2755" n="HIAT:ip">)</nts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2758" n="HIAT:w" s="T649">Üžü</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2761" n="HIAT:w" s="T650">dʼürdəbi</ts>
                  <nts id="Seg_2762" n="HIAT:ip">.</nts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T653" id="Seg_2765" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2767" n="HIAT:w" s="T651">Maluʔpi</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2770" n="HIAT:w" s="T652">bar</ts>
                  <nts id="Seg_2771" n="HIAT:ip">.</nts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2774" n="HIAT:u" s="T653">
                  <nts id="Seg_2775" n="HIAT:ip">(</nts>
                  <ts e="T654" id="Seg_2777" n="HIAT:w" s="T653">Kunollaʔbə=</ts>
                  <nts id="Seg_2778" n="HIAT:ip">)</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2781" n="HIAT:w" s="T654">Kunollaʔ</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2784" n="HIAT:w" s="T655">iʔbolaʔbə</ts>
                  <nts id="Seg_2785" n="HIAT:ip">,</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2788" n="HIAT:w" s="T656">saʔməluʔpi</ts>
                  <nts id="Seg_2789" n="HIAT:ip">.</nts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2792" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2794" n="HIAT:w" s="T657">Bar</ts>
                  <nts id="Seg_2795" n="HIAT:ip">,</nts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2798" n="HIAT:w" s="T658">kabarləj</ts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2802" n="HIAT:u" s="T659">
                  <nts id="Seg_2803" n="HIAT:ip">(</nts>
                  <nts id="Seg_2804" n="HIAT:ip">(</nts>
                  <ats e="T660" id="Seg_2805" n="HIAT:non-pho" s="T659">BRK</ats>
                  <nts id="Seg_2806" n="HIAT:ip">)</nts>
                  <nts id="Seg_2807" n="HIAT:ip">)</nts>
                  <nts id="Seg_2808" n="HIAT:ip">.</nts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2811" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2813" n="HIAT:w" s="T660">Nʼizeŋ</ts>
                  <nts id="Seg_2814" n="HIAT:ip">,</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2817" n="HIAT:w" s="T661">koʔbsaŋ</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2820" n="HIAT:w" s="T662">bar</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2823" n="HIAT:w" s="T663">iʔgö</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2826" n="HIAT:w" s="T664">oʔbdəbiʔi</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2829" n="HIAT:w" s="T665">bar</ts>
                  <nts id="Seg_2830" n="HIAT:ip">,</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2833" n="HIAT:w" s="T666">nüjleʔbəʔjə</ts>
                  <nts id="Seg_2834" n="HIAT:ip">.</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2837" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_2839" n="HIAT:w" s="T667">Garmonʼə</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2842" n="HIAT:w" s="T668">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_2843" n="HIAT:ip">,</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2846" n="HIAT:w" s="T669">suʔmileʔbəʔjə</ts>
                  <nts id="Seg_2847" n="HIAT:ip">.</nts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2850" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2852" n="HIAT:w" s="T670">Kabarləj</ts>
                  <nts id="Seg_2853" n="HIAT:ip">.</nts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2856" n="HIAT:u" s="T671">
                  <nts id="Seg_2857" n="HIAT:ip">(</nts>
                  <nts id="Seg_2858" n="HIAT:ip">(</nts>
                  <ats e="T672" id="Seg_2859" n="HIAT:non-pho" s="T671">BRK</ats>
                  <nts id="Seg_2860" n="HIAT:ip">)</nts>
                  <nts id="Seg_2861" n="HIAT:ip">)</nts>
                  <nts id="Seg_2862" n="HIAT:ip">.</nts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2865" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2867" n="HIAT:w" s="T672">Măn</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2870" n="HIAT:w" s="T673">dʼijenə</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2873" n="HIAT:w" s="T674">mĭmbiem</ts>
                  <nts id="Seg_2874" n="HIAT:ip">.</nts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2877" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2879" n="HIAT:w" s="T675">Keʔbde</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2882" n="HIAT:w" s="T676">nĭŋgəbiem</ts>
                  <nts id="Seg_2883" n="HIAT:ip">.</nts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T679" id="Seg_2886" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2888" n="HIAT:w" s="T677">Kălba</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2891" n="HIAT:w" s="T678">nĭŋgəbiem</ts>
                  <nts id="Seg_2892" n="HIAT:ip">.</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T683" id="Seg_2895" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_2897" n="HIAT:w" s="T679">Pirogəʔi</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2900" n="HIAT:w" s="T680">pürbiem</ts>
                  <nts id="Seg_2901" n="HIAT:ip">,</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2904" n="HIAT:w" s="T681">šiʔnʼileʔ</ts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2907" n="HIAT:w" s="T682">mĭlem</ts>
                  <nts id="Seg_2908" n="HIAT:ip">.</nts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2911" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_2913" n="HIAT:w" s="T683">Kabarləj</ts>
                  <nts id="Seg_2914" n="HIAT:ip">.</nts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2917" n="HIAT:u" s="T684">
                  <nts id="Seg_2918" n="HIAT:ip">(</nts>
                  <nts id="Seg_2919" n="HIAT:ip">(</nts>
                  <ats e="T685" id="Seg_2920" n="HIAT:non-pho" s="T684">BRK</ats>
                  <nts id="Seg_2921" n="HIAT:ip">)</nts>
                  <nts id="Seg_2922" n="HIAT:ip">)</nts>
                  <nts id="Seg_2923" n="HIAT:ip">.</nts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2926" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2928" n="HIAT:w" s="T685">Măn</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2931" n="HIAT:w" s="T686">iam</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2934" n="HIAT:w" s="T687">nu</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2937" n="HIAT:w" s="T688">ibi</ts>
                  <nts id="Seg_2938" n="HIAT:ip">,</nts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2941" n="HIAT:w" s="T689">abam</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2944" n="HIAT:w" s="T690">dʼirək</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2947" n="HIAT:w" s="T691">ibi</ts>
                  <nts id="Seg_2948" n="HIAT:ip">.</nts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T695" id="Seg_2951" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2953" n="HIAT:w" s="T692">Šide</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2956" n="HIAT:w" s="T693">nʼi</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2959" n="HIAT:w" s="T694">ibiʔi</ts>
                  <nts id="Seg_2960" n="HIAT:ip">.</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T714" id="Seg_2962" n="sc" s="T698">
               <ts e="T701" id="Seg_2964" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_2966" n="HIAT:w" s="T698">Muktuʔ</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2969" n="HIAT:w" s="T699">koʔbdaŋ</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2972" n="HIAT:w" s="T700">ibi</ts>
                  <nts id="Seg_2973" n="HIAT:ip">.</nts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2976" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2978" n="HIAT:w" s="T701">Bar</ts>
                  <nts id="Seg_2979" n="HIAT:ip">,</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2982" n="HIAT:w" s="T702">kabarləj</ts>
                  <nts id="Seg_2983" n="HIAT:ip">.</nts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2986" n="HIAT:u" s="T703">
                  <nts id="Seg_2987" n="HIAT:ip">(</nts>
                  <nts id="Seg_2988" n="HIAT:ip">(</nts>
                  <ats e="T704" id="Seg_2989" n="HIAT:non-pho" s="T703">BRK</ats>
                  <nts id="Seg_2990" n="HIAT:ip">)</nts>
                  <nts id="Seg_2991" n="HIAT:ip">)</nts>
                  <nts id="Seg_2992" n="HIAT:ip">.</nts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2995" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2997" n="HIAT:w" s="T704">Nagur</ts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_3000" n="HIAT:w" s="T705">külaːmbiʔi</ts>
                  <nts id="Seg_3001" n="HIAT:ip">,</nts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_3004" n="HIAT:w" s="T706">a</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_3007" n="HIAT:w" s="T707">nagur</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_3010" n="HIAT:w" s="T708">ej</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_3013" n="HIAT:w" s="T709">kübiʔi</ts>
                  <nts id="Seg_3014" n="HIAT:ip">.</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_3017" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_3019" n="HIAT:w" s="T710">Măn</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_3022" n="HIAT:w" s="T711">abam</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_3025" n="HIAT:w" s="T712">albuga</ts>
                  <nts id="Seg_3026" n="HIAT:ip">…</nts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T908" id="Seg_3028" n="sc" s="T719">
               <ts e="T722" id="Seg_3030" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_3032" n="HIAT:w" s="T719">Bʼeʔ</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_3035" n="HIAT:w" s="T720">sumna</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_3038" n="HIAT:w" s="T721">kuʔpi</ts>
                  <nts id="Seg_3039" n="HIAT:ip">.</nts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_3042" n="HIAT:u" s="T722">
                  <ts e="T724" id="Seg_3044" n="HIAT:w" s="T722">Bar</ts>
                  <nts id="Seg_3045" n="HIAT:ip">.</nts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_3048" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_3050" n="HIAT:w" s="T724">Kabarləj</ts>
                  <nts id="Seg_3051" n="HIAT:ip">.</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_3054" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_3056" n="HIAT:w" s="T725">Tažəp</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_3059" n="HIAT:w" s="T726">išo</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_3062" n="HIAT:w" s="T727">băra</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_3065" n="HIAT:w" s="T728">deʔpi</ts>
                  <nts id="Seg_3066" n="HIAT:ip">.</nts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_3069" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_3071" n="HIAT:w" s="T729">Kabarləj</ts>
                  <nts id="Seg_3072" n="HIAT:ip">.</nts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_3075" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_3077" n="HIAT:w" s="T730">Nu</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_3080" n="HIAT:w" s="T731">kuza</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3083" n="HIAT:w" s="T732">ĭzemnie</ts>
                  <nts id="Seg_3084" n="HIAT:ip">.</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_3087" n="HIAT:u" s="T733">
                  <nts id="Seg_3088" n="HIAT:ip">(</nts>
                  <nts id="Seg_3089" n="HIAT:ip">(</nts>
                  <ats e="T734" id="Seg_3090" n="HIAT:non-pho" s="T733">BRK</ats>
                  <nts id="Seg_3091" n="HIAT:ip">)</nts>
                  <nts id="Seg_3092" n="HIAT:ip">)</nts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_3096" n="HIAT:u" s="T734">
                  <ts e="T735" id="Seg_3098" n="HIAT:w" s="T734">Dĭgəttə</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3101" n="HIAT:w" s="T735">kambi</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_3104" n="HIAT:w" s="T736">šamandə</ts>
                  <nts id="Seg_3105" n="HIAT:ip">,</nts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_3108" n="HIAT:w" s="T737">dĭ</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3110" n="HIAT:ip">(</nts>
                  <ts e="T739" id="Seg_3112" n="HIAT:w" s="T738">dĭ-</ts>
                  <nts id="Seg_3113" n="HIAT:ip">)</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3116" n="HIAT:w" s="T739">dĭm</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_3119" n="HIAT:w" s="T740">bar</ts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3122" n="HIAT:w" s="T741">dʼazirlaʔpi</ts>
                  <nts id="Seg_3123" n="HIAT:ip">.</nts>
                  <nts id="Seg_3124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_3126" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_3128" n="HIAT:w" s="T742">Daška</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3131" n="HIAT:w" s="T743">bar</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3134" n="HIAT:w" s="T744">naverna</ts>
                  <nts id="Seg_3135" n="HIAT:ip">.</nts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_3138" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_3140" n="HIAT:w" s="T745">Măn</ts>
                  <nts id="Seg_3141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3143" n="HIAT:w" s="T746">ugaːndə</ts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_3146" n="HIAT:w" s="T747">püjöliam</ts>
                  <nts id="Seg_3147" n="HIAT:ip">.</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_3150" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_3152" n="HIAT:w" s="T748">Amnaʔ</ts>
                  <nts id="Seg_3153" n="HIAT:ip">,</nts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3156" n="HIAT:w" s="T749">amoraʔ</ts>
                  <nts id="Seg_3157" n="HIAT:ip">,</nts>
                  <nts id="Seg_3158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3160" n="HIAT:w" s="T750">šindi</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_3163" n="HIAT:w" s="T751">tănan</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3166" n="HIAT:w" s="T752">ej</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_3169" n="HIAT:w" s="T753">mĭlie</ts>
                  <nts id="Seg_3170" n="HIAT:ip">.</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_3173" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_3175" n="HIAT:w" s="T754">Dĭgəttə</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3177" n="HIAT:ip">(</nts>
                  <ts e="T756" id="Seg_3179" n="HIAT:w" s="T755">a-</ts>
                  <nts id="Seg_3180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3182" n="HIAT:w" s="T756">a-</ts>
                  <nts id="Seg_3183" n="HIAT:ip">)</nts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3186" n="HIAT:w" s="T757">iʔbeʔ</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3189" n="HIAT:w" s="T758">da</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3192" n="HIAT:w" s="T759">kunolaʔ</ts>
                  <nts id="Seg_3193" n="HIAT:ip">!</nts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_3196" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_3198" n="HIAT:w" s="T760">Kabarləj</ts>
                  <nts id="Seg_3199" n="HIAT:ip">.</nts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_3202" n="HIAT:u" s="T761">
                  <nts id="Seg_3203" n="HIAT:ip">(</nts>
                  <nts id="Seg_3204" n="HIAT:ip">(</nts>
                  <ats e="T762" id="Seg_3205" n="HIAT:non-pho" s="T761">BRK</ats>
                  <nts id="Seg_3206" n="HIAT:ip">)</nts>
                  <nts id="Seg_3207" n="HIAT:ip">)</nts>
                  <nts id="Seg_3208" n="HIAT:ip">.</nts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3211" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3213" n="HIAT:w" s="T762">Esseŋ</ts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3216" n="HIAT:w" s="T763">nʼiʔnen</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3219" n="HIAT:w" s="T764">bar</ts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3221" n="HIAT:ip">(</nts>
                  <ts e="T766" id="Seg_3223" n="HIAT:w" s="T765">sʼ-</ts>
                  <nts id="Seg_3224" n="HIAT:ip">)</nts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3227" n="HIAT:w" s="T766">sʼarlaʔbəʔjə</ts>
                  <nts id="Seg_3228" n="HIAT:ip">.</nts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_3231" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3233" n="HIAT:w" s="T767">Kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_3234" n="HIAT:ip">,</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3237" n="HIAT:w" s="T768">dʼabrolaʔbəʔjə</ts>
                  <nts id="Seg_3238" n="HIAT:ip">.</nts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3241" n="HIAT:u" s="T769">
                  <ts e="T769.tx-PKZ.1" id="Seg_3243" n="HIAT:w" s="T769">Na</ts>
                  <nts id="Seg_3244" n="HIAT:ip">_</nts>
                  <ts e="T770" id="Seg_3246" n="HIAT:w" s="T769.tx-PKZ.1">što</ts>
                  <nts id="Seg_3247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3249" n="HIAT:w" s="T770">dʼabrolial</ts>
                  <nts id="Seg_3250" n="HIAT:ip">?</nts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_3253" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3255" n="HIAT:w" s="T771">Jakšə</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3258" n="HIAT:w" s="T772">nada</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3261" n="HIAT:w" s="T773">sʼarzittə</ts>
                  <nts id="Seg_3262" n="HIAT:ip">.</nts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T776" id="Seg_3265" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_3267" n="HIAT:w" s="T774">Iʔ</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3270" n="HIAT:w" s="T775">dʼabrosʼtə</ts>
                  <nts id="Seg_3271" n="HIAT:ip">!</nts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_3274" n="HIAT:u" s="T776">
                  <ts e="T777" id="Seg_3276" n="HIAT:w" s="T776">Kabarləj</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3279" n="HIAT:w" s="T777">naverna</ts>
                  <nts id="Seg_3280" n="HIAT:ip">.</nts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3283" n="HIAT:u" s="T778">
                  <nts id="Seg_3284" n="HIAT:ip">(</nts>
                  <nts id="Seg_3285" n="HIAT:ip">(</nts>
                  <ats e="T779" id="Seg_3286" n="HIAT:non-pho" s="T778">BRK</ats>
                  <nts id="Seg_3287" n="HIAT:ip">)</nts>
                  <nts id="Seg_3288" n="HIAT:ip">)</nts>
                  <nts id="Seg_3289" n="HIAT:ip">.</nts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3292" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3294" n="HIAT:w" s="T779">Iʔ</ts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3297" n="HIAT:w" s="T780">dʼabrogaʔ</ts>
                  <nts id="Seg_3298" n="HIAT:ip">!</nts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_3301" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3303" n="HIAT:w" s="T781">Nada</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3306" n="HIAT:w" s="T782">jakšə</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3309" n="HIAT:w" s="T783">sʼarzittə</ts>
                  <nts id="Seg_3310" n="HIAT:ip">.</nts>
                  <nts id="Seg_3311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T786" id="Seg_3313" n="HIAT:u" s="T784">
                  <ts e="T784.tx-PKZ.1" id="Seg_3315" n="HIAT:w" s="T784">Na</ts>
                  <nts id="Seg_3316" n="HIAT:ip">_</nts>
                  <ts e="T785" id="Seg_3318" n="HIAT:w" s="T784.tx-PKZ.1">što</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3321" n="HIAT:w" s="T785">dʼabrolaʔbəl</ts>
                  <nts id="Seg_3322" n="HIAT:ip">?</nts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_3325" n="HIAT:u" s="T786">
                  <nts id="Seg_3326" n="HIAT:ip">(</nts>
                  <nts id="Seg_3327" n="HIAT:ip">(</nts>
                  <ats e="T787" id="Seg_3328" n="HIAT:non-pho" s="T786">BRK</ats>
                  <nts id="Seg_3329" n="HIAT:ip">)</nts>
                  <nts id="Seg_3330" n="HIAT:ip">)</nts>
                  <nts id="Seg_3331" n="HIAT:ip">.</nts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3334" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_3336" n="HIAT:w" s="T787">Măn</ts>
                  <nts id="Seg_3337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3339" n="HIAT:w" s="T788">teinen</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3342" n="HIAT:w" s="T789">nubiam</ts>
                  <nts id="Seg_3343" n="HIAT:ip">.</nts>
                  <nts id="Seg_3344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3346" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3348" n="HIAT:w" s="T790">Kuja</ts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3351" n="HIAT:w" s="T791">iššo</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3354" n="HIAT:w" s="T792">ej</ts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3357" n="HIAT:w" s="T793">nubi</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3360" n="HIAT:w" s="T794">măn</ts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3363" n="HIAT:w" s="T795">nubiam</ts>
                  <nts id="Seg_3364" n="HIAT:ip">.</nts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_3367" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3369" n="HIAT:w" s="T796">Dĭgəttə</ts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3372" n="HIAT:w" s="T797">tüžöj</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3375" n="HIAT:w" s="T798">surdəbiam</ts>
                  <nts id="Seg_3376" n="HIAT:ip">.</nts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_3379" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_3381" n="HIAT:w" s="T799">Büzəj</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3384" n="HIAT:w" s="T800">bĭtəlbiem</ts>
                  <nts id="Seg_3385" n="HIAT:ip">.</nts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T806" id="Seg_3388" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_3390" n="HIAT:w" s="T801">Kurizəʔi</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3392" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_3394" n="HIAT:w" s="T802">băd-</ts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3397" n="HIAT:w" s="T803">bădl-</ts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3400" n="HIAT:w" s="T804">bă-</ts>
                  <nts id="Seg_3401" n="HIAT:ip">)</nts>
                  <nts id="Seg_3402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3404" n="HIAT:w" s="T805">bădlaʔbəm</ts>
                  <nts id="Seg_3405" n="HIAT:ip">.</nts>
                  <nts id="Seg_3406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_3408" n="HIAT:u" s="T806">
                  <ts e="T807" id="Seg_3410" n="HIAT:w" s="T806">Dĭgəttə</ts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3413" n="HIAT:w" s="T807">munəjʔ</ts>
                  <nts id="Seg_3414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3416" n="HIAT:w" s="T808">pürbiem</ts>
                  <nts id="Seg_3417" n="HIAT:ip">.</nts>
                  <nts id="Seg_3418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T811" id="Seg_3420" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_3422" n="HIAT:w" s="T809">Toltanoʔ</ts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3425" n="HIAT:w" s="T810">mĭnzərbiem</ts>
                  <nts id="Seg_3426" n="HIAT:ip">.</nts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3429" n="HIAT:u" s="T811">
                  <ts e="T812" id="Seg_3431" n="HIAT:w" s="T811">Amorbiam</ts>
                  <nts id="Seg_3432" n="HIAT:ip">.</nts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3435" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3437" n="HIAT:w" s="T812">Dĭgəttə</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3439" n="HIAT:ip">(</nts>
                  <ts e="T814" id="Seg_3441" n="HIAT:w" s="T813">nu</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3444" n="HIAT:w" s="T814">kuza=</ts>
                  <nts id="Seg_3445" n="HIAT:ip">)</nts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3448" n="HIAT:w" s="T815">nu</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3451" n="HIAT:w" s="T816">il</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3454" n="HIAT:w" s="T817">šobiʔi</ts>
                  <nts id="Seg_3455" n="HIAT:ip">,</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3458" n="HIAT:w" s="T818">dʼăbaktərbiam</ts>
                  <nts id="Seg_3459" n="HIAT:ip">.</nts>
                  <nts id="Seg_3460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3462" n="HIAT:u" s="T819">
                  <nts id="Seg_3463" n="HIAT:ip">(</nts>
                  <ts e="T820" id="Seg_3465" n="HIAT:w" s="T819">Bal-</ts>
                  <nts id="Seg_3466" n="HIAT:ip">)</nts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3469" n="HIAT:w" s="T820">Bar</ts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3472" n="HIAT:w" s="T821">tararluʔpiam</ts>
                  <nts id="Seg_3473" n="HIAT:ip">.</nts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3476" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3478" n="HIAT:w" s="T822">Kabarləj</ts>
                  <nts id="Seg_3479" n="HIAT:ip">.</nts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T824" id="Seg_3482" n="HIAT:u" s="T823">
                  <nts id="Seg_3483" n="HIAT:ip">(</nts>
                  <nts id="Seg_3484" n="HIAT:ip">(</nts>
                  <ats e="T824" id="Seg_3485" n="HIAT:non-pho" s="T823">BRK</ats>
                  <nts id="Seg_3486" n="HIAT:ip">)</nts>
                  <nts id="Seg_3487" n="HIAT:ip">)</nts>
                  <nts id="Seg_3488" n="HIAT:ip">.</nts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_3491" n="HIAT:u" s="T824">
                  <ts e="T825" id="Seg_3493" n="HIAT:w" s="T824">Kandəgaʔ</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3496" n="HIAT:w" s="T825">maʔnəl</ts>
                  <nts id="Seg_3497" n="HIAT:ip">!</nts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3500" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_3502" n="HIAT:w" s="T826">Kabarləj</ts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3505" n="HIAT:w" s="T827">dʼăbaktərzittə</ts>
                  <nts id="Seg_3506" n="HIAT:ip">.</nts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T829" id="Seg_3509" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3511" n="HIAT:w" s="T828">Amgaʔ</ts>
                  <nts id="Seg_3512" n="HIAT:ip">!</nts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T831" id="Seg_3515" n="HIAT:u" s="T829">
                  <ts e="T830" id="Seg_3517" n="HIAT:w" s="T829">Iʔbeʔ</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3520" n="HIAT:w" s="T830">kunolzittə</ts>
                  <nts id="Seg_3521" n="HIAT:ip">!</nts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T833" id="Seg_3524" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_3526" n="HIAT:w" s="T831">Kabarləj</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3529" n="HIAT:w" s="T832">daška</ts>
                  <nts id="Seg_3530" n="HIAT:ip">.</nts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T834" id="Seg_3533" n="HIAT:u" s="T833">
                  <nts id="Seg_3534" n="HIAT:ip">(</nts>
                  <nts id="Seg_3535" n="HIAT:ip">(</nts>
                  <ats e="T834" id="Seg_3536" n="HIAT:non-pho" s="T833">…</ats>
                  <nts id="Seg_3537" n="HIAT:ip">)</nts>
                  <nts id="Seg_3538" n="HIAT:ip">)</nts>
                  <nts id="Seg_3539" n="HIAT:ip">.</nts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3542" n="HIAT:u" s="T834">
                  <ts e="T835" id="Seg_3544" n="HIAT:w" s="T834">Šumuranə</ts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3547" n="HIAT:w" s="T835">mĭmbiem</ts>
                  <nts id="Seg_3548" n="HIAT:ip">.</nts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3551" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3553" n="HIAT:w" s="T836">Ugaːndə</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3556" n="HIAT:w" s="T837">il</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3559" n="HIAT:w" s="T838">iʔgö</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3562" n="HIAT:w" s="T839">kubiam</ts>
                  <nts id="Seg_3563" n="HIAT:ip">.</nts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3566" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3568" n="HIAT:w" s="T840">Koŋ</ts>
                  <nts id="Seg_3569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3571" n="HIAT:w" s="T841">bar</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3574" n="HIAT:w" s="T842">šobiʔi</ts>
                  <nts id="Seg_3575" n="HIAT:ip">.</nts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3578" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3580" n="HIAT:w" s="T843">Tibizeŋ</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3583" n="HIAT:w" s="T844">dĭn</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3586" n="HIAT:w" s="T845">iʔgö</ts>
                  <nts id="Seg_3587" n="HIAT:ip">,</nts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3590" n="HIAT:w" s="T846">inezeŋ</ts>
                  <nts id="Seg_3591" n="HIAT:ip">.</nts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3594" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3596" n="HIAT:w" s="T847">Koʔbsaŋ</ts>
                  <nts id="Seg_3597" n="HIAT:ip">,</nts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3600" n="HIAT:w" s="T848">nʼizeŋ</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3603" n="HIAT:w" s="T849">bar</ts>
                  <nts id="Seg_3604" n="HIAT:ip">.</nts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T851" id="Seg_3607" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3609" n="HIAT:w" s="T850">Kabarləj</ts>
                  <nts id="Seg_3610" n="HIAT:ip">.</nts>
                  <nts id="Seg_3611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3613" n="HIAT:u" s="T851">
                  <nts id="Seg_3614" n="HIAT:ip">(</nts>
                  <nts id="Seg_3615" n="HIAT:ip">(</nts>
                  <ats e="T852" id="Seg_3616" n="HIAT:non-pho" s="T851">BRK</ats>
                  <nts id="Seg_3617" n="HIAT:ip">)</nts>
                  <nts id="Seg_3618" n="HIAT:ip">)</nts>
                  <nts id="Seg_3619" n="HIAT:ip">.</nts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3622" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3624" n="HIAT:w" s="T852">Ĭmbi</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3627" n="HIAT:w" s="T853">dĭ</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3630" n="HIAT:w" s="T854">todam</ts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3633" n="HIAT:w" s="T855">kuza</ts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3636" n="HIAT:w" s="T856">ej</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3639" n="HIAT:w" s="T857">šobi</ts>
                  <nts id="Seg_3640" n="HIAT:ip">,</nts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3643" n="HIAT:w" s="T858">koʔbdo</ts>
                  <nts id="Seg_3644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3646" n="HIAT:w" s="T859">ej</ts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3649" n="HIAT:w" s="T860">šobi</ts>
                  <nts id="Seg_3650" n="HIAT:ip">.</nts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_3653" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3655" n="HIAT:w" s="T861">Gijen</ts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3658" n="HIAT:w" s="T862">dĭzeŋ</ts>
                  <nts id="Seg_3659" n="HIAT:ip">?</nts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_3662" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_3664" n="HIAT:w" s="T863">Ĭmbi</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3667" n="HIAT:w" s="T864">ej</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3670" n="HIAT:w" s="T865">šobiʔi</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3673" n="HIAT:w" s="T866">dʼăbaktərzittə</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3676" n="HIAT:w" s="T867">mănzʼiʔ</ts>
                  <nts id="Seg_3677" n="HIAT:ip">?</nts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T869" id="Seg_3680" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3682" n="HIAT:w" s="T868">Kabarləj</ts>
                  <nts id="Seg_3683" n="HIAT:ip">.</nts>
                  <nts id="Seg_3684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3686" n="HIAT:u" s="T869">
                  <nts id="Seg_3687" n="HIAT:ip">(</nts>
                  <nts id="Seg_3688" n="HIAT:ip">(</nts>
                  <ats e="T870" id="Seg_3689" n="HIAT:non-pho" s="T869">BRK</ats>
                  <nts id="Seg_3690" n="HIAT:ip">)</nts>
                  <nts id="Seg_3691" n="HIAT:ip">)</nts>
                  <nts id="Seg_3692" n="HIAT:ip">.</nts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T872" id="Seg_3695" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3697" n="HIAT:w" s="T870">Gijen</ts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3700" n="HIAT:w" s="T871">dĭzeŋ</ts>
                  <nts id="Seg_3701" n="HIAT:ip">?</nts>
                  <nts id="Seg_3702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3704" n="HIAT:u" s="T872">
                  <ts e="T873" id="Seg_3706" n="HIAT:w" s="T872">Ĭmbi</ts>
                  <nts id="Seg_3707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3709" n="HIAT:w" s="T873">ej</ts>
                  <nts id="Seg_3710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3712" n="HIAT:w" s="T874">šobiʔi</ts>
                  <nts id="Seg_3713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3715" n="HIAT:w" s="T875">dʼăbaktərzittə</ts>
                  <nts id="Seg_3716" n="HIAT:ip">?</nts>
                  <nts id="Seg_3717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T877" id="Seg_3719" n="HIAT:u" s="T876">
                  <nts id="Seg_3720" n="HIAT:ip">(</nts>
                  <nts id="Seg_3721" n="HIAT:ip">(</nts>
                  <ats e="T877" id="Seg_3722" n="HIAT:non-pho" s="T876">BRK</ats>
                  <nts id="Seg_3723" n="HIAT:ip">)</nts>
                  <nts id="Seg_3724" n="HIAT:ip">)</nts>
                  <nts id="Seg_3725" n="HIAT:ip">.</nts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3728" n="HIAT:u" s="T877">
                  <ts e="T878" id="Seg_3730" n="HIAT:w" s="T877">Teinen</ts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3733" n="HIAT:w" s="T878">kabarləj</ts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3736" n="HIAT:w" s="T879">dʼăbaktərzittə</ts>
                  <nts id="Seg_3737" n="HIAT:ip">.</nts>
                  <nts id="Seg_3738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_3740" n="HIAT:u" s="T880">
                  <ts e="T881" id="Seg_3742" n="HIAT:w" s="T880">Kanaʔ</ts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3745" n="HIAT:w" s="T881">maʔnəl</ts>
                  <nts id="Seg_3746" n="HIAT:ip">!</nts>
                  <nts id="Seg_3747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T890" id="Seg_3749" n="HIAT:u" s="T882">
                  <ts e="T883" id="Seg_3751" n="HIAT:w" s="T882">Tăn</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3754" n="HIAT:w" s="T883">măna</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3756" n="HIAT:ip">(</nts>
                  <ts e="T885" id="Seg_3758" n="HIAT:w" s="T884">dĭ-</ts>
                  <nts id="Seg_3759" n="HIAT:ip">)</nts>
                  <nts id="Seg_3760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3762" n="HIAT:w" s="T885">dăre</ts>
                  <nts id="Seg_3763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3765" n="HIAT:w" s="T886">ej</ts>
                  <nts id="Seg_3766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3767" n="HIAT:ip">(</nts>
                  <ts e="T888" id="Seg_3769" n="HIAT:w" s="T887">šü-</ts>
                  <nts id="Seg_3770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3772" n="HIAT:w" s="T888">šü-</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3774" n="HIAT:ip">)</nts>
                  <nts id="Seg_3775" n="HIAT:ip">…</nts>
                  <nts id="Seg_3776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3778" n="HIAT:u" s="T890">
                  <ts e="T891" id="Seg_3780" n="HIAT:w" s="T890">Tăn</ts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3783" n="HIAT:w" s="T891">măna</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3786" n="HIAT:w" s="T892">dăre</ts>
                  <nts id="Seg_3787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3789" n="HIAT:w" s="T893">ej</ts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3791" n="HIAT:ip">(</nts>
                  <ts e="T895" id="Seg_3793" n="HIAT:w" s="T894">š-</ts>
                  <nts id="Seg_3794" n="HIAT:ip">)</nts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3797" n="HIAT:w" s="T895">tüšəlleʔ</ts>
                  <nts id="Seg_3798" n="HIAT:ip">!</nts>
                  <nts id="Seg_3799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T900" id="Seg_3801" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3803" n="HIAT:w" s="T896">Dăre</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3806" n="HIAT:w" s="T897">ej</ts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3809" n="HIAT:w" s="T898">jakšə</ts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3812" n="HIAT:w" s="T899">mănzittə</ts>
                  <nts id="Seg_3813" n="HIAT:ip">.</nts>
                  <nts id="Seg_3814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T903" id="Seg_3816" n="HIAT:u" s="T900">
                  <ts e="T900.tx-PKZ.1" id="Seg_3818" n="HIAT:w" s="T900">Na</ts>
                  <nts id="Seg_3819" n="HIAT:ip">_</nts>
                  <ts e="T901" id="Seg_3821" n="HIAT:w" s="T900.tx-PKZ.1">što</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3824" n="HIAT:w" s="T901">sürerzittə</ts>
                  <nts id="Seg_3825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3827" n="HIAT:w" s="T902">il</ts>
                  <nts id="Seg_3828" n="HIAT:ip">.</nts>
                  <nts id="Seg_3829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T907" id="Seg_3831" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_3833" n="HIAT:w" s="T903">Dĭ</ts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3836" n="HIAT:w" s="T904">nuzaŋ</ts>
                  <nts id="Seg_3837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3839" n="HIAT:w" s="T905">il</ts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3842" n="HIAT:w" s="T906">ведь</ts>
                  <nts id="Seg_3843" n="HIAT:ip">.</nts>
                  <nts id="Seg_3844" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T908" id="Seg_3846" n="HIAT:u" s="T907">
                  <ts e="T908" id="Seg_3848" n="HIAT:w" s="T907">Kabarləj</ts>
                  <nts id="Seg_3849" n="HIAT:ip">.</nts>
                  <nts id="Seg_3850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T533" id="Seg_3851" n="sc" s="T13">
               <ts e="T16" id="Seg_3853" n="e" s="T13">(Dʼaziraʔ), </ts>
               <ts e="T17" id="Seg_3855" n="e" s="T16">tüjö </ts>
               <ts e="T18" id="Seg_3857" n="e" s="T17">dʼăbaktərzittə </ts>
               <ts e="T19" id="Seg_3859" n="e" s="T18">nada. </ts>
               <ts e="T20" id="Seg_3861" n="e" s="T19">Dĭgəttə </ts>
               <ts e="T21" id="Seg_3863" n="e" s="T20">talerdə </ts>
               <ts e="T22" id="Seg_3865" n="e" s="T21">kalam. </ts>
               <ts e="T23" id="Seg_3867" n="e" s="T22">Dʼü </ts>
               <ts e="T24" id="Seg_3869" n="e" s="T23">talerzittə. </ts>
               <ts e="T25" id="Seg_3871" n="e" s="T24">Dĭgəttə </ts>
               <ts e="T26" id="Seg_3873" n="e" s="T25">ipek </ts>
               <ts e="T27" id="Seg_3875" n="e" s="T26">kuʔsittə. </ts>
               <ts e="T28" id="Seg_3877" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_3879" n="e" s="T28">özerləj, </ts>
               <ts e="T30" id="Seg_3881" n="e" s="T29">urgo </ts>
               <ts e="T31" id="Seg_3883" n="e" s="T30">özerləj </ts>
               <ts e="T32" id="Seg_3885" n="e" s="T31">bar. </ts>
               <ts e="T33" id="Seg_3887" n="e" s="T32">Sĭre </ts>
               <ts e="T34" id="Seg_3889" n="e" s="T33">moləj. </ts>
               <ts e="T35" id="Seg_3891" n="e" s="T34">Dĭgəttə </ts>
               <ts e="T36" id="Seg_3893" n="e" s="T35">püzittə </ts>
               <ts e="T37" id="Seg_3895" n="e" s="T36">nada. </ts>
               <ts e="T38" id="Seg_3897" n="e" s="T37">(Pag-). </ts>
               <ts e="T39" id="Seg_3899" n="e" s="T38">((BRK)). </ts>
               <ts e="T40" id="Seg_3901" n="e" s="T39">Dĭgəttə </ts>
               <ts e="T41" id="Seg_3903" n="e" s="T40">toʔnarzittə </ts>
               <ts e="T42" id="Seg_3905" n="e" s="T41">nada. </ts>
               <ts e="T43" id="Seg_3907" n="e" s="T42">Dĭgəttə </ts>
               <ts e="T44" id="Seg_3909" n="e" s="T43">băranə </ts>
               <ts e="T45" id="Seg_3911" n="e" s="T44">kămnasʼtə. </ts>
               <ts e="T46" id="Seg_3913" n="e" s="T45">Dĭgəttə </ts>
               <ts e="T47" id="Seg_3915" n="e" s="T46">nada </ts>
               <ts e="T48" id="Seg_3917" n="e" s="T47">nʼeʔsittə. </ts>
               <ts e="T49" id="Seg_3919" n="e" s="T48">Dĭgəttə </ts>
               <ts e="T50" id="Seg_3921" n="e" s="T49">nada </ts>
               <ts e="T51" id="Seg_3923" n="e" s="T50">ilgərzittə. </ts>
               <ts e="T52" id="Seg_3925" n="e" s="T51">Dĭgəttə </ts>
               <ts e="T53" id="Seg_3927" n="e" s="T52">nuldəsʼtə </ts>
               <ts e="T54" id="Seg_3929" n="e" s="T53">nada. </ts>
               <ts e="T55" id="Seg_3931" n="e" s="T54">Dĭgəttə </ts>
               <ts e="T56" id="Seg_3933" n="e" s="T55">pürzittə </ts>
               <ts e="T57" id="Seg_3935" n="e" s="T56">nada. </ts>
               <ts e="T58" id="Seg_3937" n="e" s="T57">Dĭgəttə </ts>
               <ts e="T59" id="Seg_3939" n="e" s="T58">amzittə </ts>
               <ts e="T60" id="Seg_3941" n="e" s="T59">nada. </ts>
               <ts e="T61" id="Seg_3943" n="e" s="T60">Bar! </ts>
               <ts e="T62" id="Seg_3945" n="e" s="T61">((BRK)). </ts>
               <ts e="T63" id="Seg_3947" n="e" s="T62">(Tăn=) </ts>
               <ts e="T64" id="Seg_3949" n="e" s="T63">Tăn </ts>
               <ts e="T65" id="Seg_3951" n="e" s="T64">kandlaʔbəl. </ts>
               <ts e="T66" id="Seg_3953" n="e" s="T65">Ĭmbi </ts>
               <ts e="T67" id="Seg_3955" n="e" s="T66">măna </ts>
               <ts e="T68" id="Seg_3957" n="e" s="T67">ej </ts>
               <ts e="T69" id="Seg_3959" n="e" s="T68">nörbəbiel? </ts>
               <ts e="T70" id="Seg_3961" n="e" s="T69">Măn </ts>
               <ts e="T71" id="Seg_3963" n="e" s="T70">bɨ </ts>
               <ts e="T72" id="Seg_3965" n="e" s="T71">noʔ </ts>
               <ts e="T73" id="Seg_3967" n="e" s="T72">(iʔ-) </ts>
               <ts e="T74" id="Seg_3969" n="e" s="T73">mĭbiem. </ts>
               <ts e="T75" id="Seg_3971" n="e" s="T74">Dĭn </ts>
               <ts e="T76" id="Seg_3973" n="e" s="T75">măn </ts>
               <ts e="T77" id="Seg_3975" n="e" s="T76">tugazaŋbə </ts>
               <ts e="T78" id="Seg_3977" n="e" s="T77">ige. </ts>
               <ts e="T79" id="Seg_3979" n="e" s="T78">Tăn </ts>
               <ts e="T80" id="Seg_3981" n="e" s="T79">bɨ </ts>
               <ts e="T81" id="Seg_3983" n="e" s="T80">kubiol </ts>
               <ts e="T82" id="Seg_3985" n="e" s="T81">dĭm. </ts>
               <ts e="T83" id="Seg_3987" n="e" s="T82">Kabarləj. </ts>
               <ts e="T84" id="Seg_3989" n="e" s="T83">((BRK)). </ts>
               <ts e="T85" id="Seg_3991" n="e" s="T84">Tăn </ts>
               <ts e="T86" id="Seg_3993" n="e" s="T85">ĭmbi </ts>
               <ts e="T87" id="Seg_3995" n="e" s="T86">măna </ts>
               <ts e="T88" id="Seg_3997" n="e" s="T87">ej </ts>
               <ts e="T89" id="Seg_3999" n="e" s="T88">(nörbəliel-) </ts>
               <ts e="T90" id="Seg_4001" n="e" s="T89">nörbəbiel, </ts>
               <ts e="T91" id="Seg_4003" n="e" s="T90">što </ts>
               <ts e="T92" id="Seg_4005" n="e" s="T91">kandəgal </ts>
               <ts e="T93" id="Seg_4007" n="e" s="T92">Krasnojarskə? </ts>
               <ts e="T94" id="Seg_4009" n="e" s="T93">Dĭn </ts>
               <ts e="T95" id="Seg_4011" n="e" s="T94">măn </ts>
               <ts e="T96" id="Seg_4013" n="e" s="T95">tuganbə </ts>
               <ts e="T97" id="Seg_4015" n="e" s="T96">ige. </ts>
               <ts e="T98" id="Seg_4017" n="e" s="T97">Măn </ts>
               <ts e="T99" id="Seg_4019" n="e" s="T98">bɨ </ts>
               <ts e="T100" id="Seg_4021" n="e" s="T99">noʔ </ts>
               <ts e="T101" id="Seg_4023" n="e" s="T100">tănan </ts>
               <ts e="T102" id="Seg_4025" n="e" s="T101">mĭbiem. </ts>
               <ts e="T103" id="Seg_4027" n="e" s="T102">Dĭn </ts>
               <ts e="T104" id="Seg_4029" n="e" s="T103">sĭjdə </ts>
               <ts e="T105" id="Seg_4031" n="e" s="T104">ĭzembie. </ts>
               <ts e="T106" id="Seg_4033" n="e" s="T105">(Dĭ </ts>
               <ts e="T107" id="Seg_4035" n="e" s="T106">bɨ-) </ts>
               <ts e="T108" id="Seg_4037" n="e" s="T107">Dĭ </ts>
               <ts e="T109" id="Seg_4039" n="e" s="T108">bɨ </ts>
               <ts e="T110" id="Seg_4041" n="e" s="T109">bĭʔpi. </ts>
               <ts e="T111" id="Seg_4043" n="e" s="T110">Tăn </ts>
               <ts e="T112" id="Seg_4045" n="e" s="T111">bɨ </ts>
               <ts e="T113" id="Seg_4047" n="e" s="T112">kubiol </ts>
               <ts e="T114" id="Seg_4049" n="e" s="T113">dĭm. </ts>
               <ts e="T115" id="Seg_4051" n="e" s="T114">Kabarləj. </ts>
               <ts e="T116" id="Seg_4053" n="e" s="T115">((BRK)). </ts>
               <ts e="T117" id="Seg_4055" n="e" s="T116">Amnaʔ </ts>
               <ts e="T118" id="Seg_4057" n="e" s="T117">toltanoʔ </ts>
               <ts e="T119" id="Seg_4059" n="e" s="T118">amzittə. </ts>
               <ts e="T120" id="Seg_4061" n="e" s="T119">A </ts>
               <ts e="T121" id="Seg_4063" n="e" s="T120">girgit </ts>
               <ts e="T122" id="Seg_4065" n="e" s="T121">toltanoʔ? </ts>
               <ts e="T123" id="Seg_4067" n="e" s="T122">Da </ts>
               <ts e="T124" id="Seg_4069" n="e" s="T123">măn </ts>
               <ts e="T125" id="Seg_4071" n="e" s="T124">kĭškəbiem. </ts>
               <ts e="T126" id="Seg_4073" n="e" s="T125">Dĭgəttə </ts>
               <ts e="T127" id="Seg_4075" n="e" s="T126">un </ts>
               <ts e="T128" id="Seg_4077" n="e" s="T127">ibi. </ts>
               <ts e="T129" id="Seg_4079" n="e" s="T128">Măn </ts>
               <ts e="T130" id="Seg_4081" n="e" s="T129">dĭgəttə </ts>
               <ts e="T131" id="Seg_4083" n="e" s="T130">sĭreʔpne </ts>
               <ts e="T132" id="Seg_4085" n="e" s="T131">embiem </ts>
               <ts e="T133" id="Seg_4087" n="e" s="T132">i </ts>
               <ts e="T134" id="Seg_4089" n="e" s="T133">keʔbde </ts>
               <ts e="T135" id="Seg_4091" n="e" s="T134">embiem. </ts>
               <ts e="T136" id="Seg_4093" n="e" s="T135">Dʼibige </ts>
               <ts e="T137" id="Seg_4095" n="e" s="T136">(ku-) </ts>
               <ts e="T138" id="Seg_4097" n="e" s="T137">bü </ts>
               <ts e="T139" id="Seg_4099" n="e" s="T138">kămnabiam. </ts>
               <ts e="T140" id="Seg_4101" n="e" s="T139">Dĭgəttə </ts>
               <ts e="T141" id="Seg_4103" n="e" s="T140">mĭnzərbiem. </ts>
               <ts e="T142" id="Seg_4105" n="e" s="T141">Amnaʔ, </ts>
               <ts e="T143" id="Seg_4107" n="e" s="T142">amoraʔ! </ts>
               <ts e="T144" id="Seg_4109" n="e" s="T143">((BRK)). </ts>
               <ts e="T145" id="Seg_4111" n="e" s="T144">Nʼi </ts>
               <ts e="T146" id="Seg_4113" n="e" s="T145">naga. </ts>
               <ts e="T147" id="Seg_4115" n="e" s="T146">Gibərdə </ts>
               <ts e="T911" id="Seg_4117" n="e" s="T147">kalla </ts>
               <ts e="T148" id="Seg_4119" n="e" s="T911">dʼürbi? </ts>
               <ts e="T149" id="Seg_4121" n="e" s="T148">(Nad-). </ts>
               <ts e="T150" id="Seg_4123" n="e" s="T149">Măndərzittə </ts>
               <ts e="T151" id="Seg_4125" n="e" s="T150">nada. </ts>
               <ts e="T152" id="Seg_4127" n="e" s="T151">Možet </ts>
               <ts e="T153" id="Seg_4129" n="e" s="T152">šindi-nʼibudʼ </ts>
               <ts e="T154" id="Seg_4131" n="e" s="T153">kuʔpi? </ts>
               <ts e="T155" id="Seg_4133" n="e" s="T154">Možet </ts>
               <ts e="T156" id="Seg_4135" n="e" s="T155">urgaːba </ts>
               <ts e="T157" id="Seg_4137" n="e" s="T156">ambi. </ts>
               <ts e="T158" id="Seg_4139" n="e" s="T157">Ajiriom </ts>
               <ts e="T159" id="Seg_4141" n="e" s="T158">bar </ts>
               <ts e="T160" id="Seg_4143" n="e" s="T159">ugaːndə. </ts>
               <ts e="T161" id="Seg_4145" n="e" s="T160">Kabarləj. </ts>
               <ts e="T162" id="Seg_4147" n="e" s="T161">((BRK)). </ts>
               <ts e="T163" id="Seg_4149" n="e" s="T162">Oʔb, </ts>
               <ts e="T164" id="Seg_4151" n="e" s="T163">šide, </ts>
               <ts e="T165" id="Seg_4153" n="e" s="T164">nagur, </ts>
               <ts e="T166" id="Seg_4155" n="e" s="T165">teʔtə, </ts>
               <ts e="T167" id="Seg_4157" n="e" s="T166">sumna, </ts>
               <ts e="T168" id="Seg_4159" n="e" s="T167">sejʔpü, </ts>
               <ts e="T169" id="Seg_4161" n="e" s="T168">šinteʔtə, </ts>
               <ts e="T170" id="Seg_4163" n="e" s="T169">bʼeʔ. </ts>
               <ts e="T172" id="Seg_4165" n="e" s="T170">Сбилась я. </ts>
               <ts e="T173" id="Seg_4167" n="e" s="T172">((BRK)). </ts>
               <ts e="T174" id="Seg_4169" n="e" s="T173">Kăbɨ </ts>
               <ts e="T175" id="Seg_4171" n="e" s="T174">bostə </ts>
               <ts e="T176" id="Seg_4173" n="e" s="T175">(i-) </ts>
               <ts e="T177" id="Seg_4175" n="e" s="T176">inezeŋdə </ts>
               <ts e="T178" id="Seg_4177" n="e" s="T177">ibiʔi, </ts>
               <ts e="T179" id="Seg_4179" n="e" s="T178">dĭgəttə </ts>
               <ts e="T180" id="Seg_4181" n="e" s="T179">tajirzittə </ts>
               <ts e="T181" id="Seg_4183" n="e" s="T180">možnă. </ts>
               <ts e="T182" id="Seg_4185" n="e" s="T181">I </ts>
               <ts e="T183" id="Seg_4187" n="e" s="T182">toltanoʔ </ts>
               <ts e="T184" id="Seg_4189" n="e" s="T183">amzittə. </ts>
               <ts e="T185" id="Seg_4191" n="e" s="T184">A_to </ts>
               <ts e="T186" id="Seg_4193" n="e" s="T185">bostə </ts>
               <ts e="T187" id="Seg_4195" n="e" s="T186">ineʔi </ts>
               <ts e="T188" id="Seg_4197" n="e" s="T187">naga. </ts>
               <ts e="T189" id="Seg_4199" n="e" s="T188">Bar. </ts>
               <ts e="T190" id="Seg_4201" n="e" s="T189">((BRK)). </ts>
               <ts e="T191" id="Seg_4203" n="e" s="T190">Măn </ts>
               <ts e="T192" id="Seg_4205" n="e" s="T191">meim </ts>
               <ts e="T193" id="Seg_4207" n="e" s="T192">bar </ts>
               <ts e="T194" id="Seg_4209" n="e" s="T193">jezerik. </ts>
               <ts e="T195" id="Seg_4211" n="e" s="T194">Ara </ts>
               <ts e="T196" id="Seg_4213" n="e" s="T195">bĭʔpi. </ts>
               <ts e="T197" id="Seg_4215" n="e" s="T196">Šobi </ts>
               <ts e="T198" id="Seg_4217" n="e" s="T197">bar, </ts>
               <ts e="T199" id="Seg_4219" n="e" s="T198">kudonzəbiʔi </ts>
               <ts e="T200" id="Seg_4221" n="e" s="T199">bostə </ts>
               <ts e="T201" id="Seg_4223" n="e" s="T200">tibizʼiʔ. </ts>
               <ts e="T202" id="Seg_4225" n="e" s="T201">Dʼabrobiʔi </ts>
               <ts e="T203" id="Seg_4227" n="e" s="T202">bar. </ts>
               <ts e="T204" id="Seg_4229" n="e" s="T203">Ugaːndə </ts>
               <ts e="T205" id="Seg_4231" n="e" s="T204">dĭ </ts>
               <ts e="T206" id="Seg_4233" n="e" s="T205">kurojok. </ts>
               <ts e="T207" id="Seg_4235" n="e" s="T206">Vsʼakă </ts>
               <ts e="T208" id="Seg_4237" n="e" s="T207">(kudonzə-) </ts>
               <ts e="T209" id="Seg_4239" n="e" s="T208">kudonzlaʔpi. </ts>
               <ts e="T210" id="Seg_4241" n="e" s="T209">Kăde </ts>
               <ts e="T211" id="Seg_4243" n="e" s="T210">dĭʔnə </ts>
               <ts e="T212" id="Seg_4245" n="e" s="T211">kereʔ. </ts>
               <ts e="T213" id="Seg_4247" n="e" s="T212">Bazoʔ </ts>
               <ts e="T912" id="Seg_4249" n="e" s="T213">kalla </ts>
               <ts e="T214" id="Seg_4251" n="e" s="T912">dʼürbi </ts>
               <ts e="T215" id="Seg_4253" n="e" s="T214">ara </ts>
               <ts e="T216" id="Seg_4255" n="e" s="T215">bĭʔsittə. </ts>
               <ts e="T217" id="Seg_4257" n="e" s="T216">A_to </ts>
               <ts e="T218" id="Seg_4259" n="e" s="T217">dĭʔnə </ts>
               <ts e="T219" id="Seg_4261" n="e" s="T218">amga. </ts>
               <ts e="T220" id="Seg_4263" n="e" s="T219">Kabarləj. </ts>
               <ts e="T221" id="Seg_4265" n="e" s="T220">((BRK)). </ts>
               <ts e="T222" id="Seg_4267" n="e" s="T221">Šiʔ </ts>
               <ts e="T223" id="Seg_4269" n="e" s="T222">dʼijegən </ts>
               <ts e="T224" id="Seg_4271" n="e" s="T223">ibileʔ. </ts>
               <ts e="T225" id="Seg_4273" n="e" s="T224">Gijen </ts>
               <ts e="T226" id="Seg_4275" n="e" s="T225">kunolbilaʔ? </ts>
               <ts e="T227" id="Seg_4277" n="e" s="T226">Kunolbibaʔ </ts>
               <ts e="T228" id="Seg_4279" n="e" s="T227">pălatkagən. </ts>
               <ts e="T229" id="Seg_4281" n="e" s="T228">Urgaːba </ts>
               <ts e="T230" id="Seg_4283" n="e" s="T229">šobi. </ts>
               <ts e="T231" id="Seg_4285" n="e" s="T230">Bar </ts>
               <ts e="T232" id="Seg_4287" n="e" s="T231">mĭmbi </ts>
               <ts e="T233" id="Seg_4289" n="e" s="T232">dön. </ts>
               <ts e="T234" id="Seg_4291" n="e" s="T233">(M-) </ts>
               <ts e="T235" id="Seg_4293" n="e" s="T234">Miʔ </ts>
               <ts e="T236" id="Seg_4295" n="e" s="T235">ugaːndə </ts>
               <ts e="T237" id="Seg_4297" n="e" s="T236">pimbibeʔ. </ts>
               <ts e="T238" id="Seg_4299" n="e" s="T237">Dĭ </ts>
               <ts e="T239" id="Seg_4301" n="e" s="T238">tože </ts>
               <ts e="T240" id="Seg_4303" n="e" s="T239">pimnie, </ts>
               <ts e="T241" id="Seg_4305" n="e" s="T240">multukdə. </ts>
               <ts e="T242" id="Seg_4307" n="e" s="T241">(E- </ts>
               <ts e="T243" id="Seg_4309" n="e" s="T242">ej-) </ts>
               <ts e="T244" id="Seg_4311" n="e" s="T243">Ejem </ts>
               <ts e="T245" id="Seg_4313" n="e" s="T244">nada </ts>
               <ts e="T246" id="Seg_4315" n="e" s="T245">surarzittə </ts>
               <ts e="T247" id="Seg_4317" n="e" s="T246">(iʔbəzittə), </ts>
               <ts e="T248" id="Seg_4319" n="e" s="T247">(kunolzittə). </ts>
               <ts e="T249" id="Seg_4321" n="e" s="T248">Kabarləj. </ts>
               <ts e="T250" id="Seg_4323" n="e" s="T249">((BRK)). </ts>
               <ts e="T910" id="Seg_4325" n="e" s="T250">Курочка </ts>
               <ts e="T251" id="Seg_4327" n="e" s="T910">lunʼkam </ts>
               <ts e="T252" id="Seg_4329" n="e" s="T251">bar </ts>
               <ts e="T254" id="Seg_4331" n="e" s="T252">monoʔkolaʔbə. </ts>
               <ts e="T255" id="Seg_4333" n="e" s="T254">Bar </ts>
               <ts e="T256" id="Seg_4335" n="e" s="T255">pünörleʔbə. </ts>
               <ts e="T257" id="Seg_4337" n="e" s="T256">Bar </ts>
               <ts e="T258" id="Seg_4339" n="e" s="T257">münörleʔbə. </ts>
               <ts e="T259" id="Seg_4341" n="e" s="T258">Bar </ts>
               <ts e="T260" id="Seg_4343" n="e" s="T259">(burerleʔbə). </ts>
               <ts e="T261" id="Seg_4345" n="e" s="T260">Daška </ts>
               <ts e="T262" id="Seg_4347" n="e" s="T261">bar. </ts>
               <ts e="T263" id="Seg_4349" n="e" s="T262">((BRK)). </ts>
               <ts e="T264" id="Seg_4351" n="e" s="T263">(Ĭmbej-) </ts>
               <ts e="T265" id="Seg_4353" n="e" s="T264">Ĭmbi </ts>
               <ts e="T266" id="Seg_4355" n="e" s="T265">pimniel? </ts>
               <ts e="T267" id="Seg_4357" n="e" s="T266">Kanaʔ </ts>
               <ts e="T268" id="Seg_4359" n="e" s="T267">unnʼa! </ts>
               <ts e="T269" id="Seg_4361" n="e" s="T268">Măn </ts>
               <ts e="T270" id="Seg_4363" n="e" s="T269">unnʼa </ts>
               <ts e="T271" id="Seg_4365" n="e" s="T270">mĭmbiem, </ts>
               <ts e="T272" id="Seg_4367" n="e" s="T271">šindinədə </ts>
               <ts e="T273" id="Seg_4369" n="e" s="T272">(ej-) </ts>
               <ts e="T274" id="Seg_4371" n="e" s="T273">ej </ts>
               <ts e="T275" id="Seg_4373" n="e" s="T274">pimniem. </ts>
               <ts e="T276" id="Seg_4375" n="e" s="T275">A </ts>
               <ts e="T277" id="Seg_4377" n="e" s="T276">tăn </ts>
               <ts e="T278" id="Seg_4379" n="e" s="T277">üge </ts>
               <ts e="T279" id="Seg_4381" n="e" s="T278">pimneʔbəl. </ts>
               <ts e="T280" id="Seg_4383" n="e" s="T279">Nöməlleʔpiem. </ts>
               <ts e="T281" id="Seg_4385" n="e" s="T280">Хватит. </ts>
               <ts e="T282" id="Seg_4387" n="e" s="T281">((DMG)). </ts>
               <ts e="T283" id="Seg_4389" n="e" s="T282">Dĭzeŋ </ts>
               <ts e="T284" id="Seg_4391" n="e" s="T283">ej </ts>
               <ts e="T285" id="Seg_4393" n="e" s="T284">tĭmneʔi </ts>
               <ts e="T286" id="Seg_4395" n="e" s="T285">dʼăbaktərzittə. </ts>
               <ts e="T287" id="Seg_4397" n="e" s="T286">A </ts>
               <ts e="T288" id="Seg_4399" n="e" s="T287">măn </ts>
               <ts e="T289" id="Seg_4401" n="e" s="T288">bar </ts>
               <ts e="T290" id="Seg_4403" n="e" s="T289">dʼăbaktərlaʔbəm. </ts>
               <ts e="T291" id="Seg_4405" n="e" s="T290">Iam </ts>
               <ts e="T292" id="Seg_4407" n="e" s="T291">surarbiam. </ts>
               <ts e="T293" id="Seg_4409" n="e" s="T292">Dʼăbaktərzittə </ts>
               <ts e="T294" id="Seg_4411" n="e" s="T293">axota </ts>
               <ts e="T295" id="Seg_4413" n="e" s="T294">măna. </ts>
               <ts e="T296" id="Seg_4415" n="e" s="T295">Bar. </ts>
               <ts e="T297" id="Seg_4417" n="e" s="T296">((BRK)). </ts>
               <ts e="T298" id="Seg_4419" n="e" s="T297">(Dĭn=) </ts>
               <ts e="T299" id="Seg_4421" n="e" s="T298">Dön </ts>
               <ts e="T300" id="Seg_4423" n="e" s="T299">bar </ts>
               <ts e="T301" id="Seg_4425" n="e" s="T300">dʼirəgəʔi </ts>
               <ts e="T302" id="Seg_4427" n="e" s="T301">naga. </ts>
               <ts e="T303" id="Seg_4429" n="e" s="T302">(A </ts>
               <ts e="T304" id="Seg_4431" n="e" s="T303">onʼ-) </ts>
               <ts e="T305" id="Seg_4433" n="e" s="T304">Onʼiʔ </ts>
               <ts e="T306" id="Seg_4435" n="e" s="T305">nuzaŋ </ts>
               <ts e="T307" id="Seg_4437" n="e" s="T306">amnolaʔbəʔjə. </ts>
               <ts e="T308" id="Seg_4439" n="e" s="T307">Dĭzeŋ </ts>
               <ts e="T309" id="Seg_4441" n="e" s="T308">bar </ts>
               <ts e="T310" id="Seg_4443" n="e" s="T309">dʼirəgəʔi </ts>
               <ts e="T311" id="Seg_4445" n="e" s="T310">pimnieʔjə. </ts>
               <ts e="T312" id="Seg_4447" n="e" s="T311">Bar </ts>
               <ts e="T313" id="Seg_4449" n="e" s="T312">(em-) </ts>
               <ts e="T314" id="Seg_4451" n="e" s="T313">ej </ts>
               <ts e="T315" id="Seg_4453" n="e" s="T314">dʼăbaktərlam. </ts>
               <ts e="T316" id="Seg_4455" n="e" s="T315">Tüjö </ts>
               <ts e="T317" id="Seg_4457" n="e" s="T316">băra </ts>
               <ts e="T318" id="Seg_4459" n="e" s="T317">detlem. </ts>
               <ts e="T319" id="Seg_4461" n="e" s="T318">Tăn </ts>
               <ts e="T320" id="Seg_4463" n="e" s="T319">dʼabolal. </ts>
               <ts e="T321" id="Seg_4465" n="e" s="T320">Măn </ts>
               <ts e="T322" id="Seg_4467" n="e" s="T321">toltanoʔ </ts>
               <ts e="T323" id="Seg_4469" n="e" s="T322">kămnabiam. </ts>
               <ts e="T324" id="Seg_4471" n="e" s="T323">Amnogaʔ! </ts>
               <ts e="T325" id="Seg_4473" n="e" s="T324">Măn </ts>
               <ts e="T326" id="Seg_4475" n="e" s="T325">iššo </ts>
               <ts e="T327" id="Seg_4477" n="e" s="T326">šide </ts>
               <ts e="T328" id="Seg_4479" n="e" s="T327">aspaʔ </ts>
               <ts e="T329" id="Seg_4481" n="e" s="T328">detlem. </ts>
               <ts e="T330" id="Seg_4483" n="e" s="T329">Păʔlam. </ts>
               <ts e="T331" id="Seg_4485" n="e" s="T330">Dĭgəttə </ts>
               <ts e="T332" id="Seg_4487" n="e" s="T331">dʼăbaktərzittə </ts>
               <ts e="T333" id="Seg_4489" n="e" s="T332">možna. </ts>
               <ts e="T334" id="Seg_4491" n="e" s="T333">Bar, </ts>
               <ts e="T335" id="Seg_4493" n="e" s="T334">ej </ts>
               <ts e="T336" id="Seg_4495" n="e" s="T335">dʼăbaktəriam. </ts>
               <ts e="T337" id="Seg_4497" n="e" s="T336">((BRK)). </ts>
               <ts e="T338" id="Seg_4499" n="e" s="T337">Teinen </ts>
               <ts e="T339" id="Seg_4501" n="e" s="T338">ugaːndə </ts>
               <ts e="T340" id="Seg_4503" n="e" s="T339">dʼibige </ts>
               <ts e="T341" id="Seg_4505" n="e" s="T340">dʼala. </ts>
               <ts e="T342" id="Seg_4507" n="e" s="T341">Bar </ts>
               <ts e="T343" id="Seg_4509" n="e" s="T342">ipek </ts>
               <ts e="T344" id="Seg_4511" n="e" s="T343">nendləj. </ts>
               <ts e="T345" id="Seg_4513" n="e" s="T344">(Ej-) </ts>
               <ts e="T346" id="Seg_4515" n="e" s="T345">ipek </ts>
               <ts e="T347" id="Seg_4517" n="e" s="T346">ej </ts>
               <ts e="T348" id="Seg_4519" n="e" s="T347">özerləj. </ts>
               <ts e="T349" id="Seg_4521" n="e" s="T348">Dĭgəttə </ts>
               <ts e="T350" id="Seg_4523" n="e" s="T349">naga </ts>
               <ts e="T351" id="Seg_4525" n="e" s="T350">ipek. </ts>
               <ts e="T352" id="Seg_4527" n="e" s="T351">Ĭmbi </ts>
               <ts e="T353" id="Seg_4529" n="e" s="T352">amzittə. </ts>
               <ts e="T354" id="Seg_4531" n="e" s="T353">Külaːmbiam, </ts>
               <ts e="T355" id="Seg_4533" n="e" s="T354">(ku-) </ts>
               <ts e="T356" id="Seg_4535" n="e" s="T355">küzittə </ts>
               <ts e="T357" id="Seg_4537" n="e" s="T356">možnă, </ts>
               <ts e="T358" id="Seg_4539" n="e" s="T357">ipekziʔ </ts>
               <ts e="T359" id="Seg_4541" n="e" s="T358">naga. </ts>
               <ts e="T360" id="Seg_4543" n="e" s="T359">Bar </ts>
               <ts e="T361" id="Seg_4545" n="e" s="T360">ibə, </ts>
               <ts e="T362" id="Seg_4547" n="e" s="T361">ej </ts>
               <ts e="T363" id="Seg_4549" n="e" s="T362">dʼăbaktərlam. </ts>
               <ts e="T364" id="Seg_4551" n="e" s="T363">((BRK)). </ts>
               <ts e="T365" id="Seg_4553" n="e" s="T364">Tăn </ts>
               <ts e="T366" id="Seg_4555" n="e" s="T365">ugaːndə </ts>
               <ts e="T367" id="Seg_4557" n="e" s="T366">sagər. </ts>
               <ts e="T368" id="Seg_4559" n="e" s="T367">Bazəjdaʔ! </ts>
               <ts e="T369" id="Seg_4561" n="e" s="T368">Multʼanə </ts>
               <ts e="T370" id="Seg_4563" n="e" s="T369">kanaʔ! </ts>
               <ts e="T371" id="Seg_4565" n="e" s="T370">Sabən </ts>
               <ts e="T372" id="Seg_4567" n="e" s="T371">iʔ. </ts>
               <ts e="T373" id="Seg_4569" n="e" s="T372">Dĭgəttə </ts>
               <ts e="T374" id="Seg_4571" n="e" s="T373">kujnek </ts>
               <ts e="T375" id="Seg_4573" n="e" s="T374">iʔ! </ts>
               <ts e="T376" id="Seg_4575" n="e" s="T375">Šerdə! </ts>
               <ts e="T377" id="Seg_4577" n="e" s="T376">Kabarləj. </ts>
               <ts e="T378" id="Seg_4579" n="e" s="T377">((BRK)). </ts>
               <ts e="T379" id="Seg_4581" n="e" s="T378">Ugaːndə </ts>
               <ts e="T380" id="Seg_4583" n="e" s="T379">kuvas </ts>
               <ts e="T381" id="Seg_4585" n="e" s="T380">dĭgəttə </ts>
               <ts e="T382" id="Seg_4587" n="e" s="T381">molal! </ts>
               <ts e="T383" id="Seg_4589" n="e" s="T382">Kujnek </ts>
               <ts e="T384" id="Seg_4591" n="e" s="T383">šeriel </ts>
               <ts e="T385" id="Seg_4593" n="e" s="T384">dăk. </ts>
               <ts e="T386" id="Seg_4595" n="e" s="T385">Bar </ts>
               <ts e="T387" id="Seg_4597" n="e" s="T386">koʔbsaŋ </ts>
               <ts e="T388" id="Seg_4599" n="e" s="T387">tănan </ts>
               <ts e="T389" id="Seg_4601" n="e" s="T388">(могут </ts>
               <ts e="T390" id="Seg_4603" n="e" s="T389">-) </ts>
               <ts e="T391" id="Seg_4605" n="e" s="T390">(pănarləʔi) </ts>
               <ts e="T392" id="Seg_4607" n="e" s="T391">bar. </ts>
               <ts e="T393" id="Seg_4609" n="e" s="T392">Kuvas. </ts>
               <ts e="T394" id="Seg_4611" n="e" s="T393">Kabarləj. </ts>
               <ts e="T395" id="Seg_4613" n="e" s="T394">((BRK)). </ts>
               <ts e="T396" id="Seg_4615" n="e" s="T395">Dĭzeŋ </ts>
               <ts e="T397" id="Seg_4617" n="e" s="T396">Krasnojarskəʔi </ts>
               <ts e="T398" id="Seg_4619" n="e" s="T397">kambiʔi. </ts>
               <ts e="T399" id="Seg_4621" n="e" s="T398">Măna </ts>
               <ts e="T400" id="Seg_4623" n="e" s="T399">ej </ts>
               <ts e="T401" id="Seg_4625" n="e" s="T400">nörbəʔi. </ts>
               <ts e="T402" id="Seg_4627" n="e" s="T401">Măn </ts>
               <ts e="T403" id="Seg_4629" n="e" s="T402">măn </ts>
               <ts e="T404" id="Seg_4631" n="e" s="T403">dĭn </ts>
               <ts e="T405" id="Seg_4633" n="e" s="T404">tuganbə </ts>
               <ts e="T406" id="Seg_4635" n="e" s="T405">ige. </ts>
               <ts e="T407" id="Seg_4637" n="e" s="T406">Măn </ts>
               <ts e="T408" id="Seg_4639" n="e" s="T407">bɨ </ts>
               <ts e="T409" id="Seg_4641" n="e" s="T408">noʔ </ts>
               <ts e="T410" id="Seg_4643" n="e" s="T409">mĭbiem. </ts>
               <ts e="T411" id="Seg_4645" n="e" s="T410">(Dĭn=) </ts>
               <ts e="T412" id="Seg_4647" n="e" s="T411">Dĭn </ts>
               <ts e="T413" id="Seg_4649" n="e" s="T412">sĭjdə </ts>
               <ts e="T414" id="Seg_4651" n="e" s="T413">ĭzemnie. </ts>
               <ts e="T415" id="Seg_4653" n="e" s="T414">Dĭzeŋ </ts>
               <ts e="T416" id="Seg_4655" n="e" s="T415">bar </ts>
               <ts e="T417" id="Seg_4657" n="e" s="T416">kubiʔi </ts>
               <ts e="T418" id="Seg_4659" n="e" s="T417">bɨ </ts>
               <ts e="T419" id="Seg_4661" n="e" s="T418">dĭm </ts>
               <ts e="T420" id="Seg_4663" n="e" s="T419">(dĭ-). </ts>
               <ts e="T421" id="Seg_4665" n="e" s="T420">Bar, </ts>
               <ts e="T422" id="Seg_4667" n="e" s="T421">daška </ts>
               <ts e="T423" id="Seg_4669" n="e" s="T422">ej </ts>
               <ts e="T424" id="Seg_4671" n="e" s="T423">dʼăbaktərlam. </ts>
               <ts e="T425" id="Seg_4673" n="e" s="T424">((BRK)). </ts>
               <ts e="T426" id="Seg_4675" n="e" s="T425">Sagər </ts>
               <ts e="T427" id="Seg_4677" n="e" s="T426">ne </ts>
               <ts e="T428" id="Seg_4679" n="e" s="T427">šobi. </ts>
               <ts e="T429" id="Seg_4681" n="e" s="T428">Sĭre </ts>
               <ts e="T430" id="Seg_4683" n="e" s="T429">kuza </ts>
               <ts e="T431" id="Seg_4685" n="e" s="T430">măndə: </ts>
               <ts e="T432" id="Seg_4687" n="e" s="T431">izittə </ts>
               <ts e="T433" id="Seg_4689" n="e" s="T432">nada. </ts>
               <ts e="T434" id="Seg_4691" n="e" s="T433">Kanžəbəj </ts>
               <ts e="T435" id="Seg_4693" n="e" s="T434">obberəj. </ts>
               <ts e="T436" id="Seg_4695" n="e" s="T435">Aktʼigən </ts>
               <ts e="T437" id="Seg_4697" n="e" s="T436">bar </ts>
               <ts e="T438" id="Seg_4699" n="e" s="T437">(kunol-) </ts>
               <ts e="T439" id="Seg_4701" n="e" s="T438">kunolžəbəj </ts>
               <ts e="T440" id="Seg_4703" n="e" s="T439">bar. </ts>
               <ts e="T441" id="Seg_4705" n="e" s="T440">Panaržəbəj </ts>
               <ts e="T442" id="Seg_4707" n="e" s="T441">bar. </ts>
               <ts e="T443" id="Seg_4709" n="e" s="T442">Tibil </ts>
               <ts e="T444" id="Seg_4711" n="e" s="T443">bar </ts>
               <ts e="T445" id="Seg_4713" n="e" s="T444">münörləj </ts>
               <ts e="T446" id="Seg_4715" n="e" s="T445">tănan. </ts>
               <ts e="T447" id="Seg_4717" n="e" s="T446">Kabarləj. </ts>
               <ts e="T448" id="Seg_4719" n="e" s="T447">Daška </ts>
               <ts e="T449" id="Seg_4721" n="e" s="T448">ej </ts>
               <ts e="T450" id="Seg_4723" n="e" s="T449">dʼăbaktərlam. </ts>
               <ts e="T451" id="Seg_4725" n="e" s="T450">((BRK)). </ts>
               <ts e="T452" id="Seg_4727" n="e" s="T451">Nada </ts>
               <ts e="T453" id="Seg_4729" n="e" s="T452">măna </ts>
               <ts e="T454" id="Seg_4731" n="e" s="T453">ešši </ts>
               <ts e="T455" id="Seg_4733" n="e" s="T454">(măn) </ts>
               <ts e="T456" id="Seg_4735" n="e" s="T455">băzəjsʼtə. </ts>
               <ts e="T457" id="Seg_4737" n="e" s="T456">Dĭ </ts>
               <ts e="T458" id="Seg_4739" n="e" s="T457">ugaːndə </ts>
               <ts e="T459" id="Seg_4741" n="e" s="T458">sagər. </ts>
               <ts e="T460" id="Seg_4743" n="e" s="T459">Bar </ts>
               <ts e="T461" id="Seg_4745" n="e" s="T460">tüʔpi </ts>
               <ts e="T462" id="Seg_4747" n="e" s="T461">iʔgö. </ts>
               <ts e="T463" id="Seg_4749" n="e" s="T462">(Kĭškiem-) </ts>
               <ts e="T464" id="Seg_4751" n="e" s="T463">bar, </ts>
               <ts e="T465" id="Seg_4753" n="e" s="T464">kĭškəbiem. </ts>
               <ts e="T466" id="Seg_4755" n="e" s="T465">Dĭ </ts>
               <ts e="T467" id="Seg_4757" n="e" s="T466">kolaːmbi. </ts>
               <ts e="T468" id="Seg_4759" n="e" s="T467">Dĭgəttə </ts>
               <ts e="T469" id="Seg_4761" n="e" s="T468">nada </ts>
               <ts e="T470" id="Seg_4763" n="e" s="T469">kürzittə </ts>
               <ts e="T471" id="Seg_4765" n="e" s="T470">dĭm. </ts>
               <ts e="T472" id="Seg_4767" n="e" s="T471">Dĭgəttə </ts>
               <ts e="T473" id="Seg_4769" n="e" s="T472">(am- </ts>
               <ts e="T474" id="Seg_4771" n="e" s="T473">amzit-) </ts>
               <ts e="T475" id="Seg_4773" n="e" s="T474">amorzittə </ts>
               <ts e="T476" id="Seg_4775" n="e" s="T475">nada. </ts>
               <ts e="T477" id="Seg_4777" n="e" s="T476">Dĭgəttə </ts>
               <ts e="T478" id="Seg_4779" n="e" s="T477">(ku-) </ts>
               <ts e="T479" id="Seg_4781" n="e" s="T478">kunolzittə. </ts>
               <ts e="T480" id="Seg_4783" n="e" s="T479">Ordə </ts>
               <ts e="T481" id="Seg_4785" n="e" s="T480">eššim. </ts>
               <ts e="T483" id="Seg_4787" n="e" s="T481">Bar- </ts>
               <ts e="T484" id="Seg_4789" n="e" s="T483">-gaʔ. </ts>
               <ts e="T485" id="Seg_4791" n="e" s="T484">((DMG)). </ts>
               <ts e="T486" id="Seg_4793" n="e" s="T485">Teinen </ts>
               <ts e="T487" id="Seg_4795" n="e" s="T486">urgo </ts>
               <ts e="T488" id="Seg_4797" n="e" s="T487">dʼala. </ts>
               <ts e="T489" id="Seg_4799" n="e" s="T488">A </ts>
               <ts e="T490" id="Seg_4801" n="e" s="T489">il </ts>
               <ts e="T491" id="Seg_4803" n="e" s="T490">üge </ts>
               <ts e="T492" id="Seg_4805" n="e" s="T491">togonorlaʔbəʔjə. </ts>
               <ts e="T493" id="Seg_4807" n="e" s="T492">Toltanoʔ </ts>
               <ts e="T494" id="Seg_4809" n="e" s="T493">amlaʔbəʔjə. </ts>
               <ts e="T495" id="Seg_4811" n="e" s="T494">Tajirlaʔbəʔjə. </ts>
               <ts e="T496" id="Seg_4813" n="e" s="T495">A </ts>
               <ts e="T497" id="Seg_4815" n="e" s="T496">miʔ </ts>
               <ts e="T498" id="Seg_4817" n="e" s="T497">amnolaʔbəbaʔ. </ts>
               <ts e="T499" id="Seg_4819" n="e" s="T498">Kabarləj, </ts>
               <ts e="T500" id="Seg_4821" n="e" s="T499">daška </ts>
               <ts e="T501" id="Seg_4823" n="e" s="T500">(ej=) </ts>
               <ts e="T502" id="Seg_4825" n="e" s="T501">dʼăbaktərzittə </ts>
               <ts e="T503" id="Seg_4827" n="e" s="T502">nʼe </ts>
               <ts e="T504" id="Seg_4829" n="e" s="T503">axota. </ts>
               <ts e="T505" id="Seg_4831" n="e" s="T504">((BRK)). </ts>
               <ts e="T506" id="Seg_4833" n="e" s="T505">Teinen </ts>
               <ts e="T507" id="Seg_4835" n="e" s="T506">urgo </ts>
               <ts e="T508" id="Seg_4837" n="e" s="T507">dʼala. </ts>
               <ts e="T509" id="Seg_4839" n="e" s="T508">Măna </ts>
               <ts e="T510" id="Seg_4841" n="e" s="T509">tuganbə </ts>
               <ts e="T511" id="Seg_4843" n="e" s="T510">kăštəbiʔi. </ts>
               <ts e="T512" id="Seg_4845" n="e" s="T511">Kanžəbəj. </ts>
               <ts e="T513" id="Seg_4847" n="e" s="T512">Dĭn </ts>
               <ts e="T514" id="Seg_4849" n="e" s="T513">ara </ts>
               <ts e="T515" id="Seg_4851" n="e" s="T514">bĭtləbəj. </ts>
               <ts e="T516" id="Seg_4853" n="e" s="T515">Dĭn </ts>
               <ts e="T517" id="Seg_4855" n="e" s="T516">sʼarlaʔbəʔjə </ts>
               <ts e="T518" id="Seg_4857" n="e" s="T517">garmonʼənə. </ts>
               <ts e="T519" id="Seg_4859" n="e" s="T518">(M-) </ts>
               <ts e="T520" id="Seg_4861" n="e" s="T519">Miʔ </ts>
               <ts e="T521" id="Seg_4863" n="e" s="T520">bar </ts>
               <ts e="T522" id="Seg_4865" n="e" s="T521">suʔməleʔbəbeʔ. </ts>
               <ts e="T523" id="Seg_4867" n="e" s="T522">Bar. </ts>
               <ts e="T524" id="Seg_4869" n="e" s="T523">((BRK)). </ts>
               <ts e="T525" id="Seg_4871" n="e" s="T524">Oʔb, </ts>
               <ts e="T526" id="Seg_4873" n="e" s="T525">oʔb, </ts>
               <ts e="T527" id="Seg_4875" n="e" s="T526">šide, </ts>
               <ts e="T528" id="Seg_4877" n="e" s="T527">nagur, </ts>
               <ts e="T529" id="Seg_4879" n="e" s="T528">teʔtə, </ts>
               <ts e="T530" id="Seg_4881" n="e" s="T529">sumna, </ts>
               <ts e="T531" id="Seg_4883" n="e" s="T530">sejʔpü. </ts>
               <ts e="T532" id="Seg_4885" n="e" s="T531">Amitun, </ts>
               <ts e="T533" id="Seg_4887" n="e" s="T532">šinteʔtə. </ts>
            </ts>
            <ts e="T541" id="Seg_4888" n="sc" s="T540">
               <ts e="T541" id="Seg_4890" n="e" s="T540">Bʼeʔ. </ts>
            </ts>
            <ts e="T629" id="Seg_4891" n="sc" s="T542">
               <ts e="T543" id="Seg_4893" n="e" s="T542">Bʼeʔ </ts>
               <ts e="T544" id="Seg_4895" n="e" s="T543">onʼiʔ, </ts>
               <ts e="T545" id="Seg_4897" n="e" s="T544">bʼeʔ </ts>
               <ts e="T546" id="Seg_4899" n="e" s="T545">šide, </ts>
               <ts e="T547" id="Seg_4901" n="e" s="T546">bʼeʔ </ts>
               <ts e="T548" id="Seg_4903" n="e" s="T547">sumna, </ts>
               <ts e="T549" id="Seg_4905" n="e" s="T548">bʼeʔ </ts>
               <ts e="T550" id="Seg_4907" n="e" s="T549">muktuʔ. </ts>
               <ts e="T551" id="Seg_4909" n="e" s="T550">((BRK)). </ts>
               <ts e="T552" id="Seg_4911" n="e" s="T551">Măn </ts>
               <ts e="T553" id="Seg_4913" n="e" s="T552">unnʼa </ts>
               <ts e="T554" id="Seg_4915" n="e" s="T553">šobiam, </ts>
               <ts e="T555" id="Seg_4917" n="e" s="T554">a </ts>
               <ts e="T556" id="Seg_4919" n="e" s="T555">dĭzeŋ </ts>
               <ts e="T557" id="Seg_4921" n="e" s="T556">bar </ts>
               <ts e="T558" id="Seg_4923" n="e" s="T557">sumna </ts>
               <ts e="T559" id="Seg_4925" n="e" s="T558">šobiʔi. </ts>
               <ts e="T560" id="Seg_4927" n="e" s="T559">((BRK)). </ts>
               <ts e="T561" id="Seg_4929" n="e" s="T560">Măn </ts>
               <ts e="T562" id="Seg_4931" n="e" s="T561">mĭj </ts>
               <ts e="T563" id="Seg_4933" n="e" s="T562">(mĭr-) </ts>
               <ts e="T564" id="Seg_4935" n="e" s="T563">mĭnzəriem. </ts>
               <ts e="T565" id="Seg_4937" n="e" s="T564">Ugaːndə </ts>
               <ts e="T566" id="Seg_4939" n="e" s="T565">jakšə </ts>
               <ts e="T567" id="Seg_4941" n="e" s="T566">mĭj. </ts>
               <ts e="T568" id="Seg_4943" n="e" s="T567">Nʼamga, </ts>
               <ts e="T569" id="Seg_4945" n="e" s="T568">ujazʼiʔ. </ts>
               <ts e="T570" id="Seg_4947" n="e" s="T569">Köbergən </ts>
               <ts e="T571" id="Seg_4949" n="e" s="T570">embiem </ts>
               <ts e="T572" id="Seg_4951" n="e" s="T571">bar. </ts>
               <ts e="T573" id="Seg_4953" n="e" s="T572">Tus </ts>
               <ts e="T574" id="Seg_4955" n="e" s="T573">embiem. </ts>
               <ts e="T575" id="Seg_4957" n="e" s="T574">Toltanoʔ </ts>
               <ts e="T576" id="Seg_4959" n="e" s="T575">ambiem. </ts>
               <ts e="T577" id="Seg_4961" n="e" s="T576">Amnaʔ, </ts>
               <ts e="T578" id="Seg_4963" n="e" s="T577">amoraʔ! </ts>
               <ts e="T579" id="Seg_4965" n="e" s="T578">Obberəj. </ts>
               <ts e="T580" id="Seg_4967" n="e" s="T579">Kabarləj. </ts>
               <ts e="T581" id="Seg_4969" n="e" s="T580">((BRK)). </ts>
               <ts e="T582" id="Seg_4971" n="e" s="T581">Ugaːndə </ts>
               <ts e="T583" id="Seg_4973" n="e" s="T582">kuštü </ts>
               <ts e="T584" id="Seg_4975" n="e" s="T583">kuza. </ts>
               <ts e="T585" id="Seg_4977" n="e" s="T584">Măn </ts>
               <ts e="T586" id="Seg_4979" n="e" s="T585">dĭʔnə </ts>
               <ts e="T587" id="Seg_4981" n="e" s="T586">pimniem. </ts>
               <ts e="T588" id="Seg_4983" n="e" s="T587">Dĭ </ts>
               <ts e="T589" id="Seg_4985" n="e" s="T588">bar </ts>
               <ts e="T590" id="Seg_4987" n="e" s="T589">kutləj. </ts>
               <ts e="T591" id="Seg_4989" n="e" s="T590">Măn </ts>
               <ts e="T592" id="Seg_4991" n="e" s="T591">šaʔlaːmbiam </ts>
               <ts e="T593" id="Seg_4993" n="e" s="T592">štobɨ </ts>
               <ts e="T594" id="Seg_4995" n="e" s="T593">dĭ </ts>
               <ts e="T595" id="Seg_4997" n="e" s="T594">măna </ts>
               <ts e="T596" id="Seg_4999" n="e" s="T595">ej </ts>
               <ts e="T597" id="Seg_5001" n="e" s="T596">kubi. </ts>
               <ts e="T598" id="Seg_5003" n="e" s="T597">Măn </ts>
               <ts e="T599" id="Seg_5005" n="e" s="T598">dʼijenə </ts>
               <ts e="T600" id="Seg_5007" n="e" s="T599">nuʔməluʔləm. </ts>
               <ts e="T601" id="Seg_5009" n="e" s="T600">Dĭ </ts>
               <ts e="T602" id="Seg_5011" n="e" s="T601">măna </ts>
               <ts e="T603" id="Seg_5013" n="e" s="T602">(ej=) </ts>
               <ts e="T604" id="Seg_5015" n="e" s="T603">dĭn </ts>
               <ts e="T605" id="Seg_5017" n="e" s="T604">ej </ts>
               <ts e="T606" id="Seg_5019" n="e" s="T605">kuləj. </ts>
               <ts e="T607" id="Seg_5021" n="e" s="T606">Kabarləj. </ts>
               <ts e="T608" id="Seg_5023" n="e" s="T607">((BRK)). </ts>
               <ts e="T609" id="Seg_5025" n="e" s="T608">Tibizeŋ </ts>
               <ts e="T610" id="Seg_5027" n="e" s="T609">iʔgö </ts>
               <ts e="T611" id="Seg_5029" n="e" s="T610">oʔbdəbiʔi. </ts>
               <ts e="T612" id="Seg_5031" n="e" s="T611">Ĭmbidə </ts>
               <ts e="T613" id="Seg_5033" n="e" s="T612">bar </ts>
               <ts e="T614" id="Seg_5035" n="e" s="T613">dʼăbaktərlaʔbəʔjə, </ts>
               <ts e="T615" id="Seg_5037" n="e" s="T614">nada </ts>
               <ts e="T616" id="Seg_5039" n="e" s="T615">kanzittə </ts>
               <ts e="T617" id="Seg_5041" n="e" s="T616">nʼilgösʼtə </ts>
               <ts e="T618" id="Seg_5043" n="e" s="T617">(ĭm-) </ts>
               <ts e="T619" id="Seg_5045" n="e" s="T618">ĭmbi </ts>
               <ts e="T620" id="Seg_5047" n="e" s="T619">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T621" id="Seg_5049" n="e" s="T620">Măn </ts>
               <ts e="T622" id="Seg_5051" n="e" s="T621">dĭgəttə </ts>
               <ts e="T623" id="Seg_5053" n="e" s="T622">(tĭmnə- </ts>
               <ts e="T624" id="Seg_5055" n="e" s="T623">tĭmzit-) </ts>
               <ts e="T625" id="Seg_5057" n="e" s="T624">tĭmlem. </ts>
               <ts e="T626" id="Seg_5059" n="e" s="T625">Bar, </ts>
               <ts e="T627" id="Seg_5061" n="e" s="T626">daška </ts>
               <ts e="T628" id="Seg_5063" n="e" s="T627">ej </ts>
               <ts e="T629" id="Seg_5065" n="e" s="T628">dʼăbaktərliam. </ts>
            </ts>
            <ts e="T695" id="Seg_5066" n="sc" s="T634">
               <ts e="T635" id="Seg_5068" n="e" s="T634">Dĭ </ts>
               <ts e="T636" id="Seg_5070" n="e" s="T635">kuza </ts>
               <ts e="T637" id="Seg_5072" n="e" s="T636">bar </ts>
               <ts e="T638" id="Seg_5074" n="e" s="T637">jezerik. </ts>
               <ts e="T639" id="Seg_5076" n="e" s="T638">Inegən </ts>
               <ts e="T640" id="Seg_5078" n="e" s="T639">amnoluʔpi. </ts>
               <ts e="T641" id="Seg_5080" n="e" s="T640">Dĭgəttə </ts>
               <ts e="T642" id="Seg_5082" n="e" s="T641">üzəbi </ts>
               <ts e="T643" id="Seg_5084" n="e" s="T642">dʼünə. </ts>
               <ts e="T644" id="Seg_5086" n="e" s="T643">Dĭgəttə </ts>
               <ts e="T645" id="Seg_5088" n="e" s="T644">inet </ts>
               <ts e="T646" id="Seg_5090" n="e" s="T645">nuʔməluʔpi. </ts>
               <ts e="T647" id="Seg_5092" n="e" s="T646">(Üžü, </ts>
               <ts e="T648" id="Seg_5094" n="e" s="T647">üžü, </ts>
               <ts e="T649" id="Seg_5096" n="e" s="T648">üžüd-) </ts>
               <ts e="T650" id="Seg_5098" n="e" s="T649">Üžü </ts>
               <ts e="T651" id="Seg_5100" n="e" s="T650">dʼürdəbi. </ts>
               <ts e="T652" id="Seg_5102" n="e" s="T651">Maluʔpi </ts>
               <ts e="T653" id="Seg_5104" n="e" s="T652">bar. </ts>
               <ts e="T654" id="Seg_5106" n="e" s="T653">(Kunollaʔbə=) </ts>
               <ts e="T655" id="Seg_5108" n="e" s="T654">Kunollaʔ </ts>
               <ts e="T656" id="Seg_5110" n="e" s="T655">iʔbolaʔbə, </ts>
               <ts e="T657" id="Seg_5112" n="e" s="T656">saʔməluʔpi. </ts>
               <ts e="T658" id="Seg_5114" n="e" s="T657">Bar, </ts>
               <ts e="T659" id="Seg_5116" n="e" s="T658">kabarləj. </ts>
               <ts e="T660" id="Seg_5118" n="e" s="T659">((BRK)). </ts>
               <ts e="T661" id="Seg_5120" n="e" s="T660">Nʼizeŋ, </ts>
               <ts e="T662" id="Seg_5122" n="e" s="T661">koʔbsaŋ </ts>
               <ts e="T663" id="Seg_5124" n="e" s="T662">bar </ts>
               <ts e="T664" id="Seg_5126" n="e" s="T663">iʔgö </ts>
               <ts e="T665" id="Seg_5128" n="e" s="T664">oʔbdəbiʔi </ts>
               <ts e="T666" id="Seg_5130" n="e" s="T665">bar, </ts>
               <ts e="T667" id="Seg_5132" n="e" s="T666">nüjleʔbəʔjə. </ts>
               <ts e="T668" id="Seg_5134" n="e" s="T667">Garmonʼə </ts>
               <ts e="T669" id="Seg_5136" n="e" s="T668">sʼarlaʔbəʔjə, </ts>
               <ts e="T670" id="Seg_5138" n="e" s="T669">suʔmileʔbəʔjə. </ts>
               <ts e="T671" id="Seg_5140" n="e" s="T670">Kabarləj. </ts>
               <ts e="T672" id="Seg_5142" n="e" s="T671">((BRK)). </ts>
               <ts e="T673" id="Seg_5144" n="e" s="T672">Măn </ts>
               <ts e="T674" id="Seg_5146" n="e" s="T673">dʼijenə </ts>
               <ts e="T675" id="Seg_5148" n="e" s="T674">mĭmbiem. </ts>
               <ts e="T676" id="Seg_5150" n="e" s="T675">Keʔbde </ts>
               <ts e="T677" id="Seg_5152" n="e" s="T676">nĭŋgəbiem. </ts>
               <ts e="T678" id="Seg_5154" n="e" s="T677">Kălba </ts>
               <ts e="T679" id="Seg_5156" n="e" s="T678">nĭŋgəbiem. </ts>
               <ts e="T680" id="Seg_5158" n="e" s="T679">Pirogəʔi </ts>
               <ts e="T681" id="Seg_5160" n="e" s="T680">pürbiem, </ts>
               <ts e="T682" id="Seg_5162" n="e" s="T681">šiʔnʼileʔ </ts>
               <ts e="T683" id="Seg_5164" n="e" s="T682">mĭlem. </ts>
               <ts e="T684" id="Seg_5166" n="e" s="T683">Kabarləj. </ts>
               <ts e="T685" id="Seg_5168" n="e" s="T684">((BRK)). </ts>
               <ts e="T686" id="Seg_5170" n="e" s="T685">Măn </ts>
               <ts e="T687" id="Seg_5172" n="e" s="T686">iam </ts>
               <ts e="T688" id="Seg_5174" n="e" s="T687">nu </ts>
               <ts e="T689" id="Seg_5176" n="e" s="T688">ibi, </ts>
               <ts e="T690" id="Seg_5178" n="e" s="T689">abam </ts>
               <ts e="T691" id="Seg_5180" n="e" s="T690">dʼirək </ts>
               <ts e="T692" id="Seg_5182" n="e" s="T691">ibi. </ts>
               <ts e="T693" id="Seg_5184" n="e" s="T692">Šide </ts>
               <ts e="T694" id="Seg_5186" n="e" s="T693">nʼi </ts>
               <ts e="T695" id="Seg_5188" n="e" s="T694">ibiʔi. </ts>
            </ts>
            <ts e="T714" id="Seg_5189" n="sc" s="T698">
               <ts e="T699" id="Seg_5191" n="e" s="T698">Muktuʔ </ts>
               <ts e="T700" id="Seg_5193" n="e" s="T699">koʔbdaŋ </ts>
               <ts e="T701" id="Seg_5195" n="e" s="T700">ibi. </ts>
               <ts e="T702" id="Seg_5197" n="e" s="T701">Bar, </ts>
               <ts e="T703" id="Seg_5199" n="e" s="T702">kabarləj. </ts>
               <ts e="T704" id="Seg_5201" n="e" s="T703">((BRK)). </ts>
               <ts e="T705" id="Seg_5203" n="e" s="T704">Nagur </ts>
               <ts e="T706" id="Seg_5205" n="e" s="T705">külaːmbiʔi, </ts>
               <ts e="T707" id="Seg_5207" n="e" s="T706">a </ts>
               <ts e="T708" id="Seg_5209" n="e" s="T707">nagur </ts>
               <ts e="T709" id="Seg_5211" n="e" s="T708">ej </ts>
               <ts e="T710" id="Seg_5213" n="e" s="T709">kübiʔi. </ts>
               <ts e="T711" id="Seg_5215" n="e" s="T710">Măn </ts>
               <ts e="T712" id="Seg_5217" n="e" s="T711">abam </ts>
               <ts e="T714" id="Seg_5219" n="e" s="T712">albuga… </ts>
            </ts>
            <ts e="T908" id="Seg_5220" n="sc" s="T719">
               <ts e="T720" id="Seg_5222" n="e" s="T719">Bʼeʔ </ts>
               <ts e="T721" id="Seg_5224" n="e" s="T720">sumna </ts>
               <ts e="T722" id="Seg_5226" n="e" s="T721">kuʔpi. </ts>
               <ts e="T724" id="Seg_5228" n="e" s="T722">Bar. </ts>
               <ts e="T725" id="Seg_5230" n="e" s="T724">Kabarləj. </ts>
               <ts e="T726" id="Seg_5232" n="e" s="T725">Tažəp </ts>
               <ts e="T727" id="Seg_5234" n="e" s="T726">išo </ts>
               <ts e="T728" id="Seg_5236" n="e" s="T727">băra </ts>
               <ts e="T729" id="Seg_5238" n="e" s="T728">deʔpi. </ts>
               <ts e="T730" id="Seg_5240" n="e" s="T729">Kabarləj. </ts>
               <ts e="T731" id="Seg_5242" n="e" s="T730">Nu </ts>
               <ts e="T732" id="Seg_5244" n="e" s="T731">kuza </ts>
               <ts e="T733" id="Seg_5246" n="e" s="T732">ĭzemnie. </ts>
               <ts e="T734" id="Seg_5248" n="e" s="T733">((BRK)). </ts>
               <ts e="T735" id="Seg_5250" n="e" s="T734">Dĭgəttə </ts>
               <ts e="T736" id="Seg_5252" n="e" s="T735">kambi </ts>
               <ts e="T737" id="Seg_5254" n="e" s="T736">šamandə, </ts>
               <ts e="T738" id="Seg_5256" n="e" s="T737">dĭ </ts>
               <ts e="T739" id="Seg_5258" n="e" s="T738">(dĭ-) </ts>
               <ts e="T740" id="Seg_5260" n="e" s="T739">dĭm </ts>
               <ts e="T741" id="Seg_5262" n="e" s="T740">bar </ts>
               <ts e="T742" id="Seg_5264" n="e" s="T741">dʼazirlaʔpi. </ts>
               <ts e="T743" id="Seg_5266" n="e" s="T742">Daška </ts>
               <ts e="T744" id="Seg_5268" n="e" s="T743">bar </ts>
               <ts e="T745" id="Seg_5270" n="e" s="T744">naverna. </ts>
               <ts e="T746" id="Seg_5272" n="e" s="T745">Măn </ts>
               <ts e="T747" id="Seg_5274" n="e" s="T746">ugaːndə </ts>
               <ts e="T748" id="Seg_5276" n="e" s="T747">püjöliam. </ts>
               <ts e="T749" id="Seg_5278" n="e" s="T748">Amnaʔ, </ts>
               <ts e="T750" id="Seg_5280" n="e" s="T749">amoraʔ, </ts>
               <ts e="T751" id="Seg_5282" n="e" s="T750">šindi </ts>
               <ts e="T752" id="Seg_5284" n="e" s="T751">tănan </ts>
               <ts e="T753" id="Seg_5286" n="e" s="T752">ej </ts>
               <ts e="T754" id="Seg_5288" n="e" s="T753">mĭlie. </ts>
               <ts e="T755" id="Seg_5290" n="e" s="T754">Dĭgəttə </ts>
               <ts e="T756" id="Seg_5292" n="e" s="T755">(a- </ts>
               <ts e="T757" id="Seg_5294" n="e" s="T756">a-) </ts>
               <ts e="T758" id="Seg_5296" n="e" s="T757">iʔbeʔ </ts>
               <ts e="T759" id="Seg_5298" n="e" s="T758">da </ts>
               <ts e="T760" id="Seg_5300" n="e" s="T759">kunolaʔ! </ts>
               <ts e="T761" id="Seg_5302" n="e" s="T760">Kabarləj. </ts>
               <ts e="T762" id="Seg_5304" n="e" s="T761">((BRK)). </ts>
               <ts e="T763" id="Seg_5306" n="e" s="T762">Esseŋ </ts>
               <ts e="T764" id="Seg_5308" n="e" s="T763">nʼiʔnen </ts>
               <ts e="T765" id="Seg_5310" n="e" s="T764">bar </ts>
               <ts e="T766" id="Seg_5312" n="e" s="T765">(sʼ-) </ts>
               <ts e="T767" id="Seg_5314" n="e" s="T766">sʼarlaʔbəʔjə. </ts>
               <ts e="T768" id="Seg_5316" n="e" s="T767">Kirgarlaʔbəʔjə, </ts>
               <ts e="T769" id="Seg_5318" n="e" s="T768">dʼabrolaʔbəʔjə. </ts>
               <ts e="T770" id="Seg_5320" n="e" s="T769">Na_što </ts>
               <ts e="T771" id="Seg_5322" n="e" s="T770">dʼabrolial? </ts>
               <ts e="T772" id="Seg_5324" n="e" s="T771">Jakšə </ts>
               <ts e="T773" id="Seg_5326" n="e" s="T772">nada </ts>
               <ts e="T774" id="Seg_5328" n="e" s="T773">sʼarzittə. </ts>
               <ts e="T775" id="Seg_5330" n="e" s="T774">Iʔ </ts>
               <ts e="T776" id="Seg_5332" n="e" s="T775">dʼabrosʼtə! </ts>
               <ts e="T777" id="Seg_5334" n="e" s="T776">Kabarləj </ts>
               <ts e="T778" id="Seg_5336" n="e" s="T777">naverna. </ts>
               <ts e="T779" id="Seg_5338" n="e" s="T778">((BRK)). </ts>
               <ts e="T780" id="Seg_5340" n="e" s="T779">Iʔ </ts>
               <ts e="T781" id="Seg_5342" n="e" s="T780">dʼabrogaʔ! </ts>
               <ts e="T782" id="Seg_5344" n="e" s="T781">Nada </ts>
               <ts e="T783" id="Seg_5346" n="e" s="T782">jakšə </ts>
               <ts e="T784" id="Seg_5348" n="e" s="T783">sʼarzittə. </ts>
               <ts e="T785" id="Seg_5350" n="e" s="T784">Na_što </ts>
               <ts e="T786" id="Seg_5352" n="e" s="T785">dʼabrolaʔbəl? </ts>
               <ts e="T787" id="Seg_5354" n="e" s="T786">((BRK)). </ts>
               <ts e="T788" id="Seg_5356" n="e" s="T787">Măn </ts>
               <ts e="T789" id="Seg_5358" n="e" s="T788">teinen </ts>
               <ts e="T790" id="Seg_5360" n="e" s="T789">nubiam. </ts>
               <ts e="T791" id="Seg_5362" n="e" s="T790">Kuja </ts>
               <ts e="T792" id="Seg_5364" n="e" s="T791">iššo </ts>
               <ts e="T793" id="Seg_5366" n="e" s="T792">ej </ts>
               <ts e="T794" id="Seg_5368" n="e" s="T793">nubi </ts>
               <ts e="T795" id="Seg_5370" n="e" s="T794">măn </ts>
               <ts e="T796" id="Seg_5372" n="e" s="T795">nubiam. </ts>
               <ts e="T797" id="Seg_5374" n="e" s="T796">Dĭgəttə </ts>
               <ts e="T798" id="Seg_5376" n="e" s="T797">tüžöj </ts>
               <ts e="T799" id="Seg_5378" n="e" s="T798">surdəbiam. </ts>
               <ts e="T800" id="Seg_5380" n="e" s="T799">Büzəj </ts>
               <ts e="T801" id="Seg_5382" n="e" s="T800">bĭtəlbiem. </ts>
               <ts e="T802" id="Seg_5384" n="e" s="T801">Kurizəʔi </ts>
               <ts e="T803" id="Seg_5386" n="e" s="T802">(băd- </ts>
               <ts e="T804" id="Seg_5388" n="e" s="T803">bădl- </ts>
               <ts e="T805" id="Seg_5390" n="e" s="T804">bă-) </ts>
               <ts e="T806" id="Seg_5392" n="e" s="T805">bădlaʔbəm. </ts>
               <ts e="T807" id="Seg_5394" n="e" s="T806">Dĭgəttə </ts>
               <ts e="T808" id="Seg_5396" n="e" s="T807">munəjʔ </ts>
               <ts e="T809" id="Seg_5398" n="e" s="T808">pürbiem. </ts>
               <ts e="T810" id="Seg_5400" n="e" s="T809">Toltanoʔ </ts>
               <ts e="T811" id="Seg_5402" n="e" s="T810">mĭnzərbiem. </ts>
               <ts e="T812" id="Seg_5404" n="e" s="T811">Amorbiam. </ts>
               <ts e="T813" id="Seg_5406" n="e" s="T812">Dĭgəttə </ts>
               <ts e="T814" id="Seg_5408" n="e" s="T813">(nu </ts>
               <ts e="T815" id="Seg_5410" n="e" s="T814">kuza=) </ts>
               <ts e="T816" id="Seg_5412" n="e" s="T815">nu </ts>
               <ts e="T817" id="Seg_5414" n="e" s="T816">il </ts>
               <ts e="T818" id="Seg_5416" n="e" s="T817">šobiʔi, </ts>
               <ts e="T819" id="Seg_5418" n="e" s="T818">dʼăbaktərbiam. </ts>
               <ts e="T820" id="Seg_5420" n="e" s="T819">(Bal-) </ts>
               <ts e="T821" id="Seg_5422" n="e" s="T820">Bar </ts>
               <ts e="T822" id="Seg_5424" n="e" s="T821">tararluʔpiam. </ts>
               <ts e="T823" id="Seg_5426" n="e" s="T822">Kabarləj. </ts>
               <ts e="T824" id="Seg_5428" n="e" s="T823">((BRK)). </ts>
               <ts e="T825" id="Seg_5430" n="e" s="T824">Kandəgaʔ </ts>
               <ts e="T826" id="Seg_5432" n="e" s="T825">maʔnəl! </ts>
               <ts e="T827" id="Seg_5434" n="e" s="T826">Kabarləj </ts>
               <ts e="T828" id="Seg_5436" n="e" s="T827">dʼăbaktərzittə. </ts>
               <ts e="T829" id="Seg_5438" n="e" s="T828">Amgaʔ! </ts>
               <ts e="T830" id="Seg_5440" n="e" s="T829">Iʔbeʔ </ts>
               <ts e="T831" id="Seg_5442" n="e" s="T830">kunolzittə! </ts>
               <ts e="T832" id="Seg_5444" n="e" s="T831">Kabarləj </ts>
               <ts e="T833" id="Seg_5446" n="e" s="T832">daška. </ts>
               <ts e="T834" id="Seg_5448" n="e" s="T833">((…)). </ts>
               <ts e="T835" id="Seg_5450" n="e" s="T834">Šumuranə </ts>
               <ts e="T836" id="Seg_5452" n="e" s="T835">mĭmbiem. </ts>
               <ts e="T837" id="Seg_5454" n="e" s="T836">Ugaːndə </ts>
               <ts e="T838" id="Seg_5456" n="e" s="T837">il </ts>
               <ts e="T839" id="Seg_5458" n="e" s="T838">iʔgö </ts>
               <ts e="T840" id="Seg_5460" n="e" s="T839">kubiam. </ts>
               <ts e="T841" id="Seg_5462" n="e" s="T840">Koŋ </ts>
               <ts e="T842" id="Seg_5464" n="e" s="T841">bar </ts>
               <ts e="T843" id="Seg_5466" n="e" s="T842">šobiʔi. </ts>
               <ts e="T844" id="Seg_5468" n="e" s="T843">Tibizeŋ </ts>
               <ts e="T845" id="Seg_5470" n="e" s="T844">dĭn </ts>
               <ts e="T846" id="Seg_5472" n="e" s="T845">iʔgö, </ts>
               <ts e="T847" id="Seg_5474" n="e" s="T846">inezeŋ. </ts>
               <ts e="T848" id="Seg_5476" n="e" s="T847">Koʔbsaŋ, </ts>
               <ts e="T849" id="Seg_5478" n="e" s="T848">nʼizeŋ </ts>
               <ts e="T850" id="Seg_5480" n="e" s="T849">bar. </ts>
               <ts e="T851" id="Seg_5482" n="e" s="T850">Kabarləj. </ts>
               <ts e="T852" id="Seg_5484" n="e" s="T851">((BRK)). </ts>
               <ts e="T853" id="Seg_5486" n="e" s="T852">Ĭmbi </ts>
               <ts e="T854" id="Seg_5488" n="e" s="T853">dĭ </ts>
               <ts e="T855" id="Seg_5490" n="e" s="T854">todam </ts>
               <ts e="T856" id="Seg_5492" n="e" s="T855">kuza </ts>
               <ts e="T857" id="Seg_5494" n="e" s="T856">ej </ts>
               <ts e="T858" id="Seg_5496" n="e" s="T857">šobi, </ts>
               <ts e="T859" id="Seg_5498" n="e" s="T858">koʔbdo </ts>
               <ts e="T860" id="Seg_5500" n="e" s="T859">ej </ts>
               <ts e="T861" id="Seg_5502" n="e" s="T860">šobi. </ts>
               <ts e="T862" id="Seg_5504" n="e" s="T861">Gijen </ts>
               <ts e="T863" id="Seg_5506" n="e" s="T862">dĭzeŋ? </ts>
               <ts e="T864" id="Seg_5508" n="e" s="T863">Ĭmbi </ts>
               <ts e="T865" id="Seg_5510" n="e" s="T864">ej </ts>
               <ts e="T866" id="Seg_5512" n="e" s="T865">šobiʔi </ts>
               <ts e="T867" id="Seg_5514" n="e" s="T866">dʼăbaktərzittə </ts>
               <ts e="T868" id="Seg_5516" n="e" s="T867">mănzʼiʔ? </ts>
               <ts e="T869" id="Seg_5518" n="e" s="T868">Kabarləj. </ts>
               <ts e="T870" id="Seg_5520" n="e" s="T869">((BRK)). </ts>
               <ts e="T871" id="Seg_5522" n="e" s="T870">Gijen </ts>
               <ts e="T872" id="Seg_5524" n="e" s="T871">dĭzeŋ? </ts>
               <ts e="T873" id="Seg_5526" n="e" s="T872">Ĭmbi </ts>
               <ts e="T874" id="Seg_5528" n="e" s="T873">ej </ts>
               <ts e="T875" id="Seg_5530" n="e" s="T874">šobiʔi </ts>
               <ts e="T876" id="Seg_5532" n="e" s="T875">dʼăbaktərzittə? </ts>
               <ts e="T877" id="Seg_5534" n="e" s="T876">((BRK)). </ts>
               <ts e="T878" id="Seg_5536" n="e" s="T877">Teinen </ts>
               <ts e="T879" id="Seg_5538" n="e" s="T878">kabarləj </ts>
               <ts e="T880" id="Seg_5540" n="e" s="T879">dʼăbaktərzittə. </ts>
               <ts e="T881" id="Seg_5542" n="e" s="T880">Kanaʔ </ts>
               <ts e="T882" id="Seg_5544" n="e" s="T881">maʔnəl! </ts>
               <ts e="T883" id="Seg_5546" n="e" s="T882">Tăn </ts>
               <ts e="T884" id="Seg_5548" n="e" s="T883">măna </ts>
               <ts e="T885" id="Seg_5550" n="e" s="T884">(dĭ-) </ts>
               <ts e="T886" id="Seg_5552" n="e" s="T885">dăre </ts>
               <ts e="T887" id="Seg_5554" n="e" s="T886">ej </ts>
               <ts e="T888" id="Seg_5556" n="e" s="T887">(šü- </ts>
               <ts e="T889" id="Seg_5558" n="e" s="T888">šü- </ts>
               <ts e="T890" id="Seg_5560" n="e" s="T889">)… </ts>
               <ts e="T891" id="Seg_5562" n="e" s="T890">Tăn </ts>
               <ts e="T892" id="Seg_5564" n="e" s="T891">măna </ts>
               <ts e="T893" id="Seg_5566" n="e" s="T892">dăre </ts>
               <ts e="T894" id="Seg_5568" n="e" s="T893">ej </ts>
               <ts e="T895" id="Seg_5570" n="e" s="T894">(š-) </ts>
               <ts e="T896" id="Seg_5572" n="e" s="T895">tüšəlleʔ! </ts>
               <ts e="T897" id="Seg_5574" n="e" s="T896">Dăre </ts>
               <ts e="T898" id="Seg_5576" n="e" s="T897">ej </ts>
               <ts e="T899" id="Seg_5578" n="e" s="T898">jakšə </ts>
               <ts e="T900" id="Seg_5580" n="e" s="T899">mănzittə. </ts>
               <ts e="T901" id="Seg_5582" n="e" s="T900">Na_što </ts>
               <ts e="T902" id="Seg_5584" n="e" s="T901">sürerzittə </ts>
               <ts e="T903" id="Seg_5586" n="e" s="T902">il. </ts>
               <ts e="T904" id="Seg_5588" n="e" s="T903">Dĭ </ts>
               <ts e="T905" id="Seg_5590" n="e" s="T904">nuzaŋ </ts>
               <ts e="T906" id="Seg_5592" n="e" s="T905">il </ts>
               <ts e="T907" id="Seg_5594" n="e" s="T906">ведь. </ts>
               <ts e="T908" id="Seg_5596" n="e" s="T907">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T19" id="Seg_5597" s="T13">PKZ_196X_SU0203.PKZ.001 (002)</ta>
            <ta e="T22" id="Seg_5598" s="T19">PKZ_196X_SU0203.PKZ.002 (003)</ta>
            <ta e="T24" id="Seg_5599" s="T22">PKZ_196X_SU0203.PKZ.003 (004)</ta>
            <ta e="T27" id="Seg_5600" s="T24">PKZ_196X_SU0203.PKZ.004 (005)</ta>
            <ta e="T32" id="Seg_5601" s="T27">PKZ_196X_SU0203.PKZ.005 (006)</ta>
            <ta e="T34" id="Seg_5602" s="T32">PKZ_196X_SU0203.PKZ.006 (007)</ta>
            <ta e="T37" id="Seg_5603" s="T34">PKZ_196X_SU0203.PKZ.007 (008)</ta>
            <ta e="T38" id="Seg_5604" s="T37">PKZ_196X_SU0203.PKZ.008 (009)</ta>
            <ta e="T39" id="Seg_5605" s="T38">PKZ_196X_SU0203.PKZ.009 (010)</ta>
            <ta e="T42" id="Seg_5606" s="T39">PKZ_196X_SU0203.PKZ.010 (011)</ta>
            <ta e="T45" id="Seg_5607" s="T42">PKZ_196X_SU0203.PKZ.011 (012)</ta>
            <ta e="T48" id="Seg_5608" s="T45">PKZ_196X_SU0203.PKZ.012 (013)</ta>
            <ta e="T51" id="Seg_5609" s="T48">PKZ_196X_SU0203.PKZ.013 (014)</ta>
            <ta e="T54" id="Seg_5610" s="T51">PKZ_196X_SU0203.PKZ.014 (015)</ta>
            <ta e="T57" id="Seg_5611" s="T54">PKZ_196X_SU0203.PKZ.015 (016)</ta>
            <ta e="T60" id="Seg_5612" s="T57">PKZ_196X_SU0203.PKZ.016 (017)</ta>
            <ta e="T61" id="Seg_5613" s="T60">PKZ_196X_SU0203.PKZ.017 (018)</ta>
            <ta e="T62" id="Seg_5614" s="T61">PKZ_196X_SU0203.PKZ.018 (019)</ta>
            <ta e="T65" id="Seg_5615" s="T62">PKZ_196X_SU0203.PKZ.019 (020)</ta>
            <ta e="T69" id="Seg_5616" s="T65">PKZ_196X_SU0203.PKZ.020 (021)</ta>
            <ta e="T74" id="Seg_5617" s="T69">PKZ_196X_SU0203.PKZ.021 (022)</ta>
            <ta e="T78" id="Seg_5618" s="T74">PKZ_196X_SU0203.PKZ.022 (023)</ta>
            <ta e="T82" id="Seg_5619" s="T78">PKZ_196X_SU0203.PKZ.023 (024)</ta>
            <ta e="T83" id="Seg_5620" s="T82">PKZ_196X_SU0203.PKZ.024 (025)</ta>
            <ta e="T84" id="Seg_5621" s="T83">PKZ_196X_SU0203.PKZ.025 (026)</ta>
            <ta e="T93" id="Seg_5622" s="T84">PKZ_196X_SU0203.PKZ.026 (027)</ta>
            <ta e="T97" id="Seg_5623" s="T93">PKZ_196X_SU0203.PKZ.027 (028)</ta>
            <ta e="T102" id="Seg_5624" s="T97">PKZ_196X_SU0203.PKZ.028 (029)</ta>
            <ta e="T105" id="Seg_5625" s="T102">PKZ_196X_SU0203.PKZ.029 (030)</ta>
            <ta e="T110" id="Seg_5626" s="T105">PKZ_196X_SU0203.PKZ.030 (031)</ta>
            <ta e="T114" id="Seg_5627" s="T110">PKZ_196X_SU0203.PKZ.031 (032)</ta>
            <ta e="T115" id="Seg_5628" s="T114">PKZ_196X_SU0203.PKZ.032 (033)</ta>
            <ta e="T116" id="Seg_5629" s="T115">PKZ_196X_SU0203.PKZ.033 (034)</ta>
            <ta e="T119" id="Seg_5630" s="T116">PKZ_196X_SU0203.PKZ.034 (035)</ta>
            <ta e="T122" id="Seg_5631" s="T119">PKZ_196X_SU0203.PKZ.035 (036)</ta>
            <ta e="T125" id="Seg_5632" s="T122">PKZ_196X_SU0203.PKZ.036 (037)</ta>
            <ta e="T128" id="Seg_5633" s="T125">PKZ_196X_SU0203.PKZ.037 (038)</ta>
            <ta e="T135" id="Seg_5634" s="T128">PKZ_196X_SU0203.PKZ.038 (039)</ta>
            <ta e="T139" id="Seg_5635" s="T135">PKZ_196X_SU0203.PKZ.039 (040)</ta>
            <ta e="T141" id="Seg_5636" s="T139">PKZ_196X_SU0203.PKZ.040 (041)</ta>
            <ta e="T143" id="Seg_5637" s="T141">PKZ_196X_SU0203.PKZ.041 (042)</ta>
            <ta e="T144" id="Seg_5638" s="T143">PKZ_196X_SU0203.PKZ.042 (043)</ta>
            <ta e="T146" id="Seg_5639" s="T144">PKZ_196X_SU0203.PKZ.043 (044)</ta>
            <ta e="T148" id="Seg_5640" s="T146">PKZ_196X_SU0203.PKZ.044 (045)</ta>
            <ta e="T149" id="Seg_5641" s="T148">PKZ_196X_SU0203.PKZ.045 (046)</ta>
            <ta e="T151" id="Seg_5642" s="T149">PKZ_196X_SU0203.PKZ.046 (047)</ta>
            <ta e="T154" id="Seg_5643" s="T151">PKZ_196X_SU0203.PKZ.047 (048)</ta>
            <ta e="T157" id="Seg_5644" s="T154">PKZ_196X_SU0203.PKZ.048 (049)</ta>
            <ta e="T160" id="Seg_5645" s="T157">PKZ_196X_SU0203.PKZ.049 (050)</ta>
            <ta e="T161" id="Seg_5646" s="T160">PKZ_196X_SU0203.PKZ.050 (051)</ta>
            <ta e="T162" id="Seg_5647" s="T161">PKZ_196X_SU0203.PKZ.051 (052)</ta>
            <ta e="T170" id="Seg_5648" s="T162">PKZ_196X_SU0203.PKZ.052 (053)</ta>
            <ta e="T172" id="Seg_5649" s="T170">PKZ_196X_SU0203.PKZ.053 (054)</ta>
            <ta e="T173" id="Seg_5650" s="T172">PKZ_196X_SU0203.PKZ.054 (055)</ta>
            <ta e="T181" id="Seg_5651" s="T173">PKZ_196X_SU0203.PKZ.055 (056)</ta>
            <ta e="T184" id="Seg_5652" s="T181">PKZ_196X_SU0203.PKZ.056 (057)</ta>
            <ta e="T188" id="Seg_5653" s="T184">PKZ_196X_SU0203.PKZ.057 (058)</ta>
            <ta e="T189" id="Seg_5654" s="T188">PKZ_196X_SU0203.PKZ.058 (059)</ta>
            <ta e="T190" id="Seg_5655" s="T189">PKZ_196X_SU0203.PKZ.059 (060)</ta>
            <ta e="T194" id="Seg_5656" s="T190">PKZ_196X_SU0203.PKZ.060 (061)</ta>
            <ta e="T196" id="Seg_5657" s="T194">PKZ_196X_SU0203.PKZ.061 (062)</ta>
            <ta e="T201" id="Seg_5658" s="T196">PKZ_196X_SU0203.PKZ.062 (063)</ta>
            <ta e="T203" id="Seg_5659" s="T201">PKZ_196X_SU0203.PKZ.063 (064)</ta>
            <ta e="T206" id="Seg_5660" s="T203">PKZ_196X_SU0203.PKZ.064 (065)</ta>
            <ta e="T209" id="Seg_5661" s="T206">PKZ_196X_SU0203.PKZ.065 (066)</ta>
            <ta e="T212" id="Seg_5662" s="T209">PKZ_196X_SU0203.PKZ.066 (067)</ta>
            <ta e="T216" id="Seg_5663" s="T212">PKZ_196X_SU0203.PKZ.067 (068)</ta>
            <ta e="T219" id="Seg_5664" s="T216">PKZ_196X_SU0203.PKZ.068 (069)</ta>
            <ta e="T220" id="Seg_5665" s="T219">PKZ_196X_SU0203.PKZ.069 (070)</ta>
            <ta e="T221" id="Seg_5666" s="T220">PKZ_196X_SU0203.PKZ.070 (071)</ta>
            <ta e="T224" id="Seg_5667" s="T221">PKZ_196X_SU0203.PKZ.071 (072)</ta>
            <ta e="T226" id="Seg_5668" s="T224">PKZ_196X_SU0203.PKZ.072 (073)</ta>
            <ta e="T228" id="Seg_5669" s="T226">PKZ_196X_SU0203.PKZ.073 (074)</ta>
            <ta e="T230" id="Seg_5670" s="T228">PKZ_196X_SU0203.PKZ.074 (075)</ta>
            <ta e="T233" id="Seg_5671" s="T230">PKZ_196X_SU0203.PKZ.075 (076)</ta>
            <ta e="T237" id="Seg_5672" s="T233">PKZ_196X_SU0203.PKZ.076 (077)</ta>
            <ta e="T241" id="Seg_5673" s="T237">PKZ_196X_SU0203.PKZ.077 (078)</ta>
            <ta e="T248" id="Seg_5674" s="T241">PKZ_196X_SU0203.PKZ.078 (079)</ta>
            <ta e="T249" id="Seg_5675" s="T248">PKZ_196X_SU0203.PKZ.079 (080)</ta>
            <ta e="T250" id="Seg_5676" s="T249">PKZ_196X_SU0203.PKZ.080 (081)</ta>
            <ta e="T254" id="Seg_5677" s="T250">PKZ_196X_SU0203.PKZ.081 (082)</ta>
            <ta e="T256" id="Seg_5678" s="T254">PKZ_196X_SU0203.PKZ.082 (083)</ta>
            <ta e="T258" id="Seg_5679" s="T256">PKZ_196X_SU0203.PKZ.083 (084)</ta>
            <ta e="T260" id="Seg_5680" s="T258">PKZ_196X_SU0203.PKZ.084 (085)</ta>
            <ta e="T262" id="Seg_5681" s="T260">PKZ_196X_SU0203.PKZ.085 (086)</ta>
            <ta e="T263" id="Seg_5682" s="T262">PKZ_196X_SU0203.PKZ.086 (087)</ta>
            <ta e="T266" id="Seg_5683" s="T263">PKZ_196X_SU0203.PKZ.087 (088)</ta>
            <ta e="T268" id="Seg_5684" s="T266">PKZ_196X_SU0203.PKZ.088 (089)</ta>
            <ta e="T275" id="Seg_5685" s="T268">PKZ_196X_SU0203.PKZ.089 (090)</ta>
            <ta e="T279" id="Seg_5686" s="T275">PKZ_196X_SU0203.PKZ.090 (091)</ta>
            <ta e="T280" id="Seg_5687" s="T279">PKZ_196X_SU0203.PKZ.091 (092)</ta>
            <ta e="T281" id="Seg_5688" s="T280">PKZ_196X_SU0203.PKZ.092 (093)</ta>
            <ta e="T282" id="Seg_5689" s="T281">PKZ_196X_SU0203.PKZ.093 (094)</ta>
            <ta e="T286" id="Seg_5690" s="T282">PKZ_196X_SU0203.PKZ.094 (095)</ta>
            <ta e="T290" id="Seg_5691" s="T286">PKZ_196X_SU0203.PKZ.095 (096)</ta>
            <ta e="T292" id="Seg_5692" s="T290">PKZ_196X_SU0203.PKZ.096 (097)</ta>
            <ta e="T295" id="Seg_5693" s="T292">PKZ_196X_SU0203.PKZ.097 (098)</ta>
            <ta e="T296" id="Seg_5694" s="T295">PKZ_196X_SU0203.PKZ.098 (099)</ta>
            <ta e="T297" id="Seg_5695" s="T296">PKZ_196X_SU0203.PKZ.099 (100)</ta>
            <ta e="T302" id="Seg_5696" s="T297">PKZ_196X_SU0203.PKZ.100 (101)</ta>
            <ta e="T307" id="Seg_5697" s="T302">PKZ_196X_SU0203.PKZ.101 (102)</ta>
            <ta e="T311" id="Seg_5698" s="T307">PKZ_196X_SU0203.PKZ.102 (103)</ta>
            <ta e="T315" id="Seg_5699" s="T311">PKZ_196X_SU0203.PKZ.103 (104)</ta>
            <ta e="T318" id="Seg_5700" s="T315">PKZ_196X_SU0203.PKZ.104 (105)</ta>
            <ta e="T320" id="Seg_5701" s="T318">PKZ_196X_SU0203.PKZ.105 (106)</ta>
            <ta e="T323" id="Seg_5702" s="T320">PKZ_196X_SU0203.PKZ.106 (107)</ta>
            <ta e="T324" id="Seg_5703" s="T323">PKZ_196X_SU0203.PKZ.107 (108)</ta>
            <ta e="T329" id="Seg_5704" s="T324">PKZ_196X_SU0203.PKZ.108 (109)</ta>
            <ta e="T330" id="Seg_5705" s="T329">PKZ_196X_SU0203.PKZ.109 (110)</ta>
            <ta e="T333" id="Seg_5706" s="T330">PKZ_196X_SU0203.PKZ.110 (111)</ta>
            <ta e="T336" id="Seg_5707" s="T333">PKZ_196X_SU0203.PKZ.111 (112)</ta>
            <ta e="T337" id="Seg_5708" s="T336">PKZ_196X_SU0203.PKZ.112 (113)</ta>
            <ta e="T341" id="Seg_5709" s="T337">PKZ_196X_SU0203.PKZ.113 (114)</ta>
            <ta e="T344" id="Seg_5710" s="T341">PKZ_196X_SU0203.PKZ.114 (115)</ta>
            <ta e="T348" id="Seg_5711" s="T344">PKZ_196X_SU0203.PKZ.115 (116)</ta>
            <ta e="T351" id="Seg_5712" s="T348">PKZ_196X_SU0203.PKZ.116 (117)</ta>
            <ta e="T353" id="Seg_5713" s="T351">PKZ_196X_SU0203.PKZ.117 (118)</ta>
            <ta e="T359" id="Seg_5714" s="T353">PKZ_196X_SU0203.PKZ.118 (119)</ta>
            <ta e="T363" id="Seg_5715" s="T359">PKZ_196X_SU0203.PKZ.119 (120)</ta>
            <ta e="T364" id="Seg_5716" s="T363">PKZ_196X_SU0203.PKZ.120 (121)</ta>
            <ta e="T367" id="Seg_5717" s="T364">PKZ_196X_SU0203.PKZ.121 (122)</ta>
            <ta e="T368" id="Seg_5718" s="T367">PKZ_196X_SU0203.PKZ.122 (123)</ta>
            <ta e="T370" id="Seg_5719" s="T368">PKZ_196X_SU0203.PKZ.123 (124)</ta>
            <ta e="T372" id="Seg_5720" s="T370">PKZ_196X_SU0203.PKZ.124 (125)</ta>
            <ta e="T375" id="Seg_5721" s="T372">PKZ_196X_SU0203.PKZ.125 (126)</ta>
            <ta e="T376" id="Seg_5722" s="T375">PKZ_196X_SU0203.PKZ.126 (127)</ta>
            <ta e="T377" id="Seg_5723" s="T376">PKZ_196X_SU0203.PKZ.127 (128)</ta>
            <ta e="T378" id="Seg_5724" s="T377">PKZ_196X_SU0203.PKZ.128 (129)</ta>
            <ta e="T382" id="Seg_5725" s="T378">PKZ_196X_SU0203.PKZ.129 (130)</ta>
            <ta e="T385" id="Seg_5726" s="T382">PKZ_196X_SU0203.PKZ.130 (131)</ta>
            <ta e="T392" id="Seg_5727" s="T385">PKZ_196X_SU0203.PKZ.131 (132)</ta>
            <ta e="T393" id="Seg_5728" s="T392">PKZ_196X_SU0203.PKZ.132 (133)</ta>
            <ta e="T394" id="Seg_5729" s="T393">PKZ_196X_SU0203.PKZ.133 (134)</ta>
            <ta e="T395" id="Seg_5730" s="T394">PKZ_196X_SU0203.PKZ.134 (135)</ta>
            <ta e="T398" id="Seg_5731" s="T395">PKZ_196X_SU0203.PKZ.135 (136)</ta>
            <ta e="T401" id="Seg_5732" s="T398">PKZ_196X_SU0203.PKZ.136 (137)</ta>
            <ta e="T406" id="Seg_5733" s="T401">PKZ_196X_SU0203.PKZ.137 (138)</ta>
            <ta e="T410" id="Seg_5734" s="T406">PKZ_196X_SU0203.PKZ.138 (139)</ta>
            <ta e="T414" id="Seg_5735" s="T410">PKZ_196X_SU0203.PKZ.139 (140)</ta>
            <ta e="T420" id="Seg_5736" s="T414">PKZ_196X_SU0203.PKZ.140 (141)</ta>
            <ta e="T424" id="Seg_5737" s="T420">PKZ_196X_SU0203.PKZ.141 (142)</ta>
            <ta e="T425" id="Seg_5738" s="T424">PKZ_196X_SU0203.PKZ.142 (143)</ta>
            <ta e="T428" id="Seg_5739" s="T425">PKZ_196X_SU0203.PKZ.143 (144)</ta>
            <ta e="T433" id="Seg_5740" s="T428">PKZ_196X_SU0203.PKZ.144 (145)</ta>
            <ta e="T435" id="Seg_5741" s="T433">PKZ_196X_SU0203.PKZ.145 (146)</ta>
            <ta e="T440" id="Seg_5742" s="T435">PKZ_196X_SU0203.PKZ.146 (147)</ta>
            <ta e="T442" id="Seg_5743" s="T440">PKZ_196X_SU0203.PKZ.147 (148)</ta>
            <ta e="T446" id="Seg_5744" s="T442">PKZ_196X_SU0203.PKZ.148 (149)</ta>
            <ta e="T447" id="Seg_5745" s="T446">PKZ_196X_SU0203.PKZ.149 (150)</ta>
            <ta e="T450" id="Seg_5746" s="T447">PKZ_196X_SU0203.PKZ.150 (151)</ta>
            <ta e="T451" id="Seg_5747" s="T450">PKZ_196X_SU0203.PKZ.151 (152)</ta>
            <ta e="T456" id="Seg_5748" s="T451">PKZ_196X_SU0203.PKZ.152 (153)</ta>
            <ta e="T459" id="Seg_5749" s="T456">PKZ_196X_SU0203.PKZ.153 (154)</ta>
            <ta e="T462" id="Seg_5750" s="T459">PKZ_196X_SU0203.PKZ.154 (155)</ta>
            <ta e="T465" id="Seg_5751" s="T462">PKZ_196X_SU0203.PKZ.155 (156)</ta>
            <ta e="T467" id="Seg_5752" s="T465">PKZ_196X_SU0203.PKZ.156 (157)</ta>
            <ta e="T471" id="Seg_5753" s="T467">PKZ_196X_SU0203.PKZ.157 (158)</ta>
            <ta e="T476" id="Seg_5754" s="T471">PKZ_196X_SU0203.PKZ.158 (159)</ta>
            <ta e="T479" id="Seg_5755" s="T476">PKZ_196X_SU0203.PKZ.159 (160)</ta>
            <ta e="T481" id="Seg_5756" s="T479">PKZ_196X_SU0203.PKZ.160 (161)</ta>
            <ta e="T484" id="Seg_5757" s="T481">PKZ_196X_SU0203.PKZ.161 (162)</ta>
            <ta e="T485" id="Seg_5758" s="T484">PKZ_196X_SU0203.PKZ.162 (163)</ta>
            <ta e="T488" id="Seg_5759" s="T485">PKZ_196X_SU0203.PKZ.163 (164)</ta>
            <ta e="T492" id="Seg_5760" s="T488">PKZ_196X_SU0203.PKZ.164 (165)</ta>
            <ta e="T494" id="Seg_5761" s="T492">PKZ_196X_SU0203.PKZ.165 (166)</ta>
            <ta e="T495" id="Seg_5762" s="T494">PKZ_196X_SU0203.PKZ.166 (167)</ta>
            <ta e="T498" id="Seg_5763" s="T495">PKZ_196X_SU0203.PKZ.167 (168)</ta>
            <ta e="T504" id="Seg_5764" s="T498">PKZ_196X_SU0203.PKZ.168 (169)</ta>
            <ta e="T505" id="Seg_5765" s="T504">PKZ_196X_SU0203.PKZ.169 (170)</ta>
            <ta e="T508" id="Seg_5766" s="T505">PKZ_196X_SU0203.PKZ.170 (171)</ta>
            <ta e="T511" id="Seg_5767" s="T508">PKZ_196X_SU0203.PKZ.171 (172)</ta>
            <ta e="T512" id="Seg_5768" s="T511">PKZ_196X_SU0203.PKZ.172 (173)</ta>
            <ta e="T515" id="Seg_5769" s="T512">PKZ_196X_SU0203.PKZ.173 (174)</ta>
            <ta e="T518" id="Seg_5770" s="T515">PKZ_196X_SU0203.PKZ.174 (175)</ta>
            <ta e="T522" id="Seg_5771" s="T518">PKZ_196X_SU0203.PKZ.175 (176)</ta>
            <ta e="T523" id="Seg_5772" s="T522">PKZ_196X_SU0203.PKZ.176 (177)</ta>
            <ta e="T524" id="Seg_5773" s="T523">PKZ_196X_SU0203.PKZ.177 (178)</ta>
            <ta e="T531" id="Seg_5774" s="T524">PKZ_196X_SU0203.PKZ.178 (179)</ta>
            <ta e="T533" id="Seg_5775" s="T531">PKZ_196X_SU0203.PKZ.179 (180)</ta>
            <ta e="T541" id="Seg_5776" s="T540">PKZ_196X_SU0203.PKZ.180 (182)</ta>
            <ta e="T550" id="Seg_5777" s="T542">PKZ_196X_SU0203.PKZ.181 (184)</ta>
            <ta e="T551" id="Seg_5778" s="T550">PKZ_196X_SU0203.PKZ.182 (185)</ta>
            <ta e="T559" id="Seg_5779" s="T551">PKZ_196X_SU0203.PKZ.183 (186)</ta>
            <ta e="T560" id="Seg_5780" s="T559">PKZ_196X_SU0203.PKZ.184 (187)</ta>
            <ta e="T564" id="Seg_5781" s="T560">PKZ_196X_SU0203.PKZ.185 (188)</ta>
            <ta e="T567" id="Seg_5782" s="T564">PKZ_196X_SU0203.PKZ.186 (189)</ta>
            <ta e="T569" id="Seg_5783" s="T567">PKZ_196X_SU0203.PKZ.187 (190)</ta>
            <ta e="T572" id="Seg_5784" s="T569">PKZ_196X_SU0203.PKZ.188 (191)</ta>
            <ta e="T574" id="Seg_5785" s="T572">PKZ_196X_SU0203.PKZ.189 (192)</ta>
            <ta e="T576" id="Seg_5786" s="T574">PKZ_196X_SU0203.PKZ.190 (193)</ta>
            <ta e="T578" id="Seg_5787" s="T576">PKZ_196X_SU0203.PKZ.191 (194)</ta>
            <ta e="T579" id="Seg_5788" s="T578">PKZ_196X_SU0203.PKZ.192 (195)</ta>
            <ta e="T580" id="Seg_5789" s="T579">PKZ_196X_SU0203.PKZ.193 (196)</ta>
            <ta e="T581" id="Seg_5790" s="T580">PKZ_196X_SU0203.PKZ.194 (197)</ta>
            <ta e="T584" id="Seg_5791" s="T581">PKZ_196X_SU0203.PKZ.195 (198)</ta>
            <ta e="T587" id="Seg_5792" s="T584">PKZ_196X_SU0203.PKZ.196 (199)</ta>
            <ta e="T590" id="Seg_5793" s="T587">PKZ_196X_SU0203.PKZ.197 (200)</ta>
            <ta e="T597" id="Seg_5794" s="T590">PKZ_196X_SU0203.PKZ.198 (201)</ta>
            <ta e="T600" id="Seg_5795" s="T597">PKZ_196X_SU0203.PKZ.199 (202)</ta>
            <ta e="T606" id="Seg_5796" s="T600">PKZ_196X_SU0203.PKZ.200 (203)</ta>
            <ta e="T607" id="Seg_5797" s="T606">PKZ_196X_SU0203.PKZ.201 (204)</ta>
            <ta e="T608" id="Seg_5798" s="T607">PKZ_196X_SU0203.PKZ.202 (205)</ta>
            <ta e="T611" id="Seg_5799" s="T608">PKZ_196X_SU0203.PKZ.203 (206)</ta>
            <ta e="T620" id="Seg_5800" s="T611">PKZ_196X_SU0203.PKZ.204 (207)</ta>
            <ta e="T625" id="Seg_5801" s="T620">PKZ_196X_SU0203.PKZ.205 (208)</ta>
            <ta e="T629" id="Seg_5802" s="T625">PKZ_196X_SU0203.PKZ.206 (209)</ta>
            <ta e="T638" id="Seg_5803" s="T634">PKZ_196X_SU0203.PKZ.207 (212)</ta>
            <ta e="T640" id="Seg_5804" s="T638">PKZ_196X_SU0203.PKZ.208 (213)</ta>
            <ta e="T643" id="Seg_5805" s="T640">PKZ_196X_SU0203.PKZ.209 (214)</ta>
            <ta e="T646" id="Seg_5806" s="T643">PKZ_196X_SU0203.PKZ.210 (215)</ta>
            <ta e="T651" id="Seg_5807" s="T646">PKZ_196X_SU0203.PKZ.211 (216)</ta>
            <ta e="T653" id="Seg_5808" s="T651">PKZ_196X_SU0203.PKZ.212 (217)</ta>
            <ta e="T657" id="Seg_5809" s="T653">PKZ_196X_SU0203.PKZ.213 (218)</ta>
            <ta e="T659" id="Seg_5810" s="T657">PKZ_196X_SU0203.PKZ.214 (219)</ta>
            <ta e="T660" id="Seg_5811" s="T659">PKZ_196X_SU0203.PKZ.215 (220)</ta>
            <ta e="T667" id="Seg_5812" s="T660">PKZ_196X_SU0203.PKZ.216 (221)</ta>
            <ta e="T670" id="Seg_5813" s="T667">PKZ_196X_SU0203.PKZ.217 (222)</ta>
            <ta e="T671" id="Seg_5814" s="T670">PKZ_196X_SU0203.PKZ.218 (223)</ta>
            <ta e="T672" id="Seg_5815" s="T671">PKZ_196X_SU0203.PKZ.219 (224)</ta>
            <ta e="T675" id="Seg_5816" s="T672">PKZ_196X_SU0203.PKZ.220 (225)</ta>
            <ta e="T677" id="Seg_5817" s="T675">PKZ_196X_SU0203.PKZ.221 (226)</ta>
            <ta e="T679" id="Seg_5818" s="T677">PKZ_196X_SU0203.PKZ.222 (227)</ta>
            <ta e="T683" id="Seg_5819" s="T679">PKZ_196X_SU0203.PKZ.223 (228)</ta>
            <ta e="T684" id="Seg_5820" s="T683">PKZ_196X_SU0203.PKZ.224 (229)</ta>
            <ta e="T685" id="Seg_5821" s="T684">PKZ_196X_SU0203.PKZ.225 (230)</ta>
            <ta e="T692" id="Seg_5822" s="T685">PKZ_196X_SU0203.PKZ.226 (231)</ta>
            <ta e="T695" id="Seg_5823" s="T692">PKZ_196X_SU0203.PKZ.227 (232)</ta>
            <ta e="T701" id="Seg_5824" s="T698">PKZ_196X_SU0203.PKZ.228 (234)</ta>
            <ta e="T703" id="Seg_5825" s="T701">PKZ_196X_SU0203.PKZ.229 (235)</ta>
            <ta e="T704" id="Seg_5826" s="T703">PKZ_196X_SU0203.PKZ.230 (236)</ta>
            <ta e="T710" id="Seg_5827" s="T704">PKZ_196X_SU0203.PKZ.231 (237)</ta>
            <ta e="T714" id="Seg_5828" s="T710">PKZ_196X_SU0203.PKZ.232 (238)</ta>
            <ta e="T722" id="Seg_5829" s="T719">PKZ_196X_SU0203.PKZ.233 (240)</ta>
            <ta e="T724" id="Seg_5830" s="T722">PKZ_196X_SU0203.PKZ.234 (241)</ta>
            <ta e="T725" id="Seg_5831" s="T724">PKZ_196X_SU0203.PKZ.235 (243)</ta>
            <ta e="T729" id="Seg_5832" s="T725">PKZ_196X_SU0203.PKZ.236 (244)</ta>
            <ta e="T730" id="Seg_5833" s="T729">PKZ_196X_SU0203.PKZ.237 (245)</ta>
            <ta e="T733" id="Seg_5834" s="T730">PKZ_196X_SU0203.PKZ.238 (246)</ta>
            <ta e="T734" id="Seg_5835" s="T733">PKZ_196X_SU0203.PKZ.239 (247)</ta>
            <ta e="T742" id="Seg_5836" s="T734">PKZ_196X_SU0203.PKZ.240 (248)</ta>
            <ta e="T745" id="Seg_5837" s="T742">PKZ_196X_SU0203.PKZ.241 (249)</ta>
            <ta e="T748" id="Seg_5838" s="T745">PKZ_196X_SU0203.PKZ.242 (250)</ta>
            <ta e="T754" id="Seg_5839" s="T748">PKZ_196X_SU0203.PKZ.243 (251)</ta>
            <ta e="T760" id="Seg_5840" s="T754">PKZ_196X_SU0203.PKZ.244 (252)</ta>
            <ta e="T761" id="Seg_5841" s="T760">PKZ_196X_SU0203.PKZ.245 (253)</ta>
            <ta e="T762" id="Seg_5842" s="T761">PKZ_196X_SU0203.PKZ.246 (254)</ta>
            <ta e="T767" id="Seg_5843" s="T762">PKZ_196X_SU0203.PKZ.247 (255)</ta>
            <ta e="T769" id="Seg_5844" s="T767">PKZ_196X_SU0203.PKZ.248 (256)</ta>
            <ta e="T771" id="Seg_5845" s="T769">PKZ_196X_SU0203.PKZ.249 (257)</ta>
            <ta e="T774" id="Seg_5846" s="T771">PKZ_196X_SU0203.PKZ.250 (258)</ta>
            <ta e="T776" id="Seg_5847" s="T774">PKZ_196X_SU0203.PKZ.251 (259)</ta>
            <ta e="T778" id="Seg_5848" s="T776">PKZ_196X_SU0203.PKZ.252 (260)</ta>
            <ta e="T779" id="Seg_5849" s="T778">PKZ_196X_SU0203.PKZ.253 (261)</ta>
            <ta e="T781" id="Seg_5850" s="T779">PKZ_196X_SU0203.PKZ.254 (262)</ta>
            <ta e="T784" id="Seg_5851" s="T781">PKZ_196X_SU0203.PKZ.255 (263)</ta>
            <ta e="T786" id="Seg_5852" s="T784">PKZ_196X_SU0203.PKZ.256 (264)</ta>
            <ta e="T787" id="Seg_5853" s="T786">PKZ_196X_SU0203.PKZ.257 (265)</ta>
            <ta e="T790" id="Seg_5854" s="T787">PKZ_196X_SU0203.PKZ.258 (266)</ta>
            <ta e="T796" id="Seg_5855" s="T790">PKZ_196X_SU0203.PKZ.259 (267)</ta>
            <ta e="T799" id="Seg_5856" s="T796">PKZ_196X_SU0203.PKZ.260 (268)</ta>
            <ta e="T801" id="Seg_5857" s="T799">PKZ_196X_SU0203.PKZ.261 (269)</ta>
            <ta e="T806" id="Seg_5858" s="T801">PKZ_196X_SU0203.PKZ.262 (270)</ta>
            <ta e="T809" id="Seg_5859" s="T806">PKZ_196X_SU0203.PKZ.263 (271)</ta>
            <ta e="T811" id="Seg_5860" s="T809">PKZ_196X_SU0203.PKZ.264 (272)</ta>
            <ta e="T812" id="Seg_5861" s="T811">PKZ_196X_SU0203.PKZ.265 (273)</ta>
            <ta e="T819" id="Seg_5862" s="T812">PKZ_196X_SU0203.PKZ.266 (274)</ta>
            <ta e="T822" id="Seg_5863" s="T819">PKZ_196X_SU0203.PKZ.267 (275)</ta>
            <ta e="T823" id="Seg_5864" s="T822">PKZ_196X_SU0203.PKZ.268 (276)</ta>
            <ta e="T824" id="Seg_5865" s="T823">PKZ_196X_SU0203.PKZ.269 (277)</ta>
            <ta e="T826" id="Seg_5866" s="T824">PKZ_196X_SU0203.PKZ.270 (278)</ta>
            <ta e="T828" id="Seg_5867" s="T826">PKZ_196X_SU0203.PKZ.271 (279)</ta>
            <ta e="T829" id="Seg_5868" s="T828">PKZ_196X_SU0203.PKZ.272 (280)</ta>
            <ta e="T831" id="Seg_5869" s="T829">PKZ_196X_SU0203.PKZ.273 (281)</ta>
            <ta e="T833" id="Seg_5870" s="T831">PKZ_196X_SU0203.PKZ.274 (282)</ta>
            <ta e="T834" id="Seg_5871" s="T833">PKZ_196X_SU0203.PKZ.275 (283)</ta>
            <ta e="T836" id="Seg_5872" s="T834">PKZ_196X_SU0203.PKZ.276 (284)</ta>
            <ta e="T840" id="Seg_5873" s="T836">PKZ_196X_SU0203.PKZ.277 (285)</ta>
            <ta e="T843" id="Seg_5874" s="T840">PKZ_196X_SU0203.PKZ.278 (286)</ta>
            <ta e="T847" id="Seg_5875" s="T843">PKZ_196X_SU0203.PKZ.279 (287)</ta>
            <ta e="T850" id="Seg_5876" s="T847">PKZ_196X_SU0203.PKZ.280 (288)</ta>
            <ta e="T851" id="Seg_5877" s="T850">PKZ_196X_SU0203.PKZ.281 (289)</ta>
            <ta e="T852" id="Seg_5878" s="T851">PKZ_196X_SU0203.PKZ.282 (290)</ta>
            <ta e="T861" id="Seg_5879" s="T852">PKZ_196X_SU0203.PKZ.283 (291)</ta>
            <ta e="T863" id="Seg_5880" s="T861">PKZ_196X_SU0203.PKZ.284 (292)</ta>
            <ta e="T868" id="Seg_5881" s="T863">PKZ_196X_SU0203.PKZ.285 (293)</ta>
            <ta e="T869" id="Seg_5882" s="T868">PKZ_196X_SU0203.PKZ.286 (294)</ta>
            <ta e="T870" id="Seg_5883" s="T869">PKZ_196X_SU0203.PKZ.287 (295)</ta>
            <ta e="T872" id="Seg_5884" s="T870">PKZ_196X_SU0203.PKZ.288 (296)</ta>
            <ta e="T876" id="Seg_5885" s="T872">PKZ_196X_SU0203.PKZ.289 (297)</ta>
            <ta e="T877" id="Seg_5886" s="T876">PKZ_196X_SU0203.PKZ.290 (298)</ta>
            <ta e="T880" id="Seg_5887" s="T877">PKZ_196X_SU0203.PKZ.291 (299)</ta>
            <ta e="T882" id="Seg_5888" s="T880">PKZ_196X_SU0203.PKZ.292 (300)</ta>
            <ta e="T890" id="Seg_5889" s="T882">PKZ_196X_SU0203.PKZ.293 (301)</ta>
            <ta e="T896" id="Seg_5890" s="T890">PKZ_196X_SU0203.PKZ.294 (302)</ta>
            <ta e="T900" id="Seg_5891" s="T896">PKZ_196X_SU0203.PKZ.295 (303)</ta>
            <ta e="T903" id="Seg_5892" s="T900">PKZ_196X_SU0203.PKZ.296 (304)</ta>
            <ta e="T907" id="Seg_5893" s="T903">PKZ_196X_SU0203.PKZ.297 (305)</ta>
            <ta e="T908" id="Seg_5894" s="T907">PKZ_196X_SU0203.PKZ.298 (306)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T19" id="Seg_5895" s="T13">(Dʼaziraʔ), tüjö dʼăbaktərzittə nada. </ta>
            <ta e="T22" id="Seg_5896" s="T19">Dĭgəttə talerdə kalam. </ta>
            <ta e="T24" id="Seg_5897" s="T22">Dʼü talerzittə. </ta>
            <ta e="T27" id="Seg_5898" s="T24">Dĭgəttə ipek kuʔsittə. </ta>
            <ta e="T32" id="Seg_5899" s="T27">Dĭgəttə özerləj, urgo özerləj bar. </ta>
            <ta e="T34" id="Seg_5900" s="T32">Sĭre moləj. </ta>
            <ta e="T37" id="Seg_5901" s="T34">Dĭgəttə püzittə nada. </ta>
            <ta e="T38" id="Seg_5902" s="T37">(Pag-). </ta>
            <ta e="T39" id="Seg_5903" s="T38">((BRK)). </ta>
            <ta e="T42" id="Seg_5904" s="T39">Dĭgəttə toʔnarzittə nada. </ta>
            <ta e="T45" id="Seg_5905" s="T42">Dĭgəttə băranə kămnasʼtə. </ta>
            <ta e="T48" id="Seg_5906" s="T45">Dĭgəttə nada nʼeʔsittə. </ta>
            <ta e="T51" id="Seg_5907" s="T48">Dĭgəttə nada ilgərzittə. </ta>
            <ta e="T54" id="Seg_5908" s="T51">Dĭgəttə nuldəsʼtə nada. </ta>
            <ta e="T57" id="Seg_5909" s="T54">Dĭgəttə pürzittə nada. </ta>
            <ta e="T60" id="Seg_5910" s="T57">Dĭgəttə amzittə nada. </ta>
            <ta e="T61" id="Seg_5911" s="T60">Bar! </ta>
            <ta e="T62" id="Seg_5912" s="T61">((BRK)). </ta>
            <ta e="T65" id="Seg_5913" s="T62">(Tăn=) Tăn kandlaʔbəl. </ta>
            <ta e="T69" id="Seg_5914" s="T65">Ĭmbi măna ej nörbəbiel? </ta>
            <ta e="T74" id="Seg_5915" s="T69">Măn bɨ noʔ (iʔ-) mĭbiem. </ta>
            <ta e="T78" id="Seg_5916" s="T74">Dĭn măn tugazaŋbə ige. </ta>
            <ta e="T82" id="Seg_5917" s="T78">Tăn bɨ kubiol dĭm. </ta>
            <ta e="T83" id="Seg_5918" s="T82">Kabarləj. </ta>
            <ta e="T84" id="Seg_5919" s="T83">((BRK)). </ta>
            <ta e="T93" id="Seg_5920" s="T84">Tăn ĭmbi măna ej (nörbəliel-) nörbəbiel, što kandəgal Krasnojarskə? </ta>
            <ta e="T97" id="Seg_5921" s="T93">Dĭn măn tuganbə ige. </ta>
            <ta e="T102" id="Seg_5922" s="T97">Măn bɨ noʔ tănan mĭbiem. </ta>
            <ta e="T105" id="Seg_5923" s="T102">Dĭn sĭjdə ĭzembie. </ta>
            <ta e="T110" id="Seg_5924" s="T105">(Dĭ bɨ-) Dĭ bɨ bĭʔpi. </ta>
            <ta e="T114" id="Seg_5925" s="T110">Tăn bɨ kubiol dĭm. </ta>
            <ta e="T115" id="Seg_5926" s="T114">Kabarləj. </ta>
            <ta e="T116" id="Seg_5927" s="T115">((BRK)). </ta>
            <ta e="T119" id="Seg_5928" s="T116">Amnaʔ toltanoʔ amzittə. </ta>
            <ta e="T122" id="Seg_5929" s="T119">A girgit toltanoʔ? </ta>
            <ta e="T125" id="Seg_5930" s="T122">Da măn kĭškəbiem. </ta>
            <ta e="T128" id="Seg_5931" s="T125">Dĭgəttə un ibi. </ta>
            <ta e="T135" id="Seg_5932" s="T128">Măn dĭgəttə sĭreʔpne embiem i keʔbde embiem. </ta>
            <ta e="T139" id="Seg_5933" s="T135">Dʼibige (ku-) bü kămnabiam. </ta>
            <ta e="T141" id="Seg_5934" s="T139">Dĭgəttə mĭnzərbiem. </ta>
            <ta e="T143" id="Seg_5935" s="T141">Amnaʔ, amoraʔ! </ta>
            <ta e="T144" id="Seg_5936" s="T143">((BRK)). </ta>
            <ta e="T146" id="Seg_5937" s="T144">Nʼi naga. </ta>
            <ta e="T148" id="Seg_5938" s="T146">Gibərdə kalla dʼürbi? </ta>
            <ta e="T149" id="Seg_5939" s="T148">(Nad-). </ta>
            <ta e="T151" id="Seg_5940" s="T149">Măndərzittə nada. </ta>
            <ta e="T154" id="Seg_5941" s="T151">Možet šindi-nʼibudʼ kuʔpi? </ta>
            <ta e="T157" id="Seg_5942" s="T154">Možet urgaːba ambi. </ta>
            <ta e="T160" id="Seg_5943" s="T157">Ajiriom bar ugaːndə. </ta>
            <ta e="T161" id="Seg_5944" s="T160">Kabarləj. </ta>
            <ta e="T162" id="Seg_5945" s="T161">((BRK)). </ta>
            <ta e="T170" id="Seg_5946" s="T162">Oʔb, šide, nagur, teʔtə, sumna, sejʔpü, šinteʔtə, bʼeʔ. </ta>
            <ta e="T172" id="Seg_5947" s="T170">Сбилась я. </ta>
            <ta e="T173" id="Seg_5948" s="T172">((BRK)). </ta>
            <ta e="T181" id="Seg_5949" s="T173">Kăbɨ bostə (i-) inezeŋdə ibiʔi, dĭgəttə tajirzittə možnă. </ta>
            <ta e="T184" id="Seg_5950" s="T181">I toltanoʔ amzittə. </ta>
            <ta e="T188" id="Seg_5951" s="T184">A to bostə ineʔi naga. </ta>
            <ta e="T189" id="Seg_5952" s="T188">Bar. </ta>
            <ta e="T190" id="Seg_5953" s="T189">((BRK)). </ta>
            <ta e="T194" id="Seg_5954" s="T190">Măn meim bar jezerik. </ta>
            <ta e="T196" id="Seg_5955" s="T194">Ara bĭʔpi. </ta>
            <ta e="T201" id="Seg_5956" s="T196">Šobi bar, kudonzəbiʔi bostə tibizʼiʔ. </ta>
            <ta e="T203" id="Seg_5957" s="T201">Dʼabrobiʔi bar. </ta>
            <ta e="T206" id="Seg_5958" s="T203">Ugaːndə dĭ kurojok. </ta>
            <ta e="T209" id="Seg_5959" s="T206">Vsʼakă (kudonzə-) kudonzlaʔpi. </ta>
            <ta e="T212" id="Seg_5960" s="T209">Kăde dĭʔnə kereʔ. </ta>
            <ta e="T216" id="Seg_5961" s="T212">Bazoʔ kalla dʼürbi ara bĭʔsittə. </ta>
            <ta e="T219" id="Seg_5962" s="T216">A to dĭʔnə amga. </ta>
            <ta e="T220" id="Seg_5963" s="T219">Kabarləj. </ta>
            <ta e="T221" id="Seg_5964" s="T220">((BRK)). </ta>
            <ta e="T224" id="Seg_5965" s="T221">Šiʔ dʼijegən ibileʔ. </ta>
            <ta e="T226" id="Seg_5966" s="T224">Gijen kunolbilaʔ? </ta>
            <ta e="T228" id="Seg_5967" s="T226">Kunolbibaʔ pălatkagən. </ta>
            <ta e="T230" id="Seg_5968" s="T228">Urgaːba šobi. </ta>
            <ta e="T233" id="Seg_5969" s="T230">Bar mĭmbi dön. </ta>
            <ta e="T237" id="Seg_5970" s="T233">(M-) Miʔ ugaːndə pimbibeʔ. </ta>
            <ta e="T241" id="Seg_5971" s="T237">Dĭ tože pimnie, multukdə. </ta>
            <ta e="T248" id="Seg_5972" s="T241">(E- ej-) Ejem nada surarzittə (iʔbəzittə), (kunolzittə). </ta>
            <ta e="T249" id="Seg_5973" s="T248">Kabarləj. </ta>
            <ta e="T250" id="Seg_5974" s="T249">((BRK)). </ta>
            <ta e="T254" id="Seg_5975" s="T250">Курочка lunʼkam bar monoʔkolaʔbə. </ta>
            <ta e="T256" id="Seg_5976" s="T254">Bar pünörleʔbə. </ta>
            <ta e="T258" id="Seg_5977" s="T256">Bar münörleʔbə. </ta>
            <ta e="T260" id="Seg_5978" s="T258">Bar (burerleʔbə). </ta>
            <ta e="T262" id="Seg_5979" s="T260">Daška bar. </ta>
            <ta e="T263" id="Seg_5980" s="T262">((BRK)). </ta>
            <ta e="T266" id="Seg_5981" s="T263">(Ĭmbej-) Ĭmbi pimniel? </ta>
            <ta e="T268" id="Seg_5982" s="T266">Kanaʔ unnʼa! </ta>
            <ta e="T275" id="Seg_5983" s="T268">Măn unnʼa mĭmbiem, šindinədə (ej-) ej pimniem. </ta>
            <ta e="T279" id="Seg_5984" s="T275">A tăn üge pimneʔbəl. </ta>
            <ta e="T280" id="Seg_5985" s="T279">Nöməlleʔpiem. </ta>
            <ta e="T281" id="Seg_5986" s="T280">((laughter)) Хватит. </ta>
            <ta e="T282" id="Seg_5987" s="T281">((DMG)). </ta>
            <ta e="T286" id="Seg_5988" s="T282">Dĭzeŋ ej tĭmneʔi dʼăbaktərzittə. </ta>
            <ta e="T290" id="Seg_5989" s="T286">A măn bar dʼăbaktərlaʔbəm. </ta>
            <ta e="T292" id="Seg_5990" s="T290">Iam surarbiam. </ta>
            <ta e="T295" id="Seg_5991" s="T292">Dʼăbaktərzittə axota măna. </ta>
            <ta e="T296" id="Seg_5992" s="T295">Bar. </ta>
            <ta e="T297" id="Seg_5993" s="T296">((BRK)). </ta>
            <ta e="T302" id="Seg_5994" s="T297">(Dĭn=) Dön bar dʼirəgəʔi naga. </ta>
            <ta e="T307" id="Seg_5995" s="T302">(A onʼ-) Onʼiʔ nuzaŋ amnolaʔbəʔjə. </ta>
            <ta e="T311" id="Seg_5996" s="T307">Dĭzeŋ bar dʼirəgəʔi pimnieʔjə. </ta>
            <ta e="T315" id="Seg_5997" s="T311">Bar (em-) ej dʼăbaktərlam. </ta>
            <ta e="T318" id="Seg_5998" s="T315">Tüjö băra detlem. </ta>
            <ta e="T320" id="Seg_5999" s="T318">Tăn dʼabolal. </ta>
            <ta e="T323" id="Seg_6000" s="T320">Măn toltanoʔ kămnabiam. </ta>
            <ta e="T324" id="Seg_6001" s="T323">Amnogaʔ! </ta>
            <ta e="T329" id="Seg_6002" s="T324">Măn iššo šide aspaʔ detlem. </ta>
            <ta e="T330" id="Seg_6003" s="T329">Păʔlam. </ta>
            <ta e="T333" id="Seg_6004" s="T330">Dĭgəttə dʼăbaktərzittə možna. </ta>
            <ta e="T336" id="Seg_6005" s="T333">Bar, ej dʼăbaktəriam. </ta>
            <ta e="T337" id="Seg_6006" s="T336">((BRK)). </ta>
            <ta e="T341" id="Seg_6007" s="T337">Teinen ugaːndə dʼibige dʼala. </ta>
            <ta e="T344" id="Seg_6008" s="T341">Bar ipek nendləj. </ta>
            <ta e="T348" id="Seg_6009" s="T344">(Ej-) ipek ej özerləj. </ta>
            <ta e="T351" id="Seg_6010" s="T348">Dĭgəttə naga ipek. </ta>
            <ta e="T353" id="Seg_6011" s="T351">Ĭmbi amzittə. </ta>
            <ta e="T359" id="Seg_6012" s="T353">Külaːmbiam, (ku-) küzittə možnă, ipekziʔ naga. </ta>
            <ta e="T363" id="Seg_6013" s="T359">Bar ibə, ej dʼăbaktərlam. </ta>
            <ta e="T364" id="Seg_6014" s="T363">((BRK)). </ta>
            <ta e="T367" id="Seg_6015" s="T364">Tăn ugaːndə sagər. </ta>
            <ta e="T368" id="Seg_6016" s="T367">Bazəjdaʔ! </ta>
            <ta e="T370" id="Seg_6017" s="T368">Multʼanə kanaʔ! </ta>
            <ta e="T372" id="Seg_6018" s="T370">Sabən iʔ. </ta>
            <ta e="T375" id="Seg_6019" s="T372">Dĭgəttə kujnek iʔ! </ta>
            <ta e="T376" id="Seg_6020" s="T375">Šerdə! </ta>
            <ta e="T377" id="Seg_6021" s="T376">Kabarləj. </ta>
            <ta e="T378" id="Seg_6022" s="T377">((BRK)). </ta>
            <ta e="T382" id="Seg_6023" s="T378">Ugaːndə kuvas dĭgəttə molal! </ta>
            <ta e="T385" id="Seg_6024" s="T382">Kujnek šeriel dăk. </ta>
            <ta e="T392" id="Seg_6025" s="T385">Bar koʔbsaŋ tănan (могут -) (pănarləʔi) bar. </ta>
            <ta e="T393" id="Seg_6026" s="T392">Kuvas. </ta>
            <ta e="T394" id="Seg_6027" s="T393">Kabarləj. </ta>
            <ta e="T395" id="Seg_6028" s="T394">((BRK)). </ta>
            <ta e="T398" id="Seg_6029" s="T395">Dĭzeŋ Krasnojarskəʔi kambiʔi. </ta>
            <ta e="T401" id="Seg_6030" s="T398">Măna ej nörbəʔi. </ta>
            <ta e="T406" id="Seg_6031" s="T401">Măn măn dĭn tuganbə ige. </ta>
            <ta e="T410" id="Seg_6032" s="T406">Măn bɨ noʔ mĭbiem. </ta>
            <ta e="T414" id="Seg_6033" s="T410">(Dĭn=) Dĭn sĭjdə ĭzemnie. </ta>
            <ta e="T420" id="Seg_6034" s="T414">Dĭzeŋ bar kubiʔi bɨ dĭm (dĭ-). </ta>
            <ta e="T424" id="Seg_6035" s="T420">Bar, daška ej dʼăbaktərlam. </ta>
            <ta e="T425" id="Seg_6036" s="T424">((BRK)). </ta>
            <ta e="T428" id="Seg_6037" s="T425">Sagər ne šobi. </ta>
            <ta e="T433" id="Seg_6038" s="T428">Sĭre kuza măndə: izittə nada. </ta>
            <ta e="T435" id="Seg_6039" s="T433">Kanžəbəj obberəj. </ta>
            <ta e="T440" id="Seg_6040" s="T435">Aktʼigən bar (kunol-) kunolžəbəj bar. </ta>
            <ta e="T442" id="Seg_6041" s="T440">Panaržəbəj bar. </ta>
            <ta e="T446" id="Seg_6042" s="T442">Tibil bar münörləj tănan. </ta>
            <ta e="T447" id="Seg_6043" s="T446">Kabarləj. </ta>
            <ta e="T450" id="Seg_6044" s="T447">Daška ej dʼăbaktərlam. </ta>
            <ta e="T451" id="Seg_6045" s="T450">((BRK)). </ta>
            <ta e="T456" id="Seg_6046" s="T451">Nada măna ešši (măn) băzəjsʼtə. </ta>
            <ta e="T459" id="Seg_6047" s="T456">Dĭ ugaːndə sagər. </ta>
            <ta e="T462" id="Seg_6048" s="T459">Bar tüʔpi iʔgö. </ta>
            <ta e="T465" id="Seg_6049" s="T462">(Kĭškiem-) bar, kĭškəbiem. </ta>
            <ta e="T467" id="Seg_6050" s="T465">Dĭ kolaːmbi. </ta>
            <ta e="T471" id="Seg_6051" s="T467">Dĭgəttə nada kürzittə dĭm. </ta>
            <ta e="T476" id="Seg_6052" s="T471">Dĭgəttə (am- amzit-) amorzittə nada. </ta>
            <ta e="T479" id="Seg_6053" s="T476">Dĭgəttə (ku-) kunolzittə. </ta>
            <ta e="T481" id="Seg_6054" s="T479">Ordə eššim. </ta>
            <ta e="T484" id="Seg_6055" s="T481">Bar- ((DMG)) -gaʔ. </ta>
            <ta e="T485" id="Seg_6056" s="T484">((DMG)). </ta>
            <ta e="T488" id="Seg_6057" s="T485">Teinen urgo dʼala. </ta>
            <ta e="T492" id="Seg_6058" s="T488">A il üge togonorlaʔbəʔjə. </ta>
            <ta e="T494" id="Seg_6059" s="T492">Toltanoʔ amlaʔbəʔjə. </ta>
            <ta e="T495" id="Seg_6060" s="T494">Tajirlaʔbəʔjə. </ta>
            <ta e="T498" id="Seg_6061" s="T495">A miʔ amnolaʔbəbaʔ. </ta>
            <ta e="T504" id="Seg_6062" s="T498">Kabarləj, daška (ej=) dʼăbaktərzittə nʼe axota. </ta>
            <ta e="T505" id="Seg_6063" s="T504">((BRK)). </ta>
            <ta e="T508" id="Seg_6064" s="T505">Teinen urgo dʼala. </ta>
            <ta e="T511" id="Seg_6065" s="T508">Măna tuganbə kăštəbiʔi. </ta>
            <ta e="T512" id="Seg_6066" s="T511">Kanžəbəj. </ta>
            <ta e="T515" id="Seg_6067" s="T512">Dĭn ara bĭtləbəj. </ta>
            <ta e="T518" id="Seg_6068" s="T515">Dĭn sʼarlaʔbəʔjə garmonʼənə. </ta>
            <ta e="T522" id="Seg_6069" s="T518">(M-) Miʔ bar suʔməleʔbəbeʔ. </ta>
            <ta e="T523" id="Seg_6070" s="T522">Bar. </ta>
            <ta e="T524" id="Seg_6071" s="T523">((BRK)). </ta>
            <ta e="T531" id="Seg_6072" s="T524">Oʔb, oʔb, šide, nagur, teʔtə, sumna, sejʔpü. </ta>
            <ta e="T533" id="Seg_6073" s="T531">Amitun, šinteʔtə. </ta>
            <ta e="T541" id="Seg_6074" s="T540">Bʼeʔ. </ta>
            <ta e="T550" id="Seg_6075" s="T542">Bʼeʔ onʼiʔ, bʼeʔ šide, bʼeʔ sumna, bʼeʔ muktuʔ. </ta>
            <ta e="T551" id="Seg_6076" s="T550">((BRK)). </ta>
            <ta e="T559" id="Seg_6077" s="T551">Măn unnʼa šobiam, a dĭzeŋ bar sumna šobiʔi. </ta>
            <ta e="T560" id="Seg_6078" s="T559">((BRK)). </ta>
            <ta e="T564" id="Seg_6079" s="T560">Măn mĭj (mĭr-) mĭnzəriem. </ta>
            <ta e="T567" id="Seg_6080" s="T564">Ugaːndə jakšə mĭj. </ta>
            <ta e="T569" id="Seg_6081" s="T567">Nʼamga, ujazʼiʔ. </ta>
            <ta e="T572" id="Seg_6082" s="T569">Köbergən embiem bar. </ta>
            <ta e="T574" id="Seg_6083" s="T572">Tus embiem. </ta>
            <ta e="T576" id="Seg_6084" s="T574">Toltanoʔ ambiem. </ta>
            <ta e="T578" id="Seg_6085" s="T576">Amnaʔ, amoraʔ! </ta>
            <ta e="T579" id="Seg_6086" s="T578">Obberəj. </ta>
            <ta e="T580" id="Seg_6087" s="T579">Kabarləj. </ta>
            <ta e="T581" id="Seg_6088" s="T580">((BRK)). </ta>
            <ta e="T584" id="Seg_6089" s="T581">Ugaːndə kuštü kuza. </ta>
            <ta e="T587" id="Seg_6090" s="T584">Măn dĭʔnə pimniem. </ta>
            <ta e="T590" id="Seg_6091" s="T587">Dĭ bar kutləj. </ta>
            <ta e="T597" id="Seg_6092" s="T590">Măn šaʔlaːmbiam štobɨ dĭ măna ej kubi. </ta>
            <ta e="T600" id="Seg_6093" s="T597">Măn dʼijenə nuʔməluʔləm. </ta>
            <ta e="T606" id="Seg_6094" s="T600">Dĭ măna (ej=) dĭn ej kuləj. </ta>
            <ta e="T607" id="Seg_6095" s="T606">Kabarləj. </ta>
            <ta e="T608" id="Seg_6096" s="T607">((BRK)). </ta>
            <ta e="T611" id="Seg_6097" s="T608">Tibizeŋ iʔgö oʔbdəbiʔi. </ta>
            <ta e="T620" id="Seg_6098" s="T611">Ĭmbidə bar dʼăbaktərlaʔbəʔjə, nada kanzittə nʼilgösʼtə (ĭm-) ĭmbi dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T625" id="Seg_6099" s="T620">Măn dĭgəttə (tĭmnə- tĭmzit-) tĭmlem. </ta>
            <ta e="T629" id="Seg_6100" s="T625">Bar, daška ej dʼăbaktərliam. </ta>
            <ta e="T638" id="Seg_6101" s="T634">Dĭ kuza bar jezerik. </ta>
            <ta e="T640" id="Seg_6102" s="T638">Inegən amnoluʔpi. </ta>
            <ta e="T643" id="Seg_6103" s="T640">Dĭgəttə üzəbi dʼünə. </ta>
            <ta e="T646" id="Seg_6104" s="T643">Dĭgəttə inet nuʔməluʔpi. </ta>
            <ta e="T651" id="Seg_6105" s="T646">(Üžü, üžü, üžüd-) Üžü dʼürdəbi. </ta>
            <ta e="T653" id="Seg_6106" s="T651">Maluʔpi bar. </ta>
            <ta e="T657" id="Seg_6107" s="T653">(Kunollaʔbə=) Kunollaʔ iʔbolaʔbə, saʔməluʔpi. </ta>
            <ta e="T659" id="Seg_6108" s="T657">Bar, kabarləj. </ta>
            <ta e="T660" id="Seg_6109" s="T659">((BRK)). </ta>
            <ta e="T667" id="Seg_6110" s="T660">Nʼizeŋ, koʔbsaŋ bar iʔgö oʔbdəbiʔi bar, nüjleʔbəʔjə. </ta>
            <ta e="T670" id="Seg_6111" s="T667">Garmonʼə sʼarlaʔbəʔjə, suʔmileʔbəʔjə. </ta>
            <ta e="T671" id="Seg_6112" s="T670">Kabarləj. </ta>
            <ta e="T672" id="Seg_6113" s="T671">((BRK)). </ta>
            <ta e="T675" id="Seg_6114" s="T672">Măn dʼijenə mĭmbiem. </ta>
            <ta e="T677" id="Seg_6115" s="T675">Keʔbde nĭŋgəbiem. </ta>
            <ta e="T679" id="Seg_6116" s="T677">Kălba nĭŋgəbiem. </ta>
            <ta e="T683" id="Seg_6117" s="T679">Pirogəʔi pürbiem, šiʔnʼileʔ mĭlem. </ta>
            <ta e="T684" id="Seg_6118" s="T683">Kabarləj. </ta>
            <ta e="T685" id="Seg_6119" s="T684">((BRK)). </ta>
            <ta e="T692" id="Seg_6120" s="T685">Măn iam nu ibi, abam dʼirək ibi. </ta>
            <ta e="T695" id="Seg_6121" s="T692">Šide nʼi ibiʔi. </ta>
            <ta e="T701" id="Seg_6122" s="T698">Muktuʔ koʔbdaŋ ibi. </ta>
            <ta e="T703" id="Seg_6123" s="T701">Bar, kabarləj. </ta>
            <ta e="T704" id="Seg_6124" s="T703">((BRK)). </ta>
            <ta e="T710" id="Seg_6125" s="T704">Nagur külaːmbiʔi, a nagur ej kübiʔi. </ta>
            <ta e="T714" id="Seg_6126" s="T710">Măn abam albuga … </ta>
            <ta e="T722" id="Seg_6127" s="T719">Bʼeʔ sumna kuʔpi. </ta>
            <ta e="T724" id="Seg_6128" s="T722">Bar. </ta>
            <ta e="T725" id="Seg_6129" s="T724">Kabarləj. </ta>
            <ta e="T729" id="Seg_6130" s="T725">Tažəp išo băra deʔpi. </ta>
            <ta e="T730" id="Seg_6131" s="T729">Kabarləj. </ta>
            <ta e="T733" id="Seg_6132" s="T730">Nu kuza ĭzemnie. </ta>
            <ta e="T734" id="Seg_6133" s="T733">((BRK)). </ta>
            <ta e="T742" id="Seg_6134" s="T734">Dĭgəttə kambi šamandə, dĭ (dĭ-) dĭm bar dʼazirlaʔpi. </ta>
            <ta e="T745" id="Seg_6135" s="T742">Daška bar naverna. </ta>
            <ta e="T748" id="Seg_6136" s="T745">Măn ugaːndə püjöliam. </ta>
            <ta e="T754" id="Seg_6137" s="T748">Amnaʔ, amoraʔ, šindi tănan ej mĭlie. </ta>
            <ta e="T760" id="Seg_6138" s="T754">Dĭgəttə (a- a-) iʔbeʔ da kunolaʔ! </ta>
            <ta e="T761" id="Seg_6139" s="T760">Kabarləj. </ta>
            <ta e="T762" id="Seg_6140" s="T761">((BRK)). </ta>
            <ta e="T767" id="Seg_6141" s="T762">Esseŋ nʼiʔnen bar (sʼ-) sʼarlaʔbəʔjə. </ta>
            <ta e="T769" id="Seg_6142" s="T767">Kirgarlaʔbəʔjə, dʼabrolaʔbəʔjə. </ta>
            <ta e="T771" id="Seg_6143" s="T769">Na što dʼabrolial? </ta>
            <ta e="T774" id="Seg_6144" s="T771">Jakšə nada sʼarzittə. </ta>
            <ta e="T776" id="Seg_6145" s="T774">Iʔ dʼabrosʼtə! </ta>
            <ta e="T778" id="Seg_6146" s="T776">Kabarləj naverna. </ta>
            <ta e="T779" id="Seg_6147" s="T778">((BRK)). </ta>
            <ta e="T781" id="Seg_6148" s="T779">Iʔ dʼabrogaʔ! </ta>
            <ta e="T784" id="Seg_6149" s="T781">Nada jakšə sʼarzittə. </ta>
            <ta e="T786" id="Seg_6150" s="T784">Na što dʼabrolaʔbəl? </ta>
            <ta e="T787" id="Seg_6151" s="T786">((BRK)). </ta>
            <ta e="T790" id="Seg_6152" s="T787">Măn teinen nubiam. </ta>
            <ta e="T796" id="Seg_6153" s="T790">Kuja iššo ej nubi măn nubiam. </ta>
            <ta e="T799" id="Seg_6154" s="T796">Dĭgəttə tüžöj surdəbiam. </ta>
            <ta e="T801" id="Seg_6155" s="T799">Büzəj bĭtəlbiem. </ta>
            <ta e="T806" id="Seg_6156" s="T801">Kurizəʔi (băd- bădl- bă-) bădlaʔbəm. </ta>
            <ta e="T809" id="Seg_6157" s="T806">Dĭgəttə munəjʔ pürbiem. </ta>
            <ta e="T811" id="Seg_6158" s="T809">Toltanoʔ mĭnzərbiem. </ta>
            <ta e="T812" id="Seg_6159" s="T811">Amorbiam. </ta>
            <ta e="T819" id="Seg_6160" s="T812">Dĭgəttə (nu kuza=) nu il šobiʔi, dʼăbaktərbiam. </ta>
            <ta e="T822" id="Seg_6161" s="T819">(Bal-) Bar tararluʔpiam. </ta>
            <ta e="T823" id="Seg_6162" s="T822">Kabarləj. </ta>
            <ta e="T824" id="Seg_6163" s="T823">((BRK)). </ta>
            <ta e="T826" id="Seg_6164" s="T824">Kandəgaʔ maʔnəl! </ta>
            <ta e="T828" id="Seg_6165" s="T826">Kabarləj dʼăbaktərzittə. </ta>
            <ta e="T829" id="Seg_6166" s="T828">Amgaʔ! </ta>
            <ta e="T831" id="Seg_6167" s="T829">Iʔbeʔ kunolzittə! </ta>
            <ta e="T833" id="Seg_6168" s="T831">Kabarləj daška. </ta>
            <ta e="T834" id="Seg_6169" s="T833">((…)).</ta>
            <ta e="T836" id="Seg_6170" s="T834">Šumuranə mĭmbiem. </ta>
            <ta e="T840" id="Seg_6171" s="T836">Ugaːndə il iʔgö kubiam. </ta>
            <ta e="T843" id="Seg_6172" s="T840">Koŋ bar šobiʔi. </ta>
            <ta e="T847" id="Seg_6173" s="T843">Tibizeŋ dĭn iʔgö, inezeŋ. </ta>
            <ta e="T850" id="Seg_6174" s="T847">Koʔbsaŋ, nʼizeŋ bar. </ta>
            <ta e="T851" id="Seg_6175" s="T850">Kabarləj. </ta>
            <ta e="T852" id="Seg_6176" s="T851">((BRK)). </ta>
            <ta e="T861" id="Seg_6177" s="T852">Ĭmbi dĭ todam kuza ej šobi, koʔbdo ej šobi. </ta>
            <ta e="T863" id="Seg_6178" s="T861">Gijen dĭzeŋ? </ta>
            <ta e="T868" id="Seg_6179" s="T863">Ĭmbi ej šobiʔi dʼăbaktərzittə mănzʼiʔ? </ta>
            <ta e="T869" id="Seg_6180" s="T868">Kabarləj. </ta>
            <ta e="T870" id="Seg_6181" s="T869">((BRK)). </ta>
            <ta e="T872" id="Seg_6182" s="T870">Gijen dĭzeŋ? </ta>
            <ta e="T876" id="Seg_6183" s="T872">Ĭmbi ej šobiʔi dʼăbaktərzittə? </ta>
            <ta e="T877" id="Seg_6184" s="T876">((BRK)). </ta>
            <ta e="T880" id="Seg_6185" s="T877">Teinen kabarləj dʼăbaktərzittə. </ta>
            <ta e="T882" id="Seg_6186" s="T880">Kanaʔ maʔnəl! </ta>
            <ta e="T890" id="Seg_6187" s="T882">Tăn măna (dĭ-) dăre ej (šü- šü- )… </ta>
            <ta e="T896" id="Seg_6188" s="T890">Tăn măna dăre ej (š-) tüšəlleʔ! </ta>
            <ta e="T900" id="Seg_6189" s="T896">Dăre ej jakšə mănzittə. </ta>
            <ta e="T903" id="Seg_6190" s="T900">Na što sürerzittə il. </ta>
            <ta e="T907" id="Seg_6191" s="T903">Dĭ nuzaŋ il ведь. </ta>
            <ta e="T908" id="Seg_6192" s="T907">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T16" id="Seg_6193" s="T13">dʼazir-a-ʔ</ta>
            <ta e="T17" id="Seg_6194" s="T16">tüjö</ta>
            <ta e="T18" id="Seg_6195" s="T17">dʼăbaktər-zittə</ta>
            <ta e="T19" id="Seg_6196" s="T18">nada</ta>
            <ta e="T20" id="Seg_6197" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_6198" s="T20">taler-də</ta>
            <ta e="T22" id="Seg_6199" s="T21">ka-la-m</ta>
            <ta e="T23" id="Seg_6200" s="T22">dʼü</ta>
            <ta e="T24" id="Seg_6201" s="T23">taler-zittə</ta>
            <ta e="T25" id="Seg_6202" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_6203" s="T25">ipek</ta>
            <ta e="T27" id="Seg_6204" s="T26">kuʔ-sittə</ta>
            <ta e="T28" id="Seg_6205" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_6206" s="T28">özer-lə-j</ta>
            <ta e="T30" id="Seg_6207" s="T29">urgo</ta>
            <ta e="T31" id="Seg_6208" s="T30">özer-lə-j</ta>
            <ta e="T32" id="Seg_6209" s="T31">bar</ta>
            <ta e="T33" id="Seg_6210" s="T32">sĭre</ta>
            <ta e="T34" id="Seg_6211" s="T33">mo-lə-j</ta>
            <ta e="T35" id="Seg_6212" s="T34">dĭgəttə</ta>
            <ta e="T36" id="Seg_6213" s="T35">pü-zittə</ta>
            <ta e="T37" id="Seg_6214" s="T36">nada</ta>
            <ta e="T40" id="Seg_6215" s="T39">dĭgəttə</ta>
            <ta e="T41" id="Seg_6216" s="T40">toʔ-nar-zittə</ta>
            <ta e="T42" id="Seg_6217" s="T41">nada</ta>
            <ta e="T43" id="Seg_6218" s="T42">dĭgəttə</ta>
            <ta e="T44" id="Seg_6219" s="T43">băra-nə</ta>
            <ta e="T45" id="Seg_6220" s="T44">kămna-sʼtə</ta>
            <ta e="T46" id="Seg_6221" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_6222" s="T46">nada</ta>
            <ta e="T48" id="Seg_6223" s="T47">nʼeʔ-sittə</ta>
            <ta e="T49" id="Seg_6224" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_6225" s="T49">nada</ta>
            <ta e="T51" id="Seg_6226" s="T50">ilgər-zittə</ta>
            <ta e="T52" id="Seg_6227" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_6228" s="T52">nuldə-sʼtə</ta>
            <ta e="T54" id="Seg_6229" s="T53">nada</ta>
            <ta e="T55" id="Seg_6230" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_6231" s="T55">pür-zittə</ta>
            <ta e="T57" id="Seg_6232" s="T56">nada</ta>
            <ta e="T58" id="Seg_6233" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_6234" s="T58">am-zittə</ta>
            <ta e="T60" id="Seg_6235" s="T59">nada</ta>
            <ta e="T61" id="Seg_6236" s="T60">bar</ta>
            <ta e="T63" id="Seg_6237" s="T62">tăn</ta>
            <ta e="T64" id="Seg_6238" s="T63">tăn</ta>
            <ta e="T65" id="Seg_6239" s="T64">kand-laʔbə-l</ta>
            <ta e="T66" id="Seg_6240" s="T65">ĭmbi</ta>
            <ta e="T67" id="Seg_6241" s="T66">măna</ta>
            <ta e="T68" id="Seg_6242" s="T67">ej</ta>
            <ta e="T69" id="Seg_6243" s="T68">nörbə-bie-l</ta>
            <ta e="T70" id="Seg_6244" s="T69">măn</ta>
            <ta e="T71" id="Seg_6245" s="T70">bɨ</ta>
            <ta e="T72" id="Seg_6246" s="T71">noʔ</ta>
            <ta e="T74" id="Seg_6247" s="T73">mĭ-bie-m</ta>
            <ta e="T75" id="Seg_6248" s="T74">dĭn</ta>
            <ta e="T76" id="Seg_6249" s="T75">măn</ta>
            <ta e="T77" id="Seg_6250" s="T76">tuga-zaŋ-bə</ta>
            <ta e="T78" id="Seg_6251" s="T77">i-ge</ta>
            <ta e="T79" id="Seg_6252" s="T78">tăn</ta>
            <ta e="T80" id="Seg_6253" s="T79">bɨ</ta>
            <ta e="T81" id="Seg_6254" s="T80">ku-bio-l</ta>
            <ta e="T82" id="Seg_6255" s="T81">dĭ-m</ta>
            <ta e="T83" id="Seg_6256" s="T82">kabarləj</ta>
            <ta e="T85" id="Seg_6257" s="T84">tăn</ta>
            <ta e="T86" id="Seg_6258" s="T85">ĭmbi</ta>
            <ta e="T87" id="Seg_6259" s="T86">măna</ta>
            <ta e="T88" id="Seg_6260" s="T87">ej</ta>
            <ta e="T90" id="Seg_6261" s="T89">nörbə-bie-l</ta>
            <ta e="T91" id="Seg_6262" s="T90">što</ta>
            <ta e="T92" id="Seg_6263" s="T91">kandə-ga-l</ta>
            <ta e="T93" id="Seg_6264" s="T92">Krasnojarskə</ta>
            <ta e="T94" id="Seg_6265" s="T93">dĭn</ta>
            <ta e="T95" id="Seg_6266" s="T94">măn</ta>
            <ta e="T96" id="Seg_6267" s="T95">tugan-bə</ta>
            <ta e="T97" id="Seg_6268" s="T96">i-ge</ta>
            <ta e="T98" id="Seg_6269" s="T97">măn</ta>
            <ta e="T99" id="Seg_6270" s="T98">bɨ</ta>
            <ta e="T100" id="Seg_6271" s="T99">noʔ</ta>
            <ta e="T101" id="Seg_6272" s="T100">tănan</ta>
            <ta e="T102" id="Seg_6273" s="T101">mĭ-bie-m</ta>
            <ta e="T103" id="Seg_6274" s="T102">dĭ-n</ta>
            <ta e="T104" id="Seg_6275" s="T103">sĭj-də</ta>
            <ta e="T105" id="Seg_6276" s="T104">ĭzem-bie</ta>
            <ta e="T106" id="Seg_6277" s="T105">dĭ</ta>
            <ta e="T108" id="Seg_6278" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_6279" s="T108">bɨ</ta>
            <ta e="T110" id="Seg_6280" s="T109">bĭʔ-pi</ta>
            <ta e="T111" id="Seg_6281" s="T110">tăn</ta>
            <ta e="T112" id="Seg_6282" s="T111">bɨ</ta>
            <ta e="T113" id="Seg_6283" s="T112">ku-bio-l</ta>
            <ta e="T114" id="Seg_6284" s="T113">dĭ-m</ta>
            <ta e="T115" id="Seg_6285" s="T114">kabarləj</ta>
            <ta e="T117" id="Seg_6286" s="T116">amna-ʔ</ta>
            <ta e="T118" id="Seg_6287" s="T117">toltanoʔ</ta>
            <ta e="T119" id="Seg_6288" s="T118">am-zittə</ta>
            <ta e="T120" id="Seg_6289" s="T119">a</ta>
            <ta e="T121" id="Seg_6290" s="T120">girgit</ta>
            <ta e="T122" id="Seg_6291" s="T121">toltanoʔ</ta>
            <ta e="T123" id="Seg_6292" s="T122">da</ta>
            <ta e="T124" id="Seg_6293" s="T123">măn</ta>
            <ta e="T125" id="Seg_6294" s="T124">kĭškə-bie-m</ta>
            <ta e="T126" id="Seg_6295" s="T125">dĭgəttə</ta>
            <ta e="T127" id="Seg_6296" s="T126">un</ta>
            <ta e="T128" id="Seg_6297" s="T127">i-bi</ta>
            <ta e="T129" id="Seg_6298" s="T128">măn</ta>
            <ta e="T130" id="Seg_6299" s="T129">dĭgəttə</ta>
            <ta e="T131" id="Seg_6300" s="T130">sĭreʔpne</ta>
            <ta e="T132" id="Seg_6301" s="T131">em-bie-m</ta>
            <ta e="T133" id="Seg_6302" s="T132">i</ta>
            <ta e="T134" id="Seg_6303" s="T133">keʔbde</ta>
            <ta e="T135" id="Seg_6304" s="T134">em-bie-m</ta>
            <ta e="T136" id="Seg_6305" s="T135">dʼibige</ta>
            <ta e="T138" id="Seg_6306" s="T137">bü</ta>
            <ta e="T139" id="Seg_6307" s="T138">kămna-bia-m</ta>
            <ta e="T140" id="Seg_6308" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_6309" s="T140">mĭnzər-bie-m</ta>
            <ta e="T142" id="Seg_6310" s="T141">amna-ʔ</ta>
            <ta e="T143" id="Seg_6311" s="T142">amor-a-ʔ</ta>
            <ta e="T145" id="Seg_6312" s="T144">nʼi</ta>
            <ta e="T146" id="Seg_6313" s="T145">naga</ta>
            <ta e="T147" id="Seg_6314" s="T146">gibər=də</ta>
            <ta e="T911" id="Seg_6315" s="T147">kal-la</ta>
            <ta e="T148" id="Seg_6316" s="T911">dʼür-bi</ta>
            <ta e="T150" id="Seg_6317" s="T149">măndə-r-zittə</ta>
            <ta e="T151" id="Seg_6318" s="T150">nada</ta>
            <ta e="T152" id="Seg_6319" s="T151">možet</ta>
            <ta e="T153" id="Seg_6320" s="T152">šindi=nʼibudʼ</ta>
            <ta e="T154" id="Seg_6321" s="T153">kuʔ-pi</ta>
            <ta e="T155" id="Seg_6322" s="T154">možet</ta>
            <ta e="T156" id="Seg_6323" s="T155">urgaːba</ta>
            <ta e="T157" id="Seg_6324" s="T156">am-bi</ta>
            <ta e="T158" id="Seg_6325" s="T157">ajir-io-m</ta>
            <ta e="T159" id="Seg_6326" s="T158">bar</ta>
            <ta e="T160" id="Seg_6327" s="T159">ugaːndə</ta>
            <ta e="T161" id="Seg_6328" s="T160">kabarləj</ta>
            <ta e="T163" id="Seg_6329" s="T162">oʔb</ta>
            <ta e="T164" id="Seg_6330" s="T163">šide</ta>
            <ta e="T165" id="Seg_6331" s="T164">nagur</ta>
            <ta e="T166" id="Seg_6332" s="T165">teʔtə</ta>
            <ta e="T167" id="Seg_6333" s="T166">sumna</ta>
            <ta e="T168" id="Seg_6334" s="T167">sejʔpü</ta>
            <ta e="T169" id="Seg_6335" s="T168">šinteʔtə</ta>
            <ta e="T170" id="Seg_6336" s="T169">bʼeʔ</ta>
            <ta e="T174" id="Seg_6337" s="T173">kăbɨ</ta>
            <ta e="T175" id="Seg_6338" s="T174">bos-tə</ta>
            <ta e="T177" id="Seg_6339" s="T176">ine-zeŋ-də</ta>
            <ta e="T178" id="Seg_6340" s="T177">i-bi-ʔi</ta>
            <ta e="T179" id="Seg_6341" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_6342" s="T179">tajir-zittə</ta>
            <ta e="T181" id="Seg_6343" s="T180">možnă</ta>
            <ta e="T182" id="Seg_6344" s="T181">i</ta>
            <ta e="T183" id="Seg_6345" s="T182">toltanoʔ</ta>
            <ta e="T184" id="Seg_6346" s="T183">am-zittə</ta>
            <ta e="T185" id="Seg_6347" s="T184">ato</ta>
            <ta e="T186" id="Seg_6348" s="T185">bos-tə</ta>
            <ta e="T187" id="Seg_6349" s="T186">ine-ʔi</ta>
            <ta e="T188" id="Seg_6350" s="T187">naga</ta>
            <ta e="T189" id="Seg_6351" s="T188">bar</ta>
            <ta e="T191" id="Seg_6352" s="T190">măn</ta>
            <ta e="T192" id="Seg_6353" s="T191">mei-m</ta>
            <ta e="T193" id="Seg_6354" s="T192">bar</ta>
            <ta e="T194" id="Seg_6355" s="T193">jezerik</ta>
            <ta e="T195" id="Seg_6356" s="T194">ara</ta>
            <ta e="T196" id="Seg_6357" s="T195">bĭʔ-pi</ta>
            <ta e="T197" id="Seg_6358" s="T196">šo-bi</ta>
            <ta e="T198" id="Seg_6359" s="T197">bar</ta>
            <ta e="T199" id="Seg_6360" s="T198">kudo-nzə-bi-ʔi</ta>
            <ta e="T200" id="Seg_6361" s="T199">bos-tə</ta>
            <ta e="T201" id="Seg_6362" s="T200">tibi-zʼiʔ</ta>
            <ta e="T202" id="Seg_6363" s="T201">dʼabro-bi-ʔi</ta>
            <ta e="T203" id="Seg_6364" s="T202">bar</ta>
            <ta e="T204" id="Seg_6365" s="T203">ugaːndə</ta>
            <ta e="T205" id="Seg_6366" s="T204">dĭ</ta>
            <ta e="T206" id="Seg_6367" s="T205">kurojok</ta>
            <ta e="T207" id="Seg_6368" s="T206">vsʼakă</ta>
            <ta e="T209" id="Seg_6369" s="T208">kudo-nz-laʔ-pi</ta>
            <ta e="T210" id="Seg_6370" s="T209">kăde</ta>
            <ta e="T211" id="Seg_6371" s="T210">dĭʔ-nə</ta>
            <ta e="T212" id="Seg_6372" s="T211">kereʔ</ta>
            <ta e="T213" id="Seg_6373" s="T212">bazoʔ</ta>
            <ta e="T912" id="Seg_6374" s="T213">kal-la</ta>
            <ta e="T214" id="Seg_6375" s="T912">dʼür-bi</ta>
            <ta e="T215" id="Seg_6376" s="T214">ara</ta>
            <ta e="T216" id="Seg_6377" s="T215">bĭʔ-sittə</ta>
            <ta e="T217" id="Seg_6378" s="T216">ato</ta>
            <ta e="T218" id="Seg_6379" s="T217">dĭʔ-nə</ta>
            <ta e="T219" id="Seg_6380" s="T218">amga</ta>
            <ta e="T220" id="Seg_6381" s="T219">kabarləj</ta>
            <ta e="T222" id="Seg_6382" s="T221">šiʔ</ta>
            <ta e="T223" id="Seg_6383" s="T222">dʼije-gən</ta>
            <ta e="T224" id="Seg_6384" s="T223">i-bi-leʔ</ta>
            <ta e="T225" id="Seg_6385" s="T224">gijen</ta>
            <ta e="T226" id="Seg_6386" s="T225">kunol-bi-laʔ</ta>
            <ta e="T227" id="Seg_6387" s="T226">kunol-bi-baʔ</ta>
            <ta e="T228" id="Seg_6388" s="T227">pălatka-gən</ta>
            <ta e="T229" id="Seg_6389" s="T228">urgaːba</ta>
            <ta e="T230" id="Seg_6390" s="T229">šo-bi</ta>
            <ta e="T231" id="Seg_6391" s="T230">bar</ta>
            <ta e="T232" id="Seg_6392" s="T231">mĭm-bi</ta>
            <ta e="T233" id="Seg_6393" s="T232">dön</ta>
            <ta e="T235" id="Seg_6394" s="T234">miʔ</ta>
            <ta e="T236" id="Seg_6395" s="T235">ugaːndə</ta>
            <ta e="T237" id="Seg_6396" s="T236">pim-bi-beʔ</ta>
            <ta e="T238" id="Seg_6397" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_6398" s="T238">tože</ta>
            <ta e="T240" id="Seg_6399" s="T239">pim-nie</ta>
            <ta e="T241" id="Seg_6400" s="T240">multuk-də</ta>
            <ta e="T244" id="Seg_6401" s="T243">eje-m</ta>
            <ta e="T245" id="Seg_6402" s="T244">nada</ta>
            <ta e="T246" id="Seg_6403" s="T245">surar-zittə</ta>
            <ta e="T247" id="Seg_6404" s="T246">iʔbə-zittə</ta>
            <ta e="T248" id="Seg_6405" s="T247">kunol-zittə</ta>
            <ta e="T249" id="Seg_6406" s="T248">kabarləj</ta>
            <ta e="T251" id="Seg_6407" s="T910">lunʼka-m</ta>
            <ta e="T252" id="Seg_6408" s="T251">bar</ta>
            <ta e="T254" id="Seg_6409" s="T252">monoʔko-laʔbə</ta>
            <ta e="T255" id="Seg_6410" s="T254">bar</ta>
            <ta e="T256" id="Seg_6411" s="T255">pünö-r-leʔbə</ta>
            <ta e="T257" id="Seg_6412" s="T256">bar</ta>
            <ta e="T258" id="Seg_6413" s="T257">münör-leʔbə</ta>
            <ta e="T259" id="Seg_6414" s="T258">bar</ta>
            <ta e="T260" id="Seg_6415" s="T259">burer-leʔbə</ta>
            <ta e="T261" id="Seg_6416" s="T260">daška</ta>
            <ta e="T262" id="Seg_6417" s="T261">bar</ta>
            <ta e="T265" id="Seg_6418" s="T264">ĭmbi</ta>
            <ta e="T266" id="Seg_6419" s="T265">pim-nie-l</ta>
            <ta e="T267" id="Seg_6420" s="T266">kan-a-ʔ</ta>
            <ta e="T268" id="Seg_6421" s="T267">unnʼa</ta>
            <ta e="T269" id="Seg_6422" s="T268">măn</ta>
            <ta e="T270" id="Seg_6423" s="T269">unnʼa</ta>
            <ta e="T271" id="Seg_6424" s="T270">mĭm-bie-m</ta>
            <ta e="T272" id="Seg_6425" s="T271">šindi-nə=də</ta>
            <ta e="T274" id="Seg_6426" s="T273">ej</ta>
            <ta e="T275" id="Seg_6427" s="T274">pim-nie-m</ta>
            <ta e="T276" id="Seg_6428" s="T275">a</ta>
            <ta e="T277" id="Seg_6429" s="T276">tăn</ta>
            <ta e="T278" id="Seg_6430" s="T277">üge</ta>
            <ta e="T279" id="Seg_6431" s="T278">pim-neʔbə-l</ta>
            <ta e="T280" id="Seg_6432" s="T279">nöməl-leʔ-pie-m</ta>
            <ta e="T283" id="Seg_6433" s="T282">dĭ-zeŋ</ta>
            <ta e="T284" id="Seg_6434" s="T283">ej</ta>
            <ta e="T285" id="Seg_6435" s="T284">tĭmne-ʔi</ta>
            <ta e="T286" id="Seg_6436" s="T285">dʼăbaktər-zittə</ta>
            <ta e="T287" id="Seg_6437" s="T286">a</ta>
            <ta e="T288" id="Seg_6438" s="T287">măn</ta>
            <ta e="T289" id="Seg_6439" s="T288">bar</ta>
            <ta e="T290" id="Seg_6440" s="T289">dʼăbaktər-laʔbə-m</ta>
            <ta e="T291" id="Seg_6441" s="T290">ia-m</ta>
            <ta e="T292" id="Seg_6442" s="T291">surar-bia-m</ta>
            <ta e="T293" id="Seg_6443" s="T292">dʼăbaktər-zittə</ta>
            <ta e="T294" id="Seg_6444" s="T293">axota</ta>
            <ta e="T295" id="Seg_6445" s="T294">măna</ta>
            <ta e="T296" id="Seg_6446" s="T295">bar</ta>
            <ta e="T298" id="Seg_6447" s="T297">dĭn</ta>
            <ta e="T299" id="Seg_6448" s="T298">dön</ta>
            <ta e="T300" id="Seg_6449" s="T299">bar</ta>
            <ta e="T301" id="Seg_6450" s="T300">dʼirəg-əʔi</ta>
            <ta e="T302" id="Seg_6451" s="T301">naga</ta>
            <ta e="T303" id="Seg_6452" s="T302">a</ta>
            <ta e="T305" id="Seg_6453" s="T304">onʼiʔ</ta>
            <ta e="T306" id="Seg_6454" s="T305">nu-zaŋ</ta>
            <ta e="T307" id="Seg_6455" s="T306">amno-laʔbə-ʔjə</ta>
            <ta e="T308" id="Seg_6456" s="T307">dĭ-zeŋ</ta>
            <ta e="T309" id="Seg_6457" s="T308">bar</ta>
            <ta e="T310" id="Seg_6458" s="T309">dʼirəg-əʔi</ta>
            <ta e="T311" id="Seg_6459" s="T310">pim-nie-ʔjə</ta>
            <ta e="T312" id="Seg_6460" s="T311">bar</ta>
            <ta e="T314" id="Seg_6461" s="T313">ej</ta>
            <ta e="T315" id="Seg_6462" s="T314">dʼăbaktər-la-m</ta>
            <ta e="T316" id="Seg_6463" s="T315">tüjö</ta>
            <ta e="T317" id="Seg_6464" s="T316">băra</ta>
            <ta e="T318" id="Seg_6465" s="T317">det-le-m</ta>
            <ta e="T319" id="Seg_6466" s="T318">tăn</ta>
            <ta e="T320" id="Seg_6467" s="T319">dʼabo-la-l</ta>
            <ta e="T321" id="Seg_6468" s="T320">măn</ta>
            <ta e="T322" id="Seg_6469" s="T321">toltanoʔ</ta>
            <ta e="T323" id="Seg_6470" s="T322">kămna-bia-m</ta>
            <ta e="T324" id="Seg_6471" s="T323">amno-gaʔ</ta>
            <ta e="T325" id="Seg_6472" s="T324">măn</ta>
            <ta e="T326" id="Seg_6473" s="T325">iššo</ta>
            <ta e="T327" id="Seg_6474" s="T326">šide</ta>
            <ta e="T328" id="Seg_6475" s="T327">aspaʔ</ta>
            <ta e="T329" id="Seg_6476" s="T328">det-le-m</ta>
            <ta e="T330" id="Seg_6477" s="T329">păʔ-la-m</ta>
            <ta e="T331" id="Seg_6478" s="T330">dĭgəttə</ta>
            <ta e="T332" id="Seg_6479" s="T331">dʼăbaktər-zittə</ta>
            <ta e="T333" id="Seg_6480" s="T332">možna</ta>
            <ta e="T334" id="Seg_6481" s="T333">bar</ta>
            <ta e="T335" id="Seg_6482" s="T334">ej</ta>
            <ta e="T336" id="Seg_6483" s="T335">dʼăbaktər-ia-m</ta>
            <ta e="T338" id="Seg_6484" s="T337">teinen</ta>
            <ta e="T339" id="Seg_6485" s="T338">ugaːndə</ta>
            <ta e="T340" id="Seg_6486" s="T339">dʼibige</ta>
            <ta e="T341" id="Seg_6487" s="T340">dʼala</ta>
            <ta e="T342" id="Seg_6488" s="T341">bar</ta>
            <ta e="T343" id="Seg_6489" s="T342">ipek</ta>
            <ta e="T344" id="Seg_6490" s="T343">nend-lə-j</ta>
            <ta e="T346" id="Seg_6491" s="T345">ipek</ta>
            <ta e="T347" id="Seg_6492" s="T346">ej</ta>
            <ta e="T348" id="Seg_6493" s="T347">özer-lə-j</ta>
            <ta e="T349" id="Seg_6494" s="T348">dĭgəttə</ta>
            <ta e="T350" id="Seg_6495" s="T349">naga</ta>
            <ta e="T351" id="Seg_6496" s="T350">ipek</ta>
            <ta e="T352" id="Seg_6497" s="T351">ĭmbi</ta>
            <ta e="T353" id="Seg_6498" s="T352">am-zittə</ta>
            <ta e="T354" id="Seg_6499" s="T353">kü-laːm-bia-m</ta>
            <ta e="T356" id="Seg_6500" s="T355">kü-zittə</ta>
            <ta e="T357" id="Seg_6501" s="T356">možnă</ta>
            <ta e="T358" id="Seg_6502" s="T357">ipek-ziʔ</ta>
            <ta e="T359" id="Seg_6503" s="T358">naga</ta>
            <ta e="T360" id="Seg_6504" s="T359">bar</ta>
            <ta e="T361" id="Seg_6505" s="T360">i-bə</ta>
            <ta e="T362" id="Seg_6506" s="T361">ej</ta>
            <ta e="T363" id="Seg_6507" s="T362">dʼăbaktər-la-m</ta>
            <ta e="T365" id="Seg_6508" s="T364">tăn</ta>
            <ta e="T366" id="Seg_6509" s="T365">ugaːndə</ta>
            <ta e="T367" id="Seg_6510" s="T366">sagər</ta>
            <ta e="T368" id="Seg_6511" s="T367">bazə-jd-a-ʔ</ta>
            <ta e="T369" id="Seg_6512" s="T368">multʼa-nə</ta>
            <ta e="T370" id="Seg_6513" s="T369">kan-a-ʔ</ta>
            <ta e="T371" id="Seg_6514" s="T370">sabən</ta>
            <ta e="T372" id="Seg_6515" s="T371">i-ʔ</ta>
            <ta e="T373" id="Seg_6516" s="T372">dĭgəttə</ta>
            <ta e="T374" id="Seg_6517" s="T373">kujnek</ta>
            <ta e="T375" id="Seg_6518" s="T374">i-ʔ</ta>
            <ta e="T376" id="Seg_6519" s="T375">šer-də</ta>
            <ta e="T377" id="Seg_6520" s="T376">kabarləj</ta>
            <ta e="T379" id="Seg_6521" s="T378">ugaːndə</ta>
            <ta e="T380" id="Seg_6522" s="T379">kuvas</ta>
            <ta e="T381" id="Seg_6523" s="T380">dĭgəttə</ta>
            <ta e="T382" id="Seg_6524" s="T381">mo-la-l</ta>
            <ta e="T383" id="Seg_6525" s="T382">kujnek</ta>
            <ta e="T384" id="Seg_6526" s="T383">šer-ie-l</ta>
            <ta e="T385" id="Seg_6527" s="T384">dăk</ta>
            <ta e="T386" id="Seg_6528" s="T385">bar</ta>
            <ta e="T387" id="Seg_6529" s="T386">koʔb-saŋ</ta>
            <ta e="T388" id="Seg_6530" s="T387">tănan</ta>
            <ta e="T391" id="Seg_6531" s="T390">pănar-lə-ʔi</ta>
            <ta e="T392" id="Seg_6532" s="T391">bar</ta>
            <ta e="T393" id="Seg_6533" s="T392">kuvas</ta>
            <ta e="T394" id="Seg_6534" s="T393">kabarləj</ta>
            <ta e="T396" id="Seg_6535" s="T395">dĭ-zeŋ</ta>
            <ta e="T397" id="Seg_6536" s="T396">Krasnojarskə-ʔi</ta>
            <ta e="T398" id="Seg_6537" s="T397">kam-bi-ʔi</ta>
            <ta e="T399" id="Seg_6538" s="T398">măna</ta>
            <ta e="T400" id="Seg_6539" s="T399">ej</ta>
            <ta e="T401" id="Seg_6540" s="T400">nörbə-ʔi</ta>
            <ta e="T402" id="Seg_6541" s="T401">măn</ta>
            <ta e="T403" id="Seg_6542" s="T402">măn</ta>
            <ta e="T404" id="Seg_6543" s="T403">dĭn</ta>
            <ta e="T405" id="Seg_6544" s="T404">tugan-bə</ta>
            <ta e="T406" id="Seg_6545" s="T405">i-ge</ta>
            <ta e="T407" id="Seg_6546" s="T406">măn</ta>
            <ta e="T408" id="Seg_6547" s="T407">bɨ</ta>
            <ta e="T409" id="Seg_6548" s="T408">noʔ</ta>
            <ta e="T410" id="Seg_6549" s="T409">mĭ-bie-m</ta>
            <ta e="T411" id="Seg_6550" s="T410">dĭ-n</ta>
            <ta e="T412" id="Seg_6551" s="T411">dĭ-n</ta>
            <ta e="T413" id="Seg_6552" s="T412">sĭj-də</ta>
            <ta e="T414" id="Seg_6553" s="T413">ĭzem-nie</ta>
            <ta e="T415" id="Seg_6554" s="T414">dĭ-zeŋ</ta>
            <ta e="T416" id="Seg_6555" s="T415">bar</ta>
            <ta e="T417" id="Seg_6556" s="T416">ku-bi-ʔi</ta>
            <ta e="T418" id="Seg_6557" s="T417">bɨ</ta>
            <ta e="T419" id="Seg_6558" s="T418">dĭ-m</ta>
            <ta e="T421" id="Seg_6559" s="T420">bar</ta>
            <ta e="T422" id="Seg_6560" s="T421">daška</ta>
            <ta e="T423" id="Seg_6561" s="T422">ej</ta>
            <ta e="T424" id="Seg_6562" s="T423">dʼăbaktər-la-m</ta>
            <ta e="T426" id="Seg_6563" s="T425">sagər</ta>
            <ta e="T427" id="Seg_6564" s="T426">ne</ta>
            <ta e="T428" id="Seg_6565" s="T427">šo-bi</ta>
            <ta e="T429" id="Seg_6566" s="T428">sĭre</ta>
            <ta e="T430" id="Seg_6567" s="T429">kuza</ta>
            <ta e="T431" id="Seg_6568" s="T430">măn-də</ta>
            <ta e="T432" id="Seg_6569" s="T431">i-zittə</ta>
            <ta e="T433" id="Seg_6570" s="T432">nada</ta>
            <ta e="T434" id="Seg_6571" s="T433">kan-žə-bəj</ta>
            <ta e="T435" id="Seg_6572" s="T434">obberəj</ta>
            <ta e="T436" id="Seg_6573" s="T435">aktʼi-gən</ta>
            <ta e="T437" id="Seg_6574" s="T436">bar</ta>
            <ta e="T439" id="Seg_6575" s="T438">kunol-žə-bəj</ta>
            <ta e="T440" id="Seg_6576" s="T439">bar</ta>
            <ta e="T441" id="Seg_6577" s="T440">panar-žə-bəj</ta>
            <ta e="T442" id="Seg_6578" s="T441">bar</ta>
            <ta e="T443" id="Seg_6579" s="T442">tibi-l</ta>
            <ta e="T444" id="Seg_6580" s="T443">bar</ta>
            <ta e="T445" id="Seg_6581" s="T444">münör-lə-j</ta>
            <ta e="T446" id="Seg_6582" s="T445">tănan</ta>
            <ta e="T447" id="Seg_6583" s="T446">kabarləj</ta>
            <ta e="T448" id="Seg_6584" s="T447">daška</ta>
            <ta e="T449" id="Seg_6585" s="T448">ej</ta>
            <ta e="T450" id="Seg_6586" s="T449">dʼăbaktər-la-m</ta>
            <ta e="T452" id="Seg_6587" s="T451">nada</ta>
            <ta e="T453" id="Seg_6588" s="T452">măna</ta>
            <ta e="T454" id="Seg_6589" s="T453">ešši</ta>
            <ta e="T455" id="Seg_6590" s="T454">măn</ta>
            <ta e="T456" id="Seg_6591" s="T455">băzəj-sʼtə</ta>
            <ta e="T457" id="Seg_6592" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_6593" s="T457">ugaːndə</ta>
            <ta e="T459" id="Seg_6594" s="T458">sagər</ta>
            <ta e="T460" id="Seg_6595" s="T459">bar</ta>
            <ta e="T461" id="Seg_6596" s="T460">tüʔ-pi</ta>
            <ta e="T462" id="Seg_6597" s="T461">iʔgö</ta>
            <ta e="T464" id="Seg_6598" s="T463">bar</ta>
            <ta e="T465" id="Seg_6599" s="T464">kĭškə-bie-m</ta>
            <ta e="T466" id="Seg_6600" s="T465">dĭ</ta>
            <ta e="T467" id="Seg_6601" s="T466">ko-laːm-bi</ta>
            <ta e="T468" id="Seg_6602" s="T467">dĭgəttə</ta>
            <ta e="T469" id="Seg_6603" s="T468">nada</ta>
            <ta e="T470" id="Seg_6604" s="T469">kür-zittə</ta>
            <ta e="T471" id="Seg_6605" s="T470">dĭ-m</ta>
            <ta e="T472" id="Seg_6606" s="T471">dĭgəttə</ta>
            <ta e="T475" id="Seg_6607" s="T474">amor-zittə</ta>
            <ta e="T476" id="Seg_6608" s="T475">nada</ta>
            <ta e="T477" id="Seg_6609" s="T476">dĭgəttə</ta>
            <ta e="T479" id="Seg_6610" s="T478">kunol-zittə</ta>
            <ta e="T480" id="Seg_6611" s="T479">or-də</ta>
            <ta e="T481" id="Seg_6612" s="T480">ešši-m</ta>
            <ta e="T486" id="Seg_6613" s="T485">teinen</ta>
            <ta e="T487" id="Seg_6614" s="T486">urgo</ta>
            <ta e="T488" id="Seg_6615" s="T487">dʼala</ta>
            <ta e="T489" id="Seg_6616" s="T488">a</ta>
            <ta e="T490" id="Seg_6617" s="T489">il</ta>
            <ta e="T491" id="Seg_6618" s="T490">üge</ta>
            <ta e="T492" id="Seg_6619" s="T491">togonor-laʔbə-ʔjə</ta>
            <ta e="T493" id="Seg_6620" s="T492">toltanoʔ</ta>
            <ta e="T494" id="Seg_6621" s="T493">am-laʔbə-ʔjə</ta>
            <ta e="T495" id="Seg_6622" s="T494">tajir-laʔbə-ʔjə</ta>
            <ta e="T496" id="Seg_6623" s="T495">a</ta>
            <ta e="T497" id="Seg_6624" s="T496">miʔ</ta>
            <ta e="T498" id="Seg_6625" s="T497">amno-laʔbə-baʔ</ta>
            <ta e="T499" id="Seg_6626" s="T498">kabarləj</ta>
            <ta e="T500" id="Seg_6627" s="T499">daška</ta>
            <ta e="T501" id="Seg_6628" s="T500">ej</ta>
            <ta e="T502" id="Seg_6629" s="T501">dʼăbaktər-zittə</ta>
            <ta e="T503" id="Seg_6630" s="T502">nʼe</ta>
            <ta e="T504" id="Seg_6631" s="T503">axota</ta>
            <ta e="T506" id="Seg_6632" s="T505">teinen</ta>
            <ta e="T507" id="Seg_6633" s="T506">urgo</ta>
            <ta e="T508" id="Seg_6634" s="T507">dʼala</ta>
            <ta e="T509" id="Seg_6635" s="T508">măna</ta>
            <ta e="T510" id="Seg_6636" s="T509">tugan-bə</ta>
            <ta e="T511" id="Seg_6637" s="T510">kăštə-bi-ʔi</ta>
            <ta e="T512" id="Seg_6638" s="T511">kan-žə-bəj</ta>
            <ta e="T513" id="Seg_6639" s="T512">dĭn</ta>
            <ta e="T514" id="Seg_6640" s="T513">ara</ta>
            <ta e="T515" id="Seg_6641" s="T514">bĭt-lə-bəj</ta>
            <ta e="T516" id="Seg_6642" s="T515">dĭn</ta>
            <ta e="T517" id="Seg_6643" s="T516">sʼar-laʔbə-ʔjə</ta>
            <ta e="T518" id="Seg_6644" s="T517">garmonʼə-nə</ta>
            <ta e="T520" id="Seg_6645" s="T519">miʔ</ta>
            <ta e="T521" id="Seg_6646" s="T520">bar</ta>
            <ta e="T522" id="Seg_6647" s="T521">suʔmə-leʔbə-beʔ</ta>
            <ta e="T523" id="Seg_6648" s="T522">bar</ta>
            <ta e="T525" id="Seg_6649" s="T524">oʔb</ta>
            <ta e="T526" id="Seg_6650" s="T525">oʔb</ta>
            <ta e="T527" id="Seg_6651" s="T526">šide</ta>
            <ta e="T528" id="Seg_6652" s="T527">nagur</ta>
            <ta e="T529" id="Seg_6653" s="T528">teʔtə</ta>
            <ta e="T530" id="Seg_6654" s="T529">sumna</ta>
            <ta e="T531" id="Seg_6655" s="T530">sejʔpü</ta>
            <ta e="T532" id="Seg_6656" s="T531">amitun</ta>
            <ta e="T533" id="Seg_6657" s="T532">šinteʔtə</ta>
            <ta e="T541" id="Seg_6658" s="T540">bʼeʔ</ta>
            <ta e="T543" id="Seg_6659" s="T542">bʼeʔ</ta>
            <ta e="T544" id="Seg_6660" s="T543">onʼiʔ</ta>
            <ta e="T545" id="Seg_6661" s="T544">bʼeʔ</ta>
            <ta e="T546" id="Seg_6662" s="T545">šide</ta>
            <ta e="T547" id="Seg_6663" s="T546">bʼeʔ</ta>
            <ta e="T548" id="Seg_6664" s="T547">sumna</ta>
            <ta e="T549" id="Seg_6665" s="T548">bʼeʔ</ta>
            <ta e="T550" id="Seg_6666" s="T549">muktuʔ</ta>
            <ta e="T552" id="Seg_6667" s="T551">măn</ta>
            <ta e="T553" id="Seg_6668" s="T552">unnʼa</ta>
            <ta e="T554" id="Seg_6669" s="T553">šo-bia-m</ta>
            <ta e="T555" id="Seg_6670" s="T554">a</ta>
            <ta e="T556" id="Seg_6671" s="T555">dĭ-zeŋ</ta>
            <ta e="T557" id="Seg_6672" s="T556">bar</ta>
            <ta e="T558" id="Seg_6673" s="T557">sumna</ta>
            <ta e="T559" id="Seg_6674" s="T558">šo-bi-ʔi</ta>
            <ta e="T561" id="Seg_6675" s="T560">măn</ta>
            <ta e="T562" id="Seg_6676" s="T561">mĭj</ta>
            <ta e="T564" id="Seg_6677" s="T563">mĭnzər-ie-m</ta>
            <ta e="T565" id="Seg_6678" s="T564">ugaːndə</ta>
            <ta e="T566" id="Seg_6679" s="T565">jakšə</ta>
            <ta e="T567" id="Seg_6680" s="T566">mĭj</ta>
            <ta e="T568" id="Seg_6681" s="T567">nʼamga</ta>
            <ta e="T569" id="Seg_6682" s="T568">uja-zʼiʔ</ta>
            <ta e="T570" id="Seg_6683" s="T569">köbergən</ta>
            <ta e="T571" id="Seg_6684" s="T570">em-bie-m</ta>
            <ta e="T572" id="Seg_6685" s="T571">bar</ta>
            <ta e="T573" id="Seg_6686" s="T572">tus</ta>
            <ta e="T574" id="Seg_6687" s="T573">em-bie-m</ta>
            <ta e="T575" id="Seg_6688" s="T574">toltanoʔ</ta>
            <ta e="T576" id="Seg_6689" s="T575">am-bie-m</ta>
            <ta e="T577" id="Seg_6690" s="T576">amna-ʔ</ta>
            <ta e="T578" id="Seg_6691" s="T577">amor-a-ʔ</ta>
            <ta e="T579" id="Seg_6692" s="T578">obberəj</ta>
            <ta e="T580" id="Seg_6693" s="T579">kabarləj</ta>
            <ta e="T582" id="Seg_6694" s="T581">ugaːndə</ta>
            <ta e="T583" id="Seg_6695" s="T582">kuštü</ta>
            <ta e="T584" id="Seg_6696" s="T583">kuza</ta>
            <ta e="T585" id="Seg_6697" s="T584">măn</ta>
            <ta e="T586" id="Seg_6698" s="T585">dĭʔ-nə</ta>
            <ta e="T587" id="Seg_6699" s="T586">pim-nie-m</ta>
            <ta e="T588" id="Seg_6700" s="T587">dĭ</ta>
            <ta e="T589" id="Seg_6701" s="T588">bar</ta>
            <ta e="T590" id="Seg_6702" s="T589">kut-lə-j</ta>
            <ta e="T591" id="Seg_6703" s="T590">măn</ta>
            <ta e="T592" id="Seg_6704" s="T591">šaʔ-laːm-bia-m</ta>
            <ta e="T593" id="Seg_6705" s="T592">štobɨ</ta>
            <ta e="T594" id="Seg_6706" s="T593">dĭ</ta>
            <ta e="T595" id="Seg_6707" s="T594">măna</ta>
            <ta e="T596" id="Seg_6708" s="T595">ej</ta>
            <ta e="T597" id="Seg_6709" s="T596">ku-bi</ta>
            <ta e="T598" id="Seg_6710" s="T597">măn</ta>
            <ta e="T599" id="Seg_6711" s="T598">dʼije-nə</ta>
            <ta e="T600" id="Seg_6712" s="T599">nuʔmə-luʔ-lə-m</ta>
            <ta e="T601" id="Seg_6713" s="T600">dĭ</ta>
            <ta e="T602" id="Seg_6714" s="T601">măna</ta>
            <ta e="T603" id="Seg_6715" s="T602">ej</ta>
            <ta e="T604" id="Seg_6716" s="T603">dĭn</ta>
            <ta e="T605" id="Seg_6717" s="T604">ej</ta>
            <ta e="T606" id="Seg_6718" s="T605">ku-lə-j</ta>
            <ta e="T607" id="Seg_6719" s="T606">kabarləj</ta>
            <ta e="T609" id="Seg_6720" s="T608">tibi-zeŋ</ta>
            <ta e="T610" id="Seg_6721" s="T609">iʔgö</ta>
            <ta e="T611" id="Seg_6722" s="T610">oʔbdə-bi-ʔi</ta>
            <ta e="T612" id="Seg_6723" s="T611">ĭmbi=də</ta>
            <ta e="T613" id="Seg_6724" s="T612">bar</ta>
            <ta e="T614" id="Seg_6725" s="T613">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T615" id="Seg_6726" s="T614">nada</ta>
            <ta e="T616" id="Seg_6727" s="T615">kan-zittə</ta>
            <ta e="T617" id="Seg_6728" s="T616">nʼilgö-sʼtə</ta>
            <ta e="T619" id="Seg_6729" s="T618">ĭmbi</ta>
            <ta e="T620" id="Seg_6730" s="T619">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T621" id="Seg_6731" s="T620">măn</ta>
            <ta e="T622" id="Seg_6732" s="T621">dĭgəttə</ta>
            <ta e="T625" id="Seg_6733" s="T624">tĭm-le-m</ta>
            <ta e="T626" id="Seg_6734" s="T625">bar</ta>
            <ta e="T627" id="Seg_6735" s="T626">daška</ta>
            <ta e="T628" id="Seg_6736" s="T627">ej</ta>
            <ta e="T629" id="Seg_6737" s="T628">dʼăbaktər-lia-m</ta>
            <ta e="T635" id="Seg_6738" s="T634">dĭ</ta>
            <ta e="T636" id="Seg_6739" s="T635">kuza</ta>
            <ta e="T637" id="Seg_6740" s="T636">bar</ta>
            <ta e="T638" id="Seg_6741" s="T637">jezerik</ta>
            <ta e="T639" id="Seg_6742" s="T638">ine-gən</ta>
            <ta e="T640" id="Seg_6743" s="T639">amno-luʔ-pi</ta>
            <ta e="T641" id="Seg_6744" s="T640">dĭgəttə</ta>
            <ta e="T642" id="Seg_6745" s="T641">üzə-bi</ta>
            <ta e="T643" id="Seg_6746" s="T642">dʼü-nə</ta>
            <ta e="T644" id="Seg_6747" s="T643">dĭgəttə</ta>
            <ta e="T645" id="Seg_6748" s="T644">ine-t</ta>
            <ta e="T646" id="Seg_6749" s="T645">nuʔmə-luʔ-pi</ta>
            <ta e="T647" id="Seg_6750" s="T646">üžü</ta>
            <ta e="T648" id="Seg_6751" s="T647">üžü</ta>
            <ta e="T650" id="Seg_6752" s="T649">üžü</ta>
            <ta e="T651" id="Seg_6753" s="T650">dʼürdə-bi</ta>
            <ta e="T652" id="Seg_6754" s="T651">ma-luʔ-pi</ta>
            <ta e="T653" id="Seg_6755" s="T652">bar</ta>
            <ta e="T654" id="Seg_6756" s="T653">kunol-laʔbə</ta>
            <ta e="T655" id="Seg_6757" s="T654">kunol-laʔ</ta>
            <ta e="T656" id="Seg_6758" s="T655">iʔbo-laʔbə</ta>
            <ta e="T657" id="Seg_6759" s="T656">saʔmə-luʔ-pi</ta>
            <ta e="T658" id="Seg_6760" s="T657">bar</ta>
            <ta e="T659" id="Seg_6761" s="T658">kabarləj</ta>
            <ta e="T661" id="Seg_6762" s="T660">nʼi-zeŋ</ta>
            <ta e="T662" id="Seg_6763" s="T661">koʔb-saŋ</ta>
            <ta e="T663" id="Seg_6764" s="T662">bar</ta>
            <ta e="T664" id="Seg_6765" s="T663">iʔgö</ta>
            <ta e="T665" id="Seg_6766" s="T664">oʔbdə-bi-ʔi</ta>
            <ta e="T666" id="Seg_6767" s="T665">bar</ta>
            <ta e="T667" id="Seg_6768" s="T666">nüj-leʔbə-ʔjə</ta>
            <ta e="T668" id="Seg_6769" s="T667">garmonʼə</ta>
            <ta e="T669" id="Seg_6770" s="T668">sʼar-laʔbə-ʔjə</ta>
            <ta e="T670" id="Seg_6771" s="T669">suʔmi-leʔbə-ʔjə</ta>
            <ta e="T671" id="Seg_6772" s="T670">kabarləj</ta>
            <ta e="T673" id="Seg_6773" s="T672">măn</ta>
            <ta e="T674" id="Seg_6774" s="T673">dʼije-nə</ta>
            <ta e="T675" id="Seg_6775" s="T674">mĭm-bie-m</ta>
            <ta e="T676" id="Seg_6776" s="T675">keʔbde</ta>
            <ta e="T677" id="Seg_6777" s="T676">nĭŋgə-bie-m</ta>
            <ta e="T678" id="Seg_6778" s="T677">kălba</ta>
            <ta e="T679" id="Seg_6779" s="T678">nĭŋgə-bie-m</ta>
            <ta e="T680" id="Seg_6780" s="T679">pirog-əʔi</ta>
            <ta e="T681" id="Seg_6781" s="T680">pür-bie-m</ta>
            <ta e="T682" id="Seg_6782" s="T681">šiʔnʼileʔ</ta>
            <ta e="T683" id="Seg_6783" s="T682">mĭ-le-m</ta>
            <ta e="T684" id="Seg_6784" s="T683">kabarləj</ta>
            <ta e="T686" id="Seg_6785" s="T685">măn</ta>
            <ta e="T687" id="Seg_6786" s="T686">ia-m</ta>
            <ta e="T688" id="Seg_6787" s="T687">nu</ta>
            <ta e="T689" id="Seg_6788" s="T688">i-bi</ta>
            <ta e="T690" id="Seg_6789" s="T689">aba-m</ta>
            <ta e="T691" id="Seg_6790" s="T690">dʼirək</ta>
            <ta e="T692" id="Seg_6791" s="T691">i-bi</ta>
            <ta e="T693" id="Seg_6792" s="T692">šide</ta>
            <ta e="T694" id="Seg_6793" s="T693">nʼi</ta>
            <ta e="T695" id="Seg_6794" s="T694">i-bi-ʔi</ta>
            <ta e="T699" id="Seg_6795" s="T698">muktuʔ</ta>
            <ta e="T701" id="Seg_6796" s="T700">i-bi</ta>
            <ta e="T702" id="Seg_6797" s="T701">bar</ta>
            <ta e="T703" id="Seg_6798" s="T702">kabarləj</ta>
            <ta e="T705" id="Seg_6799" s="T704">nagur</ta>
            <ta e="T706" id="Seg_6800" s="T705">kü-laːm-bi-ʔi</ta>
            <ta e="T707" id="Seg_6801" s="T706">a</ta>
            <ta e="T708" id="Seg_6802" s="T707">nagur</ta>
            <ta e="T709" id="Seg_6803" s="T708">ej</ta>
            <ta e="T710" id="Seg_6804" s="T709">kü-bi-ʔi</ta>
            <ta e="T711" id="Seg_6805" s="T710">măn</ta>
            <ta e="T712" id="Seg_6806" s="T711">aba-m</ta>
            <ta e="T714" id="Seg_6807" s="T712">albuga</ta>
            <ta e="T720" id="Seg_6808" s="T719">bʼeʔ</ta>
            <ta e="T721" id="Seg_6809" s="T720">sumna</ta>
            <ta e="T722" id="Seg_6810" s="T721">kuʔ-pi</ta>
            <ta e="T724" id="Seg_6811" s="T722">bar</ta>
            <ta e="T725" id="Seg_6812" s="T724">kabarləj</ta>
            <ta e="T726" id="Seg_6813" s="T725">tažəp</ta>
            <ta e="T727" id="Seg_6814" s="T726">išo</ta>
            <ta e="T728" id="Seg_6815" s="T727">băra</ta>
            <ta e="T729" id="Seg_6816" s="T728">deʔ-pi</ta>
            <ta e="T730" id="Seg_6817" s="T729">kabarləj</ta>
            <ta e="T731" id="Seg_6818" s="T730">nu</ta>
            <ta e="T732" id="Seg_6819" s="T731">kuza</ta>
            <ta e="T733" id="Seg_6820" s="T732">ĭzem-nie</ta>
            <ta e="T735" id="Seg_6821" s="T734">dĭgəttə</ta>
            <ta e="T736" id="Seg_6822" s="T735">kam-bi</ta>
            <ta e="T737" id="Seg_6823" s="T736">šaman-də</ta>
            <ta e="T738" id="Seg_6824" s="T737">dĭ</ta>
            <ta e="T740" id="Seg_6825" s="T739">dĭ-m</ta>
            <ta e="T741" id="Seg_6826" s="T740">bar</ta>
            <ta e="T742" id="Seg_6827" s="T741">dʼazir-laʔ-pi</ta>
            <ta e="T743" id="Seg_6828" s="T742">daška</ta>
            <ta e="T744" id="Seg_6829" s="T743">bar</ta>
            <ta e="T745" id="Seg_6830" s="T744">naverna</ta>
            <ta e="T746" id="Seg_6831" s="T745">măn</ta>
            <ta e="T747" id="Seg_6832" s="T746">ugaːndə</ta>
            <ta e="T748" id="Seg_6833" s="T747">püjö-lia-m</ta>
            <ta e="T749" id="Seg_6834" s="T748">amna-ʔ</ta>
            <ta e="T750" id="Seg_6835" s="T749">amor-a-ʔ</ta>
            <ta e="T751" id="Seg_6836" s="T750">šindi</ta>
            <ta e="T752" id="Seg_6837" s="T751">tănan</ta>
            <ta e="T753" id="Seg_6838" s="T752">ej</ta>
            <ta e="T754" id="Seg_6839" s="T753">mĭ-lie</ta>
            <ta e="T755" id="Seg_6840" s="T754">dĭgəttə</ta>
            <ta e="T758" id="Seg_6841" s="T757">iʔb-e-ʔ</ta>
            <ta e="T759" id="Seg_6842" s="T758">da</ta>
            <ta e="T760" id="Seg_6843" s="T759">kunol-a-ʔ</ta>
            <ta e="T761" id="Seg_6844" s="T760">kabarləj</ta>
            <ta e="T763" id="Seg_6845" s="T762">es-seŋ</ta>
            <ta e="T764" id="Seg_6846" s="T763">nʼiʔnen</ta>
            <ta e="T765" id="Seg_6847" s="T764">bar</ta>
            <ta e="T767" id="Seg_6848" s="T766">sʼar-laʔbə-ʔjə</ta>
            <ta e="T768" id="Seg_6849" s="T767">kirgar-laʔbə-ʔjə</ta>
            <ta e="T769" id="Seg_6850" s="T768">dʼabro-laʔbə-ʔjə</ta>
            <ta e="T770" id="Seg_6851" s="T769">našto</ta>
            <ta e="T771" id="Seg_6852" s="T770">dʼabro-lia-l</ta>
            <ta e="T772" id="Seg_6853" s="T771">jakšə</ta>
            <ta e="T773" id="Seg_6854" s="T772">nada</ta>
            <ta e="T774" id="Seg_6855" s="T773">sʼar-zittə</ta>
            <ta e="T775" id="Seg_6856" s="T774">i-ʔ</ta>
            <ta e="T776" id="Seg_6857" s="T775">dʼabro-sʼtə</ta>
            <ta e="T777" id="Seg_6858" s="T776">kabarləj</ta>
            <ta e="T778" id="Seg_6859" s="T777">naverna</ta>
            <ta e="T780" id="Seg_6860" s="T779">i-ʔ</ta>
            <ta e="T781" id="Seg_6861" s="T780">dʼabro-gaʔ</ta>
            <ta e="T782" id="Seg_6862" s="T781">nada</ta>
            <ta e="T783" id="Seg_6863" s="T782">jakšə</ta>
            <ta e="T784" id="Seg_6864" s="T783">sʼar-zittə</ta>
            <ta e="T785" id="Seg_6865" s="T784">našto</ta>
            <ta e="T786" id="Seg_6866" s="T785">dʼabro-laʔbə-l</ta>
            <ta e="T788" id="Seg_6867" s="T787">măn</ta>
            <ta e="T789" id="Seg_6868" s="T788">teinen</ta>
            <ta e="T790" id="Seg_6869" s="T789">nu-bia-m</ta>
            <ta e="T791" id="Seg_6870" s="T790">kuja</ta>
            <ta e="T792" id="Seg_6871" s="T791">iššo</ta>
            <ta e="T793" id="Seg_6872" s="T792">ej</ta>
            <ta e="T794" id="Seg_6873" s="T793">nu-bi</ta>
            <ta e="T795" id="Seg_6874" s="T794">măn</ta>
            <ta e="T796" id="Seg_6875" s="T795">nu-bia-m</ta>
            <ta e="T797" id="Seg_6876" s="T796">dĭgəttə</ta>
            <ta e="T798" id="Seg_6877" s="T797">tüžöj</ta>
            <ta e="T799" id="Seg_6878" s="T798">surdə-bia-m</ta>
            <ta e="T800" id="Seg_6879" s="T799">büzəj</ta>
            <ta e="T801" id="Seg_6880" s="T800">bĭt-əl-bie-m</ta>
            <ta e="T802" id="Seg_6881" s="T801">kurizə-ʔi</ta>
            <ta e="T806" id="Seg_6882" s="T805">băd-laʔbə-m</ta>
            <ta e="T807" id="Seg_6883" s="T806">dĭgəttə</ta>
            <ta e="T808" id="Seg_6884" s="T807">munəj-ʔ</ta>
            <ta e="T809" id="Seg_6885" s="T808">pür-bie-m</ta>
            <ta e="T810" id="Seg_6886" s="T809">toltanoʔ</ta>
            <ta e="T811" id="Seg_6887" s="T810">mĭnzər-bie-m</ta>
            <ta e="T812" id="Seg_6888" s="T811">amor-bia-m</ta>
            <ta e="T813" id="Seg_6889" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_6890" s="T813">nu</ta>
            <ta e="T815" id="Seg_6891" s="T814">kuza</ta>
            <ta e="T816" id="Seg_6892" s="T815">nu</ta>
            <ta e="T817" id="Seg_6893" s="T816">il</ta>
            <ta e="T818" id="Seg_6894" s="T817">šo-bi-ʔi</ta>
            <ta e="T819" id="Seg_6895" s="T818">dʼăbaktər-bia-m</ta>
            <ta e="T821" id="Seg_6896" s="T820">bar</ta>
            <ta e="T822" id="Seg_6897" s="T821">tarar-luʔ-pia-m</ta>
            <ta e="T823" id="Seg_6898" s="T822">kabarləj</ta>
            <ta e="T825" id="Seg_6899" s="T824">kandə-gaʔ</ta>
            <ta e="T826" id="Seg_6900" s="T825">maʔ-nə-l</ta>
            <ta e="T827" id="Seg_6901" s="T826">kabarləj</ta>
            <ta e="T828" id="Seg_6902" s="T827">dʼăbaktər-zittə</ta>
            <ta e="T829" id="Seg_6903" s="T828">am-gaʔ</ta>
            <ta e="T830" id="Seg_6904" s="T829">iʔb-e-ʔ</ta>
            <ta e="T831" id="Seg_6905" s="T830">kunol-zittə</ta>
            <ta e="T832" id="Seg_6906" s="T831">kabarləj</ta>
            <ta e="T833" id="Seg_6907" s="T832">daška</ta>
            <ta e="T835" id="Seg_6908" s="T834">šumura-nə</ta>
            <ta e="T836" id="Seg_6909" s="T835">mĭm-bie-m</ta>
            <ta e="T837" id="Seg_6910" s="T836">ugaːndə</ta>
            <ta e="T838" id="Seg_6911" s="T837">il</ta>
            <ta e="T839" id="Seg_6912" s="T838">iʔgö</ta>
            <ta e="T840" id="Seg_6913" s="T839">ku-bia-m</ta>
            <ta e="T841" id="Seg_6914" s="T840">koŋ</ta>
            <ta e="T842" id="Seg_6915" s="T841">bar</ta>
            <ta e="T843" id="Seg_6916" s="T842">šo-bi-ʔi</ta>
            <ta e="T844" id="Seg_6917" s="T843">tibi-zeŋ</ta>
            <ta e="T845" id="Seg_6918" s="T844">dĭn</ta>
            <ta e="T846" id="Seg_6919" s="T845">iʔgö</ta>
            <ta e="T847" id="Seg_6920" s="T846">ine-zeŋ</ta>
            <ta e="T848" id="Seg_6921" s="T847">koʔb-saŋ</ta>
            <ta e="T849" id="Seg_6922" s="T848">nʼi-zeŋ</ta>
            <ta e="T850" id="Seg_6923" s="T849">bar</ta>
            <ta e="T851" id="Seg_6924" s="T850">kabarləj</ta>
            <ta e="T853" id="Seg_6925" s="T852">ĭmbi</ta>
            <ta e="T854" id="Seg_6926" s="T853">dĭ</ta>
            <ta e="T855" id="Seg_6927" s="T854">todam</ta>
            <ta e="T856" id="Seg_6928" s="T855">kuza</ta>
            <ta e="T857" id="Seg_6929" s="T856">ej</ta>
            <ta e="T858" id="Seg_6930" s="T857">šo-bi</ta>
            <ta e="T859" id="Seg_6931" s="T858">koʔbdo</ta>
            <ta e="T860" id="Seg_6932" s="T859">ej</ta>
            <ta e="T861" id="Seg_6933" s="T860">šo-bi</ta>
            <ta e="T862" id="Seg_6934" s="T861">gijen</ta>
            <ta e="T863" id="Seg_6935" s="T862">dĭ-zeŋ</ta>
            <ta e="T864" id="Seg_6936" s="T863">ĭmbi</ta>
            <ta e="T865" id="Seg_6937" s="T864">ej</ta>
            <ta e="T866" id="Seg_6938" s="T865">šo-bi-ʔi</ta>
            <ta e="T867" id="Seg_6939" s="T866">dʼăbaktər-zittə</ta>
            <ta e="T868" id="Seg_6940" s="T867">măn-zʼiʔ</ta>
            <ta e="T869" id="Seg_6941" s="T868">kabarləj</ta>
            <ta e="T871" id="Seg_6942" s="T870">gijen</ta>
            <ta e="T872" id="Seg_6943" s="T871">dĭ-zeŋ</ta>
            <ta e="T873" id="Seg_6944" s="T872">ĭmbi</ta>
            <ta e="T874" id="Seg_6945" s="T873">ej</ta>
            <ta e="T875" id="Seg_6946" s="T874">šo-bi-ʔi</ta>
            <ta e="T876" id="Seg_6947" s="T875">dʼăbaktər-zittə</ta>
            <ta e="T878" id="Seg_6948" s="T877">teinen</ta>
            <ta e="T879" id="Seg_6949" s="T878">kabarləj</ta>
            <ta e="T880" id="Seg_6950" s="T879">dʼăbaktər-zittə</ta>
            <ta e="T881" id="Seg_6951" s="T880">kan-a-ʔ</ta>
            <ta e="T882" id="Seg_6952" s="T881">maʔ-nə-l</ta>
            <ta e="T883" id="Seg_6953" s="T882">tăn</ta>
            <ta e="T884" id="Seg_6954" s="T883">măna</ta>
            <ta e="T886" id="Seg_6955" s="T885">dăre</ta>
            <ta e="T887" id="Seg_6956" s="T886">ej</ta>
            <ta e="T891" id="Seg_6957" s="T890">tăn</ta>
            <ta e="T892" id="Seg_6958" s="T891">măna</ta>
            <ta e="T893" id="Seg_6959" s="T892">dăre</ta>
            <ta e="T894" id="Seg_6960" s="T893">ej</ta>
            <ta e="T896" id="Seg_6961" s="T895">tüšəl-l-e-ʔ</ta>
            <ta e="T897" id="Seg_6962" s="T896">dăre</ta>
            <ta e="T898" id="Seg_6963" s="T897">ej</ta>
            <ta e="T899" id="Seg_6964" s="T898">jakšə</ta>
            <ta e="T900" id="Seg_6965" s="T899">măn-zittə</ta>
            <ta e="T901" id="Seg_6966" s="T900">našto</ta>
            <ta e="T902" id="Seg_6967" s="T901">sürer-zittə</ta>
            <ta e="T903" id="Seg_6968" s="T902">il</ta>
            <ta e="T904" id="Seg_6969" s="T903">dĭ</ta>
            <ta e="T905" id="Seg_6970" s="T904">nu-zaŋ</ta>
            <ta e="T906" id="Seg_6971" s="T905">il</ta>
            <ta e="T908" id="Seg_6972" s="T907">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T16" id="Seg_6973" s="T13">tʼezer-ə-ʔ</ta>
            <ta e="T17" id="Seg_6974" s="T16">tüjö</ta>
            <ta e="T18" id="Seg_6975" s="T17">tʼăbaktər-zittə</ta>
            <ta e="T19" id="Seg_6976" s="T18">nadə</ta>
            <ta e="T20" id="Seg_6977" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_6978" s="T20">tajər-də</ta>
            <ta e="T22" id="Seg_6979" s="T21">kan-lV-m</ta>
            <ta e="T23" id="Seg_6980" s="T22">tʼo</ta>
            <ta e="T24" id="Seg_6981" s="T23">tajər-zittə</ta>
            <ta e="T25" id="Seg_6982" s="T24">dĭgəttə</ta>
            <ta e="T26" id="Seg_6983" s="T25">ipek</ta>
            <ta e="T27" id="Seg_6984" s="T26">kut-zittə</ta>
            <ta e="T28" id="Seg_6985" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_6986" s="T28">özer-lV-j</ta>
            <ta e="T30" id="Seg_6987" s="T29">urgo</ta>
            <ta e="T31" id="Seg_6988" s="T30">özer-lV-j</ta>
            <ta e="T32" id="Seg_6989" s="T31">bar</ta>
            <ta e="T33" id="Seg_6990" s="T32">sĭri</ta>
            <ta e="T34" id="Seg_6991" s="T33">mo-lV-j</ta>
            <ta e="T35" id="Seg_6992" s="T34">dĭgəttə</ta>
            <ta e="T36" id="Seg_6993" s="T35">pü-zittə</ta>
            <ta e="T37" id="Seg_6994" s="T36">nadə</ta>
            <ta e="T40" id="Seg_6995" s="T39">dĭgəttə</ta>
            <ta e="T41" id="Seg_6996" s="T40">toʔ-nar-zittə</ta>
            <ta e="T42" id="Seg_6997" s="T41">nadə</ta>
            <ta e="T43" id="Seg_6998" s="T42">dĭgəttə</ta>
            <ta e="T44" id="Seg_6999" s="T43">băra-Tə</ta>
            <ta e="T45" id="Seg_7000" s="T44">kămnə-zittə</ta>
            <ta e="T46" id="Seg_7001" s="T45">dĭgəttə</ta>
            <ta e="T47" id="Seg_7002" s="T46">nadə</ta>
            <ta e="T48" id="Seg_7003" s="T47">nʼeʔbdə-zittə</ta>
            <ta e="T49" id="Seg_7004" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_7005" s="T49">nadə</ta>
            <ta e="T51" id="Seg_7006" s="T50">ilgər-zittə</ta>
            <ta e="T52" id="Seg_7007" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_7008" s="T52">nuldə-zittə</ta>
            <ta e="T54" id="Seg_7009" s="T53">nadə</ta>
            <ta e="T55" id="Seg_7010" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_7011" s="T55">pür-zittə</ta>
            <ta e="T57" id="Seg_7012" s="T56">nadə</ta>
            <ta e="T58" id="Seg_7013" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_7014" s="T58">am-zittə</ta>
            <ta e="T60" id="Seg_7015" s="T59">nadə</ta>
            <ta e="T61" id="Seg_7016" s="T60">bar</ta>
            <ta e="T63" id="Seg_7017" s="T62">tăn</ta>
            <ta e="T64" id="Seg_7018" s="T63">tăn</ta>
            <ta e="T65" id="Seg_7019" s="T64">kandə-laʔbə-l</ta>
            <ta e="T66" id="Seg_7020" s="T65">ĭmbi</ta>
            <ta e="T67" id="Seg_7021" s="T66">măna</ta>
            <ta e="T68" id="Seg_7022" s="T67">ej</ta>
            <ta e="T69" id="Seg_7023" s="T68">nörbə-bi-l</ta>
            <ta e="T70" id="Seg_7024" s="T69">măn</ta>
            <ta e="T71" id="Seg_7025" s="T70">bɨ</ta>
            <ta e="T72" id="Seg_7026" s="T71">noʔ</ta>
            <ta e="T74" id="Seg_7027" s="T73">mĭ-bi-m</ta>
            <ta e="T75" id="Seg_7028" s="T74">dĭn</ta>
            <ta e="T76" id="Seg_7029" s="T75">măn</ta>
            <ta e="T77" id="Seg_7030" s="T76">tugan-zAŋ-bə</ta>
            <ta e="T78" id="Seg_7031" s="T77">i-gA</ta>
            <ta e="T79" id="Seg_7032" s="T78">tăn</ta>
            <ta e="T80" id="Seg_7033" s="T79">bɨ</ta>
            <ta e="T81" id="Seg_7034" s="T80">ku-bi-l</ta>
            <ta e="T82" id="Seg_7035" s="T81">dĭ-m</ta>
            <ta e="T83" id="Seg_7036" s="T82">kabarləj</ta>
            <ta e="T85" id="Seg_7037" s="T84">tăn</ta>
            <ta e="T86" id="Seg_7038" s="T85">ĭmbi</ta>
            <ta e="T87" id="Seg_7039" s="T86">măna</ta>
            <ta e="T88" id="Seg_7040" s="T87">ej</ta>
            <ta e="T90" id="Seg_7041" s="T89">nörbə-bi-l</ta>
            <ta e="T91" id="Seg_7042" s="T90">što</ta>
            <ta e="T92" id="Seg_7043" s="T91">kandə-gA-l</ta>
            <ta e="T93" id="Seg_7044" s="T92">Krasnojarskə</ta>
            <ta e="T94" id="Seg_7045" s="T93">dĭn</ta>
            <ta e="T95" id="Seg_7046" s="T94">măn</ta>
            <ta e="T96" id="Seg_7047" s="T95">tugan-m</ta>
            <ta e="T97" id="Seg_7048" s="T96">i-gA</ta>
            <ta e="T98" id="Seg_7049" s="T97">măn</ta>
            <ta e="T99" id="Seg_7050" s="T98">bɨ</ta>
            <ta e="T100" id="Seg_7051" s="T99">noʔ</ta>
            <ta e="T101" id="Seg_7052" s="T100">tănan</ta>
            <ta e="T102" id="Seg_7053" s="T101">mĭ-bi-m</ta>
            <ta e="T103" id="Seg_7054" s="T102">dĭ-n</ta>
            <ta e="T104" id="Seg_7055" s="T103">sĭj-də</ta>
            <ta e="T105" id="Seg_7056" s="T104">ĭzem-bi</ta>
            <ta e="T106" id="Seg_7057" s="T105">dĭ</ta>
            <ta e="T108" id="Seg_7058" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_7059" s="T108">bɨ</ta>
            <ta e="T110" id="Seg_7060" s="T109">bĭs-bi</ta>
            <ta e="T111" id="Seg_7061" s="T110">tăn</ta>
            <ta e="T112" id="Seg_7062" s="T111">bɨ</ta>
            <ta e="T113" id="Seg_7063" s="T112">ku-bi-l</ta>
            <ta e="T114" id="Seg_7064" s="T113">dĭ-m</ta>
            <ta e="T115" id="Seg_7065" s="T114">kabarləj</ta>
            <ta e="T117" id="Seg_7066" s="T116">amnə-ʔ</ta>
            <ta e="T118" id="Seg_7067" s="T117">toltano</ta>
            <ta e="T119" id="Seg_7068" s="T118">am-zittə</ta>
            <ta e="T120" id="Seg_7069" s="T119">a</ta>
            <ta e="T121" id="Seg_7070" s="T120">girgit</ta>
            <ta e="T122" id="Seg_7071" s="T121">toltano</ta>
            <ta e="T123" id="Seg_7072" s="T122">da</ta>
            <ta e="T124" id="Seg_7073" s="T123">măn</ta>
            <ta e="T125" id="Seg_7074" s="T124">kĭškə-bi-m</ta>
            <ta e="T126" id="Seg_7075" s="T125">dĭgəttə</ta>
            <ta e="T127" id="Seg_7076" s="T126">un</ta>
            <ta e="T128" id="Seg_7077" s="T127">i-bi</ta>
            <ta e="T129" id="Seg_7078" s="T128">măn</ta>
            <ta e="T130" id="Seg_7079" s="T129">dĭgəttə</ta>
            <ta e="T131" id="Seg_7080" s="T130">sĭreʔp</ta>
            <ta e="T132" id="Seg_7081" s="T131">hen-bi-m</ta>
            <ta e="T133" id="Seg_7082" s="T132">i</ta>
            <ta e="T134" id="Seg_7083" s="T133">keʔbde</ta>
            <ta e="T135" id="Seg_7084" s="T134">hen-bi-m</ta>
            <ta e="T136" id="Seg_7085" s="T135">dʼibige</ta>
            <ta e="T138" id="Seg_7086" s="T137">bü</ta>
            <ta e="T139" id="Seg_7087" s="T138">kămnə-bi-m</ta>
            <ta e="T140" id="Seg_7088" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_7089" s="T140">mĭnzər-bi-m</ta>
            <ta e="T142" id="Seg_7090" s="T141">amnə-ʔ</ta>
            <ta e="T143" id="Seg_7091" s="T142">amor-ə-ʔ</ta>
            <ta e="T145" id="Seg_7092" s="T144">nʼi</ta>
            <ta e="T146" id="Seg_7093" s="T145">naga</ta>
            <ta e="T147" id="Seg_7094" s="T146">gibər=də</ta>
            <ta e="T911" id="Seg_7095" s="T147">kan-lAʔ</ta>
            <ta e="T148" id="Seg_7096" s="T911">tʼür-bi</ta>
            <ta e="T150" id="Seg_7097" s="T149">măndo-r-zittə</ta>
            <ta e="T151" id="Seg_7098" s="T150">nadə</ta>
            <ta e="T152" id="Seg_7099" s="T151">možet</ta>
            <ta e="T153" id="Seg_7100" s="T152">šində=nʼibudʼ</ta>
            <ta e="T154" id="Seg_7101" s="T153">kut-bi</ta>
            <ta e="T155" id="Seg_7102" s="T154">možet</ta>
            <ta e="T156" id="Seg_7103" s="T155">urgaːba</ta>
            <ta e="T157" id="Seg_7104" s="T156">am-bi</ta>
            <ta e="T158" id="Seg_7105" s="T157">ajir-liA-m</ta>
            <ta e="T159" id="Seg_7106" s="T158">bar</ta>
            <ta e="T160" id="Seg_7107" s="T159">ugaːndə</ta>
            <ta e="T161" id="Seg_7108" s="T160">kabarləj</ta>
            <ta e="T163" id="Seg_7109" s="T162">oʔb</ta>
            <ta e="T164" id="Seg_7110" s="T163">šide</ta>
            <ta e="T165" id="Seg_7111" s="T164">nagur</ta>
            <ta e="T166" id="Seg_7112" s="T165">teʔdə</ta>
            <ta e="T167" id="Seg_7113" s="T166">sumna</ta>
            <ta e="T168" id="Seg_7114" s="T167">sejʔpü</ta>
            <ta e="T169" id="Seg_7115" s="T168">šĭnteʔtə</ta>
            <ta e="T170" id="Seg_7116" s="T169">biəʔ</ta>
            <ta e="T174" id="Seg_7117" s="T173">kabɨ</ta>
            <ta e="T175" id="Seg_7118" s="T174">bos-də</ta>
            <ta e="T177" id="Seg_7119" s="T176">ine-zAŋ-də</ta>
            <ta e="T178" id="Seg_7120" s="T177">i-bi-jəʔ</ta>
            <ta e="T179" id="Seg_7121" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_7122" s="T179">tojar-zittə</ta>
            <ta e="T181" id="Seg_7123" s="T180">možna</ta>
            <ta e="T182" id="Seg_7124" s="T181">i</ta>
            <ta e="T183" id="Seg_7125" s="T182">toltano</ta>
            <ta e="T184" id="Seg_7126" s="T183">am-zittə</ta>
            <ta e="T185" id="Seg_7127" s="T184">ato</ta>
            <ta e="T186" id="Seg_7128" s="T185">bos-də</ta>
            <ta e="T187" id="Seg_7129" s="T186">ine-jəʔ</ta>
            <ta e="T188" id="Seg_7130" s="T187">naga</ta>
            <ta e="T189" id="Seg_7131" s="T188">bar</ta>
            <ta e="T191" id="Seg_7132" s="T190">măn</ta>
            <ta e="T192" id="Seg_7133" s="T191">mej-m</ta>
            <ta e="T193" id="Seg_7134" s="T192">bar</ta>
            <ta e="T194" id="Seg_7135" s="T193">jezerik</ta>
            <ta e="T195" id="Seg_7136" s="T194">ara</ta>
            <ta e="T196" id="Seg_7137" s="T195">bĭs-bi</ta>
            <ta e="T197" id="Seg_7138" s="T196">šo-bi</ta>
            <ta e="T198" id="Seg_7139" s="T197">bar</ta>
            <ta e="T199" id="Seg_7140" s="T198">kudo-nzə-bi-jəʔ</ta>
            <ta e="T200" id="Seg_7141" s="T199">bos-də</ta>
            <ta e="T201" id="Seg_7142" s="T200">tibi-ziʔ</ta>
            <ta e="T202" id="Seg_7143" s="T201">tʼabəro-bi-jəʔ</ta>
            <ta e="T203" id="Seg_7144" s="T202">bar</ta>
            <ta e="T204" id="Seg_7145" s="T203">ugaːndə</ta>
            <ta e="T205" id="Seg_7146" s="T204">dĭ</ta>
            <ta e="T206" id="Seg_7147" s="T205">kurojok</ta>
            <ta e="T207" id="Seg_7148" s="T206">vsʼaka</ta>
            <ta e="T209" id="Seg_7149" s="T208">kudo-nzə-laʔbə-bi</ta>
            <ta e="T210" id="Seg_7150" s="T209">kădaʔ</ta>
            <ta e="T211" id="Seg_7151" s="T210">dĭ-Tə</ta>
            <ta e="T212" id="Seg_7152" s="T211">kereʔ</ta>
            <ta e="T213" id="Seg_7153" s="T212">bazoʔ</ta>
            <ta e="T912" id="Seg_7154" s="T213">kan-lAʔ</ta>
            <ta e="T214" id="Seg_7155" s="T912">tʼür-bi</ta>
            <ta e="T215" id="Seg_7156" s="T214">ara</ta>
            <ta e="T216" id="Seg_7157" s="T215">bĭs-zittə</ta>
            <ta e="T217" id="Seg_7158" s="T216">ato</ta>
            <ta e="T218" id="Seg_7159" s="T217">dĭ-Tə</ta>
            <ta e="T219" id="Seg_7160" s="T218">amka</ta>
            <ta e="T220" id="Seg_7161" s="T219">kabarləj</ta>
            <ta e="T222" id="Seg_7162" s="T221">šiʔ</ta>
            <ta e="T223" id="Seg_7163" s="T222">dʼije-Kən</ta>
            <ta e="T224" id="Seg_7164" s="T223">i-bi-lAʔ</ta>
            <ta e="T225" id="Seg_7165" s="T224">gijen</ta>
            <ta e="T226" id="Seg_7166" s="T225">kunol-bi-lAʔ</ta>
            <ta e="T227" id="Seg_7167" s="T226">kunol-bi-bAʔ</ta>
            <ta e="T228" id="Seg_7168" s="T227">pălatka-Kən</ta>
            <ta e="T229" id="Seg_7169" s="T228">urgaːba</ta>
            <ta e="T230" id="Seg_7170" s="T229">šo-bi</ta>
            <ta e="T231" id="Seg_7171" s="T230">bar</ta>
            <ta e="T232" id="Seg_7172" s="T231">mĭn-bi</ta>
            <ta e="T233" id="Seg_7173" s="T232">dön</ta>
            <ta e="T235" id="Seg_7174" s="T234">miʔ</ta>
            <ta e="T236" id="Seg_7175" s="T235">ugaːndə</ta>
            <ta e="T237" id="Seg_7176" s="T236">pim-bi-bAʔ</ta>
            <ta e="T238" id="Seg_7177" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_7178" s="T238">tože</ta>
            <ta e="T240" id="Seg_7179" s="T239">pim-liA</ta>
            <ta e="T241" id="Seg_7180" s="T240">multuk-də</ta>
            <ta e="T244" id="Seg_7181" s="T243">eje-m</ta>
            <ta e="T245" id="Seg_7182" s="T244">nadə</ta>
            <ta e="T246" id="Seg_7183" s="T245">surar-zittə</ta>
            <ta e="T247" id="Seg_7184" s="T246">iʔbə-zittə</ta>
            <ta e="T248" id="Seg_7185" s="T247">kunol-zittə</ta>
            <ta e="T249" id="Seg_7186" s="T248">kabarləj</ta>
            <ta e="T251" id="Seg_7187" s="T910">lunʼka-m</ta>
            <ta e="T252" id="Seg_7188" s="T251">bar</ta>
            <ta e="T254" id="Seg_7189" s="T252">monoʔko-laʔbə</ta>
            <ta e="T255" id="Seg_7190" s="T254">bar</ta>
            <ta e="T256" id="Seg_7191" s="T255">pünö-r-laʔbə</ta>
            <ta e="T257" id="Seg_7192" s="T256">bar</ta>
            <ta e="T258" id="Seg_7193" s="T257">münör-laʔbə</ta>
            <ta e="T259" id="Seg_7194" s="T258">bar</ta>
            <ta e="T260" id="Seg_7195" s="T259">burer-laʔbə</ta>
            <ta e="T261" id="Seg_7196" s="T260">daška</ta>
            <ta e="T262" id="Seg_7197" s="T261">bar</ta>
            <ta e="T265" id="Seg_7198" s="T264">ĭmbi</ta>
            <ta e="T266" id="Seg_7199" s="T265">pim-liA-l</ta>
            <ta e="T267" id="Seg_7200" s="T266">kan-ə-ʔ</ta>
            <ta e="T268" id="Seg_7201" s="T267">unʼə</ta>
            <ta e="T269" id="Seg_7202" s="T268">măn</ta>
            <ta e="T270" id="Seg_7203" s="T269">unʼə</ta>
            <ta e="T271" id="Seg_7204" s="T270">mĭn-bi-m</ta>
            <ta e="T272" id="Seg_7205" s="T271">šində-Tə=də</ta>
            <ta e="T274" id="Seg_7206" s="T273">ej</ta>
            <ta e="T275" id="Seg_7207" s="T274">pim-liA-m</ta>
            <ta e="T276" id="Seg_7208" s="T275">a</ta>
            <ta e="T277" id="Seg_7209" s="T276">tăn</ta>
            <ta e="T278" id="Seg_7210" s="T277">üge</ta>
            <ta e="T279" id="Seg_7211" s="T278">pim-laʔbə-l</ta>
            <ta e="T280" id="Seg_7212" s="T279">nöməl-lAʔ-bi-m</ta>
            <ta e="T283" id="Seg_7213" s="T282">dĭ-zAŋ</ta>
            <ta e="T284" id="Seg_7214" s="T283">ej</ta>
            <ta e="T285" id="Seg_7215" s="T284">tĭmne-jəʔ</ta>
            <ta e="T286" id="Seg_7216" s="T285">tʼăbaktər-zittə</ta>
            <ta e="T287" id="Seg_7217" s="T286">a</ta>
            <ta e="T288" id="Seg_7218" s="T287">măn</ta>
            <ta e="T289" id="Seg_7219" s="T288">bar</ta>
            <ta e="T290" id="Seg_7220" s="T289">tʼăbaktər-laʔbə-m</ta>
            <ta e="T291" id="Seg_7221" s="T290">ija-m</ta>
            <ta e="T292" id="Seg_7222" s="T291">surar-bi-m</ta>
            <ta e="T293" id="Seg_7223" s="T292">tʼăbaktər-zittə</ta>
            <ta e="T294" id="Seg_7224" s="T293">axota</ta>
            <ta e="T295" id="Seg_7225" s="T294">măna</ta>
            <ta e="T296" id="Seg_7226" s="T295">bar</ta>
            <ta e="T298" id="Seg_7227" s="T297">dĭn</ta>
            <ta e="T299" id="Seg_7228" s="T298">dön</ta>
            <ta e="T300" id="Seg_7229" s="T299">bar</ta>
            <ta e="T301" id="Seg_7230" s="T300">dʼirək-jəʔ</ta>
            <ta e="T302" id="Seg_7231" s="T301">naga</ta>
            <ta e="T303" id="Seg_7232" s="T302">a</ta>
            <ta e="T305" id="Seg_7233" s="T304">onʼiʔ</ta>
            <ta e="T306" id="Seg_7234" s="T305">nu-zAŋ</ta>
            <ta e="T307" id="Seg_7235" s="T306">amno-laʔbə-jəʔ</ta>
            <ta e="T308" id="Seg_7236" s="T307">dĭ-zAŋ</ta>
            <ta e="T309" id="Seg_7237" s="T308">bar</ta>
            <ta e="T310" id="Seg_7238" s="T309">dʼirək-jəʔ</ta>
            <ta e="T311" id="Seg_7239" s="T310">pim-liA-jəʔ</ta>
            <ta e="T312" id="Seg_7240" s="T311">bar</ta>
            <ta e="T314" id="Seg_7241" s="T313">ej</ta>
            <ta e="T315" id="Seg_7242" s="T314">tʼăbaktər-lV-m</ta>
            <ta e="T316" id="Seg_7243" s="T315">tüjö</ta>
            <ta e="T317" id="Seg_7244" s="T316">băra</ta>
            <ta e="T318" id="Seg_7245" s="T317">det-lV-m</ta>
            <ta e="T319" id="Seg_7246" s="T318">tăn</ta>
            <ta e="T320" id="Seg_7247" s="T319">dʼabə-lV-l</ta>
            <ta e="T321" id="Seg_7248" s="T320">măn</ta>
            <ta e="T322" id="Seg_7249" s="T321">toltano</ta>
            <ta e="T323" id="Seg_7250" s="T322">kămnə-bi-m</ta>
            <ta e="T324" id="Seg_7251" s="T323">amno-KAʔ</ta>
            <ta e="T325" id="Seg_7252" s="T324">măn</ta>
            <ta e="T326" id="Seg_7253" s="T325">ĭššo</ta>
            <ta e="T327" id="Seg_7254" s="T326">šide</ta>
            <ta e="T328" id="Seg_7255" s="T327">aspaʔ</ta>
            <ta e="T329" id="Seg_7256" s="T328">det-lV-m</ta>
            <ta e="T330" id="Seg_7257" s="T329">păʔ-lV-m</ta>
            <ta e="T331" id="Seg_7258" s="T330">dĭgəttə</ta>
            <ta e="T332" id="Seg_7259" s="T331">tʼăbaktər-zittə</ta>
            <ta e="T333" id="Seg_7260" s="T332">možna</ta>
            <ta e="T334" id="Seg_7261" s="T333">bar</ta>
            <ta e="T335" id="Seg_7262" s="T334">ej</ta>
            <ta e="T336" id="Seg_7263" s="T335">tʼăbaktər-liA-m</ta>
            <ta e="T338" id="Seg_7264" s="T337">teinen</ta>
            <ta e="T339" id="Seg_7265" s="T338">ugaːndə</ta>
            <ta e="T340" id="Seg_7266" s="T339">dʼibige</ta>
            <ta e="T341" id="Seg_7267" s="T340">tʼala</ta>
            <ta e="T342" id="Seg_7268" s="T341">bar</ta>
            <ta e="T343" id="Seg_7269" s="T342">ipek</ta>
            <ta e="T344" id="Seg_7270" s="T343">nend-lV-j</ta>
            <ta e="T346" id="Seg_7271" s="T345">ipek</ta>
            <ta e="T347" id="Seg_7272" s="T346">ej</ta>
            <ta e="T348" id="Seg_7273" s="T347">özer-lV-j</ta>
            <ta e="T349" id="Seg_7274" s="T348">dĭgəttə</ta>
            <ta e="T350" id="Seg_7275" s="T349">naga</ta>
            <ta e="T351" id="Seg_7276" s="T350">ipek</ta>
            <ta e="T352" id="Seg_7277" s="T351">ĭmbi</ta>
            <ta e="T353" id="Seg_7278" s="T352">am-zittə</ta>
            <ta e="T354" id="Seg_7279" s="T353">kü-laːm-bi-m</ta>
            <ta e="T356" id="Seg_7280" s="T355">kü-zittə</ta>
            <ta e="T357" id="Seg_7281" s="T356">možna</ta>
            <ta e="T358" id="Seg_7282" s="T357">ipek-ziʔ</ta>
            <ta e="T359" id="Seg_7283" s="T358">naga</ta>
            <ta e="T360" id="Seg_7284" s="T359">bar</ta>
            <ta e="T361" id="Seg_7285" s="T360">i-bə</ta>
            <ta e="T362" id="Seg_7286" s="T361">ej</ta>
            <ta e="T363" id="Seg_7287" s="T362">tʼăbaktər-lV-m</ta>
            <ta e="T365" id="Seg_7288" s="T364">tăn</ta>
            <ta e="T366" id="Seg_7289" s="T365">ugaːndə</ta>
            <ta e="T367" id="Seg_7290" s="T366">sagər</ta>
            <ta e="T368" id="Seg_7291" s="T367">bazə-jd-ə-ʔ</ta>
            <ta e="T369" id="Seg_7292" s="T368">multʼa-Tə</ta>
            <ta e="T370" id="Seg_7293" s="T369">kan-ə-ʔ</ta>
            <ta e="T371" id="Seg_7294" s="T370">sabən</ta>
            <ta e="T372" id="Seg_7295" s="T371">i-ʔ</ta>
            <ta e="T373" id="Seg_7296" s="T372">dĭgəttə</ta>
            <ta e="T374" id="Seg_7297" s="T373">kujnek</ta>
            <ta e="T375" id="Seg_7298" s="T374">i-ʔ</ta>
            <ta e="T376" id="Seg_7299" s="T375">šer-t</ta>
            <ta e="T377" id="Seg_7300" s="T376">kabarləj</ta>
            <ta e="T379" id="Seg_7301" s="T378">ugaːndə</ta>
            <ta e="T380" id="Seg_7302" s="T379">kuvas</ta>
            <ta e="T381" id="Seg_7303" s="T380">dĭgəttə</ta>
            <ta e="T382" id="Seg_7304" s="T381">mo-lV-l</ta>
            <ta e="T383" id="Seg_7305" s="T382">kujnek</ta>
            <ta e="T384" id="Seg_7306" s="T383">šer-liA-l</ta>
            <ta e="T385" id="Seg_7307" s="T384">tak</ta>
            <ta e="T386" id="Seg_7308" s="T385">bar</ta>
            <ta e="T387" id="Seg_7309" s="T386">koʔbdo-zAŋ</ta>
            <ta e="T388" id="Seg_7310" s="T387">tănan</ta>
            <ta e="T391" id="Seg_7311" s="T390">panar-lV-jəʔ</ta>
            <ta e="T392" id="Seg_7312" s="T391">bar</ta>
            <ta e="T393" id="Seg_7313" s="T392">kuvas</ta>
            <ta e="T394" id="Seg_7314" s="T393">kabarləj</ta>
            <ta e="T396" id="Seg_7315" s="T395">dĭ-zAŋ</ta>
            <ta e="T397" id="Seg_7316" s="T396">Krasnojarskə-jəʔ</ta>
            <ta e="T398" id="Seg_7317" s="T397">kan-bi-jəʔ</ta>
            <ta e="T399" id="Seg_7318" s="T398">măna</ta>
            <ta e="T400" id="Seg_7319" s="T399">ej</ta>
            <ta e="T401" id="Seg_7320" s="T400">nörbə-jəʔ</ta>
            <ta e="T402" id="Seg_7321" s="T401">măn</ta>
            <ta e="T403" id="Seg_7322" s="T402">măn</ta>
            <ta e="T404" id="Seg_7323" s="T403">dĭn</ta>
            <ta e="T405" id="Seg_7324" s="T404">tugan-m</ta>
            <ta e="T406" id="Seg_7325" s="T405">i-gA</ta>
            <ta e="T407" id="Seg_7326" s="T406">măn</ta>
            <ta e="T408" id="Seg_7327" s="T407">bɨ</ta>
            <ta e="T409" id="Seg_7328" s="T408">noʔ</ta>
            <ta e="T410" id="Seg_7329" s="T409">mĭ-bi-m</ta>
            <ta e="T411" id="Seg_7330" s="T410">dĭ-n</ta>
            <ta e="T412" id="Seg_7331" s="T411">dĭ-n</ta>
            <ta e="T413" id="Seg_7332" s="T412">sĭj-də</ta>
            <ta e="T414" id="Seg_7333" s="T413">ĭzem-liA</ta>
            <ta e="T415" id="Seg_7334" s="T414">dĭ-zAŋ</ta>
            <ta e="T416" id="Seg_7335" s="T415">bar</ta>
            <ta e="T417" id="Seg_7336" s="T416">ku-bi-jəʔ</ta>
            <ta e="T418" id="Seg_7337" s="T417">bɨ</ta>
            <ta e="T419" id="Seg_7338" s="T418">dĭ-m</ta>
            <ta e="T421" id="Seg_7339" s="T420">bar</ta>
            <ta e="T422" id="Seg_7340" s="T421">daška</ta>
            <ta e="T423" id="Seg_7341" s="T422">ej</ta>
            <ta e="T424" id="Seg_7342" s="T423">tʼăbaktər-lV-m</ta>
            <ta e="T426" id="Seg_7343" s="T425">sagər</ta>
            <ta e="T427" id="Seg_7344" s="T426">ne</ta>
            <ta e="T428" id="Seg_7345" s="T427">šo-bi</ta>
            <ta e="T429" id="Seg_7346" s="T428">sĭri</ta>
            <ta e="T430" id="Seg_7347" s="T429">kuza</ta>
            <ta e="T431" id="Seg_7348" s="T430">măn-ntə</ta>
            <ta e="T432" id="Seg_7349" s="T431">i-zittə</ta>
            <ta e="T433" id="Seg_7350" s="T432">nadə</ta>
            <ta e="T434" id="Seg_7351" s="T433">kan-žə-bəj</ta>
            <ta e="T435" id="Seg_7352" s="T434">obberəj</ta>
            <ta e="T436" id="Seg_7353" s="T435">aʔtʼi-Kən</ta>
            <ta e="T437" id="Seg_7354" s="T436">bar</ta>
            <ta e="T439" id="Seg_7355" s="T438">kunol-žə-bəj</ta>
            <ta e="T440" id="Seg_7356" s="T439">bar</ta>
            <ta e="T441" id="Seg_7357" s="T440">panar-žə-bəj</ta>
            <ta e="T442" id="Seg_7358" s="T441">bar</ta>
            <ta e="T443" id="Seg_7359" s="T442">tibi-l</ta>
            <ta e="T444" id="Seg_7360" s="T443">bar</ta>
            <ta e="T445" id="Seg_7361" s="T444">münör-lV-j</ta>
            <ta e="T446" id="Seg_7362" s="T445">tănan</ta>
            <ta e="T447" id="Seg_7363" s="T446">kabarləj</ta>
            <ta e="T448" id="Seg_7364" s="T447">daška</ta>
            <ta e="T449" id="Seg_7365" s="T448">ej</ta>
            <ta e="T450" id="Seg_7366" s="T449">tʼăbaktər-lV-m</ta>
            <ta e="T452" id="Seg_7367" s="T451">nadə</ta>
            <ta e="T453" id="Seg_7368" s="T452">măna</ta>
            <ta e="T454" id="Seg_7369" s="T453">ešši</ta>
            <ta e="T455" id="Seg_7370" s="T454">măn</ta>
            <ta e="T456" id="Seg_7371" s="T455">băzəj-zittə</ta>
            <ta e="T457" id="Seg_7372" s="T456">dĭ</ta>
            <ta e="T458" id="Seg_7373" s="T457">ugaːndə</ta>
            <ta e="T459" id="Seg_7374" s="T458">sagər</ta>
            <ta e="T460" id="Seg_7375" s="T459">bar</ta>
            <ta e="T461" id="Seg_7376" s="T460">tüʔ-bi</ta>
            <ta e="T462" id="Seg_7377" s="T461">iʔgö</ta>
            <ta e="T464" id="Seg_7378" s="T463">bar</ta>
            <ta e="T465" id="Seg_7379" s="T464">kĭškə-bi-m</ta>
            <ta e="T466" id="Seg_7380" s="T465">dĭ</ta>
            <ta e="T467" id="Seg_7381" s="T466">koː-laːm-bi</ta>
            <ta e="T468" id="Seg_7382" s="T467">dĭgəttə</ta>
            <ta e="T469" id="Seg_7383" s="T468">nadə</ta>
            <ta e="T470" id="Seg_7384" s="T469">kür-zittə</ta>
            <ta e="T471" id="Seg_7385" s="T470">dĭ-m</ta>
            <ta e="T472" id="Seg_7386" s="T471">dĭgəttə</ta>
            <ta e="T475" id="Seg_7387" s="T474">amor-zittə</ta>
            <ta e="T476" id="Seg_7388" s="T475">nadə</ta>
            <ta e="T477" id="Seg_7389" s="T476">dĭgəttə</ta>
            <ta e="T479" id="Seg_7390" s="T478">kunol-zittə</ta>
            <ta e="T480" id="Seg_7391" s="T479">or-t</ta>
            <ta e="T481" id="Seg_7392" s="T480">ešši-m</ta>
            <ta e="T486" id="Seg_7393" s="T485">teinen</ta>
            <ta e="T487" id="Seg_7394" s="T486">urgo</ta>
            <ta e="T488" id="Seg_7395" s="T487">tʼala</ta>
            <ta e="T489" id="Seg_7396" s="T488">a</ta>
            <ta e="T490" id="Seg_7397" s="T489">il</ta>
            <ta e="T491" id="Seg_7398" s="T490">üge</ta>
            <ta e="T492" id="Seg_7399" s="T491">togonər-laʔbə-jəʔ</ta>
            <ta e="T493" id="Seg_7400" s="T492">toltano</ta>
            <ta e="T494" id="Seg_7401" s="T493">am-laʔbə-jəʔ</ta>
            <ta e="T495" id="Seg_7402" s="T494">tajər-laʔbə-jəʔ</ta>
            <ta e="T496" id="Seg_7403" s="T495">a</ta>
            <ta e="T497" id="Seg_7404" s="T496">miʔ</ta>
            <ta e="T498" id="Seg_7405" s="T497">amno-laʔbə-bAʔ</ta>
            <ta e="T499" id="Seg_7406" s="T498">kabarləj</ta>
            <ta e="T500" id="Seg_7407" s="T499">daška</ta>
            <ta e="T501" id="Seg_7408" s="T500">ej</ta>
            <ta e="T502" id="Seg_7409" s="T501">tʼăbaktər-zittə</ta>
            <ta e="T503" id="Seg_7410" s="T502">nʼe</ta>
            <ta e="T504" id="Seg_7411" s="T503">axota</ta>
            <ta e="T506" id="Seg_7412" s="T505">teinen</ta>
            <ta e="T507" id="Seg_7413" s="T506">urgo</ta>
            <ta e="T508" id="Seg_7414" s="T507">tʼala</ta>
            <ta e="T509" id="Seg_7415" s="T508">măna</ta>
            <ta e="T510" id="Seg_7416" s="T509">tugan-m</ta>
            <ta e="T511" id="Seg_7417" s="T510">kăštə-bi-jəʔ</ta>
            <ta e="T512" id="Seg_7418" s="T511">kan-žə-bəj</ta>
            <ta e="T513" id="Seg_7419" s="T512">dĭn</ta>
            <ta e="T514" id="Seg_7420" s="T513">ara</ta>
            <ta e="T515" id="Seg_7421" s="T514">bĭs-lV-bəj</ta>
            <ta e="T516" id="Seg_7422" s="T515">dĭn</ta>
            <ta e="T517" id="Seg_7423" s="T516">sʼar-laʔbə-jəʔ</ta>
            <ta e="T518" id="Seg_7424" s="T517">garmonʼ-Tə</ta>
            <ta e="T520" id="Seg_7425" s="T519">miʔ</ta>
            <ta e="T521" id="Seg_7426" s="T520">bar</ta>
            <ta e="T522" id="Seg_7427" s="T521">süʔmə-laʔbə-bAʔ</ta>
            <ta e="T523" id="Seg_7428" s="T522">bar</ta>
            <ta e="T525" id="Seg_7429" s="T524">oʔb</ta>
            <ta e="T526" id="Seg_7430" s="T525">oʔb</ta>
            <ta e="T527" id="Seg_7431" s="T526">šide</ta>
            <ta e="T528" id="Seg_7432" s="T527">nagur</ta>
            <ta e="T529" id="Seg_7433" s="T528">teʔdə</ta>
            <ta e="T530" id="Seg_7434" s="T529">sumna</ta>
            <ta e="T531" id="Seg_7435" s="T530">sejʔpü</ta>
            <ta e="T532" id="Seg_7436" s="T531">amitun</ta>
            <ta e="T533" id="Seg_7437" s="T532">šĭnteʔtə</ta>
            <ta e="T541" id="Seg_7438" s="T540">biəʔ</ta>
            <ta e="T543" id="Seg_7439" s="T542">biəʔ</ta>
            <ta e="T544" id="Seg_7440" s="T543">onʼiʔ</ta>
            <ta e="T545" id="Seg_7441" s="T544">biəʔ</ta>
            <ta e="T546" id="Seg_7442" s="T545">šide</ta>
            <ta e="T547" id="Seg_7443" s="T546">biəʔ</ta>
            <ta e="T548" id="Seg_7444" s="T547">sumna</ta>
            <ta e="T549" id="Seg_7445" s="T548">biəʔ</ta>
            <ta e="T550" id="Seg_7446" s="T549">muktuʔ</ta>
            <ta e="T552" id="Seg_7447" s="T551">măn</ta>
            <ta e="T553" id="Seg_7448" s="T552">unʼə</ta>
            <ta e="T554" id="Seg_7449" s="T553">šo-bi-m</ta>
            <ta e="T555" id="Seg_7450" s="T554">a</ta>
            <ta e="T556" id="Seg_7451" s="T555">dĭ-zAŋ</ta>
            <ta e="T557" id="Seg_7452" s="T556">bar</ta>
            <ta e="T558" id="Seg_7453" s="T557">sumna</ta>
            <ta e="T559" id="Seg_7454" s="T558">šo-bi-jəʔ</ta>
            <ta e="T561" id="Seg_7455" s="T560">măn</ta>
            <ta e="T562" id="Seg_7456" s="T561">mĭje</ta>
            <ta e="T564" id="Seg_7457" s="T563">mĭnzər-liA-m</ta>
            <ta e="T565" id="Seg_7458" s="T564">ugaːndə</ta>
            <ta e="T566" id="Seg_7459" s="T565">jakšə</ta>
            <ta e="T567" id="Seg_7460" s="T566">mĭje</ta>
            <ta e="T568" id="Seg_7461" s="T567">nʼamga</ta>
            <ta e="T569" id="Seg_7462" s="T568">uja-ziʔ</ta>
            <ta e="T570" id="Seg_7463" s="T569">köbergən</ta>
            <ta e="T571" id="Seg_7464" s="T570">hen-bi-m</ta>
            <ta e="T572" id="Seg_7465" s="T571">bar</ta>
            <ta e="T573" id="Seg_7466" s="T572">tus</ta>
            <ta e="T574" id="Seg_7467" s="T573">hen-bi-m</ta>
            <ta e="T575" id="Seg_7468" s="T574">toltano</ta>
            <ta e="T576" id="Seg_7469" s="T575">am-bi-m</ta>
            <ta e="T577" id="Seg_7470" s="T576">amnə-ʔ</ta>
            <ta e="T578" id="Seg_7471" s="T577">amor-ə-ʔ</ta>
            <ta e="T579" id="Seg_7472" s="T578">obberəj</ta>
            <ta e="T580" id="Seg_7473" s="T579">kabarləj</ta>
            <ta e="T582" id="Seg_7474" s="T581">ugaːndə</ta>
            <ta e="T583" id="Seg_7475" s="T582">kuštü</ta>
            <ta e="T584" id="Seg_7476" s="T583">kuza</ta>
            <ta e="T585" id="Seg_7477" s="T584">măn</ta>
            <ta e="T586" id="Seg_7478" s="T585">dĭ-Tə</ta>
            <ta e="T587" id="Seg_7479" s="T586">pim-liA-m</ta>
            <ta e="T588" id="Seg_7480" s="T587">dĭ</ta>
            <ta e="T589" id="Seg_7481" s="T588">bar</ta>
            <ta e="T590" id="Seg_7482" s="T589">kut-lV-j</ta>
            <ta e="T591" id="Seg_7483" s="T590">măn</ta>
            <ta e="T592" id="Seg_7484" s="T591">šaʔ-laːm-bi-m</ta>
            <ta e="T593" id="Seg_7485" s="T592">štobɨ</ta>
            <ta e="T594" id="Seg_7486" s="T593">dĭ</ta>
            <ta e="T595" id="Seg_7487" s="T594">măna</ta>
            <ta e="T596" id="Seg_7488" s="T595">ej</ta>
            <ta e="T597" id="Seg_7489" s="T596">ku-bi</ta>
            <ta e="T598" id="Seg_7490" s="T597">măn</ta>
            <ta e="T599" id="Seg_7491" s="T598">dʼije-Tə</ta>
            <ta e="T600" id="Seg_7492" s="T599">nuʔmə-luʔbdə-lV-m</ta>
            <ta e="T601" id="Seg_7493" s="T600">dĭ</ta>
            <ta e="T602" id="Seg_7494" s="T601">măna</ta>
            <ta e="T603" id="Seg_7495" s="T602">ej</ta>
            <ta e="T604" id="Seg_7496" s="T603">dĭn</ta>
            <ta e="T605" id="Seg_7497" s="T604">ej</ta>
            <ta e="T606" id="Seg_7498" s="T605">ku-lV-j</ta>
            <ta e="T607" id="Seg_7499" s="T606">kabarləj</ta>
            <ta e="T609" id="Seg_7500" s="T608">tibi-zAŋ</ta>
            <ta e="T610" id="Seg_7501" s="T609">iʔgö</ta>
            <ta e="T611" id="Seg_7502" s="T610">oʔbdə-bi-jəʔ</ta>
            <ta e="T612" id="Seg_7503" s="T611">ĭmbi=də</ta>
            <ta e="T613" id="Seg_7504" s="T612">bar</ta>
            <ta e="T614" id="Seg_7505" s="T613">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T615" id="Seg_7506" s="T614">nadə</ta>
            <ta e="T616" id="Seg_7507" s="T615">kan-zittə</ta>
            <ta e="T617" id="Seg_7508" s="T616">nʼilgö-zittə</ta>
            <ta e="T619" id="Seg_7509" s="T618">ĭmbi</ta>
            <ta e="T620" id="Seg_7510" s="T619">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T621" id="Seg_7511" s="T620">măn</ta>
            <ta e="T622" id="Seg_7512" s="T621">dĭgəttə</ta>
            <ta e="T625" id="Seg_7513" s="T624">tĭm-lV-m</ta>
            <ta e="T626" id="Seg_7514" s="T625">bar</ta>
            <ta e="T627" id="Seg_7515" s="T626">daška</ta>
            <ta e="T628" id="Seg_7516" s="T627">ej</ta>
            <ta e="T629" id="Seg_7517" s="T628">tʼăbaktər-liA-m</ta>
            <ta e="T635" id="Seg_7518" s="T634">dĭ</ta>
            <ta e="T636" id="Seg_7519" s="T635">kuza</ta>
            <ta e="T637" id="Seg_7520" s="T636">bar</ta>
            <ta e="T638" id="Seg_7521" s="T637">jezerik</ta>
            <ta e="T639" id="Seg_7522" s="T638">ine-Kən</ta>
            <ta e="T640" id="Seg_7523" s="T639">amnə-luʔbdə-bi</ta>
            <ta e="T641" id="Seg_7524" s="T640">dĭgəttə</ta>
            <ta e="T642" id="Seg_7525" s="T641">üzə-bi</ta>
            <ta e="T643" id="Seg_7526" s="T642">tʼo-Tə</ta>
            <ta e="T644" id="Seg_7527" s="T643">dĭgəttə</ta>
            <ta e="T645" id="Seg_7528" s="T644">ine-t</ta>
            <ta e="T646" id="Seg_7529" s="T645">nuʔmə-luʔbdə-bi</ta>
            <ta e="T647" id="Seg_7530" s="T646">üžü</ta>
            <ta e="T648" id="Seg_7531" s="T647">üžü</ta>
            <ta e="T650" id="Seg_7532" s="T649">üžü</ta>
            <ta e="T651" id="Seg_7533" s="T650">dʼürdə-bi</ta>
            <ta e="T652" id="Seg_7534" s="T651">ma-luʔbdə-bi</ta>
            <ta e="T653" id="Seg_7535" s="T652">bar</ta>
            <ta e="T654" id="Seg_7536" s="T653">kunol-laʔbə</ta>
            <ta e="T655" id="Seg_7537" s="T654">kunol-lAʔ</ta>
            <ta e="T656" id="Seg_7538" s="T655">iʔbö-laʔbə</ta>
            <ta e="T657" id="Seg_7539" s="T656">saʔmə-luʔbdə-bi</ta>
            <ta e="T658" id="Seg_7540" s="T657">bar</ta>
            <ta e="T659" id="Seg_7541" s="T658">kabarləj</ta>
            <ta e="T661" id="Seg_7542" s="T660">nʼi-zAŋ</ta>
            <ta e="T662" id="Seg_7543" s="T661">koʔbdo-zAŋ</ta>
            <ta e="T663" id="Seg_7544" s="T662">bar</ta>
            <ta e="T664" id="Seg_7545" s="T663">iʔgö</ta>
            <ta e="T665" id="Seg_7546" s="T664">oʔbdə-bi-jəʔ</ta>
            <ta e="T666" id="Seg_7547" s="T665">bar</ta>
            <ta e="T667" id="Seg_7548" s="T666">nüj-laʔbə-jəʔ</ta>
            <ta e="T668" id="Seg_7549" s="T667">garmonʼ</ta>
            <ta e="T669" id="Seg_7550" s="T668">sʼar-laʔbə-jəʔ</ta>
            <ta e="T670" id="Seg_7551" s="T669">süʔmə-laʔbə-jəʔ</ta>
            <ta e="T671" id="Seg_7552" s="T670">kabarləj</ta>
            <ta e="T673" id="Seg_7553" s="T672">măn</ta>
            <ta e="T674" id="Seg_7554" s="T673">dʼije-Tə</ta>
            <ta e="T675" id="Seg_7555" s="T674">mĭn-bi-m</ta>
            <ta e="T676" id="Seg_7556" s="T675">keʔbde</ta>
            <ta e="T677" id="Seg_7557" s="T676">nĭŋgə-bi-m</ta>
            <ta e="T678" id="Seg_7558" s="T677">kalba</ta>
            <ta e="T679" id="Seg_7559" s="T678">nĭŋgə-bi-m</ta>
            <ta e="T680" id="Seg_7560" s="T679">pirog-jəʔ</ta>
            <ta e="T681" id="Seg_7561" s="T680">pür-bi-m</ta>
            <ta e="T682" id="Seg_7562" s="T681">šiʔnʼileʔ</ta>
            <ta e="T683" id="Seg_7563" s="T682">mĭ-lV-m</ta>
            <ta e="T684" id="Seg_7564" s="T683">kabarləj</ta>
            <ta e="T686" id="Seg_7565" s="T685">măn</ta>
            <ta e="T687" id="Seg_7566" s="T686">ija-m</ta>
            <ta e="T688" id="Seg_7567" s="T687">nu</ta>
            <ta e="T689" id="Seg_7568" s="T688">i-bi</ta>
            <ta e="T690" id="Seg_7569" s="T689">aba-m</ta>
            <ta e="T691" id="Seg_7570" s="T690">dʼirək</ta>
            <ta e="T692" id="Seg_7571" s="T691">i-bi</ta>
            <ta e="T693" id="Seg_7572" s="T692">šide</ta>
            <ta e="T694" id="Seg_7573" s="T693">nʼi</ta>
            <ta e="T695" id="Seg_7574" s="T694">i-bi-jəʔ</ta>
            <ta e="T699" id="Seg_7575" s="T698">muktuʔ</ta>
            <ta e="T701" id="Seg_7576" s="T700">i-bi</ta>
            <ta e="T702" id="Seg_7577" s="T701">bar</ta>
            <ta e="T703" id="Seg_7578" s="T702">kabarləj</ta>
            <ta e="T705" id="Seg_7579" s="T704">nagur</ta>
            <ta e="T706" id="Seg_7580" s="T705">kü-laːm-bi-jəʔ</ta>
            <ta e="T707" id="Seg_7581" s="T706">a</ta>
            <ta e="T708" id="Seg_7582" s="T707">nagur</ta>
            <ta e="T709" id="Seg_7583" s="T708">ej</ta>
            <ta e="T710" id="Seg_7584" s="T709">kü-bi-jəʔ</ta>
            <ta e="T711" id="Seg_7585" s="T710">măn</ta>
            <ta e="T712" id="Seg_7586" s="T711">aba-m</ta>
            <ta e="T714" id="Seg_7587" s="T712">albuga</ta>
            <ta e="T720" id="Seg_7588" s="T719">biəʔ</ta>
            <ta e="T721" id="Seg_7589" s="T720">sumna</ta>
            <ta e="T722" id="Seg_7590" s="T721">kut-bi</ta>
            <ta e="T724" id="Seg_7591" s="T722">bar</ta>
            <ta e="T725" id="Seg_7592" s="T724">kabarləj</ta>
            <ta e="T726" id="Seg_7593" s="T725">tažəp</ta>
            <ta e="T727" id="Seg_7594" s="T726">ĭššo</ta>
            <ta e="T728" id="Seg_7595" s="T727">băra</ta>
            <ta e="T729" id="Seg_7596" s="T728">det-bi</ta>
            <ta e="T730" id="Seg_7597" s="T729">kabarləj</ta>
            <ta e="T731" id="Seg_7598" s="T730">nu</ta>
            <ta e="T732" id="Seg_7599" s="T731">kuza</ta>
            <ta e="T733" id="Seg_7600" s="T732">ĭzem-liA</ta>
            <ta e="T735" id="Seg_7601" s="T734">dĭgəttə</ta>
            <ta e="T736" id="Seg_7602" s="T735">kan-bi</ta>
            <ta e="T737" id="Seg_7603" s="T736">šaman-Tə</ta>
            <ta e="T738" id="Seg_7604" s="T737">dĭ</ta>
            <ta e="T740" id="Seg_7605" s="T739">dĭ-m</ta>
            <ta e="T741" id="Seg_7606" s="T740">bar</ta>
            <ta e="T742" id="Seg_7607" s="T741">tʼezer-laʔbə-bi</ta>
            <ta e="T743" id="Seg_7608" s="T742">daška</ta>
            <ta e="T744" id="Seg_7609" s="T743">bar</ta>
            <ta e="T745" id="Seg_7610" s="T744">naverna</ta>
            <ta e="T746" id="Seg_7611" s="T745">măn</ta>
            <ta e="T747" id="Seg_7612" s="T746">ugaːndə</ta>
            <ta e="T748" id="Seg_7613" s="T747">püjö-liA-m</ta>
            <ta e="T749" id="Seg_7614" s="T748">amnə-ʔ</ta>
            <ta e="T750" id="Seg_7615" s="T749">amor-ə-ʔ</ta>
            <ta e="T751" id="Seg_7616" s="T750">šində</ta>
            <ta e="T752" id="Seg_7617" s="T751">tănan</ta>
            <ta e="T753" id="Seg_7618" s="T752">ej</ta>
            <ta e="T754" id="Seg_7619" s="T753">mĭ-liA</ta>
            <ta e="T755" id="Seg_7620" s="T754">dĭgəttə</ta>
            <ta e="T758" id="Seg_7621" s="T757">iʔbə-ʔə-</ta>
            <ta e="T759" id="Seg_7622" s="T758">da</ta>
            <ta e="T760" id="Seg_7623" s="T759">kunol-ə-ʔ</ta>
            <ta e="T761" id="Seg_7624" s="T760">kabarləj</ta>
            <ta e="T763" id="Seg_7625" s="T762">ešši-zAŋ</ta>
            <ta e="T764" id="Seg_7626" s="T763">nʼiʔnen</ta>
            <ta e="T765" id="Seg_7627" s="T764">bar</ta>
            <ta e="T767" id="Seg_7628" s="T766">sʼar-laʔbə-jəʔ</ta>
            <ta e="T768" id="Seg_7629" s="T767">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T769" id="Seg_7630" s="T768">tʼabəro-laʔbə-jəʔ</ta>
            <ta e="T770" id="Seg_7631" s="T769">našto</ta>
            <ta e="T771" id="Seg_7632" s="T770">tʼabəro-liA-l</ta>
            <ta e="T772" id="Seg_7633" s="T771">jakšə</ta>
            <ta e="T773" id="Seg_7634" s="T772">nadə</ta>
            <ta e="T774" id="Seg_7635" s="T773">sʼar-zittə</ta>
            <ta e="T775" id="Seg_7636" s="T774">e-ʔ</ta>
            <ta e="T776" id="Seg_7637" s="T775">tʼabəro-zittə</ta>
            <ta e="T777" id="Seg_7638" s="T776">kabarləj</ta>
            <ta e="T778" id="Seg_7639" s="T777">naverna</ta>
            <ta e="T780" id="Seg_7640" s="T779">e-ʔ</ta>
            <ta e="T781" id="Seg_7641" s="T780">tʼabəro-KAʔ</ta>
            <ta e="T782" id="Seg_7642" s="T781">nadə</ta>
            <ta e="T783" id="Seg_7643" s="T782">jakšə</ta>
            <ta e="T784" id="Seg_7644" s="T783">sʼar-zittə</ta>
            <ta e="T785" id="Seg_7645" s="T784">našto</ta>
            <ta e="T786" id="Seg_7646" s="T785">tʼabəro-laʔbə-l</ta>
            <ta e="T788" id="Seg_7647" s="T787">măn</ta>
            <ta e="T789" id="Seg_7648" s="T788">teinen</ta>
            <ta e="T790" id="Seg_7649" s="T789">nu-bi-m</ta>
            <ta e="T791" id="Seg_7650" s="T790">kuja</ta>
            <ta e="T792" id="Seg_7651" s="T791">ĭššo</ta>
            <ta e="T793" id="Seg_7652" s="T792">ej</ta>
            <ta e="T794" id="Seg_7653" s="T793">nu-bi</ta>
            <ta e="T795" id="Seg_7654" s="T794">măn</ta>
            <ta e="T796" id="Seg_7655" s="T795">nu-bi-m</ta>
            <ta e="T797" id="Seg_7656" s="T796">dĭgəttə</ta>
            <ta e="T798" id="Seg_7657" s="T797">tüžöj</ta>
            <ta e="T799" id="Seg_7658" s="T798">surdo-bi-m</ta>
            <ta e="T800" id="Seg_7659" s="T799">büzəj</ta>
            <ta e="T801" id="Seg_7660" s="T800">bĭs-lə-bi-m</ta>
            <ta e="T802" id="Seg_7661" s="T801">kuriza-jəʔ</ta>
            <ta e="T806" id="Seg_7662" s="T805">bădə-laʔbə-m</ta>
            <ta e="T807" id="Seg_7663" s="T806">dĭgəttə</ta>
            <ta e="T808" id="Seg_7664" s="T807">munəj-jəʔ</ta>
            <ta e="T809" id="Seg_7665" s="T808">pür-bi-m</ta>
            <ta e="T810" id="Seg_7666" s="T809">toltano</ta>
            <ta e="T811" id="Seg_7667" s="T810">mĭnzər-bi-m</ta>
            <ta e="T812" id="Seg_7668" s="T811">amor-bi-m</ta>
            <ta e="T813" id="Seg_7669" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_7670" s="T813">nu</ta>
            <ta e="T815" id="Seg_7671" s="T814">kuza</ta>
            <ta e="T816" id="Seg_7672" s="T815">nu</ta>
            <ta e="T817" id="Seg_7673" s="T816">il</ta>
            <ta e="T818" id="Seg_7674" s="T817">šo-bi-jəʔ</ta>
            <ta e="T819" id="Seg_7675" s="T818">tʼăbaktər-bi-m</ta>
            <ta e="T821" id="Seg_7676" s="T820">bar</ta>
            <ta e="T822" id="Seg_7677" s="T821">tarar-luʔbdə-bi-m</ta>
            <ta e="T823" id="Seg_7678" s="T822">kabarləj</ta>
            <ta e="T825" id="Seg_7679" s="T824">kandə-KAʔ</ta>
            <ta e="T826" id="Seg_7680" s="T825">maʔ-Tə-l</ta>
            <ta e="T827" id="Seg_7681" s="T826">kabarləj</ta>
            <ta e="T828" id="Seg_7682" s="T827">tʼăbaktər-zittə</ta>
            <ta e="T829" id="Seg_7683" s="T828">am-KAʔ</ta>
            <ta e="T830" id="Seg_7684" s="T829">iʔbə-ə-ʔ</ta>
            <ta e="T831" id="Seg_7685" s="T830">kunol-zittə</ta>
            <ta e="T832" id="Seg_7686" s="T831">kabarləj</ta>
            <ta e="T833" id="Seg_7687" s="T832">daška</ta>
            <ta e="T835" id="Seg_7688" s="T834">šumura-Tə</ta>
            <ta e="T836" id="Seg_7689" s="T835">mĭn-bi-m</ta>
            <ta e="T837" id="Seg_7690" s="T836">ugaːndə</ta>
            <ta e="T838" id="Seg_7691" s="T837">il</ta>
            <ta e="T839" id="Seg_7692" s="T838">iʔgö</ta>
            <ta e="T840" id="Seg_7693" s="T839">ku-bi-m</ta>
            <ta e="T841" id="Seg_7694" s="T840">koŋ</ta>
            <ta e="T842" id="Seg_7695" s="T841">bar</ta>
            <ta e="T843" id="Seg_7696" s="T842">šo-bi-jəʔ</ta>
            <ta e="T844" id="Seg_7697" s="T843">tibi-zAŋ</ta>
            <ta e="T845" id="Seg_7698" s="T844">dĭn</ta>
            <ta e="T846" id="Seg_7699" s="T845">iʔgö</ta>
            <ta e="T847" id="Seg_7700" s="T846">ine-zAŋ</ta>
            <ta e="T848" id="Seg_7701" s="T847">koʔbdo-zAŋ</ta>
            <ta e="T849" id="Seg_7702" s="T848">nʼi-zAŋ</ta>
            <ta e="T850" id="Seg_7703" s="T849">bar</ta>
            <ta e="T851" id="Seg_7704" s="T850">kabarləj</ta>
            <ta e="T853" id="Seg_7705" s="T852">ĭmbi</ta>
            <ta e="T854" id="Seg_7706" s="T853">dĭ</ta>
            <ta e="T855" id="Seg_7707" s="T854">todam</ta>
            <ta e="T856" id="Seg_7708" s="T855">kuza</ta>
            <ta e="T857" id="Seg_7709" s="T856">ej</ta>
            <ta e="T858" id="Seg_7710" s="T857">šo-bi</ta>
            <ta e="T859" id="Seg_7711" s="T858">koʔbdo</ta>
            <ta e="T860" id="Seg_7712" s="T859">ej</ta>
            <ta e="T861" id="Seg_7713" s="T860">šo-bi</ta>
            <ta e="T862" id="Seg_7714" s="T861">gijen</ta>
            <ta e="T863" id="Seg_7715" s="T862">dĭ-zAŋ</ta>
            <ta e="T864" id="Seg_7716" s="T863">ĭmbi</ta>
            <ta e="T865" id="Seg_7717" s="T864">ej</ta>
            <ta e="T866" id="Seg_7718" s="T865">šo-bi-jəʔ</ta>
            <ta e="T867" id="Seg_7719" s="T866">tʼăbaktər-zittə</ta>
            <ta e="T868" id="Seg_7720" s="T867">măn-ziʔ</ta>
            <ta e="T869" id="Seg_7721" s="T868">kabarləj</ta>
            <ta e="T871" id="Seg_7722" s="T870">gijen</ta>
            <ta e="T872" id="Seg_7723" s="T871">dĭ-zAŋ</ta>
            <ta e="T873" id="Seg_7724" s="T872">ĭmbi</ta>
            <ta e="T874" id="Seg_7725" s="T873">ej</ta>
            <ta e="T875" id="Seg_7726" s="T874">šo-bi-jəʔ</ta>
            <ta e="T876" id="Seg_7727" s="T875">tʼăbaktər-zittə</ta>
            <ta e="T878" id="Seg_7728" s="T877">teinen</ta>
            <ta e="T879" id="Seg_7729" s="T878">kabarləj</ta>
            <ta e="T880" id="Seg_7730" s="T879">tʼăbaktər-zittə</ta>
            <ta e="T881" id="Seg_7731" s="T880">kan-ə-ʔ</ta>
            <ta e="T882" id="Seg_7732" s="T881">maʔ-Tə-l</ta>
            <ta e="T883" id="Seg_7733" s="T882">tăn</ta>
            <ta e="T884" id="Seg_7734" s="T883">măna</ta>
            <ta e="T886" id="Seg_7735" s="T885">dărəʔ</ta>
            <ta e="T887" id="Seg_7736" s="T886">ej</ta>
            <ta e="T891" id="Seg_7737" s="T890">tăn</ta>
            <ta e="T892" id="Seg_7738" s="T891">măna</ta>
            <ta e="T893" id="Seg_7739" s="T892">dărəʔ</ta>
            <ta e="T894" id="Seg_7740" s="T893">ej</ta>
            <ta e="T896" id="Seg_7741" s="T895">tüšəl-lə-ə-ʔ</ta>
            <ta e="T897" id="Seg_7742" s="T896">dărəʔ</ta>
            <ta e="T898" id="Seg_7743" s="T897">ej</ta>
            <ta e="T899" id="Seg_7744" s="T898">jakšə</ta>
            <ta e="T900" id="Seg_7745" s="T899">măn-zittə</ta>
            <ta e="T901" id="Seg_7746" s="T900">našto</ta>
            <ta e="T902" id="Seg_7747" s="T901">sürer-zittə</ta>
            <ta e="T903" id="Seg_7748" s="T902">il</ta>
            <ta e="T904" id="Seg_7749" s="T903">dĭ</ta>
            <ta e="T905" id="Seg_7750" s="T904">nu-zAŋ</ta>
            <ta e="T906" id="Seg_7751" s="T905">il</ta>
            <ta e="T908" id="Seg_7752" s="T907">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T16" id="Seg_7753" s="T13">heal-EP-IMP.2SG</ta>
            <ta e="T17" id="Seg_7754" s="T16">soon</ta>
            <ta e="T18" id="Seg_7755" s="T17">speak-INF.LAT</ta>
            <ta e="T19" id="Seg_7756" s="T18">one.should</ta>
            <ta e="T20" id="Seg_7757" s="T19">then</ta>
            <ta e="T21" id="Seg_7758" s="T20">tillage-NOM/GEN/ACC.3SG</ta>
            <ta e="T22" id="Seg_7759" s="T21">go-FUT-1SG</ta>
            <ta e="T23" id="Seg_7760" s="T22">earth.[NOM.SG]</ta>
            <ta e="T24" id="Seg_7761" s="T23">plough-INF.LAT</ta>
            <ta e="T25" id="Seg_7762" s="T24">then</ta>
            <ta e="T26" id="Seg_7763" s="T25">bread.[NOM.SG]</ta>
            <ta e="T27" id="Seg_7764" s="T26">kill-INF.LAT</ta>
            <ta e="T28" id="Seg_7765" s="T27">then</ta>
            <ta e="T29" id="Seg_7766" s="T28">grow-FUT-3SG</ta>
            <ta e="T30" id="Seg_7767" s="T29">big.[NOM.SG]</ta>
            <ta e="T31" id="Seg_7768" s="T30">grow-FUT-3SG</ta>
            <ta e="T32" id="Seg_7769" s="T31">PTCL</ta>
            <ta e="T33" id="Seg_7770" s="T32">white.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7771" s="T33">become-FUT-3SG</ta>
            <ta e="T35" id="Seg_7772" s="T34">then</ta>
            <ta e="T36" id="Seg_7773" s="T35">get.ready-INF.LAT</ta>
            <ta e="T37" id="Seg_7774" s="T36">one.should</ta>
            <ta e="T40" id="Seg_7775" s="T39">then</ta>
            <ta e="T41" id="Seg_7776" s="T40">jolt-MULT-INF.LAT</ta>
            <ta e="T42" id="Seg_7777" s="T41">one.should</ta>
            <ta e="T43" id="Seg_7778" s="T42">then</ta>
            <ta e="T44" id="Seg_7779" s="T43">sack-LAT</ta>
            <ta e="T45" id="Seg_7780" s="T44">pour-INF.LAT</ta>
            <ta e="T46" id="Seg_7781" s="T45">then</ta>
            <ta e="T47" id="Seg_7782" s="T46">one.should</ta>
            <ta e="T48" id="Seg_7783" s="T47">grind-INF.LAT</ta>
            <ta e="T49" id="Seg_7784" s="T48">then</ta>
            <ta e="T50" id="Seg_7785" s="T49">one.should</ta>
            <ta e="T51" id="Seg_7786" s="T50">sieve-INF.LAT</ta>
            <ta e="T52" id="Seg_7787" s="T51">then</ta>
            <ta e="T53" id="Seg_7788" s="T52">place-INF.LAT</ta>
            <ta e="T54" id="Seg_7789" s="T53">one.should</ta>
            <ta e="T55" id="Seg_7790" s="T54">then</ta>
            <ta e="T56" id="Seg_7791" s="T55">bake-INF.LAT</ta>
            <ta e="T57" id="Seg_7792" s="T56">one.should</ta>
            <ta e="T58" id="Seg_7793" s="T57">then</ta>
            <ta e="T59" id="Seg_7794" s="T58">eat-INF.LAT</ta>
            <ta e="T60" id="Seg_7795" s="T59">one.should</ta>
            <ta e="T61" id="Seg_7796" s="T60">all</ta>
            <ta e="T63" id="Seg_7797" s="T62">you.NOM</ta>
            <ta e="T64" id="Seg_7798" s="T63">you.NOM</ta>
            <ta e="T65" id="Seg_7799" s="T64">walk-DUR-2SG</ta>
            <ta e="T66" id="Seg_7800" s="T65">what.[NOM.SG]</ta>
            <ta e="T67" id="Seg_7801" s="T66">I.LAT</ta>
            <ta e="T68" id="Seg_7802" s="T67">NEG</ta>
            <ta e="T69" id="Seg_7803" s="T68">tell-PST-2SG</ta>
            <ta e="T70" id="Seg_7804" s="T69">I.NOM</ta>
            <ta e="T71" id="Seg_7805" s="T70">IRREAL</ta>
            <ta e="T72" id="Seg_7806" s="T71">grass.[NOM.SG]</ta>
            <ta e="T74" id="Seg_7807" s="T73">give-PST-1SG</ta>
            <ta e="T75" id="Seg_7808" s="T74">there</ta>
            <ta e="T76" id="Seg_7809" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_7810" s="T76">relative-PL-ACC.3SG</ta>
            <ta e="T78" id="Seg_7811" s="T77">be-PRS.[3SG]</ta>
            <ta e="T79" id="Seg_7812" s="T78">you.NOM</ta>
            <ta e="T80" id="Seg_7813" s="T79">IRREAL</ta>
            <ta e="T81" id="Seg_7814" s="T80">see-PST-2SG</ta>
            <ta e="T82" id="Seg_7815" s="T81">this-ACC</ta>
            <ta e="T83" id="Seg_7816" s="T82">enough</ta>
            <ta e="T85" id="Seg_7817" s="T84">you.NOM</ta>
            <ta e="T86" id="Seg_7818" s="T85">what.[NOM.SG]</ta>
            <ta e="T87" id="Seg_7819" s="T86">I.LAT</ta>
            <ta e="T88" id="Seg_7820" s="T87">NEG</ta>
            <ta e="T90" id="Seg_7821" s="T89">tell-PST-2SG</ta>
            <ta e="T91" id="Seg_7822" s="T90">that</ta>
            <ta e="T92" id="Seg_7823" s="T91">walk-PRS-2SG</ta>
            <ta e="T93" id="Seg_7824" s="T92">Krasnoyarsk.[NOM.SG]</ta>
            <ta e="T94" id="Seg_7825" s="T93">there</ta>
            <ta e="T95" id="Seg_7826" s="T94">I.NOM</ta>
            <ta e="T96" id="Seg_7827" s="T95">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T97" id="Seg_7828" s="T96">be-PRS.[3SG]</ta>
            <ta e="T98" id="Seg_7829" s="T97">I.NOM</ta>
            <ta e="T99" id="Seg_7830" s="T98">IRREAL</ta>
            <ta e="T100" id="Seg_7831" s="T99">grass.[NOM.SG]</ta>
            <ta e="T101" id="Seg_7832" s="T100">you.DAT</ta>
            <ta e="T102" id="Seg_7833" s="T101">give-PST-1SG</ta>
            <ta e="T103" id="Seg_7834" s="T102">this-GEN</ta>
            <ta e="T104" id="Seg_7835" s="T103">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T105" id="Seg_7836" s="T104">hurt-PST.[3SG]</ta>
            <ta e="T106" id="Seg_7837" s="T105">this.[NOM.SG]</ta>
            <ta e="T108" id="Seg_7838" s="T107">this.[3SG]</ta>
            <ta e="T109" id="Seg_7839" s="T108">IRREAL</ta>
            <ta e="T110" id="Seg_7840" s="T109">drink-PST.[3SG]</ta>
            <ta e="T111" id="Seg_7841" s="T110">you.NOM</ta>
            <ta e="T112" id="Seg_7842" s="T111">IRREAL</ta>
            <ta e="T113" id="Seg_7843" s="T112">see-PST-2SG</ta>
            <ta e="T114" id="Seg_7844" s="T113">this-ACC</ta>
            <ta e="T115" id="Seg_7845" s="T114">enough</ta>
            <ta e="T117" id="Seg_7846" s="T116">sit.down-IMP.2SG</ta>
            <ta e="T118" id="Seg_7847" s="T117">potato.[NOM.SG]</ta>
            <ta e="T119" id="Seg_7848" s="T118">eat-INF.LAT</ta>
            <ta e="T120" id="Seg_7849" s="T119">and</ta>
            <ta e="T121" id="Seg_7850" s="T120">what.kind</ta>
            <ta e="T122" id="Seg_7851" s="T121">potato.[NOM.SG]</ta>
            <ta e="T123" id="Seg_7852" s="T122">well</ta>
            <ta e="T124" id="Seg_7853" s="T123">I.NOM</ta>
            <ta e="T125" id="Seg_7854" s="T124">rub-PST-1SG</ta>
            <ta e="T126" id="Seg_7855" s="T125">then</ta>
            <ta e="T127" id="Seg_7856" s="T126">flour.[NOM.SG]</ta>
            <ta e="T128" id="Seg_7857" s="T127">take-PST.[3SG]</ta>
            <ta e="T129" id="Seg_7858" s="T128">I.NOM</ta>
            <ta e="T130" id="Seg_7859" s="T129">then</ta>
            <ta e="T131" id="Seg_7860" s="T130">sugar.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7861" s="T131">put-PST-1SG</ta>
            <ta e="T133" id="Seg_7862" s="T132">and</ta>
            <ta e="T134" id="Seg_7863" s="T133">berry.[NOM.SG]</ta>
            <ta e="T135" id="Seg_7864" s="T134">put-PST-1SG</ta>
            <ta e="T136" id="Seg_7865" s="T135">warm.[NOM.SG]</ta>
            <ta e="T138" id="Seg_7866" s="T137">water.[NOM.SG]</ta>
            <ta e="T139" id="Seg_7867" s="T138">pour-PST-1SG</ta>
            <ta e="T140" id="Seg_7868" s="T139">then</ta>
            <ta e="T141" id="Seg_7869" s="T140">boil-PST-1SG</ta>
            <ta e="T142" id="Seg_7870" s="T141">sit-IMP.2SG</ta>
            <ta e="T143" id="Seg_7871" s="T142">eat-EP-IMP.2SG</ta>
            <ta e="T145" id="Seg_7872" s="T144">boy.[NOM.SG]</ta>
            <ta e="T146" id="Seg_7873" s="T145">NEG.EX.[3SG]</ta>
            <ta e="T147" id="Seg_7874" s="T146">where.to=INDEF</ta>
            <ta e="T911" id="Seg_7875" s="T147">go-CVB</ta>
            <ta e="T148" id="Seg_7876" s="T911">disappear-PST.[3SG]</ta>
            <ta e="T150" id="Seg_7877" s="T149">look-FRQ-INF.LAT</ta>
            <ta e="T151" id="Seg_7878" s="T150">one.should</ta>
            <ta e="T152" id="Seg_7879" s="T151">maybe</ta>
            <ta e="T153" id="Seg_7880" s="T152">who.[NOM.SG]=INDEF</ta>
            <ta e="T154" id="Seg_7881" s="T153">kill-PST.[3SG]</ta>
            <ta e="T155" id="Seg_7882" s="T154">maybe</ta>
            <ta e="T156" id="Seg_7883" s="T155">bear.[NOM.SG]</ta>
            <ta e="T157" id="Seg_7884" s="T156">eat-PST.[3SG]</ta>
            <ta e="T158" id="Seg_7885" s="T157">pity-PRS-1SG</ta>
            <ta e="T159" id="Seg_7886" s="T158">PTCL</ta>
            <ta e="T160" id="Seg_7887" s="T159">very</ta>
            <ta e="T161" id="Seg_7888" s="T160">enough</ta>
            <ta e="T163" id="Seg_7889" s="T162">one.[NOM.SG]</ta>
            <ta e="T164" id="Seg_7890" s="T163">two.[NOM.SG]</ta>
            <ta e="T165" id="Seg_7891" s="T164">three.[NOM.SG]</ta>
            <ta e="T166" id="Seg_7892" s="T165">four.[NOM.SG]</ta>
            <ta e="T167" id="Seg_7893" s="T166">five.[NOM.SG]</ta>
            <ta e="T168" id="Seg_7894" s="T167">seven.[NOM.SG]</ta>
            <ta e="T169" id="Seg_7895" s="T168">eight.[NOM.SG]</ta>
            <ta e="T170" id="Seg_7896" s="T169">ten.[NOM.SG]</ta>
            <ta e="T174" id="Seg_7897" s="T173">if</ta>
            <ta e="T175" id="Seg_7898" s="T174">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T177" id="Seg_7899" s="T176">horse-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T178" id="Seg_7900" s="T177">be-PST-3PL</ta>
            <ta e="T179" id="Seg_7901" s="T178">then</ta>
            <ta e="T180" id="Seg_7902" s="T179">steal-INF.LAT</ta>
            <ta e="T181" id="Seg_7903" s="T180">one.can</ta>
            <ta e="T182" id="Seg_7904" s="T181">and</ta>
            <ta e="T183" id="Seg_7905" s="T182">potato.[NOM.SG]</ta>
            <ta e="T184" id="Seg_7906" s="T183">eat-INF.LAT</ta>
            <ta e="T185" id="Seg_7907" s="T184">otherwise</ta>
            <ta e="T186" id="Seg_7908" s="T185">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_7909" s="T186">horse-PL</ta>
            <ta e="T188" id="Seg_7910" s="T187">NEG.EX.[3SG]</ta>
            <ta e="T189" id="Seg_7911" s="T188">all</ta>
            <ta e="T191" id="Seg_7912" s="T190">I.GEN</ta>
            <ta e="T192" id="Seg_7913" s="T191">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T193" id="Seg_7914" s="T192">PTCL</ta>
            <ta e="T194" id="Seg_7915" s="T193">drunk.[NOM.SG]</ta>
            <ta e="T195" id="Seg_7916" s="T194">vodka.[NOM.SG]</ta>
            <ta e="T196" id="Seg_7917" s="T195">drink-PST.[3SG]</ta>
            <ta e="T197" id="Seg_7918" s="T196">come-PST.[3SG]</ta>
            <ta e="T198" id="Seg_7919" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_7920" s="T198">scold-DES-PST-3PL</ta>
            <ta e="T200" id="Seg_7921" s="T199">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T201" id="Seg_7922" s="T200">man-COM</ta>
            <ta e="T202" id="Seg_7923" s="T201">fight-PST-3PL</ta>
            <ta e="T203" id="Seg_7924" s="T202">PTCL</ta>
            <ta e="T204" id="Seg_7925" s="T203">very</ta>
            <ta e="T205" id="Seg_7926" s="T204">this.[NOM.SG]</ta>
            <ta e="T206" id="Seg_7927" s="T205">angry.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7928" s="T206">sundry</ta>
            <ta e="T209" id="Seg_7929" s="T208">scold-DES-DUR-PST.[3SG]</ta>
            <ta e="T210" id="Seg_7930" s="T209">how</ta>
            <ta e="T211" id="Seg_7931" s="T210">this-LAT</ta>
            <ta e="T212" id="Seg_7932" s="T211">one.needs</ta>
            <ta e="T213" id="Seg_7933" s="T212">again</ta>
            <ta e="T912" id="Seg_7934" s="T213">go-CVB</ta>
            <ta e="T214" id="Seg_7935" s="T912">disappear-PST.[3SG]</ta>
            <ta e="T215" id="Seg_7936" s="T214">vodka.[NOM.SG]</ta>
            <ta e="T216" id="Seg_7937" s="T215">drink-INF.LAT</ta>
            <ta e="T217" id="Seg_7938" s="T216">otherwise</ta>
            <ta e="T218" id="Seg_7939" s="T217">this-LAT</ta>
            <ta e="T219" id="Seg_7940" s="T218">few</ta>
            <ta e="T220" id="Seg_7941" s="T219">enough</ta>
            <ta e="T222" id="Seg_7942" s="T221">you.PL.NOM</ta>
            <ta e="T223" id="Seg_7943" s="T222">forest-LOC</ta>
            <ta e="T224" id="Seg_7944" s="T223">be-PST-2PL</ta>
            <ta e="T225" id="Seg_7945" s="T224">where</ta>
            <ta e="T226" id="Seg_7946" s="T225">sleep-PST-2PL</ta>
            <ta e="T227" id="Seg_7947" s="T226">sleep-PST-1PL</ta>
            <ta e="T228" id="Seg_7948" s="T227">tent-LOC</ta>
            <ta e="T229" id="Seg_7949" s="T228">bear.[NOM.SG]</ta>
            <ta e="T230" id="Seg_7950" s="T229">come-PST.[3SG]</ta>
            <ta e="T231" id="Seg_7951" s="T230">PTCL</ta>
            <ta e="T232" id="Seg_7952" s="T231">go-PST.[3SG]</ta>
            <ta e="T233" id="Seg_7953" s="T232">here</ta>
            <ta e="T235" id="Seg_7954" s="T234">we.NOM</ta>
            <ta e="T236" id="Seg_7955" s="T235">very</ta>
            <ta e="T237" id="Seg_7956" s="T236">fear-PST-1PL</ta>
            <ta e="T238" id="Seg_7957" s="T237">this</ta>
            <ta e="T239" id="Seg_7958" s="T238">also</ta>
            <ta e="T240" id="Seg_7959" s="T239">fear-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_7960" s="T240">gun-NOM/GEN/ACC.3SG</ta>
            <ta e="T244" id="Seg_7961" s="T243">master-ACC</ta>
            <ta e="T245" id="Seg_7962" s="T244">one.should</ta>
            <ta e="T246" id="Seg_7963" s="T245">ask-INF.LAT</ta>
            <ta e="T247" id="Seg_7964" s="T246">lie.down-INF.LAT</ta>
            <ta e="T248" id="Seg_7965" s="T247">sleep-INF.LAT</ta>
            <ta e="T249" id="Seg_7966" s="T248">enough</ta>
            <ta e="T251" id="Seg_7967" s="T910">harrier-NOM/GEN/ACC.1SG</ta>
            <ta e="T252" id="Seg_7968" s="T251">PTCL</ta>
            <ta e="T254" id="Seg_7969" s="T252">marry-DUR.[3SG]</ta>
            <ta e="T255" id="Seg_7970" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_7971" s="T255">fart-FRQ-DUR.[3SG]</ta>
            <ta e="T257" id="Seg_7972" s="T256">PTCL</ta>
            <ta e="T258" id="Seg_7973" s="T257">beat-DUR.[3SG]</ta>
            <ta e="T259" id="Seg_7974" s="T258">PTCL</ta>
            <ta e="T260" id="Seg_7975" s="T259">%%-DUR.[3SG]</ta>
            <ta e="T261" id="Seg_7976" s="T260">else</ta>
            <ta e="T262" id="Seg_7977" s="T261">all</ta>
            <ta e="T265" id="Seg_7978" s="T264">what.[NOM.SG]</ta>
            <ta e="T266" id="Seg_7979" s="T265">fear-PRS-2SG</ta>
            <ta e="T267" id="Seg_7980" s="T266">go-EP-IMP.2SG</ta>
            <ta e="T268" id="Seg_7981" s="T267">alone</ta>
            <ta e="T269" id="Seg_7982" s="T268">I.NOM</ta>
            <ta e="T270" id="Seg_7983" s="T269">alone</ta>
            <ta e="T271" id="Seg_7984" s="T270">go-PST-1SG</ta>
            <ta e="T272" id="Seg_7985" s="T271">who-LAT=INDEF</ta>
            <ta e="T274" id="Seg_7986" s="T273">NEG</ta>
            <ta e="T275" id="Seg_7987" s="T274">fear-PRS-1SG</ta>
            <ta e="T276" id="Seg_7988" s="T275">and</ta>
            <ta e="T277" id="Seg_7989" s="T276">you.NOM</ta>
            <ta e="T278" id="Seg_7990" s="T277">always</ta>
            <ta e="T279" id="Seg_7991" s="T278">fear-DUR-2SG</ta>
            <ta e="T280" id="Seg_7992" s="T279">forget-CVB-PST-1SG</ta>
            <ta e="T283" id="Seg_7993" s="T282">this-PL</ta>
            <ta e="T284" id="Seg_7994" s="T283">NEG</ta>
            <ta e="T285" id="Seg_7995" s="T284">know-3PL</ta>
            <ta e="T286" id="Seg_7996" s="T285">speak-INF.LAT</ta>
            <ta e="T287" id="Seg_7997" s="T286">and</ta>
            <ta e="T288" id="Seg_7998" s="T287">I.NOM</ta>
            <ta e="T289" id="Seg_7999" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_8000" s="T289">speak-DUR-1SG</ta>
            <ta e="T291" id="Seg_8001" s="T290">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T292" id="Seg_8002" s="T291">ask-PST-1SG</ta>
            <ta e="T293" id="Seg_8003" s="T292">speak-INF.LAT</ta>
            <ta e="T294" id="Seg_8004" s="T293">one.wants</ta>
            <ta e="T295" id="Seg_8005" s="T294">I.LAT</ta>
            <ta e="T296" id="Seg_8006" s="T295">PTCL</ta>
            <ta e="T298" id="Seg_8007" s="T297">there</ta>
            <ta e="T299" id="Seg_8008" s="T298">here</ta>
            <ta e="T300" id="Seg_8009" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_8010" s="T300">settler-PL</ta>
            <ta e="T302" id="Seg_8011" s="T301">NEG.EX.[3SG]</ta>
            <ta e="T303" id="Seg_8012" s="T302">and</ta>
            <ta e="T305" id="Seg_8013" s="T304">one.[NOM.SG]</ta>
            <ta e="T306" id="Seg_8014" s="T305">Kamassian-PL</ta>
            <ta e="T307" id="Seg_8015" s="T306">live-DUR-3PL</ta>
            <ta e="T308" id="Seg_8016" s="T307">this-PL</ta>
            <ta e="T309" id="Seg_8017" s="T308">PTCL</ta>
            <ta e="T310" id="Seg_8018" s="T309">settler-PL</ta>
            <ta e="T311" id="Seg_8019" s="T310">fear-PRS-3PL</ta>
            <ta e="T312" id="Seg_8020" s="T311">PTCL</ta>
            <ta e="T314" id="Seg_8021" s="T313">NEG</ta>
            <ta e="T315" id="Seg_8022" s="T314">speak-FUT-1SG</ta>
            <ta e="T316" id="Seg_8023" s="T315">soon</ta>
            <ta e="T317" id="Seg_8024" s="T316">sack.[NOM.SG]</ta>
            <ta e="T318" id="Seg_8025" s="T317">bring-FUT-1SG</ta>
            <ta e="T319" id="Seg_8026" s="T318">you.NOM</ta>
            <ta e="T320" id="Seg_8027" s="T319">capture-FUT-2SG</ta>
            <ta e="T321" id="Seg_8028" s="T320">I.NOM</ta>
            <ta e="T322" id="Seg_8029" s="T321">potato.[NOM.SG]</ta>
            <ta e="T323" id="Seg_8030" s="T322">pour-PST-1SG</ta>
            <ta e="T324" id="Seg_8031" s="T323">sit-IMP.2PL</ta>
            <ta e="T325" id="Seg_8032" s="T324">I.NOM</ta>
            <ta e="T326" id="Seg_8033" s="T325">more</ta>
            <ta e="T327" id="Seg_8034" s="T326">two.[NOM.SG]</ta>
            <ta e="T328" id="Seg_8035" s="T327">cauldron.[NOM.SG]</ta>
            <ta e="T329" id="Seg_8036" s="T328">bring-FUT-1SG</ta>
            <ta e="T330" id="Seg_8037" s="T329">be.ready-FUT-1SG</ta>
            <ta e="T331" id="Seg_8038" s="T330">then</ta>
            <ta e="T332" id="Seg_8039" s="T331">speak-INF.LAT</ta>
            <ta e="T333" id="Seg_8040" s="T332">one.can</ta>
            <ta e="T334" id="Seg_8041" s="T333">PTCL</ta>
            <ta e="T335" id="Seg_8042" s="T334">NEG</ta>
            <ta e="T336" id="Seg_8043" s="T335">speak-PRS-1SG</ta>
            <ta e="T338" id="Seg_8044" s="T337">today</ta>
            <ta e="T339" id="Seg_8045" s="T338">very</ta>
            <ta e="T340" id="Seg_8046" s="T339">warm.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8047" s="T340">day.[NOM.SG]</ta>
            <ta e="T342" id="Seg_8048" s="T341">PTCL</ta>
            <ta e="T343" id="Seg_8049" s="T342">bread.[NOM.SG]</ta>
            <ta e="T344" id="Seg_8050" s="T343">burn-FUT-3SG</ta>
            <ta e="T346" id="Seg_8051" s="T345">bread.[NOM.SG]</ta>
            <ta e="T347" id="Seg_8052" s="T346">NEG</ta>
            <ta e="T348" id="Seg_8053" s="T347">grow-FUT-3SG</ta>
            <ta e="T349" id="Seg_8054" s="T348">then</ta>
            <ta e="T350" id="Seg_8055" s="T349">NEG.EX.[3SG]</ta>
            <ta e="T351" id="Seg_8056" s="T350">bread.[NOM.SG]</ta>
            <ta e="T352" id="Seg_8057" s="T351">what.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8058" s="T352">eat-INF.LAT</ta>
            <ta e="T354" id="Seg_8059" s="T353">die-RES-PST-1SG</ta>
            <ta e="T356" id="Seg_8060" s="T355">die-INF.LAT</ta>
            <ta e="T357" id="Seg_8061" s="T356">one.can</ta>
            <ta e="T358" id="Seg_8062" s="T357">bread-INS</ta>
            <ta e="T359" id="Seg_8063" s="T358">NEG.EX.[3SG]</ta>
            <ta e="T360" id="Seg_8064" s="T359">PTCL</ta>
            <ta e="T361" id="Seg_8065" s="T360">be-IMP.3SG.O</ta>
            <ta e="T362" id="Seg_8066" s="T361">NEG</ta>
            <ta e="T363" id="Seg_8067" s="T362">speak-FUT-1SG</ta>
            <ta e="T365" id="Seg_8068" s="T364">you.NOM</ta>
            <ta e="T366" id="Seg_8069" s="T365">very</ta>
            <ta e="T367" id="Seg_8070" s="T366">black.[NOM.SG]</ta>
            <ta e="T368" id="Seg_8071" s="T367">wash-DRV-EP-IMP.2SG</ta>
            <ta e="T369" id="Seg_8072" s="T368">sauna-LAT</ta>
            <ta e="T370" id="Seg_8073" s="T369">go-EP-IMP.2SG</ta>
            <ta e="T371" id="Seg_8074" s="T370">soap</ta>
            <ta e="T372" id="Seg_8075" s="T371">take-IMP.2SG</ta>
            <ta e="T373" id="Seg_8076" s="T372">then</ta>
            <ta e="T374" id="Seg_8077" s="T373">shirt.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8078" s="T374">take-IMP.2SG</ta>
            <ta e="T376" id="Seg_8079" s="T375">dress-IMP.2SG.O</ta>
            <ta e="T377" id="Seg_8080" s="T376">enough</ta>
            <ta e="T379" id="Seg_8081" s="T378">very</ta>
            <ta e="T380" id="Seg_8082" s="T379">beautiful.[NOM.SG]</ta>
            <ta e="T381" id="Seg_8083" s="T380">then</ta>
            <ta e="T382" id="Seg_8084" s="T381">become-FUT-2SG</ta>
            <ta e="T383" id="Seg_8085" s="T382">shirt.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8086" s="T383">dress-PRS-2SG</ta>
            <ta e="T385" id="Seg_8087" s="T384">so</ta>
            <ta e="T386" id="Seg_8088" s="T385">PTCL</ta>
            <ta e="T387" id="Seg_8089" s="T386">daughter-PL</ta>
            <ta e="T388" id="Seg_8090" s="T387">you.DAT</ta>
            <ta e="T391" id="Seg_8091" s="T390">kiss-FUT-3PL</ta>
            <ta e="T392" id="Seg_8092" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_8093" s="T392">beautiful.[NOM.SG]</ta>
            <ta e="T394" id="Seg_8094" s="T393">enough</ta>
            <ta e="T396" id="Seg_8095" s="T395">this-PL</ta>
            <ta e="T397" id="Seg_8096" s="T396">Krasnoyarsk-PL</ta>
            <ta e="T398" id="Seg_8097" s="T397">go-PST-3PL</ta>
            <ta e="T399" id="Seg_8098" s="T398">I.LAT</ta>
            <ta e="T400" id="Seg_8099" s="T399">NEG</ta>
            <ta e="T401" id="Seg_8100" s="T400">tell-3PL</ta>
            <ta e="T402" id="Seg_8101" s="T401">I.NOM</ta>
            <ta e="T403" id="Seg_8102" s="T402">I.NOM</ta>
            <ta e="T404" id="Seg_8103" s="T403">there</ta>
            <ta e="T405" id="Seg_8104" s="T404">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T406" id="Seg_8105" s="T405">be-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_8106" s="T406">I.NOM</ta>
            <ta e="T408" id="Seg_8107" s="T407">IRREAL</ta>
            <ta e="T409" id="Seg_8108" s="T408">grass.[NOM.SG]</ta>
            <ta e="T410" id="Seg_8109" s="T409">give-PST-1SG</ta>
            <ta e="T411" id="Seg_8110" s="T410">this-GEN</ta>
            <ta e="T412" id="Seg_8111" s="T411">this-GEN</ta>
            <ta e="T413" id="Seg_8112" s="T412">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T414" id="Seg_8113" s="T413">hurt-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_8114" s="T414">this-PL</ta>
            <ta e="T416" id="Seg_8115" s="T415">PTCL</ta>
            <ta e="T417" id="Seg_8116" s="T416">see-PST-3PL</ta>
            <ta e="T418" id="Seg_8117" s="T417">IRREAL</ta>
            <ta e="T419" id="Seg_8118" s="T418">this-ACC</ta>
            <ta e="T421" id="Seg_8119" s="T420">PTCL</ta>
            <ta e="T422" id="Seg_8120" s="T421">else</ta>
            <ta e="T423" id="Seg_8121" s="T422">NEG</ta>
            <ta e="T424" id="Seg_8122" s="T423">speak-FUT-1SG</ta>
            <ta e="T426" id="Seg_8123" s="T425">black.[NOM.SG]</ta>
            <ta e="T427" id="Seg_8124" s="T426">woman.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8125" s="T427">come-PST.[3SG]</ta>
            <ta e="T429" id="Seg_8126" s="T428">white.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8127" s="T429">man.[NOM.SG]</ta>
            <ta e="T431" id="Seg_8128" s="T430">say-IPFVZ.[3SG]</ta>
            <ta e="T432" id="Seg_8129" s="T431">take-INF.LAT</ta>
            <ta e="T433" id="Seg_8130" s="T432">one.should</ta>
            <ta e="T434" id="Seg_8131" s="T433">go-OPT.DU/PL-1DU</ta>
            <ta e="T435" id="Seg_8132" s="T434">together</ta>
            <ta e="T436" id="Seg_8133" s="T435">road-LOC</ta>
            <ta e="T437" id="Seg_8134" s="T436">PTCL</ta>
            <ta e="T439" id="Seg_8135" s="T438">sleep-OPT.DU/PL-1DU</ta>
            <ta e="T440" id="Seg_8136" s="T439">PTCL</ta>
            <ta e="T441" id="Seg_8137" s="T440">kiss-OPT.DU/PL-1DU</ta>
            <ta e="T442" id="Seg_8138" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_8139" s="T442">man-NOM/GEN/ACC.2SG</ta>
            <ta e="T444" id="Seg_8140" s="T443">PTCL</ta>
            <ta e="T445" id="Seg_8141" s="T444">beat-FUT-3SG</ta>
            <ta e="T446" id="Seg_8142" s="T445">you.ACC</ta>
            <ta e="T447" id="Seg_8143" s="T446">enough</ta>
            <ta e="T448" id="Seg_8144" s="T447">else</ta>
            <ta e="T449" id="Seg_8145" s="T448">NEG</ta>
            <ta e="T450" id="Seg_8146" s="T449">speak-FUT-1SG</ta>
            <ta e="T452" id="Seg_8147" s="T451">one.should</ta>
            <ta e="T453" id="Seg_8148" s="T452">I.LAT</ta>
            <ta e="T454" id="Seg_8149" s="T453">child.[NOM.SG]</ta>
            <ta e="T455" id="Seg_8150" s="T454">I.NOM</ta>
            <ta e="T456" id="Seg_8151" s="T455">wash.oneself-INF.LAT</ta>
            <ta e="T457" id="Seg_8152" s="T456">this</ta>
            <ta e="T458" id="Seg_8153" s="T457">very</ta>
            <ta e="T459" id="Seg_8154" s="T458">black.[NOM.SG]</ta>
            <ta e="T460" id="Seg_8155" s="T459">PTCL</ta>
            <ta e="T461" id="Seg_8156" s="T460">shit-PST.[3SG]</ta>
            <ta e="T462" id="Seg_8157" s="T461">many</ta>
            <ta e="T464" id="Seg_8158" s="T463">PTCL</ta>
            <ta e="T465" id="Seg_8159" s="T464">rub-PST-1SG</ta>
            <ta e="T466" id="Seg_8160" s="T465">this</ta>
            <ta e="T467" id="Seg_8161" s="T466">become.dry-RES-PST.[3SG]</ta>
            <ta e="T468" id="Seg_8162" s="T467">then</ta>
            <ta e="T469" id="Seg_8163" s="T468">one.should</ta>
            <ta e="T470" id="Seg_8164" s="T469">plait-INF.LAT</ta>
            <ta e="T471" id="Seg_8165" s="T470">this-ACC</ta>
            <ta e="T472" id="Seg_8166" s="T471">then</ta>
            <ta e="T475" id="Seg_8167" s="T474">eat-INF.LAT</ta>
            <ta e="T476" id="Seg_8168" s="T475">one.should</ta>
            <ta e="T477" id="Seg_8169" s="T476">then</ta>
            <ta e="T479" id="Seg_8170" s="T478">sleep-INF.LAT</ta>
            <ta e="T480" id="Seg_8171" s="T479">rock-IMP.2SG.O</ta>
            <ta e="T481" id="Seg_8172" s="T480">child-NOM/GEN/ACC.1SG</ta>
            <ta e="T486" id="Seg_8173" s="T485">today</ta>
            <ta e="T487" id="Seg_8174" s="T486">big.[NOM.SG]</ta>
            <ta e="T488" id="Seg_8175" s="T487">day.[NOM.SG]</ta>
            <ta e="T489" id="Seg_8176" s="T488">and</ta>
            <ta e="T490" id="Seg_8177" s="T489">people.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8178" s="T490">always</ta>
            <ta e="T492" id="Seg_8179" s="T491">work-DUR-3PL</ta>
            <ta e="T493" id="Seg_8180" s="T492">potato.[NOM.SG]</ta>
            <ta e="T494" id="Seg_8181" s="T493">eat-DUR-3PL</ta>
            <ta e="T495" id="Seg_8182" s="T494">plough-DUR-3PL</ta>
            <ta e="T496" id="Seg_8183" s="T495">and</ta>
            <ta e="T497" id="Seg_8184" s="T496">we.NOM</ta>
            <ta e="T498" id="Seg_8185" s="T497">sit-DUR-1PL</ta>
            <ta e="T499" id="Seg_8186" s="T498">enough</ta>
            <ta e="T500" id="Seg_8187" s="T499">else</ta>
            <ta e="T501" id="Seg_8188" s="T500">NEG</ta>
            <ta e="T502" id="Seg_8189" s="T501">speak-INF.LAT</ta>
            <ta e="T503" id="Seg_8190" s="T502">not</ta>
            <ta e="T504" id="Seg_8191" s="T503">one.wants</ta>
            <ta e="T506" id="Seg_8192" s="T505">today</ta>
            <ta e="T507" id="Seg_8193" s="T506">big.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8194" s="T507">day.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8195" s="T508">I.LAT</ta>
            <ta e="T510" id="Seg_8196" s="T509">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T511" id="Seg_8197" s="T510">call-PST-3PL</ta>
            <ta e="T512" id="Seg_8198" s="T511">go-OPT.DU/PL-1DU</ta>
            <ta e="T513" id="Seg_8199" s="T512">there</ta>
            <ta e="T514" id="Seg_8200" s="T513">vodka.[NOM.SG]</ta>
            <ta e="T515" id="Seg_8201" s="T514">drink-FUT-1DU</ta>
            <ta e="T516" id="Seg_8202" s="T515">there</ta>
            <ta e="T517" id="Seg_8203" s="T516">play-DUR-3PL</ta>
            <ta e="T518" id="Seg_8204" s="T517">accordeon-LAT</ta>
            <ta e="T520" id="Seg_8205" s="T519">we.NOM</ta>
            <ta e="T521" id="Seg_8206" s="T520">PTCL</ta>
            <ta e="T522" id="Seg_8207" s="T521">jump-DUR-1PL</ta>
            <ta e="T523" id="Seg_8208" s="T522">PTCL</ta>
            <ta e="T525" id="Seg_8209" s="T524">one.[NOM.SG]</ta>
            <ta e="T526" id="Seg_8210" s="T525">one.[NOM.SG]</ta>
            <ta e="T527" id="Seg_8211" s="T526">two.[NOM.SG]</ta>
            <ta e="T528" id="Seg_8212" s="T527">three.[NOM.SG]</ta>
            <ta e="T529" id="Seg_8213" s="T528">four.[NOM.SG]</ta>
            <ta e="T530" id="Seg_8214" s="T529">five.[NOM.SG]</ta>
            <ta e="T531" id="Seg_8215" s="T530">seven.[NOM.SG]</ta>
            <ta e="T532" id="Seg_8216" s="T531">nine.[NOM.SG]</ta>
            <ta e="T533" id="Seg_8217" s="T532">eight.[NOM.SG]</ta>
            <ta e="T541" id="Seg_8218" s="T540">ten.[NOM.SG]</ta>
            <ta e="T543" id="Seg_8219" s="T542">ten.[NOM.SG]</ta>
            <ta e="T544" id="Seg_8220" s="T543">one.[NOM.SG]</ta>
            <ta e="T545" id="Seg_8221" s="T544">ten.[NOM.SG]</ta>
            <ta e="T546" id="Seg_8222" s="T545">two.[NOM.SG]</ta>
            <ta e="T547" id="Seg_8223" s="T546">ten.[NOM.SG]</ta>
            <ta e="T548" id="Seg_8224" s="T547">five.[NOM.SG]</ta>
            <ta e="T549" id="Seg_8225" s="T548">ten.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8226" s="T549">six.[NOM.SG]</ta>
            <ta e="T552" id="Seg_8227" s="T551">I.NOM</ta>
            <ta e="T553" id="Seg_8228" s="T552">alone</ta>
            <ta e="T554" id="Seg_8229" s="T553">come-PST-1SG</ta>
            <ta e="T555" id="Seg_8230" s="T554">and</ta>
            <ta e="T556" id="Seg_8231" s="T555">this-PL</ta>
            <ta e="T557" id="Seg_8232" s="T556">PTCL</ta>
            <ta e="T558" id="Seg_8233" s="T557">five.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8234" s="T558">come-PST-3PL</ta>
            <ta e="T561" id="Seg_8235" s="T560">I.NOM</ta>
            <ta e="T562" id="Seg_8236" s="T561">soup.[NOM.SG]</ta>
            <ta e="T564" id="Seg_8237" s="T563">boil-PRS-1SG</ta>
            <ta e="T565" id="Seg_8238" s="T564">very</ta>
            <ta e="T566" id="Seg_8239" s="T565">good</ta>
            <ta e="T567" id="Seg_8240" s="T566">soup.[NOM.SG]</ta>
            <ta e="T568" id="Seg_8241" s="T567">sweet.[NOM.SG]</ta>
            <ta e="T569" id="Seg_8242" s="T568">meat-INS</ta>
            <ta e="T570" id="Seg_8243" s="T569">onion.[NOM.SG]</ta>
            <ta e="T571" id="Seg_8244" s="T570">put-PST-1SG</ta>
            <ta e="T572" id="Seg_8245" s="T571">PTCL</ta>
            <ta e="T573" id="Seg_8246" s="T572">salt.[NOM.SG]</ta>
            <ta e="T574" id="Seg_8247" s="T573">put-PST-1SG</ta>
            <ta e="T575" id="Seg_8248" s="T574">potato.[NOM.SG]</ta>
            <ta e="T576" id="Seg_8249" s="T575">eat-PST-1SG</ta>
            <ta e="T577" id="Seg_8250" s="T576">sit-IMP.2SG</ta>
            <ta e="T578" id="Seg_8251" s="T577">eat-EP-IMP.2SG</ta>
            <ta e="T579" id="Seg_8252" s="T578">together</ta>
            <ta e="T580" id="Seg_8253" s="T579">enough</ta>
            <ta e="T582" id="Seg_8254" s="T581">very</ta>
            <ta e="T583" id="Seg_8255" s="T582">powerful.[NOM.SG]</ta>
            <ta e="T584" id="Seg_8256" s="T583">human.[NOM.SG]</ta>
            <ta e="T585" id="Seg_8257" s="T584">I.NOM</ta>
            <ta e="T586" id="Seg_8258" s="T585">this-LAT</ta>
            <ta e="T587" id="Seg_8259" s="T586">fear-PRS-1SG</ta>
            <ta e="T588" id="Seg_8260" s="T587">this</ta>
            <ta e="T589" id="Seg_8261" s="T588">PTCL</ta>
            <ta e="T590" id="Seg_8262" s="T589">kill-FUT-3SG</ta>
            <ta e="T591" id="Seg_8263" s="T590">I.NOM</ta>
            <ta e="T592" id="Seg_8264" s="T591">hide-RES-PST-1SG</ta>
            <ta e="T593" id="Seg_8265" s="T592">so.that</ta>
            <ta e="T594" id="Seg_8266" s="T593">this.[NOM.SG]</ta>
            <ta e="T595" id="Seg_8267" s="T594">I.LAT</ta>
            <ta e="T596" id="Seg_8268" s="T595">NEG</ta>
            <ta e="T597" id="Seg_8269" s="T596">see-PST.[3SG]</ta>
            <ta e="T598" id="Seg_8270" s="T597">I.NOM</ta>
            <ta e="T599" id="Seg_8271" s="T598">forest-LAT</ta>
            <ta e="T600" id="Seg_8272" s="T599">run-MOM-FUT-1SG</ta>
            <ta e="T601" id="Seg_8273" s="T600">this</ta>
            <ta e="T602" id="Seg_8274" s="T601">I.LAT</ta>
            <ta e="T603" id="Seg_8275" s="T602">NEG</ta>
            <ta e="T604" id="Seg_8276" s="T603">there</ta>
            <ta e="T605" id="Seg_8277" s="T604">NEG</ta>
            <ta e="T606" id="Seg_8278" s="T605">find-FUT-3SG</ta>
            <ta e="T607" id="Seg_8279" s="T606">enough</ta>
            <ta e="T609" id="Seg_8280" s="T608">man-PL</ta>
            <ta e="T610" id="Seg_8281" s="T609">many</ta>
            <ta e="T611" id="Seg_8282" s="T610">collect-PST-3PL</ta>
            <ta e="T612" id="Seg_8283" s="T611">what.[NOM.SG]=INDEF</ta>
            <ta e="T613" id="Seg_8284" s="T612">PTCL</ta>
            <ta e="T614" id="Seg_8285" s="T613">speak-DUR-3PL</ta>
            <ta e="T615" id="Seg_8286" s="T614">one.should</ta>
            <ta e="T616" id="Seg_8287" s="T615">go-INF.LAT</ta>
            <ta e="T617" id="Seg_8288" s="T616">listen-INF.LAT</ta>
            <ta e="T619" id="Seg_8289" s="T618">what.[NOM.SG]</ta>
            <ta e="T620" id="Seg_8290" s="T619">speak-DUR-3PL</ta>
            <ta e="T621" id="Seg_8291" s="T620">I.NOM</ta>
            <ta e="T622" id="Seg_8292" s="T621">then</ta>
            <ta e="T625" id="Seg_8293" s="T624">know-FUT-1SG</ta>
            <ta e="T626" id="Seg_8294" s="T625">PTCL</ta>
            <ta e="T627" id="Seg_8295" s="T626">else</ta>
            <ta e="T628" id="Seg_8296" s="T627">NEG</ta>
            <ta e="T629" id="Seg_8297" s="T628">speak-PRS-1SG</ta>
            <ta e="T635" id="Seg_8298" s="T634">this</ta>
            <ta e="T636" id="Seg_8299" s="T635">human.[NOM.SG]</ta>
            <ta e="T637" id="Seg_8300" s="T636">PTCL</ta>
            <ta e="T638" id="Seg_8301" s="T637">drunk.[NOM.SG]</ta>
            <ta e="T639" id="Seg_8302" s="T638">horse-LOC</ta>
            <ta e="T640" id="Seg_8303" s="T639">sit.down-MOM-PST.[3SG]</ta>
            <ta e="T641" id="Seg_8304" s="T640">then</ta>
            <ta e="T642" id="Seg_8305" s="T641">descend-PST.[3SG]</ta>
            <ta e="T643" id="Seg_8306" s="T642">place-LAT</ta>
            <ta e="T644" id="Seg_8307" s="T643">then</ta>
            <ta e="T645" id="Seg_8308" s="T644">horse-NOM/GEN.3SG</ta>
            <ta e="T646" id="Seg_8309" s="T645">run-MOM-PST.[3SG]</ta>
            <ta e="T647" id="Seg_8310" s="T646">cap.[NOM.SG]</ta>
            <ta e="T648" id="Seg_8311" s="T647">cap.[NOM.SG]</ta>
            <ta e="T650" id="Seg_8312" s="T649">cap.[NOM.SG]</ta>
            <ta e="T651" id="Seg_8313" s="T650">lose-PST.[3SG]</ta>
            <ta e="T652" id="Seg_8314" s="T651">remain-MOM-PST.[3SG]</ta>
            <ta e="T653" id="Seg_8315" s="T652">PTCL</ta>
            <ta e="T654" id="Seg_8316" s="T653">sleep-DUR.[3SG]</ta>
            <ta e="T655" id="Seg_8317" s="T654">sleep-CVB</ta>
            <ta e="T656" id="Seg_8318" s="T655">lie-DUR.[3SG]</ta>
            <ta e="T657" id="Seg_8319" s="T656">fall-MOM-PST.[3SG]</ta>
            <ta e="T658" id="Seg_8320" s="T657">PTCL</ta>
            <ta e="T659" id="Seg_8321" s="T658">enough</ta>
            <ta e="T661" id="Seg_8322" s="T660">boy-PL</ta>
            <ta e="T662" id="Seg_8323" s="T661">girl-PL</ta>
            <ta e="T663" id="Seg_8324" s="T662">PTCL</ta>
            <ta e="T664" id="Seg_8325" s="T663">many</ta>
            <ta e="T665" id="Seg_8326" s="T664">collect-PST-3PL</ta>
            <ta e="T666" id="Seg_8327" s="T665">PTCL</ta>
            <ta e="T667" id="Seg_8328" s="T666">sing-DUR-3PL</ta>
            <ta e="T668" id="Seg_8329" s="T667">accordeon.[NOM.SG]</ta>
            <ta e="T669" id="Seg_8330" s="T668">play-DUR-3PL</ta>
            <ta e="T670" id="Seg_8331" s="T669">jump-DUR-3PL</ta>
            <ta e="T671" id="Seg_8332" s="T670">enough</ta>
            <ta e="T673" id="Seg_8333" s="T672">I.NOM</ta>
            <ta e="T674" id="Seg_8334" s="T673">forest-LAT</ta>
            <ta e="T675" id="Seg_8335" s="T674">go-PST-1SG</ta>
            <ta e="T676" id="Seg_8336" s="T675">berry.[NOM.SG]</ta>
            <ta e="T677" id="Seg_8337" s="T676">tear-PST-1SG</ta>
            <ta e="T678" id="Seg_8338" s="T677">bear.leek.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8339" s="T678">tear-PST-1SG</ta>
            <ta e="T680" id="Seg_8340" s="T679">pie-PL</ta>
            <ta e="T681" id="Seg_8341" s="T680">bake-PST-1SG</ta>
            <ta e="T682" id="Seg_8342" s="T681">you.PL.LAT</ta>
            <ta e="T683" id="Seg_8343" s="T682">give-FUT-1SG</ta>
            <ta e="T684" id="Seg_8344" s="T683">enough</ta>
            <ta e="T686" id="Seg_8345" s="T685">I.GEN</ta>
            <ta e="T687" id="Seg_8346" s="T686">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T688" id="Seg_8347" s="T687">Kamassian.[NOM.SG]</ta>
            <ta e="T689" id="Seg_8348" s="T688">be-PST.[3SG]</ta>
            <ta e="T690" id="Seg_8349" s="T689">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T691" id="Seg_8350" s="T690">settler.[NOM.SG]</ta>
            <ta e="T692" id="Seg_8351" s="T691">be-PST.[3SG]</ta>
            <ta e="T693" id="Seg_8352" s="T692">two.[NOM.SG]</ta>
            <ta e="T694" id="Seg_8353" s="T693">boy.[NOM.SG]</ta>
            <ta e="T695" id="Seg_8354" s="T694">be-PST-3PL</ta>
            <ta e="T699" id="Seg_8355" s="T698">six.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8356" s="T700">be-PST.[3SG]</ta>
            <ta e="T702" id="Seg_8357" s="T701">PTCL</ta>
            <ta e="T703" id="Seg_8358" s="T702">enough</ta>
            <ta e="T705" id="Seg_8359" s="T704">three.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8360" s="T705">die-RES-PST-3PL</ta>
            <ta e="T707" id="Seg_8361" s="T706">and</ta>
            <ta e="T708" id="Seg_8362" s="T707">three.[NOM.SG]</ta>
            <ta e="T709" id="Seg_8363" s="T708">NEG</ta>
            <ta e="T710" id="Seg_8364" s="T709">die-PST-3PL</ta>
            <ta e="T711" id="Seg_8365" s="T710">I.GEN</ta>
            <ta e="T712" id="Seg_8366" s="T711">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T714" id="Seg_8367" s="T712">sable.[NOM.SG]</ta>
            <ta e="T720" id="Seg_8368" s="T719">ten.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8369" s="T720">five.[NOM.SG]</ta>
            <ta e="T722" id="Seg_8370" s="T721">kill-PST.[3SG]</ta>
            <ta e="T724" id="Seg_8371" s="T722">PTCL</ta>
            <ta e="T725" id="Seg_8372" s="T724">enough</ta>
            <ta e="T726" id="Seg_8373" s="T725">squirrel.[NOM.SG]</ta>
            <ta e="T727" id="Seg_8374" s="T726">more</ta>
            <ta e="T728" id="Seg_8375" s="T727">sack.[NOM.SG]</ta>
            <ta e="T729" id="Seg_8376" s="T728">bring-PST.[3SG]</ta>
            <ta e="T730" id="Seg_8377" s="T729">enough</ta>
            <ta e="T731" id="Seg_8378" s="T730">Kamassian</ta>
            <ta e="T732" id="Seg_8379" s="T731">man.[NOM.SG]</ta>
            <ta e="T733" id="Seg_8380" s="T732">hurt-PRS.[3SG]</ta>
            <ta e="T735" id="Seg_8381" s="T734">then</ta>
            <ta e="T736" id="Seg_8382" s="T735">go-PST.[3SG]</ta>
            <ta e="T737" id="Seg_8383" s="T736">shaman-LAT</ta>
            <ta e="T738" id="Seg_8384" s="T737">this.[NOM.SG]</ta>
            <ta e="T740" id="Seg_8385" s="T739">this-ACC</ta>
            <ta e="T741" id="Seg_8386" s="T740">PTCL</ta>
            <ta e="T742" id="Seg_8387" s="T741">heal-DUR-PST.[3SG]</ta>
            <ta e="T743" id="Seg_8388" s="T742">else</ta>
            <ta e="T744" id="Seg_8389" s="T743">PTCL</ta>
            <ta e="T745" id="Seg_8390" s="T744">probably</ta>
            <ta e="T746" id="Seg_8391" s="T745">I.NOM</ta>
            <ta e="T747" id="Seg_8392" s="T746">very</ta>
            <ta e="T748" id="Seg_8393" s="T747">be.hungry-PRS-1SG</ta>
            <ta e="T749" id="Seg_8394" s="T748">sit-IMP.2SG</ta>
            <ta e="T750" id="Seg_8395" s="T749">eat-EP-IMP.2SG</ta>
            <ta e="T751" id="Seg_8396" s="T750">who.[NOM.SG]</ta>
            <ta e="T752" id="Seg_8397" s="T751">you.DAT</ta>
            <ta e="T753" id="Seg_8398" s="T752">NEG</ta>
            <ta e="T754" id="Seg_8399" s="T753">give-PRS.[3SG]</ta>
            <ta e="T755" id="Seg_8400" s="T754">then</ta>
            <ta e="T758" id="Seg_8401" s="T757">lie.down-EP-IMP.2SG</ta>
            <ta e="T759" id="Seg_8402" s="T758">and</ta>
            <ta e="T760" id="Seg_8403" s="T759">sleep-EP-IMP.2SG</ta>
            <ta e="T761" id="Seg_8404" s="T760">enough</ta>
            <ta e="T763" id="Seg_8405" s="T762">child-PL</ta>
            <ta e="T764" id="Seg_8406" s="T763">outside</ta>
            <ta e="T765" id="Seg_8407" s="T764">PTCL</ta>
            <ta e="T767" id="Seg_8408" s="T766">play-DUR-3PL</ta>
            <ta e="T768" id="Seg_8409" s="T767">shout-DUR-3PL</ta>
            <ta e="T769" id="Seg_8410" s="T768">fight-DUR-3PL</ta>
            <ta e="T770" id="Seg_8411" s="T769">what.for</ta>
            <ta e="T771" id="Seg_8412" s="T770">fight-PRS-2SG</ta>
            <ta e="T772" id="Seg_8413" s="T771">good.[NOM.SG]</ta>
            <ta e="T773" id="Seg_8414" s="T772">one.should</ta>
            <ta e="T774" id="Seg_8415" s="T773">play-INF.LAT</ta>
            <ta e="T775" id="Seg_8416" s="T774">NEG.AUX-IMP.2SG</ta>
            <ta e="T776" id="Seg_8417" s="T775">fight-INF.LAT</ta>
            <ta e="T777" id="Seg_8418" s="T776">enough</ta>
            <ta e="T778" id="Seg_8419" s="T777">probably</ta>
            <ta e="T780" id="Seg_8420" s="T779">NEG.AUX-IMP.2SG</ta>
            <ta e="T781" id="Seg_8421" s="T780">fight-CNG</ta>
            <ta e="T782" id="Seg_8422" s="T781">one.should</ta>
            <ta e="T783" id="Seg_8423" s="T782">good.[NOM.SG]</ta>
            <ta e="T784" id="Seg_8424" s="T783">play-INF.LAT</ta>
            <ta e="T785" id="Seg_8425" s="T784">what.for</ta>
            <ta e="T786" id="Seg_8426" s="T785">fight-DUR-2SG</ta>
            <ta e="T788" id="Seg_8427" s="T787">I.NOM</ta>
            <ta e="T789" id="Seg_8428" s="T788">today</ta>
            <ta e="T790" id="Seg_8429" s="T789">stand-PST-1SG</ta>
            <ta e="T791" id="Seg_8430" s="T790">sun.[NOM.SG]</ta>
            <ta e="T792" id="Seg_8431" s="T791">more</ta>
            <ta e="T793" id="Seg_8432" s="T792">NEG</ta>
            <ta e="T794" id="Seg_8433" s="T793">stand-PST.[3SG]</ta>
            <ta e="T795" id="Seg_8434" s="T794">I.NOM</ta>
            <ta e="T796" id="Seg_8435" s="T795">stand-PST-1SG</ta>
            <ta e="T797" id="Seg_8436" s="T796">then</ta>
            <ta e="T798" id="Seg_8437" s="T797">cow.[NOM.SG]</ta>
            <ta e="T799" id="Seg_8438" s="T798">milk-PST-1SG</ta>
            <ta e="T800" id="Seg_8439" s="T799">calf.[NOM.SG]</ta>
            <ta e="T801" id="Seg_8440" s="T800">drink-TR-PST-1SG</ta>
            <ta e="T802" id="Seg_8441" s="T801">hen-PL</ta>
            <ta e="T806" id="Seg_8442" s="T805">feed-DUR-1SG</ta>
            <ta e="T807" id="Seg_8443" s="T806">then</ta>
            <ta e="T808" id="Seg_8444" s="T807">egg-PL</ta>
            <ta e="T809" id="Seg_8445" s="T808">bake-PST-1SG</ta>
            <ta e="T810" id="Seg_8446" s="T809">potato.[NOM.SG]</ta>
            <ta e="T811" id="Seg_8447" s="T810">boil-PST-1SG</ta>
            <ta e="T812" id="Seg_8448" s="T811">eat-PST-1SG</ta>
            <ta e="T813" id="Seg_8449" s="T812">then</ta>
            <ta e="T814" id="Seg_8450" s="T813">Kamassian</ta>
            <ta e="T815" id="Seg_8451" s="T814">man.[NOM.SG]</ta>
            <ta e="T816" id="Seg_8452" s="T815">Kamassian</ta>
            <ta e="T817" id="Seg_8453" s="T816">people.[NOM.SG]</ta>
            <ta e="T818" id="Seg_8454" s="T817">come-PST-3PL</ta>
            <ta e="T819" id="Seg_8455" s="T818">speak-PST-1SG</ta>
            <ta e="T821" id="Seg_8456" s="T820">PTCL</ta>
            <ta e="T822" id="Seg_8457" s="T821">get.tired-MOM-PST-1SG</ta>
            <ta e="T823" id="Seg_8458" s="T822">enough</ta>
            <ta e="T825" id="Seg_8459" s="T824">walk-IMP.2PL</ta>
            <ta e="T826" id="Seg_8460" s="T825">tent-LAT-2SG</ta>
            <ta e="T827" id="Seg_8461" s="T826">enough</ta>
            <ta e="T828" id="Seg_8462" s="T827">speak-INF.LAT</ta>
            <ta e="T829" id="Seg_8463" s="T828">eat-IMP.2PL</ta>
            <ta e="T830" id="Seg_8464" s="T829">lie.down-EP-IMP.2SG</ta>
            <ta e="T831" id="Seg_8465" s="T830">sleep-INF.LAT</ta>
            <ta e="T832" id="Seg_8466" s="T831">enough</ta>
            <ta e="T833" id="Seg_8467" s="T832">else</ta>
            <ta e="T835" id="Seg_8468" s="T834">%that.place-LAT</ta>
            <ta e="T836" id="Seg_8469" s="T835">go-PST-1SG</ta>
            <ta e="T837" id="Seg_8470" s="T836">very</ta>
            <ta e="T838" id="Seg_8471" s="T837">people.[NOM.SG]</ta>
            <ta e="T839" id="Seg_8472" s="T838">many</ta>
            <ta e="T840" id="Seg_8473" s="T839">see-PST-1SG</ta>
            <ta e="T841" id="Seg_8474" s="T840">chief.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8475" s="T841">PTCL</ta>
            <ta e="T843" id="Seg_8476" s="T842">come-PST-3PL</ta>
            <ta e="T844" id="Seg_8477" s="T843">man-PL</ta>
            <ta e="T845" id="Seg_8478" s="T844">there</ta>
            <ta e="T846" id="Seg_8479" s="T845">many</ta>
            <ta e="T847" id="Seg_8480" s="T846">horse-PL</ta>
            <ta e="T848" id="Seg_8481" s="T847">daughter-PL</ta>
            <ta e="T849" id="Seg_8482" s="T848">boy-PL</ta>
            <ta e="T850" id="Seg_8483" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_8484" s="T850">enough</ta>
            <ta e="T853" id="Seg_8485" s="T852">what.[NOM.SG]</ta>
            <ta e="T854" id="Seg_8486" s="T853">this.[NOM.SG]</ta>
            <ta e="T855" id="Seg_8487" s="T854">thin</ta>
            <ta e="T856" id="Seg_8488" s="T855">man.[NOM.SG]</ta>
            <ta e="T857" id="Seg_8489" s="T856">NEG</ta>
            <ta e="T858" id="Seg_8490" s="T857">come-PST.[3SG]</ta>
            <ta e="T859" id="Seg_8491" s="T858">girl.[NOM.SG]</ta>
            <ta e="T860" id="Seg_8492" s="T859">NEG</ta>
            <ta e="T861" id="Seg_8493" s="T860">come-PST.[3SG]</ta>
            <ta e="T862" id="Seg_8494" s="T861">where</ta>
            <ta e="T863" id="Seg_8495" s="T862">this-PL</ta>
            <ta e="T864" id="Seg_8496" s="T863">what.[NOM.SG]</ta>
            <ta e="T865" id="Seg_8497" s="T864">NEG</ta>
            <ta e="T866" id="Seg_8498" s="T865">come-PST-3PL</ta>
            <ta e="T867" id="Seg_8499" s="T866">speak-INF.LAT</ta>
            <ta e="T868" id="Seg_8500" s="T867">I.NOM-COM</ta>
            <ta e="T869" id="Seg_8501" s="T868">enough</ta>
            <ta e="T871" id="Seg_8502" s="T870">where</ta>
            <ta e="T872" id="Seg_8503" s="T871">this-PL</ta>
            <ta e="T873" id="Seg_8504" s="T872">what.[NOM.SG]</ta>
            <ta e="T874" id="Seg_8505" s="T873">NEG</ta>
            <ta e="T875" id="Seg_8506" s="T874">come-PST-3PL</ta>
            <ta e="T876" id="Seg_8507" s="T875">speak-INF.LAT</ta>
            <ta e="T878" id="Seg_8508" s="T877">today</ta>
            <ta e="T879" id="Seg_8509" s="T878">enough</ta>
            <ta e="T880" id="Seg_8510" s="T879">speak-INF.LAT</ta>
            <ta e="T881" id="Seg_8511" s="T880">go-EP-IMP.2SG</ta>
            <ta e="T882" id="Seg_8512" s="T881">house-LAT-2SG</ta>
            <ta e="T883" id="Seg_8513" s="T882">you.NOM</ta>
            <ta e="T884" id="Seg_8514" s="T883">I.LAT</ta>
            <ta e="T886" id="Seg_8515" s="T885">so</ta>
            <ta e="T887" id="Seg_8516" s="T886">NEG</ta>
            <ta e="T891" id="Seg_8517" s="T890">you.NOM</ta>
            <ta e="T892" id="Seg_8518" s="T891">I.LAT</ta>
            <ta e="T893" id="Seg_8519" s="T892">so</ta>
            <ta e="T894" id="Seg_8520" s="T893">NEG</ta>
            <ta e="T896" id="Seg_8521" s="T895">study-TR-EP-IMP.2SG</ta>
            <ta e="T897" id="Seg_8522" s="T896">so</ta>
            <ta e="T898" id="Seg_8523" s="T897">NEG</ta>
            <ta e="T899" id="Seg_8524" s="T898">good.[NOM.SG]</ta>
            <ta e="T900" id="Seg_8525" s="T899">say-INF.LAT</ta>
            <ta e="T901" id="Seg_8526" s="T900">what.for</ta>
            <ta e="T902" id="Seg_8527" s="T901">drive-INF.LAT</ta>
            <ta e="T903" id="Seg_8528" s="T902">people.[NOM.SG]</ta>
            <ta e="T904" id="Seg_8529" s="T903">this</ta>
            <ta e="T905" id="Seg_8530" s="T904">Kamassian-PL</ta>
            <ta e="T906" id="Seg_8531" s="T905">people.[NOM.SG]</ta>
            <ta e="T908" id="Seg_8532" s="T907">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T16" id="Seg_8533" s="T13">вылечить-EP-IMP.2SG</ta>
            <ta e="T17" id="Seg_8534" s="T16">скоро</ta>
            <ta e="T18" id="Seg_8535" s="T17">говорить-INF.LAT</ta>
            <ta e="T19" id="Seg_8536" s="T18">надо</ta>
            <ta e="T20" id="Seg_8537" s="T19">тогда</ta>
            <ta e="T21" id="Seg_8538" s="T20">пашня-NOM/GEN/ACC.3SG</ta>
            <ta e="T22" id="Seg_8539" s="T21">пойти-FUT-1SG</ta>
            <ta e="T23" id="Seg_8540" s="T22">земля.[NOM.SG]</ta>
            <ta e="T24" id="Seg_8541" s="T23">пахать-INF.LAT</ta>
            <ta e="T25" id="Seg_8542" s="T24">тогда</ta>
            <ta e="T26" id="Seg_8543" s="T25">хлеб.[NOM.SG]</ta>
            <ta e="T27" id="Seg_8544" s="T26">убить-INF.LAT</ta>
            <ta e="T28" id="Seg_8545" s="T27">тогда</ta>
            <ta e="T29" id="Seg_8546" s="T28">расти-FUT-3SG</ta>
            <ta e="T30" id="Seg_8547" s="T29">большой.[NOM.SG]</ta>
            <ta e="T31" id="Seg_8548" s="T30">расти-FUT-3SG</ta>
            <ta e="T32" id="Seg_8549" s="T31">PTCL</ta>
            <ta e="T33" id="Seg_8550" s="T32">белый.[NOM.SG]</ta>
            <ta e="T34" id="Seg_8551" s="T33">стать-FUT-3SG</ta>
            <ta e="T35" id="Seg_8552" s="T34">тогда</ta>
            <ta e="T36" id="Seg_8553" s="T35">стать.готовым-INF.LAT</ta>
            <ta e="T37" id="Seg_8554" s="T36">надо</ta>
            <ta e="T40" id="Seg_8555" s="T39">тогда</ta>
            <ta e="T41" id="Seg_8556" s="T40">трясти-MULT-INF.LAT</ta>
            <ta e="T42" id="Seg_8557" s="T41">надо</ta>
            <ta e="T43" id="Seg_8558" s="T42">тогда</ta>
            <ta e="T44" id="Seg_8559" s="T43">мешок-LAT</ta>
            <ta e="T45" id="Seg_8560" s="T44">лить-INF.LAT</ta>
            <ta e="T46" id="Seg_8561" s="T45">тогда</ta>
            <ta e="T47" id="Seg_8562" s="T46">надо</ta>
            <ta e="T48" id="Seg_8563" s="T47">молоть-INF.LAT</ta>
            <ta e="T49" id="Seg_8564" s="T48">тогда</ta>
            <ta e="T50" id="Seg_8565" s="T49">надо</ta>
            <ta e="T51" id="Seg_8566" s="T50">просеивать-INF.LAT</ta>
            <ta e="T52" id="Seg_8567" s="T51">тогда</ta>
            <ta e="T53" id="Seg_8568" s="T52">поставить-INF.LAT</ta>
            <ta e="T54" id="Seg_8569" s="T53">надо</ta>
            <ta e="T55" id="Seg_8570" s="T54">тогда</ta>
            <ta e="T56" id="Seg_8571" s="T55">печь-INF.LAT</ta>
            <ta e="T57" id="Seg_8572" s="T56">надо</ta>
            <ta e="T58" id="Seg_8573" s="T57">тогда</ta>
            <ta e="T59" id="Seg_8574" s="T58">съесть-INF.LAT</ta>
            <ta e="T60" id="Seg_8575" s="T59">надо</ta>
            <ta e="T61" id="Seg_8576" s="T60">весь</ta>
            <ta e="T63" id="Seg_8577" s="T62">ты.NOM</ta>
            <ta e="T64" id="Seg_8578" s="T63">ты.NOM</ta>
            <ta e="T65" id="Seg_8579" s="T64">идти-DUR-2SG</ta>
            <ta e="T66" id="Seg_8580" s="T65">что.[NOM.SG]</ta>
            <ta e="T67" id="Seg_8581" s="T66">я.LAT</ta>
            <ta e="T68" id="Seg_8582" s="T67">NEG</ta>
            <ta e="T69" id="Seg_8583" s="T68">сказать-PST-2SG</ta>
            <ta e="T70" id="Seg_8584" s="T69">я.NOM</ta>
            <ta e="T71" id="Seg_8585" s="T70">IRREAL</ta>
            <ta e="T72" id="Seg_8586" s="T71">трава.[NOM.SG]</ta>
            <ta e="T74" id="Seg_8587" s="T73">дать-PST-1SG</ta>
            <ta e="T75" id="Seg_8588" s="T74">там</ta>
            <ta e="T76" id="Seg_8589" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_8590" s="T76">родственник-PL-ACC.3SG</ta>
            <ta e="T78" id="Seg_8591" s="T77">быть-PRS.[3SG]</ta>
            <ta e="T79" id="Seg_8592" s="T78">ты.NOM</ta>
            <ta e="T80" id="Seg_8593" s="T79">IRREAL</ta>
            <ta e="T81" id="Seg_8594" s="T80">видеть-PST-2SG</ta>
            <ta e="T82" id="Seg_8595" s="T81">этот-ACC</ta>
            <ta e="T83" id="Seg_8596" s="T82">хватит</ta>
            <ta e="T85" id="Seg_8597" s="T84">ты.NOM</ta>
            <ta e="T86" id="Seg_8598" s="T85">что.[NOM.SG]</ta>
            <ta e="T87" id="Seg_8599" s="T86">я.LAT</ta>
            <ta e="T88" id="Seg_8600" s="T87">NEG</ta>
            <ta e="T90" id="Seg_8601" s="T89">сказать-PST-2SG</ta>
            <ta e="T91" id="Seg_8602" s="T90">что</ta>
            <ta e="T92" id="Seg_8603" s="T91">идти-PRS-2SG</ta>
            <ta e="T93" id="Seg_8604" s="T92">Красноярск.[NOM.SG]</ta>
            <ta e="T94" id="Seg_8605" s="T93">там</ta>
            <ta e="T95" id="Seg_8606" s="T94">я.NOM</ta>
            <ta e="T96" id="Seg_8607" s="T95">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T97" id="Seg_8608" s="T96">быть-PRS.[3SG]</ta>
            <ta e="T98" id="Seg_8609" s="T97">я.NOM</ta>
            <ta e="T99" id="Seg_8610" s="T98">IRREAL</ta>
            <ta e="T100" id="Seg_8611" s="T99">трава.[NOM.SG]</ta>
            <ta e="T101" id="Seg_8612" s="T100">ты.DAT</ta>
            <ta e="T102" id="Seg_8613" s="T101">дать-PST-1SG</ta>
            <ta e="T103" id="Seg_8614" s="T102">этот-GEN</ta>
            <ta e="T104" id="Seg_8615" s="T103">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T105" id="Seg_8616" s="T104">болеть-PST.[3SG]</ta>
            <ta e="T106" id="Seg_8617" s="T105">этот.[NOM.SG]</ta>
            <ta e="T108" id="Seg_8618" s="T107">этот.[3SG]</ta>
            <ta e="T109" id="Seg_8619" s="T108">IRREAL</ta>
            <ta e="T110" id="Seg_8620" s="T109">пить-PST.[3SG]</ta>
            <ta e="T111" id="Seg_8621" s="T110">ты.NOM</ta>
            <ta e="T112" id="Seg_8622" s="T111">IRREAL</ta>
            <ta e="T113" id="Seg_8623" s="T112">видеть-PST-2SG</ta>
            <ta e="T114" id="Seg_8624" s="T113">этот-ACC</ta>
            <ta e="T115" id="Seg_8625" s="T114">хватит</ta>
            <ta e="T117" id="Seg_8626" s="T116">сесть-IMP.2SG</ta>
            <ta e="T118" id="Seg_8627" s="T117">картофель.[NOM.SG]</ta>
            <ta e="T119" id="Seg_8628" s="T118">съесть-INF.LAT</ta>
            <ta e="T120" id="Seg_8629" s="T119">а</ta>
            <ta e="T121" id="Seg_8630" s="T120">какой</ta>
            <ta e="T122" id="Seg_8631" s="T121">картофель.[NOM.SG]</ta>
            <ta e="T123" id="Seg_8632" s="T122">да</ta>
            <ta e="T124" id="Seg_8633" s="T123">я.NOM</ta>
            <ta e="T125" id="Seg_8634" s="T124">тереть-PST-1SG</ta>
            <ta e="T126" id="Seg_8635" s="T125">тогда</ta>
            <ta e="T127" id="Seg_8636" s="T126">мука.[NOM.SG]</ta>
            <ta e="T128" id="Seg_8637" s="T127">взять-PST.[3SG]</ta>
            <ta e="T129" id="Seg_8638" s="T128">я.NOM</ta>
            <ta e="T130" id="Seg_8639" s="T129">тогда</ta>
            <ta e="T131" id="Seg_8640" s="T130">сахар.[NOM.SG]</ta>
            <ta e="T132" id="Seg_8641" s="T131">класть-PST-1SG</ta>
            <ta e="T133" id="Seg_8642" s="T132">и</ta>
            <ta e="T134" id="Seg_8643" s="T133">ягода.[NOM.SG]</ta>
            <ta e="T135" id="Seg_8644" s="T134">класть-PST-1SG</ta>
            <ta e="T136" id="Seg_8645" s="T135">теплый.[NOM.SG]</ta>
            <ta e="T138" id="Seg_8646" s="T137">вода.[NOM.SG]</ta>
            <ta e="T139" id="Seg_8647" s="T138">лить-PST-1SG</ta>
            <ta e="T140" id="Seg_8648" s="T139">тогда</ta>
            <ta e="T141" id="Seg_8649" s="T140">кипятить-PST-1SG</ta>
            <ta e="T142" id="Seg_8650" s="T141">сидеть-IMP.2SG</ta>
            <ta e="T143" id="Seg_8651" s="T142">есть-EP-IMP.2SG</ta>
            <ta e="T145" id="Seg_8652" s="T144">мальчик.[NOM.SG]</ta>
            <ta e="T146" id="Seg_8653" s="T145">NEG.EX.[3SG]</ta>
            <ta e="T147" id="Seg_8654" s="T146">куда=INDEF</ta>
            <ta e="T911" id="Seg_8655" s="T147">пойти-CVB</ta>
            <ta e="T148" id="Seg_8656" s="T911">исчезнуть-PST.[3SG]</ta>
            <ta e="T150" id="Seg_8657" s="T149">смотреть-FRQ-INF.LAT</ta>
            <ta e="T151" id="Seg_8658" s="T150">надо</ta>
            <ta e="T152" id="Seg_8659" s="T151">может.быть</ta>
            <ta e="T153" id="Seg_8660" s="T152">кто.[NOM.SG]=INDEF</ta>
            <ta e="T154" id="Seg_8661" s="T153">убить-PST.[3SG]</ta>
            <ta e="T155" id="Seg_8662" s="T154">может.быть</ta>
            <ta e="T156" id="Seg_8663" s="T155">медведь.[NOM.SG]</ta>
            <ta e="T157" id="Seg_8664" s="T156">съесть-PST.[3SG]</ta>
            <ta e="T158" id="Seg_8665" s="T157">жалеть-PRS-1SG</ta>
            <ta e="T159" id="Seg_8666" s="T158">PTCL</ta>
            <ta e="T160" id="Seg_8667" s="T159">очень</ta>
            <ta e="T161" id="Seg_8668" s="T160">хватит</ta>
            <ta e="T163" id="Seg_8669" s="T162">один.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8670" s="T163">два.[NOM.SG]</ta>
            <ta e="T165" id="Seg_8671" s="T164">три.[NOM.SG]</ta>
            <ta e="T166" id="Seg_8672" s="T165">четыре.[NOM.SG]</ta>
            <ta e="T167" id="Seg_8673" s="T166">пять.[NOM.SG]</ta>
            <ta e="T168" id="Seg_8674" s="T167">семь.[NOM.SG]</ta>
            <ta e="T169" id="Seg_8675" s="T168">восемь.[NOM.SG]</ta>
            <ta e="T170" id="Seg_8676" s="T169">десять.[NOM.SG]</ta>
            <ta e="T174" id="Seg_8677" s="T173">кабы</ta>
            <ta e="T175" id="Seg_8678" s="T174">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T177" id="Seg_8679" s="T176">лошадь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T178" id="Seg_8680" s="T177">быть-PST-3PL</ta>
            <ta e="T179" id="Seg_8681" s="T178">тогда</ta>
            <ta e="T180" id="Seg_8682" s="T179">украсть-INF.LAT</ta>
            <ta e="T181" id="Seg_8683" s="T180">можно</ta>
            <ta e="T182" id="Seg_8684" s="T181">и</ta>
            <ta e="T183" id="Seg_8685" s="T182">картофель.[NOM.SG]</ta>
            <ta e="T184" id="Seg_8686" s="T183">съесть-INF.LAT</ta>
            <ta e="T185" id="Seg_8687" s="T184">а.то</ta>
            <ta e="T186" id="Seg_8688" s="T185">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T187" id="Seg_8689" s="T186">лошадь-PL</ta>
            <ta e="T188" id="Seg_8690" s="T187">NEG.EX.[3SG]</ta>
            <ta e="T189" id="Seg_8691" s="T188">весь</ta>
            <ta e="T191" id="Seg_8692" s="T190">я.GEN</ta>
            <ta e="T192" id="Seg_8693" s="T191">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T193" id="Seg_8694" s="T192">PTCL</ta>
            <ta e="T194" id="Seg_8695" s="T193">пьяный.[NOM.SG]</ta>
            <ta e="T195" id="Seg_8696" s="T194">водка.[NOM.SG]</ta>
            <ta e="T196" id="Seg_8697" s="T195">пить-PST.[3SG]</ta>
            <ta e="T197" id="Seg_8698" s="T196">прийти-PST.[3SG]</ta>
            <ta e="T198" id="Seg_8699" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_8700" s="T198">ругать-DES-PST-3PL</ta>
            <ta e="T200" id="Seg_8701" s="T199">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T201" id="Seg_8702" s="T200">мужчина-COM</ta>
            <ta e="T202" id="Seg_8703" s="T201">бороться-PST-3PL</ta>
            <ta e="T203" id="Seg_8704" s="T202">PTCL</ta>
            <ta e="T204" id="Seg_8705" s="T203">очень</ta>
            <ta e="T205" id="Seg_8706" s="T204">этот.[NOM.SG]</ta>
            <ta e="T206" id="Seg_8707" s="T205">сердитый.[NOM.SG]</ta>
            <ta e="T207" id="Seg_8708" s="T206">всякий</ta>
            <ta e="T209" id="Seg_8709" s="T208">ругать-DES-DUR-PST.[3SG]</ta>
            <ta e="T210" id="Seg_8710" s="T209">как</ta>
            <ta e="T211" id="Seg_8711" s="T210">этот-LAT</ta>
            <ta e="T212" id="Seg_8712" s="T211">нужно</ta>
            <ta e="T213" id="Seg_8713" s="T212">опять</ta>
            <ta e="T912" id="Seg_8714" s="T213">пойти-CVB</ta>
            <ta e="T214" id="Seg_8715" s="T912">исчезнуть-PST.[3SG]</ta>
            <ta e="T215" id="Seg_8716" s="T214">водка.[NOM.SG]</ta>
            <ta e="T216" id="Seg_8717" s="T215">пить-INF.LAT</ta>
            <ta e="T217" id="Seg_8718" s="T216">а.то</ta>
            <ta e="T218" id="Seg_8719" s="T217">этот-LAT</ta>
            <ta e="T219" id="Seg_8720" s="T218">мало</ta>
            <ta e="T220" id="Seg_8721" s="T219">хватит</ta>
            <ta e="T222" id="Seg_8722" s="T221">вы.NOM</ta>
            <ta e="T223" id="Seg_8723" s="T222">лес-LOC</ta>
            <ta e="T224" id="Seg_8724" s="T223">быть-PST-2PL</ta>
            <ta e="T225" id="Seg_8725" s="T224">где</ta>
            <ta e="T226" id="Seg_8726" s="T225">спать-PST-2PL</ta>
            <ta e="T227" id="Seg_8727" s="T226">спать-PST-1PL</ta>
            <ta e="T228" id="Seg_8728" s="T227">палатка-LOC</ta>
            <ta e="T229" id="Seg_8729" s="T228">медведь.[NOM.SG]</ta>
            <ta e="T230" id="Seg_8730" s="T229">прийти-PST.[3SG]</ta>
            <ta e="T231" id="Seg_8731" s="T230">PTCL</ta>
            <ta e="T232" id="Seg_8732" s="T231">идти-PST.[3SG]</ta>
            <ta e="T233" id="Seg_8733" s="T232">здесь</ta>
            <ta e="T235" id="Seg_8734" s="T234">мы.NOM</ta>
            <ta e="T236" id="Seg_8735" s="T235">очень</ta>
            <ta e="T237" id="Seg_8736" s="T236">бояться-PST-1PL</ta>
            <ta e="T238" id="Seg_8737" s="T237">этот</ta>
            <ta e="T239" id="Seg_8738" s="T238">тоже</ta>
            <ta e="T240" id="Seg_8739" s="T239">бояться-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_8740" s="T240">ружье-NOM/GEN/ACC.3SG</ta>
            <ta e="T244" id="Seg_8741" s="T243">хозяин-ACC</ta>
            <ta e="T245" id="Seg_8742" s="T244">надо</ta>
            <ta e="T246" id="Seg_8743" s="T245">спросить-INF.LAT</ta>
            <ta e="T247" id="Seg_8744" s="T246">ложиться-INF.LAT</ta>
            <ta e="T248" id="Seg_8745" s="T247">спать-INF.LAT</ta>
            <ta e="T249" id="Seg_8746" s="T248">хватит</ta>
            <ta e="T251" id="Seg_8747" s="T910">лунь-NOM/GEN/ACC.1SG</ta>
            <ta e="T252" id="Seg_8748" s="T251">PTCL</ta>
            <ta e="T254" id="Seg_8749" s="T252">жениться-DUR.[3SG]</ta>
            <ta e="T255" id="Seg_8750" s="T254">PTCL</ta>
            <ta e="T256" id="Seg_8751" s="T255">пукать-FRQ-DUR.[3SG]</ta>
            <ta e="T257" id="Seg_8752" s="T256">PTCL</ta>
            <ta e="T258" id="Seg_8753" s="T257">бить-DUR.[3SG]</ta>
            <ta e="T259" id="Seg_8754" s="T258">PTCL</ta>
            <ta e="T260" id="Seg_8755" s="T259">%%-DUR.[3SG]</ta>
            <ta e="T261" id="Seg_8756" s="T260">еще</ta>
            <ta e="T262" id="Seg_8757" s="T261">весь</ta>
            <ta e="T265" id="Seg_8758" s="T264">что.[NOM.SG]</ta>
            <ta e="T266" id="Seg_8759" s="T265">бояться-PRS-2SG</ta>
            <ta e="T267" id="Seg_8760" s="T266">пойти-EP-IMP.2SG</ta>
            <ta e="T268" id="Seg_8761" s="T267">один</ta>
            <ta e="T269" id="Seg_8762" s="T268">я.NOM</ta>
            <ta e="T270" id="Seg_8763" s="T269">один</ta>
            <ta e="T271" id="Seg_8764" s="T270">идти-PST-1SG</ta>
            <ta e="T272" id="Seg_8765" s="T271">кто-LAT=INDEF</ta>
            <ta e="T274" id="Seg_8766" s="T273">NEG</ta>
            <ta e="T275" id="Seg_8767" s="T274">бояться-PRS-1SG</ta>
            <ta e="T276" id="Seg_8768" s="T275">а</ta>
            <ta e="T277" id="Seg_8769" s="T276">ты.NOM</ta>
            <ta e="T278" id="Seg_8770" s="T277">всегда</ta>
            <ta e="T279" id="Seg_8771" s="T278">бояться-DUR-2SG</ta>
            <ta e="T280" id="Seg_8772" s="T279">забыть-CVB-PST-1SG</ta>
            <ta e="T283" id="Seg_8773" s="T282">этот-PL</ta>
            <ta e="T284" id="Seg_8774" s="T283">NEG</ta>
            <ta e="T285" id="Seg_8775" s="T284">знать-3PL</ta>
            <ta e="T286" id="Seg_8776" s="T285">говорить-INF.LAT</ta>
            <ta e="T287" id="Seg_8777" s="T286">а</ta>
            <ta e="T288" id="Seg_8778" s="T287">я.NOM</ta>
            <ta e="T289" id="Seg_8779" s="T288">PTCL</ta>
            <ta e="T290" id="Seg_8780" s="T289">говорить-DUR-1SG</ta>
            <ta e="T291" id="Seg_8781" s="T290">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T292" id="Seg_8782" s="T291">спросить-PST-1SG</ta>
            <ta e="T293" id="Seg_8783" s="T292">говорить-INF.LAT</ta>
            <ta e="T294" id="Seg_8784" s="T293">хочется</ta>
            <ta e="T295" id="Seg_8785" s="T294">я.LAT</ta>
            <ta e="T296" id="Seg_8786" s="T295">PTCL</ta>
            <ta e="T298" id="Seg_8787" s="T297">там</ta>
            <ta e="T299" id="Seg_8788" s="T298">здесь</ta>
            <ta e="T300" id="Seg_8789" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_8790" s="T300">переселенец-PL</ta>
            <ta e="T302" id="Seg_8791" s="T301">NEG.EX.[3SG]</ta>
            <ta e="T303" id="Seg_8792" s="T302">а</ta>
            <ta e="T305" id="Seg_8793" s="T304">один.[NOM.SG]</ta>
            <ta e="T306" id="Seg_8794" s="T305">камасинец-PL</ta>
            <ta e="T307" id="Seg_8795" s="T306">жить-DUR-3PL</ta>
            <ta e="T308" id="Seg_8796" s="T307">этот-PL</ta>
            <ta e="T309" id="Seg_8797" s="T308">PTCL</ta>
            <ta e="T310" id="Seg_8798" s="T309">переселенец-PL</ta>
            <ta e="T311" id="Seg_8799" s="T310">бояться-PRS-3PL</ta>
            <ta e="T312" id="Seg_8800" s="T311">PTCL</ta>
            <ta e="T314" id="Seg_8801" s="T313">NEG</ta>
            <ta e="T315" id="Seg_8802" s="T314">говорить-FUT-1SG</ta>
            <ta e="T316" id="Seg_8803" s="T315">скоро</ta>
            <ta e="T317" id="Seg_8804" s="T316">мешок.[NOM.SG]</ta>
            <ta e="T318" id="Seg_8805" s="T317">принести-FUT-1SG</ta>
            <ta e="T319" id="Seg_8806" s="T318">ты.NOM</ta>
            <ta e="T320" id="Seg_8807" s="T319">ловить-FUT-2SG</ta>
            <ta e="T321" id="Seg_8808" s="T320">я.NOM</ta>
            <ta e="T322" id="Seg_8809" s="T321">картофель.[NOM.SG]</ta>
            <ta e="T323" id="Seg_8810" s="T322">лить-PST-1SG</ta>
            <ta e="T324" id="Seg_8811" s="T323">сидеть-IMP.2PL</ta>
            <ta e="T325" id="Seg_8812" s="T324">я.NOM</ta>
            <ta e="T326" id="Seg_8813" s="T325">еще</ta>
            <ta e="T327" id="Seg_8814" s="T326">два.[NOM.SG]</ta>
            <ta e="T328" id="Seg_8815" s="T327">котел.[NOM.SG]</ta>
            <ta e="T329" id="Seg_8816" s="T328">принести-FUT-1SG</ta>
            <ta e="T330" id="Seg_8817" s="T329">быть.готовым-FUT-1SG</ta>
            <ta e="T331" id="Seg_8818" s="T330">тогда</ta>
            <ta e="T332" id="Seg_8819" s="T331">говорить-INF.LAT</ta>
            <ta e="T333" id="Seg_8820" s="T332">можно</ta>
            <ta e="T334" id="Seg_8821" s="T333">PTCL</ta>
            <ta e="T335" id="Seg_8822" s="T334">NEG</ta>
            <ta e="T336" id="Seg_8823" s="T335">говорить-PRS-1SG</ta>
            <ta e="T338" id="Seg_8824" s="T337">сегодня</ta>
            <ta e="T339" id="Seg_8825" s="T338">очень</ta>
            <ta e="T340" id="Seg_8826" s="T339">теплый.[NOM.SG]</ta>
            <ta e="T341" id="Seg_8827" s="T340">день.[NOM.SG]</ta>
            <ta e="T342" id="Seg_8828" s="T341">PTCL</ta>
            <ta e="T343" id="Seg_8829" s="T342">хлеб.[NOM.SG]</ta>
            <ta e="T344" id="Seg_8830" s="T343">гореть-FUT-3SG</ta>
            <ta e="T346" id="Seg_8831" s="T345">хлеб.[NOM.SG]</ta>
            <ta e="T347" id="Seg_8832" s="T346">NEG</ta>
            <ta e="T348" id="Seg_8833" s="T347">расти-FUT-3SG</ta>
            <ta e="T349" id="Seg_8834" s="T348">тогда</ta>
            <ta e="T350" id="Seg_8835" s="T349">NEG.EX.[3SG]</ta>
            <ta e="T351" id="Seg_8836" s="T350">хлеб.[NOM.SG]</ta>
            <ta e="T352" id="Seg_8837" s="T351">что.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8838" s="T352">съесть-INF.LAT</ta>
            <ta e="T354" id="Seg_8839" s="T353">умереть-RES-PST-1SG</ta>
            <ta e="T356" id="Seg_8840" s="T355">умереть-INF.LAT</ta>
            <ta e="T357" id="Seg_8841" s="T356">можно</ta>
            <ta e="T358" id="Seg_8842" s="T357">хлеб-INS</ta>
            <ta e="T359" id="Seg_8843" s="T358">NEG.EX.[3SG]</ta>
            <ta e="T360" id="Seg_8844" s="T359">PTCL</ta>
            <ta e="T361" id="Seg_8845" s="T360">быть-IMP.3SG.O</ta>
            <ta e="T362" id="Seg_8846" s="T361">NEG</ta>
            <ta e="T363" id="Seg_8847" s="T362">говорить-FUT-1SG</ta>
            <ta e="T365" id="Seg_8848" s="T364">ты.NOM</ta>
            <ta e="T366" id="Seg_8849" s="T365">очень</ta>
            <ta e="T367" id="Seg_8850" s="T366">черный.[NOM.SG]</ta>
            <ta e="T368" id="Seg_8851" s="T367">мыть-DRV-EP-IMP.2SG</ta>
            <ta e="T369" id="Seg_8852" s="T368">баня-LAT</ta>
            <ta e="T370" id="Seg_8853" s="T369">пойти-EP-IMP.2SG</ta>
            <ta e="T371" id="Seg_8854" s="T370">мыло</ta>
            <ta e="T372" id="Seg_8855" s="T371">взять-IMP.2SG</ta>
            <ta e="T373" id="Seg_8856" s="T372">тогда</ta>
            <ta e="T374" id="Seg_8857" s="T373">рубашка.[NOM.SG]</ta>
            <ta e="T375" id="Seg_8858" s="T374">взять-IMP.2SG</ta>
            <ta e="T376" id="Seg_8859" s="T375">надеть-IMP.2SG.O</ta>
            <ta e="T377" id="Seg_8860" s="T376">хватит</ta>
            <ta e="T379" id="Seg_8861" s="T378">очень</ta>
            <ta e="T380" id="Seg_8862" s="T379">красивый.[NOM.SG]</ta>
            <ta e="T381" id="Seg_8863" s="T380">тогда</ta>
            <ta e="T382" id="Seg_8864" s="T381">стать-FUT-2SG</ta>
            <ta e="T383" id="Seg_8865" s="T382">рубашка.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8866" s="T383">надеть-PRS-2SG</ta>
            <ta e="T385" id="Seg_8867" s="T384">так</ta>
            <ta e="T386" id="Seg_8868" s="T385">PTCL</ta>
            <ta e="T387" id="Seg_8869" s="T386">дочь-PL</ta>
            <ta e="T388" id="Seg_8870" s="T387">ты.DAT</ta>
            <ta e="T391" id="Seg_8871" s="T390">целовать-FUT-3PL</ta>
            <ta e="T392" id="Seg_8872" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_8873" s="T392">красивый.[NOM.SG]</ta>
            <ta e="T394" id="Seg_8874" s="T393">хватит</ta>
            <ta e="T396" id="Seg_8875" s="T395">этот-PL</ta>
            <ta e="T397" id="Seg_8876" s="T396">Красноярск-PL</ta>
            <ta e="T398" id="Seg_8877" s="T397">пойти-PST-3PL</ta>
            <ta e="T399" id="Seg_8878" s="T398">я.LAT</ta>
            <ta e="T400" id="Seg_8879" s="T399">NEG</ta>
            <ta e="T401" id="Seg_8880" s="T400">сказать-3PL</ta>
            <ta e="T402" id="Seg_8881" s="T401">я.NOM</ta>
            <ta e="T403" id="Seg_8882" s="T402">я.NOM</ta>
            <ta e="T404" id="Seg_8883" s="T403">там</ta>
            <ta e="T405" id="Seg_8884" s="T404">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T406" id="Seg_8885" s="T405">быть-PRS.[3SG]</ta>
            <ta e="T407" id="Seg_8886" s="T406">я.NOM</ta>
            <ta e="T408" id="Seg_8887" s="T407">IRREAL</ta>
            <ta e="T409" id="Seg_8888" s="T408">трава.[NOM.SG]</ta>
            <ta e="T410" id="Seg_8889" s="T409">дать-PST-1SG</ta>
            <ta e="T411" id="Seg_8890" s="T410">этот-GEN</ta>
            <ta e="T412" id="Seg_8891" s="T411">этот-GEN</ta>
            <ta e="T413" id="Seg_8892" s="T412">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T414" id="Seg_8893" s="T413">болеть-PRS.[3SG]</ta>
            <ta e="T415" id="Seg_8894" s="T414">этот-PL</ta>
            <ta e="T416" id="Seg_8895" s="T415">PTCL</ta>
            <ta e="T417" id="Seg_8896" s="T416">видеть-PST-3PL</ta>
            <ta e="T418" id="Seg_8897" s="T417">IRREAL</ta>
            <ta e="T419" id="Seg_8898" s="T418">этот-ACC</ta>
            <ta e="T421" id="Seg_8899" s="T420">PTCL</ta>
            <ta e="T422" id="Seg_8900" s="T421">еще</ta>
            <ta e="T423" id="Seg_8901" s="T422">NEG</ta>
            <ta e="T424" id="Seg_8902" s="T423">говорить-FUT-1SG</ta>
            <ta e="T426" id="Seg_8903" s="T425">черный.[NOM.SG]</ta>
            <ta e="T427" id="Seg_8904" s="T426">женщина.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8905" s="T427">прийти-PST.[3SG]</ta>
            <ta e="T429" id="Seg_8906" s="T428">белый.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8907" s="T429">мужчина.[NOM.SG]</ta>
            <ta e="T431" id="Seg_8908" s="T430">сказать-IPFVZ.[3SG]</ta>
            <ta e="T432" id="Seg_8909" s="T431">взять-INF.LAT</ta>
            <ta e="T433" id="Seg_8910" s="T432">надо</ta>
            <ta e="T434" id="Seg_8911" s="T433">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T435" id="Seg_8912" s="T434">вместе</ta>
            <ta e="T436" id="Seg_8913" s="T435">дорога-LOC</ta>
            <ta e="T437" id="Seg_8914" s="T436">PTCL</ta>
            <ta e="T439" id="Seg_8915" s="T438">спать-OPT.DU/PL-1DU</ta>
            <ta e="T440" id="Seg_8916" s="T439">PTCL</ta>
            <ta e="T441" id="Seg_8917" s="T440">целовать-OPT.DU/PL-1DU</ta>
            <ta e="T442" id="Seg_8918" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_8919" s="T442">мужчина-NOM/GEN/ACC.2SG</ta>
            <ta e="T444" id="Seg_8920" s="T443">PTCL</ta>
            <ta e="T445" id="Seg_8921" s="T444">бить-FUT-3SG</ta>
            <ta e="T446" id="Seg_8922" s="T445">ты.ACC</ta>
            <ta e="T447" id="Seg_8923" s="T446">хватит</ta>
            <ta e="T448" id="Seg_8924" s="T447">еще</ta>
            <ta e="T449" id="Seg_8925" s="T448">NEG</ta>
            <ta e="T450" id="Seg_8926" s="T449">говорить-FUT-1SG</ta>
            <ta e="T452" id="Seg_8927" s="T451">надо</ta>
            <ta e="T453" id="Seg_8928" s="T452">я.LAT</ta>
            <ta e="T454" id="Seg_8929" s="T453">ребенок.[NOM.SG]</ta>
            <ta e="T455" id="Seg_8930" s="T454">я.NOM</ta>
            <ta e="T456" id="Seg_8931" s="T455">мыться-INF.LAT</ta>
            <ta e="T457" id="Seg_8932" s="T456">этот</ta>
            <ta e="T458" id="Seg_8933" s="T457">очень</ta>
            <ta e="T459" id="Seg_8934" s="T458">черный.[NOM.SG]</ta>
            <ta e="T460" id="Seg_8935" s="T459">PTCL</ta>
            <ta e="T461" id="Seg_8936" s="T460">испражняться-PST.[3SG]</ta>
            <ta e="T462" id="Seg_8937" s="T461">много</ta>
            <ta e="T464" id="Seg_8938" s="T463">PTCL</ta>
            <ta e="T465" id="Seg_8939" s="T464">тереть-PST-1SG</ta>
            <ta e="T466" id="Seg_8940" s="T465">этот</ta>
            <ta e="T467" id="Seg_8941" s="T466">высохнуть-RES-PST.[3SG]</ta>
            <ta e="T468" id="Seg_8942" s="T467">тогда</ta>
            <ta e="T469" id="Seg_8943" s="T468">надо</ta>
            <ta e="T470" id="Seg_8944" s="T469">плести-INF.LAT</ta>
            <ta e="T471" id="Seg_8945" s="T470">этот-ACC</ta>
            <ta e="T472" id="Seg_8946" s="T471">тогда</ta>
            <ta e="T475" id="Seg_8947" s="T474">есть-INF.LAT</ta>
            <ta e="T476" id="Seg_8948" s="T475">надо</ta>
            <ta e="T477" id="Seg_8949" s="T476">тогда</ta>
            <ta e="T479" id="Seg_8950" s="T478">спать-INF.LAT</ta>
            <ta e="T480" id="Seg_8951" s="T479">качать-IMP.2SG.O</ta>
            <ta e="T481" id="Seg_8952" s="T480">ребенок-NOM/GEN/ACC.1SG</ta>
            <ta e="T486" id="Seg_8953" s="T485">сегодня</ta>
            <ta e="T487" id="Seg_8954" s="T486">большой.[NOM.SG]</ta>
            <ta e="T488" id="Seg_8955" s="T487">день.[NOM.SG]</ta>
            <ta e="T489" id="Seg_8956" s="T488">а</ta>
            <ta e="T490" id="Seg_8957" s="T489">люди.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8958" s="T490">всегда</ta>
            <ta e="T492" id="Seg_8959" s="T491">работать-DUR-3PL</ta>
            <ta e="T493" id="Seg_8960" s="T492">картофель.[NOM.SG]</ta>
            <ta e="T494" id="Seg_8961" s="T493">съесть-DUR-3PL</ta>
            <ta e="T495" id="Seg_8962" s="T494">пахать-DUR-3PL</ta>
            <ta e="T496" id="Seg_8963" s="T495">а</ta>
            <ta e="T497" id="Seg_8964" s="T496">мы.NOM</ta>
            <ta e="T498" id="Seg_8965" s="T497">сидеть-DUR-1PL</ta>
            <ta e="T499" id="Seg_8966" s="T498">хватит</ta>
            <ta e="T500" id="Seg_8967" s="T499">еще</ta>
            <ta e="T501" id="Seg_8968" s="T500">NEG</ta>
            <ta e="T502" id="Seg_8969" s="T501">говорить-INF.LAT</ta>
            <ta e="T503" id="Seg_8970" s="T502">не</ta>
            <ta e="T504" id="Seg_8971" s="T503">хочется</ta>
            <ta e="T506" id="Seg_8972" s="T505">сегодня</ta>
            <ta e="T507" id="Seg_8973" s="T506">большой.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8974" s="T507">день.[NOM.SG]</ta>
            <ta e="T509" id="Seg_8975" s="T508">я.LAT</ta>
            <ta e="T510" id="Seg_8976" s="T509">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T511" id="Seg_8977" s="T510">позвать-PST-3PL</ta>
            <ta e="T512" id="Seg_8978" s="T511">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T513" id="Seg_8979" s="T512">там</ta>
            <ta e="T514" id="Seg_8980" s="T513">водка.[NOM.SG]</ta>
            <ta e="T515" id="Seg_8981" s="T514">пить-FUT-1DU</ta>
            <ta e="T516" id="Seg_8982" s="T515">там</ta>
            <ta e="T517" id="Seg_8983" s="T516">играть-DUR-3PL</ta>
            <ta e="T518" id="Seg_8984" s="T517">гармонь-LAT</ta>
            <ta e="T520" id="Seg_8985" s="T519">мы.NOM</ta>
            <ta e="T521" id="Seg_8986" s="T520">PTCL</ta>
            <ta e="T522" id="Seg_8987" s="T521">прыгнуть-DUR-1PL</ta>
            <ta e="T523" id="Seg_8988" s="T522">PTCL</ta>
            <ta e="T525" id="Seg_8989" s="T524">один.[NOM.SG]</ta>
            <ta e="T526" id="Seg_8990" s="T525">один.[NOM.SG]</ta>
            <ta e="T527" id="Seg_8991" s="T526">два.[NOM.SG]</ta>
            <ta e="T528" id="Seg_8992" s="T527">три.[NOM.SG]</ta>
            <ta e="T529" id="Seg_8993" s="T528">четыре.[NOM.SG]</ta>
            <ta e="T530" id="Seg_8994" s="T529">пять.[NOM.SG]</ta>
            <ta e="T531" id="Seg_8995" s="T530">семь.[NOM.SG]</ta>
            <ta e="T532" id="Seg_8996" s="T531">девять.[NOM.SG]</ta>
            <ta e="T533" id="Seg_8997" s="T532">восемь.[NOM.SG]</ta>
            <ta e="T541" id="Seg_8998" s="T540">десять.[NOM.SG]</ta>
            <ta e="T543" id="Seg_8999" s="T542">десять.[NOM.SG]</ta>
            <ta e="T544" id="Seg_9000" s="T543">один.[NOM.SG]</ta>
            <ta e="T545" id="Seg_9001" s="T544">десять.[NOM.SG]</ta>
            <ta e="T546" id="Seg_9002" s="T545">два.[NOM.SG]</ta>
            <ta e="T547" id="Seg_9003" s="T546">десять.[NOM.SG]</ta>
            <ta e="T548" id="Seg_9004" s="T547">пять.[NOM.SG]</ta>
            <ta e="T549" id="Seg_9005" s="T548">десять.[NOM.SG]</ta>
            <ta e="T550" id="Seg_9006" s="T549">шесть.[NOM.SG]</ta>
            <ta e="T552" id="Seg_9007" s="T551">я.NOM</ta>
            <ta e="T553" id="Seg_9008" s="T552">один</ta>
            <ta e="T554" id="Seg_9009" s="T553">прийти-PST-1SG</ta>
            <ta e="T555" id="Seg_9010" s="T554">а</ta>
            <ta e="T556" id="Seg_9011" s="T555">этот-PL</ta>
            <ta e="T557" id="Seg_9012" s="T556">PTCL</ta>
            <ta e="T558" id="Seg_9013" s="T557">пять.[NOM.SG]</ta>
            <ta e="T559" id="Seg_9014" s="T558">прийти-PST-3PL</ta>
            <ta e="T561" id="Seg_9015" s="T560">я.NOM</ta>
            <ta e="T562" id="Seg_9016" s="T561">суп.[NOM.SG]</ta>
            <ta e="T564" id="Seg_9017" s="T563">кипятить-PRS-1SG</ta>
            <ta e="T565" id="Seg_9018" s="T564">очень</ta>
            <ta e="T566" id="Seg_9019" s="T565">хороший</ta>
            <ta e="T567" id="Seg_9020" s="T566">суп.[NOM.SG]</ta>
            <ta e="T568" id="Seg_9021" s="T567">сладкий.[NOM.SG]</ta>
            <ta e="T569" id="Seg_9022" s="T568">мясо-INS</ta>
            <ta e="T570" id="Seg_9023" s="T569">лук.[NOM.SG]</ta>
            <ta e="T571" id="Seg_9024" s="T570">класть-PST-1SG</ta>
            <ta e="T572" id="Seg_9025" s="T571">PTCL</ta>
            <ta e="T573" id="Seg_9026" s="T572">соль.[NOM.SG]</ta>
            <ta e="T574" id="Seg_9027" s="T573">класть-PST-1SG</ta>
            <ta e="T575" id="Seg_9028" s="T574">картофель.[NOM.SG]</ta>
            <ta e="T576" id="Seg_9029" s="T575">съесть-PST-1SG</ta>
            <ta e="T577" id="Seg_9030" s="T576">сидеть-IMP.2SG</ta>
            <ta e="T578" id="Seg_9031" s="T577">есть-EP-IMP.2SG</ta>
            <ta e="T579" id="Seg_9032" s="T578">вместе</ta>
            <ta e="T580" id="Seg_9033" s="T579">хватит</ta>
            <ta e="T582" id="Seg_9034" s="T581">очень</ta>
            <ta e="T583" id="Seg_9035" s="T582">сильный.[NOM.SG]</ta>
            <ta e="T584" id="Seg_9036" s="T583">человек.[NOM.SG]</ta>
            <ta e="T585" id="Seg_9037" s="T584">я.NOM</ta>
            <ta e="T586" id="Seg_9038" s="T585">этот-LAT</ta>
            <ta e="T587" id="Seg_9039" s="T586">бояться-PRS-1SG</ta>
            <ta e="T588" id="Seg_9040" s="T587">этот</ta>
            <ta e="T589" id="Seg_9041" s="T588">PTCL</ta>
            <ta e="T590" id="Seg_9042" s="T589">убить-FUT-3SG</ta>
            <ta e="T591" id="Seg_9043" s="T590">я.NOM</ta>
            <ta e="T592" id="Seg_9044" s="T591">спрятаться-RES-PST-1SG</ta>
            <ta e="T593" id="Seg_9045" s="T592">чтобы</ta>
            <ta e="T594" id="Seg_9046" s="T593">этот.[NOM.SG]</ta>
            <ta e="T595" id="Seg_9047" s="T594">я.LAT</ta>
            <ta e="T596" id="Seg_9048" s="T595">NEG</ta>
            <ta e="T597" id="Seg_9049" s="T596">видеть-PST.[3SG]</ta>
            <ta e="T598" id="Seg_9050" s="T597">я.NOM</ta>
            <ta e="T599" id="Seg_9051" s="T598">лес-LAT</ta>
            <ta e="T600" id="Seg_9052" s="T599">бежать-MOM-FUT-1SG</ta>
            <ta e="T601" id="Seg_9053" s="T600">этот</ta>
            <ta e="T602" id="Seg_9054" s="T601">я.LAT</ta>
            <ta e="T603" id="Seg_9055" s="T602">NEG</ta>
            <ta e="T604" id="Seg_9056" s="T603">там</ta>
            <ta e="T605" id="Seg_9057" s="T604">NEG</ta>
            <ta e="T606" id="Seg_9058" s="T605">найти-FUT-3SG</ta>
            <ta e="T607" id="Seg_9059" s="T606">хватит</ta>
            <ta e="T609" id="Seg_9060" s="T608">мужчина-PL</ta>
            <ta e="T610" id="Seg_9061" s="T609">много</ta>
            <ta e="T611" id="Seg_9062" s="T610">собирать-PST-3PL</ta>
            <ta e="T612" id="Seg_9063" s="T611">что.[NOM.SG]=INDEF</ta>
            <ta e="T613" id="Seg_9064" s="T612">PTCL</ta>
            <ta e="T614" id="Seg_9065" s="T613">говорить-DUR-3PL</ta>
            <ta e="T615" id="Seg_9066" s="T614">надо</ta>
            <ta e="T616" id="Seg_9067" s="T615">пойти-INF.LAT</ta>
            <ta e="T617" id="Seg_9068" s="T616">слушать-INF.LAT</ta>
            <ta e="T619" id="Seg_9069" s="T618">что.[NOM.SG]</ta>
            <ta e="T620" id="Seg_9070" s="T619">говорить-DUR-3PL</ta>
            <ta e="T621" id="Seg_9071" s="T620">я.NOM</ta>
            <ta e="T622" id="Seg_9072" s="T621">тогда</ta>
            <ta e="T625" id="Seg_9073" s="T624">знать-FUT-1SG</ta>
            <ta e="T626" id="Seg_9074" s="T625">PTCL</ta>
            <ta e="T627" id="Seg_9075" s="T626">еще</ta>
            <ta e="T628" id="Seg_9076" s="T627">NEG</ta>
            <ta e="T629" id="Seg_9077" s="T628">говорить-PRS-1SG</ta>
            <ta e="T635" id="Seg_9078" s="T634">этот</ta>
            <ta e="T636" id="Seg_9079" s="T635">человек.[NOM.SG]</ta>
            <ta e="T637" id="Seg_9080" s="T636">PTCL</ta>
            <ta e="T638" id="Seg_9081" s="T637">пьяный.[NOM.SG]</ta>
            <ta e="T639" id="Seg_9082" s="T638">лошадь-LOC</ta>
            <ta e="T640" id="Seg_9083" s="T639">сесть-MOM-PST.[3SG]</ta>
            <ta e="T641" id="Seg_9084" s="T640">тогда</ta>
            <ta e="T642" id="Seg_9085" s="T641">спуститься-PST.[3SG]</ta>
            <ta e="T643" id="Seg_9086" s="T642">место-LAT</ta>
            <ta e="T644" id="Seg_9087" s="T643">тогда</ta>
            <ta e="T645" id="Seg_9088" s="T644">лошадь-NOM/GEN.3SG</ta>
            <ta e="T646" id="Seg_9089" s="T645">бежать-MOM-PST.[3SG]</ta>
            <ta e="T647" id="Seg_9090" s="T646">шапка.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9091" s="T647">шапка.[NOM.SG]</ta>
            <ta e="T650" id="Seg_9092" s="T649">шапка.[NOM.SG]</ta>
            <ta e="T651" id="Seg_9093" s="T650">терять-PST.[3SG]</ta>
            <ta e="T652" id="Seg_9094" s="T651">остаться-MOM-PST.[3SG]</ta>
            <ta e="T653" id="Seg_9095" s="T652">PTCL</ta>
            <ta e="T654" id="Seg_9096" s="T653">спать-DUR.[3SG]</ta>
            <ta e="T655" id="Seg_9097" s="T654">спать-CVB</ta>
            <ta e="T656" id="Seg_9098" s="T655">лежать-DUR.[3SG]</ta>
            <ta e="T657" id="Seg_9099" s="T656">упасть-MOM-PST.[3SG]</ta>
            <ta e="T658" id="Seg_9100" s="T657">PTCL</ta>
            <ta e="T659" id="Seg_9101" s="T658">хватит</ta>
            <ta e="T661" id="Seg_9102" s="T660">мальчик-PL</ta>
            <ta e="T662" id="Seg_9103" s="T661">девушка-PL</ta>
            <ta e="T663" id="Seg_9104" s="T662">PTCL</ta>
            <ta e="T664" id="Seg_9105" s="T663">много</ta>
            <ta e="T665" id="Seg_9106" s="T664">собирать-PST-3PL</ta>
            <ta e="T666" id="Seg_9107" s="T665">PTCL</ta>
            <ta e="T667" id="Seg_9108" s="T666">петь-DUR-3PL</ta>
            <ta e="T668" id="Seg_9109" s="T667">гармонь.[NOM.SG]</ta>
            <ta e="T669" id="Seg_9110" s="T668">играть-DUR-3PL</ta>
            <ta e="T670" id="Seg_9111" s="T669">прыгнуть-DUR-3PL</ta>
            <ta e="T671" id="Seg_9112" s="T670">хватит</ta>
            <ta e="T673" id="Seg_9113" s="T672">я.NOM</ta>
            <ta e="T674" id="Seg_9114" s="T673">лес-LAT</ta>
            <ta e="T675" id="Seg_9115" s="T674">идти-PST-1SG</ta>
            <ta e="T676" id="Seg_9116" s="T675">ягода.[NOM.SG]</ta>
            <ta e="T677" id="Seg_9117" s="T676">рвать-PST-1SG</ta>
            <ta e="T678" id="Seg_9118" s="T677">черемша.[NOM.SG]</ta>
            <ta e="T679" id="Seg_9119" s="T678">рвать-PST-1SG</ta>
            <ta e="T680" id="Seg_9120" s="T679">пирог-PL</ta>
            <ta e="T681" id="Seg_9121" s="T680">печь-PST-1SG</ta>
            <ta e="T682" id="Seg_9122" s="T681">вы.LAT</ta>
            <ta e="T683" id="Seg_9123" s="T682">дать-FUT-1SG</ta>
            <ta e="T684" id="Seg_9124" s="T683">хватит</ta>
            <ta e="T686" id="Seg_9125" s="T685">я.GEN</ta>
            <ta e="T687" id="Seg_9126" s="T686">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T688" id="Seg_9127" s="T687">камасинец.[NOM.SG]</ta>
            <ta e="T689" id="Seg_9128" s="T688">быть-PST.[3SG]</ta>
            <ta e="T690" id="Seg_9129" s="T689">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T691" id="Seg_9130" s="T690">переселенец.[NOM.SG]</ta>
            <ta e="T692" id="Seg_9131" s="T691">быть-PST.[3SG]</ta>
            <ta e="T693" id="Seg_9132" s="T692">два.[NOM.SG]</ta>
            <ta e="T694" id="Seg_9133" s="T693">мальчик.[NOM.SG]</ta>
            <ta e="T695" id="Seg_9134" s="T694">быть-PST-3PL</ta>
            <ta e="T699" id="Seg_9135" s="T698">шесть.[NOM.SG]</ta>
            <ta e="T701" id="Seg_9136" s="T700">быть-PST.[3SG]</ta>
            <ta e="T702" id="Seg_9137" s="T701">PTCL</ta>
            <ta e="T703" id="Seg_9138" s="T702">хватит</ta>
            <ta e="T705" id="Seg_9139" s="T704">три.[NOM.SG]</ta>
            <ta e="T706" id="Seg_9140" s="T705">умереть-RES-PST-3PL</ta>
            <ta e="T707" id="Seg_9141" s="T706">а</ta>
            <ta e="T708" id="Seg_9142" s="T707">три.[NOM.SG]</ta>
            <ta e="T709" id="Seg_9143" s="T708">NEG</ta>
            <ta e="T710" id="Seg_9144" s="T709">умереть-PST-3PL</ta>
            <ta e="T711" id="Seg_9145" s="T710">я.GEN</ta>
            <ta e="T712" id="Seg_9146" s="T711">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T714" id="Seg_9147" s="T712">соболь.[NOM.SG]</ta>
            <ta e="T720" id="Seg_9148" s="T719">десять.[NOM.SG]</ta>
            <ta e="T721" id="Seg_9149" s="T720">пять.[NOM.SG]</ta>
            <ta e="T722" id="Seg_9150" s="T721">убить-PST.[3SG]</ta>
            <ta e="T724" id="Seg_9151" s="T722">PTCL</ta>
            <ta e="T725" id="Seg_9152" s="T724">хватит</ta>
            <ta e="T726" id="Seg_9153" s="T725">белка.[NOM.SG]</ta>
            <ta e="T727" id="Seg_9154" s="T726">еще</ta>
            <ta e="T728" id="Seg_9155" s="T727">мешок.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9156" s="T728">принести-PST.[3SG]</ta>
            <ta e="T730" id="Seg_9157" s="T729">хватит</ta>
            <ta e="T731" id="Seg_9158" s="T730">камасинец</ta>
            <ta e="T732" id="Seg_9159" s="T731">мужчина.[NOM.SG]</ta>
            <ta e="T733" id="Seg_9160" s="T732">болеть-PRS.[3SG]</ta>
            <ta e="T735" id="Seg_9161" s="T734">тогда</ta>
            <ta e="T736" id="Seg_9162" s="T735">пойти-PST.[3SG]</ta>
            <ta e="T737" id="Seg_9163" s="T736">шаман-LAT</ta>
            <ta e="T738" id="Seg_9164" s="T737">этот.[NOM.SG]</ta>
            <ta e="T740" id="Seg_9165" s="T739">этот-ACC</ta>
            <ta e="T741" id="Seg_9166" s="T740">PTCL</ta>
            <ta e="T742" id="Seg_9167" s="T741">вылечить-DUR-PST.[3SG]</ta>
            <ta e="T743" id="Seg_9168" s="T742">еще</ta>
            <ta e="T744" id="Seg_9169" s="T743">PTCL</ta>
            <ta e="T745" id="Seg_9170" s="T744">наверное</ta>
            <ta e="T746" id="Seg_9171" s="T745">я.NOM</ta>
            <ta e="T747" id="Seg_9172" s="T746">очень</ta>
            <ta e="T748" id="Seg_9173" s="T747">быть.голодным-PRS-1SG</ta>
            <ta e="T749" id="Seg_9174" s="T748">сидеть-IMP.2SG</ta>
            <ta e="T750" id="Seg_9175" s="T749">есть-EP-IMP.2SG</ta>
            <ta e="T751" id="Seg_9176" s="T750">кто.[NOM.SG]</ta>
            <ta e="T752" id="Seg_9177" s="T751">ты.DAT</ta>
            <ta e="T753" id="Seg_9178" s="T752">NEG</ta>
            <ta e="T754" id="Seg_9179" s="T753">дать-PRS.[3SG]</ta>
            <ta e="T755" id="Seg_9180" s="T754">тогда</ta>
            <ta e="T758" id="Seg_9181" s="T757">ложиться-EP-IMP.2SG</ta>
            <ta e="T759" id="Seg_9182" s="T758">и</ta>
            <ta e="T760" id="Seg_9183" s="T759">спать-EP-IMP.2SG</ta>
            <ta e="T761" id="Seg_9184" s="T760">хватит</ta>
            <ta e="T763" id="Seg_9185" s="T762">ребенок-PL</ta>
            <ta e="T764" id="Seg_9186" s="T763">снаружи</ta>
            <ta e="T765" id="Seg_9187" s="T764">PTCL</ta>
            <ta e="T767" id="Seg_9188" s="T766">играть-DUR-3PL</ta>
            <ta e="T768" id="Seg_9189" s="T767">кричать-DUR-3PL</ta>
            <ta e="T769" id="Seg_9190" s="T768">бороться-DUR-3PL</ta>
            <ta e="T770" id="Seg_9191" s="T769">зачем</ta>
            <ta e="T771" id="Seg_9192" s="T770">бороться-PRS-2SG</ta>
            <ta e="T772" id="Seg_9193" s="T771">хороший.[NOM.SG]</ta>
            <ta e="T773" id="Seg_9194" s="T772">надо</ta>
            <ta e="T774" id="Seg_9195" s="T773">играть-INF.LAT</ta>
            <ta e="T775" id="Seg_9196" s="T774">NEG.AUX-IMP.2SG</ta>
            <ta e="T776" id="Seg_9197" s="T775">бороться-INF.LAT</ta>
            <ta e="T777" id="Seg_9198" s="T776">хватит</ta>
            <ta e="T778" id="Seg_9199" s="T777">наверное</ta>
            <ta e="T780" id="Seg_9200" s="T779">NEG.AUX-IMP.2SG</ta>
            <ta e="T781" id="Seg_9201" s="T780">бороться-CNG</ta>
            <ta e="T782" id="Seg_9202" s="T781">надо</ta>
            <ta e="T783" id="Seg_9203" s="T782">хороший.[NOM.SG]</ta>
            <ta e="T784" id="Seg_9204" s="T783">играть-INF.LAT</ta>
            <ta e="T785" id="Seg_9205" s="T784">зачем</ta>
            <ta e="T786" id="Seg_9206" s="T785">бороться-DUR-2SG</ta>
            <ta e="T788" id="Seg_9207" s="T787">я.NOM</ta>
            <ta e="T789" id="Seg_9208" s="T788">сегодня</ta>
            <ta e="T790" id="Seg_9209" s="T789">стоять-PST-1SG</ta>
            <ta e="T791" id="Seg_9210" s="T790">солнце.[NOM.SG]</ta>
            <ta e="T792" id="Seg_9211" s="T791">еще</ta>
            <ta e="T793" id="Seg_9212" s="T792">NEG</ta>
            <ta e="T794" id="Seg_9213" s="T793">стоять-PST.[3SG]</ta>
            <ta e="T795" id="Seg_9214" s="T794">я.NOM</ta>
            <ta e="T796" id="Seg_9215" s="T795">стоять-PST-1SG</ta>
            <ta e="T797" id="Seg_9216" s="T796">тогда</ta>
            <ta e="T798" id="Seg_9217" s="T797">корова.[NOM.SG]</ta>
            <ta e="T799" id="Seg_9218" s="T798">доить-PST-1SG</ta>
            <ta e="T800" id="Seg_9219" s="T799">теленок.[NOM.SG]</ta>
            <ta e="T801" id="Seg_9220" s="T800">пить-TR-PST-1SG</ta>
            <ta e="T802" id="Seg_9221" s="T801">курица-PL</ta>
            <ta e="T806" id="Seg_9222" s="T805">кормить-DUR-1SG</ta>
            <ta e="T807" id="Seg_9223" s="T806">тогда</ta>
            <ta e="T808" id="Seg_9224" s="T807">яйцо-PL</ta>
            <ta e="T809" id="Seg_9225" s="T808">печь-PST-1SG</ta>
            <ta e="T810" id="Seg_9226" s="T809">картофель.[NOM.SG]</ta>
            <ta e="T811" id="Seg_9227" s="T810">кипятить-PST-1SG</ta>
            <ta e="T812" id="Seg_9228" s="T811">есть-PST-1SG</ta>
            <ta e="T813" id="Seg_9229" s="T812">тогда</ta>
            <ta e="T814" id="Seg_9230" s="T813">камасинец</ta>
            <ta e="T815" id="Seg_9231" s="T814">мужчина.[NOM.SG]</ta>
            <ta e="T816" id="Seg_9232" s="T815">камасинец</ta>
            <ta e="T817" id="Seg_9233" s="T816">люди.[NOM.SG]</ta>
            <ta e="T818" id="Seg_9234" s="T817">прийти-PST-3PL</ta>
            <ta e="T819" id="Seg_9235" s="T818">говорить-PST-1SG</ta>
            <ta e="T821" id="Seg_9236" s="T820">PTCL</ta>
            <ta e="T822" id="Seg_9237" s="T821">устать-MOM-PST-1SG</ta>
            <ta e="T823" id="Seg_9238" s="T822">хватит</ta>
            <ta e="T825" id="Seg_9239" s="T824">идти-IMP.2PL</ta>
            <ta e="T826" id="Seg_9240" s="T825">чум-LAT-2SG</ta>
            <ta e="T827" id="Seg_9241" s="T826">хватит</ta>
            <ta e="T828" id="Seg_9242" s="T827">говорить-INF.LAT</ta>
            <ta e="T829" id="Seg_9243" s="T828">съесть-IMP.2PL</ta>
            <ta e="T830" id="Seg_9244" s="T829">ложиться-EP-IMP.2SG</ta>
            <ta e="T831" id="Seg_9245" s="T830">спать-INF.LAT</ta>
            <ta e="T832" id="Seg_9246" s="T831">хватит</ta>
            <ta e="T833" id="Seg_9247" s="T832">еще</ta>
            <ta e="T835" id="Seg_9248" s="T834">%то.место-LAT</ta>
            <ta e="T836" id="Seg_9249" s="T835">идти-PST-1SG</ta>
            <ta e="T837" id="Seg_9250" s="T836">очень</ta>
            <ta e="T838" id="Seg_9251" s="T837">люди.[NOM.SG]</ta>
            <ta e="T839" id="Seg_9252" s="T838">много</ta>
            <ta e="T840" id="Seg_9253" s="T839">видеть-PST-1SG</ta>
            <ta e="T841" id="Seg_9254" s="T840">вождь.[NOM.SG]</ta>
            <ta e="T842" id="Seg_9255" s="T841">PTCL</ta>
            <ta e="T843" id="Seg_9256" s="T842">прийти-PST-3PL</ta>
            <ta e="T844" id="Seg_9257" s="T843">мужчина-PL</ta>
            <ta e="T845" id="Seg_9258" s="T844">там</ta>
            <ta e="T846" id="Seg_9259" s="T845">много</ta>
            <ta e="T847" id="Seg_9260" s="T846">лошадь-PL</ta>
            <ta e="T848" id="Seg_9261" s="T847">дочь-PL</ta>
            <ta e="T849" id="Seg_9262" s="T848">мальчик-PL</ta>
            <ta e="T850" id="Seg_9263" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_9264" s="T850">хватит</ta>
            <ta e="T853" id="Seg_9265" s="T852">что.[NOM.SG]</ta>
            <ta e="T854" id="Seg_9266" s="T853">этот.[NOM.SG]</ta>
            <ta e="T855" id="Seg_9267" s="T854">худой</ta>
            <ta e="T856" id="Seg_9268" s="T855">мужчина.[NOM.SG]</ta>
            <ta e="T857" id="Seg_9269" s="T856">NEG</ta>
            <ta e="T858" id="Seg_9270" s="T857">прийти-PST.[3SG]</ta>
            <ta e="T859" id="Seg_9271" s="T858">девушка.[NOM.SG]</ta>
            <ta e="T860" id="Seg_9272" s="T859">NEG</ta>
            <ta e="T861" id="Seg_9273" s="T860">прийти-PST.[3SG]</ta>
            <ta e="T862" id="Seg_9274" s="T861">где</ta>
            <ta e="T863" id="Seg_9275" s="T862">этот-PL</ta>
            <ta e="T864" id="Seg_9276" s="T863">что.[NOM.SG]</ta>
            <ta e="T865" id="Seg_9277" s="T864">NEG</ta>
            <ta e="T866" id="Seg_9278" s="T865">прийти-PST-3PL</ta>
            <ta e="T867" id="Seg_9279" s="T866">говорить-INF.LAT</ta>
            <ta e="T868" id="Seg_9280" s="T867">я.NOM-COM</ta>
            <ta e="T869" id="Seg_9281" s="T868">хватит</ta>
            <ta e="T871" id="Seg_9282" s="T870">где</ta>
            <ta e="T872" id="Seg_9283" s="T871">этот-PL</ta>
            <ta e="T873" id="Seg_9284" s="T872">что.[NOM.SG]</ta>
            <ta e="T874" id="Seg_9285" s="T873">NEG</ta>
            <ta e="T875" id="Seg_9286" s="T874">прийти-PST-3PL</ta>
            <ta e="T876" id="Seg_9287" s="T875">говорить-INF.LAT</ta>
            <ta e="T878" id="Seg_9288" s="T877">сегодня</ta>
            <ta e="T879" id="Seg_9289" s="T878">хватит</ta>
            <ta e="T880" id="Seg_9290" s="T879">говорить-INF.LAT</ta>
            <ta e="T881" id="Seg_9291" s="T880">пойти-EP-IMP.2SG</ta>
            <ta e="T882" id="Seg_9292" s="T881">дом-LAT-2SG</ta>
            <ta e="T883" id="Seg_9293" s="T882">ты.NOM</ta>
            <ta e="T884" id="Seg_9294" s="T883">я.LAT</ta>
            <ta e="T886" id="Seg_9295" s="T885">так</ta>
            <ta e="T887" id="Seg_9296" s="T886">NEG</ta>
            <ta e="T891" id="Seg_9297" s="T890">ты.NOM</ta>
            <ta e="T892" id="Seg_9298" s="T891">я.LAT</ta>
            <ta e="T893" id="Seg_9299" s="T892">так</ta>
            <ta e="T894" id="Seg_9300" s="T893">NEG</ta>
            <ta e="T896" id="Seg_9301" s="T895">учиться-TR-EP-IMP.2SG</ta>
            <ta e="T897" id="Seg_9302" s="T896">так</ta>
            <ta e="T898" id="Seg_9303" s="T897">NEG</ta>
            <ta e="T899" id="Seg_9304" s="T898">хороший.[NOM.SG]</ta>
            <ta e="T900" id="Seg_9305" s="T899">сказать-INF.LAT</ta>
            <ta e="T901" id="Seg_9306" s="T900">зачем</ta>
            <ta e="T902" id="Seg_9307" s="T901">гнать-INF.LAT</ta>
            <ta e="T903" id="Seg_9308" s="T902">люди.[NOM.SG]</ta>
            <ta e="T904" id="Seg_9309" s="T903">этот</ta>
            <ta e="T905" id="Seg_9310" s="T904">камасинец-PL</ta>
            <ta e="T906" id="Seg_9311" s="T905">люди.[NOM.SG]</ta>
            <ta e="T908" id="Seg_9312" s="T907">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T16" id="Seg_9313" s="T13">v-v:ins-v:mood.pn</ta>
            <ta e="T17" id="Seg_9314" s="T16">adv</ta>
            <ta e="T18" id="Seg_9315" s="T17">v-v:n.fin</ta>
            <ta e="T19" id="Seg_9316" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_9317" s="T19">adv</ta>
            <ta e="T21" id="Seg_9318" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_9319" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_9320" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_9321" s="T23">v-v:n.fin</ta>
            <ta e="T25" id="Seg_9322" s="T24">adv</ta>
            <ta e="T26" id="Seg_9323" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_9324" s="T26">v-v:n.fin</ta>
            <ta e="T28" id="Seg_9325" s="T27">adv</ta>
            <ta e="T29" id="Seg_9326" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_9327" s="T29">adj-n:case</ta>
            <ta e="T31" id="Seg_9328" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_9329" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9330" s="T32">adj-n:case</ta>
            <ta e="T34" id="Seg_9331" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_9332" s="T34">adv</ta>
            <ta e="T36" id="Seg_9333" s="T35">v-v:n.fin</ta>
            <ta e="T37" id="Seg_9334" s="T36">ptcl</ta>
            <ta e="T40" id="Seg_9335" s="T39">adv</ta>
            <ta e="T41" id="Seg_9336" s="T40">v-v&gt;v-v:n.fin</ta>
            <ta e="T42" id="Seg_9337" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_9338" s="T42">adv</ta>
            <ta e="T44" id="Seg_9339" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_9340" s="T44">v-v:n.fin</ta>
            <ta e="T46" id="Seg_9341" s="T45">adv</ta>
            <ta e="T47" id="Seg_9342" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_9343" s="T47">v-v:n.fin</ta>
            <ta e="T49" id="Seg_9344" s="T48">adv</ta>
            <ta e="T50" id="Seg_9345" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_9346" s="T50">v-v:n.fin</ta>
            <ta e="T52" id="Seg_9347" s="T51">adv</ta>
            <ta e="T53" id="Seg_9348" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_9349" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_9350" s="T54">adv</ta>
            <ta e="T56" id="Seg_9351" s="T55">v-v:n.fin</ta>
            <ta e="T57" id="Seg_9352" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9353" s="T57">adv</ta>
            <ta e="T59" id="Seg_9354" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_9355" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_9356" s="T60">quant</ta>
            <ta e="T63" id="Seg_9357" s="T62">pers</ta>
            <ta e="T64" id="Seg_9358" s="T63">pers</ta>
            <ta e="T65" id="Seg_9359" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_9360" s="T65">que-n:case</ta>
            <ta e="T67" id="Seg_9361" s="T66">pers</ta>
            <ta e="T68" id="Seg_9362" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9363" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_9364" s="T69">pers</ta>
            <ta e="T71" id="Seg_9365" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_9366" s="T71">n-n:case</ta>
            <ta e="T74" id="Seg_9367" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_9368" s="T74">adv</ta>
            <ta e="T76" id="Seg_9369" s="T75">pers</ta>
            <ta e="T77" id="Seg_9370" s="T76">n-n:num-n:case.poss</ta>
            <ta e="T78" id="Seg_9371" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_9372" s="T78">pers</ta>
            <ta e="T80" id="Seg_9373" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_9374" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_9375" s="T81">dempro-n:case</ta>
            <ta e="T83" id="Seg_9376" s="T82">ptcl</ta>
            <ta e="T85" id="Seg_9377" s="T84">pers</ta>
            <ta e="T86" id="Seg_9378" s="T85">que-n:case</ta>
            <ta e="T87" id="Seg_9379" s="T86">pers</ta>
            <ta e="T88" id="Seg_9380" s="T87">ptcl</ta>
            <ta e="T90" id="Seg_9381" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_9382" s="T90">conj</ta>
            <ta e="T92" id="Seg_9383" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_9384" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_9385" s="T93">adv</ta>
            <ta e="T95" id="Seg_9386" s="T94">pers</ta>
            <ta e="T96" id="Seg_9387" s="T95">n-n:case.poss</ta>
            <ta e="T97" id="Seg_9388" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_9389" s="T97">pers</ta>
            <ta e="T99" id="Seg_9390" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_9391" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_9392" s="T100">pers</ta>
            <ta e="T102" id="Seg_9393" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_9394" s="T102">dempro-n:case</ta>
            <ta e="T104" id="Seg_9395" s="T103">n-n:case.poss</ta>
            <ta e="T105" id="Seg_9396" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_9397" s="T105">dempro-n:case</ta>
            <ta e="T108" id="Seg_9398" s="T107">dempro-v:pn</ta>
            <ta e="T109" id="Seg_9399" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_9400" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_9401" s="T110">pers</ta>
            <ta e="T112" id="Seg_9402" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_9403" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_9404" s="T113">dempro-n:case</ta>
            <ta e="T115" id="Seg_9405" s="T114">ptcl</ta>
            <ta e="T117" id="Seg_9406" s="T116">v-v:mood.pn</ta>
            <ta e="T118" id="Seg_9407" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_9408" s="T118">v-v:n.fin</ta>
            <ta e="T120" id="Seg_9409" s="T119">conj</ta>
            <ta e="T121" id="Seg_9410" s="T120">que</ta>
            <ta e="T122" id="Seg_9411" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_9412" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_9413" s="T123">pers</ta>
            <ta e="T125" id="Seg_9414" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_9415" s="T125">adv</ta>
            <ta e="T127" id="Seg_9416" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_9417" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_9418" s="T128">pers</ta>
            <ta e="T130" id="Seg_9419" s="T129">adv</ta>
            <ta e="T131" id="Seg_9420" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_9421" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_9422" s="T132">conj</ta>
            <ta e="T134" id="Seg_9423" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_9424" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_9425" s="T135">adj-n:case</ta>
            <ta e="T138" id="Seg_9426" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_9427" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_9428" s="T139">adv</ta>
            <ta e="T141" id="Seg_9429" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_9430" s="T141">v-v:mood.pn</ta>
            <ta e="T143" id="Seg_9431" s="T142">v-v:ins-v:mood.pn</ta>
            <ta e="T145" id="Seg_9432" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_9433" s="T145">v-v:pn</ta>
            <ta e="T147" id="Seg_9434" s="T146">que=ptcl</ta>
            <ta e="T911" id="Seg_9435" s="T147">v-v:n-fin</ta>
            <ta e="T148" id="Seg_9436" s="T911">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_9437" s="T149">v-v&gt;v-v:n.fin</ta>
            <ta e="T151" id="Seg_9438" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9439" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_9440" s="T152">que=ptcl</ta>
            <ta e="T154" id="Seg_9441" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_9442" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_9443" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_9444" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_9445" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_9446" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_9447" s="T159">adv</ta>
            <ta e="T161" id="Seg_9448" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_9449" s="T162">num-n:case</ta>
            <ta e="T164" id="Seg_9450" s="T163">num-n:case</ta>
            <ta e="T165" id="Seg_9451" s="T164">num-n:case</ta>
            <ta e="T166" id="Seg_9452" s="T165">num-n:case</ta>
            <ta e="T167" id="Seg_9453" s="T166">num-n:case</ta>
            <ta e="T168" id="Seg_9454" s="T167">num-n:case</ta>
            <ta e="T169" id="Seg_9455" s="T168">num-n:case</ta>
            <ta e="T170" id="Seg_9456" s="T169">num-n:case</ta>
            <ta e="T174" id="Seg_9457" s="T173">conj</ta>
            <ta e="T175" id="Seg_9458" s="T174">refl-n:case.poss</ta>
            <ta e="T177" id="Seg_9459" s="T176">n-n:num-n:case.poss</ta>
            <ta e="T178" id="Seg_9460" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_9461" s="T178">adv</ta>
            <ta e="T180" id="Seg_9462" s="T179">v-v:n.fin</ta>
            <ta e="T181" id="Seg_9463" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_9464" s="T181">conj</ta>
            <ta e="T183" id="Seg_9465" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_9466" s="T183">v-v:n.fin</ta>
            <ta e="T185" id="Seg_9467" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_9468" s="T185">refl-n:case.poss</ta>
            <ta e="T187" id="Seg_9469" s="T186">n-n:num</ta>
            <ta e="T188" id="Seg_9470" s="T187">v-v:pn</ta>
            <ta e="T189" id="Seg_9471" s="T188">quant</ta>
            <ta e="T191" id="Seg_9472" s="T190">pers</ta>
            <ta e="T192" id="Seg_9473" s="T191">n-n:case.poss</ta>
            <ta e="T193" id="Seg_9474" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9475" s="T193">adj-n:case</ta>
            <ta e="T195" id="Seg_9476" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_9477" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_9478" s="T196">v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_9479" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_9480" s="T198">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_9481" s="T199">refl-n:case.poss</ta>
            <ta e="T201" id="Seg_9482" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_9483" s="T201">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_9484" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_9485" s="T203">adv</ta>
            <ta e="T205" id="Seg_9486" s="T204">dempro-n:case</ta>
            <ta e="T206" id="Seg_9487" s="T205">adj-n:case</ta>
            <ta e="T207" id="Seg_9488" s="T206">adj</ta>
            <ta e="T209" id="Seg_9489" s="T208">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_9490" s="T209">que</ta>
            <ta e="T211" id="Seg_9491" s="T210">dempro-n:case</ta>
            <ta e="T212" id="Seg_9492" s="T211">adv</ta>
            <ta e="T213" id="Seg_9493" s="T212">adv</ta>
            <ta e="T912" id="Seg_9494" s="T213">v-v:n-fin</ta>
            <ta e="T214" id="Seg_9495" s="T912">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_9496" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_9497" s="T215">v-v:n.fin</ta>
            <ta e="T217" id="Seg_9498" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_9499" s="T217">dempro-n:case</ta>
            <ta e="T219" id="Seg_9500" s="T218">adv</ta>
            <ta e="T220" id="Seg_9501" s="T219">ptcl</ta>
            <ta e="T222" id="Seg_9502" s="T221">pers</ta>
            <ta e="T223" id="Seg_9503" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_9504" s="T223">v-v:tense-v:pn</ta>
            <ta e="T225" id="Seg_9505" s="T224">que</ta>
            <ta e="T226" id="Seg_9506" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_9507" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_9508" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_9509" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_9510" s="T229">v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_9511" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_9512" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_9513" s="T232">adv</ta>
            <ta e="T235" id="Seg_9514" s="T234">pers</ta>
            <ta e="T236" id="Seg_9515" s="T235">adv</ta>
            <ta e="T237" id="Seg_9516" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_9517" s="T237">dempro</ta>
            <ta e="T239" id="Seg_9518" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_9519" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_9520" s="T240">n-n:case.poss</ta>
            <ta e="T244" id="Seg_9521" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_9522" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_9523" s="T245">v-v:n.fin</ta>
            <ta e="T247" id="Seg_9524" s="T246">v-v:n.fin</ta>
            <ta e="T248" id="Seg_9525" s="T247">v-v:n.fin</ta>
            <ta e="T249" id="Seg_9526" s="T248">ptcl</ta>
            <ta e="T251" id="Seg_9527" s="T910">n-n:case.poss</ta>
            <ta e="T252" id="Seg_9528" s="T251">ptcl</ta>
            <ta e="T254" id="Seg_9529" s="T252">v-v&gt;v-v:pn</ta>
            <ta e="T255" id="Seg_9530" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_9531" s="T255">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T257" id="Seg_9532" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_9533" s="T257">v-v&gt;v-v:pn</ta>
            <ta e="T259" id="Seg_9534" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_9535" s="T259">v-v&gt;v-v:pn</ta>
            <ta e="T261" id="Seg_9536" s="T260">adv</ta>
            <ta e="T262" id="Seg_9537" s="T261">quant</ta>
            <ta e="T265" id="Seg_9538" s="T264">que-n:case</ta>
            <ta e="T266" id="Seg_9539" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_9540" s="T266">v-v:ins-v:mood.pn</ta>
            <ta e="T268" id="Seg_9541" s="T267">adv</ta>
            <ta e="T269" id="Seg_9542" s="T268">pers</ta>
            <ta e="T270" id="Seg_9543" s="T269">adv</ta>
            <ta e="T271" id="Seg_9544" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_9545" s="T271">que-n:case=ptcl</ta>
            <ta e="T274" id="Seg_9546" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_9547" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_9548" s="T275">conj</ta>
            <ta e="T277" id="Seg_9549" s="T276">pers</ta>
            <ta e="T278" id="Seg_9550" s="T277">adv</ta>
            <ta e="T279" id="Seg_9551" s="T278">v-v&gt;v-v:pn</ta>
            <ta e="T280" id="Seg_9552" s="T279">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_9553" s="T282">dempro-n:num</ta>
            <ta e="T284" id="Seg_9554" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9555" s="T284">v-v:pn</ta>
            <ta e="T286" id="Seg_9556" s="T285">v-v:n.fin</ta>
            <ta e="T287" id="Seg_9557" s="T286">conj</ta>
            <ta e="T288" id="Seg_9558" s="T287">pers</ta>
            <ta e="T289" id="Seg_9559" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_9560" s="T289">v-v&gt;v-v:pn</ta>
            <ta e="T291" id="Seg_9561" s="T290">n-n:case.poss</ta>
            <ta e="T292" id="Seg_9562" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_9563" s="T292">v-v:n.fin</ta>
            <ta e="T294" id="Seg_9564" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_9565" s="T294">pers</ta>
            <ta e="T296" id="Seg_9566" s="T295">ptcl</ta>
            <ta e="T298" id="Seg_9567" s="T297">adv</ta>
            <ta e="T299" id="Seg_9568" s="T298">adv</ta>
            <ta e="T300" id="Seg_9569" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_9570" s="T300">n-n:num</ta>
            <ta e="T302" id="Seg_9571" s="T301">v-v:pn</ta>
            <ta e="T303" id="Seg_9572" s="T302">conj</ta>
            <ta e="T305" id="Seg_9573" s="T304">num-n:case</ta>
            <ta e="T306" id="Seg_9574" s="T305">n-n:num</ta>
            <ta e="T307" id="Seg_9575" s="T306">v-v&gt;v-v:pn</ta>
            <ta e="T308" id="Seg_9576" s="T307">dempro-n:num</ta>
            <ta e="T309" id="Seg_9577" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_9578" s="T309">n-n:num</ta>
            <ta e="T311" id="Seg_9579" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_9580" s="T311">ptcl</ta>
            <ta e="T314" id="Seg_9581" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_9582" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_9583" s="T315">adv</ta>
            <ta e="T317" id="Seg_9584" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_9585" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_9586" s="T318">pers</ta>
            <ta e="T320" id="Seg_9587" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_9588" s="T320">pers</ta>
            <ta e="T322" id="Seg_9589" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_9590" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_9591" s="T323">v-v:mood.pn</ta>
            <ta e="T325" id="Seg_9592" s="T324">pers</ta>
            <ta e="T326" id="Seg_9593" s="T325">adv</ta>
            <ta e="T327" id="Seg_9594" s="T326">num-n:case</ta>
            <ta e="T328" id="Seg_9595" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_9596" s="T328">v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_9597" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_9598" s="T330">adv</ta>
            <ta e="T332" id="Seg_9599" s="T331">v-v:n.fin</ta>
            <ta e="T333" id="Seg_9600" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_9601" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_9602" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_9603" s="T335">v-v:tense-v:pn</ta>
            <ta e="T338" id="Seg_9604" s="T337">adv</ta>
            <ta e="T339" id="Seg_9605" s="T338">adv</ta>
            <ta e="T340" id="Seg_9606" s="T339">adj-n:case</ta>
            <ta e="T341" id="Seg_9607" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_9608" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_9609" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_9610" s="T343">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_9611" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_9612" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9613" s="T347">v-v:tense-v:pn</ta>
            <ta e="T349" id="Seg_9614" s="T348">adv</ta>
            <ta e="T350" id="Seg_9615" s="T349">v-v:pn</ta>
            <ta e="T351" id="Seg_9616" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_9617" s="T351">que-n:case</ta>
            <ta e="T353" id="Seg_9618" s="T352">v-v:n.fin</ta>
            <ta e="T354" id="Seg_9619" s="T353">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_9620" s="T355">v-v:n.fin</ta>
            <ta e="T357" id="Seg_9621" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_9622" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_9623" s="T358">v-v:pn</ta>
            <ta e="T360" id="Seg_9624" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_9625" s="T360">v-v:pn</ta>
            <ta e="T362" id="Seg_9626" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_9627" s="T362">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_9628" s="T364">pers</ta>
            <ta e="T366" id="Seg_9629" s="T365">adv</ta>
            <ta e="T367" id="Seg_9630" s="T366">adj-n:case</ta>
            <ta e="T368" id="Seg_9631" s="T367">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T369" id="Seg_9632" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_9633" s="T369">v-v:ins-v:mood.pn</ta>
            <ta e="T371" id="Seg_9634" s="T370">n</ta>
            <ta e="T372" id="Seg_9635" s="T371">v-v:mood.pn</ta>
            <ta e="T373" id="Seg_9636" s="T372">adv</ta>
            <ta e="T374" id="Seg_9637" s="T373">n-n:case</ta>
            <ta e="T375" id="Seg_9638" s="T374">v-v:mood.pn</ta>
            <ta e="T376" id="Seg_9639" s="T375">v-v:mood.pn</ta>
            <ta e="T377" id="Seg_9640" s="T376">ptcl</ta>
            <ta e="T379" id="Seg_9641" s="T378">adv</ta>
            <ta e="T380" id="Seg_9642" s="T379">adj-n:case</ta>
            <ta e="T381" id="Seg_9643" s="T380">adv</ta>
            <ta e="T382" id="Seg_9644" s="T381">v-v:tense-v:pn</ta>
            <ta e="T383" id="Seg_9645" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_9646" s="T383">v-v:tense-v:pn</ta>
            <ta e="T385" id="Seg_9647" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_9648" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_9649" s="T386">n-n:num</ta>
            <ta e="T388" id="Seg_9650" s="T387">pers</ta>
            <ta e="T391" id="Seg_9651" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_9652" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_9653" s="T392">adj-n:case</ta>
            <ta e="T394" id="Seg_9654" s="T393">ptcl</ta>
            <ta e="T396" id="Seg_9655" s="T395">dempro-n:num</ta>
            <ta e="T397" id="Seg_9656" s="T396">n-n:num</ta>
            <ta e="T398" id="Seg_9657" s="T397">v-v:tense-v:pn</ta>
            <ta e="T399" id="Seg_9658" s="T398">pers</ta>
            <ta e="T400" id="Seg_9659" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_9660" s="T400">v-v:pn</ta>
            <ta e="T402" id="Seg_9661" s="T401">pers</ta>
            <ta e="T403" id="Seg_9662" s="T402">pers</ta>
            <ta e="T404" id="Seg_9663" s="T403">adv</ta>
            <ta e="T405" id="Seg_9664" s="T404">n-n:case.poss</ta>
            <ta e="T406" id="Seg_9665" s="T405">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_9666" s="T406">pers</ta>
            <ta e="T408" id="Seg_9667" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_9668" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_9669" s="T409">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_9670" s="T410">dempro-n:case</ta>
            <ta e="T412" id="Seg_9671" s="T411">dempro-n:case</ta>
            <ta e="T413" id="Seg_9672" s="T412">n-n:case.poss</ta>
            <ta e="T414" id="Seg_9673" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_9674" s="T414">dempro-n:num</ta>
            <ta e="T416" id="Seg_9675" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_9676" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_9677" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_9678" s="T418">dempro-n:case</ta>
            <ta e="T421" id="Seg_9679" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_9680" s="T421">adv</ta>
            <ta e="T423" id="Seg_9681" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_9682" s="T423">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_9683" s="T425">adj-n:case</ta>
            <ta e="T427" id="Seg_9684" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_9685" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_9686" s="T428">adj-n:case</ta>
            <ta e="T430" id="Seg_9687" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_9688" s="T430">v-v&gt;v-v:pn</ta>
            <ta e="T432" id="Seg_9689" s="T431">v-v:n.fin</ta>
            <ta e="T433" id="Seg_9690" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_9691" s="T433">v-v:mood-v:pn</ta>
            <ta e="T435" id="Seg_9692" s="T434">adv</ta>
            <ta e="T436" id="Seg_9693" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_9694" s="T436">ptcl</ta>
            <ta e="T439" id="Seg_9695" s="T438">v-v:mood-v:pn</ta>
            <ta e="T440" id="Seg_9696" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_9697" s="T440">v-v:mood-v:pn</ta>
            <ta e="T442" id="Seg_9698" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_9699" s="T442">n-n:case.poss</ta>
            <ta e="T444" id="Seg_9700" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_9701" s="T444">v-v:tense-v:pn</ta>
            <ta e="T446" id="Seg_9702" s="T445">pers</ta>
            <ta e="T447" id="Seg_9703" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_9704" s="T447">adv</ta>
            <ta e="T449" id="Seg_9705" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_9706" s="T449">v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_9707" s="T451">ptcl</ta>
            <ta e="T453" id="Seg_9708" s="T452">pers</ta>
            <ta e="T454" id="Seg_9709" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_9710" s="T454">pers</ta>
            <ta e="T456" id="Seg_9711" s="T455">v-v:n.fin</ta>
            <ta e="T457" id="Seg_9712" s="T456">dempro</ta>
            <ta e="T458" id="Seg_9713" s="T457">adv</ta>
            <ta e="T459" id="Seg_9714" s="T458">adj-n:case</ta>
            <ta e="T460" id="Seg_9715" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_9716" s="T460">v-v:tense-v:pn</ta>
            <ta e="T462" id="Seg_9717" s="T461">quant</ta>
            <ta e="T464" id="Seg_9718" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_9719" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_9720" s="T465">dempro</ta>
            <ta e="T467" id="Seg_9721" s="T466">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T468" id="Seg_9722" s="T467">adv</ta>
            <ta e="T469" id="Seg_9723" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_9724" s="T469">v-v:n.fin</ta>
            <ta e="T471" id="Seg_9725" s="T470">dempro-n:case</ta>
            <ta e="T472" id="Seg_9726" s="T471">adv</ta>
            <ta e="T475" id="Seg_9727" s="T474">v-v:n.fin</ta>
            <ta e="T476" id="Seg_9728" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_9729" s="T476">adv</ta>
            <ta e="T479" id="Seg_9730" s="T478">v-v:n.fin</ta>
            <ta e="T480" id="Seg_9731" s="T479">v-v:mood.pn</ta>
            <ta e="T481" id="Seg_9732" s="T480">n-n:case.poss</ta>
            <ta e="T486" id="Seg_9733" s="T485">adv</ta>
            <ta e="T487" id="Seg_9734" s="T486">adj-n:case</ta>
            <ta e="T488" id="Seg_9735" s="T487">n-n:case</ta>
            <ta e="T489" id="Seg_9736" s="T488">conj</ta>
            <ta e="T490" id="Seg_9737" s="T489">n-n:case</ta>
            <ta e="T491" id="Seg_9738" s="T490">adv</ta>
            <ta e="T492" id="Seg_9739" s="T491">v-v&gt;v-v:pn</ta>
            <ta e="T493" id="Seg_9740" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_9741" s="T493">v-v&gt;v-v:pn</ta>
            <ta e="T495" id="Seg_9742" s="T494">v-v&gt;v-v:pn</ta>
            <ta e="T496" id="Seg_9743" s="T495">conj</ta>
            <ta e="T497" id="Seg_9744" s="T496">pers</ta>
            <ta e="T498" id="Seg_9745" s="T497">v-v&gt;v-v:pn</ta>
            <ta e="T499" id="Seg_9746" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_9747" s="T499">adv</ta>
            <ta e="T501" id="Seg_9748" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_9749" s="T501">v-v:n.fin</ta>
            <ta e="T503" id="Seg_9750" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_9751" s="T503">ptcl</ta>
            <ta e="T506" id="Seg_9752" s="T505">adv</ta>
            <ta e="T507" id="Seg_9753" s="T506">adj-n:case</ta>
            <ta e="T508" id="Seg_9754" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_9755" s="T508">pers</ta>
            <ta e="T510" id="Seg_9756" s="T509">n-n:case.poss</ta>
            <ta e="T511" id="Seg_9757" s="T510">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_9758" s="T511">v-v:mood-v:pn</ta>
            <ta e="T513" id="Seg_9759" s="T512">adv</ta>
            <ta e="T514" id="Seg_9760" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_9761" s="T514">v-v:tense-v:pn</ta>
            <ta e="T516" id="Seg_9762" s="T515">adv</ta>
            <ta e="T517" id="Seg_9763" s="T516">v-v&gt;v-v:pn</ta>
            <ta e="T518" id="Seg_9764" s="T517">n-n:case</ta>
            <ta e="T520" id="Seg_9765" s="T519">pers</ta>
            <ta e="T521" id="Seg_9766" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_9767" s="T521">v-v&gt;v-v:pn</ta>
            <ta e="T523" id="Seg_9768" s="T522">ptcl</ta>
            <ta e="T525" id="Seg_9769" s="T524">num-n:case</ta>
            <ta e="T526" id="Seg_9770" s="T525">num-n:case</ta>
            <ta e="T527" id="Seg_9771" s="T526">num-n:case</ta>
            <ta e="T528" id="Seg_9772" s="T527">num-n:case</ta>
            <ta e="T529" id="Seg_9773" s="T528">num-n:case</ta>
            <ta e="T530" id="Seg_9774" s="T529">num-n:case</ta>
            <ta e="T531" id="Seg_9775" s="T530">num-n:case</ta>
            <ta e="T532" id="Seg_9776" s="T531">num-n:case</ta>
            <ta e="T533" id="Seg_9777" s="T532">num-n:case</ta>
            <ta e="T541" id="Seg_9778" s="T540">num-n:case</ta>
            <ta e="T543" id="Seg_9779" s="T542">num-n:case</ta>
            <ta e="T544" id="Seg_9780" s="T543">num-n:case</ta>
            <ta e="T545" id="Seg_9781" s="T544">num-n:case</ta>
            <ta e="T546" id="Seg_9782" s="T545">num-n:case</ta>
            <ta e="T547" id="Seg_9783" s="T546">num-n:case</ta>
            <ta e="T548" id="Seg_9784" s="T547">num-n:case</ta>
            <ta e="T549" id="Seg_9785" s="T548">num-n:case</ta>
            <ta e="T550" id="Seg_9786" s="T549">num-n:case</ta>
            <ta e="T552" id="Seg_9787" s="T551">pers</ta>
            <ta e="T553" id="Seg_9788" s="T552">adv</ta>
            <ta e="T554" id="Seg_9789" s="T553">v-v:tense-v:pn</ta>
            <ta e="T555" id="Seg_9790" s="T554">conj</ta>
            <ta e="T556" id="Seg_9791" s="T555">dempro-n:num</ta>
            <ta e="T557" id="Seg_9792" s="T556">ptcl</ta>
            <ta e="T558" id="Seg_9793" s="T557">num-n:case</ta>
            <ta e="T559" id="Seg_9794" s="T558">v-v:tense-v:pn</ta>
            <ta e="T561" id="Seg_9795" s="T560">pers</ta>
            <ta e="T562" id="Seg_9796" s="T561">n-n:case</ta>
            <ta e="T564" id="Seg_9797" s="T563">v-v:tense-v:pn</ta>
            <ta e="T565" id="Seg_9798" s="T564">adv</ta>
            <ta e="T566" id="Seg_9799" s="T565">adj</ta>
            <ta e="T567" id="Seg_9800" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_9801" s="T567">adj-n:case</ta>
            <ta e="T569" id="Seg_9802" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_9803" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_9804" s="T570">v-v:tense-v:pn</ta>
            <ta e="T572" id="Seg_9805" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_9806" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_9807" s="T573">v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_9808" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_9809" s="T575">v-v:tense-v:pn</ta>
            <ta e="T577" id="Seg_9810" s="T576">v-v:mood.pn</ta>
            <ta e="T578" id="Seg_9811" s="T577">v-v:ins-v:mood.pn</ta>
            <ta e="T579" id="Seg_9812" s="T578">adv</ta>
            <ta e="T580" id="Seg_9813" s="T579">ptcl</ta>
            <ta e="T582" id="Seg_9814" s="T581">adv</ta>
            <ta e="T583" id="Seg_9815" s="T582">adj-n:case</ta>
            <ta e="T584" id="Seg_9816" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_9817" s="T584">pers</ta>
            <ta e="T586" id="Seg_9818" s="T585">dempro-n:case</ta>
            <ta e="T587" id="Seg_9819" s="T586">v-v:tense-v:pn</ta>
            <ta e="T588" id="Seg_9820" s="T587">dempro</ta>
            <ta e="T589" id="Seg_9821" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9822" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_9823" s="T590">pers</ta>
            <ta e="T592" id="Seg_9824" s="T591">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_9825" s="T592">conj</ta>
            <ta e="T594" id="Seg_9826" s="T593">dempro-n:case</ta>
            <ta e="T595" id="Seg_9827" s="T594">pers</ta>
            <ta e="T596" id="Seg_9828" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_9829" s="T596">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_9830" s="T597">pers</ta>
            <ta e="T599" id="Seg_9831" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_9832" s="T599">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T601" id="Seg_9833" s="T600">dempro</ta>
            <ta e="T602" id="Seg_9834" s="T601">pers</ta>
            <ta e="T603" id="Seg_9835" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_9836" s="T603">adv</ta>
            <ta e="T605" id="Seg_9837" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_9838" s="T605">v-v:tense-v:pn</ta>
            <ta e="T607" id="Seg_9839" s="T606">ptcl</ta>
            <ta e="T609" id="Seg_9840" s="T608">n-n:num</ta>
            <ta e="T610" id="Seg_9841" s="T609">quant</ta>
            <ta e="T611" id="Seg_9842" s="T610">v-v:tense-v:pn</ta>
            <ta e="T612" id="Seg_9843" s="T611">que-n:case=ptcl</ta>
            <ta e="T613" id="Seg_9844" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_9845" s="T613">v-v&gt;v-v:pn</ta>
            <ta e="T615" id="Seg_9846" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_9847" s="T615">v-v:n.fin</ta>
            <ta e="T617" id="Seg_9848" s="T616">v-v:n.fin</ta>
            <ta e="T619" id="Seg_9849" s="T618">que-n:case</ta>
            <ta e="T620" id="Seg_9850" s="T619">v-v&gt;v-v:pn</ta>
            <ta e="T621" id="Seg_9851" s="T620">pers</ta>
            <ta e="T622" id="Seg_9852" s="T621">adv</ta>
            <ta e="T625" id="Seg_9853" s="T624">v-v:tense-v:pn</ta>
            <ta e="T626" id="Seg_9854" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_9855" s="T626">adv</ta>
            <ta e="T628" id="Seg_9856" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_9857" s="T628">v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_9858" s="T634">dempro</ta>
            <ta e="T636" id="Seg_9859" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_9860" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_9861" s="T637">adj-n:case</ta>
            <ta e="T639" id="Seg_9862" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9863" s="T639">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_9864" s="T640">adv</ta>
            <ta e="T642" id="Seg_9865" s="T641">v-v:tense-v:pn</ta>
            <ta e="T643" id="Seg_9866" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_9867" s="T643">adv</ta>
            <ta e="T645" id="Seg_9868" s="T644">n-n:case.poss</ta>
            <ta e="T646" id="Seg_9869" s="T645">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_9870" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_9871" s="T647">n-n:case</ta>
            <ta e="T650" id="Seg_9872" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_9873" s="T650">v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_9874" s="T651">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T653" id="Seg_9875" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_9876" s="T653">v-v&gt;v-v:pn</ta>
            <ta e="T655" id="Seg_9877" s="T654">v-v:n.fin</ta>
            <ta e="T656" id="Seg_9878" s="T655">v-v&gt;v-v:pn</ta>
            <ta e="T657" id="Seg_9879" s="T656">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T658" id="Seg_9880" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_9881" s="T658">ptcl</ta>
            <ta e="T661" id="Seg_9882" s="T660">n-n:num</ta>
            <ta e="T662" id="Seg_9883" s="T661">n-n:num</ta>
            <ta e="T663" id="Seg_9884" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_9885" s="T663">quant</ta>
            <ta e="T665" id="Seg_9886" s="T664">v-v:tense-v:pn</ta>
            <ta e="T666" id="Seg_9887" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_9888" s="T666">v-v&gt;v-v:pn</ta>
            <ta e="T668" id="Seg_9889" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_9890" s="T668">v-v&gt;v-v:pn</ta>
            <ta e="T670" id="Seg_9891" s="T669">v-v&gt;v-v:pn</ta>
            <ta e="T671" id="Seg_9892" s="T670">ptcl</ta>
            <ta e="T673" id="Seg_9893" s="T672">pers</ta>
            <ta e="T674" id="Seg_9894" s="T673">n-n:case</ta>
            <ta e="T675" id="Seg_9895" s="T674">v-v:tense-v:pn</ta>
            <ta e="T676" id="Seg_9896" s="T675">n-n:case</ta>
            <ta e="T677" id="Seg_9897" s="T676">v-v:tense-v:pn</ta>
            <ta e="T678" id="Seg_9898" s="T677">n-n:case</ta>
            <ta e="T679" id="Seg_9899" s="T678">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_9900" s="T679">n-n:num</ta>
            <ta e="T681" id="Seg_9901" s="T680">v-v:tense-v:pn</ta>
            <ta e="T682" id="Seg_9902" s="T681">pers</ta>
            <ta e="T683" id="Seg_9903" s="T682">v-v:tense-v:pn</ta>
            <ta e="T684" id="Seg_9904" s="T683">ptcl</ta>
            <ta e="T686" id="Seg_9905" s="T685">pers</ta>
            <ta e="T687" id="Seg_9906" s="T686">n-n:case.poss</ta>
            <ta e="T688" id="Seg_9907" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_9908" s="T688">v-v:tense-v:pn</ta>
            <ta e="T690" id="Seg_9909" s="T689">n-n:case.poss</ta>
            <ta e="T691" id="Seg_9910" s="T690">n-n:case</ta>
            <ta e="T692" id="Seg_9911" s="T691">v-v:tense-v:pn</ta>
            <ta e="T693" id="Seg_9912" s="T692">num-n:case</ta>
            <ta e="T694" id="Seg_9913" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_9914" s="T694">v-v:tense-v:pn</ta>
            <ta e="T699" id="Seg_9915" s="T698">num-n:case</ta>
            <ta e="T701" id="Seg_9916" s="T700">v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_9917" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9918" s="T702">ptcl</ta>
            <ta e="T705" id="Seg_9919" s="T704">num-n:case</ta>
            <ta e="T706" id="Seg_9920" s="T705">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_9921" s="T706">conj</ta>
            <ta e="T708" id="Seg_9922" s="T707">num-n:case</ta>
            <ta e="T709" id="Seg_9923" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_9924" s="T709">v-v:tense-v:pn</ta>
            <ta e="T711" id="Seg_9925" s="T710">pers</ta>
            <ta e="T712" id="Seg_9926" s="T711">n-n:case.poss</ta>
            <ta e="T714" id="Seg_9927" s="T712">n-n:case</ta>
            <ta e="T720" id="Seg_9928" s="T719">num-n:case</ta>
            <ta e="T721" id="Seg_9929" s="T720">num-n:case</ta>
            <ta e="T722" id="Seg_9930" s="T721">v-v:tense-v:pn</ta>
            <ta e="T724" id="Seg_9931" s="T722">ptcl</ta>
            <ta e="T725" id="Seg_9932" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_9933" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_9934" s="T726">adv</ta>
            <ta e="T728" id="Seg_9935" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_9936" s="T728">v-v:tense-v:pn</ta>
            <ta e="T730" id="Seg_9937" s="T729">ptcl</ta>
            <ta e="T731" id="Seg_9938" s="T730">n</ta>
            <ta e="T732" id="Seg_9939" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_9940" s="T732">v-v:tense-v:pn</ta>
            <ta e="T735" id="Seg_9941" s="T734">adv</ta>
            <ta e="T736" id="Seg_9942" s="T735">v-v:tense-v:pn</ta>
            <ta e="T737" id="Seg_9943" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_9944" s="T737">dempro-n:case</ta>
            <ta e="T740" id="Seg_9945" s="T739">dempro-n:case</ta>
            <ta e="T741" id="Seg_9946" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_9947" s="T741">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_9948" s="T742">adv</ta>
            <ta e="T744" id="Seg_9949" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_9950" s="T744">adv</ta>
            <ta e="T746" id="Seg_9951" s="T745">pers</ta>
            <ta e="T747" id="Seg_9952" s="T746">adv</ta>
            <ta e="T748" id="Seg_9953" s="T747">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_9954" s="T748">v-v:mood.pn</ta>
            <ta e="T750" id="Seg_9955" s="T749">v-v:ins-v:mood.pn</ta>
            <ta e="T751" id="Seg_9956" s="T750">que-n:case</ta>
            <ta e="T752" id="Seg_9957" s="T751">pers</ta>
            <ta e="T753" id="Seg_9958" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_9959" s="T753">v-v:tense-v:pn</ta>
            <ta e="T755" id="Seg_9960" s="T754">adv</ta>
            <ta e="T758" id="Seg_9961" s="T757">v-v:pn-v:mood.pn</ta>
            <ta e="T759" id="Seg_9962" s="T758">conj</ta>
            <ta e="T760" id="Seg_9963" s="T759">v-v:pn-v:mood.pn</ta>
            <ta e="T761" id="Seg_9964" s="T760">ptcl</ta>
            <ta e="T763" id="Seg_9965" s="T762">n-n:num</ta>
            <ta e="T764" id="Seg_9966" s="T763">adv</ta>
            <ta e="T765" id="Seg_9967" s="T764">ptcl</ta>
            <ta e="T767" id="Seg_9968" s="T766">v-v&gt;v-v:pn</ta>
            <ta e="T768" id="Seg_9969" s="T767">v-v&gt;v-v:pn</ta>
            <ta e="T769" id="Seg_9970" s="T768">v-v&gt;v-v:pn</ta>
            <ta e="T770" id="Seg_9971" s="T769">adv</ta>
            <ta e="T771" id="Seg_9972" s="T770">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_9973" s="T771">adj-n:case</ta>
            <ta e="T773" id="Seg_9974" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_9975" s="T773">v-v:n.fin</ta>
            <ta e="T775" id="Seg_9976" s="T774">aux-v:mood.pn</ta>
            <ta e="T776" id="Seg_9977" s="T775">v-v:n.fin</ta>
            <ta e="T777" id="Seg_9978" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_9979" s="T777">adv</ta>
            <ta e="T780" id="Seg_9980" s="T779">aux-v:mood.pn</ta>
            <ta e="T781" id="Seg_9981" s="T780">v-v:mood.pn</ta>
            <ta e="T782" id="Seg_9982" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_9983" s="T782">adj-n:case</ta>
            <ta e="T784" id="Seg_9984" s="T783">v-v:n.fin</ta>
            <ta e="T785" id="Seg_9985" s="T784">adv</ta>
            <ta e="T786" id="Seg_9986" s="T785">v-v&gt;v-v:pn</ta>
            <ta e="T788" id="Seg_9987" s="T787">pers</ta>
            <ta e="T789" id="Seg_9988" s="T788">adv</ta>
            <ta e="T790" id="Seg_9989" s="T789">v-v:tense-v:pn</ta>
            <ta e="T791" id="Seg_9990" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_9991" s="T791">adv</ta>
            <ta e="T793" id="Seg_9992" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_9993" s="T793">v-v:tense-v:pn</ta>
            <ta e="T795" id="Seg_9994" s="T794">pers</ta>
            <ta e="T796" id="Seg_9995" s="T795">v-v:tense-v:pn</ta>
            <ta e="T797" id="Seg_9996" s="T796">adv</ta>
            <ta e="T798" id="Seg_9997" s="T797">n-n:case</ta>
            <ta e="T799" id="Seg_9998" s="T798">v-v:tense-v:pn</ta>
            <ta e="T800" id="Seg_9999" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_10000" s="T800">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_10001" s="T801">n-n:num</ta>
            <ta e="T806" id="Seg_10002" s="T805">v-v&gt;v-v:pn</ta>
            <ta e="T807" id="Seg_10003" s="T806">adv</ta>
            <ta e="T808" id="Seg_10004" s="T807">n-n:num</ta>
            <ta e="T809" id="Seg_10005" s="T808">v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_10006" s="T809">n-n:case</ta>
            <ta e="T811" id="Seg_10007" s="T810">v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_10008" s="T811">v-v:tense-v:pn</ta>
            <ta e="T813" id="Seg_10009" s="T812">adv</ta>
            <ta e="T814" id="Seg_10010" s="T813">n</ta>
            <ta e="T815" id="Seg_10011" s="T814">n-n:case</ta>
            <ta e="T816" id="Seg_10012" s="T815">n</ta>
            <ta e="T817" id="Seg_10013" s="T816">n-n:case</ta>
            <ta e="T818" id="Seg_10014" s="T817">v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_10015" s="T818">v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_10016" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_10017" s="T821">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T823" id="Seg_10018" s="T822">ptcl</ta>
            <ta e="T825" id="Seg_10019" s="T824">v-v:mood.pn</ta>
            <ta e="T826" id="Seg_10020" s="T825">n-n:case-n:case.poss</ta>
            <ta e="T827" id="Seg_10021" s="T826">ptcl</ta>
            <ta e="T828" id="Seg_10022" s="T827">v-v:n.fin</ta>
            <ta e="T829" id="Seg_10023" s="T828">v-v:mood.pn</ta>
            <ta e="T830" id="Seg_10024" s="T829">v-v:ins-v:mood.pn</ta>
            <ta e="T831" id="Seg_10025" s="T830">v-v:n.fin</ta>
            <ta e="T832" id="Seg_10026" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_10027" s="T832">adv</ta>
            <ta e="T835" id="Seg_10028" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_10029" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_10030" s="T836">adv</ta>
            <ta e="T838" id="Seg_10031" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_10032" s="T838">quant</ta>
            <ta e="T840" id="Seg_10033" s="T839">v-v:tense-v:pn</ta>
            <ta e="T841" id="Seg_10034" s="T840">n-n:case</ta>
            <ta e="T842" id="Seg_10035" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_10036" s="T842">v-v:tense-v:pn</ta>
            <ta e="T844" id="Seg_10037" s="T843">n-n:num</ta>
            <ta e="T845" id="Seg_10038" s="T844">adv</ta>
            <ta e="T846" id="Seg_10039" s="T845">quant</ta>
            <ta e="T847" id="Seg_10040" s="T846">n-n:num</ta>
            <ta e="T848" id="Seg_10041" s="T847">n-n:num</ta>
            <ta e="T849" id="Seg_10042" s="T848">n-n:num</ta>
            <ta e="T850" id="Seg_10043" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_10044" s="T850">ptcl</ta>
            <ta e="T853" id="Seg_10045" s="T852">que-n:case</ta>
            <ta e="T854" id="Seg_10046" s="T853">dempro-n:case</ta>
            <ta e="T855" id="Seg_10047" s="T854">adj</ta>
            <ta e="T856" id="Seg_10048" s="T855">n-n:case</ta>
            <ta e="T857" id="Seg_10049" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_10050" s="T857">v-v:tense-v:pn</ta>
            <ta e="T859" id="Seg_10051" s="T858">n-n:case</ta>
            <ta e="T860" id="Seg_10052" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_10053" s="T860">v-v:tense-v:pn</ta>
            <ta e="T862" id="Seg_10054" s="T861">que</ta>
            <ta e="T863" id="Seg_10055" s="T862">dempro-n:num</ta>
            <ta e="T864" id="Seg_10056" s="T863">que-n:case</ta>
            <ta e="T865" id="Seg_10057" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_10058" s="T865">v-v:tense-v:pn</ta>
            <ta e="T867" id="Seg_10059" s="T866">v-v:n.fin</ta>
            <ta e="T868" id="Seg_10060" s="T867">pers-n:case</ta>
            <ta e="T869" id="Seg_10061" s="T868">ptcl</ta>
            <ta e="T871" id="Seg_10062" s="T870">que</ta>
            <ta e="T872" id="Seg_10063" s="T871">dempro-n:num</ta>
            <ta e="T873" id="Seg_10064" s="T872">que-n:case</ta>
            <ta e="T874" id="Seg_10065" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_10066" s="T874">v-v:tense-v:pn</ta>
            <ta e="T876" id="Seg_10067" s="T875">v-v:n.fin</ta>
            <ta e="T878" id="Seg_10068" s="T877">adv</ta>
            <ta e="T879" id="Seg_10069" s="T878">ptcl</ta>
            <ta e="T880" id="Seg_10070" s="T879">v-v:n.fin</ta>
            <ta e="T881" id="Seg_10071" s="T880">v-v:ins-v:mood.pn</ta>
            <ta e="T882" id="Seg_10072" s="T881">n-n:case-n:case.poss</ta>
            <ta e="T883" id="Seg_10073" s="T882">pers</ta>
            <ta e="T884" id="Seg_10074" s="T883">pers</ta>
            <ta e="T886" id="Seg_10075" s="T885">ptcl</ta>
            <ta e="T887" id="Seg_10076" s="T886">ptcl</ta>
            <ta e="T891" id="Seg_10077" s="T890">pers</ta>
            <ta e="T892" id="Seg_10078" s="T891">pers</ta>
            <ta e="T893" id="Seg_10079" s="T892">ptcl</ta>
            <ta e="T894" id="Seg_10080" s="T893">ptcl</ta>
            <ta e="T896" id="Seg_10081" s="T895">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T897" id="Seg_10082" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_10083" s="T897">ptcl</ta>
            <ta e="T899" id="Seg_10084" s="T898">adj-n:case</ta>
            <ta e="T900" id="Seg_10085" s="T899">v-v:n.fin</ta>
            <ta e="T901" id="Seg_10086" s="T900">adv</ta>
            <ta e="T902" id="Seg_10087" s="T901">v-v:n.fin</ta>
            <ta e="T903" id="Seg_10088" s="T902">n-n:case</ta>
            <ta e="T904" id="Seg_10089" s="T903">dempro</ta>
            <ta e="T905" id="Seg_10090" s="T904">n-n:num</ta>
            <ta e="T906" id="Seg_10091" s="T905">n-n:case</ta>
            <ta e="T908" id="Seg_10092" s="T907">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T16" id="Seg_10093" s="T13">v</ta>
            <ta e="T17" id="Seg_10094" s="T16">adv</ta>
            <ta e="T18" id="Seg_10095" s="T17">v</ta>
            <ta e="T19" id="Seg_10096" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_10097" s="T19">adv</ta>
            <ta e="T21" id="Seg_10098" s="T20">n</ta>
            <ta e="T22" id="Seg_10099" s="T21">v</ta>
            <ta e="T23" id="Seg_10100" s="T22">n</ta>
            <ta e="T24" id="Seg_10101" s="T23">v</ta>
            <ta e="T25" id="Seg_10102" s="T24">adv</ta>
            <ta e="T26" id="Seg_10103" s="T25">n</ta>
            <ta e="T27" id="Seg_10104" s="T26">v</ta>
            <ta e="T28" id="Seg_10105" s="T27">adv</ta>
            <ta e="T29" id="Seg_10106" s="T28">v</ta>
            <ta e="T30" id="Seg_10107" s="T29">adj</ta>
            <ta e="T31" id="Seg_10108" s="T30">v</ta>
            <ta e="T32" id="Seg_10109" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_10110" s="T32">adj</ta>
            <ta e="T34" id="Seg_10111" s="T33">v</ta>
            <ta e="T35" id="Seg_10112" s="T34">adv</ta>
            <ta e="T36" id="Seg_10113" s="T35">v</ta>
            <ta e="T37" id="Seg_10114" s="T36">ptcl</ta>
            <ta e="T40" id="Seg_10115" s="T39">adv</ta>
            <ta e="T41" id="Seg_10116" s="T40">v</ta>
            <ta e="T42" id="Seg_10117" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_10118" s="T42">adv</ta>
            <ta e="T44" id="Seg_10119" s="T43">n</ta>
            <ta e="T45" id="Seg_10120" s="T44">v</ta>
            <ta e="T46" id="Seg_10121" s="T45">adv</ta>
            <ta e="T47" id="Seg_10122" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_10123" s="T47">v</ta>
            <ta e="T49" id="Seg_10124" s="T48">adv</ta>
            <ta e="T50" id="Seg_10125" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_10126" s="T50">v</ta>
            <ta e="T52" id="Seg_10127" s="T51">adv</ta>
            <ta e="T53" id="Seg_10128" s="T52">v</ta>
            <ta e="T54" id="Seg_10129" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_10130" s="T54">adv</ta>
            <ta e="T56" id="Seg_10131" s="T55">v</ta>
            <ta e="T57" id="Seg_10132" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_10133" s="T57">adv</ta>
            <ta e="T59" id="Seg_10134" s="T58">v</ta>
            <ta e="T60" id="Seg_10135" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_10136" s="T60">quant</ta>
            <ta e="T63" id="Seg_10137" s="T62">pers</ta>
            <ta e="T64" id="Seg_10138" s="T63">pers</ta>
            <ta e="T65" id="Seg_10139" s="T64">v</ta>
            <ta e="T66" id="Seg_10140" s="T65">que</ta>
            <ta e="T67" id="Seg_10141" s="T66">pers</ta>
            <ta e="T68" id="Seg_10142" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_10143" s="T68">v</ta>
            <ta e="T70" id="Seg_10144" s="T69">pers</ta>
            <ta e="T71" id="Seg_10145" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_10146" s="T71">n</ta>
            <ta e="T74" id="Seg_10147" s="T73">v</ta>
            <ta e="T75" id="Seg_10148" s="T74">adv</ta>
            <ta e="T76" id="Seg_10149" s="T75">pers</ta>
            <ta e="T77" id="Seg_10150" s="T76">n</ta>
            <ta e="T78" id="Seg_10151" s="T77">v</ta>
            <ta e="T79" id="Seg_10152" s="T78">pers</ta>
            <ta e="T80" id="Seg_10153" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_10154" s="T80">v</ta>
            <ta e="T82" id="Seg_10155" s="T81">dempro</ta>
            <ta e="T83" id="Seg_10156" s="T82">ptcl</ta>
            <ta e="T85" id="Seg_10157" s="T84">pers</ta>
            <ta e="T86" id="Seg_10158" s="T85">que</ta>
            <ta e="T87" id="Seg_10159" s="T86">pers</ta>
            <ta e="T88" id="Seg_10160" s="T87">ptcl</ta>
            <ta e="T90" id="Seg_10161" s="T89">v</ta>
            <ta e="T91" id="Seg_10162" s="T90">conj</ta>
            <ta e="T92" id="Seg_10163" s="T91">v</ta>
            <ta e="T93" id="Seg_10164" s="T92">n</ta>
            <ta e="T94" id="Seg_10165" s="T93">adv</ta>
            <ta e="T95" id="Seg_10166" s="T94">pers</ta>
            <ta e="T96" id="Seg_10167" s="T95">n</ta>
            <ta e="T97" id="Seg_10168" s="T96">v</ta>
            <ta e="T98" id="Seg_10169" s="T97">pers</ta>
            <ta e="T99" id="Seg_10170" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_10171" s="T99">n</ta>
            <ta e="T101" id="Seg_10172" s="T100">pers</ta>
            <ta e="T102" id="Seg_10173" s="T101">v</ta>
            <ta e="T103" id="Seg_10174" s="T102">dempro</ta>
            <ta e="T104" id="Seg_10175" s="T103">n</ta>
            <ta e="T105" id="Seg_10176" s="T104">v</ta>
            <ta e="T106" id="Seg_10177" s="T105">dempro</ta>
            <ta e="T108" id="Seg_10178" s="T107">dempro</ta>
            <ta e="T109" id="Seg_10179" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_10180" s="T109">v</ta>
            <ta e="T111" id="Seg_10181" s="T110">pers</ta>
            <ta e="T112" id="Seg_10182" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_10183" s="T112">v</ta>
            <ta e="T114" id="Seg_10184" s="T113">dempro</ta>
            <ta e="T115" id="Seg_10185" s="T114">ptcl</ta>
            <ta e="T117" id="Seg_10186" s="T116">v</ta>
            <ta e="T118" id="Seg_10187" s="T117">n</ta>
            <ta e="T119" id="Seg_10188" s="T118">v</ta>
            <ta e="T120" id="Seg_10189" s="T119">conj</ta>
            <ta e="T121" id="Seg_10190" s="T120">que</ta>
            <ta e="T122" id="Seg_10191" s="T121">n</ta>
            <ta e="T123" id="Seg_10192" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_10193" s="T123">pers</ta>
            <ta e="T125" id="Seg_10194" s="T124">v</ta>
            <ta e="T126" id="Seg_10195" s="T125">adv</ta>
            <ta e="T127" id="Seg_10196" s="T126">n</ta>
            <ta e="T128" id="Seg_10197" s="T127">v</ta>
            <ta e="T129" id="Seg_10198" s="T128">pers</ta>
            <ta e="T130" id="Seg_10199" s="T129">adv</ta>
            <ta e="T131" id="Seg_10200" s="T130">n</ta>
            <ta e="T132" id="Seg_10201" s="T131">v</ta>
            <ta e="T133" id="Seg_10202" s="T132">conj</ta>
            <ta e="T134" id="Seg_10203" s="T133">n</ta>
            <ta e="T135" id="Seg_10204" s="T134">v</ta>
            <ta e="T136" id="Seg_10205" s="T135">adj</ta>
            <ta e="T138" id="Seg_10206" s="T137">n</ta>
            <ta e="T139" id="Seg_10207" s="T138">v</ta>
            <ta e="T140" id="Seg_10208" s="T139">adv</ta>
            <ta e="T141" id="Seg_10209" s="T140">v</ta>
            <ta e="T142" id="Seg_10210" s="T141">v</ta>
            <ta e="T143" id="Seg_10211" s="T142">v</ta>
            <ta e="T145" id="Seg_10212" s="T144">n</ta>
            <ta e="T146" id="Seg_10213" s="T145">v</ta>
            <ta e="T147" id="Seg_10214" s="T146">que</ta>
            <ta e="T911" id="Seg_10215" s="T147">v</ta>
            <ta e="T148" id="Seg_10216" s="T911">v</ta>
            <ta e="T150" id="Seg_10217" s="T149">v</ta>
            <ta e="T151" id="Seg_10218" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10219" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_10220" s="T152">que</ta>
            <ta e="T154" id="Seg_10221" s="T153">v</ta>
            <ta e="T155" id="Seg_10222" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_10223" s="T155">n</ta>
            <ta e="T157" id="Seg_10224" s="T156">v</ta>
            <ta e="T158" id="Seg_10225" s="T157">v</ta>
            <ta e="T159" id="Seg_10226" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_10227" s="T159">adv</ta>
            <ta e="T161" id="Seg_10228" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_10229" s="T162">num</ta>
            <ta e="T164" id="Seg_10230" s="T163">num</ta>
            <ta e="T165" id="Seg_10231" s="T164">num</ta>
            <ta e="T166" id="Seg_10232" s="T165">num</ta>
            <ta e="T167" id="Seg_10233" s="T166">num</ta>
            <ta e="T168" id="Seg_10234" s="T167">num</ta>
            <ta e="T169" id="Seg_10235" s="T168">num</ta>
            <ta e="T170" id="Seg_10236" s="T169">num</ta>
            <ta e="T174" id="Seg_10237" s="T173">conj</ta>
            <ta e="T175" id="Seg_10238" s="T174">refl</ta>
            <ta e="T177" id="Seg_10239" s="T176">n</ta>
            <ta e="T178" id="Seg_10240" s="T177">v</ta>
            <ta e="T179" id="Seg_10241" s="T178">adv</ta>
            <ta e="T180" id="Seg_10242" s="T179">v</ta>
            <ta e="T181" id="Seg_10243" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_10244" s="T181">conj</ta>
            <ta e="T183" id="Seg_10245" s="T182">n</ta>
            <ta e="T184" id="Seg_10246" s="T183">v</ta>
            <ta e="T185" id="Seg_10247" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_10248" s="T185">refl</ta>
            <ta e="T187" id="Seg_10249" s="T186">n</ta>
            <ta e="T188" id="Seg_10250" s="T187">v</ta>
            <ta e="T189" id="Seg_10251" s="T188">quant</ta>
            <ta e="T191" id="Seg_10252" s="T190">pers</ta>
            <ta e="T192" id="Seg_10253" s="T191">n</ta>
            <ta e="T193" id="Seg_10254" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_10255" s="T193">adj</ta>
            <ta e="T195" id="Seg_10256" s="T194">n</ta>
            <ta e="T196" id="Seg_10257" s="T195">v</ta>
            <ta e="T197" id="Seg_10258" s="T196">v</ta>
            <ta e="T198" id="Seg_10259" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_10260" s="T198">v</ta>
            <ta e="T200" id="Seg_10261" s="T199">refl</ta>
            <ta e="T201" id="Seg_10262" s="T200">n</ta>
            <ta e="T202" id="Seg_10263" s="T201">v</ta>
            <ta e="T203" id="Seg_10264" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_10265" s="T203">adv</ta>
            <ta e="T205" id="Seg_10266" s="T204">dempro</ta>
            <ta e="T206" id="Seg_10267" s="T205">adj</ta>
            <ta e="T207" id="Seg_10268" s="T206">adj</ta>
            <ta e="T209" id="Seg_10269" s="T208">v</ta>
            <ta e="T210" id="Seg_10270" s="T209">que</ta>
            <ta e="T211" id="Seg_10271" s="T210">dempro</ta>
            <ta e="T212" id="Seg_10272" s="T211">adv</ta>
            <ta e="T213" id="Seg_10273" s="T212">adv</ta>
            <ta e="T912" id="Seg_10274" s="T213">v</ta>
            <ta e="T214" id="Seg_10275" s="T912">v</ta>
            <ta e="T215" id="Seg_10276" s="T214">n</ta>
            <ta e="T216" id="Seg_10277" s="T215">v</ta>
            <ta e="T217" id="Seg_10278" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_10279" s="T217">dempro</ta>
            <ta e="T219" id="Seg_10280" s="T218">adv</ta>
            <ta e="T220" id="Seg_10281" s="T219">ptcl</ta>
            <ta e="T222" id="Seg_10282" s="T221">pers</ta>
            <ta e="T223" id="Seg_10283" s="T222">n</ta>
            <ta e="T224" id="Seg_10284" s="T223">v</ta>
            <ta e="T225" id="Seg_10285" s="T224">que</ta>
            <ta e="T226" id="Seg_10286" s="T225">v</ta>
            <ta e="T227" id="Seg_10287" s="T226">v</ta>
            <ta e="T228" id="Seg_10288" s="T227">n</ta>
            <ta e="T229" id="Seg_10289" s="T228">n</ta>
            <ta e="T230" id="Seg_10290" s="T229">v</ta>
            <ta e="T231" id="Seg_10291" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_10292" s="T231">v</ta>
            <ta e="T233" id="Seg_10293" s="T232">adv</ta>
            <ta e="T235" id="Seg_10294" s="T234">pers</ta>
            <ta e="T236" id="Seg_10295" s="T235">adv</ta>
            <ta e="T237" id="Seg_10296" s="T236">v</ta>
            <ta e="T238" id="Seg_10297" s="T237">dempro</ta>
            <ta e="T239" id="Seg_10298" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_10299" s="T239">v</ta>
            <ta e="T241" id="Seg_10300" s="T240">n</ta>
            <ta e="T244" id="Seg_10301" s="T243">n</ta>
            <ta e="T245" id="Seg_10302" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_10303" s="T245">v</ta>
            <ta e="T247" id="Seg_10304" s="T246">v</ta>
            <ta e="T248" id="Seg_10305" s="T247">v</ta>
            <ta e="T249" id="Seg_10306" s="T248">ptcl</ta>
            <ta e="T251" id="Seg_10307" s="T910">n</ta>
            <ta e="T252" id="Seg_10308" s="T251">ptcl</ta>
            <ta e="T254" id="Seg_10309" s="T252">v</ta>
            <ta e="T255" id="Seg_10310" s="T254">ptcl</ta>
            <ta e="T256" id="Seg_10311" s="T255">v</ta>
            <ta e="T257" id="Seg_10312" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_10313" s="T257">v</ta>
            <ta e="T259" id="Seg_10314" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_10315" s="T259">v</ta>
            <ta e="T261" id="Seg_10316" s="T260">adv</ta>
            <ta e="T262" id="Seg_10317" s="T261">quant</ta>
            <ta e="T265" id="Seg_10318" s="T264">que</ta>
            <ta e="T266" id="Seg_10319" s="T265">v</ta>
            <ta e="T267" id="Seg_10320" s="T266">v</ta>
            <ta e="T268" id="Seg_10321" s="T267">adv</ta>
            <ta e="T269" id="Seg_10322" s="T268">pers</ta>
            <ta e="T270" id="Seg_10323" s="T269">adv</ta>
            <ta e="T271" id="Seg_10324" s="T270">v</ta>
            <ta e="T272" id="Seg_10325" s="T271">que</ta>
            <ta e="T274" id="Seg_10326" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_10327" s="T274">v</ta>
            <ta e="T276" id="Seg_10328" s="T275">conj</ta>
            <ta e="T277" id="Seg_10329" s="T276">pers</ta>
            <ta e="T278" id="Seg_10330" s="T277">adv</ta>
            <ta e="T279" id="Seg_10331" s="T278">v</ta>
            <ta e="T280" id="Seg_10332" s="T279">v</ta>
            <ta e="T283" id="Seg_10333" s="T282">dempro</ta>
            <ta e="T284" id="Seg_10334" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_10335" s="T284">v</ta>
            <ta e="T286" id="Seg_10336" s="T285">v</ta>
            <ta e="T287" id="Seg_10337" s="T286">conj</ta>
            <ta e="T288" id="Seg_10338" s="T287">pers</ta>
            <ta e="T289" id="Seg_10339" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_10340" s="T289">v</ta>
            <ta e="T291" id="Seg_10341" s="T290">n</ta>
            <ta e="T292" id="Seg_10342" s="T291">v</ta>
            <ta e="T293" id="Seg_10343" s="T292">v</ta>
            <ta e="T294" id="Seg_10344" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_10345" s="T294">pers</ta>
            <ta e="T296" id="Seg_10346" s="T295">ptcl</ta>
            <ta e="T298" id="Seg_10347" s="T297">adv</ta>
            <ta e="T299" id="Seg_10348" s="T298">adv</ta>
            <ta e="T300" id="Seg_10349" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_10350" s="T300">n</ta>
            <ta e="T302" id="Seg_10351" s="T301">v</ta>
            <ta e="T303" id="Seg_10352" s="T302">conj</ta>
            <ta e="T305" id="Seg_10353" s="T304">num</ta>
            <ta e="T306" id="Seg_10354" s="T305">n</ta>
            <ta e="T307" id="Seg_10355" s="T306">v</ta>
            <ta e="T308" id="Seg_10356" s="T307">dempro</ta>
            <ta e="T309" id="Seg_10357" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_10358" s="T309">n</ta>
            <ta e="T311" id="Seg_10359" s="T310">v</ta>
            <ta e="T312" id="Seg_10360" s="T311">ptcl</ta>
            <ta e="T314" id="Seg_10361" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_10362" s="T314">v</ta>
            <ta e="T316" id="Seg_10363" s="T315">adv</ta>
            <ta e="T317" id="Seg_10364" s="T316">n</ta>
            <ta e="T318" id="Seg_10365" s="T317">v</ta>
            <ta e="T319" id="Seg_10366" s="T318">pers</ta>
            <ta e="T320" id="Seg_10367" s="T319">v</ta>
            <ta e="T321" id="Seg_10368" s="T320">pers</ta>
            <ta e="T322" id="Seg_10369" s="T321">n</ta>
            <ta e="T323" id="Seg_10370" s="T322">v</ta>
            <ta e="T324" id="Seg_10371" s="T323">v</ta>
            <ta e="T325" id="Seg_10372" s="T324">pers</ta>
            <ta e="T326" id="Seg_10373" s="T325">adv</ta>
            <ta e="T327" id="Seg_10374" s="T326">num</ta>
            <ta e="T328" id="Seg_10375" s="T327">n</ta>
            <ta e="T329" id="Seg_10376" s="T328">v</ta>
            <ta e="T330" id="Seg_10377" s="T329">v</ta>
            <ta e="T331" id="Seg_10378" s="T330">adv</ta>
            <ta e="T332" id="Seg_10379" s="T331">v</ta>
            <ta e="T333" id="Seg_10380" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_10381" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_10382" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_10383" s="T335">v</ta>
            <ta e="T338" id="Seg_10384" s="T337">adv</ta>
            <ta e="T339" id="Seg_10385" s="T338">adv</ta>
            <ta e="T340" id="Seg_10386" s="T339">adj</ta>
            <ta e="T341" id="Seg_10387" s="T340">n</ta>
            <ta e="T342" id="Seg_10388" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_10389" s="T342">n</ta>
            <ta e="T344" id="Seg_10390" s="T343">v</ta>
            <ta e="T346" id="Seg_10391" s="T345">n</ta>
            <ta e="T347" id="Seg_10392" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_10393" s="T347">v</ta>
            <ta e="T349" id="Seg_10394" s="T348">adv</ta>
            <ta e="T350" id="Seg_10395" s="T349">v</ta>
            <ta e="T351" id="Seg_10396" s="T350">n</ta>
            <ta e="T352" id="Seg_10397" s="T351">que</ta>
            <ta e="T353" id="Seg_10398" s="T352">v</ta>
            <ta e="T354" id="Seg_10399" s="T353">v</ta>
            <ta e="T356" id="Seg_10400" s="T355">v</ta>
            <ta e="T357" id="Seg_10401" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_10402" s="T357">n</ta>
            <ta e="T359" id="Seg_10403" s="T358">v</ta>
            <ta e="T360" id="Seg_10404" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_10405" s="T360">v</ta>
            <ta e="T362" id="Seg_10406" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_10407" s="T362">v</ta>
            <ta e="T365" id="Seg_10408" s="T364">pers</ta>
            <ta e="T366" id="Seg_10409" s="T365">adv</ta>
            <ta e="T367" id="Seg_10410" s="T366">adj</ta>
            <ta e="T368" id="Seg_10411" s="T367">v</ta>
            <ta e="T369" id="Seg_10412" s="T368">n</ta>
            <ta e="T370" id="Seg_10413" s="T369">v</ta>
            <ta e="T371" id="Seg_10414" s="T370">n</ta>
            <ta e="T372" id="Seg_10415" s="T371">v</ta>
            <ta e="T373" id="Seg_10416" s="T372">adv</ta>
            <ta e="T374" id="Seg_10417" s="T373">n</ta>
            <ta e="T375" id="Seg_10418" s="T374">v</ta>
            <ta e="T376" id="Seg_10419" s="T375">v</ta>
            <ta e="T377" id="Seg_10420" s="T376">ptcl</ta>
            <ta e="T379" id="Seg_10421" s="T378">adv</ta>
            <ta e="T380" id="Seg_10422" s="T379">adj</ta>
            <ta e="T381" id="Seg_10423" s="T380">adv</ta>
            <ta e="T382" id="Seg_10424" s="T381">v</ta>
            <ta e="T383" id="Seg_10425" s="T382">n</ta>
            <ta e="T384" id="Seg_10426" s="T383">v</ta>
            <ta e="T385" id="Seg_10427" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_10428" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_10429" s="T386">n</ta>
            <ta e="T388" id="Seg_10430" s="T387">pers</ta>
            <ta e="T391" id="Seg_10431" s="T390">v</ta>
            <ta e="T392" id="Seg_10432" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_10433" s="T392">adj</ta>
            <ta e="T394" id="Seg_10434" s="T393">ptcl</ta>
            <ta e="T396" id="Seg_10435" s="T395">dempro</ta>
            <ta e="T397" id="Seg_10436" s="T396">n</ta>
            <ta e="T398" id="Seg_10437" s="T397">v</ta>
            <ta e="T399" id="Seg_10438" s="T398">pers</ta>
            <ta e="T400" id="Seg_10439" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_10440" s="T400">v</ta>
            <ta e="T402" id="Seg_10441" s="T401">pers</ta>
            <ta e="T403" id="Seg_10442" s="T402">pers</ta>
            <ta e="T404" id="Seg_10443" s="T403">adv</ta>
            <ta e="T405" id="Seg_10444" s="T404">n</ta>
            <ta e="T406" id="Seg_10445" s="T405">v</ta>
            <ta e="T407" id="Seg_10446" s="T406">pers</ta>
            <ta e="T408" id="Seg_10447" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_10448" s="T408">n</ta>
            <ta e="T410" id="Seg_10449" s="T409">v</ta>
            <ta e="T411" id="Seg_10450" s="T410">dempro</ta>
            <ta e="T412" id="Seg_10451" s="T411">dempro</ta>
            <ta e="T413" id="Seg_10452" s="T412">n</ta>
            <ta e="T414" id="Seg_10453" s="T413">v</ta>
            <ta e="T415" id="Seg_10454" s="T414">dempro</ta>
            <ta e="T416" id="Seg_10455" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_10456" s="T416">v</ta>
            <ta e="T418" id="Seg_10457" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_10458" s="T418">dempro</ta>
            <ta e="T421" id="Seg_10459" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_10460" s="T421">adv</ta>
            <ta e="T423" id="Seg_10461" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_10462" s="T423">v</ta>
            <ta e="T426" id="Seg_10463" s="T425">adj</ta>
            <ta e="T427" id="Seg_10464" s="T426">n</ta>
            <ta e="T428" id="Seg_10465" s="T427">v</ta>
            <ta e="T429" id="Seg_10466" s="T428">adj</ta>
            <ta e="T430" id="Seg_10467" s="T429">n</ta>
            <ta e="T431" id="Seg_10468" s="T430">v</ta>
            <ta e="T432" id="Seg_10469" s="T431">v</ta>
            <ta e="T433" id="Seg_10470" s="T432">ptcl</ta>
            <ta e="T434" id="Seg_10471" s="T433">v</ta>
            <ta e="T435" id="Seg_10472" s="T434">adv</ta>
            <ta e="T436" id="Seg_10473" s="T435">n</ta>
            <ta e="T437" id="Seg_10474" s="T436">ptcl</ta>
            <ta e="T439" id="Seg_10475" s="T438">v</ta>
            <ta e="T440" id="Seg_10476" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_10477" s="T440">v</ta>
            <ta e="T442" id="Seg_10478" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_10479" s="T442">n</ta>
            <ta e="T444" id="Seg_10480" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_10481" s="T444">v</ta>
            <ta e="T446" id="Seg_10482" s="T445">pers</ta>
            <ta e="T447" id="Seg_10483" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_10484" s="T447">adv</ta>
            <ta e="T449" id="Seg_10485" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_10486" s="T449">v</ta>
            <ta e="T452" id="Seg_10487" s="T451">ptcl</ta>
            <ta e="T453" id="Seg_10488" s="T452">pers</ta>
            <ta e="T454" id="Seg_10489" s="T453">n</ta>
            <ta e="T455" id="Seg_10490" s="T454">pers</ta>
            <ta e="T456" id="Seg_10491" s="T455">v</ta>
            <ta e="T457" id="Seg_10492" s="T456">dempro</ta>
            <ta e="T458" id="Seg_10493" s="T457">adv</ta>
            <ta e="T459" id="Seg_10494" s="T458">adj</ta>
            <ta e="T460" id="Seg_10495" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_10496" s="T460">v</ta>
            <ta e="T462" id="Seg_10497" s="T461">quant</ta>
            <ta e="T464" id="Seg_10498" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_10499" s="T464">v</ta>
            <ta e="T466" id="Seg_10500" s="T465">dempro</ta>
            <ta e="T467" id="Seg_10501" s="T466">v</ta>
            <ta e="T468" id="Seg_10502" s="T467">adv</ta>
            <ta e="T469" id="Seg_10503" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_10504" s="T469">v</ta>
            <ta e="T471" id="Seg_10505" s="T470">dempro</ta>
            <ta e="T472" id="Seg_10506" s="T471">adv</ta>
            <ta e="T475" id="Seg_10507" s="T474">v</ta>
            <ta e="T476" id="Seg_10508" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_10509" s="T476">adv</ta>
            <ta e="T479" id="Seg_10510" s="T478">v</ta>
            <ta e="T480" id="Seg_10511" s="T479">v</ta>
            <ta e="T481" id="Seg_10512" s="T480">n</ta>
            <ta e="T486" id="Seg_10513" s="T485">adv</ta>
            <ta e="T487" id="Seg_10514" s="T486">adj</ta>
            <ta e="T488" id="Seg_10515" s="T487">n</ta>
            <ta e="T489" id="Seg_10516" s="T488">conj</ta>
            <ta e="T490" id="Seg_10517" s="T489">n</ta>
            <ta e="T491" id="Seg_10518" s="T490">adv</ta>
            <ta e="T492" id="Seg_10519" s="T491">v</ta>
            <ta e="T493" id="Seg_10520" s="T492">n</ta>
            <ta e="T494" id="Seg_10521" s="T493">v</ta>
            <ta e="T495" id="Seg_10522" s="T494">v</ta>
            <ta e="T496" id="Seg_10523" s="T495">conj</ta>
            <ta e="T497" id="Seg_10524" s="T496">pers</ta>
            <ta e="T498" id="Seg_10525" s="T497">v</ta>
            <ta e="T499" id="Seg_10526" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_10527" s="T499">adv</ta>
            <ta e="T501" id="Seg_10528" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_10529" s="T501">v</ta>
            <ta e="T503" id="Seg_10530" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_10531" s="T503">ptcl</ta>
            <ta e="T506" id="Seg_10532" s="T505">adv</ta>
            <ta e="T507" id="Seg_10533" s="T506">adj</ta>
            <ta e="T508" id="Seg_10534" s="T507">n</ta>
            <ta e="T509" id="Seg_10535" s="T508">pers</ta>
            <ta e="T510" id="Seg_10536" s="T509">n</ta>
            <ta e="T511" id="Seg_10537" s="T510">v</ta>
            <ta e="T512" id="Seg_10538" s="T511">v</ta>
            <ta e="T513" id="Seg_10539" s="T512">adv</ta>
            <ta e="T514" id="Seg_10540" s="T513">n</ta>
            <ta e="T515" id="Seg_10541" s="T514">v</ta>
            <ta e="T516" id="Seg_10542" s="T515">adv</ta>
            <ta e="T517" id="Seg_10543" s="T516">v</ta>
            <ta e="T518" id="Seg_10544" s="T517">n</ta>
            <ta e="T520" id="Seg_10545" s="T519">pers</ta>
            <ta e="T521" id="Seg_10546" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10547" s="T521">v</ta>
            <ta e="T523" id="Seg_10548" s="T522">ptcl</ta>
            <ta e="T525" id="Seg_10549" s="T524">num</ta>
            <ta e="T526" id="Seg_10550" s="T525">num</ta>
            <ta e="T527" id="Seg_10551" s="T526">num</ta>
            <ta e="T528" id="Seg_10552" s="T527">num</ta>
            <ta e="T529" id="Seg_10553" s="T528">num</ta>
            <ta e="T530" id="Seg_10554" s="T529">num</ta>
            <ta e="T531" id="Seg_10555" s="T530">num</ta>
            <ta e="T532" id="Seg_10556" s="T531">num</ta>
            <ta e="T533" id="Seg_10557" s="T532">num</ta>
            <ta e="T541" id="Seg_10558" s="T540">num</ta>
            <ta e="T543" id="Seg_10559" s="T542">num</ta>
            <ta e="T544" id="Seg_10560" s="T543">num</ta>
            <ta e="T545" id="Seg_10561" s="T544">num</ta>
            <ta e="T546" id="Seg_10562" s="T545">num</ta>
            <ta e="T547" id="Seg_10563" s="T546">num</ta>
            <ta e="T548" id="Seg_10564" s="T547">num</ta>
            <ta e="T549" id="Seg_10565" s="T548">num</ta>
            <ta e="T550" id="Seg_10566" s="T549">num</ta>
            <ta e="T552" id="Seg_10567" s="T551">pers</ta>
            <ta e="T553" id="Seg_10568" s="T552">adv</ta>
            <ta e="T554" id="Seg_10569" s="T553">v</ta>
            <ta e="T555" id="Seg_10570" s="T554">conj</ta>
            <ta e="T556" id="Seg_10571" s="T555">dempro</ta>
            <ta e="T557" id="Seg_10572" s="T556">ptcl</ta>
            <ta e="T558" id="Seg_10573" s="T557">num</ta>
            <ta e="T559" id="Seg_10574" s="T558">v</ta>
            <ta e="T561" id="Seg_10575" s="T560">pers</ta>
            <ta e="T562" id="Seg_10576" s="T561">n</ta>
            <ta e="T564" id="Seg_10577" s="T563">v</ta>
            <ta e="T565" id="Seg_10578" s="T564">adv</ta>
            <ta e="T566" id="Seg_10579" s="T565">adj</ta>
            <ta e="T567" id="Seg_10580" s="T566">n</ta>
            <ta e="T568" id="Seg_10581" s="T567">adj</ta>
            <ta e="T569" id="Seg_10582" s="T568">n</ta>
            <ta e="T570" id="Seg_10583" s="T569">n</ta>
            <ta e="T571" id="Seg_10584" s="T570">v</ta>
            <ta e="T572" id="Seg_10585" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_10586" s="T572">n</ta>
            <ta e="T574" id="Seg_10587" s="T573">v</ta>
            <ta e="T575" id="Seg_10588" s="T574">n</ta>
            <ta e="T576" id="Seg_10589" s="T575">v</ta>
            <ta e="T577" id="Seg_10590" s="T576">v</ta>
            <ta e="T578" id="Seg_10591" s="T577">v</ta>
            <ta e="T579" id="Seg_10592" s="T578">adv</ta>
            <ta e="T580" id="Seg_10593" s="T579">ptcl</ta>
            <ta e="T582" id="Seg_10594" s="T581">adv</ta>
            <ta e="T583" id="Seg_10595" s="T582">adj</ta>
            <ta e="T584" id="Seg_10596" s="T583">n</ta>
            <ta e="T585" id="Seg_10597" s="T584">pers</ta>
            <ta e="T586" id="Seg_10598" s="T585">dempro</ta>
            <ta e="T587" id="Seg_10599" s="T586">v</ta>
            <ta e="T588" id="Seg_10600" s="T587">dempro</ta>
            <ta e="T589" id="Seg_10601" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_10602" s="T589">v</ta>
            <ta e="T591" id="Seg_10603" s="T590">pers</ta>
            <ta e="T592" id="Seg_10604" s="T591">v</ta>
            <ta e="T593" id="Seg_10605" s="T592">conj</ta>
            <ta e="T594" id="Seg_10606" s="T593">dempro</ta>
            <ta e="T595" id="Seg_10607" s="T594">pers</ta>
            <ta e="T596" id="Seg_10608" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_10609" s="T596">v</ta>
            <ta e="T598" id="Seg_10610" s="T597">pers</ta>
            <ta e="T599" id="Seg_10611" s="T598">n</ta>
            <ta e="T600" id="Seg_10612" s="T599">v</ta>
            <ta e="T601" id="Seg_10613" s="T600">dempro</ta>
            <ta e="T602" id="Seg_10614" s="T601">pers</ta>
            <ta e="T603" id="Seg_10615" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_10616" s="T603">adv</ta>
            <ta e="T605" id="Seg_10617" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_10618" s="T605">v</ta>
            <ta e="T607" id="Seg_10619" s="T606">ptcl</ta>
            <ta e="T609" id="Seg_10620" s="T608">n</ta>
            <ta e="T610" id="Seg_10621" s="T609">quant</ta>
            <ta e="T611" id="Seg_10622" s="T610">v</ta>
            <ta e="T612" id="Seg_10623" s="T611">que</ta>
            <ta e="T613" id="Seg_10624" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_10625" s="T613">v</ta>
            <ta e="T615" id="Seg_10626" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_10627" s="T615">v</ta>
            <ta e="T617" id="Seg_10628" s="T616">v</ta>
            <ta e="T619" id="Seg_10629" s="T618">que</ta>
            <ta e="T620" id="Seg_10630" s="T619">v</ta>
            <ta e="T621" id="Seg_10631" s="T620">pers</ta>
            <ta e="T622" id="Seg_10632" s="T621">adv</ta>
            <ta e="T625" id="Seg_10633" s="T624">v</ta>
            <ta e="T626" id="Seg_10634" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_10635" s="T626">adv</ta>
            <ta e="T628" id="Seg_10636" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_10637" s="T628">v</ta>
            <ta e="T635" id="Seg_10638" s="T634">dempro</ta>
            <ta e="T636" id="Seg_10639" s="T635">n</ta>
            <ta e="T637" id="Seg_10640" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_10641" s="T637">adj</ta>
            <ta e="T639" id="Seg_10642" s="T638">n</ta>
            <ta e="T640" id="Seg_10643" s="T639">v</ta>
            <ta e="T641" id="Seg_10644" s="T640">adv</ta>
            <ta e="T642" id="Seg_10645" s="T641">v</ta>
            <ta e="T643" id="Seg_10646" s="T642">n</ta>
            <ta e="T644" id="Seg_10647" s="T643">adv</ta>
            <ta e="T645" id="Seg_10648" s="T644">n</ta>
            <ta e="T646" id="Seg_10649" s="T645">v</ta>
            <ta e="T647" id="Seg_10650" s="T646">n</ta>
            <ta e="T648" id="Seg_10651" s="T647">n</ta>
            <ta e="T650" id="Seg_10652" s="T649">n</ta>
            <ta e="T651" id="Seg_10653" s="T650">v</ta>
            <ta e="T652" id="Seg_10654" s="T651">v</ta>
            <ta e="T653" id="Seg_10655" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10656" s="T653">v</ta>
            <ta e="T655" id="Seg_10657" s="T654">v</ta>
            <ta e="T656" id="Seg_10658" s="T655">v</ta>
            <ta e="T657" id="Seg_10659" s="T656">v</ta>
            <ta e="T658" id="Seg_10660" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_10661" s="T658">ptcl</ta>
            <ta e="T661" id="Seg_10662" s="T660">n</ta>
            <ta e="T662" id="Seg_10663" s="T661">n</ta>
            <ta e="T663" id="Seg_10664" s="T662">ptcl</ta>
            <ta e="T664" id="Seg_10665" s="T663">quant</ta>
            <ta e="T665" id="Seg_10666" s="T664">v</ta>
            <ta e="T666" id="Seg_10667" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_10668" s="T666">v</ta>
            <ta e="T668" id="Seg_10669" s="T667">n</ta>
            <ta e="T669" id="Seg_10670" s="T668">v</ta>
            <ta e="T670" id="Seg_10671" s="T669">v</ta>
            <ta e="T671" id="Seg_10672" s="T670">ptcl</ta>
            <ta e="T673" id="Seg_10673" s="T672">pers</ta>
            <ta e="T674" id="Seg_10674" s="T673">n</ta>
            <ta e="T675" id="Seg_10675" s="T674">v</ta>
            <ta e="T676" id="Seg_10676" s="T675">n</ta>
            <ta e="T677" id="Seg_10677" s="T676">v</ta>
            <ta e="T678" id="Seg_10678" s="T677">n</ta>
            <ta e="T679" id="Seg_10679" s="T678">v</ta>
            <ta e="T680" id="Seg_10680" s="T679">n</ta>
            <ta e="T681" id="Seg_10681" s="T680">v</ta>
            <ta e="T682" id="Seg_10682" s="T681">pers</ta>
            <ta e="T683" id="Seg_10683" s="T682">v</ta>
            <ta e="T684" id="Seg_10684" s="T683">ptcl</ta>
            <ta e="T686" id="Seg_10685" s="T685">pers</ta>
            <ta e="T687" id="Seg_10686" s="T686">n</ta>
            <ta e="T688" id="Seg_10687" s="T687">n</ta>
            <ta e="T689" id="Seg_10688" s="T688">v</ta>
            <ta e="T690" id="Seg_10689" s="T689">n</ta>
            <ta e="T691" id="Seg_10690" s="T690">n</ta>
            <ta e="T692" id="Seg_10691" s="T691">v</ta>
            <ta e="T693" id="Seg_10692" s="T692">num</ta>
            <ta e="T694" id="Seg_10693" s="T693">n</ta>
            <ta e="T695" id="Seg_10694" s="T694">v</ta>
            <ta e="T699" id="Seg_10695" s="T698">num</ta>
            <ta e="T701" id="Seg_10696" s="T700">v</ta>
            <ta e="T702" id="Seg_10697" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_10698" s="T702">ptcl</ta>
            <ta e="T705" id="Seg_10699" s="T704">num</ta>
            <ta e="T706" id="Seg_10700" s="T705">v</ta>
            <ta e="T707" id="Seg_10701" s="T706">conj</ta>
            <ta e="T708" id="Seg_10702" s="T707">num</ta>
            <ta e="T709" id="Seg_10703" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_10704" s="T709">v</ta>
            <ta e="T711" id="Seg_10705" s="T710">pers</ta>
            <ta e="T712" id="Seg_10706" s="T711">n</ta>
            <ta e="T714" id="Seg_10707" s="T712">n</ta>
            <ta e="T720" id="Seg_10708" s="T719">num</ta>
            <ta e="T721" id="Seg_10709" s="T720">num</ta>
            <ta e="T722" id="Seg_10710" s="T721">v</ta>
            <ta e="T724" id="Seg_10711" s="T722">ptcl</ta>
            <ta e="T725" id="Seg_10712" s="T724">ptcl</ta>
            <ta e="T726" id="Seg_10713" s="T725">n</ta>
            <ta e="T727" id="Seg_10714" s="T726">adv</ta>
            <ta e="T728" id="Seg_10715" s="T727">n</ta>
            <ta e="T729" id="Seg_10716" s="T728">v</ta>
            <ta e="T730" id="Seg_10717" s="T729">ptcl</ta>
            <ta e="T731" id="Seg_10718" s="T730">n</ta>
            <ta e="T732" id="Seg_10719" s="T731">n</ta>
            <ta e="T733" id="Seg_10720" s="T732">v</ta>
            <ta e="T735" id="Seg_10721" s="T734">adv</ta>
            <ta e="T736" id="Seg_10722" s="T735">v</ta>
            <ta e="T737" id="Seg_10723" s="T736">n</ta>
            <ta e="T738" id="Seg_10724" s="T737">dempro</ta>
            <ta e="T740" id="Seg_10725" s="T739">dempro</ta>
            <ta e="T741" id="Seg_10726" s="T740">ptcl</ta>
            <ta e="T742" id="Seg_10727" s="T741">v</ta>
            <ta e="T743" id="Seg_10728" s="T742">adv</ta>
            <ta e="T744" id="Seg_10729" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_10730" s="T744">adv</ta>
            <ta e="T746" id="Seg_10731" s="T745">pers</ta>
            <ta e="T747" id="Seg_10732" s="T746">adv</ta>
            <ta e="T748" id="Seg_10733" s="T747">v</ta>
            <ta e="T749" id="Seg_10734" s="T748">v</ta>
            <ta e="T750" id="Seg_10735" s="T749">v</ta>
            <ta e="T751" id="Seg_10736" s="T750">que</ta>
            <ta e="T752" id="Seg_10737" s="T751">pers</ta>
            <ta e="T753" id="Seg_10738" s="T752">ptcl</ta>
            <ta e="T754" id="Seg_10739" s="T753">v</ta>
            <ta e="T755" id="Seg_10740" s="T754">adv</ta>
            <ta e="T758" id="Seg_10741" s="T757">v</ta>
            <ta e="T759" id="Seg_10742" s="T758">conj</ta>
            <ta e="T760" id="Seg_10743" s="T759">v</ta>
            <ta e="T761" id="Seg_10744" s="T760">ptcl</ta>
            <ta e="T763" id="Seg_10745" s="T762">n</ta>
            <ta e="T764" id="Seg_10746" s="T763">adv</ta>
            <ta e="T765" id="Seg_10747" s="T764">ptcl</ta>
            <ta e="T767" id="Seg_10748" s="T766">v</ta>
            <ta e="T768" id="Seg_10749" s="T767">v</ta>
            <ta e="T769" id="Seg_10750" s="T768">v</ta>
            <ta e="T770" id="Seg_10751" s="T769">adv</ta>
            <ta e="T771" id="Seg_10752" s="T770">v</ta>
            <ta e="T772" id="Seg_10753" s="T771">adj</ta>
            <ta e="T773" id="Seg_10754" s="T772">ptcl</ta>
            <ta e="T774" id="Seg_10755" s="T773">v</ta>
            <ta e="T775" id="Seg_10756" s="T774">aux</ta>
            <ta e="T776" id="Seg_10757" s="T775">v</ta>
            <ta e="T777" id="Seg_10758" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_10759" s="T777">adv</ta>
            <ta e="T780" id="Seg_10760" s="T779">aux</ta>
            <ta e="T781" id="Seg_10761" s="T780">v</ta>
            <ta e="T782" id="Seg_10762" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_10763" s="T782">adj</ta>
            <ta e="T784" id="Seg_10764" s="T783">v</ta>
            <ta e="T785" id="Seg_10765" s="T784">adv</ta>
            <ta e="T788" id="Seg_10766" s="T787">pers</ta>
            <ta e="T789" id="Seg_10767" s="T788">adv</ta>
            <ta e="T790" id="Seg_10768" s="T789">v</ta>
            <ta e="T791" id="Seg_10769" s="T790">n</ta>
            <ta e="T792" id="Seg_10770" s="T791">adv</ta>
            <ta e="T793" id="Seg_10771" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_10772" s="T793">v</ta>
            <ta e="T795" id="Seg_10773" s="T794">pers</ta>
            <ta e="T796" id="Seg_10774" s="T795">v</ta>
            <ta e="T797" id="Seg_10775" s="T796">adv</ta>
            <ta e="T798" id="Seg_10776" s="T797">n</ta>
            <ta e="T799" id="Seg_10777" s="T798">v</ta>
            <ta e="T800" id="Seg_10778" s="T799">n</ta>
            <ta e="T801" id="Seg_10779" s="T800">v</ta>
            <ta e="T802" id="Seg_10780" s="T801">n</ta>
            <ta e="T806" id="Seg_10781" s="T805">v</ta>
            <ta e="T807" id="Seg_10782" s="T806">adv</ta>
            <ta e="T808" id="Seg_10783" s="T807">n</ta>
            <ta e="T809" id="Seg_10784" s="T808">v</ta>
            <ta e="T810" id="Seg_10785" s="T809">n</ta>
            <ta e="T811" id="Seg_10786" s="T810">v</ta>
            <ta e="T812" id="Seg_10787" s="T811">v</ta>
            <ta e="T813" id="Seg_10788" s="T812">adv</ta>
            <ta e="T814" id="Seg_10789" s="T813">n</ta>
            <ta e="T815" id="Seg_10790" s="T814">n</ta>
            <ta e="T816" id="Seg_10791" s="T815">n</ta>
            <ta e="T817" id="Seg_10792" s="T816">n</ta>
            <ta e="T818" id="Seg_10793" s="T817">v</ta>
            <ta e="T819" id="Seg_10794" s="T818">v</ta>
            <ta e="T821" id="Seg_10795" s="T820">ptcl</ta>
            <ta e="T822" id="Seg_10796" s="T821">v</ta>
            <ta e="T823" id="Seg_10797" s="T822">ptcl</ta>
            <ta e="T825" id="Seg_10798" s="T824">v</ta>
            <ta e="T826" id="Seg_10799" s="T825">n</ta>
            <ta e="T827" id="Seg_10800" s="T826">ptcl</ta>
            <ta e="T828" id="Seg_10801" s="T827">v</ta>
            <ta e="T829" id="Seg_10802" s="T828">v</ta>
            <ta e="T830" id="Seg_10803" s="T829">v</ta>
            <ta e="T831" id="Seg_10804" s="T830">v</ta>
            <ta e="T832" id="Seg_10805" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_10806" s="T832">adv</ta>
            <ta e="T835" id="Seg_10807" s="T834">n</ta>
            <ta e="T836" id="Seg_10808" s="T835">v</ta>
            <ta e="T837" id="Seg_10809" s="T836">adv</ta>
            <ta e="T838" id="Seg_10810" s="T837">n</ta>
            <ta e="T839" id="Seg_10811" s="T838">quant</ta>
            <ta e="T840" id="Seg_10812" s="T839">v</ta>
            <ta e="T841" id="Seg_10813" s="T840">n</ta>
            <ta e="T842" id="Seg_10814" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_10815" s="T842">v</ta>
            <ta e="T844" id="Seg_10816" s="T843">n</ta>
            <ta e="T845" id="Seg_10817" s="T844">adv</ta>
            <ta e="T846" id="Seg_10818" s="T845">quant</ta>
            <ta e="T847" id="Seg_10819" s="T846">n</ta>
            <ta e="T848" id="Seg_10820" s="T847">n</ta>
            <ta e="T849" id="Seg_10821" s="T848">n</ta>
            <ta e="T850" id="Seg_10822" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_10823" s="T850">ptcl</ta>
            <ta e="T853" id="Seg_10824" s="T852">que</ta>
            <ta e="T854" id="Seg_10825" s="T853">dempro</ta>
            <ta e="T855" id="Seg_10826" s="T854">adj</ta>
            <ta e="T856" id="Seg_10827" s="T855">n</ta>
            <ta e="T857" id="Seg_10828" s="T856">ptcl</ta>
            <ta e="T858" id="Seg_10829" s="T857">v</ta>
            <ta e="T859" id="Seg_10830" s="T858">n</ta>
            <ta e="T860" id="Seg_10831" s="T859">ptcl</ta>
            <ta e="T861" id="Seg_10832" s="T860">v</ta>
            <ta e="T862" id="Seg_10833" s="T861">que</ta>
            <ta e="T863" id="Seg_10834" s="T862">dempro</ta>
            <ta e="T864" id="Seg_10835" s="T863">que</ta>
            <ta e="T865" id="Seg_10836" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_10837" s="T865">v</ta>
            <ta e="T867" id="Seg_10838" s="T866">v</ta>
            <ta e="T868" id="Seg_10839" s="T867">pers</ta>
            <ta e="T869" id="Seg_10840" s="T868">ptcl</ta>
            <ta e="T871" id="Seg_10841" s="T870">que</ta>
            <ta e="T872" id="Seg_10842" s="T871">dempro</ta>
            <ta e="T873" id="Seg_10843" s="T872">que</ta>
            <ta e="T874" id="Seg_10844" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_10845" s="T874">v</ta>
            <ta e="T876" id="Seg_10846" s="T875">v</ta>
            <ta e="T878" id="Seg_10847" s="T877">adv</ta>
            <ta e="T879" id="Seg_10848" s="T878">ptcl</ta>
            <ta e="T880" id="Seg_10849" s="T879">v</ta>
            <ta e="T881" id="Seg_10850" s="T880">v</ta>
            <ta e="T882" id="Seg_10851" s="T881">n</ta>
            <ta e="T883" id="Seg_10852" s="T882">pers</ta>
            <ta e="T884" id="Seg_10853" s="T883">pers</ta>
            <ta e="T886" id="Seg_10854" s="T885">ptcl</ta>
            <ta e="T887" id="Seg_10855" s="T886">ptcl</ta>
            <ta e="T891" id="Seg_10856" s="T890">pers</ta>
            <ta e="T892" id="Seg_10857" s="T891">pers</ta>
            <ta e="T893" id="Seg_10858" s="T892">ptcl</ta>
            <ta e="T894" id="Seg_10859" s="T893">ptcl</ta>
            <ta e="T896" id="Seg_10860" s="T895">v</ta>
            <ta e="T897" id="Seg_10861" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_10862" s="T897">ptcl</ta>
            <ta e="T899" id="Seg_10863" s="T898">adj</ta>
            <ta e="T900" id="Seg_10864" s="T899">v</ta>
            <ta e="T901" id="Seg_10865" s="T900">adv</ta>
            <ta e="T902" id="Seg_10866" s="T901">v</ta>
            <ta e="T903" id="Seg_10867" s="T902">n</ta>
            <ta e="T904" id="Seg_10868" s="T903">dempro</ta>
            <ta e="T905" id="Seg_10869" s="T904">n</ta>
            <ta e="T906" id="Seg_10870" s="T905">n</ta>
            <ta e="T908" id="Seg_10871" s="T907">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T16" id="Seg_10872" s="T13">0.2.h:A</ta>
            <ta e="T20" id="Seg_10873" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_10874" s="T20">np:G</ta>
            <ta e="T22" id="Seg_10875" s="T21">0.1.h:A</ta>
            <ta e="T23" id="Seg_10876" s="T22">np:P</ta>
            <ta e="T25" id="Seg_10877" s="T24">adv:Time</ta>
            <ta e="T26" id="Seg_10878" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_10879" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_10880" s="T28">0.3:P</ta>
            <ta e="T31" id="Seg_10881" s="T30">0.3:P</ta>
            <ta e="T34" id="Seg_10882" s="T33">0.3:P</ta>
            <ta e="T35" id="Seg_10883" s="T34">adv:Time</ta>
            <ta e="T40" id="Seg_10884" s="T39">adv:Time</ta>
            <ta e="T43" id="Seg_10885" s="T42">adv:Time</ta>
            <ta e="T44" id="Seg_10886" s="T43">np:G</ta>
            <ta e="T46" id="Seg_10887" s="T45">adv:Time</ta>
            <ta e="T49" id="Seg_10888" s="T48">adv:Time</ta>
            <ta e="T52" id="Seg_10889" s="T51">adv:Time</ta>
            <ta e="T55" id="Seg_10890" s="T54">adv:Time</ta>
            <ta e="T58" id="Seg_10891" s="T57">adv:Time</ta>
            <ta e="T64" id="Seg_10892" s="T63">pro.h:A</ta>
            <ta e="T67" id="Seg_10893" s="T66">pro.h:R</ta>
            <ta e="T69" id="Seg_10894" s="T68">0.2.h:A</ta>
            <ta e="T70" id="Seg_10895" s="T69">pro.h:A</ta>
            <ta e="T72" id="Seg_10896" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_10897" s="T74">adv:L</ta>
            <ta e="T76" id="Seg_10898" s="T75">pro.h:Poss</ta>
            <ta e="T77" id="Seg_10899" s="T76">np.h:Th</ta>
            <ta e="T79" id="Seg_10900" s="T78">pro.h:E</ta>
            <ta e="T82" id="Seg_10901" s="T81">pro.h:Th</ta>
            <ta e="T85" id="Seg_10902" s="T84">pro.h:A</ta>
            <ta e="T87" id="Seg_10903" s="T86">pro.h:R</ta>
            <ta e="T92" id="Seg_10904" s="T91">0.2.h:A</ta>
            <ta e="T93" id="Seg_10905" s="T92">np:G</ta>
            <ta e="T94" id="Seg_10906" s="T93">adv:L</ta>
            <ta e="T95" id="Seg_10907" s="T94">pro.h:Poss</ta>
            <ta e="T96" id="Seg_10908" s="T95">np.h:Th</ta>
            <ta e="T98" id="Seg_10909" s="T97">pro.h:A</ta>
            <ta e="T100" id="Seg_10910" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_10911" s="T100">pro.h:R</ta>
            <ta e="T103" id="Seg_10912" s="T102">pro.h:Poss</ta>
            <ta e="T104" id="Seg_10913" s="T103">np:E</ta>
            <ta e="T108" id="Seg_10914" s="T107">pro.h:A</ta>
            <ta e="T111" id="Seg_10915" s="T110">pro.h:E</ta>
            <ta e="T114" id="Seg_10916" s="T113">pro.h:Th</ta>
            <ta e="T117" id="Seg_10917" s="T116">0.2.h:A</ta>
            <ta e="T118" id="Seg_10918" s="T117">np:P</ta>
            <ta e="T124" id="Seg_10919" s="T123">pro.h:A</ta>
            <ta e="T126" id="Seg_10920" s="T125">adv:L</ta>
            <ta e="T127" id="Seg_10921" s="T126">np:Th</ta>
            <ta e="T128" id="Seg_10922" s="T127">0.1.h:A</ta>
            <ta e="T129" id="Seg_10923" s="T128">pro.h:A</ta>
            <ta e="T130" id="Seg_10924" s="T129">adv:Time</ta>
            <ta e="T131" id="Seg_10925" s="T130">np:Th</ta>
            <ta e="T134" id="Seg_10926" s="T133">np:Th</ta>
            <ta e="T135" id="Seg_10927" s="T134">0.1.h:A</ta>
            <ta e="T138" id="Seg_10928" s="T137">np:Th</ta>
            <ta e="T139" id="Seg_10929" s="T138">0.1.h:A</ta>
            <ta e="T140" id="Seg_10930" s="T139">adv:Time</ta>
            <ta e="T141" id="Seg_10931" s="T140">0.1.h:A</ta>
            <ta e="T142" id="Seg_10932" s="T141">0.2.h:Th</ta>
            <ta e="T143" id="Seg_10933" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_10934" s="T144">np.h:Th</ta>
            <ta e="T147" id="Seg_10935" s="T146">pro:G</ta>
            <ta e="T148" id="Seg_10936" s="T911">0.3.h:A</ta>
            <ta e="T153" id="Seg_10937" s="T152">pro.h:A</ta>
            <ta e="T156" id="Seg_10938" s="T155">np.h:A</ta>
            <ta e="T158" id="Seg_10939" s="T157">0.1.h:E</ta>
            <ta e="T175" id="Seg_10940" s="T174">pro.h:Poss</ta>
            <ta e="T177" id="Seg_10941" s="T176">np:Th</ta>
            <ta e="T179" id="Seg_10942" s="T178">adv:Time</ta>
            <ta e="T183" id="Seg_10943" s="T182">np:P</ta>
            <ta e="T186" id="Seg_10944" s="T185">pro.h:Poss</ta>
            <ta e="T187" id="Seg_10945" s="T186">np:Th</ta>
            <ta e="T191" id="Seg_10946" s="T190">pro.h:Poss</ta>
            <ta e="T192" id="Seg_10947" s="T191">np.h:E</ta>
            <ta e="T195" id="Seg_10948" s="T194">np:P</ta>
            <ta e="T196" id="Seg_10949" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_10950" s="T196">0.3.h:A</ta>
            <ta e="T199" id="Seg_10951" s="T198">0.3.h:A</ta>
            <ta e="T200" id="Seg_10952" s="T199">pro.h:Poss</ta>
            <ta e="T201" id="Seg_10953" s="T200">np.h:Com</ta>
            <ta e="T202" id="Seg_10954" s="T201">0.3.h:A</ta>
            <ta e="T205" id="Seg_10955" s="T204">pro.h:E</ta>
            <ta e="T209" id="Seg_10956" s="T208">0.3.h:A</ta>
            <ta e="T211" id="Seg_10957" s="T210">pro.h:B</ta>
            <ta e="T214" id="Seg_10958" s="T912">0.3.h:A</ta>
            <ta e="T215" id="Seg_10959" s="T214">np:P</ta>
            <ta e="T222" id="Seg_10960" s="T221">pro.h:Th</ta>
            <ta e="T223" id="Seg_10961" s="T222">np:L</ta>
            <ta e="T225" id="Seg_10962" s="T224">pro:L</ta>
            <ta e="T226" id="Seg_10963" s="T225">0.2.h:E</ta>
            <ta e="T227" id="Seg_10964" s="T226">0.1.h:E</ta>
            <ta e="T228" id="Seg_10965" s="T227">np:G</ta>
            <ta e="T229" id="Seg_10966" s="T228">np:A</ta>
            <ta e="T232" id="Seg_10967" s="T231">0.3.h:A</ta>
            <ta e="T233" id="Seg_10968" s="T232">adv:L</ta>
            <ta e="T235" id="Seg_10969" s="T234">pro.h:E</ta>
            <ta e="T238" id="Seg_10970" s="T237">pro.h:E</ta>
            <ta e="T241" id="Seg_10971" s="T240">np:Th</ta>
            <ta e="T244" id="Seg_10972" s="T243">np.h:R</ta>
            <ta e="T251" id="Seg_10973" s="T910">np.h:Th</ta>
            <ta e="T254" id="Seg_10974" s="T252">0.3.h:A</ta>
            <ta e="T256" id="Seg_10975" s="T255">0.3.h:A</ta>
            <ta e="T258" id="Seg_10976" s="T257">0.3.h:A</ta>
            <ta e="T265" id="Seg_10977" s="T264">pro:Th</ta>
            <ta e="T266" id="Seg_10978" s="T265">0.2.h:E</ta>
            <ta e="T267" id="Seg_10979" s="T266">0.2.h:A</ta>
            <ta e="T269" id="Seg_10980" s="T268">pro.h:A</ta>
            <ta e="T272" id="Seg_10981" s="T271">pro:Th</ta>
            <ta e="T275" id="Seg_10982" s="T274">0.1.h:E</ta>
            <ta e="T277" id="Seg_10983" s="T276">pro.h:E</ta>
            <ta e="T280" id="Seg_10984" s="T279">0.1.h:E</ta>
            <ta e="T283" id="Seg_10985" s="T282">pro.h:E</ta>
            <ta e="T288" id="Seg_10986" s="T287">pro.h:A</ta>
            <ta e="T291" id="Seg_10987" s="T290">np.h:R</ta>
            <ta e="T292" id="Seg_10988" s="T291">0.1.h:A</ta>
            <ta e="T295" id="Seg_10989" s="T294">pro.h:A</ta>
            <ta e="T299" id="Seg_10990" s="T298">adv:L</ta>
            <ta e="T301" id="Seg_10991" s="T300">np.h:Th</ta>
            <ta e="T306" id="Seg_10992" s="T305">np.h:E</ta>
            <ta e="T308" id="Seg_10993" s="T307">pro.h:E</ta>
            <ta e="T310" id="Seg_10994" s="T309">np.h:Th</ta>
            <ta e="T315" id="Seg_10995" s="T314">0.1.h:A</ta>
            <ta e="T317" id="Seg_10996" s="T316">np:Th</ta>
            <ta e="T318" id="Seg_10997" s="T317">0.1.h:A</ta>
            <ta e="T319" id="Seg_10998" s="T318">pro.h:A</ta>
            <ta e="T321" id="Seg_10999" s="T320">pro.h:A</ta>
            <ta e="T322" id="Seg_11000" s="T321">np:Th</ta>
            <ta e="T324" id="Seg_11001" s="T323">0.2.h:Th</ta>
            <ta e="T325" id="Seg_11002" s="T324">pro.h:A</ta>
            <ta e="T328" id="Seg_11003" s="T327">np:Th</ta>
            <ta e="T330" id="Seg_11004" s="T329">0.1.h:A</ta>
            <ta e="T331" id="Seg_11005" s="T330">adv:Time</ta>
            <ta e="T336" id="Seg_11006" s="T335">0.1.h:A</ta>
            <ta e="T338" id="Seg_11007" s="T337">n:Time</ta>
            <ta e="T341" id="Seg_11008" s="T340">np:Th</ta>
            <ta e="T343" id="Seg_11009" s="T342">np:P</ta>
            <ta e="T346" id="Seg_11010" s="T345">np:P</ta>
            <ta e="T349" id="Seg_11011" s="T348">adv:Time</ta>
            <ta e="T351" id="Seg_11012" s="T350">np:Th</ta>
            <ta e="T354" id="Seg_11013" s="T353">0.1.h:P</ta>
            <ta e="T358" id="Seg_11014" s="T357">np:Th</ta>
            <ta e="T363" id="Seg_11015" s="T362">0.1.h:A</ta>
            <ta e="T365" id="Seg_11016" s="T364">pro.h:Th</ta>
            <ta e="T368" id="Seg_11017" s="T367">0.2.h:A</ta>
            <ta e="T369" id="Seg_11018" s="T368">np:G</ta>
            <ta e="T370" id="Seg_11019" s="T369">0.2.h:A</ta>
            <ta e="T371" id="Seg_11020" s="T370">np:Th</ta>
            <ta e="T372" id="Seg_11021" s="T371">0.2.h:A</ta>
            <ta e="T373" id="Seg_11022" s="T372">adv:Time</ta>
            <ta e="T374" id="Seg_11023" s="T373">np:Th</ta>
            <ta e="T375" id="Seg_11024" s="T374">0.2.h:A</ta>
            <ta e="T376" id="Seg_11025" s="T375">0.2.h:A</ta>
            <ta e="T381" id="Seg_11026" s="T380">adv:Time</ta>
            <ta e="T382" id="Seg_11027" s="T381">0.2.h:P</ta>
            <ta e="T383" id="Seg_11028" s="T382">np:Th</ta>
            <ta e="T384" id="Seg_11029" s="T383">0.2.h:A</ta>
            <ta e="T387" id="Seg_11030" s="T386">np.h:A</ta>
            <ta e="T388" id="Seg_11031" s="T387">pro.h:Th</ta>
            <ta e="T396" id="Seg_11032" s="T395">pro.h:A</ta>
            <ta e="T397" id="Seg_11033" s="T396">np:G</ta>
            <ta e="T399" id="Seg_11034" s="T398">pro.h:R</ta>
            <ta e="T401" id="Seg_11035" s="T400">0.3.h:A</ta>
            <ta e="T403" id="Seg_11036" s="T402">pro.h:Poss</ta>
            <ta e="T404" id="Seg_11037" s="T403">adv:L</ta>
            <ta e="T405" id="Seg_11038" s="T404">np.h:Th</ta>
            <ta e="T407" id="Seg_11039" s="T406">pro.h:A</ta>
            <ta e="T409" id="Seg_11040" s="T408">np:Th</ta>
            <ta e="T412" id="Seg_11041" s="T411">pro.h:Poss</ta>
            <ta e="T413" id="Seg_11042" s="T412">np:E</ta>
            <ta e="T415" id="Seg_11043" s="T414">pro.h:E</ta>
            <ta e="T419" id="Seg_11044" s="T418">pro.h:Th</ta>
            <ta e="T424" id="Seg_11045" s="T423">0.1.h:A</ta>
            <ta e="T427" id="Seg_11046" s="T426">np.h:A</ta>
            <ta e="T430" id="Seg_11047" s="T429">np.h:A</ta>
            <ta e="T434" id="Seg_11048" s="T433">0.1.h:A</ta>
            <ta e="T436" id="Seg_11049" s="T435">np:L</ta>
            <ta e="T439" id="Seg_11050" s="T438">0.1.h:E</ta>
            <ta e="T441" id="Seg_11051" s="T440">0.1.h:A</ta>
            <ta e="T443" id="Seg_11052" s="T442">np.h:A 0.2.h:Poss</ta>
            <ta e="T446" id="Seg_11053" s="T445">pro.h:E</ta>
            <ta e="T450" id="Seg_11054" s="T449">0.1.h:A</ta>
            <ta e="T453" id="Seg_11055" s="T452">pro.h:Poss</ta>
            <ta e="T454" id="Seg_11056" s="T453">np.h:P</ta>
            <ta e="T457" id="Seg_11057" s="T456">pro.h:Th</ta>
            <ta e="T461" id="Seg_11058" s="T460">0.3.h:A</ta>
            <ta e="T465" id="Seg_11059" s="T464">0.1.h:A</ta>
            <ta e="T466" id="Seg_11060" s="T465">pro.h:P</ta>
            <ta e="T468" id="Seg_11061" s="T467">adv:Time</ta>
            <ta e="T471" id="Seg_11062" s="T470">pro:Th</ta>
            <ta e="T472" id="Seg_11063" s="T471">adv:Time</ta>
            <ta e="T477" id="Seg_11064" s="T476">adv:Time</ta>
            <ta e="T480" id="Seg_11065" s="T479">0.2.h:A</ta>
            <ta e="T481" id="Seg_11066" s="T480">np.h:Th</ta>
            <ta e="T486" id="Seg_11067" s="T485">adv:L</ta>
            <ta e="T488" id="Seg_11068" s="T487">np:Th</ta>
            <ta e="T490" id="Seg_11069" s="T489">np.h:A</ta>
            <ta e="T493" id="Seg_11070" s="T492">np:P</ta>
            <ta e="T494" id="Seg_11071" s="T493">0.3.h:A</ta>
            <ta e="T495" id="Seg_11072" s="T494">0.3.h:A</ta>
            <ta e="T497" id="Seg_11073" s="T496">pro.h:Th</ta>
            <ta e="T506" id="Seg_11074" s="T505">adv:L</ta>
            <ta e="T508" id="Seg_11075" s="T507">np:Th</ta>
            <ta e="T509" id="Seg_11076" s="T508">pro.h:Poss</ta>
            <ta e="T510" id="Seg_11077" s="T509">np.h:A</ta>
            <ta e="T512" id="Seg_11078" s="T511">0.1.h:A</ta>
            <ta e="T513" id="Seg_11079" s="T512">adv:L</ta>
            <ta e="T514" id="Seg_11080" s="T513">np:P</ta>
            <ta e="T515" id="Seg_11081" s="T514">0.1.h:A</ta>
            <ta e="T516" id="Seg_11082" s="T515">adv:L</ta>
            <ta e="T517" id="Seg_11083" s="T516">0.3.h:A</ta>
            <ta e="T518" id="Seg_11084" s="T517">np:Ins</ta>
            <ta e="T520" id="Seg_11085" s="T519">pro.h:A</ta>
            <ta e="T552" id="Seg_11086" s="T551">pro.h:A</ta>
            <ta e="T556" id="Seg_11087" s="T555">pro.h:A</ta>
            <ta e="T561" id="Seg_11088" s="T560">pro.h:A</ta>
            <ta e="T562" id="Seg_11089" s="T561">np:P</ta>
            <ta e="T570" id="Seg_11090" s="T569">np:Th</ta>
            <ta e="T571" id="Seg_11091" s="T570">0.1.h:A</ta>
            <ta e="T573" id="Seg_11092" s="T572">np:Th</ta>
            <ta e="T574" id="Seg_11093" s="T573">0.1.h:A</ta>
            <ta e="T575" id="Seg_11094" s="T574">np:P</ta>
            <ta e="T576" id="Seg_11095" s="T575">0.1.h:A</ta>
            <ta e="T577" id="Seg_11096" s="T576">0.2.h:Th</ta>
            <ta e="T578" id="Seg_11097" s="T577">0.2.h:A</ta>
            <ta e="T585" id="Seg_11098" s="T584">pro.h:E</ta>
            <ta e="T586" id="Seg_11099" s="T585">pro.h:Th</ta>
            <ta e="T588" id="Seg_11100" s="T587">pro.h:A</ta>
            <ta e="T591" id="Seg_11101" s="T590">pro.h:A</ta>
            <ta e="T594" id="Seg_11102" s="T593">pro.h:E</ta>
            <ta e="T595" id="Seg_11103" s="T594">pro.h:Th</ta>
            <ta e="T598" id="Seg_11104" s="T597">pro.h:A</ta>
            <ta e="T599" id="Seg_11105" s="T598">np:G</ta>
            <ta e="T601" id="Seg_11106" s="T600">pro.h:A</ta>
            <ta e="T604" id="Seg_11107" s="T603">adv:L</ta>
            <ta e="T609" id="Seg_11108" s="T608">np.h:A</ta>
            <ta e="T612" id="Seg_11109" s="T611">pro:Th</ta>
            <ta e="T614" id="Seg_11110" s="T613">0.3.h:A</ta>
            <ta e="T619" id="Seg_11111" s="T618">pro:Th</ta>
            <ta e="T620" id="Seg_11112" s="T619">0.3.h:A</ta>
            <ta e="T621" id="Seg_11113" s="T620">pro.h:E</ta>
            <ta e="T622" id="Seg_11114" s="T621">adv:Time</ta>
            <ta e="T629" id="Seg_11115" s="T628">0.1.h:A</ta>
            <ta e="T636" id="Seg_11116" s="T635">np.h:Th</ta>
            <ta e="T639" id="Seg_11117" s="T638">np:L</ta>
            <ta e="T640" id="Seg_11118" s="T639">0.3.h:Th</ta>
            <ta e="T641" id="Seg_11119" s="T640">adv:Time</ta>
            <ta e="T642" id="Seg_11120" s="T641">0.3.h:E</ta>
            <ta e="T643" id="Seg_11121" s="T642">np:G</ta>
            <ta e="T644" id="Seg_11122" s="T643">adv:Time</ta>
            <ta e="T645" id="Seg_11123" s="T644">np:A</ta>
            <ta e="T650" id="Seg_11124" s="T649">np:Th</ta>
            <ta e="T651" id="Seg_11125" s="T650">0.3.h:E</ta>
            <ta e="T652" id="Seg_11126" s="T651">0.3.h:Th</ta>
            <ta e="T656" id="Seg_11127" s="T655">0.3.h:Th</ta>
            <ta e="T657" id="Seg_11128" s="T656">0.3.h:E</ta>
            <ta e="T661" id="Seg_11129" s="T660">np.h:A</ta>
            <ta e="T662" id="Seg_11130" s="T661">np.h:A</ta>
            <ta e="T667" id="Seg_11131" s="T666">0.3.h:A</ta>
            <ta e="T668" id="Seg_11132" s="T667">np:Th</ta>
            <ta e="T669" id="Seg_11133" s="T668">0.3.h:A</ta>
            <ta e="T670" id="Seg_11134" s="T669">0.3.h:A</ta>
            <ta e="T673" id="Seg_11135" s="T672">pro.h:A</ta>
            <ta e="T674" id="Seg_11136" s="T673">np:G</ta>
            <ta e="T676" id="Seg_11137" s="T675">np:Th</ta>
            <ta e="T677" id="Seg_11138" s="T676">0.1.h:A</ta>
            <ta e="T678" id="Seg_11139" s="T677">np:Th</ta>
            <ta e="T679" id="Seg_11140" s="T678">0.1.h:A</ta>
            <ta e="T680" id="Seg_11141" s="T679">np:P</ta>
            <ta e="T681" id="Seg_11142" s="T680">0.1.h:A</ta>
            <ta e="T682" id="Seg_11143" s="T681">pro.h:R</ta>
            <ta e="T683" id="Seg_11144" s="T682">0.1.h:A</ta>
            <ta e="T686" id="Seg_11145" s="T685">pro.h:Poss</ta>
            <ta e="T687" id="Seg_11146" s="T686">np.h:Th</ta>
            <ta e="T688" id="Seg_11147" s="T687">np.h:Th</ta>
            <ta e="T690" id="Seg_11148" s="T689">np.h:Th</ta>
            <ta e="T691" id="Seg_11149" s="T690">np.h:Th</ta>
            <ta e="T694" id="Seg_11150" s="T693">np.h:Th</ta>
            <ta e="T700" id="Seg_11151" s="T699">np.h:Th</ta>
            <ta e="T705" id="Seg_11152" s="T704">np.h:P</ta>
            <ta e="T708" id="Seg_11153" s="T707">np.h:P</ta>
            <ta e="T711" id="Seg_11154" s="T710">pro.h:Poss</ta>
            <ta e="T721" id="Seg_11155" s="T720">np.h:P</ta>
            <ta e="T722" id="Seg_11156" s="T721">0.3.h:A</ta>
            <ta e="T726" id="Seg_11157" s="T725">np:Th</ta>
            <ta e="T728" id="Seg_11158" s="T727">np:Th</ta>
            <ta e="T729" id="Seg_11159" s="T728">0.3.h:A</ta>
            <ta e="T732" id="Seg_11160" s="T731">np.h:E</ta>
            <ta e="T735" id="Seg_11161" s="T734">adv:Time</ta>
            <ta e="T736" id="Seg_11162" s="T735">0.3.h:A</ta>
            <ta e="T737" id="Seg_11163" s="T736">np:G</ta>
            <ta e="T738" id="Seg_11164" s="T737">pro.h:A</ta>
            <ta e="T740" id="Seg_11165" s="T739">pro.h:P</ta>
            <ta e="T746" id="Seg_11166" s="T745">pro.h:Th</ta>
            <ta e="T749" id="Seg_11167" s="T748">0.2.h:Th</ta>
            <ta e="T750" id="Seg_11168" s="T749">0.2.h:A</ta>
            <ta e="T751" id="Seg_11169" s="T750">pro.h:A</ta>
            <ta e="T752" id="Seg_11170" s="T751">pro.h:R</ta>
            <ta e="T755" id="Seg_11171" s="T754">adv:Time</ta>
            <ta e="T758" id="Seg_11172" s="T757">0.2.h:A</ta>
            <ta e="T760" id="Seg_11173" s="T759">0.2.h:E</ta>
            <ta e="T763" id="Seg_11174" s="T762">np.h:A</ta>
            <ta e="T764" id="Seg_11175" s="T763">adv:L</ta>
            <ta e="T768" id="Seg_11176" s="T767">0.3.h:A</ta>
            <ta e="T769" id="Seg_11177" s="T768">0.3.h:A</ta>
            <ta e="T771" id="Seg_11178" s="T770">0.2.h:A</ta>
            <ta e="T775" id="Seg_11179" s="T774">0.2.h:A</ta>
            <ta e="T780" id="Seg_11180" s="T779">0.2.h:A</ta>
            <ta e="T786" id="Seg_11181" s="T785">0.2.h:A</ta>
            <ta e="T788" id="Seg_11182" s="T787">pro.h:A</ta>
            <ta e="T789" id="Seg_11183" s="T788">adv:Time</ta>
            <ta e="T791" id="Seg_11184" s="T790">np:Th</ta>
            <ta e="T795" id="Seg_11185" s="T794">pro.h:A</ta>
            <ta e="T797" id="Seg_11186" s="T796">adv:Time</ta>
            <ta e="T798" id="Seg_11187" s="T797">np:Th</ta>
            <ta e="T799" id="Seg_11188" s="T798">0.1.h:A</ta>
            <ta e="T800" id="Seg_11189" s="T799">np:R</ta>
            <ta e="T801" id="Seg_11190" s="T800">0.1.h:A</ta>
            <ta e="T802" id="Seg_11191" s="T801">np:R</ta>
            <ta e="T806" id="Seg_11192" s="T805">0.1.h:A</ta>
            <ta e="T807" id="Seg_11193" s="T806">adv:Time</ta>
            <ta e="T808" id="Seg_11194" s="T807">np:P</ta>
            <ta e="T809" id="Seg_11195" s="T808">0.1.h:A</ta>
            <ta e="T810" id="Seg_11196" s="T809">np:P</ta>
            <ta e="T811" id="Seg_11197" s="T810">0.1.h:A</ta>
            <ta e="T812" id="Seg_11198" s="T811">0.1.h:A</ta>
            <ta e="T813" id="Seg_11199" s="T812">adv:Time</ta>
            <ta e="T817" id="Seg_11200" s="T816">np.h:A</ta>
            <ta e="T819" id="Seg_11201" s="T818">0.1.h:A</ta>
            <ta e="T822" id="Seg_11202" s="T821">0.1.h:E</ta>
            <ta e="T825" id="Seg_11203" s="T824">0.2.h:A</ta>
            <ta e="T826" id="Seg_11204" s="T825">np:G</ta>
            <ta e="T829" id="Seg_11205" s="T828">0.2.h:A</ta>
            <ta e="T830" id="Seg_11206" s="T829">0.2.h:A</ta>
            <ta e="T835" id="Seg_11207" s="T834">np:G</ta>
            <ta e="T836" id="Seg_11208" s="T835">0.1.h:A</ta>
            <ta e="T838" id="Seg_11209" s="T837">np.h:Th</ta>
            <ta e="T840" id="Seg_11210" s="T839">0.1.h:E</ta>
            <ta e="T841" id="Seg_11211" s="T840">np.h:A</ta>
            <ta e="T844" id="Seg_11212" s="T843">np.h:Th</ta>
            <ta e="T845" id="Seg_11213" s="T844">adv:L</ta>
            <ta e="T847" id="Seg_11214" s="T846">np:Th</ta>
            <ta e="T856" id="Seg_11215" s="T855">np.h:A</ta>
            <ta e="T859" id="Seg_11216" s="T858">np.h:A</ta>
            <ta e="T862" id="Seg_11217" s="T861">pro:L</ta>
            <ta e="T863" id="Seg_11218" s="T862">pro.h:Th</ta>
            <ta e="T866" id="Seg_11219" s="T865">0.3.h:A</ta>
            <ta e="T868" id="Seg_11220" s="T867">pro.h:Com</ta>
            <ta e="T871" id="Seg_11221" s="T870">pro:L</ta>
            <ta e="T872" id="Seg_11222" s="T871">pro.h:Th</ta>
            <ta e="T875" id="Seg_11223" s="T874">0.3.h:A</ta>
            <ta e="T878" id="Seg_11224" s="T877">adv:Time</ta>
            <ta e="T881" id="Seg_11225" s="T880">0.2.h:A</ta>
            <ta e="T882" id="Seg_11226" s="T881">np:G</ta>
            <ta e="T891" id="Seg_11227" s="T890">pro.h:A</ta>
            <ta e="T892" id="Seg_11228" s="T891">pro.h:R</ta>
            <ta e="T903" id="Seg_11229" s="T902">np.h:Th</ta>
            <ta e="T904" id="Seg_11230" s="T903">pro.h:Th</ta>
            <ta e="T906" id="Seg_11231" s="T905">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T16" id="Seg_11232" s="T13">v:pred 0.2.h:S</ta>
            <ta e="T19" id="Seg_11233" s="T18">ptcl:pred</ta>
            <ta e="T22" id="Seg_11234" s="T21">v:pred 0.1.h:S</ta>
            <ta e="T23" id="Seg_11235" s="T22">np:O</ta>
            <ta e="T26" id="Seg_11236" s="T25">np:O</ta>
            <ta e="T29" id="Seg_11237" s="T28">v:pred 0.3:S</ta>
            <ta e="T31" id="Seg_11238" s="T30">v:pred 0.3:S</ta>
            <ta e="T33" id="Seg_11239" s="T32">adj:pred</ta>
            <ta e="T34" id="Seg_11240" s="T33">cop 0.3:S</ta>
            <ta e="T37" id="Seg_11241" s="T36">ptcl:pred</ta>
            <ta e="T42" id="Seg_11242" s="T41">ptcl:pred</ta>
            <ta e="T45" id="Seg_11243" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_11244" s="T46">ptcl:pred</ta>
            <ta e="T50" id="Seg_11245" s="T49">ptcl:pred</ta>
            <ta e="T54" id="Seg_11246" s="T53">ptcl:pred</ta>
            <ta e="T57" id="Seg_11247" s="T56">ptcl:pred</ta>
            <ta e="T60" id="Seg_11248" s="T59">ptcl:pred</ta>
            <ta e="T64" id="Seg_11249" s="T63">pro.h:S</ta>
            <ta e="T65" id="Seg_11250" s="T64">v:pred</ta>
            <ta e="T68" id="Seg_11251" s="T67">ptcl.neg</ta>
            <ta e="T69" id="Seg_11252" s="T68">v:pred 0.2.h:S</ta>
            <ta e="T70" id="Seg_11253" s="T69">pro.h:S</ta>
            <ta e="T72" id="Seg_11254" s="T71">np:O</ta>
            <ta e="T74" id="Seg_11255" s="T73">v:pred</ta>
            <ta e="T77" id="Seg_11256" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_11257" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_11258" s="T78">pro.h:S</ta>
            <ta e="T81" id="Seg_11259" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_11260" s="T81">pro.h:O</ta>
            <ta e="T85" id="Seg_11261" s="T84">pro.h:S</ta>
            <ta e="T88" id="Seg_11262" s="T87">ptcl.neg</ta>
            <ta e="T90" id="Seg_11263" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_11264" s="T91">v:pred 0.2.h:S</ta>
            <ta e="T96" id="Seg_11265" s="T95">np.h:S</ta>
            <ta e="T97" id="Seg_11266" s="T96">v:pred</ta>
            <ta e="T98" id="Seg_11267" s="T97">pro.h:S</ta>
            <ta e="T100" id="Seg_11268" s="T99">np:O</ta>
            <ta e="T102" id="Seg_11269" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_11270" s="T103">np:S</ta>
            <ta e="T105" id="Seg_11271" s="T104">v:pred</ta>
            <ta e="T108" id="Seg_11272" s="T107">pro.h:S</ta>
            <ta e="T110" id="Seg_11273" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_11274" s="T110">pro.h:S</ta>
            <ta e="T113" id="Seg_11275" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_11276" s="T113">pro.h:O</ta>
            <ta e="T117" id="Seg_11277" s="T116">v:pred 0.2.h:S</ta>
            <ta e="T118" id="Seg_11278" s="T117">np:O</ta>
            <ta e="T119" id="Seg_11279" s="T118">v:pred</ta>
            <ta e="T124" id="Seg_11280" s="T123">pro.h:S</ta>
            <ta e="T125" id="Seg_11281" s="T124">v:pred</ta>
            <ta e="T127" id="Seg_11282" s="T126">np:O</ta>
            <ta e="T128" id="Seg_11283" s="T127">v:pred 0.1.h:S</ta>
            <ta e="T129" id="Seg_11284" s="T128">pro.h:S</ta>
            <ta e="T131" id="Seg_11285" s="T130">np:O</ta>
            <ta e="T132" id="Seg_11286" s="T131">v:pred</ta>
            <ta e="T134" id="Seg_11287" s="T133">np:O</ta>
            <ta e="T135" id="Seg_11288" s="T134">v:pred 0.1.h:S</ta>
            <ta e="T138" id="Seg_11289" s="T137">np:O</ta>
            <ta e="T139" id="Seg_11290" s="T138">v:pred 0.1.h:S</ta>
            <ta e="T141" id="Seg_11291" s="T140">v:pred 0.1.h:S</ta>
            <ta e="T142" id="Seg_11292" s="T141">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_11293" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T145" id="Seg_11294" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_11295" s="T145">v:pred</ta>
            <ta e="T911" id="Seg_11296" s="T147">conv:pred</ta>
            <ta e="T148" id="Seg_11297" s="T911">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_11298" s="T150">ptcl:pred</ta>
            <ta e="T153" id="Seg_11299" s="T152">pro.h:S</ta>
            <ta e="T154" id="Seg_11300" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_11301" s="T155">np.h:S</ta>
            <ta e="T157" id="Seg_11302" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_11303" s="T157">v:pred 0.1.h:S</ta>
            <ta e="T177" id="Seg_11304" s="T176">np:S</ta>
            <ta e="T178" id="Seg_11305" s="T177">v:pred</ta>
            <ta e="T181" id="Seg_11306" s="T180">ptcl:pred</ta>
            <ta e="T183" id="Seg_11307" s="T182">np:O</ta>
            <ta e="T187" id="Seg_11308" s="T186">np:S</ta>
            <ta e="T188" id="Seg_11309" s="T187">v:pred</ta>
            <ta e="T192" id="Seg_11310" s="T191">np.h:S</ta>
            <ta e="T194" id="Seg_11311" s="T193">adj:pred</ta>
            <ta e="T195" id="Seg_11312" s="T194">np:O</ta>
            <ta e="T196" id="Seg_11313" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_11314" s="T196">v:pred 0.3.h:S</ta>
            <ta e="T199" id="Seg_11315" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_11316" s="T200">np.h:O</ta>
            <ta e="T202" id="Seg_11317" s="T201">v:pred 0.3.h:S</ta>
            <ta e="T205" id="Seg_11318" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_11319" s="T205">adj:pred</ta>
            <ta e="T209" id="Seg_11320" s="T208">v:pred 0.3.h:S</ta>
            <ta e="T212" id="Seg_11321" s="T211">adj:pred</ta>
            <ta e="T912" id="Seg_11322" s="T213">conv:pred</ta>
            <ta e="T214" id="Seg_11323" s="T912">v:pred 0.3.h:S</ta>
            <ta e="T215" id="Seg_11324" s="T214">np:O</ta>
            <ta e="T219" id="Seg_11325" s="T218">adj:pred</ta>
            <ta e="T222" id="Seg_11326" s="T221">pro.h:S</ta>
            <ta e="T224" id="Seg_11327" s="T223">v:pred</ta>
            <ta e="T226" id="Seg_11328" s="T225">v:pred 0.2.h:S</ta>
            <ta e="T227" id="Seg_11329" s="T226">v:pred 0.1.h:S</ta>
            <ta e="T229" id="Seg_11330" s="T228">np:S</ta>
            <ta e="T230" id="Seg_11331" s="T229">v:pred</ta>
            <ta e="T232" id="Seg_11332" s="T231">v:pred 0.3.h:S</ta>
            <ta e="T235" id="Seg_11333" s="T234">pro.h:S</ta>
            <ta e="T237" id="Seg_11334" s="T236">v:pred</ta>
            <ta e="T238" id="Seg_11335" s="T237">pro.h:S</ta>
            <ta e="T240" id="Seg_11336" s="T239">v:pred</ta>
            <ta e="T241" id="Seg_11337" s="T240">np:O</ta>
            <ta e="T244" id="Seg_11338" s="T243">np.h:O</ta>
            <ta e="T245" id="Seg_11339" s="T244">ptcl:pred</ta>
            <ta e="T251" id="Seg_11340" s="T910">np.h:O</ta>
            <ta e="T254" id="Seg_11341" s="T252">v:pred 0.3.h:S</ta>
            <ta e="T256" id="Seg_11342" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T258" id="Seg_11343" s="T257">v:pred 0.3.h:S</ta>
            <ta e="T262" id="Seg_11344" s="T261">adj:pred</ta>
            <ta e="T265" id="Seg_11345" s="T264">pro:O</ta>
            <ta e="T266" id="Seg_11346" s="T265">v:pred 0.2.h:S</ta>
            <ta e="T267" id="Seg_11347" s="T266">v:pred 0.2.h:S</ta>
            <ta e="T269" id="Seg_11348" s="T268">pro.h:S</ta>
            <ta e="T271" id="Seg_11349" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_11350" s="T271">pro:O</ta>
            <ta e="T274" id="Seg_11351" s="T273">ptcl.neg</ta>
            <ta e="T275" id="Seg_11352" s="T274">v:pred 0.1.h:S</ta>
            <ta e="T277" id="Seg_11353" s="T276">pro.h:S</ta>
            <ta e="T279" id="Seg_11354" s="T278">v:pred</ta>
            <ta e="T280" id="Seg_11355" s="T279">conv:pred 0.1.h:S</ta>
            <ta e="T283" id="Seg_11356" s="T282">pro.h:S</ta>
            <ta e="T284" id="Seg_11357" s="T283">ptcl.neg</ta>
            <ta e="T285" id="Seg_11358" s="T284">v:pred</ta>
            <ta e="T288" id="Seg_11359" s="T287">pro.h:S</ta>
            <ta e="T290" id="Seg_11360" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_11361" s="T290">np.h:O</ta>
            <ta e="T292" id="Seg_11362" s="T291">v:pred 0.1.h:S</ta>
            <ta e="T294" id="Seg_11363" s="T293">ptcl:pred</ta>
            <ta e="T301" id="Seg_11364" s="T300">np.h:S</ta>
            <ta e="T302" id="Seg_11365" s="T301">v:pred</ta>
            <ta e="T306" id="Seg_11366" s="T305">np.h:S</ta>
            <ta e="T307" id="Seg_11367" s="T306">v:pred</ta>
            <ta e="T308" id="Seg_11368" s="T307">pro.h:S</ta>
            <ta e="T310" id="Seg_11369" s="T309">np.h:O</ta>
            <ta e="T311" id="Seg_11370" s="T310">v:pred</ta>
            <ta e="T314" id="Seg_11371" s="T313">ptcl.neg</ta>
            <ta e="T315" id="Seg_11372" s="T314">v:pred 0.1.h:S</ta>
            <ta e="T317" id="Seg_11373" s="T316">np:O</ta>
            <ta e="T318" id="Seg_11374" s="T317">v:pred 0.1.h:S</ta>
            <ta e="T319" id="Seg_11375" s="T318">pro.h:S</ta>
            <ta e="T320" id="Seg_11376" s="T319">v:pred</ta>
            <ta e="T321" id="Seg_11377" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_11378" s="T321">np:O</ta>
            <ta e="T323" id="Seg_11379" s="T322">v:pred</ta>
            <ta e="T324" id="Seg_11380" s="T323">v:pred 0.2.h:S</ta>
            <ta e="T325" id="Seg_11381" s="T324">pro.h:S</ta>
            <ta e="T328" id="Seg_11382" s="T327">np:O</ta>
            <ta e="T329" id="Seg_11383" s="T328">v:pred</ta>
            <ta e="T330" id="Seg_11384" s="T329">v:pred 0.1.h:S</ta>
            <ta e="T333" id="Seg_11385" s="T332">ptcl:pred</ta>
            <ta e="T335" id="Seg_11386" s="T334">ptcl.neg</ta>
            <ta e="T336" id="Seg_11387" s="T335">v:pred 0.1.h:S</ta>
            <ta e="T338" id="Seg_11388" s="T337">np:S</ta>
            <ta e="T341" id="Seg_11389" s="T340">n:pred</ta>
            <ta e="T343" id="Seg_11390" s="T342">np:S</ta>
            <ta e="T344" id="Seg_11391" s="T343">v:pred</ta>
            <ta e="T346" id="Seg_11392" s="T345">np:S</ta>
            <ta e="T347" id="Seg_11393" s="T346">ptcl.neg</ta>
            <ta e="T348" id="Seg_11394" s="T347">v:pred</ta>
            <ta e="T350" id="Seg_11395" s="T349">v:pred</ta>
            <ta e="T351" id="Seg_11396" s="T350">np:S</ta>
            <ta e="T354" id="Seg_11397" s="T353">v:pred 0.1.h:S</ta>
            <ta e="T357" id="Seg_11398" s="T356">ptcl:pred</ta>
            <ta e="T358" id="Seg_11399" s="T357">np:S</ta>
            <ta e="T359" id="Seg_11400" s="T358">v:pred</ta>
            <ta e="T362" id="Seg_11401" s="T361">ptcl.neg</ta>
            <ta e="T363" id="Seg_11402" s="T362">v:pred 0.1.h:S</ta>
            <ta e="T365" id="Seg_11403" s="T364">pro.h:S</ta>
            <ta e="T367" id="Seg_11404" s="T366">adj:pred</ta>
            <ta e="T368" id="Seg_11405" s="T367">v:pred 0.2.h:S</ta>
            <ta e="T370" id="Seg_11406" s="T369">v:pred 0.2.h:S</ta>
            <ta e="T371" id="Seg_11407" s="T370">np:O</ta>
            <ta e="T372" id="Seg_11408" s="T371">v:pred 0.2.h:S</ta>
            <ta e="T374" id="Seg_11409" s="T373">np:O</ta>
            <ta e="T375" id="Seg_11410" s="T374">v:pred 0.2.h:S</ta>
            <ta e="T376" id="Seg_11411" s="T375">v:pred 0.2.h:S</ta>
            <ta e="T380" id="Seg_11412" s="T379">adj:pred</ta>
            <ta e="T382" id="Seg_11413" s="T381">cop 0.2.h:S</ta>
            <ta e="T383" id="Seg_11414" s="T382">np:O</ta>
            <ta e="T384" id="Seg_11415" s="T383">v:pred 0.2.h:S</ta>
            <ta e="T387" id="Seg_11416" s="T386">np.h:S</ta>
            <ta e="T391" id="Seg_11417" s="T390">v:pred</ta>
            <ta e="T396" id="Seg_11418" s="T395">pro.h:S</ta>
            <ta e="T398" id="Seg_11419" s="T397">v:pred</ta>
            <ta e="T400" id="Seg_11420" s="T399">ptcl.neg</ta>
            <ta e="T401" id="Seg_11421" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T405" id="Seg_11422" s="T404">np.h:S</ta>
            <ta e="T406" id="Seg_11423" s="T405">v:pred</ta>
            <ta e="T407" id="Seg_11424" s="T406">pro.h:S</ta>
            <ta e="T409" id="Seg_11425" s="T408">np:O</ta>
            <ta e="T410" id="Seg_11426" s="T409">v:pred</ta>
            <ta e="T413" id="Seg_11427" s="T412">np:S</ta>
            <ta e="T414" id="Seg_11428" s="T413">v:pred</ta>
            <ta e="T415" id="Seg_11429" s="T414">pro.h:S</ta>
            <ta e="T417" id="Seg_11430" s="T416">v:pred</ta>
            <ta e="T419" id="Seg_11431" s="T418">pro.h:O</ta>
            <ta e="T423" id="Seg_11432" s="T422">ptcl.neg</ta>
            <ta e="T424" id="Seg_11433" s="T423">v:pred 0.1.h:S</ta>
            <ta e="T427" id="Seg_11434" s="T426">np.h:S</ta>
            <ta e="T428" id="Seg_11435" s="T427">v:pred</ta>
            <ta e="T430" id="Seg_11436" s="T429">np.h:S</ta>
            <ta e="T431" id="Seg_11437" s="T430">v:pred</ta>
            <ta e="T433" id="Seg_11438" s="T432">ptcl:pred</ta>
            <ta e="T434" id="Seg_11439" s="T433">v:pred 0.1.h:S</ta>
            <ta e="T439" id="Seg_11440" s="T438">v:pred 0.1.h:S</ta>
            <ta e="T441" id="Seg_11441" s="T440">v:pred 0.1.h:S</ta>
            <ta e="T443" id="Seg_11442" s="T442">np.h:S</ta>
            <ta e="T445" id="Seg_11443" s="T444">v:pred</ta>
            <ta e="T446" id="Seg_11444" s="T445">pro.h:O</ta>
            <ta e="T449" id="Seg_11445" s="T448">ptcl.neg</ta>
            <ta e="T450" id="Seg_11446" s="T449">v:pred 0.1.h:S</ta>
            <ta e="T452" id="Seg_11447" s="T451">ptcl:pred</ta>
            <ta e="T454" id="Seg_11448" s="T453">np.h:O</ta>
            <ta e="T457" id="Seg_11449" s="T456">pro.h:S</ta>
            <ta e="T459" id="Seg_11450" s="T458">adj:pred</ta>
            <ta e="T461" id="Seg_11451" s="T460">v:pred 0.3.h:S</ta>
            <ta e="T465" id="Seg_11452" s="T464">v:pred 0.1.h:S</ta>
            <ta e="T466" id="Seg_11453" s="T465">pro.h:S</ta>
            <ta e="T467" id="Seg_11454" s="T466">v:pred</ta>
            <ta e="T469" id="Seg_11455" s="T468">ptcl:pred</ta>
            <ta e="T470" id="Seg_11456" s="T469">v:pred</ta>
            <ta e="T471" id="Seg_11457" s="T470">pro:O</ta>
            <ta e="T476" id="Seg_11458" s="T475">ptcl:pred</ta>
            <ta e="T480" id="Seg_11459" s="T479">v:pred 0.2.h:S</ta>
            <ta e="T481" id="Seg_11460" s="T480">np.h:O</ta>
            <ta e="T486" id="Seg_11461" s="T485">np:S</ta>
            <ta e="T488" id="Seg_11462" s="T487">n:pred</ta>
            <ta e="T490" id="Seg_11463" s="T489">np.h:S</ta>
            <ta e="T492" id="Seg_11464" s="T491">v:pred</ta>
            <ta e="T493" id="Seg_11465" s="T492">np:O</ta>
            <ta e="T494" id="Seg_11466" s="T493">v:pred 0.3.h:S</ta>
            <ta e="T495" id="Seg_11467" s="T494">v:pred 0.3.h:S</ta>
            <ta e="T497" id="Seg_11468" s="T496">pro.h:S</ta>
            <ta e="T498" id="Seg_11469" s="T497">v:pred</ta>
            <ta e="T501" id="Seg_11470" s="T500">ptcl.neg</ta>
            <ta e="T503" id="Seg_11471" s="T502">ptcl.neg</ta>
            <ta e="T504" id="Seg_11472" s="T503">ptcl:pred</ta>
            <ta e="T506" id="Seg_11473" s="T505">np:S</ta>
            <ta e="T508" id="Seg_11474" s="T507">n:pred</ta>
            <ta e="T510" id="Seg_11475" s="T509">np.h:S</ta>
            <ta e="T511" id="Seg_11476" s="T510">v:pred</ta>
            <ta e="T512" id="Seg_11477" s="T511">v:pred 0.1.h:S</ta>
            <ta e="T514" id="Seg_11478" s="T513">np:O</ta>
            <ta e="T515" id="Seg_11479" s="T514">v:pred 0.1.h:S</ta>
            <ta e="T517" id="Seg_11480" s="T516">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_11481" s="T519">pro.h:S</ta>
            <ta e="T522" id="Seg_11482" s="T521">v:pred</ta>
            <ta e="T552" id="Seg_11483" s="T551">pro.h:S</ta>
            <ta e="T554" id="Seg_11484" s="T553">v:pred</ta>
            <ta e="T556" id="Seg_11485" s="T555">pro.h:S</ta>
            <ta e="T559" id="Seg_11486" s="T558">v:pred</ta>
            <ta e="T561" id="Seg_11487" s="T560">pro.h:S</ta>
            <ta e="T562" id="Seg_11488" s="T561">np:O</ta>
            <ta e="T564" id="Seg_11489" s="T563">v:pred</ta>
            <ta e="T570" id="Seg_11490" s="T569">np:O</ta>
            <ta e="T571" id="Seg_11491" s="T570">v:pred 0.1.h:S</ta>
            <ta e="T573" id="Seg_11492" s="T572">np:O</ta>
            <ta e="T574" id="Seg_11493" s="T573">v:pred 0.1.h:S</ta>
            <ta e="T575" id="Seg_11494" s="T574">np:O</ta>
            <ta e="T576" id="Seg_11495" s="T575">v:pred 0.1.h:S</ta>
            <ta e="T577" id="Seg_11496" s="T576">v:pred 0.2.h:S</ta>
            <ta e="T578" id="Seg_11497" s="T577">v:pred 0.2.h:S</ta>
            <ta e="T585" id="Seg_11498" s="T584">pro.h:S</ta>
            <ta e="T586" id="Seg_11499" s="T585">pro.h:O</ta>
            <ta e="T587" id="Seg_11500" s="T586">v:pred</ta>
            <ta e="T588" id="Seg_11501" s="T587">pro.h:S</ta>
            <ta e="T590" id="Seg_11502" s="T589">v:pred</ta>
            <ta e="T591" id="Seg_11503" s="T590">pro.h:S</ta>
            <ta e="T592" id="Seg_11504" s="T591">v:pred</ta>
            <ta e="T594" id="Seg_11505" s="T593">pro.h:S</ta>
            <ta e="T596" id="Seg_11506" s="T595">ptcl.neg</ta>
            <ta e="T597" id="Seg_11507" s="T596">v:pred</ta>
            <ta e="T598" id="Seg_11508" s="T597">pro.h:S</ta>
            <ta e="T600" id="Seg_11509" s="T599">v:pred</ta>
            <ta e="T601" id="Seg_11510" s="T600">pro.h:S</ta>
            <ta e="T605" id="Seg_11511" s="T604">ptcl.neg</ta>
            <ta e="T606" id="Seg_11512" s="T605">v:pred</ta>
            <ta e="T609" id="Seg_11513" s="T608">np.h:S</ta>
            <ta e="T611" id="Seg_11514" s="T610">v:pred</ta>
            <ta e="T612" id="Seg_11515" s="T611">pro:O</ta>
            <ta e="T614" id="Seg_11516" s="T613">v:pred 0.3.h:S</ta>
            <ta e="T615" id="Seg_11517" s="T614">ptcl:pred</ta>
            <ta e="T619" id="Seg_11518" s="T618">pro:O</ta>
            <ta e="T620" id="Seg_11519" s="T619">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_11520" s="T620">pro.h:S</ta>
            <ta e="T625" id="Seg_11521" s="T624">v:pred</ta>
            <ta e="T628" id="Seg_11522" s="T627">ptcl.neg</ta>
            <ta e="T629" id="Seg_11523" s="T628">v:pred 0.1.h:S</ta>
            <ta e="T636" id="Seg_11524" s="T635">np.h:S</ta>
            <ta e="T638" id="Seg_11525" s="T637">adj:pred</ta>
            <ta e="T640" id="Seg_11526" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T642" id="Seg_11527" s="T641">v:pred 0.3.h:S</ta>
            <ta e="T645" id="Seg_11528" s="T644">np:S</ta>
            <ta e="T646" id="Seg_11529" s="T645">v:pred</ta>
            <ta e="T650" id="Seg_11530" s="T649">np:O</ta>
            <ta e="T651" id="Seg_11531" s="T650">v:pred 0.3.h:S</ta>
            <ta e="T652" id="Seg_11532" s="T651">v:pred 0.3.h:S</ta>
            <ta e="T655" id="Seg_11533" s="T654">conv:pred</ta>
            <ta e="T656" id="Seg_11534" s="T655">v:pred 0.3.h:S</ta>
            <ta e="T657" id="Seg_11535" s="T656">v:pred 0.3.h:S</ta>
            <ta e="T661" id="Seg_11536" s="T660">np.h:S</ta>
            <ta e="T662" id="Seg_11537" s="T661">np.h:S</ta>
            <ta e="T665" id="Seg_11538" s="T664">v:pred</ta>
            <ta e="T667" id="Seg_11539" s="T666">v:pred 0.3.h:S</ta>
            <ta e="T668" id="Seg_11540" s="T667">np:O</ta>
            <ta e="T669" id="Seg_11541" s="T668">v:pred 0.3.h:S</ta>
            <ta e="T670" id="Seg_11542" s="T669">v:pred 0.3.h:S</ta>
            <ta e="T673" id="Seg_11543" s="T672">pro.h:S</ta>
            <ta e="T675" id="Seg_11544" s="T674">v:pred</ta>
            <ta e="T676" id="Seg_11545" s="T675">np:O</ta>
            <ta e="T677" id="Seg_11546" s="T676">v:pred 0.1.h:S</ta>
            <ta e="T678" id="Seg_11547" s="T677">np:O</ta>
            <ta e="T679" id="Seg_11548" s="T678">v:pred 0.1.h:S</ta>
            <ta e="T680" id="Seg_11549" s="T679">np:O</ta>
            <ta e="T681" id="Seg_11550" s="T680">v:pred 0.1.h:S</ta>
            <ta e="T683" id="Seg_11551" s="T682">v:pred 0.1.h:S</ta>
            <ta e="T687" id="Seg_11552" s="T686">np.h:S</ta>
            <ta e="T688" id="Seg_11553" s="T687">n:pred</ta>
            <ta e="T689" id="Seg_11554" s="T688">cop</ta>
            <ta e="T690" id="Seg_11555" s="T689">np.h:S</ta>
            <ta e="T691" id="Seg_11556" s="T690">n:pred</ta>
            <ta e="T692" id="Seg_11557" s="T691">cop</ta>
            <ta e="T694" id="Seg_11558" s="T693">np.h:S</ta>
            <ta e="T695" id="Seg_11559" s="T694">v:pred</ta>
            <ta e="T700" id="Seg_11560" s="T699">np.h:S</ta>
            <ta e="T701" id="Seg_11561" s="T700">v:pred</ta>
            <ta e="T705" id="Seg_11562" s="T704">np.h:S</ta>
            <ta e="T706" id="Seg_11563" s="T705">v:pred</ta>
            <ta e="T708" id="Seg_11564" s="T707">np.h:S</ta>
            <ta e="T709" id="Seg_11565" s="T708">ptcl.neg</ta>
            <ta e="T710" id="Seg_11566" s="T709">v:pred</ta>
            <ta e="T721" id="Seg_11567" s="T720">np.h:O</ta>
            <ta e="T722" id="Seg_11568" s="T721">v:pred 0.3.h:S</ta>
            <ta e="T728" id="Seg_11569" s="T727">np:O</ta>
            <ta e="T729" id="Seg_11570" s="T728">v:pred 0.3.h:S</ta>
            <ta e="T732" id="Seg_11571" s="T731">np.h:S</ta>
            <ta e="T733" id="Seg_11572" s="T732">v:pred</ta>
            <ta e="T736" id="Seg_11573" s="T735">v:pred 0.3.h:S</ta>
            <ta e="T738" id="Seg_11574" s="T737">pro.h:S</ta>
            <ta e="T740" id="Seg_11575" s="T739">pro.h:O</ta>
            <ta e="T742" id="Seg_11576" s="T741">v:pred</ta>
            <ta e="T746" id="Seg_11577" s="T745">pro.h:S</ta>
            <ta e="T748" id="Seg_11578" s="T747">v:pred</ta>
            <ta e="T749" id="Seg_11579" s="T748">v:pred 0.2.h:S</ta>
            <ta e="T750" id="Seg_11580" s="T749">v:pred 0.2.h:S</ta>
            <ta e="T751" id="Seg_11581" s="T750">pro.h:S</ta>
            <ta e="T753" id="Seg_11582" s="T752">ptcl.neg</ta>
            <ta e="T754" id="Seg_11583" s="T753">v:pred</ta>
            <ta e="T758" id="Seg_11584" s="T757">v:pred 0.2.h:S</ta>
            <ta e="T760" id="Seg_11585" s="T759">v:pred 0.2.h:S</ta>
            <ta e="T763" id="Seg_11586" s="T762">np.h:S</ta>
            <ta e="T767" id="Seg_11587" s="T766">v:pred</ta>
            <ta e="T768" id="Seg_11588" s="T767">v:pred 0.3.h:S</ta>
            <ta e="T769" id="Seg_11589" s="T768">v:pred 0.3.h:S</ta>
            <ta e="T771" id="Seg_11590" s="T770">v:pred 0.2.h:S</ta>
            <ta e="T773" id="Seg_11591" s="T772">ptcl:pred</ta>
            <ta e="T775" id="Seg_11592" s="T774">v:pred 0.2.h:S</ta>
            <ta e="T780" id="Seg_11593" s="T779">v:pred 0.2.h:S</ta>
            <ta e="T782" id="Seg_11594" s="T781">ptcl:pred</ta>
            <ta e="T786" id="Seg_11595" s="T785">v:pred 0.2.h:S</ta>
            <ta e="T788" id="Seg_11596" s="T787">pro.h:S</ta>
            <ta e="T790" id="Seg_11597" s="T789">v:pred</ta>
            <ta e="T791" id="Seg_11598" s="T790">np:S</ta>
            <ta e="T793" id="Seg_11599" s="T792">ptcl.neg</ta>
            <ta e="T794" id="Seg_11600" s="T793">v:pred</ta>
            <ta e="T795" id="Seg_11601" s="T794">pro.h:S</ta>
            <ta e="T796" id="Seg_11602" s="T795">v:pred</ta>
            <ta e="T798" id="Seg_11603" s="T797">np:O</ta>
            <ta e="T799" id="Seg_11604" s="T798">v:pred 0.1.h:S</ta>
            <ta e="T801" id="Seg_11605" s="T800">v:pred 0.1.h:S</ta>
            <ta e="T802" id="Seg_11606" s="T801">np:O</ta>
            <ta e="T806" id="Seg_11607" s="T805">v:pred 0.1.h:S</ta>
            <ta e="T808" id="Seg_11608" s="T807">np:O</ta>
            <ta e="T809" id="Seg_11609" s="T808">v:pred 0.1.h:S</ta>
            <ta e="T810" id="Seg_11610" s="T809">np:O</ta>
            <ta e="T811" id="Seg_11611" s="T810">v:pred 0.1.h:S</ta>
            <ta e="T812" id="Seg_11612" s="T811">v:pred 0.1.h:S</ta>
            <ta e="T817" id="Seg_11613" s="T816">np.h:S</ta>
            <ta e="T818" id="Seg_11614" s="T817">v:pred</ta>
            <ta e="T819" id="Seg_11615" s="T818">v:pred 0.1.h:S</ta>
            <ta e="T822" id="Seg_11616" s="T821">v:pred 0.1.h:S</ta>
            <ta e="T825" id="Seg_11617" s="T824">v:pred 0.2.h:S</ta>
            <ta e="T829" id="Seg_11618" s="T828">v:pred 0.2.h:S</ta>
            <ta e="T830" id="Seg_11619" s="T829">v:pred 0.2.h:S</ta>
            <ta e="T836" id="Seg_11620" s="T835">v:pred 0.1.h:S</ta>
            <ta e="T838" id="Seg_11621" s="T837">np.h:O</ta>
            <ta e="T840" id="Seg_11622" s="T839">v:pred 0.1.h:S</ta>
            <ta e="T841" id="Seg_11623" s="T840">np.h:S</ta>
            <ta e="T843" id="Seg_11624" s="T842">v:pred</ta>
            <ta e="T844" id="Seg_11625" s="T843">np.h:S</ta>
            <ta e="T846" id="Seg_11626" s="T845">adj:pred</ta>
            <ta e="T847" id="Seg_11627" s="T846">np:S</ta>
            <ta e="T856" id="Seg_11628" s="T855">np.h:S</ta>
            <ta e="T857" id="Seg_11629" s="T856">ptcl.neg</ta>
            <ta e="T858" id="Seg_11630" s="T857">v:pred</ta>
            <ta e="T859" id="Seg_11631" s="T858">np.h:S</ta>
            <ta e="T860" id="Seg_11632" s="T859">ptcl.neg</ta>
            <ta e="T861" id="Seg_11633" s="T860">v:pred</ta>
            <ta e="T862" id="Seg_11634" s="T861">n:pred</ta>
            <ta e="T863" id="Seg_11635" s="T862">pro.h:S</ta>
            <ta e="T865" id="Seg_11636" s="T864">ptcl.neg</ta>
            <ta e="T866" id="Seg_11637" s="T865">v:pred 0.3.h:S</ta>
            <ta e="T871" id="Seg_11638" s="T870">n:pred</ta>
            <ta e="T872" id="Seg_11639" s="T871">pro.h:S</ta>
            <ta e="T874" id="Seg_11640" s="T873">ptcl.neg</ta>
            <ta e="T875" id="Seg_11641" s="T874">v:pred 0.3.h:S</ta>
            <ta e="T881" id="Seg_11642" s="T880">v:pred 0.2.h:S</ta>
            <ta e="T883" id="Seg_11643" s="T882">pro.h:S</ta>
            <ta e="T887" id="Seg_11644" s="T886">ptcl.neg</ta>
            <ta e="T891" id="Seg_11645" s="T890">pro.h:S</ta>
            <ta e="T894" id="Seg_11646" s="T893">ptcl.neg</ta>
            <ta e="T896" id="Seg_11647" s="T895">v:pred</ta>
            <ta e="T898" id="Seg_11648" s="T897">ptcl.neg</ta>
            <ta e="T899" id="Seg_11649" s="T898">adj:pred</ta>
            <ta e="T902" id="Seg_11650" s="T901">v:pred</ta>
            <ta e="T903" id="Seg_11651" s="T902">np.h:O</ta>
            <ta e="T904" id="Seg_11652" s="T903">pro.h:S</ta>
            <ta e="T906" id="Seg_11653" s="T905">np.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T18" id="Seg_11654" s="T17">%TURK:core</ta>
            <ta e="T19" id="Seg_11655" s="T18">RUS:mod</ta>
            <ta e="T32" id="Seg_11656" s="T31">TURK:disc</ta>
            <ta e="T37" id="Seg_11657" s="T36">RUS:mod</ta>
            <ta e="T42" id="Seg_11658" s="T41">RUS:mod</ta>
            <ta e="T47" id="Seg_11659" s="T46">RUS:mod</ta>
            <ta e="T50" id="Seg_11660" s="T49">RUS:mod</ta>
            <ta e="T54" id="Seg_11661" s="T53">RUS:mod</ta>
            <ta e="T57" id="Seg_11662" s="T56">RUS:mod</ta>
            <ta e="T60" id="Seg_11663" s="T59">RUS:mod</ta>
            <ta e="T61" id="Seg_11664" s="T60">TURK:disc</ta>
            <ta e="T71" id="Seg_11665" s="T70">RUS:gram</ta>
            <ta e="T77" id="Seg_11666" s="T76">TURK:core</ta>
            <ta e="T80" id="Seg_11667" s="T79">RUS:gram</ta>
            <ta e="T91" id="Seg_11668" s="T90">RUS:gram</ta>
            <ta e="T96" id="Seg_11669" s="T95">TURK:core</ta>
            <ta e="T99" id="Seg_11670" s="T98">RUS:gram</ta>
            <ta e="T109" id="Seg_11671" s="T108">RUS:gram</ta>
            <ta e="T112" id="Seg_11672" s="T111">RUS:gram</ta>
            <ta e="T120" id="Seg_11673" s="T119">RUS:gram</ta>
            <ta e="T123" id="Seg_11674" s="T122">RUS:disc</ta>
            <ta e="T133" id="Seg_11675" s="T132">RUS:gram</ta>
            <ta e="T147" id="Seg_11676" s="T146">TURK:gram(INDEF)</ta>
            <ta e="T151" id="Seg_11677" s="T150">RUS:mod</ta>
            <ta e="T152" id="Seg_11678" s="T151">RUS:mod</ta>
            <ta e="T153" id="Seg_11679" s="T152">RUS:gram(INDEF)</ta>
            <ta e="T155" id="Seg_11680" s="T154">RUS:mod</ta>
            <ta e="T159" id="Seg_11681" s="T158">TURK:disc</ta>
            <ta e="T174" id="Seg_11682" s="T173">RUS:gram</ta>
            <ta e="T181" id="Seg_11683" s="T180">RUS:mod</ta>
            <ta e="T182" id="Seg_11684" s="T181">RUS:gram</ta>
            <ta e="T185" id="Seg_11685" s="T184">RUS:gram</ta>
            <ta e="T189" id="Seg_11686" s="T188">TURK:disc</ta>
            <ta e="T193" id="Seg_11687" s="T192">TURK:disc</ta>
            <ta e="T195" id="Seg_11688" s="T194">TURK:cult</ta>
            <ta e="T198" id="Seg_11689" s="T197">TURK:disc</ta>
            <ta e="T203" id="Seg_11690" s="T202">TURK:disc</ta>
            <ta e="T207" id="Seg_11691" s="T206">RUS:core</ta>
            <ta e="T215" id="Seg_11692" s="T214">TURK:cult</ta>
            <ta e="T217" id="Seg_11693" s="T216">RUS:gram</ta>
            <ta e="T228" id="Seg_11694" s="T227">RUS:cult</ta>
            <ta e="T231" id="Seg_11695" s="T230">TURK:disc</ta>
            <ta e="T239" id="Seg_11696" s="T238">RUS:mod</ta>
            <ta e="T241" id="Seg_11697" s="T240">TURK:cult</ta>
            <ta e="T245" id="Seg_11698" s="T244">RUS:mod</ta>
            <ta e="T910" id="Seg_11699" s="T250">RUS:cult</ta>
            <ta e="T251" id="Seg_11700" s="T910">RUS:cult</ta>
            <ta e="T252" id="Seg_11701" s="T251">TURK:disc</ta>
            <ta e="T255" id="Seg_11702" s="T254">TURK:disc</ta>
            <ta e="T257" id="Seg_11703" s="T256">TURK:disc</ta>
            <ta e="T259" id="Seg_11704" s="T258">TURK:disc</ta>
            <ta e="T262" id="Seg_11705" s="T261">TURK:disc</ta>
            <ta e="T272" id="Seg_11706" s="T271">TURK:gram(INDEF)</ta>
            <ta e="T276" id="Seg_11707" s="T275">RUS:gram</ta>
            <ta e="T286" id="Seg_11708" s="T285">%TURK:core</ta>
            <ta e="T287" id="Seg_11709" s="T286">RUS:gram</ta>
            <ta e="T289" id="Seg_11710" s="T288">TURK:disc</ta>
            <ta e="T290" id="Seg_11711" s="T289">%TURK:core</ta>
            <ta e="T293" id="Seg_11712" s="T292">%TURK:core</ta>
            <ta e="T294" id="Seg_11713" s="T293">RUS:mod</ta>
            <ta e="T296" id="Seg_11714" s="T295">TURK:disc</ta>
            <ta e="T300" id="Seg_11715" s="T299">TURK:disc</ta>
            <ta e="T303" id="Seg_11716" s="T302">RUS:gram</ta>
            <ta e="T309" id="Seg_11717" s="T308">TURK:disc</ta>
            <ta e="T312" id="Seg_11718" s="T311">TURK:disc</ta>
            <ta e="T315" id="Seg_11719" s="T314">%TURK:core</ta>
            <ta e="T326" id="Seg_11720" s="T325">RUS:mod</ta>
            <ta e="T332" id="Seg_11721" s="T331">%TURK:core</ta>
            <ta e="T333" id="Seg_11722" s="T332">RUS:mod</ta>
            <ta e="T334" id="Seg_11723" s="T333">TURK:disc</ta>
            <ta e="T336" id="Seg_11724" s="T335">%TURK:core</ta>
            <ta e="T342" id="Seg_11725" s="T341">TURK:disc</ta>
            <ta e="T357" id="Seg_11726" s="T356">RUS:mod</ta>
            <ta e="T360" id="Seg_11727" s="T359">TURK:disc</ta>
            <ta e="T363" id="Seg_11728" s="T362">%TURK:core</ta>
            <ta e="T385" id="Seg_11729" s="T384">RUS:gram</ta>
            <ta e="T386" id="Seg_11730" s="T385">TURK:disc</ta>
            <ta e="T392" id="Seg_11731" s="T391">TURK:disc</ta>
            <ta e="T405" id="Seg_11732" s="T404">TURK:core</ta>
            <ta e="T408" id="Seg_11733" s="T407">RUS:gram</ta>
            <ta e="T416" id="Seg_11734" s="T415">TURK:disc</ta>
            <ta e="T418" id="Seg_11735" s="T417">RUS:gram</ta>
            <ta e="T421" id="Seg_11736" s="T420">TURK:disc</ta>
            <ta e="T424" id="Seg_11737" s="T423">%TURK:core</ta>
            <ta e="T433" id="Seg_11738" s="T432">RUS:mod</ta>
            <ta e="T437" id="Seg_11739" s="T436">TURK:disc</ta>
            <ta e="T440" id="Seg_11740" s="T439">TURK:disc</ta>
            <ta e="T442" id="Seg_11741" s="T441">TURK:disc</ta>
            <ta e="T444" id="Seg_11742" s="T443">TURK:disc</ta>
            <ta e="T450" id="Seg_11743" s="T449">%TURK:core</ta>
            <ta e="T452" id="Seg_11744" s="T451">RUS:mod</ta>
            <ta e="T460" id="Seg_11745" s="T459">TURK:disc</ta>
            <ta e="T464" id="Seg_11746" s="T463">TURK:disc</ta>
            <ta e="T469" id="Seg_11747" s="T468">RUS:mod</ta>
            <ta e="T476" id="Seg_11748" s="T475">RUS:mod</ta>
            <ta e="T489" id="Seg_11749" s="T488">RUS:gram</ta>
            <ta e="T496" id="Seg_11750" s="T495">RUS:gram</ta>
            <ta e="T502" id="Seg_11751" s="T501">%TURK:core</ta>
            <ta e="T503" id="Seg_11752" s="T502">RUS:gram</ta>
            <ta e="T504" id="Seg_11753" s="T503">RUS:mod</ta>
            <ta e="T510" id="Seg_11754" s="T509">TURK:core</ta>
            <ta e="T514" id="Seg_11755" s="T513">TURK:cult</ta>
            <ta e="T518" id="Seg_11756" s="T517">RUS:cult</ta>
            <ta e="T521" id="Seg_11757" s="T520">TURK:disc</ta>
            <ta e="T523" id="Seg_11758" s="T522">TURK:disc</ta>
            <ta e="T555" id="Seg_11759" s="T554">RUS:gram</ta>
            <ta e="T557" id="Seg_11760" s="T556">TURK:disc</ta>
            <ta e="T566" id="Seg_11761" s="T565">TURK:core</ta>
            <ta e="T572" id="Seg_11762" s="T571">TURK:disc</ta>
            <ta e="T573" id="Seg_11763" s="T572">TURK:cult</ta>
            <ta e="T589" id="Seg_11764" s="T588">TURK:disc</ta>
            <ta e="T593" id="Seg_11765" s="T592">RUS:gram</ta>
            <ta e="T612" id="Seg_11766" s="T611">TURK:gram(INDEF)</ta>
            <ta e="T613" id="Seg_11767" s="T612">TURK:disc</ta>
            <ta e="T614" id="Seg_11768" s="T613">%TURK:core</ta>
            <ta e="T615" id="Seg_11769" s="T614">RUS:mod</ta>
            <ta e="T620" id="Seg_11770" s="T619">%TURK:core</ta>
            <ta e="T626" id="Seg_11771" s="T625">TURK:disc</ta>
            <ta e="T629" id="Seg_11772" s="T628">%TURK:core</ta>
            <ta e="T637" id="Seg_11773" s="T636">TURK:disc</ta>
            <ta e="T653" id="Seg_11774" s="T652">TURK:disc</ta>
            <ta e="T658" id="Seg_11775" s="T657">TURK:disc</ta>
            <ta e="T663" id="Seg_11776" s="T662">TURK:disc</ta>
            <ta e="T666" id="Seg_11777" s="T665">TURK:disc</ta>
            <ta e="T668" id="Seg_11778" s="T667">RUS:cult</ta>
            <ta e="T680" id="Seg_11779" s="T679">RUS:cult</ta>
            <ta e="T702" id="Seg_11780" s="T701">TURK:disc</ta>
            <ta e="T707" id="Seg_11781" s="T706">RUS:gram</ta>
            <ta e="T724" id="Seg_11782" s="T722">TURK:disc</ta>
            <ta e="T727" id="Seg_11783" s="T726">RUS:mod</ta>
            <ta e="T737" id="Seg_11784" s="T736">RUS:cult</ta>
            <ta e="T741" id="Seg_11785" s="T740">TURK:disc</ta>
            <ta e="T744" id="Seg_11786" s="T743">TURK:disc</ta>
            <ta e="T745" id="Seg_11787" s="T744">RUS:mod</ta>
            <ta e="T759" id="Seg_11788" s="T758">RUS:gram</ta>
            <ta e="T765" id="Seg_11789" s="T764">TURK:disc</ta>
            <ta e="T770" id="Seg_11790" s="T769">RUS:mod</ta>
            <ta e="T772" id="Seg_11791" s="T771">TURK:core</ta>
            <ta e="T773" id="Seg_11792" s="T772">RUS:mod</ta>
            <ta e="T778" id="Seg_11793" s="T777">RUS:mod</ta>
            <ta e="T782" id="Seg_11794" s="T781">RUS:mod</ta>
            <ta e="T783" id="Seg_11795" s="T782">TURK:core</ta>
            <ta e="T785" id="Seg_11796" s="T784">RUS:mod</ta>
            <ta e="T792" id="Seg_11797" s="T791">RUS:mod</ta>
            <ta e="T798" id="Seg_11798" s="T797">TURK:cult</ta>
            <ta e="T802" id="Seg_11799" s="T801">RUS:cult</ta>
            <ta e="T819" id="Seg_11800" s="T818">%TURK:core</ta>
            <ta e="T821" id="Seg_11801" s="T820">TURK:disc</ta>
            <ta e="T828" id="Seg_11802" s="T827">%TURK:core</ta>
            <ta e="T841" id="Seg_11803" s="T840">TURK:cult</ta>
            <ta e="T842" id="Seg_11804" s="T841">TURK:disc</ta>
            <ta e="T850" id="Seg_11805" s="T849">TURK:disc</ta>
            <ta e="T867" id="Seg_11806" s="T866">%TURK:core</ta>
            <ta e="T876" id="Seg_11807" s="T875">%TURK:core</ta>
            <ta e="T880" id="Seg_11808" s="T879">%TURK:core</ta>
            <ta e="T899" id="Seg_11809" s="T898">TURK:core</ta>
            <ta e="T901" id="Seg_11810" s="T900">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T281" id="Seg_11811" s="T280">RUS:int</ta>
            <ta e="T389" id="Seg_11812" s="T388">RUS:int</ta>
            <ta e="T907" id="Seg_11813" s="T906">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T19" id="Seg_11814" s="T13">(…) сейчас надо говорить.</ta>
            <ta e="T22" id="Seg_11815" s="T19">Потом я на пашню пойду.</ta>
            <ta e="T24" id="Seg_11816" s="T22">Землю пахать. </ta>
            <ta e="T27" id="Seg_11817" s="T24">Потом хлеб сеять.</ta>
            <ta e="T32" id="Seg_11818" s="T27">Потом он будет расти, большой вырастет.</ta>
            <ta e="T34" id="Seg_11819" s="T32">Станет белый.</ta>
            <ta e="T37" id="Seg_11820" s="T34">Потом [ему] надо созреть.</ta>
            <ta e="T42" id="Seg_11821" s="T39">Потом его надо обмолотить.</ta>
            <ta e="T45" id="Seg_11822" s="T42">Потом сложить в мешок.</ta>
            <ta e="T48" id="Seg_11823" s="T45">Потом его надо смолоть.</ta>
            <ta e="T51" id="Seg_11824" s="T48">Потом просеивать надо.</ta>
            <ta e="T54" id="Seg_11825" s="T51">Потом надо поставить [тесто].</ta>
            <ta e="T57" id="Seg_11826" s="T54">Потом надо испечь.</ta>
            <ta e="T60" id="Seg_11827" s="T57">Потом есть надо.</ta>
            <ta e="T61" id="Seg_11828" s="T60">Все!</ta>
            <ta e="T65" id="Seg_11829" s="T62">Ты идешь.</ta>
            <ta e="T69" id="Seg_11830" s="T65">Почему мне не сказал?</ta>
            <ta e="T74" id="Seg_11831" s="T69">Я бы тебе траву дала.</ta>
            <ta e="T78" id="Seg_11832" s="T74">Там у меня родственники есть.</ta>
            <ta e="T82" id="Seg_11833" s="T78">Ты бы его [=их] повидал.</ta>
            <ta e="T83" id="Seg_11834" s="T82">Хватит!</ta>
            <ta e="T93" id="Seg_11835" s="T84">Ты почему мне не сказал, что едешь [в] Красноярск?</ta>
            <ta e="T97" id="Seg_11836" s="T93">У меня там родственник есть.</ta>
            <ta e="T102" id="Seg_11837" s="T97">Я бы тебе траву дала.</ta>
            <ta e="T105" id="Seg_11838" s="T102">У него сердце болит.</ta>
            <ta e="T110" id="Seg_11839" s="T105">Он бы [ее] выпил.</ta>
            <ta e="T114" id="Seg_11840" s="T110">Ты бы его повидал.</ta>
            <ta e="T115" id="Seg_11841" s="T114">Хватит!</ta>
            <ta e="T119" id="Seg_11842" s="T116">Садись есть картошку.</ta>
            <ta e="T122" id="Seg_11843" s="T119">А что за картошка?</ta>
            <ta e="T125" id="Seg_11844" s="T122">Да я потерла.</ta>
            <ta e="T128" id="Seg_11845" s="T125">Потом взяла муку.</ta>
            <ta e="T135" id="Seg_11846" s="T128">Потом я положила сахар и ягоды.</ta>
            <ta e="T139" id="Seg_11847" s="T135">Налила теплой воды.</ta>
            <ta e="T141" id="Seg_11848" s="T139">Потом сварила.</ta>
            <ta e="T143" id="Seg_11849" s="T141">Садись, поешь!</ta>
            <ta e="T146" id="Seg_11850" s="T144">Мальчика нет.</ta>
            <ta e="T148" id="Seg_11851" s="T146">Куда-то ушел.</ta>
            <ta e="T151" id="Seg_11852" s="T149">Надо его поискать.</ta>
            <ta e="T154" id="Seg_11853" s="T151">Может, [его] кто-нибудь убил.</ta>
            <ta e="T157" id="Seg_11854" s="T154">Может, медведь съел.</ta>
            <ta e="T160" id="Seg_11855" s="T157">Жалею ведь очень.</ta>
            <ta e="T161" id="Seg_11856" s="T160">Хватит!</ta>
            <ta e="T170" id="Seg_11857" s="T162">Один, два, три, четыре, пять, семь, восемь, десять.</ta>
            <ta e="T172" id="Seg_11858" s="T170">Сбилась я. </ta>
            <ta e="T181" id="Seg_11859" s="T173">Если бы были свои лошади, можно было бы пахать.</ta>
            <ta e="T184" id="Seg_11860" s="T181">И картошку есть.</ta>
            <ta e="T188" id="Seg_11861" s="T184">А то своих лошадей нет.</ta>
            <ta e="T189" id="Seg_11862" s="T188">Все!</ta>
            <ta e="T194" id="Seg_11863" s="T190">Моя невестка пьяная.</ta>
            <ta e="T196" id="Seg_11864" s="T194">Водки выпила.</ta>
            <ta e="T201" id="Seg_11865" s="T196">Пришла, поругалась со своим мужем.</ta>
            <ta e="T203" id="Seg_11866" s="T201">Они подрались.</ta>
            <ta e="T206" id="Seg_11867" s="T203">Она очень сердитая.</ta>
            <ta e="T209" id="Seg_11868" s="T206">По-всякому ругалась.</ta>
            <ta e="T212" id="Seg_11869" s="T209">Как ей надо. [?]</ta>
            <ta e="T216" id="Seg_11870" s="T212">Опять ушла водку пить.</ta>
            <ta e="T219" id="Seg_11871" s="T216">А то ей мало.</ta>
            <ta e="T220" id="Seg_11872" s="T219">Хватит.</ta>
            <ta e="T224" id="Seg_11873" s="T221">Вы были в лесу.</ta>
            <ta e="T226" id="Seg_11874" s="T224">Где вы спали?</ta>
            <ta e="T228" id="Seg_11875" s="T226">Мы спали в палатке.</ta>
            <ta e="T230" id="Seg_11876" s="T228">Медведь пришел.</ta>
            <ta e="T233" id="Seg_11877" s="T230">Ходил там.</ta>
            <ta e="T237" id="Seg_11878" s="T233">Мы очень испугались.</ta>
            <ta e="T241" id="Seg_11879" s="T237">Он тоже боится… ружья. [?]</ta>
            <ta e="T248" id="Seg_11880" s="T241">Надо у хозяина спросить разрешения лечь, поспать.</ta>
            <ta e="T249" id="Seg_11881" s="T248">Все!</ta>
            <ta e="T254" id="Seg_11882" s="T250">Курочка к луню сватается.</ta>
            <ta e="T256" id="Seg_11883" s="T254">Кудахчет.</ta>
            <ta e="T258" id="Seg_11884" s="T256">Бьет крыльями.</ta>
            <ta e="T260" id="Seg_11885" s="T258">[?]</ta>
            <ta e="T262" id="Seg_11886" s="T260">Это все. [?]</ta>
            <ta e="T266" id="Seg_11887" s="T263">Что ты боишься?</ta>
            <ta e="T268" id="Seg_11888" s="T266">Иди один.</ta>
            <ta e="T275" id="Seg_11889" s="T268">Я одна ходила, я никого не боюсь</ta>
            <ta e="T279" id="Seg_11890" s="T275">А ты всегда боишься.</ta>
            <ta e="T280" id="Seg_11891" s="T279">Я забываю.</ta>
            <ta e="T281" id="Seg_11892" s="T280">Хватит.</ta>
            <ta e="T286" id="Seg_11893" s="T282">Они не умеют разговаривать.</ta>
            <ta e="T290" id="Seg_11894" s="T286">А я говорю.</ta>
            <ta e="T292" id="Seg_11895" s="T290">Я мать спрашивала.</ta>
            <ta e="T295" id="Seg_11896" s="T292">Мне хочется разговаривать.</ta>
            <ta e="T296" id="Seg_11897" s="T295">Все.</ta>
            <ta e="T302" id="Seg_11898" s="T297">Тут нет поселенцев.</ta>
            <ta e="T307" id="Seg_11899" s="T302">Только камасинцы живут.</ta>
            <ta e="T311" id="Seg_11900" s="T307">Они поселенцев бояться.</ta>
            <ta e="T315" id="Seg_11901" s="T311">Я не буду [с ними?] говорить.</ta>
            <ta e="T318" id="Seg_11902" s="T315">Сейчас я принесу мешок.</ta>
            <ta e="T320" id="Seg_11903" s="T318">Ты будешь держать</ta>
            <ta e="T323" id="Seg_11904" s="T320">Я картошки насыплю.</ta>
            <ta e="T324" id="Seg_11905" s="T323">Сидите!</ta>
            <ta e="T329" id="Seg_11906" s="T324">Я еще две кастрюли принесу.</ta>
            <ta e="T330" id="Seg_11907" s="T329">Сварю.</ta>
            <ta e="T333" id="Seg_11908" s="T330">Потом можно [будет] разговаривать.</ta>
            <ta e="T336" id="Seg_11909" s="T333">Все, я не говорю.</ta>
            <ta e="T341" id="Seg_11910" s="T337">Сегодня очень жаркий день.</ta>
            <ta e="T344" id="Seg_11911" s="T341">Хлеб сгорит.</ta>
            <ta e="T348" id="Seg_11912" s="T344">Хлеб не вырастет.</ta>
            <ta e="T351" id="Seg_11913" s="T348">Потом [не] будет хлеба.</ta>
            <ta e="T353" id="Seg_11914" s="T351">Что есть?</ta>
            <ta e="T359" id="Seg_11915" s="T353">Я умерла, умереть можно, [если] нет хлеба.</ta>
            <ta e="T363" id="Seg_11916" s="T359">Хватит, не буду [больше] говорить.</ta>
            <ta e="T367" id="Seg_11917" s="T364">Ты очень грязный.</ta>
            <ta e="T368" id="Seg_11918" s="T367">Помойся!</ta>
            <ta e="T370" id="Seg_11919" s="T368">В баню сходи.</ta>
            <ta e="T372" id="Seg_11920" s="T370">Возьми мыло!</ta>
            <ta e="T375" id="Seg_11921" s="T372">Потом возьми рубашку.</ta>
            <ta e="T376" id="Seg_11922" s="T375">Одень ее.</ta>
            <ta e="T377" id="Seg_11923" s="T376">Хватит.</ta>
            <ta e="T382" id="Seg_11924" s="T378">Потом ты станешь очень красивый.</ta>
            <ta e="T385" id="Seg_11925" s="T382">Если оденешь рубашку.</ta>
            <ta e="T392" id="Seg_11926" s="T385">Все девушки будут тебя (целовать?).</ta>
            <ta e="T393" id="Seg_11927" s="T392">Красивый.</ta>
            <ta e="T394" id="Seg_11928" s="T393">Хватит.</ta>
            <ta e="T398" id="Seg_11929" s="T395">Они поехали в Красноярск.</ta>
            <ta e="T401" id="Seg_11930" s="T398">Мне не сказали.</ta>
            <ta e="T406" id="Seg_11931" s="T401">У меня там есть родственник.</ta>
            <ta e="T410" id="Seg_11932" s="T406">Я бы траву дала.</ta>
            <ta e="T414" id="Seg_11933" s="T410">У него сердце болит.</ta>
            <ta e="T420" id="Seg_11934" s="T414">Они бы его повидали.</ta>
            <ta e="T424" id="Seg_11935" s="T420">Все, больше я не буду говорить.</ta>
            <ta e="T428" id="Seg_11936" s="T425">Черная женщина пришла.</ta>
            <ta e="T433" id="Seg_11937" s="T428">Белый мужчина говорит: надо взять [ее в жены].</ta>
            <ta e="T435" id="Seg_11938" s="T433">Пойдем вместе.</ta>
            <ta e="T440" id="Seg_11939" s="T435">Давай спать на (дороге?).</ta>
            <ta e="T442" id="Seg_11940" s="T440">Давай целоваться.</ta>
            <ta e="T446" id="Seg_11941" s="T442">Твой муж будет тебя бить.</ta>
            <ta e="T447" id="Seg_11942" s="T446">Хватит.</ta>
            <ta e="T450" id="Seg_11943" s="T447">Больше не буду говорить.</ta>
            <ta e="T456" id="Seg_11944" s="T451">Надо мне ребенка помыть.</ta>
            <ta e="T459" id="Seg_11945" s="T456">Он очень грязный.</ta>
            <ta e="T462" id="Seg_11946" s="T459">Он много какал.</ta>
            <ta e="T465" id="Seg_11947" s="T462">Я его терла.</ta>
            <ta e="T467" id="Seg_11948" s="T465">Он высох.</ta>
            <ta e="T471" id="Seg_11949" s="T467">Потом надо его причесать.</ta>
            <ta e="T476" id="Seg_11950" s="T471">Потом надо поесть.</ta>
            <ta e="T479" id="Seg_11951" s="T476">Потом [ему надо] спать.</ta>
            <ta e="T481" id="Seg_11952" s="T479">Покачай ребенка.</ta>
            <ta e="T488" id="Seg_11953" s="T485">Сегодня праздник.</ta>
            <ta e="T492" id="Seg_11954" s="T488">А люди всё работают.</ta>
            <ta e="T494" id="Seg_11955" s="T492">Сажают картошку.</ta>
            <ta e="T495" id="Seg_11956" s="T494">Пашут.</ta>
            <ta e="T498" id="Seg_11957" s="T495">А мы сидим.</ta>
            <ta e="T504" id="Seg_11958" s="T498">Хватит, больше говорить неохота.</ta>
            <ta e="T508" id="Seg_11959" s="T505">Сегодня праздник.</ta>
            <ta e="T511" id="Seg_11960" s="T508">Меня мой родственник позвал.</ta>
            <ta e="T512" id="Seg_11961" s="T511">Пойдем.</ta>
            <ta e="T515" id="Seg_11962" s="T512">Там водку будем пить.</ta>
            <ta e="T518" id="Seg_11963" s="T515">Там играют на гармони.</ta>
            <ta e="T522" id="Seg_11964" s="T518">Мы танцуем.</ta>
            <ta e="T523" id="Seg_11965" s="T522">Все.</ta>
            <ta e="T531" id="Seg_11966" s="T524">Один, один, два. три, четыре, пять, семь.</ta>
            <ta e="T533" id="Seg_11967" s="T531">Девять, восемь.</ta>
            <ta e="T541" id="Seg_11968" s="T540">Десять.</ta>
            <ta e="T550" id="Seg_11969" s="T542">Одиннадцать, двенадцать, пятнадцать, шестнадцать.</ta>
            <ta e="T559" id="Seg_11970" s="T551">Я пришла одна, а они впятером пришли.</ta>
            <ta e="T564" id="Seg_11971" s="T560">Я суп варю.</ta>
            <ta e="T567" id="Seg_11972" s="T564">Очень хороший суп.</ta>
            <ta e="T569" id="Seg_11973" s="T567">Вкусный, с мясом.</ta>
            <ta e="T572" id="Seg_11974" s="T569">Я лук положила.</ta>
            <ta e="T574" id="Seg_11975" s="T572">Соль положила.</ta>
            <ta e="T576" id="Seg_11976" s="T574">Картошку положила.</ta>
            <ta e="T578" id="Seg_11977" s="T576">Садись, ешь!</ta>
            <ta e="T579" id="Seg_11978" s="T578">Вместе.</ta>
            <ta e="T580" id="Seg_11979" s="T579">Хватит!</ta>
            <ta e="T584" id="Seg_11980" s="T581">Очень сильный человек.</ta>
            <ta e="T587" id="Seg_11981" s="T584">Я его боюсь.</ta>
            <ta e="T590" id="Seg_11982" s="T587">Он убьет.</ta>
            <ta e="T597" id="Seg_11983" s="T590">Я спряталась, чтобы он меня не увидел.</ta>
            <ta e="T600" id="Seg_11984" s="T597">Я убегу в лес.</ta>
            <ta e="T606" id="Seg_11985" s="T600">Он меня там не найдет.</ta>
            <ta e="T607" id="Seg_11986" s="T606">Хватит!</ta>
            <ta e="T611" id="Seg_11987" s="T608">Много мужчин собралось.</ta>
            <ta e="T620" id="Seg_11988" s="T611">Они о чем-то говорят, надо пойти послушать, о чем они говорят.</ta>
            <ta e="T625" id="Seg_11989" s="T620">Я тогда буду знать.</ta>
            <ta e="T629" id="Seg_11990" s="T625">Все, больше я не буду говорить.</ta>
            <ta e="T638" id="Seg_11991" s="T634">Этот человек пьяный.</ta>
            <ta e="T640" id="Seg_11992" s="T638">Сел на лошадь.</ta>
            <ta e="T643" id="Seg_11993" s="T640">Потом упал на землю.</ta>
            <ta e="T646" id="Seg_11994" s="T643">Потом лошадь убежала.</ta>
            <ta e="T651" id="Seg_11995" s="T646">Он шапку потерял.</ta>
            <ta e="T653" id="Seg_11996" s="T651">Он остался.</ta>
            <ta e="T657" id="Seg_11997" s="T653">Он лежит и спит, он упал.</ta>
            <ta e="T659" id="Seg_11998" s="T657">Все, хватит!</ta>
            <ta e="T667" id="Seg_11999" s="T660">Парней и девушек много собралось, поют.</ta>
            <ta e="T670" id="Seg_12000" s="T667">Играют на гармони, танцуют.</ta>
            <ta e="T671" id="Seg_12001" s="T670">Хватит.</ta>
            <ta e="T675" id="Seg_12002" s="T672">Я пошла в лес.</ta>
            <ta e="T677" id="Seg_12003" s="T675">Собирала ягоды.</ta>
            <ta e="T679" id="Seg_12004" s="T677">Черемшу собирала.</ta>
            <ta e="T683" id="Seg_12005" s="T679">Пирогов напекла, вам дам.</ta>
            <ta e="T684" id="Seg_12006" s="T683">Хватит!</ta>
            <ta e="T692" id="Seg_12007" s="T685">Моя мать была камасинкой, отец был поселенцем.</ta>
            <ta e="T695" id="Seg_12008" s="T692">[У них] было два сына.</ta>
            <ta e="T701" id="Seg_12009" s="T698">Шесть дочерей было.</ta>
            <ta e="T703" id="Seg_12010" s="T701">Все, хватит!</ta>
            <ta e="T710" id="Seg_12011" s="T704">Трое умерло, а трое не умерло.</ta>
            <ta e="T714" id="Seg_12012" s="T710">Мой отец соболей…</ta>
            <ta e="T722" id="Seg_12013" s="T719">Пятнадцать убил.</ta>
            <ta e="T724" id="Seg_12014" s="T722">Все.</ta>
            <ta e="T725" id="Seg_12015" s="T724">Хватит!</ta>
            <ta e="T729" id="Seg_12016" s="T725">Еще мешок белок принес.</ta>
            <ta e="T730" id="Seg_12017" s="T729">Хватит!</ta>
            <ta e="T733" id="Seg_12018" s="T730">Камасинец заболел.</ta>
            <ta e="T742" id="Seg_12019" s="T734">Он пошел к шаману, тот его лечил.</ta>
            <ta e="T745" id="Seg_12020" s="T742">Все, наверное.</ta>
            <ta e="T748" id="Seg_12021" s="T745">Я очень голодный.</ta>
            <ta e="T754" id="Seg_12022" s="T748">Сядь, поешь, кто тебе не дает.</ta>
            <ta e="T760" id="Seg_12023" s="T754">Потом ложись и спи!.</ta>
            <ta e="T761" id="Seg_12024" s="T760">Хватит.</ta>
            <ta e="T767" id="Seg_12025" s="T762">Дети играют на улице.</ta>
            <ta e="T769" id="Seg_12026" s="T767">Кричат, дерутся.</ta>
            <ta e="T771" id="Seg_12027" s="T769">Зачем ты дерешься?</ta>
            <ta e="T774" id="Seg_12028" s="T771">Хорошо надо играть.</ta>
            <ta e="T776" id="Seg_12029" s="T774">Не драться.</ta>
            <ta e="T778" id="Seg_12030" s="T776">Хватит, наверное.</ta>
            <ta e="T781" id="Seg_12031" s="T779">Не деритесь!</ta>
            <ta e="T784" id="Seg_12032" s="T781">Надо хорошо играть.</ta>
            <ta e="T786" id="Seg_12033" s="T784">Зачем ты дерешься?</ta>
            <ta e="T790" id="Seg_12034" s="T787">Я сегодня встала.</ta>
            <ta e="T796" id="Seg_12035" s="T790">Солнце еще не встало, когда я встала.</ta>
            <ta e="T799" id="Seg_12036" s="T796">Я корову подоила.</ta>
            <ta e="T801" id="Seg_12037" s="T799">Теленка напоила.</ta>
            <ta e="T806" id="Seg_12038" s="T801">Кур покормила.</ta>
            <ta e="T809" id="Seg_12039" s="T806">Потом (сварила?) яйца.</ta>
            <ta e="T811" id="Seg_12040" s="T809">Сварила картошку.</ta>
            <ta e="T812" id="Seg_12041" s="T811">Поела.</ta>
            <ta e="T819" id="Seg_12042" s="T812">Потом камасинцы пришли, я поговорила.</ta>
            <ta e="T822" id="Seg_12043" s="T819">Я устала.</ta>
            <ta e="T823" id="Seg_12044" s="T822">Хватит!</ta>
            <ta e="T826" id="Seg_12045" s="T824">Идите домой!</ta>
            <ta e="T828" id="Seg_12046" s="T826">Хватит говорить.</ta>
            <ta e="T829" id="Seg_12047" s="T828">Поешьте!</ta>
            <ta e="T831" id="Seg_12048" s="T829">Ляг спать.</ta>
            <ta e="T833" id="Seg_12049" s="T831">Хватит, больше [нет?].</ta>
            <ta e="T836" id="Seg_12050" s="T834">Я ходила на тот конец.</ta>
            <ta e="T840" id="Seg_12051" s="T836">Очень много людей видела.</ta>
            <ta e="T843" id="Seg_12052" s="T840">Начальник пришел.</ta>
            <ta e="T847" id="Seg_12053" s="T843">Там мужчин много, лошадей.</ta>
            <ta e="T850" id="Seg_12054" s="T847">Девушки, парни.</ta>
            <ta e="T851" id="Seg_12055" s="T850">Хватит!</ta>
            <ta e="T861" id="Seg_12056" s="T852">Почему этот худой человек не пришел, девушка не пришла.</ta>
            <ta e="T863" id="Seg_12057" s="T861">Где они?</ta>
            <ta e="T868" id="Seg_12058" s="T863">Почему они не пришли со мной говорить?</ta>
            <ta e="T869" id="Seg_12059" s="T868">Хватит.</ta>
            <ta e="T872" id="Seg_12060" s="T870">Где они?</ta>
            <ta e="T876" id="Seg_12061" s="T872">Почему не пришли говорить?</ta>
            <ta e="T880" id="Seg_12062" s="T877">Хватит [на] сегодня говорить.</ta>
            <ta e="T882" id="Seg_12063" s="T880">Иди домой! </ta>
            <ta e="T890" id="Seg_12064" s="T882">Ты меня так не…</ta>
            <ta e="T896" id="Seg_12065" s="T890">Ты меня так не учи!</ta>
            <ta e="T900" id="Seg_12066" s="T896">Нехорошо так говорить.</ta>
            <ta e="T903" id="Seg_12067" s="T900">Зачем людей выгонять?</ta>
            <ta e="T907" id="Seg_12068" s="T903">Это ведь камасинцы.</ta>
            <ta e="T908" id="Seg_12069" s="T907">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T19" id="Seg_12070" s="T13">(…) now [we] have to speak.</ta>
            <ta e="T22" id="Seg_12071" s="T19">Then I will go to the field.</ta>
            <ta e="T24" id="Seg_12072" s="T22">To plough the ground.</ta>
            <ta e="T27" id="Seg_12073" s="T24">Then to sow grain.</ta>
            <ta e="T32" id="Seg_12074" s="T27">Then it will grow, it will grow big.</ta>
            <ta e="T34" id="Seg_12075" s="T32">It will become white.</ta>
            <ta e="T37" id="Seg_12076" s="T34">Then [it] will ripen.</ta>
            <ta e="T42" id="Seg_12077" s="T39">Then one has to thresh it.</ta>
            <ta e="T45" id="Seg_12078" s="T42">Then to pour into a bag.</ta>
            <ta e="T48" id="Seg_12079" s="T45">Then one has to grind [it].</ta>
            <ta e="T51" id="Seg_12080" s="T48">Then one has to sieve it.</ta>
            <ta e="T54" id="Seg_12081" s="T51">Then one has to make dough.</ta>
            <ta e="T57" id="Seg_12082" s="T54">Then one has to bake [it].</ta>
            <ta e="T60" id="Seg_12083" s="T57">Then one has to eat [it].</ta>
            <ta e="T61" id="Seg_12084" s="T60">That's all!</ta>
            <ta e="T65" id="Seg_12085" s="T62">You are going.</ta>
            <ta e="T69" id="Seg_12086" s="T65">Why didn't you tell me?</ta>
            <ta e="T74" id="Seg_12087" s="T69">I would have given you grass.</ta>
            <ta e="T78" id="Seg_12088" s="T74">I have relatives there.</ta>
            <ta e="T82" id="Seg_12089" s="T78">You would meet him [=them].</ta>
            <ta e="T83" id="Seg_12090" s="T82">Enough!</ta>
            <ta e="T93" id="Seg_12091" s="T84">Why didn't you tell me that you are going [to] Krasnoyarsk?</ta>
            <ta e="T97" id="Seg_12092" s="T93">I have a relative there.</ta>
            <ta e="T102" id="Seg_12093" s="T97">I would have given you grass [=medicine].</ta>
            <ta e="T105" id="Seg_12094" s="T102">His heart is aching.</ta>
            <ta e="T110" id="Seg_12095" s="T105">He would have drunk [it].</ta>
            <ta e="T114" id="Seg_12096" s="T110">You would see him/her.</ta>
            <ta e="T115" id="Seg_12097" s="T114">Enough!</ta>
            <ta e="T119" id="Seg_12098" s="T116">Sit down to eat potato.</ta>
            <ta e="T122" id="Seg_12099" s="T119">But which potato?</ta>
            <ta e="T125" id="Seg_12100" s="T122">I've grated [it].</ta>
            <ta e="T128" id="Seg_12101" s="T125">Then [I] took some flour.</ta>
            <ta e="T135" id="Seg_12102" s="T128">Then I added sugar and I added berry/ies.</ta>
            <ta e="T139" id="Seg_12103" s="T135">I poured warm water.</ta>
            <ta e="T141" id="Seg_12104" s="T139">Then I cooked.</ta>
            <ta e="T143" id="Seg_12105" s="T141">Sit, eat!</ta>
            <ta e="T146" id="Seg_12106" s="T144">The boy is not here.</ta>
            <ta e="T148" id="Seg_12107" s="T146">He went somewhere.</ta>
            <ta e="T151" id="Seg_12108" s="T149">We should look [for him].</ta>
            <ta e="T154" id="Seg_12109" s="T151">Maybe someone killed [him]?</ta>
            <ta e="T157" id="Seg_12110" s="T154">Maybe a bear ate.</ta>
            <ta e="T160" id="Seg_12111" s="T157">I pity him so much.</ta>
            <ta e="T161" id="Seg_12112" s="T160">Enough.</ta>
            <ta e="T170" id="Seg_12113" s="T162">One, two, three, four, five, seven, eight, ten.</ta>
            <ta e="T172" id="Seg_12114" s="T170">I floundered</ta>
            <ta e="T181" id="Seg_12115" s="T173">If people had own horses, they could plough.</ta>
            <ta e="T184" id="Seg_12116" s="T181">And eat potatoes.</ta>
            <ta e="T188" id="Seg_12117" s="T184">[Now] people have no own horses.</ta>
            <ta e="T189" id="Seg_12118" s="T188">That's all.</ta>
            <ta e="T194" id="Seg_12119" s="T190">My daughter-in-law is drunk.</ta>
            <ta e="T196" id="Seg_12120" s="T194">She drank vodka.</ta>
            <ta e="T201" id="Seg_12121" s="T196">She came, she quarelled with her husband.</ta>
            <ta e="T203" id="Seg_12122" s="T201">They fought.</ta>
            <ta e="T206" id="Seg_12123" s="T203">She is very angry.</ta>
            <ta e="T209" id="Seg_12124" s="T206">He was swearing [in] all kinds [of words].</ta>
            <ta e="T212" id="Seg_12125" s="T209">As she likes. [?]</ta>
            <ta e="T216" id="Seg_12126" s="T212">Again s/he left to drink vodka.</ta>
            <ta e="T219" id="Seg_12127" s="T216">Because it was not enough for her.</ta>
            <ta e="T220" id="Seg_12128" s="T219">Enough!</ta>
            <ta e="T224" id="Seg_12129" s="T221">You were in the taiga.</ta>
            <ta e="T226" id="Seg_12130" s="T224">Where did you sleep?</ta>
            <ta e="T228" id="Seg_12131" s="T226">We slept in a tent.</ta>
            <ta e="T230" id="Seg_12132" s="T228">A bear came.</ta>
            <ta e="T233" id="Seg_12133" s="T230">He went [around] there.</ta>
            <ta e="T237" id="Seg_12134" s="T233">We were very scared.</ta>
            <ta e="T241" id="Seg_12135" s="T237">It is also afraid [of] the gun. [?]</ta>
            <ta e="T248" id="Seg_12136" s="T241">One should ask the host's permission to lie down, to sleep.</ta>
            <ta e="T249" id="Seg_12137" s="T248">Enough!</ta>
            <ta e="T254" id="Seg_12138" s="T250">A hen asks a harrier to marry (her).</ta>
            <ta e="T256" id="Seg_12139" s="T254">It's cackling.</ta>
            <ta e="T258" id="Seg_12140" s="T256">She is beating its wings.</ta>
            <ta e="T260" id="Seg_12141" s="T258">[?]</ta>
            <ta e="T262" id="Seg_12142" s="T260">That's all. [?]</ta>
            <ta e="T266" id="Seg_12143" s="T263">What are you scared of?</ta>
            <ta e="T268" id="Seg_12144" s="T266">Go alone!</ta>
            <ta e="T275" id="Seg_12145" s="T268">I went alone, I'm not afraid of anybody.</ta>
            <ta e="T279" id="Seg_12146" s="T275">But you are always scared.</ta>
            <ta e="T280" id="Seg_12147" s="T279">I'm forgetting.</ta>
            <ta e="T281" id="Seg_12148" s="T280">Enough!</ta>
            <ta e="T286" id="Seg_12149" s="T282">They don't know how to speak.</ta>
            <ta e="T290" id="Seg_12150" s="T286">As to me, I speak.</ta>
            <ta e="T292" id="Seg_12151" s="T290">I asked my mother.</ta>
            <ta e="T295" id="Seg_12152" s="T292">I want to speak!</ta>
            <ta e="T296" id="Seg_12153" s="T295">That's all.</ta>
            <ta e="T302" id="Seg_12154" s="T297">Here are no settlers.</ta>
            <ta e="T307" id="Seg_12155" s="T302">Only the Kamas live.</ta>
            <ta e="T311" id="Seg_12156" s="T307">They are afraid of the settlers.</ta>
            <ta e="T315" id="Seg_12157" s="T311">I won't speak [with them?].</ta>
            <ta e="T318" id="Seg_12158" s="T315">Now I'll bring a bag.</ta>
            <ta e="T320" id="Seg_12159" s="T318">You'll hold it.</ta>
            <ta e="T323" id="Seg_12160" s="T320">I [will] pour potates [in].</ta>
            <ta e="T324" id="Seg_12161" s="T323">Sit [=remain sitting]!</ta>
            <ta e="T329" id="Seg_12162" s="T324">I will bring two more pans.</ta>
            <ta e="T330" id="Seg_12163" s="T329">I will cook [it].</ta>
            <ta e="T333" id="Seg_12164" s="T330">Then [we] can talk.</ta>
            <ta e="T336" id="Seg_12165" s="T333">That's all, I don't speak.</ta>
            <ta e="T341" id="Seg_12166" s="T337">Today is a very hot day.</ta>
            <ta e="T344" id="Seg_12167" s="T341">The grain will burn.</ta>
            <ta e="T348" id="Seg_12168" s="T344">Grain will not grow.</ta>
            <ta e="T351" id="Seg_12169" s="T348">Then there [will be] no grain.</ta>
            <ta e="T353" id="Seg_12170" s="T351">What to eat?</ta>
            <ta e="T359" id="Seg_12171" s="T353">I died, one can die, [when] there is no bread.</ta>
            <ta e="T363" id="Seg_12172" s="T359">Enough, I won't speak [anymore].</ta>
            <ta e="T367" id="Seg_12173" s="T364">You are very dirty.</ta>
            <ta e="T368" id="Seg_12174" s="T367">Wash!</ta>
            <ta e="T370" id="Seg_12175" s="T368">Go to the sauna!</ta>
            <ta e="T372" id="Seg_12176" s="T370">Take the soap!</ta>
            <ta e="T375" id="Seg_12177" s="T372">Then take a shirt!</ta>
            <ta e="T376" id="Seg_12178" s="T375">Put it on!</ta>
            <ta e="T377" id="Seg_12179" s="T376">Enough!</ta>
            <ta e="T382" id="Seg_12180" s="T378">Then you will become very beautiful.</ta>
            <ta e="T385" id="Seg_12181" s="T382">When you wear the shirt.</ta>
            <ta e="T392" id="Seg_12182" s="T385">All the girls will (kiss?) you.</ta>
            <ta e="T393" id="Seg_12183" s="T392">Beautiful.</ta>
            <ta e="T394" id="Seg_12184" s="T393">Enough!</ta>
            <ta e="T398" id="Seg_12185" s="T395">They went to Krasnoyarsk.</ta>
            <ta e="T401" id="Seg_12186" s="T398">They do not tell me.</ta>
            <ta e="T406" id="Seg_12187" s="T401">I have a relative there.</ta>
            <ta e="T410" id="Seg_12188" s="T406">I would have given a herb.</ta>
            <ta e="T414" id="Seg_12189" s="T410">His heart is hurting.</ta>
            <ta e="T420" id="Seg_12190" s="T414">They would meet him.</ta>
            <ta e="T424" id="Seg_12191" s="T420">That's all, I will not talk anymore.</ta>
            <ta e="T428" id="Seg_12192" s="T425">A black woman came.</ta>
            <ta e="T433" id="Seg_12193" s="T428">The white man says: I should take [her to wife].</ta>
            <ta e="T435" id="Seg_12194" s="T433">Let us go together.</ta>
            <ta e="T440" id="Seg_12195" s="T435">Let’s sleep on the (road?).</ta>
            <ta e="T442" id="Seg_12196" s="T440">Let’s kiss.</ta>
            <ta e="T446" id="Seg_12197" s="T442">Your man will hit you.</ta>
            <ta e="T447" id="Seg_12198" s="T446">Enough.</ta>
            <ta e="T450" id="Seg_12199" s="T447">I will not speak anymore.</ta>
            <ta e="T456" id="Seg_12200" s="T451">I should wash my child.</ta>
            <ta e="T459" id="Seg_12201" s="T456">He is very dirty.</ta>
            <ta e="T462" id="Seg_12202" s="T459">He pooped much.</ta>
            <ta e="T465" id="Seg_12203" s="T462">I rubbed [him].</ta>
            <ta e="T467" id="Seg_12204" s="T465">He got dry.</ta>
            <ta e="T471" id="Seg_12205" s="T467">Then I should comb his hair.</ta>
            <ta e="T476" id="Seg_12206" s="T471">Then we/he should eat.</ta>
            <ta e="T479" id="Seg_12207" s="T476">Then [he should] sleep.</ta>
            <ta e="T481" id="Seg_12208" s="T479">Rock the baby.</ta>
            <ta e="T488" id="Seg_12209" s="T485">Today is a holiday.</ta>
            <ta e="T492" id="Seg_12210" s="T488">But people are still working.</ta>
            <ta e="T494" id="Seg_12211" s="T492">They are planting potatoes.</ta>
            <ta e="T495" id="Seg_12212" s="T494">They are ploughing.</ta>
            <ta e="T498" id="Seg_12213" s="T495">But we are sitting.</ta>
            <ta e="T504" id="Seg_12214" s="T498">Enough, I don’t want to speak more.</ta>
            <ta e="T508" id="Seg_12215" s="T505">Today is a holiday.</ta>
            <ta e="T511" id="Seg_12216" s="T508">My relative invited me.</ta>
            <ta e="T512" id="Seg_12217" s="T511">Let’s go!</ta>
            <ta e="T515" id="Seg_12218" s="T512">We’ll drink vodka there!</ta>
            <ta e="T518" id="Seg_12219" s="T515">They are playing the accordeon there.</ta>
            <ta e="T522" id="Seg_12220" s="T518">We dance.</ta>
            <ta e="T523" id="Seg_12221" s="T522">That's all!</ta>
            <ta e="T531" id="Seg_12222" s="T524">One, one, two, three, four, five, seven.</ta>
            <ta e="T533" id="Seg_12223" s="T531">Nine, eight.</ta>
            <ta e="T541" id="Seg_12224" s="T540">Ten.</ta>
            <ta e="T550" id="Seg_12225" s="T542">Eleven, twelve, fifteen, sixteen.</ta>
            <ta e="T559" id="Seg_12226" s="T551">I came alone, and they came in five.</ta>
            <ta e="T564" id="Seg_12227" s="T560">I'm cooking soup.</ta>
            <ta e="T567" id="Seg_12228" s="T564">Very good soup.</ta>
            <ta e="T569" id="Seg_12229" s="T567">Tasty, with meat.</ta>
            <ta e="T572" id="Seg_12230" s="T569">I put onion.</ta>
            <ta e="T574" id="Seg_12231" s="T572">I put salt.</ta>
            <ta e="T576" id="Seg_12232" s="T574">I (put?) potato.</ta>
            <ta e="T578" id="Seg_12233" s="T576">Sit, eat!</ta>
            <ta e="T579" id="Seg_12234" s="T578">Together.</ta>
            <ta e="T580" id="Seg_12235" s="T579">Enough!</ta>
            <ta e="T584" id="Seg_12236" s="T581">Very strong man.</ta>
            <ta e="T587" id="Seg_12237" s="T584">I am afraid of him.</ta>
            <ta e="T590" id="Seg_12238" s="T587">He will kill.</ta>
            <ta e="T597" id="Seg_12239" s="T590">I hid so that he would not see me.</ta>
            <ta e="T600" id="Seg_12240" s="T597">I will run to taiga.</ta>
            <ta e="T606" id="Seg_12241" s="T600">He won't find me there.</ta>
            <ta e="T607" id="Seg_12242" s="T606">Enough!</ta>
            <ta e="T611" id="Seg_12243" s="T608">Many men gathered.</ta>
            <ta e="T620" id="Seg_12244" s="T611">They are speaking about something, [I] should go and listen what they are speaking about.</ta>
            <ta e="T625" id="Seg_12245" s="T620">Then I will know.</ta>
            <ta e="T629" id="Seg_12246" s="T625">That's all, I will not speak anymore.</ta>
            <ta e="T638" id="Seg_12247" s="T634">This man is drunk.</ta>
            <ta e="T640" id="Seg_12248" s="T638">He sat on the horse.</ta>
            <ta e="T643" id="Seg_12249" s="T640">Then he fell on the ground.</ta>
            <ta e="T646" id="Seg_12250" s="T643">Then his horse ran [away].</ta>
            <ta e="T651" id="Seg_12251" s="T646">He lost his hat.</ta>
            <ta e="T653" id="Seg_12252" s="T651">He stayed [behind].</ta>
            <ta e="T657" id="Seg_12253" s="T653">He's laying and sleeping, he fell down.</ta>
            <ta e="T659" id="Seg_12254" s="T657">That's all, enough!</ta>
            <ta e="T667" id="Seg_12255" s="T660">Many boys and girls gathered, they are singing.</ta>
            <ta e="T670" id="Seg_12256" s="T667">They are playing the accordeon, they are dancing.</ta>
            <ta e="T671" id="Seg_12257" s="T670">Enough!</ta>
            <ta e="T675" id="Seg_12258" s="T672">I went to the taiga.</ta>
            <ta e="T677" id="Seg_12259" s="T675">I picked berries.</ta>
            <ta e="T679" id="Seg_12260" s="T677">I picked bear leek.</ta>
            <ta e="T683" id="Seg_12261" s="T679">I baked pies, I will give you.</ta>
            <ta e="T684" id="Seg_12262" s="T683">Enough!</ta>
            <ta e="T692" id="Seg_12263" s="T685">My mother was Kamas, my father was a settler.</ta>
            <ta e="T695" id="Seg_12264" s="T692">[They] had two sons.</ta>
            <ta e="T701" id="Seg_12265" s="T698">[They] had six daughters.</ta>
            <ta e="T703" id="Seg_12266" s="T701">That's all, enough.</ta>
            <ta e="T710" id="Seg_12267" s="T704">Three died, but three did not die.</ta>
            <ta e="T714" id="Seg_12268" s="T710">My father’s, sables…</ta>
            <ta e="T722" id="Seg_12269" s="T719">He killed fifteen.</ta>
            <ta e="T724" id="Seg_12270" s="T722">That's all.</ta>
            <ta e="T725" id="Seg_12271" s="T724">Enough!</ta>
            <ta e="T729" id="Seg_12272" s="T725">And he brought a bag of squirrels.</ta>
            <ta e="T730" id="Seg_12273" s="T729">Enough!</ta>
            <ta e="T733" id="Seg_12274" s="T730">A Kamassian man is sick.</ta>
            <ta e="T742" id="Seg_12275" s="T734">He went to a shaman, [the shaman] was healing him.</ta>
            <ta e="T745" id="Seg_12276" s="T742">I think that's all.</ta>
            <ta e="T748" id="Seg_12277" s="T745">I am very hungry.</ta>
            <ta e="T754" id="Seg_12278" s="T748">Sit, eat, anybody won't mind.</ta>
            <ta e="T760" id="Seg_12279" s="T754">Then lie down and sleep!</ta>
            <ta e="T761" id="Seg_12280" s="T760">Enough.</ta>
            <ta e="T767" id="Seg_12281" s="T762">The children are playing outside.</ta>
            <ta e="T769" id="Seg_12282" s="T767">They are shouting, fighting.</ta>
            <ta e="T771" id="Seg_12283" s="T769">Why do you fight?</ta>
            <ta e="T774" id="Seg_12284" s="T771">You must play nicely.</ta>
            <ta e="T776" id="Seg_12285" s="T774">Don’t fight!</ta>
            <ta e="T778" id="Seg_12286" s="T776">That should be enough.</ta>
            <ta e="T781" id="Seg_12287" s="T779">Don’t fight!</ta>
            <ta e="T784" id="Seg_12288" s="T781">You need to play nicely.</ta>
            <ta e="T786" id="Seg_12289" s="T784">Why are you fighting?</ta>
            <ta e="T790" id="Seg_12290" s="T787">I got up today.</ta>
            <ta e="T796" id="Seg_12291" s="T790">Sun had not risen yet when I got up.</ta>
            <ta e="T799" id="Seg_12292" s="T796">Then I milked the cow.</ta>
            <ta e="T801" id="Seg_12293" s="T799">I gave the calf to drink.</ta>
            <ta e="T806" id="Seg_12294" s="T801">I am feeding [= fed] the hens.</ta>
            <ta e="T809" id="Seg_12295" s="T806">Then I (boiled?) eggs.</ta>
            <ta e="T811" id="Seg_12296" s="T809">I boiled potatoes.</ta>
            <ta e="T812" id="Seg_12297" s="T811">I ate.</ta>
            <ta e="T819" id="Seg_12298" s="T812">Then Kamas people came, I spoke.</ta>
            <ta e="T822" id="Seg_12299" s="T819">I got tired.</ta>
            <ta e="T823" id="Seg_12300" s="T822">Enough!</ta>
            <ta e="T826" id="Seg_12301" s="T824">Go home!</ta>
            <ta e="T828" id="Seg_12302" s="T826">Enough of talking.</ta>
            <ta e="T829" id="Seg_12303" s="T828">Eat!</ta>
            <ta e="T831" id="Seg_12304" s="T829">Lie down to sleep!</ta>
            <ta e="T833" id="Seg_12305" s="T831">Enough, [no?] more.</ta>
            <ta e="T836" id="Seg_12306" s="T834">I went to the other end of the village.</ta>
            <ta e="T840" id="Seg_12307" s="T836">I saw very many people.</ta>
            <ta e="T843" id="Seg_12308" s="T840">The chief came.</ta>
            <ta e="T847" id="Seg_12309" s="T843">There are many men, horses.</ta>
            <ta e="T850" id="Seg_12310" s="T847">Girls, boys.</ta>
            <ta e="T851" id="Seg_12311" s="T850">Enough!</ta>
            <ta e="T861" id="Seg_12312" s="T852">Why didn’t this skinny man come, the girl did not come?</ta>
            <ta e="T863" id="Seg_12313" s="T861">Where are they?</ta>
            <ta e="T868" id="Seg_12314" s="T863">Why didn’t they come to talk to me?</ta>
            <ta e="T869" id="Seg_12315" s="T868">Enough!</ta>
            <ta e="T872" id="Seg_12316" s="T870">Where are they?</ta>
            <ta e="T876" id="Seg_12317" s="T872">Why didn’t they come to speak?</ta>
            <ta e="T880" id="Seg_12318" s="T877">Enough of speaking [for] today!</ta>
            <ta e="T882" id="Seg_12319" s="T880">Go home!</ta>
            <ta e="T890" id="Seg_12320" s="T882">You don't … me like this.</ta>
            <ta e="T896" id="Seg_12321" s="T890">You won't teach me like this!</ta>
            <ta e="T900" id="Seg_12322" s="T896">It is not good to say so.</ta>
            <ta e="T903" id="Seg_12323" s="T900">Why to drive people [away]?</ta>
            <ta e="T907" id="Seg_12324" s="T903">They are Kamas people.</ta>
            <ta e="T908" id="Seg_12325" s="T907">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T19" id="Seg_12326" s="T13">(…) jetzt müssen [wir] sprechen.</ta>
            <ta e="T22" id="Seg_12327" s="T19">Dann gehe ich auf den Acker.</ta>
            <ta e="T24" id="Seg_12328" s="T22">Um den Boden zu pflügen.</ta>
            <ta e="T27" id="Seg_12329" s="T24">Um dann Getreide zu säen.</ta>
            <ta e="T32" id="Seg_12330" s="T27">Dann wird es wachsen, es wird groß werden.</ta>
            <ta e="T34" id="Seg_12331" s="T32">Es wird weiß werden.</ta>
            <ta e="T37" id="Seg_12332" s="T34">Dann muss [es] reif werden.</ta>
            <ta e="T42" id="Seg_12333" s="T39">Dann muss man es dreschen.</ta>
            <ta e="T45" id="Seg_12334" s="T42">Dann in einen Sack tun.</ta>
            <ta e="T48" id="Seg_12335" s="T45">Dann muss man [es] mahlen.</ta>
            <ta e="T51" id="Seg_12336" s="T48">Dann muss man [es] sieben.</ta>
            <ta e="T54" id="Seg_12337" s="T51">Dann muss man [Teig] machen.</ta>
            <ta e="T57" id="Seg_12338" s="T54">Dann muss man [es] backen.</ta>
            <ta e="T60" id="Seg_12339" s="T57">Dann muss man [es] essen.</ta>
            <ta e="T61" id="Seg_12340" s="T60">Das ist alles!</ta>
            <ta e="T65" id="Seg_12341" s="T62">Du gehst.</ta>
            <ta e="T69" id="Seg_12342" s="T65">Warum hast du es mir nicht erzählt?</ta>
            <ta e="T74" id="Seg_12343" s="T69">Ich hätte dir Gras gegeben.</ta>
            <ta e="T78" id="Seg_12344" s="T74">Ich habe dort Verwandte.</ta>
            <ta e="T82" id="Seg_12345" s="T78">Du hättest ihn [=sie] getroffen.</ta>
            <ta e="T83" id="Seg_12346" s="T82">Genug!</ta>
            <ta e="T93" id="Seg_12347" s="T84">Warum hast du mit nicht erzählt, dass du [nach] Krasnojarsk gehst?</ta>
            <ta e="T97" id="Seg_12348" s="T93">Ich habe dort einen Verwandten.</ta>
            <ta e="T102" id="Seg_12349" s="T97">Ich hätte dir Gras [=Medizin] gegeben.</ta>
            <ta e="T105" id="Seg_12350" s="T102">Sein Herz ist krank.</ta>
            <ta e="T110" id="Seg_12351" s="T105">Er hätte [sie] getrunken.</ta>
            <ta e="T114" id="Seg_12352" s="T110">Du hättest ihn/sie gesehen.</ta>
            <ta e="T115" id="Seg_12353" s="T114">Genug!</ta>
            <ta e="T119" id="Seg_12354" s="T116">Setz dich, um Kartoffeln zu essen.</ta>
            <ta e="T122" id="Seg_12355" s="T119">Aber welche Kartoffeln?</ta>
            <ta e="T125" id="Seg_12356" s="T122">Weil ich sie gerieben habe.</ta>
            <ta e="T128" id="Seg_12357" s="T125">Dann nahm [ich] Mehl.</ta>
            <ta e="T135" id="Seg_12358" s="T128">Dann gab ich Zucker dazu und gab Beeren dazu.</ta>
            <ta e="T139" id="Seg_12359" s="T135">Ich goss warmes Wasser auf.</ta>
            <ta e="T141" id="Seg_12360" s="T139">Dann kochte ich es.</ta>
            <ta e="T143" id="Seg_12361" s="T141">Setz dich, iss!</ta>
            <ta e="T146" id="Seg_12362" s="T144">Der Junge ist nicht da.</ta>
            <ta e="T148" id="Seg_12363" s="T146">Er ist irgendwohin gegangen.</ta>
            <ta e="T151" id="Seg_12364" s="T149">Wir sollten [ihn] suchen.</ta>
            <ta e="T154" id="Seg_12365" s="T151">Vielleicht hat [ihn] jemand umgebracht?</ta>
            <ta e="T157" id="Seg_12366" s="T154">Vielleicht hat [ihn] ein Bär gefressen.</ta>
            <ta e="T160" id="Seg_12367" s="T157">Ich traere um ihn so sehr.</ta>
            <ta e="T161" id="Seg_12368" s="T160">Genug.</ta>
            <ta e="T170" id="Seg_12369" s="T162">Eins, zwei, drei, vier, fünf, sieben, acht, zehn.</ta>
            <ta e="T172" id="Seg_12370" s="T170">Ich bin durcheinander gekommen.</ta>
            <ta e="T181" id="Seg_12371" s="T173">Wenn die Leute eigene Pferde hätten, dann könnten sie pflügen.</ta>
            <ta e="T184" id="Seg_12372" s="T181">Und Kartoffeln essen.</ta>
            <ta e="T188" id="Seg_12373" s="T184">Aber die Leute haben keine eigenen Pferde.</ta>
            <ta e="T189" id="Seg_12374" s="T188">Das war's!</ta>
            <ta e="T194" id="Seg_12375" s="T190">Meine Schwiegertochter ist betrunken.</ta>
            <ta e="T196" id="Seg_12376" s="T194">Sie hat Wodka getrunken.</ta>
            <ta e="T201" id="Seg_12377" s="T196">Sie kam, sie stritt mit ihrem Mann.</ta>
            <ta e="T203" id="Seg_12378" s="T201">Sie kämpften.</ta>
            <ta e="T206" id="Seg_12379" s="T203">Sie ist sehr böse.</ta>
            <ta e="T209" id="Seg_12380" s="T206">Er beschimpfte sie [mit] allem Möglichen.</ta>
            <ta e="T212" id="Seg_12381" s="T209">Wie sie es braucht. [?]</ta>
            <ta e="T216" id="Seg_12382" s="T212">Wieder ging sie, um Wodka zu trinken.</ta>
            <ta e="T219" id="Seg_12383" s="T216">Weil es ihr zu wenig war.</ta>
            <ta e="T220" id="Seg_12384" s="T219">Genug!</ta>
            <ta e="T224" id="Seg_12385" s="T221">Du warst in der Taiga.</ta>
            <ta e="T226" id="Seg_12386" s="T224">Wo hast du geschlafen?</ta>
            <ta e="T228" id="Seg_12387" s="T226">Wir haben in einem Zelt geschlafen.</ta>
            <ta e="T230" id="Seg_12388" s="T228">Ein Bär kam.</ta>
            <ta e="T233" id="Seg_12389" s="T230">Er ging dort [umher].</ta>
            <ta e="T237" id="Seg_12390" s="T233">Wir hatten sehr viel Angst.</ta>
            <ta e="T241" id="Seg_12391" s="T237">Er hat auch Angst [vor] dem Gewehr. [?]</ta>
            <ta e="T248" id="Seg_12392" s="T241">Man muss den Hausherren um Erlaubnis fragen, sich hinzulegen, zu schlafen.</ta>
            <ta e="T249" id="Seg_12393" s="T248">Genug!</ta>
            <ta e="T254" id="Seg_12394" s="T250">Ein Huhn fragt eine Weihe, es zu heiraten.</ta>
            <ta e="T256" id="Seg_12395" s="T254">Es gackert.</ta>
            <ta e="T258" id="Seg_12396" s="T256">Es schlägt mit den Flügeln.</ta>
            <ta e="T260" id="Seg_12397" s="T258">[?]</ta>
            <ta e="T262" id="Seg_12398" s="T260">Das ist alles. [?]</ta>
            <ta e="T266" id="Seg_12399" s="T263">Wovor hast du Angst?</ta>
            <ta e="T268" id="Seg_12400" s="T266">Geh alleine!</ta>
            <ta e="T275" id="Seg_12401" s="T268">Ich bin alleine gegangen, ich habe vor niemandem Angst.</ta>
            <ta e="T279" id="Seg_12402" s="T275">Aber du hast immer Angst.</ta>
            <ta e="T280" id="Seg_12403" s="T279">Ich vergesse.</ta>
            <ta e="T281" id="Seg_12404" s="T280">Genug!</ta>
            <ta e="T286" id="Seg_12405" s="T282">Sie wissen nicht, wie man spricht.</ta>
            <ta e="T290" id="Seg_12406" s="T286">Ich aber spreche.</ta>
            <ta e="T292" id="Seg_12407" s="T290">Ich fragte meine Mutter.</ta>
            <ta e="T295" id="Seg_12408" s="T292">Ich möchte sprachen.</ta>
            <ta e="T296" id="Seg_12409" s="T295">Das war's.</ta>
            <ta e="T302" id="Seg_12410" s="T297">Hier gibt es keine Siedlers.</ta>
            <ta e="T307" id="Seg_12411" s="T302">Nur Kamassen leben [hier].</ta>
            <ta e="T311" id="Seg_12412" s="T307">Sie haben Angst vor den Siedlern.</ta>
            <ta e="T315" id="Seg_12413" s="T311">Ich werde nicht [mit ihnen?] sprechen.</ta>
            <ta e="T318" id="Seg_12414" s="T315">Jetzt werde ich einen Sack bringen.</ta>
            <ta e="T320" id="Seg_12415" s="T318">Du wirst ihn halten.</ta>
            <ta e="T323" id="Seg_12416" s="T320">Ich lege Kartoffeln hinein.</ta>
            <ta e="T324" id="Seg_12417" s="T323">Sitzt!</ta>
            <ta e="T329" id="Seg_12418" s="T324">Ich bringe noch zwei Pfannen mehr.</ta>
            <ta e="T330" id="Seg_12419" s="T329">Ich werde [sie] kochen.</ta>
            <ta e="T333" id="Seg_12420" s="T330">Dann können [wir] reden.</ta>
            <ta e="T336" id="Seg_12421" s="T333">Das ist alles, ich rede nicht.</ta>
            <ta e="T341" id="Seg_12422" s="T337">Heute ist ein sehr heißer Tag.</ta>
            <ta e="T344" id="Seg_12423" s="T341">Das Getreide wird verbrennen.</ta>
            <ta e="T348" id="Seg_12424" s="T344">Das Getreide wird nicht wachsen.</ta>
            <ta e="T351" id="Seg_12425" s="T348">Dann [wird] es kein Korn geben.</ta>
            <ta e="T353" id="Seg_12426" s="T351">Was soll man essen?</ta>
            <ta e="T359" id="Seg_12427" s="T353">Ich starb, man kann sterben, [wenn] es kein Korn gibt.</ta>
            <ta e="T363" id="Seg_12428" s="T359">Genug, ich werde nicht [mehr] sprechen.</ta>
            <ta e="T367" id="Seg_12429" s="T364">Du bist sehr schmutzig.</ta>
            <ta e="T368" id="Seg_12430" s="T367">Wasch dich!</ta>
            <ta e="T370" id="Seg_12431" s="T368">Geh ins Badehaus!</ta>
            <ta e="T372" id="Seg_12432" s="T370">Nimm Seife!</ta>
            <ta e="T375" id="Seg_12433" s="T372">Dann nimm ein Hemd!</ta>
            <ta e="T376" id="Seg_12434" s="T375">Zieh es an!</ta>
            <ta e="T377" id="Seg_12435" s="T376">Genug!</ta>
            <ta e="T382" id="Seg_12436" s="T378">Dann wirst du sehr schön werden.</ta>
            <ta e="T385" id="Seg_12437" s="T382">Wenn du das Hemd trägst.</ta>
            <ta e="T392" id="Seg_12438" s="T385">Alle Mädchen werden dich (küssen?).</ta>
            <ta e="T393" id="Seg_12439" s="T392">Schön.</ta>
            <ta e="T394" id="Seg_12440" s="T393">Genug!</ta>
            <ta e="T398" id="Seg_12441" s="T395">Sie fuhren nach Krasnojarsk.</ta>
            <ta e="T401" id="Seg_12442" s="T398">Sie haben es mir nicht erzählt.</ta>
            <ta e="T406" id="Seg_12443" s="T401">Ich habe einen Verwandten dort.</ta>
            <ta e="T410" id="Seg_12444" s="T406">Ich hätte ein Kraut mitgegeben.</ta>
            <ta e="T414" id="Seg_12445" s="T410">Sein Herz ist krank.</ta>
            <ta e="T420" id="Seg_12446" s="T414">Sie hätten ihn getroffen.</ta>
            <ta e="T424" id="Seg_12447" s="T420">Das war's, ich werde nicht mehr reden.</ta>
            <ta e="T428" id="Seg_12448" s="T425">Eine schwarze Frau kam.</ta>
            <ta e="T433" id="Seg_12449" s="T428">Der weiße Mann sagt: Ich sollte sie [zur Frau] nehmen.</ta>
            <ta e="T435" id="Seg_12450" s="T433">Lass uns zusammen gehen.</ta>
            <ta e="T440" id="Seg_12451" s="T435">Lass uns auf dem (Weg?) schlafen.</ta>
            <ta e="T442" id="Seg_12452" s="T440">Lass uns küssen.</ta>
            <ta e="T446" id="Seg_12453" s="T442">Dein Mann wird dich schlagen.</ta>
            <ta e="T447" id="Seg_12454" s="T446">Genug.</ta>
            <ta e="T450" id="Seg_12455" s="T447">Ich werde nicht mehr sprechen.</ta>
            <ta e="T456" id="Seg_12456" s="T451">Ich sollte mein Kind waschen.</ta>
            <ta e="T459" id="Seg_12457" s="T456">Es ist sehr schmutzig.</ta>
            <ta e="T462" id="Seg_12458" s="T459">Es hat viel gekackt.</ta>
            <ta e="T465" id="Seg_12459" s="T462">Ich rieb es ab.</ta>
            <ta e="T467" id="Seg_12460" s="T465">Es wurde trocken.</ta>
            <ta e="T471" id="Seg_12461" s="T467">Dann sollte ich sein Haar kämmen.</ta>
            <ta e="T476" id="Seg_12462" s="T471">Dann sollte(n) es/wir essen.</ta>
            <ta e="T479" id="Seg_12463" s="T476">Dann [sollte es] schlafen.</ta>
            <ta e="T481" id="Seg_12464" s="T479">Wiege mein Kind.</ta>
            <ta e="T488" id="Seg_12465" s="T485">Heute ist ein Feiertag.</ta>
            <ta e="T492" id="Seg_12466" s="T488">Aber die Leute arbeiten die ganze Zeit.</ta>
            <ta e="T494" id="Seg_12467" s="T492">Sie pflanzen Kartoffeln.</ta>
            <ta e="T495" id="Seg_12468" s="T494">Sie pflügen.</ta>
            <ta e="T498" id="Seg_12469" s="T495">Aber wir sitzen.</ta>
            <ta e="T504" id="Seg_12470" s="T498">Genug, ich möchte nicht mehr sprechen.</ta>
            <ta e="T508" id="Seg_12471" s="T505">Heute ist ein Feiertag.</ta>
            <ta e="T511" id="Seg_12472" s="T508">Ein Verwandter von mir hat mich eingeladen.</ta>
            <ta e="T512" id="Seg_12473" s="T511">Lass uns gehen!</ta>
            <ta e="T515" id="Seg_12474" s="T512">Wir werden dort Wodka trinken!</ta>
            <ta e="T518" id="Seg_12475" s="T515">Sie spielen dort Akkordeon.</ta>
            <ta e="T522" id="Seg_12476" s="T518">Wir tanzen.</ta>
            <ta e="T523" id="Seg_12477" s="T522">Das ist alles!</ta>
            <ta e="T531" id="Seg_12478" s="T524">Eins, eins, zwei, drei, vier, fünf, sieben.</ta>
            <ta e="T533" id="Seg_12479" s="T531">Neun, acht.</ta>
            <ta e="T541" id="Seg_12480" s="T540">Zehn.</ta>
            <ta e="T550" id="Seg_12481" s="T542">Elf, zwölf, fünfzehn, sechzehn.</ta>
            <ta e="T559" id="Seg_12482" s="T551">Ich kam alleine und sie kamen zu fünft.</ta>
            <ta e="T564" id="Seg_12483" s="T560">Ich koche Suppe.</ta>
            <ta e="T567" id="Seg_12484" s="T564">Sehr gute Suppe.</ta>
            <ta e="T569" id="Seg_12485" s="T567">Lecker, mit Fleisch.</ta>
            <ta e="T572" id="Seg_12486" s="T569">Ich tue Zwiebeln hinein.</ta>
            <ta e="T574" id="Seg_12487" s="T572">Ich tue Salz hinein.</ta>
            <ta e="T576" id="Seg_12488" s="T574">Ich (tue?) Kartoffeln hinein.</ta>
            <ta e="T578" id="Seg_12489" s="T576">Sitz, iss!</ta>
            <ta e="T579" id="Seg_12490" s="T578">Zusammen.</ta>
            <ta e="T580" id="Seg_12491" s="T579">Genug!</ta>
            <ta e="T584" id="Seg_12492" s="T581">Sehr starker Mann.</ta>
            <ta e="T587" id="Seg_12493" s="T584">Ich habe Angst vor ihm.</ta>
            <ta e="T590" id="Seg_12494" s="T587">Er wird töten.</ta>
            <ta e="T597" id="Seg_12495" s="T590">Ich versteckte mich, damit er mich nicht sieht.</ta>
            <ta e="T600" id="Seg_12496" s="T597">Ich werde in die Taiga laufen.</ta>
            <ta e="T606" id="Seg_12497" s="T600">Er wird mich dort nicht finden.</ta>
            <ta e="T607" id="Seg_12498" s="T606">Genug!</ta>
            <ta e="T611" id="Seg_12499" s="T608">Viele Männer haben sich versammelt.</ta>
            <ta e="T620" id="Seg_12500" s="T611">Sie sprechen über irgendetwas, [ich] sollte gehen und hören, worüber sie sprechen.</ta>
            <ta e="T625" id="Seg_12501" s="T620">Dann werde ich es wissen.</ta>
            <ta e="T629" id="Seg_12502" s="T625">Das war's, ich werde nicht mehr sprechen.</ta>
            <ta e="T638" id="Seg_12503" s="T634">Dieser Mann ist betrunken.</ta>
            <ta e="T640" id="Seg_12504" s="T638">Er setzte sich auf das Pferd.</ta>
            <ta e="T643" id="Seg_12505" s="T640">Dann fiel er auf die Erde.</ta>
            <ta e="T646" id="Seg_12506" s="T643">Dann rannte sein Pferd davon.</ta>
            <ta e="T651" id="Seg_12507" s="T646">Er verlor seine Mütze.</ta>
            <ta e="T653" id="Seg_12508" s="T651">Er blieb zurück.</ta>
            <ta e="T657" id="Seg_12509" s="T653">Er liegt und schläft, er fiel hinunter.</ta>
            <ta e="T659" id="Seg_12510" s="T657">Das war's, genug!</ta>
            <ta e="T667" id="Seg_12511" s="T660">Viele Jungs und Mädchen haben sich versammelt, sie singen.</ta>
            <ta e="T670" id="Seg_12512" s="T667">Sie spielen Akkordeon, sie tanzen.</ta>
            <ta e="T671" id="Seg_12513" s="T670">Genug!</ta>
            <ta e="T675" id="Seg_12514" s="T672">Ich ging in die Taiga.</ta>
            <ta e="T677" id="Seg_12515" s="T675">Ich sammelte Beeren.</ta>
            <ta e="T679" id="Seg_12516" s="T677">Ich sammelte Bärlauch.</ta>
            <ta e="T683" id="Seg_12517" s="T679">Ich buk Piroggen, ich werde sie dir geben.</ta>
            <ta e="T684" id="Seg_12518" s="T683">Genug!</ta>
            <ta e="T692" id="Seg_12519" s="T685">Meine Mutter war Kamassin, mein Vater war ein Siedler.</ta>
            <ta e="T695" id="Seg_12520" s="T692">[Sie] hatten zwei Söhne.</ta>
            <ta e="T701" id="Seg_12521" s="T698">[Sie] hatten sechs Töchter.</ta>
            <ta e="T703" id="Seg_12522" s="T701">Das war's, genug.</ta>
            <ta e="T710" id="Seg_12523" s="T704">Drei starben, aber drei starben nicht.</ta>
            <ta e="T714" id="Seg_12524" s="T710">Mein Vater, Zobel…</ta>
            <ta e="T722" id="Seg_12525" s="T719">Er tötete fünfzehn.</ta>
            <ta e="T724" id="Seg_12526" s="T722">Das ist alles.</ta>
            <ta e="T725" id="Seg_12527" s="T724">Genug!</ta>
            <ta e="T729" id="Seg_12528" s="T725">Und ich brachte einen Sack mit Eichhörnchen.</ta>
            <ta e="T730" id="Seg_12529" s="T729">Genug!</ta>
            <ta e="T733" id="Seg_12530" s="T730">Ein Kamasse ist krank.</ta>
            <ta e="T742" id="Seg_12531" s="T734">Er ging zu einem Schamane, [der Schamane] heilte ihn.</ta>
            <ta e="T745" id="Seg_12532" s="T742">Das ist wohl alles.</ta>
            <ta e="T748" id="Seg_12533" s="T745">Ich bin sehr hungrig.</ta>
            <ta e="T754" id="Seg_12534" s="T748">Setz dich, iss, niemanden stört es.</ta>
            <ta e="T760" id="Seg_12535" s="T754">Dann leg dich hin und schlaf!</ta>
            <ta e="T761" id="Seg_12536" s="T760">Genug.</ta>
            <ta e="T767" id="Seg_12537" s="T762">Die Kinder spielen draußen.</ta>
            <ta e="T769" id="Seg_12538" s="T767">Sie schreien und kämpfen.</ta>
            <ta e="T771" id="Seg_12539" s="T769">Warum kämpft ihr?</ta>
            <ta e="T774" id="Seg_12540" s="T771">Ihr sollt friedlich spielen.</ta>
            <ta e="T776" id="Seg_12541" s="T774">Kämpft nicht!</ta>
            <ta e="T778" id="Seg_12542" s="T776">Das reicht wohl.</ta>
            <ta e="T781" id="Seg_12543" s="T779">Kämpft nicht!</ta>
            <ta e="T784" id="Seg_12544" s="T781">Ihr sollt friedlich spielen.</ta>
            <ta e="T786" id="Seg_12545" s="T784">Warum kämpft ihr?</ta>
            <ta e="T790" id="Seg_12546" s="T787">Ich bin heute aufgestanden.</ta>
            <ta e="T796" id="Seg_12547" s="T790">Die Sonne war noch nicht aufgegangen, als ich aufstand.</ta>
            <ta e="T799" id="Seg_12548" s="T796">Dann molk ich die Kuh.</ta>
            <ta e="T801" id="Seg_12549" s="T799">Ich gab dem Kalb zu trinken.</ta>
            <ta e="T806" id="Seg_12550" s="T801">Ich füttere die Hühner.</ta>
            <ta e="T809" id="Seg_12551" s="T806">Dann (kochte?) ich Eier.</ta>
            <ta e="T811" id="Seg_12552" s="T809">Ich kochte Kartoffeln.</ta>
            <ta e="T812" id="Seg_12553" s="T811">Ich aß.</ta>
            <ta e="T819" id="Seg_12554" s="T812">Dann kamen Kamassen, ich sprach.</ta>
            <ta e="T822" id="Seg_12555" s="T819">Ich bin müde.</ta>
            <ta e="T823" id="Seg_12556" s="T822">Genug!</ta>
            <ta e="T826" id="Seg_12557" s="T824">Geht nach Hause!</ta>
            <ta e="T828" id="Seg_12558" s="T826">Genug des Redens.</ta>
            <ta e="T829" id="Seg_12559" s="T828">Esst!</ta>
            <ta e="T831" id="Seg_12560" s="T829">Leg dich schlafen!</ta>
            <ta e="T833" id="Seg_12561" s="T831">Genug, [nichts?] mehr.</ta>
            <ta e="T836" id="Seg_12562" s="T834">Ich ging zum anderen Ende des Dorfes.</ta>
            <ta e="T840" id="Seg_12563" s="T836">Ich sah viele Leute.</ta>
            <ta e="T843" id="Seg_12564" s="T840">Der Dorfälteste kam.</ta>
            <ta e="T847" id="Seg_12565" s="T843">Dort sind viele Männer, Pferde.</ta>
            <ta e="T850" id="Seg_12566" s="T847">Mädchen, Jungen.</ta>
            <ta e="T851" id="Seg_12567" s="T850">Genug!</ta>
            <ta e="T861" id="Seg_12568" s="T852">Warum ist dieser magere Mann nicht gekommen, ist das Mädchen nicht gekommen?</ta>
            <ta e="T863" id="Seg_12569" s="T861">Wo sind sie?</ta>
            <ta e="T868" id="Seg_12570" s="T863">Warum sind sie nicht gekommen, um mit mir zu sprechen?</ta>
            <ta e="T869" id="Seg_12571" s="T868">Genug!</ta>
            <ta e="T872" id="Seg_12572" s="T870">Wo sind sie?</ta>
            <ta e="T876" id="Seg_12573" s="T872">Warum sind sie nicht gekommen, um zu sprechen?</ta>
            <ta e="T880" id="Seg_12574" s="T877">Genug des Redens [für] heute!</ta>
            <ta e="T882" id="Seg_12575" s="T880">Geh nach Hause!</ta>
            <ta e="T890" id="Seg_12576" s="T882">Du mich… so nicht…</ta>
            <ta e="T896" id="Seg_12577" s="T890">(Be)lehre mich nicht so!</ta>
            <ta e="T900" id="Seg_12578" s="T896">Es ist nicht gut so zu sprechen.</ta>
            <ta e="T903" id="Seg_12579" s="T900">Warum Leute rausjagen?</ta>
            <ta e="T907" id="Seg_12580" s="T903">Das sind Kamassen.</ta>
            <ta e="T908" id="Seg_12581" s="T907">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T19" id="Seg_12582" s="T13">[KlT:] d'azir- 'heal'</ta>
            <ta e="T74" id="Seg_12583" s="T69">[KlT:] grass = medicine.</ta>
            <ta e="T78" id="Seg_12584" s="T74">[GVY:] tugarzaŋbə</ta>
            <ta e="T135" id="Seg_12585" s="T128">Check the base form of ’sĭrepne’.</ta>
            <ta e="T157" id="Seg_12586" s="T154">[GVY:] Možetʼ</ta>
            <ta e="T160" id="Seg_12587" s="T157">[KlT:] I am very worried.</ta>
            <ta e="T212" id="Seg_12588" s="T209">[GVY:] "What does she need" ("Что ей надо")?</ta>
            <ta e="T241" id="Seg_12589" s="T237">[GVY] multukdə LAT?</ta>
            <ta e="T254" id="Seg_12590" s="T250">Лунь 'hawk' (?). !!! [GVY:] Russ. dial. лунек, standard Russian лунь 'harrier, hen-harrier'. The form лунька is not attested, but lunʼka may be the Russian ACC form. </ta>
            <ta e="T280" id="Seg_12591" s="T279">[GVY:] = I forgot?</ta>
            <ta e="T311" id="Seg_12592" s="T307">[GVY:] The opposite interpretation 'Settlers are afraid of them' is also possible.</ta>
            <ta e="T323" id="Seg_12593" s="T320">PST instead of FUT?</ta>
            <ta e="T363" id="Seg_12594" s="T359">[GVY:] The interpretation of the form ibə is tentative.</ta>
            <ta e="T392" id="Seg_12595" s="T385">[GVY:] tănarlaʔi? </ta>
            <ta e="T398" id="Seg_12596" s="T395">[GVY:] Krasnojarskəʔi seems to be an error.</ta>
            <ta e="T401" id="Seg_12597" s="T398">[GVY:] nörbəʔi = nörbəbiʔi?</ta>
            <ta e="T440" id="Seg_12598" s="T435">[GVY:] z instead of ž in both cases.</ta>
            <ta e="T456" id="Seg_12599" s="T451">[GVY:] băzəjdəsʼtə?</ta>
            <ta e="T471" id="Seg_12600" s="T467">Kür ’to wrap’.</ta>
            <ta e="T494" id="Seg_12601" s="T492">[GVY:] amnollaʔbəʔjə</ta>
            <ta e="T531" id="Seg_12602" s="T524">[GVY:] sumne</ta>
            <ta e="T550" id="Seg_12603" s="T542">[GVY:] sumne.</ta>
            <ta e="T576" id="Seg_12604" s="T574">[KlT:] ambiem = embiem?</ta>
            <ta e="T579" id="Seg_12605" s="T578">Janurik: oʔbərəj ’together.</ta>
            <ta e="T701" id="Seg_12606" s="T698">[GVY:] kobdaŋ = koʔbsaŋ.</ta>
            <ta e="T714" id="Seg_12607" s="T710">[GVY:] unfinished sentence.</ta>
            <ta e="T790" id="Seg_12608" s="T787">[GVY:] nu- is strange here; normally, PKZ uses the root uʔbdə- in the sense of 'get up'.</ta>
            <ta e="T809" id="Seg_12609" s="T806">[GVY:] or 'fried'?</ta>
            <ta e="T843" id="Seg_12610" s="T840">[GVY:] all the heads came?</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T631" start="T629">
            <tli id="T629.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T634" start="T631">
            <tli id="T631.tx-KA.1" />
            <tli id="T631.tx-KA.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T540" id="Seg_12611" n="sc" s="T533">
               <ts e="T540" id="Seg_12613" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_12615" n="HIAT:w" s="T533">Ну</ts>
                  <nts id="Seg_12616" n="HIAT:ip">,</nts>
                  <nts id="Seg_12617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_12619" n="HIAT:w" s="T534">одиннадцать</ts>
                  <nts id="Seg_12620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_12622" n="HIAT:w" s="T535">можно</ts>
                  <nts id="Seg_12623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_12625" n="HIAT:w" s="T536">там</ts>
                  <nts id="Seg_12626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_12628" n="HIAT:w" s="T537">сразу</ts>
                  <nts id="Seg_12629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_12631" n="HIAT:w" s="T538">там</ts>
                  <nts id="Seg_12632" n="HIAT:ip">…</nts>
                  <nts id="Seg_12633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_12634" n="sc" s="T541">
               <ts e="T542" id="Seg_12636" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_12638" n="HIAT:w" s="T541">Mhmh</ts>
                  <nts id="Seg_12639" n="HIAT:ip">.</nts>
                  <nts id="Seg_12640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_12641" n="sc" s="T629">
               <ts e="T631" id="Seg_12643" n="HIAT:u" s="T629">
                  <ts e="T629.tx-KA.1" id="Seg_12645" n="HIAT:w" s="T629">Может</ts>
                  <nts id="Seg_12646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_12648" n="HIAT:w" s="T629.tx-KA.1">пьяный</ts>
                  <nts id="Seg_12649" n="HIAT:ip">?</nts>
                  <nts id="Seg_12650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_12652" n="HIAT:u" s="T631">
                  <ts e="T631.tx-KA.1" id="Seg_12654" n="HIAT:w" s="T631">На</ts>
                  <nts id="Seg_12655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631.tx-KA.2" id="Seg_12657" n="HIAT:w" s="T631.tx-KA.1">коне</ts>
                  <nts id="Seg_12658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_12660" n="HIAT:w" s="T631.tx-KA.2">ехал</ts>
                  <nts id="Seg_12661" n="HIAT:ip">?</nts>
                  <nts id="Seg_12662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T698" id="Seg_12663" n="sc" s="T695">
               <ts e="T698" id="Seg_12665" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_12667" n="HIAT:w" s="T695">Шесть</ts>
                  <nts id="Seg_12668" n="HIAT:ip">—</nts>
                  <nts id="Seg_12669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_12671" n="HIAT:w" s="T696">muktuʔ</ts>
                  <nts id="Seg_12672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_12673" n="HIAT:ip">(</nts>
                  <nts id="Seg_12674" n="HIAT:ip">(</nts>
                  <ats e="T698" id="Seg_12675" n="HIAT:non-pho" s="T697">…</ats>
                  <nts id="Seg_12676" n="HIAT:ip">)</nts>
                  <nts id="Seg_12677" n="HIAT:ip">)</nts>
                  <nts id="Seg_12678" n="HIAT:ip">.</nts>
                  <nts id="Seg_12679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T719" id="Seg_12680" n="sc" s="T714">
               <ts e="T719" id="Seg_12682" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_12684" n="HIAT:w" s="T714">Ну</ts>
                  <nts id="Seg_12685" n="HIAT:ip">,</nts>
                  <nts id="Seg_12686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_12688" n="HIAT:w" s="T715">убивал</ts>
                  <nts id="Seg_12689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_12691" n="HIAT:w" s="T716">и</ts>
                  <nts id="Seg_12692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_12694" n="HIAT:w" s="T717">добывал</ts>
                  <nts id="Seg_12695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_12697" n="HIAT:w" s="T718">тут</ts>
                  <nts id="Seg_12698" n="HIAT:ip">.</nts>
                  <nts id="Seg_12699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_12700" n="sc" s="T722">
               <ts e="T723" id="Seg_12702" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_12704" n="HIAT:w" s="T722">Aha</ts>
                  <nts id="Seg_12705" n="HIAT:ip">.</nts>
                  <nts id="Seg_12706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T540" id="Seg_12707" n="sc" s="T533">
               <ts e="T534" id="Seg_12709" n="e" s="T533">Ну, </ts>
               <ts e="T535" id="Seg_12711" n="e" s="T534">одиннадцать </ts>
               <ts e="T536" id="Seg_12713" n="e" s="T535">можно </ts>
               <ts e="T537" id="Seg_12715" n="e" s="T536">там </ts>
               <ts e="T538" id="Seg_12717" n="e" s="T537">сразу </ts>
               <ts e="T540" id="Seg_12719" n="e" s="T538">там… </ts>
            </ts>
            <ts e="T542" id="Seg_12720" n="sc" s="T541">
               <ts e="T542" id="Seg_12722" n="e" s="T541">Mhmh. </ts>
            </ts>
            <ts e="T634" id="Seg_12723" n="sc" s="T629">
               <ts e="T631" id="Seg_12725" n="e" s="T629">Может пьяный? </ts>
               <ts e="T634" id="Seg_12727" n="e" s="T631">На коне ехал? </ts>
            </ts>
            <ts e="T698" id="Seg_12728" n="sc" s="T695">
               <ts e="T696" id="Seg_12730" n="e" s="T695">Шесть— </ts>
               <ts e="T697" id="Seg_12732" n="e" s="T696">muktuʔ </ts>
               <ts e="T698" id="Seg_12734" n="e" s="T697">((…)). </ts>
            </ts>
            <ts e="T719" id="Seg_12735" n="sc" s="T714">
               <ts e="T715" id="Seg_12737" n="e" s="T714">Ну, </ts>
               <ts e="T716" id="Seg_12739" n="e" s="T715">убивал </ts>
               <ts e="T717" id="Seg_12741" n="e" s="T716">и </ts>
               <ts e="T718" id="Seg_12743" n="e" s="T717">добывал </ts>
               <ts e="T719" id="Seg_12745" n="e" s="T718">тут. </ts>
            </ts>
            <ts e="T723" id="Seg_12746" n="sc" s="T722">
               <ts e="T723" id="Seg_12748" n="e" s="T722">Aha. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T540" id="Seg_12749" s="T533">PKZ_196X_SU0203.KA.001 (181)</ta>
            <ta e="T542" id="Seg_12750" s="T541">PKZ_196X_SU0203.KA.002 (183)</ta>
            <ta e="T631" id="Seg_12751" s="T629">PKZ_196X_SU0203.KA.003 (210)</ta>
            <ta e="T634" id="Seg_12752" s="T631">PKZ_196X_SU0203.KA.004 (211)</ta>
            <ta e="T698" id="Seg_12753" s="T695">PKZ_196X_SU0203.KA.005 (233)</ta>
            <ta e="T719" id="Seg_12754" s="T714">PKZ_196X_SU0203.KA.006 (239)</ta>
            <ta e="T723" id="Seg_12755" s="T722">PKZ_196X_SU0203.KA.007 (242)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T540" id="Seg_12756" s="T533">Ну, одиннадцать можно там сразу там … </ta>
            <ta e="T542" id="Seg_12757" s="T541">Mhmh. </ta>
            <ta e="T631" id="Seg_12758" s="T629">Может пьяный? </ta>
            <ta e="T634" id="Seg_12759" s="T631">На коне ехал? </ta>
            <ta e="T698" id="Seg_12760" s="T695">Шесть— muktuʔ ((…)). </ta>
            <ta e="T719" id="Seg_12761" s="T714">Ну, убивал и добывал тут. </ta>
            <ta e="T723" id="Seg_12762" s="T722">Aha. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA" />
         <annotation name="fr" tierref="fr-KA" />
         <annotation name="fe" tierref="fe-KA" />
         <annotation name="fg" tierref="fg-KA">
            <ta e="T540" id="Seg_12763" s="T533">Nun, elf kann man gleich dort…</ta>
            <ta e="T542" id="Seg_12764" s="T541">Hmm.</ta>
            <ta e="T631" id="Seg_12765" s="T629">Vielleicht betrunken?</ta>
            <ta e="T634" id="Seg_12766" s="T631">Auf dem Pferd geritten?</ta>
            <ta e="T719" id="Seg_12767" s="T714">Nun, tötete er und gab dorthin.</ta>
            <ta e="T723" id="Seg_12768" s="T722">Aha.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T698" id="Seg_12769" s="T695">[KlT:] it may be another person</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T911" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T912" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T910" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-MAK"
                          name="ref"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-MAK"
                          name="ts"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-MAK"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-MAK"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-MAK"
                          name="CS"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-MAK"
                          name="fr"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-MAK"
                          name="fe"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-MAK"
                          name="fg"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-MAK"
                          name="nt"
                          segmented-tier-id="tx-MAK"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
