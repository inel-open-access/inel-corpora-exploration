<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA6102387-ABE1-DE86-771A-F3B17A047103">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0210.wav" />
         <referenced-file url="PKZ_196X_SU0210.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0210\PKZ_196X_SU0210.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1432</ud-information>
            <ud-information attribute-name="# HIAT:w">963</ud-information>
            <ud-information attribute-name="# e">963</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">208</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.049" type="appl" />
         <tli id="T1" time="1.038" type="appl" />
         <tli id="T2" time="2.673325311338942" />
         <tli id="T3" time="3.437" type="appl" />
         <tli id="T4" time="4.178" type="appl" />
         <tli id="T5" time="4.92" type="appl" />
         <tli id="T6" time="5.661" type="appl" />
         <tli id="T7" time="6.403" type="appl" />
         <tli id="T8" time="7.144" type="appl" />
         <tli id="T9" time="8.319975033793016" />
         <tli id="T10" time="8.968" type="appl" />
         <tli id="T11" time="9.496" type="appl" />
         <tli id="T12" time="10.023" type="appl" />
         <tli id="T13" time="10.473301905519895" />
         <tli id="T14" time="11.338" type="appl" />
         <tli id="T15" time="12.126" type="appl" />
         <tli id="T16" time="12.913" type="appl" />
         <tli id="T17" time="13.701" type="appl" />
         <tli id="T18" time="14.489" type="appl" />
         <tli id="T19" time="15.277" type="appl" />
         <tli id="T20" time="16.064" type="appl" />
         <tli id="T21" time="16.852" type="appl" />
         <tli id="T22" time="17.64" type="appl" />
         <tli id="T23" time="18.428" type="appl" />
         <tli id="T24" time="19.215" type="appl" />
         <tli id="T25" time="20.003" type="appl" />
         <tli id="T26" time="22.86659804960741" />
         <tli id="T27" time="23.487" type="appl" />
         <tli id="T28" time="24.07" type="appl" />
         <tli id="T29" time="24.654" type="appl" />
         <tli id="T30" time="25.238" type="appl" />
         <tli id="T31" time="25.821" type="appl" />
         <tli id="T32" time="26.405" type="appl" />
         <tli id="T33" time="26.989" type="appl" />
         <tli id="T34" time="27.572" type="appl" />
         <tli id="T35" time="28.156" type="appl" />
         <tli id="T36" time="28.74" type="appl" />
         <tli id="T37" time="29.323" type="appl" />
         <tli id="T38" time="31.786571282952806" />
         <tli id="T39" time="32.334" type="appl" />
         <tli id="T40" time="32.978" type="appl" />
         <tli id="T41" time="33.621" type="appl" />
         <tli id="T42" time="34.43323000764498" />
         <tli id="T43" time="35.49" type="appl" />
         <tli id="T44" time="37.4132210654218" />
         <tli id="T45" time="38.068" type="appl" />
         <tli id="T46" time="38.684" type="appl" />
         <tli id="T47" time="39.3" type="appl" />
         <tli id="T48" time="40.28654577661154" />
         <tli id="T49" time="40.894" type="appl" />
         <tli id="T50" time="41.568" type="appl" />
         <tli id="T51" time="44.17986742704032" />
         <tli id="T52" time="45.156" type="appl" />
         <tli id="T53" time="46.21319465885672" />
         <tli id="T54" time="47.327" type="appl" />
         <tli id="T55" time="48.208" type="appl" />
         <tli id="T56" time="49.766517329539155" />
         <tli id="T57" time="50.517" type="appl" />
         <tli id="T58" time="51.339" type="appl" />
         <tli id="T59" time="53.939838139759054" />
         <tli id="T60" time="54.891" type="appl" />
         <tli id="T61" time="55.66" type="appl" />
         <tli id="T62" time="56.429" type="appl" />
         <tli id="T63" time="57.432" type="appl" />
         <tli id="T64" time="58.239" type="appl" />
         <tli id="T65" time="61.239816234313025" />
         <tli id="T66" time="62.313" type="appl" />
         <tli id="T67" time="63.162" type="appl" />
         <tli id="T68" time="64.75313902502529" />
         <tli id="T69" time="65.374" type="appl" />
         <tli id="T70" time="66.108" type="appl" />
         <tli id="T71" time="66.95979907004572" />
         <tli id="T72" time="68.096" type="appl" />
         <tli id="T73" time="69.27979210831492" />
         <tli id="T74" time="70.566" type="appl" />
         <tli id="T75" time="71.732" type="appl" />
         <tli id="T967" time="72.1988" type="intp" />
         <tli id="T76" time="72.899" type="appl" />
         <tli id="T77" time="74.065" type="appl" />
         <tli id="T78" time="76.8731026559834" />
         <tli id="T79" time="77.577" type="appl" />
         <tli id="T80" time="78.175" type="appl" />
         <tli id="T81" time="78.88642994781472" />
         <tli id="T82" time="79.448" type="appl" />
         <tli id="T83" time="79.968" type="appl" />
         <tli id="T84" time="80.489" type="appl" />
         <tli id="T85" time="81.0397568195416" />
         <tli id="T86" time="81.917" type="appl" />
         <tli id="T87" time="82.804" type="appl" />
         <tli id="T88" time="83.691" type="appl" />
         <tli id="T89" time="84.578" type="appl" />
         <tli id="T90" time="86.3597408555727" />
         <tli id="T91" time="87.851" type="appl" />
         <tli id="T92" time="89.024" type="appl" />
         <tli id="T93" time="89.677" type="appl" />
         <tli id="T94" time="90.31666908596706" />
         <tli id="T95" time="91.046" type="appl" />
         <tli id="T96" time="91.753" type="appl" />
         <tli id="T97" time="92.459" type="appl" />
         <tli id="T98" time="93.166" type="appl" />
         <tli id="T99" time="93.872" type="appl" />
         <tli id="T100" time="94.579" type="appl" />
         <tli id="T101" time="97.20637497414742" />
         <tli id="T102" time="98.149" type="appl" />
         <tli id="T103" time="99.101" type="appl" />
         <tli id="T104" time="100.265" type="appl" />
         <tli id="T105" time="101.45302889764594" />
         <tli id="T106" time="102.245" type="appl" />
         <tli id="T107" time="102.978" type="appl" />
         <tli id="T108" time="103.712" type="appl" />
         <tli id="T109" time="105.42635030801505" />
         <tli id="T110" time="106.433" type="appl" />
         <tli id="T111" time="107.291" type="appl" />
         <tli id="T112" time="108.148" type="appl" />
         <tli id="T113" time="109.41300501170753" />
         <tli id="T114" time="110.292" type="appl" />
         <tli id="T115" time="110.998" type="appl" />
         <tli id="T116" time="111.703" type="appl" />
         <tli id="T117" time="112.409" type="appl" />
         <tli id="T118" time="113.95965803498223" />
         <tli id="T119" time="114.797" type="appl" />
         <tli id="T120" time="115.602" type="appl" />
         <tli id="T121" time="118.20631195848075" />
         <tli id="T122" time="119.023" type="appl" />
         <tli id="T123" time="119.59" type="appl" />
         <tli id="T124" time="122.48629911528775" />
         <tli id="T125" time="123.36" type="appl" />
         <tli id="T126" time="123.923" type="appl" />
         <tli id="T127" time="124.485" type="appl" />
         <tli id="T128" time="125.048" type="appl" />
         <tli id="T129" time="125.61" type="appl" />
         <tli id="T130" time="126.173" type="appl" />
         <tli id="T131" time="126.735" type="appl" />
         <tli id="T132" time="127.786" type="appl" />
         <tli id="T133" time="129.66627756993122" />
         <tli id="T134" time="130.792" type="appl" />
         <tli id="T135" time="131.486" type="appl" />
         <tli id="T136" time="132.18" type="appl" />
         <tli id="T137" time="132.874" type="appl" />
         <tli id="T138" time="133.568" type="appl" />
         <tli id="T139" time="134.261" type="appl" />
         <tli id="T140" time="134.955" type="appl" />
         <tli id="T141" time="135.649" type="appl" />
         <tli id="T142" time="136.343" type="appl" />
         <tli id="T143" time="137.52625398406744" />
         <tli id="T144" time="138.773" type="appl" />
         <tli id="T145" time="139.744" type="appl" />
         <tli id="T146" time="140.716" type="appl" />
         <tli id="T147" time="141.687" type="appl" />
         <tli id="T148" time="147.06622535695027" />
         <tli id="T149" time="148.155" type="appl" />
         <tli id="T150" time="149.062" type="appl" />
         <tli id="T151" time="150.21" type="appl" />
         <tli id="T152" time="151.085" type="appl" />
         <tli id="T153" time="152.0862102932052" />
         <tli id="T154" time="152.746" type="appl" />
         <tli id="T155" time="153.284" type="appl" />
         <tli id="T156" time="153.822" type="appl" />
         <tli id="T157" time="157.03286211618152" />
         <tli id="T158" time="158.211" type="appl" />
         <tli id="T159" time="160.28618568708777" />
         <tli id="T160" time="161.354" type="appl" />
         <tli id="T161" time="162.291" type="appl" />
         <tli id="T162" time="163.228" type="appl" />
         <tli id="T163" time="164.165" type="appl" />
         <tli id="T164" time="165.102" type="appl" />
         <tli id="T165" time="166.039" type="appl" />
         <tli id="T968" time="166.8241858029627" type="intp" />
         <tli id="T166" time="168.13282880790055" />
         <tli id="T167" time="168.845" type="appl" />
         <tli id="T168" time="169.466" type="appl" />
         <tli id="T169" time="170.086" type="appl" />
         <tli id="T170" time="170.706" type="appl" />
         <tli id="T171" time="171.326" type="appl" />
         <tli id="T172" time="171.947" type="appl" />
         <tli id="T173" time="175.07280798272308" />
         <tli id="T174" time="175.751" type="appl" />
         <tli id="T175" time="176.36" type="appl" />
         <tli id="T176" time="176.968" type="appl" />
         <tli id="T177" time="177.576" type="appl" />
         <tli id="T178" time="178.185" type="appl" />
         <tli id="T179" time="179.0327960997688" />
         <tli id="T180" time="180.003" type="appl" />
         <tli id="T181" time="180.735" type="appl" />
         <tli id="T182" time="181.467" type="appl" />
         <tli id="T183" time="182.199" type="appl" />
         <tli id="T184" time="182.931" type="appl" />
         <tli id="T185" time="183.543" type="appl" />
         <tli id="T186" time="184.155" type="appl" />
         <tli id="T187" time="184.768" type="appl" />
         <tli id="T188" time="185.38" type="appl" />
         <tli id="T189" time="185.992" type="appl" />
         <tli id="T190" time="187.53943724008923" />
         <tli id="T191" time="188.42" type="appl" />
         <tli id="T192" time="189.146" type="appl" />
         <tli id="T193" time="191.3327591905926" />
         <tli id="T194" time="192.001" type="appl" />
         <tli id="T195" time="192.745" type="appl" />
         <tli id="T196" time="194.03941773524002" />
         <tli id="T197" time="195.108" type="appl" />
         <tli id="T198" time="196.52607694005158" />
         <tli id="T199" time="197.119" type="appl" />
         <tli id="T200" time="197.601" type="appl" />
         <tli id="T201" time="198.082" type="appl" />
         <tli id="T202" time="198.563" type="appl" />
         <tli id="T203" time="199.044" type="appl" />
         <tli id="T204" time="199.526" type="appl" />
         <tli id="T205" time="201.426062236396" />
         <tli id="T206" time="202.287" type="appl" />
         <tli id="T207" time="202.97" type="appl" />
         <tli id="T208" time="203.85938826791397" />
         <tli id="T209" time="204.646" type="appl" />
         <tli id="T210" time="205.303" type="appl" />
         <tli id="T211" time="205.96" type="appl" />
         <tli id="T212" time="206.616" type="appl" />
         <tli id="T213" time="207.273" type="appl" />
         <tli id="T214" time="208.47937440446734" />
         <tli id="T215" time="209.359" type="appl" />
         <tli id="T216" time="210.047" type="appl" />
         <tli id="T217" time="210.736" type="appl" />
         <tli id="T218" time="211.424" type="appl" />
         <tli id="T219" time="213.91269143374723" />
         <tli id="T220" time="214.712" type="appl" />
         <tli id="T221" time="215.283" type="appl" />
         <tli id="T222" time="215.854" type="appl" />
         <tli id="T223" time="216.4526838118523" />
         <tli id="T224" time="216.968" type="appl" />
         <tli id="T225" time="217.512" type="appl" />
         <tli id="T226" time="218.055" type="appl" />
         <tli id="T227" time="220.01933977585813" />
         <tli id="T228" time="221.046" type="appl" />
         <tli id="T229" time="221.71933467458987" />
         <tli id="T230" time="222.652" type="appl" />
         <tli id="T231" time="224.65932585239653" />
         <tli id="T232" time="225.802" type="appl" />
         <tli id="T233" time="227.7193166701137" />
         <tli id="T234" time="228.912" type="appl" />
         <tli id="T235" time="230.184" type="appl" />
         <tli id="T236" time="233.32596651259757" />
         <tli id="T237" time="233.999" type="appl" />
         <tli id="T238" time="234.688" type="appl" />
         <tli id="T239" time="235.378" type="appl" />
         <tli id="T240" time="236.067" type="appl" />
         <tli id="T241" time="236.705" type="appl" />
         <tli id="T242" time="237.335" type="appl" />
         <tli id="T243" time="237.966" type="appl" />
         <tli id="T244" time="238.597" type="appl" />
         <tli id="T245" time="239.227" type="appl" />
         <tli id="T246" time="239.858" type="appl" />
         <tli id="T247" time="240.713" type="appl" />
         <tli id="T248" time="241.568" type="appl" />
         <tli id="T249" time="242.422" type="appl" />
         <tli id="T250" time="243.277" type="appl" />
         <tli id="T251" time="244.132" type="appl" />
         <tli id="T252" time="244.986" type="appl" />
         <tli id="T253" time="245.841" type="appl" />
         <tli id="T254" time="247.2725913288596" />
         <tli id="T255" time="248.213" type="appl" />
         <tli id="T256" time="248.865" type="appl" />
         <tli id="T257" time="249.516" type="appl" />
         <tli id="T258" time="250.168" type="appl" />
         <tli id="T259" time="250.82" type="appl" />
         <tli id="T260" time="251.85924423210452" />
         <tli id="T261" time="252.466" type="appl" />
         <tli id="T262" time="252.927" type="appl" />
         <tli id="T263" time="253.387" type="appl" />
         <tli id="T264" time="253.847" type="appl" />
         <tli id="T265" time="254.308" type="appl" />
         <tli id="T266" time="255.78589911584174" />
         <tli id="T267" time="256.817" type="appl" />
         <tli id="T268" time="257.59" type="appl" />
         <tli id="T269" time="258.6192239470613" />
         <tli id="T270" time="259.502" type="appl" />
         <tli id="T271" time="260.184" type="appl" />
         <tli id="T272" time="260.866" type="appl" />
         <tli id="T273" time="261.548" type="appl" />
         <tli id="T274" time="262.23" type="appl" />
         <tli id="T275" time="262.912" type="appl" />
         <tli id="T276" time="263.594" type="appl" />
         <tli id="T277" time="265.69253605511767" />
         <tli id="T278" time="266.328" type="appl" />
         <tli id="T279" time="267.139" type="appl" />
         <tli id="T280" time="268.0991954999889" />
         <tli id="T281" time="268.695" type="appl" />
         <tli id="T282" time="269.128" type="appl" />
         <tli id="T283" time="269.56" type="appl" />
         <tli id="T284" time="269.993" type="appl" />
         <tli id="T285" time="270.426" type="appl" />
         <tli id="T286" time="271.84585092386044" />
         <tli id="T287" time="272.677" type="appl" />
         <tli id="T288" time="274.7991753949905" />
         <tli id="T289" time="275.789" type="appl" />
         <tli id="T290" time="277.25916801315526" />
         <tli id="T291" time="277.854" type="appl" />
         <tli id="T292" time="278.459" type="appl" />
         <tli id="T293" time="279.33916177160353" />
         <tli id="T294" time="280.179" type="appl" />
         <tli id="T295" time="280.856" type="appl" />
         <tli id="T296" time="281.533" type="appl" />
         <tli id="T297" time="282.209" type="appl" />
         <tli id="T298" time="282.886" type="appl" />
         <tli id="T299" time="283.563" type="appl" />
         <tli id="T300" time="284.6191459276645" />
         <tli id="T301" time="285.658" type="appl" />
         <tli id="T302" time="286.566" type="appl" />
         <tli id="T303" time="287.023" type="appl" />
         <tli id="T304" time="287.48" type="appl" />
         <tli id="T305" time="288.7191336246058" />
         <tli id="T306" time="289.522" type="appl" />
         <tli id="T307" time="290.116" type="appl" />
         <tli id="T308" time="290.71" type="appl" />
         <tli id="T309" time="291.305" type="appl" />
         <tli id="T310" time="291.899" type="appl" />
         <tli id="T311" time="293.5924523343034" />
         <tli id="T312" time="294.236" type="appl" />
         <tli id="T313" time="294.835" type="appl" />
         <tli id="T314" time="295.435" type="appl" />
         <tli id="T315" time="296.034" type="appl" />
         <tli id="T316" time="296.634" type="appl" />
         <tli id="T317" time="297.233" type="appl" />
         <tli id="T318" time="298.5657707439265" />
         <tli id="T319" time="299.372" type="appl" />
         <tli id="T320" time="300.196" type="appl" />
         <tli id="T321" time="301.019" type="appl" />
         <tli id="T322" time="301.843" type="appl" />
         <tli id="T323" time="303.17909023381816" />
         <tli id="T324" time="303.954" type="appl" />
         <tli id="T325" time="304.756" type="appl" />
         <tli id="T326" time="305.558" type="appl" />
         <tli id="T327" time="306.359" type="appl" />
         <tli id="T328" time="307.16" type="appl" />
         <tli id="T329" time="308.5857406764512" />
         <tli id="T330" time="309.488" type="appl" />
         <tli id="T331" time="310.351" type="appl" />
         <tli id="T332" time="311.214" type="appl" />
         <tli id="T333" time="312.078" type="appl" />
         <tli id="T334" time="312.941" type="appl" />
         <tli id="T335" time="314.4723896787263" />
         <tli id="T336" time="315.352" type="appl" />
         <tli id="T337" time="316.322" type="appl" />
         <tli id="T338" time="317.292" type="appl" />
         <tli id="T339" time="318.261" type="appl" />
         <tli id="T340" time="319.23" type="appl" />
         <tli id="T341" time="320.2" type="appl" />
         <tli id="T342" time="321.17" type="appl" />
         <tli id="T343" time="322.2256997462754" />
         <tli id="T344" time="322.759" type="appl" />
         <tli id="T345" time="323.177" type="appl" />
         <tli id="T346" time="323.594" type="appl" />
         <tli id="T347" time="324.155" type="appl" />
         <tli id="T348" time="324.716" type="appl" />
         <tli id="T349" time="325.277" type="appl" />
         <tli id="T350" time="325.838" type="appl" />
         <tli id="T351" time="326.399" type="appl" />
         <tli id="T352" time="326.961" type="appl" />
         <tli id="T353" time="327.522" type="appl" />
         <tli id="T354" time="328.083" type="appl" />
         <tli id="T355" time="328.644" type="appl" />
         <tli id="T356" time="329.205" type="appl" />
         <tli id="T357" time="329.766" type="appl" />
         <tli id="T358" time="330.484" type="appl" />
         <tli id="T359" time="331.3723389661183" />
         <tli id="T360" time="332.335" type="appl" />
         <tli id="T361" time="333.083" type="appl" />
         <tli id="T362" time="333.831" type="appl" />
         <tli id="T363" time="334.579" type="appl" />
         <tli id="T364" time="335.734" type="appl" />
         <tli id="T365" time="336.832" type="appl" />
         <tli id="T366" time="338.76565011393603" />
         <tli id="T367" time="339.534" type="appl" />
         <tli id="T368" time="340.38" type="appl" />
         <tli id="T369" time="341.225" type="appl" />
         <tli id="T370" time="342.4256391312055" />
         <tli id="T371" time="343.398" type="appl" />
         <tli id="T372" time="344.208" type="appl" />
         <tli id="T373" time="346.8322925745847" />
         <tli id="T374" time="347.515" type="appl" />
         <tli id="T375" time="348.167" type="appl" />
         <tli id="T376" time="348.82" type="appl" />
         <tli id="T377" time="349.473" type="appl" />
         <tli id="T378" time="350.125" type="appl" />
         <tli id="T379" time="351.5522784110634" />
         <tli id="T380" time="352.541" type="appl" />
         <tli id="T381" time="353.369" type="appl" />
         <tli id="T382" time="354.197" type="appl" />
         <tli id="T383" time="354.845" type="appl" />
         <tli id="T384" time="355.493" type="appl" />
         <tli id="T385" time="356.141" type="appl" />
         <tli id="T386" time="356.789" type="appl" />
         <tli id="T387" time="357.437" type="appl" />
         <tli id="T388" time="358.7589234523537" />
         <tli id="T389" time="359.52" type="appl" />
         <tli id="T390" time="360.222" type="appl" />
         <tli id="T391" time="361.04558325731443" />
         <tli id="T392" time="361.447" type="appl" />
         <tli id="T393" time="361.934" type="appl" />
         <tli id="T394" time="362.422" type="appl" />
         <tli id="T395" time="362.91" type="appl" />
         <tli id="T396" time="363.397" type="appl" />
         <tli id="T397" time="363.885" type="appl" />
         <tli id="T398" time="364.629" type="appl" />
         <tli id="T399" time="365.129" type="appl" />
         <tli id="T400" time="365.629" type="appl" />
         <tli id="T401" time="366.128" type="appl" />
         <tli id="T402" time="366.628" type="appl" />
         <tli id="T403" time="367.89889602553495" />
         <tli id="T404" time="368.538" type="appl" />
         <tli id="T405" time="369.168" type="appl" />
         <tli id="T406" time="369.798" type="appl" />
         <tli id="T407" time="371.3922188762622" />
         <tli id="T408" time="372.046" type="appl" />
         <tli id="T409" time="372.598" type="appl" />
         <tli id="T410" time="373.15" type="appl" />
         <tli id="T411" time="373.702" type="appl" />
         <tli id="T412" time="374.378" type="appl" />
         <tli id="T413" time="375.033" type="appl" />
         <tli id="T414" time="375.688" type="appl" />
         <tli id="T415" time="376.53887009908925" />
         <tli id="T416" time="377.675" type="appl" />
         <tli id="T417" time="378.57" type="appl" />
         <tli id="T418" time="380.21885905634383" />
         <tli id="T419" time="381.12" type="appl" />
         <tli id="T420" time="381.97" type="appl" />
         <tli id="T421" time="382.821" type="appl" />
         <tli id="T422" time="383.671" type="appl" />
         <tli id="T423" time="384.9188449528375" />
         <tli id="T424" time="385.359" type="appl" />
         <tli id="T425" time="385.88" type="appl" />
         <tli id="T426" time="386.59217326492245" />
         <tli id="T427" time="390.7588274284807" />
         <tli id="T428" time="391.497" type="appl" />
         <tli id="T429" time="392.148" type="appl" />
         <tli id="T430" time="392.8" type="appl" />
         <tli id="T431" time="393.452" type="appl" />
         <tli id="T432" time="394.103" type="appl" />
         <tli id="T433" time="394.755" type="appl" />
         <tli id="T434" time="395.508" type="appl" />
         <tli id="T435" time="396.261" type="appl" />
         <tli id="T436" time="397.014" type="appl" />
         <tli id="T437" time="397.767" type="appl" />
         <tli id="T438" time="398.66547036924874" />
         <tli id="T439" time="399.401" type="appl" />
         <tli id="T440" time="400.062" type="appl" />
         <tli id="T441" time="400.723" type="appl" />
         <tli id="T442" time="401.384" type="appl" />
         <tli id="T443" time="402.045" type="appl" />
         <tli id="T444" time="402.706" type="appl" />
         <tli id="T445" time="403.367" type="appl" />
         <tli id="T446" time="404.028" type="appl" />
         <tli id="T447" time="404.823" type="appl" />
         <tli id="T448" time="405.618" type="appl" />
         <tli id="T449" time="406.413" type="appl" />
         <tli id="T450" time="407.209" type="appl" />
         <tli id="T451" time="408.004" type="appl" />
         <tli id="T452" time="408.799" type="appl" />
         <tli id="T453" time="409.594" type="appl" />
         <tli id="T454" time="410.59" type="appl" />
         <tli id="T455" time="411.069" type="appl" />
         <tli id="T456" time="411.547" type="appl" />
         <tli id="T457" time="412.025" type="appl" />
         <tli id="T458" time="412.503" type="appl" />
         <tli id="T459" time="412.982" type="appl" />
         <tli id="T460" time="413.6320921247498" />
         <tli id="T461" time="414.551" type="appl" />
         <tli id="T462" time="415.318" type="appl" />
         <tli id="T463" time="417.79874628830794" />
         <tli id="T464" time="418.369" type="appl" />
         <tli id="T465" time="418.959" type="appl" />
         <tli id="T466" time="419.548" type="appl" />
         <tli id="T467" time="420.138" type="appl" />
         <tli id="T468" time="420.727" type="appl" />
         <tli id="T469" time="421.317" type="appl" />
         <tli id="T470" time="421.906" type="appl" />
         <tli id="T471" time="422.77206469793106" />
         <tli id="T472" time="423.462" type="appl" />
         <tli id="T473" time="424.2853934901354" />
         <tli id="T474" time="424.784" type="appl" />
         <tli id="T475" time="425.238" type="appl" />
         <tli id="T476" time="425.691" type="appl" />
         <tli id="T477" time="426.144" type="appl" />
         <tli id="T478" time="426.598" type="appl" />
         <tli id="T479" time="428.63204711355934" />
         <tli id="T480" time="429.396" type="appl" />
         <tli id="T481" time="429.996" type="appl" />
         <tli id="T482" time="430.597" type="appl" />
         <tli id="T483" time="431.197" type="appl" />
         <tli id="T484" time="431.798" type="appl" />
         <tli id="T485" time="432.398" type="appl" />
         <tli id="T486" time="432.999" type="appl" />
         <tli id="T487" time="433.599" type="appl" />
         <tli id="T488" time="434.31203006932185" />
         <tli id="T489" time="435.05" type="appl" />
         <tli id="T490" time="435.673" type="appl" />
         <tli id="T491" time="441.8986739703286" />
         <tli id="T492" time="442.598" type="appl" />
         <tli id="T493" time="443.32" type="appl" />
         <tli id="T494" time="444.043" type="appl" />
         <tli id="T495" time="444.90533161475224" />
         <tli id="T496" time="445.522" type="appl" />
         <tli id="T497" time="446.105" type="appl" />
         <tli id="T498" time="446.688" type="appl" />
         <tli id="T499" time="447.138" type="appl" />
         <tli id="T500" time="447.588" type="appl" />
         <tli id="T501" time="448.037" type="appl" />
         <tli id="T502" time="448.5136541203936" />
         <tli id="T503" time="451.2119793567139" />
         <tli id="T504" time="451.904" type="appl" />
         <tli id="T505" time="452.379" type="appl" />
         <tli id="T506" time="452.853" type="appl" />
         <tli id="T507" time="453.327" type="appl" />
         <tli id="T508" time="453.802" type="appl" />
         <tli id="T509" time="460.1319525900593" />
         <tli id="T510" time="461.033" type="appl" />
         <tli id="T511" time="461.751" type="appl" />
         <tli id="T512" time="462.469" type="appl" />
         <tli id="T513" time="463.187" type="appl" />
         <tli id="T514" time="463.905" type="appl" />
         <tli id="T515" time="464.623" type="appl" />
         <tli id="T516" time="465.341" type="appl" />
         <tli id="T517" time="466.292" type="appl" />
         <tli id="T518" time="467.235" type="appl" />
         <tli id="T519" time="468.178" type="appl" />
         <tli id="T520" time="469.5719242630168" />
         <tli id="T521" time="470.306" type="appl" />
         <tli id="T522" time="470.904" type="appl" />
         <tli id="T523" time="471.501" type="appl" />
         <tli id="T524" time="472.099" type="appl" />
         <tli id="T525" time="472.696" type="appl" />
         <tli id="T526" time="473.294" type="appl" />
         <tli id="T527" time="473.891" type="appl" />
         <tli id="T528" time="475.13" type="appl" />
         <tli id="T529" time="477.07190175742153" />
         <tli id="T530" time="477.786" type="appl" />
         <tli id="T531" time="478.482" type="appl" />
         <tli id="T532" time="479.179" type="appl" />
         <tli id="T533" time="479.876" type="appl" />
         <tli id="T534" time="480.572" type="appl" />
         <tli id="T535" time="481.269" type="appl" />
         <tli id="T536" time="481.966" type="appl" />
         <tli id="T537" time="482.662" type="appl" />
         <tli id="T538" time="483.359" type="appl" />
         <tli id="T539" time="486.30520738386656" />
         <tli id="T540" time="487.545" type="appl" />
         <tli id="T541" time="488.7185334753994" />
         <tli id="T542" time="489.461" type="appl" />
         <tli id="T543" time="490.247" type="appl" />
         <tli id="T544" time="491.032" type="appl" />
         <tli id="T545" time="491.818" type="appl" />
         <tli id="T546" time="492.603" type="appl" />
         <tli id="T547" time="493.567" type="appl" />
         <tli id="T548" time="494.6518490043063" />
         <tli id="T549" time="495.386" type="appl" />
         <tli id="T550" time="496.089" type="appl" />
         <tli id="T551" time="497.8385061085956" />
         <tli id="T552" time="498.686" type="appl" />
         <tli id="T553" time="499.276" type="appl" />
         <tli id="T554" time="500.3651651933773" />
         <tli id="T555" time="501.13" type="appl" />
         <tli id="T556" time="501.793" type="appl" />
         <tli id="T557" time="502.455" type="appl" />
         <tli id="T558" time="503.118" type="appl" />
         <tli id="T559" time="504.6851522301545" />
         <tli id="T560" time="506.198" type="appl" />
         <tli id="T561" time="507.497" type="appl" />
         <tli id="T562" time="509.4118047132949" />
         <tli id="T563" time="510.279" type="appl" />
         <tli id="T564" time="511.128" type="appl" />
         <tli id="T565" time="512.5984618175842" />
         <tli id="T566" time="513.641" type="appl" />
         <tli id="T567" time="514.559" type="appl" />
         <tli id="T568" time="515.477" type="appl" />
         <tli id="T569" time="516.395" type="appl" />
         <tli id="T570" time="517.313" type="appl" />
         <tli id="T571" time="518.308" type="appl" />
         <tli id="T572" time="519.303" type="appl" />
         <tli id="T573" time="520.298" type="appl" />
         <tli id="T574" time="521.293" type="appl" />
         <tli id="T575" time="522.5850985168005" />
         <tli id="T576" time="523.41" type="appl" />
         <tli id="T577" time="524.223" type="appl" />
         <tli id="T578" time="526.2650874740551" />
         <tli id="T579" time="527.175" type="appl" />
         <tli id="T580" time="527.704" type="appl" />
         <tli id="T581" time="528.233" type="appl" />
         <tli id="T582" time="529.1250788919215" />
         <tli id="T583" time="531.067" type="appl" />
         <tli id="T584" time="532.994" type="appl" />
         <tli id="T585" time="534.92" type="appl" />
         <tli id="T586" time="535.382" type="appl" />
         <tli id="T587" time="535.845" type="appl" />
         <tli id="T588" time="536.308" type="appl" />
         <tli id="T589" time="536.77" type="appl" />
         <tli id="T590" time="537.351" type="appl" />
         <tli id="T591" time="537.923" type="appl" />
         <tli id="T592" time="538.6450503248192" />
         <tli id="T593" time="539.444" type="appl" />
         <tli id="T594" time="540.029" type="appl" />
         <tli id="T595" time="540.8050438432077" />
         <tli id="T596" time="541.475" type="appl" />
         <tli id="T597" time="542.11" type="appl" />
         <tli id="T598" time="542.746" type="appl" />
         <tli id="T599" time="543.382" type="appl" />
         <tli id="T600" time="544.017" type="appl" />
         <tli id="T601" time="547.7716896046771" />
         <tli id="T602" time="548.574" type="appl" />
         <tli id="T603" time="549.342" type="appl" />
         <tli id="T604" time="550.109" type="appl" />
         <tli id="T605" time="550.876" type="appl" />
         <tli id="T606" time="551.644" type="appl" />
         <tli id="T607" time="552.6050084344046" />
         <tli id="T608" time="553.186" type="appl" />
         <tli id="T609" time="553.841" type="appl" />
         <tli id="T610" time="554.9183348260121" />
         <tli id="T611" time="555.749" type="appl" />
         <tli id="T612" time="556.581" type="appl" />
         <tli id="T613" time="557.413" type="appl" />
         <tli id="T614" time="558.245" type="appl" />
         <tli id="T615" time="559.077" type="appl" />
         <tli id="T616" time="559.909" type="appl" />
         <tli id="T617" time="560.741" type="appl" />
         <tli id="T618" time="561.9316471141133" />
         <tli id="T619" time="563.031" type="appl" />
         <tli id="T620" time="563.761" type="appl" />
         <tli id="T621" time="564.49" type="appl" />
         <tli id="T622" time="565.219" type="appl" />
         <tli id="T623" time="566.114" type="appl" />
         <tli id="T624" time="566.75" type="appl" />
         <tli id="T625" time="567.386" type="appl" />
         <tli id="T626" time="568.022" type="appl" />
         <tli id="T627" time="568.8382930556273" />
         <tli id="T628" time="569.664" type="appl" />
         <tli id="T629" time="570.278" type="appl" />
         <tli id="T630" time="573.2382798523449" />
         <tli id="T631" time="574.132" type="appl" />
         <tli id="T632" time="574.974" type="appl" />
         <tli id="T633" time="575.815" type="appl" />
         <tli id="T634" time="576.657" type="appl" />
         <tli id="T635" time="577.499" type="appl" />
         <tli id="T636" time="581.638254646078" />
         <tli id="T637" time="582.856" type="appl" />
         <tli id="T638" time="583.898" type="appl" />
         <tli id="T639" time="584.941" type="appl" />
         <tli id="T640" time="585.983" type="appl" />
         <tli id="T641" time="588.0115688546567" />
         <tli id="T642" time="589.127" type="appl" />
         <tli id="T643" time="590.009" type="appl" />
         <tli id="T644" time="590.892" type="appl" />
         <tli id="T645" time="592.5915551112399" />
         <tli id="T646" time="593.477" type="appl" />
         <tli id="T647" time="594.238" type="appl" />
         <tli id="T648" time="594.999" type="appl" />
         <tli id="T649" time="596.0048782020268" />
         <tli id="T650" time="597.088" type="appl" />
         <tli id="T651" time="597.886" type="appl" />
         <tli id="T652" time="598.684" type="appl" />
         <tli id="T653" time="601.0048631982968" />
         <tli id="T654" time="601.636" type="appl" />
         <tli id="T655" time="602.172" type="appl" />
         <tli id="T656" time="602.707" type="appl" />
         <tli id="T657" time="603.6381886296654" />
         <tli id="T658" time="604.364" type="appl" />
         <tli id="T659" time="605.144" type="appl" />
         <tli id="T660" time="606.4781801075468" />
         <tli id="T661" time="607.129" type="appl" />
         <tli id="T662" time="607.665" type="appl" />
         <tli id="T969" time="607.8942857142857" type="intp" />
         <tli id="T663" time="608.2" type="appl" />
         <tli id="T664" time="608.736" type="appl" />
         <tli id="T665" time="609.271" type="appl" />
         <tli id="T666" time="609.807" type="appl" />
         <tli id="T667" time="610.8248337309707" />
         <tli id="T668" time="611.662" type="appl" />
         <tli id="T669" time="612.212" type="appl" />
         <tli id="T670" time="612.763" type="appl" />
         <tli id="T671" time="613.313" type="appl" />
         <tli id="T672" time="613.863" type="appl" />
         <tli id="T673" time="614.413" type="appl" />
         <tli id="T674" time="614.964" type="appl" />
         <tli id="T675" time="615.514" type="appl" />
         <tli id="T676" time="616.1781510003102" />
         <tli id="T677" time="616.915" type="appl" />
         <tli id="T678" time="617.541" type="appl" />
         <tli id="T679" time="618.166" type="appl" />
         <tli id="T680" time="618.792" type="appl" />
         <tli id="T681" time="621.5848014429434" />
         <tli id="T682" time="622.484" type="appl" />
         <tli id="T683" time="623.345" type="appl" />
         <tli id="T684" time="624.206" type="appl" />
         <tli id="T685" time="625.066" type="appl" />
         <tli id="T686" time="625.927" type="appl" />
         <tli id="T687" time="628.4914473844575" />
         <tli id="T688" time="629.24" type="appl" />
         <tli id="T689" time="629.838" type="appl" />
         <tli id="T690" time="630.435" type="appl" />
         <tli id="T691" time="631.033" type="appl" />
         <tli id="T692" time="631.631" type="appl" />
         <tli id="T693" time="632.229" type="appl" />
         <tli id="T694" time="632.827" type="appl" />
         <tli id="T695" time="633.425" type="appl" />
         <tli id="T696" time="634.022" type="appl" />
         <tli id="T697" time="634.62" type="appl" />
         <tli id="T698" time="635.7047590724094" />
         <tli id="T699" time="636.809" type="appl" />
         <tli id="T700" time="637.711" type="appl" />
         <tli id="T701" time="638.613" type="appl" />
         <tli id="T702" time="639.515" type="appl" />
         <tli id="T703" time="641.7847408278735" />
         <tli id="T704" time="642.706" type="appl" />
         <tli id="T705" time="643.343" type="appl" />
         <tli id="T706" time="643.981" type="appl" />
         <tli id="T707" time="644.618" type="appl" />
         <tli id="T708" time="645.255" type="appl" />
         <tli id="T709" time="645.892" type="appl" />
         <tli id="T710" time="646.529" type="appl" />
         <tli id="T711" time="647.167" type="appl" />
         <tli id="T712" time="647.804" type="appl" />
         <tli id="T713" time="648.6713868294025" />
         <tli id="T714" time="649.525" type="appl" />
         <tli id="T715" time="650.215" type="appl" />
         <tli id="T716" time="651.4380451940051" />
         <tli id="T717" time="652.488" type="appl" />
         <tli id="T718" time="656.4180302502899" />
         <tli id="T719" time="657.361" type="appl" />
         <tli id="T720" time="658.352" type="appl" />
         <tli id="T721" time="659.6046873545793" />
         <tli id="T722" time="660.822" type="appl" />
         <tli id="T723" time="662.026" type="appl" />
         <tli id="T724" time="663.229" type="appl" />
         <tli id="T725" time="664.7913384573765" />
         <tli id="T726" time="667.0" type="appl" />
         <tli id="T727" time="669.073" type="appl" />
         <tli id="T728" time="671.147" type="appl" />
         <tli id="T729" time="673.221" type="appl" />
         <tli id="T730" time="675.294" type="appl" />
         <tli id="T731" time="677.6912997477526" />
         <tli id="T732" time="678.664" type="appl" />
         <tli id="T733" time="679.524" type="appl" />
         <tli id="T734" time="680.384" type="appl" />
         <tli id="T735" time="682.0646199578234" />
         <tli id="T736" time="683.7446149165701" />
         <tli id="T737" time="684.706" type="appl" />
         <tli id="T738" time="685.462" type="appl" />
         <tli id="T739" time="686.217" type="appl" />
         <tli id="T740" time="686.973" type="appl" />
         <tli id="T741" time="689.4979309856112" />
         <tli id="T742" time="690.736" type="appl" />
         <tli id="T743" time="691.906" type="appl" />
         <tli id="T744" time="693.077" type="appl" />
         <tli id="T745" time="694.247" type="appl" />
         <tli id="T746" time="697.1245747665881" />
         <tli id="T747" time="698.212" type="appl" />
         <tli id="T748" time="699.178" type="appl" />
         <tli id="T749" time="700.143" type="appl" />
         <tli id="T750" time="701.109" type="appl" />
         <tli id="T751" time="702.075" type="appl" />
         <tli id="T752" time="702.85" type="appl" />
         <tli id="T753" time="703.625" type="appl" />
         <tli id="T754" time="706.5645464395456" />
         <tli id="T755" time="707.078" type="appl" />
         <tli id="T756" time="707.678" type="appl" />
         <tli id="T757" time="708.278" type="appl" />
         <tli id="T758" time="708.878" type="appl" />
         <tli id="T759" time="709.478" type="appl" />
         <tli id="T760" time="710.078" type="appl" />
         <tli id="T761" time="710.678" type="appl" />
         <tli id="T762" time="711.278" type="appl" />
         <tli id="T763" time="711.982" type="appl" />
         <tli id="T764" time="712.686" type="appl" />
         <tli id="T765" time="713.389" type="appl" />
         <tli id="T766" time="714.093" type="appl" />
         <tli id="T767" time="714.797" type="appl" />
         <tli id="T768" time="715.501" type="appl" />
         <tli id="T769" time="716.205" type="appl" />
         <tli id="T770" time="716.908" type="appl" />
         <tli id="T771" time="717.612" type="appl" />
         <tli id="T772" time="718.316" type="appl" />
         <tli id="T773" time="719.02" type="appl" />
         <tli id="T774" time="719.724" type="appl" />
         <tli id="T775" time="720.427" type="appl" />
         <tli id="T776" time="721.131" type="appl" />
         <tli id="T777" time="722.0644999279821" />
         <tli id="T778" time="723.011" type="appl" />
         <tli id="T779" time="723.778" type="appl" />
         <tli id="T780" time="725.9644882250726" />
         <tli id="T781" time="727.088" type="appl" />
         <tli id="T782" time="727.84" type="appl" />
         <tli id="T783" time="728.592" type="appl" />
         <tli id="T784" time="729.344" type="appl" />
         <tli id="T785" time="730.095" type="appl" />
         <tli id="T786" time="730.847" type="appl" />
         <tli id="T787" time="731.599" type="appl" />
         <tli id="T788" time="732.3243128907967" />
         <tli id="T789" time="732.794" type="appl" />
         <tli id="T790" time="733.238" type="appl" />
         <tli id="T791" time="733.682" type="appl" />
         <tli id="T792" time="734.125" type="appl" />
         <tli id="T793" time="735.156" type="appl" />
         <tli id="T794" time="735.961" type="appl" />
         <tli id="T795" time="736.766" type="appl" />
         <tli id="T796" time="737.571" type="appl" />
         <tli id="T797" time="741.3844419535687" />
         <tli id="T798" time="742.551" type="appl" />
         <tli id="T799" time="743.408" type="appl" />
         <tli id="T800" time="744.265" type="appl" />
         <tli id="T801" time="745.122" type="appl" />
         <tli id="T802" time="746.083" type="appl" />
         <tli id="T803" time="747.045" type="appl" />
         <tli id="T804" time="748.1777549018341" />
         <tli id="T805" time="748.939" type="appl" />
         <tli id="T806" time="749.612" type="appl" />
         <tli id="T807" time="750.322" type="appl" />
         <tli id="T808" time="751.032" type="appl" />
         <tli id="T809" time="751.742" type="appl" />
         <tli id="T810" time="752.451" type="appl" />
         <tli id="T811" time="753.161" type="appl" />
         <tli id="T812" time="754.7110686302933" />
         <tli id="T813" time="755.848" type="appl" />
         <tli id="T814" time="756.723" type="appl" />
         <tli id="T815" time="757.599" type="appl" />
         <tli id="T816" time="758.475" type="appl" />
         <tli id="T817" time="759.351" type="appl" />
         <tli id="T818" time="760.226" type="appl" />
         <tli id="T819" time="762.4910452844891" />
         <tli id="T820" time="763.202" type="appl" />
         <tli id="T821" time="763.994" type="appl" />
         <tli id="T822" time="764.786" type="appl" />
         <tli id="T823" time="765.8977017286144" />
         <tli id="T824" time="766.584" type="appl" />
         <tli id="T825" time="767.205" type="appl" />
         <tli id="T826" time="767.825" type="appl" />
         <tli id="T827" time="768.446" type="appl" />
         <tli id="T828" time="769.066" type="appl" />
         <tli id="T829" time="769.953" type="appl" />
         <tli id="T830" time="770.689" type="appl" />
         <tli id="T831" time="771.62" type="appl" />
         <tli id="T832" time="773.5910119762082" />
         <tli id="T833" time="774.736" type="appl" />
         <tli id="T834" time="775.731" type="appl" />
         <tli id="T835" time="776.726" type="appl" />
         <tli id="T836" time="777.721" type="appl" />
         <tli id="T837" time="778.41" type="appl" />
         <tli id="T838" time="779.099" type="appl" />
         <tli id="T839" time="779.788" type="appl" />
         <tli id="T840" time="781.8309872500608" />
         <tli id="T841" time="782.512" type="appl" />
         <tli id="T842" time="783.225" type="appl" />
         <tli id="T843" time="783.938" type="appl" />
         <tli id="T844" time="784.65" type="appl" />
         <tli id="T845" time="785.336" type="appl" />
         <tli id="T846" time="786.014" type="appl" />
         <tli id="T847" time="786.692" type="appl" />
         <tli id="T848" time="789.0909654646447" />
         <tli id="T849" time="790.116" type="appl" />
         <tli id="T850" time="790.82" type="appl" />
         <tli id="T851" time="791.524" type="appl" />
         <tli id="T852" time="792.227" type="appl" />
         <tli id="T853" time="792.802" type="appl" />
         <tli id="T854" time="794.430949440661" />
         <tli id="T855" time="797.2242743919104" />
         <tli id="T856" time="798.006" type="appl" />
         <tli id="T857" time="798.536" type="appl" />
         <tli id="T858" time="799.067" type="appl" />
         <tli id="T859" time="799.598" type="appl" />
         <tli id="T860" time="800.129" type="appl" />
         <tli id="T861" time="800.659" type="appl" />
         <tli id="T862" time="801.19" type="appl" />
         <tli id="T863" time="802.02" type="appl" />
         <tli id="T864" time="802.84" type="appl" />
         <tli id="T865" time="803.661" type="appl" />
         <tli id="T866" time="804.481" type="appl" />
         <tli id="T867" time="805.301" type="appl" />
         <tli id="T868" time="806.115" type="appl" />
         <tli id="T869" time="806.93" type="appl" />
         <tli id="T870" time="808.7575731166394" />
         <tli id="T871" time="809.82" type="appl" />
         <tli id="T872" time="810.683" type="appl" />
         <tli id="T873" time="811.546" type="appl" />
         <tli id="T874" time="812.408" type="appl" />
         <tli id="T875" time="813.271" type="appl" />
         <tli id="T876" time="814.134" type="appl" />
         <tli id="T877" time="814.917" type="appl" />
         <tli id="T878" time="815.701" type="appl" />
         <tli id="T879" time="816.484" type="appl" />
         <tli id="T880" time="817.267" type="appl" />
         <tli id="T881" time="818.05" type="appl" />
         <tli id="T882" time="818.834" type="appl" />
         <tli id="T883" time="820.7175372277169" />
         <tli id="T884" time="821.412" type="appl" />
         <tli id="T885" time="822.063" type="appl" />
         <tli id="T886" time="822.714" type="appl" />
         <tli id="T887" time="823.365" type="appl" />
         <tli id="T888" time="824.8908580379368" />
         <tli id="T889" time="825.43" type="appl" />
         <tli id="T890" time="826.097" type="appl" />
         <tli id="T891" time="826.764" type="appl" />
         <tli id="T892" time="827.43" type="appl" />
         <tli id="T893" time="828.688" type="appl" />
         <tli id="T894" time="830.4375080604656" />
         <tli id="T895" time="831.035" type="appl" />
         <tli id="T896" time="831.579" type="appl" />
         <tli id="T897" time="832.124" type="appl" />
         <tli id="T898" time="832.669" type="appl" />
         <tli id="T899" time="833.214" type="appl" />
         <tli id="T900" time="833.758" type="appl" />
         <tli id="T901" time="836.8241555623675" />
         <tli id="T902" time="837.51" type="appl" />
         <tli id="T903" time="838.37" type="appl" />
         <tli id="T904" time="839.231" type="appl" />
         <tli id="T905" time="840.091" type="appl" />
         <tli id="T906" time="841.1441425991446" />
         <tli id="T907" time="841.947" type="appl" />
         <tli id="T908" time="842.732" type="appl" />
         <tli id="T909" time="843.517" type="appl" />
         <tli id="T910" time="844.303" type="appl" />
         <tli id="T911" time="845.088" type="appl" />
         <tli id="T912" time="845.873" type="appl" />
         <tli id="T913" time="847.4174571077979" />
         <tli id="T914" time="848.301" type="appl" />
         <tli id="T915" time="848.914" type="appl" />
         <tli id="T916" time="849.528" type="appl" />
         <tli id="T917" time="850.141" type="appl" />
         <tli id="T918" time="850.755" type="appl" />
         <tli id="T919" time="851.369" type="appl" />
         <tli id="T920" time="851.982" type="appl" />
         <tli id="T921" time="852.596" type="appl" />
         <tli id="T922" time="853.209" type="appl" />
         <tli id="T923" time="855.6040992083571" />
         <tli id="T924" time="856.324" type="appl" />
         <tli id="T925" time="857.115" type="appl" />
         <tli id="T926" time="857.906" type="appl" />
         <tli id="T927" time="858.9307558925418" />
         <tli id="T928" time="859.691" type="appl" />
         <tli id="T929" time="860.424" type="appl" />
         <tli id="T930" time="864.337406335175" />
         <tli id="T931" time="865.814" type="appl" />
         <tli id="T932" time="867.002" type="appl" />
         <tli id="T933" time="869.3707245647533" />
         <tli id="T934" time="870.642" type="appl" />
         <tli id="T935" time="871.777" type="appl" />
         <tli id="T936" time="872.912" type="appl" />
         <tli id="T937" time="874.047" type="appl" />
         <tli id="T938" time="876.8840353524814" />
         <tli id="T939" time="877.779" type="appl" />
         <tli id="T940" time="878.645" type="appl" />
         <tli id="T941" time="881.2040223892587" />
         <tli id="T942" time="882.175" type="appl" />
         <tli id="T943" time="882.929" type="appl" />
         <tli id="T944" time="883.682" type="appl" />
         <tli id="T945" time="884.435" type="appl" />
         <tli id="T946" time="885.189" type="appl" />
         <tli id="T947" time="885.942" type="appl" />
         <tli id="T948" time="886.742" type="appl" />
         <tli id="T949" time="887.251" type="appl" />
         <tli id="T950" time="887.76" type="appl" />
         <tli id="T951" time="889.6106638296536" />
         <tli id="T952" time="890.26" type="appl" />
         <tli id="T953" time="891.042" type="appl" />
         <tli id="T954" time="891.824" type="appl" />
         <tli id="T955" time="892.607" type="appl" />
         <tli id="T956" time="893.39" type="appl" />
         <tli id="T957" time="894.5239824193216" />
         <tli id="T958" time="895.529" type="appl" />
         <tli id="T959" time="896.501" type="appl" />
         <tli id="T960" time="897.473" type="appl" />
         <tli id="T961" time="898.445" type="appl" />
         <tli id="T962" time="899.417" type="appl" />
         <tli id="T963" time="900.389" type="appl" />
         <tli id="T964" time="901.361" type="appl" />
         <tli id="T965" time="902.3730213662783" />
         <tli id="T966" time="902.55" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T202" start="T201">
            <tli id="T201.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T469" start="T468">
            <tli id="T468.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T498" start="T497">
            <tli id="T497.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T570" start="T569">
            <tli id="T569.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T965" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T1" id="Seg_5" n="HIAT:w" s="T0">davaj</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_9" n="HIAT:w" s="T1">nʼezittə</ts>
                  <nts id="Seg_10" n="HIAT:ip">.</nts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_13" n="HIAT:u" s="T2">
                  <nts id="Seg_14" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_16" n="HIAT:w" s="T2">On-</ts>
                  <nts id="Seg_17" n="HIAT:ip">)</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_20" n="HIAT:w" s="T3">Onʼiʔ</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_23" n="HIAT:w" s="T4">üdʼüge</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_26" n="HIAT:w" s="T5">penzi</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">ur-</ts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">üžünə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">üzəbi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_41" n="HIAT:u" s="T9">
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_44" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_45" n="HIAT:ip">)</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_48" n="HIAT:w" s="T10">dĭgəttə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_51" n="HIAT:w" s="T11">suʔmiluʔpi</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_54" n="HIAT:w" s="T12">bar</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_58" n="HIAT:u" s="T13">
                  <nts id="Seg_59" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_61" n="HIAT:w" s="T13">Dĭ=</ts>
                  <nts id="Seg_62" n="HIAT:ip">)</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_65" n="HIAT:w" s="T14">dĭ</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_68" n="HIAT:w" s="T15">bĭdəbi</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_72" n="HIAT:w" s="T16">bĭdəbi</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_76" n="HIAT:w" s="T17">dĭgəttə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_79" n="HIAT:w" s="T18">dĭ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_82" n="HIAT:w" s="T19">üžüttə</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">saʔməluʔpi</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">dĭgəttə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">dĭ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_94" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">ej=</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_99" n="HIAT:w" s="T24">ej=</ts>
                  <nts id="Seg_100" n="HIAT:ip">)</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">kambi</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_107" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_109" n="HIAT:w" s="T26">Măn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">dĭʔnə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_115" n="HIAT:w" s="T28">ugandə</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">mĭ-</ts>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_123" n="HIAT:w" s="T30">mĭliem</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">a</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">(</nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">dĭn=</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">dĭ</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">măna</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">kaməndə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">ej</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_147" n="HIAT:w" s="T37">šolia</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_151" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">Măn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">ugandə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">tăŋ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_162" n="HIAT:w" s="T41">măndərliam</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_166" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_168" n="HIAT:w" s="T42">Jakšə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_171" n="HIAT:w" s="T43">măndərdə</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_175" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_177" n="HIAT:w" s="T44">Dĭ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">kuzam</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">kăde</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">kăštəsʼtə</ts>
                  <nts id="Seg_187" n="HIAT:ip">?</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_190" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_192" n="HIAT:w" s="T48">I</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">kăde</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_198" n="HIAT:w" s="T50">numəjzittə</ts>
                  <nts id="Seg_199" n="HIAT:ip">?</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_202" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_204" n="HIAT:w" s="T51">Nʼuʔdə</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_207" n="HIAT:w" s="T52">măndəraʔ</ts>
                  <nts id="Seg_208" n="HIAT:ip">!</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_211" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_213" n="HIAT:w" s="T53">Nʼuʔdə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_216" n="HIAT:w" s="T54">măndəraʔ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_219" n="HIAT:w" s="T55">măjanə</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_223" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_225" n="HIAT:w" s="T56">Nada</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_228" n="HIAT:w" s="T57">teʔmem</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_231" n="HIAT:w" s="T58">sarzittə</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_235" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_237" n="HIAT:w" s="T59">Üžüm</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_240" n="HIAT:w" s="T60">nada</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_243" n="HIAT:w" s="T61">šerzittə</ts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_247" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_249" n="HIAT:w" s="T62">Pargam</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_252" n="HIAT:w" s="T63">nada</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_255" n="HIAT:w" s="T64">šerzittə</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_259" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_261" n="HIAT:w" s="T65">Dĭgəttə</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_264" n="HIAT:w" s="T66">nʼiʔtə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_267" n="HIAT:w" s="T67">kanzittə</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_271" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_273" n="HIAT:w" s="T68">Măn</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_275" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_277" n="HIAT:w" s="T69">čumbə</ts>
                  <nts id="Seg_278" n="HIAT:ip">)</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_281" n="HIAT:w" s="T70">kambiam</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_285" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_287" n="HIAT:w" s="T71">Čumgən</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_290" n="HIAT:w" s="T72">amnobiam</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_294" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_296" n="HIAT:w" s="T73">Dĭgəttə</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_300" n="HIAT:w" s="T74">baroʔ</ts>
                  <nts id="Seg_301" n="HIAT:ip">)</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_304" n="HIAT:w" s="T75">kalla</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_307" n="HIAT:w" s="T967">dʼürbiem</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_311" n="HIAT:w" s="T76">čum-</ts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_315" n="HIAT:w" s="T77">čumgəndə</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_319" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_321" n="HIAT:w" s="T78">Măn</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_324" n="HIAT:w" s="T79">üdʼüge</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_327" n="HIAT:w" s="T80">ibiem</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_331" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_333" n="HIAT:w" s="T81">Ine</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_336" n="HIAT:w" s="T82">ibi</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_338" n="HIAT:ip">(</nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">nuʔ-</ts>
                  <nts id="Seg_341" n="HIAT:ip">)</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_344" n="HIAT:w" s="T84">miʔ</ts>
                  <nts id="Seg_345" n="HIAT:ip">.</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_348" n="HIAT:u" s="T85">
                  <nts id="Seg_349" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_351" n="HIAT:w" s="T85">Šide=</ts>
                  <nts id="Seg_352" n="HIAT:ip">)</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_355" n="HIAT:w" s="T86">Šide</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_357" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_359" n="HIAT:w" s="T87">kɨ-</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_362" n="HIAT:w" s="T88">ke-</ts>
                  <nts id="Seg_363" n="HIAT:ip">)</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_366" n="HIAT:w" s="T89">kö</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_370" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_372" n="HIAT:w" s="T90">Bar</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_375" n="HIAT:w" s="T91">köreleʔpibeʔ</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_379" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_381" n="HIAT:w" s="T92">Dĭʔə</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_384" n="HIAT:w" s="T93">kandəga</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_388" n="HIAT:u" s="T94">
                  <nts id="Seg_389" n="HIAT:ip">(</nts>
                  <ts e="T95" id="Seg_391" n="HIAT:w" s="T94">Măn</ts>
                  <nts id="Seg_392" n="HIAT:ip">)</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_395" n="HIAT:w" s="T95">kallam</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_399" n="HIAT:w" s="T96">dĭgəttə</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_402" n="HIAT:w" s="T97">dĭ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_404" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_406" n="HIAT:w" s="T98">kandə-</ts>
                  <nts id="Seg_407" n="HIAT:ip">)</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_410" n="HIAT:w" s="T99">kaləj</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_413" n="HIAT:w" s="T100">mănzʼə</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_417" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_419" n="HIAT:w" s="T101">Sʼabiam</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_422" n="HIAT:w" s="T102">čumdə</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_426" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_428" n="HIAT:w" s="T103">Amnobiam</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_430" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_432" n="HIAT:w" s="T104">čumdən</ts>
                  <nts id="Seg_433" n="HIAT:ip">)</nts>
                  <nts id="Seg_434" n="HIAT:ip">.</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_437" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_439" n="HIAT:w" s="T105">Dĭgəttə</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_441" n="HIAT:ip">(</nts>
                  <ts e="T107" id="Seg_443" n="HIAT:w" s="T106">už-</ts>
                  <nts id="Seg_444" n="HIAT:ip">)</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_447" n="HIAT:w" s="T107">üzəbiem</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_450" n="HIAT:w" s="T108">čumdə</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_454" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_456" n="HIAT:w" s="T109">Tura</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_459" n="HIAT:w" s="T110">bar</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_461" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_463" n="HIAT:w" s="T111">neniluʔpiʔi=</ts>
                  <nts id="Seg_464" n="HIAT:ip">)</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_467" n="HIAT:w" s="T112">neniluʔpi</ts>
                  <nts id="Seg_468" n="HIAT:ip">.</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_471" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_473" n="HIAT:w" s="T113">Il</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_476" n="HIAT:w" s="T114">bar</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_479" n="HIAT:w" s="T115">nuʔməleʔbəʔjə</ts>
                  <nts id="Seg_480" n="HIAT:ip">,</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_483" n="HIAT:w" s="T116">bü</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_486" n="HIAT:w" s="T117">detleʔbəʔjə</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_490" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_492" n="HIAT:w" s="T118">Šünə</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_495" n="HIAT:w" s="T119">kămlaʔbəʔjə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_498" n="HIAT:w" s="T120">bar</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_502" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_504" n="HIAT:w" s="T121">Štobɨ</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_507" n="HIAT:w" s="T122">ej</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_510" n="HIAT:w" s="T123">nenibi</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_514" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_516" n="HIAT:w" s="T124">Onʼiʔ</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_519" n="HIAT:w" s="T125">kuza</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_522" n="HIAT:w" s="T126">onʼiʔ</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_525" n="HIAT:w" s="T127">kuzanə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_528" n="HIAT:w" s="T128">bar</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_531" n="HIAT:w" s="T129">ipek</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_534" n="HIAT:w" s="T130">nendəbi</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_538" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_540" n="HIAT:w" s="T131">Bar</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_543" n="HIAT:w" s="T132">nendəluʔpi</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_547" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_549" n="HIAT:w" s="T133">Xatʼel</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_552" n="HIAT:w" s="T134">молотилка</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_554" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_556" n="HIAT:w" s="T135">nun-</ts>
                  <nts id="Seg_557" n="HIAT:ip">)</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_560" n="HIAT:w" s="T136">nuldəsʼtə</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_563" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_565" n="HIAT:w" s="T137">a</ts>
                  <nts id="Seg_566" n="HIAT:ip">)</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_569" n="HIAT:w" s="T138">dĭ</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_572" n="HIAT:w" s="T139">ibi</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_575" n="HIAT:w" s="T140">da</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_578" n="HIAT:w" s="T141">ipek</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_581" n="HIAT:w" s="T142">nendəluʔpi</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_585" n="HIAT:u" s="T143">
                  <nts id="Seg_586" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_588" n="HIAT:w" s="T143">Kudonzəbi</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_591" n="HIAT:w" s="T144">bʼa-</ts>
                  <nts id="Seg_592" n="HIAT:ip">)</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_595" n="HIAT:w" s="T145">dĭzi</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_599" n="HIAT:w" s="T146">dĭ</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_602" n="HIAT:w" s="T147">kuroluʔpi</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_606" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_608" n="HIAT:w" s="T148">Noʔ</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_611" n="HIAT:w" s="T149">jaʔpiam</ts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_615" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_617" n="HIAT:w" s="T150">Zarottə</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_620" n="HIAT:w" s="T151">tažerbiam</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_624" n="HIAT:w" s="T152">embiem</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_628" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_630" n="HIAT:w" s="T153">A</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_633" n="HIAT:w" s="T154">dĭ</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_636" n="HIAT:w" s="T155">bar</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_639" n="HIAT:w" s="T156">neniluʔpi</ts>
                  <nts id="Seg_640" n="HIAT:ip">.</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_643" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_645" n="HIAT:w" s="T157">Maʔtə</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_648" n="HIAT:w" s="T158">šobiam</ts>
                  <nts id="Seg_649" n="HIAT:ip">.</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_652" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_654" n="HIAT:w" s="T159">Dĭn</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_657" n="HIAT:w" s="T160">bar</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_660" n="HIAT:w" s="T161">nubiam</ts>
                  <nts id="Seg_661" n="HIAT:ip">,</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_664" n="HIAT:w" s="T162">dĭgəttə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_666" n="HIAT:ip">(</nts>
                  <ts e="T164" id="Seg_668" n="HIAT:w" s="T163">kalladʼu-</ts>
                  <nts id="Seg_669" n="HIAT:ip">)</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_672" n="HIAT:w" s="T164">maʔtə</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_675" n="HIAT:w" s="T165">kalla</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_678" n="HIAT:w" s="T968">dʼürbiem</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_682" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_684" n="HIAT:w" s="T166">Teinen</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_687" n="HIAT:w" s="T167">măn</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_690" n="HIAT:w" s="T168">ej</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_693" n="HIAT:w" s="T169">togonoriam</ts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_697" n="HIAT:w" s="T170">a</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_700" n="HIAT:w" s="T171">tănzi</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_703" n="HIAT:w" s="T172">dʼăbaktəriam</ts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_707" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_709" n="HIAT:w" s="T173">Xotʼ</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_712" n="HIAT:w" s="T174">ej</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_715" n="HIAT:w" s="T175">sedem</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_719" n="HIAT:w" s="T176">a</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_722" n="HIAT:w" s="T177">tararluʔpiem</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_725" n="HIAT:w" s="T178">ugandə</ts>
                  <nts id="Seg_726" n="HIAT:ip">.</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_729" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_731" n="HIAT:w" s="T179">Ugandə</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_734" n="HIAT:w" s="T180">surno</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_737" n="HIAT:w" s="T181">tăŋ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_740" n="HIAT:w" s="T182">kandəga</ts>
                  <nts id="Seg_741" n="HIAT:ip">,</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_744" n="HIAT:w" s="T183">šonuga</ts>
                  <nts id="Seg_745" n="HIAT:ip">.</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_748" n="HIAT:u" s="T184">
                  <nts id="Seg_749" n="HIAT:ip">(</nts>
                  <ts e="T185" id="Seg_751" n="HIAT:w" s="T184">M-</ts>
                  <nts id="Seg_752" n="HIAT:ip">)</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_755" n="HIAT:w" s="T185">A</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_758" n="HIAT:w" s="T186">măn</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_760" n="HIAT:ip">(</nts>
                  <ts e="T188" id="Seg_762" n="HIAT:w" s="T187">pa-</ts>
                  <nts id="Seg_763" n="HIAT:ip">)</nts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_765" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_767" n="HIAT:w" s="T188">paʔi</ts>
                  <nts id="Seg_768" n="HIAT:ip">)</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_771" n="HIAT:w" s="T189">amnolbiam</ts>
                  <nts id="Seg_772" n="HIAT:ip">.</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_775" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_777" n="HIAT:w" s="T190">Beržə</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_780" n="HIAT:w" s="T191">bar</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_783" n="HIAT:w" s="T192">šonuga</ts>
                  <nts id="Seg_784" n="HIAT:ip">.</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_787" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_789" n="HIAT:w" s="T193">Măn</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_792" n="HIAT:w" s="T194">kujnek</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_795" n="HIAT:w" s="T195">kuvas</ts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_799" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_801" n="HIAT:w" s="T196">Dĭn</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_804" n="HIAT:w" s="T197">amnobiam</ts>
                  <nts id="Seg_805" n="HIAT:ip">.</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_808" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_810" n="HIAT:w" s="T198">Măna</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_813" n="HIAT:w" s="T199">ej</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_816" n="HIAT:w" s="T200">nünörbi</ts>
                  <nts id="Seg_817" n="HIAT:ip">,</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201.tx-PKZ.1" id="Seg_820" n="HIAT:w" s="T201">a</ts>
                  <nts id="Seg_821" n="HIAT:ip">_</nts>
                  <ts e="T202" id="Seg_823" n="HIAT:w" s="T201.tx-PKZ.1">to</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_826" n="HIAT:w" s="T202">bɨ</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_829" n="HIAT:w" s="T203">nünörbi</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_832" n="HIAT:w" s="T204">bar</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_836" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_838" n="HIAT:w" s="T205">Măn</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_841" n="HIAT:w" s="T206">dʼijenə</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_844" n="HIAT:w" s="T207">mĭmbiem</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_848" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_850" n="HIAT:w" s="T208">Nüjnə</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_852" n="HIAT:ip">(</nts>
                  <ts e="T210" id="Seg_854" n="HIAT:w" s="T209">nüj-</ts>
                  <nts id="Seg_855" n="HIAT:ip">)</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_858" n="HIAT:w" s="T210">nüjbiem</ts>
                  <nts id="Seg_859" n="HIAT:ip">,</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_862" n="HIAT:w" s="T211">a</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_865" n="HIAT:w" s="T212">dĭgəttə</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_868" n="HIAT:w" s="T213">dʼorbiam</ts>
                  <nts id="Seg_869" n="HIAT:ip">.</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_872" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_874" n="HIAT:w" s="T214">Măn</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_877" n="HIAT:w" s="T215">nʼim</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_880" n="HIAT:w" s="T216">külaːmbi</ts>
                  <nts id="Seg_881" n="HIAT:ip">,</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_884" n="HIAT:w" s="T217">măn</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_887" n="HIAT:w" s="T218">ajirbiam</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_891" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_893" n="HIAT:w" s="T219">Nüke</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_896" n="HIAT:w" s="T220">i</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_899" n="HIAT:w" s="T221">büzʼe</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_902" n="HIAT:w" s="T222">amnolaʔbəʔjə</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_906" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_908" n="HIAT:w" s="T223">A</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_911" n="HIAT:w" s="T224">tăn</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_913" n="HIAT:ip">(</nts>
                  <ts e="T226" id="Seg_915" n="HIAT:w" s="T225">šojonə</ts>
                  <nts id="Seg_916" n="HIAT:ip">)</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_919" n="HIAT:w" s="T226">amnaʔ</ts>
                  <nts id="Seg_920" n="HIAT:ip">.</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_923" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_925" n="HIAT:w" s="T227">Gijen</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_928" n="HIAT:w" s="T228">ibiel</ts>
                  <nts id="Seg_929" n="HIAT:ip">?</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_932" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_934" n="HIAT:w" s="T229">Gibər</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_937" n="HIAT:w" s="T230">kandəgal</ts>
                  <nts id="Seg_938" n="HIAT:ip">?</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_941" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_943" n="HIAT:w" s="T231">Maʔtə</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_946" n="HIAT:w" s="T232">kandəgam</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_950" n="HIAT:u" s="T233">
                  <nts id="Seg_951" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_953" n="HIAT:w" s="T233">Aktʼa</ts>
                  <nts id="Seg_954" n="HIAT:ip">)</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_957" n="HIAT:w" s="T234">Aktʼa</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_960" n="HIAT:w" s="T235">naga</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_964" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_966" n="HIAT:w" s="T236">Aktʼažət</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_969" n="HIAT:w" s="T237">amnozittə</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_972" n="HIAT:w" s="T238">ej</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_975" n="HIAT:w" s="T239">jakšə</ts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_979" n="HIAT:u" s="T240">
                  <nts id="Seg_980" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_982" n="HIAT:w" s="T240">Aktʼa</ts>
                  <nts id="Seg_983" n="HIAT:ip">)</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_986" n="HIAT:w" s="T241">Aktʼa</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_989" n="HIAT:w" s="T242">ige</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_992" n="HIAT:w" s="T243">dak</ts>
                  <nts id="Seg_993" n="HIAT:ip">,</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_996" n="HIAT:w" s="T244">jakšə</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_999" n="HIAT:w" s="T245">amnozittə</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_1003" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_1005" n="HIAT:w" s="T246">Aktʼanə</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1008" n="HIAT:w" s="T247">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1011" n="HIAT:w" s="T248">iləl</ts>
                  <nts id="Seg_1012" n="HIAT:ip">,</nts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1015" n="HIAT:w" s="T249">munəjʔ</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1018" n="HIAT:w" s="T250">možna</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1021" n="HIAT:w" s="T251">izittə</ts>
                  <nts id="Seg_1022" n="HIAT:ip">,</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1025" n="HIAT:w" s="T252">ipek</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1028" n="HIAT:w" s="T253">izittə</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_1032" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_1034" n="HIAT:w" s="T254">Kola</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1037" n="HIAT:w" s="T255">izittə</ts>
                  <nts id="Seg_1038" n="HIAT:ip">,</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1041" n="HIAT:w" s="T256">uja</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1044" n="HIAT:w" s="T257">iləl</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1047" n="HIAT:w" s="T258">aktʼanə</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1050" n="HIAT:w" s="T259">üge</ts>
                  <nts id="Seg_1051" n="HIAT:ip">.</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_1054" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_1056" n="HIAT:w" s="T260">Aktʼa</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1059" n="HIAT:w" s="T261">naga</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1062" n="HIAT:w" s="T262">dak</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1065" n="HIAT:w" s="T263">ĭmbidə</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1068" n="HIAT:w" s="T264">ej</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1071" n="HIAT:w" s="T265">iləl</ts>
                  <nts id="Seg_1072" n="HIAT:ip">.</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_1075" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_1077" n="HIAT:w" s="T266">Kola</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1080" n="HIAT:w" s="T267">dʼabəsʼtə</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1083" n="HIAT:w" s="T268">kandəgal</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1087" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_1089" n="HIAT:w" s="T269">Kola</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1092" n="HIAT:w" s="T270">ej</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1094" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1096" n="HIAT:w" s="T271">dʼa-</ts>
                  <nts id="Seg_1097" n="HIAT:ip">)</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1100" n="HIAT:w" s="T272">dʼaʔpial</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1103" n="HIAT:w" s="T273">dak</ts>
                  <nts id="Seg_1104" n="HIAT:ip">,</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1106" n="HIAT:ip">(</nts>
                  <ts e="T275" id="Seg_1108" n="HIAT:w" s="T274">iʔ</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1111" n="HIAT:w" s="T275">šoʔ</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1114" n="HIAT:w" s="T276">maʔnəl</ts>
                  <nts id="Seg_1115" n="HIAT:ip">)</nts>
                  <nts id="Seg_1116" n="HIAT:ip">.</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1119" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1121" n="HIAT:w" s="T277">Kanaʔ</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1124" n="HIAT:w" s="T278">tuganbə</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1127" n="HIAT:w" s="T279">kăštəʔ</ts>
                  <nts id="Seg_1128" n="HIAT:ip">!</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1131" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1133" n="HIAT:w" s="T280">Ej</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1136" n="HIAT:w" s="T281">kalaʔi</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1139" n="HIAT:w" s="T282">dak</ts>
                  <nts id="Seg_1140" n="HIAT:ip">,</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1142" n="HIAT:ip">(</nts>
                  <ts e="T284" id="Seg_1144" n="HIAT:w" s="T283">boštə</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1147" n="HIAT:w" s="T284">iʔ</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1150" n="HIAT:w" s="T285">šoʔ</ts>
                  <nts id="Seg_1151" n="HIAT:ip">)</nts>
                  <nts id="Seg_1152" n="HIAT:ip">.</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1155" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1157" n="HIAT:w" s="T286">Kăštʼit</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1160" n="HIAT:w" s="T287">jakšəŋ</ts>
                  <nts id="Seg_1161" n="HIAT:ip">!</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1164" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1166" n="HIAT:w" s="T288">Štobɨ</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1169" n="HIAT:w" s="T289">šobiʔi</ts>
                  <nts id="Seg_1170" n="HIAT:ip">.</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1173" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1175" n="HIAT:w" s="T290">Ej</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1178" n="HIAT:w" s="T291">jakšə</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1181" n="HIAT:w" s="T292">kuza</ts>
                  <nts id="Seg_1182" n="HIAT:ip">.</nts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1185" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1187" n="HIAT:w" s="T293">Arda</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1190" n="HIAT:w" s="T294">sʼimamzʼə</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1193" n="HIAT:w" s="T295">ej</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1196" n="HIAT:w" s="T296">moliam</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1199" n="HIAT:w" s="T297">măndəsʼtə</ts>
                  <nts id="Seg_1200" n="HIAT:ip">,</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1203" n="HIAT:w" s="T298">putʼəga</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1206" n="HIAT:w" s="T299">ugandə</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1210" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1212" n="HIAT:w" s="T300">Koldʼəŋ</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1215" n="HIAT:w" s="T301">kanaʔ</ts>
                  <nts id="Seg_1216" n="HIAT:ip">.</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1219" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1221" n="HIAT:w" s="T302">Tăŋ</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1224" n="HIAT:w" s="T303">iʔ</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1227" n="HIAT:w" s="T304">mĭneʔ</ts>
                  <nts id="Seg_1228" n="HIAT:ip">.</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1231" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1233" n="HIAT:w" s="T305">Kuliom</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1236" n="HIAT:w" s="T306">bar:</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1239" n="HIAT:w" s="T307">dĭ</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1242" n="HIAT:w" s="T308">bostə</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1245" n="HIAT:w" s="T309">šonəga</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1248" n="HIAT:w" s="T310">măna</ts>
                  <nts id="Seg_1249" n="HIAT:ip">.</nts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1252" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1254" n="HIAT:w" s="T311">Boskəndə</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1257" n="HIAT:w" s="T312">bar</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1259" n="HIAT:ip">(</nts>
                  <ts e="T314" id="Seg_1261" n="HIAT:w" s="T313">manʼiʔleʔbə</ts>
                  <nts id="Seg_1262" n="HIAT:ip">)</nts>
                  <nts id="Seg_1263" n="HIAT:ip">,</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1266" n="HIAT:w" s="T314">a</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1269" n="HIAT:w" s="T315">măn</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1272" n="HIAT:w" s="T316">ej</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1275" n="HIAT:w" s="T317">kaliam</ts>
                  <nts id="Seg_1276" n="HIAT:ip">.</nts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1279" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1281" n="HIAT:w" s="T318">Dʼijegən</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1284" n="HIAT:w" s="T319">всякий</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1287" n="HIAT:w" s="T320">paʔi</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1290" n="HIAT:w" s="T321">bar</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1293" n="HIAT:w" s="T322">özerleʔbəʔjə</ts>
                  <nts id="Seg_1294" n="HIAT:ip">.</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1297" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1299" n="HIAT:w" s="T323">Kuvas</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1302" n="HIAT:w" s="T324">svʼetogəʔi</ts>
                  <nts id="Seg_1303" n="HIAT:ip">,</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1306" n="HIAT:w" s="T325">bar</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1309" n="HIAT:w" s="T326">ugandə</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1312" n="HIAT:w" s="T327">tăŋ</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1315" n="HIAT:w" s="T328">putʼəmniaʔi</ts>
                  <nts id="Seg_1316" n="HIAT:ip">.</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1319" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1321" n="HIAT:w" s="T329">Ugandə</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1324" n="HIAT:w" s="T330">nünniom</ts>
                  <nts id="Seg_1325" n="HIAT:ip">,</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1328" n="HIAT:w" s="T331">jakšə</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1331" n="HIAT:w" s="T332">putʼəmnia</ts>
                  <nts id="Seg_1332" n="HIAT:ip">,</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1335" n="HIAT:w" s="T333">ĭmbidə</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1338" n="HIAT:w" s="T334">mĭnzerleʔbə</ts>
                  <nts id="Seg_1339" n="HIAT:ip">.</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1342" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1344" n="HIAT:w" s="T335">Kurizəʔi</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1347" n="HIAT:w" s="T336">mĭngeʔi</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1350" n="HIAT:w" s="T337">dʼügən</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1353" n="HIAT:w" s="T338">bar</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1356" n="HIAT:w" s="T339">dʼüm</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1359" n="HIAT:w" s="T340">kădaʔlaʔbəʔjə</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1361" n="HIAT:ip">(</nts>
                  <ts e="T342" id="Seg_1363" n="HIAT:w" s="T341">ujuzə-</ts>
                  <nts id="Seg_1364" n="HIAT:ip">)</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1367" n="HIAT:w" s="T342">ujuzaŋdə</ts>
                  <nts id="Seg_1368" n="HIAT:ip">.</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1371" n="HIAT:u" s="T343">
                  <nts id="Seg_1372" n="HIAT:ip">(</nts>
                  <ts e="T344" id="Seg_1374" n="HIAT:w" s="T343">Человека</ts>
                  <nts id="Seg_1375" n="HIAT:ip">)</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1378" n="HIAT:w" s="T344">надо</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1381" n="HIAT:w" s="T345">ждать</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1385" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1387" n="HIAT:w" s="T346">Dĭ</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1390" n="HIAT:w" s="T347">kuzam</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1393" n="HIAT:w" s="T348">nada</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1395" n="HIAT:ip">(</nts>
                  <ts e="T350" id="Seg_1397" n="HIAT:w" s="T349">deʔsittə=</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1400" n="HIAT:w" s="T350">dĭ=</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1403" n="HIAT:w" s="T351">bĭ-</ts>
                  <nts id="Seg_1404" n="HIAT:ip">)</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1407" n="HIAT:w" s="T352">edəʔsittə</ts>
                  <nts id="Seg_1408" n="HIAT:ip">,</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1411" n="HIAT:w" s="T353">dĭ</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1414" n="HIAT:w" s="T354">bar</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1417" n="HIAT:w" s="T355">büžü</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1420" n="HIAT:w" s="T356">šoləj</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1424" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1426" n="HIAT:w" s="T357">Ara</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1429" n="HIAT:w" s="T358">detləj</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1433" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1435" n="HIAT:w" s="T359">Dĭzeŋ</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1438" n="HIAT:w" s="T360">bar</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1441" n="HIAT:w" s="T361">dĭm</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1444" n="HIAT:w" s="T362">edəʔleʔbəʔjə</ts>
                  <nts id="Seg_1445" n="HIAT:ip">.</nts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1448" n="HIAT:u" s="T363">
                  <nts id="Seg_1449" n="HIAT:ip">(</nts>
                  <ts e="T364" id="Seg_1451" n="HIAT:w" s="T363">Dĭzeŋ=</ts>
                  <nts id="Seg_1452" n="HIAT:ip">)</nts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1455" n="HIAT:w" s="T364">Dĭzeŋ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1458" n="HIAT:w" s="T365">edəʔleʔbəʔjə</ts>
                  <nts id="Seg_1459" n="HIAT:ip">.</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1462" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1464" n="HIAT:w" s="T366">Teinen</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1467" n="HIAT:w" s="T367">miʔ</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1470" n="HIAT:w" s="T368">jakšə</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1472" n="HIAT:ip">(</nts>
                  <ts e="T370" id="Seg_1474" n="HIAT:w" s="T369">dʼăbaktərləbiaʔ</ts>
                  <nts id="Seg_1475" n="HIAT:ip">)</nts>
                  <nts id="Seg_1476" n="HIAT:ip">.</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1479" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1481" n="HIAT:w" s="T370">A</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1484" n="HIAT:w" s="T371">ĭmbi</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1487" n="HIAT:w" s="T372">dʼăbaktərlaʔpileʔ</ts>
                  <nts id="Seg_1488" n="HIAT:ip">?</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_1491" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1493" n="HIAT:w" s="T373">Nan</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1496" n="HIAT:w" s="T374">kudolbibaʔ</ts>
                  <nts id="Seg_1497" n="HIAT:ip">,</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1500" n="HIAT:w" s="T375">i</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1503" n="HIAT:w" s="T376">bar</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1506" n="HIAT:w" s="T377">šiʔ</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1509" n="HIAT:w" s="T378">kudolbibaʔ</ts>
                  <nts id="Seg_1510" n="HIAT:ip">.</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1513" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_1515" n="HIAT:w" s="T379">Ineʔi</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1518" n="HIAT:w" s="T380">bar</ts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1521" n="HIAT:w" s="T381">kandəgaʔi</ts>
                  <nts id="Seg_1522" n="HIAT:ip">.</nts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_1525" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1527" n="HIAT:w" s="T382">Šide</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1530" n="HIAT:w" s="T383">ine</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1533" n="HIAT:w" s="T384">kandəga</ts>
                  <nts id="Seg_1534" n="HIAT:ip">,</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1537" n="HIAT:w" s="T385">onʼiʔ</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1540" n="HIAT:w" s="T386">ine</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1543" n="HIAT:w" s="T387">kandəga</ts>
                  <nts id="Seg_1544" n="HIAT:ip">.</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1547" n="HIAT:u" s="T388">
                  <ts e="T389" id="Seg_1549" n="HIAT:w" s="T388">Măn</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1552" n="HIAT:w" s="T389">nʼim</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1555" n="HIAT:w" s="T390">šonəga</ts>
                  <nts id="Seg_1556" n="HIAT:ip">.</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1559" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1561" n="HIAT:w" s="T391">A</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1564" n="HIAT:w" s="T392">gijen</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1567" n="HIAT:w" s="T393">ibi</ts>
                  <nts id="Seg_1568" n="HIAT:ip">—</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1571" n="HIAT:w" s="T394">kola</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1574" n="HIAT:w" s="T395">ile</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1577" n="HIAT:w" s="T396">mĭmbi</ts>
                  <nts id="Seg_1578" n="HIAT:ip">.</nts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1581" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1583" n="HIAT:w" s="T397">Ej</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1586" n="HIAT:w" s="T398">tĭmniem</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1590" n="HIAT:w" s="T399">deʔpi</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1593" n="HIAT:w" s="T400">li</ts>
                  <nts id="Seg_1594" n="HIAT:ip">,</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1597" n="HIAT:w" s="T401">ej</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1600" n="HIAT:w" s="T402">deʔpi</ts>
                  <nts id="Seg_1601" n="HIAT:ip">.</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1604" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1606" n="HIAT:w" s="T403">Deʔpi</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1608" n="HIAT:ip">(</nts>
                  <ts e="T405" id="Seg_1610" n="HIAT:w" s="T404">dak</ts>
                  <nts id="Seg_1611" n="HIAT:ip">)</nts>
                  <nts id="Seg_1612" n="HIAT:ip">,</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1615" n="HIAT:w" s="T405">pürzittə</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1618" n="HIAT:w" s="T406">nada</ts>
                  <nts id="Seg_1619" n="HIAT:ip">.</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1622" n="HIAT:u" s="T407">
                  <nts id="Seg_1623" n="HIAT:ip">(</nts>
                  <ts e="T408" id="Seg_1625" n="HIAT:w" s="T407">Dĭ</ts>
                  <nts id="Seg_1626" n="HIAT:ip">)</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1629" n="HIAT:w" s="T408">ne</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1632" n="HIAT:w" s="T409">bar</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1635" n="HIAT:w" s="T410">nanəʔzəbi</ts>
                  <nts id="Seg_1636" n="HIAT:ip">.</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1639" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1641" n="HIAT:w" s="T411">Büžü</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1643" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1645" n="HIAT:w" s="T412">büre</ts>
                  <nts id="Seg_1646" n="HIAT:ip">)</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1649" n="HIAT:w" s="T413">ešši</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1652" n="HIAT:w" s="T414">detləj</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1656" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1658" n="HIAT:w" s="T415">Nada</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1661" n="HIAT:w" s="T416">dĭm</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1663" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1665" n="HIAT:w" s="T417">nereluʔsittə</ts>
                  <nts id="Seg_1666" n="HIAT:ip">)</nts>
                  <nts id="Seg_1667" n="HIAT:ip">.</nts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1670" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1672" n="HIAT:w" s="T418">Kanzittə</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1675" n="HIAT:w" s="T419">nada</ts>
                  <nts id="Seg_1676" n="HIAT:ip">,</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1679" n="HIAT:w" s="T420">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1682" n="HIAT:w" s="T421">sadarla</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1685" n="HIAT:w" s="T422">izittə</ts>
                  <nts id="Seg_1686" n="HIAT:ip">.</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1689" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1691" n="HIAT:w" s="T423">Kamən</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1694" n="HIAT:w" s="T424">dʼala</ts>
                  <nts id="Seg_1695" n="HIAT:ip">…</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1698" n="HIAT:u" s="T426">
                  <nts id="Seg_1699" n="HIAT:ip">(</nts>
                  <nts id="Seg_1700" n="HIAT:ip">(</nts>
                  <ats e="T427" id="Seg_1701" n="HIAT:non-pho" s="T426">…</ats>
                  <nts id="Seg_1702" n="HIAT:ip">)</nts>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1707" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1709" n="HIAT:w" s="T427">Kamən</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1712" n="HIAT:w" s="T428">dʼala</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1715" n="HIAT:w" s="T429">šolaʔbə</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1717" n="HIAT:ip">(</nts>
                  <ts e="T431" id="Seg_1719" n="HIAT:w" s="T430">i</ts>
                  <nts id="Seg_1720" n="HIAT:ip">)</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1723" n="HIAT:w" s="T431">măn</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1726" n="HIAT:w" s="T432">uʔlaʔbəm</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1730" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1732" n="HIAT:w" s="T433">Kamən</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1735" n="HIAT:w" s="T434">dʼala</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1738" n="HIAT:w" s="T435">kandəga</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1741" n="HIAT:w" s="T436">măn</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1744" n="HIAT:w" s="T437">iʔbələm</ts>
                  <nts id="Seg_1745" n="HIAT:ip">.</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1748" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1750" n="HIAT:w" s="T438">Dĭ</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1753" n="HIAT:w" s="T439">kö</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1756" n="HIAT:w" s="T440">ugandə</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1759" n="HIAT:w" s="T441">šišəge</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1762" n="HIAT:w" s="T442">ibi</ts>
                  <nts id="Seg_1763" n="HIAT:ip">,</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1766" n="HIAT:w" s="T443">ugandə</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1769" n="HIAT:w" s="T444">numo</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1772" n="HIAT:w" s="T445">ibi</ts>
                  <nts id="Seg_1773" n="HIAT:ip">.</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1776" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1778" n="HIAT:w" s="T446">Nüdʼəʔi</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1781" n="HIAT:w" s="T447">bar</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1784" n="HIAT:w" s="T448">ugandə</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1786" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1788" n="HIAT:w" s="T449">no-</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1791" n="HIAT:w" s="T450">no-</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1794" n="HIAT:w" s="T451">nugo-</ts>
                  <nts id="Seg_1795" n="HIAT:ip">)</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1798" n="HIAT:w" s="T452">nugoʔi</ts>
                  <nts id="Seg_1799" n="HIAT:ip">.</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1802" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1804" n="HIAT:w" s="T453">A</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1807" n="HIAT:w" s="T454">dĭ</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1809" n="HIAT:ip">(</nts>
                  <ts e="T456" id="Seg_1811" n="HIAT:w" s="T455">pʼe</ts>
                  <nts id="Seg_1812" n="HIAT:ip">)</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1815" n="HIAT:w" s="T456">šobi</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1818" n="HIAT:w" s="T457">dak</ts>
                  <nts id="Seg_1819" n="HIAT:ip">,</nts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1822" n="HIAT:w" s="T458">ugandə</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1825" n="HIAT:w" s="T459">ejü</ts>
                  <nts id="Seg_1826" n="HIAT:ip">.</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1829" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1831" n="HIAT:w" s="T460">Dʼalaʔi</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1834" n="HIAT:w" s="T461">bar</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1837" n="HIAT:w" s="T462">numoʔi</ts>
                  <nts id="Seg_1838" n="HIAT:ip">.</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_1841" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1843" n="HIAT:w" s="T463">Nada</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1846" n="HIAT:w" s="T464">măna</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1849" n="HIAT:w" s="T465">bü</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1852" n="HIAT:w" s="T466">pʼeštə</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1855" n="HIAT:w" s="T467">nuldəsʼtə</ts>
                  <nts id="Seg_1856" n="HIAT:ip">,</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468.tx-PKZ.1" id="Seg_1859" n="HIAT:w" s="T468">a</ts>
                  <nts id="Seg_1860" n="HIAT:ip">_</nts>
                  <ts e="T469" id="Seg_1862" n="HIAT:w" s="T468.tx-PKZ.1">to</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1865" n="HIAT:w" s="T469">büzo</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1868" n="HIAT:w" s="T470">šoləj</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1872" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1874" n="HIAT:w" s="T471">Bü</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1877" n="HIAT:w" s="T472">naga</ts>
                  <nts id="Seg_1878" n="HIAT:ip">.</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1881" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1883" n="HIAT:w" s="T473">A</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1886" n="HIAT:w" s="T474">dĭ</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1889" n="HIAT:w" s="T475">šišəge</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1892" n="HIAT:w" s="T476">bü</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1895" n="HIAT:w" s="T477">ej</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1898" n="HIAT:w" s="T478">bĭtlie</ts>
                  <nts id="Seg_1899" n="HIAT:ip">.</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1902" n="HIAT:u" s="T479">
                  <nts id="Seg_1903" n="HIAT:ip">(</nts>
                  <ts e="T480" id="Seg_1905" n="HIAT:w" s="T479">Nu-</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1908" n="HIAT:w" s="T480">nuldʼ-</ts>
                  <nts id="Seg_1909" n="HIAT:ip">)</nts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1912" n="HIAT:w" s="T481">Nuldeʔ</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1915" n="HIAT:w" s="T482">bostə</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1918" n="HIAT:w" s="T483">ine</ts>
                  <nts id="Seg_1919" n="HIAT:ip">,</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1922" n="HIAT:w" s="T484">dĭ</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1925" n="HIAT:w" s="T485">padʼi</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1928" n="HIAT:w" s="T486">ej</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1931" n="HIAT:w" s="T487">nulia</ts>
                  <nts id="Seg_1932" n="HIAT:ip">.</nts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1935" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1937" n="HIAT:w" s="T488">Nada</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1940" n="HIAT:w" s="T489">dĭm</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1943" n="HIAT:w" s="T490">nuldəsʼtə</ts>
                  <nts id="Seg_1944" n="HIAT:ip">.</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1947" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1949" n="HIAT:w" s="T491">Nʼi</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1952" n="HIAT:w" s="T492">koʔbdozi</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1955" n="HIAT:w" s="T493">bar</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1958" n="HIAT:w" s="T494">özerbiʔi</ts>
                  <nts id="Seg_1959" n="HIAT:ip">.</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T502" id="Seg_1962" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1964" n="HIAT:w" s="T495">Dĭgəttə</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1967" n="HIAT:w" s="T496">nʼi</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1969" n="HIAT:ip">(</nts>
                  <ts e="T497.tx-PKZ.1" id="Seg_1971" n="HIAT:w" s="T497">mămbi</ts>
                  <nts id="Seg_1972" n="HIAT:ip">)</nts>
                  <ts e="T498" id="Seg_1974" n="HIAT:w" s="T497.tx-PKZ.1">:</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1976" n="HIAT:ip">(</nts>
                  <ts e="T499" id="Seg_1978" n="HIAT:w" s="T498">M-</ts>
                  <nts id="Seg_1979" n="HIAT:ip">)</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1982" n="HIAT:w" s="T499">Măna</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1985" n="HIAT:w" s="T500">kalal</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1988" n="HIAT:w" s="T501">tibinə</ts>
                  <nts id="Seg_1989" n="HIAT:ip">?</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1992" n="HIAT:u" s="T502">
                  <ts e="T503" id="Seg_1994" n="HIAT:w" s="T502">Kalam</ts>
                  <nts id="Seg_1995" n="HIAT:ip">.</nts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_1998" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_2000" n="HIAT:w" s="T503">Nu</ts>
                  <nts id="Seg_2001" n="HIAT:ip">,</nts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2004" n="HIAT:w" s="T504">măn</ts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2007" n="HIAT:w" s="T505">iam</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2010" n="HIAT:w" s="T506">abam</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2013" n="HIAT:w" s="T507">öʔlim</ts>
                  <nts id="Seg_2014" n="HIAT:ip">,</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2017" n="HIAT:w" s="T508">tănan</ts>
                  <nts id="Seg_2018" n="HIAT:ip">.</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_2021" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2023" n="HIAT:w" s="T509">Dĭgəttə</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2025" n="HIAT:ip">(</nts>
                  <ts e="T511" id="Seg_2027" n="HIAT:w" s="T510">iat</ts>
                  <nts id="Seg_2028" n="HIAT:ip">)</nts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2031" n="HIAT:w" s="T511">iam</ts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2034" n="HIAT:w" s="T512">abam</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2037" n="HIAT:w" s="T513">öʔlubi</ts>
                  <nts id="Seg_2038" n="HIAT:ip">,</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2041" n="HIAT:w" s="T514">dĭzeŋ</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2044" n="HIAT:w" s="T515">šobiʔi</ts>
                  <nts id="Seg_2045" n="HIAT:ip">.</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_2048" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_2050" n="HIAT:w" s="T516">Koʔbdon</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2053" n="HIAT:w" s="T517">iazi</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2056" n="HIAT:w" s="T518">abazi</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_2059" n="HIAT:w" s="T519">dʼăbaktərluʔpiʔi</ts>
                  <nts id="Seg_2060" n="HIAT:ip">.</nts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2063" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_2065" n="HIAT:w" s="T520">Koʔbdol</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2067" n="HIAT:ip">(</nts>
                  <ts e="T522" id="Seg_2069" n="HIAT:w" s="T521">mĭbiʔ-</ts>
                  <nts id="Seg_2070" n="HIAT:ip">)</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2073" n="HIAT:w" s="T522">mĭbileʔ</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2075" n="HIAT:ip">(</nts>
                  <ts e="T524" id="Seg_2077" n="HIAT:w" s="T523">măn=</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_2080" n="HIAT:w" s="T524">nʼi-</ts>
                  <nts id="Seg_2081" n="HIAT:ip">)</nts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2084" n="HIAT:w" s="T525">măn</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2087" n="HIAT:w" s="T526">nʼinə</ts>
                  <nts id="Seg_2088" n="HIAT:ip">?</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_2091" n="HIAT:u" s="T527">
                  <nts id="Seg_2092" n="HIAT:ip">(</nts>
                  <ts e="T528" id="Seg_2094" n="HIAT:w" s="T527">Nu-</ts>
                  <nts id="Seg_2095" n="HIAT:ip">)</nts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2098" n="HIAT:w" s="T528">Mĭbibeʔ</ts>
                  <nts id="Seg_2099" n="HIAT:ip">.</nts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T538" id="Seg_2102" n="HIAT:u" s="T529">
                  <nts id="Seg_2103" n="HIAT:ip">(</nts>
                  <ts e="T530" id="Seg_2105" n="HIAT:w" s="T529">Dĭb-</ts>
                  <nts id="Seg_2106" n="HIAT:ip">)</nts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2109" n="HIAT:w" s="T530">Dĭgəttə</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2112" n="HIAT:w" s="T531">koʔbdom</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2115" n="HIAT:w" s="T532">surarbiʔi:</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2118" n="HIAT:w" s="T533">Tăn</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2121" n="HIAT:w" s="T534">kalal</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2124" n="HIAT:w" s="T535">miʔ</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2127" n="HIAT:w" s="T536">nʼinə</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2130" n="HIAT:w" s="T537">tibinə</ts>
                  <nts id="Seg_2131" n="HIAT:ip">?</nts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2134" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_2136" n="HIAT:w" s="T538">Kalam</ts>
                  <nts id="Seg_2137" n="HIAT:ip">.</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_2140" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2142" n="HIAT:w" s="T539">Dĭgəttə</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2144" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_2146" n="HIAT:w" s="T540">monoʔkobiʔi</ts>
                  <nts id="Seg_2147" n="HIAT:ip">)</nts>
                  <nts id="Seg_2148" n="HIAT:ip">.</nts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2151" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_2153" n="HIAT:w" s="T541">Ara</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2156" n="HIAT:w" s="T542">deʔpiʔi</ts>
                  <nts id="Seg_2157" n="HIAT:ip">,</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2160" n="HIAT:w" s="T543">ipek</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2163" n="HIAT:w" s="T544">deʔpiʔi</ts>
                  <nts id="Seg_2164" n="HIAT:ip">,</nts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2167" n="HIAT:w" s="T545">uja</ts>
                  <nts id="Seg_2168" n="HIAT:ip">.</nts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_2171" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2173" n="HIAT:w" s="T546">Iʔgö</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2176" n="HIAT:w" s="T547">ibi</ts>
                  <nts id="Seg_2177" n="HIAT:ip">.</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2180" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2182" n="HIAT:w" s="T548">Dĭgəttə</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2185" n="HIAT:w" s="T549">il</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2188" n="HIAT:w" s="T550">šobiʔi</ts>
                  <nts id="Seg_2189" n="HIAT:ip">.</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_2192" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2194" n="HIAT:w" s="T551">Ara</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2197" n="HIAT:w" s="T552">bĭʔpiʔi</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2200" n="HIAT:w" s="T553">bar</ts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_2204" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_2206" n="HIAT:w" s="T554">Dĭgəttə</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2208" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_2210" n="HIAT:w" s="T555">nʼe-</ts>
                  <nts id="Seg_2211" n="HIAT:ip">)</nts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2214" n="HIAT:w" s="T556">несколько</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2217" n="HIAT:w" s="T557">dʼala</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2220" n="HIAT:w" s="T558">kambi</ts>
                  <nts id="Seg_2221" n="HIAT:ip">.</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2224" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_2226" n="HIAT:w" s="T559">Dĭzeŋ</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2229" n="HIAT:w" s="T560">svʼetogəʔjə</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2232" n="HIAT:w" s="T561">šerbiʔi</ts>
                  <nts id="Seg_2233" n="HIAT:ip">.</nts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2236" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2238" n="HIAT:w" s="T562">I</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2241" n="HIAT:w" s="T563">tʼegermaʔnə</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2244" n="HIAT:w" s="T564">kambiʔi</ts>
                  <nts id="Seg_2245" n="HIAT:ip">.</nts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_2248" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2250" n="HIAT:w" s="T565">Dĭn</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2253" n="HIAT:w" s="T566">abəs</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2255" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_2257" n="HIAT:w" s="T567">dĭzem</ts>
                  <nts id="Seg_2258" n="HIAT:ip">)</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2261" n="HIAT:w" s="T568">kudaj</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569.tx-PKZ.1" id="Seg_2264" n="HIAT:w" s="T569">numan</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2267" n="HIAT:w" s="T569.tx-PKZ.1">üzəbi</ts>
                  <nts id="Seg_2268" n="HIAT:ip">.</nts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2271" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_2273" n="HIAT:w" s="T570">Dĭgəttə</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2275" n="HIAT:ip">(</nts>
                  <ts e="T572" id="Seg_2277" n="HIAT:w" s="T571">vʼe-</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2280" n="HIAT:w" s="T572">vʼenogə-</ts>
                  <nts id="Seg_2281" n="HIAT:ip">)</nts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2284" n="HIAT:w" s="T573">vʼenogəʔi</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2287" n="HIAT:w" s="T574">šerbiʔi</ts>
                  <nts id="Seg_2288" n="HIAT:ip">.</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2291" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2293" n="HIAT:w" s="T575">Dĭgəttə</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2296" n="HIAT:w" s="T576">maːndə</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2299" n="HIAT:w" s="T577">šobiʔi</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2303" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_2305" n="HIAT:w" s="T578">Tože</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2308" n="HIAT:w" s="T579">bar</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2311" n="HIAT:w" s="T580">ara</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2314" n="HIAT:w" s="T581">bĭʔpiʔi</ts>
                  <nts id="Seg_2315" n="HIAT:ip">.</nts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2318" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2320" n="HIAT:w" s="T582">Koʔbdon</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2323" n="HIAT:w" s="T583">tugandə</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2326" n="HIAT:w" s="T584">šobiʔi</ts>
                  <nts id="Seg_2327" n="HIAT:ip">.</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2330" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2332" n="HIAT:w" s="T585">I</ts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2335" n="HIAT:w" s="T586">nʼin</ts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2338" n="HIAT:w" s="T587">koʔbdo</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2341" n="HIAT:w" s="T588">šobiʔi</ts>
                  <nts id="Seg_2342" n="HIAT:ip">.</nts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2345" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2347" n="HIAT:w" s="T589">Iʔgö</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2350" n="HIAT:w" s="T590">il</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2353" n="HIAT:w" s="T591">ibiʔi</ts>
                  <nts id="Seg_2354" n="HIAT:ip">.</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2357" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2359" n="HIAT:w" s="T592">Ara</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2362" n="HIAT:w" s="T593">iʔgö</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2365" n="HIAT:w" s="T594">ibi</ts>
                  <nts id="Seg_2366" n="HIAT:ip">.</nts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2369" n="HIAT:u" s="T595">
                  <nts id="Seg_2370" n="HIAT:ip">(</nts>
                  <ts e="T596" id="Seg_2372" n="HIAT:w" s="T595">Stolgən</ts>
                  <nts id="Seg_2373" n="HIAT:ip">)</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2376" n="HIAT:w" s="T596">ugandə</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2379" n="HIAT:w" s="T597">iʔgö</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2382" n="HIAT:w" s="T598">ĭmbi</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2384" n="HIAT:ip">(</nts>
                  <ts e="T600" id="Seg_2386" n="HIAT:w" s="T599">iʔb-</ts>
                  <nts id="Seg_2387" n="HIAT:ip">)</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2390" n="HIAT:w" s="T600">iʔbolaʔbə</ts>
                  <nts id="Seg_2391" n="HIAT:ip">.</nts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2394" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2396" n="HIAT:w" s="T601">Kamən</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2399" n="HIAT:w" s="T602">koʔbdo</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2402" n="HIAT:w" s="T603">monoʔkobiʔi</ts>
                  <nts id="Seg_2403" n="HIAT:ip">,</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2406" n="HIAT:w" s="T604">iat</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2409" n="HIAT:w" s="T605">abat</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2412" n="HIAT:w" s="T606">mămbiʔi:</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2415" n="HIAT:w" s="T607">Dĭn</ts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2418" n="HIAT:w" s="T608">oldʼat</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2421" n="HIAT:w" s="T609">naga</ts>
                  <nts id="Seg_2422" n="HIAT:ip">.</nts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_2425" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2427" n="HIAT:w" s="T610">Nada</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2430" n="HIAT:w" s="T611">bătʼinkăʔi</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2433" n="HIAT:w" s="T612">izittə</ts>
                  <nts id="Seg_2434" n="HIAT:ip">,</nts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2437" n="HIAT:w" s="T613">nada</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2440" n="HIAT:w" s="T614">palʼto</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2443" n="HIAT:w" s="T615">izittə</ts>
                  <nts id="Seg_2444" n="HIAT:ip">,</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2447" n="HIAT:w" s="T616">plat</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2450" n="HIAT:w" s="T617">izittə</ts>
                  <nts id="Seg_2451" n="HIAT:ip">.</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2454" n="HIAT:u" s="T618">
                  <ts e="T619" id="Seg_2456" n="HIAT:w" s="T618">Oldʼat</ts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2459" n="HIAT:w" s="T619">amga</ts>
                  <nts id="Seg_2460" n="HIAT:ip">,</nts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2463" n="HIAT:w" s="T620">kujnektə</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2466" n="HIAT:w" s="T621">naga</ts>
                  <nts id="Seg_2467" n="HIAT:ip">.</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_2470" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2472" n="HIAT:w" s="T622">Dĭgəttə</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2474" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2476" n="HIAT:w" s="T623">dĭʔ-</ts>
                  <nts id="Seg_2477" n="HIAT:ip">)</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2480" n="HIAT:w" s="T624">dĭzeŋ</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2483" n="HIAT:w" s="T625">aktʼam</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2486" n="HIAT:w" s="T626">ibiʔi</ts>
                  <nts id="Seg_2487" n="HIAT:ip">.</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2490" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_2492" n="HIAT:w" s="T627">Šide</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2495" n="HIAT:w" s="T628">bʼeʔ</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2498" n="HIAT:w" s="T629">sumna</ts>
                  <nts id="Seg_2499" n="HIAT:ip">.</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2502" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2504" n="HIAT:w" s="T630">Dĭgəttə</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2507" n="HIAT:w" s="T631">iat</ts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2510" n="HIAT:w" s="T632">aban</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2513" n="HIAT:w" s="T633">dĭʔnə</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2516" n="HIAT:w" s="T634">mĭbiʔi</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2519" n="HIAT:w" s="T635">tüžöj</ts>
                  <nts id="Seg_2520" n="HIAT:ip">.</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2523" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2525" n="HIAT:w" s="T636">Tugazaŋdən</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2528" n="HIAT:w" s="T637">mĭbiʔi</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2531" n="HIAT:w" s="T638">ular</ts>
                  <nts id="Seg_2532" n="HIAT:ip">,</nts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2535" n="HIAT:w" s="T639">mĭbiʔi</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2538" n="HIAT:w" s="T640">ineʔi</ts>
                  <nts id="Seg_2539" n="HIAT:ip">.</nts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2542" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2544" n="HIAT:w" s="T641">Aktʼa</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2547" n="HIAT:w" s="T642">mĭbiʔi</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2550" n="HIAT:w" s="T643">ugandə</ts>
                  <nts id="Seg_2551" n="HIAT:ip">,</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2554" n="HIAT:w" s="T644">iʔgö</ts>
                  <nts id="Seg_2555" n="HIAT:ip">.</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2558" n="HIAT:u" s="T645">
                  <nts id="Seg_2559" n="HIAT:ip">(</nts>
                  <ts e="T646" id="Seg_2561" n="HIAT:w" s="T645">I-</ts>
                  <nts id="Seg_2562" n="HIAT:ip">)</nts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2565" n="HIAT:w" s="T646">Il</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2568" n="HIAT:w" s="T647">iʔgö</ts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2571" n="HIAT:w" s="T648">ibiʔi</ts>
                  <nts id="Seg_2572" n="HIAT:ip">.</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T653" id="Seg_2575" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2577" n="HIAT:w" s="T649">Dĭgəttə</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2580" n="HIAT:w" s="T650">dĭzeŋ</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2583" n="HIAT:w" s="T651">kondʼo</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2586" n="HIAT:w" s="T652">amnobiʔi</ts>
                  <nts id="Seg_2587" n="HIAT:ip">.</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2590" n="HIAT:u" s="T653">
                  <ts e="T654" id="Seg_2592" n="HIAT:w" s="T653">Kagat</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2594" n="HIAT:ip">(</nts>
                  <ts e="T655" id="Seg_2596" n="HIAT:w" s="T654">išo</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2599" n="HIAT:w" s="T655">serbi</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2602" n="HIAT:w" s="T656">da</ts>
                  <nts id="Seg_2603" n="HIAT:ip">)</nts>
                  <nts id="Seg_2604" n="HIAT:ip">.</nts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2607" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2609" n="HIAT:w" s="T657">Bazoʔ</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2611" n="HIAT:ip">(</nts>
                  <ts e="T659" id="Seg_2613" n="HIAT:w" s="T658">dăre</ts>
                  <nts id="Seg_2614" n="HIAT:ip">)</nts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2617" n="HIAT:w" s="T659">monoʔkobi</ts>
                  <nts id="Seg_2618" n="HIAT:ip">.</nts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2621" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2623" n="HIAT:w" s="T660">Dĭgəttə</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2626" n="HIAT:w" s="T661">dĭ</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_2629" n="HIAT:w" s="T662">kalla</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2632" n="HIAT:w" s="T969">dʼürbi</ts>
                  <nts id="Seg_2633" n="HIAT:ip">,</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2636" n="HIAT:w" s="T663">dĭʔnə</ts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2639" n="HIAT:w" s="T664">abat</ts>
                  <nts id="Seg_2640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2642" n="HIAT:w" s="T665">mĭbi</ts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2645" n="HIAT:w" s="T666">bar</ts>
                  <nts id="Seg_2646" n="HIAT:ip">.</nts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2649" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_2651" n="HIAT:w" s="T667">Bar</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2654" n="HIAT:w" s="T668">ĭmbi</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2657" n="HIAT:w" s="T669">mĭbi</ts>
                  <nts id="Seg_2658" n="HIAT:ip">,</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2661" n="HIAT:w" s="T670">ine</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2664" n="HIAT:w" s="T671">mĭbi</ts>
                  <nts id="Seg_2665" n="HIAT:ip">,</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2668" n="HIAT:w" s="T672">ular</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2671" n="HIAT:w" s="T673">mĭbi</ts>
                  <nts id="Seg_2672" n="HIAT:ip">,</nts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2675" n="HIAT:w" s="T674">tüžöjʔi</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2678" n="HIAT:w" s="T675">mĭbi</ts>
                  <nts id="Seg_2679" n="HIAT:ip">.</nts>
                  <nts id="Seg_2680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2682" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2684" n="HIAT:w" s="T676">Ipek</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2687" n="HIAT:w" s="T677">mĭbi</ts>
                  <nts id="Seg_2688" n="HIAT:ip">,</nts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2690" n="HIAT:ip">(</nts>
                  <ts e="T679" id="Seg_2692" n="HIAT:w" s="T678">tu-</ts>
                  <nts id="Seg_2693" n="HIAT:ip">)</nts>
                  <nts id="Seg_2694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2696" n="HIAT:w" s="T679">tura</ts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2699" n="HIAT:w" s="T680">nuldəbi</ts>
                  <nts id="Seg_2700" n="HIAT:ip">.</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2703" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2705" n="HIAT:w" s="T681">Dĭzeŋ</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2707" n="HIAT:ip">(</nts>
                  <ts e="T683" id="Seg_2709" n="HIAT:w" s="T682">kan-</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2712" n="HIAT:w" s="T683">kanzə-</ts>
                  <nts id="Seg_2713" n="HIAT:ip">)</nts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2716" n="HIAT:w" s="T684">kambiʔi</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2719" n="HIAT:w" s="T685">венчаться</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2722" n="HIAT:w" s="T686">ineʔizi</ts>
                  <nts id="Seg_2723" n="HIAT:ip">.</nts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_2726" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2728" n="HIAT:w" s="T687">Koʔbdo</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2731" n="HIAT:w" s="T688">bostə</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2734" n="HIAT:w" s="T689">kazak</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2737" n="HIAT:w" s="T690">iat</ts>
                  <nts id="Seg_2738" n="HIAT:ip">,</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2741" n="HIAT:w" s="T691">a</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2744" n="HIAT:w" s="T692">nʼi</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2747" n="HIAT:w" s="T693">bostə</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2750" n="HIAT:w" s="T694">kazak</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2753" n="HIAT:w" s="T695">iat</ts>
                  <nts id="Seg_2754" n="HIAT:ip">,</nts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2757" n="HIAT:w" s="T696">kazak</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2760" n="HIAT:w" s="T697">abat</ts>
                  <nts id="Seg_2761" n="HIAT:ip">.</nts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2764" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_2766" n="HIAT:w" s="T698">Dibər</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2769" n="HIAT:w" s="T699">kambiʔi</ts>
                  <nts id="Seg_2770" n="HIAT:ip">,</nts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2773" n="HIAT:w" s="T700">dĭgəttə</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2776" n="HIAT:w" s="T701">šobiʔi</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2779" n="HIAT:w" s="T702">maːʔndə</ts>
                  <nts id="Seg_2780" n="HIAT:ip">.</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T713" id="Seg_2783" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2785" n="HIAT:w" s="T703">A</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2788" n="HIAT:w" s="T704">dĭgəttə</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2791" n="HIAT:w" s="T705">koʔbdon</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2794" n="HIAT:w" s="T706">kazak</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2797" n="HIAT:w" s="T707">abat</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2799" n="HIAT:ip">(</nts>
                  <ts e="T709" id="Seg_2801" n="HIAT:w" s="T708">odʼa-</ts>
                  <nts id="Seg_2802" n="HIAT:ip">)</nts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2805" n="HIAT:w" s="T709">oldʼa</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2808" n="HIAT:w" s="T710">deʔpi</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2811" n="HIAT:w" s="T711">ящик</ts>
                  <nts id="Seg_2812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2814" n="HIAT:w" s="T712">bar</ts>
                  <nts id="Seg_2815" n="HIAT:ip">.</nts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_2818" n="HIAT:u" s="T713">
                  <ts e="T714" id="Seg_2820" n="HIAT:w" s="T713">Păduškăʔi</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2823" n="HIAT:w" s="T714">deʔpi</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2826" n="HIAT:w" s="T715">bar</ts>
                  <nts id="Seg_2827" n="HIAT:ip">.</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2830" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2832" n="HIAT:w" s="T716">Pʼerină</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2835" n="HIAT:w" s="T717">deʔpi</ts>
                  <nts id="Seg_2836" n="HIAT:ip">.</nts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2839" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2841" n="HIAT:w" s="T718">Dĭgəttə</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2844" n="HIAT:w" s="T719">šobiʔi</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2847" n="HIAT:w" s="T720">tʼegermaʔtə</ts>
                  <nts id="Seg_2848" n="HIAT:ip">.</nts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2851" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2853" n="HIAT:w" s="T721">Dĭzeŋ</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2856" n="HIAT:w" s="T722">amnobiʔi</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2858" n="HIAT:ip">(</nts>
                  <ts e="T724" id="Seg_2860" n="HIAT:w" s="T723">stozə-</ts>
                  <nts id="Seg_2861" n="HIAT:ip">)</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2864" n="HIAT:w" s="T724">stoldə</ts>
                  <nts id="Seg_2865" n="HIAT:ip">.</nts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2868" n="HIAT:u" s="T725">
                  <ts e="T727" id="Seg_2870" n="HIAT:w" s="T725">Dĭgəttə</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2873" n="HIAT:w" s="T727">nʼin</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2876" n="HIAT:w" s="T728">kujnektə</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2879" n="HIAT:w" s="T729">kuvas</ts>
                  <nts id="Seg_2880" n="HIAT:ip">,</nts>
                  <nts id="Seg_2881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2883" n="HIAT:w" s="T730">toʔbdə</ts>
                  <nts id="Seg_2884" n="HIAT:ip">.</nts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2887" n="HIAT:u" s="T731">
                  <ts e="T732" id="Seg_2889" n="HIAT:w" s="T731">Koʔbdon</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2892" n="HIAT:w" s="T732">oldʼat</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2895" n="HIAT:w" s="T733">tože</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2898" n="HIAT:w" s="T734">kuvas</ts>
                  <nts id="Seg_2899" n="HIAT:ip">.</nts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2902" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2904" n="HIAT:w" s="T735">Toʔbdə</ts>
                  <nts id="Seg_2905" n="HIAT:ip">.</nts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2908" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_2910" n="HIAT:w" s="T736">Ulut</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2913" n="HIAT:w" s="T737">bar</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2916" n="HIAT:w" s="T738">kuvas</ts>
                  <nts id="Seg_2917" n="HIAT:ip">,</nts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2920" n="HIAT:w" s="T739">vʼenok</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2923" n="HIAT:w" s="T740">šerbiʔi</ts>
                  <nts id="Seg_2924" n="HIAT:ip">.</nts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2927" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2929" n="HIAT:w" s="T741">Dĭgəttə</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2932" n="HIAT:w" s="T742">dĭʔnə</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2934" n="HIAT:ip">(</nts>
                  <ts e="T744" id="Seg_2936" n="HIAT:w" s="T743">o-</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2939" n="HIAT:w" s="T744">o-</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2941" n="HIAT:ip">)</nts>
                  <nts id="Seg_2942" n="HIAT:ip">…</nts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T751" id="Seg_2945" n="HIAT:u" s="T746">
                  <ts e="T747" id="Seg_2947" n="HIAT:w" s="T746">Dĭgəttə</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2950" n="HIAT:w" s="T747">dĭʔnə</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2953" n="HIAT:w" s="T748">šide</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2956" n="HIAT:w" s="T749">kosa</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2959" n="HIAT:w" s="T750">kürbiʔi</ts>
                  <nts id="Seg_2960" n="HIAT:ip">.</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_2963" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2965" n="HIAT:w" s="T751">Dĭgəttə</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2968" n="HIAT:w" s="T752">sarbiʔi</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2971" n="HIAT:w" s="T753">ulut</ts>
                  <nts id="Seg_2972" n="HIAT:ip">.</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2975" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_2977" n="HIAT:w" s="T754">Kamən</ts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2980" n="HIAT:w" s="T755">dĭ</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2983" n="HIAT:w" s="T756">koʔbdo</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2986" n="HIAT:w" s="T757">ibi</ts>
                  <nts id="Seg_2987" n="HIAT:ip">,</nts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2990" n="HIAT:w" s="T758">onʼiʔ</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2992" n="HIAT:ip">(</nts>
                  <ts e="T760" id="Seg_2994" n="HIAT:w" s="T759">koza-</ts>
                  <nts id="Seg_2995" n="HIAT:ip">)</nts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2998" n="HIAT:w" s="T760">kosa</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3001" n="HIAT:w" s="T761">ibi</ts>
                  <nts id="Seg_3002" n="HIAT:ip">.</nts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_3005" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3007" n="HIAT:w" s="T762">Dĭgəttə</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3009" n="HIAT:ip">(</nts>
                  <ts e="T764" id="Seg_3011" n="HIAT:w" s="T763">s-</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3014" n="HIAT:w" s="T764">kazak</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3017" n="HIAT:w" s="T765">iat=</ts>
                  <nts id="Seg_3018" n="HIAT:ip">)</nts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3021" n="HIAT:w" s="T766">dĭ</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3024" n="HIAT:w" s="T767">koʔbdon</ts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3027" n="HIAT:w" s="T768">kazak</ts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3030" n="HIAT:w" s="T769">iat</ts>
                  <nts id="Seg_3031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3033" n="HIAT:w" s="T770">i</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3036" n="HIAT:w" s="T771">nʼin</ts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3039" n="HIAT:w" s="T772">kazak</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3042" n="HIAT:w" s="T773">iat</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3045" n="HIAT:w" s="T774">dĭʔnə</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_3048" n="HIAT:w" s="T775">šide</ts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3051" n="HIAT:w" s="T776">kürbiʔi</ts>
                  <nts id="Seg_3052" n="HIAT:ip">.</nts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_3055" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_3057" n="HIAT:w" s="T777">I</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3060" n="HIAT:w" s="T778">sarbiʔi</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_3063" n="HIAT:w" s="T779">ulut</ts>
                  <nts id="Seg_3064" n="HIAT:ip">.</nts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_3067" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_3069" n="HIAT:w" s="T780">Dĭzeŋdə</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3072" n="HIAT:w" s="T781">nʼi</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3075" n="HIAT:w" s="T782">koʔbdom</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3078" n="HIAT:w" s="T783">kazak</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3081" n="HIAT:w" s="T784">abat</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3083" n="HIAT:ip">(</nts>
                  <ts e="T786" id="Seg_3085" n="HIAT:w" s="T785">ku-</ts>
                  <nts id="Seg_3086" n="HIAT:ip">)</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3089" n="HIAT:w" s="T786">kumbi</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3092" n="HIAT:w" s="T787">turanə</ts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3096" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_3098" n="HIAT:w" s="T788">Dĭn</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3101" n="HIAT:w" s="T789">šindidə</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3104" n="HIAT:w" s="T790">ej</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3107" n="HIAT:w" s="T791">ibi</ts>
                  <nts id="Seg_3108" n="HIAT:ip">.</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_3111" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3113" n="HIAT:w" s="T792">Iʔbəbi</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3115" n="HIAT:ip">(</nts>
                  <ts e="T794" id="Seg_3117" n="HIAT:w" s="T793">ʔi</ts>
                  <nts id="Seg_3118" n="HIAT:ip">)</nts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3121" n="HIAT:w" s="T794">kunolzittə</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3124" n="HIAT:w" s="T795">i</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3127" n="HIAT:w" s="T796">kamroluʔpiʔi</ts>
                  <nts id="Seg_3128" n="HIAT:ip">.</nts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_3131" n="HIAT:u" s="T797">
                  <nts id="Seg_3132" n="HIAT:ip">(</nts>
                  <ts e="T798" id="Seg_3134" n="HIAT:w" s="T797">Tugazaŋ-</ts>
                  <nts id="Seg_3135" n="HIAT:ip">)</nts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3138" n="HIAT:w" s="T798">Tuganzaŋdə</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3141" n="HIAT:w" s="T799">dĭzeŋdə</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_3144" n="HIAT:w" s="T800">mămbiʔi:</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3147" n="HIAT:w" s="T801">Jakšə</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3149" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_3151" n="HIAT:w" s="T802">amnolaʔ=</ts>
                  <nts id="Seg_3152" n="HIAT:ip">)</nts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3155" n="HIAT:w" s="T803">amnogaʔ</ts>
                  <nts id="Seg_3156" n="HIAT:ip">.</nts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T806" id="Seg_3159" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_3161" n="HIAT:w" s="T804">Iʔ</ts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3164" n="HIAT:w" s="T805">dʼabərogaʔ</ts>
                  <nts id="Seg_3165" n="HIAT:ip">.</nts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3168" n="HIAT:u" s="T806">
                  <nts id="Seg_3169" n="HIAT:ip">(</nts>
                  <ts e="T807" id="Seg_3171" n="HIAT:w" s="T806">Iʔ</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3174" n="HIAT:w" s="T807">kundonz-</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3177" n="HIAT:w" s="T808">iʔ</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3180" n="HIAT:w" s="T809">kudonzlaʔ-</ts>
                  <nts id="Seg_3181" n="HIAT:ip">)</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3184" n="HIAT:w" s="T810">Iʔ</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3187" n="HIAT:w" s="T811">kudonzaʔ</ts>
                  <nts id="Seg_3188" n="HIAT:ip">.</nts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3191" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3193" n="HIAT:w" s="T812">Puskaj</ts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3195" n="HIAT:ip">(</nts>
                  <ts e="T814" id="Seg_3197" n="HIAT:w" s="T813">esseŋgə-</ts>
                  <nts id="Seg_3198" n="HIAT:ip">)</nts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3201" n="HIAT:w" s="T814">esseŋdə</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3204" n="HIAT:w" s="T815">iʔgö</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3207" n="HIAT:w" s="T816">molaj</ts>
                  <nts id="Seg_3208" n="HIAT:ip">,</nts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3211" n="HIAT:w" s="T817">nʼizeŋ</ts>
                  <nts id="Seg_3212" n="HIAT:ip">,</nts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3215" n="HIAT:w" s="T818">koʔbsaŋ</ts>
                  <nts id="Seg_3216" n="HIAT:ip">.</nts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3219" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3221" n="HIAT:w" s="T819">Dĭgəttə</ts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3224" n="HIAT:w" s="T820">dĭzeŋ</ts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3227" n="HIAT:w" s="T821">abəs</ts>
                  <nts id="Seg_3228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3230" n="HIAT:w" s="T822">kăštəbiʔi</ts>
                  <nts id="Seg_3231" n="HIAT:ip">.</nts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3234" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3236" n="HIAT:w" s="T823">Dĭ</ts>
                  <nts id="Seg_3237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3239" n="HIAT:w" s="T824">ugandə</ts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3242" n="HIAT:w" s="T825">iʔgö</ts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3245" n="HIAT:w" s="T826">ara</ts>
                  <nts id="Seg_3246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3248" n="HIAT:w" s="T827">bĭʔpi</ts>
                  <nts id="Seg_3249" n="HIAT:ip">.</nts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_3252" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3254" n="HIAT:w" s="T828">I</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3257" n="HIAT:w" s="T829">saʔməluʔpi</ts>
                  <nts id="Seg_3258" n="HIAT:ip">.</nts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_3261" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_3263" n="HIAT:w" s="T830">Dĭm</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3266" n="HIAT:w" s="T831">kunnaːlbiʔi</ts>
                  <nts id="Seg_3267" n="HIAT:ip">.</nts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T836" id="Seg_3270" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_3272" n="HIAT:w" s="T832">Dĭzeŋ</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3275" n="HIAT:w" s="T833">ugandə</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3278" n="HIAT:w" s="T834">ara</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3281" n="HIAT:w" s="T835">bĭʔpiʔi</ts>
                  <nts id="Seg_3282" n="HIAT:ip">.</nts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_3285" n="HIAT:u" s="T836">
                  <ts e="T837" id="Seg_3287" n="HIAT:w" s="T836">Kondʼo</ts>
                  <nts id="Seg_3288" n="HIAT:ip">,</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3291" n="HIAT:w" s="T837">muktuʔ</ts>
                  <nts id="Seg_3292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3294" n="HIAT:w" s="T838">dʼala</ts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3297" n="HIAT:w" s="T839">bĭʔpiʔi</ts>
                  <nts id="Seg_3298" n="HIAT:ip">.</nts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T844" id="Seg_3301" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_3303" n="HIAT:w" s="T840">Dĭgəttə</ts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3306" n="HIAT:w" s="T841">dĭzeŋ</ts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3309" n="HIAT:w" s="T842">bar</ts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3312" n="HIAT:w" s="T843">amnobiʔi</ts>
                  <nts id="Seg_3313" n="HIAT:ip">.</nts>
                  <nts id="Seg_3314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3316" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_3318" n="HIAT:w" s="T844">Dĭzeŋ</ts>
                  <nts id="Seg_3319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3321" n="HIAT:w" s="T845">bar</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3324" n="HIAT:w" s="T846">esseŋdə</ts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3327" n="HIAT:w" s="T847">ibiʔi</ts>
                  <nts id="Seg_3328" n="HIAT:ip">.</nts>
                  <nts id="Seg_3329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3331" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3333" n="HIAT:w" s="T848">Nʼizeŋ</ts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3336" n="HIAT:w" s="T849">ibiʔi</ts>
                  <nts id="Seg_3337" n="HIAT:ip">,</nts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3340" n="HIAT:w" s="T850">koʔbtaŋ</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3343" n="HIAT:w" s="T851">ibiʔi</ts>
                  <nts id="Seg_3344" n="HIAT:ip">.</nts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_3347" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3349" n="HIAT:w" s="T852">Koʔpsaŋ</ts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3351" n="HIAT:ip">(</nts>
                  <nts id="Seg_3352" n="HIAT:ip">(</nts>
                  <ats e="T854" id="Seg_3353" n="HIAT:non-pho" s="T853">DMG</ats>
                  <nts id="Seg_3354" n="HIAT:ip">)</nts>
                  <nts id="Seg_3355" n="HIAT:ip">)</nts>
                  <nts id="Seg_3356" n="HIAT:ip">.</nts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T855" id="Seg_3359" n="HIAT:u" s="T854">
                  <nts id="Seg_3360" n="HIAT:ip">(</nts>
                  <nts id="Seg_3361" n="HIAT:ip">(</nts>
                  <ats e="T855" id="Seg_3362" n="HIAT:non-pho" s="T854">…</ats>
                  <nts id="Seg_3363" n="HIAT:ip">)</nts>
                  <nts id="Seg_3364" n="HIAT:ip">)</nts>
                  <nts id="Seg_3365" n="HIAT:ip">.</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3368" n="HIAT:u" s="T855">
                  <nts id="Seg_3369" n="HIAT:ip">(</nts>
                  <ts e="T857" id="Seg_3371" n="HIAT:w" s="T855">Măn</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3374" n="HIAT:w" s="T857">nʼim</ts>
                  <nts id="Seg_3375" n="HIAT:ip">)</nts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3378" n="HIAT:w" s="T858">taldʼen</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3381" n="HIAT:w" s="T859">kambi</ts>
                  <nts id="Seg_3382" n="HIAT:ip">,</nts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3385" n="HIAT:w" s="T860">ara</ts>
                  <nts id="Seg_3386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3388" n="HIAT:w" s="T861">bĭʔpi</ts>
                  <nts id="Seg_3389" n="HIAT:ip">.</nts>
                  <nts id="Seg_3390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3392" n="HIAT:u" s="T862">
                  <ts e="T863" id="Seg_3394" n="HIAT:w" s="T862">Ugandə</ts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3397" n="HIAT:w" s="T863">kondʼo</ts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3400" n="HIAT:w" s="T864">ej</ts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3403" n="HIAT:w" s="T865">šobi</ts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3406" n="HIAT:w" s="T866">maʔnə</ts>
                  <nts id="Seg_3407" n="HIAT:ip">.</nts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3410" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3412" n="HIAT:w" s="T867">Dĭgəttə</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3415" n="HIAT:w" s="T868">šobi</ts>
                  <nts id="Seg_3416" n="HIAT:ip">,</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3419" n="HIAT:w" s="T869">kunolluʔpi</ts>
                  <nts id="Seg_3420" n="HIAT:ip">.</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3423" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3425" n="HIAT:w" s="T870">I</ts>
                  <nts id="Seg_3426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3428" n="HIAT:w" s="T871">ertən</ts>
                  <nts id="Seg_3429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3431" n="HIAT:w" s="T872">uʔbdəbi</ts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3434" n="HIAT:w" s="T873">i</ts>
                  <nts id="Seg_3435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3437" n="HIAT:w" s="T874">moluʔpi</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3440" n="HIAT:w" s="T875">togonorzittə</ts>
                  <nts id="Seg_3441" n="HIAT:ip">.</nts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_3444" n="HIAT:u" s="T876">
                  <nts id="Seg_3445" n="HIAT:ip">(</nts>
                  <ts e="T877" id="Seg_3447" n="HIAT:w" s="T876">A</ts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3450" n="HIAT:w" s="T877">măna</ts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3453" n="HIAT:w" s="T878">prišl-</ts>
                  <nts id="Seg_3454" n="HIAT:ip">)</nts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3457" n="HIAT:w" s="T879">Măn</ts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3460" n="HIAT:w" s="T880">šide</ts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3463" n="HIAT:w" s="T881">tüžöj</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3466" n="HIAT:w" s="T882">surdəbiam</ts>
                  <nts id="Seg_3467" n="HIAT:ip">.</nts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3470" n="HIAT:u" s="T883">
                  <ts e="T884" id="Seg_3472" n="HIAT:w" s="T883">Dĭn</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3475" n="HIAT:w" s="T884">tüžöjdə</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3478" n="HIAT:w" s="T885">i</ts>
                  <nts id="Seg_3479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3481" n="HIAT:w" s="T886">bostə</ts>
                  <nts id="Seg_3482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3484" n="HIAT:w" s="T887">tüžöjdə</ts>
                  <nts id="Seg_3485" n="HIAT:ip">.</nts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T892" id="Seg_3488" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3490" n="HIAT:w" s="T888">Onʼiʔ</ts>
                  <nts id="Seg_3491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3493" n="HIAT:w" s="T889">ne</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3496" n="HIAT:w" s="T890">nanəʔzəbi</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3499" n="HIAT:w" s="T891">ibi</ts>
                  <nts id="Seg_3500" n="HIAT:ip">.</nts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T894" id="Seg_3503" n="HIAT:u" s="T892">
                  <ts e="T893" id="Seg_3505" n="HIAT:w" s="T892">Dĭgəttə</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3508" n="HIAT:w" s="T893">šidəʔpi</ts>
                  <nts id="Seg_3509" n="HIAT:ip">.</nts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T901" id="Seg_3512" n="HIAT:u" s="T894">
                  <ts e="T895" id="Seg_3514" n="HIAT:w" s="T894">A</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3517" n="HIAT:w" s="T895">dĭ</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3520" n="HIAT:w" s="T896">dibər</ts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3523" n="HIAT:w" s="T897">kambi</ts>
                  <nts id="Seg_3524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3526" n="HIAT:w" s="T898">da</ts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3529" n="HIAT:w" s="T899">bĭʔpi</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3532" n="HIAT:w" s="T900">ara</ts>
                  <nts id="Seg_3533" n="HIAT:ip">.</nts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3536" n="HIAT:u" s="T901">
                  <ts e="T902" id="Seg_3538" n="HIAT:w" s="T901">Măn</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3541" n="HIAT:w" s="T902">taldʼen</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3543" n="HIAT:ip">(</nts>
                  <ts e="T904" id="Seg_3545" n="HIAT:w" s="T903">ambam</ts>
                  <nts id="Seg_3546" n="HIAT:ip">)</nts>
                  <nts id="Seg_3547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3548" n="HIAT:ip">(</nts>
                  <ts e="T905" id="Seg_3550" n="HIAT:w" s="T904">măndər-</ts>
                  <nts id="Seg_3551" n="HIAT:ip">)</nts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3554" n="HIAT:w" s="T905">măndobiam</ts>
                  <nts id="Seg_3555" n="HIAT:ip">.</nts>
                  <nts id="Seg_3556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T913" id="Seg_3558" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3560" n="HIAT:w" s="T906">A</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3563" n="HIAT:w" s="T907">teinen</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3566" n="HIAT:w" s="T908">nüdʼin</ts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3569" n="HIAT:w" s="T910">teinen</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3572" n="HIAT:w" s="T911">nüdʼin</ts>
                  <nts id="Seg_3573" n="HIAT:ip">…</nts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3576" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_3578" n="HIAT:w" s="T913">Dĭgəttə</ts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3581" n="HIAT:w" s="T914">dĭm</ts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3584" n="HIAT:w" s="T915">dʼoduni</ts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3587" n="HIAT:w" s="T916">kubiam</ts>
                  <nts id="Seg_3588" n="HIAT:ip">,</nts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3591" n="HIAT:w" s="T917">ĭmbidə</ts>
                  <nts id="Seg_3592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3594" n="HIAT:w" s="T918">deʔpi</ts>
                  <nts id="Seg_3595" n="HIAT:ip">,</nts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3598" n="HIAT:w" s="T919">a</ts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3601" n="HIAT:w" s="T920">măn</ts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3604" n="HIAT:w" s="T921">kurollaʔpiam</ts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3607" n="HIAT:w" s="T922">bar</ts>
                  <nts id="Seg_3608" n="HIAT:ip">.</nts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T927" id="Seg_3611" n="HIAT:u" s="T923">
                  <nts id="Seg_3612" n="HIAT:ip">(</nts>
                  <ts e="T924" id="Seg_3614" n="HIAT:w" s="T923">Tăneldə</ts>
                  <nts id="Seg_3615" n="HIAT:ip">)</nts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3618" n="HIAT:w" s="T924">ej</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3621" n="HIAT:w" s="T925">šobiʔi</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3624" n="HIAT:w" s="T926">dʼijegə</ts>
                  <nts id="Seg_3625" n="HIAT:ip">.</nts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T930" id="Seg_3628" n="HIAT:u" s="T927">
                  <ts e="T928" id="Seg_3630" n="HIAT:w" s="T927">Kamən</ts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3633" n="HIAT:w" s="T928">dĭzeŋ</ts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3636" n="HIAT:w" s="T929">šoləʔjə</ts>
                  <nts id="Seg_3637" n="HIAT:ip">?</nts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_3640" n="HIAT:u" s="T930">
                  <ts e="T931" id="Seg_3642" n="HIAT:w" s="T930">Teinen</ts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3645" n="HIAT:w" s="T931">šoləʔjə</ts>
                  <nts id="Seg_3646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3648" n="HIAT:w" s="T932">nüdʼində</ts>
                  <nts id="Seg_3649" n="HIAT:ip">.</nts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T938" id="Seg_3652" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_3654" n="HIAT:w" s="T933">Amorzittə</ts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3657" n="HIAT:w" s="T934">nada</ts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3660" n="HIAT:w" s="T935">dĭzeŋdə</ts>
                  <nts id="Seg_3661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3663" n="HIAT:w" s="T936">dĭgəttə</ts>
                  <nts id="Seg_3664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3666" n="HIAT:w" s="T937">mĭzittə</ts>
                  <nts id="Seg_3667" n="HIAT:ip">.</nts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_3670" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3672" n="HIAT:w" s="T938">Dĭzeŋ</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3675" n="HIAT:w" s="T939">bar</ts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3678" n="HIAT:w" s="T940">püjöliaʔi</ts>
                  <nts id="Seg_3679" n="HIAT:ip">.</nts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T947" id="Seg_3682" n="HIAT:u" s="T941">
                  <nts id="Seg_3683" n="HIAT:ip">(</nts>
                  <ts e="T942" id="Seg_3685" n="HIAT:w" s="T941">Măn</ts>
                  <nts id="Seg_3686" n="HIAT:ip">)</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3689" n="HIAT:w" s="T942">teinen</ts>
                  <nts id="Seg_3690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3692" n="HIAT:w" s="T943">tenöbiam</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3695" n="HIAT:w" s="T944">šiʔ</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3698" n="HIAT:w" s="T945">bar</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3701" n="HIAT:w" s="T946">šobilaʔ</ts>
                  <nts id="Seg_3702" n="HIAT:ip">.</nts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_3705" n="HIAT:u" s="T947">
                  <ts e="T948" id="Seg_3707" n="HIAT:w" s="T947">A</ts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3710" n="HIAT:w" s="T948">šiʔ</ts>
                  <nts id="Seg_3711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3713" n="HIAT:w" s="T949">ej</ts>
                  <nts id="Seg_3714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3716" n="HIAT:w" s="T950">šobilaʔ</ts>
                  <nts id="Seg_3717" n="HIAT:ip">.</nts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T957" id="Seg_3720" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_3722" n="HIAT:w" s="T951">Măn</ts>
                  <nts id="Seg_3723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3725" n="HIAT:w" s="T952">teinen</ts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3727" n="HIAT:ip">(</nts>
                  <ts e="T954" id="Seg_3729" n="HIAT:w" s="T953">kə-</ts>
                  <nts id="Seg_3730" n="HIAT:ip">)</nts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3733" n="HIAT:w" s="T954">teinen</ts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3735" n="HIAT:ip">(</nts>
                  <ts e="T956" id="Seg_3737" n="HIAT:w" s="T955">munəjʔjə</ts>
                  <nts id="Seg_3738" n="HIAT:ip">)</nts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3741" n="HIAT:w" s="T956">mĭnzərbiem</ts>
                  <nts id="Seg_3742" n="HIAT:ip">.</nts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T965" id="Seg_3745" n="HIAT:u" s="T957">
                  <ts e="T958" id="Seg_3747" n="HIAT:w" s="T957">Kălba</ts>
                  <nts id="Seg_3748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3749" n="HIAT:ip">(</nts>
                  <ts e="T959" id="Seg_3751" n="HIAT:w" s="T958">dʼagarbiam</ts>
                  <nts id="Seg_3752" n="HIAT:ip">)</nts>
                  <nts id="Seg_3753" n="HIAT:ip">,</nts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3756" n="HIAT:w" s="T959">munujʔ</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3759" n="HIAT:w" s="T960">embiem</ts>
                  <nts id="Seg_3760" n="HIAT:ip">,</nts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3762" n="HIAT:ip">(</nts>
                  <ts e="T962" id="Seg_3764" n="HIAT:w" s="T961">gorom</ts>
                  <nts id="Seg_3765" n="HIAT:ip">)</nts>
                  <nts id="Seg_3766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3768" n="HIAT:w" s="T962">embiem</ts>
                  <nts id="Seg_3769" n="HIAT:ip">,</nts>
                  <nts id="Seg_3770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3772" n="HIAT:w" s="T963">šiʔnʼileʔ</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3775" n="HIAT:w" s="T964">bădəsʼtə</ts>
                  <nts id="Seg_3776" n="HIAT:ip">.</nts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T965" id="Seg_3778" n="sc" s="T0">
               <ts e="T1" id="Seg_3780" n="e" s="T0">(davaj) </ts>
               <ts e="T2" id="Seg_3782" n="e" s="T1">nʼezittə. </ts>
               <ts e="T3" id="Seg_3784" n="e" s="T2">(On-) </ts>
               <ts e="T4" id="Seg_3786" n="e" s="T3">Onʼiʔ </ts>
               <ts e="T5" id="Seg_3788" n="e" s="T4">üdʼüge </ts>
               <ts e="T6" id="Seg_3790" n="e" s="T5">penzi </ts>
               <ts e="T7" id="Seg_3792" n="e" s="T6">(ur-) </ts>
               <ts e="T8" id="Seg_3794" n="e" s="T7">üžünə </ts>
               <ts e="T9" id="Seg_3796" n="e" s="T8">üzəbi. </ts>
               <ts e="T10" id="Seg_3798" n="e" s="T9">(Dĭ) </ts>
               <ts e="T11" id="Seg_3800" n="e" s="T10">dĭgəttə </ts>
               <ts e="T12" id="Seg_3802" n="e" s="T11">suʔmiluʔpi </ts>
               <ts e="T13" id="Seg_3804" n="e" s="T12">bar. </ts>
               <ts e="T14" id="Seg_3806" n="e" s="T13">(Dĭ=) </ts>
               <ts e="T15" id="Seg_3808" n="e" s="T14">dĭ </ts>
               <ts e="T16" id="Seg_3810" n="e" s="T15">bĭdəbi, </ts>
               <ts e="T17" id="Seg_3812" n="e" s="T16">bĭdəbi, </ts>
               <ts e="T18" id="Seg_3814" n="e" s="T17">dĭgəttə </ts>
               <ts e="T19" id="Seg_3816" n="e" s="T18">dĭ </ts>
               <ts e="T20" id="Seg_3818" n="e" s="T19">üžüttə </ts>
               <ts e="T21" id="Seg_3820" n="e" s="T20">saʔməluʔpi, </ts>
               <ts e="T22" id="Seg_3822" n="e" s="T21">dĭgəttə </ts>
               <ts e="T23" id="Seg_3824" n="e" s="T22">dĭ </ts>
               <ts e="T24" id="Seg_3826" n="e" s="T23">(ej= </ts>
               <ts e="T25" id="Seg_3828" n="e" s="T24">ej=) </ts>
               <ts e="T26" id="Seg_3830" n="e" s="T25">kambi. </ts>
               <ts e="T27" id="Seg_3832" n="e" s="T26">Măn </ts>
               <ts e="T28" id="Seg_3834" n="e" s="T27">dĭʔnə </ts>
               <ts e="T29" id="Seg_3836" n="e" s="T28">ugandə </ts>
               <ts e="T30" id="Seg_3838" n="e" s="T29">(mĭ-) </ts>
               <ts e="T31" id="Seg_3840" n="e" s="T30">mĭliem, </ts>
               <ts e="T32" id="Seg_3842" n="e" s="T31">a </ts>
               <ts e="T33" id="Seg_3844" n="e" s="T32">(dĭn=) </ts>
               <ts e="T34" id="Seg_3846" n="e" s="T33">dĭ </ts>
               <ts e="T35" id="Seg_3848" n="e" s="T34">măna </ts>
               <ts e="T36" id="Seg_3850" n="e" s="T35">kaməndə </ts>
               <ts e="T37" id="Seg_3852" n="e" s="T36">ej </ts>
               <ts e="T38" id="Seg_3854" n="e" s="T37">šolia. </ts>
               <ts e="T39" id="Seg_3856" n="e" s="T38">Măn </ts>
               <ts e="T40" id="Seg_3858" n="e" s="T39">ugandə </ts>
               <ts e="T41" id="Seg_3860" n="e" s="T40">tăŋ </ts>
               <ts e="T42" id="Seg_3862" n="e" s="T41">măndərliam. </ts>
               <ts e="T43" id="Seg_3864" n="e" s="T42">Jakšə </ts>
               <ts e="T44" id="Seg_3866" n="e" s="T43">măndərdə. </ts>
               <ts e="T45" id="Seg_3868" n="e" s="T44">Dĭ </ts>
               <ts e="T46" id="Seg_3870" n="e" s="T45">kuzam </ts>
               <ts e="T47" id="Seg_3872" n="e" s="T46">kăde </ts>
               <ts e="T48" id="Seg_3874" n="e" s="T47">kăštəsʼtə? </ts>
               <ts e="T49" id="Seg_3876" n="e" s="T48">I </ts>
               <ts e="T50" id="Seg_3878" n="e" s="T49">kăde </ts>
               <ts e="T51" id="Seg_3880" n="e" s="T50">numəjzittə? </ts>
               <ts e="T52" id="Seg_3882" n="e" s="T51">Nʼuʔdə </ts>
               <ts e="T53" id="Seg_3884" n="e" s="T52">măndəraʔ! </ts>
               <ts e="T54" id="Seg_3886" n="e" s="T53">Nʼuʔdə </ts>
               <ts e="T55" id="Seg_3888" n="e" s="T54">măndəraʔ </ts>
               <ts e="T56" id="Seg_3890" n="e" s="T55">măjanə. </ts>
               <ts e="T57" id="Seg_3892" n="e" s="T56">Nada </ts>
               <ts e="T58" id="Seg_3894" n="e" s="T57">teʔmem </ts>
               <ts e="T59" id="Seg_3896" n="e" s="T58">sarzittə. </ts>
               <ts e="T60" id="Seg_3898" n="e" s="T59">Üžüm </ts>
               <ts e="T61" id="Seg_3900" n="e" s="T60">nada </ts>
               <ts e="T62" id="Seg_3902" n="e" s="T61">šerzittə. </ts>
               <ts e="T63" id="Seg_3904" n="e" s="T62">Pargam </ts>
               <ts e="T64" id="Seg_3906" n="e" s="T63">nada </ts>
               <ts e="T65" id="Seg_3908" n="e" s="T64">šerzittə. </ts>
               <ts e="T66" id="Seg_3910" n="e" s="T65">Dĭgəttə </ts>
               <ts e="T67" id="Seg_3912" n="e" s="T66">nʼiʔtə </ts>
               <ts e="T68" id="Seg_3914" n="e" s="T67">kanzittə. </ts>
               <ts e="T69" id="Seg_3916" n="e" s="T68">Măn </ts>
               <ts e="T70" id="Seg_3918" n="e" s="T69">(čumbə) </ts>
               <ts e="T71" id="Seg_3920" n="e" s="T70">kambiam. </ts>
               <ts e="T72" id="Seg_3922" n="e" s="T71">Čumgən </ts>
               <ts e="T73" id="Seg_3924" n="e" s="T72">amnobiam. </ts>
               <ts e="T74" id="Seg_3926" n="e" s="T73">Dĭgəttə </ts>
               <ts e="T75" id="Seg_3928" n="e" s="T74">(baroʔ) </ts>
               <ts e="T967" id="Seg_3930" n="e" s="T75">kalla </ts>
               <ts e="T76" id="Seg_3932" n="e" s="T967">dʼürbiem </ts>
               <ts e="T77" id="Seg_3934" n="e" s="T76">(čum-) </ts>
               <ts e="T78" id="Seg_3936" n="e" s="T77">čumgəndə. </ts>
               <ts e="T79" id="Seg_3938" n="e" s="T78">Măn </ts>
               <ts e="T80" id="Seg_3940" n="e" s="T79">üdʼüge </ts>
               <ts e="T81" id="Seg_3942" n="e" s="T80">ibiem. </ts>
               <ts e="T82" id="Seg_3944" n="e" s="T81">Ine </ts>
               <ts e="T83" id="Seg_3946" n="e" s="T82">ibi </ts>
               <ts e="T84" id="Seg_3948" n="e" s="T83">(nuʔ-) </ts>
               <ts e="T85" id="Seg_3950" n="e" s="T84">miʔ. </ts>
               <ts e="T86" id="Seg_3952" n="e" s="T85">(Šide=) </ts>
               <ts e="T87" id="Seg_3954" n="e" s="T86">Šide </ts>
               <ts e="T88" id="Seg_3956" n="e" s="T87">(kɨ- </ts>
               <ts e="T89" id="Seg_3958" n="e" s="T88">ke-) </ts>
               <ts e="T90" id="Seg_3960" n="e" s="T89">kö. </ts>
               <ts e="T91" id="Seg_3962" n="e" s="T90">Bar </ts>
               <ts e="T92" id="Seg_3964" n="e" s="T91">köreleʔpibeʔ. </ts>
               <ts e="T93" id="Seg_3966" n="e" s="T92">Dĭʔə </ts>
               <ts e="T94" id="Seg_3968" n="e" s="T93">kandəga. </ts>
               <ts e="T95" id="Seg_3970" n="e" s="T94">(Măn) </ts>
               <ts e="T96" id="Seg_3972" n="e" s="T95">kallam, </ts>
               <ts e="T97" id="Seg_3974" n="e" s="T96">dĭgəttə </ts>
               <ts e="T98" id="Seg_3976" n="e" s="T97">dĭ </ts>
               <ts e="T99" id="Seg_3978" n="e" s="T98">(kandə-) </ts>
               <ts e="T100" id="Seg_3980" n="e" s="T99">kaləj </ts>
               <ts e="T101" id="Seg_3982" n="e" s="T100">mănzʼə. </ts>
               <ts e="T102" id="Seg_3984" n="e" s="T101">Sʼabiam </ts>
               <ts e="T103" id="Seg_3986" n="e" s="T102">čumdə. </ts>
               <ts e="T104" id="Seg_3988" n="e" s="T103">Amnobiam </ts>
               <ts e="T105" id="Seg_3990" n="e" s="T104">(čumdən). </ts>
               <ts e="T106" id="Seg_3992" n="e" s="T105">Dĭgəttə </ts>
               <ts e="T107" id="Seg_3994" n="e" s="T106">(už-) </ts>
               <ts e="T108" id="Seg_3996" n="e" s="T107">üzəbiem </ts>
               <ts e="T109" id="Seg_3998" n="e" s="T108">čumdə. </ts>
               <ts e="T110" id="Seg_4000" n="e" s="T109">Tura </ts>
               <ts e="T111" id="Seg_4002" n="e" s="T110">bar </ts>
               <ts e="T112" id="Seg_4004" n="e" s="T111">(neniluʔpiʔi=) </ts>
               <ts e="T113" id="Seg_4006" n="e" s="T112">neniluʔpi. </ts>
               <ts e="T114" id="Seg_4008" n="e" s="T113">Il </ts>
               <ts e="T115" id="Seg_4010" n="e" s="T114">bar </ts>
               <ts e="T116" id="Seg_4012" n="e" s="T115">nuʔməleʔbəʔjə, </ts>
               <ts e="T117" id="Seg_4014" n="e" s="T116">bü </ts>
               <ts e="T118" id="Seg_4016" n="e" s="T117">detleʔbəʔjə. </ts>
               <ts e="T119" id="Seg_4018" n="e" s="T118">Šünə </ts>
               <ts e="T120" id="Seg_4020" n="e" s="T119">kămlaʔbəʔjə </ts>
               <ts e="T121" id="Seg_4022" n="e" s="T120">bar. </ts>
               <ts e="T122" id="Seg_4024" n="e" s="T121">Štobɨ </ts>
               <ts e="T123" id="Seg_4026" n="e" s="T122">ej </ts>
               <ts e="T124" id="Seg_4028" n="e" s="T123">nenibi. </ts>
               <ts e="T125" id="Seg_4030" n="e" s="T124">Onʼiʔ </ts>
               <ts e="T126" id="Seg_4032" n="e" s="T125">kuza </ts>
               <ts e="T127" id="Seg_4034" n="e" s="T126">onʼiʔ </ts>
               <ts e="T128" id="Seg_4036" n="e" s="T127">kuzanə </ts>
               <ts e="T129" id="Seg_4038" n="e" s="T128">bar </ts>
               <ts e="T130" id="Seg_4040" n="e" s="T129">ipek </ts>
               <ts e="T131" id="Seg_4042" n="e" s="T130">nendəbi. </ts>
               <ts e="T132" id="Seg_4044" n="e" s="T131">Bar </ts>
               <ts e="T133" id="Seg_4046" n="e" s="T132">nendəluʔpi. </ts>
               <ts e="T134" id="Seg_4048" n="e" s="T133">Xatʼel </ts>
               <ts e="T135" id="Seg_4050" n="e" s="T134">молотилка </ts>
               <ts e="T136" id="Seg_4052" n="e" s="T135">(nun-) </ts>
               <ts e="T137" id="Seg_4054" n="e" s="T136">nuldəsʼtə, </ts>
               <ts e="T138" id="Seg_4056" n="e" s="T137">(a) </ts>
               <ts e="T139" id="Seg_4058" n="e" s="T138">dĭ </ts>
               <ts e="T140" id="Seg_4060" n="e" s="T139">ibi </ts>
               <ts e="T141" id="Seg_4062" n="e" s="T140">da </ts>
               <ts e="T142" id="Seg_4064" n="e" s="T141">ipek </ts>
               <ts e="T143" id="Seg_4066" n="e" s="T142">nendəluʔpi. </ts>
               <ts e="T144" id="Seg_4068" n="e" s="T143">(Kudonzəbi </ts>
               <ts e="T145" id="Seg_4070" n="e" s="T144">bʼa-) </ts>
               <ts e="T146" id="Seg_4072" n="e" s="T145">dĭzi, </ts>
               <ts e="T147" id="Seg_4074" n="e" s="T146">dĭ </ts>
               <ts e="T148" id="Seg_4076" n="e" s="T147">kuroluʔpi. </ts>
               <ts e="T149" id="Seg_4078" n="e" s="T148">Noʔ </ts>
               <ts e="T150" id="Seg_4080" n="e" s="T149">jaʔpiam. </ts>
               <ts e="T151" id="Seg_4082" n="e" s="T150">Zarottə </ts>
               <ts e="T152" id="Seg_4084" n="e" s="T151">tažerbiam, </ts>
               <ts e="T153" id="Seg_4086" n="e" s="T152">embiem. </ts>
               <ts e="T154" id="Seg_4088" n="e" s="T153">A </ts>
               <ts e="T155" id="Seg_4090" n="e" s="T154">dĭ </ts>
               <ts e="T156" id="Seg_4092" n="e" s="T155">bar </ts>
               <ts e="T157" id="Seg_4094" n="e" s="T156">neniluʔpi. </ts>
               <ts e="T158" id="Seg_4096" n="e" s="T157">Maʔtə </ts>
               <ts e="T159" id="Seg_4098" n="e" s="T158">šobiam. </ts>
               <ts e="T160" id="Seg_4100" n="e" s="T159">Dĭn </ts>
               <ts e="T161" id="Seg_4102" n="e" s="T160">bar </ts>
               <ts e="T162" id="Seg_4104" n="e" s="T161">nubiam, </ts>
               <ts e="T163" id="Seg_4106" n="e" s="T162">dĭgəttə </ts>
               <ts e="T164" id="Seg_4108" n="e" s="T163">(kalladʼu-) </ts>
               <ts e="T165" id="Seg_4110" n="e" s="T164">maʔtə </ts>
               <ts e="T968" id="Seg_4112" n="e" s="T165">kalla </ts>
               <ts e="T166" id="Seg_4114" n="e" s="T968">dʼürbiem. </ts>
               <ts e="T167" id="Seg_4116" n="e" s="T166">Teinen </ts>
               <ts e="T168" id="Seg_4118" n="e" s="T167">măn </ts>
               <ts e="T169" id="Seg_4120" n="e" s="T168">ej </ts>
               <ts e="T170" id="Seg_4122" n="e" s="T169">togonoriam, </ts>
               <ts e="T171" id="Seg_4124" n="e" s="T170">a </ts>
               <ts e="T172" id="Seg_4126" n="e" s="T171">tănzi </ts>
               <ts e="T173" id="Seg_4128" n="e" s="T172">dʼăbaktəriam. </ts>
               <ts e="T174" id="Seg_4130" n="e" s="T173">Xotʼ </ts>
               <ts e="T175" id="Seg_4132" n="e" s="T174">ej </ts>
               <ts e="T176" id="Seg_4134" n="e" s="T175">sedem, </ts>
               <ts e="T177" id="Seg_4136" n="e" s="T176">a </ts>
               <ts e="T178" id="Seg_4138" n="e" s="T177">tararluʔpiem </ts>
               <ts e="T179" id="Seg_4140" n="e" s="T178">ugandə. </ts>
               <ts e="T180" id="Seg_4142" n="e" s="T179">Ugandə </ts>
               <ts e="T181" id="Seg_4144" n="e" s="T180">surno </ts>
               <ts e="T182" id="Seg_4146" n="e" s="T181">tăŋ </ts>
               <ts e="T183" id="Seg_4148" n="e" s="T182">kandəga, </ts>
               <ts e="T184" id="Seg_4150" n="e" s="T183">šonuga. </ts>
               <ts e="T185" id="Seg_4152" n="e" s="T184">(M-) </ts>
               <ts e="T186" id="Seg_4154" n="e" s="T185">A </ts>
               <ts e="T187" id="Seg_4156" n="e" s="T186">măn </ts>
               <ts e="T188" id="Seg_4158" n="e" s="T187">(pa-) </ts>
               <ts e="T189" id="Seg_4160" n="e" s="T188">(paʔi) </ts>
               <ts e="T190" id="Seg_4162" n="e" s="T189">amnolbiam. </ts>
               <ts e="T191" id="Seg_4164" n="e" s="T190">Beržə </ts>
               <ts e="T192" id="Seg_4166" n="e" s="T191">bar </ts>
               <ts e="T193" id="Seg_4168" n="e" s="T192">šonuga. </ts>
               <ts e="T194" id="Seg_4170" n="e" s="T193">Măn </ts>
               <ts e="T195" id="Seg_4172" n="e" s="T194">kujnek </ts>
               <ts e="T196" id="Seg_4174" n="e" s="T195">kuvas. </ts>
               <ts e="T197" id="Seg_4176" n="e" s="T196">Dĭn </ts>
               <ts e="T198" id="Seg_4178" n="e" s="T197">amnobiam. </ts>
               <ts e="T199" id="Seg_4180" n="e" s="T198">Măna </ts>
               <ts e="T200" id="Seg_4182" n="e" s="T199">ej </ts>
               <ts e="T201" id="Seg_4184" n="e" s="T200">nünörbi, </ts>
               <ts e="T202" id="Seg_4186" n="e" s="T201">a_to </ts>
               <ts e="T203" id="Seg_4188" n="e" s="T202">bɨ </ts>
               <ts e="T204" id="Seg_4190" n="e" s="T203">nünörbi </ts>
               <ts e="T205" id="Seg_4192" n="e" s="T204">bar. </ts>
               <ts e="T206" id="Seg_4194" n="e" s="T205">Măn </ts>
               <ts e="T207" id="Seg_4196" n="e" s="T206">dʼijenə </ts>
               <ts e="T208" id="Seg_4198" n="e" s="T207">mĭmbiem. </ts>
               <ts e="T209" id="Seg_4200" n="e" s="T208">Nüjnə </ts>
               <ts e="T210" id="Seg_4202" n="e" s="T209">(nüj-) </ts>
               <ts e="T211" id="Seg_4204" n="e" s="T210">nüjbiem, </ts>
               <ts e="T212" id="Seg_4206" n="e" s="T211">a </ts>
               <ts e="T213" id="Seg_4208" n="e" s="T212">dĭgəttə </ts>
               <ts e="T214" id="Seg_4210" n="e" s="T213">dʼorbiam. </ts>
               <ts e="T215" id="Seg_4212" n="e" s="T214">Măn </ts>
               <ts e="T216" id="Seg_4214" n="e" s="T215">nʼim </ts>
               <ts e="T217" id="Seg_4216" n="e" s="T216">külaːmbi, </ts>
               <ts e="T218" id="Seg_4218" n="e" s="T217">măn </ts>
               <ts e="T219" id="Seg_4220" n="e" s="T218">ajirbiam. </ts>
               <ts e="T220" id="Seg_4222" n="e" s="T219">Nüke </ts>
               <ts e="T221" id="Seg_4224" n="e" s="T220">i </ts>
               <ts e="T222" id="Seg_4226" n="e" s="T221">büzʼe </ts>
               <ts e="T223" id="Seg_4228" n="e" s="T222">amnolaʔbəʔjə. </ts>
               <ts e="T224" id="Seg_4230" n="e" s="T223">A </ts>
               <ts e="T225" id="Seg_4232" n="e" s="T224">tăn </ts>
               <ts e="T226" id="Seg_4234" n="e" s="T225">(šojonə) </ts>
               <ts e="T227" id="Seg_4236" n="e" s="T226">amnaʔ. </ts>
               <ts e="T228" id="Seg_4238" n="e" s="T227">Gijen </ts>
               <ts e="T229" id="Seg_4240" n="e" s="T228">ibiel? </ts>
               <ts e="T230" id="Seg_4242" n="e" s="T229">Gibər </ts>
               <ts e="T231" id="Seg_4244" n="e" s="T230">kandəgal? </ts>
               <ts e="T232" id="Seg_4246" n="e" s="T231">Maʔtə </ts>
               <ts e="T233" id="Seg_4248" n="e" s="T232">kandəgam. </ts>
               <ts e="T234" id="Seg_4250" n="e" s="T233">(Aktʼa) </ts>
               <ts e="T235" id="Seg_4252" n="e" s="T234">Aktʼa </ts>
               <ts e="T236" id="Seg_4254" n="e" s="T235">naga. </ts>
               <ts e="T237" id="Seg_4256" n="e" s="T236">Aktʼažət </ts>
               <ts e="T238" id="Seg_4258" n="e" s="T237">amnozittə </ts>
               <ts e="T239" id="Seg_4260" n="e" s="T238">ej </ts>
               <ts e="T240" id="Seg_4262" n="e" s="T239">jakšə. </ts>
               <ts e="T241" id="Seg_4264" n="e" s="T240">(Aktʼa) </ts>
               <ts e="T242" id="Seg_4266" n="e" s="T241">Aktʼa </ts>
               <ts e="T243" id="Seg_4268" n="e" s="T242">ige </ts>
               <ts e="T244" id="Seg_4270" n="e" s="T243">dak, </ts>
               <ts e="T245" id="Seg_4272" n="e" s="T244">jakšə </ts>
               <ts e="T246" id="Seg_4274" n="e" s="T245">amnozittə. </ts>
               <ts e="T247" id="Seg_4276" n="e" s="T246">Aktʼanə </ts>
               <ts e="T248" id="Seg_4278" n="e" s="T247">ĭmbi-nʼibudʼ </ts>
               <ts e="T249" id="Seg_4280" n="e" s="T248">iləl, </ts>
               <ts e="T250" id="Seg_4282" n="e" s="T249">munəjʔ </ts>
               <ts e="T251" id="Seg_4284" n="e" s="T250">možna </ts>
               <ts e="T252" id="Seg_4286" n="e" s="T251">izittə, </ts>
               <ts e="T253" id="Seg_4288" n="e" s="T252">ipek </ts>
               <ts e="T254" id="Seg_4290" n="e" s="T253">izittə. </ts>
               <ts e="T255" id="Seg_4292" n="e" s="T254">Kola </ts>
               <ts e="T256" id="Seg_4294" n="e" s="T255">izittə, </ts>
               <ts e="T257" id="Seg_4296" n="e" s="T256">uja </ts>
               <ts e="T258" id="Seg_4298" n="e" s="T257">iləl </ts>
               <ts e="T259" id="Seg_4300" n="e" s="T258">aktʼanə </ts>
               <ts e="T260" id="Seg_4302" n="e" s="T259">üge. </ts>
               <ts e="T261" id="Seg_4304" n="e" s="T260">Aktʼa </ts>
               <ts e="T262" id="Seg_4306" n="e" s="T261">naga </ts>
               <ts e="T263" id="Seg_4308" n="e" s="T262">dak </ts>
               <ts e="T264" id="Seg_4310" n="e" s="T263">ĭmbidə </ts>
               <ts e="T265" id="Seg_4312" n="e" s="T264">ej </ts>
               <ts e="T266" id="Seg_4314" n="e" s="T265">iləl. </ts>
               <ts e="T267" id="Seg_4316" n="e" s="T266">Kola </ts>
               <ts e="T268" id="Seg_4318" n="e" s="T267">dʼabəsʼtə </ts>
               <ts e="T269" id="Seg_4320" n="e" s="T268">kandəgal. </ts>
               <ts e="T270" id="Seg_4322" n="e" s="T269">Kola </ts>
               <ts e="T271" id="Seg_4324" n="e" s="T270">ej </ts>
               <ts e="T272" id="Seg_4326" n="e" s="T271">(dʼa-) </ts>
               <ts e="T273" id="Seg_4328" n="e" s="T272">dʼaʔpial </ts>
               <ts e="T274" id="Seg_4330" n="e" s="T273">dak, </ts>
               <ts e="T275" id="Seg_4332" n="e" s="T274">(iʔ </ts>
               <ts e="T276" id="Seg_4334" n="e" s="T275">šoʔ </ts>
               <ts e="T277" id="Seg_4336" n="e" s="T276">maʔnəl). </ts>
               <ts e="T278" id="Seg_4338" n="e" s="T277">Kanaʔ </ts>
               <ts e="T279" id="Seg_4340" n="e" s="T278">tuganbə </ts>
               <ts e="T280" id="Seg_4342" n="e" s="T279">kăštəʔ! </ts>
               <ts e="T281" id="Seg_4344" n="e" s="T280">Ej </ts>
               <ts e="T282" id="Seg_4346" n="e" s="T281">kalaʔi </ts>
               <ts e="T283" id="Seg_4348" n="e" s="T282">dak, </ts>
               <ts e="T284" id="Seg_4350" n="e" s="T283">(boštə </ts>
               <ts e="T285" id="Seg_4352" n="e" s="T284">iʔ </ts>
               <ts e="T286" id="Seg_4354" n="e" s="T285">šoʔ). </ts>
               <ts e="T287" id="Seg_4356" n="e" s="T286">Kăštʼit </ts>
               <ts e="T288" id="Seg_4358" n="e" s="T287">jakšəŋ! </ts>
               <ts e="T289" id="Seg_4360" n="e" s="T288">Štobɨ </ts>
               <ts e="T290" id="Seg_4362" n="e" s="T289">šobiʔi. </ts>
               <ts e="T291" id="Seg_4364" n="e" s="T290">Ej </ts>
               <ts e="T292" id="Seg_4366" n="e" s="T291">jakšə </ts>
               <ts e="T293" id="Seg_4368" n="e" s="T292">kuza. </ts>
               <ts e="T294" id="Seg_4370" n="e" s="T293">Arda </ts>
               <ts e="T295" id="Seg_4372" n="e" s="T294">sʼimamzʼə </ts>
               <ts e="T296" id="Seg_4374" n="e" s="T295">ej </ts>
               <ts e="T297" id="Seg_4376" n="e" s="T296">moliam </ts>
               <ts e="T298" id="Seg_4378" n="e" s="T297">măndəsʼtə, </ts>
               <ts e="T299" id="Seg_4380" n="e" s="T298">putʼəga </ts>
               <ts e="T300" id="Seg_4382" n="e" s="T299">ugandə. </ts>
               <ts e="T301" id="Seg_4384" n="e" s="T300">Koldʼəŋ </ts>
               <ts e="T302" id="Seg_4386" n="e" s="T301">kanaʔ. </ts>
               <ts e="T303" id="Seg_4388" n="e" s="T302">Tăŋ </ts>
               <ts e="T304" id="Seg_4390" n="e" s="T303">iʔ </ts>
               <ts e="T305" id="Seg_4392" n="e" s="T304">mĭneʔ. </ts>
               <ts e="T306" id="Seg_4394" n="e" s="T305">Kuliom </ts>
               <ts e="T307" id="Seg_4396" n="e" s="T306">bar: </ts>
               <ts e="T308" id="Seg_4398" n="e" s="T307">dĭ </ts>
               <ts e="T309" id="Seg_4400" n="e" s="T308">bostə </ts>
               <ts e="T310" id="Seg_4402" n="e" s="T309">šonəga </ts>
               <ts e="T311" id="Seg_4404" n="e" s="T310">măna. </ts>
               <ts e="T312" id="Seg_4406" n="e" s="T311">Boskəndə </ts>
               <ts e="T313" id="Seg_4408" n="e" s="T312">bar </ts>
               <ts e="T314" id="Seg_4410" n="e" s="T313">(manʼiʔleʔbə), </ts>
               <ts e="T315" id="Seg_4412" n="e" s="T314">a </ts>
               <ts e="T316" id="Seg_4414" n="e" s="T315">măn </ts>
               <ts e="T317" id="Seg_4416" n="e" s="T316">ej </ts>
               <ts e="T318" id="Seg_4418" n="e" s="T317">kaliam. </ts>
               <ts e="T319" id="Seg_4420" n="e" s="T318">Dʼijegən </ts>
               <ts e="T320" id="Seg_4422" n="e" s="T319">всякий </ts>
               <ts e="T321" id="Seg_4424" n="e" s="T320">paʔi </ts>
               <ts e="T322" id="Seg_4426" n="e" s="T321">bar </ts>
               <ts e="T323" id="Seg_4428" n="e" s="T322">özerleʔbəʔjə. </ts>
               <ts e="T324" id="Seg_4430" n="e" s="T323">Kuvas </ts>
               <ts e="T325" id="Seg_4432" n="e" s="T324">svʼetogəʔi, </ts>
               <ts e="T326" id="Seg_4434" n="e" s="T325">bar </ts>
               <ts e="T327" id="Seg_4436" n="e" s="T326">ugandə </ts>
               <ts e="T328" id="Seg_4438" n="e" s="T327">tăŋ </ts>
               <ts e="T329" id="Seg_4440" n="e" s="T328">putʼəmniaʔi. </ts>
               <ts e="T330" id="Seg_4442" n="e" s="T329">Ugandə </ts>
               <ts e="T331" id="Seg_4444" n="e" s="T330">nünniom, </ts>
               <ts e="T332" id="Seg_4446" n="e" s="T331">jakšə </ts>
               <ts e="T333" id="Seg_4448" n="e" s="T332">putʼəmnia, </ts>
               <ts e="T334" id="Seg_4450" n="e" s="T333">ĭmbidə </ts>
               <ts e="T335" id="Seg_4452" n="e" s="T334">mĭnzerleʔbə. </ts>
               <ts e="T336" id="Seg_4454" n="e" s="T335">Kurizəʔi </ts>
               <ts e="T337" id="Seg_4456" n="e" s="T336">mĭngeʔi </ts>
               <ts e="T338" id="Seg_4458" n="e" s="T337">dʼügən </ts>
               <ts e="T339" id="Seg_4460" n="e" s="T338">bar </ts>
               <ts e="T340" id="Seg_4462" n="e" s="T339">dʼüm </ts>
               <ts e="T341" id="Seg_4464" n="e" s="T340">kădaʔlaʔbəʔjə </ts>
               <ts e="T342" id="Seg_4466" n="e" s="T341">(ujuzə-) </ts>
               <ts e="T343" id="Seg_4468" n="e" s="T342">ujuzaŋdə. </ts>
               <ts e="T344" id="Seg_4470" n="e" s="T343">(Человека) </ts>
               <ts e="T345" id="Seg_4472" n="e" s="T344">надо </ts>
               <ts e="T346" id="Seg_4474" n="e" s="T345">ждать. </ts>
               <ts e="T347" id="Seg_4476" n="e" s="T346">Dĭ </ts>
               <ts e="T348" id="Seg_4478" n="e" s="T347">kuzam </ts>
               <ts e="T349" id="Seg_4480" n="e" s="T348">nada </ts>
               <ts e="T350" id="Seg_4482" n="e" s="T349">(deʔsittə= </ts>
               <ts e="T351" id="Seg_4484" n="e" s="T350">dĭ= </ts>
               <ts e="T352" id="Seg_4486" n="e" s="T351">bĭ-) </ts>
               <ts e="T353" id="Seg_4488" n="e" s="T352">edəʔsittə, </ts>
               <ts e="T354" id="Seg_4490" n="e" s="T353">dĭ </ts>
               <ts e="T355" id="Seg_4492" n="e" s="T354">bar </ts>
               <ts e="T356" id="Seg_4494" n="e" s="T355">büžü </ts>
               <ts e="T357" id="Seg_4496" n="e" s="T356">šoləj. </ts>
               <ts e="T358" id="Seg_4498" n="e" s="T357">Ara </ts>
               <ts e="T359" id="Seg_4500" n="e" s="T358">detləj. </ts>
               <ts e="T360" id="Seg_4502" n="e" s="T359">Dĭzeŋ </ts>
               <ts e="T361" id="Seg_4504" n="e" s="T360">bar </ts>
               <ts e="T362" id="Seg_4506" n="e" s="T361">dĭm </ts>
               <ts e="T363" id="Seg_4508" n="e" s="T362">edəʔleʔbəʔjə. </ts>
               <ts e="T364" id="Seg_4510" n="e" s="T363">(Dĭzeŋ=) </ts>
               <ts e="T365" id="Seg_4512" n="e" s="T364">Dĭzeŋ </ts>
               <ts e="T366" id="Seg_4514" n="e" s="T365">edəʔleʔbəʔjə. </ts>
               <ts e="T367" id="Seg_4516" n="e" s="T366">Teinen </ts>
               <ts e="T368" id="Seg_4518" n="e" s="T367">miʔ </ts>
               <ts e="T369" id="Seg_4520" n="e" s="T368">jakšə </ts>
               <ts e="T370" id="Seg_4522" n="e" s="T369">(dʼăbaktərləbiaʔ). </ts>
               <ts e="T371" id="Seg_4524" n="e" s="T370">A </ts>
               <ts e="T372" id="Seg_4526" n="e" s="T371">ĭmbi </ts>
               <ts e="T373" id="Seg_4528" n="e" s="T372">dʼăbaktərlaʔpileʔ? </ts>
               <ts e="T374" id="Seg_4530" n="e" s="T373">Nan </ts>
               <ts e="T375" id="Seg_4532" n="e" s="T374">kudolbibaʔ, </ts>
               <ts e="T376" id="Seg_4534" n="e" s="T375">i </ts>
               <ts e="T377" id="Seg_4536" n="e" s="T376">bar </ts>
               <ts e="T378" id="Seg_4538" n="e" s="T377">šiʔ </ts>
               <ts e="T379" id="Seg_4540" n="e" s="T378">kudolbibaʔ. </ts>
               <ts e="T380" id="Seg_4542" n="e" s="T379">Ineʔi </ts>
               <ts e="T381" id="Seg_4544" n="e" s="T380">bar </ts>
               <ts e="T382" id="Seg_4546" n="e" s="T381">kandəgaʔi. </ts>
               <ts e="T383" id="Seg_4548" n="e" s="T382">Šide </ts>
               <ts e="T384" id="Seg_4550" n="e" s="T383">ine </ts>
               <ts e="T385" id="Seg_4552" n="e" s="T384">kandəga, </ts>
               <ts e="T386" id="Seg_4554" n="e" s="T385">onʼiʔ </ts>
               <ts e="T387" id="Seg_4556" n="e" s="T386">ine </ts>
               <ts e="T388" id="Seg_4558" n="e" s="T387">kandəga. </ts>
               <ts e="T389" id="Seg_4560" n="e" s="T388">Măn </ts>
               <ts e="T390" id="Seg_4562" n="e" s="T389">nʼim </ts>
               <ts e="T391" id="Seg_4564" n="e" s="T390">šonəga. </ts>
               <ts e="T392" id="Seg_4566" n="e" s="T391">A </ts>
               <ts e="T393" id="Seg_4568" n="e" s="T392">gijen </ts>
               <ts e="T394" id="Seg_4570" n="e" s="T393">ibi— </ts>
               <ts e="T395" id="Seg_4572" n="e" s="T394">kola </ts>
               <ts e="T396" id="Seg_4574" n="e" s="T395">ile </ts>
               <ts e="T397" id="Seg_4576" n="e" s="T396">mĭmbi. </ts>
               <ts e="T398" id="Seg_4578" n="e" s="T397">Ej </ts>
               <ts e="T399" id="Seg_4580" n="e" s="T398">tĭmniem, </ts>
               <ts e="T400" id="Seg_4582" n="e" s="T399">deʔpi </ts>
               <ts e="T401" id="Seg_4584" n="e" s="T400">li, </ts>
               <ts e="T402" id="Seg_4586" n="e" s="T401">ej </ts>
               <ts e="T403" id="Seg_4588" n="e" s="T402">deʔpi. </ts>
               <ts e="T404" id="Seg_4590" n="e" s="T403">Deʔpi </ts>
               <ts e="T405" id="Seg_4592" n="e" s="T404">(dak), </ts>
               <ts e="T406" id="Seg_4594" n="e" s="T405">pürzittə </ts>
               <ts e="T407" id="Seg_4596" n="e" s="T406">nada. </ts>
               <ts e="T408" id="Seg_4598" n="e" s="T407">(Dĭ) </ts>
               <ts e="T409" id="Seg_4600" n="e" s="T408">ne </ts>
               <ts e="T410" id="Seg_4602" n="e" s="T409">bar </ts>
               <ts e="T411" id="Seg_4604" n="e" s="T410">nanəʔzəbi. </ts>
               <ts e="T412" id="Seg_4606" n="e" s="T411">Büžü </ts>
               <ts e="T413" id="Seg_4608" n="e" s="T412">(büre) </ts>
               <ts e="T414" id="Seg_4610" n="e" s="T413">ešši </ts>
               <ts e="T415" id="Seg_4612" n="e" s="T414">detləj. </ts>
               <ts e="T416" id="Seg_4614" n="e" s="T415">Nada </ts>
               <ts e="T417" id="Seg_4616" n="e" s="T416">dĭm </ts>
               <ts e="T418" id="Seg_4618" n="e" s="T417">(nereluʔsittə). </ts>
               <ts e="T419" id="Seg_4620" n="e" s="T418">Kanzittə </ts>
               <ts e="T420" id="Seg_4622" n="e" s="T419">nada, </ts>
               <ts e="T421" id="Seg_4624" n="e" s="T420">ĭmbi-nʼibudʼ </ts>
               <ts e="T422" id="Seg_4626" n="e" s="T421">sadarla </ts>
               <ts e="T423" id="Seg_4628" n="e" s="T422">izittə. </ts>
               <ts e="T424" id="Seg_4630" n="e" s="T423">Kamən </ts>
               <ts e="T426" id="Seg_4632" n="e" s="T424">dʼala… </ts>
               <ts e="T427" id="Seg_4634" n="e" s="T426">((…)). </ts>
               <ts e="T428" id="Seg_4636" n="e" s="T427">Kamən </ts>
               <ts e="T429" id="Seg_4638" n="e" s="T428">dʼala </ts>
               <ts e="T430" id="Seg_4640" n="e" s="T429">šolaʔbə </ts>
               <ts e="T431" id="Seg_4642" n="e" s="T430">(i) </ts>
               <ts e="T432" id="Seg_4644" n="e" s="T431">măn </ts>
               <ts e="T433" id="Seg_4646" n="e" s="T432">uʔlaʔbəm. </ts>
               <ts e="T434" id="Seg_4648" n="e" s="T433">Kamən </ts>
               <ts e="T435" id="Seg_4650" n="e" s="T434">dʼala </ts>
               <ts e="T436" id="Seg_4652" n="e" s="T435">kandəga </ts>
               <ts e="T437" id="Seg_4654" n="e" s="T436">măn </ts>
               <ts e="T438" id="Seg_4656" n="e" s="T437">iʔbələm. </ts>
               <ts e="T439" id="Seg_4658" n="e" s="T438">Dĭ </ts>
               <ts e="T440" id="Seg_4660" n="e" s="T439">kö </ts>
               <ts e="T441" id="Seg_4662" n="e" s="T440">ugandə </ts>
               <ts e="T442" id="Seg_4664" n="e" s="T441">šišəge </ts>
               <ts e="T443" id="Seg_4666" n="e" s="T442">ibi, </ts>
               <ts e="T444" id="Seg_4668" n="e" s="T443">ugandə </ts>
               <ts e="T445" id="Seg_4670" n="e" s="T444">numo </ts>
               <ts e="T446" id="Seg_4672" n="e" s="T445">ibi. </ts>
               <ts e="T447" id="Seg_4674" n="e" s="T446">Nüdʼəʔi </ts>
               <ts e="T448" id="Seg_4676" n="e" s="T447">bar </ts>
               <ts e="T449" id="Seg_4678" n="e" s="T448">ugandə </ts>
               <ts e="T450" id="Seg_4680" n="e" s="T449">(no- </ts>
               <ts e="T451" id="Seg_4682" n="e" s="T450">no- </ts>
               <ts e="T452" id="Seg_4684" n="e" s="T451">nugo-) </ts>
               <ts e="T453" id="Seg_4686" n="e" s="T452">nugoʔi. </ts>
               <ts e="T454" id="Seg_4688" n="e" s="T453">A </ts>
               <ts e="T455" id="Seg_4690" n="e" s="T454">dĭ </ts>
               <ts e="T456" id="Seg_4692" n="e" s="T455">(pʼe) </ts>
               <ts e="T457" id="Seg_4694" n="e" s="T456">šobi </ts>
               <ts e="T458" id="Seg_4696" n="e" s="T457">dak, </ts>
               <ts e="T459" id="Seg_4698" n="e" s="T458">ugandə </ts>
               <ts e="T460" id="Seg_4700" n="e" s="T459">ejü. </ts>
               <ts e="T461" id="Seg_4702" n="e" s="T460">Dʼalaʔi </ts>
               <ts e="T462" id="Seg_4704" n="e" s="T461">bar </ts>
               <ts e="T463" id="Seg_4706" n="e" s="T462">numoʔi. </ts>
               <ts e="T464" id="Seg_4708" n="e" s="T463">Nada </ts>
               <ts e="T465" id="Seg_4710" n="e" s="T464">măna </ts>
               <ts e="T466" id="Seg_4712" n="e" s="T465">bü </ts>
               <ts e="T467" id="Seg_4714" n="e" s="T466">pʼeštə </ts>
               <ts e="T468" id="Seg_4716" n="e" s="T467">nuldəsʼtə, </ts>
               <ts e="T469" id="Seg_4718" n="e" s="T468">a_to </ts>
               <ts e="T470" id="Seg_4720" n="e" s="T469">büzo </ts>
               <ts e="T471" id="Seg_4722" n="e" s="T470">šoləj. </ts>
               <ts e="T472" id="Seg_4724" n="e" s="T471">Bü </ts>
               <ts e="T473" id="Seg_4726" n="e" s="T472">naga. </ts>
               <ts e="T474" id="Seg_4728" n="e" s="T473">A </ts>
               <ts e="T475" id="Seg_4730" n="e" s="T474">dĭ </ts>
               <ts e="T476" id="Seg_4732" n="e" s="T475">šišəge </ts>
               <ts e="T477" id="Seg_4734" n="e" s="T476">bü </ts>
               <ts e="T478" id="Seg_4736" n="e" s="T477">ej </ts>
               <ts e="T479" id="Seg_4738" n="e" s="T478">bĭtlie. </ts>
               <ts e="T480" id="Seg_4740" n="e" s="T479">(Nu- </ts>
               <ts e="T481" id="Seg_4742" n="e" s="T480">nuldʼ-) </ts>
               <ts e="T482" id="Seg_4744" n="e" s="T481">Nuldeʔ </ts>
               <ts e="T483" id="Seg_4746" n="e" s="T482">bostə </ts>
               <ts e="T484" id="Seg_4748" n="e" s="T483">ine, </ts>
               <ts e="T485" id="Seg_4750" n="e" s="T484">dĭ </ts>
               <ts e="T486" id="Seg_4752" n="e" s="T485">padʼi </ts>
               <ts e="T487" id="Seg_4754" n="e" s="T486">ej </ts>
               <ts e="T488" id="Seg_4756" n="e" s="T487">nulia. </ts>
               <ts e="T489" id="Seg_4758" n="e" s="T488">Nada </ts>
               <ts e="T490" id="Seg_4760" n="e" s="T489">dĭm </ts>
               <ts e="T491" id="Seg_4762" n="e" s="T490">nuldəsʼtə. </ts>
               <ts e="T492" id="Seg_4764" n="e" s="T491">Nʼi </ts>
               <ts e="T493" id="Seg_4766" n="e" s="T492">koʔbdozi </ts>
               <ts e="T494" id="Seg_4768" n="e" s="T493">bar </ts>
               <ts e="T495" id="Seg_4770" n="e" s="T494">özerbiʔi. </ts>
               <ts e="T496" id="Seg_4772" n="e" s="T495">Dĭgəttə </ts>
               <ts e="T497" id="Seg_4774" n="e" s="T496">nʼi </ts>
               <ts e="T498" id="Seg_4776" n="e" s="T497">(mămbi): </ts>
               <ts e="T499" id="Seg_4778" n="e" s="T498">(M-) </ts>
               <ts e="T500" id="Seg_4780" n="e" s="T499">Măna </ts>
               <ts e="T501" id="Seg_4782" n="e" s="T500">kalal </ts>
               <ts e="T502" id="Seg_4784" n="e" s="T501">tibinə? </ts>
               <ts e="T503" id="Seg_4786" n="e" s="T502">Kalam. </ts>
               <ts e="T504" id="Seg_4788" n="e" s="T503">Nu, </ts>
               <ts e="T505" id="Seg_4790" n="e" s="T504">măn </ts>
               <ts e="T506" id="Seg_4792" n="e" s="T505">iam </ts>
               <ts e="T507" id="Seg_4794" n="e" s="T506">abam </ts>
               <ts e="T508" id="Seg_4796" n="e" s="T507">öʔlim, </ts>
               <ts e="T509" id="Seg_4798" n="e" s="T508">tănan. </ts>
               <ts e="T510" id="Seg_4800" n="e" s="T509">Dĭgəttə </ts>
               <ts e="T511" id="Seg_4802" n="e" s="T510">(iat) </ts>
               <ts e="T512" id="Seg_4804" n="e" s="T511">iam </ts>
               <ts e="T513" id="Seg_4806" n="e" s="T512">abam </ts>
               <ts e="T514" id="Seg_4808" n="e" s="T513">öʔlubi, </ts>
               <ts e="T515" id="Seg_4810" n="e" s="T514">dĭzeŋ </ts>
               <ts e="T516" id="Seg_4812" n="e" s="T515">šobiʔi. </ts>
               <ts e="T517" id="Seg_4814" n="e" s="T516">Koʔbdon </ts>
               <ts e="T518" id="Seg_4816" n="e" s="T517">iazi </ts>
               <ts e="T519" id="Seg_4818" n="e" s="T518">abazi </ts>
               <ts e="T520" id="Seg_4820" n="e" s="T519">dʼăbaktərluʔpiʔi. </ts>
               <ts e="T521" id="Seg_4822" n="e" s="T520">Koʔbdol </ts>
               <ts e="T522" id="Seg_4824" n="e" s="T521">(mĭbiʔ-) </ts>
               <ts e="T523" id="Seg_4826" n="e" s="T522">mĭbileʔ </ts>
               <ts e="T524" id="Seg_4828" n="e" s="T523">(măn= </ts>
               <ts e="T525" id="Seg_4830" n="e" s="T524">nʼi-) </ts>
               <ts e="T526" id="Seg_4832" n="e" s="T525">măn </ts>
               <ts e="T527" id="Seg_4834" n="e" s="T526">nʼinə? </ts>
               <ts e="T528" id="Seg_4836" n="e" s="T527">(Nu-) </ts>
               <ts e="T529" id="Seg_4838" n="e" s="T528">Mĭbibeʔ. </ts>
               <ts e="T530" id="Seg_4840" n="e" s="T529">(Dĭb-) </ts>
               <ts e="T531" id="Seg_4842" n="e" s="T530">Dĭgəttə </ts>
               <ts e="T532" id="Seg_4844" n="e" s="T531">koʔbdom </ts>
               <ts e="T533" id="Seg_4846" n="e" s="T532">surarbiʔi: </ts>
               <ts e="T534" id="Seg_4848" n="e" s="T533">Tăn </ts>
               <ts e="T535" id="Seg_4850" n="e" s="T534">kalal </ts>
               <ts e="T536" id="Seg_4852" n="e" s="T535">miʔ </ts>
               <ts e="T537" id="Seg_4854" n="e" s="T536">nʼinə </ts>
               <ts e="T538" id="Seg_4856" n="e" s="T537">tibinə? </ts>
               <ts e="T539" id="Seg_4858" n="e" s="T538">Kalam. </ts>
               <ts e="T540" id="Seg_4860" n="e" s="T539">Dĭgəttə </ts>
               <ts e="T541" id="Seg_4862" n="e" s="T540">(monoʔkobiʔi). </ts>
               <ts e="T542" id="Seg_4864" n="e" s="T541">Ara </ts>
               <ts e="T543" id="Seg_4866" n="e" s="T542">deʔpiʔi, </ts>
               <ts e="T544" id="Seg_4868" n="e" s="T543">ipek </ts>
               <ts e="T545" id="Seg_4870" n="e" s="T544">deʔpiʔi, </ts>
               <ts e="T546" id="Seg_4872" n="e" s="T545">uja. </ts>
               <ts e="T547" id="Seg_4874" n="e" s="T546">Iʔgö </ts>
               <ts e="T548" id="Seg_4876" n="e" s="T547">ibi. </ts>
               <ts e="T549" id="Seg_4878" n="e" s="T548">Dĭgəttə </ts>
               <ts e="T550" id="Seg_4880" n="e" s="T549">il </ts>
               <ts e="T551" id="Seg_4882" n="e" s="T550">šobiʔi. </ts>
               <ts e="T552" id="Seg_4884" n="e" s="T551">Ara </ts>
               <ts e="T553" id="Seg_4886" n="e" s="T552">bĭʔpiʔi </ts>
               <ts e="T554" id="Seg_4888" n="e" s="T553">bar. </ts>
               <ts e="T555" id="Seg_4890" n="e" s="T554">Dĭgəttə </ts>
               <ts e="T556" id="Seg_4892" n="e" s="T555">(nʼe-) </ts>
               <ts e="T557" id="Seg_4894" n="e" s="T556">несколько </ts>
               <ts e="T558" id="Seg_4896" n="e" s="T557">dʼala </ts>
               <ts e="T559" id="Seg_4898" n="e" s="T558">kambi. </ts>
               <ts e="T560" id="Seg_4900" n="e" s="T559">Dĭzeŋ </ts>
               <ts e="T561" id="Seg_4902" n="e" s="T560">svʼetogəʔjə </ts>
               <ts e="T562" id="Seg_4904" n="e" s="T561">šerbiʔi. </ts>
               <ts e="T563" id="Seg_4906" n="e" s="T562">I </ts>
               <ts e="T564" id="Seg_4908" n="e" s="T563">tʼegermaʔnə </ts>
               <ts e="T565" id="Seg_4910" n="e" s="T564">kambiʔi. </ts>
               <ts e="T566" id="Seg_4912" n="e" s="T565">Dĭn </ts>
               <ts e="T567" id="Seg_4914" n="e" s="T566">abəs </ts>
               <ts e="T568" id="Seg_4916" n="e" s="T567">(dĭzem) </ts>
               <ts e="T569" id="Seg_4918" n="e" s="T568">kudaj </ts>
               <ts e="T570" id="Seg_4920" n="e" s="T569">numan üzəbi. </ts>
               <ts e="T571" id="Seg_4922" n="e" s="T570">Dĭgəttə </ts>
               <ts e="T572" id="Seg_4924" n="e" s="T571">(vʼe- </ts>
               <ts e="T573" id="Seg_4926" n="e" s="T572">vʼenogə-) </ts>
               <ts e="T574" id="Seg_4928" n="e" s="T573">vʼenogəʔi </ts>
               <ts e="T575" id="Seg_4930" n="e" s="T574">šerbiʔi. </ts>
               <ts e="T576" id="Seg_4932" n="e" s="T575">Dĭgəttə </ts>
               <ts e="T577" id="Seg_4934" n="e" s="T576">maːndə </ts>
               <ts e="T578" id="Seg_4936" n="e" s="T577">šobiʔi. </ts>
               <ts e="T579" id="Seg_4938" n="e" s="T578">Tože </ts>
               <ts e="T580" id="Seg_4940" n="e" s="T579">bar </ts>
               <ts e="T581" id="Seg_4942" n="e" s="T580">ara </ts>
               <ts e="T582" id="Seg_4944" n="e" s="T581">bĭʔpiʔi. </ts>
               <ts e="T583" id="Seg_4946" n="e" s="T582">Koʔbdon </ts>
               <ts e="T584" id="Seg_4948" n="e" s="T583">tugandə </ts>
               <ts e="T585" id="Seg_4950" n="e" s="T584">šobiʔi. </ts>
               <ts e="T586" id="Seg_4952" n="e" s="T585">I </ts>
               <ts e="T587" id="Seg_4954" n="e" s="T586">nʼin </ts>
               <ts e="T588" id="Seg_4956" n="e" s="T587">koʔbdo </ts>
               <ts e="T589" id="Seg_4958" n="e" s="T588">šobiʔi. </ts>
               <ts e="T590" id="Seg_4960" n="e" s="T589">Iʔgö </ts>
               <ts e="T591" id="Seg_4962" n="e" s="T590">il </ts>
               <ts e="T592" id="Seg_4964" n="e" s="T591">ibiʔi. </ts>
               <ts e="T593" id="Seg_4966" n="e" s="T592">Ara </ts>
               <ts e="T594" id="Seg_4968" n="e" s="T593">iʔgö </ts>
               <ts e="T595" id="Seg_4970" n="e" s="T594">ibi. </ts>
               <ts e="T596" id="Seg_4972" n="e" s="T595">(Stolgən) </ts>
               <ts e="T597" id="Seg_4974" n="e" s="T596">ugandə </ts>
               <ts e="T598" id="Seg_4976" n="e" s="T597">iʔgö </ts>
               <ts e="T599" id="Seg_4978" n="e" s="T598">ĭmbi </ts>
               <ts e="T600" id="Seg_4980" n="e" s="T599">(iʔb-) </ts>
               <ts e="T601" id="Seg_4982" n="e" s="T600">iʔbolaʔbə. </ts>
               <ts e="T602" id="Seg_4984" n="e" s="T601">Kamən </ts>
               <ts e="T603" id="Seg_4986" n="e" s="T602">koʔbdo </ts>
               <ts e="T604" id="Seg_4988" n="e" s="T603">monoʔkobiʔi, </ts>
               <ts e="T605" id="Seg_4990" n="e" s="T604">iat </ts>
               <ts e="T606" id="Seg_4992" n="e" s="T605">abat </ts>
               <ts e="T607" id="Seg_4994" n="e" s="T606">mămbiʔi: </ts>
               <ts e="T608" id="Seg_4996" n="e" s="T607">Dĭn </ts>
               <ts e="T609" id="Seg_4998" n="e" s="T608">oldʼat </ts>
               <ts e="T610" id="Seg_5000" n="e" s="T609">naga. </ts>
               <ts e="T611" id="Seg_5002" n="e" s="T610">Nada </ts>
               <ts e="T612" id="Seg_5004" n="e" s="T611">bătʼinkăʔi </ts>
               <ts e="T613" id="Seg_5006" n="e" s="T612">izittə, </ts>
               <ts e="T614" id="Seg_5008" n="e" s="T613">nada </ts>
               <ts e="T615" id="Seg_5010" n="e" s="T614">palʼto </ts>
               <ts e="T616" id="Seg_5012" n="e" s="T615">izittə, </ts>
               <ts e="T617" id="Seg_5014" n="e" s="T616">plat </ts>
               <ts e="T618" id="Seg_5016" n="e" s="T617">izittə. </ts>
               <ts e="T619" id="Seg_5018" n="e" s="T618">Oldʼat </ts>
               <ts e="T620" id="Seg_5020" n="e" s="T619">amga, </ts>
               <ts e="T621" id="Seg_5022" n="e" s="T620">kujnektə </ts>
               <ts e="T622" id="Seg_5024" n="e" s="T621">naga. </ts>
               <ts e="T623" id="Seg_5026" n="e" s="T622">Dĭgəttə </ts>
               <ts e="T624" id="Seg_5028" n="e" s="T623">(dĭʔ-) </ts>
               <ts e="T625" id="Seg_5030" n="e" s="T624">dĭzeŋ </ts>
               <ts e="T626" id="Seg_5032" n="e" s="T625">aktʼam </ts>
               <ts e="T627" id="Seg_5034" n="e" s="T626">ibiʔi. </ts>
               <ts e="T628" id="Seg_5036" n="e" s="T627">Šide </ts>
               <ts e="T629" id="Seg_5038" n="e" s="T628">bʼeʔ </ts>
               <ts e="T630" id="Seg_5040" n="e" s="T629">sumna. </ts>
               <ts e="T631" id="Seg_5042" n="e" s="T630">Dĭgəttə </ts>
               <ts e="T632" id="Seg_5044" n="e" s="T631">iat </ts>
               <ts e="T633" id="Seg_5046" n="e" s="T632">aban </ts>
               <ts e="T634" id="Seg_5048" n="e" s="T633">dĭʔnə </ts>
               <ts e="T635" id="Seg_5050" n="e" s="T634">mĭbiʔi </ts>
               <ts e="T636" id="Seg_5052" n="e" s="T635">tüžöj. </ts>
               <ts e="T637" id="Seg_5054" n="e" s="T636">Tugazaŋdən </ts>
               <ts e="T638" id="Seg_5056" n="e" s="T637">mĭbiʔi </ts>
               <ts e="T639" id="Seg_5058" n="e" s="T638">ular, </ts>
               <ts e="T640" id="Seg_5060" n="e" s="T639">mĭbiʔi </ts>
               <ts e="T641" id="Seg_5062" n="e" s="T640">ineʔi. </ts>
               <ts e="T642" id="Seg_5064" n="e" s="T641">Aktʼa </ts>
               <ts e="T643" id="Seg_5066" n="e" s="T642">mĭbiʔi </ts>
               <ts e="T644" id="Seg_5068" n="e" s="T643">ugandə, </ts>
               <ts e="T645" id="Seg_5070" n="e" s="T644">iʔgö. </ts>
               <ts e="T646" id="Seg_5072" n="e" s="T645">(I-) </ts>
               <ts e="T647" id="Seg_5074" n="e" s="T646">Il </ts>
               <ts e="T648" id="Seg_5076" n="e" s="T647">iʔgö </ts>
               <ts e="T649" id="Seg_5078" n="e" s="T648">ibiʔi. </ts>
               <ts e="T650" id="Seg_5080" n="e" s="T649">Dĭgəttə </ts>
               <ts e="T651" id="Seg_5082" n="e" s="T650">dĭzeŋ </ts>
               <ts e="T652" id="Seg_5084" n="e" s="T651">kondʼo </ts>
               <ts e="T653" id="Seg_5086" n="e" s="T652">amnobiʔi. </ts>
               <ts e="T654" id="Seg_5088" n="e" s="T653">Kagat </ts>
               <ts e="T655" id="Seg_5090" n="e" s="T654">(išo </ts>
               <ts e="T656" id="Seg_5092" n="e" s="T655">serbi </ts>
               <ts e="T657" id="Seg_5094" n="e" s="T656">da). </ts>
               <ts e="T658" id="Seg_5096" n="e" s="T657">Bazoʔ </ts>
               <ts e="T659" id="Seg_5098" n="e" s="T658">(dăre) </ts>
               <ts e="T660" id="Seg_5100" n="e" s="T659">monoʔkobi. </ts>
               <ts e="T661" id="Seg_5102" n="e" s="T660">Dĭgəttə </ts>
               <ts e="T662" id="Seg_5104" n="e" s="T661">dĭ </ts>
               <ts e="T969" id="Seg_5106" n="e" s="T662">kalla </ts>
               <ts e="T663" id="Seg_5108" n="e" s="T969">dʼürbi, </ts>
               <ts e="T664" id="Seg_5110" n="e" s="T663">dĭʔnə </ts>
               <ts e="T665" id="Seg_5112" n="e" s="T664">abat </ts>
               <ts e="T666" id="Seg_5114" n="e" s="T665">mĭbi </ts>
               <ts e="T667" id="Seg_5116" n="e" s="T666">bar. </ts>
               <ts e="T668" id="Seg_5118" n="e" s="T667">Bar </ts>
               <ts e="T669" id="Seg_5120" n="e" s="T668">ĭmbi </ts>
               <ts e="T670" id="Seg_5122" n="e" s="T669">mĭbi, </ts>
               <ts e="T671" id="Seg_5124" n="e" s="T670">ine </ts>
               <ts e="T672" id="Seg_5126" n="e" s="T671">mĭbi, </ts>
               <ts e="T673" id="Seg_5128" n="e" s="T672">ular </ts>
               <ts e="T674" id="Seg_5130" n="e" s="T673">mĭbi, </ts>
               <ts e="T675" id="Seg_5132" n="e" s="T674">tüžöjʔi </ts>
               <ts e="T676" id="Seg_5134" n="e" s="T675">mĭbi. </ts>
               <ts e="T677" id="Seg_5136" n="e" s="T676">Ipek </ts>
               <ts e="T678" id="Seg_5138" n="e" s="T677">mĭbi, </ts>
               <ts e="T679" id="Seg_5140" n="e" s="T678">(tu-) </ts>
               <ts e="T680" id="Seg_5142" n="e" s="T679">tura </ts>
               <ts e="T681" id="Seg_5144" n="e" s="T680">nuldəbi. </ts>
               <ts e="T682" id="Seg_5146" n="e" s="T681">Dĭzeŋ </ts>
               <ts e="T683" id="Seg_5148" n="e" s="T682">(kan- </ts>
               <ts e="T684" id="Seg_5150" n="e" s="T683">kanzə-) </ts>
               <ts e="T685" id="Seg_5152" n="e" s="T684">kambiʔi </ts>
               <ts e="T686" id="Seg_5154" n="e" s="T685">венчаться </ts>
               <ts e="T687" id="Seg_5156" n="e" s="T686">ineʔizi. </ts>
               <ts e="T688" id="Seg_5158" n="e" s="T687">Koʔbdo </ts>
               <ts e="T689" id="Seg_5160" n="e" s="T688">bostə </ts>
               <ts e="T690" id="Seg_5162" n="e" s="T689">kazak </ts>
               <ts e="T691" id="Seg_5164" n="e" s="T690">iat, </ts>
               <ts e="T692" id="Seg_5166" n="e" s="T691">a </ts>
               <ts e="T693" id="Seg_5168" n="e" s="T692">nʼi </ts>
               <ts e="T694" id="Seg_5170" n="e" s="T693">bostə </ts>
               <ts e="T695" id="Seg_5172" n="e" s="T694">kazak </ts>
               <ts e="T696" id="Seg_5174" n="e" s="T695">iat, </ts>
               <ts e="T697" id="Seg_5176" n="e" s="T696">kazak </ts>
               <ts e="T698" id="Seg_5178" n="e" s="T697">abat. </ts>
               <ts e="T699" id="Seg_5180" n="e" s="T698">Dibər </ts>
               <ts e="T700" id="Seg_5182" n="e" s="T699">kambiʔi, </ts>
               <ts e="T701" id="Seg_5184" n="e" s="T700">dĭgəttə </ts>
               <ts e="T702" id="Seg_5186" n="e" s="T701">šobiʔi </ts>
               <ts e="T703" id="Seg_5188" n="e" s="T702">maːʔndə. </ts>
               <ts e="T704" id="Seg_5190" n="e" s="T703">A </ts>
               <ts e="T705" id="Seg_5192" n="e" s="T704">dĭgəttə </ts>
               <ts e="T706" id="Seg_5194" n="e" s="T705">koʔbdon </ts>
               <ts e="T707" id="Seg_5196" n="e" s="T706">kazak </ts>
               <ts e="T708" id="Seg_5198" n="e" s="T707">abat </ts>
               <ts e="T709" id="Seg_5200" n="e" s="T708">(odʼa-) </ts>
               <ts e="T710" id="Seg_5202" n="e" s="T709">oldʼa </ts>
               <ts e="T711" id="Seg_5204" n="e" s="T710">deʔpi </ts>
               <ts e="T712" id="Seg_5206" n="e" s="T711">ящик </ts>
               <ts e="T713" id="Seg_5208" n="e" s="T712">bar. </ts>
               <ts e="T714" id="Seg_5210" n="e" s="T713">Păduškăʔi </ts>
               <ts e="T715" id="Seg_5212" n="e" s="T714">deʔpi </ts>
               <ts e="T716" id="Seg_5214" n="e" s="T715">bar. </ts>
               <ts e="T717" id="Seg_5216" n="e" s="T716">Pʼerină </ts>
               <ts e="T718" id="Seg_5218" n="e" s="T717">deʔpi. </ts>
               <ts e="T719" id="Seg_5220" n="e" s="T718">Dĭgəttə </ts>
               <ts e="T720" id="Seg_5222" n="e" s="T719">šobiʔi </ts>
               <ts e="T721" id="Seg_5224" n="e" s="T720">tʼegermaʔtə. </ts>
               <ts e="T722" id="Seg_5226" n="e" s="T721">Dĭzeŋ </ts>
               <ts e="T723" id="Seg_5228" n="e" s="T722">amnobiʔi </ts>
               <ts e="T724" id="Seg_5230" n="e" s="T723">(stozə-) </ts>
               <ts e="T725" id="Seg_5232" n="e" s="T724">stoldə. </ts>
               <ts e="T727" id="Seg_5234" n="e" s="T725">Dĭgəttə </ts>
               <ts e="T728" id="Seg_5236" n="e" s="T727">nʼin </ts>
               <ts e="T729" id="Seg_5238" n="e" s="T728">kujnektə </ts>
               <ts e="T730" id="Seg_5240" n="e" s="T729">kuvas, </ts>
               <ts e="T731" id="Seg_5242" n="e" s="T730">toʔbdə. </ts>
               <ts e="T732" id="Seg_5244" n="e" s="T731">Koʔbdon </ts>
               <ts e="T733" id="Seg_5246" n="e" s="T732">oldʼat </ts>
               <ts e="T734" id="Seg_5248" n="e" s="T733">tože </ts>
               <ts e="T735" id="Seg_5250" n="e" s="T734">kuvas. </ts>
               <ts e="T736" id="Seg_5252" n="e" s="T735">Toʔbdə. </ts>
               <ts e="T737" id="Seg_5254" n="e" s="T736">Ulut </ts>
               <ts e="T738" id="Seg_5256" n="e" s="T737">bar </ts>
               <ts e="T739" id="Seg_5258" n="e" s="T738">kuvas, </ts>
               <ts e="T740" id="Seg_5260" n="e" s="T739">vʼenok </ts>
               <ts e="T741" id="Seg_5262" n="e" s="T740">šerbiʔi. </ts>
               <ts e="T742" id="Seg_5264" n="e" s="T741">Dĭgəttə </ts>
               <ts e="T743" id="Seg_5266" n="e" s="T742">dĭʔnə </ts>
               <ts e="T744" id="Seg_5268" n="e" s="T743">(o- </ts>
               <ts e="T745" id="Seg_5270" n="e" s="T744">o- </ts>
               <ts e="T746" id="Seg_5272" n="e" s="T745">)… </ts>
               <ts e="T747" id="Seg_5274" n="e" s="T746">Dĭgəttə </ts>
               <ts e="T748" id="Seg_5276" n="e" s="T747">dĭʔnə </ts>
               <ts e="T749" id="Seg_5278" n="e" s="T748">šide </ts>
               <ts e="T750" id="Seg_5280" n="e" s="T749">kosa </ts>
               <ts e="T751" id="Seg_5282" n="e" s="T750">kürbiʔi. </ts>
               <ts e="T752" id="Seg_5284" n="e" s="T751">Dĭgəttə </ts>
               <ts e="T753" id="Seg_5286" n="e" s="T752">sarbiʔi </ts>
               <ts e="T754" id="Seg_5288" n="e" s="T753">ulut. </ts>
               <ts e="T755" id="Seg_5290" n="e" s="T754">Kamən </ts>
               <ts e="T756" id="Seg_5292" n="e" s="T755">dĭ </ts>
               <ts e="T757" id="Seg_5294" n="e" s="T756">koʔbdo </ts>
               <ts e="T758" id="Seg_5296" n="e" s="T757">ibi, </ts>
               <ts e="T759" id="Seg_5298" n="e" s="T758">onʼiʔ </ts>
               <ts e="T760" id="Seg_5300" n="e" s="T759">(koza-) </ts>
               <ts e="T761" id="Seg_5302" n="e" s="T760">kosa </ts>
               <ts e="T762" id="Seg_5304" n="e" s="T761">ibi. </ts>
               <ts e="T763" id="Seg_5306" n="e" s="T762">Dĭgəttə </ts>
               <ts e="T764" id="Seg_5308" n="e" s="T763">(s- </ts>
               <ts e="T765" id="Seg_5310" n="e" s="T764">kazak </ts>
               <ts e="T766" id="Seg_5312" n="e" s="T765">iat=) </ts>
               <ts e="T767" id="Seg_5314" n="e" s="T766">dĭ </ts>
               <ts e="T768" id="Seg_5316" n="e" s="T767">koʔbdon </ts>
               <ts e="T769" id="Seg_5318" n="e" s="T768">kazak </ts>
               <ts e="T770" id="Seg_5320" n="e" s="T769">iat </ts>
               <ts e="T771" id="Seg_5322" n="e" s="T770">i </ts>
               <ts e="T772" id="Seg_5324" n="e" s="T771">nʼin </ts>
               <ts e="T773" id="Seg_5326" n="e" s="T772">kazak </ts>
               <ts e="T774" id="Seg_5328" n="e" s="T773">iat </ts>
               <ts e="T775" id="Seg_5330" n="e" s="T774">dĭʔnə </ts>
               <ts e="T776" id="Seg_5332" n="e" s="T775">šide </ts>
               <ts e="T777" id="Seg_5334" n="e" s="T776">kürbiʔi. </ts>
               <ts e="T778" id="Seg_5336" n="e" s="T777">I </ts>
               <ts e="T779" id="Seg_5338" n="e" s="T778">sarbiʔi </ts>
               <ts e="T780" id="Seg_5340" n="e" s="T779">ulut. </ts>
               <ts e="T781" id="Seg_5342" n="e" s="T780">Dĭzeŋdə </ts>
               <ts e="T782" id="Seg_5344" n="e" s="T781">nʼi </ts>
               <ts e="T783" id="Seg_5346" n="e" s="T782">koʔbdom </ts>
               <ts e="T784" id="Seg_5348" n="e" s="T783">kazak </ts>
               <ts e="T785" id="Seg_5350" n="e" s="T784">abat </ts>
               <ts e="T786" id="Seg_5352" n="e" s="T785">(ku-) </ts>
               <ts e="T787" id="Seg_5354" n="e" s="T786">kumbi </ts>
               <ts e="T788" id="Seg_5356" n="e" s="T787">turanə. </ts>
               <ts e="T789" id="Seg_5358" n="e" s="T788">Dĭn </ts>
               <ts e="T790" id="Seg_5360" n="e" s="T789">šindidə </ts>
               <ts e="T791" id="Seg_5362" n="e" s="T790">ej </ts>
               <ts e="T792" id="Seg_5364" n="e" s="T791">ibi. </ts>
               <ts e="T793" id="Seg_5366" n="e" s="T792">Iʔbəbi </ts>
               <ts e="T794" id="Seg_5368" n="e" s="T793">(ʔi) </ts>
               <ts e="T795" id="Seg_5370" n="e" s="T794">kunolzittə </ts>
               <ts e="T796" id="Seg_5372" n="e" s="T795">i </ts>
               <ts e="T797" id="Seg_5374" n="e" s="T796">kamroluʔpiʔi. </ts>
               <ts e="T798" id="Seg_5376" n="e" s="T797">(Tugazaŋ-) </ts>
               <ts e="T799" id="Seg_5378" n="e" s="T798">Tuganzaŋdə </ts>
               <ts e="T800" id="Seg_5380" n="e" s="T799">dĭzeŋdə </ts>
               <ts e="T801" id="Seg_5382" n="e" s="T800">mămbiʔi: </ts>
               <ts e="T802" id="Seg_5384" n="e" s="T801">Jakšə </ts>
               <ts e="T803" id="Seg_5386" n="e" s="T802">(amnolaʔ=) </ts>
               <ts e="T804" id="Seg_5388" n="e" s="T803">amnogaʔ. </ts>
               <ts e="T805" id="Seg_5390" n="e" s="T804">Iʔ </ts>
               <ts e="T806" id="Seg_5392" n="e" s="T805">dʼabərogaʔ. </ts>
               <ts e="T807" id="Seg_5394" n="e" s="T806">(Iʔ </ts>
               <ts e="T808" id="Seg_5396" n="e" s="T807">kundonz- </ts>
               <ts e="T809" id="Seg_5398" n="e" s="T808">iʔ </ts>
               <ts e="T810" id="Seg_5400" n="e" s="T809">kudonzlaʔ-) </ts>
               <ts e="T811" id="Seg_5402" n="e" s="T810">Iʔ </ts>
               <ts e="T812" id="Seg_5404" n="e" s="T811">kudonzaʔ. </ts>
               <ts e="T813" id="Seg_5406" n="e" s="T812">Puskaj </ts>
               <ts e="T814" id="Seg_5408" n="e" s="T813">(esseŋgə-) </ts>
               <ts e="T815" id="Seg_5410" n="e" s="T814">esseŋdə </ts>
               <ts e="T816" id="Seg_5412" n="e" s="T815">iʔgö </ts>
               <ts e="T817" id="Seg_5414" n="e" s="T816">molaj, </ts>
               <ts e="T818" id="Seg_5416" n="e" s="T817">nʼizeŋ, </ts>
               <ts e="T819" id="Seg_5418" n="e" s="T818">koʔbsaŋ. </ts>
               <ts e="T820" id="Seg_5420" n="e" s="T819">Dĭgəttə </ts>
               <ts e="T821" id="Seg_5422" n="e" s="T820">dĭzeŋ </ts>
               <ts e="T822" id="Seg_5424" n="e" s="T821">abəs </ts>
               <ts e="T823" id="Seg_5426" n="e" s="T822">kăštəbiʔi. </ts>
               <ts e="T824" id="Seg_5428" n="e" s="T823">Dĭ </ts>
               <ts e="T825" id="Seg_5430" n="e" s="T824">ugandə </ts>
               <ts e="T826" id="Seg_5432" n="e" s="T825">iʔgö </ts>
               <ts e="T827" id="Seg_5434" n="e" s="T826">ara </ts>
               <ts e="T828" id="Seg_5436" n="e" s="T827">bĭʔpi. </ts>
               <ts e="T829" id="Seg_5438" n="e" s="T828">I </ts>
               <ts e="T830" id="Seg_5440" n="e" s="T829">saʔməluʔpi. </ts>
               <ts e="T831" id="Seg_5442" n="e" s="T830">Dĭm </ts>
               <ts e="T832" id="Seg_5444" n="e" s="T831">kunnaːlbiʔi. </ts>
               <ts e="T833" id="Seg_5446" n="e" s="T832">Dĭzeŋ </ts>
               <ts e="T834" id="Seg_5448" n="e" s="T833">ugandə </ts>
               <ts e="T835" id="Seg_5450" n="e" s="T834">ara </ts>
               <ts e="T836" id="Seg_5452" n="e" s="T835">bĭʔpiʔi. </ts>
               <ts e="T837" id="Seg_5454" n="e" s="T836">Kondʼo, </ts>
               <ts e="T838" id="Seg_5456" n="e" s="T837">muktuʔ </ts>
               <ts e="T839" id="Seg_5458" n="e" s="T838">dʼala </ts>
               <ts e="T840" id="Seg_5460" n="e" s="T839">bĭʔpiʔi. </ts>
               <ts e="T841" id="Seg_5462" n="e" s="T840">Dĭgəttə </ts>
               <ts e="T842" id="Seg_5464" n="e" s="T841">dĭzeŋ </ts>
               <ts e="T843" id="Seg_5466" n="e" s="T842">bar </ts>
               <ts e="T844" id="Seg_5468" n="e" s="T843">amnobiʔi. </ts>
               <ts e="T845" id="Seg_5470" n="e" s="T844">Dĭzeŋ </ts>
               <ts e="T846" id="Seg_5472" n="e" s="T845">bar </ts>
               <ts e="T847" id="Seg_5474" n="e" s="T846">esseŋdə </ts>
               <ts e="T848" id="Seg_5476" n="e" s="T847">ibiʔi. </ts>
               <ts e="T849" id="Seg_5478" n="e" s="T848">Nʼizeŋ </ts>
               <ts e="T850" id="Seg_5480" n="e" s="T849">ibiʔi, </ts>
               <ts e="T851" id="Seg_5482" n="e" s="T850">koʔbtaŋ </ts>
               <ts e="T852" id="Seg_5484" n="e" s="T851">ibiʔi. </ts>
               <ts e="T853" id="Seg_5486" n="e" s="T852">Koʔpsaŋ </ts>
               <ts e="T854" id="Seg_5488" n="e" s="T853">((DMG)). </ts>
               <ts e="T855" id="Seg_5490" n="e" s="T854">((…)). </ts>
               <ts e="T857" id="Seg_5492" n="e" s="T855">(Măn </ts>
               <ts e="T858" id="Seg_5494" n="e" s="T857">nʼim) </ts>
               <ts e="T859" id="Seg_5496" n="e" s="T858">taldʼen </ts>
               <ts e="T860" id="Seg_5498" n="e" s="T859">kambi, </ts>
               <ts e="T861" id="Seg_5500" n="e" s="T860">ara </ts>
               <ts e="T862" id="Seg_5502" n="e" s="T861">bĭʔpi. </ts>
               <ts e="T863" id="Seg_5504" n="e" s="T862">Ugandə </ts>
               <ts e="T864" id="Seg_5506" n="e" s="T863">kondʼo </ts>
               <ts e="T865" id="Seg_5508" n="e" s="T864">ej </ts>
               <ts e="T866" id="Seg_5510" n="e" s="T865">šobi </ts>
               <ts e="T867" id="Seg_5512" n="e" s="T866">maʔnə. </ts>
               <ts e="T868" id="Seg_5514" n="e" s="T867">Dĭgəttə </ts>
               <ts e="T869" id="Seg_5516" n="e" s="T868">šobi, </ts>
               <ts e="T870" id="Seg_5518" n="e" s="T869">kunolluʔpi. </ts>
               <ts e="T871" id="Seg_5520" n="e" s="T870">I </ts>
               <ts e="T872" id="Seg_5522" n="e" s="T871">ertən </ts>
               <ts e="T873" id="Seg_5524" n="e" s="T872">uʔbdəbi </ts>
               <ts e="T874" id="Seg_5526" n="e" s="T873">i </ts>
               <ts e="T875" id="Seg_5528" n="e" s="T874">moluʔpi </ts>
               <ts e="T876" id="Seg_5530" n="e" s="T875">togonorzittə. </ts>
               <ts e="T877" id="Seg_5532" n="e" s="T876">(A </ts>
               <ts e="T878" id="Seg_5534" n="e" s="T877">măna </ts>
               <ts e="T879" id="Seg_5536" n="e" s="T878">prišl-) </ts>
               <ts e="T880" id="Seg_5538" n="e" s="T879">Măn </ts>
               <ts e="T881" id="Seg_5540" n="e" s="T880">šide </ts>
               <ts e="T882" id="Seg_5542" n="e" s="T881">tüžöj </ts>
               <ts e="T883" id="Seg_5544" n="e" s="T882">surdəbiam. </ts>
               <ts e="T884" id="Seg_5546" n="e" s="T883">Dĭn </ts>
               <ts e="T885" id="Seg_5548" n="e" s="T884">tüžöjdə </ts>
               <ts e="T886" id="Seg_5550" n="e" s="T885">i </ts>
               <ts e="T887" id="Seg_5552" n="e" s="T886">bostə </ts>
               <ts e="T888" id="Seg_5554" n="e" s="T887">tüžöjdə. </ts>
               <ts e="T889" id="Seg_5556" n="e" s="T888">Onʼiʔ </ts>
               <ts e="T890" id="Seg_5558" n="e" s="T889">ne </ts>
               <ts e="T891" id="Seg_5560" n="e" s="T890">nanəʔzəbi </ts>
               <ts e="T892" id="Seg_5562" n="e" s="T891">ibi. </ts>
               <ts e="T893" id="Seg_5564" n="e" s="T892">Dĭgəttə </ts>
               <ts e="T894" id="Seg_5566" n="e" s="T893">šidəʔpi. </ts>
               <ts e="T895" id="Seg_5568" n="e" s="T894">A </ts>
               <ts e="T896" id="Seg_5570" n="e" s="T895">dĭ </ts>
               <ts e="T897" id="Seg_5572" n="e" s="T896">dibər </ts>
               <ts e="T898" id="Seg_5574" n="e" s="T897">kambi </ts>
               <ts e="T899" id="Seg_5576" n="e" s="T898">da </ts>
               <ts e="T900" id="Seg_5578" n="e" s="T899">bĭʔpi </ts>
               <ts e="T901" id="Seg_5580" n="e" s="T900">ara. </ts>
               <ts e="T902" id="Seg_5582" n="e" s="T901">Măn </ts>
               <ts e="T903" id="Seg_5584" n="e" s="T902">taldʼen </ts>
               <ts e="T904" id="Seg_5586" n="e" s="T903">(ambam) </ts>
               <ts e="T905" id="Seg_5588" n="e" s="T904">(măndər-) </ts>
               <ts e="T906" id="Seg_5590" n="e" s="T905">măndobiam. </ts>
               <ts e="T907" id="Seg_5592" n="e" s="T906">A </ts>
               <ts e="T908" id="Seg_5594" n="e" s="T907">teinen </ts>
               <ts e="T910" id="Seg_5596" n="e" s="T908">nüdʼin </ts>
               <ts e="T911" id="Seg_5598" n="e" s="T910">teinen </ts>
               <ts e="T913" id="Seg_5600" n="e" s="T911">nüdʼin… </ts>
               <ts e="T914" id="Seg_5602" n="e" s="T913">Dĭgəttə </ts>
               <ts e="T915" id="Seg_5604" n="e" s="T914">dĭm </ts>
               <ts e="T916" id="Seg_5606" n="e" s="T915">dʼoduni </ts>
               <ts e="T917" id="Seg_5608" n="e" s="T916">kubiam, </ts>
               <ts e="T918" id="Seg_5610" n="e" s="T917">ĭmbidə </ts>
               <ts e="T919" id="Seg_5612" n="e" s="T918">deʔpi, </ts>
               <ts e="T920" id="Seg_5614" n="e" s="T919">a </ts>
               <ts e="T921" id="Seg_5616" n="e" s="T920">măn </ts>
               <ts e="T922" id="Seg_5618" n="e" s="T921">kurollaʔpiam </ts>
               <ts e="T923" id="Seg_5620" n="e" s="T922">bar. </ts>
               <ts e="T924" id="Seg_5622" n="e" s="T923">(Tăneldə) </ts>
               <ts e="T925" id="Seg_5624" n="e" s="T924">ej </ts>
               <ts e="T926" id="Seg_5626" n="e" s="T925">šobiʔi </ts>
               <ts e="T927" id="Seg_5628" n="e" s="T926">dʼijegə. </ts>
               <ts e="T928" id="Seg_5630" n="e" s="T927">Kamən </ts>
               <ts e="T929" id="Seg_5632" n="e" s="T928">dĭzeŋ </ts>
               <ts e="T930" id="Seg_5634" n="e" s="T929">šoləʔjə? </ts>
               <ts e="T931" id="Seg_5636" n="e" s="T930">Teinen </ts>
               <ts e="T932" id="Seg_5638" n="e" s="T931">šoləʔjə </ts>
               <ts e="T933" id="Seg_5640" n="e" s="T932">nüdʼində. </ts>
               <ts e="T934" id="Seg_5642" n="e" s="T933">Amorzittə </ts>
               <ts e="T935" id="Seg_5644" n="e" s="T934">nada </ts>
               <ts e="T936" id="Seg_5646" n="e" s="T935">dĭzeŋdə </ts>
               <ts e="T937" id="Seg_5648" n="e" s="T936">dĭgəttə </ts>
               <ts e="T938" id="Seg_5650" n="e" s="T937">mĭzittə. </ts>
               <ts e="T939" id="Seg_5652" n="e" s="T938">Dĭzeŋ </ts>
               <ts e="T940" id="Seg_5654" n="e" s="T939">bar </ts>
               <ts e="T941" id="Seg_5656" n="e" s="T940">püjöliaʔi. </ts>
               <ts e="T942" id="Seg_5658" n="e" s="T941">(Măn) </ts>
               <ts e="T943" id="Seg_5660" n="e" s="T942">teinen </ts>
               <ts e="T944" id="Seg_5662" n="e" s="T943">tenöbiam </ts>
               <ts e="T945" id="Seg_5664" n="e" s="T944">šiʔ </ts>
               <ts e="T946" id="Seg_5666" n="e" s="T945">bar </ts>
               <ts e="T947" id="Seg_5668" n="e" s="T946">šobilaʔ. </ts>
               <ts e="T948" id="Seg_5670" n="e" s="T947">A </ts>
               <ts e="T949" id="Seg_5672" n="e" s="T948">šiʔ </ts>
               <ts e="T950" id="Seg_5674" n="e" s="T949">ej </ts>
               <ts e="T951" id="Seg_5676" n="e" s="T950">šobilaʔ. </ts>
               <ts e="T952" id="Seg_5678" n="e" s="T951">Măn </ts>
               <ts e="T953" id="Seg_5680" n="e" s="T952">teinen </ts>
               <ts e="T954" id="Seg_5682" n="e" s="T953">(kə-) </ts>
               <ts e="T955" id="Seg_5684" n="e" s="T954">teinen </ts>
               <ts e="T956" id="Seg_5686" n="e" s="T955">(munəjʔjə) </ts>
               <ts e="T957" id="Seg_5688" n="e" s="T956">mĭnzərbiem. </ts>
               <ts e="T958" id="Seg_5690" n="e" s="T957">Kălba </ts>
               <ts e="T959" id="Seg_5692" n="e" s="T958">(dʼagarbiam), </ts>
               <ts e="T960" id="Seg_5694" n="e" s="T959">munujʔ </ts>
               <ts e="T961" id="Seg_5696" n="e" s="T960">embiem, </ts>
               <ts e="T962" id="Seg_5698" n="e" s="T961">(gorom) </ts>
               <ts e="T963" id="Seg_5700" n="e" s="T962">embiem, </ts>
               <ts e="T964" id="Seg_5702" n="e" s="T963">šiʔnʼileʔ </ts>
               <ts e="T965" id="Seg_5704" n="e" s="T964">bădəsʼtə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T2" id="Seg_5705" s="T0">PKZ_196X_SU0210.001 (001)</ta>
            <ta e="T9" id="Seg_5706" s="T2">PKZ_196X_SU0210.002 (002)</ta>
            <ta e="T13" id="Seg_5707" s="T9">PKZ_196X_SU0210.003 (003)</ta>
            <ta e="T26" id="Seg_5708" s="T13">PKZ_196X_SU0210.004 (004)</ta>
            <ta e="T38" id="Seg_5709" s="T26">PKZ_196X_SU0210.005 (005)</ta>
            <ta e="T42" id="Seg_5710" s="T38">PKZ_196X_SU0210.006 (006)</ta>
            <ta e="T44" id="Seg_5711" s="T42">PKZ_196X_SU0210.007 (007)</ta>
            <ta e="T48" id="Seg_5712" s="T44">PKZ_196X_SU0210.008 (008)</ta>
            <ta e="T51" id="Seg_5713" s="T48">PKZ_196X_SU0210.009 (009)</ta>
            <ta e="T53" id="Seg_5714" s="T51">PKZ_196X_SU0210.010 (010)</ta>
            <ta e="T56" id="Seg_5715" s="T53">PKZ_196X_SU0210.011 (011)</ta>
            <ta e="T59" id="Seg_5716" s="T56">PKZ_196X_SU0210.012 (012)</ta>
            <ta e="T62" id="Seg_5717" s="T59">PKZ_196X_SU0210.013 (013)</ta>
            <ta e="T65" id="Seg_5718" s="T62">PKZ_196X_SU0210.014 (014)</ta>
            <ta e="T68" id="Seg_5719" s="T65">PKZ_196X_SU0210.015 (015)</ta>
            <ta e="T71" id="Seg_5720" s="T68">PKZ_196X_SU0210.016 (016)</ta>
            <ta e="T73" id="Seg_5721" s="T71">PKZ_196X_SU0210.017 (017)</ta>
            <ta e="T78" id="Seg_5722" s="T73">PKZ_196X_SU0210.018 (018)</ta>
            <ta e="T81" id="Seg_5723" s="T78">PKZ_196X_SU0210.019 (019)</ta>
            <ta e="T85" id="Seg_5724" s="T81">PKZ_196X_SU0210.020 (020)</ta>
            <ta e="T90" id="Seg_5725" s="T85">PKZ_196X_SU0210.021 (021)</ta>
            <ta e="T92" id="Seg_5726" s="T90">PKZ_196X_SU0210.022 (022)</ta>
            <ta e="T94" id="Seg_5727" s="T92">PKZ_196X_SU0210.023 (023)</ta>
            <ta e="T101" id="Seg_5728" s="T94">PKZ_196X_SU0210.024 (024)</ta>
            <ta e="T103" id="Seg_5729" s="T101">PKZ_196X_SU0210.025 (025)</ta>
            <ta e="T105" id="Seg_5730" s="T103">PKZ_196X_SU0210.026 (026)</ta>
            <ta e="T109" id="Seg_5731" s="T105">PKZ_196X_SU0210.027 (027)</ta>
            <ta e="T113" id="Seg_5732" s="T109">PKZ_196X_SU0210.028 (028)</ta>
            <ta e="T118" id="Seg_5733" s="T113">PKZ_196X_SU0210.029 (029)</ta>
            <ta e="T121" id="Seg_5734" s="T118">PKZ_196X_SU0210.030 (030)</ta>
            <ta e="T124" id="Seg_5735" s="T121">PKZ_196X_SU0210.031 (031)</ta>
            <ta e="T131" id="Seg_5736" s="T124">PKZ_196X_SU0210.032 (032)</ta>
            <ta e="T133" id="Seg_5737" s="T131">PKZ_196X_SU0210.033 (033)</ta>
            <ta e="T143" id="Seg_5738" s="T133">PKZ_196X_SU0210.034 (034)</ta>
            <ta e="T148" id="Seg_5739" s="T143">PKZ_196X_SU0210.035 (035)</ta>
            <ta e="T150" id="Seg_5740" s="T148">PKZ_196X_SU0210.036 (036)</ta>
            <ta e="T153" id="Seg_5741" s="T150">PKZ_196X_SU0210.037 (037)</ta>
            <ta e="T157" id="Seg_5742" s="T153">PKZ_196X_SU0210.038 (038)</ta>
            <ta e="T159" id="Seg_5743" s="T157">PKZ_196X_SU0210.039 (039)</ta>
            <ta e="T166" id="Seg_5744" s="T159">PKZ_196X_SU0210.040 (040)</ta>
            <ta e="T173" id="Seg_5745" s="T166">PKZ_196X_SU0210.041 (041)</ta>
            <ta e="T179" id="Seg_5746" s="T173">PKZ_196X_SU0210.042 (042)</ta>
            <ta e="T184" id="Seg_5747" s="T179">PKZ_196X_SU0210.043 (043)</ta>
            <ta e="T190" id="Seg_5748" s="T184">PKZ_196X_SU0210.044 (044)</ta>
            <ta e="T193" id="Seg_5749" s="T190">PKZ_196X_SU0210.045 (045)</ta>
            <ta e="T196" id="Seg_5750" s="T193">PKZ_196X_SU0210.046 (046)</ta>
            <ta e="T198" id="Seg_5751" s="T196">PKZ_196X_SU0210.047 (047)</ta>
            <ta e="T205" id="Seg_5752" s="T198">PKZ_196X_SU0210.048 (048)</ta>
            <ta e="T208" id="Seg_5753" s="T205">PKZ_196X_SU0210.049 (049)</ta>
            <ta e="T214" id="Seg_5754" s="T208">PKZ_196X_SU0210.050 (050)</ta>
            <ta e="T219" id="Seg_5755" s="T214">PKZ_196X_SU0210.051 (051)</ta>
            <ta e="T223" id="Seg_5756" s="T219">PKZ_196X_SU0210.052 (052)</ta>
            <ta e="T227" id="Seg_5757" s="T223">PKZ_196X_SU0210.053 (053)</ta>
            <ta e="T229" id="Seg_5758" s="T227">PKZ_196X_SU0210.054 (054)</ta>
            <ta e="T231" id="Seg_5759" s="T229">PKZ_196X_SU0210.055 (055)</ta>
            <ta e="T233" id="Seg_5760" s="T231">PKZ_196X_SU0210.056 (056)</ta>
            <ta e="T236" id="Seg_5761" s="T233">PKZ_196X_SU0210.057 (057)</ta>
            <ta e="T240" id="Seg_5762" s="T236">PKZ_196X_SU0210.058 (058)</ta>
            <ta e="T246" id="Seg_5763" s="T240">PKZ_196X_SU0210.059 (059)</ta>
            <ta e="T254" id="Seg_5764" s="T246">PKZ_196X_SU0210.060 (060)</ta>
            <ta e="T260" id="Seg_5765" s="T254">PKZ_196X_SU0210.061 (061)</ta>
            <ta e="T266" id="Seg_5766" s="T260">PKZ_196X_SU0210.062 (062)</ta>
            <ta e="T269" id="Seg_5767" s="T266">PKZ_196X_SU0210.063 (063)</ta>
            <ta e="T277" id="Seg_5768" s="T269">PKZ_196X_SU0210.064 (064)</ta>
            <ta e="T280" id="Seg_5769" s="T277">PKZ_196X_SU0210.065 (065)</ta>
            <ta e="T286" id="Seg_5770" s="T280">PKZ_196X_SU0210.066 (066)</ta>
            <ta e="T288" id="Seg_5771" s="T286">PKZ_196X_SU0210.067 (067)</ta>
            <ta e="T290" id="Seg_5772" s="T288">PKZ_196X_SU0210.068 (068)</ta>
            <ta e="T293" id="Seg_5773" s="T290">PKZ_196X_SU0210.069 (069)</ta>
            <ta e="T300" id="Seg_5774" s="T293">PKZ_196X_SU0210.070 (070)</ta>
            <ta e="T302" id="Seg_5775" s="T300">PKZ_196X_SU0210.071 (071)</ta>
            <ta e="T305" id="Seg_5776" s="T302">PKZ_196X_SU0210.072 (072)</ta>
            <ta e="T311" id="Seg_5777" s="T305">PKZ_196X_SU0210.073 (073)</ta>
            <ta e="T318" id="Seg_5778" s="T311">PKZ_196X_SU0210.074 (074)</ta>
            <ta e="T323" id="Seg_5779" s="T318">PKZ_196X_SU0210.075 (075)</ta>
            <ta e="T329" id="Seg_5780" s="T323">PKZ_196X_SU0210.076 (076)</ta>
            <ta e="T335" id="Seg_5781" s="T329">PKZ_196X_SU0210.077 (077)</ta>
            <ta e="T343" id="Seg_5782" s="T335">PKZ_196X_SU0210.078 (078)</ta>
            <ta e="T346" id="Seg_5783" s="T343">PKZ_196X_SU0210.079 (079)</ta>
            <ta e="T357" id="Seg_5784" s="T346">PKZ_196X_SU0210.080 (080)</ta>
            <ta e="T359" id="Seg_5785" s="T357">PKZ_196X_SU0210.081 (081)</ta>
            <ta e="T363" id="Seg_5786" s="T359">PKZ_196X_SU0210.082 (082)</ta>
            <ta e="T366" id="Seg_5787" s="T363">PKZ_196X_SU0210.083 (083)</ta>
            <ta e="T370" id="Seg_5788" s="T366">PKZ_196X_SU0210.084 (084)</ta>
            <ta e="T373" id="Seg_5789" s="T370">PKZ_196X_SU0210.085 (085)</ta>
            <ta e="T379" id="Seg_5790" s="T373">PKZ_196X_SU0210.086 (086)</ta>
            <ta e="T382" id="Seg_5791" s="T379">PKZ_196X_SU0210.087 (087)</ta>
            <ta e="T388" id="Seg_5792" s="T382">PKZ_196X_SU0210.088 (088)</ta>
            <ta e="T391" id="Seg_5793" s="T388">PKZ_196X_SU0210.089 (089)</ta>
            <ta e="T397" id="Seg_5794" s="T391">PKZ_196X_SU0210.090 (090)</ta>
            <ta e="T403" id="Seg_5795" s="T397">PKZ_196X_SU0210.091 (091)</ta>
            <ta e="T407" id="Seg_5796" s="T403">PKZ_196X_SU0210.092 (092)</ta>
            <ta e="T411" id="Seg_5797" s="T407">PKZ_196X_SU0210.093 (093)</ta>
            <ta e="T415" id="Seg_5798" s="T411">PKZ_196X_SU0210.094 (094)</ta>
            <ta e="T418" id="Seg_5799" s="T415">PKZ_196X_SU0210.095 (095)</ta>
            <ta e="T423" id="Seg_5800" s="T418">PKZ_196X_SU0210.096 (096)</ta>
            <ta e="T426" id="Seg_5801" s="T423">PKZ_196X_SU0210.097 (097)</ta>
            <ta e="T427" id="Seg_5802" s="T426">PKZ_196X_SU0210.098 (098)</ta>
            <ta e="T433" id="Seg_5803" s="T427">PKZ_196X_SU0210.099 (099)</ta>
            <ta e="T438" id="Seg_5804" s="T433">PKZ_196X_SU0210.100 (100)</ta>
            <ta e="T446" id="Seg_5805" s="T438">PKZ_196X_SU0210.101 (101)</ta>
            <ta e="T453" id="Seg_5806" s="T446">PKZ_196X_SU0210.102 (102)</ta>
            <ta e="T460" id="Seg_5807" s="T453">PKZ_196X_SU0210.103 (103)</ta>
            <ta e="T463" id="Seg_5808" s="T460">PKZ_196X_SU0210.104 (104)</ta>
            <ta e="T471" id="Seg_5809" s="T463">PKZ_196X_SU0210.105 (105)</ta>
            <ta e="T473" id="Seg_5810" s="T471">PKZ_196X_SU0210.106 (106)</ta>
            <ta e="T479" id="Seg_5811" s="T473">PKZ_196X_SU0210.107 (107)</ta>
            <ta e="T488" id="Seg_5812" s="T479">PKZ_196X_SU0210.108 (108)</ta>
            <ta e="T491" id="Seg_5813" s="T488">PKZ_196X_SU0210.109 (109)</ta>
            <ta e="T495" id="Seg_5814" s="T491">PKZ_196X_SU0210.110 (110)</ta>
            <ta e="T502" id="Seg_5815" s="T495">PKZ_196X_SU0210.111 (111) </ta>
            <ta e="T503" id="Seg_5816" s="T502">PKZ_196X_SU0210.112 (113)</ta>
            <ta e="T509" id="Seg_5817" s="T503">PKZ_196X_SU0210.113 (114)</ta>
            <ta e="T516" id="Seg_5818" s="T509">PKZ_196X_SU0210.114 (115)</ta>
            <ta e="T520" id="Seg_5819" s="T516">PKZ_196X_SU0210.115 (116)</ta>
            <ta e="T527" id="Seg_5820" s="T520">PKZ_196X_SU0210.116 (117)</ta>
            <ta e="T529" id="Seg_5821" s="T527">PKZ_196X_SU0210.117 (118)</ta>
            <ta e="T538" id="Seg_5822" s="T529">PKZ_196X_SU0210.118 (119)</ta>
            <ta e="T539" id="Seg_5823" s="T538">PKZ_196X_SU0210.119 (120)</ta>
            <ta e="T541" id="Seg_5824" s="T539">PKZ_196X_SU0210.120 (121)</ta>
            <ta e="T546" id="Seg_5825" s="T541">PKZ_196X_SU0210.121 (122)</ta>
            <ta e="T548" id="Seg_5826" s="T546">PKZ_196X_SU0210.122 (123)</ta>
            <ta e="T551" id="Seg_5827" s="T548">PKZ_196X_SU0210.123 (124)</ta>
            <ta e="T554" id="Seg_5828" s="T551">PKZ_196X_SU0210.124 (125)</ta>
            <ta e="T559" id="Seg_5829" s="T554">PKZ_196X_SU0210.125 (126)</ta>
            <ta e="T562" id="Seg_5830" s="T559">PKZ_196X_SU0210.126 (127)</ta>
            <ta e="T565" id="Seg_5831" s="T562">PKZ_196X_SU0210.127 (128)</ta>
            <ta e="T570" id="Seg_5832" s="T565">PKZ_196X_SU0210.128 (129)</ta>
            <ta e="T575" id="Seg_5833" s="T570">PKZ_196X_SU0210.129 (130)</ta>
            <ta e="T578" id="Seg_5834" s="T575">PKZ_196X_SU0210.130 (131)</ta>
            <ta e="T582" id="Seg_5835" s="T578">PKZ_196X_SU0210.131 (132)</ta>
            <ta e="T585" id="Seg_5836" s="T582">PKZ_196X_SU0210.132 (133)</ta>
            <ta e="T589" id="Seg_5837" s="T585">PKZ_196X_SU0210.133 (134)</ta>
            <ta e="T592" id="Seg_5838" s="T589">PKZ_196X_SU0210.134 (135)</ta>
            <ta e="T595" id="Seg_5839" s="T592">PKZ_196X_SU0210.135 (136)</ta>
            <ta e="T601" id="Seg_5840" s="T595">PKZ_196X_SU0210.136 (137)</ta>
            <ta e="T610" id="Seg_5841" s="T601">PKZ_196X_SU0210.137 (138) </ta>
            <ta e="T618" id="Seg_5842" s="T610">PKZ_196X_SU0210.138 (140)</ta>
            <ta e="T622" id="Seg_5843" s="T618">PKZ_196X_SU0210.139 (141)</ta>
            <ta e="T627" id="Seg_5844" s="T622">PKZ_196X_SU0210.140 (142)</ta>
            <ta e="T630" id="Seg_5845" s="T627">PKZ_196X_SU0210.141 (143)</ta>
            <ta e="T636" id="Seg_5846" s="T630">PKZ_196X_SU0210.142 (144)</ta>
            <ta e="T641" id="Seg_5847" s="T636">PKZ_196X_SU0210.143 (145)</ta>
            <ta e="T645" id="Seg_5848" s="T641">PKZ_196X_SU0210.144 (146)</ta>
            <ta e="T649" id="Seg_5849" s="T645">PKZ_196X_SU0210.145 (147)</ta>
            <ta e="T653" id="Seg_5850" s="T649">PKZ_196X_SU0210.146 (148)</ta>
            <ta e="T657" id="Seg_5851" s="T653">PKZ_196X_SU0210.147 (149)</ta>
            <ta e="T660" id="Seg_5852" s="T657">PKZ_196X_SU0210.148 (150)</ta>
            <ta e="T667" id="Seg_5853" s="T660">PKZ_196X_SU0210.149 (151)</ta>
            <ta e="T676" id="Seg_5854" s="T667">PKZ_196X_SU0210.150 (152)</ta>
            <ta e="T681" id="Seg_5855" s="T676">PKZ_196X_SU0210.151 (153)</ta>
            <ta e="T687" id="Seg_5856" s="T681">PKZ_196X_SU0210.152 (154)</ta>
            <ta e="T698" id="Seg_5857" s="T687">PKZ_196X_SU0210.153 (155)</ta>
            <ta e="T703" id="Seg_5858" s="T698">PKZ_196X_SU0210.154 (156)</ta>
            <ta e="T713" id="Seg_5859" s="T703">PKZ_196X_SU0210.155 (157)</ta>
            <ta e="T716" id="Seg_5860" s="T713">PKZ_196X_SU0210.156 (158)</ta>
            <ta e="T718" id="Seg_5861" s="T716">PKZ_196X_SU0210.157 (159)</ta>
            <ta e="T721" id="Seg_5862" s="T718">PKZ_196X_SU0210.158 (160)</ta>
            <ta e="T725" id="Seg_5863" s="T721">PKZ_196X_SU0210.159 (161)</ta>
            <ta e="T731" id="Seg_5864" s="T725">PKZ_196X_SU0210.160 (162)</ta>
            <ta e="T735" id="Seg_5865" s="T731">PKZ_196X_SU0210.161 (163)</ta>
            <ta e="T736" id="Seg_5866" s="T735">PKZ_196X_SU0210.162 (164)</ta>
            <ta e="T741" id="Seg_5867" s="T736">PKZ_196X_SU0210.163 (165)</ta>
            <ta e="T746" id="Seg_5868" s="T741">PKZ_196X_SU0210.164 (166)</ta>
            <ta e="T751" id="Seg_5869" s="T746">PKZ_196X_SU0210.165 (167)</ta>
            <ta e="T754" id="Seg_5870" s="T751">PKZ_196X_SU0210.166 (168)</ta>
            <ta e="T762" id="Seg_5871" s="T754">PKZ_196X_SU0210.167 (169)</ta>
            <ta e="T777" id="Seg_5872" s="T762">PKZ_196X_SU0210.168 (170)</ta>
            <ta e="T780" id="Seg_5873" s="T777">PKZ_196X_SU0210.169 (171)</ta>
            <ta e="T788" id="Seg_5874" s="T780">PKZ_196X_SU0210.170 (172)</ta>
            <ta e="T792" id="Seg_5875" s="T788">PKZ_196X_SU0210.171 (173)</ta>
            <ta e="T797" id="Seg_5876" s="T792">PKZ_196X_SU0210.172 (174)</ta>
            <ta e="T804" id="Seg_5877" s="T797">PKZ_196X_SU0210.173 (175) </ta>
            <ta e="T806" id="Seg_5878" s="T804">PKZ_196X_SU0210.174 (177)</ta>
            <ta e="T812" id="Seg_5879" s="T806">PKZ_196X_SU0210.175 (178)</ta>
            <ta e="T819" id="Seg_5880" s="T812">PKZ_196X_SU0210.176 (179)</ta>
            <ta e="T823" id="Seg_5881" s="T819">PKZ_196X_SU0210.177 (180)</ta>
            <ta e="T828" id="Seg_5882" s="T823">PKZ_196X_SU0210.178 (181)</ta>
            <ta e="T830" id="Seg_5883" s="T828">PKZ_196X_SU0210.179 (182)</ta>
            <ta e="T832" id="Seg_5884" s="T830">PKZ_196X_SU0210.180 (183)</ta>
            <ta e="T836" id="Seg_5885" s="T832">PKZ_196X_SU0210.181 (184)</ta>
            <ta e="T840" id="Seg_5886" s="T836">PKZ_196X_SU0210.182 (185)</ta>
            <ta e="T844" id="Seg_5887" s="T840">PKZ_196X_SU0210.183 (186)</ta>
            <ta e="T848" id="Seg_5888" s="T844">PKZ_196X_SU0210.184 (187)</ta>
            <ta e="T852" id="Seg_5889" s="T848">PKZ_196X_SU0210.185 (188)</ta>
            <ta e="T854" id="Seg_5890" s="T852">PKZ_196X_SU0210.186 (189)</ta>
            <ta e="T855" id="Seg_5891" s="T854">PKZ_196X_SU0210.187 (190)</ta>
            <ta e="T862" id="Seg_5892" s="T855">PKZ_196X_SU0210.188 (191)</ta>
            <ta e="T867" id="Seg_5893" s="T862">PKZ_196X_SU0210.189 (192)</ta>
            <ta e="T870" id="Seg_5894" s="T867">PKZ_196X_SU0210.190 (193)</ta>
            <ta e="T876" id="Seg_5895" s="T870">PKZ_196X_SU0210.191 (194)</ta>
            <ta e="T883" id="Seg_5896" s="T876">PKZ_196X_SU0210.192 (195)</ta>
            <ta e="T888" id="Seg_5897" s="T883">PKZ_196X_SU0210.193 (196)</ta>
            <ta e="T892" id="Seg_5898" s="T888">PKZ_196X_SU0210.194 (197)</ta>
            <ta e="T894" id="Seg_5899" s="T892">PKZ_196X_SU0210.195 (198)</ta>
            <ta e="T901" id="Seg_5900" s="T894">PKZ_196X_SU0210.196 (199)</ta>
            <ta e="T906" id="Seg_5901" s="T901">PKZ_196X_SU0210.197 (200)</ta>
            <ta e="T913" id="Seg_5902" s="T906">PKZ_196X_SU0210.198 (201)</ta>
            <ta e="T923" id="Seg_5903" s="T913">PKZ_196X_SU0210.199 (202)</ta>
            <ta e="T927" id="Seg_5904" s="T923">PKZ_196X_SU0210.200 (203)</ta>
            <ta e="T930" id="Seg_5905" s="T927">PKZ_196X_SU0210.201 (204)</ta>
            <ta e="T933" id="Seg_5906" s="T930">PKZ_196X_SU0210.202 (205)</ta>
            <ta e="T938" id="Seg_5907" s="T933">PKZ_196X_SU0210.203 (206)</ta>
            <ta e="T941" id="Seg_5908" s="T938">PKZ_196X_SU0210.204 (207)</ta>
            <ta e="T947" id="Seg_5909" s="T941">PKZ_196X_SU0210.205 (208)</ta>
            <ta e="T951" id="Seg_5910" s="T947">PKZ_196X_SU0210.206 (209)</ta>
            <ta e="T957" id="Seg_5911" s="T951">PKZ_196X_SU0210.207 (210)</ta>
            <ta e="T965" id="Seg_5912" s="T957">PKZ_196X_SU0210.208 (211)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T2" id="Seg_5913" s="T0">(davaj) nʼezittə. </ta>
            <ta e="T9" id="Seg_5914" s="T2">(On-) Onʼiʔ üdʼüge penzi (ur-) üžünə üzəbi. </ta>
            <ta e="T13" id="Seg_5915" s="T9">(Dĭ) dĭgəttə suʔmiluʔpi bar. </ta>
            <ta e="T26" id="Seg_5916" s="T13">(Dĭ=) dĭ bĭdəbi, bĭdəbi, dĭgəttə dĭ üžüttə saʔməluʔpi, dĭgəttə dĭ (ej= ej=) kambi. </ta>
            <ta e="T38" id="Seg_5917" s="T26">Măn dĭʔnə ugandə (mĭ-) mĭliem, a (dĭn=) dĭ măna kaməndə ej šolia. </ta>
            <ta e="T42" id="Seg_5918" s="T38">((DMG)) Măn ugandə tăŋ măndərliam. </ta>
            <ta e="T44" id="Seg_5919" s="T42">((DMG)) Jakšə măndərdə. </ta>
            <ta e="T48" id="Seg_5920" s="T44">Dĭ kuzam kăde kăštəsʼtə? </ta>
            <ta e="T51" id="Seg_5921" s="T48">I kăde numəjzittə? </ta>
            <ta e="T53" id="Seg_5922" s="T51">Nʼuʔdə măndəraʔ! </ta>
            <ta e="T56" id="Seg_5923" s="T53">((DMG)) Nʼuʔdə măndəraʔ măjanə. </ta>
            <ta e="T59" id="Seg_5924" s="T56">Nada teʔmem sarzittə. </ta>
            <ta e="T62" id="Seg_5925" s="T59">Üžüm nada šerzittə. </ta>
            <ta e="T65" id="Seg_5926" s="T62">Pargam nada šerzittə. </ta>
            <ta e="T68" id="Seg_5927" s="T65">Dĭgəttə nʼiʔtə kanzittə. </ta>
            <ta e="T71" id="Seg_5928" s="T68">Măn (čumbə) kambiam. </ta>
            <ta e="T73" id="Seg_5929" s="T71">Čumgən amnobiam. </ta>
            <ta e="T78" id="Seg_5930" s="T73">Dĭgəttə (baroʔ) kalla dʼürbiem (čum-) čumgəndə. </ta>
            <ta e="T81" id="Seg_5931" s="T78">Măn üdʼüge ibiem. </ta>
            <ta e="T85" id="Seg_5932" s="T81">Ine ibi (nuʔ-) miʔ. </ta>
            <ta e="T90" id="Seg_5933" s="T85">(Šide=) Šide (kɨ- ke-) kö. </ta>
            <ta e="T92" id="Seg_5934" s="T90">Bar köreleʔpibeʔ. </ta>
            <ta e="T94" id="Seg_5935" s="T92">Dĭʔə kandəga. </ta>
            <ta e="T101" id="Seg_5936" s="T94">(Măn) kallam, dĭgəttə dĭ (kandə-) kaləj mănzʼə. </ta>
            <ta e="T103" id="Seg_5937" s="T101">Sʼabiam čumdə. </ta>
            <ta e="T105" id="Seg_5938" s="T103">Amnobiam (čumdən). </ta>
            <ta e="T109" id="Seg_5939" s="T105">Dĭgəttə (už-) üzəbiem čumdə. </ta>
            <ta e="T113" id="Seg_5940" s="T109">Tura bar (neniluʔpiʔi=) neniluʔpi. </ta>
            <ta e="T118" id="Seg_5941" s="T113">Il bar nuʔməleʔbəʔjə, bü detleʔbəʔjə. </ta>
            <ta e="T121" id="Seg_5942" s="T118">Šünə kămlaʔbəʔjə bar. </ta>
            <ta e="T124" id="Seg_5943" s="T121">Štobɨ ej nenibi. </ta>
            <ta e="T131" id="Seg_5944" s="T124">Onʼiʔ kuza onʼiʔ kuzanə bar ipek nendəbi. </ta>
            <ta e="T133" id="Seg_5945" s="T131">Bar nendəluʔpi. </ta>
            <ta e="T143" id="Seg_5946" s="T133">Xatʼel молотилка (nun-) nuldəsʼtə, (a) dĭ ibi da ipek nendəluʔpi. </ta>
            <ta e="T148" id="Seg_5947" s="T143">(Kudonzəbi bʼa-) dĭzi, dĭ kuroluʔpi. </ta>
            <ta e="T150" id="Seg_5948" s="T148">Noʔ jaʔpiam. </ta>
            <ta e="T153" id="Seg_5949" s="T150">Zarottə tažerbiam, embiem. </ta>
            <ta e="T157" id="Seg_5950" s="T153">A dĭ bar neniluʔpi. </ta>
            <ta e="T159" id="Seg_5951" s="T157">Maʔtə šobiam. </ta>
            <ta e="T166" id="Seg_5952" s="T159">Dĭn bar nubiam, dĭgəttə (kalladʼu-) maʔtə kalla dʼürbiem. </ta>
            <ta e="T173" id="Seg_5953" s="T166">Teinen măn ej togonoriam, a tănzi dʼăbaktəriam. </ta>
            <ta e="T179" id="Seg_5954" s="T173">Xotʼ ej sedem, a tararluʔpiem ugandə. </ta>
            <ta e="T184" id="Seg_5955" s="T179">((DMG)) Ugandə surno tăŋ kandəga, šonuga. </ta>
            <ta e="T190" id="Seg_5956" s="T184">(M-) A măn (pa-) (paʔi) amnolbiam. </ta>
            <ta e="T193" id="Seg_5957" s="T190">Beržə bar šonuga. </ta>
            <ta e="T196" id="Seg_5958" s="T193">Măn kujnek kuvas. </ta>
            <ta e="T198" id="Seg_5959" s="T196">Dĭn amnobiam. </ta>
            <ta e="T205" id="Seg_5960" s="T198">Măna ej nünörbi, a to bɨ nünörbi bar. </ta>
            <ta e="T208" id="Seg_5961" s="T205">Măn dʼijenə mĭmbiem. </ta>
            <ta e="T214" id="Seg_5962" s="T208">Nüjnə (nüj-) nüjbiem, a dĭgəttə dʼorbiam. </ta>
            <ta e="T219" id="Seg_5963" s="T214">Măn nʼim külaːmbi, măn ajirbiam. </ta>
            <ta e="T223" id="Seg_5964" s="T219">Nüke i büzʼe amnolaʔbəʔjə. </ta>
            <ta e="T227" id="Seg_5965" s="T223">A tăn (šojonə) amnaʔ. </ta>
            <ta e="T229" id="Seg_5966" s="T227">Gijen ibiel? </ta>
            <ta e="T231" id="Seg_5967" s="T229">Gibər kandəgal? </ta>
            <ta e="T233" id="Seg_5968" s="T231">Maʔtə kandəgam. </ta>
            <ta e="T236" id="Seg_5969" s="T233">(Aktʼa) Aktʼa naga. </ta>
            <ta e="T240" id="Seg_5970" s="T236">Aktʼažət amnozittə ej jakšə. </ta>
            <ta e="T246" id="Seg_5971" s="T240">(Aktʼa) Aktʼa ige dak, jakšə amnozittə. </ta>
            <ta e="T254" id="Seg_5972" s="T246">Aktʼanə ĭmbi-nʼibudʼ iləl, munəjʔ možna izittə, ipek izittə. </ta>
            <ta e="T260" id="Seg_5973" s="T254">Kola izittə, uja iləl aktʼanə üge. </ta>
            <ta e="T266" id="Seg_5974" s="T260">Aktʼa naga dak ĭmbidə ej iləl. </ta>
            <ta e="T269" id="Seg_5975" s="T266">Kola dʼabəsʼtə kandəgal. </ta>
            <ta e="T277" id="Seg_5976" s="T269">Kola ej (dʼa-) dʼaʔpial dak, (iʔ šoʔ maʔnəl). </ta>
            <ta e="T280" id="Seg_5977" s="T277">Kanaʔ tuganbə kăštəʔ! </ta>
            <ta e="T286" id="Seg_5978" s="T280">Ej kalaʔi dak, (boštə iʔ šoʔ). </ta>
            <ta e="T288" id="Seg_5979" s="T286">Kăštʼit jakšəŋ! </ta>
            <ta e="T290" id="Seg_5980" s="T288">Štobɨ šobiʔi. </ta>
            <ta e="T293" id="Seg_5981" s="T290">Ej jakšə kuza. </ta>
            <ta e="T300" id="Seg_5982" s="T293">Arda sʼimamzʼə ej moliam măndəsʼtə, putʼəga ugandə. </ta>
            <ta e="T302" id="Seg_5983" s="T300">Koldʼəŋ kanaʔ. </ta>
            <ta e="T305" id="Seg_5984" s="T302">Tăŋ iʔ mĭneʔ. </ta>
            <ta e="T311" id="Seg_5985" s="T305">Kuliom bar: dĭ bostə šonəga măna. </ta>
            <ta e="T318" id="Seg_5986" s="T311">Boskəndə bar (manʼiʔleʔbə), a măn ej kaliam. </ta>
            <ta e="T323" id="Seg_5987" s="T318">Dʼijegən всякий paʔi bar özerleʔbəʔjə. </ta>
            <ta e="T329" id="Seg_5988" s="T323">Kuvas svʼetogəʔi, bar ugandə tăŋ putʼəmniaʔi. </ta>
            <ta e="T335" id="Seg_5989" s="T329">Ugandə nünniom, jakšə putʼəmnia, ĭmbidə mĭnzerleʔbə. </ta>
            <ta e="T343" id="Seg_5990" s="T335">Kurizəʔi mĭngeʔi dʼügən bar dʼüm kădaʔlaʔbəʔjə (ujuzə-) ujuzaŋdə. </ta>
            <ta e="T346" id="Seg_5991" s="T343">((DMG)) (Человека) надо ждать. </ta>
            <ta e="T357" id="Seg_5992" s="T346">Dĭ kuzam nada (deʔsittə= dĭ= bĭ-) edəʔsittə, dĭ bar büžü šoləj. </ta>
            <ta e="T359" id="Seg_5993" s="T357">Ara detləj. </ta>
            <ta e="T363" id="Seg_5994" s="T359">((DMG)) Dĭzeŋ bar dĭm edəʔleʔbəʔjə. </ta>
            <ta e="T366" id="Seg_5995" s="T363">(Dĭzeŋ=) Dĭzeŋ edəʔleʔbəʔjə. </ta>
            <ta e="T370" id="Seg_5996" s="T366">Teinen miʔ jakšə (dʼăbaktərləbiaʔ). </ta>
            <ta e="T373" id="Seg_5997" s="T370">A ĭmbi dʼăbaktərlaʔpileʔ? </ta>
            <ta e="T379" id="Seg_5998" s="T373">Nan kudolbibaʔ, i bar šiʔ kudolbibaʔ. </ta>
            <ta e="T382" id="Seg_5999" s="T379">Ineʔi bar kandəgaʔi. </ta>
            <ta e="T388" id="Seg_6000" s="T382">Šide ine kandəga, onʼiʔ ine kandəga. </ta>
            <ta e="T391" id="Seg_6001" s="T388">Măn nʼim šonəga. </ta>
            <ta e="T397" id="Seg_6002" s="T391">A gijen ibi— kola ile mĭmbi. </ta>
            <ta e="T403" id="Seg_6003" s="T397">((DMG)) Ej tĭmniem, deʔpi li, ej deʔpi. </ta>
            <ta e="T407" id="Seg_6004" s="T403">Deʔpi (dak), pürzittə nada. </ta>
            <ta e="T411" id="Seg_6005" s="T407">(Dĭ) ne bar nanəʔzəbi. </ta>
            <ta e="T415" id="Seg_6006" s="T411">Büžü (büre) ešši detləj. </ta>
            <ta e="T418" id="Seg_6007" s="T415">Nada dĭm (nereluʔsittə). </ta>
            <ta e="T423" id="Seg_6008" s="T418">Kanzittə nada, ĭmbi-nʼibudʼ sadarla izittə. </ta>
            <ta e="T426" id="Seg_6009" s="T423">Kamən dʼala … </ta>
            <ta e="T427" id="Seg_6010" s="T426">((…)).</ta>
            <ta e="T433" id="Seg_6011" s="T427">Kamən dʼala šolaʔbə (i) măn uʔlaʔbəm. </ta>
            <ta e="T438" id="Seg_6012" s="T433">Kamən dʼala kandəga măn iʔbələm. </ta>
            <ta e="T446" id="Seg_6013" s="T438">((DMG)) Dĭ kö ugandə šišəge ibi, ugandə numo ibi. </ta>
            <ta e="T453" id="Seg_6014" s="T446">Nüdʼəʔi bar ugandə (no- no- nugo-) nugoʔi. </ta>
            <ta e="T460" id="Seg_6015" s="T453">A dĭ (pʼe) šobi dak, ugandə ejü. </ta>
            <ta e="T463" id="Seg_6016" s="T460">Dʼalaʔi bar numoʔi. </ta>
            <ta e="T471" id="Seg_6017" s="T463">Nada măna bü pʼeštə nuldəsʼtə, a to büzo šoləj. </ta>
            <ta e="T473" id="Seg_6018" s="T471">Bü naga. </ta>
            <ta e="T479" id="Seg_6019" s="T473">A dĭ šišəge bü ej bĭtlie. </ta>
            <ta e="T488" id="Seg_6020" s="T479">(Nu- nuldʼ-) Nuldeʔ bostə ine, dĭ padʼi ej nulia. </ta>
            <ta e="T491" id="Seg_6021" s="T488">Nada dĭm nuldəsʼtə. </ta>
            <ta e="T495" id="Seg_6022" s="T491">Nʼi koʔbdozi bar özerbiʔi. </ta>
            <ta e="T502" id="Seg_6023" s="T495">Dĭgəttə nʼi (mămbi): "(M-) Măna kalal tibinə?" </ta>
            <ta e="T503" id="Seg_6024" s="T502">"Kalam". </ta>
            <ta e="T509" id="Seg_6025" s="T503">"Nu, măn iam abam öʔlim, tănan". </ta>
            <ta e="T516" id="Seg_6026" s="T509">Dĭgəttə (iat) iam abam öʔlubi, dĭzeŋ šobiʔi. </ta>
            <ta e="T520" id="Seg_6027" s="T516">Koʔbdon iazi abazi dʼăbaktərluʔpiʔi. </ta>
            <ta e="T527" id="Seg_6028" s="T520">"Koʔbdol (mĭbiʔ-) mĭbileʔ (măn= nʼi-) măn nʼinə?" </ta>
            <ta e="T529" id="Seg_6029" s="T527">"(Nu-) Mĭbibeʔ". </ta>
            <ta e="T538" id="Seg_6030" s="T529">(Dĭb-) Dĭgəttə koʔbdom surarbiʔi:" Tăn kalal miʔ nʼinə tibinə?" </ta>
            <ta e="T539" id="Seg_6031" s="T538">"Kalam". </ta>
            <ta e="T541" id="Seg_6032" s="T539">Dĭgəttə (monoʔkobiʔi). </ta>
            <ta e="T546" id="Seg_6033" s="T541">Ara deʔpiʔi, ipek deʔpiʔi, uja. </ta>
            <ta e="T548" id="Seg_6034" s="T546">Iʔgö ibi. </ta>
            <ta e="T551" id="Seg_6035" s="T548">Dĭgəttə il šobiʔi. </ta>
            <ta e="T554" id="Seg_6036" s="T551">Ara bĭʔpiʔi bar. </ta>
            <ta e="T559" id="Seg_6037" s="T554">Dĭgəttə (nʼe-) несколько dʼala kambi. </ta>
            <ta e="T562" id="Seg_6038" s="T559">Dĭzeŋ svʼetogəʔjə šerbiʔi. </ta>
            <ta e="T565" id="Seg_6039" s="T562">I tʼegermaʔnə kambiʔi. </ta>
            <ta e="T570" id="Seg_6040" s="T565">Dĭn abəs (dĭzem) kudaj numan üzəbi. </ta>
            <ta e="T575" id="Seg_6041" s="T570">Dĭgəttə (vʼe- vʼenogə-) vʼenogəʔi šerbiʔi. </ta>
            <ta e="T578" id="Seg_6042" s="T575">((NOISE)) Dĭgəttə maːndə šobiʔi. </ta>
            <ta e="T582" id="Seg_6043" s="T578">Tože bar ara bĭʔpiʔi. </ta>
            <ta e="T585" id="Seg_6044" s="T582">Koʔbdon tugandə šobiʔi. </ta>
            <ta e="T589" id="Seg_6045" s="T585">I nʼin koʔbdo šobiʔi. </ta>
            <ta e="T592" id="Seg_6046" s="T589">Iʔgö il ibiʔi. </ta>
            <ta e="T595" id="Seg_6047" s="T592">Ara iʔgö ibi. </ta>
            <ta e="T601" id="Seg_6048" s="T595">(Stolgən) ugandə iʔgö ĭmbi (iʔb-) iʔbolaʔbə. </ta>
            <ta e="T610" id="Seg_6049" s="T601">Kamən koʔbdo monoʔkobiʔi, iat abat mămbiʔi: "Dĭn oldʼat naga. </ta>
            <ta e="T618" id="Seg_6050" s="T610">Nada bătʼinkăʔi izittə, nada palʼto izittə, plat izittə. </ta>
            <ta e="T622" id="Seg_6051" s="T618">((NOISE)) Oldʼat amga, kujnektə naga". </ta>
            <ta e="T627" id="Seg_6052" s="T622">Dĭgəttə (dĭʔ-) dĭzeŋ aktʼam ibiʔi. </ta>
            <ta e="T630" id="Seg_6053" s="T627">Šide bʼeʔ sumna. </ta>
            <ta e="T636" id="Seg_6054" s="T630">Dĭgəttə iat aban dĭʔnə mĭbiʔi tüžöj. </ta>
            <ta e="T641" id="Seg_6055" s="T636">Tugazaŋdən mĭbiʔi ular, mĭbiʔi ineʔi. </ta>
            <ta e="T645" id="Seg_6056" s="T641">Aktʼa mĭbiʔi ugandə, iʔgö. </ta>
            <ta e="T649" id="Seg_6057" s="T645">(I-) Il iʔgö ibiʔi. </ta>
            <ta e="T653" id="Seg_6058" s="T649">Dĭgəttə dĭzeŋ kondʼo amnobiʔi. </ta>
            <ta e="T657" id="Seg_6059" s="T653">Kagat (išo serbi da). </ta>
            <ta e="T660" id="Seg_6060" s="T657">Bazoʔ (dăre) monoʔkobi. </ta>
            <ta e="T667" id="Seg_6061" s="T660">Dĭgəttə dĭ kalla dʼürbi, dĭʔnə abat mĭbi bar. </ta>
            <ta e="T676" id="Seg_6062" s="T667">Bar ĭmbi mĭbi, ine mĭbi, ular mĭbi, tüžöjʔi mĭbi. </ta>
            <ta e="T681" id="Seg_6063" s="T676">Ipek mĭbi, (tu-) tura nuldəbi. </ta>
            <ta e="T687" id="Seg_6064" s="T681">Dĭzeŋ (kan- kanzə-) kambiʔi венчаться ineʔizi. </ta>
            <ta e="T698" id="Seg_6065" s="T687">Koʔbdo bostə kazak iat, a nʼi bostə kazak iat, kazak abat. </ta>
            <ta e="T703" id="Seg_6066" s="T698">Dibər kambiʔi, dĭgəttə šobiʔi maːʔndə. </ta>
            <ta e="T713" id="Seg_6067" s="T703">A dĭgəttə koʔbdon kazak abat (odʼa-) oldʼa deʔpi ящик bar. </ta>
            <ta e="T716" id="Seg_6068" s="T713">Păduškăʔi deʔpi bar. </ta>
            <ta e="T718" id="Seg_6069" s="T716">Pʼerină deʔpi. </ta>
            <ta e="T721" id="Seg_6070" s="T718">Dĭgəttə šobiʔi tʼegermaʔtə. </ta>
            <ta e="T725" id="Seg_6071" s="T721">Dĭzeŋ amnobiʔi (stozə-) stoldə. </ta>
            <ta e="T731" id="Seg_6072" s="T725">Dĭgəttə ((PAUSE)) nʼin kujnektə kuvas, toʔbdə. </ta>
            <ta e="T735" id="Seg_6073" s="T731">Koʔbdon oldʼat tože kuvas. </ta>
            <ta e="T736" id="Seg_6074" s="T735">Toʔbdə. </ta>
            <ta e="T741" id="Seg_6075" s="T736">Ulut bar kuvas, vʼenok šerbiʔi. </ta>
            <ta e="T746" id="Seg_6076" s="T741">Dĭgəttə dĭʔnə (o- o- )… </ta>
            <ta e="T751" id="Seg_6077" s="T746">Dĭgəttə dĭʔnə šide kosa kürbiʔi. </ta>
            <ta e="T754" id="Seg_6078" s="T751">Dĭgəttə sarbiʔi ulut. </ta>
            <ta e="T762" id="Seg_6079" s="T754">Kamən dĭ koʔbdo ibi, onʼiʔ (koza-) kosa ibi. </ta>
            <ta e="T777" id="Seg_6080" s="T762">Dĭgəttə (s- kazak iat=) dĭ koʔbdon kazak iat i nʼin kazak iat dĭʔnə šide kürbiʔi. </ta>
            <ta e="T780" id="Seg_6081" s="T777">I sarbiʔi ulut. </ta>
            <ta e="T788" id="Seg_6082" s="T780">Dĭzeŋdə nʼi koʔbdom kazak abat (ku-) kumbi turanə. </ta>
            <ta e="T792" id="Seg_6083" s="T788">Dĭn šindidə ej ibi. </ta>
            <ta e="T797" id="Seg_6084" s="T792">Iʔbəbi (ʔi) kunolzittə i kamroluʔpiʔi. </ta>
            <ta e="T804" id="Seg_6085" s="T797">(Tugazaŋ-) Tuganzaŋdə dĭzeŋdə mămbiʔi: "Jakšə (amnolaʔ=) amnogaʔ. </ta>
            <ta e="T806" id="Seg_6086" s="T804">Iʔ dʼabərogaʔ. </ta>
            <ta e="T812" id="Seg_6087" s="T806">(Iʔ kundonz- iʔ kudonzlaʔ-) Iʔ kudonzaʔ. </ta>
            <ta e="T819" id="Seg_6088" s="T812">Puskaj (esseŋgə-) esseŋdə iʔgö molaj, nʼizeŋ, koʔbsaŋ. </ta>
            <ta e="T823" id="Seg_6089" s="T819">Dĭgəttə dĭzeŋ abəs kăštəbiʔi. </ta>
            <ta e="T828" id="Seg_6090" s="T823">Dĭ ugandə iʔgö ara bĭʔpi. </ta>
            <ta e="T830" id="Seg_6091" s="T828">I saʔməluʔpi. </ta>
            <ta e="T832" id="Seg_6092" s="T830">Dĭm kunnaːlbiʔi. </ta>
            <ta e="T836" id="Seg_6093" s="T832">Dĭzeŋ ugandə ara bĭʔpiʔi. </ta>
            <ta e="T840" id="Seg_6094" s="T836">Kondʼo, muktuʔ dʼala bĭʔpiʔi. </ta>
            <ta e="T844" id="Seg_6095" s="T840">Dĭgəttə dĭzeŋ bar amnobiʔi. </ta>
            <ta e="T848" id="Seg_6096" s="T844">Dĭzeŋ bar esseŋdə ibiʔi. </ta>
            <ta e="T852" id="Seg_6097" s="T848">Nʼizeŋ ibiʔi, koʔbtaŋ ibiʔi. </ta>
            <ta e="T854" id="Seg_6098" s="T852">Koʔpsaŋ ((DMG)). </ta>
            <ta e="T855" id="Seg_6099" s="T854">((…)). </ta>
            <ta e="T862" id="Seg_6100" s="T855">((DMG)) (Măn nʼim) taldʼen kambi, ara bĭʔpi. </ta>
            <ta e="T867" id="Seg_6101" s="T862">Ugandə kondʼo ej šobi maʔnə. </ta>
            <ta e="T870" id="Seg_6102" s="T867">Dĭgəttə šobi, kunolluʔpi. </ta>
            <ta e="T876" id="Seg_6103" s="T870">I ertən uʔbdəbi i moluʔpi togonorzittə. </ta>
            <ta e="T883" id="Seg_6104" s="T876">(A măna prišl-) Măn šide tüžöj surdəbiam. </ta>
            <ta e="T888" id="Seg_6105" s="T883">Dĭn tüžöjdə i bostə tüžöjdə. </ta>
            <ta e="T892" id="Seg_6106" s="T888">Onʼiʔ ne nanəʔzəbi ibi. </ta>
            <ta e="T894" id="Seg_6107" s="T892">Dĭgəttə šidəʔpi. </ta>
            <ta e="T901" id="Seg_6108" s="T894">A dĭ dibər kambi da bĭʔpi ara. </ta>
            <ta e="T906" id="Seg_6109" s="T901">Măn taldʼen (ambam) (măndər-) măndobiam. </ta>
            <ta e="T913" id="Seg_6110" s="T906">A teinen nüdʼin ((PAUSE)) teinen nüdʼin … </ta>
            <ta e="T923" id="Seg_6111" s="T913">((DMG)) Dĭgəttə dĭm dʼoduni kubiam, ĭmbidə deʔpi, a măn kurollaʔpiam bar. </ta>
            <ta e="T927" id="Seg_6112" s="T923">(Tăneldə) ej šobiʔi dʼijegə. </ta>
            <ta e="T930" id="Seg_6113" s="T927">Kamən dĭzeŋ šoləʔjə? </ta>
            <ta e="T933" id="Seg_6114" s="T930">Teinen šoləʔjə nüdʼində. </ta>
            <ta e="T938" id="Seg_6115" s="T933">Amorzittə nada dĭzeŋdə dĭgəttə mĭzittə. </ta>
            <ta e="T941" id="Seg_6116" s="T938">Dĭzeŋ bar püjöliaʔi. </ta>
            <ta e="T947" id="Seg_6117" s="T941">((DMG)) (Măn) teinen tenöbiam šiʔ bar šobilaʔ. </ta>
            <ta e="T951" id="Seg_6118" s="T947">A šiʔ ej šobilaʔ. </ta>
            <ta e="T957" id="Seg_6119" s="T951">Măn teinen (kə-) teinen (munəjʔjə) mĭnzərbiem. </ta>
            <ta e="T965" id="Seg_6120" s="T957">Kălba (dʼagarbiam), munujʔ embiem, (gorom) embiem, šiʔnʼileʔ bădəsʼtə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T1" id="Seg_6121" s="T0">davaj</ta>
            <ta e="T2" id="Seg_6122" s="T1">nʼe-zittə</ta>
            <ta e="T4" id="Seg_6123" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_6124" s="T4">üdʼüge</ta>
            <ta e="T6" id="Seg_6125" s="T5">penzi</ta>
            <ta e="T8" id="Seg_6126" s="T7">üžü-nə</ta>
            <ta e="T9" id="Seg_6127" s="T8">üzə-bi</ta>
            <ta e="T10" id="Seg_6128" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_6129" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_6130" s="T11">suʔmi-luʔ-pi</ta>
            <ta e="T13" id="Seg_6131" s="T12">bar</ta>
            <ta e="T14" id="Seg_6132" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_6133" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_6134" s="T15">bĭdə-bi</ta>
            <ta e="T17" id="Seg_6135" s="T16">bĭdə-bi</ta>
            <ta e="T18" id="Seg_6136" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_6137" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_6138" s="T19">üžü-ttə</ta>
            <ta e="T21" id="Seg_6139" s="T20">saʔmə-luʔ-pi</ta>
            <ta e="T22" id="Seg_6140" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_6141" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_6142" s="T23">ej</ta>
            <ta e="T25" id="Seg_6143" s="T24">ej</ta>
            <ta e="T26" id="Seg_6144" s="T25">kam-bi</ta>
            <ta e="T27" id="Seg_6145" s="T26">măn</ta>
            <ta e="T28" id="Seg_6146" s="T27">dĭʔ-nə</ta>
            <ta e="T29" id="Seg_6147" s="T28">ugandə</ta>
            <ta e="T31" id="Seg_6148" s="T30">mĭ-lie-m</ta>
            <ta e="T32" id="Seg_6149" s="T31">a</ta>
            <ta e="T33" id="Seg_6150" s="T32">dĭn</ta>
            <ta e="T34" id="Seg_6151" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_6152" s="T34">măna</ta>
            <ta e="T36" id="Seg_6153" s="T35">kamən=də</ta>
            <ta e="T37" id="Seg_6154" s="T36">ej</ta>
            <ta e="T38" id="Seg_6155" s="T37">šo-lia</ta>
            <ta e="T39" id="Seg_6156" s="T38">măn</ta>
            <ta e="T40" id="Seg_6157" s="T39">ugandə</ta>
            <ta e="T41" id="Seg_6158" s="T40">tăŋ</ta>
            <ta e="T42" id="Seg_6159" s="T41">măndə-r-lia-m</ta>
            <ta e="T43" id="Seg_6160" s="T42">jakšə</ta>
            <ta e="T44" id="Seg_6161" s="T43">măndə-r-də</ta>
            <ta e="T45" id="Seg_6162" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_6163" s="T45">kuza-m</ta>
            <ta e="T47" id="Seg_6164" s="T46">kăde</ta>
            <ta e="T48" id="Seg_6165" s="T47">kăštə-sʼtə</ta>
            <ta e="T49" id="Seg_6166" s="T48">i</ta>
            <ta e="T50" id="Seg_6167" s="T49">kăde</ta>
            <ta e="T51" id="Seg_6168" s="T50">numəj-zittə</ta>
            <ta e="T52" id="Seg_6169" s="T51">nʼuʔdə</ta>
            <ta e="T53" id="Seg_6170" s="T52">măndə-r-a-ʔ</ta>
            <ta e="T54" id="Seg_6171" s="T53">nʼuʔdə</ta>
            <ta e="T55" id="Seg_6172" s="T54">măndə-r-a-ʔ</ta>
            <ta e="T56" id="Seg_6173" s="T55">măja-nə</ta>
            <ta e="T57" id="Seg_6174" s="T56">nada</ta>
            <ta e="T58" id="Seg_6175" s="T57">teʔme-m</ta>
            <ta e="T59" id="Seg_6176" s="T58">sar-zittə</ta>
            <ta e="T60" id="Seg_6177" s="T59">üžü-m</ta>
            <ta e="T61" id="Seg_6178" s="T60">nada</ta>
            <ta e="T62" id="Seg_6179" s="T61">šer-zittə</ta>
            <ta e="T63" id="Seg_6180" s="T62">parga-m</ta>
            <ta e="T64" id="Seg_6181" s="T63">nada</ta>
            <ta e="T65" id="Seg_6182" s="T64">šer-zittə</ta>
            <ta e="T66" id="Seg_6183" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_6184" s="T66">nʼiʔtə</ta>
            <ta e="T68" id="Seg_6185" s="T67">kan-zittə</ta>
            <ta e="T69" id="Seg_6186" s="T68">măn</ta>
            <ta e="T70" id="Seg_6187" s="T69">čum-bə</ta>
            <ta e="T71" id="Seg_6188" s="T70">kam-bia-m</ta>
            <ta e="T72" id="Seg_6189" s="T71">čum-gən</ta>
            <ta e="T73" id="Seg_6190" s="T72">amno-bia-m</ta>
            <ta e="T74" id="Seg_6191" s="T73">dĭgəttə</ta>
            <ta e="T75" id="Seg_6192" s="T74">baroʔ</ta>
            <ta e="T967" id="Seg_6193" s="T75">kal-la</ta>
            <ta e="T76" id="Seg_6194" s="T967">dʼür-bie-m</ta>
            <ta e="T78" id="Seg_6195" s="T77">čum-gəndə</ta>
            <ta e="T79" id="Seg_6196" s="T78">măn</ta>
            <ta e="T80" id="Seg_6197" s="T79">üdʼüge</ta>
            <ta e="T81" id="Seg_6198" s="T80">i-bie-m</ta>
            <ta e="T82" id="Seg_6199" s="T81">ine</ta>
            <ta e="T83" id="Seg_6200" s="T82">i-bi</ta>
            <ta e="T85" id="Seg_6201" s="T84">miʔ</ta>
            <ta e="T86" id="Seg_6202" s="T85">šide</ta>
            <ta e="T87" id="Seg_6203" s="T86">šide</ta>
            <ta e="T90" id="Seg_6204" s="T89">kö</ta>
            <ta e="T91" id="Seg_6205" s="T90">bar</ta>
            <ta e="T92" id="Seg_6206" s="T91">köre-leʔpi-beʔ</ta>
            <ta e="T93" id="Seg_6207" s="T92">dĭʔə</ta>
            <ta e="T94" id="Seg_6208" s="T93">kandə-ga</ta>
            <ta e="T95" id="Seg_6209" s="T94">măn</ta>
            <ta e="T96" id="Seg_6210" s="T95">kal-la-m</ta>
            <ta e="T97" id="Seg_6211" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_6212" s="T97">dĭ</ta>
            <ta e="T100" id="Seg_6213" s="T99">ka-lə-j</ta>
            <ta e="T101" id="Seg_6214" s="T100">măn-zʼə</ta>
            <ta e="T102" id="Seg_6215" s="T101">sʼa-bia-m</ta>
            <ta e="T103" id="Seg_6216" s="T102">čum-də</ta>
            <ta e="T104" id="Seg_6217" s="T103">amno-bia-m</ta>
            <ta e="T105" id="Seg_6218" s="T104">čum-dən</ta>
            <ta e="T106" id="Seg_6219" s="T105">dĭgəttə</ta>
            <ta e="T108" id="Seg_6220" s="T107">üzə-bie-m</ta>
            <ta e="T109" id="Seg_6221" s="T108">čum-də</ta>
            <ta e="T110" id="Seg_6222" s="T109">tura</ta>
            <ta e="T111" id="Seg_6223" s="T110">bar</ta>
            <ta e="T113" id="Seg_6224" s="T112">neni-luʔ-pi</ta>
            <ta e="T114" id="Seg_6225" s="T113">il</ta>
            <ta e="T115" id="Seg_6226" s="T114">bar</ta>
            <ta e="T116" id="Seg_6227" s="T115">nuʔmə-leʔbə-ʔjə</ta>
            <ta e="T117" id="Seg_6228" s="T116">bü</ta>
            <ta e="T118" id="Seg_6229" s="T117">det-leʔbə-ʔjə</ta>
            <ta e="T119" id="Seg_6230" s="T118">šü-nə</ta>
            <ta e="T120" id="Seg_6231" s="T119">kăm-laʔbə-ʔjə</ta>
            <ta e="T121" id="Seg_6232" s="T120">bar</ta>
            <ta e="T122" id="Seg_6233" s="T121">štobɨ</ta>
            <ta e="T123" id="Seg_6234" s="T122">ej</ta>
            <ta e="T124" id="Seg_6235" s="T123">neni-bi</ta>
            <ta e="T125" id="Seg_6236" s="T124">onʼiʔ</ta>
            <ta e="T126" id="Seg_6237" s="T125">kuza</ta>
            <ta e="T127" id="Seg_6238" s="T126">onʼiʔ</ta>
            <ta e="T128" id="Seg_6239" s="T127">kuza-nə</ta>
            <ta e="T129" id="Seg_6240" s="T128">bar</ta>
            <ta e="T130" id="Seg_6241" s="T129">ipek</ta>
            <ta e="T131" id="Seg_6242" s="T130">nen-də-bi</ta>
            <ta e="T132" id="Seg_6243" s="T131">bar</ta>
            <ta e="T133" id="Seg_6244" s="T132">nen-də-luʔ-pi</ta>
            <ta e="T134" id="Seg_6245" s="T133">xatʼel</ta>
            <ta e="T137" id="Seg_6246" s="T136">nuldə-sʼtə</ta>
            <ta e="T138" id="Seg_6247" s="T137">a</ta>
            <ta e="T139" id="Seg_6248" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_6249" s="T139">i-bi</ta>
            <ta e="T141" id="Seg_6250" s="T140">da</ta>
            <ta e="T142" id="Seg_6251" s="T141">ipek</ta>
            <ta e="T143" id="Seg_6252" s="T142">nen-də-luʔ-pi</ta>
            <ta e="T144" id="Seg_6253" s="T143">kudo-nzə-bi</ta>
            <ta e="T146" id="Seg_6254" s="T145">dĭ-zi</ta>
            <ta e="T147" id="Seg_6255" s="T146">dĭ</ta>
            <ta e="T148" id="Seg_6256" s="T147">kuro-luʔ-pi</ta>
            <ta e="T149" id="Seg_6257" s="T148">noʔ</ta>
            <ta e="T150" id="Seg_6258" s="T149">jaʔ-pia-m</ta>
            <ta e="T151" id="Seg_6259" s="T150">zarot-tə</ta>
            <ta e="T152" id="Seg_6260" s="T151">tažer-bia-m</ta>
            <ta e="T153" id="Seg_6261" s="T152">em-bie-m</ta>
            <ta e="T154" id="Seg_6262" s="T153">a</ta>
            <ta e="T155" id="Seg_6263" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_6264" s="T155">bar</ta>
            <ta e="T157" id="Seg_6265" s="T156">neni-luʔ-pi</ta>
            <ta e="T158" id="Seg_6266" s="T157">maʔ-tə</ta>
            <ta e="T159" id="Seg_6267" s="T158">šo-bia-m</ta>
            <ta e="T160" id="Seg_6268" s="T159">dĭn</ta>
            <ta e="T161" id="Seg_6269" s="T160">bar</ta>
            <ta e="T162" id="Seg_6270" s="T161">nu-bia-m</ta>
            <ta e="T163" id="Seg_6271" s="T162">dĭgəttə</ta>
            <ta e="T165" id="Seg_6272" s="T164">maʔ-tə</ta>
            <ta e="T968" id="Seg_6273" s="T165">kal-la</ta>
            <ta e="T166" id="Seg_6274" s="T968">dʼür-bie-m</ta>
            <ta e="T167" id="Seg_6275" s="T166">teinen</ta>
            <ta e="T168" id="Seg_6276" s="T167">măn</ta>
            <ta e="T169" id="Seg_6277" s="T168">ej</ta>
            <ta e="T170" id="Seg_6278" s="T169">togonor-ia-m</ta>
            <ta e="T171" id="Seg_6279" s="T170">a</ta>
            <ta e="T172" id="Seg_6280" s="T171">tăn-zi</ta>
            <ta e="T173" id="Seg_6281" s="T172">dʼăbaktər-ia-m</ta>
            <ta e="T174" id="Seg_6282" s="T173">xotʼ</ta>
            <ta e="T175" id="Seg_6283" s="T174">ej</ta>
            <ta e="T176" id="Seg_6284" s="T175">sedem</ta>
            <ta e="T177" id="Seg_6285" s="T176">a</ta>
            <ta e="T178" id="Seg_6286" s="T177">tarar-luʔ-pie-m</ta>
            <ta e="T179" id="Seg_6287" s="T178">ugandə</ta>
            <ta e="T180" id="Seg_6288" s="T179">ugandə</ta>
            <ta e="T181" id="Seg_6289" s="T180">surno</ta>
            <ta e="T182" id="Seg_6290" s="T181">tăŋ</ta>
            <ta e="T183" id="Seg_6291" s="T182">kandə-ga</ta>
            <ta e="T184" id="Seg_6292" s="T183">šonu-ga</ta>
            <ta e="T186" id="Seg_6293" s="T185">a</ta>
            <ta e="T187" id="Seg_6294" s="T186">măn</ta>
            <ta e="T189" id="Seg_6295" s="T188">pa-ʔi</ta>
            <ta e="T190" id="Seg_6296" s="T189">amnol-bia-m</ta>
            <ta e="T191" id="Seg_6297" s="T190">beržə</ta>
            <ta e="T192" id="Seg_6298" s="T191">bar</ta>
            <ta e="T193" id="Seg_6299" s="T192">šonu-ga</ta>
            <ta e="T194" id="Seg_6300" s="T193">măn</ta>
            <ta e="T195" id="Seg_6301" s="T194">kujnek</ta>
            <ta e="T196" id="Seg_6302" s="T195">kuvas</ta>
            <ta e="T197" id="Seg_6303" s="T196">dĭn</ta>
            <ta e="T198" id="Seg_6304" s="T197">amno-bia-m</ta>
            <ta e="T199" id="Seg_6305" s="T198">măna</ta>
            <ta e="T200" id="Seg_6306" s="T199">ej</ta>
            <ta e="T201" id="Seg_6307" s="T200">nünör-bi</ta>
            <ta e="T202" id="Seg_6308" s="T201">ato</ta>
            <ta e="T203" id="Seg_6309" s="T202">bɨ</ta>
            <ta e="T204" id="Seg_6310" s="T203">nünör-bi</ta>
            <ta e="T205" id="Seg_6311" s="T204">bar</ta>
            <ta e="T206" id="Seg_6312" s="T205">măn</ta>
            <ta e="T207" id="Seg_6313" s="T206">dʼije-nə</ta>
            <ta e="T208" id="Seg_6314" s="T207">mĭm-bie-m</ta>
            <ta e="T209" id="Seg_6315" s="T208">nüjnə</ta>
            <ta e="T211" id="Seg_6316" s="T210">nüj-bie-m</ta>
            <ta e="T212" id="Seg_6317" s="T211">a</ta>
            <ta e="T213" id="Seg_6318" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_6319" s="T213">dʼor-bia-m</ta>
            <ta e="T215" id="Seg_6320" s="T214">măn</ta>
            <ta e="T216" id="Seg_6321" s="T215">nʼi-m</ta>
            <ta e="T217" id="Seg_6322" s="T216">kü-laːm-bi</ta>
            <ta e="T218" id="Seg_6323" s="T217">măn</ta>
            <ta e="T219" id="Seg_6324" s="T218">ajir-bia-m</ta>
            <ta e="T220" id="Seg_6325" s="T219">nüke</ta>
            <ta e="T221" id="Seg_6326" s="T220">i</ta>
            <ta e="T222" id="Seg_6327" s="T221">büzʼe</ta>
            <ta e="T223" id="Seg_6328" s="T222">amno-laʔbə-ʔjə</ta>
            <ta e="T224" id="Seg_6329" s="T223">a</ta>
            <ta e="T225" id="Seg_6330" s="T224">tăn</ta>
            <ta e="T226" id="Seg_6331" s="T225">šojo-nə</ta>
            <ta e="T227" id="Seg_6332" s="T226">amna-ʔ</ta>
            <ta e="T228" id="Seg_6333" s="T227">gijen</ta>
            <ta e="T229" id="Seg_6334" s="T228">i-bie-l</ta>
            <ta e="T230" id="Seg_6335" s="T229">gibər</ta>
            <ta e="T231" id="Seg_6336" s="T230">kandə-ga-l</ta>
            <ta e="T232" id="Seg_6337" s="T231">maʔ-tə</ta>
            <ta e="T233" id="Seg_6338" s="T232">kandə-ga-m</ta>
            <ta e="T234" id="Seg_6339" s="T233">aktʼa</ta>
            <ta e="T235" id="Seg_6340" s="T234">aktʼa</ta>
            <ta e="T236" id="Seg_6341" s="T235">naga</ta>
            <ta e="T237" id="Seg_6342" s="T236">aktʼa-žət</ta>
            <ta e="T238" id="Seg_6343" s="T237">amno-zittə</ta>
            <ta e="T239" id="Seg_6344" s="T238">ej</ta>
            <ta e="T240" id="Seg_6345" s="T239">jakšə</ta>
            <ta e="T241" id="Seg_6346" s="T240">aktʼa</ta>
            <ta e="T242" id="Seg_6347" s="T241">aktʼa</ta>
            <ta e="T243" id="Seg_6348" s="T242">i-ge</ta>
            <ta e="T244" id="Seg_6349" s="T243">dak</ta>
            <ta e="T245" id="Seg_6350" s="T244">jakšə</ta>
            <ta e="T246" id="Seg_6351" s="T245">amno-zittə</ta>
            <ta e="T247" id="Seg_6352" s="T246">aktʼa-nə</ta>
            <ta e="T248" id="Seg_6353" s="T247">ĭmbi=nʼibudʼ</ta>
            <ta e="T249" id="Seg_6354" s="T248">i-lə-l</ta>
            <ta e="T250" id="Seg_6355" s="T249">munəj-ʔ</ta>
            <ta e="T251" id="Seg_6356" s="T250">možna</ta>
            <ta e="T252" id="Seg_6357" s="T251">i-zittə</ta>
            <ta e="T253" id="Seg_6358" s="T252">ipek</ta>
            <ta e="T254" id="Seg_6359" s="T253">i-zittə</ta>
            <ta e="T255" id="Seg_6360" s="T254">kola</ta>
            <ta e="T256" id="Seg_6361" s="T255">i-zittə</ta>
            <ta e="T257" id="Seg_6362" s="T256">uja</ta>
            <ta e="T258" id="Seg_6363" s="T257">i-lə-l</ta>
            <ta e="T259" id="Seg_6364" s="T258">aktʼa-nə</ta>
            <ta e="T260" id="Seg_6365" s="T259">üge</ta>
            <ta e="T261" id="Seg_6366" s="T260">aktʼa</ta>
            <ta e="T262" id="Seg_6367" s="T261">naga</ta>
            <ta e="T263" id="Seg_6368" s="T262">dak</ta>
            <ta e="T264" id="Seg_6369" s="T263">ĭmbi=də</ta>
            <ta e="T265" id="Seg_6370" s="T264">ej</ta>
            <ta e="T266" id="Seg_6371" s="T265">i-lə-l</ta>
            <ta e="T267" id="Seg_6372" s="T266">kola</ta>
            <ta e="T268" id="Seg_6373" s="T267">dʼabə-sʼtə</ta>
            <ta e="T269" id="Seg_6374" s="T268">kandə-ga-l</ta>
            <ta e="T270" id="Seg_6375" s="T269">kola</ta>
            <ta e="T271" id="Seg_6376" s="T270">ej</ta>
            <ta e="T273" id="Seg_6377" s="T272">dʼaʔ-pia-l</ta>
            <ta e="T274" id="Seg_6378" s="T273">dak</ta>
            <ta e="T275" id="Seg_6379" s="T274">i-ʔ</ta>
            <ta e="T276" id="Seg_6380" s="T275">šo-ʔ</ta>
            <ta e="T277" id="Seg_6381" s="T276">maʔ-nə-l</ta>
            <ta e="T278" id="Seg_6382" s="T277">kan-a-ʔ</ta>
            <ta e="T279" id="Seg_6383" s="T278">tugan-bə</ta>
            <ta e="T280" id="Seg_6384" s="T279">kăštə-ʔ</ta>
            <ta e="T281" id="Seg_6385" s="T280">ej</ta>
            <ta e="T282" id="Seg_6386" s="T281">ka-la-ʔi</ta>
            <ta e="T283" id="Seg_6387" s="T282">dak</ta>
            <ta e="T284" id="Seg_6388" s="T283">boš-tə</ta>
            <ta e="T285" id="Seg_6389" s="T284">i-ʔ</ta>
            <ta e="T286" id="Seg_6390" s="T285">šo-ʔ</ta>
            <ta e="T287" id="Seg_6391" s="T286">kăštʼi-t</ta>
            <ta e="T288" id="Seg_6392" s="T287">jakšə-ŋ</ta>
            <ta e="T289" id="Seg_6393" s="T288">štobɨ</ta>
            <ta e="T290" id="Seg_6394" s="T289">šo-bi-ʔi</ta>
            <ta e="T291" id="Seg_6395" s="T290">ej</ta>
            <ta e="T292" id="Seg_6396" s="T291">jakšə</ta>
            <ta e="T293" id="Seg_6397" s="T292">kuza</ta>
            <ta e="T294" id="Seg_6398" s="T293">arda</ta>
            <ta e="T295" id="Seg_6399" s="T294">sʼima-m-zʼə</ta>
            <ta e="T296" id="Seg_6400" s="T295">ej</ta>
            <ta e="T297" id="Seg_6401" s="T296">mo-lia-m</ta>
            <ta e="T298" id="Seg_6402" s="T297">măndə-sʼtə</ta>
            <ta e="T299" id="Seg_6403" s="T298">putʼəga</ta>
            <ta e="T300" id="Seg_6404" s="T299">ugandə</ta>
            <ta e="T301" id="Seg_6405" s="T300">koldʼəŋ</ta>
            <ta e="T302" id="Seg_6406" s="T301">kan-a-ʔ</ta>
            <ta e="T303" id="Seg_6407" s="T302">tăŋ</ta>
            <ta e="T304" id="Seg_6408" s="T303">i-ʔ</ta>
            <ta e="T305" id="Seg_6409" s="T304">mĭn-e-ʔ</ta>
            <ta e="T306" id="Seg_6410" s="T305">ku-lio-m</ta>
            <ta e="T307" id="Seg_6411" s="T306">bar</ta>
            <ta e="T308" id="Seg_6412" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_6413" s="T308">bos-tə</ta>
            <ta e="T310" id="Seg_6414" s="T309">šonə-ga</ta>
            <ta e="T311" id="Seg_6415" s="T310">măna</ta>
            <ta e="T312" id="Seg_6416" s="T311">bos-kəndə</ta>
            <ta e="T313" id="Seg_6417" s="T312">bar</ta>
            <ta e="T314" id="Seg_6418" s="T313">manʼiʔ-leʔbə</ta>
            <ta e="T315" id="Seg_6419" s="T314">a</ta>
            <ta e="T316" id="Seg_6420" s="T315">măn</ta>
            <ta e="T317" id="Seg_6421" s="T316">ej</ta>
            <ta e="T318" id="Seg_6422" s="T317">ka-lia-m</ta>
            <ta e="T319" id="Seg_6423" s="T318">dʼije-gən</ta>
            <ta e="T321" id="Seg_6424" s="T320">pa-ʔi</ta>
            <ta e="T322" id="Seg_6425" s="T321">bar</ta>
            <ta e="T323" id="Seg_6426" s="T322">özer-leʔbə-ʔjə</ta>
            <ta e="T324" id="Seg_6427" s="T323">kuvas</ta>
            <ta e="T325" id="Seg_6428" s="T324">svʼetog-əʔi</ta>
            <ta e="T326" id="Seg_6429" s="T325">bar</ta>
            <ta e="T327" id="Seg_6430" s="T326">ugandə</ta>
            <ta e="T328" id="Seg_6431" s="T327">tăŋ</ta>
            <ta e="T329" id="Seg_6432" s="T328">putʼəm-nia-ʔi</ta>
            <ta e="T330" id="Seg_6433" s="T329">ugandə</ta>
            <ta e="T331" id="Seg_6434" s="T330">nün-nio-m</ta>
            <ta e="T332" id="Seg_6435" s="T331">jakšə</ta>
            <ta e="T333" id="Seg_6436" s="T332">putʼəm-nia</ta>
            <ta e="T334" id="Seg_6437" s="T333">ĭmbi=də</ta>
            <ta e="T335" id="Seg_6438" s="T334">mĭnzer-leʔbə</ta>
            <ta e="T336" id="Seg_6439" s="T335">kurizə-ʔi</ta>
            <ta e="T337" id="Seg_6440" s="T336">mĭn-ge-ʔi</ta>
            <ta e="T338" id="Seg_6441" s="T337">dʼü-gən</ta>
            <ta e="T339" id="Seg_6442" s="T338">bar</ta>
            <ta e="T340" id="Seg_6443" s="T339">dʼü-m</ta>
            <ta e="T341" id="Seg_6444" s="T340">kădaʔ-laʔbə-ʔjə</ta>
            <ta e="T343" id="Seg_6445" s="T342">uju-zaŋ-də</ta>
            <ta e="T347" id="Seg_6446" s="T346">dĭ</ta>
            <ta e="T348" id="Seg_6447" s="T347">kuza-m</ta>
            <ta e="T349" id="Seg_6448" s="T348">nada</ta>
            <ta e="T351" id="Seg_6449" s="T350">dĭ</ta>
            <ta e="T353" id="Seg_6450" s="T352">edəʔ-sittə</ta>
            <ta e="T354" id="Seg_6451" s="T353">dĭ</ta>
            <ta e="T355" id="Seg_6452" s="T354">bar</ta>
            <ta e="T356" id="Seg_6453" s="T355">büžü</ta>
            <ta e="T357" id="Seg_6454" s="T356">šo-lə-j</ta>
            <ta e="T358" id="Seg_6455" s="T357">ara</ta>
            <ta e="T359" id="Seg_6456" s="T358">det-lə-j</ta>
            <ta e="T360" id="Seg_6457" s="T359">dĭ-zeŋ</ta>
            <ta e="T361" id="Seg_6458" s="T360">bar</ta>
            <ta e="T362" id="Seg_6459" s="T361">dĭ-m</ta>
            <ta e="T363" id="Seg_6460" s="T362">edəʔ-leʔbə-ʔjə</ta>
            <ta e="T364" id="Seg_6461" s="T363">dĭ-zeŋ</ta>
            <ta e="T365" id="Seg_6462" s="T364">dĭ-zeŋ</ta>
            <ta e="T366" id="Seg_6463" s="T365">edəʔ-leʔbə-ʔjə</ta>
            <ta e="T367" id="Seg_6464" s="T366">teinen</ta>
            <ta e="T368" id="Seg_6465" s="T367">miʔ</ta>
            <ta e="T369" id="Seg_6466" s="T368">jakšə</ta>
            <ta e="T370" id="Seg_6467" s="T369">dʼăbaktər-ləbiaʔ</ta>
            <ta e="T371" id="Seg_6468" s="T370">a</ta>
            <ta e="T372" id="Seg_6469" s="T371">ĭmbi</ta>
            <ta e="T373" id="Seg_6470" s="T372">dʼăbaktər-laʔpi-leʔ</ta>
            <ta e="T374" id="Seg_6471" s="T373">nan</ta>
            <ta e="T375" id="Seg_6472" s="T374">kudol-bi-baʔ</ta>
            <ta e="T376" id="Seg_6473" s="T375">i</ta>
            <ta e="T377" id="Seg_6474" s="T376">bar</ta>
            <ta e="T378" id="Seg_6475" s="T377">šiʔ</ta>
            <ta e="T379" id="Seg_6476" s="T378">kudol-bi-baʔ</ta>
            <ta e="T380" id="Seg_6477" s="T379">ine-ʔi</ta>
            <ta e="T381" id="Seg_6478" s="T380">bar</ta>
            <ta e="T382" id="Seg_6479" s="T381">kan-də-ga-ʔi</ta>
            <ta e="T383" id="Seg_6480" s="T382">šide</ta>
            <ta e="T384" id="Seg_6481" s="T383">ine</ta>
            <ta e="T385" id="Seg_6482" s="T384">kandə-ga</ta>
            <ta e="T386" id="Seg_6483" s="T385">onʼiʔ</ta>
            <ta e="T387" id="Seg_6484" s="T386">ine</ta>
            <ta e="T388" id="Seg_6485" s="T387">kandə-ga</ta>
            <ta e="T389" id="Seg_6486" s="T388">măn</ta>
            <ta e="T390" id="Seg_6487" s="T389">nʼi-m</ta>
            <ta e="T391" id="Seg_6488" s="T390">šonə-ga</ta>
            <ta e="T392" id="Seg_6489" s="T391">a</ta>
            <ta e="T393" id="Seg_6490" s="T392">gijen</ta>
            <ta e="T394" id="Seg_6491" s="T393">i-bi</ta>
            <ta e="T395" id="Seg_6492" s="T394">kola</ta>
            <ta e="T396" id="Seg_6493" s="T395">i-le</ta>
            <ta e="T397" id="Seg_6494" s="T396">mĭm-bi</ta>
            <ta e="T398" id="Seg_6495" s="T397">ej</ta>
            <ta e="T399" id="Seg_6496" s="T398">tĭm-nie-m</ta>
            <ta e="T400" id="Seg_6497" s="T399">deʔ-pi</ta>
            <ta e="T401" id="Seg_6498" s="T400">li</ta>
            <ta e="T402" id="Seg_6499" s="T401">ej</ta>
            <ta e="T403" id="Seg_6500" s="T402">deʔ-pi</ta>
            <ta e="T404" id="Seg_6501" s="T403">deʔ-pi</ta>
            <ta e="T405" id="Seg_6502" s="T404">dak</ta>
            <ta e="T406" id="Seg_6503" s="T405">pür-zittə</ta>
            <ta e="T407" id="Seg_6504" s="T406">nada</ta>
            <ta e="T408" id="Seg_6505" s="T407">dĭ</ta>
            <ta e="T409" id="Seg_6506" s="T408">ne</ta>
            <ta e="T410" id="Seg_6507" s="T409">bar</ta>
            <ta e="T411" id="Seg_6508" s="T410">nanəʔzəbi</ta>
            <ta e="T412" id="Seg_6509" s="T411">büžü</ta>
            <ta e="T413" id="Seg_6510" s="T412">büre</ta>
            <ta e="T414" id="Seg_6511" s="T413">ešši</ta>
            <ta e="T415" id="Seg_6512" s="T414">det-lə-j</ta>
            <ta e="T416" id="Seg_6513" s="T415">nada</ta>
            <ta e="T417" id="Seg_6514" s="T416">dĭ-m</ta>
            <ta e="T418" id="Seg_6515" s="T417">nere-luʔ-sittə</ta>
            <ta e="T419" id="Seg_6516" s="T418">kan-zittə</ta>
            <ta e="T420" id="Seg_6517" s="T419">nada</ta>
            <ta e="T421" id="Seg_6518" s="T420">ĭmbi=nʼibudʼ</ta>
            <ta e="T422" id="Seg_6519" s="T421">sadar-la</ta>
            <ta e="T423" id="Seg_6520" s="T422">i-zittə</ta>
            <ta e="T424" id="Seg_6521" s="T423">kamən</ta>
            <ta e="T426" id="Seg_6522" s="T424">dʼala</ta>
            <ta e="T428" id="Seg_6523" s="T427">kamən</ta>
            <ta e="T429" id="Seg_6524" s="T428">dʼala</ta>
            <ta e="T430" id="Seg_6525" s="T429">šo-laʔbə</ta>
            <ta e="T431" id="Seg_6526" s="T430">i</ta>
            <ta e="T432" id="Seg_6527" s="T431">măn</ta>
            <ta e="T433" id="Seg_6528" s="T432">uʔ-laʔbə-m</ta>
            <ta e="T434" id="Seg_6529" s="T433">kamən</ta>
            <ta e="T435" id="Seg_6530" s="T434">dʼala</ta>
            <ta e="T436" id="Seg_6531" s="T435">kandə-ga</ta>
            <ta e="T437" id="Seg_6532" s="T436">măn</ta>
            <ta e="T438" id="Seg_6533" s="T437">iʔbə-lə-m</ta>
            <ta e="T439" id="Seg_6534" s="T438">dĭ</ta>
            <ta e="T440" id="Seg_6535" s="T439">kö</ta>
            <ta e="T441" id="Seg_6536" s="T440">ugandə</ta>
            <ta e="T442" id="Seg_6537" s="T441">šišəge</ta>
            <ta e="T443" id="Seg_6538" s="T442">i-bi</ta>
            <ta e="T444" id="Seg_6539" s="T443">ugandə</ta>
            <ta e="T445" id="Seg_6540" s="T444">numo</ta>
            <ta e="T446" id="Seg_6541" s="T445">i-bi</ta>
            <ta e="T447" id="Seg_6542" s="T446">nüdʼə-ʔi</ta>
            <ta e="T448" id="Seg_6543" s="T447">bar</ta>
            <ta e="T449" id="Seg_6544" s="T448">ugandə</ta>
            <ta e="T453" id="Seg_6545" s="T452">nugo-ʔi</ta>
            <ta e="T454" id="Seg_6546" s="T453">a</ta>
            <ta e="T455" id="Seg_6547" s="T454">dĭ</ta>
            <ta e="T456" id="Seg_6548" s="T455">pʼe</ta>
            <ta e="T457" id="Seg_6549" s="T456">šo-bi</ta>
            <ta e="T458" id="Seg_6550" s="T457">dak</ta>
            <ta e="T459" id="Seg_6551" s="T458">ugandə</ta>
            <ta e="T460" id="Seg_6552" s="T459">ejü</ta>
            <ta e="T461" id="Seg_6553" s="T460">dʼala-ʔi</ta>
            <ta e="T462" id="Seg_6554" s="T461">bar</ta>
            <ta e="T463" id="Seg_6555" s="T462">numo-ʔi</ta>
            <ta e="T464" id="Seg_6556" s="T463">nada</ta>
            <ta e="T465" id="Seg_6557" s="T464">măna</ta>
            <ta e="T466" id="Seg_6558" s="T465">bü</ta>
            <ta e="T467" id="Seg_6559" s="T466">pʼeš-tə</ta>
            <ta e="T468" id="Seg_6560" s="T467">nuldə-sʼtə</ta>
            <ta e="T469" id="Seg_6561" s="T468">ato</ta>
            <ta e="T470" id="Seg_6562" s="T469">büzo</ta>
            <ta e="T471" id="Seg_6563" s="T470">šo-lə-j</ta>
            <ta e="T472" id="Seg_6564" s="T471">bü</ta>
            <ta e="T473" id="Seg_6565" s="T472">naga</ta>
            <ta e="T474" id="Seg_6566" s="T473">a</ta>
            <ta e="T475" id="Seg_6567" s="T474">dĭ</ta>
            <ta e="T476" id="Seg_6568" s="T475">šišəge</ta>
            <ta e="T477" id="Seg_6569" s="T476">bü</ta>
            <ta e="T478" id="Seg_6570" s="T477">ej</ta>
            <ta e="T479" id="Seg_6571" s="T478">bĭt-lie</ta>
            <ta e="T482" id="Seg_6572" s="T481">nuld-eʔ</ta>
            <ta e="T483" id="Seg_6573" s="T482">bos-tə</ta>
            <ta e="T484" id="Seg_6574" s="T483">ine</ta>
            <ta e="T485" id="Seg_6575" s="T484">dĭ</ta>
            <ta e="T486" id="Seg_6576" s="T485">padʼi</ta>
            <ta e="T487" id="Seg_6577" s="T486">ej</ta>
            <ta e="T488" id="Seg_6578" s="T487">nu-lia</ta>
            <ta e="T489" id="Seg_6579" s="T488">nada</ta>
            <ta e="T490" id="Seg_6580" s="T489">dĭ-m</ta>
            <ta e="T491" id="Seg_6581" s="T490">nuldə-sʼtə</ta>
            <ta e="T492" id="Seg_6582" s="T491">nʼi</ta>
            <ta e="T493" id="Seg_6583" s="T492">koʔbdo-zi</ta>
            <ta e="T494" id="Seg_6584" s="T493">bar</ta>
            <ta e="T495" id="Seg_6585" s="T494">özer-bi-ʔi</ta>
            <ta e="T496" id="Seg_6586" s="T495">dĭgəttə</ta>
            <ta e="T497" id="Seg_6587" s="T496">nʼi</ta>
            <ta e="T498" id="Seg_6588" s="T497">măm-bi</ta>
            <ta e="T500" id="Seg_6589" s="T499">măna</ta>
            <ta e="T501" id="Seg_6590" s="T500">ka-la-l</ta>
            <ta e="T502" id="Seg_6591" s="T501">tibi-nə</ta>
            <ta e="T503" id="Seg_6592" s="T502">ka-la-m</ta>
            <ta e="T504" id="Seg_6593" s="T503">nu</ta>
            <ta e="T505" id="Seg_6594" s="T504">măn</ta>
            <ta e="T506" id="Seg_6595" s="T505">ia-m</ta>
            <ta e="T507" id="Seg_6596" s="T506">aba-m</ta>
            <ta e="T508" id="Seg_6597" s="T507">öʔ-li-m</ta>
            <ta e="T509" id="Seg_6598" s="T508">tănan</ta>
            <ta e="T510" id="Seg_6599" s="T509">dĭgəttə</ta>
            <ta e="T511" id="Seg_6600" s="T510">ia-t</ta>
            <ta e="T512" id="Seg_6601" s="T511">ia-m</ta>
            <ta e="T513" id="Seg_6602" s="T512">aba-m</ta>
            <ta e="T514" id="Seg_6603" s="T513">öʔlu-bi</ta>
            <ta e="T515" id="Seg_6604" s="T514">dĭ-zeŋ</ta>
            <ta e="T516" id="Seg_6605" s="T515">šo-bi-ʔi</ta>
            <ta e="T517" id="Seg_6606" s="T516">koʔbdo-n</ta>
            <ta e="T518" id="Seg_6607" s="T517">ia-zi</ta>
            <ta e="T519" id="Seg_6608" s="T518">aba-zi</ta>
            <ta e="T520" id="Seg_6609" s="T519">dʼăbaktər-luʔ-pi-ʔi</ta>
            <ta e="T521" id="Seg_6610" s="T520">koʔbdo-l</ta>
            <ta e="T523" id="Seg_6611" s="T522">mĭ-bi-leʔ</ta>
            <ta e="T524" id="Seg_6612" s="T523">măn</ta>
            <ta e="T526" id="Seg_6613" s="T525">măn</ta>
            <ta e="T527" id="Seg_6614" s="T526">nʼi-nə</ta>
            <ta e="T529" id="Seg_6615" s="T528">mĭ-bi-beʔ</ta>
            <ta e="T531" id="Seg_6616" s="T530">dĭgəttə</ta>
            <ta e="T532" id="Seg_6617" s="T531">koʔbdo-m</ta>
            <ta e="T533" id="Seg_6618" s="T532">surar-bi-ʔi</ta>
            <ta e="T534" id="Seg_6619" s="T533">tăn</ta>
            <ta e="T535" id="Seg_6620" s="T534">ka-la-l</ta>
            <ta e="T536" id="Seg_6621" s="T535">miʔ</ta>
            <ta e="T537" id="Seg_6622" s="T536">nʼi-nə</ta>
            <ta e="T538" id="Seg_6623" s="T537">tibi-nə</ta>
            <ta e="T539" id="Seg_6624" s="T538">ka-la-m</ta>
            <ta e="T540" id="Seg_6625" s="T539">dĭgəttə</ta>
            <ta e="T541" id="Seg_6626" s="T540">monoʔko-bi-ʔi</ta>
            <ta e="T542" id="Seg_6627" s="T541">ara</ta>
            <ta e="T543" id="Seg_6628" s="T542">deʔ-pi-ʔi</ta>
            <ta e="T544" id="Seg_6629" s="T543">ipek</ta>
            <ta e="T545" id="Seg_6630" s="T544">deʔ-pi-ʔi</ta>
            <ta e="T546" id="Seg_6631" s="T545">uja</ta>
            <ta e="T547" id="Seg_6632" s="T546">iʔgö</ta>
            <ta e="T548" id="Seg_6633" s="T547">i-bi</ta>
            <ta e="T549" id="Seg_6634" s="T548">dĭgəttə</ta>
            <ta e="T550" id="Seg_6635" s="T549">il</ta>
            <ta e="T551" id="Seg_6636" s="T550">šo-bi-ʔi</ta>
            <ta e="T552" id="Seg_6637" s="T551">ara</ta>
            <ta e="T553" id="Seg_6638" s="T552">bĭʔ-pi-ʔi</ta>
            <ta e="T554" id="Seg_6639" s="T553">bar</ta>
            <ta e="T555" id="Seg_6640" s="T554">dĭgəttə</ta>
            <ta e="T558" id="Seg_6641" s="T557">dʼala</ta>
            <ta e="T559" id="Seg_6642" s="T558">kam-bi</ta>
            <ta e="T560" id="Seg_6643" s="T559">dĭ-zeŋ</ta>
            <ta e="T561" id="Seg_6644" s="T560">svʼetog-əʔjə</ta>
            <ta e="T562" id="Seg_6645" s="T561">šer-bi-ʔi</ta>
            <ta e="T563" id="Seg_6646" s="T562">i</ta>
            <ta e="T564" id="Seg_6647" s="T563">tʼegermaʔ-nə</ta>
            <ta e="T565" id="Seg_6648" s="T564">kam-bi-ʔi</ta>
            <ta e="T566" id="Seg_6649" s="T565">dĭn</ta>
            <ta e="T567" id="Seg_6650" s="T566">abəs</ta>
            <ta e="T568" id="Seg_6651" s="T567">dĭ-zem</ta>
            <ta e="T569" id="Seg_6652" s="T568">kudaj</ta>
            <ta e="T570" id="Seg_6653" s="T569">numan üzə-bi</ta>
            <ta e="T571" id="Seg_6654" s="T570">dĭgəttə</ta>
            <ta e="T574" id="Seg_6655" s="T573">vʼenog-əʔi</ta>
            <ta e="T575" id="Seg_6656" s="T574">šer-bi-ʔi</ta>
            <ta e="T576" id="Seg_6657" s="T575">dĭgəttə</ta>
            <ta e="T577" id="Seg_6658" s="T576">ma-ndə</ta>
            <ta e="T578" id="Seg_6659" s="T577">šo-bi-ʔi</ta>
            <ta e="T579" id="Seg_6660" s="T578">tože</ta>
            <ta e="T580" id="Seg_6661" s="T579">bar</ta>
            <ta e="T581" id="Seg_6662" s="T580">ara</ta>
            <ta e="T582" id="Seg_6663" s="T581">bĭʔ-pi-ʔi</ta>
            <ta e="T583" id="Seg_6664" s="T582">koʔbdo-n</ta>
            <ta e="T584" id="Seg_6665" s="T583">tugan-də</ta>
            <ta e="T585" id="Seg_6666" s="T584">šo-bi-ʔi</ta>
            <ta e="T586" id="Seg_6667" s="T585">i</ta>
            <ta e="T587" id="Seg_6668" s="T586">nʼi-n</ta>
            <ta e="T588" id="Seg_6669" s="T587">koʔbdo</ta>
            <ta e="T589" id="Seg_6670" s="T588">šo-bi-ʔi</ta>
            <ta e="T590" id="Seg_6671" s="T589">iʔgö</ta>
            <ta e="T591" id="Seg_6672" s="T590">il</ta>
            <ta e="T592" id="Seg_6673" s="T591">i-bi-ʔi</ta>
            <ta e="T593" id="Seg_6674" s="T592">ara</ta>
            <ta e="T594" id="Seg_6675" s="T593">iʔgö</ta>
            <ta e="T595" id="Seg_6676" s="T594">i-bi</ta>
            <ta e="T596" id="Seg_6677" s="T595">stol-gən</ta>
            <ta e="T597" id="Seg_6678" s="T596">ugandə</ta>
            <ta e="T598" id="Seg_6679" s="T597">iʔgö</ta>
            <ta e="T599" id="Seg_6680" s="T598">ĭmbi</ta>
            <ta e="T601" id="Seg_6681" s="T600">iʔbo-laʔbə</ta>
            <ta e="T602" id="Seg_6682" s="T601">kamən</ta>
            <ta e="T603" id="Seg_6683" s="T602">koʔbdo</ta>
            <ta e="T604" id="Seg_6684" s="T603">monoʔko-bi-ʔi</ta>
            <ta e="T605" id="Seg_6685" s="T604">ia-t</ta>
            <ta e="T606" id="Seg_6686" s="T605">aba-t</ta>
            <ta e="T607" id="Seg_6687" s="T606">măm-bi-ʔi</ta>
            <ta e="T608" id="Seg_6688" s="T607">dĭ-n</ta>
            <ta e="T609" id="Seg_6689" s="T608">oldʼa-t</ta>
            <ta e="T610" id="Seg_6690" s="T609">naga</ta>
            <ta e="T611" id="Seg_6691" s="T610">nada</ta>
            <ta e="T612" id="Seg_6692" s="T611">bătʼinkă-ʔi</ta>
            <ta e="T613" id="Seg_6693" s="T612">i-zittə</ta>
            <ta e="T614" id="Seg_6694" s="T613">nada</ta>
            <ta e="T615" id="Seg_6695" s="T614">palʼto</ta>
            <ta e="T616" id="Seg_6696" s="T615">i-zittə</ta>
            <ta e="T617" id="Seg_6697" s="T616">plat</ta>
            <ta e="T618" id="Seg_6698" s="T617">i-zittə</ta>
            <ta e="T619" id="Seg_6699" s="T618">oldʼa-t</ta>
            <ta e="T620" id="Seg_6700" s="T619">amga</ta>
            <ta e="T621" id="Seg_6701" s="T620">kujnek-tə</ta>
            <ta e="T622" id="Seg_6702" s="T621">naga</ta>
            <ta e="T623" id="Seg_6703" s="T622">dĭgəttə</ta>
            <ta e="T625" id="Seg_6704" s="T624">dĭ-zeŋ</ta>
            <ta e="T626" id="Seg_6705" s="T625">aktʼa-m</ta>
            <ta e="T627" id="Seg_6706" s="T626">i-bi-ʔi</ta>
            <ta e="T628" id="Seg_6707" s="T627">šide</ta>
            <ta e="T629" id="Seg_6708" s="T628">bʼeʔ</ta>
            <ta e="T630" id="Seg_6709" s="T629">sumna</ta>
            <ta e="T631" id="Seg_6710" s="T630">dĭgəttə</ta>
            <ta e="T632" id="Seg_6711" s="T631">ia-t</ta>
            <ta e="T633" id="Seg_6712" s="T632">aba-n</ta>
            <ta e="T634" id="Seg_6713" s="T633">dĭʔ-nə</ta>
            <ta e="T635" id="Seg_6714" s="T634">mĭ-bi-ʔi</ta>
            <ta e="T636" id="Seg_6715" s="T635">tüžöj</ta>
            <ta e="T637" id="Seg_6716" s="T636">tuga-zaŋ-dən</ta>
            <ta e="T638" id="Seg_6717" s="T637">mĭ-bi-ʔi</ta>
            <ta e="T639" id="Seg_6718" s="T638">ular</ta>
            <ta e="T640" id="Seg_6719" s="T639">mĭ-bi-ʔi</ta>
            <ta e="T641" id="Seg_6720" s="T640">ine-ʔi</ta>
            <ta e="T642" id="Seg_6721" s="T641">aktʼa</ta>
            <ta e="T643" id="Seg_6722" s="T642">mĭ-bi-ʔi</ta>
            <ta e="T644" id="Seg_6723" s="T643">ugandə</ta>
            <ta e="T645" id="Seg_6724" s="T644">iʔgö</ta>
            <ta e="T647" id="Seg_6725" s="T646">il</ta>
            <ta e="T648" id="Seg_6726" s="T647">iʔgö</ta>
            <ta e="T649" id="Seg_6727" s="T648">i-bi-ʔi</ta>
            <ta e="T650" id="Seg_6728" s="T649">dĭgəttə</ta>
            <ta e="T651" id="Seg_6729" s="T650">dĭ-zeŋ</ta>
            <ta e="T652" id="Seg_6730" s="T651">kondʼo</ta>
            <ta e="T653" id="Seg_6731" s="T652">amno-bi-ʔi</ta>
            <ta e="T654" id="Seg_6732" s="T653">kaga-t</ta>
            <ta e="T657" id="Seg_6733" s="T656">da</ta>
            <ta e="T658" id="Seg_6734" s="T657">bazoʔ</ta>
            <ta e="T659" id="Seg_6735" s="T658">dăre</ta>
            <ta e="T660" id="Seg_6736" s="T659">monoʔko-bi</ta>
            <ta e="T661" id="Seg_6737" s="T660">dĭgəttə</ta>
            <ta e="T662" id="Seg_6738" s="T661">dĭ</ta>
            <ta e="T969" id="Seg_6739" s="T662">kal-la</ta>
            <ta e="T663" id="Seg_6740" s="T969">dʼür-bi</ta>
            <ta e="T664" id="Seg_6741" s="T663">dĭʔ-nə</ta>
            <ta e="T665" id="Seg_6742" s="T664">aba-t</ta>
            <ta e="T666" id="Seg_6743" s="T665">mĭ-bi</ta>
            <ta e="T667" id="Seg_6744" s="T666">bar</ta>
            <ta e="T668" id="Seg_6745" s="T667">bar</ta>
            <ta e="T669" id="Seg_6746" s="T668">ĭmbi</ta>
            <ta e="T670" id="Seg_6747" s="T669">mĭ-bi</ta>
            <ta e="T671" id="Seg_6748" s="T670">ine</ta>
            <ta e="T672" id="Seg_6749" s="T671">mĭ-bi</ta>
            <ta e="T673" id="Seg_6750" s="T672">ular</ta>
            <ta e="T674" id="Seg_6751" s="T673">mĭ-bi</ta>
            <ta e="T675" id="Seg_6752" s="T674">tüžöj-ʔi</ta>
            <ta e="T676" id="Seg_6753" s="T675">mĭ-bi</ta>
            <ta e="T677" id="Seg_6754" s="T676">ipek</ta>
            <ta e="T678" id="Seg_6755" s="T677">mĭ-bi</ta>
            <ta e="T680" id="Seg_6756" s="T679">tura</ta>
            <ta e="T681" id="Seg_6757" s="T680">nuldə-bi</ta>
            <ta e="T682" id="Seg_6758" s="T681">dĭ-zeŋ</ta>
            <ta e="T685" id="Seg_6759" s="T684">kam-bi-ʔi</ta>
            <ta e="T687" id="Seg_6760" s="T686">ine-ʔi-zi</ta>
            <ta e="T688" id="Seg_6761" s="T687">koʔbdo</ta>
            <ta e="T689" id="Seg_6762" s="T688">bos-tə</ta>
            <ta e="T690" id="Seg_6763" s="T689">kazak</ta>
            <ta e="T691" id="Seg_6764" s="T690">ia-t</ta>
            <ta e="T692" id="Seg_6765" s="T691">a</ta>
            <ta e="T693" id="Seg_6766" s="T692">nʼi</ta>
            <ta e="T694" id="Seg_6767" s="T693">bos-tə</ta>
            <ta e="T695" id="Seg_6768" s="T694">kazak</ta>
            <ta e="T696" id="Seg_6769" s="T695">ia-t</ta>
            <ta e="T697" id="Seg_6770" s="T696">kazak</ta>
            <ta e="T698" id="Seg_6771" s="T697">aba-t</ta>
            <ta e="T699" id="Seg_6772" s="T698">dibər</ta>
            <ta e="T700" id="Seg_6773" s="T699">kam-bi-ʔi</ta>
            <ta e="T701" id="Seg_6774" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_6775" s="T701">šo-bi-ʔi</ta>
            <ta e="T703" id="Seg_6776" s="T702">maːʔ-ndə</ta>
            <ta e="T704" id="Seg_6777" s="T703">a</ta>
            <ta e="T705" id="Seg_6778" s="T704">dĭgəttə</ta>
            <ta e="T706" id="Seg_6779" s="T705">koʔbdo-n</ta>
            <ta e="T707" id="Seg_6780" s="T706">kazak</ta>
            <ta e="T708" id="Seg_6781" s="T707">aba-t</ta>
            <ta e="T710" id="Seg_6782" s="T709">oldʼa</ta>
            <ta e="T711" id="Seg_6783" s="T710">deʔ-pi</ta>
            <ta e="T713" id="Seg_6784" s="T712">bar</ta>
            <ta e="T714" id="Seg_6785" s="T713">păduškă-ʔi</ta>
            <ta e="T715" id="Seg_6786" s="T714">deʔ-pi</ta>
            <ta e="T716" id="Seg_6787" s="T715">bar</ta>
            <ta e="T717" id="Seg_6788" s="T716">pʼerină</ta>
            <ta e="T718" id="Seg_6789" s="T717">deʔ-pi</ta>
            <ta e="T719" id="Seg_6790" s="T718">dĭgəttə</ta>
            <ta e="T720" id="Seg_6791" s="T719">šo-bi-ʔi</ta>
            <ta e="T721" id="Seg_6792" s="T720">tʼegermaʔ-tə</ta>
            <ta e="T722" id="Seg_6793" s="T721">dĭ-zeŋ</ta>
            <ta e="T723" id="Seg_6794" s="T722">amno-bi-ʔi</ta>
            <ta e="T725" id="Seg_6795" s="T724">stol-də</ta>
            <ta e="T727" id="Seg_6796" s="T725">dĭgəttə</ta>
            <ta e="T728" id="Seg_6797" s="T727">nʼi-n</ta>
            <ta e="T729" id="Seg_6798" s="T728">kujnek-tə</ta>
            <ta e="T730" id="Seg_6799" s="T729">kuvas</ta>
            <ta e="T731" id="Seg_6800" s="T730">toʔbdə</ta>
            <ta e="T732" id="Seg_6801" s="T731">koʔbdo-n</ta>
            <ta e="T733" id="Seg_6802" s="T732">oldʼa-t</ta>
            <ta e="T734" id="Seg_6803" s="T733">tože</ta>
            <ta e="T735" id="Seg_6804" s="T734">kuvas</ta>
            <ta e="T736" id="Seg_6805" s="T735">toʔbdə</ta>
            <ta e="T737" id="Seg_6806" s="T736">ulu-t</ta>
            <ta e="T738" id="Seg_6807" s="T737">bar</ta>
            <ta e="T739" id="Seg_6808" s="T738">kuvas</ta>
            <ta e="T740" id="Seg_6809" s="T739">vʼenok</ta>
            <ta e="T741" id="Seg_6810" s="T740">šer-bi-ʔi</ta>
            <ta e="T742" id="Seg_6811" s="T741">dĭgəttə</ta>
            <ta e="T743" id="Seg_6812" s="T742">dĭʔ-nə</ta>
            <ta e="T747" id="Seg_6813" s="T746">dĭgəttə</ta>
            <ta e="T748" id="Seg_6814" s="T747">dĭʔ-nə</ta>
            <ta e="T749" id="Seg_6815" s="T748">šide</ta>
            <ta e="T750" id="Seg_6816" s="T749">kosa</ta>
            <ta e="T751" id="Seg_6817" s="T750">kür-bi-ʔi</ta>
            <ta e="T752" id="Seg_6818" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_6819" s="T752">sar-bi-ʔi</ta>
            <ta e="T754" id="Seg_6820" s="T753">ulu-t</ta>
            <ta e="T755" id="Seg_6821" s="T754">kamən</ta>
            <ta e="T756" id="Seg_6822" s="T755">dĭ</ta>
            <ta e="T757" id="Seg_6823" s="T756">koʔbdo</ta>
            <ta e="T758" id="Seg_6824" s="T757">i-bi</ta>
            <ta e="T759" id="Seg_6825" s="T758">onʼiʔ</ta>
            <ta e="T761" id="Seg_6826" s="T760">kosa</ta>
            <ta e="T762" id="Seg_6827" s="T761">i-bi</ta>
            <ta e="T763" id="Seg_6828" s="T762">dĭgəttə</ta>
            <ta e="T765" id="Seg_6829" s="T764">kazak</ta>
            <ta e="T766" id="Seg_6830" s="T765">ia-t</ta>
            <ta e="T767" id="Seg_6831" s="T766">dĭ</ta>
            <ta e="T768" id="Seg_6832" s="T767">koʔbdo-n</ta>
            <ta e="T769" id="Seg_6833" s="T768">kazak</ta>
            <ta e="T770" id="Seg_6834" s="T769">ia-t</ta>
            <ta e="T771" id="Seg_6835" s="T770">i</ta>
            <ta e="T772" id="Seg_6836" s="T771">nʼi-n</ta>
            <ta e="T773" id="Seg_6837" s="T772">kazak</ta>
            <ta e="T774" id="Seg_6838" s="T773">ia-t</ta>
            <ta e="T775" id="Seg_6839" s="T774">dĭʔ-nə</ta>
            <ta e="T776" id="Seg_6840" s="T775">šide</ta>
            <ta e="T777" id="Seg_6841" s="T776">kür-bi-ʔi</ta>
            <ta e="T778" id="Seg_6842" s="T777">i</ta>
            <ta e="T779" id="Seg_6843" s="T778">sar-bi-ʔi</ta>
            <ta e="T780" id="Seg_6844" s="T779">ulu-t</ta>
            <ta e="T781" id="Seg_6845" s="T780">dĭ-zeŋ-də</ta>
            <ta e="T782" id="Seg_6846" s="T781">nʼi</ta>
            <ta e="T783" id="Seg_6847" s="T782">koʔbdo-m</ta>
            <ta e="T784" id="Seg_6848" s="T783">kazak</ta>
            <ta e="T785" id="Seg_6849" s="T784">aba-t</ta>
            <ta e="T787" id="Seg_6850" s="T786">kum-bi</ta>
            <ta e="T788" id="Seg_6851" s="T787">tura-nə</ta>
            <ta e="T789" id="Seg_6852" s="T788">dĭn</ta>
            <ta e="T790" id="Seg_6853" s="T789">šindi=də</ta>
            <ta e="T791" id="Seg_6854" s="T790">ej</ta>
            <ta e="T792" id="Seg_6855" s="T791">i-bi</ta>
            <ta e="T793" id="Seg_6856" s="T792">iʔbə-bi</ta>
            <ta e="T795" id="Seg_6857" s="T794">kunol-zittə</ta>
            <ta e="T796" id="Seg_6858" s="T795">i</ta>
            <ta e="T797" id="Seg_6859" s="T796">kamro-luʔ-pi-ʔi</ta>
            <ta e="T799" id="Seg_6860" s="T798">tugan-zaŋ-də</ta>
            <ta e="T800" id="Seg_6861" s="T799">dĭ-zeŋ-də</ta>
            <ta e="T801" id="Seg_6862" s="T800">măm-bi-ʔi</ta>
            <ta e="T802" id="Seg_6863" s="T801">jakšə</ta>
            <ta e="T803" id="Seg_6864" s="T802">amno-laʔ</ta>
            <ta e="T804" id="Seg_6865" s="T803">amno-gaʔ</ta>
            <ta e="T805" id="Seg_6866" s="T804">i-ʔ</ta>
            <ta e="T806" id="Seg_6867" s="T805">dʼabəro-gaʔ</ta>
            <ta e="T807" id="Seg_6868" s="T806">i-ʔ</ta>
            <ta e="T809" id="Seg_6869" s="T808">i-ʔ</ta>
            <ta e="T811" id="Seg_6870" s="T810">i-ʔ</ta>
            <ta e="T812" id="Seg_6871" s="T811">kudo-nza-ʔ</ta>
            <ta e="T813" id="Seg_6872" s="T812">puskaj</ta>
            <ta e="T815" id="Seg_6873" s="T814">es-seŋ-də</ta>
            <ta e="T816" id="Seg_6874" s="T815">iʔgö</ta>
            <ta e="T817" id="Seg_6875" s="T816">mo-la-j</ta>
            <ta e="T818" id="Seg_6876" s="T817">nʼi-zeŋ</ta>
            <ta e="T819" id="Seg_6877" s="T818">koʔb-saŋ</ta>
            <ta e="T820" id="Seg_6878" s="T819">dĭgəttə</ta>
            <ta e="T821" id="Seg_6879" s="T820">dĭ-zeŋ</ta>
            <ta e="T822" id="Seg_6880" s="T821">abəs</ta>
            <ta e="T823" id="Seg_6881" s="T822">kăštə-bi-ʔi</ta>
            <ta e="T824" id="Seg_6882" s="T823">dĭ</ta>
            <ta e="T825" id="Seg_6883" s="T824">ugandə</ta>
            <ta e="T826" id="Seg_6884" s="T825">iʔgö</ta>
            <ta e="T827" id="Seg_6885" s="T826">ara</ta>
            <ta e="T828" id="Seg_6886" s="T827">bĭʔ-pi</ta>
            <ta e="T829" id="Seg_6887" s="T828">i</ta>
            <ta e="T830" id="Seg_6888" s="T829">saʔmə-luʔ-pi</ta>
            <ta e="T831" id="Seg_6889" s="T830">dĭ-m</ta>
            <ta e="T832" id="Seg_6890" s="T831">kun-naːl-bi-ʔi</ta>
            <ta e="T833" id="Seg_6891" s="T832">dĭ-zeŋ</ta>
            <ta e="T834" id="Seg_6892" s="T833">ugandə</ta>
            <ta e="T835" id="Seg_6893" s="T834">ara</ta>
            <ta e="T836" id="Seg_6894" s="T835">bĭʔ-pi-ʔi</ta>
            <ta e="T837" id="Seg_6895" s="T836">kondʼo</ta>
            <ta e="T838" id="Seg_6896" s="T837">muktuʔ</ta>
            <ta e="T839" id="Seg_6897" s="T838">dʼala</ta>
            <ta e="T840" id="Seg_6898" s="T839">bĭʔ-pi-ʔi</ta>
            <ta e="T841" id="Seg_6899" s="T840">dĭgəttə</ta>
            <ta e="T842" id="Seg_6900" s="T841">dĭ-zeŋ</ta>
            <ta e="T843" id="Seg_6901" s="T842">bar</ta>
            <ta e="T844" id="Seg_6902" s="T843">amno-bi-ʔi</ta>
            <ta e="T845" id="Seg_6903" s="T844">dĭ-zeŋ</ta>
            <ta e="T846" id="Seg_6904" s="T845">bar</ta>
            <ta e="T847" id="Seg_6905" s="T846">es-seŋ-də</ta>
            <ta e="T848" id="Seg_6906" s="T847">i-bi-ʔi</ta>
            <ta e="T849" id="Seg_6907" s="T848">nʼi-zeŋ</ta>
            <ta e="T850" id="Seg_6908" s="T849">i-bi-ʔi</ta>
            <ta e="T852" id="Seg_6909" s="T851">i-bi-ʔi</ta>
            <ta e="T853" id="Seg_6910" s="T852">koʔp-saŋ</ta>
            <ta e="T857" id="Seg_6911" s="T855">măn</ta>
            <ta e="T858" id="Seg_6912" s="T857">nʼi-m</ta>
            <ta e="T859" id="Seg_6913" s="T858">taldʼen</ta>
            <ta e="T860" id="Seg_6914" s="T859">kam-bi</ta>
            <ta e="T861" id="Seg_6915" s="T860">ara</ta>
            <ta e="T862" id="Seg_6916" s="T861">bĭʔ-pi</ta>
            <ta e="T863" id="Seg_6917" s="T862">ugandə</ta>
            <ta e="T864" id="Seg_6918" s="T863">kondʼo</ta>
            <ta e="T865" id="Seg_6919" s="T864">ej</ta>
            <ta e="T866" id="Seg_6920" s="T865">šo-bi</ta>
            <ta e="T867" id="Seg_6921" s="T866">maʔ-nə</ta>
            <ta e="T868" id="Seg_6922" s="T867">dĭgəttə</ta>
            <ta e="T869" id="Seg_6923" s="T868">šo-bi</ta>
            <ta e="T870" id="Seg_6924" s="T869">kunol-luʔ-pi</ta>
            <ta e="T871" id="Seg_6925" s="T870">i</ta>
            <ta e="T872" id="Seg_6926" s="T871">ertə-n</ta>
            <ta e="T873" id="Seg_6927" s="T872">uʔbdə-bi</ta>
            <ta e="T874" id="Seg_6928" s="T873">i</ta>
            <ta e="T875" id="Seg_6929" s="T874">mo-luʔ-pi</ta>
            <ta e="T876" id="Seg_6930" s="T875">togonor-zittə</ta>
            <ta e="T877" id="Seg_6931" s="T876">a</ta>
            <ta e="T878" id="Seg_6932" s="T877">măna</ta>
            <ta e="T880" id="Seg_6933" s="T879">măn</ta>
            <ta e="T881" id="Seg_6934" s="T880">šide</ta>
            <ta e="T882" id="Seg_6935" s="T881">tüžöj</ta>
            <ta e="T883" id="Seg_6936" s="T882">surdə-bia-m</ta>
            <ta e="T884" id="Seg_6937" s="T883">dĭ-n</ta>
            <ta e="T885" id="Seg_6938" s="T884">tüžöj-də</ta>
            <ta e="T886" id="Seg_6939" s="T885">i</ta>
            <ta e="T887" id="Seg_6940" s="T886">bos-tə</ta>
            <ta e="T888" id="Seg_6941" s="T887">tüžöj-də</ta>
            <ta e="T889" id="Seg_6942" s="T888">onʼiʔ</ta>
            <ta e="T890" id="Seg_6943" s="T889">ne</ta>
            <ta e="T891" id="Seg_6944" s="T890">nanəʔzəbi</ta>
            <ta e="T892" id="Seg_6945" s="T891">i-bi</ta>
            <ta e="T893" id="Seg_6946" s="T892">dĭgəttə</ta>
            <ta e="T894" id="Seg_6947" s="T893">šidəʔ-pi</ta>
            <ta e="T895" id="Seg_6948" s="T894">a</ta>
            <ta e="T896" id="Seg_6949" s="T895">dĭ</ta>
            <ta e="T897" id="Seg_6950" s="T896">dibər</ta>
            <ta e="T898" id="Seg_6951" s="T897">kam-bi</ta>
            <ta e="T899" id="Seg_6952" s="T898">da</ta>
            <ta e="T900" id="Seg_6953" s="T899">bĭʔ-pi</ta>
            <ta e="T901" id="Seg_6954" s="T900">ara</ta>
            <ta e="T902" id="Seg_6955" s="T901">măn</ta>
            <ta e="T903" id="Seg_6956" s="T902">taldʼen</ta>
            <ta e="T904" id="Seg_6957" s="T903">ambam</ta>
            <ta e="T905" id="Seg_6958" s="T904">măndə-r</ta>
            <ta e="T906" id="Seg_6959" s="T905">măndo-bia-m</ta>
            <ta e="T907" id="Seg_6960" s="T906">a</ta>
            <ta e="T908" id="Seg_6961" s="T907">teinen</ta>
            <ta e="T910" id="Seg_6962" s="T908">nüdʼi-n</ta>
            <ta e="T911" id="Seg_6963" s="T910">teinen</ta>
            <ta e="T913" id="Seg_6964" s="T911">nüdʼi-n</ta>
            <ta e="T914" id="Seg_6965" s="T913">dĭgəttə</ta>
            <ta e="T915" id="Seg_6966" s="T914">dĭ-m</ta>
            <ta e="T916" id="Seg_6967" s="T915">dʼoduni</ta>
            <ta e="T917" id="Seg_6968" s="T916">ku-bia-m</ta>
            <ta e="T918" id="Seg_6969" s="T917">ĭmbi=də</ta>
            <ta e="T919" id="Seg_6970" s="T918">deʔ-pi</ta>
            <ta e="T920" id="Seg_6971" s="T919">a</ta>
            <ta e="T921" id="Seg_6972" s="T920">măn</ta>
            <ta e="T922" id="Seg_6973" s="T921">kurol-laʔ-pia-m</ta>
            <ta e="T923" id="Seg_6974" s="T922">bar</ta>
            <ta e="T924" id="Seg_6975" s="T923">tăneldə</ta>
            <ta e="T925" id="Seg_6976" s="T924">ej</ta>
            <ta e="T926" id="Seg_6977" s="T925">šo-bi-ʔi</ta>
            <ta e="T927" id="Seg_6978" s="T926">dʼije-gə</ta>
            <ta e="T928" id="Seg_6979" s="T927">kamən</ta>
            <ta e="T929" id="Seg_6980" s="T928">dĭ-zeŋ</ta>
            <ta e="T930" id="Seg_6981" s="T929">šo-lə-ʔjə</ta>
            <ta e="T931" id="Seg_6982" s="T930">teinen</ta>
            <ta e="T932" id="Seg_6983" s="T931">šo-lə-ʔjə</ta>
            <ta e="T933" id="Seg_6984" s="T932">nüdʼi-ndə</ta>
            <ta e="T934" id="Seg_6985" s="T933">amor-zittə</ta>
            <ta e="T935" id="Seg_6986" s="T934">nada</ta>
            <ta e="T936" id="Seg_6987" s="T935">dĭ-zeŋ-də</ta>
            <ta e="T937" id="Seg_6988" s="T936">dĭgəttə</ta>
            <ta e="T938" id="Seg_6989" s="T937">mĭ-zittə</ta>
            <ta e="T939" id="Seg_6990" s="T938">dĭ-zeŋ</ta>
            <ta e="T940" id="Seg_6991" s="T939">bar</ta>
            <ta e="T941" id="Seg_6992" s="T940">püjö-lia-ʔi</ta>
            <ta e="T942" id="Seg_6993" s="T941">măn</ta>
            <ta e="T943" id="Seg_6994" s="T942">teinen</ta>
            <ta e="T944" id="Seg_6995" s="T943">tenö-bia-m</ta>
            <ta e="T945" id="Seg_6996" s="T944">šiʔ</ta>
            <ta e="T946" id="Seg_6997" s="T945">bar</ta>
            <ta e="T947" id="Seg_6998" s="T946">šo-bi-laʔ</ta>
            <ta e="T948" id="Seg_6999" s="T947">a</ta>
            <ta e="T949" id="Seg_7000" s="T948">šiʔ</ta>
            <ta e="T950" id="Seg_7001" s="T949">ej</ta>
            <ta e="T951" id="Seg_7002" s="T950">šo-bi-laʔ</ta>
            <ta e="T952" id="Seg_7003" s="T951">măn</ta>
            <ta e="T953" id="Seg_7004" s="T952">teinen</ta>
            <ta e="T955" id="Seg_7005" s="T954">teinen</ta>
            <ta e="T956" id="Seg_7006" s="T955">munəj-ʔjə</ta>
            <ta e="T957" id="Seg_7007" s="T956">mĭnzər-bie-m</ta>
            <ta e="T958" id="Seg_7008" s="T957">kălba</ta>
            <ta e="T959" id="Seg_7009" s="T958">dʼagar-bia-m</ta>
            <ta e="T960" id="Seg_7010" s="T959">munuj-ʔ</ta>
            <ta e="T961" id="Seg_7011" s="T960">em-bie-m</ta>
            <ta e="T962" id="Seg_7012" s="T961">gorom</ta>
            <ta e="T963" id="Seg_7013" s="T962">em-bie-m</ta>
            <ta e="T964" id="Seg_7014" s="T963">šiʔnʼileʔ</ta>
            <ta e="T965" id="Seg_7015" s="T964">bădə-sʼtə</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T1" id="Seg_7016" s="T0">davaj</ta>
            <ta e="T2" id="Seg_7017" s="T1">nʼe-zittə</ta>
            <ta e="T4" id="Seg_7018" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_7019" s="T4">üdʼüge</ta>
            <ta e="T6" id="Seg_7020" s="T5">penzi</ta>
            <ta e="T8" id="Seg_7021" s="T7">üžü-Tə</ta>
            <ta e="T9" id="Seg_7022" s="T8">üzə-bi</ta>
            <ta e="T10" id="Seg_7023" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_7024" s="T10">dĭgəttə</ta>
            <ta e="T12" id="Seg_7025" s="T11">süʔmə-luʔbdə-bi</ta>
            <ta e="T13" id="Seg_7026" s="T12">bar</ta>
            <ta e="T14" id="Seg_7027" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_7028" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_7029" s="T15">bĭdə-bi</ta>
            <ta e="T17" id="Seg_7030" s="T16">bĭdə-bi</ta>
            <ta e="T18" id="Seg_7031" s="T17">dĭgəttə</ta>
            <ta e="T19" id="Seg_7032" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_7033" s="T19">üžü-ttə</ta>
            <ta e="T21" id="Seg_7034" s="T20">saʔmə-luʔbdə-bi</ta>
            <ta e="T22" id="Seg_7035" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_7036" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_7037" s="T23">ej</ta>
            <ta e="T25" id="Seg_7038" s="T24">ej</ta>
            <ta e="T26" id="Seg_7039" s="T25">kan-bi</ta>
            <ta e="T27" id="Seg_7040" s="T26">măn</ta>
            <ta e="T28" id="Seg_7041" s="T27">dĭ-Tə</ta>
            <ta e="T29" id="Seg_7042" s="T28">ugaːndə</ta>
            <ta e="T31" id="Seg_7043" s="T30">mĭ-liA-m</ta>
            <ta e="T32" id="Seg_7044" s="T31">a</ta>
            <ta e="T33" id="Seg_7045" s="T32">dĭn</ta>
            <ta e="T34" id="Seg_7046" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_7047" s="T34">măna</ta>
            <ta e="T36" id="Seg_7048" s="T35">kamən=də</ta>
            <ta e="T37" id="Seg_7049" s="T36">ej</ta>
            <ta e="T38" id="Seg_7050" s="T37">šo-liA</ta>
            <ta e="T39" id="Seg_7051" s="T38">măn</ta>
            <ta e="T40" id="Seg_7052" s="T39">ugaːndə</ta>
            <ta e="T41" id="Seg_7053" s="T40">tăŋ</ta>
            <ta e="T42" id="Seg_7054" s="T41">măndo-r-liA-m</ta>
            <ta e="T43" id="Seg_7055" s="T42">jakšə</ta>
            <ta e="T44" id="Seg_7056" s="T43">măndo-r-t</ta>
            <ta e="T45" id="Seg_7057" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_7058" s="T45">kuza-m</ta>
            <ta e="T47" id="Seg_7059" s="T46">kădaʔ</ta>
            <ta e="T48" id="Seg_7060" s="T47">kăštə-zittə</ta>
            <ta e="T49" id="Seg_7061" s="T48">i</ta>
            <ta e="T50" id="Seg_7062" s="T49">kădaʔ</ta>
            <ta e="T51" id="Seg_7063" s="T50">numəj-zittə</ta>
            <ta e="T52" id="Seg_7064" s="T51">nʼuʔdə</ta>
            <ta e="T53" id="Seg_7065" s="T52">măndo-r-ə-ʔ</ta>
            <ta e="T54" id="Seg_7066" s="T53">nʼuʔdə</ta>
            <ta e="T55" id="Seg_7067" s="T54">măndo-r-ə-ʔ</ta>
            <ta e="T56" id="Seg_7068" s="T55">măja-Tə</ta>
            <ta e="T57" id="Seg_7069" s="T56">nadə</ta>
            <ta e="T58" id="Seg_7070" s="T57">teʔme-m</ta>
            <ta e="T59" id="Seg_7071" s="T58">sar-zittə</ta>
            <ta e="T60" id="Seg_7072" s="T59">üžü-m</ta>
            <ta e="T61" id="Seg_7073" s="T60">nadə</ta>
            <ta e="T62" id="Seg_7074" s="T61">šer-zittə</ta>
            <ta e="T63" id="Seg_7075" s="T62">parga-m</ta>
            <ta e="T64" id="Seg_7076" s="T63">nadə</ta>
            <ta e="T65" id="Seg_7077" s="T64">šer-zittə</ta>
            <ta e="T66" id="Seg_7078" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_7079" s="T66">nʼiʔdə</ta>
            <ta e="T68" id="Seg_7080" s="T67">kan-zittə</ta>
            <ta e="T69" id="Seg_7081" s="T68">măn</ta>
            <ta e="T70" id="Seg_7082" s="T69">čum-bə</ta>
            <ta e="T71" id="Seg_7083" s="T70">kan-bi-m</ta>
            <ta e="T72" id="Seg_7084" s="T71">čum-Kən</ta>
            <ta e="T73" id="Seg_7085" s="T72">amno-bi-m</ta>
            <ta e="T74" id="Seg_7086" s="T73">dĭgəttə</ta>
            <ta e="T75" id="Seg_7087" s="T74">baroʔ</ta>
            <ta e="T967" id="Seg_7088" s="T75">kan-lAʔ</ta>
            <ta e="T76" id="Seg_7089" s="T967">tʼür-bi-m</ta>
            <ta e="T78" id="Seg_7090" s="T77">čum-gəndə</ta>
            <ta e="T79" id="Seg_7091" s="T78">măn</ta>
            <ta e="T80" id="Seg_7092" s="T79">üdʼüge</ta>
            <ta e="T81" id="Seg_7093" s="T80">i-bi-m</ta>
            <ta e="T82" id="Seg_7094" s="T81">ine</ta>
            <ta e="T83" id="Seg_7095" s="T82">i-bi</ta>
            <ta e="T85" id="Seg_7096" s="T84">miʔ</ta>
            <ta e="T86" id="Seg_7097" s="T85">šide</ta>
            <ta e="T87" id="Seg_7098" s="T86">šide</ta>
            <ta e="T90" id="Seg_7099" s="T89">kö</ta>
            <ta e="T91" id="Seg_7100" s="T90">bar</ta>
            <ta e="T92" id="Seg_7101" s="T91">körer-laʔpi-bAʔ</ta>
            <ta e="T93" id="Seg_7102" s="T92">dĭʔə</ta>
            <ta e="T94" id="Seg_7103" s="T93">kandə-gA</ta>
            <ta e="T95" id="Seg_7104" s="T94">măn</ta>
            <ta e="T96" id="Seg_7105" s="T95">kan-lV-m</ta>
            <ta e="T97" id="Seg_7106" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_7107" s="T97">dĭ</ta>
            <ta e="T100" id="Seg_7108" s="T99">kan-lV-j</ta>
            <ta e="T101" id="Seg_7109" s="T100">măn-ziʔ</ta>
            <ta e="T102" id="Seg_7110" s="T101">sʼa-bi-m</ta>
            <ta e="T103" id="Seg_7111" s="T102">čum-Tə</ta>
            <ta e="T104" id="Seg_7112" s="T103">amnə-bi-m</ta>
            <ta e="T105" id="Seg_7113" s="T104">čum-dən</ta>
            <ta e="T106" id="Seg_7114" s="T105">dĭgəttə</ta>
            <ta e="T108" id="Seg_7115" s="T107">üzə-bi-m</ta>
            <ta e="T109" id="Seg_7116" s="T108">čum-Tə</ta>
            <ta e="T110" id="Seg_7117" s="T109">tura</ta>
            <ta e="T111" id="Seg_7118" s="T110">bar</ta>
            <ta e="T113" id="Seg_7119" s="T112">neni-luʔbdə-bi</ta>
            <ta e="T114" id="Seg_7120" s="T113">il</ta>
            <ta e="T115" id="Seg_7121" s="T114">bar</ta>
            <ta e="T116" id="Seg_7122" s="T115">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T117" id="Seg_7123" s="T116">bü</ta>
            <ta e="T118" id="Seg_7124" s="T117">det-laʔbə-jəʔ</ta>
            <ta e="T119" id="Seg_7125" s="T118">šü-Tə</ta>
            <ta e="T120" id="Seg_7126" s="T119">kăn-laʔbə-jəʔ</ta>
            <ta e="T121" id="Seg_7127" s="T120">bar</ta>
            <ta e="T122" id="Seg_7128" s="T121">štobɨ</ta>
            <ta e="T123" id="Seg_7129" s="T122">ej</ta>
            <ta e="T124" id="Seg_7130" s="T123">neni-bi</ta>
            <ta e="T125" id="Seg_7131" s="T124">onʼiʔ</ta>
            <ta e="T126" id="Seg_7132" s="T125">kuza</ta>
            <ta e="T127" id="Seg_7133" s="T126">onʼiʔ</ta>
            <ta e="T128" id="Seg_7134" s="T127">kuza-Tə</ta>
            <ta e="T129" id="Seg_7135" s="T128">bar</ta>
            <ta e="T130" id="Seg_7136" s="T129">ipek</ta>
            <ta e="T131" id="Seg_7137" s="T130">nen-də-bi</ta>
            <ta e="T132" id="Seg_7138" s="T131">bar</ta>
            <ta e="T133" id="Seg_7139" s="T132">nen-də-luʔbdə-bi</ta>
            <ta e="T134" id="Seg_7140" s="T133">xatʼel</ta>
            <ta e="T137" id="Seg_7141" s="T136">nuldə-zittə</ta>
            <ta e="T138" id="Seg_7142" s="T137">a</ta>
            <ta e="T139" id="Seg_7143" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_7144" s="T139">i-bi</ta>
            <ta e="T141" id="Seg_7145" s="T140">da</ta>
            <ta e="T142" id="Seg_7146" s="T141">ipek</ta>
            <ta e="T143" id="Seg_7147" s="T142">nen-də-luʔbdə-bi</ta>
            <ta e="T144" id="Seg_7148" s="T143">kudo-nzə-bi</ta>
            <ta e="T146" id="Seg_7149" s="T145">dĭ-ziʔ</ta>
            <ta e="T147" id="Seg_7150" s="T146">dĭ</ta>
            <ta e="T148" id="Seg_7151" s="T147">kuroː-luʔbdə-bi</ta>
            <ta e="T149" id="Seg_7152" s="T148">noʔ</ta>
            <ta e="T150" id="Seg_7153" s="T149">hʼaʔ-bi-m</ta>
            <ta e="T151" id="Seg_7154" s="T150">zarot-Tə</ta>
            <ta e="T152" id="Seg_7155" s="T151">tažor-bi-m</ta>
            <ta e="T153" id="Seg_7156" s="T152">hen-bi-m</ta>
            <ta e="T154" id="Seg_7157" s="T153">a</ta>
            <ta e="T155" id="Seg_7158" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_7159" s="T155">bar</ta>
            <ta e="T157" id="Seg_7160" s="T156">neni-luʔbdə-bi</ta>
            <ta e="T158" id="Seg_7161" s="T157">maʔ-Tə</ta>
            <ta e="T159" id="Seg_7162" s="T158">šo-bi-m</ta>
            <ta e="T160" id="Seg_7163" s="T159">dĭn</ta>
            <ta e="T161" id="Seg_7164" s="T160">bar</ta>
            <ta e="T162" id="Seg_7165" s="T161">nu-bi-m</ta>
            <ta e="T163" id="Seg_7166" s="T162">dĭgəttə</ta>
            <ta e="T165" id="Seg_7167" s="T164">maʔ-Tə</ta>
            <ta e="T968" id="Seg_7168" s="T165">kan-lAʔ</ta>
            <ta e="T166" id="Seg_7169" s="T968">tʼür-bi-m</ta>
            <ta e="T167" id="Seg_7170" s="T166">teinen</ta>
            <ta e="T168" id="Seg_7171" s="T167">măn</ta>
            <ta e="T169" id="Seg_7172" s="T168">ej</ta>
            <ta e="T170" id="Seg_7173" s="T169">togonər-liA-m</ta>
            <ta e="T171" id="Seg_7174" s="T170">a</ta>
            <ta e="T172" id="Seg_7175" s="T171">tăn-ziʔ</ta>
            <ta e="T173" id="Seg_7176" s="T172">tʼăbaktər-liA-m</ta>
            <ta e="T174" id="Seg_7177" s="T173">xotʼ</ta>
            <ta e="T175" id="Seg_7178" s="T174">ej</ta>
            <ta e="T176" id="Seg_7179" s="T175">sedem</ta>
            <ta e="T177" id="Seg_7180" s="T176">a</ta>
            <ta e="T178" id="Seg_7181" s="T177">tarar-luʔbdə-bi-m</ta>
            <ta e="T179" id="Seg_7182" s="T178">ugaːndə</ta>
            <ta e="T180" id="Seg_7183" s="T179">ugaːndə</ta>
            <ta e="T181" id="Seg_7184" s="T180">surno</ta>
            <ta e="T182" id="Seg_7185" s="T181">tăŋ</ta>
            <ta e="T183" id="Seg_7186" s="T182">kandə-gA</ta>
            <ta e="T184" id="Seg_7187" s="T183">šonə-gA</ta>
            <ta e="T186" id="Seg_7188" s="T185">a</ta>
            <ta e="T187" id="Seg_7189" s="T186">măn</ta>
            <ta e="T189" id="Seg_7190" s="T188">pa-jəʔ</ta>
            <ta e="T190" id="Seg_7191" s="T189">amnol-bi-m</ta>
            <ta e="T191" id="Seg_7192" s="T190">beržə</ta>
            <ta e="T192" id="Seg_7193" s="T191">bar</ta>
            <ta e="T193" id="Seg_7194" s="T192">šonə-gA</ta>
            <ta e="T194" id="Seg_7195" s="T193">măn</ta>
            <ta e="T195" id="Seg_7196" s="T194">kujnek</ta>
            <ta e="T196" id="Seg_7197" s="T195">kuvas</ta>
            <ta e="T197" id="Seg_7198" s="T196">dĭn</ta>
            <ta e="T198" id="Seg_7199" s="T197">amno-bi-m</ta>
            <ta e="T199" id="Seg_7200" s="T198">măna</ta>
            <ta e="T200" id="Seg_7201" s="T199">ej</ta>
            <ta e="T201" id="Seg_7202" s="T200">nünör-bi</ta>
            <ta e="T202" id="Seg_7203" s="T201">ato</ta>
            <ta e="T203" id="Seg_7204" s="T202">bɨ</ta>
            <ta e="T204" id="Seg_7205" s="T203">nünör-bi</ta>
            <ta e="T205" id="Seg_7206" s="T204">bar</ta>
            <ta e="T206" id="Seg_7207" s="T205">măn</ta>
            <ta e="T207" id="Seg_7208" s="T206">dʼije-Tə</ta>
            <ta e="T208" id="Seg_7209" s="T207">mĭn-bi-m</ta>
            <ta e="T209" id="Seg_7210" s="T208">nüjnə</ta>
            <ta e="T211" id="Seg_7211" s="T210">nüj-bi-m</ta>
            <ta e="T212" id="Seg_7212" s="T211">a</ta>
            <ta e="T213" id="Seg_7213" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_7214" s="T213">tʼor-bi-m</ta>
            <ta e="T215" id="Seg_7215" s="T214">măn</ta>
            <ta e="T216" id="Seg_7216" s="T215">nʼi-m</ta>
            <ta e="T217" id="Seg_7217" s="T216">kü-laːm-bi</ta>
            <ta e="T218" id="Seg_7218" s="T217">măn</ta>
            <ta e="T219" id="Seg_7219" s="T218">ajir-bi-m</ta>
            <ta e="T220" id="Seg_7220" s="T219">nüke</ta>
            <ta e="T221" id="Seg_7221" s="T220">i</ta>
            <ta e="T222" id="Seg_7222" s="T221">büzʼe</ta>
            <ta e="T223" id="Seg_7223" s="T222">amno-laʔbə-jəʔ</ta>
            <ta e="T224" id="Seg_7224" s="T223">a</ta>
            <ta e="T225" id="Seg_7225" s="T224">tăn</ta>
            <ta e="T226" id="Seg_7226" s="T225">šojo-Tə</ta>
            <ta e="T227" id="Seg_7227" s="T226">amnə-ʔ</ta>
            <ta e="T228" id="Seg_7228" s="T227">gijen</ta>
            <ta e="T229" id="Seg_7229" s="T228">i-bi-l</ta>
            <ta e="T230" id="Seg_7230" s="T229">gibər</ta>
            <ta e="T231" id="Seg_7231" s="T230">kandə-gA-l</ta>
            <ta e="T232" id="Seg_7232" s="T231">maʔ-Tə</ta>
            <ta e="T233" id="Seg_7233" s="T232">kandə-gA-m</ta>
            <ta e="T234" id="Seg_7234" s="T233">aktʼa</ta>
            <ta e="T235" id="Seg_7235" s="T234">aktʼa</ta>
            <ta e="T236" id="Seg_7236" s="T235">naga</ta>
            <ta e="T237" id="Seg_7237" s="T236">aktʼa-žət</ta>
            <ta e="T238" id="Seg_7238" s="T237">amno-zittə</ta>
            <ta e="T239" id="Seg_7239" s="T238">ej</ta>
            <ta e="T240" id="Seg_7240" s="T239">jakšə</ta>
            <ta e="T241" id="Seg_7241" s="T240">aktʼa</ta>
            <ta e="T242" id="Seg_7242" s="T241">aktʼa</ta>
            <ta e="T243" id="Seg_7243" s="T242">i-gA</ta>
            <ta e="T244" id="Seg_7244" s="T243">tak</ta>
            <ta e="T245" id="Seg_7245" s="T244">jakšə</ta>
            <ta e="T246" id="Seg_7246" s="T245">amno-zittə</ta>
            <ta e="T247" id="Seg_7247" s="T246">aktʼa-Tə</ta>
            <ta e="T248" id="Seg_7248" s="T247">ĭmbi=nʼibudʼ</ta>
            <ta e="T249" id="Seg_7249" s="T248">i-lV-l</ta>
            <ta e="T250" id="Seg_7250" s="T249">munəj-jəʔ</ta>
            <ta e="T251" id="Seg_7251" s="T250">možna</ta>
            <ta e="T252" id="Seg_7252" s="T251">i-zittə</ta>
            <ta e="T253" id="Seg_7253" s="T252">ipek</ta>
            <ta e="T254" id="Seg_7254" s="T253">i-zittə</ta>
            <ta e="T255" id="Seg_7255" s="T254">kola</ta>
            <ta e="T256" id="Seg_7256" s="T255">i-zittə</ta>
            <ta e="T257" id="Seg_7257" s="T256">uja</ta>
            <ta e="T258" id="Seg_7258" s="T257">i-lV-l</ta>
            <ta e="T259" id="Seg_7259" s="T258">aktʼa-Tə</ta>
            <ta e="T260" id="Seg_7260" s="T259">üge</ta>
            <ta e="T261" id="Seg_7261" s="T260">aktʼa</ta>
            <ta e="T262" id="Seg_7262" s="T261">naga</ta>
            <ta e="T263" id="Seg_7263" s="T262">tak</ta>
            <ta e="T264" id="Seg_7264" s="T263">ĭmbi=də</ta>
            <ta e="T265" id="Seg_7265" s="T264">ej</ta>
            <ta e="T266" id="Seg_7266" s="T265">i-lV-l</ta>
            <ta e="T267" id="Seg_7267" s="T266">kola</ta>
            <ta e="T268" id="Seg_7268" s="T267">dʼabə-zittə</ta>
            <ta e="T269" id="Seg_7269" s="T268">kandə-gA-l</ta>
            <ta e="T270" id="Seg_7270" s="T269">kola</ta>
            <ta e="T271" id="Seg_7271" s="T270">ej</ta>
            <ta e="T273" id="Seg_7272" s="T272">dʼabə-bi-l</ta>
            <ta e="T274" id="Seg_7273" s="T273">tak</ta>
            <ta e="T275" id="Seg_7274" s="T274">e-ʔ</ta>
            <ta e="T276" id="Seg_7275" s="T275">šo-ʔ</ta>
            <ta e="T277" id="Seg_7276" s="T276">maʔ-Tə-l</ta>
            <ta e="T278" id="Seg_7277" s="T277">kan-ə-ʔ</ta>
            <ta e="T279" id="Seg_7278" s="T278">tugan-m</ta>
            <ta e="T280" id="Seg_7279" s="T279">kăštə-ʔ</ta>
            <ta e="T281" id="Seg_7280" s="T280">ej</ta>
            <ta e="T282" id="Seg_7281" s="T281">kan-lV-jəʔ</ta>
            <ta e="T283" id="Seg_7282" s="T282">tak</ta>
            <ta e="T284" id="Seg_7283" s="T283">bos-də</ta>
            <ta e="T285" id="Seg_7284" s="T284">e-ʔ</ta>
            <ta e="T286" id="Seg_7285" s="T285">šo-ʔ</ta>
            <ta e="T287" id="Seg_7286" s="T286">kăštə-t</ta>
            <ta e="T288" id="Seg_7287" s="T287">jakšə-ŋ</ta>
            <ta e="T289" id="Seg_7288" s="T288">štobɨ</ta>
            <ta e="T290" id="Seg_7289" s="T289">šo-bi-jəʔ</ta>
            <ta e="T291" id="Seg_7290" s="T290">ej</ta>
            <ta e="T292" id="Seg_7291" s="T291">jakšə</ta>
            <ta e="T293" id="Seg_7292" s="T292">kuza</ta>
            <ta e="T294" id="Seg_7293" s="T293">arda</ta>
            <ta e="T295" id="Seg_7294" s="T294">sima-m-ziʔ</ta>
            <ta e="T296" id="Seg_7295" s="T295">ej</ta>
            <ta e="T297" id="Seg_7296" s="T296">mo-liA-m</ta>
            <ta e="T298" id="Seg_7297" s="T297">măndo-zittə</ta>
            <ta e="T299" id="Seg_7298" s="T298">putʼəga</ta>
            <ta e="T300" id="Seg_7299" s="T299">ugaːndə</ta>
            <ta e="T301" id="Seg_7300" s="T300">koldʼəŋ</ta>
            <ta e="T302" id="Seg_7301" s="T301">kan-ə-ʔ</ta>
            <ta e="T303" id="Seg_7302" s="T302">tăŋ</ta>
            <ta e="T304" id="Seg_7303" s="T303">e-ʔ</ta>
            <ta e="T305" id="Seg_7304" s="T304">mĭn-ə-ʔ</ta>
            <ta e="T306" id="Seg_7305" s="T305">ku-liA-m</ta>
            <ta e="T307" id="Seg_7306" s="T306">bar</ta>
            <ta e="T308" id="Seg_7307" s="T307">dĭ</ta>
            <ta e="T309" id="Seg_7308" s="T308">bos-də</ta>
            <ta e="T310" id="Seg_7309" s="T309">šonə-gA</ta>
            <ta e="T311" id="Seg_7310" s="T310">măna</ta>
            <ta e="T312" id="Seg_7311" s="T311">bos-gəndə</ta>
            <ta e="T313" id="Seg_7312" s="T312">bar</ta>
            <ta e="T314" id="Seg_7313" s="T313">manʼiʔ-laʔbə</ta>
            <ta e="T315" id="Seg_7314" s="T314">a</ta>
            <ta e="T316" id="Seg_7315" s="T315">măn</ta>
            <ta e="T317" id="Seg_7316" s="T316">ej</ta>
            <ta e="T318" id="Seg_7317" s="T317">kan-liA-m</ta>
            <ta e="T319" id="Seg_7318" s="T318">dʼije-Kən</ta>
            <ta e="T321" id="Seg_7319" s="T320">pa-jəʔ</ta>
            <ta e="T322" id="Seg_7320" s="T321">bar</ta>
            <ta e="T323" id="Seg_7321" s="T322">özer-laʔbə-jəʔ</ta>
            <ta e="T324" id="Seg_7322" s="T323">kuvas</ta>
            <ta e="T325" id="Seg_7323" s="T324">svetok-jəʔ</ta>
            <ta e="T326" id="Seg_7324" s="T325">bar</ta>
            <ta e="T327" id="Seg_7325" s="T326">ugaːndə</ta>
            <ta e="T328" id="Seg_7326" s="T327">tăŋ</ta>
            <ta e="T329" id="Seg_7327" s="T328">putʼtʼəm-liA-jəʔ</ta>
            <ta e="T330" id="Seg_7328" s="T329">ugaːndə</ta>
            <ta e="T331" id="Seg_7329" s="T330">nünə-liA-m</ta>
            <ta e="T332" id="Seg_7330" s="T331">jakšə</ta>
            <ta e="T333" id="Seg_7331" s="T332">putʼtʼəm-liA</ta>
            <ta e="T334" id="Seg_7332" s="T333">ĭmbi=də</ta>
            <ta e="T335" id="Seg_7333" s="T334">mĭnzər-laʔbə</ta>
            <ta e="T336" id="Seg_7334" s="T335">kuriza-jəʔ</ta>
            <ta e="T337" id="Seg_7335" s="T336">mĭn-gA-jəʔ</ta>
            <ta e="T338" id="Seg_7336" s="T337">tʼo-Kən</ta>
            <ta e="T339" id="Seg_7337" s="T338">bar</ta>
            <ta e="T340" id="Seg_7338" s="T339">tʼo-m</ta>
            <ta e="T341" id="Seg_7339" s="T340">kadaʔ-laʔbə-jəʔ</ta>
            <ta e="T343" id="Seg_7340" s="T342">üjü-zAŋ-də</ta>
            <ta e="T347" id="Seg_7341" s="T346">dĭ</ta>
            <ta e="T348" id="Seg_7342" s="T347">kuza-m</ta>
            <ta e="T349" id="Seg_7343" s="T348">nadə</ta>
            <ta e="T351" id="Seg_7344" s="T350">dĭ</ta>
            <ta e="T353" id="Seg_7345" s="T352">edəʔ-zittə</ta>
            <ta e="T354" id="Seg_7346" s="T353">dĭ</ta>
            <ta e="T355" id="Seg_7347" s="T354">bar</ta>
            <ta e="T356" id="Seg_7348" s="T355">büžü</ta>
            <ta e="T357" id="Seg_7349" s="T356">šo-lV-j</ta>
            <ta e="T358" id="Seg_7350" s="T357">ara</ta>
            <ta e="T359" id="Seg_7351" s="T358">det-lV-j</ta>
            <ta e="T360" id="Seg_7352" s="T359">dĭ-zAŋ</ta>
            <ta e="T361" id="Seg_7353" s="T360">bar</ta>
            <ta e="T362" id="Seg_7354" s="T361">dĭ-m</ta>
            <ta e="T363" id="Seg_7355" s="T362">edəʔ-laʔbə-jəʔ</ta>
            <ta e="T364" id="Seg_7356" s="T363">dĭ-zAŋ</ta>
            <ta e="T365" id="Seg_7357" s="T364">dĭ-zAŋ</ta>
            <ta e="T366" id="Seg_7358" s="T365">edəʔ-laʔbə-jəʔ</ta>
            <ta e="T367" id="Seg_7359" s="T366">teinen</ta>
            <ta e="T368" id="Seg_7360" s="T367">miʔ</ta>
            <ta e="T369" id="Seg_7361" s="T368">jakšə</ta>
            <ta e="T370" id="Seg_7362" s="T369">tʼăbaktər-lV</ta>
            <ta e="T371" id="Seg_7363" s="T370">a</ta>
            <ta e="T372" id="Seg_7364" s="T371">ĭmbi</ta>
            <ta e="T373" id="Seg_7365" s="T372">tʼăbaktər-laʔpi-lAʔ</ta>
            <ta e="T374" id="Seg_7366" s="T373">nan</ta>
            <ta e="T375" id="Seg_7367" s="T374">kudonz-bi-bAʔ</ta>
            <ta e="T376" id="Seg_7368" s="T375">i</ta>
            <ta e="T377" id="Seg_7369" s="T376">bar</ta>
            <ta e="T378" id="Seg_7370" s="T377">šiʔ</ta>
            <ta e="T379" id="Seg_7371" s="T378">kudonz-bi-bAʔ</ta>
            <ta e="T380" id="Seg_7372" s="T379">ine-jəʔ</ta>
            <ta e="T381" id="Seg_7373" s="T380">bar</ta>
            <ta e="T382" id="Seg_7374" s="T381">kan-ntə-gA-jəʔ</ta>
            <ta e="T383" id="Seg_7375" s="T382">šide</ta>
            <ta e="T384" id="Seg_7376" s="T383">ine</ta>
            <ta e="T385" id="Seg_7377" s="T384">kandə-gA</ta>
            <ta e="T386" id="Seg_7378" s="T385">onʼiʔ</ta>
            <ta e="T387" id="Seg_7379" s="T386">ine</ta>
            <ta e="T388" id="Seg_7380" s="T387">kandə-gA</ta>
            <ta e="T389" id="Seg_7381" s="T388">măn</ta>
            <ta e="T390" id="Seg_7382" s="T389">nʼi-m</ta>
            <ta e="T391" id="Seg_7383" s="T390">šonə-gA</ta>
            <ta e="T392" id="Seg_7384" s="T391">a</ta>
            <ta e="T393" id="Seg_7385" s="T392">gijen</ta>
            <ta e="T394" id="Seg_7386" s="T393">i-bi</ta>
            <ta e="T395" id="Seg_7387" s="T394">kola</ta>
            <ta e="T396" id="Seg_7388" s="T395">i-lAʔ</ta>
            <ta e="T397" id="Seg_7389" s="T396">mĭn-bi</ta>
            <ta e="T398" id="Seg_7390" s="T397">ej</ta>
            <ta e="T399" id="Seg_7391" s="T398">tĭm-liA-m</ta>
            <ta e="T400" id="Seg_7392" s="T399">det-bi</ta>
            <ta e="T401" id="Seg_7393" s="T400">li</ta>
            <ta e="T402" id="Seg_7394" s="T401">ej</ta>
            <ta e="T403" id="Seg_7395" s="T402">det-bi</ta>
            <ta e="T404" id="Seg_7396" s="T403">det-bi</ta>
            <ta e="T405" id="Seg_7397" s="T404">tak</ta>
            <ta e="T406" id="Seg_7398" s="T405">pür-zittə</ta>
            <ta e="T407" id="Seg_7399" s="T406">nadə</ta>
            <ta e="T408" id="Seg_7400" s="T407">dĭ</ta>
            <ta e="T409" id="Seg_7401" s="T408">ne</ta>
            <ta e="T410" id="Seg_7402" s="T409">bar</ta>
            <ta e="T411" id="Seg_7403" s="T410">nanəʔzəbi</ta>
            <ta e="T412" id="Seg_7404" s="T411">büžü</ta>
            <ta e="T413" id="Seg_7405" s="T412">büre</ta>
            <ta e="T414" id="Seg_7406" s="T413">ešši</ta>
            <ta e="T415" id="Seg_7407" s="T414">det-lV-j</ta>
            <ta e="T416" id="Seg_7408" s="T415">nadə</ta>
            <ta e="T417" id="Seg_7409" s="T416">dĭ-m</ta>
            <ta e="T418" id="Seg_7410" s="T417">nereʔ-luʔbdə-zittə</ta>
            <ta e="T419" id="Seg_7411" s="T418">kan-zittə</ta>
            <ta e="T420" id="Seg_7412" s="T419">nadə</ta>
            <ta e="T421" id="Seg_7413" s="T420">ĭmbi=nʼibudʼ</ta>
            <ta e="T422" id="Seg_7414" s="T421">sădar-lAʔ</ta>
            <ta e="T423" id="Seg_7415" s="T422">i-zittə</ta>
            <ta e="T424" id="Seg_7416" s="T423">kamən</ta>
            <ta e="T426" id="Seg_7417" s="T424">tʼala</ta>
            <ta e="T428" id="Seg_7418" s="T427">kamən</ta>
            <ta e="T429" id="Seg_7419" s="T428">tʼala</ta>
            <ta e="T430" id="Seg_7420" s="T429">šo-laʔbə</ta>
            <ta e="T431" id="Seg_7421" s="T430">i</ta>
            <ta e="T432" id="Seg_7422" s="T431">măn</ta>
            <ta e="T433" id="Seg_7423" s="T432">uʔbdə-laʔbə-m</ta>
            <ta e="T434" id="Seg_7424" s="T433">kamən</ta>
            <ta e="T435" id="Seg_7425" s="T434">tʼala</ta>
            <ta e="T436" id="Seg_7426" s="T435">kandə-gA</ta>
            <ta e="T437" id="Seg_7427" s="T436">măn</ta>
            <ta e="T438" id="Seg_7428" s="T437">iʔbö-lV-m</ta>
            <ta e="T439" id="Seg_7429" s="T438">dĭ</ta>
            <ta e="T440" id="Seg_7430" s="T439">kö</ta>
            <ta e="T441" id="Seg_7431" s="T440">ugaːndə</ta>
            <ta e="T442" id="Seg_7432" s="T441">šišəge</ta>
            <ta e="T443" id="Seg_7433" s="T442">i-bi</ta>
            <ta e="T444" id="Seg_7434" s="T443">ugaːndə</ta>
            <ta e="T445" id="Seg_7435" s="T444">numo</ta>
            <ta e="T446" id="Seg_7436" s="T445">i-bi</ta>
            <ta e="T447" id="Seg_7437" s="T446">nüdʼi-jəʔ</ta>
            <ta e="T448" id="Seg_7438" s="T447">bar</ta>
            <ta e="T449" id="Seg_7439" s="T448">ugaːndə</ta>
            <ta e="T453" id="Seg_7440" s="T452">nugo-jəʔ</ta>
            <ta e="T454" id="Seg_7441" s="T453">a</ta>
            <ta e="T455" id="Seg_7442" s="T454">dĭ</ta>
            <ta e="T456" id="Seg_7443" s="T455">pʼe</ta>
            <ta e="T457" id="Seg_7444" s="T456">šo-bi</ta>
            <ta e="T458" id="Seg_7445" s="T457">tak</ta>
            <ta e="T459" id="Seg_7446" s="T458">ugaːndə</ta>
            <ta e="T460" id="Seg_7447" s="T459">ejü</ta>
            <ta e="T461" id="Seg_7448" s="T460">tʼala-jəʔ</ta>
            <ta e="T462" id="Seg_7449" s="T461">bar</ta>
            <ta e="T463" id="Seg_7450" s="T462">numo-jəʔ</ta>
            <ta e="T464" id="Seg_7451" s="T463">nadə</ta>
            <ta e="T465" id="Seg_7452" s="T464">măna</ta>
            <ta e="T466" id="Seg_7453" s="T465">bü</ta>
            <ta e="T467" id="Seg_7454" s="T466">pʼeːš-Tə</ta>
            <ta e="T468" id="Seg_7455" s="T467">nuldə-zittə</ta>
            <ta e="T469" id="Seg_7456" s="T468">ato</ta>
            <ta e="T470" id="Seg_7457" s="T469">büzəj</ta>
            <ta e="T471" id="Seg_7458" s="T470">šo-lV-j</ta>
            <ta e="T472" id="Seg_7459" s="T471">bü</ta>
            <ta e="T473" id="Seg_7460" s="T472">naga</ta>
            <ta e="T474" id="Seg_7461" s="T473">a</ta>
            <ta e="T475" id="Seg_7462" s="T474">dĭ</ta>
            <ta e="T476" id="Seg_7463" s="T475">šišəge</ta>
            <ta e="T477" id="Seg_7464" s="T476">bü</ta>
            <ta e="T478" id="Seg_7465" s="T477">ej</ta>
            <ta e="T479" id="Seg_7466" s="T478">bĭs-liA</ta>
            <ta e="T482" id="Seg_7467" s="T481">nuldə-ʔ</ta>
            <ta e="T483" id="Seg_7468" s="T482">bos-də</ta>
            <ta e="T484" id="Seg_7469" s="T483">ine</ta>
            <ta e="T485" id="Seg_7470" s="T484">dĭ</ta>
            <ta e="T486" id="Seg_7471" s="T485">padʼi</ta>
            <ta e="T487" id="Seg_7472" s="T486">ej</ta>
            <ta e="T488" id="Seg_7473" s="T487">nu-liA</ta>
            <ta e="T489" id="Seg_7474" s="T488">nadə</ta>
            <ta e="T490" id="Seg_7475" s="T489">dĭ-m</ta>
            <ta e="T491" id="Seg_7476" s="T490">nuldə-zittə</ta>
            <ta e="T492" id="Seg_7477" s="T491">nʼi</ta>
            <ta e="T493" id="Seg_7478" s="T492">koʔbdo-ziʔ</ta>
            <ta e="T494" id="Seg_7479" s="T493">bar</ta>
            <ta e="T495" id="Seg_7480" s="T494">özer-bi-jəʔ</ta>
            <ta e="T496" id="Seg_7481" s="T495">dĭgəttə</ta>
            <ta e="T497" id="Seg_7482" s="T496">nʼi</ta>
            <ta e="T498" id="Seg_7483" s="T497">măn-bi</ta>
            <ta e="T500" id="Seg_7484" s="T499">măna</ta>
            <ta e="T501" id="Seg_7485" s="T500">kan-lV-l</ta>
            <ta e="T502" id="Seg_7486" s="T501">tibi-Tə</ta>
            <ta e="T503" id="Seg_7487" s="T502">kan-lV-m</ta>
            <ta e="T504" id="Seg_7488" s="T503">nu</ta>
            <ta e="T505" id="Seg_7489" s="T504">măn</ta>
            <ta e="T506" id="Seg_7490" s="T505">ija-m</ta>
            <ta e="T507" id="Seg_7491" s="T506">aba-m</ta>
            <ta e="T508" id="Seg_7492" s="T507">öʔ-lV-m</ta>
            <ta e="T509" id="Seg_7493" s="T508">tănan</ta>
            <ta e="T510" id="Seg_7494" s="T509">dĭgəttə</ta>
            <ta e="T511" id="Seg_7495" s="T510">ija-t</ta>
            <ta e="T512" id="Seg_7496" s="T511">ija-m</ta>
            <ta e="T513" id="Seg_7497" s="T512">aba-m</ta>
            <ta e="T514" id="Seg_7498" s="T513">öʔlu-bi</ta>
            <ta e="T515" id="Seg_7499" s="T514">dĭ-zAŋ</ta>
            <ta e="T516" id="Seg_7500" s="T515">šo-bi-jəʔ</ta>
            <ta e="T517" id="Seg_7501" s="T516">koʔbdo-n</ta>
            <ta e="T518" id="Seg_7502" s="T517">ija-ziʔ</ta>
            <ta e="T519" id="Seg_7503" s="T518">aba-ziʔ</ta>
            <ta e="T520" id="Seg_7504" s="T519">tʼăbaktər-luʔbdə-bi-jəʔ</ta>
            <ta e="T521" id="Seg_7505" s="T520">koʔbdo-l</ta>
            <ta e="T523" id="Seg_7506" s="T522">mĭ-bi-lAʔ</ta>
            <ta e="T524" id="Seg_7507" s="T523">măn</ta>
            <ta e="T526" id="Seg_7508" s="T525">măn</ta>
            <ta e="T527" id="Seg_7509" s="T526">nʼi-Tə</ta>
            <ta e="T529" id="Seg_7510" s="T528">mĭ-bi-bAʔ</ta>
            <ta e="T531" id="Seg_7511" s="T530">dĭgəttə</ta>
            <ta e="T532" id="Seg_7512" s="T531">koʔbdo-m</ta>
            <ta e="T533" id="Seg_7513" s="T532">surar-bi-jəʔ</ta>
            <ta e="T534" id="Seg_7514" s="T533">tăn</ta>
            <ta e="T535" id="Seg_7515" s="T534">kan-lV-l</ta>
            <ta e="T536" id="Seg_7516" s="T535">miʔ</ta>
            <ta e="T537" id="Seg_7517" s="T536">nʼi-Tə</ta>
            <ta e="T538" id="Seg_7518" s="T537">tibi-Tə</ta>
            <ta e="T539" id="Seg_7519" s="T538">kan-lV-m</ta>
            <ta e="T540" id="Seg_7520" s="T539">dĭgəttə</ta>
            <ta e="T541" id="Seg_7521" s="T540">monoʔko-bi-jəʔ</ta>
            <ta e="T542" id="Seg_7522" s="T541">ara</ta>
            <ta e="T543" id="Seg_7523" s="T542">det-bi-jəʔ</ta>
            <ta e="T544" id="Seg_7524" s="T543">ipek</ta>
            <ta e="T545" id="Seg_7525" s="T544">det-bi-jəʔ</ta>
            <ta e="T546" id="Seg_7526" s="T545">uja</ta>
            <ta e="T547" id="Seg_7527" s="T546">iʔgö</ta>
            <ta e="T548" id="Seg_7528" s="T547">i-bi</ta>
            <ta e="T549" id="Seg_7529" s="T548">dĭgəttə</ta>
            <ta e="T550" id="Seg_7530" s="T549">il</ta>
            <ta e="T551" id="Seg_7531" s="T550">šo-bi-jəʔ</ta>
            <ta e="T552" id="Seg_7532" s="T551">ara</ta>
            <ta e="T553" id="Seg_7533" s="T552">bĭs-bi-jəʔ</ta>
            <ta e="T554" id="Seg_7534" s="T553">bar</ta>
            <ta e="T555" id="Seg_7535" s="T554">dĭgəttə</ta>
            <ta e="T558" id="Seg_7536" s="T557">tʼala</ta>
            <ta e="T559" id="Seg_7537" s="T558">kan-bi</ta>
            <ta e="T560" id="Seg_7538" s="T559">dĭ-zAŋ</ta>
            <ta e="T561" id="Seg_7539" s="T560">svetok-jəʔ</ta>
            <ta e="T562" id="Seg_7540" s="T561">šer-bi-jəʔ</ta>
            <ta e="T563" id="Seg_7541" s="T562">i</ta>
            <ta e="T564" id="Seg_7542" s="T563">tʼegermaʔ-Tə</ta>
            <ta e="T565" id="Seg_7543" s="T564">kan-bi-jəʔ</ta>
            <ta e="T566" id="Seg_7544" s="T565">dĭn</ta>
            <ta e="T567" id="Seg_7545" s="T566">abəs</ta>
            <ta e="T568" id="Seg_7546" s="T567">dĭ-zem</ta>
            <ta e="T569" id="Seg_7547" s="T568">kudaj</ta>
            <ta e="T570" id="Seg_7548" s="T569">numan üzə-bi</ta>
            <ta e="T571" id="Seg_7549" s="T570">dĭgəttə</ta>
            <ta e="T574" id="Seg_7550" s="T573">vʼenok-jəʔ</ta>
            <ta e="T575" id="Seg_7551" s="T574">šer-bi-jəʔ</ta>
            <ta e="T576" id="Seg_7552" s="T575">dĭgəttə</ta>
            <ta e="T577" id="Seg_7553" s="T576">maʔ-gəndə</ta>
            <ta e="T578" id="Seg_7554" s="T577">šo-bi-jəʔ</ta>
            <ta e="T579" id="Seg_7555" s="T578">tože</ta>
            <ta e="T580" id="Seg_7556" s="T579">bar</ta>
            <ta e="T581" id="Seg_7557" s="T580">ara</ta>
            <ta e="T582" id="Seg_7558" s="T581">bĭs-bi-jəʔ</ta>
            <ta e="T583" id="Seg_7559" s="T582">koʔbdo-n</ta>
            <ta e="T584" id="Seg_7560" s="T583">tugan-Tə</ta>
            <ta e="T585" id="Seg_7561" s="T584">šo-bi-jəʔ</ta>
            <ta e="T586" id="Seg_7562" s="T585">i</ta>
            <ta e="T587" id="Seg_7563" s="T586">nʼi-n</ta>
            <ta e="T588" id="Seg_7564" s="T587">koʔbdo</ta>
            <ta e="T589" id="Seg_7565" s="T588">šo-bi-jəʔ</ta>
            <ta e="T590" id="Seg_7566" s="T589">iʔgö</ta>
            <ta e="T591" id="Seg_7567" s="T590">il</ta>
            <ta e="T592" id="Seg_7568" s="T591">i-bi-jəʔ</ta>
            <ta e="T593" id="Seg_7569" s="T592">ara</ta>
            <ta e="T594" id="Seg_7570" s="T593">iʔgö</ta>
            <ta e="T595" id="Seg_7571" s="T594">i-bi</ta>
            <ta e="T596" id="Seg_7572" s="T595">stol-Kən</ta>
            <ta e="T597" id="Seg_7573" s="T596">ugaːndə</ta>
            <ta e="T598" id="Seg_7574" s="T597">iʔgö</ta>
            <ta e="T599" id="Seg_7575" s="T598">ĭmbi</ta>
            <ta e="T601" id="Seg_7576" s="T600">iʔbö-laʔbə</ta>
            <ta e="T602" id="Seg_7577" s="T601">kamən</ta>
            <ta e="T603" id="Seg_7578" s="T602">koʔbdo</ta>
            <ta e="T604" id="Seg_7579" s="T603">monoʔko-bi-jəʔ</ta>
            <ta e="T605" id="Seg_7580" s="T604">ija-t</ta>
            <ta e="T606" id="Seg_7581" s="T605">aba-t</ta>
            <ta e="T607" id="Seg_7582" s="T606">măn-bi-jəʔ</ta>
            <ta e="T608" id="Seg_7583" s="T607">dĭ-n</ta>
            <ta e="T609" id="Seg_7584" s="T608">oldʼa-t</ta>
            <ta e="T610" id="Seg_7585" s="T609">naga</ta>
            <ta e="T611" id="Seg_7586" s="T610">nadə</ta>
            <ta e="T612" id="Seg_7587" s="T611">bătʼinkă-jəʔ</ta>
            <ta e="T613" id="Seg_7588" s="T612">i-zittə</ta>
            <ta e="T614" id="Seg_7589" s="T613">nadə</ta>
            <ta e="T615" id="Seg_7590" s="T614">palʼto</ta>
            <ta e="T616" id="Seg_7591" s="T615">i-zittə</ta>
            <ta e="T617" id="Seg_7592" s="T616">plat</ta>
            <ta e="T618" id="Seg_7593" s="T617">i-zittə</ta>
            <ta e="T619" id="Seg_7594" s="T618">oldʼa-t</ta>
            <ta e="T620" id="Seg_7595" s="T619">amka</ta>
            <ta e="T621" id="Seg_7596" s="T620">kujnek-də</ta>
            <ta e="T622" id="Seg_7597" s="T621">naga</ta>
            <ta e="T623" id="Seg_7598" s="T622">dĭgəttə</ta>
            <ta e="T625" id="Seg_7599" s="T624">dĭ-zAŋ</ta>
            <ta e="T626" id="Seg_7600" s="T625">aktʼa-m</ta>
            <ta e="T627" id="Seg_7601" s="T626">i-bi-jəʔ</ta>
            <ta e="T628" id="Seg_7602" s="T627">šide</ta>
            <ta e="T629" id="Seg_7603" s="T628">biəʔ</ta>
            <ta e="T630" id="Seg_7604" s="T629">sumna</ta>
            <ta e="T631" id="Seg_7605" s="T630">dĭgəttə</ta>
            <ta e="T632" id="Seg_7606" s="T631">ija-t</ta>
            <ta e="T633" id="Seg_7607" s="T632">aba-n</ta>
            <ta e="T634" id="Seg_7608" s="T633">dĭ-Tə</ta>
            <ta e="T635" id="Seg_7609" s="T634">mĭ-bi-jəʔ</ta>
            <ta e="T636" id="Seg_7610" s="T635">tüžöj</ta>
            <ta e="T637" id="Seg_7611" s="T636">tugan-zAŋ-dən</ta>
            <ta e="T638" id="Seg_7612" s="T637">mĭ-bi-jəʔ</ta>
            <ta e="T639" id="Seg_7613" s="T638">ular</ta>
            <ta e="T640" id="Seg_7614" s="T639">mĭ-bi-jəʔ</ta>
            <ta e="T641" id="Seg_7615" s="T640">ine-jəʔ</ta>
            <ta e="T642" id="Seg_7616" s="T641">aktʼa</ta>
            <ta e="T643" id="Seg_7617" s="T642">mĭ-bi-jəʔ</ta>
            <ta e="T644" id="Seg_7618" s="T643">ugaːndə</ta>
            <ta e="T645" id="Seg_7619" s="T644">iʔgö</ta>
            <ta e="T647" id="Seg_7620" s="T646">il</ta>
            <ta e="T648" id="Seg_7621" s="T647">iʔgö</ta>
            <ta e="T649" id="Seg_7622" s="T648">i-bi-jəʔ</ta>
            <ta e="T650" id="Seg_7623" s="T649">dĭgəttə</ta>
            <ta e="T651" id="Seg_7624" s="T650">dĭ-zAŋ</ta>
            <ta e="T652" id="Seg_7625" s="T651">kondʼo</ta>
            <ta e="T653" id="Seg_7626" s="T652">amno-bi-jəʔ</ta>
            <ta e="T654" id="Seg_7627" s="T653">kaga-t</ta>
            <ta e="T657" id="Seg_7628" s="T656">da</ta>
            <ta e="T658" id="Seg_7629" s="T657">bazoʔ</ta>
            <ta e="T659" id="Seg_7630" s="T658">dărəʔ</ta>
            <ta e="T660" id="Seg_7631" s="T659">monoʔko-bi</ta>
            <ta e="T661" id="Seg_7632" s="T660">dĭgəttə</ta>
            <ta e="T662" id="Seg_7633" s="T661">dĭ</ta>
            <ta e="T969" id="Seg_7634" s="T662">kan-lAʔ</ta>
            <ta e="T663" id="Seg_7635" s="T969">tʼür-bi</ta>
            <ta e="T664" id="Seg_7636" s="T663">dĭ-Tə</ta>
            <ta e="T665" id="Seg_7637" s="T664">aba-t</ta>
            <ta e="T666" id="Seg_7638" s="T665">mĭ-bi</ta>
            <ta e="T667" id="Seg_7639" s="T666">bar</ta>
            <ta e="T668" id="Seg_7640" s="T667">bar</ta>
            <ta e="T669" id="Seg_7641" s="T668">ĭmbi</ta>
            <ta e="T670" id="Seg_7642" s="T669">mĭ-bi</ta>
            <ta e="T671" id="Seg_7643" s="T670">ine</ta>
            <ta e="T672" id="Seg_7644" s="T671">mĭ-bi</ta>
            <ta e="T673" id="Seg_7645" s="T672">ular</ta>
            <ta e="T674" id="Seg_7646" s="T673">mĭ-bi</ta>
            <ta e="T675" id="Seg_7647" s="T674">tüžöj-jəʔ</ta>
            <ta e="T676" id="Seg_7648" s="T675">mĭ-bi</ta>
            <ta e="T677" id="Seg_7649" s="T676">ipek</ta>
            <ta e="T678" id="Seg_7650" s="T677">mĭ-bi</ta>
            <ta e="T680" id="Seg_7651" s="T679">tura</ta>
            <ta e="T681" id="Seg_7652" s="T680">nuldə-bi</ta>
            <ta e="T682" id="Seg_7653" s="T681">dĭ-zAŋ</ta>
            <ta e="T685" id="Seg_7654" s="T684">kan-bi-jəʔ</ta>
            <ta e="T687" id="Seg_7655" s="T686">ine-jəʔ-ziʔ</ta>
            <ta e="T688" id="Seg_7656" s="T687">koʔbdo</ta>
            <ta e="T689" id="Seg_7657" s="T688">bos-də</ta>
            <ta e="T690" id="Seg_7658" s="T689">kazak</ta>
            <ta e="T691" id="Seg_7659" s="T690">ija-t</ta>
            <ta e="T692" id="Seg_7660" s="T691">a</ta>
            <ta e="T693" id="Seg_7661" s="T692">nʼi</ta>
            <ta e="T694" id="Seg_7662" s="T693">bos-də</ta>
            <ta e="T695" id="Seg_7663" s="T694">kazak</ta>
            <ta e="T696" id="Seg_7664" s="T695">ija-t</ta>
            <ta e="T697" id="Seg_7665" s="T696">kazak</ta>
            <ta e="T698" id="Seg_7666" s="T697">aba-t</ta>
            <ta e="T699" id="Seg_7667" s="T698">dĭbər</ta>
            <ta e="T700" id="Seg_7668" s="T699">kan-bi-jəʔ</ta>
            <ta e="T701" id="Seg_7669" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_7670" s="T701">šo-bi-jəʔ</ta>
            <ta e="T703" id="Seg_7671" s="T702">maʔ-gəndə</ta>
            <ta e="T704" id="Seg_7672" s="T703">a</ta>
            <ta e="T705" id="Seg_7673" s="T704">dĭgəttə</ta>
            <ta e="T706" id="Seg_7674" s="T705">koʔbdo-n</ta>
            <ta e="T707" id="Seg_7675" s="T706">kazak</ta>
            <ta e="T708" id="Seg_7676" s="T707">aba-t</ta>
            <ta e="T710" id="Seg_7677" s="T709">oldʼa</ta>
            <ta e="T711" id="Seg_7678" s="T710">det-bi</ta>
            <ta e="T713" id="Seg_7679" s="T712">bar</ta>
            <ta e="T714" id="Seg_7680" s="T713">păduškă-jəʔ</ta>
            <ta e="T715" id="Seg_7681" s="T714">det-bi</ta>
            <ta e="T716" id="Seg_7682" s="T715">bar</ta>
            <ta e="T717" id="Seg_7683" s="T716">pʼerină</ta>
            <ta e="T718" id="Seg_7684" s="T717">det-bi</ta>
            <ta e="T719" id="Seg_7685" s="T718">dĭgəttə</ta>
            <ta e="T720" id="Seg_7686" s="T719">šo-bi-jəʔ</ta>
            <ta e="T721" id="Seg_7687" s="T720">tʼegermaʔ-Tə</ta>
            <ta e="T722" id="Seg_7688" s="T721">dĭ-zAŋ</ta>
            <ta e="T723" id="Seg_7689" s="T722">amnə-bi-jəʔ</ta>
            <ta e="T725" id="Seg_7690" s="T724">stol-də</ta>
            <ta e="T727" id="Seg_7691" s="T725">dĭgəttə</ta>
            <ta e="T728" id="Seg_7692" s="T727">nʼi-n</ta>
            <ta e="T729" id="Seg_7693" s="T728">kujnek-də</ta>
            <ta e="T730" id="Seg_7694" s="T729">kuvas</ta>
            <ta e="T731" id="Seg_7695" s="T730">toʔbdə</ta>
            <ta e="T732" id="Seg_7696" s="T731">koʔbdo-n</ta>
            <ta e="T733" id="Seg_7697" s="T732">oldʼa-t</ta>
            <ta e="T734" id="Seg_7698" s="T733">tože</ta>
            <ta e="T735" id="Seg_7699" s="T734">kuvas</ta>
            <ta e="T736" id="Seg_7700" s="T735">toʔbdə</ta>
            <ta e="T737" id="Seg_7701" s="T736">ulu-t</ta>
            <ta e="T738" id="Seg_7702" s="T737">bar</ta>
            <ta e="T739" id="Seg_7703" s="T738">kuvas</ta>
            <ta e="T740" id="Seg_7704" s="T739">vʼenok</ta>
            <ta e="T741" id="Seg_7705" s="T740">šer-bi-jəʔ</ta>
            <ta e="T742" id="Seg_7706" s="T741">dĭgəttə</ta>
            <ta e="T743" id="Seg_7707" s="T742">dĭ-Tə</ta>
            <ta e="T747" id="Seg_7708" s="T746">dĭgəttə</ta>
            <ta e="T748" id="Seg_7709" s="T747">dĭ-Tə</ta>
            <ta e="T749" id="Seg_7710" s="T748">šide</ta>
            <ta e="T750" id="Seg_7711" s="T749">kosa</ta>
            <ta e="T751" id="Seg_7712" s="T750">kür-bi-jəʔ</ta>
            <ta e="T752" id="Seg_7713" s="T751">dĭgəttə</ta>
            <ta e="T753" id="Seg_7714" s="T752">sar-bi-jəʔ</ta>
            <ta e="T754" id="Seg_7715" s="T753">ulu-t</ta>
            <ta e="T755" id="Seg_7716" s="T754">kamən</ta>
            <ta e="T756" id="Seg_7717" s="T755">dĭ</ta>
            <ta e="T757" id="Seg_7718" s="T756">koʔbdo</ta>
            <ta e="T758" id="Seg_7719" s="T757">i-bi</ta>
            <ta e="T759" id="Seg_7720" s="T758">onʼiʔ</ta>
            <ta e="T761" id="Seg_7721" s="T760">kosa</ta>
            <ta e="T762" id="Seg_7722" s="T761">i-bi</ta>
            <ta e="T763" id="Seg_7723" s="T762">dĭgəttə</ta>
            <ta e="T765" id="Seg_7724" s="T764">kazak</ta>
            <ta e="T766" id="Seg_7725" s="T765">ija-t</ta>
            <ta e="T767" id="Seg_7726" s="T766">dĭ</ta>
            <ta e="T768" id="Seg_7727" s="T767">koʔbdo-n</ta>
            <ta e="T769" id="Seg_7728" s="T768">kazak</ta>
            <ta e="T770" id="Seg_7729" s="T769">ija-t</ta>
            <ta e="T771" id="Seg_7730" s="T770">i</ta>
            <ta e="T772" id="Seg_7731" s="T771">nʼi-n</ta>
            <ta e="T773" id="Seg_7732" s="T772">kazak</ta>
            <ta e="T774" id="Seg_7733" s="T773">ija-t</ta>
            <ta e="T775" id="Seg_7734" s="T774">dĭ-Tə</ta>
            <ta e="T776" id="Seg_7735" s="T775">šide</ta>
            <ta e="T777" id="Seg_7736" s="T776">kür-bi-jəʔ</ta>
            <ta e="T778" id="Seg_7737" s="T777">i</ta>
            <ta e="T779" id="Seg_7738" s="T778">sar-bi-jəʔ</ta>
            <ta e="T780" id="Seg_7739" s="T779">ulu-t</ta>
            <ta e="T781" id="Seg_7740" s="T780">dĭ-zAŋ-də</ta>
            <ta e="T782" id="Seg_7741" s="T781">nʼi</ta>
            <ta e="T783" id="Seg_7742" s="T782">koʔbdo-m</ta>
            <ta e="T784" id="Seg_7743" s="T783">kazak</ta>
            <ta e="T785" id="Seg_7744" s="T784">aba-t</ta>
            <ta e="T787" id="Seg_7745" s="T786">kun-bi</ta>
            <ta e="T788" id="Seg_7746" s="T787">tura-Tə</ta>
            <ta e="T789" id="Seg_7747" s="T788">dĭn</ta>
            <ta e="T790" id="Seg_7748" s="T789">šində=də</ta>
            <ta e="T791" id="Seg_7749" s="T790">ej</ta>
            <ta e="T792" id="Seg_7750" s="T791">i-bi</ta>
            <ta e="T793" id="Seg_7751" s="T792">iʔbö-bi</ta>
            <ta e="T795" id="Seg_7752" s="T794">kunol-zittə</ta>
            <ta e="T796" id="Seg_7753" s="T795">i</ta>
            <ta e="T797" id="Seg_7754" s="T796">kamro-luʔbdə-bi-jəʔ</ta>
            <ta e="T799" id="Seg_7755" s="T798">tugan-zAŋ-də</ta>
            <ta e="T800" id="Seg_7756" s="T799">dĭ-zAŋ-Tə</ta>
            <ta e="T801" id="Seg_7757" s="T800">măn-bi-jəʔ</ta>
            <ta e="T802" id="Seg_7758" s="T801">jakšə</ta>
            <ta e="T803" id="Seg_7759" s="T802">amno-lAʔ</ta>
            <ta e="T804" id="Seg_7760" s="T803">amno-KAʔ</ta>
            <ta e="T805" id="Seg_7761" s="T804">e-ʔ</ta>
            <ta e="T806" id="Seg_7762" s="T805">tʼabəro-KAʔ</ta>
            <ta e="T807" id="Seg_7763" s="T806">e-ʔ</ta>
            <ta e="T809" id="Seg_7764" s="T808">e-ʔ</ta>
            <ta e="T811" id="Seg_7765" s="T810">e-ʔ</ta>
            <ta e="T812" id="Seg_7766" s="T811">kudo-nzə-ʔ</ta>
            <ta e="T813" id="Seg_7767" s="T812">puskaj</ta>
            <ta e="T815" id="Seg_7768" s="T814">ešši-zAŋ-Tə</ta>
            <ta e="T816" id="Seg_7769" s="T815">iʔgö</ta>
            <ta e="T817" id="Seg_7770" s="T816">mo-lV-j</ta>
            <ta e="T818" id="Seg_7771" s="T817">nʼi-zAŋ</ta>
            <ta e="T819" id="Seg_7772" s="T818">koʔbdo-zAŋ</ta>
            <ta e="T820" id="Seg_7773" s="T819">dĭgəttə</ta>
            <ta e="T821" id="Seg_7774" s="T820">dĭ-zAŋ</ta>
            <ta e="T822" id="Seg_7775" s="T821">abəs</ta>
            <ta e="T823" id="Seg_7776" s="T822">kăštə-bi-jəʔ</ta>
            <ta e="T824" id="Seg_7777" s="T823">dĭ</ta>
            <ta e="T825" id="Seg_7778" s="T824">ugaːndə</ta>
            <ta e="T826" id="Seg_7779" s="T825">iʔgö</ta>
            <ta e="T827" id="Seg_7780" s="T826">ara</ta>
            <ta e="T828" id="Seg_7781" s="T827">bĭs-bi</ta>
            <ta e="T829" id="Seg_7782" s="T828">i</ta>
            <ta e="T830" id="Seg_7783" s="T829">saʔmə-luʔbdə-bi</ta>
            <ta e="T831" id="Seg_7784" s="T830">dĭ-m</ta>
            <ta e="T832" id="Seg_7785" s="T831">kun-laːm-bi-jəʔ</ta>
            <ta e="T833" id="Seg_7786" s="T832">dĭ-zAŋ</ta>
            <ta e="T834" id="Seg_7787" s="T833">ugaːndə</ta>
            <ta e="T835" id="Seg_7788" s="T834">ara</ta>
            <ta e="T836" id="Seg_7789" s="T835">bĭs-bi-jəʔ</ta>
            <ta e="T837" id="Seg_7790" s="T836">kondʼo</ta>
            <ta e="T838" id="Seg_7791" s="T837">muktuʔ</ta>
            <ta e="T839" id="Seg_7792" s="T838">tʼala</ta>
            <ta e="T840" id="Seg_7793" s="T839">bĭs-bi-jəʔ</ta>
            <ta e="T841" id="Seg_7794" s="T840">dĭgəttə</ta>
            <ta e="T842" id="Seg_7795" s="T841">dĭ-zAŋ</ta>
            <ta e="T843" id="Seg_7796" s="T842">bar</ta>
            <ta e="T844" id="Seg_7797" s="T843">amno-bi-jəʔ</ta>
            <ta e="T845" id="Seg_7798" s="T844">dĭ-zAŋ</ta>
            <ta e="T846" id="Seg_7799" s="T845">bar</ta>
            <ta e="T847" id="Seg_7800" s="T846">ešši-zAŋ-Tə</ta>
            <ta e="T848" id="Seg_7801" s="T847">i-bi-jəʔ</ta>
            <ta e="T849" id="Seg_7802" s="T848">nʼi-zAŋ</ta>
            <ta e="T850" id="Seg_7803" s="T849">i-bi-jəʔ</ta>
            <ta e="T852" id="Seg_7804" s="T851">i-bi-jəʔ</ta>
            <ta e="T853" id="Seg_7805" s="T852">koʔbdo-zAŋ</ta>
            <ta e="T857" id="Seg_7806" s="T855">măn</ta>
            <ta e="T858" id="Seg_7807" s="T857">nʼi-m</ta>
            <ta e="T859" id="Seg_7808" s="T858">taldʼen</ta>
            <ta e="T860" id="Seg_7809" s="T859">kan-bi</ta>
            <ta e="T861" id="Seg_7810" s="T860">ara</ta>
            <ta e="T862" id="Seg_7811" s="T861">bĭs-bi</ta>
            <ta e="T863" id="Seg_7812" s="T862">ugaːndə</ta>
            <ta e="T864" id="Seg_7813" s="T863">kondʼo</ta>
            <ta e="T865" id="Seg_7814" s="T864">ej</ta>
            <ta e="T866" id="Seg_7815" s="T865">šo-bi</ta>
            <ta e="T867" id="Seg_7816" s="T866">maʔ-Tə</ta>
            <ta e="T868" id="Seg_7817" s="T867">dĭgəttə</ta>
            <ta e="T869" id="Seg_7818" s="T868">šo-bi</ta>
            <ta e="T870" id="Seg_7819" s="T869">kunol-luʔbdə-bi</ta>
            <ta e="T871" id="Seg_7820" s="T870">i</ta>
            <ta e="T872" id="Seg_7821" s="T871">ertə-n</ta>
            <ta e="T873" id="Seg_7822" s="T872">uʔbdə-bi</ta>
            <ta e="T874" id="Seg_7823" s="T873">i</ta>
            <ta e="T875" id="Seg_7824" s="T874">mo-luʔbdə-bi</ta>
            <ta e="T876" id="Seg_7825" s="T875">togonər-zittə</ta>
            <ta e="T877" id="Seg_7826" s="T876">a</ta>
            <ta e="T878" id="Seg_7827" s="T877">măna</ta>
            <ta e="T880" id="Seg_7828" s="T879">măn</ta>
            <ta e="T881" id="Seg_7829" s="T880">šide</ta>
            <ta e="T882" id="Seg_7830" s="T881">tüžöj</ta>
            <ta e="T883" id="Seg_7831" s="T882">surdo-bi-m</ta>
            <ta e="T884" id="Seg_7832" s="T883">dĭ-n</ta>
            <ta e="T885" id="Seg_7833" s="T884">tüžöj-də</ta>
            <ta e="T886" id="Seg_7834" s="T885">i</ta>
            <ta e="T887" id="Seg_7835" s="T886">bos-də</ta>
            <ta e="T888" id="Seg_7836" s="T887">tüžöj-də</ta>
            <ta e="T889" id="Seg_7837" s="T888">onʼiʔ</ta>
            <ta e="T890" id="Seg_7838" s="T889">ne</ta>
            <ta e="T891" id="Seg_7839" s="T890">nanəʔzəbi</ta>
            <ta e="T892" id="Seg_7840" s="T891">i-bi</ta>
            <ta e="T893" id="Seg_7841" s="T892">dĭgəttə</ta>
            <ta e="T894" id="Seg_7842" s="T893">šidəʔ-bi</ta>
            <ta e="T895" id="Seg_7843" s="T894">a</ta>
            <ta e="T896" id="Seg_7844" s="T895">dĭ</ta>
            <ta e="T897" id="Seg_7845" s="T896">dĭbər</ta>
            <ta e="T898" id="Seg_7846" s="T897">kan-bi</ta>
            <ta e="T899" id="Seg_7847" s="T898">da</ta>
            <ta e="T900" id="Seg_7848" s="T899">bĭs-bi</ta>
            <ta e="T901" id="Seg_7849" s="T900">ara</ta>
            <ta e="T902" id="Seg_7850" s="T901">măn</ta>
            <ta e="T903" id="Seg_7851" s="T902">taldʼen</ta>
            <ta e="T904" id="Seg_7852" s="T903">ambam</ta>
            <ta e="T905" id="Seg_7853" s="T904">măndo-r</ta>
            <ta e="T906" id="Seg_7854" s="T905">măndo-bi-m</ta>
            <ta e="T907" id="Seg_7855" s="T906">a</ta>
            <ta e="T908" id="Seg_7856" s="T907">teinen</ta>
            <ta e="T910" id="Seg_7857" s="T908">nüdʼi-n</ta>
            <ta e="T911" id="Seg_7858" s="T910">teinen</ta>
            <ta e="T913" id="Seg_7859" s="T911">nüdʼi-n</ta>
            <ta e="T914" id="Seg_7860" s="T913">dĭgəttə</ta>
            <ta e="T915" id="Seg_7861" s="T914">dĭ-m</ta>
            <ta e="T916" id="Seg_7862" s="T915">dʼoduni</ta>
            <ta e="T917" id="Seg_7863" s="T916">ku-bi-m</ta>
            <ta e="T918" id="Seg_7864" s="T917">ĭmbi=də</ta>
            <ta e="T919" id="Seg_7865" s="T918">det-bi</ta>
            <ta e="T920" id="Seg_7866" s="T919">a</ta>
            <ta e="T921" id="Seg_7867" s="T920">măn</ta>
            <ta e="T922" id="Seg_7868" s="T921">kurol-lAʔ-bi-m</ta>
            <ta e="T923" id="Seg_7869" s="T922">bar</ta>
            <ta e="T924" id="Seg_7870" s="T923">tăneldə</ta>
            <ta e="T925" id="Seg_7871" s="T924">ej</ta>
            <ta e="T926" id="Seg_7872" s="T925">šo-bi-jəʔ</ta>
            <ta e="T927" id="Seg_7873" s="T926">dʼije-gəʔ</ta>
            <ta e="T928" id="Seg_7874" s="T927">kamən</ta>
            <ta e="T929" id="Seg_7875" s="T928">dĭ-zAŋ</ta>
            <ta e="T930" id="Seg_7876" s="T929">šo-lV-jəʔ</ta>
            <ta e="T931" id="Seg_7877" s="T930">teinen</ta>
            <ta e="T932" id="Seg_7878" s="T931">šo-lV-jəʔ</ta>
            <ta e="T933" id="Seg_7879" s="T932">nüdʼi-gəndə</ta>
            <ta e="T934" id="Seg_7880" s="T933">amor-zittə</ta>
            <ta e="T935" id="Seg_7881" s="T934">nadə</ta>
            <ta e="T936" id="Seg_7882" s="T935">dĭ-zAŋ-Tə</ta>
            <ta e="T937" id="Seg_7883" s="T936">dĭgəttə</ta>
            <ta e="T938" id="Seg_7884" s="T937">mĭ-zittə</ta>
            <ta e="T939" id="Seg_7885" s="T938">dĭ-zAŋ</ta>
            <ta e="T940" id="Seg_7886" s="T939">bar</ta>
            <ta e="T941" id="Seg_7887" s="T940">püjö-liA-jəʔ</ta>
            <ta e="T942" id="Seg_7888" s="T941">măn</ta>
            <ta e="T943" id="Seg_7889" s="T942">teinen</ta>
            <ta e="T944" id="Seg_7890" s="T943">tenö-bi-m</ta>
            <ta e="T945" id="Seg_7891" s="T944">šiʔ</ta>
            <ta e="T946" id="Seg_7892" s="T945">bar</ta>
            <ta e="T947" id="Seg_7893" s="T946">šo-bi-lAʔ</ta>
            <ta e="T948" id="Seg_7894" s="T947">a</ta>
            <ta e="T949" id="Seg_7895" s="T948">šiʔ</ta>
            <ta e="T950" id="Seg_7896" s="T949">ej</ta>
            <ta e="T951" id="Seg_7897" s="T950">šo-bi-lAʔ</ta>
            <ta e="T952" id="Seg_7898" s="T951">măn</ta>
            <ta e="T953" id="Seg_7899" s="T952">teinen</ta>
            <ta e="T955" id="Seg_7900" s="T954">teinen</ta>
            <ta e="T956" id="Seg_7901" s="T955">munəj-jəʔ</ta>
            <ta e="T957" id="Seg_7902" s="T956">mĭnzər-bi-m</ta>
            <ta e="T958" id="Seg_7903" s="T957">kalba</ta>
            <ta e="T959" id="Seg_7904" s="T958">dʼagar-bi-m</ta>
            <ta e="T960" id="Seg_7905" s="T959">munəj-jəʔ</ta>
            <ta e="T961" id="Seg_7906" s="T960">hen-bi-m</ta>
            <ta e="T962" id="Seg_7907" s="T961">gorom</ta>
            <ta e="T963" id="Seg_7908" s="T962">hen-bi-m</ta>
            <ta e="T964" id="Seg_7909" s="T963">šiʔnʼileʔ</ta>
            <ta e="T965" id="Seg_7910" s="T964">bădə-zittə</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T1" id="Seg_7911" s="T0">INCH</ta>
            <ta e="T2" id="Seg_7912" s="T1">give.birth-INF.LAT</ta>
            <ta e="T4" id="Seg_7913" s="T3">one.[NOM.SG]</ta>
            <ta e="T5" id="Seg_7914" s="T4">small.[NOM.SG]</ta>
            <ta e="T6" id="Seg_7915" s="T5">%snake.[NOM.SG]</ta>
            <ta e="T8" id="Seg_7916" s="T7">cap-LAT</ta>
            <ta e="T9" id="Seg_7917" s="T8">descend-PST.[3SG]</ta>
            <ta e="T10" id="Seg_7918" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_7919" s="T10">then</ta>
            <ta e="T12" id="Seg_7920" s="T11">jump-MOM-PST.[3SG]</ta>
            <ta e="T13" id="Seg_7921" s="T12">PTCL</ta>
            <ta e="T14" id="Seg_7922" s="T13">this.[NOM.SG]</ta>
            <ta e="T15" id="Seg_7923" s="T14">this.[NOM.SG]</ta>
            <ta e="T16" id="Seg_7924" s="T15">catch.up-PST.[3SG]</ta>
            <ta e="T17" id="Seg_7925" s="T16">catch.up-PST.[3SG]</ta>
            <ta e="T18" id="Seg_7926" s="T17">then</ta>
            <ta e="T19" id="Seg_7927" s="T18">this.[NOM.SG]</ta>
            <ta e="T20" id="Seg_7928" s="T19">cap-ABL.3SG</ta>
            <ta e="T21" id="Seg_7929" s="T20">fall-MOM-PST.[3SG]</ta>
            <ta e="T22" id="Seg_7930" s="T21">then</ta>
            <ta e="T23" id="Seg_7931" s="T22">this.[NOM.SG]</ta>
            <ta e="T24" id="Seg_7932" s="T23">NEG</ta>
            <ta e="T25" id="Seg_7933" s="T24">NEG</ta>
            <ta e="T26" id="Seg_7934" s="T25">go-PST.[3SG]</ta>
            <ta e="T27" id="Seg_7935" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_7936" s="T27">this-LAT</ta>
            <ta e="T29" id="Seg_7937" s="T28">very</ta>
            <ta e="T31" id="Seg_7938" s="T30">give-PRS-1SG</ta>
            <ta e="T32" id="Seg_7939" s="T31">and</ta>
            <ta e="T33" id="Seg_7940" s="T32">there</ta>
            <ta e="T34" id="Seg_7941" s="T33">this.[NOM.SG]</ta>
            <ta e="T35" id="Seg_7942" s="T34">I.LAT</ta>
            <ta e="T36" id="Seg_7943" s="T35">when=INDEF</ta>
            <ta e="T37" id="Seg_7944" s="T36">NEG</ta>
            <ta e="T38" id="Seg_7945" s="T37">come-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_7946" s="T38">I.NOM</ta>
            <ta e="T40" id="Seg_7947" s="T39">very</ta>
            <ta e="T41" id="Seg_7948" s="T40">strongly</ta>
            <ta e="T42" id="Seg_7949" s="T41">look-FRQ-PRS-1SG</ta>
            <ta e="T43" id="Seg_7950" s="T42">good.[NOM.SG]</ta>
            <ta e="T44" id="Seg_7951" s="T43">look-FRQ-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_7952" s="T44">this.[NOM.SG]</ta>
            <ta e="T46" id="Seg_7953" s="T45">man-ACC</ta>
            <ta e="T47" id="Seg_7954" s="T46">how</ta>
            <ta e="T48" id="Seg_7955" s="T47">call-INF.LAT</ta>
            <ta e="T49" id="Seg_7956" s="T48">and</ta>
            <ta e="T50" id="Seg_7957" s="T49">how</ta>
            <ta e="T51" id="Seg_7958" s="T50">name-INF.LAT</ta>
            <ta e="T52" id="Seg_7959" s="T51">up</ta>
            <ta e="T53" id="Seg_7960" s="T52">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T54" id="Seg_7961" s="T53">up</ta>
            <ta e="T55" id="Seg_7962" s="T54">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T56" id="Seg_7963" s="T55">mountain-LAT</ta>
            <ta e="T57" id="Seg_7964" s="T56">one.should</ta>
            <ta e="T58" id="Seg_7965" s="T57">rope-ACC</ta>
            <ta e="T59" id="Seg_7966" s="T58">bind-INF.LAT</ta>
            <ta e="T60" id="Seg_7967" s="T59">cap-ACC</ta>
            <ta e="T61" id="Seg_7968" s="T60">one.should</ta>
            <ta e="T62" id="Seg_7969" s="T61">dress-INF.LAT</ta>
            <ta e="T63" id="Seg_7970" s="T62">fur.coat-ACC</ta>
            <ta e="T64" id="Seg_7971" s="T63">one.should</ta>
            <ta e="T65" id="Seg_7972" s="T64">dress-INF.LAT</ta>
            <ta e="T66" id="Seg_7973" s="T65">then</ta>
            <ta e="T67" id="Seg_7974" s="T66">outwards</ta>
            <ta e="T68" id="Seg_7975" s="T67">go-INF.LAT</ta>
            <ta e="T69" id="Seg_7976" s="T68">I.NOM</ta>
            <ta e="T70" id="Seg_7977" s="T69">%tent-ACC.3SG</ta>
            <ta e="T71" id="Seg_7978" s="T70">go-PST-1SG</ta>
            <ta e="T72" id="Seg_7979" s="T71">%tent-LOC</ta>
            <ta e="T73" id="Seg_7980" s="T72">sit-PST-1SG</ta>
            <ta e="T74" id="Seg_7981" s="T73">then</ta>
            <ta e="T75" id="Seg_7982" s="T74">%%</ta>
            <ta e="T967" id="Seg_7983" s="T75">go-CVB</ta>
            <ta e="T76" id="Seg_7984" s="T967">disappear-PST-1SG</ta>
            <ta e="T78" id="Seg_7985" s="T77">%tent-LAT/LOC.3SG</ta>
            <ta e="T79" id="Seg_7986" s="T78">I.NOM</ta>
            <ta e="T80" id="Seg_7987" s="T79">small.[NOM.SG]</ta>
            <ta e="T81" id="Seg_7988" s="T80">be-PST-1SG</ta>
            <ta e="T82" id="Seg_7989" s="T81">horse.[NOM.SG]</ta>
            <ta e="T83" id="Seg_7990" s="T82">be-PST.[3SG]</ta>
            <ta e="T85" id="Seg_7991" s="T84">we.NOM</ta>
            <ta e="T86" id="Seg_7992" s="T85">two.[NOM.SG]</ta>
            <ta e="T87" id="Seg_7993" s="T86">two.[NOM.SG]</ta>
            <ta e="T90" id="Seg_7994" s="T89">winter.[NOM.SG]</ta>
            <ta e="T91" id="Seg_7995" s="T90">PTCL</ta>
            <ta e="T92" id="Seg_7996" s="T91">harness-DUR.PST-1PL</ta>
            <ta e="T93" id="Seg_7997" s="T92">from.there</ta>
            <ta e="T94" id="Seg_7998" s="T93">walk-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_7999" s="T94">I.NOM</ta>
            <ta e="T96" id="Seg_8000" s="T95">go-FUT-1SG</ta>
            <ta e="T97" id="Seg_8001" s="T96">then</ta>
            <ta e="T98" id="Seg_8002" s="T97">this.[NOM.SG]</ta>
            <ta e="T100" id="Seg_8003" s="T99">go-FUT-3SG</ta>
            <ta e="T101" id="Seg_8004" s="T100">I.NOM-INS</ta>
            <ta e="T102" id="Seg_8005" s="T101">climb-PST-1SG</ta>
            <ta e="T103" id="Seg_8006" s="T102">%tent-LAT</ta>
            <ta e="T104" id="Seg_8007" s="T103">sit-PST-1SG</ta>
            <ta e="T105" id="Seg_8008" s="T104">%tent-NOM/GEN/ACC.3PL</ta>
            <ta e="T106" id="Seg_8009" s="T105">then</ta>
            <ta e="T108" id="Seg_8010" s="T107">descend-PST-1SG</ta>
            <ta e="T109" id="Seg_8011" s="T108">%tent-LAT</ta>
            <ta e="T110" id="Seg_8012" s="T109">house.[NOM.SG]</ta>
            <ta e="T111" id="Seg_8013" s="T110">PTCL</ta>
            <ta e="T113" id="Seg_8014" s="T112">%burn-MOM-PST.[3SG]</ta>
            <ta e="T114" id="Seg_8015" s="T113">people.[NOM.SG]</ta>
            <ta e="T115" id="Seg_8016" s="T114">PTCL</ta>
            <ta e="T116" id="Seg_8017" s="T115">run-DUR-3PL</ta>
            <ta e="T117" id="Seg_8018" s="T116">water.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8019" s="T117">bring-DUR-3PL</ta>
            <ta e="T119" id="Seg_8020" s="T118">fire-LAT</ta>
            <ta e="T120" id="Seg_8021" s="T119">pour-DUR-3PL</ta>
            <ta e="T121" id="Seg_8022" s="T120">PTCL</ta>
            <ta e="T122" id="Seg_8023" s="T121">so.that</ta>
            <ta e="T123" id="Seg_8024" s="T122">NEG</ta>
            <ta e="T124" id="Seg_8025" s="T123">%burn-PST.[3SG]</ta>
            <ta e="T125" id="Seg_8026" s="T124">one.[NOM.SG]</ta>
            <ta e="T126" id="Seg_8027" s="T125">man.[NOM.SG]</ta>
            <ta e="T127" id="Seg_8028" s="T126">one.[NOM.SG]</ta>
            <ta e="T128" id="Seg_8029" s="T127">man-LAT</ta>
            <ta e="T129" id="Seg_8030" s="T128">PTCL</ta>
            <ta e="T130" id="Seg_8031" s="T129">bread.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8032" s="T130">burn-TR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_8033" s="T131">all</ta>
            <ta e="T133" id="Seg_8034" s="T132">burn-TR-MOM-PST.[3SG]</ta>
            <ta e="T134" id="Seg_8035" s="T133">want.PST.M.SG</ta>
            <ta e="T137" id="Seg_8036" s="T136">place-INF.LAT</ta>
            <ta e="T138" id="Seg_8037" s="T137">and</ta>
            <ta e="T139" id="Seg_8038" s="T138">this.[NOM.SG]</ta>
            <ta e="T140" id="Seg_8039" s="T139">take-PST.[3SG]</ta>
            <ta e="T141" id="Seg_8040" s="T140">and</ta>
            <ta e="T142" id="Seg_8041" s="T141">bread.[NOM.SG]</ta>
            <ta e="T143" id="Seg_8042" s="T142">burn-TR-MOM-PST.[3SG]</ta>
            <ta e="T144" id="Seg_8043" s="T143">scold-DES-PST.[3SG]</ta>
            <ta e="T146" id="Seg_8044" s="T145">this-INS</ta>
            <ta e="T147" id="Seg_8045" s="T146">this.[NOM.SG]</ta>
            <ta e="T148" id="Seg_8046" s="T147">be.angry-MOM-PST.[3SG]</ta>
            <ta e="T149" id="Seg_8047" s="T148">grass.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8048" s="T149">cut-PST-1SG</ta>
            <ta e="T151" id="Seg_8049" s="T150">zarod-LAT</ta>
            <ta e="T152" id="Seg_8050" s="T151">carry-PST-1SG</ta>
            <ta e="T153" id="Seg_8051" s="T152">put-PST-1SG</ta>
            <ta e="T154" id="Seg_8052" s="T153">and</ta>
            <ta e="T155" id="Seg_8053" s="T154">this.[NOM.SG]</ta>
            <ta e="T156" id="Seg_8054" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_8055" s="T156">%burn-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_8056" s="T157">tent-LAT</ta>
            <ta e="T159" id="Seg_8057" s="T158">come-PST-1SG</ta>
            <ta e="T160" id="Seg_8058" s="T159">there</ta>
            <ta e="T161" id="Seg_8059" s="T160">PTCL</ta>
            <ta e="T162" id="Seg_8060" s="T161">stand-PST-1SG</ta>
            <ta e="T163" id="Seg_8061" s="T162">then</ta>
            <ta e="T165" id="Seg_8062" s="T164">tent-LAT</ta>
            <ta e="T968" id="Seg_8063" s="T165">go-CVB</ta>
            <ta e="T166" id="Seg_8064" s="T968">disappear-PST-1SG</ta>
            <ta e="T167" id="Seg_8065" s="T166">today</ta>
            <ta e="T168" id="Seg_8066" s="T167">I.NOM</ta>
            <ta e="T169" id="Seg_8067" s="T168">NEG</ta>
            <ta e="T170" id="Seg_8068" s="T169">work-PRS-1SG</ta>
            <ta e="T171" id="Seg_8069" s="T170">and</ta>
            <ta e="T172" id="Seg_8070" s="T171">you.NOM-INS</ta>
            <ta e="T173" id="Seg_8071" s="T172">speak-PRS-1SG</ta>
            <ta e="T174" id="Seg_8072" s="T173">although</ta>
            <ta e="T175" id="Seg_8073" s="T174">NEG</ta>
            <ta e="T176" id="Seg_8074" s="T175">hard</ta>
            <ta e="T177" id="Seg_8075" s="T176">and</ta>
            <ta e="T178" id="Seg_8076" s="T177">get.tired-MOM-PST-1SG</ta>
            <ta e="T179" id="Seg_8077" s="T178">very</ta>
            <ta e="T180" id="Seg_8078" s="T179">very</ta>
            <ta e="T181" id="Seg_8079" s="T180">rain.[NOM.SG]</ta>
            <ta e="T182" id="Seg_8080" s="T181">strongly</ta>
            <ta e="T183" id="Seg_8081" s="T182">walk-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_8082" s="T183">come-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_8083" s="T185">and</ta>
            <ta e="T187" id="Seg_8084" s="T186">I.NOM</ta>
            <ta e="T189" id="Seg_8085" s="T188">tree-PL</ta>
            <ta e="T190" id="Seg_8086" s="T189">seat-PST-1SG</ta>
            <ta e="T191" id="Seg_8087" s="T190">wind.[NOM.SG]</ta>
            <ta e="T192" id="Seg_8088" s="T191">PTCL</ta>
            <ta e="T193" id="Seg_8089" s="T192">come-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_8090" s="T193">I.NOM</ta>
            <ta e="T195" id="Seg_8091" s="T194">shirt.[NOM.SG]</ta>
            <ta e="T196" id="Seg_8092" s="T195">beautiful.[NOM.SG]</ta>
            <ta e="T197" id="Seg_8093" s="T196">there</ta>
            <ta e="T198" id="Seg_8094" s="T197">sit-PST-1SG</ta>
            <ta e="T199" id="Seg_8095" s="T198">I.LAT</ta>
            <ta e="T200" id="Seg_8096" s="T199">NEG</ta>
            <ta e="T201" id="Seg_8097" s="T200">wet-PST.[3SG]</ta>
            <ta e="T202" id="Seg_8098" s="T201">otherwise</ta>
            <ta e="T203" id="Seg_8099" s="T202">IRREAL</ta>
            <ta e="T204" id="Seg_8100" s="T203">wet-PST.[3SG]</ta>
            <ta e="T205" id="Seg_8101" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_8102" s="T205">I.NOM</ta>
            <ta e="T207" id="Seg_8103" s="T206">forest-LAT</ta>
            <ta e="T208" id="Seg_8104" s="T207">go-PST-1SG</ta>
            <ta e="T209" id="Seg_8105" s="T208">song.[NOM.SG]</ta>
            <ta e="T211" id="Seg_8106" s="T210">sing-PST-1SG</ta>
            <ta e="T212" id="Seg_8107" s="T211">and</ta>
            <ta e="T213" id="Seg_8108" s="T212">then</ta>
            <ta e="T214" id="Seg_8109" s="T213">cry-PST-1SG</ta>
            <ta e="T215" id="Seg_8110" s="T214">I.NOM</ta>
            <ta e="T216" id="Seg_8111" s="T215">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T217" id="Seg_8112" s="T216">die-RES-PST.[3SG]</ta>
            <ta e="T218" id="Seg_8113" s="T217">I.NOM</ta>
            <ta e="T219" id="Seg_8114" s="T218">pity-PST-1SG</ta>
            <ta e="T220" id="Seg_8115" s="T219">woman.[NOM.SG]</ta>
            <ta e="T221" id="Seg_8116" s="T220">and</ta>
            <ta e="T222" id="Seg_8117" s="T221">man.[NOM.SG]</ta>
            <ta e="T223" id="Seg_8118" s="T222">live-DUR-3PL</ta>
            <ta e="T224" id="Seg_8119" s="T223">and</ta>
            <ta e="T225" id="Seg_8120" s="T224">you.NOM</ta>
            <ta e="T226" id="Seg_8121" s="T225">%%-LAT</ta>
            <ta e="T227" id="Seg_8122" s="T226">sit-IMP.2SG</ta>
            <ta e="T228" id="Seg_8123" s="T227">where</ta>
            <ta e="T229" id="Seg_8124" s="T228">be-PST-2SG</ta>
            <ta e="T230" id="Seg_8125" s="T229">where.to</ta>
            <ta e="T231" id="Seg_8126" s="T230">walk-PRS-2SG</ta>
            <ta e="T232" id="Seg_8127" s="T231">tent-LAT</ta>
            <ta e="T233" id="Seg_8128" s="T232">walk-PRS-1SG</ta>
            <ta e="T234" id="Seg_8129" s="T233">money.[NOM.SG]</ta>
            <ta e="T235" id="Seg_8130" s="T234">money.[NOM.SG]</ta>
            <ta e="T236" id="Seg_8131" s="T235">NEG.EX.[3SG]</ta>
            <ta e="T237" id="Seg_8132" s="T236">money-CAR.ADJ</ta>
            <ta e="T238" id="Seg_8133" s="T237">live-INF.LAT</ta>
            <ta e="T239" id="Seg_8134" s="T238">NEG</ta>
            <ta e="T240" id="Seg_8135" s="T239">good.[NOM.SG]</ta>
            <ta e="T241" id="Seg_8136" s="T240">money.[NOM.SG]</ta>
            <ta e="T242" id="Seg_8137" s="T241">money.[NOM.SG]</ta>
            <ta e="T243" id="Seg_8138" s="T242">be-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_8139" s="T243">so</ta>
            <ta e="T245" id="Seg_8140" s="T244">good.[NOM.SG]</ta>
            <ta e="T246" id="Seg_8141" s="T245">live-INF.LAT</ta>
            <ta e="T247" id="Seg_8142" s="T246">money-LAT</ta>
            <ta e="T248" id="Seg_8143" s="T247">what=INDEF</ta>
            <ta e="T249" id="Seg_8144" s="T248">take-FUT-2SG</ta>
            <ta e="T250" id="Seg_8145" s="T249">egg-PL</ta>
            <ta e="T251" id="Seg_8146" s="T250">one.can</ta>
            <ta e="T252" id="Seg_8147" s="T251">take-INF.LAT</ta>
            <ta e="T253" id="Seg_8148" s="T252">bread.[NOM.SG]</ta>
            <ta e="T254" id="Seg_8149" s="T253">take-INF.LAT</ta>
            <ta e="T255" id="Seg_8150" s="T254">fish.[NOM.SG]</ta>
            <ta e="T256" id="Seg_8151" s="T255">take-INF.LAT</ta>
            <ta e="T257" id="Seg_8152" s="T256">meat.[NOM.SG]</ta>
            <ta e="T258" id="Seg_8153" s="T257">take-FUT-2SG</ta>
            <ta e="T259" id="Seg_8154" s="T258">money-LAT</ta>
            <ta e="T260" id="Seg_8155" s="T259">always</ta>
            <ta e="T261" id="Seg_8156" s="T260">money.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8157" s="T261">NEG.EX.[3SG]</ta>
            <ta e="T263" id="Seg_8158" s="T262">so</ta>
            <ta e="T264" id="Seg_8159" s="T263">what.[NOM.SG]=INDEF</ta>
            <ta e="T265" id="Seg_8160" s="T264">NEG</ta>
            <ta e="T266" id="Seg_8161" s="T265">take-FUT-2SG</ta>
            <ta e="T267" id="Seg_8162" s="T266">fish.[NOM.SG]</ta>
            <ta e="T268" id="Seg_8163" s="T267">capture-INF.LAT</ta>
            <ta e="T269" id="Seg_8164" s="T268">walk-PRS-2SG</ta>
            <ta e="T270" id="Seg_8165" s="T269">fish.[NOM.SG]</ta>
            <ta e="T271" id="Seg_8166" s="T270">NEG</ta>
            <ta e="T273" id="Seg_8167" s="T272">capture-PST-2SG</ta>
            <ta e="T274" id="Seg_8168" s="T273">so</ta>
            <ta e="T275" id="Seg_8169" s="T274">NEG.AUX-IMP.2SG</ta>
            <ta e="T276" id="Seg_8170" s="T275">come-CNG</ta>
            <ta e="T277" id="Seg_8171" s="T276">tent-LAT-2SG</ta>
            <ta e="T278" id="Seg_8172" s="T277">go-EP-IMP.2SG</ta>
            <ta e="T279" id="Seg_8173" s="T278">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T280" id="Seg_8174" s="T279">call-IMP.2SG</ta>
            <ta e="T281" id="Seg_8175" s="T280">NEG</ta>
            <ta e="T282" id="Seg_8176" s="T281">go-FUT-3PL</ta>
            <ta e="T283" id="Seg_8177" s="T282">so</ta>
            <ta e="T284" id="Seg_8178" s="T283">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T285" id="Seg_8179" s="T284">NEG.AUX-IMP.2SG</ta>
            <ta e="T286" id="Seg_8180" s="T285">come-CNG</ta>
            <ta e="T287" id="Seg_8181" s="T286">call-IMP.2SG.O</ta>
            <ta e="T288" id="Seg_8182" s="T287">good-LAT.ADV</ta>
            <ta e="T289" id="Seg_8183" s="T288">so.that</ta>
            <ta e="T290" id="Seg_8184" s="T289">come-PST-3PL</ta>
            <ta e="T291" id="Seg_8185" s="T290">NEG</ta>
            <ta e="T292" id="Seg_8186" s="T291">good.[NOM.SG]</ta>
            <ta e="T293" id="Seg_8187" s="T292">man.[NOM.SG]</ta>
            <ta e="T294" id="Seg_8188" s="T293">right</ta>
            <ta e="T295" id="Seg_8189" s="T294">eye-NOM/GEN/ACC.1SG-INS</ta>
            <ta e="T296" id="Seg_8190" s="T295">NEG</ta>
            <ta e="T297" id="Seg_8191" s="T296">can-PRS-1SG</ta>
            <ta e="T298" id="Seg_8192" s="T297">look-INF.LAT</ta>
            <ta e="T299" id="Seg_8193" s="T298">stinking.[NOM.SG]</ta>
            <ta e="T300" id="Seg_8194" s="T299">very</ta>
            <ta e="T301" id="Seg_8195" s="T300">softly</ta>
            <ta e="T302" id="Seg_8196" s="T301">go-EP-IMP.2SG</ta>
            <ta e="T303" id="Seg_8197" s="T302">strongly</ta>
            <ta e="T304" id="Seg_8198" s="T303">NEG.AUX-IMP.2SG</ta>
            <ta e="T305" id="Seg_8199" s="T304">go-EP-CNG</ta>
            <ta e="T306" id="Seg_8200" s="T305">see-PRS-1SG</ta>
            <ta e="T307" id="Seg_8201" s="T306">PTCL</ta>
            <ta e="T308" id="Seg_8202" s="T307">this.[NOM.SG]</ta>
            <ta e="T309" id="Seg_8203" s="T308">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T310" id="Seg_8204" s="T309">come-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_8205" s="T310">I.LAT</ta>
            <ta e="T312" id="Seg_8206" s="T311">self-LAT/LOC.3SG</ta>
            <ta e="T313" id="Seg_8207" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_8208" s="T313">%call-DUR.[3SG]</ta>
            <ta e="T315" id="Seg_8209" s="T314">and</ta>
            <ta e="T316" id="Seg_8210" s="T315">I.NOM</ta>
            <ta e="T317" id="Seg_8211" s="T316">NEG</ta>
            <ta e="T318" id="Seg_8212" s="T317">go-PRS-1SG</ta>
            <ta e="T319" id="Seg_8213" s="T318">forest-LOC</ta>
            <ta e="T321" id="Seg_8214" s="T320">tree-PL</ta>
            <ta e="T322" id="Seg_8215" s="T321">PTCL</ta>
            <ta e="T323" id="Seg_8216" s="T322">grow-DUR-3PL</ta>
            <ta e="T324" id="Seg_8217" s="T323">beautiful.[NOM.SG]</ta>
            <ta e="T325" id="Seg_8218" s="T324">flower-PL</ta>
            <ta e="T326" id="Seg_8219" s="T325">PTCL</ta>
            <ta e="T327" id="Seg_8220" s="T326">very</ta>
            <ta e="T328" id="Seg_8221" s="T327">strongly</ta>
            <ta e="T329" id="Seg_8222" s="T328">smell-PRS-3PL</ta>
            <ta e="T330" id="Seg_8223" s="T329">very</ta>
            <ta e="T331" id="Seg_8224" s="T330">hear-PRS-1SG</ta>
            <ta e="T332" id="Seg_8225" s="T331">good.[NOM.SG]</ta>
            <ta e="T333" id="Seg_8226" s="T332">smell-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_8227" s="T333">what.[NOM.SG]=INDEF</ta>
            <ta e="T335" id="Seg_8228" s="T334">boil-DUR.[3SG]</ta>
            <ta e="T336" id="Seg_8229" s="T335">hen-PL</ta>
            <ta e="T337" id="Seg_8230" s="T336">go-PRS-3PL</ta>
            <ta e="T338" id="Seg_8231" s="T337">earth-LOC</ta>
            <ta e="T339" id="Seg_8232" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_8233" s="T339">earth-ACC</ta>
            <ta e="T341" id="Seg_8234" s="T340">scratch-DUR-3PL</ta>
            <ta e="T343" id="Seg_8235" s="T342">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T347" id="Seg_8236" s="T346">this.[NOM.SG]</ta>
            <ta e="T348" id="Seg_8237" s="T347">man-ACC</ta>
            <ta e="T349" id="Seg_8238" s="T348">one.should</ta>
            <ta e="T351" id="Seg_8239" s="T350">this.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8240" s="T352">wait-INF.LAT</ta>
            <ta e="T354" id="Seg_8241" s="T353">this.[NOM.SG]</ta>
            <ta e="T355" id="Seg_8242" s="T354">PTCL</ta>
            <ta e="T356" id="Seg_8243" s="T355">soon</ta>
            <ta e="T357" id="Seg_8244" s="T356">come-FUT-3SG</ta>
            <ta e="T358" id="Seg_8245" s="T357">vodka.[NOM.SG]</ta>
            <ta e="T359" id="Seg_8246" s="T358">bring-FUT-3SG</ta>
            <ta e="T360" id="Seg_8247" s="T359">this-PL</ta>
            <ta e="T361" id="Seg_8248" s="T360">PTCL</ta>
            <ta e="T362" id="Seg_8249" s="T361">this-ACC</ta>
            <ta e="T363" id="Seg_8250" s="T362">wait-DUR-3PL</ta>
            <ta e="T364" id="Seg_8251" s="T363">this-PL</ta>
            <ta e="T365" id="Seg_8252" s="T364">this-PL</ta>
            <ta e="T366" id="Seg_8253" s="T365">wait-DUR-3PL</ta>
            <ta e="T367" id="Seg_8254" s="T366">today</ta>
            <ta e="T368" id="Seg_8255" s="T367">we.NOM</ta>
            <ta e="T369" id="Seg_8256" s="T368">good.[NOM.SG]</ta>
            <ta e="T370" id="Seg_8257" s="T369">speak-FUT</ta>
            <ta e="T371" id="Seg_8258" s="T370">and</ta>
            <ta e="T372" id="Seg_8259" s="T371">what.[NOM.SG]</ta>
            <ta e="T373" id="Seg_8260" s="T372">speak-DUR.PST-2PL</ta>
            <ta e="T374" id="Seg_8261" s="T373">%%</ta>
            <ta e="T375" id="Seg_8262" s="T374">scold-PST-1PL</ta>
            <ta e="T376" id="Seg_8263" s="T375">and</ta>
            <ta e="T377" id="Seg_8264" s="T376">PTCL</ta>
            <ta e="T378" id="Seg_8265" s="T377">you.PL.NOM</ta>
            <ta e="T379" id="Seg_8266" s="T378">scold-PST-1PL</ta>
            <ta e="T380" id="Seg_8267" s="T379">horse-PL</ta>
            <ta e="T381" id="Seg_8268" s="T380">PTCL</ta>
            <ta e="T382" id="Seg_8269" s="T381">go-IPFVZ-PRS-3PL</ta>
            <ta e="T383" id="Seg_8270" s="T382">two.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8271" s="T383">horse.[NOM.SG]</ta>
            <ta e="T385" id="Seg_8272" s="T384">walk-PRS.[3SG]</ta>
            <ta e="T386" id="Seg_8273" s="T385">one.[NOM.SG]</ta>
            <ta e="T387" id="Seg_8274" s="T386">horse.[NOM.SG]</ta>
            <ta e="T388" id="Seg_8275" s="T387">walk-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_8276" s="T388">I.NOM</ta>
            <ta e="T390" id="Seg_8277" s="T389">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T391" id="Seg_8278" s="T390">come-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_8279" s="T391">and</ta>
            <ta e="T393" id="Seg_8280" s="T392">where</ta>
            <ta e="T394" id="Seg_8281" s="T393">be-PST.[3SG]</ta>
            <ta e="T395" id="Seg_8282" s="T394">fish.[NOM.SG]</ta>
            <ta e="T396" id="Seg_8283" s="T395">take-CVB</ta>
            <ta e="T397" id="Seg_8284" s="T396">go-PST.[3SG]</ta>
            <ta e="T398" id="Seg_8285" s="T397">NEG</ta>
            <ta e="T399" id="Seg_8286" s="T398">know-PRS-1SG</ta>
            <ta e="T400" id="Seg_8287" s="T399">bring-PST.[3SG]</ta>
            <ta e="T401" id="Seg_8288" s="T400">whether</ta>
            <ta e="T402" id="Seg_8289" s="T401">NEG</ta>
            <ta e="T403" id="Seg_8290" s="T402">bring-PST.[3SG]</ta>
            <ta e="T404" id="Seg_8291" s="T403">bring-PST.[3SG]</ta>
            <ta e="T405" id="Seg_8292" s="T404">so</ta>
            <ta e="T406" id="Seg_8293" s="T405">bake-INF.LAT</ta>
            <ta e="T407" id="Seg_8294" s="T406">one.should</ta>
            <ta e="T408" id="Seg_8295" s="T407">this</ta>
            <ta e="T409" id="Seg_8296" s="T408">woman.[NOM.SG]</ta>
            <ta e="T410" id="Seg_8297" s="T409">PTCL</ta>
            <ta e="T411" id="Seg_8298" s="T410">pregnant.[NOM.SG]</ta>
            <ta e="T412" id="Seg_8299" s="T411">soon</ta>
            <ta e="T413" id="Seg_8300" s="T412">%%</ta>
            <ta e="T414" id="Seg_8301" s="T413">child.[NOM.SG]</ta>
            <ta e="T415" id="Seg_8302" s="T414">bring-FUT-3SG</ta>
            <ta e="T416" id="Seg_8303" s="T415">one.should</ta>
            <ta e="T417" id="Seg_8304" s="T416">this-ACC</ta>
            <ta e="T418" id="Seg_8305" s="T417">frighten-MOM-INF.LAT</ta>
            <ta e="T419" id="Seg_8306" s="T418">go-INF.LAT</ta>
            <ta e="T420" id="Seg_8307" s="T419">one.should</ta>
            <ta e="T421" id="Seg_8308" s="T420">what=INDEF</ta>
            <ta e="T422" id="Seg_8309" s="T421">sell-CVB</ta>
            <ta e="T423" id="Seg_8310" s="T422">take-INF.LAT</ta>
            <ta e="T424" id="Seg_8311" s="T423">when</ta>
            <ta e="T426" id="Seg_8312" s="T424">day.[NOM.SG]</ta>
            <ta e="T428" id="Seg_8313" s="T427">when</ta>
            <ta e="T429" id="Seg_8314" s="T428">day.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8315" s="T429">come-DUR.[3SG]</ta>
            <ta e="T431" id="Seg_8316" s="T430">and</ta>
            <ta e="T432" id="Seg_8317" s="T431">I.NOM</ta>
            <ta e="T433" id="Seg_8318" s="T432">get.up-DUR-1SG</ta>
            <ta e="T434" id="Seg_8319" s="T433">when</ta>
            <ta e="T435" id="Seg_8320" s="T434">day.[NOM.SG]</ta>
            <ta e="T436" id="Seg_8321" s="T435">walk-PRS.[3SG]</ta>
            <ta e="T437" id="Seg_8322" s="T436">I.NOM</ta>
            <ta e="T438" id="Seg_8323" s="T437">lie-FUT-1SG</ta>
            <ta e="T439" id="Seg_8324" s="T438">this</ta>
            <ta e="T440" id="Seg_8325" s="T439">winter.[NOM.SG]</ta>
            <ta e="T441" id="Seg_8326" s="T440">very</ta>
            <ta e="T442" id="Seg_8327" s="T441">cold.[NOM.SG]</ta>
            <ta e="T443" id="Seg_8328" s="T442">be-PST.[3SG]</ta>
            <ta e="T444" id="Seg_8329" s="T443">very</ta>
            <ta e="T445" id="Seg_8330" s="T444">long.[NOM.SG]</ta>
            <ta e="T446" id="Seg_8331" s="T445">be-PST.[3SG]</ta>
            <ta e="T447" id="Seg_8332" s="T446">evening-PL</ta>
            <ta e="T448" id="Seg_8333" s="T447">PTCL</ta>
            <ta e="T449" id="Seg_8334" s="T448">very</ta>
            <ta e="T453" id="Seg_8335" s="T452">sweat-NOM/GEN/ACC.3PL</ta>
            <ta e="T454" id="Seg_8336" s="T453">and</ta>
            <ta e="T455" id="Seg_8337" s="T454">this.[NOM.SG]</ta>
            <ta e="T456" id="Seg_8338" s="T455">year.[NOM.SG]</ta>
            <ta e="T457" id="Seg_8339" s="T456">come-PST.[3SG]</ta>
            <ta e="T458" id="Seg_8340" s="T457">so</ta>
            <ta e="T459" id="Seg_8341" s="T458">very</ta>
            <ta e="T460" id="Seg_8342" s="T459">warm.[NOM.SG]</ta>
            <ta e="T461" id="Seg_8343" s="T460">day-PL</ta>
            <ta e="T462" id="Seg_8344" s="T461">PTCL</ta>
            <ta e="T463" id="Seg_8345" s="T462">long-PL</ta>
            <ta e="T464" id="Seg_8346" s="T463">one.should</ta>
            <ta e="T465" id="Seg_8347" s="T464">I.LAT</ta>
            <ta e="T466" id="Seg_8348" s="T465">water.[NOM.SG]</ta>
            <ta e="T467" id="Seg_8349" s="T466">stove-LAT</ta>
            <ta e="T468" id="Seg_8350" s="T467">place-INF.LAT</ta>
            <ta e="T469" id="Seg_8351" s="T468">otherwise</ta>
            <ta e="T470" id="Seg_8352" s="T469">calf</ta>
            <ta e="T471" id="Seg_8353" s="T470">come-FUT-3SG</ta>
            <ta e="T472" id="Seg_8354" s="T471">water.[NOM.SG]</ta>
            <ta e="T473" id="Seg_8355" s="T472">NEG.EX.[3SG]</ta>
            <ta e="T474" id="Seg_8356" s="T473">and</ta>
            <ta e="T475" id="Seg_8357" s="T474">this.[NOM.SG]</ta>
            <ta e="T476" id="Seg_8358" s="T475">cold.[NOM.SG]</ta>
            <ta e="T477" id="Seg_8359" s="T476">water.[NOM.SG]</ta>
            <ta e="T478" id="Seg_8360" s="T477">NEG</ta>
            <ta e="T479" id="Seg_8361" s="T478">drink-PRS.[3SG]</ta>
            <ta e="T482" id="Seg_8362" s="T481">place-IMP.2SG</ta>
            <ta e="T483" id="Seg_8363" s="T482">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T484" id="Seg_8364" s="T483">horse.[NOM.SG]</ta>
            <ta e="T485" id="Seg_8365" s="T484">this.[NOM.SG]</ta>
            <ta e="T486" id="Seg_8366" s="T485">probably</ta>
            <ta e="T487" id="Seg_8367" s="T486">NEG</ta>
            <ta e="T488" id="Seg_8368" s="T487">stand-PRS.[3SG]</ta>
            <ta e="T489" id="Seg_8369" s="T488">one.should</ta>
            <ta e="T490" id="Seg_8370" s="T489">this-ACC</ta>
            <ta e="T491" id="Seg_8371" s="T490">place-INF.LAT</ta>
            <ta e="T492" id="Seg_8372" s="T491">boy.[NOM.SG]</ta>
            <ta e="T493" id="Seg_8373" s="T492">daughter-INS</ta>
            <ta e="T494" id="Seg_8374" s="T493">PTCL</ta>
            <ta e="T495" id="Seg_8375" s="T494">grow-PST-3PL</ta>
            <ta e="T496" id="Seg_8376" s="T495">then</ta>
            <ta e="T497" id="Seg_8377" s="T496">boy.[NOM.SG]</ta>
            <ta e="T498" id="Seg_8378" s="T497">say-PST.[3SG]</ta>
            <ta e="T500" id="Seg_8379" s="T499">I.LAT</ta>
            <ta e="T501" id="Seg_8380" s="T500">go-FUT-2SG</ta>
            <ta e="T502" id="Seg_8381" s="T501">man-LAT</ta>
            <ta e="T503" id="Seg_8382" s="T502">go-FUT-1SG</ta>
            <ta e="T504" id="Seg_8383" s="T503">well</ta>
            <ta e="T505" id="Seg_8384" s="T504">I.NOM</ta>
            <ta e="T506" id="Seg_8385" s="T505">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T507" id="Seg_8386" s="T506">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T508" id="Seg_8387" s="T507">send-FUT-1SG</ta>
            <ta e="T509" id="Seg_8388" s="T508">you.DAT</ta>
            <ta e="T510" id="Seg_8389" s="T509">then</ta>
            <ta e="T511" id="Seg_8390" s="T510">mother-NOM/GEN.3SG</ta>
            <ta e="T512" id="Seg_8391" s="T511">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T513" id="Seg_8392" s="T512">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T514" id="Seg_8393" s="T513">send-PST.[3SG]</ta>
            <ta e="T515" id="Seg_8394" s="T514">this-PL</ta>
            <ta e="T516" id="Seg_8395" s="T515">come-PST-3PL</ta>
            <ta e="T517" id="Seg_8396" s="T516">daughter-GEN</ta>
            <ta e="T518" id="Seg_8397" s="T517">mother-INS</ta>
            <ta e="T519" id="Seg_8398" s="T518">father-INS</ta>
            <ta e="T520" id="Seg_8399" s="T519">speak-MOM-PST-3PL</ta>
            <ta e="T521" id="Seg_8400" s="T520">daughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T523" id="Seg_8401" s="T522">give-PST-2PL</ta>
            <ta e="T524" id="Seg_8402" s="T523">I.NOM</ta>
            <ta e="T526" id="Seg_8403" s="T525">I.NOM</ta>
            <ta e="T527" id="Seg_8404" s="T526">boy-LAT</ta>
            <ta e="T529" id="Seg_8405" s="T528">give-PST-1PL</ta>
            <ta e="T531" id="Seg_8406" s="T530">then</ta>
            <ta e="T532" id="Seg_8407" s="T531">daughter-ACC</ta>
            <ta e="T533" id="Seg_8408" s="T532">ask-PST-3PL</ta>
            <ta e="T534" id="Seg_8409" s="T533">you.NOM</ta>
            <ta e="T535" id="Seg_8410" s="T534">go-FUT-2SG</ta>
            <ta e="T536" id="Seg_8411" s="T535">we.NOM</ta>
            <ta e="T537" id="Seg_8412" s="T536">boy-LAT</ta>
            <ta e="T538" id="Seg_8413" s="T537">man-LAT</ta>
            <ta e="T539" id="Seg_8414" s="T538">go-FUT-1SG</ta>
            <ta e="T540" id="Seg_8415" s="T539">then</ta>
            <ta e="T541" id="Seg_8416" s="T540">marry-PST-3PL</ta>
            <ta e="T542" id="Seg_8417" s="T541">vodka.[NOM.SG]</ta>
            <ta e="T543" id="Seg_8418" s="T542">bring-PST-3PL</ta>
            <ta e="T544" id="Seg_8419" s="T543">bread.[NOM.SG]</ta>
            <ta e="T545" id="Seg_8420" s="T544">bring-PST-3PL</ta>
            <ta e="T546" id="Seg_8421" s="T545">meat.[NOM.SG]</ta>
            <ta e="T547" id="Seg_8422" s="T546">many</ta>
            <ta e="T548" id="Seg_8423" s="T547">be-PST.[3SG]</ta>
            <ta e="T549" id="Seg_8424" s="T548">then</ta>
            <ta e="T550" id="Seg_8425" s="T549">people.[NOM.SG]</ta>
            <ta e="T551" id="Seg_8426" s="T550">come-PST-3PL</ta>
            <ta e="T552" id="Seg_8427" s="T551">vodka.[NOM.SG]</ta>
            <ta e="T553" id="Seg_8428" s="T552">drink-PST-3PL</ta>
            <ta e="T554" id="Seg_8429" s="T553">PTCL</ta>
            <ta e="T555" id="Seg_8430" s="T554">then</ta>
            <ta e="T558" id="Seg_8431" s="T557">day.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8432" s="T558">go-PST.[3SG]</ta>
            <ta e="T560" id="Seg_8433" s="T559">this-PL</ta>
            <ta e="T561" id="Seg_8434" s="T560">flower-PL</ta>
            <ta e="T562" id="Seg_8435" s="T561">wear-PST-3PL</ta>
            <ta e="T563" id="Seg_8436" s="T562">and</ta>
            <ta e="T564" id="Seg_8437" s="T563">bell.tower-LAT</ta>
            <ta e="T565" id="Seg_8438" s="T564">go-PST-3PL</ta>
            <ta e="T566" id="Seg_8439" s="T565">there</ta>
            <ta e="T567" id="Seg_8440" s="T566">priest.[NOM.SG]</ta>
            <ta e="T568" id="Seg_8441" s="T567">this-ACC.PL</ta>
            <ta e="T569" id="Seg_8442" s="T568">God.[NOM.SG]</ta>
            <ta e="T570" id="Seg_8443" s="T569">bow-PST.[3SG]</ta>
            <ta e="T571" id="Seg_8444" s="T570">then</ta>
            <ta e="T574" id="Seg_8445" s="T573">wreath-PL</ta>
            <ta e="T575" id="Seg_8446" s="T574">dress-PST-3PL</ta>
            <ta e="T576" id="Seg_8447" s="T575">then</ta>
            <ta e="T577" id="Seg_8448" s="T576">tent-LAT/LOC.3SG</ta>
            <ta e="T578" id="Seg_8449" s="T577">come-PST-3PL</ta>
            <ta e="T579" id="Seg_8450" s="T578">also</ta>
            <ta e="T580" id="Seg_8451" s="T579">PTCL</ta>
            <ta e="T581" id="Seg_8452" s="T580">vodka.[NOM.SG]</ta>
            <ta e="T582" id="Seg_8453" s="T581">drink-PST-3PL</ta>
            <ta e="T583" id="Seg_8454" s="T582">daughter-GEN</ta>
            <ta e="T584" id="Seg_8455" s="T583">relative-LAT</ta>
            <ta e="T585" id="Seg_8456" s="T584">come-PST-3PL</ta>
            <ta e="T586" id="Seg_8457" s="T585">and</ta>
            <ta e="T587" id="Seg_8458" s="T586">boy-GEN</ta>
            <ta e="T588" id="Seg_8459" s="T587">girl.[NOM.SG]</ta>
            <ta e="T589" id="Seg_8460" s="T588">come-PST-3PL</ta>
            <ta e="T590" id="Seg_8461" s="T589">many</ta>
            <ta e="T591" id="Seg_8462" s="T590">people.[NOM.SG]</ta>
            <ta e="T592" id="Seg_8463" s="T591">be-PST-3PL</ta>
            <ta e="T593" id="Seg_8464" s="T592">vodka.[NOM.SG]</ta>
            <ta e="T594" id="Seg_8465" s="T593">many</ta>
            <ta e="T595" id="Seg_8466" s="T594">be-PST.[3SG]</ta>
            <ta e="T596" id="Seg_8467" s="T595">table-LOC</ta>
            <ta e="T597" id="Seg_8468" s="T596">very</ta>
            <ta e="T598" id="Seg_8469" s="T597">many</ta>
            <ta e="T599" id="Seg_8470" s="T598">what.[NOM.SG]</ta>
            <ta e="T601" id="Seg_8471" s="T600">lie-DUR.[3SG]</ta>
            <ta e="T602" id="Seg_8472" s="T601">when</ta>
            <ta e="T603" id="Seg_8473" s="T602">girl.[NOM.SG]</ta>
            <ta e="T604" id="Seg_8474" s="T603">marry-PST-3PL</ta>
            <ta e="T605" id="Seg_8475" s="T604">mother-NOM/GEN.3SG</ta>
            <ta e="T606" id="Seg_8476" s="T605">father-NOM/GEN.3SG</ta>
            <ta e="T607" id="Seg_8477" s="T606">say-PST-3PL</ta>
            <ta e="T608" id="Seg_8478" s="T607">this-GEN</ta>
            <ta e="T609" id="Seg_8479" s="T608">clothing-NOM/GEN.3SG</ta>
            <ta e="T610" id="Seg_8480" s="T609">NEG.EX.[3SG]</ta>
            <ta e="T611" id="Seg_8481" s="T610">one.should</ta>
            <ta e="T612" id="Seg_8482" s="T611">shoe-NOM/GEN/ACC.3PL</ta>
            <ta e="T613" id="Seg_8483" s="T612">take-INF.LAT</ta>
            <ta e="T614" id="Seg_8484" s="T613">one.should</ta>
            <ta e="T615" id="Seg_8485" s="T614">coat.[NOM.SG]</ta>
            <ta e="T616" id="Seg_8486" s="T615">take-INF.LAT</ta>
            <ta e="T617" id="Seg_8487" s="T616">scarf.[NOM.SG]</ta>
            <ta e="T618" id="Seg_8488" s="T617">take-INF.LAT</ta>
            <ta e="T619" id="Seg_8489" s="T618">clothing-NOM/GEN.3SG</ta>
            <ta e="T620" id="Seg_8490" s="T619">few</ta>
            <ta e="T621" id="Seg_8491" s="T620">shirt-NOM/GEN/ACC.3SG</ta>
            <ta e="T622" id="Seg_8492" s="T621">NEG.EX.[3SG]</ta>
            <ta e="T623" id="Seg_8493" s="T622">then</ta>
            <ta e="T625" id="Seg_8494" s="T624">this-PL</ta>
            <ta e="T626" id="Seg_8495" s="T625">money-ACC</ta>
            <ta e="T627" id="Seg_8496" s="T626">take-PST-3PL</ta>
            <ta e="T628" id="Seg_8497" s="T627">two.[NOM.SG]</ta>
            <ta e="T629" id="Seg_8498" s="T628">ten.[NOM.SG]</ta>
            <ta e="T630" id="Seg_8499" s="T629">five.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8500" s="T630">then</ta>
            <ta e="T632" id="Seg_8501" s="T631">mother-NOM/GEN.3SG</ta>
            <ta e="T633" id="Seg_8502" s="T632">father-GEN</ta>
            <ta e="T634" id="Seg_8503" s="T633">this-LAT</ta>
            <ta e="T635" id="Seg_8504" s="T634">give-PST-3PL</ta>
            <ta e="T636" id="Seg_8505" s="T635">cow.[NOM.SG]</ta>
            <ta e="T637" id="Seg_8506" s="T636">relative-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T638" id="Seg_8507" s="T637">give-PST-3PL</ta>
            <ta e="T639" id="Seg_8508" s="T638">sheep.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8509" s="T639">give-PST-3PL</ta>
            <ta e="T641" id="Seg_8510" s="T640">horse-PL</ta>
            <ta e="T642" id="Seg_8511" s="T641">money.[NOM.SG]</ta>
            <ta e="T643" id="Seg_8512" s="T642">give-PST-3PL</ta>
            <ta e="T644" id="Seg_8513" s="T643">very</ta>
            <ta e="T645" id="Seg_8514" s="T644">many</ta>
            <ta e="T647" id="Seg_8515" s="T646">people.[NOM.SG]</ta>
            <ta e="T648" id="Seg_8516" s="T647">many</ta>
            <ta e="T649" id="Seg_8517" s="T648">be-PST-3PL</ta>
            <ta e="T650" id="Seg_8518" s="T649">then</ta>
            <ta e="T651" id="Seg_8519" s="T650">this-PL</ta>
            <ta e="T652" id="Seg_8520" s="T651">long.time</ta>
            <ta e="T653" id="Seg_8521" s="T652">live-PST-3PL</ta>
            <ta e="T654" id="Seg_8522" s="T653">brother-NOM/GEN.3SG</ta>
            <ta e="T657" id="Seg_8523" s="T656">and</ta>
            <ta e="T658" id="Seg_8524" s="T657">again</ta>
            <ta e="T659" id="Seg_8525" s="T658">so</ta>
            <ta e="T660" id="Seg_8526" s="T659">marry-PST.[3SG]</ta>
            <ta e="T661" id="Seg_8527" s="T660">then</ta>
            <ta e="T662" id="Seg_8528" s="T661">this.[NOM.SG]</ta>
            <ta e="T969" id="Seg_8529" s="T662">go-CVB</ta>
            <ta e="T663" id="Seg_8530" s="T969">disappear-PST.[3SG]</ta>
            <ta e="T664" id="Seg_8531" s="T663">this-LAT</ta>
            <ta e="T665" id="Seg_8532" s="T664">father-NOM/GEN.3SG</ta>
            <ta e="T666" id="Seg_8533" s="T665">give-PST.[3SG]</ta>
            <ta e="T667" id="Seg_8534" s="T666">all</ta>
            <ta e="T668" id="Seg_8535" s="T667">all</ta>
            <ta e="T669" id="Seg_8536" s="T668">what.[NOM.SG]</ta>
            <ta e="T670" id="Seg_8537" s="T669">give-PST.[3SG]</ta>
            <ta e="T671" id="Seg_8538" s="T670">horse.[NOM.SG]</ta>
            <ta e="T672" id="Seg_8539" s="T671">give-PST.[3SG]</ta>
            <ta e="T673" id="Seg_8540" s="T672">sheep.[NOM.SG]</ta>
            <ta e="T674" id="Seg_8541" s="T673">give-PST.[3SG]</ta>
            <ta e="T675" id="Seg_8542" s="T674">cow-PL</ta>
            <ta e="T676" id="Seg_8543" s="T675">give-PST.[3SG]</ta>
            <ta e="T677" id="Seg_8544" s="T676">bread.[NOM.SG]</ta>
            <ta e="T678" id="Seg_8545" s="T677">give-PST.[3SG]</ta>
            <ta e="T680" id="Seg_8546" s="T679">house.[NOM.SG]</ta>
            <ta e="T681" id="Seg_8547" s="T680">place-PST.[3SG]</ta>
            <ta e="T682" id="Seg_8548" s="T681">this-PL</ta>
            <ta e="T685" id="Seg_8549" s="T684">go-PST-3PL</ta>
            <ta e="T687" id="Seg_8550" s="T686">horse-3PL-INS</ta>
            <ta e="T688" id="Seg_8551" s="T687">girl.[NOM.SG]</ta>
            <ta e="T689" id="Seg_8552" s="T688">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T690" id="Seg_8553" s="T689">Russian</ta>
            <ta e="T691" id="Seg_8554" s="T690">mother-NOM/GEN.3SG</ta>
            <ta e="T692" id="Seg_8555" s="T691">and</ta>
            <ta e="T693" id="Seg_8556" s="T692">boy.[NOM.SG]</ta>
            <ta e="T694" id="Seg_8557" s="T693">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_8558" s="T694">Russian</ta>
            <ta e="T696" id="Seg_8559" s="T695">mother-NOM/GEN.3SG</ta>
            <ta e="T697" id="Seg_8560" s="T696">Russian</ta>
            <ta e="T698" id="Seg_8561" s="T697">father-NOM/GEN.3SG</ta>
            <ta e="T699" id="Seg_8562" s="T698">there</ta>
            <ta e="T700" id="Seg_8563" s="T699">go-PST-3PL</ta>
            <ta e="T701" id="Seg_8564" s="T700">then</ta>
            <ta e="T702" id="Seg_8565" s="T701">come-PST-3PL</ta>
            <ta e="T703" id="Seg_8566" s="T702">tent-LAT/LOC.3SG</ta>
            <ta e="T704" id="Seg_8567" s="T703">and</ta>
            <ta e="T705" id="Seg_8568" s="T704">then</ta>
            <ta e="T706" id="Seg_8569" s="T705">daughter-GEN</ta>
            <ta e="T707" id="Seg_8570" s="T706">Russian</ta>
            <ta e="T708" id="Seg_8571" s="T707">father-NOM/GEN.3SG</ta>
            <ta e="T710" id="Seg_8572" s="T709">clothing.[NOM.SG]</ta>
            <ta e="T711" id="Seg_8573" s="T710">bring-PST.[3SG]</ta>
            <ta e="T713" id="Seg_8574" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_8575" s="T713">pillow-NOM/GEN/ACC.3PL</ta>
            <ta e="T715" id="Seg_8576" s="T714">bring-PST.[3SG]</ta>
            <ta e="T716" id="Seg_8577" s="T715">PTCL</ta>
            <ta e="T717" id="Seg_8578" s="T716">feather.bed.[NOM.SG]</ta>
            <ta e="T718" id="Seg_8579" s="T717">bring-PST.[3SG]</ta>
            <ta e="T719" id="Seg_8580" s="T718">then</ta>
            <ta e="T720" id="Seg_8581" s="T719">come-PST-3PL</ta>
            <ta e="T721" id="Seg_8582" s="T720">bell.tower-LAT</ta>
            <ta e="T722" id="Seg_8583" s="T721">this-PL</ta>
            <ta e="T723" id="Seg_8584" s="T722">sit.down-PST-3PL</ta>
            <ta e="T725" id="Seg_8585" s="T724">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T727" id="Seg_8586" s="T725">then</ta>
            <ta e="T728" id="Seg_8587" s="T727">boy-GEN</ta>
            <ta e="T729" id="Seg_8588" s="T728">shirt-NOM/GEN/ACC.3SG</ta>
            <ta e="T730" id="Seg_8589" s="T729">beautiful.[NOM.SG]</ta>
            <ta e="T731" id="Seg_8590" s="T730">new.[NOM.SG]</ta>
            <ta e="T732" id="Seg_8591" s="T731">daughter-GEN</ta>
            <ta e="T733" id="Seg_8592" s="T732">clothing-NOM/GEN.3SG</ta>
            <ta e="T734" id="Seg_8593" s="T733">also</ta>
            <ta e="T735" id="Seg_8594" s="T734">beautiful.[NOM.SG]</ta>
            <ta e="T736" id="Seg_8595" s="T735">new.[NOM.SG]</ta>
            <ta e="T737" id="Seg_8596" s="T736">head-NOM/GEN.3SG</ta>
            <ta e="T738" id="Seg_8597" s="T737">PTCL</ta>
            <ta e="T739" id="Seg_8598" s="T738">beautiful.[NOM.SG]</ta>
            <ta e="T740" id="Seg_8599" s="T739">wreath.[NOM.SG]</ta>
            <ta e="T741" id="Seg_8600" s="T740">dress-PST-3PL</ta>
            <ta e="T742" id="Seg_8601" s="T741">then</ta>
            <ta e="T743" id="Seg_8602" s="T742">this-LAT</ta>
            <ta e="T747" id="Seg_8603" s="T746">then</ta>
            <ta e="T748" id="Seg_8604" s="T747">this-LAT</ta>
            <ta e="T749" id="Seg_8605" s="T748">two.[NOM.SG]</ta>
            <ta e="T750" id="Seg_8606" s="T749">braid.[NOM.SG]</ta>
            <ta e="T751" id="Seg_8607" s="T750">plait-PST-3PL</ta>
            <ta e="T752" id="Seg_8608" s="T751">then</ta>
            <ta e="T753" id="Seg_8609" s="T752">bind-PST-3PL</ta>
            <ta e="T754" id="Seg_8610" s="T753">head-NOM/GEN.3SG</ta>
            <ta e="T755" id="Seg_8611" s="T754">when</ta>
            <ta e="T756" id="Seg_8612" s="T755">this.[NOM.SG]</ta>
            <ta e="T757" id="Seg_8613" s="T756">girl.[NOM.SG]</ta>
            <ta e="T758" id="Seg_8614" s="T757">be-PST.[3SG]</ta>
            <ta e="T759" id="Seg_8615" s="T758">one.[NOM.SG]</ta>
            <ta e="T761" id="Seg_8616" s="T760">braid.[NOM.SG]</ta>
            <ta e="T762" id="Seg_8617" s="T761">be-PST.[3SG]</ta>
            <ta e="T763" id="Seg_8618" s="T762">then</ta>
            <ta e="T765" id="Seg_8619" s="T764">Russian</ta>
            <ta e="T766" id="Seg_8620" s="T765">mother-NOM/GEN.3SG</ta>
            <ta e="T767" id="Seg_8621" s="T766">this.[NOM.SG]</ta>
            <ta e="T768" id="Seg_8622" s="T767">daughter-GEN</ta>
            <ta e="T769" id="Seg_8623" s="T768">Russian</ta>
            <ta e="T770" id="Seg_8624" s="T769">mother-NOM/GEN.3SG</ta>
            <ta e="T771" id="Seg_8625" s="T770">and</ta>
            <ta e="T772" id="Seg_8626" s="T771">boy-GEN</ta>
            <ta e="T773" id="Seg_8627" s="T772">Russian</ta>
            <ta e="T774" id="Seg_8628" s="T773">mother-NOM/GEN.3SG</ta>
            <ta e="T775" id="Seg_8629" s="T774">this-LAT</ta>
            <ta e="T776" id="Seg_8630" s="T775">two.[NOM.SG]</ta>
            <ta e="T777" id="Seg_8631" s="T776">plait-PST-3PL</ta>
            <ta e="T778" id="Seg_8632" s="T777">and</ta>
            <ta e="T779" id="Seg_8633" s="T778">bind-PST-3PL</ta>
            <ta e="T780" id="Seg_8634" s="T779">head-NOM/GEN.3SG</ta>
            <ta e="T781" id="Seg_8635" s="T780">this-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T782" id="Seg_8636" s="T781">boy.[NOM.SG]</ta>
            <ta e="T783" id="Seg_8637" s="T782">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T784" id="Seg_8638" s="T783">Russian</ta>
            <ta e="T785" id="Seg_8639" s="T784">father-NOM/GEN.3SG</ta>
            <ta e="T787" id="Seg_8640" s="T786">bring-PST.[3SG]</ta>
            <ta e="T788" id="Seg_8641" s="T787">house-LAT</ta>
            <ta e="T789" id="Seg_8642" s="T788">there</ta>
            <ta e="T790" id="Seg_8643" s="T789">who.[NOM.SG]=INDEF</ta>
            <ta e="T791" id="Seg_8644" s="T790">NEG</ta>
            <ta e="T792" id="Seg_8645" s="T791">be-PST.[3SG]</ta>
            <ta e="T793" id="Seg_8646" s="T792">lie-PST.[3SG]</ta>
            <ta e="T795" id="Seg_8647" s="T794">sleep-INF.LAT</ta>
            <ta e="T796" id="Seg_8648" s="T795">and</ta>
            <ta e="T797" id="Seg_8649" s="T796">%%-MOM-PST-3PL</ta>
            <ta e="T799" id="Seg_8650" s="T798">relative-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T800" id="Seg_8651" s="T799">this-PL-LAT</ta>
            <ta e="T801" id="Seg_8652" s="T800">say-PST-3PL</ta>
            <ta e="T802" id="Seg_8653" s="T801">good.[NOM.SG]</ta>
            <ta e="T803" id="Seg_8654" s="T802">live-NOM/GEN/ACC.2PL</ta>
            <ta e="T804" id="Seg_8655" s="T803">live-IMP.2PL</ta>
            <ta e="T805" id="Seg_8656" s="T804">NEG.AUX-IMP.2SG</ta>
            <ta e="T806" id="Seg_8657" s="T805">fight-CNG</ta>
            <ta e="T807" id="Seg_8658" s="T806">NEG.AUX-IMP.2SG</ta>
            <ta e="T809" id="Seg_8659" s="T808">NEG.AUX-IMP.2SG</ta>
            <ta e="T811" id="Seg_8660" s="T810">NEG.AUX-IMP.2SG</ta>
            <ta e="T812" id="Seg_8661" s="T811">scold-DES-CNG</ta>
            <ta e="T813" id="Seg_8662" s="T812">JUSS</ta>
            <ta e="T815" id="Seg_8663" s="T814">child-PL-LAT</ta>
            <ta e="T816" id="Seg_8664" s="T815">many</ta>
            <ta e="T817" id="Seg_8665" s="T816">become-FUT-3SG</ta>
            <ta e="T818" id="Seg_8666" s="T817">boy-PL</ta>
            <ta e="T819" id="Seg_8667" s="T818">daughter-PL</ta>
            <ta e="T820" id="Seg_8668" s="T819">then</ta>
            <ta e="T821" id="Seg_8669" s="T820">this-PL</ta>
            <ta e="T822" id="Seg_8670" s="T821">priest.[NOM.SG]</ta>
            <ta e="T823" id="Seg_8671" s="T822">call-PST-3PL</ta>
            <ta e="T824" id="Seg_8672" s="T823">this.[NOM.SG]</ta>
            <ta e="T825" id="Seg_8673" s="T824">very</ta>
            <ta e="T826" id="Seg_8674" s="T825">many</ta>
            <ta e="T827" id="Seg_8675" s="T826">vodka.[NOM.SG]</ta>
            <ta e="T828" id="Seg_8676" s="T827">drink-PST.[3SG]</ta>
            <ta e="T829" id="Seg_8677" s="T828">and</ta>
            <ta e="T830" id="Seg_8678" s="T829">fall-MOM-PST.[3SG]</ta>
            <ta e="T831" id="Seg_8679" s="T830">this-ACC</ta>
            <ta e="T832" id="Seg_8680" s="T831">bring-RES-PST-3PL</ta>
            <ta e="T833" id="Seg_8681" s="T832">this-PL</ta>
            <ta e="T834" id="Seg_8682" s="T833">very</ta>
            <ta e="T835" id="Seg_8683" s="T834">vodka.[NOM.SG]</ta>
            <ta e="T836" id="Seg_8684" s="T835">drink-PST-3PL</ta>
            <ta e="T837" id="Seg_8685" s="T836">long.time</ta>
            <ta e="T838" id="Seg_8686" s="T837">six.[NOM.SG]</ta>
            <ta e="T839" id="Seg_8687" s="T838">day.[NOM.SG]</ta>
            <ta e="T840" id="Seg_8688" s="T839">drink-PST-3PL</ta>
            <ta e="T841" id="Seg_8689" s="T840">then</ta>
            <ta e="T842" id="Seg_8690" s="T841">this-PL</ta>
            <ta e="T843" id="Seg_8691" s="T842">PTCL</ta>
            <ta e="T844" id="Seg_8692" s="T843">live-PST-3PL</ta>
            <ta e="T845" id="Seg_8693" s="T844">this-PL</ta>
            <ta e="T846" id="Seg_8694" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_8695" s="T846">child-PL-LAT</ta>
            <ta e="T848" id="Seg_8696" s="T847">be-PST-3PL</ta>
            <ta e="T849" id="Seg_8697" s="T848">boy-PL</ta>
            <ta e="T850" id="Seg_8698" s="T849">be-PST-3PL</ta>
            <ta e="T852" id="Seg_8699" s="T851">be-PST-3PL</ta>
            <ta e="T853" id="Seg_8700" s="T852">daughter-PL</ta>
            <ta e="T857" id="Seg_8701" s="T855">I.NOM</ta>
            <ta e="T858" id="Seg_8702" s="T857">son-NOM/GEN/ACC.1SG</ta>
            <ta e="T859" id="Seg_8703" s="T858">yesterday</ta>
            <ta e="T860" id="Seg_8704" s="T859">go-PST.[3SG]</ta>
            <ta e="T861" id="Seg_8705" s="T860">vodka.[NOM.SG]</ta>
            <ta e="T862" id="Seg_8706" s="T861">drink-PST.[3SG]</ta>
            <ta e="T863" id="Seg_8707" s="T862">very</ta>
            <ta e="T864" id="Seg_8708" s="T863">long.time</ta>
            <ta e="T865" id="Seg_8709" s="T864">NEG</ta>
            <ta e="T866" id="Seg_8710" s="T865">come-PST.[3SG]</ta>
            <ta e="T867" id="Seg_8711" s="T866">tent-LAT</ta>
            <ta e="T868" id="Seg_8712" s="T867">then</ta>
            <ta e="T869" id="Seg_8713" s="T868">come-PST.[3SG]</ta>
            <ta e="T870" id="Seg_8714" s="T869">sleep-MOM-PST.[3SG]</ta>
            <ta e="T871" id="Seg_8715" s="T870">and</ta>
            <ta e="T872" id="Seg_8716" s="T871">morning-GEN</ta>
            <ta e="T873" id="Seg_8717" s="T872">get.up-PST.[3SG]</ta>
            <ta e="T874" id="Seg_8718" s="T873">and</ta>
            <ta e="T875" id="Seg_8719" s="T874">become-MOM-PST.[3SG]</ta>
            <ta e="T876" id="Seg_8720" s="T875">work-INF.LAT</ta>
            <ta e="T877" id="Seg_8721" s="T876">and</ta>
            <ta e="T878" id="Seg_8722" s="T877">I.LAT</ta>
            <ta e="T880" id="Seg_8723" s="T879">I.NOM</ta>
            <ta e="T881" id="Seg_8724" s="T880">two.[NOM.SG]</ta>
            <ta e="T882" id="Seg_8725" s="T881">cow.[NOM.SG]</ta>
            <ta e="T883" id="Seg_8726" s="T882">milk-PST-1SG</ta>
            <ta e="T884" id="Seg_8727" s="T883">this-GEN</ta>
            <ta e="T885" id="Seg_8728" s="T884">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T886" id="Seg_8729" s="T885">and</ta>
            <ta e="T887" id="Seg_8730" s="T886">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T888" id="Seg_8731" s="T887">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T889" id="Seg_8732" s="T888">one.[NOM.SG]</ta>
            <ta e="T890" id="Seg_8733" s="T889">woman.[NOM.SG]</ta>
            <ta e="T891" id="Seg_8734" s="T890">pregnant.[NOM.SG]</ta>
            <ta e="T892" id="Seg_8735" s="T891">be-PST.[3SG]</ta>
            <ta e="T893" id="Seg_8736" s="T892">then</ta>
            <ta e="T894" id="Seg_8737" s="T893">gave.birth-PST.[3SG]</ta>
            <ta e="T895" id="Seg_8738" s="T894">and</ta>
            <ta e="T896" id="Seg_8739" s="T895">this.[NOM.SG]</ta>
            <ta e="T897" id="Seg_8740" s="T896">there</ta>
            <ta e="T898" id="Seg_8741" s="T897">go-PST.[3SG]</ta>
            <ta e="T899" id="Seg_8742" s="T898">and</ta>
            <ta e="T900" id="Seg_8743" s="T899">drink-PST.[3SG]</ta>
            <ta e="T901" id="Seg_8744" s="T900">vodka.[NOM.SG]</ta>
            <ta e="T902" id="Seg_8745" s="T901">I.NOM</ta>
            <ta e="T903" id="Seg_8746" s="T902">yesterday</ta>
            <ta e="T904" id="Seg_8747" s="T903">%%</ta>
            <ta e="T905" id="Seg_8748" s="T904">look-FRQ</ta>
            <ta e="T906" id="Seg_8749" s="T905">look-PST-1SG</ta>
            <ta e="T907" id="Seg_8750" s="T906">and</ta>
            <ta e="T908" id="Seg_8751" s="T907">today</ta>
            <ta e="T910" id="Seg_8752" s="T908">evening-LOC.ADV</ta>
            <ta e="T911" id="Seg_8753" s="T910">today</ta>
            <ta e="T913" id="Seg_8754" s="T911">evening-LOC.ADV</ta>
            <ta e="T914" id="Seg_8755" s="T913">then</ta>
            <ta e="T915" id="Seg_8756" s="T914">this-ACC</ta>
            <ta e="T916" id="Seg_8757" s="T915">%%</ta>
            <ta e="T917" id="Seg_8758" s="T916">see-PST-1SG</ta>
            <ta e="T918" id="Seg_8759" s="T917">what.[NOM.SG]=INDEF</ta>
            <ta e="T919" id="Seg_8760" s="T918">bring-PST.[3SG]</ta>
            <ta e="T920" id="Seg_8761" s="T919">and</ta>
            <ta e="T921" id="Seg_8762" s="T920">I.NOM</ta>
            <ta e="T922" id="Seg_8763" s="T921">get.angry-CVB-PST-1SG</ta>
            <ta e="T923" id="Seg_8764" s="T922">PTCL</ta>
            <ta e="T924" id="Seg_8765" s="T923">%%</ta>
            <ta e="T925" id="Seg_8766" s="T924">NEG</ta>
            <ta e="T926" id="Seg_8767" s="T925">come-PST-3PL</ta>
            <ta e="T927" id="Seg_8768" s="T926">forest-ABL</ta>
            <ta e="T928" id="Seg_8769" s="T927">when</ta>
            <ta e="T929" id="Seg_8770" s="T928">this-PL</ta>
            <ta e="T930" id="Seg_8771" s="T929">come-FUT-3PL</ta>
            <ta e="T931" id="Seg_8772" s="T930">today</ta>
            <ta e="T932" id="Seg_8773" s="T931">come-FUT-3PL</ta>
            <ta e="T933" id="Seg_8774" s="T932">evening-LAT/LOC.3SG</ta>
            <ta e="T934" id="Seg_8775" s="T933">eat-INF.LAT</ta>
            <ta e="T935" id="Seg_8776" s="T934">one.should</ta>
            <ta e="T936" id="Seg_8777" s="T935">this-PL-LAT</ta>
            <ta e="T937" id="Seg_8778" s="T936">then</ta>
            <ta e="T938" id="Seg_8779" s="T937">give-INF.LAT</ta>
            <ta e="T939" id="Seg_8780" s="T938">this-PL</ta>
            <ta e="T940" id="Seg_8781" s="T939">PTCL</ta>
            <ta e="T941" id="Seg_8782" s="T940">be.hungry-PRS-3PL</ta>
            <ta e="T942" id="Seg_8783" s="T941">I.NOM</ta>
            <ta e="T943" id="Seg_8784" s="T942">today</ta>
            <ta e="T944" id="Seg_8785" s="T943">think-PST-1SG</ta>
            <ta e="T945" id="Seg_8786" s="T944">you.PL.NOM</ta>
            <ta e="T946" id="Seg_8787" s="T945">PTCL</ta>
            <ta e="T947" id="Seg_8788" s="T946">come-PST-2PL</ta>
            <ta e="T948" id="Seg_8789" s="T947">and</ta>
            <ta e="T949" id="Seg_8790" s="T948">you.PL.NOM</ta>
            <ta e="T950" id="Seg_8791" s="T949">NEG</ta>
            <ta e="T951" id="Seg_8792" s="T950">come-PST-2PL</ta>
            <ta e="T952" id="Seg_8793" s="T951">I.NOM</ta>
            <ta e="T953" id="Seg_8794" s="T952">today</ta>
            <ta e="T955" id="Seg_8795" s="T954">today</ta>
            <ta e="T956" id="Seg_8796" s="T955">egg-PL</ta>
            <ta e="T957" id="Seg_8797" s="T956">boil-PST-1SG</ta>
            <ta e="T958" id="Seg_8798" s="T957">bear.leek.[NOM.SG]</ta>
            <ta e="T959" id="Seg_8799" s="T958">stab-PST-1SG</ta>
            <ta e="T960" id="Seg_8800" s="T959">egg-PL</ta>
            <ta e="T961" id="Seg_8801" s="T960">put-PST-1SG</ta>
            <ta e="T962" id="Seg_8802" s="T961">%%</ta>
            <ta e="T963" id="Seg_8803" s="T962">put-PST-1SG</ta>
            <ta e="T964" id="Seg_8804" s="T963">you.PL.ACC</ta>
            <ta e="T965" id="Seg_8805" s="T964">feed-INF.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T1" id="Seg_8806" s="T0">INCH</ta>
            <ta e="T2" id="Seg_8807" s="T1">рожать-INF.LAT</ta>
            <ta e="T4" id="Seg_8808" s="T3">один.[NOM.SG]</ta>
            <ta e="T5" id="Seg_8809" s="T4">маленький.[NOM.SG]</ta>
            <ta e="T6" id="Seg_8810" s="T5">%snake.[NOM.SG]</ta>
            <ta e="T8" id="Seg_8811" s="T7">шапка-LAT</ta>
            <ta e="T9" id="Seg_8812" s="T8">спуститься-PST.[3SG]</ta>
            <ta e="T10" id="Seg_8813" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_8814" s="T10">тогда</ta>
            <ta e="T12" id="Seg_8815" s="T11">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T13" id="Seg_8816" s="T12">PTCL</ta>
            <ta e="T14" id="Seg_8817" s="T13">этот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_8818" s="T14">этот.[NOM.SG]</ta>
            <ta e="T16" id="Seg_8819" s="T15">догонять-PST.[3SG]</ta>
            <ta e="T17" id="Seg_8820" s="T16">догонять-PST.[3SG]</ta>
            <ta e="T18" id="Seg_8821" s="T17">тогда</ta>
            <ta e="T19" id="Seg_8822" s="T18">этот.[NOM.SG]</ta>
            <ta e="T20" id="Seg_8823" s="T19">шапка-ABL.3SG</ta>
            <ta e="T21" id="Seg_8824" s="T20">упасть-MOM-PST.[3SG]</ta>
            <ta e="T22" id="Seg_8825" s="T21">тогда</ta>
            <ta e="T23" id="Seg_8826" s="T22">этот.[NOM.SG]</ta>
            <ta e="T24" id="Seg_8827" s="T23">NEG</ta>
            <ta e="T25" id="Seg_8828" s="T24">NEG</ta>
            <ta e="T26" id="Seg_8829" s="T25">пойти-PST.[3SG]</ta>
            <ta e="T27" id="Seg_8830" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_8831" s="T27">этот-LAT</ta>
            <ta e="T29" id="Seg_8832" s="T28">очень</ta>
            <ta e="T31" id="Seg_8833" s="T30">дать-PRS-1SG</ta>
            <ta e="T32" id="Seg_8834" s="T31">а</ta>
            <ta e="T33" id="Seg_8835" s="T32">там</ta>
            <ta e="T34" id="Seg_8836" s="T33">этот.[NOM.SG]</ta>
            <ta e="T35" id="Seg_8837" s="T34">я.LAT</ta>
            <ta e="T36" id="Seg_8838" s="T35">когда=INDEF</ta>
            <ta e="T37" id="Seg_8839" s="T36">NEG</ta>
            <ta e="T38" id="Seg_8840" s="T37">прийти-PRS.[3SG]</ta>
            <ta e="T39" id="Seg_8841" s="T38">я.NOM</ta>
            <ta e="T40" id="Seg_8842" s="T39">очень</ta>
            <ta e="T41" id="Seg_8843" s="T40">сильно</ta>
            <ta e="T42" id="Seg_8844" s="T41">смотреть-FRQ-PRS-1SG</ta>
            <ta e="T43" id="Seg_8845" s="T42">хороший.[NOM.SG]</ta>
            <ta e="T44" id="Seg_8846" s="T43">смотреть-FRQ-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_8847" s="T44">этот.[NOM.SG]</ta>
            <ta e="T46" id="Seg_8848" s="T45">мужчина-ACC</ta>
            <ta e="T47" id="Seg_8849" s="T46">как</ta>
            <ta e="T48" id="Seg_8850" s="T47">позвать-INF.LAT</ta>
            <ta e="T49" id="Seg_8851" s="T48">и</ta>
            <ta e="T50" id="Seg_8852" s="T49">как</ta>
            <ta e="T51" id="Seg_8853" s="T50">называть-INF.LAT</ta>
            <ta e="T52" id="Seg_8854" s="T51">вверх</ta>
            <ta e="T53" id="Seg_8855" s="T52">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T54" id="Seg_8856" s="T53">вверх</ta>
            <ta e="T55" id="Seg_8857" s="T54">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T56" id="Seg_8858" s="T55">гора-LAT</ta>
            <ta e="T57" id="Seg_8859" s="T56">надо</ta>
            <ta e="T58" id="Seg_8860" s="T57">веревка-ACC</ta>
            <ta e="T59" id="Seg_8861" s="T58">завязать-INF.LAT</ta>
            <ta e="T60" id="Seg_8862" s="T59">шапка-ACC</ta>
            <ta e="T61" id="Seg_8863" s="T60">надо</ta>
            <ta e="T62" id="Seg_8864" s="T61">надеть-INF.LAT</ta>
            <ta e="T63" id="Seg_8865" s="T62">парка-ACC</ta>
            <ta e="T64" id="Seg_8866" s="T63">надо</ta>
            <ta e="T65" id="Seg_8867" s="T64">надеть-INF.LAT</ta>
            <ta e="T66" id="Seg_8868" s="T65">тогда</ta>
            <ta e="T67" id="Seg_8869" s="T66">наружу</ta>
            <ta e="T68" id="Seg_8870" s="T67">пойти-INF.LAT</ta>
            <ta e="T69" id="Seg_8871" s="T68">я.NOM</ta>
            <ta e="T70" id="Seg_8872" s="T69">%чум-ACC.3SG</ta>
            <ta e="T71" id="Seg_8873" s="T70">пойти-PST-1SG</ta>
            <ta e="T72" id="Seg_8874" s="T71">%чум-LOC</ta>
            <ta e="T73" id="Seg_8875" s="T72">сидеть-PST-1SG</ta>
            <ta e="T74" id="Seg_8876" s="T73">тогда</ta>
            <ta e="T75" id="Seg_8877" s="T74">%%</ta>
            <ta e="T967" id="Seg_8878" s="T75">пойти-CVB</ta>
            <ta e="T76" id="Seg_8879" s="T967">исчезнуть-PST-1SG</ta>
            <ta e="T78" id="Seg_8880" s="T77">%чум-LAT/LOC.3SG</ta>
            <ta e="T79" id="Seg_8881" s="T78">я.NOM</ta>
            <ta e="T80" id="Seg_8882" s="T79">маленький.[NOM.SG]</ta>
            <ta e="T81" id="Seg_8883" s="T80">быть-PST-1SG</ta>
            <ta e="T82" id="Seg_8884" s="T81">лошадь.[NOM.SG]</ta>
            <ta e="T83" id="Seg_8885" s="T82">быть-PST.[3SG]</ta>
            <ta e="T85" id="Seg_8886" s="T84">мы.NOM</ta>
            <ta e="T86" id="Seg_8887" s="T85">два.[NOM.SG]</ta>
            <ta e="T87" id="Seg_8888" s="T86">два.[NOM.SG]</ta>
            <ta e="T90" id="Seg_8889" s="T89">зима.[NOM.SG]</ta>
            <ta e="T91" id="Seg_8890" s="T90">PTCL</ta>
            <ta e="T92" id="Seg_8891" s="T91">запрячь-DUR.PST-1PL</ta>
            <ta e="T93" id="Seg_8892" s="T92">оттуда</ta>
            <ta e="T94" id="Seg_8893" s="T93">идти-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_8894" s="T94">я.NOM</ta>
            <ta e="T96" id="Seg_8895" s="T95">пойти-FUT-1SG</ta>
            <ta e="T97" id="Seg_8896" s="T96">тогда</ta>
            <ta e="T98" id="Seg_8897" s="T97">этот.[NOM.SG]</ta>
            <ta e="T100" id="Seg_8898" s="T99">пойти-FUT-3SG</ta>
            <ta e="T101" id="Seg_8899" s="T100">я.NOM-INS</ta>
            <ta e="T102" id="Seg_8900" s="T101">влезать-PST-1SG</ta>
            <ta e="T103" id="Seg_8901" s="T102">%чум-LAT</ta>
            <ta e="T104" id="Seg_8902" s="T103">сидеть-PST-1SG</ta>
            <ta e="T105" id="Seg_8903" s="T104">%чум-NOM/GEN/ACC.3PL</ta>
            <ta e="T106" id="Seg_8904" s="T105">тогда</ta>
            <ta e="T108" id="Seg_8905" s="T107">спуститься-PST-1SG</ta>
            <ta e="T109" id="Seg_8906" s="T108">%чум-LAT</ta>
            <ta e="T110" id="Seg_8907" s="T109">дом.[NOM.SG]</ta>
            <ta e="T111" id="Seg_8908" s="T110">PTCL</ta>
            <ta e="T113" id="Seg_8909" s="T112">%гореть-MOM-PST.[3SG]</ta>
            <ta e="T114" id="Seg_8910" s="T113">люди.[NOM.SG]</ta>
            <ta e="T115" id="Seg_8911" s="T114">PTCL</ta>
            <ta e="T116" id="Seg_8912" s="T115">бежать-DUR-3PL</ta>
            <ta e="T117" id="Seg_8913" s="T116">вода.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8914" s="T117">принести-DUR-3PL</ta>
            <ta e="T119" id="Seg_8915" s="T118">огонь-LAT</ta>
            <ta e="T120" id="Seg_8916" s="T119">лить-DUR-3PL</ta>
            <ta e="T121" id="Seg_8917" s="T120">PTCL</ta>
            <ta e="T122" id="Seg_8918" s="T121">чтобы</ta>
            <ta e="T123" id="Seg_8919" s="T122">NEG</ta>
            <ta e="T124" id="Seg_8920" s="T123">%гореть-PST.[3SG]</ta>
            <ta e="T125" id="Seg_8921" s="T124">один.[NOM.SG]</ta>
            <ta e="T126" id="Seg_8922" s="T125">мужчина.[NOM.SG]</ta>
            <ta e="T127" id="Seg_8923" s="T126">один.[NOM.SG]</ta>
            <ta e="T128" id="Seg_8924" s="T127">мужчина-LAT</ta>
            <ta e="T129" id="Seg_8925" s="T128">PTCL</ta>
            <ta e="T130" id="Seg_8926" s="T129">хлеб.[NOM.SG]</ta>
            <ta e="T131" id="Seg_8927" s="T130">гореть-TR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_8928" s="T131">весь</ta>
            <ta e="T133" id="Seg_8929" s="T132">гореть-TR-MOM-PST.[3SG]</ta>
            <ta e="T134" id="Seg_8930" s="T133">хотеть.PST.M.SG</ta>
            <ta e="T137" id="Seg_8931" s="T136">поставить-INF.LAT</ta>
            <ta e="T138" id="Seg_8932" s="T137">а</ta>
            <ta e="T139" id="Seg_8933" s="T138">этот.[NOM.SG]</ta>
            <ta e="T140" id="Seg_8934" s="T139">взять-PST.[3SG]</ta>
            <ta e="T141" id="Seg_8935" s="T140">и</ta>
            <ta e="T142" id="Seg_8936" s="T141">хлеб.[NOM.SG]</ta>
            <ta e="T143" id="Seg_8937" s="T142">гореть-TR-MOM-PST.[3SG]</ta>
            <ta e="T144" id="Seg_8938" s="T143">ругать-DES-PST.[3SG]</ta>
            <ta e="T146" id="Seg_8939" s="T145">этот-INS</ta>
            <ta e="T147" id="Seg_8940" s="T146">этот.[NOM.SG]</ta>
            <ta e="T148" id="Seg_8941" s="T147">сердиться-MOM-PST.[3SG]</ta>
            <ta e="T149" id="Seg_8942" s="T148">трава.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8943" s="T149">резать-PST-1SG</ta>
            <ta e="T151" id="Seg_8944" s="T150">зарод-LAT</ta>
            <ta e="T152" id="Seg_8945" s="T151">носить-PST-1SG</ta>
            <ta e="T153" id="Seg_8946" s="T152">класть-PST-1SG</ta>
            <ta e="T154" id="Seg_8947" s="T153">а</ta>
            <ta e="T155" id="Seg_8948" s="T154">этот.[NOM.SG]</ta>
            <ta e="T156" id="Seg_8949" s="T155">PTCL</ta>
            <ta e="T157" id="Seg_8950" s="T156">%гореть-MOM-PST.[3SG]</ta>
            <ta e="T158" id="Seg_8951" s="T157">чум-LAT</ta>
            <ta e="T159" id="Seg_8952" s="T158">прийти-PST-1SG</ta>
            <ta e="T160" id="Seg_8953" s="T159">там</ta>
            <ta e="T161" id="Seg_8954" s="T160">PTCL</ta>
            <ta e="T162" id="Seg_8955" s="T161">стоять-PST-1SG</ta>
            <ta e="T163" id="Seg_8956" s="T162">тогда</ta>
            <ta e="T165" id="Seg_8957" s="T164">чум-LAT</ta>
            <ta e="T968" id="Seg_8958" s="T165">пойти-CVB</ta>
            <ta e="T166" id="Seg_8959" s="T968">исчезнуть-PST-1SG</ta>
            <ta e="T167" id="Seg_8960" s="T166">сегодня</ta>
            <ta e="T168" id="Seg_8961" s="T167">я.NOM</ta>
            <ta e="T169" id="Seg_8962" s="T168">NEG</ta>
            <ta e="T170" id="Seg_8963" s="T169">работать-PRS-1SG</ta>
            <ta e="T171" id="Seg_8964" s="T170">а</ta>
            <ta e="T172" id="Seg_8965" s="T171">ты.NOM-INS</ta>
            <ta e="T173" id="Seg_8966" s="T172">говорить-PRS-1SG</ta>
            <ta e="T174" id="Seg_8967" s="T173">хотя</ta>
            <ta e="T175" id="Seg_8968" s="T174">NEG</ta>
            <ta e="T176" id="Seg_8969" s="T175">трудный</ta>
            <ta e="T177" id="Seg_8970" s="T176">а</ta>
            <ta e="T178" id="Seg_8971" s="T177">устать-MOM-PST-1SG</ta>
            <ta e="T179" id="Seg_8972" s="T178">очень</ta>
            <ta e="T180" id="Seg_8973" s="T179">очень</ta>
            <ta e="T181" id="Seg_8974" s="T180">дождь.[NOM.SG]</ta>
            <ta e="T182" id="Seg_8975" s="T181">сильно</ta>
            <ta e="T183" id="Seg_8976" s="T182">идти-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_8977" s="T183">прийти-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_8978" s="T185">а</ta>
            <ta e="T187" id="Seg_8979" s="T186">я.NOM</ta>
            <ta e="T189" id="Seg_8980" s="T188">дерево-PL</ta>
            <ta e="T190" id="Seg_8981" s="T189">сажать-PST-1SG</ta>
            <ta e="T191" id="Seg_8982" s="T190">ветер.[NOM.SG]</ta>
            <ta e="T192" id="Seg_8983" s="T191">PTCL</ta>
            <ta e="T193" id="Seg_8984" s="T192">прийти-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_8985" s="T193">я.NOM</ta>
            <ta e="T195" id="Seg_8986" s="T194">рубашка.[NOM.SG]</ta>
            <ta e="T196" id="Seg_8987" s="T195">красивый.[NOM.SG]</ta>
            <ta e="T197" id="Seg_8988" s="T196">там</ta>
            <ta e="T198" id="Seg_8989" s="T197">сидеть-PST-1SG</ta>
            <ta e="T199" id="Seg_8990" s="T198">я.LAT</ta>
            <ta e="T200" id="Seg_8991" s="T199">NEG</ta>
            <ta e="T201" id="Seg_8992" s="T200">мочить-PST.[3SG]</ta>
            <ta e="T202" id="Seg_8993" s="T201">а.то</ta>
            <ta e="T203" id="Seg_8994" s="T202">IRREAL</ta>
            <ta e="T204" id="Seg_8995" s="T203">мочить-PST.[3SG]</ta>
            <ta e="T205" id="Seg_8996" s="T204">PTCL</ta>
            <ta e="T206" id="Seg_8997" s="T205">я.NOM</ta>
            <ta e="T207" id="Seg_8998" s="T206">лес-LAT</ta>
            <ta e="T208" id="Seg_8999" s="T207">идти-PST-1SG</ta>
            <ta e="T209" id="Seg_9000" s="T208">песня.[NOM.SG]</ta>
            <ta e="T211" id="Seg_9001" s="T210">петь-PST-1SG</ta>
            <ta e="T212" id="Seg_9002" s="T211">а</ta>
            <ta e="T213" id="Seg_9003" s="T212">тогда</ta>
            <ta e="T214" id="Seg_9004" s="T213">плакать-PST-1SG</ta>
            <ta e="T215" id="Seg_9005" s="T214">я.NOM</ta>
            <ta e="T216" id="Seg_9006" s="T215">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T217" id="Seg_9007" s="T216">умереть-RES-PST.[3SG]</ta>
            <ta e="T218" id="Seg_9008" s="T217">я.NOM</ta>
            <ta e="T219" id="Seg_9009" s="T218">жалеть-PST-1SG</ta>
            <ta e="T220" id="Seg_9010" s="T219">женщина.[NOM.SG]</ta>
            <ta e="T221" id="Seg_9011" s="T220">и</ta>
            <ta e="T222" id="Seg_9012" s="T221">мужчина.[NOM.SG]</ta>
            <ta e="T223" id="Seg_9013" s="T222">жить-DUR-3PL</ta>
            <ta e="T224" id="Seg_9014" s="T223">а</ta>
            <ta e="T225" id="Seg_9015" s="T224">ты.NOM</ta>
            <ta e="T226" id="Seg_9016" s="T225">%%-LAT</ta>
            <ta e="T227" id="Seg_9017" s="T226">сидеть-IMP.2SG</ta>
            <ta e="T228" id="Seg_9018" s="T227">где</ta>
            <ta e="T229" id="Seg_9019" s="T228">быть-PST-2SG</ta>
            <ta e="T230" id="Seg_9020" s="T229">куда</ta>
            <ta e="T231" id="Seg_9021" s="T230">идти-PRS-2SG</ta>
            <ta e="T232" id="Seg_9022" s="T231">чум-LAT</ta>
            <ta e="T233" id="Seg_9023" s="T232">идти-PRS-1SG</ta>
            <ta e="T234" id="Seg_9024" s="T233">деньги.[NOM.SG]</ta>
            <ta e="T235" id="Seg_9025" s="T234">деньги.[NOM.SG]</ta>
            <ta e="T236" id="Seg_9026" s="T235">NEG.EX.[3SG]</ta>
            <ta e="T237" id="Seg_9027" s="T236">деньги-CAR.ADJ</ta>
            <ta e="T238" id="Seg_9028" s="T237">жить-INF.LAT</ta>
            <ta e="T239" id="Seg_9029" s="T238">NEG</ta>
            <ta e="T240" id="Seg_9030" s="T239">хороший.[NOM.SG]</ta>
            <ta e="T241" id="Seg_9031" s="T240">деньги.[NOM.SG]</ta>
            <ta e="T242" id="Seg_9032" s="T241">деньги.[NOM.SG]</ta>
            <ta e="T243" id="Seg_9033" s="T242">быть-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_9034" s="T243">так</ta>
            <ta e="T245" id="Seg_9035" s="T244">хороший.[NOM.SG]</ta>
            <ta e="T246" id="Seg_9036" s="T245">жить-INF.LAT</ta>
            <ta e="T247" id="Seg_9037" s="T246">деньги-LAT</ta>
            <ta e="T248" id="Seg_9038" s="T247">что=INDEF</ta>
            <ta e="T249" id="Seg_9039" s="T248">взять-FUT-2SG</ta>
            <ta e="T250" id="Seg_9040" s="T249">яйцо-PL</ta>
            <ta e="T251" id="Seg_9041" s="T250">можно</ta>
            <ta e="T252" id="Seg_9042" s="T251">взять-INF.LAT</ta>
            <ta e="T253" id="Seg_9043" s="T252">хлеб.[NOM.SG]</ta>
            <ta e="T254" id="Seg_9044" s="T253">взять-INF.LAT</ta>
            <ta e="T255" id="Seg_9045" s="T254">рыба.[NOM.SG]</ta>
            <ta e="T256" id="Seg_9046" s="T255">взять-INF.LAT</ta>
            <ta e="T257" id="Seg_9047" s="T256">мясо.[NOM.SG]</ta>
            <ta e="T258" id="Seg_9048" s="T257">взять-FUT-2SG</ta>
            <ta e="T259" id="Seg_9049" s="T258">деньги-LAT</ta>
            <ta e="T260" id="Seg_9050" s="T259">всегда</ta>
            <ta e="T261" id="Seg_9051" s="T260">деньги.[NOM.SG]</ta>
            <ta e="T262" id="Seg_9052" s="T261">NEG.EX.[3SG]</ta>
            <ta e="T263" id="Seg_9053" s="T262">так</ta>
            <ta e="T264" id="Seg_9054" s="T263">что.[NOM.SG]=INDEF</ta>
            <ta e="T265" id="Seg_9055" s="T264">NEG</ta>
            <ta e="T266" id="Seg_9056" s="T265">взять-FUT-2SG</ta>
            <ta e="T267" id="Seg_9057" s="T266">рыба.[NOM.SG]</ta>
            <ta e="T268" id="Seg_9058" s="T267">ловить-INF.LAT</ta>
            <ta e="T269" id="Seg_9059" s="T268">идти-PRS-2SG</ta>
            <ta e="T270" id="Seg_9060" s="T269">рыба.[NOM.SG]</ta>
            <ta e="T271" id="Seg_9061" s="T270">NEG</ta>
            <ta e="T273" id="Seg_9062" s="T272">ловить-PST-2SG</ta>
            <ta e="T274" id="Seg_9063" s="T273">так</ta>
            <ta e="T275" id="Seg_9064" s="T274">NEG.AUX-IMP.2SG</ta>
            <ta e="T276" id="Seg_9065" s="T275">прийти-CNG</ta>
            <ta e="T277" id="Seg_9066" s="T276">чум-LAT-2SG</ta>
            <ta e="T278" id="Seg_9067" s="T277">пойти-EP-IMP.2SG</ta>
            <ta e="T279" id="Seg_9068" s="T278">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T280" id="Seg_9069" s="T279">позвать-IMP.2SG</ta>
            <ta e="T281" id="Seg_9070" s="T280">NEG</ta>
            <ta e="T282" id="Seg_9071" s="T281">пойти-FUT-3PL</ta>
            <ta e="T283" id="Seg_9072" s="T282">так</ta>
            <ta e="T284" id="Seg_9073" s="T283">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T285" id="Seg_9074" s="T284">NEG.AUX-IMP.2SG</ta>
            <ta e="T286" id="Seg_9075" s="T285">прийти-CNG</ta>
            <ta e="T287" id="Seg_9076" s="T286">позвать-IMP.2SG.O</ta>
            <ta e="T288" id="Seg_9077" s="T287">хороший-LAT.ADV</ta>
            <ta e="T289" id="Seg_9078" s="T288">чтобы</ta>
            <ta e="T290" id="Seg_9079" s="T289">прийти-PST-3PL</ta>
            <ta e="T291" id="Seg_9080" s="T290">NEG</ta>
            <ta e="T292" id="Seg_9081" s="T291">хороший.[NOM.SG]</ta>
            <ta e="T293" id="Seg_9082" s="T292">мужчина.[NOM.SG]</ta>
            <ta e="T294" id="Seg_9083" s="T293">верно</ta>
            <ta e="T295" id="Seg_9084" s="T294">глаз-NOM/GEN/ACC.1SG-INS</ta>
            <ta e="T296" id="Seg_9085" s="T295">NEG</ta>
            <ta e="T297" id="Seg_9086" s="T296">мочь-PRS-1SG</ta>
            <ta e="T298" id="Seg_9087" s="T297">смотреть-INF.LAT</ta>
            <ta e="T299" id="Seg_9088" s="T298">вонючий.[NOM.SG]</ta>
            <ta e="T300" id="Seg_9089" s="T299">очень</ta>
            <ta e="T301" id="Seg_9090" s="T300">мягко</ta>
            <ta e="T302" id="Seg_9091" s="T301">пойти-EP-IMP.2SG</ta>
            <ta e="T303" id="Seg_9092" s="T302">сильно</ta>
            <ta e="T304" id="Seg_9093" s="T303">NEG.AUX-IMP.2SG</ta>
            <ta e="T305" id="Seg_9094" s="T304">идти-EP-CNG</ta>
            <ta e="T306" id="Seg_9095" s="T305">видеть-PRS-1SG</ta>
            <ta e="T307" id="Seg_9096" s="T306">PTCL</ta>
            <ta e="T308" id="Seg_9097" s="T307">этот.[NOM.SG]</ta>
            <ta e="T309" id="Seg_9098" s="T308">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T310" id="Seg_9099" s="T309">прийти-PRS.[3SG]</ta>
            <ta e="T311" id="Seg_9100" s="T310">я.LAT</ta>
            <ta e="T312" id="Seg_9101" s="T311">сам-LAT/LOC.3SG</ta>
            <ta e="T313" id="Seg_9102" s="T312">PTCL</ta>
            <ta e="T314" id="Seg_9103" s="T313">%звать-DUR.[3SG]</ta>
            <ta e="T315" id="Seg_9104" s="T314">а</ta>
            <ta e="T316" id="Seg_9105" s="T315">я.NOM</ta>
            <ta e="T317" id="Seg_9106" s="T316">NEG</ta>
            <ta e="T318" id="Seg_9107" s="T317">пойти-PRS-1SG</ta>
            <ta e="T319" id="Seg_9108" s="T318">лес-LOC</ta>
            <ta e="T321" id="Seg_9109" s="T320">дерево-PL</ta>
            <ta e="T322" id="Seg_9110" s="T321">PTCL</ta>
            <ta e="T323" id="Seg_9111" s="T322">расти-DUR-3PL</ta>
            <ta e="T324" id="Seg_9112" s="T323">красивый.[NOM.SG]</ta>
            <ta e="T325" id="Seg_9113" s="T324">цветок-PL</ta>
            <ta e="T326" id="Seg_9114" s="T325">PTCL</ta>
            <ta e="T327" id="Seg_9115" s="T326">очень</ta>
            <ta e="T328" id="Seg_9116" s="T327">сильно</ta>
            <ta e="T329" id="Seg_9117" s="T328">пахнуть-PRS-3PL</ta>
            <ta e="T330" id="Seg_9118" s="T329">очень</ta>
            <ta e="T331" id="Seg_9119" s="T330">слышать-PRS-1SG</ta>
            <ta e="T332" id="Seg_9120" s="T331">хороший.[NOM.SG]</ta>
            <ta e="T333" id="Seg_9121" s="T332">пахнуть-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_9122" s="T333">что.[NOM.SG]=INDEF</ta>
            <ta e="T335" id="Seg_9123" s="T334">кипятить-DUR.[3SG]</ta>
            <ta e="T336" id="Seg_9124" s="T335">курица-PL</ta>
            <ta e="T337" id="Seg_9125" s="T336">идти-PRS-3PL</ta>
            <ta e="T338" id="Seg_9126" s="T337">земля-LOC</ta>
            <ta e="T339" id="Seg_9127" s="T338">PTCL</ta>
            <ta e="T340" id="Seg_9128" s="T339">земля-ACC</ta>
            <ta e="T341" id="Seg_9129" s="T340">чесать-DUR-3PL</ta>
            <ta e="T343" id="Seg_9130" s="T342">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T347" id="Seg_9131" s="T346">этот.[NOM.SG]</ta>
            <ta e="T348" id="Seg_9132" s="T347">мужчина-ACC</ta>
            <ta e="T349" id="Seg_9133" s="T348">надо</ta>
            <ta e="T351" id="Seg_9134" s="T350">этот.[NOM.SG]</ta>
            <ta e="T353" id="Seg_9135" s="T352">ждать-INF.LAT</ta>
            <ta e="T354" id="Seg_9136" s="T353">этот.[NOM.SG]</ta>
            <ta e="T355" id="Seg_9137" s="T354">PTCL</ta>
            <ta e="T356" id="Seg_9138" s="T355">скоро</ta>
            <ta e="T357" id="Seg_9139" s="T356">прийти-FUT-3SG</ta>
            <ta e="T358" id="Seg_9140" s="T357">водка.[NOM.SG]</ta>
            <ta e="T359" id="Seg_9141" s="T358">принести-FUT-3SG</ta>
            <ta e="T360" id="Seg_9142" s="T359">этот-PL</ta>
            <ta e="T361" id="Seg_9143" s="T360">PTCL</ta>
            <ta e="T362" id="Seg_9144" s="T361">этот-ACC</ta>
            <ta e="T363" id="Seg_9145" s="T362">ждать-DUR-3PL</ta>
            <ta e="T364" id="Seg_9146" s="T363">этот-PL</ta>
            <ta e="T365" id="Seg_9147" s="T364">этот-PL</ta>
            <ta e="T366" id="Seg_9148" s="T365">ждать-DUR-3PL</ta>
            <ta e="T367" id="Seg_9149" s="T366">сегодня</ta>
            <ta e="T368" id="Seg_9150" s="T367">мы.NOM</ta>
            <ta e="T369" id="Seg_9151" s="T368">хороший.[NOM.SG]</ta>
            <ta e="T370" id="Seg_9152" s="T369">говорить-FUT</ta>
            <ta e="T371" id="Seg_9153" s="T370">а</ta>
            <ta e="T372" id="Seg_9154" s="T371">что.[NOM.SG]</ta>
            <ta e="T373" id="Seg_9155" s="T372">говорить-DUR.PST-2PL</ta>
            <ta e="T374" id="Seg_9156" s="T373">%%</ta>
            <ta e="T375" id="Seg_9157" s="T374">ругать-PST-1PL</ta>
            <ta e="T376" id="Seg_9158" s="T375">и</ta>
            <ta e="T377" id="Seg_9159" s="T376">PTCL</ta>
            <ta e="T378" id="Seg_9160" s="T377">вы.NOM</ta>
            <ta e="T379" id="Seg_9161" s="T378">ругать-PST-1PL</ta>
            <ta e="T380" id="Seg_9162" s="T379">лошадь-PL</ta>
            <ta e="T381" id="Seg_9163" s="T380">PTCL</ta>
            <ta e="T382" id="Seg_9164" s="T381">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T383" id="Seg_9165" s="T382">два.[NOM.SG]</ta>
            <ta e="T384" id="Seg_9166" s="T383">лошадь.[NOM.SG]</ta>
            <ta e="T385" id="Seg_9167" s="T384">идти-PRS.[3SG]</ta>
            <ta e="T386" id="Seg_9168" s="T385">один.[NOM.SG]</ta>
            <ta e="T387" id="Seg_9169" s="T386">лошадь.[NOM.SG]</ta>
            <ta e="T388" id="Seg_9170" s="T387">идти-PRS.[3SG]</ta>
            <ta e="T389" id="Seg_9171" s="T388">я.NOM</ta>
            <ta e="T390" id="Seg_9172" s="T389">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T391" id="Seg_9173" s="T390">прийти-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_9174" s="T391">а</ta>
            <ta e="T393" id="Seg_9175" s="T392">где</ta>
            <ta e="T394" id="Seg_9176" s="T393">быть-PST.[3SG]</ta>
            <ta e="T395" id="Seg_9177" s="T394">рыба.[NOM.SG]</ta>
            <ta e="T396" id="Seg_9178" s="T395">взять-CVB</ta>
            <ta e="T397" id="Seg_9179" s="T396">идти-PST.[3SG]</ta>
            <ta e="T398" id="Seg_9180" s="T397">NEG</ta>
            <ta e="T399" id="Seg_9181" s="T398">знать-PRS-1SG</ta>
            <ta e="T400" id="Seg_9182" s="T399">принести-PST.[3SG]</ta>
            <ta e="T401" id="Seg_9183" s="T400">ли</ta>
            <ta e="T402" id="Seg_9184" s="T401">NEG</ta>
            <ta e="T403" id="Seg_9185" s="T402">принести-PST.[3SG]</ta>
            <ta e="T404" id="Seg_9186" s="T403">принести-PST.[3SG]</ta>
            <ta e="T405" id="Seg_9187" s="T404">так</ta>
            <ta e="T406" id="Seg_9188" s="T405">печь-INF.LAT</ta>
            <ta e="T407" id="Seg_9189" s="T406">надо</ta>
            <ta e="T408" id="Seg_9190" s="T407">этот</ta>
            <ta e="T409" id="Seg_9191" s="T408">женщина.[NOM.SG]</ta>
            <ta e="T410" id="Seg_9192" s="T409">PTCL</ta>
            <ta e="T411" id="Seg_9193" s="T410">беременная.[NOM.SG]</ta>
            <ta e="T412" id="Seg_9194" s="T411">скоро</ta>
            <ta e="T413" id="Seg_9195" s="T412">%%</ta>
            <ta e="T414" id="Seg_9196" s="T413">ребенок.[NOM.SG]</ta>
            <ta e="T415" id="Seg_9197" s="T414">принести-FUT-3SG</ta>
            <ta e="T416" id="Seg_9198" s="T415">надо</ta>
            <ta e="T417" id="Seg_9199" s="T416">этот-ACC</ta>
            <ta e="T418" id="Seg_9200" s="T417">пугать-MOM-INF.LAT</ta>
            <ta e="T419" id="Seg_9201" s="T418">пойти-INF.LAT</ta>
            <ta e="T420" id="Seg_9202" s="T419">надо</ta>
            <ta e="T421" id="Seg_9203" s="T420">что=INDEF</ta>
            <ta e="T422" id="Seg_9204" s="T421">продавать-CVB</ta>
            <ta e="T423" id="Seg_9205" s="T422">взять-INF.LAT</ta>
            <ta e="T424" id="Seg_9206" s="T423">когда</ta>
            <ta e="T426" id="Seg_9207" s="T424">день.[NOM.SG]</ta>
            <ta e="T428" id="Seg_9208" s="T427">когда</ta>
            <ta e="T429" id="Seg_9209" s="T428">день.[NOM.SG]</ta>
            <ta e="T430" id="Seg_9210" s="T429">прийти-DUR.[3SG]</ta>
            <ta e="T431" id="Seg_9211" s="T430">и</ta>
            <ta e="T432" id="Seg_9212" s="T431">я.NOM</ta>
            <ta e="T433" id="Seg_9213" s="T432">встать-DUR-1SG</ta>
            <ta e="T434" id="Seg_9214" s="T433">когда</ta>
            <ta e="T435" id="Seg_9215" s="T434">день.[NOM.SG]</ta>
            <ta e="T436" id="Seg_9216" s="T435">идти-PRS.[3SG]</ta>
            <ta e="T437" id="Seg_9217" s="T436">я.NOM</ta>
            <ta e="T438" id="Seg_9218" s="T437">лежать-FUT-1SG</ta>
            <ta e="T439" id="Seg_9219" s="T438">этот</ta>
            <ta e="T440" id="Seg_9220" s="T439">зима.[NOM.SG]</ta>
            <ta e="T441" id="Seg_9221" s="T440">очень</ta>
            <ta e="T442" id="Seg_9222" s="T441">холодный.[NOM.SG]</ta>
            <ta e="T443" id="Seg_9223" s="T442">быть-PST.[3SG]</ta>
            <ta e="T444" id="Seg_9224" s="T443">очень</ta>
            <ta e="T445" id="Seg_9225" s="T444">длинный.[NOM.SG]</ta>
            <ta e="T446" id="Seg_9226" s="T445">быть-PST.[3SG]</ta>
            <ta e="T447" id="Seg_9227" s="T446">вечер-PL</ta>
            <ta e="T448" id="Seg_9228" s="T447">PTCL</ta>
            <ta e="T449" id="Seg_9229" s="T448">очень</ta>
            <ta e="T453" id="Seg_9230" s="T452">пот-NOM/GEN/ACC.3PL</ta>
            <ta e="T454" id="Seg_9231" s="T453">а</ta>
            <ta e="T455" id="Seg_9232" s="T454">этот.[NOM.SG]</ta>
            <ta e="T456" id="Seg_9233" s="T455">год.[NOM.SG]</ta>
            <ta e="T457" id="Seg_9234" s="T456">прийти-PST.[3SG]</ta>
            <ta e="T458" id="Seg_9235" s="T457">так</ta>
            <ta e="T459" id="Seg_9236" s="T458">очень</ta>
            <ta e="T460" id="Seg_9237" s="T459">теплый.[NOM.SG]</ta>
            <ta e="T461" id="Seg_9238" s="T460">день-PL</ta>
            <ta e="T462" id="Seg_9239" s="T461">PTCL</ta>
            <ta e="T463" id="Seg_9240" s="T462">длинный-PL</ta>
            <ta e="T464" id="Seg_9241" s="T463">надо</ta>
            <ta e="T465" id="Seg_9242" s="T464">я.LAT</ta>
            <ta e="T466" id="Seg_9243" s="T465">вода.[NOM.SG]</ta>
            <ta e="T467" id="Seg_9244" s="T466">печь-LAT</ta>
            <ta e="T468" id="Seg_9245" s="T467">поставить-INF.LAT</ta>
            <ta e="T469" id="Seg_9246" s="T468">а.то</ta>
            <ta e="T470" id="Seg_9247" s="T469">теленок</ta>
            <ta e="T471" id="Seg_9248" s="T470">прийти-FUT-3SG</ta>
            <ta e="T472" id="Seg_9249" s="T471">вода.[NOM.SG]</ta>
            <ta e="T473" id="Seg_9250" s="T472">NEG.EX.[3SG]</ta>
            <ta e="T474" id="Seg_9251" s="T473">а</ta>
            <ta e="T475" id="Seg_9252" s="T474">этот.[NOM.SG]</ta>
            <ta e="T476" id="Seg_9253" s="T475">холодный.[NOM.SG]</ta>
            <ta e="T477" id="Seg_9254" s="T476">вода.[NOM.SG]</ta>
            <ta e="T478" id="Seg_9255" s="T477">NEG</ta>
            <ta e="T479" id="Seg_9256" s="T478">пить-PRS.[3SG]</ta>
            <ta e="T482" id="Seg_9257" s="T481">поставить-IMP.2SG</ta>
            <ta e="T483" id="Seg_9258" s="T482">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T484" id="Seg_9259" s="T483">лошадь.[NOM.SG]</ta>
            <ta e="T485" id="Seg_9260" s="T484">этот.[NOM.SG]</ta>
            <ta e="T486" id="Seg_9261" s="T485">наверное</ta>
            <ta e="T487" id="Seg_9262" s="T486">NEG</ta>
            <ta e="T488" id="Seg_9263" s="T487">стоять-PRS.[3SG]</ta>
            <ta e="T489" id="Seg_9264" s="T488">надо</ta>
            <ta e="T490" id="Seg_9265" s="T489">этот-ACC</ta>
            <ta e="T491" id="Seg_9266" s="T490">поставить-INF.LAT</ta>
            <ta e="T492" id="Seg_9267" s="T491">мальчик.[NOM.SG]</ta>
            <ta e="T493" id="Seg_9268" s="T492">дочь-INS</ta>
            <ta e="T494" id="Seg_9269" s="T493">PTCL</ta>
            <ta e="T495" id="Seg_9270" s="T494">расти-PST-3PL</ta>
            <ta e="T496" id="Seg_9271" s="T495">тогда</ta>
            <ta e="T497" id="Seg_9272" s="T496">мальчик.[NOM.SG]</ta>
            <ta e="T498" id="Seg_9273" s="T497">сказать-PST.[3SG]</ta>
            <ta e="T500" id="Seg_9274" s="T499">я.LAT</ta>
            <ta e="T501" id="Seg_9275" s="T500">пойти-FUT-2SG</ta>
            <ta e="T502" id="Seg_9276" s="T501">мужчина-LAT</ta>
            <ta e="T503" id="Seg_9277" s="T502">пойти-FUT-1SG</ta>
            <ta e="T504" id="Seg_9278" s="T503">ну</ta>
            <ta e="T505" id="Seg_9279" s="T504">я.NOM</ta>
            <ta e="T506" id="Seg_9280" s="T505">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T507" id="Seg_9281" s="T506">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T508" id="Seg_9282" s="T507">послать-FUT-1SG</ta>
            <ta e="T509" id="Seg_9283" s="T508">ты.DAT</ta>
            <ta e="T510" id="Seg_9284" s="T509">тогда</ta>
            <ta e="T511" id="Seg_9285" s="T510">мать-NOM/GEN.3SG</ta>
            <ta e="T512" id="Seg_9286" s="T511">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T513" id="Seg_9287" s="T512">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T514" id="Seg_9288" s="T513">посылать-PST.[3SG]</ta>
            <ta e="T515" id="Seg_9289" s="T514">этот-PL</ta>
            <ta e="T516" id="Seg_9290" s="T515">прийти-PST-3PL</ta>
            <ta e="T517" id="Seg_9291" s="T516">дочь-GEN</ta>
            <ta e="T518" id="Seg_9292" s="T517">мать-INS</ta>
            <ta e="T519" id="Seg_9293" s="T518">отец-INS</ta>
            <ta e="T520" id="Seg_9294" s="T519">говорить-MOM-PST-3PL</ta>
            <ta e="T521" id="Seg_9295" s="T520">дочь-NOM/GEN/ACC.2SG</ta>
            <ta e="T523" id="Seg_9296" s="T522">дать-PST-2PL</ta>
            <ta e="T524" id="Seg_9297" s="T523">я.NOM</ta>
            <ta e="T526" id="Seg_9298" s="T525">я.NOM</ta>
            <ta e="T527" id="Seg_9299" s="T526">мальчик-LAT</ta>
            <ta e="T529" id="Seg_9300" s="T528">дать-PST-1PL</ta>
            <ta e="T531" id="Seg_9301" s="T530">тогда</ta>
            <ta e="T532" id="Seg_9302" s="T531">дочь-ACC</ta>
            <ta e="T533" id="Seg_9303" s="T532">спросить-PST-3PL</ta>
            <ta e="T534" id="Seg_9304" s="T533">ты.NOM</ta>
            <ta e="T535" id="Seg_9305" s="T534">пойти-FUT-2SG</ta>
            <ta e="T536" id="Seg_9306" s="T535">мы.NOM</ta>
            <ta e="T537" id="Seg_9307" s="T536">мальчик-LAT</ta>
            <ta e="T538" id="Seg_9308" s="T537">мужчина-LAT</ta>
            <ta e="T539" id="Seg_9309" s="T538">пойти-FUT-1SG</ta>
            <ta e="T540" id="Seg_9310" s="T539">тогда</ta>
            <ta e="T541" id="Seg_9311" s="T540">жениться-PST-3PL</ta>
            <ta e="T542" id="Seg_9312" s="T541">водка.[NOM.SG]</ta>
            <ta e="T543" id="Seg_9313" s="T542">принести-PST-3PL</ta>
            <ta e="T544" id="Seg_9314" s="T543">хлеб.[NOM.SG]</ta>
            <ta e="T545" id="Seg_9315" s="T544">принести-PST-3PL</ta>
            <ta e="T546" id="Seg_9316" s="T545">мясо.[NOM.SG]</ta>
            <ta e="T547" id="Seg_9317" s="T546">много</ta>
            <ta e="T548" id="Seg_9318" s="T547">быть-PST.[3SG]</ta>
            <ta e="T549" id="Seg_9319" s="T548">тогда</ta>
            <ta e="T550" id="Seg_9320" s="T549">люди.[NOM.SG]</ta>
            <ta e="T551" id="Seg_9321" s="T550">прийти-PST-3PL</ta>
            <ta e="T552" id="Seg_9322" s="T551">водка.[NOM.SG]</ta>
            <ta e="T553" id="Seg_9323" s="T552">пить-PST-3PL</ta>
            <ta e="T554" id="Seg_9324" s="T553">PTCL</ta>
            <ta e="T555" id="Seg_9325" s="T554">тогда</ta>
            <ta e="T558" id="Seg_9326" s="T557">день.[NOM.SG]</ta>
            <ta e="T559" id="Seg_9327" s="T558">пойти-PST.[3SG]</ta>
            <ta e="T560" id="Seg_9328" s="T559">этот-PL</ta>
            <ta e="T561" id="Seg_9329" s="T560">цветок-PL</ta>
            <ta e="T562" id="Seg_9330" s="T561">носить-PST-3PL</ta>
            <ta e="T563" id="Seg_9331" s="T562">и</ta>
            <ta e="T564" id="Seg_9332" s="T563">колокольня-LAT</ta>
            <ta e="T565" id="Seg_9333" s="T564">пойти-PST-3PL</ta>
            <ta e="T566" id="Seg_9334" s="T565">там</ta>
            <ta e="T567" id="Seg_9335" s="T566">священник.[NOM.SG]</ta>
            <ta e="T568" id="Seg_9336" s="T567">этот-ACC.PL</ta>
            <ta e="T569" id="Seg_9337" s="T568">бог.[NOM.SG]</ta>
            <ta e="T570" id="Seg_9338" s="T569">кланяться-PST.[3SG]</ta>
            <ta e="T571" id="Seg_9339" s="T570">тогда</ta>
            <ta e="T574" id="Seg_9340" s="T573">венок-PL</ta>
            <ta e="T575" id="Seg_9341" s="T574">надеть-PST-3PL</ta>
            <ta e="T576" id="Seg_9342" s="T575">тогда</ta>
            <ta e="T577" id="Seg_9343" s="T576">чум-LAT/LOC.3SG</ta>
            <ta e="T578" id="Seg_9344" s="T577">прийти-PST-3PL</ta>
            <ta e="T579" id="Seg_9345" s="T578">тоже</ta>
            <ta e="T580" id="Seg_9346" s="T579">PTCL</ta>
            <ta e="T581" id="Seg_9347" s="T580">водка.[NOM.SG]</ta>
            <ta e="T582" id="Seg_9348" s="T581">пить-PST-3PL</ta>
            <ta e="T583" id="Seg_9349" s="T582">дочь-GEN</ta>
            <ta e="T584" id="Seg_9350" s="T583">родственник-LAT</ta>
            <ta e="T585" id="Seg_9351" s="T584">прийти-PST-3PL</ta>
            <ta e="T586" id="Seg_9352" s="T585">и</ta>
            <ta e="T587" id="Seg_9353" s="T586">мальчик-GEN</ta>
            <ta e="T588" id="Seg_9354" s="T587">девушка.[NOM.SG]</ta>
            <ta e="T589" id="Seg_9355" s="T588">прийти-PST-3PL</ta>
            <ta e="T590" id="Seg_9356" s="T589">много</ta>
            <ta e="T591" id="Seg_9357" s="T590">люди.[NOM.SG]</ta>
            <ta e="T592" id="Seg_9358" s="T591">быть-PST-3PL</ta>
            <ta e="T593" id="Seg_9359" s="T592">водка.[NOM.SG]</ta>
            <ta e="T594" id="Seg_9360" s="T593">много</ta>
            <ta e="T595" id="Seg_9361" s="T594">быть-PST.[3SG]</ta>
            <ta e="T596" id="Seg_9362" s="T595">стол-LOC</ta>
            <ta e="T597" id="Seg_9363" s="T596">очень</ta>
            <ta e="T598" id="Seg_9364" s="T597">много</ta>
            <ta e="T599" id="Seg_9365" s="T598">что.[NOM.SG]</ta>
            <ta e="T601" id="Seg_9366" s="T600">лежать-DUR.[3SG]</ta>
            <ta e="T602" id="Seg_9367" s="T601">когда</ta>
            <ta e="T603" id="Seg_9368" s="T602">девушка.[NOM.SG]</ta>
            <ta e="T604" id="Seg_9369" s="T603">жениться-PST-3PL</ta>
            <ta e="T605" id="Seg_9370" s="T604">мать-NOM/GEN.3SG</ta>
            <ta e="T606" id="Seg_9371" s="T605">отец-NOM/GEN.3SG</ta>
            <ta e="T607" id="Seg_9372" s="T606">сказать-PST-3PL</ta>
            <ta e="T608" id="Seg_9373" s="T607">этот-GEN</ta>
            <ta e="T609" id="Seg_9374" s="T608">одежда-NOM/GEN.3SG</ta>
            <ta e="T610" id="Seg_9375" s="T609">NEG.EX.[3SG]</ta>
            <ta e="T611" id="Seg_9376" s="T610">надо</ta>
            <ta e="T612" id="Seg_9377" s="T611">ботинок-NOM/GEN/ACC.3PL</ta>
            <ta e="T613" id="Seg_9378" s="T612">взять-INF.LAT</ta>
            <ta e="T614" id="Seg_9379" s="T613">надо</ta>
            <ta e="T615" id="Seg_9380" s="T614">пальто.[NOM.SG]</ta>
            <ta e="T616" id="Seg_9381" s="T615">взять-INF.LAT</ta>
            <ta e="T617" id="Seg_9382" s="T616">платок.[NOM.SG]</ta>
            <ta e="T618" id="Seg_9383" s="T617">взять-INF.LAT</ta>
            <ta e="T619" id="Seg_9384" s="T618">одежда-NOM/GEN.3SG</ta>
            <ta e="T620" id="Seg_9385" s="T619">мало</ta>
            <ta e="T621" id="Seg_9386" s="T620">рубашка-NOM/GEN/ACC.3SG</ta>
            <ta e="T622" id="Seg_9387" s="T621">NEG.EX.[3SG]</ta>
            <ta e="T623" id="Seg_9388" s="T622">тогда</ta>
            <ta e="T625" id="Seg_9389" s="T624">этот-PL</ta>
            <ta e="T626" id="Seg_9390" s="T625">деньги-ACC</ta>
            <ta e="T627" id="Seg_9391" s="T626">взять-PST-3PL</ta>
            <ta e="T628" id="Seg_9392" s="T627">два.[NOM.SG]</ta>
            <ta e="T629" id="Seg_9393" s="T628">десять.[NOM.SG]</ta>
            <ta e="T630" id="Seg_9394" s="T629">пять.[NOM.SG]</ta>
            <ta e="T631" id="Seg_9395" s="T630">тогда</ta>
            <ta e="T632" id="Seg_9396" s="T631">мать-NOM/GEN.3SG</ta>
            <ta e="T633" id="Seg_9397" s="T632">отец-GEN</ta>
            <ta e="T634" id="Seg_9398" s="T633">этот-LAT</ta>
            <ta e="T635" id="Seg_9399" s="T634">дать-PST-3PL</ta>
            <ta e="T636" id="Seg_9400" s="T635">корова.[NOM.SG]</ta>
            <ta e="T637" id="Seg_9401" s="T636">родственник-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T638" id="Seg_9402" s="T637">дать-PST-3PL</ta>
            <ta e="T639" id="Seg_9403" s="T638">овца.[NOM.SG]</ta>
            <ta e="T640" id="Seg_9404" s="T639">дать-PST-3PL</ta>
            <ta e="T641" id="Seg_9405" s="T640">лошадь-PL</ta>
            <ta e="T642" id="Seg_9406" s="T641">деньги.[NOM.SG]</ta>
            <ta e="T643" id="Seg_9407" s="T642">дать-PST-3PL</ta>
            <ta e="T644" id="Seg_9408" s="T643">очень</ta>
            <ta e="T645" id="Seg_9409" s="T644">много</ta>
            <ta e="T647" id="Seg_9410" s="T646">люди.[NOM.SG]</ta>
            <ta e="T648" id="Seg_9411" s="T647">много</ta>
            <ta e="T649" id="Seg_9412" s="T648">быть-PST-3PL</ta>
            <ta e="T650" id="Seg_9413" s="T649">тогда</ta>
            <ta e="T651" id="Seg_9414" s="T650">этот-PL</ta>
            <ta e="T652" id="Seg_9415" s="T651">долго</ta>
            <ta e="T653" id="Seg_9416" s="T652">жить-PST-3PL</ta>
            <ta e="T654" id="Seg_9417" s="T653">брат-NOM/GEN.3SG</ta>
            <ta e="T657" id="Seg_9418" s="T656">и</ta>
            <ta e="T658" id="Seg_9419" s="T657">опять</ta>
            <ta e="T659" id="Seg_9420" s="T658">так</ta>
            <ta e="T660" id="Seg_9421" s="T659">жениться-PST.[3SG]</ta>
            <ta e="T661" id="Seg_9422" s="T660">тогда</ta>
            <ta e="T662" id="Seg_9423" s="T661">этот.[NOM.SG]</ta>
            <ta e="T969" id="Seg_9424" s="T662">пойти-CVB</ta>
            <ta e="T663" id="Seg_9425" s="T969">исчезнуть-PST.[3SG]</ta>
            <ta e="T664" id="Seg_9426" s="T663">этот-LAT</ta>
            <ta e="T665" id="Seg_9427" s="T664">отец-NOM/GEN.3SG</ta>
            <ta e="T666" id="Seg_9428" s="T665">дать-PST.[3SG]</ta>
            <ta e="T667" id="Seg_9429" s="T666">весь</ta>
            <ta e="T668" id="Seg_9430" s="T667">весь</ta>
            <ta e="T669" id="Seg_9431" s="T668">что.[NOM.SG]</ta>
            <ta e="T670" id="Seg_9432" s="T669">дать-PST.[3SG]</ta>
            <ta e="T671" id="Seg_9433" s="T670">лошадь.[NOM.SG]</ta>
            <ta e="T672" id="Seg_9434" s="T671">дать-PST.[3SG]</ta>
            <ta e="T673" id="Seg_9435" s="T672">овца.[NOM.SG]</ta>
            <ta e="T674" id="Seg_9436" s="T673">дать-PST.[3SG]</ta>
            <ta e="T675" id="Seg_9437" s="T674">корова-PL</ta>
            <ta e="T676" id="Seg_9438" s="T675">дать-PST.[3SG]</ta>
            <ta e="T677" id="Seg_9439" s="T676">хлеб.[NOM.SG]</ta>
            <ta e="T678" id="Seg_9440" s="T677">дать-PST.[3SG]</ta>
            <ta e="T680" id="Seg_9441" s="T679">дом.[NOM.SG]</ta>
            <ta e="T681" id="Seg_9442" s="T680">поставить-PST.[3SG]</ta>
            <ta e="T682" id="Seg_9443" s="T681">этот-PL</ta>
            <ta e="T685" id="Seg_9444" s="T684">пойти-PST-3PL</ta>
            <ta e="T687" id="Seg_9445" s="T686">лошадь-3PL-INS</ta>
            <ta e="T688" id="Seg_9446" s="T687">девушка.[NOM.SG]</ta>
            <ta e="T689" id="Seg_9447" s="T688">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T690" id="Seg_9448" s="T689">русский</ta>
            <ta e="T691" id="Seg_9449" s="T690">мать-NOM/GEN.3SG</ta>
            <ta e="T692" id="Seg_9450" s="T691">а</ta>
            <ta e="T693" id="Seg_9451" s="T692">мальчик.[NOM.SG]</ta>
            <ta e="T694" id="Seg_9452" s="T693">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_9453" s="T694">русский</ta>
            <ta e="T696" id="Seg_9454" s="T695">мать-NOM/GEN.3SG</ta>
            <ta e="T697" id="Seg_9455" s="T696">русский</ta>
            <ta e="T698" id="Seg_9456" s="T697">отец-NOM/GEN.3SG</ta>
            <ta e="T699" id="Seg_9457" s="T698">там</ta>
            <ta e="T700" id="Seg_9458" s="T699">пойти-PST-3PL</ta>
            <ta e="T701" id="Seg_9459" s="T700">тогда</ta>
            <ta e="T702" id="Seg_9460" s="T701">прийти-PST-3PL</ta>
            <ta e="T703" id="Seg_9461" s="T702">чум-LAT/LOC.3SG</ta>
            <ta e="T704" id="Seg_9462" s="T703">а</ta>
            <ta e="T705" id="Seg_9463" s="T704">тогда</ta>
            <ta e="T706" id="Seg_9464" s="T705">дочь-GEN</ta>
            <ta e="T707" id="Seg_9465" s="T706">русский</ta>
            <ta e="T708" id="Seg_9466" s="T707">отец-NOM/GEN.3SG</ta>
            <ta e="T710" id="Seg_9467" s="T709">одежда.[NOM.SG]</ta>
            <ta e="T711" id="Seg_9468" s="T710">принести-PST.[3SG]</ta>
            <ta e="T713" id="Seg_9469" s="T712">PTCL</ta>
            <ta e="T714" id="Seg_9470" s="T713">подушка-NOM/GEN/ACC.3PL</ta>
            <ta e="T715" id="Seg_9471" s="T714">принести-PST.[3SG]</ta>
            <ta e="T716" id="Seg_9472" s="T715">PTCL</ta>
            <ta e="T717" id="Seg_9473" s="T716">перина.[NOM.SG]</ta>
            <ta e="T718" id="Seg_9474" s="T717">принести-PST.[3SG]</ta>
            <ta e="T719" id="Seg_9475" s="T718">тогда</ta>
            <ta e="T720" id="Seg_9476" s="T719">прийти-PST-3PL</ta>
            <ta e="T721" id="Seg_9477" s="T720">колокольня-LAT</ta>
            <ta e="T722" id="Seg_9478" s="T721">этот-PL</ta>
            <ta e="T723" id="Seg_9479" s="T722">сесть-PST-3PL</ta>
            <ta e="T725" id="Seg_9480" s="T724">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T727" id="Seg_9481" s="T725">тогда</ta>
            <ta e="T728" id="Seg_9482" s="T727">мальчик-GEN</ta>
            <ta e="T729" id="Seg_9483" s="T728">рубашка-NOM/GEN/ACC.3SG</ta>
            <ta e="T730" id="Seg_9484" s="T729">красивый.[NOM.SG]</ta>
            <ta e="T731" id="Seg_9485" s="T730">новый.[NOM.SG]</ta>
            <ta e="T732" id="Seg_9486" s="T731">дочь-GEN</ta>
            <ta e="T733" id="Seg_9487" s="T732">одежда-NOM/GEN.3SG</ta>
            <ta e="T734" id="Seg_9488" s="T733">тоже</ta>
            <ta e="T735" id="Seg_9489" s="T734">красивый.[NOM.SG]</ta>
            <ta e="T736" id="Seg_9490" s="T735">новый.[NOM.SG]</ta>
            <ta e="T737" id="Seg_9491" s="T736">голова-NOM/GEN.3SG</ta>
            <ta e="T738" id="Seg_9492" s="T737">PTCL</ta>
            <ta e="T739" id="Seg_9493" s="T738">красивый.[NOM.SG]</ta>
            <ta e="T740" id="Seg_9494" s="T739">венок.[NOM.SG]</ta>
            <ta e="T741" id="Seg_9495" s="T740">надеть-PST-3PL</ta>
            <ta e="T742" id="Seg_9496" s="T741">тогда</ta>
            <ta e="T743" id="Seg_9497" s="T742">этот-LAT</ta>
            <ta e="T747" id="Seg_9498" s="T746">тогда</ta>
            <ta e="T748" id="Seg_9499" s="T747">этот-LAT</ta>
            <ta e="T749" id="Seg_9500" s="T748">два.[NOM.SG]</ta>
            <ta e="T750" id="Seg_9501" s="T749">коса.[NOM.SG]</ta>
            <ta e="T751" id="Seg_9502" s="T750">плести-PST-3PL</ta>
            <ta e="T752" id="Seg_9503" s="T751">тогда</ta>
            <ta e="T753" id="Seg_9504" s="T752">завязать-PST-3PL</ta>
            <ta e="T754" id="Seg_9505" s="T753">голова-NOM/GEN.3SG</ta>
            <ta e="T755" id="Seg_9506" s="T754">когда</ta>
            <ta e="T756" id="Seg_9507" s="T755">этот.[NOM.SG]</ta>
            <ta e="T757" id="Seg_9508" s="T756">девушка.[NOM.SG]</ta>
            <ta e="T758" id="Seg_9509" s="T757">быть-PST.[3SG]</ta>
            <ta e="T759" id="Seg_9510" s="T758">один.[NOM.SG]</ta>
            <ta e="T761" id="Seg_9511" s="T760">коса.[NOM.SG]</ta>
            <ta e="T762" id="Seg_9512" s="T761">быть-PST.[3SG]</ta>
            <ta e="T763" id="Seg_9513" s="T762">тогда</ta>
            <ta e="T765" id="Seg_9514" s="T764">русский</ta>
            <ta e="T766" id="Seg_9515" s="T765">мать-NOM/GEN.3SG</ta>
            <ta e="T767" id="Seg_9516" s="T766">этот.[NOM.SG]</ta>
            <ta e="T768" id="Seg_9517" s="T767">дочь-GEN</ta>
            <ta e="T769" id="Seg_9518" s="T768">русский</ta>
            <ta e="T770" id="Seg_9519" s="T769">мать-NOM/GEN.3SG</ta>
            <ta e="T771" id="Seg_9520" s="T770">и</ta>
            <ta e="T772" id="Seg_9521" s="T771">мальчик-GEN</ta>
            <ta e="T773" id="Seg_9522" s="T772">русский</ta>
            <ta e="T774" id="Seg_9523" s="T773">мать-NOM/GEN.3SG</ta>
            <ta e="T775" id="Seg_9524" s="T774">этот-LAT</ta>
            <ta e="T776" id="Seg_9525" s="T775">два.[NOM.SG]</ta>
            <ta e="T777" id="Seg_9526" s="T776">плести-PST-3PL</ta>
            <ta e="T778" id="Seg_9527" s="T777">и</ta>
            <ta e="T779" id="Seg_9528" s="T778">завязать-PST-3PL</ta>
            <ta e="T780" id="Seg_9529" s="T779">голова-NOM/GEN.3SG</ta>
            <ta e="T781" id="Seg_9530" s="T780">этот-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T782" id="Seg_9531" s="T781">мальчик.[NOM.SG]</ta>
            <ta e="T783" id="Seg_9532" s="T782">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T784" id="Seg_9533" s="T783">русский</ta>
            <ta e="T785" id="Seg_9534" s="T784">отец-NOM/GEN.3SG</ta>
            <ta e="T787" id="Seg_9535" s="T786">нести-PST.[3SG]</ta>
            <ta e="T788" id="Seg_9536" s="T787">дом-LAT</ta>
            <ta e="T789" id="Seg_9537" s="T788">там</ta>
            <ta e="T790" id="Seg_9538" s="T789">кто.[NOM.SG]=INDEF</ta>
            <ta e="T791" id="Seg_9539" s="T790">NEG</ta>
            <ta e="T792" id="Seg_9540" s="T791">быть-PST.[3SG]</ta>
            <ta e="T793" id="Seg_9541" s="T792">лежать-PST.[3SG]</ta>
            <ta e="T795" id="Seg_9542" s="T794">спать-INF.LAT</ta>
            <ta e="T796" id="Seg_9543" s="T795">и</ta>
            <ta e="T797" id="Seg_9544" s="T796">%%-MOM-PST-3PL</ta>
            <ta e="T799" id="Seg_9545" s="T798">родственник-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T800" id="Seg_9546" s="T799">этот-PL-LAT</ta>
            <ta e="T801" id="Seg_9547" s="T800">сказать-PST-3PL</ta>
            <ta e="T802" id="Seg_9548" s="T801">хороший.[NOM.SG]</ta>
            <ta e="T803" id="Seg_9549" s="T802">жить-NOM/GEN/ACC.2PL</ta>
            <ta e="T804" id="Seg_9550" s="T803">жить-IMP.2PL</ta>
            <ta e="T805" id="Seg_9551" s="T804">NEG.AUX-IMP.2SG</ta>
            <ta e="T806" id="Seg_9552" s="T805">бороться-CNG</ta>
            <ta e="T807" id="Seg_9553" s="T806">NEG.AUX-IMP.2SG</ta>
            <ta e="T809" id="Seg_9554" s="T808">NEG.AUX-IMP.2SG</ta>
            <ta e="T811" id="Seg_9555" s="T810">NEG.AUX-IMP.2SG</ta>
            <ta e="T812" id="Seg_9556" s="T811">ругать-DES-CNG</ta>
            <ta e="T813" id="Seg_9557" s="T812">JUSS</ta>
            <ta e="T815" id="Seg_9558" s="T814">ребенок-PL-LAT</ta>
            <ta e="T816" id="Seg_9559" s="T815">много</ta>
            <ta e="T817" id="Seg_9560" s="T816">мочь-FUT-3SG</ta>
            <ta e="T818" id="Seg_9561" s="T817">мальчик-PL</ta>
            <ta e="T819" id="Seg_9562" s="T818">дочь-PL</ta>
            <ta e="T820" id="Seg_9563" s="T819">тогда</ta>
            <ta e="T821" id="Seg_9564" s="T820">этот-PL</ta>
            <ta e="T822" id="Seg_9565" s="T821">священник.[NOM.SG]</ta>
            <ta e="T823" id="Seg_9566" s="T822">позвать-PST-3PL</ta>
            <ta e="T824" id="Seg_9567" s="T823">этот.[NOM.SG]</ta>
            <ta e="T825" id="Seg_9568" s="T824">очень</ta>
            <ta e="T826" id="Seg_9569" s="T825">много</ta>
            <ta e="T827" id="Seg_9570" s="T826">водка.[NOM.SG]</ta>
            <ta e="T828" id="Seg_9571" s="T827">пить-PST.[3SG]</ta>
            <ta e="T829" id="Seg_9572" s="T828">и</ta>
            <ta e="T830" id="Seg_9573" s="T829">упасть-MOM-PST.[3SG]</ta>
            <ta e="T831" id="Seg_9574" s="T830">этот-ACC</ta>
            <ta e="T832" id="Seg_9575" s="T831">нести-RES-PST-3PL</ta>
            <ta e="T833" id="Seg_9576" s="T832">этот-PL</ta>
            <ta e="T834" id="Seg_9577" s="T833">очень</ta>
            <ta e="T835" id="Seg_9578" s="T834">водка.[NOM.SG]</ta>
            <ta e="T836" id="Seg_9579" s="T835">пить-PST-3PL</ta>
            <ta e="T837" id="Seg_9580" s="T836">долго</ta>
            <ta e="T838" id="Seg_9581" s="T837">шесть.[NOM.SG]</ta>
            <ta e="T839" id="Seg_9582" s="T838">день.[NOM.SG]</ta>
            <ta e="T840" id="Seg_9583" s="T839">пить-PST-3PL</ta>
            <ta e="T841" id="Seg_9584" s="T840">тогда</ta>
            <ta e="T842" id="Seg_9585" s="T841">этот-PL</ta>
            <ta e="T843" id="Seg_9586" s="T842">PTCL</ta>
            <ta e="T844" id="Seg_9587" s="T843">жить-PST-3PL</ta>
            <ta e="T845" id="Seg_9588" s="T844">этот-PL</ta>
            <ta e="T846" id="Seg_9589" s="T845">PTCL</ta>
            <ta e="T847" id="Seg_9590" s="T846">ребенок-PL-LAT</ta>
            <ta e="T848" id="Seg_9591" s="T847">быть-PST-3PL</ta>
            <ta e="T849" id="Seg_9592" s="T848">мальчик-PL</ta>
            <ta e="T850" id="Seg_9593" s="T849">быть-PST-3PL</ta>
            <ta e="T852" id="Seg_9594" s="T851">быть-PST-3PL</ta>
            <ta e="T853" id="Seg_9595" s="T852">дочь-PL</ta>
            <ta e="T857" id="Seg_9596" s="T855">я.NOM</ta>
            <ta e="T858" id="Seg_9597" s="T857">сын-NOM/GEN/ACC.1SG</ta>
            <ta e="T859" id="Seg_9598" s="T858">вчера</ta>
            <ta e="T860" id="Seg_9599" s="T859">пойти-PST.[3SG]</ta>
            <ta e="T861" id="Seg_9600" s="T860">водка.[NOM.SG]</ta>
            <ta e="T862" id="Seg_9601" s="T861">пить-PST.[3SG]</ta>
            <ta e="T863" id="Seg_9602" s="T862">очень</ta>
            <ta e="T864" id="Seg_9603" s="T863">долго</ta>
            <ta e="T865" id="Seg_9604" s="T864">NEG</ta>
            <ta e="T866" id="Seg_9605" s="T865">прийти-PST.[3SG]</ta>
            <ta e="T867" id="Seg_9606" s="T866">чум-LAT</ta>
            <ta e="T868" id="Seg_9607" s="T867">тогда</ta>
            <ta e="T869" id="Seg_9608" s="T868">прийти-PST.[3SG]</ta>
            <ta e="T870" id="Seg_9609" s="T869">спать-MOM-PST.[3SG]</ta>
            <ta e="T871" id="Seg_9610" s="T870">и</ta>
            <ta e="T872" id="Seg_9611" s="T871">утро-GEN</ta>
            <ta e="T873" id="Seg_9612" s="T872">встать-PST.[3SG]</ta>
            <ta e="T874" id="Seg_9613" s="T873">и</ta>
            <ta e="T875" id="Seg_9614" s="T874">стать-MOM-PST.[3SG]</ta>
            <ta e="T876" id="Seg_9615" s="T875">работать-INF.LAT</ta>
            <ta e="T877" id="Seg_9616" s="T876">а</ta>
            <ta e="T878" id="Seg_9617" s="T877">я.LAT</ta>
            <ta e="T880" id="Seg_9618" s="T879">я.NOM</ta>
            <ta e="T881" id="Seg_9619" s="T880">два.[NOM.SG]</ta>
            <ta e="T882" id="Seg_9620" s="T881">корова.[NOM.SG]</ta>
            <ta e="T883" id="Seg_9621" s="T882">доить-PST-1SG</ta>
            <ta e="T884" id="Seg_9622" s="T883">этот-GEN</ta>
            <ta e="T885" id="Seg_9623" s="T884">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T886" id="Seg_9624" s="T885">и</ta>
            <ta e="T887" id="Seg_9625" s="T886">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T888" id="Seg_9626" s="T887">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T889" id="Seg_9627" s="T888">один.[NOM.SG]</ta>
            <ta e="T890" id="Seg_9628" s="T889">женщина.[NOM.SG]</ta>
            <ta e="T891" id="Seg_9629" s="T890">беременная.[NOM.SG]</ta>
            <ta e="T892" id="Seg_9630" s="T891">быть-PST.[3SG]</ta>
            <ta e="T893" id="Seg_9631" s="T892">тогда</ta>
            <ta e="T894" id="Seg_9632" s="T893">рожать-PST.[3SG]</ta>
            <ta e="T895" id="Seg_9633" s="T894">а</ta>
            <ta e="T896" id="Seg_9634" s="T895">этот.[NOM.SG]</ta>
            <ta e="T897" id="Seg_9635" s="T896">там</ta>
            <ta e="T898" id="Seg_9636" s="T897">пойти-PST.[3SG]</ta>
            <ta e="T899" id="Seg_9637" s="T898">и</ta>
            <ta e="T900" id="Seg_9638" s="T899">пить-PST.[3SG]</ta>
            <ta e="T901" id="Seg_9639" s="T900">водка.[NOM.SG]</ta>
            <ta e="T902" id="Seg_9640" s="T901">я.NOM</ta>
            <ta e="T903" id="Seg_9641" s="T902">вчера</ta>
            <ta e="T904" id="Seg_9642" s="T903">%%</ta>
            <ta e="T905" id="Seg_9643" s="T904">смотреть-FRQ</ta>
            <ta e="T906" id="Seg_9644" s="T905">смотреть-PST-1SG</ta>
            <ta e="T907" id="Seg_9645" s="T906">а</ta>
            <ta e="T908" id="Seg_9646" s="T907">сегодня</ta>
            <ta e="T910" id="Seg_9647" s="T908">вечер-LOC.ADV</ta>
            <ta e="T911" id="Seg_9648" s="T910">сегодня</ta>
            <ta e="T913" id="Seg_9649" s="T911">вечер-LOC.ADV</ta>
            <ta e="T914" id="Seg_9650" s="T913">тогда</ta>
            <ta e="T915" id="Seg_9651" s="T914">этот-ACC</ta>
            <ta e="T916" id="Seg_9652" s="T915">%%</ta>
            <ta e="T917" id="Seg_9653" s="T916">видеть-PST-1SG</ta>
            <ta e="T918" id="Seg_9654" s="T917">что.[NOM.SG]=INDEF</ta>
            <ta e="T919" id="Seg_9655" s="T918">принести-PST.[3SG]</ta>
            <ta e="T920" id="Seg_9656" s="T919">а</ta>
            <ta e="T921" id="Seg_9657" s="T920">я.NOM</ta>
            <ta e="T922" id="Seg_9658" s="T921">рассердиться-CVB-PST-1SG</ta>
            <ta e="T923" id="Seg_9659" s="T922">PTCL</ta>
            <ta e="T924" id="Seg_9660" s="T923">%%</ta>
            <ta e="T925" id="Seg_9661" s="T924">NEG</ta>
            <ta e="T926" id="Seg_9662" s="T925">прийти-PST-3PL</ta>
            <ta e="T927" id="Seg_9663" s="T926">лес-ABL</ta>
            <ta e="T928" id="Seg_9664" s="T927">когда</ta>
            <ta e="T929" id="Seg_9665" s="T928">этот-PL</ta>
            <ta e="T930" id="Seg_9666" s="T929">прийти-FUT-3PL</ta>
            <ta e="T931" id="Seg_9667" s="T930">сегодня</ta>
            <ta e="T932" id="Seg_9668" s="T931">прийти-FUT-3PL</ta>
            <ta e="T933" id="Seg_9669" s="T932">вечер-LAT/LOC.3SG</ta>
            <ta e="T934" id="Seg_9670" s="T933">есть-INF.LAT</ta>
            <ta e="T935" id="Seg_9671" s="T934">надо</ta>
            <ta e="T936" id="Seg_9672" s="T935">этот-PL-LAT</ta>
            <ta e="T937" id="Seg_9673" s="T936">тогда</ta>
            <ta e="T938" id="Seg_9674" s="T937">дать-INF.LAT</ta>
            <ta e="T939" id="Seg_9675" s="T938">этот-PL</ta>
            <ta e="T940" id="Seg_9676" s="T939">PTCL</ta>
            <ta e="T941" id="Seg_9677" s="T940">быть.голодным-PRS-3PL</ta>
            <ta e="T942" id="Seg_9678" s="T941">я.NOM</ta>
            <ta e="T943" id="Seg_9679" s="T942">сегодня</ta>
            <ta e="T944" id="Seg_9680" s="T943">думать-PST-1SG</ta>
            <ta e="T945" id="Seg_9681" s="T944">вы.NOM</ta>
            <ta e="T946" id="Seg_9682" s="T945">PTCL</ta>
            <ta e="T947" id="Seg_9683" s="T946">прийти-PST-2PL</ta>
            <ta e="T948" id="Seg_9684" s="T947">а</ta>
            <ta e="T949" id="Seg_9685" s="T948">вы.NOM</ta>
            <ta e="T950" id="Seg_9686" s="T949">NEG</ta>
            <ta e="T951" id="Seg_9687" s="T950">прийти-PST-2PL</ta>
            <ta e="T952" id="Seg_9688" s="T951">я.NOM</ta>
            <ta e="T953" id="Seg_9689" s="T952">сегодня</ta>
            <ta e="T955" id="Seg_9690" s="T954">сегодня</ta>
            <ta e="T956" id="Seg_9691" s="T955">яйцо-PL</ta>
            <ta e="T957" id="Seg_9692" s="T956">кипятить-PST-1SG</ta>
            <ta e="T958" id="Seg_9693" s="T957">черемша.[NOM.SG]</ta>
            <ta e="T959" id="Seg_9694" s="T958">зарезать-PST-1SG</ta>
            <ta e="T960" id="Seg_9695" s="T959">яйцо-PL</ta>
            <ta e="T961" id="Seg_9696" s="T960">класть-PST-1SG</ta>
            <ta e="T962" id="Seg_9697" s="T961">%%</ta>
            <ta e="T963" id="Seg_9698" s="T962">класть-PST-1SG</ta>
            <ta e="T964" id="Seg_9699" s="T963">вы.ACC</ta>
            <ta e="T965" id="Seg_9700" s="T964">кормить-INF.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T1" id="Seg_9701" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_9702" s="T1">v-v:n.fin</ta>
            <ta e="T4" id="Seg_9703" s="T3">num-n:case</ta>
            <ta e="T5" id="Seg_9704" s="T4">adj-n:case</ta>
            <ta e="T6" id="Seg_9705" s="T5">n-n:case</ta>
            <ta e="T8" id="Seg_9706" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_9707" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_9708" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_9709" s="T10">adv</ta>
            <ta e="T12" id="Seg_9710" s="T11">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_9711" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_9712" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_9713" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_9714" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_9715" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_9716" s="T17">adv</ta>
            <ta e="T19" id="Seg_9717" s="T18">dempro-n:case</ta>
            <ta e="T20" id="Seg_9718" s="T19">n-n:case.poss</ta>
            <ta e="T21" id="Seg_9719" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_9720" s="T21">adv</ta>
            <ta e="T23" id="Seg_9721" s="T22">dempro-n:case</ta>
            <ta e="T24" id="Seg_9722" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_9723" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_9724" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_9725" s="T26">pers</ta>
            <ta e="T28" id="Seg_9726" s="T27">dempro-n:case</ta>
            <ta e="T29" id="Seg_9727" s="T28">adv</ta>
            <ta e="T31" id="Seg_9728" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_9729" s="T31">conj</ta>
            <ta e="T33" id="Seg_9730" s="T32">adv</ta>
            <ta e="T34" id="Seg_9731" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_9732" s="T34">pers</ta>
            <ta e="T36" id="Seg_9733" s="T35">que=ptcl</ta>
            <ta e="T37" id="Seg_9734" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_9735" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_9736" s="T38">pers</ta>
            <ta e="T40" id="Seg_9737" s="T39">adv</ta>
            <ta e="T41" id="Seg_9738" s="T40">adv</ta>
            <ta e="T42" id="Seg_9739" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_9740" s="T42">adj-n:case</ta>
            <ta e="T44" id="Seg_9741" s="T43">v-v&gt;v-v:mood.pn</ta>
            <ta e="T45" id="Seg_9742" s="T44">dempro-n:case</ta>
            <ta e="T46" id="Seg_9743" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_9744" s="T46">que</ta>
            <ta e="T48" id="Seg_9745" s="T47">v-v:n.fin</ta>
            <ta e="T49" id="Seg_9746" s="T48">conj</ta>
            <ta e="T50" id="Seg_9747" s="T49">que</ta>
            <ta e="T51" id="Seg_9748" s="T50">v-v:n.fin</ta>
            <ta e="T52" id="Seg_9749" s="T51">adv</ta>
            <ta e="T53" id="Seg_9750" s="T52">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T54" id="Seg_9751" s="T53">adv</ta>
            <ta e="T55" id="Seg_9752" s="T54">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T56" id="Seg_9753" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_9754" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9755" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_9756" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_9757" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_9758" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_9759" s="T61">v-v:n.fin</ta>
            <ta e="T63" id="Seg_9760" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_9761" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_9762" s="T64">v-v:n.fin</ta>
            <ta e="T66" id="Seg_9763" s="T65">adv</ta>
            <ta e="T67" id="Seg_9764" s="T66">adv</ta>
            <ta e="T68" id="Seg_9765" s="T67">v-v:n.fin</ta>
            <ta e="T69" id="Seg_9766" s="T68">pers</ta>
            <ta e="T70" id="Seg_9767" s="T69">n-n:case.poss</ta>
            <ta e="T71" id="Seg_9768" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_9769" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_9770" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_9771" s="T73">adv</ta>
            <ta e="T75" id="Seg_9772" s="T74">%%</ta>
            <ta e="T967" id="Seg_9773" s="T75">v-v:n-fin</ta>
            <ta e="T76" id="Seg_9774" s="T967">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_9775" s="T77">n-n:case.poss</ta>
            <ta e="T79" id="Seg_9776" s="T78">pers</ta>
            <ta e="T80" id="Seg_9777" s="T79">adj-n:case</ta>
            <ta e="T81" id="Seg_9778" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_9779" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_9780" s="T82">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_9781" s="T84">pers</ta>
            <ta e="T86" id="Seg_9782" s="T85">num-n:case</ta>
            <ta e="T87" id="Seg_9783" s="T86">num-n:case</ta>
            <ta e="T90" id="Seg_9784" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_9785" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_9786" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_9787" s="T92">adv</ta>
            <ta e="T94" id="Seg_9788" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_9789" s="T94">pers</ta>
            <ta e="T96" id="Seg_9790" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_9791" s="T96">adv</ta>
            <ta e="T98" id="Seg_9792" s="T97">dempro-n:case</ta>
            <ta e="T100" id="Seg_9793" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_9794" s="T100">pers-n:case</ta>
            <ta e="T102" id="Seg_9795" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_9796" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_9797" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_9798" s="T104">n-n:case.poss</ta>
            <ta e="T106" id="Seg_9799" s="T105">adv</ta>
            <ta e="T108" id="Seg_9800" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_9801" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_9802" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_9803" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_9804" s="T112">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_9805" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_9806" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_9807" s="T115">v-v&gt;v-v:pn</ta>
            <ta e="T117" id="Seg_9808" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_9809" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_9810" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_9811" s="T119">v-v&gt;v-v:pn</ta>
            <ta e="T121" id="Seg_9812" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_9813" s="T121">conj</ta>
            <ta e="T123" id="Seg_9814" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_9815" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_9816" s="T124">num-n:case</ta>
            <ta e="T126" id="Seg_9817" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_9818" s="T126">num-n:case</ta>
            <ta e="T128" id="Seg_9819" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_9820" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_9821" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_9822" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_9823" s="T131">quant</ta>
            <ta e="T133" id="Seg_9824" s="T132">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_9825" s="T133">v</ta>
            <ta e="T137" id="Seg_9826" s="T136">v-v:n.fin</ta>
            <ta e="T138" id="Seg_9827" s="T137">conj</ta>
            <ta e="T139" id="Seg_9828" s="T138">dempro-n:case</ta>
            <ta e="T140" id="Seg_9829" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_9830" s="T140">conj</ta>
            <ta e="T142" id="Seg_9831" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_9832" s="T142">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_9833" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_9834" s="T145">dempro-n:case</ta>
            <ta e="T147" id="Seg_9835" s="T146">dempro-n:case</ta>
            <ta e="T148" id="Seg_9836" s="T147">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_9837" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_9838" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_9839" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_9840" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_9841" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_9842" s="T153">conj</ta>
            <ta e="T155" id="Seg_9843" s="T154">dempro-n:case</ta>
            <ta e="T156" id="Seg_9844" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_9845" s="T156">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_9846" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_9847" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_9848" s="T159">adv</ta>
            <ta e="T161" id="Seg_9849" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_9850" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_9851" s="T162">adv</ta>
            <ta e="T165" id="Seg_9852" s="T164">n-n:case</ta>
            <ta e="T968" id="Seg_9853" s="T165">v-v:n-fin</ta>
            <ta e="T166" id="Seg_9854" s="T968">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_9855" s="T166">adv</ta>
            <ta e="T168" id="Seg_9856" s="T167">pers</ta>
            <ta e="T169" id="Seg_9857" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_9858" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_9859" s="T170">conj</ta>
            <ta e="T172" id="Seg_9860" s="T171">pers-n:case</ta>
            <ta e="T173" id="Seg_9861" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_9862" s="T173">conj</ta>
            <ta e="T175" id="Seg_9863" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_9864" s="T175">adj</ta>
            <ta e="T177" id="Seg_9865" s="T176">conj</ta>
            <ta e="T178" id="Seg_9866" s="T177">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_9867" s="T178">adv</ta>
            <ta e="T180" id="Seg_9868" s="T179">adv</ta>
            <ta e="T181" id="Seg_9869" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_9870" s="T181">adv</ta>
            <ta e="T183" id="Seg_9871" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_9872" s="T183">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_9873" s="T185">conj</ta>
            <ta e="T187" id="Seg_9874" s="T186">pers</ta>
            <ta e="T189" id="Seg_9875" s="T188">n-n:num</ta>
            <ta e="T190" id="Seg_9876" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_9877" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_9878" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_9879" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_9880" s="T193">pers</ta>
            <ta e="T195" id="Seg_9881" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_9882" s="T195">adj-n:case</ta>
            <ta e="T197" id="Seg_9883" s="T196">adv</ta>
            <ta e="T198" id="Seg_9884" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_9885" s="T198">pers</ta>
            <ta e="T200" id="Seg_9886" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_9887" s="T200">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_9888" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_9889" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_9890" s="T203">v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_9891" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_9892" s="T205">pers</ta>
            <ta e="T207" id="Seg_9893" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_9894" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_9895" s="T208">n-n:case</ta>
            <ta e="T211" id="Seg_9896" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_9897" s="T211">conj</ta>
            <ta e="T213" id="Seg_9898" s="T212">adv</ta>
            <ta e="T214" id="Seg_9899" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_9900" s="T214">pers</ta>
            <ta e="T216" id="Seg_9901" s="T215">n-n:case.poss</ta>
            <ta e="T217" id="Seg_9902" s="T216">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_9903" s="T217">pers</ta>
            <ta e="T219" id="Seg_9904" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_9905" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_9906" s="T220">conj</ta>
            <ta e="T222" id="Seg_9907" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_9908" s="T222">v-v&gt;v-v:pn</ta>
            <ta e="T224" id="Seg_9909" s="T223">conj</ta>
            <ta e="T225" id="Seg_9910" s="T224">pers</ta>
            <ta e="T226" id="Seg_9911" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_9912" s="T226">v-v:mood.pn</ta>
            <ta e="T228" id="Seg_9913" s="T227">que</ta>
            <ta e="T229" id="Seg_9914" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_9915" s="T229">que</ta>
            <ta e="T231" id="Seg_9916" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_9917" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_9918" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_9919" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_9920" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_9921" s="T235">v-v:pn</ta>
            <ta e="T237" id="Seg_9922" s="T236">n-n&gt;adj</ta>
            <ta e="T238" id="Seg_9923" s="T237">v-v:n.fin</ta>
            <ta e="T239" id="Seg_9924" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_9925" s="T239">adj-n:case</ta>
            <ta e="T241" id="Seg_9926" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_9927" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_9928" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_9929" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_9930" s="T244">adj-n:case</ta>
            <ta e="T246" id="Seg_9931" s="T245">v-v:n.fin</ta>
            <ta e="T247" id="Seg_9932" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_9933" s="T247">que=ptcl</ta>
            <ta e="T249" id="Seg_9934" s="T248">v-v:tense-v:pn</ta>
            <ta e="T250" id="Seg_9935" s="T249">n-n:num</ta>
            <ta e="T251" id="Seg_9936" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_9937" s="T251">v-v:n.fin</ta>
            <ta e="T253" id="Seg_9938" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_9939" s="T253">v-v:n.fin</ta>
            <ta e="T255" id="Seg_9940" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_9941" s="T255">v-v:n.fin</ta>
            <ta e="T257" id="Seg_9942" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_9943" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_9944" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_9945" s="T259">adv</ta>
            <ta e="T261" id="Seg_9946" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_9947" s="T261">v-v:pn</ta>
            <ta e="T263" id="Seg_9948" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_9949" s="T263">que-n:case=ptcl</ta>
            <ta e="T265" id="Seg_9950" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9951" s="T265">v-v:tense-v:pn</ta>
            <ta e="T267" id="Seg_9952" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_9953" s="T267">v-v:n.fin</ta>
            <ta e="T269" id="Seg_9954" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_9955" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_9956" s="T270">ptcl</ta>
            <ta e="T273" id="Seg_9957" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_9958" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_9959" s="T274">aux-v:mood.pn</ta>
            <ta e="T276" id="Seg_9960" s="T275">v-v:mood.pn</ta>
            <ta e="T277" id="Seg_9961" s="T276">n-n:case-n:case.poss</ta>
            <ta e="T278" id="Seg_9962" s="T277">v-v:ins-v:mood.pn</ta>
            <ta e="T279" id="Seg_9963" s="T278">n-n:case.poss</ta>
            <ta e="T280" id="Seg_9964" s="T279">v-v:mood.pn</ta>
            <ta e="T281" id="Seg_9965" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_9966" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_9967" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_9968" s="T283">refl-n:case.poss</ta>
            <ta e="T285" id="Seg_9969" s="T284">aux-v:mood.pn</ta>
            <ta e="T286" id="Seg_9970" s="T285">v-v:n.fin</ta>
            <ta e="T287" id="Seg_9971" s="T286">v-v:mood.pn</ta>
            <ta e="T288" id="Seg_9972" s="T287">adj-adj&gt;adv</ta>
            <ta e="T289" id="Seg_9973" s="T288">conj</ta>
            <ta e="T290" id="Seg_9974" s="T289">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_9975" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_9976" s="T291">adj-n:case</ta>
            <ta e="T293" id="Seg_9977" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_9978" s="T293">adv</ta>
            <ta e="T295" id="Seg_9979" s="T294">n-n:case.poss-n:case</ta>
            <ta e="T296" id="Seg_9980" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_9981" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_9982" s="T297">v-v:n.fin</ta>
            <ta e="T299" id="Seg_9983" s="T298">adj-n:case</ta>
            <ta e="T300" id="Seg_9984" s="T299">adv</ta>
            <ta e="T301" id="Seg_9985" s="T300">adv</ta>
            <ta e="T302" id="Seg_9986" s="T301">v-v:ins-v:mood.pn</ta>
            <ta e="T303" id="Seg_9987" s="T302">adv</ta>
            <ta e="T304" id="Seg_9988" s="T303">aux-v:mood.pn</ta>
            <ta e="T305" id="Seg_9989" s="T304">v-v:ins-v:n.fin</ta>
            <ta e="T306" id="Seg_9990" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_9991" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_9992" s="T307">dempro-n:case</ta>
            <ta e="T309" id="Seg_9993" s="T308">refl-n:case.poss</ta>
            <ta e="T310" id="Seg_9994" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_9995" s="T310">pers</ta>
            <ta e="T312" id="Seg_9996" s="T311">refl-n:case.poss</ta>
            <ta e="T313" id="Seg_9997" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_9998" s="T313">v-v&gt;v-v:pn</ta>
            <ta e="T315" id="Seg_9999" s="T314">conj</ta>
            <ta e="T316" id="Seg_10000" s="T315">pers</ta>
            <ta e="T317" id="Seg_10001" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_10002" s="T317">v-v:tense-v:pn</ta>
            <ta e="T319" id="Seg_10003" s="T318">n-n:case</ta>
            <ta e="T321" id="Seg_10004" s="T320">n-n:num</ta>
            <ta e="T322" id="Seg_10005" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_10006" s="T322">v-v&gt;v-v:pn</ta>
            <ta e="T324" id="Seg_10007" s="T323">adj-n:case</ta>
            <ta e="T325" id="Seg_10008" s="T324">n-n:num</ta>
            <ta e="T326" id="Seg_10009" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_10010" s="T326">adv</ta>
            <ta e="T328" id="Seg_10011" s="T327">adv</ta>
            <ta e="T329" id="Seg_10012" s="T328">v-v:tense-v:pn</ta>
            <ta e="T330" id="Seg_10013" s="T329">adv</ta>
            <ta e="T331" id="Seg_10014" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_10015" s="T331">adj-n:case</ta>
            <ta e="T333" id="Seg_10016" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_10017" s="T333">que-n:case=ptcl</ta>
            <ta e="T335" id="Seg_10018" s="T334">v-v&gt;v-v:pn</ta>
            <ta e="T336" id="Seg_10019" s="T335">n-n:num</ta>
            <ta e="T337" id="Seg_10020" s="T336">v-v:tense-v:pn</ta>
            <ta e="T338" id="Seg_10021" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_10022" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_10023" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_10024" s="T340">v-v&gt;v-v:pn</ta>
            <ta e="T343" id="Seg_10025" s="T342">n-n:num-n:case.poss</ta>
            <ta e="T347" id="Seg_10026" s="T346">dempro-n:case</ta>
            <ta e="T348" id="Seg_10027" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_10028" s="T348">ptcl</ta>
            <ta e="T351" id="Seg_10029" s="T350">dempro-n:case</ta>
            <ta e="T353" id="Seg_10030" s="T352">v-v:n.fin</ta>
            <ta e="T354" id="Seg_10031" s="T353">dempro-n:case</ta>
            <ta e="T355" id="Seg_10032" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_10033" s="T355">adv</ta>
            <ta e="T357" id="Seg_10034" s="T356">v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_10035" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_10036" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_10037" s="T359">dempro-n:num</ta>
            <ta e="T361" id="Seg_10038" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_10039" s="T361">dempro-n:case</ta>
            <ta e="T363" id="Seg_10040" s="T362">v-v&gt;v-v:pn</ta>
            <ta e="T364" id="Seg_10041" s="T363">dempro-n:num</ta>
            <ta e="T365" id="Seg_10042" s="T364">dempro-n:num</ta>
            <ta e="T366" id="Seg_10043" s="T365">v-v&gt;v-v:pn</ta>
            <ta e="T367" id="Seg_10044" s="T366">adv</ta>
            <ta e="T368" id="Seg_10045" s="T367">pers</ta>
            <ta e="T369" id="Seg_10046" s="T368">adj-n:case</ta>
            <ta e="T370" id="Seg_10047" s="T369">v-v:tense</ta>
            <ta e="T371" id="Seg_10048" s="T370">conj</ta>
            <ta e="T372" id="Seg_10049" s="T371">que-n:case</ta>
            <ta e="T373" id="Seg_10050" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_10051" s="T373">%%</ta>
            <ta e="T375" id="Seg_10052" s="T374">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_10053" s="T375">conj</ta>
            <ta e="T377" id="Seg_10054" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_10055" s="T377">pers</ta>
            <ta e="T379" id="Seg_10056" s="T378">v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_10057" s="T379">n-n:num</ta>
            <ta e="T381" id="Seg_10058" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_10059" s="T381">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T383" id="Seg_10060" s="T382">num-n:case</ta>
            <ta e="T384" id="Seg_10061" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_10062" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_10063" s="T385">num-n:case</ta>
            <ta e="T387" id="Seg_10064" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_10065" s="T387">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_10066" s="T388">pers</ta>
            <ta e="T390" id="Seg_10067" s="T389">n-n:case.poss</ta>
            <ta e="T391" id="Seg_10068" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_10069" s="T391">conj</ta>
            <ta e="T393" id="Seg_10070" s="T392">que</ta>
            <ta e="T394" id="Seg_10071" s="T393">v-v:tense-v:pn</ta>
            <ta e="T395" id="Seg_10072" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_10073" s="T395">v-v:n.fin</ta>
            <ta e="T397" id="Seg_10074" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_10075" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_10076" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_10077" s="T399">v-v:tense-v:pn</ta>
            <ta e="T401" id="Seg_10078" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_10079" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_10080" s="T402">v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_10081" s="T403">v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_10082" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_10083" s="T405">v-v:n.fin</ta>
            <ta e="T407" id="Seg_10084" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_10085" s="T407">dempro</ta>
            <ta e="T409" id="Seg_10086" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_10087" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_10088" s="T410">adj-n:case</ta>
            <ta e="T412" id="Seg_10089" s="T411">adv</ta>
            <ta e="T413" id="Seg_10090" s="T412">%%</ta>
            <ta e="T414" id="Seg_10091" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_10092" s="T414">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_10093" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_10094" s="T416">dempro-n:case</ta>
            <ta e="T418" id="Seg_10095" s="T417">v-v&gt;v-v:n.fin</ta>
            <ta e="T419" id="Seg_10096" s="T418">v-v:n.fin</ta>
            <ta e="T420" id="Seg_10097" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_10098" s="T420">que=ptcl</ta>
            <ta e="T422" id="Seg_10099" s="T421">v-v:n.fin</ta>
            <ta e="T423" id="Seg_10100" s="T422">v-v:n.fin</ta>
            <ta e="T424" id="Seg_10101" s="T423">que</ta>
            <ta e="T426" id="Seg_10102" s="T424">n-n:case</ta>
            <ta e="T428" id="Seg_10103" s="T427">que</ta>
            <ta e="T429" id="Seg_10104" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_10105" s="T429">v-v&gt;v-v:pn</ta>
            <ta e="T431" id="Seg_10106" s="T430">conj</ta>
            <ta e="T432" id="Seg_10107" s="T431">pers</ta>
            <ta e="T433" id="Seg_10108" s="T432">v-v&gt;v-v:pn</ta>
            <ta e="T434" id="Seg_10109" s="T433">que</ta>
            <ta e="T435" id="Seg_10110" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_10111" s="T435">v-v:tense-v:pn</ta>
            <ta e="T437" id="Seg_10112" s="T436">pers</ta>
            <ta e="T438" id="Seg_10113" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_10114" s="T438">dempro</ta>
            <ta e="T440" id="Seg_10115" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_10116" s="T440">adv</ta>
            <ta e="T442" id="Seg_10117" s="T441">adj-n:case</ta>
            <ta e="T443" id="Seg_10118" s="T442">v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_10119" s="T443">adv</ta>
            <ta e="T445" id="Seg_10120" s="T444">adj-n:case</ta>
            <ta e="T446" id="Seg_10121" s="T445">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_10122" s="T446">n-n:num</ta>
            <ta e="T448" id="Seg_10123" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_10124" s="T448">adv</ta>
            <ta e="T453" id="Seg_10125" s="T452">n-n:case.poss</ta>
            <ta e="T454" id="Seg_10126" s="T453">conj</ta>
            <ta e="T455" id="Seg_10127" s="T454">dempro-n:case</ta>
            <ta e="T456" id="Seg_10128" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_10129" s="T456">v-v:tense-v:pn</ta>
            <ta e="T458" id="Seg_10130" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_10131" s="T458">adv</ta>
            <ta e="T460" id="Seg_10132" s="T459">adj-n:case</ta>
            <ta e="T461" id="Seg_10133" s="T460">n-n:num</ta>
            <ta e="T462" id="Seg_10134" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_10135" s="T462">adj-n:num</ta>
            <ta e="T464" id="Seg_10136" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_10137" s="T464">pers</ta>
            <ta e="T466" id="Seg_10138" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_10139" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_10140" s="T467">v-v:n.fin</ta>
            <ta e="T469" id="Seg_10141" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_10142" s="T469">n</ta>
            <ta e="T471" id="Seg_10143" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_10144" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_10145" s="T472">v-v:pn</ta>
            <ta e="T474" id="Seg_10146" s="T473">conj</ta>
            <ta e="T475" id="Seg_10147" s="T474">dempro-n:case</ta>
            <ta e="T476" id="Seg_10148" s="T475">adj-n:case</ta>
            <ta e="T477" id="Seg_10149" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_10150" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_10151" s="T478">v-v:tense-v:pn</ta>
            <ta e="T482" id="Seg_10152" s="T481">v-v:mood.pn</ta>
            <ta e="T483" id="Seg_10153" s="T482">refl-n:case.poss</ta>
            <ta e="T484" id="Seg_10154" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_10155" s="T484">dempro-n:case</ta>
            <ta e="T486" id="Seg_10156" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_10157" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_10158" s="T487">v-v:tense-v:pn</ta>
            <ta e="T489" id="Seg_10159" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_10160" s="T489">dempro-n:case</ta>
            <ta e="T491" id="Seg_10161" s="T490">v-v:n.fin</ta>
            <ta e="T492" id="Seg_10162" s="T491">n-n:case</ta>
            <ta e="T493" id="Seg_10163" s="T492">n-n:case</ta>
            <ta e="T494" id="Seg_10164" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_10165" s="T494">v-v:tense-v:pn</ta>
            <ta e="T496" id="Seg_10166" s="T495">adv</ta>
            <ta e="T497" id="Seg_10167" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_10168" s="T497">v-v:tense-v:pn</ta>
            <ta e="T500" id="Seg_10169" s="T499">pers</ta>
            <ta e="T501" id="Seg_10170" s="T500">v-v:tense-v:pn</ta>
            <ta e="T502" id="Seg_10171" s="T501">n-n:case</ta>
            <ta e="T503" id="Seg_10172" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_10173" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_10174" s="T504">pers</ta>
            <ta e="T506" id="Seg_10175" s="T505">n-n:case.poss</ta>
            <ta e="T507" id="Seg_10176" s="T506">n-n:case.poss</ta>
            <ta e="T508" id="Seg_10177" s="T507">v-v:tense-v:pn</ta>
            <ta e="T509" id="Seg_10178" s="T508">pers</ta>
            <ta e="T510" id="Seg_10179" s="T509">adv</ta>
            <ta e="T511" id="Seg_10180" s="T510">n-n:case.poss</ta>
            <ta e="T512" id="Seg_10181" s="T511">n-n:case.poss</ta>
            <ta e="T513" id="Seg_10182" s="T512">n-n:case.poss</ta>
            <ta e="T514" id="Seg_10183" s="T513">v-v:tense-v:pn</ta>
            <ta e="T515" id="Seg_10184" s="T514">dempro-n:num</ta>
            <ta e="T516" id="Seg_10185" s="T515">v-v:tense-v:pn</ta>
            <ta e="T517" id="Seg_10186" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_10187" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_10188" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_10189" s="T519">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T521" id="Seg_10190" s="T520">n-n:case.poss</ta>
            <ta e="T523" id="Seg_10191" s="T522">v-v:tense-v:pn</ta>
            <ta e="T524" id="Seg_10192" s="T523">pers</ta>
            <ta e="T526" id="Seg_10193" s="T525">pers</ta>
            <ta e="T527" id="Seg_10194" s="T526">n-n:case</ta>
            <ta e="T529" id="Seg_10195" s="T528">v-v:tense-v:pn</ta>
            <ta e="T531" id="Seg_10196" s="T530">adv</ta>
            <ta e="T532" id="Seg_10197" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_10198" s="T532">v-v:tense-v:pn</ta>
            <ta e="T534" id="Seg_10199" s="T533">pers</ta>
            <ta e="T535" id="Seg_10200" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_10201" s="T535">pers</ta>
            <ta e="T537" id="Seg_10202" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_10203" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_10204" s="T538">v-v:tense-v:pn</ta>
            <ta e="T540" id="Seg_10205" s="T539">adv</ta>
            <ta e="T541" id="Seg_10206" s="T540">v-v:tense-v:pn</ta>
            <ta e="T542" id="Seg_10207" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_10208" s="T542">v-v:tense-v:pn</ta>
            <ta e="T544" id="Seg_10209" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_10210" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_10211" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_10212" s="T546">quant</ta>
            <ta e="T548" id="Seg_10213" s="T547">v-v:tense-v:pn</ta>
            <ta e="T549" id="Seg_10214" s="T548">adv</ta>
            <ta e="T550" id="Seg_10215" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_10216" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_10217" s="T551">n-n:case</ta>
            <ta e="T553" id="Seg_10218" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_10219" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_10220" s="T554">adv</ta>
            <ta e="T558" id="Seg_10221" s="T557">n-n:case</ta>
            <ta e="T559" id="Seg_10222" s="T558">v-v:tense-v:pn</ta>
            <ta e="T560" id="Seg_10223" s="T559">dempro-n:num</ta>
            <ta e="T561" id="Seg_10224" s="T560">n-n:num</ta>
            <ta e="T562" id="Seg_10225" s="T561">v-v:tense-v:pn</ta>
            <ta e="T563" id="Seg_10226" s="T562">conj</ta>
            <ta e="T564" id="Seg_10227" s="T563">n-n:case</ta>
            <ta e="T565" id="Seg_10228" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_10229" s="T565">adv</ta>
            <ta e="T567" id="Seg_10230" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_10231" s="T567">dempro-n:case</ta>
            <ta e="T569" id="Seg_10232" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_10233" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_10234" s="T570">adv</ta>
            <ta e="T574" id="Seg_10235" s="T573">n-n:num</ta>
            <ta e="T575" id="Seg_10236" s="T574">v-v:tense-v:pn</ta>
            <ta e="T576" id="Seg_10237" s="T575">adv</ta>
            <ta e="T577" id="Seg_10238" s="T576">n-n:case.poss</ta>
            <ta e="T578" id="Seg_10239" s="T577">v-v:tense-v:pn</ta>
            <ta e="T579" id="Seg_10240" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_10241" s="T579">ptcl</ta>
            <ta e="T581" id="Seg_10242" s="T580">n-n:case</ta>
            <ta e="T582" id="Seg_10243" s="T581">v-v:tense-v:pn</ta>
            <ta e="T583" id="Seg_10244" s="T582">n-n:case</ta>
            <ta e="T584" id="Seg_10245" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_10246" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_10247" s="T585">conj</ta>
            <ta e="T587" id="Seg_10248" s="T586">n-n:case</ta>
            <ta e="T588" id="Seg_10249" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_10250" s="T588">v-v:tense-v:pn</ta>
            <ta e="T590" id="Seg_10251" s="T589">quant</ta>
            <ta e="T591" id="Seg_10252" s="T590">n-n:case</ta>
            <ta e="T592" id="Seg_10253" s="T591">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_10254" s="T592">n-n:case</ta>
            <ta e="T594" id="Seg_10255" s="T593">quant</ta>
            <ta e="T595" id="Seg_10256" s="T594">v-v:tense-v:pn</ta>
            <ta e="T596" id="Seg_10257" s="T595">n-n:case</ta>
            <ta e="T597" id="Seg_10258" s="T596">adv</ta>
            <ta e="T598" id="Seg_10259" s="T597">quant</ta>
            <ta e="T599" id="Seg_10260" s="T598">que-n:case</ta>
            <ta e="T601" id="Seg_10261" s="T600">v-v&gt;v-v:pn</ta>
            <ta e="T602" id="Seg_10262" s="T601">que</ta>
            <ta e="T603" id="Seg_10263" s="T602">n-n:case</ta>
            <ta e="T604" id="Seg_10264" s="T603">v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_10265" s="T604">n-n:case.poss</ta>
            <ta e="T606" id="Seg_10266" s="T605">n-n:case.poss</ta>
            <ta e="T607" id="Seg_10267" s="T606">v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_10268" s="T607">dempro-n:case</ta>
            <ta e="T609" id="Seg_10269" s="T608">n-n:case.poss</ta>
            <ta e="T610" id="Seg_10270" s="T609">v-v:pn</ta>
            <ta e="T611" id="Seg_10271" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_10272" s="T611">n-n:case.poss</ta>
            <ta e="T613" id="Seg_10273" s="T612">v-v:n.fin</ta>
            <ta e="T614" id="Seg_10274" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_10275" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_10276" s="T615">v-v:n.fin</ta>
            <ta e="T617" id="Seg_10277" s="T616">n-n:case</ta>
            <ta e="T618" id="Seg_10278" s="T617">v-v:n.fin</ta>
            <ta e="T619" id="Seg_10279" s="T618">n-n:case.poss</ta>
            <ta e="T620" id="Seg_10280" s="T619">adv</ta>
            <ta e="T621" id="Seg_10281" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_10282" s="T621">v-v:pn</ta>
            <ta e="T623" id="Seg_10283" s="T622">adv</ta>
            <ta e="T625" id="Seg_10284" s="T624">dempro-n:num</ta>
            <ta e="T626" id="Seg_10285" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_10286" s="T626">v-v:tense-v:pn</ta>
            <ta e="T628" id="Seg_10287" s="T627">num-n:case</ta>
            <ta e="T629" id="Seg_10288" s="T628">num-n:case</ta>
            <ta e="T630" id="Seg_10289" s="T629">num-n:case</ta>
            <ta e="T631" id="Seg_10290" s="T630">adv</ta>
            <ta e="T632" id="Seg_10291" s="T631">n-n:case.poss</ta>
            <ta e="T633" id="Seg_10292" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_10293" s="T633">dempro-n:case</ta>
            <ta e="T635" id="Seg_10294" s="T634">v-v:tense-v:pn</ta>
            <ta e="T636" id="Seg_10295" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_10296" s="T636">n-n:num-n:case.poss</ta>
            <ta e="T638" id="Seg_10297" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_10298" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_10299" s="T639">v-v:tense-v:pn</ta>
            <ta e="T641" id="Seg_10300" s="T640">n-n:num</ta>
            <ta e="T642" id="Seg_10301" s="T641">n-n:case</ta>
            <ta e="T643" id="Seg_10302" s="T642">v-v:tense-v:pn</ta>
            <ta e="T644" id="Seg_10303" s="T643">adv</ta>
            <ta e="T645" id="Seg_10304" s="T644">quant</ta>
            <ta e="T647" id="Seg_10305" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_10306" s="T647">quant</ta>
            <ta e="T649" id="Seg_10307" s="T648">v-v:tense-v:pn</ta>
            <ta e="T650" id="Seg_10308" s="T649">adv</ta>
            <ta e="T651" id="Seg_10309" s="T650">dempro-n:num</ta>
            <ta e="T652" id="Seg_10310" s="T651">adv</ta>
            <ta e="T653" id="Seg_10311" s="T652">v-v:tense-v:pn</ta>
            <ta e="T654" id="Seg_10312" s="T653">n-n:case.poss</ta>
            <ta e="T657" id="Seg_10313" s="T656">conj</ta>
            <ta e="T658" id="Seg_10314" s="T657">adv</ta>
            <ta e="T659" id="Seg_10315" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_10316" s="T659">v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_10317" s="T660">adv</ta>
            <ta e="T662" id="Seg_10318" s="T661">dempro-n:case</ta>
            <ta e="T969" id="Seg_10319" s="T662">v-v:n-fin</ta>
            <ta e="T663" id="Seg_10320" s="T969">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_10321" s="T663">dempro-n:case</ta>
            <ta e="T665" id="Seg_10322" s="T664">n-n:case.poss</ta>
            <ta e="T666" id="Seg_10323" s="T665">v-v:tense-v:pn</ta>
            <ta e="T667" id="Seg_10324" s="T666">quant</ta>
            <ta e="T668" id="Seg_10325" s="T667">quant</ta>
            <ta e="T669" id="Seg_10326" s="T668">que-n:case</ta>
            <ta e="T670" id="Seg_10327" s="T669">v-v:tense-v:pn</ta>
            <ta e="T671" id="Seg_10328" s="T670">n-n:case</ta>
            <ta e="T672" id="Seg_10329" s="T671">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10330" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_10331" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_10332" s="T674">n-n:num</ta>
            <ta e="T676" id="Seg_10333" s="T675">v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_10334" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_10335" s="T677">v-v:tense-v:pn</ta>
            <ta e="T680" id="Seg_10336" s="T679">n-n:case</ta>
            <ta e="T681" id="Seg_10337" s="T680">v-v:tense-v:pn</ta>
            <ta e="T682" id="Seg_10338" s="T681">dempro-n:num</ta>
            <ta e="T685" id="Seg_10339" s="T684">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_10340" s="T686">n-n:case.poss-n:case</ta>
            <ta e="T688" id="Seg_10341" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_10342" s="T688">refl-n:case.poss</ta>
            <ta e="T690" id="Seg_10343" s="T689">n</ta>
            <ta e="T691" id="Seg_10344" s="T690">n-n:case.poss</ta>
            <ta e="T692" id="Seg_10345" s="T691">conj</ta>
            <ta e="T693" id="Seg_10346" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_10347" s="T693">refl-n:case.poss</ta>
            <ta e="T695" id="Seg_10348" s="T694">n</ta>
            <ta e="T696" id="Seg_10349" s="T695">n-n:case.poss</ta>
            <ta e="T697" id="Seg_10350" s="T696">n</ta>
            <ta e="T698" id="Seg_10351" s="T697">n-n:case.poss</ta>
            <ta e="T699" id="Seg_10352" s="T698">adv</ta>
            <ta e="T700" id="Seg_10353" s="T699">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_10354" s="T700">adv</ta>
            <ta e="T702" id="Seg_10355" s="T701">v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_10356" s="T702">n-n:case.poss</ta>
            <ta e="T704" id="Seg_10357" s="T703">conj</ta>
            <ta e="T705" id="Seg_10358" s="T704">adv</ta>
            <ta e="T706" id="Seg_10359" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_10360" s="T706">n</ta>
            <ta e="T708" id="Seg_10361" s="T707">n-n:case.poss</ta>
            <ta e="T710" id="Seg_10362" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_10363" s="T710">v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_10364" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_10365" s="T713">n-n:case.poss</ta>
            <ta e="T715" id="Seg_10366" s="T714">v-v:tense-v:pn</ta>
            <ta e="T716" id="Seg_10367" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_10368" s="T716">n-n:case</ta>
            <ta e="T718" id="Seg_10369" s="T717">v-v:tense-v:pn</ta>
            <ta e="T719" id="Seg_10370" s="T718">adv</ta>
            <ta e="T720" id="Seg_10371" s="T719">v-v:tense-v:pn</ta>
            <ta e="T721" id="Seg_10372" s="T720">n-n:case</ta>
            <ta e="T722" id="Seg_10373" s="T721">dempro-n:num</ta>
            <ta e="T723" id="Seg_10374" s="T722">v-v:tense-v:pn</ta>
            <ta e="T725" id="Seg_10375" s="T724">n-n:case.poss</ta>
            <ta e="T727" id="Seg_10376" s="T725">adv</ta>
            <ta e="T728" id="Seg_10377" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_10378" s="T728">n-n:case.poss</ta>
            <ta e="T730" id="Seg_10379" s="T729">adj-n:case</ta>
            <ta e="T731" id="Seg_10380" s="T730">adj-n:case</ta>
            <ta e="T732" id="Seg_10381" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_10382" s="T732">n-n:case.poss</ta>
            <ta e="T734" id="Seg_10383" s="T733">ptcl</ta>
            <ta e="T735" id="Seg_10384" s="T734">adj-n:case</ta>
            <ta e="T736" id="Seg_10385" s="T735">adj-n:case</ta>
            <ta e="T737" id="Seg_10386" s="T736">n-n:case.poss</ta>
            <ta e="T738" id="Seg_10387" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_10388" s="T738">adj-n:case</ta>
            <ta e="T740" id="Seg_10389" s="T739">n-n:case</ta>
            <ta e="T741" id="Seg_10390" s="T740">v-v:tense-v:pn</ta>
            <ta e="T742" id="Seg_10391" s="T741">adv</ta>
            <ta e="T743" id="Seg_10392" s="T742">dempro-n:case</ta>
            <ta e="T747" id="Seg_10393" s="T746">adv</ta>
            <ta e="T748" id="Seg_10394" s="T747">dempro-n:case</ta>
            <ta e="T749" id="Seg_10395" s="T748">num-n:case</ta>
            <ta e="T750" id="Seg_10396" s="T749">n-n:case</ta>
            <ta e="T751" id="Seg_10397" s="T750">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_10398" s="T751">adv</ta>
            <ta e="T753" id="Seg_10399" s="T752">v-v:tense-v:pn</ta>
            <ta e="T754" id="Seg_10400" s="T753">n-n:case.poss</ta>
            <ta e="T755" id="Seg_10401" s="T754">que</ta>
            <ta e="T756" id="Seg_10402" s="T755">dempro-n:case</ta>
            <ta e="T757" id="Seg_10403" s="T756">n-n:case</ta>
            <ta e="T758" id="Seg_10404" s="T757">v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_10405" s="T758">num-n:case</ta>
            <ta e="T761" id="Seg_10406" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_10407" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_10408" s="T762">adv</ta>
            <ta e="T765" id="Seg_10409" s="T764">n</ta>
            <ta e="T766" id="Seg_10410" s="T765">n-n:case.poss</ta>
            <ta e="T767" id="Seg_10411" s="T766">dempro-n:case</ta>
            <ta e="T768" id="Seg_10412" s="T767">n-n:case</ta>
            <ta e="T769" id="Seg_10413" s="T768">n</ta>
            <ta e="T770" id="Seg_10414" s="T769">n-n:case.poss</ta>
            <ta e="T771" id="Seg_10415" s="T770">conj</ta>
            <ta e="T772" id="Seg_10416" s="T771">n-n:case</ta>
            <ta e="T773" id="Seg_10417" s="T772">n</ta>
            <ta e="T774" id="Seg_10418" s="T773">n-n:case.poss</ta>
            <ta e="T775" id="Seg_10419" s="T774">dempro-n:case</ta>
            <ta e="T776" id="Seg_10420" s="T775">num-n:case</ta>
            <ta e="T777" id="Seg_10421" s="T776">v-v:tense-v:pn</ta>
            <ta e="T778" id="Seg_10422" s="T777">conj</ta>
            <ta e="T779" id="Seg_10423" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_10424" s="T779">n-n:case.poss</ta>
            <ta e="T781" id="Seg_10425" s="T780">dempro-n:num-n:case.poss</ta>
            <ta e="T782" id="Seg_10426" s="T781">n-n:case</ta>
            <ta e="T783" id="Seg_10427" s="T782">n-n:case.poss</ta>
            <ta e="T784" id="Seg_10428" s="T783">n</ta>
            <ta e="T785" id="Seg_10429" s="T784">n-n:case.poss</ta>
            <ta e="T787" id="Seg_10430" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_10431" s="T787">n-n:case</ta>
            <ta e="T789" id="Seg_10432" s="T788">adv</ta>
            <ta e="T790" id="Seg_10433" s="T789">que-n:case=ptcl</ta>
            <ta e="T791" id="Seg_10434" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_10435" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_10436" s="T792">v-v:tense-v:pn</ta>
            <ta e="T795" id="Seg_10437" s="T794">v-v:n.fin</ta>
            <ta e="T796" id="Seg_10438" s="T795">conj</ta>
            <ta e="T797" id="Seg_10439" s="T796">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T799" id="Seg_10440" s="T798">n-n:num-n:case.poss</ta>
            <ta e="T800" id="Seg_10441" s="T799">dempro-n:num-n:case</ta>
            <ta e="T801" id="Seg_10442" s="T800">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_10443" s="T801">adj-n:case</ta>
            <ta e="T803" id="Seg_10444" s="T802">v-v:(case.poss)</ta>
            <ta e="T804" id="Seg_10445" s="T803">v-v:mood.pn</ta>
            <ta e="T805" id="Seg_10446" s="T804">aux-v:mood.pn</ta>
            <ta e="T806" id="Seg_10447" s="T805">v-v:mood.pn</ta>
            <ta e="T807" id="Seg_10448" s="T806">aux-v:mood.pn</ta>
            <ta e="T809" id="Seg_10449" s="T808">aux-v:mood.pn</ta>
            <ta e="T811" id="Seg_10450" s="T810">aux-v:mood.pn</ta>
            <ta e="T812" id="Seg_10451" s="T811">v-v&gt;v-v:n.fin</ta>
            <ta e="T813" id="Seg_10452" s="T812">ptcl</ta>
            <ta e="T815" id="Seg_10453" s="T814">n-n:num-n:case</ta>
            <ta e="T816" id="Seg_10454" s="T815">quant</ta>
            <ta e="T817" id="Seg_10455" s="T816">v-v:tense-v:pn</ta>
            <ta e="T818" id="Seg_10456" s="T817">n-n:num</ta>
            <ta e="T819" id="Seg_10457" s="T818">n-n:num</ta>
            <ta e="T820" id="Seg_10458" s="T819">adv</ta>
            <ta e="T821" id="Seg_10459" s="T820">dempro-n:num</ta>
            <ta e="T822" id="Seg_10460" s="T821">n-n:case</ta>
            <ta e="T823" id="Seg_10461" s="T822">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_10462" s="T823">dempro-n:case</ta>
            <ta e="T825" id="Seg_10463" s="T824">adv</ta>
            <ta e="T826" id="Seg_10464" s="T825">quant</ta>
            <ta e="T827" id="Seg_10465" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_10466" s="T827">v-v:tense-v:pn</ta>
            <ta e="T829" id="Seg_10467" s="T828">conj</ta>
            <ta e="T830" id="Seg_10468" s="T829">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_10469" s="T830">dempro-n:case</ta>
            <ta e="T832" id="Seg_10470" s="T831">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T833" id="Seg_10471" s="T832">dempro-n:num</ta>
            <ta e="T834" id="Seg_10472" s="T833">adv</ta>
            <ta e="T835" id="Seg_10473" s="T834">n-n:case</ta>
            <ta e="T836" id="Seg_10474" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_10475" s="T836">adv</ta>
            <ta e="T838" id="Seg_10476" s="T837">num-n:case</ta>
            <ta e="T839" id="Seg_10477" s="T838">n-n:case</ta>
            <ta e="T840" id="Seg_10478" s="T839">v-v:tense-v:pn</ta>
            <ta e="T841" id="Seg_10479" s="T840">adv</ta>
            <ta e="T842" id="Seg_10480" s="T841">dempro-n:num</ta>
            <ta e="T843" id="Seg_10481" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_10482" s="T843">v-v:tense-v:pn</ta>
            <ta e="T845" id="Seg_10483" s="T844">dempro-n:num</ta>
            <ta e="T846" id="Seg_10484" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_10485" s="T846">n-n:num-n:case</ta>
            <ta e="T848" id="Seg_10486" s="T847">v-v:tense-v:pn</ta>
            <ta e="T849" id="Seg_10487" s="T848">n-n:num</ta>
            <ta e="T850" id="Seg_10488" s="T849">v-v:tense-v:pn</ta>
            <ta e="T852" id="Seg_10489" s="T851">v-v:tense-v:pn</ta>
            <ta e="T853" id="Seg_10490" s="T852">n-n:num</ta>
            <ta e="T857" id="Seg_10491" s="T855">pers</ta>
            <ta e="T858" id="Seg_10492" s="T857">n-n:case.poss</ta>
            <ta e="T859" id="Seg_10493" s="T858">adv</ta>
            <ta e="T860" id="Seg_10494" s="T859">v-v:tense-v:pn</ta>
            <ta e="T861" id="Seg_10495" s="T860">n-n:case</ta>
            <ta e="T862" id="Seg_10496" s="T861">v-v:tense-v:pn</ta>
            <ta e="T863" id="Seg_10497" s="T862">adv</ta>
            <ta e="T864" id="Seg_10498" s="T863">adv</ta>
            <ta e="T865" id="Seg_10499" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_10500" s="T865">v-v:tense-v:pn</ta>
            <ta e="T867" id="Seg_10501" s="T866">n-n:case</ta>
            <ta e="T868" id="Seg_10502" s="T867">adv</ta>
            <ta e="T869" id="Seg_10503" s="T868">v-v:tense-v:pn</ta>
            <ta e="T870" id="Seg_10504" s="T869">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T871" id="Seg_10505" s="T870">conj</ta>
            <ta e="T872" id="Seg_10506" s="T871">n-n:case</ta>
            <ta e="T873" id="Seg_10507" s="T872">v-v:tense-v:pn</ta>
            <ta e="T874" id="Seg_10508" s="T873">conj</ta>
            <ta e="T875" id="Seg_10509" s="T874">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T876" id="Seg_10510" s="T875">v-v:n.fin</ta>
            <ta e="T877" id="Seg_10511" s="T876">conj</ta>
            <ta e="T878" id="Seg_10512" s="T877">pers</ta>
            <ta e="T880" id="Seg_10513" s="T879">pers</ta>
            <ta e="T881" id="Seg_10514" s="T880">num-n:case</ta>
            <ta e="T882" id="Seg_10515" s="T881">n-n:case</ta>
            <ta e="T883" id="Seg_10516" s="T882">v-v:tense-v:pn</ta>
            <ta e="T884" id="Seg_10517" s="T883">dempro-n:case</ta>
            <ta e="T885" id="Seg_10518" s="T884">n-n:case.poss</ta>
            <ta e="T886" id="Seg_10519" s="T885">conj</ta>
            <ta e="T887" id="Seg_10520" s="T886">refl-n:case.poss</ta>
            <ta e="T888" id="Seg_10521" s="T887">n-n:case.poss</ta>
            <ta e="T889" id="Seg_10522" s="T888">num-n:case</ta>
            <ta e="T890" id="Seg_10523" s="T889">n-n:case</ta>
            <ta e="T891" id="Seg_10524" s="T890">adj-n:case</ta>
            <ta e="T892" id="Seg_10525" s="T891">v-v:tense-v:pn</ta>
            <ta e="T893" id="Seg_10526" s="T892">adv</ta>
            <ta e="T894" id="Seg_10527" s="T893">v-v:tense-v:pn</ta>
            <ta e="T895" id="Seg_10528" s="T894">conj</ta>
            <ta e="T896" id="Seg_10529" s="T895">dempro-n:case</ta>
            <ta e="T897" id="Seg_10530" s="T896">adv</ta>
            <ta e="T898" id="Seg_10531" s="T897">v-v:tense-v:pn</ta>
            <ta e="T899" id="Seg_10532" s="T898">conj</ta>
            <ta e="T900" id="Seg_10533" s="T899">v-v:tense-v:pn</ta>
            <ta e="T901" id="Seg_10534" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_10535" s="T901">pers</ta>
            <ta e="T903" id="Seg_10536" s="T902">adv</ta>
            <ta e="T904" id="Seg_10537" s="T903">%%</ta>
            <ta e="T905" id="Seg_10538" s="T904">v-v&gt;v</ta>
            <ta e="T906" id="Seg_10539" s="T905">v-v:tense-v:pn</ta>
            <ta e="T907" id="Seg_10540" s="T906">conj</ta>
            <ta e="T908" id="Seg_10541" s="T907">adv</ta>
            <ta e="T910" id="Seg_10542" s="T908">n-n:case</ta>
            <ta e="T911" id="Seg_10543" s="T910">adv</ta>
            <ta e="T913" id="Seg_10544" s="T911">n-n:case</ta>
            <ta e="T914" id="Seg_10545" s="T913">adv</ta>
            <ta e="T915" id="Seg_10546" s="T914">dempro-n:case</ta>
            <ta e="T916" id="Seg_10547" s="T915">%%</ta>
            <ta e="T917" id="Seg_10548" s="T916">v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_10549" s="T917">que-n:case=ptcl</ta>
            <ta e="T919" id="Seg_10550" s="T918">v-v:tense-v:pn</ta>
            <ta e="T920" id="Seg_10551" s="T919">conj</ta>
            <ta e="T921" id="Seg_10552" s="T920">pers</ta>
            <ta e="T922" id="Seg_10553" s="T921">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T923" id="Seg_10554" s="T922">ptcl</ta>
            <ta e="T924" id="Seg_10555" s="T923">%%</ta>
            <ta e="T925" id="Seg_10556" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_10557" s="T925">v-v:tense-v:pn</ta>
            <ta e="T927" id="Seg_10558" s="T926">n-n:case</ta>
            <ta e="T928" id="Seg_10559" s="T927">que</ta>
            <ta e="T929" id="Seg_10560" s="T928">dempro-n:num</ta>
            <ta e="T930" id="Seg_10561" s="T929">v-v:tense-v:pn</ta>
            <ta e="T931" id="Seg_10562" s="T930">adv</ta>
            <ta e="T932" id="Seg_10563" s="T931">v-v:tense-v:pn</ta>
            <ta e="T933" id="Seg_10564" s="T932">n-n:case.poss</ta>
            <ta e="T934" id="Seg_10565" s="T933">v-v:n.fin</ta>
            <ta e="T935" id="Seg_10566" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_10567" s="T935">dempro-n:num-n:case</ta>
            <ta e="T937" id="Seg_10568" s="T936">adv</ta>
            <ta e="T938" id="Seg_10569" s="T937">v-v:n.fin</ta>
            <ta e="T939" id="Seg_10570" s="T938">dempro-n:num</ta>
            <ta e="T940" id="Seg_10571" s="T939">ptcl</ta>
            <ta e="T941" id="Seg_10572" s="T940">v-v:tense-v:pn</ta>
            <ta e="T942" id="Seg_10573" s="T941">pers</ta>
            <ta e="T943" id="Seg_10574" s="T942">adv</ta>
            <ta e="T944" id="Seg_10575" s="T943">v-v:tense-v:pn</ta>
            <ta e="T945" id="Seg_10576" s="T944">pers</ta>
            <ta e="T946" id="Seg_10577" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_10578" s="T946">v-v:tense-v:pn</ta>
            <ta e="T948" id="Seg_10579" s="T947">conj</ta>
            <ta e="T949" id="Seg_10580" s="T948">pers</ta>
            <ta e="T950" id="Seg_10581" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_10582" s="T950">v-v:tense-v:pn</ta>
            <ta e="T952" id="Seg_10583" s="T951">pers</ta>
            <ta e="T953" id="Seg_10584" s="T952">adv</ta>
            <ta e="T955" id="Seg_10585" s="T954">adv</ta>
            <ta e="T956" id="Seg_10586" s="T955">n-n:num</ta>
            <ta e="T957" id="Seg_10587" s="T956">v-v:tense-v:pn</ta>
            <ta e="T958" id="Seg_10588" s="T957">n-n:case</ta>
            <ta e="T959" id="Seg_10589" s="T958">v-v:tense-v:pn</ta>
            <ta e="T960" id="Seg_10590" s="T959">n-n:num</ta>
            <ta e="T961" id="Seg_10591" s="T960">v-v:tense-v:pn</ta>
            <ta e="T962" id="Seg_10592" s="T961">%%</ta>
            <ta e="T963" id="Seg_10593" s="T962">v-v:tense-v:pn</ta>
            <ta e="T964" id="Seg_10594" s="T963">pers</ta>
            <ta e="T965" id="Seg_10595" s="T964">v-v:n.fin</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T1" id="Seg_10596" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_10597" s="T1">v</ta>
            <ta e="T4" id="Seg_10598" s="T3">num</ta>
            <ta e="T5" id="Seg_10599" s="T4">adj</ta>
            <ta e="T6" id="Seg_10600" s="T5">n</ta>
            <ta e="T8" id="Seg_10601" s="T7">n</ta>
            <ta e="T9" id="Seg_10602" s="T8">v</ta>
            <ta e="T10" id="Seg_10603" s="T9">dempro</ta>
            <ta e="T11" id="Seg_10604" s="T10">adv</ta>
            <ta e="T12" id="Seg_10605" s="T11">v</ta>
            <ta e="T13" id="Seg_10606" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_10607" s="T13">dempro</ta>
            <ta e="T15" id="Seg_10608" s="T14">dempro</ta>
            <ta e="T16" id="Seg_10609" s="T15">v</ta>
            <ta e="T17" id="Seg_10610" s="T16">v</ta>
            <ta e="T18" id="Seg_10611" s="T17">adv</ta>
            <ta e="T19" id="Seg_10612" s="T18">dempro</ta>
            <ta e="T21" id="Seg_10613" s="T20">v</ta>
            <ta e="T22" id="Seg_10614" s="T21">adv</ta>
            <ta e="T23" id="Seg_10615" s="T22">dempro</ta>
            <ta e="T24" id="Seg_10616" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_10617" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_10618" s="T25">v</ta>
            <ta e="T27" id="Seg_10619" s="T26">pers</ta>
            <ta e="T28" id="Seg_10620" s="T27">dempro</ta>
            <ta e="T29" id="Seg_10621" s="T28">adv</ta>
            <ta e="T31" id="Seg_10622" s="T30">v</ta>
            <ta e="T32" id="Seg_10623" s="T31">conj</ta>
            <ta e="T33" id="Seg_10624" s="T32">adv</ta>
            <ta e="T34" id="Seg_10625" s="T33">dempro</ta>
            <ta e="T35" id="Seg_10626" s="T34">pers</ta>
            <ta e="T36" id="Seg_10627" s="T35">que</ta>
            <ta e="T37" id="Seg_10628" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_10629" s="T37">v</ta>
            <ta e="T39" id="Seg_10630" s="T38">pers</ta>
            <ta e="T40" id="Seg_10631" s="T39">adv</ta>
            <ta e="T41" id="Seg_10632" s="T40">adv</ta>
            <ta e="T42" id="Seg_10633" s="T41">v</ta>
            <ta e="T43" id="Seg_10634" s="T42">adj</ta>
            <ta e="T44" id="Seg_10635" s="T43">v</ta>
            <ta e="T45" id="Seg_10636" s="T44">dempro</ta>
            <ta e="T46" id="Seg_10637" s="T45">n</ta>
            <ta e="T47" id="Seg_10638" s="T46">que</ta>
            <ta e="T48" id="Seg_10639" s="T47">v</ta>
            <ta e="T49" id="Seg_10640" s="T48">conj</ta>
            <ta e="T50" id="Seg_10641" s="T49">que</ta>
            <ta e="T51" id="Seg_10642" s="T50">v</ta>
            <ta e="T52" id="Seg_10643" s="T51">adv</ta>
            <ta e="T53" id="Seg_10644" s="T52">v</ta>
            <ta e="T54" id="Seg_10645" s="T53">adv</ta>
            <ta e="T55" id="Seg_10646" s="T54">v</ta>
            <ta e="T56" id="Seg_10647" s="T55">n</ta>
            <ta e="T57" id="Seg_10648" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_10649" s="T57">n</ta>
            <ta e="T59" id="Seg_10650" s="T58">v</ta>
            <ta e="T60" id="Seg_10651" s="T59">n</ta>
            <ta e="T61" id="Seg_10652" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_10653" s="T61">v</ta>
            <ta e="T63" id="Seg_10654" s="T62">n</ta>
            <ta e="T64" id="Seg_10655" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_10656" s="T64">v</ta>
            <ta e="T66" id="Seg_10657" s="T65">adv</ta>
            <ta e="T67" id="Seg_10658" s="T66">adv</ta>
            <ta e="T68" id="Seg_10659" s="T67">v</ta>
            <ta e="T69" id="Seg_10660" s="T68">pers</ta>
            <ta e="T70" id="Seg_10661" s="T69">n</ta>
            <ta e="T71" id="Seg_10662" s="T70">v</ta>
            <ta e="T72" id="Seg_10663" s="T71">n</ta>
            <ta e="T73" id="Seg_10664" s="T72">v</ta>
            <ta e="T74" id="Seg_10665" s="T73">adv</ta>
            <ta e="T967" id="Seg_10666" s="T75">v</ta>
            <ta e="T76" id="Seg_10667" s="T967">v</ta>
            <ta e="T78" id="Seg_10668" s="T77">n</ta>
            <ta e="T79" id="Seg_10669" s="T78">pers</ta>
            <ta e="T80" id="Seg_10670" s="T79">adj</ta>
            <ta e="T81" id="Seg_10671" s="T80">v</ta>
            <ta e="T82" id="Seg_10672" s="T81">n</ta>
            <ta e="T83" id="Seg_10673" s="T82">v</ta>
            <ta e="T85" id="Seg_10674" s="T84">pers</ta>
            <ta e="T86" id="Seg_10675" s="T85">num</ta>
            <ta e="T87" id="Seg_10676" s="T86">num</ta>
            <ta e="T90" id="Seg_10677" s="T89">n</ta>
            <ta e="T91" id="Seg_10678" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_10679" s="T91">v</ta>
            <ta e="T93" id="Seg_10680" s="T92">adv</ta>
            <ta e="T94" id="Seg_10681" s="T93">v</ta>
            <ta e="T95" id="Seg_10682" s="T94">pers</ta>
            <ta e="T96" id="Seg_10683" s="T95">v</ta>
            <ta e="T97" id="Seg_10684" s="T96">adv</ta>
            <ta e="T98" id="Seg_10685" s="T97">dempro</ta>
            <ta e="T100" id="Seg_10686" s="T99">v</ta>
            <ta e="T101" id="Seg_10687" s="T100">pers</ta>
            <ta e="T102" id="Seg_10688" s="T101">v</ta>
            <ta e="T103" id="Seg_10689" s="T102">n</ta>
            <ta e="T104" id="Seg_10690" s="T103">v</ta>
            <ta e="T105" id="Seg_10691" s="T104">n</ta>
            <ta e="T106" id="Seg_10692" s="T105">adv</ta>
            <ta e="T108" id="Seg_10693" s="T107">v</ta>
            <ta e="T109" id="Seg_10694" s="T108">n</ta>
            <ta e="T110" id="Seg_10695" s="T109">n</ta>
            <ta e="T111" id="Seg_10696" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_10697" s="T112">v</ta>
            <ta e="T114" id="Seg_10698" s="T113">n</ta>
            <ta e="T115" id="Seg_10699" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_10700" s="T115">v</ta>
            <ta e="T117" id="Seg_10701" s="T116">n</ta>
            <ta e="T118" id="Seg_10702" s="T117">v</ta>
            <ta e="T119" id="Seg_10703" s="T118">n</ta>
            <ta e="T120" id="Seg_10704" s="T119">v</ta>
            <ta e="T121" id="Seg_10705" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_10706" s="T121">conj</ta>
            <ta e="T123" id="Seg_10707" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_10708" s="T123">v</ta>
            <ta e="T125" id="Seg_10709" s="T124">num</ta>
            <ta e="T126" id="Seg_10710" s="T125">n</ta>
            <ta e="T127" id="Seg_10711" s="T126">num</ta>
            <ta e="T128" id="Seg_10712" s="T127">n</ta>
            <ta e="T129" id="Seg_10713" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_10714" s="T129">n</ta>
            <ta e="T131" id="Seg_10715" s="T130">v</ta>
            <ta e="T132" id="Seg_10716" s="T131">quant</ta>
            <ta e="T133" id="Seg_10717" s="T132">v</ta>
            <ta e="T134" id="Seg_10718" s="T133">v</ta>
            <ta e="T137" id="Seg_10719" s="T136">v</ta>
            <ta e="T138" id="Seg_10720" s="T137">conj</ta>
            <ta e="T139" id="Seg_10721" s="T138">dempro</ta>
            <ta e="T140" id="Seg_10722" s="T139">v</ta>
            <ta e="T141" id="Seg_10723" s="T140">conj</ta>
            <ta e="T142" id="Seg_10724" s="T141">n</ta>
            <ta e="T143" id="Seg_10725" s="T142">v</ta>
            <ta e="T144" id="Seg_10726" s="T143">v</ta>
            <ta e="T146" id="Seg_10727" s="T145">dempro</ta>
            <ta e="T147" id="Seg_10728" s="T146">dempro</ta>
            <ta e="T148" id="Seg_10729" s="T147">v</ta>
            <ta e="T149" id="Seg_10730" s="T148">n</ta>
            <ta e="T150" id="Seg_10731" s="T149">v</ta>
            <ta e="T151" id="Seg_10732" s="T150">n</ta>
            <ta e="T152" id="Seg_10733" s="T151">v</ta>
            <ta e="T153" id="Seg_10734" s="T152">v</ta>
            <ta e="T154" id="Seg_10735" s="T153">conj</ta>
            <ta e="T155" id="Seg_10736" s="T154">dempro</ta>
            <ta e="T156" id="Seg_10737" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_10738" s="T156">v</ta>
            <ta e="T158" id="Seg_10739" s="T157">n</ta>
            <ta e="T159" id="Seg_10740" s="T158">v</ta>
            <ta e="T160" id="Seg_10741" s="T159">adv</ta>
            <ta e="T161" id="Seg_10742" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_10743" s="T161">v</ta>
            <ta e="T163" id="Seg_10744" s="T162">adv</ta>
            <ta e="T165" id="Seg_10745" s="T164">n</ta>
            <ta e="T968" id="Seg_10746" s="T165">v</ta>
            <ta e="T166" id="Seg_10747" s="T968">v</ta>
            <ta e="T167" id="Seg_10748" s="T166">adv</ta>
            <ta e="T168" id="Seg_10749" s="T167">pers</ta>
            <ta e="T169" id="Seg_10750" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_10751" s="T169">v</ta>
            <ta e="T171" id="Seg_10752" s="T170">conj</ta>
            <ta e="T172" id="Seg_10753" s="T171">pers</ta>
            <ta e="T173" id="Seg_10754" s="T172">v</ta>
            <ta e="T174" id="Seg_10755" s="T173">conj</ta>
            <ta e="T175" id="Seg_10756" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_10757" s="T175">adj</ta>
            <ta e="T177" id="Seg_10758" s="T176">conj</ta>
            <ta e="T178" id="Seg_10759" s="T177">v</ta>
            <ta e="T179" id="Seg_10760" s="T178">adv</ta>
            <ta e="T180" id="Seg_10761" s="T179">adv</ta>
            <ta e="T181" id="Seg_10762" s="T180">n</ta>
            <ta e="T182" id="Seg_10763" s="T181">adv</ta>
            <ta e="T183" id="Seg_10764" s="T182">v</ta>
            <ta e="T184" id="Seg_10765" s="T183">v</ta>
            <ta e="T186" id="Seg_10766" s="T185">conj</ta>
            <ta e="T187" id="Seg_10767" s="T186">pers</ta>
            <ta e="T189" id="Seg_10768" s="T188">n</ta>
            <ta e="T190" id="Seg_10769" s="T189">v</ta>
            <ta e="T191" id="Seg_10770" s="T190">n</ta>
            <ta e="T192" id="Seg_10771" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_10772" s="T192">v</ta>
            <ta e="T194" id="Seg_10773" s="T193">pers</ta>
            <ta e="T195" id="Seg_10774" s="T194">n</ta>
            <ta e="T196" id="Seg_10775" s="T195">adj</ta>
            <ta e="T197" id="Seg_10776" s="T196">adv</ta>
            <ta e="T198" id="Seg_10777" s="T197">v</ta>
            <ta e="T199" id="Seg_10778" s="T198">pers</ta>
            <ta e="T200" id="Seg_10779" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_10780" s="T200">v</ta>
            <ta e="T202" id="Seg_10781" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_10782" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_10783" s="T203">v</ta>
            <ta e="T205" id="Seg_10784" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_10785" s="T205">pers</ta>
            <ta e="T207" id="Seg_10786" s="T206">n</ta>
            <ta e="T208" id="Seg_10787" s="T207">v</ta>
            <ta e="T209" id="Seg_10788" s="T208">n</ta>
            <ta e="T211" id="Seg_10789" s="T210">v</ta>
            <ta e="T212" id="Seg_10790" s="T211">conj</ta>
            <ta e="T213" id="Seg_10791" s="T212">adv</ta>
            <ta e="T214" id="Seg_10792" s="T213">v</ta>
            <ta e="T215" id="Seg_10793" s="T214">pers</ta>
            <ta e="T216" id="Seg_10794" s="T215">n</ta>
            <ta e="T217" id="Seg_10795" s="T216">v</ta>
            <ta e="T218" id="Seg_10796" s="T217">pers</ta>
            <ta e="T219" id="Seg_10797" s="T218">v</ta>
            <ta e="T220" id="Seg_10798" s="T219">n</ta>
            <ta e="T221" id="Seg_10799" s="T220">conj</ta>
            <ta e="T222" id="Seg_10800" s="T221">n</ta>
            <ta e="T223" id="Seg_10801" s="T222">v</ta>
            <ta e="T224" id="Seg_10802" s="T223">conj</ta>
            <ta e="T225" id="Seg_10803" s="T224">pers</ta>
            <ta e="T226" id="Seg_10804" s="T225">n</ta>
            <ta e="T227" id="Seg_10805" s="T226">v</ta>
            <ta e="T228" id="Seg_10806" s="T227">que</ta>
            <ta e="T229" id="Seg_10807" s="T228">v</ta>
            <ta e="T230" id="Seg_10808" s="T229">que</ta>
            <ta e="T231" id="Seg_10809" s="T230">v</ta>
            <ta e="T232" id="Seg_10810" s="T231">n</ta>
            <ta e="T233" id="Seg_10811" s="T232">v</ta>
            <ta e="T234" id="Seg_10812" s="T233">n</ta>
            <ta e="T235" id="Seg_10813" s="T234">n</ta>
            <ta e="T236" id="Seg_10814" s="T235">v</ta>
            <ta e="T237" id="Seg_10815" s="T236">adj</ta>
            <ta e="T238" id="Seg_10816" s="T237">v</ta>
            <ta e="T239" id="Seg_10817" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_10818" s="T239">adj</ta>
            <ta e="T241" id="Seg_10819" s="T240">n</ta>
            <ta e="T242" id="Seg_10820" s="T241">n</ta>
            <ta e="T243" id="Seg_10821" s="T242">v</ta>
            <ta e="T244" id="Seg_10822" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_10823" s="T244">adj</ta>
            <ta e="T246" id="Seg_10824" s="T245">v</ta>
            <ta e="T247" id="Seg_10825" s="T246">n</ta>
            <ta e="T248" id="Seg_10826" s="T247">que</ta>
            <ta e="T249" id="Seg_10827" s="T248">v</ta>
            <ta e="T250" id="Seg_10828" s="T249">n</ta>
            <ta e="T251" id="Seg_10829" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_10830" s="T251">v</ta>
            <ta e="T253" id="Seg_10831" s="T252">n</ta>
            <ta e="T254" id="Seg_10832" s="T253">v</ta>
            <ta e="T255" id="Seg_10833" s="T254">n</ta>
            <ta e="T256" id="Seg_10834" s="T255">v</ta>
            <ta e="T257" id="Seg_10835" s="T256">n</ta>
            <ta e="T258" id="Seg_10836" s="T257">v</ta>
            <ta e="T259" id="Seg_10837" s="T258">n</ta>
            <ta e="T260" id="Seg_10838" s="T259">adv</ta>
            <ta e="T261" id="Seg_10839" s="T260">n</ta>
            <ta e="T262" id="Seg_10840" s="T261">v</ta>
            <ta e="T263" id="Seg_10841" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_10842" s="T263">que</ta>
            <ta e="T265" id="Seg_10843" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_10844" s="T265">v</ta>
            <ta e="T267" id="Seg_10845" s="T266">n</ta>
            <ta e="T268" id="Seg_10846" s="T267">v</ta>
            <ta e="T269" id="Seg_10847" s="T268">v</ta>
            <ta e="T270" id="Seg_10848" s="T269">n</ta>
            <ta e="T271" id="Seg_10849" s="T270">ptcl</ta>
            <ta e="T273" id="Seg_10850" s="T272">v</ta>
            <ta e="T274" id="Seg_10851" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_10852" s="T274">aux</ta>
            <ta e="T276" id="Seg_10853" s="T275">v</ta>
            <ta e="T277" id="Seg_10854" s="T276">n</ta>
            <ta e="T278" id="Seg_10855" s="T277">v</ta>
            <ta e="T279" id="Seg_10856" s="T278">n</ta>
            <ta e="T280" id="Seg_10857" s="T279">v</ta>
            <ta e="T281" id="Seg_10858" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_10859" s="T281">v</ta>
            <ta e="T283" id="Seg_10860" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_10861" s="T283">refl</ta>
            <ta e="T285" id="Seg_10862" s="T284">aux</ta>
            <ta e="T286" id="Seg_10863" s="T285">v</ta>
            <ta e="T287" id="Seg_10864" s="T286">v</ta>
            <ta e="T288" id="Seg_10865" s="T287">adv</ta>
            <ta e="T289" id="Seg_10866" s="T288">conj</ta>
            <ta e="T290" id="Seg_10867" s="T289">v</ta>
            <ta e="T291" id="Seg_10868" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_10869" s="T291">adj</ta>
            <ta e="T293" id="Seg_10870" s="T292">n</ta>
            <ta e="T294" id="Seg_10871" s="T293">adv</ta>
            <ta e="T295" id="Seg_10872" s="T294">n</ta>
            <ta e="T296" id="Seg_10873" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_10874" s="T296">v</ta>
            <ta e="T298" id="Seg_10875" s="T297">v</ta>
            <ta e="T299" id="Seg_10876" s="T298">adj</ta>
            <ta e="T300" id="Seg_10877" s="T299">adv</ta>
            <ta e="T301" id="Seg_10878" s="T300">adv</ta>
            <ta e="T302" id="Seg_10879" s="T301">v</ta>
            <ta e="T303" id="Seg_10880" s="T302">adv</ta>
            <ta e="T304" id="Seg_10881" s="T303">aux</ta>
            <ta e="T305" id="Seg_10882" s="T304">v</ta>
            <ta e="T306" id="Seg_10883" s="T305">v</ta>
            <ta e="T307" id="Seg_10884" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_10885" s="T307">dempro</ta>
            <ta e="T309" id="Seg_10886" s="T308">refl</ta>
            <ta e="T310" id="Seg_10887" s="T309">v</ta>
            <ta e="T311" id="Seg_10888" s="T310">pers</ta>
            <ta e="T312" id="Seg_10889" s="T311">refl</ta>
            <ta e="T313" id="Seg_10890" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_10891" s="T313">v</ta>
            <ta e="T315" id="Seg_10892" s="T314">conj</ta>
            <ta e="T316" id="Seg_10893" s="T315">pers</ta>
            <ta e="T317" id="Seg_10894" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_10895" s="T317">v</ta>
            <ta e="T319" id="Seg_10896" s="T318">n</ta>
            <ta e="T321" id="Seg_10897" s="T320">n</ta>
            <ta e="T322" id="Seg_10898" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_10899" s="T322">v</ta>
            <ta e="T324" id="Seg_10900" s="T323">adj</ta>
            <ta e="T325" id="Seg_10901" s="T324">n</ta>
            <ta e="T326" id="Seg_10902" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_10903" s="T326">adv</ta>
            <ta e="T328" id="Seg_10904" s="T327">adv</ta>
            <ta e="T329" id="Seg_10905" s="T328">v</ta>
            <ta e="T330" id="Seg_10906" s="T329">adv</ta>
            <ta e="T331" id="Seg_10907" s="T330">v</ta>
            <ta e="T332" id="Seg_10908" s="T331">adj</ta>
            <ta e="T333" id="Seg_10909" s="T332">v</ta>
            <ta e="T334" id="Seg_10910" s="T333">que</ta>
            <ta e="T335" id="Seg_10911" s="T334">v</ta>
            <ta e="T336" id="Seg_10912" s="T335">n</ta>
            <ta e="T337" id="Seg_10913" s="T336">v</ta>
            <ta e="T338" id="Seg_10914" s="T337">n</ta>
            <ta e="T339" id="Seg_10915" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_10916" s="T339">n</ta>
            <ta e="T341" id="Seg_10917" s="T340">v</ta>
            <ta e="T343" id="Seg_10918" s="T342">n</ta>
            <ta e="T347" id="Seg_10919" s="T346">dempro</ta>
            <ta e="T348" id="Seg_10920" s="T347">n</ta>
            <ta e="T349" id="Seg_10921" s="T348">ptcl</ta>
            <ta e="T351" id="Seg_10922" s="T350">dempro</ta>
            <ta e="T353" id="Seg_10923" s="T352">v</ta>
            <ta e="T354" id="Seg_10924" s="T353">dempro</ta>
            <ta e="T355" id="Seg_10925" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_10926" s="T355">adv</ta>
            <ta e="T357" id="Seg_10927" s="T356">v</ta>
            <ta e="T358" id="Seg_10928" s="T357">n</ta>
            <ta e="T359" id="Seg_10929" s="T358">v</ta>
            <ta e="T360" id="Seg_10930" s="T359">dempro</ta>
            <ta e="T361" id="Seg_10931" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_10932" s="T361">dempro</ta>
            <ta e="T363" id="Seg_10933" s="T362">v</ta>
            <ta e="T364" id="Seg_10934" s="T363">dempro</ta>
            <ta e="T365" id="Seg_10935" s="T364">dempro</ta>
            <ta e="T366" id="Seg_10936" s="T365">v</ta>
            <ta e="T367" id="Seg_10937" s="T366">adv</ta>
            <ta e="T368" id="Seg_10938" s="T367">pers</ta>
            <ta e="T369" id="Seg_10939" s="T368">adj</ta>
            <ta e="T371" id="Seg_10940" s="T370">conj</ta>
            <ta e="T372" id="Seg_10941" s="T371">que</ta>
            <ta e="T373" id="Seg_10942" s="T372">v</ta>
            <ta e="T375" id="Seg_10943" s="T374">v</ta>
            <ta e="T376" id="Seg_10944" s="T375">conj</ta>
            <ta e="T377" id="Seg_10945" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_10946" s="T377">pers</ta>
            <ta e="T379" id="Seg_10947" s="T378">v</ta>
            <ta e="T380" id="Seg_10948" s="T379">n</ta>
            <ta e="T381" id="Seg_10949" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_10950" s="T381">v</ta>
            <ta e="T383" id="Seg_10951" s="T382">num</ta>
            <ta e="T384" id="Seg_10952" s="T383">n</ta>
            <ta e="T385" id="Seg_10953" s="T384">v</ta>
            <ta e="T386" id="Seg_10954" s="T385">num</ta>
            <ta e="T387" id="Seg_10955" s="T386">n</ta>
            <ta e="T388" id="Seg_10956" s="T387">v</ta>
            <ta e="T389" id="Seg_10957" s="T388">pers</ta>
            <ta e="T390" id="Seg_10958" s="T389">n</ta>
            <ta e="T391" id="Seg_10959" s="T390">v</ta>
            <ta e="T392" id="Seg_10960" s="T391">conj</ta>
            <ta e="T393" id="Seg_10961" s="T392">que</ta>
            <ta e="T394" id="Seg_10962" s="T393">v</ta>
            <ta e="T395" id="Seg_10963" s="T394">n</ta>
            <ta e="T396" id="Seg_10964" s="T395">v</ta>
            <ta e="T397" id="Seg_10965" s="T396">v</ta>
            <ta e="T398" id="Seg_10966" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_10967" s="T398">v</ta>
            <ta e="T400" id="Seg_10968" s="T399">v</ta>
            <ta e="T401" id="Seg_10969" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_10970" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_10971" s="T402">v</ta>
            <ta e="T404" id="Seg_10972" s="T403">v</ta>
            <ta e="T405" id="Seg_10973" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_10974" s="T405">v</ta>
            <ta e="T407" id="Seg_10975" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_10976" s="T407">dempro</ta>
            <ta e="T409" id="Seg_10977" s="T408">n</ta>
            <ta e="T410" id="Seg_10978" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_10979" s="T410">adj</ta>
            <ta e="T412" id="Seg_10980" s="T411">adv</ta>
            <ta e="T414" id="Seg_10981" s="T413">n</ta>
            <ta e="T415" id="Seg_10982" s="T414">v</ta>
            <ta e="T416" id="Seg_10983" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_10984" s="T416">dempro</ta>
            <ta e="T418" id="Seg_10985" s="T417">v</ta>
            <ta e="T419" id="Seg_10986" s="T418">v</ta>
            <ta e="T420" id="Seg_10987" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_10988" s="T420">que</ta>
            <ta e="T422" id="Seg_10989" s="T421">v</ta>
            <ta e="T423" id="Seg_10990" s="T422">v</ta>
            <ta e="T424" id="Seg_10991" s="T423">conj</ta>
            <ta e="T426" id="Seg_10992" s="T424">n</ta>
            <ta e="T428" id="Seg_10993" s="T427">conj</ta>
            <ta e="T429" id="Seg_10994" s="T428">n</ta>
            <ta e="T430" id="Seg_10995" s="T429">v</ta>
            <ta e="T431" id="Seg_10996" s="T430">conj</ta>
            <ta e="T432" id="Seg_10997" s="T431">pers</ta>
            <ta e="T433" id="Seg_10998" s="T432">v</ta>
            <ta e="T434" id="Seg_10999" s="T433">conj</ta>
            <ta e="T435" id="Seg_11000" s="T434">n</ta>
            <ta e="T436" id="Seg_11001" s="T435">v</ta>
            <ta e="T437" id="Seg_11002" s="T436">pers</ta>
            <ta e="T438" id="Seg_11003" s="T437">v</ta>
            <ta e="T439" id="Seg_11004" s="T438">dempro</ta>
            <ta e="T440" id="Seg_11005" s="T439">n</ta>
            <ta e="T441" id="Seg_11006" s="T440">adv</ta>
            <ta e="T442" id="Seg_11007" s="T441">adj</ta>
            <ta e="T443" id="Seg_11008" s="T442">v</ta>
            <ta e="T444" id="Seg_11009" s="T443">adv</ta>
            <ta e="T445" id="Seg_11010" s="T444">adj</ta>
            <ta e="T446" id="Seg_11011" s="T445">v</ta>
            <ta e="T447" id="Seg_11012" s="T446">n</ta>
            <ta e="T448" id="Seg_11013" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_11014" s="T448">adv</ta>
            <ta e="T453" id="Seg_11015" s="T452">n</ta>
            <ta e="T454" id="Seg_11016" s="T453">conj</ta>
            <ta e="T455" id="Seg_11017" s="T454">dempro</ta>
            <ta e="T456" id="Seg_11018" s="T455">n</ta>
            <ta e="T457" id="Seg_11019" s="T456">v</ta>
            <ta e="T458" id="Seg_11020" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_11021" s="T458">adv</ta>
            <ta e="T460" id="Seg_11022" s="T459">adj</ta>
            <ta e="T461" id="Seg_11023" s="T460">n</ta>
            <ta e="T462" id="Seg_11024" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_11025" s="T462">adj</ta>
            <ta e="T464" id="Seg_11026" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_11027" s="T464">pers</ta>
            <ta e="T466" id="Seg_11028" s="T465">n</ta>
            <ta e="T467" id="Seg_11029" s="T466">n</ta>
            <ta e="T468" id="Seg_11030" s="T467">v</ta>
            <ta e="T469" id="Seg_11031" s="T468">ptcl</ta>
            <ta e="T470" id="Seg_11032" s="T469">n</ta>
            <ta e="T471" id="Seg_11033" s="T470">v</ta>
            <ta e="T472" id="Seg_11034" s="T471">n</ta>
            <ta e="T473" id="Seg_11035" s="T472">v</ta>
            <ta e="T474" id="Seg_11036" s="T473">conj</ta>
            <ta e="T475" id="Seg_11037" s="T474">dempro</ta>
            <ta e="T476" id="Seg_11038" s="T475">adj</ta>
            <ta e="T477" id="Seg_11039" s="T476">n</ta>
            <ta e="T478" id="Seg_11040" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_11041" s="T478">v</ta>
            <ta e="T482" id="Seg_11042" s="T481">v</ta>
            <ta e="T483" id="Seg_11043" s="T482">refl</ta>
            <ta e="T484" id="Seg_11044" s="T483">n</ta>
            <ta e="T485" id="Seg_11045" s="T484">dempro</ta>
            <ta e="T486" id="Seg_11046" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_11047" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_11048" s="T487">v</ta>
            <ta e="T489" id="Seg_11049" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_11050" s="T489">dempro</ta>
            <ta e="T491" id="Seg_11051" s="T490">v</ta>
            <ta e="T492" id="Seg_11052" s="T491">n</ta>
            <ta e="T493" id="Seg_11053" s="T492">n</ta>
            <ta e="T494" id="Seg_11054" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_11055" s="T494">v</ta>
            <ta e="T496" id="Seg_11056" s="T495">adv</ta>
            <ta e="T497" id="Seg_11057" s="T496">n</ta>
            <ta e="T498" id="Seg_11058" s="T497">v</ta>
            <ta e="T500" id="Seg_11059" s="T499">pers</ta>
            <ta e="T501" id="Seg_11060" s="T500">v</ta>
            <ta e="T502" id="Seg_11061" s="T501">n</ta>
            <ta e="T503" id="Seg_11062" s="T502">v</ta>
            <ta e="T504" id="Seg_11063" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_11064" s="T504">pers</ta>
            <ta e="T506" id="Seg_11065" s="T505">n</ta>
            <ta e="T507" id="Seg_11066" s="T506">n</ta>
            <ta e="T508" id="Seg_11067" s="T507">v</ta>
            <ta e="T509" id="Seg_11068" s="T508">pers</ta>
            <ta e="T510" id="Seg_11069" s="T509">adv</ta>
            <ta e="T511" id="Seg_11070" s="T510">n</ta>
            <ta e="T512" id="Seg_11071" s="T511">n</ta>
            <ta e="T513" id="Seg_11072" s="T512">n</ta>
            <ta e="T514" id="Seg_11073" s="T513">v</ta>
            <ta e="T515" id="Seg_11074" s="T514">dempro</ta>
            <ta e="T516" id="Seg_11075" s="T515">v</ta>
            <ta e="T517" id="Seg_11076" s="T516">n</ta>
            <ta e="T518" id="Seg_11077" s="T517">n</ta>
            <ta e="T519" id="Seg_11078" s="T518">n</ta>
            <ta e="T520" id="Seg_11079" s="T519">v</ta>
            <ta e="T521" id="Seg_11080" s="T520">n</ta>
            <ta e="T523" id="Seg_11081" s="T522">v</ta>
            <ta e="T524" id="Seg_11082" s="T523">pers</ta>
            <ta e="T526" id="Seg_11083" s="T525">pers</ta>
            <ta e="T527" id="Seg_11084" s="T526">n</ta>
            <ta e="T529" id="Seg_11085" s="T528">v</ta>
            <ta e="T531" id="Seg_11086" s="T530">adv</ta>
            <ta e="T532" id="Seg_11087" s="T531">n</ta>
            <ta e="T533" id="Seg_11088" s="T532">v</ta>
            <ta e="T534" id="Seg_11089" s="T533">pers</ta>
            <ta e="T535" id="Seg_11090" s="T534">v</ta>
            <ta e="T536" id="Seg_11091" s="T535">pers</ta>
            <ta e="T537" id="Seg_11092" s="T536">n</ta>
            <ta e="T538" id="Seg_11093" s="T537">n</ta>
            <ta e="T539" id="Seg_11094" s="T538">v</ta>
            <ta e="T540" id="Seg_11095" s="T539">adv</ta>
            <ta e="T541" id="Seg_11096" s="T540">v</ta>
            <ta e="T542" id="Seg_11097" s="T541">n</ta>
            <ta e="T543" id="Seg_11098" s="T542">v</ta>
            <ta e="T544" id="Seg_11099" s="T543">n</ta>
            <ta e="T545" id="Seg_11100" s="T544">v</ta>
            <ta e="T546" id="Seg_11101" s="T545">n</ta>
            <ta e="T547" id="Seg_11102" s="T546">quant</ta>
            <ta e="T548" id="Seg_11103" s="T547">v</ta>
            <ta e="T549" id="Seg_11104" s="T548">adv</ta>
            <ta e="T550" id="Seg_11105" s="T549">n</ta>
            <ta e="T551" id="Seg_11106" s="T550">v</ta>
            <ta e="T552" id="Seg_11107" s="T551">n</ta>
            <ta e="T553" id="Seg_11108" s="T552">v</ta>
            <ta e="T554" id="Seg_11109" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_11110" s="T554">adv</ta>
            <ta e="T558" id="Seg_11111" s="T557">n</ta>
            <ta e="T559" id="Seg_11112" s="T558">v</ta>
            <ta e="T560" id="Seg_11113" s="T559">dempro</ta>
            <ta e="T561" id="Seg_11114" s="T560">n</ta>
            <ta e="T562" id="Seg_11115" s="T561">v</ta>
            <ta e="T563" id="Seg_11116" s="T562">conj</ta>
            <ta e="T564" id="Seg_11117" s="T563">n</ta>
            <ta e="T565" id="Seg_11118" s="T564">v</ta>
            <ta e="T566" id="Seg_11119" s="T565">adv</ta>
            <ta e="T567" id="Seg_11120" s="T566">n</ta>
            <ta e="T568" id="Seg_11121" s="T567">dempro</ta>
            <ta e="T569" id="Seg_11122" s="T568">n</ta>
            <ta e="T570" id="Seg_11123" s="T569">v</ta>
            <ta e="T571" id="Seg_11124" s="T570">adv</ta>
            <ta e="T574" id="Seg_11125" s="T573">n</ta>
            <ta e="T575" id="Seg_11126" s="T574">v</ta>
            <ta e="T576" id="Seg_11127" s="T575">adv</ta>
            <ta e="T577" id="Seg_11128" s="T576">n</ta>
            <ta e="T578" id="Seg_11129" s="T577">v</ta>
            <ta e="T579" id="Seg_11130" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_11131" s="T579">ptcl</ta>
            <ta e="T581" id="Seg_11132" s="T580">n</ta>
            <ta e="T582" id="Seg_11133" s="T581">v</ta>
            <ta e="T583" id="Seg_11134" s="T582">n</ta>
            <ta e="T584" id="Seg_11135" s="T583">n</ta>
            <ta e="T585" id="Seg_11136" s="T584">v</ta>
            <ta e="T586" id="Seg_11137" s="T585">conj</ta>
            <ta e="T587" id="Seg_11138" s="T586">n</ta>
            <ta e="T588" id="Seg_11139" s="T587">n</ta>
            <ta e="T589" id="Seg_11140" s="T588">v</ta>
            <ta e="T590" id="Seg_11141" s="T589">quant</ta>
            <ta e="T591" id="Seg_11142" s="T590">n</ta>
            <ta e="T592" id="Seg_11143" s="T591">v</ta>
            <ta e="T593" id="Seg_11144" s="T592">n</ta>
            <ta e="T594" id="Seg_11145" s="T593">quant</ta>
            <ta e="T595" id="Seg_11146" s="T594">v</ta>
            <ta e="T596" id="Seg_11147" s="T595">n</ta>
            <ta e="T597" id="Seg_11148" s="T596">adv</ta>
            <ta e="T598" id="Seg_11149" s="T597">quant</ta>
            <ta e="T599" id="Seg_11150" s="T598">que</ta>
            <ta e="T601" id="Seg_11151" s="T600">v</ta>
            <ta e="T602" id="Seg_11152" s="T601">conj</ta>
            <ta e="T603" id="Seg_11153" s="T602">n</ta>
            <ta e="T604" id="Seg_11154" s="T603">v</ta>
            <ta e="T605" id="Seg_11155" s="T604">n</ta>
            <ta e="T606" id="Seg_11156" s="T605">n</ta>
            <ta e="T607" id="Seg_11157" s="T606">v</ta>
            <ta e="T608" id="Seg_11158" s="T607">dempro</ta>
            <ta e="T609" id="Seg_11159" s="T608">n</ta>
            <ta e="T610" id="Seg_11160" s="T609">v</ta>
            <ta e="T611" id="Seg_11161" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_11162" s="T611">n</ta>
            <ta e="T613" id="Seg_11163" s="T612">v</ta>
            <ta e="T614" id="Seg_11164" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_11165" s="T614">n</ta>
            <ta e="T616" id="Seg_11166" s="T615">v</ta>
            <ta e="T617" id="Seg_11167" s="T616">n</ta>
            <ta e="T618" id="Seg_11168" s="T617">v</ta>
            <ta e="T619" id="Seg_11169" s="T618">n</ta>
            <ta e="T620" id="Seg_11170" s="T619">adv</ta>
            <ta e="T621" id="Seg_11171" s="T620">n</ta>
            <ta e="T622" id="Seg_11172" s="T621">v</ta>
            <ta e="T623" id="Seg_11173" s="T622">adv</ta>
            <ta e="T625" id="Seg_11174" s="T624">dempro</ta>
            <ta e="T626" id="Seg_11175" s="T625">n</ta>
            <ta e="T627" id="Seg_11176" s="T626">v</ta>
            <ta e="T628" id="Seg_11177" s="T627">num</ta>
            <ta e="T629" id="Seg_11178" s="T628">num</ta>
            <ta e="T630" id="Seg_11179" s="T629">num</ta>
            <ta e="T631" id="Seg_11180" s="T630">adv</ta>
            <ta e="T632" id="Seg_11181" s="T631">n</ta>
            <ta e="T633" id="Seg_11182" s="T632">n</ta>
            <ta e="T634" id="Seg_11183" s="T633">dempro</ta>
            <ta e="T635" id="Seg_11184" s="T634">v</ta>
            <ta e="T636" id="Seg_11185" s="T635">n</ta>
            <ta e="T637" id="Seg_11186" s="T636">n</ta>
            <ta e="T638" id="Seg_11187" s="T637">v</ta>
            <ta e="T639" id="Seg_11188" s="T638">n</ta>
            <ta e="T640" id="Seg_11189" s="T639">v</ta>
            <ta e="T641" id="Seg_11190" s="T640">n</ta>
            <ta e="T642" id="Seg_11191" s="T641">n</ta>
            <ta e="T643" id="Seg_11192" s="T642">v</ta>
            <ta e="T644" id="Seg_11193" s="T643">adv</ta>
            <ta e="T645" id="Seg_11194" s="T644">quant</ta>
            <ta e="T647" id="Seg_11195" s="T646">n</ta>
            <ta e="T648" id="Seg_11196" s="T647">quant</ta>
            <ta e="T649" id="Seg_11197" s="T648">v</ta>
            <ta e="T650" id="Seg_11198" s="T649">adv</ta>
            <ta e="T651" id="Seg_11199" s="T650">dempro</ta>
            <ta e="T652" id="Seg_11200" s="T651">adv</ta>
            <ta e="T653" id="Seg_11201" s="T652">v</ta>
            <ta e="T654" id="Seg_11202" s="T653">n</ta>
            <ta e="T657" id="Seg_11203" s="T656">conj</ta>
            <ta e="T658" id="Seg_11204" s="T657">adv</ta>
            <ta e="T659" id="Seg_11205" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_11206" s="T659">v</ta>
            <ta e="T661" id="Seg_11207" s="T660">adv</ta>
            <ta e="T662" id="Seg_11208" s="T661">dempro</ta>
            <ta e="T969" id="Seg_11209" s="T662">v</ta>
            <ta e="T663" id="Seg_11210" s="T969">v</ta>
            <ta e="T664" id="Seg_11211" s="T663">dempro</ta>
            <ta e="T665" id="Seg_11212" s="T664">n</ta>
            <ta e="T666" id="Seg_11213" s="T665">v</ta>
            <ta e="T667" id="Seg_11214" s="T666">quant</ta>
            <ta e="T668" id="Seg_11215" s="T667">quant</ta>
            <ta e="T669" id="Seg_11216" s="T668">que</ta>
            <ta e="T670" id="Seg_11217" s="T669">v</ta>
            <ta e="T671" id="Seg_11218" s="T670">n</ta>
            <ta e="T672" id="Seg_11219" s="T671">v</ta>
            <ta e="T673" id="Seg_11220" s="T672">n</ta>
            <ta e="T674" id="Seg_11221" s="T673">v</ta>
            <ta e="T675" id="Seg_11222" s="T674">n</ta>
            <ta e="T676" id="Seg_11223" s="T675">v</ta>
            <ta e="T677" id="Seg_11224" s="T676">n</ta>
            <ta e="T678" id="Seg_11225" s="T677">v</ta>
            <ta e="T680" id="Seg_11226" s="T679">n</ta>
            <ta e="T681" id="Seg_11227" s="T680">n</ta>
            <ta e="T682" id="Seg_11228" s="T681">dempro</ta>
            <ta e="T685" id="Seg_11229" s="T684">v</ta>
            <ta e="T687" id="Seg_11230" s="T686">n</ta>
            <ta e="T688" id="Seg_11231" s="T687">n</ta>
            <ta e="T689" id="Seg_11232" s="T688">refl</ta>
            <ta e="T690" id="Seg_11233" s="T689">n</ta>
            <ta e="T691" id="Seg_11234" s="T690">n</ta>
            <ta e="T692" id="Seg_11235" s="T691">conj</ta>
            <ta e="T693" id="Seg_11236" s="T692">n</ta>
            <ta e="T694" id="Seg_11237" s="T693">refl</ta>
            <ta e="T695" id="Seg_11238" s="T694">n</ta>
            <ta e="T696" id="Seg_11239" s="T695">n</ta>
            <ta e="T697" id="Seg_11240" s="T696">n</ta>
            <ta e="T698" id="Seg_11241" s="T697">n</ta>
            <ta e="T699" id="Seg_11242" s="T698">adv</ta>
            <ta e="T700" id="Seg_11243" s="T699">v</ta>
            <ta e="T701" id="Seg_11244" s="T700">adv</ta>
            <ta e="T702" id="Seg_11245" s="T701">v</ta>
            <ta e="T703" id="Seg_11246" s="T702">n</ta>
            <ta e="T704" id="Seg_11247" s="T703">conj</ta>
            <ta e="T705" id="Seg_11248" s="T704">adv</ta>
            <ta e="T706" id="Seg_11249" s="T705">n</ta>
            <ta e="T707" id="Seg_11250" s="T706">n</ta>
            <ta e="T708" id="Seg_11251" s="T707">n</ta>
            <ta e="T710" id="Seg_11252" s="T709">n</ta>
            <ta e="T711" id="Seg_11253" s="T710">v</ta>
            <ta e="T713" id="Seg_11254" s="T712">ptcl</ta>
            <ta e="T714" id="Seg_11255" s="T713">n</ta>
            <ta e="T715" id="Seg_11256" s="T714">v</ta>
            <ta e="T716" id="Seg_11257" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_11258" s="T716">n</ta>
            <ta e="T718" id="Seg_11259" s="T717">v</ta>
            <ta e="T719" id="Seg_11260" s="T718">adv</ta>
            <ta e="T720" id="Seg_11261" s="T719">v</ta>
            <ta e="T721" id="Seg_11262" s="T720">n</ta>
            <ta e="T722" id="Seg_11263" s="T721">dempro</ta>
            <ta e="T723" id="Seg_11264" s="T722">v</ta>
            <ta e="T725" id="Seg_11265" s="T724">n</ta>
            <ta e="T727" id="Seg_11266" s="T725">adv</ta>
            <ta e="T728" id="Seg_11267" s="T727">n</ta>
            <ta e="T729" id="Seg_11268" s="T728">n</ta>
            <ta e="T730" id="Seg_11269" s="T729">adj</ta>
            <ta e="T731" id="Seg_11270" s="T730">adj</ta>
            <ta e="T732" id="Seg_11271" s="T731">n</ta>
            <ta e="T733" id="Seg_11272" s="T732">n</ta>
            <ta e="T734" id="Seg_11273" s="T733">ptcl</ta>
            <ta e="T735" id="Seg_11274" s="T734">adj</ta>
            <ta e="T736" id="Seg_11275" s="T735">adj</ta>
            <ta e="T737" id="Seg_11276" s="T736">n</ta>
            <ta e="T738" id="Seg_11277" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_11278" s="T738">adj</ta>
            <ta e="T740" id="Seg_11279" s="T739">n</ta>
            <ta e="T741" id="Seg_11280" s="T740">v</ta>
            <ta e="T742" id="Seg_11281" s="T741">adv</ta>
            <ta e="T743" id="Seg_11282" s="T742">dempro</ta>
            <ta e="T747" id="Seg_11283" s="T746">adv</ta>
            <ta e="T748" id="Seg_11284" s="T747">dempro</ta>
            <ta e="T749" id="Seg_11285" s="T748">num</ta>
            <ta e="T750" id="Seg_11286" s="T749">n</ta>
            <ta e="T751" id="Seg_11287" s="T750">v</ta>
            <ta e="T752" id="Seg_11288" s="T751">adv</ta>
            <ta e="T753" id="Seg_11289" s="T752">v</ta>
            <ta e="T754" id="Seg_11290" s="T753">n</ta>
            <ta e="T755" id="Seg_11291" s="T754">conj</ta>
            <ta e="T756" id="Seg_11292" s="T755">dempro</ta>
            <ta e="T757" id="Seg_11293" s="T756">n</ta>
            <ta e="T758" id="Seg_11294" s="T757">v</ta>
            <ta e="T759" id="Seg_11295" s="T758">num</ta>
            <ta e="T761" id="Seg_11296" s="T760">n</ta>
            <ta e="T762" id="Seg_11297" s="T761">v</ta>
            <ta e="T763" id="Seg_11298" s="T762">adv</ta>
            <ta e="T765" id="Seg_11299" s="T764">n</ta>
            <ta e="T766" id="Seg_11300" s="T765">n</ta>
            <ta e="T767" id="Seg_11301" s="T766">dempro</ta>
            <ta e="T768" id="Seg_11302" s="T767">n</ta>
            <ta e="T769" id="Seg_11303" s="T768">n</ta>
            <ta e="T770" id="Seg_11304" s="T769">n</ta>
            <ta e="T771" id="Seg_11305" s="T770">conj</ta>
            <ta e="T772" id="Seg_11306" s="T771">n</ta>
            <ta e="T773" id="Seg_11307" s="T772">n</ta>
            <ta e="T774" id="Seg_11308" s="T773">n</ta>
            <ta e="T775" id="Seg_11309" s="T774">dempro</ta>
            <ta e="T776" id="Seg_11310" s="T775">num</ta>
            <ta e="T777" id="Seg_11311" s="T776">v</ta>
            <ta e="T778" id="Seg_11312" s="T777">conj</ta>
            <ta e="T779" id="Seg_11313" s="T778">v</ta>
            <ta e="T780" id="Seg_11314" s="T779">n</ta>
            <ta e="T781" id="Seg_11315" s="T780">dempro</ta>
            <ta e="T782" id="Seg_11316" s="T781">n</ta>
            <ta e="T783" id="Seg_11317" s="T782">n</ta>
            <ta e="T784" id="Seg_11318" s="T783">n</ta>
            <ta e="T785" id="Seg_11319" s="T784">n</ta>
            <ta e="T787" id="Seg_11320" s="T786">v</ta>
            <ta e="T788" id="Seg_11321" s="T787">n</ta>
            <ta e="T789" id="Seg_11322" s="T788">adv</ta>
            <ta e="T790" id="Seg_11323" s="T789">que</ta>
            <ta e="T791" id="Seg_11324" s="T790">ptcl</ta>
            <ta e="T792" id="Seg_11325" s="T791">v</ta>
            <ta e="T793" id="Seg_11326" s="T792">v</ta>
            <ta e="T795" id="Seg_11327" s="T794">v</ta>
            <ta e="T796" id="Seg_11328" s="T795">conj</ta>
            <ta e="T797" id="Seg_11329" s="T796">v</ta>
            <ta e="T799" id="Seg_11330" s="T798">n</ta>
            <ta e="T800" id="Seg_11331" s="T799">dempro</ta>
            <ta e="T801" id="Seg_11332" s="T800">v</ta>
            <ta e="T802" id="Seg_11333" s="T801">adj</ta>
            <ta e="T803" id="Seg_11334" s="T802">v</ta>
            <ta e="T804" id="Seg_11335" s="T803">v</ta>
            <ta e="T805" id="Seg_11336" s="T804">aux</ta>
            <ta e="T806" id="Seg_11337" s="T805">v</ta>
            <ta e="T807" id="Seg_11338" s="T806">aux</ta>
            <ta e="T809" id="Seg_11339" s="T808">aux</ta>
            <ta e="T811" id="Seg_11340" s="T810">aux</ta>
            <ta e="T812" id="Seg_11341" s="T811">v</ta>
            <ta e="T813" id="Seg_11342" s="T812">ptcl</ta>
            <ta e="T815" id="Seg_11343" s="T814">n</ta>
            <ta e="T816" id="Seg_11344" s="T815">quant</ta>
            <ta e="T817" id="Seg_11345" s="T816">v</ta>
            <ta e="T818" id="Seg_11346" s="T817">n</ta>
            <ta e="T819" id="Seg_11347" s="T818">n</ta>
            <ta e="T820" id="Seg_11348" s="T819">adv</ta>
            <ta e="T821" id="Seg_11349" s="T820">dempro</ta>
            <ta e="T822" id="Seg_11350" s="T821">n</ta>
            <ta e="T823" id="Seg_11351" s="T822">v</ta>
            <ta e="T824" id="Seg_11352" s="T823">dempro</ta>
            <ta e="T825" id="Seg_11353" s="T824">adv</ta>
            <ta e="T826" id="Seg_11354" s="T825">quant</ta>
            <ta e="T827" id="Seg_11355" s="T826">n</ta>
            <ta e="T828" id="Seg_11356" s="T827">v</ta>
            <ta e="T829" id="Seg_11357" s="T828">conj</ta>
            <ta e="T830" id="Seg_11358" s="T829">v</ta>
            <ta e="T831" id="Seg_11359" s="T830">dempro</ta>
            <ta e="T832" id="Seg_11360" s="T831">v</ta>
            <ta e="T833" id="Seg_11361" s="T832">dempro</ta>
            <ta e="T834" id="Seg_11362" s="T833">adv</ta>
            <ta e="T835" id="Seg_11363" s="T834">n</ta>
            <ta e="T836" id="Seg_11364" s="T835">v</ta>
            <ta e="T837" id="Seg_11365" s="T836">adv</ta>
            <ta e="T838" id="Seg_11366" s="T837">num</ta>
            <ta e="T839" id="Seg_11367" s="T838">n</ta>
            <ta e="T840" id="Seg_11368" s="T839">v</ta>
            <ta e="T841" id="Seg_11369" s="T840">adv</ta>
            <ta e="T842" id="Seg_11370" s="T841">dempro</ta>
            <ta e="T843" id="Seg_11371" s="T842">ptcl</ta>
            <ta e="T844" id="Seg_11372" s="T843">v</ta>
            <ta e="T845" id="Seg_11373" s="T844">dempro</ta>
            <ta e="T846" id="Seg_11374" s="T845">ptcl</ta>
            <ta e="T847" id="Seg_11375" s="T846">n</ta>
            <ta e="T848" id="Seg_11376" s="T847">v</ta>
            <ta e="T849" id="Seg_11377" s="T848">n</ta>
            <ta e="T850" id="Seg_11378" s="T849">v</ta>
            <ta e="T852" id="Seg_11379" s="T851">v</ta>
            <ta e="T853" id="Seg_11380" s="T852">n</ta>
            <ta e="T857" id="Seg_11381" s="T855">pers</ta>
            <ta e="T858" id="Seg_11382" s="T857">n</ta>
            <ta e="T859" id="Seg_11383" s="T858">adv</ta>
            <ta e="T860" id="Seg_11384" s="T859">v</ta>
            <ta e="T861" id="Seg_11385" s="T860">n</ta>
            <ta e="T862" id="Seg_11386" s="T861">v</ta>
            <ta e="T863" id="Seg_11387" s="T862">adv</ta>
            <ta e="T864" id="Seg_11388" s="T863">adv</ta>
            <ta e="T865" id="Seg_11389" s="T864">ptcl</ta>
            <ta e="T866" id="Seg_11390" s="T865">v</ta>
            <ta e="T867" id="Seg_11391" s="T866">n</ta>
            <ta e="T868" id="Seg_11392" s="T867">adv</ta>
            <ta e="T869" id="Seg_11393" s="T868">v</ta>
            <ta e="T870" id="Seg_11394" s="T869">v</ta>
            <ta e="T871" id="Seg_11395" s="T870">conj</ta>
            <ta e="T872" id="Seg_11396" s="T871">n</ta>
            <ta e="T873" id="Seg_11397" s="T872">v</ta>
            <ta e="T874" id="Seg_11398" s="T873">conj</ta>
            <ta e="T875" id="Seg_11399" s="T874">v</ta>
            <ta e="T876" id="Seg_11400" s="T875">v</ta>
            <ta e="T877" id="Seg_11401" s="T876">conj</ta>
            <ta e="T878" id="Seg_11402" s="T877">pers</ta>
            <ta e="T880" id="Seg_11403" s="T879">pers</ta>
            <ta e="T881" id="Seg_11404" s="T880">num</ta>
            <ta e="T882" id="Seg_11405" s="T881">n</ta>
            <ta e="T883" id="Seg_11406" s="T882">v</ta>
            <ta e="T884" id="Seg_11407" s="T883">dempro</ta>
            <ta e="T885" id="Seg_11408" s="T884">n</ta>
            <ta e="T886" id="Seg_11409" s="T885">conj</ta>
            <ta e="T887" id="Seg_11410" s="T886">refl</ta>
            <ta e="T888" id="Seg_11411" s="T887">n</ta>
            <ta e="T889" id="Seg_11412" s="T888">num</ta>
            <ta e="T890" id="Seg_11413" s="T889">n</ta>
            <ta e="T891" id="Seg_11414" s="T890">adj</ta>
            <ta e="T892" id="Seg_11415" s="T891">v</ta>
            <ta e="T893" id="Seg_11416" s="T892">adv</ta>
            <ta e="T894" id="Seg_11417" s="T893">v</ta>
            <ta e="T895" id="Seg_11418" s="T894">conj</ta>
            <ta e="T896" id="Seg_11419" s="T895">dempro</ta>
            <ta e="T897" id="Seg_11420" s="T896">adv</ta>
            <ta e="T898" id="Seg_11421" s="T897">v</ta>
            <ta e="T899" id="Seg_11422" s="T898">conj</ta>
            <ta e="T900" id="Seg_11423" s="T899">v</ta>
            <ta e="T901" id="Seg_11424" s="T900">n</ta>
            <ta e="T902" id="Seg_11425" s="T901">pers</ta>
            <ta e="T903" id="Seg_11426" s="T902">adv</ta>
            <ta e="T906" id="Seg_11427" s="T905">v</ta>
            <ta e="T907" id="Seg_11428" s="T906">conj</ta>
            <ta e="T908" id="Seg_11429" s="T907">adv</ta>
            <ta e="T910" id="Seg_11430" s="T908">n</ta>
            <ta e="T911" id="Seg_11431" s="T910">adv</ta>
            <ta e="T913" id="Seg_11432" s="T911">n</ta>
            <ta e="T914" id="Seg_11433" s="T913">adv</ta>
            <ta e="T915" id="Seg_11434" s="T914">dempro</ta>
            <ta e="T917" id="Seg_11435" s="T916">v</ta>
            <ta e="T918" id="Seg_11436" s="T917">que</ta>
            <ta e="T919" id="Seg_11437" s="T918">v</ta>
            <ta e="T920" id="Seg_11438" s="T919">conj</ta>
            <ta e="T921" id="Seg_11439" s="T920">pers</ta>
            <ta e="T922" id="Seg_11440" s="T921">v</ta>
            <ta e="T923" id="Seg_11441" s="T922">ptcl</ta>
            <ta e="T925" id="Seg_11442" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_11443" s="T925">v</ta>
            <ta e="T927" id="Seg_11444" s="T926">n</ta>
            <ta e="T928" id="Seg_11445" s="T927">conj</ta>
            <ta e="T929" id="Seg_11446" s="T928">dempro</ta>
            <ta e="T930" id="Seg_11447" s="T929">v</ta>
            <ta e="T931" id="Seg_11448" s="T930">adv</ta>
            <ta e="T932" id="Seg_11449" s="T931">v</ta>
            <ta e="T933" id="Seg_11450" s="T932">n</ta>
            <ta e="T934" id="Seg_11451" s="T933">v</ta>
            <ta e="T935" id="Seg_11452" s="T934">ptcl</ta>
            <ta e="T936" id="Seg_11453" s="T935">dempro</ta>
            <ta e="T937" id="Seg_11454" s="T936">adv</ta>
            <ta e="T938" id="Seg_11455" s="T937">v</ta>
            <ta e="T939" id="Seg_11456" s="T938">dempro</ta>
            <ta e="T940" id="Seg_11457" s="T939">ptcl</ta>
            <ta e="T941" id="Seg_11458" s="T940">v</ta>
            <ta e="T942" id="Seg_11459" s="T941">pers</ta>
            <ta e="T943" id="Seg_11460" s="T942">adv</ta>
            <ta e="T944" id="Seg_11461" s="T943">v</ta>
            <ta e="T945" id="Seg_11462" s="T944">pers</ta>
            <ta e="T946" id="Seg_11463" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_11464" s="T946">v</ta>
            <ta e="T948" id="Seg_11465" s="T947">conj</ta>
            <ta e="T949" id="Seg_11466" s="T948">pers</ta>
            <ta e="T950" id="Seg_11467" s="T949">ptcl</ta>
            <ta e="T951" id="Seg_11468" s="T950">v</ta>
            <ta e="T952" id="Seg_11469" s="T951">pers</ta>
            <ta e="T953" id="Seg_11470" s="T952">adv</ta>
            <ta e="T955" id="Seg_11471" s="T954">adv</ta>
            <ta e="T956" id="Seg_11472" s="T955">n</ta>
            <ta e="T957" id="Seg_11473" s="T956">v</ta>
            <ta e="T958" id="Seg_11474" s="T957">n</ta>
            <ta e="T959" id="Seg_11475" s="T958">v</ta>
            <ta e="T960" id="Seg_11476" s="T959">n</ta>
            <ta e="T961" id="Seg_11477" s="T960">v</ta>
            <ta e="T963" id="Seg_11478" s="T962">v</ta>
            <ta e="T964" id="Seg_11479" s="T963">pers</ta>
            <ta e="T965" id="Seg_11480" s="T964">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T6" id="Seg_11481" s="T5">np:E</ta>
            <ta e="T8" id="Seg_11482" s="T7">np:G</ta>
            <ta e="T10" id="Seg_11483" s="T9">pro:A</ta>
            <ta e="T11" id="Seg_11484" s="T10">adv:Time</ta>
            <ta e="T15" id="Seg_11485" s="T14">pro:A</ta>
            <ta e="T17" id="Seg_11486" s="T16">0.3:A</ta>
            <ta e="T18" id="Seg_11487" s="T17">adv:Time</ta>
            <ta e="T19" id="Seg_11488" s="T18">pro:A</ta>
            <ta e="T20" id="Seg_11489" s="T19">np:So</ta>
            <ta e="T22" id="Seg_11490" s="T21">adv:Time</ta>
            <ta e="T23" id="Seg_11491" s="T22">pro:A</ta>
            <ta e="T27" id="Seg_11492" s="T26">pro.h:A</ta>
            <ta e="T28" id="Seg_11493" s="T27">pro:G</ta>
            <ta e="T34" id="Seg_11494" s="T33">pro.h:A</ta>
            <ta e="T35" id="Seg_11495" s="T34">pro:G</ta>
            <ta e="T36" id="Seg_11496" s="T35">n:Time</ta>
            <ta e="T39" id="Seg_11497" s="T38">pro.h:Th</ta>
            <ta e="T44" id="Seg_11498" s="T43">0.2.h:A</ta>
            <ta e="T46" id="Seg_11499" s="T45">np.h:Th</ta>
            <ta e="T53" id="Seg_11500" s="T52">0.2.h:A</ta>
            <ta e="T55" id="Seg_11501" s="T54">0.2.h:A</ta>
            <ta e="T56" id="Seg_11502" s="T55">np:G</ta>
            <ta e="T58" id="Seg_11503" s="T57">np:Th</ta>
            <ta e="T60" id="Seg_11504" s="T59">np:Th</ta>
            <ta e="T63" id="Seg_11505" s="T62">np:Th</ta>
            <ta e="T66" id="Seg_11506" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_11507" s="T66">adv:G</ta>
            <ta e="T69" id="Seg_11508" s="T68">pro.h:A</ta>
            <ta e="T70" id="Seg_11509" s="T69">np:G</ta>
            <ta e="T72" id="Seg_11510" s="T71">np:L</ta>
            <ta e="T73" id="Seg_11511" s="T72">0.1.h:Th</ta>
            <ta e="T74" id="Seg_11512" s="T73">adv:Time</ta>
            <ta e="T76" id="Seg_11513" s="T967">0.1.h:A</ta>
            <ta e="T78" id="Seg_11514" s="T77">np:G</ta>
            <ta e="T79" id="Seg_11515" s="T78">pro.h:Th</ta>
            <ta e="T82" id="Seg_11516" s="T81">np:Th</ta>
            <ta e="T85" id="Seg_11517" s="T84">pro.h:Poss</ta>
            <ta e="T92" id="Seg_11518" s="T91">0.1.h:A</ta>
            <ta e="T94" id="Seg_11519" s="T93">0.3:A</ta>
            <ta e="T95" id="Seg_11520" s="T94">pro.h:A</ta>
            <ta e="T97" id="Seg_11521" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_11522" s="T97">pro:A</ta>
            <ta e="T101" id="Seg_11523" s="T100">pro.h:Com</ta>
            <ta e="T102" id="Seg_11524" s="T101">0.1.h:A</ta>
            <ta e="T103" id="Seg_11525" s="T102">np:G</ta>
            <ta e="T104" id="Seg_11526" s="T103">0.1.h:Th</ta>
            <ta e="T105" id="Seg_11527" s="T104">np:L</ta>
            <ta e="T106" id="Seg_11528" s="T105">adv:Time</ta>
            <ta e="T108" id="Seg_11529" s="T107">0.1.h:A</ta>
            <ta e="T109" id="Seg_11530" s="T108">np:G</ta>
            <ta e="T110" id="Seg_11531" s="T109">np:P</ta>
            <ta e="T114" id="Seg_11532" s="T113">np.h:A</ta>
            <ta e="T117" id="Seg_11533" s="T116">np:Th</ta>
            <ta e="T118" id="Seg_11534" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_11535" s="T118">np:G</ta>
            <ta e="T120" id="Seg_11536" s="T119">0.3.h:A</ta>
            <ta e="T124" id="Seg_11537" s="T123">0.3:P</ta>
            <ta e="T126" id="Seg_11538" s="T125">np.h:A</ta>
            <ta e="T128" id="Seg_11539" s="T127">np.h:Poss</ta>
            <ta e="T130" id="Seg_11540" s="T129">np:P</ta>
            <ta e="T133" id="Seg_11541" s="T132">0.3.h:A</ta>
            <ta e="T139" id="Seg_11542" s="T138">pro.h:A</ta>
            <ta e="T142" id="Seg_11543" s="T141">np:P</ta>
            <ta e="T143" id="Seg_11544" s="T142">0.3.h:A</ta>
            <ta e="T144" id="Seg_11545" s="T143">0.3.h:A</ta>
            <ta e="T146" id="Seg_11546" s="T145">pro.h:Com</ta>
            <ta e="T147" id="Seg_11547" s="T146">pro.h:E</ta>
            <ta e="T149" id="Seg_11548" s="T148">np:P</ta>
            <ta e="T150" id="Seg_11549" s="T149">0.1.h:A</ta>
            <ta e="T151" id="Seg_11550" s="T150">np:G</ta>
            <ta e="T152" id="Seg_11551" s="T151">0.1.h:A</ta>
            <ta e="T153" id="Seg_11552" s="T152">0.1.h:A</ta>
            <ta e="T155" id="Seg_11553" s="T154">pro.h:P</ta>
            <ta e="T158" id="Seg_11554" s="T157">np:G</ta>
            <ta e="T159" id="Seg_11555" s="T158">0.1.h:A</ta>
            <ta e="T160" id="Seg_11556" s="T159">adv:L</ta>
            <ta e="T162" id="Seg_11557" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_11558" s="T162">adv:Time</ta>
            <ta e="T165" id="Seg_11559" s="T164">np:G</ta>
            <ta e="T166" id="Seg_11560" s="T968">0.1.h:A</ta>
            <ta e="T167" id="Seg_11561" s="T166">adv:Time</ta>
            <ta e="T168" id="Seg_11562" s="T167">pro.h:A</ta>
            <ta e="T172" id="Seg_11563" s="T171">pro.h:Com</ta>
            <ta e="T173" id="Seg_11564" s="T172">0.1.h:A</ta>
            <ta e="T178" id="Seg_11565" s="T177">0.1.h:E</ta>
            <ta e="T181" id="Seg_11566" s="T180">np:Th</ta>
            <ta e="T184" id="Seg_11567" s="T183">0.3:Th</ta>
            <ta e="T187" id="Seg_11568" s="T186">pro.h:Th</ta>
            <ta e="T191" id="Seg_11569" s="T190">np:A</ta>
            <ta e="T194" id="Seg_11570" s="T193">pro.h:Poss</ta>
            <ta e="T195" id="Seg_11571" s="T194">np:Th</ta>
            <ta e="T197" id="Seg_11572" s="T196">adv:L</ta>
            <ta e="T198" id="Seg_11573" s="T197">0.1.h:Th</ta>
            <ta e="T199" id="Seg_11574" s="T198">pro:G</ta>
            <ta e="T201" id="Seg_11575" s="T200">0.3:Th</ta>
            <ta e="T204" id="Seg_11576" s="T203">0.3:Th</ta>
            <ta e="T206" id="Seg_11577" s="T205">pro.h:A</ta>
            <ta e="T207" id="Seg_11578" s="T206">np:G</ta>
            <ta e="T209" id="Seg_11579" s="T208">np:Th</ta>
            <ta e="T211" id="Seg_11580" s="T210">0.1.h:A</ta>
            <ta e="T213" id="Seg_11581" s="T212">adv:Time</ta>
            <ta e="T214" id="Seg_11582" s="T213">0.1.h:E</ta>
            <ta e="T215" id="Seg_11583" s="T214">pro.h:Poss</ta>
            <ta e="T216" id="Seg_11584" s="T215">np.h:P</ta>
            <ta e="T218" id="Seg_11585" s="T217">pro.h:E</ta>
            <ta e="T220" id="Seg_11586" s="T219">np.h:E</ta>
            <ta e="T222" id="Seg_11587" s="T221">np.h:E</ta>
            <ta e="T225" id="Seg_11588" s="T224">pro.h:Th</ta>
            <ta e="T228" id="Seg_11589" s="T227">pro:L</ta>
            <ta e="T229" id="Seg_11590" s="T228">0.2.h:Th</ta>
            <ta e="T230" id="Seg_11591" s="T229">pro:L</ta>
            <ta e="T231" id="Seg_11592" s="T230">0.2.h:A</ta>
            <ta e="T232" id="Seg_11593" s="T231">np:G</ta>
            <ta e="T233" id="Seg_11594" s="T232">0.1.h:A</ta>
            <ta e="T235" id="Seg_11595" s="T234">np:Th</ta>
            <ta e="T242" id="Seg_11596" s="T241">np:Th</ta>
            <ta e="T247" id="Seg_11597" s="T246">np:Ins</ta>
            <ta e="T248" id="Seg_11598" s="T247">pro:Th</ta>
            <ta e="T249" id="Seg_11599" s="T248">0.2.h:A</ta>
            <ta e="T250" id="Seg_11600" s="T249">np:Th</ta>
            <ta e="T253" id="Seg_11601" s="T252">np:Th</ta>
            <ta e="T255" id="Seg_11602" s="T254">np:Th</ta>
            <ta e="T257" id="Seg_11603" s="T256">np:Th</ta>
            <ta e="T258" id="Seg_11604" s="T257">0.2.h:A</ta>
            <ta e="T259" id="Seg_11605" s="T258">np:Ins</ta>
            <ta e="T261" id="Seg_11606" s="T260">np:Th</ta>
            <ta e="T264" id="Seg_11607" s="T263">pro:Th</ta>
            <ta e="T266" id="Seg_11608" s="T265">0.2.h:A</ta>
            <ta e="T267" id="Seg_11609" s="T266">np:Th</ta>
            <ta e="T269" id="Seg_11610" s="T268">0.2.h:A</ta>
            <ta e="T270" id="Seg_11611" s="T269">np:Th</ta>
            <ta e="T273" id="Seg_11612" s="T272">0.2.h:A</ta>
            <ta e="T275" id="Seg_11613" s="T274">0.2.h:A</ta>
            <ta e="T277" id="Seg_11614" s="T276">np:G</ta>
            <ta e="T278" id="Seg_11615" s="T277">0.2.h:A</ta>
            <ta e="T279" id="Seg_11616" s="T278">np.h:Th</ta>
            <ta e="T280" id="Seg_11617" s="T279">0.2.h:A</ta>
            <ta e="T282" id="Seg_11618" s="T281">0.3.h:A</ta>
            <ta e="T285" id="Seg_11619" s="T284">0.2.h:A</ta>
            <ta e="T287" id="Seg_11620" s="T286">0.2.h:A</ta>
            <ta e="T290" id="Seg_11621" s="T289">0.3.h:A</ta>
            <ta e="T295" id="Seg_11622" s="T294">np:Ins</ta>
            <ta e="T297" id="Seg_11623" s="T296">0.1.h:A</ta>
            <ta e="T302" id="Seg_11624" s="T301">0.2.h:A</ta>
            <ta e="T304" id="Seg_11625" s="T303">0.2.h:A</ta>
            <ta e="T306" id="Seg_11626" s="T305">0.1.h:E</ta>
            <ta e="T308" id="Seg_11627" s="T307">pro.h:A</ta>
            <ta e="T311" id="Seg_11628" s="T310">pro:G</ta>
            <ta e="T312" id="Seg_11629" s="T311">pro:G</ta>
            <ta e="T314" id="Seg_11630" s="T313">0.3.h:A</ta>
            <ta e="T316" id="Seg_11631" s="T315">pro.h:A</ta>
            <ta e="T319" id="Seg_11632" s="T318">np:L</ta>
            <ta e="T321" id="Seg_11633" s="T320">np:P</ta>
            <ta e="T329" id="Seg_11634" s="T328">0.3:Th</ta>
            <ta e="T331" id="Seg_11635" s="T330">0.1.h:E</ta>
            <ta e="T333" id="Seg_11636" s="T332">0.3:Th</ta>
            <ta e="T334" id="Seg_11637" s="T333">pro:P</ta>
            <ta e="T335" id="Seg_11638" s="T334">0.3.h:A</ta>
            <ta e="T336" id="Seg_11639" s="T335">np:A</ta>
            <ta e="T338" id="Seg_11640" s="T337">np:L</ta>
            <ta e="T340" id="Seg_11641" s="T339">np:P</ta>
            <ta e="T341" id="Seg_11642" s="T340">0.3.h:A</ta>
            <ta e="T343" id="Seg_11643" s="T342">np:Ins 0.3.h:Poss</ta>
            <ta e="T348" id="Seg_11644" s="T347">np.h:Th</ta>
            <ta e="T354" id="Seg_11645" s="T353">pro.h:A</ta>
            <ta e="T358" id="Seg_11646" s="T357">np:P</ta>
            <ta e="T359" id="Seg_11647" s="T358">0.3.h:A</ta>
            <ta e="T360" id="Seg_11648" s="T359">pro.h:A</ta>
            <ta e="T362" id="Seg_11649" s="T361">pro.h:Th</ta>
            <ta e="T365" id="Seg_11650" s="T364">pro.h:A</ta>
            <ta e="T367" id="Seg_11651" s="T366">adv:Time</ta>
            <ta e="T368" id="Seg_11652" s="T367">pro.h:A</ta>
            <ta e="T372" id="Seg_11653" s="T371">pro:Th</ta>
            <ta e="T373" id="Seg_11654" s="T372">0.2.h:A</ta>
            <ta e="T375" id="Seg_11655" s="T374">0.1.h:A</ta>
            <ta e="T378" id="Seg_11656" s="T377">pro.h:A</ta>
            <ta e="T380" id="Seg_11657" s="T379">np:A</ta>
            <ta e="T384" id="Seg_11658" s="T383">np:A</ta>
            <ta e="T387" id="Seg_11659" s="T386">np:A</ta>
            <ta e="T389" id="Seg_11660" s="T388">pro.h:Poss</ta>
            <ta e="T390" id="Seg_11661" s="T389">np.h:A</ta>
            <ta e="T394" id="Seg_11662" s="T393">0.3.h:Th</ta>
            <ta e="T395" id="Seg_11663" s="T394">np:Th</ta>
            <ta e="T397" id="Seg_11664" s="T396">0.3.h:A</ta>
            <ta e="T399" id="Seg_11665" s="T398">0.1.h:E</ta>
            <ta e="T400" id="Seg_11666" s="T399">0.3.h:A</ta>
            <ta e="T403" id="Seg_11667" s="T402">0.3.h:A</ta>
            <ta e="T404" id="Seg_11668" s="T403">0.3.h:A</ta>
            <ta e="T409" id="Seg_11669" s="T408">np.h:Th</ta>
            <ta e="T414" id="Seg_11670" s="T413">np.h:Th</ta>
            <ta e="T415" id="Seg_11671" s="T414">0.3.h:A</ta>
            <ta e="T417" id="Seg_11672" s="T416">pro.h:E</ta>
            <ta e="T421" id="Seg_11673" s="T420">pro:Th</ta>
            <ta e="T429" id="Seg_11674" s="T428">np:A</ta>
            <ta e="T432" id="Seg_11675" s="T431">pro.h:A</ta>
            <ta e="T435" id="Seg_11676" s="T434">np:A</ta>
            <ta e="T437" id="Seg_11677" s="T436">pro.h:A</ta>
            <ta e="T440" id="Seg_11678" s="T439">np:Th</ta>
            <ta e="T456" id="Seg_11679" s="T455">np:A</ta>
            <ta e="T461" id="Seg_11680" s="T460">np:Th</ta>
            <ta e="T465" id="Seg_11681" s="T464">pro.h:A</ta>
            <ta e="T466" id="Seg_11682" s="T465">np:Th</ta>
            <ta e="T467" id="Seg_11683" s="T466">np:G</ta>
            <ta e="T470" id="Seg_11684" s="T469">np:A</ta>
            <ta e="T472" id="Seg_11685" s="T471">np:Th</ta>
            <ta e="T475" id="Seg_11686" s="T474">np:A</ta>
            <ta e="T477" id="Seg_11687" s="T476">np:P</ta>
            <ta e="T482" id="Seg_11688" s="T481">0.2.h:A</ta>
            <ta e="T483" id="Seg_11689" s="T482">pro.h:Poss</ta>
            <ta e="T484" id="Seg_11690" s="T483">np:Th</ta>
            <ta e="T485" id="Seg_11691" s="T484">pro:Th</ta>
            <ta e="T490" id="Seg_11692" s="T489">pro:Th</ta>
            <ta e="T492" id="Seg_11693" s="T491">np.h:P</ta>
            <ta e="T493" id="Seg_11694" s="T492">np.h:P</ta>
            <ta e="T496" id="Seg_11695" s="T495">adv:Time</ta>
            <ta e="T497" id="Seg_11696" s="T496">np.h:A</ta>
            <ta e="T500" id="Seg_11697" s="T499">pro.h:Th</ta>
            <ta e="T501" id="Seg_11698" s="T500">0.2.h:A</ta>
            <ta e="T503" id="Seg_11699" s="T502">0.1.h:A</ta>
            <ta e="T505" id="Seg_11700" s="T504">pro.h:Poss</ta>
            <ta e="T506" id="Seg_11701" s="T505">np.h:Th</ta>
            <ta e="T507" id="Seg_11702" s="T506">np.h:Th 0.1.h:Poss</ta>
            <ta e="T508" id="Seg_11703" s="T507">0.1.h:A</ta>
            <ta e="T509" id="Seg_11704" s="T508">pro:G</ta>
            <ta e="T510" id="Seg_11705" s="T509">adv:Time</ta>
            <ta e="T512" id="Seg_11706" s="T511">np.h:Th</ta>
            <ta e="T513" id="Seg_11707" s="T512">np.h:Th</ta>
            <ta e="T514" id="Seg_11708" s="T513">0.3.h:A</ta>
            <ta e="T515" id="Seg_11709" s="T514">pro.h:A</ta>
            <ta e="T517" id="Seg_11710" s="T516">np.h:Poss</ta>
            <ta e="T518" id="Seg_11711" s="T517">np.h:Com</ta>
            <ta e="T519" id="Seg_11712" s="T518">np.h:Com</ta>
            <ta e="T520" id="Seg_11713" s="T519">0.3.h:A</ta>
            <ta e="T521" id="Seg_11714" s="T520">np.h:Th 0.2.h:Poss</ta>
            <ta e="T523" id="Seg_11715" s="T522">0.2.h:A</ta>
            <ta e="T526" id="Seg_11716" s="T525">pro.h:Poss</ta>
            <ta e="T527" id="Seg_11717" s="T526">np.h:R</ta>
            <ta e="T529" id="Seg_11718" s="T528">0.1.h:A</ta>
            <ta e="T531" id="Seg_11719" s="T530">adv:Time</ta>
            <ta e="T532" id="Seg_11720" s="T531">np.h:R</ta>
            <ta e="T533" id="Seg_11721" s="T532">0.3.h:A</ta>
            <ta e="T534" id="Seg_11722" s="T533">pro.h:A</ta>
            <ta e="T536" id="Seg_11723" s="T535">pro.h:Poss</ta>
            <ta e="T537" id="Seg_11724" s="T536">np.h:Th</ta>
            <ta e="T539" id="Seg_11725" s="T538">0.1.h:A</ta>
            <ta e="T540" id="Seg_11726" s="T539">adv:Time</ta>
            <ta e="T541" id="Seg_11727" s="T540">0.3.h:A</ta>
            <ta e="T542" id="Seg_11728" s="T541">np:Th</ta>
            <ta e="T543" id="Seg_11729" s="T542">0.3.h:A</ta>
            <ta e="T544" id="Seg_11730" s="T543">np:Th</ta>
            <ta e="T545" id="Seg_11731" s="T544">0.3.h:A</ta>
            <ta e="T546" id="Seg_11732" s="T545">np:Th</ta>
            <ta e="T548" id="Seg_11733" s="T547">0.3:Th</ta>
            <ta e="T549" id="Seg_11734" s="T548">adv:Time</ta>
            <ta e="T550" id="Seg_11735" s="T549">np.h:A</ta>
            <ta e="T552" id="Seg_11736" s="T551">np:P</ta>
            <ta e="T553" id="Seg_11737" s="T552">0.3.h:A</ta>
            <ta e="T555" id="Seg_11738" s="T554">adv:Time</ta>
            <ta e="T558" id="Seg_11739" s="T557">np:A</ta>
            <ta e="T560" id="Seg_11740" s="T559">pro.h:A</ta>
            <ta e="T561" id="Seg_11741" s="T560">np:Th</ta>
            <ta e="T564" id="Seg_11742" s="T563">np:G</ta>
            <ta e="T565" id="Seg_11743" s="T564">0.3.h:A</ta>
            <ta e="T566" id="Seg_11744" s="T565">adv:L</ta>
            <ta e="T567" id="Seg_11745" s="T566">np.h:A</ta>
            <ta e="T569" id="Seg_11746" s="T568">np:R</ta>
            <ta e="T571" id="Seg_11747" s="T570">adv:Time</ta>
            <ta e="T574" id="Seg_11748" s="T573">np:Th</ta>
            <ta e="T575" id="Seg_11749" s="T574">0.3.h:A</ta>
            <ta e="T576" id="Seg_11750" s="T575">adv:Time</ta>
            <ta e="T577" id="Seg_11751" s="T576">np:G</ta>
            <ta e="T578" id="Seg_11752" s="T577">0.3.h:A</ta>
            <ta e="T581" id="Seg_11753" s="T580">np:P</ta>
            <ta e="T582" id="Seg_11754" s="T581">0.3.h:A</ta>
            <ta e="T583" id="Seg_11755" s="T582">np.h:Poss</ta>
            <ta e="T584" id="Seg_11756" s="T583">np.h:A</ta>
            <ta e="T587" id="Seg_11757" s="T586">np.h:Poss</ta>
            <ta e="T588" id="Seg_11758" s="T587">np.h:A</ta>
            <ta e="T591" id="Seg_11759" s="T590">np.h:Th</ta>
            <ta e="T593" id="Seg_11760" s="T592">np:Th</ta>
            <ta e="T596" id="Seg_11761" s="T595">np:L</ta>
            <ta e="T599" id="Seg_11762" s="T598">pro:Th</ta>
            <ta e="T602" id="Seg_11763" s="T601">n:Time</ta>
            <ta e="T603" id="Seg_11764" s="T602">np.h:A</ta>
            <ta e="T605" id="Seg_11765" s="T604">np.h:A 0.3.h:Poss</ta>
            <ta e="T606" id="Seg_11766" s="T605">np.h:A 0.3.h:Poss</ta>
            <ta e="T608" id="Seg_11767" s="T607">pro.h:Poss</ta>
            <ta e="T609" id="Seg_11768" s="T608">np:Th</ta>
            <ta e="T612" id="Seg_11769" s="T611">np:Th</ta>
            <ta e="T615" id="Seg_11770" s="T614">np:Th</ta>
            <ta e="T617" id="Seg_11771" s="T616">np:Th</ta>
            <ta e="T619" id="Seg_11772" s="T618">np:Th 0.3.h:Poss</ta>
            <ta e="T621" id="Seg_11773" s="T620">np:Th 0.3.h:Poss</ta>
            <ta e="T623" id="Seg_11774" s="T622">adv:Time</ta>
            <ta e="T625" id="Seg_11775" s="T624">pro.h:A</ta>
            <ta e="T626" id="Seg_11776" s="T625">np:Th</ta>
            <ta e="T631" id="Seg_11777" s="T630">adv:Time</ta>
            <ta e="T632" id="Seg_11778" s="T631">np.h:A 0.3.h:Poss</ta>
            <ta e="T633" id="Seg_11779" s="T632">np.h:A</ta>
            <ta e="T634" id="Seg_11780" s="T633">pro.h:R</ta>
            <ta e="T636" id="Seg_11781" s="T635">np:Th</ta>
            <ta e="T637" id="Seg_11782" s="T636">np.h:A 0.3.h:Poss</ta>
            <ta e="T639" id="Seg_11783" s="T638">np:Th</ta>
            <ta e="T640" id="Seg_11784" s="T639">0.3.h:A</ta>
            <ta e="T641" id="Seg_11785" s="T640">np:Th</ta>
            <ta e="T642" id="Seg_11786" s="T641">np:Th</ta>
            <ta e="T643" id="Seg_11787" s="T642">0.3.h:A</ta>
            <ta e="T647" id="Seg_11788" s="T646">np.h:Th</ta>
            <ta e="T650" id="Seg_11789" s="T649">adv:Time</ta>
            <ta e="T651" id="Seg_11790" s="T650">pro.h:E</ta>
            <ta e="T660" id="Seg_11791" s="T659">0.3.h:A</ta>
            <ta e="T661" id="Seg_11792" s="T660">adv:Time</ta>
            <ta e="T662" id="Seg_11793" s="T661">pro.h:A</ta>
            <ta e="T664" id="Seg_11794" s="T663">pro.h:R</ta>
            <ta e="T665" id="Seg_11795" s="T664">np.h:A 0.3.h:Poss</ta>
            <ta e="T667" id="Seg_11796" s="T666">np:Th</ta>
            <ta e="T668" id="Seg_11797" s="T667">np:Th</ta>
            <ta e="T670" id="Seg_11798" s="T669">0.3.h:A</ta>
            <ta e="T671" id="Seg_11799" s="T670">np:Th</ta>
            <ta e="T672" id="Seg_11800" s="T671">0.3.h:A</ta>
            <ta e="T673" id="Seg_11801" s="T672">np:Th</ta>
            <ta e="T674" id="Seg_11802" s="T673">0.3.h:A</ta>
            <ta e="T675" id="Seg_11803" s="T674">np:Th</ta>
            <ta e="T676" id="Seg_11804" s="T675">0.3.h:A</ta>
            <ta e="T677" id="Seg_11805" s="T676">np:Th</ta>
            <ta e="T678" id="Seg_11806" s="T677">0.3.h:A</ta>
            <ta e="T680" id="Seg_11807" s="T679">np:P</ta>
            <ta e="T681" id="Seg_11808" s="T680">0.3.h:A</ta>
            <ta e="T682" id="Seg_11809" s="T681">pro.h:A</ta>
            <ta e="T687" id="Seg_11810" s="T686">np:Ins</ta>
            <ta e="T688" id="Seg_11811" s="T687">np.h:Poss</ta>
            <ta e="T689" id="Seg_11812" s="T688">pro.h:Poss</ta>
            <ta e="T693" id="Seg_11813" s="T692">np.h:Poss</ta>
            <ta e="T694" id="Seg_11814" s="T693">pro.h:Poss</ta>
            <ta e="T699" id="Seg_11815" s="T698">adv:L</ta>
            <ta e="T700" id="Seg_11816" s="T699">0.3.h:A</ta>
            <ta e="T701" id="Seg_11817" s="T700">adv:Time</ta>
            <ta e="T702" id="Seg_11818" s="T701">0.3.h:A</ta>
            <ta e="T703" id="Seg_11819" s="T702">np:G</ta>
            <ta e="T705" id="Seg_11820" s="T704">adv:Time</ta>
            <ta e="T706" id="Seg_11821" s="T705">np.h:Poss</ta>
            <ta e="T708" id="Seg_11822" s="T707">np.h:A</ta>
            <ta e="T710" id="Seg_11823" s="T709">np:Th</ta>
            <ta e="T714" id="Seg_11824" s="T713">np:Th</ta>
            <ta e="T715" id="Seg_11825" s="T714">0.3.h:A</ta>
            <ta e="T717" id="Seg_11826" s="T716">np:Th</ta>
            <ta e="T718" id="Seg_11827" s="T717">0.3.h:A</ta>
            <ta e="T719" id="Seg_11828" s="T718">adv:Time</ta>
            <ta e="T720" id="Seg_11829" s="T719">0.3.h:A</ta>
            <ta e="T721" id="Seg_11830" s="T720">np:G</ta>
            <ta e="T722" id="Seg_11831" s="T721">pro.h:A</ta>
            <ta e="T725" id="Seg_11832" s="T724">np:L</ta>
            <ta e="T727" id="Seg_11833" s="T725">adv:Time</ta>
            <ta e="T728" id="Seg_11834" s="T727">np.h:Poss</ta>
            <ta e="T729" id="Seg_11835" s="T728">np:Th</ta>
            <ta e="T732" id="Seg_11836" s="T731">np.h:Poss</ta>
            <ta e="T733" id="Seg_11837" s="T732">np:Th</ta>
            <ta e="T737" id="Seg_11838" s="T736">np:Th</ta>
            <ta e="T740" id="Seg_11839" s="T739">np:Th</ta>
            <ta e="T741" id="Seg_11840" s="T740">0.3.h:A</ta>
            <ta e="T742" id="Seg_11841" s="T741">adv:Time</ta>
            <ta e="T743" id="Seg_11842" s="T742">pro:G</ta>
            <ta e="T747" id="Seg_11843" s="T746">adv:Time</ta>
            <ta e="T748" id="Seg_11844" s="T747">pro.h:B</ta>
            <ta e="T750" id="Seg_11845" s="T749">np:P</ta>
            <ta e="T751" id="Seg_11846" s="T750">0.3.h:A</ta>
            <ta e="T752" id="Seg_11847" s="T751">adv:Time</ta>
            <ta e="T753" id="Seg_11848" s="T752">0.3.h:A</ta>
            <ta e="T754" id="Seg_11849" s="T753">np:Th</ta>
            <ta e="T756" id="Seg_11850" s="T755">pro.h:Th</ta>
            <ta e="T757" id="Seg_11851" s="T756">np.h:Th</ta>
            <ta e="T761" id="Seg_11852" s="T760">np:Th</ta>
            <ta e="T763" id="Seg_11853" s="T762">adv:Time</ta>
            <ta e="T768" id="Seg_11854" s="T767">np.h:Poss</ta>
            <ta e="T770" id="Seg_11855" s="T769">np.h:A</ta>
            <ta e="T772" id="Seg_11856" s="T771">np.h:Poss</ta>
            <ta e="T774" id="Seg_11857" s="T773">np.h:A</ta>
            <ta e="T775" id="Seg_11858" s="T774">pro.h:B</ta>
            <ta e="T776" id="Seg_11859" s="T775">np:P</ta>
            <ta e="T779" id="Seg_11860" s="T778">0.3.h:A</ta>
            <ta e="T780" id="Seg_11861" s="T779">np:Th</ta>
            <ta e="T782" id="Seg_11862" s="T781">np.h:Th</ta>
            <ta e="T783" id="Seg_11863" s="T782">np.h:Th</ta>
            <ta e="T785" id="Seg_11864" s="T784">np.h:A</ta>
            <ta e="T788" id="Seg_11865" s="T787">np:G</ta>
            <ta e="T789" id="Seg_11866" s="T788">adv:L</ta>
            <ta e="T790" id="Seg_11867" s="T789">pro.h:Th</ta>
            <ta e="T793" id="Seg_11868" s="T792">0.3.h:Th</ta>
            <ta e="T799" id="Seg_11869" s="T798">np.h:A 0.3.h:Poss</ta>
            <ta e="T800" id="Seg_11870" s="T799">pro.h:R</ta>
            <ta e="T804" id="Seg_11871" s="T803">0.2.h:E</ta>
            <ta e="T805" id="Seg_11872" s="T804">0.2.h:A</ta>
            <ta e="T811" id="Seg_11873" s="T810">0.2.h:A</ta>
            <ta e="T815" id="Seg_11874" s="T814">np.h:Th</ta>
            <ta e="T820" id="Seg_11875" s="T819">adv:Time</ta>
            <ta e="T821" id="Seg_11876" s="T820">pro.h:A</ta>
            <ta e="T822" id="Seg_11877" s="T821">np.h:R</ta>
            <ta e="T824" id="Seg_11878" s="T823">pro.h:A</ta>
            <ta e="T827" id="Seg_11879" s="T826">np:P</ta>
            <ta e="T830" id="Seg_11880" s="T829">0.3.h:E</ta>
            <ta e="T831" id="Seg_11881" s="T830">pro.h:Th</ta>
            <ta e="T832" id="Seg_11882" s="T831">0.3.h:A</ta>
            <ta e="T833" id="Seg_11883" s="T832">pro.h:A</ta>
            <ta e="T835" id="Seg_11884" s="T834">np:P</ta>
            <ta e="T839" id="Seg_11885" s="T838">n:Time</ta>
            <ta e="T840" id="Seg_11886" s="T839">0.3.h:A</ta>
            <ta e="T841" id="Seg_11887" s="T840">adv:Time</ta>
            <ta e="T842" id="Seg_11888" s="T841">pro.h:E</ta>
            <ta e="T845" id="Seg_11889" s="T844">pro.h:Th</ta>
            <ta e="T847" id="Seg_11890" s="T846">np.h:Th</ta>
            <ta e="T849" id="Seg_11891" s="T848">np.h:Th</ta>
            <ta e="T857" id="Seg_11892" s="T855">pro.h:Poss</ta>
            <ta e="T858" id="Seg_11893" s="T857">np.h:A</ta>
            <ta e="T859" id="Seg_11894" s="T858">adv:Time</ta>
            <ta e="T861" id="Seg_11895" s="T860">np:P</ta>
            <ta e="T862" id="Seg_11896" s="T861">0.3.h:A</ta>
            <ta e="T866" id="Seg_11897" s="T865">0.3.h:A</ta>
            <ta e="T867" id="Seg_11898" s="T866">np:G</ta>
            <ta e="T868" id="Seg_11899" s="T867">adv:Time</ta>
            <ta e="T869" id="Seg_11900" s="T868">0.3.h:A</ta>
            <ta e="T870" id="Seg_11901" s="T869">0.3.h:E</ta>
            <ta e="T872" id="Seg_11902" s="T871">n:Time</ta>
            <ta e="T873" id="Seg_11903" s="T872">0.3.h:A</ta>
            <ta e="T875" id="Seg_11904" s="T874">0.3.h:A</ta>
            <ta e="T880" id="Seg_11905" s="T879">pro.h:A</ta>
            <ta e="T882" id="Seg_11906" s="T881">np:Th</ta>
            <ta e="T884" id="Seg_11907" s="T883">pro.h:Poss</ta>
            <ta e="T887" id="Seg_11908" s="T886">pro.h:Poss</ta>
            <ta e="T890" id="Seg_11909" s="T889">np.h:Th</ta>
            <ta e="T893" id="Seg_11910" s="T892">adv:Time</ta>
            <ta e="T894" id="Seg_11911" s="T893">0.3.h:E</ta>
            <ta e="T896" id="Seg_11912" s="T895">pro.h:A</ta>
            <ta e="T897" id="Seg_11913" s="T896">adv:L</ta>
            <ta e="T900" id="Seg_11914" s="T899">0.3.h:A</ta>
            <ta e="T901" id="Seg_11915" s="T900">np:P</ta>
            <ta e="T902" id="Seg_11916" s="T901">pro.h:A</ta>
            <ta e="T903" id="Seg_11917" s="T902">adv:Time</ta>
            <ta e="T908" id="Seg_11918" s="T907">adv:Time</ta>
            <ta e="T910" id="Seg_11919" s="T908">n:Time</ta>
            <ta e="T911" id="Seg_11920" s="T910">adv:Time</ta>
            <ta e="T913" id="Seg_11921" s="T911">n:Time</ta>
            <ta e="T914" id="Seg_11922" s="T913">adv:Time</ta>
            <ta e="T915" id="Seg_11923" s="T914">pro.h:Th</ta>
            <ta e="T917" id="Seg_11924" s="T916">0.1.h:E</ta>
            <ta e="T918" id="Seg_11925" s="T917">pro:Th</ta>
            <ta e="T919" id="Seg_11926" s="T918">0.3.h:A</ta>
            <ta e="T921" id="Seg_11927" s="T920">pro.h:E</ta>
            <ta e="T926" id="Seg_11928" s="T925">0.3.h:A</ta>
            <ta e="T927" id="Seg_11929" s="T926">np:So</ta>
            <ta e="T929" id="Seg_11930" s="T928">pro.h:A</ta>
            <ta e="T931" id="Seg_11931" s="T930">adv:Time</ta>
            <ta e="T932" id="Seg_11932" s="T931">0.3.h:A</ta>
            <ta e="T933" id="Seg_11933" s="T932">n:Time</ta>
            <ta e="T936" id="Seg_11934" s="T935">pro.h:R</ta>
            <ta e="T937" id="Seg_11935" s="T936">adv:Time</ta>
            <ta e="T939" id="Seg_11936" s="T938">pro.h:E</ta>
            <ta e="T943" id="Seg_11937" s="T942">adv:Time</ta>
            <ta e="T944" id="Seg_11938" s="T943">0.1.h:E</ta>
            <ta e="T945" id="Seg_11939" s="T944">pro.h:A</ta>
            <ta e="T949" id="Seg_11940" s="T948">pro.h:A</ta>
            <ta e="T952" id="Seg_11941" s="T951">pro.h:A</ta>
            <ta e="T953" id="Seg_11942" s="T952">adv:Time</ta>
            <ta e="T955" id="Seg_11943" s="T954">adv:Time</ta>
            <ta e="T956" id="Seg_11944" s="T955">np:P</ta>
            <ta e="T958" id="Seg_11945" s="T957">np:P</ta>
            <ta e="T959" id="Seg_11946" s="T958">0.1.h:A</ta>
            <ta e="T960" id="Seg_11947" s="T959">np:Th</ta>
            <ta e="T961" id="Seg_11948" s="T960">0.1.h:A</ta>
            <ta e="T963" id="Seg_11949" s="T962">0.1.h:A</ta>
            <ta e="T964" id="Seg_11950" s="T963">pro.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T1" id="Seg_11951" s="T0">ptcl:pred</ta>
            <ta e="T6" id="Seg_11952" s="T5">np:S</ta>
            <ta e="T9" id="Seg_11953" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_11954" s="T9">pro:S</ta>
            <ta e="T12" id="Seg_11955" s="T11">v:pred</ta>
            <ta e="T15" id="Seg_11956" s="T14">pro:S</ta>
            <ta e="T16" id="Seg_11957" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_11958" s="T16">v:pred 0.3:S</ta>
            <ta e="T19" id="Seg_11959" s="T18">pro:S</ta>
            <ta e="T21" id="Seg_11960" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_11961" s="T22">pro:S</ta>
            <ta e="T26" id="Seg_11962" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_11963" s="T26">pro.h:S</ta>
            <ta e="T31" id="Seg_11964" s="T30">v:pred</ta>
            <ta e="T34" id="Seg_11965" s="T33">pro.h:S</ta>
            <ta e="T37" id="Seg_11966" s="T36">ptcl.neg</ta>
            <ta e="T38" id="Seg_11967" s="T37">v:pred</ta>
            <ta e="T39" id="Seg_11968" s="T38">pro.h:S</ta>
            <ta e="T42" id="Seg_11969" s="T41">v:pred</ta>
            <ta e="T44" id="Seg_11970" s="T43">v:pred 0.2.h:S</ta>
            <ta e="T46" id="Seg_11971" s="T45">np.h:O</ta>
            <ta e="T48" id="Seg_11972" s="T47">v:pred</ta>
            <ta e="T53" id="Seg_11973" s="T52">v:pred 0.2.h:S</ta>
            <ta e="T55" id="Seg_11974" s="T54">v:pred 0.2.h:S</ta>
            <ta e="T57" id="Seg_11975" s="T56">ptcl:pred</ta>
            <ta e="T58" id="Seg_11976" s="T57">np:O</ta>
            <ta e="T60" id="Seg_11977" s="T59">np:O</ta>
            <ta e="T61" id="Seg_11978" s="T60">ptcl:pred</ta>
            <ta e="T63" id="Seg_11979" s="T62">np:O</ta>
            <ta e="T64" id="Seg_11980" s="T63">ptcl:pred</ta>
            <ta e="T68" id="Seg_11981" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_11982" s="T68">pro.h:S</ta>
            <ta e="T71" id="Seg_11983" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_11984" s="T72">v:pred 0.1.h:S</ta>
            <ta e="T967" id="Seg_11985" s="T75">conv:pred</ta>
            <ta e="T76" id="Seg_11986" s="T967">v:pred 0.1.h:S</ta>
            <ta e="T79" id="Seg_11987" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_11988" s="T79">adj:pred</ta>
            <ta e="T81" id="Seg_11989" s="T80">cop</ta>
            <ta e="T82" id="Seg_11990" s="T81">np:S</ta>
            <ta e="T83" id="Seg_11991" s="T82">v:pred</ta>
            <ta e="T90" id="Seg_11992" s="T89">n:pred</ta>
            <ta e="T92" id="Seg_11993" s="T91">v:pred 0.1.h:S</ta>
            <ta e="T94" id="Seg_11994" s="T93">v:pred 0.3:S</ta>
            <ta e="T95" id="Seg_11995" s="T94">pro.h:S</ta>
            <ta e="T96" id="Seg_11996" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_11997" s="T97">pro:S</ta>
            <ta e="T100" id="Seg_11998" s="T99">v:pred</ta>
            <ta e="T102" id="Seg_11999" s="T101">v:pred 0.1.h:S</ta>
            <ta e="T104" id="Seg_12000" s="T103">v:pred 0.1.h:S</ta>
            <ta e="T108" id="Seg_12001" s="T107">v:pred 0.1.h:S</ta>
            <ta e="T110" id="Seg_12002" s="T109">np:S</ta>
            <ta e="T113" id="Seg_12003" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_12004" s="T113">np.h:S</ta>
            <ta e="T116" id="Seg_12005" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_12006" s="T116">np:O</ta>
            <ta e="T118" id="Seg_12007" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T120" id="Seg_12008" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_12009" s="T122">ptcl.neg</ta>
            <ta e="T124" id="Seg_12010" s="T123">v:pred 0.3:S</ta>
            <ta e="T126" id="Seg_12011" s="T125">np.h:S</ta>
            <ta e="T130" id="Seg_12012" s="T129">np:O</ta>
            <ta e="T131" id="Seg_12013" s="T130">v:pred</ta>
            <ta e="T133" id="Seg_12014" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_12015" s="T133">ptcl:pred</ta>
            <ta e="T139" id="Seg_12016" s="T138">pro.h:S</ta>
            <ta e="T140" id="Seg_12017" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_12018" s="T141">np:O</ta>
            <ta e="T143" id="Seg_12019" s="T142">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_12020" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_12021" s="T146">pro.h:S</ta>
            <ta e="T148" id="Seg_12022" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_12023" s="T148">np:O</ta>
            <ta e="T150" id="Seg_12024" s="T149">v:pred 0.1.h:S</ta>
            <ta e="T152" id="Seg_12025" s="T151">v:pred 0.1.h:S</ta>
            <ta e="T153" id="Seg_12026" s="T152">v:pred 0.1.h:S</ta>
            <ta e="T155" id="Seg_12027" s="T154">pro.h:S</ta>
            <ta e="T157" id="Seg_12028" s="T156">v:pred</ta>
            <ta e="T159" id="Seg_12029" s="T158">v:pred 0.1.h:S</ta>
            <ta e="T162" id="Seg_12030" s="T161">v:pred 0.1.h:S</ta>
            <ta e="T968" id="Seg_12031" s="T165">conv:pred</ta>
            <ta e="T166" id="Seg_12032" s="T968">v:pred 0.1.h:S</ta>
            <ta e="T168" id="Seg_12033" s="T167">pro.h:S</ta>
            <ta e="T169" id="Seg_12034" s="T168">ptcl.neg</ta>
            <ta e="T170" id="Seg_12035" s="T169">v:pred</ta>
            <ta e="T173" id="Seg_12036" s="T172">v:pred 0.1.h:S</ta>
            <ta e="T175" id="Seg_12037" s="T174">ptcl.neg</ta>
            <ta e="T176" id="Seg_12038" s="T175">adj:pred</ta>
            <ta e="T178" id="Seg_12039" s="T177">v:pred 0.1.h:S</ta>
            <ta e="T181" id="Seg_12040" s="T180">np:S</ta>
            <ta e="T183" id="Seg_12041" s="T182">v:pred</ta>
            <ta e="T184" id="Seg_12042" s="T183">v:pred 0.3:S</ta>
            <ta e="T187" id="Seg_12043" s="T186">pro.h:S</ta>
            <ta e="T190" id="Seg_12044" s="T189">v:pred</ta>
            <ta e="T191" id="Seg_12045" s="T190">np:S</ta>
            <ta e="T193" id="Seg_12046" s="T192">v:pred</ta>
            <ta e="T195" id="Seg_12047" s="T194">np:S</ta>
            <ta e="T196" id="Seg_12048" s="T195">adj:pred</ta>
            <ta e="T198" id="Seg_12049" s="T197">v:pred 0.1.h:S</ta>
            <ta e="T200" id="Seg_12050" s="T199">ptcl.neg</ta>
            <ta e="T201" id="Seg_12051" s="T200">v:pred 0.3:S</ta>
            <ta e="T204" id="Seg_12052" s="T203">v:pred 0.3:S</ta>
            <ta e="T206" id="Seg_12053" s="T205">pro.h:S</ta>
            <ta e="T208" id="Seg_12054" s="T207">v:pred</ta>
            <ta e="T209" id="Seg_12055" s="T208">np:O</ta>
            <ta e="T211" id="Seg_12056" s="T210">v:pred 0.1.h:S</ta>
            <ta e="T214" id="Seg_12057" s="T213">v:pred 0.1.h:S</ta>
            <ta e="T216" id="Seg_12058" s="T215">np.h:S</ta>
            <ta e="T217" id="Seg_12059" s="T216">v:pred</ta>
            <ta e="T218" id="Seg_12060" s="T217">pro.h:S</ta>
            <ta e="T219" id="Seg_12061" s="T218">v:pred</ta>
            <ta e="T220" id="Seg_12062" s="T219">np.h:S</ta>
            <ta e="T222" id="Seg_12063" s="T221">np.h:S</ta>
            <ta e="T223" id="Seg_12064" s="T222">v:pred</ta>
            <ta e="T225" id="Seg_12065" s="T224">pro.h:S</ta>
            <ta e="T227" id="Seg_12066" s="T226">v:pred</ta>
            <ta e="T229" id="Seg_12067" s="T228">v:pred 0.2.h:S</ta>
            <ta e="T231" id="Seg_12068" s="T230">v:pred 0.2.h:S</ta>
            <ta e="T233" id="Seg_12069" s="T232">v:pred 0.1.h:S</ta>
            <ta e="T235" id="Seg_12070" s="T234">np:S</ta>
            <ta e="T236" id="Seg_12071" s="T235">v:pred</ta>
            <ta e="T238" id="Seg_12072" s="T237">v:pred</ta>
            <ta e="T239" id="Seg_12073" s="T238">ptcl.neg</ta>
            <ta e="T240" id="Seg_12074" s="T239">adj:pred</ta>
            <ta e="T242" id="Seg_12075" s="T241">np:S</ta>
            <ta e="T243" id="Seg_12076" s="T242">v:pred</ta>
            <ta e="T246" id="Seg_12077" s="T245">v:pred</ta>
            <ta e="T248" id="Seg_12078" s="T247">pro:O</ta>
            <ta e="T249" id="Seg_12079" s="T248">v:pred 0.2.h:S</ta>
            <ta e="T250" id="Seg_12080" s="T249">np:O</ta>
            <ta e="T251" id="Seg_12081" s="T250">ptcl:pred</ta>
            <ta e="T253" id="Seg_12082" s="T252">np:O</ta>
            <ta e="T255" id="Seg_12083" s="T254">np:O</ta>
            <ta e="T256" id="Seg_12084" s="T255">v:pred</ta>
            <ta e="T257" id="Seg_12085" s="T256">np:O</ta>
            <ta e="T258" id="Seg_12086" s="T257">v:pred 0.2.h:S</ta>
            <ta e="T261" id="Seg_12087" s="T260">np:S</ta>
            <ta e="T262" id="Seg_12088" s="T261">v:pred</ta>
            <ta e="T264" id="Seg_12089" s="T263">pro:O</ta>
            <ta e="T265" id="Seg_12090" s="T264">ptcl.neg</ta>
            <ta e="T266" id="Seg_12091" s="T265">v:pred 0.2.h:S</ta>
            <ta e="T267" id="Seg_12092" s="T266">np:O</ta>
            <ta e="T268" id="Seg_12093" s="T267">s:purp</ta>
            <ta e="T269" id="Seg_12094" s="T268">v:pred 0.2.h:S</ta>
            <ta e="T270" id="Seg_12095" s="T269">np:O</ta>
            <ta e="T271" id="Seg_12096" s="T270">ptcl.neg</ta>
            <ta e="T273" id="Seg_12097" s="T272">v:pred 0.2.h:S</ta>
            <ta e="T275" id="Seg_12098" s="T274">v:pred 0.2.h:S</ta>
            <ta e="T278" id="Seg_12099" s="T277">v:pred 0.2.h:S</ta>
            <ta e="T279" id="Seg_12100" s="T278">np.h:O</ta>
            <ta e="T280" id="Seg_12101" s="T279">v:pred 0.2.h:S</ta>
            <ta e="T281" id="Seg_12102" s="T280">ptcl.neg</ta>
            <ta e="T282" id="Seg_12103" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_12104" s="T284">v:pred 0.2.h:S</ta>
            <ta e="T287" id="Seg_12105" s="T286">v:pred 0.2.h:S</ta>
            <ta e="T290" id="Seg_12106" s="T289">v:pred 0.3.h:S</ta>
            <ta e="T291" id="Seg_12107" s="T290">ptcl.neg</ta>
            <ta e="T293" id="Seg_12108" s="T292">adj:pred</ta>
            <ta e="T296" id="Seg_12109" s="T295">ptcl.neg</ta>
            <ta e="T297" id="Seg_12110" s="T296">v:pred 0.1.h:S</ta>
            <ta e="T299" id="Seg_12111" s="T298">adj:pred</ta>
            <ta e="T302" id="Seg_12112" s="T301">v:pred 0.2.h:S</ta>
            <ta e="T304" id="Seg_12113" s="T303">v:pred 0.2.h:S</ta>
            <ta e="T306" id="Seg_12114" s="T305">v:pred 0.1.h:S</ta>
            <ta e="T308" id="Seg_12115" s="T307">pro.h:S</ta>
            <ta e="T310" id="Seg_12116" s="T309">v:pred</ta>
            <ta e="T314" id="Seg_12117" s="T313">v:pred 0.3.h:S</ta>
            <ta e="T316" id="Seg_12118" s="T315">pro.h:S</ta>
            <ta e="T317" id="Seg_12119" s="T316">ptcl.neg</ta>
            <ta e="T318" id="Seg_12120" s="T317">v:pred</ta>
            <ta e="T321" id="Seg_12121" s="T320">np:S</ta>
            <ta e="T323" id="Seg_12122" s="T322">v:pred</ta>
            <ta e="T329" id="Seg_12123" s="T328">v:pred 0.3:S</ta>
            <ta e="T331" id="Seg_12124" s="T330">v:pred 0.1.h:S</ta>
            <ta e="T333" id="Seg_12125" s="T332">v:pred 0.3:S</ta>
            <ta e="T334" id="Seg_12126" s="T333">pro:O</ta>
            <ta e="T335" id="Seg_12127" s="T334">v:pred 0.3.h:S</ta>
            <ta e="T336" id="Seg_12128" s="T335">np:S</ta>
            <ta e="T337" id="Seg_12129" s="T336">v:pred</ta>
            <ta e="T341" id="Seg_12130" s="T340">v:pred 0.3.h:S</ta>
            <ta e="T348" id="Seg_12131" s="T347">np.h:O</ta>
            <ta e="T349" id="Seg_12132" s="T348">ptcl:pred</ta>
            <ta e="T354" id="Seg_12133" s="T353">pro.h:S</ta>
            <ta e="T357" id="Seg_12134" s="T356">v:pred</ta>
            <ta e="T358" id="Seg_12135" s="T357">np:O</ta>
            <ta e="T359" id="Seg_12136" s="T358">v:pred 0.3.h:S</ta>
            <ta e="T360" id="Seg_12137" s="T359">pro.h:S</ta>
            <ta e="T362" id="Seg_12138" s="T361">pro.h:O</ta>
            <ta e="T363" id="Seg_12139" s="T362">v:pred</ta>
            <ta e="T365" id="Seg_12140" s="T364">pro.h:S</ta>
            <ta e="T366" id="Seg_12141" s="T365">v:pred</ta>
            <ta e="T368" id="Seg_12142" s="T367">pro.h:S</ta>
            <ta e="T370" id="Seg_12143" s="T369">v:pred</ta>
            <ta e="T372" id="Seg_12144" s="T371">pro:O</ta>
            <ta e="T373" id="Seg_12145" s="T372">v:pred 0.2.h:S</ta>
            <ta e="T375" id="Seg_12146" s="T374">v:pred 0.1.h:S</ta>
            <ta e="T378" id="Seg_12147" s="T377">pro.h:S</ta>
            <ta e="T379" id="Seg_12148" s="T378">v:pred</ta>
            <ta e="T380" id="Seg_12149" s="T379">np:S</ta>
            <ta e="T382" id="Seg_12150" s="T381">v:pred</ta>
            <ta e="T384" id="Seg_12151" s="T383">np:S</ta>
            <ta e="T385" id="Seg_12152" s="T384">v:pred</ta>
            <ta e="T387" id="Seg_12153" s="T386">np:S</ta>
            <ta e="T388" id="Seg_12154" s="T387">v:pred</ta>
            <ta e="T390" id="Seg_12155" s="T389">np.h:S</ta>
            <ta e="T391" id="Seg_12156" s="T390">v:pred</ta>
            <ta e="T394" id="Seg_12157" s="T393">v:pred 0.3.h:S</ta>
            <ta e="T395" id="Seg_12158" s="T394">np:O</ta>
            <ta e="T396" id="Seg_12159" s="T395">conv:pred</ta>
            <ta e="T397" id="Seg_12160" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T398" id="Seg_12161" s="T397">ptcl.neg</ta>
            <ta e="T399" id="Seg_12162" s="T398">v:pred 0.1.h:S</ta>
            <ta e="T400" id="Seg_12163" s="T399">v:pred 0.3.h:S</ta>
            <ta e="T402" id="Seg_12164" s="T401">ptcl.neg</ta>
            <ta e="T403" id="Seg_12165" s="T402">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_12166" s="T403">v:pred 0.3.h:S</ta>
            <ta e="T407" id="Seg_12167" s="T406">ptcl:pred</ta>
            <ta e="T409" id="Seg_12168" s="T408">np.h:S</ta>
            <ta e="T411" id="Seg_12169" s="T410">adj:pred</ta>
            <ta e="T414" id="Seg_12170" s="T413">np.h:O</ta>
            <ta e="T415" id="Seg_12171" s="T414">v:pred 0.3.h:S</ta>
            <ta e="T416" id="Seg_12172" s="T415">ptcl:pred</ta>
            <ta e="T417" id="Seg_12173" s="T416">pro.h:O</ta>
            <ta e="T420" id="Seg_12174" s="T419">ptcl:pred</ta>
            <ta e="T421" id="Seg_12175" s="T420">pro:O</ta>
            <ta e="T422" id="Seg_12176" s="T421">conv:pred</ta>
            <ta e="T423" id="Seg_12177" s="T422">v:pred</ta>
            <ta e="T429" id="Seg_12178" s="T428">np:S</ta>
            <ta e="T430" id="Seg_12179" s="T429">v:pred</ta>
            <ta e="T432" id="Seg_12180" s="T431">pro.h:S</ta>
            <ta e="T433" id="Seg_12181" s="T432">v:pred</ta>
            <ta e="T435" id="Seg_12182" s="T434">np:S</ta>
            <ta e="T436" id="Seg_12183" s="T435">v:pred</ta>
            <ta e="T437" id="Seg_12184" s="T436">pro.h:S</ta>
            <ta e="T438" id="Seg_12185" s="T437">v:pred</ta>
            <ta e="T440" id="Seg_12186" s="T439">np:S</ta>
            <ta e="T442" id="Seg_12187" s="T441">adj:pred</ta>
            <ta e="T443" id="Seg_12188" s="T442">cop</ta>
            <ta e="T445" id="Seg_12189" s="T444">adj:pred</ta>
            <ta e="T446" id="Seg_12190" s="T445">cop</ta>
            <ta e="T447" id="Seg_12191" s="T446">np:S</ta>
            <ta e="T456" id="Seg_12192" s="T455">np:S</ta>
            <ta e="T457" id="Seg_12193" s="T456">v:pred</ta>
            <ta e="T460" id="Seg_12194" s="T459">adj:pred</ta>
            <ta e="T461" id="Seg_12195" s="T460">np:S</ta>
            <ta e="T463" id="Seg_12196" s="T462">adj:pred</ta>
            <ta e="T464" id="Seg_12197" s="T463">ptcl:pred</ta>
            <ta e="T466" id="Seg_12198" s="T465">np:O</ta>
            <ta e="T470" id="Seg_12199" s="T469">np:S</ta>
            <ta e="T471" id="Seg_12200" s="T470">v:pred</ta>
            <ta e="T472" id="Seg_12201" s="T471">np:S</ta>
            <ta e="T473" id="Seg_12202" s="T472">v:pred</ta>
            <ta e="T475" id="Seg_12203" s="T474">np:S</ta>
            <ta e="T477" id="Seg_12204" s="T476">np:O</ta>
            <ta e="T478" id="Seg_12205" s="T477">ptcl.neg</ta>
            <ta e="T479" id="Seg_12206" s="T478">v:pred</ta>
            <ta e="T482" id="Seg_12207" s="T481">v:pred 0.2.h:S</ta>
            <ta e="T484" id="Seg_12208" s="T483">np:O</ta>
            <ta e="T485" id="Seg_12209" s="T484">pro:S</ta>
            <ta e="T487" id="Seg_12210" s="T486">ptcl.neg</ta>
            <ta e="T488" id="Seg_12211" s="T487">v:pred</ta>
            <ta e="T489" id="Seg_12212" s="T488">ptcl:pred</ta>
            <ta e="T490" id="Seg_12213" s="T489">pro:O</ta>
            <ta e="T492" id="Seg_12214" s="T491">np.h:S</ta>
            <ta e="T493" id="Seg_12215" s="T492">np.h:S</ta>
            <ta e="T495" id="Seg_12216" s="T494">v:pred</ta>
            <ta e="T497" id="Seg_12217" s="T496">np.h:S</ta>
            <ta e="T498" id="Seg_12218" s="T497">v:pred</ta>
            <ta e="T501" id="Seg_12219" s="T500">v:pred 0.2.h:S</ta>
            <ta e="T503" id="Seg_12220" s="T502">v:pred 0.1.h:S</ta>
            <ta e="T506" id="Seg_12221" s="T505">np.h:O</ta>
            <ta e="T507" id="Seg_12222" s="T506">np.h:O</ta>
            <ta e="T508" id="Seg_12223" s="T507">v:pred 0.1.h:S</ta>
            <ta e="T512" id="Seg_12224" s="T511">np.h:O</ta>
            <ta e="T513" id="Seg_12225" s="T512">np.h:O</ta>
            <ta e="T514" id="Seg_12226" s="T513">v:pred 0.3.h:S</ta>
            <ta e="T515" id="Seg_12227" s="T514">pro.h:S</ta>
            <ta e="T516" id="Seg_12228" s="T515">v:pred</ta>
            <ta e="T518" id="Seg_12229" s="T517">np.h:O</ta>
            <ta e="T519" id="Seg_12230" s="T518">np.h:O</ta>
            <ta e="T520" id="Seg_12231" s="T519">v:pred 0.3.h:S</ta>
            <ta e="T521" id="Seg_12232" s="T520">np.h:O</ta>
            <ta e="T523" id="Seg_12233" s="T522">v:pred 0.2.h:S</ta>
            <ta e="T529" id="Seg_12234" s="T528">v:pred 0.1.h:S</ta>
            <ta e="T532" id="Seg_12235" s="T531">np.h:O</ta>
            <ta e="T533" id="Seg_12236" s="T532">v:pred 0.3.h:S</ta>
            <ta e="T534" id="Seg_12237" s="T533">pro.h:S</ta>
            <ta e="T535" id="Seg_12238" s="T534">v:pred</ta>
            <ta e="T537" id="Seg_12239" s="T536">np.h:O</ta>
            <ta e="T539" id="Seg_12240" s="T538">v:pred 0.1.h:S</ta>
            <ta e="T541" id="Seg_12241" s="T540">v:pred 0.3.h:S</ta>
            <ta e="T542" id="Seg_12242" s="T541">np:O</ta>
            <ta e="T543" id="Seg_12243" s="T542">v:pred 0.3.h:S</ta>
            <ta e="T544" id="Seg_12244" s="T543">np:O</ta>
            <ta e="T545" id="Seg_12245" s="T544">v:pred 0.3.h:S</ta>
            <ta e="T546" id="Seg_12246" s="T545">np:O</ta>
            <ta e="T547" id="Seg_12247" s="T546">adj:pred</ta>
            <ta e="T548" id="Seg_12248" s="T547">cop 0.3:S</ta>
            <ta e="T550" id="Seg_12249" s="T549">np.h:S</ta>
            <ta e="T551" id="Seg_12250" s="T550">v:pred</ta>
            <ta e="T552" id="Seg_12251" s="T551">np:O</ta>
            <ta e="T553" id="Seg_12252" s="T552">v:pred 0.3.h:S</ta>
            <ta e="T558" id="Seg_12253" s="T557">np:S</ta>
            <ta e="T559" id="Seg_12254" s="T558">v:pred</ta>
            <ta e="T560" id="Seg_12255" s="T559">pro.h:S</ta>
            <ta e="T561" id="Seg_12256" s="T560">np:O</ta>
            <ta e="T562" id="Seg_12257" s="T561">v:pred</ta>
            <ta e="T565" id="Seg_12258" s="T564">v:pred 0.3.h:S</ta>
            <ta e="T567" id="Seg_12259" s="T566">np.h:S</ta>
            <ta e="T570" id="Seg_12260" s="T569">v:pred</ta>
            <ta e="T574" id="Seg_12261" s="T573">np:O</ta>
            <ta e="T575" id="Seg_12262" s="T574">v:pred 0.3.h:S</ta>
            <ta e="T578" id="Seg_12263" s="T577">v:pred 0.3.h:S</ta>
            <ta e="T581" id="Seg_12264" s="T580">np:O</ta>
            <ta e="T582" id="Seg_12265" s="T581">v:pred 0.3.h:S</ta>
            <ta e="T584" id="Seg_12266" s="T583">np.h:S</ta>
            <ta e="T585" id="Seg_12267" s="T584">v:pred</ta>
            <ta e="T588" id="Seg_12268" s="T587">np.h:S</ta>
            <ta e="T589" id="Seg_12269" s="T588">v:pred</ta>
            <ta e="T591" id="Seg_12270" s="T590">np.h:S</ta>
            <ta e="T592" id="Seg_12271" s="T591">v:pred</ta>
            <ta e="T593" id="Seg_12272" s="T592">np:S</ta>
            <ta e="T594" id="Seg_12273" s="T593">adj:pred</ta>
            <ta e="T595" id="Seg_12274" s="T594">cop</ta>
            <ta e="T599" id="Seg_12275" s="T598">pro:S</ta>
            <ta e="T601" id="Seg_12276" s="T600">v:pred</ta>
            <ta e="T603" id="Seg_12277" s="T602">np.h:S</ta>
            <ta e="T604" id="Seg_12278" s="T603">v:pred</ta>
            <ta e="T605" id="Seg_12279" s="T604">np.h:S</ta>
            <ta e="T606" id="Seg_12280" s="T605">np.h:S</ta>
            <ta e="T607" id="Seg_12281" s="T606">v:pred</ta>
            <ta e="T609" id="Seg_12282" s="T608">np:S</ta>
            <ta e="T610" id="Seg_12283" s="T609">v:pred</ta>
            <ta e="T611" id="Seg_12284" s="T610">ptcl:pred</ta>
            <ta e="T612" id="Seg_12285" s="T611">np:O</ta>
            <ta e="T614" id="Seg_12286" s="T613">ptcl:pred</ta>
            <ta e="T615" id="Seg_12287" s="T614">np:O</ta>
            <ta e="T617" id="Seg_12288" s="T616">np:O</ta>
            <ta e="T619" id="Seg_12289" s="T618">np:S</ta>
            <ta e="T620" id="Seg_12290" s="T619">adj:pred</ta>
            <ta e="T621" id="Seg_12291" s="T620">np:S</ta>
            <ta e="T622" id="Seg_12292" s="T621">v:pred</ta>
            <ta e="T625" id="Seg_12293" s="T624">pro.h:S</ta>
            <ta e="T626" id="Seg_12294" s="T625">np:O</ta>
            <ta e="T627" id="Seg_12295" s="T626">v:pred</ta>
            <ta e="T632" id="Seg_12296" s="T631">np.h:S</ta>
            <ta e="T633" id="Seg_12297" s="T632">np.h:S</ta>
            <ta e="T635" id="Seg_12298" s="T634">v:pred</ta>
            <ta e="T636" id="Seg_12299" s="T635">np:O</ta>
            <ta e="T637" id="Seg_12300" s="T636">np.h:S</ta>
            <ta e="T638" id="Seg_12301" s="T637">v:pred</ta>
            <ta e="T639" id="Seg_12302" s="T638">np:O</ta>
            <ta e="T640" id="Seg_12303" s="T639">v:pred 0.3.h:S</ta>
            <ta e="T641" id="Seg_12304" s="T640">np:O</ta>
            <ta e="T642" id="Seg_12305" s="T641">np:O</ta>
            <ta e="T643" id="Seg_12306" s="T642">v:pred 0.3.h:S</ta>
            <ta e="T647" id="Seg_12307" s="T646">np.h:S</ta>
            <ta e="T648" id="Seg_12308" s="T647">adj:pred</ta>
            <ta e="T649" id="Seg_12309" s="T648">cop</ta>
            <ta e="T651" id="Seg_12310" s="T650">pro.h:S</ta>
            <ta e="T653" id="Seg_12311" s="T652">v:pred</ta>
            <ta e="T660" id="Seg_12312" s="T659">v:pred 0.3.h:S</ta>
            <ta e="T662" id="Seg_12313" s="T661">pro.h:S</ta>
            <ta e="T969" id="Seg_12314" s="T662">conv:pred</ta>
            <ta e="T663" id="Seg_12315" s="T969">v:pred</ta>
            <ta e="T665" id="Seg_12316" s="T664">np.h:S</ta>
            <ta e="T666" id="Seg_12317" s="T665">v:pred</ta>
            <ta e="T667" id="Seg_12318" s="T666">np:O</ta>
            <ta e="T668" id="Seg_12319" s="T667">np:O</ta>
            <ta e="T670" id="Seg_12320" s="T669">v:pred 0.3.h:S</ta>
            <ta e="T671" id="Seg_12321" s="T670">np:O</ta>
            <ta e="T672" id="Seg_12322" s="T671">v:pred 0.3.h:S</ta>
            <ta e="T673" id="Seg_12323" s="T672">np:O</ta>
            <ta e="T674" id="Seg_12324" s="T673">v:pred 0.3.h:S</ta>
            <ta e="T675" id="Seg_12325" s="T674">np:O</ta>
            <ta e="T676" id="Seg_12326" s="T675">v:pred 0.3.h:S</ta>
            <ta e="T677" id="Seg_12327" s="T676">np:O</ta>
            <ta e="T678" id="Seg_12328" s="T677">v:pred 0.3.h:S</ta>
            <ta e="T680" id="Seg_12329" s="T679">np:O</ta>
            <ta e="T681" id="Seg_12330" s="T680">v:pred 0.3.h:S</ta>
            <ta e="T682" id="Seg_12331" s="T681">pro.h:S</ta>
            <ta e="T685" id="Seg_12332" s="T684">v:pred</ta>
            <ta e="T700" id="Seg_12333" s="T699">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_12334" s="T701">v:pred 0.3.h:S</ta>
            <ta e="T708" id="Seg_12335" s="T707">np.h:S</ta>
            <ta e="T710" id="Seg_12336" s="T709">np:O</ta>
            <ta e="T711" id="Seg_12337" s="T710">v:pred</ta>
            <ta e="T714" id="Seg_12338" s="T713">np:O</ta>
            <ta e="T715" id="Seg_12339" s="T714">v:pred 0.3.h:S</ta>
            <ta e="T717" id="Seg_12340" s="T716">np:O</ta>
            <ta e="T718" id="Seg_12341" s="T717">v:pred 0.3.h:S</ta>
            <ta e="T720" id="Seg_12342" s="T719">v:pred 0.3.h:S</ta>
            <ta e="T722" id="Seg_12343" s="T721">pro.h:S</ta>
            <ta e="T723" id="Seg_12344" s="T722">v:pred</ta>
            <ta e="T729" id="Seg_12345" s="T728">np:S</ta>
            <ta e="T730" id="Seg_12346" s="T729">adj:pred</ta>
            <ta e="T731" id="Seg_12347" s="T730">adj:pred</ta>
            <ta e="T733" id="Seg_12348" s="T732">np:S</ta>
            <ta e="T735" id="Seg_12349" s="T734">adj:pred</ta>
            <ta e="T737" id="Seg_12350" s="T736">np:S</ta>
            <ta e="T739" id="Seg_12351" s="T738">adj:pred</ta>
            <ta e="T740" id="Seg_12352" s="T739">np:O</ta>
            <ta e="T741" id="Seg_12353" s="T740">v:pred 0.3.h:S</ta>
            <ta e="T750" id="Seg_12354" s="T749">np:O</ta>
            <ta e="T751" id="Seg_12355" s="T750">v:pred 0.3.h:S</ta>
            <ta e="T753" id="Seg_12356" s="T752">v:pred 0.3.h:S</ta>
            <ta e="T754" id="Seg_12357" s="T753">np:O</ta>
            <ta e="T756" id="Seg_12358" s="T755">pro.h:S</ta>
            <ta e="T757" id="Seg_12359" s="T756">n:pred</ta>
            <ta e="T758" id="Seg_12360" s="T757">cop</ta>
            <ta e="T761" id="Seg_12361" s="T760">np:S</ta>
            <ta e="T762" id="Seg_12362" s="T761">v:pred</ta>
            <ta e="T770" id="Seg_12363" s="T769">np.h:S</ta>
            <ta e="T774" id="Seg_12364" s="T773">np.h:S</ta>
            <ta e="T776" id="Seg_12365" s="T775">np:O</ta>
            <ta e="T777" id="Seg_12366" s="T776">v:pred</ta>
            <ta e="T779" id="Seg_12367" s="T778">v:pred 0.3.h:S</ta>
            <ta e="T780" id="Seg_12368" s="T779">np:O</ta>
            <ta e="T782" id="Seg_12369" s="T781">np.h:O</ta>
            <ta e="T783" id="Seg_12370" s="T782">np.h:O</ta>
            <ta e="T785" id="Seg_12371" s="T784">np.h:S</ta>
            <ta e="T787" id="Seg_12372" s="T786">v:pred</ta>
            <ta e="T790" id="Seg_12373" s="T789">pro.h:S</ta>
            <ta e="T791" id="Seg_12374" s="T790">ptcl.neg</ta>
            <ta e="T792" id="Seg_12375" s="T791">v:pred</ta>
            <ta e="T793" id="Seg_12376" s="T792">v:pred 0.3.h:S</ta>
            <ta e="T795" id="Seg_12377" s="T794">s:purp</ta>
            <ta e="T799" id="Seg_12378" s="T798">np.h:S</ta>
            <ta e="T801" id="Seg_12379" s="T800">v:pred</ta>
            <ta e="T804" id="Seg_12380" s="T803">v:pred 0.2.h:S</ta>
            <ta e="T805" id="Seg_12381" s="T804">v:pred 0.2.h:S</ta>
            <ta e="T811" id="Seg_12382" s="T810">v:pred 0.2.h:S</ta>
            <ta e="T813" id="Seg_12383" s="T812">ptcl:pred</ta>
            <ta e="T815" id="Seg_12384" s="T814">np.h:S</ta>
            <ta e="T816" id="Seg_12385" s="T815">adj:pred</ta>
            <ta e="T817" id="Seg_12386" s="T816">cop</ta>
            <ta e="T821" id="Seg_12387" s="T820">pro.h:S</ta>
            <ta e="T822" id="Seg_12388" s="T821">np.h:O</ta>
            <ta e="T823" id="Seg_12389" s="T822">v:pred</ta>
            <ta e="T824" id="Seg_12390" s="T823">pro.h:S</ta>
            <ta e="T827" id="Seg_12391" s="T826">np:O</ta>
            <ta e="T828" id="Seg_12392" s="T827">v:pred</ta>
            <ta e="T830" id="Seg_12393" s="T829">v:pred 0.3.h:S</ta>
            <ta e="T831" id="Seg_12394" s="T830">pro.h:O</ta>
            <ta e="T832" id="Seg_12395" s="T831">v:pred 0.3.h:S</ta>
            <ta e="T833" id="Seg_12396" s="T832">pro.h:S</ta>
            <ta e="T835" id="Seg_12397" s="T834">np:O</ta>
            <ta e="T836" id="Seg_12398" s="T835">v:pred</ta>
            <ta e="T840" id="Seg_12399" s="T839">v:pred 0.3.h:S</ta>
            <ta e="T842" id="Seg_12400" s="T841">pro.h:S</ta>
            <ta e="T844" id="Seg_12401" s="T843">v:pred</ta>
            <ta e="T845" id="Seg_12402" s="T844">pro.h:S</ta>
            <ta e="T848" id="Seg_12403" s="T847">v:pred</ta>
            <ta e="T849" id="Seg_12404" s="T848">np.h:S</ta>
            <ta e="T850" id="Seg_12405" s="T849">v:pred</ta>
            <ta e="T852" id="Seg_12406" s="T851">v:pred</ta>
            <ta e="T858" id="Seg_12407" s="T857">np.h:S</ta>
            <ta e="T860" id="Seg_12408" s="T859">v:pred</ta>
            <ta e="T861" id="Seg_12409" s="T860">np:O</ta>
            <ta e="T862" id="Seg_12410" s="T861">v:pred 0.3.h:S</ta>
            <ta e="T865" id="Seg_12411" s="T864">ptcl.neg</ta>
            <ta e="T866" id="Seg_12412" s="T865">v:pred 0.3.h:S</ta>
            <ta e="T869" id="Seg_12413" s="T868">v:pred 0.3.h:S</ta>
            <ta e="T870" id="Seg_12414" s="T869">v:pred 0.3.h:S</ta>
            <ta e="T873" id="Seg_12415" s="T872">v:pred 0.3.h:S</ta>
            <ta e="T875" id="Seg_12416" s="T874">v:pred 0.3.h:S</ta>
            <ta e="T880" id="Seg_12417" s="T879">pro.h:S</ta>
            <ta e="T882" id="Seg_12418" s="T881">np:O</ta>
            <ta e="T883" id="Seg_12419" s="T882">v:pred</ta>
            <ta e="T890" id="Seg_12420" s="T889">np.h:S</ta>
            <ta e="T891" id="Seg_12421" s="T890">adj:pred</ta>
            <ta e="T892" id="Seg_12422" s="T891">cop</ta>
            <ta e="T894" id="Seg_12423" s="T893">v:pred 0.3.h:S</ta>
            <ta e="T896" id="Seg_12424" s="T895">pro.h:S</ta>
            <ta e="T898" id="Seg_12425" s="T897">v:pred</ta>
            <ta e="T900" id="Seg_12426" s="T899">v:pred 0.3.h:S</ta>
            <ta e="T901" id="Seg_12427" s="T900">np:O</ta>
            <ta e="T902" id="Seg_12428" s="T901">pro.h:S</ta>
            <ta e="T906" id="Seg_12429" s="T905">v:pred</ta>
            <ta e="T915" id="Seg_12430" s="T914">pro.h:O</ta>
            <ta e="T917" id="Seg_12431" s="T916">v:pred 0.1.h:S</ta>
            <ta e="T918" id="Seg_12432" s="T917">pro:O</ta>
            <ta e="T919" id="Seg_12433" s="T918">v:pred 0.3.h:S</ta>
            <ta e="T921" id="Seg_12434" s="T920">pro.h:S</ta>
            <ta e="T922" id="Seg_12435" s="T921">conv:pred</ta>
            <ta e="T925" id="Seg_12436" s="T924">ptcl.neg</ta>
            <ta e="T926" id="Seg_12437" s="T925">v:pred 0.3.h:S</ta>
            <ta e="T929" id="Seg_12438" s="T928">pro.h:S</ta>
            <ta e="T930" id="Seg_12439" s="T929">v:pred</ta>
            <ta e="T932" id="Seg_12440" s="T931">v:pred 0.3.h:S</ta>
            <ta e="T934" id="Seg_12441" s="T933">s:purp</ta>
            <ta e="T935" id="Seg_12442" s="T934">ptcl:pred</ta>
            <ta e="T939" id="Seg_12443" s="T938">pro.h:S</ta>
            <ta e="T941" id="Seg_12444" s="T940">v:pred</ta>
            <ta e="T944" id="Seg_12445" s="T943">v:pred 0.1.h:S</ta>
            <ta e="T945" id="Seg_12446" s="T944">pro.h:S</ta>
            <ta e="T947" id="Seg_12447" s="T946">v:pred</ta>
            <ta e="T949" id="Seg_12448" s="T948">pro.h:S</ta>
            <ta e="T950" id="Seg_12449" s="T949">ptcl.neg</ta>
            <ta e="T951" id="Seg_12450" s="T950">v:pred</ta>
            <ta e="T952" id="Seg_12451" s="T951">pro.h:S</ta>
            <ta e="T956" id="Seg_12452" s="T955">np:O</ta>
            <ta e="T957" id="Seg_12453" s="T956">v:pred</ta>
            <ta e="T958" id="Seg_12454" s="T957">np:O</ta>
            <ta e="T959" id="Seg_12455" s="T958">v:pred 0.1.h:S</ta>
            <ta e="T960" id="Seg_12456" s="T959">np:O</ta>
            <ta e="T961" id="Seg_12457" s="T960">v:pred 0.1.h:S</ta>
            <ta e="T963" id="Seg_12458" s="T962">v:pred 0.1.h:S</ta>
            <ta e="T964" id="Seg_12459" s="T963">pro.h:O</ta>
            <ta e="T965" id="Seg_12460" s="T964">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T1" id="Seg_12461" s="T0">RUS:gram</ta>
            <ta e="T13" id="Seg_12462" s="T12">TURK:disc</ta>
            <ta e="T32" id="Seg_12463" s="T31">RUS:gram</ta>
            <ta e="T36" id="Seg_12464" s="T35">TURK:gram(INDEF)</ta>
            <ta e="T43" id="Seg_12465" s="T42">TURK:core</ta>
            <ta e="T49" id="Seg_12466" s="T48">RUS:gram</ta>
            <ta e="T57" id="Seg_12467" s="T56">RUS:mod</ta>
            <ta e="T61" id="Seg_12468" s="T60">RUS:mod</ta>
            <ta e="T64" id="Seg_12469" s="T63">RUS:mod</ta>
            <ta e="T70" id="Seg_12470" s="T69">RUS:core</ta>
            <ta e="T72" id="Seg_12471" s="T71">RUS:core</ta>
            <ta e="T78" id="Seg_12472" s="T77">RUS:core</ta>
            <ta e="T91" id="Seg_12473" s="T90">TURK:disc</ta>
            <ta e="T103" id="Seg_12474" s="T102">RUS:core</ta>
            <ta e="T105" id="Seg_12475" s="T104">RUS:core</ta>
            <ta e="T109" id="Seg_12476" s="T108">RUS:core</ta>
            <ta e="T110" id="Seg_12477" s="T109">TAT:cult</ta>
            <ta e="T111" id="Seg_12478" s="T110">TURK:disc</ta>
            <ta e="T115" id="Seg_12479" s="T114">TURK:disc</ta>
            <ta e="T121" id="Seg_12480" s="T120">TURK:disc</ta>
            <ta e="T122" id="Seg_12481" s="T121">RUS:gram</ta>
            <ta e="T129" id="Seg_12482" s="T128">TURK:disc</ta>
            <ta e="T130" id="Seg_12483" s="T129">TURK:cult</ta>
            <ta e="T132" id="Seg_12484" s="T131">TURK:disc</ta>
            <ta e="T134" id="Seg_12485" s="T133">RUS:mod</ta>
            <ta e="T138" id="Seg_12486" s="T137">RUS:gram</ta>
            <ta e="T141" id="Seg_12487" s="T140">RUS:gram</ta>
            <ta e="T142" id="Seg_12488" s="T141">TURK:cult</ta>
            <ta e="T151" id="Seg_12489" s="T150">RUS:cult</ta>
            <ta e="T154" id="Seg_12490" s="T153">RUS:gram</ta>
            <ta e="T156" id="Seg_12491" s="T155">TURK:disc</ta>
            <ta e="T161" id="Seg_12492" s="T160">TURK:disc</ta>
            <ta e="T171" id="Seg_12493" s="T170">RUS:gram</ta>
            <ta e="T173" id="Seg_12494" s="T172">%TURK:core</ta>
            <ta e="T174" id="Seg_12495" s="T173">RUS:gram</ta>
            <ta e="T177" id="Seg_12496" s="T176">RUS:gram</ta>
            <ta e="T186" id="Seg_12497" s="T185">RUS:gram</ta>
            <ta e="T192" id="Seg_12498" s="T191">TURK:disc</ta>
            <ta e="T202" id="Seg_12499" s="T201">RUS:gram</ta>
            <ta e="T203" id="Seg_12500" s="T202">RUS:gram</ta>
            <ta e="T205" id="Seg_12501" s="T204">TURK:disc</ta>
            <ta e="T212" id="Seg_12502" s="T211">RUS:gram</ta>
            <ta e="T221" id="Seg_12503" s="T220">RUS:gram</ta>
            <ta e="T224" id="Seg_12504" s="T223">RUS:gram</ta>
            <ta e="T240" id="Seg_12505" s="T239">TURK:core</ta>
            <ta e="T244" id="Seg_12506" s="T243">RUS:gram</ta>
            <ta e="T245" id="Seg_12507" s="T244">TURK:core</ta>
            <ta e="T248" id="Seg_12508" s="T247">RUS:gram(INDEF)</ta>
            <ta e="T251" id="Seg_12509" s="T250">RUS:mod</ta>
            <ta e="T253" id="Seg_12510" s="T252">TURK:cult</ta>
            <ta e="T263" id="Seg_12511" s="T262">RUS:gram</ta>
            <ta e="T264" id="Seg_12512" s="T263">TURK:gram(INDEF)</ta>
            <ta e="T274" id="Seg_12513" s="T273">RUS:gram</ta>
            <ta e="T279" id="Seg_12514" s="T278">TURK:core</ta>
            <ta e="T283" id="Seg_12515" s="T282">RUS:gram</ta>
            <ta e="T288" id="Seg_12516" s="T287">TURK:core</ta>
            <ta e="T289" id="Seg_12517" s="T288">RUS:gram</ta>
            <ta e="T292" id="Seg_12518" s="T291">TURK:core</ta>
            <ta e="T307" id="Seg_12519" s="T306">TURK:disc</ta>
            <ta e="T313" id="Seg_12520" s="T312">TURK:disc</ta>
            <ta e="T315" id="Seg_12521" s="T314">RUS:gram</ta>
            <ta e="T322" id="Seg_12522" s="T321">TURK:disc</ta>
            <ta e="T325" id="Seg_12523" s="T324">RUS:core</ta>
            <ta e="T326" id="Seg_12524" s="T325">TURK:disc</ta>
            <ta e="T332" id="Seg_12525" s="T331">TURK:core</ta>
            <ta e="T334" id="Seg_12526" s="T333">TURK:gram(INDEF)</ta>
            <ta e="T336" id="Seg_12527" s="T335">RUS:cult</ta>
            <ta e="T339" id="Seg_12528" s="T338">TURK:disc</ta>
            <ta e="T349" id="Seg_12529" s="T348">RUS:mod</ta>
            <ta e="T355" id="Seg_12530" s="T354">TURK:disc</ta>
            <ta e="T358" id="Seg_12531" s="T357">TURK:cult</ta>
            <ta e="T361" id="Seg_12532" s="T360">TURK:disc</ta>
            <ta e="T369" id="Seg_12533" s="T368">TURK:core</ta>
            <ta e="T370" id="Seg_12534" s="T369">%TURK:core</ta>
            <ta e="T371" id="Seg_12535" s="T370">RUS:gram</ta>
            <ta e="T373" id="Seg_12536" s="T372">%TURK:core</ta>
            <ta e="T376" id="Seg_12537" s="T375">RUS:gram</ta>
            <ta e="T377" id="Seg_12538" s="T376">TURK:disc</ta>
            <ta e="T381" id="Seg_12539" s="T380">TURK:disc</ta>
            <ta e="T392" id="Seg_12540" s="T391">RUS:gram</ta>
            <ta e="T401" id="Seg_12541" s="T400">RUS:gram</ta>
            <ta e="T405" id="Seg_12542" s="T404">RUS:gram</ta>
            <ta e="T407" id="Seg_12543" s="T406">RUS:mod</ta>
            <ta e="T410" id="Seg_12544" s="T409">TURK:disc</ta>
            <ta e="T416" id="Seg_12545" s="T415">RUS:mod</ta>
            <ta e="T420" id="Seg_12546" s="T419">RUS:mod</ta>
            <ta e="T421" id="Seg_12547" s="T420">RUS:gram(INDEF)</ta>
            <ta e="T422" id="Seg_12548" s="T421">TURK:cult</ta>
            <ta e="T431" id="Seg_12549" s="T430">RUS:gram</ta>
            <ta e="T448" id="Seg_12550" s="T447">TURK:disc</ta>
            <ta e="T454" id="Seg_12551" s="T453">RUS:gram</ta>
            <ta e="T458" id="Seg_12552" s="T457">RUS:gram</ta>
            <ta e="T462" id="Seg_12553" s="T461">TURK:disc</ta>
            <ta e="T464" id="Seg_12554" s="T463">RUS:mod</ta>
            <ta e="T467" id="Seg_12555" s="T466">RUS:cult</ta>
            <ta e="T469" id="Seg_12556" s="T468">RUS:gram</ta>
            <ta e="T474" id="Seg_12557" s="T473">RUS:gram</ta>
            <ta e="T486" id="Seg_12558" s="T485">RUS:mod</ta>
            <ta e="T489" id="Seg_12559" s="T488">RUS:mod</ta>
            <ta e="T494" id="Seg_12560" s="T493">TURK:disc</ta>
            <ta e="T520" id="Seg_12561" s="T519">%TURK:core</ta>
            <ta e="T542" id="Seg_12562" s="T541">TURK:cult</ta>
            <ta e="T544" id="Seg_12563" s="T543">TURK:cult</ta>
            <ta e="T552" id="Seg_12564" s="T551">TURK:cult</ta>
            <ta e="T554" id="Seg_12565" s="T553">TURK:disc</ta>
            <ta e="T561" id="Seg_12566" s="T560">RUS:core</ta>
            <ta e="T563" id="Seg_12567" s="T562">RUS:gram</ta>
            <ta e="T567" id="Seg_12568" s="T566">TURK:cult</ta>
            <ta e="T574" id="Seg_12569" s="T573">RUS:cult</ta>
            <ta e="T579" id="Seg_12570" s="T578">RUS:mod</ta>
            <ta e="T580" id="Seg_12571" s="T579">TURK:disc</ta>
            <ta e="T581" id="Seg_12572" s="T580">TURK:cult</ta>
            <ta e="T584" id="Seg_12573" s="T583">TURK:core</ta>
            <ta e="T586" id="Seg_12574" s="T585">RUS:gram</ta>
            <ta e="T593" id="Seg_12575" s="T592">TURK:cult</ta>
            <ta e="T596" id="Seg_12576" s="T595">RUS:cult</ta>
            <ta e="T611" id="Seg_12577" s="T610">RUS:mod</ta>
            <ta e="T612" id="Seg_12578" s="T611">RUS:cult</ta>
            <ta e="T614" id="Seg_12579" s="T613">RUS:mod</ta>
            <ta e="T615" id="Seg_12580" s="T614">RUS:cult</ta>
            <ta e="T617" id="Seg_12581" s="T616">RUS:cult</ta>
            <ta e="T636" id="Seg_12582" s="T635">TURK:cult</ta>
            <ta e="T637" id="Seg_12583" s="T636">TURK:core</ta>
            <ta e="T657" id="Seg_12584" s="T656">RUS:gram</ta>
            <ta e="T667" id="Seg_12585" s="T666">TURK:disc</ta>
            <ta e="T668" id="Seg_12586" s="T667">TURK:disc</ta>
            <ta e="T675" id="Seg_12587" s="T674">TURK:cult</ta>
            <ta e="T677" id="Seg_12588" s="T676">TURK:cult</ta>
            <ta e="T680" id="Seg_12589" s="T679">TAT:cult</ta>
            <ta e="T690" id="Seg_12590" s="T689">RUS:cult</ta>
            <ta e="T692" id="Seg_12591" s="T691">RUS:gram</ta>
            <ta e="T695" id="Seg_12592" s="T694">RUS:cult</ta>
            <ta e="T697" id="Seg_12593" s="T696">RUS:cult</ta>
            <ta e="T704" id="Seg_12594" s="T703">RUS:gram</ta>
            <ta e="T707" id="Seg_12595" s="T706">RUS:cult</ta>
            <ta e="T713" id="Seg_12596" s="T712">TURK:disc</ta>
            <ta e="T714" id="Seg_12597" s="T713">RUS:cult</ta>
            <ta e="T716" id="Seg_12598" s="T715">TURK:disc</ta>
            <ta e="T717" id="Seg_12599" s="T716">RUS:cult</ta>
            <ta e="T725" id="Seg_12600" s="T724">RUS:cult</ta>
            <ta e="T734" id="Seg_12601" s="T733">RUS:mod</ta>
            <ta e="T738" id="Seg_12602" s="T737">TURK:disc</ta>
            <ta e="T740" id="Seg_12603" s="T739">RUS:cult</ta>
            <ta e="T750" id="Seg_12604" s="T749">RUS:core</ta>
            <ta e="T761" id="Seg_12605" s="T760">RUS:core</ta>
            <ta e="T765" id="Seg_12606" s="T764">RUS:cult</ta>
            <ta e="T769" id="Seg_12607" s="T768">RUS:cult</ta>
            <ta e="T771" id="Seg_12608" s="T770">RUS:gram</ta>
            <ta e="T773" id="Seg_12609" s="T772">RUS:cult</ta>
            <ta e="T778" id="Seg_12610" s="T777">RUS:gram</ta>
            <ta e="T784" id="Seg_12611" s="T783">RUS:cult</ta>
            <ta e="T788" id="Seg_12612" s="T787">TAT:cult</ta>
            <ta e="T790" id="Seg_12613" s="T789">TURK:gram(INDEF)</ta>
            <ta e="T796" id="Seg_12614" s="T795">RUS:gram</ta>
            <ta e="T799" id="Seg_12615" s="T798">TURK:core</ta>
            <ta e="T802" id="Seg_12616" s="T801">TURK:core</ta>
            <ta e="T813" id="Seg_12617" s="T812">RUS:mod</ta>
            <ta e="T822" id="Seg_12618" s="T821">TURK:cult</ta>
            <ta e="T827" id="Seg_12619" s="T826">TURK:cult</ta>
            <ta e="T829" id="Seg_12620" s="T828">RUS:gram</ta>
            <ta e="T835" id="Seg_12621" s="T834">TURK:cult</ta>
            <ta e="T843" id="Seg_12622" s="T842">TURK:disc</ta>
            <ta e="T846" id="Seg_12623" s="T845">TURK:disc</ta>
            <ta e="T861" id="Seg_12624" s="T860">TURK:cult</ta>
            <ta e="T871" id="Seg_12625" s="T870">RUS:gram</ta>
            <ta e="T874" id="Seg_12626" s="T873">RUS:gram</ta>
            <ta e="T877" id="Seg_12627" s="T876">RUS:gram</ta>
            <ta e="T882" id="Seg_12628" s="T881">TURK:cult</ta>
            <ta e="T885" id="Seg_12629" s="T884">TURK:cult</ta>
            <ta e="T886" id="Seg_12630" s="T885">RUS:gram</ta>
            <ta e="T888" id="Seg_12631" s="T887">TURK:cult</ta>
            <ta e="T895" id="Seg_12632" s="T894">RUS:gram</ta>
            <ta e="T899" id="Seg_12633" s="T898">RUS:gram</ta>
            <ta e="T901" id="Seg_12634" s="T900">TURK:cult</ta>
            <ta e="T907" id="Seg_12635" s="T906">RUS:gram</ta>
            <ta e="T918" id="Seg_12636" s="T917">TURK:gram(INDEF)</ta>
            <ta e="T920" id="Seg_12637" s="T919">RUS:gram</ta>
            <ta e="T923" id="Seg_12638" s="T922">TURK:disc</ta>
            <ta e="T935" id="Seg_12639" s="T934">RUS:mod</ta>
            <ta e="T940" id="Seg_12640" s="T939">TURK:disc</ta>
            <ta e="T946" id="Seg_12641" s="T945">TURK:disc</ta>
            <ta e="T948" id="Seg_12642" s="T947">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T135" id="Seg_12643" s="T134">RUS:int</ta>
            <ta e="T143" id="Seg_12644" s="T139">RUS:calq</ta>
            <ta e="T320" id="Seg_12645" s="T319">RUS:int</ta>
            <ta e="T346" id="Seg_12646" s="T343">RUS:ext</ta>
            <ta e="T557" id="Seg_12647" s="T556">RUS:int</ta>
            <ta e="T686" id="Seg_12648" s="T685">RUS:int</ta>
            <ta e="T712" id="Seg_12649" s="T711">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T2" id="Seg_12650" s="T0">…начала откладывать яйца.</ta>
            <ta e="T9" id="Seg_12651" s="T2">Маленькая змейка на шапку упала</ta>
            <ta e="T13" id="Seg_12652" s="T9">Потом она спрыгнула [на землю].</ta>
            <ta e="T26" id="Seg_12653" s="T13">Потом она гналась-гналась, потом та с шапки упала, и она не пошла.</ta>
            <ta e="T38" id="Seg_12654" s="T26">Я часто к нему хожу, а он ко мне никогда не приходит.</ta>
            <ta e="T42" id="Seg_12655" s="T38">Я очень хорошо смотрю.</ta>
            <ta e="T44" id="Seg_12656" s="T42">Хорошо смотри.</ta>
            <ta e="T48" id="Seg_12657" s="T44">Этого человека как звать?</ta>
            <ta e="T51" id="Seg_12658" s="T48">И как его величать?</ta>
            <ta e="T53" id="Seg_12659" s="T51">Посмотри наверх!</ta>
            <ta e="T56" id="Seg_12660" s="T53">Посмотри наверх на гору!</ta>
            <ta e="T59" id="Seg_12661" s="T56">Надо веревку завязать.</ta>
            <ta e="T62" id="Seg_12662" s="T59">Шапку надо надеть.</ta>
            <ta e="T65" id="Seg_12663" s="T62">Шубу надо надеть.</ta>
            <ta e="T68" id="Seg_12664" s="T65">Потом на улицу идти.</ta>
            <ta e="T71" id="Seg_12665" s="T68">Я к чуму ходила.</ta>
            <ta e="T73" id="Seg_12666" s="T71">В чуме сидела.</ta>
            <ta e="T78" id="Seg_12667" s="T73">Потом ушла от чума.</ta>
            <ta e="T81" id="Seg_12668" s="T78">Я была маленькая.</ta>
            <ta e="T85" id="Seg_12669" s="T81">У нас была лошадь.</ta>
            <ta e="T90" id="Seg_12670" s="T85">[Ей было] два года.</ta>
            <ta e="T92" id="Seg_12671" s="T90">Мы ее запрягали.</ta>
            <ta e="T94" id="Seg_12672" s="T92">Тогда(?) она идет.</ta>
            <ta e="T101" id="Seg_12673" s="T94">Я иду, и она идет со мной.</ta>
            <ta e="T103" id="Seg_12674" s="T101">Я залезла на [=в?] чум.</ta>
            <ta e="T105" id="Seg_12675" s="T103">Посидела (на?) чуме.</ta>
            <ta e="T109" id="Seg_12676" s="T105">Потом я слезла с чума.</ta>
            <ta e="T113" id="Seg_12677" s="T109">Дом загорелся.</ta>
            <ta e="T118" id="Seg_12678" s="T113">Люди бегают, воду носят.</ta>
            <ta e="T121" id="Seg_12679" s="T118">Льют на огонь.</ta>
            <ta e="T124" id="Seg_12680" s="T121">Чтобы дом не сгорел.</ta>
            <ta e="T131" id="Seg_12681" s="T124">Один человек у другого все зерно сжег.</ta>
            <ta e="T133" id="Seg_12682" s="T131">Все сгорело.</ta>
            <ta e="T143" id="Seg_12683" s="T133">[Тот] хотел молотилку поставить, а этот взял и сжег зерно.</ta>
            <ta e="T148" id="Seg_12684" s="T143">Они поругались с ним, [тот] рассердился.</ta>
            <ta e="T150" id="Seg_12685" s="T148">Я траву косила.</ta>
            <ta e="T153" id="Seg_12686" s="T150">В зарод отвезла, положила.</ta>
            <ta e="T157" id="Seg_12687" s="T153">А оно загорелось.</ta>
            <ta e="T159" id="Seg_12688" s="T157">Я в чум пришла.</ta>
            <ta e="T166" id="Seg_12689" s="T159">Там постояла, потом домой пошла.</ta>
            <ta e="T173" id="Seg_12690" s="T166">Сегодня я не работаю, а с тобой разговариваю.</ta>
            <ta e="T179" id="Seg_12691" s="T173">Хоть не тяжело, а я очень устала.</ta>
            <ta e="T184" id="Seg_12692" s="T179">Очень сильный дождь идет.</ta>
            <ta e="T190" id="Seg_12693" s="T184">А я (под?) деревом сидела.</ta>
            <ta e="T193" id="Seg_12694" s="T190">Ветер начался.</ta>
            <ta e="T196" id="Seg_12695" s="T193">У меня рубаха красивая.</ta>
            <ta e="T198" id="Seg_12696" s="T196">Я там сидела.</ta>
            <ta e="T205" id="Seg_12697" s="T198">Меня не намочило, а то бы намочило.</ta>
            <ta e="T208" id="Seg_12698" s="T205">Я ходила в тайгу.</ta>
            <ta e="T214" id="Seg_12699" s="T208">Песню пела, а потом плакала.</ta>
            <ta e="T219" id="Seg_12700" s="T214">Мой сын умер, я жалею его.</ta>
            <ta e="T223" id="Seg_12701" s="T219">Женщина и мужчиниа живуть.</ta>
            <ta e="T227" id="Seg_12702" s="T223">А ты садись на (?).</ta>
            <ta e="T229" id="Seg_12703" s="T227">Где ты был?</ta>
            <ta e="T231" id="Seg_12704" s="T229">Куда идешь?</ta>
            <ta e="T233" id="Seg_12705" s="T231">Я домой иду.</ta>
            <ta e="T236" id="Seg_12706" s="T233">Денег нет.</ta>
            <ta e="T240" id="Seg_12707" s="T236">Без денег жить плохо.</ta>
            <ta e="T246" id="Seg_12708" s="T240">Когда деньги есть, жить хорошо.</ta>
            <ta e="T254" id="Seg_12709" s="T246">За деньги что-нибудь купишь, яйца можно купить, хлеб купить. </ta>
            <ta e="T260" id="Seg_12710" s="T254">Рыбу купить, мясо купишь за деньги всегда.</ta>
            <ta e="T266" id="Seg_12711" s="T260">А денег нет, так ничего не купишь.</ta>
            <ta e="T269" id="Seg_12712" s="T266">Ты идешь ловить рыбу.</ta>
            <ta e="T277" id="Seg_12713" s="T269">Если рыбу не поймаешь, домой не приходи.</ta>
            <ta e="T280" id="Seg_12714" s="T277">Иди позови родню.</ta>
            <ta e="T286" id="Seg_12715" s="T280">Если они не пойдут, один не (приходи?).</ta>
            <ta e="T288" id="Seg_12716" s="T286">Зови их хорошенько!</ta>
            <ta e="T290" id="Seg_12717" s="T288">Чтобы пришли.</ta>
            <ta e="T293" id="Seg_12718" s="T290">Нехороший человек.</ta>
            <ta e="T300" id="Seg_12719" s="T293">Просто глазами не могу смотреть, очень вонючий.</ta>
            <ta e="T302" id="Seg_12720" s="T300">Иди (медленно?).</ta>
            <ta e="T305" id="Seg_12721" s="T302">Быстро не (иди?).</ta>
            <ta e="T311" id="Seg_12722" s="T305">Я смотрю: он сам ко мне идет.</ta>
            <ta e="T318" id="Seg_12723" s="T311">К себе зовет, а я не иду.</ta>
            <ta e="T323" id="Seg_12724" s="T318">В лесу всякие деревья растут.</ta>
            <ta e="T329" id="Seg_12725" s="T323">Красивые цветы, очень силно пахнут.</ta>
            <ta e="T335" id="Seg_12726" s="T329">Я чувствую сильный [запах], что-то готовят.</ta>
            <ta e="T343" id="Seg_12727" s="T335">Куры ходят по земле, ногами в земле копаются.</ta>
            <ta e="T357" id="Seg_12728" s="T346">Этого человека надо подождать, он скоро придет.</ta>
            <ta e="T359" id="Seg_12729" s="T357">Водку принесет.</ta>
            <ta e="T363" id="Seg_12730" s="T359">Они его ждут.</ta>
            <ta e="T366" id="Seg_12731" s="T363">Они ждут.</ta>
            <ta e="T370" id="Seg_12732" s="T366">Сегодня мы хорошо поговорим.</ta>
            <ta e="T373" id="Seg_12733" s="T370">А что вы говорили?</ta>
            <ta e="T379" id="Seg_12734" s="T373">(…) мы ругались, и (вы?) ругались.</ta>
            <ta e="T382" id="Seg_12735" s="T379">Лошади идут.</ta>
            <ta e="T388" id="Seg_12736" s="T382">Две лошади идут, одна лошадь идет.</ta>
            <ta e="T391" id="Seg_12737" s="T388">Мой сын идет сюда.</ta>
            <ta e="T397" id="Seg_12738" s="T391">А где он был? - Ходил за рыбой.</ta>
            <ta e="T403" id="Seg_12739" s="T397">Я не знаю, принес или нет.</ta>
            <ta e="T407" id="Seg_12740" s="T403">Если принес, пожарить надо.</ta>
            <ta e="T411" id="Seg_12741" s="T407">Эта женщина беременна.</ta>
            <ta e="T415" id="Seg_12742" s="T411">(…) скоро ребенка родит.</ta>
            <ta e="T418" id="Seg_12743" s="T415">Надо ее (напугать?).</ta>
            <ta e="T423" id="Seg_12744" s="T418">Надо пойти что-нибудь купить.</ta>
            <ta e="T426" id="Seg_12745" s="T423">Когда день…</ta>
            <ta e="T427" id="Seg_12746" s="T426">(…).</ta>
            <ta e="T433" id="Seg_12747" s="T427">Когда день настает, я встаю.</ta>
            <ta e="T438" id="Seg_12748" s="T433">Когда день проходит, я ложусь.</ta>
            <ta e="T446" id="Seg_12749" s="T438">Эта зима была очень холодная, очень длинная.</ta>
            <ta e="T453" id="Seg_12750" s="T446">Вечера очень (длинныие?)</ta>
            <ta e="T460" id="Seg_12751" s="T453">А когда этот (год?) пришел, [стало] очень тепло.</ta>
            <ta e="T463" id="Seg_12752" s="T460">Дни длинные.</ta>
            <ta e="T471" id="Seg_12753" s="T463">Надо мне воду на печь поставить, а то теленок [скоро] придет.</ta>
            <ta e="T473" id="Seg_12754" s="T471">Воды нет.</ta>
            <ta e="T479" id="Seg_12755" s="T473">А холодную воду он не пьет.</ta>
            <ta e="T488" id="Seg_12756" s="T479">Поставь свою лошадь, она, наверное, не стоит. [?]</ta>
            <ta e="T491" id="Seg_12757" s="T488">Надо ее поставить.</ta>
            <ta e="T495" id="Seg_12758" s="T491">[Мои] сын с дочерью выросли.</ta>
            <ta e="T498" id="Seg_12759" s="T495">Сын говорит [одной девушке]</ta>
            <ta e="T502" id="Seg_12760" s="T498">"Пойдешь за меня замуж?"</ta>
            <ta e="T503" id="Seg_12761" s="T502">"Пойду".</ta>
            <ta e="T509" id="Seg_12762" s="T503">"Ну, я пришлю к тебе родителей".</ta>
            <ta e="T516" id="Seg_12763" s="T509">Он родителей послал, они пришли.</ta>
            <ta e="T520" id="Seg_12764" s="T516">С родителями девушки поговорили.</ta>
            <ta e="T527" id="Seg_12765" s="T520">"Свою дочь вы отдали нашему сыну?"</ta>
            <ta e="T529" id="Seg_12766" s="T527">"Мы отдали".</ta>
            <ta e="T538" id="Seg_12767" s="T529">Потом девушку спросили: "Ты пойдешь замуж за нашего сына?"</ta>
            <ta e="T539" id="Seg_12768" s="T538">"Пойду".</ta>
            <ta e="T541" id="Seg_12769" s="T539">Тогда они устроили (свадьбу?).</ta>
            <ta e="T546" id="Seg_12770" s="T541">Водку принесли, хлеб принесли, мясо.</ta>
            <ta e="T548" id="Seg_12771" s="T546">Много было.</ta>
            <ta e="T551" id="Seg_12772" s="T548">Потом люди пришли.</ta>
            <ta e="T554" id="Seg_12773" s="T551">Водку пили.</ta>
            <ta e="T559" id="Seg_12774" s="T554">Прошло несколько дней.</ta>
            <ta e="T562" id="Seg_12775" s="T559">Они одели цветочные [венки].</ta>
            <ta e="T565" id="Seg_12776" s="T562">И пошли в церковь.</ta>
            <ta e="T570" id="Seg_12777" s="T565">Там священник (за?) них молился.</ta>
            <ta e="T575" id="Seg_12778" s="T570">Они венки надели.</ta>
            <ta e="T578" id="Seg_12779" s="T575">Потом домой пришли.</ta>
            <ta e="T582" id="Seg_12780" s="T578">Они снова пили водку.</ta>
            <ta e="T585" id="Seg_12781" s="T582">Пришли родственники девушки.</ta>
            <ta e="T589" id="Seg_12782" s="T585">И (сестра?) парня пришла.</ta>
            <ta e="T592" id="Seg_12783" s="T589">Было много народу.</ta>
            <ta e="T595" id="Seg_12784" s="T592">Много водки было.</ta>
            <ta e="T601" id="Seg_12785" s="T595">На столе много всего лежало.</ta>
            <ta e="T607" id="Seg_12786" s="T601">Когда девушку (посватали?), ее родители сказали:</ta>
            <ta e="T610" id="Seg_12787" s="T607">"У нее одежды нет.</ta>
            <ta e="T618" id="Seg_12788" s="T610">Надо ботинки купить, надо пальто купить, платок купить.</ta>
            <ta e="T622" id="Seg_12789" s="T618">У нее одежды мало, рубахи нет".</ta>
            <ta e="T627" id="Seg_12790" s="T622">Они взяли деньги.</ta>
            <ta e="T630" id="Seg_12791" s="T627">Двадцать пять.</ta>
            <ta e="T636" id="Seg_12792" s="T630">Родители дали им корову.</ta>
            <ta e="T641" id="Seg_12793" s="T636">Родственники дали [им] овец, дали лошадей.</ta>
            <ta e="T645" id="Seg_12794" s="T641">Денег дали очень много.</ta>
            <ta e="T649" id="Seg_12795" s="T645">Много людей было.</ta>
            <ta e="T653" id="Seg_12796" s="T649">Потом они долго жили.</ta>
            <ta e="T657" id="Seg_12797" s="T653">Ее брат (?).</ta>
            <ta e="T660" id="Seg_12798" s="T657">Опять свадьбу устроили (??).</ta>
            <ta e="T667" id="Seg_12799" s="T660">Потом она ушла, отец дал ей все.</ta>
            <ta e="T676" id="Seg_12800" s="T667">Все дал, лошадь дал, овец дал, коров</ta>
            <ta e="T681" id="Seg_12801" s="T676">Дал зерно, построил дом.</ta>
            <ta e="T687" id="Seg_12802" s="T681">Они поехали на лошадях венчаться.</ta>
            <ta e="T698" id="Seg_12803" s="T687">Девушка со своим посаженым отцом, парень со своим посаженым отцом, посаженой матерью.</ta>
            <ta e="T703" id="Seg_12804" s="T698">Они пришли туда, потом пошли домой.</ta>
            <ta e="T713" id="Seg_12805" s="T703">Потом посаженый отец девушки дал ей ящик одежды.</ta>
            <ta e="T716" id="Seg_12806" s="T713">Дал подушки.</ta>
            <ta e="T718" id="Seg_12807" s="T716">Дал перину.</ta>
            <ta e="T721" id="Seg_12808" s="T718">Потом они пошли в церковь.</ta>
            <ta e="T725" id="Seg_12809" s="T721">Они сели за стол.</ta>
            <ta e="T731" id="Seg_12810" s="T725">Потом… У парня рубаха красивая, новая.</ta>
            <ta e="T735" id="Seg_12811" s="T731">У девушки одежда тоже красивая.</ta>
            <ta e="T736" id="Seg_12812" s="T735">Новая.</ta>
            <ta e="T741" id="Seg_12813" s="T736">Ее(?) голова красивая, они надели венки.</ta>
            <ta e="T746" id="Seg_12814" s="T741">Потом ей…</ta>
            <ta e="T751" id="Seg_12815" s="T746">Потом ей две косы заплели.</ta>
            <ta e="T754" id="Seg_12816" s="T751">Голову повязали [платком].</ta>
            <ta e="T762" id="Seg_12817" s="T754">Когда она девушкой была, у нее была одна коса.</ta>
            <ta e="T777" id="Seg_12818" s="T762">Потом посаженая мать девушки и посаженая мать юноши заплели ей две косы.</ta>
            <ta e="T780" id="Seg_12819" s="T777">И повязали голову.</ta>
            <ta e="T788" id="Seg_12820" s="T780">Их, юношу и девушку, посаженый отец привез домой.</ta>
            <ta e="T792" id="Seg_12821" s="T788">Там никого не было</ta>
            <ta e="T797" id="Seg_12822" s="T792">Они легли спать, (?).</ta>
            <ta e="T801" id="Seg_12823" s="T797">Родственники им сказали:</ta>
            <ta e="T804" id="Seg_12824" s="T801">"Хорошо живите.</ta>
            <ta e="T806" id="Seg_12825" s="T804">Не ссорьтесь.</ta>
            <ta e="T812" id="Seg_12826" s="T806">Не ругайтесь.</ta>
            <ta e="T819" id="Seg_12827" s="T812">Пусть детей будет много, девочек и мальчиков.</ta>
            <ta e="T823" id="Seg_12828" s="T819">Потом они позвали священника.</ta>
            <ta e="T828" id="Seg_12829" s="T823">Он очень много водки выпил.</ta>
            <ta e="T830" id="Seg_12830" s="T828">И упал.</ta>
            <ta e="T832" id="Seg_12831" s="T830">Его унесли.</ta>
            <ta e="T836" id="Seg_12832" s="T832">Они много водки выпили.</ta>
            <ta e="T840" id="Seg_12833" s="T836">Долго, шесть дней пили.</ta>
            <ta e="T844" id="Seg_12834" s="T840">Потом они жили.</ta>
            <ta e="T848" id="Seg_12835" s="T844">У них дети были.</ta>
            <ta e="T852" id="Seg_12836" s="T848">Сыновья были, дочери были.</ta>
            <ta e="T854" id="Seg_12837" s="T852">Дочери.</ta>
            <ta e="T855" id="Seg_12838" s="T854">(…)</ta>
            <ta e="T862" id="Seg_12839" s="T855">Мой сын вчера ушел, водку пил.</ta>
            <ta e="T867" id="Seg_12840" s="T862">Очень долго не приходил домой.</ta>
            <ta e="T870" id="Seg_12841" s="T867">Потом пришел, уснул.</ta>
            <ta e="T876" id="Seg_12842" s="T870">Утром встал и стал работать.</ta>
            <ta e="T883" id="Seg_12843" s="T876">А я двух коров доила</ta>
            <ta e="T888" id="Seg_12844" s="T883">Его корову и свою корову.</ta>
            <ta e="T892" id="Seg_12845" s="T888">Одна женщина была беременная.</ta>
            <ta e="T894" id="Seg_12846" s="T892">Она родила ребенка.</ta>
            <ta e="T901" id="Seg_12847" s="T894">А он туда пошел и выпил водки.</ta>
            <ta e="T906" id="Seg_12848" s="T901">Я вчера видела (?).</ta>
            <ta e="T913" id="Seg_12849" s="T906">А сегодня вечером…</ta>
            <ta e="T923" id="Seg_12850" s="T913">А я его видела (?) он что-то принес, а я рассердилась.</ta>
            <ta e="T927" id="Seg_12851" s="T923">(…) не пришли из леса.</ta>
            <ta e="T930" id="Seg_12852" s="T927">Когда они придут?</ta>
            <ta e="T933" id="Seg_12853" s="T930">Сегодня придут к вечеру.</ta>
            <ta e="T938" id="Seg_12854" s="T933">Надо их покормить.</ta>
            <ta e="T941" id="Seg_12855" s="T938">Они же голодные.</ta>
            <ta e="T947" id="Seg_12856" s="T941">Я сегодня думала, что вы придете.</ta>
            <ta e="T951" id="Seg_12857" s="T947">А вы не пришли.</ta>
            <ta e="T957" id="Seg_12858" s="T951">Я сегодня (яйца?) сварила.</ta>
            <ta e="T965" id="Seg_12859" s="T957">Черемшу порезала, яйца поставила, (?) поставила, чтобы вас покормить.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T2" id="Seg_12860" s="T0">…began to lay eggs.</ta>
            <ta e="T9" id="Seg_12861" s="T2">One small snake fell to the cap.</ta>
            <ta e="T13" id="Seg_12862" s="T9">Then it sprang [to the ground].</ta>
            <ta e="T26" id="Seg_12863" s="T13">It pursued and pursued [it], then it sprang from the cap, and didn't go.</ta>
            <ta e="T38" id="Seg_12864" s="T26">I often go to him, and he never comes to me.</ta>
            <ta e="T42" id="Seg_12865" s="T38">I look very well.</ta>
            <ta e="T44" id="Seg_12866" s="T42">Look well.</ta>
            <ta e="T48" id="Seg_12867" s="T44">What is this person's name?</ta>
            <ta e="T51" id="Seg_12868" s="T48">What is his [patronymic?] name?</ta>
            <ta e="T53" id="Seg_12869" s="T51">Look up!</ta>
            <ta e="T56" id="Seg_12870" s="T53">Look up at the mountain.</ta>
            <ta e="T59" id="Seg_12871" s="T56">[I] should tie the rope.</ta>
            <ta e="T62" id="Seg_12872" s="T59">[I] should put on the cap.</ta>
            <ta e="T65" id="Seg_12873" s="T62">[I] should put on the coat.</ta>
            <ta e="T68" id="Seg_12874" s="T65">Then go outside.</ta>
            <ta e="T71" id="Seg_12875" s="T68">I went to the (tent?).</ta>
            <ta e="T73" id="Seg_12876" s="T71">I sat in the tent.</ta>
            <ta e="T78" id="Seg_12877" s="T73">Then I went (back?) to the tent.</ta>
            <ta e="T81" id="Seg_12878" s="T78">I was little.</ta>
            <ta e="T85" id="Seg_12879" s="T81">We had a horse.</ta>
            <ta e="T90" id="Seg_12880" s="T85">[It was] two years [old].</ta>
            <ta e="T92" id="Seg_12881" s="T90">We harnessed it.</ta>
            <ta e="T94" id="Seg_12882" s="T92">It goes.</ta>
            <ta e="T101" id="Seg_12883" s="T94">I go, and it goes with me.</ta>
            <ta e="T103" id="Seg_12884" s="T101">I climbed on [=in?] the tent.</ta>
            <ta e="T105" id="Seg_12885" s="T103">I sat (on?) the tent.</ta>
            <ta e="T109" id="Seg_12886" s="T105">Then I descended (to?) the tent.</ta>
            <ta e="T113" id="Seg_12887" s="T109">A house has caught fire.</ta>
            <ta e="T118" id="Seg_12888" s="T113">People are running, bringing water.</ta>
            <ta e="T121" id="Seg_12889" s="T118">They're pouring [it] on the fire.</ta>
            <ta e="T124" id="Seg_12890" s="T121">So that [the house] won't burn down.</ta>
            <ta e="T131" id="Seg_12891" s="T124">One man burned all another's grain.</ta>
            <ta e="T133" id="Seg_12892" s="T131">He burned all [grain].</ta>
            <ta e="T143" id="Seg_12893" s="T133">He wanted to set a threshing machine, but that [person] suddenly burned the grain.</ta>
            <ta e="T148" id="Seg_12894" s="T143">He [had] quarrelled with him, he became angry.</ta>
            <ta e="T150" id="Seg_12895" s="T148">I cut grass.</ta>
            <ta e="T153" id="Seg_12896" s="T150">Carried it to zarod, put [there].</ta>
            <ta e="T157" id="Seg_12897" s="T153">And it took fire.</ta>
            <ta e="T159" id="Seg_12898" s="T157">I came to a (house?).</ta>
            <ta e="T166" id="Seg_12899" s="T159">I stood there, then went home.</ta>
            <ta e="T173" id="Seg_12900" s="T166">Today I'm not working, I'm speaking with you.</ta>
            <ta e="T179" id="Seg_12901" s="T173">Although it's not hard, I'm very tired.</ta>
            <ta e="T184" id="Seg_12902" s="T179">It rains hard. </ta>
            <ta e="T190" id="Seg_12903" s="T184">And I sat (under?) a tree.</ta>
            <ta e="T193" id="Seg_12904" s="T190">Wind comes.</ta>
            <ta e="T196" id="Seg_12905" s="T193">My shirt is beautiful.</ta>
            <ta e="T198" id="Seg_12906" s="T196">I sat there.</ta>
            <ta e="T205" id="Seg_12907" s="T198">I didn't get wet, otherwise I would've got wet.</ta>
            <ta e="T208" id="Seg_12908" s="T205">I went to the forest.</ta>
            <ta e="T214" id="Seg_12909" s="T208">I sang a song and then I cried.</ta>
            <ta e="T219" id="Seg_12910" s="T214">My son died, I am sorry for him.</ta>
            <ta e="T223" id="Seg_12911" s="T219">There live a woman and a man.</ta>
            <ta e="T227" id="Seg_12912" s="T223">And you sit on (?).</ta>
            <ta e="T229" id="Seg_12913" s="T227">Where were you?</ta>
            <ta e="T231" id="Seg_12914" s="T229">Where do you go?</ta>
            <ta e="T233" id="Seg_12915" s="T231">I'm going home.</ta>
            <ta e="T236" id="Seg_12916" s="T233">There is no money.</ta>
            <ta e="T240" id="Seg_12917" s="T236">It is not good to live without money.</ta>
            <ta e="T246" id="Seg_12918" s="T240">When you have money, you live good.</ta>
            <ta e="T254" id="Seg_12919" s="T246">For money, you'll buy something, you can buy eggs, or bread.</ta>
            <ta e="T260" id="Seg_12920" s="T254">Buy fish, you'll buy meat for money, always.</ta>
            <ta e="T266" id="Seg_12921" s="T260">If there is no money, you won't buy anything.</ta>
            <ta e="T269" id="Seg_12922" s="T266">You're going to catch fish.</ta>
            <ta e="T277" id="Seg_12923" s="T269">If you don't catch any fish, don't come home.</ta>
            <ta e="T280" id="Seg_12924" s="T277">Go to call your relatives.</ta>
            <ta e="T286" id="Seg_12925" s="T280">If they don't come, don't come alone. [?]</ta>
            <ta e="T288" id="Seg_12926" s="T286">Call them well!</ta>
            <ta e="T290" id="Seg_12927" s="T288">So that they come.</ta>
            <ta e="T293" id="Seg_12928" s="T290">Not a good person.</ta>
            <ta e="T300" id="Seg_12929" s="T293">I can't even look at him with my eyes, he's so stinking.</ta>
            <ta e="T302" id="Seg_12930" s="T300">Go slowly. [?]</ta>
            <ta e="T305" id="Seg_12931" s="T302">Don't go fast. [?]</ta>
            <ta e="T311" id="Seg_12932" s="T305">I see: he's coming himself to me.</ta>
            <ta e="T318" id="Seg_12933" s="T311">He (calls?) me to himself, but I don't go.</ta>
            <ta e="T323" id="Seg_12934" s="T318">All kinds of trees grow in the forest.</ta>
            <ta e="T329" id="Seg_12935" s="T323">Beautiful flowers, they smell very strongly.</ta>
            <ta e="T335" id="Seg_12936" s="T329">I feel very well, it smells good, something is being cooked.</ta>
            <ta e="T343" id="Seg_12937" s="T335">The hens walk on the ground, dig the earth with their feet.</ta>
            <ta e="T357" id="Seg_12938" s="T346">We should wait for this man, he comes soon.</ta>
            <ta e="T359" id="Seg_12939" s="T357">He'll bring vodka.</ta>
            <ta e="T363" id="Seg_12940" s="T359">They are waiting for him.</ta>
            <ta e="T366" id="Seg_12941" s="T363">They are waiting.</ta>
            <ta e="T370" id="Seg_12942" s="T366">Today we will speak well.</ta>
            <ta e="T373" id="Seg_12943" s="T370">What did you say?</ta>
            <ta e="T379" id="Seg_12944" s="T373">(…) we were scolding and (you?) were scolding.</ta>
            <ta e="T382" id="Seg_12945" s="T379">Horses are going.</ta>
            <ta e="T388" id="Seg_12946" s="T382">Two horses are going, one horse is going.</ta>
            <ta e="T391" id="Seg_12947" s="T388">My son is coming.</ta>
            <ta e="T397" id="Seg_12948" s="T391">Where he was? - He went to bring fish.</ta>
            <ta e="T403" id="Seg_12949" s="T397">I don't know, whether he brought [it] or not.</ta>
            <ta e="T407" id="Seg_12950" s="T403">If he brought [it], [I] must fry it.</ta>
            <ta e="T411" id="Seg_12951" s="T407">This woman is pregnant.</ta>
            <ta e="T415" id="Seg_12952" s="T411">(…) will soon bring a child.</ta>
            <ta e="T418" id="Seg_12953" s="T415">[We] should frighten her. [?]</ta>
            <ta e="T423" id="Seg_12954" s="T418">[I] should go and buy something.</ta>
            <ta e="T426" id="Seg_12955" s="T423">When a day…</ta>
            <ta e="T427" id="Seg_12956" s="T426">(…).</ta>
            <ta e="T433" id="Seg_12957" s="T427">When the day comes, I stand up.</ta>
            <ta e="T438" id="Seg_12958" s="T433">When the day goes away, I lie down.</ta>
            <ta e="T446" id="Seg_12959" s="T438">This winter was very cold, was very long.</ta>
            <ta e="T453" id="Seg_12960" s="T446">Evenings are very (long?)</ta>
            <ta e="T460" id="Seg_12961" s="T453">And as this (year?) came, [it is] very warm.</ta>
            <ta e="T463" id="Seg_12962" s="T460">Days are long.</ta>
            <ta e="T471" id="Seg_12963" s="T463">I must put water on the stove, because the calf will come [soon].</ta>
            <ta e="T473" id="Seg_12964" s="T471">There is no water.</ta>
            <ta e="T479" id="Seg_12965" s="T473">It doesn't drink cold water.</ta>
            <ta e="T488" id="Seg_12966" s="T479">Put your horse, it's probably not standing. [?]</ta>
            <ta e="T491" id="Seg_12967" s="T488">[You] should put it.</ta>
            <ta e="T495" id="Seg_12968" s="T491">[My] son and [my] daughter have grown up.</ta>
            <ta e="T498" id="Seg_12969" s="T495">Then [my] son said [to some girl]:</ta>
            <ta e="T502" id="Seg_12970" s="T498">"Will you marry me?"</ta>
            <ta e="T503" id="Seg_12971" s="T502">"I will."</ta>
            <ta e="T509" id="Seg_12972" s="T503">"Well, then I'll send my parents to you."</ta>
            <ta e="T516" id="Seg_12973" s="T509">Then he sent his parents, they came.</ta>
            <ta e="T520" id="Seg_12974" s="T516">They spoke with the girl's parents.</ta>
            <ta e="T527" id="Seg_12975" s="T520">"Did you give your daughter to my son?"</ta>
            <ta e="T529" id="Seg_12976" s="T527">"We gave."</ta>
            <ta e="T538" id="Seg_12977" s="T529">Then they asked the girl: "Will you marry our son?"</ta>
            <ta e="T539" id="Seg_12978" s="T538">"I will."</ta>
            <ta e="T541" id="Seg_12979" s="T539">Then they organized a wedding. [?]</ta>
            <ta e="T546" id="Seg_12980" s="T541">They brought vodka, brought bread, meat.</ta>
            <ta e="T548" id="Seg_12981" s="T546">There was much.</ta>
            <ta e="T551" id="Seg_12982" s="T548">Then people came.</ta>
            <ta e="T554" id="Seg_12983" s="T551">They drank vodka.</ta>
            <ta e="T559" id="Seg_12984" s="T554">Then a few days passed.</ta>
            <ta e="T562" id="Seg_12985" s="T559">They put on flower [wreaths].</ta>
            <ta e="T565" id="Seg_12986" s="T562">And went to the church.</ta>
            <ta e="T570" id="Seg_12987" s="T565">There the priest prayed God for them. [?]</ta>
            <ta e="T575" id="Seg_12988" s="T570">Then they put on garlands.</ta>
            <ta e="T578" id="Seg_12989" s="T575">Then they came home.</ta>
            <ta e="T582" id="Seg_12990" s="T578">They drank vodka again.</ta>
            <ta e="T585" id="Seg_12991" s="T582">The girl's relatives came.</ta>
            <ta e="T589" id="Seg_12992" s="T585">And the boy's sister (?) came.</ta>
            <ta e="T592" id="Seg_12993" s="T589">There was a lot of people.</ta>
            <ta e="T595" id="Seg_12994" s="T592">There was a lot of vodka.</ta>
            <ta e="T601" id="Seg_12995" s="T595">There was a lot of various things on the table.</ta>
            <ta e="T607" id="Seg_12996" s="T601">When the girl was going to (marry?), her parents said:</ta>
            <ta e="T610" id="Seg_12997" s="T607">"She has no clothes.</ta>
            <ta e="T618" id="Seg_12998" s="T610">We should buy shoes, coat, headscarf.</ta>
            <ta e="T622" id="Seg_12999" s="T618">She has few clothes, she has no shirt."</ta>
            <ta e="T627" id="Seg_13000" s="T622">Then they took money.</ta>
            <ta e="T630" id="Seg_13001" s="T627">Twenty-five.</ta>
            <ta e="T636" id="Seg_13002" s="T630">Then the parents gave them a cow.</ta>
            <ta e="T641" id="Seg_13003" s="T636">The relatives gave [them] sheep, gave [them] horses.</ta>
            <ta e="T645" id="Seg_13004" s="T641">They gave [them] much money.</ta>
            <ta e="T649" id="Seg_13005" s="T645">There were many people.</ta>
            <ta e="T653" id="Seg_13006" s="T649">Then they lived long.</ta>
            <ta e="T657" id="Seg_13007" s="T653">His elder brother (?).</ta>
            <ta e="T660" id="Seg_13008" s="T657">They made wedding again (??).</ta>
            <ta e="T667" id="Seg_13009" s="T660">Then she went off, her father gave her everything.</ta>
            <ta e="T676" id="Seg_13010" s="T667">He gave everything, he gave a horse, he gave sheep, he gave cows.</ta>
            <ta e="T681" id="Seg_13011" s="T676">He gave grain, he built a house.</ta>
            <ta e="T687" id="Seg_13012" s="T681">They went to marry to the church, on horses.</ta>
            <ta e="T698" id="Seg_13013" s="T687">The girl has her own honorary father, the boy has his own honorary father, honorary mother.</ta>
            <ta e="T703" id="Seg_13014" s="T698">They went there, then came home.</ta>
            <ta e="T713" id="Seg_13015" s="T703">Then the girl's honorary father brought her a box of clothes.</ta>
            <ta e="T716" id="Seg_13016" s="T713">He gave pillows.</ta>
            <ta e="T718" id="Seg_13017" s="T716">He gave a feather bed.</ta>
            <ta e="T721" id="Seg_13018" s="T718">Then they went to the church.</ta>
            <ta e="T725" id="Seg_13019" s="T721">They sat at table.</ta>
            <ta e="T731" id="Seg_13020" s="T725">Then… The boy's shirt is beautiful, new.</ta>
            <ta e="T735" id="Seg_13021" s="T731">The girl's clothes are beautiful, too.</ta>
            <ta e="T736" id="Seg_13022" s="T735">New.</ta>
            <ta e="T741" id="Seg_13023" s="T736">Her(?) head is beautiful, they wear wreaths.</ta>
            <ta e="T746" id="Seg_13024" s="T741">Then to her…</ta>
            <ta e="T751" id="Seg_13025" s="T746">Then they made her two braids.</ta>
            <ta e="T754" id="Seg_13026" s="T751">They they bound [a headscarf] on her head.</ta>
            <ta e="T762" id="Seg_13027" s="T754">When she was a girl, she had one braid.</ta>
            <ta e="T777" id="Seg_13028" s="T762">Then this girl's honorary mother and the boy's honorary mother made her two [braids].</ta>
            <ta e="T780" id="Seg_13029" s="T777">And bound up her head.</ta>
            <ta e="T788" id="Seg_13030" s="T780">The honorary father brought them, the girl and the boy, in the house.</ta>
            <ta e="T792" id="Seg_13031" s="T788">Nobody was there.</ta>
            <ta e="T797" id="Seg_13032" s="T792">They(?) went to bed, (?).</ta>
            <ta e="T801" id="Seg_13033" s="T797">Their relatives said them:</ta>
            <ta e="T804" id="Seg_13034" s="T801">"Live well.</ta>
            <ta e="T806" id="Seg_13035" s="T804">Don't quarrel.</ta>
            <ta e="T812" id="Seg_13036" s="T806">Don't swear at each other.</ta>
            <ta e="T819" id="Seg_13037" s="T812">Let the children be numerous, girls and boys.</ta>
            <ta e="T823" id="Seg_13038" s="T819">Then they called the priest.</ta>
            <ta e="T828" id="Seg_13039" s="T823">He drank very much vodka.</ta>
            <ta e="T830" id="Seg_13040" s="T828">And fell down.</ta>
            <ta e="T832" id="Seg_13041" s="T830">They took him away.</ta>
            <ta e="T836" id="Seg_13042" s="T832">They drank a lot of vodka.</ta>
            <ta e="T840" id="Seg_13043" s="T836">They drank for a long time, six days.</ta>
            <ta e="T844" id="Seg_13044" s="T840">Then they lived.</ta>
            <ta e="T848" id="Seg_13045" s="T844">They had children.</ta>
            <ta e="T852" id="Seg_13046" s="T848">[They] had sons, and daughters.</ta>
            <ta e="T854" id="Seg_13047" s="T852">Daughters.</ta>
            <ta e="T855" id="Seg_13048" s="T854">(…)</ta>
            <ta e="T862" id="Seg_13049" s="T855">My son (?) went away yesterday, he drank vodka.</ta>
            <ta e="T867" id="Seg_13050" s="T862">He didn't come home for a very long time.</ta>
            <ta e="T870" id="Seg_13051" s="T867">Then he came, fell asleep.</ta>
            <ta e="T876" id="Seg_13052" s="T870">In the morning he got up and began to work.</ta>
            <ta e="T883" id="Seg_13053" s="T876">I milked two cows.</ta>
            <ta e="T888" id="Seg_13054" s="T883">His cow and my own cow.</ta>
            <ta e="T892" id="Seg_13055" s="T888">One woman was pregnant.</ta>
            <ta e="T894" id="Seg_13056" s="T892">Then she bore a child.</ta>
            <ta e="T901" id="Seg_13057" s="T894">And he went there and drank vodka.</ta>
            <ta e="T906" id="Seg_13058" s="T901">I saw (?) yesterday.</ta>
            <ta e="T913" id="Seg_13059" s="T906">And this evening…</ta>
            <ta e="T923" id="Seg_13060" s="T913">Then I saw him (?), (he?) brought something, and I was angry.</ta>
            <ta e="T927" id="Seg_13061" s="T923">(…) they didn't come from the forest.</ta>
            <ta e="T930" id="Seg_13062" s="T927">When will they come?</ta>
            <ta e="T933" id="Seg_13063" s="T930">They'll come today in the evening.</ta>
            <ta e="T938" id="Seg_13064" s="T933">[We] should then give them to eat.</ta>
            <ta e="T941" id="Seg_13065" s="T938">They are hungry.</ta>
            <ta e="T947" id="Seg_13066" s="T941">I believed today that you all would come.</ta>
            <ta e="T951" id="Seg_13067" s="T947">And you didn't come.</ta>
            <ta e="T957" id="Seg_13068" s="T951">I cooked (eggs?) today.</ta>
            <ta e="T965" id="Seg_13069" s="T957">I cut onions, put eggs [on the table], put (?), to give you to eat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T2" id="Seg_13070" s="T0">… fing an Eier zu legen.</ta>
            <ta e="T9" id="Seg_13071" s="T2">Eine kleine Schlange fiel auf die Mütze.</ta>
            <ta e="T13" id="Seg_13072" s="T9">Dann sprang sie [auf den Boden].</ta>
            <ta e="T26" id="Seg_13073" s="T13">Sie(?) verfolgte und verfolgte [es], dann sprang sie von der Mütze und ging nicht.</ta>
            <ta e="T38" id="Seg_13074" s="T26">Ich gehe oft zu ihm und er kommt nie zu mir.</ta>
            <ta e="T42" id="Seg_13075" s="T38">Ich kann sehr gut gucken.</ta>
            <ta e="T44" id="Seg_13076" s="T42">Gucke gut.</ta>
            <ta e="T48" id="Seg_13077" s="T44">Wie heißt diese Person?</ta>
            <ta e="T51" id="Seg_13078" s="T48">Wie ist ihr Name [= Vatersname?]?</ta>
            <ta e="T53" id="Seg_13079" s="T51">Schau hoch!</ta>
            <ta e="T56" id="Seg_13080" s="T53">Schau hoch auf den Berg!</ta>
            <ta e="T59" id="Seg_13081" s="T56">[Ich] sollte das Seil festbinden.</ta>
            <ta e="T62" id="Seg_13082" s="T59">[Ich] sollte die Mütze aufsetzen.</ta>
            <ta e="T65" id="Seg_13083" s="T62">[Ich] sollte den Mantel anziehen.</ta>
            <ta e="T68" id="Seg_13084" s="T65">Dann hinausgehen.</ta>
            <ta e="T71" id="Seg_13085" s="T68">Ich ging zum (Zelt?).</ta>
            <ta e="T73" id="Seg_13086" s="T71">Ich saß im Zelt.</ta>
            <ta e="T78" id="Seg_13087" s="T73">Dann ging ich im Zelt zurück. [?]</ta>
            <ta e="T81" id="Seg_13088" s="T78">Ich war klein.</ta>
            <ta e="T85" id="Seg_13089" s="T81">Wir hatten ein Pferd.</ta>
            <ta e="T90" id="Seg_13090" s="T85">[Es war] zwei Jahre [alt].</ta>
            <ta e="T92" id="Seg_13091" s="T90">Wir spannten es an.</ta>
            <ta e="T94" id="Seg_13092" s="T92">Es geht.</ta>
            <ta e="T101" id="Seg_13093" s="T94">Ich gehe und es geht mit mir.</ta>
            <ta e="T103" id="Seg_13094" s="T101">Ich kletterte auf [=in?] das Zelt.</ta>
            <ta e="T105" id="Seg_13095" s="T103">Ich saß auf (?) dem Zelt.</ta>
            <ta e="T109" id="Seg_13096" s="T105">Dann kletterte ich (zum?) Zelt hinunter.</ta>
            <ta e="T113" id="Seg_13097" s="T109">Ein Haus fing an zu brennen.</ta>
            <ta e="T118" id="Seg_13098" s="T113">Die Leute rennen und bringen Wasser.</ta>
            <ta e="T121" id="Seg_13099" s="T118">Sie gießen [es] ins Feuer.</ta>
            <ta e="T124" id="Seg_13100" s="T121">Damit [das Haus] nicht abbrennt.</ta>
            <ta e="T131" id="Seg_13101" s="T124">Ein Mann verbrannt das ganze Korn eines anderen.</ta>
            <ta e="T133" id="Seg_13102" s="T131">Er verbrannte alles [Korn].</ta>
            <ta e="T143" id="Seg_13103" s="T133">Er wollte eine Dreschmaschine in Gang setzen, but jener verbrannte plötzlich das Korn.</ta>
            <ta e="T148" id="Seg_13104" s="T143">Er [hatte]mit ihm gestritten, er wurde böse.</ta>
            <ta e="T150" id="Seg_13105" s="T148">Ich schnitt Gras.</ta>
            <ta e="T153" id="Seg_13106" s="T150">Ich trug es nach Zarod, legte es [dorthin].</ta>
            <ta e="T157" id="Seg_13107" s="T153">Und es fing an zu brennen.</ta>
            <ta e="T159" id="Seg_13108" s="T157">Ich kam zu einem Haus. [?]</ta>
            <ta e="T166" id="Seg_13109" s="T159">Ich stand dort, dann ging ich nach Hause.</ta>
            <ta e="T173" id="Seg_13110" s="T166">Heute arbeite ich nicht, ich spreche mit dir.</ta>
            <ta e="T179" id="Seg_13111" s="T173">Obwohl es nicht schwierig ist, bin ich sehr müde.</ta>
            <ta e="T184" id="Seg_13112" s="T179">Es regnet doll.</ta>
            <ta e="T190" id="Seg_13113" s="T184">Und ich saß (unter?) einem Baum.</ta>
            <ta e="T193" id="Seg_13114" s="T190">Wind kommt auf.</ta>
            <ta e="T196" id="Seg_13115" s="T193">Mein Hemd ist schön.</ta>
            <ta e="T198" id="Seg_13116" s="T196">Ich saß dort.</ta>
            <ta e="T205" id="Seg_13117" s="T198">Ich wurde nicht nass, sonst wäre ich nass geworden.</ta>
            <ta e="T208" id="Seg_13118" s="T205">Ich ging in den Wald.</ta>
            <ta e="T214" id="Seg_13119" s="T208">Ich sang ein Lied und dann weinte ich.</ta>
            <ta e="T219" id="Seg_13120" s="T214">Mein Sohn ist gestorben, ich trauere um ihn.</ta>
            <ta e="T223" id="Seg_13121" s="T219">Es leben eine Frau und ein Mann.</ta>
            <ta e="T227" id="Seg_13122" s="T223">Und du setzte dich auf (?).</ta>
            <ta e="T229" id="Seg_13123" s="T227">Wo warst du?</ta>
            <ta e="T231" id="Seg_13124" s="T229">Wohin gehst du?</ta>
            <ta e="T233" id="Seg_13125" s="T231">Ich gehe nach Hause.</ta>
            <ta e="T236" id="Seg_13126" s="T233">Es gibt kein Geld.</ta>
            <ta e="T240" id="Seg_13127" s="T236">Es ist nicht gut, ohne Geld zu leben.</ta>
            <ta e="T246" id="Seg_13128" s="T240">Wenn du Geld hast, lebt man gut.</ta>
            <ta e="T254" id="Seg_13129" s="T246">Für Geld kannst du etwas kaufen, du kannst Eier kaufen oder Brot.</ta>
            <ta e="T260" id="Seg_13130" s="T254">Fisch kaufen, du wirst Fleisch für Geld kaufen, immer.</ta>
            <ta e="T266" id="Seg_13131" s="T260">Wenn es kein Geld gibt, wirst du nichts kaufen.</ta>
            <ta e="T269" id="Seg_13132" s="T266">Du gehst, um Fisch zu fangen.</ta>
            <ta e="T277" id="Seg_13133" s="T269">Wenn du keinen Fisch fängst, komm nicht nach Hause.</ta>
            <ta e="T280" id="Seg_13134" s="T277">Geh, um deinen Verwandten zu rufen.</ta>
            <ta e="T286" id="Seg_13135" s="T280">Wenn sie nicht kommen, komm nicht alleine. [?]</ta>
            <ta e="T288" id="Seg_13136" s="T286">Rufe sie gut!</ta>
            <ta e="T290" id="Seg_13137" s="T288">Damit sie kommen.</ta>
            <ta e="T293" id="Seg_13138" s="T290">Kein guter Mensch.</ta>
            <ta e="T300" id="Seg_13139" s="T293">Ich kann ihn mit meinen Augen nicht einmal angucken, er ist so stinkend.</ta>
            <ta e="T302" id="Seg_13140" s="T300">Geh langsam. [?]</ta>
            <ta e="T305" id="Seg_13141" s="T302">Geh nicht schnell. [?]</ta>
            <ta e="T311" id="Seg_13142" s="T305">Ich sehe: Er selbst kommt zu mir.</ta>
            <ta e="T318" id="Seg_13143" s="T311">Er (ruft?) mich zu ihm selbst, aber ich gehe nicht.</ta>
            <ta e="T323" id="Seg_13144" s="T318">Alle möglichen Bäume wachsen im Wald.</ta>
            <ta e="T329" id="Seg_13145" s="T323">Schöne Blumen, sie riechen sehr stark.</ta>
            <ta e="T335" id="Seg_13146" s="T329">Ich fühle mich sehr gut, es riecht gut, es wird etwas gekocht.</ta>
            <ta e="T343" id="Seg_13147" s="T335">Die Hühner laufen auf dem Boden, sie graben mit ihren Füßen in der Erde.</ta>
            <ta e="T346" id="Seg_13148" s="T343">(Auf den Menschen) muss man warten.</ta>
            <ta e="T357" id="Seg_13149" s="T346">Wir sollten auf diesen Mann waren, er kommt bald.</ta>
            <ta e="T359" id="Seg_13150" s="T357">Er wird Wodka bringen.</ta>
            <ta e="T363" id="Seg_13151" s="T359">Sie warten auf ihn.</ta>
            <ta e="T366" id="Seg_13152" s="T363">Sie warten.</ta>
            <ta e="T370" id="Seg_13153" s="T366">Heute werden wir gut sprechen.</ta>
            <ta e="T373" id="Seg_13154" s="T370">Was hast du gesagt?</ta>
            <ta e="T379" id="Seg_13155" s="T373">(…) wir haben dich beschimpft und (du?) hast geschimpft.</ta>
            <ta e="T382" id="Seg_13156" s="T379">Pferde laufen [umher].</ta>
            <ta e="T388" id="Seg_13157" s="T382">Zwei Pferde laufen, ein Pferd läuft.</ta>
            <ta e="T391" id="Seg_13158" s="T388">Mein Sohn kommt.</ta>
            <ta e="T397" id="Seg_13159" s="T391">Wo war er? - Er ging, um Fisch zu holen.</ta>
            <ta e="T403" id="Seg_13160" s="T397">Ich weiß nicht, ob er [welchen] gebracht hat oder nicht.</ta>
            <ta e="T407" id="Seg_13161" s="T403">Wenn er [welchen] gebracht hat, muss [ich] ihn braten.</ta>
            <ta e="T411" id="Seg_13162" s="T407">Diese Frau ist schwanger.</ta>
            <ta e="T415" id="Seg_13163" s="T411">(…) wird bald ein Kind bringen.</ta>
            <ta e="T418" id="Seg_13164" s="T415">[Wir] sollten ihr Angst machen. [?]</ta>
            <ta e="T423" id="Seg_13165" s="T418">[Ich] sollte gehen und etwas kaufen.</ta>
            <ta e="T426" id="Seg_13166" s="T423">Wann ein Tag…</ta>
            <ta e="T427" id="Seg_13167" s="T426">(…).</ta>
            <ta e="T433" id="Seg_13168" s="T427">Wenn es Tag wird, stehe ich auf.</ta>
            <ta e="T438" id="Seg_13169" s="T433">Wenn der Tag zuende geht, lege ich mich hin.</ta>
            <ta e="T446" id="Seg_13170" s="T438">Dieser Winter war sehr kalt, war sehr lang.</ta>
            <ta e="T453" id="Seg_13171" s="T446">Die Abende sind sehr (lang?)</ta>
            <ta e="T460" id="Seg_13172" s="T453">Und als dieses (Jahr?) kam, [ist es] sehr warm.</ta>
            <ta e="T463" id="Seg_13173" s="T460">Die Tage sind lang.</ta>
            <ta e="T471" id="Seg_13174" s="T463">Ich muss Wasser auf den Ofen stellen, weil das Kalb bald [kommt].</ta>
            <ta e="T473" id="Seg_13175" s="T471">Es gibt kein Wasser.</ta>
            <ta e="T479" id="Seg_13176" s="T473">Es trinkt kein kaltes Wasser.</ta>
            <ta e="T488" id="Seg_13177" s="T479">Bring dein Pferd, es steht wahrscheinlich nicht. [?]</ta>
            <ta e="T491" id="Seg_13178" s="T488">[Du] solltest es bringen.</ta>
            <ta e="T495" id="Seg_13179" s="T491">[Mein] Sohn und [meine] Tochter sind erwachsen geworden.</ta>
            <ta e="T498" id="Seg_13180" s="T495">Dann sagte [mein] Sohn [zu einem Mädchen]:</ta>
            <ta e="T502" id="Seg_13181" s="T498">"Wirst du mich heiraten?"</ta>
            <ta e="T503" id="Seg_13182" s="T502">"Ich werde."</ta>
            <ta e="T509" id="Seg_13183" s="T503">"Gut, dann schicke ich meine Eltern zu dir."</ta>
            <ta e="T516" id="Seg_13184" s="T509">Dann schickte er seine Eltern, sie kamen.</ta>
            <ta e="T520" id="Seg_13185" s="T516">Sie sprachen mit den Eltern des Mädchens.</ta>
            <ta e="T527" id="Seg_13186" s="T520">"Habt ihr eure Tochter meinem Sohn gegeben?"</ta>
            <ta e="T529" id="Seg_13187" s="T527">"Haben wir gegeben."</ta>
            <ta e="T538" id="Seg_13188" s="T529">Dann fragten sie das Mädchen: "Wirst du unseren Sohn heiraten?"</ta>
            <ta e="T539" id="Seg_13189" s="T538">"Werde ich."</ta>
            <ta e="T541" id="Seg_13190" s="T539">Dann organisierten sie die Hochzeit. [?]</ta>
            <ta e="T546" id="Seg_13191" s="T541">Sie kauften Wodka, kauften Brot und Fleisch.</ta>
            <ta e="T548" id="Seg_13192" s="T546">Es gab viel.</ta>
            <ta e="T551" id="Seg_13193" s="T548">Dann kamen die Leute.</ta>
            <ta e="T554" id="Seg_13194" s="T551">Sie tranken Wodka.</ta>
            <ta e="T559" id="Seg_13195" s="T554">Dann vergingen ein paar Tage.</ta>
            <ta e="T562" id="Seg_13196" s="T559">Sie setzten Blumen[kränze] auf.</ta>
            <ta e="T565" id="Seg_13197" s="T562">Und gingen zur Kirche.</ta>
            <ta e="T570" id="Seg_13198" s="T565">Dort betete der Priester für sie zu Gott. [?]</ta>
            <ta e="T575" id="Seg_13199" s="T570">Dann setzten sie Kränze auf.</ta>
            <ta e="T578" id="Seg_13200" s="T575">Dann kamen sie nach Hause.</ta>
            <ta e="T582" id="Seg_13201" s="T578">Sie tranken wieder Wodka.</ta>
            <ta e="T585" id="Seg_13202" s="T582">Die Verwandten des Mädchens kamen.</ta>
            <ta e="T589" id="Seg_13203" s="T585">Und die Schwester des Jungen (?) kam.</ta>
            <ta e="T592" id="Seg_13204" s="T589">Es waren viele Leute da.</ta>
            <ta e="T595" id="Seg_13205" s="T592">Es gab viel Wodka.</ta>
            <ta e="T601" id="Seg_13206" s="T595">Es waren viele unterschiedliche Sachen auf dem Tisch.</ta>
            <ta e="T607" id="Seg_13207" s="T601">Als das Mädchen heiraten wollte (?), sagten ihre Eltern:</ta>
            <ta e="T610" id="Seg_13208" s="T607">"Sie hat keine Kleidung.</ta>
            <ta e="T618" id="Seg_13209" s="T610">Wir müssen Schuhe, einen Mantel, ein Kopftuch kaufen.</ta>
            <ta e="T622" id="Seg_13210" s="T618">Sie hat wenig Kleidung, sie hat kein Hemd."</ta>
            <ta e="T627" id="Seg_13211" s="T622">Dann nahmen sie Geld.</ta>
            <ta e="T630" id="Seg_13212" s="T627">Fünfundzwanzig.</ta>
            <ta e="T636" id="Seg_13213" s="T630">Dann gaben die Eltern ihnen eine Kuh.</ta>
            <ta e="T641" id="Seg_13214" s="T636">Die Verwandten gaben [ihnen] Schafe, sie gaben [ihnen] Pferde.</ta>
            <ta e="T645" id="Seg_13215" s="T641">Sie gaben [ihnen] viel Geld.</ta>
            <ta e="T649" id="Seg_13216" s="T645">Es waren viele Leute dort.</ta>
            <ta e="T653" id="Seg_13217" s="T649">Dann lebten sie lange.</ta>
            <ta e="T657" id="Seg_13218" s="T653">Sein älterer Bruder (?).</ta>
            <ta e="T660" id="Seg_13219" s="T657">Sie heirateten wieder (??).</ta>
            <ta e="T667" id="Seg_13220" s="T660">Dann ging sie weg, ihr Vater gab ihr alles.</ta>
            <ta e="T676" id="Seg_13221" s="T667">Er gab alles, er gab ein Pferd, er gab Schafe, er gab Kühe.</ta>
            <ta e="T681" id="Seg_13222" s="T676">Er gab Korn, er baute ein Haus.</ta>
            <ta e="T687" id="Seg_13223" s="T681">Dann gingen sie in die Kirche, um zu heiraten, auf Pferden.</ta>
            <ta e="T698" id="Seg_13224" s="T687">Das Mädchen hat ihren eigenen Ehrenvater, der Junge hat seinen eigenen Ehrenvater, seine eigene Ehrenmutter.</ta>
            <ta e="T703" id="Seg_13225" s="T698">Sie gingen dorthin, dann kamen sie nach Hause.</ta>
            <ta e="T713" id="Seg_13226" s="T703">Dann brachte der Ehrenvater des Mädchens ihr eine Kiste mit Kleidung.</ta>
            <ta e="T716" id="Seg_13227" s="T713">Er gab Kissen.</ta>
            <ta e="T718" id="Seg_13228" s="T716">Er gab ein Federbett.</ta>
            <ta e="T721" id="Seg_13229" s="T718">Dann gingen sie zur Kirche.</ta>
            <ta e="T725" id="Seg_13230" s="T721">Sie saßen am Tisch.</ta>
            <ta e="T731" id="Seg_13231" s="T725">Dann… Das Hemd des Jungen ist schön, neu.</ta>
            <ta e="T735" id="Seg_13232" s="T731">Die Kleidung des Mädchens ist auch schön.</ta>
            <ta e="T736" id="Seg_13233" s="T735">Neu.</ta>
            <ta e="T741" id="Seg_13234" s="T736">Ihr(?) Kopf ist schön, sie tragen Kränze.</ta>
            <ta e="T746" id="Seg_13235" s="T741">Dann zu ihr…</ta>
            <ta e="T751" id="Seg_13236" s="T746">Dann machten sie ihr zwei Zöpfe.</ta>
            <ta e="T754" id="Seg_13237" s="T751">Dann banden sie [ein Kopftuch] auf ihren Kopf.</ta>
            <ta e="T762" id="Seg_13238" s="T754">Als sie ein Mädchen war, hatte sie einen Zopf.</ta>
            <ta e="T777" id="Seg_13239" s="T762">Dann machten die Ehrenmutter des Mädchens und die Ehrenmutter des Jungen ihr zwei [Zöpfe].</ta>
            <ta e="T780" id="Seg_13240" s="T777">Und verbanden ihren Kopf.</ta>
            <ta e="T788" id="Seg_13241" s="T780">Der Ehrenvater brachte sie, das Mädchen und den Jungen, ins Haus.</ta>
            <ta e="T792" id="Seg_13242" s="T788">Niemand war da.</ta>
            <ta e="T797" id="Seg_13243" s="T792">Sie(?) gingen ins Bett, (?).</ta>
            <ta e="T801" id="Seg_13244" s="T797">Ihre Verwandten sagten ihnen:</ta>
            <ta e="T804" id="Seg_13245" s="T801">"Lebt gut.</ta>
            <ta e="T806" id="Seg_13246" s="T804">Streitet euch nicht.</ta>
            <ta e="T812" id="Seg_13247" s="T806">Beschimpft einander nicht.</ta>
            <ta e="T819" id="Seg_13248" s="T812">Soll es viele Kinder geben, Mädchen und Jungen.</ta>
            <ta e="T823" id="Seg_13249" s="T819">Dann riefen sie den Priester.</ta>
            <ta e="T828" id="Seg_13250" s="T823">Er trank sehr viel Wodka.</ta>
            <ta e="T830" id="Seg_13251" s="T828">Und fiel um.</ta>
            <ta e="T832" id="Seg_13252" s="T830">Sie brachten ihn weg.</ta>
            <ta e="T836" id="Seg_13253" s="T832">Sie tranken viel Wodka.</ta>
            <ta e="T840" id="Seg_13254" s="T836">Sie tranken eine lange Zeit, sechs Tage.</ta>
            <ta e="T844" id="Seg_13255" s="T840">Dann lebten sie.</ta>
            <ta e="T848" id="Seg_13256" s="T844">Sie hatten Kinder.</ta>
            <ta e="T852" id="Seg_13257" s="T848">[Sie] hatten Söhne und Töchter.</ta>
            <ta e="T854" id="Seg_13258" s="T852">Töchter.</ta>
            <ta e="T855" id="Seg_13259" s="T854">(…)</ta>
            <ta e="T862" id="Seg_13260" s="T855">Mein Sohn (?) ist gestern weggegangen, er trank Wodka.</ta>
            <ta e="T867" id="Seg_13261" s="T862">Er ist für eine lange Zeit nicht nach Hause gekommen.</ta>
            <ta e="T870" id="Seg_13262" s="T867">Dann kam er und schlief ein.</ta>
            <ta e="T876" id="Seg_13263" s="T870">Am Morgen stand er auf und fing an zu arbeiten.</ta>
            <ta e="T883" id="Seg_13264" s="T876">Ich molk zwei Kühe.</ta>
            <ta e="T888" id="Seg_13265" s="T883">Seine Kuh und meine eigene Kuh.</ta>
            <ta e="T892" id="Seg_13266" s="T888">Eine Frau war schwanger.</ta>
            <ta e="T894" id="Seg_13267" s="T892">Dann gebar sie ein Kind.</ta>
            <ta e="T901" id="Seg_13268" s="T894">Und er ging dorthin und trank Wodka.</ta>
            <ta e="T906" id="Seg_13269" s="T901">Ich sah (?) gestern.</ta>
            <ta e="T913" id="Seg_13270" s="T906">Und heute Abend…</ta>
            <ta e="T923" id="Seg_13271" s="T913">Dann sah ich ihn (?), (er?) brachte etwas und ich war böse.</ta>
            <ta e="T927" id="Seg_13272" s="T923">(…) sie sind nicht aus dem Wald gekommen.</ta>
            <ta e="T930" id="Seg_13273" s="T927">Wann werden sie kommen?</ta>
            <ta e="T933" id="Seg_13274" s="T930">Sie kommen heute Abend.</ta>
            <ta e="T938" id="Seg_13275" s="T933">[Wir] müssen ihnen dann zu essen geben.</ta>
            <ta e="T941" id="Seg_13276" s="T938">Sie sind hungrig.</ta>
            <ta e="T947" id="Seg_13277" s="T941">Ich dachte, dass ihr alle heute kommen würdet.</ta>
            <ta e="T951" id="Seg_13278" s="T947">Und ihr seid nicht gekommen.</ta>
            <ta e="T957" id="Seg_13279" s="T951">Ich habe heute (Eier?) gekocht.</ta>
            <ta e="T965" id="Seg_13280" s="T957">Ich habe Zwiebeln geschnitten, habe Eier [auf den Tisch] gelegt, habe (?) gelegt, um euch zu essen zu geben.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T2" id="Seg_13281" s="T0">[GVY] KT5:49. The beginning is missing (should be on another tape).</ta>
            <ta e="T26" id="Seg_13282" s="T13">[GVY:] üžüttəʔ? Not entirely clear. </ta>
            <ta e="T38" id="Seg_13283" s="T26">KT5:50</ta>
            <ta e="T42" id="Seg_13284" s="T38">KT5:51</ta>
            <ta e="T48" id="Seg_13285" s="T44">[GVY:] KT5:52</ta>
            <ta e="T51" id="Seg_13286" s="T48">[GVY:] It seems that PKZ uses kăštə- for a person's first name and numəj- for the patronymic, maybe after the Russian folklore-style pattern звать-величать.</ta>
            <ta e="T56" id="Seg_13287" s="T53">KT5:53</ta>
            <ta e="T59" id="Seg_13288" s="T56">KT:54.</ta>
            <ta e="T62" id="Seg_13289" s="T59">KT5:55</ta>
            <ta e="T71" id="Seg_13290" s="T68">KT5:56 [GVY:] The case endings in this short fragments are not entirely clear.</ta>
            <ta e="T81" id="Seg_13291" s="T78">KT5:57</ta>
            <ta e="T101" id="Seg_13292" s="T94">[GVY:] mănzʼe?</ta>
            <ta e="T103" id="Seg_13293" s="T101">KT5:58.</ta>
            <ta e="T109" id="Seg_13294" s="T105">[GVY:] In KT, "from the tent", which is more logical in this context, but the form is not clear.</ta>
            <ta e="T113" id="Seg_13295" s="T109">KT5:59 </ta>
            <ta e="T148" id="Seg_13296" s="T143">[GVY:] Kudonzəbiaʔ?</ta>
            <ta e="T150" id="Seg_13297" s="T148">KT5:60</ta>
            <ta e="T153" id="Seg_13298" s="T150">[GVY:] Зарод - 1. Сушило для снопов. 2. Огороженное место для стога.</ta>
            <ta e="T159" id="Seg_13299" s="T157">KT5:61. [GVY:] [maktə]</ta>
            <ta e="T173" id="Seg_13300" s="T166">KT5:62</ta>
            <ta e="T184" id="Seg_13301" s="T179">KT5:63</ta>
            <ta e="T193" id="Seg_13302" s="T190">[GVY:] This sentence is missing in KT5:63</ta>
            <ta e="T208" id="Seg_13303" s="T205">KT5:64</ta>
            <ta e="T223" id="Seg_13304" s="T219">KT5:65 - KT5:65-end</ta>
            <ta e="T229" id="Seg_13305" s="T227">KT5:66</ta>
            <ta e="T236" id="Seg_13306" s="T233">KT5:67</ta>
            <ta e="T240" id="Seg_13307" s="T236">[GVY:] In KT, in the place of this sentence, there is "Iʔ aktʼa!" 'Take money'.</ta>
            <ta e="T246" id="Seg_13308" s="T240">[GVY:] Probably, here PKZ said amnozitsa wihth the Russian ending (= живется).</ta>
            <ta e="T269" id="Seg_13309" s="T266">KT5:68 [GVY:] Should be dʼaʔbəsʼtə?</ta>
            <ta e="T280" id="Seg_13310" s="T277">KT5:69</ta>
            <ta e="T286" id="Seg_13311" s="T280">[GVY:] KT5:68 "kăštəʔ iššo" 'Call again'.</ta>
            <ta e="T293" id="Seg_13312" s="T290">KT5:70</ta>
            <ta e="T302" id="Seg_13313" s="T300">[GVY:] This and the next sentence are missing in KT.</ta>
            <ta e="T318" id="Seg_13314" s="T311">KT5:70-end. [GVY:] manə iʔleʔpə? Unclear.</ta>
            <ta e="T343" id="Seg_13315" s="T335">[GVY:] [ujuzoŋdɨ]</ta>
            <ta e="T370" id="Seg_13316" s="T366">[GVY:] dʼăbaktərləbiaʔ seems a contamination of dʼăbaktərləbaʔ 'speak-FUT-1PL', dʼăbaktərbibaʔ 'speak-PST-1PL', dʼăbaktərbiam 'speak-PST-1SG'</ta>
            <ta e="T379" id="Seg_13317" s="T373">[GVY:] Unclear</ta>
            <ta e="T397" id="Seg_13318" s="T391">[GVY:] to buy fish? Or kolajle 'catch.fish-CVB' 'to catch fish'?</ta>
            <ta e="T418" id="Seg_13319" s="T415">Unclear</ta>
            <ta e="T453" id="Seg_13320" s="T446">[GVY:] nugo Schweiss? Probably nugoʔi = numoʔi 'long-PL'</ta>
            <ta e="T460" id="Seg_13321" s="T453">[GVY:] pie 'year' = 'summer'?</ta>
            <ta e="T488" id="Seg_13322" s="T479">[GVY:] in the horse-box?</ta>
            <ta e="T516" id="Seg_13323" s="T509">[GVY:] iam abam = iat abat [-3SG]</ta>
            <ta e="T527" id="Seg_13324" s="T520">[GVY:] The future tense would be expected here.</ta>
            <ta e="T585" id="Seg_13325" s="T582">[GVY:] tugandə may be also interpreted as relative-LAT, then it would mean "they went to the girl's relatives'.</ta>
            <ta e="T636" id="Seg_13326" s="T630">[GVY:] aban = abat?</ta>
            <ta e="T641" id="Seg_13327" s="T636">[GVY:] tuganzaŋdən?</ta>
            <ta e="T645" id="Seg_13328" s="T641">[GVY:] or aktʼam</ta>
            <ta e="T657" id="Seg_13329" s="T653">[GVY:] Unclear</ta>
            <ta e="T660" id="Seg_13330" s="T657">[GVY:] Unclear</ta>
            <ta e="T698" id="Seg_13331" s="T687">[GVY:] kazak aba/ia, lit. 'Russian father/mother' - посаженый отец/мать, persons who gives away the bride?</ta>
            <ta e="T721" id="Seg_13332" s="T718">[GVY:] should be "from the church"?</ta>
            <ta e="T754" id="Seg_13333" s="T751">[GVY:] ulot</ta>
            <ta e="T777" id="Seg_13334" s="T762">[GVY:] it may be that here are mixed up the godmother/godfather and the honorary mother/father, that is the persons who lead the newly married during the wedding ceremony.</ta>
            <ta e="T852" id="Seg_13335" s="T848">[GVY:] koʔbtaŋ = koʔbsaŋ</ta>
            <ta e="T883" id="Seg_13336" s="T876">[GVY:] Probably she was going to say "Мне пришлось…"</ta>
            <ta e="T938" id="Seg_13337" s="T933">[AAV] From here on, the speed is slowing down. </ta>
            <ta e="T965" id="Seg_13338" s="T957">[GVY:] Kălba - Allium angulosum (D 29b)</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T967" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T968" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T969" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
